<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	public function __construct($application)
	{
		require('../application/models/Plugin/Activity.php');
		require('../application/plugins/CookiePlugin.php');

		parent::__construct($application);
	}

	public function run()
	{
		//Zend_Layout::startMvc();

		// Initialize hooks to avoid code confusion
		Model_Hooks::init();

		parent::run();
	}

	protected function _initRoutes()
	{
		require('../../library/Cubix/Security/Plugin.php');
		require('../application/models/Plugin/I18n.php');

		$this->bootstrap('frontController');
		$front = $this->getResource('frontController');

		$router = $front->getRouter();
		$router->removeDefaultRoutes();

		$router->addRoute(
			'home-page',
			new Zend_Controller_Router_Route(
				':lang_id/*',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'index',
					'lang_id' => ''
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'video',
			new Zend_Controller_Router_Route_Regex(
				'^(video|Video)',
				array(
					'module' => 'default',
					'controller' => 'video',
					'action' => 'index',
					'lang_id' => ''

				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'video_upload',
			new Zend_Controller_Router_Route_Regex(
				'^(video|Video)/upload',
				array(
					'module' => 'default',
					'controller' => 'video',
					'action' => 'upload',
					'lang_id' => ''

				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);
        $router->addRoute(
            'account-actions',
            new Zend_Controller_Router_Route('/account/:action/*',
                array(
                    'module' => 'default',
                    'controller' => 'account'
                ))
        );
        $router->addRoute(
            'skype-actions',
            new Zend_Controller_Router_Route('/skype/:action/*',
                array(
                    'module' => 'default',
                    'controller' => 'skype'
                ))
        );
		$router->addRoute(
			'video_remove',
			new Zend_Controller_Router_Route_Regex(
				'^(video|Video)/remove',
				array(
					'module' => 'default',
					'controller' => 'video',
					'action' => 'remove',
					'lang_id' => ''

				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'get_video_escorts',
			new Zend_Controller_Router_Route_Regex(
				'^(video|Video)/getvideo',
				array(
					'module' => 'default',
					'controller' => 'video',
					'action' => 'getvideo',
					'lang_id' => ''

				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'glossary',
			new Zend_Controller_Router_Route(
				'glossary/:action/*',
				array(
					'module' => 'default',
					'controller' => 'glossary',
					'action' => 'index'
				)
			)
		);

		$router->addRoute(
			'forum',
			new Zend_Controller_Router_Route(
				'forum/:action/*',
				array(
					'module' => 'default',
					'controller' => 'forum',
					'action' => 'index'
				)
			)
		);

		$router->addRoute(
			'external-link',
			new Zend_Controller_Router_Route(
				'go',
				array(
					'module' => 'default',
					'controller' => 'index',
					'action' => 'go'
				)
			)
		);

		$router->addRoute(
			'captcha',
			new Zend_Controller_Router_Route(
				':lang_id/captcha',
				array(
					'module' => 'default',
					'controller' => 'index',
					'action' => 'captcha'
				)
			)
		);

		$router->addRoute(
			'captcha',
			new Zend_Controller_Router_Route(
				':lang_id/captcha',
				array(
					'module' => 'default',
					'controller' => 'index',
					'action' => 'captcha'
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'captcha-def',
			new Zend_Controller_Router_Route(
				'captcha',
				array(
					'module' => 'default',
					'controller' => 'index',
					'action' => 'captcha'
				)
			)
		);

		$router->addRoute(
			'paymentit-fake',
			new Zend_Controller_Router_Route(
				'pagamento',
				array(
					'module' => 'default',
					'controller' => 'index',
					'action' => 'payment-fake'
				)
			)
		);
		
		$router->addRoute(
			'geotargeted',
			new Zend_Controller_Router_Route(
				'geotargeted',
				array(
					'module' => 'default',
					'controller' => 'index',
					'action' => 'geotargeted'
				)
			)
		);
		
		$router->addRoute(
			'analytics',
			new Zend_Controller_Router_Route_Regex(
				'^(analytics|Analytics)',
				array(
					'module' => 'default',
					'controller' => 'analytics',
					'action' => 'set-value',
					'lang_id' => ''

				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);
			/*REDIRECT*/
		$router->addRoute(
			'escort_list_php',
			new Zend_Controller_Router_Route_Regex(
				'^escort_list.php(.+)?',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'main-page'
				),
				array()
			)
		);

		/*REDIRECT*/
		$router->addRoute(
			'agency-profile-redirect',
			new Zend_Controller_Router_Route_Regex(
				'^agency.php(.+)?',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'agency'
				),
				array(
					1 => 'agencyId',
				)
			)
		);
		/*REDIRECT*/
//		$router->addRoute(
//			'escorts-filter-redirect',
//			new Zend_Controller_Router_Route_Regex(
//				'^escorts/(.+)?',
//				array(
//					'module' => 'default',
//					'controller' => 'redirect',
//					'action' => 'index'
//				),
//				array(
//					1 => 'req'
//				)
//			)
//		);

		$router->addRoute(
			'escorts-filter',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/escorts(/.+)?',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'index'
				),
				array(
					1 => 'lang_id',
					2 => 'req'
				)
			)
		);

		$router->addRoute(
			'escorts-filter-def',
			new Zend_Controller_Router_Route_Regex(
				'^escorts(/.+)?',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'index'
				),
				array(
					1 => 'req'
				)
			)
		);

		$router->addRoute(
			'escort-cams',
			new Zend_Controller_Router_Route_Regex(
				'(([a-z]{2})/)?ef-cams(/.+)?',
				array(
					'module' => 'default',
					'controller' => 'cams',
					'action' => 'index'
				),
				array(
					3 => 'req'
				)
			)
		);
		
		/*$router->addRoute(
			'escort-cams-agency-escorts-login',
			new Zend_Controller_Router_Route_Regex(
				'(([a-z]{2})/)?ef-cams/ag-login/([a-f0-9]{32})',
				array(
					'module' => 'default',
					'controller' => 'cams',
					'action' => 'login-agency-escorts'
				),
				array(
					1 => 'lang_id',
					3 => 'hash'
				)
			)
		);*/
		$router->addRoute(
			'escort-cams-goto-book',
			new Zend_Controller_Router_Route_Regex(
				'(([a-z]{2})/)?ef-cams/go-to-book',
				array(
					'module' => 'default',
					'controller' => 'cams',
					'action' => 'go-to-book'
				),
				array(
					1 => 'lang_id'
				)
			)
		);
		
		$router->addRoute(
			'escort-cams-login',
			new Zend_Controller_Router_Route_Regex(
				'(([a-z]{2})/)?ef-cams/login',
				array(
					'module' => 'default',
					'controller' => 'cams',
					'action' => 'login'
				),
				array(
					1 => 'lang_id'
				)
			)
		);
		
		$router->addRoute(
			'escort-cams-signup',
			new Zend_Controller_Router_Route_Regex(
				'(([a-z]{2})/)?ef-cams/signup',
				array(
					'module' => 'default',
					'controller' => 'cams',
					'action' => 'signup'
				),
				array(
					1 => 'lang_id'
				)
			)
		);
		
		$router->addRoute(
			'escort-cams-buy-tokens',
			new Zend_Controller_Router_Route_Regex(
				'(([a-z]{2})/)?ef-cams/buy-tokens',
				array(
					'module' => 'default',
					'controller' => 'cams',
					'action' => 'buy-tokens'
				),
				array(
					1 => 'lang_id'
				)
			)
		);
		
		$router->addRoute(
			'cams-ecardon-popup',
			new Zend_Controller_Router_Route_Regex(
				'(([a-z]{2})/)?ef-cams/ecardon-popup',
				array(
					'module' => 'default',
					'controller' => 'cams',
					'action' => 'ecardon-popup'
				),
				array(
					1 => 'lang_id'
				)
			)
		);
		
		$router->addRoute(
			'ecardon-token-success',
			new Zend_Controller_Router_Route_Regex(
				'(([a-z]{2})/)?ef-cams/ecardon-response',
				array(
					'module' => 'default',
					'controller' => 'cams',
					'action' => 'ecardon-response'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'twispay-token-success',
			new Zend_Controller_Router_Route_Regex(
				'(([a-z]{2})/)?ef-cams/twispay-response',
				array(
					'module' => 'default',
					'controller' => 'cams',
					'action' => 'twispay-response'
				),
				array(
					1 => 'lang_id'
				)
			)
		);
		
		/*$router->addRoute(
			'escorts-redirect-to-main',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/escorts',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'main-page'
				)
			),
			array(
				1 => 'lang'
			)
		);
		$router->addRoute(
			'def-escorts-redirect-to-main',
			new Zend_Controller_Router_Route_Regex(
				'^escorts',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'main-page'
				)
			)
		);*/

		// <editor-fold defaultstate="collapsed" desc="Compatibility Redirects">
		$router->addRoute(
			'main-page-en-redirect',
			new Zend_Controller_Router_Route_Regex(
				'^escorts/(en)/?',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'main-page'
				),
				array(
					1 => 'lang'
				)
			)
		);

		$router->addRoute(
			'escorts-filter-global-redirect',
			new Zend_Controller_Router_Route_Regex(
				'^escorts/(en)(/(boys|trans|tours|upcomingtours|nuove|agence|independantes))?(/(city|region)_it_(.+?))?(/(incall|outcall))?(/([0-9]+))?$',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'geo'
				),
				array(
					1 => 'lang',
					3 => 'type',
					5 => 'geo',
					6 => 'geoval',
					8 => 'filter',
					10 => 'page'
				)
			)
		);

		/*$router->addRoute(
			'escorts-filter-global-redirect-require-page',
			new Zend_Controller_Router_Route_Regex(
				'^escorts(/(boys|trans|citytours|upcomingtours|nuove|agence|independantes))?(/(city|region)_fr_(.+?))?(/(incall|outcall))?(/([0-9]+))$',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'geo',
					'lang' => null
				),
				array(
					2 => 'type',
					4 => 'geo',
					5 => 'geoval',
					7 => 'filter',
					9 => 'page'
				)
			)
		);*/

		// </editor-fold>





		$router->addRoute(
			'static-pages',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/page/(.+)?',
				array(
					'module' => 'default',
					'controller' => 'static-page',
					'action' => 'show'
				),
				array(
					1 => 'lang_id',
					2 => 'page_slug'
				)
			)
		);

		$router->addRoute(
			'static-pages-def',
			new Zend_Controller_Router_Route_Regex(
				'^page/(.+)?',
				array(
					'module' => 'default',
					'controller' => 'static-page',
					'action' => 'show'
				),
				array(
					1 => 'page_slug'
				)
			)
		);

		$router->addRoute(
			'escort-photos',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/escorts/photos',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'photos'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'escort-photos-def',
			new Zend_Controller_Router_Route_Regex(
				'^escorts/photos',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'photos'
				)
			)
		);

		$router->addRoute(
			'escort-photos-v2',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/escorts/photos-v2',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'photos-v2'
				),
				array(
					1 => 'lang_id'
				)
			)
		);
		$router->addRoute(
			'escort-photos-v2-def',
			new Zend_Controller_Router_Route_Regex(
				'^escorts/photos-v2',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'photos-v2'
				)
			)
		);

		$router->addRoute(
			'escort-private-photos',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/escorts/private-photos',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'private-photos'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'escort-private-photos-def',
			new Zend_Controller_Router_Route_Regex(
				'^escorts/private-photos',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'private-photos'
				)
			)
		);

		$router->addRoute(
			'agency-escorts',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/escorts/agency-escorts',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'agency-escorts'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'agency-escorts-def',
			new Zend_Controller_Router_Route_Regex(
				'^escorts/agency-escorts',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'agency-escorts'
				)
			)
		);

		$router->addRoute(
			'agency-escorts-v2',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/escorts/agency-escorts-v2',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'agency-escorts-v2'
				),
				array(
					1 => 'lang_id'
				)
			)
		);
		$router->addRoute(
			'agency-escorts-v2-def',
			new Zend_Controller_Router_Route_Regex(
				'^escorts/agency-escorts-v2',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'agency-escorts-v2'
				)
			)
		);

		/*$router->addRoute(
			'agency-profile',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/agency/([^/]+)(/.+)?',
				array(
					'module' => 'default',
					'controller' => 'agencies',
					'action' => 'show'
				),
				array(
					1 => 'lang_id',
					2 => 'agencyName',
					3 => 'req'
				)
			)
		);

		$router->addRoute(
			'agency-profile-def',
			new Zend_Controller_Router_Route_Regex(
				'^agency/([^/]+)(/.+)?',
				array(
					'module' => 'default',
					'controller' => 'agencies',
					'action' => 'show'
				),
				array(
					1 => 'agencyName',
					2 => 'req'
				)
			)
		);*/

		/*REDIRECT*/
		$router->addRoute(
			'agency-profile-redirect2',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/agency/([^/]+)(/.+)?',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'agencies2'
				),
				array(
					1 => 'lang_id',
					2 => 'agencyName',
					3 => 'req'
				)
			)
		);

		$router->addRoute(
			'agency-profile-redirect2-def',
			new Zend_Controller_Router_Route_Regex(
				'^agency/([^/]+)(/.+)?',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'agencies2'
				),
				array(
					1 => 'agencyName',
					2 => 'req'
				)
			)
		);
		//

		$router->addRoute(
			'agency-profile',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/agency/(.+)?\-([0-9]+)(/.+)?',
				array(
					'module' => 'default',
					'controller' => 'agencies',
					'action' => 'show'
				),
				array(
					1 => 'lang_id',
					2 => 'agencyName',
					3 => 'agency_id',
					4 => 'req'
				)
			)
		);

		$router->addRoute(
			'agency-profile-def',
			new Zend_Controller_Router_Route_Regex(
				'^agency/(.+)?\-([0-9]+)(/.+)?',
				array(
					'module' => 'default',
					'controller' => 'agencies',
					'action' => 'show'
				),
				array(
					1 => 'agencyName',
					2 => 'agency_id',
					3 => 'req'
				)
			)
		);

		/*REDIRECT*/
		$router->addRoute(
			'escort-profile-redirect2',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/accompagnatrici/(.+)?',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'escort2'
				),
				array(
					1 => 'lang_id',
					2 => 'showname'
				)
			)
		);

		$router->addRoute(
			'escort-profile-redirect2-def',
			new Zend_Controller_Router_Route_Regex(
				'^accompagnatrici/(.+)?',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'escort2'
				),
				array(
					1 => 'showname'
				)
			)
		);
		//

		/*$router->addRoute(
			'escort-profile',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/accompagnatrici/(.+)?\-([0-9]+)',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'profile'
				),
				array(
					1 => 'lang_id',
					2 => 'escortName',
					3 => 'escort_id'
				)
			)
		);*/

		$router->addRoute(
			'escort-profile-v2',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/accompagnatrici/(.+)?\-([0-9]+)',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'profile-v2'
				),
				array(
					1 => 'lang_id',
					2 => 'escortName',
					3 => 'escort_id'
				)
			)
		);

		/*$router->addRoute(
			'escort-profile-red',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/escort/(.+)?',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'escort'
				),
				array(
					1 => 'lang',
					2 => 'showname'
				)
			)
		);
		$router->addRoute(
			'escort-profile-red-def',
			new Zend_Controller_Router_Route_Regex(
				'^escort/(.+)?',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'escort'
				),
				array(
					1 => 'showname'
				)
			)
		);*/

		/*$router->addRoute(
			'escort-profile-def',
			new Zend_Controller_Router_Route_Regex(
				'^accompagnatrici/(.+)?\-([0-9]+)',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'profile'
				),
				array(
					1 => 'escortName',
					2 => 'escort_id'
				)
			)
		);*/

		$router->addRoute(
			'escort-profile-v2-def',
			new Zend_Controller_Router_Route_Regex(
				'^accompagnatrici/(.+)?\-([0-9]+)',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'profile-v2'
				),
				array(
					1 => 'escortName',
					2 => 'escort_id'
				)
			)
		);

		/*REDIRECT*/
		$router->addRoute(
			'escort-profile-redirect',
			new Zend_Controller_Router_Route_Regex(
				'^accompagnatrici/(.+?)/(en)$',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'escort'
				),
				array(
					1 => 'showname',
					2 => 'lang'
				)
			)
		);

		/*REDIRECT*/
		$router->addRoute(
			'directory-redirect',
			new Zend_Controller_Router_Route_Regex(
				'^directory/((en)|it)-(.+)',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'directory'
				),
				array(
					2 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'login-redirect',
			new Zend_Controller_Router_Route_Regex(
				'^login.php',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'login'
				)
			)
		);

		$router->addRoute(
			'last-viewed-escorts',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/escorts/viewed-escorts',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'viewed-escorts'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'last-viewed-escorts-def',
			new Zend_Controller_Router_Route_Regex(
				'^escorts/viewed-escorts',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'viewed-escorts'
				)
			)
		);

		$router->addRoute(
			'watched-escorts',
			new Zend_Controller_Router_Route_Regex(
				'^escorts/watched-escorts',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'watched-escorts'
				)
			)
		);

		$router->addRoute(
			'latest-videos',
			new Zend_Controller_Router_Route_Regex(
				'^escorts/latest-videos',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'latest-videos'
				)
			)
		);

		$router->addRoute(
			'late-night-escorts',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/escorts/late-night-girls',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'late-night-girls'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'late-night-escorts-def',
			new Zend_Controller_Router_Route_Regex(
				'^escorts/late-night-girls',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'late-night-girls'
				)
			)
		);

		$router->addRoute(
			'escorts-search',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/search',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'search'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'escorts-search-def',
			new Zend_Controller_Router_Route_Regex(
				'^search',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'search'
				)
			)
		);

		$router->addRoute(
			'geography',
			new Zend_Controller_Router_Route(
				':lang_id/geography/:action/*',
				array(
					'module' => 'default',
					'controller' => 'geography',
					'action' => 'index'
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'geography-def',
			new Zend_Controller_Router_Route(
				'geography/:action/*',
				array(
					'module' => 'default',
					'controller' => 'geography',
					'action' => 'index'
				)
			)
		);

		$router->addRoute(
			'planner',
			new Zend_Controller_Router_Route(
				'planner/:action/*',
				array(
					'module' => 'default',
					'controller' => 'planner',
					'action' => 'index'
				)
			)
		);

		$router->addRoute(
			'private',
			new Zend_Controller_Router_Route(
				':lang_id/private/:action/*',
				array(
					'module' => 'default',
					'controller' => 'private',
					'action' => 'index'
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'private-def',
			new Zend_Controller_Router_Route(
				'private/:action/*',
				array(
					'module' => 'default',
					'controller' => 'private',
					'action' => 'index'
				)
			)
		);

		$router->addRoute(
			'privateV2Lng',
			new Zend_Controller_Router_Route(
				':lang_id/private-v2/:action/*',
				array(
					'module' => 'default',
					'controller' => 'private-v2',
					'action' => 'index',
					'lang_id' => ''
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'privateV2',
			new Zend_Controller_Router_Route(
				'private-v2/:action/*',
				array(
					'module' => 'default',
					'controller' => 'private-v2',
					'action' => 'index'
				)
			)
		);

		$router->addRoute(
			'privateProfileLng',
			new Zend_Controller_Router_Route(
				':lang_id/private-v2/profile/:action/*',
				array(
					'module' => 'default',
					'controller' => 'profile',
					'action' => 'index',
					'lang_id' => ''
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'privateProfile',
			new Zend_Controller_Router_Route(
				'private-v2/profile/:action/*',
				array(
					'module' => 'default',
					'controller' => 'profile',
					'action' => 'index'
				)
			)
		);


		$router->addRoute(
			'photoVideo',
			new Zend_Controller_Router_Route(
				'photo-video/:action/*',
				array(
					'module' => 'default',
					'controller' => 'photo-video',
					'action' => 'index'
				)
			)
		);

		$router->addRoute(
			'photoVideoLng',
			new Zend_Controller_Router_Route(
				':lang_id/photo-video/:action/*',
				array(
					'module' => 'default',
					'controller' => 'photo-video',
					'action' => 'index',
					'lang_id' => ''
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'privateMessaging',
			new Zend_Controller_Router_Route(
				'private-messaging/:action/*',
				array(
					'module' => 'default',
					'controller' => 'private-messaging',
					'action' => 'index'
				)
			)
		);

		$router->addRoute(
			'privateMessagingLng',
			new Zend_Controller_Router_Route(
				':lang_id/private-messaging/:action/*',
				array(
					'module' => 'default',
					'controller' => 'private-messaging',
					'action' => 'index',
					'lang_id' => ''
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'splash',
			new Zend_Controller_Router_Route(
				'/splash',
				array(
					'module' => 'default',
					'controller' => 'index',
					'action' => 'splash'
				)
			)
		);

		$router->addRoute(
			'robots',
			new Zend_Controller_Router_Route(
				'/robots.txt',
				array(
					'module' => 'default',
					'controller' => 'index',
					'action' => 'robots'
				)
			)
		);

		$router->addRoute(
			'private-signup',
			new Zend_Controller_Router_Route_Regex(
				'([a-z]{2})/private/signup-(member|vip-member|escort|agency)',
				array(
					'module' => 'default',
					'controller' => 'private',
					'action' => 'signup'
				),
				array(
					1 => 'lang_id',
					2 => 'type'
				)
			)
		);

		$router->addRoute(
			'private-signup-def',
			new Zend_Controller_Router_Route_Regex(
				'private/signup-(member|vip-member|escort|agency)',
				array(
					'module' => 'default',
					'controller' => 'private',
					'action' => 'signup'
				),
				array(
					1 => 'type'
				)
			)
		);

		$router->addRoute(
			'private-cam-signin',
			new Zend_Controller_Router_Route_Regex(
				'private/cams-signin',
				array(
					'module' => 'default',
					'controller' => 'private',
					'action' => 'signin',
					'is_cams' => true
				)
			)
		);
		
		$router->addRoute(
			'gotm-current',
			new Zend_Controller_Router_Route_Regex(
				'escorts/gotm-current',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'gotm-current'
				),
				array(
					2 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'private-remove-from-favorites',
			new Zend_Controller_Router_Route_Regex(
				'([a-z]{2})/private/remove-from-favorites(.+)',
				array(
					'module' => 'default',
					'controller' => 'private',
					'action' => 'remove-from-favorites'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'private-remove-from-favorites-def',
			new Zend_Controller_Router_Route_Regex(
				'private/remove-from-favorites(.+)',
				array(
					'module' => 'default',
					'controller' => 'private',
					'action' => 'remove-from-favorites'
				)
			)
		);

		$router->addRoute(
			'private-add-to-favorites',
			new Zend_Controller_Router_Route_Regex(
				'([a-z]{2})/private/add-to-favorites(.+)',
				array(
					'module' => 'default',
					'controller' => 'private',
					'action' => 'add-to-favorites'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'private-add-to-favorites-def',
			new Zend_Controller_Router_Route_Regex(
				'private/add-to-favorites(.+)',
				array(
					'module' => 'default',
					'controller' => 'private',
					'action' => 'add-to-favorites'
				)
			)
		);

		$router->addRoute(
			'private-activate',
			new Zend_Controller_Router_Route_Regex(
				'([a-z]{2})/(.+)?/private/activate/(.+)?/([a-f0-9]{32})',
				array(
					'module' => 'default',
					'controller' => 'private',
					'action' => 'activate'
				),
				array(
					1 => 'lang_id',
					2 => 'user_type',
					3 => 'email',
					4 => 'hash'
				)
			)
		);

		$router->addRoute(
			'private-activate-def',
			new Zend_Controller_Router_Route_Regex(
				'(.+)?/private/activate/(.+)?/([a-f0-9]{32})',
				array(
					'module' => 'default',
					'controller' => 'private',
					'action' => 'activate'
				),
				array(
					1 => 'user_type',
					2 => 'email',
					3 => 'hash'
				)
			)
		);

		$router->addRoute(
			'private-change-pass',
			new Zend_Controller_Router_Route_Regex(
				'(([a-z]{2})/)?private/change-pass/([-_a-z0-9]+)/([a-f0-9]{32})',
				array(
					'module' => 'default',
					'controller' => 'private',
					'action' => 'change-pass',
					'lang_id' => null
				),
				array(
					2 => 'lang_id',
					3 => 'username',
					4 => 'hash'
				)
			)
		);

		$router->addRoute(
			'private-verified',
			new Zend_Controller_Router_Route(
				':lang_id/private/verify/:action',
				array(
					'module' => 'default',
					'controller' => 'verify',
					'action' => 'index'
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'private-verified-def',
			new Zend_Controller_Router_Route(
				'private/verify/:action',
				array(
					'module' => 'default',
					'controller' => 'verify',
					'action' => 'index'
				)
			)
		);

		$router->addRoute(
			'city-alerts',
			new Zend_Controller_Router_Route(
				'city-alerts/:action/*',
				array(
					'module' => 'default',
					'controller' => 'city-alerts',
					'action' => 'index'
				)
			)
		);

		$router->addRoute(
			'city-votes',
			new Zend_Controller_Router_Route(
				'city-votes/:action/*',
				array(
					'module' => 'default',
					'controller' => 'city-votes',
					'action' => 'index'
				)
			)
		);

		$router->addRoute(
			'city-votes-save',
			new Zend_Controller_Router_Route(
				'city-votes/save',
				array(
					'module' => 'default',
					'controller' => 'city-votes',
					'action' => 'save'
				)
			)
		);

		$router->addRoute(
			'city-votes-ajax',
			new Zend_Controller_Router_Route(
				'city-votes/ajax-get',
				array(
					'module' => 'default',
					'controller' => 'city-votes',
					'action' => 'ajax-get'
				)
			)
		);

		$router->addRoute(
			'city-votes-chart-data',
			new Zend_Controller_Router_Route(
				'city-votes/chart-data',
				array(
					'module' => 'default',
					'controller' => 'city-votes',
					'action' => 'chart-data'
				)
			)
		);

		$router->addRoute(
			'city-votes-get-escorts',
			new Zend_Controller_Router_Route(
				'city-votes/get-escorts',
				array(
					'module' => 'default',
					'controller' => 'city-votes',
					'action' => 'get-escorts'
				)
			)
		);

		$router->addRoute(
			'xmas-game-index',
			new Zend_Controller_Router_Route(
				'xmas/:action/*',
				array(
					'module' => 'default',
					'controller' => 'xmas',
					'action' => 'index'
				)
			)
		);

		$router->addRoute(
			'xmas-game-getstatus',
			new Zend_Controller_Router_Route(
				'xmas/get-status',
				array(
					'module' => 'default',
					'controller' => 'xmas',
					'action' => 'get-status'
				)
			)
		);

		$router->addRoute(
			'xmas-game-terms',
			new Zend_Controller_Router_Route(
				'xmas/terms',
				array(
					'module' => 'default',
					'controller' => 'xmas',
					'action' => 'terms'
				)
			)
		);

		$router->addRoute(
			'xmas-game-dates',
			new Zend_Controller_Router_Route(
				'xmas/get-timestamps',
				array(
					'module' => 'default',
					'controller' => 'xmas',
					'action' => 'get-timestamps'
				)
			)
		);

		$router->addRoute(
			'xmas-set-agree',
			new Zend_Controller_Router_Route(
				'xmas/set-agree',
				array(
					'module' => 'default',
					'controller' => 'xmas',
					'action' => 'set-agree'
				)
			)
		);

		$router->addRoute(
			'xmas-game',
			new Zend_Controller_Router_Route(
				'xmas/game',
				array(
					'module' => 'default',
					'controller' => 'xmas',
					'action' => 'game'
				)
			)
		);

		$router->addRoute(
			'xmas-send-vote',
			new Zend_Controller_Router_Route(
				'xmas/send-vote',
				array(
					'module' => 'default',
					'controller' => 'xmas',
					'action' => 'send-vote'
				)
			)
		);
		/*****************/

		$router->addRoute(
			'feedback',
			new Zend_Controller_Router_Route(
				':lang_id/feedback',
				array(
					'module' => 'default',
					'controller' => 'index',
					'action' => 'feedback'
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'feedback-def',
			new Zend_Controller_Router_Route(
				'feedback',
				array(
					'module' => 'default',
					'controller' => 'index',
					'action' => 'feedback'
				)
			)
		);

		$router->addRoute(
			'contact',
			new Zend_Controller_Router_Route(
				':lang_id/contact',
				array(
					'module' => 'default',
					'controller' => 'index',
					'action' => 'contact'
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'contact-def',
			new Zend_Controller_Router_Route(
				'contact',
				array(
					'module' => 'default',
					'controller' => 'index',
					'action' => 'contact'
				)
			)
		);


		/* --> Support <-- */
		$router->addRoute(
			'support',
			new Zend_Controller_Router_Route(
				':lang_id/support/:action',
				array(
					'module' => 'default',
					'controller' => 'support',
					'action' => 'index'
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'skype',
			new Zend_Controller_Router_Route(
				'skype/:action',
				array(
					'module' => 'default',
					'controller' => 'skype',
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'support-def',
			new Zend_Controller_Router_Route(
				'support/:action',
				array(
					'module' => 'default',
					'controller' => 'support',
					'action' => 'index'
				)
			)
		);
		/* <-- Support --> */

		/* --> Online Billing <-- */
		$router->addRoute(
			'online-billing',
			new Zend_Controller_Router_Route(
				':lang_id/online-billing/:action',
				array(
					'module' => 'default',
					'controller' => 'online-billing',
					'action' => 'list'
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'online-billing-def',
			new Zend_Controller_Router_Route(
				'online-billing/:action',
				array(
					'module' => 'default',
					'controller' => 'online-billing',
					'action' => 'list'
				)
			)
		);
		/* <-- Online Billing --> */

		/* --> Online Billing V2 <-- */
		$router->addRoute(
			'online-billing-v2',
			new Zend_Controller_Router_Route(
				':lang_id/online-billing-v2/:action',
				array(
					'module' => 'default',
					'controller' => 'online-billing-v2',
					'action' => 'index'
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'online-billing-v2-def',
			new Zend_Controller_Router_Route(
				'online-billing-v2/:action',
				array(
					'module' => 'default',
					'controller' => 'online-billing-v2',
					'action' => 'index'
				)
			)
		);
		/* <-- Online Billing V2 --> */

		/* --> Profile boost <-- */
		$router->addRoute(
			'ob-profile-boost',
			new Zend_Controller_Router_Route(
				':lang_id/profile-boost/:action',
				array(
					'module' => 'default',
					'controller' => 'profile-boost',
					'action' => 'index'
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'ob-profile-boost-def',
			new Zend_Controller_Router_Route(
				'profile-boost/:action',
				array(
					'module' => 'default',
					'controller' => 'profile-boost',
					'action' => 'index'
				)
			)
		);
		/* <-- Profile boost --> */


		/* --> ob expo boost <-- */
		$router->addRoute(
			'ob-expo',
			new Zend_Controller_Router_Route(
				':lang_id/expo/:action',
				array(
					'module' => 'default',
					'controller' => 'expo',
					'action' => 'index'
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'ob-expo-def',
			new Zend_Controller_Router_Route(
				'expo/:action',
				array(
					'module' => 'default',
					'controller' => 'expo',
					'action' => 'index'
				)
			)
		);
		/* <-- Profile boost --> */

		/* --> Profile bump <-- */
		$router->addRoute(
			'ob-profile-bump',
			new Zend_Controller_Router_Route(
				':lang_id/profile-bumps/:action',
				array(
					'module' => 'default',
					'controller' => 'profile-bumps',
					'action' => 'index'
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'ob-profile-bump-def',
			new Zend_Controller_Router_Route(
				'profile-bumps/:action',
				array(
					'module' => 'default',
					'controller' => 'profile-bumps',
					'action' => 'index'
				)
			)
		);
		/* <-- Profile bump --> */
		
		$router->addRoute(
			'benchmark',
			new Zend_Controller_Router_Route(
				'benchmark',
				array(
					'module' => 'default',
					'controller' => 'index',
					'action' => 'benchmark'
				)
			)
		);

		$router->addRoute(
			'comments',
			new Zend_Controller_Router_Route(
				':lang_id/comments/ajax-show',
				array(
					'module' => 'default',
					'controller' => 'comments',
					'action' => 'ajax-show'
				)
			),
			array(
				'lang_id' => '[a-z]{2}'
			)
		);

		$router->addRoute(
			'comments-def',
			new Zend_Controller_Router_Route(
				'comments/ajax-show',
				array(
					'module' => 'default',
					'controller' => 'comments',
					'action' => 'ajax-show'
				)
			)
		);

		$router->addRoute(
			'comments-v2',
			new Zend_Controller_Router_Route(
				':lang_id/comments-v2/ajax-show',
				array(
					'module' => 'default',
					'controller' => 'comments-v2',
					'action' => 'ajax-show'
				)
			),
			array(
				'lang_id' => '[a-z]{2}'
			)
		);
		$router->addRoute(
			'comments-v2-def',
			new Zend_Controller_Router_Route(
				'comments-v2/ajax-show',
				array(
					'module' => 'default',
					'controller' => 'comments-v2',
					'action' => 'ajax-show'
				)
			)
		);

		$router->addRoute(
			'add-comment-v2',
			new Zend_Controller_Router_Route(
				':lang_id/comments-v2/ajax-add-comment',
				array(
					'module' => 'default',
					'controller' => 'comments-v2',
					'action' => 'ajax-add-comment'
				)
			),
			array(
				'lang_id' => '[a-z]{2}'
			)
		);
		$router->addRoute(
			'add-comment-v2-def',
			new Zend_Controller_Router_Route(
				'comments-v2/ajax-add-comment',
				array(
					'module' => 'default',
					'controller' => 'comments-v2',
					'action' => 'ajax-add-comment'
				)
			)
		);

		$router->addRoute(
			'getAlerts',
			new Zend_Controller_Router_Route(
				':lang_id/escorts/ajax-alerts',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'ajax-alerts'
				)
			),
			array(
				'lang_id' => '[a-z]{2}'
			)
		);

		$router->addRoute(
			'getAlerts-def',
			new Zend_Controller_Router_Route(
				'escorts/ajax-alerts',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'ajax-alerts'
				)
			)
		);

		$router->addRoute(
			'alertsSave',
			new Zend_Controller_Router_Route(
				':lang_id/escorts/ajax-alerts-save',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'ajax-alerts-save'
				)
			),
			array(
				'lang_id' => '[a-z]{2}'
			)
		);

		$router->addRoute(
			'alertsSave-def',
			new Zend_Controller_Router_Route(
				'escorts/ajax-alerts-save',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'ajax-alerts-save'
				)
			)
		);


		$router->addRoute(
			'ajax-instant-book',
			new Zend_Controller_Router_Route_Regex(
				'(([a-z]{2})/)?escorts/ajax-instant-book',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'ajax-instant-book',
				),
				array(
					2 => 'lang_id',
				)
			)
		);

		$router->addRoute(
			'add-comment',
			new Zend_Controller_Router_Route(
				':lang_id/comments/ajax-add-comment',
				array(
					'module' => 'default',
					'controller' => 'comments',
					'action' => 'ajax-add-comment'
				)
			),
			array(
				'lang_id' => '[a-z]{2}'
			)
		);

		$router->addRoute(
			'add-comment-def',
			new Zend_Controller_Router_Route(
				'comments/ajax-add-comment',
				array(
					'module' => 'default',
					'controller' => 'comments',
					'action' => 'ajax-add-comment'
				)
			)
		);

		$router->addRoute(
			'vote-comment',
			new Zend_Controller_Router_Route(
				':lang_id/comments/ajax-vote',
				array(
					'module' => 'default',
					'controller' => 'comments',
					'action' => 'ajax-vote'
				)
			),
			array(
				'lang_id' => '[a-z]{2}'
			)
		);

		$router->addRoute(
			'vote-comment-def',
			new Zend_Controller_Router_Route(
				'comments/ajax-vote',
				array(
					'module' => 'default',
					'controller' => 'comments',
					'action' => 'ajax-vote'
				)
			)
		);

		$router->addRoute(
			'vote-comment-v2',
			new Zend_Controller_Router_Route(
				':lang_id/comments-v2/ajax-vote',
				array(
					'module' => 'default',
					'controller' => 'comments-v2',
					'action' => 'ajax-vote'
				)
			),
			array(
				'lang_id' => '[a-z]{2}'
			)
		);
		$router->addRoute(
			'vote-comment-v2-def',
			new Zend_Controller_Router_Route(
				'comments-v2/ajax-vote',
				array(
					'module' => 'default',
					'controller' => 'comments-v2',
					'action' => 'ajax-vote'
				)
			)
		);

		$router->addRoute(
			'reviews',
			new Zend_Controller_Router_Route(
				':lang_id/recensioni',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'index'
				)
			),
			array(
				'lang_id' => '[a-z]{2}'
			)
		);

		$router->addRoute(
			'reviews-def',
			new Zend_Controller_Router_Route(
				'recensioni',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'index'
				)
			)
		);

		/*REDIRECT*/
		$router->addRoute(
			'reviews-escort-redirect2',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/recensioni/(.+)?',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'reviews-escort'
				),
				array(
					1 => 'lang_id',
					2 => 'showname'
				)
			)
		);

		$router->addRoute(
			'reviews-escort-redirect2-def',
			new Zend_Controller_Router_Route_Regex(
				'^recensioni/(.+)?',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'reviews-escort'
				),
				array(
					1 => 'showname'
				)
			)
		);
		//

		$router->addRoute(
			'reviews-escort',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/recensioni/(.+)?\-([0-9]+)',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'escort'
				),
				array(
					1 => 'lang_id',
					2 => 'escortName',
					3 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'reviews-escort-def',
			new Zend_Controller_Router_Route_Regex(
				'^recensioni/(.+)?\-([0-9]+)',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'escort'
				),
				array(
					1 => 'escortName',
					2 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'reviews-redirect',
			new Zend_Controller_Router_Route_Regex(
				'^recensioni/(en)$',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'reviews'
				),
				array(
					1 => 'lang'
				)
			)
		);

		$router->addRoute(
			'reviews-member',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/recensioni/member/(.+)?',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'member'
				),
				array(
					1 => 'lang_id',
					2 => 'username'
				)
			)
		);

		$router->addRoute(
			'reviews-member-def',
			new Zend_Controller_Router_Route_Regex(
				'^recensioni/member/(.+)?',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'member'
				),
				array(
					1 => 'username'
				)
			)
		);

		/*$router->addRoute(
			'reviews-escort-redirect',
			new Zend_Controller_Router_Route_Regex(
				'^recensioni/(en)/(.+?)(/([0-9]+))?$',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'reviews-escort'
				),
				array(
					1 => 'lang',
					2 => 'showname',
					4 => 'page'
				)
			)
		);*/

		$router->addRoute(
			'top-reviewers',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/recensioni/top-reviewers',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'top-reviewers'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'top-reviewers-def',
			new Zend_Controller_Router_Route_Regex(
				'^recensioni/top-reviewers',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'top-reviewers'
				),
				array()
			)
		);

		$router->addRoute(
			'ajax-escorts-search',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/reviews/ajax-escorts-search',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'ajax-escorts-search'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'ajax-escorts-search-def',
			new Zend_Controller_Router_Route_Regex(
				'^reviews/ajax-escorts-search',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'ajax-escorts-search'
				),
				array()
			)
		);

		$router->addRoute(
			'ajax-members-search',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/reviews/ajax-members-search',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'ajax-members-search'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'ajax-members-search-def',
			new Zend_Controller_Router_Route_Regex(
				'^reviews/ajax-members-search',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'ajax-members-search'
				),
				array()
			)
		);

		$router->addRoute(
			'ajax-agencies-search',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/reviews/ajax-agencies-search',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'ajax-agencies-search'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'ajax-agencies-search-def',
			new Zend_Controller_Router_Route_Regex(
				'^reviews/ajax-agencies-search',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'ajax-agencies-search'
				),
				array()
			)
		);

		$router->addRoute(
			'top-ladies',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/recensioni/top-ladies',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'top-ladies'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'top-ladies-def',
			new Zend_Controller_Router_Route_Regex(
				'^recensioni/top-ladies',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'top-ladies'
				),
				array()
			)
		);

		$router->addRoute(
			'reviews-search',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/recensioni/search',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'search'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'reviews-search-def',
			new Zend_Controller_Router_Route_Regex(
				'^recensioni/search',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'search'
				),
				array()
			)
		);

		$router->addRoute(
			'reviews-escorts',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/recensioni/escorts',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'escorts'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'reviews-escorts-def',
			new Zend_Controller_Router_Route_Regex(
				'^recensioni/escorts',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'escorts'
				),
				array()
			)
		);

		$router->addRoute(
			'reviews-agencies',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/recensioni/agencies',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'agencies'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'reviews-agencies-def',
			new Zend_Controller_Router_Route_Regex(
				'^recensioni/agencies',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'agencies'
				),
				array()
			)
		);

		$router->addRoute(
			'private-v2-reviews',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/private-v2/reviews',
				array(
					'module' => 'default',
					'controller' => 'private-v2',
					'action' => 'reviews'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'private-v2-reviews-def',
			new Zend_Controller_Router_Route_Regex(
				'^private-v2/reviews',
				array(
					'module' => 'default',
					'controller' => 'private-v2',
					'action' => 'reviews'
				),
				array()
			)
		);

		$router->addRoute(
			'members-choice',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/members-choice',
				array(
					'module' => 'default',
					'controller' => 'members',
					'action' => 'choice'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'members-choice-def',
			new Zend_Controller_Router_Route_Regex(
				'^members-choice',
				array(
					'module' => 'default',
					'controller' => 'members',
					'action' => 'choice'
				),
				array()
			)
		);

		$router->addRoute(
			'member-info',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?member/(.+)?',
				array(
					'module' => 'default',
					'controller' => 'members',
					'action' => 'index'
				),
				array(
					2 => 'lang_id',
					3 => 'username'

				)
			)
		);

		$router->addRoute(
			'member-info-actions-lng',
			new Zend_Controller_Router_Route(
				':lang_id/members/:action/*',
				array(
					'module' => 'default',
					'controller' => 'members',
					'action' => 'index',
					'lang_id' => ''
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'member-info-actions',
			new Zend_Controller_Router_Route(
				'members/:action/*',
				array(
					'module' => 'default',
					'controller' => 'members',
					'action' => 'index'
				)
			)
		);

		$router->addRoute(
			'private-v2-alerts',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/private-v2/alerts',
				array(
					'module' => 'default',
					'controller' => 'private-v2',
					'action' => 'alerts'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'private-v2-alerts-def',
			new Zend_Controller_Router_Route_Regex(
				'^private-v2/alerts',
				array(
					'module' => 'default',
					'controller' => 'private-v2',
					'action' => 'alerts'
				),
				array()
			)
		);

		$router->addRoute(
			'newsletter-cron-def',
			new Zend_Controller_Router_Route_Regex(
				'^newsletter/email-sync-cron',
				array(
					'module' => 'default',
					'controller' => 'newsletter',
					'action' => 'email-sync-cron'
				),
				array()
			)
		);





		/*$router->addRoute(
			'signInUp',
			new Zend_Controller_Router_Route(':lang_id/private/sign-in-up',
			array(
				'module' => 'default',
				'controller' => 'private',
				'action' => 'sign-in-up'
			)), array(
				'lang_id' => '[a-z]{2}'
			)
		);

		$router->addRoute(
			'signInUp-def',
			new Zend_Controller_Router_Route('private/sign-in-up',
			array(
				'module' => 'default',
				'controller' => 'private',
				'action' => 'sign-in-up'
			))
		);*/

		$router->addRoute(
			'api-main',
			new Zend_Controller_Router_Route(
				'api/:controller/:action/*',
				array(
					'module' => 'api',
					'controller' => 'index',
					'action' => 'index'
				)
			)
		);

		$router->addRoute(
			'api-cams',
			new Zend_Controller_Router_Route(
				'cams-api/:controller/:action/*',
				array(
					'module' => 'cams-api',
					'controller' => 'index',
					'action' => 'index'
				)
			)
		);
		
		$router->addRoute(
			'index-controller',
			new Zend_Controller_Router_Route(
				'/index/:action',
				array(
					'module' => 'default',
					'controller' => 'index',
					'action' => 'index'
				)
			)
		);

		$router->addRoute(
			'girl-of-month',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?girl-of-month(/history)?(/page_([0-9]+))?',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'gotm'
				),
				array(
					2 => 'lang_id',
					3 => 'history',
					5 => 'page'
				)
			)
		);



		$router->addRoute(
			'voting-widget',
			new Zend_Controller_Router_Route_Regex(
				//'^(([a-z]{2})/)?escort/\-([0-9]+)/(.+?)/vote',
				'^(([a-z]{2})/)?escort/(.+?)\-([0-9]+)/vote',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'vote'
				),
				array(
					2 => 'lang_id',
					3 => 'showname',
					4 => 'escort_id'
				)
			)
		);


         /* update Grigor*/

        /* comment Subpage*/

		$router->addRoute(
			'subpagecomments',
			new Zend_Controller_Router_Route(
				':lang_id/comments',
				array(
					'module' => 'default',
					'controller' => 'comments',
					'action' => 'index'
				)
			),
			array(
				'lang_id' => '[a-z]{2}'
			)
		);

		$router->addRoute(
			'subpagecomments-def',
			new Zend_Controller_Router_Route(
				'comments',
				array(
					'module' => 'default',
					'controller' => 'comments',
					'action' => 'index'
				)
			)
		);

		$router->addRoute(
			'subpagecomments-escort',
			new Zend_Controller_Router_Route_Regex(
				':lang_id/escortcomments',
				array(
					'module' => 'default',
					'controller' => 'comments',
					'action' => 'get-escort-comments'
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'subpagecomments-escort-def',
			new Zend_Controller_Router_Route_Regex(
				'escortcomments',
				array(
					'module' => 'default',
					'controller' => 'comments',
					'action' => 'get-escort-comments'
				)
			)
		);
        /* Comments subpage*/


		$router->addRoute(
			'bubbles',
			new Zend_Controller_Router_Route(
				'ajax-bubble',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'ajax-bubble'
				)
			)
		);

		$router->addRoute(
			'confirm-deletion-lang',
			new Zend_Controller_Router_Route_Regex(
				'([a-z]{2})/private-v2/confirm-deletion/([a-f0-9]{10})',
				array(
					'module' => 'default',
					'controller' => 'private-v2',
					'action' => 'confirm-deletion'
				),
				array(
					1 => 'lang_id',
					2 => 'hash'
				)
			)
		);

		$router->addRoute(
			'confirm-deletion',
			new Zend_Controller_Router_Route_Regex(
				'private-v2/confirm-deletion/([a-f0-9]{10})',
				array(
					'module' => 'default',
					'controller' => 'private-v2',
					'action' => 'confirm-deletion'
				),
				array(
					1 => 'hash'
				)
			)
		);

		$router->addRoute(
			'get-gallery-photos',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?escorts/gallery-photos',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'gallery-photos'
				),
				array(
					2 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'ajax-tell-friend',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?escorts/ajax-tell-friend',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'ajax-tell-friend'
				),
				array(
					2 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'video-chat-popup',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?escorts/video-chat-popup',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'video-chat-popup'
				),
				array(
					2 => 'lang_id'
				)
			)
		);
		
		$router->addRoute(
			'video-chat-ecardon-popup',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?escorts/ecardon-popup',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'ecardon-popup'
				),
				array(
					2 => 'lang_id'
				)
			)
		);
		
		$router->addRoute(
			'video-chat-success-page',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?escorts/video-chat-success',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'video-chat-success'
				),
				array(
					2 => 'lang_id'
				)
			)
		);

		/*$router->addRoute(
			'video-chat-twispay-success-page',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?escorts/video-chat-twispay-success',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'video-chat-twispay-success'
				),
				array(
					2 => 'lang_id'
				)
			)
		);*/
		
		$router->addRoute(
			'ajax-susp-photo',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?escorts/ajax-susp-photo',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'ajax-susp-photo'
				),
				array(
					2 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'ajax-report-problem',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?escorts/ajax-report-problem',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'ajax-report-problem'
				),
				array(
					2 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'online',
			new Zend_Controller_Router_Route(
				'ajax-online',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'ajax-online'
				)
			)
		);

		$router->addRoute(
			'get-filter',
			new Zend_Controller_Router_Route_Regex(
				'(([a-z]{2})/)?escorts/get-filter',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'get-filter'
				),
				array(
					2 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'get-filter-v2',
			new Zend_Controller_Router_Route_Regex(
				'(([a-z]{2})/)?escorts/get-filter-v2',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'get-filter-v2'
				),
				array(
					2 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'ajax-get-personal-site',
			new Zend_Controller_Router_Route_Regex(
				'(([a-z]{2})/)?escorts/ajax-get-personal-site',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'ajax-get-personal-site'
				),
				array(
					2 => 'lang_id'
				)
			)
		);

		/* REDIRECT */
		$router->addRoute(
			'lang-profile-redirect',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/(escort|accompagnatrici)/(.+)-(.+)',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'lang-escort'
				),
				array(
					1 => 'lang'
				)
			)
		);

		// <editor-fold defaultstate="collapsed" desc="Gateway Callbacks">
		$router->addRoute('callbacks', new Zend_Controller_Router_Route(
			'callbacks/:action',
			array(
				'module' => 'default',
				'controller' => 'callbacks',
				'action' => 'index'
			)
		));
		// </editor-fold>


		// <editor-fold defaultstate="collapsed" desc="Mobile Version">
		$router->addRoute(
			'mobile',
			new Zend_Controller_Router_Route(
				'mobile/:controller/:action/*',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'index'
				)
			)
		);

		$router->addRoute(
			'def-mobile',
			new Zend_Controller_Router_Route(
				':lang_id/mobile/:controller/:action/*',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'index',
					'lang_id' => ''
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);
//
		$router->addRoute(
			'mobile-allcities',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/allcities',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'index',
					'all' => 1
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'mobile-robots',
			new Zend_Controller_Router_Route(
				'mobile/robots.txt',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'robots'
				)
			)
		);

		$router->addRoute(
			'mobile-show-video',
			new Zend_Controller_Router_Route_Regex(
				'(([a-z]{2})/)?mobile/show-video',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'show-video'
				),
				array(
					2 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'mobile-allcities-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/allcities',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'index',
					'all' => 1
				)
			)
		);

		$router->addRoute(
			'mobile-escorts',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/escorts(/.+)?',
				array(
					'module' => 'mobilev2',
					'controller' => 'escorts',
					'action' => 'index'
				),
				array(
					1 => 'lang_id',
					2 => 'req'
				)
			)
		);
        $router->addRoute(
            'mobile-skype',
            new Zend_Controller_Router_Route_Regex(
                '^([a-z]{2})/skype(/.+)?',
                array(
                    'module' => 'mobilev2',
                    'controller' => 'skype',
                    'action' => 'index'
                ),
                array(
                    1 => 'lang_id',
                    2 => 'req'
                )
            )
        );

		$router->addRoute(
			'mobile-escorts-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/escorts(/.+)?',
				array(
					'module' => 'mobilev2',
					'controller' => 'escorts',
					'action' => 'index'
				),
				array(
					1 => 'req'
				)
			)
		);

		$router->addRoute(
            'mobile-gotm',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?mobile/girl-of-month',
                array(
                    'module' => 'mobilev2',
                    'controller' => 'index',
                    'action' => 'girl-of-month'
                ),
                array(
                    2 => 'lang_id'
                )
            )
        );
		/** Instant book route */
		
		$router->addRoute(
			'mobile-escorts-instant-book',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/escorts/instant-book',
				array(
					'module' => 'mobilev2',
					'controller' => 'escorts',
					'action' => 'instant-book'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'mobile-escorts-instant-book-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/escorts/instant-book',
				array(
					'module' => 'mobilev2',
					'controller' => 'escorts',
					'action' => 'instant-book'
				),
				array()
			)
		);


		$router->addRoute(
			'mob-escorts-personal-site',
			new Zend_Controller_Router_Route_Regex(
				'(([a-z]{2})/)?mobile/escorts/ajax-get-personal-site',
				array(
					'module' => 'mobilev2',
					'controller' => 'escorts',
					'action' => 'ajax-get-personal-site'
				),
				array(
					2 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'mob-escort-cams',
			new Zend_Controller_Router_Route_Regex(
				'(([a-z]{2})/)?mobile/ef-cams(/.+)?',
				array(
					'module' => 'mobilev2',
					'controller' => 'cams',
					'action' => 'index'
				),
				array(
					3 => 'req'
				)
			)
		);
		
		$router->addRoute(
			'mob-escort-cams-goto-book',
			new Zend_Controller_Router_Route_Regex(
				'(([a-z]{2})/)?mobile/ef-cams/go-to-book',
				array(
					'module' => 'mobilev2',
					'controller' => 'cams',
					'action' => 'go-to-book'
				),
				array(
					1 => 'lang_id'
				)
			)
		);
		
		$router->addRoute(
			'mob-escort-cams-login',
			new Zend_Controller_Router_Route_Regex(
				'(([a-z]{2})/)?mobile/ef-cams/login',
				array(
					'module' => 'mobilev2',
					'controller' => 'cams',
					'action' => 'login'
				),
				array(
					1 => 'lang_id'
				)
			)
		);
		
		$router->addRoute(
			'mob-escort-cams-signup',
			new Zend_Controller_Router_Route_Regex(
				'(([a-z]{2})/)?mobile/ef-cams/signup',
				array(
					'module' => 'mobilev2',
					'controller' => 'cams',
					'action' => 'signup'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'mob-escort-cams-buy-tokens',
			new Zend_Controller_Router_Route_Regex(
				'(([a-z]{2})/)?mobile/ef-cams/buy-tokens',
				array(
					'module' => 'mobilev2',
					'controller' => 'cams',
					'action' => 'buy-tokens'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'mob-cams-ecardon-popup',
			new Zend_Controller_Router_Route_Regex(
				'(([a-z]{2})/)?mobile/ef-cams/ecardon-popup',
				array(
					'module' => 'mobilev2',
					'controller' => 'cams',
					'action' => 'ecardon-popup'
				),
				array(
					1 => 'lang_id'
				)
			)
		);
		
		$router->addRoute(
			'mob-ecardon-token-success',
			new Zend_Controller_Router_Route_Regex(
				'(([a-z]{2})/)?mobile/ef-cams/ecardon-response',
				array(
					'module' => 'mobilev2',
					'controller' => 'cams',
					'action' => 'ecardon-response'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'mob-twispay-token-success',
			new Zend_Controller_Router_Route_Regex(
				'(([a-z]{2})/)?mobile/ef-cams/twispay-response',
				array(
					'module' => 'mobilev2',
					'controller' => 'cams',
					'action' => 'twispay-response'
				),
				array(
					1 => 'lang_id'
				)
			)
		);
		
		$router->addRoute(
			'videos-mobile',
			new Zend_Controller_Router_Route(
				':lang_id/mobile/video-list',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'video-list'
				)
			)
		);

		$router->addRoute(
			'videos-mobile-def',
			new Zend_Controller_Router_Route(
				'mobile/video-list',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'video-list'
				)
			)
		);

		$router->addRoute(
			'videos-mobile-city',
			new Zend_Controller_Router_Route(
				':lang_id/mobile/video-list/:city_id',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'video-list'
				)
			),
			array(
				'city_id' => '[0-9]+'
			)
		);

		$router->addRoute(
			'videos-mobile-city-def',
			new Zend_Controller_Router_Route(
				'mobile/video-list/:city_id',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'video-list'
				)
			),
			array(
				'city_id' => '[0-9]+'
			)
		);


		$router->addRoute(
			'mobile-escort-favorites',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/favorites',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'favorites'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'mobile-ajax-favorites-index',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/ajax-favorites',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'ajax-favorites'
				)
			)
		);


		$router->addRoute(
			'ajax-instant-book-mobile-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/escorts/ajax-instant-book',
				array(
					'module' => 'mobilev2',
					'controller' => 'escorts',
					'action' => 'ajax-instant-book',
				),
				array(
					2 => 'lang_id',
				)
			)
		);

		$router->addRoute(
			'ajax-instant-book-mobile',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/escorts/ajax-instant-book',
				array(
					'module' => 'mobilev2',
					'controller' => 'escorts',
					'action' => 'ajax-instant-book',
				),
				array(
					2 => 'lang_id',
				)
			)
		);

		$router->addRoute(
			'mobile-add-to-top',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/ajax-favorites-add-to-top',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'ajax-favorites-add-to-top'
				),
				array(
					1 => 'lang_id'
				)
			)
		);



		$router->addRoute(
			'mobile-add-to-top-index',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/ajax-favorites-add-to-top',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'ajax-favorites-add-to-top'
				)
			)
		);

		$router->addRoute(
			'mobile-remove-from-top',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/ajax-favorites-remove-from-top',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'ajax-favorites-remove-from-top'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'mobile-remove-from-top-index',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/ajax-favorites-remove-from-top',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'ajax-favorites-remove-from-top'
				)
			)
		);

        ##
		$router->addRoute(
			'mobile-ajax-favorites-down',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/ajax-favorites-down',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'ajax-favorites-down'
				)
			)
		);

		$router->addRoute(
			'mobile-ajax-favorites-up',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/ajax-favorites-up',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'ajax-favorites-up'
				)
			)
		);
        ##

		$router->addRoute(
			'mobile-add-fav-comment',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/add-fav-comment',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'add-fav-comment'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'mobile-add-fav-comment-index',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/add-fav-comment',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'add-fav-comment'
				)
			)
		);

        //
		$router->addRoute(
			'mobile-top10-list',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/top10-list',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'top10-list'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'mobile-top10-list-index',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/top10-list',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'top10-list'
				)
			)
		);
		$router->addRoute(
			'mobile-ajax-top10',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/ajax-top10',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'ajax-top10'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'mobile-ajax-top10-index',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/ajax-top10',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'ajax-top10'
				)
			)
		);
        //

		$router->addRoute(
			'mobile-ajax-favorites',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/ajax-favorites',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'ajax-favorites'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-favorites-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/favorites',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'favorites'
				)
			)
		);

		$router->addRoute(
			'mobile-private-activate-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/(.+)?/private/activate/(.+)?/([a-f0-9]{32})',
				array(
					'module' => 'mobilev2',
					'controller' => 'private',
					'action' => 'activate'
				),
				array(
					1 => 'user_type',
					2 => 'email',
					3 => 'hash'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-search',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/search',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'search'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-search-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/search',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'search'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-gps-location',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/gps-location',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'gps-location'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-gps-location-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/gps-location',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'gps-location'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-profile',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/escort/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobilev2',
					'controller' => 'escort-profile',
					'action' => 'escort'
				),
				array(
					1 => 'lang_id',
					2 => 'escortName',
					3 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'mob-escorts-filter-global-redirect',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/escorts/(en)(/(boys|trans|tours|upcomingtours|nuove|agence|independantes))?(/(city|region)_it_(.+?))?(/(incall|outcall))?(/([0-9]+))?$',
				array(
					'module' => 'mobile',
					'controller' => 'redirect',
					'action' => 'geo'
				),
				array(
					1 => 'lang',
					3 => 'type',
					5 => 'geo',
					6 => 'geoval',
					8 => 'filter',
					10 => 'page'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-profile-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/escort/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobilev2',
					'controller' => 'escort-profile',
					'action' => 'escort'
				),
				array(
					1 => 'escortName',
					2 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'mob-escort-profile-redirect2-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/accompagnatrici/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobilev2',
					'controller' => 'escort-profile',
					'action' => 'escort'
				),
				array(
					1 => 'escortName',
					2 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'mob-escort-profile-redirect2',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/accompagnatrici/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobilev2',
					'controller' => 'escort-profile',
					'action' => 'escort'
				),
				array(
					1 => 'lang_id',
					2 => 'escortName',
					3 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'mobile-ajax-alerts',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?mobile/escorts/ajax-alerts',
				array(
					'module' => 'mobilev2',
					//'controller' => 'escort-profile',
					'controller' => 'escorts',
					'action' => 'ajax-alerts'
				),
				array(
					2 => 'lang_id',
				)
			)
		);

		$router->addRoute(
			'mobile-video-chat-popup',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?mobile/escorts/video-chat-popup',
				array(
					'module' => 'mobilev2',
					'controller' => 'escorts',
					'action' => 'video-chat-popup'
				),
				array(
					2 => 'lang_id',
				)
			)
		);
		
		$router->addRoute(
			'mobile-video-chat-ecardon-popup',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?mobile/escorts/ecardon-popup',
				array(
					'module' => 'mobilev2',
					'controller' => 'escorts',
					'action' => 'ecardon-popup'
				),
				array(
					2 => 'lang_id'
				)
			)
		);
		
		$router->addRoute(
			'mobile-video-chat-success-page',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?mobile/escorts/video-chat-success',
				array(
					'module' => 'mobilev2',
					'controller' => 'escorts',
					'action' => 'video-chat-success'
				),
				array(
					2 => 'lang_id'
				)
			)
		);
		
        $router->addRoute(
            'mob-alertsSave-def',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?mobile/escorts/ajax-alerts-save',
                array(
                    'module' => 'mobilev2',
                    'controller' => 'escorts',
                    'action' => 'ajax-alerts-save'
                )
            )
        );
		
		$router->addRoute(
			'mobile-comments-bubble',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/escorts/ajax-bubble',
				array(
					'module' => 'mobilev2',
					'controller' => 'escorts',
					'action' => 'ajax-bubble'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'mobile-comments-bubble-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/escorts/ajax-bubble',
				array(
					'module' => 'mobilev2',
					'controller' => 'escorts',
					'action' => 'ajax-bubble'
				),
				array()
			)
		);

		$router->addRoute(
			'mobile-escort-services',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/escort/services/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobilev2',
					'controller' => 'escort-profile',
					'action' => 'services'
				),
				array(
					1 => 'lang_id',
					2 => 'escortName',
					3 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-services-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/escort/services/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobilev2',
					'controller' => 'escort-profile',
					'action' => 'services'
				),
				array(
					1 => 'escortName',
					2 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-rates',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/escort/rates/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobilev2',
					'controller' => 'escort-profile',
					'action' => 'rates'
				),
				array(
					1 => 'lang_id',
					2 => 'escortName',
					3 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-rates-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/escort/rates/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobilev2',
					'controller' => 'escort-profile',
					'action' => 'rates'
				),
				array(
					1 => 'escortName',
					2 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-contacts',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/escort/contacts/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobilev2',
					'controller' => 'profile',
					'action' => 'contacts'
				),
				array(
					1 => 'lang_id',
					2 => 'escortName',
					3 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-contacts-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/escort/contacts/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobilev2',
					'controller' => 'escort-profile',
					'action' => 'contacts'
				),
				array(
					1 => 'escortName',
					2 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-tours',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/escort/tours/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobilev2',
					'controller' => 'escort-profile',
					'action' => 'tours'
				),
				array(
					1 => 'lang_id',
					2 => 'escortName',
					3 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-tours-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/escort/tours/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobilev2',
					'controller' => 'escort-profile',
					'action' => 'tours'
				),
				array(
					1 => 'escortName',
					2 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'mobile-static-pages',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/page/(.+)?',
				array(
					'module' => 'mobilev2',
					'controller' => 'page',
					'action' => 'show'
				),
				array(
					1 => 'lang_id',
					2 => 'page_slug'
				)
			)
		);

		$router->addRoute(
			'mobile-static-pages-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/page/(.+)?',
				array(
					'module' => 'mobilev2',
					'controller' => 'page',
					'action' => 'show'
				),
				array(
					1 => 'page_slug'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-photo',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/escort/photo/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobilev2',
					'controller' => 'escort-profile',
					'action' => 'photo'
				),
				array(
					1 => 'lang_id',
					2 => 'escortName',
					3 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-photo-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/escort/photo/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobilev2',
					'controller' => 'escort-profile',
					'action' => 'photo'
				),
				array(
					1 => 'escortName',
					2 => 'escort_id'
				)
			)
		);

		/* mobile-photos */
		$router->addRoute(
			'mobile-escort-photos',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/escort/photos/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobilev2',
					'controller' => 'escort-profile',
					'action' => 'profile-photos'
				),
				array(
					1 => 'lang_id',
					2 => 'escortName',
					3 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-photos-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/escort/photos/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobilev2',
					'controller' => 'escort-profile',
					'action' => 'profile-photos'
				),
				array(
					1 => 'escortName',
					2 => 'escort_id'
				)
			)
		);
		/* escort photos end */

		$router->addRoute(
			'mobile-add-to-favorites',
			new Zend_Controller_Router_Route_Regex(
				'([a-z]{2})/mobile/add-to-favorites(.+)?',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'add-to-favorites'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'mobile-add-to-favorites-def',
			new Zend_Controller_Router_Route_Regex(
				'mobile/add-to-favorites(.+)?',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'add-to-favorites'
				)
			)
		);

		$router->addRoute(
			'mobile-remove-from-favorites',
			new Zend_Controller_Router_Route_Regex(
				'([a-z]{2})/mobile/remove-from-favorites(.+)?',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'remove-from-favorites'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'mobile-remove-from-favorites-def',
			new Zend_Controller_Router_Route_Regex(
				'mobile/remove-from-favorites(.+)?',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'remove-from-favorites'
				)
			)
		);

		$router->addRoute(
			'mobile-feedback',
			new Zend_Controller_Router_Route(
				':lang_id/mobile/feedback',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'feedback'
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'mobile-feedback-def',
			new Zend_Controller_Router_Route(
				'mobile/feedback',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'feedback'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-evaluations',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?mobile/evaluations/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobilev2',
					'controller' => 'escort-profile',
					'action' => 'reviews'
				),
				array(
					2 => 'lang_id',
					3 => 'escortName',
					4 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-evaluations-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/evaluations/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobilev2',
					'controller' => 'escort-profile',
					'action' => 'reviews'
				),
				array(
					1 => 'escortName',
					2 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'mobile-evaluations',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?mobile/evaluations',
				array(
					'module' => 'mobilev2',
					'controller' => 'escorts',
					'action' => 'reviews'
				),
				array(
					2 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'mobile-evaluations-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/evaluations',
				array(
					'module' => 'mobilev2',
					'controller' => 'escorts',
					'action' => 'reviews'
				),
				array()
			)
		);

		$router->addRoute(
			'mobile-ajax-escort-evaluations-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/reviews-ajax/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobilev2',
					'controller' => 'escort-profile',
					'action' => 'reviews-ajax'
				),
				array(
					1 => 'escortName',
					2 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'mobile-ajax-escort-evaluations',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?mobile/reviews-ajax/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobilev2',
					'controller' => 'escort-profile',
					'action' => 'reviews-ajax'
				),
				array(
					2 => 'lang_id',
					3 => 'escortName',
					4 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'mobile-contact-def',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?mobile/contact',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'contact'
				),
				array(
					2 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'mobile-contact',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/(([a-z]{2})/)?contact',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'contact'
				),
				array(
					2 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'mobile-captcha',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?mobile/captcha',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'captcha'
				),
				array(
					2 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'mobile-captcha-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/captcha',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'captcha'
				)
			)
		);

		$router->addRoute(
			'mobile-geotargeted',
			new Zend_Controller_Router_Route(
				'mobile/geotargeted',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'geotargeted'
				)
			)
		);
		
        /* $router->addRoute(
            'mob-captcha',
            new Zend_Controller_Router_Route('mobile/:lang_id/captcha',
                array(
                    'module' => 'mobilev2',
                    'controller' => 'index',
                    'action' => 'captcha'
                ))
        );

        $router->addRoute(
            'mob-captcha-1',
            new Zend_Controller_Router_Route('^mobile/:lang_id/captcha',
                array(
                    'module' => 'mobilev2',
                    'controller' => 'index',
                    'action' => 'captcha'
                ), array(
                    'lang_id' => '[a-z]{2}'
                ))
        );

        $router->addRoute(
            'mob-captcha-def',
            new Zend_Controller_Router_Route('^mobile/captcha',
                array(
                    'module' => 'mobilev2',
                    'controller' => 'index',
                    'action' => 'captcha'
                ))
        ); */

		$router->addRoute(
			'mobile-privateV2',
			new Zend_Controller_Router_Route(
				'mobile/private-v2/:action/*',
				array(
					'module' => 'mobilev2',
					'controller' => 'private-v2',
					'action' => 'index'
				)
			)
		);

		$router->addRoute(
			'mobile-privateProfile',
			new Zend_Controller_Router_Route(
				'mobile/private-v2/profile/:action/*',
				array(
					'module' => 'mobilev2',
					'controller' => 'profile',
					'action' => 'index'
				)
			)
		);

		$router->addRoute(
			'mobile-privateV2Lng',
			new Zend_Controller_Router_Route(
				'mobile/:lang_id/private-v2/:action/*',
				array(
					'module' => 'mobilev2',
					'controller' => 'private-v2',
					'action' => 'index',
					'lang_id' => ''
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'mobile-privateProfileLng',
			new Zend_Controller_Router_Route(
				':lang_id/mobile/private-v2/profile/:action/*',
				array(
					'module' => 'mobilev2',
					'controller' => 'profile',
					'action' => 'index',
					'lang_id' => ''
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'mobile-privateMessaging',
			new Zend_Controller_Router_Route(
				':lang_id/mobile/private-messaging/:action/*',
				array(
					'module' => 'mobilev2',
					'controller' => 'private-messaging',
					'action' => 'index'
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'mobile-private-verified',
			new Zend_Controller_Router_Route(
				':lang_id/mobile/private/verify/:action',
				array(
					'module' => 'mobilev2',
					'controller' => 'verify',
					'action' => 'index'
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'mobile-private-verified-def',
			new Zend_Controller_Router_Route(
				'mobile/private/verify/:action',
				array(
					'module' => 'mobilev2',
					'controller' => 'verify',
					'action' => 'index'
				)
			)
		);

		$router->addRoute(
			'mobile-external-link',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/go',
				array(
					'module' => 'mobilev2',
					'controller' => 'index',
					'action' => 'go'
				)
			)
		);

		$router->addRoute(
			'mob-photoVideo',
			new Zend_Controller_Router_Route(
				'mobile/photo-video/:action/*',
				array(
					'module' => 'mobilev2',
					'controller' => 'photo-video',
					'action' => 'index'
				)
			)
		);

		$router->addRoute(
			'mob-photoVideoLng',
			new Zend_Controller_Router_Route(
				':lang_id/mobile/photo-video/:action/*',
				array(
					'module' => 'mobilev2',
					'controller' => 'photo-video',
					'action' => 'index',
					'lang_id' => ''
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'mob-private-activate',
			new Zend_Controller_Router_Route_Regex(
				'([a-z]{2})/mobile/(.+)?/private/activate/(.+)?/([a-f0-9]{32})',
				array(
					'module' => 'mobilev2',
					'controller' => 'private',
					'action' => 'activate'
				),
				array(
					1 => 'lang_id',
					2 => 'user_type',
					3 => 'email',
					4 => 'hash'
				)
			)
		);

		$router->addRoute(
			'mob-private-activate-def',
			new Zend_Controller_Router_Route_Regex(
				'(.+)?/mobile/private/activate/(.+)?/([a-f0-9]{32})',
				array(
					'module' => 'mobilev2',
					'controller' => 'private',
					'action' => 'activate'
				),
				array(
					1 => 'user_type',
					2 => 'email',
					3 => 'hash'
				)
			)
		);

		$router->addRoute(
			'mob-private-change-pass',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/(([a-z]{2})/)?private/change-pass/([-_a-z0-9]+)/([a-f0-9]{32})',
				array(
					'module' => 'mobilev2',
					'controller' => 'private',
					'action' => 'change-pass',
					'lang_id' => null
				),
				array(
					2 => 'lang_id',
					3 => 'username',
					4 => 'hash'
				)
			)
		);

//		$router->addRoute(
//			'mob-photoVideoMain',
//			new Zend_Controller_Router_Route('mobile/gallery-main/:action/*',
//				array(
//						'module' => 'mobilev2',
//						'controller' => 'photo-video',
//						'action' => 'index'
//				))
//		);
//
//		$router->addRoute(
//			'mob-photoVideoLngMain',
//			new Zend_Controller_Router_Route(':lang_id/mobile/gallery-main/:action/*',
//				array(
//						'module' => 'mobilev2',
//						'controller' => 'photo-video',
//						'action' => 'index',
//						'lang_id' => ''
//				),
//				array(
//						'lang_id' => '[a-z]{2}'
//				))
//		);

		$router->addRoute(
			'mob-xmas-game-index',
			new Zend_Controller_Router_Route(
				'mobile/xmas/:action/*',
				array(
					'module' => 'mobilev2',
					'controller' => 'xmas',
					'action' => 'index'
				)
			)
		);

		$router->addRoute(
			'mob-support',
			new Zend_Controller_Router_Route(
				':lang_id/mobile/support/:action',
				array(
					'module' => 'mobilev2',
					'controller' => 'support',
					'action' => 'index'
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'mob-support-def',
			new Zend_Controller_Router_Route(
				'mobile/support/:action',
				array(
					'module' => 'mobilev2',
					'controller' => 'support',
					'action' => 'index'
				)
			)
		);

		$router->addRoute(
			'mob-skype',
			new Zend_Controller_Router_Route(
				'mobile/skype/:action',
				array(
					'module' => 'mobilev2',
					'controller' => 'skype',
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'mobile-private-membership',
			new Zend_Controller_Router_Route_Regex(
				'mobile/private/membership-type',
				array(
					'module' => 'mobilev2',
					'controller' => 'private',
					'action' => 'signup'
				),
				array(
					1 => 'type'
				)
			)
		);

		$router->addRoute(
			'mobile-private-signup-def',
			new Zend_Controller_Router_Route_Regex(
				'mobile/private/signup-(member|escort|agency)',
				array(
					'module' => 'mobilev2',
					'controller' => 'private',
					'action' => 'signup'
				),
				array(
					1 => 'type'
				)
			)
		);

		$router->addRoute(
			'mobile-private-signup',
			new Zend_Controller_Router_Route_Regex(
				'([a-z]{2})/mobile/private/signup-(member|escort|agency)',
				array(
					'module' => 'mobilev2',
					'controller' => 'private',
					'action' => 'signup'
				),
				array(
					1 => 'lang_id',
					2 => 'type'
				)
			)
		);



		$router->addRoute(
			'mobile-private-signup-def',
			new Zend_Controller_Router_Route_Regex(
				'mobile/private/signup-(member|escort|agency)',
				array(
					'module' => 'mobilev2',
					'controller' => 'private',
					'action' => 'signup'
				),
				array(
					1 => 'type'
				)
			)
		);

		$router->addRoute(
			'mobile-private-cam-signin',
			new Zend_Controller_Router_Route_Regex(
				'mobile/private/cams-signin',
				array(
					'module' => 'mobilev2',
					'controller' => 'private',
					'action' => 'signin',
					'is_cams' => true
				)
			)
		);
		
		/* --> Online Billing V2 <-- */

		$router->addRoute(
			'mobile-online-billing-v2-def',
			new Zend_Controller_Router_Route(
				'/mobile/online-billing-v2/:action',
				array(
					'module' => 'mobilev2',
					'controller' => 'online-billing-v2',
					'action' => 'index'
				)
			)
		);


		$router->addRoute(
			'mobile-online-billing-v2',
			new Zend_Controller_Router_Route(
				':lang_id/mobile/online-billing-v2/:action',
				array(
					'module' => 'mobilev2',
					'controller' => 'online-billing-v2',
					'action' => 'index'
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		/* --> Profile boost <-- */
		$router->addRoute(
			'mob-ob-profile-boost',
			new Zend_Controller_Router_Route(
				':lang_id/mobile/profile-boost/:action',
				array(
					'module' => 'mobilev2',
					'controller' => 'profile-boost',
					'action' => 'index'
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'mob-ob-profile-boost-def',
			new Zend_Controller_Router_Route(
				'mobile/profile-boost/:action',
				array(
					'module' => 'mobilev2',
					'controller' => 'profile-boost',
					'action' => 'index'
				)
			)
		);

		$router->addRoute(
			'mob-xmas-game-index',
			new Zend_Controller_Router_Route(
				'mobile/xmas/:action/*',
				array(
					'module' => 'mobile',
					'controller' => 'xmas',
					'action' => 'index'
				)
			)
		);

		$router->addRoute(
			'mob-escorts-search',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/search',
				array(
					'module' => 'mobilev2',
					'controller' => 'escorts',
					'action' => 'search'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'mob-escorts-search-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/search',
				array(
					'module' => 'mobilev2',
					'controller' => 'escorts',
					'action' => 'search'
				)
			)
		);



		$router->addRoute(
			'mobile-search-filter-data',
			new Zend_Controller_Router_Route(
				'/mobile/escorts/search-filter-data',
				array(
					'module' => 'mobilev2',
					'controller' => 'escorts',
					'action' => 'search-filter-data'
				)
			)
		);

        $router->addRoute(
            'mobile-get-cities-geo-data',
            new Zend_Controller_Router_Route(
                'mobile/escorts/get-cities-geo-data',
                array(
                    'module' => 'mobilev2',
                    'controller' => 'escorts',
                    'action' => 'get-cities-geo-data'
                )
            )
        );
		/* <-- Profile boost --> */


        /* ---- Profile bump ---- */
        $router->addRoute(
            'mob-ob-profile-bump',
            new Zend_Controller_Router_Route(
                ':lang_id/mobile/profile-bumps/:action',
                array(
                    'module' => 'mobilev2',
                    'controller' => 'profile-bumps',
                    'action' => 'index'
                ),
                array(
                    'lang_id' => '[a-z]{2}'
                )
            )
        );

        $router->addRoute(
            'mob-ob-profile-bump-def',
            new Zend_Controller_Router_Route(
                'mobile/profile-bumps/:action',
                array(
                    'module' => 'mobilev2',
                    'controller' => 'profile-bumps',
                    'action' => 'index'
                )
            )
        );
        /* ---- Profile bump ---- */

		// </editor-fold>

        /* CLASSIFIED ADS */
		$router->addRoute(
			'classified-ads',
			new Zend_Controller_Router_Route(
				':lang_id/classified-ads/:action',
				array(
					'module' => 'default',
					'controller' => 'classified-ads',
					'action' => 'index'
				)
			),
			array(
				'lang_id' => '[a-z]{2}'
			)
		);

		$router->addRoute(
			'classified-ads-def',
			new Zend_Controller_Router_Route(
				'classified-ads/:action',
				array(
					'module' => 'default',
					'controller' => 'classified-ads',
					'action' => 'index'
				)
			),
			array()
		);

		$router->addRoute(
			'feedback-ads',
			new Zend_Controller_Router_Route(
				':lang_id/feedback-ads',
				array(
					'module' => 'default',
					'controller' => 'index',
					'action' => 'feedback-ads'
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'feedback-ads-def',
			new Zend_Controller_Router_Route(
				'feedback-ads',
				array(
					'module' => 'default',
					'controller' => 'index',
					'action' => 'feedback-ads'
				)
			)
		);
        /* CLASSIFIED ADS */

		/* Latest ACtions */
		$router->addRoute(
			'latest-actions',
			new Zend_Controller_Router_Route(
				':lang_id/latest-actions/:action',
				array(
					'module' => 'default',
					'controller' => 'latest-actions',
					'action' => 'index'
				)
			),
			array(
				'lang_id' => '[a-z]{2}'
			)
		);

		$router->addRoute(
			'latest-actions-def',
			new Zend_Controller_Router_Route(
				'latest-actions/:action',
				array(
					'module' => 'default',
					'controller' => 'latest-actions',
					'action' => 'index'
				)
			),
			array()
		);
		/* Latest ACtions */

		/*PHOTO FEED*/
		$router->addRoute(
			'photo-feed',
			new Zend_Controller_Router_Route_Regex(
				'photo-feed',
				array(
					'module' => 'default',
					'controller' => 'photo-feed',
					'action' => 'index',
				),
				array()
			)
		);

		$router->addRoute(
			'photo-feed-ajax-list',
			new Zend_Controller_Router_Route_Regex(
				'photo-feed/ajax-list',
				array(
					'module' => 'default',
					'controller' => 'photo-feed',
					'action' => 'ajax-list',
				),
				array()
			)
		);

		$router->addRoute(
			'photo-feed-ajax-header',
			new Zend_Controller_Router_Route_Regex(
				'photo-feed/ajax-header',
				array(
					'module' => 'default',
					'controller' => 'photo-feed',
					'action' => 'ajax-header',
				),
				array()
			)
		);

		$router->addRoute(
			'photo-feed-ajax-voting-box',
			new Zend_Controller_Router_Route_Regex(
				'photo-feed/ajax-voting-box/([0-9]+)/([0-9]+)',
				array(
					'module' => 'default',
					'controller' => 'photo-feed',
					'action' => 'ajax-voting-box',
				),
				array(
					1 => 'escort_id',
					2 => 'photo_id',
				)
			)
		);
		$router->addRoute(
			'photo-feed-ajax-more-photos',
			new Zend_Controller_Router_Route_Regex(
				'photo-feed/ajax-get-more-photos/([0-9]+)/([0-9]+)/([0-9]+)',
				array(
					'module' => 'default',
					'controller' => 'photo-feed',
					'action' => 'ajax-get-more-photos',
				),
				array(
					1 => 'escort_id',
					2 => 'exclude_photo_id',
					3 => 'page',
				)
			)
		);

		$router->addRoute(
			'photo-feed-ajax-vote',
			new Zend_Controller_Router_Route_Regex(
				'photo-feed/ajax-vote/([0-9]+)/([0-9]+)/([0-9]+)',
				array(
					'module' => 'default',
					'controller' => 'photo-feed',
					'action' => 'ajax-vote',
				),
				array(
					1 => 'escort_id',
					2 => 'photo_id',
					3 => 'rate',
				)
			)
		);
		/*PHOTO FEED*/

		$router->addRoute(
			'online-chat-city-redirect',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2}/)?escorts/chat-online/city_(.+)',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'online-chat-city'
				),
				array(
					2 => 'city'
				)
			)
		);

		$router->addRoute(
			'ajax-gotm',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?escorts/ajax-gotm',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'ajax-gotm'
				),
				array(
					2 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'popunder',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?popunder',
				array(
					'module' => 'default',
					'controller' => 'index',
					'action' => 'popunder'
				),
				array(
					2 => 'lang_id',
				)
			)
		);

		$router->addRoute(
			'follow',
			new Zend_Controller_Router_Route(
				'follow/:action/*',
				array(
					'module' => 'default',
					'controller' => 'follow',
					'action' => 'index'
				)
			)
		);
		
		$router->addRoute(
			'test-rabbit',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?rabbit-test',
				array(
					'module' => 'default',
					'controller' => 'index',
					'action' => 'rabbit'
				)
			)
		);

        $router->addRoute(
            'internal-separated-payments-desktop',
            new Zend_Controller_Router_Route(
                'payment/:action',
                array(
                    'module' => 'default',
                    'controller' => 'payment',
                    'action' => 'index',
                )
            )
        );

        $router->addRoute(
            'internal-separated-payments-desktop-alternatives',
            new Zend_Controller_Router_Route_Regex(
                'payment-(alt1)', // Put here something like payment-(alt1|alt2|alt3)
                array(
                    'module' => 'default',
                    'controller' => 'payment',
                    'action' => 'index',
                ),
                array(
                    1 => 'paymentMethodKey',
                )
            )
        );

        $router->addRoute(
            'internal-separated-payments-mobile',
            new Zend_Controller_Router_Route(
                'mobile/payment/:action',
                array(
                    'module' => 'mobilev2',
                    'controller' => 'payment',
                    'action' => 'index',
                )
            )
        );

        $router->addRoute(
            'mobile-internal-separated-payments-desktop-alternatives',
            new Zend_Controller_Router_Route_Regex(
                'mobile/payment-(alt1)',
                array(
                    'module' => 'mobilev2',
                    'controller' => 'payment',
                    'action' => 'index',
                    'all' => 1
                ),
                array(
                    1 => 'paymentMethodKey'
                )
            )
        );
		
		
		// escortforumcams 
       	if(IS_DEBUG){
			$cams_domain = 'www.escortforumcams.com.test';
			$cams_mob_domain = 'm.escortforumcams.com.test';
		}
		else{
			$cams_domain = 'www.escortforumcams.com';
			$cams_mob_domain = 'm.escortforumcams.com';
		}
		$cams_plain_routes = array();
       	
        $cams_host_route = new Zend_Controller_Router_Route_Hostname($cams_domain,
            array(
                'module' => 'default',
                'controller' => 'cams',
                'action' => 'redirect',
            )
		);
		
		$cams_plain_routes['cams-domain-redirect'] = new Zend_Controller_Router_Route_Regex(
            '(([a-z]{2})/)?(.+)?',
            array('action' => 'redirect'),
            array(
				3 => 'req'
			)
        );
		
		/*$cams_plain_routes['cams-domain-main'] = new Zend_Controller_Router_Route_Regex(
            '(([a-z]{2})/)?(.+)?',
            array('action' => 'index'),
            array(
				3 => 'req'
			)
        );
		
		$cams_plain_routes['cams-domain-auth'] = new Zend_Controller_Router_Route_Regex(
            '(([a-z]{2})/)?cam-([^/]+)(/.+)?',
            array('action' => 'index'),
            array(
				3 => 'sid',
				4 => 'req'
			)
        );
		
		$cams_plain_routes['cams-domain-escort-login'] = new Zend_Controller_Router_Route_Regex(
            '(([a-z]{2})/)?ef-cams/login',
            array('action' => 'login'),
            array(1 => 'lang_id')
        );
		
		$cams_plain_routes['cams-domain-escort-signup'] = new Zend_Controller_Router_Route_Regex(
            '(([a-z]{2})/)?ef-cams/signup',
            array('action' => 'signup'),
            array(1 => 'lang_id')
        );
		
		$cams_plain_routes['cams-domain-buy-tokens'] = new Zend_Controller_Router_Route_Regex(
            '(([a-z]{2})/)?ef-cams/buy-tokens',
            array('action' => 'buy-tokens'),
            array(1 => 'lang_id')
        );
				
		$cams_plain_routes['cams-domain-eca-popup'] = new Zend_Controller_Router_Route_Regex(
            '(([a-z]{2})/)?ef-cams/eca-popup',
            array('action' => 'eca-popup'),
            array(1 => 'lang_id')
        );
						
		$cams_plain_routes['cams-domain-ecardon-popup'] = new Zend_Controller_Router_Route_Regex(
            '(([a-z]{2})/)?ef-cams/ecardon-popup',
            array('action' => 'ecardon-popup'),
            array(1 => 'lang_id')
        );
		
		$cams_plain_routes['cams-domain-ecardon-response'] = new Zend_Controller_Router_Route_Regex(
            '(([a-z]{2})/)?ef-cams/ecardon-response',
            array('action' => 'ecardon-response'),
            array(1 => 'lang_id')
        );
		
		$cams_plain_routes['cams-domain-twispay-response'] = new Zend_Controller_Router_Route_Regex(
            '(([a-z]{2})/)?ef-cams/twispay-response',
            array('action' => 'twispay-response'),
            array(1 => 'lang_id')
        );
				
		$cams_plain_routes['cams-domain-private'] = new Zend_Controller_Router_Route(
			'private/:action/*',
			array(
				'controller' => 'private',
				'action' => 'index'
			)
		);
		
		$cams_plain_routes['cams-domain-private-signup'] =	new Zend_Controller_Router_Route_Regex(
			'(([a-z]{2})/)?private/signup-(member|vip-member|escort|agency)',
			array(
				'controller' => 'private',
				'action' => 'signup'
			),
			array(
				1 => 'lang_id',
				3 => 'type'
			)
		);*/
					
		foreach($cams_plain_routes as $key => $route){
			$router->addRoute($key, $cams_host_route->chain($route));
		}
		
		// m.escortforumcams
		
		$mob_cams_plain_routes = array();
       		
        $mob_cams_host_route = new Zend_Controller_Router_Route_Hostname($cams_mob_domain,
            array(
                'module' => 'mobilev2',
                'controller' => 'cams',
                'action' => 'redirect',
            )
		);
		$mob_cams_plain_routes['mob-cams-domain-def'] =  new Zend_Controller_Router_Route('*');
		
		$mob_cams_plain_routes['mob-cams-redirect'] = new Zend_Controller_Router_Route_Regex(
            '(([a-z]{2})/)?mobile/(.+)?',
            array('action' => 'redirect'),
            array(
				3 => 'req'
			)
        );
		
		/*$mob_cams_plain_routes['mob-cams-domain-def'] =  new Zend_Controller_Router_Route('*');
		
		$mob_cams_plain_routes['mob-cams-domain-main'] = new Zend_Controller_Router_Route_Regex(
            '(([a-z]{2})/)?mobile/(.+)?',
            array('action' => 'index'),
            array(
				3 => 'req'
			)
        );
		
		$mob_cams_plain_routes['mob-cams-domain-auth'] = new Zend_Controller_Router_Route_Regex(
            '(([a-z]{2})/)?mobile/cam-([^/]+)(/.+)?',
            array('action' => 'index'),
            array(
				3 => 'sid',
				4 => 'req'
			)
        );
		
		$mob_cams_plain_routes['mob-cams-domain-escort-login'] = new Zend_Controller_Router_Route_Regex(
            '(([a-z]{2})/)?mobile/ef-cams/login',
            array('action' => 'login'),
            array(1 => 'lang_id')
        );
		
		$mob_cams_plain_routes['mob-cams-domain-escort-signup'] = new Zend_Controller_Router_Route_Regex(
            '(([a-z]{2})/)?mobile/ef-cams/signup',
            array('action' => 'signup'),
            array(1 => 'lang_id')
        );
		
		$mob_cams_plain_routes['mob-cams-domain-buy-tokens'] = new Zend_Controller_Router_Route_Regex(
            '(([a-z]{2})/)?mobile/ef-cams/buy-tokens',
            array('action' => 'buy-tokens'),
            array(1 => 'lang_id')
        );
				
		$mob_cams_plain_routes['mob-cams-domain-ecardon-popup'] = new Zend_Controller_Router_Route_Regex(
            '(([a-z]{2})/)?mobile/ef-cams/ecardon-popup',
            array('action' => 'ecardon-popup'),
            array(1 => 'lang_id')
        );
						
		$mob_cams_plain_routes['mob-cams-domain-ecardon-response'] = new Zend_Controller_Router_Route_Regex(
            '(([a-z]{2})/)?mobile/ef-cams/ecardon-response',
            array('action' => 'ecardon-response'),
            array(1 => 'lang_id')
        );
		
		$mob_cams_plain_routes['mob-cams-domain-twispay-response'] = new Zend_Controller_Router_Route_Regex(
            '(([a-z]{2})/)?mobile/ef-cams/twispay-response',
            array('action' => 'twispay-response'),
            array(1 => 'lang_id')
        );
				
		$mob_cams_plain_routes['mob-cams-domain-private'] = new Zend_Controller_Router_Route(
			'mobile/private/:action/*',
			array(
				'controller' => 'private',
				'action' => 'index'
			)
		);*/
		
		foreach($mob_cams_plain_routes as $key => $route){
			$router->addRoute($key, $mob_cams_host_route->chain($route));
		}
			
	}

	protected function _initAutoload()
	{
		$moduleLoader = new Zend_Application_Module_Autoloader(array(
			'namespace' => '',
			'basePath' => APPLICATION_PATH
		));

		$autoloader = Zend_Loader_Autoloader::getInstance();
		$autoloader->registerNamespace('Cubix_');


		/*$mobile_sess = new Zend_Session_Namespace('mobile_sess');
		 $mobile_sess->full_version = false;*/
       /* if( isset($_GET['full_version']) && ! empty($_GET['full_version']) ) {
            $mobile_sess->full_version = true;
        }*/

      /* */


		return $moduleLoader;
	}

	protected function _initDatabase()
	{
		$this->bootstrap('db');
		if(getenv('MYSQL_HOST')){
			$db_configs = array(
				'adapter' => 'MYSQLi',
				'host' => getenv('MYSQL_HOST'),
				'username' => getenv('MYSQL_USER'),
				'password' => getenv('MYSQL_PASS'),
				'port' => getenv('MYSQL_PORT') ? getenv('MYSQL_PORT') : 3306,
				'dbname' => getenv('MYSQL_DBNAME')
			);
			$db = new Cubix_Db_Adapter_Mysqli($db_configs);
		}
		else{
			$db = $this->getResource('db');
		}
		$db->setFetchMode(Zend_Db::FETCH_OBJ);

		Zend_Registry::set('db', $db);
		Cubix_Model::setAdapter($db);

		$db->query('SET NAMES `utf8`');
		
		if(defined('RABBIT_ON') && RABBIT_ON ){
			$db->getProfiler()->setEnabled(true);
		}
		
	}

	protected function _initConfig()
	{

		$images_configs = $this->getOption('images');
		$video_configs = $this->getOption('videos');
		$images_configs['remote']['serverUrl'] = APP_HTTP . '://' . $images_configs['remote']['serverUrl'];
		$images_configs['remote']['url'] = APP_HTTP . '://' . $images_configs['remote']['url'];
		$video_configs['Media'] = APP_HTTP . '://' . $video_configs['Media'];
		$video_configs['remote_url'] = APP_HTTP . '://' . $video_configs['remote_url'];

		if (getenv('IMAGE_TEMP_DIR_PATH')) {
			// this path has to be shared file system for chunked html5 upload to work
			$images_configs['temp_dir_path'] = getenv('IMAGE_TEMP_DIR_PATH');
		}
		
		if(getenv('VIDEO_FTP_HOST')){
			$video_configs['remote']['ftp']['host'] = getenv('VIDEO_FTP_HOST');
			$video_configs['remote']['ftp']['port'] = getenv('VIDEO_FTP_PORT') ? getenv('FTP_PORT') : 21;
			$video_configs['remote']['ftp']['login'] = getenv('VIDEO_FTP_USER');
			$video_configs['remote']['ftp']['password'] = getenv('VIDEO_FTP_PASS');
			$video_configs['remote']['ftp']['passive'] = getenv('VIDEO_FTP_PASSIVE');

			if (getenv('VIDEO_TEMP_DIR_PATH')) {
				// this path has to be shared file system for chunked html5 upload to work
				$video_configs['temp_dir_path'] = getenv('VIDEO_TEMP_DIR_PATH');
			}
		}
		
		if(getenv('FTP_HOST')){
			$images_configs['remote']['ftp']['host'] = getenv('FTP_HOST');
			$images_configs['remote']['ftp']['port'] = getenv('FTP_PORT') ? getenv('FTP_PORT') : 21;
			$images_configs['remote']['ftp']['login'] = getenv('FTP_USER');
			$images_configs['remote']['ftp']['password'] = getenv('FTP_PASS');
			$images_configs['remote']['ftp']['passive'] = getenv('FTP_PASSIVE');
		}

		Zend_Registry::set('images_config', $images_configs);
		Zend_Registry::set('videos_config', $video_configs);
		Zend_Registry::set('escorts_config', $this->getOption('escorts'));
		Zend_Registry::set('mobile_config', $this->getOption('mobile'));
		Zend_Registry::set('reviews_config', $this->getOption('reviews'));
		Zend_Registry::set('faq_config', $this->getOption('faq'));
		Zend_Registry::set('feedback_config', $this->getOption('feedback'));
		Zend_Registry::set('system_config', $this->getOption('system'));
		Zend_Registry::set('payment_config', $this->getOption('payment'));
		Zend_Registry::set('newsman_config', $this->getOption('newsman'));
		Zend_Registry::set('payment_config', $this->getOption('payment'));
		Zend_Registry::set('statistics', $this->getOption('statistics'));
	}

	protected function _initViewHelpers()
	{
		$this->bootstrap('layout');
		$layout = $this->getResource('layout');
		$view = $layout->getView();

		$view->doctype('XHTML1_STRICT');

		$view->headMeta()
			->appendHttpEquiv('Content-Type', 'text/html; charset=utf-8')
			->appendHttpEquiv('Cache-Control', 'no-cache');
		$view->headTitle()->setSeparator(' - ');
		//$view->headTitle('EscortForum Backend');

	}

	protected function _initDefines()
	{

	}

	protected function _initCache()
	{
		$frontendOptions = array(
			'lifetime' => null,
			'automatic_serialization' => true
		);

		
		if(getenv('MEMCACHE_HOST')){
			$backendOptions = array(
				'servers' => array(
					array(
						'host' => getenv('MEMCACHE_HOST'),
						'port' => getenv('MEMCACHE_PORT'),
						'persistent' => true
					)
				),
			);
		}
		else{
			$backendOptions = array(
				'servers' => array(
					array(
						'host' => '127.0.0.1',
						//'host' => '172.16.20.20',
						//'host' => '172.16.1.51',
						'port' => '11211',
						'persistent' => true
					)
				),
			);
		}

		if (defined('IS_DEBUG') && IS_DEBUG && APPLICATION_ENV == 'development') {
			$cache = Zend_Cache::factory(new Cubix_Cache_Core($frontendOptions), new Cubix_Cache_Backend_Memcached($backendOptions));
			//$cache = Zend_Cache::factory('Core', 'Blackhole', $frontendOptions, array());
		} else {
			$cache = Zend_Cache::factory(new Cubix_Cache_Core($frontendOptions), new Cubix_Cache_Backend_Memcached($backendOptions));
		}

		Zend_Registry::set('cache', $cache);

		/*************************************/
		if(getenv('MEMCACHE_HOST')){
			$servers = array('host' => getenv('MEMCACHE_HOST'), 'port' => getenv('MEMCACHE_PORT'));
		}
		else{
			$servers = array('host' => 'fe01', 'port' => 11211);
		}
		
		//$servers = array('host' => '77.109.150.104',  'port' => 11211);
		$memcache = new Memcache();
		$memcache->addServer('memcached-backend', 11211);

		Zend_Registry::set('cache_native', $memcache);
		$redis = new Redis();
		if(getenv('REDIS_HOST')){
			$redis->connect(getenv('REDIS_HOST'), getenv('REDIS_PORT'));
		}
		else{
			$redis->connect('127.0.0.1', 6379); 
		}
		
		Zend_Registry::set('redis', $redis);
	}

	protected function _initBanners()
	{
		if (defined('IS_CLI') && IS_CLI) return;

		$banners = array();

		/*$cache = Zend_Registry::get('cache');

		$config = Zend_Registry::get('system_config');

		$banners['banners']['top_banners'] = array();
		$banners['banners']['right_banners'] = array();
		$banners['banners']['right_banners_10'] = array();
		$banners['banners']['right_banners_bottom'] = array();
		$banners['banners']['right_banners_bottom2'] = array();


		if ( ! $banners = $cache->load('v2_banners_cache_efv2') ) {
			for ($i = 0; $i < 10; $i++) {
				$banners['banners']['right_banners'][] = Cubix_Banners::GetBanner(1);
			}

			for ($i = 0; $i < 4; $i++) {
				$banners['banners']['top_banners'][] = Cubix_Banners::GetBanner(1);
			}

			for ($i = 0; $i < 3; $i++) {
				$banners['banners']['right_banners_bottom'][] = Cubix_Banners::GetBanner(2);
			}



			$banners['banners']['right_banners_comment'][] = Cubix_Banners::GetBanner(4);

			$cache->save($banners, 'v2_banners_cache_efv2', array(), $config['bannersCacheLifeTime']);
		}
		 */

		Zend_Registry::set('banners', $banners);
	}

	protected function _initApi()
	{
		$api_config = $this->getOption('api');
		if (getenv('API_SERVER')) {
			$api_config['server'] = getenv('API_SERVER');
		}
		Zend_Registry::set('api_config', $api_config);
		Cubix_Api_XmlRpc_Client::setApiKey($api_config['key']);
		Cubix_Api_XmlRpc_Client::setServer($api_config['server']);
		
		/* RABBITMQ */
		if(defined('RABBIT_ON') && RABBIT_ON ){
			$rm_conf = $this->getOption('rabbitmq');
			$connection = new Cubix_PhpAmqpLib_Connection_AMQPStreamConnection(
					$rm_conf['host'], 
					$rm_conf['port'], 
					$rm_conf['user'], 
					$rm_conf['password'], 
					$rm_conf['vhost']
				);
			
			$channel = $connection->channel();
			//$que_name = 'hello_test';
			//$channel->queue_declare($que_name, false, true, false, false);
			Zend_Registry::set('rabbit_mq', array('connection' => $connection, 'channel' => $channel, 'uniq_id' => uniqid()));
		}
	}

    protected function _initPlugins()
    {
        $objFront = Zend_Controller_Front::getInstance();
        $objFront->registerPlugin(new CookiePlugin());
        return $objFront;
    }
}
