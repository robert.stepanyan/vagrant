<?php
class Api_IxsController extends Zend_Controller_Action
{
	/**
	 * @var Zend_Db_Adapter_Mysqli
	 */
	protected $_db;
	
	protected $model;

	protected $client;

	private $cli;

	public function init()
	{
		$this->client = Cubix_Api_XmlRpc_Client::getInstance();
		$this->_db = Zend_Registry::get('db');
		$this->view->layout()->disableLayout();
		$this->model = new Model_ixsBanners();
		$this->cli = new Cubix_Cli();

	}

	protected function syncWithBackend()
    {
        $this->cli->clear();
        $this->cli->colorize('yellow')->out('Fetching Banners data from backend. ')->reset();
        $rows = $this->client->call('Escorts.getBannersEd');
        $this->cli->colorize('green')->out(count($rows) . ' Banners Fetched.')->reset();

        $this->cli->colorize('purple')->out('Downloading images. ')->reset();
        $c = $this->model->copyImages($rows);
        $this->cli->colorize('green')->out($c . ' Images downloaded.')->reset();

        $this->model->fill($rows);

        return true;
    }

	public function ixsBannersAction(){
		$this->syncWithBackend();
        exit();
        
		$this->view->layout()->disableLayout();
		$url = 'https://www.ixspublic.com/deliver.php?websiteID=10';
		$ixspublic = file_get_contents($url);

		//preg_match_all('|(\{)("id"[^\}]+)\}|is', $ixspublic, $matches);

		// bazza lcneluc zone id a petq vor chei drel poxum ei bayc mnac kisat kati eresic))
		preg_match_all('|zonesJsonBuffer = ({[^;]+);|is', $ixspublic, $matches);
		$ixs = json_decode($matches[1][0]);

		//$images is an numeric array of images with their attributes
		$model_ixs = new Model_ixsBanners();

		$model_ixs->truncate();
		$result = true;
		
		foreach ($ixs as $zone => $images) {
			$mysql_insert_result = $model_ixs->insert( $ixs->$zone, $zone);
			if($mysql_insert_result){
				$images = $model_ixs->get();
				$copycount = $model_ixs->copyImages($images);
				
				if(count($images) == $copycount){
					$result = 'Images successfully synced';
				}else{
					$result = 'Error during sync';
				}
			}
		}
		echo($result);
		die;
	}

}
