<?php

class Api_DictionaryController extends Zend_Controller_Action 
{
	/**
	 * @var Zend_Db_Adapter_Mysqli
	 */
	protected $_db;
	
	public function init()
	{
		$this->_db = Zend_Registry::get('db');
		
		$this->view->layout()->disableLayout();
	}
	
	/**
	 * Get escorts with xml rpc and insert all rows into the short table
	 */
	public function indexAction()
	{
		$errors = array();
		
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		
		$langs = $client->call('Application.getLangs');
		$lang_ids = array();
		
		$this->_db->query('TRUNCATE langs');
		foreach ( $langs as $lang ) {
			$this->_db->insert('langs', $lang);
			$lang_ids[] = $lang['id'];
		}
		
		$this->_db->query('TRUNCATE application_langs');
		$app_id = Cubix_Application::getId();
		foreach ( $langs as $lang ) {
			$this->_db->insert('application_langs', array(
				'application_id' => $app_id,
				'lang_id' => $lang['id']
			));
		}
		
		$dic = $client->call('Application.getDictionary', array($lang_ids));
		
		$this->_db->query('TRUNCATE dictionary');
		foreach ( $dic as $row ) {
			$this->_db->insert('dictionary', $row);
		}

		die;
	}

	protected function _call($method, $params = array())
	{
		$url = Cubix_Api_XmlRpc_Client::getServer() . '/?api_key=' . Cubix_Api_XmlRpc_Client::getApiKey();
		$url .= '&method=' . $method;

		foreach ( $params as $i => $param ) {
			$params[$i] = 'params[]=' . urlencode($param);
		}

		$url .= '&' . implode('&', $params);

		return unserialize(file_get_contents($url));
	}

	public function plainAction()
	{

		$errors = array();

		$client = new Cubix_Api_XmlRpc_Client();

		$datum = $status = array();
		try {
			// Transfer Langs Table
			$datum['langs'] = $this->_call('getLangs');
			$status[] = 'Transfered Data of `langs` Table';

			// Transfer Application Langs Table
			$datum['application_langs'] = $this->_call('getAppLangs');
			$status[] = 'Transfered Data of `application_langs` Table';

			// Transfer Dictionary
			$datum['dictionary'] = $this->_call('getDictionary');
			$status[] = 'Transfered Data of `dictionary` Table';

			foreach ( $datum as $table => $data ) {
				if ( $data ) {
					$this->_db->query('TRUNCATE `' . $table . '`');
					foreach ( $data as $row ) {
						$this->_db->insert($table, (array)$row);
					}

					$status[] = 'Updated Data of `' . $table . '` Table (' . count($data) . ' rows in total)';
				}
			}
			// <--

			$status[] = 'Transfer Successfully Completed!';
		}
		catch ( Exception $e ) {
			$status[] = 'ERROR: Exception Cauth';
			$status[] = $e->__toString();
		}

		echo implode('<br/>' . "\r\n", $status);
		ob_flush();
		die;
	}

	public function seoPlainAction()
	{
		$seo_ents = $this->_call('getSeoEntities');
		
		if ( count($seo_ents) > 0 ) {
			$this->_db->query('TRUNCATE seo_entities');
			foreach ($seo_ents as $se)
			{
				$this->_db->insert('seo_entities', $se);
			}
		}

		$seo_ents_inst = $this->_call('getSeoEntitiesInstances');
		if ( count($seo_ents_inst) > 0 ) {
			$this->_db->query('TRUNCATE seo_entity_instances');
			foreach ($seo_ents_inst as $sei)
			{
				$this->_db->insert('seo_entity_instances', $sei);
			}
		}

		$seo_robots_google_id = $this->_call('getSR');
		
		$this->_db->update('applications', array('robots' => $seo_robots_google_id['body'], 'm_robots' => $seo_robots_google_id['m_body'], 'google_id' => $seo_robots_google_id['google_id']), $this->_db->quoteInto('id = ?', Cubix_Application::getId()));

		die('Done!');
	}

	public function emailTemplatesAction()
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();

		$ets = $client->call('Application.getEmailTemplates', array(Cubix_Application::getId()));


		$this->_db->query('TRUNCATE email_templates');
		/*$this->_db->query('DROP TABLE IF EXISTS `email_templates`;');
		$this->_db->query('
CREATE TABLE `email_templates` (
`id` varchar(255) NOT NULL,
`application_id` int(10) unsigned NOT NULL,
`from_addr` varchar(255) NOT NULL,
`subject` varchar(255) DEFAULT NULL,
`body_plain` text,
`body` text,
`lang_id` varchar(2) NOT NULL,
PRIMARY KEY (`id`,`lang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;');*/

		foreach ( $ets as $et ) {
			$this->_db->insert('email_templates', $et);
		}

		die;
	}
	
	public function geographyAction()
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		
		foreach ( array('countries', 'regions', 'cities') as $section ) {
			$data = $client->call('Application.getGeography', array($section));
			
			$this->_db->query('TRUNCATE ' . $section);
			
			foreach ( $data as $row ) {
				if ( isset($row['ordering']) ) unset($row['ordering']);
				
				$this->_db->insert($section, $row);
			}
		}
		
		die;
	}

	public function geographyPlainAction()
	{
		$datum = array();
		$client = new Cubix_Api_XmlRpc_Client();
		foreach ( array('countries', 'regions', 'cities', 'cityzones') as $section ) {
			$datum[$section] = $client->call('getGeography', array($section));
		}

		foreach ( $datum as $section => $data ) {
			if ( $data ) {
				$this->_db->query('TRUNCATE ' . $section);

				foreach ( $data as $row ) {
					if ( isset($row['ordering']) ) unset($row['ordering']);

					$this->_db->insert($section, $row);
				}
			}
		}

		die;
	}

	public function staticContentAction()
	{
		$this->_db->query('TRUNCATE static_content');

		foreach ( $this->_call('getStaticContent') as $page ) {
			$this->_db->insert('static_content', $page);
		}
		
		die;
	}

	public function glossaryAction()
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();

		$ets = $client->call('Application.geGlossary', array());

		$this->_db->query('TRUNCATE glossary');

		foreach ( $ets as $et ) {
			$this->_db->insert('glossary', $et);
		}

		die;
	}
}
