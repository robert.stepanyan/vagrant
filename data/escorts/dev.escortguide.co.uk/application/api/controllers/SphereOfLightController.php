<?php

/**
 * His Eduard Hovhannisyan's Majesty Requests and request
 * in the name of His Majesty all those whom is may concern
 * to allow the bearer to use freely without let or hindrance the code written below.
 *
 * Will use this class to fix all subsequent issues, that
 * are caused by big/small syncs.
 *
 * NOTE:
 * If you add more functionality, please dont be lazy pussy-ass, add some comments
 *
 * Class SphereOfLightController
 * @author Eduard Hovhannisyan
 */
class Api_SphereOfLightController extends Zend_Controller_Action
{

    /**
     * @var Zend_Db_Adapter_Mysqli
     */
    protected $db;

    /**
     * @var Cubix_Api_XmlRpc_Client
     */
    protected $client;

    /**
     * @var Cubix_Cli
     */
    private $cli;

    /**
     * First step of actions lifecycle.
     * @return void
     */
    public function init()
    {
        $this->client = Cubix_Api_XmlRpc_Client::getInstance();
        $this->cli = new Cubix_Cli();

        $this->db = Zend_Registry::get('db');
        $this->view->layout()->disableLayout();
    }

    /**
     * This action will be executed every day (only once) to calculate the number of
     * cities for each country and update "countries" table. This is needed
     * in order to avoid this type of redundant calculations with every request.
     *
     * @return void
     */
    public function updateCountryCitiesCountAction()
    {
        try {

            $this->cli->clear();
            $this->cli->colorize('yellow')->out('Calculating number of cities for each country')->reset();

            $sql = "
                SELECT c.id, c.title_en, count( ct.id ) AS cities_count   
                FROM  countries c 
                LEFT JOIN cities ct ON ct.country_id = c.id 
                GROUP BY c.id 
                ORDER BY cities_count
            ";
            $rows = $this->db->fetchAll($sql);

            $index = $overall = count($rows);
            $updated = 0;
            $error = 0;

            $this->cli->colorize('green')->out('Found ' . $overall . ' countries to update')->reset();

            while ($index--) {
                $row = (array) $rows[$index];

                $fields = ['cities_count' => $row['cities_count']];

                if (!empty($fields)) {
                    $this->db->update('countries', $fields, [
                        $this->db->quoteInto('id = ?', $row['id']),
                    ]);
                    $updated++;
                } else {
                    $error++;
                }

                $this->cli->colorize('green')->out("[$updated/$overall] Updated")
                    ->reset();
            }

            $this->cli->colorize('green')->out("Done! Enjoy your life.")->reset();

        } catch (\Exception $exception) {
            $this->cli->colorize('red')->out($exception->getMessage())->reset();
        }

        exit;
    }

}

?>