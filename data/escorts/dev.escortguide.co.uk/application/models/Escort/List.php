<?php

class Model_Escort_List extends Cubix_Model
{
	public static function getActiveFilter($filter, $where_query = array(), $is_upcomingtour = false, $is_tour = false)
	{
		if ( isset($filter['e.showname LIKE ?']) ) {
			unset($filter['e.showname LIKE ?']);
		}

		if ( isset($filter['(e.showname LIKE ? OR e.agency_name LIKE ?)']) ) {
			unset($filter['(e.showname LIKE ? OR e.agency_name LIKE ?)']);
		}

		if ( $is_tour ) {
			if ( isset($filter['eic.gender = 1']) ) unset($filter['eic.gender = 1']);

			if ( ! $is_upcomingtour ) {
				$filter[] = 'eic.is_tour = 1';
			}
			else {
				$filter[] = 'eic.is_upcoming = 1';
			}
		} else {
			// Only escorts with base city (or city tour) will be shown in this list
			if ( ! isset($filter['ct.slug = ?']) && ! isset($filter['r.slug = ?']) ) {
				$filter[] = 'eic.is_base = 1';
			}
			else {
				$filter[] = 'eic.is_upcoming = 0';
			}
		}

		$where = self::getWhereClause($filter, true);

		$wqa = array();
		if ( count($where_query) ) {
			foreach( $where_query as $wq ) {

				if ( strlen($wq['query']) ) {
					$where_query = ' AND ' . $wq['filter_query'];

					$where .= $where_query;
					$wqa[] = array('filter_query' => $wq['filter_query'], 'query' => $wq['query'] );
				}
			}
		}

		$metadata = self::db()->describeTable('escort_filter_data');
		$column_names = array_keys($metadata);
		unset($column_names[0]);

		$select = array();
		foreach ( $column_names as $column ) {
			$select[] = 'SUM(' . $column . ') AS ' . $column;
		}

		$join = " ";

		if ( isset($filter['cz.slug = ?']) ) {
			$join = "
				INNER JOIN escorts_in_cityzones eicz ON eicz.escort_id = fd.escort_id
				INNER JOIN cityzones cz ON cz.id = eicz.cityzone_id
			";
		}

		$sql = '
			SELECT 
				' . implode(', ', $select) . ' 
			FROM ( SELECT fd.*
			FROM escort_filter_data fd
			INNER JOIN escorts_in_cities eic ON eic.escort_id = fd.escort_id
			INNER JOIN escorts e ON e.id = fd.escort_id
			INNER JOIN cities ct ON eic.city_id = ct.id
			' . $join . '
			' . (! is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY fd.escort_id ) f
		';

		//echo $sql;

		return array('result' => self::db()->fetchRow($sql), 'global_filter' => $filter , 'where_query' => $wqa, 'is_upcoming' => $is_upcomingtour, 'is_tour' => $is_tour );
	}


	public static function getMainAgencySplit($page = 1, $limit = 80)
	{
		$page = (int) $page;
		if ( $page < 1 ) $page = 1;
		$start = ($limit * ($page - 1));

		/*if ( $gotd_escorts && $page == 1 ) {
			$limit -= count($gotd_escorts);
		} elseif ( $gotd_escorts && $page > 1 ) {
			$start -= count($gotd_escorts);
			unset($gotd_escorts);
		}*/

		$lng = Cubix_I18n::getLang();

		$cache = Zend_Registry::get('cache');
		$cache_key = 'main_agency_split' . Cubix_Application::getId() . '_page_' . $page . '_' . $limit . '_lang_' . Cubix_I18n::getLang();
		$cache_key .= Cubix_Countries::blacklistCountryCacheKeyCase();

		if ( ! $escorts = $cache->load($cache_key) ) {

			if (date('G') < 2)
				$date = 2;
			else
				$date = date('G');

			$where = Cubix_Countries::blacklistCountryWhereCase();

			$sql = '
				SELECT
					/* escorts table */
					e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id,
					e.rates, e.incall_type, e.outcall_type, e.is_new, e.verified_status,
					e.photo_hash, e.photo_ext, e.photo_status,
					e.products, e.slogan, e.date_last_modified, e.last_hand_verification_date,
					e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.is_suspicious, e.comment_count, e.review_count,

					ct.title_' . $lng . ' AS city,

					/* seo_entity_instances table */
					IF (sei.primary_id IS NULL, se.title_' . $lng . ', sei.title_' . $lng . ') AS alt,

					IF (e.available_24_7 = 1 OR (e.is_now_open = 1 AND e.close_time >= "' . $date . '"), 1, 0) AS is_late_night,
					/* misc */
					' . Cubix_application::getId() . ' AS application_id, e.hh_is_active, e.is_online, e.about_' . $lng . ' AS about,
					UNIX_TIMESTAMP(e.date_last_modified) AS date_last_modified , ct.id AS city_id
				FROM escorts e
				INNER JOIN escorts_in_cities eic ON eic.escort_id = e.id AND eic.is_base = 1
				INNER JOIN cities ct ON ct.id = eic.city_id
				LEFT JOIN seo_entity_instances sei ON sei.primary_id = e.id
				LEFT JOIN seo_entities se ON (se.id = sei.entity_id) OR (se.application_id = 18 AND se.slug = "escort")
				WHERE
					eic.gender = ' . GENDER_FEMALE . ' AND e.is_main_premium_spot = 0 AND (e.agency_id IS NULL OR e.show_on_main = 1) ' . $where . '
				GROUP BY e.id
				ORDER BY e.id
				LIMIT ' . $start . ', ' . $limit . '
			';
			$escorts = self::db()->fetchAll($sql);

			/* seo heading escorts */
			if ($escorts)
			{
				$es = array();
				foreach ($escorts as $e)
				{
					$es[] = $e->id;
				}
				$es_str = implode(',', $es);

				$query = "SELECT e.id, sei.heading_escort_{$lng} as heading_escort
					FROM escorts e
					INNER JOIN escorts_in_cities eic ON eic.escort_id = e.id AND eic.is_base = 1
					INNER JOIN seo_entity_instances sei ON sei.primary_id = eic.city_id
					INNER JOIN seo_entities se ON se.application_id = " . Cubix_Application::getId() . " AND se.slug = 'city' AND sei.entity_id = se.id
					WHERE e.id IN ({$es_str})
				";
				$result = self::db()->fetchAll($query);

				foreach ($result as $r)
				{
					foreach ($escorts as $e)
					{
						if ($e->id == $r->id)
						{
							$e->heading_escort = $r->heading_escort;
							break;
						}
					}
				}
			}
			/**/

			$cache->save($escorts, $cache_key, array());
		}

		// <editor-fold defaultstate="collapsed" desc="Make Escort Alt for SEO">
		$tpl_data = array(
			'app_title' => Cubix_Application::getById()->title
		);

		foreach ( $escorts as $i =>  $escort ) {
			$additional = array(
				'showname' => $escort->showname,
				'city' => $escort->city
			);

			foreach ( $tpl_data + $additional as $tpl_var => $tpl_value ) {
				$escorts[$i]->alt = str_replace('%' . $tpl_var . '%', $tpl_value, $escorts[$i]->alt);
			}
		}
		// </editor-fold>

		shuffle($escorts);

		// <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
		$sid = 'sedcard_paging_' . Cubix_Application::getId();
		$sess = new Zend_Session_Namespace($sid);

		$sess->main_premium_spot = array();
		foreach ( $escorts as $escort ) {
			$sess->main_premium_spot[] = $escort->showname;
		}

		$sess->main_premium_spot_criterias = array(
			'page'	=> $page
		);
		// </editor-fold>


		return $escorts;
	}

	public static function getFiltered($filter, $ordering = 'random', $page = 1, $page_size = 50, &$count = 0, $sess_name = false, $where_query = array(), $only_premiums = false, $external_filters = array())
	{
		$lng = Cubix_I18n::getLang();
		$j = "";
		$f = "";
		$select = "";

		if (date('G') < 2)
			$date = 2;
		else
			$date = date('G');

		if( strlen($external_filters['name']) > 50 ){
			echo 'Showname cannot be longer than 50 symbols'; die;
		}

		if ( isset($external_filters['price-from']) && $external_filters['price-from'] ) {
			$filter[] = 'e.incall_price >= ' . $external_filters['price-from'];
			//$filter[] = 'e.incall_currency = 8';
		}

		if ( isset($external_filters['price-to']) && $external_filters['price-to'] ) {
			$filter[] = 'e.incall_price <= ' . $external_filters['price-to'];
			//$filter[] = 'e.incall_currency = 8';
		}

        if ( $external_filters['price-from'] || $external_filters['price-to']) {
            $filter[] = 'e.incall_price is not null';
        }


		if ( isset($external_filters['age-from']) && $external_filters['age-from'] ) {
			$filter[] = 'e.age >= ' . $external_filters['age-from'];
		}

		if ( isset($external_filters['age-to']) && $external_filters['age-to'] ) {
			$filter[] = 'e.age <= ' . $external_filters['age-to'];
		}

		if( $external_filters['age-from'] || $external_filters['age-to']){
            $filter[] = 'e.age is not null';
        }

		if ( isset($external_filters['incall']) && $external_filters['incall'] ) {
			$filter[] = 'e.incall_type IS NOT NULL';
		}

		if ( isset($external_filters['outcall']) && $external_filters['outcall'] ) {
			$filter[] = 'e.outcall_type IS NOT NULL';
		}

		if ( isset($external_filters['verified-contact']) && $external_filters['verified-contact'] ) {
			$filter[] = 'e.last_hand_verification_date IS NOT NULL';
		}

		if ( isset($external_filters['review']) && $external_filters['review'] ) {
			$j .= " INNER JOIN reviews rv ON rv.escort_id = e.id ";
		}

		if ( isset($external_filters['verified']) && $external_filters['verified'] ) {
			$filter[] = 'e.verified_status = 2';
		}

		if ( 'price' == substr($ordering, 0, 5) ) {
			$filter[] = 'e.incall_price IS NOT NULL';
		}

        if ( $external_filters['name'] != '') {
            $filter[] = "e.showname like '%{$external_filters['name']}%'";
        }

        if ( $external_filters['show_on_main'] != '') {
            $filter[] = "(IF( FIND_IN_SET( 18, e.products ) > 0, 1, 0 ) > 0	OR e.show_on_main = 1 or agency_id is null OR is_main_premium_spot = 1)";
        }

        if ( isset($external_filters['video']) && $external_filters['video'] ) {
            $filter[] = 'e.has_video = 1';

        }

        if ( isset($external_filters['f_independent']) && $external_filters['f_independent'] && !$external_filters['f_agency']) {
            $filter[] = 'e.agency_id is null';
        }

        if ( isset($external_filters['f_agency']) && $external_filters['f_agency'] && !$external_filters['f_independent']) {
            $filter[] = 'e.agency_id is not null';
        }

		// Only for Videos Page
		if ( isset($filter['e.has_video = 1']) ) {
			 $j .= " INNER JOIN video vo ON vo.escort_id = e.id ";
			 $j .= " INNER JOIN video_image vi ON vo.id = vi.video_id ";
			 $select .= ' ,vi.hash as video_img_hash, vi.ext as video_img_ext';
		}

		// Only escorts with base city (or city tour) will be shown in this list

		if ( ! isset($filter['ct.slug = ?']) && ! isset($filter['r.slug = ?']) ) {
            if ( $only_premiums ) {
                $filter[] = 'eic.is_premium = 1';
            } else {
			    $filter[] = 'eic.is_base = 1';
            }
		}
		else {
			$filter[] = 'eic.is_upcoming = 0';
		}

		$isAjaxFiltered = false;
        if (isset($filter['ajax_filtered']))
        {
            $isAjaxFiltered = true;
            unset($filter['ajax_filtered']);
        }

        if (isset($filter['tagged_escorts']))
        {
            $filter[] = 'e.id IN ('.$filter['tagged_escorts'].')';
            unset($filter['tagged_escorts']);
        }

		$where = self::getWhereClause($filter, true);
		$order = self::_mapSorting($ordering);



		if ( isset($filter['f.user_id = ?']) ) {
			$j .= " INNER JOIN favorites f ON f.escort_id = e.id ";
			$f .= " , f.user_id AS fav_user_id ";
		}

		//Rotation for premium escorts
		$order = str_replace('eic.is_premium', 'IF(eic.is_tour = 1, 0, eic.is_premium)', $order);
        $order = preg_replace('/is_premium_plus/', 'RAND(),is_premium_plus', $order);
        
		if ( count($where_query) ) {
			foreach( $where_query as $wq ) {
				if ( strlen($wq['query']) ) {
					if( strpos( $wq['query'],'e.weight') !== false){
						$wq['query'] = Cubix_UnitsConverter::filterQueryConvert($wq['query']);
					}
					$where_query = ' AND ' . $wq['query'];
					$where .= $where_query;
				}
			}
		}

		// Bad but Fast solution sorry :)
		if(strpos($where, 'ek.keyword_id') !== false){
			$j .= " INNER JOIN escort_keywords ek ON ek.escort_id = e.id ";
		}

		if(strpos($where, 'es.service_id') !== false){
			$j .= " INNER JOIN escort_services es ON es.escort_id = e.id ";
		}

		is_null($where) ? $where = ' 1 ' . Cubix_Countries::blacklistCountryWhereCase() : $where .= Cubix_Countries::blacklistCountryWhereCase();

		$location_where = '';

		if(isset($filter['ct.slug = ?']) && !$isAjaxFiltered) {
			$location_where = ' FIND_IN_SET(16, e.products) > 0 AND ';
		}

        if ($page == 1 && $page_size == 5555)
        {
            $premioum_gotd_sql = "SELECT SQL_CALC_FOUND_ROWS
                                            e.id 
                                        FROM
                                            escorts e
                                            INNER JOIN escorts_in_cities eic ON eic.escort_id = e.id 
                                        WHERE 1
                                            ". (! is_null($where) ? ' AND ' . $location_where . $where : '') ."
                                            AND eic.is_premium = 1 
                                            OR FIND_IN_SET( 18, e.products ) > 0 
                                        GROUP BY
                                            eic.escort_id";
            $premium_gotd_escorts = $escorts = self::db()->fetchAll($premioum_gotd_sql);
            $premium_gotd_escorts_count = $page_size = self::db()->fetchOne('SELECT FOUND_ROWS()');
            if ($premium_gotd_escorts_count > 59)
            {
                while ($page_size % 3 != 0)
                {
                    $page_size++;
                }
                if ($page_size != $premium_gotd_escorts_count)
                {
                    --$page_size;
                }else{
                    do{
                        $page_size++;
                    }while ($page_size % 3 != 0);

                    --$page_size;
                }
            }else{
                while ($page_size != 59)
                {
                    $page_size++;
                }
            }

        }else if( $page > 1 && $page_size == 5555 ){
            $page_size = 59;
            $where .= ' AND eic.is_premium = 0 AND FIND_IN_SET(18, e.products) <> 1';
        }

        $page = (int) $page;
        if ( $page < 1 ) $page = 1;
        $limit = ($page_size * ($page - 1)) . ', ' . $page_size;

        $sql = '
			SELECT SQL_CALC_FOUND_ROWS
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.gender , e.user_id, e.is_inactive,
				UNIX_TIMESTAMP(e.date_activated) AS date_activated,
				ct.title_' . $lng . ' AS city, e.rates, e.incall_type, e.outcall_type, e.is_new,
				e.verified_status, e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_application::getId() . ' AS application_id,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.comment_count, e.review_count,
                eic.is_featured * (FLOOR(RAND() * 10) + 1) as is_featured,
                UNIX_TIMESTAMP(e.last_login_date) as last_login_date,
				IF (sei.primary_id IS NULL, se.title_' . $lng . ', sei.title_' . $lng . ') AS alt,
				IF (e.available_24_7 = 1 OR (e.is_now_open = 1 AND e.close_time >= "' . $date . '"), 1, 0) AS is_late_night,
				e.hit_count, e.slogan, e.incall_price,
				e.date_last_modified, e.is_pornstar, e.last_hand_verification_date,
				/*IF(eic.is_tour = 1 AND eic.is_premium = 1, 0, eic.is_premium) AS is_premium,*/ 
				IF(eic.is_tour = 1, 0, eic.is_premium) AS is_premium,
				IF(FIND_IN_SET(14, e.products) > 0, 1, 0 ) AS is_premium_plus,
				IF(FIND_IN_SET(18, e.products) > 0 AND e.gender = 1, 1, 0 ) AS is_gotd,
				IF(FIND_IN_SET(18, e.products) > 0 AND e.gender = 2, 1, 0 ) AS is_botd,
				e.hh_is_active, e.is_suspicious, e.is_online ' . $f . ', e.about_' . $lng . ' AS about, UNIX_TIMESTAMP(e.date_last_modified) AS date_last_modified, ct.id AS city_id
				'.$select.' 
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			LEFT JOIN regions r ON r.id = eic.region_id
			' . $j . '
			LEFT JOIN escort_cityzones ecz ON ecz.escort_id = eic.escort_id
			LEFT JOIN cityzones cz ON cz.id = ecz.city_zone_id
			LEFT JOIN seo_entity_instances sei ON sei.primary_id = eic.escort_id
			LEFT JOIN seo_entities se ON (se.id = sei.entity_id) OR (se.application_id = 18 AND se.slug = "escort")
			/*LEFT JOIN escort_services es ON es.escort_id = e.id
			LEFT JOIN escort_keywords ek ON ek.escort_id = e.id*/
			' . (! is_null($where) ? 'WHERE ' . $location_where . $where : '') . '
			GROUP BY eic.escort_id
			ORDER BY is_gotd desc, is_botd desc, e.is_inactive asc, ' . $order . '
			LIMIT ' . $limit . '
		';

		if ($_GET['qaq'])
        {
            echo '<pre>';var_dump($sql);die;
        }
		
		$escorts = self::db()->fetchAll($sql);
		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');

		// <editor-fold defaultstate="collapsed" desc="Make Escort Alt for SEO">
		$tpl_data = array(
			'app_title' => Cubix_Application::getById()->title
		);

		foreach ( $escorts as $i =>  $escort ) {
			$additional = array(
				'showname' => $escort->showname,
				'city' => $escort->city
			);

			foreach ( $tpl_data + $additional as $tpl_var => $tpl_value ) {
				$escorts[$i]->alt = str_replace('%' . $tpl_var . '%', $tpl_value, $escorts[$i]->alt);
			}
		}
		// </editor-fold>

		if ( $sess_name ) {
			// <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
			$sid = 'sedcard_paging_' . Cubix_Application::getId();
			$sess = new Zend_Session_Namespace($sid);

			$sess->{$sess_name} = array();
			foreach ( $escorts as $escort ) {
				$sess->{$sess_name}[] = $escort->id;
			}
			$sess->{$sess_name . '_criterias'} = array(
				'filter'	=> $filter,
				'sort'	=> $ordering,
				'page'		=> $page
			);
			// </editor-fold>
		}

        return $escorts;
	}

	public static function getTours($is_upcoming, $filter, $ordering = 'random', $page = 1, $page_size = 50, &$count = 0, $where_query = array(), $external_filters = array())
	{
		$lng = Cubix_I18n::getLang();
		$j = " ";

		if ( isset($external_filters['price-from']) && $external_filters['price-from'] ) {
			$filter[] = 'e.incall_price >= ' . $external_filters['price-from'];
			$filter[] = 'e.incall_currency = 8';
		}

		if ( isset($external_filters['price-to']) && $external_filters['price-to'] ) {
			$filter[] = 'e.incall_price <= ' . $external_filters['price-to'];
			$filter[] = 'e.incall_currency = 8';
		}

		if ( isset($external_filters['age-from']) && $external_filters['age-from'] ) {
			$filter[] = 'e.age >= ' . $external_filters['age-from'];
		}

		if ( isset($external_filters['age-to']) && $external_filters['age-to'] ) {
			$filter[] = 'e.age <= ' . $external_filters['age-to'];
		}

		if ( isset($external_filters['incall']) && $external_filters['incall'] ) {
			$filter[] = 'e.incall_type IS NOT NULL';
		}

		if ( isset($external_filters['outcall']) && $external_filters['outcall'] ) {
			$filter[] = 'e.outcall_type IS NOT NULL';
		}

		if ( isset($external_filters['verified-contact']) && $external_filters['verified-contact'] ) {
			$filter[] = 'e.last_hand_verification_date IS NOT NULL';
		}

		/*if ( isset($external_filters['video']) && $external_filters['video'] ) {
			$j .= " INNER JOIN video v ON v.escort_id = e.id ";
		}*/

		if ( isset($external_filters['review']) && $external_filters['review'] ) {
			$j .= " INNER JOIN reviews rv ON rv.escort_id = e.id ";
		}

		if ( isset($external_filters['verified']) && $external_filters['verified'] ) {
			$filter[] = 'e.verified_status = 2';
		}

		if ( 'price' == substr($ordering, 0, 5) ) {
			$filter[] = 'e.incall_price IS NOT NULL';
		}

		if ( isset($filter['eic.gender = 1']) ) unset($filter['eic.gender = 1']);

		if ( ! $is_upcoming ) {
			$filter[] = 'eic.is_tour = 1';
		}
		else {
			$filter[] = 'eic.is_upcoming = 1';
		}

		$where = self::getWhereClause($filter, true);
		$order = self::_mapSorting($ordering);

		$page = (int) $page;
		if ( $page < 1 ) $page = 1;
		$limit = ($page_size * ($page - 1)) . ', ' . $page_size;

		if ( count($where_query) ) {
			foreach( $where_query as $wq ) {
				if ( strlen($wq['query']) ) {
					$where_query = ' AND ' . $wq['query'];
					$where .= $where_query;
				}
			}
		}

		is_null($where) ? $where = ' 1 ' . Cubix_Countries::blacklistCountryWhereCase() : $where .= Cubix_Countries::blacklistCountryWhereCase();

		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id, e.is_inactive,
				ct.title_' . $lng . ' AS city, e.rates, e.incall_type, e.outcall_type, e.is_new,
				e.verified_status, e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_application::getId() . ' AS application_id,
				IF (sei.primary_id IS NULL, IF(se1.id IS NOT NULL, se1.title_' . $lng . ', se2.title_' . $lng . '), sei.title_' . $lng . ') AS alt,
				e.hit_count, e.slogan, e.incall_price, e.comment_count, e.review_count, e.last_hand_verification_date,
				e.date_last_modified, e.is_pornstar,
				IF(FIND_IN_SET(14, e.products) > 0, 1, 0 ) AS is_premium_plus,
				eic.is_featured * (FLOOR(RAND() * 10) + 1) as is_featured,
				eic.is_premium, e.is_online, e.about_' . $lng . ' AS about, UNIX_TIMESTAMP(e.date_last_modified) AS date_last_modified
				' .
				(! $is_upcoming ? ', e.tour_date_from, e.tour_date_to' : // if
				($is_upcoming ? ', ut.tour_date_from, ut.tour_date_to' : '')) // else
				. '
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			LEFT JOIN seo_entity_instances sei ON sei.primary_id = eic.escort_id
			LEFT JOIN seo_entities se1 ON se1.id = sei.entity_id
			LEFT JOIN seo_entities se2 ON se2.slug = "escort" AND se2.application_id = 2
			LEFT JOIN escort_services es ON es.escort_id = e.id
			' . $j . '
			LEFT JOIN escort_keywords ek ON ek.escort_id = e.id
			' . ($is_upcoming ? 'INNER JOIN upcoming_tours ut ON ut.id = eic.escort_id AND ut.tour_city_id = eic.city_id' : '') . '
			' . (! is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.escort_id
			ORDER BY e.is_inactive ASC, ' . $order . '
			LIMIT ' . $limit . '
		';

		$escorts = self::db()->fetchAll($sql);
		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');

		// <editor-fold defaultstate="collapsed" desc="Make Escort Alt for SEO">
		$tpl_data = array(
			'app_title' => Cubix_Application::getById()->title
		);

		foreach ( $escorts as $i =>  $escort ) {
			$additional = array(
				'showname' => $escort->showname,
				'city' => $escort->city
			);

			foreach ( $tpl_data + $additional as $tpl_var => $tpl_value ) {
				$escorts[$i]->alt = str_replace('%' . $tpl_var . '%', $tpl_value, $escorts[$i]->alt);
			}
		}
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
		$sid = 'sedcard_paging_' . Cubix_Application::getId();
		$sess = new Zend_Session_Namespace($sid);

		if ( ! $is_upcoming ) {
			$filter_name = 'tours_list';
			$sess->tours_list = array();
			foreach ( $escorts as $escort ) {
				$sess->tours_list[] = $escort->showname;
			}
		}
		else {
			$filter_name = 'up_tours_list';
			$sess->up_tours_list = array();
			foreach ( $escorts as $escort ) {
				$sess->up_tours_list[] = $escort->showname;
			}
		}

		$sess->{$filter_name . '_criterias'} = array(
			'is_upcoming'	=> $is_upcoming,
			'filter'		=> $filter,
			'sort'		=> $ordering,
			'page'			=> $page
		);
		// </editor-fold>

		return $escorts;
	}

	public static function getAgencyEscorts($filter, $ordering = 'random', $page = 1, $page_size = 50, &$count = 0)
	{
		$lng = Cubix_I18n::getLang();

		$page = (int) $page;
		if ( $page < 1 ) $page = 1;
		$limit = ($page_size * ($page - 1)) . ', ' . $page_size;


		$where = self::getWhereClause($filter, true);
		$order = self::_mapSorting($ordering);

		$group_by = " GROUP BY eic.escort_id ";

		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id, e.is_pornstar, e.is_inactive,
				UNIX_TIMESTAMP(e.date_activated) AS date_activated, ct.slug AS city_slug, ct.id AS city_id, ct.title_' . $lng . ' AS city, e.rates,
				e.incall_type, e.outcall_type, e.is_new, cr.id AS country_id, cr.slug AS country_slug, cr.title_' . $lng . ' AS country, e.verified_status,
				e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, 
				e.incall_price, eic.is_premium, e.is_vip, e.hh_is_active,
				IF(FIND_IN_SET(14, e.products) > 0, 1, 0 ) AS is_premium_plus,
				eic.is_featured * (FLOOR(RAND() * 10) + 1) as is_featured,
				e.is_suspicious, e.is_online, ct.id AS city_id
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			INNER JOIN countries cr ON cr.id = ct.country_id
			' . (! is_null($where) ? 'WHERE ' . $where : '') . '
			' . $group_by . '
			ORDER BY is_inactive ASC, ' . $order . '
			LIMIT ' . $limit . '
		';

		$escorts = self::db()->fetchAll($sql);

		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');

		return $escorts;
	}

	private static function _mapSorting($param)
	{
		$map = array(
			'price-asc' => ' eic.is_premium DESC, e.incall_price ASC, is_premium_plus DESC, is_featured DESC',
			'price-desc' => 'eic.is_premium DESC, e.incall_price DESC ,is_premium_plus DESC, is_featured DESC',
			'top-listing' => 'eic.is_premium DESC, is_premium_plus DESC, is_featured DESC, eic.last_15days_active DESC ,eic.ordering DESC ' ,
			'alpha' => 'eic.is_premium DESC, is_premium_plus DESC, e.showname ASC',
			'most-viewed' => 'eic.is_premium DESC, e.hit_count DESC, is_premium_plus DESC',
//			'newest' => 'eic.is_premium DESC, is_premium_plus DESC, e.is_new DESC, is_featured DESC, COALESCE(e.date_activated, e.date_registered) DESC',
            'newest' => 'COALESCE(e.date_activated, e.date_registered) DESC, eic.is_premium DESC, is_premium_plus DESC, e.is_new DESC, is_featured DESC',
			//eic.is_premium DESC, is_premium_plus DESC,
			'new-escorts-list' => ' e.is_new DESC, is_featured DESC, COALESCE(e.date_activated, e.date_registered) DESC',
			'date-activated' => 'eic.is_premium DESC, is_premium_plus DESC,  e.date_activated DESC, is_featured DESC',
			'last-modified' => 'eic.is_premium DESC, e.date_last_modified DESC, is_premium_plus DESC',
			'city-asc' => 'eic.is_premium DESC, is_premium_plus DESC, ct.slug ASC, is_featured DESC, eic.last_15days_active DESC'
		);

		$order = 'e.ordering DESC';
		if ( isset($map[$param]) ) {
			$order = $map[$param];
		}
		elseif ( false !== strpos($param, 'FIELD') ) {
			$order = $param;
		}

        return $order;
	}

	public static function getPrevNext($showname)
	{
		return array(null, null);
		// Retrieve criterias from session
		$sid = 'v2_sedcard_paging_' . Cubix_Application::getId();
		$sess = new Zend_Session_Namespace($sid);

		$page = $sess->page;
		$page_size = $sess->page_size;
		$pages_count = $sess->pages_count;

		list($filter, $ordering) = $sess->criteria;
		foreach ( $sess->escorts as $i => $name ) {
			if ( $name == $showname ) {
				$prev_i = $i - 1;
				$next_i = $i + 1;

				break;
			}
		}

		echo "Prev_i: $prev_i, Next_i: $next_i\n";
		print_r($sess->escorts);

		if ( ! isset($prev_i) && ! isset($next_i) ) {
			return array(null, null);
		}

		// This is first escort, we need to fetch data from prev page
		if ( isset($prev_i) && (! isset($sess->escorts[$prev_i]) || $prev_i < 0) && $page > 1 ) {
			$sess->escorts = array_slice($sess->escorts, 0, $page_size * 2);

			$escorts = call_user_func_array(
				array(__CLASS__, $sess->callback),
				array($filter, $ordering, $page - 1, $page_size)
			);
			foreach ( $escorts as $i => $escort ) {
				$escorts[$i] = $escort->showname;
			}

			$sess->escorts = array_merge($escorts, $sess->escorts);

			$prev_i = $page_size - 1;
		}


		echo "Escorts after 'first' logic\n";
		echo "Prev_i: $prev_i, Next_i: $next_i\n";
		print_r($sess->escorts);

		// If this last escort on page, we need to fetch data from next page
		if ( isset($next_i) &&  (! isset($sess->escorts[$next_i]) || $next_i > count($sess->escorts) - 1 ) && $page < $pages_count ) {
			if ( count($sess->escorts) >= $page_size * 3 ) {
				$sess->escorts = array_slice($sess->escorts, $page_size, $page_size * 2);
				$next_i -= $page_size;
			}

			$escorts = call_user_func_array(
				array(__CLASS__, $sess->callback),
				array($filter, $ordering, $page + 1, $page_size)
			);
			foreach ( $escorts as $i => $escort ) {
				$escorts[$i] = $escort->showname;
			}

			$sess->escorts = array_merge($sess->escorts, $escorts);
		}

		echo "Escorts after 'last' logic\n";
		echo "Prev_i: $prev_i, Next_i: $next_i\n";
		print_r($sess->escorts);

		return array(isset($prev_i) ? $sess->escorts[$prev_i] : null, isset($next_i) ? $sess->escorts[$next_i] : null);
	}
}
