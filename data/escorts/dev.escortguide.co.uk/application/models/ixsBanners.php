<?php
class Model_ixsBanners extends Cubix_Model
{
	public function insert($images, $zone){
		
		$sql = "INSERT INTO ixs_banners ( `id`, `ixs_id`, `zone_id`, `zone_rotate`, `zone_rotation`,
										 `banner_id`, `filename`, `target`,`title` , `link`, `cmp_id`,
										 `user_id`, `size`, `md5_hash`, `sort`)";
		$sql_value = 'VALUES ';	

		foreach ($images as $image) {
			
			$image_arr = (array) $image;
			$image_arr['zone_id'] = $zone;
		
			$image_data[0] = (isset($image_arr['id']) ? $image_arr['id']  : '' );
			$image_data[1] = (isset($image_arr['zone_id']) ? $image_arr['zone_id']  : '' );
			$image_data[2] = (isset($image_arr['zone_rotate']) ? $image_arr['zone_rotate']  : '' );
			$image_data[3] = (isset($image_arr['zone_rotation']) ? $image_arr['zone_rotation']  : '' );
			$image_data[4] = (isset($image_arr['banner_id']) ? $image_arr['banner_id']  : '' );
			$image_data[5] = (isset($image_arr['filename']) ? $image_arr['filename']  : '' );
			$image_data[6] = (isset($image_arr['target']) ? $image_arr['target']  : '' );
			$image_data[7] = (isset($image_arr['title']) ? $image_arr['title']  : 'notitle' );
			$image_data[8] = (isset($image_arr['link']) ? $image_arr['link']  : '' );
			$image_data[9] = (isset($image_arr['cmp_id']) ? $image_arr['cmp_id']  : '' );
			$image_data[10] = (isset($image_arr['user_id']) ? $image_arr['user_id']  : '' );
			$image_data[11] = (isset($image_arr['size']) ? $image_arr['size']  : '' );
			$image_data[12] = (isset($image_arr['md5_hash']) ? $image_arr['md5_hash']  : '' );
			$image_data[13] = (isset($image_arr['sort']) ? $image_arr['sort']  : '' );
		
			$sql_value .='(null,';
			//$last_key = end(array_keys($image));
			
			foreach ($image_data as $key => $value) {
				$sql_value .= '"'.$value.'"';
				if ($key < 13) {
			        $sql_value .= ',';
			    }
			}
			$sql_value .='),';

			unset($image_data[$key]);
		}
			
		$sql .= $sql_value;

		
		try{
			$this->getAdapter()->query(rtrim($sql, ','));
			return TRUE;
		}catch(Exception $e ){
			 var_dump($e->getMessage());
		}
	}

    public function swapTempTable()
    {
        $this->getAdapter()->query("DROP TABLE IF EXISTS `ixs_banners`");
        $this->getAdapter()->query("RENAME TABLE `ixs_banners_tmp` TO `ixs_banners`");
    }

    public function fill(Array $seeds)
    {
        $this->getAdapter()->query("DELETE FROM `ixs_banners`");

        $table = 'ixs_banners';
        $sql = "INSERT INTO $table ( `id`, `type`, `frozen_position`, `frozen_country_id`, `frozen_city_id`,
										 `ixs_id`, `zone_id`, `zone_rotate`,`zone_rotation` , `banner_id`, `filename`,
										 `target`, `title`, `link`, `cmp_id`, `user_id`, `size`, `md5_hash`, `is_follow` ,`sort`, `low_priority`, `frozen_region_id`) VALUES ";
        foreach ($seeds as $seed) {
            $sql .= '(';
            foreach ($seed as $key => $value) {
                $sql .= '"' . $value . '",';
            }
            $sql = substr($sql, 0, -1); // Remove last comma
            $sql .= '),';
        }
        $sql = substr($sql, 0, -1); // Remove last comma

        try {
            $this->getAdapter()->query($sql);
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }

        /* creating notification if ixs-banner do not have dofollow */
        if (Cubix_Application::getId() == APP_EG_CO_UK) {

            /* checkincg one banner is missing or not */
            $checkingSqlForTargetBanner = 'SELECT * FROM ixs_banners WHERE `link` = "https://www.asiandollslondon.co.uk/portfolio.html"';

            $banner = self::db()->fetchAll($checkingSqlForTargetBanner);
            if (!$banner || is_null($banner) || empty($banner)) {

                $emails = [
                    'aramnatsyan018@gmail.com',
                    'mihranhayrapetyan@gmail.com',
                    'brandy@escortguide.co.uk'
                ];

                foreach ($emails as $email) {
                    mail($email, 'warning', 'https://www.asiandollslondon.co.uk banner is missing');
                }
            }
            /*.........................................................*/

            $webSites = [
                'https://www.bossagency.co.uk/',
                'https://www.elitemanchesterescorts.co.uk/',
                'https://www.secretbabes.co.uk/',
                'https://www.shushescorts.co.uk/',
                'https://www.hydeclubescorts.com',
                'https://www.diamondgirls.co.uk',
                'https://www.cheshirecompanions.com/locations/manchester-escorts',
                'https://www.bossagency.co.uk',
                'https://www.babylongirls.co.uk'
            ];

            $sql = [];
            foreach ($webSites as $key => $webSite) {
                array_push($sql, '(link LIKE "%' . $webSite . '%" AND `is_follow` = 0)');
            }

            $sql = 'SELECT * FROM ixs_banners WHERE '.implode(" OR ", $sql);

            $banners = self::db()->fetchAll($sql);

            if ($banners) {
                $warningBanners = [];
                foreach ($banners as $key => $banner) {
                    array_push($warningBanners, $banner->link);
                }

                $massage = '';
                foreach ($warningBanners as $warningBanner) {
                    $massage .= $warningBanner.' /------/  ';
                }

                $emails = [
                    'aramnatsyan018@gmail.com',
                    'mihranhayrapetyan@gmail.com',
                    'brandy@escortguide.co.uk'
                ];

                foreach ($emails as $email) {
                    mail($email, 'warning banners', $massage);
                }

            }
        }
    }

    public function createTmpTable()
    {
        $this->getAdapter()->query("DROP TABLE IF EXISTS `ixs_banners_tmp`");

        $sql = "
        CREATE TABLE `ixs_banners_tmp`  (
            `id` int(11) NOT NULL AUTO_INCREMENT,
          `type` enum('rotating','fixed') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'rotating',
          `frozen_position` int(3) NULL DEFAULT NULL,
          `frozen_country_id` int(11) NULL DEFAULT NULL,
          `frozen_city_id` int(11) NULL DEFAULT NULL,
          `ixs_id` int(11) NULL DEFAULT NULL,
          `zone_id` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
          `zone_rotate` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
          `zone_rotation` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
          `banner_id` int(11) NULL DEFAULT NULL,
          `filename` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
          `target` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
          `title` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
          `link` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
          `cmp_id` int(11) NULL DEFAULT NULL,
          `user_id` int(11) NULL DEFAULT NULL,
          `size` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
          `md5_hash` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
          `is_follow` int(11) NULL DEFAULT 0,
          `sort` int(3) NULL DEFAULT NULL,
          PRIMARY KEY (`id`) USING BTREE,
          UNIQUE INDEX `unique_banner_id`(`banner_id`) USING BTREE
        ) ENGINE = InnoDB AUTO_INCREMENT = 2637 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;";

        return $this->getAdapter()->query($sql);
    }
	
	static public function get($zone_id = null, $user_id = null, $size = null)
	{
		$where = array(
			'ixs.zone_id = ?' => $zone_id,
			'ixs.user_id = ?' => $user_id,
			'ixs.size = ?' => $size
		);

		$where = self::getWhereClause($where, true);

		$highPriorityBannersSql = 'SELECT * FROM ixs_banners ixs ' . (!is_null($where) ? 'WHERE ' . $where : '').'AND `low_priority` <> 1 ORDER BY RAND()';
        $highPriorityBanners = parent::_fetchAll($highPriorityBannersSql);

		$lowPriorityBannersSql = 'SELECT * FROM ixs_banners ixs ' . (!is_null($where) ? 'WHERE ' . $where : '').'AND `low_priority` = 1 ORDER BY RAND()';
        $lowPriorityBanners = parent::_fetchAll($lowPriorityBannersSql);

		return array_merge($highPriorityBanners, $lowPriorityBanners);
	}

	public static function get_right_sidebar_banners($zone_id = null, $rand_count = false )
	{
		
		if ( isset($zone_id) ) {
			$model_ixs = new Model_ixsBanners();
			$banners = $model_ixs->get($zone_id);

			if($rand_count){
				$random_banners = array();
				$random_els = array_rand($banners, $rand_count);
				if($rand_count == 1){
					$random_banners[] = $banners[$random_els];
					return $random_banners;
				}else{
					foreach($random_els as $random_el){
						$random_banners[] = $banners[$random_el];
					}	
					$banners = $random_banners;	
				}
					
			}
			
			return $banners;

		}

		return "";
	}

	public function truncate(){
		$sql = 'truncate table ixs_banners';
		if($this->getAdapter()->query($sql)){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function copyImages($imgs){
		$copycount = 0;
		foreach ($imgs as $img) {
			
			if (!is_file('./images/ixsBanners/'.$img['filename'])) {

				$sourceurl = 'https://ixspublic.com/uploads/'.$img['filename'];
				
				$result = copy($sourceurl, './images/ixsBanners/'.$img['filename']);
				if(!$result){
					return 'image copy error';
				}
			}
			$copycount++;
		}
		return $copycount;
	}
}
