<?php

class Model_PunterBoard extends Cubix_Model
{
	public function getAll($page = 1, $per_page = 5){

		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$user = Model_Users::getCurrent();
		
		return $client->call('Punter.getAllPunters', array('user_id' => $user->id, 'page' => $page, 'per_page' => $per_page));
	}
}
