<?php

class Model_VipHardcodes extends Cubix_Model
{
    protected $_table = 'vip_hardcodes';

    /**
     * @return mixed
     */
    static public function getAll()
    {
        $cache_key = Cubix_Application::getId() . '_vip_hardcodes';

        // CACHE DISABLED
        if ( 1 or ! $places = self::cache()->load($cache_key) ) {
            $places = self::db()->fetchAll('
				SELECT *
				FROM vip_hardcodes
				where city_id IS NOT NULL 
			');
            self::cache()->save($places, $cache_key, array(),
                self::config()->currencies->cache_lifetime);
        }

        return $places;
    }
}