<?php

class Model_PaymentMethodes extends Cubix_Model
{

    public function __construct()
    {
        $this->db = Zend_Registry::get('db');
    }

	public function getActivePaymentMethodes($app_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
        $active_payment_methodes = $client->call('Application.getActivePaymentMethodes', array($app_id));
        $activ_payments = array();
        if ( count($active_payment_methodes) )
        {
            foreach ($active_payment_methodes as $payment )
            {
                $activ_payments[] = $payment['slug'];
            }
        }
		return $activ_payments;
	}
	
}
