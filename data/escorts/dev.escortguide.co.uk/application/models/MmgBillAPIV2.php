<?php
class Model_MmgBillAPIV2
{
    private $payment_link = 'https://secure.f5fin.com/payment/?';
    private $merchantId = '';
    private $secret_key = '';
    private $ts = 'd'; //Stands for dynamic system
    private $txn_type = 'SALE'; 
    private $currencyCode = '826';

    public function __construct()
    {
        //Test acccount opon c2 below
        // $this->merchantId = 'M1572';
        // $this->secret_key = 'qaRxBH6laojLkfkIbbUiRupyJLL0rcfv';

        
       $this->merchantId = 'M1576';
       $this->secret_key = 'cdsY9vYSrmKxTzcgC34nsfc41uGpVTuJ';
    }

    public function getHostedPageUrl($amount, $transaction_id, $postback_url)
    {
        $amount = $amount * 100;

        $array_request = array(
            'mid' => $this->merchantId,
            'ts' => $this->ts,
            'ti_mer' => $transaction_id,
            'txn_type' => $this->txn_type,
            'cu_num' => $this->currencyCode,
            'amc' => $amount,
            'surl' => $postback_url,
            //ENABLE WHEN TESTING
            //'c2' => 'escortguide.co.uk'
        );

        // return json_encode($array_request); die;

        /* create the hash signature */
        $sh = $this->create_hash($array_request, $this->secret_key);

        /* create the payment link */
        $payment_link = $this->create_payment_link($array_request, $this->payment_link, $sh);

        return $payment_link;
    }

    /* function to create the hash signature */
    private function create_hash ($array_request, $secret_key)
    {
        $hash_string = "";
        ksort($array_request);
        foreach ($array_request as $key => $value) {
            $hash_string .= "|" . $key . $value;
        }

        return hash_hmac("sha256", $hash_string, $secret_key, false);
    }

    /* function to create payment link */
    private function create_payment_link($array_request, $payment_link, $sh)
    {
        foreach ($array_request as $key => $value) {
            $payment_link .= $key . "=" . $value . "&";
        }

        return $payment_link .= "sh=" . $sh;
    }


}
