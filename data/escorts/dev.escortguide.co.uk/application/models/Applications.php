<?php

class Model_Applications
{
	public static function getAll()
	{
		$db = Zend_Registry::get('db');
		
		return $db->query('
			SELECT a.id, a.host, c.iso AS country_iso, c.' . Cubix_I18n::getTblField('title') . ' AS country_title
			FROM applications a
			INNER JOIN countries c ON c.id = a.country_id
			ORDER BY c.iso DESC
		')->fetchAll();
	}

	public static function getDefaultCurrencyTitle()
	{
		$db = Zend_Registry::get('db');
		$currency = $db->fetchOne('
				SELECT currency_title
				FROM applications
				WHERE id = '.Cubix_Application::getId()
			);
		return $currency;
	}

	public static function clearCache()
	{
		// Clear whole cache except hits count
		$cache = Zend_Registry::get('cache');
		$hits_cache = $cache->load(Model_Escorts::HITS_CACHE_KEY);
		$banners = $cache->load('v2_banners_cache');
		$latest_actions_date = $cache->load('latest_action_date_eg_co_uk');
		$cache->clean();
		if ( $hits_cache ) {
			$cache->save($hits_cache, Model_Escorts::HITS_CACHE_KEY);
		}
		if ( $banners ) {
			$cache->save($banners, 'v2_banners_cache');
		}
		if ( $latest_actions_date ) {
			$cache->save($latest_actions_date, 'latest_action_date_eg_co_uk');
		}

		$sid = 'sedcard_paging_' . Cubix_Application::getId();
		$ses = new Zend_Session_Namespace($sid);
		$ses->unsetAll();

		return 'Cache Cleared';
	}
}
