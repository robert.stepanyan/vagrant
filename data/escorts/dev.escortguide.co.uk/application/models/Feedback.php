<?php

class Model_Feedback extends Cubix_Model
{
	public function addFeedback($data,$isAgency = false)
	{
		$client = Cubix_Api::getInstance();
        
        $feedbackdata = array(
            "name" => @$data['name'],
            "email" => @$data['email'],
            "to_addr" => @$data['to_addr'],
            "message" => @$data['message'],
            "flag" => @$data['flag'],
            "status" => @$data['status'],
            "application_id" => @$data['application_id']
        );

        if(!$isAgency){
            $feedbackdata["escort_showname"] = @$data['to_name'];
            $feedbackdata["escort_id"] = @$data['to'];
        }else{
            $feedbackdata["agency_name"] = @$data['to_name'];
            $feedbackdata["agency_id"] = @$data['to'];
        }
		
		// ip
		$ip = Cubix_Geoip::getIP();

		$feedbackdata['ip'] = $ip;
		//
		
		// city
		$loc = Cubix_Geoip::getClientLocation($ip);
		$city = null;
		
		if (is_array($loc))
			$city = $loc['city'];
		
		$feedbackdata['city'] = $city;
		//
        
		$comment = $client->call('addFeedback', array($feedbackdata) );
	}

	public function getBySenderEmail($email, $from_date = null)
	{
		$data = array(
			"email" => $email,
			"from_date" => $from_date
		);

		$client_ = new Cubix_Api_XmlRpc_Client();
		return $client_->call('ClassifiedAds.getFeedbacksBySenderEmail', array($data) );
	}

	public function getBySenderAndReceiverEmails($email, $to_addr, $from_date = null)
	{
		$data = array(
			"email" => $email,
			"to_addr" => $to_addr,
			"from_date" => $from_date
		);

		$client_ = new Cubix_Api_XmlRpc_Client();
		return $client_->call('ClassifiedAds.getFeedbacksBySenderEmail', array($data) );
	}
}
