<?php

class Model_Users extends Cubix_Model
{
	protected $_table = 'users';
	protected $_itemClass = 'Model_UserItem';
	
	public function getAll()
	{
		
	}
	
	public function getByUsername($username)
	{
		return $this->_fetchRow('
			SELECT id FROM users WHERE username = ?
		', $username);
	}
	
	public function getByUsernamePassword($username, $password)
	{
		$ip = Cubix_Geoip::getIP();

		$client = new Cubix_Api_XmlRpc_Client();
		$user = $client->call('Users.getByUsernamePassword', array($username, $password, $ip));
//        var_dump($user);die;
		if ( is_array($user) && isset($user['error']) ) {
			Cubix_Debug::log(print_r($user['error'], true), 'ERROR');
			die;
		}

		if ( ! $user ) return null;

		return new Model_UserItem($user);
		
		
		/*$item = $this->_fetchRow('
			SELECT id, username, email, status, user_type
			FROM users
			WHERE username = ? AND password = ?
		', array($username, md5($password)));
		
		return $item;*/
	}

	public function getById($id)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$user = $client->call('Users.getDataById', array($id));

		if ( ! $user ) {
			return null;
		}

		return new Model_UserItem($user);
	}
	
	public function getByEmail($email)
	{
		return $this->_fetchRow('
			SELECT id FROM users WHERE email = ?
		', $email);
	}
	
	public function save(Model_UserItem $item)
	{
		$ip = Cubix_Geoip::getIP();
		
		$hash = md5(microtime() * rand());
		$item->registered_ip = $ip;
		
		$item->setActivationHash($hash);
		$item->setApplicationId(Cubix_Application::getId());

		
		$client = new Cubix_Api_XmlRpc_Client();
		$item->new_user_id = $client->call('Users.save', array( (array) $item ));
		
		//parent::save($item);
		
		return $item;
	}
	
	public function activate($email, $hash)
	{
		$user = Cubix_Api_XmlRpc_Client::getInstance()->call('Users.activate', array($email, $hash));
				
		return $user;
	}
	
	public function resetPassword($email)
	{
		$user = Cubix_Api_XmlRpc_Client::getInstance()->call('Users.resetPassword', array($email));

		return $user;
		/*$user = self::_fetchRow('
			SELECT id, username, email FROM users WHERE email = ?
		', $email);
		
		if ( ! $user ) {
			return NULL;
		}
		
		$password = md5(microtime() * rand());
		$password = substr($password, 0, 10);
		
		$user->setPassword(md5($password));
		
		parent::save($user);
		
		$user->setPassword($password);
		
		return $user;*/
	}
	public function forgotPassword($email)
	{
		$user = Cubix_Api_XmlRpc_Client::getInstance()->call('Users.forgotPassword', array($email));

		return $user;
	}
	
	/**
	 * @var Zend_Session_Namespace
	 */
	protected static $_session;
	
	/**
	 * @return Zend_Session_Namespace
	 */
	protected static function _getSession()
	{
		return new Zend_Session_Namespace('auth');
	}
	
	public static function setCurrent($user)
	{
		self::_getSession()->current_user = $user;
	}

	/**
	 *
	 * @return Model_UserItem
	 */
	public static function getCurrent()
	{
		return self::_getSession()->current_user;
	}

	public static function setLastRefreshTime($user)
	{
		$is_exist = parent::getAdapter()->fetchOne('SELECT TRUE FROM users_last_refresh_time WHERE user_id = ?', $user->id);

		if ( $is_exist ) {			
			parent::getAdapter()->update('users_last_refresh_time', array(
				'refresh_date' => new Zend_Db_Expr('NOW()'),
				'user_agent' => $_SERVER['HTTP_USER_AGENT'],
				'session_id' => session_id()
			), parent::getAdapter()->quoteInto('user_id = ?', $user->id));
		}
		else {
			$data = array(
				'user_id' => $user->id,
				'user_agent' => $_SERVER['HTTP_USER_AGENT'],
				'session_id' => session_id()
			);

			if ( $user->user_type == 'agency' ) {
				$data = array_merge($data, array('agency_id' => $user->agency_data['agency_id']));
			}
			else if ( $user->user_type == 'escort' ) {
				$data = array_merge($data, array('escort_id' => $user->escort_data['escort_id']));
			}

			parent::getAdapter()->insert('users_last_refresh_time', $data);
		}
	}

	public static function isOnlineNow($escort_id = null, $agency_id = null)
	{
		$field = " agency_id = ?";
		$param = $agency_id;
		if ( ! is_null($escort_id) ) {
			$field = " escort_id = ?";
			$param = $escort_id;
		}

		$last_refresh_time = parent::getAdapter()->fetchOne("SELECT UNIX_TIMESTAMP(refresh_date) AS refresh_date FROM users_last_refresh_time WHERE {$field}", $param);

		if ( $last_refresh_time ) {
			$to_time = time();
			$from_time = $last_refresh_time;
			$diff_minutes = round(abs($to_time - $from_time) / 60, 0);

			if ( $diff_minutes >= 10 ) {
				return false;
			}
			else {
				return true;
			}
		}

		return false;
	}

    public function get_last_login_date( $userid ){
        return Cubix_Api_XmlRpc_Client::getInstance()->call('Users.get_last_login_date', array($userid));
    }

    public function getByToken($secure_token)
    {
        $client = Cubix_Api_XmlRpc_Client::getInstance();
        $user = $client->call('Users.getUserByToken', array($secure_token));

        if ( ! $user ) {
            return null;
        }

        return new Model_UserItem($user);
    }

    public function getLastUserId()
    {
        $client = Cubix_Api_XmlRpc_Client::getInstance();
        return $client->call('Users.getLastUserId');
    }
}
