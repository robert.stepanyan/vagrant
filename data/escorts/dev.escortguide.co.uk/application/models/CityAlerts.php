<?php

class Model_CityAlerts extends Cubix_Model
{
	public function getAll($user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('CityAlerts.getAll', array($user_id));
	}
	
	public function remove($id, $user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('CityAlerts.remove', array($id, $user_id));
	}
	
	public function add($data)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('CityAlerts.add', array($data));
	}
	
	public function checkCityExists($city_id, $user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('CityAlerts.checkCityExists', array($city_id, $user_id));
	}
}
