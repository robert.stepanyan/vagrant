<?php

class Model_ClassifiedAds extends Cubix_Model
{
	public function getAllCitiesForFilter($country_id)
	{
		return parent::_fetchAll('
			SELECT c.id, ' . Cubix_I18n::getTblField('c.title') . ' AS title, ' . Cubix_I18n::getTblField('r.title') . ' AS region_title, co.has_regions 
			FROM cities c
			INNER JOIN classified_ads_cities ca ON ca.city_id = c.id
			LEFT JOIN regions r ON r.id = c.region_id
			LEFT JOIN countries co ON co.id = c.country_id
			WHERE c.country_id = ?
			GROUP BY c.id
			ORDER BY title ASC
		', $country_id);
	}
	
	//protected $_table = 'comments';
	//protected $_itemClass = 'Model_CommentItem';
	public function getList($filter = array(), $page = 1, $perPage = 20, &$count = null)
	{
		$defines = $this->view->defines = Zend_Registry::get('defines');
		if( ! $page ) $page = 1;
		
		$limit = ' LIMIT ' . ($page - 1) * $perPage . ', ' . $perPage;
		$fields = '';
		$where = '';
		$order = '';
								
		$where = '';
		$order = ' is_premium DESC, approvation_date DESC ';
		
		if ( isset($filter['category']) && $filter['category'] ) {
			$where .= parent::quote(' AND ca.category_id = ?', $filter['category']);
		}
		
		if ( isset($filter['city']) && $filter['city'] ) {
			$where .= parent::quote(' AND cac.city_id = ?', $filter['city']);
		}
		
		if ( isset($filter['text']) && $filter['text'] ) {
			$where .= parent::quote(' AND ca.search_text LIKE ?', '%' . $filter['text'] . '%');
		}
		
		$sql = "
			SELECT SQL_CALC_FOUND_ROWS ca.*, c." . Cubix_I18n::getTblField('title') . " AS city_title
			FROM classified_ads ca
			INNER JOIN classified_ads_cities cac ON cac.ad_id = ca.id
			INNER JOIN cities c ON c.id = cac.city_id
			WHERE 1 {$where}
			GROUP BY ca.id
			ORDER BY {$order}
			{$limit}
		";
//		echo $sql;die;
		$items = $this->getAdapter()->fetchAll($sql);

		$count = $this->getAdapter()->fetchOne('SELECT FOUND_ROWS()');
		
		if ( count($items) ) {
			foreach ( $items as $i => $item ) {
				
				$items[$i]->category = $defines['classified_ad_categories'][$item->category_id]; 
				$items[$i]->images = $this->getAdapter()->fetchAll('
					SELECT 
						id AS image_id, hash, ext, ' . Cubix_Application::getId() . ' AS application_id 
					FROM classified_ads_images
					WHERE ad_id = ?
				', array($item->id));
			}
		}
		
		return $items;
	}	
	
	public function save($data)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$ad_id = $client->call('ClassifiedAds.save', array($data));
		
		return $ad_id;
	}
	
	public function get($id)
	{
		$defines = $this->view->defines = Zend_Registry::get('defines');
		$sql = "
			SELECT
				ca.*, c." . Cubix_I18n::getTblField('title') . " AS city_title,
				IF( cpc.phone_prefix is not null,CONCAT(cpc.phone_prefix,'',ca.phone) , ca.phone ) as phone
			FROM classified_ads ca
			INNER JOIN classified_ads_cities cac ON cac.ad_id = ca.id
			INNER JOIN cities c ON c.id = cac.city_id
			LEFT JOIN countries_phone_code cpc ON cpc.id = ca.country_prefix
			
			WHERE ca.id = ?
		";
		
		$ad = $this->getAdapter()->fetchRow($sql, array($id));

		$ad_sql = '
			SELECT
				id AS image_id, hash, ext, ' . Cubix_Application::getId() . ' AS application_id 
			FROM classified_ads_images
			WHERE ad_id = ?
		';
		
		$ad->images = $this->getAdapter()->fetchAll($ad_sql, array($ad->id));
		$ad->category = $defines['classified_ad_categories'][$ad->category_id]; 
		
		return $ad;
	}

    public function updateViewCount($ad_id)
    {
        $this->getAdapter()->query('UPDATE classified_ads SET view_count = view_count + 1 WHERE id = ?', array($ad_id));

        $client = Cubix_Api_XmlRpc_Client::getInstance();
        $client->call('ClassifiedAds.updateViewCount', array($ad_id));
    }

    public function ads_count_by_categories()
    {
        $sql = "SELECT  category_id, count(id) as count from classified_ads group by category_id";
		$cats = $this->getAdapter()->fetchAll($sql, array($id));
		$ads_count_by_cat = array();
		foreach ($cats as $cat) {
			$ads_count_by_cat[$cat->category_id] = $cat->count;
		}
		return $ads_count_by_cat;
    }

    public function adHavingCities($category_id)
    {
        $sql = "SELECT
	            c.title_en,
	            ca.id,
	            c.id
                FROM
	            classified_ads ca
	            INNER JOIN classified_ads_cities cac ON cac.ad_id = ca.id
	            INNER JOIN cities c ON c.id = cac.city_id
                WHERE
	            ca.category_id = ?
	            GROUP BY c.title_en";
        $cities = $this->getAdapter()->fetchAll($sql, array($category_id));
        $adHavingCities = array();
        foreach ($cities as $city) {
            $adHavingCities[$city->title_en] = $city;
        }
        return $adHavingCities;
    }

    public function adHavingCategories($city_id)
    {
        $sql = "SELECT
	            ca.id,
	            c.id,
	            category_id
                FROM
	            classified_ads ca
	            INNER JOIN classified_ads_cities cac ON cac.ad_id = ca.id
	            INNER JOIN cities c ON c.id = cac.city_id
                WHERE
	            c.id = ?
	            GROUP BY category_id";
        $categories = $this->getAdapter()->fetchAll($sql, array($city_id));
        $adHavingCategories = array();
        foreach ($categories as $category) {
            $adHavingCategories[$category->category_id] = $category;
        }
        return $adHavingCategories;
    }
}
