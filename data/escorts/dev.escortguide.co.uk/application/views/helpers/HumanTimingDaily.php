<?php
class Zend_View_Helper_HumanTimingDaily extends Zend_View_Helper_Abstract
{
	function humanTimingDaily($time){

	    $time = time() - $time; // to get the time since that moment
	    $time = ( $time < 1 ) ? 1 : $time;
	  
	    $tokens = array (
			31536000 => 'y',
			2592000 => 'm’th',
			604800 => 'w',
			86400 => 'd',
			3600 => 'h',
			60 => 'm',
			1 => 's'
	    );

	    foreach ( $tokens as $unit => $text ) {
	        if ( $time < $unit ) continue;
	        $numberOfUnits = floor($time / $unit);

	        if($format){
	        	 // You can add "s" when unit is multiple 
	        	$text .= (($numberOfUnits>1)?'s':'');
	        }
	       
	      	$howlong = $numberOfUnits.$text.' ago';
	      	
	      	if($text == 's' || $text == 'm' || $text == 'h'){
				$howlong = 'Escorts added today';
	      	}

	      	if($text == 'd'){
	      		if($numberOfUnits>1){
	      			$howlong = 'Escorts added '.$numberOfUnits.' days ago';
	      		}else{
	      			$howlong = 'Escorts added yesterday';
	      		}
	      	}

	      	if($text == 'w'){
	      		if($numberOfUnits>1){
	      			$howlong = 'Escorts added '.$numberOfUnits.' weeks ago';
	      		}else{
	      			$howlong = 'Escorts added one week ago';
	      		}
	      	}

	      	if($text == 'm’th'){
	      		if($numberOfUnits>1){
	      			$howlong = 'Escorts added '.$numberOfUnits.' months ago';
	      		}else{
	      			$howlong = 'Escorts added one month ago';
	      		}
	      	}

	      	if($text == 'y'){
	      		if($numberOfUnits>1){
	      			$howlong = 'Escorts added '.$numberOfUnits.' years ago';
	      		}else{
	      			$howlong = 'Escorts added one year ago';
	      		}
	      	}

	        return $howlong;
	    }
	}
}