<?php
class Zend_View_Helper_BubbleTextsWidget extends Zend_View_Helper_Abstract
{
	public function bubbleTextsWidget($page = 1, $per_page = 14)
	{
//		$cache = Zend_Registry::get('cache');
		$bubble_data = array();
//		$cache_key = 'widget_bubble_texts_page_' . $page;
//		if ( ! $bubbles = $cache->load($cache_key) ) {
			try {
				$client = Cubix_Api_XmlRpc_Client::getInstance();
				$bubbles = $client->call('Escorts.getBubbleTexts', array($page, $per_page));

			}
			catch ( Exception $e ) {
				$bubbles = array('texts' => array(), 'count' => 0);
			}

			foreach ( $bubbles['texts'] as &$text ) {
				$text = new Model_EscortItem($text);
			}

//			$cache->save($bubbles, $cache_key, array(), 300);
//		}
		
		$this->view->per_page = $per_page;
		$this->view->page = $page;
		$this->view->bubbles = $bubbles;

		return $bubbles;
	}
}