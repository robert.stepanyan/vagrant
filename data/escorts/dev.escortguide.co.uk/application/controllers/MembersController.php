<?php

class MembersController extends Zend_Controller_Action
{
	public function init()
	{
		$this->view->layout()->setLayout('main');
		$this->user = Model_Users::getCurrent();
	}
	
	public function indexAction()
	{
		$username = $this->_request->username;
		$model = new Model_Members();
		$this->view->member_info = $member = $model->getMembersInfoByUsername($username);
		$this->view->username = $username;
		$this->view->top10 = $model->getFavoritesTop10($member['user_id']);
		
		$this->view->user = $this->user;
		
		if ($this->user)
		{
			$reqs = $model->getFavoriteRequests($user->id, $member['user_id']);
			
			$r_arr = array();
			
			if ($reqs)
			{
				foreach ($reqs as $r)
				{
					$r_arr[$r['fav_id']] = $r['status'];
				}
			}
			
			$this->view->reqs = $r_arr;
		}
	}

	public function getMemberCommentsAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
        $user_id = $req->user_id;
        $page = $req->page;
		if ( ! $page )
			$page = 1;
        $per_page = 5;
        $count = 0;
		$model = new Model_Comments();
        $comments = $model->getCommentsByUserId($user_id,$page, $per_page,$count);
		
		$this->view->comments = $comments;
        $this->view->page = $page;
        $this->view->count = $count;
        $this->view->per_page = $per_page;
    }
	
	public function choiceAction()
	{
		$country_id = Cubix_Application::getById()->country_id;
		$modelC = new Model_Cities();
		$this->view->cities = $modelC->getByCountry($country_id);
		$this->view->u = $this->user;
	}
	
	public function choiceListAction()
	{
		$this->view->layout()->disableLayout();
		
		$page = $this->view->page = $this->_request->page ? intval($this->_request->page) : 1;		
		$per_page = $this->view->per_page = 20;
		
		$filter = array();
		
		if ($this->_request->showname)
			$filter['showname'] = strip_tags(trim($this->_request->showname));
		
		if ($this->_request->city)
			$filter['city'] = intval($this->_request->city);
		
		if ($this->_request->with_comments)
		{
			$filter['with_comments'] = 1;
			$filter['with_comments_text'] = 'com';
		}
		
		if ($this->_request->active_girls)
		{
			$filter['active_girls'] = 1;
			$filter['active_girls_text'] = 'act';
		}
		
		if (in_array($this->_request->show, array('top1_only', 'top3_only', 'most_girls', 'gainers', 'loosers')))
			$filter['show'] = $this->_request->show;
		
		if (in_array($this->_request->sort_by, array('date_added', 'showname', 'random', 'rating')))
			$filter['sort_by'] = $this->_request->sort_by;
		else
			$filter['sort_by'] = 'date_added';
		
		$cache = Zend_Registry::get('cache');
		$cache_key = Cubix_Application::getId() . '_' . $page . '_' . $per_page . '_' . implode('_', $filter);
		$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
		
		if ( ! $ret = $cache->load($cache_key) )
		{
			$model = new Model_Members();
			$ret = $model->getChoices($filter, $page, $per_page);

			$cache->save($ret, $cache_key, array(), 1800); //30 min
		}
		
		$this->view->items = $ret['data'];
		$this->view->count = $ret['count'];
	}
	
	public function choiceRankAction()
	{
		$this->view->layout()->disableLayout();
		
		$escort_id = intval($this->_request->escort_id);
		$rank = intval($this->_request->rank);
		
		$cache = Zend_Registry::get('cache');
		$cache_key = Cubix_Application::getId() . '_' . $escort_id . '_' . $rank;
				
		if ( ! $users = $cache->load($cache_key) )
		{
			$model = new Model_Members();
			$users = $model->getByRanks($escort_id, $rank);
		
			$cache->save($users, $cache_key, array(), 1800); //30 min
		}
		
		$arr = array();
		
		if ($rank == 10 && $users)
		{
			foreach ($users as $u)
			{
				$arr[$u['rank']][] = array('username' => $u['username'], 'total_count' => $u['total_count']);
			}
			
			ksort($arr);
			$users = $arr;
		}
		
		$this->view->users = $users;
		$this->view->rank = $rank;
	}
	
	public function choiceCommentsAction()
	{
		$this->view->layout()->disableLayout();
		
		$escort_id = intval($this->_request->escort_id);
		
		$model = new Model_Members();
		
		$comments = $model->getCommentsByEscort($escort_id);
		
		$this->view->comments = $comments;
		$this->view->user = $this->user;
		
		if ($this->user)
		{
			$reqs = $model->getFavoriteRequestsByUser($this->user->id, $escort_id);
			
			$r_arr = array();
			
			if ($reqs)
			{
				foreach ($reqs as $r)
				{
					$r_arr[$r['fav_id']] = $r['status'];
				}
			}
			
			$this->view->reqs = $r_arr;
		}
	}
}
