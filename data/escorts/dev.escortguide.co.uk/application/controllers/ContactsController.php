<?php

class ContactsController extends Zend_Controller_Action
{
	public function indexAction()
	{

	}

	public function contactUsAction()
	{
		if ( ! is_null($this->_getParam('ajax')) ) {
			$this->view->layout()->disableLayout();
			$this->_helper->viewRenderer->setScriptAction('contact-to-ajax');
		}
		
		if ( $this->_request->isPost() ) {

			$validator = new Cubix_Validator();

			$data = new Cubix_Form_Data($this->_request);
			$fields = array(
				'name' => 'notags|special',
				'email' => 'notags|special',
				'message' => 'notags|special',
				'g-recaptcha-response' => ''
			);
			$data->setFields($fields);

			$data = $data->getData();

			if ( ! strlen($data['email']) ) {
				$validator->setError('email', 'Email is required');
			} elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
				$validator->setError('email', 'Wrong email format');
			}

			if ( ! strlen($data['message']) ) {
				$validator->setError('message', 'Message is required');
			}

			if ( ! strlen($data['g-recaptcha-response']) ) {
				$validator->setError('captcha', 'Captcha is required');//
			}
			
			
			$result = $validator->getStatus();

			if ( ! $validator->isValid() ) {
				$this->view->errors = $result['msgs'];
				$this->view->data = $data;
			}
			else {
				$this->view->status = 'true';
				// Fetch administrative emails from config
				$config = Zend_Registry::get('feedback_config');
				$site_emails = $config['emails'];

				$email_tpl = 'feedback_template';
				$data['to_addr'] = $site_emails['support'];

				$tpl_data = array(
					'name' => $data['name'],
					'email' => $data['email'],
					'to_addr' => $data['to_addr'],
					'message' => $data['message']
				);

				Cubix_Email::sendTemplate($email_tpl, $data['to_addr'], $tpl_data, isset($escort) ? $data['email'] : null);
				
				$this->view->ok = true;
			}
			
			if ( ! is_null($this->_getParam('ajax')) ) {
				echo(json_encode($result));
				die;
			}
		}
	}

	public function contactToAction()
	{
		$this->view->layout()->disableLayout();

		$req = $this->_request;

		$id = $this->view->id = (int) $req->id;
		$type = $this->view->type = $req->type;
		$is_ajax = ! is_null($this->_getParam('ajax'));

		$config = Zend_Registry::get('feedback_config');

		if ( ! $id || ! $type ) die;

		if ( $type == 'e' ) {
			$model = new Model_EscortsV2();
			$to_user = $this->view->to_user = $model->get($id, 'contact_to_cache_key' . $id . Cubix_Application::getId());
			$this->view->to_user_title = $this->view->t('escort');
		} elseif ( $type == 'a' ) {
			$model = new Model_Agencies();
			$to_user = $this->view->to_user = $model->get($id);
			$this->view->to_user_title = $this->view->t('agency');
		} elseif ( $type == 'm' ) {
			$this->view->to_user_title = $this->view->t('member');
		} elseif ( $type == 'c' ) {
			$model = new Model_ClassifiedAds();
			$to_user = $this->view->to_user = $model->get($id);
		}

		//$feedback = new Model_Feedback();
		//$blackListModel = new Model_BlacklistedWords();
		$config = Zend_Registry::get('feedback_config');

		if ( $req->isPost() ) {
			$data = new Cubix_Form_Data($this->getRequest());

			$fields = array(
				'name' => 'notags|special',
				'email' => '',
				'message' => 'notags|special',
				'captcha' => ''
			);

			$data->setFields($fields);
			$data = $data->getData();

			$data['to_addr'] = $to_user->email;
			if ( $type == 'e' ) {
				$data['to'] = $id;
				$data['to_name'] = $to_user->showname;
			}
			elseif ($type == 'c')
			{
				$current_user = Model_Users::getCurrent();
				$user_id = null;

				if ($current_user) $user_id = $current_user->id;

				$data['user_id'] = $user_id;
				$data['ca_id'] = $id;
			}
			
			
			$blackListModel = new Model_BlacklistedWords();
			$feedback = new Model_Feedback();
			$cl_ads = new Model_ClassifiedAds();

			$validator = new Cubix_Validator();

			if ( ! $blackListModel->checkFeedback($data) ){
				$blackListModel->mergeFeedbackData($data);
			}

			if ( ! strlen($data['email']) ) {
				$validator->setError('email', 'Email is required');
			} elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
				$validator->setError('email', 'Wrong email format');
			} elseif( $blackListModel->checkEmail($data['email']) ){
				$data['flag'] = FEEDBACK_FLAG_BLOCKED_BY_EMAIL;
				$data['status'] = FEEDBACK_STATUS_DISABLED;
				//$validator->setError('email', 'This email is blocked');
			}

			if ( ! strlen($data['message']) ) {
				$validator->setError('message','Please type the message you want to send' /*'Please type the message you want to send'*/);
			} elseif($blackListModel->checkWords($data['message'])) {

				$data['flag'] = FEEDBACK_FLAG_BLOCKED_BY_WORD;
				$data['status'] = FEEDBACK_STATUS_DISABLED;
				$validator->setError('message', 'You can`t use words "' . $blackListModel->getWords() . '"');
			}

			if( isset($data['flag']) && ($data['flag'] == FEEDBACK_FLAG_BLOCKED_BY_EMAIL || $data['flag'] == FEEDBACK_FLAG_BLOCKED_BY_WORD) ){
				$data['application_id'] = Cubix_Application::getId();

				if ( $type == 'c' )
					$cl_ads->addFeedback($data);
				else
					$feedback->addFeedback($data);
			}

			if ( ! strlen($data['captcha']) ) {
				$validator->setError('captcha', 'Captcha is required');//
			}
			else {
				$session = new Zend_Session_Namespace('captcha');
				$orig_captcha = $session->captcha;

				if ( strtolower($data['captcha']) != $orig_captcha ) {
					$validator->setError('captcha', 'Captcha is invalid');//Captcha is invalid
				}
			}

			$result = $validator->getStatus();

			if ( ! $validator->isValid() ) {

				if ( ! is_null($this->_getParam('ajax')) ) {
					echo(json_encode($result));
					ob_flush();
					die;
				}
			}
			else {
				if ($data['status'] != FEEDBACK_STATUS_DISABLED)
				{
					$email_tpl = "escort_feedback";

						$data['application_id'] = Cubix_Application::getId();
						
						if( $data['application_id'] == APP_ED ){
							if($type == 'e'){
								if(is_numeric($to_user->agency_id)){
									$email_tpl = "feedback_write_me_agency_v1";
									$agency = Cubix_Api::getInstance()->call('customSelect', array( 'name' , 'agencies', array('id' => $to_user->agency_id) ));
								}else{
									$email_tpl = "feedback_write_me_escort_v1";
								}
							}elseif($type == 'a'){
								$email_tpl = "email_received_agency_v1";
								$agency['name'] = $to_user->showname;
							}
						}
						
						
						if ( $config['approvation'] == 1 ) {
							$data['flag'] = FEEDBACK_FLAG_NOT_SEND;
							$data['status'] = FEEDBACK_STATUS_NOT_APPROVED;
						} else {
							$data['flag'] = FEEDBACK_FLAG_SENT;
							$data['status'] = FEEDBACK_STATUS_APPROVED;

							$tpl_data = array(
								'name' => $data['name'],
								'email' => $data['email'],
								'to_addr' => $to_user->email,
								'message' => $data['message'],
								'escort_id' => $id,
								'showname' => $to_user->showname,
								'escort_showname' => '<a target="_blank" href="'. APP_HTTP .'://www.'.Cubix_Application::getById(Cubix_Application::getId())->host.'/escort/'.$to_user->showname.'-'.$id.'">'.$to_user->showname.'</a>',
								'agency' => $agency['name']
							);

							Cubix_Email::sendTemplate($email_tpl, $to_user->email, $tpl_data, $data['email']);
						}

						$feedback->addFeedback($data);
				}

				$session->unsetAll();

				$config = Zend_Registry::get('newsman_config');

				if ($config)
				{
					$list_id = $config['list_id'];
					$segment = $config['member'];
					$nm = new Cubix_Newsman($_SERVER["REMOTE_ADDR"]);

					$subscriber_id = $nm->subscribe($list_id, $data['email'], $data['name']);
					$is_added = $nm->bindToSegment($subscriber_id, $segment);
				}

				echo json_encode($result);
				die;
			}
		}
	}

	public function contactAdvertiseAction()
	{
		$this->view->layout()->disableLayout();

		$req = $this->_request;

		$category = $this->view->category = $req->category;
		$position = $this->view->position = $req->position;
		$country = $this->view->country = $req->country;
		$days = $this->view->days = $req->days;
		$price = $this->view->price = $req->price;
		

		if ( ! $category ) die;
					
		if ( $req->isPost() ) {
			$data = new Cubix_Form_Data($this->getRequest());

			$fields = array(
				'category' => 'notags|special',
				'name' => 'notags|special',
				'email' => '',
				'message' => 'notags|special',
				'position' => 'notags|special',
				'country' => 'notags|special',
				'days' => 'notags|special',
				'price' => 'notags|special',
				'captcha' => ''
			);

			$data->setFields($fields);
			$data = $data->getData();

			$validator = new Cubix_Validator();

			if ( ! strlen($data['name']) ) {
				$validator->setError('name', 'Name is required');
			}
			if ( ! strlen($data['email']) ) {
				$validator->setError('email', 'Email is required');
			} elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
				$validator->setError('email', 'Wrong email format');
			} 

			if ( ! strlen($data['message']) ) {
				$validator->setError('message','Please type the message you want to send' /*'Please type the message you want to send'*/);
			} 

			if ( ! strlen($data['captcha']) ) {
				$validator->setError('captcha', 'Captcha is required');//
			}
			else {
				$session = new Zend_Session_Namespace('captcha');
				$orig_captcha = $session->captcha;

				if ( strtolower($data['captcha']) != $orig_captcha ) {
					$validator->setError('captcha', 'Captcha is invalid');//Captcha is invalid
				}
			}

			$result = $validator->getStatus();

			if ( ! $validator->isValid() ) {
				echo(json_encode($result));
				ob_flush();	die;
			}
			else {
				$config = Zend_Registry::get('feedback_config');
				$site_emails = $config['emails'];
				$email_tpl = "advertise_inquiry";
				
				$data['application_id'] = Cubix_Application::getId();

				$message = "<p>Category - ".$data['category']."</p>".
						   "<p>Position - ".$data['position']."</p>".
						   "<p>Country - ".$data['country']."</p>".
						   "<p>Days - ".$data['days']."</p>".
						   "<p>Price - ".$data['price']."</p><p>-----------</p>".$data['message'];

				$tpl_data = array(
					'advertise' => $data['category'],
					'name' => $data['name'],
					'email' => $data['email'],
					'message' => $message
				);
				
				Cubix_Email::sendTemplate($email_tpl, $site_emails['sales'], $tpl_data);
				echo json_encode($result);
				die;
			}

			$session->unsetAll();
		}
	}
	
	public function contactToSuccessAction()
	{
		$this->view->layout()->disableLayout();

		$req = $this->_request;

		$id = $this->view->id = (int) $req->id;
		$type = $this->view->type = $req->type;
		$is_ajax = ! is_null($this->_getParam('ajax'));

		if ( ! $id || ! $type ) die;

		if ( $type == 'e' ) {
			$model = new Model_EscortsV2();
			$to_user = $this->view->to_user = $model->get($id, 'contact_to_cache_key' . $id . Cubix_Application::getId());
			$this->view->to_user_title = $this->view->t('escort');
		} elseif ( $type == 'a' ) {
			$model = new Model_Agencies();
			$to_user = $this->view->to_user = $model->get($id);
			$this->view->to_user_title = $this->view->t('agency');

		} elseif ( $type == 'm' ) {

			$this->view->to_user_title = $this->view->t('member');
		}
	}
}
