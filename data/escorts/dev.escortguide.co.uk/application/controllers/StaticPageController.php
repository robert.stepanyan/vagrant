<?php

class StaticPageController extends Zend_Controller_Action
{
	public function init()
	{
		//$this->view->layout()->disableLayout();
		
		
	}
	
	public function showAction()
	{
		$slug = $this->_request->page_slug;
		$lang = Cubix_I18n::getLang();
		$app_id = Cubix_Application::getId();
		
		$model = new Model_StaticPage();
		$this->view->page = $model->getBySlug($slug, $app_id, $lang);
	}
}
