<?php

class XmasController extends Zend_Controller_Action
{
	private $_model;
	private $user;
		
	public function init() 
	{
		$this->user = $this->view->user = Model_Users::getCurrent();
        $this->view->layout()->disableLayout();

        //echo $this->user->user_type; die;
        if( $this->user->user_type == 'member' ){
            die;
        }
		
		if ( ! $this->user ) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}
		
		$this->_model = new Model_XmasGame();
	}

    public function getStatusAction(){
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $status = $this->_model->getXmasStatus();
        die( json_encode( array( 'status' => $status ) ) );
    }

    public function getIfWinAction(){
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $status = $this->_model->getSimpleWinStatus();
        die( json_encode( array( 'data' => $status ) ) );
    }

    public function getSimpleStatusAction(){
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $status = $this->_model->getSimpleXmasStatus();
        die( json_encode( array( 'status' => $status ) ) );
    }

    public function sendVoteAction(){
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $status = $this->_model->sendVote();
        die( json_encode( array( 'status' => $status ) ) );
    }

    public function getTimestampsAction(){
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        die( json_encode( $this->_model->getServerDates() ) );
    }
	
	public function indexAction()
	{
        $this->view->layout()->disableLayout();
        $this->view->status = $this->_model->getXmasStatus();
	}

    public function termsAction()
    {
        $this->view->status = $this->_request->st;
        $this->view->layout()->disableLayout();
    }

    public function simplePopupAction(){
        $this->view->secure = $_secure = ( $this->_request->secure ) ? $this->_request->secure : false;

        if( !$this->_request->isPost() || !$_secure ) {
            $this->_response->setRawHeader('HTTP/1.1 404 Not Found');
            $this->_forward('error', 'error', null, array('error_msg' => '404 Page Not Found. Please try the following links instead.'));
            return;
        } else {

        }
    }

    public function winnerPopupAction(){
        $this->view->secure = $_secure = ( $this->_request->secure ) ? $this->_request->secure : false;

        if( !$this->_request->isPost() || !$_secure ) {
            $this->_response->setRawHeader('HTTP/1.1 404 Not Found');
            $this->_forward('error', 'error', null, array('error_msg' => '404 Page Not Found. Please try the following links instead.'));
            return;
        }
    }

    public function setAgreeAction()
    {
        $this->view->status = $this->_request->agree;
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $user_model = Model_Users::getCurrent();

        $result = $this->_model->setAgree( $user_model->id );
        die( json_encode( array( "status" => $result ) ) );
    }

    public function getAgreeAction()
    {
        $this->view->status = $this->_request->agree;
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $user_model = Model_Users::getCurrent();

        $result = $this->_model->getAgree( $user_model->id );
        die( json_encode( array( "status" => $result ) ) );
    }

    public function gameAction()
    {
        $this->view->layout()->disableLayout();
        //$this->_helper->viewRenderer->setNoRender(true);
        $user_model = Model_Users::getCurrent();
        $this->view->data = json_encode( $this->_model->getByUserId( $user_model->id ) );
    }

	public function saveEscortVoteAction()
	{
        $data = array();
		$this->view->layout()->disableLayout();

		if ($this->_request->isPost())
		{
			$validator = new Cubix_Validator();
            $escort_id = intval( $this->_request->escort_id );
            $ball_count = intval( $this->_request->ball_count );

			if ( !$escort_id )
				$validator->setError('escort_id', 'Required');
			if ( !$ball_count )
				$validator->setError('ball_count', 'Required');

			if ( $validator->isValid() ) 
			{

				$data = array(
					'escort_id' => $this->user->id,
					'ball_count' => $this->user->user_type
				);

                $result = $this->_model->save( $data );

			}

			$status =  $validator->getStatus();

            if( $status['status'] == 'success' ){
                die( json_encode( array( 'result' => $result ) ) );
            } else {
                die( json_encode( $status ) );
            }

            //print_r( $data );
		}
	}

    public function getClickedBallsByEscortId( $escort_id ){

    }
}