<?php

class PublicApiController extends Zend_Controller_Action
{

    public function init()
    {
        $this->tokens = [
            'www.escortrankings.uk' => 'ZXV2]$tVeFzMDM:/'
        ];

        $this->setGuard();
    }

    public function indexAction()
    {
        $client = Cubix_Api_XmlRpc_Client::getInstance();
        $page = $_GET['page'];
        $escorts = $client->call('Escorts.getActiveEscorts', ['page' => $page]);
        $totalEscorts = $escorts['count'];
        $escorts = $escorts['list'];
        $escortsMap = [];

        $errors = [];
        if (empty($page)) {
            $errors['page'] = 'Parameter :page was not specified.';
        }

        if (!empty($errors)) {
            echo json_encode([
                'errors' => $errors
            ]);
            exit;
        }

        $DEFINITIONS = Zend_Registry::get('defines');
        $statusNames =  $DEFINITIONS['escort_status_options'];
        $servicesNames = $DEFINITIONS['services'];
        $time_units = $DEFINITIONS['time_unit_options'];
        $availability_options = $DEFINITIONS['available_for'];
        $yes_or_no_options = $DEFINITIONS['y_n_na_options'];
        $breast_types = $DEFINITIONS['breast_type_options'];
        $hair_color = $DEFINITIONS['hair_color_options'];
        $hair_length = $DEFINITIONS['hair_length_options'];
        $ethnicity = $DEFINITIONS['ethnic_options'];
        $pubic_hair = $DEFINITIONS['pubic_hair_options'];
        $pornstar = $DEFINITIONS['y_n_oc_options'];
        $piercing = $DEFINITIONS['piercing_options'];
        $gender = $DEFINITIONS['gender_options'];
        $conversation_options = $DEFINITIONS['conversation_options'];
        $availability_options_review = $DEFINITIONS['availability_options'];
        $photos_options = $DEFINITIONS['photos_options'];
        $kissing_options = $DEFINITIONS['kissing_options'];
        $blowjob_options = $DEFINITIONS['blowjob_options'];
        $cumshot_options = $DEFINITIONS['cumshot_options'];
        $sex_options = $DEFINITIONS['sex_options'];
        $attitude_options = $DEFINITIONS['attitude_options'];
        $breast_options = $DEFINITIONS['breast_options'];
        $currency = $DEFINITIONS['currency_symbols'];
        $sex_count = $DEFINITIONS['mts_options'];
        $anal = $DEFINITIONS['y_n_na_options'];
        $tattoo = $DEFINITIONS['tatoo_options'];
        $metric_system = $DEFINITIONS['metric_system'];
        $review_suspicious_options = $DEFINITIONS['review_suspicious_options'];

        foreach ($escorts as $escort) {
            $escort['status'] = $statusNames[$escort['status']];
            // Bad decision before my brain starts working
            if ($escort['status'] === null){
                $escort['status'] = 'Inactive / disabled';
            }
            $escort['description'] = strip_tags($escort['description']);
            $escort['measure_units'] =  $metric_system[$escort['measure_units']];
            $escort['weight'] = round($escort['weight']) . ' kg';
            $escort['height'] = round($escort['height']) . ' sm';
            $escort['hair_color'] = $hair_color[$escort['hair_color']];
            $escort['smokes'] = $yes_or_no_options[$escort['smokes']];
            $escort['breast_type'] = $breast_types[$escort['breast_type']];
            $escort['hair_length'] = $hair_length[$escort['hair_length']];
            $escort['pornstar'] = $pornstar[$escort['pornstar']];
            $escort['piercing'] = $piercing[$escort['piercing']];
            $escort['tattoo'] = $tattoo[$escort['tattoo']];
            $escort['gender'] = $gender[$escort['gender']];
            $escort['kitty'] = $pubic_hair[$escort['kitty']];
            $escort['ethnicity'] = $ethnicity[$escort['ethnicity']];

            $escortsMap[$escort['id']]['pictures'] = null;
            $escortsMap[$escort['id']]['videos'] = null;
            $escortsMap[$escort['id']]['escort'] = $escort;
            $escortsMap[$escort['id']]['services'] = null;
            $escortsMap[$escort['id']]['rates_values_incalls'] = null;
            $escortsMap[$escort['id']]['rates_values_outcalls'] = null;
            $escortsMap[$escort['id']]['reviews'] = null;
        }


        if (!empty($escortsMap)) {
            $reviews = $client->call('Escorts.getReviewsByEscortIds', ['ids' => array_keys($escortsMap)]);
            $photos = $client->call('Escorts.getPhotosByEscortIds', ['ids' => array_keys($escortsMap)]);
            $videos = $client->call('Escorts.getVideosByEscortIds', ['ids' => array_keys($escortsMap)]);
            $services = $client->call('Escorts.getEscortServices', ['ids' => array_keys($escortsMap)]);
            $incallServicesRates = $client->call('Escorts.getEscortIncallServices', ['ids' => array_keys($escortsMap)]);
            $outcallServicesRates = $client->call('Escorts.getEscortOutcallServices', ['ids' => array_keys($escortsMap)]);

            // Getting escorts reviews
            foreach ($reviews as $review) {
                $review['conversation'] = $conversation_options[$review['conversation']];
                $review['availability'] = $availability_options_review[$review['availability']];
                $review['photos'] = $photos_options[$review['photos']];
                $review['kissing'] = $kissing_options[$review['kissing']];
                $review['blowjob'] = $blowjob_options[$review['blowjob']];
                $review['cumshot'] = $cumshot_options[$review['cumshot']];
                $review['sex'] = $sex_options[$review['sex']];
                $review['attitude'] = $attitude_options[$review['attitude']];
                $review['breast'] = $breast_options[$review['breast']];
                $review['currency'] = $currency[$review['currency']];
                $review['multiple_sex'] = $sex_count[$review['multiple_sex']];
                $review['anal'] = $anal[$review['anal']];
                $review['69_pos'] = $anal[$review['69_pos']];
                $review['duration_unit'] = $time_units[$review['duration_unit']];
                $review['is_suspicious'] = $review_suspicious_options[$review['is_suspicious']];
                $escortsMap[$review['escort_id']]['reviews'][] = $review;
            }


            // Getting escort photos
            foreach ($photos as $photo) {
                $photoItem = new Model_Escort_PhotoItem(array(
                    'application_id' => Cubix_Application::getId(),
                    'escort_id' => $photo['escort_id'],
                    'hash' => $photo['hash'],
                    'ext' => $photo['ext']. '?788298dc5c29b952ff9355ac2c22d703=true',
                ));

                $photo_url = $photoItem->getUrl();
                $escortsMap[$photo['escort_id']]['pictures'][] = $photo_url;
            }

            // Getting escort videos
            foreach ($videos as $video) {
                $video_url = 'https://video.escortguide.co.uk/escortguide.co.uk/' . $video['escort_id'] . '/' . $video['video'] . '_' . $video['height'] . 'p' . '.' . 'mp4';
                $escortsMap[$video['escort_id']]['videos'][] = $video_url;
            }

            // Getting escort services
            foreach ($services as $service) {
                $id = $service['service_id'];
                $serviceName = null;
                if (isset($servicesNames['fetish'][$id])) {
                    $serviceName = $servicesNames['fetish'][$id];
                } else if (isset($servicesNames['common'][$id])) {

                    $serviceName = $servicesNames['common'][$id];
                } else if (isset($servicesNames['extra'][$id])) {
                    $serviceName = $servicesNames['extra'][$id];
                }

                if (!empty($serviceName)) {
                    $escortsMap[$service['escort_id']]['services'][$id] = $serviceName;
                }
            }

            // Getting escorts rates
            // Getting incall rates
            foreach ($incallServicesRates as $incall) {
                $incall['time_unit'] = $time_units[$incall['time_unit']];
                $incall['availability'] = $availability_options[$incall['availability']];

                if ($incall['time'] === 0) {
                    $total = $incall['type'] . ':' . $incall['price'] . $incall['title'];
                } else {
                    $total = $incall['time'] . $incall['time_unit'] . ':' . $incall['price'] . $incall['title'];
                }
                $escortsMap[$incall['escort_id']]['rates_values_incalls'][] = $total;

            }

            // Getting outcall rates
            foreach ($outcallServicesRates as $outcall) {
                $outcall['time_unit'] = $time_units[$outcall['time_unit']];
                $outcall['availability'] = $availability_options[$outcall['availability']];
                if ($outcall['time'] === 0) {
                    $total = $outcall['type'] . ':' . $outcall['price'] . $outcall['title'];
                } else {
                    $total = $outcall['time'] . $outcall['time_unit'] . ':' . $outcall['price'] . $outcall['title'];
                }
                $escortsMap[$outcall['escort_id']]['rates_values_outcalls'][] = $total;
            }


        }
        //echo '<pre>';
        echo json_encode([
            'total' => $totalEscorts,
            'escorts' => $escortsMap,
        ]);
        // echo '<pre>';
        exit();
    }

    private function setGuard()
    {
        $token = $_SERVER['AUTH_TOKEN']; // Auth-Token in headers
        if (empty($token)) $token = $_SERVER['HTTP_AUTH_TOKEN'];

        if (!in_array($token, $this->tokens) && APPLICATION_ENV != 'development') {
            header('HTTP/1.0 403 Forbidden');
            echo 'You are forbidden!';
            exit();
        }
    }
}
