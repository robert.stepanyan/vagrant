<?php

class ReviewsController extends Zend_Controller_Action
{
	public function init()
	{
		$this->user = Model_Users::getCurrent();	
	}

	public function indexAction()
	{
	    $lng = Cubix_I18n::getLang();
        $request = $this->_request;
        $allowedKeys = ['index', 'top'];
        $this->view->pageKey = isset($request->{'page-key'}) ? $request->{'page-key'} : 'index';
        if (!in_array ($this->view->pageKey, $allowedKeys) ) {
            $this->_response->setRawHeader('HTTP/1.1 404 Not Found');
            $this->_forward('error', 'error', null, array('error_msg' => '404 Page Not Found. Please try the following links instead.'));
        }
        // if ($this->view->pageKey == 'top-reviewed') $this->view->pageKey = 'top';
        $validator = new Cubix_Validator();
		if ( $request->ajax ) {
			$this->view->layout()->disableLayout();
			$this->view->ajax = true;
		}
		$filter = array();

        if($request->fromEscortReview){
            $this->view->fromEscortReview = true;
        }

		if (isset($request->r_city) && is_numeric($request->r_city) )
		{
			$filter['city_id'] = intval($request->r_city);
			$this->view->r_city = intval($request->r_city);
		}

		if (isset($request->showname) && $request->showname )
		{
			$filter['showname'] = $validator->cleanUsername($request->showname);
			$this->view->showname = $filter['showname'];
		}

		if (isset($request->member_name) && $request->member_name )
		{
			$filter['member_name'] = $validator->cleanUsername($request->member_name);
			$this->view->member_name = $filter['member_name'];
		}
	
		
		if (isset($request->gender) && $request->gender)
		{
			$filter['gender'] = preg_replace('#[^0-9,]#', '', $request->gender);
			$this->view->gender = $filter['gender'];
		}

		if(isset($request->without_escort) && is_numeric($request->without_escort)){
            $filter['without_escort'] = (int) $request->without_escort;
        }

		switch ($request->ord_field)
		{
			case 'city_title':
				$ord_field_v = 'city_title';
				$ord_field = 'c.title_' . $lng;
				break;
			case 'showname':
				$ord_field_v = 'showname';
				$ord_field = 'r.showname';
				break;
			case 'agency_name':
				$ord_field_v = 'agency_name';
				$ord_field = 'r.agency_name';
				break;
			case 'looks':
				$ord_field_v = 'looks';
				$ord_field = 'r.looks_rating';
				break;
			case 'services':
				$ord_field_v = 'services';
				$ord_field = 'r.services_rating';
				break;
			case 'member':
				$ord_field_v = 'member';
				$ord_field = 'r.username';
				break;
			case 'date':
			default:
				$ord_field_v = 'creation_date';
				$ord_field = 'r.creation_date';
				break;
		}		
		
		switch ($request->ord_dir)
		{
			case 'asc':
				$ord_dir_v = 'asc';
				$ord_dir = 'ASC';
				break;
			case 'desc':
			default:
				$ord_dir_v = 'desc';
				$ord_dir = 'DESC';
				break;
		}

        $group_field = 'r.id';

        if ($this->view->pageKey == 'top') {
            $ord_field   = 'review_count';
            $ord_dir     = 'desc';
            $group_field = 'r.escort_id';

            $filter['exclude_inactive'] = true;
        }

		if (count($filter) > 0)
			$arg_filter = $filter;
		else
			$arg_filter = '-999';

		$this->view->config = $config = Zend_Registry::get('reviews_config');

		$first_call = (boolean) !$request->ajax;

		if (isset($request->page) && intval($request->page) > 0)
		{
			$this->view->page = $page = intval($request->page);
		}
		else
            $this->view->page = $page = 1;

		is_array($arg_filter) ? $filter_str = implode('.', $arg_filter) : $filter_str = $arg_filter;
		$cache = Zend_Registry::get('cache');
		$cache_key = 'v2_reviews_list_' . $lng . '_page_' . $page . '_perpage_' . $config['perPage'] . '_filter_' . $filter_str . '_ordfield_' . $ord_field . '_orddir_' . $ord_dir . $group_field;
		$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

		/*if ( ! $ret = $cache->load($cache_key) )
		{*/
		    $checkPerPage = ($request->perPage) ? $request->perPage : false;
			$ret_revs = Model_Reviews::getReviews($page, ($request->perPage) ? $request->perPage : $config['perPage'], $arg_filter, $ord_field, $ord_dir , $checkPerPage,$first_call, $group_field);
			
			$cities = Model_Reviews::getReviewsCities();

			foreach ( $ret_revs[0] as $i => $rev ) {
				$model_photos = new Model_Escort_PhotoItem($rev);
				$photos = $model_photos->getPhotosByEscortId($rev['escort_id']);
					
				for ($loopphoto=0; $loopphoto < 3; $loopphoto++) { 
					$ret_revs[0][$i]['photo_url_' . $loopphoto] = $photos[$loopphoto]->photo_url;
				}
			}

			$ret = array($ret_revs, $cities);

			/*$cache->save($ret, $cache_key, array(), $config['cacheTime']);
		}*/

        $model = new Model_Reviews();
        $this->view->page = $page;
        $imported_res = $model->getImportedReviews($page, ($request->perPage) ? $request->perPage : $config['perPage']);
        $imported_reviews = $imported_res;
		list($ret_revs, $cities) = $ret;
		list($items, $count, $genders) = $ret_revs;
        $mergedReviews = $model::mergeReviews($items, $imported_reviews);
        if (isset($request->r_city)){
            $mergedReviews = array_filter($mergedReviews, function($v) {
                $request = $this->_request;
                return $v['city_id'] == $request->r_city;}, ARRAY_FILTER_USE_BOTH);
        }
        $this->view->items = $mergedReviews;
        $this->view->request = $request;
		$this->view->count = $count;
		$this->view->cities = $cities;
		$this->view->page = $page;
		$this->view->genders = $genders;
        $this->view->from_members_info = isset ($request->from_members_info) ? 1 : 0;
		$this->view->ord_field = $ord_field_v;
		$this->view->ord_dir = $ord_dir_v;
        if ( $request->ajax ) {
            if($request->fromEscortReview){
                $this->_helper->viewRenderer('review-items');
            }else{
                $escort_list =  $this->view->render('reviews/review-items.phtml');
                die(json_encode(array('escort_list' => $escort_list)));
            }
        }
	}

	public function escortAction()
	{
		$request = $this->_request;
		$escortName = $request->escortName;
		$escort_id = $request->escort_id;

		if ( $request->ajax ) {
			$this->view->layout()->disableLayout();
			$this->view->ajax = true;
		}

		$config = Zend_Registry::get('reviews_config');

		$modelR = new Model_Reviews();

		$escort = $modelR->getEscortInfo($escort_id);

		$escort = new Model_EscortItem($escort);
        if (!isset($escort->is_inactive)){
            $escort->is_inactive = $escort->getIsInactive($escort->id);
        }
		$model = new Model_Escorts();

        $add_esc_data = $model->getRevComById($escort_id);
        $escortCityForReview = $model->getCityByEscortId($escort_id);
		$escort->disabled_reviews = $add_esc_data->disabled_reviews;
		$escort->disabled_comments = $add_esc_data->disabled_comments;
		$escort->city = $escortCityForReview->title;
		$escort->city_id = $escortCityForReview->id;
		$count = 10;
		$photosHashes = $escort->getPhotos(1, $count, true, false, null, null, true);
		
		foreach($photosHashes as $photo){
			$arr_photo[] = $photo->getUrl('agency_escorts_slider');
		}

		$this->view->escort = $escort;
		$this->view->escort->photos = $arr_photo;
		
		if (isset($request->page) && intval($request->page) > 0)
			$page = intval($request->page);
		else
			$page = 1;

		$perpage = 3;
		
		if (isset($request->perpage))
		{
			if ($request->perpage == 'all')
				$perpage = 99999;
			elseif (intval($request->perpage) > 0)
				$perpage = intval($request->perpage);
		}

		$cache = Zend_Registry::get('cache');
		$cache_key = 'v2_reviews_escort_' . $lng . '_page_' . $page . '_perpage_' . $perpage . '_escortName_' . $escortName . '_escort_id_' . $escort_id;
		$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

		if ( ! $store = $cache->load($cache_key) )
		{
			//$topLadies = Cubix_Api::getInstance()->call('getTopLadies', array($config['topLadiesCount'], $lng));
			$topLadies = Model_Reviews::getTopLadies($config['topLadiesCount']);
			//$ret = Cubix_Api::getInstance()->call('getEscortReviews', array($escortName, $page, $perpage, $lng));
			//$ret = Model_Reviews::getEscortReviews($escortName, $page, $perpage);
			$ret = Model_Reviews::getEscortReviews($escort_id, $page, $perpage);

			$store = array($ret, $topLadies);

			$cache->save($store, $cache_key, array(), $config['cacheTime']);
		}

		list($ret, $topLadies) = $store;
		list($items, $count) = $ret;

		$topEsc = array();
		$topEscK = array();
		foreach ($topLadies as $k => $escort2)
		{
			$topEsc[$k] = $escort2['escort_id'];
			$topEscK[$escort2['escort_id']] = $k;
		}
		
		//$escort_id = $items[0]['escort_id'];
		
		if (in_array($escort_id, $topEsc))
			$this->view->inTop30 = $topEscK[$escort_id] + 1;
		
		$this->view->items = $items;
		$this->view->count = $count;
		$this->view->page = $page;
		if ($perpage == 99999) $perpage = 'all';
		$this->view->perpage = $perpage;

		if (($escort->status & ESCORT_STATUS_DELETED) || ($escort->status & ESCORT_STATUS_OWNER_DISABLED) || ($escort->status & ESCORT_STATUS_ADMIN_DISABLED))
		{
			$this->view->disable = true;
		}
	}

	public function addReviewAction()
	{

		if ( $this->user->user_type != 'member' )
			$this->_redirect($this->view->getLink('signin'));
		else
		{
			
			if (isset($this->_request->escort_id) && intval($this->_request->escort_id) > 0)
			{
				$escort_id = $this->view->escort_id = intval($this->_request->escort_id);
				$model = new Model_Escorts();

				$esc = $model->getRevComById($escort_id);

				if ($esc->disabled_reviews)
				{
					$this->_redirect($this->view->getLink());
					return;
				}
				else
				{
					$lng = Cubix_I18n::getLang();

					list($showname, $cities) = Cubix_Api::getInstance()->call('getEscortDetailsForReviews', array($escort_id, $lng));

				
					if (!$showname)
					{
						$this->_forward('error', 'error', 'default', array('error_msg' => 'Wrong URL.'));
						return;
					}

					$this->view->showname = $showname;
					$this->view->cities = $cities;
					$this->view->username = $this->user->username;
				}
			}
			else
			{
				$this->_forward('error', 'error', 'default', array('error_msg' => 'Wrong URL.'));
				return;
			}

			if ($this->_request->isPost())
			{

				$validator = new Cubix_Validator();

				$req = $this->_request;
				
				if (intval($req->m_date) == 0)
					$validator->setError('m_date', 'Required');

				if ($req->meeting_city == 'other')
				{
					if (!$req->country_id)
						$validator->setError('country_id', 'Required');
					
					if (!$req->city_id)
						$validator->setError('city_id', 'Required');
				}

				if (!$req->meeting_place)
					$validator->setError('meeting_place', 'Required');

				if (!trim($req->duration))
					$validator->setError('duration', 'Required');
				elseif (!is_numeric($req->duration))
					$validator->setError('duration', $this->view->t('must_be_numeric'));

				if (!$req->duration_unit)
					$validator->setError('duration_unit', 'Required');

				if (!trim($req->price))
					$validator->setError('currency', 'Required');
				elseif (!is_numeric($req->price))
					$validator->setError('currency', $this->view->t('must_be_numeric'));

				if ($req->looks_rate == '-1')
					$validator->setError('looks_rate', 'Required');

				if ($req->services_rate == '-1')
					$validator->setError('services_rate', 'Required');

				if (!$req->s_kissing)
					$validator->setError('s_kissing', 'Required');

				if (!$req->s_blowjob)
					$validator->setError('s_blowjob', 'Required');

				if (!$req->s_cumshot)
					$validator->setError('s_cumshot', 'Required');

				if (!$req->s_69)
					$validator->setError('s_69', 'Required');

				if (!$req->s_anal)
					$validator->setError('s_anal', 'Required');

				if (!$req->s_sex)
					$validator->setError('s_sex', 'Required');

				if (!$req->s_multiple_times_sex)
					$validator->setError('s_multiple_times_sex', 'Required');

				if (!$req->s_breast)
					$validator->setError('s_breast', 'Required');

				if (!$req->s_attitude)
					$validator->setError('s_attitude', 'Required');

				if (!$req->s_conversation)
					$validator->setError('s_conversation', 'Required');

				if (!$req->s_availability)
					$validator->setError('s_availability', 'Required');

				if (!$req->s_photos)
					$validator->setError('s_photos', 'Required');

				if (!trim($req->t_user_info))
					$validator->setError('t_user_info', 'Required');

				if (!trim($req->t_meeting_date))
					$validator->setError('t_meeting_date', 'Required');

				if ($req->hrs == '-1')
					$validator->setError('hrs', 'Required');

				if ($req->min == '-1')
					$validator->setError('hrs', 'Required');

				if (!trim($req->t_meeting_duration))
					$validator->setError('t_meeting_duration', 'Required');

				if (!trim($req->t_meeting_place))
					$validator->setError('t_meeting_place', 'Required');

				if (!trim($req->t_comments))
					$validator->setError('t_comments', 'Required');


			
				if ( ! strlen( $_POST['g-recaptcha-response']  ) ) {
					$validator->setError('captcha', 'Required');
				}
				

				$t_user_info = trim($req->t_user_info);
				$t_meeting_date = trim($req->t_meeting_date);
				$t_meeting_time = $req->hrs . ':' . $req->min;
				$t_meeting_duration = trim($req->t_meeting_duration);
				$t_meeting_place = trim($req->t_meeting_place);

				if ($validator->isValid())
				{
					$ip = Cubix_Geoip::getIP();

					$req->setParam('ip', $ip);
					$req->setParam('user_id', $this->user->id);
					$req->setParam('m_date', $req->t_meeting_date);

					list($sms_unique, $phone_to, $mem_susp) = Cubix_Api::getInstance()->call('addReview', array($this->getRequest()->getParams()));
					Cubix_Api::getInstance()->call('SyncNotifier', array($escort_id, 29 ,array('escort_id' => $escort_id) ));
					if (strlen(trim($phone_to)) > 0 && !$mem_susp)
					{
						$text = $this->view->t('review_sms_template', array(
							'user_info' => $t_user_info,
							'showname' => $showname,
							'date' => $t_meeting_date,
							'meeting_place' => $t_meeting_place,
							'duration' => $t_meeting_duration,
							'time' => $t_meeting_time,
							'unique_number' => $sms_unique
						));

						$originator = Cubix_Application::getPhoneNumber(Cubix_Application::getId());

						$phone_from = $originator;
						$sms_id = Cubix_Api::getInstance()->call('outbox', array($escort_id, $phone_to, $phone_from, $text, Cubix_Application::getId()));

						//sms info
						$config = Zend_Registry::get('system_config');
						$sms_config = $config['sms'];
						
						$SMS_USERKEY = $sms_config['userkey'];
						$SMS_PASSWORD = $sms_config['password'];
						$SMS_DeliveryNotificationURL = $sms_config['DeliveryNotificationURL'];
						$SMS_NonDeliveryNotificationURL = $sms_config['NonDeliveryNotificationURL'];
						$SMS_BufferedNotificationURL = $sms_config['BufferedNotificationURL'];
						
						$sms = new Cubix_SMS($SMS_USERKEY, $SMS_PASSWORD);
						
						$sms->setOriginator($originator);
						$sms->addRecipient($phone_to, (string)$sms_id);
						$sms->setBufferedNotificationURL($SMS_BufferedNotificationURL);
						$sms->setDeliveryNotificationURL($SMS_DeliveryNotificationURL);
						$sms->setNonDeliveryNotificationURL($SMS_NonDeliveryNotificationURL);

						$sms->setContent($text);
						
						if (1 != $result = $sms->sendSMS())
						{

						}
						else
						{
							Cubix_Api::getInstance()->call('smsSent', array($sms_unique));
						}
					}

					$this->_redirect($this->view->getLink('escort-reviews', array('showname' => $showname, 'escort_id' => $escort_id)));
				}
				else
				{
					$status = $validator->getStatus();
					$this->view->errors = $status['msgs'];
					$this->view->review = $req;
				}
			}
		}
	}

    public function memberAction()
    {
        $lng = Cubix_I18n::getLang();
		$validator = new Cubix_Validator();
        $request = $this->_request;
        $filter = array();

        if ( $request->ajax ) {
            $this->view->layout()->disableLayout();
            $this->view->ajax = true;
        }

        $filter['username'] = $this->view->username = $validator->cleanUsername($request->username);

        switch ($request->ord_field)
        {
            case 'city_title':
                $ord_field_v = 'city_title';
                $ord_field = 'c.title_' . $lng;
                break;
            case 'showname':
                $ord_field_v = 'showname';
                $ord_field = 'r.showname';
                break;
            case 'agency_name':
                $ord_field_v = 'agency_name';
                $ord_field = 'r.agency_name';
                break;
            case 'looks':
                $ord_field_v = 'looks';
                $ord_field = 'r.looks_rating';
                break;
            case 'services':
                $ord_field_v = 'services';
                $ord_field = 'r.services_rating';
                break;
            case 'member':
                $ord_field_v = 'member';
                $ord_field = 'r.username';
                break;
            case 'date':
            case 'member':
                $ord_field_v = 'fuckometer';
                $ord_field = 'r.fuckometer';
                break;
            case 'date':
            default:
                $ord_field_v = 'creation_date';
                $ord_field = 'r.creation_date';
                break;
        }

        switch ($request->ord_dir)
        {
            case 'asc':
                $ord_dir_v = 'asc';
                $ord_dir = 'ASC';
                break;
            case 'desc':
            default:
                $ord_dir_v = 'desc';
                $ord_dir = 'DESC';
                break;
        }

        if (count($filter) > 0)
            $arg_filter = $filter;
        else
            $arg_filter = '-999';

        $config = Zend_Registry::get('reviews_config');

        if (isset($request->page) && intval($request->page) > 0)
        {
            $page = intval($request->page);
        }
        else
            $page = 1;

        is_array($arg_filter) ? $filter_str = implode('.', $arg_filter) : $filter_str = $arg_filter;
        $cache = Zend_Registry::get('cache');
        $cache_key = 'v2_reviews_member_' . $lng . '_page_' . $page . '_perpage_' . $config['perPage'] . '_filter_' . $filter_str . '_ordfield_' . $ord_field . '_orddir_' . $ord_dir;
        $cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

        $ret = Model_Reviews::getReviews($page, $config['perPage'], $arg_filter, $ord_field, $ord_dir);

        foreach ( $ret[0] as $i => $rev ) {
            $photo_url = new Model_Escort_PhotoItem($rev);
            $ret[0][$i]['photo_url'] = $photo_url->getUrl('agency_p100');
        }



        list($items, $count) = $ret;

        $this->view->items = $items;
        $this->view->count = $count;
        $this->view->page = $page;

        $this->view->ord_field = $ord_field_v;
        $this->view->ord_dir = $ord_dir_v;
    }

}
