<?php

class AccountController extends Zend_Controller_Action
{
	public static $linkHelper;

	protected $_session;

	public function init()
	{
		$this->_request->setParam('no_tidy', true);
		
		$anonym = array('membership-type', 'signup', 'signin', 'forgot', 'activate', 'check', 'sign-in-up', 'choose-vip-package','change-pass' );
		$this->blacklisted_usernames = array( 'admin' , 'moderator', 'webmaster' ,'manager', 'sales' );
		$this->user = Model_Users::getCurrent();
		
		if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {
			$this->_response->setRedirect($this->view->getLink('signin'));
			return;
		}

		if( $this->user && isset($_GET['referrer'])){
			if(strpos($_GET['referrer'], 'private') === false){
				$this->_response->setRedirect( Cubix_Application::getById()->url . '/' . $_GET['referrer']);
			}else{
				$_GET['referrer'] = str_replace('private/', 'private#', $_GET['referrer']);
				$this->_response->setRedirect(Cubix_Application::getById()->url .'/'. $_GET['referrer']);
			}
		}

		$this->_session = new Zend_Session_Namespace('private');

		if ( !isset($_COOKIE['ec_session_closed']) ) {
			setcookie('ec_session_closed', true, strtotime('+1 year'), "/", '.' . Cubix_Application::getById(Cubix_Application::getId())->host);
		}

	}

	
	public function indexAction()
	{
		if ( $this->user->isAgency() || $this->user->isEscort() || $this->user->isMember() ) {
			$this->_redirect($this->view->getLink('private'));
		}
	}

	public function signupAction()
	{
		// $type = $this->_getParam('type');
		
		$this->view->type = $type;
		$signup_i18n = $this->view->signup_i18n = (object) array(
			'username_invalid' => 'Username must be at least 6 characters long',
			'username_invalid_characters' => 'Only "a-z", "0-9", "-" and "_" are allowed',
			'username_exists' => 'Username already exists, please choose another',
			'password_invalid' => 'Password must contain at least 6 characters',
			'password2_invalid' => 'Passwords must be equal',
			'email_invalid' => 'Email is invalid, please provide valid email address',
			'email_exists' => 'Email already exists, please enter another',
			'terms_required' => 'You must agree with terms and conditions',
			'form_errors' => 'Please check errors in form',
			'terms_required' => 'You have to accept terms and conditions before continue',
			'domain_blacklisted' => 'Domain is blacklisted',
            'user_type' => 'Choose your business',
			'username_equal_password' => __('username_equal_password'),
		);
		
		if ( $this->_request->isPost() ) {


			

			$user_type = $this->_getParam('user_type', $type);
            $user_t = $this->_getParam('user_type');
            $g_recaptcha_response = $this->_getParam('g-recaptcha-response');
      
            $validator = new Cubix_Validator();

			if ( ! in_array($user_type, array('member',  'escort', 'agency')) ) {
                $validator->setError('user_type', $signup_i18n->user_type);  
			}

            if ($user_type == 'escort' &&  ! in_array($user_t, array('member', 'vip-member', 'escort', 'agency')) ) {
                /* Update Grigor */
                $validator->setError('user_type', $signup_i18n->user_type);
                /* Update Grigor */

			}

			$data = new Cubix_Form_Data($this->_request);
			$fields = array(
				'promotion_code' => '',
				'username' => 'notags|xss',
				'password' => '',
				'password2' => '',
				'email' => 'notags|xss',
				'terms' => 'int',
				'subscribe' => 'int',
				'g-recaptcha-response' => ''
				//'recaptcha_response_field' => ''
			);
			$data->setFields($fields);
			$data = $data->getData();
			
			$data['username'] = substr($data['username'], 0, 24);
			$data['user_type'] = $user_t;
			$this->view->data = $data;
			
			$has_bl_username = false;
			foreach($this->blacklisted_usernames as $bl_username){
				if( strpos( $data['username'], $bl_username) !== false){
					$has_bl_username = true;
					BREAK;
				}
			}
			
			$client = new Cubix_Api_XmlRpc_Client();
			$model = new Model_Users();
			
			if ( strlen($data['username']) < 6 ) {
				$validator->setError('username', $signup_i18n->username_invalid);
			}
			elseif ( ! preg_match('/^[-_a-z0-9]+$/i', $data['username']) ) {
				$validator->setError('username', $signup_i18n->username_invalid_characters);
			}
			//elseif (stripos($data['username'], 'admin') !== FALSE) {
			elseif ($has_bl_username) {
				$validator->setError('username', $signup_i18n->username_exists);
			}
			elseif ( $client->call('Users.getByUsername', array($data['username'])) ) {
				$validator->setError('username', $signup_i18n->username_exists);
			}

			if ( $data['password'] == $data['username']) {
				$validator->setError('password', $signup_i18n->username_equal_password);	
			}
			
			if ( strlen($data['password']) < 6 ) {
				$validator->setError('password', $signup_i18n->password_invalid);
			}
			elseif ( $data['password'] != $data['password2'] ) {
				$validator->setError('password2', $signup_i18n->password2_invalid);
			}
			
			if ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
				$validator->setError('email', $signup_i18n->email_invalid);
			}
			elseif( $client->call('Application.isDomainBlacklisted', array($data['email'])) )
			{
				$validator->setError('email', $signup_i18n->domain_blacklisted);
			}
			elseif ( $client->call('Users.getByEmail', array($data['email'])) ) {
				$validator->setError('email', $signup_i18n->email_exists);
			}
			
			if ( ! $data['terms'] ) {
				$validator->setError('terms', $signup_i18n->terms_required);
			}
			if (  $g_recaptcha_response == '' ) {
				$validator->setError('captcha', 'Captcha is required');
			}
			
			if ( is_null($this->_getParam('ajax')) ) {
				/*$captcha = Cubix_Captcha::verify($this->_request->recaptcha_response_field);

				$captcha_errors = array(
					'invalid-site-public-key' => 'We weren\'t able to verify the public key.',
					'invalid-site-private-key' => 'We weren\'t able to verify the private key.',
					'invalid-request-cookie' => 'The challenge parameter of the verify script was incorrect.',
					'incorrect-captcha-sol' => 'The CAPTCHA solution was incorrect.',
					'verify-params-incorrect' => 'The parameters to /verify were incorrect, make sure you are passing all the required parameters.',
					'invalid-referrer' => 'reCAPTCHA API keys are tied to a specific domain name for security reasons.',
					'recaptcha-not-reachable' => 'reCAPTCHA never returns this error code. A plugin should manually return this code in the unlikely event that it is unable to contact the reCAPTCHA verify server.'
				);



				if (!is_bool($captcha) ) {
					$validator->setError('captcha', $captcha_errors[$captcha]);
				}*/
				// if ( ! strlen($data['captcha']) ) {
				// $validator->setError('captcha', 'Captcha is required');
				// }
				// else {
				// 	$session = new Zend_Session_Namespace('captcha');
				// 	$orig_captcha = $session->captcha;

				// 	if ( strtolower($data['captcha']) != $orig_captcha ) {
				// 		$validator->setError('captcha', 'Captcha is invalid');
				// 	}
				// }
			}

			$this->view->errors = array();
			$result = $validator->getStatus();

			if ( ! $validator->isValid() ) {
				$this->view->errors = $result['msgs'];
				if ( ! is_null($this->_getParam('ajax')) ) {
					echo(json_encode($result));
					ob_flush();
					die;
				}
			}
			else {
				// will set the most free sales user id
				
				// Users model
				$user = new Model_UserItem(array(
					'username' => $data['username'],
					'email' => $data['email'],
					'user_type' => $user_type == 'vip-member' ? 'member' : $user_type
				));

				/*if (in_array($data['promotion_code'], array('123456', '9988')))
					$user->sales_user_id = 16;
				else
					Model_Hooks::preUserSignUp($user);*/

                //EGUK-217
                if (in_array($user_type,array('escort','agency'))){
                    $lastUserId = $model->getLastUserId();
                    if ($lastUserId % 2 == 0)
                    {
                        $user->sales_user_id = 192;//Jolyne
                    }else{
                        $user->sales_user_id = 191;//CARRIE
                    }
                }else{
                    $user->sales_user_id = 157; //Katie
                }
                // EGUK-217 end

				$salt_hash = Cubix_Salt::generateSalt($data['email']);
				$user->salt_hash = $salt_hash;
				$user->password = Cubix_Salt::hashPassword($data['password'], $salt_hash);
				
				$new_user = $model->save($user);
				
				/* add to newsman */
				$subscribe = $this->_getParam('subscribe');

				if($subscribe){
					$ip = Cubix_Geoip::getIP();

					$m_n_i = new Cubix_Newsman_Ids();
					$n_ids = $m_n_i->get(Cubix_Application::getId());

					if (count($n_ids) > 0) {
					    $array_keys = array_keys($n_ids);
						$list_id = reset($array_keys);
						$conf = $m_n_i->getConf();

						try {
							$client = new Cubix_Newsman_Client($conf['user_id'], $conf['api_key']);
							$client->setCallType("rest");
							$client->setApiUrl("https://ssl.nzsrv.com/api");

							$subscriber_id = $client->subscriber->saveSubscribe($list_id, $data['email'], '', '', $ip, array());

							$n_user_type = $user_type;
							if ($n_user_type == 'vip-member') $n_user_type = 'member';

                            $type_key = array_key_exists($user_type, $n_ids[$list_id]) ? $user_type : 'member';
                            $segment_id = $n_ids[$list_id][$type_key];

							if ($subscriber_id)
							{
								$client->subscriber->addToSegment(
									$subscriber_id, 
									$segment_id 
								);
							}
						} catch (Exception $e)
						{
							//var_dump($e);die;
						}
					}
				}
				/**/
				
				if ( 'escort' == $user_type ) {
					$m_escorts = new Model_Escorts();
					
					$escort = new Model_EscortItem(array(
						'user_id' => $user->new_user_id
					));
					
					// will set current app country id
					Model_Hooks::preEscortSignUp($escort);
					
					$escort = $m_escorts->save($escort);
					
					// will update escort status bits using api
					Model_Hooks::postEscortSignUp($escort);
				}
				elseif ( 'agency' == $user_type ) {
					$m_agencies = new Model_Agencies();

                    /* Creating Slug in agency table */
                    $slug = Cubix_Utils::makeSlug($data['username']);
                    /**/
					
					$agency = new Model_AgencyItem(array(
						'user_id' => $user->new_user_id,
						'name' => ucfirst($user->username),
						'slug' => $slug
					));
					
					// will set current app country id
					Model_Hooks::preAgencySignUp($agency);
					
					$agency = $m_agencies->save($agency);
					
					// 
					Model_Hooks::postAgencySignUp($agency);
				}
				elseif ( 'member' == $user_type || 'vip-member' == $user_type ) {
					unset($this->_session->want_premium);
					$m_members = new Model_Members;

					$member = new Model_MemberItem(array(
						'user_id' => $user->new_user_id,
						'email'   => $user->email
					));

					$this->view->n_member = $m_members->save($member);
				}

				//newsletter email log
				$emails = array(
					'old' => null,
					'new' => $user->email
				);
				Cubix_Api::getInstance()->call('addNewlstterEmailLog', array($user->new_user_id, $user_type, 'add', $emails));
				//
				
				// will send activation email
				$user->reg_type = $user_type;
				Model_Hooks::postUserSignUp($user);

                $forum2 = new Cubix_Forum2Api();

                $forum2->RegisterUser($new_user->username, $new_user->email);
                
				if ( ! is_null($this->_getParam('ajax')) ) {
					$result['msg'] = "
						<h1 style='margin-bottom:20px'><img src='/img/" . Cubix_I18n::getLang() . "_h1_memberssignup.gif' alt='Members signup' title='Members signup' /></h1>
						<div style='width:520px; background: none; margin-bottom:0' class='cbox-small'>
							<h3>". Cubix_I18n::translate('congratulations') . "</h3>
							<p>" . Cubix_I18n::translate('successfully_signed_up', array('SITE_URL' => Cubix_Application::getById()->url, 'SITE_NAME' => Cubix_Application::getById()->title)) . "</p>

							<h3>" . Cubix_I18n::translate('complete_your_registration') . "</h3>
							<p>&nbsp;</p>

							<h4>
								" . Cubix_I18n::translate('signup_successful_04') . "
							</h4>
						</div>
					";
					$result['signup'] = true;
					echo json_encode($result);
					die;
				}
				else {
					if ( $user_type == 'vip-member' ) {
						$this->_session->want_premium = (object) array('member_data' => array('id' => $this->view->n_member->getId()), 'email' => $user->email);
						header('Location: /' . Cubix_I18n::getLang() . '/private-v2/upgrade');
						exit;
						//$this->_helper->viewRenderer->setScriptAction('signup-success-cc');
					}
					else {
						$this->_helper->viewRenderer->setScriptAction('signup-success');
					}
				}
			}
		}

	}

	public function signinAction()
	{
		$this->view->data = array('username' => '');
		$this->view->errors = array();
		

		/*if (isset($_GET['test']))
		{
			if(isset($_SERVER['HTTP_CLIENT_IP']))
				$ip = $_SERVER['HTTP_CLIENT_IP'];
			else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
				$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			else if(isset($_SERVER['REMOTE_ADDR']))
				$ip = $_SERVER['REMOTE_ADDR'];
			else
				$ip = 'UNKNOWN';

			$client = new Cubix_Api_XmlRpc_Client();
			try {
				$user = $client->call('Users.getClientLocation', array($ip));
				print_r($user);die;
			}
			catch ( Exception $e ) {
				var_dump($client->getHttpClient()->getLastResponse()->getBody());
				die;
			}
		}*/

        if ( $this->_request->isPost() ) {

			$username = trim($this->_getParam('username'));
			$password = trim($this->_getParam('password'));
			//$remember_me = intval($this->_getParam('remember_me'));
			
			
			$this->view->data['username'] = $username;

			$validator = new Cubix_Validator();

			if ( ! strlen($username) ) {
				$validator->setError('username', 'Username is required');
			}


			if ( ! strlen($password) ) {
				$validator->setError('password', 'Password is required');
			}

			if ( strlen($username) && strlen($password) ) {
				$model = new Model_Users();

				if ( ! $user = $model->getByUsernamePassword($username, $password) ) {
					$validator->setError('username', 'Wrong username/password combination');
                }

				if ( $user ) {
//                    if ( STATUS_ACTIVE != $user->status ) {
//                        $validator->setError('username', 'Your account is not active yet');
//                    }

                    if($this->_hasStatusBit($user->escort_data['escort_status'],ESCORT_STATUS_ADMIN_DISABLED) || $this->_hasStatusBit($user->escort_data['escort_status'],ESCORT_STATUS_DELETED)){
                        $validator->setError('status_error', __('status_error'));
                    }

                    if($user->status == -1){
                        $validator->setError('verification_error', __('verification_error'));
                    }

                    if($user->status == STATUS_ADMIN_DISABLED){
                        $validator->setError('status_error', __('agency_disabled'));
                    }
                }

			}


			$result = $validator->getStatus();

			if ( ! $validator->isValid() ) {
				
				$this->view->errors = $result['msgs'];
				
				if ( ! is_null($this->_getParam('ajax')) ) {
					echo(json_encode($result));
					ob_flush();
					die;
				}
			}
			else {
				/*$model = new Model_Users();
				
				if ( ! $user = $model->getByUsernamePassword($username, $password) ) {
					$this->view->errors['username'] = 'Wrong username/password combination';
					return;
				}
				
				if ( STATUS_ACTIVE != $user->status ) {
					$this->view->errors['username'] = 'Your account is not active yet';
					return;
				}*/
				
				$has_active_package = false;
				$is_susp = false;

                $first_timeLogin  = Model_Users::get_last_login_date( $user->id );
                $first_timeLogin = ( is_null($first_timeLogin) ?  true : false);

                if (in_array($user->user_type, array('escort', 'agency'))) {
                    $client = new Cubix_Api_XmlRpc_Client();
                    if ($first_timeLogin && !in_array($user->sales_user_id, array(192, 191))) {

                        $lastUserId = $model->getLastUserId();
                        if ($lastUserId % 2 == 0) {
                            $client->call('Users.assignToSales', array(192,$user->id));//Jolyne
                        } else {
                            $client->call('Users.assignToSales', array(191,$user->id));//CARRIE
                        }
                    }
                }

				if ( $user->user_type == 'agency' ) {
					$_SESSION['fc_gender'] = 2;
					
					/***********************************/
					$client = new Cubix_Api_XmlRpc_Client();
					$has_active_package = $client->call('Escorts.hasPaidActivePackageForAgency', array($user->id));
				}
				else if ( $user->user_type == 'member' ) {
					$_SESSION['fc_gender'] = 1;
				}
				else if ( $user->user_type == 'escort' ) {
					$_SESSION['fc_gender'] = 2;
					
					/***********************************/
					$client = new Cubix_Api_XmlRpc_Client();
					$has_active_package = $client->call('Escorts.hasPaidActivePackageForEscort', array($user->id));
					$m_e = new Model_Escorts();
					$is_susp = $m_e->isSuspicious($user->id);
				}
				
				$_SESSION['fc_username'] = $username;
				$_SESSION['fc_password'] = $password;
				
				Zend_Session::regenerateId();
				$user->sign_hash = md5(rand(100000, 999999) * microtime(true));
				$user->has_active_package = $has_active_package;
				$user->is_susp = $is_susp;
				
				/*-->Setting chat info*/
				$chat_info = array(
					'nickName'	=> $user->username,
					'userId'	=> $user->id,
					'userType'	=> $user->user_type,
					'imIsBlocked' => $user->im_is_blocked
				);
				if ( $user->user_type == 'escort' ) {
					$m = new Model_Escorts();
					$e = $m->getByUserId($user->id);
					$hasProfile = $e->hasProfile();
					$chat_info['nickName'] = $e->showname . ' (Escort)';
					$chat_info['isSusp'] = $is_susp;
					$chat_info['hasActivePackage'] = $has_active_package;
					$chat_info['link'] = '/escort/' . $e->showname . '-' . $e->id;
					
					//Getting main image url
					$parts = array();
					if ( strlen($e->id) > 2 ) {
						$parts[] = substr($e->id, 0, 2);
						$parts[] = substr($e->id, 2);
					}
					else {
						$parts[] = '_';
						$parts[] = $e->id;
					}
					$catalog = implode('/', $parts);
					
					$app = Cubix_Application::getById();
					if ($e->photo_hash)
                    {
                        $chat_info['avatar'] = 'https://pic.' . $app->host . '/' . $app->host . '/' . $catalog . '/' . $e->photo_hash . '_lvthumb.' . $e->photo_ext;
                    }else{
                        $chat_info['avatar'] = '/img/chat/img-no-avatar.gif';
                    }

				} elseif ( $user->user_type == 'agency' ) {
					$m = new Model_Agencies();
					$a = $m->getByUserId($user->id);
					$chat_info['nickName'] = $a->name;
					$chat_info['hasActivePackage'] = $has_active_package;
				} elseif ( $user->user_type == 'member' ) {
					$chat_info['link'] = '/member/' . $user->username;
				
					
				}
				$user->chat_info = $chat_info;

				/* watching settings */
				if ($user->user_type == 'member')
				{
					$m_m = new Model_Members();
					$additional_info = $m_m->getAdditionalInfo($user->id);
					$user->escorts_watched_type = $additional_info['escorts_watched_type'];
				}
				/**/

				Model_Users::setCurrent($user);

				/*set client ID*/
				Model_Reviews::createCookieClientID($user->id);
				/**/

				if ( $remember_me ) {
					$this->addRememberMeCook($username, $password);
				}

				//$this->removeXmasCook('xmas_simple_closed');
                $isLoged  = Model_Users::get_last_login_date( $user->id );
				if($user->user_type != 'member'){
                    $url = ( is_null($isLoged) && !$hasProfile) ?  'private-complete-profile' : 'private';
                }else{
                    $url = 'private-member-profile';
                }

				Model_Hooks::preUserSignIn($user->id);
                //login into forum
                // -----------------------
                $forum2 = new Cubix_Forum2Api();
                $forum2->Login($user->username, $user->email);

				if ( ! is_null($this->_getParam('ajax')) ) {
					$result['msgs'] = "You have successfully loged in</div>";
					$result['signin'] = true;
					echo json_encode($result);
					die;
				}
				else {
					
					//http://www.escortguide2.co.uk.test/account/signin#?referrer=private/billing/index
					//http://www.escortguide2.co.uk.test/account/signin?referrer=private/billing/index
					///private#billing/index/
					if(isset($_GET['referrer'])){
						if(1 or strpos($_GET['referrer'], 'private') === false){
							$this->_response->setRedirect( Cubix_Application::getById()->url . '/' . $_GET['referrer']);
						}else{
							$_GET['referrer'] = str_replace('private/', 'private#', $_GET['referrer']);
							$this->_response->setRedirect(Cubix_Application::getById()->url .'/'. $_GET['referrer']);
						}
					}else{
						$this->_response->setRedirect($this->view->getLink($url));
					}
				}
			}
		}
		
	}

	public function signoutAction()
	{
		
		Model_Users::setCurrent(NULL);
		Zend_Session::regenerateId();

		$cookie_name = 'signin_remember_' . Cubix_Application::getId();
		if ( isset($_COOKIE[$cookie_name]) && $_COOKIE[$cookie_name] ) {
			setcookie($cookie_name, null, strtotime('- 1 year'), "/");
		}
		$redirect_link = $this->view->getLink();
		
		if (strpos($_SERVER['HTTP_REFERER'], 'private') === false) {
			$redirect_link = $_SERVER['HTTP_REFERER'];
		}

        $forum2 = new Cubix_Forum2Api(false);
        $forum2->LogOut();

		$this->_response->setRedirect($redirect_link);
	}

	public function forgotAction()
	{
		$this->view->data = array('email' => '');
		$this->view->errors = array();

		if ( $this->_request->isPost() ) {
			$this->view->data['email'] = $email = trim($this->_getParam('email'));

			if ( ! strlen($email) ) {
				$this->view->errors['email'] = 'Please enter a  email address.';
				return;
			}

			$model = new Model_Users();
			if ( ! ($user = $model->forgotPassword($email)) ) {
				$this->view->errors['email'] = 'This email is not registered on our site';
				return;
			}

			Cubix_Email::sendTemplate('forgot_verification', $email, array(
				'username' => $user['username'],
				'hash' => $user['email_hash']
			));

			$this->_helper->viewRenderer->setScriptAction('forgot-success');
		}
	}

	public function changePassAction()
	{
		
		$client = new Cubix_Api_XmlRpc_Client();
		if ( $this->_request->isPost() ) {
			$pass = $this->_getParam('password');
			$conf_pass = $this->_getParam('password2');
			$hash = $this->_getParam('hash');
			$id = intval($this->_getParam('id'));
			$validator = new Cubix_Validator();
			if ( strlen($pass) < 6 ) {
				$validator->setError('password', __('password_invalid'));
			}
			elseif ( $pass != $conf_pass ) {
				$validator->setError('password2',  __('password_missmatch'));
			}
			elseif ( ! preg_match('/[a-f0-9]{32}/', $hash) ) {
				$validator->setError('hash',  'Invalid hash !');
			}
			$this->view->errors = array();
			$result = $validator->getStatus();

			if ( ! $validator->isValid() ) {
				$this->view->errors = $result['msgs'];
			}
			else{
				$client->call('Users.checkUpdatePassword', array($id, $hash, $pass));
				$this->_helper->viewRenderer->setScriptAction('change-pass-success');
			}
		}
		else
		{
			$hash = $this->_getParam('hash');
			$username = $this->_getParam('username');
			$error = false;
			if(!isset($username) || !isset($hash)){
				$error = true;
			}
			$username = substr($username, 0, 24);
			if ( strlen($username) < 6 ) {
				$error = true;
			}
			elseif ( ! preg_match('/^[-_a-z0-9]+$/i', $username) ) {
				$error = true;
			}
			elseif ( ! preg_match('/[a-f0-9]{32}/', $hash) ) {
				$error = true;
			}

			if(!$error)
			{
				$id = $client->call('Users.getByUsernameMailHash', array($username, $hash));
				if($id){
					$this->view->id = $id;
					$this->view->hash = $hash;

				}
				else{
					$error = true;
				}

			}
			if ($error){
				$this->_response->setRawHeader('HTTP/1.1 404 Not Found');
				$this->_forward('error', 'error', null, array('error_msg' => '404 Page Not Found. Please try the following links instead.'));
			}
		}
	}

	public function activateAction()
	{

		$hash = $this->_getParam('hash');
		$email = $this->_getParam('email');

		$model = new Model_Users();
		

		if ( ! $user = $model->activate($email, $hash) ) {
			$this->_response->setRedirect($this->view->getLink('signin'));
			return;
		}
		
		Model_Hooks::postUserActivate($user);
	}

    private function _hasStatusBit($old_status, $status)
    {
        $result = true;
        $result = ($old_status & $status) && $result;
        return $result;
    }

    public function secureSigninAction()
    {
        $this->view->layout()->disableLayout();

        $secure_token = $this->_getParam('secure_token');

        $validator = new Cubix_Validator();
        $model = new Model_Users();
        $user = $model->getByToken($secure_token);

        $result = $validator->getStatus();

        if ( $user ) {

            $has_active_package = false;
            $is_susp = false;

            $client = new Cubix_Api_XmlRpc_Client();

            if ( $user->user_type == 'agency' ) {
                $_SESSION['fc_gender'] = 2;
            }
            else if ( $user->user_type == 'member' ) {
                $_SESSION['fc_gender'] = 1;
            }
            else if ( $user->user_type == 'escort' ) {
                $_SESSION['fc_gender'] = 2;

                /***********************************/
                $has_active_package = $client->call('Escorts.hasPaidActivePackageForEscort', array($user->id));
                $m_e = new Model_Escorts();
                $is_susp = $m_e->isSuspicious($user->id);
            }


            // UNCOMMENT WHEN DEPLOYED
            //Zend_Session::regenerateId();

            $user->sign_hash = md5(rand(100000, 999999) * microtime(true));
            $user->has_active_package = $has_active_package;
            $user->is_susp = $is_susp;

            if ( $user->user_type == 'escort' ) {
                $user->need_phone_verification = $client->call('Users.needPhoneVerificationByUserId', array($user->id));

            } elseif ( $user->user_type == 'agency' ) {
                $m = new Model_Agencies();
                $a = $m->getByUserId($user->id);
                // $m->updateLastModified($a->id);
                $user->need_phone_verification = $client->call('Users.agencyNeedPhoneVerification', array($user->id));

            }

            Model_Users::setCurrent($user);
            Model_Reviews::createCookieClientID($user->id);

            $first_timeLogin  = Model_Users::get_last_login_date( $user->id );
            $first_timeLogin = ( is_null($first_timeLogin) ?  true : false);

            Model_Hooks::preUserSignIn($user->id);

            // login into forum
            $forum2 = new Cubix_Forum2Api();
            $forum2->Login($user->username, $user->email);
            //

            if ( ! is_null($this->_getParam('ajax')) ) {
                $result['signin_type'] = ucfirst($user->user_type);
                echo json_encode($result);
                die;
            }
            else {
                if($first_timeLogin){
                    $hash  = ($user->user_type == 'escort') ? '#complete-profile-step':'#add-new';
                    $this->_response->setRedirect($this->view->getLink('dashboard').$hash);
                }else{
                    $hash = ($user->user_type == 'escort' && $user->getEscort()->status == 67) ? '#':'';
                    $this->_response->setRedirect($this->view->getLink('private'));
                }
            }
        }
    }


}
