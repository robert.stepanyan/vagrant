<?php

class EscortsController extends Zend_Controller_Action
{
	public static $linkHelper;

	protected $_client;

	public function init()
	{
		self::$linkHelper = $this->view->getHelper('GetLink');
		$this->_client = new Cubix_Api_XmlRpc_Client();
	}


	public function indexAction()
	{
		$this->view->req = $req = $this->_getParam('req');
		$config = Zend_Registry::get('escorts_config');

		$cache = Zend_Registry::get('cache');
		if (preg_match('#^\/[0-9]#', $req) ) {
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			die;
		}

		if (strpos($req, '/new/city_') !== false)
		{
			$ar = explode('/', $req);
			unset($ar[1]);
			$uri = implode('/', $ar);
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /escorts' . $uri);
			die;
		}


		if (strlen($_SERVER['REQUEST_URI']) > 1)
		{
			$a = explode('/', $_SERVER['REQUEST_URI']);

			if (!end($a))
			{
				$new_url = substr_replace($_SERVER['REQUEST_URI'], "", -1);
				header('HTTP/1.1 301 Moved Permanently');
				header('Location: ' . $new_url);
				die;
			}
		}
		/**/

		/* redirect regions pages to index */
		if (strpos($req, '/region_') !== false)
		{
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			die;
		}

		if (strpos($req,'/city_gb_') !== false){
		    $uri = str_replace("city_gb_",'', $req);
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: /escorts'.$uri);
            die;
        }


        $this->view->isHomePage = $isHomePage = ($_SERVER['REQUEST_URI'] === '/' || ($this->_request->isHomePage && $this->_request->isHomePage == "1"))? true : false;

		// <editor-fold defaultstate="collapsed" desc="If it is ajax request, disable the layout and render only list.phtml">
		if ( ! is_null($this->_getParam('ajax')) ) {
			$this->view->layout()->disableLayout();
			$this->_helper->viewRenderer->setScriptAction('list');
			$f_price_from = $this->view->f_price_from = (int) $this->_request->price_from;
			$f_price_to = $this->view->f_price_to   = (int) $this->_request->price_to;

			$f_age_from = $this->view->f_age_from = (int) $this->_request->age_from;
			$f_age_to = $this->view->f_age_to   = (int) $this->_request->age_to;

			$f_incall = $this->view->f_incall = (int) $this->_request->f_incall;
			$f_outcall = $this->view->f_outcall   = (int) $this->_request->f_outcall;

			//$f_video = $this->view->f_video   = (int) $this->_request->video;
			$f_verified_contact = $this->view->f_verified_contact   = (int) $this->_request->verified_contact;
			$f_verified = $this->view->f_verified   = (int) $this->_request->verified;
			$f_review = $this->view->f_review   = (int) $this->_request->review;
			$f_video = $this->view->f_video = (int) $this->_request->video;
			$f_independent = $this->view->f_independent = (int) $this->_request->f_independent;
			$f_agency = $this->view->f_agency = (int) $this->_request->f_agency;
			$f_gender = $this->view->f_gender = (int) $this->_request->f_gender;
			$f_city = $this->view->city_id = $this->_request->city_id;

			$external_filters = array(
				'price-from' => $f_price_from,
				'price-to' => $f_price_to,
				'age-from' => $f_age_from,
				'age-to' => $f_age_to,
				'incall' => $f_incall,
				'outcall' => $f_outcall,
				'f_agency' => $f_agency,
				'f_independent' => $f_independent,

				//'video' => $f_video,
				'verified-contact' => $f_verified_contact,
				'verified' => $f_verified,
				'review' => $f_review,
                'video' => $f_video,
                'name' => $this->_request->name
			);
		}
		// </editor-fold>


		//$this->_request->setParam('req', $req);
		// </editor-fold>

		$params = array(
			'sort' => 'newest',
			'filter' => array(array('field' => 'girls', 'value' => array())),
			'page' => 1
		);

		$static_page_key = 'main';
		$is_tour = $this->_request->is_tour;
		$is_city_tour = false;
		$is_upcoming = $this->_request->is_upcoming;
		$gender = GENDER_FEMALE;
		$is_agency = $this->_request->is_agency;

		$req = explode('/', $req);

		if($f_city){
		    $req[] = 'city_gb_'.$f_city;
        }


		foreach ( $req as $r ) {
			if ( ! strlen($r) ) continue;
			$param = explode('_', $r);

			if ( count($param) < 2 ) {

				switch( $r )
				{
					case 'nuove':
						$static_page_key = 'nuove';
						$params = array('sort' => 'new-escorts-list');
						$params['filter'][] = array('field' => 'new_arrivals', 'value' => array());
					break;
					case 'independents':
						$static_page_key = 'independents';
						$params['filter'][] = array('field' => 'independents', 'value' => array());
						$gender = GENDER_FEMALE;
					continue;
					case 'regular':
						$static_page_key = 'regular';
						$params['filter'][] = array('field' => 'regular', 'value' => array());
					continue;
					case 'agency':
						$static_page_key = 'agency';
						$is_agency = 1;
						$params['filter'][] = array('field' => 'agency', 'value' => array());
					continue;
					case 'boys':
						$static_page_key = 'boys';
						$params['filter'][0] = array('field' => 'boys', 'value' => array());
						$gender = GENDER_MALE;
					continue;
					case 'trans':
						$static_page_key = 'trans';
						$params['filter'][0] = array('field' => 'trans', 'value' => array());
						$gender = GENDER_TRANS;
					continue;
					case 'citytours':
						$static_page_key = 'citytours';
						$is_tour = 1;
						$is_city_tour = 1;
						$params['filter'][] = array('field' => 'tours', 'value' => array());
					continue;
					case 'upcomingtours':
						$static_page_key = 'upcomingtours';
						$is_tour = 1;
						$is_upcoming = 1;
						$params['filter'][] = array('field' => 'upcomingtours', 'value' => array());
					continue;
					case 'happy-hours':
						$static_page_key = 'happy-hours';
                        $params['filter'][] = array('field' => 'happy-hours', 'value' => array());
					continue;
					case 'chat-online':
						$static_page_key = 'chat-online';
					continue;
					case 'videos':
						$static_page_key = 'videos';
						$params['filter'][] = array('field' => 'videos', 'value' => array());
					continue;
					default:
                        $City  =  (new Model_Cities())->getBySlug($r);
                        if($City){
                            $params['city'] = $r;
                            $params['filter'][] = array('field' => 'city', 'value' => array($r));
                        }else{
                            $this->_response->setRawHeader('HTTP/1.1 404 Not Found');
                            $this->_forward('error', 'error', null, array('error_msg' => '404 Page Not Found. Please try the following links instead.'));
                            return;
                        }
				}
			}
			else{
				$param_name = $param[0];
				array_shift($param);

				switch ( $param_name ) {
					case 'region':
					case 'state':
						$params['country'] = $param[0];
						$params['region'] = $param[1];
						$params['filter'][] = array('field' => 'region', 'value' => $param[1]);
					break;
					case 'city':
					case 'zone':
						$params[$param_name] = $param[1];
						$params['filter'][] = array('field' => $param_name, 'value' => array($param[1]));
					break;
					case 'name':
						$has_filter = true;

						$srch_str = "";
						if ( count($req) ) {
							foreach( $req as $r ) {
								if ( preg_match('#name_#', $r) ) {
									$srch_str = str_replace("name_", "", $r);
									$srch_str = str_replace("_", "\_", $srch_str);
								}
							}
						}

						$params['filter'][] = array('field' => 'name', 'value' => array('%' . $srch_str . '%'));

					break;
					/*default:
						if ( ! in_array($param_name, array('nuove', 'independents', 'regular', 'agency', 'boys', 'trans', 'citytours', 'upcomingtours', 'happy-hours', 'chat-online')) ) {
							$this->_response->setRawHeader('HTTP/1.1 404 Not Found');
							$this->_forward('error','error', null, array('error_msg' => '404 Page Not Found. Please try the following links instead.'));
							return;
						}*/
				}
			}
		}

		$filter_params = array(
			'order' => 'e.date_registered DESC',
			'limit' => array('page' => 1),
			'filter' => array()
		);

		// FOR ADVANCED FILTER
		if ( $this->_request->isPost() && $this->_request->name ) {
			$has_filter = true;
			$name = $this->_request->name;
			$srch_str = "";

			$srch_str = str_replace("_", "\_", $name);

			//$params['filter'][] = array('field' => 'name', 'value' => array('%' . $srch_str . '%'));
			//$filter_params['filter']['e.showname LIKE ?'] = '%' . $srch_str . '%';
			$filter_params['filter']['(e.showname LIKE ? OR e.agency_name LIKE ?)'] = array('%' . $srch_str . '%', '%' . $srch_str . '%');
		}

		// FOR ADVANCED FILTER

		$this->view->static_page_key = $static_page_key;

        //Katie stupid Logic | EGUK-396
		$_female = ($f_gender) ? $f_gender : GENDER_FEMALE;
		$_trans =  ($f_gender) ? $f_gender : GENDER_TRANS ;
		$_male = ($f_gender) ? $f_gender : GENDER_MALE;

		$this->view->footer_seo_text = $this->_init_seo_texts($params['city']);

		if(!$f_city && isset($params['city'])){
		    $this->view->city_id = $params['city'];
		    $model_cities = new Model_Cities();
		    $this->view->origin_city_id = $model_cities->getBySlug($params['city'])->id;

        }

		// Getting available tags for city
        $city_id = null;

		if ($f_city)
        {
            $city_id = $f_city;
        }elseif (isset($params['city']))
        {
            $city_id = $model_cities->getBySlug($params['city'])->id;
        }

        $available_tags = $this->_client->call('Cities.getAvailableTags', array($city_id));

        $this->view->available_tags = $available_tags;


		// <editor-fold defaultstate="collapsed" desc="Map parameters from url to SQL where clauses">
		$filter_map = array(
			'verified' => 'e.verified_status = 2',
			'brazil' => 'e.nationality_id = 7',
			'videos' => 'e.has_video = 1',
			'happy-hours' => 'e.hh_is_active = 1',
			'age' => 'e.age < ? AND e.age > ?',
			'ethnic' => 'e.ethnicity = ?',
			'height' => 'e.height < ? AND e.height > ?',
			'weight' => 'e.weight < ? AND e.weight > ?',
			'cup-size' => 'e.cup_size = ?',
			'hair-color' => 'e.hair_color = ?',
			'hair-length' => 'e.hair_length = ?',
			'eye-color' => 'e.eye_color = ?',
			'dress-size' => 'e.dress_size = ?',
			'shoe-size' => 'e.shoe_size = ?',
			'available-for-incall' => 'e.incall_type IS NOT NULL',
			'available-for-outcall' => 'e.outcall_type IS NOT NULL',
			'service-for' => 'FIND_IN_SET(?, e.sex_availability)',
			'smoker' => 'e.is_smoking = ?',
			'language' => 'FIND_IN_SET(?, e.languages)',
			'now-open' => 'e.is_now_open',

			'region' => 'r.slug = ?',
			'city' => 'ct.slug = ?',
			'cityzone' => 'c.id = ?',
			'zone' => 'cz.slug = ?',
			'name' => 'e.showname LIKE ?',

			// 'new_arrivals' => 'e.gender = ' . $_female,
			'new_arrivals' => 'DATE(e.date_registered) >= DATE(NOW() - INTERVAL 3 MONTH)',
			'independents' => 'eic.is_agency = 0 AND eic.gender = ' . $_female,
			'agency' => 'eic.is_agency = 1 AND eic.gender = ' . $_female,
			'boys' => 'eic.gender = ' . $_male,
			'trans' => 'eic.gender = ' . $_trans,
			'girls' => 'eic.gender = ' . $_female,

			'tours' => 'eic.is_tour = 1',
			'upcomingtours' => 'eic.is_upcoming = 1'
		);

		// <editor-fold defaultstate="collapsed" desc="Get page from request parameters">
		if ( $this->_request->page ) {
			$params['page'] = $this->_request->page;
		}

		$page = intval($params['page']);
		if ( $page == 0 ) {
			$page = 1;
		}

		$filter_params['limit']['page'] = $page;
		// </editor-fold>

		foreach ( $params['filter'] as $i => $filter ) {

			if ( ! isset($filter_map[$filter['field']]) ) continue;

			$value = $filter['value'];
			$filter_params['filter'][$filter_map[$filter['field']]] = $value;
		}
		// </editor-fold>

		// 3 girls from every agency EGUK-446
		if ( $static_page_key == 'main' && !$params['city'] && !$params['zone']) {
			$external_filters['show_on_main'] = 1;
		}
		///

		if ( $static_page_key != 'nuove' ) {
			$default_sorting = "tl";
			$current_sorting = $this->_request->getParam('sort', $default_sorting);

			$sort_types = array(
                'tl' => array('title' => 'top-listing', 'map' => 'top-listing'),
                'n' => array('title' => 'newest_first', 'map' => 'newest'),
				//'lcv' => array('title' => 'last_contact_verified', 'map' => 'last-contact-verified'),
				'pa' => array('title' => 'price_ascending', 'map' => 'price-asc'),
				'pd' => array('title' => 'price_descending', 'map' => 'price-desc'),
				'lm' => array('title' => 'last_modified', 'map' => 'last-modified'),
				//'lc' => array('title' => 'last_connection', 'map' => 'last-connection'),
				'mv' => array('title' => 'most_viewed', 'map' => 'most-viewed'),
				'a' => array('title' => 'alphabetically', 'map' => 'alpha'),
				'ca' => array('title' => 'city_ascending', 'map' => 'city-asc'),
			);

			if ( !array_key_exists($current_sorting, $sort_types) ) {
				$current_sorting = $default_sorting;
				//$this->_request->setParam('sort', $default_sorting);
			}

			if ( $current_sorting != $_COOKIE['sorting'] && $this->_request->isPost() ) {
				setcookie("sorting", $current_sorting, strtotime('+1 year'), "/", "." . Cubix_Application::getById()->host);
				$_COOKIE['sorting'] = $current_sorting;
			}

			if ( isset($_COOKIE['sorting']) && !$current_sorting) {
				$current_sorting = $_COOKIE['sorting'];
				//$this->_request->setParam('sort', $current_sorting);
			}

			$tmp = $sort_types[$current_sorting];
			unset($sort_types[$current_sorting]);
			$sort_types = array_merge(array( $current_sorting => $tmp), $sort_types);

			$params['sort'] = $sort_types[$current_sorting]['map'];
		}
		else{
			$sort_types = null;
			$current_sorting = 'new-escorts-list';
		}
		$this->view->sort_types = $sort_types;
		$this->view->current_sorting = $current_sorting;

		$model = new Model_Escorts();
		$count = 0;

		// <editor-fold defaultstate="collapsed" desc="If it is ajax request, disable the layout and render only list.phtml">
		if ( ! is_null($this->_getParam('ajax')) ) {
		    $params['sort'] = $sort_types[$current_sorting]['map'];
            $current_filter = $this->getFilterAction();

            $efCacheKey = sha1(json_encode([$filter_params['filter'], $current_filter, $is_upcoming, $is_tour]));
            $existing_filters = $cache->load($efCacheKey);
            if (empty($existing_filters)) {
                $existing_filters = Model_Escort_List::getActiveFilter($filter_params['filter'], $current_filter, $is_upcoming, $is_tour);
                $cache->save($existing_filters, $efCacheKey, array());
            }

			$where_query = $this->getFilterAction($existing_filters);

			$have_filter = false;
			if ( count($where_query) ) {
				foreach ( $where_query as $q ) {
					if ( strlen($q['query']) ) {
						$have_filter = true;
						break;
					}
				}
			}

			if ($this->_getParam('ajax_filtered'))
            {
                $filter_params['filter']['ajax_filtered'] = true;
            }

			if ( count($external_filters) ) {
				foreach ($external_filters as $key => $value) {
					if ( $value ) {
						$have_filter = true;
						break;
					}
				}
			}

			$this->view->have_filter = $have_filter;

			$filter_body = $this->view->render('escorts/get-filter.phtml');

			$this->view->layout()->disableLayout();
			$this->_helper->viewRenderer->setScriptAction('list');
		}

		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Cache key generation FOR NEW FILTER">
		$filters_str = '';
		$f_params = $this->_request->getParams();
		$f_params_exclude = array('lang_id', 'module', 'action', 'controller', 'ajax');

		foreach ( $filter_params['filter'] as $k => $filter ) {
			if ( ! is_array($filter) ) {
				$filters_str .= $k . '_' . $filter;
			}
			else {
				$filters_str .= $k;
				foreach($filter as $f) {
					$filters_str .= '_' . $f;
				}
			}
		}

		foreach ( $f_params as $k => $value ) {
			if ( ! in_array($k, $f_params_exclude) ) {
				if ( ! is_array($value) ) {
				$filters_str .= $k . '_' . $value;
				}
				else {
					$filters_str .= $k;
					foreach($value as $f) {
						$filters_str .= '_' . $f;
					}
				}
			}
		}
		// </editor-fold>

		$external_filters_string = "";
		if ( count($external_filters) ) {
			foreach ($external_filters as $key => $value) {
				$external_filters_string .= $key . "_" . $value;
			}
		}

		$cache_key = 'v2_' . Cubix_I18n::getLang() . '_' . 	$params['sort'] . '_' . $filter_params['limit']['page'] . '_' . sha1($filters_str . (string) $has_filter . $external_filters_string);
		$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
        $this->view->params = $params;

		$this->view->is_main_page = $is_main_page = ($static_page_key == 'main' && ( ! isset($params['city']) && ! isset($params['zone']) && ! isset($params['region']) ));

		$this->view->filter_params = base64_encode(json_encode($filter_params['filter']));

		/*if ( $is_main_page ) {
			return $this->_forward('main-agency-split');
		}*/


		if ( $static_page_key == 'chat-online' ) {
			return $this->_forward('chat-online');
		}

		//SHOW UPCOMING TOURS BUTTON LOGIC
		$cache_key_tours = 'v2_upcoming_tours_count';
		$t_key = isset($params['city']) ? $params['city'] : 'total';
		$tours_count = $cache->load($cache_key_tours);
		if ( ! $tours_count || ! isset($tours_count[$t_key]) ) {
			if ( ! $tours_count ) $tours_count = array();
			$upcoming_filter = array();
			if ( isset($filter_params['filter']['ct.slug = ?']) ) {
				$upcoming_filter['ct.slug = ?'] = $filter_params['filter']['ct.slug = ?'];
			}

			Model_Escort_List::getTours(true, $upcoming_filter, 'random', 1, 45, $t_count, $where_query, $external_filters);

			$tours_count[$t_key] = $t_count;
			$cache->save($tours_count, $cache_key_tours, array());
		}
		$this->view->show_tours_btn = false;
		if ( ( ! $is_tour || $is_city_tour ) && $tours_count[$t_key] ) {
			$this->view->show_tours_btn = true;
		}

		/* Ordering by city from GeoIp */
		if ($params['sort'] == 'random')
		{
			$modelCities = new Model_Cities();

			$ret = $modelCities->getByGeoIp();

			if (is_array($ret))
			{
				$cache_key .= '_city_id_' . $ret['city_id'];
				$params['sort'] = $ret['ordering'];
			}
		}
		/**/

		$cache_key .= Cubix_Countries::blacklistCountryCacheKeyCase();

        if ( $static_page_key == 'videos')
            $config['perPage'] = 60;

        if ($this->_request->tag)
        {
            $cache_key .= 'tag'.$this->_request->tag;
        }

        $escorts = $cache->load($cache_key);


		if ( !$escorts ) {
            if ($this->_request->tag) {
                $taggedEscorts = $this->_client->call('Escorts.getTaggedEscorts', array($this->_request->tag));

                if (count($taggedEscorts))
                    $filter_params['filter']['tagged_escorts'] = implode(',',$taggedEscorts);
            }

            if ( isset($is_tour) && $is_tour ) {
				$escorts = Model_Escort_List::getTours($is_upcoming, $filter_params['filter'], $params['sort'], $filter_params['limit']['page'], $config['perPage'], $count, $where_query, $external_filters);
			}elseif ( $isHomePage && !$filter_params['filter']['ajax_filtered'])
            {
                $escorts = Model_Escort_List::getFiltered($filter_params['filter'], $params['sort'], $filter_params['limit']['page'], 5555, $count, 'regular_list', $where_query, false, $external_filters);
            }else {
				$escorts = Model_Escort_List::getFiltered($filter_params['filter'], $params['sort'], $filter_params['limit']['page'], $config['perPage'], $count, 'regular_list', $where_query, false, $external_filters);
			}

            $cache->save($escorts, $cache_key, array());
            $cache->save($count, $cache_key . '_count', array());

        } else {
            $count = $cache->load($cache_key . '_count');
        }

        if (count($escorts) < 1) {
            $this->view->fallback_escorts = Model_Escort_List::getFiltered($filter_params['filter'], $params['sort'], $filter_params['limit']['page'], $config['perPage'], $count, 'regular_list', $where_query, false, null);
        }

        // Hardcode position for VIP Ads, if they match to criteria
        if(!empty($escorts)) {
            $escorts = $this->_hardcode_vips_position([
                //'country' => $country,
                'city' => $City->id,
                'vips' => $escorts
            ]);
            $gotd_escorts_count = 0;
            foreach ($escorts as $escort)
            {
                if ($escort->is_gotd)
                {
                    $gotd_escorts_count ++;
                }
            }
        }
		$this->view->filter_params = base64_encode(json_encode($filter_params['filter']));

		$this->view->gotd_count = $gotd_escorts_count;
		$this->view->count = $count;
		$this->view->escorts = $escorts;
		$this->view->current_tag = $this->_request->tag;

		$this->view->has_filter = $has_filter;
		$this->view->gender = $gender;
		$this->view->is_agency = $is_agency;
		$this->view->is_tour = $is_tour;
		$this->view->is_upcoming = $is_upcoming;
		$this->view->view_type = ($_COOKIE['view-type']) ? $_COOKIE['view-type'] : 'grid';

		/* watched escorts */
		$watched_escorts = $this->_watchedEscorts($escorts);
		$this->view->watched_escorts = $watched_escorts;
		/**/

		$this->view->html_cache_key = $cache_key . $static_page_key;

		//////////////////////////////////////////////////////////////
		/*if ($this->view->allowChat2())
		{*/
			$model = new Model_Escorts();
			$res = $model->getChatNodeOnlineEscorts(0, 1000);
			$ids = $res['ids'];
			$this->view->escort_ids = explode(',', $ids);

		/*}*/
		/////////////////////////////////////////////////
		if ( ! is_null($this->_getParam('ajax')) ) {
			$this->view->is_ajax = true;
			$escort_list = $this->view->render('escorts/list.phtml');
			die(json_encode(array('filter_body' => $filter_body, 'escort_list' => $escort_list, 'count' => $count)));
		}
	}

	private function _init_seo_texts($city = null)
    {
        $texts = [
            'london' => '
                <p>The capital of England has plenty of beautiful and intelligent girls who provide in-call London escort services throughout the city. Privacy is guaranteed, so you do not have to worry about this aspect absolutely at all. London escorts guarantee that you will relax and enjoy the whole experience to the fullest. There are also out-call services, so you are welcome to go for this option if this is what you actually want.</p>
                <p>Escortguide.co.uk is a London escorts directory where you will find exactly what you need. You can choose between lots of amazing girls, who will offer you an unforgettable experience. Just take a look at the London escort ads gallery and choose what you like the most. On our website, you can find London escorts that are extremely sexy, funny, welcoming, and intelligent as well. However, one thing is sure, you will definitely not be bored and the provided experience will be absolutely fantastic. Men all over the world request the services of London escorts while they are in the metropolis, and this is definitely not a taboo subject anymore these days. Therefore, you must not feel uncomfortable if you do so as well.</p>
                <p>The London escorts from here will surely make you feel absolutely great. No matter what your wishes are, you will be completely satisfied. You can choose a girl that you like by selecting the preferences. We want all our clients to be pleased and that’s why we provide only high-quality services. Not only your desires will be satisfied but you can also receive the best massage ever, as we also offer this type of service. Our wonderful London escorts girls and busty escorts are absolutely lovely. Just browse their pictures and choose the one that you like the most.</p>
                <p>This London escort agency does its best to be on top so that every client can feel safe and 100% happy. Another important aspect any client should seriously take into account is the fact that our beauties can also accompany them to different events. Our girls can either show you the city in case you are a tourist and you do not want to feel alone on your trip, or they can keep you company at a business event or at any other event you want. You just need to know exactly what services you want in order to look for the girl who can meet all your needs.</p>
                <p>All our London escorts are different and not all of them do the same things. There are different categories with girls that offer different services. Therefore, it is essential that you, as a client, don’t omit this essential detail. However, as mentioned earlier you should not worry about your privacy because we guarantee it for you. Nobody will ever know that you requested our services, as our girls are trained to not do anything wrong from this point of view.</p>
                <p>We have plenty of clients from all over the world and most of them actually came back for more than twice, which certainly means that our services are absolutely excellent. If you want to request the services of a London Escort for the first time then you will not need to worry about that because the steps you need to follow are very simple.</p>
                <p>As mentioned above you must choose any of our london escorts, the type of service you want, and the location as well like: Central London, Earls Court etc and we will make your wish come true in a short period of time. The entire procedure takes quite a short time and our seriousness is what recommends us, and this is exactly what we want from you, as a client, as well, so that everything turns out as planned. Once you meet the chosen escort, the only thing you will actually need to do is to highly relax and enjoy to the fullest your time spent with one of our charming London escorts. </p>
            ',
            'manchester' => '
                <p>Manchester is a city that should be on everyone’s radar all year-round. This bold and vibrant gem of a place has many things to offer you and your dream companion. For starters, it has some of the most famous and best clubs you could visit with an escort in Manchester. It’s said that the city has everything you could wish for, except a beach. Listen to live music, eat at a fancy restaurant, and participate in famous Pride celebrations with girls in Manchester. You’ll have a blast!</p>
                <p>Feeling horny and ready to party? On Escortguide you will find the perfect companion for you. Experiment to your heart’s desire with the sort of ladies and men you could only dream of. Would you love to go wild with a trans provider instead? No one’s stopping you. On the contrary, feel free to do as you please with whom you please all day and all night long. You deserve to enjoy the most amazing services in Manchester with professionals who really enjoy their jobs. You’ve earned some TLC and you’re about to receive it from the companions who advertises their escort services in Manchester on our platform.</p>
                <p>Are you tired of not having your fantasies fulfilled? Have you already looked for the ideal lady or guy on other websites and no one attracted your attention or turned out to be what you wanted when you met them in real life? You can stop your search, because we’re just where you need to be, and we have exactly what you can’t live without. </p>
                <p>Being with someone who understands you and helps you relax makes life exciting, don’t you think? Girls in Manchester know where it’s at. As do men and trans providers. You’ll find new ads being promoted on Escortguide every day. In other words: you will always be able to find the perfect people for you. All you have to do is this: after typing in our address, select your city. In a matter of seconds, you’ll see hundreds of girls in Manchester dying to play with you in every position and as many times as you’d like.</p>
                <p>After looking at the list for a bit, check further options, like Verified contact details, 100% verified photos, With Video, and so on. They’re there to make it easy for our users to narrow it down to the companions who’d be a match for them. Interested in men or trans instead? Click those options instead. Check their photos out, read the list of escort services in Manchester for each provider, find out how to contact them, see if they’re available when you’re available, and more.</p>
                <p>In case you’re looking for providers who belong with an agency, we have ads from them, too. Just click Agencies, browse the names for as long as you want, and choose the one with the sexiest and skilled girls in Manchester. The moment one or more of the women or guys catches your eye, visit their profiles to find out more about them. Be careful what contact method they prefer, what their sexual orientation is, when they can see you/you can see them, if they have reviews, and whatnot. Not liking what you read and what escort services in Manchester they have in their menu? Move on to the next beauty.</p>
                <p>Many an escort in Manchester feature videos on their profiles. If they don’t, but you’d love to see what you pay for, click our latest feature called Videos. You won’t be able to take your eyes off them, we promise. After your date is over and you want to write your impressions about the companion you met and their service, you can either add a review or a comment, but only if you’re registered as a member. Your newfound favorite escort in Manchester will appreciate you for it, just make sure you’re respectful.</p>
                <p>When it comes to sensitive data, rest assured that it’s completely safe with us. The talented team of security experts working behind the scenes is always alert. While you search for certain escort services in Manchester, they will see to it that you and that gorgeous escort in Manchester stay safe 24/7.</p>
            ',
            'birmingham' => '
                <p>Raise your hands who&rsquo;d like a taste of some amazing guys, trans, and girls in Birmingham. So that&rsquo;s all of you, then? Let us tell you that you&rsquo;d be making a most excellent choice. The appetites of the providers on Escortguide are voracious and their stamina is out of this world. You&rsquo;ll be in seventh heaven from the first touch until the last bed-shattering orgasm. The most satisfying escort services in Birmingham are just what you need and deserve in your life right now.</span></p>
                <p>One of UK&rsquo;s most populous cities, Birmingham is a must-see. Especially when summer marks its presence. Imagine walking the town&rsquo;s old and newer districts, eating delicious food in one of its popular restaurants, drinking great beverages, then ending the night between the legs of a skilled escort in Birmingham. Bliss, wouldn&rsquo;t you say? That&rsquo;s what we thought, which is why you should pay our platform a visit and choose the lady, guy, or trans professional of your dreams for a rough or sweet night. Make it count with just the kind of escort services in Birmingham you&rsquo;ve been fantasizing about.</span></p>
                <p>There are so many breath-taking companions looking forward to accompanying you on a ride to pleasure-land, that you&rsquo;ll have a hard time deciding which one to pick. Worry not, you have all day and all night to decide, because these girls in Birmingham aren&rsquo;t going anywhere. For starters, take your time and look at their photos. Like what you see? Click on the profiles to have a better grasp on their ages, personalities, nationalities, services, rates, and so on. Interested in a bisexual or a heterosexual escort in Birmingham? There are plenty of those for you to discover. Want to have a threesome or a foursome where men and women can participate? You can find those, too. Whatever your heart&rsquo;s desire, you&rsquo;ll get it here.</span></p>
                <p>If you spotted someone who might rock your world, check out whether they have reviews or comments. Nothing there? Be the first to test out the waters, then feel free to add a comment or a review of your own. Before you can reach that stage, make sure you know all about their availability, list of services, and preferred method of contact. Otherwise you might have surprises, e.g. that particular escort in Birmingham doesn&rsquo;t want to indulge in your favourite practices because they&rsquo;re not the same as hers or his. </span></p>
                <p>Enjoy a feast of the senses with professionals who use our platform to promote their excellent escort services in Birmingham. You will be taken care of by attentive and loving ladies, trans, and gentlemen. By the end of the date, you&rsquo;ll find it extra hard to leave them and you&rsquo;ll look forward to your second time. Yes, girls in Birmingham are that good! As are all the other types of providers from our website. </span></p>
                <p>Would you like to spend valuable moments in the company of ladies from agencies instead? Feel free to do it and see what it&rsquo;s like to be with an exclusive escort in Birmingham. Hint: it will be the best frolicking of your existence and that&rsquo;s the irrefutable truth. Let your body unwind under the soft caresses of butterfly fingers. Let handsome gentlemen give you what you never experienced with the guys in your life. Lose yourself in the most splendid escort services in Birmingham. </span></p>
                <p>While you take your sweet time coming across the most extraordinary girls in Birmingham, our behind-the-scenes team of experts in all things security work tirelessly to provide the safest experience. Say goodbye to threats of any kind, keep cool, and find the perfect date who will take you to the moon and back on a rollercoaster of sensations.</span></p>
            ',
            'edinburgh' => '
                <p>Ever fancied visiting a Scottish city and sampling the local beauties on an unforgettable weekend or weekday? Give Edinburgh a chance, you won&rsquo;t regret it. Yes, we all know that the weather here can put a damper on even the most patient of people. However, that shouldn&rsquo;t stop you from fucking the hottest girls in Edinburgh with aplomb. Nicknamed &ldquo;The Athens of the North&rdquo;, this city is more than excellent for exploration and for snapping beautiful photos to send to your family and friends back home. You can always partake in exciting festivals, eat like a king, drink their famous whisky, and party all night long with your favourite escort in Edinburgh.</p>
                <p>The best way to forget the world outside is to have a taste of exquisite escort services in Edinburgh. You will discover new and sensual ways to relieve your stress and relax with the city&rsquo;s most beautiful and talented ladies, men, and trans professionals. Escortguide has the best ads in town and we invite you to explore them at your leisure from the comfort of your room using your favourite mobile device. It&rsquo;s easy, fun, and you will always find someone available for you.</p>
                <p>Before you can enjoy the most mind-blowing orgasms of your entire life with girls in Edinburgh, feel free to get a feel of our platform. Look around for the numerous options that will help you find what you seek in a matter of minutes, check out photos and videos of providers, read up on escort services in Edinburgh, browse reviews and comments, locate agencies, and more. </p>
                <p>Now that you know what&rsquo;s what, go ahead and start your search for the perfect escort in Edinburgh. You can choose <em>Newest first</em>, <em>Price ascending</em> or <em>descending</em>, <em>last modified</em>, <em>random</em>, and so on. Go for the grid view or the list view, opt for <em>Verified contact results</em> or our other options, and wait for results. In seconds, you will start drooling like never. The girls in Edinburgh who choose to advertise with us are some of the most special ones you will meet. One night with them will not be enough to enjoy all the memorable things they can do for you.</p>
                <p>You need to experience escort services in Edinburg at least once to fully understand what makes the providers here so amazing. Just make sure the lady, gentleman, or trans companions of your choosing are up for the sort of kinks you&rsquo;re into. Say you found the ideal escort in Edinburgh for you, but she or he doesn&rsquo;t offer what you most crave in bed. Don&rsquo;t beat yourself over it: simply move on to the next provider who caught your eye and check if that one is OK with what you fantasize about. No luck with that companion, either? Choose another one, and so on until you find your much-needed sweet escape.</p>
                <p>The horniest girls in Edinburgh are currently open for business. Are you? While you conduct your business with them, our security team will keep an eye on suspicious behaviour so that you don&rsquo;t have to worry about a thing except having fun. </p>
                <p>If you&rsquo;re not too sure about getting hot and heavy with an independent escort in Edinburgh, there are always agency girls to knock your socks off. Keep an eye out on our <em>New Escorts</em> if you ever want to see new faces. Ladies, the handsome studs in this gorgeous Scottish city can&rsquo;t wait to treat you like a queen and show you their extensive list of tingly escort services in Edinburgh. Trust us, you will want to get fucked by these guys all day and all night! </p>
            ',
            'bristol' => '
                <p>Bristol escorts are the best. Come and see for yourself!</p>
                <p>Welcome to Bristol, one of the happiest cities to work in the UK, as voted by the brits. It is also a very diverse city, with over 91 main languages spoken! </p>
                <p>In this melting pot of cultures, religions and languages, it is most likely that you will find the perfect escort, just for you. The best of them are all on EscortGuide.co.uk, so start browsing through our vast offer and plan a date with a gorgeous woman.</p>
                <p>The Bristol escorts are just like the city they leave in, versatile, energetic, elegant and sensual. The EscortGuide companions will raise to the occasion, being able to cater to all of your desires. They can be flirty and dirty minded, but they can also offer you great companionship, if you need a smart and elegant lady to accompany you to dinner or to a fancy event.</p>
                <p>From busty to fit, from slutty to romantic, from playful and funny, to kinky and wild, our top escorts meet all your deepest needs and desires. Come open minded and ready to have a great time and the EscortGuide escorts will not disappoint you.</p>
                ',
            'croydon' => '
                <p>Dating a Croydon escort is a must!</p>
                <p>A fast-growing tech cluster, Croydon is not to be missed when it comes to its elite escort offer. EscortGuide.co.uk has a great selection of some of the finest companions, gorgeous ladies, eager to please and to make you come back for more.</p>
                <p>So, next time you find yourself in the area, check out the Croydon escorts and book a date you’ll never forget. This busy town, full of commuters, will surprise you with its diversity when it comes to beautiful escorts.</p>
                <p>From redheads to brunettes, from busty ladies to super-fit ones, from the girlfriend experience to BDSM, from MILFs to pure and innocent babes, EscortGuide.co.uk has it all, here in Croydon!</p>
                <p>Don’t miss out on the chance of dating a hot, cosmopolite lady, that can offer you a real adventure of the senses. Browse through our escort offer and book a date now, with a VIP companion.</p>
                ',
            'gatwick' => '
                <p>Explore your fantasies with a Gatwick escort!</p>
                <p>Only 30 minutes by train, from London, besides its famous airport, Gatwick stands out from the crowd with its amazing selection of top escorts. So, if you find yourself in the area, don’t miss out on the chance of dating a gorgeous Gatwick companion with a great multicultural experience.</p>
                <p>The Gatwick escorts you’ll find on EscortGuide.co.uk are some of the best in UK. These ladies have seen it all and are prepared to offer you some wild fun in the sack. So, come open minded and book the lady of your choice, for a unique erotic experience.</p>
                <p>Check out the EscortGuide offer and book a date for an outcall or incall. </p>
                <p>Are you into the girlfriend experience? You have a passion for lace and leather? You want to be spanked, or you want the lady to talk dirty to you? Everything you desire you can find here. Unleash your fantasies and let them come to life.</p>
                ',
            'glasgow' => '
                <p>Get your classy act on, with a Glasgow escort!</p>
                <p>Glasgow, Scotland’s biggest city, with its more than 20 museums and art galleries, is world renowned as a cultural hub and a vibrant city with lots of personality. And its escorts are nothing less than elegant, well-educated and full of life. </p>
                <p>If you find yourself visiting this amazing city, and you feel the need to be accompanied by a gorgeous, high-class Glasgow escort, then don’t hesitate to check out the vast offer that EscortGuide.co.uk has on display.</p>
                <p>From classy ladies, with whom you can have a lovely dinner and a great conversation, to naughty girls that can culturize you in the art of love making, Glasgow escorts have it all. You just have to choose the lady of your dreams and book a date.</p>
                <p>The EscortGuide companions know how to deliver a complete and discreet escort service, you just have to name your fantasy and lay back and relax. From the perfect girlfriend experience, to naughtier sensations, the Glasgow escorts will deliver what you need and much more!</p>
                ',
            'heathrow' => '
                <p>The best escorts are the Heathrow escorts!</p>
                <p>With over 66 million passengers a year, Heathrow is one of the busiest airports in the world, and that makes it the perfect place to find that unique escort you’ve been dreaming about! The Heathrow escorts are versatile, experienced, well-educated and very open-minded when it comes to fulfilling some of the deepest fantasies of their dates.</p>
                <p>If you find yourself in the area, check out EscortGuide.co.uk, in order to find that perfect Heathrow escort that will blow your mind in bed.</p>
                <p>Check out their photos and list of services and book a date. You can opt for an outcall or an incall, whatever is best for you. The ladies are all professionals, trained in the art of love making, so don’t be afraid to open up to them regarding your wildest fantasies and desires. Complete discretion is guaranteed.</p>
                <p>What are you fantasizing about? You dream of the real girlfriend experience, or you want it rough? You want a busty redhead or a curvaceous blonde? Either way, the Heathrow escorts will raise to the occasion and make you moan in pleasure.</p>
                ',
            'leeds' => '
                <p>When in Leeds, join the escort fun!</p>
                <p>Located in the heart of UK, with a rich industrial past, Leeds has continued to be an important hub where businessmen come to socialize, participate at conferences and network in a vibrant, up and coming city. In this business climate, the Leeds escorts rise to the occasion, being prepared to meet all your desires and fantasies.</p>
                <p>The EscortGuide.co.uk escorts from Leeds are experienced ladies, able to sustain a witty conversation while blowing your mind in bed. So, when booking a date with a Leeds escort you know for sure you will have it all, the fun, smart and elegant dinner companion and the wild beast in the sack. </p>
                <p>The ladies are versatile and ready to have some fun, so stay open minded and get ready to turn into reality all those naughty dreams of yours. Leeds companions are open to both outcalls and incalls, just make up your mind, make the call and set up the details of your encounter.</p>
                <p>From the ultimate girlfriend experience, to the deepest BDSM fantasy turned into reality, the EscortGuide escorts are ready for everything, just to make your stay in Leeds as memorable as possible. </p>
                ',
            'liverpool' => '
                <p>Explore Liverpool’s escort offer!</p>
                <p>Dating in a big city like Liverpool can be intimidating at times, that’s why using the services of one of the most reliable escort directories out there - EscortGuide.co.uk, can be a pretty smart call. The best of the Liverpool escorts is right here, eager to date you and offer you the genuine escort experience.</p>
                <p>Liverpool, the city that gave the world the Beatles, has a growing tourism industry, and that translates into a vast escort offer, from ladies that are willing to offer you that steamy one night stand that you’ve been dreaming of, to great companionship, if you are just looking for a beautiful, intelligent woman to have a lovely dinner date with.</p>
                <p>Let your imagination wonder and trust the Liverpool escorts to offer you exactly what you need. We have it all – sexy blondes, mysterious brunettes, hot redheads, ladies that are into BDSM and companions that have turned the girlfriend experience into an art. </p>
                <p>All you have to do is be bold enough to try something new and exciting.  Book a date now and enjoy the adventure of your life. The ladies are available for both outcalls and incalls.</p>
                ',
            'luton' => '
                <p>Go international with a Luton VIP escort!</p>
                <p>Known as the perfect place to live for commuters, because of its proximity to London and to one of the biggest airports in UK, Luton is a place of diversity and multiculturalism which also translates into a vast and varied escort offer.</p>
                <p>This busy city can be overwhelming for the regular traveler, especially when it comes to meeting and dating young ladies. That’s why is always a good idea to book the services of a professional EscortGuide.co.uk companion. </p>
                <p>Luton escorts are the perfect illustration of the city they live in – eclectic, open minded, fun, full of life and ready for anything. Just take your time to browse through the vast escort offer on EscortGuide.co.uk and pick the lady of your desires.</p>
                <p>This is your chance to get our of your comfort zone and try something new and exciting, like a role play date with a foxy redhead, or try the perfect girlfriend experience, or maybe get into some BDSM games with a naughty dominatrix, your imagination is the limit.</p>
                <p>The ladies are well-educated, fun and easy to be around with. So, book a Luton escort for an outcall or incall and have the time of your life. </p>
                ',
            'nottingham' => '
                <p>Enjoy the Nottingham VIP escort offer!</p>
                <p>While in Nottingham, go rogue and enjoy the best that one of UK’s top destinations has to offer. Here is where Escortguide.co.uk steps in, tempting you with the finest selection of VIP Nottingham escorts. Browse through our vast elite escorts and decide which lady will be joining you in a real adventure of the senses. The escort offer is diverse, from beautiful blondes to hot brunettes, from busty ladies to athletic girls, perfectly fit to offer you the best ride of your life.</p>
                <p>Nottingham escorts are some of the best in UK, accustomed to offer a large array of services and to provide you with the genuine girlfriend experience. So, book a date with one of our EscortGuide companions and let her show you all the perks of a great city like Nottingham – fine dining at Michelin star restaurants, excellent nightlife, great art and high fashion.</p>
                <p>Keep in mind that the rebellious nature of the Nottingham people runs through the blood of our top escorts, so prepare yourself for an intense adventure. The ladies are open minded and ready to fulfill some of your deepest desires, you just have to open up to them and express your fantasies.</p>
               '
        ];

        return (!empty($city) && isset($texts[strtolower($city)])) ? $texts[strtolower($city)] : null;
    }

	/**
	 * Chat Online Escorts Action
	 */
	public function chatOnlineAction()
	{
		$action = 'index';
		if ( ! is_null($this->_getParam('ajax')) ) {
			$action = 'list';
		}
		$this->_helper->viewRenderer->setScriptAction($action);

		$req = $this->_getParam('req');
		$param = explode('/', $req);
		$page = explode("_", ( isset($param[2]) ) ? $param[2] : '' );

        $m_e = new Model_Escorts();
        $res = $m_e->getChatNodeOnlineEscorts(0, 1000, false, true);
        $ids = $res['ids'];


		if (strlen($ids) > 0)
		{
			$filter = array(
				'e.id IN (' . $ids . ')'
			);

			$page_num = 1;
			if ( isset($page[1]) && $page[1] ) {
				$page_num = $page[1];
			}

			$count = 0;
			$escorts = Model_Escort_List::getFiltered($filter, 'random', $page_num, 50, $count);
		}
		else
		{
			$count = 0;
			$escorts = array();
		}

		$this->view->count = $count;
		$this->view->escorts = $escorts;
        $this->view->view_type = 'grid';
		list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls();

		$this->view->chat_online = true;
	}

	/**
	 * Happy Hours action
	 */
	/*public function happyHoursAction()
	{
		$action = 'index';
		if ( ! is_null($this->_getParam('ajax')) ) {
			$action = 'list';
		}
		$this->_helper->viewRenderer->setScriptAction($action);

		$req = $this->_getParam('req');
		$param = explode('/', $req);
		$page = explode("_", ( isset($param[2]) ) ? $param[2] : '' );

		$filter = array('e.gender = ?' => 1, 'e.hh_is_active = ?' => 1);

		$page_num = 1;
		if ( isset($page[1]) && $page[1] ) {
			$page_num = $page[1];
		}

		$count = 0;
		$escorts = Model_Escort_List::getFiltered($filter, 'random', $page_num, 10, $count);

		$this->view->count = $count;
		$this->view->escorts = $escorts;

		list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls();

		$this->view->hh_page = true;
	}*/

	private function _watchedEscorts($escorts)
	{
		$ids = array();
		$watched_escorts = array();

		foreach ($escorts as $e)
		{
			$ids[] = $e->id;
		}

		if ($ids)
		{
			$ids_str = trim(implode(',', $ids), ',');
			$user = Model_Users::getCurrent();
			$modelM = new Model_Members();

			if ($user)
			{
				$escorts_watched_type = $user->escorts_watched_type;

				if (!$escorts_watched_type) $escorts_watched_type = 1;

				$res = $modelM->getFromWatchedEscortsForListing($ids_str, $escorts_watched_type, $user->id, session_id());
			}
			else
			{
				$res = $modelM->getFromWatchedEscortsForListing($ids_str, WATCH_TYPE_PER_SESSION, null, session_id());
			}

			if ($res)
			{
				foreach ($res as $r)
				{
					$watched_escorts[] = $r->escort_id;
				}
			}
		}

		return $watched_escorts;
	}

	public function getFilterAction($existing_filters = array())
	{
        $cache = Zend_Registry::get('cache');
        $this->view->layout()->disableLayout();
        $defines = Zend_Registry::get('defines');
        $this->view->genders = $defines['gender_options'];

		if ( ! is_null($this->_getParam('ajax')) && ! is_null($this->_getParam('filter_params')) ) {
		    $cacheKey = $this->_getParam('filter_params');

			$filter_params = (array) json_decode(base64_decode($this->_request->filter_params));
			$is_upcomingtour = $this->_request->is_upcomingtour;
			$is_tour = $this->_request->is_tour;

            $existing_filters = $cache->load($cacheKey);
            if (empty($existing_filters)) {
                $existing_filters = Model_Escort_List::getActiveFilter($filter_params, $is_upcomingtour, $is_tour);
                $cache->save($existing_filters, $cacheKey, array());
            }
		}



		$mapper = new Cubix_Filter_Mapper();

		//$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'n', 'filter' => 'Nationality', 'field' => 'e.nationality_id', 'filterField' => 'fd.nationality', 'queryJoiner' => 'OR', 'weight' => 10)) );
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array(
			'name' => 's', 'filter' => 'Service',
			'field' => 'es.service_id', 'filterField' => 'fd.services',
			'queryJoiner' => 'OR', 'weight' => 10)
		));
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array(
			'name' => 'k', 'filter' => 'Keyword',
			'field' => 'ek.keyword_id', 'filterField' => 'fd.keywords',
			'queryJoiner' => 'OR', 'weight' => 11)
		));
		/*$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array(
			'name' => 'af', 'filter' => 'AvailableFor',
			'field' => 'e.available_for_type', 'queryJoiner' => 'OR', 'weight' => 20)
		));*/
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array(
			'name' => 'ai', 'filter' => 'Incall', 'group' => 'availability', 'groupWeight' => 8,
			'field' => 'e.available_for_type', 'queryJoiner' => 'OR', 'weight' => 21)
		));
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array(
			'name' => 'ao', 'filter' => 'Outcall', 'group' => 'availability', 'groupWeight' => 8,
			'field' => 'e.available_for_type', 'queryJoiner' => 'OR', 'weight' => 20)
		));
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array(
			'name' => 'l', 'filter' => 'Language', 'group' => 'Others', 'groupWeight' => 20,
			'field' => 'e.languages', 'filterField' => 'fd.language',
			'queryJoiner' => 'AND', 'weight' => 30)
		));
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array(
			'name' => 'sf', 'filter' => 'ServiceFor', 'group' => 'Others', 'groupWeight' => 20,
			'field' => 'e.sex_availability', 'filterField' => 'fd.service_for',
			'queryJoiner' => 'OR', 'weight' => 31)
		));
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array(
			'name' => 'hc', 'filter' => 'HairColor', 'group' => 'Physical Appearance', 'groupWeight' => 10,
			'field' => 'e.hair_color', 'filterField' => 'fd.hair_color',
			'queryJoiner' => 'OR', 'weight' => 130)
		));
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array(
			'name' => 'hl', 'filter' => 'HairLength', 'group' => 'Physical Appearance', 'groupWeight' => 10,
			'field' => 'e.hair_length', 'filterField' => 'fd.hair_length',
			'queryJoiner' => 'OR', 'weight' => 131)
		));
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array(
			'name' => 'cs', 'filter' => 'CupSize', 'group' => 'Physical Appearance', 'groupWeight' => 10,
			'field' => 'e.cup_size', 'filterField' => 'fd.cup_size',
			'queryJoiner' => 'OR', 'weight' => 40)
		));
		/*$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array(
			'name' => 'a', 'filter' => 'Age', 'field' => 'e.age', 'filterField' => 'fd.age', 'weight' => 50)
		));*/
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array(
			'name' => 'e', 'filter' => 'Ethnicity', 'group' => 'Physical Appearance', 'groupWeight' => 10,
			'field' => 'e.ethnicity', 'filterField' => 'fd.ethnicity',
			'queryJoiner' => 'OR', 'weight' => 70)
		));

		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array(
			'name' => 'so', 'filter' => 'Orientation', 'group' => 'Others', 'groupWeight' => 20,
			'field' => 'e.sex_orientation', 'filterField' => 'fd.orientation',
			'queryJoiner' => 'OR', 'weight' => 32)
		));


		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array(
			'name' => 'h', 'filter' => 'Height', 'group' => 'Physical Appearance', 'groupWeight' => 10,
			'field' => 'e.height', 'filterField' => 'fd.height', 'weight' => 90)
		));
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array(
			'name' => 'w', 'filter' => 'Weight', 'group' => 'Physical Appearance', 'groupWeight' => 10,
			'field' => 'e.weight', 'filterField' => 'fd.weight', 'weight' => 100)
		));
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array(
			'name' => 'ec', 'filter' => 'EyeColor', 'group' => 'Physical Appearance', 'groupWeight' => 10,
			'field' => 'e.eye_color', 'filterField' => 'fd.eye_color', 'queryJoiner' => 'OR', 'weight' => 110)
		));

		$filter = new Cubix_FilterV2( array('mapper' => $mapper->getAll(), 'request' => $this->_request, 'existingFilters' => $existing_filters) );

		$this->view->filter = $filter->renderEG();

		return $filter->makeQuery();
	}



    /*public function ajaxBubbleAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$page = intval($this->_getParam('page', 1));
		if ( $page < 1 ) $page = 1;
        $per_page = 7;

		echo $this->view->bubbleTextsWidget($page, $per_page);
    }

    public function ajaxOnlineAction()
	{
        $page = intval($this->_getParam('page', 1));
		if ( $page < 1 ) $page = 1;
        $per_page = 5;
		$this->view->layout()->disableLayout();

		if ($this->view->allowChat2())
		{
			$model = new Model_Escorts();
			$results = $model->getChatNodeOnlineEscorts($page, $per_page);

			$this->view->escorts = $results['escorts'];
			$this->view->per_page = $per_page;
			$this->view->page = $page;
			$this->view->count = $results['count'];
		}
    }*/

	public function profileAction()
	{
		//////   redirect not active escorts to home page   ///////
		$m = new Model_Escorts();
		if ($this->_getParam('f') != 'b' && !$m->getShownameById($this->_getParam('escort_id')))
		{
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			die;
		}
		//////////////////////////////////////////////////////////
		//</editor-fold>
		//$route_name = Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName();

		$this->_helper->viewRenderer->setScriptAction("profile/profile");

		$this->view->is_profile = true;
		$this->view->is_preview = $is_preview = $this->_getParam('prev', false);
		$this->view->show_advertise_block = $show_advertise_block = $this->_getParam('show_advertise_block', false);

		$showname = $this->_getParam('showname');
		$escort_id = $this->_getParam('escort_id');

		$model = new Model_Escorts();

		$cache_key = 'escort_profile_' . $showname . '_' . $escort_id .  '_' . Cubix_I18n::getLang();
		$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

		$this->view->profile_cache_key = 'html_profile_' . $cache_key;

		$escort = $model->get($escort_id, $cache_key, $is_preview);
        $escort->slogan = $model->getSloganById($escort_id);

		$escort->total_views = $this->sedcardTotalViews($escort_id);
		$this->view->HandVerificationDate = $escort->getHandVerificationDate();

		if ( ! $escort || isset($escort['error']) )
		{
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			die;
			return;
		}

		if ($escort->showname != $showname)
		{
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			die;
		}
		
		$city = new Cubix_Geography_Cities();
		$city = $city->get($escort->base_city_id);
		$this->_request->setParam('region', $city->region_slug);
		$this->view->base_city = $city;

		if ($escort->gender == GENDER_MALE){
			$this->_request->setParam('req', "boys");
		}
		elseif ($escort->gender == GENDER_TRANS){
			$this->_request->setParam('req', "trans");
		}

		$add_esc_data = $model->getRevComById($escort->id);
		$bl = $model->getBlockWebsite($escort->id);

		$escort->block_website = $bl->block_website;
		$escort->check_website = $bl->check_website;
		$escort->bad_photo_history = $bl->bad_photo_history;
		$escort->disabled_reviews = $add_esc_data->disabled_reviews;
		$escort->disabled_comments = $add_esc_data->disabled_comments;
        $escort->hand_verification = $escort->getHandVerificationDate();
		$this->view->escort_id = $escort->id;
		$this->view->escort = $escort;

		$this->view->filtered_phone =  (isset($escort->phone_country_id) && !$escort->disable_phone_prefix) ? '+'. Model_Countries::getPhonePrefixById($escort->phone_country_id) . ltrim($escort->phone_number, '0') : $escort->phone_number;
		$this->view->add_phones = $model->getAdditionalPhones($escort->id);
		$this->view->apps = $model->getApps($escort->id);

		$this->view->videoConfig = Zend_Registry::get('videos_config');
		$host =  Cubix_Application::getById(Cubix_Application::getId())->host;
		$video = new Model_Video();
		$escort_video = $video->GetEscortVideo($escort->id, 1, ($escort->agency_id) ? $escort->agency_id : null, FALSE);

		if(!empty($escort_video[0]))
		{
			$photo = new Cubix_ImagesCommonItem($escort_video[0][1]);
			$this->view->photo_video = $photo->getUrl('orig');
            $this->view->video = $escort_video[1][0];
            $this->view->host = Cubix_Application::getById(Cubix_Application::getId())->host;
		}

		if ( $escort->home_city_id ) {
			$m_city = new Cubix_Geography_Cities();
			$home_city = $m_city->get($escort->home_city_id);

			$m_country = new Cubix_Geography_Countries();
			$home_country = $m_country->get($home_city->country_id);

			$this->view->home_city_country = $home_city->{'title_' . $this->_request->lang_id} . ' (' . $home_country->{'title_' . $this->_request->lang_id} . ')';
		}

		$bt = $model->getBubbleText($escort->id);
		$this->view->bubble_text = $bt ? $bt->bubble_text : null;

		/* Reviews */
		$cache = Zend_Registry::get('cache');
		$cache_key = 'reviews_' . $escort->id;
		if ( ! $res = $cache->load($cache_key) ) {
			try {
				$res = $model->getEscortLastReviews($escort->id);

				//$res = Cubix_Api::getInstance()->call('getEscortLastReview', array($escort->id));
			}
			catch ( Exception $e ) {
				$res = array(false, false);
			}

			$cache->save($res, $cache_key, array());
		}
        $imported_res = $model->getEscortImportedReviews($escort->id);
		list($reviews, $rev_count) = $res;
		list($imported_reviews, $imported_rev_count) = $imported_res;
		$this->view->reviews = Model_Reviews::mergeReviews($reviews, $imported_reviews);
		$this->view->rev_count = $rev_count;
        $this->view->imported_reviews = $imported_reviews;
        $this->view->imported_rev_count = $imported_rev_count;

		/* Comments */
		$model_comments = new Model_Comments();
		$com_count = 0;
		$comments = $model_comments->getEscortComments(1, 3, $com_count, $escort_id);

		$this->view->comments = $comments;
		$this->view->com_count = $com_count;

		$this->photosV2Action($escort);

		if ( $escort->agency_id )
			$this->agencyEscortsAction($escort->agency_id, $escort->id,6);

        $client = new Cubix_Api_XmlRpc_Client();
        $user_id = $model->getUserId($escort->id);
		if ($escort->agency_id)
        {
            $has_active_package = $client->call('Escorts.hasPaidActivePackageForAgency', array($user_id));
        }else{
            $has_active_package = $client->call('Escorts.hasPaidActivePackageForEscort', array($user_id));
        }

        $this->view->escort->locations = $client->call('Escorts.getLngLat', array($escort->id));

		$this->view->has_active_package = $has_active_package;
		if (!$has_active_package)
        {
            $this->view->nearByEscorts = Model_Escorts::getNearByEscorts($escort);
            $this->view->escortCity = $escort->city_slug;
        }

		$user = Model_Users::getCurrent();
		$this->view->user = $user;

		//<editor-fold defaultstate="collapsed" desc="Back Next buttons logic">
		//Modified by Vahag

		$paging_key = $this->_getParam('from');

		if(is_null($paging_key)){
            $paging_key = 'regular_list';
        }

		$paging_keys_map = array(
			'last_viewed_escorts',
			'search_list',
			'regular_list',
			'new_list',
			'profile_disabled_list',
			'premiums',
			'main_premium_spot',
			'tours_list',
			'tour_main_premium_spot',
			'up_tours_list',
			'up_tour_main_premium_spot'
		);

		if ( in_array($paging_key, $paging_keys_map) ) {
			$sid = 'sedcard_paging_' . Cubix_Application::getId();
			$ses = new Zend_Session_Namespace($sid);

			$ses_pages = $ses->{$paging_key};
			$criterias = $ses->{$paging_key . '_criterias'};

			if ( ! isset($criterias['first_page']) ) {
				$criterias['first_page'] = $criterias['page'];
			}
			if ( ! isset($criterias['last_page']) ) {
				$criterias['last_page'] = $criterias['page'];
			}

			$ses_index = 0;
			if ( is_array($ses_pages) ) {
				$ses_index = array_search($escort_id, $ses_pages);
			}


			if ( isset($ses_pages[$ses_index - 1]) ) {
				$this->view->back_showname = $model->getShownameById($ses_pages[$ses_index - 1]);
				$this->view->back_id = $back_id = $ses_pages[$ses_index - 1];
			} elseif ( $criterias['first_page'] > 1 ) { //Loading previous page escorts
				switch( $paging_key ) {
					case 'regular_list' :
						$prev_page_escorts = Model_Escort_List::getFiltered($criterias['filter'], $criterias['sort'], $criterias['first_page'] - 1);
						break;
					case 'tours_list':
						$prev_page_escorts = Model_Escort_List::getTours($criterias['is_upcoming'], $criterias['filter'], $criterias['sort'], $criterias['first_page'] - 1);
						break;
				}

				$p_escorts_count = count($prev_page_escorts);
				if ( count($p_escorts_count) ) {
					$this->view->back_showname = $prev_page_escorts[$p_escorts_count - 1]->showname;
					$this->view->back_id = $prev_page_escorts[$p_escorts_count - 1]->id;
					//Adding previous page escorts to $ses_pages and set in session
					$ids = array();
					foreach($prev_page_escorts as $p_esc) {
						$ids[] = $p_esc->id;
					}

					$criterias['first_page'] -= 1;
					$ses->{$paging_key} = array_merge($ids, $ses_pages);
					$ses->{$paging_key . '_criterias'} = $criterias;
				}
			}

			if ( isset($ses_pages[$ses_index + 1]) ) {
				$this->view->next_showname = $model->getShownameById($ses_pages[$ses_index + 1]);
				$this->view->next_id = $next_id = $ses_pages[$ses_index + 1];
			} else { //Loading next page escorts
				switch( $paging_key ) {
					case 'regular_list' :
						$next_page_escorts = Model_Escort_List::getFiltered($criterias['filter'], $criterias['sort'], $criterias['last_page'] + 1);
						break;
					case 'main_premium_spot':
						$next_page_escorts = array();
						break;
					case 'tours_list':
						$next_page_escorts = Model_Escort_List::getTours($criterias['is_upcoming'], $criterias['filter'], $criterias['sort'], $criterias['last_page'] + 1);
						break;
				}

				if ( count($next_page_escorts) ) {
					$this->view->next_showname = $next_page_escorts[0]->showname;
					$this->view->next_id = $next_page_escorts[0]->id;
					//Adding next page escorts to $ses_pages and set in session
					foreach($next_page_escorts as $p_esc) {
						$ses_pages[] = $p_esc->id;
					}

					$criterias['last_page'] += 1;
					$ses->{$paging_key} = $ses_pages;
					$ses->{$paging_key . '_criterias'} = $criterias;
				}
			}
			$this->view->paging_key = $paging_key;
		}
		//</editor-fold>

		/*list($prev, $next) = Model_Escort_List::getPrevNext($escort->showname);

		if ( ! is_null($prev) ) {
			$this->view->back_showname = $prev;
		}
		if ( ! is_null($next) ) {
			$this->view->next_showname = $next;
		}*/

		//list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls();

		/* Cities list */
		$app = Cubix_Application::getById();

		if ($app->country_id)
		{
			$modelCities = new Model_Cities();

			$cities = $modelCities->getByCountry($app->country_id);
		}
		else
		{
			$modelCountries = new Model_Countries();
			$modelCities = new Model_Cities();

			$countries = $modelCountries->getCountries();
			$this->view->countries_list = $countries;

			$c = array();

			foreach ($countries as $country)
				$c[] = $country->id;

			$cities = $modelCities->getByCountries($c);
		}

		$this->view->cities_list = $cities;
		/* End Cities list */

		$this->view->escort_user_id = $model->getUserId($escort->id);

		/* watched */
		$model_m = new Model_Members();
		$sess_id = session_id();
		$user_id = null;
		$cur_user = Model_Users::getCurrent();

		if ($cur_user)
		{
			$user_id = $cur_user->id;

			$has = $model_m->getFromWatchedEscortsForUser($sess_id, $escort_id, $user_id, date('Y-m-d'));

			if ($has)
			{
				$model_m->updateToWatchedEscortsForUser($sess_id, $escort_id, $user_id);
			}
			else
			{
				$model_m->addToWatchedEscorts($sess_id, $escort_id, $user_id);
			}
		}
		else
		{
			$has = $model_m->getFromWatchedEscorts($sess_id, $escort_id);

			if ($has)
			{
				$model_m->updateToWatchedEscorts($sess_id, $escort_id);
			}
			else
			{
				$model_m->addToWatchedEscorts($sess_id, $escort_id);
			}
		}

		$except_arr[] = $escort->id;
		$agency_girls_ids = array();
		if($this->view->a_escorts){
			foreach($this->view->a_escorts as $agency_escort) {
				$agency_girls_ids[] = $agency_escort->id;
			}
		}
		$except_arr = array_merge($agency_girls_ids, $except_arr);
		if(is_null($escort->gender))$escort->gender = 1;

		if($cur_user){
			$watched_escorts = $model_m->getWatchedEscortsIDSForUser($sess_id, $user_id);
			foreach ($watched_escorts as $watched_escort) {
				$except_arr[] = $watched_escort->escort_id;
			}
		}

		$this->view->recommended = $m->recommendedEscorts( $escort->gender, $except_arr , $escort->base_city_id,false,12);
	}

	public function photosV2Action($escort = null)
	{
		$config = Zend_Registry::get('escorts_config');

		$page = $this->_getParam('photo_page');

		$escort_id = $this->_getParam('escort_id');
		$mode = $this->_getParam('mode');

		if( $mode == 'ajax' ) {
			$this->view->layout()->disableLayout();
			$model = new Model_Escorts();
			$escort = $model->get($escort_id);
			$this->view->escort_id = $escort_id;
		}

		if ( ! $page )
			$page = 1;

		$count = 0;
		if ( $this->view->is_preview || $this->_getParam('is_preview') ) {
			$photos = $escort->getPhotosApi($page, $count, true, true, true);
		}
		else {
			$count = 100;
			$photos = $escort->getPhotos($page, $count, true, false, null, null, false, true, true);

			$user = Model_Users::getCurrent();
			if($user->user_type == 'member'){
				$photosPrivate = $escort->getPhotos($page, $count, true, true, null, null, false, true, true);
				$photos = array_merge($photos,$photosPrivate);
			}
		}
		$this->view->photos = $photos;
		$this->view->photos_count = $count;
		$this->view->photos_page = $page;

		$this->view->photos_perPage = $config['photos']['perPage'];
	}

	public function agencyEscortsAction($agency_id = null, $escort_id = null,$limit = false)
	{
		$config = Zend_Registry::get('escorts_config');

		if ( ! $agency_id )
		{
			$this->view->layout()->disableLayout();
			$agency_id = $this->view->agency_id = $this->_getParam('agency_id');
		}
		else
			$this->view->agency_id = $agency_id;

		if( ! $escort_id )
			$escort_id = $this->view->escort_id = $this->_getParam('escort_id');

		$page = intval($this->_getParam('agency_page'));
		if ( $page < 1 )
			$page = 1;

		$params = array('e.agency_id = ?' => $agency_id);
		//EGUK EGUK-492
//		$params = array('e.agency_id = ?' => $agency_id,'e.id <> ?' => $escort_id);

		$escorts_count = 0;
		$escorts = Model_Escort_List::getFiltered($params, 'random', $page, ($limit) ? $limit : $config['widgetPerPageV2'], $escorts_count);

		if (count($escorts) ) {
			$this->view->a_escorts = $escorts;
			$this->view->a_escorts_count = $escorts_count;

			$this->view->agencies_page = $page;
			$this->view->widgetPerPage = $config['widgetPerPageV2'];

			$m_esc = new Model_Escorts();
			$agency = $m_esc->getAgency($escort_id);

			$this->view->agency = new Model_AgencyItem($agency);
		}
	}

	public function viewedEscortsAction()
	{
		$this->view->layout()->disableLayout();
		$escort_id = intval($this->_getParam('escort_id'));

		if ( $escort_id )
		{
			self::_addToLatestViewedList($escort_id);
			$viewedEscorts = self::_getLatestViewedList(10, $escort_id);
		}
		else
			$viewedEscorts = self::_getLatestViewedList(10);

		if ( ! is_array($viewedEscorts) || count($viewedEscorts) == 0 )
			return false;



		$ve = array();

		$ids = array();

		foreach ( $viewedEscorts as $e ) {
			$ids[] = $e['id'];
		}

		$filter = array('e.id IN (' . implode(', ', $ids) . ')' => array());
		$order = 'FIELD(eic.escort_id, ' . implode(', ', $ids) . ')';

		$count = 0;
		$escorts = Model_Escort_List::getFiltered($filter, $order, 1, 10, $count);

		$this->view->escorts = $escorts;
	}

	public function lateNightGirlsAction()
	{
		$this->view->layout()->disableLayout();

		$page = intval($this->_request->l_n_g_page);

		if ($page < 1) $page = 1;

		list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls($page);

		$this->view->l_n_g_page = $page;
	}

	private function _getLateNightGirls($page = 1)
	{
		return Model_Escorts::getLateNightGirls($page);
	}

    public function ajaxReportProblemAction()
    {
        $this->view->layout()->disableLayout();
        $url = $_SERVER['HTTP_REFERER'];
        if($this->_request->isPost()){
            // Fetch administrative emails from config
            $config = Zend_Registry::get('feedback_config');
            $support_email = $config['emails']['support'];
            $email_tpl = 'report_problem';
            // Get data from request
            $data = new Cubix_Form_Data($this->getRequest());

            $fields = array(
                'escort_id' => 'int',
                'name' => 'notags|special',
                'email' => '',
                'message' => 'notags|special',

            );
            $data->setFields($fields);
            $data = $data->getData();

            $validator = new Cubix_Validator();

            if ( ! strlen($data['name']) ) {
                $validator->setError('name', 'Your Name is required');
            }

            if ( ! strlen($data['email']) ) {
                $validator->setError('email', 'Your Email is required');
            }
            elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
                $validator->setError('email', 'Wrong email format');
            }
            if ( ! strlen($data['message']) ) {
                $validator->setError('message','Please type the report you want to send');
            }

            $result = $validator->getStatus();

            if ( $validator->isValid() ) {

                // Set the template parameters and send it to support
                $tpl_data = array(
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'message' => $data['message'],
                    'application_id' => Cubix_Application::getId(),
                    'url' => $url
                );
                $data['application_id'] = Cubix_Application::getId();

                try{
                    Cubix_Email::sendTemplate($email_tpl, $support_email, $tpl_data, null);

                    $user_id = null;
                    $user = Model_Users::getCurrent();

                    if ($user && $user->user_type == 'member')
                        $user_id = $user->id;

                    $client = new Cubix_Api_XmlRpc_Client();
                    $client->call('Users.addProblemReport', array($data['name'], $data['email'], null, $data['message'], $data['escort_id'], $user_id));
                }catch ( Exception $e ) {
                    die($e->getMessage());
                }
                die(json_encode($result));
            }
            else {
                die(json_encode($result));

            }

        }
    }

    public function ajaxSuspPhotoAction()
    {
        $this->view->layout()->disableLayout();

        if ($this->_request->isPost())
        {
            // Get data from request
            $data = new Cubix_Form_Data($this->getRequest());

            $fields = array(
                'escort_id' => 'int',
                'link_1' => '',
                'link_2' => '',
                'link_3' => '',
                'comment' => 'notags|special'
            );

            $data->setFields($fields);
            $data = $data->getData();

            $validator = new Cubix_Validator();

            if (!strlen($data['link_1']))
            {
                $validator->setError('link_1', $this->view->t('photo_link_required'));
            }
            elseif (!$validator->isValidURL($data['link_1']))
            {
                $validator->setError('link_1', $this->view->t('photo_link_not_valid'));
            }

            if (strlen($data['link_2']) && !$validator->isValidURL($data['link_2']))
            {
                $validator->setError('link_2', $this->view->t('photo_link_not_valid'));
            }

            if (strlen($data['link_3']) && !$validator->isValidURL($data['link_3']))
            {
                $validator->setError('link_3', $this->view->t('photo_link_not_valid'));
            }

            if (! strlen($data['comment']))
            {
                $validator->setError('comment', $this->view->t('comment_required'));
            }

            $result = $validator->getStatus();

            if ($validator->isValid())
            {
                $data['application_id'] = Cubix_Application::getId();
                $data['status'] = SUSPICIOUS_PHOTOS_REQUEST_PENDING;

                $user = Model_Users::getCurrent();
                $user_id = null;

                if ($user)
                    $user_id = $user->id;

                $data['user_id'] = $user_id;

                $client = new Cubix_Api_XmlRpc_Client();
                $client->call('Users.addSuspPhoto', array($data));
//                die(json_encode())
            }

            die(json_encode($result));
        }
    }

	/*private function _addToLatestViewedList($escort_id)
	{
		$count = 11; // actually we need 10
		$escort = array('id' => $escort_id);
		$sid = 'last_viewed_escorts_' . Cubix_Application::getId();

		$ses = new Zend_Session_Namespace($sid);

		// Checking if session exists
		// creating if not exists
		if ( false == (isset($ses->escorts) && is_array($ses->escorts)) )
		{
			$ses->escorts = array();
		}
		else
		{
			// Checking if escort exists in stack
			// removing escort from stack if exists
			foreach ($ses->escorts as $key => $item)
			{
				if ( $escort['id'] == $item['id'] )
				{
					array_splice($ses->escorts, $key, 1);
				}
			}
		}

		// Pushing escort to the end of the stack
		array_unshift($ses->escorts, $escort);

		// Checking if
		if ( count($ses->escorts) > $count )
		{
			array_pop($ses->escorts);
		}
		//var_dump($ses->escorts);
	}*/

	// Getting latest viewed escorts list (stack)
	// as Array of Assoc
	/*public function _getLatestViewedList($max, $exclude = NULL)
	{
		$sid = 'last_viewed_escorts_' . Cubix_Application::getId();

		$ses = new Zend_Session_Namespace($sid);

		if ( ! is_array($ses->escorts) )
			return array();

		if ( $exclude )
		{
			$arr = $ses->escorts;
			foreach($arr as $key => $item)
			{
				if($exclude == $item['id'])
				{
					array_splice($arr, $key, 1);
				}
			}
			$ret = array_slice($arr, 0, $max);

			return $arr;
		}

		$ret = array_slice($ses->escorts, 0, $max);

		return $ret;
	}*/

	/*public function gotmAction()
	{
		$this->view->layout()->setLayout('main-simple');
		$show_history = (bool) $this->_getParam('history');

		$page = intval($this->_getParam('page', 1));
		if ( $page < 1 ) $page = 1;
		$this->view->per_page = $per_page = 40;
		$this->view->page = $page;

		$result = Model_Escorts::getMonthGirls($page, $per_page, $show_history);

		$this->view->escorts = $escorts = $result['escorts'];
		$this->view->count = $result['count'];

		if ( ! $show_history ) {
			$this->view->gotm = Model_Escorts::getCurrentGOTM();
		}
		else {
			$this->_helper->viewRenderer->setScriptAction('gotm-history');
		}
	}*/

	/*public function gotmCurrentAction()
	{
		$this->view->layout()->disableLayout();

		try {
			$gotm = Model_Escorts::getCurrentGOTM();

			if ($gotm)
			{
				$escort = new Model_EscortsItem();
				$escort->setId($gotm->id);
				$gotm_photos = $escort->getPhotos();
			}
		}
		catch ( Exception $e ) { $gotm = false; }

		$this->view->gotm = $gotm;
		$this->view->gotm_photos = $gotm_photos;
	}*/

	/**
	 * Voting Widget Action
	 */
	public function voteAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$response = array('status' => null, 'desc' => '', 'html' => '');

		$showname = $this->_getParam('showname');
		$escort_id = $this->_getParam('escort_id');
		$model = new Model_Escorts();
		//$escort = $model->get($showname);
		$escort = $model->get($escort_id);

		$user = Model_Users::getCurrent();

		if ( ! $user ) {
			$response['status'] = 'error';
			$response['desc'] = 'Please sign in as member.';
		}
		elseif ( 'member' != $user->user_type ) {
			$response['status'] = 'dinaid';
			$response['desc'] = 'Only members are allowed to vote';
		}
		elseif ( ! $escort ) {
			$response['status'] = 'error';
			$response['desc'] = 'Invalid escort showname';
		}
		else {
			try {
				$result = $escort->vote($user->id);

				if ( $result !== true ) {
					$response['status'] = 'voted';
					$response['desc'] = 'You have already voted for this escort.';
				}
				else {
					$response['status'] = 'success';
					$response['desc'] =  'Your vote has been successfully placed';

					$response['html'] = $this->view->votingWidget($escort, true);
				}
			}
			catch ( Exception $e ) {
				var_dump($e);die;
				$response['status'] = 'error';
				$response['desc'] = 'unexpected error';
			}
		}

		echo json_encode($response);
	}

	public function getAdvancedFilterAction(){
	    $this->view->layout()->disableLayout();
        $filter = new Cubix_FilterV2( array('mapper' => $mapper->getAll(), 'request' => $this->_request, 'existingFilters' => '') );
        $this->view->filter = $filter->renderEG();
        $this->_helper->viewRenderer->setScriptAction("get-filter");
    }

    public function sedcardTotalViews($escort_id){

        if (!is_numeric($escort_id)) {
            return 1;
        }

        $client = Cubix_Api_XmlRpc_Client::getInstance();
        $total = $client->call('Escorts.getTotalViews', array($escort_id));


        return (int) ($total) ? number_format($total) : 1;

    }

    public function _hardcode_vips_position(Array $args)
    {
        $city = $args['city'];
        $vips = $args['vips'];
        if (empty($vips)) return;
        $escort_places_arr = Model_VipHardcodes::getAll();
        $count_of_ads_in_this_area = 0;
        $escort_places = [];

        foreach ($escort_places_arr as $key => $place) {

            $city_matches = $place->city_id == $city;

            if ($city && $city_matches) {
                $count_of_ads_in_this_area++;
                $escort_places[$place->escort_id] = $place;
            }
        }
        $ordered_vips = range(0, count($vips) - 1);
        foreach ($vips as $id => $escort) {
            $pp_data = $escort_places[$escort->id];
            if (!is_null($pp_data)) {

                $city_matches = $pp_data->city_id == $city;
                if ($city && $city_matches) {
                    $escort->hardcoded_id = $pp_data->agency_id;
                    $ordered_vips[$pp_data->place] = $escort;
                }
            }
        }
        foreach ($vips as $id => $escort)
            if (!isset($escort_places[$escort->id]))
                foreach ($ordered_vips as $key => $val)
                    if (is_numeric($val)) {
                        $ordered_vips[$key] = $escort;
                        break;
                    }
        for ($i = 0; $i < count($ordered_vips); $i++) {
            for ($j = 0; $j < count($ordered_vips); $j++) {
                if (!is_null($ordered_vips[$i]->hardcoded_id) && $ordered_vips[$i]->hardcoded_id == $ordered_vips[$j]->hardcoded_id && (rand(0, 10) / 10) < 0.5) {
                    $temp = $ordered_vips[$i];
                    $ordered_vips[$i] = $ordered_vips[$j];
                    $ordered_vips[$j] = $temp;
                }
            }
        }
        $ordered_vips = array_filter($ordered_vips, function ($x) {
            return !is_numeric($x);
        });

        return $ordered_vips;
    }

    public function filtersLoadAction()
    {
        $this->view->layout()->disableLayout();
        $is_upcoming = $this->_request->is_upcoming;
        $is_tour = $this->_request->is_tour;

        $external_filters = array();

        $filter_params = array(
            'order' => 'e.date_registered DESC',
            'limit' => array('page' => 1),
            'filter' => array()
        );

        $current_filter = $this->getFilterAction();
        $existing_filters = Model_Escort_List::getActiveFilter($filter_params['filter'], $current_filter, $is_upcoming, $is_tour);

        $where_query = $this->getFilterAction($existing_filters);

        $have_filter = false;
        if ( count($where_query) ) {
            foreach ( $where_query as $q ) {
                if ( strlen($q['query']) ) {
                    $have_filter = true;
                    break;
                }
            }
        }

        $this->view->have_filter = $have_filter;

        $filter_body = $this->view->render('escorts/get-filter.phtml');

        die(json_encode(array('filter_body' => $filter_body)));
    }

    public function gotdClickTrackerAction()
    {
        if ($this->_request->isPost())
        {
            $request = $this->_request;

           if (!empty($request->escortId)) {

               $clickedEscortId = $request->escortId;
               $clickedUserIpAddress = $_SERVER['REMOTE_ADDR'];
               $clickedUserId = 0;
               $user = Model_Users::getCurrent();

               if (!is_null($user)) {
                    $clickedUserId = $user->id;
               }

               $model_gotdClickTracker = new Model_GotdClickTracker();
               $model_gotdClickTracker->insertNewClickEvent($clickedUserIpAddress, $clickedEscortId, $clickedUserId);
           }
        }
    }

    public function tagTrackerAction()
    {
        if ($this->_request->isPost())
        {
            $modelCities = new Model_Cities();

            $tag_id = $this->_request->tagId;
            $city = $this->_request->city;
            $ip = Cubix_Geoip::getIP();

            $cityData = $modelCities->getBySlug(strtolower($city));

            if ($tag_id && $cityData)
            {
                $data = array('city_id' => $cityData->id,'tag_id' => $tag_id,'ip' => $ip);
                die(json_encode(array('status' => Model_Escorts::insertIntoTracker($data))));
            }

            die(json_encode(array('status' => 'error')));
        }
    }
}
