<?php
class BubbleController extends Zend_Controller_Action
{	
	public function indexAction()
	{
        if ($this->_getParam('ajax')) {
            $this->view->layout()->disableLayout();
            $page = $this->_request->page;
            $countToLoad = $this->_request->countToLoad;

            $count = IS_MOBILE_DEVICE ? 5 : 44;
            $page = $this->_request->page ? $this->_request->page : 1;

            if (!empty($countToLoad) AND is_numeric($countToLoad)) {
                $count = intval($countToLoad);
            }

            $client = Cubix_Api_XmlRpc_Client::getInstance();
            try {
                $bubbles = $client->call('Escorts.getBubbleTexts', array($page, $count, null, null, null, $this->_request->city_id));
            } catch (Exception $e) {
                $bubbles = array('texts' => array(), 'count' => 0);
            }

            $this->view->bubbles = $bubbles;
            $this->view->page = $page;
        }
	}

    public function bubbleAction()
    {
        if($this->_getParam('ajax')){
            $this->view->layout()->disableLayout();
        }

        if ($this->_request->page)
        {
            $page = $this->_request->page;
        }else{
            $page = 1;
        }

        $countToLoad = $this->_request->countToLoad;
        $count = BUBBLES_PER_PAGE_COUNT;

        if (!empty($countToLoad) AND is_numeric($countToLoad)) {
            $count = intval($countToLoad);
        }

        $client = Cubix_Api_XmlRpc_Client::getInstance();
        try {
            $bubbles = $client->call('Escorts.getBubbleTexts', array($page, $count, null, null, null, $this->_request->city_id));
        }
        catch ( Exception $e ) {
            $bubbles = array('texts' => array(), 'count' => 0);
        }

        $this->view->cities = Model_Cities::getByCountry(Cubix_Application::getById()->country_id);
        $this->view->city = $this->_request->city_id;
        $this->view->bubbles = $bubbles;
        $this->view->page = $page;
    }
}
