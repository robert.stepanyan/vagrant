<?php
class AdvertiseController extends Zend_Controller_Action{
	public function indexAction()
    {
        if(isset($_SESSION['message'])){
            $this->view->message = $_SESSION['message'];
            unset($_SESSION['message']);
        }
    }

    // public $packages = [
    //     'single' => [
    //         ['title' => '15 DAYS', 'price' => 16, 'days' => 15, 'type' => 'female'],
    //         ['title' => '1 MONTH', 'price' => 28, 'days' => 30, 'type' => 'female'],
    //         ['title' => '2 MONTHS', 'price' => 51, 'days' => 60, 'type' => 'female'],
    //         ['title' => '3 MONTHS', 'price' => 65, 'days' => 90, 'type' => 'female'],
    //     ],
    //     'single-trans-male' => [
    //         ['title' => '1 MONTH', 'price' => 16, 'days' => 30, 'type' => 'trans or male'],
    //         ['title' => '2 MONTH', 'price' => 30, 'days' => 60, 'type' => 'trans or male'],
    //         ['title' => '3 MONTH', 'price' => 39, 'days' => 90, 'type' => 'trans or male'],
    //     ],
    //     'agencies-3' => [
    //         ['title' => '15 DAYS', 'price' => 42.24, 'days' => 15, 'type' => 'agency 3'],
    //         ['title' => '1 MONTH', 'price' => 73.92, 'days' => 30, 'type' => 'agency 3'],
    //         ['title' => '2 MONTHS', 'price' => 134.64, 'days' => 60, 'type' => 'agency 3'],
    //         ['title' => '3 MONTHS', 'price' => 211.2, 'days' => 90, 'type' => 'agency 3'],
    //     ],
    //     'agencies-6' => [
    //         ['title' => '15 DAYS', 'price' => 79.68, 'days' => 15, 'type' => 'agency 6'],
    //         ['title' => '1 MONTH', 'price' => 139.44, 'days' => 30, 'type' => 'agency 6'],
    //         ['title' => '2 MONTHS', 'price' => 253.98, 'days' => 60, 'type' => 'agency 6'],
    //         ['title' => '3 MONTHS', 'price' => 323.70, 'days' => 90, 'type' => 'agency 6'],
    //     ],
    //     'banner' => [
    //         ['title' => '1 MONTH', 'price' => 50, 'days' => 30, 'type' => 'Sidebar'],
    //         ['title' => '1 MONTH', 'price' => 35, 'days' => 30, 'type' => 'Between Ads'],
    //         ['title' => '1 MONTH', 'price' => 30, 'days' => 30, 'type' => 'For Agencies Who Have 1 PREMIUM Ad Active'],
    //         ['title' => '1 MONTH', 'price' => 25, 'days' => 30, 'type' => 'For Agencies With At Least 3 PREMIUM Ads Active'],
    //     ],
    // ];

    public function mmgAction()
    {

        $this->view->layout()->disableLayout();
        $request = $this->_request;

        if (!$this->_request->isPost() && !isint($request->id)) die("Invalid Request");
        

        $mmgBill = new Model_MmgBillAPIV2();
        $client = new Cubix_Api_XmlRpc_Client();
        
        $id = $this->_getParam('id');
        $price = $this->_getParam('price');
        $days = $this->_getParam('days');

        
        $hash  = base_convert(time(), 10, 36);
        $this->user = Model_Users::getCurrent();

        $escort_id = 0;
        $agency_id = 0;
        $user_type  = 'not-logged';

        
        if($this->user){
            $user_id = $this->user->id;
            $user_type = $this->user->isAgency() ? USER_TYPE_AGENCY : USER_TYPE_SINGLE_GIRL;
            if($user_type == USER_TYPE_SINGLE_GIRL){
                $escort_id = $this->user->escort_data['escort_id'];
            }else{
                $agency_id = $this->user->agency_data['agency_id'];
            }
        }else{
            $user_id = 0;
        }

        $data = array('advertise_page' => true, 'amount' => $price,  'days' => $days, 'package_id' => $id);

        $post_data[] = array(
            'user_id' => $user_id,
            'escort_id' => $escort_id,
            'agency_id' => $agency_id,
            'package_id'=> null,
            'data' => serialize($data),
            'hash' => $hash
        );


        if(APPLICATION_ENV == 'production') {
            $postback_url = 'https://www.escortguide.co.uk/advertise';
        }else{
            $postback_url = 'http://www.escortguide2.co.uk.test/advertise';
        }


        $amount = $client->call(
            'OnlineBillingV2.addToShoppingCartED',
            array(
                $post_data,
                $user_id,
                $user_type,
                $hash
            )
        );

        $hosted_url = $mmgBill->getHostedPageUrl($price, 'ADZ'.$hash, $postback_url.$this->view->getLink('callback-mmg-response'));

        if (filter_var($hosted_url, FILTER_VALIDATE_URL) === FALSE) {
            $status = 'not-valid-url';
        }else{
            $status = 'success';
        }

        die(json_encode(array('status' => $status, 'url' =>  $hosted_url, 'package' => $package)));

    }

    public function callbackMmgResponseAction()
    {
      
        $client = new Cubix_Api_XmlRpc_Client();
        $request = $this->_request;

        $payment_result = [];
        
       
        if (isset($request->txn_status) and $request->txn_status == 'APPROVED') {

            $hash = str_ireplace('ADZ', '', $request->ti_mer);

            $shoping_card = $client->call( 'OnlineBillingV2.updateShoppingCartED', array( $hash ) );
            if(!is_array( $shoping_card)) return $this->_redirect('/advertise');

            $data = unserialize($shoping_card['data']);
          


            $payment_result['status'] = $request->txn_status;            
            $payment_result['merchant'] = $request->mid;
            $payment_result['mmg'] = $request->ti_mmg;
            $payment_result['ti_mer'] = $request->ti_mer;
            $payment_result['amount'] = $data['amount'];

            if ($request->mmg_errno != '0000')
                $payment_result['error'] = $request->mmg_errno;

        } else {
            $payment_result['status'] = 'REJECTED';
        }


        $this->view->payment_status = $payment_result['status'];
        $this->view->payment_result = $payment_result;
    }
    
    public function advertisePaymentEmailAction(){
        
    
        $request = $this->_request;

        $payment_result = (isset($request->payment_data) && !empty($request->payment_data)) ? unserialize($request->payment_data) : [];
        $payment_result['email'] = isset($request->email) ? $request->email : '- - -';
        $payment_result['Link'] = isset($request->link) ? $request->link : '- - -';
        
        $current_user = Model_Users::getCurrent();

        if(!empty($current_user)){

            if ( $current_user->user_type == 'agency' ) {
                $payment_result['User'] = $current_user->agency_data['agency_id'];
            }
            else if ( $current_user->user_type == 'escort' ) {
                $payment_result['User'] = $current_user->escort_data['escort_id'];
            }
            
        }else{
            $payment_result['User'] = 'Was not authenticated';
        }
    
        $payment_details_string = '<ul>';
        foreach($payment_result as $key => $val){
            $payment_details_string .= "<li> <b> $key </b> $val </li>";
        }
        $payment_details_string .= '</ul>';

        $to = array('brandy@escortguide.co.uk ', 'mihranhayrapetyan@gmail.com' );
        //$to = 'edhovhannisyan97@gmail.com';
        
        $mail_sent = Cubix_Email::sendTemplate('payment_details', $to, array(
            'details' => $payment_details_string
        ));

        if($mail_sent){
            $message = [
                'type' => "success",
                'text' => "Email has been sent."
            ];
        }else{
            $message = [
                'type' => "danger",
                'text' => "Couldn't send email, please contact our team."
            ];
        }

        $_SESSION['message'] = $message;
        unset($_SESSION['payment_mmg_amount']);
        $this->_redirect('/advertise');
    }

}