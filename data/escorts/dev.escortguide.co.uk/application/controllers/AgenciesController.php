<?php

class AgenciesController extends Zend_Controller_Action
{
	public static $linkHelper;
	
	public function init()
	{
		self::$linkHelper = $this->view->getHelper('GetLink');
	}
	
	public function indexAction()
	{
		
	}

	public function listAction()
	{
		$cache = Zend_Registry::get('cache');
	
		
		$this->view->p_id = $this->_request->getParam('p_id');

		$this->view->static_page_key = 'agencies';
		

		$default_sorting = "default";
		$current_sorting = $this->_getParam('agency_sort', $default_sorting);

		$sort_map = array(
			'newest' => 'date_registered DESC',
			'last_modified' => 'cd.last_modified DESC',
			'last-connection' => 'refresh_date DESC',
			'most-viewed' => 'hit_count DESC',
			'random' => 'RAND()',
			'by-city' => 'ct.title_en ASC',			
			'alpha' => 'cd.club_name ASC',
            'default' => 'cd.escorts_count DESC'
		);

		$filter_text = $this->view->text = trim($this->_request->text);
		$filter_name = $this->view->name = trim($this->_request->name);
		$filter_city_id = $this->view->city_id = $this->_request->city_id;


		$filter = array(
			'text' => $filter_text,
			'name' => $filter_name,
			'city_id' => $filter_city_id
		);
		
		if ( !array_key_exists($current_sorting, $sort_map) ) {
			$current_sorting = $default_sorting;
			$this->_request->setParam('agency_sort', $default_sorting);
		}

		if ( $current_sorting != $_COOKIE['agency_sort'] && $this->_request->s ) {
			setcookie("a_sorting", $current_sorting, strtotime('+1 year'), "/", "www." . preg_replace('#www.|dev.#', '', $_SERVER['HTTP_HOST']));
			setcookie("a_sortingMap", $sort_map[$current_sorting]['map'], strtotime('+1 year'), "/", "www." . preg_replace('#www.|dev.#', '', $_SERVER['HTTP_HOST']));
			$_COOKIE['a_sorting'] = $current_sorting;
			$_COOKIE['a_sortingMap'] = $sort_map[$current_sorting]['map'];
		}

		if ( isset($_COOKIE['a_sorting']) ) {
			$current_sorting = $_COOKIE['a_sorting'];
			$this->_request->setParam('agency_sort', $current_sorting);
		}

		if ( $current_sorting ) {
			$tmp = $sort_map[$current_sorting];
			unset($sort_map[$current_sorting]);
			$sort_map = array_merge(array( $current_sorting => $tmp), $sort_map);
		}

		if ( $current_sorting ) {
			$sorting = $current_sorting;
		}
		
		$this->view->sort_map = $sort_map;
		$this->view->current_sort = $current_sorting;

		$page = intval($this->_request->page);

		if ( $page == 0 ) {
			$page = 1;
		}

		$e_config = Zend_Registry::get('escorts_config');
		$per_page_def = $e_config['agencyPerPage'];
		$per_page = $this->_getParam('per_page', $per_page_def);
		$this->view->per_page = $per_page;

		/*$geoData = array();

		if ( $sorting == 'close-to-me' ) {
			$geoData = Cubix_Geoip::getClientLocation();
		}*/

		$m_agencies = new Model_Agencies();

		$count = 0;

        $cache = Zend_Registry::get('cache');
        $cache_key = 'main_agencies_'.Cubix_I18n::getLang().'_page_'.$page.'_perpage_'.$per_page.'_sort_'.$sorting.'_filter_'.implode('_', $filter);
        $cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

        $forPage = 'agencies_page';

        $agencies = $cache->load($cache_key);
        if ( ! $agencies ) {
            $agencies = $m_agencies->getAll($filter, $page, $per_page, $sorting, $count, $forPage);
            $cache->save($agencies, $cache_key, array());
        }

		$this->view->count = $agencies['count'];
		$this->view->page = $page;
		$this->view->per_page = $per_page;
		$this->view->agencies = $agencies['agencies'];
        $this->view->cities =$m_agencies->getAllCitiesForFilter(Cubix_Application::getById()->country_id);

		if ( ! is_null($this->_getParam('ajax')) ) {
            $agencies_list = $this->view->render('agencies/list-agencies.phtml');
            die(json_encode(array('escort_list' => $agencies_list)));
		}
	}

	public function showAction()
	{
		$agency_id = (int) $this->_request->agency_id;

		$m_agencies = new Model_Agencies();

		$agency = $this->view->agency = $m_agencies->getById($agency_id);

	
		if ( $agency ) {
			Model_Agencies::hit($agency_id);
			$params = array('e.agency_id = ?' => $agency_id);
			$escorts_count = 0;
			$this->view->a_page = $page = 1;
			$this->view->a_per_page = $per_page = 12;
			$this->view->a_escorts = Model_Escort_List::getAgencyEscorts($params, 'newest', $page, $per_page, $escorts_count);
			
			$this->view->a_count = $escorts_count;
		} else {
			header("HTTP/1.1 301 Moved Permanently");
			header("Location: " . $this->view->getLink('navigation-link', array('top_category' => 'agencies')));
		}
	}

	public function escortsAction()
	{

		$agency_id = (int) $this->_request->agency_id;

		$m_agencies = new Model_Agencies();

		$agency = $this->view->agency = $m_agencies->getById($agency_id);

		$page = (int) ($this->_request->page) ? $this->_request->page : 1;

        if ( $agency ) {
			$params = array('e.agency_id = ?' => $agency_id);

			$escorts_count = 0;
			$this->view->a_page = $page;
			$this->view->a_per_page = $per_page = 12;
			$this->view->a_escorts = Model_Escort_List::getAgencyEscorts($params, 'newest', $page, $per_page, $escorts_count);
			$this->view->a_count = $escorts_count;

		} else {
			header("HTTP/1.1 301 Moved Permanently");
			header("Location: " . $this->view->getLink('navigation-link', array('top_category' => 'agencies')));
		}

	}
}
