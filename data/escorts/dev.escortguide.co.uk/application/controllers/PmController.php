<?php

class PmController extends Zend_Controller_Action
{
	public function init()
	{
		
		$anonym = array();
		$this->user = Model_Users::getCurrent();

		if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}else if($this->user && $this->user->user_type == 'escort' && !$this->user->getEscort()->hasProfile()){
            $this->_redirect('/private#profile/index');
            return;
        }


		$this->view->user = $this->user;
		$this->client = Cubix_Api::getInstance();
	
	}

	public function indexAction()
	{
		
	}

}