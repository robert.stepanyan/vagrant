<?php

/**
 * Created by Zhora.
 * User: Zhora
 * Date: 04.07.2018
 * Time: 11:44
 */

require_once 'DBConnection.php';

class Base
{
    protected $_instance;

    function __construct()
    {
        $this->_instance = DBConnection::getInstance();
    }
}