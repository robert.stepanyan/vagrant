<?php
/**
 * Created by Zhora.
 * User: Zhora
 * Date: 04.07.2018
 * Time: 13:43
 */

abstract class AbstractMigrationBuilder {
    abstract function buildFile();
    abstract function saveFile();
}