<?php
        
require_once dirname(__DIR__).'/core/Base.php';
        
class m_5b3cc9e810726_test_table extends Base{
                
    public function up(){
        try{
            $sql = 'CREATE TABLE tests (
                    PersonID int,
                    LastName varchar(255),
                    FirstName varchar(255),
                    Address varchar(255),
                    City varchar(255) 
                );';
            $query = $this->_instance->_db->prepare($sql);
            $query->execute();
            
            echo  "m_5b3cc9e810726_test_table migration is done !\n\r ";
            return true;
        }catch (Exception $e){
            var_dump($e->getMessage());
            return false;
        }
    } 
}
         
