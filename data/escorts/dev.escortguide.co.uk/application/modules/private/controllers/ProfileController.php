<?php

class Private_ProfileController extends Zend_Controller_Action
{
	/**
	 * @var Model_Escort_Profile
	 */
	protected $profile;

	/**
	 * @var Model_EscortItem
	 */
	protected $escort;

	/**
	 * @var Zend_Session_Namespace
	 */
	protected $session;

	/*
	 * @var Model_Video
	 */
	protected $videoModel;

	public function init()
	{
        $this->videoModel = new Model_Video();
        $client = Cubix_Api_XmlRpc_Client::getInstance();
		$this->_request->setParam('no_tidy', true);
		$anonym = array();

		$this->view->user = $this->user = Model_Users::getCurrent();
		if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {
            if ( is_null($this->_getParam('ajax')) ) {
                $this->_redirect($this->view->getLink('signin'));
                return;
            }else{
                die(json_encode(
                    array(
                        'status' => 'error',
                        'redirect_to_signin' => $this->view->getLink('signin')
                    )
                )
                );
            }
		}

		$this->view->layout()->setLayout('private/main');

        if ( ! is_null($this->_getParam('ajax')) ) {
            $this->view->layout()->disableLayout();
        }

        if ( ! is_null($this->_getParam('escort')) ) {
            $this->view->escortId = $this->_getParam('escort');
        }

		$this->step = $this->_getParam('step');
        $this->view->step_title = 'Complete your profile!';

		$escort_id = intval($this->_getParam('escort'));
		if ( $this->user->isAgency() && $escort_id > 1 ) {
			$this->agency = $agency = $this->user->getAgency();
			if ( ! $agency->hasEscort($escort_id) ) {
				return $this->_redirect($this->view->getLink('private-v2-escorts'));
			}

			$escorts = new Model_Escorts();
			$this->escort = $escorts->getById($escort_id);
		}
		elseif ( $this->user->isAgency() ) {
			$this->agency = $agency = $this->user->getAgency();
			$this->escort = new Model_EscortItem(array('id' => null));
            $agency_escorts = $client->call('Agencies.getEscortsByStatus', array($this->agency->getId()));
            if(!(boolean) $agency_escorts['escorts_count']){
                $this->view->step_title = 'Add your first escort!';
            }else{
                $this->view->step_title = 'Add a new escort!';
            }


		}
		else {
			$this->escort = $this->user->getEscort();
			if ( is_null($this->escort) ) $this->escort = new Model_EscortItem(array('id' => null));
		}

        
		$this->view->escort = $this->escort;

        if(isset($this->escort->is_suspicious) && $this->escort->is_suspicious && !in_array($this->_request->getActionName(), array('gallery', 'index'))) {
            $this->_redirect($this->view->getLink('private'));
			return;
        }
		
		// Determine the mode depending on if user has profile
		$this->mode = $this->view->mode = ((isset($this->escort->id) && $this->escort->id && $this->escort->hasProfile() && $this->step != 'finish') ? 'update' : 'create');

        //echo $this->_request->getActionName();
        if( $this->mode === 'create' && !$this->step && !in_array($this->_request->getActionName(), array('index','gotd')) ){
            die('Bad request !');
        }
		
		$session_hash = '';
		if ( $this->_request->session_hash ) {
			$session_hash = $this->view->session_hash = $this->_request->session_hash;
		}
		else {
			$session_hash = $this->view->session_hash = md5(microtime() * time());
		}

		$this->session = new Zend_Session_Namespace('profile-data-' . $session_hash);


		$this->profile = $this->view->profile = $this->escort->getProfile();
		$this->profile->setSession($this->session);
		$this->profile->load();

		// If we are in update mode, then we need to set the profile model
		// update in real-time mode instead of updating session
		if ( 'update' == $this->mode ) {
			$this->profile->setMode(Model_Escort_Profile::MODE_REALTIME);
		}

		$this->defines = $this->view->defines = Zend_Registry::get('defines');

		$config = Zend_Registry::get('escorts_config');

        if($this->mode == 'create' || ($this->step == 'contact-info-step' || $this->step == 'gallery')){
            $steps = array('biography-step', 'about-me-step', 'working-cities-step', 'services-step', 'working-times-step','contact-info-step','gallery','finish-step');
            if($this->user->isEscort()){
                array_unshift($steps,'complete-profile');
            }

        }else{
            $steps = array('biography', 'about-me', 'working-cities', 'services', 'working-times','contact-info','gallery');

            if ($config['profile']['rates'])
                $steps[] = 'prices';

            $steps[] = 'contact-info';
            $steps[] = 'gallery';
            $steps[] = 'finish';
        }

		
		$this->steps = $steps;
	}

	protected $_c = 0;

	//protected $steps = array('biography', 'about-me', 'languages', 'working-cities', 'services', 'working-times', 'prices', 'contact-info', 'gallery', 'finish');
	protected $steps = array();
	protected $_posted = false;



    public function simpleAction(){
        
            $this->view->simple = true;

            //$this->view->layout()->setLayout('simple-profile');
            $this->view->addScriptPath($this->view->getScriptPath('private'));

            $this->_helper->viewRenderer->setScriptAction("simple");

            

            $this->agency = $agency = $this->user->getAgency();
			$this->escort = new Model_EscortItem(array('id' => null));

            $this->_posted = $this->_request->isPost();
            $data = array();
            $errors = array();
            $is_error = false;
            $counter = 0;

            $this->view->step = 'biography';

            foreach($this->steps as $step){
                $method = str_replace('-', ' ', $step);
                $method = ucwords($method);
                $method = str_replace(' ', '', $method);
                $method[0] = strtolower($method[0]);
                
                if($step === 'finish' && $this->_posted && !$is_error){
                   
                     $this->finishAction();
                     $this->view->step = 'finish';
//                     $this->_helper->viewRenderer->setScriptAction('finish');
					 $tid = (int)$this->view->escort_id;
					 $this->_redirect($this->view->getLink('private-v2-profile', array('step' => 'gallery', 'escort' => $tid, 'show_success' => 1), true));
                }elseif($step !== 'finish'){
					if ( $step === 'gallery' ){
						continue;
					}
                     $this->{$method . 'Action'}();
                }else{
                    continue;
                }
                
                $tmpData = $this->view->data;
                if($tmpData){
                    $data = array_merge($data,$tmpData);
                }

                $tmpErrors = $this->view->errors;
                if($tmpErrors){
                    $errors = array_merge($errors,$tmpErrors);
                    $is_error = true;
                }
                $counter++;
            }

            $this->view->data = $data;
            
            if(!$this->_posted){
                $this->view->data['phone_number'] = $agency['phone'];
                $this->view->data['email'] = $agency['email'];
                $this->view->data['website'] = $agency['web'];
            }

            $this->view->errors = $errors;
        
    }


	public function indexAction()
	{

//        if($this->escort->is_suspicious){
//            $this->_response->setRedirect($this->view->getLink('signin'));
//			return;
//        }

	
//		var_Dump( $_POST );
//		exit;
		//$titles = array('biography', 'about_me', 'languages', 'working_cities', 'services', 'working_times', 'prices', 'contact_info', 'gallery', 'finish' );



		$config = Zend_Registry::get('escorts_config');
		$titles = array('biography', 'about_me', 'languages', 'working_cities', 'services', 'working_times');
				
		if ($config['profile']['rates'])
			$titles[] = 'prices';
		
		$titles[] = 'contact_info';
		$titles[] = 'gallery';
		$titles[] = 'finish';
		
		foreach ( $titles as $i => $key ) {
			$titles[$i] = __('pv2_tab_' . $key);
		}

		// If the mode is update, i.e. user already has a profile revision
		// we don't need the last step "Finish"
		if ( $this->mode == 'update' ) {
			unset($this->steps[count($this->steps) - 1]);
			unset($titles[count($titles) - 1]);
		}

		$this->view->steps = array_combine($this->steps, $titles);

		$this->_posted = $this->_request->isPost() && (
			($this->_getParam('then') == ':next' && $this->mode == 'create') ||
			( $this->mode == 'update' )
		);

		$result = $this->_do();

		
		$then = $this->_getParam('then');
     
		// If the user clicked the update button, and the result is successfull
		// and we don't need to switch to another tab, just display
		// a user friendly message
		if ( $this->_posted && true === $result && ! strlen($then) ) {
			$this->view->status = 'The section "' . $this->view->steps[$this->view->step] . '" has been successfully updated!';
		}

		if ( false === $result || ! strlen($then) ) { return; }

		switch (true) {
			case ($then == ':next'):
				$i = array_search($this->_getParam('step'), $this->steps);
				if ( $i == count($steps) - 1 ) return;
				
				$step = $this->steps[$i + 1];
				
				break;
			case ($then == ':back'):
				$i = array_search($this->_getParam('step'), $this->steps);
				if ( $i == 0 ) return;

				$step = $this->steps[$i - 1];

				break;
			default:
                
				if ( ! in_array($then, $this->steps) ) {
					return;
				}

				$step = $then;
		}

        if($step == 'gallery'){
            $this->view->inStep = true;
        }


		$this->_setParam('step', $step);
		$this->_setParam('then', '');
		$this->_posted = false;

		$this->_do();

       


		
	}

	protected function _do()
	{

		$step = $this->_getParam('step');

		// User requested editting of profile without specifying
		// the step, i.e. if he/she clicks on profile icon in private are
		// load the profile from api into current session
		if ( ! in_array($step, $this->steps) ) {
			$this->profile->load(true);
			$step = reset($this->steps);
		}

		$this->view->step = $step;

		$method = str_replace('-', ' ', $step);
		$method = ucwords($method);
		$method = str_replace(' ', '', $method);
		$method[0] = strtolower($method[0]);


		$validator = new Cubix_Validator();

		$about_me_data = $this->profile->getAboutMe();
		foreach ( Cubix_I18n::getLangs(true) as $lng ) {
			$about_me_data['about_' . $lng] = $validator->urlCleaner($about_me_data['about_' . $lng], Cubix_Application::getById()->host);
			$clean_text_length = strlen(trim(strip_tags($validator->specialCharsCleaner($about_me_data['about_' . $lng]))));
			if( $clean_text_length > 0 && $clean_text_length < 200 ){
				$this->_helper->viewRenderer->setScriptAction('aboutMe');
				$this->view->step = 'about-me';
				$this->_setParam('about_me_validator', 1);
				return $this->{'aboutMeAction'}();
			}
		}



		$this->_helper->viewRenderer->setScriptAction($step);

		return $this->{$method . 'Action'}();
	}

	protected function _validateBiography($validator)
	{
		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'showname' => '',
			'slogan' => 'notags|special',
			'gender' => 'int-nz',
			'age' => 'int-nz',
			'ethnicity' => 'int-nz',
			'nationality_id' => 'int-nz',
			'home_city_id' => 'int-nz',
			'hair_color' => 'int-nz',
			'hair_length' => 'int-nz',
			'eye_color' => 'int-nz',
			'measure_units' => 'int-nz',
			'height' => '',
			'weight' => 'int-nz',
			'dress_size' => '',
			'shoe_size' => 'int-nz',
			'bust' => 'int-nz',
			'waist' => 'int-nz',
			'hip' => 'int-nz',
			'cup_size' => '',
			'breast_type' => '',
			'pubic_hair' => 'int-nz',
			'tatoo' => 'int-nz',
			'piercing' => 'int-nz',
			'block_countries' => 'arr-int',
		));
		$data = $form->getData();

		$defines = Zend_Registry::get('defines');
		$data['showname'] = preg_replace('/(\s)+/','$1', trim($data['showname']));
		$escorts_model = new Model_Escorts();
		$blackListModel = new Model_BlacklistedWords();
		if ( ! strlen($data['showname']) ) {
			$validator->setError('showname', Cubix_I18n::translate('sys_error_required'));
		}
		elseif(mb_strlen($data['showname']) > 25 ){
				$validator->setError('showname', Cubix_I18n::translate('sys_error_showname_no_longer'));
		}
		elseif ( ! preg_match('#^[-_a-z0-9\s]+$#i', $data['showname']) ) {
			$validator->setError('showname', Cubix_I18n::translate('sys_error_must_contain'));
		}
		/*elseif ( $escorts_model->existsByShowname($data['showname'], $this->escort->getId()) ) {
			$validator->setError('showname', 'An escort with same showname exists');
		}*/

		if ( ! strlen($data['home_city_id']) ) {
//			$validator->setError('home_city_a', 'Home city is required');
			$data['home_city_id'] = null;
		}

		if ( is_null($data['eye_color']) ) {
			$validator->setError('eye_color', Cubix_I18n::translate('sys_error_required'));
		}

		if ( is_null($data['hair_color']) ) {
			$validator->setError('hair_color', Cubix_I18n::translate('sys_error_required'));
		}

		if ( ! strlen($data['slogan']) ) {
			$data['slogan'] = null;
		}
		else if( mb_strlen($data['slogan']) > 20 ) {
			$validator->setError('slogan', Cubix_I18n::translate('sys_error_slogan_text_must_be'));
		}
		else if($blackListModel->checkWords($data['slogan'], Model_BlacklistedWords::BL_TYPE_SLOGAN)) {
			$validator->setError('slogan','You can`t use word "'.$blackListModel->getWords() .'"');
		}
		else{
			$data['slogan'] = $validator->urlCleaner($data['slogan'], Cubix_Application::getById()->host);
			$data['slogan'] = $validator->removeEmoji($data['slogan']);
		}
		
		if ( is_null($data['gender']) ) {
			$validator->setError('gender', Cubix_I18n::translate('sys_error_required'));
		}
		else if ( ! array_key_exists($data['gender'], $defines['gender_options']) ) {
			$validator->setError('gender', Cubix_I18n::translate('sys_error_gender_is_invalid'));
		}

		$age = $data['age']; unset($data['age']);
		if ( ! is_null($age) ) {
			$data['birth_date'] = strtotime('-' . $age . ' year');
		}

		if ( ! in_array($data['measure_units'], array(METRIC_SYSTEM, ROYAL_SYSTEM)) ) {
			$data['measure_units'] = METRIC_SYSTEM;
		}

		if ( ! in_array($data['cup_size'], $this->defines['breast_size_options']) ) {
			$data['cup_size'] = null;
		}

		if ( Cubix_Application::getById()->measure_units == ROYAL_SYSTEM ) {
			if ( $data['height'] ) {
				$data['height'] = Cubix_UnitsConverter::convert(stripslashes($data['height']), 'ftin', 'cm');
			}

			if ( $data['weight'] ) {
				$data['weight'] = Cubix_UnitsConverter::convert($data['weight'], 'lbs', 'kg');
			}
		}

		foreach ( $data['block_countries'] as $i => $zone_id ) {

			if ( $zone_id != '0' ) {
				$data['block_countries'][$i] = array('country_id' => $zone_id);

			}
			else {
				unset($data['block_countries'][$i]);
			}
		}



		$data['height'] = intval($data['height']);
		if ( ! $data['height'] ) $data['height'] = null;
		if ( Cubix_Application::getById()->measure_units != ROYAL_SYSTEM ) {
			$data['weight'] = intval($data['weight']);
		}
		if ( ! $data['weight'] ) $data['weight'] = null;


		return $data;
	}

	public function biographyAction()
	{

        $countyModel = new Model_Countries();

		$this->view->countries = $countyModel->getCountries();
        $this->view->action = 'biography';
		if ( $this->_request->isPost() ) {
			$validator = new Cubix_Validator();
			
			$this->view->data = $data = $this->_validateBiography($validator);


			$home_city_id = $this->view->data['home_city_id'];

			$home_city_country = '';
			if ( $home_city_id ) {
				$m_city = new Cubix_Geography_Cities();
				$home_city = $m_city->get($home_city_id);

				$m_country = new Cubix_Geography_Countries();
				$home_country = $m_country->get($home_city->country_id);

				$home_city_country = $home_city->{'title_' . $this->_request->lang_id} . ' (' . $home_country->{'title_' . $this->_request->lang_id} . ')';
			}
			$this->view->home_city_country = $home_city_country;

		

			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();
				
				$this->view->errors = $status['msgs'];
                die(json_encode($status));
			}
			
			$this->_addSlogan($data['slogan'], $this->escort->id);
			$slogan = $this->_getSlogan($this->escort->id);
			$this->view->slogan = $slogan['text'];
			$this->view->slogan_status = $slogan['status'];

			return $this->profile->update($data, 'biography');
		}
		else {

			$this->view->data = $this->profile->getBiography();

			$home_city_id = $this->view->data['home_city_id'];

			$home_city_country = '';
			if ( $home_city_id ) {
				$m_city = new Cubix_Geography_Cities();
				$home_city = $m_city->get($home_city_id);

				$m_country = new Cubix_Geography_Countries();
				$home_country = $m_country->get($home_city->country_id);

				$home_city_country = $home_city->{'title_' . $this->_request->lang_id} . ' (' . $home_country->{'title_' . $this->_request->lang_id} . ')';
			}

		
			
//			$this->view->countries = $countyModel->getCountries();
			
			$this->view->home_city_country = $home_city_country;
			$slogan = $this->_getSlogan($this->escort->id);
			$this->view->slogan = $slogan['text'];
			$this->view->slogan_status = $slogan['status'];

		}
	}

	protected function _addSlogan($slogan, $escort_id)
	{
		$config_system = Zend_Registry::get('system_config');
		
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$client->call('Escorts.addSlogan', array($slogan, $escort_id, $config_system['sloganApprovation']));

		return true;
	}

	protected function _getSlogan($escort_id)
	{
		if ( $escort_id ) {
			$client = Cubix_Api_XmlRpc_Client::getInstance();
			$slogan = $client->call('Escorts.getSlogan', array($escort_id));

			return $slogan;
		}
		else {
			return false;
		}
	}

	protected function _validateAboutMe($validator)
	{
		$blackListModel = new Model_BlacklistedWords();
		$form = new Cubix_Form_Data($this->_request);
		$fields = array(
			'is_smoking' => 'int-nz',
			'is_drinking' => 'int-nz',
			'characteristics' => 'notags|special'
		);

		foreach ( Cubix_I18n::getLangs(true) as $lng ) {
			$fields['about_' . $lng] = 'xss';
		}
		
		$form->setFields($fields);
		$data = $form->getData();

		$a_exists = array();
        foreach ( Cubix_I18n::getLangs(true) as $lng ) {
			if($bl_words = $blackListModel->checkWords($data['about_' . $lng], Model_BlacklistedWords::BL_TYPE_ABOUT)) {
				foreach($bl_words as $bl_word){
					$pattern = '/' . preg_quote($bl_word, '/') . '/i';
					$data['about_' . $lng] = preg_replace($pattern, '<abbr class = "black-listed" >' . $bl_word . '</abbr>', $data['about_' . $lng]);
				}

				$validator->setError('about_me_text', 'You can`t use word "'.$blackListModel->getWords() .'"');
				break;
			}
			else
				$data['about_' . $lng] = $validator->urlCleaner($data['about_' . $lng], Cubix_Application::getById()->host);
				$data['about_' . $lng] = $validator->removeEmoji($data['about_' . $lng]);
                $data['about_' . $lng] = strip_tags(htmlspecialchars($data['about_' . $lng]), '<b><i><u><strong><em><p><span>');

				$clean_text_length = strlen(trim(strip_tags($data['about_' . $lng])));
				if( $clean_text_length < 200 ){
					$validator->setError('about_me_text', Cubix_I18n::translate('about_me_is_short',array('LENGTH'=>200)));
				}
			if (strlen($data['about_' . $lng]))
				$a_exists[] = $lng;
		}

		if (!count($a_exists))
			$validator->setError('about_me_text', 'Required');
		
		if($blackListModel->checkWords($data['characteristics'], Model_BlacklistedWords::BL_TYPE_SPECIAL_CHAR)) {
			$validator->setError('characteristics','You can`t use word "'.$blackListModel->getWords() .'"');
		}
		else{
			$data['characteristics'] = $validator->urlCleaner($data['characteristics'], Cubix_Application::getById()->host);
		}
		
		return $data;
	}

	public function aboutMeAction()
	{
		$validator = new Cubix_Validator();
        $this->view->action = 'about-me';

		if ( $this->_request->isPost() ) {
			$blackListModel = new Model_BlacklistedWords();
			$dataAboutMe = $this->_validateAboutMe($validator);
			$dataLanguages = $this->_validateLanguages($validator);
			$this->view->data = $data = array_merge($dataAboutMe,$dataLanguages);

			$data['about_has_bl'] = 0;
			if ( ! $validator->isValid() ) {
				
				$status = $validator->getStatus();
				
				$this->view->errors = $status['msgs'];
                die(json_encode($status));
				
			}

            return $this->profile->update($data, 'about-me');
		}


        $this->view->data = $this->profile->getAboutMe();

        if ($this->_getParam('about_me_validator') === 1) {
            $validator->setError('about_me_text', Cubix_I18n::translate('about_me_profile_required', array('LENGTH' => 200)));
            $status = $validator->getStatus();
            $this->view->errors = $status['msgs'];
        }


	}

	public function _validateLanguages($validator)
	{
		$defines = Zend_Registry::get('defines');
		
		$langs = $this->_getParam('langs', array());
		if ( ! is_array($langs) ) $langs = array();

		$data = array('langs' => array());

		$invalid_lang = false;
		foreach ( $langs as $lang_id => $level ) {
			$level = intval($level); if ( $level < 1 ) continue;
			$data['langs'][] = array('lang_id' => $lang_id, 'level' => $level);

			if ( ! array_key_exists($lang_id, $defines['language_options']) ) {
				$invalid_lang = true;
			}
		}

		if ( ! count($data['langs']) ) {
			$validator->setError('language_error', Cubix_I18n::translate('sys_error_language_required'));
		}
		else if ( $invalid_lang ) {
			$validator->setError('language_error', Cubix_I18n::translate('sys_error_lang_is_invalid'));
		}

		return $data;
	}


	protected function _validateWorkingCities($validator)
	{
	    $maxWorkingCitiesCount = Cubix_Application::getById()->max_working_cities;
		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'country_id' => 'int-nz',
			'city_id' => 'int-nz',
			'cities' => 'arr-int',
			'premium_cities' => 'arr-int',
			'cityzones' => 'arr-int',
			'zip' => '',
			'incall' => 'int',
			'incall_type' => 'int-nz',
			'incall_hotel_room' => 'int-nz',
			'incall_other' => 'notags|special',
			'outcall' => 'int',
			'outcall_type' => 'int-nz',
			'outcall_other' => 'notags|special',
            'latitude' => '',
            'longitude' => ''
		));
		$data = $form->getData();

		if ( ! is_array($data['cities']) ) $data['cities'] = array();
		if ( ! is_array($data['premium_cities']) ) $data['premium_cities'] = array();
		if (!in_array($data['city_id'], $data['cities'])){
            $maxWorkingCitiesCount -= 1;
        }
		$invalid_city = false;
		foreach ( $data['cities'] as $i => $city_id ) {
			$data['cities'][$i] = array('city_id' => $city_id);
			if ( ! $city_id ) {
				unset($data['cities'][$i]);
			}
			else if ( ! Cubix_Geography_Cities::isFromApplicationCountry($city_id) ) {
				$invalid_city = true;
				unset($data['cities'][$i]);
			}
		}

		// Anti-Hack: Ignore trailing cities
		$data['cities'] = array_slice($data['cities'], 0, $maxWorkingCitiesCount);

		if ( ! is_array($data['cityzones']) ) $data['cityzones'] = array();

		foreach ( $data['cityzones'] as $i => $zone_id ) {
			if ( $zone_id != '0' ) {
				$data['cityzones'][$i] = array('city_zone_id' => $zone_id);
			}
			else {
				unset($data['cityzones'][$i]);
			}
		}
		
		if ( ! $data['incall'] ) {
			$data['incall_type'] = null;
			$data['incall_hotel_room'] = null;
			$data['incall_other'] = null;
			$data['zip'] = null;
		}
		else{
			if ( !$data['incall_type'] ) {
			$validator->setError('zip', Cubix_I18n::translate('sys_error_choose_suboption'));
			$data['incall_type'] = 999;
			}
		}

		if ( ! $data['outcall'] ) {
			$data['outcall_type'] = null;
			$data['outcall_other'] = null;
		}
		

		if ( $data['incall'] && ! strlen($data['zip']) ) {
//			$validator->setError('zip', 'zip code required');
            $data['zip'] = null;
		}
		unset($data['incall']);
		unset($data['outcall']);
		
		if ( ! is_null($data['incall_type']) && ! $validator->validateZipCode($data['zip'], false) ) {
//			$validator->setError('zip', 'Invalid zip code');
            $data['zip'] = null;
		}
		
		$data['zip'] = $validator->urlCleaner($data['zip'], Cubix_Application::getById()->host);
		$data['incall_other'] = $validator->urlCleaner($data['incall_other'], Cubix_Application::getById()->host);
		$data['outcall_other'] = $validator->urlCleaner($data['outcall_other'], Cubix_Application::getById()->host);
		
		if ( ! is_null($data['incall_type']) && $data['incall_type'] == 2 ) {
			if ( ! isset($data['incall_hotel_room']) ) {
				$validator->setError('incall_hotel_room', Cubix_I18n::translate('sys_error_choose_hotel_room'));
			}
		}

		if ( ! $data['city_id'] && ! count($data['cities']) ) {
			$validator->setError('country_id', Cubix_I18n::translate('sys_error_base_city_required'));
		}
		elseif(!$data['city_id'] && count($data['cities'])){
            $validator->setError('country_id', Cubix_I18n::translate('sys_error_base_city_missing'));
        }
		else if ( ! Cubix_Geography_Cities::isFromApplicationCountry($data['city_id']) ) {
			$validator->setError('country_id', Cubix_I18n::translate('sys_error_base_city_not_from', array('country' => Cubix_Application::getById()->country_title)));
		}
		else if ( $invalid_city ) {
			$validator->setError('country_id', Cubix_I18n::translate('sys_error_selected_cities_not_from', array('country' => Cubix_Application::getById()->country_title)));
		}

		if ( ! $data['country_id'] ) {
			$validator->setError('country_id', Cubix_I18n::translate('sys_error_required'));
		}
		else if ( $data['country_id'] != Cubix_Application::getById()->country_id ) {
			$validator->setError('country_id', Cubix_I18n::translate('sys_error_invalid_country_selected'));
		}

		$lng_lat_regex = '/^(-?\d+(\.\d+)?)\.\s*(-?\d+(\.\d+)?)$/';

        if (!is_null($data['longitude']) && $data['longitude'] != '') {
            if (!preg_match($lng_lat_regex, $data['longitude'])) {
                $validator->setError('longitude', Cubix_I18n::translate('sys_error_invalid_longitude'));
            }
        }

        if (!is_null($data['latitude']) && $data['latitude'] != '') {
            if (!preg_match($lng_lat_regex, $data['latitude'])) {
                $validator->setError('latitude', Cubix_I18n::translate('sys_error_invalid_latitude'));
            }
        }
		
		return $data;
	}

	public function workingCitiesAction()
	{
		$data = $this->profile->getWorkingCities();
		$countries = new Cubix_Geography_Countries();
		$this->view->countries = $countries->ajaxGetAll(false);

        $client = Cubix_Api_XmlRpc_Client::getInstance();
        $user_id = $this->user->id;
		$cities = new Cubix_Geography_Cities();
		$country_id = Cubix_Application::getById()->country_id;
		$region_cities = $this->view->cities = $cities->ajaxGetAll( null , $country_id );
		$this->view->regions = $region_cities;
        $this->view->action = 'working-cities';
        $current_city = $client->call( 'Users.getCityByUserId', array( $user_id ));
        $current_cities = $client->call( 'Escorts.getCitiesByEscortId' , array( $this->escort->id ) );
        $current_cities_data = $client->call( 'Escorts.getCitiesDataByEscortId' , array( $this->escort->id ) );
        $data['city_id'] = $current_city['city_id'];
        $data['cities'] = $current_cities;
        $this->view->cities_data = $current_cities_data;

        $data['city_locations'] = $client->call('Cities.getLocations', array($current_city['city_id']));

        $locations = $client->call('Escorts.getLngLat', array($this->escort->id));
        $data['longitude'] = $locations['longitude'];
        $data['latitude'] = $locations['latitude'];

        $citizone_array = array();
       foreach ($data['cities'] as $data_city){
           $citizone_array[] = $data_city['city_id'];
       }

        $this->_request->setParam('ajax_cities', implode(',', $citizone_array));

		if (  $this->_request->isPost() ) {
			$validator = new Cubix_Validator();

			$this->view->data = $data = $this->_validateWorkingCities($validator);

            $found = false;

            $param_cities = array();
			if ( count($data['cities']) ) {
				foreach ( $data['cities'] as $city ) {
					$param_cities[] = $city['city_id'];
                    if($city['city_id'] == $data['city_id']){
                        $found = true;
                    }
				}
			}


			$citizone_array = $param_cities;
            $citizone_array[] = $data['city_id'];
			$this->_request->setParam('ajax_cities', implode(',', $citizone_array));

			$this->getCityzonesAction();

			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

                $this->view->errors = $status['msgs'];
                die(json_encode($status));
			}


            if(!$found){
                $data['cities'][] = ['city_id' => $data['city_id']];
            }

			$this->profile->update($data, 'working-cities');
		}


			$param_cities = array();
			if ( count($data['cities']) ) {
				foreach ( $data['cities'] as $city ) {
					$param_cities[] = $city['city_id'];
				}
			}

			//////////////////////////////////////
			$client = Cubix_Api::getInstance();
			$premium_cities = $client->call('getEscortActivePackageForPremiumCities', array($this->profile->getId(), 'en'));
			$p_c = array();
			if ( count($premium_cities['cities']) > 0 ) {
				foreach ( $premium_cities['cities'] as $premium_city ) {
					$p_c[] = $premium_city['id'];
				}
			}
        $data['count_premium_cities'] = $premium_cities['count_premium_cities'];
        $data['premium_cities'] = $p_c;
			//////////////////////////////////////

            $this->getCityzonesAction();

			$data['country_id'] = $country_id;

			$this->view->data = $data;

	}

	public function getCityzonesAction()
	{
		$this->view->layout()->disableLayout();

		$cities = $this->_request->ajax_cities;
		$cities = trim($cities, ',');

		$cities = explode(',', $cities);

		$cz_model = new Cubix_Geography_Cityzones();

		$all_cityzones = array();
		if ( count($cities) ) {
			foreach( $cities as $city ) {
				$cityzones = $cz_model->ajaxGetAll($city);
				if ( count($cityzones) ) {
					$all_cityzones = array_merge($all_cityzones, $cityzones);
				}
			}
		}


			$this->view->all_cityzones = $all_cityzones;

	}

	public function _validateServices($validator)
	{		
		$form = new Cubix_Form_Data($this->_request);
		$fields = array(
			'sex_orientation' => 'int-nz',
			'sex_availability' => '',
			'services' => 'arr-int',
			'keywords' => 'arr-int',
			'service_prices' => 'arr-int',
			'service_currencies' => 'arr-int',
            'pornstar_link' =>''
		);

		foreach ( Cubix_I18n::getLangs(true) as $lng ) {
			$fields['additional_service_' . $lng] = 'notags|special';
		}

		$form->setFields($fields);
		$data = $form->getData();

		if ( ! is_array($data['services']) ) $data['services'] = array();

		foreach ( $data['services'] as $i => $svc ) {
			$price = isset($data['service_prices'][$svc]) ? intval($data['service_prices'][$svc]) : null;
			if ( ! $price ) $price = null;

			$currency = isset($data['service_currencies'][$svc]) ? intval($data['service_currencies'][$svc]) : null;
			if ( ! $currency ) $currency = null;
			
			$data['services'][$i] = array('service_id' => $svc, 'price' => $price, 'currency_id' => $currency);
		}

		if ( ! is_array($data['sex_availability']) ) {
			$data['sex_availability'] = array();

			$validator->setError('services_offered_for_error', Cubix_I18n::translate('sys_error_at_least_one_required'));
		}

		if($data['pornstar_link']){
            if ( !filter_var($data['pornstar_link'], FILTER_VALIDATE_URL)) {
                $validator->setError('keywords_error', Cubix_I18n::translate('invalid_url'));
            }
        }

		foreach ( $data['sex_availability'] as $i => $opt ) {
			if ( ! isset($this->defines['sex_availability_options'][$opt]) ) {
				unset($data['sex_availability'][$i]);
			}
		}
		$data['sex_availability'] = count($data['sex_availability']) ?
			implode(',', $data['sex_availability']) : null;

		unset($data['service_prices']);
		unset($data['service_currencies']);
		
		foreach ( Cubix_I18n::getLangs(true) as $lng ) {
			$data['additional_service_' . $lng] = $validator->urlCleaner($data['additional_service_' . $lng], Cubix_Application::getById()->host);
			$data['additional_service_' . $lng] = $validator->removeEmoji($data['additional_service_' . $lng]);
		}
		
		return $data;
	}

	public function servicesAction()
	{
        $this->view->action = 'services';

		if ( $this->_request->isPost() ) {
			$validator = new Cubix_Validator();

			$this->view->data = $data = $this->_validateServices($validator);

			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];
                die(json_encode($status));
			}
			
			return $this->profile->update($data, 'services');
		}

			$data = $this->profile->getServices();
			$this->view->data = $data;

	}

	protected function _validateWorkingTimes($validator)
	{
		$form = new Cubix_Form_Data($this->_request);
		

		$form->setFields(array(
			'available_24_7' => 'int-nz',
			'night_escort' => 'int-nz',
			'day_index' => 'arr-int',
			'time_from' => 'date',
			'time_from_m' => 'date',
			'time_to' => 'date',
			'time_to_m' => 'date',
			'vac_date_from_day' => 'int-nz',
			'vac_date_from_month' => 'int-nz',
			'vac_date_from_year' => 'int-nz',
			'vac_date_to_day' => 'int-nz',
			'vac_date_to_month' => 'int-nz',
			'vac_date_to_year' => 'int-nz',
			'night_escorts' => 'arr-int'
		));

		$data = $form->getData();
		
		
		$_data = array('available_24_7' => $data['available_24_7'], 'night_escort' => $data['night_escort'],'times' => array(),'vac_date_from' => null,'vac_date_to' => null);
		
		for ( $i = 1; $i <= 7; $i++ ) {
			if ( isset($data['day_index'][$i]) && isset($data['time_from'][$i]) &&  isset($data['time_to'][$i])  ) {
				$night_escort =  in_array($i,$data['night_escorts']) ? 1 : 0;
				
				// sorry for stupid logic)) fast solution
				$time_arr_from = explode( ':',$data['time_from'][$i]);
				$time_arr_to = explode( ':',$data['time_to'][$i]);

				$data['time_from'][$i] = $time_arr_from[0];
				$data['time_to'][$i] = $time_arr_to[0];
				$data['time_from_m'][$i] = $time_arr_from[1];
				$data['time_to_m'][$i] = $time_arr_to[1];

				$_data['times'][] = array(
					'day_index' => $i,
					'time_from' => $data['time_from'][$i],
					'time_from_m' => $data['time_from_m'][$i],
					'time_to' => $data['time_to'][$i],
					'time_to_m' => $data['time_to_m'][$i],
					'night_escorts'=>$night_escort
				);
			}
		}
		if(isset($data['vac_date_from_day']) || isset($data['vac_date_from_month']) || isset($data['vac_date_from_year']) || isset($data['vac_date_to_day']) || isset($data['vac_date_to_month']) || isset($data['vac_date_to_year'])){

			$date_from = $data['vac_date_from_year']."-".$data['vac_date_from_month']."-".$data['vac_date_from_day'];
			$date_to = $data['vac_date_to_year']."-".$data['vac_date_to_month']."-".$data['vac_date_to_day'];

			if( isset($data['vac_date_from_day']) && isset($data['vac_date_from_month']) && isset($data['vac_date_from_year']) && isset($data['vac_date_to_day']) && isset($data['vac_date_to_month']) && isset($data['vac_date_to_year'])){

				if ( strtotime($date_from) >= strtotime($date_to) )
				{
					$validator->setError('vacation', Cubix_I18n::translate('sys_error_invalid_date_interval'));
				}
				else if ( strtotime($date_to) < strtotime(date('Y-m-d')) )
				{
					$validator->setError('vacation', Cubix_I18n::translate('sys_error_invalid_date_interval'));
				}
			}
			else{
				$validator->setError('vacation', Cubix_I18n::translate('sys_error_invalid_date_interval'));
			}
			$_data['vac_date_from'] = $date_from;
			$_data['vac_date_to'] = $date_to;
		}

		return $_data;
	}
	
	public function workingTimesAction()
	{
		$vacation = new Model_EscortItem(array('id' =>$this->escort->id  ));
		$this->view->escort_id = $this->escort->id;
        $this->view->action = 'working-times';
		if ( $this->_request->isPost() ) {
			$validator = new Cubix_Validator();

			$this->view->data = $data = $this->_validateWorkingTimes($validator);
			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];
                die(json_encode($status));
			}
			$vacation->updateVacation($data['vac_date_from'], $data['vac_date_to']);
			return $this->profile->update($data, 'working-times');
		}


			$vac = (array) $vacation->getVacation($this->escort->id);
			$data = $this->profile->getWorkingTimes();
			$data = array_merge($data,$vac[0]);
			$this->view->data = $data;
	}


    public function ajaxReturnFromVacationAction()
    {
        $this->view->layout()->disableLayout();

        $client = new Cubix_Api_XmlRpc_Client();

        if ( $this->user )
        {
            if ($this->_request->escort_id)
                $escort_id = $this->_request->escort_id;
            else
                $escort_id = $this->user->getEscort()->getId();
        }

        try {
            $result = $client->call('Escorts.removeVacation', array($escort_id));
            $m_escorts = new Model_Escorts();
            $m_escorts->removeVacation($escort_id);

            die(json_encode(array('success'=>'true')));

        }
        catch (Exception $e) {
            echo json_encode($client->getLastResponse()); die;
        }

    }

	protected function _validatePrices($validator)
	{
		$rates = $this->_getParam('rates');

		if ( ! is_array($rates) ) {
			$rates = array();
		}

		$data = array('rates' => array());

		//$currencies = array_keys($this->defines['currencies']);
		$currencies = array_keys(Model_Currencies::getAllAssoc());
		$units = array_keys($this->defines['time_unit_options']);

		foreach ( $rates as $availability => $_rates ) {
			if ( $availability == 'incall' ) { $availability = 1; $types = array('additional-hour', 'overnight', 'dinner-date', 'weekend'); }
			elseif ( $availability == 'outcall' ) { $availability = 2; $types = array('additional-hour', 'overnight', 'dinner-date', 'weekend'); }
			else continue;

			foreach ( $_rates as $rate ) {
				if ( get_magic_quotes_gpc() ) $rate = stripslashes($rate);
				$rate = @json_decode($rate);

				if ( is_object($rate) ) $rate = (array) $rate;
				if ( $rate === false || ! is_array($rate) ) {
					continue;
				}

				// Case when rate is standart and some data are not valid
				if ( ! isset($rate['type']) && ( ! isset($rate['time']) || ! isset($rate['unit']) || ! isset($rate['price']) || ! isset($rate['currency']) ) ) {
					continue;
				}
				// Case when rate is a type of custom and some data are not valid
				elseif ( isset($rate['type']) && ( ! isset($rate['price']) || ! isset($rate['currency']) || ! in_array($rate['type'], $types) ) ) {
					continue;
				}

				// If price is invalid
				$price = intval($rate['price']);
				if ( $price <= 0 ) continue;

				// If currency is invalid
				$currency = intval($rate['currency']);
				if ( ! in_array($currency, $currencies) ) {
					continue;
				}

				// If rate is custom validate data add only type, price and currency fields
				if ( ! isset($rate['type']) ) {
					$time = intval($rate['time']);
					if ( $time <= 0 ) continue;

					$unit = intval($rate['unit']);
					if ( ! in_array($unit, $units) ) continue;

					$data['rates'][] = array('availability' => $availability, 'time' => $time, 'time_unit' => $unit, 'price' => $price, 'currency_id' => $currency);
				}
				// Otherwize add also time and time unit
				else {
					$data['rates'][] = array('availability' => $availability, 'type' => $rate['type'], 'price' => $price, 'currency_id' => $currency);
				}
			}
		}
		
		return $data;
	}


	public function	pricesAction()
	{
		$this->_request->setParam('no_tidy', true);
        $this->view->action = 'prices';

		if ( $this->_request->isPost() ) {
			$validator = new Cubix_Validator();
			$data = $this->_validatePrices($validator);
			$this->view->data = array('rates' => $this->profile->reconstructRates($data['rates']));

			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];
				die(json_encode($status));
			}

			return $this->profile->update($data, 'prices');
		}

			$data = $this->profile->getPrices();
			$this->view->data = $data;

	}

	public function _validateContactInfo($validator)
	{
		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'phone_prefix' => '',
			'phone_number' => '',
			/*'phone_number_alt' => '',*/
			'phone_instr' => 'int-nz',
			'phone_instr_no_withheld' => 'int-nz',
			'phone_instr_other' => 'notags|special',
			'email' => '',
			'website' => 'notags|special',
			'club_name' => 'notags|special',
			'street' => 'notags|special',
			'street_no' => 'notags|special',
			'display_address' => 'int',
			'viber' => 'int',
			'whatsapp' => 'int',
			'telegram' => 'int',
			'ssignal' => 'int',
		));
		$data = $form->getData();
		$data['display_address'] = (bool) $data['display_address'];
        $data['contact_phone_parsed'] = null;
		
		$data['phone_country_id'] = null;
		$phone = null;

		if (! strlen($data['phone_prefix']) && ! strlen($data['phone_number']) && ! strlen($data['email']))
		{
			$validator->setError('phone_number', Cubix_I18n::translate('sys_error_phone_email_required'));
		}
		else
		{
			if (strlen($data['phone_prefix']) && ! strlen($data['phone_number']))
			{
				$validator->setError('phone_number', Cubix_I18n::translate('sys_error_phone_required'));
			}
			elseif (!strlen($data['phone_prefix']) && strlen($data['phone_number']))
			{
				$validator->setError('phone_prefix', Cubix_I18n::translate('sys_error_country_calling_code_required'));
			}
			elseif (strlen($data['phone_prefix']) && strlen($data['phone_number']))
			{
				if (preg_match("/^(\+|00)/", trim($data['phone_number'])))
				{
					$validator->setError('phone_number', Cubix_I18n::translate('sys_error_enter_phone_without_country_code'));
				}
				elseif (false === ($phone = $this->_parsePhoneNumber($data['phone_number'])) || ! preg_match("/^[0-9\s\+\-\(\)]+$/i", $data['phone_number']))
				{
					$validator->setError('phone_number', Cubix_I18n::translate('sys_error_invalid_phone_number'));
				}
				else
				{
					$data['phone_number'] = $phone;
				}
			}
		}
		
		/*if ( ! strlen($data['phone_prefix']) ) {
			$validator->setError('phone_prefix', Cubix_I18n::translate('sys_error_country_calling_code_required'));
		}
		if ( ! strlen($data['phone_number']) ) {
			$validator->setError('phone_number', Cubix_I18n::translate('sys_error_phone_required'));
		}
		elseif(preg_match("/^(\+|00)/", trim($data['phone_number'])) ) {
				$validator->setError('phone_number', Cubix_I18n::translate('sys_error_enter_phone_without_country_code'));
			}
		else if ( false === ($phone = $this->_parsePhoneNumber($data['phone_number'])) || ! preg_match("/^[0-9\s\+\-\(\)]+$/i", $data['phone_number'])) {
			$validator->setError('phone_number', Cubix_I18n::translate('sys_error_invalid_phone_number'));
		}
		else {
			$data['phone_number'] = $phone;

		}*/
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		if ($phone)
		{   
			list($country_id,$phone_prefix,$ndd_prefix) = explode('-',$data['phone_prefix']);
			$data['phone_country_id'] = intval($country_id);
			
			$data['contact_phone_parsed'] = preg_replace('/^'.$ndd_prefix.'/', '', $phone);
			$data['contact_phone_parsed'] = '00'.intval($phone_prefix).$data['contact_phone_parsed'];
			
			$agency_id = $this->user->isAgency() ? $this->agency->id : null;
			$escort_id = $this->escort->id ? $this->escort->id : null;
			
			$results = $client->call('Escorts.existsByPhone', array($data['contact_phone_parsed'], $escort_id, $agency_id));
			
			$resCount = is_array($results) ? count($results) : 0;
			if ( $resCount > 0 ) {
				$validator->setError('phone_number', Cubix_I18n::translate('sys_error_phone_already_exists'));
			}
		}
		
		$countyModel = new Model_Countries();

        $country_iso = strtoupper($countyModel->getPhoneCountryIsoById(intval($country_id)));
		$is_valid_phone = Cubix_PhoneNumber::isValid($data['phone_number'],$country_iso, null);
        if(!is_bool($is_valid_phone)){
        	$validator->setError('phone_number', Cubix_I18n::translate('sys_error_phone_not_right'));
        }

		$data['phone_exists'] = $data['contact_phone_parsed'];
		unset($data['phone_prefix']);
		if ( ! in_array($data['phone_instr'], array_keys($this->defines['phone_instructions'])) ) {
			$data['phone_instr'] = null;
		}

		if ( ! is_null($data['phone_instr_no_withheld']) ) {
			$data['phone_instr_no_withheld'] = 1;
		}

		if ( ! is_null($data['phone_instr_other']) && $this->_hasPhoneNumber($data['phone_instr_other']) ) {
			$validator->setError('phone_instr_other', Cubix_I18n::translate('sys_error_invalid_other'));
		}
		
		$blackListModel = new Model_BlacklistedWords();
		if($blackListModel->checkWords($data['phone_instr_other'], Model_BlacklistedWords::BL_TYPE_OTHER)) {
			$validator->setError('phone_instr_other','You can`t use word "'.$blackListModel->getWords() .'"');
		}
		
		if ( strlen($data['email']) && ! $this->_validateEmailAddress($data['email']) ) {
			$validator->setError('email', Cubix_I18n::translate('sys_error_invalid_email_address'));
		}

		if ( strlen($data['website']) && ! $this->_validateWebsiteUrl($data['website']) ) {
			$validator->setError('website', Cubix_I18n::translate('sys_error_invalid_url'));
		}
		
		$contact_data = $client->call('Escorts.getWebPhone', array($this->escort->id));
		
		if ( strlen($data['website'])){
			$old_website = $contact_data['website'];
			if ($old_website != $data['website']){
				$data['website_changed'] = 1;
			}
		}
		if ( strlen($data['contact_phone_parsed'])){
			$old_phone = $contact_data['contact_phone_parsed'];
			if ($old_phone != $data['contact_phone_parsed']){
				$data['phone_changed'] = 1;
				$data['old_phone'] = $old_phone;
			}
		}
		
		$data['phone_instr_other'] = $validator->urlCleaner($data['phone_instr_other'], Cubix_Application::getById()->host);
		$data['club_name'] = $validator->urlCleaner($data['club_name'], Cubix_Application::getById()->host);
		$data['street'] = $validator->urlCleaner($data['street'], Cubix_Application::getById()->host);
		$data['street_no'] = $validator->urlCleaner($data['street_no'], Cubix_Application::getById()->host);
		
		return $data;
	}

	public function contactInfoAction()
	{
		$countyModel = new Model_Countries();
		$this->view->phone_countries = $countyModel->getPhoneCountries();
        $this->view->action = 'contact-info';

		if ( $this->_request->isPost() ) {

			$validator = new Cubix_Validator();

			$data = $this->view->data = $this->_validateContactInfo($validator);
            $this->view->phone_prefix_id = $data[phone_country_id];
			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];
                die(json_encode($status));
			}

			$this->profile->setAvailableApps($data);
			return $this->profile->update($data, 'contact-info');
		}
		else {
			$data = $this->profile->getContactInfo();
			$apps = $this->profile->getAvailableApps($this->escort->id);
			$data = array_merge($apps, $data);
			$phone_prfixes = $countyModel->getPhonePrefixs();
			$this->view->phone_prefix_id = $data[phone_country_id];
			if($data[phone_number]){
				if(preg_match('/^(\+|00)/',trim($data[phone_number])))
				{
					$phone_prefix_id = NULL;
					$phone_number = preg_replace('/^(\+|00)/', '',trim($data[phone_number]));
					foreach($phone_prfixes as $prefix)
					{
						if(preg_match('/^('.$prefix->phone_prefix.')/',$phone_number))
						{
							$phone_prefix_id = $prefix->id;
							$data[phone_number] = preg_replace('/^('.$prefix->phone_prefix.')/', '',$phone_number);
							BREAK;
						}

					}
					$this->view->phone_prefix_id = $phone_prefix_id;
				}
			}
			$this->view->data = $data;
		}
	}

	public function videoAction(){
		$this->view->layout()->disableLayout();
		$this->_request->setPost(array(
		    'only_video' => true
		));
		return $this->_forward('gallery', 'profile', 'private');
	}

	public function galleryAction()
	{
	
        $this->view->layout()->disableLayout();

		$escort_id = $this->view->escort_id;

		$this->view->user = $this->user;
		
		if($this->_getParam('only_video')){
		   $this->view->only_video = $this->_getParam('only_video');
        }else{
        	$this->view->only_video = false;
        }

		if($this->_getParam('in_step')){
		    $this->view->inStep = 1;
        }

		$client = new Cubix_Api_XmlRpc_Client();
		$this->view->is_message = false;
		if ( $this->_getParam('show_success') ){
			$this->view->is_message = true;
		}

		if ( $this->user->isEscort() ) {
			$escort = $this->user->getEscort();
			$escort_id = $escort->id;

		}
		else {

			$escort_id = intval($this->_getParam('escort'));

			if ( 1 > $escort_id ){
			    $session = new Zend_Session_Namespace('escort_id');
                $escort_id = (int)$session->escort_id;

			    if(1 > $escort_id) die;
            }


			if ( ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
				die('Doesn\'t have escorts');
			}

			$model = new Model_Escorts();
			$this->escort = $escort = $model->getById($escort_id);
		}

        $this->view->videoConfig = $video_config = Zend_Registry::get('videos_config');


        $video = $this->videoModel->GetEscortVideo($this->escort->id,$video_config['VideoCount']);


        $this->view->video_photo = $video[0];
        $this->view->video = $video[1][0];
        $this->view->host = Cubix_Application::getById(Cubix_Application::getId())->host;

		

		
		$photos = $this->_loadPhotos();

		$photo_ids = array();
		foreach ( $photos as $photo ) {
			$photo_ids[] = intval($photo['id']);
		}

		$action = $this->_getParam('a');
		
		$photos_min_count = Cubix_Application::getById(Cubix_Application::getId())->min_photos;

		$this->view->is_private = $is_private = (bool) intval($this->_getParam('is_private'));


        if($action == 'finish-step'){
            $limit = 10;
            $images_count = $client->call('Escorts.getAllPhotosCount', array($escort_id));
            if( $images_count['count'] > 2){
                return  $this->_forward('finish-step');
            }else{
                return $this->view->errorPhotoCount = 'Please upload at least 3 public photos in order to save your profile. Thank you!';
            }
        }
        else if ( 'set_main' == $action || ! is_null($this->_getParam('set_main')) ) {
            $id = $this->_getParam('photo_id');
            if ( ! count($id) ) {
                return $this->view->actionError = Cubix_I18n::translate('sys_error_select_at_least_on_photo');
            }

            $rotate_type = Model_Escort_Photos::PHOTO_ROTATE_SELECTED;
            if(count($id) == 1){
                $rotate_type = Model_Escort_Photos::PHOTO_ROTATE_SELECTED_ONE;
            }

            $id = array($id[0]);

            $photo = new Model_Escort_PhotoItem(array('id' => $id ,'escort_id' => $escort_id));
            $photo->setRotatePics($id);
            $result = $photo->setMain();
            $client->call('Escorts.setPhotoRotateType', array($escort_id, $rotate_type));
            $escort->photo_rotate_type = $rotate_type;
            $this->_loadPhotos();
		}
		elseif ( 'delete' == $action || ! is_null($this->_getParam('delete')) ) {
			$ids = $this->_getParam('photo_id');

			if ( ! is_array($ids) || ! count($ids) ) {
				return $this->view->actionError = Cubix_I18n::translate('sys_error_select_at_least_on_photo');
			}

			foreach ( $ids as $id ) {
				if ( ! in_array($id, $photo_ids) ) {
					return $this->view->actionError = Cubix_I18n::translate('sys_error_invalid_id_photo');
				}
			}

			$photo = new Model_Escort_Photos();
			$result = $photo->remove($ids);

			$this->_loadPhotos();
		}
		elseif ( 'upload' == $action || ! is_null($this->_getParam('upload')) ) {
				$set_photo = false;
				$config = Zend_Registry::get('images_config');
				$new_photos = array();
				$upload_errors = array();

				

				foreach ( $_FILES as $i => $file )
				{
					try {

						if ( ! isset($file['name']) || ! strlen($file['name']) ) {
							continue;
						}
						else {
							$set_photo = true;
						}

						$img_ext = strtolower(@end(explode('.', $file[name])));
						if (!in_array( $img_ext , $config['allowedExts'])){
							throw new Exception(Cubix_I18n::translate('sys_error_upload_img_allowed_extensions', array('extensions' => implode(', ', $config['allowedExts']))), Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
						}

						// Save on remote storage
						$images = new Cubix_Images();
						$image = $images->save($file['tmp_name'], $escort->id, $escort->application_id, strtolower(@end(explode('.', $file['name']))));

						$image = new Cubix_Images_Entry($image);
						$image->setSize('sthumb');
						$image->setCatalogId($escort->id);
						$image_url = $images->getUrl($image);

						$image_size = getimagesize($file['tmp_name']);
						$is_portrait = 0;
						if ( $image_size ) {
							if ( $image_size[0] < $image_size[1] ) {
								$is_portrait = 1;
							}
						}
						
						$photo_arr = array(
							'escort_id' => $escort->id,
							'hash' => $image->getHash(),
							'ext' => $image->getExt(),
							'type' => $is_private ? ESCORT_PHOTO_TYPE_PRIVATE : ESCORT_PHOTO_TYPE_HARD,
							'is_portrait' => $is_portrait,
							'width' => $image_size[0],
							'height' => $image_size[1],
							'creation_date' => date('Y-m-d H:i:s', time())
						);

						if ( $client->call('Escorts.isPhotoAutoApproval', array($escort_id)) ) {
							$photo_arr['is_approved'] = 1;
						}

						$photo = new Model_Escort_PhotoItem($photo_arr);

						$model = new Model_Escort_Photos();
						$photo = $model->save($photo);

						$new_photos[] = $photo;
					} catch ( Exception $e ) {
						$upload_errors[$i] = $file['name'] . ' (' . $e->getMessage() . ')';
					}
					
			}

			if ( ! $set_photo ) {
				$this->view->uploadError = Cubix_I18n::translate('sys_error_select_photo');
			}

			$this->view->newPhotos = $new_photos;
			$this->view->uploadErrors = $upload_errors;
		}
		elseif ( 'set-adj' == $action ) {
			$photo_id = intval($this->_getParam('photo_id'));

			if ( ! in_array($photo_id, $photo_ids) ) {
				die(json_encode(array('error' => 'An error occured')));
			}

			$photo = new Model_Escort_PhotoItem(array(
				'id' => $photo_id
			));

			try {
				$hash = $photo->getHash();
				$result = array(
					'x' => intval($this->_getParam('x')),
					'y' => intval($this->_getParam('y')),
					'px' => floatval($this->_getParam('px')),
					'py' => floatval($this->_getParam('py'))
				);
				$photo->setCropArgs($result);

				// Crop All images
				$size_map = array(
					'backend_thumb' => array('width' => 150, 'height' => 205),
					'medium' => array('width' => 225, 'height' => 300),
					'thumb' => array('width' => 150, 'height' => 200),
					'thumb_cropper' => array('width' => 150, 'height' => 200),
					'nlthumb' => array('width' => 120, 'height' => 160),
					'sthumb' => array('width' => 76, 'height' => 103),
					'lvthumb' => array('width' => 75, 'height' => 100),
					'agency_p100' => array('width' => 90, 'height' => 120),
					't100p' => array('width' => 117, 'height' => 97)
				);
				$conf = Zend_Registry::get('images_config');

				get_headers($conf['remote']['url'] . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash);
				// echo $conf['remote']['url'] . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash;

				$catalog = $escort_id;
				$a = array();
				if ( is_numeric($catalog) ) {
					$parts = array();

					if ( strlen($catalog) > 2 ) {
						$parts[] = substr($catalog, 0, 2);
						$parts[] = substr($catalog, 2);
					}
					else {
						$parts[] = '_';
						$parts[] = $catalog;
					}
				}
				else if ( preg_match('#^([0-9]+)/(.+)$#', $catalog, $a) ) {
					array_shift($a);
					$catalog = $a[0];

					$parts = array();

					if ( strlen($catalog) > 2 ) {
						$parts[] = substr($catalog, 0, 2);
						$parts[] = substr($catalog, 2);
					}
					else {
						$parts[] = '_';
						$parts[] = $catalog;
					}

					$parts[] = $a[1];
				}

				$catalog = implode('/', $parts);

				foreach($size_map as $size => $sm) {
					get_headers($conf['remote']['url'] . "/" . Cubix_Application::getById()->host . "/" . $catalog . "/" . $hash . "_" . $size . ".jpg?args=" . $result['x'] . ":" . $result['y']);
				}
			}
			catch ( Exception $e ) {
				die(json_encode(array('error' => 'An error occured')));
			}

			die(json_encode(array('success' => true)));
		}
        elseif( 'set-rotate' == $action){
            $photo_id = intval($this->_getParam('photo_id'));
            $degree = intval($this->_getParam('degree'));
            $degree = $degree == 90 ? 90 : -90;

            if ( !in_array($photo_id, $photo_ids) ) {
                die(json_encode(array('error' => 'An error occured')));
            }

            $photo = new Model_Escort_PhotoItem(array(
                'id' => $photo_id
            ));

            try {
                $photo_info = $photo->getHashExt();
                $hash = $photo_info['hash'];
                $ext = $photo_info['ext'];

                $conf = Zend_Registry::get('images_config');
//                $remoteHost= str_replace('https://','http://', $conf['remote']['url']);
                $remoteHost= $conf['remote']['url'];
                $rotateUrl = $remoteHost . "/get_image.php?a=rotate&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash . "&ext=". $ext . "&d=". $degree;
                file_get_contents($rotateUrl);
                $cacheUrl = $remoteHost . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash;
                file_get_contents($cacheUrl);
                $result = array(
                    'x' => 0,
                    'y' => 0,
                    'px' => 0,
                    'py' => 0
                );

                $photo->setCropArgs($result);

                $catalog = $escort_id;
                $a = array();
                if ( is_numeric($catalog) ) {
                    $parts = array();

                    if ( strlen($catalog) > 2 ) {
                        $parts[] = substr($catalog, 0, 2);
                        $parts[] = substr($catalog, 2);
                    }
                    else {
                        $parts[] = '_';
                        $parts[] = $catalog;
                    }
                }
                else if ( preg_match('#^([0-9]+)/(.+)$#', $catalog, $a) ) {
                    array_shift($a);
                    $catalog = $a[0];

                    $parts = array();

                    if ( strlen($catalog) > 2 ) {
                        $parts[] = substr($catalog, 0, 2);
                        $parts[] = substr($catalog, 2);
                    }
                    else {
                        $parts[] = '_';
                        $parts[] = $catalog;
                    }

                    $parts[] = $a[1];
                }

                $catalog = implode('/', $parts);
                $imgUrl = $remoteHost . "/" . Cubix_Application::getById()->host . "/" . $catalog . "/" . $hash . "_thumb_cropper.".$ext;
                get_headers($imgUrl);
            }
            catch ( Exception $e ) {
                die(json_encode(array('error' => 'An error occured')));
            }

            die(json_encode(array('success' => true, "photo"=>$photo_id)));
        }
		elseif ( $action == 'make_private' || $action == 'make_public' ) {
			if ( $action == 'make_private' ) {
				$type = 'private';
			} elseif ( $action == "make_public") {
				$type = 'public';
			} else {
				die;
			}

			$ids = $this->_getParam('photo_id');

			if ( ! is_array($ids) || ! count($ids) ) {
				return $this->view->actionError = Cubix_I18n::translate('sys_error_select_at_least_on_photo');
			}

			foreach ( $ids as $id ) {
				if ( ! in_array($id, $photo_ids) ) {
					return $this->view->actionError = Cubix_I18n::translate('sys_error_invalid_id_photo');
				}
			}

			foreach ( $ids as $id ) {
				$photo = new Model_Escort_PhotoItem(array('id' => $id));
				if($type == 'private'){
					if ( $photo->isMain() ) {
						return $this->view->actionError = Cubix_I18n::translate('sys_error_cant_private_main_photo');
					}
					$photo->make(ESCORT_PHOTO_TYPE_PRIVATE);
				}
				else{
					$photo->make(ESCORT_PHOTO_TYPE_HARD);
				}
			}

			$this->_loadPhotos();
		}
		elseif ( 'sort' == $action ) {
			$ids = $this->_getParam('photo_id');

			if ( ! is_array($ids) || ! count($ids) ) {
				die(Cubix_I18n::translate('sys_error_select_at_least_on_photo'));
			}

			foreach ( $ids as $id ) {
				if ( ! in_array($id, $photo_ids) ) {
					die(Cubix_I18n::translate('sys_error_invalid_id_photo'));
				}
			}

			$model = new Model_Escort_Photos();
			$model->reorder($ids);

			die;
		}

		$this->view->escort = $escort;
        $this->view->action = ( $this->view->only_video ) ? 'video' : 'gallery';
	}

	private function _loadPhotos()
	{
		$photos = Cubix_Api::getInstance()->call('getEscortPhotosByOrdering', array($this->escort->id));
		return $this->view->photos = $photos;

		/*$public_photos = Cubix_Api::getInstance()->call('getEscortPhotosList', array($this->escort->id, false));
		$nil = null;
		$private_photos = Cubix_Api::getInstance()->call('getEscortPhotosList', array($this->escort->id, true));

		return $this->view->photos = array_merge($public_photos, $private_photos);*/
	}



	protected function _parsePhoneNumber($phone)
	{
		$phone = preg_replace('/[^0-9]+/', '', $phone);

		if ( ! strlen($phone) || ! is_numeric($phone) ) {
			return false;
		}

		if ( '00' != substr($phone, 0, 2) ) {
			if ( ! is_null($escort_id) ) {
				$iso = $this->profile->country_id;
				if ( $iso && isset($this->defines['dial_codes'][$iso]) ) {
					$phone = '00' . $this->defines['dial_codes'][$iso] . $phone;
				}
			}
		}

		return $phone;
	}

	protected function _hasPhoneNumber($string)
	{
		$string = preg_replace('/[^0-9]+/', '', $string);

		if ( strlen($string) > 8 ) {
			return true;
		}

		return false;
	}

	protected function _validateEmailAddress($email)
	{
		return Cubix_Validator::validateEmailAddress($email);
	}

	protected function _validateWebsiteUrl($url)
	{
		//return (bool) preg_match('/^((http|https):\/\/)?(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)(:(\d+))?\/?/i', $url);
		if (strtolower(substr($url, 0, 7)) != 'http://' && strtolower(substr($url, 0, 8)) != 'https://' && strtolower(substr($url, 0, 4)) != 'www.')
			return false;
		else
			return true;
	}

	public function finishAction()
	{
		$data = array();
		if ( $this->user->isAgency() ) {
			$data['agency_id'] = $this->agency->getId();
		}

		$data['user_id'] = $this->user->getId();
		
		// Send the data from session to API, a new revision of the profile will be created!
		$res = $this->profile->flush($data);

		if ( ! $res ) {
			$this->_redirect($this->view->getLink('private'));
		}

		$this->view->escort_id = $res;
	}

	public function ajaxChangeCityAction()
	{
		$this->view->layout()->disableLayout();


		$city_id = intval($this->_getParam('city_id'));
		$data = $this->profile->getWorkingCities();

		$model = new Cubix_Geography_Countries();
		if ( ! $model->hasCity($data['country_id'], $city_id) ) {
			die('Invalid city id');
		}

		$data['city_id'] = $city_id;
		foreach ( $data['cities'] as $i => $city ) {
			if ( $city_id == $city['city_id'] ) {
				$data['cities'][$i]['city_id'] = $city_id;
			}
		}



		$result = $this->profile->update($data, 'working-cities');

		$response = array('status' => 'error');

		if ( $result ) {
			$response['status'] = 'ok';
			$response['city'] = Cubix_Geography_Cities::getTitleById($city_id);
		}

		die(json_encode($response));
	}

	public function ajaxChangeZipAction()
	{
		$zip = $this->_getParam('zip');
		$data = $this->profile->getWorkingCities();

		if ( ! $data['incall_type'] ) {
			die('There is no need for zip code');
		}

		$validator = new Cubix_Validator();

		if ( ! $validator->validateZipCode($zip, true) ) {
			die(json_encode(array('status' => 'error')));
		}

		$data['zip'] = $zip;


		$result = $this->profile->update($data, 'working-cities');

		$response = array('status' => 'error');

		if ( $result ) {
			$response['status'] = 'ok';
			$response['zip'] = $zip;
		}

		die(json_encode($response));
	}


	public function completeProfileAction(){}

    public function biographyStepAction()
    {

        $countyModel = new Model_Countries();

        $this->view->countries = $countyModel->getCountries();

        if ($this->_posted) {
            $validator = new Cubix_Validator();

            $this->view->data = $data = $this->_validateBiography($validator);

            if (!$validator->isValid()) {
                $status = $validator->getStatus();

                $this->view->errors = $status['msgs'];
                die(json_encode($status));
            }

            return $this->profile->update($data, 'biography');
        }

        $this->view->data = $this->profile->getBiography();

    }


    public function aboutMeStepAction()
    {
        $validator = new Cubix_Validator();
        if ($this->_posted) {

            $blackListModel = new Model_BlacklistedWords();
            $dataAboutMe = $this->_validateAboutMe($validator);
            $dataLanguages = $this->_validateLanguages($validator);

            $this->view->data = $data = array_merge($dataAboutMe,$dataLanguages);

            $data['about_has_bl'] = 0;
            if (!$validator->isValid()) {

                $status = $validator->getStatus();

                $this->view->errors = $status['msgs'];

                die(json_encode($status));

            }


                   $this->profile->update($dataAboutMe, 'about-me');
            return $this->profile->update($dataLanguages, 'languages');
        }

        $this->view->data = $this->profile->getAboutMe();
    }

    public function workingCitiesStepAction()
    {


        $data = $this->profile->getWorkingCities();

        $countries = new Cubix_Geography_Countries();
        $cities = new Cubix_Geography_Cities();
        $country_id = Cubix_Application::getById()->country_id;

        $this->view->countries = $countries->ajaxGetAll(false);
        $this->view->cities = $cities->ajaxGetAll(null, $country_id);

        if (isset($data['city_id']) && $data['city_id']) {
            $city = $cities->get($data['city_id']);
            $country_id = $city->country_id;
        }



        if ($this->_posted) {
            $validator = new Cubix_Validator();

            $this->view->data = $data = $this->_validateWorkingCities($validator);


            $param_cities = array();
            if (count($data['cities'])) {
                foreach ($data['cities'] as $city) {
                    $param_cities[] = $city['city_id'];
                }
            }

            if (!$validator->isValid()) {
                $status = $validator->getStatus();

                $this->view->errors = $status['msgs'];

                die(json_encode($status));
            }

            $found = false;

            foreach ($data['cities'] as $city){
                if($city['city_id'] == $data['city_id']){
                    $found = true;
                }
            }

            if(!$found){
                $data['cities'][] = ['city_id' => $data['city_id']];
            }


            return $this->profile->update($data, 'working-cities');
        }

        $param_cities = array();
        if (count($data['cities'])) {
            foreach ($data['cities'] as $city) {
                $param_cities[] = $city['city_id'];
            }
        }

        $data['country_id'] = $country_id;


        $this->view->data = $data;

    }

    public function servicesStepAction(){
        if ( $this->_posted ) {
            $validator = new Cubix_Validator();

            $this->view->data = $data = $this->_validateServices($validator);

            if ( ! $validator->isValid() ) {
                $status = $validator->getStatus();

                $this->view->errors = $status['msgs'];

                die(json_encode($status));
            }

            return $this->profile->update($data, 'services');
        }


        $data = $this->profile->getServices();

        $this->view->data = $data;

    }

    public function workingTimesStepAction(){
        $this->view->escort_id = $this->escort->id;
        if ( $this->_posted ) {
            $validator = new Cubix_Validator();

            $this->view->data = $data = $this->_validateWorkingTimes($validator);
            if ( ! $validator->isValid() ) {
                $status = $validator->getStatus();

                $this->view->errors = $status['msgs'];

                die(json_encode($status));
            }
            return $this->profile->update($data, 'working-times');
        }


        $data = $this->profile->getWorkingTimes();
        $this->view->data = $data;

    }



    public function contactInfoStepAction(){
        $countyModel = new Model_Countries();
        $this->view->phone_countries = $countyModel->getPhoneCountries();
        if($this->user->isAgency()){

            $client = new Cubix_Api_XmlRpc_Client();

            $this->view->agency = $client->call('Agencies.getByUserId', array($this->user->id));

        }
        if ( $this->_posted ) {

            $validator = new Cubix_Validator();

            $data = $this->view->data = $this->_validateContactInfo($validator);
            $this->view->phone_prefix_id = $data[phone_country_id];
            if ( ! $validator->isValid() ) {
                $status = $validator->getStatus();

               $this->view->errors = $status['msgs'];

                die(json_encode($status));
            }
            $data['id'] = $this->escort->id;
            $this->profile->setAvailableApps($data);
            $this->profile->update($data, 'contact-info');

            $data = array();
            if ( $this->user->isAgency() ) {
                $data['agency_id'] = $this->agency->getId();
            }

            $data['user_id'] = $this->user->getId();

            // Send the data from session to API, a new revision of the profile will be created!
            $escort_id = $this->profile->flush($data);

            if($this->user->isAgency()){
                $session = new Zend_Session_Namespace('escort_id');
                $session->escort_id = $escort_id;
            }

            return $escort_id;

        }

            $data = $this->profile->getContactInfo();
            $apps = $this->profile->getAvailableApps();
            $data = array_merge($apps, $data);
            $phone_prfixes = $countyModel->getPhonePrefixs();
            $this->view->phone_prefix_id = $data[phone_country_id];
            if($data[phone_number]){
                if(preg_match('/^(\+|00)/',trim($data[phone_number])))
                {
                    $phone_prefix_id = NULL;
                    $phone_number = preg_replace('/^(\+|00)/', '',trim($data[phone_number]));
                    foreach($phone_prfixes as $prefix)
                    {
                        if(preg_match('/^('.$prefix->phone_prefix.')/',$phone_number))
                        {
                            $phone_prefix_id = $prefix->id;
                            $data[phone_number] = preg_replace('/^('.$prefix->phone_prefix.')/', '',$phone_number);
                            BREAK;
                        }

                    }
                    $this->view->phone_prefix_id = $phone_prefix_id;
                }
            }
            $this->view->data = $data;

    }


    public function finishStepAction(){
        $this->view->layout()->disableLayout();
        $this->view->id = $this->_getParam('escort');
        $this->_helper->viewRenderer->setScriptAction('finish-step-first-time');
    }

    public static function getModifiedArray($column,$array){
        $new_array = array();
        foreach ($array as $key => $value){
            $new_array[] = $value[$column];
        }

        return $new_array;
    }

    public function gotdAction()
    {
        $this->view->layout()->disableLayout();
        $client = Cubix_Api_XmlRpc_Client::getInstance();
        $this->view->booked_days = $booked_days = $client->call('Billing.getGotdBookedDays');
        $this->view->selectedPayment = 'mmgbill';

        $cities = new Model_Cities();

        $cities = $cities->getByCountries(array(215));

        $grouped_cities = array();
        foreach ( $cities as $city ) {
            if ( ! isset($grouped_cities[$city->country_title]) ) {
                $grouped_cities[$city->country_title] = array();
            }

            $grouped_cities[$city->country_title][] = $city;
        }
        $this->view->cities = $grouped_cities;

        $is_agency = false;
        if ( $this->user->isAgency() ) {
            $is_agency = true;
            $escorts = $client->call('Billing.getAgencyEscortsForGotdEG', array($this->agency->id));
            $escortsGendersArray = array();
            $has_girl_escort = false;
            $has_boy_escort = false;
            foreach ($escorts as $escort) {
                $escortsGendersArray[] = $escort['gender'];
            }
            if (in_array(1,$escortsGendersArray))
                $has_girl_escort = true;
            if (in_array(2,$escortsGendersArray))
                $has_boy_escort = true;

            $this->view->has_girl_escort = $has_girl_escort;
            $this->view->has_boy_escort = $has_boy_escort;

            if (!$has_boy_escort) {
                $this->view->escorts = $escorts;
            } elseif ($has_boy_escort) {
                $this->view->escorts = $escorts;
            }
        } else {
            $this->view->escort_packages = $escort_packages = $client->call('Billing.getEscortPackagesForGotdEG', array($this->escort->id, Cubix_Application::getId()));
            $escort_package = $escort_packages[0];
            /*if (!empty($escort_package)){ //EGUK-340
                $expDateHour = date("G", $escort_package['expiration_date']);
                if ($expDateHour <= 5 ){
                    $escort_package['expiration_date'] = strtotime("-1 day", $escort_package['expiration_date']);
                }
            }*/
            $this->view->escort_package = $escort_package;
            $this->view->escort_package['modified_name'] = $escort_package['package_name'] .'('. date('M d', $escort_package['date_activated']) .' - '. date('M d', $escort_package['expiration_date']) .')';;
        }

        // current date + 1 month, temporary solution for available days
        $this->view->gotd_max_available_day = $gotd_max_available_day = strtotime('+1 month');

        $this->view->is_agency = $is_agency;

        $DEFINITIONS = Zend_Registry::get('defines');
        $this->view->free_packages = $DEFINITIONS['free_packages'];
        $this->view->free_package_hide = $is_agency || in_array($this->view->escort_package['package_id'], $this->view->free_packages);

        $this->view->region_relations = $region_relations = array();

        $this->view->gotd_product = $product = $client->call('Billing.getProduct', array(18));
        $this->view->user_id = $this->user->id;


        if ( $this->_request->isPost() ) {
            $hash  = base_convert(time(), 10, 36);
            $errors = array();
            $data = array(
                'city_id'	=> $this->_request->city_id,
                'escort_id' => $this->_request->escort_id,
                'days'		=> $this->_request->days,
                'package'	=> $this->_request->package,
                'package_id'=> $this->_request->package_id,
            );

//            if ( ! $is_agency && count($escort_packages) == 1 ) {
                $data['package'] = 0;
//            }

            if ( ! $data['city_id'] ) {
                $errors['city_id'] = Cubix_I18n::translate('city_required');
            } else {
                $city_also_check[] = $data['city_id'];
                if ( isset($region_relations[$data['city_id']]) ) {
                    $city_also_check = $region_relations[$data['city_id']];
                }
            }
            $this->view->city_also_check = $city_also_check;

            if ($is_agency) {
                if ( ! $data['escort_id'] ) {
                    $errors['escort_id'] = Cubix_I18n::translate('escort_required');
                }
            } else {
                $data['escort_id'] = $this->escort->id;
            }

            if ( !strlen($data['package'])) {
                $errors['package'] = Cubix_I18n::translate('select_package');
            }

            $selected_days = array();
            if ( $data['days'] ) {
                $selected_days = explode(';', $data['days']);
            }
            if ( ! count($selected_days) ) {
                $errors['days'] = Cubix_I18n::translate('date_required');
            } else {
                foreach($selected_days as $k => $day) {
                    $day = explode(',', $day);
                    //set to 5 to avoid daylight saving
                    $selected_days[$k] = mktime(5,0,0, $day[1], $day[2], $day[0]);
                }
            }

            $product['price'] = 0 ;
            $gotd_price = GIRL_OF_THE_DAY_WITHOUT_PACKAGE_PRICE;
            $escort_package = $is_agency
                ? $escorts[$data['escort_id']]['packages'][$data['package']]
                : $escort_packages[$data['package']];

            foreach ($selected_days as $day) {
                //Checking date to be not booked
                if ($data['city_id']) {
                    foreach ($city_also_check as $ct) {
                        foreach ($booked_days[$ct] as $dt) {
                            if (date('Y-m-d', $day) == date('Y-m-d', strtotime($dt['date']))) {
                                $errors['days'] = Cubix_I18n::translate('day_already_booked');
                                break;
                            }
                        }
                    }
                }

                //Escort could have only one package per day
                if ($data['city_id']) {
                    $escort_gotds = $client->call('Billing.getEscortGotdBookedDays', array($data['escort_id']));

                    foreach ($escort_gotds as $gotd) {
                        if (date('Y-m-d', $day) == date('Y-m-d', strtotime($gotd['date']))) {
                            if ($this->escort['gender'] == 2) {
                                $errors['days'] = Cubix_I18n::translate('only_one_botd_per_day');
                            } else {
                                $errors['days'] = Cubix_I18n::translate('only_one_gotd_per_day');
                            }
                        }
                    }

                    foreach ($booked_days as $city_id => $dt) {
                        foreach ($dt as $d) {
                            if (date('Y-m-d', $day) == date('Y-m-d', strtotime($d['date'])) && $data['escort_id'] == $d['escort_id']) {
                                if ($this->escort['gender'] == 2) {
                                    $errors['days'] = Cubix_I18n::translate('only_one__botd_per_day');
                                } else {
                                    $errors['days'] = Cubix_I18n::translate('only_one_gotd_per_day');
                                }
                            }
                        }
                    }
                }

                //Checking date to be in package active range
                if ($data['escort_id']) {
                    if ($is_agency) {
                        $escort_package['expiration_date'] += 5 * 60 * 60;
                        if ($day < time() || $day > $gotd_max_available_day) {
                            $errors['days'] = Cubix_I18n::translate('date_invalid');
                        }
                    } else {
                        //If not in range of active and pending packages
                        //add 5 hours to avoid daylight saving
                        $escort_package['expiration_date'] += 5 * 60 * 60;
                        if ($day < time() || $day > $gotd_max_available_day) {
                            $errors['days'] = Cubix_I18n::translate('date_invalid');
                        }
                    }
                }

                if ($this->escort->gender == 1 || ($is_agency && !$has_boy_escort)) {
                    $gotd_price = !in_array($escort_package['package_id'], $DEFINITIONS['free_packages']) && $day >= $escort_package['date_activated'] && $day <= $escort_package['expiration_date']
                        ? GIRL_OF_THE_DAY_WITH_PACKAGE_PRICE
                        : GIRL_OF_THE_DAY_WITHOUT_PACKAGE_PRICE;
                } elseif ( $this->escort->gender == 2 || ($is_agency && $has_boy_escort && !$has_girl_escort)) {
                    $gotd_price = !in_array($escort_package['package_id'], $DEFINITIONS['free_packages']) && $day >= $escort_package['date_activated'] && $day <= $escort_package['expiration_date']
                        ? BOY_OF_THE_DAY_WITH_PACKAGE_PRICE
                        : BOY_OF_THE_DAY_WITHOUT_PACKAGE_PRICE;
                }

                $product['price'] += $gotd_price;
            }

            $this->view->gotd_product = $product;

            $this->view->data = $data;
            $this->view->selected_days_count = $selected_days_count = count($selected_days);


            if ( count($errors) ) {
                $this->view->gotd_errors = $errors;

                if($this->getRequest()->isXmlHttpRequest()) {
                    exit(json_encode([
                        'status' => 'error',
                        'msgs' => $errors
                    ]));
                }

            } else {
                $book_id = $client->call('Billing.bookGotd', array($data['escort_id'], $data['city_id'], $selected_days));

                if ( $book_id ) {
                    $amount = $product['price'];
                    if($this->_request->payment_gateway == "mmgbill"){

						$book_id = str_replace(':', 'x', $book_id);
                        $mmgBill = new Model_MmgBillAPIV2();
						
                        $hosted_url = $mmgBill->getHostedPageUrl(
                            $amount,
                            'GZ' . $book_id,
                            'http://www.escortguide.co.uk' . $this->view->getLink('ob-mmg-postback')
                        );

                    }elseif ($this->_request->payment_gateway == 'paysafe') {
                        $first_name = trim(preg_replace('#[^a-zA-Z]#', ' ', $this->user->username));
                        $last_name = trim(preg_replace('#[^a-zA-Z]#', ' ', $this->user->chat_info['nickName']));

                        if( empty($first_name)){
                            $first_name = 'NAME';
                        }

                        if(empty($last_name)){
                            $last_name = 'LASTNAME';
                        }

                        $reference = 'gotd-' . $book_id;

                        $customer = new Cubix_2000charge_Model_Customer();
                        $customer->setEmail($this->user->email);
                        $customer->setCountry("GB");
                        $customer->setFirstName($first_name);
                        $customer->setLastName($last_name);

                        $payment = new Cubix_2000charge_Model_Payment();
                        $payment->setPaymentOption("paysafe");
                        $payment->setHolder($first_name.' '.$last_name);

                        $transaction = new Cubix_2000charge_Model_Transaction();
                        $transaction->setCustomer($customer);
                        $transaction->setPayment($payment);
                        $transaction->setAmount($amount * 100);
                        $transaction->setCurrency("GBP");
                        $transaction->setIPAddress(Cubix_Geoip::getIP());
                        $transaction->setMerchantTransactionId($reference);

                        $host = 'https://' . $_SERVER['SERVER_NAME'];
                        $redirectUrls = new Cubix_2000charge_Model_RedirectUrls();
                        $redirectUrls->setReturnUrl($host . $this->view->getLink('billing-paysafe-success'));
                        $redirectUrls->setCancelUrl($host . $this->view->getLink('billing-paysafe-failure'));
                        $transaction->setRedirectUrls($redirectUrls);
                        $client = new Cubix_Api_XmlRpc_Client();
                        $res = Cubix_2000charge_Transaction::post($transaction);
                        $client->call('OnlineBillingV2.storeToken', array($res->id, $this->user->id, '2000charge'));
                        $hosted_url = $res->redirectUrl;

                    }
                }
                die(json_encode(array('status' => 'success', 'url' =>  $hosted_url)));
            }
        }
    }

    public function gotdSuccessAction()
    {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setScriptAction('gotd-responses');
        $this->view->key = 'gotd_success_message';
    }

    public function gotdFailureAction()
    {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setScriptAction('gotd-responses');
        $this->view->key = 'gotd_failure_message';
    }
}


