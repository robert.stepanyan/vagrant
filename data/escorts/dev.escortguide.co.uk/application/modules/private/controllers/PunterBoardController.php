<?php

class Private_PunterBoardController extends Zend_Controller_Action
{
	private $_model;
	private $user;
		
	public function init() 
	{
		
		$this->user = $this->view->user = Model_Users::getCurrent();
		$this->view->layout()->disableLayout();
		
		if ( ! $this->user ) {
            if ( is_null($this->_getParam('ajax')) ) {
                $this->_redirect($this->view->getLink('signin'));
                return;
            }else{
                die(json_encode(
                    array(
                        'status' => 'error',
                        'redirect_to_signin' => $this->view->getLink('signin')
                    )
                )
                );
            }
		}
		else if ($this->user->user_type != 'member')
		{
			$this->_redirect($this->view->getLink('private-v2'));
			return;
		}
		
		 $this->_model = new Model_PunterBoard();
	}
	
	public function indexAction() 
	{
		// var_dump($this->_model->getAll()['user_punters']);die;
		$page = 1;
		if(isset($this->_request->page)){
			$page = $this->_request->page;
		}
		$this->view->page = $page;
		$this->view->per_page = 10;
		$punters = $this->_model->getAll($page, 5);

		$this->view->count = $punters['count'];
		$this->view->user_punters = $punters['user_punters'];
		$this->view->other_punters = $punters['other_punters'];
	}

	
	public function removeAction() 
	{
		/*
		if ($this->_request->isPost())
		{
			$id = intval($this->_request->id);
			
			if (!$id) die;
			
			$this->_model->remove($id, $this->user->id);
		}
		
		die;*/
	}
	
	public function addAction() 
	{

	}
}

?>
