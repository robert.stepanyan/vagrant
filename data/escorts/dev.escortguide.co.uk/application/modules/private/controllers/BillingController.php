<?php

class Private_BillingController extends Zend_Controller_Action {
	

	const PACKAGE_STATUS_PENDING  	= 1;
	const PACKAGE_STATUS_ACTIVE   	= 2;
	const PACKAGE_STATUS_EXPIRED  	= 3;
	const PACKAGE_STATUS_CANCELLED 	= 4;
	const PACKAGE_STATUS_UPGRADED 	= 5;
	const PACKAGE_STATUS_SUSPENDED 	= 6;
	const GENDER = 1;

	private static $packages = array(101, 102, 103, 104);
	private static $verifiedPackages = array(108,109,110,111);
	private static $packages_monthly = array(107);
	protected $_session;

	public function init() {

		$this->_session = new Zend_Session_Namespace('online_billing');
   		$this->_session->setExpirationSeconds(60 * 60);

		   $this->view->layout()->disableLayout();
		$this->view->user = $this->user = Model_Users::getCurrent();

    	$action = $this->_request->getActionName();
		$anonym_actions = array('success', 'failure', 'paysafe-success', 'paysafe-failure', 'payment' );

		if (!$this->user && !in_array($action, $anonym_actions)) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}

		if (!in_array($action, $anonym_actions) && !$this->user->isAgency() && !$this->user->isEscort()) {
			$this->_redirect($this->view->getLink('private'));
		}

		$this->client = new Cubix_Api_XmlRpc_Client();
	}

	public function indexAction() 
	{
		if ($this->user->isEscort()) {
			$escorts_model = new Model_Escorts();

			$payment_methodes_model = new Model_PaymentMethodes();
			$activePaymentmethodes = $payment_methodes_model->getActivePaymentMethodes(Cubix_Application::getId());

			$escort_id = $this->user->escort_data['escort_id'];

			$escort = $escorts_model->getforselfcheckout($escort_id, null, true);

			$is_pseudo_escort = $this->client->call(
				'OnlineBillingV2.isPseudoEscort', array($escort_id)
			);

            $isVerified = $this->client->call(
                'OnlineBillingV2.isVerified', array($escort_id)
            );

			$user_type = $is_pseudo_escort ? USER_TYPE_AGENCY : USER_TYPE_SINGLE_GIRL;
			$escort->is_pseudo_escort = $is_pseudo_escort;
			$escort->is_verified = $isVerified;

			$escort_packages = $this->client->call(
				'OnlineBillingV2.checkIfHasPaidPackage', array($escort_id)
			);

			$is_package_purchase_allowed = true;

			if (count($escort_packages)) {
				list($ongoing_package, $pending_package) = $escort_packages;

				if ($ongoing_package && ($ongoing_package['status'] == self::PACKAGE_STATUS_PENDING)) {
					$is_package_purchase_allowed = false;
				}
			}

			if ($escort->escort_status != ESCORT_STATUS_ACTIVE) {
				$is_package_purchase_allowed = false;
			}
			
			$available_packages = $this->client->call(
				'OnlineBillingV2.getPackagesList',
				array($user_type, $escort->gender, $is_pseudo_escort, self::$packages)
			);
//			if ($isVerified){
//                $available_packages = $this->client->call(
//                    'OnlineBillingV2.getPackagesList',
//                    array($user_type, $escort->gender, $is_pseudo_escort, self::$verifiedPackages, false)
//                );
//            }

			die(json_encode(array(
				'user_type' => 'escort',
				'escort_status' => $escort->escort_status,
				'escort' => $escort,
				'escort_packages' => $escort_packages,
				'is_package_purchase_allowed' => $is_package_purchase_allowed,
				'available_packages' => $available_packages,
				'activePaymentmethodes' => $activePaymentmethodes,
				'verified' => $isVerified
			)));
			
		} elseif ($this->user->isAgency()) {
			$user_type = USER_TYPE_AGENCY;
			$agency_id = $this->user->agency_data['agency_id'];

            $payment_methodes_model = new Model_PaymentMethodes();
            $activePaymentmethodes = $payment_methodes_model->getActivePaymentMethodes(Cubix_Application::getId());

			$agency_escorts = $this->client->call('OnlineBillingV2.getAgencyEscortsEGUKsc', array($agency_id));

			foreach($agency_escorts as $i => $escort) {

				$escort = new Model_EscortItem($escort);

				$cur_main_photo = $escort->getMainPhoto();
				
				if(is_null($cur_main_photo->hash)){
					$agency_escorts[$i]['photo'] = 'https://www.escortguide.co.uk/img/private/nopic.jpg';
				}else{
					$agency_escorts[$i]['photo'] = $cur_main_photo->getUrl('backend_smallest');
				}

				if ($escort['gender'] !== 1) {
					unset($escort);
				}
			}

			$available_packages = $this->client->call(
				'OnlineBillingV2.getPackagesList',
				array($user_type, self::GENDER, $is_pseudo_escort, array_merge(self::$packages_monthly, self::$packages))
			);
			$agency_discounts = $this->client->call('OnlineBillingV2.getAgencyDiscounts');

			die(json_encode(array(
				'user_type' => 'agency',
				'agency' => $this->user->agency_data,
				'escorts' => $agency_escorts,
				'available_packages' => $available_packages,
				'agency_discounts' =>  $agency_discounts,
				'activePaymentmethodes' =>  $activePaymentmethodes
			)));
		}
	}


    public function advertiseAction()
    {
        if(isset($_SESSION['message'])){
            $this->view->message = $_SESSION['message'];
            unset($_SESSION['message']);
        }
    }

    public function advertiseMmgAction()
    {

        $this->view->layout()->disableLayout();
        $request = $this->_request;

        if (!$this->_request->isPost() && !is_int($request->price)) die();

        $mmgBill = new Model_MmgBillAPIV2();
        if( empty($request->price) ) {

            $amount = '';
        } else {

            $amount = stripslashes($request->price);
        }

        $_SESSION['payment_mmg_amount'] = $amount;
        $hosted_url = $mmgBill->getHostedPageUrl($amount,time(), 'https://www.escortguide.co.uk'.$this->view->getLink('callback-mmg-response'));

        if (filter_var($hosted_url, FILTER_VALIDATE_URL) === FALSE) {
            $status = 'not-valid-url';
        }else{
            $status = 'success';
        }

        die(json_encode(array('status' => $status, 'url' =>  $hosted_url)));

    }

    public function advertiseMmgResponseAction()
    {
        $request = $this->_request;
        $payment_result = [];

        if (isset($request->txn_status) and $request->txn_status == 'APPROVED') {
            $payment_result['status'] = $request->txn_status;
            $payment_result['merchant'] = $request->mid;
            $payment_result['ti_mmg'] = $request->ti_mmg;
            $payment_result['ti_mer'] = $request->ti_mer;
            $payment_result['amount'] = isset($_SESSION['payment_mmg_amount']) ? $_SESSION['payment_mmg_amount'] : 0;

            if ($request->mmg_errno != '0000')
                $payment_result['error'] = $request->mmg_errno;

        } else {
            $payment_result['status'] = 'REJECTED';
        }


        $this->view->payment_status = $payment_result['status'];
        $this->view->payment_result = $payment_result;
    }

    public function advertisePaymentEmailAction(){


        $request = $this->_request;

        $payment_result = (isset($request->payment_data) && !empty($request->payment_data)) ? unserialize($request->payment_data) : [];
        $payment_result['email'] = isset($request->email) ? $request->email : '- - -';
        $payment_result['Link'] = isset($request->link) ? $request->link : '- - -';

        $current_user = Model_Users::getCurrent();

        if(!empty($current_user)){

            if ( $current_user->user_type == 'agency' ) {
                $payment_result['User'] = $current_user->agency_data['agency_id'];
            }
            else if ( $current_user->user_type == 'escort' ) {
                $payment_result['User'] = $current_user->escort_data['escort_id'];
            }

        }else{
            $payment_result['User'] = 'Was not authenticated';
        }

        $payment_details_string = '<ul>';
        foreach($payment_result as $key => $val){
            $payment_details_string .= "<li> <b> $key </b> $val </li>";
        }
        $payment_details_string .= '</ul>';

        $to = 'brandy@escortguide.co.uk';
        // $to = 'edhovhannisyan97@gmail.com';

        $mail_sent = Cubix_Email::sendTemplate('payment_details', $to, array(
            'details' => $payment_details_string
        ));

        if($mail_sent){
            $message = [
                'type' => "success",
                'text' => "Email has been sent."
            ];
        }else{
            $message = [
                'type' => "danger",
                'text' => "Couldn't send email, please contact our team."
            ];
        }

        $_SESSION['message'] = $message;
        unset($_SESSION['payment_mmg_amount']);
        $this->_redirect('/advertise');
    }



    public function checkoutAction() {
		if (!$this->_request->isPost()) die();
		$request = $this->_request;

		$hash  = base_convert(time(), 10, 36);
		$cart = $request->cart;
		$payment_method = $request->payment_method;
		$user_id = $this->user->id;
		$user_type = $this->user->isAgency() ? USER_TYPE_AGENCY : USER_TYPE_SINGLE_GIRL;
        $promo_code  = strip_tags($request->promo_code);

        $post_data = array();

		// ESCORT
		if ($this->user->isEscort()) {
			$data = array();
			if ($cart[0]['activation_date']) {
				$data['activation_date'] = $cart[0]['activation_date'];
			}
			if ($cart[0]['premium_cities']) {
				$data['premium_cities'] = $cart[0]['premium_cities'];
			}
            if ($cart[0]['banner_price']) {
                $data['banner_price'] = $cart[0]['banner_price'];
            }

			$post_data[] = array(
				'user_id' => $user_id,
				'escort_id' => $cart[0]['escortId'],
				'agency_id' => null,
				'package_id'=> $cart[0]['packageId'],
				'data' => serialize($data),
				'hash' => $hash,
                'promo_code' => $promo_code
			);
		}

		// AGENCY

		if ($this->user->isAgency()) {
			foreach($cart as $package) {

				$data = array();

				if ($package['activation_date']) {
					$data['activation_date'] = $package['activation_date'];
				}

				if ($package['premium_cities']) {
					$data['premium_cities'] = $package['premium_cities'];
				}

				if ($package['discount']) {
					$data['discount'] = $package['discount'];
				}

                if ($cart[0]['banner_price']) {
                    $data['banner_price'] = $cart[0]['banner_price'];
                }

				$post_data[] = array(
					'user_id' => $user_id,
					'escort_id' => $package['escortId'],
					'agency_id' => $package['agencyId'],
					'package_id'=> $package['packageId'],
					'data' => serialize($data),
					'hash' => $hash,
                    'promo_code' => $promo_code
				);
			}
		}

		$amount = $this->client->call(
			'OnlineBillingV2.addToShoppingCartED',
			array(
				$post_data,
				$user_id,
				$user_type,
				$hash
			)
		);

        if ($payment_method == "mmgbill") {
//            if ($cart[0]['banner_price'] && count($cart) < 3) {
//                $amount = $amount + 30;
//            } else if ($cart[0]['banner_price'] && count($cart) > 2) {
//                $amount = $amount + 25;
//            } else if ($cart[0]['banner_price'] && !$this->user->IsAgency()) {
//                $amount = $amount + 30;
//            }

            $promoData = $this->client->call('OnlineBillingV2.checkPromoCode',array($promo_code));

            if ($promoData['amount'])
            {
                if( $amount > $promoData['amount'] )
                {
                    $amount = $amount - $promoData['amount'];
                }else{
                    $amount = 0;
                }
            }elseif($promoData['percent']){
                $amount = $amount - ( $amount * $promoData['percent'] ) / 100;
            }

            $mmgBill = new Model_MmgBillAPIV2();

            $hosted_url = $mmgBill->getHostedPageUrl(
                $amount,
                'SCZ' . $this->user->id . 'Z' . $hash,
                'http://www.escortguide.co.uk' . $this->view->getLink('ob-mmg-postback')
            );
		}elseif ($payment_method == 'paysafe') {
				$first_name = trim(preg_replace('#[^a-zA-Z]#', ' ', $this->user->username)); 
				$last_name = trim(preg_replace('#[^a-zA-Z]#', ' ', $this->user->chat_info['nickName']));
				
				if( empty($first_name)){
					$first_name = 'NAME';
				}

				if(empty($last_name)){
					$last_name = 'LASTNAME';
				}
					
				$reference = 'SC-' . $this->user->id . '-' . $hash;
				
				$customer = new Cubix_2000charge_Model_Customer();
				$customer->setEmail($this->user->email);
				$customer->setCountry("GB");
				$customer->setFirstName($first_name);
				$customer->setLastName($last_name);

				$payment = new Cubix_2000charge_Model_Payment();
				$payment->setPaymentOption("paysafe");
				$payment->setHolder($first_name.' '.$last_name);

				$transaction = new Cubix_2000charge_Model_Transaction();
				$transaction->setCustomer($customer);
				$transaction->setPayment($payment);
				$transaction->setAmount($amount * 100);
				$transaction->setCurrency("GBP");
				$transaction->setIPAddress(Cubix_Geoip::getIP());
				$transaction->setMerchantTransactionId($reference);
				
				$host = 'https://' . $_SERVER['SERVER_NAME'];
				// $host = 'http://' . $_SERVER['SERVER_NAME'];
				$redirectUrls = new Cubix_2000charge_Model_RedirectUrls();
				$redirectUrls->setReturnUrl($host . $this->view->getLink('billing-paysafe-success'));
				$redirectUrls->setCancelUrl($host . $this->view->getLink('billing-paysafe-failure'));
				$transaction->setRedirectUrls($redirectUrls);

				$res = Cubix_2000charge_Transaction::post($transaction);
				$this->client->call('OnlineBillingV2.storeToken', array($res->id, $this->user->id, '2000charge'));
				$hosted_url = $res->redirectUrl;
			}

		die(json_encode(array('status' => 'success', 'url' =>  $hosted_url)));
	}

	public function successAction() {
	}
	public function mmgPostbackAction() {
		$this->view->layout()->enableLayout();
		$this->view->transaction_status = $this->_request->txn_status;
		$this->view->mmg_errno = $this->_request->mmg_errno;
	}

	public function paysafeSuccessAction() {
		// var_dump('success');die;
		$this->view->layout()->enableLayout();
	}

	public function paysafeFailureAction() {
		// var_dump('failure');die;
		$this->view->layout()->enableLayout();
	}

	public function failureAction()	{

	}

	public function movePackageAction(){

		 if ( ! $this->user->isAgency() ) die;

		 $agency = $this->user->getAgency();

		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'from_escort_id' => 'int-nz',
			'to_escort_id' => 'int-nz',
			'city_ids' => 'arr-int'
		));
		

		 $data = $form->getData();
		
		if ( !$agency->hasEscort($data['from_escort_id']) || ! $agency->hasEscort($data['to_escort_id']) ) {
			die;
		}
		
		$result = $this->client->call('premium_switchActivePackages', array($data['from_escort_id'], $data['to_escort_id'], $data['city_ids']));

		if($result['success'] == false && isset($result['error'])) {
		   die(json_encode(array('status' => 'error', 'result' =>  $result['error'])));	
		}

		die(json_encode(array('status' => 'success', 'result' =>  $result)));
	}

	public function paymentAction()
	{
		$this->view->layout()->enableLayout();
		if(isset($this->_request->price)){
			$this->view->price = intval($this->_request->price);
		}
		
	}

    public function checkPromoCodeAction() {
        if (!$this->_request->isPost()) die();
        $request = $this->_request;

        $promo_code  = strip_tags($request->promo_code);

        $promoCode = $this->client->call('OnlineBillingV2.checkPromoCode',array($promo_code));

        if ($promoCode)
        {
            die(json_encode(array('status' => 'success', 'promo_data' =>  $promoCode)));
        }else{
            die(json_encode(array('status' => 'error', 'message' =>  'Invalid Promo code')));
        }
    }
}