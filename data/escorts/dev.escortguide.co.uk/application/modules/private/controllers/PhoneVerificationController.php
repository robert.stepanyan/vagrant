<?php
class Private_PhoneVerificationController extends Zend_Controller_Action
{
	
	public function init()
	{
		$this->client = new Cubix_Api_XmlRpc_Client();
		$this->session = new Zend_Session_Namespace('phone-confirmation');
		$cache = Zend_Registry::get('cache');
		$this->view->user = $this->user = Model_Users::getCurrent();

        if ( ! $this->user ) {

            if($this->getRequest()->isXmlHttpRequest()){
                header("HTTP/1.1 403 Forbidden");
            }else{
                $this->_redirect($this->view->getLink('signin'));
            }

            return;
        }
		
		//$this->model = new Model_Escorts();
		$cache_key =  'v2_user_pva_' . $this->user->id;

		if ( $this->user->isAgency() ) {
			if ( ! $agency = $cache->load($cache_key) ){
				$agency = $this->user->getAgency();
				$cache->save($agency, $cache_key, array(), 300);
			}

			$this->agency = $this->view->agency = $agency;
			//$this->view->agency_escorts = Model_Escorts::getAgencyEscorts($agency->id);
		}
		else if ( $this->user->isEscort() ) {
			if ( ! $escort = $cache->load($cache_key) ) {
				$escort = $this->user->getEscort();
				$cache->save($escort, $cache_key, array(), 300);
			}

			$this->escort = $this->view->escort = $escort;
		}
	}

	public function indexAction()
	{
		$this->view->layout()->disableLayout();
        $this->view->availableVerificationMethodes = $availableVerificationMethodes = $this->client->call('Verification.getPhoneVerificationActiveMethodes');
        if ( $this->user->isEscort() ) {
            $details = $this->client->call('Verification.getPhoneVerificationDetails', array($this->escort->id));
            if ($details['last_hand_verification_date']) {
                $this->_helper->viewRenderer->setScriptAction('confirmed');
            }
            $this->view->phone = $this->session->phone = preg_replace('/^00/', '+', $details['phone_exists']);

            $us_canada_pattern = "/^001([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})([0-9]{4})$/i";
            $this->view->is_us_canada = preg_match($us_canada_pattern, $details['phone_exists']);
            $this->view->escort_id = $this->escort->id;
            $this->view->sales = array(
                'Jolyne' => '+41767704542',
                'Carrie' => '+41767431209'
            );
        }elseif ( $this->user->isAgency() )
        {
            $details = $this->client->call('Verification.getAgencyPhoneVerificationDetails', array($this->user->id));
            if ($details[1])
            {
                $this->user->need_phone_verification = false;
            }
            if (!empty($details[0])) {
                $this->view->phones = array_unique($details[0]);
                $us_canada_pattern = "/^001([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})([0-9]{4})$/i";
                $this->view->is_us_canada = false;
                foreach ($details as $val) {
                    $is_us_canada = preg_match($us_canada_pattern, $val['phone']);
                    if ($is_us_canada)
                        $this->view->is_us_canada = true;
                    continue;
                }
                $this->view->sales = array(
                    'Jolyne' => '+41767704542',
                    'Carrie' => '+41767431209'
                );
            }else{
                $this->user->need_phone_verification = false;
            }
        }
	}
	
	/*public function escortsAction()
	{
		$this->view->layout()->disableLayout();
		
		
		if ( ! $this->user->isAgency() ) die;

		$this->view->s_showname = '';
	
		
		$per_page = $this->_request->getParam('per_page', 10);
		if ( !$per_page ) {
			$per_page = 10;
		}

		$escorts_page = $this->view->escorts_page = $this->_getParam('escorts_page', 1);

		$data = $this->agency->getEscortsPerPage($escorts_page, $per_page, 1, false);
		
		$this->view->count = $data['escorts_count'];
		$this->view->escorts_per_page = $per_page;

		$this->view->escorts = $data['escorts'];
		$this->view->agency_id = $this->agency->getId();
        $this->view->current_page = $cur_page;

	}
	
	public function ajaxEscortsAction()
	{
		
		$this->view->layout()->disableLayout();
		
		if ( ! $this->user->isAgency() ) die("This page is available only for Agencies");

		$this->view->s_showname = '';

		$per_page = $this->_request->getParam('per_page', 10);
		if ( !$per_page ) {
			$per_page = 10;
		}
		
		if($this->_request->getParam('search_content')){
			$searched_word = $this->_request->getParam('search_content');
		}else{
			$searched_word = '';
		}
		
		$escorts_page = $this->view->escorts_page = $this->_getParam('escorts_page', 1);
		$data = $this->agency->getEscortsPerPage($escorts_page, $per_page, 1, false, $searched_word);
		$this->view->count = $data['escorts_count'];
		$this->view->escorts_per_page = $per_page;
		$this->view->did_search = true;

		$this->view->escorts = $data['escorts'];
		$this->view->searched_word = $searched_word;
		$this->view->agency_id = $this->agency->getId();
		$this->view->current_page = $this->_getParam('escorts_page', 1);
		$this->_helper->viewRenderer->setScriptAction('escorts');
		
	}*/
	
	
	public function phoneCallAction()
	{
        $callType = $this->_request->callType;
        if ($callType == 'misscall') {
            $s_config = Zend_Registry::get('system_config');
            $citcall = new Cubix_Citcall($s_config['citcall']['key']);

            if (!isset($this->session->phone)) {
                die(json_encode(array('status' => 'failed')));
            }
            $ret = $citcall->asynccall($this->session->phone);

            if ($ret['result'] === 'Success') {
                $this->session->code = $ret['token'];
                $first = substr($ret['token'], 0, -4);
                $code = substr($ret['token'], -4);
                $data = array(
                    'escort_id' => $this->escort->id,
                    'type' => 'misscall',
                    'code' => $code,
                    'phone' => $this->session->phone
                );
                $this->session->id = $this->client->call('Verification.addPhoneVerification', array($data));
                die(json_encode(array('first' => $first, 'status' => 'success')));
            } else {
                die(json_encode(array('status' => 'failed')));
            }
        }else{
            $sinchConfig = Zend_Registry::get('system_config');
            $sinchCallKey = $sinchConfig['sinchCall']['key'];
            $sinchCallSecret = $sinchConfig['sinchCall']['secret'].'==';
            $sinchCall = new Cubix_Sinchcall($sinchCallKey,$sinchCallSecret);

            if (!isset($this->session->phone)) {
                die(json_encode(array('status' => 'failed')));
            }

            $ret = $sinchCall->asynccall($this->session->phone);

            if ($ret['result'] === 'Success') {
                $first = substr($ret['token'], 0, -4);
                $code = substr($first, 4);
                $this->session->code = $code;
                $data = array(
                    'escort_id' => $this->escort->id,
                    'type' => 'sinchcall',
                    'code' => $code,
                    'phone' => $this->session->phone
                );
                $this->session->id = $this->client->call('Verification.addPhoneVerification', array($data));
                die(json_encode(array('type'=> 'sinchcall', 'status' => 'success')));
            } else {
                die(json_encode(array('status' => 'failed')));
            }
        }
	}

    public function agencyPhoneCallAction()
    {
        $callType = $this->_request->callType;
        if ($callType == 'misscall') {
            $s_config = Zend_Registry::get('system_config');
            $citcall = new Cubix_Citcall($s_config['citcall']['key']);
            $phone = $this->_request->phone;
            $this->session->phone = $phone;
            $agency_id = intval($this->_request->agency_id);
            if(!isset($phone)){
                die(json_encode(array('status' => 'failed')));
            }
            $ret = $citcall->asynccall(intval($phone));

            if($ret['result'] === 'Success'){
                $this->session->code = $ret['token'];
                $first = substr($ret['token'], 0, -4);
                $code =  substr($ret['token'], -4);
                $data = array(
                    'agency_id' => $agency_id,
                    'user_id' => $this->user->id,
                    'type' => 'misscall',
                    'code' => $code,
                    'phone' => $phone
                );
                $this->session->id = $this->client->call('Verification.addAgencyPhoneVerification', array($data));
                die(json_encode(array('first' => $first, 'status' => 'success')));
            }
            else{
                die(json_encode(array('status' => 'failed')));
            }
        }else{
            $sinchConfig = Zend_Registry::get('system_config');
            $sinchCallKey = $sinchConfig['sinchCall']['key'];
            $sinchCallSecret = $sinchConfig['sinchCall']['secret'].'==';
            $sinchCall = new Cubix_Sinchcall($sinchCallKey,$sinchCallSecret);
            $phone = $this->_request->phone;
            $phone = preg_replace('/^00/', '+', $phone);
            $this->session->phone = $phone;
            $agency_id = intval($this->_request->agency_id);

            if (!isset($this->session->phone)) {
                die(json_encode(array('status' => 'failed')));
            }

            $ret = $sinchCall->asynccall($this->session->phone);

            if ($ret['result'] === 'Success') {
                $first = substr($ret['token'], 0, -4);
                $code = substr($first, 4);
                $this->session->code = $code;
                $data = array(
                    'agency_id' => $agency_id,
                    'user_id' => $this->user->id,
                    'type' => 'sinchcall',
                    'code' => $code,
                    'phone' => $phone
                );
                $this->session->id = $this->client->call('Verification.addAgencyPhoneVerification', array($data));
                die(json_encode(array('status' => 'success','type'=> 'sinchcall')));
            } else {
                die(json_encode(array('status' => 'failed')));
            }
        }

    }
	
	public function sendSmsAction()
	{
		if(!isset($this->session->phone)){
			die(json_encode(array('status' => 'failed')));
		}
		
		try{
			$code = dechex(rand(70000,100000));
			$this->session->code = $code; 			
			$phone = $this->session->phone;	
			//$phone = '0037499555538';
			$data = array(
				'escort_id' => $this->escort->id,
				'type' => 'sms',
				'code' => $code,
				'phone' => $phone
			);

			$this->session->id = $this->client->call('Verification.addPhoneVerification', array($data));
			
			$msg_id = Cubix_Api::getInstance()->call('outbox', array($this->escort->id, $phone, Cubix_Application::getPhoneNumber(Cubix_Application::getId()), $code, Cubix_Application::getId()));
			$conf = Zend_Registry::get('system_config');
			$sms_conf = $conf['sms'];
			$originator = Cubix_Application::getPhoneNumber(Cubix_Application::getId());
			$sms = new Cubix_SMS($sms_conf['userkey'], $sms_conf['password']);
			$sms->setOriginator($originator);
			$sms->addRecipient($phone, $msg_id);
			$sms->setBufferedNotificationURL($sms_conf['BufferedNotificationURL']);
			$sms->setDeliveryNotificationURL($sms_conf['DeliveryNotificationURL']);
			$sms->setNonDeliveryNotificationURL($sms_conf['NonDeliveryNotificationURL']);
			$sms->setContent($code);
			$sms->sendSMS();

			die("done");
		}catch( Exception $e ) {
			die(json_encode(array('status' => 'failed')));
		}		
		
	}


    public function agencySendSmsAction()
    {
        try{
            $code = dechex(rand(70000,100000));
            $this->session->code = $code;
            $phone = $this->_request->phone;
            $this->session->phone = $phone;
            $agency_id = intval($this->_request->agency_id);
            if(!isset($phone)){
                die(json_encode(array('status' => 'failed')));
            }
//            $phone = '0037455828280';
            $data = array(
                'agency_id' => $agency_id,
                'user_id' => $this->user->id,
                'type' => 'sms',
                'code' => $code,
                'phone' => $phone
            );

            $this->session->id = $this->client->call('Verification.addAgencyPhoneVerification', array($data));

            $msg_id = Cubix_Api::getInstance()->call('outbox', array(null, $phone, Cubix_Application::getPhoneNumber(Cubix_Application::getId()), $code, Cubix_Application::getId()));
            $conf = Zend_Registry::get('system_config');
            $sms_conf = $conf['sms'];
            $originator = Cubix_Application::getPhoneNumber(Cubix_Application::getId());
            $sms = new Cubix_SMS($sms_conf['userkey'], $sms_conf['password']);
            $sms->setOriginator($originator);
            $sms->addRecipient($phone, $msg_id);
            $sms->setBufferedNotificationURL($sms_conf['BufferedNotificationURL']);
            $sms->setDeliveryNotificationURL($sms_conf['DeliveryNotificationURL']);
            $sms->setNonDeliveryNotificationURL($sms_conf['NonDeliveryNotificationURL']);
            $sms->setContent($code);
            $sms->sendSMS();

            die("done");
        }catch( Exception $e ) {
            die(json_encode(array('status' => 'failed')));
        }

    }

	
	public function confirmCodeAction()
	{
		$req = $this->_request;

		if ($req->type && $req->type == "sinchcall")
        {
            $preCode = preg_replace('/[^a-zA-Z0-9]+/', '', $req->getParam('verification_code'));
            $code = substr($preCode,1,6);
        }else{
            $code = preg_replace('/[^a-zA-Z0-9]+/', '', $req->getParam('verification_code'));
        }
		try{
			$data = array(
				'input_code' => $code
			);
			if ( $this->user->isEscort()) {
                if ($this->session->code === $code) {
                    $data['status'] = 1;
                    $this->client->call('Verification.updatePhoneVerification', array($this->session->id, $this->escort->id, $data));
                    $this->user->need_phone_verification = false;
                    Model_Users::setCurrent($this->user);
                    $this->view->phone = $this->session->phone;
                    $html = $this->view->render('/phone-verification/confirmed.phtml');
                    die(json_encode(array('status' => 'success', 'html' => $html)));
                } else {
                    $data['status'] = 0;
                    $this->client->call('Verification.updatePhoneVerification', array($this->session->id, $this->escort->id, $data));
                    die(json_encode(array('status' => 'failed')));
                }
            }elseif ( $this->user->isAgency())
            {
                if ($this->session->code === $code) {
                    $data['status'] = 1;
                    $this->client->call('Verification.updateAgencyPhoneVerification', array($this->session->id, $data));
                    Model_Users::setCurrent($this->user);
                    $this->view->phone = $this->session->phone;
                    $html = $this->view->render('/phone-verification/confirmed.phtml');
                    die(json_encode(array('status' => 'success', 'html' => $html)));
                } else {
                    $data['status'] = 0;
                    $this->client->call('Verification.updateAgencyPhoneVerification', array($this->session->id, $data));
                    die(json_encode(array('status' => 'failed')));
                }
            }
		}catch( Exception $e ) {
			die(json_encode(array('status' => 'failed')));
		}
	}		
	
	
}
