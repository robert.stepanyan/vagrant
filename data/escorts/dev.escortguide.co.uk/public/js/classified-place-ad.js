
var PlaceClassifiedAdd = {

  init: function(){
    this.initSummernote();
    this._upload();
    this._preview();
    this._preview();
    this._city_filters();
    this._removepic();
  },

  _upload: function(){
    $('.classfied-ad-pic').on('click',  function(event) {
      var imginput = $(this).siblings('input');
      imginput.trigger( "click" );
    });
  },
  _preview: function(){
     $(".pic-upload-btn").on('change', function(event) {
        if (this.files && this.files[0]) {
          var reader = new FileReader();
          var preview = $(this).siblings('img');
          reader.onload = function(e) {
            preview.attr('src', e.target.result);
          }
          reader.readAsDataURL(this.files[0]);
          preview.parent('div').removeClass('default-pic');
          preview.siblings('input.sessionpic').val(0);
        }
     });
  },
  initSummernote: function(){
    document.emojiSource = '/js/private/emoji/';
      $('#summernote').summernote({
          height: 255,
          disableDragAndDrop: true,
          disableResizeEditor: true,
          shortcuts: false,
          toolbar: [
              ['misc', ['emoji', 'undo', 'redo']],
              ['style', ['bold', 'italic', 'underline',  'strikethrough','clear']],
          ],
          popover: { }
      });
  },
  _city_filters: function(){
    var cities_selected = ['city', 'city2', 'city3'];
    var previous_city_id;
    $('select[name^=city], select[name^=city2], select[name^=city3]').on('focus', function(event) {
     previous_city_id = this.value;
    }) .on('change', function(event) {

    var selected_city_id = $(this).find(":selected").val();
    var selected_cilectbox = $(this).attr('name');
    
      for(c of cities_selected){
        if(c != selected_cilectbox){
          if(typeof(previous_city_id) !== undefined && previous_city_id > 0){
              $('select[name^='+c+']').find('option[value="'+previous_city_id+'"]').attr('disabled', false);
          }
          $('select[name^='+c+']').find('option[value="'+selected_city_id+'"]').attr('disabled','disabled');
        }
      }
      
      /* Act on the event */
    });
   },
  _removepic: function(){
    $('.ion-close-circled').on('click',  function(event) {
      var image = $(this).siblings('img');
      $(this).siblings('img').fadeOut('fast', function () {
          image.attr('src', '../img/classfied-ad-girl.png');
          image.fadeIn('fast');
      });

      $(this).siblings('input').val("");
      $(this).siblings('input.sessionpic').val(0);
      $(this).parent('div').addClass('default-pic');

    });
  }

}//PlaceClassifiedAdd END

$( document ).ready(function() {
  PlaceClassifiedAdd.init();
});
