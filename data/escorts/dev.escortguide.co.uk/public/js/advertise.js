$( document ).ready(function() {

	if($('.btn-buy button')){
		$('.btn-buy button').on( 'click', function(){
			
			var price = $(this).data('price');
			var id = $(this).data('id');
			var days = $(this).data('days');

			$('#govazd').LoadingOverlay("show");

			$.ajax({
			  method: "POST",
			  url: "/advertise/mmg",
			  data: { id: id,  days: days, price: price },

			   success: function(response, status, xhr){
				   try {
					response = JSON.parse(response);

					if(typeof response =='object'  ){
					 if(response.status == 'success'){
						window.location.href = response.url;
					 }
					}
				   }catch (e) {

				   }finally{
					$('#govazd').LoadingOverlay('hide');
				   }
			   }
			});
		});
	}
		
	if($('#paymentForm')){
		$('#paymentForm').on('submit', function(e){
			e.preventDefault();
			var price = $('#amount-input').val();
			
			$(this).LoadingOverlay("show");

			$.ajax({
				method: "POST",
				url: "/advertise/mmg",
				data: {price: price},
				success: function(response, status, xhr){
					try {
						response = JSON.parse(response);

						if(typeof response =='object'  ){
							if(response.status == 'success'){
								window.location.href = response.url;
							}
						}
					}catch (e) {

					}finally{
					  $('this').LoadingOverlay('hide');
					}
				}
			});
		});
	}
	
  	$('.fancy-form input')
    .focus(function () {
        $(this).parents('.fancy-form').addClass('focused');
    })
    .blur(function () {
        if (!$(this).val().length) {
            $(this).parents('.fancy-form').removeClass('focused');
        } else {
            $(this).parents('.fancy-form').addClass('focused');
        }
    })
    .keydown(function () {
        if ($(this).val().length) {
            $(this).parents('.fancy-form').addClass('focused');
        }
    })

    $(window).on('resize',function () {
        setTimeout(function () {
            var content_height = parseInt($('.content').height() + $('footer').height() + 150);
            //checking for fixed footer
            if( content_height < $(window).height()) {
                $('footer').addClass('fixed-bottom');
            }else{
                $('footer').removeClass('fixed-bottom');
            }
        },300);
    });

    $(window).trigger('resize');
});