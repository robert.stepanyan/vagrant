// Dummy hijacking back button of chrome
// ------------------------------------
// (function (window, location) {
//     if (!document.referrer.includes(location.host)) {
//         var _url = location.toString().replace(location.hash, "");
//
//         history.replaceState(null, document.title, _url + "#!/history");
//         history.pushState(null, document.title, _url);
//
//         window.addEventListener("popstate", function () {
//             if (location.hash === "#!/history") {
//                 history.replaceState(null, document.title, location.pathname);
//                 setTimeout(function () {
//                     location.replace("/");
//                 }, 0);
//             }
//         }, false);
//     }
// }(window, location));
// ------------------------------------


Sceon.GovazdContact = {
    Show : function ( package, days , price ) {
        var popup =  $('#advertise_popup');

        popup.modal();
        popup.find('.modal-title').html(package);

        //Reset Form old datas
        $('#advertise_form')[0].reset();
        $('.has-danger').removeClass('has-danger');
        $('.error-report').html(' ');

        $('input[name=package]').val(package);
        $('input[name=days]').val(days);
        $('input[name=price]').val(price);

    }
};

var checkForPageBanners = function () {
    var hashed = window.location.hash;
    $('.babylon_girls').css('display', 'block');
    if (hashed.includes("page") && hashed !== "#page=1;") {
        $('.babylon_girls').css('display', 'none');
    }

};


$( document ).ready(function() {

    if($('.review-chunk').length > 0)
    {
        var lastPage = $('.review-chunk').last().data('chunk');
        var activePage = 1;
        $.each($('.review-chunk'), function () {
            if ($(this).data('chunk') != activePage) {
                $(this).hide();
            }
        });
        $('.page-item').on('click', function (e) {
            e.preventDefault();
            var page = parseInt($('input[name=page]').val());

            if ($(this).data('page') === 'next' && page < (lastPage)) {
                $(".escort-reviews").LoadingOverlay("show", {color: "rgba(239, 243, 249, 0.80)", zIndex: 1000});
                $.each($('.review-chunk'), function () {
                    if ($(this).data('chunk') === (page + 1)) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }
                });
                $('input[name=page]').val(page + 1);

            } else if ($(this).data('page') === 'previous' && page > 1) {
                $(".escort-reviews").LoadingOverlay("show", {color: "rgba(239, 243, 249, 0.80)", zIndex: 1000});
                $.each($('.review-chunk'), function () {
                    if ($(this).data('chunk') === (page - 1)) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }
                });
                $('input[name=page]').val(page - 1);
            }

            $(".escort-reviews").LoadingOverlay("hide");
        });
    }

    $('.fancy-form input')
    .focus(function () {
        $(this).parents('.fancy-form').addClass('focused');
    })
    .blur(function () {
        if (!$(this).val().length) {
            $(this).parents('.fancy-form').removeClass('focused');
        } else {
            $(this).parents('.fancy-form').addClass('focused');
        }
    })
    .keydown(function () {
        if ($(this).val().length) {
            $(this).parents('.fancy-form').addClass('focused');
        }
    })

    $(window).on('resize',function () {
        setTimeout(function () {
            var content_height = parseInt($('.content').height() + $('footer').height() + 150);
            //checking for fixed footer
            if( content_height < $(window).height()) {
                $('footer').addClass('fixed-bottom');
            }else{
                $('footer').removeClass('fixed-bottom');
            }
        },300);
    });

    $(window).trigger('resize');

     // moved to chat js
     // $(".chat-info-online-escorts").mCustomScrollbar({theme:"dark", autoHideScrollbar: true, scrollButtons:{ enable: true }});
     // $(".online-escorts-scroll-block").mCustomScrollbar({theme:"dark", autoHideScrollbar: true, scrollButtons:{ enable: true }});

    //  $(document).on('focus','input, textarea, select',function() {
    //     setTimeout(function() {
    //         $('body').addClass('keyboard-open');
    //     },20);
    // });
    // $(document).on('blur','input, textarea, select',function() {
    //     setTimeout(function() {
    //         $('body').removeClass('keyboard-open');
    //     },10);
    // });
    
    //Agency Profile Escort Slider
    var loadedPhotosCount = 0;
    $('.gotm-box').LoadingOverlay("show", {color : "rgba(239, 243, 249, 0.80)", zIndex: 1000   });
    var photosCount = $('.gotm-slider img').length;
    
    $('.gotm-slider img').on('load', function(){
      loadedPhotosCount++;
      if (loadedPhotosCount == photosCount) {
        intigotmslider();
      }
    }).each(function() {
      if (this.complete) {
        $(this).trigger('load');
      }
    });

    function intigotmslider() {
       $('.gotm-slider').slick({
       	infinite: true,
		 	  autoplay: true,
		  	autoplaySpeed: 5000,
     });
       $('.gotm-box').LoadingOverlay('hide');
    }



    $('.ajax-login-form').submit( function(event) {
      event.preventDefault();
      $('.ajax-login-form').LoadingOverlay("show");
      var data = $(this).serializeArray();
      data.push({name:'ajax',value:'true'});


      $.ajax({
        url: '/account/signin/',
        type: 'POST',
        data: data, 
        success: function(response){
          $('.ajax-login-form').LoadingOverlay('hide');
          try {
            response = JSON.parse(response);
            if(typeof response =='object' && response['status'] == 'error' ){
              $.each( response['msgs'], function( key, value ) {
                $('.ajax-login-response').html('<div class=\"alert alert-danger\" >'+ value +'</div>');
              });
           }
           if(response['status'] == 'success'){
            $('.ajax-login-response').html('<div class=\"alert alert-success\" >'+ response['msgs']+'</div>');
            setTimeout(function() {
               $('#LoginPopup').modal('hide');
                location.reload();
            }, 1000);
           }                    
        }
        catch (e) {

        }
        
        
        }
      })

     });

    if($('#advertise_form').length) {
        $('#advertise_form').submit(function (event) {
            event.preventDefault();

            var self = $(this);

            var data = self.serializeArray();

            $.ajax({
                url:'/govazd-contact-send-mail',
                type:'POST',
                data : data,
                dataType: 'json',
                success:function (resp) {
                    if (resp.status === 'error') {

                        $('.has-danger').removeClass('has-danger');
                        $('.error-report').html(' ');
                        $.each(resp['msgs'], function (key, value) {
                            $('#' + key + '-error').html(value).parent().addClass('has-danger');
                        });
                        return false;
                    }

                    self.closest('.modal').modal('hide');
                    Notify.alert('success', 'Your message has been successfully sent!');
                }
            });

        });
    }

     //enabling tooltips
       $('[data-toggle="tooltip"]').tooltip()
     //enabling tooltips END

         // Find Escort btn VIEW
        $('#collapsesearch').on('show.bs.collapse', function(){
          $(window).scrollTop(0);
          $('#mobile-menu').collapse('hide');
          if($(window).width() < 576){
            $( "body > div.container-fluid, body > div.container-jumbotron" ).not( $('.container-fluid.main-menu, .container-fluid.mobile-menu') ).css('display', 'none');
          }
        })

        if($(window).width() < 576){
          $('#collapsesearch').on(' hide.bs.collapse', function(){
            $( "body > div.container-fluid, body > div.container-jumbotron" ).css('display', 'block');
          })
        }
        
        $(window).scroll(function (event) {
          var scroll = $(window).scrollTop();

          if(scroll > 1 && $(window).width() > 992){
            $('#collapsesearch').collapse('hide');
          }
        });
        // Find Escort btn VIEW END

        // GRID-LIST VIEW
        $('#view-type-box').on('change', function() {
           if( this.value == 'grid' ){
             viewChange('grid');
          }else if(this.value == 'list'){
             viewChange('list');
          }
        });
        $('#mob-list-view').on('click', function() {
          viewChange('list');
        })
        $('#mob-grid-view').on('click', function() {
          viewChange('grid');
        })
        
        function viewChange($viewtype){
            if( $viewtype == 'list' ){
            $('.escorts-grid .escort-photo').removeClass('col-6 col-sm-6 col-md-4 col-lg-4');
            $('.escorts-grid .escort-photo').addClass('col-6 col-sm-6 col-md-4 col-lg-3 escorts-grid-list');
            $('.adv').addClass('escort-photo mb-3 col-md-4 col-lg-4 hidden-sm-down');
            $('.premium-banner-spot').addClass('hidden-xs-up');

            $('.escorts-grid .list-view').removeClass('hidden-xs-up');
            $('.escorts-grid .list-view').addClass('col-6 col-sm-6 col-md-8 col-lg-9');

            $('.escorts-grid .short-info-box').addClass('hidden-xs-up');
          }else if($viewtype == 'grid'){
            $('.escorts-grid .escort-photo').removeClass('col-6 col-sm-6 col-md-3 col-lg-3 escorts-grid-list');
            $('.escorts-grid .escort-photo').addClass('col-6 col-sm-6 col-md-4 col-lg-4');
            $('.premium-banner-spot').removeClass('hidden-xs-up');
            $('.escorts-grid .list-view').removeClass('col-6 col-sm-6 col-md-9 col-lg-9');
            $('.escorts-grid .list-view').addClass('hidden-xs-up');

            $('.escorts-grid .short-info-box').removeClass('hidden-xs-up');
          }
            document.cookie = 'view-type='+$viewtype+';expires=2050;path=/';
        }
        // GRID-LIST VIEW END           

        //overlay for mobilemenu
        $('#mobile-menu').on('show.bs.collapse', function (){
            $('<div class="modal-backdrop fade show"></div>').appendTo(document.body);
            $('.fixed-top').css('z-index', 1080);
            $('#nav-icon1').toggleClass('open');
            $('#collapsesearch').collapse('hide');

        })

        $('#mobile-menu').on('hide.bs.collapse', function (){
            $(".modal-backdrop").remove();
            $('.fixed-top').css('z-index', 1030);
            $('#nav-icon1').toggleClass('open');
        })

        $('.modal-backdrop').on('click', function(){
            $('#mobile-menu').collapse('hide');
        })
        //overlay for mobilemenu end

        // mobile scroll top
        window.onscroll = function(ev) {
            if ((window.innerHeight + window.scrollY) >= 2500) {
              $('.mobile-scroll-top').removeClass('d-none');
            }else{
              $('.mobile-scroll-top').addClass('d-none');
            }
        };
        $('.mobile-scroll-top').on('click', function() {
          $('html,body').animate({ scrollTop: 0 }, 'slow');
        });
        // mobile scroll top END

        // IXS banenrs impressions image lazy loading just for GT metrix
        setTimeout(function(){ 
          $('.ixs-banners').each(function(index, el) {
            $(el).attr("src", $(el).attr('lsrc'));
          });
          console.log('loading ixs banners done')
        }, 3000);

        $('#inner-search-form').submit(function(event){
            event.preventDefault();
            $( this ).trigger( "blur" );
        });
    checkForPageBanners();

    //  For classified ads filters
    //  Showing the city selection only the cities for which we have results
    if ($('.ca-filters'))
    {
        $('.ca-filters').change(function (e) {
            fetchData();
        });
    }

    if ($('#ca-category'))
    {
        $('#ca-category').change(function () {
            if ($('#ca-city'))
            {
                $('#ca-city').val('');
                fetchData();
            }
        });
    }


    function fetchData() {
        var catergory = $('#ca-category').val();
        var city = $('#ca-city').val();
        var text = $('.ca-filters [name=text]').val();
        var container = $('#classified-ads');
        container.LoadingOverlay("show");

        $.ajax({
            url: "/classified-ads?ajax=1&category=" + catergory + "&city=" + city + "&text=" + text,
            success: function (r) {
                try {
                    r = JSON.parse(r);
                    container.html(r.escort_list);

                    if (r.cities_list) {
                        var citiesDropdown = $('#ca-city');
                        citiesDropdown.empty().append('<option value=""> City </option>');
                        for (var x in r.cities_list) {
                            var cityItem = r.cities_list[x];
                            var selected = cityItem.id == city ? 'selected=selected' : '';
                            var html = '<option ' + selected + ' value="' + cityItem.id + '">' + cityItem.title_en + '</option>';
                            citiesDropdown.append(html);
                        }
                    }
                    // if (r.categories_list){
                    //     var categoriesDropdown = $('#ca-category');
                    //     var categories = headerVars.classifiedAdCategories;
                    //     categoriesDropdown.empty().append('<option value=""> Category </option>');
                    //     for (var y in r.categories_list){
                    //         var catergoryItem = r.categories_list[y];
                    //         var selected = catergoryItem.id == catergory ? 'selected=selected' : '';
                    //         var html = '<option ' + selected + ' value="' + catergoryItem.category_id + '">' + categoryname + '</option>';
                    //         categoriesDropdown.append(html)
                    //     }
                    // }
                } catch (e) {
                }
                container.LoadingOverlay("hide");
            }
        })
    }
    //  For classified ads filters - END

    $('.tag-count').on('click',function (e){
        e.stopPropagation();

        var tagId = $(this).data('tag');
        var city = $('input[name=city]').val();

        $.ajax({
            url:'/escorts/tag-tracker',
            type:'post',
            data : {tagId,city},
            success:function (resp) {
                console.log(resp);return ;
            }
        });
    })
});

$( document ).ajaxStart(function() {
    checkForPageBanners();
});

// Dummy hijacking back button of chrome
// (function (window, location) {
//     if (!document.referrer.includes(location.host)) {
//
//         var _url = location.toString().replace(location.hash, "");
//
//         history.replaceState(null, document.title, _url + "#!/history");
//         history.pushState(null, document.title, _url);
//
//         window.addEventListener("popstate", function () {
//             if (location.hash === "#!/history") {
//                 history.replaceState(null, document.title, location.pathname);
//                 setTimeout(function () {
//                     location.replace("/");
//                 }, 0);
//             }
//         }, false);
//     }
// }(window, location));
// ------------------------------------//