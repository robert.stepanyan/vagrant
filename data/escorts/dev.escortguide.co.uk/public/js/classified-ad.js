$( document ).ready(function() {
        //classified  ad slider
        var loadedPhotosCount = 0;
        var photosCount = $('.classified-ad-slider img').length;
       
        $('.classified-ad-slider img').on('load', function() {
          loadedPhotosCount++;
          if (loadedPhotosCount == photosCount) {
            
            $('.classified-ad-slider ').css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1}, 1000);
            classifiedAdSlider();
          }
        }).each(function() {
          if (this.complete) {
            $(this).trigger('load');
          }
        });

        function classifiedAdSlider() {
         
           $('.classified-ad-slider').slick({
            infinite: false,
            slidesToShow: 3,
            slidesToScroll: 2,
            centerPadding: '50px',
             responsive: [
              {
                breakpoint: 576,
                settings: {
                   centerPadding: '0px',
                  infinite: true,
                  centerMode: true,
                  slidesToShow: 1,
                  slidesToScroll: 1,
                
                  
                }
              },
            ]
         
         });
        }
        // classified ad slider end
        $('input[name="email"]').blur(function () {
            if(this.value.length == 0){
                $('input[name="phone"]').attr('required','required');
            }else{
                $('input[name="phone"]').removeAttr('required');
            }
        });
});
