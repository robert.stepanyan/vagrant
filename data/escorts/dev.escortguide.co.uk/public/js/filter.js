var Sceon = Sceon || {};
Sceon.firstLoad = true;

/* LOCATION */

Sceon.Location = (function(global, S, $) {

	'use strict';

	var 
		_primitiveParams = [
			'page', 'sort', 'reg', 'name', 'price_from', 'price_to',
			'age_from', 'age_to', 'f_incall', 'f_outcall',
			'video',  'review', 'is_online_now', 'verified', 'verified_contact', 'f_agency' , 'f_independent'
		],
		_sep = ';', _eq = '=', _enum = ',', _hashEvt = 'hashChanged';


	/**
	 * Sets the window location hash
	 * @param String hash
	 * @return Boolean result
	 */
	function setHash(hash) {

		if (hash && hash.length) {
			document.location.hash = hash;
			return true;
		}

		return false;

	}

	function replaceHash(key, val) {
		var hash = document.location.hash;

		if (!hash) {
			document.location.hash = key + _eq + val;
			return true;
		}

		var params = hash.replace('#', '').split(_sep);
	}

	/**
	 * Builds query string from hash
	 * @return String query string
	 */
	function getQuery() {

		var hash = document.location.hash.substring(1);
	
		if (!hash.length) {
			return {};
		}

		var 
			params = hash.split(_sep),
			filter = '';

		$.each(params, function(i, param) {

			var 
				pair = param.split(_eq),
				key = pair[0],
				val = pair[1];

			if (typeof val === 'undefined') return false;

			$.each(val.split(','), function(i, el) {
				filter += key + ($.inArray(key, _primitiveParams) ? '[]=' : '=') + el + '&';
			});
		});
		
		return filter;

	}

	/**
	 * Building hash from params and selects
	 * @return String hash text
	 */
	function buildHash(filters) {
		var 
			hash = '',
			map = {};

		$.each(filters, function(i, el) {
			var 
				key = i.replace('[]', '');

			if (typeof map[key] === 'undefined')
				map[key] = [];

			map[key].push(el);

		});

		for (var i in map) {
			hash += i + _eq + map[i].join(_enum) + _sep;
		}
		return hash;

	}

	function getHash() {
		return global.location.hash.replace('#', '');
	}

	function clearHash() {

		document.location.hash = '';

	}

	return {
		getHash: getHash,
		setHash: setHash,
		getQuery: getQuery,
		buildHash: buildHash
	};

})(window, Sceon, jQuery);


/* FILTER */

Sceon.Filter = (function(global, S, $) {
	var
		_params = S.pageParams,
		_hashEvt = 'hashChanged';

	/* Cities filter */

	function CitiesFilter($el, $dest) {
		return this.init($el, $dest);
	}

	CitiesFilter.prototype = {
		options: {
			showCount: 10
		},

		init: function($el, $dest) {
			this.$el = $el;
			this.$dest = $dest;
			this.cities = _params.cities;
			this.bindEvents();
		},

		bindEvents: function() {
			var _self = this;

			this.$el.on('keyup', function(e) {
				var val = $(this).val().toLowerCase();

				if (val.length < 1) {
					val = '';
					_self.$dest.hide();
					return false;
				}

				_self.filtered = [];

				$.each(_self.cities, function(i, city) {
					var
						title = city.city_title,
						startPos = title.toLowerCase().indexOf(val),
						endPos = startPos + val.length + 1;


					if (~startPos) {
						var highlighted = _self.highlightMatch(title, startPos, endPos);

						_self.filtered.push({
							slug: city.city_slug,
							title: highlighted,
							count: city.escort_count
						});
					}
				});

				_self
					.decorateOptions()
					.displayMatched();
			});


		},

		decorateOptions: function() {
			if (!this.filtered || !this.filtered.length) return this;

			var
				_self = this,
				part = this.filtered.slice(0, this.options.showCount);

			_self.rowHtml = '';

			$.each(part, function(i, city) {
				_self.rowHtml += [
				'<a class="dropdown-item" href="/escorts/city_gb_' + city.slug +  '" >',
					city.title,
					'<span> ' + city.count + '</span>',
				'</a>'
				].join('');

			});

			return this;
		},

		highlightMatch: function(str, startPos, endPos) {
			var splitted = str.split('');

			splitted.splice(startPos, 0, '<strong>');
			splitted.splice(endPos, 0, '</strong>');

			return splitted.join('');
		},

		displayMatched: function() {
			this.$dest.html(this.rowHtml.toString());
			this.$dest.show('slow');
		}
	};

	/* Escorts filter */

	function EscortsFilter($filterCont, props) {
		return this.init($filterCont, props);
	}

	EscortsFilter.prototype = {
		props: {},
		renderedAdvancedFilter:false,

		primitiveParams: [
			'page', 'sort', 'reg', 'name', 'price_from', 'price_to',
			'age_from', 'age_to', 'f_incall', 'f_outcall',
			'video',  'review', 'is_online_now', 'verified', 'verified_contact' ,'video','sort' , 'list_view', 'member_name','showname',
			'r_city','text','category','city','agency_sort','city_slug','city_id','f_gender','ajax_filtered'
		],

		escortsFilterUrl: '/escorts?ajax',

		controls: ['input','select[data-filter="true"]'],
		actionBtns: ['button.btn-primary'],
		resetBtns: ['button.btn-secondary'],

		tempFilter: {},

		init: function($filterCont, props) {
			//if data-object=false we are creating new object for advanced filter
			$('.advanced-search-button').attr("data-object","true");

			this.container = $filterCont;


			this
				.setProps(props)
				.parseHash()
				.loadData()
				.bindEvents();
			$('.main-filter > input, .main-filter > select').removeAttr('disabled')
		},

		setProps: function (props) {
			this.props = $.extend(this.props, props);
			return this;
		},

		parseHash: function() {
			var
				_self = this,
				filter = {},
				hash = Sceon.Location.getHash(),
				splittedHash = hash.split(';').filter(function(el) {
					return el !== '';
				});

				$.each(splittedHash, function(i, el) {
					var
						pair = el.split('='),
						key = !~$.inArray(pair[0], _self.primitiveParams) ? (pair[0] + '[]') : pair[0],
						val = !~$.inArray(pair[0], _self.primitiveParams) ? (pair[1].split(',')) : pair[1];

					filter[key] = val;
				});

			this.tempFilter = filter;

			return this;
		},

		loadData: function() {
            if (Object.keys(this.tempFilter).length > 0) this.renderTempFilter();

            // if(Sceon.firstLoad && location.pathname === '/' || location.hash !== ""){
                this.sendRequest();
            // }


			return this;
		},

		setFilterParam: function(pair) {
			if(pair.name === 'city_id'){
				this.changeUrl = true;
				this.citySlug = pair.value;
			}
			if (this.tempFilter[pair.name]) {
				if (!~$.inArray(pair.name, this.primitiveParams)) {
					this.tempFilter[pair.name].push(pair.value);
				} else {
					this.tempFilter[pair.name] = pair.value;

					if (this.tempFilter[pair.name] === '') {
						delete this.tempFilter[pair.name];
					}
				}
			} else {
				if (!~$.inArray(pair.name, this.primitiveParams)) {
					this.tempFilter[pair.name] = [pair.value];
				} else {
					this.tempFilter[pair.name] = pair.value;
				}
			}

			return this;
		},

		removeFilterParam: function(pair) {
			if (!~$.inArray(pair.name, this.primitiveParams)) {
				this.tempFilter[pair.name] = $.grep(this.tempFilter[pair.name], function(el) {
					return el != pair.value;
				});

				if (!this.tempFilter[pair.name].length) {
					delete this.tempFilter[pair.name];
				}
			} else {
				delete this.tempFilter[pair.name];
			}

			return this;
		},

		renderTempFilter: function($control) {
			var
				_self = this,
				$controls = this.container.find(this.controls.join(','));


			$.each($controls, function() {
				var $control = $(this);

				if ($control.is(':checkbox') || $control.is(':radio')) {
					if (!_self.tempFilter.review && !_self.tempFilter.video && !_self.tempFilter.verified_contact && !_self.tempFilter.verified)
					{
						$control.prop('checked', false);
					}
				} else if($control.is('select')){

				} else {
					$control.val('');
				}
			})

			$.each(this.tempFilter, function(name, values) {
				if (!~$.inArray(name, _self.primitiveParams)) {
					$.each(values, function(i, val) {

						var $el = _self.container.find('[name="' + name + '"][value="' + val + '"]');
						if ($el !== $control) $el.prop('checked', true);
					})
				} else {

					var $el = _self.container.find('[name="' + name + '"]');

					if ($el.attr('type') === 'text' || $el.attr('type') === 'number') {
						try	{
							values = decodeURI(values);
							$el.val(values);
						}catch(e) {
						}
					} else if($el.is('select')) {
						$el.find('option').each(function () {
							if($(this).val() === values){
                                $(this).prop('selected',true);
							}
                        });
					}else{
                        $el.prop('checked', true);
					}
				}
			});

			return this;
		},

		bindEvents: function() {

			var _self = this;
			this.container.on('change', this.controls.join(','), function(e) {
				e.preventDefault();
				_self.setAjaxFilteredField();
				var
					$control = $(this),
					pair = {
						name: $control.attr('name'),
						value: $control.val()
					};

				if ($control.attr('type') === 'checkbox' && !$control.is(':checked')) {
					_self.removeFilterParam(pair);
				} else {
					_self.setFilterParam(pair);
				}

				if($control.hasClass('view-change-trigger')){
					return false;
				}

				//Delete page param for set it 1
                delete _self.tempFilter['page'];

				if($control.attr('data-scenario') === 'trigger'){
					$('#do_search').click();
				}

				_self.renderTempFilter($control);
			});

			this.container.on('click', this.actionBtns.join(','), function() {
				var 
					$btn = $(this),
					hash = Sceon.Location.buildHash(_self.tempFilter);
					var pathname= location.pathname;

				if(pathname !=='/agencies' && _self.changeUrl && _self.citySlug){
					hash = hash.replace('city_id='+_self.citySlug+';', '')
				}
				Sceon.Location.setHash(hash);

				if($btn.attr('data-redirect')){
					window.location.href = '/'+location.hash;
				}else{
                    _self.sendRequest();
                }

			});

			this.container.on('click', this.resetBtns.join(','), function() {
				var $btn = $(this);

				if ($btn.data('role') && $btn.data('role') === 'reset') {
					var
						$parentModal = $btn.closest('.modal-content'),
						$wrapper = $parentModal.length ? $parentModal : this.$container,
						$controls = $wrapper.find( _self.controls.join(',') );

					var $controls_obj = [];

					$.each($controls, function() {
						var $control = $(this);

						switch( $control.attr('type') || $control.prop("tagName").toLowerCase() ) {
							case 'text':
								$control.val('');
								break;
							case 'checkbox':
								$control.prop( 'checked', false );
								break;
							case 'select':
								$control.prop('selectedIndex', 0);
								break;
							case 'radio':
								$control.prop('checked', false);

								break;
						}

						$controls_obj.push( $control );
					});
					_self.tempFilter = {};
				}

			});

			return this;
		},

		setAjaxFilteredField : function () {
			var _self = this;
			var manualPair = {
				name: 'ajax_filtered',
				value: 1
			};

			_self.setFilterParam(manualPair);
		},

        generateNewUrl : function () {
            this.hideCitiesList = true;
            var a = ["nuove", "independents", "agency", "agencies", "citytours", "videos", "boys", "trans", "happy-hours"];
            var pathname= location.pathname;
            pathname = (pathname === '' || pathname === '/') ? '/escorts' : pathname;
			var UrlArray =  pathname.split('/');
			var lastItem = UrlArray.pop();
			if(a.indexOf(lastItem) > -1){
                UrlArray.push(lastItem);
			}else if(pathname === '/escorts'){
				UrlArray.push(lastItem);
			}
			var filterUrlArray= this.props.filterUrl.split('?');


            if(this.citySlug === 'london' && lastItem !== 'london'){
                this.hideCityZonesList = false;
            }else if(this.citySlug !== 'london' && lastItem === 'london'){
                this.hideCityZonesList = true;
			}

            if (pathname !== '/agencies'){
				UrlArray.push(this.citySlug);
			}
            filterUrlArray[0] =UrlArray.join('/');
            this.props.filterUrl = filterUrlArray.join('?');
            var newUrl = UrlArray.join('/') + location.hash;
			return {newUrl:newUrl, oldCitySlug: lastItem};
        },

		generateNewTitle : function (oldCitySlug) {
            var title = document.title;
            var escortsInText = $("div.desktop-additional-filter h2").html();
            var metaDescriptionT = $('meta[name=description]');
            var metaDescription = metaDescriptionT.length >0 ? metaDescriptionT.attr('content') : '';
            var metaKeywordsT = $('meta[name=keywords]');
            var metaKeywords = metaKeywordsT.length >0 ? metaKeywordsT.attr('content') : '';

            var breadcrumbT = $('.breadcrumb-item.active');
            var breadcrumb = breadcrumbT.text();

            var oldSlugPartArr = oldCitySlug.split('-');
            oldSlugPartArr.forEach(function (value, index) {
                oldSlugPartArr[index] = value.charAt(0).toUpperCase() + value.slice(1)
            });
            oldCitySlug = oldSlugPartArr.join(' ');

            var newSlugPartArr = this.citySlug.split('-');
            newSlugPartArr.forEach(function (value, index) {
                newSlugPartArr[index] = value.charAt(0).toUpperCase() + value.slice(1)
            });
            this.citySlug = newSlugPartArr.join(' ');
            if(title.indexOf('London') >= 0 && oldCitySlug !== 'London'){
                title = title.replace('London', this.citySlug)
            }
            if(title.indexOf('escorts in UK') >= 0){
                title = title.replace('escorts in UK', 'escorts in '+this.citySlug)
            }
            title = title.replace(oldCitySlug, this.citySlug)
                .replace(oldCitySlug, this.citySlug);

            if (escortsInText && escortsInText.indexOf('close to you') > 0) {
                escortsInText = escortsInText.replace('close to you', 'in '+this.citySlug);
            }else if(escortsInText && escortsInText.indexOf('Escort Videos') > 0){
                escortsInText = escortsInText.replace('Escort Videos', 'Escort Videos in ' + this.citySlug);
			}
            else {
            	if (escortsInText)
				{
					escortsInText = escortsInText.replace(new RegExp(oldCitySlug, 'g'), this.citySlug);
				}
            }

            $("div.desktop-additional-filter h2").html(escortsInText);
            // replace meta keywords
			if(metaKeywordsT.length >0){
                metaKeywords = metaKeywords.replace(oldCitySlug, this.citySlug)
                    .replace(oldCitySlug, this.citySlug)
                    .replace(oldCitySlug, this.citySlug);
                metaKeywordsT.attr('content', metaKeywords);
			}


            // replace meta description
            if(metaDescriptionT){
                metaDescriptionT.attr('content', metaDescription.replace(oldCitySlug, this.citySlug));
			}
            // replace meta breadcrumb
            if(breadcrumbT.length > 0 && location.pathname !== '/agencies'){
            	breadcrumbT.text(breadcrumb.replace(oldCitySlug, this.citySlug))
			}
            return title;
        },

		sendRequest: function() {
			var _self = this;
			var newUrl = '';
			var oldCitySlug = '';
			if(_self.changeUrl && _self.citySlug.length > 2){
				var ajaxUrlObj = _self.generateNewUrl();
                newUrl = ajaxUrlObj.newUrl;
                oldCitySlug = ajaxUrlObj.oldCitySlug;
			}
			/*alert(location.pathname);
			alert(Sceon.firstLoad);*/
			if (location.pathname !== '/' || !Sceon.firstLoad || location.hash !== '')
			{
				if (location.pathname == '/')
				{
					this.tempFilter.isHomePage = "1";
				}

				$.ajax({
					url: this.props.filterUrl,
					method: 'GET',
					dataType: 'json',
					data: this.tempFilter,

					beforeSend: function() {
						if(!Sceon.firstLoad){
							_self.props.htmlArea.LoadingOverlay("show", {color : "rgba(255, 255, 255, 0.9)", zIndex: 1000});
						}
					},
					success: function(resp) {

						if (!resp) {
							console.warn('Error occured.');
							return false;
						}


						var $escortList = resp.escort_list;
						if(_self.props.page === 'main'){
							var $filterBody = resp.filter_body;
							$('#filter-container').html($filterBody).hide().fadeIn(1000);
						}else{
							_self.getAdvancedPage();
						}



						if(_self.props.htmlArea){
							if((Sceon.firstLoad && location.pathname === '/') || location.hash !== "" || newUrl.length > 0){
								var width = $( window ).width();
								var checkPagination = false;

								$.each(_self.tempFilter, function(key, val) {
									if(key == "page" && val != 1) {
										checkPagination = true;
									}
								});

								$('.scroll-page').LoadingOverlay("hide", true);

								if(width < 992 && _self.props.filterUrl.includes('escorts?ajax') && checkPagination) {
									// $('#pagination-m').hide();
									$('#escorts-sceleton').hide();

									if(_self.props.htmlArea.find("#escorts-list").length == 0) {
										_self.props.htmlArea.append($escortList);
									} else {
										var list = $($escortList);
										var pagition = list.find('#pagination-m').detach();
										_self.props.htmlArea.find("#escorts-list").append(list);
										$el = _self.props.htmlArea.find(".escorts-list").slice(1);
										$el.replaceWith($el.html());
										$('#pagination-m').replaceWith(pagition);
									}

									_self.props.htmlArea.find(".no-escort").hide();
								} else {
									$('.scroll-page').attr('id', 2 );
									_self.props.htmlArea.html('').append($escortList).hide().fadeIn(150);
								}
							}
						}

						$('#collapsesearch').collapse('hide');
						if(newUrl){
							var title =_self.generateNewTitle(oldCitySlug);
							history.replaceState(resp, title, newUrl);
							$('.scroll-page').hide();
							pagination.bindEvents();
							document.title = title;
							_self.changeUrl = false;
							_self.citySlug = null;
							if(_self.hideCitiesList){
								// $('.cities-list').hide();
								_self.hideCitiesList = false;
							}else{
								// $('.cities-list').show();
							}
							if(_self.hideCityZonesList){
								// $('.cities-list').hide();
								// $('.cities.zone').hide();
								_self.hideCitiesList = false
							}else{
								// $('.cities-list').show();
								// $('.cities.zone').show();
							}
						}

						loadBubbleTexts({page: 1, ajax: true});

					},

					error: function(resp) {
						_self.props.htmlArea.LoadingOverlay("hide", {color : "rgba(239, 243, 249, 0.80)", zIndex: 1000});
					}
				}).done(function () {
					if(!Sceon.firstLoad && _self.props.htmlArea) {
						_self.props.htmlArea.LoadingOverlay("hide");
					}
					Sceon.firstLoad = false;
				});
			}else{
				$.ajax({
					url: '/escorts/filters-load',
					method: 'get',
					dataType: 'json',
					data: this.tempFilter,

					beforeSend: function() {
						if(!Sceon.firstLoad){
							_self.props.htmlArea.LoadingOverlay("show", {color : "rgba(255, 255, 255, 0.9)", zIndex: 1000});
						}
					},
					success: function(resp) {

						if (!resp) {
							console.warn('Error occured.');
							return false;
						}

						if(_self.props.page === 'main'){
							var $filterBody = resp.filter_body;
							$('#filter-container').html($filterBody).hide().fadeIn(1000);
						}else{
							_self.getAdvancedPage();
						}

						$('#collapsesearch').collapse('hide');
						if(newUrl){
							var title =_self.generateNewTitle(oldCitySlug);
							$('.scroll-page').hide();
							pagination.bindEvents();
							document.title = title;
							_self.changeUrl = false;
							_self.citySlug = null;
							if(_self.hideCitiesList){
								// $('.cities-list').hide();
								_self.hideCitiesList = false;
							}else{
								// $('.cities-list').show();
							}
							if(_self.hideCityZonesList){
								// $('.cities-list').hide();
								// $('.cities.zone').hide();
								_self.hideCitiesList = false
							}else{
								// $('.cities-list').show();
								// $('.cities.zone').show();
							}
						}

					},

					error: function(resp) {
						_self.props.htmlArea.LoadingOverlay("hide", {color : "rgba(239, 243, 249, 0.80)", zIndex: 1000});
					}
				}).done(function () {
					if(!Sceon.firstLoad && _self.props.htmlArea) {
						_self.props.htmlArea.LoadingOverlay("hide");
					}
					Sceon.firstLoad = false;
				});
			}

		},

		getAdvancedPage :function () {
			if(this.renderedAdvancedFilter) return false;

			var _self = this;

			$.ajax({
				url:'/escorts/get-filter?ajax=true&filter_params=true',
				type:'POST',
				async:false,
				success:function (resp) {
					$('#collapsesearch').removeClass('filters').find('.advanced-search-button').attr('data-redirect','true');
                    $('#filter-container').html(resp).hide().fadeIn(1000);
                    _self.renderedAdvancedFilter = true;
                }
			});
        }
	};

	/* Pagination filter */

	function PaginationFilter(E) {
		return this.init(E);
	}

	PaginationFilter.prototype = {
		Escorts : {},
		Page : 1,

		init: function(E) {

        	this.Escorts = E;
			this.bindEvents();
		},

		bindEvents: function() {
			var _self = this;
			var width = $( window ).width();
			var tempPage = _self.Escorts.tempFilter.page;

			if(width < 992 && _self.Escorts.props.filterUrl.includes('escorts?ajax')) {
				$(document).on('appear', '.scroll-page', function(e, $affected) {
					if(tempPage != undefined) {
						var page = parseInt(tempPage) + 1;
						tempPage = undefined;
					} else {
						var page = parseInt( $('.scroll-page').attr('id'));
					}

					e.preventDefault();
					$('.scroll-page').LoadingOverlay("show", {color : "", zIndex: 1000   });
					$('.scroll-page').attr('id', page + 1 );

					_self.Page = page;
					var hash_link = S.Location.getHash();

	                var hash_obj = {};

	                var hash = hash_link.split(';').filter(function(el) {
	                    return el !== '';
	                });

	                $.each(hash, function(i, el) {
	                    var
	                        pair = el.split('='),
	                        key = !~$.inArray(pair[0], _self.Escorts.primitiveParams) ? (pair[0] + '[]') : pair[0],
	                        val = !~$.inArray(pair[0], _self.Escorts.primitiveParams) ? (pair[1].split(',')) : pair[1];

	                    hash_obj[key] = val;
	                });

	                var tmp_hash = '';
	                var obj_length = Object.keys(hash_obj).length;

	                if ( !obj_length ) {

	                	tmp_hash = 'page=' + _self.Page + ';';

	                } else if (('page' in hash_obj) && obj_length > 1) {

	                	tmp_hash = window.location.hash.replace(/\;page(.*?)=(.*?)\;/g, ';page='+ _self.Page+';');

	                } else if (('page' in hash_obj) && obj_length === 1) {

	                	tmp_hash = 'page=' + _self.Page + ';';

	                } else {

	                    tmp_hash = hash_link + 'page=' + _self.Page + ';';

	                }

	                S.Location.setHash(tmp_hash);

	                hash_obj['page'] = _self.Page;

	                _self.Escorts.tempFilter = hash_obj;

	                _self.Escorts.loadData();


	            });
			} else {
				$(document).on('click','.pagination a',function (e) {
					e.preventDefault();
					_self.Page = $(this).attr('data-page');
					var hash_link = S.Location.getHash();

					var hash_obj = {};

					var hash = hash_link.split(';').filter(function(el) {
						return el !== '';
					});

					$.each(hash, function(i, el) {
						var
							pair = el.split('='),
							key = !~$.inArray(pair[0], _self.Escorts.primitiveParams) ? (pair[0] + '[]') : pair[0],
							val = !~$.inArray(pair[0], _self.Escorts.primitiveParams) ? (pair[1].split(',')) : pair[1];

						hash_obj[key] = val;
					});

					var tmp_hash = '';
					var obj_length = Object.keys(hash_obj).length;

					if ( !obj_length ) {

						tmp_hash = 'page=' + _self.Page + ';';

					} else if (('page' in hash_obj) && obj_length > 1) {

						tmp_hash = window.location.hash.replace(/\;page(.*?)=(.*?)\;/g, ';page='+ _self.Page+';');

					} else if (('page' in hash_obj) && obj_length === 1) {

						tmp_hash = 'page=' + _self.Page + ';';

					} else {

						tmp_hash = hash_link + 'page=' + _self.Page + ';';

					}

					S.Location.setHash(tmp_hash);

					hash_obj['page'] = _self.Page;

					_self.Escorts.tempFilter = hash_obj;

					_self.Escorts.loadData();

					$('html,body').animate({scrollTop:0},500);
					$('#banner-975').hide();

					if (!$('.bubble-p').length && !$('.desktop-bubbles').length)
					{
						setTimeout(function () {
							var page = 1;
							var singleBubbleHeight = 102;
							var n = parseInt($('.bubble-texts').height() / singleBubbleHeight);
							$('.bubble-texts').LoadingOverlay("show", {color: "rgba(239, 243, 249, 0.80)", zIndex: 1000});
							$.ajax({
								url: '/bubble',
								type: 'POST',
								dataType: "html",
								data: {page: page, countToLoad: n, ajax: true},
								success: function (resp) {
									$('.bubble-texts').append(resp);
								},
								complete: function () {
									$('.bubble-texts').LoadingOverlay("hide", true);
								}
							});
						}, 1000);
					}
	            });
			}
		}
	};

	return {
		Cities: CitiesFilter,
		Escorts: EscortsFilter,
		Pagination: PaginationFilter
	};
})(window, Sceon, jQuery);

var Cities = new Sceon.Filter.Cities($('#citySearch'), $('#matchedCities'));
//var Escort = new Sceon.Filter.Escorts($('.filters'));


$(document).ready(function () {
    if(!$('button[data-object="false"]').length) return false;
    var Escort = new Sceon.Filter.Escorts($('.filters'), {
        filterUrl: "/escorts?ajax=true",
        page: 'another'
    });
});







// Cubix.Filter.Set = function (filter) {
		
// 	Cubix.LocationHash.Set(Cubix.LocationHash.Make(filter));

// 	return false;
// };

// /* --> HashController */
// Cubix.HashController = {
// 	_current: '',
	
// 	init: function () {
// 		setInterval('Cubix.HashController.check()', 100);
// 	},
	
// 	check: function () {
// 		var hash = document.location.hash.substring(1);
		
// 		if (hash != this._current) {
// 			this._current = hash;
			
					
// 			var data = Cubix.LocationHash.Parse();
// 			//Cubix.Filter.getForm(data);
// 			Cubix.Escorts.Load(hash, data/*, Cubix.HashController.Callback*/);
// 		}
// 	}
// };

// Cubix.HashController.Callback = function () {
// 	Cubix.Filter.Change(Cubix.LocationHash.Parse());
// 	Cubix.Filter.Set(Cubix.LocationHash.Parse(), true);
// }
/* <-- */