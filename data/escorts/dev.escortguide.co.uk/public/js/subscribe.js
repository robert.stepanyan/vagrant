$( document ).ready(function() {
	$('body').on('submit','.subscribe-newsletter',function( event ) {
		event.preventDefault();
		var form = $(event.target).parent('form');
		var sub_email = $(event.target).find('.subscribe-email').val();
		var subscribe_box = $(event.target).parent('.subscribe-newsletter-box');
		subscribe_box.LoadingOverlay("show", {color : "rgba(239, 243, 249, 0.80)", zIndex: 1000   });

		$.ajax({
	        url: '/index/email-collecting/',
	        type: 'POST',
	        data: {email: sub_email}, 
	        success: function(response){
	         subscribe_box.LoadingOverlay("hide", true);
	          try {
	          	response = JSON.parse(response);
	            if(typeof response =='object' && response['status'] == 'error' ){
	              $.each( response['msgs'], function( key, value ) {
	                subscribe_box.html('<div class=\"alert mb-0 alert-danger\" >'+ value +'</div>');
	              });
	           }
	           if(response['status'] == 'success'){
	            subscribe_box.html('<div class=\"alert mb-0 \" >'+ response['msg']+'</div>');
	           
	           }                    
	        }
	        catch (e) {
	        	
	        }
	        
	        }
	      })
	});
})