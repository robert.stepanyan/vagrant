var Punter = {

    init: function () {
        this.initForm();
        this.initPunterAnswer();
    },

    initPunterAnswer: function () {
    	$('.post-answer').on('click', function(event){
            var $el = $(this);
            var message = $el.siblings('.my-answer-text').val();
    		var parent = $el.siblings('.my-answer-text').data('id');
            if(message == ''){
                Notify.alert('danger', "Please write your answer!!!");
                return; 
            }
            
            $.ajax({
                url: '/private/new-answer',
                type: 'POST',
                data: {
                    'message': message,
                    'parent' : parent
                },
                beforeSend:function () {
                    $('#ui-view').LoadingOverlay("show", true);
                },
                success: function (response) {
                    var resp = JSON.parse(response);
                    if(resp.success){
                        $('#ui-view').LoadingOverlay("hide", true);
                        Notify.alert('success', "Your answer has been posted!");
                        $el.closest('.punter-div').find('a').prepend('<span class="my-answer">[Answered] </span>');
                        $el.closest('div').addClass('my-answer-message');
                        $el.closest('div').html(message)
                    } else {
                        $('#ui-view').LoadingOverlay("hide", true);
                        Notify.alert('danger', "Something went wrong!");
                    }

                }
          });
    	})
        
    },


    initForm:function () {
        $('#punter-form').submit(function (event) {
            event.preventDefault();
            var data = $(this).serializeArray();

            $.ajax({
                url: '/private/new-punter',
                type: 'POST',
                data: data,
                beforeSend:function () {
                    $('#questionModal').LoadingOverlay("show", true);
                },
                success: function (response) {
                    // $('#ui-view').html(data);
                    var resp = JSON.parse(response);
                    if(resp.success){
                        $('#questionModal').LoadingOverlay("hide", true);
                        $('#questionModal').modal("hide");
                        Notify.alert('success', "Your question will be posted after our admins aprove it!");
                    } else {
                        $('#questionModal').LoadingOverlay("hide", true);
                        Notify.alert('danger', "Something went wrong!");
                    }

                }
          });
      });

    },


};


$(document).ready(function () {
   Punter.init();
});

var loadedPhotosCount = 0;
  var photosCount = $('.pImg').length;
 
  $('.pImg').on('load', function() {
    loadedPhotosCount++;
    if (loadedPhotosCount == photosCount) {
      initVideoSlider();
      initEscortsSlider();
      
    }
  }).each(function() {
    if (this.complete) {
      $(this).trigger('load');
    }
  });



function sliderFullscreenOpen(arrow, element) {
  arrow.addClass('fullscreen-out');
    $('.pSlider').addClass('fullscreen-slider');   
    $('html,body').css({'overflow': 'hidden'});
    $('body').css({'touch-action':'auto'})
}

function sliderFullscreenClose(arrow) {
  arrow.removeClass('fullscreen-out');

  $('.pSlider').removeClass('fullscreen-slider');
  $('html,body').removeAttr('style');
}

function sliderNavigate(slider) {
  var index = slider.slick('slickCurrentSlide');
  slider.slick('slickGoTo', index, true);
}


function initVideoSlider() {

    var video_slider = $('#new-videos-slider .pSlider').slick({
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        centerMode: true,
        variableWidth: true
    });

    $('new-videos-slider .slick-slide').click(function() {
     
   
        var index = $(this).data("slick-index");
        var current = video_slider.slick('slickCurrentSlide');
        var last = video_slider.slideCount - 1;

        if( current === 0 && index === last ) {
           video_slider.slick('slickGoTo', -1);
        } else if ( current === last && index === 0 ) {
           video_slider.slick('slickGoTo', last + 1);
        } else {
           video_slider.slick('slickGoTo', index);
        }

    });

  /*$('.slick-slide').on('mousedown', function(edown) {
    if (edown.button == 0 && 
      !$('.fullscreenToggle').hasClass('fullscreen-out')) {

      var handle;

      $('.slick-slide').on('mouseup', handle = function(eup) {
        $('.slick-slide').off('mouseup', handle);

        if (Math.abs(edown.screenX - eup.screenX) < 10 && 
          Math.abs(edown.screenY - eup.screenY) < 10) {
            sliderFullscreenOpen($('.fullscreenToggle'), $(".slick-current"));
            sliderNavigate(slider);
        }
      });
    }
  });*/

   /* $(window).on('keydown', function(e) {
        if (e.keyCode == 37) {
            slider.slick('slickPrev');
        }

        if (e.keyCode == 39) {
            slider.slick('slickNext');
        }

        if (e.keyCode == 27 && $('.fullscreenToggle').hasClass('fullscreen-out')) {
            sliderFullscreenClose($('.fullscreenToggle'));
            sliderNavigate(slider);
        }
    });*/

    $(window).on('scroll' , function () {
        if(window.pageYOffset > 300){
            $('.back_to_top').removeClass('d-none').delay(1000);
        }else{
            $('.back_to_top').addClass('d-none').delay(1000);
        }
    });

    $('#new-videos-slider').bind('DOMMouseScroll', function(e){
        console.log('test');
        if(e.originalEvent.detail > 0) {
            video_slider.slick('slickNext');
        }else {
            video_slider.slick('slickPrev');
        }
     
       return false;
   });

   //IE, Opera, Safari
   $('#new-videos-slider').bind('mousewheel', function(e){
    console.log('test');
        if(e.originalEvent.wheelDelta < 0) {
          video_slider.slick('slickNext');
        }else {
            video_slider.slick('slickPrev');
        }
      
       return false;
   });


    $('.fullscreenToggle, .escort-photos .img-fluid').click(function() {
        if (!$('.fullscreenToggle').hasClass('fullscreen-out')) {
            sliderFullscreenOpen($('.fullscreenToggle'), $(".slick-current"));
            sliderNavigate(video_slider);
            $('#new-videos-slider').removeClass('hidden-lg-up');
            if($(this).data('slidePos')){
                video_slider.slick('slickGoTo', $(this).data('slidePos')-1);
            }
        } else {
            $('#new-videos-slider').addClass('hidden-lg-up');
            sliderFullscreenClose($('.fullscreenToggle'), $(".slick-current"));
            sliderNavigate(video_slider);
        }
    });
    video_slider.slick('slickNext');
}


function initEscortsSlider() {

    var escort_slider = $('#new-escorts-slider .pSlider').slick({
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        centerMode: true,
        variableWidth: true
    });

    $('#new-escorts-slider .slick-slide').click(function() {
     
   
        var index = $(this).data("slick-index");
        var current = escort_slider.slick('slickCurrentSlide');
        var last = escort_slider.slideCount - 1;

        if( current === 0 && index === last ) {
           escort_slider.slick('slickGoTo', -1);
        } else if ( current === last && index === 0 ) {
           escort_slider.slick('slickGoTo', last + 1);
        } else {
           escort_slider.slick('slickGoTo', index);
        }

    });

  /*$('.slick-slide').on('mousedown', function(edown) {
    if (edown.button == 0 && 
      !$('.fullscreenToggle').hasClass('fullscreen-out')) {

      var handle;

      $('.slick-slide').on('mouseup', handle = function(eup) {
        $('.slick-slide').off('mouseup', handle);

        if (Math.abs(edown.screenX - eup.screenX) < 10 && 
          Math.abs(edown.screenY - eup.screenY) < 10) {
            sliderFullscreenOpen($('.fullscreenToggle'), $(".slick-current"));
            sliderNavigate(slider);
        }
      });
    }
  });*/

   /* $(window).on('keydown', function(e) {
        if (e.keyCode == 37) {
            slider.slick('slickPrev');
        }

        if (e.keyCode == 39) {
            slider.slick('slickNext');
        }

        if (e.keyCode == 27 && $('.fullscreenToggle').hasClass('fullscreen-out')) {
            sliderFullscreenClose($('.fullscreenToggle'));
            sliderNavigate(slider);
        }
    });*/

    $(window).on('scroll' , function () {
        if(window.pageYOffset > 300){
            $('#new-escorts-slider .back_to_top').removeClass('d-none').delay(1000);
        }else{
            $('#new-escorts-slider .back_to_top').addClass('d-none').delay(1000);
        }
    });

    $('#new-escorts-slider').bind('DOMMouseScroll', function(e){
        console.log('test');
        if(e.originalEvent.detail > 0) {
            escort_slider.slick('slickNext');
        }else {
            escort_slider.slick('slickPrev');
        }
     
       return false;
   });

   //IE, Opera, Safari
   $('#new-escorts-slider').bind('mousewheel', function(e){
    console.log('test');
        if(e.originalEvent.wheelDelta < 0) {
          escort_slider.slick('slickNext');
        }else {
            escort_slider.slick('slickPrev');
        }
      
       return false;
   });


    $('.fullscreenToggle, .escort-photos .img-fluid').click(function() {
        if (!$('.fullscreenToggle').hasClass('fullscreen-out')) {
            sliderFullscreenOpen($('.fullscreenToggle'), $(".slick-current"));
            sliderNavigate(escort_slider);
            $('#new-escorts-slider').removeClass('hidden-lg-up');
            if($(this).data('slidePos')){
                escort_slider.slick('slickGoTo', $(this).data('slidePos')-1);
            }
        } else {
            $('#new-escorts-slider').addClass('hidden-lg-up');
            sliderFullscreenClose($('.fullscreenToggle'), $(".slick-current"));
            sliderNavigate(escort_slider);
        }
    });
    escort_slider.slick('slickNext');
}
