function setMapMarker(locations, changeEscortLocations = true) {
    escortMap._markers[1]?.remove();
    escortMap._markers[0]?.remove();

    if (locations) {
        let el = document.createElement('div');
        el.className = 'marker';
        el.id = 'marker1';
        el.style.backgroundImage =`url(${escortImage})`;
        el.style.width = '40px';
        el.style.height = '40px';
        el.style.borderRadius = '50%';
        el.style.backgroundSize = 'contain';

        marker = new mapboxgl.Marker(el);
        marker.setLngLat([locations.lng, locations.lat]).addTo(escortMap);
        if (changeEscortLocations === true) {
            $('#longitude').val(locations.lng);
            $('#latitude').val(locations.lat);
        }
    }
}

function initializeMap() {
    escortImage = $('.img-avatar').attr('src');
    escortLng = $('#longitude').val();
    escortLat = $('#latitude').val();
    escortCityLng = $('#base-city-longitude').val();
    escortCityLat = $('#base-city-latitude').val();
    defaultLng = -0.12764739999999997; // London lng
    defaultLat = 51.507321899999994; // London lat

    bounds = [
        [-11.05, 49.98], // Southwest coordinates UK
        [1.9, 60.871651] // Northeast coordinates UK
    ];

    mapboxgl.accessToken = 'pk.eyJ1IjoibW1hcHNlZ3VrIiwiYSI6ImNreTl3aHVkdTBhbDkzMm8wajR1dnNkMGwifQ.1QDoqucHypAcGuQI6dmiIw';
    escortMap = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v11',
        center: [escortLng || escortCityLng || defaultLng, escortLat || escortCityLat || defaultLat],
        zoom: 12,
        maxBounds: bounds // Set the map's geographical boundaries.
    });

    if (escortLng && escortLat) {
        setMapMarker({lng: escortLng, lat: escortLat});
    } else if (escortCityLng && escortCityLat) {
        setMapMarker({lng: escortCityLng, lat: escortCityLat}, false);
    }

    // Add the control to the window.escortMap.
    escortMap.addControl(
        new MapboxGeocoder({
            accessToken: mapboxgl.accessToken,
            placeholder: 'Search with place name',
            mapboxgl: mapboxgl,
            countries: 'gb', // United Kingdom of Great Britain and Northern Ireland
        })
    );

    escortMap.on('click', function(e) {
        setMapMarker(e.lngLat);
    }).on('moveend', e => {
        if (!e.hasOwnProperty('originalEvent')) {
            setMapMarker(escortMap._markers[1]?._lngLat);
        }
    });
}

$().ready( () => {
    initializeMap();
});