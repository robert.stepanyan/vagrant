'use strict';

var Cubix = Cubix || {};

Cubix.SelfCheckout = (function(global) {
  var ismobile = $(document).width() < 500 ? true : false;
  var Bootstrap = function() {};
  Bootstrap.prototype = (function() {
    function run (userType, data) {
      var stateController = new StateController(userType, data);
      new ViewRenderer(stateController);
    }

    return {
      run: run
    };
  })();

  var StateController = function(userType, data) {
    return this.construct(userType, data);
  };

  StateController.prototype = {
    construct: function(userType, data) {
      //UK TIME
      moment.tz.setDefault("Europe/London");

      this.userType = userType;
      this.pageView = 1;

      if (userType === 'escort') {
        this.userData = data.escort;
        this.premium_packages = data.escort_packages;
        this.status_owner_disabled = 8;
        this.verified = data.escort.is_verified;
      }

      if (userType === 'agency') {
        this.searchTerm = '';
        this.escortsPage = 1;
        this.escortsPerPage = 10;
        this.userData = data.agency;
        this.escorts = data.escorts.reduce(function(a, escort) {
          a[escort.id] = escort;
          return a;
        }, {});
        this.visibleEscorts = data.escorts;
        this.agencyDiscounts = data.agency_discounts.reduce(function(a, obj) {
          var o = {};

          o.range = [obj.value_from, obj.value_to];
          o.discount = obj.discount;

          return a = a.concat(o);
        }, []);
      }

      this.available_packages = data.available_packages;
      if (data.activePaymentmethodes)
      {
        this.activePaymentmethodes = data.activePaymentmethodes;
      }
      this.cart = {};
    },
  };

  var _draw_notification = function(reason){
    var $main_container = $('#ui-view');
    
    switch(reason) {
        case 'owner-disabled':
             $main_container.html('<div class="alert alert-danger" role="alert">Your profile is owner disabled. In order to continue, you must activate your ad</div><button id="profile-status-activate" class="btn m-auto btn-outline-success btn-lg active fixed-button">Activate now</button>')
                $('#profile-status-activate').on('click', function(){
                $(".main").LoadingOverlay("show", {color : "rgba(239, 243, 249, 0.80)", zIndex: 1000   });
                
                $.ajax({
                 type: "GET",
                 url: "private/profile-status?act=enable",
                 success: function(response){
                    if(response){
                     $main_container.html('<div class="alert alert-success" role="alert">Your account has been successfully activated.</div><button onClick="window.location.reload()"  class="btn m-auto btn-outline-success btn-lg active fixed-button">Upgrade now</button>')
                    }else{
                     $main_container.html('<div class="alert alert-danger" role="alert">Unexpected error please contact support</div>')
                    }
                 },
                 complete: function(e){
                   $(".main").LoadingOverlay("hide", true);
                 }
              })
            })
            break;
        case 'escort-gender':
          $main_container.html('<div class="alert alert-danger" role="alert">You can not buy a package. Please contact support</div><button data-ajax-url="/private/support#index" class="btn m-auto btn-outline-success btn-lg active fixed-button">Support</button>')
        break;
        case 'complete_7_steps':
          $main_container.html('<div class="alert alert-danger" role="alert">In order to upgrade your ad, you need to complete your profile</div><button data-ajax-url="/private/profile#index" class="btn m-auto btn-outline-success btn-lg active fixed-button">Complete profile</button>')
        break;
        case 'escorts_no_found':
          $main_container.html('<div class="alert alert-danger" role="alert">In order to upgrade your ads, you need to add profiles</div><button data-ajax-url="/private/profile#index" class="btn m-auto btn-outline-success btn-lg active fixed-button">Add Profile</button>')
        break;
         
        default:
            console.log('nothing');
    }
  };

  var Cart = (function () {
    var instance;

    function createInstance() {
      var obj = {};
      obj.premium_packages = {};
      obj.dateCreated = Date.now();

      return obj;
    }

    return {
      getInstance: function () {
        if (!instance) {
          instance = createInstance();
        }
        return instance;
      }
    };
  })();

   var CartItemBuilder = function() {
     return this.construct();
   };

   CartItemBuilder.prototype = {
     construct: function() {
      return this;
     },
     setAvailablePackages: function(premium_packages) {
       this.available_packages = premium_packages;

       return this;
     },

     setEscort: function(id) {
       this.escortId = id;

       return this;
     },

     setType: function(type) {

      this.type = type;

      return this;
     },

     setGender: function(gender) {
       this.gender = gender;
       return this;
     },

     setDuration: function(duration) {
      this.duration = duration;

      return this;
     },

     setActivationDate: function(date) {
      this.activationDate = date;

      return this;
     },

     setEscortPhoto: function (photo) {
       this.escortPhoto = photo;

       return this;
     },


    setBaseCityId: function(cityId) {
    this.baseCityId = cityId;

    return this;
    },

     setShowname: function(showname) {
      this.showname = showname;
     },

     getAmount: function() {
      if(this.type == 'MonthlyPlus') {
          var _name = this.type;
      } else {
          var _name = this.type + ' ' + this.duration;
      }
      var _gender = this.gender;

      var premium_packages = this.available_packages.filter(function(p) {

        if (p.name.indexOf(_name) >= 0) {
          return p;
        }
      });
       return premium_packages[0].price;
     },

     getForCart: function() {
      if(this.type == 'MonthlyPlus') {
          var _name = this.type;
          this.duration = 30;
      } else {
          var _name = this.type + ' ' + this.duration;
      }

      var _gender = this.gender;
      var premium_package = this.available_packages.filter(function (p) {
         if (p.name.indexOf(_name) >= 0) {
           return p;
         }
       });
      
       return {
         type: this.type,
         duration: this.duration,
         premium_cities: [this.baseCityId],
         escortId: this.escortId,
         showname: this.showname,
         packageId: premium_package[0].id,
         activation_date: this.activationDate,
         price: premium_package[0].price,
         photo: this.escortPhoto
       };
     }
   };

  var ViewRenderer = function(state) {
    return this.construct(state);
  };

  ViewRenderer.prototype = {
    construct: function(state) {
      var vr = this;
      vr.state = state;
      vr._draw();
    },

    _draw: function() {
      var vr = this;

      var $main_container = $('#ui-view');
      $main_container.html( '');

      if (vr.state.userType === 'escort') {
        var escortView = vr._drawEscortView();
        $main_container.append(escortView);
        vr._bindEvents();
      } else if (vr.state.userType === 'agency') {
        var agencyView = vr._drawAgencyView();
       
        $main_container.append(agencyView);
        vr._bindAgencyEvents();
      }
    },

    _drawEscortView: function() {
      var vr = this;
      var $container = $('<div/>', {
        class: "self-checkout-container row no-gutters"
      });

      /** PAGE 1 */
      var $page1 = $('<div/>', {
        'data-component': 'page',
        'data-id': 1,
        'data-name': 'escortslist',
        'class': 'page card col-12',
        'css': {
          'display': this.state.pageView !== 1 ? 'none' : 'block'
        }
      });

      var $headerBar = $('<div/>', {
        'data-component': 'bar',
        'data-name': 'headerbar',
        'class': 'bar card-header'
      });

      var $title = $('<h2/>', {
        'text': 'Your',
        'data-component': 'heading',
        'data-name': 'title',
        'class': 'title'
      });

      $headerBar.append($title);

      var $escortsList = $('<div/>', {
        'data-component': 'list',
        'data-name': 'escortslist',
        'class': 'escorts-list-wrapper'
      });

      var $premium_packageItem = this._drawpremium_packageItem();
      $escortsList.append($premium_packageItem);
      // https://sceonteam.atlassian.net/browse/EGUK-319 unseting $headerBar by task request.
      // If u needed back, add it in append function below.
      $page1.append($escortsList);

      /** PAGE 2 */
      var $page2 = $('<div/>', {
        'data-component': 'page',
        'data-id': 2,
        'data-name': 'cart',
        'class': 'page col-12 p-static',
        'css': {
          'display': this.state.pageView !== 2 ? 'none' : 'block'
        }
      });

      var $titleBar = $('<div/>', {
        'data-component': 'bar',
        'data-name': 'titlebar',
        'class': 'bar--inverted card-header'
      });

      var $title = $('<div/>', {
        'text': 'Review your cart',
        'class': 'row-custom title',
      });
      $titleBar.append($title);

      var $cartTable = $('<div/>', {
        'class': 'table cart-review agency-cart-review agency-cart-review-escort'
      });

      var $tableHead = $('<div/>', {
        'class': 'head custom-data-row data-row-escort'
      });

      $.each([  'Start Date', 'Duration', 'Price', 'Options'],function(idx,el) {
        $tableHead.append($('<div/>', {
          'class': 'td custom-td',
          'text': el
        }))
      });

      var _buildTable = function (){
        if (vr.state.cart.premium_packages && Object.keys(vr.state.cart.premium_packages).length) {
          $.each(vr.state.cart.premium_packages, function(idx, p) {

            var $tableRow = $('<div/>', {
              'class': 'custom-data-row data-row-4 body'
            });
            var fixedDate = new Date(p.activation_date);

            if (ismobile){

              $tableRow.append($('<div/>', {
                'class': 'td custom-td',
                'html': '<span class="light-gray"> Package </span>' + '<span class="dark-text">' + p.type + '</span>'
              }));

              $tableRow.append($('<div/>', {
                'class': 'td custom-td',
                'html': '<span class="light-gray"> Activation date </span>' + '<span class="dark-text">' + (p.activation_date ? fixedDate.getDate() + 1 + ' ' + fixedDate.toLocaleString('default', { month: 'short' }) + ' ' + fixedDate.getFullYear() : 'ASAP') + '</span>'
              }));

              $tableRow.append($('<div/>', {
                'class': 'td custom-td',
                'html': '<span class="light-gray"> Duration </span>' + '<span class="dark-text">' + p.duration+' Days' + '</span>'
              }));

              $tableRow.append($('<div/>', {
                'class': 'td custom-td',
                'html': '<span class="light-gray"> Price </span>' + '<span class="dark-text">' +  p.price +' GBP' + '</span>'
              }));

              $tableRow.append($('<div/>', {
                'data-actions-escort-id': p.escortId,
                'class': 'td action-buttons td actions',
                'html': '<div class="action-buttons-item button--edit" data-id="' + idx + '" > <img src="/img/button--edit.png" alt="edit">' + '<span class="ml-2"> Edit </span>' + '</div>' +
                    '<div class="action-buttons-item button--delete" data-id="' + idx + '" > <img src="/img/button--delete.png"  alt="remove">' + '<span class="ml-2"> Remove </span>' + '</div>'
              }));

            }
            else if (!ismobile) {
              $tableRow.append($('<div/>', {
                'class': 'td custom-td',
                'text': p.activation_date ? fixedDate.getDate() + 1 + ' ' + fixedDate.toLocaleString('default', { month: 'short' }) + ' ' + fixedDate.getFullYear() : 'ASAP'
              }));

              $tableRow.append($('<div/>', {
                'class': 'td custom-td',
                'text': p.duration + ' Days'
              }));

              $tableRow.append($('<div/>', {
                'class': 'td custom-td',
                'text': p.price + ' GBP'
              }));

              $tableRow.append($('<div/>', {
                'class': 'td action-buttons td actions ',
                'html': '<div id="edit-cart-item" class="action-buttons-item"> <img src="/img/button--edit.png" class="button--edit" data-id="' + idx + '" alt="edit">' + '</div>' +
                    '<div class="action-buttons-item button--delete"> <img src="/img/button--delete.png" class="button--delete" data-id="' + idx + '" alt="remove">' + '</div>'
              }));
            }
            var $banner = $('<div/>', {
              'html': '<div class="discount-title">' + 'DISCOUNT' + '</div>' +
                  '<div class="discount-content">' + 'You have one premium ad in your cart, you can now purchase a banner for only 30 GBP/30 days!\n ' + '</div>' +
                  '<div class="discount-inputs">' +
                  '<div class="discount-input">' +
                  '<input type="radio" id="get-discount" name="radio" value="" class="get-discount mr-2"/><label for="get-discount"> Get the discount banner' + '</label>' +
                  '</div>' +
                  '<div class="discount-input">' +
                  '<input type="radio" id="not-now" checked="checked" name="radio" value="0" class="not-now ml-3 mr-2"/><label for="not-now"> Not now' + '</label>' +
                  '</div>' +
                  '</div>'
              ,
              'class': 'discount-banner'
            });
            $cartTable.append($tableRow, $banner)
          });
        }
      }

      $cartTable.append($tableHead);
      _buildTable();

      var $cartFooter = $('<div/>', {
        'data-component': 'footer',
        'data-name': 'cartfooter',
        'class': 'footer--cart escort-footer--cart d-flex',
      });

      if (this.state.cart.premium_packages && Object.keys(this.state.cart.premium_packages).length) {
        var totalAmount = Object.values(this.state.cart.premium_packages).reduce(function(a, el) {
          a += +el.price;

          return a;
        }, 0);

      }
      var $this = this;

      var $promoInput = $('<div/>', {
        class: 'promo-input',
        'html': '<input type="text" name="promo_code" class="promo-code form-control" id="promo-code" placeholder="Promo Code">'
      });

      var $promoCheck = $('<div/>', {
        class: 'promo-button',
        'html': '<button type="button" name="promo_check" class="promo-check btn btn-info" id="promo-code" >APPLY</button>'
      }).on('click', function() {
        var $promoCode = $('#promo-code').val();

        $.ajax({
          url: 'private/billing/check-promo-code',
          method: 'POST',
          dataType: 'json',
          data: {
            promo_code: $promoCode
          },
          success: function(resp) {
            if (resp.status == 'success') {
              var cartData = Object.values(vr.state.cart.premium_packages);

              $('#promo-code').css('border', '3px solid lightgreen')
              $('#promo-code').css('border-radius','3px');
              if ($('.promo-value').length)
              {
                $('.promo-value').remove();
              }
              if (resp.promo_data.amount)
              {
                var cartTotal = 0;
                $.each(cartData, function(index, el) {
                  cartTotal += cartData[index].price
                });
                if (cartTotal > resp.promo_data.amount)
                {
                  var writeOffPricePerCartItem = resp.promo_data.amount/cartData.length;

                  if (cartData.length > 1)
                  {
                    $.each(cartData, function(index, el) {
                      cartData[index].price = cartData[index].price - writeOffPricePerCartItem;
                    });
                  }else{
                    $.each(cartData, function(index, el) {
                      cartData[index].price = ((cartData[index].price - writeOffPricePerCartItem) >= 0)?cartData[index].price - writeOffPricePerCartItem : 0;
                    });
                  }
                }else{
                  if ($('.promo-value').length)
                  {
                    $('.promo-value').remove();
                  }
                  $('#promo-code').css('border', '3px solid red');
                  $('#promo-code').css('border-radius','3px');
                  setTimeout(() => {$('#promo-code').css('border', '3px solid transparent')},200)
                  setTimeout(() => {$('#promo-code').css('border', '3px solid red')},500)
                  setTimeout(() => {$('#promo-code').css('border', '3px solid transparent')},700)
                  setTimeout(() => {$('#promo-code').css('border', '3px solid red')},900)
                  setTimeout(() => {alert("Cart total amount should exceed promo code amount")},1200)
                  return;
                }
              }else if (resp.promo_data.percent)
              {
                $.each(cartData, function(index, el) {
                  cartData[index].price = cartData[index].price - (cartData[index].price * resp.promo_data.percent)/100;
                });
              }else if(resp.promo_data.days){
                var additionalDaysPerCartItem;
                if (cartData.length > 1)
                {
                  if (resp.promo_data.days % cartData.length)
                  {
                    additionalDaysPerCartItem = resp.promo_data.days/cartData.length;
                    $.each(cartData, function(index, el) {
                      cartData[index].duration = cartData[index].duration + '+' + additionalDaysPerCartItem;
                    });
                  }else{
                    additionalDaysPerCartItem = (resp.promo_data.days - 1)/cartData.length;
                    $.each(cartData, function(index, el) {
                      if (index === 1)
                      {
                        cartData[index].duration = cartData[index].duration + '+' + additionalDaysPerCartItem + 1;
                      }else{
                        cartData[index].duration = cartData[index].duration + '+' + additionalDaysPerCartItem;
                      }
                    });
                  }
                }else{
                  $.each(cartData, function(index, el) {
                    cartData[index].duration = cartData[index].duration + '+' + resp.promo_data.days;
                  });
                }
              }
              $cartTable.empty();

              var $tableHead = $('<div/>', {
                'class': 'head custom-data-row data-row-escort'
              });

              $.each([  'Start Date', 'Duration', 'Price', 'Options'],function(idx,el) {
                $tableHead.append($('<div/>', {
                  'class': 'td custom-td',
                  'text': el
                }))
              });

              $cartTable.append($tableHead);
              _buildTable();
              var total_amount = Object.values(vr.state.cart.premium_packages).reduce(function(a, el) {
                a += +el.price;

                return a;
              }, 0);

              var $total_amount = $('<div/>', {
                class: 'align-self-center',
                'html': '<div class="total-amount-data total-amount-data-escort">Total: <strong>' + (total_amount ? (total_amount + '.00') : 0) + ' GBP' +'</strong></div>'
              });
              $('div.total-amount-data').replaceWith($total_amount);
              $('button.promo-check').attr('disabled','disabled');
            }else{
              if ($('.promo-value').length)
              {
                $('.promo-value').remove();
              }
              $('#promo-code').css('border', '3px solid red');
              $('#promo-code').css('border-radius','3px');
              setTimeout(() => {$('#promo-code').css('border', '3px solid transparent')},500)
              setTimeout(() => {$('#promo-code').css('border', '3px solid red')},800)
              setTimeout(() => {$('#promo-code').css('border', '3px solid transparent')},1000)
            }
          }
        });
      });

      var $promoSection = $('<div/>', {
        class: 'promo-container'
      });

      $promoSection.append($promoInput,$promoCheck);

      var $totalAmount = $('<div/>', {
        class: 'align-self-center',
        'html': '<div class="total-amount-data total-amount-data-escort">Total: <strong>' + (totalAmount ? (totalAmount + '.00') : 0) + ' GBP' +'</strong></div>'
      });


      var $paymentMethod = $('<div/>', {
        class: 'payment_method_block d-none'
      });

      if (vr.state.activePaymentmethodes && vr.state.activePaymentmethodes.indexOf("mmg") >= 0) {
        $paymentMethod.append($('<div/>', {
          'data-name': 'payment_method',
          'class': 'td sc-input-block',
          'html': '<input type="radio" id="payment_method_mmgbill" name="payment_method" value="mmgbill" class="payment_method"/><label for="payment_method_mmgbill">Credit/Debit card (MMGbill)'
        }));
      }
      if (vr.state.activePaymentmethodes && vr.state.activePaymentmethodes.indexOf("psf") >= 0)
      {
        $paymentMethod.append($('<div/>', {
          'data-name': 'payment_method',
          'class': 'td sc-input-block',
          'html': '<input type="radio" id="payment_method_paysafe" name="payment_method" value="paysafe" class="payment_method"/><label for="payment_method_paysafe">Paysafe Card'
        }));
      }

      $paymentMethod.change(function(){
        window.payment_method = $('input[name=payment_method]:checked').val();
      });

      $cartTable.find('#get-discount').change('click', function (e) {
        var getDiscount = $('#get-discount').val(1);
        if (getDiscount.val() > 0) {
          totalAmount = totalAmount + 30;
          $totalAmount.html('<div class="total-amount-data total-amount-data-escort">Total: <strong>' + (totalAmount ? (totalAmount + '.00') : 0) + ' GBP' + '</strong></div>');
        }
      });

      $cartTable.find('#not-now').change('click', function (e) {
        var getDiscount = $('#get-discount').val(0);
        if (getDiscount.val() < 1) {
          totalAmount = totalAmount - 30;
          $totalAmount.html('<div class="total-amount-data total-amount-data-escort">Total: <strong>' + (totalAmount ? (totalAmount + '.00') : 0) + ' GBP' + '</strong></div>');
        }
      });

      if (window.payment_method) {
        $paymentMethod.find('#payment_method_' + window.payment_method).attr('checked', true);
      } else {
        $paymentMethod.find('input[type="radio"]').first().attr('checked', true);
      }

      var $checkoutBtn = $('<button/>', {
        'data-component': 'button',
        'data-name': 'buy-button',
        'class': 'modal-button--checkout fixed-button py-3',
        'text': 'FINISH AND PAY'
      }).on('click', function() {
        var cartData = Object.values(vr.state.cart.premium_packages);
        //convert date as needed for shopping card
        var getDiscount = $('#get-discount').val();
        if (getDiscount > 0) {
          cartData[0].price = parseInt(cartData[0].price) + 30;
          cartData[0].banner_price = '30';
        }
        if(cartData[0].activation_date){
          var dateMoment = moment.utc(cartData[0].activation_date).format('YYYY-MM-DD');
          cartData[0].activation_date = dateMoment;
        }
        $checkoutBtn.prop('disabled', true) ;
        var promoCode = $('input[name=promo_code]').val();

        $.ajax({
          url: 'private/billing/checkout',
          method: 'POST',
          dataType: 'json',
          data: {
            cart: cartData,
            payment_method: $('input[name=payment_method]:checked').val(),
            promo_code: promoCode
          },
          success: function(resp) {
            if (resp.status == 'success') {
              window.location.href = resp.url;
            }
          }
        });
      });
      
      $cartFooter.append($promoSection, $totalAmount, $paymentMethod, $checkoutBtn);

      $page2.append($titleBar, $cartTable, $cartFooter);
        var $footer_text = $('<div/>', {
        class: 'footer-notification w-100',
        'html': '<div class="row no-gutters"><div class="col-12 col-md-5 p-3 d-none d-md-block"><p class="font-weight-bold">Why a free ad is not enough?</p><p>Regardless of the type of clients you’re looking for, you need to make sure they see you. A free ad means that your ad gets lost among all the other ads-sometimes it might be on the first page but most often it will be in the last pages and, unfortunately, most potential clients search in the first 2 pages, if not up to the middle of first page. So maximise your chances of being seen by potential clients by upgrading your ad for the time period you need!</p><p>Remember, premium ads get in average ten times more views than the free ones!</p></div><div class="col-2 justify-content-center d-none d-md-flex"><div class="vertical-line" style=""></div></div><div class="col-12 col-md-5 p-3"><img src="/img/mcvs_acc_opt_hrz_105_3x.png" height="35px" width="110px"><p class="font-weight-bold pt-3">On your bank statement will appear AA Media GmbH</p><p>If your bank account is in another currency than the one you see, the bank will automatically convert the amount into your currency at the exchange rate of the day. No additional action is needed from you.</p><p>What is the process?</p><p>1. Please review your cart and make sure it contains the products you want.</p><p>2. Click on the checkout button and wait to be taken to the billing page of our partner MMGBill. Enter your card details and confirm the payment.</p><p>3. Wait for the site to return you a success or failed message; if the message is a success one, then your option will be activated automatically.</p><p>In case of any trouble, please contact us.</p><p class="d-md-none font-weight-bold pt-3">For checkout you will be directed to a secure payment page managed by our online payments partner, mmgbill. All information will be transmitted securely for the payment.</p></div></div>'
      });
      $container.append($page1, $page2, $footer_text);
    


     
      return $container;
    },

    _drawpremium_packageItem: function() {
      var $tr = $('<div/>', {
        class: 'tr'
      });

      if (!this.state.premium_packages.length) {
        var $tdInfo = $('<div/>', {
          text: headerVars.dictionary.buy_premium_package,
          class: 'desc--row my-auto d-flex'
        });
      } else {
        var premium_packs = this.state.premium_packages;
        var infopremium_packages = premium_packs.reduce(function(a, p) {
          if (premium_packs.length > 1)
          {
            if(p.activation_date)
            {
              var actDate = p.activation_date;
              var activationDate = actDate.split("-");
              activationDate = new Date( activationDate[0], activationDate[1] - 1, activationDate[2]);
            }

            var previous_expiration = a.match(/"(.*)"/);
            if (!activationDate && previous_expiration && previous_expiration[1] && previous_expiration[1].length > 0){
              return ((a ? a + ' | ' : '') + '&nbsp;<span>Your <b>'+ p.package_name +'</b> will be activated on "'+ previous_expiration[1] +'"</span>&nbsp;');
            }else if ( activationDate ){
              return ((a ? a + ' | ' : '') + '&nbsp;<span>Your <b>'+ p.package_name +'</b> will be activated on "'+ moment(activationDate).format('D MMM YYYY') +'"</span>&nbsp;');
            }else if ( p.expiration_date ){
              return ((a ? a + ' | ' : '') + '<span>You have ' + p.package_name + '  until "' + moment.unix(p.expiration_date).format("D MMM YYYY")  + '" </span>&nbsp;');
            }else if ( p.expiration_date == null ){
              return ((a ? a + ' | ' : '') + '<span>Your <b>'+ p.package_name +'</b> will be activated shortly.</span>&nbsp;');
            }
          }else{
            if (p.expiration_date == null) {
              return ((a ? a + ' | ' : '') + '<span>Your <b>'+ p.package_name +'</b> will be activated shortly.</span>&nbsp;');
            }else{
              return ((a ? a + ' | ' : '') + '<span>You have ' + p.package_name + '  until ' + moment.unix(p.expiration_date).format("D MMM YYYY")  + ' </span>&nbsp;');
            }
          }
          
        }, '');

        var $tdInfo = $('<div/>', {
          html: infopremium_packages,
          class: 'desc--row my-auto d-flex'
        });
      }

      var $tdBtnWrapper1 = $('<div/>', {
        'data-component': 'td',
        'data-name': 'button-wrapper',
        'class': 'wrapper--button'
      });

      var $newpremium_packageBtn = $('<button/>', {
        'data-component': 'button',
        'data-name': 'purchase-button',
        'class': 'button--purchase fixed-button' ,
        'text': headerVars.dictionary.buy_premium_package
      });

      $tdBtnWrapper1.append($newpremium_packageBtn);

      if (!this._canBuy(this.state.premium_packages)) {
         $tdBtnWrapper1.addClass('disabled');

        var $tooltip = $('<div/>', {
          'class': 'tooltip',
          'text': 'You already have a package. You cannot buy more.'
        });
        $tdBtnWrapper1.append($tooltip);

        $newpremium_packageBtn.addClass('disabled');
        $newpremium_packageBtn.text('EXTEND');
      }

      $tr.append($tdInfo, $tdBtnWrapper1);

      return $tr;
    },

    _drawAgencyView: function() {
      var vr = this;

      var $container = $('<div/>', {
        'class': "self-checkout-container row no-gutters"
      });

      /** PAGE 1 */
      var $page1 = $('<div/>', {
        'data-component': 'page',
        'data-id': 1,
        'data-name': 'escortslist',
        'class': 'page card col-12',
        'css': {
          'display': this.state.pageView !== 1 ? 'none' : 'block'
        }
      });

      var $headerBar = $('<div/>', {
        'data-component': 'bar',
        'data-name': 'headerbar',
        'class': 'bar card-header-agency'
      });

      var $title = $('<div/>', {
        'text': 'Your Escorts',
        'data-component': 'heading',
        'data-name': 'title',
        'class': 'title'
      });

      var $searchBar = $('<div/>', {
        'data-component': 'input',
        'data-name': 'searchbar',
        'class': 'search-bar',
        'html': '<input type="text" placeholder="Search" value="' + vr.state.searchTerm + '" />',
      });

      var $sortBar = $('<div/>', {
        'data-component': 'select',
        'data-name': 'sortbar',
        'class': 'sort-bar d-flex',
        'html': '<select></select>'
      });

      var $cartIcon = $('<i>', {
        'class': 'fa fa-cart-plus fa-2x go-to-cart px-3'
      }).on('click', function(e) {
        e.preventDefault();

        vr.state.pageView = 2;
        new ViewRenderer(vr.state);
      });

      if (vr.state.cart.premium_packages && Object.keys(vr.state.cart.premium_packages).length) {
        $cartIcon.append($('<span>', { text: Object.keys(vr.state.cart.premium_packages).length, class: 'cart-packages-count' }));
      }

      var $sortOptions = ['Alphabetically', 'By escort ID', 'Latest modified', 'Newest first'].map(function(type) {
        return $('<option/>', {
          'text': type,
          'data-id': type.toLowerCase().replace(/ /g, '-')
        });
      });
      $sortBar.find('select').append($sortOptions);
      $sortBar.append($cartIcon);

      $headerBar.append($title, $searchBar, $sortBar);

      var $escortsList = $('<div/>', {
        'data-component': 'list',
        'data-name': 'escortslist',
        'class': 'escorts-list-wrapper agency'
      });
      var term = vr.state.searchTerm;
      if (term) {
        vr.state.visibleEscorts = Object.values(vr.state.escorts).filter(function(escort) {
          if ( ~escort.showname.toLowerCase().search(term.toLowerCase()) || ~escort.id.toString().search(term)) return escort;
        });
      } else {
        vr.state.visibleEscorts = Object.values(vr.state.escorts);
      }

      $.each(vr.state.visibleEscorts, function(idx,escort) {
        var $escortItem = vr._drawEscortItem(escort);
        $escortsList.append($escortItem);
      });

      $page1.append($headerBar, $escortsList);

      /** PAGE 2 */
      var $page2 = $('<div/>', {
        'data-component': 'page',
        'data-id': 2,
        'data-name': 'cart',
        'class': 'page col-12',
        'css': {
          'display': this.state.pageView !== 2 ? 'none' : 'block'
        }
      });

      var $titleBar = $('<div/>', {
        'data-component': 'bar',
        'data-name': 'titlebar',
        'class': 'bar--inverted card-header'
      });

      var $backToEscortList = $('<a/>', {
        'html': '<img src="/img/button--back.png" class="arrow-left" alt="back-button"><span class="d-none d-md-block"></span>',
        'href': '#',
        'data-component': 'heading',
        'class': 'go-to-page px-3',
      }).on('click', function(e) {
        e.preventDefault();
        vr.state.pageView = 1;
        new ViewRenderer(vr.state);
      });

      var $title = $('<div/>', {
        'text': 'Review your cart',
        'data-component': 'heading',
        'data-name': 'title',
        'class': 'title',
      });

      $titleBar.append($backToEscortList, $title);

      var $cartTable = $('<div/>', {
        'data-component': 'table',
        'data-name': 'carttable',
        'class': 'table agency-cart-review'
      });

      var $tableHead = $('<div/>', {
        'data-component': 'thead',
        'data-name': 'tablehead',
        'class': 'head head-custom'
      });

      $.each([ 'Escort Name', 'Package' ,'Activation Date', 'Duration', 'Price', 'Options' ], function(idx, el) {
        $tableHead.append($('<div/>', {
          'data-component': 'td',
          'data-name': 'colname',
          'class': 'row-custom td columnHead' + el,
          'text': el
        }))
      });

      var _buildAgencyTable = () => {
        if (this.state.cart.premium_packages && Object.keys(this.state.cart.premium_packages).length) {

          $.each(this.state.cart.premium_packages, function(idx, p) {
            var fixedDate = new Date(p.activation_date);
            var $tableRow = $('<div/>', {
              'data-id': p.escortId,
              'id': p.escortId,
              'data-component': 'tr',
              'data-name': 'tablerow',
              'class': 'custom-data-row'
            }).on('click', function(event) {
              $.each($('.agency-cart-review tr td.action-buttons'), function(index, el) {
                $(el).addClass('');
              });

              if($(this).hasClass('selected-escort')){
                $(this).removeClass('selected-escort');
              }else{
                $(this).addClass('selected-escort');
                $(this).find('.action-buttons').removeClass('d-none');
              }

              $.each($('.agency-cart-review tr').not('#'+$(this).data('id')), function(index, el) {
                $(el).removeClass('selected-escort');
              });
            });

            if (ismobile){
              $tableRow.append($('<div/>', {
                'class': 'td custom-td custom-mobile',
                'html': '<img src='+p.photo+' class="escort-photo" data-id="' + idx + '" alt="escort-photo">' + p.showname
              }));

              $tableRow.append($('<div/>', {
                'class': 'td custom-td',
                'html': '<span class="light-gray"> Package </span>' + '<span class="dark-text">' + p.type + '</span>'
              }));

              $tableRow.append($('<div/>', {
                'class': 'td custom-td',
                'html': '<span class="light-gray"> Activation date </span>' + '<span class="dark-text">' + (p.activation_date ? fixedDate.getDate() + 1 + ' ' + fixedDate.toLocaleString('default', { month: 'short' }) + ' ' + fixedDate.getFullYear() : 'ASAP') + '</span>'
              }));

              $tableRow.append($('<div/>', {
                'class': 'td custom-td',
                'html': '<span class="light-gray"> Duration </span>' + '<span class="dark-text">' + p.duration+' Days' + '</span>'
              }));

              $tableRow.append($('<div/>', {
                'class': 'td custom-td',
                'html': '<span class="light-gray"> Price </span>' + '<span class="dark-text">' +  p.price +' GBP' + '</span>'
              }));

              $tableRow.append($('<div/>', {
                'data-actions-escort-id': p.escortId,
                'class': 'td action-buttons td actions',
                'html': '<div class="action-buttons-item button--edit" data-id="' + idx + '"> <img src="/img/button--edit.png" alt="edit">' + '<span class="ml-2 "> Edit </span>' + '</div>' +
                    '<div class="action-buttons-item button--delete" data-id="' + idx + '"> <img src="/img/button--delete.png" alt="remove">' + '<span class="ml-2 "> Remove </span>' + '</div>'
              }));
            }
            else if (!ismobile){
              $tableRow.append($('<div/>', {
                'class': 'td custom-td',
                'html': '<img src=' + p.photo + ' class="escort-photo" data-id="' + idx + '">' + p.showname
              }));

              $tableRow.append($('<div/>', {
                'class': 'td custom-td',
                'text': p.type
              }));

              $tableRow.append($('<div/>', {
                'class': 'td custom-td',
                'text': p.activation_date ? fixedDate.getDate() + 1 + ' ' + fixedDate.toLocaleString('default', { month: 'short' }) + ' ' + fixedDate.getFullYear() : 'ASAP'
              }));

              $tableRow.append($('<div/>', {
                'class': 'td custom-td',
                'text': p.duration + ' Days'
              }));

              $tableRow.append($('<div/>', {
                'class': 'td custom-td',
                'text': p.price + ' GBP'
              }));

              $tableRow.append($('<div/>', {
                'data-actions-escort-id': p.escortId,
                'class': 'td action-buttons td actions',
                'html': '<img src="/img/button--edit.png" class="button--edit" data-id="' + idx + '" alt="edit-button">' +
                    '<img src="/img/button--delete.png" class="button--delete" data-id="' + idx + '" alt="remove-button">'
              }));
            }
            $cartTable.append($tableRow);
          });
        }else{
          var $tableRow = $('<tr/>', {
            'class': '',
            'html': '<p class="pl-3">Cart is empty</p>'
          });
          $cartTable.append($tableRow);
        }
      }

      $cartTable.append($tableHead);
      _buildAgencyTable();


      var packageCnt = this.state.cart.premium_packages ? Object.keys(this.state.cart.premium_packages).length : 0;
      var $banner = ($('<div/>', {
        'html': '<div class="discount-title">' + 'DISCOUNT' + '</div>' +
            '<div class="discount-content">' + 'You have one premium ad in your cart, you can now purchase ' +
            'a banner for your agency for only 30 GBP/30 days!' + '</div>' +
            '<div class="discount-inputs">' +
            '<div class="discount-input">' +
            '<input type="radio" id="get-discount" name="radio" value="" class="get-discount mr-2"/><label for="get-discount"> Get the discount banner' + '</label>' +
            '</div>' +
            '<div class="discount-input">' +
            '<input type="radio" id="not-now" name="radio" value="0" checked="checked" class="not-now ml-3 mr-2"/><label for="not-now"> Not now' + '</label>' +
            '</div>' +
            '</div>'
        ,
        'class': 'discount-banner'
      }));
      if (packageCnt > 2) {
        $banner = ($('<div/>', {
          'html': '<div class="discount-title">' + 'DISCOUNT' + '</div>' +
              '<div class="discount-content">' + 'You have one premium ad in your cart, you can now purchase ' +
              'a banner for your agency for only 25 GBP/30 days!' + '</div>' +
              '<div class="discount-inputs">' +
              '<div class="discount-input">' +
              '<input type="radio" id="get-discount" name="radio" value="" class="get-discount mr-2"/><label for="get-discount"> Get the discount banner' + '</label>' +
              '</div>' +
              '<div class="discount-input">' +
              '<input type="radio" id="not-now" name="radio" value="0" checked="checked" class="not-now ml-3 mr-2"/><label for="not-now"> Not now' + '</label>' +
              '</div>' +
              '</div>'
          ,
          'class': 'discount-banner'
        }));
      }

      var $cartFooter = $('<div/>', {
        'class': 'agency--footer--cart d-flex flex-md-row ' + ((!ismobile)? 'pt-5': ''),
      });
      var totalAmount = 0;
      var discountedAmount = 0;
      var discountPct = 0;
      var discounts = this.state.agencyDiscounts;

      if (packageCnt) {
        if (discounts) {
          $.each(discounts, function(index, el) {
              if (el.range[1]) {
              if (packageCnt >= el.range[0] && packageCnt < el.range[1]) {
                discountPct = el.discount;
                return false;
              }
            } else {
              if (packageCnt >= el.range[0]) {
                discountPct = el.discount;
                return false;
              }
            }
          });
          
        }
      } 

      if (this.state.cart.premium_packages && Object.keys(this.state.cart.premium_packages).length) {
        discountedAmount = Object.keys(this.state.cart.premium_packages).reduce(function(a, key) {
          var el = vr.state.cart.premium_packages[key];
          totalAmount += +el.price;
          a += +el.price - (el.price * discountPct / 100);
          return a;
        }, 0);
      }

      discountedAmount = discountedAmount.toFixed(2);
      discountedAmount = discountedAmount * 1;

      var $this = this;

      var $promoInput = $('<div/>', {
        class: 'agency-promo-input',
        'html': '<input type="text" name="promo_code" class="agency-promo-code form-control" id="agency-promo-code" placeholder="Promo Code">'
      });

      var $promoCheck = $('<div/>', {
        class: 'agency-promo-button',
        'html': '<button type="button" name="promo_check" class="agency-promo-check btn btn-info" id="agency-promo-code" >APPLY</button>'
      }).on('click', function() {
        var $promoCode = $('#agency-promo-code').val();
        $.ajax({
          url: 'private/billing/check-promo-code',
          method: 'POST',
          dataType: 'json',
          data: {
            promo_code: $promoCode
          },
          success: function(resp) {
            if (resp.status == 'success') {
              var cartData = Object.values(vr.state.cart.premium_packages);
              var discounted = 0;
              $('#agency-promo-code').css('border', '3px solid lightgreen')
              $('#agency-promo-code').css('border-radius','3px');
              if ($('.promo-value').length)
              {
                $('.promo-value').remove();
              }
              if (resp.promo_data.amount)
              {
                var cartTotal = 0;
                $.each(cartData, function(index, el) {
                  cartTotal += parseInt(cartData[index].price);
                });
                if (cartTotal > resp.promo_data.amount)
                {
                  var writeOffPricePerCartItem = resp.promo_data.amount/cartData.length;
                  discounted = resp.promo_data.amount;
                  if (cartData.length > 1)
                  {
                    $.each(cartData, function(index, el) {
                      cartData[index].price = parseInt(cartData[index].price) - writeOffPricePerCartItem;
                    });
                  }else{
                    $.each(cartData, function(index, el) {
                      cartData[index].price = ((parseInt(cartData[index].price) - writeOffPricePerCartItem) >= 0)?parseInt(cartData[index].price) - writeOffPricePerCartItem : 0;
                    });
                  }
                }else{
                  if ($('.promo-value').length)
                  {
                    $('.promo-value').remove();
                  }
                  $('#agency-promo-code').css('border', '3px solid red');
                  $('#agency-promo-code').css('border-radius','3px');
                  setTimeout(() => {$('#promo-code').css('border', '3px solid transparent')},200)
                  setTimeout(() => {$('#promo-code').css('border', '3px solid red')},500)
                  setTimeout(() => {$('#promo-code').css('border', '3px solid transparent')},700)
                  setTimeout(() => {$('#promo-code').css('border', '3px solid red')},900)
                  setTimeout(() => {alert("Cart total amount should exceed promo code amount")},1200)
                  return;
                }
              }else if (resp.promo_data.percent)
              {
                $.each(cartData, function(index, el) {
                  cartData[index].price = cartData[index].price - (cartData[index].price * resp.promo_data.percent)/100;
                  discounted += cartData[index].price * resp.promo_data.percent/100;
                });
              }else if(resp.promo_data.days){
                $.each(cartData, function(index, el) {
                  cartData[index].duration = cartData[index].duration + '+' + resp.promo_data.days;
                });
              }
              $cartTable.empty();

              var $tableHead = $('<div/>', {
                'data-component': 'thead',
                'data-name': 'tablehead',
                'class': 'head head-custom'
              });

              $.each([ 'Escort Name', 'Package' ,'Activation Date', 'Duration', 'Price', 'Options' ], function(idx, el) {
                $tableHead.append($('<div/>', {
                  'data-component': 'td',
                  'data-name': 'colname',
                  'class': 'row-custom td columnHead' + el,
                  'text': el
                }))
              });

              $cartTable.append($tableHead);
              _buildAgencyTable();

              var total_amount = Object.values(vr.state.cart.premium_packages).reduce(function(a, el) {
                a += +el.price;

                return a;
              }, 0);

              var $totalAmount = $('<div>', {
                'html': '<p class="total-amount-data-agency">Total: \<br>' + '<strong>' + (total_amount ? total_amount : 0) + ' GBP' + '</strong> </p>',
                'class': 'total-amount-wrapper'
              });

              var $discountAmount = $('<div>', {
                'html': '<p class="discount-amount-data-agency">Discount: \<br>' + '<strong>' + (discounted ? discounted : 0) + ' GBP'  + '</strong></p>',
                'class': 'discount-amount-wrapper'
              });

              var $toPayAmount = $('<div>', {
                'html': '<p class="to-pay-amount-data-agency">To pay: \<br>' + ' <strong>' + (total_amount ? total_amount : 0) + ' GBP' +'</strong></p>',
                'class': 'to-pay-amount-wrapper'
              });

              $('p.total-amount-data-agency').replaceWith($totalAmount);
              $('p.discount-amount-data-agency').replaceWith($discountAmount);
              $('p.to-pay-amount-data-agency').replaceWith($toPayAmount);
              $('button.agency-promo-check').attr('disabled','disabled');
            }else{
              if ($('.promo-value').length)
              {
                $('.promo-value').remove();
              }
              $('#promo-code').css('border', '3px solid red');
              $('#promo-code').css('border-radius','3px');
              setTimeout(() => {$('#promo-code').css('border', '3px solid transparent')},500)
              setTimeout(() => {$('#promo-code').css('border', '3px solid red')},800)
              setTimeout(() => {$('#promo-code').css('border', '3px solid transparent')},1000)
            }
          }
        });
      });

      var $promoSection = $('<div/>', {
        class: 'agency-promo-container'
      });

      $promoSection.append($promoInput,$promoCheck);


     /** Total Price El */

      var $totalAmount = $('<div>', {
        'html': '<p class="total-amount-data-agency ">Total: \<br>' + '<strong>' + totalAmount + ' GBP' + '</strong> </p>',
        'class': 'total-amount-wrapper'
      });

      var $discountAmount = $('<div>', {
        'html': '<p class="discount-amount-data-agency ">Discount: \<br>' + '<strong>' + (totalAmount - discountedAmount).toFixed(2) + ' GBP'  + '</strong></p>',
        'class': 'discount-amount-wrapper'
      });

      var $toPayAmount = $('<div>', {
        'html': '<p class="to-pay-amount-data-agency">To pay: \<br>' + ' <strong>' + discountedAmount  + ' GBP' +'</strong></p>',
        'class': 'to-pay-amount-wrapper'
      });
      
      var $paymentMethod = $('<div/>', {
        class: 'payment_method_block d-none'
      });
      if (vr.state.activePaymentmethodes && vr.state.activePaymentmethodes.indexOf("mmg") >= 0) {
        $paymentMethod.append($('<div/>', {
          'data-name': 'payment_method',
          'class': 'td sc-input-block',
          'html': '<input type="radio" id="payment_method_mmgbill" name="payment_method" value="mmgbill" class="payment_method"/><label for="payment_method_mmgbill">Credit/Debit card (MMGbill)'
        }));
      }
      if (vr.state.activePaymentmethodes && vr.state.activePaymentmethodes.indexOf("psf") >= 0)
      {
        $paymentMethod.append($('<div/>', {
            'data-name': 'payment_method',
            'class': 'td sc-input-block',
            'html': '<input type="radio" id="payment_method_paysafe" name="payment_method" value="paysafe" class="payment_method"/><label for="payment_method_paysafe">Paysafe Card'
        }));
      }
      $paymentMethod.change(function(){
        window.payment_method = $('input[name=payment_method]:checked').val();
      });

      if (window.payment_method) {
        $paymentMethod.find('#payment_method_' + window.payment_method).attr('checked', true);
      } else {
        $paymentMethod.find('input[type="radio"]').first().attr('checked', true);
      }

      $banner.find('#get-discount').change('click', function (e) {
        var getDiscount = $('#get-discount').val(1);
        if (getDiscount.val() > 0 && packageCnt < 3) {
          totalAmount = totalAmount + 30;
          discountedAmount = discountedAmount + 30;
          $totalAmount.html('<p class="total-amount-data ml-5">Total: \<br>' + '<strong>' + totalAmount + ' GBP' + '</strong> </p>');
          $toPayAmount.html('<p class="to-pay-amount-data ml-5 mr-3">To pay: \<br>' + ' <strong>' + discountedAmount  + ' GBP' +'</strong></p>');
        }
        else {
          totalAmount = totalAmount + 25;
          discountedAmount = discountedAmount + 25;
          discountedAmount = discountedAmount.toFixed(2) * 1;
          $totalAmount.html('<p class="total-amount-data ml-5">Total: \<br>' + '<strong>' + totalAmount + ' GBP' + '</strong> </p>');
          $toPayAmount.html('<p class="to-pay-amount-data ml-5 mr-3">To pay: \<br>' + ' <strong>' + discountedAmount  + ' GBP' +'</strong></p>');
        }
      });

      $banner.find('#not-now').change('click', function (e) {
        var getDiscount = $('#get-discount').val(0);
        if (getDiscount.val() < 1 && packageCnt < 3) {
          totalAmount = totalAmount - 30;
          discountedAmount = discountedAmount - 30;
          $totalAmount.html('<p class="total-amount-data ml-5">Total: \<br>' + ' <strong>' + totalAmount  + ' GBP' +'</strong></p>');
          $toPayAmount.html('<p class="to-pay-amount-data ml-5 mr-3">To pay: \<br>' + ' <strong>' + discountedAmount  + ' GBP' +'</strong></p>');
        }
        else{
          totalAmount = totalAmount - 25;
          discountedAmount = discountedAmount - 25;
          discountedAmount = discountedAmount.toFixed(2) * 1;
          $totalAmount.html('<p class="total-amount-data ml-5">Total: \<br>' + ' <strong>' + totalAmount  + ' GBP' +'</strong></p>');
          $toPayAmount.html('<p class="to-pay-amount-data ml-5 mr-3">To pay: \<br>' + ' <strong>' + discountedAmount  + ' GBP' +'</strong></p>');
        }
      });

      var $checkoutBtn = $('<button>', {
        'data-component': 'button',
        'data-name': 'buy-button',
        'class': 'modal-button--checkout fixed-button finish-and-pay',
        'text': 'FINISH & PAY'
      }).on('click', function() {
        
        var packages = vr.state.cart.premium_packages;

        var cartData = Object.keys(packages).map(function(k) { 
          if (discountPct) {
            packages[k].discount = discountPct;
          }

          return vr.state.cart.premium_packages[k]; 
        });
        var promoCode = $('input[name=promo_code]').val();

        $.each(cartData, function(index, el) {
          var getDiscount = $('#get-discount').val();
          if (getDiscount > 0) {
            cartData[index].price = (cartData[0].price) + 30;
            cartData[0].banner_price = '30';
          }
          if (getDiscount > 0 && packageCnt >= 3) {
            cartData[0].price = (cartData[0].price) + 25;
            cartData[0].banner_price = '25';
          }
            if(cartData[index].activation_date){
                var dateMoment = moment.utc(cartData[index].activation_date).format('YYYY-MM-DD');
                cartData[index].activation_date = dateMoment;
            }
        });
        $checkoutBtn.prop('disabled', true) ;

        $.ajax({
          url: 'private/billing/checkout',
          method: 'POST',
          dataType: 'json',
          data: {
            cart: cartData,
            payment_method: $('input[name=payment_method]:checked').val(),
            promo_code: promoCode
          },
          success: function(resp) {
            if (resp.status == 'success') {
              window.location.href = resp.url;
            }
          }
        })
      });
      if (!ismobile)
      {
        $cartFooter.append($promoSection, $totalAmount,$discountAmount, $toPayAmount, $paymentMethod, $checkoutBtn);
      }else{

        var $amountSection = $('<div/>', {
          class: 'agency-amount-container'
        });

        $amountSection.append($totalAmount,$discountAmount, $toPayAmount, $paymentMethod, $checkoutBtn);

        $cartFooter.append($promoSection, $amountSection);
      }

      $page2.append($titleBar, $cartTable, $banner, $cartFooter);

      $container.append($page1, $page2);

      return $container;
    },

    _drawEscortItem: function (escort) {
      var inCart = false;
      var cartPackages = this.state.cart.premium_packages;
      if (cartPackages) {
        var inCartEscorts = Object.keys(cartPackages).map(function (idx) {
          return cartPackages[idx].escortId;
        });

        var escortPackagePair = Object.keys(cartPackages).reduce(function (a, idx) {
          a[cartPackages[idx].escortId] = idx;

          return a;
        }, {});

        inCart = inCartEscorts.includes(escort.id.toString());
      }

      var $tr = $('<div/>', {
        'class': 'tr'
      });

      var $photo = $('<img/>', {
        'data-component': 'image',
        'data-name': 'image',
        'src': escort.photo,
        'css': {
          'width': '80px',
          'height': '80px',
          'border-radius': '50%',
          'object-fit': 'cover',
          'object-position': 'center right'
        },
        'class': inCart ? 'in-cart' : 'escort-photo'
      });

      var $escortInfo = $('<div/>', {
        'data-component': 'td',
        'data-name': 'escortinfo',
        'class': 'info--escort',
        'html': '<p class="info--escort--showname"><b>' + escort.showname + '</b><br>ID: #'+escort.id+'</p>' 
      });

    if (!inCart) {
        if (!escort.escort_packages.length) {
          var $tdInfo = $('<div/>', {
            // 'text': headerVars.dictionary.buy_premium_package,    //EGUK-274
            'class': 'desc--row my-auto d-flex'
          });
        } else {
          var infoPackages = escort.escort_packages.reduce(function (a, p) {
            if (p.expiration_date == null) {
              var previous_expiration = a.match(/"(.*)"/);
              if (previous_expiration && previous_expiration[1] && previous_expiration[1].length > 0)
              {
                return ((a ? a + ' | ' : '') + '&nbsp;<span> <b>'+ p.package_name +'</b> will be activated on ' + previous_expiration[0] + '</span>&nbsp;');
              }else{
                return ((a ? a + ' | ' : '') + '&nbsp;<span> <b>'+ p.package_name +'</b> will be activated shortly.</span>&nbsp;');
              }
            }else{
              return ((a ? a + ' | ' : '') + '<span>Has ' + p.package_name + '  until "' + moment.unix(p.expiration_date).format("D MMM YYYY")  + '" </span>&nbsp;');
            }
          }, '');

          var $tdInfo = $('<div/>', {
            'html': infoPackages,
            'class': 'desc--row my-auto d-flex'
          });
        }
      } else {
        var $tdInfo = $('<div>', {
          'text': 'Is in your cart',
          'class': 'desc--row in-cart align-center align-md-left'
        });
      }

      var $tdBtnWrapper1 = $('<div/>', {
        'data-component': 'td',
        'data-name': 'button-wrapper',
        'class': 'wrapper--button'
      });
      var $tdBtnWrapper2 = $tdBtnWrapper1.clone();

      var $newPackageBtn = null;
      
      if (inCart) {
        $newPackageBtn = $('<button/>', {
          'data-component': 'button',
          'data-name': 'purchase-button',
          'title': 'Remove from the cart',
          'data-id': escortPackagePair[escort.id],
          'class': 'button--remove-from-cart button--remove',
          'html': 'Remove &times;'
        });
      } else {
        var hasPackage = escort.escort_packages.length;
        // Not the best way to do this, sorry.
        // If hasPackage = 1 -> escort has package and can buy one more (pending)
        // If hasPackage = 2 -> escort has 2 packages.
        $newPackageBtn = $('<button/>', {
          'data-component': 'button',
          'data-name': 'purchase-button',
          'data-id': escort.id,
          'title': !escort.escort_packages.length ? 'Upgrade now' : 'This Escort is Upgraded',
          'class': !escort.escort_packages.length ? 'button--purchase inverted' : 'button--purchase disabled',
          'text' : 'Upgrade now'
        });
        if ( hasPackage == 1){
          $newPackageBtn = $('<button/>', {
            'data-component': 'button',
            'data-name': 'purchase-button',
            'data-id': escort.id,
            'text': 'Extend',
            'class' : 'button--purchase inverted'
          });
        }
        if ( hasPackage == 2){
          $newPackageBtn = $('<button/>', {
            'data-component': 'button',
            'data-name': 'purchase-button',
            'data-id': escort.id,
            'text': 'Upgraded',
            'class' : 'button--purchase disabled'
          });
        }
      }
      $tdBtnWrapper2.append($newPackageBtn);
      $escortInfo.prepend($photo);
      $tr.append($escortInfo, $tdInfo, $tdBtnWrapper2);

      return $tr;
    },

    _bindEvents: function() {
      var vr = this;
      
      $('.button--purchase:not(.disabled)').on('click', function() {
        var modalContent = vr._getModalContent(null, null);

        var $modal = new Modal({
          close: true,
          header: modalContent.$header,
          body: modalContent.$body
        });
        $modal.show();

        var cartItem = vr._getCartItem($modal);
        $modal.find('.amount').html( '£' + cartItem.getAmount());

        var startDate;
        if (vr.state.premium_packages && (vr.state.premium_packages[0] && vr.state.premium_packages[0].expiration_date))
        {
          startDate = new Date(vr.state.premium_packages[0].expiration_date * 1000);
        }else {
          startDate = moment().toDate();
        }
        var $pikaday = new Pikaday({
          format: 'D MMM YYYY',
          minDate : startDate,
          defaultDate: moment().toDate(),
          formatSubmit: 'yyyy/mm/dd',
          field: $('#premium_packageActivationDate')[0],
          container: $('#ModalOverlay #render-pikaday')[0],
          trigger: $('#psd_scheduled')[0],
         
          onSelect: function(selected_date) {
            if ($('#premium_packageActivationDate') && $('#premium_packageActivationDate').val())
            {
              var date = selected_date.setDate(selected_date.getDate() + 1);
              var selDate = moment(date).format('D MMM YYYY');
              $('#to-show').html(selDate);
            }
          },
        });

        $modal.find('.modal-button--amount').on('click', function (e) {
          e.preventDefault();

          var cartItem = vr._getCartItem($modal);

          var itemForCart = cartItem.getForCart();
            
          var cartInstance = Cart.getInstance();
          var hash = 'p' + Date.now().toString(16);
          cartInstance.premium_packages[hash] = itemForCart;
          vr.state.cart = cartInstance;
          vr.state.pageView = 2;
          $modal.hide();
          new ViewRenderer(vr.state); 
        });

        $modal.find('input[type="radio"]').on('change', function() {
          var cartItem = vr._getCartItem($modal);
          $modal.find('.amount').html( '£' + cartItem.getAmount());
        });

      });


      $('.button--edit').on('click', function() {
        var premium_packageUid = $(this).data('id');
        var modalContent = vr._getModalContent(null, vr.state.cart.premium_packages[premium_packageUid]);

        var $modal = new Modal({
          close: true,
          header: modalContent.$header,
          body: modalContent.$body
        });
        $modal.show();

        var cartItem = vr._getCartItem($modal);
        $modal.find('.amount').html( '£' + cartItem.getAmount());

        new Pikaday({
             format: 'D MMM YYYY',
          formatSubmit: 'yyyy/mm/dd',
          field: $('#premium_packageActivationDate')[0],
          container: $('#ModalOverlay #render-pikaday')[0],
          trigger: $('#psd_scheduled')[0],
          minDate : moment().toDate(),
        });

        $modal.find('.modal-button--amount').on('click', function (e) {
          e.preventDefault();
          var cartItem = vr._getCartItem($modal);
          var itemForCart = cartItem.getForCart();

          var cartInstance = Cart.getInstance();
          cartInstance.premium_packages[premium_packageUid] = itemForCart;
          vr.state.cart = cartInstance;
          vr.state.pageView = 2;
          $modal.hide();
          new ViewRenderer(vr.state);
        });

        $modal.find('input[type="radio"]').on('change', function() {
          var cartItem = vr._getCartItem($modal);
          $modal.find('.amount').html( '£' + cartItem.getAmount());
        });
      });

      $('.button--delete').on('click', function() {
        var premium_packageUid = $(this).data('id');
        delete vr.state.cart.premium_packages[premium_packageUid];
        if (Object.keys(vr.state.cart.premium_packages).length === 0) {
          vr.state.pageView = 1;
        } else {
          vr.state.pageView = 2;
        }

        new ViewRenderer(vr.state);
      });
    },

    _bindAgencyEvents: function() {
      var vr = this;

      $('.button--purchase:not(.disabled)').on('click', function() {
        var escortId = $(this).data('id');
        var escort = vr.state.escorts[escortId];

        var modalContent = vr._getModalContent(escort, null);

        var $modal = new Modal({
          close: true,
          header: modalContent.$header,
          body: modalContent.$body
        });
        $modal.show();

        if($modal.find('#pn_MonthlyPlus').prop('checked')) {
            $modal.find('.duration').removeClass('d-flex');
            $modal.find('.duration').hide();
        } 

        var cartItem = vr._getCartItem($modal);
        $modal.find('.amount').html( '£' + cartItem.getAmount());
        var hasPackage = false;
        if (escort.escort_packages.length > 0)
        {
          hasPackage = true;
        }
        if ( hasPackage )
        {
          $modal.find('.psd').html('After current one expires');
        }

       var $pikaday = new Pikaday({
          format: 'D MMM YYYY',
          minDate : moment().toDate(),
          formatSubmit: 'yyyy/mm/dd',
          field: $('#premium_packageActivationDate')[0],
          container: $('#ModalOverlay #render-pikaday')[0],
          trigger: $('#psd_scheduled')[0],

         onSelect: function(selected_date) {
           if ($('#premium_packageActivationDate') && $('#premium_packageActivationDate').val())
           {
             var date = selected_date.setDate(selected_date.getDate() + 1);
             var selDate = moment(date).format('D MMM YYYY');
             $('#to-show').html(selDate);
           }
         },
        });

        $modal.find('.modal-button--amount').on('click', function (e) {
          e.preventDefault();
          var cartItem = vr._getCartItem($modal);
          var itemForCart = cartItem.getForCart();

          var cartInstance = Cart.getInstance();
          var hash = 'p' + Date.now().toString(16);
          cartInstance.premium_packages[hash] = itemForCart;
          vr.state.cart = cartInstance;

          $modal.hide();
          new ViewRenderer(vr.state);

          var infoModalContent = vr._getInfoModalContent();


          var $modalMsg = new Modal({
            header: infoModalContent.$header,
            body: infoModalContent.$body
          });

          $modalMsg.find('.modal-button--continue').on('click', function() {
            vr.state.pageView = 1;
            $modalMsg.hide();
            new ViewRenderer(vr.state);
          });

          $modalMsg.find('.modal-button--gotocart').on('click', function() {
            vr.state.pageView = 2;
            $modalMsg.hide();
            new ViewRenderer(vr.state);
          });

          $modalMsg.show()
        });

        $modal.find('input[type="radio"]').on('change', function() {
          if($modal.find('#pn_MonthlyPlus').prop('checked')) {
              $modal.find('.duration').removeClass('d-flex');
              $modal.find('.duration').hide();
          } else if($modal.find('.duration').is(":hidden")) {
              $modal.find('.duration').addClass('d-flex');
              $modal.find('.duration').show();
          }

          var cartItem = vr._getCartItem($modal);
          $modal.find('.amount').html( '£' + cartItem.getAmount());
        });
      });

      $('.button--edit').on('click', function() {
        var premium_packageUid = $(this).data('id');
        var modalContent = vr._getModalContent(vr.state.escorts[vr.state.cart.premium_packages[premium_packageUid].escortId], vr.state.cart.premium_packages[premium_packageUid]);

        var $modal = new Modal({
          close: true,
          header: modalContent.$header,
          body: modalContent.$body
        });
        $modal.show();

        if($modal.find('#pn_MonthlyPlus').prop('checked')) {
            $modal.find('.duration').removeClass('d-flex');
            $modal.find('.duration').hide();
        } 

        var cartItem = vr._getCartItem($modal);
        $modal.find('.amount').html( '£' + cartItem.getAmount());

        new Pikaday({
          format: 'D MMM YYYY',
          formatSubmit: 'yyyy/mm/dd',
          field: $('#premium_packageActivationDate')[0],
          container: $('#ModalOverlay #render-pikaday')[0],
          trigger: $('#psd_scheduled')[0],
          minDate: moment().toDate(),
        });

        $modal.find('.modal-button--amount').on('click', function (e) {
          e.preventDefault();

          var cartItem = vr._getCartItem($modal);
          var itemForCart = cartItem.getForCart();
          var cartInstance = Cart.getInstance();
          cartInstance.premium_packages[premium_packageUid] = itemForCart;
          vr.state.cart = cartInstance;
          vr.state.pageView = 2;
          $modal.hide();
          new ViewRenderer(vr.state);
        });

        $modal.find('input[type="radio"]').on('change', function() {
          if($modal.find('#pn_MonthlyPlus').prop('checked')) {
              $modal.find('.duration').removeClass('d-flex');
              $modal.find('.duration').hide();
          } else if($modal.find('.duration').is(":hidden")) {
              $modal.find('.duration').addClass('d-flex');
              $modal.find('.duration').show();
          }

          var cartItem = vr._getCartItem($modal);
          $modal.find('.amount').html( '£' + cartItem.getAmount());
        });
      });

       $('.button--delete').on('click', function() {
        var premium_packageUid = $(this).data('id');
        delete vr.state.cart.premium_packages[premium_packageUid];
        if (Object.keys(vr.state.cart.premium_packages).length === 0) {
          vr.state.pageView = 1;
        } else {
          vr.state.pageView = 2;
        }

        new ViewRenderer(vr.state);
      });

        $('.button--remove-from-cart').on('click', function () {
        var packageUid = $(this).data('id');
        delete vr.state.cart.premium_packages[packageUid];

        new ViewRenderer(vr.state);
      });

      $('.search-bar').find('input').on('input', function() {
        vr.state.searchTerm = $(this).val();
        new ViewRenderer(vr.state);
      });

      $('.sort-bar').find('select').on('change', function() {
        var selectedOpt = $(this).find(":selected").data('id');

        switch (selectedOpt) {
          case 'alphabetically':
            vr.state.visibleEscorts.sort(function(a, b) {
              return ('' + a.showname).localeCompare(b.showname);
            });
            $('.escorts-list-wrapper.agency').html( '');
            $.each(vr.state.visibleEscorts, function(idx, escort) {
              var $escortItem = vr._drawEscortItem(escort);
              $('.escorts-list-wrapper.agency').append($escortItem);
            });
            break;
          case 'by-escort-id':
            vr.state.visibleEscorts.sort(function(a, b) {
              return (a.id - b.id);
            });

            $('.escorts-list-wrapper.agency').html( '')
            $.each(vr.state.visibleEscorts, function(idx, escort) {
              var $escortItem = vr._drawEscortItem(escort);
              $('.escorts-list-wrapper.agency').append($escortItem);
            });
            break;
          case 'latest-modified':
            vr.state.visibleEscorts.sort(function(a, b) {
              return new Date(b.date_last_modified) - new Date(a.date_last_modified);
            });

            $('.escorts-list-wrapper.agency').html( '')
           $.each(vr.state.visibleEscorts, function(idx, escort) {
              var $escortItem = vr._drawEscortItem(escort);
              $('.escorts-list-wrapper.agency').append($escortItem);
            });
            break;
          case 'newest-first':
            vr.state.visibleEscorts.sort(function(a, b) {
              return new Date(b.date_registered) - new Date(a.date_registered);
            });

            $('.escorts-list-wrapper.agency').html( '')
            $.each(vr.state.visibleEscorts, function(idx, escort) {
              var $escortItem = vr._drawEscortItem(escort);
              $('.escorts-list-wrapper.agency').append($escortItem);
            });
            break;
        }
        vr._bindAgencyEvents();

        // vr.state.visibleEscorts.sort()
      });
      
      var $searchBar = $('.search-bar').find('input');

      if (vr.state.searchTerm) {
        $searchBar.focus();

        setTimeout(function(){
          $searchBar.get(0).selectionStart = $searchBar.get(0).selectionEnd = 10000; }, 0);
      } else {
        //$searchBar.focus();
      }
    },

    _getInfoModalContent: function() {
      var vr = this;
      var $header = $('<h6/>', {
        'class': 'card-header',
        'text': 'Package added to a cart'
      });

      var modalTxt = '';
      var packageCnt = Object.keys(vr.state.cart.premium_packages).length;
      var modalTxt = '';

      if (vr.state.agencyDiscounts) {
        var discounts = vr.state.agencyDiscounts.sort(function (a, b) {
          return a.range[0] > b.range[0] ? 1 : -1
        });
        for (var i = 0; i < discounts.length; i++) {
          var el = discounts[i];
          
          if (packageCnt < el.range[0]) {
            modalTxt = '<h6 class="p-4">Buy '+ (el.range[0]-packageCnt) +' more packages to get '+el.discount +'% off.</h6>';
            break;
          }
        }
      }
      var $body = $('<div/>', {
        'class': 'card-body',
        'html': '<div>' + modalTxt + '</div>'
      });

      var $footer = $('<div/>', {
        'data-component': 'footer',
        'data-name': 'modalfooter',
        'class': 'footer d-flex justify-content-center py-3 flex-column align-items-center'
      });

    

      var $goToCartButton = $('<button/>', {
        'data-component': 'button',
        'data-name': 'gotocartbutton',
        'class': 'modal-button--gotocart btn btn-primary my-2 btn-lg',
        'text': 'Go to cart'
      });

      var $continueButton = $('<button/>', {
        'data-component': 'button',
        'data-name': 'continuebutton',
        'class': 'modal-button--continue btn btn-success my-2 btn-lg',
        'text': 'Continue shopping'
      });

      $footer.append($goToCartButton, $continueButton);
      $body.append($footer);

      return {
        $header: $header,
        $body: $body
      };
    },

    _getCartItem: function($container) {
      var premium_packageType = $container.find('input[name="premium_package_name"]:checked').val();
      var escortId = $container.find('input[name="escort_id"]').val();
      var premium_packageDuration = $container.find('input[name="premium_package_duration"]:checked').val();
      var premium_packageGender = $container.find('input[name="premium_package_gender"]').val();
      var packageBaseCity = $container.find('input[name="package_base_city"]').val();
      var escortShowname = $container.find('input[name="escort_showname"]').val();
      var escortPhoto = $container.find('input[name="escort_photo"]').val();
      
      var premium_packageActivationDate = $container.find('input[name="premium_package_start_date"]:checked').val();
      var activationDate = premium_packageActivationDate == 'scheduled' ? $('#premium_packageActivationDate').val() : null;

      var cartItem = new CartItemBuilder();
      

      cartItem
        .setAvailablePackages(this.state.available_packages)
        .setEscort(escortId)
        .setGender(premium_packageGender)
        .setType(premium_packageType)
        .setDuration(premium_packageDuration)
        .setActivationDate(activationDate)
        .setBaseCityId(packageBaseCity)
        .setEscortPhoto(escortPhoto);

      if (escortShowname) {
        cartItem.setShowname(escortShowname);
      }

      return cartItem;
    },

    _getModalContent: function(escort, premium_package) {
      var vr = this;
      var showname = '';

      if(escort){
        showname = '(' + escort.showname + ')';
      }

      var $header = $('<h6/>', {
        'class': 'modal-header',
        'text': !premium_package ? 'Set your Premium package' : 'Edit the Premium package ' + showname
       
      });

      var $body = $('<div/>', {
        'class': 'modal-body'
      });

      var $bodyRow1 = $('<tr/>', {
        'data-component': 'tr',
        'data-name': 'tablerow',
        'class': 'd-flex'
      });

      var $bodyRow2 = $bodyRow1.clone().addClass('duration d-flex justify-content-between d-flex flex-column flex-md-row');
      var $bodyRow3 = $bodyRow1.clone().addClass('flex-column flex-sm-row premium-package-datetime d-none d-md-flex');
      var $bodyRow4 = $bodyRow1.clone().addClass('paddingless d-flex');

      var countryISO = escort ?  escort.country_iso : vr.state.userData.country_iso;
      

      if (escort) {
        var inputs = '<input type="hidden" name="escort_showname" value="' + escort.showname + '" />';
      }else{
        var inputs ='';
      }

      $bodyRow4.append($('<div/>', {
        'data-component': 'td',
        'data-name': 'premium_packagedata',
        'class': 'td sc-input-block',
        'html': '<input type="hidden" name="premium_package_gender" value="' + (escort ? escort.gender : vr.state.userData.gender) + '" />' +
          '<input type="hidden" name="escort_id" value="' + (escort ? escort.id : vr.state.userData.id) + '" />' +
          '<input type="hidden" name="escort_photo" value="' + (escort ? escort.photo : '') + '" />' +
          '<input type="hidden" name="escort_photo" value="' + (escort ? escort.photo : '') + '" />' +
          '<input type="hidden" name="package_base_city" value="' + (escort ? escort.base_city_id : vr.state.userData.base_city_id) + '" />'
          + inputs
      }));

      if (vr.state.userData.agency_id){
        var arr = ['Premium', 'MonthlyPlus'];
}
 else {
        var arr = ['Premium'];
      }
      // if(headerVars.currentUser.user_type == 'escort' || (escort && escort.gender && escort.gender != 1) || escort.country_iso == 'us') {
      //   arr.pop();
      // }
                 
      $.each(arr, function(idx,premium_packageName) {
        $bodyRow1.append($('<div/>', {
          'data-component': 'td',
          'data-name': 'premium_packagedata',
          'class': 'td sc-input-block scheduled',
          'html': '<input type="radio" id="pn_' + premium_packageName + '" name="premium_package_name" value="' + premium_packageName + '" /><label class="" for="pn_' + premium_packageName + '">' + premium_packageName + '</label>'
        }));
      });

      if (premium_package) {
        $bodyRow1.find('#pn_' + premium_package.type).attr('checked', true);
      } else {

        $bodyRow1.find('input[type="radio"]:first').attr('checked', true);
      }
      // if (this.state.userData.id === 140261){
      //   $.each(['15+3', '30+6', '60+12', '90+18'], function(idx,duration) {
      //     $bodyRow2.append($('<div/>', {
      //       'data-component': 'td',
      //       'data-name': 'premium_packagedata',
      //       'class': 'td sc-input-block',
      //       'html': '<input type="radio" id="pd_'+ duration + '" name="premium_package_duration" value="' + duration + '" /><label for="pd_'+ duration + '"><span class="d-md-none">Premium </span>' + duration + ' <span class="">Days</span></label>'
      //     }));
      //
      //   });
      // }

       if (this.state.userData.gender != 2 && this.state.userData.gender != 3){
        $.each(['15', '30', '60', '90'], function(idx,duration) {

          $bodyRow2.append($('<div/>', {
            'data-component': 'td',
            'data-name': 'premium_packagedata',
            'class': 'td sc-input-block',
            'html': '<input type="radio" id="pd_'+ duration + '" name="premium_package_duration" value="' + duration + '" /><label for="pd_'+ duration + '"><span class="d-md-none">Premium </span>' + duration + ' <span class="">Days</span></label>'
          }));

        });
      }else{
        $.each(['30', '60', '90'], function(idx,duration) {

          $bodyRow2.append($('<div/>', {
            'data-component': 'td',
            'data-name': 'premium_packagedata',
            'class': 'td sc-input-block',
            'html': '<input type="radio" id="pd_'+ duration + '" name="premium_package_duration" value="' + duration + '" /><label for="pd_'+ duration + '"><span class="d-md-none">Premium </span>' + duration + ' <span class="">Days</span></label>'
          }));

        });
      }


      if (premium_package) {
        $bodyRow2.find('#pd_' + premium_package.duration).attr('checked', true);
      } else {
        $bodyRow2.find('input[type="radio"]').filter(':first').attr('checked', true);
      }

      var title;
      var packageClass = false;
      if (vr.state.premium_packages && (vr.state.premium_packages[0] && vr.state.premium_packages[0].expiration_date))
      {
        title = 'After current one expires';
        packageClass = true;
      }else {
        title = 'Starts now';
        packageClass = false;
      }

      $.each([{ title: title}, { title: 'Scheduled'}]
        ,function(idx,startDate) {
        var htmlInput = '';
        var packClass = '';

        if ( idx == 0 && packageClass )
        {
          packClass = 'pack-class';
        }

        if (startDate.title != 'Starts now' && startDate.title != 'After current one expires') {
          htmlInput = '<input type="hidden" disabled id="premium_packageActivationDate"/><div id="render-pikaday"></div><div id="to-show"></div>';
        }
        var sd = startDate.title.toLowerCase().replace(/ /g, '-');
        if (sd == 'after-current-one-expires')
        {
          sd = 'starts-now';
        }
        $bodyRow3.append($('<div/>', {
          'data-component': 'td',
          'data-name': 'premium_packagedata',
          'class': 'td sc-input-block',
          'html': '<input type="radio" id="psd_'+ sd + '" name="premium_package_start_date" value="' + sd + '"/>' +
            '<label class="'+ ((idx == 0) ? 'psd ' : '') +'psd_'+ sd + ' ' + packClass+ '" for="psd_'+ sd + '">' + startDate.title + '<div class="text-muted"></span></label>' + htmlInput
        }));
      });
      
      if (premium_package) {
        $bodyRow3.find('#psd_' + (!premium_package.activationDate ? 'starts-now' : 'scheduled')).attr('checked', true);
      } else {
        $bodyRow3.find('input[type="radio"]').filter(':first').attr('checked', true);
      }

      var $footer = $('<div/>', {
        'data-component': 'footer',
        'data-name': 'modalfooter',
        'class': 'footer d-flex justify-content-between pt-5'
      });

       var $amountTitle = $('<div/>', {
        'class': 'total-amount',
        'text': 'Total:'
      });

      var $amount = $('<div/>', {
        'data-component': 'block',
        'data-name': 'amount',
        'class': 'amount',
        'text': ''
      });

      var $buyButton = $('<button/>', {
        'data-component': 'button',
        'data-name': 'buy-button',
        'class': 'modal-button--amount btn btn-success rounded ml-auto btn-lg',
        'text': !premium_package ? 'ADD TO CART': 'DONE'
      });

      $footer.append($amountTitle,$amount, $buyButton);

      var $bodyRow2Title = $('<h6/>', {
        'class': 'duration_title d-block d-md-none py-2',
        'text': 'Duration'
      });

      $body.append( $bodyRow4, $bodyRow1, $bodyRow2Title, $bodyRow2,  $bodyRow3, $footer);

      return {
        $header: $header,
        $body: $body
      }
    },

    _canBuy: function(premium_packages) {
      var STATUS_PENDING = 1;
      var STATUS_ACTIVE = 2;
      
      if (premium_packages.length > 1) {
        return false;
      }

      /*if (premium_packages.length === 1 && premium_packages[0].status === STATUS_ACTIVE ) {
        return false;
      }*/

      /*if (premium_packages.length === 2) {
        return false;
      }*/

      return true;
    }
  }


  function Modal (params) {
    /**
     * params should contain { header, body }
     */
    return this.construct(params);
  };
  Modal.prototype = {
    construct: function (params) {
      this.params = params || {
        header: $('<div>'),
        body: $('<div>')
      };
  
      this.build().bindEvents();
  
      this.html.show = this._show;
      this.html.hide = this._hide;
  
      return this.html;
    },

    _buildOverlay: function () {
      return $('<div/>', {
        id: this.params.id || 'ModalOverlay',
        class: 'modal show',
        css: {
             display: 'block'
        }
      });
    },

    _buildModal: function () {
      return  $('<div/>', {
        class: 'modal-dialog'
      });
    },

    _buildCloseBtn: function() {
      return $('<div/>', {
        class: 'close-btn p-2',
        html: '&times;'
      });
    },
  
    build: function () {
      var Overlay = this._buildOverlay(),
        Modal = this._buildModal(),  
        dialog = $('<div/>', {class: "modal-dialog"});
    
      if (this.params.close) {
        this.closeBtn = this._buildCloseBtn();
        Modal.append(this.closeBtn);
      }
  
      Modal.append(this.params.header);
      Modal.append(this.params.body);
      Modal.wrapInner("<div class='modal-content'></div>");
    
      Overlay.append(Modal);
     
      this.Overlay = Overlay;
  
      $(document.body).append(Overlay);
  
      return this;
    },
  
    bindEvents: function () {
      var _self = this;
  
      if (this.closeBtn) {
        this.closeBtn.on('click', function() {
          _self.Overlay.remove();
        });
      }
  
      return this;
    },
  
    _show: function () {
      this.find('.Modal').addClass('shown');
    },
  
    _hide: function () {
      this.remove();
    },
  
    get html() {
      return this.Overlay;
    }
  };


  function loadPage () {
    var $container = $('#ui-view');
    $container.LoadingOverlay("show", {color : "rgba(239, 243, 249, 0.80)", zIndex: 1000   });

    $.ajax({
      url: 'private/billing/index',
      dataType: 'json',
      success: function(data){

        var bootstrap = new Bootstrap;

        if(data.user_type == 'escort'){
          if((data.escort_status & 8) == 8){
              _draw_notification('owner-disabled');
          }else if((data.escort_status & 67) == 67 && (data.escort_status & 128) != 128){
            _draw_notification('complete_7_steps');
          }
          else{
              bootstrap.run(data.user_type, data);
          }
        }else if(data.user_type == 'agency'){
           if(data.escorts.length > 0){
              bootstrap.run(data.user_type, data);
           }else{
              _draw_notification('escorts_no_found');
           }
           
        }
        return 0;
      },
      complete: function(){
         $container.LoadingOverlay("hide", true);
      }
    })
  };

  return {
    load: loadPage
  };
})(window);
Cubix.SelfCheckout.load();