var Settings = {

    init: function () {
        this.initForm();
        this.initCountries();
        this.initNewsletters();
        this.initProfileStatus();
    },

    initProfileStatus: function(){
    $('#profile-status').on('click', function(){
       $(".profile-status").LoadingOverlay("show", {color : "rgba(239, 243, 249, 0.80)", zIndex: 1000 });
        var action = $('#profile-status').attr('value');

        $.ajax({
         type: "GET",
         url: "private/profile-status?act=" + action,
         success: function(response){
            if(response){
              if(action == 'enable'){
                $('#profile-status').val("disable");
                $('.status-profile').text("active");
                $('.active-block').find('.alert-warning').removeClass('alert-warning').addClass('alert-success');
              }else{
               $('#profile-status').val("enable");
               $('.status-profile').text("owner disabled");
                  $('.active-block').find('.alert-success').removeClass('alert-success').addClass('alert-warning');
              }
            }
         },

         complete: function(e){
         $(".profile-status").LoadingOverlay("hide", true);
         }
      })
    })
  },

    initCountries: function () {
        $('select#country,select#city').select2({
            theme:'bootstrap'
        });

        $("#country").change(function () {
            var country_id = $(this).val();

            $.ajax({
                url: '/geography/ajax-get-cities',
                type: 'POST',
                data: {country_id:country_id},
                dataType:'json',
                beforeSend:function () {
                    $('#profile-settings').LoadingOverlay("show", true);
                },
                success: function (data) {

                    var city_area = $('#city');

                    city_area.find('option').remove();

                    $.each(data['data'],function (key,city) {
                       $('<option/>',{value:city.id,html:city.title}).appendTo(city_area);
                    });

                    $('#profile-settings').LoadingOverlay("hide", true);
                }
            });
        });
    },


    initForm:function () {
      $('#settings-form').submit(function (event) {
          event.preventDefault();

          var activeElement = $(document.activeElement);
          var action = activeElement.attr('id');
          var actionValue = activeElement.val();

          $('#action').attr({name:action,value:actionValue});

          var data = $(this).serializeArray();


          $.ajax({
              url: '/private/settings',
              type: 'POST',
              data: data,
              beforeSend:function () {
                  activeElement.closest('.card-body').LoadingOverlay("show", true);
              },
              success: function (data) {
                  $('#ui-view').html(data);
                  activeElement.closest('.card-body').LoadingOverlay("hide", true);

                  Settings.init();
              }
          });
      });

    },

    initNewsletters: function () {
        $('input[name="newsletters"]').change(function () {
           $(this).closest('form').submit();
        });
    }

};


$(document).ready(function () {
   Settings.init();
});