// Active ajax page loader
$.ajaxLoad = true;

$.defaultPage = '#index';
$.subPagesDirectory = 'private/';
$.page404 = 'profile/index';
$.mainContent = $('#ui-view');
$.isRunning = false;
$.ident = 1;
//Main navigation
$.navigation = $('nav > ul.nav.desktop-version');

$.panelIconOpened = 'icon-arrow-up';
$.panelIconClosed = 'icon-arrow-down';

Number.prototype.isNumeric = function (n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
};
'use strict';

var CheckDevice = {
    isMobile:function () {
        return ($(window).width() < 567);
    }
};

if ($.ajaxLoad) {
   
  var url = location.hash.replace(/^#/, '');
  if (url != '') {
    setUpUrl(url);
  }

  $(document).on('click', 'button[data-ajax-url],a[data-ajax-url],option[data-ajax-url]', function(e) {
      e.preventDefault();
      var target = $(e.currentTarget);
      $('.nav li .nav-link, .mob-pa-nav').removeClass('active');
      target.addClass('active');
      var targeturl = target.attr('data-ajax-url').replace(/\/private#?\/?/g, '').replace('#','/');
     routie(targeturl);
  });

    routie('private *', function(url) {
        $(".main").LoadingOverlay("show", {color : "rgba(239, 243, 249, 0.80)", zIndex: 1000   });
          setUpUrl(url);
    });
}

function setCookie(name,value,days) {
  var expires = "";
  if (days) {
    var date = new Date();
    date.setTime(date.getTime() + (days*24*60*60*1000));
    expires = "; expires=" + date.toUTCString();
  }
  document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}
function getCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for(var i=0;i < ca.length;i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') c = c.substring(1,c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
  }
  return null;
}
function eraseCookie(name) {
  document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

function setUpUrl(url,area) {
  if($.isRunning) return false;

  $.isRunning = true;
  if(area){
      $.mainContent = area;
  }
  $(".main").LoadingOverlay("show", {color : "rgba(239, 243, 249, 0.80)", zIndex: 1000   });



  $('.modal').modal('hide');
  loadPage(url);
}

function innerMenustart(response, url){
  window.location.hash = url;
  $.mainContent.html(response);
  innerjsroutes(url);
  innerStylesroutes(url);
  userNotification();
  $('html, body').animate({
      scrollTop: $("html").offset().top
  }, 1000);
}

function loadPage(url,area) {
  $.ajax({
    method: 'GET',
    url: $.subPagesDirectory + url,
    dataType: 'html',
    data: { ajax: true },
    cache : true,

    success: function(response) {
        try {
            response = JSON.parse(response);
            if (typeof response == 'object' && response['status'] == 'error') {
                if (typeof response.redirect_to_signin !== undefined) {
                    window.location.href = response.redirect_to_signin;
                }
            }
            if(typeof response == 'object'){
              innerMenustart(response, url);
            }
        } catch (e) {
          innerMenustart(response, url);
        }

      if($('body').hasClass('sidebar-mobile-show')) {
        $( "button.navbar-toggler" ).trigger( "click" );
      }
      
      $('.main').LoadingOverlay("hide", true);
      $.isRunning = false;
    },

    error : function() {
      window.location.href = $.page404;
    },
   
  });
}

function innerjsroutes(url){
  var jsFiles = '';

    if ( ~url.indexOf('verify/idcard' ) || ~url.indexOf('verify/index')) {
        url = 'verify';
    }else if ( ~url.indexOf('profile/' )) {
        url = 'profile';
    }else if( ~url.indexOf('tours' ) ){
       url = 'tours';
    }else if( ~url.indexOf('punter-board' ) ){
       url = 'punter-board/index';
    }
 
  switch (url) {

    case 'index':  
      jsFiles = [
          'js/slick.min.js',
          'js/private/punter.js',
          'js/private/profile-dash.js?v4' + Date.now()
      ];
    break;
    case 'agency-profile':
      jsFiles = [
      "js/private/datepair/jquery.timepicker.js",
      "js/private/datepair/datepair.min.js",
      "js/private/datepair/jquery.datepair.min.js",
      "js/private/profile-agency.js?v1",
      ];
    break;
    case 'billing/index/':
      jsFiles = [
       "js/private/datepair/lib/moment.js",
       "js/private/datepair/lib/moment-timezone-with-data-2012-2022.min.js",
       "js/private/datepair/lib/pikaday.js",
       "js/private/self-checkout.js?v="+Math.random(),
      ];
    break;
    case 'tours':  
      jsFiles = [
        "js/private/dataTables/jquery.dataTables.min.js",
        "js/private/dataTables/dataTables.bootstrap4.min.js",
        "js/private/dataTables/dataTables.responsive.min.js",
        "js/private/dataTables/responsive.bootstrap4.min.js",
        "js/private/datepair/bootstrap-datepicker.min.js",
        "js/private/datepair/bootstrap-datepicker.en-GB.min.js",
        "js/private/datepair/jquery.timepicker.js",
        "js/private/datepair/datepair.min.js",
        "js/private/datepair/jquery.datepair.min.js",
        "js/private/profile-tours.js"

      ];
    break;
    case 'profile':
      jsFiles = [
        "js/private/libs/select2.full.js",
        "js/private/libs/jquery.maskedinput.min.js",
        "js/private/libs/moment.min.js",
        "js/private/datepair/jquery.timepicker.js",
        "js/private/datepair/jquery.datepair.min.js",
        "js/private/libs/daterangepicker.min.js",
        "js/private/libs/advanced-forms.js?v1",

       // 'js/private/blueimp/load-image.all.min.js',
        'js/private/blueimp/jquery.ui.widget.js',
       // 'js/private/blueimp/jquery.iframe-transport.js',
        'js/private/blueimp/jquery.fileupload.js?v2',
        'js/private/blueimp/jquery.fileupload-process.js',
        'js/private/blueimp/jquery.fileupload-validate.js',
        'js/private/profile-steps.js?v1.4',
        'js/private/libs/sortable.js?v1.4',
        //+ Date.now()
       // 'js/private/cropper/rcrop.min.js'
      ];break;
    case 'verify':
      jsFiles = [
          "js/private/verification.js"
      ];
      break;
    case 'show-escorts':
      jsFiles = [
          'js/private/agency-escorts.js?v1.1'
      ];
      break;
    case 'phone-verification/index':
      jsFiles = [
        'js/jquery.inputmask.min.js',
        'js/private/phone-verification.js?v=2'
      ];
      break;
    case 'settings':
      jsFiles = [
          'js/private/libs/select2.full.js',
          'js/private/settings.js'
      ];
      break;
    case 'punter-board/index':
      jsFiles = [
          'js/private/punter.js',
          'js/private/notify.js'
      ];
      break;
    case 'client-blacklist':
      jsFiles = [
          "js/private/dataTables/jquery.dataTables.min.js",
          "js/private/dataTables/dataTables.bootstrap4.min.js",
          "js/private/dataTables/dataTables.responsive.min.js",
          "js/private/dataTables/responsive.bootstrap4.min.js",
          'js/private/datepair/bootstrap-datepicker.min.js',
          'js/private/client-blacklist.js',
      ];
      break;

    case 'support/index':
      jsFiles = [
          'js/private/libs/lightbox.js',
          'js/private/support.js'
      ];
      break;

    case 'premium':
      jsFiles = [
          'js/private/premium-models.js'
      ];
      break;

      case 'city-alerts/index':
      jsFiles = [
          'js/private/libs/select2.full.js',
          'js/private/member/city-alerts.js?v1'
      ];
      break;
       case 'alerts':
      jsFiles = [
          'js/private/libs/select2.full.js',
          'js/private/member/profile-alerts.js',
      ];
      break;
      case 'member-profile':
      jsFiles = [
          'js/private/libs/select2.full.js',
          'js/private/member/member-profile.js?v6'
      ];
      break;
      case  'happy-hour':
      jsFiles = [
          'js/private/happy-hour.js'
      ];break;
  }
  loadJS(jsFiles);
}
function innerStylesroutes(url){
   if( ~url.indexOf('tours' ) ){
    url = 'tours';
  }
  
  var styles = '';

  switch (url) {

    case 'tours':
      styles = [
        "css/private/responsive.bootstrap4.min.css",
        'css/private/bootstrap-datepicker.css',
      ];
      break;
    case 'billing/index/':
      styles = [
        "css/private/self-checkout.css?v=" + Date.now(),
        "css/private/pikaday.css",
      ];
      break;
    case 'client-blacklist':
      styles = [
        'css/private/bootstrap-datepicker.css',
        'css/private/responsive.bootstrap4.min.css',
      ];
      break;
    case 'profile/working-cities':
      styles = [
        'css/mapbox/mapbox-gl-v2.6.1.css',
        'css/mapbox/mapbox-gl-geocoder-v4.7.2.css',
      ];
      break;
    case 'support/index':
      styles = [
        'css/private/lightbox.css'
      ];
      break;
  }
  loadCSS(styles);
}

function userNotification(){
  $.ajax({
    method: 'POST',
    url: 'private/user-notifications',
    dataType: 'html',
    data: { ajax: true },
    cache : true,

    success: function(responseText) {
      $('.user-notifications').html('');
      $('.user-notifications').append(responseText);
    },

    error : function() {
     
    },
   
  });
}

function loadJS(jsFiles) {
  $('.inner-js-file').remove();
  for(var i = 0; i<jsFiles.length;i++){
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.async = false;
    script.src = jsFiles[i];
        // +'?rand='+Math.random();
    script.className = 'inner-js-file';
    document.body.appendChild(script);
  }
}

function loadCSS(styles){
  $('.inner-css-file').remove();
  for(var i = 0; i<styles.length;i++){
    var style = document.createElement('link');
    style.type = 'text/css';
    style.href = styles[i];
    style.rel  = 'stylesheet';
    style.className = 'inner-css-file';
    document.head.appendChild(style);
  }
}

/****
* MAIN NAVIGATION
*/

$(document).ready(function($){

  // Add class .active to current link - AJAX Mode off
  $.navigation.find('a').each(function(){

    var cUrl = String(window.location).split('?')[0];

    if (cUrl.substr(cUrl.length - 1) == '#') {
      cUrl = cUrl.slice(0,-1);
    }

    if ($($(this))[0].href==cUrl) {
      $(this).addClass('active');

      $(this).parents('ul').add(this).each(function(){
        $(this).parent().addClass('open');
      });
    }
  });

  // Dropdown Menu
  $.navigation.on('click', 'a', function(e){
    
    if ($.ajaxLoad) {
      e.preventDefault();
    }

    if ($(this).hasClass('nav-dropdown-toggle')) {
      $(this).parent().toggleClass('open');
      resizeBroadcast();
    }
  });

  
  $('nav > ul.nav').on('click', 'a', function(e){
    if ($(this).hasClass('nav-dropdown-toggle')) {
      $(this).parent().toggleClass('open');
      resizeBroadcast();
    }
  });

  function resizeBroadcast() {

    var timesRun = 0;
    var interval = setInterval(function(){
      timesRun += 1;
      if(timesRun === 5){
        clearInterval(interval);
      }
      window.dispatchEvent(new Event('resize'));
    }, 62.5);
  }

  /* ---------- Main Menu Open/Close, Min/Full ---------- */
  $('.navbar-toggler').click(function(){
    if ($(this).hasClass('sidebar-toggler')) {
      $('body').toggleClass('sidebar-hidden');
      resizeBroadcast();
    }

    if ($(this).hasClass('sidebar-minimizer')) {
      $('body').toggleClass('sidebar-minimized');
      resizeBroadcast();
    }

    if ($(this).hasClass('aside-menu-toggler')) {
      $('body').toggleClass('aside-menu-hidden');
      resizeBroadcast();
    }

    if ($(this).hasClass('mobile-sidebar-toggler')) {
      
      $('body').toggleClass('sidebar-mobile-show');
      resizeBroadcast();
      
    }
  });

  $('.sidebar-close').click(function(){
    $('body').toggleClass('sidebar-opened').parent().toggleClass('sidebar-opened');
  });

  /* ---------- Disable moving to top ---------- */
   $('a[href="#"][data-top!=true]').click(function(e){
    e.preventDefault();
  });

  $('.loged-user-data').on('show.bs.dropdown', function () {
    if ($(window).width() < 769) {
      if($('body').hasClass('sidebar-mobile-show')) {
        $( "button.navbar-toggler" ).trigger( "click" );
      }
    }
  });

    $(".chat-info-online-escorts").mCustomScrollbar({theme:"dark", autoHideScrollbar: true, scrollButtons:{ enable: true }});

});

/****
* CARDS ACTIONS
*/

$(document).on('click', '.card-actions a', function(e){
  e.preventDefault();

  if ($(this).hasClass('btn-close')) {
    $(this).parent().parent().parent().fadeOut();
  } else if ($(this).hasClass('btn-minimize')) {
    var $target = $(this).parent().parent().next('.card-block');
    if (!$(this).hasClass('collapsed')) {

      $('i',$(this)).removeClass($.panelIconOpened).addClass($.panelIconClosed);
    } else {
      $('i',$(this)).removeClass($.panelIconClosed).addClass($.panelIconOpened);
    }

  } else if ($(this).hasClass('btn-setting')) {
    $('#myModal').modal('show');
  }

});
