var Agency = {

  init:function() {
    this.iniSummernote();
    this.initTimepairs();
    this.initFormSubmit();
    this.initDaysjs();
    this.cityfilterbyCountry();
    this.initLogoUpload();
    this.initLogoDelete();
  },

   initFormSubmit:function(  ){
    var self = this;

    $('form').submit(function(event) {
      $(".main").LoadingOverlay("show", {color : "rgba(239, 243, 249, 0.80)", zIndex: 1000   });
      
      event.preventDefault();
      
      var values = $('form').serializeArray();
    
        $.ajax({
          type: "POST",
          url: "private/agency-profile?ajax=true",
          data: values,
          success: function(response, status, xhr){

           try {
            response = JSON.parse(response);
              if(typeof response =='object' && response['status'] == 'error' ){
                  if(typeof response.redirect_to_signin !== undefined){
                      window.location.href = response.redirect_to_signin;
                  }
               $.each( response['msgs'], function( key, value ) {
                $( "[name='"+key+"']" ).addClass('is-invalid');

                $('#validation-'+key).html(value);
              });
             }
           } catch (e) {
       
         $('#ui-view').html(response);
          self.init();
          Notify.alert('success','Your change has been saved!');
         }finally{
          $(".main").LoadingOverlay("hide", true);
         }
       }
     });
    });

  },

  iniSummernote: function(){
    document.emojiSource = '/js/private/emoji/';
      $('#summernote').summernote({
          height: 150,
          disableDragAndDrop: true,
          disableResizeEditor: true,
          shortcuts: false,
          toolbar: [
              ['misc', ['emoji', 'undo', 'redo']],
              ['style', ['bold', 'italic', 'underline',  'strikethrough','clear']],
          ],
          popover: { }
      });
  },

  initTimepairs: function(){
    
      $.each($('div[id^="datepair-"]'), function( key,val){
         
         $('#'+val.id+' .time').timepicker({
            'showDuration': true,
            'timeFormat': 'H:i'
        });

        $('#'+val.id).datepair();
      });
  },

  initDaysjs: function(){
       $.each($('div[id^="datepair-"]'), function( key,val){
         $('#'+val.id+' .day-checkbox').on('click', function(){
          if( $(this).is(':checked') ) {
            $('#'+val.id+' .start').attr('readonly', false);
            $('#'+val.id+' .end').attr('readonly', false);
          }else{
            $('#'+val.id+' .start').val('');
            $('#'+val.id+' .end').val('');
            $('#'+val.id+' .start').attr('readonly', true);
            $('#'+val.id+' .end').attr('readonly', true);
          };
         });
      })

        $( "[name='available_24_7']" ).on('change', function(){
        
          if( $(this).val() == 1 ) {
            $('.day-checkbox').prop('checked', false);
            $('.day-checkbox').prop('disabled', true);
            $('.start').attr('readonly', true);
            $('.end').attr('readonly', true);
            $('.start').val('');
            $('.end').val('');
            $('.schedule-by-week').addClass('d-none d-md-block');
          }else{
             $('.schedule-by-week').removeClass('d-none d-md-block');
             $('.day-checkbox').prop('disabled', false);
          };
        });
  },

  cityfilterbyCountry: function(){
    $( "[name='country_id']" ).on('change', function(){
       $(".country-city").LoadingOverlay("show", {color : "rgba(239, 243, 249, 0.80)", zIndex: 1000   });
      
       $.ajax({
          type: "POST",
          url: "geography/ajax-get-cities?country_id=" + $(this).val(),
         
          success: function(response, status, xhr){
           try {
            response = JSON.parse(response);
            if(typeof response =='object'  ){
               $( "[name='city_id']" ).children('option').remove();
               $( "[name='city_id']" ).append('<option value=""> --- </option>');
               $.each( response['data'] , function( key, value ){
                
               $( "[name='city_id']" ).append('<option value="' + value.id + '">' + value.title +'</option>');
             });
            }
           }catch (e) {
            
           }finally{
            $(".country-city").LoadingOverlay("hide", true);
           }
       }
     });
        
    });
  },

    initLogoUpload : function () {
        $('.agency-logo').on('click',  function(event) {
          $( "#upload_logo" ).trigger( "click" );
        });
        $('#upload_logo').change(function(){

            $(this).simpleUpload("/private/agency-logo?a=upload&ajax=true", {
                limit: 1,
                start: function(file){
                  $('#filename').html(file.name);
                  $('#progress').html("");
                  $('#progressBar').width(0);
                },

                progress: function(progress){
                    $('#progress').html("Progress: " + Math.round(progress) + "%");
                    $('#progressBar').width(progress + "%");
                },

                success: function(data){
                    data = JSON.parse(data);

                    if(data.success){

                       console.log($('.agency-logo'),'-',data.image);
                       $('.agency-logo').attr('src',data.image);

                       $('#progress').html("<span class='text-success'>Success!</span>");
                    }else{
                        $('#progress').html("<span class='text-danger'>"+ data.error +"</span>");
                    }

                    $('#delete_logo').show();

                },

                error: function(error){
                    $('#progress').html("Failure!<br>" + error.name + ": " + error.message);
                }

            });

        });
    },

    initLogoDelete : function () {
        $('#delete_logo').click(function () {
            var self = $(this);
            $.ajax({
               url:'/private/agency-logo',
               type:'POST',
               data:{
                   ajax:'true',
                   d:'delete'
               },
               success : function (data) {
                   data = JSON.parse(data);

                   $('.agency-logo').attr('src',data.image);
                   $('#filename').html(' ');
                   $('#progress').html(' ');
                   $('#progressBar').width(0);
                   self.hide();
               }
            });

        });
    }

};


$(document).ready(function(){
  Agency.init();
}); 
