/**
 * Created by Zhora on 23.10.2017.
 */

var Notify = {

    alert: function (type, message ,delay) {


        $.notify({
                message: '<strong>' + message + '<strong>'
            }, {
                type: type,
                delay: (delay) ? delay : 3000,
                animate: {
                    enter: 'animated fadeIn',
                    exit: 'animated fadeOut'
                },
                placement: {
                    from: "top",
                    align: "right"
                },
                offset: {
                    x: (screen.width > 576) ? 20 : 0,
                    y:70,
                    spacing: 10,
                    z_index: 1031

                }
            }
        );

    }

};
