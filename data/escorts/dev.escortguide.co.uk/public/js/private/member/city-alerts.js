/**
 * Created by Zhora on 24.10.2017.
 */

var CityAlerts = {
    init:function () {
        this.initForm();
        this.initRemove();
    },

    initForm:function () {
        $('select').select2({
            theme:'bootstrap'
        });

        $('#city-alerts-form').submit(function (event) {
           event.preventDefault();
           var self = $(this);

           //destroy old errors
           $('.error-area').html(' ');

           var data = $(this).serializeArray();

           $.ajax({
              url:'/private/city-alerts/add',
              type:'POST',
              data:data,
              beforeSend:function () {
                  self.closest('.card-body').LoadingOverlay('show',true);
              },
              success:function (resp) {

                  try{
                      resp = JSON.parse(resp);

                      $.each(resp['msgs'],function (key, value) {
                          $('#'+key).html(value);
                      });


                  }catch(err){

                      $('#ui-view').html(resp);
                      CityAlerts.init();

                      Notify.alert('success',"Your change has been saved!");
                  }

                  self.closest('.card-body').LoadingOverlay('hide',true);

              }
           });


        });
    },

    initRemove:function () {
        $('.remove_city_alert').click(function () {
            var self = $(this);
            var row = self.closest('tr');
            var id = self.attr('data-id');

            $.ajax({
                url:'/private/city-alerts/remove',
                type:'POST',
                data:{id:id},
                async:false,
                success:function (resp) {
                    row.remove();
                }
            });
        });
    }

};


$(document).ready(function () {
    CityAlerts.init();
});
