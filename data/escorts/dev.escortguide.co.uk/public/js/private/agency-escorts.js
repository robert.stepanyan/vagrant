/**
 * Created by SceonDev on 11.10.2017.
 */
var AgencyEscorts = {

    action : function (type,active,escort_id,elem) {

        var current_tab = $('.nav-link.active').attr('data-value');

        $.ajax({
            'url':'/private/ajax-escorts-do',
            'type':'POST',
            'data':{
                a:type,
                escort_id:escort_id,
                is_active:active
            },
            'dataType':'json',

            beforeSend:function () {
                $('.main').LoadingOverlay("show", true);
            },
            success: function (data) {

                $('.escort_preview').modal('hide');
                if (!data.success && data.message != '') {
                    if (!$('#escort-notification').length) {
                        $('<div/>', {
                            'class': 'text-danger',
                            id: 'escort-notification'
                        }).html(data.message).appendTo('.notification_escort .modal-body');
                    } else {
                        $('#escort-notification').html(data.message).appendTo('.notification_escort .modal-body');
                    }

                    $('.notification_escort').modal('show');
                    $('.main').LoadingOverlay("hide", true);
                    return false;
                }

                AgencyEscorts.load({status: current_tab},true);
                $('.main').LoadingOverlay("hide", true);
            }

        });
    },

    load : function (object,without_loader) {

        object['showname'] = $('#search-escort').val();


        if (!object.status) {
            $('.active').removeClass('active');
            $('a[data-value="active"]').addClass('active');
            $('#nav-active').removeClass('fade').addClass('active');
            object.status = 'active';

        }

        $.ajax({
            'url':'/private/escorts',
            'type':'POST',
            'data':object,
            beforeSend:function () {
                if(!without_loader){
                    $('#manage-models').LoadingOverlay("show", true);
                }
            },
            success:function (data) {
                if(!without_loader){
                    $('#manage-models').LoadingOverlay("hide", true);
                }

                $('#nav-active').html(' ');
                $('#nav-disabled').html(' ');
                $('#nav-deleted').html(' ');

                $('#nav-'+object.status).html(data);
                AgencyEscorts.initPreview();


                $('#move-package').on('show.bs.modal', function (e) {
                   var packageName = $(e.relatedTarget).data('package-name');
                   var packageEscortfrom = $(e.relatedTarget).data('package-escort');
                   var packageCities = $(e.relatedTarget).data('premium-city');

                   $('.modal-package-name').html(packageName );
                   $('#modal-package-from-escort-id').val(packageEscortfrom);
                   $('#modal-package-cities').val(packageCities);

                   $('#submit-moving-package').on('click', function(event) {
                       event.preventDefault();
                       $('#modal-move-package').LoadingOverlay("show", true);

                        var submitedEscortID = $('#modal-package-from-escort-id').val();
                        var assignpackageEscortid = $( "#to_escort_id option:selected" ).val();
                        var submitedPackageCities = $('#modal-package-cities').val();

                        if(isNaN(submitedPackageCities)){
                            submitedPackageCities = submitedPackageCities.split(',');
                        }else{
                            submitedPackageCities = [submitedPackageCities];
                        }
                                      
                        $.ajax({
                            url: '/private/billing/move-package',
                            type: 'POST',
                            data: {from_escort_id: packageEscortfrom, to_escort_id: assignpackageEscortid, city_ids: submitedPackageCities},
                            success: function(data){
                                var result = JSON.parse(data);

                                if(result.status == "success"){
                                    Notify.alert('success',"Package move request has been successfully placed ");
                                }else{
                                    Notify.alert('danger', result.result ? result.result : "Unexpected error");
                                }
                                AgencyEscorts.load({status:'active'});   
                            },
                            complete: function(){
                                $('#move-package').modal('hide');
                                $('#modal-move-package').LoadingOverlay("hide", true);
                            }
                        })
                                           
                   });
                }),

                $('#move-package').on('hide.bs.modal', function (e) {
                    $('#submit-moving-package').unbind();
                })
            }

        });

    },

    initPreview: function () {
        $('.p-escort img').click(function (e) {

           if(!CheckDevice.isMobile()) return false;

           e.preventDefault();
           var escort_data = JSON.parse( $(this).next('.escort_data').val() ) ;
           
           $('.escort_preview .list-group a').hide();
           $('.escort_preview .list-group a').unbind( "click" );
        

           $('.escort_preview  h5.escort-showname').html(escort_data.showname);
           $('.escort_preview  .escort_preview-id').html('ID: '+escort_data.id);
           if(escort_data.package != null){
               if (escort_data.package == 'Free Monthly Package'){
                   $('.escort_preview  .escort_preview-package').html(' <b>'+escort_data.package+' </b>');
               }
               else {
                   $('.escort_preview  .escort_preview-package').html(' <b>'+escort_data.package+' days </b>');
               }
                $('.escort_preview  .escort_preview-package_act_date').html(' <b>Expires on: '+escort_data.expiration_date+'</b>');
           }else{
                 $('.escort_preview  .escort_preview-package').html(' <b>No package</b>');
           }
           $('.escort_preview  img.escort-image').attr('src', escort_data.photo_url);

           if(escort_data.status == 'active'){
            $('.escort_preview .btn-prev').show();
            $('.escort_preview .btn-prev').attr('href', escort_data.public_url);
           }
           if(escort_data.is_suspicious == 0){
            $('.escort_preview .btn-edit').show();
            $('.escort_preview .btn-edit').attr('data-ajax-url', '/private/profile#index?escort='+escort_data.id);
           }
           
            $('.escort_preview .btn-tours').show();
            $('.escort_preview .btn-tours').attr('data-ajax-url', '/private#tours/index/?escort='+escort_data.id);

            $('.escort_preview .btn-gallery').show();
            $('.escort_preview .btn-gallery').attr('data-ajax-url', '/private/profile#gallery?escort='+escort_data.id);

            $('.escort_preview .btn-100-verification').show();
            $('.escort_preview .btn-100-verification').attr('data-ajax-url', '/private/verify#idcard?id='+escort_data.id);
          
            if(escort_data.status == 'active'){
                $('.escort_preview .btn-deactivate').show();
                $('.escort_preview .btn-deactivate').click( function(){
                    AgencyEscorts.action('deactivate',1,escort_data.id,$(this));
                })
            }
         
            if(escort_data.package !== null){
               $('.escort_preview .btn-move-package').show();
                $('.escort_preview .btn-move-package').on('click',  function(event) {
                    event.preventDefault();
                    $('.escort_preview').modal('hide');
                    $('#movepackage-'+escort_data.id).trigger('click');
                   
                });
            }
            if (escort_data.has_pending !== 1 && escort_data.package !==null){
                $('.escort_preview .btn-extend-package').show();
                $('.escort_preview .btn-extend-package').on('click',  function(event) {
                    $('.escort_preview').modal('hide');
                    location.replace("/private#billing/index/");
                });
            }
            else{
                $('.escort_preview .btn-move-package').hide();
                $('.escort_preview .btn-move-package').unbind();
            }

            if(escort_data.status == 'disabled'){
                $('.escort_preview .btn-activate').show();
                $('.escort_preview .btn-activate').click( function(){
                    AgencyEscorts.action('activate',1,escort_data.id,$(this));
                })
            }

            if(escort_data.package != 'null' && escort_data.status != 'deleted'){
                $('.escort_preview .btn-delete').show();
                $('.escort_preview .btn-delete').click(function(){
                    AgencyEscorts.action('delete',1,escort_data.id,$(this));
                })
            }

            if(escort_data.status == 'deleted'){
                $('.escort_preview .btn-restore').show();
                $('.escort_preview .btn-restore').click( function(){
                    AgencyEscorts.action('restore',1,escort_data.id,$(this));
                })
            }


           $('.escort_preview').modal('show');
        });
    }
};



$(document).ready(function () {
    AgencyEscorts.load({status:'active'});   
});