'use strict';

function loadBubbleTexts(data) {
    let pageMatches = location.hash.match(new RegExp('page=([0-9]+)')),
        page = pageMatches ? pageMatches[1] : 1;

    let ajaxFilteredMatches = location.hash.match(new RegExp('ajax_filtered=1')),
        homePageMatches = location.hash.match(new RegExp('isHomePage=1'));

    let singleBubbleHeight = 120,
        escortDivHeight = 440,
        escortRowsCount = page > 1 || ajaxFilteredMatches || homePageMatches ? 20 : 37,

        adRowsCountInEscortRows = 5,
        adRowDivHeight = 130,

        bannerListHeight = page > 1 ? 100 : 400,
        subscribeNewsletterDivHeight = $('#is-logged-in').val() === 'false' ? 170 : 0;

    data.countToLoad = parseInt((escortRowsCount * escortDivHeight - adRowsCountInEscortRows * adRowDivHeight - bannerListHeight - subscribeNewsletterDivHeight) / singleBubbleHeight);

    $('.bubble-texts').LoadingOverlay("show", {color: "rgba(239, 243, 249, 0.80)", zIndex: 1000});
    $.ajax({
        url: '/bubble',
        type: 'POST',
        dataType: "html",
        data: data,
        success: function (resp) {
            $('.bubble-texts').html(resp);
        },
        complete: function () {
            $('.bubble-texts').LoadingOverlay("hide", true);
        }
    });
}

$(document).ready(function () {
    if (!$('.bubble-p').length) {

        setTimeout(function () {
            loadBubbleTexts({page: 1, ajax: true});
        }, 1000);
    }

    if ($('.bubble-pagination .page-item')) {
        $('body').on('click', 'bubble-pagination .page-item', function (e) {
            e.preventDefault();
            $('#bubble-area').LoadingOverlay("show", {color: "rgba(239, 243, 249, 0.80)", zIndex: 1000});
            let page = $(e.target).data('page');
            var city_id = $('.bubble-city-select').val();
            $.ajax({
                url: '/status-messages',
                type: 'POST',
                dataType: "html",
                data: {page: page, ajax: true, city_id: city_id},
                success: function (resp) {
                    $('.bubbles').html(resp);
                },
                complete: function () {
                    $('#bubble-area').LoadingOverlay("hide", true);
                }
            });
        });
    }

    if ($('.bubble-pagination .page-item-bar'))
    {
        $('body').on('click','.bubble-pagination .page-item-bar',function (e){
            e.preventDefault();
            e.stopPropagation();
            let page = $(e.target).data('page'),
                city_id = $('.bubble-city-select').val();

            loadBubbleTexts({page: page, city_id, ajax: true})
        });
    }

    if ($('.bubble-search'))
    {
        $('body').on('click','.bubble-search',function (e){
            e.preventDefault();
            $('.bubble-texts').LoadingOverlay("show", {color: "rgba(239, 243, 249, 0.80)", zIndex: 1000});
            var city_id = $('.bubble-city-select').val();
            $.ajax({
                url: '/status-messages',
                type: 'POST',
                dataType: "html",
                data: {city_id: city_id, ajax: true},
                success: function (resp) {
                    $('.bubbles').html(resp);
                },
                complete: function () {
                    $('.bubble-texts').LoadingOverlay("hide", true);
                }
            });
        });
    }
});