$( document ).ready(function() {
//Agency Profile Escort Slider
var loadedPhotosCount = 0;
var photosCount = $('.review-template-slider img').length;

$('.review-template-slider img').on('load', function() {
  loadedPhotosCount++;
  if (loadedPhotosCount == photosCount) {
    $('.review-template-slider img').show('slow');
    initreviewEscortSlider();
    
  }
}).each(function() {
  if (this.complete) {
    $(this).trigger('load');
  }
});

//review template ad slider
function initreviewEscortSlider() {
   $('.review-template-slider').slick({
    infinite: false,
    slidesToShow: 3,
    slidesToScroll: 2,
     responsive: [
      {
        breakpoint: 768,
        settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        }
      },
    ]
 
 });
}
//slider end  

var cityfilter = {
    init: function(){
    $( "[name='country_id']" ).on('change', function(){
       $(".country-city").LoadingOverlay("show", {color : "rgba(239, 243, 249, 0.80)", zIndex: 1000   });
      
       $.ajax({
          type: "POST",
          url: "/geography/ajax-get-cities?country_id=" + $(this).val(),
         
          success: function(response, status, xhr){
           try {
            response = JSON.parse(response);
           
            if(typeof response =='object'  ){
               $( "[name='city_id']" ).children('option').remove();
               $( "[name='city_id']" ).append('<option value=""> --- </option>');
               $.each( response['data'] , function( key, value ){
                
               $( "[name='city_id']" ).append('<option value="' + value.id + '">' + value.title +'</option>');
             });
            }
           }catch (e) {
            
           }finally{
            $(".country-city").LoadingOverlay("hide", true);
           }
       }
     });
        
    });
  },
};

var loadReviews = {
    dataArea : '',
    init: function(){
        this.dataArea = $('#reviews_by_country');
        var self = this;

        if(!self.dataArea) return false;

        var city_id = $('#data_city_id').val();
        var without_escort = $('#escort_id').val();

        $.ajax({
           url:'/reviews',
           type:'get',
           beforeSend:function () {
               self.dataArea.LoadingOverlay('show', true);
           },
           data:{
               ajax:true,
               fromEscortReview:true,
               perPage:5,
               r_city:city_id,
               without_escort:without_escort
           },
           success :function (resp) {
               self.dataArea.html(resp);
               self.dataArea.LoadingOverlay('hide', true);
           }
        });
    }
};

loadReviews.init();
cityfilter.init();

$('#m_date, #t_meeting_date').datepicker({
    language: "en-GB",
    endDate: new Date(),
    todayHighlight: true,
    autoclose: true,
    format: 'dd M yyyy',
 });

});
