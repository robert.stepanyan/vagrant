<?php

$srv = 'unknown';
if ( preg_match('#\.([0-9]+)$#', $_SERVER['SERVER_ADDR'], $match) ) { 

	switch ( (int) $match[1] ) {

		case 11:

			$srv = 'web1';

			break;

		case 51:

			$srv = 'pic';

			break;

		case 13:

			$srv = 'web3';

			break;

	}

}

header('Cubix-Server: ' . $srv);


if ( preg_match('#\.test$#', $_SERVER['SERVER_NAME']) > 0 ) {
	define('IS_DEBUG', true);
}
else {
	define('IS_DEBUG', false);
}

$host = $_SERVER['HTTP_HOST'];

$host = explode('.',$host);


//mb_internal_encoding('UTF-8');


$app_env = 'production';
if ( preg_match('#\.test$#', $_SERVER['SERVER_NAME']) ) {
    $app_env = 'development';
}

define('APPLICATION_ENV', $app_env);


/*require '../../library/Cubix/Debug.php';

require '../../library/Cubix/Debug/Benchmark.php';



if ( isset($_REQUEST['benchmark']) ) Cubix_Debug::setDebug(true);

Cubix_Debug_Benchmark::setLogType(Cubix_Debug_Benchmark::LOG_TYPE_DB);

Cubix_Debug_Benchmark::setApplication('frontend');

Cubix_Debug_Benchmark::start($_SERVER['REQUEST_URI']);

*/



/**

 * Used to construct static server url

 *

 * This function will automatically decide which folder to prefix to the given

 * parameter by examining it's extension

 *

 * /img/ - png, gif, jpg

 * /js/ - js

 * /css/ - css

 */

function _st($object)
{
	$matches = array();
	if ( 0 == preg_match('#\.([a-z]{2,4})/?#', $object, $matches) ) {
		throw new Exception('Invalid object, expected a filename with extension');
	}


	$base_url = 'https://'.$_SERVER['HTTP_HOST'];
	if ( defined('IS_DEBUG') && IS_DEBUG ) {
		$base_url = 'http://' . $_SERVER['HTTP_HOST'];
	}
	array_shift($matches);
	list($ext) = $matches;

	switch ( $ext ) {
		case 'jpg':
		case 'gif':
		case 'png':
			$prefix = '/img/';
			break;
		case 'css':
			$prefix = '/css/';
			break;
		case 'js':
			$prefix = '/js/';
			break;
		case 'php':
			$prefix = '/';
			break;
		default:
			throw new Exception('Invalid object, unsupported extension "' . $ext . '"');
	}

	$url = $base_url . $prefix . $object;
	return $url;
}

// Define path to application directory

defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment

defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path

set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../../library'),
	realpath(APPLICATION_PATH . '/../../../usr/lib'),
    get_include_path(),

)));



/** Zend_Application */

require_once 'Zend/Application.php';
require_once 'Cubix/Mobile_Detect.php';

$detect = new Mobile_Detect;
define('IS_MOBILE_DEVICE', ($detect->isMobile() && !$detect->isTablet()) ? true : false);

// Create application, bootstrap, and run

$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

$mtime = microtime();
$mtime = explode(" ",$mtime);
$mtime = $mtime[1] + $mtime[0];
$GLOBALS['exec_start_time'] = $mtime;


//	error_reporting(E_ALL);
//	ini_set('display_errors', '1');

try {
	$application->bootstrap()->run();
}
catch ( Exception $e ) {
	$error = '[' . date('d.m.Y H:i:s') . ']: URL: ' . $_SERVER['REQUEST_URI'] . "\n";
	$error .= get_class($e) . "\n";
	$error .= $e->__toString();
	$error .= "\n------------------\n";
	file_put_contents('debug.log', $error, FILE_APPEND);
	header("HTTP/1.1 404 Not Found");
	echo 'Unexpected error!';
}

/*

Cubix_Debug_Benchmark::end($_SERVER['REQUEST_URI']);



if ( Cubix_Debug::isDebug() ) {

	Cubix_Debug_Benchmark::log();

}

*/