/* --> Photos */
Cubix.Bubble = {};

Cubix.Bubble.container = 'bubbles-widget';
Cubix.Bubble.toScrolling = true;

Cubix.Bubble.Load = function (page) {
	if ( undefined == page || page < 1 ) page = 1;
	var url = '/ajax-bubble?page=' + page;
	Cubix.Bubble.Show(url);
    Cubix.Bubble.OnScroll();
	
	return false;
};

Cubix.Bubble.Show = function (url) {
    var container = $('bubbles-widget');
	var overlay = new Cubix.Overlay($(Cubix.Bubble.container), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	//overlay.disable();
    Cubix.Bubble.toScrolling = false;
    container.tween( 'opacity', 0.3 );
	
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {
            var bubble_content = $(Cubix.Bubble.container).get('html');
			$(Cubix.Bubble.container).set('html', bubble_content + resp);
			//overlay.enable();
            Cubix.Bubble.toScrolling = true;
            container.tween( 'opacity', 1 );
		}
	}).send();
};

Cubix.Bubble.OnScroll = function (){
    window.addEvent('scroll', function(){
        var windowPosTop = window.getScrollTop();
        var elmPos = $('bubbles-widget').getCoordinates();
        var elmPosBottom = elmPos.bottom;
        var currentPage = Number( $$('.bubble_i').get('data-page') );

        if( Cubix.Bubble.toScrolling ){
            if( windowPosTop > elmPosBottom - 350 ){
                var page = currentPage + 1;
                $$('.bubble_i').set('data-page', page );

                elmPosBottom = windowPosTop;
                Cubix.Bubble.Load( page );
            }
        }

        //console.log( $$('.bubble_i').get('data-page') );
    });
};
