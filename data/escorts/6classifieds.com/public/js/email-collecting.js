Cubix.EmailCollectingPopup = {};

Cubix.EmailCollectingPopup.inProcess = false;
Cubix.EmailCollectingPopup.url = '';
Cubix.EmailCollectingPopup.domain = '.6anuncio.com';

Cubix.EmailCollectingPopup.Show = function (box_height, box_width) {
	
	if ( Cubix.EmailCollectingPopup.inProcess ) return false;

	/*var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000' });
	page_overlay.disable();*/

	var y_offset = 130;

	var container = new Element('div', { 'class': 'email-collecting-wrapper'}).setStyles({
		left: window.getWidth() / 2 - box_width / 2,
		top:  window.getHeight() / 2 - box_height / 2 + window.getScroll().y - y_offset,
		opacity: 0,
        position: 'absolute',
        'z-index': 150
	}).inject(document.body);
	
	Cubix.EmailCollectingPopup.inProcess = true;

	new Request({
		url: Cubix.EmailCollectingPopup.url,
		method: 'get',
		onSuccess: function (resp) {
			
			Cubix.EmailCollectingPopup.inProcess = false;
			container.set('html', resp);

			var close_btn = new Element('div', {
				html: '',
				'class': 'email-collecting-close-btn'
			}).inject(container);

			close_btn.addEvent('click', function() {
				Cookie.write('ec_session_closed', true, {domain: Cubix.EmailCollectingPopup.domain});
				$$('.email-collecting-wrapper').destroy();
				page_overlay.enable();
			});

			container.tween('opacity', '1');
			
			var forms = container.getElements('form');
			forms.addEvent('submit', Cubix.EmailCollectingPopup.Send);
			forms.each(function (form) { form.set('action', form.get('action') + (form.get('action').indexOf('?') != -1 ? '&' : '?') + 'ajax'); });
		}
	}).send();
		
	
	return false;
}

Cubix.EmailCollectingPopup.Send = function (e) {
	e.stop();
   
	var overlay = new Cubix.Overlay($$('.email-collecting-wrapper')[0], { loader: _st('loader-small.gif'), position: '50%', no_relative: true });
	overlay.disable();

	this.set('send', {
		onSuccess: function (resp) {

			resp = JSON.decode(resp);

			overlay.enable();
			$$('.err').set('html', '');
			if ( resp.status == 'error' ) {
				$$('.err').set('html', resp.msg);
			}
			else if ( resp.status == 'success' ) {
				$$('.email-collecting-wrapper').destroy();
				$$('.overlay').destroy();
				
				Cookie.write('email_collecting', 'done', {domain: Cubix.EmailCollectingPopup.domain, duration: 365});
			}
		}.bind(this)
	});

	this.send();
}