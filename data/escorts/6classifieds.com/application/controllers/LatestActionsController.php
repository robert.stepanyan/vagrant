<?php

class LatestActionsController extends Zend_Controller_Action
{
	public function init()
	{
		$this->view->layout()->setLayout('latest-actions');
		
		$this->model = new Model_LatestActions();
		
		$this->client = new Cubix_Api_XmlRpc_Client();
		
		$this->view->perRow = 5;
	}
	
	public function indexAction()
	{
		$defines = $this->view->defines = Zend_Registry::get('defines');
		$this->view->cities = $this->model->getAllCitiesForFilter(Cubix_Application::getById()->country_id);
		
		$filter = array();
		
		$this->view->perPage = $perPage = 30;
		$this->view->page = $page = 1;
		
		$escorts = $this->model->getAll($filter, $page, $perPage, $count);
		
		$this->view->escorts = $escorts;
		$this->view->count = $count;
		
		$this->view->action_counts = $this->model->getActionCounts($filter);
	}
	
	public function ajaxHeaderAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$defines = $this->view->defines = Zend_Registry::get('defines');
		$this->view->cities = $this->model->getAllCitiesForFilter(Cubix_Application::getById()->country_id);
		
		$this->view->city = $city = (int)$req->city;
		$this->view->action_type = $action_type = (int)$req->action_type;
		
		$filter = array();
		
		$city = (int)$req->city;
		$action = (int)$req->action_type;
		
		if ( $city ) {
			$filter['city_id'] = $city;
		}
		
		if ( $action ) {
			$filter['action_type'] = $action;
		}
		
		$this->view->action_counts = $this->model->getActionCounts($filter);
	}
	
	public function ajaxListAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$defines = $this->view->defines = Zend_Registry::get('defines');
		
		$this->view->perPage = $perPage = 30;
		$page = $req->page;
		if ( $page < 1 ) {
			$page = 1;
		}
		$this->view->page = $page;
		
		$filter = array();
		
		$city = (int)$req->city;
		$action = (int)$req->action_type;
		
		if ( $city ) {
			$filter['city_id'] = $city;
		}
		
		if ( $action ) {
			$filter['action_type'] = $action;
		}
		
		$this->view->escorts = $this->model->getAll($filter, $page, $perPage, $count);
		$this->view->count = $count;
	}
}