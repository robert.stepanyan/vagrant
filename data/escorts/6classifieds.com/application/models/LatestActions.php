<?php

class Model_LatestActions extends Cubix_Model
{
	public static function getTotalCount()
	{
		$cache = Zend_Registry::get('cache');		
		$cache_key = 'latest_action_total_count_' . Cubix_Application::getId();
		
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
					count(d.id) 
				FROM latest_actions d
				INNER JOIN escorts e ON e.id = d.escort_id
				INNER JOIN escorts_in_cities eic ON eic.escort_id = d.escort_id
				INNER JOIN cities c ON c.id = eic.city_id
				WHERE 1 and eic.is_base = 1
			GROUP BY e.id, d.action_type;
		';
		
		if ( ! $total_count = $cache->load($cache_key) ) {
			
			self::db()->fetchAll($sql);
			$total_count = self::db()->fetchOne('SELECT FOUND_ROWS()');
			
			$cache->save($total_count, $cache_key, array());
		}
		
		$zero_count = 4 - strlen($total_count);
		
		if ( $zero_count ) {
			for ( $i = 0; $i < $zero_count; $i++ ) {
				$total_count = '0' . $total_count;
			}
		}
		
		return $total_count;
	}
	
	public function getActionCounts($filter = array())
	{	
		$cache = Zend_Registry::get('cache');		
		$cache_key = 'latest_action_counts_' . Cubix_Application::getId() . '_' . (int) $filter['city_id'];
		
		$defines = Zend_Registry::get('defines');
		
		$where = '';
		
		if ( isset($filter['city_id']) && $filter['city_id'] ) {
			$where .= ' AND eic.city_id = ' . $filter['city_id'] . ' ';
		}
		
		$actions = $defines['latest_actions_titles'];
		$action_counts = array();
		
		if ( ! $action_counts = $cache->load($cache_key) ) {
			
			foreach ( $actions as $id => $action ) {
				$sql = '
					SELECT
						COUNT(DISTINCT d.escort_id) as count
					FROM latest_actions d
					INNER JOIN escorts e ON e.id = d.escort_id
					INNER JOIN escorts_in_cities eic ON eic.escort_id = d.escort_id
					INNER JOIN cities c ON c.id = eic.city_id
					WHERE 1 AND eic.is_base = 1 AND d.action_type = ? ' . $where
				;

				$action_counts[$id] = array('count' => self::db()->fetchOne($sql, array($id)), 'title' => 'la_top_title_' . $id);
			}
			
			$cache->save($action_counts, $cache_key, array());
		}
		
		return $action_counts;
	}
	
	public function getAll($filter = array(), $page = 1, $page_size = 30, &$count = 0)
	{
		
		$page = (int) $page;
		if ( $page < 1 ) $page = 1;
		$limit = ($page_size * ($page - 1)) . ', ' . $page_size;
		
		$cache = Zend_Registry::get('cache');		
		$cache_key = 'latest_action_list_' . Cubix_Application::getId() . '_' . (int) $filter['city_id']. '_' . (int) $filter['action_type'] . '_' . $page;
		$cache_key_count = 'latest_action_count_list_' . Cubix_Application::getId() . '_' . (int) $filter['city_id']. '_' . (int) $filter['action_type'] . '_' . $page;
		
		$where = '';
		
		if ( isset($filter['city_id']) && $filter['city_id'] ) {
			$where .= ' AND eic.city_id = ' . $filter['city_id'] . ' ';
		}
		
		if ( isset($filter['action_type']) && $filter['action_type'] ) {
			$where .= ' AND d.action_type = ' . $filter['action_type'] . ' ';
		}

		$where .= Cubix_Countries::blacklistCountryWhereCase();
		
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS 
				d.action_type, d.escort_id as id, e.showname, e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id
			FROM (
					SELECT * 
					FROM latest_actions ORDER BY escort_id, action_type) d
					
			INNER JOIN escorts e ON e.id = d.escort_id
			INNER JOIN escorts_in_cities eic ON eic.escort_id = d.escort_id
			INNER JOIN cities c ON c.id = eic.city_id
			WHERE 1 ' . $where . '
			GROUP BY d.escort_id
			ORDER BY d.date desc
			LIMIT ' . $limit . '
		';
		
		if ( ! $escorts = $cache->load($cache_key) ) {
			$escorts = self::db()->fetchAll($sql);
			
			$cache->save($escorts, $cache_key, array());
		}
		
		if ( ! $count = $cache->load($cache_key_count) ) {
			$count = self::db()->fetchOne('SELECT FOUND_ROWS()');
			
			$cache->save($count, $cache_key_count, array());
		}	
		
		return $escorts;
	}
	
	public function getAllCitiesForFilter($country_id)
	{
		return parent::_fetchAll('
			SELECT c.id, ' . Cubix_I18n::getTblField('c.title') . ' AS title, ' . Cubix_I18n::getTblField('r.title') . ' AS region_title, co.has_regions 
			FROM cities c
			INNER JOIN escorts_in_cities eic ON eic.city_id = c.id
			INNER JOIN latest_actions la ON la.escort_id = eic.escort_id			
			LEFT JOIN regions r ON r.id = c.region_id
			LEFT JOIN countries co ON co.id = c.country_id
			WHERE c.country_id = ?
			GROUP BY c.id
			ORDER BY title ASC
		', $country_id);
	}
}
