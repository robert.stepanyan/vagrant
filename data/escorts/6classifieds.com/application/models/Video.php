<?php


class Model_Video extends Cubix_Model {
	protected $_table = 'escorts';
	private $db;
	
	public function __construct() 
	{
		
		$this->db = Zend_Registry::get('db');
	}

	public function GetEscortVideo($escort_id,$limit,$agency_id=NULL,$private=TRUE)
	{
				
		$escort_id = $this->db->quote($escort_id);
		if(isset($agency_id) && is_numeric($agency_id))
		{	$check =  parent::_fetchRow("SELECT id FROM escorts WHERE id=$escort_id AND agency_id='$agency_id'");	
			if(empty($check))
			return FALSE;
		}
		if($private)
		{	
			$client = Cubix_Api_XmlRpc_Client::getInstance();
			return  $client->call('Application.GetVideos',array($escort_id,$limit));
		}
		else
		{
			
			$escort_id = $this->db->quote($escort_id);
			$sql_video = "SELECT * FROM video WHERE escort_id=$escort_id  ORDER BY `date` DESC LIMIT 0,$limit";
			$video = parent::_fetchAll($sql_video);
			
			$photo = array();
			if(!empty($video))
			{
				$v_id =array();
				foreach ($video as $v)
				{
					$v_id[]=$v->id;
				}
				$v_id =  implode(',', $v_id);
				$sql_image = "SELECT * FROM video_image  WHERE video_id IN($v_id) ";
				$photo =  parent::_fetchAll($sql_image);
			}
			return array($photo,$video);
		}
	}

	public function GetEscortsVideoArray(array $escorts)
	{	
		$escorts = implode(',', $escorts);
		$sql = "SELECT video,`hash`,width,height,escort_id,ext FROM video_image AS img 
				JOIN (SELECT video,escort_id,id FROM video WHERE escort_id IN($escorts) ORDER BY `date` DESC ) AS v 
				ON v.id=img.video_id 
				GROUP BY escort_id ORDER BY img.`date` DESC ";
		return parent::_fetchAll($sql);
	}
	
	public function Remove($id,array $video,$escort_id=null)
	{	$count = count($video);
		$video_id =array();
		for($i=0;$i<$count;$i++)
		{
			$video_id[]=$this->db->quote($video[$i]);
		}
		$video_id = implode(',', $video_id);
		$id = $this->db->quote($id);
		if(isset($escort_id) && is_numeric($escort_id))
		{	
			$escort_id = $this->db->quote($escort_id);
			$check =  parent::_fetchRow("SELECT id FROM escorts WHERE id=$escort_id AND agency_id=$id ");	
			if(empty($check))
			return FALSE;
			$id = $escort_id;
		}
		
	
		$client = Cubix_Api_XmlRpc_Client::getInstance();	
		$affected_video_id = $client->call('Application.RemoveVideo',array($id,$video_id));
		if($affected_video_id)
		 $this->db->query("DELETE FROM video WHERE  escort_id=$id AND id IN($video_id) ");
		return $affected_video_id;
		
	}
	
	public function GetAgencyEscort($agency_id)
	{	
		$escorts =parent::_fetchAll("SELECT showname,id FROM  escorts WHERE agency_id=?  ",$agency_id);

		if(!empty($escorts))
		{	
			$client = Cubix_Api_XmlRpc_Client::getInstance();
			$escorts=$client->call('Application.GetAgencyEscortVideo',array($agency_id,$escorts));
		}
		return $escorts;
	}
	
	public function GetAgencyEscorts($id,$escort_id)
	{	
		$escort_id = $this->db->quote($escort_id);
		$escorts = parent::_fetchAll("SELECT showname,id FROM escorts WHERE agency_id='$id' AND id=$escort_id ");
		return empty($escorts)?FALSE:TRUE;
	}


	public function UserVideoCount($escort_id)
	{	
		$escort_id = $this->db->quote($escort_id);
		$count= parent::_fetchRow("SELECT count(id) AS quantity FROM video WHERE escort_id=$escort_id  GROUP BY escort_id ");
		return $count->quantity;
	}
}

?>
