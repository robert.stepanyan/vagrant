<?
include('inc/common.php');
$nbx = new Cube_NetbanxAPI();
$id = $_GET['id'];
$order = $nbx->get_order($id);
$date = new DateTime($order->transaction->lastUpdate, new DateTimeZone('America/New_York'));
$date->setTimezone(new DateTimeZone('Europe/Rome'));
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>Beneluxxx payment</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, width=device-width" />
        <link rel="stylesheet" type="text/css" href="css/reset.css" />
        <link rel="stylesheet" type="text/css" href="css/font.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link rel="stylesheet" type="text/css" href="css/media.css" />
        <link rel="icon" href="/images/favicon.ico" type="image/png" />
        <!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie.css" />
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <script type="text/javascript" src="/js/jquery-1.12.4.min.js" ></script>
        <script type="text/javascript" src="/js/jquery.validate1.js"></script>

        <!--Script-->
        <script src="js/script.js" rel="script"></script>
        <!--Script-->
	</head>
	<body>
    <div class="close-nav"></div>
		 <div class="wrapper aprove success">
             <? $page = 'success'; ?>
             <? include('header.php'); ?>
             <div id="content" class="wrap">
                <div class="balancer">
                    <div id="beneluxxx">
	                    <div class="logo" src="images/pic_logo.png" alt=""></div>
                        <div class="hr"></div>
                        <h6 class="ttu">Your advertising specialists!</h6>
                        <p>Advertising packages on <a class="gold" href="">beneluxxx.com</a><br/>
                            the leading portal for escort ads<br/>
                            in Belgium, Holland and Luxembourg.</p>
                    </div>
                    <div class="content_box">
                        <div class="aprovation">
                            <span class="ttu">Your payment 
                            <b>was successful</b></span>
                            Thank you very much!
                        </div>
                        <div class="sale_person">
                            <p>Please contact your sales person with the payment details</p>
                            <ul>
								<li>Address: <?= $order->billingDetails->street ?></li>
								<li>Zip: <?= $order->billingDetails->zip ?></li>
								<li>Country: <?= strtoupper($order->transaction->card->country) ?></li>
								<li>Transaction Id: <?= $order->transaction->confirmationNumber ?></li>
								<li>Amount: <?=  number_format($order->transaction->amount / 100, 2) ?></li>
								<li>Currency: <?=  $order->transaction->currencyCode ?></li>
								<li>Date: <?=  $date->format('Y-m-d H:i:s') ?></li>
                            </ul>
                            <a class="bth ttu" href="/">back to home</a>
                        </div>
                    </div>
                </div>
            </div>
			<? include('footer.php'); ?>
		</div>
	</body>
</html>