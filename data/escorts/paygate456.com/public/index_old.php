<?php include 'inc/common.php'; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>Beneluxxx payment</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, width=device-width" />
		<link rel="stylesheet" type="text/css" href="css/reset.css" charset=UTF-8" />
		<link rel="stylesheet" type="text/css" href="css/font.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link rel="stylesheet" type="text/css" href="css/media.css" />
		<link rel="icon" href="/images/favicon.ico" type="image/png" />
        <!--[if lt IE 9]>
            <link rel="stylesheet" type="text/css" href="css/ie.css" />
            <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

		<script type="text/javascript" src="/js/jquery-1.12.4.min.js" ></script>
		<script type="text/javascript" src="/js/jquery.validate1.js"></script>

		<!--Script-->
		<script src="js/script.js" rel="script"></script>
		<!--Script-->
	</head>

	<script type="text/javascript">
	$(document).ready(function(){
		$("#pay-form").validate({
			rules: {
				amount: {
					required: true,
					number: true
				}
			}
		});
		
		$('.payment-option').click(function() {
			$('.payment-option').removeClass('checked');
			$(this).addClass('checked');
			
			$('#payment-gateway').val($(this).attr('data-method'));
		});
	});
	</script>
	<body>
	<div class="close-nav"></div>
	<div class="wrapper home_page">
		<?  
			session_start();
			$token = $_SESSION['token'] ? $_SESSION['token'] : false;
			unset($_SESSION['token']);

		?>
		<? if($token):?>
		<script src="https://oppwa.com/v1/paymentWidgets.js?checkoutId=<?= $token ?>"></script>
		<div class="ecardon-popup">
			<form action="http://www.paygate456.com/ecardon_success.php" class="paymentWidgets" data-brands="VISA MASTER"></form>
		</div>
		<? endif; ?>
			<? $page = 'index'; ?>
			<? include('header.php'); ?>
			<div id="content" class="wrap">
				<div class="balancer">
					<div id="beneluxxx">
						<div class="logo" alt=""></div>
						<div class="hr"></div>
						<h6 class="ttu"><?= __('slogan_block_row_1') ?></h6>
						<p><?= __('slogan_block_row_2') ?></p>
					</div>
					<div class="content_box">
						<div class="desc_text">
							<p><?= __('index_left_block_row_1') ?></p>
							<p><?= __('index_left_block_row_2') ?></p>
							<p><?= __('index_left_block_row_3') ?></p>
							<div class="clear"></div>
						</div>
						<div class="buy_now">
												
							<form action="process.php" method="post" id="pay-form">
								<input type="hidden" name="payment_gateway" id="payment-gateway" value="ecardon"/>
								<p class="ttu"><?= __('please_enter_the_amount') ?></p>
								<div class="clear"></div>
								<div class="text_type-cont">
									<input class="text_type" type="text" name="amount"/>
								</div>
								<div class="clear"></div>
								<div class="payment-option-wrapper">
									<p><?= __('please_select_payment_method') ?></p>
									<div class="clear"></div>

									

									<div class="payment-option checked" data-method="ecardon"><?= __('checkout_with_ecardon') ?></div>
									<div class="clear"></div>

									<div class="payment-option" data-method="netbanx"><?= __('checkout_with_netbanx') ?></div>
									<div class="clear"></div>
									<div class="payment-option" data-method="mmgbill"><?= __('checkout_with_mmgbill') ?></div> 
									<div class="clear"></div>
									
									
								</div>
								<div class="clear"></div>
								<button class="ttu"><?= __('buy_now') ?></button>
								<div class="clear"></div>
							</form>
						</div>
						
					</div>
				</div>
			</div>
			<? include('footer.php'); ?>
		</div>
	</body>
</html>