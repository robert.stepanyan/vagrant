<header id="header" class="animated fadeInUp">
	<nav class="main-nav">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-xs-3">
					<div class="logo">
						<a class="text-center" href="#"></a>
					</div>
				</div>
				<div class="col-md-9 col-xs-9">
					<ul class="pull-right">
						<li>
							<a class="text-center" href="#">{{home_page}}</a>
						</li>
						<li>
							<a class="text-center" href="#about">{{about_us}}</a>
						</li>
						<li>
							<a class="text-center" href="#terms">{{terms}}</a>
						</li>
						<li>
							<a class="text-center" href="#privacy">{{privacy}}</a>
						</li>

						<li class="lang">
							<a class="flag-en" href="javascript:void(0)">
								<img src="/images/en.svg" alt="EN" width="28" height="18">
							</a>
							<a class="flag-fr" href="javascript:void(0)">
								<img src="/images/fr.svg" alt="FR" width="28" height="18">
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</nav>
	<div class="container">
		<div class="row">
			<div class="col-lg-9 col-lg-offset-3 col-md-12">
				
			</div>
		</div>
		<div class="row m-t-30 m-b-20">
			<div class="col-md-6 title-block">
				<div class="bene-logo">
					<a href="https://www.paygate456.com/">
						<img src="/images/beneluxxx-logo.png" alt="">
					</a>
				</div>
			</div>
		</div>
	</div>
</header>