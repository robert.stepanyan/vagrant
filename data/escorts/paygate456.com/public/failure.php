<? include('inc/common.php'); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>Beneluxxx payment</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, width=device-width" />
        <link rel="stylesheet" type="text/css" href="css/reset.css" />
        <link rel="stylesheet" type="text/css" href="css/font.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link rel="stylesheet" type="text/css" href="css/media.css" />
        <link rel="icon" href="/images/favicon.ico" type="image/png" />
        <!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie.css" />
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <script type="text/javascript" src="/js/jquery-1.12.4.min.js" ></script>
        <script type="text/javascript" src="/js/jquery.validate1.js"></script>

        <!--Script-->
        <script src="js/script.js" rel="script"></script>
        <!--Script-->
	</head>
	<body>
    <div class="close-nav"></div>
		 <div class="wrapper aprove">
			<? $page = 'failure'; ?>
			<? include('header.php'); ?>
			<div id="content" class="wrap">
                <div class="balancer">
                    <div id="beneluxxx">
	                    <div class="logo" src="images/pic_logo.png" alt=""></div>
                        <div class="hr"></div>
                        <h6 class="ttu">Your advertising specialists!</h6>
                        <p>Advertising packages on <a class="gold" href="">beneluxxx.com</a><br/>
                            the leading portal for escort ads<br/>
                            in Belgium, Holland and Luxembourg.</p>
                    </div>
                    <div class="content_box">
                        <div class="failed_msg">
                            <div class="msg">We are sorry to inform you that your payment was not successful.
                                <p>Please contact us at <a class="gold" href="mailto:info@beneluxxx.com">info@beneluxxx.com</a> for more details.
                                    Thank you!</p></div>
                            <a class="bth ttu" href="/">back to home</a>
                        </div>
                    </div>
                </div>
            </div>
			<? include('footer.php'); ?>
		</div>
	</body>
</html>