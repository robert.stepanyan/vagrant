<footer id="footer" class="container m-t-80">
	<div class="row">
		<div class="row">
			<div class="col-md-12 footer-wrap">
				<div class="col-md-10 text-left">
					<div class="copy">
						<p>
							{{footer_text}} &copy; Copyright {{date}} / paygate456.com
						</p>
					</div>
				</div>
				<div class="col-md-2">
					<div class="cards text-center">
						<img src="/images/mv.svg" alt="master visa cards" width="120">
					</div>
				</div>
			</div>
		</div>
	</div>
	<nav class="mobile-nav">
		<ul>
			<li class="menu-icon-wrapper">
				<a href="javascript:void(0)">
					<img src="/images/menu.svg" alt="">
				</a>
				<ul class="hamburger-menu">
					<li>
						<a class="text-center" href="#">{{home_page}}</a>
						
					</li>
					<li>
						<a class="text-center" href="#about">{{about_us}}</a>
					</li>
					<li>
						<a class="text-center" href="#terms">{{terms}}</a>
					</li>
					<li>
						<a class="text-center" href="#privacy">{{privacy}}</a>
					</li>
				</ul>
			</li>
			<li class="logo">
				<a class="text-center" href="#"></a>
			</li>
			<li class="lang">
				<a class="flag-en" href="javascript:void(0)">
					<img src="/images/en.svg" alt="EN" width="28" height="18">
				</a>
				<a class="flag-fr" href="javascript:void(0)">
					<img src="/images/fr.svg" alt="FR" width="28" height="18">
				</a>
			</li>
		</ul>
		
	</nav>
</footer>