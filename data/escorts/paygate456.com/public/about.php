<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="hero-page">
                <h2 class="page-title">{{about_us}}</h2>
                <p>{{index_left_block_row_1}}</p>
                <p>{{{index_left_block_row_2}}}</p>
                <div class="text-center m-t-30">
                    <p><strong>AA Media Services GmbH </strong></p>
                    <p>Industriestrasse 28, 9100 Herisau</p>
                    <p>CHE-389.533.593</p>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>