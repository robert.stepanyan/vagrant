<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="hero-page">
                <h2 class="page-title">{{advertising_terms_row1_title}}</h2>
                <p>{{advertising_terms_row1_text}}</p>
                <h3 class="light-text hightlighted">{{advertising_terms_row2_title}}</h3>
                <p>{{advertising_terms_row2_text}}</p>
                <h3 class="light-text hightlighted">{{advertising_terms_row3_title}}</h3>
                <p>{{advertising_terms_row3_text}}</p>
                <h3 class="light-text hightlighted">{{advertising_terms_row4_title}}</h3>
                <p>{{advertising_terms_row4_text}}</p>
                <h3 class="light-text hightlighted">{{advertising_terms_row5_title}}</h3>
				{{advertising_terms_row5_text}}
            </div>
        </div>
    </div>
</div>
