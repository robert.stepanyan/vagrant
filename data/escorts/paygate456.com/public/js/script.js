var PayGate = {
    Home: {}
};

PayGate.Home = {
    init: function(){
        this.toogleMobileMenu();
        this.calculateFooterPosition();
        this.navbar_events();
    },

    toogleMobileMenu: function(){
        $('#nav-toggle').click(function () {
            $(this).toggleClass("active");
            $('.nav').toggleClass("display-block");
            $('.close-nav').toggleClass("displayBlock");
        });

        $('.close-nav').click(function () {
            $('#nav-toggle').removeClass("active");
            $('.nav').removeClass("display-block");
            $('.close-nav').removeClass("displayBlock");
        });
    },

    

    calculateFooterPosition: function(){
        var content_box = $('.content_box'),
            elemHeight = content_box.height(),
            body =  $('body'),
            h =  content_box.offset().top,
            k = body.innerHeight() - h,
            bodyheight = body.innerHeight() + ( elemHeight - k + 150 ),
            wrapEemHeight = content_box.height();


        body.css({"height": bodyheight});
        $('.wrapper.privacy #footer').css({ "margin-top":wrapEemHeight/1.5 });
    }
};

$(document).ready(function(){
    PayGate.Home.init();
});

$(window).on("resize", function(){
    PayGate.Home.calculateFooterPosition();
});