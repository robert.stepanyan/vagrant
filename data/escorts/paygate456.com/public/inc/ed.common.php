<?
// ini_set('display_errors',true);
// error_reporting(E_ALL);

date_default_timezone_set('Asia/Yerevan');
ini_set('include_path', ini_get('include_path').PATH_SEPARATOR.'library');



// Registering Autoloader
require 'Zend/Loader/Autoloader.php';
include 'dictionary.php';
include 'application.php';



$autoloader = Zend_Loader_Autoloader::getInstance();
$autoloader->registerNamespace ( 'Cube_'  );

$app = new Application();


function page_content(){
	$data['prefilled'] = isset($_GET['prefilled']) ? $_GET['prefilled'] : null;
	$data['package'] = isset($_GET['package']) ? $_GET['package'] : null;

	if(is_numeric($data['prefilled']) && $data['prefilled'] > 0){
		$data['prefilled'] =  (float) $_GET['prefilled'];
	}else{
		$data['prefilled'] =  0;
	}

	if(isset($_SESSION['money'])){
		$data['prefilled'] = $_SESSION['money'];
		unset($_SESSION['money']);
	}

	if(isset($data['package'])){
		$_SESSION['package'] = $data['package'];
	}
	return $data;
}

function getLink($link) {
	$lang = $_GET['lang'];
	if ( $lang && $lang != 'fr' ) {
		$link .= '?lang=' . $lang;
	}
	
	return $link;
}

function __($id, $args = array()) {
	global $dictionary;
	$lang = 'fr';
	if ( $_GET['lang'] == 'en' ) {
		$lang = 'en';
	}
	
	if ( $dictionary[$id] ) {
		$result = $dictionary[$id][$lang];
		
		
		foreach($args as $key => $value) {
			$result = preg_replace('#%' . $key . '%#', $value, $result);
		}
	} else {
		$result = '*' . $id . '*';
	}
	
	return $result;
}

?>