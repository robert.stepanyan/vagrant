<?

	// header('location: https://www.beneluxxx.com/paiement');
	// exit;	



date_default_timezone_set('Asia/Yerevan');
ini_set('include_path', ini_get('include_path').PATH_SEPARATOR.'library');
// Registering Autoloader

require_once 'Zend/Loader/Autoloader.php';
include 'dictionary.php';

$autoloader = Zend_Loader_Autoloader::getInstance();
$autoloader->registerNamespace ( 'Cube_'  );


function getLink($link) {
	$lang = $_GET['lang'];
	if ( $lang && $lang != 'fr' ) {
		$link .= '?lang=' . $lang;
	}
	
	return $link;
}

function __($id, $args = array()) {
	global $dictionary;
	$lang = 'fr';
	if ( $_GET['lang'] == 'en' ) {
		$lang = 'en';
	}
	
	if ( $dictionary[$id] ) {
		$result = $dictionary[$id][$lang];
		
		
		foreach($args as $key => $value) {
			$result = preg_replace('#%' . $key . '%#', $value, $result);
		}
	} else {
		$result = '*' . $id . '*';
	}
	
	return $result;
}

?>