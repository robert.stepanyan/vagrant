<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

define('MAIL_SERVER_HOST', "smtp.edir-mail.com");
define('MAIL_SERVER_PORT', 25);
define('MAIL_SERVER_AUTH', true);
define('MAIL_SERVER_USER', "info@escortdirectory.com");
define('MAIL_SERVER_PASS', "ED1-ppq#W");
define('MAIL_FROM', "info@escortdirectory.com");
define('MAIL_FROM_NAME', "escortdirectory.com");
define('API_KEY', '9p30yrtt4j1xbyzwscg5rkecz78vsitr');
define('API_SERVER', 'https://server.escortdirectory.com');

class Application {

    public function __construct(){

        session_start();

        if($_SERVER['REQUEST_METHOD'] == "POST"){
            
            $action = isset($_POST['action']) ? $_POST['action'] : null;

            if(!empty($action)){
                call_user_func([$this, $action . 'Action']);
            }

        }

      
    }


    public function paymentInfoEmailAction(){

        require_once 'library/PHPMailer/autoloader.php';
      
        $to = 'sales@escortdirectory.com';
        // $to = 'narek.amirkhanyan92@gmail.com';

        $email          = !empty($_POST['email']) ? htmlspecialchars($_POST['email']) : null; 
        $link           = !empty($_POST['link']) ? htmlspecialchars($_POST['link']) : null; 
        $package        = !empty($_SESSION['package']) ? ucwords(str_replace('-' ,' ', htmlspecialchars($_SESSION['package']))) : 'Not selected'; 
        $payment_data   = !empty($_POST['payment_data']) ? unserialize($_POST['payment_data']) : []; 
        $link           = !empty($_POST['link']) ? htmlspecialchars($_POST['link']) : null; 

        $result = 'error';
        $subject = 'Advertising inquiry';

        if(empty($payment_data)){
            $payment_data = [];
        }
        // Flush Prefilled Package
        unset($_SESSION['package']);
        // PUTTING TEMPLATE INTO A VARIABLE
        ob_start();
        require "templates/mail.php";
        $template_message = ob_get_clean();

        try {
            $config = array(
                'port' => MAIL_SERVER_PORT,
                'auth' => 'login',
                'ssl' => 'tls',
                'username' => MAIL_SERVER_USER,
                'password' => MAIL_SERVER_PASS
            );

            $tr = new \Zend_Mail_Transport_Smtp(MAIL_SERVER_HOST, $config);
            \Zend_Mail::setDefaultTransport($tr);

            $mail = new \Zend_Mail('UTF-8');
            $mail->setBodyHtml($template_message);

            $mail->addTo($to);
            $mail->setSubject($subject);
            $mail->setFrom(MAIL_FROM, MAIL_FROM_NAME);
            $mail->send();
            $result = 'success';
        } catch (\Exception $e) {

        }

        $response_message = [
            'type' => $result
        ];

        if($result == 'success'){
            $response_message['title'] = 'Mail sent';
            $response_message['text'] = 'Your request has been sent to our staff.';
        }else{
            $response_message['title'] = 'Mail could not be sent';
            $response_message['text'] = 'Whoops! it seems like there was an error, please contact our Staff .';
        }

        $_SESSION['message'] = $response_message;

        header("location: /ed.php");

    }

 
}

?>