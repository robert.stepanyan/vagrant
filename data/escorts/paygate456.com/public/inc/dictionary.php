<?
$dictionary = array(
	'home_page' => array(
		'fr'	=> 'ACCUEIL',
		'en'	=> 'home page'
	),
	'about_us' => array(
		'fr'	=> 'A PROPOS DE NOUS',
		'en'	=> 'ABOUT US'
	),
	'terms' => array(
		'fr'	=> 'CONDITIONS',
		'en'	=> 'Terms'
	),
	'privacy' => array(
		'fr'	=> 'CONFIDENTIALITÉ',
		'en'	=> 'Privacy'
	),
	'slogan_block_row_1' => array(
		'fr'	=> 'Votre spécialiste publicitaire !',
		'en'	=> 'Your advertising specialists!'
	),
	'slogan_block_row_2' => array(
		'fr'	=> 'Forfaits publicitaires sur <a class="gold" href="http://www.beneluxxx.com">beneluxxx.com</a> le plus grand portail de pubs d\'escortes
					en Belgique, en Hollande et au Luxembourg.',
		'en'	=> 'Advertising packages on <a class="gold" href="http://www.beneluxxx.com">beneluxxx.com</a><br/>
					the leading portal for escort ads<br/>
					in Belgium, Holland and Luxembourg.'
	),
	'index_left_block_row_1' => array(
		'fr'	=> 'Vous en avez marre de payer trop pour des publicités sans voir de résultats ?',
		'en'	=> 'Are you tired of paying too much for your advertising and not seeing good results ? Then we have the right solution for you! We offer a range of advertising packages for your needs. If the packages don\'t fit your needs - no problem. We also offer customized solutions for any demand. For customized solutions get in touch with our staff.'
	),
	'index_left_block_row_2' => array(
		'fr'	=> 'Nous avons la solution pour vous ! Nous offrons une large gamme de forfaits publicitaires sur <a class="gold" href="http://www.beneluxxx.com">beneluxxx.com</a> - le plus grand portail de pubs d\'escortes en Belgique, en Hollande et au Luxembourg.',
		'en'	=> 'Then we have the right solution for you! We offer a range of advertising packages on <a class="gold" href="http://www.beneluxxx.com">beneluxxx.com</a>- the leading portal for escort ads in Belgium, Holland and Luxembourg.'
	),
	'index_left_block_row_3' => array(
		'fr'	=> 'Si les forfaits ne répondent pas à vos besoins - aucun problème. Nous offrons également des solutions personnalisées selon les demandes. Contactez notre équipe pour en savoir plus sur les options personnalisées.',
		'en'	=> 'If the packages don\'t fit your needs - no problem. We also offer customized solutions for
							any demand. For customized solutions get in touch with our staff. '
	),
	'please_enter_the_amount' => array(
		'fr'	=> 'Veuillez entrer en EURO le montant que vous avez convenu avec votre représentant.',
		'en'	=> 'Please enter the amount in EURO which you agreed with your sales person.'
	),
	'please_select_payment_method' => array(
		'fr'	=> 'Veuillez sélectionner l\'une des options ci-dessous',
		'en'	=> 'Please select one of these options below'
	),
	'checkout_with_netbanx' => array(
		'fr'	=> 'paiement avec Netbanx (option alternative)',
		'en'	=> 'checkout with Netbanx (alternative)'
	),
	'checkout_with_mmgbill' => array(
		'fr'	=> 'paiement avec MMGBILL (option alternative)',
		'en'	=> 'checkout with MMGBILL (alternative)'
	),
	'checkout_with_ecardon' => array(
		'fr'	=> 'paiement avec Ecardon (recommandé)',
		'en'	=> 'checkout with Ecardon (recommended)'
	),
	'buy_now' => array(
		'fr'	=> 'ACHETER',
		'en'	=> 'buy now'
	),
	'advertising_terms' => array(
		'fr'	=> 'CONDITIONS EN MATIÈRES DE PUBLICITÉ',
		'en'	=> 'ADVERTISING TERMS'
	),
	'terms_read_carefully' => array(
		'fr'	=> 'Veuillez lire attentivement nos conditions en matière de publicité. Vous acceptez nos conditions en achetant tout forfait sur notre site.',
		'en'	=> 'Please read carefully our advertising terms. By ordering any package from our site you agree with our conditions.'
	),
	'terms_row1' => array(
		'fr'	=> 'Lorsque vous passer commande, vous serez redirigé automatiquement vers notre service de paiement externe.',
		'en'	=> 'When you submit your order, you will get redirected to our external payment provider.'
	),
	'terms_row2' => array(
		'fr'	=> 'Vous pouvez être contacté par un membre de notre équipe si vous avez besoin de plus d\'informations une le fois le paiement effectué avec succès.',
		'en'	=> 'After successful payment you may be contacted by our staff if we need more details. '
	),
	'terms_row3' => array(
		'fr'	=> 'Toutes les commandes passés sur paygate456.com sont non-remboursables.',
		'en'	=> 'All orders done through paygate456.com are non-refundable.'
	),
	'terms_row4' => array(
		'fr'	=> 'Toutes les données sont garder confidentielles, veuillez lire notre politique de confidentialité.',
		'en'	=> 'All data are kept confidentially, please read our privacy policy.'
	),
	'terms_row5' => array(
		'fr'	=> 'Toutes les demandes seront traités sous 24-48 heures.',
		'en'	=> 'All requests will be answered within 24-48 hours.'
	),
	'enter_custom_amount' => array(
		'fr'	=> 'Per favore inserisci l\'ammontare in Euro che avete concordato con il vostro sales',
		'en'	=> 'Please enter the amount in EURO which you agreed with your sales person'
	),
	'advertising_terms' => array(
		'fr'	=> 'Termini Pubblicitari',
		'en'	=> 'Advertising Terms'
	),
	'please_read_carefully' => array(
		'fr'	=> 'Leggi attentamente i nostri termini pubblicitari. Ordinando qualsiasi confezione dal nostro sito accetti le nostre condizioni.',
		'en'	=> 'Please read carefully our advertising terms. By ordering any package from our site you agree with our conditions.'
	),
	'terms_row_1' => array(
		'fr'	=> 'Quando invii il tuo ordine, verrai reindirizzato sul nostro provider esterno per i pagamenti.',
		'en'	=> 'When you submit your order, you will get redirected to our external payment provider.'
	),
	'terms_row_2' => array(
		'fr'	=> 'Dopo aver effettuato con successo il pagamento potresti venire contattato dal nostro personale se avessimo bisogno di ulteriori dettagli.',
		'en'	=> 'After successful payment you may be contacted by our staff if we need more details.'
	),
	'terms_row_3' => array(
		'fr'	=> 'Tutti gli ordini effettuati tramite il sito adsgate789.com non sono rimborsabili.',
		'en'	=> 'All orders done through adsgate789.com are non-refundable.'
	),
	'terms_row_4' => array(
		'fr'	=> 'Tutte le informazioni verranno trattate in maniera confidenziale, leggi a proposito la nostra <a href="%link%">politica sulla privacy</a>.',
		'en'	=> 'All data are kept confidentially, pls read our <a href="%link%">privacy policy.</a>'
	),
	'terms_row_5' => array(
		'fr'	=> 'Tutte le richieste verranno soddisfatte nel giro di 24-48 ore.',
		'en'	=> 'All requests will be answered within 24-48 hours.'
	),
	
	'advertising_terms' => array(
		'fr'	=> 'Conditions en matière de publicité',
		'en'	=> 'advertising terms'
	),
	
	'advertising_terms_row1_title' => array(
		'fr'	=> 'Notre engagement à la confidentialité',
		'en'	=> 'Our Commitment to Privacy'
	),
	'advertising_terms_row1_text' => array(
		'fr'	=> 'Votre confidentialité est importante pour nous. Afin de mieux la protéger, nous fournissons cette notification expliquant nos pratiques d’informations en ligne et les choix que vous pouvez effectuer concernant la façon dont vos informations sont recueillies et utilisées. Pour rendre cette notification accessible, nous la mettons à disposition sur notre page principale et à tout endroit où des informations personnellement identifiables peuvent être demandées.',
		'en'	=> 'Your Privacy is important to us. To better protect your privacy we provide this notice explaining our online information practices and the choices you can make about the way your information is collected and used. To make this notice easy to find, we make it available an our homepage and at every point where personally identifiable information may be requested.'
	),
	
	'advertising_terms_row2_title' => array(
		'fr'	=> 'Les informations que nous recueillons',
		'en'	=> 'The Information We Collect'
	),
	'advertising_terms_row2_text' => array(
		'fr'	=> 'Cette notification s’applique à toutes les informations recueillies ou soumises à ce site. Sur certaines pages, vous pouvez commander des produits/souscriptions, faire des demandes et vous enregistrer pour recevoir des documents. Les types d’informations personnelles collectées sur ces pages sont : Nom complet, nom d’utilisateur, adresse email.',
		'en'	=> 'This notice applies to all information collected or submitted on this website. On some pages, you can order products/subscriptions, make requests, and register to receive materials. The types of personal information collected at these pages are: Fullname, Username, Email address.'
	),
	
	'advertising_terms_row3_title' => array(
		'fr'	=> 'La façon dont nous utilisons les informations',
		'en'	=> 'The Way We Use Information'
	),
	'advertising_terms_row3_text' => array(
		'fr'	=> 'Nous utilisons les informations recueillies pour la protection de l’accès à notre système (nom d’utilisateur, mot de passe) et pour des vérifications d’utilisateurs (adresse email).',
		'en'	=> 'We use collected information for protection of access to our system (username, password) and for user verification (email)'
	),
	'advertising_terms_row4_title' => array(
		'fr'	=> 'Notre engagement sur la sécurité des données',
		'en'	=> 'Our Commitment To Data Security'
	),
	'advertising_terms_row4_text' => array(
		'fr'	=> 'Afin d’empêcher un accès non-autorisé, de maintenir l’exactitude des données et d’assurer l’utilisation correcte des informations, nous avons mis en place des procédures appropriées physiques, électroniques et gestionnaires pour protéger et sécuriser les informations que nous recueillons en ligne.',
		'en'	=> 'To Prevent unauthorized access, maintain data accuracy, and ensure the correct use of information, we have put in place appropriate physical, electronic, and managerial procedures to safeguard and secure the information we collect online.'
	),
	'advertising_terms_row5_title' => array(
		'fr'	=> 'Comment pouvez-vous accéder à vos informations ou les modifier',
		'en'	=> 'How You Can Access Or Correct Your Information'
	),
	'advertising_terms_row5_text' => array(
		'fr'	=> '<p>Vous pouvez accéder à toutes vos informations personnelles identifiables que nous recueillons en ligne que nous maintenons avec une base de données SQL. Nous utilisons cette procédure pour mieux protéger vos informations.</p>
					<p>Pour protéger votre confidentialité et votre sécurité, nous prenons toutes les mesures raisonnables pour vérifier votre identité avant de donner un accès ou effectuer ces corrections.</p>
					<p>Si vous avez toute question concernant cette politique de confidentialités, veuillez nous envoyer un message.</p>',
		'en'	=> '<p>You can access all your personally identifiable information that we collect online and maintain by SQL database. We use this procedure to better safeguard your information.</p>
                    <p>To protect your privacy and security, we will also take reasonable steps to verify your identity before granting access or making corrections. How to Contact us</p>
                    <p>Should you have other questions or concerns about these privacy policies, send us an message</p>'
	),
	'footer_text' => array(
		'fr'	=> 'AA Media Services GmbH, Industriestrasse 28, 9100 Herisau. AA Media Services GmbH est situé en Suisse, où la société paie des taxes pour tous ses services.',
		'en'	=> 'AA Media Services GmbH, Industriestrasse 28, 9100 Herisau. AA Media Services GmbH is placed in Switzerland, where it pays taxes for all services.'
	),
	'terms_top' => array(
		'fr'	=> 'Le site est la propriété de, réglementé et opéré par AA Media Services GmbH depuis son siège en Suisse.',
		'en'	=> 'This website is owned, regulated and operated by AA Media Services GmbH, from the Switzerland offices.'
	),
);
?>
