<?php include 'inc/common.php'; ?>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<link rel="icon" href="/images/favicon.ico" type="image/png" />
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300|Roboto:300,400,700" rel="stylesheet">

		<link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css" />
		<link rel="stylesheet" href="/css/font-awesome/css/font-awesome.min.css" />
		<link rel="stylesheet" href="/css/animate.css" />

		<link rel="stylesheet" type="text/css" media="screen" href="css/main.css?v=2"/>
	</head>
	
	<body>
		<div id="mainWrapper">
			<div data-device="{{device}}" data-lang="{{lang}}" id="pageConf">
				<?php
	                session_start();

	                $token = $_SESSION['token'] ? $_SESSION['token'] : false;
	                unset($_SESSION['token']);
				?>

				<?php if($token): ?>
				<div class="ecardon-popup modal" >
					<script>
						var wpwlOptions = {
							onBeforeSubmitCard: function(){
								//card holder validtion
								if ( $(".wpwl-control-cardHolder").val() === "" )
								{             
									$(".wpwl-control-cardHolder").addClass("wpwl-has-error");
									$(".wpwl-control-cardHolder").after("<div class='wpwl-hint wpwl-hint-cardHolderError'>Invalid card holder</div>");
									$(".wpwl-button-pay").addClass("wpwl-button-error").attr("disabled", "disabled");
									return false;
								}
								else
									return true;
							}
						}
					</script>
					<script async src="https://oppwa.com/v1/paymentWidgets.js?checkoutId=<?= $token ?>"></script>
					<form action="https://www.paygate456.com/#ecardonResponse/" class="paymentWidgets" data-brands="VISA MASTER"></form>
				</div>
				<?php endif; ?>

				<?php include('header.php'); ?>
				<main id="main">
					<div id="root" class="page">
						<?php include('main.php'); ?>
					</div>
					<!--<div id="about" class="page" style="display: none">
						<?php /*include('about.php'); */?>
					</div>-->
					<div id="terms" class="page" style="display: none">
						<?php include('terms.php'); ?>
					</div>
					<div id="privacy" class="page" style="display: none">
						<?php include('privacy.php'); ?>
					</div>
					<div id="ecardonResponse" class="page" style="display:none"></div>
					<div id="mmgResponse" class="page" style="display:none"></div>
				</main>
				<?php include('footer.php'); ?>
			</div>
		</div>
	
		<!-- SCRIPTS -->
		<script type="text/javascript" src="/js/jquery-3.2.1.min.js?v=2" ></script>
		<script type="text/javascript" src="/css/bootstrap/js/bootstrap.min.js?v=2" ></script>
		<script type="text/javascript" src="/js/jquery.validate.js?v=2"></script>
		<script type="text/javascript" src="/js/pace.min.js?v=2"></script>
		<script type="text/javascript" src="/js/mustache.min.js?v=2"></script>
		<script type="text/javascript" src="/js/t.js?v=3"></script>
		<script type="text/javascript" src="/js/routie.js?v=2"></script>
		<script type="text/javascript" src="/js/app.js?v=6"></script>
	</body>
</html>
