<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="payment-block text-right clearfix">
				<h2 class="clearfix">{{please_enter_the_amount}}</h2>
				
				<form action="process.php" method="POST" id="paymentForm" class="payment-form m-t-30">
					<div class="amount-wrapper clearfix">
						<input type="text" name="amount" value=""/>
						<input type="hidden" name="lang" value=""/>
						<input type="hidden" name="payment_gateway" id="paymentGateway" value="mmgbill"/>
						<span class="currency">EUR</span>
					</div>

					<!-- <div class="payment-option  clearfix">
						<span class="checkbox">
							<span class="bullet"></span>
						</span>

						<label for="ecardon">
							<input type="radio" checked="checked" data-method="ecardon" id="ecardon" />
							{{checkout_with_ecardon}}
						</label>
					</div> -->
					
					<div class="payment-option checked clearfix">
						<span class="checkbox">
							<span class="bullet"></span>
						</span>

						<label for="mmgbill">
							<input type="radio" data-method="mmgbill" id="mmgbill" />
							{{checkout_with_mmgbill}}
						</label>
					</div>

					<button type="submit" class="buy-now-btn clearfix" id="buyBtn">{{buy_now}}</button>
				</form>
			</div>
		</div>
	</div>
	<div class="packages">
		<article class="row m-t-30">
			<section class="col-md-4">
				<div class="package m-b-30">
					<div class="row">
						<h3 class="package-title col-md-12 col-sm-6 col-xs-6">Premium (30 {{days}})</h3>
						<div class="package-price col-md-12 col-sm-6 col-xs-6">149 EUR</div>
					</div>
				</div>
			</section>
			<section class="col-md-4">
				<div class="package m-b-30">
					<div class="row">
						<h3 class="package-title col-md-12 col-xs-6">Premium (15 {{days}})</h3>
						<div class="package-price col-md-12 col-xs-6">99 EUR</div>
					</div>
				</div>
			</section>
			<section class="col-md-4">
				<div class="package m-b-30">
					<div class="row">
						<h3 class="package-title col-md-12 col-xs-6">Gold (10 {{days}})</h3>
						<div class="package-price col-md-12 col-xs-6">45 EUR</div>
					</div>
				</div>
			</section>
			<section class="col-md-4">
				<div class="package m-b-30">
					<div class="row">
						<h3 class="package-title col-md-12 col-xs-6">Vip Light (15 {{days}})</h3>
						<div class="package-price col-md-12 col-xs-6">149 EUR</div>
					</div>
				</div>
			</section>
			<section class="col-md-4">
				<div class="package m-b-30">
					<div class="row">
						<h3 class="package-title col-md-12 col-xs-6">Vip Premium (30 {{days}})</h3>
						<div class="package-price col-md-12 col-xs-6">199 EUR</div>
					</div>
				</div>
			</section>
			<section class="col-md-4">
				<div class="package m-b-30">
					<div class="row">
						<h3 class="package-title col-md-12 col-xs-6">Vip (10 {{days}})</h3>
						<div class="package-price col-md-12 col-xs-6">99 EUR</div>
					</div>
				</div>
			</section>
		</article>
	</div>
</div>