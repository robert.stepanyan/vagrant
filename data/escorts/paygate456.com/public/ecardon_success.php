<?
include 'inc/common.php';
$token =  preg_replace('#[^a-zA-Z0-9.\-]#', '', $_GET['id']);
	
$error = true;
if ($token){
	$alt_payment = new Cube_EcardonGateway();
	$result = $alt_payment->getStatus($token);
	$result_dec = json_decode($result);
	$copy_examples = 'MerchantTransactionId, Holder, Amount etc.';
	if(isset($result_dec->result) && ($result_dec->result->code == '000.000.000' || $result_dec->result->code == '000.100.110')){
		$message = '<span class="ttu"> Your payment <b>was successful </b></span> Thank you very much! ';

		$responce = array(
			'Transaction Id' => $result_dec->descriptor,
			'Holder' => $result_dec->card->holder,
			'Amount' => $result_dec->amount,
			'Currency' => $result_dec->currency,
			'Payment Brand' => $result_dec->paymentBrand,
		);
		$error = false;
	} 
	else {
		$message = '<span class="ttu">Error Transaction!</span><br/><br/>' . $result_dec->result->description ;
	}
}
else{
	$message = '<span class="ttu">Error Transaction!</span><br/><br/> Please Try Again ';
}

$date = new DateTime($order->transaction->lastUpdate, new DateTimeZone('America/New_York'));
$date->setTimezone(new DateTimeZone('Europe/Rome'));
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>Beneluxxx payment</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, width=device-width" />
        <link rel="stylesheet" type="text/css" href="css/reset.css" />
        <link rel="stylesheet" type="text/css" href="css/font.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link rel="stylesheet" type="text/css" href="css/media.css" />
        <link rel="icon" href="/images/favicon.ico" type="image/png" />
        <!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie.css" />
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <script type="text/javascript" src="/js/jquery-1.12.4.min.js" ></script>
        <script type="text/javascript" src="/js/jquery.validate1.js"></script>

        <!--Script-->
        <script src="js/script.js" rel="script"></script>
        <!--Script-->
	</head>
	<body>
    <div class="close-nav"></div>
		 <div class="wrapper aprove success">
             <? $page = 'success'; ?>
             <? include('header.php'); ?>
             <div id="content" class="wrap">
                <div class="balancer">
                    <div id="beneluxxx">
	                    <div class="logo" src="images/pic_logo.png" alt=""></div>
                        <div class="hr"></div>
                        <h6 class="ttu">Your advertising specialists!</h6>
                        <p>Advertising packages on <a class="gold" href="">beneluxxx.com</a><br/>
                            the leading portal for escort ads<br/>
                            in Belgium, Holland and Luxembourg.</p>
                    </div>
                    <div class="content_box">
                        <div class="aprovation">
                            <?= $message; ?>
                        </div>
						<? if ( count($responce) ): ?>
						<div class="sale_person">
                            <p>Please contact your sales person with the payment details</p>
							<ul>
								<? foreach($responce as $key => $value): ?>
									<li><?= $key ?>: <?= $value ?></li>
								<? endforeach; ?>
							</ul>
                            <a class="bth ttu" href="/">back to home</a>
                        </div>
						<? endif; ?>	
                    </div>
                </div>
            </div>
			<? include('footer.php'); ?>
		</div>
	</body>
</html>