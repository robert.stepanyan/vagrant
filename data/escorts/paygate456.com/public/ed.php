<?php 
    header('location: https://www.paygate456.com/?test=1');
    exit;
    include './inc/ed.common.php';
    $page = page_content();
?>

<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" href="/ed/images/edirfavicon.png" type="image/png" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans|Roboto" rel="stylesheet">
    <link rel="stylesheet" href="/css/animate.css?v=1" />


    <link rel="stylesheet" href="/ed/bootstrap-4/fonts/font-awesome.min.css?v=1">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css?v=1" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="/ed/bootstrap-4/css/bootstrap.min.css">
    <!-- <link rel="stylesheet" href="/ed/bootstrap-4/css/material.min.css"> -->

    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.15/css/mdb.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" media="screen" href="/ed/css/ed.main.css?v=4" />

</head>

<body>

    <main id="mainWrapper">

    <div data-device="{{device}}" data-lang="{{lang}}" id="pageConf">

        <?php
            $token = isset($_SESSION['token']) ? $_SESSION['token'] : false;
            unset($_SESSION['token']);
        ?>

        <?php if($token): ?>
        <div class="ecardon-popup modal center" >
            <script>
                var wpwlOptions = {
                    onBeforeSubmitCard: function(){
                        //card holder validtion
                        if ( $(".wpwl-control-cardHolder").val() === "" )
                        {             
                            $(".wpwl-control-cardHolder").addClass("wpwl-has-error");
                            $(".wpwl-control-cardHolder").after("<div class='wpwl-hint wpwl-hint-cardHolderError'>Invalid card holder</div>");
                            $(".wpwl-button-pay").addClass("wpwl-button-error").attr("disabled", "disabled");
                            return false;
                        }
                        else
                            return true;
                    }
                }
            </script>

            <script async src="https://oppwa.com/v1/paymentWidgets.js?checkoutId=<?= $token ?>"></script>
            <form action="https://www.paygate456.com/ed.php#ecardonResponse/" class="paymentWidgets" data-brands="VISA MASTER"></form>
        </div>
        <?php endif; ?>


        <?php include "./ed/header.php" ?>
            
            <div class="content-box">

            <div id="root" class="page">
                <?php require './ed/main.php'; ?>
            </div>

            <div id="about" class="page" style="display: none">
                <?php require './ed/about.php'; ?>
            </div>

            <div id="terms" class="page" style="display: none">
                <?php require './ed/terms.php'; ?>
            </div>

            <div id="privacy" class="page" style="display: none">
                <?php require './ed/privacy.php'; ?>
            </div>

            <div id="ecardonResponse" class="page" style="display:none"></div>
            <div id="mmgResponse" class="page" style="display:none"></div>
            
            </div>
        <?php include "./ed/footer.php" ?>
    </div>
    </main>

    <!-- NEW JS FILES -->
    <script type="text/javascript" src="/ed/js/jquery.min.js"></script>
    <script type="text/javascript" src="/ed/bootstrap-4/js/popper.min.js"></script>
    <script type="text/javascript" src="/ed/bootstrap-4/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/ed/bootstrap-4/js/mdb.min.js"></script>

    <!-- OLD SCRIPTS -->
    <script type="text/javascript" src="/js/jquery.validate.js?v=3"></script>
    <script type="text/javascript" src="/js/pace.min.js?v=3"></script>
    <script type="text/javascript" src="/js/mustache.min.js?v=3"></script>
    <script type="text/javascript" src="/ed/js/t.js?v=4"></script>
    <script type="text/javascript" src="/js/routie.js?v=5"></script>
    <script type="text/javascript" src="/js/app.js?v=6"></script>
    <script type="text/javascript" src="/ed/js/script.js?v=1"></script>

  
</body>

</html>