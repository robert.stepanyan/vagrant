<?php include './inc/ed.common.php'; ?>

<?php
	$token =  preg_replace('#[^a-zA-Z0-9.\-]#', '', $_GET['id']);
	$error = true;

	if ($token){
		$alt_payment = new Cube_EcardonGateway();
		$result = $alt_payment->getStatus($token);
		$result_dec = json_decode($result);

		$copy_examples = 'MerchantTransactionId, Holder, Amount etc.';
		
		if (isset($result_dec->result) && ($result_dec->result->code == '000.000.000' || $result_dec->result->code == '000.100.110')) {

			$response = array(
				'Transaction Id' => $result_dec->descriptor,
				'Holder' => $result_dec->card->holder,
				'Amount' => $result_dec->amount,
				'Currency' => $result_dec->currency,
				'Payment Brand' => $result_dec->paymentBrand,
				'Date' => date('Y-m-d H:i:s', strtotime($result_dec->timestamp))   
			);

			$error = false;
		}

	} 

?>


<div class="container mt-4">

	<!-- Card content -->
	
	<div class="header mt-4">

		<?php if(isset($_SESSION['message'])): ?>
			<?php $message = $_SESSION['message'] ?>
			
			<?php if(isset($message['title'])): ?>
				<h2 class="page-title"><?= $message['title'] ?></h2>
			<?php endif; ?>

		<?php else: ?>

			<?php if($error): ?>
				<h2 class="page-title">An Error occured!</h2>
			<?php else: ?>
				<h2 class="page-title">Successful transaction!</h2>
			<?php endif; ?>

		<?php endif; ?>

	</div>

	<div class="card-body form-card card-body-cascade text-left">

		<div class="hero-page">

			<?php if(!empty($_SESSION['message'])): ?>

				<p><?= $_SESSION['message']['text'] ?></p>
				<p><a href="#">Back to home</a></p>

				<?php unset($_SESSION['message']) ?>

			<?php else: ?>

				<?php if ($error): ?>

					<h3 class="light-text hightlighted">Your payment was unsuccessful. </h3>
					<p>Please contact our staff for details.</p>

					<p><a href="#">Back to home</a></p>

				<?php else: ?>

				<form action="" method="POST" class="mail-feedback-form">
				<h3 class="light-text">Please <span class="bold-text green-text"> read & fill</span> in below</h3>

				<?php 
					// Testing data (successfull transaction)
//                if (isset($_GET['bblk']) && $_GET['bblk'] == "chjk") {
//                    $response = [
//                        'Transaction Id' => '1231321321',
//                        'Holder' => 'Shaga laga',
//                        'Amount' => 1234,
//                        'Currency' => 'EUR',
//                        'Payment Brand' => 'Mastercard',
//                        'Date' => date('Y-m-d H:i:s')
//                    ];
//                }

				?>

				<div class="footer">
					<p class="bold-text m-0">Transaction details:</p>
					<ul>
						<?php foreach($response as $key => $value): ?>
							<li><?= $key ?>: <?= $value ?></li>
						<?php endforeach; ?>
					</ul>
				</div>


				<div class="row">
					<div class="col-12 col-md-5">
						<div class="form-group fancy-form">
							<div class="md-form">
								<input autocomplete="off" required type="email" name="email" id="email-input" class="form-control pay-validate">
								<label for="email-input">YOUR EMAIL <i class="far for-valid-email fa-check-circle"></i></i></label>
							</div>
						</div>
					</div>
					<!-- Link/ Phone no/ Mail of the ad you paid for -->
					<div class="col-12 col-md-7">
						
						<div class="form-group fancy-form">
							<div class="md-form">
								<input autocomplete="off" required type="text" name="link"  id="link-input" class="form-control pay-validate">
								<label for="link-input">Link/ Phone no/ Mail of the ad you paid for<i class="far for-valid-email fa-check-circle"></i></i></label>
							</div>
						</div>
					</div>
				</div>

				

				<input type="hidden" name="payment_data" value="<?= htmlentities(serialize($response)); ?>">

				<button class="buy-now-btn buy-now-btn-green full mt-4" name="action" value="paymentInfoEmail">
					SEND INFO
				</button>


				</form>

				<?php endif; ?>


			<?php endif; ?>

		</div>
	</div>

</div>
<!-- Card -->

</div>