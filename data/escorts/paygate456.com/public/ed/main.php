<div class="container main-content">
    <div class="text-banner">
        <p class="large-text">{{main_title}}</p>
        <h1><span class="pinky-text">Escort</span>directory<span class="lightgray-text">.com</span> </h1>
    </div>

    <!-- Card content -->
    <div class="card-body form-card card-body-cascade text-center">


        <form action="/process.php" method="POST" id="paymentForm" class="payment-form m-t-30">
            <div class="form-wrapper">

                <? if(empty($page['prefilled'])): ?>
                <div class="amount-wrapper clearfix">
                    
                    <input type="number"  min="1" max="10000" id="amount-input" name="amount" value="<?= $page['prefilled'] ?>" />

                    <span class="currency">EUR</span>
                </div>
                <? else: ?>
                <div class="prefilled-text">
                    <span>
                        <?= $page['prefilled'] ?></span>
                    <span class="prefilled-currency">EUR</span>

                    <input type="hidden"  min="1" max="10000" id="amount-input" name="amount" value="<?= $page['prefilled'] ?>" />
                    <input type="hidden" name="prefilled" value="true" >
                    <input type="hidden" name="package" value="<?= $page['package'] ?>" >
                </div>
                <? endif; ?>

                <input type="hidden" name="lang" value=""/>
                <input type="hidden" name="page_name" value="ed.php" />
                <input type="hidden" name="payment_gateway" id="paymentGateway" value="ecardon" />

                <label for="ecardon" class='d-none'>
                    <input type="radio" checked="checked" data-method="ecardon" id="ecardon" />
                </label>

                <button type="submit" class=" buy-now-btn clearfix" id="buyBtn">{{buy_now}}</button>
            </div>


        </form>

        <p class="card-text mt-4">{{main_row1_text}}</p>


    </div>
    <!-- Card -->
</div>

