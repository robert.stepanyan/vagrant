;(function(global, $) {
	var App = function (id) {
		return this.init(id);
	};

	App.prototype = {
		init: function (id) {
			this.$app = $('#' + id);

			this
				.build()
				.addRouts()
				.addI18n()
				.bindEvents();
		},

		build: function() {
			$EcardonModal = $('.ecardon-popup');
			if ($EcardonModal.length) $EcardonModal.modal({backdrop: "static"});

			$redirectModal = $('.redirect-modal');
			if ($redirectModal.length) {
				$redirectModal.modal({backdrop: "static"});
				
				var 
					secCount = 4,
					timer = setInterval(function() {
						timer--;

						if (timer === 0) {
							clearInterval(timer);
							location.href = "//www.adspay-789.com";
						}
					}, 1000);
			}

			return this;
		},

		addRouts: function() {

			var _self = this;

			routie({
		        '': function () {
		        	$('.page').hide();
		        	$('#root').show();

		        	$('.main-nav').find('li').removeClass('selected');
		        	$('.main-nav').find('a[href="#"]').closest('li').addClass('selected');

		        	$('.hamburger-menu').find('li').removeClass('selected');
					$('.hamburger-menu').find('a[href="#"]').closest('li').addClass('selected');

					$('[data-device="mobile"] .subtitle-block').show();		        	
		        },
		        'about': function () {
		        	_self._buildRout('about');
		        },
		        'terms': function () {
		        	_self._buildRout('terms');
		        },
		        'sites': function () {
		        	_self._buildRout('sites');
		        },
		        'privacy': function () {
		        	_self._buildRout('privacy');
		        },
		        'ecardonResponse/*': function() {
		        	var 
		        		regExp = /#ecardonResponse\/(.*)/,
		        		search = regExp.exec(location.hash)[1];

		        	$.ajax({
		        		url:'/ed/ecardon_response.php' + search,
		        		method: 'GET',
		        		success: function(resp) {
		        			$('#ecardonResponse').html(resp);
		        			$('.ecardon-popup').modal().hide();
		        			$('.modal-backdrop').remove();
		        		}
		        	});
		        	_self._buildRout('ecardonResponse');
		        },
		        'mmgResponse/*': function() {
		        	var 
		        		regExp = /#mmgResponse\/(.*)/,
		        		search = regExp.exec(location.hash)[1];

		        	$.ajax({
		        		url:'/callback_mmg.php' + search,
		        		method: 'GET',
		        		success: function(resp) {
		        			$('#mmgResponse').html(resp);
		        		}
		        	});
		        	_self._buildRout('mmgResponse');
		        }
		    });

		    return this;
		},

		_buildRout: function(pageId) {
			var 
				$pages = $('.page'),
				$navEls = $('.main-nav').find('li'),
				$mobileNavEl = $('.hamburger-menu').find('li');

			$pages.hide();
		    $('#' + pageId).show();

		    $navEls
				.removeClass('selected')
				.find('a[href="#' + pageId + '"]')
				.closest('li')
				.addClass('selected');

			$mobileNavEl
				.removeClass('selected')
				.find('a[href="#' + pageId + '"]')
				.closest('li')
				.addClass('selected');

			var $menuWrapLi = $('.menu-icon-wrapper');

			if ($menuWrapLi.hasClass('selected')) {
				$menuWrapLi.removeClass('selected');
			}

			$('[data-device="mobile"] .subtitle-block').hide();
		},

		bindEvents: function() {
			var _self = this;

			$("#paymentForm").validate({
				rules: {
					amount: {
						required: true,
						number: true,
						min: 5,
						max: 10000
					}
				}
			});

			var $paymentOptions = $('.payment-option');

			$paymentOptions.on('click', function() {
				var paymentMethod = $(this).find('[type="radio"]').attr('data-method');

				$paymentOptions.removeClass('checked');
				$(this).closest('.payment-option').addClass('checked');
				
				$('#paymentGateway').val(paymentMethod);
			});

			var $langs = $('[class^=flag-], a[data-lang]');
			
			$langs.on('click', function() {
				var lang;

				if(!$(this).hasClass('dropdown-item'))
					lang = $(this).attr('class').replace('flag-', '');
				else
					lang = $(this).attr('data-lang').replace('flag-', '');

				_self._setCookie('ln', lang, 360);

				location.reload();
			});

			var 
				mobileNav = $('.hamburger-menu'),
				mobileNavBtn = $('.menu-icon-wrapper a');

			mobileNavBtn.on('click', function() {
				$(this).closest('li').toggleClass('selected');
				mobileNav.toggle();
			});

			return this;
		},

		addI18n: function() {
			var 
				ln = this._getCookie('ln') || 'en',
				dictionary = t[ln];

			this._setCookie('ln', ln, 360);

			$('#current-lang-flag').attr('src', '/images/' + ln + ".svg")

			
			$('#pageConf').data('lang', ln);
			t[ln].lang = ln;

			var 
				pageHtml = this.$app.html(),
				translated = Mustache.render(pageHtml, dictionary);

			this.$app.html(translated);

			return this;
		},

		_setCookie: function(cName, cValue, expDays) {
		    var d = new Date();
		    d.setTime(d.getTime() + (expDays * 24 * 60 * 60 * 1000));

		    var expiration = 'expires=' + d.toUTCString();
		    document.cookie = cName + '=' + cValue + ';' + expiration + ';path=/';
		},

		_getCookie: function (cName) {
		    var 
		    	name = cName + '=',
		    	cookies = document.cookie.split(';');

		    for (var i = 0; i < cookies.length; i++) {
		        var _cookie = cookies[i];

		        while (_cookie.charAt(0) == ' ') {
		            _cookie = _cookie.substring(1);
		        }

		        if (_cookie.indexOf(name) == 0) {
		            return _cookie.substring(name.length, _cookie.length);
		        }
		    }

		    return '';
		}
	};
	
	if (!global.App) global.App = App;
})(window, jQuery);

$(document).ready(function() {
	new App('mainWrapper');
});
