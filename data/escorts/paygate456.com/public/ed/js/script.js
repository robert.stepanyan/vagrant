var PayGate = {
    Home: {}
};

PayGate.Home = {
    init: function(){
        this.navbar_events();
        this.modal_events();
        this.calculateFooterPosition();
        this.validations();
    },

    navbar_events: function(){

        $('.nav-item').click(function(){
            $('.nav-item.active').removeClass('active');
            $(this).addClass('active');
        })
    },
    validations: function(){
        var self = this;

        $('input[type="number"]').keydown(function(e) {

            if(!((e.keyCode > 95 && e.keyCode < 106)
              || (e.keyCode > 47 && e.keyCode < 58) 
              || e.keyCode == 8)) {
                return false;
            }
        });

        $(document).on('input', "input[type='email'].pay-validate", function(){
            var text = $(this).val();

            if(self.validateEmail(text)){
                $(this).parent().find('.for-valid-email').show();
                $(this).parent().find('label').addClass('valid');
            }else{
                $(this).parent().find('.for-valid-email').hide();
                $(this).parent().find('label').removeClass('valid');

            }
        });

        
    },

    modal_events: function(){

        $(document).on('click', '.ecardon-popup', function(event){
            if($(event.target).hasClass('ecardon-popup')){
                window.location.reload();
            }
        })
    },

    calculateFooterPosition: function(){

        $('footer').css('display', 'block');
        $('footer').css('height', 'auto');
        var footerHeight = $('footer').outerHeight();
        $('body').css('padding-bottom', footerHeight);
        $('footer').css('height', footerHeight);
    },

    validateEmail: function(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }


};

$(document).ready(function(){
    PayGate.Home.init();
});


$(window).on("resize", function(){
    PayGate.Home.calculateFooterPosition();
});