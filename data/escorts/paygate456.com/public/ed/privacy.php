<div class="container mt-4">

<!-- Card content -->
<div class="card-body form-card card-body-cascade text-left">

    <div class="hero-page">
        <h2 class="page-title">{{advertising_terms_row1_title}}</h2>
        <p>{{advertising_terms_row1_text}}</p>
        <h3 class="light-text pinky-text">{{advertising_terms_row2_title}}</h3>
        <p>{{advertising_terms_row2_text}}</p>
        <h3 class="light-text pinky-text">{{advertising_terms_row3_title}}</h3>
        <p>{{advertising_terms_row3_text}}</p>
        <h3 class="light-text pinky-text">{{advertising_terms_row4_title}}</h3>
        <p>{{advertising_terms_row4_text}}</p>
        <h3 class="light-text pinky-text">{{advertising_terms_row5_title}}</h3>
        {{advertising_terms_row5_text}}
    </div>
</div>

</div>
<!-- Card -->

</div>