<nav class="navbar navbar-expand-lg navbar-dark black">
	<div class="container">
		<a class="navbar-brand" href="#"><strong><i>PAYGATE</strong>456</i></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
		 aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">

			</ul>
			<ul class="navbar-nav nav-flex-icons">
				<li class="nav-item active">
					<a class="nav-link waves-effect waves-light" href="#">{{home_page}}<span class="sr-only">(current)</span></a>
				</li>
				<!-- <li class="nav-item">
					<a class="nav-link waves-effect waves-light" href="#about">{{about_us}}</a>
				</li> -->
				<li class="nav-item">
					<a class="nav-link waves-effect waves-light" href="#terms">{{terms}}</a>
				</li>
				<li class="nav-item">
					<a class="nav-link waves-effect waves-light" href="#privacy">{{privacy}}</a>
				</li>

				<!-- Dropdown -->
				<!-- <li class="nav-item dropdown lang">
					<a class="nav-link dropdown-toggle" href="javascript:void(0)" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<img id="current-lang-flag" src="/images/en.svg" alt="EN" width="28" height="18">
					</a>

					<div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
						<a class="dropdown-item" data-lang="flag-en" href="javascript:void(0)"> <img src="/images/en.svg" alt="EN" width="28" height="18"> </a>
						<a class="dropdown-item" data-lang="flag-fr" href="javascript:void(0)"> <img src="/images/fr.svg" alt="EN" width="28" height="18"> </a>
					</div>
				</li> -->

			</ul>
		</div>
	</div>
</nav>