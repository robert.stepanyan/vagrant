<!-- Footer -->
<footer class="transparent-gray page-footer font-small teal pt-4 ">

	<!-- Footer Text -->
	<div class="container text-center text-md-left p-4">

		<!-- Grid row -->
		<div class="row">

			<!-- Grid column -->
			<div class="col-md-9 mt-md-0 mt-3">

				<!-- Content -->
				<p>AA Media Services GmbH, Industriestrasse 28, 9100 Herisau. AA Media Services GmbH is placed in Switzerland, where it pays taxes for all services. © Copyright 2018 / paygate456.com</p>

			</div>
			<!-- Grid column -->

			<hr class="clearfix w-100 d-md-none pb-3">

			<!-- Grid column -->
			<div class="col-md-3 mb-md-0 mb-3">

				<img src="/ed/images/mv.svg" class="payment-logo"/>

			</div>
			<!-- Grid column -->

		</div>
		<!-- Grid row -->

	</div>
	<!-- Footer Text -->


</footer>
<!-- Footer -->