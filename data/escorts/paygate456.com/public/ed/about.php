<div class="container mt-4">

    <!-- Card content -->
    <div class="card-body form-card card-body-cascade text-left">

        <div class="hero-page">
            <h2 class="page-title">{{about_us}}</h2>
            <p>{{index_left_block_row_1}}</p>
            <p>{{{index_left_block_row_2}}}</p>
            <div class="text-center mt-4">
                <p><strong>AA Media Services GmbH </strong></p>
                <p>Industriestrasse 28, 9100 Herisau</p>
                <p>CHE-389.533.593</p>
            </div>
        </div>
    </div>

</div>
<!-- Card -->


</div>