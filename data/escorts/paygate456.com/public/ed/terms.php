<div class="container mt-4">

    <!-- Card content -->
    <div class="card-body form-card card-body-cascade text-left">

        <div class="hero-page">
            <h2 class="page-title">{{advertising_terms}}</h2>
            <p>{{terms_top}}</p>
            <h3 class="light-text pinky-text">{{advertising_terms}}</h3>
            <p>{{terms_read_carefully}}</p>
            <ul>
                <li>{{terms_row1}}</li>
                <li>{{terms_row2}}</li>
                <li>{{terms_row3}}</li>
                <li>{{terms_row4}}</li>
                <li>{{terms_row5}}</li>
            </ul>
        </div>
    </div>

</div>
<!-- Card -->

</div>