<?php

class RedirectController extends Zend_Controller_Action
{
	private $host = '';
	
	public function init()
	{
		$host = $_SERVER['HTTP_HOST'];
		if ( $host !== 'www.escortforumvip.com' ) {
			$host = 'http://www.escortforumvip.com';
			$this->host = $host;
		}
	}

	public function indexAction()
	{
		header("HTTP/1.1 301 Moved Permanently");
		header('Location: ' . $this->host . $_SERVER['REQUEST_URI']);
		exit;
	}

	public function escort2Action()
	{
		$showname = Zend_Controller_Front::getInstance()->getRequest()->getParam('showname');
		$model = new Model_EscortsV2();
		$showname = trim($showname, '-');
		$id = $model->getIdByShowname($showname)->id;

		if ($id)
		{
			header("HTTP/1.1 301 Moved Permanently");
			header('Location: ' . $this->host . "/accompagnatrici/{$showname}-{$id}");
		}
		else
		{
			header("HTTP/1.1 301 Moved Permanently");
			header('Location: ' . $this->host . "/");
		}
		
		exit;
	}

	
}
