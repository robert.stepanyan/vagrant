<?php

class IndexController extends Zend_Controller_Action
{
	public function indexAction()
	{
		
	}
	
	public function listAction()
	{
		$filter = array('e.gender = ?' => 1, 'e.is_vip = ?' => 1);
		$escorts = Model_Escort_List::getFiltered($filter, 'random');

		$this->view->escorts = $escorts;
	}
	
	public function profileAction()
	{
		$req = $this->_request;
		
		$showname = $req->escort_name;
		$escort_id = $req->escort_id;

		$model = new Model_EscortsV2();
		
		//////   redirect not active escorts to home page   ///////
		if ( ! $model->getShownameById($escort_id) ) {
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			die;
		}
		//////////////////////////////////////////////////////////
		
		$cache_key = 'v2_' . $showname . '_' . $escort_id .  '_profile_' . Cubix_I18n::getLang();
		$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

		$this->view->profile_cache_key = 'html_profile_' . $cache_key;
		
		//$escort = $model->get($showname, $cache_key);
		$escort = $model->get($escort_id, $cache_key);
		if ( ! $escort || isset($escort['error']) )
		{
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			die;
			return;
		}		
		
		$has_vip = $model->checkVip($escort_id);
		
		if (!$has_vip)
		{
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			return;
		}

		$this->view->escort_id = $escort->id;
		$this->view->escort = $escort;

		if ( $escort->home_city_id ) {
			$m_city = new Cubix_Geography_Cities();
			$home_city = $m_city->get($escort->home_city_id);

			$m_country = new Cubix_Geography_Countries();
			$home_country = $m_country->get($home_city->country_id);

			$this->view->home_city_country = $home_city->{'title_' . $this->_request->lang_id} . ' (' . $home_country->{'title_' . $this->_request->lang_id} . ')';
		}

		$this->view->bubble_text = $model->getBubbleText($escort->id)->bubble_text;

		$this->view->is_profile = true;
		//$this->photosAction($escort);
		
		$this->view->has_package = $escort->hasProduct(1);
	}
	
	public function photosAction()
	{
		$req = $this->_request;
		$escort_id = $req->escort_id;
		
		if( $escort_id ) {
			$this->view->layout()->disableLayout();
			$model = new Model_EscortsV2();
			$escort = $model->get($escort_id, '_photos_vip_' . $escort_id);
			$this->view->escort_id = $escort_id;
			$this->view->escort = $escort;
			
			$count = 0;
			$photos = $escort->getPhotos(1, $count, false, false, null, 1000);		
			$this->view->photos = $photos;			
		}
	}

	protected function _ajaxError()
	{
		die(json_encode(array('result' => 'error')));
	}
}
