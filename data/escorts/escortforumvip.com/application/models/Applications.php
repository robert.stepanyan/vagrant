<?php

class Model_Applications
{
	public static function getAll()
	{
		$db = Zend_Registry::get('db');
		
		return $db->query('
			SELECT a.id, a.host, c.iso AS country_iso, c.' . Cubix_I18n::getTblField('title') . ' AS country_title
			FROM applications a
			INNER JOIN countries c ON c.id = a.country_id
			ORDER BY c.iso DESC
		')->fetchAll();
	}
}
