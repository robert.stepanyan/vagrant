<?php

class Model_Plugin_I18n extends Zend_Controller_Plugin_Abstract
{
	public function __construct()
	{
		
	}

	public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request)
	{
	
	}

	
	public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
	
    }

	public function routeShutdown($request)
	{
		$request->setParam('lang_id', 'it');
		
		Cubix_I18n::init();
		
		require 'Cubix/defines.php';
		Zend_Registry::set('definitions', $DEFINITIONS);
	}
}
