<?php

class Model_Escort_List extends Cubix_Model
{
	const PRODUCT_VIP = 16;
	
	public static function getFiltered($filter, $ordering = 'random')
	{
		$lng = Cubix_I18n::getLang();

		if (date('G') < 2)
			$date = 2;
		else
			$date = date('G');

		if ( 'price' == substr($ordering, 0, 5) ) {
			$filter[] = 'e.incall_price IS NOT NULL';
		}
		
		// Only escorts with base city (or city tour) will be shown in this list
		if ( ! isset($filter['ct.slug = ?']) && ! isset($filter['r.slug = ?']) ) {
			$filter[] = 'eic.is_base = 1';
		}
		else {
			$filter[] = 'eic.is_upcoming = 0';
		}
		
		$filter[] = 'FIND_IN_SET(' . self::PRODUCT_VIP . ', e.products) > 0';

		$where = self::getWhereClause($filter, true);
		$order = self::_mapSorting($ordering);

		$sql = '
			SELECT
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered,
				UNIX_TIMESTAMP(e.date_activated) AS date_activated,
				ct.title_' . $lng . ' AS city, e.rates, e.incall_type, e.outcall_type, e.is_new,
				e.verified_status, e.photo_hash, e.photo_ext, e.photo_status, 2 AS application_id,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price,

				IF (sei.primary_id IS NULL, se.title_' . $lng . ', sei.title_' . $lng . ') AS alt,
				IF (e.available_24_7 = 1 OR (e.is_now_open = 1 AND e.close_time >= "' . $date . '"), 1, 0) AS is_late_night,
				e.hit_count, e.slogan, e.incall_price,
				e.date_last_modified,
				eic.is_premium, e.hh_is_active, e.is_suspicious, e.is_online, eic.is_tour
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			INNER JOIN regions r ON r.id = eic.region_id
			LEFT JOIN escort_cityzones ecz ON ecz.escort_id = eic.escort_id
			LEFT JOIN cityzones cz ON cz.id = ecz.city_zone_id
			LEFT JOIN seo_entity_instances sei ON sei.primary_id = eic.escort_id
			LEFT JOIN seo_entities se ON (se.id = sei.entity_id) OR (se.application_id = 2 AND se.slug = "escort")
			LEFT JOIN escort_services es ON es.escort_id = e.id
			' . (! is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.escort_id
			ORDER BY ' . $order . '
		';


		$escorts = self::db()->fetchAll($sql);
		//$count = self::db()->fetchOne('SELECT FOUND_ROWS()');

		return $escorts;
	}

	

	private static function _mapSorting($param)
	{
		$map = array(
			'price-asc' => 'e.incall_price ASC',
			'price-desc' => 'e.incall_price DESC',
			'random' => 'eic.is_premium DESC, eic.ordering DESC',
			'alpha' => 'e.showname ASC',
			'most-viewed' => 'e.hit_count DESC',
			'newest' => 'e.date_activated DESC',
			'last-modified' => 'e.date_last_modified DESC'
		);
		
		$order = 'e.ordering DESC';
		if ( isset($map[$param]) ) {
			$order = $map[$param];
		}
		elseif ( false !== strpos($param, 'FIELD') ) {
			$order = $param;
		}

		return $order;
	}
}
