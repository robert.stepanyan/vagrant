<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	public function __construct($application)
	{
		parent::__construct($application);
	}
	
	public function run()
	{
		//Zend_Layout::startMvc();
		
		// Initialize hooks to avoid code confusion
		//Model_Hooks::init();
		
		parent::run();
	}

	protected function _initRoutes()
	{
		require('../application/models/Plugin/I18n.php');		
		
		$this->bootstrap('frontController');
		$front = $this->getResource('frontController');
		
		$router = $front->getRouter();
		$router->removeDefaultRoutes();

		$router->addRoute(
			'frontMain',
			new Zend_Controller_Router_Route(':controller/:action/*',
			array(
				'module' => 'default',
				'controller' => 'index',
				'action' => 'list',
				'lang' => NULL
			))
		);
		
		/*REDIRECT*/
		$router->addRoute(
			'front-profile-redirect2-def',
			new Zend_Controller_Router_Route_Regex(
				'^accompagnatrici/(.+)?',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'escort2',
					'lang' => NULL
				),
				array(
					1 => 'showname'
				)
			)
		);
		//

		$router->addRoute(
			'frontProfile',
			new Zend_Controller_Router_Route_Regex('^accompagnatrici/(.+)?\-([0-9]+)',
			array(
				'module' => 'default',
				'controller' => 'index',
				'action' => 'profile',
				'lang' => NULL
			),
			array(
				1 => 'escortName',
				2 => 'escort_id'
			))
		);

		$router->addRoute(
			'frontPhotos',
			new Zend_Controller_Router_Route_Regex('^photos/(.+)',
			array(
				'module' => 'default',
				'controller' => 'index',
				'action' => 'photos',
				'lang' => NULL
			), 
			array(
				1 => 'escort_id'
			))
		);
	}

	protected function _initAutoload()
	{
		$moduleLoader = new Zend_Application_Module_Autoloader(array(
			'namespace' => '',
			'basePath' => APPLICATION_PATH
		));

		$autoloader = Zend_Loader_Autoloader::getInstance();
		$autoloader->registerNamespace('Cubix_');

		return $moduleLoader;
	}

	protected function _initDatabase()
	{
		$this->bootstrap('db');
		$db = $this->getResource('db');
		$db->setFetchMode(Zend_Db::FETCH_OBJ);

		Zend_Registry::set('db', $db);
		Cubix_Model::setAdapter($db);

		$db->query('SET NAMES `utf8`');
	}

	protected function _initConfig()
	{
		Zend_Registry::set('images_config', $this->getOption('images'));
		Zend_Registry::set('escorts_config', $this->getOption('escorts'));
		Zend_Registry::set('feedback_config', $this->getOption('feedback'));
		Zend_Registry::set('system_config', $this->getOption('system'));
	}

	protected function _initViewHelpers()
	{
		$this->bootstrap('layout');
		$layout = $this->getResource('layout');
		$view = $layout->getView();

		$view->doctype('XHTML1_STRICT');

		$view->headMeta()
			->appendHttpEquiv('Content-Type', 'text/html; charset=utf-8')
			->appendHttpEquiv('Cache-Control', 'no-cache');
		$view->headTitle()->setSeparator(' - ');
		//$view->headTitle('EscortForum Backend');
	}
	
	protected function _initDefines()
	{
		
	}

	protected function _initCache()
	{
		$frontendOptions = array(
			'lifetime' => null,
			'automatic_serialization' => true
		);

		$backendOptions = array(
			'servers' => array(
				array(
					//'host' => 'localhost',
					'host' => 'fe01',
					'port' => '11211',
					'persistent' =>  true
				)
			),
		);

		$cache = Zend_Cache::factory('Core', new Cubix_Cache_Backend_Memcached($backendOptions), $frontendOptions);

		Zend_Registry::set('cache', $cache);
	}

	protected function _initBanners()
	{
		$banners = array();

		$cache = Zend_Registry::get('cache');

		$config = Zend_Registry::get('system_config');

		$banners['banners']['list_banners'] = array();
		$banners['banners']['right_banner'] = new stdClass();

		/*if ( ! $banners = $cache->load('v2_banners_cache') ) {
			$banners['funcs'] = Cubix_Banners::GetFunctions();

			for ($i = 0; $i < 4; $i++) {
				$banners['banners']['list_banners'][] = Cubix_Banners::GetBanner(17);
			}

			$banners['banners']['right_banner'] = Cubix_Banners::GetBanner(53);
			
			$cache->save($banners, 'v2_banners_cache', array(), $config['bannersCacheLifeTime']);
		}*/
		//var_dump($banners);die;
		Zend_Registry::set('banners', $banners);
	}

	protected function _initApi()
	{
		$api_config = $this->getOption('api');
		Zend_Registry::set('api_config', $api_config);
		Cubix_Api_XmlRpc_Client::setApiKey($api_config['key']);
		Cubix_Api_XmlRpc_Client::setServer($api_config['server']);
	}
}
