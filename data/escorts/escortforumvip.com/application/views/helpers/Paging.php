<?php

class Zend_View_Helper_Paging
{
	public function paging($count, $page, $perPage)
	{
		$pages = ceil($count / $perPage);
		
		$start_page = 1;
        $end_page = $pages;
        
        if ( 20 < $pages )  {
            $start_page = 1;
            $end_page = 20;
            if ( 10 < $page ) {
                $start_page = $page - 9;
                $end_page = $page + 10;
                if ( $page + 10 > $pages ) {
                    $start_page = $pages - 20;
                    $end_page = $pages;
                }
            }
        }
        
        $pages_arr = array();
        
        for ($i = $start_page; $i <= $end_page; $i++) {
            $pages_arr[] = $i;
        }
        
        return $pages_arr;
	}
}
