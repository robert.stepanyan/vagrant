<?php

class Zend_View_Helper_Seo extends Zend_View_Helper_Abstract
{
	public function seo($entity, $primary_id, $tpl_data = array(), $auto_meta = false)
	{
		$data = Model_Seo_Instances::get($entity, $primary_id);
		
		$tpl_data = array_merge(array(
			'app_title' => Cubix_Application::getById()->title
		), $tpl_data);
		
		foreach ( (array) $data as $key => $value ) {
			foreach ( $tpl_data as $tpl_var => $tpl_value ) {
				$data->$key = str_replace('%' . $tpl_var . '%', $tpl_value, $data->$key);
			}
		}
		
		if ( $auto_meta && $data ) {
			if ( strlen($data->title) ) {
				$this->view->headTitle($data->title, 'PREPEND');
			}
			
			if ( strlen($data->keywords) ) {
				$this->view->headMeta()->appendName('keywords', $data->keywords);
			}
			
			if ( strlen($data->description) ) {
				$this->view->headMeta()->appendName('description', $data->description);
			}
		}
		
		return $data;
	}
}
