<?php
$LNG['ladies'] = "Donne";
$LNG['latest_join'] = "Le ultime arrivate";
$LNG['width_comments'] = "Larghezza commenti";
$LNG['100_fake_free_review'] = "Foto originali al 100%";
$LNG['100_fake_free_review_desc'] = "Recensione verificata al 100% è quando l'agenzia/escort conferma in sms che l'appuntamento è avvenuto, in questo modo tutte e due le parti sono d'accordo sull'incontro e la recensione è 100% originale!";
$LNG['24_7'] = "24/7";
$LNG['PROD_additional_city'] = "Aggiungi citt?";
$LNG['PROD_city_premium_spot'] = "Silver premium spot";
$LNG['PROD_girl_of_the_month'] = "Ragazza del mese";
$LNG['PROD_international_directory'] = "Indice internazionale";
$LNG['PROD_main_premium_spot'] = "Gold premium spot";
$LNG['PROD_national_listing'] = "Lista nazionale";
$LNG['PROD_no_reviews'] = "No recensione";
$LNG['PROD_search'] = "Cerca";
$LNG['PROD_tour_ability'] = "Disponibilità tour";
$LNG['PROD_tour_premium_spot'] = "&quot;On tour&quot; premium spot";
$LNG['_all'] = "Tutte";
$LNG['about_me'] = "Su di me";
$LNG['about_meeting'] = "Sull'appuntamento";
$LNG['account_activated'] = "Il tuo account è stato attivato con successo, per favore accedi e crea il tuo profilo/le tue escorts.  <a href='%sys_url%/login.php'>Clicca qui</a> per accedere.";
$LNG['account_blocked_desc'] = "<p>Il tuo account è stato sospeso per uno dei seguenti motivi: </p> 
<ul> 
<li>Hai insultato un altro utente/ una escort</li> 
<li>Discriminazione di un altro utente/una escort</li> 
<li>Hai scritto recensioni o post falsi</li> 
<li>Istigazione di altri u???    ?????? ? ?? ????( ?À  ???  ?  ???? ?    ?????   ??????À ??????????????????????  ?????????????  ????W?(  ?(??? &  ?1??????1?(  ?1À  ????1? ??1? ????1???1????1??? ?Item  ??      ?Count  ????1?0  ?1    0????0????????  ??????   0   ??  ??? ?  ??????  ?W??  ?ò   ???  ??W???W?  ??? ?W?W  ???t";
$LNG['account_not_verified'] = "L'account non è stato confermato.";
$LNG['account_type'] = "Tipo di account";
$LNG['active'] = "Attivo";
$LNG['add'] = "Aggiungi ";
$LNG['add_escort'] = "Aggiungi profilo escort";
$LNG['add_escort_profile'] = "Aggiungi profilo escort";
$LNG['add_profile_now'] = "Aggiungi il tuo profilo con foto ADESSO";
$LNG['add_review'] = "Aggiungi recensioni";
$LNG['add_third_party_girl'] = "Aggiungi una ragazza di un altro sito al nostro database";
$LNG['add_watch'] = "Aggiungi alla Lista dei Favoriti";
$LNG['add_watch_success'] = "<b>%NAME%</b> è stata aggiunta alla Lista dei Favoriti con successo. <a href='javascript:history.back()'>Clicca qui per tornare indietro.</a>";
$LNG['additional'] = "Altre";
$LNG['address'] = "Via";
$LNG['age'] = "Età";
$LNG['age_between'] = "Età tra";
$LNG['agencies'] = "Agenzie";
$LNG['agencies_match'] = "Agenzie che corrispondono alle criterie";
$LNG['agency'] = "Agenzia";
$LNG['agency_city_tour_not_allowed'] = "Sono elencate solo le ragazze non in periodo di prova. City Tour non è applicabile per escort in periodo di prova. Compra un Premium spot per abilitarle il City Tour! Clicca <a href='mailto:%sys_contact_email%'>qui per comprare</a>.";
$LNG['agency_data'] = "Dati agenzia";
$LNG['agency_denied_review'] = "Rifiutato";
$LNG['agency_denied_review_desc'] = "L'agenzia/ragazza smentisce che c'è stato un appuntamento con quei dati, probabilmente questa è una recensione falsa, perché l'agenzia/ragazza non sa se la recensione è positiva/negativa quando conferma che l'appuntamento è avvenuto.";
$LNG['agency_girls'] = "Escort di agenzia";
$LNG['agency_name'] = "Nome agenzia";
$LNG['agency_profile'] = "Profilo agenzia";
$LNG['agency_without_name'] = "Agenzia senza Nome";
$LNG['ajaccio'] = "Ajaccio";
$LNG['all'] = "Tutti/e";
$LNG['all_cities'] = "Tutte le città";
$LNG['all_countries'] = "Tutti i paesi";
$LNG['all_escorts'] = "Tutte le escort";
$LNG['all_origins'] = "Tutte le nazionalità";
$LNG['all_reviews'] = "Tutte le recensioni";
$LNG['already_registered_on_another_site'] = "Se sei già registrato/a su %SITE% , non hai bisogno di fare un'altra registrazione. Semplicemente accedi con il tuo  %SITE% login e password.";
$LNG['already_reviewed'] = "Hai già scritto una recensione su questa persona! E' possibile lasciare una sola recensione per escort, se vuoi aggiorna la tua recensione attuale!";
$LNG['already_voted'] = "Hai già votato questa ragazza.";
$LNG['also_provided_services'] = "Fornisce anche i seguenti servizi";
$LNG['anal'] = "Sesso Anale";
$LNG['are_you_escort'] = "Sei una escort indipendente, agenzia o privè?";
$LNG['at_her_apartment'] = "Al suo appartamento";
$LNG['at_her_hotel'] = "In albergo da lei";
$LNG['at_my_flat_or_house'] = "A casa mia";
$LNG['at_my_hotel'] = "In albergo da me";
$LNG['attention'] = "Attenzione!";
$LNG['attitude'] = "Attitudine";
$LNG['availability'] = "Disponbilità";
$LNG['available_for'] = "Disponibile per";
$LNG['basic'] = "Base";
$LNG['back_to_actual_tours'] = "Torna ai tour attuali";
$LNG['back_to_reviews'] = "Torna alle recensioni";
$LNG['beauty'] = "Bellezza";
$LNG['become_premium_member_and_view_private_photos'] = "<a href='%LINK%'>Diventa un Premium Member adesso</a> e vedi le foto speciali e senza censura di questa ragazza!";
$LNG['bio'] = "Bio";
$LNG['birth_date'] = "Data di nascità";
$LNG['black'] = "Neri";
$LNG['blond'] = "Biondi";
$LNG['blowjob'] = "Blowjob";
$LNG['blowjob_with_condom'] = "Blowjob coperto";
$LNG['blowjob_without_condom'] = "Blowjob scoperto";
$LNG['blue'] = "Blu/Azzurri";
$LNG['bombshellf'] = "Sex bomb";
$LNG['book_email_to_escort'] = "<pre> 
Gentile %ESCORT%, 
   un Utente Premium del sito %sys_site_title% vorrebbe incontrarti il %DATE%. 
Questo utente vorrebbe utilizzare lo sconto del %DISCOUNT%%, che tu hai deciso di offrire agli utenti fidati.
Dettagli: 
Nome utente: %USERNAME% 
Nom???    ?????? ? ?? ????( ?À  ???  ?  ???? ?    ?????   ??????À ??????????????????????  ?????????????  ????W?(  ?(??? &  ??????????(  ??À  ?????? ???? ????????????????? ?Item  ??      ?Count  ??????0  ??    0????0????????  ??????   0   ??  ??? ?  ??????  ?W??  ?ò   ???  ??W???W?  ??? ?W";
$LNG['book_this_girl'] = "Prenota questa ragazza";
$LNG['booking_desc'] = "Grazie per il tuo interessamento a prenotare %NAME%. Come Utente Premium hai il diritto di avere uno sconto del %DISCOUNT%% quando incontri questa escort. Compila per favore il seguente modulo per prenotare la ragazza ed usufruire dello sconto!";
$LNG['booking_mail_subject'] = "Prenotazione %SITE%";
$LNG['booking_mail_text'] = "<pre> 
Gentile %NAME%, 
hai prenotato un appuntamento con  %ESCORT_NAME% per la seguente data: %DATE%. 
La ragazza ti contetterà in breve per mettersi d'accordo sui dettagli dell'appuntamento. 
Come Utente Premium del sito %SITE% hai il diritto di usufrui???    ?????? ? ?? ????( ?À  ???  ?  ???? ?    ?????   ??????À ??????????????????????  ?????????????  ????W?(  ?(??? &  ??????????(  ??À  ?????? ???? ????????????????? ?Item  ??      ?Count  ??????0  ??    0????0????????  ??????";
$LNG['boys'] = "Uomini";
$LNG['boys_trans'] = "Ragazzi/Trans";
$LNG['breast'] = "Seno";
$LNG['breast_size'] = "Misura del seno";
$LNG['brown'] = "Castani";
$LNG['bruntal'] = "Bruntál";
$LNG['bust'] = "Seno";
$LNG['by'] = "da";
$LNG['calabria'] = "Calabria";
$LNG['can_have_max_cities'] = "Puoi aggiungere al massimo %NUMBER% città";
$LNG['cancel'] = "Cancella";
$LNG['cant_be_empty'] = "Non può rimanare vuoto";
$LNG['cant_go_ontour_to_homecity'] = "Escort non può andare in city tour nella tua città.";
$LNG['castelfranco'] = "Castelfranco";
$LNG['chalons-en-champagne'] = "Chalons-en-Champagne";
$LNG['change_all_escorts'] = "Applica i cambiamenti per tutti i profili";
$LNG['change_passwd'] = "Cambia password";
$LNG['char_desc'] = "tatoo, etc.";
$LNG['characteristics'] = "Caratteristiche?";
$LNG['chiavari'] = "Chiavari";
$LNG['choose'] = "Scegli";
$LNG['choose_availability'] = "Scegli disponibilità";
$LNG['choose_another_city'] = "Scegli un'altra città";
$LNG['choose_another_region'] = "Scegli un\"altra regione";
$LNG['choose_city'] = "Scegli città";
$LNG['choose_country'] = "Scegli paese";
$LNG['choose_currency'] = "Scegli moneta";
$LNG['choose_escort'] = "Scegli escort";
$LNG['choose_escort_to_promote'] = "Scegli escort da promuovere";
$LNG['choose_language'] = "Scegli la lingua";
$LNG['choose_level'] = "Scegli livello";
$LNG['choose_ethnic'] = "Scegli etnia";
$LNG['choose_nationality'] = "Scegli nazionalità";
$LNG['choose_state'] = "Scegli sato";
$LNG['choose_this_user'] = "Scegli questo utente";
$LNG['choose_time'] = "Scegli orario";
$LNG['choose_your_city'] = "Scegli la tua città";
$LNG['choose_your_country'] = "Scegli il tuo paese";
$LNG['choosen_escort'] = "Ragazza scelta: %NAME%";
$LNG['cities'] = "Città";
$LNG['city'] = "Città";
$LNG['city_escort_notify'] = "Ricevere notifica via e-mail quando una escorto viene aggiunta nella tua città";
$LNG['city_of_meeting'] = "Città d'incontro";
$LNG['claim_your_discount'] = "Prenota questa ragazza ADESSO e usufruisci del tuo sconto!";
$LNG['comment_review'] = "Commenta recensione";
$LNG['comment'] = "Commenta";
$LNG['comments'] = "Commenti";
$LNG['comments_to_her_services'] = "Commenta i suoi servizi";
$LNG['comments_to_her_services_desc'] = "Descrivi il lato sessuale, per esempio servizi speciali ";
$LNG['company_name'] = "Nome agenzia";
$LNG['conf_password'] = "Conferma password";
$LNG['confirmation_email_1'] = "Gentile %USER%, grazie per esserti registrato su %SITE%. I tuoi dati d'accesso sono i seguenti: ";
$LNG['confirmation_email_2'] = "Tieni queste informazioni in un posto sicuro.";
$LNG['confirmation_email_subject'] = "Registrazione %sys_site_title% ";
$LNG['contact'] = "Contattaci";
$LNG['contact_her'] = "Contatta la ragazza direttamente";
$LNG['contact_her_desc'] = "Stai contattando %NAME%. Questa email arriverà dal dominio %SITE% e sarà certificata per dimostrare la tua affidabilità come Utente Premium del sito %SITE% ";
$LNG['contact_her_mail_header'] = "<pre> 
************************************************************************************* 
Questa è una email verificata da un Utente Premium del sito  %SITE%. 
Questo utente è un membro affidabile della nostra comunità e paga regolarmente il canone d'???    ?????? ? ?? ????( ?À  ???  ?  ???? ?    ?????   ??????À ????????????????";
$LNG['contact_her_mail_subject'] = "Messaggio da un Utente Premium del sito %SITE%";
$LNG['contact_info'] = "Contatto Info";
$LNG['contact_text'] = " 
Escortforum pubblicità<BR /><BR /> 
Noi come la communità di escort più grande in %sys_app_country_title% Vi offriamo spazio di pubblicità sul nostro sito. 
Limitiamo il numero dei siti pubblicizzati a 10, che significa un ottima frequenza di click per ogni banner.<BR /> 
<BR /> 
Alcune statistiche:<BR /> 
<BR /> 
Hits: 3 millioni in media, e in continuo incremento <BR /> 
Visitatori: in media piu di 25.000 al giorno <BR /> 
Membri registrati: 15000 attualmente e in continua crescita<BR /> 
<BR /> 
<BR /> 
Se volete provare e discutere sugli dettagli ora, contattateci. Vi rispondiamo immediatamente.<BR /> 
<BR /> 
<a href='mailto:%sys_contact_email%'>%sys_contact_email%</a> 
Tel.: 0036 1203 1808 (10:00 - 20:00) 
";
$LNG['contact_us'] = "Contatti/Annunci";
$LNG['continue'] = "Continua";
$LNG['conversation'] = "Conversazione";
$LNG['countries'] = "Paesi";
$LNG['country'] = "Paese";
$LNG['country_tw'] = "Taiwan";
$LNG['couples'] = "Coppie";
$LNG['created'] = "Creata/o";
$LNG['cumshot'] = "Venire";
$LNG['cumshot_on_body'] = "Venire sul corpo";
$LNG['cumshot_on_face'] = "Venire in viso";
$LNG['curr_password'] = "Password corrente";
$LNG['curr_password_empty_err'] = "Devi inserire la tua password corrente";
$LNG['currency'] = "Valuta";
$LNG['currently_listed_sites'] = "Abbiamo elencato qui la lista dei siti da cui scegliere. Scegli il sito!";
$LNG['date'] = "Data";
$LNG['day'] = "Giorno";
$LNG['day_of_meeting'] = "Data dell'incontro";
$LNG['day_phone'] = "Telefono";
$LNG['days'] = "giorni";
$LNG['default_city'] = "Città predefinita dopo log in";
$LNG['delete_rates'] = "Cancella tutti i prezzi";
$LNG['delete_sel_pcis'] = "Cancella le foto selezionate";
$LNG['delete_selected_reviews'] = "Cancella le recensioni selezionate";
$LNG['delete_selected_tour'] = "Cancella il tour selezionato";
$LNG['desired_date'] = "Data desiderata";
$LNG['discount'] = "Sconto";
$LNG['discount_code'] = "Codice sconto";
$LNG['discount_code_txt'] = "Per usufruire dello sconto del 5%, digita il tuo tuo codice sconto, se ti è stato dato il codice da uno dei nostri partner.";
$LNG['discount_desc'] = "In questa pagina ti presentiamo tutte le ragazze che offrono lo sconto del <b>discount</b>  agli Utenti Premium";
$LNG['discounted_girls'] = "Ragazze che offrono sconti";
$LNG['dont_forget_variable_symbol'] = " 
    <p>RICORDATI di inserire il tuo variable symbol: <big>%ESCORT_ID%</big>.<br /> 
    Senza questo non riusciamo ad identificare il tuo pagamento.<br /> 
    Grazie.</p> 
";
$LNG['dont_know'] = "Non lo so.";
$LNG['dress_size'] = "Taglia del vestito";
$LNG['duration'] = "Durata";
$LNG['easy_to_get_appointment'] = "Facile da incontrare";
$LNG['edit'] = "Modifica";
$LNG['edit_escort'] = "Modifica profilo escort";
$LNG['edit_gallery'] = "Modifica galleria";
$LNG['edit_languages'] = "Modifica lingua";
$LNG['edit_locations'] = "Modifica locations";
$LNG['edit_private_photos'] = "Modifica foto private";
$LNG['edit_private_photos_desc'] = "Carica qui le tue foto non censurate (senza nascondere alcuna parte del corpo) e le foto glamour che solo i nostri utenti Premium potranno vedere. In questo modo ti assicuriamo che le tue foto private saranno viste solo da clienti di alto livello che hann???    ?????? ? ?? ????(";
$LNG['edit_profile'] = "Aggiorna profilo";
$LNG['edit_rates'] = "Modifica prezzi";
$LNG['edit_review'] = "Aggiorna recensione";
$LNG['edit_settings'] = "Aggiorna impostazioni";
$LNG['edit_units_in'] = "Aggiorna unità in";
$LNG['email'] = "Email";
$LNG['email_domain_blocked_err'] = "Siamo spiacenti ma %DOMAIN% attualmente non accetta email da noi. Per favore scelga un indirizzo email o contatta %DOMAIN% per accettare la nostra corrispondenza.";
$LNG['email_lng'] = "Lingua preferita nelle email";
$LNG['email_not_registered'] = "Questo indirizzo email non è registrato presso il nostro sito.";
$LNG['empty_login_err'] = "Il campo Login non può essere vuoto.";
$LNG['empty_passwd_err'] = "Il campo Password non può essere vuoto.";
$LNG['empty_watchlist'] = "Non hai nessuno nella tua Lista dei Favoriti";
$LNG['english'] = "Inglese";
$LNG['enthusiastic'] = "entusiasta";
$LNG['error'] = "Errore";
$LNG['error_adding_watch'] = "Si è verificato un'errore. Per favore contatta l'amministratore del sito.";
$LNG['esc_mb_chng_subject'] = "Cambiamento utenza sul sito  %sys_site_title%";
$LNG['esc_mb_chng_to_free_email'] = "<pre>Gentile %LOGIN%, 
  l'utenza del profilo escort %SHOWNAME% è stata cambiata in \"Gratuita\". 
Hai perso tutti i vantaggi della tua utenza Premium.
Acquista la tua utenza premium adesso e potrai: 
- partecipare nella rotazioine della pagina principale 
???    ?????? ? ?? ????( ??  ???  ?  ???? ?    ?????   ??????? ??????????????????????  ?????????????  ????W?(  ?(??? &  ??????????(  ??À  ?????? ???? ????????????????? ?Item  ??      ?Count  ??????0  ??    0????0????????  ??????   0   ??  ??? ?  ??????  ?W??  ?ò   ???  ??W???W?  ??? ?W?W  ???t ??? ? ? ??????    ??0Eo  ?0  ?????  ????0??i  ??????????0i  ????????????  ??????????????? ?? ??????? ????????????W ?   ?????    ?? ???????    ?? ????t ?? ???    ?  ?";
$LNG['esc_mb_chng_to_normal_email'] = "<pre>Gentile %LOGIN%, 
   l'utenza del tuo profilo escort %SHOWNAME% è stato cambiato in \"Normale\". 
Data di scadenza: %EXPIRE% 
Da adesso potrai: 
- partecipare nella rotazioine della pagina principale 
-non sarai mai nelle ultime pagine dove gli utenti ???    ?????? ? ?? ????( ??  ???  ?  ???? ?    ?????   ??????? ??????????????????????  ?????????????  ????W?(  ?(??? &  ??????????(  ??À  ?????? ???? ????????????????? ?Item  ??      ?Count  ??????0  ??    0????0????????  ??????   0   ??  ??? ?  ??????  ?W??  ?ò   ???  ??W???W?  ??? ?W?W  ???t ??? ? ? ??????    ??0Eo  ?0  ?????  ????0??i  ??????????0i  ????????????  ??????????????? ?? ??????? ????????????W ?   ??";
$LNG['esc_mb_chng_to_premium_email'] = "<pre>Gentile %LOGIN%, 
 l'utenza del tuo profilo escorto %SHOWNAME% è stato cambiato in \"Premium\". 
Data di scadenza: %EXPIRE% 
Da adesso potrai: 
- UNDER CONSTRUCTION 
%sys_site_title% Team 
</pre> 
";
$LNG['esc_mb_chng_to_trial_email'] = "<pre> Caro/a %LOGIN%, 
l’iscrizione della tua escort con il nome %SHOWNAME% è stata upgradata a \"prova\". 
La nuova data di scadenza è: %EXPIRE% 
Il tuo stato di utenza è stato approvato in account di %sys_site_title%. 
In questo periodo di prova avrai gli stessi privilegi degli utenti iscritti a pagamento: 
- partecipazione alla rotazione delle ragazze nella main page, significa che non rimarrai mai fermo nelle ultime pagine del sito dove nessun utente può vederti. Tutti i profili a pagamento, vengono ruotati ogni ora, quindi tutti hanno la stessa possibilità di apparire nella prima pagina di una particolare città o nella main page; 
- potrai partecipare al concorso \�?Ragazza del mese\�? 
- avrai la possibilità di partecipare alla lista delle ragazze TOP10 nella sezione recensioni. 
Dopo il periodo di prova, dovrai fare l’upgrade ad una iscrizione a pagamento, altrimenti perderai i privilegi e il tuo profilo apparirà nell’ultima pagina. 
<!—Fai l’UPGRADE ADESSO e prendi la speciale offerta: 
- un annuncio il prossimo mese nella rivista Redmag magazine in %sys_app_country_title%.--> 
Per fare l’upgrade puoi rispondere a questa mail, oppure accedere al sito di %sys_site_title% con il tuo account e cliccare \" FARE L’UPGRADE ADESSO!\". 
%sys_site_title% Team 
</pre> 
";
$LNG['esc_mb_chng_to_vip_email'] = "<pre>Gentile %LOGIN%, 
l'utenza del profilo escort %SHOWNAME% è stato cambiato in \"VIP\". 
Il tuo profilo non scade e avrai gli stessi vantaggi dell'utenza normale.
%sys_site_title% Team 
</pre> 
";
$LNG['esc_mb_memb_expired_email'] = "<pre>Gentile %LOGIN%, 
l'utenza del tuo profilo escort  %SHOWNAME% è scaduta.
Hai perso tutti i tuoi vantaggi e il tuo account è stato disabilitato.
Se vuoi rinnovare la tua utenza, contattaci al %sys_contact_email%. 
%sys_site_title% Team 
</pre> 
";
$LNG['esc_mb_memb_expired_email_subj'] = "La sua utenza è scaduta";
$LNG['esc_mb_memb_expires_soon_email'] = "<pre>Gentile %LOGIN%, 
  la utenza del suo profilo escort con il nome %SHOWNAME% scade in %DAYS% giorni, 
per favore non dimenticare di rinnovarlo. Per farlo contattaci su %sys_contact_email%. 
%sys_site_title% Team 
</pre> 
";
$LNG['esc_mb_memb_expires_soon_email_subj'] = "La sua utenza scade tra poco";
$LNG['esc_to_upg'] = "Profilo escort aggiornato";
$LNG['esc_upg_promo'] = " 
<ul> 
<li>partecipare nella rotazioine della pagina principale significa che il tuo profilo non sarà mai nelle ultime pagine del sito dove gli utenti non potranno vederlo. Tutti i profili a pagamento ruotano ogni ora, quindi ogni profilo ha la stessa pr???    ?????? ? ?? ????( ?À  ???  ?  ???? ?    ?????   ??????À ??????????????????????  ?????????????  ????W?(  ?(??? &  ??????????(  ??À  ?????? ???? ????????????????? ?Item  ??      ?Count  ??????0  ??    0????0???";
$LNG['esc_upg_promo_txt'] = "<a href='/private/upgrade_escort.php'>Acquista Pacchetto Premium</a>";
$LNG['esc_upgrade_payment_details'] = " 
    Thank you for your interest in purchasing membership on %sys_site_title%.<br /> 
    Please transfer %AMOUNT% EUR to our account and your profile will be update immediatelly after the money arrival.<br /> 
    <p><strong>Bank details:</strong><br />???    ?????? ? ?? ????( ?À  ???  ?  ???? ?    ?????   ??????À ??????????????????????  ?????????????  ????W?(  ?(??? &  ?1??????1?(  ?1À  ????1? ??1? ????1???1????1??? ?Item  ??      ?Count  ????1?0  ?1    0????0????????  ??????   0   ??  ??? ?  ??????  ?W??  ?ò   ???  ??W???W?  ??? ?W?W  ???t ??? ";
$LNG['escort'] = "Escort";
$LNG['escort_active'] = "Il tuo profilo escort è attivo. <a href='%LINK%'>Clicca qui</a> per disattivare";
$LNG['escort_added_continue_review'] = "Escort aggiunta/o con successo. <a href='%LINK%'>Clicca qui</a> per aggiungere la recensione.";
$LNG['escort_city_tour_not_allowed'] = "Non puoi usare la funzione City Tour durante il periodo di prova. Compra un Premium spot per poter usare questa opzione. Clicca qui <a href='mailto:%sys_contact_email%'>per comprare</a>.";
$LNG['escort_data_form_text'] = "Compila solo le informazioni che vuoi condividere sul sito.";
$LNG['escort_inactive'] = "Questa escort attuamente non è attiva/o";
$LNG['escort_modification_disabled'] = "In questo momento stiamo facendo manutenzione, al momento non è possibile fare alcuna modifica nei profili escort. Per favore torna al sito più tardi. Ci scusiamo per l'inconveniente.";
$LNG['escort_name_active'] = "Il profilo di %ESCORT% è attivo. <a href='%LINK%'>Clicca qui</a> per disattivarlo.";
$LNG['escort_name_not_active'] = "Il profilo di %ESCORT% è attivo. <a href='%LINK%'>Clicca qui</a> per disattivarlo.";
$LNG['escort_name_waiting_for_approval'] = "Il profilo di %ESCORT% sta aspettando l'approvazione dell'amministatore del sito.";
$LNG['escort_not_active'] = "Il tuo profilo escort non è attivo. <a href='%LINK%'>Clicca qui</a> per attivarlo.";
$LNG['escort_not_found_use_search'] = "Escort %NAME% non è identificata, per favore cerca un altro nome";
$LNG['escort_not_in_db_add_her'] = "Se questo/a escort non è presente nel nostro database, <a href='%LINK%'>aggiungilo/a adesso</a>.";
$LNG['escort_profile_added'] = "Il profilo escort è stato aggiunto al nostro database con successo!<BR /> Per favore
<a href='/private/edit_gallery.php'>carica le foto per il tuo profilo.</a>";
$LNG['escort_profile_updated'] = "Il profilo escort è stato aggiornato con successo! <BR /> 
<a href='/private/index.php'>Torna alla pagina precedente.</a>";
$LNG['escort_waiting_for_approval'] = "Il tuo profilo escort sta aspettando l'approvazione dell'amministratore del sito.";
$LNG['escorts'] = "Escorts";
$LNG['escorts_in_city'] = "Escorts a %CITY%";
$LNG['escorts_match'] = "Escorts corrispondenti alle criterie di ricerca";
$LNG['evening_phone'] = "Altro numero di telefono";
$LNG['expires'] = "Scadenza";
$LNG['eye_color'] = "Colore degli occhi";
$LNG['eyes'] = "Occhi";
$LNG['fake_free_review_confirm_mail_01'] = " 
    <p>Gentile %SHOWNAME%,<br /> 
un utente del sito %sys_site_title% ha scritto una recensione su di te sul nostro sito. Per evitare recensione false ti chiediamo di verificare se hai avuto questo cliente e conferma o rigetta l'informazione in questa e???    ?????? ? ?? ????( ?À  ???  ?  ???? ?    ?????   ??????À ??????????????????????  ?????????????  ????W?(  ?(?";
$LNG['fake_free_review_confirm_mail_02'] = " 
    <p>Rispondi per favore: Yes se l'incontro è avvenuto e No se non è avvenuto.</p> 
    <p>Il tuo %sys_site_title% team</p> 
";
$LNG['fake_free_review_confirm_mail_subj'] = "%sys_site_title% conferma per recensione verificata al 100%";
$LNG['fake_photo'] = "Foto false";
$LNG['fax'] = "Fax";
$LNG['female'] = "Donna";
$LNG['ff_guests'] = "<div class='big center'>mancano solo %DAYS% giorni all'estrazione!</div><a href='%LINK%' rel='nofollow'>Passa adesso</a> all'Utenza Premium - e vinci la possibilità di incontrare <a href='%LINK%' rel='nofollow'>la tua escort preferita GRATUITAMENTE!</a> *";
$LNG['ff_guests_today'] = "<div class='big center'>TODAY</div>Stiamo per annunciare il vincitore di questo mese della \"Lotteria FREE FUCK\". Potresti essere tu? Prendi la tua chance e <a href='%LINK%' rel='nofollow'>iscriviti ADESSO</a> al Premium Member per essere sorteggiato! *";
$LNG['ff_nonpaid'] = "<div class='big center'>solo %DAYS% giorni per il sorteggio!</div><a href='%LINK%' rel='nofollow'>Fai un upgrade all'utenza Premium ADESSO</a>  ed aggiudicati la possibilità di vincere un incontro con la tua <a href='%LINK%' rel='nofollow'>escort favorita???    ?????";
$LNG['ff_nonpaid_today'] = "<div class='big center'>OGGI</div>annunceremo il vincitore della Lotteria FREE FUCK! Forse sei TU? Prendi la tua chance e <a href='%LINK%' rel='nofollow'>iscriviti ADESSO</a> al Premium Member per essere sorteggiato! *";
$LNG['ff_paid'] = " <div class='big center'>solo %DAYS% giorni al sorteggio!</div>";
$LNG['ff_paid_today'] = " <div class='big center'>OGGI</div> annunceremo il vincitore del mese della  Lotteria 'Free Fuck'! Forse sei TU?";
$LNG['file_uploaded'] = "Foto caricate con successo.";
$LNG['finish_reg'] = "Completa la registrazione";
$LNG['first_name'] = "Nome";
$LNG['fluent'] = "Fluente";
$LNG['forgot_email_1'] = "Gentile %USER% hai richiesto la tua password tramite il modulo 'Password dimenticata' sul sito %SITE%.";
$LNG['forgot_email_2'] = "I tuoi dati d'accesso sono:";
$LNG['forgot_email_subject'] = "%sys_site_title% Richiesta password";
$LNG['forgot_passwd'] = "Dimenticato password?";
$LNG['forgot_password'] = "Password dimenticata";
$LNG['free'] = "Gratis";
$LNG['free_fuck_desc'] = "Alla fine di ogni mese noi sorteggiamo 1 fortunato vincitore tra gli utenti Premium, chi vincerà potrà avere un incontro con la ragazza che lui sceglierà su %sys_site_title%. Noi pagheremo l'incontro per te!<BR /> <BR /> Congratulazioni a tutti i fortunat???    ?????? ? ?? ????( ?À  ???  ?  ???? ?    ?????   ??????À ???????????????";
$LNG['free_fuck_lotto'] = "Lotteria Free Fuck";
$LNG['free_fuck_note'] = "Nota: questo vincita è solo valida per un incontro di 1 ora massimo o per un massimo di 300 euro, se sceglierai per essempio una escort dove il costo è 250 per 1 ora non potrai usufruire della differenza in nessun modo. Dettagli per la procedura di vincit???    ?????? ? ?? ???";
$LNG['free_mb_desc'] = "Il tuo profilo attualmente è visualizzato in basso in una delle ultime pagine, che significa bassa visibilità.
Acquista un Pacchetto Premium e riceverai:";
$LNG['free_mb_desc_ag'] = "%COUNT% escorts - <a href='/private/upgrade_escort.php'>Upgrade now </a>to let clients know about them !";
$LNG['friday'] = "Venerdi";
$LNG['friendly'] = "Amichevole";
$LNG['from'] = "Da";
$LNG['fuckometer'] = "Fuckometer";
$LNG['fuckometer_desc'] = "Il Fuckometer è un indicatore che viene calcolato in base alle recensioni ed altri parametri complessi. Viene utilizzato per stabilire la migliore escort in azione.";
$LNG['fuckometer_range'] = "Intervallo Fuckometer";
$LNG['fuckometer_rating'] = "Voto Fuckometer";
$LNG['fuckometer_short'] = "FM";
$LNG['fuckometer_toplist'] = "Fuckometer toplist";
$LNG['full_name'] = "Nome completo";
$LNG['gays'] = "Gays";
$LNG['gender'] = "Genere";
$LNG['genuine_photo'] = "100% foto reali";
$LNG['get_trusted_review'] = "Prendi una recensione fidata";
$LNG['get_trusted_review_desc'] = " 
Prendi una recensione fidata! Per favore compila il modulo di sotto!<br /> 
    Come funziona?<a href='%LINK%'>Clicca qui</a>!";
$LNG['girl'] = "Donna";
$LNG['girl_booked'] = "Hai prenotato %NAME% per il %DATE%.";
$LNG['girl_no_longer_listed'] = "Questa ragazza non è più inserita in %SITE%";
$LNG['girl_of'] = "Ragazza di";
$LNG['girl_of_the_month_history'] = "Cronologia ragazza del mese";
$LNG['girl_on_tour_1'] = "Ragazza in tour a %CITY% - %COUNTRY%";
$LNG['girl_on_tour_2'] = "Dal %FROM_DATE% al %TO_DATE%";
$LNG['girl_on_tour_3'] = ": %PHONE%";
$LNG['girl_on_tour_4'] = ": %EMAIL%";
$LNG['girl_origin'] = "Origine della ragazza";
$LNG['girls_in_italy'] = "Le ragazze inserite qui sono adesso in %sys_app_country_title%! Prenotale ADESSO!";
$LNG['girls_international'] = "Ragazze con disponibilità internazionale, per favora nota che se vuoi incontrarle in %sys_app_country_title% devi prenotarle almeno per 12h.";
$LNG['go_to_escort_profile'] = "Vai al tuo profilo escort";
$LNG['go_to_reviews_overview'] = "Visualizza le recensioni";
$LNG['gray'] = "Grigio";
$LNG['green'] = "Verde";
$LNG['hair'] = "Capelli";
$LNG['hair_color'] = "Colore dei capelli";
$LNG['hard_to_believe_its_her'] = "Difficile da credere che sia lei";
$LNG['hard_to_book'] = "Difficle da prenotare";
$LNG['height'] = "Altezza";
$LNG['help_system'] = "Aiuto";
$LNG['help_watch_list'] = "Puoi aggiungere qualsiasi utente, ragazza o agenzia alla Lista dei Favoriti.
Questa lista ti permette di ricevere notifiche via email quando succede un evento speciale.
Se una  <b>escort</b> è nella tua lista, riceverai notifiche sui seguenti eventi:
<ul>???    ?????? ? ?? ????( ??  ???  ?  ???? ?    ?????   ??????? ??????????????????????  ?????????????  ????W?(  ?(??? &  ??????????(  ??À  ?????? ???? ????????????????? ?Item  ??      ?Count  ??????0  ??    0????0????????  ??????   0   ??  ??? ?  ??????  ?W??  ?ò   ???  ??W???W?  ??? ?W?W  ???t ??? ? ? ??????    ??0Eo  ?0  ?????  ????0??i  ??????????0i  ????????????  ??????????????? ?? ??????? ????????????W ?   ?????    ?? ???????    ?? ????t ?? ???    ?  ???????????  ???6? ???/?";
$LNG['hip'] = "Fianchi";
$LNG['home'] = "Home";
$LNG['home_page'] = "Home page";
$LNG['hour'] = "ora";
$LNG['hours'] = "ore";
$LNG['howto_set_main'] = "Per settare la foto come principale basta cliccare sopra la stessa";
$LNG['i_agree_with_terms'] = "Ho letto ed accetto le regole generali <a target=\"_blank\" id='terms-link' href='%LINK%'>termini e condizioni</a>";
$LNG['in'] = "In";
$LNG['in_face'] = "In viso";
$LNG['in_mouth_spit'] = "In bocca con sputo";
$LNG['in_mouth_swallow'] = "In bocca con ingoio";
$LNG['incall'] = "Ricevo";
$LNG['independent'] = "Indipendente";
$LNG['independent_escorts_from'] = "Escort indipendenti da";
$LNG['independent_girls'] = "Escorts indipendenti";
$LNG['info'] = "Info";
$LNG['intelligent'] = "Intelligente";
$LNG['international'] = "Internazionale";
$LNG['international_directory'] = "Lista internazionale";
$LNG['invalid_birth_date'] = "Data di nascita non valida";
$LNG['invalid_curr_password'] = "Password corrente errata";
$LNG['invalid_desired_date'] = "Data desiderata non valida";
$LNG['invalid_discount_code'] = "Codice sconto non valido";
$LNG['invalid_email'] = "E-mail non valida";
$LNG['invalid_email_info'] = "Il nostro sistema ha indentificato che la sua mail non è valida.Non è possibile inviarle una email. Il suo account è temporaneamente bloccato. Per favore ci fornisca una email valida, le invieremo una email di verifica con un link per confermare. Clicchi ???    ?????? ? ?? ????( ?À  ???  ?  ???? ?    ?????   ??????À ?????????????????";
$LNG['invalid_file_type'] = "File non valido. Puoi caricare solo file .jpeg";
$LNG['invalid_meeting_date'] = "Data dell'incontro non valida";
$LNG['invalid_showname'] = "Showname non valido, per favore utilizzi solo caratteri alfanumerici.";
$LNG['invalid_user_type'] = "User non valido";
$LNG['invitation'] = "Benvenuti su Escort Service! Per favore seleziona la città dove vuoi trovare una escort.";
$LNG['invitation_1'] = "Nella pagina successiva troverai le migliori escorts, club privati ed agenzie escort nella tua zona %sys_app_country_title% e quelle Internazionali. Puoi vedere i contatti, le foto ed i dettagli per ogni singola ragazza.";
$LNG['invitation_2'] = "Speriamo che possiate essere soddisfatte del nostro sito. Per domande o problemi non esitate a conattarci: <a href='mailto:%sys_contact_email%'>%sys_contact_email%</a>";
$LNG['joined'] = "Accesso avvenuto";
$LNG['kiss'] = "Bacio";
$LNG['kiss_with_tongue'] = "Bacio con lingua";
$LNG['kissing'] = "Baciare";
$LNG['kissing_with_tongue'] = "Bacio con lingua";
$LNG['language'] = "Lingua";
$LNG['language_problems'] = "Problemi linguistici";
$LNG['languages'] = "Lingue";
$LNG['languages_changed'] = "Lingue aggiornate con successo";
$LNG['last'] = "Ultimo";
$LNG['last_name'] = "Cognome";
$LNG['latest_10_reviews'] = "Le ultime 10 recensioni delle ragazze";
$LNG['latest_10_third_party_reviews'] = "Le ultime 10 recensioni di ragazze di terze parti";
$LNG['level'] = "Livello";
$LNG['limitrofi'] = "Limitrofi";
$LNG['links'] = "Links";
$LNG['loano'] = "Loano";
$LNG['locations_changed'] = "Posizione aggiornata con successo";
$LNG['logged_in'] = "Sei loggato/a come";
$LNG['login'] = "Login";
$LNG['enter'] = "Login";
$LNG['login_and_password_alphanum'] = "Login e password possono contenere solamente caratteri alfanumerici (0-9, a-z, A-Z, _).";
$LNG['login_and_signup_disabled'] = "In questo momento stiamo facendo della manutenzione al sito. Non è possibile accettare nuove registrazioni. Per favore tornate più tardi. Ci scusiamo per l'inconveniente.";
$LNG['login_exists'] = "Login già presente. Per favore scegli altro login…";
$LNG['login_problems_contact'] = "Se il problema persiste non esistate a <a href='#' id='feedback'>contattarci</a>";
$LNG['logout'] = "Esci";
$LNG['looking_for_girls_in_your_city'] = "Stai cercando ragazze nella tua città?";
$LNG['looking_for_girls_in_your_region'] = "Stai cercando ragazze nella tua regione?";
$LNG['looks'] = "Aspetto";
$LNG['looks_and_services'] = "Aspetto e Servizi";
$LNG['looks_range'] = "Intervallo Aspetto";
$LNG['looks_rating'] = "Voto aspetto";
$LNG['lower_date'] = "La data \"fino a\" non può essere antecedente alla data \"da\"";
$LNG['lower_fuckometer'] = "Intervallo Fuckometer errato.";
$LNG['lower_looks_rating'] = "Intervallo Aspetto errato.";
$LNG['lower_services_rating'] = "Voto servizi non corretto.";
$LNG['male'] = "Uomo";
$LNG['meeting_costs'] = "Costo dell'incontro";
$LNG['meeting_date'] = "Data dell'incontro";
$LNG['meeting_date_range'] = "Intervallo data d'incontro";
$LNG['meeting_dress_comments'] = "Posto dell'incontro, abbigliamento richiesto, commenti, ";
$LNG['meeting_length'] = "Durata dell'incontro";
$LNG['member'] = "Utente";
$LNG['member_name'] = "Nome utente";
$LNG['member_profit_01'] = "Diventa utente del forum per commentare e condividere le tue esperienze";
$LNG['member_profit_02'] = "scrivi recensioni sulle ragazze";
$LNG['member_profit_03'] = "prendi le informazioni circa le nuove ragazze su %SITE%";
$LNG['member_profits'] = "Come utente puoi potrai";
$LNG['membership_change_subject'] = "Utenza cambiata sul sito %sys_site_title%";
$LNG['membership_change_to_free_mail'] = " 
    <p>Gentile %LOGIN%,<br /> 
    la Sua sottoscrizione a *Utenza Premium* di %sys_site_title% è scaduta e la sua utenza ritorna ad essere una utenza normale gratuita.</p> 
    <p>Se volesse estendere la sua utenza premium, per favore la faccia sul sit???    ?????? ? ?? ????( ??  ???  ?  ???? ?    ?????   ??????? ??????????????????????  ?????????????  ????W?(  ?(??? &  ??????????(  ??À  ?????? ???? ????????????????? ?Item  ??      ?Count  ??????0  ??    0????0????????  ??????   0   ??  ??? ?  ??????  ?W??  ?ò   ???  ??W???W?  ??? ?W?W  ???t ??? ? ? ??????    ??0Eo  ?0  ?????  ????0??i  ??????????0i  ????????????  ??????????????? ?? ??????? ????????????W ?   ?????    ?? ???????    ?? ????t ?? ???    ?  ???????????  ???6? ???/?????????   ??j  ?  ????????????????? ??	 ???????????? ?? ??????? ????????????t ?   ?????    ??????f??t??????????????/???????? ?/  ??#             ???????Eq ???Q?";
$LNG['membership_change_to_premium_mail'] = " 
    <p>Gentile %LOGIN%,<br /> 
   l'iscrizione * Utente Premium* di %sys_site_title% è avvenuto con successo. Grazie per la tua fiducia.</p> 
    <p>I tuoi dati d'accesso sono i seguenti:<br /> 
    Login: %LOGIN%</p> 
    <p>(Tieni queste informazioni ???    ?????? ? ?? ????( ?À  ???  ?  ???? ?    ?????   ??????À ??????????????????????  ?????????????  ????W?(  ?(??? &  ??????????(  ??À  ?????? ???? ????????????????? ?Item  ??      ?Count  ??????0  ??    0????0????????  ??????   0   ??  ??? ?  ??????  ?W??  ?ò   ???  ??W???W?  ??? ?W?W  ???t ??? ? ? ??????    ??0Eo  ?0  ?????  ????0??i  ??????????0i  ????????????  ??????????????? ?? ??????? ????????????W ?   ?????    ?? ???????    ?? ????t ?? ???    ?  ???????????  ???6? ???/?????????   ??j  ?  ????????????????? ??	 ???????????? ?? ??????? ????????????t ?   ?????    ??????f??t??????????????/???????? ?/";
$LNG['membership_type'] = "Tipologia utenza";
$LNG['membership_types'] = "Tipologie di utenze";
$LNG['men'] = "Uomini";
$LNG['menu'] = "Menù";
$LNG['message'] = "Messaggio";
$LNG['message_for'] = "Messaggio per %NAME%";
$LNG['message_not_sent'] = "Invio messaggio fallito";
$LNG['message_sent'] = "Il tuo messaggio è stato inviato a %NAME%.";
$LNG['metric'] = "Sistema metrico";
$LNG['metric_desc'] = "centimetri, kilogrammi, …";
$LNG['middle_name'] = "Secondo nome";
$LNG['minutes'] = "minuti";
$LNG['modified'] = "Modificato";
$LNG['modify_escort'] = "Modifica escort";
$LNG['monday'] = "Lunedì";
$LNG['month'] = "Mese";
$LNG['month_1'] = "Gennaio";
$LNG['month_10'] = "Ottobre";
$LNG['month_11'] = "Novembre";
$LNG['month_12'] = "Dicembre";
$LNG['month_2'] = "Febbraio";
$LNG['month_3'] = "Marzo";
$LNG['month_4'] = "Aprile";
$LNG['month_5'] = "Maggio";
$LNG['month_6'] = "Giugno";
$LNG['month_7'] = "Luglio";
$LNG['month_8'] = "Agosto";
$LNG['month_9'] = "Settembre";
$LNG['months'] = "Mesi";
$LNG['more_info'] = "Più informazioni…";
$LNG['more_new_girls'] = "Altre nuove ragazze";
$LNG['more_news'] = "Altre novità...";
$LNG['multiple_times_sex'] = "Rapporti per più volte";
$LNG['my_escort_profile'] = "Il mio profilo escort";
$LNG['my_escorts'] = "Le mie escorts";
$LNG['my_languages'] = "Le mie lingue";
$LNG['my_locations'] = "Città di lavoro";
$LNG['my_new_review_notify_agency'] = "Informatemi via mail quando una mia escort riceve una nuova recensione";
$LNG['my_new_review_notify_single'] = "Informami via mail quando ricevo una recensione";
$LNG['my_photos'] = "Le mie foto";
$LNG['my_rates'] = "Le mie tariffe";
$LNG['my_real_profile'] = "I miei dati reali";
$LNG['my_reviews'] = "Le mie recensioni";
$LNG['name'] = "Nome";
$LNG['name_of_the_lady'] = "Nome della escort";
$LNG['name_of_the_site'] = "Nome del sito";
$LNG['ethnic'] = "Etnia";
$LNG['nationality'] = "Nazionalità";
$LNG['need_help'] = "Bisogno di aiuto?";
$LNG['need_to_be_registered_to_review'] = "Devi essere registrato per %SITE% recensire una ragazza, <a href='%LINK%'>iscriviti ADESSO e GRATIS!</a>";
$LNG['never'] = "mai";
$LNG['new'] = "Nuovo";
$LNG['new_arrivals'] = "Nuove escorts";
$LNG['new_email'] = "Nuova mail";
$LNG['new_entries'] = "Nuove entrate";
$LNG['new_escort_email_by_agency_body'] = "<pre>L'genzia \"%AGENCY%\" ha aggiunto una nuova escort: %ESCORT% ! 
Riceve questa notifica perchè questa agenzia è inserita tra le tue preferite. 
Clicca questo link <a href=\"%LINK%\">%LINK%</a> per vedere il profilo della escort. 
Il suo %sys_site_title% T???    ???";
$LNG['new_escort_email_by_agency_subject'] = "%AGENCY% ha aggiunto una nuova escort !";
$LNG['new_password'] = "Nuova password";
$LNG['new_password_empty_err'] = "Nuova password non può essere vuota";
$LNG['new_review_email_agency_body'] = "<pre>L'utente %USER% ha scritto una nuova recensione su una sua escort %ESCORT% ! 
Clicca in questo link <a href=\"%LINK%\">%LINK%</a> per leggere la recensione. 
Il suo %sys_site_title% Team 
</pre>";
$LNG['new_review_email_agency_subject'] = "La sua escort ha una nuova recensione";
$LNG['new_review_email_by_escort_body'] = "<pre>Escort %ESCORT% ha ricevuto una nuova recensione da %USER% ! 
Hai ricevuto questa notifica perchè hai inserito questa escort nella lsita delle favorite. 
Clicca questo link
<a href=\"%LINK%\">%LINK%</a> 
per leggere la recensione. 
Il suo %sys_site_tit???    ??????";
$LNG['new_review_email_by_escort_subject'] = "%ESCORT% ha ricevuto una nuova recensione!";
$LNG['new_review_email_by_user_body'] = "<pre>Utente %USER% ha lasciato una nuova recensione su %ESCORT% ! 
Hai ricevuto questa notifica perché hai messo questo utente nella tua lista dei preferiti. 
Clicca su questo link
<a href=\"%LINK%\">%LINK%</a> 
per leggere la recensione. 
Il Tuo %sys_site_???    ?????? ?";
$LNG['new_review_email_by_user_subject'] = "%USER% ha scritto una nuova recensione!";
$LNG['new_review_email_escort_body'] = "<pre>Utente %USER% ha scritto una nuova recensione su di te! 
Clicca su questo link
<a href=\"%LINK%\">%LINK%</a> 
per leggere la recensione. 
Il Tuo %sys_site_title% Team 
</pre>";
$LNG['new_review_email_escort_subject'] = "Hai una nuova recensione!";
$LNG['news'] = "Notizie";
$LNG['newsletter'] = "Newsletter";
$LNG['next'] = "Prossimo";
$LNG['next_step'] = "Avanti";
$LNG['no'] = "No";
$LNG['no_blowjob'] = "No orale";
$LNG['no_cumshot'] = "No venuta";
$LNG['no_esc_sel_err'] = "Devi selezionare una escort.";
$LNG['no_escort_languages'] = "Non hai selezionata nessuna lingua. <a href='%LINK%'>Clicca qui</a> per selezionarla!";
$LNG['no_escort_main_pic'] = "Non hai selezionata nessuna foto principale. <a href='%LINK%'>Clicca qui</a> per selezionarla!";
$LNG['no_escort_name_languages'] = "Escort %ESCORT% non ha nessuna lingua selezionata. <a href='%LINK%'>Clicca qui</a> per selezionarla!";
$LNG['no_escort_name_main_pic'] = "Escort %ESCORT% non ha nessuna foto principale selezionata . <a href='%LINK%'>Clicca qui</a> per selezionarla!";
$LNG['no_escort_name_photos'] = "Escort %ESCORT% non ha nessuna foto caricata nel suo profilo. <a href='%LINK%'>Clicca qui</a> per aggiornare la tua galleria.";
$LNG['no_escort_photos'] = "Non hai nessuna foto caricata nel tuo profilo. <a href='%LINK%'>Clicca qui</a> per aggiornare la tua galleria.";
$LNG['no_escort_profile'] = "Non hai nessun profilo escorto attivo. <a href='%LINK%'>Clicca qui</a> per aggiungerne uno.";
$LNG['no_escorts_found'] = "Nessuna escort trovata.";
$LNG['no_escorts_profile'] = "Non hai nessun profilo escort definito. <a href='%LINK%'>Clicca qui per </a> aggiungerlo";
$LNG['no_kiss'] = "No bacio";
$LNG['no_kissing'] = "No bacio";
$LNG['no_only_once'] = "No, solo una volta";
$LNG['no_premium_escorts'] = "No premium escorts.";
$LNG['no_reviews_found'] = "Recensione non trovata";
$LNG['no_sex_avail_chosen'] = "Devi selezionare almeno un servizio.";
$LNG['no_users_found'] = "Nessun utente trovato.";
$LNG['no_votes'] = "Nessun voto";
$LNG['norm_memb'] = "1 umese di utenza \"normale\" per 50 EUR";
$LNG['normal'] = "Normale";
$LNG['normal_mb_desc_ag'] = "%COUNT% escorts (Prima scadenza fra %DAYS% giorni)";
$LNG['normal_review'] = "normale";
$LNG['normal_review_desc'] = "Recensione normale significa che l'agenzia/ragazza non ha risposto al sms/email quindi la recensione non è confermata ma non è nemmeno rigettata! Questo significa che la recensione può essere comunque affidabile.";
$LNG['not_approved'] = "In attesa di approvazione";
$LNG['not_available'] = "Non disponibile";
$LNG['not_logged'] = "Non sei loggato/a";
$LNG['not_logged_desc'] = "Devi accedere al sito per poter utilizzare questa funzione";
$LNG['not_verified_msg'] = " 
<p>Il suo account non è stato ancora verificato.<br /> 
Per avere accesso come utente su %sys_site_title% dobbiamo verificare la tua email. Se non hai ricevuto la mail per favore <a href=\"%LINK%\">clicca qui per inviare di nuovo la mail di attivazione</a???    ?????? ? ?? ??1?( 1?  ???  ?  ???? ?    ?????   ?????1? ??????????????????????  ?????????????  ????W?(  ?(??? &  ??????????(  ??À  ?????? ???? ?????";
$LNG['not_yet_member'] = "Non sei ancora nostro utente? Iscriviti gratuitamente!";
$LNG['not_yet_premium_signup_now'] = "Non sei ancora un Utente Premium?  <a href='%LINK%'>Iscriviti ADESSO</a> e risparmia!";
$LNG['note'] = "Note";
$LNG['notify_comon'] = "<pre>Hai ricevuto questa email perché sei registrato su %sys_site_title% e hai scelto di ricevere queste notifiche. Per cambiare le tue impostazioni, accedi al sito e clicca su \"Impostazioni\" in the menu.</pre>";
$LNG['now'] = "Adesso";
$LNG['number'] = "Nr.";
$LNG['number_of_reviews'] = "Numero delle recensioni";
$LNG['offers_discounts'] = "Offre sconto";
$LNG['on_tour_in'] = "in tour a";
$LNG['only'] = "solo";
$LNG['only_fake_free_reviews'] = "Solo recensioni verificate al 100%";
$LNG['only_for_premium_members'] = "solo per Utenti Premium";
$LNG['only_girls_with_reviews'] = "Solo ragazze con recensioni";
$LNG['only_reviews_from_user'] = "Solo recensioni scritte da utente";
$LNG['only_reviews_not_older_then'] = "Solo recensioni più recenti di";
$LNG['optional'] = "optional";
$LNG['oral_without_condom'] = "Orale con preservativo";
$LNG['order_premium_spot_now'] = "Ordina il tuo premium spot adesso!";
$LNG['origin'] = "Origine";
$LNG['origin_escortforum'] = "escortforum";
$LNG['origin_non_escortforum'] = "non escortforum";
$LNG['other'] = "Altro";
$LNG['other_boys_from'] = "Altri escorts da";
$LNG['other_escorts_from'] = "Altre escorts da";
$LNG['boys_from'] = "Escorts da";
$LNG['escorts_from'] = "Escorts da";
$LNG['outcall'] = "Outcall";
$LNG['overall_rating'] = "Voto medio";
$LNG['partners'] = "Partners";
$LNG['passive'] = "Passiva";
$LNG['passwd_has_been_sent'] = "La tua password è stata inviata alla mail %EMAIL%";
$LNG['passwd_succesfuly_sent'] = "La tua password è stata inviata al tuo indirizzo email con successo.";
$LNG['password'] = "Password";
$LNG['password_changed'] = "Password cambiata con successo";
$LNG['password_invalid'] = "Password non valida. Deve contenere minimo 6 caratteri.";
$LNG['password_missmatch'] = "Password non corrisponde";
$LNG['percent_discount'] = "%DISCOUNT%% sconto";
$LNG['phone'] = "Telefono";
$LNG['phone_instructions'] = "Istruzioni telefono";
$LNG['photo_correct'] = "Telefono corretto";
$LNG['photos'] = "Le foto";
$LNG['pic_delete_failed'] = "Fallito cancellazione immagine";
$LNG['pic_deleted'] = "Immagine cancellata";
$LNG['place'] = "Posto";
$LNG['please_choose_services'] = "Per favore seleziona i servizi ricevuti dalla ragazza, questo influenza il suo Fuckometer!";
$LNG['please_fill_form'] = "Compila per favore il seguente modulo";
$LNG['please_provide_email'] = "Per favore inserisci l'indirizzo email che hai utilizzato per registrarti su %SITE%";
$LNG['please_select'] = "Per favore seleziona…";
$LNG['plus2'] = "2+";
$LNG['poshy'] = "Elegante";
$LNG['posted_reviews'] = "Hai scritto %NUMBER% recensioni.";
$LNG['posted_to_forum'] = "Hai scritto %NUMBER% post nel forum.";
$LNG['premium'] = "Premium";
$LNG['premium_escorts'] = "Premium escorts";
$LNG['premium_mb_desc_ag'] = "%COUNT% escorts (Prima scadenza tra %DAYS% giorni)";
$LNG['premium_user'] = "Premium User";
$LNG['previous'] = "Precedente";
$LNG['price'] = "Prezzo";
$LNG['priv_apart'] = "Appartamento privato";
$LNG['private_menu'] = "Menù privato";
$LNG['problem_report'] = "Riporta un problema";
$LNG['problem_report_back'] = "Clicca qui per tornare al profilo escort";
$LNG['problem_report_no_accept'] = "Grazie, la tua segnalazione è stata accettata.";
$LNG['problem_report_no_name'] = "Insersci il tuo nome";
$LNG['problem_report_no_report'] = "Descrivil il problema";
$LNG['prof_city_expl'] = "La città d'origine della ragazza non è necessariamente la sua città di lavoro. Le città di lavoro possono essere definite più avanti.";
$LNG['prof_country_expl'] = "Il paese d'origine della ragazza non è necessariamente il suo paese di lavoro. Il paese di lavoro può essere definito più avanti.";
$LNG['profile'] = "Profilo";
$LNG['profile_changed'] = "Profilo aggiornato con successo";
$LNG['promote_this_girl'] = "Promuovi questa ragazza";
$LNG['promote_yourself'] = "Promuoviti";
$LNG['provided_services'] = "Servizi forniti";
$LNG['publish_date'] = "Data di pubblicazione";
$LNG['rank'] = "Posizione classifica";
$LNG['rate'] = "Voto";
$LNG['rate_looks'] = "Bellezza";
$LNG['rate_services'] = "Servizi";
$LNG['rates'] = "Voti";
$LNG['rates_changed'] = "Voti aggiornati con successo.";
$LNG['rates_deleted'] = "Voti cancellati con successo.";
$LNG['rating'] = "Votazione";
$LNG['re_verify_sent'] = "Ti abbiamo inviato la email di attivazione. Per favore controlla la tua casella postale e segui le istruzioni della email.";
$LNG['read_this_review'] = "Leggi questa recensione";
$LNG['read_whole_article'] = "Leggi l'intero articolo";
$LNG['real'] = "Reale";
$LNG['real_data_form_text'] = "Per favore inserisci i tuoi dati reali. Questi dati non faranno parte del tuo profilo, non saranno visualizzati in nessuna pagina e verranno trattati in modo confidenziale, privato e sicuro.";
$LNG['real_name'] = "Nome reale";
$LNG['recalibrate_now'] = "Ricalibrare adesso";
$LNG['receive_newsletter'] = "Ricevere %sys_site_title% newsletter";
$LNG['red'] = "Rosso";
$LNG['reference'] = "Dove hai sentito di noi?";
$LNG['regards'] = "Saluti, %SITE% team.";
$LNG['region'] = "Regione";
$LNG['register'] = "Registrati";
$LNG['registration'] = "Registrazione ";
$LNG['rem_watch'] = "Rimuovi dalla Lista dei Favoriti";
$LNG['rem_watch_success'] = "<b>%NAME%</b> è stato/a remosso/a dalla tua Lista dei Favoriti con successo. <a href='javascript:history.back()'>Clicca qui per tornare indietro.</a>";
$LNG['remove'] = "Rimuovi";
$LNG['renew_now'] = "Rinnova adesso!";
$LNG['required_fields'] = "Campi obbligatori segnalati con*";
$LNG['rev_resume_title'] = "Ultime recensioni su questa escort";
$LNG['review'] = "Recensione";
$LNG['review_changed'] = "Recensione aggiornata con successo";
$LNG['review_guide_and_rules'] = " 
    <strong>Guida e regole:</strong><br /> 
    Ti chiediamo gentilmente di rispettare alcune regole e non inserire recensioni false! Ogni recensione aiuta gli utenti a ricevere servizi migliori! 
    <ul> 
        <li>per favore specifica tutti i servi???    ?????? ? ?? ????( ?À  ???  ?  ???? ?    ?????   ??????À ??????????????????????  ?????????????  ????W?(  ?(??? &  ??????????(  ??À  ?????? ???? ????????????????? ?Item  ??      ?Count  ??????0  ??    0????0????????  ??????   0   ??  ??? ?  ??????  ?W??  ?ò   ???  ??W???W?  ??? ?W?W  ???t ??? ? ? ??????    ??0Eo  ?0  ?????  ????0??i  ??????????0i  ????????????  ??????????????? ?? ??????? ????????????W ?   ?????    ?? ???????    ?? ????t ??";
$LNG['review_language'] = "Lingua della recensione";
$LNG['review_sms'] = "Sei stata recensita su %sys_site_title%! Semplicemente rispondi via sms: YES per vero , e NO, per falso! Nella seconda sms riceverai i dati del cliente.";
$LNG['review_status'] = "Stato della recensione";
$LNG['review_this_girl'] = "Recensisci questa ragazza";
$LNG['reviews'] = "Recensioni";
$LNG['reviews_to_recalibrate'] = " 
    Gentile %NAME%,<br /> 
Hai  %REVIEWS% recensioni da migliorare. Per favore aiutaci a migliorare le recensioni dando informazioni più dettagliate sulla ragazza che hai incontrato. ";
$LNG['rouen'] = "Rouen";
$LNG['royal'] = "Royal system";
$LNG['royal_desc'] = "inches, feet, ...";
$LNG['rss_channel_desc'] = "Ultime arrivate";
$LNG['salon'] = "Night Club";
$LNG['sardegna'] = "Sardegna";
$LNG['saturday'] = "Domenica";
$LNG['sauna'] = "Sauna";
$LNG['save_changes'] = "Modifiche salvate";
$LNG['score'] = "Punteggio";
$LNG['search'] = "Cerca";
$LNG['search_desc'] = "Benvenuto nella ricerca semplice di %sys_site_title%. Per altre opzioni di ricerca clicca su <a href='%LINK%'>ricerca avanzata</a>.";
$LNG['search_desc2'] = "Benvenuto nella ricerca semplice di %sys_site_title%. Per una ricerca più semplice clicca su <a href='%LINK%'>ricerca semplice</a>.";
$LNG['search_escorts'] = "Cerca escorts";
$LNG['search_girl'] = "Cerca ragazza";
$LNG['search_agency'] = "Cerca agenzia";
$LNG['search_name'] = "Nome ragazza/Agenzia";
$LNG['search_reviews'] = "Cerca recensioni";
$LNG['search_reviews_back'] = "Torna al modulo di ricerca";
$LNG['search_reviews_no_params'] = "Per favore, inserisci almeno un criterio di ricerca";
$LNG['search_reviews_no_result'] = "Nessun risultato, per favore riprova";
$LNG['search_users'] = "Cerca utente";
$LNG['see_escorts_soon_on_tour'] = "Clicca qui per vedere tutte le ragazze che arriveranno a %sys_app_country_title%";
$LNG['see_independent_preview'] = "Vedi l'anteprima di tutte le indipendenti";
$LNG['select_as_main'] = "Vuoi impostare la foto selezionata come immagine principale?";
$LNG['select_your_service_requirements'] = "Seleziona i servizi richiesti";
$LNG['send_message'] = "Invia messagio";
$LNG['service_comments'] = "Commenti sul servizio";
$LNG['services'] = "Servizi";
$LNG['services_range'] = "Intervallo servizi";
$LNG['services_rating'] = "Voto servizi non corretto.";
$LNG['services_she_was_willing'] = "Servizi che lei offre";
$LNG['settings'] = "Impostazioni";
$LNG['settings_update_error'] = "Errore durante l'aggiornamento delle impostazioni";
$LNG['settings_updated'] = "Impostazioni aggiornate con successo.";
$LNG['sex'] = "Sesso";
$LNG['sex_available_to'] = "Disponibile per ";
$LNG['shoe_size'] = "Misura scarpe";
$LNG['show_all_reviews'] = "Mostra tutte le recensioni";
$LNG['show_email'] = "Mostrare email sul sito?";
$LNG['showname'] = "Nome da visualizzare";
$LNG['show_name'] = "Nome da visualizzare";
$LNG['show_only_girls_on_tour_in'] = "Visualizza solo ragazze in tour a";
$LNG['show_site_reviews'] = "Mostra recensioni %SITE%";
$LNG['show_third_party_reviews'] = "Mostra recensioni di ragazze di terze parti";
$LNG['showname_exists'] = "Nome da visualizzare già esistente, per favore scegli un altro nome!";
$LNG['sign_up'] = "Iscriviti";
$LNG['sign_up_for_free'] = "Iscriviti adesso gratis!";
$LNG['signup_activated_01'] = "Il suo account è stato attivato con successo!";
$LNG['signup_activated_02'] = " 
        Complimenti!<br /> 
       Sei diventato un utente del sito %SITE% <br /> 
       I tuoi dati d'accesso sono stati inviati al tuo indirizzo email.";
$LNG['signup_activated_03'] = "Clicca qui per accedere.";
$LNG['signup_and_win'] = "Iscriviti come Utente Premium e vinci un incontro da 300 Euro!";
$LNG['signup_confirmation_email'] = "<pre> 
Gentile %USER%, 
grazie per la registrazione su %SITE%. 
Ora sei un utente del sito %SITE%. 
Qui puoi trovare i tuoi dati d'accesso. Tieni queste informazioni in un posto sicuro: 
Login: %USER% 
Sito: %SITE% 
Divertiti sul nostro sito!
Non vediamo ???    ?????? ? ?? ????( ??  ???  ?  ???? ?    ?????  ";
$LNG['signup_confirmation_email_subject'] = "Registrazione su";
$LNG['signup_error_email_missing'] = "Errore: L'indirizzo email non è valido, per favore inserisci un indirizzo email valido.";
$LNG['signup_error_email_used'] = "Errore: Questo indirizzo email è già stato registrato su %SITE%. Per favore usa un altro indirizzo email.";
$LNG['signup_error_login_missing'] = "Errore: Non hai inserito nessun login, per favore aggiungi un login.";
$LNG['signup_error_login_used'] = "Errore: Login già esistente, per favore scegli un altro login.";
$LNG['signup_error_password_invalid'] = "Errore: Password errata. La password deve contenere almeno 6 caratteri.";
$LNG['signup_error_password_missing'] = "Errore: Non hai inserito nessuna password, per favore aggiungi una password.";
$LNG['signup_error_password_wrong'] = "Errore:  La password è la verifica password non sono identici. La password deve essere identica su entrambi i campi.";
$LNG['signup_error_terms_missing'] = "Devi approvare il regolamento del sito per poter diventare un utente del sito.";
$LNG['signup_successful_01'] = "La registrazione su %SITE% è avvenuta con successo.";
$LNG['signup_successful_02'] = "Grazie per essersi registrato/a!";
$LNG['signup_successful_03'] = "Per completare la sua registrazione per favore controlli la sua email.";
$LNG['signup_successful_04'] = "ATTENTIONE:<br/> 
<small>Se non ricevi la nostra email, per favora controlla anche la cartella spam. In alcuni casi ci vogliono ore prima che la email arrivi</small>";
$LNG['signup_verification_email'] = "<pre> 
Gentile %USER%, 
grazie per esserti registrato su %SITE%. 
Per diventare un nostro utente devi attivare il tuo account, cliccando il seguente link:
<a href=\"%LINK%\">%LINK%</a> 
Sito: %SITE% 
Il Tuo %SITE% Team.</pre>";
$LNG['signup_verification_email_subject'] = "Registrazione su %SITE%";
$LNG['silicon'] = "Silicone";
$LNG['single_girl'] = "Ragazza singola";
$LNG['sitemap'] = "Mappa del sito";
$LNG['smoker'] = "Fumatore/rice";
$LNG['somewhere_else'] = "Altrove";
$LNG['soon'] = "Presto";
$LNG['state'] = "Stato della recensione";
$LNG['step_1'] = "1. passo";
$LNG['step_2'] = "2. passo";
$LNG['submit'] = "Invio";
$LNG['subscribe'] = "Iscriviti";
$LNG['successfuly_logged_in'] = "Hai accesso al sito con successo.";
$LNG['sucks'] = "Succhiare";
$LNG['sunday'] = "Domenica";
$LNG['swallow'] = "Con ingoio";
$LNG['system_error'] = "Errore di sistema, per favore contatta l'assistenza clienti.";
$LNG['terms_and_conditions'] = "Regolamento";
$LNG['terms_text'] = " 
    <h1>Condizioni generali di contratto (CGC)</h1> 
    <h2>&sect; 1 Rapporto d’affari</h2> 
    <p>Le CGC regolano i rapporti tra gli utenti (di seguito „utenti“), i clienti (di seguito „clienti“) e la DA Products GmbH (di seguito „DA“), il gestore del sito Internet „%sys_site_title%“ (di seguito „sito web“) e valgono per tutti i servizi e i prodotti offerti nel sito web, a seconda dei prodotti e dei servizi di seguito vengono indicate ulteriori condizioni contrattuali.</p> 
    <h2>&sect; 2 Direttive generali d’uso</h2> 
    <p>Il sito web è destinato a utenti che abbiano più di 18 anni.</p> 
    <p>L’età e altre informazioni personali contenute nel profilo dell’utente o cliente, in annunci, chat, forum o recensioni ecc. sono controllati dalla DA solo a campione; questo controllo non ha validità giuridica e non è vincolante. Ogni utente ha la responsabilità di controllare che i suoi partner abbiano compiuto la maggiore età in base alla legislazione in vigore nel proprio paese.</p> 
    <p>La DA non è responsabile per l’utilizzo delle informazioni personali rese accessibili nel sito web.</p> 
    <h2>&sect; 3 Dati utente / cliente, link di terzi, cookies</h2> 
    <p>All’atto dell’utilizzo e in particolare all’atto della registrazione e inserzione nel sito web la DA provvede a mettere in memoria alcune informazioni. Il cliente/utente può controllare e modificare i propri dati contenuti nel profilo e nelle inserzioni ogni qualvolta lo desideri. La DA inoltre mette in memoria in forma anonima i dati utente e cliente per i contatti, per la sicurezza tecnica del sistema, per motivi di statistica, di innovazione e per il miglioramento del prodotto. I dati non vengono messi a disposizione di terzi se non per la lotta agli abusi e in caso di contenuti vietati per legge. Con la registrazione e il conseguente utilizzo del sito web l’utente/cliente da il proprio <strong>consenso</strong> affinché i dati messi a disposizione possano essere <strong>elaborati e memorizzati</strong> per uso interno come consentito dalla legge sulla privacy. In caso di partecipazione a concorsi si provvederà ad indicare caso per caso se i dati vengono trasmessi a terzi per permettere lo svolgimento del concorso.</p> 
    <p>I contenuti di dati trasmessi dall’utente/cliente via Internet come e-mail („messaggi privati“), SMS o simili, non vengono sottoposti al controllo della DA. La DA non è responsabile per il contenuti di tali dati. La DA non utilizza indirizzi mail consultabili per l’invio di spam (le newsletter vengono spedite esclusivamente a utenti/clienti che richiedono espressamente questo servizio v. “Settings”) e non vende indirizzi mail a chi spedisce spam. Non è da escludersi che gli indirizzi mail consultabili pubblicamente vengano utilizzati in modo improprio da terzi, la DA tuttavia non si assume alcuna responsabilità in merito.</p> 
    <p>Il sito web contiene dei <strong>link</strong> per offerte di terzi. La DA non ha alcuna influenza sul trattamento che gli offerenti terzi fanno dei dati degli utenti raccolti sui propri siti web, l’utilizzo di questi siti è a rischio proprio dell’utente/cliente, la Da non si assume alcuna responsabilità per un utilizzo improprio o la diffusione dei dati.</p> 
    <p>Per poter adattare il sito web nel miglior modo possibile alle esigenze degli utenti/clienti, in certi casi vengono inseriti dei <strong>cookies</strong> (piccoli file memorizzati nel computer dell’utente/cliente per facilitare il ritrovamento del sito e il suo aggiornamento). I cookies non contengono dati personali. Configurando il browser in un certo modo è possibile rinunciare all’installazione dei cookies. Rinunciando ai cookies si può tuttavia provocare un peggioramento o l’impossibilità di accedere ai servizi e alle funzioni offerti dal sito web; la DA non si assume alcuna responsabilità in merito.</p> 
    <h2>&sect; 4 Forum e recensioni</h2> 
    <p>Con la registrazione al sito web e la partecipazione a forum e recensioni l’utente dichiara che i diritti d’autore sono suoi e che alla DA vengono conferiti i <strong>diritti d’uso</strong> per gli articoli e le recensioni. Non è possibile garantire la riservatezza dei forum e la pubblicazione degli articoli spediti. La DA non si assume alcuna responsabilità per la correttezza dei contenuti degli articoli e non tiene alcuna corrispondenza in merito.</p> 
    <p>Il contenuto della registrazione e degli articoli in forum e recensioni non deve contravvenire alle leggi svizzere né a quelle dei paesi dove articoli/recensioni vengono immessi nella rete. Contenuti con riferimenti a pornografia infantile, sesso con animali o escrementi e tutti i dati memorizzati dei relativi utenti e clienti vengono trasmessi subito e senza preavviso alle autorità penali competenti. L’utente assicura espressamente che i suoi articoli/recensioni non contravvengono a diritti d’autore, alla legislazione sulla tutela dei dati o al diritto della concorrenza e si impegna a mantenere la DA <strong>libera e indenne</strong> da rivendicazioni da parte di terzi che si basano su atti illegali dell’utente o su errori nel contenuto dei suoi articoli e delle informazioni ivi messe a disposizione e in generale su infrazioni alla legge, comprese le spese giudiziarie e di difesa.</p> 
    <p>L’utente si impegna a <strong>mantenere segreti</strong> i dati di login e le password, per evitare che persone non autorizzate (in particolare minorenni) abbiano accesso al sito web.</p> 
    <p>La DA ha il diritto di cancellare senza indicare i motivi e senza preavviso qualsiasi articolo. Nel caso in cui ci siano dei terzi che rivendicano in modo attendibile i diritti per contenuti di articoli di cui è responsabile l’utente, la DA può bloccare questi articoli e i relativi dati (anche per salvare le prove).</p> 
    <p>La <strong>cancellazione</strong> di utenti registrati avviene solo su richiesta scritta e può essere richiesto un controllo dell’identità. Da canto suo la DA può cancellare in qualsiasi momento e senza giustificazione registrazioni indebite.</p> 
    <p>La DA si riserva il diritto, ma non è suo dovere, di controllare il sito web e i profili, i contenuti e le offerte ivi pubblicate. Il blocco di tali dati è esclusivamente a discrezione della DA, l’utilizzo responsabile da parte dell’utente/cliente rimane espressamente valida.</p> 
    <h2>&sect; 5 Inserzioni e pubblicità su banner nel sito web</h2> 
    <p>L’inserzionista, in qualità di cliente, conferisce a DA l’incarico di mettergli a disposizione sul sito web, per un certo periodo, un numero desiderato di spazi per inserzioni commerciali o non commerciali (con o senza fotografia) o banner (attualmente le rubriche sono Escort, Boys, Trans, Discounted Girls, Tours, Directories interne, queste possono essere in qualsiasi momento ampliate o ridotte) in cambio del versamento del costo corrente dell’annuncio. La DA si riserva il diritto di modificare in qualsiasi momento il tipo, il volume, le condizioni e i costi delle inserzioni per le scadenze future.</p> 
    <p>Le inserzioni e i banner vengono concordati per iscritto o online sulla base di accordi separati sulle inserzioni, inoltre i regolamenti contenuti in questo paragrafo valgono come parte integrante degli accordi individuali. L’attivazione di incarichi viene confermata al cliente per e-mail (se questa è stata fornita) dopo l’avvenuto pagamento della tariffa. La DA si riserva il diritto, dopo aver controllato i dati del cliente o in caso di non avvenuto pagamento della tariffa dovuta per le inserzioni, di rifiutare l’incarico o di sospenderlo o stornarlo senza preavviso; ciò non solleva il cliente dal pagamento delle somme contrattualmente dovute per le prestazioni in scadenza. <em>Il pagamento può essere effettuato in franchi svizzeri o euro in contanti alla stipula del contratto, mediante bonifico anticipato o carta di credito.</em></p> 
    <p>I clienti si impegnano a fornire solo dati conformi a verità per quanto riguarda la loro persona e i servizi da loro offerti, essi sono responsabili dei dati pubblicati e si impegnano a indennizzare la DA in caso di rivalsa diretta (comprese eventuali spese di difesa e penali). Se vengono riscontrate violazioni le inserzioni possono essere cancellate senza diffida fino alla loro correzione o in casi gravi, in modo permanente senza diritto di rimborso delle somme già pagate. Contenuti con riferimenti a pornografia infantile, sesso con animali o escrementi e i dati memorizzati dei relativi utenti e clienti vengono trasmessi subito e senza preavviso alle autorità penali competenti.</p> 
    <p>Per quanto concerne le inserzioni viene applicato il <strong>diritto svizzero sulle commesse, luogo d’adempimento e foro competente è Zurigo.</strong></p> 
    <h2>&sect; 6 Diritti d’autore, modifica del sito web</h2> 
    <p>Tutti i file di testo, le immagini, gli elementi web e sonori, i loghi e la grafica di questo sito e di tutte le sue sottocartelle sono di proprietà intellettuale della DA e sono protetti da diritti d’autore. E’ vietato scaricare, copiare o utilizzare file aggirando o non rispettando le regole d’uso descritte nel presente contratto. Nel caso questa clausola non venga rispettata, l’utente/cliente incorre in un procedimento civile e penale con richiesta di risarcimento danni. La concessione di licenza deve essere autorizzata espressamente e per iscritto dalla DA. La DA può modificare il sito web in qualsiasi momento senza preavviso.</p> 
    <h2>&sect; 7 Responsabilità, hyperlinks</h2> 
    <p>La DA non è tenuta in alcun momento a garantire o a ripristinare l’accesso al sito, il sito web può essere disattivato in qualsiasi momento senza preavviso, anche a tempo indeterminato. Non si assume <strong>alcuna responsabilità</strong> per danni causati da hardware o software difettosi o da azioni di terzi (anche in caso di negligenza) o per danni di qualsiasi tipo conseguenti all’accesso (o all’impossibilità di accedere). Rimane salvo il rimborso di parte dei costi delle inserzioni in caso di impossibilità di accesso per un periodo prolungato per sola responsabilità della DA (appositamente o per colpa grave); l’impossibilità di accesso dovuta a manutenzione del sito non da diritto a rimborsi. Salvo ai clienti inserzionisti la DA non fornisce al alcun supporto utente.</p> 
    <p>Il sito web contiene link a siti internet esterni (cosiddetti „hyperlinks“). Inserendo questi link la DA non si appropria né dei siti internet, né dei loro contenuti. Per i contenuti dei siti nei link sono responsabili solo ed esclusivamente i rispettivi offerenti. Per contenuti sconosciuti non si fornisce alcuna garanzia, neanche per danni conseguenti e per la disponibilità dei siti linkati.</p> 
    <h2>&sect; 8 Scelta giuridica, foro competente</h2> 
    <p>Queste CGC e i relativi rapporti giuridici sottostanno al diritto svizzero. Il foro competente è Zurigo.</p> 
    <p><em>June 2006</em>, ci si riserva il diritto di apportare modifiche in ogni momento.</p> 
    <p>DA Products GmbH<br /> 
    Forchstrasse 113a<br /> 
    8127 Forch<br /> 
    Svizzera</p> 
    <p><a href='mailto:info@daproducts.ch'>info@daproducts.ch</a></p> 
";
$LNG['terni'] = "Terni";
$LNG['text_of_message'] = "Testo del messaggio";
$LNG['thank_you'] = "Grazie";
$LNG['thanks_for_registration'] = "Grazie per la registrazione! Per terminare la registrazione, per favore controlla la tua casella postale e segui le istruzioni del sito.";
$LNG['this_escort_has'] = "La ragazza ha <strong>%RESULT_COUNT%</strong> recensione/i.<br />";
$LNG['third_party_escort_reviews'] = "Recensioni su ragazze di terze parti";
$LNG['thursday'] = "Giovedi";
$LNG['time'] = "Ora ";
$LNG['title'] = "Titolo";
$LNG['to'] = "A";
$LNG['to_change_use_menu'] = "Per cambiare i tuoi dati e impostazioni usa il Menu privato in alto.";
$LNG['today_new'] = "Oggi nuova";
$LNG['top20'] = "Ragazza del mese";
$LNG['top20_desc'] = "Vota la tua ragazza preferita e aiutala a diventare la Ragazza del Mese! Ogni fine mese la ragazza con il maggior numero di voti diventa la Ragazza del Mese! 
<br /></br /> Vota ADESSO! I voti sono da 1(peggiore) a 10 (migliore) !";
$LNG['top_10_ladies'] = "Top 10 ragazze";
$LNG['top_10_reviewers'] = "Top 10 utenti";
$LNG['top_30_ladies'] = "Top 30 ragazze";
$LNG['top_30_reviewers'] = "Top 30 utenti";
$LNG['tour_add'] = "Aggiungi tour";
$LNG['tour_already'] = "Hai già impostato un altro tour per questo periodo";
$LNG['tour_contact'] = "Contatto del tour";
$LNG['tour_duration'] = "Durata del tour";
$LNG['tour_email'] = "Email del tour";
$LNG['tour_empty_country'] = "Il campo Paese non può essere vuoto";
$LNG['tour_empty_from_date'] = "Dalla data non può essere vuoto";
$LNG['tour_empty_to_date'] = "Alla data non può essere vuoto";
$LNG['tour_girl'] = "Ragazza in tour";
$LNG['tour_location'] = "Luogo del tour";
$LNG['tour_lower_date'] = "Fine tour non può essere inferiore al campo inizio tour";
$LNG['tour_no_escorts'] = "Per favore, aggiunga un profilo escort come prima cosa";
$LNG['tour_phone'] = "Telefono del tour";
$LNG['tour_saved'] = "Dati Tour salvati";
$LNG['tour_update'] = "Aggiorna il tour";
$LNG['tour_updated'] = "Dati Tour aggiornati";
$LNG['tours'] = "City tours";
$LNG['trans'] = "Transessuali";
$LNG['trial'] = "Prova";
$LNG['trial_mb_desc'] = " 
<p>Ti è stato regalato un periodo di prova gratuito su %sys_site_title%. In questo periodo potrai utilizzare le seguenti funzioni:</p> 
<ul> 
    <li>profilo sulla pagina principale e sulla pagina di una città da te scelta</li> 
    <li>gli utenti potranno aggiungere recensioni su di te</li> 
    <li>partecipazione nella classifica Top10 delle recensioni e nel concorso Ragazza del Mese</li> 
    <li>partecipazione nella rotazione dei profile, così il tuo profile non sarà mai nelle ultime pagine</li> 
</ul> 
<p>e Più !</p> 
";
$LNG['trial_mb_desc2'] = " 
<p> 
Se vuoi rimanere inserzionista, dopo il periodo di prova sarà necessario comprare un Premium Spot che a parte dei vantaggi sovrascritti, ti consenterà di usare una funzione in più! 
<ul> 
    <li>Potrai usare la funzione City Tour e aprire numerose strumenti pubblicitari</li> 
    <li><strong style='color: red;'>Free printed advertisment in the Redmag 
magazine in Italy</strong> (<a 
href='http://www.redmag.it/'>www.redmag.it</a>)</li> 
</ul> 
<p><a href='mailto:%sys_contact_email%'>Clicca qui per ordinare!</a></p> 
";
$LNG['trial_mb_desc_ag'] = "%COUNT% escorts (prima scadenza in %DAYS% giorni)";
$LNG['trustability'] = "Affidabilità";
$LNG['trusted_review_popup'] = " 
    <p>Se tu ci fornisci le informazioni dettagliati sull’incontro, noi le inoltriamo direttamente alla ragazza. La ragazza non può vedere la recensione, quindi lei non sa se la tua recensione è positiva o negativa. Dopo che lei avrà confermato che ti ha incontrato, la recensione apparerà come veritiera per 100 %.</p> 
    <p>Per favore dacci qualche informazione su di te e sul posto dove hai incontrato la ragazza! Nota che solo la ragazza in questione riceverà questa informazione per sms o e-mail. Tutti questi dati non vengono memorizzati sul nostro sito. Per esempio se hai prenotato con il tuo nome o con nome falso, scrivi sempre quello e la data /ora esatta, se hai prenotato via e-mail scrivi il tuo indirizzo e-mail, etc.</p> 
    <p>Esempi.:</p> 
    <ol> 
        <li>gianni escortforum@hotmail.com, Lunedì 17 Genn. 2005 20:00</li> 
        <li>pinky 333 333 454, Mercoledì 16. Feb. 05 verso le 08:00 hotel Ibis a Milano</li> 
    </ol>";
$LNG['trusted_user_mail_subject'] = "Utente fidato di %SITE%";
$LNG['trusted_user_mail_text'] = "<pre> 
Gentile %NAME%, 
grazie per aver postato su %SITE% ! 
Sei diventato un utente affidabile del nostro sito, i tuoi post non neccessitano più l'approvazione dell'amministrare del sito.
Benvenuto nella nostra comunità! 
%SITE% Team.</pre>";
$LNG['tuesday'] = "Martedi";
$LNG['tuscany'] = "Toscana";
$LNG['type'] = "Tipo";
$LNG['ugly'] = "Brutta";
$LNG['unfriendly'] = "Scortese";
$LNG['unsubscribe'] = "Cancella iscrizione";
$LNG['upcoming_tours'] = "Tour in arrivo";
$LNG['update_languages'] = "Aggiorna lingua";
$LNG['update_locations'] = "Aggiorna location";
$LNG['update_rates'] = "Aggiorna prezzi";
$LNG['upg_esc_memb'] = "Upgrade la tua utenza";
$LNG['upg_esc_memb_desc'] = "Grazie per aver scelto di effettuare un upgrade della sua utenza su  %sys_site_title% !<BR /> Per favore scegli il tipo e la durata della utenza che lei vorrebbe acquistare e poi clicca su \"<B>UPGRADE ADESSO</B>\" per essere reindirizzato al nostro provide???    ??????";
$LNG['upgrade_membership'] = "Upgrade la tua utenza";
$LNG['upgrade_now'] = "UPGRADE ADESSO!";
$LNG['upload_failed'] = "Errore durante il caricamento delle foto";
$LNG['upload_picture'] = "Carica foto";
$LNG['upload_private_photo'] = "Carica le foto private";
$LNG['url'] = "Indirizzo Web";
$LNG['user'] = "Utente";
$LNG['username'] = "Nome utente";
$LNG['users_match'] = "Utenti corrispondenti ai criteri";
$LNG['vacation_add'] = "Aggiungi vacanza";
$LNG['vacation_end'] = "Ritornata dalla vacanza";
$LNG['vacation_no_escorts'] = "Per favore, aggiunga un profilo escort come prima cosa";
$LNG['vacation_start'] = "Inizio vacanza";
$LNG['vacations'] = "Vacanze";
$LNG['vacations_active'] = "%NAME% è in vacanza <strong>dal %FROM_DATE% al %TO_DATE%</strong>";
$LNG['vacations_active_soon'] = "%NAME%  è in vacanza, tornerà molto <strong>presto</strong>";
$LNG['vacations_currently_date'] = "Questa ragazza attualmente è in vacanza, tornerà giorno %DATE%.";
$LNG['vacations_currently_soon'] = "Questa ragazza attualmente è in vacanza, tornerà molto <strong>presto</strong>.";
$LNG['vacations_empty_to_date'] = "Il campo ritorno dalle vacanza non può essere vuoto";
$LNG['invalid_vacation_dates'] = "I dati per le vacanze non sono validi";
$LNG['vacations_lower_date'] = "Data di inizio non può essere postecedente rispetto alla data";
$LNG['vacations_return'] = "Ritornata dalla vacanza";
$LNG['verification_email'] = "Cara/o %USER%, grazie per essersi registrata/o su %SITE%. Per confermare la sua registrazione utilizzi questo link:";
$LNG['verification_email_subject'] = "%sys_site_title% account verificato";
$LNG['very_simple'] = "Molto semplice";
$LNG['view_gallery'] = "Vedi galleria fotografica";
$LNG['view_my_private_photos'] = "Vedi la mia galleria fotografica privata";
$LNG['view_my_profile'] = "Vedi il mio profilo";
$LNG['view_my_public_photos'] = "Vedi le mie foto pubbliche";
$LNG['view_profile'] = "Vedi il mio profilo";
$LNG['view_reviews'] = "Aggiungi/Visualizza recensioni";
$LNG['view_site_escort_reviews'] = "Vedi le recensioni escort di %SITE%";
$LNG['view_third_party_escort_reviews'] = "Vedi le recensioni escort di terze parti";
$LNG['vip'] = "VIP";
$LNG['vip_mb_desc_ag'] = "%COUNT% escorts";
$LNG['vote'] = "Vota";
$LNG['vote_her_looks'] = "Vota il suo aspetto";
$LNG['votes'] = "Voti";
$LNG['waist'] = "Vita";
$LNG['watch_list'] = "Lista dei Favoriti";
$LNG['web'] = "Web";
$LNG['wednesday'] = "Mercoledì";
$LNG['week'] = "Settimana";
$LNG['weeks'] = "Settimane";
$LNG['weight'] = "Peso";
$LNG['welcome'] = "Benvenuto su %sys_site_title%";
$LNG['whats_new'] = "Quali novità?";
$LNG['when_you_met_her'] = "Quando l'hai incontrata?";
$LNG['when_you_met_her_desc'] = "data e ora";
$LNG['when_you_met_lady'] = "Quando hai incontrato la ragazza?";
$LNG['where_did_you_meet_her'] = "Dove hai incontrato la ragazza?";
$LNG['where_you_met_her'] = "Dove hai incontrato la ragazza?";
$LNG['where_you_met_her_desc'] = "hotel, casa sua, casa mia";
$LNG['white'] = "Bianca";
$LNG['whole'] = "Intero";
$LNG['wlc_city'] = "Città di lavoro";
$LNG['wlc_country'] = "Paese di lavoro";
$LNG['women'] = "Donne";
$LNG['working_locations'] = "Città di lavoro";
$LNG['working_time'] = "Orario di lavoro";
$LNG['write_here_name_of_site'] = "Per favore aggiungi sotto il nome e il link della ragazza.";
$LNG['wrong_login'] = "Login non corretta";
$LNG['wrong_login_or_password'] = "Login o password non corretta";
$LNG['wrong_time_interval'] = "Intervallo errato negli orari di lavoro il %DAY%.";
$LNG['year'] = "anno";
$LNG['years'] = "anni";
$LNG['yellow'] = "Giallo";
$LNG['yes'] = "Si";
$LNG['yes_more_times'] = "Si, più volte";
$LNG['you_are_premium'] = "Tu sei un Utente Premium";
$LNG['you_have_log'] = "Devi effettuare l'accesso per utilizzare questa funzione. <a href='%LINK%'>Accedi al tuo profilo</a>oppure<a href='%LINK2%'>Iscriviti adesso.</a>";
$LNG['your_comment_added'] = "Il tuo commento è stato aggiunto con successo.";
$LNG['you_have_voted_for'] = "Hai votato per %ESCORT%! Grazie.";
$LNG['you_r_watching_agencies'] = "Queste <b>agenzie</b> sono nella tua Lista dei Favoriti:";
$LNG['you_r_watching_escorts'] = "Queste <b>escorts</b> sono nella tua Lista dei Favoriti:";
$LNG['you_r_watching_users'] = "Questi <b>utenti</b> sono nella tua Lista dei Favoriti:";
$LNG['your_booking_text'] = "Il testo della prenotazione";
$LNG['your_data'] = "I tuoi dati";
$LNG['your_discount'] = "Il tuo sconto è: %DISCOUNT%%";
$LNG['your_full_name'] = "Il tuo nome completo";
$LNG['your_info'] = "Le tue informazioni";
$LNG['your_info_desc'] = "e-mail, telefono, nome";
$LNG['your_login_name'] = "La tua login";
$LNG['your_name'] = "Il tuo nome";
$LNG['your_report'] = "Il tuo report";
$LNG['zip'] = "Codice postale";
$LNG['69'] = "69";
$LNG['tours_in_other_countries'] = "Tours negli altri paesi";
$LNG['brunette'] = "Bruna";
$LNG['hazel'] = "Nocciola";
$LNG['profile_or_main_page_links_only'] = "Aggiungi il link solo del tuo sitopersonale o la pagina principale del sito dell'agenzia. ";
$LNG['competitors_links_will_be_deleted'] = "Tutti gli annunci di terze parti/links di concorrenti verranno cancellati dall'amministratore!";
$LNG['domain_blacklisted'] = "Siti in blacklist";
$LNG['phone_blocked'] = "Numero di telefono è bloccato";
$LNG['girls'] = "Ragazze";
$LNG['inform_about_changes'] = "Grazie. Vi informeremo circa i cambiamenti su %sys_site_title%";
$LNG['escort_photos_over_18'] = "Tutte le escorts avevano 18 anni al momento dell'inserimento dell'annuncio.";
$LNG['captcha_please_enter'] = "Per favore accedi";
$LNG['captcha_here'] = "qui";
$LNG['captcha_verification_failed'] = "Errore nella verifica";
$LNG['my_awards'] = "I miei premi";
$LNG['escort_girls'] = "Ragazze Escort";
$LNG['locally_available_in_countries'] = "Siamo raggiungibili anche nei seguenti paesi";
$LNG['domina'] = "Domina";
$LNG['dominas'] = "Dominas";
$LNG['club'] = "Club";
$LNG['club_name'] = "Nome del Club";
$LNG['club_profile'] = "Profilo del Club";
$LNG['studio'] = "Studio";
$LNG['studio_girls'] = "Ragazze Studio";
$LNG['all_regions'] = "Tutte le regioni…";
$LNG['see_all_escorts_of_this_agency'] = "Vedi tutte le escorts di questa agenzia";
$LNG['see_all_escorts_of_this_club'] = "Vedi tutte le escorts di questo club";
$LNG['invalid_height'] = "Altezza non valida";
$LNG['invalid_weight'] = "Peso non valido";
$LNG['subscribe_to_newsletter'] = "Iscriviti alla nostra newsletter!";
$LNG['inform_every_week'] = "Vi informeremo ogni settimana sulle escort di %sys_site_title%";
$LNG['clubs_match'] = "Clubs corrispondenti ai criteri";
$LNG['welcome_description'] = "";
$LNG['advertise_now'] = "";
$LNG['autumn_action'] = "";
$LNG['escort_girls_description'] = "";
$LNG['independent_girls_description'] = "";
$LNG['studio_girls_description'] = "";
$LNG['index_bottom_text'] = "";
$LNG['escort_girls_in_city'] = "Ragazze escort in %CITY%";
$LNG['independent_girls_in_city'] = "Ragazze Private in %CITY%";
$LNG['studio_girls_in_city'] = "Ragazze Studio in %CITY%";
$LNG['new_arrivals_in_city'] = "Ragazze novità in %CITY%";
$LNG['all_girls_in_city'] = "Tutte le ragazze in %CITY%";
$LNG['see_all_girls_from_agency'] = "Clicca qui per vedere tutte le ragazze di questa agenzia";
$LNG['see_all_independent_girls'] = "Clicca qui per vedere tutte le ragazze indipendenti";
$LNG['last_modified'] = "Ultima modifica";
$LNG['advertise_with_us'] = "Pubblicizza con noi";
$LNG['nearest_cities_to_you'] = "Città vicine a te";
$LNG['nearest_cities_to_city'] = "Vicine alla città di %CITY%";
$LNG['cities_in_region'] = "Città nella %REGION%";
$LNG['local'] = "Locale";
$LNG['local_directory'] = "Liste Club/Escort";
$LNG['exchange_banners'] = "If do you want interchange banners and/or links with us, email us to <a href='mailto:%sys_contact_email%'>%sys_contact_email%</a>";
$LNG['our_network_in_europe'] = "La nostra rete in Europa";
$LNG['agencies_and_independent_in_spain'] = "Agenzie e ragazze indipendenti in Spagna";
$LNG['directories_and_guide_in_spain'] = "Siti, forum e guide erotiche in Spagna";
$LNG['friends'] = "Amici";
$LNG['escort_service'] = "Servizi Escort";
$LNG['studio_and_private_girls'] = "Studio e ragazze private";
$LNG['advertising'] = "Annunci";
$LNG['premium_girls'] = "Ragazze Premium";
$LNG['submit_form_to_signup'] = "Invia questo modulo per iscriverti e verrai publicizzata dal più grande directory escort!";
$LNG['signup_now_to_largest_escort_directory'] = "Iscriviti adesso <span class='pink'>GRATIS</span> alla comunità più grande di escort! Attualmente abbiamo più di<span class='pink'>3 millioni di clienti</span> al mese sul nostro network. Usa la nostra poplarità per farti della pubblicità <span class='pin???    ??????";
$LNG['signing_on_all_local_sites'] = "Registrandoti il tuo profilo sarà automaticamente visualizzata in tutti i nostri siti locali. I tuoi dati d'accesso funzioneranno su tutti i nostri siti - basta registrarti una volta!";
$LNG['please_choose_your_business'] = "Per favore seleziona la tua attività";
$LNG['independent_privat'] = "Escort Independente / Ragazza Privata";
$LNG['escort_agency'] = "Agenzia Escort";
$LNG['club_bordell_strip'] = "Club / Bordello / Sauna Club / Stripclub (Tabledance), Studio Massaggi";
$LNG['please_write_numbers_from_field'] = "Per favore scriva i numeri nel campo";
$LNG['just_moment_away_from_escort_directory'] = "You are just moment away from having access to largest escort directory!";
$LNG['choose_your_region'] = "Scegli regione";
$LNG['setcards_online'] = "%COUNT% escorts online";
$LNG['only_escorts'] = "solo escort";
$LNG['only_private_girls'] = "solo ragazze private";
$LNG['only_studio_girls'] = "solo ragazze in studio";
$LNG['most_popular_cities'] = "Le città più popolari";
$LNG['renew_your_profile'] = "Il tuo profilo è scaduto. Se vuoi rinnovarlo, contatta il tuo Sales di riferimento oppure scrivici all'indirizzo <a href='mailto:%sys_contact_email%'>%sys_contact_email%</a>";
$LNG['relax'] = "Relax";
$LNG['email_unsubscribed'] = "E-mail %EMAIL% è stata rimossa dalla newsletter del sito %sys_site_title%";
$LNG['vip_girls'] = "Ragazze VIP";
$LNG['crop_photo'] = "Foto principale";
$LNG['show_thumbnails_on'] = "Mostra anteprima in miniatura";
$LNG['photo_was_cropped'] = "Questa foto adesso è la principale.";
$LNG['crop_feature_teaser'] = " 
    <p><strong style='color: red;'>ATTENZIONE:</strong><br /> 
    Abbiamo lanciato una nuova funzione. tutte le foto che sono nella prima pagina e nella lista internazionale hanno la stessa dimensione. Per questo vi chiediamo di verificare la sua foto ???    ?????? ? ?? ????( ?À  ???  ?  ???? ?    ?????   ??";
$LNG['crop_feature_description'] = " 
    <p><strong style='color: red;'>ATTENZIONE:</strong><br /> 
    Con la nostra nuova funzione, tutte le foto nella PAGINA PRINCIPALE appariranno della stessa dimensione. Questa funzione darà al sito una aspetto molto più professionale.</p> 
    <p>Ecc???    ?????? ? ?? ????( ?À  ???  ?  ???? ?    ?????   ??????À ??????????????????????  ?????????????  ????W?(  ?(??? &  ??????????(  ??À  ?????? ???? ????????????????? ?Item  ??      ?Count  ??????0  ??    0????0????????  ??????   0   ??  ??? ?  ??????  ?W??  ?ò   ???  ??W???W?  ??? ?W?W  ???t ??? ? ? ??????    ??0Eo  ?0  ?????  ????0??i  ??????????0i  ????????????  ??????????????? ?? ??????? ????????????W ?   ?????    ?? ???????    ?? ????t ?? ???    ?  ???????????  ???6? ???/?????????   ??j  ?  ????????????????? ??	 ???????????? ?? ??????? ????????????t ?   ?????    ??????f??t??????????????/???????? ?/  ??#             ???????Eq ???Q??@ ???S?? ??q ??S??? ?? ????????( ????? ??????????????";
$LNG['we_only_sell_advertisement'] = "<strong>%sys_site_title%</strong> è un sito di annunci e di ricerca informazioni e non ha nessuna connessione o nessuna responsabilità con siti o annunci individuali presenti sul nostro sito. Noi vendiamo SOLO spazi pubblicitari, non siamo una agenzia esc???    ?????? ? ?? ????( ?À  ???  ?  ???? ?    ?????   ??????À ??????????????????????  ?????????????  ????W?(  ?(??? &  ??????????(  ??À  ?????? ???? ????????????????? ?Item  ??      ?Count  ??????0  ??    0???";
$LNG['user_messages_review_waiting_for_approval'] = "La sua recensione su %MESSAGE% è in attesa di approvazione. Attendere per favore";
$LNG['user_messages_favorite_girl_review'] = "La sua ragazza favorita <strong>%MESSAGE%</strong> ha una nuova recensione. <a href='%LINK%'>clicchi qui per leggerla</a>";
$LNG['user_messages_review_approval'] = "La sua recensione <strong>%MESSAGE%</strong> è stata approvata dall'amministratore.";
$LNG['user_messages_review_deleted'] = "La sua recensione <strong>%MESSAGE%</strong> è stata cancellata dall'amministratore. Motivo: %LINK%";
$LNG['user_messages_new_discount'] = "%MESSAGE% le sta offrendo uno sconto speciale su Escortforum. <a href='%LINK%'>Clicchi qui per accettare!</a>";
$LNG['user_messages_deleted_discount'] = "%MESSAGE% non le offre più uno sconto speciale su Escortforum.";
$LNG['user_messages_favorite_disabled'] = "La sua ragazza preferita <strong>%MESSAGE%</strong> non è più nel nostro sito! Ci dispiace! Forse vuole aggiungere un'altra ragazza nei favoriti?";
$LNG['user_messages_favorite_activated'] = "La sua ragazza preferita <strong>%MESSAGE%</strong> è di nuovo nel nostro sito!";
$LNG['user_messages_girl_of_month'] = "La ragazza del mese %MONTH%/%YEAR% è: <strong>%MESSAGE%</strong>!! Congratulazioni %MESSAGE%!!!";
$LNG['user_messages_free_fuck_winner'] = "<strong>Sei il VINCITORE della LOTTERIA FREEFUCK, contattaci adesso.</strong>";
$LNG['user_messages_free_fuck_others'] = "Il vincitore della LOTTERIA FREEFUCK del mese %MONTH%/%YEAR% è: <strong>%MESSAGE%</strong>!! Congratulazioni %MESSAGE%!!!";
$LNG['user_messages_favorite_region_girls'] = "Nuove ragazza nella tua regione si sono iscritti dall'ultima sua visita. %LINK%";
$LNG['user_messages_favorite_region_vip'] = "Vi sono nuove VIP %MESSAGE% nella sua regione preferita dall'ultima sua visita. %LINK%";
$LNG['user_email_review_approved'] = "Salve %TARGET_0%
la sua recensione %TARGET_1% è stata approvata dall'amministratore.
%sys_site_title% Team";
$LNG['escort_email_review_approved'] = "Salve %TARGET_0%
la recensione %TARGET_1% su di lei è stata approvata dall'amministratore.
%sys_site_title% Team";
$LNG['user_email_favorites_review_approved'] = "Salve %TARGET_0%
la sua ragazza preferita  %TARGET_1% ha una nuova recensione.
%sys_site_title% Team";
$LNG['agency_email_review_approved'] = "Salve %TARGET_0%
La recensione su %TARGET_1%  è stata approvata dall'amministratore.
%sys_site_title% Team";
$LNG['user_email_subject_review_approval_favorites'] = "%sys_site_title% nuova recensione";
$LNG['user_email_subject_review_approval'] = "%sys_site_title% recensione approvata";
$LNG['escort_email_subject_review_approval'] = "%sys_site_title% recensione approvata";
$LNG['agency_email_subject_review_approval'] = "%sys_site_title% recensione approvata";
$LNG['escort_email_review_new'] = "Salve %TARGET_0%
una nuova recensione %TARGET_1% su di te è in attesa di approvazione.
%sys_site_title% Team";
$LNG['agency_email_review_new'] = "Salve %TARGET_0%
la sua escort %TARGET_1% ha una nuova recensione in attesa di approvazione.
%sys_site_title% Team";
$LNG['user_email_review_new'] = "Salve %TARGET_0%
la sua recensione su %TARGET_1% è in attesa di approvazione. Per favore attenda.
%sys_site_title% Team";
$LNG['user_email_subject_review_new'] = "La recensione su %sys_site_title% è in attesa di approvazione";
$LNG['escort_email_subject_review_new'] = "La nuova recensione su %sys_site_title% è in attesa di approvazione";
$LNG['agency_email_subject_review_new'] = "La nuova recensione su %sys_site_title% è in attesa di approvazione";
$LNG['user_email_review_deleted'] = "Salve %TARGET_0%
la sua recensione su <strong>%TARGET_1%</strong>  è stata cancellata dall'amministratore. Motivazione: %TARGET_2%
%sys_site_title% Team";
$LNG['escort_email_review_deleted'] = "Salve %TARGET_0%
La recensione <strong>%TARGET_1%</strong> su di te è stata cancellata dall'amministratore. Motivazione: %TARGET_2%
%sys_site_title% Team";
$LNG['agency_email_review_deleted'] = "Salve %TARGET_0%
la recensione su <strong>%TARGET_1%</strong> è stata cancellata dall'amministratore. Motivazione: %TARGET_2%
%sys_site_title% Team";
$LNG['user_email_subject_review_delete'] = "%sys_site_title% recensione cancellata";
$LNG['escort_email_subject_review_delete'] = "%sys_site_title% recensione cancellata";
$LNG['agency_email_subject_review_delete'] = "%sys_site_title% recensione cancellata";
$LNG['user_email_discount_new'] = "Hello %TARGET_0%
%TARGET_1% ti offre uno sconto speciale EF.
%sys_site_title% Team";
$LNG['escort_email_discount_new'] = "Hello %TARGET_0%
è stato assegnato un nuovo sconto.
%sys_site_title% Team";
$LNG['agency_email_discount_new'] = "Hello %TARGET_0%
%TARGET_1% EF ti offre uno sconto speciale.
%sys_site_title% Team";
$LNG['user_email_subject_discount_new'] = "%sys_site_title% nuovo sconto";
$LNG['escort_email_subject_discount_new'] = "%sys_site_title% nuovo sconto";
$LNG['agency_email_subject_discount_new'] = "%sys_site_title% nuovo sconto";
$LNG['user_email_discount_deleted'] = "Hello %TARGET_0%
Your escort %TARGET_1% EF non ti offre mai piu uno sconto speciale.
%sys_site_title% Team";
$LNG['user_email_subject_discount_deleted'] = "%sys_site_title% sconto scaduto";
$LNG['user_email_escort_deactivated'] = "Hello %TARGET_0%
Your favorite girl <strong>%TARGET_1%</strong> non è più con noi! Ci dispiace! Forse il tempo per trovare un nuovo sito?
%sys_site_title% Team";
$LNG['escort_email_escort_deactivated'] = "Hello %TARGET_0%
Il tuo account è stato disattivato.
%sys_site_title% Team";
$LNG['agency_email_escort_deactivated'] = "Hello %TARGET_0%
Account di %TARGET_1% è stato disattivato.
%sys_site_title% Team";
$LNG['user_email_subject_favorites_deactivated'] = "%sys_site_title% escort disattivata";
$LNG['escort_email_subject_favorites_deactivated'] = "%sys_site_title% account disattivato";
$LNG['agency_email_subject_favorites_deactivated'] = "%sys_site_title% account disattivato";
$LNG['user_email_escort_activated'] = "Hello %TARGET_0%
La tua ragazza favorita <strong>%TARGET_1%</strong> si è riscritta!
%sys_site_title% Team";
$LNG['escort_email_escort_activated'] = "Hello %TARGET_0%
Il tuo accounto è stato attivato.
%sys_site_title% Team";
$LNG['agency_email_escort_activated'] = "Hello %TARGET_0%
Account di %TARGET_1% è stato attivato.
%sys_site_title% Team";
$LNG['agency_email_subject_favorites_activated'] = "%sys_site_title% account attivato";
$LNG['user_email_girl_of_the_month'] = "Hello %TARGET_0%
La ragazza del mese %TARGET_1% è: <strong>%TARGET_2%</strong>!! Congratulazioni %TARGET_2%!!!
%sys_site_title% Team";
$LNG['escort_email_girl_of_the_month'] = "Hello %TARGET_0%
La ragazza del mese %TARGET_1% è: <strong>%TARGET_2%</strong>!! Congratulazioni %TARGET_2%!!!
%sys_site_title% Team";
$LNG['escort_email_girl_of_the_month_winner'] = "Hello %TARGET_0%
Sei la nuova ragazza del mese %TARGET_1%! Congratulazioni %TARGET_0%!!!
%sys_site_title% Team";
$LNG['user_email_subject_girl_of_the_month'] = "%sys_site_title% ragazza del mese";
$LNG['escort_email_subject_girl_of_the_month'] = "%sys_site_title% ragazza del mese";
$LNG['user_email_free_fuck_winner'] = "Hello %TARGET_0%
<strong>Tu sei il VINCITORE della lotteria freefuck contattaci.</strong>
%sys_site_title% Team";
$LNG['user_email_free_fuck_others'] = "Hello %TARGET_0%
Il vincitore della lotteria freefuck del mese %TARGET_1% è: <strong>%TARGET_2%</strong>!! Congratulazioni %TARGET_2%!!!
%sys_site_title% Team";
$LNG['user_email_subject_free_fuck_winner'] = "%sys_site_title% vincitore della lotteria freefuck";
$LNG['user_email_subject_free_fuck_others'] = "%sys_site_title% lotteria freefuck";
$LNG['user_email_new_girls_in_region'] = "Hello %TARGET_0%
Una nuova ragazza della tua regione favorita si è iscritta su EF.
%sys_site_title% Team";
$LNG['user_email_subject_new_girls_in_region'] = "%sys_site_title% nuove ragazze";
$LNG['user_email_new_vip_girls_in_region'] = "Hello %TARGET_0%
Ci sono nuove VIP %TARGET_1% dalla vostra regione preferita dalla tua ultima visita. %TARGET_2%
%sys_site_title% Team";
$LNG['user_email_subject_new_vip_girls_in_region'] = "%sys_site_title% nuove ragazze VIP";
$LNG['loft'] = "Loft";
$LNG['loft_escorts_photo_limit_reached'] = "Loft escorts possono avere max %LIMIT% foto in galleria.";
$LNG['loft_escorts_cant_go_on_tour'] = "Le escorts Loft non possono andare in tour.";
$LNG['email_exists'] = "L'indirizzo email esiste";
$LNG['members_login'] = "Members Login";
$LNG['signup_now'] = "Iscriviti ora";
$LNG['live_now'] = "Live adesso";
$LNG['girl_has_video'] = "Ragazza con video";
$LNG['girl_has_reviews'] = "Ragazza con recensioni";
$LNG['100%_real_photos'] = "100% foto reali";
$LNG['happy_hour'] = "Happy hour";
$LNG['icons_explanation'] = "Icone spiegazione";
$LNG['offer_incall'] = "rivevo";
$LNG['offer_outcall'] = "outcall";
$LNG['ebony_only'] = "Solo ebano";
$LNG['asian_only'] = "solo Asiatica";
$LNG['blondes_only'] = "solo bionde";
$LNG['add_to_my_favourites'] = "aggiungi ai miei favoriti";
$LNG['remove_from_favourites'] = "Rimuovi da Preferiti";
$LNG['add_view_reviews'] = "aggiungi/vedi recensioni";
$LNG['about_escort'] = "Escort %SHOWNAME%";
$LNG['contact_me'] = "Contattami";
$LNG['now_open'] = "apero";
$LNG['closed'] = "chiuso";
$LNG['viewed'] = "Visto (ultimi 30 giorni)";
$LNG['quick_links'] = "Links rapidi";
$LNG['free_signup'] = "Iscrizione gratuita";
$LNG['signup_profit'] = " 
<ul> 
    <li>Aggiungi <strong>commenti</strong></li> 
    <li class='fsa-odd'>gestire <strong>favorite</strong> list</li> 
    <li>scrivere <strong>recensioni</strong></li> 
    <li class='fsa-odd'>l'accesso ai <strong>ricerca avanzata</strong></li> 
 ???    ?????? ? ?? ????( ?Ð  ???  ?  ?";
$LNG['sort_by'] = "Ordina";
$LNG['random'] = "Casuale";
$LNG['alphabetically'] = "In ordine alfabetico";
$LNG['most_viewed'] = "Le più viste";
$LNG['newest_first'] = "Più recenti";
$LNG['narrow_your_search'] = "Perfeziona la tua ricerca";
$LNG['with_reviews'] = "Con recensioni";
$LNG['search_by_name_agency'] = "Cerca per nome / agenzia";
$LNG['comments_responses'] = "Commenti escort &amp; Risposte";
$LNG['post_comment'] = "Pubblica un commento";
$LNG['back'] = "Indietro";
$LNG['reply'] = "Rispondi";
$LNG['spam'] = "Spam";
$LNG['only_registered_users_can_add_comments'] = "Solo gli utenti registrati possono aggiungere commenti per questa escort. Si prega di <a href='%VALUE%' rel='nofollow'>login here</a>.";
$LNG['your_comment'] = "Il tuo commento";
$LNG['close'] = "Chiudi";
$LNG['message_waiting_for_approval'] = "Grazie. Il tuo messaggio è ora in attesa di approvazione.";
$LNG['ago'] = "fa";
$LNG['time_ago'] = "%TIME% fa";
$LNG['yesterday'] = "Ieri";
$LNG['minute'] = "minuto";
$LNG['spanking'] = "Sculacciata";
$LNG['domination'] = "Dominazione";
$LNG['fisting'] = "Fisting";
$LNG['massage'] = "Massaggio";
$LNG['role_play'] = "Gioco di ruolo";
$LNG['bdsm'] = "BDSM";
$LNG['hardsports'] = "Hardsports";
$LNG['hummilation'] = "Umiliazione";
$LNG['rimming'] = "Rimming";
$LNG['less_than'] = "meno di %NUMBER%";
$LNG['more_than'] = "di piu di %NUMBER%";
$LNG['free_signup_advantages'] = "La registrazione su <span class='red'>%sys_site_title%</span> è assolutamente <span class='red-big'>GRATUITO</span> e ha molti vantaggi.";
$LNG['reset'] = "Reset";
$LNG['private_area'] = "Area riservata";
$LNG['choose_your_choice'] = "Fai la tua scelta";
$LNG['base_city'] = "Citta base";
$LNG['additional_cities'] = "Altre città";
$LNG['zone'] = "Zone";
$LNG['actual_tour_data'] = "Adesso in %CITY% (%COUNTRY%)";
$LNG['upcoming_tour_data'] = "in %DAYS% in %CITY% (%COUNTRY%), from %FROM_DATE% - %TO_DATE%";
$LNG['beta_warning'] = "%sys_site_title% è in fase Beta. Siamo costantemente impegnati nel lavorare e rendere regolari aggiornamenti e miglioramenti.";
$LNG['ethnic_white'] = "Caucasica (bianca)";
$LNG['ethnic_black'] = "nero";
$LNG['ethnic_asian'] = "Asiatica";
$LNG['ethnic_latin'] = "Latina";
$LNG['ethnic_african'] = "Africana";
$LNG['ethnic_indian'] = "indiana";
$LNG['members_signup'] = "Utenti registrati";
$LNG['click_here_your_signup'] = "Clicca qui per la registrazione.";
$LNG['your_email'] = "Il tuo indirizzo e-mail";
$LNG['missing_message'] = "Manca il messaggio";
$LNG['your_message_sent'] = "Il tuo messaggio è stato inviato.";
$LNG['signup'] = "Iscriviti";
$LNG['signup_to_largest_escort_directory'] = "Iscriviti ora per <span class='red-big'>FREE</span> la guida di escort più grande di tutto il mondo. Attualmente abbiamo più di <span class='red-big'>3 million clients</span> mensilmente sul nostro sito. Inizia ad usare la nostra popolarità per promuovere???    ?????? ? ?? ????( ?à  ???  ?  ???";
$LNG['one_signup_all_sites'] = "Con la firma, il tuo profilo verrà automaticamente visualizzato su tutti questi siti nazionali locali. I dati di login funziona su tutti i siti - è sufficiente per l'iscrizione una volta.";
$LNG['email_verification_subject'] = "%sys_site_title% verifica dell' account";
$LNG['email_verification_text'] = "<b>Dear %USER%,</b><br /><br /> 
<b>Grazie per esserti registrato a %sys_site_title%</b>.<br /><br /> 
Tu sei a un passo dall' attivazione dell'account, è sufficiente fare clic sul link qui sotto o copia e incolla nel tuo browser.<br /><br /> 
<b><a href=???    ?????? ? ?? ????( ?à  ???  ?  ???? ?    ?????   ??????à ??????????????????????  ?????????????  ????W?(  ?(??? &  ??????????(  ??Ð  ?????? ???? ????????????????? ?Item  ??      ?Count  ???????  ??    ??????????????  ??????   ?   ??  ??? ?  ??????  ?W??  ?ò   ???";
$LNG['email_confirmation_subject'] = "%sys_site_title% registrazione";
$LNG['email_confirmation_text'] = "Gentile %Utente%,</b><br /><br /> 
Grazie per esserti registrato su %sys_site_title%.<br /> 
La tua registrazione è ora completa e sei un membro attivo di %sys_site_title%.<br /><br /> 
<b>Di seguito sono  riportati i dati di accesso. Si prega di conserva???    ?????? ? ?? ????( ??  ???  ?  ???? ?    ?????   ??????? ??????????????????????  ?????????????  ????W?(  ?(??? &  ??????????(  ??Ð  ?????? ???? ????????????????? ?Item  ??      ?Count  ???????  ??    ??????????????  ??????   ?   ??  ??? ?  ??????  ?W??  ?ò   ???  ??W???W?  ??? ?W?W  ???t ??? ? ? ??????    ???Eo  ??  ???????????????i  ?";
$LNG['contact_us_text1'] = "Nel caso in cui se volete contattarci per informazioni generali, commenti o pubblicità, scegliere tra le seguenti opzioni";
$LNG['phone_international'] = "Telefono internazionale";
$LNG['contact_us_text2'] = "In alternativa si prega di utilizzare il nostro modulo di contatto qui sotto";
$LNG['your_message'] = "Il tuo messaggio";
$LNG['link_exschange'] = "Scambio di link";
$LNG['contact_us_text3'] = "Se volete lo scambio di link, invia un'email al webmaster (e-mail).<br />Si prega di copiare il nostro banner e inserirlo sul server prima di contattarci per quanto riguarda lo scambio di link.";
$LNG['delete_profile'] = "Elimina profilo";
$LNG['confirm_delete_profile'] = "Sei sicuro di voler eliminare il profilo escort?";
$LNG['delete_escort'] = "Elimina il profilo escort";
$LNG['escort_profile_deleted'] = "Il profilo escort è stato eliminato con successo";
$LNG['escort_profile_deleted_error'] = "Errore: il profilo Escort non è stato eliminato";
$LNG['same_city'] = "La citta base non puo' essere uguale alla citta del tour";
$LNG['overnight'] = "per una notte";
$LNG['directory_for_female_escorts'] = "<strong>Escort Guide è una directory UK per le ragazze escort, agenzie di escort, indipendente escort, trans e coppie</strong>. 
On <a href='http://www.escortguide.co.uk' title='escort guide'>escortguide.co.uk</a> È possibile trovare <strong>escort in Lon???    ?????? ? ?? ????( ?à  ???  ?  ???? ?    ?????   ??????à ??????????????????????  ?????????????  ????W?(  ?(??? &  ?1??????1?(  ?1Ð  ????1? ??1? ????1???1????1??? ?Item  ??      ?Count  ????1??  ?1    ??????????????  ??????   ?   ??  ??? ?  ??????  ?W??  ?ò   ???  ??W???W?  ??? ?W?W  ???t ??? ? ? ??????    ???Eo  ??  ???????????????i  ??";
$LNG['popularity_views'] = "Popolarità / Vista";
$LNG['last_modified_date'] = "Data ultima modifica";
$LNG['registered_date'] = "Data di registrazione";
$LNG['word'] = "parola";
$LNG['phrase'] = "frase";
$LNG['colour'] = "colore ...";
$LNG['doesnt_matter'] = "Non importa";
$LNG['specific_words'] = "Parole specifiche";
$LNG['kissing_no'] = "No bacio";
$LNG['blowjob_no'] = "No Pompini";
$LNG['cumshot_no'] = "Non è possibile venire";
$LNG['cumshot_in_mouth_spit'] = "Venire in bocca con sputo";
$LNG['cumshot_in_mouth_swallow'] = "Venire In bocca con ingoio";
$LNG['cumshot_in_face'] = "Venire in faccia";
$LNG['with_comments'] = "Con commenti";
$LNG['nickname'] = "Nickname";
$LNG['without_reviews'] = "Senza recensioni";
$LNG['not_older_then'] = "Non più vecchi di";
$LNG['order_results'] = "Ordine dei risultati";
$LNG['select_your_girl_agency_requirements'] = "Seleziona la tua ragazza / requisiti di agenzia";
$LNG['girls_details_bios'] = "Dettagli ragazze/bio??™s";
$LNG['cm'] = "cm";
$LNG['ft'] = "ft";
$LNG['kg'] = "kg";
$LNG['lb'] = "lb";
$LNG['girls_origin'] = "Origini ragazza";
$LNG['ethnicity'] = "Etnia";
$LNG['measurements'] = "Misure";
$LNG['open_hours'] = "Orari d'apertura";
$LNG['my_favorites'] = "I miei favoriti";
$LNG['learn_more_webcam'] = "\"Se avete una webcam, un account su
Skype o MSN, questo è il modo piu' semplice per
verificare le tue foto.
Basta cliccare sul link 'richiesta Webcam sessione,
scegliere una data e ora e attendere la nostra
conferma. Al tempo richiesto si
poi ho un appunt???    ?????? ? ?? ??1?( 1?  ???  ?  ???? ?    ?????   ?????1? ??????????????????????  ?????????????  ????W?(  ?(??? &  ??????????(  ??à  ??????";
$LNG['learn_more_passport'] = "\"Se si sceglie questa opzione è necessario eseguire la scansione del
passaporto o qualsiasi altro documento ufficiale come
carta d'identità o patente di guida e caricarlo
nel nostro sistema insieme con 1-2 foto private \".";
$LNG['verify_welcome'] = "Benvenuti!";
$LNG['verify_welcome_text'] = "Benvenuti al processo al 100% dell'immagine verificata.
Usa la possibilità e ottenere la vostra foto di profilo che deve
100% verificato genuino. In pochi semplici
passi si aumenterà la credibilità del tuo profilo
e ottenere molti più utenti. Si otterrà i???    ?????? ? ?? ??1?( 1?  ???  ?  ???? ?    ?????   ?????1? ??????????????????????  ?????????????  ????W?(  ?(??? &  ??????????(  ??";
$LNG['verify_enroll'] = "Iscriviti ora!";
$LNG['verify_enroll_text'] = "Forniamo attualmente i seguenti <strong>2 opzioni</strong> per ottenere le immagini 100% verificato:";
$LNG['verify_enroll_1'] = "Webcam richiesta con il nostro admin";
$LNG['verify_enroll_2'] = "Copia del passaporto";
$LNG['verify_learn_more'] = "Per saperne di più ...";
$LNG['verify_start_now'] = "<span>PARTE ADESSO </span> il processo di";
$LNG['verify_steps'] = "STEP LINEA GUIDA:";
$LNG['verify_select_2_options'] = "Si prega di selezionare una delle seguenti 2 opzioni per il processo:";
$LNG['verify_select_1'] = "Webcam richiesta con il nostro admin";
$LNG['verify_select_1_note'] = "<strong>note:</strong> hai bisogno di avere una webcam e un account Skype o MSN ";
$LNG['verify_select_2'] = "Copia del passaporto - è necessario un passaporto valido o carta d'identità";
$LNG['verify_select_2_note'] = "<strong>note:</strong> per accelerare il processo si consiglia di caricare 1-2 foto private, oltre alla copia del passaporto.";
$LNG['verify_important'] = "\"IMPORTANTE - LEGGERE CON ATTENZIONE: Ogni tentativo di frode o di ingannare il sito si tradurrà in un danno permanente
Vietando la rete EF. Tutti i vostri annunci a pagamento verranno disattivati senza alcun rimborso. \"";
$LNG['verify_sms_message'] = "\"conferma Videochat per il processo di verifica foto:
Date: #date#, Time: #time#. Sarete contattati dal nostro amministratore. #title#";
$LNG['verify_email_subject'] = "Appuntamento online confermato";
$LNG['verify_email_message'] = "Con la presente confermiamo l'appuntamento on-line per il processo di verifica in foto:<br /><br />
Date: #date#<br />
Time: #time#<br /><br />
Sarai contattato da uno dei nostri amministratori. Cordiali saluti.<br />#title#";
$LNG['reject_sms_message'] = "Si prega di scegliere 3 nuove date per la sessione di videochat (verifica foto). Non siamo riusciti a confermare le date richieste. Effettua il login e scegliere di nuovo. #title#";
$LNG['reject_email_subject'] = "100% Foto processo di verifica / Si prega di inserire nuove date";
$LNG['reject_email_message'] = "Egregio #showname#<br /><br />
Purtroppo tutte le date proposte per la sessione di videochat per verificare la tua foto non sono state confermate. Effettua il login di nuovo dalla tua area privata e scegli 3 nuove
date. Ci scusiamo per eventuali disagi ca???    ?????? ? ?? ??1?( 1?  ???  ?";
$LNG['reg_has_been_succ'] = "La registrazione è avvenuta con successo!";
$LNG['verify_attention'] = "Nota importante: Per il caricamento delle foto si prega di utilizzare esclusivamente il modulo sottostante. Non inviare le foto via e-mail. Inviare una e-mail al venditore solo nel caso si verifichino problemi durante il caricamento delle immagini.";
$LNG['city_zones_in'] = "Zone della città in";
$LNG['contact_showname'] = "Contatta %SHOWNAME%";
$LNG['masturbation'] = "Masturbazione";
$LNG['signup_today'] = "Iscriviti oggi come un Membro Premium";
$LNG['temp_announce'] = "Il nostro nuovo dominio è ESCORTFORUM.NET";
$LNG['latin_only'] = "Solo latina";
$LNG['seo_h1_uk'] = "Escort Guide è il principale directory escort del Regno Unito, servizio ragazze escort in tutto UK";
$LNG['escort_is_premium'] = "Questa escort ha il pacchetto premium. <br/> Se si desidera eliminare il profilo di questa escort, <br/> Si prega di assegnare il pacchetto a un'altra escort e eliminare il profilo.";
$LNG['100_verify'] = "100% Verifica";
$LNG['verify_global_title'] = "100% VERIFICATO PROCESSO DI IMMAGINE";
$LNG['verify_webcam_session'] = "Sessione webcam";
$LNG['verify_webcam_request'] = "Richiesta di una sessione webcam con il nostro admin, si prega di fornire almeno 2 appuntamenti. In caso di cancellazione pls informa l'amministratore.";
$LNG['verify_appointment'] = "Appuntamento nr .";
$LNG['verify_preffered'] = "Tempo preferito";
$LNG['verify_comment_field'] = "Commenta campo";
$LNG['verify_webcam_software'] = "Webcam software:<br /><span>(selezionare MSN o skype)</span>";
$LNG['verify_type_username'] = "Digitare il nome utente";
$LNG['verify_select_form'] = "Pls selezionato la forma che si desidera ottenere confermato la convocazione di riunione";
$LNG['verify_recommended'] = "raccomandato";
$LNG['verify_over_sms'] = "Over phone SMS";
$LNG['verify_congratulations'] = "Congratulazioni!";
$LNG['verify_congratulations_text'] = "<p>correttamente chiesto un incontro per <strong>webcam verifica</strong>. Sarete informati <strong>via mail</strong> o nel tuo <strong>account personale</strong> Se l' incontro è stato confermato e verrà fornito con tutti i dettagli.</p><p class='colored???    ?????? ? ?? ????( ?à  ???  ?  ???? ?    ?????   ??????à ??????????????????????  ?????????????  ????W?(  ?(??? &  ??????????(  ??à  ?????? ???? ????????????????? ?Item  ??      ?Count  ???????  ??    ?????";
$LNG['verify_already_pending'] = "Siete gia' in attesa di una richiesta. Si prega di attendere conferma dal nostro amministratore.";
$LNG['verify_rejected'] = "Rifiutato";
$LNG['verify_passport_upload'] = "Passaporto, carta d'identità upload";
$LNG['verify_passport_upload_text'] = "Si è scelto di fornire un passaporto o carta d'identità per il processo di verifica. Si prega notare, non possiamo accettare e-mail con la copia del passaporto o carta d'identità. È necessario caricarlo dal vostro pannello. In caso di difficoltà pls conta???    ?????? ? ?? ??1?( 1";
$LNG['verify_upload_now'] = "Carica ora i tuoi documenti";
$LNG['verify_provide_pics'] = "TIP: Fornire 1-2 foto privat, in questo modo si aumenta la tua occasione per il processo di verifica al 100% e accelerare il processo.";
$LNG['verify_select_notify'] = "Si prega di selezionare come si desidera ricevere la notifica per i risultati della verifica:";
$LNG['verify_image_no'] = "Immagine no.";
$LNG['verify_uploaded'] = "caricato";
$LNG['verify_uploaded_pictures'] = "Caricato immagini";
$LNG['verify_uploaded_pictures_text'] = "Hai caricato le immagini seguenti, si prega di mettere in campo commento, che tipo di documenti che hai fornito.";
$LNG['verify_uploaded_comment_field'] = "Commenta campo";
$LNG['verify_congratulations_text2'] = "<p class='colored'><strong>È stato completato il processo di verifica al 100% da upload di documenti!</strong></p><p class='verpic-note'>Si prega di notare che dal lunedi al venerdì la vostra richiesta verrà data risposta entro 24 ore, nei week-end appena???    ?????? ? ?? ????( ?à  ???  ?  ???? ?    ?????   ??????à ??????????????????????";
$LNG['verify_genuine'] = "100% verified foto !<br/>
Tutte le foto sono genuine - approvate e confermate dallo staff  EF.";
$LNG['verify_welcome_agency'] = "\"Benvenuti al 100% processo di verifica delle foto. Questa caratteristica aumenta la credibilità del vostro sedcards e vi porterà molti clienti in più.
Avviare il processo  oggi e saperne di più scegliendo il profilo  escort che si desidera essere verific???  ";
$LNG['verify_choose_profile'] = "SCEGLI PROFILO ESCORT DA VERIFICARE";
$LNG['verify_unverified'] = "Non hai ancora verificato le escort.";
$LNG['ad_reg_confirm'] = "Grazie per la registrazione !<br /> Ti abbiamo mandato una mail con le istruzioni come puoi accedere al tuo account che hai appena creato. Per favore controlla la tua casella di posta elettronica. Se hai qualsiasi problema di confermare il tuo acount, contattaci al <a 
href='mailto:%sys_contact_email%'>%sys_contact_email%</a>. 
<br/><br/>%sys_site_title% team.";
$LNG['email_sig'] = "%sys_site_title% team.";
$LNG['atten_add_review'] = "";
$LNG['tour_rules_desc'] = "";
$LNG['more_escorts_by'] = "Piu' escort da";
$LNG['service_for'] = "servizi per";
$LNG['time_overnight'] = "per una notte";
$LNG['time_hours'] = "ore";
$LNG['time_minutes'] = "minuti";
$LNG['signout'] = "Esci";
$LNG['date_from'] = "Data dal";
$LNG['date_to'] = "data al";
$LNG['biography'] = "Biografia";
$LNG['dbl_click_to_set_base_city'] = "Fare doppio clic su seleziona città, per impostare città di riferimento.";
$LNG['no_tours_defined'] = "Nessun tour definito";
$LNG['edit_tours'] = "Modifica tour";
$LNG['tip'] = "TIP";
$LNG['double_click_to_main'] = "Doppio click per impostare l'immagine principale";
$LNG['drag_to_adjust'] = "Trascinare per regolare l'immagine";
$LNG['no_rates_defined'] = "Non ci sono tariffe definite";
$LNG['100_verification'] = "100% Verificato";
$LNG['edit_profile_data'] = "Modifica i dati del profilo";
$LNG['apartment'] = "Appartamento";
$LNG['incall_outcall'] = "Incall / Outcall";
$LNG['language_en'] = "Inglese";
$LNG['language_fr'] = "Francese";
$LNG['language_de'] = "Tedesco";
$LNG['language_it'] = "italiano";
$LNG['language_pt'] = "Portoghese";
$LNG['language_ru'] = "Russo";
$LNG['language_es'] = "spagnolo";
$LNG['language_gr'] = "Greco";
$LNG['congratulations'] = "Congratulazioni!";
$LNG['successfully_signed_up'] = "Hai completato la registrazione come membro del <a href='%SITE_URL%'>%SITE_NAME%</a> Grazie per la vostra scelta!";
$LNG['complete_your_registration'] = "NOTA: Per completare la registrazione controlla la tua email.";
$LNG['attention_2'] = "ATTENZIONE";
$LNG['account_succesfully_activated'] = "Il tuo account è stato attivato con successo";
$LNG['full_member'] = "Si è ora membro a pieno titolo del nostro sito. I dati di login sono
stati inviati alla tua email.";
$LNG['click_to_signin'] = "<a href='%LINK%'>Clicca qui</a> per accedere al tuo account.";
$LNG['lbs'] = "libra";
$LNG['status_active'] = "Attivo";
$LNG['edit_agency_profile'] = "Modifica profilo agenzia";
$LNG['edit_escorts'] = "Modifica escorts";
$LNG['upload_logo'] = "Carica il logo";
$LNG['location'] = "localizzazione";
$LNG['feedback_form_name'] = "Nome";
$LNG['feedback_form_email'] = "Il tuo indirizzo e-mail";
$LNG['feedback_form_message'] = "Messaggio";
$LNG['additional_city'] = "Altre città";
$LNG['city_premium_spot'] = "Citta' premium spot";
$LNG['girl_of_the_month'] = "Ragazza del mese";
$LNG['main_premium_spot'] = "Main premium spot";
$LNG['national_listing'] = "Lista nazionale";
$LNG['no_reviews'] = "Nessuna recensione";
$LNG['tour_ability'] = "Tour abilitato";
$LNG['tour_premium_spot'] = "Tour premium spot";
$LNG['agency_profile_updated'] = "Agenzia profilo aggiornato";
$LNG['escort_added_to_favorites'] = "Escort %showname% è stato aggiunto ai tuoi preferiti";
$LNG['escort_in_city'] = "escort in %CITY%";
$LNG['viewed_short'] = "Visto";
$LNG['escort_removed_from_favorites'] = "Escort %showname% è stato rimosso dalla lista dei preferiti.";
$LNG['more_than_years'] = "più di %NUMBER% anni";
$LNG['comment_is_short'] = "il testo è troppo corto ( min. lunghezza %LENGTH% )";
$LNG['no_comment'] = "Non vi è alcun commento su questa escort";
$LNG['already_comment_voted'] = "Hai già votato questo commento.";
$LNG['disabled'] = "Disattivato";
$LNG['captcha_is_wrong'] = "Testo sbagliato";
$LNG['additional_city_premium_spot_1_city'] = "Ulteriori Premium Spot City - 1 Citta'";
$LNG['additional_city_premium_spot_2_cities'] = "Ulteriori Premium Spot City - 2 Citta'";
$LNG['additional_city_premium_spot_3_cities'] = "Ulteriori Premium Spot City - 3 Citta'";
$LNG['add_comment'] = "Aggiungi un commento";
$LNG['sexindex_bottom_text'] = "Gemäss Statistik sind Begriffe die etwas mit Sex und Erotik zu tun haben die meist gesuchten im Internet. 98 Prozent der Männer waren gemäss Umfragen schon mindestens einmal auf Pornoseiten. Beliebt sind auch Suchbegriffe wie Kontaktanzeigen, Bordell, Sex Clubs, Escort, ficken, Transen etc. In der Schweiz gibt es eine Vielzahl an Sex Seiten und Sexführern. Und manchmal ist es schwierig den Ueberblick zu behalten. Denn wirklich gute Sex-Angebote, wo man auch das findet was man sucht, sind rar. Oder ist es Ihnen nicht auch schon so ergangen? Sie tippen bei einer Suchmaschine den Begriff „Bordell“ ein und landen, statt auf einem Sexführer, auf einer kostenpflichtigen Seite mit Kontaktanzeigen? Oder Sie haben Lust auf schnellen Sex und suchen ein Bordell in Ihrer Nähe wo Sie richtig geil ficken können? Statt dessen werden Sie auf eine Seite geführt wo Sie nur ein kleines Sexangebot haben und wenige Escort Girls / Sex Clubs aufgeführt sind. Wie gesagt – es ist ziemlich schwierig. Einige Seiten haben sich etabliert und decken gewisse Sparten ab. Für Transen Kontakte zum Beispiel eignet sich das Portal happysex hervorragend. Denn happysex hat in den meisten Regionen der Schweiz Kontaktanzeigen bzw. Sexanzeigen von Transen. Auch Bordelle, Escort Girls und Sex Clubs werden aufgeführt. Jedoch nimmt happysex im direkten Vergleich zu anderen Sex Seiten vom Design und Aufbau her einer der hinteren Plätze ein. Bei uns auf Sexindex legen wir wert auf Aktualität der Sex Kontakt Anzeigen und Sedcards der Escort Girls. Wir nehmen regelmässig neue Bordelle, Sexstudios, private Frauen, Escorts und Salons in unsere Datenbank auf. Alle Sedcards sind mit Fotos und Details und sind einheitlich präsentiert. Auch die Werbung versuchen wir dezent zu halten – jedoch brauchen wir die Banner um für Sie weiterhin ein gratis Sexführer zu bleiben. In diesem Sinne – ficken ist die schönste Nebensache der Welt – und ob Sie auf sexindex oder happysex fündig werden spielt schlussendlich keine Rolle. Happy hunting!";
$LNG['incall_rates'] = "Incall Prezzi";
$LNG['latest_viewed'] = "Ultime ragazze viste";
$LNG['outcall_rates'] = "Outcall Prezzi";
$LNG['signin_success'] = "Accesso con successo!";
$LNG['hits'] = "Hits";
$LNG['signup_for_free'] = "<p>Questa ed altre funzioni sono disponibili solo per utenti registrati</p>
<p>Registrati adesso gratis:</p>
";
$LNG['100_verified'] = "100% verificato";
$LNG['co_uk_bottom_text'] = "<strong>London Escort Guide</strong>è una directory escort Regno Unito per le femmine escorts, escort agencies, independent escorts, trans e coppies. On escortguide.co.uk  Potete trovare l'escort perfetta Londra o compagni di escort a Manchester, Leeds e ???    ?????? ? ?? ??1?( 1?  ???  ?  ???? ?    ?????   ?????1? ??????????????????????  ?????????????  ????W?(  ?(??? &  ??????????(  ??Ð  ?????? ???? ????????????????? ?Item  ??      ?Count  ???????  ??    ??????????????  ??????   ?   ??  ??? ?  ??????  ?W??  ?ò   ???  ??W???W?  ??? ?W?W  ???t ??? ? ? ??????    ???Eo  ??  ???????????????i  ???????????i  ????????????  ??????????????? ?? ??????? ????????????W ?";
$LNG['male_escorts'] = "Escort Uomo";
$LNG['tv_ts'] = "TV/TS";
$LNG['service_69_position'] = "69 posizione";
$LNG['service_anal_sex'] = "Sesso anale";
$LNG['service_cum_in_mouth'] = "Cum in bocca";
$LNG['service_cum_on_face'] = "Cum sulla faccia";
$LNG['service_dildo_play_toys'] = "Dildo Play/Toys";
$LNG['service_french_kissing'] = "French Kissing";
$LNG['service_girlfriend_experience_gfe'] = "Girlfriend Experience (GFE)";
$LNG['service_kissing'] = "Bacio";
$LNG['service_oral_with_condom'] = "Pompini con Preservativo";
$LNG['service_oral_without_condom'] = "Pompino senza preservativo";
$LNG['service_oral_without_condom_to_completion'] = "Pompino scoperto fino in fondo";
$LNG['service_sex_in_different_positions'] = "Sesso in posizioni diverse";
$LNG['service_ball_licking_and_sucking'] = "leccare e succhiare le palle";
$LNG['service_deep_throat'] = "Gola Profonda";
$LNG['service_dirtytalk'] = "Dirtytalk";
$LNG['service_double_penetration'] = "Doppia penetrazione (DP)";
$LNG['service_extraball'] = "Extraball";
$LNG['service_facesitting'] = "Seduta in faccia";
$LNG['service_fisting'] = "Fisting";
$LNG['service_full_body_sensual_massage'] = "massaggio sensuale su tutto il corpo";
$LNG['service_gangbang'] = "Gangbang";
$LNG['service_kamasutra'] = "Kamasutra";
$LNG['service_lesbian_sex_games'] = "Giochi di sesso Lesbiche";
$LNG['service_lingerie'] = "Lingerie";
$LNG['service_masturbate'] = "Masturbarsi";
$LNG['service_oral_without_condom_swallow'] = "pompino scoperto con ingoio";
$LNG['service_outdoor_sex'] = "Sesso all'aperto";
$LNG['service_pornstar_experience_pse'] = "Pornstar Esperienza (PSE)";
$LNG['service_private_photos'] = "Foto private";
$LNG['service_prostate_massage'] = "Massaggio alla prostata";
$LNG['service_rimming_give'] = "Rimming (dare)";
$LNG['service_rimming_recieve'] = "Rimming (ricevere)";
$LNG['service_snowballing'] = "Snowballing";
$LNG['service_striptease_lapdance'] = "Striptease/Lapdance";
$LNG['service_tantric'] = "Tantric";
$LNG['service_bdsm'] = "BDSM";
$LNG['service_bondage'] = "Bondage";
$LNG['service_clinic_sex'] = "Clinic Sex";
$LNG['service_fetish'] = "Fetish";
$LNG['service_foot_fetish'] = "Foot Fetish";
$LNG['service_golden_shower_give'] = "Pioggia dorata (dare)";
$LNG['service_golden_shower_recieve'] = "Pioggia dorata (ricevere)";
$LNG['service_lather_latex_pvc'] = "Leather/Latex/PVC";
$LNG['service_mistress_hard'] = "Mistress (hard)";
$LNG['service_mistress_soft'] = "Mistress (soft)";
$LNG['service_role_play_and_fantasy'] = "Gioco di Ruolo e Fantasy";
$LNG['service_spanking_give'] = "Sculacciata (dare)";
$LNG['service_spanking_recieve'] = "Sculacciata (ricevere)";
$LNG['service_strap_on'] = "Strap on";
$LNG['service_submissive_slave_hard'] = "Sottomesso / Slave (hard)";
$LNG['service_submissive_slave_soft'] = "Sottomesso / Slave (soft)";
$LNG['new_setcards_online'] = "%COUNT% nuovi annunci escort";
$LNG['use_our_contact_form'] = "In alternativa si prega di utilizzare il nostro modulo di contatto qui sotto";
$LNG['heterosexual'] = "Heterosexual";
$LNG['bisexual'] = "Bisexual";
$LNG['homosexual'] = "Homosexual";
$LNG['cup_size'] = "Coppa taglia";
$LNG['pubic_hair_shaved_completely'] = "Rasata completamente";
$LNG['pubic_hair_shaved_mostly'] = "per lo più rasata";
$LNG['pubic_hair_trimmed'] = "Assettato";
$LNG['pubic_hair_all_natural'] = "Tutto naturale";
$LNG['ocasionally'] = "Occasionalmente";
$LNG['drinking'] = "Bere";
$LNG['orientation'] = "Orientamento";
$LNG['dinner-date'] = "Data cena";
$LNG['phone_alternative'] = "Telefono alternativo";
$LNG['sms_only'] = "solo sms";
$LNG['phone_instr_no_withheld'] = "No numeri sconosciuti";
$LNG['phone_instructions_other'] = "Altre istruzioni del telefono";
$LNG['club_agency_name'] = "Club/Agenzia nome";
$LNG['street'] = "via";
$LNG['pubic_hair'] = "Peli pubici";
$LNG['hotel_room'] = "camera hotel";
$LNG['additional-hour'] = "ora addizionale";
$LNG['weekend'] = "fine settimana";
$LNG['club_studio'] = "Club/Studio";
$LNG['private_apartment'] = "Appartamento privato";
$LNG['hotel_visits_only'] = "solo visite hotel";
$LNG['home_visits_only'] = "solo visite a casa";
$LNG['hotel_and_home_visits'] = "visita casa e hotel";
$LNG['pv2_tab_biography'] = "biografia";
$LNG['pv2_top_bar_btn_back'] = "indietro";
$LNG['pv2_top_bar_btn_tours'] = "Tours";
$LNG['pv2_top_bar_btn_photos'] = "Foto";
$LNG['pv2_tab_about_me'] = "Su di me";
$LNG['pv2_tab_languages'] = "Lingue";
$LNG['pv2_tab_working_cities'] = "Citta' di lavoro";
$LNG['pv2_tab_services'] = "Servizi";
$LNG['pv2_tab_working_times'] = "Tempi di lavoro";
$LNG['pv2_tab_prices'] = "Prezzi";
$LNG['pv2_tab_contact_info'] = "Contact info";
$LNG['pv2_btn_save'] = "Salva";
$LNG['pv2_btn_reset'] = "Cancella";
$LNG['ethnic_mixed'] = "Misto";
$LNG['pv2_btn_settings'] = "Impostazioni";
$LNG['pv2_btn_add_edit_profile'] = "Aggiungi / Modifica profilo";
$LNG['pv2_btn_add_edit_photos'] = "Aggiungi / Modifica foto";
$LNG['pv2_btn_set_tours'] = "Set tours";
$LNG['pv2_btn_100p_verified'] = "100% verificato";
$LNG['pv2_btn_logout'] = "Logout";
$LNG['pv2_label_country'] = "Paese";
$LNG['pv2_label_city'] = "Citta'";
$LNG['pv2_settings_title'] = "Modifica profilo dati";
$LNG['pv2_text_change_metric_system_to_royal'] = "<strong>ATTENZIONE: </strong>Tutte le unità vengono visualizzate in sistema metrico. Se si desidera cambiarlo in Royal System, <a href='#' onclick='return changeSystem(2)'>clicca qui</a>";
$LNG['pv2_text_change_metric_system_to_metric'] = "<strong>ATTENZIONE:</strong>Tutte le unità sono visualizzati in Royal System. Se si desidera cambiarlo in sistema metrico, <a href='#' onclick='return changeSystem(1)'>clicca qui</a>";
$LNG['pv2_tip_showname'] = "Nome che apparirà sul sito sul tuo prfilo";
$LNG['pv2_tip_special_characteristics'] = "Si prega di citare tutti i particolari ad esempio caratteristiche tatuaggi, piercing, ecc";
$LNG['pv2_tip_about_me'] = "Descrivere se stessi e scrivere alcune informazioni aggiuntive";
$LNG['pv2_tip_country'] = "Se si sceglie un paese diverso allora %COUNTRY%, il tuo <br/>profilo internazionale sarà quotata EscortDirectory.com";
$LNG['pv2_note_max_cities'] = "<strong>Note: </strong> quando si offrono outcall puoi scegliere fino a %CITIES_COUNT% le città. Per impostare la propria città di riferimento è sufficiente fare doppio clic su di esso.";
$LNG['pv2_tip_available_for'] = "Si prega di indicare dove si offrono i propri servizi";
$LNG['pv2_label_incall_other'] = "Altro (si prega di fornire dettagli)";
$LNG['pv2_label_outcall_other'] = "Altro (si prega di fornire dettagli)";
$LNG['pv2_label_working_schedule'] = "Il mio orario di lavoro è il seguente";
$LNG['pv2_label_no_incall_rates'] = "Non hai ancora fissato i prezzi incall";
$LNG['pv2_label_no_outcall_rates'] = "Non hai ancora fissato i prezzi outcall";
$LNG['pv2_title_public_photos'] = "Foto pubbliche di%SHOWNAME%";
$LNG['pv2_text_public_photos'] = "Si prega di caricare almeno %MIN_PHOTOS% foto per la tua scheda. In caso contrario, il profilo non verrà approvato.";
$LNG['pv2_title_private_photos'] = "Foto private di %SHOWNAME%";
$LNG['pv2_text_private_photos'] = "È possibile caricare alcune foto private che possono essere visti solo dai membri premium del %APP_TITLE% e non per essere pubblicato sulla tua scheda.";
$LNG['pv2_escort_heading'] = "BENVENUTO %showname% nella tua area privata";
$LNG['pv2_escort_top_text'] = "Da quest'area è possibile gestire i profili di escort, impostare le vacanze e sarete sempre informati sulle novità e gli aggiornamenti";
$LNG['pv2_main_nav'] = "Navigazione principale";
$LNG['pv2_label_email'] = "Indirizzo e-mail";
$LNG['pv2_settings_profile'] = "Profilo";
$LNG['pv2_settings_change_password'] = "Cambia password";
$LNG['pv2_label_enter_current_password'] = "Immettere la password corrente";
$LNG['pv2_label_enter_new_password'] = "Inserire la nuova password";
$LNG['pv2_label_reenter_new_password'] = "Reinserire la nuova password";
$LNG['pv2_settings_newsletters'] = "Newsletter";
$LNG['pv2_settings_newsletters_question'] = "Vuoi ricevere la newsletter da %sitename%?";
$LNG['pv2_option_yes'] = "Si";
$LNG['pv2_option_no'] = "No";
$LNG['pv2_section_basic_bio'] = "Base Bio";
$LNG['pv2_label_showname'] = "Showname";
$LNG['pv2_label_gender'] = "Sesso";
$LNG['pv2_label_age'] = "Età";
$LNG['pv2_label_ethnicity'] = "Etnia";
$LNG['pv2_label_nationality'] = "Nazionalita'";
$LNG['pv2_section_vital_statistics'] = "Vital Statistics";
$LNG['pv2_label_hair_color'] = "Colore capelli";
$LNG['pv2_label_eye_color'] = "Colore occhi";
$LNG['pv2_label_height'] = "Altezza";
$LNG['pv2_label_weight'] = "Peso";
$LNG['pv2_label_dress_size'] = "Taglia vestito";
$LNG['pv2_label_shoe_size'] = "numero scarpa";
$LNG['pv2_label_bust_waist_hip'] = "Bust-vita-fianchi";
$LNG['pv2_label_cup_size'] = "Taglia coppa";
$LNG['pv2_label_pubic_hair'] = "Peli pubici";
$LNG['color_blond'] = "Bionda";
$LNG['color_light_brown'] = "nocciola";
$LNG['color_brunette'] = "mora";
$LNG['color_black'] = "Neri";
$LNG['color_red'] = "Rosso";
$LNG['color_brown'] = "castano";
$LNG['color_green'] = "verde";
$LNG['color_blue'] = "Blue";
$LNG['color_gray'] = "grigio";
$LNG['pv2_section_about_me'] = "Su di me";
$LNG['pv2_section_additional_information'] = "Ulteriori informazioni";
$LNG['pv2_label_smoking'] = "Fumatore";
$LNG['pv2_label_drinking'] = "Bere";
$LNG['pv2_label_special_characteristics'] = "Particolarità";
$LNG['pv2_section_spoken_languages'] = "Lingue";
$LNG['pv2_label_basic'] = "Di base";
$LNG['pv2_label_fair'] = "chiaro";
$LNG['pv2_label_good'] = "buono";
$LNG['pv2_label_excellent_native'] = "Eccellente / naturale";
$LNG['pv2_label_display_more_languages'] = "visualizzare più lingue";
$LNG['pv2_section_working_locations'] = "Sedi di lavoro";
$LNG['pv2_section_available_for'] = "Disponibile per";
$LNG['pv2_label_incall'] = "Incall";
$LNG['pv2_label_outcall'] = "Outcall";
$LNG['pv2_label_private_apartment'] = "Appartamento privato";
$LNG['pv2_label_hotel_room'] = "Camera hotel";
$LNG['pv2_label_club_studio'] = "Club / Studio";
$LNG['pv2_label_hotel_visits_only'] = "ricevo solo hotel";
$LNG['pv2_label_home_visits_only'] = "ricevo solo a casa";
$LNG['pv2_label_hotel_and_home_visits'] = "ricevo in casa e hotel";
$LNG['pv2_section_sexual_orientation'] = "Orientamento sessuale";
$LNG['pv2_section_services_offered_for'] = "Offro servizi per";
$LNG['pv2_tip_sexual_orientation'] = "";
$LNG['pv2_section_provided_services'] = "Fornisco servizi di";
$LNG['pv2_title_most_common_services'] = "La maggior parte dei servizi comuni";
$LNG['pv2_title_extra_services'] = "Servizi extra";
$LNG['pv2_title_fetish_bizzare_services'] = "Fetish / Bizzare";
$LNG['pv2_label_surcharge'] = "sovrapprezzo";
$LNG['pv2_label_i_am_available_24_7'] = "Sono disponibili 24 / 7";
$LNG['pv2_section_working_times'] = "Tempi di lavoro";
$LNG['pv2_label_or'] = "o";
$LNG['pv2_label_select_all'] = "selezionare tutti";
$LNG['pv2_label_from'] = "da";
$LNG['pv2_label_to'] = "a";
$LNG['pv2_section_rates'] = "prezzi";
$LNG['pv2_section_incall_rates'] = "prezzi rivevo";
$LNG['pv2_label_time'] = "tempo";
$LNG['pv2_label_unit'] = "unita'";
$LNG['pv2_label_amount'] = "Importo";
$LNG['pv2_label_currency'] = "Valuta";
$LNG['pv2_section_outcall_rates'] = "Outcall Prezzi";
$LNG['pv2_label_quick_options'] = "Opzioni rapide";
$LNG['pv2_label_overnight'] = "per una notte";
$LNG['pv2_label_any_additional_hour'] = "ogni ora aggiuntiva";
$LNG['pv2_label_dinner_date'] = "ora cena";
$LNG['pv2_label_weekend'] = "fine settimana";
$LNG['pv2_label_your_incall_rates'] = "i tuoi prezzi incall";
$LNG['pv2_label_your_outcall_rates'] = "i tuoi prezzi outcall";
$LNG['pv2_section_contact_information'] = "Informazioni di contatto";
$LNG['pv2_section_contact'] = "Contatto";
$LNG['pv2_label_phone_number'] = "numero telefonico";
$LNG['pv2_label_alternative_phone'] = "Telefono alternativo";
$LNG['pv2_label_phone_instructions'] = "istruzioni telefoniche";
$LNG['phone_instr_sms_call'] = "sms e chiamate";
$LNG['phone_instr_sms_only'] = "solo sms";
$LNG['phone_instr_no_sms'] = "no sms";
$LNG['pv2_label_no_withheld_numbers'] = "no sconosciuti";
$LNG['pv2_label_phone_instr_other'] = "altro";
$LNG['pv2_section_web'] = "web";
$LNG['pv2_label_email_address'] = "Indirizzo e-mail";
$LNG['pv2_label_website_url'] = "Website / URL";
$LNG['pv2_section_address_details'] = "dettagli indirizzo";
$LNG['pv2_label_club_name'] = "nome club";
$LNG['pv2_label_street_n'] = "via/nr";
$LNG['pv2_section_photos'] = "foto";
$LNG['pv2_section_my_tours'] = "miei tour";
$LNG['pv2_title_add_your_tours'] = "aggiungi il tour";
$LNG['pv2_label_view_profile'] = "guarda profilo";
$LNG['pv2_title_planned_active_tours'] = "Progetto/ tour attivi";
$LNG['pv2_label_range'] = "Range";
$LNG['pv2_label_location'] = "Posizione";
$LNG['pv2_label_contact'] = "Contattaci";
$LNG['pv2_label_from_date'] = "Dalla Data";
$LNG['pv2_label_until_date'] = "Fino Data";
$LNG['pv2_label_tour_phone'] = "Telefono tour";
$LNG['pv2_label_tour_email'] = "Email tour";
$LNG['pv2_agency_top_text'] = "Da quest'area è possibile gestire i profili di escort, impostare le vacanze o escursioni e sarete sempre informati sulle novità e gli aggiornamenti";
$LNG['pv2_agency_heading'] = "WELCOME %agency_name% Alla tua area privata";
$LNG['pv2_btn_add_escort_profile'] = "Aggiungi profilo Escort";
$LNG['pv2_btn_agency_profile'] = "Agenzia profilo";
$LNG['pv2_btn_manage_models'] = "Gestione di modelli";
$LNG['pv2_btn_city_tours'] = "City Tours";
$LNG['pv2_tab_finish'] = "Fine";
$LNG['pv2_btn_next_step'] = "Il passo successivo";
$LNG['pv2_btn_go_back'] = "Torna Indietro";
$LNG['pv2_label_zip_postal_code'] = "ZIP / Postal code";
$LNG['pv2_btn_finish'] = "Fine";
$LNG['pv2_btn_private_area'] = "Area Riservata";
$LNG['pv2_agency_create_profile_text'] = "<h2>Crea il tuo profilo in pochi passi semplici</h2>
<p>Basta inserire le informazioni necessarie nel passaggio dalla forma semplice passo. Tutte le informazioni possono essere modificate in qualsiasi momento.</p>";
$LNG['verify_form_confirmed'] = "Si prega di selezionare la forma che si desidera ottenere confermato la convocazione di riunione";
$LNG['verify_picture_upload'] = "Carica foto passaporto";
$LNG['pv2_section_manage_models'] = "Gestione di modelli";
$LNG['pv2_title_your_active_models'] = "I tuoi modelli Active";
$LNG['pv2_title_your_inactive_models'] = "I tuoi modelli inattivi";
$LNG['verify_number'] = "no.";
$LNG['pv2_member_heading'] = "WELCOME %username% Alla tua area privata";
$LNG['pv2_btn_member_profile'] = "Profilo Utente";
$LNG['pv2_label_your_username'] = "Il tuo nome utente";
$LNG['pv2_section_user_profile'] = "Profilo Utente";
$LNG['pv2_member_top_text'] = "";
$LNG['pv2_btn_my_favorites'] = "I miei preferiti";
$LNG['incall_services'] = "Incall";
$LNG['outcall_services'] = "Outcall";
$LNG['sms_and_call'] = "SMS e chiamate";
$LNG['pv2_escort_create_profile_text'] = "<h2>Crea il tuo profilo in pochi semplici passi</h2>
<p>Basta inserire le informazioni necessarie nel passaggio dalla forma semplice passo. Tutte le informazioni possono essere modificate in qualsiasi momento.</p>";
$LNG['languages_sa'] = "Arabo";
$LNG['languages_be'] = "Belga";
$LNG['languages_bg'] = "Bulgaro";
$LNG['languages_cn'] = "Cinese";
$LNG['languages_cz'] = "Ceca";
$LNG['languages_hr'] = "Croato";
$LNG['languages_nl'] = "Olandese";
$LNG['languages_fi'] = "Finlandese";
$LNG['languages_gr'] = "Greco";
$LNG['languages_hu'] = "Ungherese";
$LNG['languages_in'] = "Indiano";
$LNG['languages_jp'] = "Giapponese";
$LNG['languages_lv'] = "Lituana";
$LNG['languages_pl'] = "Polacco";
$LNG['languages_ro'] = "Rumeno";
$LNG['languages_tr'] = "Turco";
$LNG['language_tr'] = "Turco";
$LNG['no_sms'] = "No SMS";
$LNG['escort_profile_disabled'] = "Escort profilo è disattivato";
$LNG['pv2_weekday_monday'] = "Lunedi";
$LNG['pv2_weekday_tuesday'] = "Martedì";
$LNG['pv2_weekday_wednesday'] = "Mercoledì";
$LNG['pv2_weekday_thursday'] = "Giovedi";
$LNG['pv2_weekday_friday'] = "Venerdì";
$LNG['pv2_weekday_saturday'] = "Sabato";
$LNG['pv2_weekday_sunday'] = "Domenica";
$LNG['pv2_label_zip_code'] = "CAP";
$LNG['su2_choose_membership'] = "Scegli il tuo tipo di abbonamento";
$LNG['su2_thank_you_for_interest'] = "Grazie per l'interesse %sitename% ! Scegliere il tipo di sotto del tuo account e seguire semplicemente i passi successivi.";
$LNG['services_extra_charge'] = "supplemento";
$LNG['su2_escort_text'] = "<p class=\"text\">Looking for real good advertising?</p>
			<p>Signup now for FREE to the largest escort directory worldwide. We currently have over 4 MILLION CLIENTS monthly on our network. Start using our popularity to promote yourself now <span class=\"red\">FOR FREE!</span></p>

			<ul>
				<li>Easy sedcard setup</li>
				<li>Targeted users from our network</li>
				<li>Free signup</li>
			</ul>";
$LNG['su2_member_text'] = "<p class=\"text\">Sei alla ricerca di alcune ragazze hot?</p>
			<p>The signup on %sitename% is absolutely FREE and has many advantages.</p>

			<ul>
				<li>Add comments</li>
				<li>Manage favourite list</li>
				<li>Access to advanced search</li>
				<li>Watch list</li>
			</ul>";
$LNG['su2_please_fill_out_form'] = "Si prega di compilare il modulo sottostante e fare clic???Signup???";
$LNG['su2_choose_your_business'] = "Scegli il tuo business";
$LNG['su2_independent_escort'] = "Independent Escort / Girl privato";
$LNG['su2_agency_escort'] = "Escort Agenzia / Club";
$LNG['su2_username'] = "Username";
$LNG['su2_password'] = "Password";
$LNG['su2_confirm_password'] = "Conferma password";
$LNG['su2_email'] = "Email";
$LNG['su2_congratulations'] = "Congratulazioni - quasi fatta";
$LNG['su2_thank_you'] = "Grazie per esserti iscritto!";
$LNG['su2_email_sent_text'] = "Una e-mail è stata inviata al tuo indirizzo e-mail con un link di conferma. Per completare la registrazione, basta cliccare su quel link o copia / incolla nel tuo browser.";
$LNG['su2_note_text'] = "<span class=\"red\">Note:</span> A volte le e-mail da %sitename% fine nella cartella spam.";
$LNG['p_address'] = "Indirizzo";
$LNG['pv2_to_get_attention'] = "Per ottenere l'attenzione da più clienti e ad avere più successo in tempi di bassa stagione, %sitename% Offerte di impostare ogni due ore settimana felice fino a 5 ore!";
$LNG['pv2_happy_hour_guideline'] = "Felice guida ora";
$LNG['pv2_ul_agency_text_1'] = "È possibile impostare il prezzo happy hour e la data / ora nel modulo sottostante";
$LNG['pv2_ul_agency_text_2'] = "C'è un periodo massimo happy hour di 5 ore";
$LNG['pv2_ul_agency_text_3'] = "La funzione di happy hour può essere utilizzato solo due volte a settimana o per due ragazze a prescindere i periodi di tempo sono meno di 5 ore";
$LNG['pv2_hh_note_text'] = "NOTA: è possibile scegliere un massimo di 2 ragazze";
$LNG['pv2_label_motto'] = "Qual è il motto del vostro happy hour?";
$LNG['pv2_tip_motto'] = "se volete potete dare al vostro happy hour un motto (e.g. ???I??™m bored and horny??? or ???Best<br/> offer ever???). Scrivete quello che volete, che potrebbe aumentare l'attenzione del cliente.";
$LNG['pv2_label_date'] = "Data";
$LNG['pv2_label_time_from'] = "tempo da";
$LNG['pv2_label_time_until'] = "tempo fino a";
$LNG['pv2_label_save_hh'] = "Salva l'happy hour per ogni settimana";
$LNG['pv2_tip_save_hh'] = "Se si sceglie questa opzione, il sistema<br/> attivare le impostazioni per il felice<br/> ora automaticamente ogni settimana.";
$LNG['pv2_happy_hour_rates'] = "Prezzi happy hour";
$LNG['pv2_current_incall_rates'] = "Le vostre tariffe attuali sono incall";
$LNG['pv2_current_outcall_rates'] = "Le vostre tariffe attuali sono outcall";
$LNG['pv2_section_happy_hour'] = "Happy Hour";
$LNG['pv2_section_agency_happy_hour_step_1'] = "Passo 1: Selezionare la ragazza (s) per l'happy hour";
$LNG['pv2_section_agency_happy_hour_step_2'] = "Passo 2: Selezionare Happy Hour";
$LNG['pv2_section_escort_happy_hour_step_1'] = "Si prega di impostare il proprio happy hour";
$LNG['pv2_escort_to_get_attention'] = "Per ottenere l'attenzione da più clienti e ad avere più successo in tempi di bassa stagione, %sitename% Offerte di impostare ogni settimana un happy hour fino a 5 ore!";
$LNG['pv2_ul_escort_text_1'] = "È possibile impostare il prezzo happy hour e la data / ora nel modulo sottostante";
$LNG['pv2_ul_escort_text_2'] = "C'è un periodo massimo happy hour di 5 ore";
$LNG['pv2_ul_escort_text_3'] = "La funzione di happy hour può essere utilizzato solo una volta a settimana indipendentemente dal periodo di tempo è meno di 5 ore.";
$LNG['pv2_btn_happy_hour'] = "Happy Hour";
$LNG['profile_mention_text'] = "Si prega di citare %sitename% quando si chiama";
$LNG['sprt_open_tickets_list'] = "Ticket di supporto aperti sono elencati qui di seguito";
$LNG['sprt_closed_tickets_list'] = "Tutti i biglietti di sostegno chiusi sono elencati di seguito";
$LNG['sprt_no_opened_tickets'] = "Nessun tickets aperto";
$LNG['sprt_no_closed_tickets'] = "Nessun tickets chiuso";
$LNG['support'] = "Support";
$LNG['sprt_date'] = "Data";
$LNG['sprt_subject'] = "Oggetto";
$LNG['sprt_ticket'] = "Ticket";
$LNG['sprt_form_text'] = "Se sei in difficoltà con il vostro account personale sentitevi liberi di inserire un ticket di supporto nel nostro sistema.";
$LNG['sprt_your_message'] = "Il tuo messaggio";
$LNG['sprt_thank_you'] = "Grazie per il tuo messaggio.";
$LNG['sprt_ticket_number'] = "Il numero del ticket è: #%ticket_id%<br/>
(si prega di utilizzare questo numero come riferimento futuro)";
$LNG['sprt_usually_you_will'] = "Di solito si ottiene una risposta alla tua richiesta entro 24 ore<br/>
durante la settimana, nei week-end potrebbe richiedere più tempo.";
$LNG['sprt_replied_on'] = "%name% ha risposto il %date%";
$LNG['sprt_you'] = "tu";
$LNG['sprt_ticket_n'] = "Ticket Nr";
$LNG['sprt_reply_to_message'] = "Rispondi a questo messaggio";
$LNG['sprt_support_ticket'] = "Ticket";
$LNG['other_city_escorts'] = "Altri <a href='%city_url%'>%city_name% Escorts</a>";
$LNG['send_message_to'] = "Invia messaggio a%showname%";
$LNG['message_sent_to_escort'] = "Il tuo messaggio è stato inviato con successo per %showname%!";
$LNG['added'] = "ha aggiunto %date%";
$LNG['hour_rate'] = "%number% tariffa oraria";
$LNG['pv2_profile_was_saved'] = "Il tuo profilo è stato salvato con successo!";
$LNG['pv2_profile_needs_to_be_approved'] = "<strong>Note</strong> che il tuo profilo deve essere approvato dai nostri amministratori e che precede l'approvazione delle modifiche non verrà visualizzato sul sito.";
$LNG['pv2_add_now_photos'] = "Aggiungere ora le foto per questo profilo, clicca qui";
$LNG['pv2_up_browse_files'] = "Sfoglia file";
$LNG['pv2_up_clear_list'] = "Cancella elenco";
$LNG['pv2_up_start_upload'] = "Inizio caricamento";
$LNG['pv2_up_overall_progress'] = "Nell'insieme, i progressi";
$LNG['pv2_up_file_progress'] = "Progress File";
$LNG['visiting'] = "Visitare";
$LNG['status'] = "stato";
$LNG['this_girl_is_right_now_on_tour'] = "This girl is right now on tour";
$LNG['available_in'] = "Disponibile in";
$LNG['top20_colored'] = "La ragazza del<span style=\"color: #f00\">Mese</span>";
$LNG['top20_history'] = "Storia della ragazza del <span style=\"color: #f00\">Mese</span>";
$LNG['last_review'] = "%showname% Recensioni";
$LNG['currently'] = "Attualmente";
$LNG['latest_by'] = "Ultime da";
$LNG['french'] = "Orogine francese";
$LNG['whole_france'] = "Tutta la Francia";
$LNG['over_count_reviews'] = "Piu' %count% recensioni.";
$LNG['go_reviews_overview'] = "Vai alla pagina recensioni";
$LNG['go_escort_profile'] = "Vai al profilo di escort";
$LNG['about_the_meeting'] = "A proposito della riunione";
$LNG['she_provided_services'] = "Ha anche fornito questi servizi";
$LNG['signed_up'] = "firmato";
$LNG['member_reviews'] = "Commenti Utenti";
$LNG['top_count_ladies'] = "Top %count% Donne";
$LNG['last_reviewer'] = "Ultimo recensore";
$LNG['top_count_reviewers'] = "Top %count% Recensori";
$LNG['reviews_search'] = "Recensioni di ricerca";
$LNG['france_only'] = "Solo francia";
$LNG['only_100_fake_free_reviews'] = "Solo 100% fake free recensioni";
$LNG['pv2_btn_my_reviews'] = "Le mie recensioni";
$LNG['msg_no_review'] = "Non hai nessuna recensione.";
$LNG['pv2_section_escort'] = "Escort";
$LNG['pv2_label_date_of_the_meeting'] = "Data dell' incontro";
$LNG['pv2_label_city_of_the_meeting'] = "Città dell' incontro";
$LNG['pv2_label_other'] = "ALTRI";
$LNG['pv2_label_meeting_place'] = "Ritrovo";
$LNG['pv2_label_meeting_length'] = "Durata incontro";
$LNG['pv2_label_meeting_costs'] = "Costo incontro";
$LNG['pv2_label_rate_her_looks'] = "Vota il suo aspetto";
$LNG['pv2_label_rate_her_services'] = "Vota i suoi servizi";
$LNG['pv2_section_services'] = "Servizi";
$LNG['pv2_label_kissing'] = "Bacio";
$LNG['pv2_label_blowjob'] = "Pompini";
$LNG['pv2_label_cumshot'] = "Eiaculazione";
$LNG['pv2_label_69'] = "69";
$LNG['pv2_label_anal'] = "Anal";
$LNG['pv2_label_sex'] = "Sex";
$LNG['pv2_label_multiple_times_sex'] = "Multiple volte sesso";
$LNG['pv2_label_breast'] = "Seno";
$LNG['pv2_label_attitude'] = "Atteggiamento";
$LNG['pv2_label_conversation'] = "Conversazione";
$LNG['pv2_label_availability'] = "Disponibilità";
$LNG['pv2_label_photos'] = "Foto";
$LNG['pv2_label_services_comments'] = "Servizi commenti";
$LNG['pv2_section_review'] = "Recensione";
$LNG['pv2_section_get_trusted_review'] = "Prendi recensione di fiducia";
$LNG['pv2_label_trusted_review_example'] = "Prendi un 100% di fiducia recensione! Si prega di compilare i campi sottostanti quindi fuori!";
$LNG['pv2_label_t_your_info'] = "La tua info";
$LNG['pv2_label_t_user_info_example'] = "(e-mail, telefono, nome)";
$LNG['pv2_label_t_meeting_date'] = "Incontro data";
$LNG['pv2_label_t_meeting_date_example'] = "(data)";
$LNG['pv2_label_t_meeting_time'] = "tempo incontro";
$LNG['pv2_label_t_meeting_time_example'] = "(ora e minuti)";
$LNG['pv2_label_t_meeting_duration'] = "Durata incontro";
$LNG['pv2_label_t_meeting_duration_example'] = "(per esempio: 1.5) deve essere in ore";
$LNG['pv2_label_t_meeting_place'] = "Posto incontro";
$LNG['pv2_label_t_meeting_place_example'] = "(hotel, suo posto, tuo appartamento)";
$LNG['pv2_label_t_comments'] = "Commenti";
$LNG['pv2_label_t_comments_example'] = "(ulteriori info)";
$LNG['pv2_label_services_comments_example'] = "Descrivere la sua parte sessuale, per esempio i servizi speciali (S/M, toy, etc).";
$LNG['easy'] = "Facile";
$LNG['hard'] = "Hard";
$LNG['genuine'] = "Genuino";
$LNG['fake'] = "Fake";
$LNG['hard_to_belive'] = "Difficile da credere";
$LNG['at_my_flat'] = "A mio appartamento";
$LNG['at_her_apartament'] = "Al suo appartamento";
$LNG['her_hotel'] = "Il suo hotel";
$LNG['my_hotel'] = "Il mio hotel";
$LNG['pv2_label_slogan'] = "Slogan";
$LNG['pv2_tip_slogan'] = "Metti qui uno slogan o una parola chiave che descrive voi e / o <br/> il servizio migliore. Massimo 20 lettere.";
$LNG['user_ip_duplicity'] = "Membro duplicato";
$LNG['same_escort_and_ip'] = "escort duplicata";
$LNG['cookie_id_duplicity'] = "cookie duplicati";
$LNG['review_not_approved'] = "Non approvato";
$LNG['review_approved'] = "Approvato";
$LNG['review_disabled'] = "disabilitato";
$LNG['sms_not_sent'] = "Non inviate";
$LNG['sms_sent'] = "Inviati";
$LNG['sms_confirmed'] = "Confermato";
$LNG['sms_rejected'] = "Rifiutato";
$LNG['sms_mismatch'] = "Mismatch";
$LNG['more_days'] = "più giorni";
$LNG['i_am_right_now_on_tour'] = "Mi trovo ora in tour";
$LNG['i_am_coming_soon_to_tour'] = "Verrò presto in tour";
$LNG['review_sms_template'] = "Nuova recensione! %user_info% si è riunito in %date% at %meeting_place% for %duration% (%time%).Si prega di confermare con 'Sì ' o 'No' e includono %unique_number% nella risposta.";
$LNG['wt_24_7_text'] = "Sono disponibili 24 / 7";
$LNG['over_count_agencies'] = "di più %count% agenzie.";
$LNG['spnsors_6annonce'] = "Spnsors 6annonce.com";
$LNG['100_photos_verified'] = "100% foto verificato!";
$LNG['100_photos_verified_text'] = "Tutte le foto sono genuine - approvate e confermate dallo staff di EF.";
$LNG['suspicious_gallery'] = "Galleria sospetti!";
$LNG['suspicious_gallery_text'] = "Le foto fornite non sembrano reali e può essere falso.";
$LNG['pv2_btn_up_to_premium'] = "PREMIUM AGGIORNAMENTO";
$LNG['choose_your_card'] = "scegliere il metodo di pagamento";
$LNG['pay_credit_card'] = "pagamento con carta di credito";
$LNG['latest_status_messages'] = "Ultimi messaggi di stato";
$LNG['display'] = "Display";
$LNG['all_rev'] = "tutti";
$LNG['remember_me'] = "Ricordati di me";
$LNG['pv2_add_escort_tour'] = "Aggiungi tour escort";
$LNG['your_tour_schedule'] = "Il tuo calendario del tour";
$LNG['commented'] = "Commentate";
$LNG['rev_add_comment'] = "Aggiungi commento";
$LNG['pv2_btn_my_escorts_reviews'] = "Le mie recensioni escorts";
$LNG['see_all_reviews'] = "Vedi tutte le recensioni";
$LNG['cityzones'] = "Zone citta'";
$LNG['regions'] = "Regioni";
$LNG['slider_text'] = "Utilizzare il cursore di rango escort";
$LNG['forum'] = "Forum";
$LNG['cams'] = "Cams";
$LNG['chat'] = "Chat";
$LNG['pv2_label_home_city'] = "Home città";
$LNG['home_city'] = "Home città";
$LNG['save_me_the_login'] = "salvami il login";
$LNG['vip_m_title_1'] = "Accesso sezione riesame completo";
$LNG['vip_m_title_2'] = "Opzioni di ricerca avanzata";
$LNG['vip_m_title_3'] = "Nessuna pubblicità";
$LNG['vip_m_title_4'] = "Accesso alle foto private escort";
$LNG['vip_m_text_4'] = "Molti escort hanno fissato foto private. Essi si limitano a mostrare quelle foto a un pubblico selezionato. premio come membro di VIP si ha accesso a tutte le foto private di tutte le escort!";
$LNG['vip_m_title_5'] = "Accesso al forum premium";
$LNG['vip_m_text_5'] = "Come membro VIP del 6annonce.com avete il privilegio di accedere al forum VIP premio dedicato solo a te.";
$LNG['vip_m_title_6'] = "Icona Premium nel forum";
$LNG['vip_m_text_6'] = "Come membro premium si avrà una bandiera speciale VIP in aggiunta al vostro avatar personale.";
$LNG['vip_m_title_7'] = "La partecipazione a lotterie mensili";
$LNG['vip_m_text_7'] = "Ogni mese estraiamo il  fortunato membro premium che vince una scopata GRATIS con una ragazza dal nostro sito!*";
$LNG['vip_m_title_8'] = "VIP icona durante la scrittura di recensioni";
$LNG['vip_m_text_8'] = "Quando si scrive recensioni premio come membro VIP, le tue recensioni sono più attendibili e confermate più veloce. Inoltre il tuo nome utente è mostrato in colore dorato e contrassegnato con l'icona premio VIP.";
$LNG['vip_m_title_9'] = "Speciale modulo di contatto VIP";
$LNG['vip_m_text_9'] = "Membri premium hanno una forma speciale di contatto VIP per entrare in contatto con il personale della scorta per e-mail. il messaggio verrà contrassegnato speciali come 'messaggio VIP'.";
$LNG['vip_m_title_10'] = "Più credibilità con le ragazze";
$LNG['vip_m_text_10'] = "Come contattare una escort come membro di VIP ha il vantaggio che la scorta vede che il messaggio arrivato da un membro di un premio di fiducia VIP. Questo aumenta la credibilità e accelera il processo di come si sono privilegiati.";
$LNG['choose_your_membership'] = "Scegli la tua iscrizione!";
$LNG['vip_m_bottom_text'] = "<p>*Nota: questo valore è fino a 300 euro o la prenotazione di 1 ora, quindi se si sceglie un signore che costa 250 euro,</p>
		il loro non sarà payout del 50 euro, questo canot pirce essere cambiato in els contanti o nientee.</p>
		<p>Details about the procedure receives the winner.</p>";
$LNG['description'] = "Descrizione";
$LNG['normal_member'] = "Normale membri";
$LNG['premium_member'] = "Premium Member";
$LNG['euro'] = "euro";
$LNG['vip_m_page_title'] = "Membro di registrazione";
$LNG['vip_m_top_text'] = "<p>Grazie per il vostro interesse ad aderire Escort-Annonce.com!</p>
	<p>Sign up as premium member and get the <strong class='red'>CHANCE TO WIN A FREE FUCK WORTH 300 EURO!</strong></p>
	<p>*Choose the girl of your dreams and we will pay for YOUR DATE!</p???    ?????? ? ?? ??1?( 1?  ???  ?  ???? ?    ?????   ?????1? ??????????????????????";
$LNG['example'] = "Esempio";
$LNG['mouse_over_to_see_details'] = "Muovi il mouse sopra per vedere i dettagli";
$LNG['available_24_7'] = "Disponibile 24 / 7";
$LNG['pv2_remaining_cities'] = "<!-- Si prega di non toccare tag forte, è utilizzato da javascript -->
<strong>Attenzione: </strong>Si può scegliere <strong id='cities-remain'>%count%</strong> più città";
$LNG['p_not_approved'] = "non approvato";
$LNG['today_last_day'] = "Oggi ultimo giorno";
$LNG['bust-waist-hip'] = "Bust-vita-fianchi";
$LNG['see_my_sedcard'] = "Vedi la mia scheda";
$LNG['sign_in_bottom_text'] = "";
$LNG['girl_is_on_tour_in'] = "Ragazza è in tour in";
$LNG['share_mood'] = "Condividi il tuo stato d'animo di oggi, pensieri, sentimenti o desideri con la comunità!
Basta inserire qualcosa di personale nella finestra di messaggio immediato seguito - sarà
mostrano immediatamente sul tuo Sedcard e througout il sito e porta l'attenz???    ?????? ?";
$LNG['select_escort'] = "Seleziona Escort";
$LNG['instant_live_messages'] = "Instant Messages Live";
$LNG['your_premium_models'] = "I tuoi modelli premium";
$LNG['available_premium_packages'] = "Disponibile Pacchetti Premium";
$LNG['offering_wide_rage'] = "... offre una vasta gamma di pacchetti premium speciale.
Si prega di controllare e scegliere quello che si adatta alle vostre esigenze di più.
Se avete domande o se volete prenotare un pacchetto premium:";
$LNG['package'] = "Pacchetto";
$LNG['days_main_site'] = "Giorni Sito principale";
$LNG['city_tour_option'] = "City Tour Opzione";
$LNG['international_listing'] = "Lista internazionale";
$LNG['your_sales_representative'] = "Il vostro rappresentante di vendita";
$LNG['office_hours'] = "Ore d'ufficio";
$LNG['mon-fri'] = "Lun-Ven";
$LNG['from_date'] = "Dalla data";
$LNG['to_date'] = "alla data";
$LNG['no_tours'] = "Non ci sono tour definiti";
$LNG['delete_selected_tours'] = "Cancella Tour selezionati (s)";
$LNG['your_password_updated'] = "La password è stata aggiornata.";
$LNG['display_address_on_website'] = "Display indirizzo sul sito?";
$LNG['not_found'] = "non trovato";
$LNG['unrated'] = "prive di prezzi";
$LNG['use_slider_rank_escort'] = "Utilizzare il cursore di rango escort";
$LNG['pv2_start_process'] = "Avviare il processo di adesso!";
$LNG['today'] = "Oggi";
$LNG['pv2_alert_profile_is_not_approved'] = "Il profilo non è ancora approvato! Si prega di attendere l'approvazione da parte degli amministratori";
$LNG['pv2_alert_no_profile_yet'] = "Non hai ancora un profilo, fare clic <a href='%LINK%'>here</a>per aggiungere un nuovo profilo!";
$LNG['pv2_alert_profile_admin_disabled'] = "Il tuo profilo è disabilitato dai nostri amministratori!";
$LNG['pv2_alert_profile_not_enough_photos'] = "Non si dispone di abbastanza foto per andare on-line!";
$LNG['pv2_alert_package_info'] = "Attuale pacchetto è attivo '%PACKAGE_NAME%' - scade in  %DAYS% giorni";
$LNG['pv2_alert_ag_no_profile_yet'] = "Non hai ancora un profilo, fare clic <a href='%LINK%'>here</a> per aggiungere un nuovo profilo!";
$LNG['pv2_alert_ag_profile_admin_disabled'] = "il profilo è disattivato dai nostri amministratori!";
$LNG['pv2_alert_ag_is_not_approved'] = "profilo non è ancora approvato! Si prega di attendere l'approvazione da parte degli amministratori";
$LNG['pv2_alert_ag_profile_not_enough_photos'] = "Le foto non sono abbastanza per andare on-line!";
$LNG['pv2_btn_premium_girls'] = "Premium Models";
$LNG['pv2_alert_ag_owner_disabled'] = "Il profilo è disattivato dall' utente!";
$LNG['msg_escort_no_reviews'] = "Questo escort non ha Recensioni.";
$LNG['pv2_alert_one_per_day_limit'] = "Avviso: Hai il permesso di modificare il testo della bolla solo una volta al giorno!";
$LNG['middle_text'] = "6annonce.com est un site d'information et de publicit?© et n'a aucune connexion ou lien avec quelques sites que ce soient ou individus mentionn?©s ici. Nous sommes seulement un espace de publicit?©, nous ne sommes pas une agence d'escorte, ni m??me dans l???    ?????? ? ?? ????( ?à  ???  ?  ???? ?    ?????   ??????à ??????????????????????  ?????????????  ????W?(  ?(??? &  ?1??????1?(  ?1à  ????1? ??1? ????1???1????1??? ?Item  ??      ?Count  ????1??  ?1    ??????????";
$LNG['new_domain_text'] = "EscortForum.net è un sistema di ricerca di informazioni e di annunci pubblicitari e non ha nessuna connessione o nessuna responsabilità con qualsiasi sito o informazioni individuali menzionati qui. Noi vendiamo SOLAMENTE spazi pubblicitari, non siamo nè una agenzia di escort, nè siamo in alcun modo coinvolti in affari con le accompagnatrici e la prostituzione. Non ci assumiamo nessuna responsabilità per le azioni svolte da siti terzi o individuali, ai quali si ha accesso tramite links, e-mail o telefono presi all'interno di questo portale.";
$LNG['escorte_a'] = "Escort ? ";
$LNG['pv2_edit_escort_tour'] = "Modifica Tour";
$LNG['disable_my_profile'] = "Disabilitare il mio profilo";
$LNG['enable_my_profile'] = "Attiva il mio profilo";
$LNG['delete'] = "Elimina";
$LNG['pv2_label_secure_text'] = "Testo sicuro";
$LNG['wrong_secure_text'] = "Sbagliato testo sicuro";
$LNG['open_now'] = "Adesso aperto";
$LNG['available_now'] = "disponibile ora";
$LNG['hh_top_text'] = "%showname% Offerte Ora HAPPY HOUR prezzi da %date_from%%date_from_a% fino a %date_to%%date_to_a%!";
$LNG['save'] = "Salva";
$LNG['hh_add_hour'] = "Aggiungi. ora";
$LNG['hh_rates'] = "Tariffe happy hour";
$LNG['hh_please_add_some_rates'] = "Si prega di aggiungere alcuni prezzi al tuo profilo, prima di \"Happy Hours\"!";
$LNG['hh_what_is_motto'] = "Qual è il motto del vostro happy hour?";
$LNG['hh_info_motto'] = "se volete potete dare il vostro happy hour un motto (ad esempio '\"io sono così annoiato'o 'migliore offerta mai! '). Scrivere quello che ti piace e che potrebbe salire l'attenzione al cliente.
";
$LNG['hh_info_save'] = "Se si sceglie questa opzione, il sistema attiva le impostazioni per l'happy hour automaticamente ogni settimana.";
$LNG['hh_save_label'] = "Salva l'happy hour per ogni settimana";
$LNG['hh_set_your_hh'] = "Si prega di impostare il proprio happy hour";
$LNG['hh_happy_hour_rates'] = "Tariffe happy hour";
$LNG['hh_your_current_outcall_rates_are'] = "Le vostre tariffe attuali sono outcall";
$LNG['hh_your_current_incall_rates_are'] = "Le vostre tariffe attuali sono incall";
$LNG['hh_max_2_escort'] = "Nota: è ??possibile scegliere un massimo di 2 ragazze";
$LNG['hh_please_select_escort'] = "Passo 1: Selezionare i punti per l'happy hour";
$LNG['hh_agency_escorts'] = "Agenzia escorts";
$LNG['hh_gray_top_text'] = "Per ottenere l'attenzione da più clienti e ad avere più successo in tempi di bassa stagione, il nostro sito vi propone di impostare ogni settimana un happy hour fino a 5 ore!
";
$LNG['hh_happy_hour_guideline'] = "Happy hour guida breve";
$LNG['hh_list_txt_1'] = "È possibile impostare il prezzo happy hour e la data / ora nel modulo sottostante";
$LNG['hh_list_txt_2'] = "C'è un periodo massimo happy hour di 5 ore";
$LNG['hh_list_txt_3'] = "La funzione di happy hour può essere utilizzato solo una volta a settimana indipendentemente dal periodo di tempo è meno di 5 ore";
$LNG['week_day'] = "Giorno settimana";
$LNG['hour_from'] = "Ora da";
$LNG['hour_until'] = "Ora fino alle";
$LNG['latest_comments'] = "Ultimi commenti";
$LNG['advanced_bio'] = "ADVANCED (BIO)";
$LNG['order_direction'] = "Ordine Direzione";
$LNG['free_text'] = "Testo libero";
$LNG['exact_match'] = "corrispondenza esatta";
$LNG['and_or'] = "e / o";
$LNG['ASC'] = "Asc";
$LNG['DESC'] = "Desc";
$LNG['hh_status_expired'] = "Questo \"Happy Hour\" è scaduto. Si può riattivarsi dopo %DAYS% giorni!";
$LNG['hh_status_active'] = "Questo \"Happy Hour\" è ora attivo!";
$LNG['hh_you_have_active_hh'] = "Questa escort è \"Happy Hour\" definito. Si prega di fare attenzione se si modifica comunque la \"Happy Hour\" verrà automaticamente rimossa!";
$LNG['att_photos_count'] = "Attenzione: il numero minimo di foto del profilo è 3. Il tuo profilo non sarà online, se hai meno di 3 foto.";
$LNG['lc_latest_comments'] = "Ultimi commenti";
$LNG['lc_posted_by'] = "Inviato da";
$LNG['lc_comments'] = "commenti";
$LNG['lc_read_all_my_comments'] = "leggere tutti i miei commenti";
$LNG['verify_idcard_session'] = "IDcard sessione";
$LNG['verify_request_confirmed'] = "100% chiede una verifica di processo è confermato!";
$LNG['hh_internal_text'] = "<span class=\"red\">News</span>: Hai già provato il nostro strumento unico chiamato \"Happy Hour \"? Questa caratteristica è stata creata per aiutarti a attirare più clienti durante i periodi bassi. Se si
Si noti che per esempio Domenica pomeriggio è sempre
m???    ?????? ? ?? ????( ??  ???  ?  ???? ?    ?????   ??????? ??????????????????????  ?????????????  ????W?(  ?(??? &  ??????????(  ??à  ?????? ???? ????????????????? ?Item  ??      ?Count  ???????  ??    ??????????????  ??????   ?   ??  ??? ?  ??????  ?W??  ?ò   ???  ??W???W?  ??? ?W?W  ???t ??? ? ? ??????    ???Eo  ??  ???????????????i  ???????????i  ????????????  ??????????????? ?? ??????? ????????????W ?   ?????    ?? ????";
$LNG['hh_escorts'] = "Happy Hour Escorts";
$LNG['suspicious_internal_text'] = "<span class=\"red\">Attention</span>: 6annonce.com si preoccupa di evitare immagini in falsi
i profili elencati. Purtroppo le foto sono
stato contrassegnato come sospetto da uno dei nostri amministratori del sito.

A causa di questo motivo il vostro Sedcard???    ?????? ? ?? ????( ??  ???  ?  ???? ?    ?????   ??????? ??????????????????????  ?????????????  ????W?(  ?(??? &  ??????????(  ??à  ?????? ???? ????????????????? ?Item  ??      ?Count  ???????  ??    ??????????????  ??????   ?   ??  ??? ?  ??????  ?W??  ?ò   ???  ??W???W?  ??? ?W?W  ???t ??? ? ? ??????    ???Eo  ??  ???????????????i  ???????????i  ????????????  ??????????????? ?? ??????? ??????";
$LNG['no_hh_text'] = "Oops ... attualmente ci sono <span class=\"r\">no happy hour escorts...</span> Si prega di controllare più tardi.";
$LNG['suspicious_internal_text_agency'] = "<span class=\"red\">Attention:</span> 6annonce.com si preoccupa di evitare immagini in falsi
i profili elencati. Purtroppo %shownames% Le foto sono
stato contrassegnato come sospetto da uno dei nostri amministratori del sito.
A causa di questo motivo %shown???    ?????? ? ?? ??1?( 1?  ???  ?  ???? ?    ?????   ?????1? ??????????????????????  ?????????????  ????W?(  ?(??? &  ?1??????1?(  ?1Ð  ????1? ??1? ????1???1????1??? ?Item  ??      ?Count  ????1??  ?1    ??????????????  ??????   ?   ??  ??? ?  ??????  ?W??  ?ò   ???  ??W???W?  ??? ?W?W  ???t ??? ? ? ??????    ???Eo  ??  ???????????????i  ???????????i  ????????????  ??????????????? ?? ??????? ????????????W ?   ??";
$LNG['more'] = "Più";
$LNG['more_escorts_in_whole_france'] = "Più di %NUMBER% escorts in tutta la Francia";
$LNG['search_your_city'] = "Cerca la tua città";
$LNG['back_to_top'] = "Torna in alto";
$LNG['no_services_found'] = "Nessun servizio trovato";
$LNG['no_tours_found'] = "Nessun tour trovato";
$LNG['no_rates_found'] = "Non ci sono tariffe";
$LNG['pv2_btn_client_blacklist'] = "Client blacklist";
$LNG['favorites'] = "Preferiti";
$LNG['bl_top_text_1'] = "A volte stiamo ottenendo riscontri da accompagnatori che si lamentano di rude, i clienti ubriachi o aggressiva - o clienti, che non vogliono pagare o stanno rubando i soldi. Pertanto abbiamo deciso di creare una lista nera client, che è accessibile a tutt???    ?????? ? ?? ??1?( 1?  ???  ?  ???? ?    ?????   ?????1? ??????????????????????  ?????????????  ????W?(  ?(??? &  ???????";
$LNG['bl_top_text_2'] = "Se ti hanno fatto brutte esperienze di ogni tipo, aiutare gli altri ad evitare di incontrare i clienti cattivi e aggiungere quante più informazioni possibili alla lista nera.";
$LNG['pv2_title_client_blacklist'] = "Client Blacklist";
$LNG['bl_make_entry'] = "Fai un' accesso - clicca qui";
$LNG['bl_search_input_label'] = "Ricerca dopo che il numero di telefono, nome o testo libero";
$LNG['client_name'] = "Nome cliente";
$LNG['client_phone_number'] = "Client Numero di telefono";
$LNG['experience_comment'] = "Esperienza / Commenta";
$LNG['phone_number'] = "Numero di telefono";
$LNG['full_site'] = "Sito completo";
$LNG['answer'] = "Risposta";
$LNG['question'] = "Domanda";
$LNG['m_popup_top_text'] = "Per vostra comodità vi offriamo ora una versione mobile del nostro sito!";
$LNG['m_popup_bottom_text'] = "<ul>
	<li>Use it with <span>any</span> mobile device</li>
	<li>Use it from anywhere in the world <span>without computer</span></li>
	<li>All import functions available</li>
	<li>Direct access, <span>no download needed</span></li>
	<li>Free to use</li>
</u???    ?????? ? ?? ????( ?Ð  ???  ?  ???? ?    ?????   ?????";
$LNG['vote_me_title'] = "Competizione ragazza del mese";
$LNG['vote_me_text'] = "Votate me per la ragazza del mese";
$LNG['vote_me_button'] = "clicca qui - grazie";
$LNG['su2_banner_text'] = "Si prega di essere consapevole del fatto che per poter essere elencati sul nostro sito si
necessario inserire uno dei banner follwing in posizione superiore
nella tua home page.";
$LNG['su2_banner_text2'] = "Basta copiare / incollare il codice html:";
$LNG['online_chat'] = "Online Chat";
$LNG['chat_text_1'] = "<span class=\"red\">NEW:</span> Chat 6annonce.com funzione ti offre ora la possibilità di chattare online con gli utenti / clienti! Molte opzioni disponibili come: private chat, messaggi personali (PM), messaggio sussurro, ignorare ecc!";
$LNG['chat_text_2'] = "Quando si fa clic sull'icona sottostante, sarà indicato sul vostro Sedcard che sei disponibile per la live chat. Inoltre si ottiene elencate in un menu a parte e si otterrà una maggiore attenzione da parte dei clienti - significa più posti di lavoro!";
$LNG['chat_member_text'] = "<span class=\"red\">NEW:</span> Chat 6annonce.com funzione ti offre ora la possibilità di chattare online con accompagnatori! Numerose opzioni disponibili quali: chat privata, i messaggi personali (PM), messaggio sussurro, ignorare ecc!";
$LNG['tomorrow'] = "domani";
$LNG['pv2_title_your_deleted_models'] = "I tuoi modelli eliminati";
$LNG['restore'] = "Ripristino";
$LNG['pv2_label_i_am_night_escort'] = "Mostrami come la notte Escort";
$LNG['night_escort_help'] = "Il tuo profilo sarà evidenziata con un adesivo Disponibile ora tra mezzanotte e le 5 del mattino. <br>NOTE: Si deve garantire di essere a disposizione in quel periodo.<br>6annonce.com rende casualmente telefono chiamate di prova per dimostrare disponibili???";
$LNG['profile_not_active'] = "This profile is currently not active (by Zero Package)";
$LNG['italian'] = "Italiana nativa";
