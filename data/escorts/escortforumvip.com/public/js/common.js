function openPopup(url) {
	new_win = window.open(url, 'my_popup', 'top=30,left=100,width=680,height=677,buttons=no,scrollbars=no,location=no,menubar=no,resizable=no,status=no,directories=no,toolbar=no');
	new_win.focus();
}



function resizeWindow() {
    var img = document.getElementById("image");
    var w, h;

    if (isNaN(window.innerHeight)) {
        h = document.body.clientHeight;
        w = document.body.clientWidth;
    }
	else {
        h = window.innerHeight;
        w = window.innerWidth;
	}
    if (img.width <= 260) {
        window.resizeBy(260 - w + 46, img.height - h + 83);
    }
    else {
        window.resizeBy(img.width - w + 46, img.height - h + 83);
    }
}



function showPhoto(photo) {
    document.getElementById('photo').innerHTML = '<img src=\"' + photo + '\" alt=\"\">';
}
