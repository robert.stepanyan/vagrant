<?php

$GLOBALS['SQLS'] = array();

require '../../library/Cubix/Debug.php';
require '../../library/Cubix/Debug/Benchmark.php';

if ( isset($_REQUEST['benchmark']) ) Cubix_Debug::setDebug(true);
Cubix_Debug_Benchmark::setLogType(Cubix_Debug_Benchmark::LOG_TYPE_DB);
Cubix_Debug_Benchmark::setApplication('frontend');
Cubix_Debug_Benchmark::start($_SERVER['REQUEST_URI']);

set_time_limit(0);
//session_start();
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

if ( preg_match('#.dev$#', $_SERVER['SERVER_NAME']) ) {
	define('APPLICATION_ENV', 'development');
}

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../../library'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

$application->bootstrap()
            ->run();

Cubix_Debug_Benchmark::end($_SERVER['REQUEST_URI']);

if ( Cubix_Debug::isDebug() ) {
	Cubix_Debug_Benchmark::log();
}
