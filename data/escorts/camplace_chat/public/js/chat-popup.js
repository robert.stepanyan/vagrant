function ChatPopup(sessionId, participantId, uHash) {
	
	var socket,
		selfUser = {},
		browser = Browser.name,
		browserVersion = Browser.version,
		windowFocused,
		reconnectionAttempts = 0;
		reconnectOnDisconnect = false;
	
	var monthNames = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
	
	var messages = {
		'offlineMessage' : 'This user is offline. They will receive the message next login',
		'blackListedWordsMessage' : 'You can`t use words "%words%"',
		'pmAllow' : 'This message cannot be sent due to privacy restrictions'
	};
	
	var customEmotions = {
		'>:o'	: 'upset',
		'O:)'	: 'angel',
		'3:)'	: 'devil',
		'>:('	: 'grumpy'
	};
	var emotions = {
		':)'	: 'smile',
		':('	: 'frown',
		':P'	: 'tongue',
		':D'	: 'grin',
		':*'	: 'kiss',
		':o'	: 'gasp',
		';)'	: 'wink',
		':v'	: 'pacman',
		':/'	: 'unsure',
		':\'('	: 'cry',
		'^_^'	: 'kiki',
		'8)'	: 'glasses',
		'B|)'	: 'sunglasses',
		'<3'	: 'heart',
		'-_-'	: 'squint',
		'o.O'	: 'confused',
		':3'	: 'colonthree'
	};
	
	
	var originalTitle = document.title;
	var blinkTimer;
	
	this.connect = function()
	{
		if ( typeof socket == 'undefined' ) {
			socket = io.connect('http://vid2.pluginnetwork.com:8888', {
				'reconnect': true
				, 'reconnection delay': 500
				, 'reconnection limit': 30000
				, 'max reconnection attempts': 10000
			});
		} else {
			socket.socket.connect();
		}
	}
	
	this.initListeners = function()
	{
		var self = this;
		
		socket.on('auth-complete', function(data) {
			if ( data.auth ) {
				if ( data.availability ) {
					selfUser = data.userInfo;
					prepareSettings();
					socket.emit('message-history', {userId : participantId});
				} else {
					
				}
			} else {
				
			}
			
			//Init events only on first connection
			if ( reconnectionAttempts <= 1 ) {
				self.initEvents();
				setInterval(function() {
					socket.emit('ping', {name : selfUser.nickName});
				}, 5000);
			}
		});
		
		var r = 0;
		socket.on('reconnect', function() {
			console.log('need to reconnect');
			r++;
			if ( r > 5 ) {
				r = 0;
				socket.disconnect();
				self.connect();
			}
		});
		
		socket.on('connect', function() {
			if ( ! sessionId ) {
				return;
			}
			
			socket.emit('auth', {
				sid: sessionId,
				uHash: uHash
			});
			
			reconnectionAttempts += 1;
		});
		
		socket.on('message-history', function(data) {
			fillHistory(data.senderInfo, data.messages);
		});
		
		socket.on('new-message', function(data) {
			if ( data.senderData.userId == participantId ) {
				addMessage(data.senderData, data.message);
				blinkPageTitle('New message from ' + data.senderData.nickName);
				playAudio('new-message');
			}
			
			//If not self message emit message-read
			if ( windowFocused ) {
				socket.emit('conversation-read', {
					userId : participantId
				});
			}
			
		});
		
		socket.on('settings-sound-changed', function(data) {
			selfUser.settings.sound = data.value;
		});
		
		socket.on('logout', function() {
			reconnectOnDisconnect = false;
		});
	}
	
	this.initEvents = function()
	{
		addDialogCookies(participantId);
		
		var typing = false,
		timer;
		document.id('ChatEntry').addEvent('keypress', function(e) {
			/*--> Detect typing start/end */
			if ( ! typing ) {
				emitTypingStart(participantId);
				typing = true;
			}
			
			clearTimeout(timer);
			timer = setTimeout(function(){ 
				emitTypingEnd(participantId);
				typing = false;
			}, 3*1000);
			/*<-- Detect typing start/end */
			
			if ( e.code == 13 && ! e.shift ) {
				e.stop();
				
				message = this.get('value');
				
				message = Plugin.Data.ReplaceEmoticons(message, true);
				
				socket.emit('message-sent', {
					userId : participantId,
					message: message
				});
				
				//After enter empty text area and set default height
				this.set('value', '');
				
				message = {
					body : message,
					date : new Date().getTime(),
					userId : selfUser.userId
				}
				
				addMessage(selfUser, message);
			}
		});
		
		window.onunload = function (e) {
			removeDialogCookies(participantId);
		};
		
		
		window.onfocus = function() {
			windowFocused = true;
			
			socket.emit('conversation-read', {
				userId : participantId
			});
		}
		window.onblur = function() {
			windowFocused = false;
		}
		
		document.getElement('.emotions_btn').addEvent('click', function() {
			if ( this.hasClass('opened') ) {
				hideEmotionsTab();
			} else {
				openEmotionsTab();
			}
		});
	}
	
	this.tipModel = function(userId, amount)
	{
		socket.emit('tip-model', {
			userId : userId,
			amount : amount
		});
		
		/*var msg = 'You tipped ' + onlineUsersObj[userId].nickName + ' ' + amount + ' tokens';
		showMessage(userId, msg);*/
	}
	
	this.trackConnection = function()
	{
		var self = this;
		setInterval(function() {
			if ( ! socket.socket.connected && reconnectOnDisconnect ) {
				console.log('No connection');
				socket.disconnect();
				self.connect();
			}
		}, 10000);
	}
	
	fillHistory = function(senderInfo, messages)
	{
		$$('.chat-messages-box .inner').empty();
		messages.each(function(message) {
			addMessage(senderInfo, message)
		});
	}
	
	addMessage = function(senderInfo, message)
	{
		messagesContainer = $$('.chat-messages-box .inner')[0];
		
		//first of all clearing message
		message.body = clearMessage(message.body);
		
		if ( message.body.length == 0 ) return;
		
		lastUserMessage = messagesContainer.getElements('.chat-message-item').getLast();
		
		
		//if messaging is empty or last message is not his, create new user_message raw
		//else add to existing one
		if ( lastUserMessage && ( lastUserMessage.hasClass(message.userId) && message.date - parseInt(lastUserMessage.get('id')) <= 60*1000 ) ) {
			new Element('p', {
				'html': message.body
			}).inject(lastUserMessage);
		} else {
			if ( message.userId == selfUser.userId ) {
				userMessage = getMessageRow(selfUser);
			} else {
				userMessage = getMessageRow(senderInfo);
			}
			
			new Element('span', {
				'class' : 'message-time',
				'html' : getTimeForMessage(new Date(message.date))
			}).inject(userMessage);
			userMessage.set('id', message.date);
			
			new Element('p', {
				'html': message.body
			}).inject(userMessage);

			userMessage.inject(messagesContainer);
		}


		//scroll down messaging
		messagesContainer.scrollTo(0, messagesContainer.getScrollSize().y);
	}
	
	getMessageRow = function(userInfo)
	{
		var chatMessageItem = new Element('div', {
			'class' : 'chat-message-item ' + userInfo.userId
		})
		new Element('span', {
			'class' : userInfo.userId == selfUser.userId ? 'model-chat-name' : 'user-chat-name',
			'html'	: userInfo.userId == selfUser.userId ? 'Me' : userInfo.nickName
		}).inject(chatMessageItem);
		
		return chatMessageItem;
	}
	
	clearMessage = function(message)
	{
		//htmlentities
		message = htmlEntities(message);
		
		//Replacing one more whitespaces
		message = message.trim();
		message = message.replace(/\s+/g, ' ');
		
		//replacing urls with html links
		var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
		message = message.replace(exp,"<a href='$1' target='_blank'>$1</a>");
		
		message = replaceEmotions(message);
		
		return message;
	}
	
	htmlEntities = function(str) {
		return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
	}
	
	replaceEmotions = function(message)
	{
		var exp = /(\/handle\/emoticons.ashx\?icon=[0-9]+.(gif|jpg|png))/ig;
		message = message.replace(exp,"<img src='$1'/>");
		
		//First replacing custom emotions
		/*for ( i in customEmotions) {
			
			//For smiles cleaned by htmlentities
			while(message.indexOf(htmlEntities(i)) != -1) {
				message = message.replace(htmlEntities(i), '<span class="emotions emotions_' + customEmotions[i] + '"></span>');
			}
			
			while( message.indexOf(i) != -1 ) {
				message = message.replace(i, '<span class="emotions emotions_' + customEmotions[i] + '"></span>');
			}
		}
		for ( i in emotions ) {
			//small fix for. for this smile :/ (http://)
			if ( i == ':/' && (message.indexOf('http://') != -1 || message.indexOf('https://') != -1) ) {
				continue;
			}
			
			while(message.indexOf(i) != -1) {
				message = message.replace(i, '<span class="emotions emotions_' + emotions[i] + '"></span>');
			}
			
			//For smiles cleaned by htmlentities
			while(message.indexOf(htmlEntities(i)) != -1) {
				message = message.replace(htmlEntities(i), '<span class="emotions emotions_' + emotions[i] + '"></span>');
			}
		}*/
		return message;
	}
	
	getTimeForMessage = function(date)
	{
		curDate = new Date();
		
		hours = date.getHours();
		minutes = ('0' + date.getMinutes()).slice(-2);//two number format
		
		d = hours + ':' + minutes;
		
		if ( curDate.getDate() != date.getDate() ) {
			d += ' ' + date.getDate() + 'th ' + monthNames[date.getMonth()];
		}
		
		if ( curDate.getYear() != date.getYear() ) {
			d += ' ' + date.getYear();
		}
		
		return d;
	}
	
	emitTypingStart = function(userId)
	{
		socket.emit('typing-start', {
			userId : userId
		});
	}
	emitTypingEnd = function(userId)
	{
		socket.emit('typing-end', {
			userId : userId
		});
	}
	
	//Adding dialogId to the cookie named chatPopups
	addDialogCookies = function(userId)
	{
		cook = Cookie.read('chatPopups');
		if ( cook ) {
			cook = JSON.parse(cook);
		} else {
			cook = new Object();
		}
		
		if ( cook[userId] ) {
			return;
		}
		
		cook[userId] = {};
		
		Cookie.write('chatPopups', JSON.stringify(cook), 0);
		
	}
	
	//Removing dialogId from the cookie named chatPopups
	removeDialogCookies = function(userId)
	{
		cook = Cookie.read('chatPopups');
		if ( cook ) {
			cook = JSON.parse(cook);
		} else {
			cook = new Object();
		}
		
		
		delete cook[userId];
		
		Cookie.write('chatPopups', JSON.stringify(cook));
	}
	
	prepareSettings = function()
	{
		if ( typeof selfUser.settings == 'undefined' ) selfUser.settings = {};
	}
	
	blinkPageTitle = function(message)
	{
		var originalTitle = window.document.title;
		
		if ( ! originalTitle.length ) return;
		if ( typeof blinkTimer != 'undefined' ) clearInterval(blinkTimer);
		blinkTimer = setInterval(function() {
			if ( window.document.title == message ) {
				window.document.title = originalTitle;
			} else {
				window.document.title = message;
			}
		}, 1000);
		window.addEvent('focus', function() {
			if ( typeof blinkTimer != 'undefined' ) clearInterval(blinkTimer);
			document.title = originalTitle;
			this.removeEvents('click');
		});
	}
	
	initAudio = function()
	{
		if ( "Audio" in window && browser.indexOf('safari') == -1 ){
			audio = new Audio();
			if( !!(audio.canPlayType && audio.canPlayType('audio/ogg; codecs="vorbis"').replace(/no/, '')) )
				audio.ext = 'ogg';
			else if( !!(audio.canPlayType && audio.canPlayType('audio/mpeg;').replace(/no/, '')) )
				audio.ext = 'mp3';
			else if( !!(audio.canPlayType && audio.canPlayType('audio/mp4; codecs="mp4audio.40.2"').replace(/no/, '')) )
				audio.ext = 'm4a';
			else
				audio.ext = 'mp3';

			return;
		}

	}
	
	playAudio = function(name)
	{
		if (selfUser.settings && selfUser.settings.sound != 1) return;
		
		if ( audio && audio.play ) {
			audio.src = "/js/fchat/sounds/" + name + '.' + audio.ext;
			audio.play()
		}
	}
	
	//Open smiles list
	openEmotionsTab = function()
	{
		textarea = document.id('ChatEntry');
		
		document.getElement('.emotions_btn').addClass('opened');
		if ( document.getElement('.emotions_container') ) {
			document.getElement('.emotions_container').setStyle('display', 'block');
			return;
		}
		
		emotionsContainer = new Element('div', {
			'class' : 'emotions_container'
		});
		for(i in emotions) {
			new Element('span', {
				'class' : 'emotions emotions_' + emotions[i],
				'rel' : i
			}).inject(emotionsContainer);
		}
		for(i in customEmotions) {
			new Element('span', {
				'class' : 'emotions emotions_' + customEmotions[i],
				'rel' : i
			}).inject(emotionsContainer);
		}
		emotionsContainer.inject(document.getElement('.emotions_wrapper'));
		
		//Adding events to the emotions spans
		document.getElements('.emotions_container span').addEvent('click', function() {
			textarea.set('value', textarea.get('value') + this.get('rel'));//Adding smile to text
			textarea.focus();
			hideEmotionsTab();
		});
	}
	
	hideEmotionsTab = function()
	{
		document.getElement('.emotions_container').setStyle('display', 'none');
		document.getElement('.emotions_btn').removeClass('opened');
	}
	
	replaceEmotions = function(message)
	{
		//First replacing custom emotions
		for ( i in customEmotions) {
			message = message.replace(htmlEntities(i), '<span class="emotions emotions_' + customEmotions[i] + '"></span>');
		}
		for ( i in emotions ) {
			message = message.replace(htmlEntities(i), '<span class="emotions emotions_' + emotions[i] + '"></span>');
		}
		return message;
	}
	
	this.connect();
	this.initListeners();
	initAudio();
}