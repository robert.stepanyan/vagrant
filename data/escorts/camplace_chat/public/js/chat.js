function Chat(sessionId, siteId, host, uHash) {
	var socket,
		forceEnable,
		onlineUsers = [], //For easily sorting users
		onlineUsersObj = {},//For easily getting user by id
		selfUser = {},
		browser = Browser ? Browser.name : '',
		popupDialogs = [],
		selfObj = this,
		reconnectionAttempts = 0,
		reconnectOnDisconnect = false;
	
	var tabSpace,
		windowSize,
		tabWidth = 149,
		tabsCanOpened,
		openTabs = 0,
		tabsFromLeft = tabsFromRight = 0;
		
	var monthNames = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
	
	var customEmotions = {
		'>:o'	: 'upset',
		'O:)'	: 'angel',
		'3:)'	: 'devil',
		'>:('	: 'grumpy'
	};
	var emotions = {
		':)'	: 'smile',
		':('	: 'frown',
		':P'	: 'tongue',
		':D'	: 'grin',
		':*'	: 'kiss',
		':o'	: 'gasp',
		';)'	: 'wink',
		':v'	: 'pacman',
		':/'	: 'unsure',
		':\'('	: 'cry',
		'^_^'	: 'kiki',
		'8)'	: 'glasses',
		'B|)'	: 'sunglasses',
		'<3'	: 'heart',
		'-_-'	: 'squint',
		'o.O'	: 'confused',
		':3'	: 'colonthree'
	};
	
	var originalTitle = document.title;
	var blinkTimer;
	
	this.connect = function()
	{
		if ( typeof socket == 'undefined' ) {
			socket = io.connect('http://chat.pluginnetwork.com:8888', {
				 'reconnect': true
				, 'reconnection delay': 500
				, 'reconnection limit': 30000
				, 'max reconnection attempts': 10000
			});
		} else {
			socket.socket.connect();
		}
	}
	
	this.initListeners = function()
	{
		var self = this;
		
		socket.on('auth-complete', function(data) {
			signedOffToolbar();
			if ( data.auth ) {
				selfUser = data.userInfo;
				//selfUser.friendUserIds = selfUser.friendUserIds.concat(selfUser.notConfirmedFriendUserIds);
				prepareSettings();
				if ( data.userInfo.status == 'offline' ) {
					notAvailableToolbar();
				} else {
					availableToolbar();
				}
			} else {
				
			}
			//Init events only on first connection
			if ( reconnectionAttempts <= 1 ) {
				self.initEvents();
				self.trackConnection();
				setInterval(function() {
					socket.emit('ping', {name : selfUser.nickName});
					
				}, 5000);
			}
			
			prepareToolbar();
			reconnectOnDisconnect = true;
			
			
			
			document.id('chat-container').removeClass('hidden');

			if ( ! Cookie.read('chatnotifications') && selfUser.status != 'offline' ) {
				selfObj.showNotices();
			}
			
			
		});
		var r = 0;
		socket.on('reconnect', function() {
			r++;
			if ( r > 5 ) {
				r = 0;
				socket.disconnect();
				self.connect();
			}
		});
		
		socket.on('connect', function() {
			if ( typeof forceEnable == 'undefined' ) {
				forceEnable = false;
			}
			if ( ! sessionId ) {
				notAvailableToolbar();
				return;
			}
			
			socket.emit('auth', {
				sid: sessionId, 
				forceEnable: forceEnable,
				siteId : siteId,
				uHash: uHash
			});
			forceEnable = false;
			
			reconnectionAttempts += 1;
		});
		
		socket.on('online-users-list', function(users) {
			onlineUsers = [];
			onlineUsersObj = users;
			for(var i in users) {
				onlineUsers.push(users[i]);
			}
			sortOnlineUsers();
			fillOnlineUsers();
			
			if ( selfUser.userId == 12664 && selfUser.pendingFriendsCount && selfUser.pendingFriendsCount > 0 ) {
				document.id('pending-friends-count').set('html', selfUser.pendingFriendsCount);
				document.id('pending-friends-tab').setStyle('display', 'block');
				//selfUser.pendingFriendsCount = 0;
			}
			
			filterBaddyList();
			filterFavouritesList();
		});
		
		socket.on('user-status-changed', function(data) {
			var userId = data.userId.toInt(),
				status = data.status;
			
			if ( document.id('user_tab_' + userId) ) {
				$$('#user_tab_' + userId).removeClass('online')
					.removeClass('offline')
					.removeClass('away')
					.removeClass('busy')
					.addClass(status);
			}
			
			if ( ! onlineUsersObj[userId] ) return;
			
			oldStatus = onlineUsersObj[userId].status;
			
			onlineUsers.each(function(u, i) {
				if ( u.userId == userId ) {
					onlineUsers[i].status = status;
				}
			});
			onlineUsersObj[userId].status = status;

			//change online friends/favourites count
			
			if ( selfUser.friendUserIds.indexOf(userId)  != -1 ) {
				countContainer = document.id('online-friends-count')
			} else {
				countContainer = document.id('online-favourites-count');
			}
			
			/*-->Preparing notification message*/
			onlineUsersCount = countContainer.get('html').toInt();
			
			if ( oldStatus == 'offline' && status != 'offline' ) {
				countContainer.set('html', onlineUsersCount + 1);
				
				notice = _('USERISONLINE', {nickName: onlineUsersObj[userId].nickName});
				selfObj.notification(notice, userId);
			} else if ( status == 'offline' ) {
				countContainer.set('html', onlineUsersCount <= 0 ? 0 : onlineUsersCount  - 1);
				
				notice = _('PMUSERWENTOFFLINE', {nickName: onlineUsersObj[userId].nickName});
				selfObj.notification(notice, userId);
			}
			
			/*<-->Preparing notification message*/
			
			if ( document.id('user_' + userId) ) {
				document.id('user_' + userId).getElement('.user_status')
				.removeProperty('class')
				.addClass('user_status')
				.addClass(status);
			}
			
			if ( selfUser.friendUserIds.indexOf(userId) == -1 ) {
				filterFavouritesList();
			} else {
				filterBaddyList();
			}
		});
		
		socket.on('open-dialogs', function(data) {
			//Creating open tabs
			data.dialogs.each(function(dialog) {
				createChatTab(dialog.userInfo);
			});
			
			//Open active dialog
			if ( data.activeDialog ) {
				openUserTab(data.activeDialog, true);
			}
		});
		
		socket.on('new-message-self', function(data) {
			addMessage(data.userId, selfUser, data.message);
		});
		
		socket.on('new-message', function(data) {
			if ( isPopupOpened(data.senderData.userId) ) {
				markUserRowUnread(data.senderData.userId, 1, false);
				return;
			}
			
			//FOR ESCORT/MODEL Do not open user tab if new message comes
			/*if ( selfUser.userType == 'escort' ) {
				markUserRowUnread(data.senderData.userId);
			} else {
				createChatTab(data.senderData);
				markTabUnread(data.senderData.userId);
			}*/
			
			markUserRowUnread(data.senderData.userId, 1, false);
			createChatTab(data.senderData);
			markTabUnread(data.senderData.userId);
			

			addMessage(data.senderData.userId, data.senderData, data.message);
			blinkPageTitle('New message from ' + data.senderData.nickName);
			playAudio('new-message');


			/*-->*/
			userTab = document.id('user_tab_' + data.senderData.userId);
			if ( userTab && userTab.hasClass('expand') ) {
				socket.emit('conversation-read', {
					userId : data.senderData.userId
				});
			}
			/*<--*/
			
		});
		
		socket.on('new-messages', function(messages) {
			messages.each(function(message) {
				if ( message.senderInfo ) {
					
					/*if ( selfUser.userType == 'escort' && false ) {
						markUserRowUnread(message.userId, message.count);
					} else {
						createChatTab(message.senderInfo);
						markTabUnread(message.userId, message.count);
					}*/
					
					if ( isPopupOpened(message.userId) ) {
						markUserRowUnread(message.userId, message.count);
					} else {
						createChatTab(message.senderInfo);
						markTabUnread(message.userId, message.count);
					}
				}
			});
		});
		
		socket.on('message-history', function(data) {
			fillHistory(data.senderInfo, data.messages);
		});
		
		socket.on('typing-start', function(data) {
			userTab = document.id('user_tab_' + data.userId);
			if ( userTab ) {
				userTab.addClass('typing');
			}
		});
		socket.on('typing-end', function(data) {
			if ( document.id('user_tab_' + data.userId) ) {
				document.id('user_tab_' + data.userId).removeClass('typing');
			}
		});
		
		socket.on('pending-friends', function(pendingFriends) {
			selfUser.pendingFriends = pendingFriends;
			fillPendingFriends();
		});
		
		socket.on('chat-off', function(data) {
			notAvailableToolbar();
		});
		
		socket.on('bl-word', function(data) {
			showMessage(data.dialogId, _('BLACKLISTEDWORDSMESSAGE', {words: data.word}) );
		});
		
		socket.on('pm-allow', function(data) {
			showMessage(data.dialogId, _('PMALLOW'), true);
		});
		
		socket.on('conversation-limit', function(data) {
			showMessage(data.dialogId, _('CONVERSATIONLIMIT'));
		});
		
		socket.on('restr-member-to-member', function(data) {
			showMessage(data.dialogId, _('RESTRICTIONBASICMEMBERTOMEMBER'));
		});
		
		socket.on('restr-member-to-model', function(data) {
			showMessage(data.dialogId, _('RESTRICTIONBASICMEMBERTOMODEL'));
		});
		
		socket.on('tip-model', function(data) {
			selfObj.notification('You got a tip from ' + data.userInfo.nickName + ' for ' + data.amount + ' tokens.');
			try {
				Plugin.Publisher.pmTipRecv(data.userInfo.userId, data.userInfo.nickName, data.amount)
			} catch(err) {
				console.log(err.message);
			}
		});
		
		socket.on('status-changed', function(data) {
			changeStatus(data.status);
		});
		
		socket.on('conversation-read', function(data) {
			userTab = document.id('user_tab_' + data.userId);
			
			markUserRowRead(data.userId);
			
			if ( userTab && userTab.hasClass('new_message') ) {
				userTab.removeClass('new_message');
				userTab.getElement('.new_messages_count').destroy();
			}
			
		});
		
		socket.on('favourite-added', function(data) {
			if ( document.id('active_chat_' + data.favouriteUserId) ) {
				document.id('active_chat_' + data.favouriteUserId).getElement('.remove_fav_button').removeClass('hidden');
				document.id('active_chat_' + data.favouriteUserId).getElement('.add_fav_button').addClass('hidden');
			}
			
			selfUser.favouritesIds.push(data.favouriteUserId);
			onlineUsersObj[data.favouriteUserId] = data.favUser;
			
			if ( selfUser.friendUserIds.indexOf(data.favouriteUserId) == -1 ) {
				onlineUsers.push(data.favUser);

				insertUserBuddyList(data.favUser);
				document.id('favourites-count').set('html', selfUser.favouritesIds.length);
				if ( data.favUser.status != 'offline' ) {
					document.id('online-favourites-count').set('html', document.id('online-favourites-count').get('html').toInt() + 1);

				}
				filterFavouritesList();
			}
		});
		
		socket.on('favourite-removed', function(data) {
			if ( document.id('active_chat_' + data.favouriteUserId) ) {
				document.id('active_chat_' + data.favouriteUserId).getElement('.remove_fav_button').addClass('hidden');
				document.id('active_chat_' + data.favouriteUserId).getElement('.add_fav_button').removeClass('hidden');
			}
			
			selfUser.favouritesIds.splice(selfUser.favouritesIds.indexOf(data.favouriteUserId), 1);
			
			if ( selfUser.friendUserIds.indexOf(data.favouriteUserId) == -1 ) {
				delete onlineUsersObj[data.favouriteUserId];
				onlineUsers.each(function(u, i) {
					if ( u.userId == data.favouriteUserId ) onlineUsers.splice(i, 1);//removing from onlineUsers list
				});

				document.id('user_' + data.favouriteUserId).destroy();
					
				document.id('favourites-count').set('html', selfUser.favouritesIds.length);
				if ( data.favUser.status != 'offline' ) {
					document.id('online-favourites-count').set('html', document.id('online-favourites-count').get('html').toInt() - 1);
				}
			}
		});
		
		
		socket.on('friend-added', function(data) {
			if ( document.id('active_chat_' + data.friendUserId) ) {
				document.id('active_chat_' + data.friendUserId).getElement('.remove_friend_button').removeClass('hidden');
				document.id('active_chat_' + data.friendUserId).getElement('.add_friend_button').addClass('hidden');
			}
			
			selfUser.favouritesIds.splice(selfUser.favouritesIds.indexOf(data.favouriteUserId), 1);
			
			if ( selfUser.favouritesIds.indexOf(data.friendUserId) != -1 ) { 
				document.id('user_' + data.friendUserId).destroy();
				document.id('favourites-count').set('html', document.id('favourites-count').get('html').toInt() - 1);
			}
			
			
			selfUser.notConfirmedFriendUserIds.push(data.friendUserId);
			selfUser.friendUserIds.push(data.friendUserId);
			
			onlineUsersObj[data.friendUserId] = data.friendUser;
			
			onlineUsers.push(data.friendUser);

			insertUserBuddyList(data.friendUser);
			document.id('friends-count').set('html', selfUser.friendUserIds.length);
			
			
			filterBaddyList();
		});
		
		socket.on('friend-removed', function(data) {
			if ( document.id('active_chat_' + data.friendUserId) ) {
				document.id('active_chat_' + data.friendUserId).getElement('.add_friend_button').removeClass('hidden');
				document.id('active_chat_' + data.friendUserId).getElement('.remove_friend_button').addClass('hidden');
			}
			
			selfUser.friendUserIds.splice(selfUser.friendUserIds.indexOf(data.friendUserId), 1);
			selfUser.notConfirmedFriendUserIds.splice(selfUser.notConfirmedFriendUserIds.indexOf(data.friendUserId), 1);
			document.id('user_' + data.friendUserId).destroy();
			
			if ( selfUser.favouritesIds.indexOf(data.friendUserId) == -1 ) {
				delete onlineUsersObj[data.friendUserId];
			} else {
				insertUserBuddyList(data.friendUser);
				document.id('favourites-count').set('html', document.id('favourites-count').get('html').toInt() + 1);
			}
			
			
			document.id('friends-count').set('html', selfUser.friendUserIds.length);
			
			filterBaddyList();
			filterFavouritesList();
		});
		
		socket.on('friend-request-confirmed', function(data) {
			document.id('pending_friend_user_' + data.userId).destroy();
			document.id('pending-friends-count').set('html', document.id('pending-friends-count').get('html').toInt() - 1);
			
			selfUser.friendUserIds.push(data.userId);
			
			onlineUsersObj[data.userId] = selfUser.pendingFriends[data.userId];
			onlineUsers.push(selfUser.pendingFriends[data.userId]);
			
			insertUserBuddyList(selfUser.pendingFriends[data.userId]);
			document.id('friends-count').set('html', selfUser.friendUserIds.length);
			
			
			filterBaddyList();
			
			delete selfUser.pendingFriends[data.userId];
			selfUser.pendingFriendsCount -= 1;
			
			if ( selfUser.pendingFriendsCount == 0 ) {
				document.id('pending-friends-tab').setStyle('display', 'none');
				prepareToolbar();
				hidePendingFriendsList();
			}
		});
		
		socket.on('friend-request-declined', function(data) {
			delete selfUser.pendingFriends[data.userId];
			selfUser.pendingFriendsCount -= 1;
			
			document.id('pending_friend_user_' + data.userId).destroy();
			document.id('pending-friends-count').set('html', document.id('pending-friends-count').get('html').toInt() - 1);
			
			if ( selfUser.pendingFriendsCount == 0 ) {
				document.id('pending-friends-tab').setStyle('display', 'none');				
				prepareToolbar();
				hidePendingFriendsList();
			}
			
		});
		
		socket.on('settings-sound-changed', function(data) {
			var button = document.getElementById('sound-settings').getElements('.button')[0];
			var onOff = document.getElementById('sound-settings').getElements('.on-off')[0];
			
			selfUser.settings.sound = data.value;
			if ( data.value ) {
				onOff.set('html', 'ON');
				button.removeClass('off').addClass('on');
			} else {
				button.removeClass('on').addClass('off');
				onOff.set('html', 'OFF');
			}
		});
		
		socket.on('settings-pm-allow-changed', function(data) {
			setPmAllow(data.pmAllow);
		});
		
		socket.on('logout', function() {
			reconnectOnDisconnect = false;
		});
		
		socket.on('disconnect', function() {
		});
		
		socket.on('reconnect_failed ', function() {
			console.log('----------------------Reconnection Failed -------------------------------');
		});
	}
	
	this.initEvents = function()
	{
		var self = this;
		
		document.id('chat-on').removeEvents('click').addEvent('click', function() {
			self.enableChat();
		});
		
		document.id('sign-off-chat').removeEvents('click').addEvent('click', function() {
			self.disableChat();
		});
		
		document.id('chat-status').removeEvents('click').addEvent('click', function(e) {
			if ( this.hasClass('opened') ) {
				hideStatusMenu();
			} else {
				showStatusMenu();
			}
		});
		
		//Adding event only on status values
		$$('#chat-status-menu li.with-icon').addEvent('click', function(e) {
			var cl = this.getElement('span.status').getProperty('class');
			var status = cl.replace('status', '').replace(/\s/g, '');
			changeStatus(status);
			hideStatusMenu();
			
			socket.emit('status-changed', {status: status});
		});
		
		initBuddyListEvents();
		
		var timer;
		window.addEvent('resize', function() {
			/*clearInterval(timer);
			timer = setInterval(function() {
				prepareToolbar();
			}, 100);*/
			prepareToolbar();
		});
		
		var tabFxInAction = false;
		var tabFx = new Fx.Elements(document.id('tabs_roller'), {
			onComplete: function(){
				tabFxInAction = false;
			}
		});
		
		document.id('user_tabs_scroll_left').addEvent('click', function() {
			if ( tabsFromLeft > 0 && ! tabFxInAction ) {
				/*-->Hiding all open tabs*/
				$$('.chat_base .user_tab').removeClass('expand');
				$$('.active_chat').addClass('hidden').removeClass('showen');
				/*<--Hiding all open tabs*/
				
				tabFxInAction = true
				var curMargin = document.id('tabs_roller').getStyle('margin-right').toInt();
				tabFx.start({
					0 : {
						'margin-right' : [curMargin, curMargin - tabWidth]
					}
				});
				
				//document.id('tabs_roller').setStyle('margin-right', '-' + tabWidth);
				tabsFromLeft -= 1;
				tabsFromRight += 1;
				$$('#user_tabs_scroll_left b')[0].set('html', tabsFromLeft);
				$$('#user_tabs_scroll_right b')[0].set('html', tabsFromRight);
			}
		});
		
		document.id('user_tabs_scroll_right').addEvent('click', function() {
			if ( tabsFromRight > 0 && ! tabFxInAction ) {
				/*-->Hiding all open tabs*/
				$$('.chat_base .user_tab').removeClass('expand');
				$$('.active_chat').addClass('hidden').removeClass('showen');
				/*<--Hiding all open tabs*/
				
				tabFxInAction = true;
				var curMargin = document.id('tabs_roller').getStyle('margin-right').toInt();
				tabFx.start({
					0: {
						'margin-right' : [curMargin, curMargin + tabWidth]
					}
				});
				tabsFromLeft += 1;
				tabsFromRight -= 1;
				$$('#user_tabs_scroll_left b')[0].set('html', tabsFromLeft);
				$$('#user_tabs_scroll_right b')[0].set('html', tabsFromRight);
			}
		});
	}
	
	initBuddyListEvents = function()
	{
		/*-->Baddy list events*/
		document.id('baddy-list-tab').removeEvents('click');
		document.id('baddy-list-tab').addEvent('click', function() {
			if ( document.id('baddy-list').hasClass('closed') ) {
				showBaddyList();
			} else {
				hideBaddyList();
			}
		});
		
		$$('#baddy-list .tab_header').addEvent('click', function() {
			hideBaddyList();
		});
		
		//Show friends all/online/offline logic
		document.id('baddy-list').getElements('.status-filter .val').addEvent('click', function() {
			document.id('baddy-list').getElements('.status-filter .val').removeClass('selected');
			this.addClass('selected');
			filterBaddyList();
			
			
			var value = this.getProperty('class').replace('val', '').replace('selected', '').trim();
			socket.emit('change-settings-friends-filter', {value: value});
		});
		
		//Search logic
		document.id('baddy_list_search').addEvent('keyup', function() {
			filterBaddyList();
		});
		
		/*<--Baddy list events*/
		
		
		/*-->Favs list events*/
		document.id('favourites-list-tab').removeEvents('click');
		document.id('favourites-list-tab').addEvent('click', function() {
			if ( document.id('favourites-list').hasClass('closed') ) {
				showFavouritesList();
			} else {
				hideFavouritesList();
			}
		});
		
		$$('#favourites-list .tab_header').addEvent('click', function() {
			hideFavouritesList();
		});
		
		
		
		//Search logic
		document.id('favourites_list_search').addEvent('keyup', function() {
			filterFavouritesList();
		});
		
		//Show friends all/online/offline logic
		document.id('favourites-list').getElements('.status-filter .val').addEvent('click', function() {
			document.id('favourites-list').getElements('.status-filter .val').removeClass('selected');
			this.addClass('selected');
			filterFavouritesList();
			
			var value = this.getProperty('class').replace('val', '').replace('selected', '').trim();
			socket.emit('change-settings-favs-filter', {value: value});
		});
		
		/*<--Favs list events*/
		
		/*-->Pending friends events*/
		document.id('pending-friends-tab').removeEvents('click');
		document.id('pending-friends-tab').addEvent('click', function() {
			if ( document.id('pending-friends-list').hasClass('closed') ) {
				showPendingFriendsList();
			} else {
				hidePendingFriendsList();
			}
		});
		
		$$('#pending-friends-list .tab_header').addEvent('click', function() {
			hidePendingFriendsList();
		});
		
		/*<--Pending friends events*/
		
		$$('#baddy_list_search, #favourites_list_search').addEvent('focus', function() {
			if ( this.get('value') == 'Search' ) {
				this.set('value', '');
			}
		});
		$$('#baddy_list_search, #favourites_list_search').addEvent('blur', function() {
			if ( this.get('value').length == 0 ) {
				this.set('value', 'Search');
			}
		});
		
		
	}
	
	this.chatWith = function(userId)
	{
		if ( userId == selfUser.userId ) return false;
		
		if ( isPopupOpened(userId) ) {
			selfObj.chatWithPopup(userId);
			return;
		}
		if ( selfUser.status == 'offline' ) {
			return;
		}
		
		if ( typeof onlineUsersObj[userId] != 'undefined' ) {
			createChatTab(onlineUsersObj[userId], true);
		} else {
			socket.emit('chat-with', {
				userId : userId
			});
		}
	}
	
	this.chatWithPopup = function(userId)
	{
		if ( userId == selfUser.userId ) return false;
		
		if ( selfUser.status == 'offline' ) {
			return;
		}
		
		//If we have opened tab close it before opening PM
		if ( $$('#user_tab_' + userId + ' .close_tab, #active_chat_' + userId + ' .minimize_button').length ) {
			document.id('user_tab_' + userId).destroy();
			document.id('active_chat_' + userId).destroy();
			resizeTabBar('remove');
		}
		popup = window.open('/chat/popup/?userid=' + userId,'privatechat' + userId,'height=450,width=370,scrollbars=yes');
		popup.focus();
		
		if ( ! window['chatPopups'] ) {
			window['chatPopups'] = {};
		}
		
		window['chatPopups'][userId] = popup;
	}
	
	this.getStatus = function()
	{
		return selfUser.status;
	}
	
	this.isOnline = function(userId)
	{
		if ( typeof onlineUsersObj[userId] != 'undefined' ) {
			return true;
		} else {
			return false;
		}
	}
	
	this.enableChat = function()
	{
		selfObj.showNotices();
		socket.emit('availability-changed', 1);
	}
	
	this.disableChat = function()
	{
		Cookie.dispose('chatnotifications');
		socket.emit('availability-changed', 0);
	}
	
	this.emitMessageHistory = function(userId)
	{
		socket.emit('message-history', {userId : userId});
	}
	
	showBaddyList = function()
	{
		document.id('baddy-list').removeClass('closed');
		document.id('baddy-list').removeClass('hidden');
		document.id('baddy-list-tab').addClass('expand');
		document.id('baddy_list_search').focus();
		
		bringToFront(document.id('baddy-list'));
	}
	
	hideBaddyList = function()
	{
		document.id('baddy-list').addClass('closed');
		document.id('baddy-list').addClass('hidden');
		document.id('baddy-list-tab').removeClass('expand');
	}
	
	showFavouritesList = function()
	{
		document.id('favourites-list').removeClass('closed');
		document.id('favourites-list').removeClass('hidden');
		document.id('favourites-list-tab').addClass('expand');
		document.id('favourites_list_search').focus();
		
		bringToFront(document.id('favourites-list'));
	}
	
	hideFavouritesList = function()
	{
		document.id('favourites-list').addClass('closed');
		document.id('favourites-list').addClass('hidden');
		document.id('favourites-list-tab').removeClass('expand');
	}
	
	showPendingFriendsList = function()
	{
		document.id('pending-friends-list').removeClass('closed');
		document.id('pending-friends-list').removeClass('hidden');
		document.id('pending-friends-tab').addClass('expand');
		
		bringToFront(document.id('pending-friends-list'));
	}
	
	hidePendingFriendsList = function()
	{
		document.id('pending-friends-list').addClass('closed');
		document.id('pending-friends-list').addClass('hidden');
		document.id('pending-friends-tab').removeClass('expand');
	}
	
	prepareToolbar = function()
	{
		windowSize = document.getSize().x;
		w = windowSize - 32;//subtract margins from left and right
		$$('.chat_base').setStyle('width', w);
		document.id('chat-wrapper').setStyle('width', windowSize);
		
		w -= 138;//substract budy list
		w -= 138;//substract favs list
		
		tabSpace = w - 59 - 70;//subtract turn off chat , pre/next  width
		
		if ( selfUser.pendingFriendsCount ) {
			tabSpace -= 175;
		}
		
		tabsCanOpened = Math.floor(tabSpace / tabWidth);
		document.id('tabs').setStyle('width', tabsCanOpened * tabWidth);
		
		$$('.baddy_listing').setStyle('max-height', document.getSize().y - 100);
		resizeTabBar();
	}
	
	availableToolbar = function()
	{
		document.id('chat-wrapper').fireEvent('chat-on');
		changeStatus(selfUser.status);
		
		document.id('chat-status').setStyle('display', 'block');
		$$('.chat_base.signed_off').addClass('hidden');
		$$('.chat_base.signed_on').removeClass('hidden');
		//$$('.baddy_list').addClass('hidden');
		$$('.baddy_list_tab').removeClass('expand').removeClass('hidden');
		$$('.open_chat ').addClass('hidden');
	}
	
	notAvailableToolbar = function()
	{
		openTabs = 0,
		tabsFromLeft = tabsFromRight = 0;
		
		document.id('chat-wrapper').fireEvent('chat-off');
		$$('.user_tab').destroy();
		$$('.active_chat').destroy();
		
		$$('.baddy_list').addClass('hidden');
		$$('.chat_base.signed_off').addClass('hidden');
		$$('.baddy_list_tab').addClass('hidden');
		$$('.chat_base.signed_on').removeClass('hidden');
		$$('.open_chat ').removeClass('hidden');
		
		document.id('chat-status').removeClass('opened').setStyle('display', 'none');
		document.id('chat-status-menu').setStyle('display', 'none');
		document.id('user_tabs_scroll_left').setStyle('display', 'none');
		document.id('user_tabs_scroll_right').setStyle('display', 'none');
	}
	
	signedOffToolbar = function()
	{
		$$('.chat_base.signed_off').removeClass('hidden');
		$$('.chat_base.signed_on').addClass('hidden');
		$$('.open_chat ').addClass('hidden');
	}
	
	fillPendingFriends = function()
	{
		$$('#pending-friends-list ul.listing')[0].empty();
		for ( userId in selfUser.pendingFriends ) {
			var user = selfUser.pendingFriends[userId];
			
			var li = new Element('li').setProperties({
				id : 'pending_friend_user_' + user.userId,
				'class' : ''
			});
			new Element('img').setProperties({
				'class' :  'user_avatar',
				src	: user.avatar
			}).inject(li);
			
			var userNameBlock = new Element('div', {
				'class': 'user_name_block'
			});
			new Element('span').setProperties({
				'class' : 'user_name',
				html : user.nickName
			}).inject(userNameBlock);
			new Element('div').setProperties({
				'class' : 'clear'
			}).inject(userNameBlock);
			new Element('span').setProperties({
				'class' : 'cam_status',
				html : user.camStatus
			}).inject(userNameBlock);
			
			userNameBlock.inject(li);
			
			new Element('a', {
				'class' : 'btn-decline',
				'title' : _('DECLINEFRIENDREQUEST')
			}).inject(li);
			
			new Element('a', {
				'class' : 'btn-confirm',
				'title' : _('CONFIRMFRIENDREQUEST')
			}).inject(li);
			
			li.inject($$('#pending-friends-list ul.listing')[0]);
		}
		
		
		
		/*-->Attaching events to the pending friends tab*/
		document.id('pending-friends-list').getElements('a.btn-decline').addEvent('click', function() {
			userId = this.getParent('li').get('id').replace('pending_friend_user_', '').toInt();
			socket.emit('friend-request-decline', {userId: userId});
		});
		
		
		document.id('pending-friends-list').getElements('a.btn-confirm').addEvent('click', function() {
			userId = this.getParent('li').get('id').replace('pending_friend_user_', '').toInt();
			socket.emit('friend-request-confirm', {userId: userId});
		});
		/*<--Attaching events to the pending friends tab*/
	}
	
	fillOnlineUsers = function()
	{
		$$('#baddy-list ul.listing')[0].empty();
		$$('#favourites-list ul.listing')[0].empty();
		
		var onlineFavourites = 0, onlineFriends = 0;
		onlineUsers.each(function(user) {
			insertUserBuddyList(user);
			if ( user.status != 'offline' ) {
				if ( selfUser.friendUserIds.indexOf(user.userId) != -1 ) {
					onlineFriends += 1;
				} else {
					onlineFavourites += 1;
				}
			}
		});
		
		document.id('friends-count').set('html', selfUser.friendUserIds.length);
		document.id('favourites-count').set('html', onlineUsers.length - selfUser.friendUserIds.length);
		
		document.id('online-friends-count').set('html', onlineFriends);
		document.id('online-favourites-count').set('html', onlineFavourites);
	}
	
	insertUserBuddyList = function(userInfo, beforeId)
	{
		li = getUserRow(userInfo);
		/*if ( beforeId ) {
			li.inject(document.id('user_' + beforeId), 'before');
		} else {
			li.inject($$('#baddy-list ul.listing')[0]);
		}*/
		
		if ( document.id('user_' + userInfo.userId) ) {
			document.id('user_' + userInfo.userId).destroy();
		}
		
		if ( selfUser.friendUserIds.indexOf(userInfo.userId) == -1 ) {
			li.inject($$('#favourites-list ul.listing')[0]);
		} else {
			li.inject($$('#baddy-list ul.listing')[0]);
		}
		
		attachUserRowEvents(li);
	}
	
	removeUserBuddyList = function(userId)
	{
		if ( typeof document.id('user_' + userId) == 'undefined' ) return;
		document.id('user_' + userId).destroy();
	}
	
	getUserRow = function(user) 
	{
		var li = new Element('li').set('id', 'user_' + user.userId);
		
//		if ( selfUser.notConfirmedFriendUserIds.indexOf(user.userId) != -1 ) {
//			li.set('class', 'not-confirmed');
//			user.camStatus = _('WAITING')
//		}
		
		new Element('img').setProperties({
			'class' :  'user_avatar',
			src	: user.avatar
		}).inject(li);
		new Element('span').setProperties({
			'class' : 'user_name',
			html : user.nickName
		}).inject(li);
		
		
		cl = 'user_status';
		cl += ' ' + user.status;
		
		if ( user.status != 'offline' && user.isBroadcasting ) {
			cl += ' broadcasting';
		}

		new Element('span').setProperties({
			'class' : cl
		}).inject(li);
		
		
		if ( user.camStatus.length ) {
			new Element('span', {
				'class' : 'cam_status',
				'html' : user.camStatus
			}).inject(li);
		}
		
		return li;
	}
	
	sortOnlineUsers = function()
	{
		onlineUsers.sort(function(a, b) {
			if (a.ordering < b.ordering) {
				return 1;
			}
			if ( a.ordering > b.ordering ) {
				return -1;
			}

			return 0;
		});
	}

	//When user_row adds in baddy list, adding to this element eventss 
	attachUserRowEvents = function(el)
	{
		el.addEvent('click', function() {
			userId = el.get('id').replace('user_', '');
			if ( isPopupOpened(userId) ) {
				selfObj.chatWithPopup(userId);
				return;
			}
			createChatTab(onlineUsersObj[userId], true);
		});
	}
	
	
	createChatTab = function(user, isOpened)
	{
		if ( isPopupOpened(user.userId) ) return;
		
		if ( typeof isOpened == 'undefined' ) isOpened = false;
		
		if ( document.id('user_tab_' + user.userId) ) {
			if ( isOpened ) {
				openUserTab(user.userId);
			}
		} else {
			//Emiting about opened dialog
			socket.emit('dialog-opened', {
				userId: user.userId
			});
			
			shortNickName = user.nickName;
			if ( shortNickName.length >= 12) {
				shortNickName = shortNickName.substr(0, 12) + '...';
			}
			
			/*-->drawing chat tab*/
			chatInnerButton = new Element('div', {
				'class' : 'chat_inner_button'
			});
			userTabName = new Element('span', {
				'class': 'user_tab_name'
			});

			new Element('b', {
				'html': shortNickName
			}).inject(userTabName);

			userTabName.inject(chatInnerButton);

			new Element('div', {
				'class' : 'user_status'
			}).inject(chatInnerButton);

			new Element('div', {
				'class' : 'close_tab cpoint'
			}).inject(chatInnerButton);

			userTabClass = 'user_tab cpoint ' + user.status;
			
			userTab = new Element('div', {
				'class' : userTabClass,
				'id'	: 'user_tab_' + user.userId
			});

			chatInnerButton.inject(userTab);
			/*<--drawing chat tab*/
			
			/*-->drawing chat opened window*/
			activeChat = new Element('div', {
				'class' : 'active_chat hidden',
				'id'	: 'active_chat_' + user.userId
			});
			tabHeader = new Element('div', {
				'class' : 'tab_header cpoint'
			});
			
			new Element('div', {
				'class' : 'minimize_button showen cpoint'
			}).inject(tabHeader);
			
			new Element('div', {
				'class' : 'popout_button showen cpoint'
			}).inject(tabHeader);
			
			new Element('a', {
				'class' : 'broadcasting showen cpoint ' + ( user.isBroadcasting ? '' : 'hidden'),
				'href' : getChatRoomUrl(user),
				'target' : '_blank'
			}).inject(tabHeader);
			
			new Element('div', {
				'class' : 'add_fav_button showen cpoint ' + ( selfUser.favouritesIds.indexOf(user.userId) != -1 ? 'hidden' : '' )
			}).inject(tabHeader);

			new Element('div', {
				'class' : 'remove_fav_button showen cpoint ' + ( selfUser.favouritesIds.indexOf(user.userId) == -1 ? 'hidden' : '')
			}).inject(tabHeader);
			
			/*new Element('div', {
				'class' : 'add_friend_button showen cpoint ' + ( selfUser.friendUserIds.indexOf(user.userId) == -1 ? '' : 'hidden' )
			}).inject(tabHeader);

			new Element('div', {
				'class' : 'remove_friend_button showen cpoint ' + ( selfUser.friendUserIds.indexOf(user.userId) == -1 ? 'hidden' : '')
			}).inject(tabHeader);*/
						
			
			tabHeader.inject(activeChat);
			
			avatarBlock = new Element('div', {
				'class' : 'avatar-block'
			});
			new Element('img', {
				src : user.avatar
			}).inject(avatarBlock);
			
			if ( user.userType == 'escort'  ) {
				new Element('a', {
					'class' : 'tip-me-btn'
				}).inject(avatarBlock);

				new Element('a', {
					'class' : 'view-profile-btn',
					'href' : getProfileUrl(user),
					'target' : '_blank'
				}).inject(avatarBlock);
			}
			
			avatarBlock.inject(activeChat);
			
			
			activeChatContent = new Element('div', {
				'class' : 'active_chat_content'
			});
			
			if ( user.userType == 'escort' && user.isBroadcasting ) {
				drawBroadcastBlock(user).inject(activeChatContent);
			}
			
			new Element('div', {
				'class' : 'messaging'
			}).inject(activeChatContent);
			
			var textareaWrapper = new Element('div', {
				'class' : 'textarea_wrapper'
			});
			
			var textareaInner = new Element('div', {
				'class' :  'textarea_inner'
			});
			textareaWrapper.inject(textareaInner);
			
			new Element('textarea', {
				'cols' : '30',
				'rows' : '10'
			}).inject(textareaWrapper);
			
			//Tmp solution. ff can't getScroll if overflow hidden
			if ( browser == 'firefox' ) {
				textareaWrapper.getElement('textarea').setStyle('overflow', 'auto');
			}
			
			emotionsWrapper = new Element('div', {
				'class' : 'emotions_wrapper'
			});
			
			new Element('span', {
				'class' : 'emotions_btn'
			}).inject(emotionsWrapper);
			
			emotionsWrapper.inject(textareaInner);
			
			textareaInner.inject(activeChatContent);
			
			activeChatContent.inject(activeChat);
			
			userTab.inject(document.id('tabs_roller'));
			activeChat.inject(document.id('chat-container'));
			
			/*<--drawing chat opened window*/
			
			
			if ( isOpened ) {
				openUserTab(user.userId);
			}
			
			resizeTabBar('add');
			
			attachUserTabEvents(user.userId);
		}
	}
	
	//Attaching events to a new user tab
	attachUserTabEvents = function(id)
	{	
		var activeChat = document.id('active_chat_' + id);
		var textarea = activeChat.getElement('textarea');
		
		$$('#user_tab_' + id + ' .close_tab').addEvent('click', function() {
			document.id('user_tab_' + id).destroy();
			document.id('active_chat_' + id).destroy();
			resizeTabBar('remove');
			socket.emit('dialog-closed', {
				userId: id
			});
		});
		
		$$('#active_chat_' + id + ' .add_fav_button').addEvent('click', function() {
			socket.emit('add-favourite', {
				userId: id
			});
		});
		
		$$('#active_chat_' + id + ' .remove_fav_button').addEvent('click', function() {
			socket.emit('remove-favourite', {
				userId: id
			});
		});
		
		
		$$('#active_chat_' + id + ' .remove_friend_button').addEvent('click', function() {
			socket.emit('remove-friend', {
				userId: id
			});
		});
		
		$$('#active_chat_' + id + ' .add_friend_button').addEvent('click', function() {
			socket.emit('add-friend', {
				userId: id
			});
		});
		
		$$('#active_chat_' + id + ' .popout_button').addEvent('click', function() {
			selfObj.chatWithPopup(id);
		});
		
		$$('#active_chat_' + id + ' .tab_header, #user_tab_' + id ).addEvent('click', function(e) {
			if ( e.target.className.indexOf('tab_name') >= 0 || 
					e.target.className.indexOf('popout_button') >= 0 || 
					e.target.className.indexOf('add_fav_button') >= 0 || 
					e.target.className.indexOf('broadcasting') >= 0 || 
					e.target.className.indexOf('remove_fav_button') >= 0 ||
					e.target.className.indexOf('add_friend_button') >= 0 ||
					e.target.className.indexOf('remove_friend_button') >= 0) {
				return;
			}
			
			if ( document.id('user_tab_' + id).hasClass('expand') ) {
				closeUserTab(id);
			} else {
				openUserTab(id);
			}
		});

		$$('#active_chat_' + id + ' .tip-me-btn').addEvent('click', function() {
			 Plugin.Data.TipModel(id);
		});
		
		var typing = false,
		timer;
		textarea.addEvent('keypress', function(e) {
			/*--> Detect typing start/end */
			if ( ! typing ) {
				selfObj.emitTypingStart(id);
				typing = true;
			}
			
			clearTimeout(timer);
			timer = setTimeout(function(){ 
				selfObj.emitTypingEnd(id);
				typing = false;
			}, 3*1000);
			/*<-- Detect typing start/end */
			
			if ( e.code == 13 && ! e.shift ) {
				e.stop();
				
				message = this.get('value');
				
				socket.emit('message-sent', {
					userId : id,
					message: message
				});
				
				//After enter empty text area and set default height
				this.set('value', '');
				this.setStyle('height', 18);
				
				message = {
					body : message,
					date : new Date().getTime(),
					userId : selfUser.userId
				}
				
				addMessage(id, selfUser, message);
				
				//If client is sending message and participant is offline show notification
				if ( document.id('user_tab_' + id).hasClass('offline') ) {
					if ( onlineUsersObj[id] ) {
						nickname = onlineUsersObj[id].nickName;
					} else {
						nickname = document.id('user_tab_' + id).getElement('.user_tab_name b').get('html');
					}
					msg = _('OFFLINEMESSAGE', {nickName : nickname});
					showMessage(id, msg);
				}
			}
			
			
			/*-->dynamically increasing textarea height*/
			
			scrollSize = this.getScroll().y;
			height = this.getHeight();
			if ( scrollSize ) {
				offset = height + scrollSize;
				this.setStyle('height', offset);
				this.getParent('.textarea_wrapper').scrollTo(0, offset);
				if ( this.getParent('.textarea_wrapper').getHeight() > 100 ) {
					activeChat.getElement('.emotions_wrapper').setStyle('right', 13);
				}
			}
			
			/*<--dynamically increasing textarea height*/
			
		});
		
		$$('#active_chat_' + id).getElement('.emotions_btn').addEvent('click', function() {
			if ( this.hasClass('opened') ) {
				hideEmotionsTab(id);
			} else {
				openEmotionsTab(id);
			}
			
		});
		
	}
	
	drawBroadcastBlock = function(user)
	{
		var broadcastBlock = new Element('div', {
			'class': 'broadcasting-block'
		});
		
		new Element('div', {
			'class': 'bullet'
		}).inject(broadcastBlock);
		
		new Element('span', {
			'class': 'i',
			'html': 'I\'m live on cam'
		}).inject(broadcastBlock);
		
		/*new Element('a', {
			'class': 'close'
		}).addEvent('click', function() {
			broadcastBlock.destroy();
		}).inject(broadcastBlock);*/

		new Element('a', {
			'class': 'enter-chatroom-button',
			'href' : getChatRoomUrl(user),
			'target' : '_blank'
		}).inject(broadcastBlock);
		
		return broadcastBlock;
	}
	
	showMessage = function(id, message, keepOpen)
	{
		if ( typeof keepOpen == 'undefined' ) keepOpen = false;
		
		activeChatContent = document.id('active_chat_' + id ).getElement('.active_chat_content');
		
		if ( activeChatContent.getElement('.message_box') ) {
			activeChatContent.getElement('.message_box').destroy();
		}
		
		messageBox = new Element('div', {
			'class' : 'message_box',
			'html' : message
		});
		messageBox.inject(activeChatContent);
		
		var messageFx = new Fx.Morph(messageBox, {
			duration : 500
		});
		
		messageFx.start({
			'top' : [-1 * $$('.message_box')[0].getSize().y, 0]
		});
		
		if ( ! keepOpen ) {
			setTimeout(function() {
				messageFx.start({
					'top' : [0, (-1) * $$('.message_box')[0].getSize().y]
				});
			}, 10 * 1000);
		}
		/*var messageFx = new Fx.Elements(messageBox, {
			onComplete: function(){
				if ( ! keepOpen ) {
					setTimeout(function() {
						messageFx.start({
							0 : {
								top: [0, (-1) * $$('.message_box')[0].getSize().y]
							}
						});
					}, 5 * 1000);
				}
			}
		}).start({
			0 : {
				top: [-1 * $$('.message_box')[0].getSize().y, 0]
			}
		});*/
		
	}
	
	//Opening user tab for messaging by closing all others
	openUserTab = function(id, scroll)
	{
		if ( typeof scroll == 'undefined' ) scroll = true;
		
		socket.emit('dialog-activated', {
			userId: id
		});
		
		//first Closing all opened tabs
		$$('.chat_base .user_tab').removeClass('expand');
		$$('.active_chat').addClass('hidden').removeClass('showen');
		
		var tabFx = new Fx.Elements(document.id('tabs_roller'), {
			onComplete: function(){
				showUserTab(id);
			}
		});
		
		/*-->Scrolling tabs to the right position*/
		var index;
		$$('.user_tab').each(function(el, i) {
			if ( el.get('id') == 'user_tab_' + id ) {
				index = i;
			}
		});
		
		//If in visible part just calling showUserTab
		if ( index >= tabsFromRight && index < tabsFromRight + tabsCanOpened ) {
			showUserTab(id);
			return;
		}
		
		if ( ! scroll ) return;//if no need scroll just return
		
		curMargin = document.id('tabs_roller').getStyle('margin-right').toInt();
		
		//If tab is on the right side scroll then open
		if ( index < tabsFromRight   ) {
			tabFx.start({
				0 : {
					'margin-right' : [curMargin, (-1) * index * tabWidth]
				}
			});
			
			s = tabsFromRight - index;//tabs count shifted to the left
			tabsFromLeft += s;
			tabsFromRight -= s;
		}
		
		//if tab is on the left side
		if ( index >= tabsCanOpened + tabsFromRight ) {
			s = (index + 1) - tabsCanOpened - tabsFromRight;
			tabsFromLeft -= s;
			tabsFromRight += s;
			
			tabFx.start({
				0 : {
					'margin-right' : [curMargin, curMargin -  (s * tabWidth)]
				}
			});
		}
		
		$$('#user_tabs_scroll_left b')[0].set('html', tabsFromLeft);
		$$('#user_tabs_scroll_right b')[0].set('html', tabsFromRight);
		
	/*<--Scrolling tabs to the right position*/
	}
	
	showUserTab = function(id)
	{
		userTab = document.id('user_tab_' + id);
		activeChat = document.id('active_chat_' + id);
		
		bringToFront(userTab);
		
		userTab.addClass('expand');
		
		offset = windowSize - document.id('user_tab_' + id).getPosition().x - tabWidth - 1;//1px for borders
		activeChat.setStyle('right', offset);
		
		activeChat.removeClass('hidden').addClass('showen');
		
		activeChat.getElement('.messaging').scrollTo(0, activeChat.getElement('.messaging').getScrollSize().y);
		activeChat.getElement('textarea').focus();
		
		
		if ( userTab.hasClass('new_message') ) {
			userTab.removeClass('new_message');
			userTab.getElement('.new_messages_count').destroy();
			
			socket.emit('conversation-read', {
				userId : id
			});
		}
	}
	
	//Closing user tab
	closeUserTab = function(id)
	{
		socket.emit('dialog-deactivated', {
			userId: id
		});
		document.id('user_tab_' + id).removeClass('expand');
		document.id('active_chat_' + id).removeClass('showen').addClass('hidden');
		document.id('active_chat_' + id).setStyle('right', '');
	}
	
	getMessageRow = function(userInfo)
	{
		var userMessage = new Element('div', {
			'class' : 'user_message ' + userInfo.userId
		})
		var userAvatar = new Element('div', {
			'class' : 'user_avatar'
		});
		
		new Element('div', {
			'class' : userInfo.userId == selfUser.userId ? 'chat-name self' : 'chat-name',
			'html'	: userInfo.userId == selfUser.userId ? 'Me' : userInfo.nickName
		}).inject(userMessage);
		
		new Element('div', {
			'class' : 'date'
		}).inject(userMessage);
		
		new Element('div', {
			'class': 'clear'
		}).inject(userMessage);
		
		new Element('div', {
			'class' : 'messages'
		}).inject(userMessage);
		
		
		return userMessage
	}
	
	addMessage = function(id, senderInfo, message)
	{
		//first of all clearing message
		message.body = clearMessage(message.body);
		
		if ( message.body.length == 0 ) return;
		
		userTab = document.id('user_tab_' + id);
		activeChat = document.id('active_chat_' + id);
		
		if ( ! userTab ) return;
		
		if ( typeof isNewMessage == 'undefined' ) {
			isNewMessage = false;
		}
		if ( typeof newMessagesCount == 'undefined' ) {
			newMessagesCount = 1;
		}
		
		lastUserMessage = activeChat.getElements('.user_message').getLast();
		
		//if messaging is empty or last message is not his, create new user_message raw
		//else add to existing one
		if ( lastUserMessage && ( lastUserMessage.hasClass(message.userId) && message.date - parseInt(lastUserMessage.get('id')) <= 60*1000 ) ) {
			new Element('p', {
				'html': message.body
			}).inject(lastUserMessage.getElement('.messages'));
		} else {
			if ( message.userId == selfUser.userId ) {
				userMessage = getMessageRow(selfUser);
			} else {
				userMessage = getMessageRow(senderInfo);
			}
			
			userMessage.getElement('.date').set('html', getTimeForMessage(new Date(message.date)));
			
			userMessage.set('id', message.date);
			
			new Element('p', {
				'html': message.body
			}).inject(userMessage.getElement('.messages'));

			userMessage.inject(activeChat.getElement('.messaging'));

		}

		//scroll down messaging
		activeChat.getElement('.messaging').scrollTo(0, activeChat.getElement('.messaging').getScrollSize().y);
	}
	
	markTabUnread = function(id, unreadMessagesCount)
	{
		userTab = document.id('user_tab_' + id);
		if ( userTab && userTab.hasClass('expand') ) return; //Mark only closed tabs
		
		if ( typeof unreadMessagesCount == 'undefined' ) unreadMessagesCount = 1;
		
		userTab.addClass('new_message');
		if ( ! userTab.getElement('.new_messages_count') ) {
			new Element('div', {
				'class' : 'new_messages_count',
				html : unreadMessagesCount
			}).inject(userTab.getElement('.chat_inner_button'));
		} else {
			count = parseInt(userTab.getElement('.new_messages_count').get('html'));
			count += unreadMessagesCount;
			userTab.getElement('.new_messages_count').set('html', count);
		}
	}
	
	markUserRowUnread = function(id, unreadMessagesCount, withDelay)
	{
		/*userTab = document.id('user_tab_' + id);
		
		if ( userTab ) {
			if ( userTab.hasClass('expand') ) {
				return;
			}
			markTabUnread(id);
			return;
		}*/
		if ( typeof withDelay == 'undefined' ) widthDelay = true;
		if ( typeof unreadMessagesCount == 'undefined' ) unreadMessagesCount = 1;
		
		//new-messages event fires before online-users-list(here we have api call)
		//and when new-messages arrives we don't have user row in buddy list
		//seting inteverval for 300ms
		var attemptsCount = 0,
			delay = withDelay ? 300 : 0;
			
		var interval = setInterval(function() {
			userRow = document.id('user_' + id);
			//If repeats more than 5 times break
			if ( attemptsCount > 5) {
				clearInterval(interval);
				return;
			}
			
			if ( userRow ) {
				if ( userRow.getElement('.cam_status') ) {
					userRow.getElement('.cam_status').setStyle('display', 'none');
				}
				
				if ( ! userRow.getElement('.new_msg_count') ) {
					new Element('span', {
						'class' : 'new_msg_count',
						html : unreadMessagesCount
					}).inject(userRow.getElement('.user_status'), 'after');
				} else {
					count = parseInt(userRow.getElement('.new_msg_count').get('html'));
					count += unreadMessagesCount;
					userRow.getElement('.new_msg_count').set('html', count);
				}
				
				//$$('.unread-message-text').setStyle('display', 'inline');
				
				var bdlist = document.id('baddy-list-tab');
				if ( selfUser.friendUserIds.indexOf(id) == -1 ) {
					bdlist = document.id('favourites-list-tab');
				}
				bdlist.addClass('unread-msg');
				
				
				clearInterval(interval);
			}
			attemptsCount +=1;
			
		}, delay);
		
	}
	
	markUserRowRead = function(id)
	{
		userRow = document.id('user_' + id);
		
		if ( ! userRow ) return;
		
		if ( userRow.getElement('.new_msg_count') ) {
			userRow.getElement('.new_msg_count').destroy();
		}
		
		if ( userRow.getElement('.cam_status') ) {
			userRow.getElement('.cam_status').setStyle('display', 'block');
		}
		
		if ( document.id('baddy-list').getElements('.new_msg_count').length == 0 ) {
			document.id('baddy-list-tab').removeClass('unread-msg');
		}
		
		if ( document.id('favourites-list-tab').getElements('.new_msg_count').length == 0 ) {
			document.id('favourites-list-tab').removeClass('unread-msg');
		}
	}
	
	fillHistory = function(senderInfo, messages)
	{
		userTab = document.id('user_tab_' + senderInfo.userId);
		activeChat = document.id('active_chat_' + senderInfo.userId);
		
		activeChat.getElement('.messaging').empty();
		if ( userTab ) {
			messages.each(function(message) {
				addMessage(senderInfo.userId, senderInfo, message)
			});
		}
	}
	
	getTimeForMessage = function(date)
	{
		curDate = new Date();
		//date = date + date.getTimezoneOffset() * 60 * 1000;
		
		hours = date.getHours();
		minutes = ('0' + date.getMinutes()).slice(-2);//two number format
		
		d = hours + ':' + minutes;
		
		if ( curDate.getDate() != date.getDate() ) {
			d += ' ' + date.getDate() + 'th ' + monthNames[date.getMonth()];
		}
		
		if ( curDate.getYear() != date.getYear() ) {
			d += ' ' + date.getYear();
		}
		
		return d;
	}
	
	clearMessage = function(message)
	{
		//htmlentities
		/*message = htmlEntities(message);
		
		//Replacing one more whitespaces
		message = message.trim();
		message = message.replace(/\s+/g, ' ');*/
		
		//replacing urls with html links
		var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
		message = message.replace(exp,"<a href='$1' target='_blank'>$1</a>");
		
		message = replaceEmotions(message);
		
		return message;
	}
	
	htmlEntities = function(str) {
		return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
	}
	
	//Open smiles list
	openEmotionsTab = function(id)
	{
		activeChat = document.id('active_chat_' + id);
		textarea = activeChat.getElement('textarea');
		
		activeChat.getElement('.emotions_btn').addClass('opened');
		if ( activeChat.getElement('.emotions_container') ) {
			activeChat.getElement('.emotions_container').setStyle('display', 'block');
			return;
		}
		
		emotionsContainer = new Element('div', {
			'class' : 'emotions_container'
		});
		for(i in emotions) {
			new Element('span', {
				'class' : 'emotions emotions_' + emotions[i],
				'rel' : i
			}).inject(emotionsContainer);
		}
		for(i in customEmotions) {
			new Element('span', {
				'class' : 'emotions emotions_' + customEmotions[i],
				'rel' : i
			}).inject(emotionsContainer);
		}
		emotionsContainer.inject(activeChat.getElement('.emotions_wrapper'));
		
		//Adding events to the emotions spans
		activeChat.getElements('.emotions_container span').addEvent('click', function() {
			textarea.set('value', textarea.get('value') + this.get('rel'));//Adding smile to text
			textarea.focus();
			hideEmotionsTab(id);
		});
	}
	
	hideEmotionsTab = function(id)
	{
		activeChat.getElement('.emotions_container').setStyle('display', 'none');
		activeChat.getElement('.emotions_btn').removeClass('opened');
	}
	
	replaceEmotions = function(message)
	{
		//First replacing custom emotions
		for ( i in customEmotions) {
			
			//For smiles cleaned by htmlentities
			while(message.indexOf(htmlEntities(i)) != -1) {
				message = message.replace(htmlEntities(i), '<span class="emotions emotions_' + customEmotions[i] + '"></span>');
			}
			
			while( message.indexOf(i) != -1 ) {
				message = message.replace(i, '<span class="emotions emotions_' + customEmotions[i] + '"></span>');
			}
		}
		for ( i in emotions ) {
			//small fix for. for this smile :/ (http://)
			if ( i == ':/' && (message.indexOf('http://') != -1 || message.indexOf('https://') != -1) ) {
				continue;
			}
			
			while(message.indexOf(i) != -1) {
				message = message.replace(i, '<span class="emotions emotions_' + emotions[i] + '"></span>');
			}
			
			//For smiles cleaned by htmlentities
			while(message.indexOf(htmlEntities(i)) != -1) {
				message = message.replace(htmlEntities(i), '<span class="emotions emotions_' + emotions[i] + '"></span>');
			}
		}
		return message;
	}
	
	function RegExpEscape(text) {
		return text.replace(/[\\=!^$*+?.:|(){}[\]]/g, "\\$&");
	}
	
	resizeTabBar = function(action)
	{
		if ( typeof action == 'undefined' ) action = 'resize';

		//getting open tab id and hidding it
		var openDialogId = false;
		if ( $$('.user_tab.expand').length ) {
			openDialogId = $$('.user_tab.expand')[0].get('id').replace('user_tab_', '');
			document.id('user_tab_' + openDialogId).removeClass('expand');
			document.id('active_chat_' + openDialogId).removeClass('showen').addClass('hidden');
		}
	
		if ( action == 'add' ) {
			openTabs += 1;
			if ( openTabs > tabsCanOpened ) {
				tabsFromLeft += 1;
				$$('#user_tabs_scroll_left b')[0].set('html', tabsFromLeft);
				document.id('user_tabs_scroll_left').setStyle('display', 'block');
				document.id('user_tabs_scroll_right').setStyle('display', 'block');
			}
		}
		
		if ( action == 'remove' ) {
			
			if ( openTabs > tabsCanOpened ) {
				if ( tabsFromLeft > 0 ) {
					tabsFromLeft -= 1;
				} else {
					if ( tabsFromRight > 0 ) {
						tabsFromRight -= 1;
						curMargin = document.id('tabs_roller').getStyle('margin-right').toInt();
						document.id('tabs_roller').setStyle('margin-right', curMargin + tabWidth);
					}
				}
				
				$$('#user_tabs_scroll_left b')[0].set('html', tabsFromLeft);
				$$('#user_tabs_scroll_right b')[0].set('html', tabsFromRight);
			}
			openTabs -= 1;
			if ( openTabs <= tabsCanOpened ) {
				$$('#user_tabs_scroll_right b')[0].set('html', 0);
				$$('#user_tabs_scroll_left b')[0].set('html', 0);
				document.id('user_tabs_scroll_left').setStyle('display', 'none');
				document.id('user_tabs_scroll_right').setStyle('display', 'none');
			}
		}
		
		if ( action == 'resize' ) {
			if ( openTabs > tabsCanOpened ) {

				diff = openTabs - tabsCanOpened;

				if ( tabsFromRight > tabsFromLeft ) {
					$$('#user_tabs_scroll_right b')[0].set('html', diff);
					$$('#user_tabs_scroll_left b')[0].set('html', 0);
					document.id('tabs_roller').setStyle('margin-right', (-1) * diff * tabWidth);
					tabsFromRight = diff;
					tabsFromLeft = 0;
				} else {
					$$('#user_tabs_scroll_left b')[0].set('html', diff);
					$$('#user_tabs_scroll_right b')[0].set('html', 0);
					document.id('tabs_roller').setStyle('margin-right', '');
					tabsFromLeft = diff;
					tabsFromRight = 0;
				}
				document.id('user_tabs_scroll_left').setStyle('display', 'block');
				document.id('user_tabs_scroll_right').setStyle('display', 'block');
			} else {
				if ( tabsFromLeft > 0 ) {
					$$('#user_tabs_scroll_left b')[0].set('html', 0);
				}
				if ( tabsFromRight > 0 ) {
					document.id('tabs_roller').setStyle('margin-right', '');
					$$('#user_tabs_scroll_right b')[0].set('html', 0);
				}
				document.id('user_tabs_scroll_left').setStyle('display', 'none');
				document.id('user_tabs_scroll_right').setStyle('display', 'none');
			}
		}
		
		//Afer changes open it back
		if ( openDialogId ) {
			openUserTab(openDialogId, false);
		}
	}
	
	prepareSettings = function()
	{
		if ( typeof selfUser.settings == 'undefined' ) selfUser.settings = {};
		
		/*-->Prepare sound settings*/
		if ( typeof selfUser.settings.sound == 'undefined' ) {
			selfUser.settings.sound = 1;
		}
		
		if ( selfUser.settings.sound ) {
			$$('#sound-settings .button').addClass('on');
			$$('#sound-settings .on-off').set('html', 'ON');
		} else {
			$$('#sound-settings .button').addClass('off');
			$$('#sound-settings .on-off').set('html', 'OFF');
		}
		
		$$('#sound-settings')[0].addEvent('click', function() {
			var button = $$('#sound-settings .button')[0];
			var onOff = $$('#sound-settings .on-off')[0];
			
			
			if ( button.hasClass('on')) {
				button.removeClass('on').addClass('off');
				onOff.set('html', 'OFF');
				socket.emit('change-settings-sound', {value: 0});
				selfUser.settings.sound = 0;
			} else if ( button.hasClass('off') ) {
				onOff.set('html', 'ON');
				button.removeClass('off').addClass('on');
				socket.emit('change-settings-sound', {value: 1});
				selfUser.settings.sound = 1;
			}
		});
		/*<--Prepare sound settings*/
		
		/*-->Prepare friend list filter settings*/
		if ( typeof selfUser.settings.friendsFilter == 'undefined' ) {
			selfUser.settings.friendsFilter = 'online';
		}
		document.id('baddy-list').getElements('.status-filter .val.' + selfUser.settings.friendsFilter).addClass('selected');
		/*<--Prepare friend list filter settings*/
		
		/*-->Prepare favs list filter settings*/
		if ( typeof selfUser.settings.favsFilter == 'undefined' ) {
			selfUser.settings.favsFilter = 'online';
		}
		document.id('favourites-list').getElements('.status-filter .val.' + selfUser.settings.favsFilter).addClass('selected');
		/*<--Prepare favs list filter settings*/
		
		/*-->Prepare font-size settings*/
		if ( typeof selfUser.settings.fontSize == 'undefined' ) {
			selfUser.settings.fontSize = 'small';
		}
		setFontSize(selfUser.settings.fontSize)
		
		document.id('chat-status-menu').getElements('.change-font-size .font-size.').addEvent('click', function() {
			document.id('chat-status-menu').getElements('.change-font-size .font-size').removeClass('selected');
			var size = this.getProperty('class').replace('font-size', '').trim();
			selfUser.settings.fontSize = size;
			setFontSize(selfUser.settings.fontSize);
			socket.emit('change-settings-font-size', {value: selfUser.settings.fontSize});
			
			//Scrolling down opened chat window text. cause after resizing fonts
			//scroll goes up
			/*-->*/
			activeChat = $$('.active_chat.showen')[0];
			if ( activeChat ) {
				activeChat.getElement('.messaging').scrollTo(0, activeChat.getElement('.messaging').getScrollSize().y);
			}
			/*<--*/
			
			
			
		});
		/*<--Prepare font-size settings*/
		
		/*-->Prepare PM allow settings*/
//		var valuesBlock;
//		if ( selfUser.userType == 'escort' ) {
//			valuesBlock = $$('#chat-status-menu .pm-allow-values.models')[0];
//		} else {
//			valuesBlock = $$('#chat-status-menu .pm-allow-values.members')[0];
//		}
//		setPmAllow(selfUser.pmAllow);
//		
//		valuesBlock.setStyle('display', 'block');
//		
//		valuesBlock.getElements('a').addEvent('click', function() {
//			if ( this.get('rel') == 'all' ) {
//				valuesBlock.getElements('a').removeClass('selected');
//				this.addClass('selected')
//			} else {
//				
//				if ( this.hasClass('selected') ) {
//					this.removeClass('selected');
//				} else {
//					this.addClass('selected');
//				}
//				
//				if ( valuesBlock.getElements('a.selected[rel!="all"]').length ) {
//					valuesBlock.getElements('a[rel="all"]').removeClass('selected');
//				} else {
//					valuesBlock.getElements('a[rel="all"]').addClass('selected');
//				}
//			}
//			
//			socket.emit('change-settings-pm-allow', {pmAllow: valuesBlock.getElements('.selected').get('rel')});
//		});
		
		/*<--Prepare PM allow settings*/
		
	}
	
	blinkPageTitle = function(message)
	{
		var originalTitle = window.document.title;
		
		if ( ! originalTitle.length ) return;
		if ( typeof blinkTimer != 'undefined' ) clearInterval(blinkTimer);
		blinkTimer = setInterval(function() {
			if ( window.document.title == message ) {
				window.document.title = originalTitle;
			} else {
				window.document.title = message;
			}
		}, 1000);
		window.addEvent('focus', function() {
			if ( typeof blinkTimer != 'undefined' ) clearInterval(blinkTimer);
			document.title = originalTitle;
			this.removeEvents('click');
		});
	}
	
	this.emitTypingStart = function(userId)
	{
		if ( selfUser.status == 'invisible' ) return;
		
		socket.emit('typing-start', {
			userId : userId
		});
	}
	this.emitTypingEnd = function(userId)
	{
		if ( selfUser.status == 'invisible' ) return;
		
		socket.emit('typing-end', {
			userId : userId
		});
	}
	
	this.notification = function(message, userId)
	{
		if ( typeof userId == 'undefined' ) userId = false;
		
		if ( userId && ! onlineUsersObj[userId] ) {userId = false;}
		
		var notice = new Element('div', {
			'class' : 'notification'
		});
		
		if ( userId ) {
			new Element('img', {
				src : onlineUsersObj[userId].avatar
			}).inject(notice);
		}
		new Element('div', {
			'html' : message
		}).inject(notice);
		
		var ef = new Fx.Morph(notice, {
			duration : 500
		});

		new Element('div', {
			'class' : 'clear'
		}).inject(document.id('chat-notifications-container'));
		
		notice.inject(document.id('chat-notifications-container'));
		
		ef.start({
			'opacity' : [0, 1]
		});
		
		setTimeout(function() {
			ef.addEvent('complete', function() {
				notice.getPrevious('.clear').destroy();
				notice.destroy();
			});
			ef.start({
				'opacity' : [1, 0]
			});
		}, 5 * 1000);
		
	}
	
	this.tipModel = function(userId, amount)
	{
		socket.emit('tip-model', {
			userId : userId,
			amount : amount
		});
		
		var msg = 'You tipped ' + onlineUsersObj[userId].nickName + ' ' + amount + ' tokens';
		showMessage(userId, msg);
	}
	
	this.showNotices = function()
	{
		var notification = '';
		if ( selfUser.pendingFriendsCount && selfUser.pendingFriendsCount > 0 ) {
			notification += 'You have ' + selfUser.pendingFriendsCount + ' pending friends.';
		}
		
		if ( notification.length > 0 ) {
			Cookie.write('chatnotifications', 1, {duration: 1 / 24});//for 1 hour
			selfObj.notification(notification, selfUser.userId);
		}
	}
	
	this.trackConnection = function()
	{
		var self = this;
		setInterval(function() {
			if ( ! socket.socket.connected && reconnectOnDisconnect ) {
				socket.disconnect();
				self.connect();
			}
		}, 10000);
	}
	
	setFontSize = function(size)
	{
		document.id('chat-status-menu').getElements('.change-font-size .font-size.').removeClass('selected');
		document.id('chat-wrapper').removeClass('small').removeClass('large');
		
		document.id('chat-status-menu').getElement('.change-font-size .font-size.' + size).addClass('selected');
		document.id('chat-wrapper').addClass(size);
	}
	
	setPmAllow = function(pmAllow)
	{
		var valuesBlock;
		if ( selfUser.userType == 'escort' ) {
			valuesBlock = $$('#chat-status-menu .pm-allow-values.models')[0];
		} else {
			valuesBlock = $$('#chat-status-menu .pm-allow-values.members')[0];
		}
		
		valuesBlock.getElements('a').removeClass('selected');
		
		pmAllow.each(function(el, i) {
			valuesBlock.getElements('a[rel=' + el + ']').addClass('selected');
		});
	}
	
	initChatBox = function()
	{
		//$('#baddy-list .baddy_listing').jScrollPane();
	}
	
	isPopupOpened = function(userId)
	{
		cook = Cookie.read('chatPopups');
		cook = JSON.parse(cook);
		
		if ( cook && cook[userId] ) {
			return true;
		} else {
			return false;
		}
	}
	
	initAudio = function()
	{
		if ( "Audio" in window && browser.indexOf('safari') == -1 ) {
			audio = new Audio();
			if( !!(audio.canPlayType && audio.canPlayType('audio/ogg; codecs="vorbis"').replace(/no/, '')) )
				audio.ext = 'ogg';
			else if( !!(audio.canPlayType && audio.canPlayType('audio/mpeg;').replace(/no/, '')) )
				audio.ext = 'mp3';
			else if( !!(audio.canPlayType && audio.canPlayType('audio/mp4; codecs="mp4audio.40.2"').replace(/no/, '')) )
				audio.ext = 'm4a';
			else
				audio.ext = 'mp3';

			return;
		}

	}
	
	playAudio = function(name)
	{
		if (selfUser.settings && selfUser.settings.sound != 1) return;
		
		if ( typeof audio != 'undefined' && audio.play ) {
			audio.src = "/js/fchat/sounds/" + name + '.' + audio.ext;
			audio.play()
		}
	}
	
	filterBaddyList = function()
	{
		var availSelected = document.id('baddy-list').getElement('.status-filter .val.selected');
		var search = document.id('baddy_list_search').get('value');
		
		if ( search == 'Search' ) search = '';
		
		if ( search.length == 0 && availSelected.hasClass('all') ) {
			document.id('baddy-list').getElements('.listing li').setStyle('display', 'block');
			return;
		}
		
		if ( availSelected.hasClass('online') ) {
			availFilter = 'online busy away';
		} else if ( availSelected.hasClass('offline') ) {
			availFilter = 'offline';
		} else {
			availFilter = 'online busy away offline';
		}
		
		for ( i = 0; i < onlineUsers.length; i ++ ) {
			user = onlineUsers[i];
			
			if ( selfUser.friendUserIds.indexOf(user.userId) == -1 )  continue;
			
			if ( ! user.nickName.toLowerCase().contains(search) || ! availFilter.contains(user.status) ) {
				document.id('user_' + user.userId).setStyle('display', 'none');
			} else {
				document.id('user_' + user.userId).setStyle('display', 'block');
			}
		}
		
		/*onlineUsers.each(function(user) {
			if ( ! user.nickName.toLowerCase().contains(search) || ! availFilter.contains(user.status) ) {
				document.id('user_' + user.userId).setStyle('display', 'none');
			} else {
				document.id('user_' + user.userId).setStyle('display', 'block');
			}
		})*/
		
	}
	
	filterFavouritesList = function()
	{
		var availSelected = document.id('favourites-list').getElement('.status-filter .val.selected');
		var search = document.id('favourites_list_search').get('value');
		
		if ( search == 'Search' ) search = '';
		
		if ( search.length == 0 && availSelected.hasClass('all') ) {
			document.id('favourites-list').getElements('.listing li').setStyle('display', 'block');
			return;
		}
		
		if ( availSelected.hasClass('online') ) {
			availFilter = 'online busy away';
		} else if ( availSelected.hasClass('offline') ) {
			availFilter = 'offline';
		} else {
			availFilter = 'online busy away offline';
		}
		
		for ( i = 0; i < onlineUsers.length; i ++ ) {
			user = onlineUsers[i];
			
			if ( selfUser.favouritesIds.indexOf(user.userId) == -1 || selfUser.friendUserIds.indexOf(user.userId) != -1 )  continue;
			
			if ( ! user.nickName.toLowerCase().contains(search) || ! availFilter.contains(user.status) ) {
				document.id('user_' + user.userId).setStyle('display', 'none');
			} else {
				document.id('user_' + user.userId).setStyle('display', 'block');
			}
		}
	}
	
	//Bringing to front element increasing z-index
	bringToFront = function(element)
	{
		$$('.baddy_list, .active_chat').setStyle('z-index', 100);
		element.setStyle('z-index', 101);
	}
	
	getProfileUrl = function(user)
	{
		var url = 'http://' + host + '/' + user.profileUrl;
		
		return url;
	}
	
	getChatRoomUrl = function(user)
	{
		var url = 'http://' + host + '/' + user.chatRoomUrl;
		
		return url;
	}
	
	hideStatusMenu = function()
	{
		document.id('chat-status-menu').setStyle('display', 'none');
		document.id('chat-status').removeClass('opened');
	}
	
	showStatusMenu = function()
	{
		document.id('chat-status-menu').setStyle('display', 'block');
		document.id('chat-status').addClass('opened');
	}
	
	changeStatus = function(status) 
	{
		var oldStatus = selfUser.status;
		selfUser.status = status;
		
		/*-->If user changes status to offline - change also bottom taskbar*/
		if ( status == 'offline' ) {
			notAvailableToolbar();
			return;
		}
		/*<--If user changes status to offline - change also bottom taskbar*/
		
		/*-->If user changes status from offline - change back taskabar*/
		if ( oldStatus == 'offline' ) {
			availableToolbar();
		}
		/*<--If user changes status from offline - change back taskabar*/
		
		$$('#chat-status .status-icon')
			.removeProperty('class')
			.addClass('status-icon ' + status);
	}
	
	_ = function(key, quote)
	{
		if ( ! quote ) quote = {};
		
		if ( chati18n && chati18n[key] ) {
			translation = chati18n[key];
			for( i in quote ) {
				var reg = new RegExp('%%' + i + '%%', 'g');
				translation = translation.replace(reg, quote[i]);
			}
			return translation;
		} else {
			return key;
		}
	}
	
	initChatBox();
	this.connect();
	this.initListeners();
	initAudio();
}