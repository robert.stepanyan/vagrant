//socket.io lib http://camplace.com/scripts/chat/socket-io.js

//Connection to the nodeJS serve

socket = io.connect('http://chat.pluginnetwork.com:8899', {});

//Authentication
//After establishing connection fire “auth” event with params 
socket.emit('auth', {
	sid: userId, 
	uHash: uHash,
	siteId : siteId
});
//Where  uHash = md5(userId + “laskjasldkjaslkdjasldjlajasdljasdlkjasdlkjasdldjaslasdjlasdjasdljasdlkj”);


//If you pass authentication you will receive “auth-complete” event with params
socket.on('auth-complete', function(data){
	
});

//Where
data = {
	auth: false/true,
	userInfo: userInfo
}

//userInfo contains
//userId,
//nickName,
//userType - escort/member. escort eq. model. comes from escortforum version :)
//status - //online, offline, away, busy
//avatar,
//friendUserIds,
//favouritesIds,
//pendingFriendsUserIds,
//notConfirmedFriendUserIds,
//pmAllow,
//hasTokens,
//isPremium,
//isPaidMember,
//memberStatus,
//ordering,
//isBroadcasting,
//camStatus,
//displayName,
//profileUrl,
//chatRoomUrl,
//ignoreUserIds


//“auth-complete” data example
{
"auth": true,
"userInfo":
	{
		"userId":12664,
		"nickName":"vaha",
		"userType":"member",
		"status":"online",
		"avatar":"http://content2.camplace.com/static/avatar_12664_av_0_40x30_10177_nohash.jpg",
		"friendUserIds":[10007,10831,12535],
		"favouritesIds":[14873,15103,15288,17624,10008,10008,45270,15414],
		"pendingFriendsUserIds":[],
		"notConfirmedFriendUserIds":[],
		"pmAllow":["all"],
		"hasTokens":true,
		"isPremium":true,
		"isPaidMember":false,
		"memberStatus":"Premium",
		"isBroadcasting":false,
		"camStatus":"",
		"displayName":"vaha",
		"profileUrl":"handle/redirect.ashx?userid=12664",
		"chatRoomUrl":"handle/redirect.ashx?userid=12664&chat=true",
		"siteId":"80031",
		"ignoreUserIds":[],
		"pendingFriendsCount":0,
		"isFirstSession":false,
		"pendingCamRequests":[],
		"settings":{
			"openDialogs":[11054,12664],
			"activeDialog":195386,
			"availability":1,
			"friendsFilter":"all",
			"favsFilter":"all",
			"sound":0,
			"fontSize":"small",
		}
	}
}

//After that, you will get 
//"online-users-list" event with your favourites and friends list,
//"open-dialogs" event with users that you have opened dialogs,
//"pending-friends" event with users which sent you friend request,

//for sending message fire "message-sent" event
socket.emit('message-sent', {
	userId : id,
	message: message
});

//Listen for "new-message" event to receive messages
socket.on('new-message', function(data){
	
});

//data contains
{
	message: newMessageData, 
	senderData: userInfo
}

//Where
newMessageData = {
	body : message,
	userId : userId,
	date : date
}

//Listen for "new-messages" event for messages array. Fires only one time after auth-complete




//Fire events 
//"conversation-read" - to mark conversation as read,
//"typing-start", "typing-end" - notfy about typing status,
//"dialog-opened", "dialog-closed" - when opening and closing dialog, 
//"dialog-activated", "dialog-activated" - when focus and blur dialog,
//"add-favourite", "remove-favourite",
//"add-friend", "remove-friend",
//"friend-request-confirm", "friend-request-decline" - to confirm/decline friend request,
//for all these events send userId - {userId: 12664}


//Fire "change-settings-sound" to turn on/off sound
//Params {value: value //[0, 1]}

//After you fire "change-settings-sound" server will fire back "settings-sound-changed" event
//Params {value: value //[0, 1]}

//Fire "change-settings-friends-filter" to change friends tab default filter value
//Params {value: value //["all", "online", "offline"]}

//Fire "change-settings-favs-filter" to change favs tab default filter value
//Params {value: value //["all", "online", "offline"]}

//Fire "change-settings-pm-allow" to change Pm allow value
//Params {pmAllow: value //["all", "onlygirls", "onlyfriends", "favorites", "withtokens"]}

//After you fire "change-settings-pm-allow" server will fire back "settings-pm-allow-changed" event
//Params {pmAllow: value //["all", "onlygirls", "onlyfriends", "favorites", "withtokens"]}



//Fire "tip-model" to notify model about tip.(Attn. doens't do any transaction)
//Actualy tip done on backend side(Steve's side).
//params to send on "tip-model" event
{
	userId: 10668, //Model user Id
	amount: 250, //Amount
	transactionId: 123456 //Transaction id to check if that tip exists.
}


//Fire "status-changed" to change PM status
//params
{
	status: status //["online", "offline", "away", "busy"]
}


//Listen "user-status-changed" to track all users statuses
//You will receive 
{
	userId: userId, 
	status: status //["online", "offline", "away", "busy"]
}

//Listen "new-message-self". Server will fire it to user's other connections 
//to sync messages


//Listen to events "typing-start", "typing-end", "convesation-read" with params {userId: userId}


//After you fire "add-favourite" or "remove-favourite"
//You will get "favourite-added", "favourite-removed" on success
//All user's other connections will receive these event's too
//params
{
	favouriteUserId: userId,
	favUser: userInfo
}


//Same with "friend-added", "friend-removed" events 
//params
{
	friendUserId: userId,
	friendUser: userInfo
}


//After you fire "friend-request-confirm" or "friend-request-decline"
//You will get "friend-request-confirmed", "friend-request-declined" on success
//All user's other connections will receive these event's too
//params {userId: userId}


//If user sent message to the model/member with restrictions on "PM allow" ["all", "onlygirls", "onlyfriends", "favorites", "withtokens"]
//server will fire "pm-allow" event

//Basic members can not pm models
//After sending message server will fire "restr-member-to-model" event

//Basic members can start only 5 conversations with members during 1 hour
//After 6th message to member server will fire "conversation-limit" event