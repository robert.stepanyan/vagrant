var config = require('../configs/config.js').get('development');
var redis = require('redis');

function SettingsManager2() {
	
	client = redis.createClient();
	client.auth(config.redis.password);
	client.select('user_settings');
	
	/*-->open dialogs logic*/
	this.addOpenDialog = function(userId, dialogId, callback)
	{
		getUserSettings(userId, function(settings) {
			openDialogs = settings['openDialogs']
			if ( typeof openDialogs == 'undefined' ) {
				settings['openDialogs'] = [dialogId]
			} else {
				for(var i = 0; i < openDialogs.length; i++) {
					if ( openDialogs[i] == dialogId ) {
						return;
					}
				}
				settings['openDialogs'].push(dialogId);
			}
			setUserSettings(userId, settings, callback);
		});
	}
	
	this.removeOpenDialog = function(userId, dialogId, callback) 
	{
		getUserSettings(userId, function(settings) {
			if ( typeof settings['openDialogs'] != 'undefined' ) {
				for(i in settings['openDialogs']) {
					if (settings['openDialogs'][i] == dialogId ) {
						settings['openDialogs'].splice(i, 1);
					}
				}
				
				setUserSettings(userId, settings, callback);
			}
		});
	}
	
	this.getOpenDialogs = function(userId, callback)
	{
		getUserSettings(userId, function(settings) {
			if ( typeof settings['openDialogs'] == 'undefined' ) {
				callback(false);
			} else {
				callback(settings['openDialogs']);
			}
		});
	}
	/*<--open dialogs logic*/
	
	/*-->active dialogs logic*/
	this.setActiveDialog = function(userId, dialogId, callback)
	{
		getUserSettings(userId, function(settings) {
			settings['activeDialog'] = dialogId;
			setUserSettings(userId, settings, callback);
		});
	}
	
	this.removeActiveDialog = function(userId, callback)
	{
		getUserSettings(userId, function(settings) {
			settings['activeDialog'] = false
			setUserSettings(userId, settings, callback);
		});
	}
	
	this.getActiveDialog = function(userId, callback)
	{
		getUserSettings(userId, function(settings) {
			if ( typeof settings['activeDialog'] == 'undefined' ) {
				callback(false)
			} else {
				callback(settings['activeDialog']);
			}
		});
	}
	/*<--active dialogs logic*/
	
	/*-->Availability logic*/
	this.setAvailability = function(userId, availability, callback) {
		if ( availability != 0 ) {
			availability = 1;
		} else {
			availability = 0;
		}
		 
		getUserSettings(userId, function(settings) {
			settings['availability'] = availability;
			setUserSettings(userId, settings, callback);
		});
	}
	
	this.getAvailability = function(userId, callback) {
		getUserSettings(userId, function(settings) {
			if ( typeof settings['availability'] == 'undefined' ) {
				callback(1)
			} else {
				callback(settings['availability']);
			}
		});
	}
	/*<--Availability logic*/
	
	
	getUserSettings = function(userId, callback)
	{
		client.get(userId, function(err, res) {
			if ( err ) {
				console.log('Error: ' + err);
			} else {
				if ( typeof callback == 'function' ) {
					if ( ! res ) {
						callback({});
					} else {
						callback(JSON.parse(res));
					}
				}
			}
		});
	}
	
	setUserSettings = function(userId, settings, callback)
	{
		client.set(userId, JSON.stringify(settings), function(err, res) {
			if ( err ) {
				console.log('Error: ' + err);
			} else {
				if ( typeof callback == 'function' ) {
					callback();
				}
			}
		});
	}
	
}

function get() {
	return new SettingsManager2();
}

exports.get = get;