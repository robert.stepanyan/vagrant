var config = require('../configs/config.js').get('production'),
	messageManager = require("./messageManager.js").get(),
	settingsManager = require("./settingsManager.js").get(),
	settingsManager2 = require("./settingsManager2.js").get(),
	userManager = require("./userManager.js").get();
	
	app = require('http').createServer().listen(8888),
	io = require('socket.io').listen(app),

	userManager.startOfflineUsersCheck(function(userIds) {
		if ( userIds.length ) {
			io.sockets.emit('offline-users', userIds);
		}
	});
		
function start() {
	io.sockets.on('connection', function (socket) {
		socket.on('auth', function(data) {
			userManager.getUser(data.sid, true, function(resp) {
				if ( resp.info.status == 'offline' && false ) {
					socket.emit('auth-complete', {auth: false});
					socket.disconnect('unauthorized');
					return;
				}
				userInfo = resp.info;
				
				settingsManager2.getAvailability(userInfo.userId, function(availability) {
					if ( data.forceEnable ) {
						settingsManager.setAvailability(userInfo.userId, 1);
						availability = 1;
					}
					socket.emit('auth-complete', {auth: true, availability: availability, userInfo: userInfo});
					
					if ( availability ) {
						auth(socket);
					} else {
						socket.disconnect('status:not-available');
					}
				});
				
				/*if ( data.forceEnable ) {
					settingsManager.setAvailability(userInfo.userId, 1);
					availability = 1;
				} else {
					availability = settingsManager.getAvailability(userInfo.userId);
				}
				socket.emit('auth-complete', {auth: true, availability: availability, userInfo: userInfo});
				
				if ( availability ) {
					auth(socket);
				} else {
					socket.disconnect('status:not-available');
				}*/
			});
		});
		
		socket.on('message-sent', function(messageData) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			
			user = userManager.getUser(messageData.userId, true, function(user) {
				if ( user ) {
					
					message = messageManager.clearMessage(messageData.message);
					if ( message.length > 0 ) {
						messageManager.storeMessage(socket.userInfo.userId, user.info.userId, message);

						sockets = user.sockets;

						newMessageData = {
							body : message,
							userId : socket.userInfo.userId,
							date : new Date().getTime()
						}
						for(var i = 0; i <= sockets.length - 1; i++) {
							io.sockets.socket(sockets[i]).emit('new-message', {message: newMessageData, senderData: socket.userInfo})
						}
					}
				}
			});
		});
		
		socket.on('dialog-opened', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			//showing todays history
			date = new Date();
			date.setHours(0);
			date.setMinutes(0);
			date.setSeconds(0);
			
			messageManager.getConversation(socket.userInfo.userId, data.userId, date, function(messages) {
				if ( messages.length ) {
					senderInfo = userManager.getUser(data.userId, true, function(user) {
						socket.emit('message-history', {messages: messages, senderInfo : user.info});
					})
				}
				
			});
			settingsManager2.addOpenDialog(socket.userInfo.userId, data.userId);
		});
		
		socket.on('dialog-closed', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			settingsManager2.removeOpenDialog(socket.userInfo.userId, data.userId);
		});
		
		socket.on('dialog-activated', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			settingsManager2.setActiveDialog(socket.userInfo.userId, data.userId);
		});
		
		socket.on('dialog-deactivated', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			settingsManager2.removeActiveDialog(socket.userInfo.userId);
		});
		
		socket.on('availability-changed', function($availability) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			
			if ( $availability ) {
				settingsManager.setAvailability(socket.userInfo.userId, 1);
			} else {
				settingsManager.setAvailability(socket.userInfo.userId, 0);
				//Emitting to all sockets about availability changed
				user = userManager.getUser(socket.userInfo.userId);
				
				if ( user ) {
					/*sockets = user.sockets;
					for(var i = 0; i < sockets.length; i++) {
						io.sockets.socket(sockets[i]).emit('chat-off', {});
						
						io.sockets.socket(user.sockets[i]).disconnect('chat disabled');
					}*/
					while ( user.sockets.length > 0 ) {
						io.sockets.socket(user.sockets[0]).emit('chat-off', {});
						io.sockets.socket(user.sockets[0]).disconnect('chat disabled');
					}
				}
			}
		});
		
		socket.on('conversation-read', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			messageManager.markAsRead(socket.userInfo.userId, data.userId);
		});
		
		socket.on('typing-start', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			
			user = userManager.getUser(data.userId);
			if ( user ) {
				for(var i = 0; i <= user.sockets.length - 1; i++) {
					io.sockets.socket(user.sockets[i]).emit('typing-start', {userId : socket.userInfo.userId});
				}
			}
		});
		
		socket.on('typing-end', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			
			user = userManager.getUser(data.userId);
			if ( user ) {
				for(var i = 0; i <= user.sockets.length - 1; i++) {
					io.sockets.socket(user.sockets[i]).emit('typing-end', {userId : socket.userInfo.userId});
				}
			}
		});
		
		socket.on('chat-with', function(data) {
			userManager.getUser(data.userId, true, function(user) {
				socket.emit('open-dialogs', {dialogs : [{userId: data.userId, userInfo : user.info}], activeDialog : data.userId});
			});
		});
		
		socket.on('disconnect', function() {
			if ( ! socket.userInfo ) return;
			userManager.removeSocket(socket.userInfo.userId, socket.id);
		});
	});
}
function auth(socket)
{
	socket.userInfo = userInfo;
					
	newUser = true;
	if ( userManager.getUser(userInfo.userId) ) {
		newUser = false;
	} 

	userManager.addUser(socket.id, userInfo);

	//Emit new-user only if a new user. f.ex can login from another browser.
	if ( newUser ) {
		socket.broadcast.emit('new-user', userInfo);
	}


	var ou = userManager.getUsersPublicList();
	//Removing himself from list
	delete ou[userInfo.userId];

	socket.emit('online-users-list', ou);

	//Checking if has opened dialogs. 
	//if yes getting userInfo and emiting open-dialogs event
	settingsManager2.getOpenDialogs(userInfo.userId, function(openDialogs) {
		od = [];
		if ( openDialogs ) {
			userManager.getUsers(openDialogs, true, function(users) {
				for(var i = 0; i <= openDialogs.length- 1; i++) {
					userId = openDialogs[i];
					if ( users[userId] ) {
						od.push({
							'userId' : userId,
							'userInfo' : users[userId].info
						});
					}
				}
				settingsManager2.getActiveDialog(userInfo.userId, function(activeDialog) {
					socket.emit('open-dialogs', {dialogs : od, activeDialog : activeDialog});
				});
			});
		}
	});

	//Checking if has new messages. if yes emiting new-message event.
	messageManager.getNewMessagesCount(userInfo.userId, function(messages) {
		if ( messages && messages.length ) {
			userIds = [];
			//gathering user ids to call getUsers
			for ( var i = 0; i < messages.length; i++ ) {
				userIds.push(messages[i].userId);
			}
			//getting users and merging with messages
			userManager.getUsers(userIds, true, function(users) {
				for ( var i = 0; i < messages.length; i++ ) {
					if ( users[messages[i].userId] ) {
						messages[i].senderInfo = users[messages[i].userId].info;
					}
				}
				socket.emit('new-messages', messages);
			});
		}
	});
}
exports.start = start;