var fs = require('fs');
var http = require('http');
var querystring = require('querystring');
var config = require('../configs/config.js').get('production');
function DictionaryManager() {
	var dictionary = {};
	
	this.init = function()
	{
		this.initDictionaryCache();
	}
	
	this.getDictionary = function(lang, callback)
	{
		if ( dictionary[lang] ) {
			callback(dictionary[lang]);
			return;
		}
		
		fs.readFile(config.common.dictionaryCachePath + 'dic_' + lang, function(err, data) {
			if ( err ) {
				callback(false);
				return;
			}
			
			callback(JSON.parse(data));
			
		});
	}
	
	this.initDictionaryCache = function()
	{
		var self = this;
		setInterval(function() {
			var tmp = {};
			self.fetchDictionary(function(resp) {
				delete resp.$id;
				for(lang in resp) {
					delete resp[lang].$id;


					tmp[lang] = {};

					for( key in resp[lang] ) {
						tmp[lang][key] = resp[lang][key].ResourceValue;
					}

					fs.writeFile(config.common.dictionaryCachePath + 'dic_' + lang, JSON.stringify(tmp[lang]), function(err) {
						if ( err ) { 
							console.log(err); 
						}
					});
				}
				dictionary = tmp;
				console.log('Dictionary cache updated');
			});
			
		}, config.common.dictionaryCacheTime);
	}
	
	this.fetchDictionary = function(callback)
	{
		var post_data = {
			'compilation_level' : 'ADVANCED_OPTIMIZATIONS',
			'output_format': 'json',
			'output_info': 'compiled_code',
			'warning_level' : 'QUIET'
		};


		post_data = querystring.stringify(post_data);

		var post_options = {
			host: 'admin.camplace.com',
			port: '80',
			path: '/Api/GetTranslationsbygroup/' + config.api.translationGroupId,
			method: 'GET',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
				'Content-Length': post_data.length,
				'Authorization' : "Basic " + new Buffer('TICKET:' + config.api.accountId + ':' + config.api.secretCode ).toString("base64")
			}
			
		};

		var post_req = http.request(post_options, function(res) {
			var output = '';
			
			res.setEncoding('utf8');

			res.on('data', function (chunk) {
				output += chunk;
			});
			
			res.on('end', function() {
				try {
					callback(JSON.parse(output));
				} catch(err) {
					console.log(err);
				}
			});
			
			res.on("error", function(e) {
				console.log("Goto error: " + e.message);
			});
		});

		post_req.write(post_data);
		post_req.end();
	}
	
	this.init();
}

function get() {
	return new DictionaryManager();
}

exports.get = get;