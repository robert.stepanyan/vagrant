var config = require('../configs/config.js').get('production'),
	messageManager = require("./messageManager.js").get(),
	settingsManager = require("./settingsManager.js").get(),
	userManager = require("./userManager.js").get();
	dictionaryManager = require("./dictionaryManager.js").get(),
	
	app = require('http').createServer(handler).listen(8888),
	io = require('socket.io').listen(app, {log: false}),
	url = require('url'),
	crypto = require('crypto');
	
	function handler(request, response) {
		var queryData = url.parse(request.url, true);

		var query = queryData.query;
		
		
		
		switch ( queryData.pathname ) {
			case '/node-chat/signout' :
				//Changing availability
				var uid = query.userId,
				hash = query.hash;

				if ( ! uid || ! hash || crypto.createHash('md5').update(uid + '-' + config.common.key).digest("hex") != hash)  {
					response.writeHead(200, {"Content-Type": "text/plain"});
					response.end("sta=error");
					return;
				} 

				userManager.getUser(uid, false, function(user) {
					if ( user ) {
						if ( user.status != 'invisible' && user.status != 'offline' ) {
							emitToGroup(false, 'user-status-changed', {userId : uid, status : 'offline'});
						}
						
						var sSockets = user.sockets.slice(0);//Copying array
						for ( i = 0; i < sSockets.length; i++ ) {
							io.sockets.socket(sSockets[i]).emit('logout');
							io.sockets.socket(sSockets[i]).disconnect('logout');
						}
						userManager.removeUser(uid);
					}


					response.writeHead(200, {"Content-Type": "text/plain"});
					response.end("sta=ok");
				});
				break;
			case '/node-chat/profile-changed' :
				//Clearing cache
				var uid = query.userId,
				hash = query.hash;

				if ( ! uid || ! hash || crypto.createHash('md5').update(uid + '-' + config.common.key).digest("hex") != hash)  {
					response.writeHead(200, {"Content-Type": "text/plain"});
					response.end("sta=error");
					return;
				} 
				
				if ( userManager.getUser(uid) ) {
					userManager.getOfflineUserById(uid, function(resp) {
						userManager.updateUserInfo(resp);
					});
				}
					
				
				response.writeHead(200, {"Content-Type": "text/plain"});
				response.end("sta=ok");
				break;
			case '/node-chat/localization' :
				var lang = query.lang;
				
				if ( ! lang || lang.length == 0 ) {
					response.writeHead(200, {"Content-Type": "text/plain"});
					response.end("sta=errorrr");
					return;
				}
				
				dictionaryManager.getDictionary(lang.toLowerCase(), function(dic) {
					response.writeHead(200, {"Content-Type": "application/javascript"});
					var output = '$(document).ready(function() { chati18n = {';
					var pairs = [];
					for( i in dic ) {
						if ( typeof dic[i] ) {
							pairs.push(i + ': "' + dic[i].replace(/"/g, '\'') + '"' + '\n');
						}
					}
					output += pairs.join(',') + '}; })';
					
					response.end(output);
					return;
				});
				break;
			default :
				response.writeHead(200, {"Content-Type": "text/plain"});
				response.end("sta=error");
				break;
		}
	}
	
	settingsManager.startSettingsBackup(function() {
		console.log('User settings backup done');
	});

	userManager.startOfflineUsersCheck(function(removedUsers) {
		for( var i = 0; i < removedUsers.length; i++ ) {
			if ( removedUsers[i].status != 'invisible' && removedUsers[i].status != 'offline' ) {
				emitToGroup(false, 'user-status-changed', {userId : removedUsers[i].userId, status : 'offline'});
			}
			userManager.notifyOffline(removedUsers[i].userId);
		}
	});
	
	//Sending online users id to their system
	userManager.startSendingOnlineList();
		
function start() {
	io.sockets.on('connection', function (socket) {
		socket.on('auth', function(data) {
			if ( crypto.createHash('md5').update(data.sid + config.common.camplaceSecret).digest("hex") != data.uHash ) {
				socket.emit('auth-complete', {auth: false});
				socket.emit('logout');
				socket.disconnect('unauthorized');
				return;
			}
			
			userManager.getOfflineUserById(data.sid, function(resp) {
				if ( ! resp ) {
					
					socket.emit('auth-complete', {auth: false});
					socket.emit('logout');
					socket.disconnect('unauthorized');
					return;
				}
				userInfo = resp.info;
				
				newUser = true;
				if ( userManager.getUser(userInfo.userId) ) {
					newUser = false;
				}
				
				userInfo.status = resp.status; //Setting actual status
				userInfo.pendingFriendsCount = resp.pendingFriendsCount;
				userInfo.siteId = data.siteId;
				userInfo.domain = data.domain;
				userInfo.isFirstSession = newUser;

				if ( data.forceEnable ) {
					settingsManager.setAvailability(userInfo.userId, 1);
					availability = 1;
				} else {
					availability = settingsManager.getAvailability(userInfo.userId);
				}
				userInfo.settings = settingsManager.get(userInfo.userId);
				socket.emit('auth-complete', {auth: true, availability: availability, userInfo: userInfo});
				
				socket.userInfo = userInfo;
				userManager.addUser(socket.id, userInfo);

				if ( userInfo.status == 'offline' ) {
					return;
				}

				if ( newUser && userInfo.status != 'invisible' ) {
					emitToGroup(false, 'user-status-changed', {userId : userInfo.userId, status : userInfo.status});
				}

				sendNotifications(socket);
					
			});
		});
		
		socket.on('message-sent', function(messageData) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			
			userManager.getUser(messageData.userId, true, function(user) {
				if ( user ) {
					var allow = checkPmAllow(socket.userInfo, user.info);
					if ( ! allow ) {
						socket.emit('pm-allow', {dialogId : messageData.userId});
					} else {
						message = messageManager.clearMessage(messageData.message);
						if ( message.length > 0 ) {
							
							
							restrictions = checkRestrictions(socket.userInfo, user.info, settingsManager);
							
							if ( ! restrictions.allow ) {
								socket.emit(restrictions.reason, {dialogId : messageData.userId});
								return;
							}
							
							messageManager.storeMessage(socket.userInfo.userId, user.info.userId, message);
							sockets = user.sockets;
							
							newMessageData = {
								body : message,
								userId : socket.userInfo.userId,
								date : new Date().getTime()
							}
							
							var userInfo = socket.userInfo;
							
							/*-->Sending user's own message to his other sockets*/
							var selfSockets = userManager.getUser(socket.userInfo.userId).sockets;
							if ( selfSockets ) {
								for ( var i = 0; i < selfSockets.length; i++ ) {
									if ( selfSockets[i] != socket.id ) {
										io.sockets.socket(selfSockets[i]).emit('new-message-self', {message: newMessageData, userId: user.info.userId});
									}
								}
							}
							/*<--Sending user's own message to his other sockets*/
							
							//Do not send event if offline
							if ( user.status == 'offline' ) return;
							
							if ( userInfo.status == 'invisible' ) {
								userInfo.status = 'offline';
							}
							for(var i = 0; i < sockets.length; i++) {
								io.sockets.socket(sockets[i]).emit('new-message', {message: newMessageData, senderData: userInfo})
							}
						}
					}
				}
			});
		});
		
		socket.on('dialog-opened', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			//showing todays history
			date = new Date();
			date.setHours(0);
			date.setMinutes(0);
			date.setSeconds(0);
			
			messageManager.getConversation(socket.userInfo.userId, data.userId, date, function(messages) {
				if ( messages.length ) {
					senderInfo = userManager.getUser(data.userId, true, function(user) {
						socket.emit('message-history', {messages: messages, senderInfo : user.info});
					})
				}
				
			});
			settingsManager.addOpenDialog(socket.userInfo.userId, data.userId);
		});
		
		socket.on('message-history', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			//showing todays history
			date = new Date();
			date.setHours(0);
			date.setMinutes(0);
			date.setSeconds(0);
			
			
			messageManager.getConversation(socket.userInfo.userId, data.userId, date, function(messages) {
				if ( messages.length ) {
					senderInfo = userManager.getUser(data.userId, true, function(user) {
						socket.emit('message-history', {messages: messages, senderInfo : user.info});
					})
				}
			});
		});
		
		socket.on('dialog-closed', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			settingsManager.removeOpenDialog(socket.userInfo.userId, data.userId);
		});
		
		socket.on('dialog-activated', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			settingsManager.setActiveDialog(socket.userInfo.userId, data.userId);
		});
		
		socket.on('dialog-deactivated', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			settingsManager.removeActiveDialog(socket.userInfo.userId);
		});
		
		socket.on('availability-changed', function(availability) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			
			var status,
				notifyUsers = true,
				user = userManager.getUser(socket.userInfo.userId);
			
			if ( user.status == 'invisible' ) {
				notifyUsers = false;
			}
			
			if ( availability ) {
				//settingsManager.setAvailability(socket.userInfo.userId, 1);
				status = 'online';
				
				userManager.setStatus(socket.userInfo.userId, status);
				for(var i = 0; i < user.sockets.length; i++) {
					io.sockets.socket(user.sockets[i]).emit('status-changed', {status: 'online'});
					
					sendNotifications(io.sockets.socket(user.sockets[i]));
				}
				
				
			} else {
				//settingsManager.setAvailability(socket.userInfo.userId, 0);
				status = 'offline';
				//Emitting to all sockets about availability changed
				
				userManager.setStatus(socket.userInfo.userId, 'offline');
				if ( user ) {
					for(var i = 0; i <= user.sockets.length - 1; i++) {
						io.sockets.socket(user.sockets[i]).emit('status-changed', {status: 'offline'});
					}
				}
			}
			
			if ( notifyUsers ) {
				emitToGroup(false, 'user-status-changed', {userId : socket.userInfo.userId, status : status});
			}
		});
		
		socket.on('status-changed', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			//Emitting to user's all sockets about status change
			user = userManager.getUser(socket.userInfo.userId);
			if ( user && data.status.length ) {
				userManager.setStatus(socket.userInfo.userId, data.status);
				for(var i = 0; i < user.sockets.length; i++) {
					io.sockets.socket(user.sockets[i]).emit('status-changed', {status: data.status});
				}
				
				status = data.status;
				if ( status == 'invisible' ) {
					status = 'offline';
				}
				emitToGroup(false, 'user-status-changed', {userId : socket.userInfo.userId, status : status});
			}
		});
		
		socket.on('conversation-read', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			messageManager.markAsRead(socket.userInfo.userId, data.userId);
			
			user = userManager.getUser(socket.userInfo.userId);
			
			if ( ! user.sockets ) return;
			
			for(var i = 0; i < user.sockets.length; i++) {
				io.sockets.socket(user.sockets[i]).emit('conversation-read', {userId: data.userId});
			}
		});
		
		socket.on('typing-start', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			
			user = userManager.getUser(data.userId);
			if ( user ) {
				for(var i = 0; i < user.sockets.length; i++) {
					io.sockets.socket(user.sockets[i]).emit('typing-start', {userId : socket.userInfo.userId});
				}
			}
		});
		
		socket.on('typing-end', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			
			user = userManager.getUser(data.userId);
			if ( user ) {
				for(var i = 0; i < user.sockets.length; i++) {
					io.sockets.socket(user.sockets[i]).emit('typing-end', {userId : socket.userInfo.userId});
				}
			}
		});
		
		socket.on('chat-with', function(data) {
			userManager.getUser(data.userId, true, function(user) {
				socket.emit('open-dialogs', {dialogs : [{userId: data.userId, userInfo : user.info}], activeDialog : data.userId});
			});
		});
		
		socket.on('chat-popup-opened', function(data) {
			settingsManager.addPopupDialog(socket.userInfo.userId, data.userId)
		});
		
		socket.on('chat-popup-closed', function(data) {
			settingsManager.removePopupDialog(socket.userInfo.userId, data.userId)
		});
		
		socket.on('tip-model', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			
			user = userManager.getUser(data.userId);
			if ( user ) {
				for(var i = 0; i < user.sockets.length; i++) {
					io.sockets.socket(user.sockets[i]).emit('tip-model', {userInfo : socket.userInfo, amount : data.amount});
				}
			}
		});
		
		socket.on('ping', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) {
				console.log('aaaaaaaaaaaaaaaaaaaaaaa', data);
				socket.emit('reconnect');
			}
		});
		
		socket.on('disconnect', function() {
			if ( typeof socket.userInfo == 'undefined' ) return;
			userManager.removeSocket(socket.userInfo.userId, socket.id);
		});
		
		socket.on('add-favourite', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			userManager.addFavourite(socket.userInfo.userId, data.userId, function(res) {
				if ( res.Success ) {
					userManager.getUser(data.userId, true, function(favUser) {
						user = userManager.getUser(socket.userInfo.userId);
						if ( user ) {
							for(var i = 0; i < user.sockets.length; i++) {
								io.sockets.socket(user.sockets[i]).emit('favourite-added', {favouriteUserId: data.userId, favUser: favUser.info});
							}
						}
					});
				}
			});
		});
		
		socket.on('remove-favourite', function(data) {
			if ( ! socket.userInfo ) return;
			userManager.removeFavourite(socket.userInfo.userId, data.userId, function(res) {
				if ( res.Success ) {
					userManager.getUser(data.userId, true, function(favUser) {
						user = userManager.getUser(socket.userInfo.userId);
						if ( user ) {
							for(var i = 0; i < user.sockets.length; i++) {
								io.sockets.socket(user.sockets[i]).emit('favourite-removed', {favouriteUserId: data.userId, favUser: favUser.info});
							}
						}
					});
				}
			});
		});
		
		socket.on('change-settings-sound', function(data) {
			if ( ! socket.userInfo ) return;
			settingsManager.setSound(socket.userInfo.userId, data.value);
			
			/*-->Emit to all sockets about settings change*/
			var selfSockets = userManager.getUser(socket.userInfo.userId).sockets;
			for ( var i = 0; i < selfSockets.length; i++ ) {
				if ( selfSockets[i] != socket.id ) {
					io.sockets.socket(selfSockets[i]).emit('settings-sound-changed', {value: data.value});
				}
			}
			/*<--Emit to all sockets about settings change*/
			
		});
		
		socket.on('change-settings-friends-filter', function(data) {
			if ( ! socket.userInfo ) return;
			settingsManager.setFriendsFilter(socket.userInfo.userId, data.value);
		});
		socket.on('change-settings-favs-filter', function(data) {
			if ( ! socket.userInfo ) return;
			settingsManager.setFavsFilter(socket.userInfo.userId, data.value);
		});
		
		socket.on('change-settings-font-size', function(data) {
			if ( ! socket.userInfo ) return;
			settingsManager.setFontSize(socket.userInfo.userId, data.value);
		});
	});
}

//Sending new-messages, online-users, opened-dialogs etc
function sendNotifications(socket)
{
	var userInfo = socket.userInfo;
	if ( ! userInfo ) return;
	/*-->Getting friends*/
	userManager.getUsers(userInfo.friendUserIds.concat(userInfo.favouritesIds), true, function(friends) {
		//We need only info of users(public info)
		pFriends = {};
		for ( var i in friends ) {
			pFriends[friends[i].info.userId] = friends[i].info;
		}
		socket.emit('online-users-list', pFriends);
		
		
		//Checking if has opened dialogs. 
		//if yes getting userInfo and emiting open-dialogs event
		openDialogs = settingsManager.getOpenDialogs(userInfo.userId);
		//popupDialogs = settingsManager.getPopupDialogs(userInfo.userId);

		od = [];
		activeDialog = false;
		if ( openDialogs ) {
			userManager.getUsers(openDialogs, true, function(users) {
				for(var i = 0; i < openDialogs.length; i++) {
					userId = openDialogs[i];
					if ( users[userId] ) {

						//IF user is disabled remove him from opened dialogs
						if ( users[userId].activeStatus == 'NOT ACTIVE' ) {
							settingsManager.removeOpenDialog(userInfo.userId, userId);
							continue;
						}

						od.push({
							'userId' : userId,
							'userInfo' : users[userId].info
						});
					} else {
						settingsManager.removeOpenDialog(userInfo.userId, userId);
					}
				}
				activeDialog = settingsManager.getActiveDialog(userInfo.userId);
				socket.emit('open-dialogs', {dialogs : od, activeDialog : activeDialog});
			});
		}




		//Checking if has new messages. if yes emiting new-message event.
		messageManager.getNewMessagesCount(userInfo.userId, function(messages) {
			if ( messages && messages.length ) {
				userIds = [];
				//gathering user ids to call getUsers
				for ( var i = 0; i < messages.length; i++ ) {
					userIds.push(messages[i].userId);
				}

				//getting users and merging with messages
				userManager.getUsers(userIds, true, function(users) {
					for ( var i = 0; i < messages.length; i++ ) {
						if ( users[messages[i].userId] ) {
							//If user is not online, getting him from api
							//Api will return online if he is logged in
							//User can disable chat. Then we must change status from 'online' to 'offline''
							availability = settingsManager.getAvailability(messages[i].userId);
							if ( availability == 0 ) {
								users[messages[i].userId].info.status = 'offline';
							}


							messages[i].senderInfo = users[messages[i].userId].info;
						}
					}
					socket.emit('new-messages', messages);
				});
			}
		});
	});
	/*-->Collecting online friends*/
					
					

	
}

function emitToGroup(userIds, event, data)
{
	//Getting online friends and emitting them an event
	if ( userIds ) {
		userManager.getUsers(userIds, false, function(res) {
			for( var userId in res ) {
				user = res[userId];
				if ( user.status != 'offline' && user.sockets && user.sockets.length ) {
					for ( var i = 0; i < user.sockets.length; i++ ) {
						io.sockets.socket(user.sockets[i]).emit(event, data);
					}
				}
			}
		});
	} else {
		usersPrivateList = userManager.getUsersPrivateList();
		
		for ( var userId in usersPrivateList) {
			var user = usersPrivateList[userId];
			if ( user.status != 'offline' && user.sockets && user.sockets.length ) {
				for ( var i = 0; i < user.sockets.length; i++ ) {
					io.sockets.socket(user.sockets[i]).emit(event, data);
				}
			}
		}
	}
}

function checkPmAllow(senderInfo, participantInfo)
{
	
	return true;
	isFriend = false;
	
	
	for( i = 0; i < participantInfo.friendUserIds.length; i++ ) {
		if ( senderInfo.userId == participantInfo.friendUserIds[i] ) {
			isFriend = true;
			break;
		}
	}
	
	for( i = 0; i < participantInfo.favouritesIds.length; i++ ) {
		if ( senderInfo.userId == participantInfo.favouritesIds[i] ) {
			isFriend = true;
			break;
		}
	}
	
	var pmAllow = participantInfo.pmAllow;
	
	if ( participantInfo.userType == 'member' && senderInfo.userType == 'escort' ) {
		return true;
	}
	
	if ( ( pmAllow.indexOf('BASIC') == -1 && ! isFriend ) ||
		( pmAllow.indexOf('MODELS') == -1 && senderInfo.userType != 'escort' ) ||
		( pmAllow.indexOf('PREMIUM') == -1 && ! senderInfo.isPremium ) ||
		( pmAllow.indexOf('PAID') == -1 && ! senderInfo.isPaidMember )
	) {
		return false;
	}
	
	return true;
}

function checkRestrictions(senderInfo, participantInfo, settingsManager) {
	//Limitation for basic members. can start only 3 conversations per hour
	
	var result = {
		allow: true,
		reason: ''
	};
	
	//if ( senderInfo.userId != 12664 ) { return result;}
	
	if ( senderInfo.isPremium ) {
		return result;
	}
	
	isFriend = false;
	for( i = 0; i < participantInfo.friendUserIds.length; i++ ) {
		if ( senderInfo.userId == participantInfo.friendUserIds[i] ) {
			isFriend = true;
			break;
		}
	}
	
	//if friend allow anyway
	if ( isFriend || senderInfo.userType == 'escort' ) {
		console.log('allow is friend');
		result.allow = true;
		return result;
	}
	
	/*--> Do not allow basic members pm to models*/
	if ( senderInfo.userType == 'member' && senderInfo.memberStatus == 'Basic' && participantInfo.userType == 'escort' ) {
		result.allow = false;
		result.reason = 'restr-member-to-model';
		return result;
	}
	/*<-- Do not allow basic members pm to models*/
	
	
	/*-->Restriction in conversations count per hour for basic members*/
	if ( senderInfo.userType == 'member' && participantInfo.userType == 'member' ) {
		conversations = settingsManager.getConversations(senderInfo.userId);

		if ( conversations.length >= config.common.conversationsLimit ) {
			result.allow = false;
			result.reason = 'conversation-limit';

			var isAlreadyInList = false;
			for(var i = 0; i < conversations.length; i++) {
				if ( conversations[i].userId == participantInfo.userId ) {
					isAlreadyInList = true;
				}
			}

			if ( isAlreadyInList ) {
				console.log('alredy in list');
				result.allow = true;
				result.reason = '';
			}
		}
		if ( participantInfo.userType == 'member' && result.allow ) {
			settingsManager.addConversation(senderInfo.userId, participantInfo.userId);
		}
	}
	/*<--Restriction in conversations count per hour for basic members*/
	
	return result;
}

exports.start = start;