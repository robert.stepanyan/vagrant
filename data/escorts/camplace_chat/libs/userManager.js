var config = require('../configs/config.js').get('production');
var bridge = require('./bridge.js').get();

function UserManager() {
	var usersPrivateList = {}; //For internal use. Contains user info and sockets array
	var usersPublicList = {}; //Contains only user info
	var usersWithoutSockets = {};
	
	var statuses = {
		10879 : 'online',
		10880 : 'offline',
		10881 : 'away',
		10882 : 'busy',
		10883 : 'invisible',
		
		'invisible' : 10883,
		'busy' : 10882,
		'away' : 10881,
		'offline' : 10880,
		'online' : 10879
	}
	
	this.getUsersPrivateList = function() 
	{
		return usersPrivateList;
	}
	
	this.getUsersPublicList = function() 
	{
		//Returns object by reference, not by value
		//Refactor this part later
		return cloneObject(usersPublicList);
	}
	
	this.getUser = function(userId, offline, callback)
	{
		//this.debugLog('getUser', userId);
		try {
			if ( typeof offline == 'undefined' ) offline = false;
			
			
			if ( offline == true ) {
				return this.getOfflineUserById(userId, callback);
			}

			//if in privateList (is online) get from there 
			if ( typeof usersPrivateList[userId] != 'undefined' ) {
				
				//If callback is given call calback else just return value
				if ( typeof callback == 'function' ) {
					return callback(usersPrivateList[userId]);
				}
				return usersPrivateList[userId];
			}

			//if is not in privateList(is offline) and offline = false return false
			if ( ! offline ) {
				if ( typeof callback == 'function' ) {
					return callback(false);
				} else {
					return false;
				}
			}
			
		} catch(err) {
			console.log(err);
		}
	}
	
	this.getOfflineUserById = function(userId, callback)
	{
		bridge.getUserById(userId, function(data) {
			if ( typeof data == 'undefined'  ) {
				return callback(false);
			}
			callback(getUserObject(data));
		});
	}
	
	this.getOfflineUserBySessionId = function(sessionId, callback)
	{
		bridge.getUserBySessionId(sessionId, function(data) {
			callback(getUserObject(data));
		});
	}
	
	this.getUsers = function(userIds, offline, callback)
	{
		if ( ! userIds || ! userIds.length ) {
			if ( typeof callback == 'function' ) {
				callback({});
			}
			
			return false;
		}
		
		var users = {},
			c = 0;
		for ( var i = 0; i < userIds.length; i++ ) {
			this.getUser(userIds[i], offline, function(result) {
				if ( result ) {
					users[result.info.userId] = result;
				}
				c++;
				if ( c >= userIds.length ) {
					callback(users);
				}
			});
		}
		
		return false;
	}
	
	this.addUser = function(socketId, info)
	{
		this.debugLog('addUser');
		// If already have in list only add socket in array
		// Else add new element in array with userId
		if ( ! usersPrivateList[info.userId] ) { 
			usersPrivateList[info.userId] = {
				sockets: [],
				info: info,
				status: info.status
			}
		}

		usersPublicList[info.userId] = info;
		addSocket(info.userId, socketId);
	}
	
	this.removeUser = function(userId) 
	{
		delete usersPrivateList[userId];
		delete usersPublicList[userId];
		delete usersWithoutSockets[userId];
	}
	
	this.removeSocket = function(userId, socketId)
	{
		if ( ! usersPrivateList[userId] ) return;
		sockets = usersPrivateList[userId].sockets;
		for(i = 0; i <= sockets.length; i++) {
			if ( sockets[i] == socketId ) {
				usersPrivateList[userId].sockets.splice(i, 1);
				break;
			}
		}
		
		//If user has no other sockets(f.e. close browser or all tabs) put him
		//in array which we will check in some period. If he will not 
		//come back for a while we will remove him from online users list.
		if ( usersPrivateList[userId].sockets.length == 0 ) {
			usersWithoutSockets[userId] = new Date();
		}
	}
	
	function addSocket(userId, socketId)
	{
		if ( ! usersPrivateList[userId] ) return;
		
		sockets = usersPrivateList[userId].sockets;
		for(var i = 0; i <= sockets.length; i++) {
			if ( sockets[i] == socketId ) return;
		}
		
		usersPrivateList[userId].sockets.push(socketId);
		
		//If user in usersWithoutSockets list remove him from there
		if ( typeof usersWithoutSockets[userId] !== "undefined") {
			delete usersWithoutSockets[userId];
		}
	}
	
	this.startOfflineUsersCheck = function(callback)
	{
		this.debugLog('startofflineuserscheck');
		var self = this;
		setInterval(function() {
			removedUserIds = [];
			for(var userId in usersWithoutSockets) {
				if ( typeof usersPrivateList[userId] !== "undefined" && usersPrivateList[userId].sockets.length == 0 && (new Date() - usersWithoutSockets[userId]) >= 2 * 1000 ) {
					//If has no sockets more than 2 seconds remove from public and private online users list
					removedUserIds.push({
						userId : userId, 
						friendUserIds : usersPrivateList[userId].info.friendUserIds.concat(usersPrivateList[userId].info.favouritesIds), 
						status : usersPrivateList[userId].status
					});
					self.removeUser(userId);
				}
			}
			callback(removedUserIds);
		}, 10 * 1000);
	}
	
	this.startSendingOnlineList = function()
	{
		setInterval(function() {
			var ids = [],
				statusIds = [];
			for(i in usersPrivateList ) {
				console.log(usersPrivateList[i].info.nickName, usersPrivateList[i].info.status, usersPrivateList[i].sockets.length);
				ids.push(i);
				statusIds.push(statuses[usersPrivateList[i].info.status]);
			}
			if ( ids.length  ) {
				bridge.call('chatping', {userids : ids.join(','), pmstatusids : statusIds.join(',')}, function(resp) {	});
			}
		}, config.api.notifyOnlineUsersPeriod);
	}
	
	this.notifyOffline = function(userId)
	{
		this.debugLog('notifyOffline');
		bridge.call('chatpingoffline', {userid : userId}, function(resp) {	});
	}
	
	this.updateUserInfo = function(info)
	{
		if ( usersPrivateList[info.userId] ) {
			usersPrivateList[info.userId].info = info;
			usersPublicList[info.userId] = info;
		}

	}
	
	this.setStatus = function(userId, status)
	{
		this.debugLog('setStatus');
		var statusId = statuses[status];
		
		usersPrivateList[userId].status = status;
		usersPrivateList[userId].info.status = status == 'invisible' ? 'offline' : status;
		
		bridge.call('saveonlinestatus', {userid : userId, statusid : statusId}, function(res) {});
	}
	
	this.addFavourite = function(userId, favouriteUserId, callback)
	{
		this.debugLog('addFavourite');
		bridge.call('addfavorite', {userid : userId, frienduserid : favouriteUserId}, function(res) {
			callback(res);
		});
	}
	
	this.removeFavourite = function(userId, favouriteUserId, callback)
	{
		
		this.debugLog('removeFavourite');
		bridge.call('removefavorite', {userid : userId, frienduserid : favouriteUserId}, function(res) {
			callback(res);
		});
	}
	
	this.debugLog = function(message)
	{
		if ( false ) {
			console.log(message);
		}
	}
	
	getUserObject = function(data)
	{
		var pmAllow = '';
		for( i = 0; i < data.ChatSettings.length; i++) {
			if (data.ChatSettings[i].Code == 'PMAllow') {
				pmAllow = data.ChatSettings[i].Value;
			}
		}
		
		/*-->Getting string from id*/
		/*var isLoggedIn = false;
		
		if ( typeof usersPrivateList[data.UserId] != 'undefined' ) {
			isLoggedIn = true;
		}*/
		
		var pmStatus = status = statuses[data.PMStatusId];
		var onlineStatus = statuses[data.OnlineStatusId];
		
		/*if ( ( onlineStatus == 'offline' && status != 'invisible') ) {
			status = 'offline';
		}*/
		
		if ( typeof usersPrivateList[data.UserId] != 'undefined' ) {
			if ( status == 'invisible' ) {
				status = 'offline';
			}
		} else {
			status = 'offline';
		}
		
		/*<--Getting string from id*/
		user = {
			info : {
				userId : data.UserId,
				nickName : data.Chatname,
				userType : data.IsModel ? 'escort' : 'member',
				/*this is online status (online, offline, away, busy)*/
				status : status == 'invisible' ? 'offline' : status,//users must see him offline if he is invisible
				avatar : config.common.imagesHost + '?userid=' + data.UserId + '&imageindex=0&size=' + config.common.avatarSize + '&gender=' + data.Gender + '&type=av',
				friendUserIds : data.ActiveFriendUserIds,//.concat(data.FavoritedUserIds),
				favouritesIds : data.FavoritedUserIds,
				pmAllow : pmAllow,
				isPremium : data.IsPremium,
				isPaidMember : data.IsPaidMember,
				memberStatus : data.MemberStatus,
				ordering : data.LastMoveToTopDate,
				isBroadcasting : data.IsBroadcasting,
				camStatus : data.CamStatus,
				displayName : data.DisplayName,
				profileUrl: 'handle/redirect.ashx?userid=' + data.UserId,
				chatRoomUrl: 'handle/redirect.ashx?userid=' + data.UserId + '&chat=true',
				siteId : null
			},
			sockets : [],
			status : pmStatus,//for private use
			activeStatus : data.Status,//(Active, Not Active)
			pendingFriendsCount : data.PendingFriendCount,
			newMessagesCount : data.NewMessageCount
		};
		
		//If in privateList means online, put sockets in this object
		if ( usersPrivateList[data.UserId] ) {
			user.sockets = usersPrivateList[data.UserId].sockets;
		}
		
		return user;
	}
	
	function cloneObject(obj)
	{
		var c = new Object();
		for (var e in obj) {
			c[e] = obj[e];
		}
		return c;
	}
}

function get() {
	return new UserManager();
}
exports.get = get;
