var mysql = require('mysql');
var config = require('../configs/config.js').get('development');
function UserManager() {
	var usersPrivateList = {}; //For internal use. Contains user info and sockets array
	var usersPublicList = {}; //Contains only user info
	var usersWithoutSockets = {};
	
	var client = mysql.createClient({
		user:     config.db.user,
		database: config.db.database,
		password: config.db.password,
		host:     config.db.host,
		port: 3306
	});
	
	this.getUsersPrivateList = function() 
	{
		return usersPrivateList;
	}
	
	this.getUsersPublicList = function() 
	{
		//Returns object by reference, not by value
		//Refactor this part later
		return cloneObject(usersPublicList);
	}
	
	//offline true - if is not in usersPrivateList(is offline) select from db
	//offline false - get only from usersPrivateList
	this.getUser = function(userId, offline, callback) 
	{
		if ( typeof offline == 'undefined' ) offline = false;
		
		//if in privateList (is online) get from there 
		if ( typeof usersPrivateList[userId] != 'undefined' ) {
			
			//If callback is given call calback else just return value
			if ( typeof callback == 'function' ) {
				return callback(usersPrivateList[userId]);
			}
			
			return usersPrivateList[userId];
		}
		
		//if is not in privateList(is offline) and offline = false return false
		if ( ! offline ) {
			return false;
		}
		
		//else select from db
		//here callback is required
		return this.getOfflineUser(userId, callback);
	}
	
	this.getUsers = function(userIds, offline, callback)
	{
		if ( ! userIds || ! userIds.length ) {
			if ( typeof callback == 'function' ) {
				callback({});
			}
			
			return false;
		}
		
		var users = {},
			c = 0;
		console.log(userIds);
		for ( var i = 0; i <= userIds.length - 1; i++ ) {
			this.getUser(userIds[i], offline, function(result) {
				if ( result ) {
					users[result.info.userId] = result;
				}
				c++;
				if ( c >= userIds.length ) {
					callback(users);
				}
			});
		}
		
		return false;
	}
	
	this.getOfflineUser = function(userId, callback)
	{
		
		//Getting user type
		client.query('SELECT id, user_type FROM users WHERE id = ?', [userId], function(error, result) {
			if ( result.length == 0 ) callback(false);
			
			switch(result[0].user_type) {
				case 'escort' : 
					getEscort(userId, function(result) {
						callback(result);
					});
					break;
				case 'member' : 
					getMember(userId, function(result) {
						callback(result);
					});
					break;
				case 'agency' : 
					getAgency(userId, function(result) {
						callback(result);
					});
				break;
			}
		});
	}
	
	this.addUser = function(socketId, info)
	{
		// If already have in list only add socket in array
		// Else add new element in array with userId
		
		if ( ! usersPrivateList[info.userId] ) { 
			usersPrivateList[info.userId] = {
				sockets: [],
				info: info
			}
		}
		
		usersPublicList[info.userId] = info;
		addSocket(info.userId, socketId);
	}
	
	this.removeUser = function(userId) 
	{
		delete usersPrivateList[userId];
		delete usersPublicList[userId];
		delete usersWithoutSockets[userId];
	}
	
	this.removeSocket = function(userId, socketId)
	{
		if ( ! usersPrivateList[userId] ) return;
		sockets = usersPrivateList[userId].sockets;
		for(i = 0; i <= sockets.length; i++) {
			if ( sockets[i] == socketId ) {
				usersPrivateList[userId].sockets.splice(i, 1);
				break;
			}
		}
		
		//If user has no other sockets(f.e. close browser or all tabs) put him
		//in array which we will check in some period. If he will not 
		//come back for a while we will remove him from online users list.
		if ( usersPrivateList[userId].sockets.length == 0 ) {
			usersWithoutSockets[userId] = new Date();
		}
	}
	
	function addSocket(userId, socketId)
	{
		if ( ! usersPrivateList[userId] ) return;
		
		sockets = usersPrivateList[userId].sockets;
		for(var i = 0; i <= sockets.length; i++) {
			if ( sockets[i] == socketId ) return;
		}
		
		usersPrivateList[userId].sockets.push(socketId);
		
		//If user in usersWithoutSockets list remove him from there
		if ( typeof usersWithoutSockets[userId] !== "undefined") {
			delete usersWithoutSockets[userId];
		}
	}
	
	this.startOfflineUsersCheck = function(callback)
	{
		var self = this;
		setInterval(function() {
			removedUserIds = [];
			for(var userId in usersWithoutSockets) {
				if ( typeof usersPrivateList[userId] !== "undefined" && usersPrivateList[userId].sockets.length == 0 && (new Date() - usersWithoutSockets[userId]) >= 2 * 1000 ) {
					//If has no sockets more than 5 seconds remove from public and private online users list
					self.removeUser(userId);
					removedUserIds.push(userId);
					
				}
			}
			callback(removedUserIds);
		}, 10 * 1000);
	}
	
	this.getOnlineEscorts = function(callback)
	{
		var userIds = [];
		for(var i in usersPublicList) {
			if ( usersPublicList[i].userType == 'escort' ) {
				userIds.push(i);
			}
		}
		
		callback(userIds);
			
	}
	
	getEscort = function(userId, callback) 
	{
		client.query('SELECT e.id, e.showname, ep.hash, ep.ext FROM escorts e INNER JOIN escort_photos ep ON ep.escort_id = e.id AND is_main = 1 WHERE e.user_id = ?', [userId], function(error, result) {
			if ( ! result.length ) return false;
			escort = {
				info : {
					nickName : result[0].showname + ' (Escort)',
					userId : userId,
					userType : 'escort',
					avatar : getEscortImageUrl(result[0].id, result[0].hash, result[0].ext),
					status : 'offline',
					link: '/accompagnatrici/' + result[0].showname + '-' + result[0].id
				},
				sockets : []
			}
			
			if ( typeof callback == 'function' ) {
				callback(escort);
			}
		});
		
	}
	
	getMember = function(userId, callback)
	{
		client.query('SELECT username FROM users WHERE id = ?', [userId], function(error, result) {
			member = {
				info : {
					nickName: result[0].username,
					userId : userId,
					userType : 'member',
					status : 'offline',
					avatar : config.common.noPhotoUrl,
					link: '/member/' + result[0].username
				},
				sockets : []
			}
			
			if ( typeof callback == 'function' ) {
				callback(member);
			}
		});
	}
	
	getAgency = function(userId, callback)
	{
		client.query('SELECT username FROM users WHERE id = ?', [userId], function(error, result) {
			agency = {
				info : {
					nickName: result[0].username,
					userId : userId,
					userType : 'agency',
					status : 'offline',
					avatar : config.common.noPhotoUrl
				},
				sockets : []
			}
			
			if ( typeof callback == 'function' ) {
				callback(agency);
			}
		});
	}
	
	getEscortImageUrl = function(escortId, hash, ext)
	{
		parts = [];
		escortId = escortId.toString(); //converting to string
		if ( escortId.length > 2 ) {
			parts.push(escortId.substr(0, 2));
			parts.push(escortId.substr(2));
		} else {
			parts.push('_');
			parts.push(escortId);
		}
		catalog = parts.join('/');
		return config.common.imagesHost + '/' + catalog + '/' + hash + '_' + config.common.avatarSize +  '.' + ext;
	}
}

function cloneObject(obj)
{
	var c = new Object();
	for (var e in obj) {
		c[e] = obj[e];
	}
	return c;
}

function get() {
	return new UserManager();
}
exports.get = get;


