var config = require('../configs/config.js').get('production');
var request = require('request');


function Bridge() {
	var token = {};
	var _cache = {};
	
	this.getChatUsers = function(userIds, callback)
	{
		currentDate = new Date();
		/*if ( typeof _cache[userId] != 'undefined' && ( currentDate - _cache[userId]['date'] < config.common.sessionChacheTime ) ) {
			return callback(_cache[userId]['data']);
		}*/
		
		this.call('SimpleChatUsers', {userIds: userIds.join(',')}, function(result) {
			/*_cache[userId] = {
				date : new Date(),
				data : result
			};*/
			return callback(result);
		});
	}
	
	this.getUserById = function(userId, callback)
	{
		currentDate = new Date();
		/*if ( typeof _cache[userId] != 'undefined' && ( currentDate - _cache[userId]['date'] < config.common.sessionChacheTime ) ) {
			return callback(_cache[userId]['data']);
		}*/
		
		this.call('getchatuser', {userid: userId}, function(result) {
			result = result.shift();
			/*_cache[userId] = {
				date : new Date(),
				data : result
			};*/
			return callback(result);
		});
	}
	
	this.call = function(method, params, callback) {
		try {
			getToken( function(token) {
				var form = {
					'token' : token
				};

				//Setting params
				for(i in params) {
					form[i] = params[i];
				}

				var post_options = {
					url: 'http://' + config.api.host + config.api.path + '/' + method,
					method: 'GET',
					form: form,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					}
				};
				
				request(post_options, function(error, response, body) {
					if (! error && response.statusCode == 200) {
						resp = JSON.parse(body);
						callback(resp);
					} else {
						console.log('Error during getting token', error);
					}

				});

			});
		} catch(err) {
			console.log('Call func', err);
		}

		
	}
	
	getToken = function(callback)
	{
		try {
			if ( typeof token.token != 'undefined' && ( new Date() - token.date < config.api.tokenCacheTime ) ) {
				return callback(token.token);
			}
			
			console.log('Requesting for new token');
			
			var options = {
				//url: 'http://' + config.api.host + config.api.path + '/GetAccountTicket',
				url: 'http://' + config.api.host + config.api.path + '/mobile/NewAccountTicket',
				method: 'GET',
				form: {
					'accountid' : config.api.accountId,
					'secretcode' : config.api.secretCode
				},
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			};
			
			request(options, function(error, response, body) {
				if ( ! error && response.statusCode == 200 ) {
					resp = JSON.parse(body);
					token = {
						token : resp.Token,
						date : new Date()
					}
					callback(token.token);
					
				} else {
					console.log('Error during getting token', error);
				}
			});
		} catch(err) {
			console.log('getToken func', err);
		}
	}
	
	startclearCache = function()
	{
		setInterval(function() {
			_cache = {};
		}, 5 * 60 * 60 * 1000);//5 hours
	}
	
	//startclearCache();
}



exports.get = function()  {
	return new Bridge();
}
