var fs = require('fs');
var config = require('../configs/config.js').get('production');
function SettingsManager() {
	var settings = {};
	
	this.init = function()
	{
		fs.readFile(config.common.userSettingsBackupPath, function(err, data) {
			if ( err ) return;
			
			settings = JSON.parse(data);
		});
	}
	
	this.addOpenDialog = function(userId, dialogId)
	{
		if ( typeof settings[userId] == 'undefined'  ) {
			settings[userId] = {};
		}
		if ( typeof settings[userId]['openDialogs'] == 'undefined' ) {
			settings[userId]['openDialogs'] = [];
		}
		
		openDialogs = settings[userId]['openDialogs'];
		for(var i = 0; i <= openDialogs.length; i++) {
			if (openDialogs[i] == dialogId) {
				return;
			}
		}
		
		settings[userId]['openDialogs'].push(dialogId);
	}
	
	this.removeOpenDialog = function(userId, dialogId) 
	{
		if ( settings[userId] && settings[userId]['openDialogs'] ) {
			for(i in settings[userId]['openDialogs']) {
				if (settings[userId]['openDialogs'][i] == dialogId ) {
					settings[userId]['openDialogs'].splice(i, 1);
				}
			}
		}
	}
	
	this.getOpenDialogs = function(userId)
	{
		if ( typeof settings[userId] != 'undefined' && typeof settings[userId]['openDialogs'] != 'undefined' && settings[userId]['openDialogs'].length  ) {
			return settings[userId]['openDialogs']
		} else {
			return false;
		}
	}
	
	this.addPopupDialog = function(userId, dialogId)
	{
		sett = this.get(userId);
		if ( typeof sett['popupDialogs'] == 'undefined' ) {
			sett['popupDialogs'] = [];
		}
		
		popupDialogs = sett['popupDialogs'];
		for(var i = 0; i <= popupDialogs.length; i++) {
			if (popupDialogs[i] == dialogId) {
				return;
			}
		}
		sett['popupDialogs'].push(dialogId);
		
		this.set(userId, sett);
	}
	
	this.removePopupDialog = function(userId, dialogId) 
	{
		sett = this.get(userId);
		if ( sett['popupDialogs'] ) {
			for(i in sett['popupDialogs']) {
				if (sett['popupDialogs'][i] == dialogId ) {
					sett['popupDialogs'].splice(i, 1);
				}
			}
		}
		
		this.set(userId, sett);
	}
	
	this.getPopupDialogs = function(userId)
	{
		sett = this.get(userId);
		if ( typeof sett['popupDialogs'] != 'undefined' && sett['popupDialogs'].length  ) {
			return sett['popupDialogs']
		} else {
			return [];
		}
	}
	
	this.setActiveDialog = function(userId, dialogId)
	{
		sett = this.get(userId);
		sett['activeDialog'] = dialogId;
		this.set(userId, sett);
	}
	
	this.removeActiveDialog = function(userId)
	{
		sett = this.get(userId);
		sett['activeDialog'] = false;
		this.set(userId, sett);
	}
	
	this.getActiveDialog = function(userId)
	{
		sett = this.get(userId);
		if ( typeof sett['activeDialog'] != 'undefined' ) {
			return sett['activeDialog'];
		} else {
			return false;
		}
	}
	
	//availability[1, 0]
	this.setAvailability = function(userId, availability) {
		if ( availability != 0 ) {
			availability = 1;
		} else {
			availability = 0;
		}
		 
		if ( typeof settings[userId] == 'undefined' ) {
			settings[userId] = {};
		}
		settings[userId]['availability'] = availability;
	}
	
	this.getAvailability = function(userId) {
		if ( typeof settings[userId] == 'undefined' || typeof settings[userId]['availability'] == 'undefined' ) {
			return 1;
		} else {
			return settings[userId]['availability'];
		}
	}
	
	this.setPmAllow = function(userId, value) {
		sett = this.get(userId);
		sett['pmAllow'] = value;
		this.set(userId, sett);
	}
	
	this.getPmAllow = function(userId) {
		sett = this.get(userId);
		
		if ( typeof sett['pmAllow'] == 'undefined' ) {
			return [];
		} else {
			return sett['pmAllow'];
		}
	}
	
	
	this.setSound = function(userId, value) {
		if ( value != 0 ) {
			value = 1;
		}
		
		sett = this.get(userId);
		sett['sound'] = value;
		this.set(userId, sett);
	}
	
	this.getSound = function(userId)
	{
		sett = this.get(userId);
		
		if ( typeof sett['sound'] == 'undefined' ) {
			return 1;
		} else {
			return sett['sound'];
		}
	}
	
	this.setFriendsFilter = function(userId, value) {
		sett = this.get(userId);
		sett['friendsFilter'] = value;
		this.set(userId, sett);
	}
	
	this.getFriendsFilter = function(userId)
	{
		sett = this.get(userId);
		
		if ( typeof sett['friendsFilter'] == 'undefined' ) {
			return 'all';
		} else {
			return sett['friendsFilter'];
		}
	}
	
	this.setFavsFilter = function(userId, value) {
		sett = this.get(userId);
		sett['favsFilter'] = value;
		this.set(userId, sett);
	}
	
	this.getFavsFilter = function(userId)
	{
		sett = this.get(userId);
		
		if ( typeof sett['favsFilter'] == 'undefined' ) {
			return 'all';
		} else {
			return sett['favsFilter'];
		}
	}
	
	this.setFontSize = function(userId, value)
	{
		sett = this.get(userId);
		sett['fontSize'] = value;
		this.set(userId, sett);
	}
	
	this.getFontSize = function(userId)
	{
		sett = this.get(userId);
		if ( typeof sett['fontSize'] == 'undefined' ) {
			return 'small';
		} else {
			return sett['fontSize'];
		}
	}
	
	this.addConversation = function(userId, conversationId)
	{
		sett = this.get(userId);
		
		if ( typeof sett['conversations'] == 'undefined' ) {
			sett['conversations'] = [];
		}
		var alreadyInList = false;
		for( var i = 0; i < sett['conversations'].length; i++ ) {
			if ( sett['conversations'][i].userId == conversationId ) {
				alreadyInList = true;
			}
		}
		
		if ( ! alreadyInList ) {
			sett['conversations'].push({
				userId: conversationId,
				date: new Date()
			});
		}
		this.set(userId, sett);
	}
	
	this.getConversations = function(userId)
	{
		sett = this.get(userId);
		
		if ( typeof sett['conversations'] == 'undefined' ) {
			sett['conversations'] = [];
		}
		
		for( var i = 0; i < sett['conversations'].length; i++ ) {
			if ( new Date() - new Date(sett['conversations'][i].date) >= config.common.conversationsLimitPeriod ) {
				sett['conversations'].splice(i, 1);
			}
		}
		this.set(userId, sett);
		
		return sett['conversations'];
	}
	
	this.get = function(userId)
	{
		if ( typeof settings[userId] == 'undefined' ) {
			settings[userId] = {};
			return {};
		} else {
			return settings[userId];
		}
	}
	
	this.set = function(userId, sett)
	{
		if ( typeof settings[userId] == 'undefined' ) {
			settings[userId] = {};
		}
		settings[userId] = sett;
		
	}
	
	this.startSettingsBackup = function(callback)
	{
		
		setInterval(function() {
			fs.writeFile(config.common.userSettingsBackupPath, JSON.stringify(settings), function(err) {
				if ( err ) { 
					console.log(err); 
				}
				
				callback();
			});
		}, 20 * 60 * 1000);
	}
	
	this.init();
}

function get() {
	return new SettingsManager();
}

exports.get = get;