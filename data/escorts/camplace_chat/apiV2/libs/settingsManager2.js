var config = require('../configs/config.js').get('development');
var redis = require('redis');
var client;

function SettingsManager2() {
	var self = this;
	
	client = redis.createClient();
	client.auth(config.redis.password);
	client.select('user_settings');
	
	client.on("error", function (err) {
        console.log("Error " + err);
    });
	
	/*-->open dialogs logic*/
	this.addOpenDialog = function(userId, dialogId)
	{
		self.getUserSettings(userId, function(settings) {
			var openDialogs = settings['openDialogs']
			
			if ( typeof openDialogs == 'undefined' ) {
				settings['openDialogs'] = [dialogId]
			} else {
				for(var i = 0; i <= openDialogs.length; i++) {
					if ( openDialogs[i] == dialogId ) {
						return;
					}
				}
				settings['openDialogs'].push(dialogId);
			}
			settings['activeDialog'] = dialogId;
			
			self.setUserSettings(userId, settings, function() {
			});
		});
	}
	
	this.removeOpenDialog = function(userId, dialogId) 
	{
		self.getUserSettings(userId, function(settings) {
			if ( typeof settings['openDialogs'] != 'undefined' ) {
				for(i in settings['openDialogs']) {
					if (settings['openDialogs'][i] == dialogId ) {
						settings['openDialogs'].splice(i, 1);
					}
				}
				
				self.setUserSettings(userId, settings);
			}
		});
	}
	
	this.getOpenDialogs = function(userId, callback)
	{
		self.getUserSettings(userId, function(settings) {
			if ( typeof settings['openDialogs'] == 'undefined' ) {
				return callback(false);
			} else {
				return callback(settings['openDialogs']);
			}
		});
	}
	/*<--open dialogs logic*/
	
	/*-->active dialogs logic*/
	this.setActiveDialog = function(userId, dialogId)
	{
		self.getUserSettings(userId, function(settings) {
			settings['activeDialog'] = dialogId;
			self.setUserSettings(userId, settings);
			return;
		});
	}
	
	this.removeActiveDialog = function(userId)
	{
		self.getUserSettings(userId, function(settings) {
			settings['activeDialog'] = false
			self.setUserSettings(userId, settings);
			return;
		});
	}
	
	this.getActiveDialog = function(userId, callback)
	{
		self.getUserSettings(userId, function(settings) {
			if ( typeof settings['activeDialog'] == 'undefined' ) {
				return callback(false);
			} else {
				return callback(settings['activeDialog']);
			}
		});
	}
	/*<--active dialogs logic*/
	
//	/*-->Availability logic*/
//	this.setAvailability = function(userId, availability, callback) {
//		if ( availability != 0 ) {
//			availability = 1;
//		} else {
//			availability = 0;
//		}
//		 
//		getUserSettings(userId, function(settings) {
//			settings['availability'] = availability;
//			setUserSettings(userId, settings, callback);
//		});
//	}
//	
//	this.getAvailability = function(userId, callback) {
//		getUserSettings(userId, function(settings) {
//			if ( typeof settings['availability'] == 'undefined' ) {
//				callback(1)
//			} else {
//				callback(settings['availability']);
//			}
//		});
//	}
//	/*<--Availability logic*/
//	

	this.addPopupDialog = function(userId, dialogId)
	{
		self.getUserSettings(userId, function(settings) {
			if ( typeof settings['popupDialogs'] == 'undefined' ) {
				settings['popupDialogs'] = [];
			}
			
			var popupDialogs = settings['popupDialogs'];
			for(var i = 0; i <= popupDialogs.length; i++) {
				if (popupDialogs[i] == dialogId) {
					return;
				}
			}
			sett['popupDialogs'].push(dialogId);
			
			self.setUserSettings(userId, settings);
		});
	}
	
	this.removePopupDialog = function(userId, dialogId) 
	{
		self.getUserSettings(userId, function(settings) {
			if ( typeof settings['popupDialogs'] != 'undefined' ) {
				for(i in settings['popupDialogs']) {
					if (settings['popupDialogs'][i] == dialogId ) {
						settings['popupDialogs'].splice(i, 1);
					}
				}
				
				self.setUserSettings(userId, settings);
			}
		});
	}
	
	this.setSound = function(userId, value) {
		if ( value != 0 ) {
			value = 1;
		}
		
		self.getUserSettings(userId, function(settings) {
			settings['sound'] = value;
			self.setUserSettings(userId, settings);
		});
	}
	
	
	this.setFriendsFilter = function(userId, value) {
		self.getUserSettings(userId, function(settings) {
			settings['friendsFilter'] = value;
			self.setUserSettings(userId, settings);
		});
	}
	
	this.setFavsFilter = function(userId, value) {
		self.getUserSettings(userId, function(settings) {
			settings['favsFilter'] = value;
			self.setUserSettings(userId, settings);
		});
	}
	
	
	this.setFontSize = function(userId, value)
	{
		self.getUserSettings(userId, function(settings) {
			settings['fontSize'] = value;
			self.setUserSettings(userId, settings);
		});
	}
	
	
	this.addConversation = function(userId, conversationId)
	{
		self.getUserSettings(userId, function(settings) {
		
			if ( typeof settings['conversations'] == 'undefined' ) {
				settings['conversations'] = [];
			}
			var alreadyInList = false;
			for( var i = 0; i < settings['conversations'].length; i++ ) {
				if ( settings['conversations'][i].userId == conversationId ) {
					alreadyInList = true;
				}
			}

			if ( ! alreadyInList ) {
				settings['conversations'].push({
					userId: conversationId,
					date: new Date()
				});
			}
			
			
			self.setUserSettings(userId, settings);
		});
	}
	
	this.getConversations = function(userId, callback)
	{
		self.getUserSettings(userId, function(settings) {
			if ( typeof settings['conversations'] == 'undefined' ) {
				settings['conversations'] = [];
			}

			for( var i = 0; i < settings['conversations'].length; i++ ) {
				if ( new Date() - new Date(settings['conversations'][i].date) >= config.common.conversationsLimitPeriod ) {
					settings['conversations'].splice(i, 1);
				}
			}
			callback(settings['conversations']);
			self.setUserSettings(userId, settings);
		});
	}
	
	this.getUserSettings = function(userId, callback)
	{
		client.get(userId, function(err, res) {
			if ( err ) {
				console.log('Error: ' + err);
			} else {
				if ( ! res ) {
					callback({});
				} else {
					callback(JSON.parse(res));
				}
			}
		});
	}
	
	this.setUserSettings = function(userId, settings, callback)
	{
		client.set(userId, JSON.stringify(settings), function(err, res) {
			if ( err ) {
				console.log('Error: ' + err);
			} else {
				if ( typeof callback == 'function' ) {
					callback();
				}
			}
		});
	}
	
}

function get() {
	return new SettingsManager2();
}

exports.get = get;