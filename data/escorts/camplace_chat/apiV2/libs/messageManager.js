var mysql = require('mysql');
var config = require('../configs/config.js').get('production');

function MessageManager() {
	var self = this;
	
	var client = mysql.createConnection({
		user:     config.db.user,
		database: config.db.database,
		password: config.db.password,
		host:     config.db.host,
		port: 3306
	});
	
	var keywords = [];
	setInterval(function() {
		self.getMaskableMessages();
	}, config.common.keywordsCacheTime);
	
	/*-->Reconnecting to mysql if connection lost*/
	//It happens sometimes. about once in 8 hours
	function handleDisconnect() {
		client.on('error', function(err) {
			if (! err.fatal ) {
				console.log(err);
				return;
			}
			
			if ( err.code !== 'PROTOCOL_CONNECTION_LOST' ) {
				console.log("The mysql library fired a PROTOCOL_CONNECTION_LOST exception");
				throw err;
			}
			
			console.log('Re-connecting lost connection: ' + err.stack);

			var client = mysql.createConnection({
				user:     config.db.user,
				database: config.db.database,
				password: config.db.password,
				host:     config.db.host,
				port: 3306
			});
			handleDisconnect(client);
			client.connect();
		});
	}

	handleDisconnect();
	/*<--Reconnecting to mysql if connection lost*/
	
	this.storeMessage = function(userIdFrom, userIdTo, message) 
	{
		getThreadId(userIdFrom, userIdTo, function(threadId) {
			insertMessage(userIdFrom, threadId, message);
		});
	}
	
	this.getConversation = function(userId, participantId, dateFrom, callback)
	{
		var bind = [], sql;
		getThreadId(userId, participantId, function(threadId) {
			if ( ! dateFrom ) {
				where = ' WHERE thread_id = ? ';
				bind.push(threadId);
			} else {
				where = ' WHERE thread_id = ? AND (date >= ? OR read_date IS NULL )';
				bind.push(threadId, dateFrom.toMysqlFormat())
			}
			
			sql = 'SELECT user_id AS userId, body, UNIX_TIMESTAMP(date) * 1000 AS date FROM chat_messages ' + where + ' ORDER BY id ASC';//getting timestamp in milliseconds
			
			client.query(sql, bind, function(error, messages) {
				callback(messages);
			});
		});
	}
	
	this.getConversationV2 = function(userId, participantId, lastMessageId, count, callback)
	{
		if ( ! count ) count = 10;
		
		var bind = [], sql;
		getThreadId(userId, participantId, function(threadId) {
			where = ' WHERE thread_id = ?';
			bind.push(threadId);
			
			if ( lastMessageId ) {
				where += ' AND id < ?';
				bind.push(lastMessageId);
			}
			
			sql = 'SELECT id, user_id AS userId, body, UNIX_TIMESTAMP(date) * 1000 AS date FROM chat_messages ' + where + ' ORDER BY id DESC LIMIT ' + count;//getting timestamp in milliseconds
			client.query(sql, bind, function(error, messages) {
				callback(messages);
			});
		});
	}
	
	this.getNewMessagesCount = function(userId, callback) 
	{
		sql = 'SELECT count(cm.id) count, cm.user_id AS userId FROM chat_threads_participants ctp INNER JOIN chat_messages cm ON cm.thread_id = ctp.thread_id  WHERE ctp.user_id = ? AND cm.user_id <> ? AND read_date IS NULL GROUP BY cm.user_id';
		client.query(sql, [userId, userId], function(error, result) {
			callback(result);
		});
	}
	
	this.markAsRead = function(userId, participantId, callback)  
	{
		getThreadId(userId, participantId, function(threadId) {
			date = new Date();
			client.query('UPDATE chat_messages SET read_date = ? WHERE thread_id = ? AND user_id = ?', [date.toMysqlFormat(), threadId, participantId], function(error, result) {
				if (typeof callback == 'function') callback();
			});
		});
	}
	
	this.clearMessage = function(message)
	{
		//htmlentities
		message = htmlEntities(message);
		
		//Replacing one more whitespaces
		message = message.trim();
		message = message.replace(/\s+/g, ' ');
		
		//replacing urls with html links
		/*var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
		zmessage = message.replace(exp,"<a href='$1' target='_blank'>$1</a>"); */
		
		var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
		message = message.replace(exp, "");
		
		if ( message && message.length ) {
			var found = false;
			var matches;
			for(i in keywords) {
				matches = [];
				if ( keywords[i].type == 'exact' ) {
					matches = message.match(new RegExp('^' + keywords[i].keyword + '$'));
					if ( matches && matches.length ) found = true;
				} else {
					if ( message.indexOf(keywords[i].keyword) != -1 ) found = true;
				}
				
				if ( found ) {
					message = '';
					break;
				}
			}
		}
		
		return message;
	}
	
	this.getMaskableMessages = function()
	{
		client.query(
			'SELECT * FROM keywords WHERE is_maskable = 1',	[],
			function(error, result) {
				if ( typeof result != 'undefined' || result.length != 0 ) {
					keywords = result;
				}
			}
		);
	}
	
	this.getWithLastMessage = function(userId, participantUserIds, callback)
	{
		client.query(
			'SELECT ctp2.user_id AS userId, ctp1.thread_id AS threadId, ct.last_message AS lastMessage, UNIX_TIMESTAMP(ct.last_message_date) * 1000 AS lastMessageDate \n\
			FROM chat_threads_participants ctp1 \n\
			INNER JOIN chat_threads_participants ctp2 ON ctp1.thread_id = ctp2.thread_id \n\
			INNER JOIN chat_threads ct ON ct.id = ctp1.thread_id \n\
			WHERE ctp1.user_id = ? AND ctp2.user_id IN(' + participantUserIds.join(',') + ') AND ctp1.user_id <> ctp2.user_id \n\
			GROUP BY ctp2.user_id \n\
			ORDER BY ct.last_message_date DESC',
			[userId],
			function(error, result) {
				callback(result);
			}
		);
	}
	
	function getThreadId(userIdFrom, userIdTo, callback)
	{
		client.query(
			'SELECT tp1.thread_id AS threadId FROM chat_threads_participants tp1 INNER JOIN chat_threads_participants tp2 ON tp2.thread_id = tp1.thread_id AND tp2.user_id = ? WHERE tp1.user_id = ?',
			[userIdFrom, userIdTo],
			function(error, result) {
				if ( typeof result == 'undefined' || result.length == 0 ) {// If doesn't exist with this userIds pair insert
					client.query('INSERT INTO chat_threads VALUES()', function(error, result) {
						createThreadParticipants(result.insertId, userIdFrom, userIdTo, callback);
					});
				} else {
					callback(result[0].threadId);
				}
			}
		);
	}
	
	function createThreadParticipants(threadId, userIdFrom, userIdTo, callback)
	{
		client.query('INSERT INTO chat_threads_participants (user_id, thread_id) VALUES(?,?)', [userIdFrom, threadId], function(error, result) {
			client.query('INSERT INTO chat_threads_participants (user_id, thread_id) VALUES(?,?)', [userIdTo, threadId]);
		});
	}
	
	function insertMessage(userId, threadId, message)
	{
		date = new Date();
		client.query(
			'INSERT INTO chat_messages (user_id, thread_id, body, date) VALUES(?, ?, ?, ?)',
			[userId, threadId, message, date.toMysqlFormat()], function(error, result) {
				if ( error ) {
					console.log(error);
				}
			}
		);

		client.query(
			'UPDATE chat_threads SET last_message = ?, last_message_date = ? WHERE id = ?',
			[message, date.toMysqlFormat(), threadId], function(error, result) {
				if ( error ) {
					console.log(error);
				}
			}
		);
	}
	
	function twoDigits(d) {
		if(0 <= d && d < 10) return "0" + d.toString();
		if(-10 < d && d < 0) return "-0" + (-1*d).toString();
		return d.toString();
	}
	
	function htmlEntities(str) {
		return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
	}
	
	Date.prototype.toMysqlFormat = function() {
		return this.getFullYear() + "-" + twoDigits(1 + this.getMonth()) + "-" + twoDigits(this.getDate()) + " " + twoDigits(this.getHours()) + ":" + twoDigits(this.getMinutes()) + ":" + twoDigits(this.getSeconds());
	};
}

function get() {
	return new MessageManager();
}
exports.get = get;