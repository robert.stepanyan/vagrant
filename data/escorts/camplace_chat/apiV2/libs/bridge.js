var config = require('../configs/config.js').get('production');
var http = require('http');
var querystring = require('querystring');



function Bridge() {
	var token = {};
	var _cache = {};
	
	this.getChatUsers = function(userIds, callback)
	{
		currentDate = new Date();
		/*if ( typeof _cache[userId] != 'undefined' && ( currentDate - _cache[userId]['date'] < config.common.sessionChacheTime ) ) {
			return callback(_cache[userId]['data']);
		}*/
		this.call('SimpleChatUsers', {userIds: userIds.join(',')}, function(result) {
			/*_cache[userId] = {
				date : new Date(),
				data : result
			};*/
			return callback(result);
		});
	}
	
	this.getUserById = function(userId, callback)
	{
		currentDate = new Date();
		/*if ( typeof _cache[userId] != 'undefined' && ( currentDate - _cache[userId]['date'] < config.common.sessionChacheTime ) ) {
			return callback(_cache[userId]['data']);
		}*/
		
		this.call('getchatuser', {userid: userId}, function(result) {
			result = result.shift();
			/*_cache[userId] = {
				date : new Date(),
				data : result
			};*/
			return callback(result);
		});
	}
	
	this.call = function(method, params, callback) {
		var self = this;
		try {
			getToken( function(token) {
				var post_data = {
					'output_format': 'json',
					'token' : token
				};

				//Setting params
				for(i in params) {
					post_data[i] = params[i];
				}

				post_data = querystring.stringify(post_data);
				
				var post_options = {
					host: config.api.host,
					port: '80',
					path: config.api.path + '/' + method,
					method: 'POST',
					agent: false,
					keepAlive: false,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'Content-Length': post_data.length
					}
				};
				
				var post_req = http.request(post_options, function(res) {
					res.setEncoding('utf8');
					
					var resp = '';
					res.on('data', function (chunk) {
						resp += chunk;
					}).on('end', function() {
						clearTimeout(foo);
						try {
							resp = JSON.parse(resp);
							callback(resp);
						} catch(err) {
							console.log(err);
						}
					});
				}).on("error", function(e) {
					console.log(e);
					if ( e.message.indexOf('EAADRINFO') != -1 ) {
						console.log("Error catched:  " + e.message);
						console.log('Can\'t resolve ip, stoping script. Cron will turn it back.')
						process.exit();
					}
				}).on('timeout-foo', function(details) {
					//console.log(config.api.host + config.api.path + '/' + details.method);
				});
				
				
				var foo = setTimeout(function() {
					post_req.emit("timeout-foo", {method: method, params: params});
				}, 10000);

				post_req.write(post_data);
				post_req.end();
			});
		} catch(err) {
			console.log('Call func', err);
			console.log(err);
		}

		
	}
	
	getToken = function(callback)
	{
		return callback('aaaaaaaaa');
		try {
			if ( typeof token.token != 'undefined' && ( new Date() - token.date < config.api.tokenCacheTime ) ) {
				
				return callback(token.token);
			}
			
			//console.log('Requesting for new token');
			
			
			var post_data = querystring.stringify({
				'compilation_level' : 'ADVANCED_OPTIMIZATIONS',
				'output_format': 'json',
				'output_info': 'compiled_code',
				'warning_level' : 'QUIET',
				'accountid' : config.api.accountId,
				'secretcode' : config.api.secretCode
			});

			var post_options = {
				host: config.api.host,
				port: '80',
				//path: config.api.path + '/GetAccountTicket',
				path: config.api.path + '/mobile/NewAccountTicket',
				method: 'POST',
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
					'Content-Length': post_data.length
				}
			};
			var post_req = http.request(post_options, function(res) {
				res.setEncoding('utf8');

				res.on('data', function (resp) {
					try {
						resp = JSON.parse(resp);

						token = {
							token : resp.Token,
							date : new Date()
						}
						callback(token.token);
					} catch(err) {
						console.log('Invalid response', resp);
					}
				}).on("error", function(e) {
					if ( e.message.indexOf('EAADRINFO') != -1 ) {
						console.log("Error catched:  " + e.message);
						console.log('Can\'t resolve ip, stoping script. Cron will turn it back.')
						process.exit();
					}
				});
			});
			post_req.write(post_data);
			post_req.end();
		} catch(err) {
			console.log('getToken func', err);
		}
	}
	
	startclearCache = function()
	{
		setInterval(function() {
			_cache = {};
		}, 5 * 60 * 60 * 1000);//5 hours
	}
	
	//startclearCache();
}



exports.get = function()  {
	return new Bridge();
}
