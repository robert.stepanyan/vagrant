var config = require('../configs/config.js').get('production'),
	messageManager = require("./messageManager.js").get(),
	settingsManager = require("./settingsManager2.js").get(),
	userManager = require("./userManager.js").get();
	dictionaryManager = require("./dictionaryManager.js").get(),
	//require('longjohn'),
	
	app = require('http').createServer(handler).listen(config.common.listenPort),
	io = require('socket.io').listen(app, {log: false, origins: '*:*'}),
	
	url = require('url'),
	crypto = require('crypto');
	
	function handler(request, response) {
		var queryData = url.parse(request.url, true);

		var query = queryData.query;
		
		
		
		switch ( queryData.pathname ) {
			case '/node-chat/signout' :
				//Changing availability
				var uid = query.userId,
				hash = query.hash;

				if ( ! uid || ! hash || crypto.createHash('md5').update(uid + '-' + config.common.key).digest("hex") != hash)  {
					response.writeHead(200, {"Content-Type": "text/plain"});
					response.end("sta=error");
					return;
				} 

				userManager.getUser(uid, false, function(user) {
					if ( user ) {
						if ( user.status != 'invisible' && user.status != 'offline' ) {
							emitToGroup(false, 'user-status-changed', {userId : uid, status : 'offline'});
						}
						
						var sSockets = user.sockets.slice(0);//Copying array
						for ( i = 0; i < sSockets.length; i++ ) {
							io.sockets.socket(sSockets[i]).emit('logout');
							io.sockets.socket(sSockets[i]).disconnect('logout');
						}
						userManager.removeUser(uid);
					}


					response.writeHead(200, {"Content-Type": "text/plain"});
					response.end("sta=ok");
				});
				break;
			case '/node-chat/profile-changed' :
				//Clearing cache
				var uid = query.userId,
				hash = query.hash;
				
				if ( ! uid || ! hash || crypto.createHash('md5').update(uid + '-' + config.common.key).digest("hex") != hash)  {
					response.writeHead(200, {"Content-Type": "text/plain"});
					response.end("sta=error");
					return;
				} 
				userManager.getUser(uid, false, function(res) {
					var oldUserInfo = res.info;
					var sockets = res.sockets;
					
					if ( typeof sockets == 'undefined' ) sockets = [];
					
					userManager.getUsersV2([uid], function(res) {
						
						var user = res[uid];
						userInfo = user.info;
						userInfo.pendingCamRequests = user.pendingCamRequests;
						
						
						if ( sockets && sockets.length ) {
							for ( var i = 0; i < sockets.length; i++ ) {
								io.sockets.socket(sockets[i]).userInfo = userInfo;
							}
						}
						userManager.updateUserInfo(userInfo);
						
						if ( user.activeStatus == "Banned" ) {
							if ( sockets && sockets.length ) {
								while(sockets.length > 0) {
									io.sockets.socket(sockets[0]).emit('logout');
									io.sockets.socket(sockets[0]).disconnect('banned');
									sockets = sockets.slice(1)	
								}
							}
						}
						
						if ( oldUserInfo ) {
							if ( ! oldUserInfo.isBroadcasting && userInfo.isBroadcasting ) {
								emitToGroup(false, 'cam-status-changed', {userId : uid, camStatus : 1, userType: userInfo.userType});
							} else if ( oldUserInfo.isBroadcasting && ! userInfo.isBroadcasting ) {
								emitToGroup(false, 'cam-status-changed', {userId : uid, camStatus : 0, userType: userInfo.userType});
							}
						}
						
						

					});
				})
				
				response.writeHead(200, {"Content-Type": "text/plain"});
				response.end("sta=ok");
				break;
			case '/node-chat/localization' :
				var lang = query.lang;
				
				if ( ! lang || lang.length == 0 ) {
					response.writeHead(200, {"Content-Type": "text/plain"});
					response.end("sta=errorrr");
					return;
				}
				
				dictionaryManager.getDictionary(lang.toLowerCase(), function(dic) {
					response.writeHead(200, {"Content-Type": "application/javascript"});
					var output = '$(document).ready(function() { chati18n = {';
					var pairs = [];
					for( i in dic ) {
						if ( typeof dic[i] ) {
							pairs.push(i + ': "' + dic[i].replace(/"/g, '\'') + '"' + '\n');
						}
					}
					output += pairs.join(',') + '}; })';
					
					response.end(output);
					return;
				});
				break;
			case '/node-chat/cam-status-changed' :
				//Clearing cache
				var uid = query.userId,
					camStatus = query.camStatus,
					hash = query.hash;
					

				if ( ! uid || ! camStatus /*|| ! hash || crypto.createHash('md5').update(uid + '-' + camStatus + '-' + config.common.key).digest("hex") != hash*/)  {
					response.writeHead(200, {"Content-Type": "text/plain"});
					response.end("sta=error");
					return;
				} 
				
				userManager.getOfflineUserById(uid, function(resp) {
					emitToGroup(false, 'cam-status-changed', {userId : uid, camStatus : camStatus, userType: resp.info.userType});
				});
				
				response.writeHead(200, {"Content-Type": "text/plain"});
				response.end("sta=ok");
				break;
			default :
				response.writeHead(200, {"Content-Type": "text/plain"});
				response.end("sta=error");
				break;
		}
	}
	
	/*settingsManager.startSettingsBackup(function() {
		console.log('User settings backup done');
	});*/

	userManager.startOfflineUsersCheck(function(removedUsers) {
		for( var i = 0; i < removedUsers.length; i++ ) {
			if ( removedUsers[i].status != 'invisible' && removedUsers[i].status != 'offline' ) {
				emitToGroup(false, 'user-status-changed', {userId : removedUsers[i].userId, status : 'offline'});
			}
			userManager.notifyOffline(removedUsers[i].userId);
		}
	});
	
	//Sending online users id to their system
	userManager.startSendingOnlineList();
	
	
	setInterval(function() {
		removeDeadSockets();
	}, 20*1000);
		
function start() {
	io.sockets.on('connection', function (socket) {
		socket.on('auth', function(data) {
			if ( crypto.createHash('md5').update(data.sid + config.common.camplaceSecret).digest("hex") != data.uHash) {
				socket.emit('auth-complete', {auth: false});
				socket.emit('logout');
				socket.disconnect('unauthorized');
				return;
			}
			
			userManager.getOfflineUserById(data.sid, function(resp) {
				if ( ! resp || resp.activeStatus == "Banned" ) {
					
					socket.emit('auth-complete', {auth: false});
					socket.emit('logout');
					socket.disconnect('unauthorized');
					return;
				}
				var userInfo = resp.info;
				var newUser = true;
				
				if ( userManager.getUser(userInfo.userId) ) {
					newUser = false;
				}
				
				userInfo.status = resp.status; //Setting actual status
				userInfo.pendingFriendsCount = resp.pendingFriendsCount;
				userInfo.siteId = data.siteId;
				userInfo.domain = data.domain;
				userInfo.isFirstSession = newUser;
				userInfo.pendingCamRequests = resp.pendingCamRequests;
				userInfo.pendingCamRequestsCount = resp.pendingCamRequestsCount;

				settingsManager.getUserSettings(userInfo.userId, function(settings) {
					
					userInfo.settings = settings;
					socket.emit('auth-complete', {auth: true, /*availability: availability,*/ userInfo: userInfo});
					
					socket.userInfo = userInfo;
					userManager.addUser(socket.id, userInfo);

					if ( userInfo.status == 'offline' ) {
						return;
					}

					if ( newUser && userInfo.status != 'invisible' ) {
						emitToGroup(false, 'user-status-changed', {userId : userInfo.userId, status : userInfo.status});
					}
					
					sendNotifications(socket);
				});
			});
		});
		
		socket.on('message-sent', function(messageData) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			
			userManager.getUser(messageData.userId, true, function(user) {
				if ( user ) {
					var pmAllow = checkPmAllow(socket.userInfo, user.info);
					
					if ( ! pmAllow.allow ) {
						if (  pmAllow.reason == 'basic-restriction' ) {
							socket.emit('pm-allow-basic-restriction', {dialogId : messageData.userId});
						} else {
							socket.emit('pm-allow', {dialogId : messageData.userId});
						}
					} else {
						var message = messageManager.clearMessage(messageData.message);
						if ( message.length > 0 ) {
							
							checkRestrictions(socket.userInfo, user.info, settingsManager, function(restrictions) {
							
								if ( ! restrictions.allow ) {
									socket.emit(restrictions.reason, {dialogId : messageData.userId});
									return;
								}


								messageManager.storeMessage(socket.userInfo.userId, user.info.userId, message);
								sockets = user.sockets;

								newMessageData = {
									body : message,
									userId : socket.userInfo.userId,
									date : new Date().getTime()
								}

								var userInfo = socket.userInfo;

								/*-->Sending user's own message to his other sockets*/
								var selfSockets = userManager.getUser(socket.userInfo.userId).sockets;
								if ( selfSockets ) {
									for ( var i = 0; i < selfSockets.length; i++ ) {
										if ( selfSockets[i] != socket.id ) {
											io.sockets.socket(selfSockets[i]).emit('new-message-self', {message: newMessageData, userId: user.info.userId});
										}
									}
								}
								/*<--Sending user's own message to his other sockets*/

								//Do not send event if offline
								if ( user.status == 'offline' ) return;

								if ( userInfo.status == 'invisible' ) {
									userInfo.status = 'offline';
								}
								
								if ( socket.userInfo.userId == 12664 ) {
									console.log(sockets);
								}
								for(var i = 0; i < sockets.length; i++) {
									io.sockets.socket(sockets[i]).emit('new-message', {message: newMessageData, senderData: userInfo});
								}
							});
						}
					}
				}
			});
		});
		
		socket.on('dialog-opened', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			//showing todays history
			date = new Date();
			date.setHours(0);
			date.setMinutes(0);
			date.setSeconds(0);
			
			messageManager.getConversation(socket.userInfo.userId, data.userId, date, function(messages) {
				if ( messages.length ) {
					userManager.getUser(data.userId, true, function(user) {
						socket.emit('message-history', {messages: messages, senderInfo : user.info});
					});
				}
				
			});
			settingsManager.addOpenDialog(socket.userInfo.userId, data.userId);
		});
		
		socket.on('message-history', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			//showing todays history
			date = new Date();
			date.setHours(0);
			date.setMinutes(0);
			date.setSeconds(0);
			
			
			messageManager.getConversation(socket.userInfo.userId, data.userId, date, function(messages) {
				if ( messages.length ) {
					userManager.getUser(data.userId, true, function(user) {
						socket.emit('message-history', {messages: messages, senderInfo : user.info});
					});
				}
			});
			
		});
		
		
		socket.on('message-history-chunked', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			
			
			messageManager.getConversationV2(socket.userInfo.userId, data.userId, data.lastMessageId, data.count, function(messages) {
				if ( messages.length ) {
					userManager.getUser(data.userId, true, function(user) {
						socket.emit('message-history-chunked', {messages: messages, senderInfo : user.info});
					});
				}
			});
			
		});
		
		socket.on('dialog-closed', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			settingsManager.removeOpenDialog(socket.userInfo.userId, data.userId);
		});
		
		socket.on('dialog-activated', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			settingsManager.setActiveDialog(socket.userInfo.userId, data.userId);
		});
		
		socket.on('dialog-deactivated', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			settingsManager.removeActiveDialog(socket.userInfo.userId);
		});
		
		socket.on('availability-changed', function(availability) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			
			var status,
				notifyUsers = true,
				user = userManager.getUser(socket.userInfo.userId);
			
			if ( user.status == 'invisible' ) {
				notifyUsers = false;
			}
			
			if ( availability ) {
				status = 'online';
				
				userManager.setStatus(socket.userInfo.userId, status);
				
				if ( user && user.sockets ) {
					for(var i = 0; i < user.sockets.length; i++) {
						io.sockets.socket(user.sockets[i]).emit('status-changed', {status: 'online'});

						sendNotifications(io.sockets.socket(user.sockets[i]));
					}
				}
				
				
			} else {
				status = 'offline';
				
				userManager.setStatus(socket.userInfo.userId, 'offline');
				if ( user && user.sockets ) {
					for(var i = 0; i <= user.sockets.length - 1; i++) {
						io.sockets.socket(user.sockets[i]).emit('status-changed', {status: 'offline'});
					}
				}
			}
			
			if ( notifyUsers ) {
				emitToGroup(false, 'user-status-changed', {userId : socket.userInfo.userId, status : status});
			}
		});
		
		socket.on('status-changed', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			//Emitting to user's all sockets about status change
			user = userManager.getUser(socket.userInfo.userId);
			if ( user && data.status.length ) {
				userManager.setStatus(socket.userInfo.userId, data.status);
				for(var i = 0; i < user.sockets.length; i++) {
					io.sockets.socket(user.sockets[i]).emit('status-changed', {status: data.status});
				}
				
				status = data.status;
				if ( status == 'invisible' ) {
					status = 'offline';
				}
				emitToGroup(false, 'user-status-changed', {userId : socket.userInfo.userId, status : status});
			}
		});
		
		socket.on('conversation-read', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			messageManager.markAsRead(socket.userInfo.userId, data.userId);
			
			user = userManager.getUser(socket.userInfo.userId);
			
			if ( ! user.sockets ) return;
			
			for(var i = 0; i < user.sockets.length; i++) {
				io.sockets.socket(user.sockets[i]).emit('conversation-read', {userId: data.userId});
			}
		});
		
		socket.on('typing-start', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			
			user = userManager.getUser(data.userId);
			if ( user ) {
				for(var i = 0; i < user.sockets.length; i++) {
					io.sockets.socket(user.sockets[i]).emit('typing-start', {userId : socket.userInfo.userId});
				}
			}
		});
		
		socket.on('typing-end', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			
			user = userManager.getUser(data.userId);
			if ( user ) {
				for(var i = 0; i < user.sockets.length; i++) {
					io.sockets.socket(user.sockets[i]).emit('typing-end', {userId : socket.userInfo.userId});
				}
			}
		});
		
		socket.on('chat-with', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			userManager.getUser(data.userId, true, function(user) {
				socket.emit('open-dialogs', {dialogs : [{userId: data.userId, userInfo : user.info}], activeDialog : data.userId});
			});
		});
		
		socket.on('chat-popup-opened', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			settingsManager.addPopupDialog(socket.userInfo.userId, data.userId)
		});
		
		socket.on('chat-popup-closed', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			settingsManager.removePopupDialog(socket.userInfo.userId, data.userId)
		});
		
		socket.on('tip-model', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			
			userManager.checkTransaction(socket.userInfo.userId, data.userId, data.amount, parseInt(data.transactionId), function(res) {
				if ( res.Success !== true ) return;
				
				user = userManager.getUser(data.userId);
				if ( user ) {
					for(var i = 0; i < user.sockets.length; i++) {
						io.sockets.socket(user.sockets[i]).emit('tip-model', {userInfo : socket.userInfo, amount : data.amount});
					}
				}
				
			});
			
			
		});
		
		socket.on('ping', function(data) {
			/*if ( typeof socket.userInfo == 'undefined' ) {
				socket.emit('reconnect');
			}*/
		});
		
		socket.on('disconnect', function() {
			if ( typeof socket.userInfo == 'undefined' ) return;
			userManager.removeSocket(socket.userInfo.userId, socket.id);
		});
		
		socket.on('add-favourite', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			userManager.addFavourite(socket.userInfo.userId, data.userId, function(res) {
				if ( res.Success ) {
					userManager.getUser(data.userId, true, function(favUser) {
						user = userManager.getUser(socket.userInfo.userId);
						if ( user ) {
							for(var i = 0; i < user.sockets.length; i++) {
								io.sockets.socket(user.sockets[i]).emit('favourite-added', {favouriteUserId: data.userId, favUser: favUser.info});
							}
						}
					});
				}
			});
		});
		
		socket.on('remove-favourite', function(data) {
			if ( ! socket.userInfo ) return;
			userManager.removeFavourite(socket.userInfo.userId, data.userId, function(res) {
				if ( res.Success ) {
					userManager.getUser(data.userId, true, function(favUser) {
						user = userManager.getUser(socket.userInfo.userId);
						if ( user ) {
							for(var i = 0; i < user.sockets.length; i++) {
								io.sockets.socket(user.sockets[i]).emit('favourite-removed', {favouriteUserId: data.userId, favUser: favUser.info});
							}
						}
					});
				}
			});
		});
		
		
		socket.on('add-friend', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			userManager.addFriend(socket.userInfo.userId, data.userId, function(res) {
				if ( res.Success ) {
					userManager.getUser(data.userId, true, function(friendUser) {
						user = userManager.getUser(socket.userInfo.userId);
						if ( user ) {
							for(var i = 0; i < user.sockets.length; i++) {
								io.sockets.socket(user.sockets[i]).emit('friend-added', {friendUserId: data.userId, friendUser: friendUser.info});
							}
						}
					});
				}
			});
		});
		
		
		socket.on('remove-friend', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			userManager.removeFriend(socket.userInfo.userId, data.userId, function(res) {
				if ( res.Success ) {
					userManager.getUser(data.userId, true, function(friendUser) {
						user = userManager.getUser(socket.userInfo.userId);
						if ( user ) {
							for(var i = 0; i < user.sockets.length; i++) {
								io.sockets.socket(user.sockets[i]).emit('friend-removed', {friendUserId: data.userId, friendUser: friendUser.info});
							}
						}
					});
				}
			});
		});
		
		
		socket.on('add-to-ignore-list', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			
			user = userManager.getUser(socket.userInfo.userId);
			if ( user ) {
				for(var i = 0; i < user.sockets.length; i++) {
					io.sockets.socket(user.sockets[i]).emit('added-to-ignore-list', {userId: data.userId});
				}
			}
			
			/*userManager.addToIgnoreList(socket.userInfo.userId, data.userId, function(res) {
				
				
				
				
			});*/
		});
		
		
		socket.on('remove-from-ignore-list', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			
			
			user = userManager.getUser(socket.userInfo.userId);
			if ( user ) {
				for(var i = 0; i < user.sockets.length; i++) {
					io.sockets.socket(user.sockets[i]).emit('removed-from-ignore-list', {userId: data.userId});
				}
			}
			
			/*userManager.removeFromIgnoreList(socket.userInfo.userId, data.userId, function(res) {
				
			});*/
		});
		
		socket.on('friend-request-confirm', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			userManager.confirmFriendRequest(socket.userInfo.userId, data.userId, function(res) {
				if ( res.Success ) {
					user = userManager.getUser(socket.userInfo.userId);
					if ( user ) {
						for(var i = 0; i < user.sockets.length; i++) {
							io.sockets.socket(user.sockets[i]).emit('friend-request-confirmed', {userId: data.userId});
						}
					}
				}
			});
		});
		
		socket.on('friend-request-decline', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			userManager.declineFriendRequest(socket.userInfo.userId, data.userId, function(res) {
				if ( res.Success ) {
					user = userManager.getUser(socket.userInfo.userId);
					if ( user ) {
						for(var i = 0; i < user.sockets.length; i++) {
							io.sockets.socket(user.sockets[i]).emit('friend-request-declined', {userId: data.userId});
						}
					}
				}
			});
		});
		
		socket.on('cam-request-action', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			
			userManager.camRequestAction(socket.userInfo.userId, data.userId, data.action, function(res) {
				if ( res.Success ) {
					user = userManager.getUser(socket.userInfo.userId);
					if ( user ) {
						for(var i = 0; i < user.sockets.length; i++) {
							io.sockets.socket(user.sockets[i]).emit('cam-request-removed', {userId: data.userId});
						}
					}
				}
			});
		});
		
		socket.on('change-settings-sound', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			settingsManager.setSound(socket.userInfo.userId, data.value);
			
			/*-->Emit to all sockets about settings change*/
			var selfSockets = userManager.getUser(socket.userInfo.userId).sockets;
			for ( var i = 0; i < selfSockets.length; i++ ) {
				if ( selfSockets[i] != socket.id ) {
					io.sockets.socket(selfSockets[i]).emit('settings-sound-changed', {value: data.value});
				}
			}
			/*<--Emit to all sockets about settings change*/
			
		});
		
		socket.on('change-settings-friends-filter', function(data) {
			if ( ! socket.userInfo ) return;
			settingsManager.setFriendsFilter(socket.userInfo.userId, data.value);
		});
		socket.on('change-settings-favs-filter', function(data) {
			if ( ! socket.userInfo ) return;
			settingsManager.setFavsFilter(socket.userInfo.userId, data.value);
		});
		
		socket.on('change-settings-font-size', function(data) {
			if ( ! socket.userInfo ) return;
			settingsManager.setFontSize(socket.userInfo.userId, data.value);
		});
		
		socket.on('change-settings-pm-allow', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			values = ['all', 'onlygirls', 'onlyfriends', 'favorites', 'withtokens', 'withtokensplusfriend']
			var pmAllow = data.pmAllow;
			
			for(var i = 0; i < pmAllow.length; i++) {
				if ( values.indexOf(pmAllow[i]) == -1 ) {
					delete pmAllow[i];
				}
			}
			userManager.changePmAllow(socket.userInfo.userId, pmAllow, function(res) {
				if ( res.Success ) {
					/*-->Emit to all sockets about settings change*/
					var selfSockets = userManager.getUser(socket.userInfo.userId).sockets;
					for ( var i = 0; i < selfSockets.length; i++ ) {
						if ( selfSockets[i] != socket.id ) {
							io.sockets.socket(selfSockets[i]).emit('settings-pm-allow-changed', {pmAllow: pmAllow});
						}
					}
					/*<--Emit to all sockets about settings change*/
					
					
					//Emiting to all others about settings change
					emitToGroup(false, 'user-pm-allow-changed', {userId : socket.userInfo.userId, pmAllow : pmAllow});
				}
			});
			
			
		});
		
		socket.on('open-dialogs-mobile', function() {
			if ( typeof socket.userInfo == 'undefined' ) return;
			settingsManager.getOpenDialogs(socket.userInfo.userId, function(openDialogs) {
				var od = [];
				if ( openDialogs ) {
					userManager.getUsersV2(openDialogs, function(users) {
						messageManager.getWithLastMessage(socket.userInfo.userId, openDialogs, function(conversations) {
							if ( conversations ) {
								for(var i = 0; i < conversations.length; i++) {
									userId = conversations[i].userId;
									if ( users[userId] ) {

										//IF user is disabled remove him from opened dialogs
										if ( users[userId].activeStatus == 'NOT ACTIVE' ) {
											settingsManager.removeOpenDialog(userInfo.userId, userId);
											continue;
										}

										od.push({
											userId : userId,
											userInfo : users[userId].info,
											lastMessage: {
												userId: userId,
												body: conversations[i].lastMessage,
												date: conversations[i].lastMessageDate
											}
										});
									} else {
										settingsManager.removeOpenDialog(userInfo.userId, userId);
									}
								}
							}
							socket.emit('open-dialogs-mobile', {dialogs : od});
						});
					});
				}
			});
		});
	});
}

//Sending new-messages, online-users, opened-dialogs etc
function sendNotifications(socket)
{
	var userInfo = socket.userInfo;
	if ( ! userInfo ) return;
	/*-->Getting friends*/
	var uids = userInfo.friendUserIds.concat(userInfo.favouritesIds).concat(userInfo.notConfirmedFriendUserIds);
	//userManager.getUsers(uids, true, function(friends) {
	userManager.getUsersV2(uids, function(friends) {
		//We need only info of users(public info)
		pFriends = {};
		for ( var i in friends ) {
			pFriends[friends[i].info.userId] = friends[i].info;
		}
		socket.emit('online-users-list', pFriends);
		
		
		//Checking if has opened dialogs. 
		//if yes getting userInfo and emiting open-dialogs event
		settingsManager.getOpenDialogs(userInfo.userId, function(openDialogs) {
			var od = [];
			var activeDialog = false;
			if ( openDialogs ) {
				//userManager.getUsers(openDialogs, true, function(users) {
				userManager.getUsersV2(openDialogs, function(users) {
					for(var i = 0; i < openDialogs.length; i++) {
						userId = openDialogs[i];
						if ( users[userId] ) {

							//IF user is disabled remove him from opened dialogs
							if ( users[userId].activeStatus == 'NOT ACTIVE' ) {
								settingsManager.removeOpenDialog(userInfo.userId, userId);
								continue;
							}

							od.push({
								'userId' : userId,
								'userInfo' : users[userId].info
							});
						} else {
							settingsManager.removeOpenDialog(userInfo.userId, userId);
						}
					}
					settingsManager.getActiveDialog(userInfo.userId, function(activeDialog) {
						socket.emit('open-dialogs', {dialogs : od, activeDialog : activeDialog});
					});
				});
			}
		});




		//Checking if has new messages. if yes emiting new-message event.
		messageManager.getNewMessagesCount(userInfo.userId, function(messages) {
			if ( messages && messages.length ) {
				userIds = [];
				//gathering user ids to call getUsers
				for ( var i = 0; i < messages.length; i++ ) {
					userIds.push(messages[i].userId);
				}

				//getting users and merging with messages
				//userManager.getUsers(userIds, true, function(users) {
				userManager.getUsersV2(userIds, function(users) {
					for ( var i = 0; i < messages.length; i++ ) {
						if ( users[messages[i].userId] ) {
							//If user is not online, getting him from api
							//Api will return online if he is logged in
							//User can disable chat. Then we must change status from 'online' to 'offline''
							/*availability = settingsManager.getAvailability(messages[i].userId);
							if ( availability == 0 ) {
								users[messages[i].userId].info.status = 'offline';
							}*/


							messages[i].senderInfo = users[messages[i].userId].info;
						}
					}
					socket.emit('new-messages', messages);
				});
			}
		});
	});
	/*-->Collecting online friends*/
					
	/*-->Getting pending friends*/
	if ( userInfo.pendingFriendsUserIds.length > 0 ) {
		userManager.getUsersV2(userInfo.pendingFriendsUserIds, function(pendingFriends) {
			for(userId in pendingFriends) {
				pendingFriends[userId] = pendingFriends[userId].info;
			}
			
			socket.emit('pending-friends', pendingFriends);
		});
	}
	/*<--Getting pending friends*/
	
	/*-->Getting pending cam requests*/
	if ( userInfo.pendingCamRequests.length > 0 && userInfo.userId == 12664 ) {
		userManager.getUsersV2(userInfo.pendingCamRequests, function(pendingRequests) {
			for(userId in pendingRequests) {
				pendingRequests[userId] = pendingRequests[userId].info;
			}
			
			socket.emit('pending-cam-requests', pendingRequests);
		});
	}
	/*<--Getting pending cam requests*/

	
}

function emitToGroup(userIds, event, data)
{
	//Getting online friends and emitting them an event
	if ( userIds ) {
		userManager.getUsersV2(userIds, function(res) {
			for( var userId in res ) {
				user = res[userId];
				if ( user.status != 'offline' && user.sockets && user.sockets.length ) {
					for ( var i = 0; i < user.sockets.length; i++ ) {
						io.sockets.socket(user.sockets[i]).emit(event, data);
					}
				}
			}
		});
	} else {
		usersPrivateList = userManager.getUsersPrivateList();
		
		for ( var userId in usersPrivateList) {
			var user = usersPrivateList[userId];
			if ( user.status != 'offline' && user.sockets && user.sockets.length ) {
				for ( var i = 0; i < user.sockets.length; i++ ) {
					io.sockets.socket(user.sockets[i]).emit(event, data);
				}
			}
		}
	}
}

function checkPmAllow(senderInfo, participantInfo)
{
	isFriend = false;
	isFavourite = false;

	for( i = 0; i < participantInfo.friendUserIds.length; i++ ) {
		if ( senderInfo.userId == participantInfo.friendUserIds[i] ) {
			isFriend = true;
			break;
		}
	}

	for( i = 0; i < participantInfo.favouritesIds.length; i++ ) {
		if ( senderInfo.userId == participantInfo.favouritesIds[i] ) {
			isFavourite = true;
			break;
		}
	}

	var pmAllow = participantInfo.pmAllow;


	var result = {
		allow: true,
		reason: ''
	};

	if ( pmAllow.indexOf('all') != -1 || senderInfo.isAdmin || senderInfo.isOperator ) { //If pm set to all
		return result;
	}


	if ( participantInfo.userType == 'member' ) {
		if ( 
			( pmAllow.indexOf('onlygirls') != -1 && senderInfo.userType == 'escort') ||
			( pmAllow.indexOf('onlyfriends') != -1 && isFriend) 
		) {
			return result;
		}


		if ( pmAllow.indexOf('onlygirls') != -1 && senderInfo.userType != 'escort' ) { 
			result.allow = false;
			return result;
		} else if( pmAllow.indexOf('onlyfriends') != -1 && ! isFriend ) { 
			result.allow = false;
			return result;
		}
	}

	if ( participantInfo.userType == 'escort' ) {
		if ( 
			(pmAllow.indexOf('favorites') != -1 && isFavourite) ||
			( pmAllow.indexOf('withtokensplusfriend') != -1 && (senderInfo.isPremium || isFriend) ) ||
			( pmAllow.indexOf('withtokens') != -1 && senderInfo.isPremium) ||
			( senderInfo.userType == 'escort' )
		) {
			return result;
		}


		if ( pmAllow.indexOf('withtokensplusfriend') != -1 && ! senderInfo.isPremium && ! isFriend ) {
			result.allow = false;
			result.reason = 'basic-restriction';
			return result;
		} else if ( pmAllow.indexOf('withtokens') != -1 && ! senderInfo.isPremium ) {
			result.allow = false;
			result.reason = 'basic-restriction';
			return result;
		} else if ( pmAllow.indexOf('favorites') != -1 && ! isFavourite ) {
			result.allow = false;
			result.reason = '';
			return result;
		}


	}

	return result;
	
	
	/*isFriend = false;
	isFavourite = false;
	
	for( i = 0; i < participantInfo.friendUserIds.length; i++ ) {
		if ( senderInfo.userId == participantInfo.friendUserIds[i] ) {
			isFriend = true;
			break;
		}
	}
	
	for( i = 0; i < participantInfo.favouritesIds.length; i++ ) {
		if ( senderInfo.userId == participantInfo.favouritesIds[i] ) {
			isFavourite = true;
			break;
		}
	}
	
	var pmAllow = participantInfo.pmAllow;
	
	if ( pmAllow.indexOf('all') != -1 || isFriend ) { //If pm set to all
		return true;
	}
	
	
	
	var allow = false;
	
	if ( participantInfo.userType == 'member' ) {
		if ( pmAllow.indexOf('onlygirls') != -1 && senderInfo.userType == 'escort' ) { // If set to only girls
			allow = true;
		}
		
		if ( pmAllow.indexOf('onlyfriends') != -1 && isFriend ) { // if set only friends
			allow = true;
		}
	} 
	
	if ( participantInfo.userType == 'escort' ) {
		
		if ( pmAllow.indexOf('favorites') != -1 && isFavourite ) {
			allow = true;
		}
		
		if ( pmAllow.indexOf('withtokens') != -1 && senderInfo.hasTokens ) {
			allow = true;
		}
	}
	
	
	return allow;*/
}

function checkRestrictions(senderInfo, participantInfo, settingsManager, callback) {
	//Limitation for basic members. can start only 3 conversations per hour
	
	var result = {
		allow: true,
		reason: ''
	};
	
	/*if ( senderInfo.userId != 12664 ) {return true;}*/

	if ( participantInfo.ignoreUserIds.indexOf(senderInfo.userId) != -1 ) {
		result.reason = 'ignore-list';
		result.allow = false;
		
		return callback(result);
	}
	
	if ( senderInfo.isPremium ) {
		return callback(result);
	}
	
	/*isFriend = false;
	for( i = 0; i < participantInfo.friendUserIds.length; i++ ) {
		if ( senderInfo.userId == participantInfo.friendUserIds[i] ) {
			isFriend = true;
			break;
		}
	}*/
	
	//if friend allow anyway
	if ( senderInfo.userType == 'escort' ) {
		result.allow = true;
		return callback(result);
	}
	
	/*--> Do not allow basic members pm to models*/
	/*if ( senderInfo.userType == 'member' && senderInfo.memberStatus == 'Basic' && participantInfo.userType == 'escort' ) {
		result.allow = false;
		result.reason = 'restr-member-to-model';
		return callback(result);
	}*/
	/*<-- Do not allow basic members pm to models*/
	
	
	/*-->Restriction in conversations count per hour for basic members*/
	if ( senderInfo.userType == 'member' && participantInfo.userType == 'member' ) {
		settingsManager.getConversations(senderInfo.userId, function(conversations) {

			if ( conversations && conversations.length >= config.common.conversationsLimit ) {
				result.allow = false;
				result.reason = 'conversation-limit';

				var isAlreadyInList = false;
				for(var i = 0; i < conversations.length; i++) {
					if ( conversations[i].userId == participantInfo.userId ) {
						isAlreadyInList = true;
					}
				}

				if ( isAlreadyInList ) {
					result.allow = true;
					result.reason = '';
				}
			}
			if ( participantInfo.userType == 'member' && result.allow ) {
				settingsManager.addConversation(senderInfo.userId, participantInfo.userId);
			}
			
			return callback(result);
		});
	} else {
		return callback(result);
	}
	/*<--Restriction in conversations count per hour for basic members*/
	
	
	
}


function removeDeadSockets() {
	users = userManager.getUsersPrivateList();
	for(userId in users) {
		if ( typeof users[userId] !== 'undefined' && typeof users[userId].sockets !== 'undefined' ) {
			for(i in users[userId].sockets) {
				var sock = users[userId].sockets[i];
				if ( ! io.sockets.sockets[sock] ) {
					console.log("Socket not connected, userId: " + userId);
					//userManager.removeSocket(userId, sock);
					//console.log('Socket removed ' + userId);
				}
			}
		}
	}
}


exports.start = start;