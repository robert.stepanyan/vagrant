function Config() {
	var config = {
		production : {
			common : {
				listenPort : 8899,
				host: 'www.escortforum.net',
				authPath : '/get_user_info.php',
				sessionChacheTime: 30 * 60 * 1000, //30 minutes
				imagesHost : 'pluginnetwork.net/static/',
				imageGenMin: 1,
				imageGenMax: 5,
				avatarSize : '40x30',
				noPhotoUrl : '/img/chat/img-no-avatar.gif',
				userSettingsBackupPath : 'user_settings.bk',
				key : 'JFusL12f8A9',
				camplaceSecret: 'laskjasldkjaslkdjasldjlajasdljasdlkjasdlkjasdldjaslasdjlasdjasdljasdlkj',
				conversationsLimit: 5, // Limitation for basic members. can start only 5 conversations per hour
				conversationsLimitPeriod: 60 * 60 * 1000,//1 hour 
				dictionaryCachePath : 'dictionary_cache/',
				dictionaryCacheTime : 1 * 60 * 60 * 1000,//1 hour
				keywordsCacheTime : 1 * 60 * 60 * 1000//1 hour
			},
			db : {
				user:     'chat',
				database: 'camplace_chat',
				password: '8e3_eBd6cAE0',
				host:     'localhost'
			},
			api : {
				host: 'scripts.camplace.com',
				path : '/Api',
				secretCode : '6537AD5E7DDE8',
				accountId : 10241,
				tokenCacheTime : 1000 * 60 * 60 * 12, //12 hours
				notifyOnlineUsersPeriod : 1000 * 60 * 1, /** 2*/
				translationGroupId: 11213
			},
			redis : {
				host : '127.0.0.1',
				port : 6379,
				password : '9E72E32A9263BC503C2'
			}
		},
		development: {
			common : {
				listenPort : 8899,
				host: 'www.escortforum.net',
				authPath : '/get_user_info.php',
				sessionChacheTime: 30 * 60 * 1000, //30 minutes
				imagesHost : 'http://content.pluginnetwork.com/handle/image.ashx',
				avatarSize : '40x30',
				noPhotoUrl : '/img/chat/img-no-avatar.gif',
				userSettingsBackupPath : 'user_settings.bk',
				key : 'JFusL12f8A9',
				dictionaryCachePath : 'dictionary.bk',
				dictionaryCacheTime : 60 * 60 * 1000//1 hour
			},
			db : {
				user:     'chat',
				database: 'camplace_chat',
				password: '8e3_eBd6cAE0',
				host:     'localhost'
			},
			api : {
				host: 'freecamplace.com',
				path : '/handle/dataservice.ashx',
				secretCode : '6537AD5E7DDE8',
				accountId : 10241,
				tokenCacheTime : 1000 * 60 * 60 * 12, //12 hours
				notifyOnlineUsersPeriod : 1000 * 60 * 2
			},
			redis : {
				host : '127.0.0.1',
				port : 6379,
				password : '9E72E32A9263BC503C2'
			},
			mongoDb: {
				host: '127.0.0.1',
				port: 27017,
				database: 'camplaceChat'
			}
		}
	};
	
	this.get = function(type) {
		if ( type == 'development' ) {
			return config.development;
		} else {
			return config.production;
		}
	}
}

function get(type) {
	conf = new Config();
	return conf.get(type);
}



exports.get = get;
