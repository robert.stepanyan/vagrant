/*require('nodetime').profile({
	accountKey: 'ceefd8448a5c5fab987f12e9e2888ca8f5e89998', 
	appName: 'Node.js Application'
});*/


var socketServer = require("./libs/socketServer.js");

var winston = require('winston');
winston.remove(winston.transports.Console);
winston.add(winston.transports.File, { filename: 'debug.log' });

console.log = function(text) {
	winston.info(text);
};


socketServer.start();

