function sessionManager(app) 
{
	var USER_SESSION_DATABASE = 0;//Storing user_id-session_id pairs
	var SESSION_USER_DATABASE = 1;//Storing session_id-user_id pairs
	
	var redisClient = app.get('redisClient');
	
	this.storeSession = function(sessionId, userId, callback) {
		redisClient.select(USER_SESSION_DATABASE, function() {
			redisClient.set(userId, sessionId);
			
			redisClient.select(SESSION_USER_DATABASE, function() {
				redisClient.set(sessionId, userId);
				
				callback();
			});
		});
	}
	
	this.getSessionByUserId = function(userId, callback) {
		redisClient.select(USER_SESSION_DATABASE, function() {
			redisClient.get(userId, function(err, session) {
				if ( err ) console.log(err);
				callback(session);
			});
		});
	}
	
	this.getSession = function(sessionId, callback) {
		redisClient.select(SESSION_USER_DATABASE, function() {
			redisClient.get(sessionId, function(err, res) {
				if ( err ) console.log(err);
				callback(res);
			});
		});
	}
}

module.exports = sessionManager;

