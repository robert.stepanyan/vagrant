function UserManager(app) {
	var dbClient = app.get('mysqlClient');
	
	var crypto = require('crypto');
	
	this.USER_STATUS_OFFLINE = 0;
	this.USER_STATUS_ONLINE = 1;
	this.USER_STATUS_NOT_CONFIRMED = 8;
	this.USER_STATUS_REQUEST_SENT = 9;
	
	var sockets = {};

	this.getUser = function(id, callback)
	{
		var self = this;
		dbClient.fetchRow('SELECT id AS Id, username AS ChatName, email AS Email, full_name AS FullName, details AS Details, img_path AS ImgPath, status AS Status, public_key AS PublicKey FROM users WHERE id = ?', [id], function(result) {
			var ss = self.getSockets(id);
			if ( ss.length == 0 ) {
				result.Status = self.USER_STATUS_OFFLINE;
			}
			callback(result);
		});
	}
	
	this.getUsers = function(ids, callback)
	{
		var self = this;
		dbClient.fetchAll('SELECT id AS Id, username AS ChatName, email AS Email, full_name AS FullName, details AS Details, img_path AS ImgPath, status AS Status, public_key AS PublicKey FROM users WHERE id IN (' + ids.join(',') + ')', [],  function(result) {
			for( i = 0; i < result.length; i++ ) {
				var ss = self.getSockets(result[i].Id);
				
				if ( ss.length == 0 ) {
					result[i].Status = self.USER_STATUS_OFFLINE;
				}
			}
			callback(result);
		});
	}

	this.getUserByUsernamePassword = function(username, password, callback) {
		//MD5 password
		password = crypto.createHash('md5').update(password).digest("hex");
		dbClient.fetchRow('SELECT id AS Id, username AS ChatName, email AS Email, full_name AS FullName, details AS Details, img_path AS ImgPath, status AS Status, public_key AS PublicKey FROM users WHERE username = ? AND password = ?', [username, password], function(result) {
			callback(result);
		});
		
		return;
	}
	
	this.getContact = function(userId, contactUserId, callback) {
		var self = this;
		dbClient.fetchRow('SELECT u.id AS Id, u.username AS ChatName, u.email AS Email, u.full_name AS FullName, u.details AS Details, u.img_path AS ImgPath, if(fl.group_id IS NULL, 0, fl.group_id) AS GroupId, if(fl.is_confirmed = 1, u.status, if(fl.group_id IS NULL, ' + this.USER_STATUS_NOT_CONFIRMED + ', ' + this.USER_STATUS_REQUEST_SENT + ')) AS Status, public_key AS PublicKey FROM friend_list fl INNER JOIN users u ON fl.friend_user_id = u.id WHERE fl.user_id = ? AND fl.friend_user_id = ?', [userId, contactUserId], function(result) {
			var ss = self.getSockets(userId);
			if ( ss.length == 0 ) {
				result.Status = self.USER_STATUS_OFFLINE;
			}
			callback(result);
		});
		
		return;
	}
	
	this.getContactList = function(userId, callback) {
		var self = this;
		dbClient.fetchAll('SELECT u.id AS Id, u.username AS ChatName, u.email AS Email, u.full_name AS FullName, u.details AS Details, u.img_path AS ImgPath, if(fl.group_id IS NULL, 0, fl.group_id) AS GroupId, if(fl.is_confirmed = 1, u.status, if(fl.group_id IS NULL, ' + this.USER_STATUS_NOT_CONFIRMED + ', ' + this.USER_STATUS_REQUEST_SENT + ')) AS Status, public_key AS PublicKey FROM friend_list fl INNER JOIN users u ON fl.friend_user_id = u.id WHERE fl.user_id = ?', [userId], function(result) {
			for( i = 0; i < result.length; i++ ) {
				var ss = self.getSockets(result[i].Id);
				
				if ( ss.length == 0 ) {
					result[i].Status = self.USER_STATUS_OFFLINE;
				}
			}
			callback(result);
		});
		
		return;
	}
	
	this.getGroupList = function(userId, callback) {
		dbClient.fetchAll('SELECT id AS Id, name AS GroupName, Details, img_path AS ImgPath FROM groups WHERE user_id = ? ', [userId], function(result) {
			callback(result);
		});
		
		return;
	}
	
	this.searchContact = function(userId, filter, callback)
	{
		var where = [];
		var bind = [];
		
		
		bind.push(userId)
		
		if ( filter.ChatName && filter.ChatName.length ) {
			where.push('username LIKE ?');
			bind.push('%' + filter.ChatName + '%');
		}
		if ( filter.FullName && filter.FullName.length ) {
			where.push('full_name LIKE ?');
			bind.push('%' + filter.FullName + '%');
		}
		if ( filter.Email && filter.Email.length ) {
			where.push('email LIKE ?');
			bind.push('%' + filter.Email + '%');
		}
		
		dbClient.fetchAll('SELECT id AS Id, username AS ChatName, email AS Email, full_name AS FullName, details AS Details, img_path AS ImgPath FROM users WHERE id <> ? AND (' + where.join(' OR ') + ')', 
		bind, function(result) {
			callback(result);
		});
		
		return;
	}
	
	this.addContact = function(userId, contactUserId, callback)
	{
		var self = this;
		dbClient.fetchRow('SELECT id FROM users WHERE id = ?', contactUserId, function(contact) {
			if ( contact ) {
				dbClient.query('SELECT user_id FROM friend_list WHERE user_id = ? AND friend_user_id = ?', [contactUserId, userId], function(error, result) {
					isExist = result[0] && result[0].user_id ? 1 : 0;
					
					dbClient.query('INSERT INTO friend_list (user_id, friend_user_id, group_id, is_confirmed) VALUES(?, ?, ?, ?)', [userId, contactUserId, 0, isExist], function(error, result) {
						if ( error ) {
							console.log(error);
						}
						if ( isExist ) {
							dbClient.query('UPDATE friend_list SET is_confirmed = 1 WHERE user_id = ? AND friend_user_id = ?', [contactUserId, userId], function(error, result) {
								self.getContact(userId, contactUserId, function(contact) {
									callback(contact);
								});
							});
						} else {
							dbClient.query('INSERT INTO friend_list (user_id, friend_user_id) VALUES(?, ?)', [contactUserId, userId], function(error, result) {

								self.getContact(userId, contactUserId, function(contact) {
									callback(contact);
								});
							});
						}
						

					});
				});
				
			}

		});
	}
	
	this.confirmContact = function(userId, contactId, callback)
	{
		var self = this;
		dbClient.query('UPDATE friend_list SET is_confirmed = 1 WHERE (user_id = ? && friend_user_id = ?) OR (user_id = ? && friend_user_id = ?)', [userId, contactId, contactId, userId], function(err, res) {
			if ( err ) console.log(err);
				
			dbClient.query('UPDATE friend_list SET group_id = ? WHERE user_id = ? AND friend_user_id = ?', [0, userId, contactId], function(err, res) {
				if ( err ) {
					console.log(err);
				}

				self.getContact(userId, contactId, function(user) {
					callback(user);
				});
			});
			
		});
	}
	
	this.removeContacts = function(userId, friendUserIds, callback)
	{
		dbClient.query('DELETE FROM friend_list WHERE user_id = ? AND friend_user_id IN (' + friendUserIds.join(',') + ')', [userId], function(err, result) {
			if ( err ) {
				console.log(err);
			}
			dbClient.query('UPDATE friend_list SET is_confirmed = 0 WHERE friend_user_id = ? AND user_id IN (' + friendUserIds.join(',') + ')', [userId], function(err, result) {
				if ( err ) {
					console.log(err);
				}
				callback(friendUserIds);
			});
			
		});
	}
	
	this.createGroup = function(userId, groupName, callback)
	{
		var self = this;
		dbClient.query('INSERT INTO groups (user_id, name) VALUES(?, ?)', [userId, groupName], function(err, result) {
			if ( err ) console.log(err);
			self.getGroup(result.insertId, callback);
		});
	}
	
	this.getGroup = function(id, callback)
	{
		dbClient.fetchRow('SELECT id AS Id, name AS GroupName, Details, img_path AS ImgPath FROM groups WHERE id = ? ', [id], function(res) {
			callback(res);
		});
	}
	
	this.removeGroups = function(userId, groupIds, callback)
	{
		var groupResponse = [];
		dbClient.fetchAll('SELECT id FROM groups WHERE user_id = ? AND id IN (' + groupIds.join(',') + ')', [userId], function(groups) {
			for(i = 0; i < groups.length; i ++) {
				groupResponse.push(groups[i].id);
			}
			
			if ( ! groupResponse.length ) return callback([], []);
			
			dbClient.fetchAll('SELECT u.id AS Id, u.username AS ChatName, u.email AS Email, u.full_name AS FullName, u.details AS Details, u.img_path AS ImgPath, fl.group_id AS GroupId FROM friend_list fl INNER JOIN users u ON fl.friend_user_id = u.id WHERE fl.user_id = ? AND fl.group_id IN (' + groupResponse.join(',') + ')', [userId], function(contacts){
				dbClient.query('UPDATE friend_list SET group_id = 0 WHERE user_id = ? AND group_id IN (' + groupResponse.join(',') + ')', [userId], function(err, res) {
					if ( err ) console.log(err);
					
					dbClient.query('DELETE FROM groups WHERE id IN (' + groupResponse.join(',') + ')', [], function(err, res) {});
				});
				
				for( i = 0; i < contacts.length; i++ ) {
					contacts[i].GroupId = 0;
				}
				
				callback(groupResponse, contacts);
			});
			
		});
	}
	
	this.moveToGroup = function(userId, userIds, groupId, callback)
	{
		var result = [],
			c = 0;
		this.getUsers(userIds, function(users) {
			for( i = 0; i < users.length; i++ ) {
				f = function(k) {
					dbClient.query('UPDATE friend_list SET group_id = ? WHERE user_id = ? AND friend_user_id = ?', [groupId, userId, users[k].Id], function(err, res) {
						if ( err ) console.log(err);

						users[k].GroupId = groupId;
						result.push(users[k]);

						if ( c == users.length - 1 ) {
							callback(result);
						}

						c++;
					});
				}(i);
			}
		});
	}
	
	this.updateUser = function(user, callback)
	{
		dbClient.query('UPDATE users SET email = ?, full_name = ?, details = ? WHERE id = ?', [user.Email, user.FullName, user.Details, user.Id], function(err, res) {
			if ( err ) console.log(err);
			
			callback();
		});
	}
	
	this.updateStatus = function(id, status, callback)
	{
		var self = this;
		dbClient.query('UPDATE users SET status = ? WHERE id = ?', [status, id], function(err, res) {
			if ( err ) console.log(err);
			
			self.getUser(id, function(res) {
				callback(res);
			});
		});
	}
	
	this.addSocket = function(userId, socketId)
	{
		if ( ! sockets[userId] ) {
			sockets[userId] = [];
		} 
	
		sockets[userId].push(socketId);
	}
	
	this.getSockets = function(userId)
	{
		if ( sockets[userId] ) {
			return sockets[userId];
		} else {
			return [];
		}
		
	}
	
	this.removeSocket = function(userId, socketId, callback)
	{
		if ( ! sockets[userId] ) return;
		
		for (i = 0; i < sockets[userId].length; i++) {
			if ( sockets[userId][i] == socketId ) {
				sockets[userId].splice(i, 1);
				break;
			}
		}
		callback();
		
		if ( sockets[userId].length == 0 ) {
			delete sockets[userId];
		}
		
	}

}

module.exports = UserManager;


	