module.exports.get = function(app) {
	redis = require('redis');
	redisConnetions = app.get('redisConnections');
	
	redisClient = redis.createClient(redisConnetions.port, redisConnetions.host);
	
	redisClient.on("error", function (err) {
		console.log("error event - " + redisClient.host + ":" + redisClient.port + " - " + err);
	});
	
	return redisClient;
}