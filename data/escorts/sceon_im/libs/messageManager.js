function MessageManager(app) {
	var dbClient = app.get('mysqlClient');
	
	
	this.storeMessage = function(userIdFrom, userIdTo, message, sender_key, participant_key) 
	{
		createThreadId(userIdFrom, userIdTo, function(threadId) {
			insertMessage(userIdFrom, threadId, message, sender_key, participant_key);
		});
	}
	
	this.getConversation = function(userId, participantId, dateFrom, callback)
	{
		var bind = [], sql;
		var self = this;
		getThreadId(userId, participantId, function(threadId) {
			if ( ! threadId ) {
				return callback([]);
			}
			if ( ! dateFrom ) {
				where = ' WHERE thread_id = ? ';
				bind.push(threadId);
			} else {
				where = ' WHERE thread_id = ? AND (date >= ? OR read_date IS NULL )';
				bind.push(threadId, dateFrom.toMysqlFormat())
			}
			
			sql = 'SELECT user_id AS contactId , body, UNIX_TIMESTAMP(date) * 1000 AS date, sender_key AS key1, participant_key AS key2 FROM messages ' + where + ' ORDER BY id ASC';//getting timestamp in milliseconds
			
			dbClient.query(sql, bind, function(error, messages) {
				for(i in messages) {
					messages[i].body = self.wrapMessage(messages[i].body);
				}
				callback(messages);
			});
		});
	}
	
	this.clearMessage = function(message)
	{
		//htmlentities
		message = htmlEntities(message);
		
		//Replacing one more whitespaces
		message = message.trim();
		message = message.replace(/\s+/g, ' ');
		
		//replacing urls with html links
		/*var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
		message = message.replace(exp,"<a href='$1' target='_blank'>$1</a>"); */
		
		return message;
	}
	
	this.wrapMessage = function(message)
	{
		return message;
		//return '<p>' + message + '</p>';
	}
	
	function createThreadId(userIdFrom, userIdTo, callback)
	{
		dbClient.query(
			'SELECT tp1.thread_id AS threadId FROM threads_participants tp1 INNER JOIN threads_participants tp2 ON tp2.thread_id = tp1.thread_id AND tp2.user_id = ? WHERE tp1.user_id = ?',
			[userIdFrom, userIdTo],
			function(error, result) {
				if ( ! result || result.length == 0 ) {// If doesn't exist with this userIds pair insert
					dbClient.query('INSERT INTO threads VALUES()', function(error, result) {
						createThreadParticipants(result.insertId, userIdFrom, userIdTo, callback);
					});
				} else {
					callback(result[0].threadId);
				}
			}
		);
	}
	
	function getThreadId(userIdFrom, userIdTo, callback)
	{	
		dbClient.query(
			'SELECT tp1.thread_id AS threadId FROM threads_participants tp1 INNER JOIN threads_participants tp2 ON tp2.thread_id = tp1.thread_id AND tp2.user_id = ? WHERE tp1.user_id = ?',
			[userIdFrom, userIdTo],
			function(error, result) {
				if ( error ) {console.log(error); return;}
				
				if ( result && result.length != 0 ) {
					callback(result[0].threadId);
				} else {
					callback(false);
				}
			}
		);
	}
	
	function createThreadParticipants(threadId, userIdFrom, userIdTo, callback)
	{
		dbClient.query('INSERT INTO threads_participants (user_id, thread_id) VALUES(?,?)', [userIdFrom, threadId], function(error, result) {
			dbClient.query('INSERT INTO threads_participants (user_id, thread_id) VALUES(?,?)', [userIdTo, threadId], function(error, result) {
				callback(threadId);
			});
		});
	}
	
	function insertMessage(userId, threadId, message, sender_key, participant_key)
	{
		date = new Date();
		dbClient.query(
			'INSERT INTO messages (user_id, thread_id, body, sender_key, participant_key, date) VALUES(?, ?, ?, ?, ?, NOW())',
			[userId, threadId, message, sender_key, participant_key]
		);
	}
	
	this.markAsRead = function(userId, participantId, callback)  
	{
		getThreadId(userId, participantId, function(threadId) {
			date = new Date();
			client.query('UPDATE messages SET read_date = ? WHERE thread_id = ? AND user_id = ?', [date.toMysqlFormat(), threadId, participantId], function(error, result) {
				if (typeof callback == 'function') callback();
			});
		});
	}
	
	this.getNewMessagesCount = function(userId, callback) 
	{
		sql = 'SELECT count(cm.id) count, u.id AS userId, u.username FROM threads_participants ctp INNER JOIN messages cm ON cm.thread_id = ctp.thread_id INNER JOIN users u ON u.id = cm.user_id WHERE ctp.user_id = ? AND cm.user_id <> ? AND read_date IS NULL GROUP BY cm.user_id';
		client.query(sql, [userId, userId], function(error, result) {
			callback(result);
		});
	}
	
	function twoDigits(d) 
	{
		if(0 <= d && d < 10) return "0" + d.toString();
		if(-10 < d && d < 0) return "-0" + (-1*d).toString();
		return d.toString();
	}
	
	function htmlEntities(str) {
		return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
	}

	Date.prototype.toMysqlFormat = function() {
		return this.getUTCFullYear() + "-" + twoDigits(1 + this.getUTCMonth()) + "-" + twoDigits(this.getUTCDate()) + " " + twoDigits(this.getUTCHours()) + ":" + twoDigits(this.getUTCMinutes()) + ":" + twoDigits(this.getUTCSeconds());
	};
}

module.exports = MessageManager;