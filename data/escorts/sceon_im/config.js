module.exports = function(app, express){
	var config = this;
	
	app.configure(function(){
		app.set('port', 8888);
		app.set('views', __dirname + '/views');
		app.set('view engine', 'jade');
		app.use(express.logger('dev'));
		app.use(express.bodyParser());
		app.use(express.methodOverride());
		app.use(app.router);
		
	});

	app.configure('development', function() {
		app.set('port', 8899);
		
		app.set('mysqlConnections', {
			host : '192.168.0.1',
			user : 'sceon',
			password : '123456',
			database : 'sceon_messenger'
		});
		
		app.set('redisConnections', {
			port : '6379',
			host : '192.168.0.1',
			options : {}
		});
		
		app.use(express.errorHandler({ 
			dumpExceptions: true, 
			showStack: true 
		}));
	});
	
	app.configure('production', function() {
		app.set('port', 8899);
		
		app.set('mysqlConnections', {
			host : '192.168.0.1',
			user : 'sceon',
			password : '123456',
			database : 'sceon_messenger'
		});
		
		app.set('redisConnections', {
			port : '6379',
			host : '127.0.0.1',
			options : {}
		});
		
		app.use(express.errorHandler());
	});
	
	return config;
};