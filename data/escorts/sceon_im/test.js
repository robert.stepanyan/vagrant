var express = require('express')
  , routes = require('./routes')
  , user = require('./routes/user')
  , path = require('path')
  , crypto = require('crypto');

var app = express();

var config = require('./config.js')(app, express);

//Setting mysql and redis clients to the app
mysqlClient = require('./libs/mysqlClient').get(app);
redisClient = require('./libs/redisClient').get(app);

app.set('mysqlClient', mysqlClient);
app.set('redisClient', redisClient);
//
var server = app.listen(1111);
//
var io = require('socket.io').listen(server);

UserManager = require('./libs/userManager');
userManager = new UserManager(app);


MessageManager = require('./libs/messageManager');
messageManager = new MessageManager(app);

messageManager.getConversation(1, 2, null, function(result) {
	console.log(result);
});


/*userManager.createGroup(1, 'tesssssst', function(res) {
	console.log(res);
});*/

/*SessionManager = require('./libs/sessionManager');
sessionManager = new SessionManager(app);

var sessionId = 'aaaaaaaaaaaaaaaaaaaaaaaa';
userId = 99;

sessionManager.getSessionByUserId(99, function(res) {
	console.log(res);
});*/

/*sessionManager.storeSession(sessionId, userId, function() {
	sessionManager.getSession(sessionId, function(res) {
		console.log(res);
	});
});*/



