var express = require('express')
  , routes = require('./routes')
  , user = require('./routes/user')
  , path = require('path')
  , crypto = require('crypto')
  , cookie = require('cookie');

var app = express();

var config = require('./config.js')(app, express);

//Setting mysql and redis clients to the app
mysqlClient = require('./libs/mysqlClient').get(app);
redisClient = require('./libs/redisClient').get(app);

app.set('mysqlClient', mysqlClient);
app.set('redisClient', redisClient);

UserManager = require('./libs/userManager');
SessionManager = require('./libs/sessionManager');
MessageManager = require('./libs/messageManager');

userManager = new UserManager(app);
sessionManager = new SessionManager(app);
messageManager = new MessageManager(app);

var server = app.listen(app.get('port'));
var io = require('socket.io').listen(server);

app.get('/', routes.index);

var ss = 0;

io.of('/private').authorization(function(handshakeData, callback) {
	if ( handshakeData.headers.cookie) {
		var cookies = cookie.parse(handshakeData.headers.cookie);
		if ( cookies['sessionId'] ) {
			sessionManager.getSession(cookies['sessionId'], function(session) {
				//actually sessin is userId
				if ( session ) {
					handshakeData.sessionId = session;
					
					callback(null, true);
				} else {
					callback(null, false);
				}
			});
		} else {
			callback(null, false);
		}
	}
	else {
		callback(null, false);
	}
});

io.of('/private').on('connection', function (socket) {
	var userId = socket.handshake.sessionId;
	
	if ( ! userId ) return;
	
	var firstSocket = false;
	
	ss = userManager.getSockets(userId);
	
	if ( ! ss.length ) {
		firstSocket = true;
	}
	
	userManager.addSocket(userId, socket.id);
	
	if ( firstSocket ) {
		userManager.getUser(userId, function(user) {
			emitFriends(userId, 'statusChanged', user );
		});
		
	}

	socket.on('getContactList', function(data) {
		userManager.getContactList(userId, function(result) {
			socket.emit('contactListResponse', result);
		});
	});

	socket.on('getGroupList', function(data) {
		userManager.getGroupList(userId, function(result) {
			socket.emit('groupListResponse', result);
		});
	});
	
	socket.on('searchContact', function(data) {
		userManager.searchContact(userId, data, function(result) {
			socket.emit('searchContactResponse', result);
		});
	});
	
	socket.on('addContact', function(contactUserId) {
		userManager.addContact(userId, contactUserId, function(result) {
			socket.emit('addContactResponse', result);
			
			ss = userManager.getSockets(contactUserId);
			if ( ss.length ) {
				userManager.getUser(userId, function(user) {
					for( i = 0; i < ss.length; i++ ) {
						user.status = userManager.USER_STATUS_NOT_CONFIRMED;
						io.of('/private').socket(ss[i]).emit('friendRequest', user);
					}
				});
			}
		});
	});
	
	socket.on('confirmFriendRequest', function(contactUserId) {
		userManager.confirmContact(userId, contactUserId, function(user) {
			socket.emit('confirmFriendRequestResponse', user);
			
			ss = userManager.getSockets(contactUserId);
			if ( ss.length ) {
				userManager.getUser(userId, function(user) {
					for( i = 0; i < ss.length; i++ ) {
						io.of('/private').socket(ss[i]).emit('friendRequestConfirmed', user);
					}
				});
			}
		});
	});
	
	socket.on('removeContacts', function(contactUserIds) {
		userManager.removeContacts(userId, contactUserIds, function(result) {
			socket.emit('removeContactsResponse', result);
		});
	});
	
	socket.on('createGroup', function(groupName) {
		userManager.createGroup(userId, groupName, function(res) {
			socket.emit('createGroupResponse', res);
		});
	});
	
	socket.on('removeGroups', function(groupIds) {
		userManager.removeGroups(userId, groupIds, function(groupIds, contacts) {
			socket.emit('removeGroupsResponse', {groupIdList: groupIds, contactList: contacts});
		});
	});
	
	socket.on('moveToGroup', function(data) {
		userManager.moveToGroup(userId, data.contactIdList, data.groupId, function(res) {
			socket.emit('moveToGroupResponse', res);
		});
	});
	
	socket.on('changeDetail', function(user) {
		userManager.updateUser(user, function() {
			socket.emit('changeDetailResponse', user);
			emitFriends(user.Id, 'detailChanged', user);
		});
	});
	
	socket.on('changeStatus', function(status) {
		userManager.updateStatus(userId, status, function(user) {
			
			socket.emit('changeStatusResponse', user);
			emitFriends(user.Id, 'statusChanged', user );
		});
	});
	
	socket.on('getConversation', function(data) {
		messageManager.getConversation(userId, data.contactId, data.date, function(result) {
			socket.emit('getConversationResponse', result);
		});
	});
	
	socket.on('newMessage', function(data) {
		console.log(data);
		user = userManager.getUser(data.contactId, function(user) {
			if ( user ) {
				message = messageManager.clearMessage(data.body);
				if ( message.length > 0 ) {
					messageManager.storeMessage(userId, data.contactId, data.body, data.key1, data.key2);
					
					/*-->Sending response event to the all sockets*/
					
					var selfSockets = userManager.getSockets(userId);
					data.date = new Date().getTime();
					
					for ( i = 0; i < selfSockets.length; i++ ) {
						io.of('/private').socket(selfSockets[i]).emit('newMessageResponse', data);
					}
					/*<--Sending response event to the all sockets*/
					
					ss = userManager.getSockets(data.contactId);
					if ( ss.length ) {
						newMessageData = {
							body : message,
							contactId : userId,
							date : new Date().getTime(),
							key1: data.key1,
							key2: data.key2
						}
						for( i = 0; i < ss.length; i++ ) {
							io.of('/private').socket(ss[i]).emit('newMessage', newMessageData);
						}
					}
				}
			}
		});
	});
	
	socket.on('markAsRead', function(participantUserId) {
		messageManager.markAsRead(userId, participantUserId);
	});
		
	socket.on('disconnect', function() {
		userManager.removeSocket(userId, socket.id, function() {
			ss = userManager.getSockets(userId);
			
			if ( ss.length == 0 ) {
				userManager.getUser(userId, function(user) {
					emitFriends(userId, 'statusChanged', user);
				});
			}
			
		});
		
		
	});
	
});

io.sockets.on('connection', function (socket) {
	socket.on('Authenticate', function(authData) {
		authenticationResponse = {
			success : false,
			message : '',
			sessionId : ''
		};
		
		userManager.getUserByUsernamePassword(authData.username, authData.password, function(user) {
			if ( ! user ) {
				authenticationResponse.message = 'Wrong Credentials';
				socket.emit('AuthenticationResponse', authenticationResponse);
				return;
			}
			
			sessionManager.getSessionByUserId(user.Id, function(sessionId) {
				if ( ! sessionId ) {
					//TODO check if sessionId is already exists
					sessionId = crypto.randomBytes(32).toString('hex');
					sessionManager.storeSession(sessionId, user.Id, function() {});
				}
				authenticationResponse = {
					success : true,
					message : 'Logged in',
					sessionId : sessionId,
					user : user
				};
				
				socket.emit('AuthenticationResponse', authenticationResponse);
			});
			
		});
	});
});

emitFriends = function(userId, event, data)
{
	userManager.getContactList(userId, function(contacts) {
		for( i = 0; i < contacts.length; i++ ) {
			ss = userManager.getSockets(contacts[i]['Id']);//sockets of this user
			if ( contacts[i]['Status'] != userManager.USER_STATUS_NOT_CONFIRMED && contacts[i]['Status'] != userManager.USER_STATUS_REQUEST_SENT && ss && ss.length ) {
				for( j = 0; j < ss.length; j++ ) {
					io.of('/private').socket(ss[j]).emit(event, data);
				}
			}
		}
	});
}