module.exports = function(app, express){
	var config = this;
	
	app.configure(function(){
		app.set('port', 8899);
		app.set('views', __dirname + '/views');
		app.set('view engine', 'jade');
		app.use(express.logger('dev'));
		app.use(express.bodyParser());
		app.use(express.methodOverride());
		app.use(app.router);
		//app.use(express.static(path.join(__dirname, 'public')));
	});

	app.configure('development', function() {
		app.set('port', 8888);
		app.use(express.errorHandler({ 
			dumpExceptions: true, 
			showStack: true 
		}));
	});
	
	app.configure('production', function() {
		app.set('port', 9999);
		app.use(express.errorHandler());
	});
	
	return config;
};