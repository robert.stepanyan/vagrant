Cubix.Button = new Class({
	Implements: [Events, Options],
	
	el: null,
	
	options: {
		caption: 'Unnamed',
		classPrefix: 'cx-button-',
		icon: null,
		imgUrl: '/img/cubix-button/icons',
		imgExt: '.png',
		classes: null
	},
	
	initialize: function (el, options) {
		this.el = $(el);
		
		options = options || {};
		/*if ( ! options.imgUrl ) {
			options.imgUrl = new URI(window.location.href).
				get('directory') + 'img/cubix-button/icons';
		}*/
		
		this.setOptions(options);
		
		this._decorate(this.el);
		
		this.setCaption(this.options.caption);
		
		this.btn.addEvent('mouseenter', this._btnEnter.bind(this));
		this.btn.addEvent('mouseleave', this._btnLeave.bind(this));
		
		this.btn.addEvent('mousedown', this._btnDown.bind(this));
		this.btn.addEvent('mouseup', this._btnUp.bind(this));
		
		this.btn.addEvent('click', function (e) {
			e.stop();
			this.fireEvent('click', e);
		}.bind(this));
	},
	
	_btnEnter: function () {
		this.btn.addClass(this.options.classPrefix + 'overed');
	},
	
	_btnLeave: function () {
		this.btn.removeClass(this.options.classPrefix + 'overed');
	},
	
	_btnDown: function () {
		this.btn.addClass(this.options.classPrefix + 'pushed');
		
		var fn;
		
		fn = function () {
			this._btnUp();
		};
		
		document.addEvent('mouseup', fn.bind(this));
	},
	
	_btnUp: function () {
		this.btn.removeClass(this.options.classPrefix + 'pushed');
	},
	
	
	_decorate: function (el) {
		var btn = this._createElement().wraps(el);
		if ( this.options.classes ) {
			btn.addClass(this.options.classes);
		}
		
		this.btn = btn;
		
		var l = this._createElement('left').wraps(el);
		var r = this._createElement('right').wraps(el);
		var m = this._createElement('middle').wraps(el);
		
		var caption = this._createElement('caption', 'span').wraps(el);
		this.caption = caption;
		
		var wrapper = this._createElement('wrapper').wraps(caption);
		
		if ( this.options.icon ) {
			caption.addClass(this.options.classPrefix + 'icon');
			caption.setStyle('background-image', 'url(' + this.options.imgUrl + '/' + this.options.icon + this.options.imgExt + ')');
		}
	},
	
	_createElement: function (what, type) {
		type = type || 'div';
		return new Element(type, {
			'class': (what ? 
				this.options.classPrefix + what : 
				this.options.classPrefix.substring(0, this.options.classPrefix.length - 1)
			)
		});
	},
	
	setCaption: function (caption) {
		this.caption.set('html', caption);
	}
});
