Cubix.Tip = new Class({
	Implements: [Events, Options],
	
	el: null,
	
	/*options: {
		contentUrl: ''
	},*/
	
	initialize: function (el, url, options) {
		
		
		
		this.el = $$(el);
		/*options = options || {};
		this.setOptions(options);*/
		
		var tip = new Tips({showDelay: 0, hideDelay: 0});
		
		var func;
		
		this.el.addEvent('mouseenter', func = function(event) {
			/*$$('.tip-wrap').each(function(it) {
				it.setStyle('display', 'none');
			});*/
			
			var showname = this.get('href').split('/');

			showname = showname[showname.length -1];
			new Request({
				method: 'get',
				url: url + '?showname=' + showname,
				onSuccess: function (data) {
					this.store('tip:text', data);
					tip.attach(this);
					tip.elementEnter(event, this);
					this.removeEvent('mouseenter', func);
				}.bind(this)
			}).send();
		});
		
		$$('.tip-wrap').addEvent('mouseleave', function(){
			this.setStyle('display', 'none');
		});
		/*this.el.addEvent('mouseleave', func = function(event) {
			$$('.tip-wrap').each(function(it) {
				it.setStyle('display', 'none');
			});
		});*/		
	}
});
