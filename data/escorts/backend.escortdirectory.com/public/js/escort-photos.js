var Escort = {};

Escort.Photos = function (popup) {
	this.popup = popup;
	this.content = this.popup.content;
	this.overlay = new Cubix.Overlay($('photo-tabs'), {showLoader: false});
	
	this.init_mootabs = function () {
		/* >>> Handle Private and Public photos parameters */
		this.tabs = new mootabs('photo-tabs', {width: null, height: null});
		this.active_tab = 'public';
		
		this.tabs.addEvent('activate', function (tab) {
			if ( tab.get('title') == 'Private' ) {
				//this.uploader.setData({type: 3});
				this.setUploaderAction(3);
				this.active_tab = 'private';
			}
			else if ( tab.get('title') == 'Public' ) {
				//this.uploader.setData({type: 1});
				this.setUploaderAction(1);
				this.active_tab = 'public';
			} else if ( tab.get('title') == 'Disabled' ) {
				//this.uploader.setData({type: 4});
				this.setUploaderAction(4);
				this.active_tab = 'disabled';
			} else if ( tab.get('title') == 'Archived' ) {
				//this.uploader.setData({type: 4});
				this.setUploaderAction(5);
				this.active_tab = 'archived';
			}
			else if ( tab.get('title') == 'Photo-history' ) {
				this.active_tab = 'Photo-history';
			}
			
			if (this.active_tab != 'Photo-history'){
				$(tab.get('title')).getElements('li').each(function (li) {li.cropper.reinitLimits();});
			}
			
		}.bind(this));
		/* <<< */
	};
	
	this.init_uploader = function () {
		var self = this;
		
		this.uploader = new MooUpload('filecontrol', {    
			action: '/escorts-v2/upload-html5?escort_id=' + this.popup.escort_id,// Server side upload script    
			method: 'html5',		// Use only HTML5 method		
			blocksize: 999999999,	// Load per one chunk
			onAddFiles: function() {},
			onFinishUpload: function() {},
			onFileUpload: function(fileindex, resp){
				if ( ! resp.error && resp.finish ) {
					var container = 'public';
					if ( resp.photo.type == 3 ) container = 'private';
					if ( resp.photo.type == 4 ) container = 'disabled';
					if ( resp.photo.type == 5 ) container = 'archived';

					
					var photo = self.render_photo(resp.photo).
						set('opacity', 0).
						inject(self.content.getElement('.photos-' + container).getElement('.clear'), 'before').
						fade(1);

					if ( container == 'public' ) {
						self.mark_photo(photo, 'hard');
					}

					self.sortables.addItems(photo);
				} else if ( resp.error ) {
					if ( ! $$('#filecontrol_listView ul.errors').length ) {
						new Element('ul', {
							'class' : 'errors'
						}).inject($('filecontrol_listView'));
					}
					
					
					var li = new Element('li', {});
					
					li.inject($$('#filecontrol_listView ul.errors')[0]);
					
					new Element('span', {
						'html' : resp.name + ' - '
					}).inject(li);
					new Element('span', {
						'class' : 'mooupload_error',
						'html' : resp.msg
					}).inject(li);
					
					
					
				}
				
				self.overlay.enable();
				window.fireEvent('resize');
			}
		});
		
		/*this.uploader = new Cubix.Uploader({
			url: '/escorts-v2/upload-photo?escort_id=' + this.popup.escort_id
		});
		
		this.uploader.addEvent('fileUploaded', function (resp) {
			if ( resp.status == 'success' ) {
				var container = 'public';
				if ( resp.photo.type == 3 ) container = 'private';
				if ( resp.photo.type == 4 ) container = 'disabled';
				
				var photo = this.render_photo(resp.photo).
					set('opacity', 0).
					inject(this.content.getElement('.photos-' + container).getElement('.clear'), 'before').
					tween('opacity', 1);
				
				if ( container == 'public' ) {
					this.mark_photo(photo, 'hard');
				}
				
				this.sortables.addItems(photo);
			}
			
			this.overlay.enable();
			window.fireEvent('resize');
		}.bind(this));*/
	};
	
	this.setUploaderAction = function(type) {
		this.uploader.options.action = '/escorts-v2/upload-html5?escort_id=' + this.popup.escort_id;
		if ( type ) {
			this.uploader.options.action += '&type=' + type;
		}
		
	}
	
	/* >>> Photos initialization */
	this.render_photo = function (photo) {
		var container = new Element('table', {
			'class': 'photo'
		});
		
		var content = new Element('td', {}).inject(new Element('tr', {}).inject(container));
		
		var wrapper = new Element('div', {'class': 'wrapper'}).inject(content);
		wrapper.data = photo;

		var status = new Element('div', {'class': 'status'}).inject(wrapper);
		var move_handler = new Element('div', {'class': 'handler'}).inject(wrapper);
		var checkbox = new Element('input', {
			type: 'checkbox',
			name: 'photos[]',
			value: photo.id,
			events: {
				change: function () {
					if ( checkbox.get('checked') ) {
						checkbox.set('checked', true);
					}
					else {
						checkbox.set('checked', false);
					}
				}
			}
		}).inject(new Element('div', {'class': 'chk'}).inject(wrapper));
		
		var img = new Element('img', {
			src: photo.image_url
//			events: {
//				click: function () {
//					if ( checkbox.get('checked') ) {
//						checkbox.set('checked', false);
//					}
//					else {
//						checkbox.set('checked', true);
//					}
//				}
//			}
		}).inject(wrapper);
		//console.log(photo);
		var a = new Element('a', {
			href: photo.orig_url + '?orig=true',
			html: 'download',
			target: '_blank',
			'class': 'down_link'

		}).inject(wrapper);

		var li = new Element('li');
		
		li.data = photo;
		
		container.inject(li);
		
		if ( photo.is_verified ) {
			this.mark_photo(li, 'p100');
		}
		
		if ( photo.gotd ) {
			this.mark_photo(li, 'gotd');
		}

		if ( ! photo.is_approved ) {
			this.mark_photo(li, 'not_approved');
		}

		if ( ! photo.double_approved  && $defined($('btn-photos-public-double-approve'))) {
			this.mark_photo(li, 'auto_approved');
		}
		
		if ( photo.is_rotatable ) {
			this.mark_photo(li, 'rotatable');
		}
		
		if ( photo.is_suspicious ) {
			
			this.mark_photo(li, 'suspicious');
		}
		
		if ( photo.is_main && photo.type != 3 ) {
			this.mark_photo(li, 'main');
		}
		
		
		
		if ( photo.type == 1 ) {
			this.mark_photo(li, 'hard');
		}
		else if ( photo.type == 2 ) {
			this.mark_photo(li, 'soft');
		}
		
		if ( photo.retouch_status == 1 ) {
			this.mark_photo(li, 'retouch');
		}
		
		li.cropper = new Cropper(wrapper).addEvent('complete', saveAdjustment);
		
		window.fireEvent('resize');
		
		return li;
	}.bind(this);
	
	this.mark_photo = function (photos, flag) {
		if ( ! photos ) return;
		
		if ( $type(photos) == 'element' ) photos = [photos];
		
		if ( flag == 'main' ) {
			var tab = this.active_tab;
			this.content.getElement('.photos-' + tab).getElements('li').each(function (el) {
				el.getElement('td').removeClass('main');
				el.data.is_main = 0;
			});
		}
		
			
		
		photos.each(function (photo) {
			var status = photo.getElement('.status');
			var flags = new Hash();
			
			status.getElements('div').each(function (el) {
				flags.set(el.get('class'), el);
			});
			
			if ( flag == 'hard' ) {
				if ( flags.get('hard') ) return;
				flag = new Element('div', {'class': 'hard', html: 'hard'});
				if ( flags.get('soft') ) flag.replaces(flags.get('soft'));
				else flag.injectBottom(status);
			}
			else if ( flag == 'soft' ) {
				if ( flags.get('soft') ) return;
				flag = new Element('div', {'class': 'soft', html: 'soft'});
				if ( flags.get('hard') ) flag.replaces(flags.get('hard'));
				else flag.injectBottom(status);
			}
			else if ( flag == 'p100' ) {
				if ( flags.get('p100') ) {flags.get('p100').destroy();return;}
				new Element('div', {'class': 'p100'}).injectTop(status);
			}
			else if ( flag == 'gotd' ) {
				if ( flags.get('gotd') ) {flags.get('gotd').destroy();return;}
				this.content.getElements('.gotd').destroy();
				new Element('div', {'class': 'gotd', html: 'GOTD'}).injectTop(status);
			}
			else if ( flag == 'private' ) {
				if ( flags.get('hard') ) flags.get('hard').destroy();
				if ( flags.get('soft') ) flags.get('soft').destroy();
				photo.getElement('.wrapper').removeClass('main');
				return;
			}
			else if ( flag == 'main' ) {
				photo.getElement('td').addClass('main');
				photo.data.is_main = 1;
			}
			else if ( flag == 'rotatable' ) {
				
				if(photo.data.is_main != 1){
					photo.getElement('td').addClass('rotatable');
				}
			}
			else if ( flag == 'not_approved' ) {
				//var tab = this.is_private ? 'private' : 'public';
				//this.content.getElement('.photos-' + tab).getElements('li').each(function (el) {
					if ( photo.getElement('.wrapper').get('not_approved') ) {photo.getElement('.wrapper').get('not_approved').destroy();return;}
					flag = new Element('div', {'class': 'not_approved', html: 'not approved'});
					flag.injectBottom(photo.getElement('.wrapper'));
				//});
			}
			else if ( flag == 'auto_approved' ) {
					if ( photo.getElement('.wrapper').get('auto_approved') ) {photo.getElement('.wrapper').get('auto_approved').destroy();return;}
					flag = new Element('div', {'class': 'auto_approved', html: 'auto approved'});
					flag.injectBottom(photo.getElement('.wrapper'));
			}
			else if ( flag == 'retouch' ) {
				if ( flags.get('retouch') ) {flags.get('retouch').destroy();return;}
				new Element('div', {'class': 'retouch', html: 'retouch'}).injectTop(status);
				if ( photo.getElement('.wrapper').getElement('.not_approved') ) {photo.getElement('.wrapper').getElement('.not_approved').destroy();return;}
			}
			else if ( flag == 'suspicious' ) {
				if ( flags.get('suspicious') ) {flags.get('suspicious').destroy();return;}
				new Element('div', {'class': 'suspicious', html: 'suspicious'}).injectTop(status);
				
			}
		}.bind(this));
	};
	
	this.init_photos = function () {
		/* >>> Get the json data from containers */
		['public', 'private', 'disabled', 'archived'].each(function (container) {
			if ( ! this.content.getElement('.photos-' + container) ) return;
			container = this.content.getElement('.photos-' + container);
			var photos = JSON.decode(container.get('text'));
			container.empty();
			var clearer = new Element('div', {'class': 'clear'}).inject(container);
			
			photos.each(function (photo) {
				this.render_photo(photo).inject(clearer, 'before');
				window.fireEvent('resize');
			}.bind(this));
		}.bind(this));
		/* <<< */
	};
	/* <<< */
	
	this.init_sorting = function (list) {
		var self = this;
		
		this.sortables = new Sortables(list, {
			clone: function (event, element, list) {
				if ( event.event.button ) {this.reset();return new Element('div');}
				
				var pos = element.getPosition(element.getOffsetParent());
				pos.x += 5;
				pos.y += 5;
				
				return element.getElement('img').clone(true).setStyles({
					'margin': '0px',
					'position': 'absolute',
					'z-index': 100,
					'visibility': 'hidden',
					'opacity': .7,
					'width': element.getElement('img').getStyle('width')
				}).inject(this.list).position(pos);
			},
			
			opacity: 1,
			revert: true,
			handle: '.handler',
			
			onStart: function (el) {
				this.drag.addEvent('enter', function () {
					this.started = true;
				}.bind(this));
			},
			
			onComplete: function (el) {
				if ( ! this.started ) return;
				else this.started = false;
				
				self.actions.save_order();
			}
		});
	};
	
	this.init_uploader();
	this.init_mootabs();
	this.init_photos();
	this.init_sorting($$('.photos-public')[0]);
	this.init_sorting($$('.photos-private')[0]);
	
	this.actions = {};
	
	this.actions.make_public_private = function (type) {
		var dest_tab;
		if ( type == 'private' ) {
			dest_tab = 'Public';
		}
		else if ( type == 'public' ) {
			dest_tab = 'Private';
		}
		
		var selection = this.get_selection(type);
		
		this.request_action(dest_tab.toLowerCase(), selection, function (resp) {
			var change_main = false;
			if ( type == 'public' ) {
				selection.els.each(function (el) {
					
					/*if ( el.getElement('.photo').data.is_main ) {*/
					if ( el.getElement('.photo').getElement('div.wrapper').hasClass('main') ) {
						change_main = true;
					}
				}.bind(this));
			}
			
			selection.els.tween({'opacity': [1, 0]}, function () {
				this.mark_photo(selection.els, ( type == 'private' ? 'hard' : 'private' ));
				this.tabs.activate(dest_tab);
				selection.els.each(function (el) {el.getElements('input[type=checkbox]').set('checked', null);});
				selection.els.inject(this.content.getElement('.photos-' + dest_tab.toLowerCase()).getElement('.clear'), 'before');
				selection.els.tween({'opacity': [0, 1]});
				
				if ( change_main ) {
					this.mark_photo(this.content.getElement('.photos-' + type).getFirst('li'), 'main');
				}
				
				window.fireEvent('resize');
			}.bind(this));
		}.bind(this));
	}.bind(this);
	
	// Refactored function for make_public_private.
	this.actions.changeType = function (from, to) {
		var dest_tab = to.charAt(0).toUpperCase() + to.slice(1);
		var selection = this.get_selection(from);
		
		var change_main = false;
		if ( from == 'public' ) {
			selection.els.each(function (el) {
				if ( el.getElement('.photo').getElement('div.wrapper').getParent('td').hasClass('main') ) {
					change_main = true;
				}
			}.bind(this));
		}
		
		this.request_action(to, selection, function (resp) {
			
			selection.els.tween({'opacity': [1, 0]}, function () {
				switch ( to ) {
					case 'public' :
						this.mark_photo(selection.els, 'hard');
						break;
					case 'private' :
						this.mark_photo(selection.els, 'private');
						break;
					case 'disabled' :
						this.mark_photo(selection.els, 'private');
						break;
					case 'archived' :
						this.mark_photo(selection.els, 'archived');
						break;
				}
				this.tabs.activate(dest_tab);
				selection.els.each(function (el) {el.getElements('input[type=checkbox]').set('checked', null);});
				selection.els.inject(this.content.getElement('.photos-' + to).getElement('.clear'), 'before');
				selection.els.tween({'opacity': [0, 1]});
				
				if ( change_main ) {
					this.mark_photo(this.content.getElement('.photos-' + from).getFirst('li'), 'main');
				}
				
				window.fireEvent('resize');
			}.bind(this));
		}.bind(this));
	}.bind(this);
	

	this.actions.select_all = function (tab) {
		$$('#' + tab + ' .photos input[type=checkbox]').each(function(it){
			if ( it.get('checked') ) {
				it.set('checked', '');
			}
			else {
				it.set('checked', 'checked');
			}
		});
		/*this.request_action('verified', selection, function (resp) {
			selection.els.each(function (el) {
				this.mark_photo(el, 'p100');
			}.bind(this));

			selection.els.each(function (el) { el.getElements('input[type=checkbox]').set('checked', null); });
		}.bind(this));*/
	}.bind(this);

	this.actions.select_not_approved = function (tab) {
		$$('#' + tab + ' .photos input[type=checkbox]').each(function(it){
			if( $defined(it.getParent('div.wrapper').getElements('div.not_approved')[0]) ) {
				if ( it.get('checked') ) {
					it.set('checked', '');
				}
				else {
					it.set('checked', 'checked');
				}
			}
		});
		/*this.request_action('verified', selection, function (resp) {
			selection.els.each(function (el) {
				this.mark_photo(el, 'p100');
			}.bind(this));

			selection.els.each(function (el) { el.getElements('input[type=checkbox]').set('checked', null); });
		}.bind(this));*/
	}.bind(this);
	
	this.actions.toggle_verified = function () {
		var selection = this.get_selection();
		
		this.request_action('verified', selection, function (resp) {
			selection.els.each(function (el) {
				this.mark_photo(el, 'p100');
			}.bind(this));
			
			selection.els.each(function (el) {el.getElements('input[type=checkbox]').set('checked', null);});
		}.bind(this));
	}.bind(this);
	
	this.actions.toggle_retouch = function () {
		var selection = this.get_selection();
		
		if (!selection.ids.length){
			var tab = this.active_tab;
			if($$('.photos-'+ tab).getElements('div.retouch')[0].length){
				$$('.photos-'+ tab +' input[type=checkbox]').each(function(it){
					if( $defined(it.getParent('div.wrapper').getElements('div.retouch')[0]) ) {
						it.set('checked', 'checked');
						
					}
				});
				return;
			}
			
		}
				
		this.request_action('retouch', selection, function (resp) {
			selection.els.each(function (el) {
				this.mark_photo(el, 'retouch');
			}.bind(this));
			
			selection.els.each(function (el) {el.getElements('input[type=checkbox]').set('checked', null);});
		}.bind(this));
	}.bind(this);

	this.actions.toggle_approve = function () {
		var selection = this.get_selection();
		
		this.request_action('approve', selection, function (resp) {
			selection.els.each(function (el) {
				if ( el.getElement('.wrapper').getElement('.not_approved') ) {
					el.getElement('.wrapper').getElement('.not_approved').destroy();
				}
				else {
					this.mark_photo(el, 'not_approved');
				}
			}.bind(this));

			selection.els.each(function (el) {el.getElements('input[type=checkbox]').set('checked', null);});
		}.bind(this));
	}.bind(this);

	this.actions.double_approve = function () {
		var selection = this.get_selection();

		this.request_action('double_approve', selection, function (resp) {
			selection.els.each(function (el) {
				if ( el.getElement('.wrapper').getElement('.auto_approved') ) {
					el.getElement('.wrapper').getElement('.auto_approved').destroy();
				}
				
			}.bind(this));

			selection.els.each(function (el) {el.getElements('input[type=checkbox]').set('checked', null);});
			
		}.bind(this));
	}.bind(this);
	
	this.actions.set_main = function () {
		var selection = this.get_selection();

		if ( ! selection.els[0].getElement('.wrapper').getElement('.not_approved') ) {
			this.request_action('set-main', selection, function (resp) {
				this.mark_photo(selection.els[0], 'main'); 

				selection.els.each(function (el) {el.getElements('input[type=checkbox]').set('checked', null);});
			}.bind(this));
		}
		else {
			alert('You can\'t use unapproved photo for main image !');
		}
	}.bind(this);
	
	this.actions.set_rotatable = function () {
		var selection = this.get_selection();
		var tab = this.active_tab;		
		this.content.getElement('.photos-' + tab).getElements('li').each(function (el) {
				el.getElement('td').removeClass('rotatable');
				el.data.is_rotatable = 0;
			
		});
		this.request_action('rotatable', selection, function (resp) {
			selection.els.each(function (el) {
				this.mark_photo(el, 'rotatable');
			}.bind(this));
			
			selection.els.each(function (el) {el.getElements('input[type=checkbox]').set('checked', null);});
		}.bind(this));
	}.bind(this);
	
	this.actions.make_soft_hard = function (type) {
		var selection = this.get_selection();
		this.request_action(type, selection, function (resp) {
			selection.els.each(function (el) {
				this.mark_photo(el, type);
			}.bind(this));
			
			selection.els.each(function (el) {el.getElements('input[type=checkbox]').set('checked', null);});
		}.bind(this));
	}.bind(this);
	
	this.actions.remove = function () {
		var selection = this.get_selection();
		
		this.request_action('remove', selection, function (resp) {
			selection.els.each(function (el) {
				el.getElements('input[type=checkbox]').set('checked', null);
			}.bind(this));
			
			selection.els.tween({'opacity': [1, 0], 'width': 0}, function () {
				this.sortables.removeItems.pass(selection.els, this.sortables).call();
				selection.els.destroy();
			}.bind(this));
		}.bind(this));
	}.bind(this);
	
	this.actions.save_order = function () {
		var selection = {ids: []};
		var tab = this.active_tab;
		
		this.content.getElement('.photos-' + tab).getElements('input[type=checkbox]').each(function (el) {
			selection.ids.include(el.get('value'));
		});
		
		this.request_action('sort', selection, function () {
			
		});
	}.bind(this);
	
	this.actions.set_gotd = function () {
		var selection = this.get_selection();

		if ( ! selection.els[0].getElement('.wrapper').getElement('.not_approved') ) {
			this.request_action('set-gotd', selection, function (resp) {
				this.mark_photo(selection.els[0], 'gotd'); 

				selection.els.each(function (el) {el.getElements('input[type=checkbox]').set('checked', null);});
			}.bind(this));
		}
		else {
			alert('You can\'t use unapproved photo for gotd image !');
		}
	}.bind(this);
	
	this.request_action = function (action, selection, callback) {
		
		if ( ! selection.ids.length ) {
			alert('Please select some photos and then continue');
			return;
		}
		
		if ( action == 'remove' && ! confirm('Are you sure you want delete these photos?') ) {
			return;
		}
		
		this.overlay.disable();
		
		var ids = [];
		for ( i = 0; i < selection.ids.length; i++ ) {
			ids.include('photos[]=' + selection.ids[i]);
		}
		
		new Request({
			url: '/escorts-v2/do-photos?act=' + action + '&' + ids.join('&'),
			method: 'get',
			onSuccess: function (resp) {
				(callback.bind(this))(resp);
				this.overlay.enable();

				if ( action == 'approve' ) {
					this.popup.close();
				}

			}.bind(this)
		}).send();
	};
	
	this.get_selection = function (tab) {
		if ( ! $defined(tab) ) {
			var tab = this.active_tab;
		}
		
		var chks = this.content.getElement('.photos-' + tab);
		
		var result = {ids: [], els: $$()};
		
		chks.getElements('input[type=checkbox]').each(function (chk) {
			if ( chk.get('checked') ) {
				result.ids.include(chk.get('value'));
				result.els.include(chk.getParent('li'));
			}
		});
		
		result.els.tween = function (opt, callback) {
			var opts = {};
			for ( var i = 0; i < result.els.length; i++ ) {
				opts[i] = opt;
			}
			
			new Fx.Elements(result.els, {
				onComplete: callback
			}).start(opts);
		}
		
		return result;
	};
	
	
	// In Public Tab
	if ( $defined($('btn-photos-public-gotd')) ) {
		new Cubix.Button($('btn-photos-public-gotd'), {onClick: function () {
			this.actions.set_gotd('public');
		}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'GOTD'});
	}
	
	new Cubix.Button($('btn-photos-public-make-private'), {onClick: function () {
		this.actions.changeType('public', 'private');
	}.bind(this), icon: 'key', classes: 'float-left ml5 mb5', caption: 'Make Private'});
	
	new Cubix.Button($('btn-photos-public-make-soft'), {onClick: function () {
		this.actions.make_soft_hard('soft');
	}.bind(this), classes: 'float-left ml5 mb5', caption: 'Make Soft'});
		
	new Cubix.Button($('btn-photos-public-make-hard'), {onClick: function () {
		this.actions.make_soft_hard('hard');
	}.bind(this), classes: 'float-left ml5 mb5', caption: 'Make Hard'});
	
	new Cubix.Button($('btn-photos-public-remove'), {onClick: function () {
		this.actions.remove();
	}.bind(this), icon: 'delete', classes: 'float-left ml5 mb5', caption: 'Remove'});
	
	if ( $defined($('btn-photos-public-make-verified')) ) {
		new Cubix.Button($('btn-photos-public-make-verified'), {onClick: function () {
			this.actions.toggle_verified();
		}.bind(this), icon: 'rosette', classes: 'float-left ml5 mb5', caption: 'Tog. Verification'});
	}

	new Cubix.Button($('btn-photos-public-approve'), {onClick: function () {
		this.actions.toggle_approve();
	}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Tog. Approvement'});
	
	new Cubix.Button($('btn-photos-public-set-main'), {onClick: function () {
		this.actions.set_main();
	}.bind(this), icon: 'accept', classes: 'float-left ml5 mb5', caption: 'Set Main'});
	
	new Cubix.Button($('btn-photos-public-set-rotatable'), {onClick: function () {
		this.actions.set_rotatable();
	}.bind(this), icon: 'accept-yellow', classes: 'float-left ml5 mb5', caption: 'Set Rotatable'});
	
	new Cubix.Button($('btn-photos-public-select-all'), {onClick: function () {
		this.actions.select_all('Public');
	}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Select All'});

	if ( $defined($('btn-photos-public-not-approved')) ) {
		new Cubix.Button($('btn-photos-public-not-approved'), {onClick: function () {
			this.actions.select_not_approved('Public');
		}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Not Approved'});
	}

	if ( $defined($('btn-photos-public-double-approve')) ) {
		new Cubix.Button($('btn-photos-public-double-approve'), {onClick: function () {
			this.actions.double_approve('Public');
		}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Approve'});
	}
	
	new Cubix.Button($('btn-photos-public-make-disabled'), {onClick: function () {
		this.actions.changeType('public', 'disabled');
	}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Disable'});

	if ( $defined($('btn-photos-public-make-retouch')) ) {
		new Cubix.Button($('btn-photos-public-make-retouch'), {onClick: function () {
			this.actions.toggle_retouch();
		}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Retouch'});
	}
	
	if ( $('btn-photos-public-move-archive') ) {
		new Cubix.Button($('btn-photos-public-move-archive'), {onClick: function () {
			this.actions.changeType('public', 'archived');
		}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Move to Archive'});
	}

	// In Private Tab
	
	if ( $defined($('btn-photos-private-gotd')) ) {
		new Cubix.Button($('btn-photos-private-gotd'), {onClick: function () {
			this.actions.set_gotd('private');
		}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'GOTD'});
	}
	
	new Cubix.Button($('btn-photos-private-remove'), {onClick: function () {
		this.actions.remove();
	}.bind(this), icon: 'delete', classes: 'float-left ml5 mb5', caption: 'Remove'});
	
	new Cubix.Button($('btn-photos-private-make-public'), {onClick: function () {
		this.actions.changeType('private', 'public');
	}.bind(this), icon: 'group', classes: 'float-left ml5 mb5', caption: 'Make Public'});
	
	if ( $defined($('btn-photos-private-make-verified')) ) {
		new Cubix.Button($('btn-photos-private-make-verified'), {onClick: function () {
			this.actions.toggle_verified();
		}.bind(this), icon: 'rosette', classes: 'float-left ml5 mb5', caption: 'Toggle Verification'});
	}

	new Cubix.Button($('btn-photos-private-approve'), {onClick: function () {
		this.actions.toggle_approve();
	}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Toggle Approvement'});
	new Cubix.Button($('btn-photos-private-select-all'), {onClick: function () {
		this.actions.select_all('Private');
	}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Select All'});

	if ( $defined($('btn-photos-private-not-approved')) ) {
		new Cubix.Button($('btn-photos-private-not-approved'), {onClick: function () {
			this.actions.select_not_approved('Private');
		}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Not Approved'});
	}

	if ( $defined($('btn-photos-private-double-approve')) ) {
		new Cubix.Button($('btn-photos-private-double-approve'), {onClick: function () {
			this.actions.double_approve('Private');
		}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Approve'});
	}
	
	new Cubix.Button($('btn-photos-private-make-disabled'), {onClick: function () {
		this.actions.changeType('private', 'disabled');
	}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Disable'});

	if ( $defined($('btn-photos-private-make-retouch')) ) {
		new Cubix.Button($('btn-photos-private-make-retouch'), {onClick: function () {
			this.actions.toggle_retouch();
		}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Retouch'});
	}
	
	if ( $('btn-photos-private-move-archive') ) {
		new Cubix.Button($('btn-photos-private-move-archive'), {onClick: function () {
			this.actions.changeType('private', 'archived');
		}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Move to Archive'});
	}

	// IN disabled tab
	
	
	new Cubix.Button($('btn-photos-disabled-select-all'), {onClick: function () {
		this.actions.select_all('Disabled');
	}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Select All'});
	new Cubix.Button($('btn-photos-disabled-remove'), {onClick: function () {
		this.actions.remove();
	}.bind(this), icon: 'delete', classes: 'float-left ml5 mb5', caption: 'Remove'});
	new Cubix.Button($('btn-photos-disabled-make-private'), {onClick: function () {
		this.actions.changeType('disabled', 'private');
	}.bind(this), icon: 'key', classes: 'float-left ml5 mb5', caption: 'Make Private'});
	new Cubix.Button($('btn-photos-disabled-make-public'), {onClick: function () {
		this.actions.changeType('disabled', 'public');
	}.bind(this), icon: 'group', classes: 'float-left ml5 mb5', caption: 'Make Public'});

	if ( $('btn-photos-archived-select-all') ) {
		new Cubix.Button($('btn-photos-archived-select-all'), {onClick: function () {
			this.actions.select_all('Archived');
		}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Select All'});
	
		new Cubix.Button($('btn-photos-archived-remove'), {onClick: function () {
			this.actions.remove();
		}.bind(this), icon: 'delete', classes: 'float-left ml5 mb5', caption: 'Remove'});
	
		new Cubix.Button($('btn-photos-archived-make-public'), {onClick: function () {
			this.actions.changeType('archived', 'public');
		}.bind(this), icon: 'group', classes: 'float-left ml5 mb5', caption: 'Make Public'});
	}
	
	window.fireEvent('resize');
};

var Cropper = new Class({
	Implements: [Events],

	el: null,
	els: {},
	bind: {},

	disabled: false,

	max: {x: 0, y: 0},
	mouse: {start: {x: 0, y: 0}, now: {x: 0, y: 0}, diff: {x: 0, y: 0}},

	moved: false,

	reinitLimits: function () {
		this.max = {
			x: this.els.img.getWidth() - this.el.getWidth(),
			y: this.els.img.getHeight() - this.el.getHeight()
		};
		
	},

	initialize: function (el) {
		this.el = $(el);
		this.els.img = this.el.getElement('img');
		this.els.img.ondragstart = function () {return false;};
		
		var initial = el.data.args;
		
		this.els.img.onload = function () {
			this.bind = {
				start: this.handleStart.bindWithEvent(this),
				move: this.handleMove.bindWithEvent(this),
				end: this.handleEnd.bindWithEvent(this)
			};
			
			this.max = {
				x: this.els.img.getWidth() - this.el.getWidth(),
				y: this.els.img.getHeight() - this.el.getHeight()
			};
			
			this.init();

			if ( initial ) {
				this.setInitial(initial.x, initial.y);
			}
		}.bind(this);
		
	},

	init: function () {
		this.els.img.addEvent('mousedown', this.bind.start);
	},

	handleStart: function (e) {
		e.stop();

		if ( this.disabled ) return;
		
		document.addEvent('mousemove', this.bind.move);
		document.addEvent('mouseup', this.bind.end)
		
		this.mouse.start = e.page;

		this.mouse.start.x -= this.mouse.diff.x;
		this.mouse.start.y -= this.mouse.diff.y;
	},

	handleMove: function (e) {
		this.moved = true;
		
		this.mouse.now = e.page;

		var x = this.mouse.now.x - this.mouse.start.x,
			 y = this.mouse.now.y - this.mouse.start.y;
		
		this.mouse.diff = {x: x, y: y};
		
		if ( this.mouse.diff.x > 0 ) this.mouse.diff.x = 0;
		if ( this.mouse.diff.y > 0 ) this.mouse.diff.y = 0;

		if ( -1 * this.mouse.diff.x >= this.max.x ) {
			this.mouse.diff.x = -1 * this.max.x;
		}

		if ( -1 * this.mouse.diff.y >= this.max.y ) {
			this.mouse.diff.y = -1 * this.max.y;
		}
		
		this.update();
	},

	handleEnd: function (e) {
		document.removeEvent('mousemove', this.bind.move);
		document.removeEvent('mouseup', this.bind.end);

		if ( this.moved ) {
			this.fireEvent('complete', [this.mouse.diff]);
		}
	},

	update: function () {
		this.els.img.setStyles({
			left: this.mouse.diff.x,
			top: this.mouse.diff.y
		});
	},

	enable: function () {
		this.els.img.set('opacity', 1);
		this.disabled = false;
	},

	disable: function () {
		this.els.img.set('opacity', 0.5);
		this.disabled = true;
	},

	revert: function () {
		this.mouse.diff = this.initial;
		this.update();
	},

	initial: {},

	setInitial: function (x, y) {
		this.mouse.diff = {x: x, y: y};
		this.initial = {x: x, y: y}

		this.update();
	}
});


var saveAdjustment = function (args) {
	this.disable();

	args = $extend({
		px: args.x / 150.0,
		py: args.y / 205.0
	}, args);

	new Request({
		url: '/escorts-v2/do-photos',
		method: 'get',
		data: $extend({act: 'adjust', photo_id: this.el.data.id}, args),
		onSuccess: function (resp) {
			resp = JSON.decode(resp);

			if ( resp.error ) {
				alert('An error occured');
				this.revert();
			}
			else {
				this.setInitial(args.x, args.y);
			}

			this.enable();
		}.bind(this)
	}).send();
}