Cubix.Popup = new Class({
	Implements: [Events, Options],
	
	el: null,
	
	options: {
		title: 'Untitled Window',
		classPrefix: 'cx-popup-',
		imgUrl: '',
		width: 420,
		offsetTop: 40,
		zIndex: null
	},
	
	zIndex: function () {
		var zIndex = this.options.zIndex;
		
		if ( $type(zIndex) == 'function' ) {
			zIndex = zIndex.call();
		}
		
		if ( ! zIndex ) {
			zIndex = Cubix.Popup.zIndex;
		}
		
		return zIndex;
	},
	
	initialize: function (options) {
		options = options || {};
		this.setOptions(options);

		if ( $defined(this.options.id) ) {
			Cubix.Popup.add(this.options.id, this);
		}

		if ( ! this.options.zIndex ) {
			Cubix.Popup.zIndex += 2;
		}
		
		this.overlay = new Cubix.Overlay($(document.body), { showLoader: false, type: '40p', zIndex: this.zIndex() });
		this.el = this._createElement('').setStyle('z-index', this.zIndex() + 1).inject($(document.body));
		
		this._decorate(this.el);
		
		window.addEvent('resize', function () {
			this.overlay.fit();
		}.bind(this));
	},
	
	_decorate: function (el) {
		this.popup = this._createElement().setStyle('z-index', this.zIndex() + 1).wraps(el);
		this.popup.setStyle('visibility', 'hidden');
		
		this.shadow = new Cubix.Shadow(el, { color: 'black' });
		
		var wrapper = this.wrapper = this._createElement('wrapper').wraps(el);

		this.ajaxOverlay = new Cubix.Overlay.Box(wrapper, {
			type: 'white',
			zIndex: 1,
			offset: [-8, 16, 16, -8],
			styles: { '-moz-border-radius': '6px' }
		});

		var content = this._createElement('content').wraps(el);
		content.popup = this;
		var header = this._createElement('header').inject(content, 'before').setStyle('cursor', 'move');
		
		this.popup.makeDraggable({
			handle: header
		});
		
		var title = new Element('span', {
		}).inject(header);
		
		var btn_close = this._createElement('btn-close').inject(header).addEvent('click', function () {
			this.close();
		}.bind(this));
		
		this.title = title;
		
		this.content = content;
		
		if ( this.options.abar ) {
			var abar = this._createElement('action-bar').inject(wrapper);
			
			this.options.abar.reverse().each(function (el) {
				if ( 'element' == $type(el) ) {
					el.inject(abar);
				}
				else {
					var btn = new Element('span', { html: el }).inject(abar);
					var btn = new Cubix.Button(btn, {
						caption: el.caption,
						icon: el.icon,
						classes: 'fright ml5' + (typeof el.klass !== 'undefined' ? ' ' + el.klass : ''),
						onClick: (el.action ? el.action.bind(this) : $empty())
					});
				}
			}.bind(this));
			
			this._createElement('clearer').inject(abar);
		}
		
		this.diffWidth = this.popup.getWidth() - this.content.getWidth();
		this.diffHeight = this.popup.getHeight() - this.content.getHeight();
		
		this.setTitle(this.options.title);
		this.setSize({ width: this.options.width });
		this.reposition();
		
		this.hide();
		this.popup.setStyle('visibility', null);
	},

	reposition: function () {
		this.setPos({ left: $(document.body).getWidth() / 2 - this.getSize().width / 2, top: window.getScroll().y + this.options.offsetTop });
	},

	_createElement: function (what, type) {
		type = type || 'div';
		return new Element(type, {
			'class': (what ? this.options.classPrefix + what : this.options.classPrefix.substring(0, this.options.classPrefix.length - 1))
		});
	},
	
	setSize: function (size) {
		size = size || {};
		
		if ( size.width ) {
			this.content.setStyle('width', size.width - this.diffWidth);
		}
		
		if ( size.height ) {
			this.content.setStyle('height', size.height - this.diffHeight);
		}

		return this;
	},
	
	getSize: function () {
		return {
			width: this.popup.getWidth(),
			height: this.popup.getHeight()
		};
	},
	
	setPos: function (pos) {
		this.popup.setStyles(pos);
	},
	
	show: function (reload) {
		this.setPos({ top: window.getScroll().y + this.options.offsetTop });
		this.popup.setStyle('display', 'block');
	},
	
	hide: function (destroy) {
		this.popup.setStyle('display', 'none');
	},
	
	open: function () {
		this.overlay.options.zIndex = this.zIndex();
		this.el.setStyle('z-index', this.zIndex() + 1);
		this.popup.setStyle('z-index', this.zIndex() + 1);
		
		this.show();
		this.overlay.disable();
	},
	
	close: function (destroy) {
		this.fireEvent('close');
		
		this.hide();
		this.overlay.enable();
		this.ajaxOverlay.enable();
	},
	
	setTitle: function (title) {
		this.title.set('html', title);
	}
});

Cubix.Popup.zIndex = 0;

Cubix.Popup.Ajax = new Class({
	Extends: Cubix.Popup,
	
	load: function (url, callback) {		
		this.overlay.disable();
		Cubix.Popup.JustLoaded = this;
		new Request({
			url: url,
			method: 'get',
			evalScripts: true,
			onSuccess: function (resp) {
				this.content.set('html', resp);
				if ( callback ) (callback.bind(this)).call();
				this.fireEvent('loaded');
			}.bind(this)
		}).send();
	}
});

Cubix.Popup.Plain = new Class({
	Extends: Cubix.Popup,
	
	load: function (html, callback) {
		this.content.set('html', html);
		if ( callback ) (callback.bind(this)).call();
		else this.fireEvent('loaded');
	}
});


/* Popups container implementation */
Cubix.Popup._popups = new Hash();
Cubix.Popup.get = function(id) {
	return Cubix.Popup._popups.get(id);
};
Cubix.Popup.add = function (id, instance) {
	Cubix.Popup._popups.set(id, instance);
};







Cubix.Popup.Actions = {};

Cubix.Popup.Actions.Save = function () {
	this.content.getElement('form').set('send', {
		onSuccess: function () {
			this.close();
			if ( Cubix.Grid.Instance ) {
				Cubix.Grid.Instance.load();
			}
		}.bind(this)
	}).send();
};

Cubix.Popup.Actions.Close = function () {
	this.close();
};

Cubix.Popup.Actions.SaveValid = function (callback) {
	var e = { _stop: false, stop: function () { this._stop = true; } };
	this.fireEvent('beforesubmit', e);

	if ( e._stop ) return;
	this.ajaxOverlay.disable();
	this.content.getElement('form').set('send', {
		onSuccess: function (resp) {
			var result = JSON.decode(resp);
			
			if ( ! result ) {
				alert('An error occured, result: "' + resp + '"');
				return;
			}
			
			if ( result.status == 'success' ) {
				this.close();
				if ( Cubix.Grid.Instance ) {
					Cubix.Grid.Instance.load();
				}
				
				if ( $defined(callback) && $defined(callback.bind) ) {
					(callback.bind(this))(result);
				}
				return;
			}
			
			this.content.getElements('.error').removeClass('error').each(function (el) { if ( el.getElement('strong') ) el.getElement('strong').destroy() });
			$$('.mootabs_title li.red').removeClass('red');
			
			for ( var field in result.msgs ) {
				eval('var msg = result.msgs.' + field);
				var inner = this.content.getElements("*[name<>'']").filter(function (el) { return el.get('name') == field; })[0];
				
				if ( inner.get('tag') != 'div' ) {
					inner = inner.getParent('.inner');
				}
				
				inner.addClass('error');
				
				if ( msg.length ) {
					new Element('strong', { html: msg }).inject(inner, 'top');
				}
				
				var panel = inner.getParent('.mootabs_panel');
				
				if ( panel && (! $$('.mootabs_title li[title=' + panel.get('id') + '] img').length ) ) {
					$$('.mootabs_title li[title=' + panel.get('id') + ']').addClass('red');
					// new Element('img', { src: '/img/famfamfam/cancel.png', width: 16, height: 16 })
					// 						.inject($$('.mootabs_title li[title=' + panel.get('id') + ']')[0], 'top');
				}
			}

			this.ajaxOverlay.enable();

			window.fireEvent('resize');
		}.bind(this)
	}).send();
}
