Cubix.Tip = new Class({
	Implements: [Events, Options],

	options: {
		elements: [],
		classPrefix: 'cx-tip',
		plugins: [],
		behaviour: 'element'
	},

	plugins: {},

	initialize: function (options) {
		this.setOptions(options || {});

		this.initPlugins();

		this.render();
		this.hide();
		
		this.bind = {
			enter: {},
			leave: {},
			move: {}
		};

		this.options.elements.each(function (el) {
			this.attach(el);
		}.bind(this));
	},

	attach: function (el) {
		/*this.el.removeEvent('mouseenter', this.bind.enter[this.el]);*/
		this.bind.enter[el] = this.mouseenter.bindWithEvent(this, [el]);
		el.addEvent('mouseenter', this.bind.enter[el]);
	},

	mousemove: function (e, el) {
		if ( this.options.behaviour == 'element' ) return;
		
		var styles = {
			left: e.page.x + 5
		};

		if ( this.options.behaviour == 'mouse' ) {
			styles.top = e.page.y + 5;
		}

		this.fireEvent('move', [styles]);

		this.tip.setStyles(styles);
	},

	mouseenter: function (e, el) {
		if ( ! this.el ) {
			this.el = el;
		}
		
		this.el.removeEvents({ mousemove: this.bind.move[this.el], mouseleave: this.bind.leave[this.el] });
		this.el = el;
		
		var coords = el.getCoordinates(document.body);

		this.show({
			left: (this.options.behaviour == 'mouse' ? e.page.x + 5 : coords.left),
			top: (this.options.behaviour == 'mouse' ? e.page.y + 5 : coords.bottom)
		});

		this.bind.move[el] = this.mousemove.bindWithEvent(this, [el]);
		el.addEvent('mousemove', this.bind.move[el]);
		this.bind.leave[el] = this.mouseleave.bindWithEvent(this, [el]);
		el.addEvent('mouseleave', this.bind.leave[el]);
	},

	mouseleave: function (e, el) {
		
		if ( e.target == this.tip || e.target.getParent('.cx-tip') ) { e.stop(); return; }
		
		this.el.removeEvent('mouseleave', this.bind.leave[this.el]);
		this.hide();
	},

	setContent: function (html) {
		this.content.set('html', html);
		this.fireEvent('content');
	},

	show: function (position) {
		this.hide();

		var e = { stopped: false, stop: function () { this.stopped = true; } };
		this.fireEvent('show', [e, position]);
		this.tip.setStyles(position);
		if ( e.stopped ) return;

		this.tip.setStyle('visibility', 'visible');
		this.tip.setStyle('display', 'block');
	},

	hide: function () {
		var e = { stopped: false, stop: function () { this.stopped = true; } };
		this.fireEvent('hide', [e]);
		if ( e.stopped ) return;
		
		this.tip.setStyle('visibility', 'hidden');
		this.tip.setStyle('display', 'none');
	},

	initPlugins: function () {
		this.options.plugins.each(function (plugin) {
			var instance, name;
			
			if ( $type(plugin) == 'string' ) {
				instance = new Cubix.Tip.Plugin[plugin](this);
				name = plugin;
			}
			else if ( $type(plugin) == 'object' ) {
				instance = new Cubix.Tip.Plugin[plugin.name](this, plugin.options);
				name = plugin.name;
			}

			this.plugins[name] = instance;
		}.bind(this));
	},

	render: function () {
		var tip, wrapper, content;

		this.tip = tip = new Element('div', {
			'class': this.getClass()
		}).adopt(
			this.wrapper = wrapper = new Element('div', {
				'class': this.getClass('wrapper')
			}).adopt(
				this.content = content = new Element('div', {
					'class': this.getClass('content')
				})
			)
		).inject($(document.body));

		this.hide();

		this.fireEvent('render');
	},

	getClass: function (cls) {
		var options = this.options,
			prefix = options.classPrefix;

		if ( ! $defined(cls) ) return prefix;

		if ( $type(cls) == 'array' ) {
			var classes = [];
			for ( var i = 0; i < cls.length; i++ ) { classes.include(this.getClass(cls[i])); }
			return classes.join(' ');
		}
		else if ( $type(cls) == 'string' ) {
			if ( ! cls.length ) return prefix;
			
			return prefix + '-' + cls;
		}
		else {
			return null;
		}
	}
});

Cubix.Tip.Plugin = new Class({
	Implements: [Options],

	options: {

	},

	tip: null,

	initialize: function (tip, options) {
		this.tip = tip;
		this.setOptions(options || {});
		this.attach();
	},

	attach: function () {

	}
});

Cubix.Tip.Plugin.TitleBar = new Class({
	Extends: Cubix.Tip.Plugin,

	attach: function () {
		this.tip.addEvents({
			render: function () {
				var title;
				this.title = title = new Element('div', {
					'class': this.getClass('titlebar')
				}).inject(this.wrapper, 'top');
			}
		});

		this.tip = $extend(this.tip, {
			setTitle: function (title) {
				this.title.set('html', title);
			}
		});
	}
});

Cubix.Tip.Plugin.Shadow = new Class({
	Extends: Cubix.Tip.Plugin,

	attach: function () {
		var options = this.options;
		this.tip.addEvents({
			render: function () {
				this.shadow = new Cubix.Shadow(this.wrapper, options);
			}
		});

		/*this.tip = $extend(this.tip, {
			setTitle: function (title) {
				this.title.set('html', title);
			}
		});*/
	}
});

Cubix.Tip.Plugin.Fade = new Class({
	Extends: Cubix.Tip.Plugin,

	attach: function () {
		var align = this.align.bind(this.tip);
		this.tip.addEvents({
			render: function () {
				this.tween = new Fx.Tween(this.tip, { link: 'cancel', duration: 100, property: 'opacity' });
			},
			content: function () {
				var co = this.tip.getCoordinates();
				position = {
					left: co.left,
					top: co.top
				};
				align(position);
				this.tip.setStyles(position);
			},
			show: function (e, position) {
				align(position);
				
				if ( this.tween ) {
					e.stop();

					this.tween.start(1);
				}
				this.tip.setStyle('display', 'block');
			},
			hide: function (e) {
				e.stop();
				
				if ( this.tween ) {
					this.tween.start(0);
				}
				else {
					this.tip.setStyle('opacity', 0)
				}
				this.tip.setStyle('display', 'none');
			},
			move: function (position) {
				align(position);
			}
		});
	},

	align: function (position) {
		var tipHeight = this.tip.getHeight(),
		height = $(document.body).getHeight(),
		scroll = window.getScroll();

		if ( scroll.y + height < position.top + tipHeight ) {
			position.top = scroll.y + height - tipHeight;
		}
	}
});

Cubix.Tip.Plugin.AjaxContent = new Class({
	Extends: Cubix.Tip.Plugin,

	cache: {},

	attach: function () {
		var options = this.options;

		var request = this.request = new Request({
			url: options.url,
			link: 'cancel',
			method: 'get'
		});

		var self = this;
		this.tip.addEvent('show', function () {
			this.setContent('<img src="/img/ajax-loader-s.gif" width="16" height="16" alt="indicator" />');

			var params = options.params.call(this), key = '';

			for ( var k in params ) {
				key += k + '_' + params[k] + ';';
			}

			if ( $defined(self.cache[key]) ) {
				this.setContent(self.cache[key]);
				return;
			}

			request.removeEvents('success').addEvent('success', self.success.bindWithEvent(self, key));
			request.options.data = params;
			request.send();
		});
	},

	success: function (resp, key) {
		this.cache[key] = resp;
		this.tip.setContent(resp);
	}
});
