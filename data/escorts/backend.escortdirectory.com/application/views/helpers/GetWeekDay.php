<?php

class Zend_View_Helper_GetWeekDay
{
	public function getWeekDay($day_index)
	{
		if ( $day_index == 0 ) $day_index = 7;
		
		$week_days = array( 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday', 7 => 'Sunday');
		
		if ( isset($week_days[$day_index]) ) {
			return $week_days[$day_index];
		}
		
		return false;
	}
}

