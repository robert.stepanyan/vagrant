<?php

class AuthController extends Zend_Controller_Action
{
	/**
	 * @var Zend_Auth
	 */
	protected $_auth;
	
	public function init()
	{
		$this->_auth = Zend_Auth::getInstance();
	}
	
	public function indexAction()
	{
		header('Cubix-redirect: /auth');
		$this->_forward('login');
	}

	public function loginAction()
	{
		$host = preg_replace('/^(backend|backoffice)./', '', $_SERVER['HTTP_HOST']);
		
		$this->view->application = Sceon_Application::getByHost($host);
		
		if ( $this->_auth->hasIdentity() ) {
			$this->_redirect($this->view->baseUrl() . '/escorts');
		}
		elseif ( $this->_request->isPost() ) {
			$username = strtolower(trim($this->_request->username));
			$password = $this->_request->password;

			if ( strlen($username) !== 0 && strlen($password) !== 0 ) {
				$adapter = new Model_Auth_Adapter($username, $password, $this->view->application->id);
				$result = $this->_auth->authenticate($adapter);
                
				if( ! $result->isValid() ) {
					/*switch ( $result->getCode() ) {
					case Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID:
						$this->view->errors = 'user credentials not found';
					}*/
					$this->view->error = true;
				} else {
					//Model_Activity::getInstance()->log('signin');
                    $sales_id = $result->getIdentity()->getId();
                    //Model_BOUsersSessions::setStart($sales_id);
					
					if ($result->getIdentity()->type != 'journal manager')
						$this->_redirect($this->view->baseUrl() . '/dashboard');
					else
						$this->_redirect($this->view->baseUrl() . '/journal');
				}
			}
			else {
				$this->view->error = true;
			}
		}
	}

	public function logoutAction()
	{
		if ( $this->_auth->hasIdentity() ) {
			//Model_Activity::getInstance()->log('signout');
			$this->_auth->clearIdentity();
		}
		
		$this->_redirect($this->view->baseUrl() . '/auth');
	}
}
