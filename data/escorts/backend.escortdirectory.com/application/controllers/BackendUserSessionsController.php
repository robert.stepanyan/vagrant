<?php

class BackendUserSessionsController extends Zend_Controller_Action
{
	public function init()
	{
		$this->model = new Model_BOUsers();
	}
	
	public function indexAction() 
	{
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('default_backend-users-sessions_data');
		$this->view->filter_params = $ses_filter->filter_params;
		/* FILTER PARAMS */
	}
	
	public function dataAction()
	{
		
		$req = $this->_request;
        $model = new Model_BOUsersSessions();
        $search_time_interval = $req->search_time_interval;
        
        $filter = array(
			'sales_id = ?' => $req->sales_user_id,
			// Date
			'filter_by' => $req->filter_by,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to,

		);

        if ( $req->total ){
             $data = $model->getTotal($filter);
             $count = count($data);
        }else{
            $data = $model->getAll(
                $this->_request->page,
                $this->_request->per_page,
                $filter,
                $this->_request->sort_field,
                $this->_request->sort_dir,
                $count
            );
        }

				
		
       
//        var_dump($data,$newData);
//        exit;
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

    public function viewAction(){
        $this->view->event_id = (int)$this->_request->id;

    }

    public function viewDataAction(){
        $id = (int)$this->_request->id;
        $total = (int)$this->_request->total;

        $journaModel = new Model_Journal();
        $sessionModel = new Model_BOUsersSessions();

        if ( $id ){
            $this->view->event_id = $id;
            $res = $sessionModel->get($id);

            $filter = array(
                "date <= ?" => $res->last_refresh_time,
                "date >= ?" => $res->start_time,
                "bu.sales_user_id" => $res->sales_id,
                "event_type" => true
            );
        }  elseif ( $total ) {
             $filter = array(
                "date <= ?" => date('Y-m-d H:i:s',$this->_request->last_refresh_time),
                "date >= ?" => date('Y-m-d H:i:s',$this->_request->start_time),
                "bu.sales_user_id" => $this->_request->sales_id,
                "event_type" => true
            );
        }

        
        $data = $journaModel->getAll(
			$this->_request->page,
			$this->_request->per_page,
			$filter,
			$this->_request->sort_field,
			$this->_request->sort_dir,
			$count
		);

        foreach ( $data as $i => $item ) {

			if ( strlen($item->data) ) {
				//$data[$i]->data = 'ddd';


				$arr = json_decode($item->data);

				$str = '';

                foreach ( $arr as $key => $value ) {
                    $str .= '<span>' . $key . ': </span> <b>' . $value . '</b><br/>';
                }
				//var_dump($str);
				$data[$i]->data = $str;
			}
		}


        $dat = array(
			'data' => $data,
			'count' => $count
		);
       
        echo json_encode($dat);
        exit;


        
    }

	
}
