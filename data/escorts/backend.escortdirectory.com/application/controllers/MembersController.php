<?php

class MembersController extends Zend_Controller_Action 
{
	protected $model;
	protected $defines;
	
	public function init()
	{
		$this->model = new Model_Members();
		$this->view->defines = $this->defines = Zend_Registry::get('defines');
	}
	
	public function indexAction()
	{
		$ses_filter = new Zend_Session_Namespace('default_members_data');
		$this->view->filter_params = $ses_filter->filter_params;
	}
	
	public function dataAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		
		$request = $this->_request;
		
		$filter = array(
			'u.id'			=> $request->user_id,
			'u.username'	=> $request->username,
			'u.email'		=> $request->email,
			'u.status'		=> $request->status
		);
		
		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		foreach( $data as $i => $row ) {
			$data[$i]->status = $this->defines['user_status_options'][$data[$i]->status];
		}
		
		echo json_encode(array(
			'data' => $data,
			'count' => $count
		));
	}
	
	public function editAction()
	{
		$this->view->layout()->disableLayout();
		$this->view->member = $member = $this->model->getById($this->_request->id);
		
		
		$country_model = new Model_Geography_Countries();
		$city_model = new Model_Geography_Cities();
			
		$this->view->countries = $country_model->ajaxGetAll(false);
		$this->view->cities = $city_model->ajaxGetAll(null, $member->country_id);
		
		
		if( $this->_request->isPost() )
		{
			$data = new Cubix_Form_Data($this->_request);

			$params = array(
				'id' => 'int',
				'user_id' => 'int',
				'status' => '',
				'username' => '',
				'password' => '',
				'country_id' => 'int',
				'city_id' => 'int',
				'email' => '',
				'about_me' => ''
			);

			$data->setFields($params);
			$data = $data->getData();

			$validator = new Cubix_Validator();
			
			
			if ( ! $data['status'] ) {
				$validator->setError('status', 'Required');
			}
			
			if ( ! strlen($data['username']) ) {
				$validator->setError('username', 'Required');
			}
			elseif ( Model_Users::existsByUsername($data['username'], $data['user_id']) ) {
				$validator->setError('username', 'Username Already exists');
			}
			elseif ( strlen($data['username']) < 2 ) {
				$validator->setError('username', 'Username must be at least 6 characters long');
			}
									
			if ( strlen($data['email']) && Model_Users::existsByEmail($data['email'], $data['user_id']) ) {
				$validator->setError('email', 'Already exists');
			}

			if ( $validator->isValid() ) {
				$this->model->update(new Model_MemberItem($data));
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function createAction()
	{
		$this->view->layout()->disableLayout();
		
		$country_model = new Model_Geography_Countries();
		$this->view->countries = $country_model->ajaxGetAll(false);
		
		if( $this->_request->isPost() )
		{
			$data = new Cubix_Form_Data($this->_request);

			$params = array(
				'id' => 'int',
				'user_id' => 'int',
				'status' => '',
				'username' => '',
				'password' => '',
				'country_id' => 'int',
				'city_id' => 'int',
				'email' => '',
				'about_me' => ''
			);

			$data->setFields($params);
			$data = $data->getData();

			$validator = new Cubix_Validator();
			
			
			if ( ! $data['status'] ) {
				$validator->setError('status', 'Required');
			}
			
			if ( ! strlen($data['username']) ) {
				$validator->setError('username', 'Required');
			}
			
			if ( ! strlen($data['password']) ) {
				$validator->setError('password', 'Required');
			}
			
			elseif ( Model_Users::existsByUsername($data['username'], $data['user_id']) ) {
				$validator->setError('username', 'Username Already exists');
			}
			elseif ( strlen($data['username']) < 2 ) {
				$validator->setError('username', 'Username must be at least 6 characters long');
			}
									
			if ( strlen($data['email']) && Model_Users::existsByEmail($data['email'], $data['user_id']) ) {
				$validator->setError('email', 'Already exists');
			}

			if ( $validator->isValid() ) {
				$this->model->create(new Model_MemberItem($data));
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function removeAction()
	{
		$this->view->layout()->disableLayout();
		
		$ids = $this->_request->id;
		if ( ! is_array($ids) ) {
			$ids = array($ids);
		}
		
		foreach($ids as $id) {
			$this->model->remove($id);
		}
		
		die;
	}
}

?>
