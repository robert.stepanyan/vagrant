<?php

class EscortsController extends Zend_Controller_Action
{
	/**
	 * @var Model_Escorts
	 */
	protected $model;
	
	/**
	 * @var Model_UserItem
	 */
	protected $user;

	public function init()
	{
		$this->model = new Model_Escorts();
		$this->user = Zend_Auth::getInstance()->getIdentity();

		$this->view->DEFINITIONS = Zend_Registry::get('defines');
		$this->view->measure_units = METRIC_SYSTEM;
	}
	
	public function indexAction()
	{
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('default_escorts_data');
		$this->view->filter_params = $ses_filter->filter_params;
		/* FILTER PARAMS */
	}

	public function dataAction()
	{
		$req = $this->_request;

		$bu_user = Zend_Auth::getInstance()->getIdentity();

		if ( $bu_user->type == 'sales manager' ) {
			$req->setParam('sales_user_id', $bu_user->id);
		}
		else if ($bu_user->type == 'admin') {
			$req->setParam('application_id', $bu_user->application_id);
		}

		$filter = array(
			// Personal
			'e.id = ?' => $req->escort_id,
			'u.application_id = ?' => $req->application_id,

			'e.showname' => $req->showname,
			'u.username' => $req->username,
			'ep.contact_email' => $req->email,
			'ep.contact_phone' => $req->phone,
			'ep.contact_web' => $req->web,
			'u.last_ip' => $req->last_ip,
			
			'u.sales_user_id = ?' => $req->sales_user_id,
			
			'e.gender = ?' => $req->gender,
			
			'e.country_id = ?' => $req->country,
			'c.region_id = ?' => $req->region,
			'ec.city_id = ?' => $req->city,
			'ecz.city_zone_id = ?' => $req->cityzone,
		
			// Status
			'e.status' => $req->status,
			
			'u.disabled = ?' => $req->disabled,
			'ep.admin_verified = ?' => $req->verified,
		
			'e.verified_status = ?' => $req->only_verified ? Model_Escorts::STATUS_VERIFIED : null,

			
			'only_city_tour' => $req->only_city_tour,
		
			// Date
			'filter_by' => $req->filter_by,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to,

			'excl_status' => Model_Escorts::ESCORT_STATUS_DELETED
		);

		// Independent and Agency girl filtering
		$escort_type_filter = $this->_getParam('escort_type');
		if ( $escort_type_filter == 'independent' ) {
			$filter[] = 'e.agency_id IS NULL';
		}
		elseif ( $escort_type_filter == 'agency' ) {
			$filter[] = 'e.agency_id IS NOT NULL';
		}

		// Revisions filter fix
		if ( $this->_getParam('sort_field') == 'revs_count' ) {
			$this->_setParam('sort_field', 'COUNT(DISTINCT(pu.revision))');
		}

		$model = new Model_Escorts();
		$data = $model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		$DEFINITIONS = Zend_Registry::get('defines');
		
		foreach ($data as $d)
		{
			$stat = array();
			foreach( $DEFINITIONS['escort_status_options'] as $key => $e_status )
			{
				if ( $this->model->hasStatusBit($d['id'], $key) )
				{
					$stat[] = $e_status;
				}
			}

			$d['status'] = implode(', ', $stat);//$DEFINITIONS['escort_status_options'][$d['status']];

			// $d['main_photo'] = $d->getMainPhoto()->getUrl('agency_p100');
		}
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

	protected function _validateLogin($edit_mode, $validator)
	{
		$escort = new stdClass();

		$fields = array(
			'showname' => '',
			'username' => '',
			'password' => '',
			'sales_user_id' => 'int-nz',
			'agency_id' => 'int-nz',
			'slogan' => '',
			'reg_email' => ''
		);

		$form = new Cubix_Form_Data($this->_request);

		$form->setFields($fields);
		$data = $form->getData();

		$data['application_id'] = intval($this->_getParam('application_id'));

		if ( $edit_mode ) {

		}

		if ( ! strlen($data['showname']) ) {
			$validator->setError('showname', 'Required');
		}
		elseif ( mb_strlen($data['showname']) > 25  ) {
			$validator->setError('showname', 'Showname must not be <br/> longer then 25 chars.');
		}
		elseif ( ! preg_match('/^[-_a-z0-9\s]+$/i', $data['showname']) ) {
			$validator->setError('showname', 'Allowed only: -, _, a-z, 0-9');
		}

		if ( ! $data['agency_id'] ) {

			if ( ! strlen($data['username']) ) {
				$validator->setError('username', 'Required');
			}
			else if ( ! $validator->isValidUsername($data['username']) ) {
				$validator->setError('username', 'Username is invalid <br/> [a-z0-9_]{3,15}');
			}
			else if ( Model_Users::existsByUsername($data['username']) ) {
				if ( $escort->agency_id ) {
					$validator->setError('username', 'Already exists');
				}
				else if ( $escort->username != $data['username'] ) {
					$validator->setError('username', 'Already exists');
				}
			}

			if ( ! $edit_mode && ! strlen($data['password']) ) {
				$validator->setError('password', 'Required');
			}
		}

		if ( ! $data['slogan'] ) {
			$data['slogan'] = null;
		}
		else if ( mb_strlen($data['slogan'],'UTF-8') > 20 ) {
			$validator->setError('slogan', 'Slogan text must be not <br/> longer then 20 chars.');
		}

		if ( ! $data['sales_user_id'] && ! $data['agency_id'] ) {
			$validator->setError('sales_user_id', 'Required');
		}

		if ( ! $data['agency_id'] ) {
			if ( ! strlen($data['reg_email']) ) {
				$validator->setError('reg_email', 'Required');
			}
			elseif ( ! $validator->isValidEmail($data['reg_email']) ) {
				$validator->setError('reg_email', 'Invalid Email');
			}
		}

		return $data;
	}

	protected function _validateContacts($edit_mode = false, $validator)
	{
		$form = new Cubix_Form_Data($this->_request);

		$form->setFields(array(
			'phone_number' => '',
			'phone_prefix'=>'',
			'phone_instr' => 'int-nz',
			'phone_instr_no_withheld' => 'int-nz',
			'phone_instr_other' => 'string',
			'email' => 'string',
			'website' => 'string',
			'street' => 'string',
			'street_no' => 'string'
		));

		$data = $form->getData();

		if ( $edit_mode ) {
			$escort_id = intval($this->_getParam('id'));
		}

		if ( strlen($data['website']) && strtolower(substr($data['website'], 0, 7)) != 'http://' && strtolower(substr($data['website'], 0, 8)) != 'https://' && strtolower(substr($data['website'], 0, 4)) != 'www.' ) {
			$validator->setError('website', 'Website must start with <br/> http://, https:// or www.');
		}

		if ( strlen($data['email']) && ! $validator->isValidEmail($data['email']) ) {
			$validator->setError('email', 'Invalid Email');
		}

		$data['contact_phone_parsed'] = null;
		$data['phone_country_id'] = null;

		if ( $data['phone_number'] ) {
			if ( ! preg_match("/^[0-9\s\+\-\(\)]+$/i", $data['phone_number']) ) {
				$validator->setError('email_phone_error', 'Invalid phone number');
			}
			elseif(preg_match("/^(\+|00)/", $data['phone_number']) ) {
				$validator->setError('email_phone_error', 'Please enter phone number without country calling code');
			}
			if ( ! ($data['phone_prefix']) ) {
				$validator->setError('email_phone_error', 'Country calling code Required');
			}
			list($country_id, $phone_prefix,$ndd_prefix) = explode('-',$data['phone_prefix']);


			$data['contact_phone_parsed'] = preg_replace('/[^0-9]/', '', $data['phone_number']);
			$data['contact_phone_parsed'] = preg_replace('/^' . $ndd_prefix . '/', '', $data['contact_phone_parsed']);
			$data['contact_phone_parsed'] = '00' . $phone_prefix . $data['contact_phone_parsed'];

			$data['phone_country_id'] = $country_id;
		}

		unset($data['phone_prefix']);

		return $data;
	}

	protected function _validateBiography($edit_mode = false, $validator)
	{
		$form = new Cubix_Form_Data($this->_request);

		$fields = array(
			'gender' => 'int',
			'birth_date' => 'int-nz',
			'ethnicity' => 'int-nz',
			'nationality_id' => 'int-nz',
			'characteristics' => 'notags|special',

			'height' => '',
			'weight' => '',
			'bust' => 'int-nz',
			'waist' => 'int-nz',
			'hip' => 'int-nz',
			'cup_size' => '',
			'hair_color' => 'int-nz',
			'eye_color' => 'int-nz',
			'shoe_size' => 'int-nz',
			'dress_size' => '',
			'pubic_hair' => 'int-nz',
			'is_smoking' => 'int-nz',
			'is_drinking' => 'int-nz',

			'langs' => ''
		);

		$form->setFields($fields);
		$data = $form->getData();

		if ( ! $data['gender'] ) {
			$validator->setError('gender', 'Required');
		}


		if ( strlen($data['birth_date']) ) {
			$data['birth_date'] = strtotime('-' . $data['birth_date'] . ' year');
		}

		$data['measure_units'] = METRIC_SYSTEM;

		$data['height'] = intval($data['height']);
		if ( ! $data['height'] ) {
			$data['height'] = null;
		}


		$data['weight'] = intval($data['weight']);
		if ( ! $data['weight'] ) {
			$data['weight'] = null;
		}

		$i18n_data = Sceon_I18n::getValues($this->_request->getParams(), 'about', '');
		$security = new Cubix_Security();
		foreach ( $i18n_data as $field => $value ) {
			$value = $security->clean_html($value);
			$i18n_data[$field] = $security->xss_clean($value);
		}
		$data = array_merge($data, $i18n_data);

		$langs = array();
		if ( ! is_array($data['langs']) || ! count($data['langs']) ) {
			$validator->setError('langs', 'At least one is required');
		}
		else {
			foreach ( $data['langs'] as $i => $lang ) {
				$arr = explode(':', $lang);
				$langs[$i] = array('lang_id' => $arr[0], 'level' => $arr[1]);
			}
			unset($data['langs']);
			$data['langs'] = $langs;
		}

		return $data;
	}

	protected function _validateWorking($edit_mode = false, $validator)
	{
		$form = new Cubix_Form_Data($this->_request);

		$form->setFields(array(
			'country_id' => 'int-nz',
			'base_city_id' => 'int-nz',
			'locations' => 'arr-int',

			'zip' => 'string',
			'incall' => 'int',
			'incall_type' => 'int-nz',
			'incall_hotel_room' => 'int-nz',
			'incall_other' => 'notags|special',
			'outcall' => 'int',
			'outcall_type' => 'int-nz',
			'outcall_other' => 'notags|special'
		));

		$data = $form->getData();

		$app = Sceon_Application::getById($this->_request->application_id);

		if ( count($data['locations']) > $app->max_working_cities ) {
			$validator->setError('locations', 'You can add maximum ' . $app->max_working_cities . ' working locations');
		}

		if ( ! $data['locations'] ) {
			$validator->setError('locations', 'At least one is required');
		}
		elseif ( ! $data['base_city_id'] ) {
			$validator->setError('locations', 'Please select the base city below');
		}

		$cities = array();
		if ( $data['locations'] ) {
			foreach($data['locations'] as $i => $city) {
				if ( $data['base_city_id'] != $city ) {
					$cities[$i] = array('city_id' => $city);
				}
			}
		}
		$data['cities'] = $cities;
		unset($data['locations']);

		$data['city_id'] = $data['base_city_id'];
		unset($data['base_city_id']);

		if ( $data['incall_type'] == 2 && ! $data['incall_hotel_room'] ) {
			$validator->setError('err_incall', 'Please select hotel room');
		}

		if ( ! $data['incall'] ) {
			$data['incall_type'] = null;
			$data['incall_hotel_room'] = null;
			$data['incall_other'] = null;
			$data['zip'] = null;
		}
		else{
			if ( !$data['incall_type'] ) {
				$validator->setError('err_incall', 'Please select Incall suboption');
			}
		}
		unset($data['incall']);

		if ( ! $data['outcall'] ) {
			$data['outcall_type'] = null;
			$data['outcall_other'] = null;
		}
		unset($data['outcall']);

		return $data;
	}

	protected function _validateWorkingTimes($edit_mode = false, $validator)
	{
		$form = new Cubix_Form_Data($this->_request);

		$form->setFields(array(
			'available_24_7' => 'int-nz',
			'night_escort' => 'int',
			'day_index' => 'arr-int',
			'time_from' => 'arr-int',
			'time_from_m' => 'arr-int',
			'time_to' => 'arr-int',
			'time_to_m' => 'arr-int'
		));
		$data = $form->getData();

		if ( ! $data['night_escort'] ) {
			$data['night_escort'] = null;
		}

		$_data = array('available_24_7' => $data['available_24_7'],'night_escort' => $data['night_escort'], 'times' => array());

		for ( $i = 1; $i <= 7; $i++ ) {
			if ( isset($data['day_index'][$i]) && isset($data['time_from'][$i]) && isset($data['time_from_m']) && isset($data['time_to'][$i]) && isset($data['time_to_m'][$i]) ) {
				$_data['times'][] = array(
					'day_index' => $i,
					'time_from' => $data['time_from'][$i],
					'time_from_m' => $data['time_from_m'][$i],
					'time_to' => $data['time_to'][$i],
					'time_to_m' => $data['time_to_m'][$i]
				);
			}
		}

		return $_data;
	}

	protected function _validateServices($edit_mode = false, $validator)
	{
		$form = new Cubix_Form_Data($this->_request);

		$form->setFields(array(
			'sex_orientation' => 'int-nz',
			'sex_availability' => '',
			'services' => 'arr-int',
			'service_prices' => 'arr',
			'service_currencies' => 'arr'
		));

		$data = $form->getData();

		$DEFINITIONS = Zend_Registry::get('defines');

		if ( is_array($data['services']) ) {
			foreach ( $data['services'] as $i => $svc ) {
				$data['services'][$i] = array('service_id' => $svc);
				if ( $data['service_prices'][$svc] ) {
					$price = floatval($data['service_prices'][$svc]);
					$currency = (isset($data['service_currencies'][$svc])) ? $data['service_currencies'][$svc] : $this->view->currency('id');
					if ( $price && $price > 0 ) {
						$data['services'][$i] = array_merge($data['services'][$i], array('price' => $price, 'currency_id' => $currency));
					}
				}
			}
		}
		unset($data['service_prices']);
		unset($data['service_currencies']);

		if ( is_array($data['sex_availability']) ) {
			foreach ( $data['sex_availability'] as $i => $opt ) {
				if ( ! isset($DEFINITIONS['sex_availability_options'][$opt]) ) {
					unset($data['sex_availability'][$i]);
				}
			}
			$data['sex_availability'] = count($data['sex_availability']) ?
				implode(',', $data['sex_availability']) : null;
		}
		else {
			$validator->setError('sex_availability', 'Required');
		}

		$i18n_data = Sceon_I18n::getValues($this->_request->getParams(), 'additional_service', '');
		foreach ( $i18n_data as $field => $value ) {
			$i18n_data[$field] = htmlspecialchars(strip_tags(trim($value)));
		}
		$data = array_merge($data, $i18n_data);

		return $data;
	}

	protected function _validatePrices($edit_mode = false, $validator)
	{
		$rates = $this->_getParam('rates');

		if ( ! is_array($rates) ) {
			$rates = array();
		}

		$data = array('rates' => array());

		$DEFINITIONS = Zend_Registry::get('defines');

		$currencies = array_keys(Sceon_Currencies::getAllAssoc());

		$units = array_keys($DEFINITIONS['time_unit_options']);

		foreach ( $rates as $availability => $_rates ) {
			if ( $availability == 'incall' ) { $availability = 1; $types = array('additional-hour', 'overnight', 'dinner-date', 'weekend'); }
			elseif ( $availability == 'outcall' ) { $availability = 2; $types = array('additional-hour', 'overnight', 'dinner-date', 'weekend'); }
			elseif ( $availability == 'travel' ) { $availability = 0; $types = array('weekend'); }
			else continue;

			foreach ( $_rates as $rate ) {
				if ( get_magic_quotes_gpc() ) $rate = stripslashes($rate);
				$rate = @json_decode($rate);

				if ( is_object($rate) ) $rate = (array) $rate;
				if ( $rate === false || ! is_array($rate) ) {
					continue;
				}

				// Case when rate is standart and some data are not valid
				if ( ! isset($rate['type']) && ( ! isset($rate['time']) || ! isset($rate['unit']) || ! isset($rate['price']) || ! isset($rate['currency']) ) ) {
					continue;
				}
				// Case when rate is a type of custom and some data are not valid
				elseif ( isset($rate['type']) && ( ! isset($rate['price']) || ! isset($rate['currency']) || ! in_array($rate['type'], $types) ) ) {
					continue;
				}

				// If price is invalid
				$price = intval($rate['price']);
				if ( $price <= 0 ) continue;

				// If currency is invalid
				$currency = intval($rate['currency']);
				if ( ! in_array($currency, $currencies) ) {
					continue;
				}
				// If plus taxi exced
				/*$taxi = intval($rate['taxi']);
				if($taxi !=1 ){
					$taxi = null;
				}*/

				// If rate is custom validate data add only type, price and currency fields
				if ( ! isset($rate['type']) ) {
					$time = intval($rate['time']);
					if ( $time <= 0 ) { continue; }

					$unit = intval($rate['unit']);
					if ( ! in_array($unit, $units) ) { continue; };


					$data['rates'][] = array('availability' => $availability, 'time' => $time, 'time_unit' => $unit, 'price' => $price, 'currency_id' => $currency/*, 'plus_taxi'=> $taxi*/);

				}
				// Otherwize add also time and time unit
				else {
					$data['rates'][] = array('availability' => $availability, 'type' => $rate['type'], 'price' => $price, 'currency_id' => $currency/*, 'plus_taxi'=> $taxi*/);
				}
			}
		}

		$special_rates = htmlspecialchars(strip_tags(trim($this->_getParam('special_rates'))));
		$data['special_rates'] = $special_rates;

		/*$travel_pay_methods = $this->_getParam('travel_payment_method');
		$travel_other_method = $this->_getParam('travel_other_method');

		$data['travel_payment_methods'][] = $travel_pay_methods;
		$data['travel_other_method'][] = $travel_other_method;*/

		return $data;
	}

	protected function __validate($edit_mode, $validator)
	{
		$data = array();

		$login_data = $this->_validateLogin($edit_mode, $validator);
		$contact_data = $this->_validateContacts($edit_mode, $validator);
		$biography_data = $this->_validateBiography($edit_mode, $validator);
		$working_data = $this->_validateWorking($edit_mode, $validator);
		$working_times_data = $this->_validateWorkingTimes($edit_mode, $validator);
		$services_data = $this->_validateServices($edit_mode, $validator);
		$prices_data = $this->_validatePrices($edit_mode, $validator);

		$data = array_merge(
			$data,
			$login_data,
			$contact_data,
			$biography_data,
			$working_data,
			$working_times_data,
			$services_data,
			$prices_data
		);

		return $data;
	}

	public function createAction()
	{
		$edit_mode = false;
		$this->view->layout()->disableLayout();

		$this->view->countries = Model_Geography_Countries::getPhoneCountries();

		if ( $this->_request->isPost() ) {

			$validator = new Cubix_Validator();

			$data = $this->__validate($edit_mode, $validator);

			//var_dump($data);

			if ( $validator->isValid() ) {

			}

			$this->_helper->viewRenderer->setNoRender();
			echo json_encode($validator->getStatus());
			return;
		}
	}

	public function editAction()
	{
		$edit_mode = true;
		$this->view->layout()->disableLayout();
	}
}
