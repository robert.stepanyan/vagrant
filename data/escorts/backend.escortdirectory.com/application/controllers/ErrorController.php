<?php

class ErrorController extends Zend_Controller_Action
{
	public function errorAction()
	{
		$content = '';
		$errors = $this->_getParam ('error_handler') ;
		
		switch ($errors->type) {
			case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER :
			case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION :
				$this->_response->setRawHeader('HTTP/1.1 404 Not Found');

				$content .= "<h1>404 Page not found!</h1>" . PHP_EOL;
				$content .= "<p>The page you requested was not found.</p>";

				break ;
			default :
				$content .= "<h1>Error!</h1>" . PHP_EOL;
				$content .= "<p>An unexpected error occurred with your request. Please try again later.</p>";

				$exception = $errors->exception;
				
				$content .= "<pre class=\"debug-exception\">{$exception->__toString()}</pre>";

				$log = new Zend_Log(
					new Zend_Log_Writer_Stream(APPLICATION_PATH . '/logs/backend-exceptions.log')
				);
				$log->debug(
					$exception->getMessage() . PHP_EOL . $exception->getTraceAsString()
				);
				break ;
		}

		$this->getResponse()->clearBody();
		$this->view->content = $content;
	}

	public function deniedAction()
	{
		$this->view->layout()->setLayout('error');
		if ( isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) ) {
			$this->view->layout()->disableLayout();
		}
	}

	public function permissionDeniedAction()
	{
		$error = $this->_getParam('error');
		if ( ! strlen($error) ) $error = 'You don\'t have enough permissions to access this function';
		// echo '<script type="text/javascript">Cubix.Popup.JustLoaded.setSize({ width: 300 }).reposition();</script>';
		die('<p style="margin: 0; padding: 20px; text-align: center; font-size: 18px; font-weight: bold; color: #d00">' . $error . '</p>');
	}
}
