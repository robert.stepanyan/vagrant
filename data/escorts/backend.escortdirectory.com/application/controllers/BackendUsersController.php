<?php

class BackendUsersController extends Zend_Controller_Action
{
	public function init()
	{
		$this->model = new Model_BOUsers();
	}
	
	public function indexAction() 
	{
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('default_backend-users_data');
		$this->view->filter_params = $ses_filter->filter_params;
		/* FILTER PARAMS */
	}
	
	public function dataAction()
	{
		
		$req = $this->_request;
		
		$filter = array(
			'bu.username' => $req->username,
			'bu.id' => $req->id,
//			'u.application_id' => $req->application_id,
			/*'u.email' => $req->email,
			'u.last_ip' => $req->last_ip,*/
			'bu.type' => $req->type,
			'bu.application_id' => $req->application_id
		);
		
		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		/*foreach ( $data as $i => $item ) {
			if ( $data[$i]->status == Model_Issues::STATUS_TICKET_CLOSED )
				$data[$i]->status = 'closed';
			else
				$data[$i]->status = 'opened';

			$data[$i]->message = mb_substr($data[$i]->message, 0, 50, "UTF-8") . " ...";
			$data[$i]->subject = mb_substr($data[$i]->subject, 0, 20, "UTF-8") . " ...";
		}*/
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

	public function toggleActiveAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->model->toggle($id);
	}

	public function createAction()
	{
		$req = $this->_request;

		if ( $req->isPost() ) {

			$validator = new Cubix_Validator();

			if ( ! $req->username ) {
				$validator->setError('username', 'Required');
			}
			else if ( $this->model->userNameExists($req->username) ) {
				$validator->setError('username', 'Username already exists!');
			}

			if ( ! $req->password ) {
				$validator->setError('password', 'Required');
			}
			
			if ( ! $req->first_name ) {
				$validator->setError('first_name', 'Required');
			}

			if ( ! $req->type ) {
				$validator->setError('type', 'Required');
			}

			if ( $validator->isValid() )
			{
				$bu_user = Zend_Auth::getInstance()->getIdentity();
				
				$data = array(
					'first_name' => $req->first_name,
					'last_name' => $req->last_name,
					'username' => $req->username,
					'password' => md5($req->password),
					'email' => $req->email,
					'skype' => $req->skype,
					'type' => $req->type,
					'application_id' => $req->application_id,
					'is_disabled' => 0
				);
				
				if ( (Sceon_Application::getId() == APP_A6 || Sceon_Application::getId() == APP_A6_AT) && $bu_user->type == 'superadmin' ) {
					$data = array_merge($data, array('is_auto_approve_photos' => $req->is_auto_approve_photos));
					$data = array_merge($data, array('receive_emails' => $req->receive_emails));
				}

				$this->model->insert($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}

	public function editAction()
	{
		$req = $this->_request;

		$user_id = $req->id;
		$user = $this->model->get($user_id);
		$this->view->user = $user;

		if ( $req->isPost() ) {
			$validator = new Cubix_Validator();

			if ( ! $req->type ) {
				$validator->setError('type', 'Required');
			}
			
			if ( ! $req->first_name ) {
				$validator->setError('first_name', 'Required');
			}
			
			/*if ( ! $req->is_auto_approve_photos ) {
				$req->is_auto_approve_photos = 1;
			}*/

			if ( $validator->isValid() )
			{
				$bu_user = Zend_Auth::getInstance()->getIdentity();
				
				$data = array(
					'first_name' => $req->first_name,
					'last_name' => $req->last_name,
					'email' => $req->email,
					'skype' => $req->skype,
					'type' => $req->type,
					'application_id' => $req->application_id,
					'is_disabled' => 0
				);

				if ( (Sceon_Application::getId() == APP_A6 || Sceon_Application::getId() == APP_A6_AT) && $bu_user->type == 'superadmin' ) {
					$data = array_merge($data, array('is_auto_approve_photos' => $req->is_auto_approve_photos));
					$data = array_merge($data, array('receive_emails' => $req->receive_emails));
				}
				
				if ( strlen($req->password) ) {
					$data = array_merge($data, array('password' => md5($req->password)));
				}

				$this->model->update($user_id, $data);
			}

			die(json_encode($validator->getStatus()));
		}
	}
	
	public function removeAction()
	{
		/*$id = intval($this->_request->id);

		$this->model->remove($id);*/
		die;
	}
}
