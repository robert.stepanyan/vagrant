<?php

class IndexController extends Zend_Controller_Action {
	/**
	 * The default action - show the home page
	 */
	public function indexAction()
	{
		
	}
	
	public function goAction()
	{
		$link = $_SERVER['QUERY_STRING'];
		if ( ! preg_match('#^http://#', $link) ) $link = 'http://' . $link;
		
		header('Location: ' . $link);
		die;
	}
}