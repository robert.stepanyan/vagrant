<?php

class TestController extends Zend_Controller_Action {

	public function indexAction()
	{
		$req = $this->_request;

		if ( $req->isPost() )
		{
			try {
				$images = new Sceon_ImagesCommon('file');
				foreach ( $_FILES as $i => $file ) {
					if ( strlen($file['tmp_name']) ) {
						$photo = $images->save($file);
						if ( $photo && count($photos) < 3  ) {
							$photos[$photo['image_id']] = $photo;
						}
					}
				}
			} catch ( Exception $e ) {
				$errors['photos'][] = $e->getMessage();
			}

			$this->view->photos = $photos;
		}
	}
}