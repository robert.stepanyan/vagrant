<?php

class AgenciesController extends Zend_Controller_Action
{
	protected $model;
	protected $defines;
	
	public function init()
	{
		$this->model = new Model_Agencies();
		$this->view->defines = $this->defines = Zend_Registry::get('defines');
	}
	
	public function indexAction()
	{
		
	}
	
	public function dataAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		
		$request = $this->_request;
		
		$filter = array(
			'u.username'		=> $request->username,
			'u.email'			=> $request->email
		);
		
		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		foreach ( $data as $i => $item ) {
			$data[$i]->status = $this->defines['user_status_options'][$item->status];
		}
		
		die(json_encode(array(
			'data' => $data,
			'count' => $count
		)));
	}
	
	public function createAction()
	{
		$this->view->layout()->disableLayout();
		
		$moderators = new Model_Moderators();
		$countries = new Model_Geography_Countries();
		$utils = new Cubix_Utils();
		
		$this->view->sales_persons = $moderators->getByApplicationId($this->_request->application_id);
		$this->view->phone_countries = $countries->getPhoneCountries();
		$this->view->countries = $countries->ajaxGetAll();
		
		if ( $this->_request->isPost() )
		{
			$data = new Cubix_Form_Data($this->_request);
						
			$data->setFields(array(
				'agency_name'=> '',
				'status' => '',
				'username' => '',
				'u_email' => '',
				'password' => '',
				'sales_person_id' => 'int',
				'country_id' => 'int',
				'city_id' => 'int',
				'work_days' => 'arr-int',
				'work_times_from' => 'arr-int',
				'work_times_to' => 'arr-int', 
				'available_24_7' => 'int',
				'email' => '',
				'web' => '',
				'phone_prefix' => '',
				'phone' => '',
				'phone_instructions' => '',
				'address' => '',
				'latitude' => 'double',
				'longitude' => 'double',
				'is_anonymous'	=> 'int'
			));
			
			$data = $data->getData();
			
			$validator = new Cubix_Validator();
			
			
			// <editor-fold defaultstate="collapsed" desc="Validation">
			if ( ! $data['available_24_7'] ) {
				$data['available_24_7'] = null;
			}
			
			if ( ! $data['sales_person_id'] ) {
				$validator->setError('sales_person_id', 'Required');
			}
			
			if ( ! $data['status'] ) {
				$validator->setError('status', 'Required');
			}
			
			if ( ! strlen($data['username']) ) {
				$validator->setError('username', 'Required');
			}
			elseif ( Model_Users::existsByUsername($data['username']) ) {
				$validator->setError('username', 'Already exists');
			}
			
			if ( ! strlen($data['u_email']) ) {
				$validator->setError('u_email', 'Required');
			}
			elseif ( ! $validator->isValidEmail($data['u_email']) ) {
				$validator->setError('u_email', 'Invalid Email');
			}
			
			if ( ! strlen($data['password']) ) {
				$validator->setError('password', 'Required');
			}
			
			if ( ! strlen($data['agency_name']) ) {
				$validator->setError('agency_name', 'Required');
			}
			elseif ( ! preg_match('/^[-\.\s_a-z0-9]+$/i', $data['agency_name']) ) {
				$validator->setError('agency_name', 'Allowed only: -, _, a-z, 0-9');
			}
			
			if ( ! $validator->isValidEmail($data['email']) ) {
				$validator->setError('email', 'Invalid Email');
			}
			
			if ( ! $data['country_id'] ) {
				$validator->setError('country_id', 'Country Required');
			}
			if ( ! $data['city_id'] ) {
				$validator->setError('city_id', 'City Required');
			}
			
			for ( $i = 1; $i <= 7; $i++ ) {
				if ( isset($data['work_days'][$i]) && ( ! @$data['work_times_from'][$i] || ! @$data['work_times_to'][$i] ) ) {
					$validator->setError('work_times_' . $i, '');
				}
			}
			
			
			$data['contact_phone_parsed'] = null;
			if ( $data['phone'] ) {
				if ( ! preg_match("/^[0-9\s\+\-\(\)]+$/i", $data['phone']) ) {
					$validator->setError('phone', 'Invalid phone number');
				}
				elseif(preg_match("/^(\+|00)/", $data['phone']) ) {
					$validator->setError('phone', 'Please enter phone number without country calling code');
				}
				
				if ( ! ($data['phone_prefix']) ) {
					$validator->setError('phone_prefix', 'Country calling code Required');
				}
				
				list($country_id, $phone_prefix, $ndd_prefix) = explode('-',$data['phone_prefix']);
				$data['contact_phone_parsed'] = $data['phone'] = preg_replace('/[^0-9]/', '', $data['phone']);
				$data['contact_phone_parsed'] = preg_replace('/^'.$ndd_prefix.'/', '', $data['contact_phone_parsed']);
				$data['contact_phone_parsed'] = '00'.$phone_prefix.$data['contact_phone_parsed'];

				$data['phone_country_id'] = intval($country_id);
			}
			// </editor-fold>
			
			$slug = $utils->makeSlug($data['agency_name']);
			
			
			$i18n_data = Sceon_I18n::getValues($this->_request->getParams(), 'about', '');
			$security = new Cubix_Security();
			foreach ( $i18n_data as $field => $value ) {
				$value = strip_tags($value);
				$i18n_data[$field] = $security->xss_clean($value);
			}
			
			$save_data = array(
				'sales_user_id'    		=> $data['sales_person_id'],
				'status' 	   			=> $data['status'],
				'name'       			=> $data['agency_name'],
				'slug'       			=> $slug,
				'country_id' 			=> $data['country_id'],
				'city_id'    			=> $data['city_id'],
				'email'      			=> $data['email'],
				'u_email'      			=> $data['u_email'],
				'web'		 			=> $data['web'],
				'phone_country_id'		=> $data['phone_country_id'],
				'phone'		 			=> $data['phone'],
				'contact_phone_parsed'	=> $data['contact_phone_parsed'],
				'phone_instructions' 	=> $data['phone_instructions'],
				'username' 				=> $data['username'],
				'password' 				=> $data['password'],
				'work_days'				=> $data['work_days'],
				'work_times_from'		=> $data['work_times_from'],
				'work_times_to'			=> $data['work_times_to'],
				'available_24_7'		=> $data['available_24_7'],
				'address'				=> $data['address'],
				'latitude'				=> $data['latitude'],
				'longitude'				=> $data['longitude'],
				'is_anonymous'			=> $data['is_anonymous'],
				'about'					=> $i18n_data
			);
			
			
			if ( $validator->isValid() ) {
				$this->model->create(new Model_AgencyItem($save_data));
			}
			
			$this->_helper->viewRenderer->setNoRender();
			echo json_encode(array_merge($validator->getStatus()));
			
		}
	}
	
	public function editAction()
	{
		$this->view->layout()->disableLayout();
		
		$moderators = new Model_Moderators();
		$countries = new Model_Geography_Countries();
		$utils = new Cubix_Utils();
		$cities = new Model_Geography_Cities();
		
		$agency = $this->model->getById($this->_request->id);
		$this->view->agency_data = $agency['agency_data'];
		$this->view->working_hours = $agency['working_hours'];
		
		$this->view->sales_persons = $moderators->getByApplicationId($this->_request->application_id);
		$this->view->phone_countries = $countries->getPhoneCountries();
		$this->view->countries = $countries->ajaxGetAll();
		$this->view->cities = $cities->ajaxGetAll(null, $agency['agency_data']->country_id);
		
		if ( $this->_request->isPost() )
		{
			$data = new Cubix_Form_Data($this->_request);
						
			$data->setFields(array(
				'id'	=> 'int',
				'agency_name'=> '',
				'user_id'	=> 'int',
				'status' => '',
				'username' => '',
				'u_email' => '',
				'password' => '',
				'sales_person_id' => 'int',
				'country_id' => 'int',
				'city_id' => 'int',
				'work_days' => 'arr-int',
				'work_times_from' => 'arr-double',
				'work_times_to' => 'arr-double', 
				'available_24_7' => 'int',
				'email' => '',
				'web' => '',
				'phone_prefix' => '',
				'phone' => '',
				'phone_instructions' => '',
				'address' => '',
				'latitude' => 'double',
				'longitude' => 'double',
				'is_anonymous'	=> 'int'
				
			));
			
			$data = $data->getData();
			$validator = new Cubix_Validator();
			
			
			// <editor-fold defaultstate="collapsed" desc="Validation">
			if ( ! $data['available_24_7'] ) {
				$data['available_24_7'] = null;
			}
			
			if ( ! $data['sales_person_id'] ) {
				$validator->setError('sales_person_id', 'Required');
			}
			
			if ( ! $data['status'] ) {
				$validator->setError('status', 'Required');
			}
			
			if ( ! strlen($data['username']) ) {
				$validator->setError('username', 'Required');
			}
			elseif ( Model_Users::existsByUsername($data['username'], $agency['agency_data']->user_id) ) {
				$validator->setError('username', 'Already exists');
			}
			
			if ( ! strlen($data['u_email']) ) {
				$validator->setError('u_email', 'Required');
			}
			elseif ( ! $validator->isValidEmail($data['u_email']) ) {
				$validator->setError('u_email', 'Invalid Email');
			}
			
			if ( ! strlen($data['agency_name']) ) {
				$validator->setError('agency_name', 'Required');
			}
			elseif ( ! preg_match('/^[-\.\s_a-z0-9]+$/i', $data['agency_name']) ) {
				$validator->setError('agency_name', 'Allowed only: -, _, a-z, 0-9');
			}
			
			if ( ! $validator->isValidEmail($data['email']) ) {
				$validator->setError('email', 'Invalid Email');
			}
			
			if ( ! $data['country_id'] ) {
				$validator->setError('country_id', 'Country Required');
			}
			if ( ! $data['city_id'] ) {
				$validator->setError('city_id', 'City Required');
			}
			
			for ( $i = 1; $i <= 7; $i++ ) {
				if ( isset($data['work_days'][$i]) && ( ! @$data['work_times_from'][$i] || ! @$data['work_times_to'][$i] ) ) {
					$validator->setError('work_times_' . $i, '');
				}
			}
			
			
			$data['contact_phone_parsed'] = null;
			if ( $data['phone'] ) {
				if ( ! preg_match("/^[0-9\s\+\-\(\)]+$/i", $data['phone']) ) {
					$validator->setError('phone', 'Invalid phone number');
				}
				elseif(preg_match("/^(\+|00)/", $data['phone']) ) {
					$validator->setError('phone', 'Please enter phone number without country calling code');
				}
				
				if ( ! ($data['phone_prefix']) ) {
					$validator->setError('phone_prefix', 'Country calling code Required');
				}
				
				list($country_id, $phone_prefix, $ndd_prefix) = explode('-',$data['phone_prefix']);
				$data['contact_phone_parsed'] = $data['phone'] = preg_replace('/[^0-9]/', '', $data['phone']);
				$data['contact_phone_parsed'] = preg_replace('/^'.$ndd_prefix.'/', '', $data['contact_phone_parsed']);
				$data['contact_phone_parsed'] = '00'.$phone_prefix.$data['contact_phone_parsed'];

				$data['phone_country_id'] = intval($country_id);
			}
			// </editor-fold>
			
			$slug = $utils->makeSlug($data['agency_name']);
			
			
			$i18n_data = Sceon_I18n::getValues($this->_request->getParams(), 'about', '');
			$security = new Cubix_Security();
			foreach ( $i18n_data as $field => $value ) {
				$value = strip_tags($value);
				$i18n_data[$field] = $security->xss_clean($value);
			}
			
			$save_data = array(
				'id'					=> $data['id'],
				'user_id'				=> $data['user_id'],
				'sales_user_id'    		=> $data['sales_person_id'],
				'status' 	   			=> $data['status'],
				'name'       			=> $data['agency_name'],
				'slug'       			=> $slug,
				'country_id' 			=> $data['country_id'],
				'city_id'    			=> $data['city_id'],
				'email'      			=> $data['email'],
				'u_email'      			=> $data['u_email'],
				'web'		 			=> $data['web'],
				'phone_country_id'		=> $data['phone_country_id'],
				'phone'		 			=> $data['phone'],
				'contact_phone_parsed'	=> $data['contact_phone_parsed'],
				'phone_instructions' 	=> $data['phone_instructions'],
				'username' 				=> $data['username'],
				'password' 				=> $data['password'],
				'work_days'				=> $data['work_days'],
				'work_times_from'		=> $data['work_times_from'],
				'work_times_to'			=> $data['work_times_to'],
				'available_24_7'		=> $data['available_24_7'],
				'address'				=> $data['address'],
				'latitude'				=> $data['latitude'],
				'longitude'				=> $data['longitude'],
				'is_anonymous'			=> $data['is_anonymous'],
				'about'					=> $i18n_data
			);
			if ( $validator->isValid() ) {
				$this->model->update(new Model_AgencyItem($save_data));
			}
			
			$this->_helper->viewRenderer->setNoRender();
			echo json_encode(array_merge($validator->getStatus()));
			
		}
	}
	
	public function removeAction()
	{
		$this->view->layout()->disableLayout();
		
		$ids = $this->_request->id;
		if ( ! is_array($ids) ) {
			$ids = array($ids);
		}
		
		foreach($ids as $id) {
			$this->model->remove($id);
		}
		
		die;
	}
}

?>
