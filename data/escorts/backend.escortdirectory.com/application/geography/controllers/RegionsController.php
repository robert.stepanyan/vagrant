<?php

class Geography_RegionsController extends Zend_Controller_Action
{
	public function init()
	{
		$this->model = new Model_Geography_Regions();
	}
	
	public function indexAction()
	{
		
	}
	
	public function dataAction()
	{
		$country_id = intval($this->_getParam('country_id'));
		$title = trim($this->_getParam('title'));
		$page = $this->_getParam('page');
		$per_page = $this->_getParam('per_page');
		$sort_field = $this->_getParam('sort_field');
		$sort_dir = $this->_getParam('sort_dir');
		$count = 0;
		
		$data = $this->model->getAll($country_id, $page, $per_page, $sort_field, $sort_dir, $count, $title);
		echo json_encode(array('data' => $data, 'count' => $count));
		die;
	}
	
	public function ajaxAction()
	{
		$country_id = intval($this->_getParam('country_id'));
		
		echo json_encode(array(
			'data' => $this->model->ajaxGetAll($country_id)
		));
		die;
	}
	
	public function createAction()
	{
		$this->view->country_id = $country_id = intval($this->_getParam('country_id'));
		
		if ( $this->_request->isPost() ) {
			$titles = Cubix_I18n::getValues($this->_request->getParams(), 'title', '');
			$state = $this->_getParam('state');
			
			$validator = new Cubix_Validator();
			
			if ( ! strlen($titles['title_en']) ) {
				$validator->setError('title_en', 'English title is required');
			}
			
			if ( ! $country_id ) {
				$validator->setError('country_id', 'Required');
			}
			
			if ( $validator->isValid() ) {
				$this->model->save(new Model_Geography_RegionItem(array_merge(
					array(
						'state' => $state,
						'country_id' => $country_id
					),
					$titles
				)));
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function editAction()
	{
		$this->view->country_id = $country_id = intval($this->_getParam('country_id'));
		
		$id = intval($this->_getParam('id'));
		
		if ( $this->_request->isPost() ) {
			$titles = Cubix_I18n::getValues($this->_request->getParams(), 'title', '');
			$state = $this->_getParam('state');
			
			$validator = new Cubix_Validator();
			
			if ( ! strlen($titles['title_en']) ) {
				$validator->setError('title_en', 'English title is required');
			}
			
			if ( ! $country_id ) {
				$validator->setError('country_id', 'Required');
			}
			
			if ( $validator->isValid() ) {
				$this->model->save(new Model_Geography_RegionItem(array_merge(
					array(
						'id' => $id,
						'state' => $state,
						'country_id' => $country_id
					),
					$titles
				)));
			}
			
			die(json_encode($validator->getStatus()));
		}
		else {
			$this->view->region = $this->model->get($id);
		}
	}
	
	public function removeAction()
	{
		$id = intval($this->_getParam('id'));
		
		$this->model->remove($id);
	}

	public function toggleFrenchAction()
	{
		$id = intval($this->_getParam('id'));
		$db = Zend_Registry::get('db');
		$db->update('regions', array('is_french' => new Zend_Db_Expr('NOT is_french')), array($db->quoteInto('id = ?', $id)));
	}
}
