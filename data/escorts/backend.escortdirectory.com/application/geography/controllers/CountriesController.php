<?php

class Geography_CountriesController extends Zend_Controller_Action
{
	public function init()
	{
		$this->model = new Model_Geography_Countries();
	}
	
	public function indexAction()
	{
		
	}
	
	public function dataAction()
	{
		$page = $this->_getParam('page');
		$per_page = $this->_getParam('per_page');
		$sort_field = $this->_getParam('sort_field');
		$sort_dir = $this->_getParam('sort_dir');
		$count = 0;
		
		$data = $this->model->getAll($page, $per_page, $sort_field, $sort_dir, $count);
		echo json_encode(array('data' => $data, 'count' => $count));
		die;
	}
	
	public function ajaxAction()
	{
		$data = $this->model->ajaxGetAll($this->_getParam('has_regions', FALSE));
		
		foreach ( $data as $i => $row ) {
			if ( $row->getOrdering() == 0 ) {
				break;
			}
		}
		
		$data = array_merge(
			array(array(
				'title' => 'Important Countries'
			)),
			array_slice($data, 0, $i),
			array(array(
				'title' => '------------------'
			)),
			array_slice($data, $i, count($data) - $i)
		);
		
		echo json_encode(array(
			'data' => $data
		));
		die;
	}
	
	public function createAction()
	{
		if ( $this->_request->isPost() ) {
			$iso = $this->_getParam('iso');
			$has_regions = intval($this->_getParam('has_regions'));
			$titles = Cubix_I18n::getValues($this->_request->getParams(), 'title', '');
			
			$validator = new Cubix_Validator();
			
			if ( ! strlen($titles['title_en']) ) {
				$validator->setError('title_en', 'English title is required');
			}
			
			if ( ! strlen($iso) ) {
				$validator->setError('iso', 'Required');
			}
			
			if ( $validator->isValid() ) {
				$this->model->save(new Model_Geography_CountryItem(array_merge(
					array(
						'iso' => $iso,
						'has_regions' => $has_regions
					),
					$titles
				)));
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function editAction()
	{
		$id = intval($this->_getParam('id'));
		
		if ( $this->_request->isPost() ) {
			$iso = $this->_getParam('iso');
			$has_regions = intval($this->_getParam('has_regions'));
			$titles = Cubix_I18n::getValues($this->_request->getParams(), 'title', '');
			
			$validator = new Cubix_Validator();
			
			if ( ! strlen($titles['title_en']) ) {
				$validator->setError('title_en', 'English title is required');
			}
			
			if ( ! strlen($iso) ) {
				$validator->setError('iso', 'Required');
			}
			
			if ( $validator->isValid() ) {
				$this->model->save(new Model_Geography_CountryItem(array_merge(
					array(
						'id' => $id,
						'iso' => $iso,
						'has_regions' => $has_regions
					),
					$titles
				)));
			}
			
			die(json_encode($validator->getStatus()));
		}
		else {
			$this->view->country = $this->model->get($id);
		}
	}
	
	public function removeAction()
	{
		$id = intval($this->_getParam('id'));
		
		$this->model->remove($id);
	}
}

