<?php

class Geography_CityzonesController extends Zend_Controller_Action
{
	public function init()
	{
		$this->model = new Model_Geography_Cityzones();
	}
	
	public function indexAction()
	{
		
	}
	
	public function dataAction()
	{
		$country_id = intval($this->_getParam('country_id'));
		$region_id = intval($this->_getParam('region_id'));
		$city_id = intval($this->_getParam('city_id'));
		
		$title = trim($this->_getParam('title'));
		
		$page = $this->_getParam('page');
		$per_page = $this->_getParam('per_page');
		$sort_field = $this->_getParam('sort_field');
		$sort_dir = $this->_getParam('sort_dir');
		$count = 0;
		
		$data = $this->model->getAll($city_id, $page, $per_page, $sort_field, $sort_dir, $count, $title);
		echo json_encode(array('data' => $data, 'count' => $count));
		die;
	}
	
	public function ajaxAction()
	{
		$city_id = intval($this->_getParam('city_id'));
		
		echo json_encode(array(
			'data' => $this->model->ajaxGetAll($city_id)
		));
		die;
	}
	
	public function createAction()
	{
		$this->view->country_id = $country_id = $this->_getParam('country_id');
		$this->view->region_id = $region_id = $this->_getParam('region_id');
		$this->view->city_id = $city_id = $this->_getParam('city_id');
		
		if ( $this->_request->isPost() ) {
			$city_id = intval($this->_getParam('city_id'));
			$titles = Cubix_I18n::getValues($this->_request->getParams(), 'title', '');
			
			$validator = new Cubix_Validator();
			
			if ( ! $city_id ) {
				$validator->setError('city_id', 'Please select city');
			}
			
			if ( ! strlen($titles['title_en']) ) {
				$validator->setError('title_en', 'English title is required');
			}
			
			if ( $validator->isValid() ) {
				$this->model->save(new Model_Geography_CityzoneItem(array_merge(
					array(
						'city_id' => $city_id
					),
					$titles
				)));
			}
			
			echo json_encode($validator->getStatus());
			die;
		}
	}
	
	public function editAction()
	{
		$id = intval($this->_getParam('id'));
		
		if ( $this->_request->isPost() ) {
			$city_id = intval($this->_getParam('city_id'));
			$titles = Cubix_I18n::getValues($this->_request->getParams(), 'title', '');
			
			$validator = new Cubix_Validator();
			
			if ( ! $city_id ) {
				$validator->setError('city_id', 'Please select city');
			}
			
			if ( ! strlen($titles['title_en']) ) {
				$validator->setError('title_en', 'English title is required');
			}
			
			if ( $validator->isValid() ) {
				$this->model->save(new Model_Geography_CityzoneItem(array_merge(
					array(
						'id' => $id,
						'city_id' => $city_id
					),
					$titles
				)));
			}
			
			echo json_encode($validator->getStatus());
			die;
		}
		else {
			$this->view->cityzone = $this->model->get($id);
		}
	}
	
	public function removeAction()
	{
		$id = intval($this->_getParam('id'));
		
		$this->model->remove($id);
	}
}

