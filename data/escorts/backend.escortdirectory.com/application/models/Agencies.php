<?php

class Model_Agencies extends Cubix_Model
{
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		if ( $sort_field == 'id' ) {
			$sort_field = 'u.id';
		}
		
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				a.id, a.name, a.email,
				u.id AS user_id, u.username, u.email, u.status
			FROM agencies a
			INNER JOIN users u ON u.id = a.user_id
			WHERE 1
		';
		
		$where = '';
		
		
		if ( strlen($filter['u.username']) ) {
			$where .= self::quote('AND u.username LIKE ?', $filter['u.username'] . '%');
		}
		
		if ( strlen($filter['u.email']) ) {
			$where .= self::quote('AND u.email = ?', $filter['u.email']);
		}
		
		if ( strlen($filter['u.status']) ) {
			$where .= self::quote('AND u.status = ?', $filter['u.status']);
		}
		
		$sql .= $where;
		
		$sql .= '
			GROUP BY u.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$data = self::db()->fetchAll($sql);
		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');
		return $data;
	}
	
	public function getById($id)
	{
		$sql = '
			SELECT 
				a.id,
				a.name, 
				c.id AS city_id, 
				a.email, a.phone, 
				u.status, u.username, 
				a.user_id, a.web,
				a.phone_country_id,
				a.phone_instructions, 
				a.contact_phone_parsed,
				a.country_id, 
				u.sales_user_id,
				u.email AS u_email,
				u.id AS user_id,
				a.available_24_7,
				a.address,
				a.latitude,
				a.longitude,
				a.about_en,
				a.about_fr,
				a.about_it,
				a.about_pt,
				a.about_de,
				a.about_gr,
				a.is_anonymous,
				a.web
			FROM agencies a
			INNER JOIN users u ON a.user_id = u.id
			LEFT JOIN cities c ON c.id = a.city_id
			WHERE 1 AND a.id = ?
		';
		$agency = self::db()->fetchRow($sql, array($id));
		
		$working_hours = self::db()->fetchAll('SELECT * FROM agency_working_times WHERE agency_id = ?', array($id));
		$whs = array();		
		foreach ( $working_hours as $wh )
		{
			$whs[$wh->day_index] = array('day_index' => $wh->day_index, 'time_from' => $wh->time_from, 'time_to' => $wh->time_to);
		}
		
		return array(
			'agency_data' => $agency,
			'working_hours' => $whs
		);
	}
	
	public function create(Model_AgencyItem $item)
	{
		try {
			self::db()->beginTransaction();
			
			
			// <editor-fold defaultstate="collapsed" desc="Inserting User">
			
			
			$users_model = new Model_Users();
			$user_data = $item->getData(array('u_email', 'username', 'password', 'status', 'sales_user_id'));

			$user_data['activation_hash'] = md5(microtime());
			$user_data['user_type'] = Model_Users::USER_TYPE_AGENCY;
			$user_data['email'] = $user_data['u_email'];
			unset($user_data['u_email']);
			
			$salt_hash = Cubix_Salt::generateSalt($user_data['password']);
			$pass = Cubix_Salt::hashPassword($user_data['password'], $salt_hash);
			$user_data['password'] = $pass;
			$user_data['salt_hash'] = $salt_hash;

			$user_id = $users_model->save(new Model_UserItem($user_data));
			// </editor-fold>

			// <editor-fold defaultstate="collapsed" desc="Insert Member">
			$agency_data = $item->getData(array(
				'name', 'slug', 'country_id', 'city_id', 'email', 'web', 'phone', 
				'contact_phone_parsed', 'phone_country_id', 'phone_instructions', 
				'available_24_7', 'address', 'latitude', 'longitude', 'is_anonymous'
			));
			
			$agency_data['user_id'] = $user_id;
			
			$about = $item->getData(array('about'));
			$agency_data = array_merge($agency_data, $about['about']);
			
			self::db()->insert('agencies', $agency_data);
			
			$agency_id = self::db()->lastInsertId();
			
			
			// </editor-fold>
			
			$working_hours = $item->getData(array('work_days', 'work_times_from', 'work_times_to'));
			if ( ! $agency_data['available_24_7'] ) {
				
				for($i = 1; $i <= 7; $i++)
				{
					if(isset($working_hours['work_days'][$i]))
					{
						self::db()->insert('agency_working_times', array('agency_id' => $agency_id, 'day_index' => $working_hours['work_days'][$i], 'time_from' => $working_hours['work_times_from'][$i], 'time_to' => $working_hours['work_times_to'][$i]));
					}
				}
			}
			else {
				self::db()->query('DELETE FROM agency_working_times WHERE agency_id = ?', parent::quote('agency_id = ?', $agency_id));
			}
			self::db()->commit();
		} catch( Exception $e ) {
			self::db()->rollBack();
			throw $e;
		}
	}
	
	public function update(Model_AgencyItem $item)
	{
		try {
			self::db()->beginTransaction();
			
			// <editor-fold defaultstate="collapsed" desc="Updating User">
			
			
			$users_model = new Model_Users();
			$user_data = $item->getData(array('u_email', 'username', 'password', 'status', 'sales_user_id'));
			
			$user_data['activation_hash'] = md5(microtime());
			$user_data['user_type'] = Model_Users::USER_TYPE_AGENCY;
			$user_data['email'] = $user_data['u_email'];
			$user_data['id'] = $item->user_id;
			unset($user_data['u_email']);
			
			if ( strlen($user_data['password']) ) {
				$salt_hash = Cubix_Salt::generateSalt($user_data['password']);
				$pass = Cubix_Salt::hashPassword($user_data['password'], $salt_hash);
				$user_data['password'] = $pass;
				$user_data['salt_hash'] = $salt_hash;
			} else {
				unset($user_data['password']);
			}

			$users_model->save(new Model_UserItem($user_data));
			// </editor-fold>

			// <editor-fold defaultstate="collapsed" desc="Update Member">
			$agency_data = $item->getData(array(
				'name', 'slug', 'country_id', 'city_id', 'email', 'web', 'phone', 
				'contact_phone_parsed', 'phone_country_id', 'phone_instructions', 
				'available_24_7', 'address', 'latitude', 'longitude', 'is_anonymous'
			));
			
			$agency_data['user_id'] = $item->user_id;
			
			$about = $item->getData(array('about'));
			$agency_data = array_merge($agency_data, $about['about']);
			
			self::db()->update('agencies', $agency_data, self::quote('id = ?', $item->id));
			
			$agency_id = $item->id;
			
			
			// </editor-fold>
			
			self::db()->delete('agency_working_times', self::quote('agency_id = ?', $agency_id));
			
			$working_hours = $item->getData(array('work_days', 'work_times_from', 'work_times_to'));
			$working_hours['agency_id'] = $agency_id;
			
			if ( ! $agency_data['available_24_7'] ) {
				for($i = 1; $i <= 7; $i++)
				{
					if(isset($working_hours['work_days'][$i]))
					{
						self::db()->insert('agency_working_times', array('agency_id' => $agency_id, 'day_index' => $working_hours['work_days'][$i], 'time_from' => $working_hours['work_times_from'][$i], 'time_to' => $working_hours['work_times_to'][$i]));
					}
				}
			}
			else {
				self::db()->query('DELETE FROM agency_working_times WHERE agency_id = ?', self::quote('agency_id = ?', $agency_id));
			}
			self::db()->commit();
		} catch( Exception $e ) {
			self::db()->rollBack();
			throw $e;
		}
	}
	
	public function remove($id)
	{
		$m_user = new Model_Users();
		try {
			self::db()->beginTransaction();
			
			$agency = $this->getById($id);
			$agency = $agency['agency_data'];
			$m_user->remove($agency->user_id);
			
			self::db()->query('DELETE FROM agency_working_times ', self::quote('agency_id = ?', $id));
			self::db()->query('DELETE FROM agencies', self::quote('id = ?', $id));
			
			self::db()->commit();
		} catch( Exception $e ) {
			self::db()->rollBack();
			throw $e;
		}
	}
}

?>
