<?php

class Model_Geography_Countries extends Cubix_Model
{
	protected $_table = 'countries';
	
	protected $_itemClass = 'Model_Geography_CountryItem';
	
	public function getAll($p, $pp, $sort_field, $sort_dir, &$count)
	{
		$count = self::getAdapter()->fetchOne('
			SELECT COUNT(id) FROM countries
		');
		
		return parent::_fetchAll('
			SELECT id, ' . Cubix_I18n::getTblField('title') . ', iso FROM countries
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
		');
	}
	
	public function ajaxGetAll($must_have_regions = true)
	{
		return parent::_fetchAll('
			SELECT id, ' . Cubix_I18n::getTblField('title') . ' AS title, ordering FROM countries
			' . ($must_have_regions ? 'WHERE has_regions = TRUE' : '') . '
			ORDER BY ordering DESC, title ASC
		');
	}
	
	public function get($id)
	{
		return parent::_fetchRow('
			SELECT id, ' . Cubix_I18n::getTblFields('title') . ', iso, has_regions FROM countries
			WHERE id = ?
		', $id);
	}
	
	public function save(Model_Geography_CountryItem $item) {
		/* will update all countries slugs
		$cs = $this->ajaxGetAll(false);
		foreach ($cs as $c) {
			self::getAdapter()->query('UPDATE countries SET slug = ? WHERE id = ?', array(Cubix_Utils::makeSlug($c->title), $c->id));
		}
		*/
		$slug = Cubix_Utils::makeSlug(Cubix_Model::translit($item->{'title_' . Cubix_I18n::getLang()}));
		$item->setSlug($slug);
		
		parent::save($item);
	}
	
	public function remove($id)
	{
		parent::remove(self::quote('id = ?' , $id));
	}

	public static function getPhoneCountries() {
		$db = Zend_Registry::get('db');
		$sql = '
			SELECT cpc.id,
				cpc.' . Cubix_I18n::getTblField('title') . ' as title,
				cpc.phone_prefix,
				cpc.ndd_prefix
			FROM countries_phone_code cpc
		';
		return $db->fetchAll($sql);
	}
}
