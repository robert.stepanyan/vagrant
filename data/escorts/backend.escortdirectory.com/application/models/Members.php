<?php

class Model_Members extends Cubix_Model
{
	
	public function update(Model_MemberItem $item)
	{
		try {
			self::db()->beginTransaction();
			// <editor-fold defaultstate="collapsed" desc="Updating User">
			$users_model = new Model_Users();
			$user_data = $item->getData(array('email', 'username', 'password', 'status'));

			$user_data['activation_hash'] = md5(microtime());
			$user_data['user_type'] = Model_Users::USER_TYPE_MEMBER;
			$user_data['id'] = $item->user_id;

			if ( strlen($user_data['password']) )
			{
				$salt_hash = Cubix_Salt::generateSalt($user_data['password']);
				$pass = Cubix_Salt::hashPassword($user_data['password'], $salt_hash);
				$user_data['password'] = $pass;
				$user_data['salt_hash'] = $salt_hash;
			}
			else 
			{
				unset($user_data['password']);
			}

			$users_model->save(new Model_UserItem($user_data));
			// </editor-fold>

			// <editor-fold defaultstate="collapsed" desc="Update Member">
			$member_data = $item->getData(array('about_me', 'country_id', 'city_id', 'email'));

			self::db()->update('members', $member_data, parent::quote('id = ?', $item->id));
			// </editor-fold>
			self::db()->commit();
		} catch( Exception $e ) {
			self::db()->rollBack();
			throw $e;
		}
	}
	
	public function create(Model_MemberItem $item)
	{
		try {
			self::db()->beginTransaction();
			// <editor-fold defaultstate="collapsed" desc="Updating User">
			$users_model = new Model_Users();
			$user_data = $item->getData(array('email', 'username', 'password', 'status'));

			$user_data['activation_hash'] = md5(microtime());
			$user_data['user_type'] = Model_Users::USER_TYPE_MEMBER;

			$salt_hash = Cubix_Salt::generateSalt($user_data['password']);
			$pass = Cubix_Salt::hashPassword($user_data['password'], $salt_hash);
			$user_data['password'] = $pass;
			$user_data['salt_hash'] = $salt_hash;

			$user_id = $users_model->save(new Model_UserItem($user_data));
			// </editor-fold>

			// <editor-fold defaultstate="collapsed" desc="Update Member">
			$member_data = $item->getData(array('about_me', 'country_id', 'city_id', 'email'));
			$member_data['user_id'] = $user_id;
			self::db()->insert('members', $member_data);
			// </editor-fold>
			self::db()->commit();
		} catch( Exception $e ) {
			self::db()->rollBack();
			throw $e;
		}
	}
	
	public function getById($id) //UserId
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS 
				m.id, m.country_id, m.city_id, m.about_me,
				u.id AS user_id, u.username, u.email, u.status
			FROM members m
			INNER JOIN users u ON u.id = m.user_id
			WHERE u.id = ?
		';
		
		return self::db()->fetchRow($sql, array($id));
	}
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS 
				m.id, 
				u.id AS user_id, u.username, u.email, u.status
			FROM members m
			INNER JOIN users u ON u.id = m.user_id
			WHERE 1
		';
		
		$where = '';
		
		if ( strlen($filter['u.id']) ) {
			$where .= self::quote('AND u.id = ?', $filter['u.id']);
		}
		
		if ( strlen($filter['u.username']) ) {
			$where .= self::quote('AND u.username LIKE ?', $filter['u.username'] . '%');
		}
		
		if ( strlen($filter['u.email']) ) {
			$where .= self::quote('AND u.email = ?', $filter['u.email']);
		}
		
		if ( strlen($filter['u.status']) ) {
			$where .= self::quote('AND u.status = ?', $filter['u.status']);
		}
		
		$sql .= $where;
		
		$sql .= '
			GROUP BY u.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$data = self::db()->fetchAll($sql);
		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');
		return $data;
	}
	
	public function remove($id)
	{
		$m_user = new Model_Users();
		try {
			self::db()->beginTransaction();
			$m_user->remove($id);
			self::db()->delete('members', parent::quote('user_id = ?', $id));
			self::db()->commit();
		} catch( Exception $e ) {
			self::db()->rollBack();
			throw $e;
		}
	}
		
}

?>
