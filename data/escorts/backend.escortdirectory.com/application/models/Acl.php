<?php

class Model_Acl extends Zend_Acl 
{
	/**
	 * @var Model_Acl
	 */
	protected static $_instance;
	
	/**
	 * @return Model_Acl
	 */
	public static function getInstance()
	{
		if ( empty(self::$_instance) ) {
			self::$_instance = new self();
		}
		
		return self::$_instance;
	}
	
	/**
	 * @var Zend_Auth
	 */
	protected $_auth;
	
	public function __construct()
	{
		$this->_auth = Zend_Auth::getInstance();
	}
	
	public function isAllowed($resource)
	{
		return true;
		
		$resource = explode('/', $resource);
		$resources = array();
		for ( $i = 0; $i < count($resource); $i++ ) { $resources[] = implode('/', array_slice($resource, 0, $i + 1)); }
		
		foreach ( $this->_auth->getIdentity()->getGroups() as $role_id ) {
			foreach ( $resources as $resource ) {
				if ( parent::has($resource) && parent::isAllowed($role_id, $resource) ) {
					return true;
				}
			}
		}
		
		return false;
	}
}
