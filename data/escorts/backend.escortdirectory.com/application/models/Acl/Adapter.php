<?php
class Model_Acl_Adapter
{
	public function getRoles()
	{
		$db = Zend_Registry::get('db');
		
		return $db->fetchAll("SELECT * FROM acl_roles");
	}
	
	public function getResources()
	{
		$db = Zend_Registry::get('db');
	
		return $db->fetchAll("
			SELECT res.*, r.role  FROM acl_resources res
			INNER JOIN acl_roles r ON r.id = res.role_id
		");
	}
}
