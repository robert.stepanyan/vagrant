<?php

class Model_Dashboard
{
	protected $_request;
	
	const IVR_USER_ID = 100;
	
	public function __construct()
	{
		$this->_request = Zend_Controller_Front::getInstance()->getRequest();
	}

	public function updateGeography()
	{
		$db = Zend_Registry::get('db');
		$regions = $db->query("SELECT id, slug FROM regions")->fetchAll();
		foreach($regions as $region)
		{
			$slug = str_replace('_', '-', $region->slug);
			$db->query('UPDATE regions SET slug = ? WHERE id = ?', array($slug, $region->id));
		}		
	}
	
	public function getVerifyRequestsByType($type, $be_user)
	{
		$model = new Model_Verifications_Requests();

		$filter = array(
			'vr.status' => Model_Verifications_Requests::PENDING,
			'u.application_id' => $this->_request->application_id
		);

		if ( $be_user->type != 'superadmin' && $be_user->type != 'admin' && $be_user->type != 'dash manager' ) {
			$filter = array_merge($filter, array('bu.id' => $be_user->id));
		}

		$count = 0;
		$data = $model->getAll(1, 1000000, 'date', 'DESC', $filter, $count, $type);
		
		return (object) array('data' => $data, 'count' => $count);
	}
	
	public function getVerifyNotifications($be_user)
	{
		$escorts = new Model_Escorts();

		$filter = array(
			'e.verified_status' => Model_Escorts::VERIFY_RESET,
			'u.application_id' => $this->_request->application_id
		);

		
		/*if ( $be_user->type != 'superadmin' && $be_user->type != 'admin' ) {
			$filter = array_merge($filter, array('bu.id' => $be_user->id));
		}*/

		$count = 0;
		$data = $escorts->getAll(1, 10, $filter, 'showname', 'ASC', $count);
		
		foreach ( $data as $k => $escort ) {
			$aStr = array();
			
			if ( $private_count = $escort->getNewPhotoCount(ESCORT_PHOTO_TYPE_PRIVATE) ) {
				$aStr[] = "{$private_count} private photo" . ($private_count > 1 ? 's' : '');
			}
			
			if ( $public_count = $escort->getNewPhotoCount(new Zend_Db_Expr(
				"" . ESCORT_PHOTO_TYPE_SOFT . ", " . ESCORT_PHOTO_TYPE_HARD . ""
			)) ) {
				$aStr[] = "{$public_count} public photo" . ($public_count > 1 ? 's' : '');
			}
			
			$data[$k]->info = 'added ' . implode(' and ', $aStr);
		}
		
		return (object) array('data' => $data, 'count' => $count);
	}
	
	public function getLatestRegisteredEscorts($app_id, $be_user)
	{
		//$app_id = Zend_Auth::getInstance()->getIdentity()->application_id;

		$model = new Model_Escorts();

		$join = '';
		$where = '';
		if ( $be_user->type != 'superadmin' && $be_user->type != 'admin' && $be_user->type != 'dash manager' ) {
			$join .= 'INNER JOIN backend_users be ON be.id = u.sales_user_id';
			$where = " AND be.id = " . $be_user->id . " ";
		}
		
		$sql = "
			SELECT ag.name AS agency_name, ag.id AS agency_id, e.id, e.showname, pu.date_updated AS date_registered, pu.status FROM escorts e
			INNER JOIN profile_updates pu ON pu.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id
			" . $join . "
			LEFT JOIN agencies ag ON ag.id = e.agency_id
			WHERE " . Model_Escort_ListV2::_whereStatus('e.status', Model_Escorts::ESCORT_STATUS_IS_NEW, array(Model_Escorts::ESCORT_STATUS_ACTIVE, Model_Escorts::ESCORT_STATUS_NO_PROFILE, Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS, Model_Escorts::ESCORT_STATUS_DELETED)) . "
				AND u.application_id = ? {$where}
			GROUP BY e.id
		";
		
		$sqlCount = "
			SELECT COUNT(DISTINCT(e.id)) FROM escorts e
			INNER JOIN profile_updates pu ON pu.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id
			" . $join . "
			WHERE " . Model_Escort_ListV2::_whereStatus('e.status', Model_Escorts::ESCORT_STATUS_IS_NEW, array(Model_Escorts::ESCORT_STATUS_ACTIVE, Model_Escorts::ESCORT_STATUS_NO_PROFILE, Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS, Model_Escorts::ESCORT_STATUS_DELETED)) . "
				AND u.application_id = ? {$where}
		";
		
		$db = Zend_Registry::get('db');
		
		$data = $db->query($sql, array($app_id))->fetchAll();
		$count = $db->fetchOne($sqlCount, array($app_id));
		
		return (object) array('data' => $data, 'count' => $count);
	}

	public function getLatestRegisteredEscortsV2($app_id, $be_user)
	{
		//$app_id = Zend_Auth::getInstance()->getIdentity()->application_id;

		$model = new Model_Escorts();

		$join = '';
		$where = '';
		if ( $be_user->type != 'superadmin' && $be_user->type != 'admin' && $be_user->type != 'dash manager' ) {
			$join .= 'INNER JOIN backend_users be ON be.id = u.sales_user_id';
			$where = " AND be.id = " . $be_user->id . " ";
		}
		else {
			$join .= 'LEFT JOIN backend_users be ON be.id = u.sales_user_id';
		}
		
		/*if ( $app_id == 16 ) {
			$where = " AND be.id <> " . self::IVR_USER_ID . " ";
		}*/
		
		$sql = "
			SELECT 
				ag.name AS agency_name, ag.id AS agency_id, e.id, e.showname, pu.date_updated AS date_registered, pu.status, be.username AS sales_name, u.email
			FROM escorts e
			INNER JOIN profile_updates_v2 pu ON pu.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id
			" . $join . "
			LEFT JOIN agencies ag ON ag.id = e.agency_id
			WHERE " . Model_Escort_ListV2::_whereStatus('e.status', Model_Escorts::ESCORT_STATUS_IS_NEW, array(/*Model_Escorts::ESCORT_STATUS_OWNER_DISABLED,*/ Model_Escorts::ESCORT_STATUS_ACTIVE, /*Model_Escorts::ESCORT_STATUS_NO_PROFILE,*/ Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS, Model_Escorts::ESCORT_STATUS_DELETED)) . "
				/*AND pu.status = " . Model_Escort_ProfileV2::REVISION_STATUS_NOT_APPROVED . "*/ AND u.application_id = ? {$where}
			GROUP BY e.id
			ORDER BY date_registered DESC
		";

		$sqlCount = "
			SELECT COUNT(DISTINCT(e.id)) FROM escorts e
			INNER JOIN profile_updates_v2 pu ON pu.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id
			" . $join . "
			WHERE " . Model_Escort_ListV2::_whereStatus('e.status', Model_Escorts::ESCORT_STATUS_IS_NEW, array(/*Model_Escorts::ESCORT_STATUS_OWNER_DISABLED,*/ Model_Escorts::ESCORT_STATUS_ACTIVE, /*Model_Escorts::ESCORT_STATUS_NO_PROFILE,*/ Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS, Model_Escorts::ESCORT_STATUS_DELETED)) . "
				/*AND pu.status = " . Model_Escort_ProfileV2::REVISION_STATUS_NOT_APPROVED . "*/ AND u.application_id = ? {$where}
		";

		$db = Zend_Registry::get('db');

		$data = $db->query($sql, array($app_id))->fetchAll();
		$count = $db->fetchOne($sqlCount, array($app_id));

		return (object) array('data' => $data, 'count' => $count);
	}
	
	public function phoneBillingEscorts($app_id)
	{
		$model = new Model_Escorts();

		$join = '';
		$where = '';
		
		$join .= 'INNER JOIN backend_users be ON be.id = u.sales_user_id';
		//$where = " AND be.id = " . self::IVR_USER_ID . " ";
		
		$sql = "
			SELECT SQL_CALC_FOUND_ROWS
				ag.name AS agency_name, ag.id AS agency_id, e.id, e.showname, pu.date_updated AS date_registered, pu.status, be.username AS sales_name, u.email
			FROM escorts e
			INNER JOIN profile_updates_v2 pu ON pu.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id
			" . $join . "
				
			INNER JOIN order_packages op ON op.escort_id = e.id
			INNER JOIN orders o ON o.id = op.order_id
			INNER JOIN packages p ON op.package_id = p.id

			INNER JOIN transfer_orders tto ON tto.order_id = o.id
			INNER JOIN transfers t ON t.id = tto.transfer_id

			LEFT JOIN agencies ag ON ag.id = e.agency_id
			WHERE 
				t.transfer_type_id = 7 AND t.status = " . Model_Billing_Transfers::STATUS_PENDING . " AND t.phone_billing_paid = 1 AND u.application_id = ? {$where}
			GROUP BY e.id
			ORDER BY date_registered DESC
		";

		$sqlCount = "SELECT FOUND_ROWS();";

		$db = Zend_Registry::get('db');

		$data = $db->query($sql, array($app_id))->fetchAll();
		$count = $db->fetchOne($sqlCount);

		return (object) array('data' => $data, 'count' => $count);
	}
	
	/*public function _whereStatus($field, $status, $excl_status = array())
	{
		if ( ! is_array($status) ) $status = array($status);
		if ( ! is_array($excl_status) ) $excl_status = array($excl_status);
		
		
		$ret = array();
		foreach ( $status as $st ) {
			$ret[] = "({$field} & {$st})";
		}
		
		$result = implode(' AND ', $ret);
		
		if ( count($status) && count($excl_status) ) $result .= ' AND ';
		
		$ret = array();
		foreach ( $excl_status as $st ) {
			$ret[] = "NOT ({$field} & {$st})";
		}
		
		$result .= implode(' AND ', $ret);
		
		return $result;
	}*/
	
	public function getNotApprovedRevisions($app_id, $be_user)
	{
		//$app_id = Zend_Auth::getInstance()->getIdentity()->application_id;

		$model = new Model_Escort_Profile();
		$revs = $model->getNotApprovedRevisions($app_id, $be_user);
		
		
		return $revs;
	}

	public function getNotApprovedRevisionsV2($app_id, $be_user)
	{
		//$app_id = Zend_Auth::getInstance()->getIdentity()->application_id;

		$model = new Model_Escort_ProfileV2();
		$revs = $model->getNotApprovedRevisions($app_id, $be_user);


		return $revs;
	}
	
	public function getStatistics($app_id)
	{
		$cache = Zend_Registry::get('cache');
		
		$result = (object) array(
			'online_escorts' => 0,
			'online_guests' => 0,
			'online_clients' => 0,
			'total_escorts' => 0,
			'total_agencies' => 0,
			'new_escorts' => 0,
			'new_members' => 0,
			'active_packages' => 0,
			'total_active_packages' => 0,
			'new_reviews' => 0,
			'new_comments' => 0,
			'booking_requests' => 0,
			'contact_me' => 0,
			'views_today' => 0,
			'views_yesterday' => 0,
			'views_week' => 0,
			'views_month' => 0,
			'views_avg_today' => 0,
			'views_avg_yesterday' => 0,
			'views_avg_week' => 0,
			'views_avg_month' => 0
		);
		
		$cache_key = 'v2_views_of_profile_' . Sceon_Application::getId();
		
		if ( ! $result = $cache->load($cache_key) )
		{
			echo "no cache";
			$db = Zend_Registry::get('db');

			$config = Zend_Registry::get('system_config');
			$interval = $config['dashboard']['onlineAccessInterval'];

			$result->online_escorts = $db->fetchOne('
				SELECT COUNT(acs.id) FROM activity_client_sessions acs
				INNER JOIN users u ON u.id = acs.user_id AND u.user_type = "escort"
				WHERE acs.user_id IS NOT NULL AND acs.date_last_access > DATE_ADD(NOW(), INTERVAL -' . $interval . ' MINUTE)
			');

			$result->online_guests = $db->fetchOne('
				SELECT COUNT(acs.id) FROM activity_client_sessions acs
				WHERE acs.user_id IS NULL AND acs.date_last_access > DATE_ADD(NOW(), INTERVAL -' . $interval . ' MINUTE)
			');

			$result->online_clients = $db->fetchOne('
				SELECT COUNT(*) FROM activity_clients
				WHERE date_last_access > DATE_ADD(NOW(), INTERVAL -' . $interval . ' MINUTE)
			');

			$model = new Model_Escorts();

			$count = 0;
			$data = $model->getAll(1, 0, array(
				'e.status' => Model_Escorts::ESCORT_STATUS_ACTIVE,
				'u.application_id' => $this->_request->application_id,
				'excl_status' => Model_Escorts::ESCORT_STATUS_DELETED
			), 'u.id', 'DESC', $count);

			$result->total_escorts = $count;

			$model = new Model_Agencies();

			$model->getAll(1, 0, array(
				'u.status' => STATUS_ACTIVE,
				'u.application_id' => $this->_request->application_id
			), 'u.id', 'DESC', $count);

			$result->total_agencies = $count;

			/* Added by Tiko */
			$result->new_escorts = $db->fetchOne('SELECT COUNT(DISTINCT(e.id)) FROM escorts e INNER JOIN users u ON u.id = e.user_id WHERE u.application_id = ? AND DATE(e.date_registered) = DATE(NOW())', $app_id);
			$result->new_members = $db->fetchOne('SELECT COUNT(*) FROM users WHERE application_id = ? AND user_type = ? AND DATE(date_registered) = DATE(NOW())', array($app_id, 'member'));
			$result->active_packages = $db->fetchOne('SELECT COUNT(*) FROM order_packages WHERE application_id = ? AND DATE(date_activated) = DATE(NOW())', $app_id);
			$result->new_reviews = $db->fetchOne('SELECT COUNT(*) FROM reviews WHERE is_deleted = 0 AND application_id = ? AND status = ?', array($app_id, REVIEW_NOT_APPROVED));
			$result->contact_me = $db->fetchOne('SELECT COUNT(*) FROM contact_log WHERE application_id = ? AND DATE(date) = DATE(NOW())', array($app_id));
			if (in_array($app_id, array(1, 2, APP_A6, APP_A6_AT)))
				$result->total_active_packages = $db->fetchOne('
					SELECT COUNT(p.id) 
					FROM order_packages p 
					INNER JOIN orders o ON o.id = p.order_id AND o.status = 3
					WHERE p.application_id = ? AND p.status = 2
				', $app_id);
			else
				$result->total_active_packages = $db->fetchOne('SELECT COUNT(*) FROM order_packages WHERE application_id = ? AND status = 2', $app_id);
			if (in_array($app_id, array(22, 33)))
				$result->booking_requests = $db->fetchOne('SELECT COUNT(*) FROM booking_requests WHERE application_id = ? AND DATE(creation_date) = DATE(NOW())', array($app_id));


			if (in_array($app_id, array(6, 5, 9)))
			{
				$result->new_comments = $db->fetchOne('
					SELECT COUNT(c.id) 
					FROM comments c
					INNER JOIN users u ON u.id = c.user_id
					WHERE u.application_id = ? AND c.status = ?
				', array($app_id, COMMENT_NOT_APPROVED));
				$result->views_today = $db->fetchOne('
					SELECT SUM(h.count) 
					FROM escort_hits_daily h 
					INNER JOIN escorts e ON e.id = h.escort_id 
					INNER JOIN users u ON u.id = e.user_id
					WHERE u.application_id = ? AND DATE(h.date) = DATE(NOW())
				', array($app_id));
				$result->views_yesterday = $db->fetchOne('
					SELECT SUM(h.count) 
					FROM escort_hits_daily h 
					INNER JOIN escorts e ON e.id = h.escort_id 
					INNER JOIN users u ON u.id = e.user_id
					WHERE u.application_id = ? AND DATE(h.date) = DATE_ADD(DATE(NOW()), INTERVAL -1 DAY)
				', array($app_id));
				$result->views_week = $db->fetchOne('
					SELECT SUM(h.count) 
					FROM escort_hits_daily h 
					INNER JOIN escorts e ON e.id = h.escort_id 
					INNER JOIN users u ON u.id = e.user_id
					WHERE u.application_id = ? AND (DATE(h.date) >= DATE_ADD(DATE(NOW()), INTERVAL -1 WEEK) AND DATE(h.date) < DATE(NOW()))
				', array($app_id));
				$result->views_month = $db->fetchOne('
					SELECT SUM(h.count) 
					FROM escort_hits_daily h 
					INNER JOIN escorts e ON e.id = h.escort_id 
					INNER JOIN users u ON u.id = e.user_id
					WHERE u.application_id = ? AND (DATE(h.date) >= DATE_ADD(DATE(NOW()), INTERVAL -1 MONTH) AND DATE(h.date) < DATE(NOW()))
				', array($app_id));
				$result->views_avg_today = $db->fetchOne('
					SELECT SUM(h.count) / COUNT(DISTINCT h.escort_id) 
					FROM escort_hits_daily h 
					INNER JOIN escorts e ON e.id = h.escort_id 
					INNER JOIN users u ON u.id = e.user_id
					WHERE u.application_id = ? AND DATE(h.date) = DATE(NOW())
				', array($app_id));
				$result->views_avg_yesterday = $db->fetchOne('
					SELECT SUM(h.count) / COUNT(DISTINCT h.escort_id) 
					FROM escort_hits_daily h 
					INNER JOIN escorts e ON e.id = h.escort_id 
					INNER JOIN users u ON u.id = e.user_id
					WHERE u.application_id = ? AND DATE(h.date) = DATE_ADD(DATE(NOW()), INTERVAL -1 DAY)
				', array($app_id));
				$w = $db->fetchAll('
					SELECT (SUM(count) / COUNT(DISTINCT escort_id)) AS avg 
					FROM escort_hits_daily h 
					INNER JOIN escorts e ON e.id = h.escort_id 
					INNER JOIN users u ON u.id = e.user_id
					WHERE u.application_id = ? AND (DATE(h.date) >= DATE_ADD(DATE(NOW()), INTERVAL -1 WEEK) AND DATE(h.date) < DATE(NOW())) GROUP BY date
				', array($app_id));
				$m = $db->fetchAll('
					SELECT (SUM(count) / COUNT(DISTINCT escort_id)) AS avg 
					FROM escort_hits_daily h 
					INNER JOIN escorts e ON e.id = h.escort_id 
					INNER JOIN users u ON u.id = e.user_id
					WHERE u.application_id = ? AND (DATE(h.date) >= DATE_ADD(DATE(NOW()), INTERVAL -1 MONTH) AND DATE(h.date) < DATE(NOW())) GROUP BY date
				', array($app_id));
			}
			else
			{
				$result->new_comments = $db->fetchOne('SELECT COUNT(*) FROM comments WHERE status = ?', array(COMMENT_NOT_APPROVED));
				$result->views_today = $db->fetchOne('SELECT SUM(count) FROM escort_hits_daily WHERE DATE(date) = DATE(NOW())');
				$result->views_yesterday = $db->fetchOne('SELECT SUM(count) FROM escort_hits_daily WHERE DATE(date) = DATE_ADD(DATE(NOW()), INTERVAL -1 DAY)');
				$result->views_week = $db->fetchOne('SELECT SUM(count) FROM escort_hits_daily WHERE (DATE(date) >= DATE_ADD(DATE(NOW()), INTERVAL -1 WEEK) AND DATE(date) < DATE(NOW()))');
				$result->views_month = $db->fetchOne('SELECT SUM(count) FROM escort_hits_daily WHERE (DATE(date) >= DATE_ADD(DATE(NOW()), INTERVAL -1 MONTH) AND DATE(date) < DATE(NOW()))');
				$result->views_avg_today = $db->fetchOne('SELECT SUM(count) / COUNT(DISTINCT escort_id) FROM escort_hits_daily WHERE DATE(date) = DATE(NOW())');
				$result->views_avg_yesterday = $db->fetchOne('SELECT SUM(count) / COUNT(DISTINCT escort_id) FROM escort_hits_daily WHERE DATE(date) = DATE_ADD(DATE(NOW()), INTERVAL -1 DAY)');
				$w = $db->fetchAll('SELECT (SUM(count) / COUNT(DISTINCT escort_id)) AS avg FROM escort_hits_daily WHERE (DATE(date) >= DATE_ADD(DATE(NOW()), INTERVAL -1 WEEK) AND DATE(date) < DATE(NOW())) GROUP BY date');
				$m = $db->fetchAll('SELECT (SUM(count) / COUNT(DISTINCT escort_id)) AS avg FROM escort_hits_daily WHERE (DATE(date) >= DATE_ADD(DATE(NOW()), INTERVAL -1 MONTH) AND DATE(date) < DATE(NOW())) GROUP BY date');
			}

			$sum_w = 0;
			$sum_m = 0;
			foreach ($w as $item)
				$sum_w += $item->avg;
			foreach ($m as $item)
				$sum_m += $item->avg;

			$result->views_avg_week = $sum_w;
			$result->views_avg_month = $sum_m;
		
			$cache->save($result, $cache_key, array(), 7200);
		}
		
		return $result;
	}

	public function getPendingOrders($bu_user)
	{
		$m_orders = new Model_Billing_Orders();

		$filter = array();

		if ( $bu_user->type == 'sales manager' ) {
			$filter['o.backend_user_id = ?'] = $bu_user->id;
		}
		else if ($bu_user->type == 'admin') {
			$filter['bu.application_id = ?'] = $bu_user->application_id;
		}
		$filter['o.status = ?'] = Model_Billing_Orders::STATUS_PENDING;

		$data = $m_orders->getAll(
			$filter,
			1,
			1000000
		);

		return $data;
	}
	
	public function getPaymentRejectedOrders($bu_user)
	{
		$m_orders = new Model_Billing_Orders();

		$filter = array();

		if ( $bu_user->type == 'sales manager' ) {
			$filter['o.backend_user_id = ?'] = $bu_user->id;
		}
		else if ($bu_user->type == 'admin') {
			$filter['bu.application_id = ?'] = $bu_user->application_id;
		}
		$filter['o.status = ?'] = Model_Billing_Orders::STATUS_PAYMENT_REJECTED;

		$data = $m_orders->getAll(
			$filter,
			1,
			1000000
		);

		return $data;
	}

	public function getPendingTransfers($bu_user)
	{
		$m_transfers = new Model_Billing_Transfers();
		$filter = array();

		if ( $bu_user->type == 'sales manager' ) {
			$filter['t.backend_user_id = ?'] = $bu_user->id;
		}
		else if ($bu_user->type == 'admin') {
			$filter['bu.application_id = ?'] = $bu_user->application_id;
		}
		$filter['t.status = ?'] = Model_Billing_Transfers::STATUS_PENDING;
		

		$data = $m_transfers->getAll(
			$filter,
			1,
			1000000
		);

		return $data;
	}

	public function getNotApprovedPhotos($app_id, $bu_user)
	{
		$db = Zend_Registry::get('db');

		$where = '';
		if ($bu_user->type == 'admin' || $bu_user->type == 'superadmin' || $bu_user->type == 'dash manager') {
			$where = ' AND bu.application_id = ' . $app_id . ' ';
		}
		else if ($bu_user->type == 'sales manager') {
			$where = ' AND bu.id = ' . $bu_user->id . ' AND bu.application_id = ' . $bu_user->application_id . ' ';
		}

		$items = $db->query("
			SELECT
				e.id, e.showname, count(ep.id) as photo_count, bu.username AS sales_name
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN escort_photos ep ON ep.escort_id = e.id
			INNER JOIN backend_users bu ON bu.id = u.sales_user_id
			WHERE ep.is_approved = 0 {$where} /*AND NOT (e.status & 1)*/ AND NOT (e.status & 256) AND e.showname IS NOT NULL
			GROUP BY e.id
			ORDER BY e.showname ASC
		", array())->fetchAll();

		return $items;
	}
	
	public function getVipVerifiedEscortsByIds($ids)
	{
		$db = Zend_Registry::get('db');
		
		return $db->query('
			SELECT v.escort_id
			FROM vip_requests v 
			WHERE v.escort_id IN (' . $ids . ') AND v.status IN (2, 4) AND v.id = (SELECT MAX(id) FROM vip_requests WHERE escort_id = v.escort_id)
		')->fetchAll();
	}

	static public function getPhonePackage($escort_id)
	{
		$db = Zend_Registry::get('db');

		$sql = '
			SELECT
				op.id,
				e.id AS escort_id,
				e.showname,
				e.agency_id,
				e.gender,

				op.id AS order_package_id,
				op.order_id,
				op.application_id,
				op.period,
				op.discount,
				op.discount_fixed,
				op.surcharge,
				op.price,
				op.base_price,
				op.date_activated,
				op.expiration_date,
				op.activation_date,
				op.activation_type,

				p.name AS package_name,
				p.id AS package_id,
				c.iso AS app_iso,
				u.balance,
				u.id AS user_id,

				ag.name AS agency_name,

				o.status AS order_status,
				op.status,
				o.id AS order_id,
				
				t.id AS transfer_id,
				t.amount AS t_amount,
				t.status AS t_status,
				t.phone_billing_paid
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			LEFT JOIN agencies ag ON ag.id = e.agency_id
			INNER JOIN order_packages op ON op.escort_id = e.id
			INNER JOIN orders o ON o.id = op.order_id
			INNER JOIN packages p ON op.package_id = p.id

			INNER JOIN transfer_orders tto ON tto.order_id = o.id
			INNER JOIN transfers t ON t.id = tto.transfer_id
			
			LEFT JOIN phone_packages pp ON pp.order_package_id = op.id

			INNER JOIN applications a ON a.id = op.application_id
			INNER JOIN countries c ON c.id = a.country_id
			WHERE (op.status = ? OR op.status = ?) AND e.id = ? AND t.transfer_type_id = ?
			ORDER BY t.date_transfered DESC
		';
		
		$items = $db->fetchRow($sql, array(Model_Billing_Packages::STATUS_ACTIVE, Model_Billing_Packages::STATUS_PENDING, $escort_id, 7));

		return $items;
	}
	
	public function getGotdBookedDays()
	{
		$db = Zend_Registry::get('db');
		
		$sql = '
			SELECT * FROM (
				SELECT 
					DATE(op.gotd_activation_date) AS date, op.gotd_city_id AS city_id, 
					op.escort_id, e.showname, c.title_en AS city_title, 2 AS status
				FROM order_packages op
				INNER JOIN cities c ON c.id = op.gotd_city_id
				INNER JOIN escorts e ON e.id = op.escort_id
				WHERE DATE(gotd_activation_date) >= DATE(NOW()) AND op.status = 2
				UNION ALL
				SELECT 
					go.activation_date AS date, go.city_id, go.escort_id, 
					e.showname, c.title_en AS city_title, go.status
				FROM gotd_orders go
				LEFT JOIN order_packages op ON go.order_package_id = op.id
				INNER JOIN cities c ON c.id = go.city_id
				INNER JOIN escorts e ON e.id = go.escort_id
				WHERE DATE(go.activation_date) >= DATE(NOW()) AND ((go.order_package_id IS NULL) OR (go.order_package_id IS NOT NULL AND go.status = 2) )
			) d ORDER BY d.status DESC
		';
		
		$rows = $db->fetchAll($sql);
		
		foreach($rows as $i => $row) {
			if ( ! isset($rows[$row->date]) ) { $rows[$row->date] = array(); }
			$rows[$row->date][] = $row;
			unset($rows[$i]);
		}
		
		return $rows;
	}
}
