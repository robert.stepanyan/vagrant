<?php

class Model_UserItem extends Cubix_Model_Item
{
	public function __construct($rowData = array())
	{
		parent::__construct($rowData);
	}
	
	public function hasAccessToEscort($escort_id)
	{
		if ( 'sales manager' == $this->type/* || 'data entry' == $this->type*/ ) {
			//return false;
			return $this->getId() == $this->getAdapter()->fetchOne('
				SELECT u.sales_user_id FROM escorts e INNER JOIN users u ON u.id = e.user_id WHERE e.id = ?
			', $escort_id);
		}
		else {
			return true;
		}
	}
	
	public function __wakeup()
	{
		$this->setAdapter(Zend_Registry::get('db'));
	}

	//
	public function getEscortId()
	{
		return $this->_adapter->fetchOne('
			SELECT id FROM escorts WHERE user_id = ?
		', $this->getId());
	}

	public function getAgencyId()
	{
		return $this->_adapter->fetchOne('
			SELECT id FROM agencies WHERE user_id = ?
		', $this->getId());
	}
}
