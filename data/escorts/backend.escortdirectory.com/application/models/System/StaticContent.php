<?php

class Model_System_StaticContent extends Cubix_Model
{
	protected $_table = 'static_content';
	protected $_itemClass = 'Model_System_StaticContentItem';

	public function get($slug, $app_id)
	{
		return self::_fetchAll('
			SELECT * FROM static_content WHERE slug = ? AND application_id = ?
		', array($slug, $app_id));
	}
	
	public function getById($slug, $app_id)
	{
		return self::_fetchRow('
			SELECT * FROM static_content WHERE slug = ? AND application_id = ?
		', array($slug, $app_id));
	}
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT slug, title, ( IF(LENGTH(body) > 100, CONCAT(SUBSTRING(body, 1, 400), "..."), body ) ) AS body
			FROM static_content
			WHERE 1
		';
		
		$countSql = '
			SELECT COUNT(DISTINCT(slug))
			FROM static_content
			WHERE 1
		';
		
		if ( strlen($filter['title']) ) {
			$sql .= self::quote('AND title LIKE ?', '%' . $filter['title'] . '%');
			$countSql .= self::quote('AND title LIKE ?', '%' . $filter['title'] . '%');
		}
		
		if ( strlen($filter['application_id']) ) {
			$sql .= self::quote('AND application_id = ?', $filter['application_id']);
			$countSql .= self::quote('AND application_id = ?', $filter['application_id']);
		}
		
		$sql .= '
			GROUP BY slug
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$countSql .= '
			
		';
		
		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return parent::_fetchAll($sql);
	}
	
	public function insert(Model_System_StaticContentItem $item)
	{
		foreach ($item as $it)
		{
			self::getAdapter()->insert($this->_table, $it);
		}
	}
	
	public function update(Model_System_StaticContentItem $item)
	{
		foreach ($item as $it)
		{
			self::getAdapter()->query("DELETE FROM static_content WHERE slug = '" . $it['slug'] . "' AND application_id = '" . $it['application_id'] . "' AND lang_id = '" . $it['lang_id'] . "'");
			self::getAdapter()->insert('static_content', (array)$it);
			//self::getAdapter()->query('UPDATE static_content SET title = ?, body = ? WHERE slug = ? AND lang_id = ? AND application_id = ?', array($it['title'], $it['body'], $it['slug'], $it['lang_id'], $it['application_id']));
			//self::getAdapter()->update($this->_table, $it, self::getAdapter()->quoteInto('id = ?', $it['id']));	
		}	
	}
		
	public function save(Model_System_StaticContentItem $item)
	{
		if ( $item->getId() ) {
			self::getAdapter()->update($this->_table, $item->getData(), self::getAdapter()->quoteInto('id = ?', $item->getId()));
		}
		else {
			$data = $item->getData();
			if ( isset($data['entry_id']) ) {
				$data['id'] = $data['entry_id'];
				unset($data['entry_id']);
			}
			
			self::getAdapter()->insert($this->_table, $data);
		}
	}
	
	public function remove($slug, $app_id)
	{
		parent::remove(array(self::quote('slug = ?', $slug), self::quote('application_id = ?', $app_id)));
	}
}