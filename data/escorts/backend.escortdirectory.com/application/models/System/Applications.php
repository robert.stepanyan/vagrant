<?php

class Model_System_Applications extends Cubix_Model
{
	
	protected $_table = 'applications';
	//protected $_itemClass = 'Model_SMS_SMSItem';
	
	public function get($id)
	{
		$sql = '
			SELECT a.id, a.host, a.url, a.title AS application_title, a.phone_number,
			a.min_photos, a.max_working_cities
			FROM applications a
			WHERE a.id = ?
		';
		
		return parent::_fetchRow($sql, $id);
	}
	
	public function getAll($p, $pp, $sort_field, $sort_dir, $filter, &$count)
	{
		$sql = '
			SELECT a.id, a.host, a.url, a.title AS application_title, a.phone_number
			FROM applications a
			WHERE 1
		';
		
		$countSql = '
			SELECT
				COUNT(a.id)
			FROM applications a
			/*INNER JOIN countries c ON c.id = a.country_id*/
			WHERE 1
		';
		
		$where = '';
		
		if ( strlen($filter['a.id']) ) {
			$where .= self::quote('AND a.id = ?', $filter['a.id']);
		}
						
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
		';
		
		$count = $this->getAdapter()->fetchOne($countSql);
		return parent::_fetchAll($sql);
	}
	
	public function save(Model_System_ApplicationsItem $item)
	{
		parent::save($item);
	}
}
