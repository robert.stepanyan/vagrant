<?php

class Model_System_Dictionary extends Cubix_Model
{
	protected $_table = 'dictionary';
	protected $_itemClass = 'Model_System_DictionaryItem';

	public function getById($id)
	{
		return self::_fetchRow('
			SELECT * FROM dictionary WHERE id = ?
		', $id);
	}
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT id, ' . Cubix_I18n::getTblFields('value') . '/*, ( IF(LENGTH(value_en) > 100, CONCAT(SUBSTRING(value_en, 1, 400), "..."), value_en ) ) AS value_en*/
			FROM dictionary
			WHERE 1
		';
		
		$countSql = '
			SELECT COUNT(id)
			FROM dictionary
			WHERE 1
		';
		
		if ( strlen($filter['id']) ) {
			$sql .= self::quote('AND id LIKE ?', $filter['id'] . '%');
			$countSql .= self::quote('AND id LIKE ?', $filter['id'] . '%');
		}

		if ( strlen($filter['value_en']) ) {
			$sql .= self::quote('AND value_en LIKE ?', '%' . $filter['value_en'] . '%');
			$countSql .= self::quote('AND value_en LIKE ?', '%' . $filter['value_en'] . '%');
		}
		
		if ( strlen($filter['value_it']) ) {
			$sql .= self::quote('AND value_it LIKE ?', '%' . $filter['value_it'] . '%');
			$countSql .= self::quote('AND value_it LIKE ?', '%' . $filter['value_it'] . '%');
		}
		
		if ( strlen($filter['value_fr']) ) {
			$sql .= self::quote('AND value_fr LIKE ?', '%' . $filter['value_fr'] . '%');
			$countSql .= self::quote('AND value_fr LIKE ?', '%' . $filter['value_fr'] . '%');
		}

		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$countSql .= '
			
		';
		
		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return parent::_fetchAll($sql);
	}	
	
	public function save(Model_System_DictionaryItem $item)
	{
		if ( $item->getId() ) {
			self::getAdapter()->update($this->_table, $item->getData(), self::getAdapter()->quoteInto('id = ?', $item->getId()));
		}
		else {
			$data = $item->getData();
			if ( isset($data['entry_id']) ) {
				$data['id'] = $data['entry_id'];
				unset($data['entry_id']);
			}
			
			self::getAdapter()->insert($this->_table, $data);
		}
	}
	
	public function remove($id)
	{
		parent::remove(self::quote('id = ?', $id));
	}
	
	public function getAllForExport()
	{
		$sql = '
			SELECT id, value_en AS value
			FROM dictionary
		';
		
		$data = parent::_fetchAll($sql, array(), Zend_Db::FETCH_ASSOC);
		
		$dic = array();
		
		if ( count($data) > 0 ) {
			foreach ( $data as $d ) {
				$dic[] = array($d['id'], $d['value']);
			}
		}
		
		return $dic;
	}	
}
