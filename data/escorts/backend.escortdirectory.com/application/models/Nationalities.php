<?php

class Model_Nationalities extends Cubix_Model
{
	protected $_table = 'nationalities';
	
	protected $_itemClass = null;
	
	public function getAll()
	{
		$result = parent::_fetchAll('
			SELECT id, ' . Cubix_I18n::getTblField('title') . ' AS title 
			FROM nationalities
			ORDER BY title_en ASC
		');
		
		$new = array();
		foreach ( $result as $i => $row ) {
			$new[$row->id] = $row->title;
		}
		
		return $new;
	}
	
	public function get($id)
	{
		return parent::_fetchRow('
			SELECT id, iso, ' . Cubix_I18n::getTblField('title') . ' AS title
			FROM nationalities
			WHERE id = ?
		', $id);
	}
}
