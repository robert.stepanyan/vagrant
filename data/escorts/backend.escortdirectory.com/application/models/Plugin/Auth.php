<?php

class Model_Plugin_Auth extends Zend_Controller_Plugin_Abstract
{
	/**
	 * @var Zend_Auth
	 */
	protected $_auth;

	public function __construct()
	{
		$this->_auth = Zend_Auth::getInstance();
	}

	public function hasAccess($request, array $permissions = array())
	{
		$m = $request->getModuleName();
		$c = $request->getControllerName();
		$a = $request->getActionName();
		
		$allowed = isset($permissions['allowed']) ? $permissions['allowed'] : null;
		$disallowed = isset($permissions['disallowed']) ? $permissions['disallowed'] : null;

		if ( ! isset($allowed[$m]) ) {
			return false;
		}
		elseif ( $allowed[$m] == '*' ) {}
		elseif ( ! isset($allowed[$m][$c]) ) {
			return false;
		}
		elseif ( $allowed[$m][$c] == '*' ) {}
		elseif ( ! in_array($a, $allowed[$m][$c]) ) {
			return false;
		}

		if ( ! isset($disallowed[$m]) ) {}
		elseif ( ! isset($disallowed[$m][$c]) ) {}
		elseif ( in_array($a, $disallowed[$m][$c])) {
			return false;
		}

		// If passed through filter return true
		return true;
	}

	public function routeStartup($request)
	{
		if ( defined('IS_CLI') && IS_CLI ) return;

		$this->_auth = Zend_Auth::getInstance();
		
		if ( $this->_auth->hasIdentity() ) {
			
			$user = $this->_auth->getIdentity();
			
			$current_app_id = isset($_COOKIE['application_id']) ? $_COOKIE['application_id'] : null;
			
			if ( ! $current_app_id ) $current_app_id = $request->application_id;
			
			if ( ! $current_app_id ) {
				$current_app_id = $user->application_id;
			}
			else if ( $user->type !== 'superadmin' && $user->type !== 'seo manager' && $current_app_id != $user->application_id ) {
				$current_app_id = $user->application_id;
			}

			if ( ! $current_app_id ) {
				$current_app_id = Sceon_Application::getAll();
				$current_app_id = $current_app_id[0]->id;
			}
			
			setcookie('application_id', $current_app_id, time() + 2 * 7 * 24 * 60 * 60 , '/');
			
			$request->setParam('application_id', $current_app_id);
		}
	}
	
	public function routeShutdown($request)
	{
		$request->setParam('lang_id', 'en');
		Cubix_I18n::init();

		require 'Sceon/defines.php';
		Zend_Registry::set('definitions', $DEFINITIONS);

		if ( defined('IS_CLI') && IS_CLI ) return;
		
		if ( ! ($request->getModuleName() == 'billing' && $request->getControllerName() == 'cron' && $request->getActionName() == 'index')
				&& ! ($request->getModuleName() == 'sms' && $request->getControllerName() == 'index' && ($request->getActionName() == 'sms-callback' || $request->getActionName() == 'sms-inbox'))
				&& ! ($request->getModuleName() == 'default' && $request->getControllerName() == 'escorts' && $request->getActionName() == 'add-snapshots') 
				&& ! ($request->getModuleName() == 'default' && $request->getControllerName() == 'escorts-v2' && $request->getActionName() == 'assign-sales')
				&& ! ($request->getModuleName() == 'default' && $request->getControllerName() == 'index' && $request->getActionName() == 'login-chat')
				&& ! ($request->getModuleName() == 'default' && $request->getControllerName() == 'gateway')
				&& ! ($request->getModuleName() == 'billing' && $request->getControllerName() == 'online-billing' && $request->getActionName() == 'confirm-payment')
				&& ! ($request->getModuleName() == 'billing' && $request->getControllerName() == 'online-billing' && $request->getActionName() == 'cardgate-confirm')
				//&& ! ($request->getModuleName() == 'default' && $request->getControllerName() == 'escorts-v2' && $request->getActionName() == 'do-photos')
				
				/*&& ! ($request->getModuleName() == 'default' && $request->getControllerName() == 'cron' && $request->getActionName() == 'reports-per-month')
				&& ! ($request->getModuleName() == 'default' && $request->getControllerName() == 'cron' && $request->getActionName() == 'reports-per-week')
						
				&& ! ($request->getModuleName() == 'billing' && $request->getControllerName() == 'orders' && $request->getActionName() == 'view')*/	) {
			
			if ( ! $this->_auth->hasIdentity() &&
					$request->getControllerName() != 'auth' ) {
				header('Cubix-redirect: /auth');
				header('Location: /auth');
				die('Access denied!');
			}
		}

		$user = $this->_auth->getIdentity();

		if ( 'data entry' == $user->type ) {
			$permissions = array(
				'allowed' => array(
					'default' => array(
						'escorts' => '*',
						'escorts-v2' => '*',
						'agencies' => '*',
						'comments' => '*',
						'reviews' => '*',
						'retouch-photos' => '*',
						'dashboard' => array('index', 'dataentry', 'tip-data'),
						'auth' => array('login', 'logout', 'index'),
						'index' => array('quick-email'),
					),
					'geography' => '*'
				),
				'disallowed' => array(
					'default' => array(
						'escorts' => array('delete'),
						'agencies' => array('delete')
					)
				)
			);

			if ( ! $this->hasAccess($request, $permissions)) {
				die(header('Location: /dashboard'));
			}
		}
		elseif ( 'data entry plus' == $user->type ) {
			$permissions = array(
				'allowed' => array(
					'default' => array(
						'escorts' => '*',
						'escorts-v2' => '*',
						'agencies' => '*',
						'comments' => '*',
						'reviews' => '*',
						'retouch-photos' => '*',
						'dashboard' => array('index', 'dataentry', 'tip-data'),
						'auth' => array('login', 'logout', 'index'),
						'index' => array('quick-email'),
					),
					'geography' => '*',
					'sms' => '*'
				),
				'disallowed' => array(
					'default' => array(
						'escorts' => array('delete'),
						'agencies' => array('delete')
					)
				)
			);

			if ( ! $this->hasAccess($request, $permissions)) {
				die(header('Location: /dashboard'));
			}
		}
		else if ( 'sales manager' == $user->type ) {
			$permissions = array(
				'allowed' => array(
					'default' => array(
						'escorts' => '*',
						'escorts-v2' => '*',
						'agencies' => '*',
						'escort-templates' => '*',
						'comments' => '*',
						'wiki' => '*',
						'issues' => '*',
						'backend-users' => array('edit'),
						//'members' => '*',
						//'dashboard' => array('index', 'dataentry', 'tip-data'),
						'dashboard' => '*',
						'auth' => array('login', 'logout', 'index'),
						'index' => '*',
						'journal'	=> '*',
						'report-problem' => '*',
						'balance' => array('edit')
					),
					'geography' => '*',
					'verifications' => '*',
					'sms' => '*',
					//'newsletter' => '*',
					'billing' => '*'
				),
				'disallowed' => array(
					'default' => array(
						'escorts' => array('delete'),
						'agencies' => array('delete')
					)
				)
			);

			if ( ! $this->hasAccess($request, $permissions)) {
				die(header('Location: /dashboard'));
			}
		}

		else if ( 'moderator plus' == $user->type ) {
			$permissions = array(
				'allowed' => array(
					'default' => array(
						'escorts' => '*',
						'escorts-v2' => '*',
						'agencies' => '*',
						'escort-templates' => '*',
						'comments' => '*',
						'reviews' => '*',
						'client-blacklist' => '*',
						'bubbles' => '*',
						'slogans' => '*',
						'auto-approved-photos' => '*',
						'wiki' => '*',
						'issues' => '*',
						'backend-users' => array('edit'),
						'members' => '*',
						'payments' => '*',
						'turnover' => '*',
						'alertme' => '*',
						//'dashboard' => array('index', 'dataentry', 'tip-data'),
						'dashboard' => '*',
						'auth' => array('login', 'logout', 'index'),
						'index' => '*',
						'journal'	=> '*',
						'feedbacks' => '*',
						'blacklisted-words' => '*',
						'blacklisted-emails' => '*',
						'backend-user-sessions' => '*',
						'report-problem' => '*',
						'balance' => array('edit')
					),
					'geography' => '*',
					'verifications' => '*',
					'sms' => '*',
					'seo' => '*',
					'newsletter' => '*',
					'system' => '*',


				),
				'disallowed' => array(
					'default' => array(
						'backend-users' => '*',
						'escorts-v2' => array('sales-work','delete', 'assign-to','assign-to-me'),
						'escorts' => array('delete'),
						'agencies' => array('delete')
					),
					'billing' => '*'
				)
			);

			if ( ! $this->hasAccess($request, $permissions)) {
				die(header('Location: /dashboard'));
			}
		}
		else if ( 'moderator' == $user->type ) {
			$permissions = array(
				'allowed' => array(
					'default' => array(
						'escorts' => '*',
						'escorts-v2' => '*',
						'agencies' => '*',
						'escort-templates' => '*',
						'comments' => '*',
						'reviews' => '*',
						'client-blacklist' => '*',
						'bubbles' => '*',
						'slogans' => '*',
						'auto-approved-photos' => '*',
						'wiki' => '*',
						'issues' => '*',
						'backend-users' => array('edit'),
						'members' => '*',
						'payments' => '*',
						'turnover' => '*',
						'alertme' => '*',
						//'dashboard' => array('index', 'dataentry', 'tip-data'),
						'dashboard' => '*',
						'auth' => array('login', 'logout', 'index'),
						'index' => '*',
						'journal'	=> '*',
						'feedbacks' => '*',
						'blacklisted-words' => '*',
						'blacklisted-emails' => '*',
						'backend-user-sessions' => '*',
						'report-problem' => '*',
						'balance' => array('edit'),
						'chat' => '*',
					),
					'geography' => '*',
					'verifications' => '*',
					'sms' => '*',
					'seo' => '*',
					'newsletter' => '*',
					'system' => '*',


				),
				'disallowed' => array(
					'default' => array(
						'backend-users' => '*',
						'members' => '*',
						'payments' => '*',
						'turnover' => '*',
						'alertme' => '*',
						'escorts-v2' => array('sales-work','delete', 'assign-to','assign-to-me'),
						'escorts' => array('delete'),
						'agencies' => array('delete')
					),
					'billing' => '*'
				)
			);

			if ( ! $this->hasAccess($request, $permissions)) {
				die(header('Location: /dashboard'));
			}
		}
		elseif ( 'seo manager' == $user->type ) {
			$permissions = array(
				'allowed' => array(
					'default' => array(
						'dashboard' => array('index', 'dataentry', 'tip-data', 'switch-application'),
						'auth' => array('login', 'logout', 'index'),
						'escorts-v2' => array('index', 'data'/*, 'edit'*/)
					),
					'seo' => '*',
					'geography' => array(
						'countries' => array('ajax'),
						'regions' => array('index', 'data', 'ajax'),
						'cities' => '*',
						'cityzones' => array('index', 'data', 'ajax')
					),
					'system' => array(
						'static-content' => '*',
						'blacklisted-ips' => '*',
					 )
				),
				'disallowed' => array(
					'geography' => array(
						'cities'	=> array('delete')
					)
				)
			);

			if ( ! $this->hasAccess($request, $permissions)) {
				$request->setModuleName('default');
				$request->setControllerName('error');
				$request->setActionName('permission-denied');
				//die(header('Location: /dashboard'));
			}
		}
		elseif ( 'journal manager' == $user->type ) {
			$permissions = array(
				'allowed' => array(
					'default' => array(
						'dashboard' => array('tip-data', 'switch-application'),
						'journal' => '*',
						'auth' => array('login', 'logout', 'index')
					)
				),
				'disallowed' => array(
				)
			);

			if ( ! $this->hasAccess($request, $permissions)) {
				$request->setModuleName('default');
				$request->setControllerName('error');
				$request->setActionName('permission-denied');
				//die(header('Location: /dashboard'));
			}
		}

		// Zend_Controller_Front::getInstance()->registerPlugin(new Model_Plugin_Acl());
	}
}
