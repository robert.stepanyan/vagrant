<?php

class Model_Activity
{
	/**
	 * @var Zend_Db_Adapter_Abstract
	 */
	protected $_db;
	
	/**
	 * @var Model_Activity
	 */
	protected static $_instance;
	
	public function __construct()
	{
		$this->_db = Zend_Registry::get('db');
	}
	
	/**
	 * @return Model_Activity
	 */
	public static function getInstance()
	{
		if ( empty(self::$_instance) ) {
			self::$_instance = new self();
		}
		
		return self::$_instance;
	}
	
	public function log($action_slug, $params = null)
	{
		$user = Zend_Auth::getInstance()->getIdentity();
		
		$action_id = $this->_db->fetchOne('SELECT id FROM backend_activity_actions WHERE slug = ?', $action_slug);
		
		if ( ! $action_id ) return false; // throw new Exception('Action does not exist in database');
		
		$data = array('backend_user_id' => $user->id, 'action_id' => $action_id);
		
		if ( ! is_null($params) ) {
			$data['params'] = serialize($params);
		}
		
		return $this->_db->insert('backend_activity_log', $data);
	}
}
