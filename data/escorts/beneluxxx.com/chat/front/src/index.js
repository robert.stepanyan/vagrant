import React from 'react'
import {render} from 'react-dom'
import {Provider} from 'react-redux'
import App from './components/App/App'
import store from "./store.js";

// Localization
import i18n from "./i18n/index";

import {SnackbarProvider} from 'notistack';
import Button from "@material-ui/core/Button";
import CloseIcon from '@material-ui/icons/Close';

// add action to all snackbars
const notistackRef = React.createRef();
const onClickDismiss = key => () => notistackRef.current.closeSnackbar(key);

render(
  <Provider store={store}>
    <SnackbarProvider
      ref={notistackRef} maxSnack={3}
      action={(key) => (
        <Button onClick={onClickDismiss(key)}>
          <CloseIcon/>
        </Button>
      )}
    >
      <App/>
    </SnackbarProvider>
  </Provider>,
  document.getElementById('root')
)