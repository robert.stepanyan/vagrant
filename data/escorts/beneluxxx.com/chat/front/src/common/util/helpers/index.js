const Helpers = {

    getThreadGUID(user1, user2) {
      return [parseInt(user1), parseInt(user2)].sort().join('-');
    },

    getPartnerId(roomGUID, id) {
      return roomGUID.replace(new RegExp(`${id}-|-${id}`, "g"), '');
    },

    highlight(text, keywords) {
      return text.replace(new RegExp(keywords, 'gi'), '<span class="matched">$&</span>');
    },

    isDevelopment() {
      return !process.env.NODE_ENV || process.env.NODE_ENV === 'development';
    },

    removeByKey(myObj, deleteKey) {
      return Object.keys(myObj)
        .filter(key => key !== deleteKey)
        .reduce((result, current) => {
          result[current] = myObj[current];
          return result;
        }, {});
    }
  }
;

export default Helpers;