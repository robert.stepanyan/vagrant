import React, {Fragment} from "react";
import Button from "@material-ui/core/Button";
import CloseIcon from "@material-ui/core/SvgIcon/SvgIcon";

const action = key => (
  <Fragment>
    <Button onClick={() => {
      this.props.closeSnackbar(key)
    }}>
      <CloseIcon/>
    </Button>
  </Fragment>
);

export default action;