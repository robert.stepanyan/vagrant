import Helpers from "~/common/util/helpers";

export const APP_HOSTNAME = process.env.REACT_APP_HOSTNAME || "beneluxxx.com";
export const APP_ORIGIN = process.env.REACT_APP_ORIGIN || "http://beneluxxx.com";
export const SOCKET_SERVER_ORIGIN = process.env.REACT_APP_SOCKET_SERVER_ORIGIN || "http://beneluxxx.com";
export const EXPRESS_SERVER_ORIGIN = process.env.REACT_APP_EXPRESS_SERVER_ORIGIN || "http://beneluxxx.com";
export const GENERAL_CHAT_GUID = process.env.REACT_APP_GENERAL_CHAT_GUID || "1";

export const PRIVACY_LEVEL_GENERAL_CHAT =  1;
export const PRIVACY_LEVEL_PRIVATE_GROUP = 2;
export const PRIVACY_LEVEL_DIRECT = 3;
