import * as GlobalConfig from "../common/config/global";
import * as io from 'socket.io-client';

export class SocketService {

  constructor(adapterOptions = {}) {
    this._adapter = null;
    this._user = null;
    this._dispatch = null;
    this.adapterOptions = adapterOptions;
    this.connected = false;
  }

  setDispatcher(args) {
      this._dispatch = args;
  }

  dispatchToStore(data) {
    if (!this.connected || !this._dispatch) return null;
    this._dispatch(data);
  }

  setUser(user) {
    this._user = user;
  }

  getUser() {
    return this._user;
  }

  getAdapter() {
    return this._adapter;
  }

  buildAdapter() {
    return this._adapter = io(GlobalConfig.SOCKET_SERVER_ORIGIN, this.adapterOptions);
  }

  run() {
    this.buildAdapter();

    return this._adapter;
  }
}