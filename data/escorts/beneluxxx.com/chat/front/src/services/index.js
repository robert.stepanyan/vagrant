import axios from "axios";
import * as GLOBAL from "../common/config/global";

export const API = {
  async getApplicationInitState() {
    const url = GLOBAL.APP_ORIGIN + '/chatroom-api/get-initial-state';
    return axios({
      url,
      method: "POST",
      headers: {'Content-Type': 'multipart/form-data'}
    });
  },
  async sendAuthReuqest(params) {
    const url = GLOBAL.APP_ORIGIN + '/private/signin';
    console.log('url', url)
    const data = API._buildRequestBodyFrom({ajax: true, ...params});
    return axios({
      url,
      data,
      method: "POST",
    });
  },
  async sendReport(params) {
    const url = GLOBAL.APP_ORIGIN + '/chatroom-api/report';
    const data = API._buildRequestBodyFrom({ajax: true, ...params});
    return axios({
      url,
      data,
      method: "POST",
      headers: {'Content-Type': 'multipart/form-data'}
    });
  },

  _buildRequestBodyFrom(options) {
    const data = new FormData();
    for (let key in options) {
      data.append(key, options[key]);
    }
    return data;
  }
};
