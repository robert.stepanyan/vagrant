import React, {Component} from "react";
import "./AvailableChatsGroup.scss";

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';

import {ReactComponent as ExpandMore} from "common/icons/expand-more.svg";
import {ReactComponent as ExpandLess} from "common/icons/expand-less.svg";

import * as GlobalConfig from "common/config/global";
import CreateChatroomDialog from "~/components/CreateChatroomDialog/CreateChatroomDialog";
import ChatRow from "~/components/ChatRow/ChatRow";
import {withTranslation} from "react-i18next";

const privateChatActions = {
  SEND_PM: 1,
  INVITE_TO_CHAT: 2,
  REPORT_TO_ADMIN: 4,
  BLOCK: 8,
  KICK_FROM_CHATROOM: 16,
  VIEW_PROFILE: 32
};

const groupChatActions = {
  LEAVE_CHAT: 1
};

class AvailableChatsGroup extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isOpen: this.props.isOpen
    }


    this.onListItemClick = this.onListItemClick.bind(this);
    this.isGroupChat = this.props.groupName.toLowerCase() === 'chatrooms';

  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.isOpen !== this.props.isOpen) {
      this.setState({isOpen: this.props.isOpen})
    }
  }

  toggleItem = () => {
    this.setState({isOpen: !this.state.isOpen})
  }

  getPrivacyLevelOfRoom(room) {
    return parseInt(room.hasOwnProperty('user_type') ? GlobalConfig.PRIVACY_LEVEL_DIRECT : room.privacy_level);
  }

  onListItemClick = (item) => {
    if (!item) return;

	let usernameTemp = item.username || item.title;
    this.props.onSelectItem({
      userId: item.id,
	  username: usernameTemp.replace(/<\/?[^>]+(>|$)/g, ""),
	  privacyLevel: this.getPrivacyLevelOfRoom(item),
      roomGUID: item.guid
    });
  }

  render() {
    const {t} = this.props;
    return <div>
      <ListItem button className={`availableChatsListTitle ${this.state.isOpen ? 'open' : ''}`}
                onClick={this.toggleItem.bind(this)}>
        <ListItemText primary={this.props.groupName}/>
        {this.state.isOpen ? <ExpandLess/> : <ExpandMore/>}
      </ListItem>
      <Collapse in={this.state.isOpen} timeout="auto" unmountOnExit>
        <List className="listWrapper fancyScroller fancyScroller--light" component="div" disablePadding>

          {!this.props.list.length && !this.isGroupChat &&
          <ListItem key={0} className="availableChatsListItem empty">
            <div className="availableChatsListItem--userDetails">
              <span className="empty">{t('no_connected_group_of_users', {groupName: this.props.groupName})}</span>
            </div>
          </ListItem>}

          {this.props.list.map((item, i) => {

            const title = item.username || item.title;
			
            let avatar = <Avatar
              className="userAvatar empty">{title.replace(/<\/?[^>]+(>|$)/g, "")[0] || 'M' /* Meaning its a member :) */}</Avatar>;
            if (item.chat_info && item.chat_info.avatar) {
              avatar = <Avatar className="userAvatar" alt={title} src={item.chat_info.avatar}/>
            }

            let actions = {
              ...privateChatActions
            };

            if (this.isGroupChat) {
              actions = {
                ...groupChatActions
              };
            }

            let isActive = false;

            if (item.hasOwnProperty('user_type')) {
              isActive = this.props.activeChatGuid.includes('-0-') == false &&
                (this.props.activeChatGuid.includes('-' + item.id) || this.props.activeChatGuid.includes(item.id + '-'));
            } else {
              isActive = this.props.activeChatGuid === item.guid;
            }

            return <ChatRow
              key={i}
              userId={item.id}
              onTalkButtonClick={this.onListItemClick.bind(this, item)}
              avatar={avatar}
              unreadMessagesCount={item.unreadMessagesCount}
              title={title}
              city={item.city}
              directChatIcon={false}
              actions={actions}
              isActive={isActive}
              privacyLevel={this.getPrivacyLevelOfRoom(item)}
              onRoomLeave={() => this.props.onRoomLeave(item)}
              onUserKick={this.props.onUserKick}
              onUserBlock={this.props.onUserBlock}
              onUserProfileOpen={() => this.props.onUserProfileOpen(item.chat_info.link)}
            />
          })}

          {/* Create Chatroom button */}
          {this.isGroupChat &&
          <CreateChatroomDialog onChatroomCreate={this.props.onChatroomCreate}/>}
        </List>
      </Collapse>
    </div>
  }

}

export default withTranslation()(AvailableChatsGroup);