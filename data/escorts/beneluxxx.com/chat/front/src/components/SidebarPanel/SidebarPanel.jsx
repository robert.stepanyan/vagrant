import React, {Component} from "react";
import "./SidebarPanel.scss";
import UsersSearchbar from "../UsersSearchbar/UsersSearchbar";
import AvailableChatsGroup from "../AvailableChatsGroup/AvailableChatsGroup";

import Badge from "@material-ui/core/Badge/Badge";
import List from '@material-ui/core/List';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';

import Button from "@material-ui/core/Button";
import {withTranslation} from 'react-i18next';


class SidebarPanel extends Component {

  render() {

    const {t} = this.props;
    const cls = ['sidebarPanel'];
    if (this.props.isOnMobileOpen === false) cls.push('mobileHidden');
	
    let connectedUsersCount = this.props.connectedUsersCount;
    let generalChatUnreadMessages = this.props.generalChatUnreadMessages;
    let escorts = Object.values(this.props.connectedEscorts);
    let agencies = Object.values(this.props.connectedAgencies);
    let members = Object.values(this.props.connectedMembers);

    let chatrooms = Object.values(this.props.chatrooms);
    let recentChats = Object.values(this.props.recentChats);
	
	return <div className={cls.join(' ')}>

      <UsersSearchbar
        onChatroomSearch={this.props.onChatroomSearch}
        onSidebarClose={this.props.onSidebarClose}
        onChatLeave={this.props.onChatLeave}/>

	  <Button className="general-chat-sidebar" onClick={this.props.onGeneralChatSwitch}>
	  <span className="title-text">
		{t('general_chat')} ({connectedUsersCount})
		<Badge className={'badge'} badgeContent={generalChatUnreadMessages} max={999}/>
	  </span>
		
	  </Button>
      
      <div className="availableChatsGroupListWrapper">
        <List component="div" className="availableChatsGroupList fancyScroller fancyScroller--light" disablePadding>

          <AvailableChatsGroup
            activeChatGuid={this.props.activeChatGuid}
            onSelectItem={this.props.onRoomSwitch}
            isOpen={recentChats.length > 0}
            onUserKick={this.props.onUserKick}
            onUserBlock={this.props.onUserBlock}
            onUserProfileOpen={this.props.onUserProfileOpen}
            groupName={t('recent_chats')}
            list={recentChats}/>

          <AvailableChatsGroup
            activeChatGuid={this.props.activeChatGuid}
            onSelectItem={this.props.onRoomSwitch}
            isOpen={escorts.length > 0}
            onUserKick={this.props.onUserKick}
            onUserBlock={this.props.onUserBlock}
            onUserProfileOpen={this.props.onUserProfileOpen}
            groupName={t('escorts')}
            list={escorts}/>

          <AvailableChatsGroup
            activeChatGuid={this.props.activeChatGuid}
            onSelectItem={this.props.onRoomSwitch}
            isOpen={agencies.length > 0}
            onUserKick={this.props.onUserKick}
            onUserBlock={this.props.onUserBlock}
            onUserProfileOpen={this.props.onUserProfileOpen}
            groupName={t('agencies')}
            list={agencies}/>

          <AvailableChatsGroup
            activeChatGuid={this.props.activeChatGuid}
            onSelectItem={this.props.onRoomSwitch}
            onUserKick={this.props.onUserKick}
            onUserBlock={this.props.onUserBlock}
            onUserProfileOpen={this.props.onUserProfileOpen}
            isOpen={members.length > 0}
            groupName={t('members')}
            list={members}/>

          <AvailableChatsGroup
            activeChatGuid={this.props.activeChatGuid}
            onSelectItem={this.props.onRoomSwitch}
            onRoomLeave={this.props.onRoomLeave}
            onChatroomCreate={this.props.onChatroomCreate}
            isOpen={true}
            groupName={t('chatrooms')}
            list={chatrooms}/>
        </List>
      </div>

    </div>
  }
}

export default withTranslation()(SidebarPanel);