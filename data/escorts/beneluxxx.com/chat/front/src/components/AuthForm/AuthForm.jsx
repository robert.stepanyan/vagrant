import React from "react";
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';

import "./AuthForm.scss";
import {withTranslation} from "react-i18next";

class AuthForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {};

    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleClickOpen = () => {
  }

  handleClose = () => {
  }

  handleSubmit(event) {
    event.preventDefault();

    const requestData = {
      username: this.state.username,
      password: this.state.password
    };

    this.props.onAuthRequest(requestData);
  }

  render() {
    const {t} = this.props;

    return <Dialog open={true} onClose={this.handleClose} aria-labelledby="form-dialog-title">
      <form className="authForm" onSubmit={this.handleSubmit.bind(this)}>
        <DialogTitle id="form-dialog-title">{t('sign_in')}</DialogTitle>
        <DialogContent>
          <DialogContentText>
              <span dangerouslySetInnerHTML={{__html: t('already_have_profile_warning')}}></span>
          </DialogContentText>

          <TextField
            autoFocus
            error={!!this.props.errors.username}
            margin="dense"
            id="name"
            name="username"
            label={t('email_address')}
            type="text"
            fullWidth
            helperText={this.props.errors.username}
            onInput={this.handleInputChange}
          />

          <TextField
            error={!!this.props.errors.password}
            margin="dense"
            id="password"
            name="password"
            label={t("password")}
            type="password"
            fullWidth
            helperText={this.props.errors.password}
            onInput={this.handleInputChange}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={this.handleClose} id="submitForm" type="submit" disabled={this.props.isRequestPending}>
            Submit
            {this.props.isRequestPending && <CircularProgress size={24} className="progressIcon"/>}
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  }
}

export default withTranslation()(AuthForm);