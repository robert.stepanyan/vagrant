import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';

import AddIcon from '@material-ui/icons/Add';
import IconButton from '@material-ui/core/IconButton';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItem from '@material-ui/core/ListItem';
import TextField from '@material-ui/core/TextField';
import {useTranslation} from "react-i18next";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function CreateChatroomDialog(props) {

  const {t} = useTranslation();
  const [open, setOpen] = React.useState(false);
  const [name, setName] = React.useState('');
  const [message, setMessage] = React.useState('');

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleInputChange = (event) => {
    const target = event.target;

    if (target.value.length > 0) {
      setMessage('');
    }

    setName(target.value.replace(/[^A-Za-z0-9 _]+/g, ''));
  };

  const submit = () => {
    if (!name.length) {
      return setMessage(t('type_chatroom_name'));
    }

    setMessage('');
    setName('');

    props.onChatroomCreate(name.trim());
    handleClose();
  };

  return (<ListItem className="availableChatsListItem availableChatsListItem--action">
      <div className="actionButton">
        <ListItemIcon onClick={handleClickOpen}>
          <IconButton>
            <AddIcon/>
          </IconButton>
        </ListItemIcon>
      </div>

      <div className="availableChatsListItem--userDetails">
        <span className="username" onClick={handleClickOpen}>{t('create_chatroom')}</span>

        <Dialog
          open={open}
          TransitionComponent={Transition}
          keepMounted
          onClose={handleClose}
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogTitle id="alert-dialog-slide-title">{t('choose_chatroom_name')}</DialogTitle>
          <DialogContent>
            <TextField
              margin="dense"
              placeholder={t('chatroom_name_example')}
              type="text"
              autoComplete="off"
              error={message.length > 0}
              helperText={message}
              onInput={handleInputChange}
              inputProps={{
                'maxLength': 25,
                'id': 'chatroomName',
              }}
              value={name}
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary">{t('cancel')}</Button>
            <Button onClick={submit} color="primary">{t('create')}</Button>
          </DialogActions>
        </Dialog>
      </div>
    </ListItem>
  );
}
