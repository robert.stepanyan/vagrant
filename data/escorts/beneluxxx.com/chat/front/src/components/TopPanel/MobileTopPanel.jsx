import IconButton from "@material-ui/core/IconButton";
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import NotificationsPanelContainer from "containers/NotificationsPanelContainer";
import {ClickAwayListener} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import {ReactComponent as DoubleMessageIcon} from "components/CoversationPanel/partial/double-message-icon.svg";
import React from "react";
import SettingsIcon from '@material-ui/icons/Settings';
import ChatOptionsListContainer from "~/containers/ChatOptionsListContainer";
import Badge from '@material-ui/core/Badge';
import "./TopPanel.scss";
import {useTranslation} from "react-i18next";

const MobileTopPanel = (props) => {

  const {
    onGeneralTabActivate,
    isGeneralChat,
    closeChatroomOptions,
    chatOptionsOpen,
    openChatroomOptions,
    activeChatActions,
    onSidebarOpen,
    totalUnreadMessages,
    generalChatUnreadMessages,
    title
  } = props;

  const {t} = useTranslation();

  const titleBar = (hasOptions) => {
    return (
      <div
        className={`btn-block title-btn ${isGeneralChat ? 'push-left' : ''} ${hasOptions ? 'has-options' : ''}`}
        onClick={openChatroomOptions}>
        {title || t('user_disconnected')}

        {hasOptions && <div className={`action-wrapper activeChatOptionsWrapper ${chatOptionsOpen ? 'open' : ''}`}>
          <Button className={'chatroom-settings-btn'}>
            <SettingsIcon/>
          </Button>

          <ChatOptionsListContainer
            open={chatOptionsOpen}
            actions={activeChatActions}
          />
        </div>}
      </div>)
  };

  return (
    <div className="active-chat-actions-panel">

      <section>
        {!isGeneralChat && <div className={"btn-block back-btn"} onClick={onGeneralTabActivate}>
          <IconButton aria-label="Back to general chat" className={"back-to-general"}>
            <Badge invisible={(generalChatUnreadMessages) <= 0} color="secondary" badgeContent=" " variant="dot">
              <ArrowBackIcon/>
            </Badge>
          </IconButton>
        </div>}

        {Object.keys(activeChatActions).length > 0 ?
          <ClickAwayListener onClickAway={closeChatroomOptions}>
            {titleBar(true)}
          </ClickAwayListener> : titleBar(false)}

      </section>

      <section>
        <div className={"btn-block notification-btn"}>
          <NotificationsPanelContainer/>
        </div>

        <div className={"btn-block sidebar-open-btn"}>
          <IconButton aria-label="delete" onClick={onSidebarOpen}>
            <Badge invisible={(totalUnreadMessages - generalChatUnreadMessages) <= 0} color="secondary" badgeContent=" "
                   variant="dot">
              <DoubleMessageIcon/>
            </Badge>
          </IconButton>
        </div>
      </section>
    </div>
  )
}

export default MobileTopPanel;