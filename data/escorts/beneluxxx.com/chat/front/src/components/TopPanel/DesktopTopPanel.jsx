import IconButton from "@material-ui/core/IconButton";
import ArrowBackIcon from "@material-ui/core/SvgIcon/SvgIcon";
import Badge from "@material-ui/core/Badge/Badge";
import {isMobile} from "react-device-detect";
import NotificationsPanelContainer from "containers/NotificationsPanelContainer";
import {ClickAwayListener} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import {ReactComponent as DoubleMessageIcon} from "components/CoversationPanel/partial/double-message-icon.svg";
import React from "react";
import {ReactComponent as GearIcon} from "~/common/icons/gear-bold.svg";
import ChatOptionsListContainer from "~/containers/ChatOptionsListContainer";
import {useTranslation} from "react-i18next";

const DesktopTopPanel = (props) => {

  const {t} = useTranslation();
  const {
    onGeneralTabActivate,
    isGeneralChat,
    connectedUsersCount,
	activeThreadUsername,
    generalChatUnreadMessages,
    isOptionsPanelAvailable,
    closeChatroomOptions,
    chatOptionsOpen,
    openChatroomOptions,
    activeChatActions,
    onSidebarOpen
  } = props;
  let generalChat = `${t('general_chat')} (${connectedUsersCount})`;
  return (<div className="active-chat-actions-panel">

    <div className="active-chat-title__wrapper">
      <span className={"general-text"}  >{/*onClick={onGeneralTabActivate} */}
	  <strong>{activeThreadUsername ? 'Chat with ' + activeThreadUsername : generalChat } </strong>

        {/*<Badge className={'badge'} badgeContent={generalChatUnreadMessages} max={999}/>*/}
      </span>
    </div>

    <div className="action-wrapper notifications-panel-wrapper">
      <NotificationsPanelContainer/>
    </div>

    {isOptionsPanelAvailable && <ClickAwayListener onClickAway={closeChatroomOptions}>
      <div className={`action-wrapper activeChatOptionsWrapper ${chatOptionsOpen ? 'open' : ''}`}>
        <Button onClick={openChatroomOptions}>
          {t('options')} &nbsp;
          <GearIcon/>
        </Button>

        <ChatOptionsListContainer
          open={chatOptionsOpen}
          actions={activeChatActions}
        />
      </div>
    </ClickAwayListener>}

    <div className="sidebarToggleBtn">
      <IconButton aria-label="delete" onClick={onSidebarOpen}>
        <DoubleMessageIcon/>
      </IconButton>
    </div>

  </div>)
}

export default DesktopTopPanel;