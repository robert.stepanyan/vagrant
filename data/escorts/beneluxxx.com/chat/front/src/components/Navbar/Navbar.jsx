import React from 'react';
import Cookie from "js-cookie";
import {makeStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import "./Navbar.scss";
import {ReactComponent as ExitIcon} from "./partial/exit-icon.svg";
import {APP_ORIGIN} from "../../common/config/global";
import {useTranslation} from "react-i18next";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
}));

export default function DenseAppBar() {
  const classes = useStyles();
  const {t} = useTranslation();

  const exitChat = (e) => {
	Cookie.set('chat_status', 'off');
	window.location.href = APP_ORIGIN
  }
  
  return (
    <div className={classes.root}>
      <AppBar className="navbar" position="static">
        <Toolbar variant="dense">
          <div className="mainLogoWrapper">
            <a href="/"><img className="mainLogo" src="/images/common/logo_main.png" alt={t('main_logo')}/></a>
          </div>

          <div className="exitChatWrapper">
            <a href='#' onClick={exitChat} >{t('exit_chat')}</a>
            <ExitIcon/>
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
}
