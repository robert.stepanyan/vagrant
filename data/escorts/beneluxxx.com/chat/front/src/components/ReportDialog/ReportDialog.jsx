import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from "@material-ui/core/CircularProgress";
import {useSnackbar} from "notistack";
import {useTranslation} from "react-i18next";

export default function ReportDialog(props) {

  const {t} = useTranslation();
  let {open, onClose, onSubmit, isLoading, message, messageType} = props;
  let [reasonText, setReasonText] = React.useState('');
  const {enqueueSnackbar, closeSnackbar} = useSnackbar();

  const handleSubmit = () => {

    if (reasonText.length <= 0) {
      return enqueueSnackbar(t('msg_cannot_be_empty'), {
        preventDuplicate: true,
        variant: "warning",
        anchorOrigin: {
          vertical: 'top',
          horizontal: 'center',
        },
      })
    }

    setReasonText('');
    onSubmit({reason: reasonText});
  };

  const handleClose = () => {

    onClose();
  };

  const handleInputChange = (event) => {
    setReasonText(event.target.value);
  }

  return (
    <div>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">{t('provide_info_about_user')}</DialogTitle>
        <DialogContent>
          {message &&
          <p style={{color: `${messageType === 'success' ? 'green' : 'red'}`, margin: '5px 0'}}>
            {message}
          </p>}

          <TextField
            autoFocus
            id="report-text"
            label={t('description_details')}
            multiline
            rows="4"
            placeholder={t('report_details_example')}
            onChange={handleInputChange}
            value={reasonText}
            disabled={isLoading}
            variant="outlined"
            fullWidth
          />

        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            {t('close')}
          </Button>
          <Button onClick={handleSubmit} color="primary" disabled={isLoading}>
            {t('submit')}
            {isLoading && <CircularProgress size={24} style={{position: 'absolute'}}/>}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
