import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import {ReactComponent as AttachmentIcon} from '~/common/icons/attachment-icon.svg';
import {ReactComponent as SendIcon} from "~/common/icons/send-icon.svg";
import {FilePond, registerPlugin} from 'react-filepond';
import {isMobile} from 'react-device-detect';
import 'filepond/dist/filepond.min.css';

import "./AddAttachmentDialog.scss";

import * as GlobalConfig from "~/common/config/global";

import FilePondPluginImageExifOrientation from 'filepond-plugin-image-exif-orientation';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css';

import FilePondPluginFileValidateType from 'filepond-plugin-file-validate-type';
import TextField from '@material-ui/core/TextField';
import {useSnackbar} from 'notistack';
import {useTranslation} from "react-i18next";

registerPlugin(FilePondPluginImageExifOrientation, FilePondPluginImagePreview, FilePondPluginFileValidateType);

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function AddAttachmentDialog(props) {

  const {t} = useTranslation();
  const textInput = React.createRef(null);
  const [open, setOpen] = React.useState(false);
  const [files, setFiles] = React.useState({});
  const {enqueueSnackbar, closeSnackbar} = useSnackbar();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setFiles({});
  };

  const hasPendingFile = () => {

    for (let key in files) {
      if (files[key]?.status === 'pending') return true;
    }

    return false;
  }

  const filesUploadedSuccessfully = () => {
    return Object.values(files).filter(item => item);
  }

  const handleSubmitClick = () => {

    if (hasPendingFile()) {
      return enqueueSnackbar(t('processing_attachments_wait'), {
        preventDuplicate: true,
        anchorOrigin: {
          vertical: 'bottom',
          horizontal: 'center',
        },
      });
    }

    if (textInput.current.value.length <= 0 && filesUploadedSuccessfully().length <= 0) {
      return enqueueSnackbar(t('cant_send_empty_msg'), {
        preventDuplicate: true,
        variant: 'error',
        anchorOrigin: {
          vertical: 'bottom',
          horizontal: 'center',
        },
      });
    }

    props.onSubmit({
      text: textInput.current.value,
      attachments: Object.values(files)
    });
    handleClose();
  }

  const getFileType = (file) => {
    let types = {
      image: ['image/gif', 'image/jpeg', 'image/pjpeg', 'image/png', 'image/svg+xml', 'image/webp', 'image/tiff', 'image/vnd.microsoft.icon', 'image/vnd.wap.wbmp'],
      video: ['video/mpeg', 'video/mp4', 'video/ogg', 'video/quicktime', 'video/webm', 'video/x-ms-wmv', 'video/x-flv', 'video/3gpp', 'video/3gpp2'],
    };

    for (let key in types) {
      if (types[key].includes(file.fileType.toLowerCase())) return key;
    }

    return 'other';
  }

  const onFileAdd = (error, file) => {
    if (error) return console.error(error);

    setFiles({
      ...files,
      [file.id]: {
        status: 'done',
        url: file.serverId + '.' + file.fileExtension,
        original_name: file.filename,
        type: getFileType(file)
      }
    });
  }

  const onFileProcessStart = (file) => {

    setFiles({
      ...files,
      [file.id]: {
        status: 'pending'
      }
    });
  }

  const onFileRemove = (error, file) => {
    if (error) console.error(error);

    setFiles({
      ...files,
      [file.id]: undefined
    });
  }

  const preventFileDrop = (e) => {
    e.preventDefault();
  };

  return (
    <div className="addAttachmentDialog" onDragOver={preventFileDrop} onDrop={preventFileDrop}>
      <IconButton color="primary" onClick={handleClickOpen} id="attachmentTrigger" aria-label="directions">
        <AttachmentIcon/>
      </IconButton>

      <Dialog fullScreen className="dialog"
              open={open}
              onClose={handleClose}
              TransitionComponent={Transition}>
        <AppBar className="addAttachmentDialog--topBar">
          <Toolbar>
            <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
              <CloseIcon/>
            </IconButton>
            <Typography variant="h6">
              {t('add_attachment')}
            </Typography>
          </Toolbar>
        </AppBar>
        <div className="addAttachmentDialog--content">

          <div className="filesWrapper">
            <FilePond
              allowMultiple={true}
              chunkUploads={true}
              maxFiles={5}
              chunkSize={50000} // 50mb
              allowFileTypeValidation={true}
              acceptedFileTypes={['image/jpg', 'image/jpeg', 'image/png', 'video/*']}
              server={GlobalConfig.EXPRESS_SERVER_ORIGIN + '/attachment/send'}
              onaddfilestart={onFileProcessStart}
              onprocessfile={onFileAdd}
              onerror={onFileRemove}
              onremovefile={onFileRemove}/>
          </div>

          <div className="bottomPanel">
            <div className="formWrapper">
              <TextField
                autoFocus={!isMobile}
                inputRef={textInput}
                id="standard-secondary"
                placeholder={t('write_description') + '...'}
                color="secondary"/>
            </div>

            <div className="submitPanel">
              <div className="submitButtonWrapper">
                <IconButton id="sendBtn" onClick={handleSubmitClick}>
                  <SendIcon/>
                </IconButton>
              </div>
            </div>
          </div>
        </div>
      </Dialog>
    </div>
  );
}
