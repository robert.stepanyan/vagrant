import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import {blue} from '@material-ui/core/colors';
import ErrorIcon from '@material-ui/icons/Error';
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import {useTranslation} from "react-i18next";

const useStyles = makeStyles({
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
});

function ChatroomInviteDialog(props) {

  const classes = useStyles();
  let {onClose, open, roomsList = [], onSelectRoom} = props;
  const {t} = useTranslation();

  const handleListItemClick = room => {
    onSelectRoom(room.id);
  };

  return (
    <Dialog onClose={onClose} aria-labelledby="Chatroom Invitation dialog" open={open}>
      {roomsList.length > 0 && <DialogTitle id="simple-dialog-title">{t('select_room_to_invite')}</DialogTitle>}
      <List>
        {roomsList.length <= 0 && <ListItem key={0}>
          <ListItemAvatar>
            <Avatar className={classes.avatar}>
              <ErrorIcon/>
            </Avatar>
          </ListItemAvatar>
          <ListItemText primary={t('no_rooms_yet')}/>
        </ListItem>}

        {roomsList.length > 0 && roomsList.map((room, i) => (
          <ListItem button onClick={() => handleListItemClick(room)} key={i}>
            <ListItemAvatar>
              <Avatar className={classes.avatar}>
                {room.title.replace(/<\/?[^>]+(>|$)/g, "")[0] || 'R'}
              </Avatar>
            </ListItemAvatar>
            <ListItemText primary={room?.title}/>
          </ListItem>
        ))}
      </List>

      <DialogActions>
        <Button onClick={onClose} color="primary">
          {t('dismiss')}
        </Button>
      </DialogActions>
    </Dialog>
  );
}


export default ChatroomInviteDialog;