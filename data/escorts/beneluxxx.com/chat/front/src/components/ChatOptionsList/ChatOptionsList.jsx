import React from "react";
import Collapse from "@material-ui/core/Collapse";
import PropTypes from "prop-types";
import * as GlobalConfig from "~/common/config/global";
import ChatroomInviteDialogContainer from "containers/ChatroomInviteDialogContainer";
import ReportDialogContainer from "containers/ReportDialogContainer";
import {ReactComponent as SolidTalkIcon} from "~/common/icons/talk-solid.svg";
import {ReactComponent as RoundedPlusIcon} from "~/common/icons/plus-rounded.svg";
import {ReactComponent as WarningIcon} from "~/common/icons/warning.svg";
import {ReactComponent as BlockIcon} from "~/common/icons/block.svg";
import {ReactComponent as OpenDoorIcon} from "~/common/icons/door-open-solid.svg";
import {ReactComponent as EyeIcon} from "~/common/icons/eye.svg";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import {useTranslation} from "react-i18next";

const ChatOptionsList = (props) => {

  let {open, onOptionsToggle} = props;
  const {t} = useTranslation();
  const {targetUserId, onUserKick, onUserBlock, onUserProfileOpen, onTalkButtonClick} = props;
  const [inviteDialogOpen, setInviteDialogOpen] = React.useState(false);
  const [reportDialogOpen, setReportDialogOpen] = React.useState(false);

  let cls = [];
  if (props.privacyLevel === GlobalConfig.PRIVACY_LEVEL_PRIVATE_GROUP) {
    cls.push('group-chat');
  } else {
    cls.push('private-chat');
  }

  if (props.isActive) {
    cls.push('active');
  }

  const toggleOptions = () => {
    onOptionsToggle();
  }

  const handleListClick = () => {
    if (props.privacyLevel === GlobalConfig.PRIVACY_LEVEL_PRIVATE_GROUP) {
      props.onTalkButtonClick();
    }
  };

  const handleUserKick = () => {
    onUserKick(targetUserId);
  }

  const handleUserBlock = () => {
    onUserBlock(targetUserId);
  }

  const handleUserProfileOpen = () => {
    onUserProfileOpen(targetUserId);
  }


  return (<Collapse in={open} className={"options-wrapper"}>

      <div className="options-list">

        {props.actions.SEND_PM && <div className="options-list__item" onClick={onTalkButtonClick}>
          <SolidTalkIcon/>
          <p>{t('send_pm')}</p>
        </div>}

        {props.actions.INVITE_TO_CHAT &&
        <div className="options-list__item" onClick={() => setInviteDialogOpen(true)}>
          <RoundedPlusIcon/>
          <p>{t('invite_to_chatroom')}</p>
        </div>}

        {props.actions.INVITE_TO_CHAT &&
        <ChatroomInviteDialogContainer
          targetUserId={targetUserId}
          open={inviteDialogOpen}
          onClose={() => setInviteDialogOpen(false)}/>}

        {props.actions.REPORT_TO_ADMIN &&
        <div className="options-list__item" onClick={() => setReportDialogOpen(true)}>
          <WarningIcon/>
          <p>{t('report_to_admin')}</p>
        </div>}

        {props.actions.REPORT_TO_ADMIN &&
        <ReportDialogContainer
          open={reportDialogOpen}
          reportedUserId={targetUserId}
          onClose={() => setReportDialogOpen(false)}/>}

        {props.actions.BLOCK && <div className="options-list__item" onClick={handleUserBlock}>
          <BlockIcon/>
          <p>{t('block')}</p>
        </div>}

        {props.actions.KICK_FROM_CHATROOM && <div className="options-list__item" onClick={handleUserKick}>
          <OpenDoorIcon/>
          <p>{t('kick_from_my_chatrooms')}</p>
        </div>}

        {props.actions.VIEW_PROFILE && <div className="options-list__item" onClick={handleUserProfileOpen}>
          <EyeIcon/>
          <p>{t('view_profile')}</p>
        </div>}

        {props.actions.LEAVE_CHAT && <div className="options-list__item" onClick={props.onRoomLeave}>
          <ExitToAppIcon style={{transform: "rotate(-180deg)"}}/>
          <p>{t('leave_chat')}</p>
        </div>}
      </div>
    </Collapse>
  )
}

ChatOptionsList.propTypes = {
  onUserBlock: PropTypes.func
};

export default ChatOptionsList;