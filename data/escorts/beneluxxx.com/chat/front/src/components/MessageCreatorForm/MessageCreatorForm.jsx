import React, {Component} from "react";
import "./MessageCreatorForm.scss";
import 'emoji-mart/css/emoji-mart.css'
import {Emoji, Picker} from 'emoji-mart'
import IconButton from '@material-ui/core/IconButton';
import SentimentSatisfiedOutlinedIcon from '@material-ui/icons/SentimentSatisfiedOutlined';
import Button from '@material-ui/core/Button';
import {ReactComponent as SendIcon} from "../../common/icons/send-icon.svg";
import ContentEditable from 'react-contenteditable';
import {ClickAwayListener} from '@material-ui/core';
import AddAttachmentDialog from '../AddAttachmentDialog/AddAttachmentDialog';
import {withSnackbar} from 'notistack';
import {isMobile} from 'react-device-detect';
import sanitizeHtml from "sanitize-html";
import {withTranslation} from "react-i18next";
import {batch} from "react-redux";

class MessageCreatorForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isEmojiPickerOpen: false,
      messageText: ''
    };

    this.messageMaxLength = 300;
    this.messageBox = React.createRef(null);

    this.snackbarPosition = {
      vertical: isMobile ? 'top' : 'bottom',
      horizontal: isMobile ? 'center' : 'left',
    }
  }

  render() {
    const {t} = this.props;

    return <div>
      <form className="messageCreatorForm" onSubmit={this.onFormSubmit}>

        <div className="messageCreationBlock">

          <ClickAwayListener onClickAway={this.handleEmojiPickerAwayClick}>
            <div className="emojiPickerWrapper">
              <IconButton id="emojiTrigger" onClick={(event) => this.setPickerOpen(true)}
                          aria-label="menu">
                <SentimentSatisfiedOutlinedIcon/>
              </IconButton>

              <Picker set="messenger"
                      darkMode={false}
                      showPreview={false}
                      onSelect={this.onEmojiSelect}
                      include={['people']}
                      i18n={{
                        search: t('search'),
                        clear: t('clear'), // Accessible label on "clear" button
                        notfound: t('no_emoji_found'),
                        skintext: 'Choose your default skin tone',
                        categories: {
                          search: t('search_results'),
                          recent: t('frequently_used'),
                          people: t('smiles_and_ppl'),
                          nature: t('animals_and_nature'),
                          foods: t('food_and_drink'),
                          activity: t('activity'),
                          places: t('travel_and_places'),
                          objects: t('objects'),
                          symbols: t('symbols'),
                          flags: t('flags'),
                          custom: t('custom'),
                        },
                        categorieslabel: 'Emoji categories', // Accessible title for the list of categories
                        skintones: {
                          1: t('default_skin_ton'), // 'Default Skin Tone',
                          2: t('light_skin_ton'), // 'Light Skin Tone',
                          3: t('medium_light_skin_ton'), // 'Medium-Light Skin Tone',
                          4: t('medium_skin_ton'), // 'Medium Skin Tone',
                          5: t('medium_dark_skin_ton'), // 'Medium-Dark Skin Tone',
                          6: t('dark_skin_ton'), // 'Dark Skin Tone',
                        },
                      }}
                      style={{
                        position: 'absolute',
                        zIndex: 2,
                        bottom: '100%',
                        left: '0',
                        display: this.state.isEmojiPickerOpen ? 'block' : 'none'
                      }}
              />
            </div>
          </ClickAwayListener>

          <ContentEditable
            className={'fancyScroller'}
            innerRef={this.messageBox}
            html={this.state.messageText} // innerHTML of the editable div
            disabled={false}       // use true to disable editing
            onChange={this.handleInputChange} // handle innerHTML change
            onKeyDown={this.handleInputKeyDown}
            tagName='div' // Use a custom HTML tag (uses a div by default)
            placeholder={t('type_your_msg_here')}
          />

          <AddAttachmentDialog onSubmit={this.submitMessage}/>
        </div>

        <Button variant="contained" type="submit" id="sendBtn"> <SendIcon/> </Button>
      </form>
    </div>
  }

  handleEmojiPickerAwayClick = () => {
    this.setPickerOpen(false);
  };

  handleInputChange = (event) => {

    let value = event.target.value;
    if (this.state.messageText.length <= 0) {
      value = value.trim();
    }

    this.setState({
      messageText: value
    });
  }

  handleInputKeyDown = (event) => {
    if (event.keyCode === 13) {
      this.onFormSubmit();
      event.preventDefault();
      return false;
    }
  }

  onEmojiSelect = (emoji) => {

    let _emojiHTML = Emoji({
      html: true,
      set: 'messenger',
      emoji: emoji.id,
      size: 18,
    });

    var html = new DOMParser().parseFromString(_emojiHTML, 'text/html').body.firstChild;
    var resultEmojiIcon = `<img class='emoji' style='${html.getAttribute('style')}' />`

    this.setState({
      messageText: this.state.messageText + resultEmojiIcon
    });

    this.messageBox.current.focus()
  }

  setPickerOpen = (open = true) => {
    if (open) {
      this.messageBox.current.focus()
    }
    this.setState({isEmojiPickerOpen: open});
  }

  onFormSubmit = (event = null) => {
    if (event) event.preventDefault();

    if (this.props.isConversationBlocked) {
      return this.props.enqueueSnackbar(this.props.t('conversation_is_blocked'), {
        preventDuplicate: true,
        anchorOrigin: this.snackbarPosition,
      });
    }

    if (this.state.messageText.length <= 0) {
      return this.props.enqueueSnackbar(this.props.t('cant_send_empty_msg'), {
        preventDuplicate: true,
        anchorOrigin: this.snackbarPosition,

      });
    } else if (this.state.messageText.replace(/(<([^>]+)>)/ig, "").length >= this.messageMaxLength) {
      this.props.enqueueSnackbar(this.props.t('too_long_text'), {
        preventDuplicate: true,
        variant: "info",
        anchorOrigin: this.snackbarPosition,
      });
    }

    this.submitMessage({
      text: sanitizeHtml(this.state.messageText.trim(), {
        allowedTags: ['img', 'br'],
        allowedAttributes: {
          'img': ['class', 'style']
        },
      }), // remove trailing space from end
    });
  }

  submitMessage = (options) => {

    batch(() => {
      this.props.submitMessage({
        ...options,
        sentDate: new Date().toISOString(),
        roomGUID: this.props.activeRoomGUID,
        sender: {
          id: this.props.userId
        }
      });

      this.setState({
        messageText: ''
      });
    });
  }

}

export default withSnackbar(withTranslation()(MessageCreatorForm));