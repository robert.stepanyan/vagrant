import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import NotificationsNoneIcon from '@material-ui/icons/NotificationsNone';
import "./Notifications.scss";
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import DeleteIcon from '@material-ui/icons/Delete';
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import IconButton from "@material-ui/core/IconButton";
import ListItemText from "@material-ui/core/ListItemText";
import Slide from '@material-ui/core/Slide';
import InfoIcon from '@material-ui/icons/Info';
import CheckIcon from '@material-ui/icons/Check';
import CircularProgress from "@material-ui/core/CircularProgress";
import Badge from "@material-ui/core/Badge";
import {ReactComponent as UndrawNewsletter} from "~/common/icons/undraw-newsletter-vovu.svg";
import {useTranslation} from "react-i18next";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function NotificationsPanel(props) {

  const {t} = useTranslation();
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);

    props.onOpen();
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleDeleteClick = (data) => {
    props.onInvitationDelete(data)
  };

  const handleAcceptClick = (data) => {
    props.onInvitationAccept(data)
  };

  return (
    <div>
      <Button id={"notifications-btn"} className={"notifications-btn"} onClick={handleClickOpen}>
        <Badge badgeContent={props.unseenInvitationsCount} color="error">
          <NotificationsNoneIcon/>
        </Badge>
      </Button>

      <Dialog
        open={open}
        onClose={handleClose}
        maxWidth={"sm"}
        fullWidth={true}
        TransitionComponent={Transition}
        className={'notifications-dialog'}
      >

        <DialogTitle id="notifications-dialog-title">
          {props.invitationsList.length > 0 ? t('notifications') : ''}
        </DialogTitle>

        <DialogContent className={"notifications-dialog__content"}>
          <List>
            {
              props.isLoading && <div className={"loader-wrapper"}>
                <CircularProgress/>
                <p>{t('please_wait')}...</p>
              </div>
            }

            {
              (!props.isLoading && props.invitationsList.length <= 0) && <div className={"empty-clue"}>
                <UndrawNewsletter/>
                <p style={{padding: "0 15px"}}>
                  {t('no_notifications')}...
                </p>
              </div>
            }

            {props.invitationsList.map((item, i) => {
              return (<ListItem key={i}>
                <ListItemAvatar>
                  <Avatar>
                    <InfoIcon/>
                  </Avatar>
                </ListItemAvatar>
                <ListItemText
                  primary={t('you_have_been_invited', {chatroom: item.title})}
                  secondary={t('invited_by', {username: item.username})}
                />
                <ListItemSecondaryAction>
                  <IconButton edge="end" aria-label="delete" onClick={() => handleDeleteClick(item)}>
                    <DeleteIcon/>
                  </IconButton>

                  <IconButton edge="end" aria-label="accept" onClick={() => handleAcceptClick(item)}>
                    <CheckIcon/>
                  </IconButton>
                </ListItemSecondaryAction>
              </ListItem>)
            })}
          </List>

        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary" autoFocus>
            {t('close')}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
