import React from 'react';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import {connect} from "react-redux";
import "./App.scss";
import MainchatContainer from '../../containers/MainchatContainer';
import Navbar from "../Navbar/Navbar";
import StartupLoader from "../StartupLoader/StartupLoader";
import {requestAppInitialState} from "../../ducks/chatroom/actions";
import CircularProgress from "@material-ui/core/CircularProgress";
import Backdrop from "@material-ui/core/Backdrop/Backdrop";
import {withTranslation} from 'react-i18next';
import {Transition} from "react-transition-group";

class App extends React.Component {

  componentDidMount() {
    this.props.dispatch(requestAppInitialState());
  }

  render() {
    const {t} = this.props;

    return <Router>
      <div className="App">

        {this.props.isGlobalLoaderOn && <Backdrop className="backdrop" open={this.props.isGlobalLoaderOn}>
          <div>
            <CircularProgress color="inherit"/>
            <p>{t('processing')}...</p>
          </div>
        </Backdrop>}

        <Navbar className="navbar"/>

        <Switch>
          <Route path="/">
            {this.props.isLoading ? <StartupLoader/> : <MainchatContainer />}
          </Route>
        </Switch>
      </div>
    </Router>
  }
}

const mapStateToProps = (state) => ({
  isLoading: state.chatroom.isLoading,
  isGlobalLoaderOn: state.chatroom.isGlobalLoaderOn,
  user: state.chatroom.user,
});

export default connect(mapStateToProps)(withTranslation()(App));