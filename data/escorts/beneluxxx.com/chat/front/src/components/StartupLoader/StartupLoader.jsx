import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import CircularProgress from '@material-ui/core/CircularProgress';

import "./StartupLoader.scss";

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
    }
}));

function LinearDeterminate() {

    const classes = useStyles();
    const [completed, setCompleted] = React.useState(0);

    React.useEffect(() => {
        let timer;

        function progress() {
            setCompleted(oldCompleted => {
                if (oldCompleted === 100) {
                    clearTimeout(timer);
                }

                const diff = Math.random() * 10;
                return Math.min(oldCompleted + diff, 100);
            });
        }

        timer = setInterval(progress, Math.pow(navigator?.connection?.downlink || 1, -1) * 400);
        return () => {
            clearInterval(timer);
        };
    }, []);

    return (
        <div className={classes.root}>
            <LinearProgress className="loaderProgressbar" variant="determinate" value={completed} />
        </div>
    );
}

export default class StartupLoader extends React.Component {

    render() {
        return <div className="startupLoaderWrapper">
            <div className="startupLoader--content">
                <div className="primaryLoader">
                    <CircularProgress />
                </div>

                <div className="secondaryLoader">
                    <LinearDeterminate />
                </div>
            </div>
        </div>
    }
}