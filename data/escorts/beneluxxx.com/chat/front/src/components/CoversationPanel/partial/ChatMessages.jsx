import React from "react";
import ChatMessageItem from "components/CoversationPanel/partial/ChatMessageItem";
import LinearProgress from "@material-ui/core/LinearProgress";
import Fade from "@material-ui/core/Fade";
import {ReactComponent as BlockedChatIcon} from "~/common/icons/blocked-chat.svg";
import * as GlobalConfig from "~/common/config/global";
import InfiniteScroll from 'react-infinite-scroller';
import CircularProgress from '@material-ui/core/CircularProgress';
import {v1 as uuidv1} from 'uuid';
import {withTranslation} from 'react-i18next';

const LoaderTemplate = () => {
  return (<div className={"loading-messages-wrapper"}>
    <CircularProgress/>
  </div>)
}

class ChatMessages extends React.Component {

  render() {
    const {t} = this.props;

    const cls = [this.props.activeThreadType];
    if (this.props.isRoomLoding) cls.push('loading');
    if (this.props.isBlocked) cls.push('blocked');

    switch (this.props.activeThreadType) {
      case GlobalConfig.PRIVACY_LEVEL_PRIVATE_GROUP:
        cls.push('group-chat');
        break;
      case GlobalConfig.PRIVACY_LEVEL_DIRECT:
        cls.push('direct-chat');
        break;
      default:
        cls.push('general-chat');
        break;
    }

    var messages = [];

    if (!this.props.isBlocked) {
      this.props.messagesList.map((message, i, arr) => {

        let prevWasSameAuthor = arr[i - 1]?.sender.id === message.sender.id;
        let nextIsSameAuthor = arr[i + 1]?.sender.id === message.sender.id;
        let id = uuidv1();

        messages.push(<ChatMessageItem
          avatar={message.sender.profile?.avatar}
          sentFromCity={message.sender.profile?.location?.city_title}
          text={message.text}
          sentDate={message.sentDate}
          senderId={message.sender.id}
          username={message.sender.username}
          isSelfSent={message.sender.id === this.props.userId}
          attachments={message.attachments}
          prevWasSameAuthor={prevWasSameAuthor}
          nextIsSameAuthor={nextIsSameAuthor}
          key={id}
        />)
      })
    }


    return <div id="chatMessages" ref={this.getNode} className={`chatMessages fancyScroller ${cls.join(' ')} `}>

      <Fade in={this.props.isRoomLoding} timeout={{enter: 0, exit: 320}} unmountOnExit>
        <div className={"spinner-wrapper"}>
          <LinearProgress/>
          <div className={"text-panel"}>
            <span>{t('loading')}...</span>
          </div>
        </div>
      </Fade>

      {this.props.isBlocked && <div className={"blocked-clue"}>
        <div className={"blocked-clue__content"}>
          <div className={"text-wrapper"}>
            <p className={"text-wrapper__title"}>{t('conversation_is_blocked')}</p>
            <div className={"icon-wrapper"}>
              <BlockedChatIcon/>
            </div>
            <p className={"text-wrapper__description"}>
              {
                this.props.canUnlock ? t('conversation_blocked_by_me') :
                  t('conversation_blocked_by_opponent')
              }
            </p>
          </div>

          <div className={"actions-wrapper"}>
            {this.props.canUnlock && <button className={"unlock-btn"} onClick={this.props.onUserUnlock}>
              {t('unlock')}
            </button>}
          </div>
        </div>
      </div>}

      {<Fade in={!this.props.chatDescriptionShowed} timeout={{enter: 0, exit: 150}} unmountOnExit>
        <div className={"chat-description-clue"}>
          <div className={"chat-description-clue__content"}>
            <div className={"text-wrapper"}>
              <p className={"text-wrapper__title"}>{t('welcome_to_chat')}</p>
              <p className={"text-wrapper__description"} dangerouslySetInnerHTML={{__html: t('chat_description')}}></p>
            </div>

            <div className={"actions-wrapper"}>
              <button className={"start-chatting-btn"} onClick={this.props.onChatDescriptionClose}>
                {t('start_chatting')}
              </button>
            </div>
          </div>
        </div>
      </Fade>}

      <InfiniteScroll
        pageStart={0}
        loadMore={this.props.loadMessages}
        hasMore={this.props.hasMoreMessages}
        isReverse={true}
        useWindow={false}
        threshold={100}
        className={'messages-list'}
        loader={<LoaderTemplate key={uuidv1()}/>}>
        {messages}
      </InfiniteScroll>

    </div>
  }

  static refs = {
    chatMessages: null
  };

  static scrollToEnd() {
    var objDiv = document.getElementById("chatMessages");
    objDiv.scrollTop = objDiv.scrollHeight;
  }

  getNode(node) {
    if (node) {
      let id = node.getAttribute('id');
      ChatMessages.refs[id] = node;
    }
  }

  componentDidMount() {
    ChatMessages.scrollToEnd();
  }

  getSnapshotBeforeUpdate() {
    const elm = ChatMessages.refs.chatMessages;
    const epsilon = 57;
    const isScrolledTillEnd =
      (elm.scrollHeight - elm.scrollTop) <= (elm.clientHeight + epsilon) &&
      (elm.scrollHeight - elm.scrollTop) >= (elm.clientHeight - epsilon);

    if (isScrolledTillEnd === true) {
      return {action: 'scroll-to-end'};
    }

    return null;
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (snapshot !== null) {

      if (snapshot.action) {
        switch (snapshot.action) {
          case 'scroll-to-end':
            ChatMessages.scrollToEnd();
            break;
          default:
            return;
        }
      }
    }
  }
}


export default withTranslation()(ChatMessages);

