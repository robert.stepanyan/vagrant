import React from "react";
import Moment from 'react-moment';
import * as GlobalConfig from "~/common/config/global";

import {Player} from 'video-react';
import {isMobile} from 'react-device-detect';
import 'video-react/dist/video-react.css';

import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css';
import {v1 as uuidv1} from 'uuid';
import {ReactComponent as ZoomIcon} from "~/common/icons/zoom-with-plus.svg";
import GetAppIcon from '@material-ui/icons/GetApp';
import ReactImageFallback from "react-image-fallback";

const AttachmentsList = (props) => {

  const [open, setOpen] = React.useState(false);
  const [photoIndex, setPhotoIndex] = React.useState(0);
  const {list} = props;
  const imageUrls = [];

  const handleMovePrevImage = () => {
    setPhotoIndex((photoIndex + imageUrls.length - 1) % imageUrls.length)
  };

  const handleMoveNextImage = () => {
    setPhotoIndex((photoIndex + 1) % imageUrls.length)
  };

  function handleOpenViewer(event) {
    let index = event.currentTarget.getAttribute('data-index');
    setPhotoIndex(index);
    setOpen(true);
  };

  const handleCloseViewer = () => {
    setOpen(false);
  };

  const getFileUrl = (url) => {
    return GlobalConfig.EXPRESS_SERVER_ORIGIN + '/attachments/' + url;
  };

  const videos = list.filter(file => file?.type === 'video');
  const images = list.filter(file => file?.type === 'image');
  const others = list.filter(file => file?.type === 'other');

  return (<div className="attachmentsWrapper">

    {images.length > 0 && <div className="images">
      {images.map((file, i) => {
        imageUrls.push(getFileUrl(file.url));
        return <div key={uuidv1()} className={`content`} data-index={i} onClick={handleOpenViewer}>
          <span className={'overlay-zoom'}><ZoomIcon/></span>
          <img src={getFileUrl(file.url)}/>
        </div>
      })}
    </div>}

    {videos.length > 0 && <div className="videos">
      {videos.map(file => {
        return <div key={uuidv1()} className={`content`}>
          <Player>
            <source src={getFileUrl(file.url)}/>
          </Player>
        </div>
      })}
    </div>}

    {others.length > 0 && <div className="others">
      {others.map(file => {
        return <div key={uuidv1()} className={`content`}>
          <a href={getFileUrl(file.url)} download={file.original_name} target="_blank">
            <div className="textWrapper">
              <span className="downloadFileName">{file.original_name}</span>
            </div>
            <GetAppIcon/>
          </a>
        </div>
      })}
    </div>}

    {open && imageUrls[photoIndex] && (
      <Lightbox
        mainSrc={imageUrls[photoIndex]}
        nextSrc={imageUrls[(photoIndex + 1) % imageUrls.length]}
        prevSrc={imageUrls[(photoIndex + imageUrls.length - 1) % imageUrls.length]}
        onCloseRequest={handleCloseViewer}
        onMovePrevRequest={handleMovePrevImage}
        onMoveNextRequest={handleMoveNextImage}
      />
    )}
  </div>)
};

const ChatMessageItem = (props) => {

    const {prevWasSameAuthor, nextIsSameAuthor, isSelfSent} = props;

    let tzOffset = new Date().getTimezoneOffset();
    let sentDate = new Date(props.sentDate);

    if(props.sentDate.includes('Z') === false) {
      sentDate.setHours(sentDate.getHours() - tzOffset / 60);
    }

    let sentDateFormat = 'YYYY/MM/DD HH:mm';
    const today = new Date();
    const diffDays = Math.ceil(Math.abs(today - sentDate) / (1000 * 60 * 60 * 24));

    if (diffDays <= 1) { // less than one day
      sentDateFormat = 'HH:mm';
    } else if (diffDays >= 1 * 368) { // More than one year
      sentDateFormat = 'HH:mm, MMM DD, YYYY';
    } else if (diffDays > 1) { // More than one day
      sentDateFormat = 'HH:mm, MMM DD';
    }

    // Generating classes must be on this message item
    // ----------------------------
    const classNames = ['chatMessageItem'];
    if (isSelfSent) classNames.push('selfSent');
    if (prevWasSameAuthor) classNames.push('prev-same-author');
    if (nextIsSameAuthor) classNames.push('next-same-author');

    return (
      <div className={classNames.join(' ')}>

        {props.text.length > 0 && <div className="messageContent">
          {!prevWasSameAuthor &&
          <img className={'messageSenderAvatar'} src={props.avatar} />}
          <div className="messageDetails">
            <div className="messageSenderDetails">
              <span className="senderName">{props.username}</span>
              <span className="senderCity">{props.sentFromCity}</span>
              {!isMobile && !prevWasSameAuthor && <span className="sentDate">
              {props.sentFromCity && ', '}
                <Moment format={sentDateFormat}>{sentDate}</Moment>
            </span>}
            </div>
            <div className="messageBody" dangerouslySetInnerHTML={{__html: props.text}}/>
          </div>
        </div>}
        {props.attachments?.length > 0 && <AttachmentsList list={props.attachments}/>}
        {isMobile && !nextIsSameAuthor && <span className="sentDate"><Moment format={sentDateFormat}>{sentDate}</Moment></span>}
      </div>
    )
  }
;

export default ChatMessageItem;