import React, {Component} from "react";
import "./ConversationPanel.scss";
import {isMobile} from 'react-device-detect';

import MessageCreatorFormContainer from "~/containers/MessageCreatorFormContainer";
import ChatMessagesContainer from "~/containers/ChatMessagesContainer";
import * as GlobalConfig from "~/common/config/global";
import MobileTopPanel from "~/components/TopPanel/MobileTopPanel";
import DesktopTopPanel from "~/components/TopPanel/DesktopTopPanel";

class ConversationPanel extends Component {

  constructor(props) {
    super(props);

    this.state = {
      chatOptionsOpen: false
    }
  }

  openChatroomOptions = () => {
    this.setState({chatOptionsOpen: true});
  }

  closeChatroomOptions = () => {
    this.setState({chatOptionsOpen: false});
  }

  render() {
    let isGeneralChat = this.props.activeThreadType === GlobalConfig.PRIVACY_LEVEL_GENERAL_CHAT;
    let isOptionsPanelAvailable = this.props.activeThreadType && !isGeneralChat;

    if (!isMobile) {
      if (this.props.activeThreadType === GlobalConfig.PRIVACY_LEVEL_PRIVATE_GROUP) {
        isOptionsPanelAvailable = false
      }
    }

    let activeChatActions = {};
    switch (this.props.activeThreadType) {
      case GlobalConfig.PRIVACY_LEVEL_PRIVATE_GROUP:
        activeChatActions = {
          LEAVE_CHAT: 1
        };
        break;
      case GlobalConfig.PRIVACY_LEVEL_DIRECT:
        activeChatActions = {
          INVITE_TO_CHAT: 2,
          REPORT_TO_ADMIN: 4,
          BLOCK: 8,
          KICK_FROM_CHATROOM: 16,
          VIEW_PROFILE: 32
        };
        break;
    }

    const cls = ['conversationPanel'];
    if (this.props.isOnMobileOpen === false) cls.push('mobileHidden');


    return <div className={cls.join(' ')}>

      {isMobile ? <MobileTopPanel connectedUsersCount={this.props.connectedUsersCount}
                                  closeChatroomOptions={this.closeChatroomOptions}
                                  isOptionsPanelAvailable={isOptionsPanelAvailable}
                                  activeChatActions={activeChatActions}
                                  title={this.props.title}
                                  chatOptionsOpen={this.state.chatOptionsOpen}
                                  openChatroomOptions={this.openChatroomOptions}
                                  isGeneralChat={isGeneralChat}
                                  generalChatUnreadMessages={this.props.generalChatUnreadMessages}
                                  totalUnreadMessages={this.props.totalUnreadMessages}
                                  onSidebarOpen={this.props.onSidebarOpen}
                                  onGeneralTabActivate={this.props.onGeneralTabActivate}/> :
        <DesktopTopPanel connectedUsersCount={this.props.connectedUsersCount}
                         isOptionsPanelAvailable={isOptionsPanelAvailable}
                         activeChatActions={activeChatActions}
						 activeThreadUsername={this.props.activeThreadUsername}
                         chatOptionsOpen={this.state.chatOptionsOpen}
                         openChatroomOptions={this.openChatroomOptions}
                         closeChatroomOptions={this.closeChatroomOptions}
                         generalChatUnreadMessages={this.props.generalChatUnreadMessages}
                         isGeneralChat={isGeneralChat}
                         onGeneralTabActivate={this.props.onGeneralTabActivate}/>}

      <div className={`activeChatMessagesPanel`}>
        <ChatMessagesContainer/>
      </div>

      <div className="messageCreatorFormPanel">
        <MessageCreatorFormContainer/>
      </div>

    </div>
  }
}

export default ConversationPanel;