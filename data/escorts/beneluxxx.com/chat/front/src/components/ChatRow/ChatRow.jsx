import React from "react";
import * as GlobalConfig from "~/common/config/global";
import ListItem from "@material-ui/core/ListItem";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Fab from "@material-ui/core/Fab";
import {ReactComponent as TalkIcon} from "~/common/icons/talk.svg";
import {ReactComponent as GearIcon} from "~/common/icons/gear.svg";
import CloseIcon from '@material-ui/icons/Close';
import Collapse from "@material-ui/core/Collapse";
import {ReactComponent as SolidTalkIcon} from "~/common/icons/talk-solid.svg";
import {ReactComponent as RoundedPlusIcon} from "~/common/icons/plus-rounded.svg";
import ChatroomInviteDialogContainer from "containers/ChatroomInviteDialogContainer";
import {ReactComponent as WarningIcon} from "~/common/icons/warning.svg";
import {ReactComponent as BlockIcon} from "~/common/icons/block.svg";
import {ReactComponent as OpenDoorIcon} from "~/common/icons/door-open-solid.svg";
import {ReactComponent as EyeIcon} from "~/common/icons/eye.svg";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import ReportDialogContainer from "containers/ReportDialogContainer";
import {useTranslation} from "react-i18next";
import {isMobile} from 'react-device-detect';

const ChatRow = (props) => {

  const {t} = useTranslation();
  const {avatar, unreadMessagesCount, title, city, userId, onUserKick, onUserBlock, onUserProfileOpen} = props;
  const [open, setOpen] = React.useState(false);
  const [inviteDialogOpen, setInviteDialogOpen] = React.useState(false);
  const [reportDialogOpen, setReportDialogOpen] = React.useState(false);

  let cls = [];
  if (props.privacyLevel === GlobalConfig.PRIVACY_LEVEL_PRIVATE_GROUP) {
    cls.push('group-chat');
  } else {
    cls.push('private-chat');
  }

  if (props.isActive) {
    cls.push('active');
  }

  const toggleOptions = () => {
    setOpen(!open);
  }

  const handleListClick = () => {
    if (props.privacyLevel === GlobalConfig.PRIVACY_LEVEL_PRIVATE_GROUP) {
      props.onTalkButtonClick();
    } else {
      if (isMobile) toggleOptions();
      else props.onTalkButtonClick();
    }
  };

  const handleUserKick = () => {
    onUserKick(userId);
  };

  const handleUserBlock = () => {
    onUserBlock(userId);
  };

  const handleUserProfileOpen = () => {
    onUserProfileOpen(userId);
  };

  const handleOptionsListAwayClick = () => {
    setOpen(false);
  }

  return (
    <ListItem className={`availableChatsListItem ${open ? 'open' : ''} ${cls.join(' ')}`}>

      <div className="availableChatsListItem--userAvatar" onClick={handleListClick}>
        {avatar}
        {unreadMessagesCount > 0 && <span className="unreadMessagesBadge">{unreadMessagesCount}</span>}
      </div>

      <div className="availableChatsListItem--userDetails" onClick={handleListClick}>
        <span title={title} className="username" dangerouslySetInnerHTML={{__html: title}}></span>
        <span className="userCity">{city}</span>
      </div>

      {!isMobile && <ClickAwayListener onClickAway={handleOptionsListAwayClick}>
        <div className="actions-wrapper">
          {!open && props.actions.SEND_PM &&
          <Fab aria-label="talk" onClick={props.onTalkButtonClick}>
            <TalkIcon/>
          </Fab>}

          {!open && <Fab aria-label="options" onClick={toggleOptions}>
            <GearIcon/>
          </Fab>}

          {open && <Fab className={"btn--close"} aria-label="options" onClick={() => setOpen(false)}>
            <CloseIcon/>
          </Fab>}
        </div>
      </ClickAwayListener>}

      <Collapse in={open}>
        <div className={"options-wrapper"}>
          <div className="options-list">

            {props.actions.SEND_PM && <div className="options-list__item" onClick={props.onTalkButtonClick}>
              <SolidTalkIcon/>
              <p>{t('send_pm')}</p>
            </div>}

            {props.actions.INVITE_TO_CHAT &&
            <div className="options-list__item" onClick={() => setInviteDialogOpen(true)}>
              <RoundedPlusIcon/>
              <p>{t('invite_to_chatroom')}</p>
            </div>}

            {props.actions.INVITE_TO_CHAT &&
            <ChatroomInviteDialogContainer
              targetUserId={userId}
              open={inviteDialogOpen}
              onClose={() => setInviteDialogOpen(false)}/>}

            {props.actions.REPORT_TO_ADMIN &&
            <div className="options-list__item" onClick={() => setReportDialogOpen(true)}>
              <WarningIcon/>
              <p>{t('report_to_admin')}</p>
            </div>}

            {props.actions.REPORT_TO_ADMIN &&
            <ReportDialogContainer
              open={reportDialogOpen}
              reportedUserId={props.userId}
              onClose={() => setReportDialogOpen(false)}/>}

            {props.actions.BLOCK && <div className="options-list__item" onClick={handleUserBlock}>
              <BlockIcon/>
              <p>{t('block')}</p>
            </div>}

            {props.actions.KICK_FROM_CHATROOM && <div className="options-list__item" onClick={handleUserKick}>
              <OpenDoorIcon/>
              <p>{t('kick_from_my_chatrooms')}</p>
            </div>}

            {props.actions.VIEW_PROFILE && <div className="options-list__item" onClick={onUserProfileOpen}>
              <EyeIcon/>
              <p>{t('view_profile')}</p>
            </div>}

            {props.actions.LEAVE_CHAT && <div className="options-list__item" onClick={props.onRoomLeave}>
              <ExitToAppIcon style={{transform: "rotate(-180deg)"}}/>
              <p>{t('leave_chat')}</p>
            </div>}

          </div>
        </div>
      </Collapse>

    </ListItem>);

}

export default ChatRow;