import React from "react";
import clsx from 'clsx';
import {makeStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import SearchIcon from '@material-ui/icons/Search';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import "./UsersSearchbar.scss";
import {ReactComponent as ExitIcon} from "../Navbar/partial/exit-icon.svg";
import {isMobile} from 'react-device-detect';
import {useTranslation} from "react-i18next";

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  }
}));


function UsersSearchbar(props) {

  const {t} = useTranslation();
  const classes = useStyles();
  const [searchedKeyword, setSearchedKeyword] = React.useState('');

  let _typingInterval = null;

  const handleInputChange = (event) => {
    const target = event.target;

    if (_typingInterval) clearInterval(_typingInterval);

    _typingInterval = setTimeout(() => {
      setSearchedKeyword(target.value.trim());

      if (!isMobile) props.onChatroomSearch(target.value.trim());
    }, 360);
  }

  const handleKeyDown = (event) => {
    if (event.keyCode === 13 && isMobile) {
      props.onChatroomSearch(searchedKeyword);
    }
  }

  const handleSearchButtonClick = (event) => {
    props.onChatroomSearch(searchedKeyword);
  }

  return <div className="usersSearchbar">

    <div className="mobileSidebarTopPanel mobileOnly">
      <div className="mobileSidebarTopPanel--content">
        <IconButton className="exitChatBtn" onClick={props.onChatLeave} aria-label="exit">
          <ExitIcon/> {t('exit')}
        </IconButton>

        <span className="mobileSidebarTopPanel--title">
                    {t('benelux_chat_list')}
                </span>

        <IconButton className="closeChatBtn" onClick={props.onSidebarClose} aria-label="close">
          <CloseIcon/>
        </IconButton>
      </div>

    </div>

    <div className="mobileSidebarFormPanel">
      <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined">
        <InputLabel htmlFor="usersSearchInput">{t('search_for_someone')}</InputLabel>
        <OutlinedInput
          id="usersSearchInput"
          onInput={handleInputChange}
          onKeyDown={handleKeyDown}
          autoComplete="off"
          endAdornment={
            <InputAdornment position="end">
              <SearchIcon/>
            </InputAdornment>
          }
          labelWidth={165}
        />
      </FormControl>

      <Button className="mobileOnly" id="mobileAvailableChatsSearchTrigger" onClick={handleSearchButtonClick}>
        <SearchIcon/>
      </Button>
    </div>
  </div>
}

export default UsersSearchbar;