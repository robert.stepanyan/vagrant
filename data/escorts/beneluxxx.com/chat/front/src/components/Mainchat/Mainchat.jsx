import React from 'react';
import "./Mainchat.scss";
import ConversationContainer from "../../containers/ConversationContainer";
import SidebarContainer from "../../containers/SidebarContainer";

export default function Mainchat(props) {

  return <div className="Mainchat">
    <ConversationContainer/>
    <SidebarContainer/>
  </div>
}