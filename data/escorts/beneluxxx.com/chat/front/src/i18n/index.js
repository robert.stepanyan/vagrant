import i18n from 'i18next';
import {initReactI18next} from 'react-i18next';
import Cookies from "js-cookie";
import {Locals} from "./locals";

i18n
  .use(initReactI18next)
  .init({
    lng: Cookies.get('ln'),
    fallbackLng: 'en',
    debug: true,

    interpolation: {
      escapeValue: false, // not needed for react as it escapes by default
    },
    resources: {
      en: {translation: Locals.en},
      fr: {translation: Locals.fr},
      nl: {translation: Locals.nl},
    },

    // special options for react-i18next
    // learn more: https://react.i18next.com/components/i18next-instance
    react: {
      wait: true,
    },
  });

export default i18n;