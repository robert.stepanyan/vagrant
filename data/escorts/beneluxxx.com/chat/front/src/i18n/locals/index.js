import en from "./en";
import fr from "./fr";
import nl from "./nl";

export const Locals = {
  en, fr, nl
};