import * as ActionTypes from "./types";

export const openAvailableChats = () => ({
  type: ActionTypes.OPEN_AVAILABLE_CHATS
});

export const closeAvailableChats = () => ({
  type: ActionTypes.CLOSE_AVAILABLE_CHATS
});

export const updateConnectedUsersList = ({usersList}) => {
  return {
    type: ActionTypes.CONNECTED_USERS_LIST_UPDATE,
    usersList
  }
};

export const updateInitialState = ({threadsUnreadMessages}) => {
  return {
    type: ActionTypes.UPDATE_INITIAL_STATE,
    threadsUnreadMessages
  }
};

export const openDirectMessage = ({userType, userId}) => {
  return {
    type: ActionTypes.DIRECT_MESSAGE_OPEN,
    userType,
    userId
  }
};

export const readMessages = ({user, roomGUID}) => {
  return {
    type: ActionTypes.READ_MESSAGES,
    user,
    roomGUID
  }
};

export const sendComposedMessage = (message) => {
  return {
    type: ActionTypes.COMPOSED_MESSAGE_SEND,
    message
  }
};

export const blockPage = () => {
  return {
    type: ActionTypes.BLOCK_PAGE
  }
};

export const receivedComposedMessage = (message) => {
  return {
    type: ActionTypes.COMPOSED_MESSAGE_RECEIVED,
    message
  }
};

export const receivedDM = (message) => {
  return {
    type: ActionTypes.DIRECT_MESSAGE_RECEIVED,
    message
  }
};

export const requestMessagesChunk = ({roomGUID}) => {
  return {
    type: ActionTypes.MESSAGES_CHUNK_REQUEST,
    roomGUID
  }
};

export const switchRoom = ({roomGUID, username, privacyLevel}) => {
  return {
    type: ActionTypes.SWITCH_ROOM,
    roomGUID,
	username,
    privacyLevel
  }
};

export const newMessagesChunk = ({roomGUID, messages, hasMore}) => {
  return {
    type: ActionTypes.NEW_MESSAGES_CHUNK,
    roomGUID, messages, hasMore
  }
};

export const setGlobalLoader = ({show}) => {
  return {
    type: ActionTypes.SET_GLOBAL_LOADER,
    show
  }
};

export const updateRoomHistory = ({thread, messages}) => {
  return {
    type: ActionTypes.ROOM_HISTORY_UPDATE,
    thread,
    messages
  }
};

export const updateChatroomList = (list) => {
  return {
    type: ActionTypes.CHATROOM_LIST_UPDATE,
    chatrooms: list
  }
};

export const updateUnreadMessages = (unreadMessages) => {
  return {
    type: ActionTypes.UNREAD_MESSAGES_UPDATE,
    unreadMessages: unreadMessages
  }
};

export const updateRecentChats = (list) => {
  return {
    type: ActionTypes.RECENT_CHATS_UPDATE,
    recentChats: list
  }
};

export const setChatroomsVisibilityFilter = (filter) => {
  return {
    type: ActionTypes.CHATROOMS_VISIBILITY_FILTER_SET,
    filter
  }
};

export const leaveRoom = ({guid, id}) => {
  return {
    type: ActionTypes.ROOM_LEAVE,
    guid, id
  }
};

export const sendWssMessage = (eventName, payload, cb = null) => {
  return {
    type: ActionTypes.SEND_WSS_MESSAGE,
    eventName,
    payload,
    cb
  }
};


export const updateInvitationsList = ({list, operation, requested}) => {
  return {
    type: ActionTypes.INVITATIONS_LIST_UPDATE,
    list,
    operation,
    requested
  }
};

export const updateUnseenInvitations = ({count}) => {
  return {
    type: ActionTypes.UNSEEN_INVITATIONS_UPDATE,
    count
  }
};

export const deleteInvitation = ({id}) => {
  return {
    type: ActionTypes.INVITATION_DELETE,
    id
  }
};


export const requestAppInitialState = () => {
  return {
    type: ActionTypes.APP_INITIAL_STATE_REQUEST
  }
};

export const requestAuth = (data) => {
  return {
    type: ActionTypes.AUTH_REQUEST,
    username: data.username,
    password: data.password
  }
};

export const updateAuthData = (user) => {
  return {
    type: ActionTypes.AUTH_DATA_UPDATE,
    user
  }
};

export const updateNotifications = ({action}) => {
  return {
    type: ActionTypes.UPDATE_NOTIFICATIONS,
    action
  }
};

export const sendReportRequest = ({senderId, reportedUserId, reason}) => {
  return {
    type: ActionTypes.REPORT_REQUEST,
    senderId,
    reportedUserId,
    reason
  }
};

export const gotKickedFromRooms = ({rooms}) => {
  return {
    type: ActionTypes.GOT_KICKED,
    rooms
  }
};

export const updateBlockedChat = ({roomGUID, blockedBy}) => {
  return {
    type: ActionTypes.TOGGLE_CHAT_BLOCK_STATE,
    roomGUID, blockedBy
  }
};