import * as ActionTypes from "~/ducks/chatroom/types";
import {call, fork, put, takeEvery} from 'redux-saga/effects';
import {API} from "services";
import UIfx from 'uifx';
import bellAudio from '~/common/sounds/pull-out.mp3';
import {delay, take} from "@redux-saga/core/effects";
import {SocketService} from "~/services/SocketService";
import {eventChannel} from 'redux-saga';

import {
  blockPage,
  gotKickedFromRooms,
  newMessagesChunk,
  receivedComposedMessage,
  sendComposedMessage,
  updateBlockedChat,
  updateChatroomList,
  updateUnreadMessages,
  updateConnectedUsersList,
  updateInitialState,
  updateInvitationsList,
  updateRecentChats,
  updateRoomHistory,
  updateUnseenInvitations
} from "~/ducks/chatroom/actions";

const _bell = new UIfx(bellAudio);
const _socketService = new SocketService({
  transports: ['websocket', 'polling'],
  reconnection: true,
  reconnectionDelay: 1000,
  secure: true,
  path: '/chatroom/socket.io'
});

// Send message
// -------------------------------------
function* sendMessage() {
  const resp = yield call(sendComposedMessage);

  if (resp.status === 200) {
    yield put({type: ActionTypes.COMPOSED_MESSAGE_SEND_SUCCESS, payload: resp.data});
  } else {
    yield put({type: ActionTypes.COMPOSED_MESSAGE_SEND_FAILURE});
  }
}

function* watchSendMessage() {
  yield takeEvery(ActionTypes.COMPOSED_MESSAGE_SEND, sendMessage);
}

/**
 * Creating socketIO instance and establishing connection.
 */
function connect() {
  const socket = _socketService.run();

  return new Promise(resolve => {
    socket.on('connect', () => {
      _socketService.connected = true;
      resolve(socket);
    });
  });
}

function subscribe(socket) {
  return eventChannel(emit => {

    const user = _socketService.getUser();
    _socketService.setDispatcher(emit);

    socket.emit('new-connection', {user});

    socket.on('reconnect', (data) => {
      _socketService.connected = true;
      socket.emit('new-connection', {user});
    });

    socket.on('disconnect', (data) => {
      console.log('disconnect', data);
    });

    socket.on('chat-blocked', (data) => {
      window.location.reload();
    });

    socket.on('force-disconnect', ({reason}) => {
      _socketService.connected = false;

      switch (reason) {
        case 'duplicate-tab':
          emit(blockPage({reason}));
          break;
      }
    });

    socket.on('connected-users-list', (usersList) => {
      _socketService.dispatchToStore(updateConnectedUsersList({usersList}))
    });

    socket.on('update-chatroom-state', (data) => {
      //console.log("INITIAL STATE OF CHAT")
      //console.log(data);
      if (!data) return;

      if (data.recentChats) {
        _socketService.dispatchToStore(updateRecentChats(data.recentChats));
      }

      if (data.chatrooms) {
        _socketService.dispatchToStore(updateChatroomList(data.chatrooms));
      }

	  if(data.unreadMessages){
		_socketService.dispatchToStore(updateUnreadMessages(data.unreadMessages));
	  }
	  
      if (data.unseenInvitationsCount) {
        _socketService.dispatchToStore(updateUnseenInvitations({
          count: data.unseenInvitationsCount
        }));
      }

      /*if (data.unreadMessages) {
        _socketService.dispatchToStore(updateInitialState({
          threadsUnreadMessages: data.unreadMessages
        }));
      }*/
    });

	socket.on('new-message', (message) => {
      if (_socketService.connected) _bell.play();
      _socketService.dispatchToStore(receivedComposedMessage(message));
    });
	
    socket.on('new-recent-chat', (recentChats) => {
        _socketService.dispatchToStore(updateRecentChats(recentChats));
    });

    socket.on('room-history', (data) => {
      _socketService.dispatchToStore(updateRoomHistory(data));
    });

    socket.on('messages-chunk', ({roomGUID, messages, hasMore}) => {
      _socketService.dispatchToStore(newMessagesChunk({roomGUID, messages, hasMore}));
    });

    socket.on('update-chatrooms-list', ({chatrooms}) => {
      _socketService.dispatchToStore(updateChatroomList({chatrooms}));
    });

    socket.on('join-to-chatroom', ({room}) => {
      _socketService.dispatchToStore(updateChatroomList({
        [room.guid]: room
      }));
    });

    socket.on('kicked-from-rooms', (rooms) => {
      _socketService.dispatchToStore(gotKickedFromRooms({rooms}));
    });

    socket.on('update-blocked-chat', ({roomGUID, blockedBy}) => {
      _socketService.dispatchToStore(updateBlockedChat({roomGUID, blockedBy}));
    });

    socket.on('chatroom-invitation', ({roomGUID, blockedBy}) => {
      _socketService.dispatchToStore(updateInvitationsList({operation: 'increment'}));
    });

    return () => {
    };
  });
}

/**
 * Whenever user dispatches some action, this method
 * takes the description of that action, and sends
 * data to server via WSS protocol
 * @param {*} socket
 */
function* write(socket) {
  while (true) {
    const {eventName, payload, cb} = yield take(ActionTypes.SEND_WSS_MESSAGE);
    socket.emit(eventName, payload, cb);
  }
}

/**
 * This action is responsible for listening to
 * events from socket server.
 * @param {*} socket
 */
function* read(socket) {
  const channel = yield call(subscribe, socket);
  while (true) {
    let action = yield take(channel);
    yield put(action);
  }
}

/**
 * Starting to listen for "emit" and "on" actions for socketIO messages
 * @param {*} socket
 */
function* handleIO(socket) {
  yield fork(read, socket);
  yield fork(write, socket);
}

function* fetchApplicationInitState() {
  const resp = yield call(API.getApplicationInitState);

  if (resp.status === 200) {
    yield put({type: ActionTypes.APP_INITIAL_STATE_REQUEST_SUCCESS, payload: resp.data});
  } else {
    yield put({type: ActionTypes.APP_INITIAL_STATE_REQUEST_FAILURE});
  }
}

function* watchFetchApplicationInitState() {
  yield takeEvery(ActionTypes.APP_INITIAL_STATE_REQUEST, fetchApplicationInitState);
}

// Auth
function* sendAuthReuqest(params) {
  const resp = yield call(API.sendAuthReuqest, params);

  if (resp.status === 200) {
    yield put({type: ActionTypes.AUTH_REQUEST_SUCCESS, payload: resp.data});
  } else {
    yield put({type: ActionTypes.AUTH_REQUEST_FAILURE});
  }
}

function* watchSendAuthReuqest() {
  yield takeEvery(ActionTypes.AUTH_REQUEST, sendAuthReuqest);
}

// Report
function* sendReportRequest(options) {
  const resp = yield call(API.sendReport, options);

  if (resp.status === 200) {
    yield put({
      type: ActionTypes.REPORT_REQUEST_SUCCESS,
      payload: {
        messages: {
          default: {
            type: resp.data.status,
            text: resp.data.status === 'success' ? 'Report submitted successfully' : 'Service is not available, please try again later'
          }
        }
      }
    });
    yield delay(2500);
    yield put({
      type: ActionTypes.REPORT_REQUEST_SUCCESS, payload: {
        messages: {
          default: {
            type: 'success',
            text: null
          }
        }
      }
    });
  } else {
    yield put({type: ActionTypes.REPORT_REQUEST_FAILURE});
  }
}

function* watchSendReportRequest() {
  yield takeEvery(ActionTypes.REPORT_REQUEST, sendReportRequest);
}

/**
 * Determines the order of actions to do,
 * to start socket connection.
 */
function* flow() {
  let {user} = yield take(ActionTypes.AUTH_DATA_UPDATE);
  _socketService.setUser(user);

  const socket = yield call(connect);

  //socket.emit('login', { username: payload.username });

  const task = yield fork(handleIO, socket);

  // Whenever logout action will be available
  // action that action, uncomment please 2 lines below
  // --------------------------------
  //let action = yield take(`${logout}`);
  //yield cancel(task);
}

export default function* rootSaga() {
  yield fork(watchFetchApplicationInitState);
  yield fork(watchSendAuthReuqest);
  yield fork(watchSendReportRequest);
  yield fork(watchSendMessage);
  yield fork(flow);
}