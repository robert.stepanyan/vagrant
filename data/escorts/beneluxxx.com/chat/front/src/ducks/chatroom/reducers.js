import * as GlobalConfig from "../../common/config/global";
import * as ActionTypes from "./types";
import Helpers from "~/common/util/helpers";

const INITIAL_STATE = {
  isAvailableChatsOpen: false,
  activeThreadGUID: GlobalConfig.GENERAL_CHAT_GUID,
  activeThreadUsername: undefined,
  threadsList: {
    [GlobalConfig.GENERAL_CHAT_GUID]: {
      messages: [],
      hasMoreMessages: true,
	  loadingMessages: false,
      privacyLevel: GlobalConfig.PRIVACY_LEVEL_GENERAL_CHAT,
      guid: GlobalConfig.GENERAL_CHAT_GUID,
    }
  },
  connectedUsersList: {},
  chatrooms: {},
  unreadMessages: {},
  recentChats: {},
  visibilityFilter: {
    keyword: null
  },
  invitations: {
    map: {},
    requested: false,
    unseenCount: 0
  },
  isLoading: true,
  isGlobalLoaderOn: false,
  isAuthRequestPending: false,
  requests: {
    report: {
      isPending: false,
      messages: {
        default: {type: 'success', text: null}
      }
    }
  },
  authErrors: [],
  user: null,
  pageBlocked: {
    status: false,
    reason: null
  }
};

// In dev mode lets take the user from cache
// to save some time instead of fetching each time
// ---------------------------------------
if (/*false &&*/ process.env.NODE_ENV === 'development') {
  let user = localStorage.getItem('user');
  if (user) {
    INITIAL_STATE.user = JSON.parse(user);
  }
}

//---------------------------------------

function chatroomReducer(state = INITIAL_STATE, action) {
  //console.log('reducer --------', action.type, state);	  
  switch (action.type) {
    case ActionTypes.BLOCK_PAGE:
      return {
        ...state,
        pageBlocked: {
          status: true,
          reason: 'duplicate_tab'
        }
      }
    case ActionTypes.UPDATE_INITIAL_STATE:

      var _connectedUsers = {...state.connectedUsersList};

      if (action.threadsUnreadMessages) {
        for (let guid in action.threadsUnreadMessages) {
          let room = action.threadsUnreadMessages[guid];

          if (parseInt(room.privacyLevel) === GlobalConfig.PRIVACY_LEVEL_DIRECT) {
            let partnerId = Helpers.getPartnerId(room.guid, state.user.id);

            if (!_connectedUsers[partnerId]) {
              _connectedUsers[partnerId] = {};
            }

            _connectedUsers[partnerId].unreadMessagesCount = room.messagesCount;
          }
        }
      }

      return {
        ...state,
        connectedUsersList: _connectedUsers
      }
    case ActionTypes.SET_GLOBAL_LOADER:
      return {
        ...state,
        isGlobalLoaderOn: action.show
      }
    case ActionTypes.OPEN_AVAILABLE_CHATS:
      return {...state, isAvailableChatsOpen: true};
    case ActionTypes.CLOSE_AVAILABLE_CHATS:
      return {...state, isAvailableChatsOpen: false};
    case ActionTypes.COMPOSED_MESSAGE_SEND:

      var newMessage = {
        text: action.message.text,
        sentDate: action.message.sentDate,
        sender: action.message.sender,
        attachments: action.message.attachments
      }

      return {
        ...state,
        threadsList: {
          ...state.threadsList,
          [state.activeThreadGUID]: {
            ...state.threadsList[state.activeThreadGUID],
            messages: [...state.threadsList[state.activeThreadGUID].messages, newMessage]
          }
        }
      };
    case ActionTypes.COMPOSED_MESSAGE_RECEIVED:

      var roomExisted = !!state.threadsList[action.message.roomGUID]?.guid;
      var room = {
        ...state.threadsList[action.message.roomGUID],
        messages: [
          ...(state.threadsList[action.message.roomGUID]?.messages || []),
          action.message
        ]
      }
      var connectedUsersList = {
        ...state.connectedUsersList
      }
      var chatrooms = {
        ...state.chatrooms
      }
      var unreadMessagesCount = 0;

      // Incrementing unread messages count
      // But if the user is in the same room from where message
      // is received then the count must be nullified
      if (action.message.roomGUID !== state.activeThreadGUID) {

        // Calculation unread messages count based on message privacy level
        switch (action.message.privacyLevel) {
          case GlobalConfig.PRIVACY_LEVEL_GENERAL_CHAT:
            unreadMessagesCount = (state.threadsList[GlobalConfig.GENERAL_CHAT_GUID].unreadMessagesCount || 0) + 1;
            room.unreadMessagesCount = unreadMessagesCount;
            break;
          case GlobalConfig.PRIVACY_LEVEL_DIRECT:
            unreadMessagesCount = (state.connectedUsersList[action.message.sender.id].unreadMessagesCount || 0) + 1;
            connectedUsersList[action.message.sender.id].unreadMessagesCount = unreadMessagesCount;
            break;
          case GlobalConfig.PRIVACY_LEVEL_PRIVATE_GROUP:
            unreadMessagesCount = (state.chatrooms[action.message.roomGUID].unreadMessagesCount || 0) + 1;
            chatrooms[action.message.roomGUID].unreadMessagesCount = unreadMessagesCount;
            break;
        }
      }

      if (action.message) {
        action.message.sender = {
          ...action.message.sender,
          profile: JSON.parse(action.message.sender.profile)
        }
      }

      if (!roomExisted && action.message.roomGUID !== GlobalConfig.GENERAL_CHAT_GUID) {
        room.hasMoreMessages = true;
        room.messagesPage = 0;
        room.messages = [];
      }

      return {
        ...state,
        connectedUsersList: connectedUsersList,
        chatrooms: chatrooms,
        threadsList: {
          ...state.threadsList,
          [action.message.roomGUID]: room
        }
      };
    case ActionTypes.SWITCH_ROOM:

      var isFirstEnter = typeof state.threadsList[action.roomGUID] === 'undefined';
      var isGeneralChat = action.roomGUID.toString() === GlobalConfig.GENERAL_CHAT_GUID.toString();
      var newState = {
        ...state,
        isAvailableChatsOpen: false,
        activeThreadGUID: action.roomGUID,
		activeThreadUsername: action.username,
        chatrooms: {
          ...state.chatrooms
        },
        threadsList: {
          ...state.threadsList
        }
      };

      // If switched to a new room then
      // it must be initialized with empty fields
      if (isFirstEnter && !isGeneralChat) {
        newState.threadsList = {
          ...newState.threadsList,
          [action.roomGUID]: {
            messages: [],
            unreadMessagesCount: 0,
            isLoading: true,
            loadingMessages: false,
            guid: action.roomGUID,
            hasMoreMessages: true,
            messagesPage: 0,
            privacyLevel: isGeneralChat ? GlobalConfig.PRIVACY_LEVEL_GENERAL_CHAT : (
              action.roomGUID.includes('-0-') ? GlobalConfig.PRIVACY_LEVEL_PRIVATE_GROUP : GlobalConfig.PRIVACY_LEVEL_DIRECT
            )
          }
        }
      }
	  
	  if (action.privacyLevel === GlobalConfig.PRIVACY_LEVEL_PRIVATE_GROUP) {
		
        newState.chatrooms[action.roomGUID] = {
          ...newState.chatrooms[action.roomGUID],
          unreadMessagesCount: 0
        };
      }
	  else if(action.privacyLevel === GlobalConfig.PRIVACY_LEVEL_DIRECT) {
		
        newState.connectedUsersList[action.roomGUID] = {
          ...newState.connectedUsersList[action.roomGUID],
          unreadMessagesCount: 0
        };
      } 

      if (!isFirstEnter) {
        newState.threadsList = {
          ...newState.threadsList,
          [action.roomGUID]: {
            ...newState.threadsList[action.roomGUID],
            unreadMessagesCount: 0,
            guid: action.roomGUID,
            privacyLevel: isGeneralChat ? GlobalConfig.PRIVACY_LEVEL_GENERAL_CHAT : (
              action.roomGUID.includes('-0-') ? GlobalConfig.PRIVACY_LEVEL_PRIVATE_GROUP : GlobalConfig.PRIVACY_LEVEL_DIRECT
            )
          }
        };
      }

      return newState;
    case ActionTypes.ROOM_HISTORY_UPDATE:
      return {
        ...state,
        threadsList: {
          ...state.threadsList,
          [action.thread.guid]: {
            ...state.threadsList[action.thread.guid],
            canUnlock: state.user.id === parseInt(action.thread.blockedBy),
            blockedBy: action.thread.blockedBy,
            isLoading: false,
            messages: [
              ...state.threadsList[action.thread.guid].messages,
              ...action.messages
            ],
          }
        }
      };
    case ActionTypes.NEW_MESSAGES_CHUNK:
      return {
        ...state,
        threadsList: {
          ...state.threadsList,
          [action.roomGUID]: {
            ...state.threadsList[action.roomGUID],
            hasMoreMessages: action.hasMore,
            loadingMessages: false,
            messagesPage: (state.threadsList[action.roomGUID].messagesPage || 0) + 1,
            messages: [
              ...action.messages,
              ...state.threadsList[action.roomGUID].messages
            ]
          }
        }
      };
    case ActionTypes.MESSAGES_CHUNK_REQUEST:
      return {
        ...state,
        threadsList: {
          ...state.threadsList,
          [action.roomGUID]: {
            ...state.threadsList[action.roomGUID],
            loadingMessages: true,
          }
        }
      };
    case ActionTypes.CONNECTED_USERS_LIST_UPDATE:
	
	  var _connectedUsers = action.usersList; 
	  
      if (state.unreadMessages) {
        for (let guid in state.unreadMessages) {
          let room = state.unreadMessages[guid];

          if (parseInt(room.privacyLevel) === GlobalConfig.PRIVACY_LEVEL_DIRECT) {
            let partnerId = Helpers.getPartnerId(room.guid, state.user.id);

            if (!_connectedUsers[partnerId]) {
              _connectedUsers[partnerId] = {};
            }

            _connectedUsers[partnerId].unreadMessagesCount = room.messagesCount;
          }
        }
      }
	  
      return {
        ...state,
        connectedUsersList: _connectedUsers
      };
    case ActionTypes.DIRECT_MESSAGE_OPEN:
      return {
        ...state,
        connectedUsersList: {
          ...state.connectedUsersList,
          [action.userId]: {
            ...state.connectedUsersList[action.userId],
            unreadMessagesCount: 0
          }
        }
      };
    case ActionTypes.CHATROOM_LIST_UPDATE:
      return {
        ...state,
        chatrooms: {
          ...action.chatrooms,
          ...state.chatrooms
        }
      };
	case ActionTypes.UNREAD_MESSAGES_UPDATE:
      return {
        ...state,
        unreadMessages: action.unreadMessages
      };  
    case ActionTypes.RECENT_CHATS_UPDATE:
      return {
        ...state,
        recentChats: action.recentChats
      };
    case ActionTypes.CHATROOMS_VISIBILITY_FILTER_SET:
      return {
        ...state,
        visibilityFilter: {
          ...state.visibilityFilter,
          ...action.filter
        }
      };
    case ActionTypes.ROOM_LEAVE:
      return {
        ...state,
        activeThreadGUID: GlobalConfig.GENERAL_CHAT_GUID,
        chatrooms: Helpers.removeByKey(state.chatrooms, action.guid),
        threadsList: Helpers.removeByKey(state.threadsList, action.guid)
      };
    case ActionTypes.UNSEEN_INVITATIONS_UPDATE:
      return {
        ...state,
        unseenInvitationsCount: action.count
      }
    case ActionTypes.INVITATIONS_LIST_UPDATE:

      var count = 0;
      var map = {
        ...state.invitations.map
      };

      if (action.operation === 'increment') {
        count = (state.invitations.unseenCount || 0) + 1;
      }

      if (action.list) {
        action.list.forEach(item => map[item.id] = item);
      }

      return {
        ...state,
        invitations: {
          unseenCount: count,
          requested: action.requested || state.invitations.requested,
          map: map
        }
      };
    case ActionTypes.INVITATION_DELETE:

      var map = {...state.invitations.map};
      delete map[action.id];

      return {
        ...state,
        invitations: {
          ...state.invitations,
          map: map
        }
      }
    case ActionTypes.ACCEPT_INVITATION:
      return state;
    case ActionTypes.APP_INITIAL_STATE_REQUEST_SUCCESS:

      var newState = {
        ...state,
        isLoading: false
      };

      if (action.payload.user && !(action.payload.user.chat_info?.imIsBlocked || action.payload.user.im_is_blocked)) {
        newState.user = action.payload.user
      }

      return {
        ...state,
        ...newState
      };
    case ActionTypes.APP_INITIAL_STATE_REQUEST_FAILURE:
      return {
        ...state,
        isLoading: false
      };
    case ActionTypes.AUTH_REQUEST:
      return {
        ...state,
        isAuthRequestPending: true
      };
    case ActionTypes.REPORT_REQUEST:
      return {
        ...state,
        requests: {
          ...state.requests,
          report: {
            ...state.requests.report,
            isPending: true
          }
        }
      };
    case ActionTypes.REPORT_REQUEST_SUCCESS:
      return {
        ...state,
        requests: {
          ...state.requests,
          report: {
            isPending: false,
            messages: action.payload.messages
          }
        }
      };
    case ActionTypes.REPORT_REQUEST_FAILURE:
      return {
        ...state,
        requests: {
          ...state.requests,
          report: {
            isPending: false,
            messages: {
              default: {
                type: 'error',
                text: 'Service is unreachable, please try again a bit later'
              }
            }
          }
        }
      };
    case ActionTypes.AUTH_REQUEST_SUCCESS:

      if (action.payload.status === 'success' && action.payload.user) {
        action.payload.user.city_id = action.payload.user[action.payload.user.user_type + '_data']?.city_id || null;
        action.payload.user.country_id = action.payload.user[action.payload.user.user_type + '_data']?.country_id || null;

        if(action.payload.user.im_is_blocked || action.payload.user.chat_info?.imIsBlocked) {
          action.payload.status = 'error';
          action.payload.msgs = {username: 'This account is blocked from using chat.'};
        }
      }

      return {
        ...state,
        isAuthRequestPending: false,
        authErrors: (action.payload.status === 'error' ? action.payload.msgs : {}),
        user: (action.payload.status === 'success' ? action.payload.user : null)
      };
    case ActionTypes.AUTH_REQUEST_FAILURE:
      return {
        ...state,
        isAuthRequestPending: false
      };
    case ActionTypes.GOT_KICKED:

      var _chatrooms = {...state.chatrooms};
      var _activeThreadGUID = state.activeThreadGUID;

      action.rooms.forEach(room => {
        delete _chatrooms[room.guid];

        // If user had one of the kicked rooms
        // as his/her active Chat, then move him/her to general
        if (state.activeThreadGUID === room.guid) {
          _activeThreadGUID = GlobalConfig.GENERAL_CHAT_GUID;
        }
      });

      return {
        ...state,
        activeThreadGUID: _activeThreadGUID,
        chatrooms: _chatrooms
      }
    case ActionTypes.TOGGLE_CHAT_BLOCK_STATE:

      // If room was not entered by user yet
      if (!state.threadsList[action.roomGUID]) return state;

      return {
        ...state,
        threadsList: {
          ...state.threadsList,
          [action.roomGUID]: {
            ...state.threadsList[action.roomGUID],
            blockedBy: action.blockedBy,
            canUnlock: state.user.id === parseInt(action.blockedBy),
          }
        }
      }
    default:
      return state;
	
  }
 
}

export default chatroomReducer;