import {createSelector} from 'reselect';
import * as GlobalConfig from "~/common/config/global";
import Helpers from "~/common/util/helpers";

const threadsSelector = state => state.threadsList;
const activeThreadGUIDSelector = state => state.activeThreadGUID;
const searchedKeywordSelector = state => state.visibilityFilter.keyword;
const groupChatroomsSelector = state => state.chatrooms;
const recentChatsSelector = state => state.recentChats;
const invitationsSelector = state => state.invitations.map;
export const currentUserSelector = state => state.user;
const connectedUsersSelector = state => {
  let result = {};
  for (let id in state.connectedUsersList) {
    if (state.connectedUsersList[id].hasOwnProperty('user_type')) {
      result[id] = state.connectedUsersList[id];
    }
  }
  return result;
}

export const connectedUsersExceptCurrentSelector = createSelector(
  currentUserSelector,
  connectedUsersSelector,
  (user, usersList) => {
    delete usersList[user.id];
    return usersList;
  }
);

export const activeThreadSelector = createSelector(
  threadsSelector,
  activeThreadGUIDSelector,
  (threads, activeThreadGUID) => threads[activeThreadGUID]
);

export const connectedUsersCountSelector = createSelector(
  connectedUsersSelector,
  usersList => Object.keys(usersList).length
);

export const visibleChatroomsSelector = createSelector(
  groupChatroomsSelector,
  searchedKeywordSelector,
  (rooms, keyword) => {

    if (!keyword) return rooms;

    keyword = keyword.toLowerCase();
    let result = {};

    for (let key in rooms) {
      let item = rooms[key];

      if (item.title.toLowerCase().includes(keyword)) {
        result[key] = {
          ...item,
          title: Helpers.highlight(item.title, keyword)
        };
      }
    }

    return result;
  }
);

export const visibleRecentChatsSelector = createSelector(
  recentChatsSelector,
  searchedKeywordSelector,
  (recentChats, keyword) => {

    if (!keyword) return recentChats;

    keyword = keyword.toLowerCase();
    let result = {};

    for (let key in recentChats) {
      let item = recentChats[key];

      if (item.username.toLowerCase().includes(keyword)) {
        result[key] = {
          ...item,
          username: Helpers.highlight(item.username, keyword)
        };
      }
    }

    return result;
  }
);

export const visibleConnectedUsersListSelector = (userType) => createSelector(
  connectedUsersExceptCurrentSelector,
  searchedKeywordSelector,
  (usersList, keyword) => {

    let result = {};
    if (keyword) keyword = keyword.toLowerCase();

    for (let key in usersList) {
      let item = usersList[key];
      // This contains only unread messages count, so lets exclude them
      if (!item.username) continue;

      let typeMatches = userType ? item.user_type === userType : true;
      let keywordMatches = keyword ? item.username.toLowerCase().includes(keyword) : true;

      if (typeMatches && keywordMatches) {
        result[item.id] = {
          ...item,
          username: Helpers.highlight(item.username, keyword)
        };
        ;
      }
    }

    return result;
  }
);

export const unreadMessagesSelector = createSelector(
  threadsSelector,
  connectedUsersSelector,
  groupChatroomsSelector,
  (threads, usersList, chatroomsList) => {
    let result = {overall: 0, generalChat: 0}
    for (let key in threads) {
      result.overall += threads[key].unreadMessagesCount || 0;

      if (key === GlobalConfig.GENERAL_CHAT_GUID) {
        result.generalChat += threads[key].unreadMessagesCount || 0;
      }
    }
    for (let key in chatroomsList) {
      result.overall += chatroomsList[key].unreadMessagesCount || 0;
    }
    for (let key in usersList) {
      result.overall += usersList[key].unreadMessagesCount || 0;
    }
    return result;
  }
);

export const activeThreadTitleSelector = createSelector(
  activeThreadSelector,
  connectedUsersSelector,
  groupChatroomsSelector,
  currentUserSelector,
  (thread, usersList, chatroomsList, user) => {

    switch (thread.privacyLevel) {
      case GlobalConfig.PRIVACY_LEVEL_GENERAL_CHAT:
        return "General Chat";
      case GlobalConfig.PRIVACY_LEVEL_DIRECT:
        let partnerId = Helpers.getPartnerId(thread.guid, user.id);
        return usersList[partnerId]?.username;
      case GlobalConfig.PRIVACY_LEVEL_PRIVATE_GROUP:
        return chatroomsList[thread.guid]?.title;
    }

    return "Chat";
  }
);

export const invitationsListSelector = createSelector(
  invitationsSelector,
  (invitations) => Object.values(invitations)
);
