import { combineReducers } from 'redux';
import chatroomReducer from "./ducks/chatroom/reducers";

const reducers = combineReducers({
    chatroom: chatroomReducer
});

export default reducers;