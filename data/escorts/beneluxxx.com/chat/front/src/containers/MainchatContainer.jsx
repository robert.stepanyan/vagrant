import React, {Component} from 'react';
import {connect} from "react-redux";
import {updateAuthData} from "~/ducks/chatroom/actions";
import Mainchat from "~/components/Mainchat/Mainchat";
import AuthContainer from "./AuthContainer";
import {withTranslation} from "react-i18next";
import Button from "@material-ui/core/Button";
import {ReactComponent as UndrawChatBot} from "~/common/icons/undraw-chat-bot.svg";

class MainchatContainer extends Component {

  render() {
    const {t} = this.props;
    if (this.props.user && process.env.NODE_ENV === 'development') {
      localStorage.setItem('user', JSON.stringify(this.props.user));
    }

    /**
     * Whenever user is detected, i.e. Auth succeeded etc.
     * Setting user's credential into socket Service and establish
     * connection with socket Server.
     */
    if (this.props.user) {
      this.props.updateUser(this.props.user);
    }

    if (this.props.pageBlocked.status === true) {
      return <div className={'page-blocked-dialog'}>
        <div className="page-blocked-dialog__content">
          <div className="title-wrapper">
            <p dangerouslySetInnerHTML={{__html: t(this.props.pageBlocked.reason)}}></p>
          </div>

          <div className="icon-wrapper">
            <UndrawChatBot/>
          </div>

          <div className="actions-wrapper">
            <Button onClick={this.moveToHomepage} className={'action--secondary'}>{t('close')}</Button>
            <Button onClick={this.refreshThePage} className={'action--primary'} variant="contained" disableElevation>
              {t('use_here')}
            </Button>
          </div>
        </div>
      </div>;
    }

    return !this.props.user ? <AuthContainer/> : <Mainchat/>;
  }

  moveToHomepage = () => {
    // Will not have any effect for dev mode
    window.location.href = '/';
  }

  refreshThePage = () => {
    window.location.reload();
  }
}

const mapStateToProps = (state) => ({
  user: state.chatroom.user,
  pageBlocked: state.chatroom.pageBlocked
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  updateUser(user) {
    dispatch(updateAuthData(user));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(MainchatContainer));