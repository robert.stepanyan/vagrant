import React, {Component} from "react";
import {batch, connect} from 'react-redux'
import * as GlobalConfig from "~/common/config/global";
import {APP_ORIGIN} from "~/common/config/global";
import {
  closeAvailableChats,
  leaveRoom,
  openDirectMessage,
  sendWssMessage,
  setChatroomsVisibilityFilter,
  setGlobalLoader,
  switchRoom,
  updateBlockedChat,
  updateChatroomList
} from 'ducks/chatroom/actions';
// Components
// ---------------------
import SidebarPanel from 'components/SidebarPanel/SidebarPanel';
import {withSnackbar} from "notistack";
import {
  visibleChatroomsSelector,
  visibleConnectedUsersListSelector,
  visibleRecentChatsSelector,
  connectedUsersCountSelector,
  unreadMessagesSelector
} from "~/ducks/chatroom/selectors";
import Helpers from "~/common/util/helpers";
import {withTranslation} from "react-i18next";

class SidebarContainer extends Component {

  render() {
    return <SidebarPanel
      isOnMobileOpen={this.props.isAvailableChatsOpen === true}
      activeChatGuid={this.props.activeThreadGUID}
      generalChatUnreadMessages={this.props.unreadMessages.generalChat}
      connectedUsersCount={this.props.connectedUsersCount}
      connectedEscorts={this.props.escorts}
      connectedMembers={this.props.members}
      connectedAgencies={this.props.agencies}
      chatrooms={this.props.visibleChatrooms}
      recentChats={this.props.visibleRecentChats}
	  
      onChatLeave={this.leaveChat}
      onRoomSwitch={this.switchRoom}
      onSidebarClose={this.closeSidebar}
      onChatroomCreate={this.createChatroom}
      onChatroomSearch={this.chatroomSearch}
      onRoomLeave={this.roomLeave}
      onUserKick={this.kickUser}
      onUserBlock={this.blockUser}
      onUserProfileOpen={this.openUserProfile}
      onGeneralChatSwitch={this.activateGeneralTab}
    />
  }

  activateGeneralTab = () => {
    this.props.dispatch(switchRoom({
      roomGUID: GlobalConfig.GENERAL_CHAT_GUID,
      privacyLevel: GlobalConfig.PRIVACY_LEVEL_GENERAL_CHAT
    }));
  };

  openUserProfile = (link) => {
	window.open(link);
	return false;
	/*batch(() => {
      this.props.dispatch(setGlobalLoader({show: true}));
      this.props.dispatch(sendWssMessage('get-profile-link', {userId}, (response) => {
	  
        if (response.link) {
		  window.open(response.link);
		  this.props.dispatch(setGlobalLoader({show: false}));
        } else {
          this.props.enqueueSnackbar(this.props.t('profile_not_available_atm'), {
            variant: 'warning',
            autoHideDuration: 2500
          });
          this.props.dispatch(setGlobalLoader({show: false}));
        }
      }));
    });*/
  };

  kickUser = (userId) => {
    this.props.dispatch(setGlobalLoader({show: true}));

    setTimeout(() => {
      batch(() => {
        this.props.dispatch(sendWssMessage('kick-user', {userId}, (response) => {

          this.props.dispatch(setGlobalLoader({show: false}));
          if (response.status === 'success') {
            let text = response.kickedRoomsCount > 0 ?
              this.props.t('user_kicked_from_rooms', {count: response.kickedRoomsCount}) : this.props.t('no_rooms_to_kick_user');
            this.props.enqueueSnackbar(text, {
              variant: 'success',
              autoHideDuration: 2500
            });
          } else {
            this.props.enqueueSnackbar(this.props.t('server_error'), {
              variant: 'warning',
              autoHideDuration: 2500
            });
          }
        }));
      })
    }, 1000)
  }

  blockUser = (userId) => {
    this.props.dispatch(setGlobalLoader({show: true}));

    setTimeout(() => {
      batch(() => {
        this.props.dispatch(sendWssMessage('block-user', {userId}, (response) => {

          if (response.status === 'success') {
            let text = response.result?.affectedRows > 0 ? this.props.t('user_blocked') : this.props.t('conversation_already_blocked');

            this.props.enqueueSnackbar(text, {
              variant: response.result?.affectedRows > 0 ? 'success' : 'warning',
              autoHideDuration: 2500
            });

            this.props.dispatch(updateBlockedChat({
              roomGUID: Helpers.getThreadGUID(this.props.user.id, userId),
              blockedBy: this.props.user.id
            }));

          } else {
            this.props.enqueueSnackbar(this.props.t('server_error'), {
              variant: 'warning',
              autoHideDuration: 2500
            });
          }

          this.props.dispatch(setGlobalLoader({show: false}));
        }));
      })
    }, 1000)
  }

  roomLeave = (options) => {
    batch(() => {
      this.props.dispatch(sendWssMessage('leave-room', {
        roomGUID: options.guid,
        roomId: options.id
      }));

      this.props.dispatch(leaveRoom({
        guid: options.guid,
        id: options.id
      }));
    })
  }

  chatroomSearch = (keyword) => {
    this.props.dispatch(setChatroomsVisibilityFilter({
      keyword
    }));
  }

  switchRoom = (params) => {

    let selectedRoomGUID = params.privacyLevel === GlobalConfig.PRIVACY_LEVEL_DIRECT ? Helpers.getThreadGUID(this.props.user.id, params.userId) : params.roomGUID;
    const options = {
      ...params,
      roomGUID: selectedRoomGUID,
      privacyLevel: params.privacyLevel
    };

    batch(() => {

      // If user was not joined to this room
      if (typeof this.props.threads[options.roomGUID] === 'undefined' || this.props.threads[options.roomGUID].hasOwnProperty('guid') === false) {
        this.props.dispatch(sendWssMessage('join-room', options));
      }

      this.props.dispatch(switchRoom({
        roomGUID: options.roomGUID,
		username: options.username,
        privacyLevel: options.privacyLevel,
      }));

      if (params.privacyLevel === GlobalConfig.PRIVACY_LEVEL_DIRECT) {
        this.props.dispatch(openDirectMessage({
          userType: params.userType,
          userId: params.userId,
        }));
		// unset unread messages
		this.props.dispatch(sendWssMessage('reading-messages', {roomGUID: selectedRoomGUID}));
      }
    });
  }

  closeSidebar = () => {
    this.props.dispatch(closeAvailableChats());
  }

  leaveChat() {
    window.location.href = APP_ORIGIN;
  }

  createChatroom = (name) => {
    this.props.dispatch(setGlobalLoader({show: true}));

    setTimeout(() => {

      batch(() => {
        this.props.dispatch(sendWssMessage('create-chatroom', {name}, (response) => {

          batch(() => {
            this.props.dispatch(setGlobalLoader({show: false}));
            if (response.chatrooms) {
              this.props.dispatch(updateChatroomList(response.chatrooms))
            }
          });

        }));
      });
    }, 1000)
  }
}

const mapStateToProps = (state, ownProps) => ({
  unreadMessages: unreadMessagesSelector(state.chatroom),
  connectedUsersList: state.chatroom.connectedUsersList,
  connectedUsersCount: connectedUsersCountSelector(state.chatroom),
  escorts: visibleConnectedUsersListSelector('escort')(state.chatroom),
  agencies: visibleConnectedUsersListSelector('agency')(state.chatroom),
  members: visibleConnectedUsersListSelector('member')(state.chatroom),

  isAvailableChatsOpen: state.chatroom.isAvailableChatsOpen,
  activeThreadGUID: state.chatroom.activeThreadGUID,
  user: state.chatroom.user,
  threads: state.chatroom.threadsList,

  visibleChatrooms: visibleChatroomsSelector(state.chatroom),
  visibleRecentChats: visibleRecentChatsSelector(state.chatroom),
});

export default connect(mapStateToProps)(withSnackbar(withTranslation()(SidebarContainer)));