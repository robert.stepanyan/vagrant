import MessageCreatorForm from "../components/MessageCreatorForm/MessageCreatorForm";
import React from "react";
import {batch, connect} from "react-redux";

import {sendComposedMessage, sendWssMessage} from "../ducks/chatroom/actions";
import {activeThreadSelector} from "ducks/chatroom/selectors";

class MessageCreatorFormContainer extends React.Component {

  render() {
    return <MessageCreatorForm
      submitMessage={this.props.sendComposedMessage}
      userId={this.props.user.id}
      activeRoomGUID={this.props.activeThreadGUID}
      isConversationBlocked={this.props.isConversationBlocked}
    />
  }
}

const mapStateToProps = (state) => ({
  user: state.chatroom.user,
  activeThreadGUID: state.chatroom.activeThreadGUID,
  isConversationBlocked: Boolean(activeThreadSelector(state.chatroom).blockedBy),
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  'sendComposedMessage': (message) => {

    batch(() => {
      // to show message locally
      dispatch(sendComposedMessage(message));

      // send to other users
      dispatch(sendWssMessage('send-message', message));
    })
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(MessageCreatorFormContainer);