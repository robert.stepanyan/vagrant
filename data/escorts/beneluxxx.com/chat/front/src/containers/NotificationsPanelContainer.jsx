import React from "react";
import {batch, connect} from "react-redux";

import NotificationsPanel from "~/components/NotificationsPanel/NotificationsPanel";
import {deleteInvitation, updateInvitationsList, sendWssMessage} from "~/ducks/chatroom/actions";
import {invitationsListSelector} from "~/ducks/chatroom/selectors";

class NotificationsPanelContainer extends React.Component {

  render() {
    return <NotificationsPanel
      invitationsList={this.props.invitationsList}
      unseenInvitationsCount={this.props.unseenInvitationsCount}
      isLoading={!this.props.requested}

      onInvitationAccept={this.acceptInvitation}
      onInvitationDelete={this.deleteInvitation}
      onOpen={this.open}
    />
  }

  deleteInvitation = (data) => {
    batch(() => {
      this.props.dispatch(sendWssMessage('delete-invitation', {
        id: data.id
      }));
      this.props.dispatch(deleteInvitation({
        id: data.id
      }));
    });
  }

  acceptInvitation = (data) => {

    this.props.dispatch(sendWssMessage('invitation-accept', {
      roomGUID: data.guid
    }));

    this.deleteInvitation(data);
  }

  open = () => {
    setTimeout(() => {
      if (this.props.requested === false || this.props.unseenInvitationsCount > 0) {
        this.props.dispatch(sendWssMessage('get-invitations', null, (data) => {
          this.props.dispatch(updateInvitationsList({
            list: data.invitations,
            requested: true
          }));
        }));
      }
    }, 250)
  }

}

const mapStateToProps = (state) => ({
  invitationsList: invitationsListSelector(state.chatroom),
  requested: state.chatroom.invitations.requested,
  unseenInvitationsCount: state.chatroom.invitations.unseenCount,
});

export default connect(mapStateToProps)(NotificationsPanelContainer);