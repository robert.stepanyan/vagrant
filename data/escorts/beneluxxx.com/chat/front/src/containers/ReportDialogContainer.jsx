import React, {Component} from "react";
import {connect} from 'react-redux'
import {withSnackbar} from 'notistack';
import ReportDialog from "components/ReportDialog/ReportDialog";
import {sendReportRequest} from "ducks/chatroom/actions";
import {currentUserSelector} from "ducks/chatroom/selectors";
import PropTypes from "prop-types";

class ReportDialogContainer extends Component {

  render() {
    return <ReportDialog
      request={this.props.request}
      isLoading={this.props.request.isPending}
      open={this.props.open}

      message={this.props.request.messages.default?.text}
      messageType={this.props.request.messages.default?.type}

      onSelectRoom={this.sendInvitation}
      onSubmit={this.submitReport}
      onClose={this.props.onClose}
    />
  }

  submitReport = ({reason}) => {
    this.props.dispatch(sendReportRequest({
      senderId: this.props.userId,
      reportedUserId: this.props.reportedUserId,
      reason
    }))
  }
}

const mapStateToProps = (state, ownProps) => ({
  userId: currentUserSelector(state.chatroom).id,
  request: state.chatroom.requests.report
})

ReportDialogContainer.propTypes = {
  reportedUserId: PropTypes.number.isRequired
};

export default connect(
  mapStateToProps
)(withSnackbar(ReportDialogContainer));