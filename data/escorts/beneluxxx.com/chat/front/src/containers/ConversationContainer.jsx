import React, {Component} from "react";
import {connect} from 'react-redux'
import * as GlobalConfig from "../common/config/global";
import ConversationPanel from "../components/CoversationPanel/ConversationPanel";
import {openAvailableChats, switchRoom} from "../ducks/chatroom/actions";
import {
  activeThreadSelector,
  connectedUsersCountSelector,
  activeThreadTitleSelector,
  unreadMessagesSelector
} from "~/ducks/chatroom/selectors";

class ConversationContainer extends Component {

  render() {
    return <ConversationPanel
      title={this.props.activeThreadTitle}
	  activeThreadUsername={this.props.activeThreadUsername}
      isOnMobileOpen={this.props.isAvailableChatsOpen === false}
      onGeneralTabActivate={this.props.activateGeneralTab}
      onSidebarOpen={this.props.openSidebar}

      generalChatUnreadMessages={this.props.unreadMessages.generalChat}
      totalUnreadMessages={this.props.unreadMessages.overall}

      connectedUsersCount={this.props.connectedUsersCount}

      activeChatId={this.props.activeThread.guid}
      activeThreadType={this.props.activeThread.privacyLevel}
      thread={this.props.activeThread}
    />
  }
}

const mapStateToProps = (state, ownProps) => ({
  connectedUsersCount: connectedUsersCountSelector(state.chatroom),
  isAvailableChatsOpen: state.chatroom.isAvailableChatsOpen,
  activeThread: activeThreadSelector(state.chatroom),
  activeThreadTitle: activeThreadTitleSelector(state.chatroom),
  unreadMessages: unreadMessagesSelector(state.chatroom),
  activeThreadUsername: state.chatroom.activeThreadUsername
});

const mapDispatchToProps = (dispatch, ownProps) => ({

  openSidebar: () => {
    dispatch(openAvailableChats())
  },

  activateGeneralTab: () => {
    dispatch(switchRoom({
      roomGUID: GlobalConfig.GENERAL_CHAT_GUID,
      privacyLevel: GlobalConfig.PRIVACY_LEVEL_GENERAL_CHAT
    }));
  }
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConversationContainer);