import React, { Component } from "react";
import { connect } from 'react-redux'
import AuthForm from "../components/AuthForm/AuthForm";
import {requestAuth} from "../ducks/chatroom/actions";

class AuthContainer extends Component {

    render() {
        return <AuthForm 
            onAuthRequest={this.props.authRequest}
            isRequestPending={this.props.isAuthPending}
            errors={this.props.authErrors}
        />
    }
}

const mapStateToProps = (state, ownProps) => ({
    isAuthPending: state.chatroom.isAuthRequestPending,
    authErrors: state.chatroom.authErrors
})

const mapDispatchToProps = (dispatch, ownProps) => ({
    authRequest: (data) => {
        dispatch(requestAuth(data))
    }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AuthContainer);