import React, {Component} from "react";
import {connect} from 'react-redux'
import ChatroomInviteDialog from "~/components/ChatroomInviteDialog/ChatroomInviteDialog";
import {sendWssMessage} from "~/ducks/chatroom/actions";
import {withSnackbar} from 'notistack';
import PropTypes from 'prop-types';
import {withTranslation} from "react-i18next";

class ChatroomInviteDialogContainer extends Component {

  render() {
    const roomsList = Object.values(this.props.chatrooms);

    return <ChatroomInviteDialog
      roomsList={roomsList}
      open={this.props.open}
      onClose={this.props.onClose}
      onSelectRoom={this.sendInvitation}
    />
  }

  sendInvitation = (roomId) => {

    this.props.dispatch(sendWssMessage('invite-to-chatroom', {
      roomId,
      userId: this.props.targetUserId
    }, ({status, message = null}) => {
      if (status === 'success') {
        this.props.enqueueSnackbar(this.props.t('invitation_sent'), {
          variant: 'success',
          autoHideDuration: 1500,
        });
      } else if (status === 'error') {
        this.props.enqueueSnackbar(message, {
          variant: 'warning',
          autoHideDuration: 1500,
        });
      }

      this.props.onClose();
    }));
  }
}

ChatroomInviteDialogContainer.propTypes = {
  targetUserId: PropTypes.number.isRequired
};

const mapStateToProps = (state, ownProps) => ({
  chatrooms: state.chatroom.chatrooms,
})

export default connect(mapStateToProps)(withSnackbar(withTranslation()(ChatroomInviteDialogContainer)));