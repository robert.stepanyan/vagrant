import React, {Component} from "react";
import {batch, connect} from 'react-redux'
import * as GlobalConfig from "../common/config/global";

import ChatMessages from "../components/CoversationPanel/partial/ChatMessages";
import {openAvailableChats, requestMessagesChunk, sendWssMessage, switchRoom} from "~/ducks/chatroom/actions";
import {activeThreadSelector} from "~/ducks/chatroom/selectors";
import {setGlobalLoader, updateBlockedChat} from "ducks/chatroom/actions";
import {withSnackbar} from "notistack";
import Helpers from "common/util/helpers";
import {withTranslation} from "react-i18next";

class ChatMessagesContainer extends Component {

  chatDescriptionShowedKey = 'chatDescriptionShowed';

  state = {
    chatDescriptionShowed: localStorage.getItem(this.chatDescriptionShowedKey) === 'true' || false
  };

  render() {
    return (<ChatMessages
      isRoomLoding={this.props.activeThread.isLoading}
      isBlocked={Boolean(this.props.activeThread.blockedBy)}
      activeThreadType={this.props.activeThread.privacyLevel}
      canUnlock={this.props.activeThread.canUnlock}
      messagesList={this.props.activeThread.messages}
      userId={this.props.userId}
      chatDescriptionShowed={this.state.chatDescriptionShowed}

      onUserUnlock={this.unlockUser}
      loadMessages={this.loadMessages}
      onChatDescriptionClose={this.closeChatDescription}
      hasMoreMessages={this.props.activeThread.hasMoreMessages}
      messagesPage={this.props.activeThread.messagesPage}
      isLoadingMessages={this.props.activeThread.loadingMessages}
    />)
  };

  unlockUser = () => {
    batch(() => {
      this.props.dispatch(setGlobalLoader({show: true}));

      let userId = Helpers.getPartnerId(this.props.activeThread.guid, this.props.userId);
      this.props.dispatch(sendWssMessage('unlock-user', {userId}, (response) => {

        if (response.status === 'success') {
          this.props.enqueueSnackbar(this.props.t('user_unlocked'), {
            variant: 'success',
            autoHideDuration: 2500
          });

          this.props.dispatch(updateBlockedChat({
            roomGUID: Helpers.getThreadGUID(this.props.userId, userId),
            blockedBy: null
          }));

        } else {

          this.props.enqueueSnackbar(this.props.t('server_error'), {
            variant: 'warning',
            autoHideDuration: 2500
          });
        }

        this.props.dispatch(setGlobalLoader({show: false}));
      }));
    });
  };

  loadMessages = () => {
	setTimeout(() => {
		
		//if (~~this.props.activeThread.guid == GlobalConfig.GENERAL_CHAT_GUID) return;
		if (this.props.activeThread.loadingMessages) return;

		batch(() => {
			this.props.dispatch(requestMessagesChunk({roomGUID: this.props.activeThread.guid}));

			this.props.dispatch(sendWssMessage('load-messages', {
				roomGUID: this.props.activeThread.guid,
				page: this.props.activeThread.messagesPage
			}));
		}
		);  
	
    }, 800);

  };

  shouldComponentUpdate(nextProps, nextState) {

    if (this.props.activeThread.guid !== nextProps.activeThread.guid) {
      // Switched to new room

    } else {
      // It's same room, but component did some update
      this.markMessagesAsRead({
        roomGUID: nextProps.activeThread.guid
      });
    }

    return true;
  };

  closeChatDescription = () => {
    localStorage.setItem(this.chatDescriptionShowedKey, true);
    this.setState({chatDescriptionShowed: true});
  };

  activateGeneralTab() {
    this.props.dispatch(switchRoom({
      roomGUID: GlobalConfig.GENERAL_CHAT_GUID,
      privacyLevel: GlobalConfig.PRIVACY_LEVEL_GENERAL_CHAT
    }));
  };

  markMessagesAsRead(options) {
    this.props.dispatch(sendWssMessage('reading-messages', {
      roomGUID: options.roomGUID
    }));
  };
}

const mapStateToProps = (state, ownProps) => ({
  activeThread: activeThreadSelector(state.chatroom),
  userId: state.chatroom.user.id
});

export default connect(
  mapStateToProps
)(withSnackbar(withTranslation()(ChatMessagesContainer)));