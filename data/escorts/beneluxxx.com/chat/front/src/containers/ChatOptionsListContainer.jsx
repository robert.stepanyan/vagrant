import React, {Component} from "react";
import {batch, connect} from 'react-redux'
import ChatOptionsList from "~/components/ChatOptionsList/ChatOptionsList";
import {activeThreadSelector, currentUserSelector} from "ducks/chatroom/selectors";
import Helpers from "~/common/util/helpers";
import {sendWssMessage, setGlobalLoader, updateBlockedChat} from "~/ducks/chatroom/actions";
import {withSnackbar} from "notistack";
import {APP_ORIGIN} from "~/common/config/global";
import {withTranslation} from "react-i18next";

class ChatOptionsListContainer extends Component {

  constructor(props) {
    super(props);

    this.state = {
      open: this.props.open
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevState.open !== this.props.open) {
      this.setState({open: this.props.open});
    }
  }

  optionsToggle = (open = null) => {
    if (!open) open = !this.state.open;
    this.setState({open})
  };

  blockUser = (userId) => {
    batch(() => {
      this.props.dispatch(setGlobalLoader({show: true}));
      this.props.dispatch(sendWssMessage('block-user', {userId}, (response) => {

        if (response.status === 'success') {
          let text = response.result?.affectedRows > 0 ? this.props.t('user_blocked') :  this.props.t('conversation_already_blocked');

          this.props.enqueueSnackbar(text, {
            variant: response.result?.affectedRows > 0 ? 'success' : 'warning',
            autoHideDuration: 2500
          });

          this.props.dispatch(updateBlockedChat({
            roomGUID: Helpers.getThreadGUID(this.props.userId, userId),
            blockedBy: this.props.userId
          }));

        } else {
          this.props.enqueueSnackbar(this.props.t('server_error'), {
            variant: 'warning',
            autoHideDuration: 2500
          });
        }

        this.props.dispatch(setGlobalLoader({show: false}));
      }));
    })
  };

  kickUser = (userId) => {
    batch(() => {
      this.props.dispatch(setGlobalLoader({show: true}));
      this.props.dispatch(sendWssMessage('kick-user', {userId}, (response) => {

        this.props.dispatch(setGlobalLoader({show: false}));
        if (response.status === 'success') {
          let text = response.kickedRoomsCount > 0 ?
            this.props.t('user_kicked_from_rooms', {count: response.kickedRoomsCount}) : this.props.t('no_rooms_to_kick_user');
          this.props.enqueueSnackbar(text, {
            variant: 'success',
            autoHideDuration: 2500
          });
        } else {
          this.props.enqueueSnackbar(this.props.t('server_error'), {
            variant: 'warning',
            autoHideDuration: 2500
          });
        }

      }));
    })
  };

  openUserProfile = (userId) => {

    batch(() => {
      this.props.dispatch(setGlobalLoader({show: true}));
      this.props.dispatch(sendWssMessage('get-profile-link', {userId}, (response) => {

        if (response.link) {
          window.location.href = APP_ORIGIN + response.link;
        } else {
          this.props.enqueueSnackbar(this.props.t('profile_not_available_atm'), {
            variant: 'warning',
            autoHideDuration: 2500
          });
          this.props.dispatch(setGlobalLoader({show: false}));
        }
      }));
    });
  };

  render() {
    const targetUserId = Helpers.getPartnerId(this.props.activeThreadGUID, this.props.userId);

    return <ChatOptionsList
      open={this.state.open}
      actions={this.props.actions}
      targetUserId={Number(targetUserId)}

      onUserBlock={this.blockUser}
      onUserKick={this.kickUser}
      onOptionsToggle={this.optionsToggle}
      onUserProfileOpen={this.openUserProfile}
    />
  }
}

const mapStateToProps = (state, ownProps) => ({
  activeThreadGUID: activeThreadSelector(state.chatroom).guid,
  connectedUsers: state.chatroom,
  userId: currentUserSelector(state.chatroom).id,
});

export default connect(mapStateToProps)(withSnackbar(withTranslation()(ChatOptionsListContainer)));