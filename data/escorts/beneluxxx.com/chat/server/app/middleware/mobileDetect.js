import isMobile from 'ismobilejs';

export default (req, res, next) => {
  const userAgent = req.headers['user-agent'];
  let [first] = req.subdomains;

  if (first !== 'm' && isMobile(userAgent).any) {
    let url = req.protocol + "://m." + process.env.DOMAIN + req.originalUrl;
    return res.redirect(url);
  }

  if (first !== 'www' && isMobile(userAgent).any === false) {
    let url = req.protocol + "://www." + process.env.DOMAIN + req.originalUrl;
    return res.redirect(url);
  }

  return next();
};