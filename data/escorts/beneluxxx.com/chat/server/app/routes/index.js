'use strict';

const express = require('express');
const router = express.Router();
const path = require("path");

const ApiRoutes = require("./api.routes");
const AttachmentsRoutes = require("./attachments.routes");

router.use('/api', ApiRoutes);
router.use('/attachment', AttachmentsRoutes);

router.get('/', function (req, res, next) {
  res.sendFile(path.join(__dirname + "/../../../front/build/index.html"));
});

module.exports = router;