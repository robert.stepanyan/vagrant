const express = require('express');
const router = express.Router();
const config = require('../config');
const redisService = require("../services/redis");

router.get('/block-user', async function (req, res, next) {
  const {userId} = req.query;
  const io = req.app.get('io');
  const response = {userId, success: false};

  let receiverSocketId = await redisService.getAsync('connected-user:' + userId);

  if (receiverSocketId) {
    response.success = true;
    io.of(config.constants.CHATROOM_NS).to(receiverSocketId).emit('chat-blocked');
  }else {
    response.msg = "User not online";
  }

  return res.json(response);
});

module.exports = router;