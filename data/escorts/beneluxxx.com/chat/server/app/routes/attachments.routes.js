const express = require('express');
const router = express.Router();
const uuid = require('uuid');
const path = require('path');
const fs = require('fs');

const PATH_TO_ASSETS = path.resolve(__dirname, '../../public/attachments');

router.post('/send', (req, res) => {
  const d = new Date();
  const targetDir = d.getFullYear() + '/' + d.getMonth();
  fs.mkdirSync(path.resolve(PATH_TO_ASSETS, targetDir), {recursive: true});

  const fileId = targetDir + '/' + uuid.v4();
  res.send(fileId);
});

router.patch('/send', (req, res) => {

  const totalSize = req.header('Upload-Length');
  const filename = req.header('Upload-Name');
  const offset = req.header('Upload-Offset');
  const fileId = req.query.patch;
  const ext = filename.split('.').pop();

  const chunk = [];

  req
    .on("data", (part) => {
      chunk.push(part);
    })
    .on("end", () => {
      const filePath = PATH_TO_ASSETS + '/' + fileId + "." + ext;

      fs.appendFile(filePath, Buffer.concat(chunk), function (err) {
        if (err) throw err;
      });

      res.send(JSON.stringify({
        status: 200,
        offset,
        totalSize
      }));

    });
});

module.exports = router;