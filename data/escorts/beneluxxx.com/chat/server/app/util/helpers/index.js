const Helpers = {

  getThreadGUID(user1, user2) {
    return [parseInt(user1), parseInt(user2)].sort().join('-');
  },

  getPartnerId(roomGUID, id) {
    return roomGUID.replace(new RegExp(`${id}-|-${id}`, "g"), '');
  },

  isDevelopment() {
    let mode = process.env.NODE_ENV?.toLowerCase().trim();
    return ['development', 'dev'].includes(mode);
  }
};

export default Helpers;