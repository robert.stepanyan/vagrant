const config = require('../..//config');
const redis = require('redis').createClient;

module.exports = {

  _client: redis({
    host: config.redis.host,
    port: config.redis.port
  }),

  setAdapter(client) {
    this._client = client;
    return this;
  },

  getAdapter() {
    return this._client;
  },

  getAsync(key) {
    return new Promise((res, rej) => {
      this._client.get(key, function (err, reply) {
        if (err) rej(err);

        res(reply);
      });
    });
  },

  hgetallAsync(key) {
    return new Promise((res, rej) => {
      this._client.hgetall(key, function (err, reply) {
        if (err) rej(err);

        res(reply);
      });
    });
  },

  smembersAsync(key) {
    return new Promise((res, rej) => {
      this._client.smembers(key, function (err, reply) {
        if (err) rej(err);

        res(reply);
      });
    });
  },
};