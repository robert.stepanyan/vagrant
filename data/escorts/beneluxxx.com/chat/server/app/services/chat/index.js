const clc = require("cli-color");
const db = require("../../db");
const config = require("../../config");
import Helpers from "../../util/helpers";

module.exports = {

  _storage: null,
  TABLES: {
    MESSAGES: 'chatroom_messages',
    THREADS: 'chatroom_threads',
    USER_THREADS: 'chatroom_user_threads',
    ATTACHMENTS: 'chatroom_attachments',
    INVITATIONS: 'chatroom_invitations',
    USERS: 'users',
    FAKE_USERS: 'chat_fake_users_schedule',
    ESCORTS: 'escorts',
    ESCORT_PHOTOS: 'escort_photos'
  },

  /**
   * @param {Object} storage
   * @returns {Object} ref
   */
  setStorage(storage) {
    this._storage = storage;
    return this;
  },

  /**
   * @returns {(Object|null)} storage
   */
  getStorage() {
    if (!this._storage) throw new Error("Storage was not passed to ChatService!");
    return this._storage;
  },

  /**
   * When server starts, need to remove all previous data
   * to avoid duplications and other expired information.
   */
  purifyStorageContents() {

    let storageAdapter = this.getStorage().getAdapter();
    let storage = this.getStorage();

    let removeInfoAboutOldConnectedUsers = new Promise((res, rej) => {
      storageAdapter
        .multi()
        .keys('connected-*', async (err, replies) => {
          if (err) throw err;

          let promises = [];

          for (let i = 0; i < replies.length; i++) {
            storageAdapter.del(replies[i]);
          }

          res();
        })
        .exec();
    });

    return Promise.all([removeInfoAboutOldConnectedUsers]);
  },

  /**
   * Updates invitations, first object specifies is WHERE
   * clause, second object specifies what to update
   * @param userId
   * @param is_seen
   * @returns {Promise<void>}
   */
  async updateInvitations(conditionFields, fieldsToUpdate) {
    try {

      let updateFields = [];
      let whereClause = [];
      let params = [];

      for (let key in fieldsToUpdate) {
        updateFields.push(key + ' = ?');
        params.push(fieldsToUpdate[key]);
      }

      for (let key in conditionFields) {
        whereClause.push(key + ' = ?');
        params.push(conditionFields[key]);
      }

      let sql = `UPDATE ${this.TABLES.INVITATIONS} SET ${updateFields.join(', ')} WHERE ${whereClause.join('AND ')} `

      return new Promise((res, rej) => {
        db.query(sql, params, (error, results) => {
          if (error) return rej(error);

          res(results);
        });
      })

    } catch (e) {
      console.log(clc.red(e));
    }
  },

  /**
   * Deleted invitations by specified options from DB
   * @param id
   * @returns {Promise<unknown>}
   */
  async removeInvitation({id}) {
    try {

      let sql = `DELETE FROM ${this.TABLES.INVITATIONS} WHERE id = ? `;
      let params = [id];

      return new Promise((res, rej) => {
        db.query(sql, params, (error, results) => {
          if (error) return rej(error);

          res(results);
        });
      })

    } catch (e) {
      console.log(clc.red(e));
    }
  },

  async updateRecentChatsByUser({userId, chatParticipant}) {
    try {
      let storageAdapter = this.getStorage().getAdapter();
      storageAdapter.zadd("recent-chats:" + userId, Date.now(),  chatParticipant);
    } catch (e) {
      console.log(clc.red(e));
    }
  },

  /**
   * @param threadId
   * @param Array<int>: excludeFor
   * @returns {Promise<unknown>}
   */
  async incrementUnreadMessagesCount({threadId, excludeFor}) {
    try {
      const sql = `
                UPDATE ${this.TABLES.USER_THREADS}
                SET unread_messages = unread_messages + 1 
                WHERE thread_id = ? AND user_id NOT IN (?)
            `;
      const params = [threadId, excludeFor];

      return new Promise((res, rej) => {
        db.query(sql, params, (error, results) => {
          if (error) return rej(error);

          res(results);
        });
      })
    } catch (e) {
      console.log(e.message);
    }
  },

  /**
   * Sets unread messages count to zero
   * @returns {Promise<void>}
   */
  async resetUnreadMessages({threadId, userId}) {
    try {
      const sql = `
                UPDATE ${this.TABLES.USER_THREADS}
                SET unread_messages = 0 
                WHERE thread_id = ? AND user_id = ?
            `;
      const params = [threadId, userId];

      return new Promise((res, rej) => {
        db.query(sql, params, (error, results) => {
          if (error) return rej(error);

          res(results);
        });
      })
    } catch (e) {
      console.log(e.message);
    }
  },

  /**
   * This retrieves users that are connected and therefore
   * stored into the Redis or whatever cache
   * NOTE! If options.grouped was passed as a "true" value
   * then users are going to be groups by their userType field.
   * @param {*} options
   */
  getConnectedUsers({grouped}) {

    let storageAdapter = this.getStorage().getAdapter();
    let storage = this.getStorage();
    let result = [];

    if (grouped) {
      result = {'escort': {}, 'agency': {}, 'member': {}};
    } else {
      result = {};
    }

    return new Promise((res, rej) => {
      storageAdapter
        .multi()
        .keys('connected-client:*', async (err, replies) => {
          if (err) throw err;

          let promises = [];
          for (let i = 0; i < replies.length; i++) {
            promises.push(storage.getAsync(replies[i]));
          }

          let connectedUsers = await Promise.all(promises);

          for (let i = 0; i < connectedUsers.length; i++) {
            let user = JSON.parse(connectedUsers[i]);

            if (grouped) {
              if (result.hasOwnProperty(user.user_type) == false) continue;
              result[user.user_type][user.id] = user;
            } else {
              result[user.id] = user;
            }
          }

          res(result);
        })
        .exec();
    });

  },

  /**
   * Retrieves chatroom state, meaning count of unread messages,
   * old created groups, recent chats and so on.
   * @param {*} userId
   */
  async getChatroomStateByUserId(userId) {
    try {
      const result = {
        recentChats: {},
        chatrooms: {},
        unreadMessages: {},
        unseenInvitationsCount: 0,
      };

      let [
        chatrooms,
        unseenInvitationsCount,
        unreadMessages
      ] = await Promise.all([
        this.getChatroomsByUser({
          userId,
          privacyLevel: config.constants.PRIVACY_LEVEL_PRIVATE_GROUP
        }),
        this.getUnseenInvitations({userId}),
        this.getUserUnreadMessages({userId})
      ]);

      result.chatrooms = chatrooms;
      result.unseenInvitationsCount = unseenInvitationsCount;
      result.recentChats = {};

	  for (let msg of unreadMessages) {
        result.unreadMessages[msg.guid] = {
          guid: msg.guid,
          messagesCount: msg.unread_messages,
          privacyLevel: msg.privacy_level
        };
		
		if (result.chatrooms && result.chatrooms[msg.guid]) {
			result.chatrooms[msg.guid].unreadMessagesCount = msg.unread_messages;
		}
	  }
	  let recentChats = await this.zrangeAsync('recent-chats:' + userId, 0, - 1);
      /*let recentChats = await this.getStorage().smembersAsync('recent-chats:' + userId);*/
      if (!recentChats) recentChats = [];

      // If recent chats are more than 3
      // remove all excepts last 3
      /*if (recentChats.length > 3) {
        for (let item of recentChats.slice(3)) {
          this.getStorage().getAdapter().srem('recent-chats:' + userId, item);
        }
      }*/

      for (let item of recentChats.slice(0, 3)) {
        item = JSON.parse(item);
        result.recentChats[item.id] = item;
      }

      return result;
    } catch (e) {
      console.log(clc.red(e));
    }

  },
  
  /**
   * Retrieves chatroom state, meaning count of unread messages,
   * old created groups, recent chats and so on.
   * @param {*} userId
   */
  async getRecentByUserId(userId) {
    try {
      let recents = {};
	  let recentChats = await this.zrangeAsync('recent-chats:' + userId, 0, - 1);
	   //redis.zrange( key, 0, 10, function( err, results ){
      /*let recentChats = await this.getStorage().smembersAsync('recent-chats:' + userId);*/
	 
      if (!recentChats) recentChats = [];
	  
      // If recent chats are more than 3
      // remove all excepts last 3
      /*if (recentChats.length > 3) {
        for (let item of recentChats.slice(3)) {
          this.getStorage().getAdapter().srem('recent-chats:' + userId, item);
        }
      }*/

      for (let item of recentChats.slice(0, 3)) {
        item = JSON.parse(item);
        recents[item.id] = item;
      }
	  
	  return recents
    } catch (e) {
      console.log(clc.red(e));
    }

  },
	
  /**
   * @param userId
   * @returns {Promise<void>}
   */
  async getUserUnreadMessages({userId}) {
    try {
      let sql = `SELECT t.guid, ut.unread_messages, t.privacy_level FROM ${this.TABLES.USER_THREADS} ut
        INNER JOIN ${this.TABLES.THREADS} t ON t.id = ut.thread_id
        WHERE ut.user_id = ? AND ut.unread_messages > 0`;
	  /*let sql = `SELECT cut.user_id, cm.thread_id, cm.sender_id, COUNT(*) as unread_messages,  
        ct.privacy_level, ct.guid FROM ${this.TABLES.USER_THREADS} cut
		INNER JOIN ${this.TABLES.THREADS} ct ON ct.id = cut.thread_id
		INNER JOIN ${this.TABLES.MESSAGES} cm ON cm.thread_id = cut.thread_id
		WHERE cut.user_id = ? AND cm.sender_id <> ? AND cm.is_read = 0
		GROUP BY cut.thread_id`	*/
      let params = [userId, userId];

      return new Promise((res, rej) => {
        db.query(sql, params, (error, results) => {
          if (error) return rej(error);

          res(results);
        });
      })

    } catch (e) {
      console.log(clc.red(e));
    }
  },

  /**
   * Returns count of unseen messages by specified params
   * @param userId
   * @returns {Promise<void>}
   */
  async getUnseenInvitations({userId}) {
    try {
      let sql = `SELECT count(id) as _count FROM ${this.TABLES.INVITATIONS} WHERE is_seen = 0 AND receiver_id = ?`;
      let params = [userId];

      return new Promise((res, rej) => {
        db.query(sql, params, (error, results) => {
          if (error) return rej(error);

          res(results && results[0] ? results[0]['_count'] : 0);
        });
      })

    } catch (e) {
      console.log(clc.red(e));
    }
  },

  /**
   * Store message and return result.
   * @param {*} options
   */
  async storeMessage(options) {

    try {
      let sql = `INSERT INTO ${this.TABLES.MESSAGES} (text, sender_id, date_created, sender_profile, thread_id) 
                VALUES(?, ?, ?, ?, (SELECT id from ${this.TABLES.THREADS} WHERE guid = '${options.roomGUID}'))`;
      let params = [options.text, options.sender.id, options.sentDate, options.sender.profile];

      return new Promise((res, rej) => {
        db.query(sql, params, (error, results) => {
          if (error) return rej(error);

          if (options.attachments && options.attachments.length) {
            this.storeAttachments({
              ...options,
              messageId: results.insertId
            });
          }

          res(results);
        });
      })

    } catch (e) {
      console.log(clc.red(e));
    }

  },

  /**
   * 2020-03-18T07:23:46.391Z | 11:23
   * 2020-03-18T06:23:46.000Z | 11:23
   *
   * Insert attachments bound to some message
   * @param {*} options
   */
  async storeAttachments(options) {

    try {
      let sql = `INSERT INTO ${this.TABLES.ATTACHMENTS} (message_id, file_url, file_type, original_name, date_created) VALUES ?`
      let params = [];

      let file;
      for (let i = 0; i < options.attachments.length; i++) {
        file = options.attachments[i];
        params.push([options.messageId, file.url, file.type, file.original_name, options.sentDate]);
      }

      return new Promise((res, rej) => {
        db.query(sql, [params], (error, results) => {
          if (error) return rej(error);

          res(results);
        });
      })

    } catch (e) {
      console.log(clc.red(e));
    }
  },

  /**
   * Insert new thread, but if it was
   * already created then return it's id.
   * @param {*} options
   */
  async createThreadIfNotExist({guid, privacyLevel, title = null}) {

    try {
      const sql = `
                INSERT INTO ${this.TABLES.THREADS} (guid, title, privacy_level, date_created) VALUES(?, ?, ?, NOW())
                ON DUPLICATE KEY UPDATE id = LAST_INSERT_ID(id)
            `;
      const params = [guid, title, privacyLevel];

      return new Promise((res, rej) => {
        db.query(sql, params, (error, results) => {
          if (error) return rej(error);

          res({
            id: results.insertId,
            privacyLevel: privacyLevel,
            guid,
            title
          });
        });
      })

    } catch (e) {
      console.log(clc.red(e));
    }
  },

  async getChatroomsByUser({userId, privacyLevel}) {

    try {
      const sql = `
                SELECT id, guid, title, is_admin, privacy_level FROM ${this.TABLES.THREADS} t
                INNER JOIN ${this.TABLES.USER_THREADS} ut 
                ON ut.thread_id = t.id
                WHERE ut.user_id = ? AND privacy_level ${privacyLevel instanceof Array ? 'in (?)' : '= ?'} 
            `;
      const params = [userId, privacyLevel];

      return new Promise((res, rej) => {
        db.query(sql, params, (error, rows) => {
          if (error) return rej(error);

          let result = {};
          for (let i = 0; i < rows.length; i++) {
            result[rows[i].guid] = rows[i];
          }

          res(result);
        });
      })

    } catch (e) {
      console.log(clc.red(e));
    }
  },

  /**
   * Removes user1 (kickedUserId) from chatrooms that belong to user2 (chatroomOwnerId)
   * @param chatroomOwnerId
   * @param kickedUserId
   * @returns {Promise<unknown>}
   */
  async kickFromChatrooms({chatroomOwnerId, kickedUserId}) {

    try {
      let sql, params;
      sql = `SELECT t.id, t.guid FROM ${this.TABLES.USER_THREADS} ut 
            INNER JOIN ${this.TABLES.THREADS} t
            ON t.id = ut.thread_id
            WHERE ut.is_admin = 1 AND ut.user_id = ?`;
      params = [chatroomOwnerId];

      // Select rooms where user2 was the admin
      // ----------------------------
      let roomsOfOwnerId = await new Promise((res, rej) => {
        db.query(sql, params, (error, rows) => {
          if (error) return rej(error);

          res(rows);
        });
      });

      let roomIds = roomsOfOwnerId.reduce((arr, item) => [...arr, item.id], []);
      if (!roomIds.length) return Promise.resolve({
        roomsOverall: [],
        kickedRoomsCount: 0,
      });

      // Perform DELETE operation to Kick from rooms
      // -----------------------
      sql = `DELETE FROM ${this.TABLES.USER_THREADS}
            WHERE user_id = ? 
            AND thread_id IN (${roomIds.join(',')}) 
      `;
      params = [kickedUserId];

      return new Promise((res, rej) => {
        db.query(sql, params, (error, result) => {
          if (error) return rej(error);

          res({
            roomsOverall: roomsOfOwnerId,
            kickedRoomsCount: result.affectedRows,
          });

        });
      })

    } catch (e) {
      console.log(clc.red(e));
    }
  },

  /**
   * Blocks conversation between user1 (initiatorId) and user2 (blockedUserId)
   * Afterwards only user1 will be able to unblock it.
   * @param initiatorId
   * @param blockedUserId
   * @returns {Promise<void>}
   */
  async blockUser({initiatorId, blockedUserId}) {

    try {
      let sql, params;
      sql = `UPDATE ${this.TABLES.THREADS} t 
            SET blocked_by = ?
            WHERE t.guid = ?
            AND blocked_by IS NULL`;
      params = [initiatorId, Helpers.getThreadGUID(initiatorId, blockedUserId)];

      return new Promise((res, rej) => {
        db.query(sql, params, (error, result) => {
          if (error) return rej(error);

          res(result);
        });
      })

    } catch (e) {
      console.log(clc.red(e));
    }
  },

  /**
   * Blocks conversation between user1 (initiatorId) and user2 (blockedUserId)
   * Afterwards only user1 will be able to unblock it.
   * @param initiatorId
   * @param blockedUserId
   * @returns {Promise<void>}
   */
  async unlockUser({initiatorId, blockedUserId}) {

    try {
      let sql, params;
      sql = `UPDATE ${this.TABLES.THREADS} t 
            SET blocked_by = ?
            WHERE t.guid = ?`;
      params = [null, Helpers.getThreadGUID(initiatorId, blockedUserId)];

      return new Promise((res, rej) => {
        db.query(sql, params, (error, result) => {
          if (error) return rej(error);

          res(result);
        });
      })

    } catch (e) {
      console.log(clc.red(e));
    }
  },

  /**
   * Add user to thread
   * @param {*} options
   */
  async joinUserToThread({threadId, userId, isAdmin}) {

    try {
      const sql = `
                INSERT IGNORE INTO ${this.TABLES.USER_THREADS} (thread_id, user_id, is_admin) VALUES(?, ?, ?)
            `;
      const params = [threadId, userId, isAdmin];

      return new Promise((res, rej) => {
        db.query(sql, params, (error, results) => {
          if (error) return rej(error);

          res(results);
        });
      })

    } catch (e) {
      console.log(clc.red(e));
    }
  },

  /**
   * Remove user from thread
   * @param {*} options
   */
  async extractUserFromThread({threadId, userId}) {

    try {
      const sql = `
                DELETE FROM ${this.TABLES.USER_THREADS} WHERE thread_id = ? AND user_id = ?
            `;
      const params = [threadId, userId];

      return new Promise((res, rej) => {
        db.query(sql, params, (error, results) => {
          if (error) return rej(error);

          res(results);
        });
      })

    } catch (e) {
      console.log(clc.red(e));
    }
  },

  /**
   * Returns messages based on passed parameters.
   * @param {*} options
   */
  async getMessagesChunk({threadId, period, page = 0}) {

    try {
      if (page < 0) page = 0;
      let pageSize = 20;
      let skip = page * pageSize;

      const sql = `
                SELECT m.id, m.sender_profile, m.date_created as sentDate,
                m.text, m.sender_id, u.username, 
                at.file_url as attachment_url, at.file_type as attachment_file_type, at.original_name as file_original_name

                FROM ${this.TABLES.MESSAGES} as m 
                LEFT JOIN ${this.TABLES.ATTACHMENTS} as at ON at.message_id = m.id
                INNER JOIN users u ON u.id = m.sender_id
                WHERE m.thread_id = ? AND m.date_created >= ?
                ORDER BY m.date_created DESC
                LIMIT ?, ?
            `;
      const params = [threadId, this._mapPeriodToSql(period), skip, pageSize];

      return new Promise((res, rej) => {
        db.query(sql, params, (error, results) => {
          if (error) {
            console.log(clc.red(error))
            return rej(error);
          }

          let resultMessages = {};

          for (let i = 0; i < results.length; i++) {

            let message = results[i];

            resultMessages[message.id] = resultMessages[message.id] || {
              sentDate: message.sentDate,
              text: message.text,
              sender_id: message.sender_id,
              attachments: [],
              sender: {
                id: message.sender_id,
                username: message.username,
                profile: JSON.parse(message.sender_profile)
              }
            }

            if (message.attachment_url) {
              resultMessages[message.id].attachments.push({
                original_name: message.file_original_name,
                url: message.attachment_url,
                type: message.attachment_file_type
              })
            }
          }

          res({
            map: resultMessages,
            hasMore: results.length >= pageSize,
          });
        });
      })

    } catch (e) {
      console.log(clc.red(e));
    }
  },

  /**
   * Returns Thread by passed GUID
   * @param {(Object|null)} guid
   */
  async getThreadByGUID(guid) {

    try {
      const sql = `
                SELECT * from ${this.TABLES.THREADS} 
                WHERE guid = ?
            `;
      const params = [guid];

      return new Promise((res, rej) => {
        db.query(sql, params, (error, results) => {
          if (error) return rej(error);

          res(results && results[0] ? results[0] : null);
        });
      })

    } catch (e) {
      console.log(clc.red(e));
    }
  },

  /**
   * Returns thread by specified id
   * @param id
   * @returns {Promise<unknown>}
   */
  async getThreadById(id) {

    try {
      const sql = `
                SELECT * from ${this.TABLES.THREADS} 
                WHERE id = ?
            `;
      const params = [id];

      return new Promise((res, rej) => {
        db.query(sql, params, (error, results) => {
          if (error) return rej(error);

          res(results && results[0] ? results[0] : null);
        });
      })

    } catch (e) {
      console.log(clc.red(e));
    }
  },

  /**
   * R
   * @param {(Object|null)} guid
   */
  async isUserInRoom({threadId, userId}) {

    try {
      const sql = `
                SELECT true from ${this.TABLES.USER_THREADS} 
                WHERE user_id = ? AND thread_id = ?
            `;
      const params = [userId, threadId];

      return new Promise((res, rej) => {
        db.query(sql, params, (error, results) => {
          if (error) return rej(error);

          res(results.length > 0);
        });
      })

    } catch (e) {
      console.log(clc.red(e));
    }
  },

  /**
   * Stores invitation into DB
   * @param initiatorId
   * @param threadId
   * @param receiverId
   * @returns {Promise<void>}
   */
  async addInvitation({initiatorId, threadId, receiverId, dateCreated = new Date()}) {
    try {
      const sql = `
                INSERT IGNORE INTO ${this.TABLES.INVITATIONS} (initiator_id, receiver_id, thread_id, date_created)
                VALUES (?, ?, ?, ?)
            `;
      const params = [initiatorId, receiverId, threadId, dateCreated];

      return new Promise((res, rej) => {
        db.query(sql, params, (error, results) => {
          if (error) return rej(error);

          res(results);
        });
      })

    } catch (e) {
      console.log(clc.red(e));
    }
  },

  /**
   * Filters invitation data by specified params
   * @param initiatorId
   * @param threadId
   * @param receiverId
   * @returns {Promise<unknown>}
   */
  async getInvitation({threadId, receiverId}) {
    try {
      const sql = `
                SELECT * FROM ${this.TABLES.INVITATIONS} 
                WHERE receiver_id = ? AND thread_id = ? 
            `;
      const params = [receiverId, threadId];

      return new Promise((res, rej) => {
        db.query(sql, params, (error, results) => {
          if (error) return rej(error);

          res(results && results[0] ? results[0] : null);
        });
      })

    } catch (e) {
      console.log(clc.red(e));
    }
  },

  /**
   * Returns invitations list of specified user
   * @param userId
   * @returns {Promise<void>}
   */
  async getInvitationsArchive({userId}) {
    try {
      const sql = `
                SELECT i.id, u.username, t.title, i.thread_id, t.guid, t.privacy_level FROM ${this.TABLES.INVITATIONS} i 
                INNER JOIN ${this.TABLES.THREADS} t ON i.thread_id = t.id
                INNER JOIN ${this.TABLES.USERS} u ON i.initiator_id = u.id
                WHERE receiver_id = ? 
            `;
      const params = [userId];

      return new Promise((res, rej) => {
        db.query(sql, params, (error, results) => {
          if (error) return rej(error);

          res(results);
        });
      })

    } catch (e) {
      console.log(clc.red(e));
    }
  },

  async getFakeOnlineUsers() {
    try {
      const sql = `SELECT e.showname, e.id, e.user_id, ep.hash AS photo_hash, ep.ext AS photo_ext, cfus.date_to
        FROM ${this.TABLES.FAKE_USERS} cfus 
        INNER JOIN ${this.TABLES.ESCORTS} e ON e.user_id = cfus.user_id
        INNER JOIN ${this.TABLES.ESCORT_PHOTOS} ep ON ep.escort_id = e.id AND ep.is_main = 1
        WHERE NOW() > cfus.date_from AND NOW() < cfus.date_to AND is_removed = 0
        `;

      return new Promise((res, rej) => {
        db.query(sql, [], (error, results) => {
          if (error) return rej(error);

          var data = {};
          for (var i in results) {
            let parts = [];
            let escort = results[i];

            if (escort.id > 99) {
              var id = escort.id + '';
              parts.push(id.substr(0, 2));
              parts.push(id.substr(2));
            } else {
              parts.push('_');
              parts.push(escort.id);
            }

            let catalog = parts.join('/');
            let d = new Date(escort.date_to);

            data[escort.user_id] = {
              id: escort.user_id,
              user_type: 'escort',
              username: escort.showname,
              chat_info: {
                'nickName': escort.showname + ' (Escort)',
                'userId': escort.user_id,
                'userType': 'escort',
                'timeToo': d.getTime(), // chat fake users session time in miliseconds
                'link': '/escort/' + escort.showname + '-' + escort.id,
                'avatar': process.env.IMAGE_HOST + '/' + catalog + '/' + escort.photo_hash + '_lvthumb.' + escort.photo_ext
              }
            };
          }

          res(data);
        });
      })

    } catch (e) {
      console.log(clc.red(e));
    }
  },
  
  async getExpiredFakeUsers() {
    try {
      const sql = `SELECT user_id
        FROM ${this.TABLES.FAKE_USERS}  
        WHERE NOW() > date_to OR is_removed = 1
        `;

      return new Promise((res, rej) => {
        db.query(sql, [], (error, results) => {
          if (error) return rej(error);
          res(results);
        });
      })

    } catch (e) {
      console.log(clc.red(e));
    }
  },
  
  async deleteExpiredFakeUser(userId) {
    try {
      const sql = `DELETE FROM ${this.TABLES.FAKE_USERS} WHERE user_id  = ?`;
	 
      return new Promise((res, rej) => {
        db.query(sql, [userId], (error, results) => {
          if (error) return rej(error);
          res(results);
        });
      })

    } catch (e) {
      console.log(clc.red(e));
    }
  },
  /**
   * Simply returns SQL based on passed parameter.
   * @param {*} period
   */
  _mapPeriodToSql(period) {

    const periodMapper = {
      [config.constants.PERIOD_LAST_3_DAYS]: 'DATE_ADD(CURDATE(), INTERVAL -3 DAY)'
    };

    return (periodMapper[period]) ? periodMapper[period] : null;
  },
  
  
  zrangeAsync(key, from , to) {
	  
	//console.log('zrange trigered');  
    return new Promise((res, rej) => {
      this.getStorage().getAdapter().zrange(key, 0, -1, function (err, reply) {
        if (err) rej(err);

        res(reply);
      });
    });
  },

};