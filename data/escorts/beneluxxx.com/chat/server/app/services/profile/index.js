const db = require("../../db");
const config = require("../../config");

module.exports = {

    _storage: null,
    TABLES: {
        CITIES: 'cities',
        COUNTRIES: 'countries',
    },

    /**
     * Returns location object based on passed cityId
     * @param {*} cityId 
     * @returns {Object}
     */
    async getLocationByCityId (cityId) {

        try {
            const sql = `SELECT 
                ct.id as city_id, ct.title_en as city_title,
                c.id as country_id, c.title_en as country_title

                FROM ${this.TABLES.CITIES} as ct
                INNER JOIN ${this.TABLES.COUNTRIES} c ON c.id = ct.country_id
                WHERE ct.id = ?
            `;
            const params = [cityId];

            return new Promise((res, rej) => {
                db.query(sql, params, (error, results) => {
                    if (error) return rej(error);

                    res(results && results.length ? results[0] : null);
                });
            })

        } catch (e) {
            console.log(e);
        }
    }
};