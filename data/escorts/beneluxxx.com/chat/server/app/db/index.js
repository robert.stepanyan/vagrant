const mysql = require('mysql');
const config = require('../config');

const pool = mysql.createPool({
    connectionLimit: config.db.connectionLimit,
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.name,
    dateStrings : true
});

module.exports = pool;