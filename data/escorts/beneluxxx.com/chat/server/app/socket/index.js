'use strict';

const http = require('http');

const redisAdapter = require('socket.io-redis');
const redisService = require("../services/redis");

const ioEvents = require("./ioEvents");

/**
 * Initialize Socket.io
 * Uses Redis as Adapter for Socket.io
 *
 */

const handler = function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/plain'});
  res.end('ok');
};

module.exports = function (options) {

  const {port} = options;
  const server = http.createServer(handler)

  const io = require('socket.io')(server, {
    path: '/chatroom',
  });

  // Force Socket.io to ONLY use "websockets"; No Long Polling.
  io.set('transports', ['websocket', 'polling']);

  // Using Redis
  io.adapter(redisAdapter({
    redisClient: redisService.getAdapter()
  }));

  // Define all Events
  ioEvents(io, redisService);

  server.listen(port, function (error) {
    if (error) throw error;
    console.log("Socket Server started on port " + port);
  });

  return io;
};