const clc = require("cli-color");
const chatService = require("../services/chat");
const profilesService = require("../services/profile");
import Helpers from "../util/helpers";

const config = require("../config");

// Constants
const CHATROOM_NS = config.constants.CHATROOM_NS

/**
 * Encapsulates all code for emitting and listening to socket events
 */
module.exports = function (io, redisService) {

  chatService.setStorage(redisService);

  // Remove old data
  chatService.purifyStorageContents();

  /**
   * Update users list in sidebar
   * for each user that is connected
   */
  async function sendConnectedUsersListToAll() {
    const connectedUsersList = await chatService.getConnectedUsers({grouped: false});
	const expiredFakeUsers = await chatService.getExpiredFakeUsers();
    const fakeUsers = await chatService.getFakeOnlineUsers();
	
	for (let id in expiredFakeUsers) {
		redisService.getAdapter().del('connected-client:' + expiredFakeUsers[id].user_id);
		await chatService.deleteExpiredFakeUser(expiredFakeUsers[id].user_id);
	}
	
    for (let id in fakeUsers) {
      let user = fakeUsers[id];
      let fakeSocketId = user.id;
      redisService.getAdapter().set('connected-client:' + fakeSocketId, JSON.stringify(user));
      redisService.getAdapter().set('connected-user:' + user.id, fakeSocketId);
    }

    io.of(CHATROOM_NS).emit('connected-users-list', {...fakeUsers, ...connectedUsersList});
  }

  // Chatroom namespace
  io.of(CHATROOM_NS).on('connection', function (socket) {

    socket.on('new-connection', async function (params) {

      try {

        const user = params.user;
        user.location = await profilesService.getLocationByCityId(user.city_id);
        user.socketId = socket.id;

        // Disconnect duplicate Socket connections
        let duplicateSocket = await redisService.getAsync('connected-user:' + user.id);
        if (duplicateSocket) {
          io.of(CHATROOM_NS).sockets[duplicateSocket]?.emit('force-disconnect', {reason: 'duplicate-tab'});
        }

        console.log(clc.green('User: ' + user.id + ' connected socket ID' + socket.id ));

        redisService.getAdapter().set('connected-client:' + socket.id, JSON.stringify(user));
        redisService.getAdapter().set('connected-user:' + user.id, socket.id);

        sendConnectedUsersListToAll();

        const initialState = await chatService.getChatroomStateByUserId(user.id);

        // Join user automatically so
        // that he can receive notifications
        for (let key in initialState.chatrooms) {
          socket.join(initialState.chatrooms[key].guid);
        }

        socket.emit('update-chatroom-state', initialState);

      } catch (e) {
        console.log(e.message);
      }

    });

    // When a socket exits
    socket.on('disconnect', async function (params) {

      try {

        let user = await redisService.getAsync('connected-client:' + socket.id);
        user = JSON.parse(user);

        // Remove user from connected list
        redisService.getAdapter().del('connected-client:' + socket.id);
        //redisService.getAdapter().del('connected-user:' + user.id);

        sendConnectedUsersListToAll();

        console.log("User Disconnected " + user.id);
      } catch (e) {
        console.log(e.message);
      }

    });

    // When a new message arrives
    socket.on('send-message', async function (data) {

      let user = await redisService.getAsync('connected-client:' + socket.id);
      user = JSON.parse(user);

      let messageData = {
        text: data.text,
        attachments: data.attachments,
        sentDate: data.sentDate,
        roomGUID: data.roomGUID,
        sender: {
          ...data.sender,
          username: user.username,
          type: user.user_type,
          profile: JSON.stringify({
            avatar: user.chat_info.avatar,
            location: user.location
          })
        }
      };

      const [result, thread] = await Promise.all([
        chatService.storeMessage(messageData),
        chatService.getThreadByGUID(data.roomGUID)
      ]);

      if (result.affectedRows > 0) {

        messageData = {
          ...messageData,
          sentDate: data.sentDate,
          privacyLevel: parseInt(thread.privacy_level)
        };

        if (messageData.roomGUID == process.env.GENERAL_CHAT_GUID) {
          socket.broadcast.emit('new-message', messageData);
        } else if (parseInt(thread.privacy_level) === config.constants.PRIVACY_LEVEL_DIRECT) {

          const receiverId = Helpers.getPartnerId(data.roomGUID, user.id);
          let receiverSocketId = await redisService.getAsync('connected-user:' + receiverId);

          io.of(CHATROOM_NS).to(receiverSocketId).emit('new-message', messageData);
		  
		  // Insert receiver user into User threads table
		  // as a participant of this room
		  await chatService.joinUserToThread({
			threadId: thread.id,
			userId: receiverId,
			isAdmin: 0
		  });
		  
		  chatService.incrementUnreadMessagesCount({threadId: thread.id, excludeFor: [user.id]});
		  
        } else if (parseInt(thread.privacy_level) === config.constants.PRIVACY_LEVEL_PRIVATE_GROUP) {

          socket.broadcast.to(data.roomGUID).emit('new-message', messageData);
		 
          chatService.incrementUnreadMessagesCount({threadId: thread.id, excludeFor: [user.id]});
        }
      }
    });

    // Update unread count of messages
    socket.on('reading-messages', async ({roomGUID}) => {

      let user = await redisService.getAsync('connected-client:' + socket.id);
      user = JSON.parse(user);
	  
      const thread = await chatService.getThreadByGUID(roomGUID);

      chatService.resetUnreadMessages({threadId: thread.id, userId: user.id});
    });

    // When user opens messages
    socket.on('load-messages', async ({roomGUID, page}) => {

      let thread = await chatService.getThreadByGUID(roomGUID);
      let result = await chatService.getMessagesChunk({
        threadId: thread.id,
        period: config.constants.PERIOD_LAST_3_DAYS,
        page
      });
	  
      socket.emit('messages-chunk', {page, roomGUID, messages: Object.values(result.map), hasMore: result.hasMore});
    });

    // When user joins a new room
    socket.on('join-room', async ({roomGUID, privacyLevel}) => {

      let user = await redisService.getAsync('connected-client:' + socket.id);
      user = JSON.parse(user);

      // If user is opening for the first time this tread
      // then lest insert it, otherwise just return id of
      // existing thread
      await chatService.createThreadIfNotExist({
        guid: roomGUID,
        adminId: user.id,
        privacyLevel
      });

      const thread = await chatService.getThreadByGUID(roomGUID);

      // Retrieve last 3 days messages
      // to show in Conversation panel
      const messages = await chatService.getMessagesChunk({
        threadId: thread.id,
        period: config.constants.PERIOD_LAST_3_DAYS
      });

      // Insert user into User threads table
      // as a participant of this room
      await chatService.joinUserToThread({
        threadId: thread.id,
        userId: user.id,
        isAdmin: 0
      });

      socket.join(roomGUID);
	  
      if (parseInt(thread.privacy_level) === config.constants.PRIVACY_LEVEL_DIRECT) {

        let otherUserId = Helpers.getPartnerId(roomGUID, user.id);

        let otherUserSocketId = await redisService.getAsync('connected-user:' + otherUserId);
        let otherUser = await redisService.getAsync('connected-client:' + otherUserSocketId);
		
        if (otherUser) {
          chatService.updateRecentChatsByUser({
            userId: user.id,
            chatParticipant: otherUser
          });
        }
		
		// OPTIMIZE LATER MADE BY NOOB 
		//let receiverSocketId = await redisService.getAsync('connected-user:' + receiverId);
		let recentChat = await chatService.getRecentByUserId(user.id);
		socket.emit('new-recent-chat', recentChat);
		//io.of(CHATROOM_NS).to(receiverSocketId).emit('new-recent-chat', recentChat);
		// ----------------
		
      } else if (thread.privacy_level === config.constants.PRIVACY_LEVEL_PRIVATE_GROUP) {

        // Store GUID of this room in list, to remember
        // which rooms this user is joined already
        //redisService.getAdapter().sadd("user-joined-rooms:" + user.id, thread.guid);
      }

      // Return to user the history of this thread
      socket.emit('room-history', {
        messages: [], // Object.values(messages.map),
        thread: {
          guid: roomGUID,
          id: thread.id,
          blockedBy: thread.blocked_by
        }
      });
    });

    // When user leaves from group chat
    socket.on('leave-room', async ({roomGUID, roomId}) => {

      let user = await redisService.getAsync('connected-client:' + socket.id);
      user = JSON.parse(user);

      // If user is opening for the first time this tread
      // then lest insert it, otherwise just return id of
      // existing thread
      await chatService.extractUserFromThread({
        userId: user.id,
        threadId: roomId,
      });

      socket.leave(roomGUID);
    });

    socket.on('invitation-accept', async ({roomGUID}) => {

      let user = await redisService.getAsync('connected-client:' + socket.id);
      user = JSON.parse(user);

      const thread = await chatService.getThreadByGUID(roomGUID);

      // Insert user into User threads table
      // as a participant of this room
      await chatService.joinUserToThread({
        threadId: thread.id,
        userId: user.id,
        isAdmin: 0
      });

      // Store GUID of this room, IN list for this user, to remember
      // which rooms this user is in already
      // redisService.getAdapter().sadd("user-joined-rooms:" + user.id, thread.guid);
      socket.join(roomGUID);

      // Return to user the history of this thread
      socket.emit('join-to-chatroom', {
        //messages,
        room: {
          id: thread.id,
          guid: thread.guid,
          title: thread.title,
          is_admin: 0,
          privacy_level: thread.privacy_level
        }
      });
    });

    // When user creates a new chatroom
    socket.on('create-chatroom', async ({name}, cb = null) => {

      let user = await redisService.getAsync('connected-client:' + socket.id);
      user = JSON.parse(user);

      const thread = await chatService.createThreadIfNotExist({
        guid: user.id + '-0-' + name,
        title: name,
        adminId: user.id,
        privacyLevel: config.constants.PRIVACY_LEVEL_PRIVATE_GROUP
      });

      await chatService.joinUserToThread({
        threadId: thread.id,
        userId: user.id,
        isAdmin: 1
      });

      const chatrooms = await chatService.getChatroomsByUser({
        userId: user.id,
        privacyLevel: config.constants.PRIVACY_LEVEL_PRIVATE_GROUP
      });

      if (cb) {
        cb({chatrooms});
      }
    });

    // When user Invites another user to one of his/her ChatRooms
    socket.on('invite-to-chatroom', async ({roomId, userId}, cb = null) => {

      const response = {};

      let user = await redisService.getAsync('connected-client:' + socket.id);
      user = JSON.parse(user);

      let [
        wasUserInRoom,
        duplicateInvitation,
        roomData
      ] = await Promise.all([
        chatService.isUserInRoom({threadId: roomId, userId}), // Check in db if user was already in this room
        chatService.getInvitation({threadId: roomId, receiverId: userId}),
        chatService.getThreadById(roomId) // Select rooms details to show room details in notification
      ]);

      if (duplicateInvitation) {
        response.status = 'error';
        response.message = 'This user has been invited to this room!';
      } else if (wasUserInRoom) {
        response.status = 'error';
        response.message = 'This user is already joined!';
      } else {
        response.status = 'success';
      }

      // send invitation to exact user
      if (!wasUserInRoom && !duplicateInvitation) {

        chatService.addInvitation({
          initiatorId: user.id,
          threadId: roomId,
          receiverId: userId
        });

        let receiverSocketId = await redisService.getAsync('connected-user:' + userId);
        io.of(CHATROOM_NS).to(receiverSocketId).emit('chatroom-invitation', {
          inviter: user,
          room: roomData
        });
      }

      if (cb) {
        cb(response);
      }
    });

    // When user opens notifications popup
    socket.on('get-invitations', async (options, cb = null) => {

      let user = await redisService.getAsync('connected-client:' + socket.id);
      user = JSON.parse(user);

      let [
        invitationsArchive,
      ] = await Promise.all([
        chatService.getInvitationsArchive({userId: user.id})
      ]);

      if (cb) {
        cb({
          invitations: invitationsArchive
        });
      }

      chatService.updateInvitations({receiver_id: user.id}, {is_seen: 1})
    });

    // When user removes invitations request
    socket.on('delete-invitation', async ({id}) => {

      chatService.removeInvitation({id})

    });

    socket.on('kick-user', async ({userId}, cb = null) => {

      let user = await redisService.getAsync('connected-client:' + socket.id);
      user = JSON.parse(user);

      const result = await chatService.kickFromChatrooms({chatroomOwnerId: user.id, kickedUserId: userId});

      if (cb) {
        cb({
          status: 'success',
          kickedRoomsCount: result.kickedRoomsCount
        });
      }

      // Notify user that he was kicked
      let receiverSocketId = await redisService.getAsync('connected-user:' + userId);
      io.of(CHATROOM_NS).to(receiverSocketId).emit('kicked-from-rooms', result.roomsOverall);

      // Exclude user automatically so
      // to make sure he wont get notifications anymore
      result.roomsOverall.forEach(room => {
        io.of(CHATROOM_NS).sockets[receiverSocketId].leave(room.guid);
      });

    });

    socket.on('unlock-user', async ({userId}, cb = null) => {

      let user = await redisService.getAsync('connected-client:' + socket.id);
      user = JSON.parse(user);

      const result = await chatService.unlockUser({initiatorId: user.id, blockedUserId: userId});

      if (cb) {
        cb({
          status: 'success',
          result
        });
      }

      // Notify user that he was blocked
      let receiverSocketId = await redisService.getAsync('connected-user:' + userId);
      io.of(CHATROOM_NS).to(receiverSocketId).emit('update-blocked-chat', {
        roomGUID: Helpers.getThreadGUID(user.id, userId),
        blockedBy: null
      });
    });

    socket.on('block-user', async ({userId}, cb = null) => {

      let user = await redisService.getAsync('connected-client:' + socket.id);
      user = JSON.parse(user);

      const result = await chatService.blockUser({initiatorId: user.id, blockedUserId: userId});

      if (cb) {
        cb({
          status: 'success',
          result
        });
      }

      // Notify user that he was blocked
      let receiverSocketId = await redisService.getAsync('connected-user:' + userId);
      io.of(CHATROOM_NS).to(receiverSocketId).emit('update-blocked-chat', {
        roomGUID: Helpers.getThreadGUID(user.id, userId),
        blockedBy: user.id
      });
    });

    socket.on('get-profile-link', async ({userId}, cb = null) => {

      const response = {status: 'error'};

      try {
        let userSocketId = await redisService.getAsync('connected-user:' + userId);
        let profile = await redisService.getAsync('connected-client:' + userSocketId);
        profile = JSON.parse(profile);

        response.status = 'success';
        response.link = profile.chat_info.link;
      } catch (e) {
        console.log(clc.red(e.message));
      }

      if (cb) {
        cb(response);
      }
    });

  });
}
