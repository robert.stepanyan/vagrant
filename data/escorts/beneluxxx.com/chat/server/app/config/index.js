'use strict';

var init = function () {
  return {
    db: {
      connectionLimit: process.env.DB_CONNECTIONS_LIMIT,

      user: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      host: process.env.DB_HOST,
      port: process.env.DB_PORT,
      name: process.env.DB_NAME
    },
    redis: {
      host: process.env.REDIS_HOST,
      port: process.env.REDIS_PORT,
    },
    constants: {
      PERIOD_LAST_3_DAYS: 'LAST 3 DAYS',

      PRIVACY_LEVEL_GENERAL_CHAT: 1,
      PRIVACY_LEVEL_PRIVATE_GROUP: 2,
      PRIVACY_LEVEL_DIRECT: 3,

      CHATROOM_NS: '/chatroom',
    },
    server: {
      ssl_key: process.env.SSL_KEY_PATH,
      ssl_cert: process.env.SSL_CERT_PATH,
    }
  }
}

module.exports = init();