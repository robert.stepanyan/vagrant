process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const dotEnv = require('dotenv-flow').config();
const config = require('./config');

const ExpressProxy = require('./modules/ExpressProxy');
const SocketServer = require("./socket");

const options = {
  port: process.env.EXPRESS_PORT || 3000,
  key: config.server.ssl_key,
  cert: config.server.ssl_cert
};

const io = SocketServer({
  port: process.env.SOCKET_IO_PORT || 3001
});
const expressApp = new ExpressProxy({...options, io}).build().run();
