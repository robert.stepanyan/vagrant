import express from 'express';
import cors from 'cors';
import fs from 'fs';
import routes from '../routes';
import Helpers from "../util/helpers";

class ExpressProxy {

  constructor(options) {
    this.options = options;
    this.app = express();
    this.httpAdapter = Helpers.isDevelopment() ? require('http') : require('https');

    this.app.set('io', options.io);
    if (this.options.key) {
      this.options.key = fs.readFileSync(this.options.key);
    }

    if (this.options.cert) {
      this.options.cert = fs.readFileSync(this.options.cert);
    }
  }

  /**
   * Configures all Middleware handlers and Routes definitions
   *
   * @returns {ExpressProxy}
   */
  build() {
    this.setupMiddleware();
    this.setupRoutes();
    return this;
  }

  /**
   * Starts http(s) server which listens to the
   * port passed via options to constructor.
   *
   * @returns {instance of express server}
   */
  run() {
    const {port, key, cert} = this.options;

    const httpServer = this.httpAdapter.createServer({key, cert}, this.app);
    httpServer.listen(port, (error) => {
      if (error) throw error;
      console.log("Express Server started on port " + port);
    });

    return this;
  }

  /**
   * Setup Cors, body parser, static file definitions etc...
   *
   * @returns {void}
   */
  setupMiddleware() {
    this.app.use(express.urlencoded());
    this.app.use(express.json());
    this.app.use('/chatroom/attachments', express.static(__dirname + '/../../public/attachments'));
    this.app.use('/chatroom/static', express.static(__dirname + '/../../../front/build/static'));

    this.app.use(cors({
      "origin": "*",
      "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
      "preflightContinue": false,
      "optionsSuccessStatus": 204
    }));
  }

  /**
   * Basic route definitions
   *
   * @returns {void}
   */
  setupRoutes() {
    this.app.use('/chatroom', routes);
  }
}

module.exports = ExpressProxy;