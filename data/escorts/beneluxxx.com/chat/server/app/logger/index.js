'use strict';

var winston = require('winston');

var logger = winston.createLogger({
	transports: [
		new (winston.transports.File)({
			level: 'error',
			json: true,
			filename: './debug.log',
			handleExceptions: true
		}),
		new (winston.transports.Console)({
			level: 'error',
			json: true,
			handleExceptions: true
		})
	],
	exitOnError: false
});

module.exports = logger;