<?php

class CamsApi_VideoBookingController extends Zend_Controller_Action
{
	/**
	 * @var Zend_Db_Adapter_Mysqli
	 */
	protected $_db;
	
	public function init()
	{
		$this->_db = Zend_Registry::get('db');
		$system = Zend_Registry::get('system_config');
		$this->secret = $system['cams']['secret'];
		$this->errors = array(
			'error' => array(
				"code" => "ERR_VALIDATION",
				"details" => array()  
			)
		);
		$headers = var_export($_SERVER, true);
		$rawdata = file_get_contents("php://input");
		
		if( $this->secret != $_SERVER['HTTP_X_SERVER_SECRET']){
			header("HTTP/1.1 401 Unauthorized"); die;
		}
		elseif (!$this->_request->isPost())
		{
			header($_SERVER["SERVER_PROTOCOL"]." 405 Method Not Allowed", true, 405); die;
		}
	}

	public function purchaseAction()
	{
		header('Content-Type: application/json');
		$model_vc = new Model_VideoChatRequests();
		$duration_min = [ 15 , 30 ];
		$booking_time_options = $this->_renderBookingTimes();
		// ask also amount 
		$rawdata = file_get_contents("php://input");

		$decoded = json_decode($rawdata, true);
		
		$is_valid = true;

		if(!isset($decoded['remote_id'])){
			$this->errors['error']['details'][] = array("field" => "remote_id",	"validator"=> "required");
			$is_valid = false;
		}
		elseif(!is_numeric($decoded['remote_id'])){
			$this->errors['error']['details'][] = array("field" => "remote_id",	"validator" => "invalid");
			$is_valid = false;
		}

		if(!isset($decoded['duration'])){
			$this->errors['error']['details'][] = array("field" => "duration",	"validator"=> "required");
			$is_valid = false;
		}
		elseif(!in_array($decoded['duration'], $duration_min )){
			$this->errors['error']['details'][] = array("field" => "duration",	"validator"=> "invalid");
			$is_valid = false;
		}
		
		if(!isset($decoded['amount'])){
			$this->errors['error']['details'][] = array("field" => "amount",	"validator"=> "required");
			$is_valid = false;
		}
		elseif(!is_numeric($decoded['amount'])){
			$this->errors['error']['details'][] = array("field" => "amount",	"validator"=> "must be numeric");
			$is_valid = false;
		}
				
		if(!isset($decoded['booking_time'])){
			$this->errors['error']['details'][] = array("field" => "booking_time",	"validator"=> "required");
			$is_valid = false;
		}
		elseif(!in_array($decoded['booking_time'], $booking_time_options )){
			$this->errors['error']['details'][] = array("field" => "booking_time",	"validator"=> "invalid");
			$is_valid = false;
		}
		
		if(!isset($decoded['member_username'])){
			$this->errors['error']['details'][] = array("field" => "member_username",	"validator"=> "required");
			$is_valid = false;
		}
		elseif(!preg_match('/^[A-Za-z0-9_\-\.]+$/', $decoded['member_username'])){
			$this->errors['error']['details'][] = array("field" => "member_username",	"validator"=> "invalid");
			$is_valid = false;
		}
		
		if(isset($decoded['video_booking_id'])){
			
			// GET video_booking request
			if(!$model_vc->checkExists($decoded['video_booking_id'])){
				$this->errors['error']['details'][] = array("field" => "video_booking_id",	"validator"=> "ID dosn't exists");
				$is_valid = false;
			}
			
		}
		
		if($is_valid){
			header("HTTP/1.1 200 OK");
			
			$escort_id = $decoded['remote_id'];
			$request_id = $decoded['video_booking_id'];
			
			
			$notification_data = $model_vc->getDataforNotification($escort_id); 
			//echo json_encode($notification_data);die;
			$data = array(
				'schedule_time' => $decoded['booking_time'],
				'escort_id' => $escort_id,
				'agency_id' => $notification_data['agency_id'],
				'duration' => $decoded['duration'],
				'amount' => $decoded['amount'],
				'username' => $decoded['member_username'],
				'status' => 2
			);
			
			if(isset($request_id)){
				$model_vc->update($request_id, $data);
			}
			else{
				$request_id = $model_vc->add($data);
			}
			
			$notification_data['schedule_time'] = $decoded['booking_time'];
			$notification_data['duration'] = $decoded['duration'];
			$notification_data['amount'] = $decoded['amount'];
			
			$escort_data = ['escort_id' => $escort_id, 'showname' => $notification_data['showname'] ];		
			$model_vc->sendSmsCam($escort_data, $decoded['member_username'], $notification_data, $request_id);
		}
		else{
			header('HTTP/1.0 400 Bad Request');
			echo json_encode($this->errors);
		}
						
		die;
	}
	
	
	private function _renderBookingTimes()
	{
		$booking_times = [ 'ASAP', 'FLEXIBLE'];
		for( $i = 0; $i < 24; $i++ ){
			$hour = $i < 10 ? '0'. $i : $i;
			array_push($booking_times, $hour.':00', $hour.':30'); 
		}
		return $booking_times;
	}		
}
