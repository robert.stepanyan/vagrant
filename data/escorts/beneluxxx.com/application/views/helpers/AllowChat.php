<?php

class Zend_View_Helper_AllowChat
{
	public function allowChat()
	{
		//return FALSE;
		$request = Zend_Controller_Front::getInstance()->getRequest();
		
		if ( $request->getParam('chatVersion') != 1 ) 
		{
			return false;
		}
		
		$user = Model_Users::getCurrent();
		
		if ($user)
		{			
			if ($user->user_type == 'escort')
			{
				if ($user->is_susp)
					return false;
			}		
		}
		
		return TRUE;
	}
}

