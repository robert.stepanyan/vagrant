<?php

class Zend_View_Helper_GetLink
{
	protected static $_params;
	
	public function getLink($type = '', array $args = array(), $clean = false, $no_args = false)
	{
		$lang_id = Cubix_I18n::getLang();

		$link = '/';
		/*if ( $lang_id != Cubix_Application::getDefaultLang() ) {
			$link .= $lang_id . '/';
		}*/
		
		if ( isset($args['state']) && 7 != Cubix_Application::getId() ) {
			$args['region'] = $args['state'];
			unset($args['state']);
		}
		
		switch ( $type ) {
			case 'photo-feed-index':
				$link .= 'photo-feed';
				break;
			case 'sexy-euro-16':
				$link .= 'sexy-euro-16';
				break;
			case 'members-list':
				$link .= 'members-list';
				break;
			case 'member':
				$link .= 'member';
				break;
			case 'photo-feed-ajax-list':
				$link .= 'photo-feed/ajax-list?ajax';
				break;
			case 'photo-feed-ajax-header':
				$link .= 'photo-feed/ajax-header?ajax';
				break;
			case 'photo-feed-ajax-voting-box':
				$link .= 'photo-feed/ajax-voting-box/';
				break;

			case 'photo-feed-ajax-voting-box':
				$link .= 'photo-feed/ajax-voting-box/';
				break;
			case 'photo-feed-ajax-more-photos':
				$link .= 'photo-feed/ajax-get-more-photos/';
				break;
			case 'photo-feed-ajax-vote':
				$link .= 'photo-feed/ajax-vote/';
				break;			
			
			case 'latest-actions-index':
				$link .= 'latest-actions';
				break;
			case 'latest-actions-ajax-header':
				$link .= 'latest-actions/ajax-header?ajax';
				break;
			case 'latest-actions-ajax-list':
				$link .= 'latest-actions/ajax-list?ajax';
				break;
			case 'latest-actions-ajax-get-details':
				$link .= 'latest-actions/ajax-get-details?ajax';
				break;
			case 'planner':
				$link .= 'planner';
			break;
			case 'glossary':
				$link .= 'glossary';
			break;
			case 'cityalerts':
				$link .= 'city-alerts';
			break;
            case 'feedback-ads':
                $link .= 'feedback-ads';
                break;
            case 'classified-ads-index':
                $link .= 'classified-ads';
                break;
            case 'classified-ads-place-ad':
                $link .= 'classified-ads/place-ad';
                break;
            case 'classified-ads-success':
                $link .= 'classified-ads/success';
                break;
			case 'classified-ads-error':
                $link .= 'classified-ads/error';
                break;
            case 'classified-ads-ajax-filter':
                $link .= 'classified-ads/ajax-filter?ajax';
                break;
            case 'classified-ads-ajax-list':
                $link .= 'classified-ads/ajax-list?ajax';
                break;
            case 'classified-ads-print':
                $link .= 'classified-ads/print';
                break;
			case 'classified-ads-ad':
				$link .= 'classified-ads/' . $args['title'] . '/ad/' . $args['id'];
				unset($args['id']);
				unset($args['title']);
				break;

			case 'online-billing':
				$link .= 'online-billing';
				break;
			case 'ob-mmg-postback':
				$link .= 'online-billing/mmg-postback';
				break;
			case 'get-filter-v2':
				$link .= 'escorts/get-filter-v2';
				break;	
			case 'forum':
				$link = 'http://www.escort-annonce.com/forum/?lang=' . $lang_id;
				break;
			case 'chat':
				$link = 'http://chat.escort-annonce.com/';
				break;
			case 'cams':
				$link = 'http://escort-annonce.streamray.com/';
				break;
			case 'search':
				$link .= 'search';
				break;
			case 'escorts-list':
				$link .= 'escorts';
			break;
			case 'base-city':
				$section = isset($args['section']) ? $args['section'] . '/' : '';
				if ( strlen($section) ) unset($args['section']);

				$link .= 'escorts/' . $section . 'city_' . $args['city'];
				unset($args['city']);
			break;
			case 'escorts':
				if ( empty(self::$_params) ) {
					$request = Zend_Controller_Front::getInstance()->getRequest();
					$req = trim($request->getParam('req', ''), '/');
					$req = explode('/', $req);
					
					$params = array();
					
					foreach ($req as $r)  {
						$param = explode('_', $r);
						if ( count($param) < 2 ) {
							$params[] = $r;
							continue;
						}
						
						$param_name = reset($param);
						array_shift($param);
						$params[$param_name] = implode('_', $param);
					}
					
					self::$_params = $params;
				}
				
				if ( $clean ) {
					$params = array();
				}
				else {
					$params = self::$_params;
				}
				
				foreach ( $args as $key => $value ) {
					if ( is_null($value) ) {
						unset($params[$key]);
						continue;
					}
					
					$params[$key] = $value;
				}
				
				$link .= 'escorts/';
				foreach ( $params as $param => $value ) {
					if ( ! strlen($param) || ! strlen($value)) continue;
					if ( is_int($param) ) {
						if ( $value == 'tours' ) $value = 'citytours';
						$link .= $value . '/';
					}
					elseif ( strlen($param) ) {
						if ( $param == 'page' ) {
							if ( $value - 1 > 0 ) {
								$link .= ($value - 1) . '/';
							}
						}
						else {
							$link .= $param . '_' . $value . '/';
						}
					}
				}
				
				$link = rtrim($link, '/');
				
				$args = array();
			break;
			case 'gotm':
				$link .= 'girl-of-month';

				if ( isset($args['history']) && $args['history'] ) {
					$link .= '/history';
				}
				
				if ( isset($args['page']) ) {
					if ( $args['page'] != 1 ) {
						$link .= '/page_' . $args['page'];
						//$link .= '/page_' . $args['page'];
					}
					
					unset($args['page']);
				}
				break;
			case 'profile':
				$link .= 'escort/' . $args['showname'] . '-' . $args['escort_id'];
				unset($args['showname']);
				unset($args['escort_id']);
			break;
			case 'forgot':
				$link .= 'private/forgot';
			break;
			case 'signup':
				$link .= 'private/signup-';
				
				if ( ! isset($args['type']) ) {
					$args['type'] = 'member';
				}
				
				$link .= $args['type'];
			break;
			case 'signin':
				$link .= 'private/signin';
			break;
			case 'signout':
				$link .= 'private/signout';
			break;
			case 'private':
				$link .= 'private';
			break;
			case 'membership-type':
				$link .= 'private/membership-type';
			break;
			case 'feedback':
				$link .= 'feedback';
			break;
			case 'contact':
				$link .= 'contact';
			break;
			case 'links':
				$link .= 'links';
			break;
			case 'add-review-comment':
				$link .= 'private-v2/add-review-comment';
			break;
			case 'captcha':
				$link .= 'captcha?' . rand();
				//$link = '/img/sample_captcha.gif?' . rand();
			break;
			case 'viewed-escorts':
				$link .= 'escorts/viewed-escorts';
			break;
			case '100p-verify':
				$link .= 'private/verify';
			break;
			case '100p-verify-webcam':
				$link .= 'private/verify/webcam';
			break;
			case '100p-verify-idcard':
				$link .= 'private/verify/idcard';
			break;
			case 'agency':
				$link .= 'agency/' . $args['slug'] . '-' . $args['id'];
				unset($args['slug']);
				unset($args['id']);
			break;
			
			case 'favorites':
				$link .= 'private/favorites';
			break;
			case 'add-to-favorites':
				$link .= 'private/add-to-favorites';
			break;
			case 'remove-from-favorites':
				$link .= 'private/remove-from-favorites';
			break;
			
			// Private Area
			case 'private-v2-gotd':
				$link .= 'private-v2/profile/gotd';
				break;
			case 'private-v2-gotd-success':
				$link = APP_HTTP.'://' . $_SERVER['SERVER_NAME'] . '/private-v2/profile/gotd-success';
				break;
			case 'private-v2-gotd-failure':
				$link = APP_HTTP.'://' . $_SERVER['SERVER_NAME'] . '/private-v2/profile/gotd-failure';
				break;
			case 'private-v2-gotd-cancel':
				$link = APP_HTTP.'://' . $_SERVER['SERVER_NAME'] . '/private-v2/profile/gotd';
				break;
            case 'simple-profile':
				$link .= 'private-v2/profile/simple';
				break;
			 case 'fill-contact-info':
				$link .= 'private-v2/ajax-fill-contact-info';
				break;
			case 'edit-profile':
				$link .= 'private/profile';
			break;
			case 'private-v2-statistics':
				$link .= 'private-v2/statistics';
				break;
			case 'edit-agency-profile':
				$link .= 'private/agency-profile';
			break;
			case 'profile-data':
				$link .= 'private/profile-data';
			break;
			case 'edit-photos':
				$link .= 'private/photos';
			break;
			case 'edit-rates':
				$link .= 'private/rates';
			break;
			case 'edit-escorts':
				$link .= 'private/escorts';
			break;
			case 'delete-escort':
				$link .= 'private/delete-escort';
			break;
			case 'edit-tours':
				$link .= 'private/tours';
			break;
			case 'change-passwd':
				$link .= 'private/change-password';
			break;
			
			case 'search':
				$link .= 'search';
			break;

			case 'late-night-girls':
				$link .= 'escorts/late-night-girls';
			break;
			
			case 'terms':
				$link .= 'page/terms-and-conditions';
			break;
			case 'user-terms':
				$link .= 'page/user-terms-and-conditions';
			break;
			case 'advertiser-terms':
				$link .= 'page/advertiser-terms-and-conditions';
			break;
			case 'privacy-policy':
				$link .= 'page/privacy-policy';
			break;
			case 'webcam-lm':
				$link .= 'page/webcam-verification-learn-more';
			break;
			case 'how-it-works-adv':
				$link .= 'page/about-beneluxxx-cams';
			break;
			case 'passport-lm':
				$link .= 'page/passport-verification-learn-more';
			break;
			
			case 'external-link':
				$link = '/go?' . $args['link'];
				unset($args['link']);
			break;
			// V2 Private Area
			case 'private-v2':
				$link .= 'private-v2';
				break;
			case 'private-v2-settings':
				$link .= 'private-v2/settings';
				break;
			case 'private-v2-happy-hour':
				$link .= 'private-v2/happy-hour';
				break;
			case 'private-v2-client-blacklist':
				$link .= 'private-v2/client-blacklist';
				break;
			case 'private-v2-add-client-to-blacklist':
				$link .= 'private-v2/add-client-to-blacklist';
				break;
			case 'private-v2-profile':
				$link .= 'private-v2/profile';
				break;
			case 'private-v2-photos':
				$link .= 'private-v2/photos';
				break;
			case 'private-v2-plain-photos':
				$link .= 'private-v2/plain-photos';
				break;
			case 'private-v2-tours':
				$link .= 'private-v2/tours';
				break;
			case 'private-v2-ajax-tours':
				$link .= 'private-v2/ajax-tours';
				break;
			case 'private-v2-ajax-tours-add':
				$link .= 'private-v2/ajax-tours-add';
				break;
			case 'private-v2-ajax-tours-remove':
				$link .= 'private-v2/ajax-tours-remove';
				break;
			case 'private-v2-verify':
				$link .= 'private-v2/verify';
				break;
			case 'private-v2-faq':
				$link .= 'private-v2/faq';
				break;
			case 'private-v2-premium':
				$link .= 'private-v2/premium';
				break;
			case 'private-v2-support':
				$link .= 'support';
				break;
			case 'private-v2-get-urgent-message':
				$link .= 'private-v2/get-urgent-message';
				break;
			case 'private-v2-set-urgent-message':
				$link .= 'private-v2/set-urgent-message';
				break;
			case 'private-v2-get-rejected-verification':
				$link .= 'private-v2/get-rejected-verification';
				break;
			case 'private-v2-set-rejected-verification':
				$link .= 'private-v2/set-rejected-verification';
				break;
			case 'alerts':
				$link .= 'private-v2/alerts';
				break;
			case 'my-video':
				$link .= 'Video/';
			break;
			case 'private-v2-agency-profile':
				$link .= 'private-v2/agency-profile';
				break;
			case 'private-v2-member-profile':
				$link .= 'private-v2/member-profile';
				break;
			case 'private-v2-upgrade-premium':
				$link .= 'private-v2/upgrade';
				break;
			case 'ticket-open':
				$link .= 'support/ticket';
				break;
			case 'private-v2-escorts':
				$link .= 'private-v2/escorts';
				break;
            case 'private-v2-escorts-delete':
				$link .= 'private-v2/profile-delete';
				break;
             case 'private-v2-escorts-restore':
				$link .= 'private-v2/profile-restore';
				break;
			case 'private-messaging':
				$link .= 'private-messaging';
				break;
			case 'pv-thread':
				$link .= 'private-messaging/thread?id=' . $args['id'];
				if ( $args['escort_id'] ) {
					$link .= '&escort_id=' . $args['escort_id'];
					unset($args['escort_id']);
				}
				unset($args['id']);
				break;
			case 'reviews':
				$link .= 'evaluations';
				break;
			case 'escort-reviews':
				$link .= 'evaluations/' . $args['showname'] . '-' . $args['escort_id'];
				unset ($args['showname']);
				unset ($args['escort_id']);
				break;
			case 'member-reviews':
				$link .= 'evaluations/member/' . $args['username'];
				unset ($args['username']);
				break;
			case 'escorts-reviews':
				$link .= 'evaluations/escorts';
				break;
			case 'agencies-reviews':
				$link .= 'evaluations/agencies';
				break;
			case 'top-reviewers':
				$link .= 'evaluations/top-reviewers';
				break;
			case 'top-ladies':
				$link .= 'evaluations/top-ladies';
				break;
			case 'voting-widget':
				$link .= 'escort/' . $args['showname'] . '-' . $args['escort_id'] . '/vote';
				unset($args['showname']);
				unset($args['escort_id']);
				break;
			case 'private-v2-reviews':
				$link .= 'private-v2/reviews';
				break;
			case 'private-v2-escort-reviews':
				$link .= 'private-v2/escort-reviews';
				break;
			case 'private-v2-escorts-reviews':
				$link .= 'private-v2/escorts-reviews';
				break;
			case 'private-v2-add-review':
				$link .= 'private-v2/add-review';
				break;
			case 'private-v2-add-reply';
				$link .= 'private-v2/add-reply';
				break;
			case 'private-v2-ajax-escort-comments';
				$link .= 'private-v2/ajax-escort-comments';
				break;
			case 'private-v2-phone-confirmation';
				$link .= 'private-v2/confirm-sms';
				break;
			case 'reviews-search':
				$link .= 'evaluations/search';
				break;
            case 'comments':
				$link .= 'comments';
				break;
			case 'members-choice':
				$link .= 'members-choice';
				break;
			case 'pornstars':
				$link .= 'escorts/pornstars';
				break;
			case 'member-info':
				$link .= 'member/'.$args['username'];
				unset($args['username']);
				break;
			case 'member-comments':
				$link .= 'members/get-member-comments';
				break;
			case 'tell-friend':
				$link .= 'escorts/ajax-tell-friend';
				break;
			case 'susp-photo':
				$link .= 'escorts/ajax-susp-photo';
				break;
			case 'report-problem':
				$link .= 'escorts/ajax-report-problem';
				break;
			case 'get-filter':
				$link .= 'escorts/get-filter';
				break;
			case 'get-gallery-photos':
				$link .= 'escorts/gallery-photos';
				break;
			case 'private-v2-photo-video':
				$link .= 'photo-video';
				break;
			case 'private-v2-get-galleries':
				$link .= 'photo-video/get-galleries';
				break;
			case 'private-v2-gallery':
				$link .= 'photo-video/ajax-gallery';
				break;
			case 'private-v2-get-gallery':
				$link .= 'photo-video/ajax-get-gallery';
				break;
			case 'private-v2-video':
				$link .= 'photo-video/ajax-video';
				break;
			case 'private-v2-video-action':
				$link .= 'photo-video/video';
				break;
			case 'private-v2-video-ready':
				$link .= 'photo-video/is-video-ready';
				break;
			case 'private-v2-natural-pic':
				$link .= 'photo-video/ajax-natural-pic';
				break;
			case 'private-v2-natural-pic-action':
				$link .= 'photo-video/natural-pic';
				break;
			case 'follow':
				$link .= 'follow';
            case 'chatroom':
                $link = APP_HTTP.'://www.beneluxxx.com/chatroom';
			break;
            case 'ben-cams':
                $link = 'https://beneluxxx.escortcams.com/';
				$link .= $lang_id;
                break;
			case 'ecardon-response':
				$link = APP_HTTP.'://www.beneluxxx.com/online-billing/ecardon-response';
			break;
		
			case 'bl-cams':
				$link =  IS_DEBUG ? 'https://beneluxxx.escortcams.dev/' : 'https://beneluxxx.escortcams.com/';
				
				$link .= $lang_id;
				if ($args['path']) {
					$link .= $args['path'];
				}
				unset($args['path']);
			break;
		
			case 'cams-member':
				$link .= 'bl-cams'; 
			break;
			
			case 'cams-escort-dash':
				$link .= 'bl-cams/broadcaster/dashboard';
			break;
		
			case 'cams-broadcaster':
				$encoded_showname = strtolower(str_replace(' ', '-', $args['showname']));
				$link = 'https://escortforumit.escortcams.com/channel/' . $encoded_showname.'-'.$args['escort_id'];
				unset($args['showname']);
				unset($args['escort_id']);
			break;
				
			case 'cams-booking-redirect':
				$link =  IS_DEBUG ? 'https://beneluxxx.escortcams.dev' : 'https://beneluxxx.escortcams.com';
				$link .= '/escort/';
				$link .= $args['escort_id'];
				$link .='/channel';
				unset($args['escort_id']);
			break;
		}

		if ( ! $no_args ) {
			if ( count($args) ) {
				$link .= '?';
				$params = array();
				foreach ( $args as $arg => $value ) {
					if ( ! is_array($value) ) {
						$params[] = $arg . '=' . urlencode($value);
					}
					else {
						foreach ( $value as $v ) {
							$params[] = $arg . '[]=' . $v;
						}
					}
				}

				$link .= implode('&', $params);
			}
		}
		
		return $link;
	}
}
