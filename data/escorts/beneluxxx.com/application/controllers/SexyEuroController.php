<?php 

class SexyEuroController extends Zend_Controller_Action
{
	protected $_session;
	
	public function init()
	{
		$this->view->layout()->setLayout('sexy-euro');
		$this->model = new Model_SexyEuro();
		$this->client = new Cubix_Api_XmlRpc_Client();
	}
	
	public function indexAction()
	{
		
		if (!$this->_request->ajax){
			$this->view->perPage = $perPage = 15;
			$this->view->page = $page = $this->_request->page || 1;
			$escorts = $this->model->getAll($page, $perPage, $count);
		} else {
			$this->view->layout()->disableLayout();
			$result = $this->model->getAllAjax();
			echo json_encode($result); die();
		}

		$signup_i18n = $this->view->signup_i18n = (object) array(
			'username_invalid' => 'Username must be at least 6 characters long',
			'username_invalid_characters' => 'Only "a-z", "0-9", "-" and "_" are allowed',
			'username_exists' => 'Username already exists, please choose another',
			'username_has_blocked_word' => Cubix_I18n::translate('username_has_blocked_word'),
			'password_invalid' => 'Password must contain at least 6 characters',
			'password2_invalid' => 'Passwords must be equal',
			'email_invalid' => 'Email is invalid, please provide valid email address',
			'email_exists' => 'Email already exists, please enter another',
			'terms_required' => 'You must agree with terms and conditions',
			'form_errors' => 'Please check errors in form',
			'terms_required' => 'You have to accept terms and conditions before continue',
			'domain_blacklisted' => 'Domain is blacklisted',
            'user_type' => 'Choose your business',
			'username_equal_password' => __('username_equal_password'),
		);

		$this->view->escorts = $escorts;
		$this->view->count = $count;
	}

	public function voteForEscortAction()
	{
		$user = Model_Users::getCurrent();
		
		if ( !$user || !$user->isMember()) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}
		
		$client = new Cubix_Api_XmlRpc_Client();
		$escort_id = intval($this->_request->escort_id);
		if(isset($escort_id)){
			$this->client->call('Members.addEuroLotteryVote', array($user->id, $escort_id));
		}
		
		die;
	}
}