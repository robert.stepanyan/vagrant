<?php

class OnlineBillingController extends Zend_Controller_Action
{
	public static $linkHelper;

	const SECRET_PREFIX = 'ipnI3Inr';
	const GATEWAY_URL = 'https://gateway.cardgateplus.com/';
	const CURRENCY = 'EUR';
	const SITEID = 3076;
	/**
	 * @var Zend_Session_Namespace
	 */
	protected $_session;
	
	const STATUS_PENDING  = 1;
	const STATUS_ACTIVE   = 2;
	const STATUS_EXPIRED  = 3;
	const STATUS_CANCELLED = 4;
	const STATUS_UPGRADED = 5;
	const STATUS_SUSPENDED = 6;

	public static $STATUS_LABELS = array(
		self::STATUS_PENDING  => 'pending',
		self::STATUS_ACTIVE   => 'active',
		self::STATUS_EXPIRED  => 'expired',
		self::STATUS_CANCELLED => 'cancelled',
		self::STATUS_UPGRADED => 'upgraded',
		self::STATUS_SUSPENDED => 'suspended'
	);

	public function init()
	{	
		$this->_request->setParam('no_tidy', true);
		$this->view->layout()->setLayout('private-v2');
		
		$anonym = array('epg-response', 'mmg-postback', 'twispay-response');
		
		$this->user = Model_Users::getCurrent();

		if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}
		
		if ( ! in_array($this->_request->getActionName(), $anonym) && ! $this->user->isAgency() && ! $this->user->isEscort() ) {
			$this->_redirect($this->view->getLink('private-v2'));
		}
		
		$this->view->headTitle('Private Area', 'PREPEND');

		$this->_session = new Zend_Session_Namespace('online_billing');
		
		$this->client = new Cubix_Api_XmlRpc_Client();
	}
	
	public function indexAction()
	{
		
	}
	
	public function mmgPostbackAction()
	{
		$this->view->txn_status = $this->_request->txn_status;
		$this->view->ti_mmg = $this->_request->ti_mmg;
	}
	
	public function epgGotdResponseAction()
	{
		$this->view->layout()->disableLayout();
		
		$req = $this->_request;
		
		$token = $req->Token;
		if ( ! $token ) $this->_redirect('/');
		
		$epg_payment = new Model_EpgGateway();
		$result = $epg_payment->getTransactionStatus($token);
		
		if ( $result['ResultStatus'] == "OK" && $result['TransactionResult'] == "OK" ) {
			$this->_redirect($this->view->getLink('private-v2-gotd-success'));
		} else {
			$this->_redirect($this->view->getLink('private-v2-gotd-failure'));
		}
	}
	
	public function ecardonPopupAction()
	{
		$this->view->layout()->disableLayout();
	}
	
	public function ecardonResponseAction()
	{
		$this->view->layout()->disableLayout();
		
		$req = $this->_request;
				
		$token =  preg_replace('#[^a-zA-Z0-9.\-]#', '', $req->id);
		
		if ( ! $token ) $this->_redirect('/');
		
		$alt_payment = new Model_EcardonGateway();
		$result = $alt_payment->getStatus($token);
		$result_dec = json_decode($result);
		$this->client->call('OnlineBillingV2.storeStatus', array($token, $this->user->id, $result));
		
		if(isset($result_dec->result) && ( $result_dec->result->code == '000.000.000' ||  $result_dec->result->code == '000.100.110')){
			$this->_redirect($this->view->getLink('private-v2-gotd-success'));
		} 
		else {
			$this->_redirect($this->view->getLink('private-v2-gotd-failure'));
		}
	}

    public function twispayResponseAction()
    {
        $this->view->layout()->disableLayout();
        $twispayConfigs = Zend_Registry::get('payment_config')['twispay'];

        $decryptedData = Cubix_Twispay_TwispayApi::decryptIpnResponse($this->_request->opensslResult, $twispayConfigs['key']);

        if ($decryptedData['transactionStatus'] == 'in-progress' || $decryptedData['transactionStatus'] == 'complete-ok') {

            $userId = str_replace('user-', '', $decryptedData['identifier']);
            $token = json_encode(['orderId' => $decryptedData['orderId'], 'transactionId' => $decryptedData['transactionId']]);
            $this->client->call('OnlineBillingV2.storeToken', array($token, $userId, 'twispay'));

            $this->_redirect($this->view->getLink('private-v2-gotd-success'));
        } else {
            $this->_redirect($this->view->getLink('private-v2-gotd-failure'));
        }
    }
}
