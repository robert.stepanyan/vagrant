<?php

class FollowController extends Zend_Controller_Action
{
	private $model;

	public function init()
	{

		$this->user = Model_Users::getCurrent();

		$anonym = array();
		if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}

		$user = Model_Users::getCurrent();
		$this->view->user = $user;
		
		$this->model = new Model_Follow();
		$this->view->layout()->setLayout('private-v2');
	}

	public function indexAction()
	{
		
	}

	public function ajaxMemberSearchAction(){

		$name = trim($this->_getParam('member_name'));

		if ( ! strlen($name) ) {
			die(json_encode(array()));
		}

		$members = $this->model->searchMember($name);


		echo json_encode($members);
		die;
	}

	public function followAction(){
		$followed_id = (int)$this->_getParam('followed_id');
		$events = $this->_getParam('events');

		$edit_mode = $this->_getParam('edit_mode');
		if($edit_mode == 'false')
			$edit_mode = false;	


		$followed_data = $this->model->follow($followed_id,$events,$edit_mode);

		echo json_encode($followed_data);
		die;
	}

	public function ajaxFollowListAction(){
		$type = $this->_getParam('type');

		$types = array('followed','follower');
		$search = $this->_getParam('search');

		if(empty($search)){
			$search = false;	
		}

		if(!in_array($type, $types)){
			$type = 'followed';
		}

		$page = intval($this->_getParam('page'));
		if ( $page == 0 || !$page ) {
			$page = 1;
		}
		$per_page = 10;

		$this->view->layout()->disableLayout();

		$this->_helper->viewRenderer->setScriptAction('follow-list');
		$list = $this->model->getFollowers($type,$page,$per_page,$search);

		$this->view->list = $list['data'];
		$this->view->count = $list['count'];
		$this->view->page = $page;
		$this->view->type = $type;
		$this->view->per_page = $per_page;
	}

	public function unFollowAction(){
		$follow_id = (int)$this->_getParam('follow_id');

		$response = $this->model->unFollow($follow_id);
		echo json_encode($response);
		die;
	}

	public function removeMyFollowerAction(){
		$follow_id = (int)$this->_getParam('follow_id');

		$response = $this->model->removeMyFollower($follow_id);
		echo json_encode($response);
		die;
	}
	
	public function approveAction(){
		$follow_id = (int)$this->_getParam('follow_id');

		$response = $this->model->approve($follow_id);
		echo json_encode($response);
		die;
	}

	public function denyAction(){
		$follow_id = (int)$this->_getParam('follow_id');

		$response = $this->model->deny($follow_id);
		echo json_encode($response);
		die;
	}

	public function followPopupAction(){
		$followed_id = (int)$this->_getParam('followed_id');

		if(!$followed_id){
			return false;
		}

		$edit_mode = $this->_getParam('edit_mode');
		if($edit_mode == 'false')
			$edit_mode = false;

		if($edit_mode){
			$this->view->current_events = $this->model->getEvents($followed_id);	
		}

		$memberModel = new Model_Members();
		$username = $memberModel->getUsernameById($followed_id);

		$this->view->username = $username;
		$this->view->followed_id = $followed_id;
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setScriptAction('follow-popup');
	}

	public function checkFollowedAction(){
		$follow_id = (int)$this->_getParam('followed_id');

		$alreadyFollowed = $this->model->checkAlreadyFollow($follow_id);

		$return = array();
		if($alreadyFollowed){
			$return['status'] = 1;
			echo json_encode($return);
			die;
		}else{
			$return['status'] = 0;
			echo json_encode($return);
			die;
		}
	}

	public function alreadyFollowedPopupAction(){
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setScriptAction('already-followed-popup');	
	}

}