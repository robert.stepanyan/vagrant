<?php 

class MembersListController extends Zend_Controller_Action
{
	protected $_session;
	
	public function init()
	{
		$this->view->layout()->setLayout('members-list');
		$this->model = new Model_MembersList();
		$this->client = new Cubix_Api_XmlRpc_Client();
		$this->currentUser = new Model_Users();
		
		if( is_null($this->currentUser->getCurrent()) ){
			$this->view->current_user = 'false';
		}else{
			$this->view->current_user = 'true';
		}
	}
	
	public function indexAction()
	{
		$lng = Cubix_I18n::getLang();
		
		$request = $this->_request;
		
		if ( $request->ajax ) {
			$this->view->layout()->disableLayout();
			$this->view->ajax = true;
		}
		$arg_filter = array();
		
		if (isset($request->username) && $request->username ){
			$arg_filter['username'] = $request->username;
			$this->view->username = $request->username;
		}

		switch ($request->ord_field)
		{
			case 'creation_date':
				$ord_field_v = 'creation_date';
				$ord_field = 'creation_date';
				break;
			case 'city_title':
				$ord_field_v = 'city_title';
				$ord_field = 'city_title';
			case 'country_title':
				$ord_field_v = 'country_title';
				$ord_field = 'country_title';
			default:
				$ord_field_v = 'username';
				$ord_field = 'username';
				break;
		}
		
		switch ($request->ord_dir)
		{
			case 'desc':
				$ord_dir_v = 'desc';
				$ord_dir = 'DESC';
				break;
			case 'asc':
			default:
				$ord_dir_v = 'asc';
				$ord_dir = 'ASC';
				break;
		}		
		$config = Zend_Registry::get('membersList_config');

		if (isset($request->page) && intval($request->page) > 0)
		{
			$page = intval($request->page);
		}
		else
			$page = 1;

		is_array($arg_filter) ? $filter_str = implode('.', $arg_filter) : $filter_str = $arg_filter;
		$cache = Zend_Registry::get('cache');
		$cache_key = 'members_list_' . $lng . '_page_' . $page . '_perpage_' . $config['perPage'] . '_filter_' . $filter_str . '_ordfield_' . $ord_field . '_orddir_' . $ord_dir;
		
		if (!$members = $cache->load($cache_key) ){
			$members = Model_MembersList::getMembers($page, 25, $arg_filter, $ord_field, $ord_dir);
			$cache->save($members, $cache_key, array(), 1800); //30 min
		}
		
		$this->view->items = $members['members'];
		$this->view->count = $members['rowsCount'];
		$this->view->page = $page;
		$this->view->ord_field = $ord_field_v;
		$this->view->ord_dir = $ord_dir_v;
	}
	
	public function ajaxMembersListSearchAction()
	{
		$this->_request->setParam('no_tidy', true);

		$name = trim($this->_getParam('f_username'));

		$cache = Zend_Registry::get('cache');
		$cache_key = 'ajax_members-list_search_' . $name;
		$cache_key = preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

		if ( ! $members = $cache->load($cache_key) ) {
			$arg_filter['username'] = $name;
			$members = Model_MembersList::getMembers( 1, 35, $arg_filter, 'username' , 'asc' );
			$cache->save($members, $cache_key, array());
		}
		
		echo json_encode($members['members']);
		die;
	}
	
}