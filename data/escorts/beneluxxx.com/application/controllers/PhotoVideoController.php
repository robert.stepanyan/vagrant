<?php

class PhotoVideoController extends Zend_Controller_Action
{
	public function init()
	{
		$this->_request->setParam('no_tidy', true);

		
	
		$this->user = Model_Users::getCurrent();
		if ( ! $this->user) { 
			$PA_redirection = new Zend_Session_Namespace('pa_redirection');
			$PA_redirection->pa_redirect_url = $_SERVER["REQUEST_URI"];
			
			$this->_redirect($this->view->getLink('signin'));
			return;
		}

		$this->view->layout()->setLayout('private-v2');
		$this->client = Cubix_Api::getInstance();
		$this->app_id = Cubix_Application::getId();
		$cache = Zend_Registry::get('cache');
		$this->view->user = $this->user;
		
		$cache_key =  'v2_user_pva_' . $this->user->id;

		if ( $this->user->isAgency() ) {

			if ( ! $agency = $cache->load($cache_key) ) {
				$agency = $this->user->getAgency();
				$cache->save($agency, $cache_key, array(), 300);
			}
			
			$this->agency = $this->view->agency = $agency;
			$escort_id = intval($this->_getParam('escort'));
			
			if ( 1 > $escort_id ) $this->_redirect($this->view->getLink('private-v2-escorts'));
			$client = new Cubix_Api_XmlRpc_Client();		
			if ( ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
				$this->_redirect($this->view->getLink('private-v2-escorts'));
			}
			$this->view->escort_id = $this->escort_id = $escort_id;
			$modelE = new Model_EscortsV2();
			$esc = $modelE->getById($escort_id);
			$this->view->is_suspicious = $this->is_suspicious = $esc->is_suspicious;
		}
		else if ( $this->user->isEscort() ) {

			if ( ! $escort = $cache->load($cache_key) ) {
				$escort = $this->user->getEscort();
				$cache->save($escort, $cache_key, array(), 300);
			}
			$this->escort = $this->view->escort = $escort;
			$this->view->escort_id = $this->escort_id = $this->escort->id;
			$this->view->is_suspicious = $this->is_suspicious = $escort->is_suspicious;
		}


		/*If doesn't have valid about_me text redirect to profile about me tab*/
		$e_model = new Model_Escorts();
		$_escort = $e_model->getById($this->escort_id);
		$profile = $_escort->getProfile();
		$profile->load();
		// $about_me_data = $profile->getAboutMe();
		

		// $validator = new Cubix_Validator();
		// $about_me_data = $profile->getAboutMe();
		// $about_me_data['about_it'] = $validator->urlCleaner($about_me_data['about_it'], Cubix_Application::getById()->host);
		// $clean_text_length = strlen(trim(strip_tags($validator->specialCharsCleaner($about_me_data['about_it']))));
		// if( $clean_text_length > 0 && $clean_text_length < 200 ){
		// 	if ( $this->user->isAgency() ) {
		// 		$this->_redirect($this->view->getLink('private-v2-profile',  array('escort' => $this->escort_id)));
		// 	}
		// 	$this->_redirect($this->view->getLink('private-v2-profile'));
		// }
		/*If doesn't have valid about_me text redirect to profile about me tab*/
		
	}

	public function indexAction()
	{
		
	}
	
	public function getGalleriesAction()
	{
		$cur_gallery_id = $this->_getParam('gallery_id');
		$cur_gallery_id = is_null($cur_gallery_id) ? null : intval($cur_gallery_id);
		
		$galleries = $this->client->call('getPhotoGalleries', array($this->escort_id, $cur_gallery_id));
		if($cur_gallery_id !== 0){
			array_unshift($galleries, array('id' => 0, 'title' => "Main Gallery" )); 
		}
		die(json_encode($galleries));
	}
	
	public function ajaxGalleryAction()
	{
		$this->view->layout()->disableLayout();
		$this->view->galleries = $this->client->call('getPhotoGalleries', array($this->escort_id));
	}
	
	public function ajaxGetGalleryAction()
	{
		$this->view->layout()->disableLayout();
		$client = new Cubix_Api_XmlRpc_Client();
		$action = $this->_getParam('act');
		
		switch($action){
			case 'upload':
				try {
					
					$gallery_id = intval($this->_getParam('gallery_id'));
					//$is_private = intval($this->_getParam('is_private'));
					$model = new Model_Escort_Photos();
					$ext = $model->uploadValidation();
					$response = $model->upload();
					
					if($response['error'] == 0 && $response['finish']){
						// Save on remote storage
						$images = new Cubix_Images();
						$image = $images->save($response['tmp_name'], $this->escort_id, $this->app_id, $ext);
						
						$image = new Cubix_Images_Entry($image);
						$image_size = getimagesize($response['tmp_name']);
						
						$is_portrait = 0;
						if ( $image_size ) {
							if ( $image_size[0] < $image_size[1] ) {
								$is_portrait = 1;
							}
						}

						$photo_arr = array(
							'escort_id' => $this->escort_id,
							'gallery_id' => $gallery_id,
							'hash' => $image->getHash(),
							'ext' => $image->getExt(),
							//'type' => $is_private ? ESCORT_PHOTO_TYPE_PRIVATE : ESCORT_PHOTO_TYPE_HARD,
							'type' => ESCORT_PHOTO_TYPE_HARD,
							'is_portrait' => $is_portrait,
							'width' => $image_size[0],
							'height' => $image_size[1],
							'creation_date' => date('Y-m-d H:i:s', time())
						);

						if ( $client->call('Escorts.hasStatusBit', array($this->escort_id, array(Model_Escorts::ESCORT_STATUS_NO_PROFILE))) || $client->call('Escorts.isAutoApproval', array($this->escort_id)) ) {
							$photo_arr['is_approved'] = 1;
						}

						$photo = new Model_Escort_PhotoItem($photo_arr);

						$model = new Model_Escort_Photos();
						$photo = $model->save($photo);
						$this->view->newPhoto = $photo;

						$response['photo_id'] = $photo->id;
						$response['photo_url'] = $photo->getUrl('thumb_cropper2');
						$response['is_main'] = $photo->is_main;
						$response['photo_type'] = $photo->type;
						$response['is_approved'] = $photo->is_approved;
						$response['args'] = unserialize($photo->args);
					}
					$model->showResponse($response);
					die;

				} catch ( Exception $e ) {
					echo json_encode(array(
						'finish' => 0,
						'error' => $e->getMessage()
					));
					die;
				}
				break;
				
			case 'set-main':
				$ids = $this->_getParam('photo_id');
				if ( ! is_array($ids) || ! count($ids) ) {
					die(json_encode(array('success' => 0 ,'error' => 'Please select at least one photo')));
				}
				$rotate_type = Model_Escort_Photos::PHOTO_ROTATE_SELECTED;
				if(count($ids) == 1){
					$rotate_type = Model_Escort_Photos::PHOTO_ROTATE_SELECTED_ONE;
				}
				$photo_id = $client->call('Escorts.getApprovedPhoto', array($this->escort_id, implode(',',$ids)));
				
				if(!$photo_id){
					die(json_encode(array('success' => 0, 'error' => Cubix_I18n::translate('sys_error_not_approved_photo'))));
				}
				if ( !$this->_check($ids) ) {
					die(json_encode(array('success' => 0,'error' => Cubix_I18n::translate('sys_error_invalid_id_photo'))));
				}
				
				$photo = new Model_Escort_PhotoItem(array('id' => $photo_id ,'escort_id' => $this->escort_id));
				$photo->setRotatePics($ids);
				$photo->setMain();
				$this->client->call('Escorts.setPhotoRotateType', array($this->escort_id, $rotate_type));
				$this->view->gallery_id = $gallery_id = intval($this->_getParam('gallery'));
				$this->view->photos = $this->client->call('getEscortPhotosList', array($this->escort_id, false, $gallery_id ));
				$html = $this->view->render('photo-video/ajax-get-gallery.phtml');
				die(json_encode(array('success' => 1, 'html' => $html)));
				break;
				
			case 'make-private':
			case 'make-public':
			case 'make-archived':
				if ( $action == 'make-private'){
					$type = ESCORT_PHOTO_TYPE_PRIVATE;
				} elseif ( $action == 'make-public') {
					$type = ESCORT_PHOTO_TYPE_HARD;
				} elseif ( $action == 'make-archived'){
					$approved_photos = $this->_getApprovedPhotos();
					$type = ESCORT_PHOTO_TYPE_ARCHIVED;
				} else {
					die;
				}

				$ids = $this->_getParam('photo_id');

				if ( ! is_array($ids) || ! count($ids) ) {
					die(json_encode(array('success' => 0, 'error' => 'Please select at least one photo')));
				}
				
				if ( !$this->_check($ids) ) {
					die(json_encode(array('success' => 0,'error' => Cubix_I18n::translate('sys_error_invalid_id_photo'))));
				}
							
				foreach ( $ids as $id ) {
					if( $action == 'make-archived'){
						if ( ! in_array($id, $approved_photos) ) {
							die(json_encode(array('success' => 0,'error' => Cubix_I18n::translate('sys_error_only_soft_photos'))));
						}
					}
					$photo = new Model_Escort_PhotoItem(array('id' => $id));
					$photo->make($type, $this->is_suspicious);
				}
				die(json_encode(array('success' => 1)));
				break;
				
			case 'change-gallery':
				$ids = $this->_getParam('photo_id');
				$gallery_id = intval($this->_getParam('gallery_id'));
				$type_id = is_null($this->_getParam('type')) ? null : intval($this->_getParam('type'));
				
				if ( ! is_array($ids) || ! count($ids) ) {
					die(json_encode(array('success' => 0, 'error' => 'Please select at least one photo')));
				}
				
				if ( !$this->_check($ids) ) {
					die(json_encode(array('success' => 0,'error' => Cubix_I18n::translate('sys_error_invalid_id_photo'))));
				}
				
				foreach ( $ids as $id ) {
					$this->client->call('changeGallery', array($this->escort_id, $id, $gallery_id, $type_id  ));
				}
				die(json_encode(array('success' => 1)));
				break;
				
			case 'set-gallery-status':
				$status = intval($this->_getParam('status'));
				$gallery_id = intval($this->_getParam('gallery'));
				
				$return = $this->client->call('setGalleryStatus', array($this->escort_id, $gallery_id, $status));
				
				if($return){
					die(json_encode(array('success' => 1)));
				}
				else{
					die(json_encode(array('success' => 0)));
				}
				break;
			
			case 'sort':
				$ids = $this->_getParam('photo_id');
				
				if ( ! is_array($ids) || ! count($ids) ) {
					die('Please select at least one photo');
				}
				elseif ( !$this->_check($ids) ) {
					die('Invalid id of one of the photos');
				}
				$model = new Model_Escort_Photos();
				$model->reorder($ids);
				die;
				
			case 'remove':
				$ids = $this->_getParam('photo_id');

				if ( ! is_array($ids) || ! count($ids) ) {
					die(json_encode(array('success' => 0, 'error' => 'Please select at least one photo')));
				}
				
				if ( !$this->_check($ids) ) {
					die(json_encode(array('success' => 0,'error' => Cubix_I18n::translate('sys_error_invalid_id_photo'))));
				}
				
				if($this->is_suspicious){
					die(json_encode(array('success' => 0,'error' => Cubix_I18n::translate('sys_error_is_suspicious'))));
				}
				$photo = new Model_Escort_Photos();
				$photo->remove($ids);
				die(json_encode(array('success' => 1)));
				break;
				
			case 'rename':
					$gallery_name = $this->_getParam('gallery_name');
					$gallery_id = $this->_getParam('gallery_id');
					$return = $this->client->call('changeGalleryName', array($this->escort_id,$gallery_id, $gallery_name ));
					die($return);
				break;
			
			case 'get-archive':
				$this->view->is_archive = 1;
				$this->view->photos = $this->client->call('getEscortArchivedPhotosList', array($this->escort_id));
				$this->_helper->viewRenderer->setScriptAction('ajax-get-archive');
				break;
			
			case 'set-adj':
				$photo_id = intval($this->_getParam('photo_id'));
				
				if ( !$this->_check( array($photo_id)) ) {
					die(json_encode(array('error' => 'An error occured')));
				}
				
				$photo = new Model_Escort_PhotoItem(array(
					'id' => $photo_id
				));
				
				try {
					$hash = $photo->getHash();
					
					$result = array(
						'x' => intval($this->_getParam('x')),
						'y' => intval($this->_getParam('y')),
						'px' => floatval($this->_getParam('px')),
						'py' => floatval($this->_getParam('py'))
					);
					
					$photo->setCropArgs($result);

					// Crop All images
					$size_map = array(
						'backend_thumb' => array('width' => 150, 'height' => 205),
						'medium' => array('width' => 225, 'height' => 300),
						'thumb' => array('width' => 150, 'height' => 200),
						'nlthumb' => array('width' => 120, 'height' => 160),
						'sthumb' => array('width' => 76, 'height' => 103),
						'lvthumb' => array('width' => 75, 'height' => 100),
						'agency_p100' => array('width' => 90, 'height' => 120),
                        'xl_thumb' => array('width' => 302, 'height' => 455),
						't100p' => array('width' => 117, 'height' => 97)
					);
					$conf = Zend_Registry::get('images_config');
									
					get_headers($conf['remote']['url'] . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $this->escort_id . "&hash=" . $hash);
										
					$catalog = $this->escort_id;
					$a = array();
					if ( is_numeric($catalog) ) {
						$parts = array();

						if ( strlen($catalog) > 2 ) {
							$parts[] = substr($catalog, 0, 2);
							$parts[] = substr($catalog, 2);
						}
						else {
							$parts[] = '_';
							$parts[] = $catalog;
						}
					}
					else if ( preg_match('#^([0-9]+)/(.+)$#', $catalog, $a) ) {
						array_shift($a);
						$catalog = $a[0];

						$parts = array();

						if ( strlen($catalog) > 2 ) {
							$parts[] = substr($catalog, 0, 2);
							$parts[] = substr($catalog, 2);
						}
						else {
							$parts[] = '_';
							$parts[] = $catalog;
						}

						$parts[] = $a[1];
					}

					$catalog = implode('/', $parts);

					foreach($size_map as $size => $sm) {
						get_headers($conf['remote']['url'] . "/" . Cubix_Application::getById()->host . "/" . $catalog . "/" . $hash . "_" . $size . ".jpg?args=" . $result['x'] . ":" . $result['y']);
					}
				}
				catch ( Exception $e ) {
					die(json_encode(array('error' => 'An error occured')));
				}

				die(json_encode(array('success' => true)));
			case 'set-rotate':	
			
				$photo_id = intval($this->_getParam('photo_id'));
				$degree = intval($this->_getParam('degree'));
				$degree = $degree == 90 ? 90 : -90;
				
				if ( !$this->_check( array($photo_id)) ) {
					die(json_encode(array('error' => 'An error occured')));
				}
				
				$photo = new Model_Escort_PhotoItem(array(
					'id' => $photo_id
				));

				try {
					$photo_info = $photo->getHashExt();
					$hash = $photo_info['hash'];
					$ext = $photo_info['ext'];
					
					$conf = Zend_Registry::get('images_config');
					$remoteUrl = str_replace('https://','', $conf['remote']['url']);
					$rotateUrl = $remoteUrl. "/get_image.php?a=rotate&app=" . Cubix_Application::getById()->host . "&eid=" . $this->escort_id . "&hash=" . $hash . "&ext=". $ext . "&d=". $degree;
					$ch = curl_init($rotateUrl);
					$rH = curl_exec($ch);
                    curl_close($ch);
					$cacheUrl = $remoteUrl . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $this->escort_id . "&hash=" . $hash;
					$cH = get_headers($cacheUrl);
					$result = array(
						'x' => 0,
						'y' => 0,
						'px' => 0,
						'py' => 0
					);
					
					$photo->setCropArgs($result);
					
					$catalog = $this->escort_id;
					$a = array();
					if ( is_numeric($catalog) ) {
						$parts = array();

						if ( strlen($catalog) > 2 ) {
							$parts[] = substr($catalog, 0, 2);
							$parts[] = substr($catalog, 2);
						}
						else {
							$parts[] = '_';
							$parts[] = $catalog;
						}
					}
					else if ( preg_match('#^([0-9]+)/(.+)$#', $catalog, $a) ) {
						array_shift($a);
						$catalog = $a[0];

						$parts = array();

						if ( strlen($catalog) > 2 ) {
							$parts[] = substr($catalog, 0, 2);
							$parts[] = substr($catalog, 2);
						}
						else {
							$parts[] = '_';
							$parts[] = $catalog;
						}

						$parts[] = $a[1];
					}

					$catalog = implode('/', $parts);
					get_headers($rotateUrl . "/" . Cubix_Application::getById()->host . "/" . $catalog . "/" . $hash . "_thumb_cropper.".$ext);
					$log = array(
					    'rotateUrl'=> $rotateUrl,
                        'rH' => $rH,
                        'cache' => $cacheUrl,
                        'cH' => $cH,
                    );
					file_put_contents('rotate.log',var_export($log, true)."\r\n", FILE_APPEND);
				}
				catch ( Exception $e ) {
					die(json_encode(array('error' => 'An error occured')));
				}

				die(json_encode(array('success' => true)));
					
			default:
				$this->view->gallery_id = $gallery_id = intval($this->_getParam('gallery'));
				if($gallery_id != 0){
					$this->view->gallery_status = $this->client->call('getGalleryStatus', array($gallery_id ));
				}
				//$is_private = intval($this->_getParam('private')) == 1 ? true : false;
				$this->view->photos = $this->client->call('getEscortPhotosList', array($this->escort_id, false, $gallery_id ));
				
		}
		
	}
	
	private function _check($check_ids)
	{
		
		$photos = $this->client->call('getEscortPhotoIds', array($this->escort_id, false ));
		$photo_ids = array();
		foreach ( $photos as $photo ) {
			$photo_ids[] = intval($photo['id']);
		}
		foreach($check_ids as $check_id){
			if(!in_array($check_id, $photo_ids)){
				return false;
			}
		}
		
		return true; 
	}
	
	private function _getApprovedPhotos()
	{
		$app_photo_ids = array();
		$approved_photos = $this->client->call('getApprovedPhotos', array($this->escort_id, false ));
		foreach ( $approved_photos as $photo ) {
			$app_photo_ids[] = $photo['id'];
		}
		
		return $app_photo_ids;
	}
	
	public function ajaxVideoAction()
	{
		$this->view->layout()->disableLayout();
		$this->view->user = $this->user;
		$video_model = new Model_Video();
			
		$video = $video_model->GetEscortVideoV2($this->escort_id);
		if($video){
			$video['photo'] = new Cubix_ImagesCommonItem($video['photo']);
		}
		$this->view->host =  Cubix_Application::getById($this->app_id)->host;
		$this->view->config =  Zend_Registry::get('videos_config');
		$this->view->video = $video;
		
	}		
	
	public function isVideoReadyAction() {
		$client = new Cubix_Api_XmlRpc_Client();
		$pending_video = $client->call('Application.getPendingVideo', array($this->escort_id));
		
		if($pending_video){
			$config =  Zend_Registry::get('videos_config');
			$app_id = Cubix_Application::getId();
			$host =  Cubix_Application::getById($app_id)->host;
			$video_image = array(
				'application_id' => $app_id,
				'hash' => $pending_video['image_hash'],
				'ext' => $pending_video['image_ext']
			);
			$photo_model = new Cubix_ImagesCommonItem($video_image);
			$image_url = $photo_model->getUrl('m320');
			
			$response['vod'] = $config['remote_url'].$host.'/'.$this->escort_id.'/';
			$response['photo_url'] = $image_url;
			$response['video_width'] = $pending_video['width'];
			$response['video_height'] = $pending_video['height'];
			$response['video'] = $pending_video['video'];
			$response['video_id'] = $pending_video['id'];
			echo json_encode($response);die;
		}
		else{
			echo json_encode(array('status' => 0));die;
		}
		
	}
	
	public function videoAction()
	{
		$this->view->user = $this->user;
		$video_model = new Model_Video();
		$client = new Cubix_Api_XmlRpc_Client();
		$config =  Zend_Registry::get('videos_config');
				
		$action = $this->_getParam('act');
		
		switch($action){
			case 'upload':
				//$host =  Cubix_Application::getById(Cubix_Application::getId())->host;
				$response = array(
					'error' => '',
					'finish' => FALSE
				);

				try {

					if($video_model->UserVideoCount($this->escort_id) < $config['VideoCount'])
					{			
						$backend_video_count = $client->call('Application.UserVideoCount',array($this->escort_id));
						$check = Cubix_Videos::CheckVideoStatus($backend_video_count, $config['VideoCount'], true);

						if(!$check)
						{	
							$video = new Cubix_Videos($this->escort_id,$config);
							$response = $video->getResponse();

							if($response['finish'] && $response['error'] === 0 ){
								$response['wait_for_video'] = 1;
								ignore_user_abort(true);
								ob_start();
								Cubix_Videos::showResponse($response);
								header('Connection', 'close');
								header('Content-Length: '.ob_get_length());	
								ob_end_flush();
								ob_flush();
								flush();
								fastcgi_finish_request();
																												
								$video->ConvertToMP4();
								
								$image = $video->SaveImage();		
								if(is_array($image))
								{	
									if(!$video->SaveVideoImageFtpFrontend($image,$client)){
										throw new Exception('system error while uploading Video');
										//$response['error'] = Cubix_I18n::translate('sys_error_while_upload');
									}
								}
								else{
									throw new Exception($image);
									//$response['error'] = $image;
								}
							}
						}
						else{
							$response['error'] = $check;
						}
					}
					else{
						$response['error'] = Cubix_I18n::translate('sys_error_upload_video_count',array('count' => $config['VideoCount']));
					}
				} catch ( Exception $e ) {
					 $response['error'] = $e->getMessage();
				}
				Cubix_Videos::showResponse($response);
				die;
				
			case 'delete':
				$video_id = intval($this->_getParam('video_id'));
				var_dump($video_id,$this->escort_id);
				$video_model->RemoveV2($video_id,$this->escort_id);
				die;
		}
	}
	
	public function ajaxNaturalPicAction()
	{
		$this->view->layout()->disableLayout();
		$nat_model = new Model_NaturalPic();
		$this->view->nat_pic = $nat_pic = $nat_model->get($this->escort_id);
		
		if($nat_pic){
			$photo_arr = array(
				'hash' => $nat_pic['hash'], 
				'ext' => $nat_pic['ext']
			);
			$images = new Cubix_Images();
			$image = new Cubix_Images_Entry($photo_arr);
			$image->setSize('nat_thumb');
			$image->setCatalogId($this->escort_id. '/natural-pics');
			$this->view->image_url = $images->getUrl($image);
		}
	}
	
	public function naturalPicAction()
	{
		$nat_model = new Model_NaturalPic();
		$config =  Zend_Registry::get('images_config');		
		$action = $this->_getParam('act');
		
		switch($action){
			case 'upload':
				try {
					
					$model = new Model_Escort_Photos();
					$ext = $model->uploadValidation();
					$response = $model->upload();
					
					if($response['error'] == 0 && $response['finish']){
						// Save on remote storage
						$images = new Cubix_Images();
						$image = $images->save($response['tmp_name'], $this->escort_id. '/natural-pics', $this->app_id, $ext);
						$image = new Cubix_Images_Entry($image);
						$image->setSize('nat_thumb');
						$image->setCatalogId($this->escort_id. '/natural-pics');
						$image_url = $images->getUrl($image);

						$photo_arr = array(
							'escort_id' => $this->escort_id,
							'hash' => $image->getHash(),
							'ext' => $image->getExt(),
							'creation_date' => date('Y-m-d H:i:s', time())
						);
						$new_id = $nat_model->save($photo_arr);

						$response['pic_id'] = $new_id;
						$response['photo_url'] = $image_url;
					}
					$model->showResponse($response);
					die;

				} catch ( Exception $e ) {
					echo json_encode(array(
						'finish' => 0,
						'error' => $e->getMessage()
					));
					die;
				}
				break;
				
			case 'delete':
				$nat_model->remove($this->escort_id);
				die;
		}
	}
}
