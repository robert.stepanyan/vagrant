<?php

/**
 * @autor Eduard Hovhannisyan
 * @created 03/06/2020
 *
 * Class PaymentController
 */
class PaymentController extends Zend_Controller_Action
{
    /**
     * @var Array
     */
    private $paymentMethods;

    /**
     * @return void
     */
    public function init()
    {
        $this->view->layout()->setLayout('main-simple');
        $this->paymentMethods = [
            'default' => 'twispay',
            'alt1' => 'twispay',
        ];
    }

    /**
     * @return void
     */
    public function toPaymentAction()
    {
        // Permanent 301 redirection
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: /paiement");
        exit();
    }

    /**
     * Home page for /payments
     *
     * @acceptsRequests GET
     * @return void
     */
    public function indexAction()
    {
        $selectedKey = $this->getRequest()->getParam('paymentMethodKey', 'default');

        if(array_key_exists($selectedKey, $this->paymentMethods) === FALSE) return $this->_redirect($this->view->getLink('home'));

        $selectedPaymentMethod = $this->paymentMethods[$selectedKey];
        $this->view->paymentMethod = $selectedPaymentMethod;
    }

    /**
     * This method simply displays ecardon popup.
     *
     * @acceptsRequests GET | POST
     * @return void
     */
    public function ecardonPopupAction()
    {
        $this->view->layout()->disableLayout();
    }

    /**
     * Ecardon must redirect users to this route
     * after a successful/failure payment process.
     *
     * @return void
     */
    public function ecardonResponseAction()
    {
        $token = preg_replace('#[^a-zA-Z0-9.\-]#', '', $this->_request->id);

        if (!$token) $this->_redirect('/payment');

        $ecardon = new Model_EcardonGateway();
        $result = $ecardon->getStatus($token);

        $resultDec = json_decode($result);

        if (isset($resultDec->result) && ($resultDec->result->code == '000.000.000' || $resultDec->result->code == '000.100.110')) {

            $this->view->success = true;
            $this->view->response = array(
                'Transaction Id' => $resultDec->descriptor,
                'Holder' => $resultDec->card->holder,
                'Amount' => $resultDec->amount,
                'Currency' => $resultDec->currency,
                'Payment Brand' => $resultDec->paymentBrand,
                'Date' => date('Y-m-d H:i:s', strtotime($resultDec->timestamp))
            );

        } else {
            $this->view->success = false;
        }
    }

    /**
     * Endpoint action to handle Twispay redirection,
     * whenever user completes payment process in https://secure.twispay.com
     *
     * @acceptsRequests GET
     * @return void
     * @throws Zend_Exception
     */
    public function twispayResponseAction()
    {
        $twispayConfigs = Zend_Registry::get('payment_config')['twispay'];
        $twispayKey = $twispayConfigs['key'];
        $twispayApiUrl = $twispayConfigs['api_url'];

        $decryptedData = Cubix_Twispay_TwispayApi::decryptIpnResponse($this->_request->opensslResult, $twispayConfigs['key']);

        // Getting Transaction data
        $ch = curl_init($twispayApiUrl . '/transaction/' . $decryptedData['transactionId']);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization:" . $twispayKey));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $transaction = json_decode(curl_exec($ch), true);

        if ($decryptedData['transactionStatus'] == 'in-progress' || $decryptedData['transactionStatus'] == 'complete-ok') {

            $this->view->success = true;
            $this->view->response = array(
                'Card Holder' => $transaction['data']['cardHolderName'],
                'Transaction Id' => $decryptedData['transactionId'],
                'Amount' => $decryptedData['amount'],
                'Currency' => $decryptedData['currency'],
                'Date' => date('Y-m-d H:i', $decryptedData['timestamp'])
				
            );

        } else {
            $this->view->success = false;
        }
    }

    /**
     * Is used only for ajax request.
     *
     * @acceptsRequests GET | POST*
     * @return void
     */
    public function checkoutAction()
    {
        try {
            $paymentMethod = $this->_request->paymentMethod;

            $methodName = 'checkoutWith' . $paymentMethod;

            if (!method_exists($this, $methodName)) throw new \Exception('Invalid Payment method!');

            $result = call_user_func([$this, $methodName]);
            echo json_encode($result, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES);

        } catch (\Exception $e) {
            echo json_encode([
                'status' => 'error',
                'error' => $e->getMessage()
            ]);
        }

        exit;
    }

    /**
     * Only $this->checkoutAction is allowed to call this method.
     * Please keep it private or protected.
     *
     * @acceptsRequests POST
     * @return array
     * @throws \Exception
     */
    private function checkoutWithTwispay()
    {
        $reference = 'd-payment-bl-' . uniqid();
        $amount = $this->_request->amount;

        $paymentConfigs = Zend_Registry::get('payment_config');
        $twispayConfigs = $paymentConfigs['twispay'];

        $data = array(
            'siteId' => intval($twispayConfigs['siteid']),
            'cardTransactionMode' => 'authAndCapture',
            'backUrl' => APP_HTTP . '://' . $_SERVER['HTTP_HOST'] . '/paiement/twispay-response',
            'invoiceEmail' => '',
            'customer' => [
                'identifier' => 'user-adspay-guest',
                'firstName' => '',
                'username' => '',
                'email' => '',
            ],
            'order' => [
                'orderId' => $reference,
                'type' => 'purchase',
                'amount' => $amount,
                'currency' => $twispayConfigs['currency'],
                'description' => 'Dedicated payment in Desktop beneluxxx',
            ]
        );

        $base64JsonRequest = Cubix_Twispay_TwispayApi::getBase64JsonRequest($data);
        $base64Checksum = Cubix_Twispay_TwispayApi::getBase64Checksum($data, $twispayConfigs['key']);

        try {
            $paymentFrom = "
                <form id=\"twispay-payment-form\" action='{$twispayConfigs['url']}' method='post' accept-charset='UTF-8'>
                    <input type='hidden' name='jsonRequest' value = \"{$base64JsonRequest}\" >
                    <input type='hidden' name='checksum' value = \"{$base64Checksum}\" >
                    <input type = \"submit\" value = \"Pay\" >
                </form > ";

        } catch (Exception $e) {
            return array(
                'status' => 'error',
                'error' => $e->getMessage()
            );
        }

        return array(
            'status' => 'twispay_success',
            'form' => $paymentFrom,
            'data' => $data,
            '$twispayConfigs' => $twispayConfigs
        );
    }

    /**
     * Only $this->checkoutAction is allowed to call this method.
     * Please keep it private or protected.
     *
     * @return array
     */
    private function checkoutWithEcardon()
    {
        $ref = 'd-payment-bl-' . md5(rand(100000, 1000000000));
        $amount = $this->_request->amount;

        $tokenParams = array(
            'amount' => $amount,
            'descriptor' => $ref,
            'merchantTransactionId' => $ref
        );

        $ecardonPayment = new Model_EcardonGateway($tokenParams);
        $tokenData = $ecardonPayment->checkout();
        $tokenDataDecoded = json_decode($tokenData);

        if (isset($tokenDataDecoded->result) && $tokenDataDecoded->result->code == '000.200.100') {
            return array(
                'status' => 'ecardon_success',
                'checkoutId' => $tokenDataDecoded->id
            );
        } else {
            return array(
                'status' => 'payment_failure'
            );
        }
    }
}
