<?php

class CamsController extends Zend_Controller_Action
{
	public function init()
	{	
		$this->view->layout()->setLayout('cams');
	}
		
	public function indexAction()
	{
		$this->user = Model_Users::getCurrent();
		$api_model = new Model_Api_Cams($this->user->id);
		$token = $api_model->userSignin($this->user);
		$redirect_path = null;
		if($this->_request->req)
		{
			$redirect_path = preg_replace('/[^-_a-z0-9\/]/i', '', $this->_request->req);
		}		
					
		$this->_response->setRedirect($this->view->getLink('bl-cams', array('one-time-token' => $token, 'over18' => 1, 'path' => $redirect_path )));
	}
	
	public function goToBookAction()
	{
		$user = Model_Users::getCurrent();
		$api_model = new Model_Api_Cams($user->id);
		$token = $api_model->userSignin($user);
		$escort_id = intval($this->_request->escort_id);
		$duration = intval($this->_request->duration);
		$amount = intval($this->_request->price);
		
		$model_vc = new Model_VideoChatRequests();
		$data = [
			'escort_id' => $escort_id,
			'duration' => $duration,
			'amount' => $amount,
			'session_id' => session_id(),
			'user_agent' => $_SERVER['HTTP_USER_AGENT'],
			'user_id' => $user->id
		];

		$video_booking_id = $model_vc->add($data);
		
		$params = [
			'booking-purchase-modal' => 1, 
			'escort_id' => $escort_id, 
			'one-time-token' => $token,
			'duration' => $duration,
			'over18' => 1,
			'video-booking-id' => $video_booking_id	
		];
		
		$url = $this->view->getLink('cams-booking-redirect', $params);
		echo json_encode( ['url' => $url]); die;
	}
	
	public function loginAction()
	{
		$this->view->layout()->disableLayout();
	}
	
	public function signupAction()
	{
		$this->view->layout()->disableLayout();
	}
		
	public function redirectAction()
	{
		header("HTTP/1.1 301 Moved Permanently");
		header('Location: https://escortforumit.xxx/ef-cams/' . $this->_getParam('req'));
		exit;
	}		
	
}