<?php

class CityVotesController extends Zend_Controller_Action
{
	private $_model;
	private $user;
		
	public function init() 
	{
		$this->user = $this->view->user = Model_Users::getCurrent();
		$this->view->layout()->setLayout('private-v2');
		
		if ( ! $this->user ) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}
		
		$this->_model = new Model_CityVotes();
	}
	
	public function indexAction()
	{
        $this->view->layout()->disableLayout();
        $this->view->note = $this->_model->getIfExpired( $this->user->id );

        //$data = $this->view->data = $this->_model->getData( $screenPage );
	}

    public function chartDataAction()
    {
        //$this->_helper->viewRenderer->setNoRender(true);
        $this->view->layout()->disableLayout();
        $this->view->data = $this->_model->getData();
    }
	
	public function saveAction()
	{
        $data = array();
		$this->view->layout()->disableLayout();

		/*if ($this->_request->isPost())
		{*/
			$validator = new Cubix_Validator();
            $city_id = intval( $this->_request->city_id );
            $page = intval( $this->_request->page );
						
			/*if (!$city_id)
				$validator->setError('city_id', 'Required');
			elseif ( $this->_model->checkCityExists($city_id, $this->user->id) )
				$validator->setError('city_id', 'exists');*/

			if ( $validator->isValid() ) 
			{

				$data = array(
					'user_id' => $this->user->id,
					'user_type' => $this->user->user_type,
                    'city_id' => $city_id
				);

                if( !$data['city_id'] ){
                    $data['city_id'] = 0;
                }

                $this->view->note = $this->_model->save( $data );

			}

			$this->view->status = $status =  $validator->getStatus();

            if( $status['status'] == 'success' ){
                $data = $this->view->data = $this->_model->getData( $page );
                $city_count = $this->view->cities = $this->_model->getCitiesCount();
            }

            //print_r( $data );
		//}
	}
}