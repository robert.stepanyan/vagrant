var mysql = require('mysql');
Memcached = require('memcached');
var PHPUnserialize = require('php-unserialize');

var config = require('../configs/config.js').get('production');
function UserManager() {
	var usersPrivateList = {}; //For internal use. Contains user info and sockets array
	var usersPublicList = {}; //Contains only user info
	var usersWithoutSockets = {};
	
	var client = mysql.createConnection({
		user:     config.db.user,
		database: config.db.database,
		password: config.db.password,
		host:     config.db.host,
		port: 3306,
		insecureAuth: true
	});
	
	var memcacheClient = new Memcached(config.memcache.host + ':' + config.memcache.port);
    memcacheClient.on('issue', function( details ){ console.log( details ) });
    memcacheClient.on('failure', function( details ){ console.error( "Server " + details.server + "went down due to: " + details.messages.join( '' ) ) });
    memcacheClient.on('reconnecting', function( details ){ console.error( "Total downtime caused by server " + details.server + " :" + details.totalDownTime + "ms")});
	/*-->Reconnecting to mysql if connection lost*/
	//It happens sometimes. about once in 8 hours
	function handleDisconnect() {
		client.on('error', function(err) {
			if (! err.fatal ) {
				return;
			}
			
			if ( err.code !== 'PROTOCOL_CONNECTION_LOST' ) {
				console.log("The mysql library fired a PROTOCOL_CONNECTION_LOST exception");
				throw err;
			}
			
			console.log('Re-connecting lost connection: ' + err.stack);

			var client = mysql.createConnection({
				user:     config.db.user,
				database: config.db.database,
				password: config.db.password,
				host:     config.db.host,
				port: 3306
			});
			handleDisconnect(client);
			client.connect();
		});
	}

	handleDisconnect();
	/*<--Reconnecting to mysql if connection lost*/
	
	var self = this;
	setInterval(function() {
		self.updateFakeUsers();
	}, 10*1000);
	
	this.getUsersPrivateList = function() 
	{
		return usersPrivateList;
	}
	
	this.getUsersPublicList = function() 
	{
		//Returns object by reference, not by value
		//Refactor this part later
		return cloneObject(usersPublicList);
	}
	
	//offline true - if is not in usersPrivateList(is offline) select from db
	//offline false - get only from usersPrivateList
	this.getUser = function(userId, offline, callback) 
	{
		if ( typeof offline == 'undefined' ) offline = false;
		
		//if in privateList (is online) get from there 
		if ( typeof usersPrivateList[userId] != 'undefined' ) {
			
			//If callback is given call calback else just return value
			if ( typeof callback == 'function' ) {
				return callback(usersPrivateList[userId]);
			}
			
			return usersPrivateList[userId];
		}
		
		//if is not in privateList(is offline) and offline = false return false
		if ( ! offline ) {
			return false;
		}
		
		//else select from db
		//here callback is required
		return this.getOfflineUser(userId, callback);
	}
	
	this.getUsers = function(userIds, offline, callback)
	{
		if ( ! userIds || ! userIds.length ) {
			if ( typeof callback == 'function' ) {
				callback({});
			}
			
			return false;
		}
		
		var users = {},
			c = 0;
		for ( var i = 0; i <= userIds.length - 1; i++ ) {
			this.getUser(userIds[i], offline, function(result) {
				if ( result ) {
					users[result.info.userId] = result;
				}
				c++;
				if ( c >= userIds.length ) {
					callback(users);
				}
			});
		}
		
		return false;
	}
	
	this.getOfflineUser = function(userId, callback)
	{
		
		//Getting user type
		client.query('SELECT id, user_type FROM users WHERE id = ?', [userId], function(error, result) {
			if ( error ) {console.log('ERROR: ' + error);}
			if ( ! result || result.length == 0 ) return callback(false);
			
			switch(result[0].user_type) {
				case 'escort' :
					getEscort(userId, function(result) {
						callback(result);
					});
					break;
				case 'member' :
					getMember(userId, function(result) {
						callback(result);
					});
					break;
				case 'agency' :
					getAgency(userId, function(result) {
						callback(result);
					});
				break;
			}
		});
	}
	
	this.addUser = function(socketId, info, isPrivate)
	{
		// If already have in list only add socket in array
		// Else add new element in array with userId

		if ( typeof isPrivate == 'undefined' ) {
			isPrivate = false;
		}

		if ( ! usersPrivateList[info.userId] ) { 
			usersPrivateList[info.userId] = {
				sockets: [],
				info: info
			}
		}
		
		//if user is invisible do not add to public list
		if ( ! isPrivate ) {
			this.addUserToPublicList(info);
		}
		
		addSocket(info.userId, socketId);
		this.updateOnlineEscorts();
	}
	
	this.removeUser = function(userId) 
	{
		delete usersPrivateList[userId];
		delete usersPublicList[userId];
		delete usersWithoutSockets[userId];
		console.log("removeUser : " +userId);
		this.updateOnlineEscorts();
	}
	
	this.removeUserFromPublicList = function(userId) 
	{
		delete usersPublicList[userId];
	}
	
	this.addUserToPublicList = function(info) 
	{
		usersPublicList[info.userId] = info;
	}
	
	this.removeSocket = function(userId, socketId)
	{
		if ( ! usersPrivateList[userId] ) return;
		var sockets = usersPrivateList[userId].sockets;
		for(var i = 0; i <= sockets.length; i++) {
			if ( sockets[i] == socketId ) {
				usersPrivateList[userId].sockets.splice(i, 1);
				break;
			}
		}
		
		//If user has no other sockets(f.e. close browser or all tabs) put him
		//in array which we will check in some period. If he will not 
		//come back for a while we will remove him from online users list.
		if ( usersPrivateList[userId].sockets.length == 0 ) {
			usersWithoutSockets[userId] = (new Date()).getTime();
		}
	}
	
	function addSocket(userId, socketId)
	{
		if ( ! usersPrivateList[userId] ) return;
		
		sockets = usersPrivateList[userId].sockets;
		for(var i = 0; i <= sockets.length; i++) {
			if ( sockets[i] == socketId ) return;
		}
		
		usersPrivateList[userId].sockets.push(socketId);
		
		//If user in usersWithoutSockets list remove him from there
		if ( typeof usersWithoutSockets[userId] !== "undefined") {
			delete usersWithoutSockets[userId];
		}
	}
	
	this.startOfflineUsersCheck = function(callback)
	{
		var self = this;
		setInterval(function() {
			var removedUserIds = [];
			var date = new Date();
			for(var userId in usersWithoutSockets) {
				if ( typeof usersPrivateList[userId] !== "undefined" && usersPrivateList[userId].sockets.length === 0 && (date.getTime() - usersWithoutSockets[userId]) >= 2 * 1000 ) {
					//If has no sockets more than 5 seconds remove from public and private online users list
					self.removeUser(userId);
					removedUserIds.push(userId);
					
				}
			}
			callback(removedUserIds);
		}, 10 * 1000);
	}
	
	this.getOnlineEscorts = function(callback)
	{
		var userIds = [];
		for(var i in usersPublicList) {
			if ( usersPublicList[i].userType == 'escort' ||  usersPublicList[i].userType == 'agency' ) {
				userIds.push(i);
			}
		}
		
		callback(userIds);
			
	}
	
	this.addUserWithoutSockets = function(userId)
	{
		usersWithoutSockets[userId] = new Date();
	}
	
	//Setting online escorts to memcache for web app.
	this.updateOnlineEscorts = function()
	{
		this.getOnlineEscorts(function(userIds) {
			memcacheClient.set(config.common.applicationId + '_chat_online_escorts', JSON.stringify(userIds), 60*1000, function(error, ok) {
				if ( error ) {
					console.trace(error);
				}
			});
		});
	}
	
	
	//
	this.updateFakeUsers = function()
	{
		var self = this;
		console.log('Updating fake accounts');
		var date = new Date();
		// for(var userId in usersPrivateList) {
		// 	if ( usersPrivateList[userId] && usersPrivateList[userId].info.f === 1 &&  (date.getTime() - usersPrivateList[userId].info.timeToo) > 0) {
		// 		console.log("remove user " + userId);
		// 		console.log({timeToo1: usersPrivateList[userId].info.timeToo, timeToo2:date.getTime(), date: date.toLocaleString()});
		// 		self.removeUser(userId);
		// 	}
		// }
		
		
		// memcacheClient.get(config.common.applicationId + '_chat_fake_users', function(error, data) {
		// 	if ( error ) {
		// 		console.log(error);
		// 	} else {
		// 		var date = new Date();
		// 		if ( data ) {
		// 			var users = JSON.parse(PHPUnserialize.unserialize(data));
		// 			var removeExpectUsers = [];
		// 			for(var i in users) {
		// 				var user = users[i];
		// 				if((date.getTime() - user.timeToo) <= 0){
         //                    user.f  = 1;
         //                    user.status = 'online';
         //                    self.addUser('fakesocketid', user);
         //                    removeExpectUsers.push(user.userId);
         //                    console.log("update fake users list(add/update) user: "+ user.userId)
		// 				}else{
         //                    if(usersPrivateList.hasOwnProperty(user.userId)){
         //                        console.log("remove user(fake session time of chat expired)" + user.userId);
         //                        var obj = {expTime: usersPrivateList[user.userId].info.timeToo, currTime:date.getTime()};
         //                        obj.timeDiff = obj.expTime - obj.currTime;
         //                        console.log(obj);
         //                        self.removeUser(user.userId);
         //                    }else{
         //                        console.log("user fake session time expired " + user.userId);
         //                    }
		// 				}
		// 			}
         //            for(var user_id in usersPrivateList) {
         //                if ( usersPrivateList[user_id] && usersPrivateList[user_id].info.f === 1 && !in_array(user_id, removeExpectUsers)) {
         //                	console.log("remove user(fake session time of chat expired or removed)" + user_id);
         //                	console.log(removeExpectUsers.indexOf(user_id));
         //                    self.removeUser(user_id);
         //                }
         //            }
		// 		}
		// 	}
		// });

        getChatFakeUsers(function (error, data) {
            if ( error ) {
                console.log(error);
            } else {
                var date = new Date();
                if ( data ) {
                    var removeExpectUsers = [];
                    for(var i in data) {
                        var user = data[i];
                        if((date.getTime() - user.timeToo) <= 0){
                            user.f  = 1;
                            user.status = 'online';
                            self.addUser('fakesocketid', user);
                            removeExpectUsers.push(user.userId);
                           // console.log("update fake users list(add/update) user: "+ user.userId)
                        }else{
                            if(usersPrivateList.hasOwnProperty(user.userId)){
                                console.log("remove user(fake session time of chat expired)" + user.userId);
                                var obj = {expTime: usersPrivateList[user.userId].info.timeToo, currTime:date.getTime()};
                                obj.timeDiff = obj.expTime - obj.currTime;
                                console.log(obj);
                                self.removeUser(user.userId);
                            }else{
                                console.log("user fake session time expired " + user.userId);
                            }
                        }
                    }
                    for(var user_id in usersPrivateList) {
                        if ( usersPrivateList[user_id] && usersPrivateList[user_id].info.f === 1 && !in_array(user_id, removeExpectUsers)) {
                            console.log("remove user(fake session time of chat expired or removed)" + user_id);
                            console.log(removeExpectUsers.indexOf(user_id));
                            self.removeUser(user_id);
                        }
                    }
                }
            }
        })
	}

	in_array = function(value, array){
		for(var i =0; i < array.length; i++){
			if(array[i] == value){
				return true;
			}
		}
		return false;
	}
	
	getEscort = function(userId, callback) 
	{
		client.query('SELECT e.id, e.showname, ep.hash, ep.ext FROM escorts e INNER JOIN escort_photos ep ON ep.escort_id = e.id AND is_main = 1 WHERE e.user_id = ?', [userId], function(error, result) {
			if ( ! result.length ) {
				if ( typeof callback == 'function' ) {
					return callback(false);
				} else {
					return false;
				}
			}
			escort = {
				info : {
					nickName : result[0].showname + ' (Escort)',
					userId : userId,
					userType : 'escort',
					avatar : getEscortImageUrl(result[0].id, result[0].hash, result[0].ext),
					status : 'offline',
					link: '/accompagnatrici/' + result[0].showname + '-' + result[0].id
				},
				sockets : []
			}
			
			if ( typeof callback == 'function' ) {
				callback(escort);
			}
		});
		
	}
	
	getMember = function(userId, callback)
	{
		client.query('SELECT username FROM users WHERE id = ?', [userId], function(error, result) {
			if ( ! result.length ) {
				if ( typeof callback == 'function' ) {
					return callback(false);
				} else {
					return false;
				}
			}
			member = {
				info : {
					nickName: result[0].username,
					userId : userId,
					userType : 'member',
					status : 'offline',
					avatar : config.common.noPhotoUrl,
					link: '/member/' + result[0].username
				},
				sockets : []
			}
			
			if ( typeof callback == 'function' ) {
				callback(member);
			}
		});
	}
	
	getAgency = function(userId, callback)
	{
		client.query('SELECT username FROM users WHERE id = ?', [userId], function(error, result) {
			if ( ! result.length ) {
				if ( typeof callback == 'function' ) {
					return callback(false);
				} else {
					return false;
				}
			}
			agency = {
				info : {
					nickName: result[0].username,
					userId : userId,
					userType : 'agency',
					status : 'offline',
					avatar : config.common.noPhotoUrl
				},
				sockets : []
			}
			
			if ( typeof callback == 'function' ) {
				callback(agency);
			}
		});
	}
	
	getEscortImageUrl = function(escortId, hash, ext)
	{
		parts = [];
		escortId = escortId.toString(); //converting to string
		if ( escortId.length > 2 ) {
			parts.push(escortId.substr(0, 2));
			parts.push(escortId.substr(2));
		} else {
			parts.push('_');
			parts.push(escortId);
		}
		catalog = parts.join('/');
		return config.common.imagesHost + '/' + catalog + '/' + hash + '_' + config.common.avatarSize +  '.' + ext;
	}

	getChatFakeUsers = function(callback){
		var query = "SELECT e.showname, e.id, e.user_id, ep.hash AS photo_hash, ep.ext AS photo_ext, cfus.date_to" +
			" FROM chat_fake_users_schedule cfus" +
			" INNER JOIN escorts e ON e.user_id = cfus.user_id" +
			" INNER JOIN escort_photos ep ON ep.escort_id = e.id AND ep.is_main = 1" +
			" WHERE NOW() > cfus.date_from AND NOW() < cfus.date_to";
        client.query(query, function (error, escorts) {
        	if(error) console.log(error);

            if ( ! escorts.length ) {
                if ( typeof callback == 'function' ) {
                    return callback(error, false);
                } else {
                    return false;
                }
            }
            var data = [];
            for(var i in escorts) {
                var parts = [];
                var escort = escorts[i];
                if ( escort.id > 99 ) {
                	var id = escort.id+'';
                    parts.push(id.substr(0, 2));
                    parts.push(id.substr(2));
                }
                else {
                    parts.push('_');
                    parts.push(escort.id);
                }
                var catalog = parts.join('/');
                var d=new Date(escort.date_to);
                data.push({
                    'nickName': escort.showname + ' (Escort)',
                    'userId': escort.user_id,
                    'userType': 'escort',
                    'timeToo': d.getTime(), // chat fake users session time in miliseconds
                    'link': '/escort/' + escort.showname + '-' + escort.id,
                    'avatar': config.common.imagesHost+ catalog + '/' + escort.photo_hash + '_lvthumb.' + escort.photo_ext
                });
            }
            if ( typeof callback == 'function' ) {
                callback(error, data);
            }
        })
	}

}

function cloneObject(obj)
{
	var c = new Object();
	for (var e in obj) {
		c[e] = obj[e];
	}
	return c;
}

function get() {
	return new UserManager();
}
exports.get = get;