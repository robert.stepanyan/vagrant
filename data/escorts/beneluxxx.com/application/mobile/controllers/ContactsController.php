<?php

class Mobile_ContactsController extends Zend_Controller_Action
{
	public function indexAction()
	{

	}

	public function contactUsAction()
	{
		$this->view->layout()->disableLayout();

		if ( $this->_request->isPost() ) {

			$validator = new Cubix_Validator();

			$data = new Cubix_Form_Data($this->_request);
			$fields = array(
				'name' => '',
				'email' => '',
				'message' => ''
			);
			$data->setFields($fields);
			$data = $data->getData();

			if ( ! strlen($data['email']) ) {
				$validator->setError('email', 'Email is required');
			} elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
				$validator->setError('email', 'Wrong email format');
			}

			if ( ! strlen($data['message']) ) {
				$validator->setError('message', 'Message is required');
			}

			$result = $validator->getStatus();

			if ( ! $validator->isValid() ) {

				if ( ! is_null($this->_getParam('ajax')) ) {

					//$result['status'] = 'success';

					echo(json_encode($result));
					ob_flush();
					die;
				}
			}
			else {

				// Fetch administrative emails from config
				$config = Zend_Registry::get('feedback_config');
				$site_emails = $config['emails'];

				$email_tpl = 'feedback_template';
				$data['to_addr'] = $site_emails['support'];

				$tpl_data = array(
					'name' => $data['name'],
					'email' => $data['email'],
					'to_addr' => $data['to_addr'],
					'message' => $data['message'],
					//'escort_showname' => $about_escort->showname,
					//'escort_id' => $about_escort->id,
					//'sent_by' => $sent_by
				);

				Cubix_Email::sendTemplate($email_tpl, $data['to_addr'], $tpl_data, isset($escort) ? $data['email'] : null);

				echo json_encode($result);
				die;
			}
		}
	}

	public function contactUsSuccessAction()
	{
		$this->view->layout()->disableLayout();
	}

	public function contactToAction()
	{

        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

		$req = $this->_request;

		$id = $this->view->id = $req->id;
		$type = $this->view->type = $req->type;

		$is_ajax = ! is_null($this->_getParam('ajax'));

		if ( ! $id || ! $type ) die;

		if ( $type == 'e' ) {
			$model = new Model_EscortsV2(); $id = intval( $id );
			$to_user = $this->view->to_user = $model->get($id);
			$this->view->to_user_title = $this->view->t('escort');
		} elseif ( $type == 'a' ) {
			$model = new Model_Agencies();
			$to_user = $this->view->to_user = $model->get($id);
			$this->view->to_user_title = $this->view->t('agency');

		} elseif ( $type == 'm' ) {

			$this->view->to_user_title = $this->view->t('member');
		}

		//$feedback = new Model_Feedback();
		//$blackListModel = new Model_BlacklistedWords();
		$config = Zend_Registry::get('feedback_config');

		if ( $req->isPost() ) {

            $data = $this->getRequest()->getPost();

			$validator = new Cubix_Validator();

			if ( ! strlen($data['email']) ) {
				$validator->setError('email', '' /*'Email is required'*/);
			} elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
				$validator->setError('email', '' /*'Wrong email format'*/);
			}

            $blackListModel = new Model_BlacklistedWords();
			if ( ! strlen($data['message']) ) {
				$validator->setError('message','' /*'Please type the message you want to send'*/);
			} elseif( $blackListModel->checkWords($data['message']) ){
                $validator->setError('message', 'You can`t use words "'.$blackListModel->getWords() .'"');
            }


			if ( ! strlen($data['captcha']) ) {
				$validator->setError('captcha', '');//Captcha is required
			}
			else {
				$session = new Zend_Session_Namespace('captcha');
				$orig_captcha = $session->captcha;

				if ( strtolower($data['captcha']) != $orig_captcha ) {
					$validator->setError('captcha', '');//Captcha is invalid
				}
			}

			$result = $validator->getStatus();

			if ( ! $validator->isValid() ) {

				if ( ! is_null($this->_getParam('ajax')) ) {

					//$result['status'] = 'success';

					echo(json_encode($result));
					ob_flush();
					die;
				}
			}
			else {

				$email_tpl = "escort_feedback";

				$tpl_data = array(
					'name' => $data['name'],
					'email' => $data['email'],
					'to_addr' => $to_user->email,
					'message' => $data['message'],
					'showname' => $to_user->showname,
				);

				Cubix_Email::sendTemplate($email_tpl, $to_user->email, $tpl_data);

				$session->unsetAll();

				echo json_encode($result);
				die;
			}
		}
	}

	public function contactToSuccessAction()
	{
		$this->view->layout()->disableLayout();

		$req = $this->_request;

		$id = $this->view->id = (int) $req->id;
		$type = $this->view->type = $req->type;
		$is_ajax = ! is_null($this->_getParam('ajax'));

		if ( ! $id || ! $type ) die;

		if ( $type == 'e' ) {
			$model = new Model_EscortsV2();
			$to_user = $this->view->to_user = $model->get($id);
			$this->view->to_user_title = $this->view->t('escort');
		} elseif ( $type == 'a' ) {
			$model = new Model_Agencies();
			$to_user = $this->view->to_user = $model->get($id);
			$this->view->to_user_title = $this->view->t('agency');

		} elseif ( $type == 'm' ) {

			$this->view->to_user_title = $this->view->t('member');
		}
	}
}
