<?php

class Model_Cities extends Cubix_Model 
{
	protected $_table = 'cities';

	public function getByCountry($id) 
	{
		$sql = '
			SELECT c.id, c.' . Cubix_I18n::getTblField('title') . ' as title
			FROM cities c
			WHERE country_id = ' . $id . '
			ORDER BY title
		';

		return parent::_fetchAll($sql);
	}
	
	public function getByCountries( array $ids)
	{
		$ids_str = implode(',', $ids);
		$sql = '
			SELECT c.id, c.slug, c.country_id, c.' . Cubix_I18n::getTblField('title') . ' as title, co.' . Cubix_I18n::getTblField('title') . ' as country_title
			FROM cities c
			INNER JOIN countries co ON co.id = c.country_id
			WHERE country_id IN (' . $ids_str . ')
			ORDER BY title
		';
		return parent::_fetchAll($sql);
	}
	
	public function getByIds($ids)
	{
		return parent::_fetchAll('SELECT id, ' . Cubix_I18n::getTblField('title') . ' as title FROM cities WHERE id IN (' . $ids . ') ORDER BY title ASC');
	}
	
	public function getBySlug($slug)
	{
		return parent::getAdapter()->query('SELECT id AS id, id AS city_id, ' . Cubix_I18n::getTblField('title') . ' as city_title FROM cities WHERE slug = ?', $slug)->fetch();
	}
	
	public function getByGeoIp()
	{
		$geoData = Cubix_Geoip::getClientLocation();
		
		/*if ($_GET['test'] == 1)
			var_dump($geoData);*/
		
		if (!is_null($geoData))
		{
			$log = false;
			
			if (strlen($geoData['city']))
			{
				$city_id = parent::getAdapter()->fetchOne('SELECT id FROM cities WHERE title_geoip = ?', $geoData['city']);
				
				if ($city_id)
				{
					$ordering = 'eic.is_premium DESC, ' . 'FIELD(eic.city_id, ' . $city_id . ') DESC, ' . 'eic.ordering DESC';
					
					return array('ordering' => $ordering, 'city_id' => $city_id);
				}
				else
					$log = true;
			}
			else
			{
				//$log = true;
				$log = false;
			}
			
			if ($log)
			{
				//$str = date('d M, Y H:i:s') . ' - City: ' . $geoData['city'] . ' - ' . var_export($geoData, true) . "\n";
				//file_put_contents('/var/log/geoip_missing_cities.log', $str, FILE_APPEND);
				
				$geoData['city'] = utf8_encode($geoData['city']);
				
				$c = parent::getAdapter()->fetchOne('SELECT COUNT(*) FROM geoip_missing_cities WHERE city = ?', $geoData['city']);
				
				if ($c == 0)
					parent::getAdapter()->insert('geoip_missing_cities', array(
						'ip' => $geoData['ip'],
						'country' => $geoData['country'],
						'country_iso' => $geoData['country_iso'],
						'region' => $geoData['region'],
						'city' => $geoData['city'],
						'latitude' => $geoData['latitude'],
						'longitude' => $geoData['longitude']
					));
				
				return null;
			}
		}
		else
			return null;
	}

    public function getByCountriesWithEscorts( $ids )
    {
        $ids_str = implode(',', $ids);

        $where = array(
            'ec.gender = ?' => GENDER_FEMALE,
            'ec.is_agency = ?' => false,
            'ec.is_tour = ?' => null,
            'ec.is_upcoming = ?' => false
        );

        $where = self::getWhereClause($where, true);

        $sql = '
			SELECT c.id, c.slug, c.country_id, c.' . Cubix_I18n::getTblField('title') . ' as title, co.' . Cubix_I18n::getTblField('title') . ' as country_title, COUNT(DISTINCT ec.escort_id) as escort_count
			FROM cities c
			INNER JOIN escorts_in_cities ec ON ec.city_id = c.id
			INNER JOIN countries co ON co.id = c.country_id
			WHERE c.country_id IN (' . $ids_str . ') AND ' . (!is_null($where) ? '' . $where : '') . '
			GROUP BY ec.city_id
			ORDER BY title
		';

        return parent::_fetchAll($sql);
    }

    /*static function haveEscorts( $city_id ){
        $sql = "
			SELECT * COUNT
			FROM escorts e
			WHERE city_id = {$city_id}
		";
        return parent::_fetchAll($sql);
    }*/
}
