<?php

class Model_Api_Cams
{
	
	private $dev_site_id = 's_4f1e2cf24aed40ed0614fd2ccd6bc952'; // dev
	private $dev_domain = 'https://escort.dev-dliver.prmrgt.com'; //dev
	private $prod_site_id = 's_056ca7626d1ad085f44f781ed174564d';
	private $domain = 'https://escort.dliver.prmrgt.com';
	
	private $url_escort_create = '/escort-api/v2/site-group/beneluxxx_sites/escorts/{profile_remote_id}';
	private $url_escort_login = '/escort-api/v2/site-group/beneluxxx_sites/escorts/{profile_remote_id}/login';
	private $url_member_create = '/escort-api/v2/site-group/beneluxxx_sites/members/{profile_remote_id}';
	private $url_member_login = '/escort-api/v2/site-group/beneluxxx_sites/members/{profile_remote_id}/login';
	private $url_member_purchase = '/escort-api/v2/site-group/beneluxxx_sites/members/{profile_remote_id}/purchase';
	
	public function __construct($user_id, $use_debug = false )
	{
		$this->user_id = $user_id;
		if( IS_DEBUG || $use_debug ){
			$this->prod_site_id = $this->dev_site_id;
			$this->domain = $this->dev_domain;
			$this->debug = true;
		}
	}		
	public function escortLogin($fields)
	{
		$url_escort_login = str_replace("{profile_remote_id}", $fields['remote_id'], $this->url_escort_login);
		unset($fields['remote_id']);
		$fields['signup_site_id'] = $this->prod_site_id;
		$url = $this->domain . $url_escort_login;
		return $this->send($url, $fields);
	}		
	
	public function escortCreate($fields)
	{
		$escort_id = $fields['remote_id'];
		$url_escort_create = str_replace("{profile_remote_id}", $fields['remote_id'], $this->url_escort_create);
		unset($fields['remote_id']);
		$fields['signup_site_id'] = $this->prod_site_id;
		$url = $this->domain . $url_escort_create;
		
		$results = $this->send($url, $fields);
		if($results['status'] == 409 && $results['body']['error']['code'] == 'ERR_EMAIL_ALREADY_TAKEN'){
			$fields['email'] = $this->escortCreateFakeEmail($escort_id, $fields['email']);
			$results = $this->send($url, $fields);
		}
		
		return $results;
	}
	
	public function escortUpdate($fields)
	{
		$url_escort_create = str_replace("{profile_remote_id}", $fields['remote_id'], $this->url_escort_create);
		unset($fields['remote_id']);
		
		$fields['signup_site_id'] = $this->prod_site_id;
		$url = $this->domain . $url_escort_create;
		$api_results = $this->send($url, $fields, 'patch');
		return $api_results;
	}
	
	public function escortCreateFakeEmail($escort_id, $email)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$fake_email = str_replace('@', '.fk'.$escort_id.'@', $email);
		$client->call('Users.saveCamsFakeEmail', array($this->user_id, $fake_email));
		return $fake_email;
	}		
	
	public function memberCreate($fields)
	{
		$url_member_create = str_replace("{profile_remote_id}", $this->user_id, $this->url_member_create);
		$fields['signup_site_id'] = $this->prod_site_id;
		$url = $this->domain . $url_member_create;
		return $this->send($url, $fields);
	}
	
	public function memberLogin($fields)
	{
		$url_member_login = str_replace("{profile_remote_id}", $this->user_id, $this->url_member_login);
		$fields['signup_site_id'] = $this->prod_site_id;
		$url = $this->domain . $url_member_login;
		return $this->send($url, $fields);
	}
	
	public function memberPurchase($fields)
	{
		$url_member_purchase = str_replace("{profile_remote_id}", $this->user_id, $this->url_member_purchase);
		$fields['site_id'] = $this->prod_site_id;
		
		$url = $this->domain . $url_member_purchase ;
		
		return $this->send($url, $fields);
	}
	
	public function userSignin($user)
	{
		$token = null;
		if($user->isMember()){
			$fields = array(
				'display_name' => $user->username,
				'email' => $user->email
			);
						
			$api_results = $this->memberLogin($fields);
			
			if($api_results['status'] == 200){
				$token = $api_results['body']['auth_token'];
			}
			elseif($api_results['status'] == 404){
				$api_results = $this->memberCreate($fields);
				if($api_results['status'] == 200 ){
					$token = $api_results['body']['auth_token'];
				}
			}
		}
		
		if ($user->isEscort() && !$user->escort_data['block_cam']) {
			
			$fields = $this->prepareEscortDataForApi($user);
			//$start_time = microtime(true);
			$api_results = $this->escortLogin($fields); 
			
			//$end_time = microtime(true);
			// Just print the execution time
			//die('Execution took ' . round($end_time - $start_time, 4) . ' seconds');
			
			if($api_results['status'] == 200){
				$token = $api_results['body']['auth_token'];
			}
			elseif($api_results['status'] == 404){
				$api_results = $this->escortCreate($fields);
				if($api_results['status'] == 200){
					$token = $api_results['body']['auth_token'];
				}
			}
		}
		
		return $token;
	}
	
	private function send($url, $fields, $type = 'post')
	{
		$system = Zend_Registry::get('system_config');
		$secret = $system['cams']['secret'];
		
		$headers  = [
			'X-Server-Secret: '.$secret,
			'Content-Type: application/json'
		];

		//$fields_string = json_encode($fields, JSON_HEX_QUOT | JSON_HEX_TAG);
		$fields_string = json_encode($fields);
		
		//open connection
		$ch = curl_init();
	
		curl_setopt($ch, CURLOPT_URL, $url);
		if($type === 'post'){
			curl_setopt($ch, CURLOPT_POST, 1);
		}
		else{
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
		}
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		//execute post
		$result = curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		
		//close connection
		curl_close($ch);
		$this->log($url, $httpcode, $fields_string, $result);
		return array('body' => json_decode($result, true), 'status' => $httpcode );
	}
	
	private function log($url, $status, $request, $response)
	{
		$db = Zend_Registry::get('db');
		
		$log_data = array(
			'user_id' => $this->user_id,
			'url' => $url, 
			'status' => $status, 
			'request' => $request,
			'response' => $response
		);
		
		$db->insert('cam_logs', $log_data);
		
	}
	
	public function prepareEscortDataForApi($user)
	{
		$bl_countries_iso = array();
		$langs = array();

		$escort_id = $user['escort_data']['escort_id'];
		$model_escort = new Model_EscortsV2();
		$model_blc = new Model_BlockedCountries();
		$escort_profile = $model_escort->get($escort_id);
		
		if($escort_profile['langs']){
			$langs = array_map(function($el) { return $el['id']; }, $escort_profile['langs']);
		}

		$bl_countries = $model_blc->getByEscortId($escort_id);
		if($bl_countries){
			$bl_countries_iso = array_map(function($el) { return $el->iso; }, $bl_countries);
		}

		$photos =  $escort_profile->getPhotos(1, $count, true, false, null, 11);
				
		foreach( $photos as $photo){
			
			if($photo['is_main']){
				$profile_image = array(
					'height' => $photo['height'],
					'url' =>  $photo->getUrl('orig', false, false),
					'width' => $photo['width']
				);
			}
			else{
				$gallery_photos[] = array(
					'height' => $photo['height'],
					'url' =>  $photo->getUrl('orig', false, false),
					'width' => $photo['width']
				);
			}
		}
		
		$about_me = strlen($escort_profile['about_fr']) > 0 ? strip_tags($escort_profile['about_fr']) : strip_tags($escort_profile['about_en']);
		$subscription = ( $user['escort_data']['package_id'] && $user->escort_data['escort_status'] & 32) ? true : false;
			
		// video_booking details 
		$client = new Cubix_Api_XmlRpc_Client();
		$video_booking = $client->call('CamBooking.get', array( $escort_id ) );
		$video_booking_enabled = (bool) $video_booking['status'];  // (boolean)
				
		$email = $user->cams_fake_email ? $user->cams_fake_email : $user->email;
		$fields = array(
			'about' => $about_me,
			'blocked_countries' => $bl_countries_iso,
			'display_name' => $user['escort_data']['showname'],
			'remote_id' => $escort_id,
			'email' => $email,
			'gallery_images' => $gallery_photos,
			'profile_image' => $profile_image,
			'languages' => $langs,
			'legal_name' => $user['escort_data']['showname'],
			'nationality' => $escort_profile['nationality_iso'],
			'subscription_active' => $subscription,
			'video_booking_enabled' => $video_booking_enabled,
		);
		
		if($video_booking){
			$video_booking_contact_type =  Model_CamBooking::$contact_options[$video_booking['contact_option']]; // whatsapp|sms|phone|viber
			$video_booking_prices = [
				['duration' => '15m' , 'token' => $video_booking["price_30"] * 10],
				['duration' => '30m' , 'token' => $video_booking["price_60"] * 10]
			];
			
			$fields['video_booking_prices'] = $video_booking_prices;
			$fields['video_booking_contact_type'] = $video_booking_contact_type;
		}
					
		return $fields;
	}
		
}
