<?php

class Model_Api_Members
{
	/**
	 * @return Cubix_Api_XmlRpc_Client
	 */
	protected static function getApiClient()
	{
		static $client;
		
		if ( empty($client) ) {
			$client = new Cubix_Api_XmlRpc_Client();
		}
		
		return $client;
	}
	
	public function authenticate($username, $password)
	{
		$user = self::getApiClient()->call('Members.getMemberByCredentials', array($username, $password));
		
		if ( is_null($user) ) {
			throw new Exception('Wrong credentials');
		}
		
		return $user;
	}
	
	public function resetPassword($email)
	{
		return self::getApiClient()->call('Members.resetPassword', array($email));
	}
	
	public function getFavorites()
	{
		return self::getApiClient()->call('Members.getFavorites');
	}
}
