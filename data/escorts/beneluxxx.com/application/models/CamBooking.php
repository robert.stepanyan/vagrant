<?php

class Model_CamBooking extends Cubix_Model
{
	const STATUS_ENABLED = 1;
	const STATUS_DISABLED = 0;

	public static $contact_options = array(
		1 => 'whatsapp', 
		2 => 'sms', 
		3 => 'phone_call', 
		4 => 'viber' 
	);

	public function save($data)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$status = $client->call('CamBooking.saveCamSettings', array($data));
		return $status;
	}
	
	public function getData($escort_id){
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('CamBooking.getData', array($escort_id));
	}
	
	public function getDataForProfile($escort_id)
	{
		$sql = 'SELECT cam_channel_url, cam_status, cam_preview_url FROM escorts WHERE id = ?';
		return self::db()->fetchRow($sql, array($escort_id));
	}

	
}
