<?php
class Model_Robots extends Cubix_Model
{
	public function get()
	{
		return self::getAdapter()->fetchOne('SELECT robots FROM applications WHERE id = ?', Cubix_Application::getId());
	}
	
	public function getMobile()
	{
		return self::getAdapter()->fetchOne('SELECT m_robots FROM applications WHERE id = ?', Cubix_Application::getId());
	}
}
