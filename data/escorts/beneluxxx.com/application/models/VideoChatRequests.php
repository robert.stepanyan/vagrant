<?php

class Model_VideoChatRequests extends Cubix_Model
{
	public function add($data)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('VideoChatRequests.save', array($data));
	}
	
	public function update($id, $data)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('VideoChatRequests.update', array($id, $data));
	}
	
	public function getDataforNotification($escort_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('VideoChatRequests.getDataforNotification', array($escort_id));
	}
	
	public function sendSms($escort, $name, $skype, $customer_email, $request_id)
	{
		//$phone = '0037499555538';
		
		$duration = $escort['duration'];
		$phone = $escort['phone'];
		
		$text = "Nuova richiesta video skype: $name skype id $skype ha pagato per te una chiamata di $duration minuti. Accetta la sua richiesta e organizza la tua chiamata! Dettagli nel tuo account su escortforumit.xxx. info@escortforumit.xxx";
		
		$conf = Zend_Registry::get('system_config');
		$conf = $conf['sms'];
		$SMS_DeliveryNotificationURL = $conf['DeliveryNotificationURL'];
		$SMS_NonDeliveryNotificationURL = $conf['NonDeliveryNotificationURL'];
		$SMS_BufferedNotificationURL = $conf['BufferedNotificationURL'];
		
		// Get from number
		$originator = $phone_from = Cubix_Application::getPhoneNumber(Cubix_Application::getId());
		$sms_id = Cubix_Api::getInstance()->call('outbox', array($escort['id'], $phone, $originator, $text, Cubix_Application::getId(), null, 1));
		
						
		$sms = new Cubix_SMS($conf['userkey'], $conf['password']);
		$sms->setBufferedNotificationURL($SMS_BufferedNotificationURL);
		$sms->setDeliveryNotificationURL($SMS_DeliveryNotificationURL);
		$sms->setNonDeliveryNotificationURL($SMS_NonDeliveryNotificationURL);
		$sms->setOriginator($originator);
		$sms->setRecipient($phone, $sms_id);
		$sms->setContent($text);
		$sms->sendSMS();
		
		
		$client = new Cubix_Api_XmlRpc_Client();
		$client->call('VideoChatRequests.update', array($request_id, array('sms_text' => $text)) );
		
		$model_pm = new Cubix_PrivateMessaging('member', 525597); // Skype Video Booking;
		$model_pm->sendMessage($text, 'escort', $escort['id'], false, false);
		
		//SENDING EMAIL TO CUSTOMER 
		$email_content = Cubix_I18n::translateByLng('it', 'video_call_cutomer_email_text', array(
			'skype' => $skype,
			'duration' => $duration,
			'escort' => $escort['showname']
		));
		
		Cubix_Email::sendTemplate('universal_message', $customer_email , array(
			'subject' => 'Nuova Richiesta per Cam',
			'message' => $email_content
		));
		
	}
	
	public function checkBySession($id, $session){
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('VideoChatRequests.checkBySession', array($id, $session));
	}
	
	public function checkExists($id){
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('VideoChatRequests.checkExists', array($id));
	}
	
	public function getData($escort_id){
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('VideoChatRequests.getData', array($escort_id));
	}
	
	public function getPrice($escort_id, $duration){
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('VideoChatRequests.getPrice', array($escort_id, $duration));
	}
	
	public function getForSuccess($request_id){
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('VideoChatRequests.getForSuccess', array($request_id));
	}
	
	public function sendSmsCam($escort, $membername,  $additional, $request_id)
	{
		$duration = $additional['duration'];
		$phone = $additional['contact_phone_parsed'];
		$customer_email = $additional['email'];

		//$phone = '0037499555538';
		//$customer_email = 'badalyano@gmail.com';

		$token_count = $additional['amount'] * 10;
		
		if ($additional['schedule_time'] == 'ASAP'){
            $schedule_time = Cubix_I18n::translateByLng('fr', 'ASAP');
        }
		elseif($additional['schedule_time'] == 'FLEXIBLE' || $additional['schedule_time'] == 'I_am_flexible'){
			$schedule_time = Cubix_I18n::translateByLng('fr', 'I_am_flexible');
		}
		else{
            $schedule_time = Cubix_I18n::translateByLng('fr', 'at_time')." ".$additional['schedule_time'];
        }

		$text = Cubix_I18n::translateByLng('fr', 'cam_sms', array('membername'=> $membername, 'time'=> $additional['duration'], 'amount'=> $additional['amount'], 'count'=> $token_count,'schedule_time' => $schedule_time, 'count_per_min' => round($token_count / $additional['duration'])));
		
		$conf = Zend_Registry::get('system_config');
		$conf = $conf['sms'];
		$SMS_DeliveryNotificationURL = $conf['DeliveryNotificationURL'];
		$SMS_NonDeliveryNotificationURL = $conf['NonDeliveryNotificationURL'];
		$SMS_BufferedNotificationURL = $conf['BufferedNotificationURL'];
		
		// Get from number
		$originator = $phone_from = Cubix_Application::getPhoneNumber(Cubix_Application::getId());
		
		$sms_id = Cubix_Api::getInstance()->call('outbox', array($escort['escort_id'], $phone, $originator, $text, Cubix_Application::getId(), null, 1));
		$sms = new Cubix_SMS($conf['userkey'], $conf['password']);
		$sms->setBufferedNotificationURL($SMS_BufferedNotificationURL);
		$sms->setDeliveryNotificationURL($SMS_DeliveryNotificationURL);
		$sms->setNonDeliveryNotificationURL($SMS_NonDeliveryNotificationURL);
		$sms->setOriginator($originator);
		$sms->setRecipient($phone, $sms_id);
		$sms->setContent($text);
		$sms->sendSMS();
		
		
		$client = new Cubix_Api_XmlRpc_Client();
		$client->call('VideoChatRequests.update', array($request_id, array('sms_text' => $text)) );
		
		//$model_pm = new Cubix_PrivateMessaging('member', 525597); // Skype Video Booking;
		//$model_pm->sendMessage($text, 'escort', $escort['escort_id'], false, false);
		
		//SENDING EMAIL TO CUSTOMER 
				
		/*Cubix_Email::sendTemplate('universal_message', $customer_email , array(
			'subject' => 'a tua videochiamata  con '. $escort['showname'],
			'message' => $text,
        ));*/
	}
}
