<?php

class Model_Escort_List extends Cubix_Model
{
	public static function getActiveFilter($filter, $where_query = array(), $is_upcomingtour = false, $is_tour = false)
	{
		if ( isset($filter['e.showname LIKE ?']) ) {
			unset($filter['e.showname LIKE ?']);
		}
		
		if ( isset($filter['video']) ) {
			unset($filter['video']);
		}
		
		if ( isset($filter['e.is_pornstar = ?']) ) {
			unset($filter['e.is_pornstar = ?']);
		}
		
		if ( isset($filter['e.is_pornstar = 1']) ) {
			unset($filter['e.is_pornstar = 1']);
		}

		if ( isset($filter['review']) ) {
			unset($filter['review']);
		}
		
		if ( isset($filter['is_online_now']) ) {
			unset($filter['is_online_now']);
		}
		
		if ( isset($filter['has_nat_pic']) ) {
			unset($filter['has_nat_pic']);
		}
		
		if ( isset($filter['e.snapchat = ?']) ) {
			unset($filter['e.snapchat = ?']);
		}
		
		if ( $is_tour ) {
			if ( isset($filter['eic.gender = 1']) ) unset($filter['eic.gender = 1']);
		
			if ( ! $is_upcomingtour ) {
				$filter[] = 'eic.is_tour = 1';
			}
			else {
				$filter[] = 'eic.is_upcoming = 1';
			}
		} else {
			// Only escorts with base city (or city tour) will be shown in this list
			if ( ! isset($filter['ct.slug = ?']) && ! isset($filter['r.slug = ?']) ) {
				$filter[] = 'eic.is_base = 1';
			}
			else {
				$filter[] = 'eic.is_upcoming = 0';
			}
		}
		
		$where = self::getWhereClause($filter, true);
		
		$wqa = array();
		if ( count($where_query) ) {
			foreach( $where_query as $wq ) {
				
				if ( strlen($wq['query']) ) {
					$where_query = ' AND ' . $wq['filter_query'];
					
					$where .= $where_query;
					$wqa[] = array('filter_query' => $wq['filter_query'], 'query' => $wq['query'] );
				}
			}
		}
		
		$metadata = self::db()->describeTable('escort_filter_data');
		$column_names = array_keys($metadata);
		unset($column_names[0]);	
		
		$select = array();
		foreach ( $column_names as $column ) {
			$select[] = 'SUM(' . $column . ') AS ' . $column;
		}
		
		$join = " ";
		
		if ( isset($filter['cz.slug = ?']) ) {
			$join = "
				INNER JOIN escorts_in_cityzones eicz ON eicz.escort_id = fd.escort_id
				INNER JOIN cityzones cz ON cz.id = eicz.cityzone_id
			";
		}
		
		$sql = '
			SELECT 
				' . implode(', ', $select) . ' 
			FROM ( SELECT fd.*
			FROM escort_filter_data fd
			INNER JOIN escorts_in_cities eic ON eic.escort_id = fd.escort_id
			INNER JOIN cities ct ON eic.city_id = ct.id
			' . $join . '
			' . (! is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY fd.escort_id ) f
		';
		
		//echo $sql;
		return array('result' => self::db()->fetchRow($sql), 'global_filter' => $filter , 'where_query' => $wqa, 'is_upcoming' => $is_upcomingtour, 'is_tour' => $is_tour ); 
	}
	
	public static function getMainPremiumSpot()
	{
		$lng = Cubix_I18n::getLang();

		if (date('G') < 2)
			$date = 2;
		else
			$date = date('G');

		$where = Cubix_Countries::blacklistCountryWhereCase();

		$sql = '
			SELECT
				/* escorts table */
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id, e.agency_id,
				e.rates, e.incall_type, e.outcall_type, e.is_new, e.verified_status,
				e.photo_hash, e.photo_ext, e.photo_status,
				e.products, e.slogan, e.date_last_modified, e.is_pornstar, e.snapchat,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.is_suspicious,e.comment_count, e.review_count,
				
				ct.title_' . $lng . ' AS city, e.disabled_reviews, e.disabled_comments,

				/* seo_entity_instances table */
				IF (sei.primary_id IS NULL, se.title_' . $lng . ', sei.title_' . $lng . ') AS alt,

				IF (e.available_24_7 = 1 OR (e.is_now_open = 1 AND e.close_time >= "' . $date . '"), 1, 0) AS is_late_night,
				/* misc */
				1 AS is_premium, ' . Cubix_Application::getId() . ' AS application_id, e.hh_is_active, e.is_online, 
				e.about_' . $lng . ' AS about, UNIX_TIMESTAMP(e.date_last_modified) AS date_last_modified,
				IF (ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE), 1, 0) AS is_login
			FROM escorts e
			INNER JOIN escorts_in_cities eic ON eic.escort_id = e.id AND eic.is_base = 1
			INNER JOIN cities ct ON ct.id = eic.city_id
			LEFT JOIN seo_entity_instances sei ON sei.primary_id = e.id
			LEFT JOIN seo_entities se ON (se.id = sei.entity_id) OR (se.application_id = 2 AND se.slug = "escort")
			LEFT JOIN users_last_refresh_time ulrt ON ulrt.user_id = e.user_id
			WHERE
				eic.gender = ' . GENDER_FEMALE . ' AND e.is_main_premium_spot = 1 AND FIND_IN_SET(77, products) = 0 ' . $where . '
			GROUP BY e.id
		';// 77 - dont_display product: dont display in listing

		$cache = Zend_Registry::get('cache');
		$cache_key = 'main_premium_spot_' . Cubix_Application::getId() . '_lang_' . Cubix_I18n::getLang();
		$cache_key .= Cubix_Countries::blacklistCountryCacheKeyCase();

		if ( ! $escorts = $cache->load($cache_key) ) {
			
			$escorts = self::db()->fetchAll($sql);
			
			/* seo heading escorts */
			if ($escorts)
			{
				$es = array();
				foreach ($escorts as $e)
				{
					$es[] = $e->id;
				}
				$es_str = implode(',', $es);

				$query = "SELECT e.id, sei.heading_escort_{$lng} as heading_escort
					FROM escorts e
					INNER JOIN escorts_in_cities eic ON eic.escort_id = e.id AND eic.is_base = 1
					INNER JOIN seo_entity_instances sei ON sei.primary_id = eic.city_id
					INNER JOIN seo_entities se ON se.application_id = " . Cubix_Application::getId() . " AND se.slug = 'city' AND sei.entity_id = se.id
					WHERE e.id IN ({$es_str})
				";
				$result = self::db()->fetchAll($query);

				foreach ($result as $r)
				{				
					foreach ($escorts as $e)
					{
						if ($e->id == $r->id)
						{
							$e->heading_escort = $r->heading_escort;
							break;
						}
					}
				}
			}
			/**/

			$cache->save($escorts, $cache_key, array());
		}

		// <editor-fold defaultstate="collapsed" desc="Make Escort Alt for SEO">
		$tpl_data = array(
			'app_title' => Cubix_Application::getById()->title
		);

		foreach ( $escorts as $i =>  $escort ) {
			$additional = array(
				'showname' => $escort->showname,
				'city' => $escort->city
			);

			foreach ( $tpl_data + $additional as $tpl_var => $tpl_value ) {
				$escorts[$i]->alt = str_replace('%' . $tpl_var . '%', $tpl_value, $escorts[$i]->alt);
			}
		}
		// </editor-fold>

		shuffle($escorts);

		// <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
		$sid = 'sedcard_paging_' . Cubix_Application::getId();
		$sess = new Zend_Session_Namespace($sid);

		$sess->main_premium_spot = array();
		foreach ( $escorts as $escort ) {
			$sess->main_premium_spot[] = $escort->showname;
		}
		// </editor-fold>


		return $escorts;
	}

	public static function getFiltered($filter, $ordering = 'random', $page = 1, $page_size = 50, &$count = 0, $sess_name = false, $where_query = array(), $only_premiums = false, $only_vip = false)
	{
		$lng = Cubix_I18n::getLang();

		if (date('G') < 2)
			$date = 2;
		else
			$date = date('G');

		if ( 'price' == substr($ordering, 0, 5) ) {
			$filter[] = 'e.incall_price IS NOT NULL';
		}

		if ( $only_vip ) {
			$filter[] = 'FIND_IN_SET(17, e.products) > 0';

			if ( isset($filter['ct.slug = ?']) ) {
				$country_id = Model_Statistics::getCountryByCitySlug($filter['ct.slug = ?'][0]);
				$filter[] = 'e.country_id = ' . $country_id;
				unset($filter['ct.slug = ?']);
			}
		}
		/*else
			$filter[] = 'FIND_IN_SET(17, e.products) = 0';*/

		// Only escorts with base city (or city tour) will be shown in this list
		if ( ! isset($filter['ct.slug = ?']) && ! isset($filter['r.slug = ?']) ) {
            if ( $only_premiums ) {
                $filter[] = 'eic.is_premium = 1';
            } else {
                $filter[] = 'eic.is_base = 1';
            }
		}
		else {
			$filter[] = 'eic.is_upcoming = 0';
		}
		
		$has_video = false;
		if (isset($filter['video'])) {
			$has_video = true;
			unset($filter['video']);
		}
		
		$has_review = false;
		if (isset($filter['review'])) {
			$has_review = true;
			unset($filter['review']);
		}
		
		$has_online_now = false;
		if (isset($filter['is_online_now'])) {
			$has_online_now = true;
			unset($filter['is_online_now']);
		}
		
		$has_nat_pic = false;
		if (isset($filter['has_nat_pic'])) {
			$has_nat_pic = true;
			unset($filter['has_nat_pic']);
		}
		
//		$snapchat = false;
//		if (isset($filter['e.snapchat = ?'])) {
//			$snapchat = true;
//			unset($filter['e.snapchat = ?']);
//		}
		
		if ($has_online_now) {
			// this is ONLY for "is online now filter"
			return array();
			$where = ' ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE) AND allow_show_online = 1 AND ';
		}

		$where .= self::getWhereClause($filter, true);
		$order = self::_mapSorting($ordering);

		$page = (int) $page;
		if ( $page < 1 ) $page = 1;
		
		
		
		/*-->GOTD LOGIC*/
		$gotd_escort = false;
		$exclude = ' ';
			
		$in_city = false;
		if ( isset($filter['ct.slug = ?']) ) {
			$in_city = true;
			$city_slug = reset($filter['ct.slug = ?']);
		} elseif ( isset($filter['cz.slug = ?']) ) {
			$cityzone_slug = reset($filter['cz.slug = ?']);
			$m_city = new Model_Cities();
			$city = $m_city->getCityByCityZone($cityzone_slug);
			
			$city_slug = $city->slug;
			$in_city = true;
		}

		$gender = GENDER_FEMALE;
		if ( isset($filter['eic.gender = 3']) ) {
			$gender = GENDER_TRANS;
		}
		else if ( isset($filter['eic.gender = 2']) ) {
			$gender = GENDER_MALE;
		}
		
		/*if ( $only_premiums ) 
		{
			$g_where = '  ';
			if ( $in_city ) {
				$g_where = ' AND ct.slug = ? ';
			}
			$bind = array($city_slug);
			$sqlGOTDinCity = '
				SELECT
					e.id, e.showname, e.age,
					e.hit_count,
					ct.title_' . $lng . ' AS city,
					e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,

					epg.hash AS photo_hash_g,
					epg.ext AS photo_ext_g,
					epg.status AS photo_status_g,

					1 AS is_gotd, e.gotd_city_id,
					e.slogan, e.about_' . $lng . ' AS about
				FROM escorts e
				INNER JOIN cities ct ON ct.id = e.gotd_city_id
				LEFT JOIN escort_photos epg ON epg.escort_id = e.id AND epg.is_main = 1
				WHERE FIND_IN_SET(16, e.products) > 0 AND FIND_IN_SET(17, e.products) = 0 ' . $g_where . ' AND (e.gender = ' . $gender . ' OR e.gender = ' . GENDER_TRANS . ')
			';

			$gotd_escorts = self::db()->fetchAll($sqlGOTDinCity, $bind);
			
			if ( $gotd_escorts ) {
				shuffle($gotd_escorts);
			}

			if ( $gotd_escorts ) {
				$g_ids = array();
				foreach( $gotd_escorts as $k => $gotd_escort ) {
					$photo_hash_g = $gotd_escort->photo_hash_g;
					$photo_ext_g = $gotd_escort->photo_ext_g;
					$photo_status_g = $gotd_escort->photo_status_g;

					if ( $photo_hash_g ) {
						$gotd_escorts[$k]->photo_hash = $photo_hash_g;
						$gotd_escorts[$k]->photo_ext = $photo_ext_g;
						$gotd_escorts[$k]->photo_status = $photo_status_g;
					}
					
					$g_ids[] = $gotd_escort->id;
				}

				$exclude = ' AND e.id NOT IN (' . implode(',', $g_ids) . ') ';

			}
		}*/
		/*<--GOTD LOGIC*/
		
		$start = ($page_size * ($page - 1));
		
		if ( $gotd_escorts && $page == 1 ) {
			$page_size -= count($gotd_escorts);
		} elseif ( $gotd_escorts && $page > 1 ) {
			$start -= count($gotd_escorts);
			unset($gotd_escorts);
		}
		
		$limit = $start . ', ' . $page_size;
		
		$j = "";
		$f = "";
		if ( isset($filter['f.user_id = ?']) ) {
			$j = " INNER JOIN favorites f ON f.escort_id = e.id ";
			$f = " , f.user_id AS fav_user_id ";
		}
		
		$m_e = new Model_EscortsV2();
		$user_ids = $m_e->getChatNodeOnlineUsersIds();
		
		if(count($user_ids) > 0){
			$user_ids_str = implode(',', $user_ids);
			$f .= ", IF ( ( ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE ) OR e.user_id IN (" . $user_ids_str . ") ), 1, 0) AS is_login";
		}else{
			$f .= ", IF ( ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE ), 1, 0) AS is_login";
		}
		
		if ($has_video) {
			$j .= " INNER JOIN video v ON v.escort_id = e.id ";
		}
		
		if ($has_review) {
			$j .= " INNER JOIN reviews rv ON rv.escort_id = e.id ";
		}
				
		if ($has_nat_pic) {
			$j .= " INNER JOIN natural_pics np ON np.escort_id = e.id ";
		}
		
		if ( count($where_query) ) {
			foreach( $where_query as $wq ) {
				if ( strlen($wq['query']) ) {
					$where_query = ' AND ' . $wq['query'];
					$where .= $where_query;
				}
			}
		}
		
		/**/
		if (!$where)
			$where .= ' 1 ';
		
		$where .= ' AND FIND_IN_SET(77, products) = 0 '; // 77 - dont_display product: dont display in listing
		/**/
		
		/*if ($has_online_now) {
			$where .= ' AND ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE) AND allow_show_online = 1 ';
		}*/

		is_null($where) ? $where = ' 1 ' . Cubix_Countries::blacklistCountryWhereCase() : $where .= Cubix_Countries::blacklistCountryWhereCase();

		$order = str_replace('eic.is_premium', 'IF(eic.is_tour = 1, 0, eic.is_premium)', $order);
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				e.id, e.showname, e.contact_phone_parsed, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id, e.agency_id,
				UNIX_TIMESTAMP(e.date_activated) AS date_activated,
				ct.title_' . $lng . ' AS city, e.rates, e.incall_type, e.outcall_type, e.is_new,
				e.verified_status, e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.comment_count, e.review_count,

				IF (sei.primary_id IS NULL, se.title_' . $lng . ', sei.title_' . $lng . ') AS alt,
				IF (e.available_24_7 = 1 OR (e.is_now_open = 1 AND e.close_time >= "' . $date . '"), 1, 0) AS is_late_night,
				e.hit_count, e.slogan, e.incall_price,
				e.date_last_modified,
				/*IF(eic.is_tour = 1 AND eic.is_premium = 1, 0, eic.is_premium) AS is_premium,*/ 
				IF(eic.is_tour = 1, 0, eic.is_premium) AS is_premium,
				e.hh_is_active, e.is_suspicious, e.is_online ' . $f . ', e.about_' . $lng . ' AS about, UNIX_TIMESTAMP(e.date_last_modified) AS date_last_modified, ct.id AS city_id,
				
				e.is_main_premium_spot, e.is_pornstar, e.snapchat, e.disabled_reviews, e.disabled_comments,
				IF(FIND_IN_SET(17, e.products) > 0, 1, 0) AS is_vip
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			LEFT JOIN regions r ON r.id = eic.region_id
			' . $j . '
			LEFT JOIN escort_cityzones ecz ON ecz.escort_id = eic.escort_id
			LEFT JOIN cityzones cz ON cz.id = ecz.city_zone_id
			LEFT JOIN seo_entity_instances sei ON sei.primary_id = eic.escort_id
			LEFT JOIN seo_entities se ON (se.id = sei.entity_id) OR (se.application_id = ' . Cubix_Application::getId() . ' AND se.slug = "escort")
			LEFT JOIN escort_services es ON es.escort_id = e.id
			LEFT JOIN users_last_refresh_time ulrt ON ulrt.user_id = e.user_id
			' . (! is_null($where) ? 'WHERE ' . $where . $exclude : '') . '
			GROUP BY eic.escort_id
			ORDER BY ' . $order . '
			LIMIT ' . $limit . '
		'; 
		/*if ($_GET['is_test']) {
			echo $sql;die;
		}*/
		
		$escorts = self::db()->fetchAll($sql);
		
		if ( $gotd_escorts ) {
			$escorts = array_merge($gotd_escorts, $escorts);
		}
		
		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');

		// <editor-fold defaultstate="collapsed" desc="Make Escort Alt for SEO">
		$tpl_data = array(
			'app_title' => Cubix_Application::getById()->title
		);

		foreach ( $escorts as $i =>  $escort ) {
			$additional = array(
				'showname' => $escort->showname,
				'city' => $escort->city
			);

			foreach ( $tpl_data + $additional as $tpl_var => $tpl_value ) {
				$escorts[$i]->alt = str_replace('%' . $tpl_var . '%', $tpl_value, $escorts[$i]->alt);
			}
		}
		// </editor-fold>

		if ( $sess_name ) {
			// <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
			$sid = 'sedcard_paging_' . Cubix_Application::getId();
			$sess = new Zend_Session_Namespace($sid);

			$sess->{$sess_name} = array();
			foreach ( $escorts as $escort ) {
				$sess->{$sess_name}[] = $escort->id;
			}
			
			$sess->{$sess_name . '_criterias'} = array(
				'filter'	=> $filter,
				'sort'	=> $ordering,
				'page'		=> $page
			);
			// </editor-fold>
		}

		return $escorts;
	}

	public static function getTours($is_upcoming, $filter, $ordering = 'random', $page = 1, $page_size = 50, &$count = 0, $where_query = array())
	{
		$lng = Cubix_I18n::getLang();

		if ( 'price' == substr($ordering, 0, 5) ) {
			$filter[] = 'e.incall_price IS NOT NULL';
		}

		if ( isset($filter['eic.gender = 1']) ) unset($filter['eic.gender = 1']);
		
		if ( ! $is_upcoming ) {
			$filter[] = 'eic.is_tour = 1';
		}
		else {
			$filter[] = 'eic.is_upcoming = 1';
		}

		$m_e = new Model_EscortsV2();
		$user_ids = $m_e->getChatNodeOnlineUsersIds();
		
		if(count($user_ids) > 0){
			$user_ids_str = implode(',', $user_ids);
			$is_login = ", IF ( ( ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE ) OR e.user_id IN (" . $user_ids_str . ") ), 1, 0) AS is_login";
		}else{
			$is_login = ", IF ( ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE ), 1, 0) AS is_login";
		}
		
		$where = self::getWhereClause($filter, true);
		$order = self::_mapSorting($ordering);

		$page = (int) $page;
		if ( $page < 1 ) $page = 1;
		$limit = ($page_size * ($page - 1)) . ', ' . $page_size;
		
		if ( count($where_query) ) {
			foreach( $where_query as $wq ) {
				if ( strlen($wq['query']) ) {
					$where_query = ' AND ' . $wq['query'];
					$where .= $where_query;
				}
			}
		}
		
		/**/
		if (!$where)
			$where .= ' 1 ';
		
		$where .= ' AND FIND_IN_SET(77, products) = 0 '; // 77 - dont_display product: dont display in listing
		/**/

		$where .= Cubix_Countries::blacklistCountryWhereCase();

		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id, e.agency_id,
				ct.title_' . $lng . ' AS city, e.rates, e.incall_type, e.outcall_type, e.is_new,
				e.verified_status, e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,
				IF (sei.primary_id IS NULL, IF(se1.id IS NOT NULL, se1.title_' . $lng . ', se2.title_' . $lng . '), sei.title_' . $lng . ') AS alt,
				e.hit_count, e.slogan, e.incall_price, e.comment_count, e.review_count,
				e.date_last_modified, e.is_pornstar, e.snapchat, e.disabled_reviews, e.disabled_comments ' . $is_login . ',
				eic.is_premium, e.is_online, e.about_' . $lng . ' AS about, UNIX_TIMESTAMP(e.date_last_modified) AS date_last_modified
				' .
				(! $is_upcoming ? ', e.tour_date_from, e.tour_date_to' : // if
				($is_upcoming ? ', ut.tour_date_from, ut.tour_date_to' : '')) // else
				. '
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			LEFT JOIN seo_entity_instances sei ON sei.primary_id = eic.escort_id
			LEFT JOIN seo_entities se1 ON se1.id = sei.entity_id
			LEFT JOIN seo_entities se2 ON se2.slug = "escort" AND se2.application_id = 2
			LEFT JOIN escort_services es ON es.escort_id = e.id
			LEFT JOIN users_last_refresh_time ulrt ON ulrt.user_id = e.user_id
			' . ($is_upcoming ? 'INNER JOIN upcoming_tours ut ON ut.id = eic.escort_id AND ut.tour_city_id = eic.city_id' : '') . '
			' . (! is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.escort_id
			ORDER BY ' . $order . '
			LIMIT ' . $limit . '
		';
		
		$escorts = self::db()->fetchAll($sql);
		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');

		// <editor-fold defaultstate="collapsed" desc="Make Escort Alt for SEO">
		$tpl_data = array(
			'app_title' => Cubix_Application::getById()->title
		);

		foreach ( $escorts as $i =>  $escort ) {
			$additional = array(
				'showname' => $escort->showname,
				'city' => $escort->city
			);

			foreach ( $tpl_data + $additional as $tpl_var => $tpl_value ) {
				$escorts[$i]->alt = str_replace('%' . $tpl_var . '%', $tpl_value, $escorts[$i]->alt);
			}
		}
		// </editor-fold>
		
		// <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
		$sid = 'sedcard_paging_' . Cubix_Application::getId();
		$sess = new Zend_Session_Namespace($sid);
		
		if ( ! $is_upcoming ) {
			$filter_name = 'tours_list';
			$sess->tours_list = array();
			foreach ( $escorts as $escort ) {
				$sess->tours_list[] = $escort->id;
			}
		}
		else {
			$filter_name = 'up_tours_list';
			$sess->up_tours_list = array();
			foreach ( $escorts as $escort ) {
				$sess->up_tours_list[] = $escort->id;
			}
		}
		
		$sess->{$filter_name . '_criterias'} = array(
			'is_upcoming'	=> $is_upcoming,
			'filter'		=> $filter,
			'sort'		=> $ordering,
			'page'			=> $page
		);
		// </editor-fold>

		return $escorts;
	}

	public static function getVIPEscorts($filter, $ordering = 'random', $page = 1, $page_size = 10, &$count = 0, $sess_name = false, $where_query = array(), $only_premiums = false, $only_vip = false, $is_tour = false)
	{
		$lng = Cubix_I18n::getLang();

		if (date('G') < 2)
			$date = 2;
		else
			$date = date('G');

		if ( 'price' == substr($ordering, 0, 5) ) {
			$filter[] = 'e.incall_price IS NOT NULL';
		}
        $in_city = false;
		if ( $only_vip ) {
			$filter[] = 'FIND_IN_SET(17, e.products) > 0';
			if (isset($filter['ct.slug = ?'])){
			    $in_city = true;
                $city_slug = reset($filter['ct.slug = ?']);
            }
			if ( isset($filter['ct.slug = ?']) && ! $is_tour ) {
				$country_id = Model_Statistics::getCountryByCitySlug($filter['ct.slug = ?'][0]);
				$filter[] = 'ex.country_id = ' . $country_id;
				unset($filter['ct.slug = ?']);
			}
		}

		// Only escorts with base city (or city tour) will be shown in this list
		if ( ! isset($filter['ct.slug = ?']) && ! isset($filter['r.slug = ?']) ) {
			if ( $only_premiums ) {
				$filter[] = 'eic.is_premium = 1';
			} else {
				$filter[] = 'eic.is_base = 1';
			}
		}
		else {
			$filter[] = 'eic.is_upcoming = 0';
		}
		
		$has_video = false;
		if (isset($filter['video'])) {
			$has_video = true;
			unset($filter['video']);
		}
		
		$has_review = false;
		if (isset($filter['review'])) {
			$has_review = true;
			unset($filter['review']);
		}
		
		$has_online_now = false;
		if (isset($filter['is_online_now'])) {
			$has_online_now = true;
			unset($filter['is_online_now']);
		}
		
		$has_nat_pic = false;
		if (isset($filter['has_nat_pic'])) {
			$has_nat_pic = true;
			unset($filter['has_nat_pic']);
		}
		
//		$snapchat = false;
//		if (isset($filter['snapchat'])) {
//			$snapchat = true;
//			unset($filter['snapchat']);
//		}
		
		if ($has_online_now) {
			$ids = $m_e->getChatNodeOnlineUsersIds();

			if (count($ids) > 0)
			{
				$filter[] = array(
					'(e.user_id IN (' . implode(',', $ids) . ') OR ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE))'
				);
			}
			else{
				$filter[] = array(
					'ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE)'
				);
			}
		}

		$where = self::getWhereClause($filter, true);
		$order = self::_mapSorting($ordering);

		$page = (int) $page;
		if ( $page < 1 ) $page = 1;



		/*-->GOTD LOGIC*/
		$gotd_escort = false;
		$exclude = ' ';

//		$in_city = false;
		if ( isset($filter['ct.slug = ?']) ) {
			$in_city = true;
			$city_slug = reset($filter['ct.slug = ?']);
		} elseif ( isset($filter['cz.slug = ?']) ) {
			$cityzone_slug = reset($filter['cz.slug = ?']);
			$m_city = new Model_Cities();
			$city = $m_city->getCityByCityZone($cityzone_slug);

			$city_slug = $city->slug;
			$in_city = true;
		}

		$gender = GENDER_FEMALE;
		if ( isset($filter['eic.gender = 3']) ) {
			$gender = GENDER_TRANS;
		}
		else if ( isset($filter['eic.gender = 2']) ) {
			$gender = GENDER_MALE;
		}

		if ( $only_vip )
		{
			$g_where = '  ';
			if ( $in_city ) {
				$g_where = ' AND ct.slug = ? ';
			}
			$bind = array($city_slug);
			$sqlGOTDinCity = '
				SELECT
					e.id, e.showname, e.age,
					e.hit_count,
					ct.title_' . $lng . ' AS city,
					e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,

					epg.hash AS photo_hash_g,
					epg.ext AS photo_ext_g,
					epg.status AS photo_status_g,

					1 AS is_gotd, e.gotd_city_id,
					e.slogan, e.about_' . $lng . ' AS about
				FROM escorts e
				INNER JOIN cities ct ON ct.id = e.gotd_city_id
				LEFT JOIN escort_photos epg ON epg.escort_id = e.id AND epg.is_main = 1
				WHERE FIND_IN_SET(16, e.products) > 0 ' . $g_where . ' AND (e.gender = ' . $gender . ' OR e.gender = ' . GENDER_TRANS . ')
			';

			
			$gotd_escorts = self::db()->fetchAll($sqlGOTDinCity, $bind);

			if ( $gotd_escorts ) {
				shuffle($gotd_escorts);
			}

			if ( $gotd_escorts ) {
				$g_ids = array();
				foreach( $gotd_escorts as $k => $gotd_escort ) {
					$photo_hash_g = $gotd_escort->photo_hash_g;
					$photo_ext_g = $gotd_escort->photo_ext_g;
					$photo_status_g = $gotd_escort->photo_status_g;

					if ( $photo_hash_g ) {
						$gotd_escorts[$k]->photo_hash = $photo_hash_g;
						$gotd_escorts[$k]->photo_ext = $photo_ext_g;
						$gotd_escorts[$k]->photo_status = $photo_status_g;
					}

					$g_ids[] = $gotd_escort->id;
				}

				$exclude = ' AND e.id NOT IN (' . implode(',', $g_ids) . ') ';

			}
			
		}
		/*<--GOTD LOGIC*/

		$start = ($page_size * ($page - 1));

		if ( $gotd_escorts && $page == 1 ) {
			$page_size -= count($gotd_escorts);
		} elseif ( $gotd_escorts && $page > 1 ) {
			$start -= count($gotd_escorts);
			unset($gotd_escorts);
		}

		$limit = $start . ', ' . $page_size;

		$j = "";
		$f = "";
		if ( isset($filter['f.user_id = ?']) ) {
			$j = " INNER JOIN favorites f ON f.escort_id = e.id ";
			$f = " , f.user_id AS fav_user_id ";
		}
		
		$m_e = new Model_EscortsV2();
		$user_ids = $m_e->getChatNodeOnlineUsersIds();
		
		if(count($user_ids) > 0){
			$user_ids_str = implode(',', $user_ids);
			$f .= ", IF ( ( ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE ) OR e.user_id IN (" . $user_ids_str . ") ), 1, 0) AS is_login";
		}else{
			$f .= ", IF ( ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE ), 1, 0) AS is_login";
		}
		
		if ($has_video) {
			$j .= " INNER JOIN video v ON v.escort_id = e.id ";
		}
		
		if ($has_review) {
			$j .= " INNER JOIN reviews rv ON rv.escort_id = e.id ";
		}
		
		if ($has_nat_pic) {
			$j .= " INNER JOIN natural_pics np ON np.escort_id = e.id ";
		}
		
		if ( count($where_query) ) {
			foreach( $where_query as $wq ) {
				if ( strlen($wq['query']) ) {
					$where_query = ' AND ' . $wq['query'];
					$where .= $where_query;
				}
			}
		}

		/**/
		if (!$where)
			$where .= ' 1 ';

		$where .= ' AND FIND_IN_SET(77, products) = 0 '; // 77 - dont_display product: dont display in listing
		/**/

		$where .= Cubix_Countries::blacklistCountryWhereCase();
		
		/*if ($has_online_now) {
			$where .= ' AND ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE) AND allow_show_online = 1 ';
		}*/

		$order = str_replace('eic.is_premium', 'IF(eic.is_tour = 1, 0, eic.is_premium)', $order);
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id, e.agency_id,
				UNIX_TIMESTAMP(e.date_activated) AS date_activated,
				ct.title_' . $lng . ' AS city, e.rates, e.incall_type, e.outcall_type, e.is_new,
				e.verified_status, e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.comment_count, e.review_count,

				IF (sei.primary_id IS NULL, se.title_' . $lng . ', sei.title_' . $lng . ') AS alt,
				IF (e.available_24_7 = 1 OR (e.is_now_open = 1 AND e.close_time >= "' . $date . '"), 1, 0) AS is_late_night,
				e.hit_count, e.slogan, e.incall_price,
				e.date_last_modified,
				/*IF(eic.is_tour = 1 AND eic.is_premium = 1, 0, eic.is_premium) AS is_premium,*/
				IF(eic.is_tour = 1, 0, eic.is_premium) AS is_premium,
				e.hh_is_active, e.is_suspicious, e.is_online ' . $f . ', e.about_' . $lng . ' AS about, UNIX_TIMESTAMP(e.date_last_modified) AS date_last_modified, ct.id AS city_id,
				
				e.is_main_premium_spot, e.is_pornstar, e.snapchat, e.disabled_reviews, e.disabled_comments,
				IF(FIND_IN_SET(17, e.products) > 0, 1, 0) AS is_vip
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			LEFT JOIN regions r ON r.id = eic.region_id
			' . $j . '
			LEFT JOIN escort_cityzones ecz ON ecz.escort_id = eic.escort_id
			LEFT JOIN cityzones cz ON cz.id = ecz.city_zone_id
			LEFT JOIN seo_entity_instances sei ON sei.primary_id = eic.escort_id
			LEFT JOIN seo_entities se ON (se.id = sei.entity_id) OR (se.application_id = ' . Cubix_Application::getId() . ' AND se.slug = "escort")
			LEFT JOIN escort_services es ON es.escort_id = e.id
			INNER JOIN extra_vip_countries ex ON ex.escort_id = eic.escort_id 
			LEFT JOIN users_last_refresh_time ulrt ON ulrt.user_id = e.user_id
			' . (! is_null($where) ? 'WHERE ' . $where . $exclude : '') . '
			GROUP BY eic.escort_id
			ORDER BY ' . $order . '
			/*LIMIT ' . $limit . '*/
		';

		if ($_GET['dd'] && $only_vip) {
			echo $sql;die;
		}

		$escorts = self::db()->fetchAll($sql);

		if ( $gotd_escorts ) {
			$escorts = array_merge($gotd_escorts, $escorts);
		}

		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');

		// <editor-fold defaultstate="collapsed" desc="Make Escort Alt for SEO">
		$tpl_data = array(
			'app_title' => Cubix_Application::getById()->title
		);

		foreach ( $escorts as $i =>  $escort ) {
			$additional = array(
				'showname' => $escort->showname,
				'city' => $escort->city
			);

			foreach ( $tpl_data + $additional as $tpl_var => $tpl_value ) {
				$escorts[$i]->alt = str_replace('%' . $tpl_var . '%', $tpl_value, $escorts[$i]->alt);
			}
		}
		// </editor-fold>

		if ( $sess_name ) {
			// <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
			$sid = 'sedcard_paging_' . Cubix_Application::getId();
			$sess = new Zend_Session_Namespace($sid);

			$sess->{$sess_name} = array();
			foreach ( $escorts as $escort ) {
				$sess->{$sess_name}[] = $escort->id;
			}

			$sess->{$sess_name . '_criterias'} = array(
				'filter'	=> $filter,
				'sort'	=> $ordering,
				'page'		=> $page
			);
			// </editor-fold>
		}

		return $escorts;
	}

	private static function _mapSorting($param)
	{
		$map = array(
			'price-asc' => 'e.incall_price ASC',
			'price-desc' => 'e.incall_price DESC',
			'random' => 'eic.is_premium DESC, eic.ordering DESC',
			'alpha' => 'e.showname ASC',
			'most-viewed' => 'e.hit_count DESC',
			'newest' => 'e.is_new DESC, COALESCE(e.date_activated, e.date_registered) DESC',
			'last-modified' => 'e.date_last_modified DESC',
			'city-asc' => 'ct.slug ASC',
			'last-login' => 'eic.is_premium DESC, e.last_login_date DESC'
		);
		
		$order = 'e.ordering DESC';
		if ( isset($map[$param]) ) {
			$order = $map[$param];
		}
		elseif ( false !== strpos($param, 'FIELD') ) {
			$order = $param;
		}

		return $order;
	}

	public static function getPrevNext($showname)
	{
		return array(null, null);
		// Retrieve criterias from session
		$sid = 'v2_sedcard_paging_' . Cubix_Application::getId();
		$sess = new Zend_Session_Namespace($sid);

		$page = $sess->page;
		$page_size = $sess->page_size;
		$pages_count = $sess->pages_count;

		list($filter, $ordering) = $sess->criteria;
		foreach ( $sess->escorts as $i => $name ) {
			if ( $name == $showname ) {
				$prev_i = $i - 1;
				$next_i = $i + 1;

				break;
			}
		}

		echo "Prev_i: $prev_i, Next_i: $next_i\n";
		print_r($sess->escorts);

		if ( ! isset($prev_i) && ! isset($next_i) ) {
			return array(null, null);
		}

		// This is first escort, we need to fetch data from prev page
		if ( isset($prev_i) && (! isset($sess->escorts[$prev_i]) || $prev_i < 0) && $page > 1 ) {
			$sess->escorts = array_slice($sess->escorts, 0, $page_size * 2);

			$escorts = call_user_func_array(
				array(__CLASS__, $sess->callback),
				array($filter, $ordering, $page - 1, $page_size)
			);
			foreach ( $escorts as $i => $escort ) {
				$escorts[$i] = $escort->showname;
			}

			$sess->escorts = array_merge($escorts, $sess->escorts);
			
			$prev_i = $page_size - 1;
		}

		
		echo "Escorts after 'first' logic\n";
		echo "Prev_i: $prev_i, Next_i: $next_i\n";
		print_r($sess->escorts);
		
		// If this last escort on page, we need to fetch data from next page
		if ( isset($next_i) &&  (! isset($sess->escorts[$next_i]) || $next_i > count($sess->escorts) - 1 ) && $page < $pages_count ) {
			if ( count($sess->escorts) >= $page_size * 3 ) {
				$sess->escorts = array_slice($sess->escorts, $page_size, $page_size * 2);
				$next_i -= $page_size;
			}
			
			$escorts = call_user_func_array(
				array(__CLASS__, $sess->callback),
				array($filter, $ordering, $page + 1, $page_size)
			);
			foreach ( $escorts as $i => $escort ) {
				$escorts[$i] = $escort->showname;
			}

			$sess->escorts = array_merge($sess->escorts, $escorts);
		}

		echo "Escorts after 'last' logic\n";
		echo "Prev_i: $prev_i, Next_i: $next_i\n";
		print_r($sess->escorts);
		
		return array(isset($prev_i) ? $sess->escorts[$prev_i] : null, isset($next_i) ? $sess->escorts[$next_i] : null);
	}

	public function getEmailCollectingEscorts(){
		$client = new Cubix_Api_XmlRpc_Client();		
		$collectingEscorts = $client->call('Escorts.getEmailCollectingEscorts');

		if($collectingEscorts){
			$photoModel = new Model_Escort_Photos();

			$escorts = array();

			$lng = Cubix_I18n::getLang();


			$sql = '
				SELECT
					e.id, e.showname, e.age, ct.title_' . $lng . ' AS city
				FROM escorts e
				LEFT JOIN escorts_in_cities eic ON eic.escort_id = e.id AND eic.is_base = 1
				LEFT JOIN cities ct ON ct.id = eic.city_id
				WHERE e.id = ?';

			foreach ($collectingEscorts as $p => $collectingEscort) {
				$escorts[$p] = self::db()->fetchRow($sql,$collectingEscort['escort_id']);
				$escorts[$p]->photo = $photoModel->get($collectingEscort['photo_id']);
			}
		}
		
		return $escorts;
	}

	public static function checkEscortOnline($escort_id){
		$sql = 'SELECT * from users_last_refresh_time where escort_id='.$escort_id.' AND refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE)';
		$result = self::db()->fetchAll($sql);
		if(count($result) > 0){
			return true;
		} else {
			return false;
		}
	}
}