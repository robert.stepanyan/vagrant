<?php

class Model_Escort_Photos extends Cubix_Model
{
	const STATUS_VERIFIED = 1;
	const STATUS_NOT_VERIFIED = 2;
	const STATUS_NORMAL = 3;
	
	const PHOTO_ROTATE_ALL = 1;
	const PHOTO_ROTATE_SELECTED = 2;
	const PHOTO_ROTATE_SELECTED_ONE = 3;
		
	const MAX_PHOTOS_COUNT = 12;
	
	protected $_table = 'escort_photos';
	protected $_itemClass = 'Model_Escort_PhotoItem';

	public function get($id)
	{
		$photo = self::getAdapter()->fetchRow('
			SELECT * FROM escort_photos WHERE id = ?
		', $id);
		
		if ( ! $photo ) return null;
		
		$photo->application_id = Cubix_Application::getId();
		
		return new Model_Escort_PhotoItem($photo);
	}
	
	public function remove($ids)
	{
		$user = Model_Users::getCurrent();
		
		if ( ! is_array($ids) ) $ids = array($ids);
		
		foreach ( $ids as $i => $id ) {
			$ids[$i] = intval($id);
			if ( ! $ids[$i] ) unset($ids[$i]);
		}
		
		$client = new Cubix_Api_XmlRpc_Client();
//		try {
			$result = $client->call('Escorts.removePhotos', array($ids, $user->sign_hash));
//		}
//		catch ( Exception $e ) {
//			echo $e; ob_flush(); die;
//		}
		
		if ( is_array($result) && isset($result['error']) ) {
			return array('error' => 'An error occured when removing photos');
		}
		
		if ( is_int($result) ) {
			$main_photo_id = $result;
			self::getAdapter()->update('escort_photos', array('is_main' => 1), self::quote('id = ?', $main_photo_id));
		}
		
		try {
			$this->_db->delete('escort_photos', 'id IN (' . implode(', ', $ids) . ')');
		}
		catch ( Exception $e ) {
			return array('error' => 'An error occured when removing photos');
		}
		
		return true;
	}
	
	public function save(Model_Escort_PhotoItem $item)
	{
		$user = Model_Users::getCurrent();
		
		$escort_status = $this->_db->fetchOne('SELECT verified_status FROM escorts WHERE id = ?', $item->escort_id);
		
		if ( Model_Escorts::VERIFIED_STATUS_VERIFIED == $escort_status || Model_Escorts::VERIFIED_STATUS_VERIFIED_RESET == $escort_status ) {
			$item->status = self::STATUS_NOT_VERIFIED;
			
			if ( Model_Escorts::VERIFIED_STATUS_VERIFIED_RESET != $escort_status ) {
				$this->_db->update('escorts', array('verified_status' => Model_Escorts::VERIFIED_STATUS_VERIFIED_RESET), $this->_db->quoteInto('id = ?', $item->escort_id));
			}
		}
		
		$max = intval(self::getAdapter()->fetchOne('
			SELECT MAX(ordering) FROM escort_photos WHERE escort_id = ?' . 
			( $item->type == ESCORT_PHOTO_TYPE_PRIVATE ? ' AND type = ' . ESCORT_PHOTO_TYPE_PRIVATE : ' AND type <> ' . ESCORT_PHOTO_TYPE_PRIVATE ) . '
		', $item->getEscortId()));
		
		if ( ! $max ) $max = 0;
		$max++;
		
		$item->ordering = $max;
		
		$client = new Cubix_Api_XmlRpc_Client();
		/*if ( 0 == self::getAdapter()->fetchOne('SELECT COUNT(id) FROM escort_photos WHERE escort_id = ? AND is_main = 1 AND type <> 3', $item->getEscortId()) &&
				$item->type <> ESCORT_PHOTO_TYPE_PRIVATE ) {
			$item->is_main = 1;
		}*/
		if ( 0 == $client->call('Escorts.hasMainPhoto', array($item->getEscortId())) &&
				$item->type <> ESCORT_PHOTO_TYPE_PRIVATE ) {
			$item->is_main = 1;
		}
		
		
		$id = $client->call('Escorts.addPhoto', array($item->getData(), $user->sign_hash));
		$id = intval($id);
		
		$item->id = $id;
		
		try {
			self::getAdapter()->insert('escort_photos', array_merge(array('id' => $item->id), $item->getData()));
		}
		catch ( Exception $e ) {
			
		}
		
		if ( ! $id ) {
			throw new Exception('An error occured when uploading your file');
		}
		
		return $this->get($id);
	}

	public function reorder($photos)
	{
		Cubix_Api::getInstance()->call('reorderEscortPhotos', array($photos));

		foreach ( $photos as $i => $photo_id ) {
			$p = new Model_Escort_PhotoItem(array('id' => $photo_id));
			$p->reorder($i);
		}
	}
	
	public function getEscortPhotoCount($escort_id){
		$count = Cubix_Api::getInstance()->call('getEscortPhotoCount', array($escort_id));
		return $count;
	}
		
	public function getRotatablePicsByEscortId($escort_id)
	{
		$photos = array();
		$results = self::getAdapter()->fetchAll('
			SELECT SQL_CALC_FOUND_ROWS id , is_main FROM escort_photos WHERE is_rotatable = 1 AND status !='.self::STATUS_NOT_VERIFIED.' AND type !='. ESCORT_PHOTO_TYPE_PRIVATE .' AND type !='. ESCORT_PHOTO_TYPE_DISABLED .' AND escort_id = ? ORDER BY id
		', $escort_id);
		
		if ( ! $results ) return null;
		
		$count = parent::getAdapter()->fetchOne('SELECT FOUND_ROWS()');
		
		$photos['results'] = $results;
		$photos['count'] = $count;
		
		return $photos;
	}
	
	public function setMainImageFront($id)
	{
				
		self::getAdapter()->query('UPDATE escorts e INNER JOIN escort_photos ep ON ep.escort_id = e.id SET e.photo_hash = ep.hash, e.photo_ext = ep.ext, e.photo_status = ep.status WHERE ep.id = ?', $id);
		
		$this->getAdapter()->query('
			UPDATE escort_photos ep
			INNER JOIN escort_photos ep1 ON ep.escort_id = ep1.escort_id
			SET ep.is_main = FALSE
			WHERE ep1.id = ?
		', $id);
		
		self::getAdapter()->query('UPDATE escort_photos SET is_main = TRUE WHERE id = ?', $id);
		
		return true;
	}
	
	public function uploadValidation()
	{
		$config = Zend_Registry::get('images_config');
		$headers = self::_read_headers();
		$img_name = $headers["X-File-Name"];
		$img_ext = strtolower(@end(explode('.', $img_name)));
		if ( !strlen($img_ext) ) {
			throw new Exception('Please select a photo to upload');
		}
		elseif (!in_array( $img_ext , $config['allowedExts'])){
			throw new Exception(Cubix_I18n::translate('sys_error_upload_img_allowed_extensions', array('extensions' => implode(', ', $config['allowedExts']))), Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
		}
		
		return $img_ext;
		
	}
	
	public function is_HTML5_upload()
	{
		return empty($_FILES);
	}
		
	public function HTML4_upload($file_prefix = '')
	{
		$container_index = '_tbxFile';
		$response = array();
  
		foreach ($_FILES as $k => $file)
		{
			$response['key']         = (int)substr($k, strpos($k, $container_index) + strlen($container_index));
			$response['name']        = basename($file['name']);	// Basename for security issues
			$response['error']       = $file['error'];
			$response['size']        = $file['size'];
			$response['upload_name'] = $file['name'];
			$response['finish']      = FALSE;

			if ($response['error'] == 0)
			{
				$response['tmp_name'] = $file['tmp_name'];
				$response['finish'] = TRUE;
			}          
		}   
	         
	 	return $response;	
	}
	
	public static function HTML5_upload($file_prefix = '')
	{
	
		// Read headers
		$response = array();
		$headers 	= self::_read_headers();

		$response['id']    	= $headers['X-File-Id'];
		$response['name']  	= basename($headers['X-File-Name']); 	// Basename for security issues
		$response['size']  	= $headers['Content-Length'];
		$response['error'] 	= UPLOAD_ERR_OK; 
		$response['finish'] = FALSE;

		// Is resume?	  
		$flag = (bool)$headers['X-File-Resume'] ? FILE_APPEND : 0;
		$filename = $response['id'].'_'.$response['name'];
		$response['upload_name'] = $filename;
		
		// Write file
		$file = sys_get_temp_dir().DIRECTORY_SEPARATOR.$response['upload_name'];
		
		if (file_put_contents($file, file_get_contents('php://input'), $flag) === FALSE){
			$response['error'] = UPLOAD_ERR_CANT_WRITE;
		}
		else
		{
			$response['file_size'] = filesize($file);
			$response['X-File-Size'] = $headers['X-File-Size'];
			if (filesize($file) == $headers['X-File-Size'])
			{
				$response['tmp_name'] = $file;
				$response['finish'] = TRUE;
			}
		} 

		return $response;
	}
	
	
	public static function upload($file_prefix = '')
	{			
		return self::is_HTML5_upload() ? self::HTML5_upload($file_prefix) : self::HTML4_upload($file_prefix);		
	}	 		 	 	
	
	private function _read_headers()
	{
		// GetAllHeaders doesn't work with PHP-CGI
		if (function_exists('getallheaders')) 
		{
			$headers = getallheaders();
		}
		else 
		{
			$headers = array();
			$headers['Content-Length'] 	= $_SERVER['CONTENT_LENGTH'];
			$headers['X-File-Id'] 			= $_SERVER['HTTP_X_FILE_ID'];
			$headers['X-File-Name'] 		= $_SERVER['HTTP_X_FILE_NAME'];			
			$headers['X-File-Resume'] 	= $_SERVER['HTTP_X_FILE_RESUME'];
			$headers['X-File-Size'] 		= $_SERVER['HTTP_X_FILE_SIZE'];
		}
		
		return $headers;
		
	}
	
	public function showResponse($response)
	{
		// If is HTML5
		if(isset($response['id'])){
			header('Cache-Control: no-cache, must-revalidate');
			header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
			header('Content-type: application/json');
		}
		echo json_encode($response);
	}		
}
