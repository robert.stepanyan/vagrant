<?php

class Model_Mobile_GA extends Cubix_Model_Item
{
	const GA_ACCOUNT = "UA-26272467-1";
	const GA_PIXEL = "/ga.php";

	public static function googleAnalytics()
	{
		return "
			<script type=\"text/javascript\">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-26272467-1']);
			_gaq.push(['_setDomainName', 'beneluxxx.com']);
			_gaq.push(['_trackPageview']);
			(function() { var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); })();
			</script>
		";
	}
	
	public static function googleAnalyticsGetImageUrl()
	{		
		$url = "";
		$url .= self::GA_PIXEL . "?";
		$url .= "utmac=" . self::GA_ACCOUNT;
		$url .= "&utmn=" . rand(0, 0x7fffffff);

		$referer = $_SERVER["HTTP_REFERER"];
		$query = $_SERVER["QUERY_STRING"];
		$path = $_SERVER["REQUEST_URI"];

		if (empty($referer)) {
		$referer = "-";
		}
		$url .= "&utmr=" . urlencode($referer);

		if (!empty($path)) {
		$url .= "&utmp=" . urlencode($path);
		}

		$url .= "&guid=ON";

		return str_replace("&", "&amp;", $url);
	}

}
