<?php

class Model_Plugin_I18n extends Zend_Controller_Plugin_Abstract
{
	public function __construct()
	{
		
	}

	public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request)
	{
		$user = Model_Users::getCurrent();
		
		if ( ! is_null($user) ) {
			Model_Users::setLastRefreshTime($user);
		}

		
		
		//$this->rememberMeSignIn();
	}

	public function rememberMeSignIn()
	{		
		$cookie_name = 'signin_remember_' . Cubix_Application::getId();

		if ( isset($_COOKIE[$cookie_name]) ) {
			$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
			$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
			$key = '_-_SignInRememberMe_-_' . Cubix_Application::getId();

			$crypt_login_data = $_COOKIE[$cookie_name];
			$crypt_login_data = base64_decode($crypt_login_data);
			$login_data = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $crypt_login_data, MCRYPT_MODE_ECB, $iv);

			$login_data = @unserialize($login_data);
			
			try {
				if ( is_array($login_data) ) {

					$model = new Model_Users();

					if ( ! $user = $model->getByUsernamePassword($login_data['username'], $login_data['password']) ) {
						$this->view->errors['username'] = 'Wrong username/password combination';
						return;
					}
					//print_r($user);die;
					if ( STATUS_ACTIVE != $user->status ) {
						$this->view->errors['username'] = 'Your account is not active yet';
						return;
					}

					Zend_Session::regenerateId();
					$user->sign_hash = md5(rand(100000, 999999) * microtime(true));
					Model_Users::setCurrent($user);

					/*set client ID*/
					Model_Reviews::createCookieClientID($user->id);
					/**/


					/*if ( ! Models_Auth::getAuth()->hasIdentity() ) {
						Models_Auth::auth($login_data['username'], $login_data['password'], Estate_Application::ID);
					}*/
				}
			}
			catch (Exception $e) {

			}
		}
	}
	
	public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
		if ( ! isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $request->getActionName() != 'js-init-exact' && $request->getActionName() != 'js-init' ) {
			if ( isset($_COOKIE['ec_count']) ) {
				$count = ($_COOKIE['ec_count'] < 3) ? $_COOKIE['ec_count'] + 1 : 1;				
				setcookie("ec_count", $count, 0, "/", ".beneluxxx.com");
			} else {
				setcookie("ec_count", 1, 0, "/", ".beneluxxx.com");
			}
		}
		
		/*$config = Zend_Registry::get('system_config');

		if ($request->getModuleName() != 'api' )
		{
			if ( $request->getControllerName() != 'private' && $request->getActionName() != 'activate' )
			{
				if ( $config['showSplash'] )
				{
					if ( ( ! isset($_COOKIE['18']) || ! $_COOKIE['18'] ) &&
						($request->getControllerName() != 'index' && $request->getActionName() != 'splash') &&
						($request->getControllerName() != 'error' && $request->getActionName() != 'error') )
					{

						$then = $_SERVER['REQUEST_URI'];
						if ( $_SERVER['REQUEST_URI'] == '' )
							$then = '/';

						header('HTTP/1.1 301 Moved Permanently');
						header('Location: /splash?then=' . urlencode($then));
						die;
					}
				}
				elseif ( ($request->getControllerName() == 'index' && $request->getActionName() == 'splash') )
				{
					header('HTTP/1.1 301 Moved Permanently');
					header('Location: /');
					die;
				}
			}
		}*/

		/*if ( $_SERVER['HTTP_HOST'] != 'www.6annonce.com' && 
				($request->getControllerName() != 'redirect' ||
				($request->getControllerName() != 'escorts' && $request->getActionName() != 'profile')) &&
				($request->getModuleName() != 'api') ) {
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: http://www.6annonce.com' . $_SERVER['REQUEST_URI']);
			exit;
		}*/
		
		//1 - arrow chat
		//2 - node chat
		if ( ! $request->getParam('chatVersion') ) {
			$request->setParam('chatVersion', 2);
		}
    }

	public function routeShutdown($request)
	{
		$db = Zend_Registry::get('db');
		
		$lang_id = $request->lang_id;
//var_dump($lang_id);die;
		$default_lang_id = 'fr';
		
		$g = Cubix_Geoip::getClientLocation();
		
		if (strtolower($g['country_iso']) == 'nl')
			$default_lang_id = 'nl';
		
		$cook_ln = $request->ln;
		
		if (  $cook_ln ) {
			
			if ( $cook_ln != $default_lang_id ) {
				setcookie('ln', $cook_ln, time() + 31536000, '/', '.beneluxxx.com');
				$_COOKIE['ln'] = $cook_ln;
			}
			else {
				setcookie('ln', "", time() - 3600, '/', '.beneluxxx.com');
				$_COOKIE['ln'] = null;
			}
		}
		
		$lngs = array('en', 'fr', 'nl');
		
		if ( isset($_COOKIE['ln']) && $_COOKIE['ln'] ) {
			
			if ( ! in_array($_COOKIE['ln'], $lngs) ) {
				$default_lang_id = 'fr';
				setcookie('ln', "", time() - 3600, '/', '.beneluxxx.com');
				$_COOKIE['ln'] = null;
			}
			else {
				$default_lang_id = $_COOKIE['ln'];
			}
		}
		
		if (preg_match('#(\?|\&)ln\=#', $_SERVER['REQUEST_URI']) && ! isset($_SERVER['HTTP_X_REQUESTED_WITH']) ) {
			$url = preg_replace('#mobile#', '', $_SERVER['REQUEST_URI']);
			//header('HTTP/1.1 301 Moved Permanently');
			header('Location: ' . preg_replace('#(\?|\&)ln\=(.+)#', '', $url));
			die;
		}

		if ( ! $default_lang_id ) {
			$this->_request->setParam('error_msg', 'This Site is temporary off');
			$this->_request->setModuleName('default');
			$this->_request->setControllerName('error');
			$this->_request->setActionName('no-default-lang');
			return;
		}
		
		/*if ( ! is_null($lang_id) && $lang_id == $default_lang_id && ! $request->isPost() ) {
			$uri = trim($_SERVER['REQUEST_URI'], '/');
			$uri = explode('/', $uri);
			array_shift($uri);
			$uri = implode('/', $uri);
			
			header('HTTP/1.1 301 Moved Permamently');
			header('Location: /' . $uri);
			die;
		}*/

		$cache = Zend_Registry::get('cache');
		$cache_key =  $lang_id . '_application_get_true_from_app_langs__';
		if ( ! $is_lng_exist = $cache->load($cache_key) ) {
			$is_lng_exist = $db->fetchOne('SELECT TRUE FROM application_langs WHERE application_id = ? AND lang_id = ?', array(Cubix_Application::getId(), $lang_id));
			$cache->save($is_lng_exist, $cache_key, array());
		}

		if ( is_null($lang_id) || ! $is_lng_exist ) {
			//$lang_id = $db->fetchOne('SELECT lang_id FROM application_langs ORDER BY FIELD(lang_id, "en", "de") LIMIT 1');
			
			$request->setParam('lang_id', $default_lang_id);
			/*header('HTTP/1.1 301 Moved Permamently');
			header('Location: /' . $lang_id);
			die;*/
		}

		Cubix_I18n::init();
		
		require 'Cubix/defines.php';
		Zend_Registry::set('definitions', $DEFINITIONS);
	}
}
