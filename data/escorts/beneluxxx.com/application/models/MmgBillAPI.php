<?php
class Model_MmgBillAPI
{
	 private $host		= 'https://secure.mmgbill.com/';
	 private $uri		= 'process/';
	 private $merchantId = 'M1524';
	 private $secretKey = 'GRBOyQdTHZZn862';
	 private $ts = 'd'; //Stands for dynamic system
	 private $currencyCode = '978';
	 
	 public function __construct()
	 {
	 }
	 
	 public function getHostedPageUrl($amount, $transaction_id, $store_info = false, $postback_url = '')
	 {
		$amount = $amount * 100;
		$c1 = $store_info ? 1 : 0;
		
		//Postback is going to ef backend
		//setting c2 to detect that it's bl request and forward to this url
		$c2 = 'beneluxxx.com';
		
		$hash_string = $this->merchantId . $this->ts . $transaction_id . $this->currencyCode . $amount . $c1 . $c2 . $postback_url . "address" . "email" . $this->secretKey;
		
		$hash = hash("sha256", $hash_string);
		$hash = bin2hex($hash);
		
		$link = $this->host . $this->uri . 
			'?mid=' . $this->merchantId .
			'&ts=' . $this->ts .
			'&cu=' . $this->currencyCode .
			'&ti=' . $transaction_id .
			'&iac=' . $amount .
			'&email=' .
			'&c1=' . $c1 .
			'&c2=' . $c2 .
			(strlen($postback_url) ? '&surl=' . $postback_url : '') .
			'&address=' .
			'&sh=' . $hash;

		return $link;
	}
}

?>
