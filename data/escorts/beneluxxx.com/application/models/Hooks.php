<?php

class Model_Hooks
{
	/**
	 * @var Zend_Db_Adapter_Abstract
	 */
	protected static $_db;
	
	/**
	 * @var Cubix_Api_XmlRpc_Client
	 */
	protected static $_client;
	
	public static function init()
	{
		self::$_client = Cubix_Api_XmlRpc_Client::getInstance();
		self::$_db = Zend_Registry::get('db');
	}

	public static function preUserSignIn($user_id)
	{
		$ip = Cubix_Geoip::getIP();
		
		self::$_client->call('Users.updateLastLoginDate', array($user_id, $ip, $_SERVER['HTTP_USER_AGENT'], session_id()));
	}
	
	public static function preUserSignUp(&$user)
	{
		//var_dump(self::$_client->call('Users.getRandomSalesPerson', array())); die;
		$user->sales_user_id = self::$_client->call('Users.getRandomSalesPerson', array());
	}
	
	public static function postUserSignUp($user)
	{
		$reg_type = '';
		if ( $user->reg_type == 'escort' ) {
			$reg_type = 'signup-independent';
		}
		else if ( $user->reg_type == 'agency' ) {
			$reg_type = 'signup-agency';
		}
		else if ( $user->reg_type == 'member' ) {
			$reg_type = 'signup-freemember';
		}
		else if ( $user->reg_type == 'vip-member' ) {
			$reg_type = 'signup-vipmember';
		}
		
		Cubix_Email::sendTemplate('signup_success', $user->email, array(
			'activation_hash' => $user->activation_hash,
			'email' => $user->email,
			'username' => $user->username,
			'reg_type' => $reg_type
		));
	}
	
	public static function postUserActivate($user)
	{
		Cubix_Email::sendTemplate('activate_success', $user['email'], array(
			'username' => $user['username']
		));
	}
	
	public static function preEscortSignUp(&$escort)
	{
		$escort->country_id = Cubix_Application::getById(Cubix_Application::getId())->country_id;
		
	}
	
	public static function postEscortSignUp($escort)
	{
		self::$_client->call('Escorts.setStatusBit', array($escort->getId(), array(
			Model_Escorts::ESCORT_STATUS_IS_NEW,
			Model_Escorts::ESCORT_STATUS_NO_PROFILE,
			Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS
		)));
	}
	
	public static function preAgencySignUp(&$agency)
	{
		$agency->country_id = Cubix_Application::getById(Cubix_Application::getId())->country_id;
	}
	
	public static function postAgencySignUp(&$agency)
	{
		
	}

	public static function deactivate($escort){
        self::$_client->call('Escorts.deactivate', array($escort->getId()));
    }

    public static function deactivateAgency($agency){
        self::$_client->call('Users.deactivate', array($agency->user_id));
    }

    public static function addComment($field)
    {
        self::$_client->call('Users.addComment', array($field));
    }
}
