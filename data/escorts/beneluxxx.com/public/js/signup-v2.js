Cubix.Lang.signup = {
	username_invalid: '',
	username_invalid_characters: '',
	username_exists: '',
	password_invalid: '',
	password2_invalid: '',
	email_invalid: '',
	email_exists: '',
	terms_required: '',
	form_errors: '',
	domain_blacklisted: ''
};

Cubix.Validator = {};

Cubix.Validator.Lang = '';

Cubix.Validator.SignUp = function () {
	if ( ! $('username') ) return;

	this.getErrorElement = function (input) {
		var el = input.getNext('.error').getElement('span');

		if ( ! el ) {
			el = new Element('span').inject(input.getNext('.error').getElement('div.tip-arrow'), 'after');
		}

		return el;
	};

	var self = this;

	// Check username
	$('username').addEvent('blur', function () {
		var lang = Cubix.Lang.signup;

		this.addClass('txt');
		this.removeClass('txt-error');

		this.removeClass('invalid');
		this.removeClass('valid');
		self.getErrorElement(this).getParent('div').addClass('none');

		if ( this.get('value').length < 6 ) {
			self.getErrorElement(this).set('html', lang.username_invalid)
			this.addClass('txt-error');
			this.removeClass('txt');
			this.addClass('invalid');
			self.getErrorElement(this).getParent('div').removeClass('none');
			return;
		}

		var regex = /^[-_a-z0-9]+$/i;
		if ( ! regex.test(this.get('value')) ) {
			self.getErrorElement(this).set('html', lang.username_invalid_characters);
			this.addClass('txt-error');
			this.removeClass('txt');
			this.addClass('invalid');
			self.getErrorElement(this).getParent('div').removeClass('none');
			return;
		}

		this.addClass('loading');

		new Request({
			url: '/' + Cubix.Validator.Lang + '/private/check?username=' + this.get('value'),
			method: 'get',
			onSuccess: function (resp) {
				var resp = JSON.decode(resp);

				this.removeClass('loading');

				if ( resp.status == 'found' ) {
					self.getErrorElement(this).set('html', lang.username_exists);
					this.addClass('txt-error');
					this.removeClass('txt');
					this.addClass('invalid');
					self.getErrorElement(this).getParent('div').removeClass('none');
				}
				else if ( resp.status == 'blocked' ) {
					self.getErrorElement(this).set('html', lang.username_has_blocked_word);
					this.addClass('txt-error');
					this.removeClass('txt');
					this.addClass('invalid');
					self.getErrorElement(this).getParent('div').removeClass('none');
				}
				else if ( resp.status == 'not found' ) {
					self.getErrorElement(this).destroy();
					this.removeClass('txt-error');
					this.addClass('txt');
					this.addClass('valid');
					self.getErrorElement(this).getParent('div').addClass('none');
				}
			}.bind(this)
		}).send();


	});

	// Check password
	$('password').addEvent('blur', function () {
		var lang = Cubix.Lang.signup;

		this.addClass('txt');
		this.removeClass('txt-error');

		this.removeClass('invalid');
		this.removeClass('valid');
		self.getErrorElement(this).getParent('div').addClass('none');

		if ( this.get('value') == $$('.registration-box #username')[0].get('value') ) {
			self.getErrorElement(this).set('html', lang.username_equal_password)
			this.addClass('txt-error');
			this.removeClass('txt');
			this.addClass('invalid');
			self.getErrorElement(this).getParent('div').removeClass('none');
			return;
		}
		
		if ( this.get('value').length < 6 ) {
			self.getErrorElement(this).set('html', lang.password_invalid);
			this.addClass('txt-error');
			this.removeClass('txt');
			this.addClass('invalid');
			self.getErrorElement(this).getParent('div').removeClass('none');
		}
		else {
			self.getErrorElement(this).destroy();
			this.addClass('txt');
			this.removeClass('txt-error');
			this.addClass('valid');
			self.getErrorElement(this).getParent('div').addClass('none');
		}
	});

	// Check password confirmation
	$('password2').addEvent('blur', function () {
		var lang = Cubix.Lang.signup;

		$('password').fireEvent('blur');
		if ( $('password').hasClass('txt-error') ) return;

		this.addClass('txt');
		this.removeClass('txt-error');

		this.removeClass('invalid');
		this.removeClass('valid');
		self.getErrorElement(this).getParent('div').addClass('none');

		if ( this.get('value') != $('password').get('value') ) {
			self.getErrorElement(this).set('html', lang.password2_invalid);
			this.addClass('txt-error');
			this.removeClass('txt');
			this.addClass('invalid');
			self.getErrorElement(this).getParent('div').removeClass('none');
		}
		else {
			self.getErrorElement(this).destroy();
			this.addClass('txt');
			this.removeClass('txt-error');
			this.addClass('valid');
			self.getErrorElement(this).getParent('div').addClass('none');
		}
	});

	// Check email
	$('email').addEvent('blur', function () {
		var lang = Cubix.Lang.signup;

		this.addClass('txt');
		this.removeClass('txt-error');

		this.removeClass('invalid');
		this.removeClass('valid');
		self.getErrorElement(this).getParent('div').addClass('none');

		var regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
		if ( ! this.get('value').length || ! regex.test(this.get('value') ) ) {
			self.getErrorElement(this).set('html', lang.email_invalid)
			this.addClass('txt-error');
			this.removeClass('txt');
			this.addClass('invalid');
			self.getErrorElement(this).getParent('div').removeClass('none');
			return;
		}

		this.addClass('loading');

		new Request({
			url: '/' + Cubix.Validator.Lang + '/private/check?email=' + this.get('value'),
			method: 'get',
			onSuccess: function (resp) {
				var resp = JSON.decode(resp);

				this.removeClass('loading');

				if ( resp.status == 'found' ) {
					self.getErrorElement(this).set('html', lang.email_exists);
					this.addClass('txt-error');
					this.removeClass('txt');
					this.addClass('invalid');
					self.getErrorElement(this).getParent('div').removeClass('none');
				}
				else if ( resp.status == 'not found' ) {
					self.getErrorElement(this).destroy();
					this.addClass('txt');
					this.removeClass('txt-error');
					this.addClass('valid');
					self.getErrorElement(this).getParent('div').addClass('none');
				}
				else if ( resp.status == 'domain blacklisted' ) {
					self.getErrorElement(this).set('html', lang.domain_blacklisted);
					this.addClass('txt-error');
					this.removeClass('txt');
					this.addClass('invalid');
					self.getErrorElement(this).getParent('div').removeClass('none');
				}
			}.bind(this)
		}).send();
	});

	/*$$('.signup-form')[0].addEvent('submit', function (e) {
		var lang = Cubix.Lang.signup;

		['username', 'password', 'password2', 'email'].each(function (id) {
			$(id).fireEvent('blur');
		});

		if ( this.getElement('.error') ) {
			e.stop();
			alert(lang.form_errors);
			return;
		}

		if ( ! $('terms').get('checked') ) {
			e.stop();
			alert(lang.terms_required);
			return;
		}
	});*/
};

window.addEvent('domready', function () {
	new Cubix.Validator.SignUp();
});
