Cubix.CityAlerts = {};

Cubix.CityAlerts.Show = function() {
	var ov = new Cubix.Overlay($('cont'), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});

	ov.disable();
	
	new Request({
		url: '/city-alerts/ajax-get',
		method: 'get',
		onSuccess: function (resp) {			
			$('cont').set('html', resp);

			ov.enable();
		}
	}).send();
};

Cubix.CityAlerts.Remove = function(id) {
	var ov = new Cubix.Overlay($('cont'), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});

	ov.disable();
	
	new Request({
		url: '/city-alerts/remove',
		method: 'post',
		data: {
			id: id
		},
		onSuccess: function (resp) {			
			ov.enable();
			
			Cubix.CityAlerts.Show();
		}
	}).send();
	
	return false;
};

Cubix.CityAlerts.Add = function() {
	var ov = new Cubix.Overlay($('city-alert-cont'), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});

	ov.disable();
		
	var data = {
		city_id: $('city').get('value'),
		/*notification: $$('input[name=notification]:checked').get('value').toString(),
		period: $('period').get('value')*/
	};
	
	if ($('escorts').get('checked'))
		data.escorts = 1;
	else
		data.escorts = 0;
	
	if ($('agencies').get('checked'))
		data.agencies = 1;
	else
		data.agencies = 0;
	
	new Request({
		url: '/city-alerts/add',
		method: 'post',
		data: data,
		onSuccess: function (resp) {
			resp = JSON.decode(resp);
			var status = resp.status;
			
			$('city').removeClass('err');
			$('ch-wr').removeClass('err');

			if (!$('err-msg').hasClass('none'))
				$('err-msg').addClass('none');

			if (status == 'success')
			{
				ov.enable();
				
				Cubix.CityAlerts.Show();
			}
			else if (status == 'error')
			{
				var msgs = resp.msgs;
				
				if (msgs.city_id)
				{
					$('city').addClass('err');
					
					if (msgs.city_id == 'exists')
						$('err-msg').removeClass('none');
				}

				if (msgs.about)
				{
					$('ch-wr').addClass('err');
				}
				
				ov.enable();
			}
		}
	}).send();
	
	return false;
};

window.addEvent('domready', function() {
	Cubix.CityAlerts.Show();
});
