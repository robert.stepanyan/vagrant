Cubix.PhishingWarning = {};

Cubix.PhishingWarning.inProcess = false;

Cubix.PhishingWarning.url = '';

Cubix.PhishingWarning.Show = function () {
	if ( Cubix.PhishingWarning.inProcess ) return false;
	
	var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000', z_index: 200 });
	page_overlay.disable();


	$$('#PhishingWarningWrapper').addClass('open');
	
	Cubix.PhishingWarning.inProcess = true;

	$$('#PhishingWarningButton').addEvent('click', function() {
		$$('#PhishingWarningWrapper').destroy();
		page_overlay.enable();
	});
	
	return false;
}
