/* --> GOTM */
Cubix.Gotm = {};

Cubix.Gotm.container = 'gotm-list-box';
Cubix.Gotm.url = '/escorts/ajax-gotm';
Cubix.Gotm.search_input_text = 'search by showname';
Cubix.Gotm.data = {
	page: 1,
	showname: ""
}
Cubix.Gotm.Load = function (data) {
	
	Cubix.Gotm.InitSearchInput('search');
	Cubix.Gotm.Show(data);
	
	return false;
}
Cubix.Gotm.InitSearchInput = function (input) {
	Cubix.Gotm.InitSearchInput.input = $(input);
	
	this.timer = null;
	
	
	Cubix.Gotm.InitSearchInput.input.addEvent('focus', function(){
		if ( this.get('value') == Cubix.Gotm.search_input_text ) {
			this.set('value', '');
			this.removeClass('def-text');
		}	
	});
	Cubix.Gotm.InitSearchInput.input.addEvent('blur', function(){
		if ( ! this.get('value').length ) {
			this.set('value', Cubix.Gotm.search_input_text);
			this.addClass('def-text');
		}
	});
	
	Cubix.Gotm.InitSearchInput.input.addEvents({
		keyup: function () {
			$clear(this.timer);
			this.timer = setTimeout('Cubix.Gotm.InitSearchInput.KeyUp(Cubix.Gotm.InitSearchInput.input)', 500);
		}.bind(this)
	});
}

Cubix.Gotm.InitSearchInput.KeyUp = function (input) {
	
	
	var value = ( input.get('value').length && input.get('value') != Cubix.Gotm.search_input_text ) ? input.get('value') : '';
		
	Cubix.Gotm.Show({showname: value,page:1});		
	
}
Cubix.Gotm.Show = function (data) {
	
	Object.append(Cubix.Gotm.data,data);
	var myScroll = new Fx.Scroll(document.getElement('body'));
	var overlay = new Cubix.Overlay($(Cubix.Gotm.container), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	overlay.disable();
	
	new Request({
		url: Cubix.Gotm.url,
		method: 'get',
		data: Cubix.Gotm.data,
		onSuccess: function (resp) {			
			$(Cubix.Gotm.container).set('html', resp);
			overlay.enable();
			myScroll.toTop();
		}
	}).send();
	return false;
}
