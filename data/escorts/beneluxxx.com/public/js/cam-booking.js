window.addEvent('domready', function () {
	init();
});

var init = function(){
	$$('#toggle').addEvent('change', function(){
		var checkbox = $$('input.toggle-value:checked');
		$$('.cam-block').toggleClass('blue');
		$$('.cam-block-body').toggleClass('none');
		if(typeof checkbox[0]!='undefined'){
			saveSkypeSettings();
		}else{
			saveSkypeSettings();
		}
	}),
  
	$$('.cam-booking-btn').addEvent('click', function(){
		saveSkypeSettings();
	});

	//this is so stupid omg
	$$("#price-60").addEvent('change',function() {
		var should_be_selected = $(this).getElements(":selected")[0].get('class');
		$$("#price-30").getElement('.' + should_be_selected)[0].selected=true;
		settks();
	}); 
	$$("#price-30").addEvent('change',function() {
		var should_be_selected = $(this).getElements(":selected")[0].get('class');
		$$("#price-60").getElement('.' + should_be_selected)[0].selected=true;
		settks();
	}); 
}

var saveSkypeSettings = function(){
	loader(1);
	var formObjects = $('cam-form').toQueryString().parseQueryString();
	var ajax = new Request({
		url: '/cam-booking/save',
		method:'POST',
		data: formObjects,
		onSuccess: function(resp){
			resp = JSON.decode(resp);
			console.log(resp);
			if(resp.status == 'error'){
				$$('input').removeClass('cam-input-error');
				let inputName = Object.keys(resp.msgs)[0];
				let inputValues = Object.values(resp.msgs)[0];
				$(inputName).addClass('cam-input-error');
				//inputValues
				$('cam-status').set('html', '');
			}
			else{
				$('cam-status').set('html', resp.text);
				$$('input').removeClass('cam-input-error');
			}
		},
		onComplete:function()
		{
			loader(0);
		}
	});

	ajax.send();
}

var settks = function(){
	$$('#price30tks').set('html', $$("#price-30").getElements(":selected")[0].get("value") * 10);
	$$('#price60tks').set('html', $$("#price-60").getElements(":selected")[0].get("value") * 10);
}

var loader = function(status){
	if(status == 1){
		$$('.cam-overlay').removeClass('none');
		$$('.toggle-cam-btn').setStyle('pointer-events', 'none');
	}else{
		$$('.cam-overlay').addClass('none');
		$$('.toggle-cam-btn').setStyle('pointer-events', 'auto');
	}
}
