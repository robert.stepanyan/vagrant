window.addEvent('domready',function() {
	$$('.big-red-btn-wrapper').hide();

	var finish = false, page = 1, req = new Request({
		method: 'get',
		url: Cubix.Escorts.GetRequestUrl(''),
		data: { page: 1 },
		onSuccess: function (resp) {
			ss.options.min = window.getScrollSize().y - window.getSize().y - 150;
			var target = $$('.escorts')[0];
			target.getElements('.spinner').destroy();

			var rows = new Element('div', { html: resp }).getElements('.row');
			if ( rows.length == 0 ) { finish = true; $$('.big-red-btn-wrapper').show(); }
			rows.each(function (row) {
				row.inject(target);
			});

			Cubix.PhotoRoller.Init();
			Cubix.EscortHover.Init();
		}
	});

	window.addEvent('escortsFilterChange', function () { page = 1; finish = false });

	var ss = new ScrollSpy({
		min: window.getScrollSize().y - window.getSize().y - 150,
		onScroll: function (p) {
			if ( req.running || finish ) return;
			if ( p.y > window.getScrollSize().y - window.getSize().y - 150 ) {
				if ( $$('.escorts')[0].getElements('.row').length > 0 ) {
					var hash = document.location.hash.substring(1);
					req.options.url = Cubix.Escorts.GetRequestUrl(hash);
					req.options.data.page = ++page;

					$$('.escorts')[0].getElements('.row.last').removeClass('last');
					getSpinner().inject($$('.escorts')[0]);
					req.send();
				}
			}
		},
		container: window
	});

	var getSpinner = function () {
		var el = new Element('div', { 'class': 'spinner' });
		new Element('span', { html: 'Loading more escorts...' }).inject(el);
		return el;
	};
});
