Votes = {
    selBox: null,
    pageBox: null,
    maxPageBox: null,

    init: function(){
        var self = this;
        if( $$('div[data-escort]').length > 0 ){
            self.getVotesBlock( false );
        } else {
            self.getVotesBlock( true );
        }
    },

    getVotesBlock: function( stat, _page ){
        var url, self = this, page, city_id;
        self.pageBox = $('chart-page');
        self.selBox = $$('.validateCity');

        if( stat ){
            url = "/city-votes";
        } else {
            url = "/city-votes/save";
            if( !_page ){
                city_id = ( self.selBox.get("value") ) ? self.selBox.get("value") : '';
                page = ( self.pageBox.get("value") ) ? self.pageBox.get("value") : 1;
            } else {
                city_id = 0;
                page = _page;
            }
        }

        if( Object.prototype.toString.call( city_id ) === '[object Array]' ) {
            city_id = city_id[0];
        }

        var data = { 'city_id' : city_id, 'page' : page };
        
        if( $$('div[data-escort]').length > 0 ){
            self.sendRequest( url, data, stat, 'escort-city-chart' )
        } else {
            self.sendRequest( url, data, stat )
        }
    },

    getVotesStat: function(){
        var self = this;

        $$('.vote-this').addEvent('click', function(){
            var selBox = $$('.validateCity');

            if( selBox.get("value") != '0' ){
                selBox.setStyle('borderColor', '#cccccc');
                self.getVotesBlock( false );
            } else {
                selBox.setStyle('borderColor', '#ff0000');
            }
        });

        $$('.city-stats').addEvent('click', function(){
            self.getVotesBlock( false );
        });

        $$('.to-vote').addEvent('click', function(){
            self.getVotesBlock( true );
        });
    },

    sendRequest: function( url, data, stat, _container ){
        var self = this, container;

        if( !_container ){
            container = $$('.member-right-col');
        } else {
            container = $$('.' + _container);
        }

        var requestData = new Request ({
            url: url,
            method: 'post',
            data: data,
            onRequest: function(){
                container.addClass('load');
            },
            onSuccess: function( response ){
                container.set('html', response);

                self.chartNavigation();

                if( $('expired-index') ){
                    if( $('expired-index').get('value') == 1 ){
                        self.getVotesBlock( false );
                    }
                }

                if( !_container ){
                    self.getVotesStat();
                }

                if( !stat ){
                    Charts.init( ( $('labels-data').get('value') ).split( "," ), ( $('data-1').get('value') ).split( "," ), ( $('data-2').get('value') ).split( "," ) );

                    if( $('expired').get('value') == 1 ){
                        if( data.city_id > 0 ){
                            alert('Maximum 3 votes allowed per month');
                        }
                        $$('.votes-expired').fade('in');
                    } else {
                        $$('.votes-expired').fade('out');
                    }

                    var pgLeft =  $$('.chart-prev');
                    if( Number(self.pageBox.get("value")) < 2 ){
                        pgLeft.fade('out');
                    } else {
                        pgLeft.fade('in');
                    }

                    var pgRight =  $$('.chart-next');
                    if( Number( self.pageBox.get("value")) >= Math.ceil( ( Number( $('city-count').get('value') / 5 ) ) ) ){
                        pgRight.fade('out');
                    } else {
                        pgRight.fade('in');
                    }
                }

                container.removeClass('load');
            }
        });

        requestData.send();
    },

    chartNavigation: function(){
        var self = this;
        var page;

        $$('.chart-next').addEvent('click', function(){

            /*if( Number( page ) < 2 ){
                page = 1;
                self.pageBox.set("value", 1);
            } else {*/
                self.pageBox.set("value", Number( self.pageBox.get("value") ) + 1);
                page = self.pageBox.get("value");
            //}

            self.getVotesBlock( false, page );

        });

        $$('.chart-prev').addEvent('click', function(){

            page = self.pageBox.get("value");

            if( Number( page ) < 2 ){
                page = 1;
                self.pageBox.set("value", 1);
                //this.fade('out');
            } else {
                self.pageBox.set("value", Number( self.pageBox.get("value") ) - 1);
                page = self.pageBox.get("value");
                //this.fade('in');
            }

            self.getVotesBlock( false, page );

        });
    }
};
