MCubix.Comments = {
	lng: 'nl',
	escort_id: '',
	is_member: false,
	is_escort: false,

	init(params) {
		var escort_id = this.escort_id = params?.escort_id;
		this.loadComments({escort_id});
		this.initAddForm();
	},

	initAddForm() {
		$('.add-comment-btn')
			.off('click')
			.on('click', function(e) {
				e.preventDefault();
				e.stopPropagation();

				$('#add-comment-form').toggle();

				if (this.is_member || this.is_escort) {
					$('#add-comment-form').html('');
					$('#comment_added').css('display', 'none');

					$('.reply_place').each( it => {
						it.html('');
					});

					var overlay = new MCubix.Overlay($('#comments_container'), {
							loader: _st('loader-small.gif'),
							position: '50%'
						}),
						params = {};

					if ($(this).attr('rel')) {
						$.ajax({
							url: `/${this.lng}/comments-v2/ajax-add-comment`,
							method: 'get',
							data: $merge(
								{
									escort_id: this.escort_id,
									comment_id: this.get('rel')
								},
								params || {}
							),
							onSuccess: function (resp) {
								var el = this.getParent('span.commenter').getNext('span.comment_body').getElement('div.reply_place'),
									height = new Element('div', { html: resp }).getHiddenHeight(el);

								el.css({ height: 0, overflow: 'hidden' })
									.set('tween', {
										duration: 500,
										onComplete: function () {
											this.element.setStyle('height', null) }
									})
									.html(resp)
									.tween('height', height);

								this.initFormSubmit();
								this.closeAddForm();

								overlay.enable();
							}
						});
					} else {
						$.ajax({
							url: `/${this.lng}/comments-v2/ajax-add-comment`,
							method: 'get',
							evalScripts: true,
							data: $merge({
								escort_id: this.escort_id
							}, params || {}),
							onSuccess: function(resp) {
								var height = new Element('div', { html: resp }).getHiddenHeight($('add-comment-form'));
								$('#add-comment-form').css({ height: 0, overflow: 'hidden' })
									.html(resp)
									.attr('tween', { duration: 500, onComplete: function () { this.element.setStyle('height', null) } })
									.tween('height', height);

								this.initFormSubmit();
								this.closeAddForm();

								overlay.enable();

							}
						});
					}
				} else {
					console.log(this.is_member);
					console.log(this.is_escort);

					console.log(`${location.protocol}//www.beneluxxx.com/private/signin`)
					// location.href = `${location.protocol}//www.beneluxxx.com/private/signin`;
				}

		});

		$('.mobile-link-btn')
			.off('click')
			.on('click', function (e) {
				e.preventDefault();
				e.stopPropagation();

				location.href = $(this).attr('data-href');
			});
	},

	initFormSubmit() {
		$('.btn-save').on('click', function (e) {
			e.stopPropagation();

			var overlay = new MCubix.Overlay($('.myprofile')[0], {
				loader: _st('loader-small.gif'),
				position: '50%'
			});

			overlay.disable();

			new Request({
				url: '/' + this.lng + '/comments-v2/ajax-add-comment',
				method: 'post',
				evalScripts: true,
				data: this.parent('form'),
				onSuccess: function (resp) {
					if (this.isJSON(resp)) {
						if (JSON.decode(resp)) {
							var data = JSON.decode(resp),
								error_map = ['comment_to_short', 'captcha_error'];

							error_map.each( it => {
								$(it).css('display', 'none');
							});

							data.each( it => {
								$(it).css('display', 'block');
							});
						}
						overlay.enable();
					} else {
						$('#add-comment-form').html('');

						this.getParent('div.reply_place').html('');

						$('#comment_added').css('display', 'block');

						// this.loadComments({ page: 1 });
					}

				}.bind(this)
			}).send();
		});
	},

	isJSON(str) {
		str = str.replace(/\\./g, '@').replace(/"[^"\\\n\r]*"/g, '');
		return (/^[,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]*$/).test(str);
	},

	closeAddForm() {
		$('.btn-close').on('click', function (e) {
			e.stopPropagation();

			$('add-comment-form').html('');
			$('div.reply_place').each( it => {
				it.html('');
			});
		});
	},

	initVote() {
		$('a.vote-up, a.vote-down').on('click', e => {
			e.stopPropagation();

			if (this.is_member) {
				var overlay = new MCubix.Overlay($('#comments_container'), {
					loader: _st('loader-small.gif'),
					position: '50%'
				});

				overlay.disable();

				var params = { type: e.target.classList.contains('vote-up') ? 'vote-up' : 'vote-down' };

				new Request({
					url: '/' + this.lng + '/comments-v2/ajax-vote',
					method: 'get',
					evalScripts: true,
					data: $merge({ escort_id: this.escort_id, comment_id: this.attr('rel') }, params || {}),
					onSuccess: function (resp) {
						if ( resp.length === 0 ) {
							this.loadComments({ page: 1 });
						}
						else {
							var el = this.parent('span.commenter').find('span.comment_body .reply_place')[0];
							el.html(resp);
							overlay.enable();
						}
					}.bind(this)
				}).send();
			} else {
				MCubix.Popup.Show('489', '652');
			}
		})
	},

	loadComments(params) {
		var commentsContainer = $('#comments_container')
			// overlay = new MCubix.Overlay(commentsContainer, {
			// loader: this._st('loader-small.gif'),
			// position: '50%'
		// })
		;

		// var regex = /([a-z]{2})\/escort/g,
		// 	matches = regex.exec(location.pathname);
		//
		// this.lng = typeof matches[1] !== typeof undefined ? matches[1] : 'nl';

		// overlay.disable();

		// var tween = commentsContainer.find('tween');
		// tween.options.duration = 300;

		$.ajax({
			url: `/${this.lng}/comments-v2/ajax-show`,
			type: 'get',
			dataType: 'text',
			data: params || {},
			success: response => {
				commentsContainer.html(response);
				// var height = new Element('div', { html: resp, styles: { width: commentsContainer.getWidth() } }).getHiddenHeight(commentsContainer),
				// 	startHeight = commentsContainer.getHeight();
				//
				// tween.removeEvents('complete').addEvent('complete', function () {
				// 	tween.removeEvents('complete').addEvent('complete', function () {
				// 		tween.set('opacity', 0);
				//
				// 		tween.removeEvents('complete').addEvent('complete', function () {
				//
				// 		}).set('opacity', 0).start('opacity', 1);
				//
				// 		this.element.set('html', resp).setStyle('height', null);
				//
				// 		this.init();
				// 		this.initVote();
				//
				// 		overlay.enable();
				//
				// 	}).set('height', startHeight).start('height', height);
				// }).set('opacity', 1).start('opacity', 0);
			},
			error: error => {
				console.log(error)
			}
		});

		return false;
	},
};
