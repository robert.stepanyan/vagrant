;(function (window, document) {

    function setInputFilter(textbox, inputFilter) {
        ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function (event) {
            textbox.addEventListener(event, function () {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
            });
        });
    };

    function ecardonPopup(checkoutId) {

        $.ajax({
            url: '/paiement/ecardon-popup',
            type: 'get',
            success: function (resp) {
                document.querySelector('#ecardon-popup').innerHTML = resp;


                var scriptContent = 'var wpwlOptions = { locale: "' + Cubix.Lang.id + '", style: "plain", onBeforeSubmitCard: function(){ if ( $(".wpwl-control-cardHolder").val() == "") { $(".wpwl-control-cardHolder").addClass("wpwl-has-error"); $(".wpwl-button-pay").addClass("wpwl-button-error").attr("disabled", "disabled"); $("div", {"class": "wpwl-hint wpwl-hint-cardHolderError", "text": "card holder required"}).appendTo($(".wpwl-control-cardHolder"), "after");  return false} else return true}}';

                var script = document.createElement('script');
                script.innerHTML = scriptContent;

                var j = document.getElementsByTagName("script")[0];
                j.parentNode.insertBefore(script, j);

                var script = document.createElement('script');
                script.src = "https://oppwa.com/v1/paymentWidgets.js?checkoutId=" + checkoutId;

                var j = document.getElementsByTagName("script")[0];
                j.parentNode.insertBefore(script, j);

                $('.ecardon-popup__wrapper').show();
            }
        })
    };

    function submitPaymentForm(data) {

        $('.payment-form__wrapper').addClass('processing');

        $.ajax({
            url: '/paiement/checkout',
            data: data,
            type: 'post',
            success: function (response) {
                response = JSON.parse(response);

                $('.payment-form__wrapper').removeClass('processing');

                switch (response.status) {
                    case 'ecardon_success':
                        ecardonPopup(response.checkoutId);
                        break;

                    case 'twispay_success':
                        var formHtml = response.form;
                        $('body').append(formHtml.replace(/ \"/g, "\""));
                        $('#twispay-payment-form').submit();
                        break;

                    case 'error':
                        alert('Currently payment system is not available, please check back soon. ' + (resposne.error ? 'Details ' + resposne.error : ''));
                        break;
                }

            }
        })
    }


    document.addEventListener("DOMContentLoaded", function () {

        var numbersOnlyInputs = document.querySelectorAll("[data-validate='true'][data-accept='number']");

        numbersOnlyInputs.forEach(function ($element) {
            setInputFilter($element, function (value) {
                return /^\d*\.?\d*$/.test(value); // Allow digits and '.' only, using a RegExp
            });
        });

        document.getElementById('pay-btn').addEventListener('click', function () {

            var data = {
                amount: document.getElementById('amount-inp').value,
                paymentMethod: document.querySelector('[name="paymentMethod"]:checked').value,
            };

            if (data.amount <= 0) {
                return alert("Invalid Amount specified");
            }

            if (!['twispay', 'ecardon'].includes(data.paymentMethod)) {
                return alert("Invalid PaymentMethod specified");
            }

            submitPaymentForm(data);
        });

        $('.ecardon-popup__wrapper > .heading > svg').click(function () {
            $(this).parents('.ecardon-popup__wrapper').hide();
        });
    });

})(this, document);