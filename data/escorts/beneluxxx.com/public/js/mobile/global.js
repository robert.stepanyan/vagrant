var Cubix = {};

Cubix.Lang = {};

Cubix.ShowTerms = function (link) {
	window.open(link, 'showTerms', 'menubar=no, resizable=yes, status=no, scrollbars=yes, toolbar=no, width=640, height=480');
	return false;
}

var _log = function (data) {
	if ( console && console.log != undefined ) {
		console.log(data);
	}
}


var _st = function (object) {
	var parts = object.split('.');
	if ( parts.length < 2 ) {
		return _log('invalid parameter, must contain filename with extension');
	}

	var ext = parts[parts.length - 1];
	var protocol = (location.protocol === 'https:') ? 'https' : 'http';
	var base_url = protocol + '://st.beneluxxx.com',
	prefix = '';

	switch ( ext ) {
		case 'jpg':
		case 'gif':
		case 'png':
			prefix = '/img/';
			break;
		case 'css':
			prefix = '/css/';
			break;
		case 'js':
			prefix = '/js/';
			break;
		default:
			return _log('invalid extension "' + ext + '"');
	}

	url = base_url + prefix + object;

	return url;
}

// Taken from viewed-escorts.js
/* --> Viewed Escorts */
Cubix.Viewed = {};

Cubix.Viewed.url = ''; // Must be set from php
Cubix.Viewed.container = '';

$(document).on('ready', function() {
	setInterval(function(){ 
		updateLastLogin();
	}, 240000);
	
	$('.flags a').on('click', function(e){
		var url = this.get('rel');
		window.location = url;
	});
	
	// if ( Cookie.read('email_collecting') != 'done' && ! Cookie.read('ec_session_closed') && ! headerVars.signedIn && Cookie.read('ec_count') == 3) {
	// 	Cubix.EmailCollectingPopup.url = '/' + headerVars.lang + '/index/email-collecting'
	// 	Cubix.EmailCollectingPopup.Show(100, 900);
	// }
	//
	// if ( ! $(Cubix.Viewed.container) || ! Cubix.Viewed.url ) return;
	//
	// new Request({
	// 	url: Cubix.Viewed.url,
	// 	method: 'get',
	// 	onSuccess: function (resp) {
	// 		if(resp.contains('rapper'))
	// 		{
	// 			$(Cubix.Viewed.container).setStyle('opacity', 0);
	// 			var myFx = new Fx.Tween($(Cubix.Viewed.container), {
	// 				duration: 400,
	// 				onComplete: function() {
	// 					$(Cubix.Viewed.container).set('html', resp);
	// 					$(Cubix.Viewed.container).tween('opacity', 0, 1);
	// 				}
	// 			});
	// 			myFx.start('height', '137');
	// 		}
	// 	}
	// }).send();
});

var updateLastLogin = function () {
	$.ajax({
		url: '/private/check-login',
		method: 'get',
		onSuccess: function (resp) {			
			if (resp === 1) {
				$.ajax({
					url: '/private/update-login',
					method: 'get',
					onSuccess: function (resp) {
						
					}
				});
			}
		}
	});
};

var resizeProfile = function() {
	var comments = $('.cTab')[0],
		profileHeight;

	if (comments) {
		var leftSide = $('#left'),
			profile = $('#profile-container');

		if (!profile.attr('rel')) {
			profile.attr('rel', profile.size().y);
		}

		var commentsHeight = comments.css({
			visibility: 'hidden',
			display: 'block'
		}).height();

		comments.css({
			visibility: null,
			display: null
		})

		profileHeight = (profile.attr('rel') * 1) + (commentsHeight * 1) - 4700;

		if ( leftSide.size().y > profileHeight ) {
			profile.css('height', left.size().y);
		} else {
			profile.css('height', profileHeight);
		}
	} else {
		var leftHeight = $("#left").getCoordinates().height;

		profileHeight = $("#profile-container").position().height - 4600;

		if ( leftHeight > profileHeight ) {
			$("#profile-container").setStyle('height', leftHeight);
		} else {
			$("#profile-container").setStyle('height', profileHeight);
		}
	}
};

var _setOptions = function () {
	var b = this.options = Object.merge.apply(null, [{}, this.options].append(arguments));
	if (this.addEvent) {
		for (var c in b) {
			if (typeOf(b[c]) != "function" || !(/^on[A-Z]/).test(c)) {
				continue;
			}
			this.addEventListener(c, b[c]);
			delete b[c];
		}
	}
	return this;
}