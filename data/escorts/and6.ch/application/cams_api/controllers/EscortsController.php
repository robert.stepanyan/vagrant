<?php

class CamsApi_EscortsController extends Zend_Controller_Action
{
	/**
	 * @var Zend_Db_Adapter_Mysqli
	 */
	protected $_db;
	
	public function init()
	{
		$this->_db = Zend_Registry::get('db');
		$system = Zend_Registry::get('system_config');
		$this->secret = $system['cams']['secret'];
		$this->errors = array(
			'error' => array(
				"code" => "ERR_VALIDATION",
				"details" => array()  
			)
		);
		
		if( $this->secret != $_SERVER['HTTP_X_SERVER_SECRET']){
			header("HTTP/1.1 401 Unauthorized"); die;
		}
		elseif (!$this->_request->isPost())
		{
			header($_SERVER["SERVER_PROTOCOL"]." 405 Method Not Allowed", true, 405); die;
		}
	}

	public function indexAction()
	{
		
	}
	
	public function statusAction()
	{
		header('Content-Type: application/json');
		//"offline", "away", "free", "spy-only", "private"
				
		$rawdata = file_get_contents("php://input");

		$decoded = json_decode($rawdata, true);
		
		$statuses = array("offline", "away", "free", "spy-only", "incognito" , "private");
		$is_valid = true;

		if(!isset($decoded['remote_id'])){
			$this->errors['error']['details'][] = array("field" => "remote_id",	"validator"=> "required");
			$is_valid = false;
		}
		elseif(!is_numeric($decoded['remote_id'])){
			$this->errors['error']['details'][] = array("field" => "remote_id",	"validator" => "invalid");
			$is_valid = false;
		}

		if(!isset($decoded['state'])){
			$this->errors['error']['details'][] = array("field" => "state",	"validator"=> "required");
			$is_valid = false;
		}
		elseif(!in_array($decoded['state'], $statuses )){
			$this->errors['error']['details'][] = array("field" => "state",	"validator"=> "invalid");
			$is_valid = false;
		}

		if(strlen($decoded['preview_url']) > 0 && !preg_match('/^[A-Za-z0-9_\-\/\.]+$/', $decoded['preview_url'])){
			$this->errors['error']['details'][] = array("field" => "preview_url",	"validator"=> "invalid");
			$is_valid = false;
		}
		
		if(strlen($decoded['channel_url']) > 0 && !preg_match('/^[A-Za-z0-9_\-\/]+$/', $decoded['channel_url'])){
			$this->errors['error']['details'][] = array("field" => "channel_url",	"validator"=> "invalid");
			$is_valid = false;
		}
		
		if($is_valid){
			header("HTTP/1.1 200 OK");
			
			$preview_url = isset($decoded['preview_url']) ? $decoded['preview_url'] : null;
			$channel_url = isset($decoded['channel_url']) ? $decoded['channel_url'] : null;
			$data = array(
				'cam_status' => $decoded['state'],
				'cam_preview_url' => $preview_url,
				'cam_channel_url' => $channel_url
			); 
			$this->_db->update('escorts', $data, $this->_db->quoteInto('id = ?', $decoded['remote_id']));
			$model_escort = new Model_EscortsV2();
			$model_escort->updateCamStatus($decoded['remote_id'],$data);
		}
		else{
			header('HTTP/1.0 400 Bad Request');
			echo json_encode($this->errors);
		}
						
		die;
	}		
	

}
