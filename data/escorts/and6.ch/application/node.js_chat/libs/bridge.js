var config = require('../configs/config.js').get('production');
var request = {
	http: require('http'),
	https: require('https')
};
var querystring = require('querystring');

var cache = {};

function getUserInfo(sessionId, callback, useCache)
{
	if ( typeof useCache == 'undefined' ) {
		useCache = true;
	}
	
	currentDate = new Date();
	if ( typeof cache[sessionId] != 'undefined' && ( currentDate - cache[sessionId]['date'] < config.common.sessionCacheTime ) && useCache ) {
		callback(cache[sessionId]['info']);
		return;
	}
	var post_data = querystring.stringify({
		'compilation_level' : 'ADVANCED_OPTIMIZATIONS',
		'output_format': 'json',
		'output_info': 'compiled_code',
		'warning_level' : 'QUIET',
		'sid': sessionId
	});

	var post_options = {
		host: config.common.host,
		port: config.common.ssl ? 443 : 80,
		path: config.common.authPath,
		method: 'POST',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Content-Length': post_data.length
		}
	};

	var post_req = request[config.common.ssl ? 'https' : 'http'].request(post_options, function(res) {
		res.setEncoding('utf8');
		res.on('data', function (resp) {
            try {
			  resp = JSON.parse(resp);
            }
            catch (ex) {
              resp = { auth: false, data: null };
            }
			
			//Setting cache
			cache[sessionId] = {
				info: resp,
				date: new Date()
			};
			callback(resp);
		});
	});

	post_req.write(post_data);
	post_req.end();
}

exports.getUserInfo = getUserInfo;
