var config = require('../configs/config.js').get('production'),
  app = require('http').createServer(handler).listen(8887, '127.0.0.1'),

  io = require('socket.io').listen(app, {
    log: false
  }),
  //io = require('socket.io').listen(app),
  crypto = require('crypto'),
  url = require('url'),

  userManager = require("./userManager.js").get(),
  messageManager = require("./messageManager.js").get(),
  settingsManager = require("./settingsManager.js").get(),
  bridge = require("./bridge.js");


function handler(request, response) {
  var queryData = url.parse(request.url, true)

  var query = queryData.query;

  //Changing availability
  if (queryData.pathname == '/node-chat/change-availability') {

    var uid = query.userId
    availability = query.availability,
      hash = query.hash;

    if (!uid || !availability || !hash || crypto.createHash('md5').update(uid + '-' + availability + '-' + config.common.key).digest("hex") != hash) {
      response.writeHead(200, {
        "Content-Type": "text/plain"
      });
      response.end("sta=error");
      return;
    }

    console.log('Changing availability for userId ' + uid + ' to ' + availability);

    settingsManager.setAvailability(uid, availability);

    response.writeHead(200, {
      "Content-Type": "text/plain"
    });
    response.end("sta=ok");
  }

  //Get availability
  if (queryData.pathname == '/node-chat/get-availability') {
    var uid = query.userId,
      hash = query.hash;

    if (!uid || crypto.createHash('md5').update(uid + '-' + config.common.key).digest("hex") != hash) {
      response.writeHead(200, {
        "Content-Type": "text/plain"
      });
      response.end("sta=error");
      return;
    }
    result = JSON.stringify({
      availability: settingsManager.getAvailability(uid)
    });

    response.writeHead(200, {
      "Content-Type": "text/plain"
    });
    response.end(result);
  }

  //Get online escorts
  if (queryData.pathname == '/node-chat/get-online-escorts') {
    userManager.getOnlineEscorts(function (userIds) {
      response.writeHead(200, {
        "Content-Type": "text/plain"
      });
      response.end(JSON.stringify(userIds));
    });
  }
}

settingsManager.startSettingsBackup(function () {
  console.log('User settings backup done');
});

userManager.startOfflineUsersCheck(function (userIds) {
  if (userIds.length) {
    io.sockets.emit('offline-users', userIds);
  }
});

messageManager.startBlackListedWordsSync(function () {
  console.log('Bl word sync is done.');
});

//Checking sessions. if session expired kick out user
setInterval(function () {
  users = userManager.getUsersPrivateList();
  for (var userId in users) {
    (function (userId) {
      bridge.getUserInfo(users[userId].info.sid, function (resp) {
        if (!resp.auth && users[userId]) {
          for (i = 0; i < users[userId].sockets.length; i++) {
            io.sockets.socket(users[userId].sockets[i]).disconnect('Loged off');
          }
          users[userId].sockets = [];
          userManager.addUserWithoutSockets(userId);
        }
      }, false);
    })(userId);
  }
}, config.common.sessionCheckInterval);

function start() {
  io.sockets.on('connection', function (socket) {
    socket.on('auth', function (data) {
      bridge.getUserInfo(data.sid, function (resp) {
        if (!resp.auth) {
          socket.emit('auth-complete', {
            auth: false
          });
          socket.disconnect('unauthorized');
          return;
        }
        var userInfo = resp.data;
        userInfo.status = 'online'; //Setting status to online
        userInfo.sid = data.sid;
        if (userInfo.userType != 'escort') {
          userInfo.avatar = config.common.noPhotoUrl;
        }

        console.log(resp.data)

        var availability = 1;
        if (data.forceEnable) {
          settingsManager.setAvailability(userInfo.userId, 1);
        } else {
          availability = settingsManager.getAvailability(userInfo.userId);

          if (availability === false) {
            if (userInfo.userType == 'agency') {
              availability = 0;
            } else {
              availability = 1;
            }
          }
        }

        var sound = settingsManager.getSoundsOn(userInfo.userId);
        userInfo.settings = settingsManager.get(userInfo.userId);
        userInfo.settings.sound = 1;

        socket.emit('auth-complete', {
          auth: true,
          availability: availability,
          userInfo: userInfo,
        });

        if (availability) {
          socket.userInfo = userInfo;

          var newUser = true;
          if (userManager.getUser(userInfo.userId)) {
            newUser = false;
          }

          isPrivate = socket.userInfo.settings.invisibility ? true : false;

          userManager.addUser(socket.id, userInfo, isPrivate);

          //Emit new-user only if a new user. f.ex can login from another browser.
          if (newUser && !isPrivate) {
            delete userInfo.settings;
            socket.broadcast.emit('new-user', userInfo);
          }


          var ou = userManager.getUsersPublicList();

          //Removing himself from list
          delete ou[userInfo.userId];

          socket.emit('online-users-list', ou);

          //Checking if has opened dialogs.
          //if yes getting userInfo and emiting open-dialogs event
          var openDialogs = settingsManager.getOpenDialogs(userInfo.userId);

            var od = [];
          if (openDialogs) {

            //messageManager.getMessagesThatWillBeVisibleFor(userInfo.userId, openDialogs, function (found_last_messages) {

            if(userInfo.userId == 8778) {
              console.log('openDialogs', openDialogs);
            }

            // When there are more than 5 dialogs open
            // then this will close all except first 5
            // ------------------------------------------
            // if (openDialogs.length > 5) {
            //   for (var i = 5; i < openDialogs.length; i++) {
            //     settingsManager.removeOpenDialog(userInfo.userId, openDialogs[i]);
            //   }
            //   openDialogs = openDialogs.slice(0, 5);
            // }
            // ------------------------------------------

            userManager.getUsers(openDialogs, true, function (users) {
              for (var i = 0; i <= openDialogs.length - 1; i++) {

                userId = openDialogs[i];
                if (users[userId] /*&& found_last_messages.includes(userId)*/) {
                  od.push({
                    'userId': userId,
                    'userInfo': users[userId].info
                  });
                }else{
                  settingsManager.removeOpenDialog(userInfo.userId, userId);
                }
              }
              activeDialog = settingsManager.getActiveDialog(userInfo.userId);
              socket.emit('open-dialogs', {
                dialogs: od,
                activeDialog: activeDialog,
                //'found_last_messages' : found_last_messages
                'found_last_messages': []
              });
            });

            //})
          }
            //Checking if has new messages. if yes emiting new-message event.
            messageManager.getNewMessagesCount(userInfo.userId, function (messages) {
              if (messages && messages.length) {
                userIds = [];
                //gathering user ids to call getUsers
                for (var i = 0; i < messages.length; i++) {
                  userIds.push(messages[i].userId);
                }
                //getting users and merging with messages
                userManager.getUsers(userIds, true, function (users) {
                  for (var i = 0; i < messages.length; i++) {
                    if (users[messages[i].userId]) {
                      messages[i].senderInfo = users[messages[i].userId].info;
                      messages[i].userInfo = userInfo;
                    }
                  }
                  socket.emit('new-messages', messages);
                });
              }
            });


        } else {
          socket.disconnect('status:not-available');
        }
      });
    });

    socket.on('message-sent', function (messageData) {
      if (typeof socket.userInfo == 'undefined') return;


      var blockedUsers = settingsManager.getBlockedUsers(messageData.userId);

      if (blockedUsers && blockedUsers.indexOf(socket.userInfo.userId) >= 0) {
        socket.emit('user-blocked-you', {
          dialogId: messageData.userId
        });

        return;
      }
	  
	  if(messageManager.detectUrl(messageData.message)){
		return;
	  }
	  
      user = userManager.getUser(messageData.userId, true, function (user) {
        if (user) {

          var message = messageManager.clearMessage(messageData.message);
          if (message.length > 0) {
            messageManager.checkMessage(message, function (res) {
              if (false /*res && res.length*/ ) { // Message is in black listed words, don't send message. emit sender about it.
                socket.emit('bl-word', {
                  dialogId: messageData.userId,
                  word: res.join(', ')
                });
              } else {
                messageManager.storeMessage(socket.userInfo.userId, user.info.userId, message);
                sockets = user.sockets;

                newMessageData = {
                  body: message,
                  userId: socket.userInfo.userId,
                  date: new Date().getTime()
                }
                for (var i = 0; i <= sockets.length - 1; i++) {
                  io.sockets.socket(sockets[i]).emit('new-message', {
                    message: newMessageData,
                    senderData: socket.userInfo
                  })
                }
              }
            });
          }
        }
      });
    });

    socket.on('dialog-opened', function (data) {
      if (typeof socket.userInfo == 'undefined') return;
      //showing todays history

      date = new Date();
      date.setDate(date.getDate() - 3);
      date.setHours(0);
      date.setMinutes(0);
      date.setSeconds(0);

      messageManager.getConversation(socket.userInfo.userId, data.userId, date, function (messages, threadId, ids) {
        var msgs = messages || [];

        senderInfo = userManager.getUser(data.userId, true, function (user) {
          socket.emit('message-history', {
            messages: msgs,
            senderInfo: user.info,
            threadId: threadId || 'gago',
            all_threads: ids || []
          });
        })
      });


      settingsManager.addOpenDialog(socket.userInfo.userId, data.userId);
    });

    socket.on('clear-history', function(data) {
      messageManager.clearHistory(socket.userInfo.userId, data.threadId);
    });

    socket.on('dialog-closed', function (data) {
      if (typeof socket.userInfo == 'undefined') return;
      settingsManager.removeOpenDialog(socket.userInfo.userId, data.userId);
    });

    socket.on('dialog-activated', function (data) {
      if (typeof socket.userInfo == 'undefined') return;
      settingsManager.setActiveDialog(socket.userInfo.userId, data.userId);
    });

    socket.on('dialog-deactivated', function (data) {
      if (typeof socket.userInfo == 'undefined') return;
      settingsManager.removeActiveDialog(socket.userInfo.userId);
    });

    socket.on('availability-changed', function ($availability) {
      if (typeof socket.userInfo == 'undefined') return;

      if ($availability) {
        settingsManager.setAvailability(socket.userInfo.userId, 1);
      } else {
        settingsManager.setAvailability(socket.userInfo.userId, 0);
        //Emitting to all sockets about availability changed
        user = userManager.getUser(socket.userInfo.userId);

        if (user) {
          for (var i = 0; i < user.sockets.length; i++) {
            io.sockets.socket(user.sockets[i]).emit('chat-off', {});
            io.sockets.socket(user.sockets[i]).disconnect('chat disabled');
          }
          /*while ( user.sockets.length > 0 ) {
            io.sockets.socket(user.sockets[0]).emit('chat-off', {});
            io.sockets.socket(user.sockets[0]).disconnect('chat disabled');
          }*/
        }
      }
    });

    socket.on('change-options', function (data) {
      if (typeof socket.userInfo == 'undefined') return;

      switch (data.option) {
        case 'show-only-escorts':
          settingsManager.setShowOnlyEscorts(socket.userInfo.userId, data.value);
          break;
        case 'keep-list-open':
          settingsManager.setKeepListOpen(socket.userInfo.userId, data.value);
          break;
        case 'counds':
          settingsManager.setSoundsOn(socket.userInfo.userId, data.value);
          break;
        case 'invisibility':
          settingsManager.setInvisibility(socket.userInfo.userId, data.value);

          if (data.value) {
            //If set invisibility = 1 emit users about his status and remove from public list
            socket.broadcast.emit('offline-users', [socket.userInfo.userId]);
            userManager.removeUserFromPublicList(socket.userInfo.userId);
          } else {
            socket.broadcast.emit('new-user', socket.userInfo);
            userManager.addUserToPublicList(socket.userInfo);
          }

          break;
      }
    });

    socket.on('block-user', function (data) {
      if (typeof socket.userInfo == 'undefined') return;

      settingsManager.addBlockedUser(socket.userInfo.userId, data.userId);
    });

    socket.on('unblock-user', function (data) {
      if (typeof socket.userInfo == 'undefined') return;

      settingsManager.removeBlockedUser(socket.userInfo.userId, data.userId);

      //If unblocked user is online emit "online-user"
      user = userManager.getUser(data.userId);
      if (user) {
        socket.emit('new-user', user.info);
      }
    });

    socket.on('blocked-users', function (data) {
      if (typeof socket.userInfo == 'undefined') return;
      if (typeof socket.userInfo.settings != 'undefined' && typeof socket.userInfo.settings.blockedUsers != 'undefined') {
        userManager.getUsers(socket.userInfo.settings.blockedUsers, true, function (blockedUsers) {
          var users = [];
          for (var userId in blockedUsers) {
            users.push(blockedUsers[userId].info);
          }

          socket.emit('blocked-users', users);
        });
      }
    });

    socket.on('conversation-read', function (data) {
      if (typeof socket.userInfo == 'undefined') return;
      messageManager.markAsRead(socket.userInfo.userId, data.userId);
    });

    socket.on('typing-start', function (data) {
      if (typeof socket.userInfo == 'undefined') return;

      user = userManager.getUser(data.userId);
      if (user) {
        for (var i = 0; i <= user.sockets.length - 1; i++) {
          io.sockets.socket(user.sockets[i]).emit('typing-start', {
            userId: socket.userInfo.userId
          });
        }
      }
    });

    socket.on('typing-end', function (data) {
      if (typeof socket.userInfo == 'undefined') return;

      user = userManager.getUser(data.userId);
      if (user) {
        for (var i = 0; i <= user.sockets.length - 1; i++) {
          io.sockets.socket(user.sockets[i]).emit('typing-end', {
            userId: socket.userInfo.userId
          });
        }
      }
    });

    socket.on('chat-with', function (data) {
      userManager.getUser(data.userId, true, function (user) {
        socket.emit('open-dialogs', {
          dialogs: [{
            userId: data.userId,
            userInfo: user.info
          }],
          activeDialog: data.userId
        });
      });
    });

    socket.on('disconnect', function () {
      if (!socket.userInfo) return;
      userManager.removeSocket(socket.userInfo.userId, socket.id);
    });
  });
}

exports.start = start;