var fs = require('fs');
var config = require('../configs/config.js').get('production');

function SettingsManager() {
  var settings = {};

  this.init = function () {
    console.log(config.common.userSettingsBackupPath);
    fs.readFile(config.common.userSettingsBackupPath, function (err, data) {
      if (err) return;
	  console.log(data);
      settings = JSON.parse(data);
      console.log(JSON.stringify(settings[73115]));
    });
  }

  this.addOpenDialog = function (userId, dialogId) {
    if (typeof settings[userId] == 'undefined') {
      settings[userId] = {};
    }
    if (typeof settings[userId]['openDialogs'] == 'undefined') {
      settings[userId]['openDialogs'] = [];
    }

    openDialogs = settings[userId]['openDialogs'];
    for (var i = 0; i <= openDialogs.length; i++) {
      if (openDialogs[i] == dialogId) {
        return;
      }
    }

    settings[userId]['openDialogs'].push(dialogId);
  }

  this.removeOpenDialog = function (userId, dialogId) {
    if (settings[userId] && settings[userId]['openDialogs']) {
      for (i in settings[userId]['openDialogs']) {
        if (settings[userId]['openDialogs'][i] == dialogId) {
          settings[userId]['openDialogs'].splice(i, 1);
        }
      }
    }
  }

  this.getOpenDialogs = function (userId) {
    if (typeof settings[userId] != 'undefined' && typeof settings[userId]['openDialogs'] != 'undefined' && settings[userId]['openDialogs'].length) {
      return settings[userId]['openDialogs']
    } else {
      return false;
    }
  }

  this.setActiveDialog = function (userId, dialogId) {
    sett = this.get(userId);
    sett['activeDialog'] = dialogId;
    this.set(userId, sett);
  }

  this.removeActiveDialog = function (userId) {
    sett = this.get(userId);
    sett['activeDialog'] = false;
    this.set(userId, sett);
  }

  this.getActiveDialog = function (userId) {
    sett = this.get(userId);
    if (typeof sett['activeDialog'] != 'undefined') {
      return sett['activeDialog'];
    } else {
      return false;
    }
  }

  //availability[1, 0]
  this.setAvailability = function (userId, availability) {
    if (availability != 0) {
      availability = 1;
    } else {
      availability = 0;
    }

    if (typeof settings[userId] == 'undefined') {
      settings[userId] = {};
    }
    settings[userId]['availability'] = availability;
  }

  this.getAvailability = function (userId) {
    if (typeof settings[userId] == 'undefined' || typeof settings[userId]['availability'] == 'undefined') {
      return false;
    } else {
      return settings[userId]['availability'];
    }
  }

  this.setShowOnlyEscorts = function (userId, value) {
    sett = this.get(userId);
    sett['showOnlyEscorts'] = value;
    this.set(userId, sett);
  }

  this.getShowOnlyEscorts = function (userId) {
    sett = this.get(userId);
    if (typeof sett['showOnlyEscorts'] != 'undefined') {
      return sett['showOnlyEscorts'];
    } else {
      return 0;
    }
  }

  this.setSoundsOn = function (userId, value) {
    sett = this.get(userId);
    sett['sounds'] = value;
    this.set(userId, sett);
  }

  this.getSoundsOn = function (userId) {
    sett = this.get(userId);
    if (typeof sett['getSoundsOn'] != 'undefined') {
      return sett['getSoundsOn'];
    } else {
      return 1;
    }
  }

  this.setKeepListOpen = function (userId, value) {
    sett = this.get(userId);
    sett['keepListOpen'] = value;
    this.set(userId, sett);
  }

  this.getKeepListOpen = function (userId) {
    sett = this.get(userId);
    if (typeof sett['keepListOpen'] != 'undefined') {
      return sett['keepListOpen'];
    } else {
      return 0;
    }
  }

  this.setInvisibility = function (userId, value) {
    sett = this.get(userId);
    sett['invisibility'] = value;
    this.set(userId, sett);
  }

  this.getInvisibility = function (userId) {
    sett = this.get(userId);
    if (typeof sett['invisibility'] != 'undefined') {
      return sett['invisibility'];
    } else {
      return 0;
    }
  }

  this.addBlockedUser = function (userId, blockedUserId) {
    sett = this.get(userId);

    if (typeof sett['blockedUsers'] == 'undefined') {
      sett['blockedUsers'] = [];
    }

    for (var i = 0; i <= sett['blockedUsers'].length; i++) {
      if (sett['blockedUsers'][i] == blockedUserId) {
        return;
      }
    }

    sett['blockedUsers'].push(blockedUserId);
  }

  this.removeBlockedUser = function (userId, blockedUserId) {
    sett = this.get(userId);

    if (sett['blockedUsers']) {
      for (i in sett['blockedUsers']) {
        if (sett['blockedUsers'][i] == blockedUserId) {
          sett['blockedUsers'].splice(i, 1);
        }
      }
    }
  }

  this.getBlockedUsers = function (userId) {
    sett = this.get(userId);

    if (typeof sett['blockedUsers'] != 'undefined' && sett['blockedUsers'].length) {
      return sett['blockedUsers']
    } else {
      return [];
    }
  }

  this.get = function (userId) {
    if (typeof settings[userId] == 'undefined') {
      settings[userId] = {};
      return {};
    } else {
      return settings[userId];
    }
  }

  this.set = function (userId, sett) {
    if (typeof settings[userId] == 'undefined') {
      settings[userId] = {};
    }
    settings[userId] = sett;

  }

  this.startSettingsBackup = function (callback) {

    setInterval(function () {
      fs.writeFile(config.common.userSettingsBackupPath, JSON.stringify(settings), function (err) {
        callback();
      });
    }, 60 * 60 * 1000);
  }

  this.init();
}

function get() {
  return new SettingsManager();
}

exports.get = get;