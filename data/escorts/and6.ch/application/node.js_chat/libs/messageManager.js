var mysql = require('mysql');
var config = require('../configs/config.js').get('production');
var redis = require('redis');

function MessageManager() {
	
	var client = mysql.createConnection({
		user:     config.db.user,
		database: config.db.database,
		password: config.db.password,
		host:     config.db.host,
		port: 3306
	});
	

	/*-->Reconnecting to mysql if connection lost*/
	//It happens sometimes. about once in 8 hours
	function handleDisconnect() {
		client.on('error', function(err) {
			if (! err.fatal ) {
				return;
			}
			
			if ( err.code !== 'PROTOCOL_CONNECTION_LOST' ) {
				console.log("The mysql library fired a PROTOCOL_CONNECTION_LOST exception");
				throw err;
			}
			
			console.log('Re-connecting lost connection: ' + err.stack);

			var client = mysql.createConnection({
				user:     config.db.user,
				database: config.db.database,
				password: config.db.password,
				host:     config.db.host,
				port: 3306
			});
			handleDisconnect(client);
			client.connect();
		});
	}

	handleDisconnect();
	/*<--Reconnecting to mysql if connection lost*/
	
	redisClient = redis.createClient();
	redisClient.on("error", function (err) {
		console.log("error event - " + redisClient.host + ":" + redisClient.port + " - " + err);
	});
	
	this.storeMessage = function(userIdFrom, userIdTo, message) 
	{
		getThreadId(userIdFrom, userIdTo, function(threadId) {
			console.log('Thread Found ' + threadId);
			insertMessage(userIdFrom, threadId, message);
		});
	}

	function getHistoryDate (threadId, userId, callback) {
		var sql = 'SELECT history_date FROM chat_history_cleared WHERE thread_id = ? AND user_id = ?';

		client.query(sql, [threadId, userId], function(error, data) {
			if (error) {
				console.log('error getting date from history');
				return false;
			}
			if (data.length) {
				callback(data[0].history_date.toMysqlFormat());
			} else {
				callback(null);
			}

		});
	}

	this.clearHistory = function(userId, threadId) {
		var sql1 = 'SELECT * FROM chat_history_cleared WHERE user_id = ? AND thread_id = ?';

		client.query(sql1, [userId, threadId], function(error, data) {
			var historyDate = new Date();
			var bind = null;
			if (data.length) {
				var sql2 = 'UPDATE chat_history_cleared SET ? WHERE user_id = ? AND thread_id = ?';
				bind = [ { user_id: userId, thread_id: threadId, history_date: historyDate.toMysqlFormat() }, userId, threadId ];
			} else {
				var sql2 = 'INSERT INTO chat_history_cleared SET ?';
				bind = { user_id: userId, thread_id: threadId, history_date: historyDate.toMysqlFormat() };
			}

			client.query(sql2, bind, function(error, data) {
				if (error) {
					console.log(error);
					console.log('Failed to clear the history for ' + userId);
				}
			});
		});
	}

	this.getMessagesThatWillBeVisibleFor = function (userId, participants, cb)
	{
		var result = [];
		var c = 0;

		for (let i = 0; i < participants.length; i++) {
			this.getConversation(userId, participants[i], null, function(messages) {
				if(typeof(messages) == "object" && messages.length) {
					result.push(participants[i]);
				}
				c++;

				if(c >= participants.length) {
					cb(result);
				}
			})
		}
	};

	this.getConversation = function(userId, participantId, dateFrom, callback)
	{
		var bind = [], sql;

		getAllThreadId(userId, participantId, function(ids) {

			var threadId = ids[ids.length - 1];

			if ( ! dateFrom ) {
				where = ' WHERE thread_id in (?) ';
				bind.push(ids);
			} else {
				where = ' WHERE thread_id in (?) AND (date >= ? OR read_date IS NULL )';
				bind.push(ids, dateFrom.toMysqlFormat());
			}

			if(userId == 8) {
				console.log("THREADID");
				console.log(threadId);
				console.log(ids);
			}

			getHistoryDate(threadId, userId, function(historyDate) {
				if (historyDate) {
					var t = historyDate.split(/[- :]/);
					var d = Date.UTC(t[0], t[1]-1, t[2], t[3], t[4], t[5]) - new Date().getTimezoneOffset()*60*1000;

					where += ' AND date > ? ';
					bind.push(new Date(d).toMysqlFormat());
				}
				
				sql = 'SELECT user_id AS userId, body, UNIX_TIMESTAMP(date) * 1000 AS date FROM chat_messages ' + where + ' ORDER BY id ASC';//getting timestamp in milliseconds

				client.query(sql, bind, function(error, messages) {
					if(userId == 8) {
						console.log(messages)
					}
					callback(messages, threadId, ids);
				});
			});
		});
	}
	
	this.getNewMessagesCount = function(userId, callback) 
	{
		sql = 'SELECT count(cm.id) count, cm.thread_id as threadId, u.id AS userId, u.username FROM chat_threads_participants ctp INNER JOIN chat_messages cm ON cm.thread_id = ctp.thread_id INNER JOIN users u ON u.id = cm.user_id WHERE ctp.user_id = ? AND cm.user_id <> ? AND read_date IS NULL GROUP BY cm.user_id';
		client.query(sql, [userId, userId], function(error, result) {
			callback(result);
		});
	}
	
	this.markAsRead = function(userId, participantId, callback)  
	{
		getAllThreadId(userId, participantId, function(ids) {

			date = new Date();
			client.query('UPDATE chat_messages SET read_date = ? WHERE thread_id in (?) AND user_id = ?', [date.toMysqlFormat(), ids, participantId], function(error, result) {
				if (typeof callback == 'function') callback();
			});
		});
	}
	
	this.detectUrl = function(message)
	{
		var exp = /([-a-zA-Z0-9]{2,256})\.[a-z]{2,50}\b/g;
		var matchUrl = message.match(exp);
		if(!!matchUrl && matchUrl != 'and6.com'){
			return true;
		}
		else{
			return false;
		}
	}
	
	this.clearMessage = function(message)
	{
		//htmlentities
		message = htmlEntities(message);
		
		//Replacing one more whitespaces
		message = message.trim();
		message = message.replace(/\s+/g, ' ');
		
		//replacing urls with html links
		/*var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
		message = message.replace(exp,"<a href='$1' target='_blank'>$1</a>"); */
		
		return message;
	}
	
	this.startBlackListedWordsSync = function(callback)
	{
		var self = this;
		setInterval(function() {
			self.syncBlackListedWords(callback);
		}, config.common.blWordsLifeTime);
	}
	
	this.syncBlackListedWords = function(callback)
	{
		sql = 'SELECT name, search_type FROM blacklisted_words WHERE types & 8';
		
		client.query(sql, function(error, result) {
			if ( error ) {
				console.log(error);
			} else {
				redisClient.select(config.common.redisBlWordsDb, function() {
					//Flush all blacklisted words then fill again
					redisClient.flushdb(function(err, res) {
						for(i = 0; i < result.length; i++) {
							redisClient.sadd(config.common.redisBlWordsKey, [result[i].name, result[i].search_type].join(','), function(err, res){
								if ( error ) {
									console.log('Error: ' + error);
								}
							});
						}
					});
				});
				callback();
			}
		});
	}
	
	this.checkMessage = function(message, callback)
	{
		redisClient.select(config.common.redisBlWordsDb, function() {
			redisClient.smembers(config.common.redisBlWordsKey, function(err, words) {
				matches = [];
				
				if ( err ) {
					return callback(matches);
				}
				
				for ( i = 0; i < words.length; i++ ) {
					word = words[i].split(',');
					
					type = word[1];
					word = word[0];
					
					match = false;
					
					if ( type == 1 ) { //Cont
						
						if ( message.indexOf(word) != -1 ) {
							match = true;
						}
					} else { //Exact
						m = message.match('/\b' + word + '\b/i');
						if ( m && m.length )  match = true;
					}
					
					if ( match ) {
						matches.push(word);
					}
				}
				callback(matches);
			});
		})
	}

	function getAllThreadId(userIdFrom, userIdTo, callback)
	{
		/*if(userIdFrom == 8) {
			console.log('----------------GETTING ALL THREAD IDS---------------------------------------');
			console.log([userIdFrom, userIdTo])
			console.log('SELECT tp1.thread_id AS threadId FROM chat_threads_participants tp1 INNER JOIN chat_threads_participants tp2 ON tp2.thread_id = tp1.thread_id AND tp2.user_id = ? WHERE tp1.user_id = ?')
			console.log('----------------!!!!!!!!!!!!!!!!!!!!!!---------------------------------------');
		}*/

		client.query(
			'SELECT tp1.thread_id AS threadId FROM chat_threads_participants tp1 INNER JOIN chat_threads_participants tp2 ON tp2.thread_id = tp1.thread_id AND tp2.user_id = ? WHERE tp1.user_id = ?',
			[userIdFrom, userIdTo],
			function(error, result) {
				if ( ! result || result.length == 0 ) {// If doesn't exist with this userIds pair insert
					client.query('INSERT INTO chat_threads VALUES()', function(error, result) {
						createThreadParticipants(result.insertId, userIdFrom, userIdTo, callback);
					});
				} else {
					var ids = [];
					for(var i = 0; i < result.length; i++) {
						ids.push(result[i].threadId)
					}
					callback(ids);
				}
			}
		);
	}
	
	function getThreadId(userIdFrom, userIdTo, callback)
	{
		client.query(
			'SELECT tp1.thread_id AS threadId FROM chat_threads_participants tp1 INNER JOIN chat_threads_participants tp2 ON tp2.thread_id = tp1.thread_id AND tp2.user_id = ? WHERE tp1.user_id = ?',
			[userIdFrom, userIdTo],
			function(error, result) {
				if ( ! result || result.length == 0 ) {// If doesn't exist with this userIds pair insert
					client.query('INSERT INTO chat_threads VALUES()', function(error, result) {
						createThreadParticipants(result.insertId, userIdFrom, userIdTo, callback);
					});
				} else {
					callback(result[0].threadId);
				}
			}
		);
	}
	
	function createThreadParticipants(threadId, userIdFrom, userIdTo, callback)
	{
		console.log("CREATING thread participants");
		client.query('INSERT INTO chat_threads_participants (user_id, thread_id) VALUES(?,?)', [userIdFrom, threadId], function(error, result) {
			if(error) {
				console.log('ERROR ON messageManager');
				console.log(error);
			}
			client.query('INSERT INTO chat_threads_participants (user_id, thread_id) VALUES(?,?)', [userIdTo, threadId], function(error, result) {
				if(error) {
					console.log('ERROR ON messageManager');
					console.log(error);
				}
				callback(threadId);
			});
		});
	}
	
	function insertMessage(userId, threadId, message)
	{
		date = new Date();
		client.query(
			'INSERT INTO chat_messages (user_id, thread_id, body, date) VALUES(?, ?, ?, ?)',
			[userId, threadId, message, date.toMysqlFormat()]
		);
	}
	
	
	function twoDigits(d) {
		if(0 <= d && d < 10) return "0" + d.toString();
		if(-10 < d && d < 0) return "-0" + (-1*d).toString();
		return d.toString();
	}
	
	function htmlEntities(str) {
		return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
	}
	
	Date.prototype.toMysqlFormat = function() {
		return this.getUTCFullYear() + "-" + twoDigits(1 + this.getUTCMonth()) + "-" + twoDigits(this.getUTCDate()) + " " + twoDigits(this.getUTCHours()) + ":" + twoDigits(this.getUTCMinutes()) + ":" + twoDigits(this.getUTCSeconds());
	};
}

function get() {
	return new MessageManager();
}
exports.get = get;