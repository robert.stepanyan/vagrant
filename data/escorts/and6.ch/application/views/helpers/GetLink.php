<?php

class Zend_View_Helper_GetLink
{
	protected static $_params;
	
	public function getLink($type = '', array $args = array(), $clean = false, $no_args = false)
	{
		$lang_id = Cubix_I18n::getLang();

		$link = '/';
//		if ( $lang_id != Cubix_Application::getDefaultLang() ) {
//			$link .= $lang_id . '/';
//		}
		
		if ( isset($args['state']) && 7 != Cubix_Application::getId() ) {
			$args['region'] = $args['state'];
			unset($args['state']);
		}

		foreach ($args as $k => $v) {
		    if($v == 'undefined') {
		        unset($args[$k]);
            }
        }
		
		switch ( $type ) {
			case 'latest-actions-index':
				$link .= 'latest-actions';
				break;
			case 'latest-actions-ajax-header':
				$link .= 'latest-actions/ajax-header?ajax';
				break;
			case 'latest-actions-ajax-list':
				$link .= 'latest-actions/ajax-list?ajax';
				break;
			case 'latest-actions-ajax-get-details':
				$link .= 'latest-actions/ajax-get-details?ajax';
				break;
			case 'online-billing':
				$link .= 'online-billing-v2';
				break;
			case 'ajax-add-package':
				$link .= 'online-billing/ajax-add-package';
				break;
			case 'ajax-shopping-cart':
				$link .= 'online-billing/ajax-get-shopping-cart';
				break;
			case 'ajax-shopping-cart-remove':
				$link .= 'online-billing/ajax-remove-from-shopping-cart';
				break;
			case 'ajax-change-city':
				$link .= 'online-billing/ajax-change-city';
				break;
			case 'glossary':
				$link .= 'glossary';
				break;
			case 'ob-checkout':
				$link .= 'online-billing/checkout';
				break;
			case 'ob-order-history':
				$link .= 'online-billing/order-history';
				break;
			case 'ob-ajax-order-history':
				$link .= 'online-billing/ajax-get-order-history';
				break;
			case 'ob-successful':
				$link .= 'online-billing/successful-payment';
				break;
			case 'ob-unsuccessful':
				$link .= 'online-billing/unsuccessful-payment';
				break;
			case 'online-billing-v2':
				$link .= 'online-billing-v2';
				break;
			case 'ob-successful-v2':
				$link .= 'online-billing-v2/successful-payment';
				break;
			case 'ob-unsuccessful-v2':
				$link .= 'online-billing-v2/unsuccessful-payment';
				break;
			case 'online-billing-v2-wizard':
				$link .= 'online-billing-v2/';
				if ( $args['user_type'] == USER_TYPE_AGENCY ) {
					$link .= 'wizard-agency';
				} else {
					$link .= 'wizard-independent';
				}
				unset($args['user_type']);
				break;
			case 'online-billing-payment':
				$link .= 'online-billing-v2/payment';
				break;
			case 'ajax-show-ad-place':
				$link .= 'online-billing/ajax-show-ad-place';
				break;
			case 'online-billing-v3':
				$link .= 'online-billing-v3/index';
				break;
			case 'online-billing-v3-select-escort':
				$link .= 'online-billing-v3/select-escort';
				break;
			case 'online-billing-v3-select-package':
				$link .= 'online-billing-v3/select-package';
				break;
			case 'online-billing-v3-select-optional-products':
				$link .= 'online-billing-v3/select-optional-products';
				break;
			case 'online-billing-v4-payment':
				$link .= 'online-billing-v4/payment';
				break;
			case 'online-billing-v4-success':
				$link .= 'online-billing-v4/success';
				break;
			case 'online-billing-v4-failure':
				$link .= 'online-billing-v4/failure';
				break;
			case 'online-billing-v4':
				$link .= 'online-billing-v4/index';
				break;
			case 'online-billing-v4-select-escort':
				$link .= 'online-billing-v4/select-escort';
				break;
			case 'online-billing-v4-select-package':
				$link .= 'online-billing-v4/select-package';
				break;
            case 'jobs_rent':
                $link .= 'classified-ads/place-ad';
                break;
			case 'online-billing-v4-select-optional-products':
				$link .= 'online-billing-v4/select-optional-products';
				break;
			case 'online-billing-v4-payment':
				$link .= 'online-billing-v4/payment';
				break;
			case 'online-billing-v4-failure':
				$link .= 'online-billing-v4/failure';
				break;
			case 'return_false':
				$link = 'return false;';
				break;
			case 'romandie':
				$link .= 'region-romandie';
			break;
			case 'forum':
				$link = 'http://www.escort-annonce.com/forum/?lang=' . $lang_id;
				break;
			case 'chat':
				$link = 'http://chat.escort-annonce.com/';
				break;
			case 'cams':
				$link = 'http://escort-annonce.streamray.com/';
				break;
			case 'search':
				$link .= 'search';
				break;
            case 'new-banner-home-desktop':
                $link = 'https://www.bag.admin.ch/bag/de/home/krankheiten/ausbrueche-epidemien-pandemien/aktuelle-ausbrueche-epidemien/novel-cov/massnahmen-des-bundes.html#617891184';
                break;
			case 'virtual':
				$link .= 'escorts/virtual';
				break;
			case 'escorts-url':
				$link .= 'escorts';
				break;
			case 'escorts-list':
				$link .= 'escorts';
			break;
			case 'base-city':
				$request = Zend_Controller_Front::getInstance()->getRequest();
				$region = $request->region;
				$section = isset($args['section']) ? $args['section'] . '/' : '';
				if ( strlen($section) ) unset($args['section']);

				$link .= 'escorts/region_' . $region . '/' . $section . 'city_' . $args['city'];
				unset($args['city']);
			break;
			case 'escorts':
				$request = Zend_Controller_Front::getInstance()->getRequest();

                if ( empty(self::$_params) ) {
					$req = trim($request->getParam('category', ''), '/');
					
					if($request->getParam('outcall')){
						$link .= 'escorts/';
					}

					$req = explode('/', $req);
					
					$params = array();


                    foreach ($req as $r)  {
                        if($r == 'undefined') continue;
						$param = explode('_', $r);

                        if ( count($param) < 2 ) {
							$params[] = $r;
							continue;
						}
						
						$param_name = reset($param);
						array_shift($param);

                        if ($param_name == 'undefined') continue;
                        $params[$param_name] = implode('_', $param);
					}

					if ( $request->getParam('city') ) {
						$params['city'] = $request->getParam('city');
					}
					if ( $request->getParam('city_id') ) {
						$params['city_id'] = $request->getParam('city_id');
					}
					self::$_params = $params;
				}

                if ( $clean ) {
					$params = array();
				}
				else {
					$params = self::$_params;
				}


                foreach ( $args as $key => $value ) {
                    if ( is_null($value) || $value == 'undefined') {
						unset($params[$key]);
						continue;
					}
					
					$params[$key] = $value;
				}

               // $link .= 'escorts/';
				
				if ( $params['region'] ) {
					$host = explode('.', $request->getHttpHost());
					$subdomain = $params['region'];
					if ( $subdomain == 'deutschschweiz' || $subdomain == 'romandie') {
						$subdomain = 'www';
					}
					$host[0] = $subdomain;
					$link = 'http://' . implode('.', $host) . '/';

					unset($params['region']);
				}
				if  ( $params['gender_page']) {
					if($params['gender_page'] == 'escorts'){
						$link .=  $params['gender_page'] . '/';
					}
					elseif( $params['gender_page'] != 'main' && $request->region != 'romandie' ){
						$link .= 'escorts/' . $params['gender_page'] . '/';
					}
					unset($params['gender_page']);
				}

				$romandie_non_dynemic_urls = array( 'boys', 'trans', 'nuove', 'virtual','regular' , 'massage','happy-hours' );
				if($request->region == 'romandie' && is_null($params['city_id']) && !in_array($params[0], $romandie_non_dynemic_urls) ){
					$link .= 'region-romandie/';
				}else{
					if  ( $params['city_id'] ) {
							$params['city_slug'] = $params['city'] . '-c' . $params['city_id'] . '/';
							unset($params['city']);
							unset($params['city_id']);
					}

					foreach ( $params as $param => $value ) {
						if ( ! strlen($param) || ! strlen($value)) continue;
						if ( is_int($param) ) {
							if ( $value == 'tours' ) $value = 'citytours';
							$link .= 'escorts/' . $value . '/';
						}
						elseif ( strlen($param) ) {
							if ( $param == 'city_slug' ) {
								$link .= $params['city_slug'];
								unset($params['city_slug']);
							}
							elseif ( $param == 'page' ) {
								if ( $value - 1 > 0 ) {
									$link .= ($value - 1) . '/';
								}
							} 
							else {
								$link .= $param . '_' . $value . '/';
							}
						}
					}
				}

                $link = rtrim($link, '/');
				$args = array();
			break;
            case 'agencies':
                $link .= 'agencies';
                break;
            case 'agencies-cities':
                $link .= 'agencies/city/' . $args['city_id'];
                unset($args['city_id']);
                break;
			case 'club_directory_map':
				$link .= 'club-directory/map';
				break;
			case 'club_directory':
				$link .= 'club-directory';
				break;
			case 'gotm':
				$link .= 'girl-of-month';

				if ( isset($args['history']) && $args['history'] ) {
					$link .= '/history';
				}
				
				if ( isset($args['page']) ) {
					if ( $args['page'] != 1 ) {
						$link .= '/page_' . $args['page'];
						//$link .= '/page_' . $args['page'];
					}
					
					unset($args['page']);
				}
				break;
			case 'profile':
				$link .= 'sex-model/' . $args['showname'] . '-' . $args['escort_id'];
				unset($args['showname']);
				unset($args['escort_id']);
			break;
			case 'profile-list':
				$link .= 'sex-model/' . $args['showname'] . '-' . $args['escort_id'] . '?from=' . $args['list_type'];
				unset($args['showname']);
				unset($args['escort_id']);
				unset($args['list_type']);
			break;
			case 'forgot':
				$link .= 'private/forgot';
			break;
			case 'signup':
				$link .= 'private/signup';
				
				// if ( ! isset($args['type']) ) {
				// 	$args['type'] = 'member';
				// }
				
				// $link .= $args['type'];
			break;
			case 'signin':
				$link .= 'private/signin';
			break;
			case 'signout':
				$link .= 'private/signout';
			break;
			case 'private':
				$link .= 'private';
			break;
			case 'membership-type':
				$link .= 'private/membership-type';
			break;
			case 'feedback':
				$link .= 'feedback';
			break;
            case 'feedback-ads':
                $link .= 'feedback-ads';
                break;
			case 'feedback-agency':
				$link .= 'feedback-agency';
			break;
			case 'contact':
				$link .= 'contact';
			break;
			case 'add-review-comment':
				$link .= 'private-v2/add-review-comment';
			break;
			case 'captcha':
				$link .= 'captcha?' . rand();
				//$link = '/img/sample_captcha.gif?' . rand();
			break;
			case 'viewed-escorts':
				$link .= 'escorts/viewed-escorts';
			break;
			case '100p-verify':
				$link .= 'private/verify';
			break;
			case '100p-verify-webcam':
				$link .= 'private/verify/webcam';
			break;
			case '100p-verify-idcard':
				$link .= 'private/verify/idcard';
			break;
			case 'agency':
				$link .= 'agency/' . $args['slug'] . '-' . $args['id'];
				unset($args['slug']);
				unset($args['id']);
			break;
			
			case 'favorites':
				$link .= 'private/favorites';
			break;
                    
                        case 'faq':
				$link .= 'faq';
			break;
            case 'video':
                $link .= 'video';
                break;

            case 'video-example':
                $link .= 'video-example';
                break;
            case 'video-voting-info':
                $link .= 'plain/video-voting';
                break;
			case 'add-to-favorites':
				$link .= 'private/add-to-favorites';
			break;
			case 'remove-from-favorites':
				$link .= 'private/remove-from-favorites';
			break;
			
			// Private Area
            case 'simple-profile':
				$link .= 'private-v2/profile/simple';
				break;
			case 'fast-profile':
				$link .= 'fast-profile';
				break;
			case 'edit-profile':
				$link .= 'private/profile';
			break;
			case 'edit-agency-profile':
				$link .= 'private/agency-profile';
			break;
			case 'profile-data':
				$link .= 'private/profile-data';
			break;
			case 'edit-photos':
				$link .= 'private/photos';
			break;
			case 'edit-rates':
				$link .= 'private/rates';
			break;
			case 'edit-escorts':
				$link .= 'private/escorts';
			break;
			case 'delete-escort':
				$link .= 'private/delete-escort';
			break;
			case 'edit-tours':
				$link .= 'private/tours';
			break;
			case 'change-passwd':
				$link .= 'private/change-password';
			break;
			
			case 'search':
				$link .= 'search';
			break;

			case 'late-night-girls':
				$link .= 'escorts/late-night-girls';
			break;
			
			case 'terms':
				$link .= 'page/terms-and-conditions';
			break;
			case 'webcam-lm':
				$link .= 'page/webcam-verification-learn-more';
			break;
			case 'passport-lm':
				$link .= 'page/passport-verification-learn-more';
			break;
			
			case 'external-link':
				$link = '/go?' . $args['link'];
				unset($args['link']);
			break;
			case 'ixs-banner-url':
				$link = Cubix_Application::getById()->url.'/images/ixsGovazd/'.$args['filename'];
				unset($args['filename']);
			break;
			
			// V2 Private Area
			case 'private-v2':
				$link .= 'private-v2';
				break;
			case 'private-v2-settings':
				$link .= 'private-v2/settings';
				break;
			case 'private-v2-statistics':
				$link .= 'private-v2/statistics';
				break;
			case 'private-v2-happy-hour':
				$link .= 'private-v2/happy-hour';
				break;
			case 'private-v2-client-blacklist':
				$link .= 'private-v2/client-blacklist';
				break;
			case 'private-v2-add-client-to-blacklist':
				$link .= 'private-v2/add-client-to-blacklist';
				break;
			case 'private-v2-profile':
				$link .= 'private-v2/profile';
				break;
			case 'private-v2-advertise-now':
				$link .= 'private-v2/profile/advertise-now';
				break;
			case 'private-v2-advertise-success':
				$link = 'http://' . $_SERVER['SERVER_NAME'] . '/private-v2/profile/advertise-success';
				break;
			case 'private-v2-advertise-failure':
				$link = 'http://' . $_SERVER['SERVER_NAME'] . '/private-v2/profile/advertise-failure';
				break;
			case 'private-v2-advertise-cancel':
				$link = 'http://' . $_SERVER['SERVER_NAME'] . '/private-v2/profile/simple';
				break;
			case 'private-v2-gotd-agency':
				// $link .= 'private-v2/profile/gotd';
				$link .= 'online-billing-v4/select-escort?gotd=1';
				break;
			case 'private-v2-votd-agency':
				// $link .= 'private-v2/profile/gotd';
				$link .= 'online-billing-v4/select-escort?votd=1';
				break;
			case 'private-v2-gotd':
				// $link .= 'private-v2/profile/gotd';
				$link .= 'online-billing-v4/select-package?gotd=1';
				break;
			case 'private-v2-votd':
				// $link .= 'private-v2/profile/gotd';
				$link .= 'online-billing-v4/select-package?votd=1';
				break;
			case 'private-v2-gotd-success':
				$link = 'http://' . $_SERVER['SERVER_NAME'] . '/private-v2/profile/gotd-success';
				break;
			case 'private-v2-gotd-failure':
				$link = 'http://' . $_SERVER['SERVER_NAME'] . '/private-v2/profile/gotd-failure';
				break;
			case 'private-v2-gotd-cancel':
				$link = 'http://' . $_SERVER['SERVER_NAME'] . '/private-v2/profile/gotd';
				break;
			case 'private-v2-photos':
				$link .= 'private-v2/photos';
				break;
			case 'profile-gallery':
				$link .= 'profile/gallery';
				break;
			case 'private-v2-plain-photos':
				$link .= 'private-v2/plain-photos';
				break;
			case 'private-v2-agency-plain-photos':
				$link .= 'private-v2/agency-plain-photos';
				break;
			case 'private-v2-tours':
				$link .= 'private-v2/tours';
				break;
			case 'private-v2-ajax-tours':
				$link .= 'private-v2/ajax-tours';
				break;
			case 'private-v2-ajax-tours-add':
				$link .= 'private-v2/ajax-tours-add';
				break;
			case 'private-v2-ajax-tours-remove':
				$link .= 'private-v2/ajax-tours-remove';
				break;
			case 'private-v2-verify':
				$link .= 'private-v2/verify';
				break;
			case 'private-v2-faq':
				$link .= 'private-v2/faq';
				break;
			case 'private-v2-premium':
				$link .= 'private-v2/premium';
				break;
			case 'private-v2-add-areas':
				$link .= 'private-v2/areas';
				break;
			case 'private-v2-support':
				$link .= 'support';
				break;
			case 'private-v2-get-rejected-verification':
				$link .= 'private-v2/get-rejected-verification';
				break;
			case 'private-v2-set-rejected-verification':
				$link .= 'private-v2/set-rejected-verification';
				break;
            case 'skype-bookings':
                $link .= 'private-v2/skype-bookings';
                break;
			case 'alerts':
				$link .= 'private-v2/alerts';
				break;
			case 'private-v2-agency-profile':
				$link .= 'private-v2/agency-profile';
				break;
			case 'private-v2-member-profile':
				$link .= 'private-v2/member-profile';
				break;
			case 'private-v2-upgrade-premium':
				$link .= 'private-v2/upgrade';
				break;
			case 'ticket-open':
				$link .= 'support/ticket';
				break;
			case 'private-v2-escorts':
				$link .= 'private-v2/escorts';
				break;
            case 'private-v2-escorts-delete':
				$link .= 'private-v2/profile-delete';
				break;
            case 'private-v2-escorts-restore':
				$link .= 'private-v2/profile-restore';
				break;
			case 'private-classified-ads':
				$link .= 'private-v2/classified-ads';
				break;
			case 'private-classified-ads-ad':
				$link .= 'private-v2/classified-ads-ad';
				break;
			case 'private-classified-ads-ad-delete-confirm':
				$link .= 'private-v2/classified-ad-delete-confirm';
				break;
			case 'private-classified-ads-ad-delete':
				$link .= 'private-v2/classified-ad-delete';
				break;
			case 'private-classified-ads-make-premium':
				$link .= 'private-v2/classified-ads-make-premium';
				break;
			case 'private-classified-ads-premium-success':
				$link .= 'private-v2/classified-ads-premium-success';
				break;
			case 'private-classified-ads-premium-error':
				$link .= 'private-v2/classified-ads-premium-error';
				break;
			case 'private-classified-ads-twispay-response':
                $link .= 'private-v2/twispay-response';
                break;
			case 'private-classified-ads-ajax-list':
                $link .= 'private-v2/classified-ads-ajax-list?ajax';
                break;
			case 'reviews':
				$link .= 'evaluations';
				break;
			case 'escort-reviews':
				$link .= 'evaluations/' . $args['showname'] . '-' . $args['escort_id'];
				unset ($args['showname']);
				unset ($args['escort_id']);
				break;
			case 'member-reviews':
				$link .= 'evaluations/member/' . $args['username'];
				unset ($args['username']);
				break;
			case 'escorts-reviews':
				$link .= 'evaluations/escorts';
				break;
			case 'agencies-reviews':
				$link .= 'evaluations/agencies';
				break;
			case 'top-reviewers':
				$link .= 'evaluations/top-reviewers';
				break;
			case 'top-ladies':
				$link .= 'evaluations/top-ladies';
				break;
			case 'voting-widget':
				$link .= 'escort/' . $args['showname'] . '-' . $args['escort_id'] . '/vote';
				unset($args['showname']);
				unset($args['escort_id']);
				break;
			case 'private-v2-reviews':
				$link .= 'private-v2/reviews';
				break;
			case 'private-v2-escort-reviews':
				$link .= 'private-v2/escort-reviews';
				break;
			case 'private-v2-escorts-reviews':
				$link .= 'private-v2/escorts-reviews';
				break;
			case 'private-v2-add-review':
				$link .= 'private-v2/add-review';
				break;
			case 'reviews-search':
				$link .= 'evaluations/search';
				break;
            case 'comments':
				$link .= 'comments/' . $args['reg'];
				unset($args['reg']);
				break;
			case 'member-info':
				$link .= 'member/'.$args['username'];
				unset($args['username']);
				break;
			case 'member-comments':
				$link .= 'members/get-member-comments';
				break;
			case 'tell-friend':
				$link .= 'escorts/ajax-tell-friend';
				break;
			case 'report-problem':
				$link .= 'escorts/ajax-report-problem';
				break;
			case 'get-filter':
				$link .= 'escorts/get-filter?ajax_lng=' . $lang_id;
				break;
			case 'switch-agency':
				$link .= 'private/switch-agency';
				break;
			case 'all-clubs':
				$link .= 'all-clubs';
				break;
            case 'classified-ads-index':
                $link .= 'classified-ads';
                break;
            case 'classified-ads-ad':
                $link .= 'classified-ads/ad';
                break;
            case 'classified-ads-place-ad':
                $link .= 'classified-ads/place-ad';
                break;
            case 'classified-ads-success':
                $link .= 'classified-ads/success';
                break;
            case 'classified-ads-error':
                $link .= 'classified-ads/error';
                break;
            case 'classified-ads-twispay-response':
                $link .= 'classified-ads/twispay-response';
                break;
            case 'classified-ads-ajax-filter':
                $link .= 'classified-ads/ajax-filter?ajax';
                break;
            case 'classified-ads-ajax-list':
                $link .= 'classified-ads/ajax-list?ajax';
                break;
            case 'classified-ads-print':
                $link .= 'classified-ads/print';
                break;
            case 'classified-ads-ad':
                $link .= 'classified-ads/ad';
                break;
            case 'skype-available-listing':
                $link .= 'escorts/skype-available';
                break;
			case 'video-listing':
				$request = Zend_Controller_Front::getInstance()->getRequest();
								
				$link .= 'escorts/videos/';
				
				
				if  ( $args['city_id'] ) {
					$link .= $args['city'] . '-c' . $args['city_id'] . '/';
					unset($args['city']);
					unset($args['city_id']);
				}
				
				/*foreach ( $params as $param => $value ) {
					if ( ! strlen($param) || ! strlen($value)) continue;
					if ( is_int($param) ) {
						$link .= 'escorts/videos/' . $value . '/';
					}
					elseif ( strlen($param) ) {
						if ( $param == 'page' ) {
							if ( $value - 1 > 0 ) {
								$link .= ($value - 1) . '/';
							}
						} 
						else {
							$link .= $param . '_' . $value . '/';
						}
					}
				}*/
				
				$link = rtrim($link, '/');
				
				$args = array();
				break;
			case 'my-video':
				$link .= 'video/';
				break;
            case 'jobs':
                $link .= 'classified-ads?category=5';
                break;
            case 'rent':
                $link .= 'classified-ads?category=6';
                break;
			case 'private-v2-video':
				$link .= 'video';
				break;
			case 'private-v2-video-action':
				$link .= 'video/do';
				break;
			case 'private-v2-video-ready':
				$link .= 'video/is-video-ready';
				break;
			case 'video-chat-success':
				$link .= 'escorts/video-chat-success';
				break;
			case 'video-chat-failure':
				$link .= 'escorts/video-chat-failure';
				break;
			case 'buy-credits':
				$link .= 'private-v2/buy-credits';
				break;
			case 'and6-cams':
				$link .= 'and6-cams';
			break;
			case 'and6-cams-buy-tokens':
				$link .= 'and6-cams/buy-tokens';
			break;
			case 'and6-cams-payment-response':
				$link = 'https://www.and6.com/and6-cams/payment-response';
			break;	
			case 'and6-cams-escort-dash':
				$link .= 'and6-cams/broadcaster/dashboard';
			break;	
			
			case 'cams-login-redirect':
				$link = 'https://and6.escortcams.com';
				if ($args['path']) {
					$link .= $args['path'];
				}
				unset($args['path']);
			break;
			
			case 'cams-broadcaster':
				$encoded_showname = strtolower(str_replace(' ', '-', $args['showname']));
				$link .= 'and6-cams/channel/' . $encoded_showname.'-'.$args['escort_id'];
				unset($args['showname']);
				unset($args['escort_id']);
			break;	
			
			case '/':
				$link = '/';
				break;
			case null:
				return $this;

		}

		if ( ! $no_args ) {
			if ( count($args) ) {
				$link .= '?';
				$params = array();
				foreach ( $args as $arg => $value ) {
					if ( ! is_array($value) ) {
						$params[] = $arg . '=' . urlencode($value);
					}
					else {
						foreach ( $value as $v ) {
							$params[] = $arg . '[]=' . $v;
						}
					}
				}

				$link .= implode('&', $params);
			}
		}
        $link = preg_replace('/undefined\/?/','', $link);
		return $link;
	}
	
	public function getParams()
	{
		return self::$_params;
	}
}
