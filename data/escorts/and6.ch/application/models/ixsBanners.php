<?php
class Model_ixsBanners extends Cubix_Model
{
	public function insert($images, $zone){
		
		$sql = "INSERT INTO ixs_banners ( `id`, `ixs_id`, `zone_id`, `zone_rotate`, `zone_rotation`,
										 `banner_id`, `filename`, `target`,`title` , `link`, `cmp_id`,
										 `user_id`, `size`, `md5_hash`)";
		$sql_value = 'VALUES ';	

		foreach ($images as $image) {
			
			$image_arr = (array) $image;
			$image_arr['zone_id'] = $zone;
		
			$image_data[0] = (isset($image_arr['id']) ? $image_arr['id']  : '' );
			$image_data[1] = (isset($image_arr['zone_id']) ? $image_arr['zone_id']  : '' );
			$image_data[2] = (isset($image_arr['zone_rotate']) ? $image_arr['zone_rotate']  : '' );
			$image_data[3] = (isset($image_arr['zone_rotation']) ? $image_arr['zone_rotation']  : '' );
			$image_data[4] = (isset($image_arr['banner_id']) ? $image_arr['banner_id']  : '' );
			$image_data[5] = (isset($image_arr['filename']) ? $image_arr['filename']  : '' );
			$image_data[6] = (isset($image_arr['target']) ? $image_arr['target']  : '' );
			$image_data[7] = (isset($image_arr['title']) ? $image_arr['title']  : 'notitle' );
			$image_data[8] = (isset($image_arr['link']) ? $image_arr['link']  : '' );
			$image_data[9] = (isset($image_arr['cmp_id']) ? $image_arr['cmp_id']  : '' );
			$image_data[10] = (isset($image_arr['user_id']) ? $image_arr['user_id']  : '' );
			$image_data[11] = (isset($image_arr['size']) ? $image_arr['size']  : '' );
			$image_data[12] = (isset($image_arr['md5_hash']) ? $image_arr['md5_hash']  : '' );
		
			$sql_value .='(null,';
			//$last_key = end(array_keys($image));
			
			foreach ($image_data as $key => $value) {
				$sql_value .= '"'.$value.'"';
				if ($key < 12) {
			        $sql_value .= ',';
			    }
			}
			$sql_value .='),';

			unset($image_data[$key]);
		}
			
		$sql .= $sql_value;

		
		try{
			$this->getAdapter()->query(rtrim($sql, ','));
			return TRUE;
		}catch(Exception $e ){
			 var_dump($e->getMessage());
		}
	}
	
	static public function get($zone_id = null, $user_id = null, $size = null, $cache = true, $count = 0)
	{
		$where = array(
			'ixs.zone_id = ?' => $zone_id,
			'ixs.user_id = ?' => $user_id,
			'ixs.size = ?' => $size
		);
		
		$where = self::getWhereClause($where, true);
		
		$sql = '
			SELECT *
			FROM ixs_banners ixs
			' . (!is_null($where) ? 'WHERE ' . $where : '');

		if( is_int($count) && $count){
		 	$sql .= ' ORDER BY RAND() LIMIT '. $count;
		}

		if( $cache ){
			$cache = Zend_Registry::get('cache');
			$cache_key = 'ixs_banners_'.$zone_id.'_'.$user_id.'_'.$size ;	

			if ( ! $result = $cache->load( $cache_key ) ) {
				$result = parent::_fetchAll($sql);
				$cache->save($result, $cache_key, array());
			}
		}else{
			
			$result = parent::_fetchAll($sql);
		}
		
		
		return $result;
	}

	static public function getbyarray($zones, $cache = true, $count = 0)
	{
		if(!is_array($zones)){die;}
		$zones_string = implode(',', $zones);

		$sql = '
			SELECT *
			FROM ixs_banners ixs where ixs.zone_id in ('.$zones_string.')';
		
		if( is_int($count) && $count){
		 	$sql .= ' ORDER BY RAND() LIMIT '. $count;
		}

		if( $cache ){
			$cache = Zend_Registry::get('cache');
			$cache_key = 'ixs_banners_'.$zone_id.'_'.$user_id.'_'.$size ;	

			if ( ! $result = $cache->load( $cache_key ) ) {
				$result = parent::_fetchAll($sql);
				$cache->save($result, $cache_key, array());
			}
		}else{
			
			$result = parent::_fetchAll($sql);
		}
		
		
		return $result;
	}

	public static function getForCityPage($zone_name = null, $city_id = null, $rand_count = false)
	{
		if ( ! $zone_name || ! $city_id ) return ""; 

		$banner_mapping = array(
			'right_banners_175_375' => array(
				//deutschschweiz				
				17 => 'x313531', //zurich-umgebung
				18 => 'x313435', //wthur-thurgau-st-gallen
				19 => 'x3835', //basel-und-umgebung
				20 => 'x313330', //mittelland-aargau
				21 => 'x3931', //bern-city
				22 => 'x3934', //bern-umgeb-oberland
				23 => 'x3937', //biel-bienne-grenchen				
				24 => 'x313234', //luzern-innerschweiz				
				25 => 'x313132', //graub-nden-glarus				
				26 => 'x313438', //wallis				
				44 => 'x313333', //mittelland-solothurn
				45 => 'x313533', //zurich-city

				//romandie
				28 => 'x313039', //geneve-et-region				
				29 => 'x313135', //lausanne-renens-moudon				
				30 => 'x313336', //montreux-vevey-aigle				
				31 => 'x313237', //sierre-sion-martigny-bas-valais				
				32 => 'x313339', //neuchatel-et-region-jura				
				33 => 'x313432', //vaud-payerne-yverdon				
				34 => 'x313036', //fribourg-bulle
				35 => 'x313030', //bienne

				//ticino
				36 => 'x3838', //bellinzona
				37 => 'x313138', //locarno
				38 => 'x313231', //lugano
				39 => 'x313033', //chiasso
				42 => 'x313533', //cadenazzo [NO ZONE]
				43 => 'x313533', //riazzino [NO ZONE]
			),
			'right_banners_175_250' => array(
				//deutschschweiz				
				17 => 'x313530', //zurich-umgebung				
				18 => 'x313434', //wthur-thurgau-st-gallen				
				19 => 'x3834', //basel-und-umgebung				
				20 => 'x313239', //mittelland-aargau				
				21 => 'x3930', //bern-city				
				22 => 'x3933', //bern-umgeb-oberland				
				23 => 'x3936', //biel-bienne-grenchen				
				24 => 'x313233', //luzern-innerschweiz				
				25 => 'x313131', //graub-nden-glarus				
				26 => 'x313437', //wallis
				44 => 'x313332', //mittelland-solothurn
				45 => 'x313534', //zurich-city [zurich nord]

				//romandie
				28 => 'x313038', //geneve-et-region				
				29 => 'x313134', //lausanne-renens-moudon
				30 => 'x313335', //montreux-vevey-aigle				
				31 => 'x313236', //sierre-sion-martigny-bas-valais				
				32 => 'x313338', //neuchatel-et-region-jura				
				33 => 'x313431', //vaud-payerne-yverdon				
				34 => 'x313035', //fribourg-bulle
				35 => 'x3939', //bienne

				//ticino
				36 => 'x3837', //bellinzona
				37 => 'x313137', //locarno
				38 => 'x313230', //lugano				
				39 => 'x313032', //chiasso
				42 => 'x313533', //cadenazzo [NO ZONE]
				43 => 'x313533', //riazzino [NO ZONE]
			),
			'right_banners_175_125' => array(
				//deutschschweiz				
				17 => 'x313439', //zurich-umgebung
				18 => 'x313433', //wthur-thurgau-st-gallen				
				19 => 'x3833', //basel-und-umgebung
				20 => 'x313238', //mittelland-aargau				
				21 => 'x3839', //bern-city
				22 => 'x3932', //bern-umgeb-oberland				
				23 => 'x3935', //biel-bienne-grenchen				
				24 => 'x313232', //luzern-innerschweiz								
				25 => 'x313130', //graub-nden-glarus
				26 => 'x313436', //wallis
				44 => 'x313331', //mittelland-solothurn
				45 => 'x313532', //zurich-city

				//romandie
				28 => 'x313037', //geneve-et-region				
				29 => 'x313133', //lausanne-renens-moudon				
				30 => 'x313334', //montreux-vevey-aigle
				31 => 'x313235', //sierre-sion-martigny-bas-valais				
				32 => 'x313337', //neuchatel-et-region-jura
				33 => 'x313430', //vaud-payerne-yverdon				
				34 => 'x313034', //fribourg-bulle
				35 => 'x3938', //bienne

				//ticino
				36 => 'x3836', //bellinzona
				37 => 'x313136', //locarno
				38 => 'x313139', //lugano				
				39 => 'x313031', //chiasso
				42 => 'x313533', //cadenazzo [NO ZONE]
				43 => 'x313533', //riazzino [NO ZONE]
			),
			'mobile_banners_265_82' => array(
				//deutschschweiz				
				17 => 'x3639', //zurich-umgebung
				18 => 'x3637', //wthur-thurgau-st-gallen				
				19 => 'x3531', //basel-und-umgebung
				20 => 'x3631', //mittelland-aargau				
				21 => 'x3533', //bern-city
				22 => 'x3132', //bern-umgeb-oberland				
				23 => 'x3133', //biel-bienne-grenchen				
				24 => 'x3630', //luzern-innerschweiz								
				25 => 'x3536', //graub-nden-glarus
				26 => 'x3638', //wallis
				44 => 'x3632', //mittelland-solothurn
				45 => 'x3730', //zurich-city

				//romandie
				28 => 'x3535', //geneve-et-region				
				29 => 'x3537', //lausanne-renens-moudon				
				30 => 'x3633', //montreux-vevey-aigle
				31 => 'x3635', //sierre-sion-martigny-bas-valais				
				32 => 'x3634', //neuchatel-et-region-jura
				33 => 'x3636', //vaud-payerne-yverdon				
				34 => 'x3534', //fribourg-bulle
				35 => 'x3131', //bienne

				//ticino
				36 => 'x3532', //bellinzona
				37 => 'x3538', //locarno
				38 => 'x3539', //lugano				
				39 => 'x31', //chiasso
				42 => 'x313533', //cadenazzo [NO ZONE]
				43 => 'x313533', //riazzino [NO ZONE]
			),
			'row_banners_desktop_526_213' => array(
				-1  => 'cF21Dg27',
				17 => 'x323332', //zurich-umgebung
				18 => 'x323336', //wthur-thurgau-st-gallen
				19 => 'x323333', //basel-und-umgebung
				20 => 'x323334', //mittelland-aargau
				21 => 'x323337', //bern-city
				22 => 'x323338', //bern-umgeb-oberland
				23 => 'x323432', //biel-bienne-grenchen				
				24 => 'x323339', //luzern-innerschweiz				
				25 => 'x323430', //graub-nden-glarus				
				26 => 'x323431', //wallis				
				44 => 'x323335', //mittelland-solothurn
				45 => 'x323331', //zurich-city
				99 => 'x333538', //Romandie Region (Area)
			),
			'row_banners_mobile_300_250' => array(
				-1  => 'cF21Dg27',
				17 => 'x323433', //zurich-umgebung
				18 => 'x323437', //wthur-thurgau-st-gallen
				19 => 'x323434', //basel-und-umgebung
				20 => 'x323435', //mittelland-aargau
				21 => 'x323438', //bern-city
				22 => 'x323439', //bern-umgeb-oberland
				23 => 'x323533', //biel-bienne-grenchen				
				24 => 'x323530', //luzern-innerschweiz				
				25 => 'x323531', //graub-nden-glarus				
				26 => 'x323532', //wallis				
				44 => 'x323436', //mittelland-solothurn
				45 => 'x323330', //zurich-city
                99 => 'x333539', //Romandie Region (Area)
            ),
			'row_banners_mobile_283_x_202' => array(
				-1  => 'cF21Dg27',
				17 => 'x333238', //zurich umgebung mob big
				18 => 'x333239', //wthur thurgau st gallen mob big
				19 => 'x333330', //basel und umgebung mob big
				20 => 'x333331', //mittelland aargau mob big
				21 => 'x333332', //bern city mob big
				22 => 'x333333', //bern umgeb oberland mob big
				23 => 'x333334', //biel bienne grenchen mob big	
				24 => 'x333335', //luzern innerschweiz mob	big
				25 => 'x333336', //graub nden glarus mob big			
				26 => 'x333337', //wallis mob big
				44 => 'x333338', //mittelland solothurn mob big
				45 => 'x333339', //zurich city mob big
                99 => 'x333633', //Romandie Region (Area)
			),	
			'row_banners_mobile_139_x_99' => array(
				-1  => 'cF21Dg27',
				17 => 'x333430', //zurich umgebung mob small
				18 => 'x333431', //wthur thurgau st gallen mob small
				19 => 'x333432', //basel und umgebung mob small
				20 => 'x333433', //mittelland aargau mob small
				21 => 'x333434', //bern city mob small
				22 => 'x333435', //bern umgeb oberland mob small
				23 => 'x333436', //biel bienne grenchen mob small
				24 => 'x333437', //luzern innerschweiz mob small
				25 => 'x333438', //graub nden glarus mob small
				26 => 'x333439', //wallis mob small
				44 => 'x333530', //mittelland solothurn mob small
				45 => 'x333531', //zurich city mob small
			),	
			'row_banners_zone_5_big' => array(
				-1  => 'cF21Dg27',
				17 => 'x323736', //zurich umgebung big
				18 => 'x323737', //wthur thurgau st gallen big
				19 => 'x323738', //basel und umgebung big
				20 => 'x323739', //mittelland aargau big
				21 => 'x323830', //bern city big
				22 => 'x323831', //bern umgeb oberland big
				23 => 'x323832', //biel bienne grenchen big				
				24 => 'x323833', //luzern innerschweiz big				
				25 => 'x323834', //graub nden glarus big
				26 => 'x323835', //wallis big				
				44 => 'x323836', //mittelland solothurn big
				45 => 'x323837', //zurich city big
			),	
			'row_banners_zone_5_small' => array(
				-1  => 'cF21Dg27',
				17 => 'x333032', //zurich umgebung small
				18 => 'x333033', //wthur thurgau st gallen small
				19 => 'x333034', //basel und umgebung small
				20 => 'x333035', //mittelland aargau small
				21 => 'x333036', //bern city small
				22 => 'x333037', //bern umgeb oberland small
				23 => 'x333038', //biel bienne grenchen small
				24 => 'x333039', //luzern innerschweiz small
				25 => 'x333130', //graub nden glarus small
				26 => 'x333131', //wallis small
				44 => 'x333132', //mittelland solothurn small
				45 => 'x333532', //zurich city small
			),	
			//Romandie
			'row_banners_romandie_big' => array(
				-1  => 'cF21Dg27',
				99 => 'x333536', 
			),
			'row_banners_romandie_small' => array(
				-1  => 'cF21Dg27',
				99 => 'x333537', 
			),			
		);

		$top_prirority_zones = array(
			'row_banners_desktop_526_213' 	=> 'x323539',
			'row_banners_mobile_300_250' 	=> 'x323630',			
		);

		if( ($zone_name == 'row_banners_desktop_526_213' || $zone_name == 'row_banners_mobile_300_250') && $city_id != 99){
			//https://sceonteam.atlassian.net/browse/A6-215

			$cache = Zend_Registry::get('cache');
			$cache_key = "banners_" . Cubix_Application::getId() . "_" . $top_prirority_zones[$zone_name];

			if ( ! $result = $cache->load( $cache_key ) ) {
				$model_ixs = new Model_ixsBanners();
				$result = $banners = $model_ixs->get($top_prirority_zones[$zone_name]);
			
				$banners_count = count($banners);
				if($banners_count){

					if($banners_count < $rand_count){
						$rand_count = $banners_count;
					}

					if($rand_count){
						
						$random_banners = array();
						$random_els = array_rand($banners, $rand_count);

						if($rand_count == 1){
							$random_banners[] = $banners[$random_els];

							return $random_banners;
						}else{
							foreach($random_els as $random_el){
								$random_banners[] = $banners[$random_el];
							}	
							$banners = $random_banners;	
						}
					}
					
					$cache->save($banners, $cache_key, array());
					shuffle($result);
					return $result;
				}
			} else{
				shuffle($result);
				return $result;
			}

		}

		if ( isset($banner_mapping[$zone_name][$city_id]) ) {
			
			$cache = Zend_Registry::get('cache');
			$cache_key = "banners_" . Cubix_Application::getId() . "_" . $banner_mapping[$zone_name][$city_id];

			
			if ( ! $result = $cache->load( $cache_key ) ) {
				
				$model_ixs = new Model_ixsBanners();
				$result = $banners = $model_ixs->get($banner_mapping[$zone_name][$city_id]);
				

				$banners_count = count($banners);

				if($banners_count < $rand_count){
					$rand_count = $banners_count;
				}

				if($rand_count){
					$random_banners = array();
					$random_els = array_rand($banners, $rand_count);
					if($rand_count == 1){
						$random_banners[] = $banners[$random_els];
						return $random_banners;
					}else{
						foreach($random_els as $random_el){
							$random_banners[] = $banners[$random_el];
						}	
						$banners = $random_banners;	
					}
				}
				$cache->save($banners, $cache_key, array());
		
			}

			shuffle($result);
			return $result;

		}

		return "";
	}

	public static function getForMainPage( $zone_name = null, $region_slug = null, $rand_count = false )
	{
		if ( ! $zone_name || ! $region_slug ) return "";

		$banner_mapping = array(
			'deutschschweiz' => array(
				'main_top_left' => 'x37',
				'main_10' => 'x333535',
				'main_top_two' => 'x333534',
				'main_top_right' => 'x3439', 
				'main_middle' => 'x333535', 
				'main_bottom_left' => 'x323135',
				'main_bottom_right' => 'x323136',
				'main_popup' => 'x3530', 
				'gotd_place' => 'x3130', 
			),
			'romandie' => array(
				'main_top_left' => 'x3735',
				'main_10' => 'x333535',
				'main_top_two' => 'x333534',
				'main_top_right' => 'x3736', 
				'main_middle' => 'x333535', 
				'main_bottom' => 'x3733', 
				'main_popup' => 'x3731', 
				'gotd_place' => 'x3732', 
			),
			'ticino' => array(
				'main_top_left' => 'x3831',
				'main_10' => 'x333535',
				'main_top_two' => 'x333534',
				'main_top_right' => 'x3832', 
				'main_middle' => 'x333535', 
				'main_bottom' => 'x3739', 
				'main_popup' => 'x3737',
				'gotd_place' => 'x3738', 
			)
		);
		if ( isset($banner_mapping[$region_slug][$zone_name]) ) {
			$model_ixs = new Model_ixsBanners();
			$banners = $model_ixs->get($banner_mapping[$region_slug][$zone_name]);
	
			if($rand_count){
				$random_banners = array();
				if (count($banners) < $rand_count) $rand_count = count($banners);
				$random_els = array_rand($banners, $rand_count);
				if($rand_count == 1){
					$random_banners[] = $banners[$random_els];
					return $random_banners;
				}else{
					foreach($random_els as $random_el){
						$random_banners[] = $banners[$random_el];
					}	
					$banners = $random_banners;	
				}
			}
			
			return $banners;
		}
		return "";
	}
	 public static function getAll_265_82(){
        return array(
            //deutschschweiz
            17 => 'x3639', //zurich-umgebung
            18 => 'x3637', //wthur-thurgau-st-gallen
            19 => 'x3531', //basel-und-umgebung
            20 => 'x3631', //mittelland-aargau
            21 => 'x3533', //bern-city
            22 => 'x3132', //bern-umgeb-oberland
            23 => 'x3133', //biel-bienne-grenchen
            24 => 'x3630', //luzern-innerschweiz
            25 => 'x3536', //graub-nden-glarus
            26 => 'x3638', //wallis
            44 => 'x3632', //mittelland-solothurn
            45 => 'x3730', //zurich-city

            //romandie
            28 => 'x3535', //geneve-et-region
            29 => 'x3537', //lausanne-renens-moudon
            30 => 'x3633', //montreux-vevey-aigle
            31 => 'x3635', //sierre-sion-martigny-bas-valais
            32 => 'x3634', //neuchatel-et-region-jura
            33 => 'x3636', //vaud-payerne-yverdon
            34 => 'x3534', //fribourg-bulle
            35 => 'x3131', //bienne

            //ticino
            36 => 'x3532', //bellinzona
            37 => 'x3538', //locarno
            38 => 'x3539', //lugano
            39 => 'x31', //chiasso
        );
    }

	public static function getAllclubs(){
		return array(
			1 => 'x333630',
			2 => 'x333535',

			4 	=> 'x323737', //wthur thurgau st gallen big
			5 	=> 'x323738', //basel und umgebung big
			6 	=> 'x323739', //mittelland aargau big
			7 	=> 'x323830', //bern city big
			8 	=> 'x323831', //bern umgeb oberland big
			9 	=> 'x323832', //biel bienne grenchen big
			10 => 'x323833', //luzern innerschweiz big
			11 => 'x323834', //graub nden glarus big
			12 => 'x323835', //wallis big
			13 => 'x323836', //mittelland solothurn big
			14 => 'x323837', //zurich city big

			15 =>'x333032', //zurich umgebung small
			16 =>'x333033', //wthur thurgau st gallen small
			17 =>'x333034', //basel und umgebung small
			18 =>'x333035', //mittelland aargau small
			19 =>'x333036', //bern city small
			20 =>'x333037', //bern umgeb oberland small
			21 =>'x333038', //biel bienne grenchen small
			22 =>'x333039', //luzern innerschweiz small
			23 =>'x333130', //graub nden glarus small
			24 =>'x333131', //wallis small
			25 =>'x333132', //mittelland solothurn small
			26 =>'x333133', //zurich city small )
        );
	}

	public static function getAll_175_375($rand_count = false){
        return array(
            //deutschschweiz
            17 => 'x313531', //zurich-umgebung
            18 => 'x313435', //wthur-thurgau-st-gallen
            19 => 'x3835', //basel-und-umgebung
            20 => 'x313330', //mittelland-aargau
            21 => 'x3931', //bern-city
            22 => 'x3934', //bern-umgeb-oberland
            23 => 'x3937', //biel-bienne-grenchen
            24 => 'x313234', //luzern-innerschweiz
            25 => 'x313132', //graub-nden-glarus
            26 => 'x313438', //wallis
            44 => 'x313333', //mittelland-solothurn
            45 => 'x313533', //zurich-city

            //romandie
            28 => 'x313039', //geneve-et-region
            29 => 'x313135', //lausanne-renens-moudon
            30 => 'x313336', //montreux-vevey-aigle
            31 => 'x313237', //sierre-sion-martigny-bas-valais
            32 => 'x313339', //neuchatel-et-region-jura
            33 => 'x313432', //vaud-payerne-yverdon
            34 => 'x313036', //fribourg-bulle
            35 => 'x313030', //bienne

            //ticino
            36 => 'x3838', //bellinzona
            37 => 'x313138', //locarno
            38 => 'x313231', //lugano
            39 => 'x313033', //chiasso
            42 => 'x313533', //cadenazzo [NO ZONE]
            43 => 'x313533', //riazzino [NO ZONE]
        );
    }

	public function truncate(){
		$sql = 'truncate table ixs_banners';
		if($this->getAdapter()->query($sql)){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function copyImages($imgs){
		$copycount = 0;
		foreach ($imgs as $img) {
			
			if (!file_exists('./images/ixsGovazd/'.$img->filename)) {

				$sourceurl = 'https://www.ixspublic.com/uploads/'.$img->filename;
				
				$result = copy($sourceurl, './images/ixsGovazd/'.$img->filename);
				if(!$result){
					return 'image copy error';
				}
			}
			$copycount++;
		}
		return $copycount;
	}
}
