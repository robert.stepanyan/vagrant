<?php

$GLOBALS['tour_getAll'] = false;
$GLOBALS['upcoming_tour_getAll'] = false;

class Model_EscortsV3 extends Cubix_Model
{
	protected $_table = 'escorts';
	protected $_itemClass = 'Model_EscortV2Item';
	
	const VERIFIED_STATUS_NOT_VERIFIED = 1;
	const VERIFIED_STATUS_VERIFIED = 2;
	const VERIFIED_STATUS_VERIFIED_RESET = 3;

	const ESCORT_STATUS_NO_PROFILE 			= 1;		// 000000001
	const ESCORT_STATUS_NO_ENOUGH_PHOTOS 	= 2;		// 000000010
	const ESCORT_STATUS_NOT_APPROVED 		= 4;		// 000000100
	const ESCORT_STATUS_OWNER_DISABLED 		= 8;		// 000001000
	const ESCORT_STATUS_ADMIN_DISABLED 		= 16;		// 000010000
	const ESCORT_STATUS_ACTIVE 				= 32;		// 000100000
	const ESCORT_STATUS_IS_NEW				= 64;		// 001000000
	const ESCORT_STATUS_PROFILE_CHANGED		= 128;		// 010000000


	const PRODUCT_NATIONAL_LISTING			= 1;
	const PRODUCT_INTERNATIONAL_DIRECTORY	= 2;
	const PRODUCT_GIRL_OF_THE_MONTH			= 3;
	const PRODUCT_MAIN_PREMIUM_SPOT			= 4;
	const PRODUCT_CITY_PREMIUM_SPOT			= 5;
	const PRODUCT_TOUR_PREMIUM_SPOT			= 6;
	const PRODUCT_TOUR_ABILITY				= 7;
	const PRODUCT_NO_REVIEWS				= 8;
	const PRODUCT_ADDITIONAL_CITY			= 9;
	const PRODUCT_SEARCH					= 10;

	const HH_STATUS_ACTIVE  = 1;
	const HH_STATUS_PENDING = 2;
	const HH_STATUS_EXPIRED = 3;
	
	public function getChatNodeOnlineEscorts($page = 1, $per_page = 10)
	{
		//$user_ids = file_get_contents(Cubix_Application::getById()->url . '/node-chat/get-online-escorts');
		$cache_native = Zend_Registry::get('cache_native');
		$user_ids = $cache_native->get(Cubix_Application::getId() . '_chat_online_escorts');
		$user_ids = json_decode($user_ids);
		
		if (count($user_ids) > 0)
		{
			$user_ids_str = implode(',', $user_ids);
		
			$escorts = self::getAdapter()->fetchAll("
				SELECT SQL_CALC_FOUND_ROWS
					e.id AS id, e.gender, e.showname, e.photo_hash, e.user_id,
					e.photo_ext AS photo_ext, " . Cubix_Application::getId() . " AS application_id, e.is_online, ct.title_en AS city
				FROM escorts e
				INNER JOIN cities ct ON ct.id = e.city_id
				WHERE e.user_id IN (" . $user_ids_str . ") AND FIND_IN_SET(1, e.products) > 0
				GROUP BY e.id
				LIMIT ?,?",array(($page - 1) * $per_page, $per_page));
			
			$count = self::getAdapter()->fetchOne('SELECT FOUND_ROWS()');

			$escs_ids_arr = array();
			
			if ( $escorts ){
				foreach ( $escorts as &$escort ){
					$escort->application_id = Cubix_Application::getId();
					$escort->photo_status = 3;
					$escort = new Model_EscortV2Item($escort);
					$escs_ids_arr[] = $escort->id;
				}
			}
			//var_dump($escs_ids_arr);
			//var_dump(implode("\,", (array)$escs_ids_arr));
			$escs_ids = implode(',', $escs_ids_arr);			
		}
		else
		{
			$escorts = null;
			$count = 0;
			$escs_ids = '';
		}

		$result = array('escorts' => $escorts, 'count' => $count, 'ids' => $escs_ids);
		
		return $result;
	}
}