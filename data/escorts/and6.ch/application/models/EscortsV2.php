<?php
	$GLOBALS['tour_getAll'] = false;
	$GLOBALS['upcoming_tour_getAll'] = false;

	class Model_EscortsV2 extends Cubix_Model
	{
		protected $_table = 'escorts';
		protected $_itemClass = 'Model_EscortV2Item';
		
		const VERIFIED_STATUS_NOT_VERIFIED = 1;
		const VERIFIED_STATUS_VERIFIED = 2;
		const VERIFIED_STATUS_VERIFIED_RESET = 3;

		const ESCORT_STATUS_NO_PROFILE 			= 1;		// 000000001
		const ESCORT_STATUS_NO_ENOUGH_PHOTOS 	= 2;		// 000000010
		const ESCORT_STATUS_NOT_APPROVED 		= 4;		// 000000100
		const ESCORT_STATUS_OWNER_DISABLED 		= 8;		// 000001000
		const ESCORT_STATUS_ADMIN_DISABLED 		= 16;		// 000010000
		const ESCORT_STATUS_ACTIVE 				= 32;		// 000100000
		const ESCORT_STATUS_IS_NEW				= 64;		// 001000000
		const ESCORT_STATUS_PROFILE_CHANGED		= 128;		// 010000000


		const PRODUCT_NATIONAL_LISTING			= 1;
		const PRODUCT_INTERNATIONAL_DIRECTORY	= 2;
		const PRODUCT_GIRL_OF_THE_MONTH			= 3;
		const PRODUCT_MAIN_PREMIUM_SPOT			= 4;
		const PRODUCT_CITY_PREMIUM_SPOT			= 5;
		const PRODUCT_TOUR_PREMIUM_SPOT			= 6;
		const PRODUCT_TOUR_ABILITY				= 7;
		const PRODUCT_NO_REVIEWS				= 8;
		const PRODUCT_ADDITIONAL_CITY			= 9;
		const PRODUCT_SEARCH					= 10;

		const HH_STATUS_ACTIVE  = 1;
		const HH_STATUS_PENDING = 2;
		const HH_STATUS_EXPIRED = 3;

		const CAN_ESCORT_HIDE_PHONE_CC = FALSE; /* A6-129 */

        /**
         * @author Eduard Hovhannisyan
         *
         * Removes country calling code from phone number
         * Also if there is a trailing 0 at the beginning.
         *
         * @param array $params; keys: [phone_country_id, disable_phone_prefix, phone_number]
         * @return mixed|string
         */
        public static function formatPhoneNumber($params)
        {
            $phoneCountryId     = @$params['phone_country_id'];
            $disablePhonePrefix = @$params['disable_phone_prefix'];
            $phoneNumber        = @$params['phone_number'];
            $phoneNdd           = !empty($phoneCountryId) ? Model_Countries::getPhonePrefixById($phoneCountryId) : null;

            $formattedNumber = $phoneNumber;

            if(self::CAN_ESCORT_HIDE_PHONE_CC === false) $disablePhonePrefix = false;

            if($phoneCountryId && !$disablePhonePrefix) {
                // Remove unnecessary characters
                $phoneNumber = preg_replace("/^$phoneNdd/", '', $phoneNumber);
                $phoneNumber = ltrim($phoneNumber, '0');

                $formattedNumber = '+' . $phoneNdd . ' ' . $phoneNumber;
            }

            return $formattedNumber;
        }
		
		public function getShownameById($id)
		{
			return parent::getAdapter()->fetchOne('SELECT showname FROM escorts WHERE id = ?', $id);
		}

		public function getIdByShowname($showname, $region_slug, $city_slug)
		{
			$sql = '
				SELECT e.id
				FROM escorts e
				INNER JOIN cities c ON c.id = e.city_id
				INNER JOIN regions r ON r.id = c.region_id
				WHERE e.showname = ? AND r.slug = ? AND c.slug = ?
			';
			
			$escort_id = parent::_fetchRow($sql, array($showname, $region_slug, $city_slug));
			
			if ( ! $escort_id ) {
				$sql = '
					SELECT e.id
					FROM escorts e
					INNER JOIN cities c ON c.id = e.city_id
					INNER JOIN regions r ON r.id = c.region_id
					WHERE e.showname = ? AND r.slug = ?
				';
				$escort_id = parent::_fetchRow($sql, array($showname, $region_slug));
			}
			
			return $escort_id;
		}


		public function getRevComById($id)
		{
			return parent::getAdapter()->query('SELECT disabled_reviews, disabled_comments, cam_booking_status, cam_status, cam_preview_url, cam_channel_url  FROM escorts WHERE id = ?', $id)->fetch();
		}

		
		/**
		 * @param int $user_id
		 * @return Model_EscortV2Item
		 */
		public function getByUserId($user_id)
		{
			$client = new Cubix_Api_XmlRpc_Client();
			$escort = $client->call('Escorts.getByUserId', array($user_id));
			
			if ( ! $escort ) {
				return null;
			}
			
			return new Model_EscortV2Item((array) $escort);
			
			/*
			$item = parent::_fetchRow('
				SELECT e.id, u.email, ep.contact_phone, e.verified_status, u.application_id
				FROM escorts e
				INNER JOIN users u ON u.id = e.user_id
				INNER JOIN escort_profiles ep ON ep.escort_id = e.id
				WHERE e.user_id = ?
			', $user_id);
			
			return $item;*/
		}

		public function checkhash($escort_id, $hash)
		{
			$client = new Cubix_Api_XmlRpc_Client();
			return $client->call('Escorts.checkDeletionHash', array($escort_id, $hash));
		}

		public function existsByShowname($showname, $escort_id)
		{
			$client = new Cubix_Api_XmlRpc_Client();
			return $client->call('Escorts.existsByShowname', array($showname, $escort_id));
		}
		
		public function getById($id)
		{
			$client = new Cubix_Api_XmlRpc_Client();
			$escort = $client->call('Escorts.getById', array($id));
			
			if ( isset($escort['error']) ) {
				print_r($escort);die;
			}
			
			$escort = new Model_EscortV2Item($escort);
			$escort->setAdapter(self::getAdapter());
			
			return $escort;
		}
		
		public function getUserId($escort_id)
		{
			return self::db()->fetchOne('SELECT user_id FROM escorts WHERE id = ?', $escort_id);
		}
		
		public function get($shownameOrId, $cache_key = null, $is_preview = false, &$from_api = false)
		{		
			/*if ( $is_preview ) {
				$escort = Cubix_Api::getInstance()->call('makeVirtualSnapshotFromRevision', array($shownameOrId));
				
				if ( $escort ) {
					$escort = new Model_EscortV2Item($escort);
					$escort->setAdapter(self::getAdapter());

					return $escort;
				}
			}*/
			
			$cache = Zend_Registry::get('cache');

			$escort = array();
			if ( $cache_key ) {
				if ( ! $escort = $cache->load($cache_key) ) {
					$escort = unserialize(self::getAdapter()->fetchOne('SELECT ep.data FROM escort_profiles ep INNER JOIN escorts e ON e.id = ep.escort_id WHERE e.' . ( is_numeric($shownameOrId) ? 'id' : 'showname' ) . ' = ?', array($shownameOrId)));
                   
					$cache->save($escort, $cache_key, array());
				}
			}
			


			if ( $escort ) {
				$escort = new Model_EscortV2Item($escort);
				$escort->setAdapter(self::getAdapter());

				// PROBLEM: agency id is being taken from profile_updates, which is not being changed when switching escort's agency
				// simple workaround is to take the agency id from escorts table and override
				$escort->agency_id = (int) parent::getAdapter()->fetchOne('SELECT agency_id FROM escorts WHERE id = ?', $escort->id);
				return $escort;
			}
			else {
				$client = new Cubix_Api_XmlRpc_Client();
				
				$only_active = true;
				if ( $is_preview ) {
					$only_active = false;
				}
				
				$escort = $client->call('Escorts.getProfileV2', array($shownameOrId, Cubix_I18n::getLang(), true, $only_active));
				$escort = new Model_EscortV2Item($escort);
				$from_api = true;			
			}
			
			return $escort;
		}


		public function getPremiums($params, $upcoming_tours = false, $sess_name = '', $has_filter = false)
		{
			$config = Zend_Registry::get('escorts_config');

			$group = null;
			if ( isset($params['group']) ) {
				$group = $params['group'];
			}

			$order = $params['order'];
			
			$sql = $this->_getAll_sql($order, $page, $group);
			$sql['where'] = array_merge($sql['where'], $params['filter']);

			$sql['fields'][] = '1 AS is_premium';

			$sql1 = $sql2 = $sql;		

			$sql1['fields'][] = '1';
			$sql2['fields'][] = '1';

			if ($group == 'e.agency_id')
			{
				$sql1['fields'][] = 'e.agency_id';
				$sql2['fields'][] = 'e.agency_id';

				$sql1['fields'][] = 'COUNT(DISTINCT(e.id)) AS count';
				$sql2['fields'][] = 'COUNT(DISTINCT(e.id)) AS count';

				$sql1['where'][] = 'LENGTH(e.agency_id) > 0';
				$sql2['where'][] = 'LENGTH(e.agency_id) > 0';
			}

			$sql1['joins'][30] = 'INNER JOIN cities cc ON (cc.id = ec.city_id AND FIND_IN_SET(' . self::PRODUCT_NATIONAL_LISTING . ', e.products) > 0 AND e.is_on_tour = 0)';
			$sql1['joins'][45] = 'INNER JOIN premium_cities pc ON pc.escort_id = e.id AND cc.id = pc.city_id';
			$sql2['joins'][10] = '';
			$sql2['joins'][30] = 'INNER JOIN cities cc ON (IF (e.is_on_tour = 1, e.tour_city_id, ut.tour_city_id) = cc.id AND FIND_IN_SET(' . self::PRODUCT_TOUR_ABILITY . ', e.products) > 0 AND e.is_tour_premium = 1)';

			if ( is_array($params['filter']) ) {
				if ( array_key_exists('es.service_id = ?', $params['filter']) ) {
					$sql1['joins'][55] = 'INNER JOIN escort_services es ON es.escort_id = e.id';
					$sql2['joins'][55] = 'INNER JOIN escort_services es ON es.escort_id = e.id';
				}

				if ( array_key_exists('cz.slug = ?', $params['filter']) ) {
					$sql1['joins'][56] = 'INNER JOIN escort_cityzones ecz ON ecz.escort_id = e.id';
					$sql1['joins'][57] = 'INNER JOIN cityzones cz ON cz.id = ecz.city_zone_id';

					$sql2['joins'][56] = 'INNER JOIN escort_cityzones ecz ON ecz.escort_id = e.id';
					$sql2['joins'][57] = 'INNER JOIN cityzones cz ON cz.id = ecz.city_zone_id';
				}
			}

			// <editor-fold defaultstate="collapsed" desc="Favorites Filtration">
			if ( is_integer($params['filter']['f.user_id = ?']) ) {
				$sql1['fields'][] = 'f.user_id AS fav_user_id ';
				$sql1['joins'][] = 'INNER JOIN favorites f ON f.escort_id = e.id ';
				$sql2['fields'][] = 'f.user_id AS fav_user_id ';
				$sql2['joins'][] = 'INNER JOIN favorites f ON f.escort_id = e.id ';
			}
			// </editor-fold>

			$page = null;
			
			$__sql = $this->_getAll_union($sql1, $sql2, $order, $page, $group, true, false, $has_filter);

			return $__sql;

			/*$count = $result['count'];

			if ( $sess_name ) {
				$sid = 'sedcard_paging_' . Cubix_Application::getId();
				$ses = new Zend_Session_Namespace($sid);
				
				if ( ! $count ) {
					unset($ses->{$sess_name});
					$ses->{$sess_name} = array();
				}
				else {
					unset($ses->{$sess_name});
					foreach ( $result['data'] as $escort ) {
						$ses->{$sess_name}[] = $escort->showname;
					}
				}
			}

			return $result['data'];*/
		}

		public function getAll($params, &$count = 0, $sess_name = '', $has_filter = false)
		{
			$config = Zend_Registry::get('escorts_config');

			$order = null;
			if ( isset($params['order']) ) {
				$order = $params['order'];
			}

			$page = null;
			if ( isset($params['limit']) ) {
				$page = array($params['limit']['page'], $params['limit']['perPage'] ? $params['limit']['perPage'] : $config['perPage']);
			}

			$group = null;
			if ( isset($params['group']) ) {
				$group = $params['group'];
			}

			$sql = $this->_getAll_sql($order, $page, $group);
			
			$sql['where'] = array_merge($sql['where'], $params['filter']);

			$sql['fields'][] = '1';//'IF (pc.escort_id IS NOT NULL, 1, 0) AS is_premium';

			$sql1 = $sql2 = $sql;
			
			$sql1['fields'][] = '0 AS is_premium';
			$sql2['fields'][] = 'e.is_tour_premium AS is_premium';

			if ($group == 'e.agency_id')
			{
				$sql1['fields'][] = 'e.agency_id';
				$sql2['fields'][] = 'e.agency_id';

				$sql1['fields'][] = 'COUNT(DISTINCT(e.id)) AS count';
				$sql2['fields'][] = 'COUNT(DISTINCT(e.id)) AS count';

				$sql1['where'][] = 'LENGTH(e.agency_id) > 0';
				$sql2['where'][] = 'LENGTH(e.agency_id) > 0';
			}

			$wh = 'FIND_IN_SET(' . self::PRODUCT_NATIONAL_LISTING . ', e.products) > 0';
			if ( $sess_name == 'new_list' ) {
				$wh = 'FIND_IN_SET(' . self::PRODUCT_NATIONAL_LISTING . ', e.products) > 0 OR e.package_id = 7';
			}

			$sql1['joins'][30] = 'INNER JOIN cities cc ON (cc.id = ec.city_id AND ' . $wh . ' AND e.is_on_tour = 0)';
			$sql1['joins'][] = $sql2['joins'][] = 'LEFT JOIN premium_cities pc ON pc.escort_id = e.id AND pc.city_id = cc.id';
			$sql2['joins'][30] = 'INNER JOIN cities cc ON (IF (e.is_on_tour = 1, e.tour_city_id, ut.tour_city_id) = cc.id AND FIND_IN_SET(' . self::PRODUCT_TOUR_ABILITY . ', e.products) > 0)';

			if ( array_key_exists('es.service_id = ?', $params['filter']) ) {
				$sql1['joins'][55] = 'INNER JOIN escort_services es ON es.escort_id = e.id';
				$sql2['joins'][55] = 'INNER JOIN escort_services es ON es.escort_id = e.id';
			}

			if ( array_key_exists('cz.slug = ?', $params['filter']) ) {
				$sql1['joins'][56] = 'INNER JOIN escort_cityzones ecz ON ecz.escort_id = e.id';
				$sql1['joins'][57] = 'INNER JOIN cityzones cz ON cz.id = ecz.city_zone_id';

				$sql2['joins'][56] = 'INNER JOIN escort_cityzones ecz ON ecz.escort_id = e.id';
				$sql2['joins'][57] = 'INNER JOIN cityzones cz ON cz.id = ecz.city_zone_id';
			}

			// <editor-fold defaultstate="collapsed" desc="Favorites Filtration">
			if ( is_integer($params['filter']['f.user_id = ?']) ) {
				$sql1['fields'][] = 'f.user_id AS fav_user_id ';
				$sql1['joins'][] = 'INNER JOIN favorites f ON f.escort_id = e.id ';
				$sql2['fields'][] = 'f.user_id AS fav_user_id ';
				$sql2['joins'][] = 'INNER JOIN favorites f ON f.escort_id = e.id ';
			}
			// </editor-fold>

			$sql1['where'][] = 'pc.escort_id IS NULL';
			$sql2['where'][] = 'pc.escort_id IS NULL';

			$__sql1 = $this->_getAll_union($sql1, $sql2, $order, $page, $group, true, false, $has_filter);

			$__sql2 = $this->getPremiums($params, false, '', $has_filter);

			$result = $this->_getAll_union_union($__sql1, $__sql2, $order, $page, $group, isset($params['new']) ? true : false, $has_filter);
			

			$count = $result['count'];

			if ( $sess_name ) {
				$sid = 'sedcard_paging_' . Cubix_Application::getId();
				$ses = new Zend_Session_Namespace($sid);

				if ( ! $count ) {
					unset($ses->{$sess_name});
					$ses->{$sess_name} = array();
				}
				else {
					unset($ses->{$sess_name});
					foreach ( $result['data'] as $escort ) {
						$ses->{$sess_name}[] = $escort->showname;
					}
				}
			}

			return $result['data'];
		}


		/* Grigor Start */
		public function getSearchAll($params, &$count = 0, $page = 1, $page_size = 50 )
		{
					$lng = Cubix_I18n::getLang();

					$servicesJoin = '';
					$workingTimeJoin = '';
					
			if ( 'price' == substr($ordering, 0, 5) ) {
				$filter[] = 'e.incall_price IS NOT NULL';
			}
			// Only escorts with base city (or city tour) will be shown in this list
			if ( ! isset($filter['ct.slug = ?']) && ! isset($filter['r.slug = ?']) ) {
				$filter[] = 'eic.is_base = 1';
			}
			else {
				$filter[] = 'eic.is_upcoming = 0';
			}
					$filter = $params['filter'];
					if ( array_key_exists('es.service_id = ?',$filter) && is_array($filter['es.service_id = ?']) && count($filter['es.service_id = ?']) > 0 ) {
							$services = $filter['es.service_id = ?'];
							unset($filter['es.service_id = ?']);
							$query = array();
							$servicesCount = count($services);
							foreach($services as $service){
									$query[] =  'estmp.service_id = '.$service;
							}
							$resquery = '('.implode(' OR ',$query).')';
							$servicesJoin = " INNER JOIN (SELECT estmp.escort_id,COUNT(estmp.escort_id) as count FROM escort_services estmp WHERE $resquery GROUP BY estmp.escort_id  HAVING count = $servicesCount) es ON es.escort_id = e.id ";
					}

                    if (array_key_exists(Model_Escort_List::getSqlEquivalent('massage-only'), $filter)) {
                        $servicesJoin = "INNER JOIN escort_services es ON es.escort_id = e.id";
                    } else if (array_key_exists(Model_Escort_List::getSqlEquivalent('companion-only'), $filter)) {
                        $servicesJoin = "INNER JOIN escort_services es ON es.escort_id = e.id";
                    }

					if ( array_key_exists('working_times = ?', $filter) && is_array($filter['working_times = ?']) && count($filter['working_times = ?']) > 0 ) {
							$query = array();
							$workingDays = $filter['working_times = ?'];
							unset($filter['ewt.day_index = ?']);
							
							if(count($workingDays) > 0){
									foreach($workingDays as $value){
											$day = $value['day_index'];
											$time_from = $value['time_from'];
											$time_from_m = $value['time_from_m'];
											$time_to = $value['time_to'];
											$time_to_m = $value['time_to_m'];

											$query[] = "(ewttmp.day_index = $day AND ewttmp.time_from <= $time_from AND ewttmp.time_from_m <= $time_from_m AND ewttmp.time_to >= $time_to AND ewttmp.time_to_m >= $time_to_m )";
									}
							}

							unset($filter['working_times = ?']);

							$resquery = '('.implode(' OR ',$query).')';

							$workingdayCount = count($workingDays);

							$workingTimeJoin = " INNER JOIN (SELECT ewttmp.escort_id,COUNT(ewttmp.escort_id) as count FROM escort_working_times ewttmp INNER JOIN escorts e ON e.id = ewttmp.escort_id  WHERE e.available_24_7 = 1 OR  $resquery GROUP BY ewttmp.escort_id  HAVING count = $workingdayCount) ewt ON ewt.escort_id = e.id ";
					}

			$where = self::getWhereClause($filter, true);
            $g_where = Cubix_Countries::blacklistCountryWhereCase();
			$order = $params['order'];
					
			$page = (int) $page;
			if ( $page < 1 ) $page = 1;
			$limit = ($page_size * ($page - 1)) . ', ' . $page_size;

			$sql = '
				SELECT SQL_CALC_FOUND_ROWS
					e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered,
					ct.title_' . $lng . ' AS city, e.rates, e.incall_type, e.outcall_type, e.is_new,
					e.verified_status, e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,
					e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.review_count, e.comment_count,
					has_video, e.cam_booking_status, e.cam_status,
					IF (sei.primary_id IS NULL, se.title_' . $lng . ', sei.title_' . $lng . ') AS alt,
					e.hit_count, e.slogan, e.incall_price,
					e.date_last_modified,
					eic.is_premium,
					IF(enh.expires_at >= NOW(), 1, 0) as is_new_h,
					fct.title_' . $lng . ' AS f_city, afct.title_' . $lng . ' AS af_city, fct.id AS f_city_id, afct.id AS af_city_id,
					IF (ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -10 MINUTE), 1, 0) AS is_login
						
				FROM escorts_in_cities eic
				INNER JOIN escorts e ON e.id = eic.escort_id
				INNER JOIN cities ct ON ct.id = eic.city_id
				LEFT JOIN regions r ON r.id = eic.region_id
				LEFT JOIN escort_cityzones ecz ON ecz.escort_id = eic.escort_id
				LEFT JOIN cityzones cz ON cz.id = ecz.city_zone_id
				LEFT JOIN seo_entity_instances sei ON sei.primary_id = eic.escort_id
				LEFT JOIN f_cities fct ON fct.id = e.f_city_id
				LEFT JOIN f_cities afct ON afct.id = e.a_fake_city_id
				LEFT JOIN seo_entities se ON (se.id = sei.entity_id) OR (se.application_id = ' . Cubix_Application::getId() . ' AND se.slug = "escort")
				LEFT JOIN escorts_new_history enh on enh.escort_id = e.id 
				LEFT JOIN users_last_refresh_time ulrt ON ulrt.user_id = e.user_id
							'.$servicesJoin.$workingTimeJoin.'
				' . (! is_null($where) ? 'WHERE ' . $where : '') . ' ' . $g_where . '
				GROUP BY eic.escort_id
				ORDER BY ' . $order . '
				LIMIT ' . $limit . '
			';


			$escorts = self::db()->fetchAll($sql);
			$count = self::db()->fetchOne('SELECT FOUND_ROWS()');
					$result['data'] = $escorts;
					$result['count'] = $count;

			// <editor-fold defaultstate="collapsed" desc="Make Escort Alt for SEO">
			$tpl_data = array(
				'app_title' => Cubix_Application::getById()->title
			);

			foreach ( $escorts as $i =>  $escort ) {
				$additional = array(
					'showname' => $escort->showname,
					'city' => $escort->city
				);

				foreach ( $tpl_data + $additional as $tpl_var => $tpl_value ) {
					$escorts[$i]->alt = str_replace('%' . $tpl_var . '%', $tpl_value, $escorts[$i]->alt);
				}
			}
			// </editor-fold>

			// <editor-fold defaultstate="collapsed" desc="Store escorts on this page for further next/prev">
			if ( $store_session ) {
				//			$sid = 'v2_sedcard_paging_' . Cubix_Application::getId();
				//			$sess = new Zend_Session_Namespace($sid);
				//			$sess->escorts = array();
				//			$sess->page = $page;
				//			$sess->page_size = $page_size;
				//			$sess->pages_count = ceil($count / $page_size);
				//			$sess->criteria = array($filter, $ordering);
				//			$sess->callback = 'getFiltered';
				//			foreach ( $escorts as $escort ) {
				//				$sess->escorts[] = $escort->showname;
				//			}
			}
			// </editor-fold>
			return $result;
		}
		/* Grigor End */

		public function getEscortsByLocation($lat, $long, $limit, $page = 1)
		{
			$sql = "
				SELECT 
					SQL_CALC_FOUND_ROWS *, /*Distance($lat, $long, a.latitude, a.longitude) AS dist*/
					((2 * 6371 * 
						ATAN2(
						SQRT(
							POWER(SIN((RADIANS(" . $lat . " - latitude))/2), 2) +
							COS(RADIANS(latitude)) *
							COS(RADIANS(" . $lat . ")) *
							POWER(SIN((RADIANS(" . $long . " - longitude))/2), 2)
						),
						SQRT(1-(
							POWER(SIN((RADIANS(" . $lat . " - latitude))/2), 2) +
							COS(RADIANS(latitude)) *
							COS(RADIANS(" . $lat . ")) *
							POWER(SIN((RADIANS(" . $long . " - longitude))/2), 2)
						))
						)
					)) AS dist
				FROM  ( 
					SELECT escorts.id, showname, latitude, longitude, `hash`, width, escort_photos.height,ext, escort_photos.`status` 
					FROM escorts 
					INNER JOIN escort_photos ON escorts.id = escort_photos.escort_id AND escort_photos.is_main = 1 AND latitude IS NOT NULL AND longitude IS NOT NULL  
					INNER JOIN escorts_in_cities ON escorts.id = escorts_in_cities.escort_id GROUP BY escorts.id
				)
				AS a ORDER BY dist ASC LIMIT $page, $limit
			";
			
			$escorts = self::db()->fetchAll($sql);
			$count = self::db()->fetchOne('SELECT FOUND_ROWS()');
			$result = array();
			$result['data'] = $escorts;
					$result['count'] = $count;
			
			return $result;
		}
		
		public function getAllMainPremiumSpot($params, &$count = 0, $sess_name = '')
		{
			$config = Zend_Registry::get('escorts_config');

			$params['filter']['e.is_main_premium_spot = 1'] = array();

			$order = null;
			if ( isset($params['order']) ) {
				$order = $params['order'];
			}

			$page = null;
			if ( isset($params['limit']) ) {
				$page = array($params['limit']['page'], is_numeric($params['limit']['perPage']) ? $params['limit']['perPage'] : $config['perPage']);
			}

			$group = null;
			if ( isset($params['group']) ) {
				$group = $params['group'];
			}

			$sql = $this->_getAll_sql($order, $page, $group);
			$sql['where'] = array_merge($sql['where'], $params['filter']);

			$sql['fields'][] = '1 AS is_premium';

			$sql1 = $sql2 = $sql;
			$sql1['joins'][30] = 'INNER JOIN cities cc ON (cc.id = e.city_id AND FIND_IN_SET(' . self::PRODUCT_NATIONAL_LISTING . ', e.products) > 0 AND e.is_on_tour = 0)';
			/*$sql1['where'][] = 'c.id = 71';*/
			$sql2['joins'][30] = 'INNER JOIN cities cc ON (IF (e.is_on_tour = 1, e.tour_city_id, ut.tour_city_id) = cc.id AND FIND_IN_SET(' . self::PRODUCT_TOUR_ABILITY . ', e.products) > 0)';


			if ( array_key_exists('es.service_id = ?', $params['filter']) ) {
				$sql1['joins'][55] = 'INNER JOIN escort_services es ON es.escort_id = e.id';
				$sql2['joins'][55] = 'INNER JOIN escort_services es ON es.escort_id = e.id';
			}

			if ( array_key_exists('cz.slug = ?', $params['filter']) ) {
				$sql1['joins'][56] = 'INNER JOIN escort_cityzones ecz ON ecz.escort_id = e.id';
				$sql1['joins'][57] = 'INNER JOIN cityzones cz ON cz.id = ecz.city_zone_id';

				$sql2['joins'][56] = 'INNER JOIN escort_cityzones ecz ON ecz.escort_id = e.id';
				$sql2['joins'][57] = 'INNER JOIN cityzones cz ON cz.id = ecz.city_zone_id';
			}

			$result = $this->_getAll_union($sql1, $sql2, $order, $page, $group);

			$count = $result['count'];

			if ( $sess_name ) {
				$sid = 'sedcard_paging_' . Cubix_Application::getId();
				$ses = new Zend_Session_Namespace($sid);

				if ( ! $count ) {
					unset($ses->{$sess_name});
					$ses->{$sess_name} = array();
				}
				else {
					unset($ses->{$sess_name});
					foreach ( $result['data'] as $escort ) {
						$ses->{$sess_name}[] = $escort->showname;
					}
				}
			}

			return $result['data'];
		}

		public function getAllTourMainPremiumSpot($params, &$count = 0, $upcoming_tours = false, $sess_name = '')
		{
			$config = Zend_Registry::get('escorts_config');

			if ( isset($params['filter']['e.gender = 1']) ) {
				unset($params['filter']['e.gender = 1']);
			}

			$order = null;
			if ( isset($params['order']) ) {
				$order = $params['order'];
			}

			$page = null;
			if ( isset($params['limit']) ) {
				$page = array($params['limit']['page'], $params['limit']['perPage'] ? $params['limit']['perPage'] : $config['perPage']);
			}

			$group = null;
			if ( isset($params['group']) ) {
				$group = $params['group'];
			}

			$sql = $this->_getAll_sql($order, $page, $group);
			unset($sql['page']);

			$sql['where'] = $params['filter'];

			$sql['fields'][] = 'e.is_tour_premium AS is_premium';

			if ( $upcoming_tours ) {
				$sql['fields']['tour_city_id'] = 'ut.tour_city_id';
				$sql['fields']['tour_date_from'] = 'ut.tour_date_from';
				$sql['fields']['tour_date_to'] = 'ut.tour_date_to';
				$sql['joins'][30] = 'INNER JOIN cities cc ON ut.tour_city_id = cc.id';
				$sql['joins'][21] = 'INNER JOIN upcoming_tours ut ON ut.id = e.id';
				$sql['where'][] = 'ut.tour_date_from < CURDATE() + INTERVAL 7 DAY';
			}
			else {
				$sql['fields']['tour_city_id'] = 'e.tour_city_id';
				$sql['fields']['tour_date_from'] = 'e.tour_date_from';
				$sql['fields']['tour_date_to'] = 'e.tour_date_to';
				$sql['joins'][30] = 'INNER JOIN cities cc ON e.tour_city_id = cc.id';
			}

			if ( array_key_exists('es.service_id = ?', $params['filter']) ) {
				$sql['joins'][55] = 'INNER JOIN escort_services es ON es.escort_id = e.id';
			}

			if ( array_key_exists('cz.slug = ?', $params['filter']) ) {
					$sql['joins'][56] = 'INNER JOIN escort_cityzones ecz ON ecz.escort_id = e.id';
					$sql['joins'][57] = 'INNER JOIN cityzones cz ON cz.id = ecz.city_zone_id';
				}
			
			

			$sql['where'][] = 'e.is_tour_premium = 1';

			return $sql;//$__sql = parent::getSql($sql);

			/*$result = $this->_getAll($sql);

			$count = $result['count'];

			if ( $sess_name ) {
				$sid = 'sedcard_paging_' . Cubix_Application::getId();
				$ses = new Zend_Session_Namespace($sid);

				if ( $upcoming_tours ) {
					$sess_name = 'up_' . $sess_name;
				}

				if ( ! $count ) {
					unset($ses->{$sess_name});
					$ses->{$sess_name} = array();
				}
				else {
					unset($ses->{$sess_name});
					foreach ( $result['data'] as $escort ) {
						$ses->{$sess_name}[] = $escort->showname;
					}
				}
			}

			return $result['data'];*/
		}

		public function getAllTours($params, &$count, $upcoming_tours = false, $sess_name = '', $has_filter = false)
		{
			$config = Zend_Registry::get('escorts_config');

			if ( isset($params['filter']['e.gender = 1']) ) {
				unset($params['filter']['e.gender = 1']);
			}

			$order = null;
			if ( isset($params['order']) ) {
				$order = $params['order'];
			}

			$page = null;
			if ( isset($params['limit']) ) {
				$page = array($params['limit']['page'], $params['limit']['perPage'] ? $params['limit']['perPage'] : $config['perPage']);
			}

			$group = null;
			if ( isset($params['group']) ) {
				$group = $params['group'];
			}

			$sql = $this->_getAll_sql($order, $page, $group);

			$sql['where'] = array_merge($sql['where'], $params['filter']);

			$sql['fields'][] = '0 AS is_premium';

			if ( $upcoming_tours ) {
				$sql['fields']['tour_city_id'] = 'ut.tour_city_id';
				$sql['fields']['tour_date_from'] = 'ut.tour_date_from';
				$sql['fields']['tour_date_to'] = 'ut.tour_date_to';
				$sql['joins'][30] = 'INNER JOIN cities cc ON ut.tour_city_id = cc.id';
				$sql['joins'][21] = 'INNER JOIN upcoming_tours ut ON ut.id = e.id';
				$sql['where'][] = 'ut.tour_date_from < CURDATE() + INTERVAL 7 DAY';
			}
			else {
				$sql['fields']['tour_city_id'] = 'e.tour_city_id';
				$sql['fields']['tour_date_from'] = 'e.tour_date_from';
				$sql['fields']['tour_date_to'] = 'e.tour_date_to';
				$sql['joins'][30] = 'INNER JOIN cities cc ON e.tour_city_id = cc.id';
			}

			if ( array_key_exists('es.service_id = ?', $params['filter']) ) {
				$sql['joins'][55] = 'INNER JOIN escort_services es ON es.escort_id = e.id';
			}

			if ( array_key_exists('cz.slug = ?', $params['filter']) ) {
				$sql['joins'][56] = 'INNER JOIN escort_cityzones ecz ON ecz.escort_id = e.id';
				$sql['joins'][57] = 'INNER JOIN cityzones cz ON cz.id = ecz.city_zone_id';
			}

			

			$sql['where'][] = 'e.is_tour_premium <> 1';
			$sql['where'][] = "FIND_IN_SET(" . Model_Escorts::PRODUCT_TOUR_ABILITY . ", e.products) > 0";
			//print_r($sql);

			$premium_sql = $this->getAllTourMainPremiumSpot($params, $count, $upcoming_tours, $sess_name);	

			$result = $this->_getAll_union($sql, $premium_sql, $order, $page, $group, false, true, $has_filter);
			
			$count = $result['count'];

			if ( $sess_name ) {
				$sid = 'sedcard_paging_' . Cubix_Application::getId();
				$ses = new Zend_Session_Namespace($sid);

				if ( $upcoming_tours ) {
					$sess_name = 'up_' . $sess_name;
				}

				if ( ! $count ) {
					unset($ses->{$sess_name});
					$ses->{$sess_name} = array();
				}
				else {
					unset($ses->{$sess_name});
					foreach ( $result['data'] as $escort ) {
						$ses->{$sess_name}[] = $escort->showname;
					}
				}
			}

			return $result['data'];
		}

		public function _getAll(array $sql)
		{
			ksort($sql['joins']);

			$dataSql = parent::getSql($sql);

			//echo $dataSql;

			unset($sql['page']);
			unset($sql['group']);
			unset($sql['order']);
			$sql['fields'] = array('COUNT(DISTINCT(e.id))');
			$countSql = parent::getSql($sql);

			try {
				$escorts = parent::_fetchAll($dataSql);
			}
			catch ( Exception $e ) {
				/*print_r($sql);
				print_r($orig_sql);
				echo $dataSql . "<br/>\n";
				echo $e;
				die;*/
			}

			// <editor-fold defaultstate="collapsed" desc="Make Escort Alt for SEO">
			$tpl_data = array(
				'app_title' => Cubix_Application::getById()->title
			);

			foreach ( $escorts as $i =>  $escort ) {
				$additional = array(
					'showname' => $escort->showname,
					'city' => $escort->city,
					'region' => $escort->region
				);

				foreach ( ($tpl_data + $additional) as $tpl_var => $tpl_value ) {
					$escorts[$i]->alt = str_replace('%' . $tpl_var . '%', $tpl_value, $escorts[$i]->alt);
				}
			}
			// </editor-fold>

			return array(
				'data' => $escorts,
				'count' => parent::getAdapter()->fetchOne($countSql)
			);
		}

		public function _getAll_union(array $sql1, array $sql2, $order = null, $page = null, $group = null, $ret_sql = false, $is_tour = false, $has_filter = false)
		{
			$config = Zend_Registry::get('escorts_config');

			unset($sql1['page']); unset($sql1['order']);
			//		$sql1['joins'][30] = 'INNER JOIN cities cc ON (cc.id = ec.city_id AND FIND_IN_SET(' . self::PRODUCT_NATIONAL_LISTING . ', e.products) > 0 AND ec.is_tour = 0 AND e.is_on_tour = 0)';
			ksort($sql1['joins']);
			$dataSql1 = parent::getSql($sql1);

			unset($sql2['page']); unset($sql2['order']);
			//		$sql2['joins'][30] = 'INNER JOIN cities cc ON (IF (e.is_on_tour = 1, e.tour_city_id, ut.tour_city_id) = cc.id AND FIND_IN_SET(' . self::PRODUCT_TOUR_ABILITY . ', e.products) > 0)';
			ksort($sql2['joins']);
			$dataSql2 = parent::getSql($sql2);

			if ($group == 'e.agency_id')
				$f = 'agency_id';
			else
				$f = 'id';
			$dataSql = 'SELECT x.* FROM ((' . $dataSql1 . ') UNION (' . $dataSql2 . ')) AS x GROUP BY x.' . $f;


			if ( $ret_sql ) {
				return $dataSql;
			}

			if ( ! is_null($order) ) {
				if ( 'price' == substr($order, 0, 5) ) {
					$sql['where'][] = 'e.incall_price IS NOT NULL';
					$dataSql .= ' ORDER BY incall_price ' . ($order == 'price-desc' ? ' DESC' : ' ASC');
				}
				else {
					if ( 'ordering' == substr($order, 0, 8) ) {
						$is_prem = '';
						if ( ! $has_filter ) {
							$is_prem = 'x.is_premium DESC,';
						}
						$dataSql .= ' ORDER BY ' . $is_prem . ' ' . $order;
						//$dataSql .= ' ORDER BY x.is_premium DESC, ' . $order;
					}
					else {
						$dataSql .= ' ORDER BY ' . $order;
					}
				}
			}
			
			$_page = array(1, $config['perPage']);
			if ( ! is_null($page) ) {
				$_page = $page + $_page;
				if ( $_page[0] < 1 ) $_page[0] = 1;
			}
			$dataSql .= ' LIMIT ' . (($_page[0] - 1) * $_page[1]) . ', ' . $_page[1];

			
			
			//if ( $page != array(1, 10) ) {echo $dataSql;die;}



			$countSql = 'SELECT COUNT(DISTINCT(x.id)) FROM ((' . $dataSql1 . ') UNION (' . $dataSql2 . ')) AS x';

			try {
				$escorts = parent::_fetchAll($dataSql);
			}
			catch ( Exception $e ) {
				throw $e;
			}

			// <editor-fold defaultstate="collapsed" desc="Make Escort Alt for SEO">
			$tpl_data = array(
				'app_title' => Cubix_Application::getById()->title
			);

			foreach ( $escorts as $i =>  $escort ) {
				$additional = array(
					'showname' => $escort->showname,
					'city' => $escort->city,
					'region' => $escort->region
				);

				foreach ( ($tpl_data + $additional) as $tpl_var => $tpl_value ) {
					$escorts[$i]->alt = str_replace('%' . $tpl_var . '%', $tpl_value, $escorts[$i]->alt);
				}
			}
			// </editor-fold>
			
			return array(
				'data' => $escorts,
				'count' => parent::getAdapter()->fetchOne($countSql)
			);
		}

		public function _getAll_union_union($sql1, $sql2, $order = null, $page = null, $group = null, $is_new = false, $has_filter = false)
		{
			$config = Zend_Registry::get('escorts_config');
			$dataSql1 = $sql1;
			$dataSql2 = $sql2;
			/*unset($sql1['page']); unset($sql1['order']);

			ksort($sql1['joins']);
			$dataSql1 = parent::getSql($sql1);

			unset($sql2['page']); unset($sql2['order']);

			ksort($sql2['joins']);
			$dataSql2 = parent::getSql($sql2);*/

			if ($group == 'e.agency_id')
				$f = 'agency_id';
			else
				$f = 'id';
			$dataSql = 'SELECT xx.* FROM ((' . $dataSql1 . ') UNION (' . $dataSql2 . ')) AS xx GROUP BY xx.' . $f;

			//		$order = 'date_registered DESC';
			if ( ! is_null($order) ) {
				if ( 'price' == substr($order, 0, 5) ) {
					$sql['where'][] = 'e.incall_price IS NOT NULL';
					$dataSql .= ' ORDER BY incall_price ' . ($order == 'price-desc' ? ' DESC ' : ' ASC ');
				}
				else {
					if ( 'ordering' == substr($order, 0, 8) ) {
						$is_prem = '';
						if ( ! $has_filter ) {
							$is_prem = 'xx.is_premium DESC,';
						}

						//var_dump(' ' . $is_prem . ' ' . $order);
						$dataSql .= ' ORDER BY ' . $is_prem . ' ' . $order;
						//$dataSql .= ' ORDER BY xx.is_premium DESC, ' . $order;
					}
					else {
						$dataSql .= ' ORDER BY ' . $order;
					}
				}
			}

			$_page = array(1, $config['perPage']);
			if ( ! is_null($page) ) {
				$_page = $page + $_page;
				if ( $_page[0] < 1 ) $_page[0] = 1;
			}
			$dataSql .= ' LIMIT ' . (($_page[0] - 1) * $_page[1]) . ', ' . $_page[1];
			
			$countSql = 'SELECT COUNT(DISTINCT(xx.id)) FROM ((' . $dataSql1 . ') UNION (' . $dataSql2 . ')) AS xx';

			try {
				//die($dataSql);
				$escorts = parent::_fetchAll($dataSql);
			}
			catch ( Exception $e ) {
				throw $e;
			}

			// <editor-fold defaultstate="collapsed" desc="Make Escort Alt for SEO">
			$tpl_data = array(
				'app_title' => Cubix_Application::getById()->title
			);

			foreach ( $escorts as $i =>  $escort ) {
				$additional = array(
					'showname' => $escort->showname,
					'city' => $escort->city,
					'region' => $escort->region
				);

				foreach ( ($tpl_data + $additional) as $tpl_var => $tpl_value ) {
					$escorts[$i]->alt = str_replace('%' . $tpl_var . '%', $tpl_value, $escorts[$i]->alt);
				}
			}
			// </editor-fold>

			return array(
				'data' => $escorts,
				'count' => parent::getAdapter()->fetchOne($countSql)
			);
		}

		public function _getAll_sql($order = null, array $page = null, $group = null)
		{
			$config = Zend_Registry::get('escorts_config');
			
			$sql = array(
				'tables' => 'escorts e',
				'fields' => array(
					'e.id', 'e.showname', 'e.age', 'UNIX_TIMESTAMP(e.date_registered) AS date_registered',
					'cc.' . Cubix_I18n::getTblField('title') . ' AS city',

					'e.rates',

					'e.incall_type', 'e.outcall_type',

					'e.is_new', 'e.verified_status', 'e.photo_hash', 'e.photo_ext', 'e.photo_status',
					Cubix_Application::getId() . ' AS application_id',
					
					'e.city_id', 
					'tour_city_id' => 'IF (e.is_on_tour, e.tour_city_id, ut.tour_city_id) AS tour_city_id',
					'cc.' . Cubix_I18n::getTblField('title') . ' AS tour_city',
					'tour_date_from' => 'IF (e.is_on_tour, e.tour_date_from, ut.tour_date_from) AS tour_date_from',
					'tour_date_to' => 'IF (e.is_on_tour, e.tour_date_to, ut.tour_date_to) AS tour_date_to',

					'e.hit_count',
					
					'e.products', /*'e.is_premium', */'IF (sei.primary_id IS NULL, se.title_' . Cubix_I18n::getLang() . ', sei.title_' . Cubix_I18n::getLang() . ') AS alt',
					'e.slogan',

					'e.incall_price', 'e.ordering', 'e.date_last_modified'
				),
				'joins' => array(
					10 => 'INNER JOIN escort_cities ec ON ec.escort_id = e.id',
					20 => 'INNER JOIN countries c ON c.id = e.country_id',

					21 => 'LEFT JOIN upcoming_tours ut ON ut.id = e.id',

					30 => '', // Reserverd for cities join
					
					50 => 'LEFT JOIN regions r ON r.id = cc.region_id',

					

					/* --> SEO */
					60 => 'LEFT JOIN seo_entity_instances sei ON sei.primary_id = e.id',
					70 => 'LEFT JOIN seo_entities se ON (se.id = sei.entity_id) OR (se.application_id = ' . Cubix_Application::getId(). ' AND se.slug = "escort")',
					/* SEO <-- */
				),
				'where' => array(

				),
				'order' => array('date_registered', 'DESC'),
				'page' => array(1, $config['perPage'])
			);

			if (is_null($group))
				$sql['group'] = 'e.id';
			else
				$sql['group'] = $group;

			//		$order = 'date_registered DESC';
			if ( ! is_null($order) ) {
				if ( 'price' == substr($order, 0, 5) ) {
					$sql['where'][] = 'e.incall_price IS NOT NULL';
					$order = 'incall_price ' . ($order == 'price-desc' ? ' DESC' : ' ASC');
				}
				$sql['order'] = explode(' ', $order);
			}

			if ( ! is_null($page) ) {
				$sql['page'] = $page + $sql['page'];
			}
			else {
				unset($sql['page']);
			}

			return $sql;
		}

		public function getCount($params)
		{
			/* --> Construct where clause */
			$where_arr = array();
			if ( isset($params['filter']) ) {
				$params['filter'] = array_merge($params['filter'], array(
					// 'u.status = ?' => array(1),
					// 'u.application_id = ?' => array(Cubix_Application::getId())
				));

				$where_arr = array();

				if ( is_integer($params['filter']['f.user_id = ?']) ) {
					$what = ' , f.user_id AS fav_user_id ';
					$join = ' INNER JOIN favorites f ON f.escort_id = e.id ';
				}

				foreach ( $params['filter'] as $exp => $value ) {
					if ( ! is_array($value) ) {
						$value = array($value);
					}

					$where_arr[$exp] = $value;
				}

				$where = implode(' AND ', array_keys($where_arr));
			}
			/* <-- */

			$countSql = '
				SELECT
					COUNT(DISTINCT(e.id)) AS count
				FROM escorts e

				INNER JOIN escort_cities ec ON ec.escort_id = e.id

				INNER JOIN escort_products eprod ON eprod.escort_id = e.id AND eprod.product_id = ' . self::PRODUCT_NATIONAL_LISTING . '

				INNER JOIN countries c ON c.id = e.country_id
				INNER JOIN cities cc ON cc.id = ec.city_id OR e.tour_city_id = cc.id
				LEFT JOIN regions r ON r.id = cc.region_id
				LEFT JOIN cityzones cz ON cz.id = e.cityzone_id
				' . $join . '
				' . ( strlen($where) ? 'WHERE ' . $where  : '' ) . '
			';
var_dump($countSql); die;
			$bind = array();
			foreach ( $where_arr as $value ) {
				// If an empty array supplied in $where_arr for a value, dont add it
				if ( ! count($value) ) continue;

				$bind = array_merge($bind, $value);
			}

			return parent::getAdapter()->fetchOne($countSql, $bind);
		}
		
		/*public function remove($id)
		{
			$client = new Cubix_Api_XmlRpc_Client();
			$client->call('Escorts.delete', array($escort_id));
			
			self::getAdapter()->delete('escorts', self::getAdapter()->quoteInto('id = ?', $id));
			self::getAdapter()->delete('escort_photos', self::getAdapter()->quoteInto('escort_id = ?', $id));
			self::getAdapter()->delete('escort_cities', self::getAdapter()->quoteInto('escort_id = ?', $id));
		}*/
		
		public function getNationalities()
		{
			$sql = "SELECT id, " . Cubix_I18n::getTblField('title') . " AS title FROM nationalities";

			return parent::_fetchAll($sql);
		}
		
		// -- >> FAVORITES
		public function addToFavorites($user_id, $escort_id)
		{
			$sql = 'INSERT INTO favorites (user_id, escort_id, application_id) VALUES(?, ?, ?)';
			
			$this->getAdapter()->query($sql, array($user_id, $escort_id, Cubix_Application::getId()));
		}
		
		public function removeFromFavorites($user_id, $escort_id)
		{
			$this->getAdapter()->query('DELETE FROM favorites WHERE user_id = ? AND escort_id = ?', array($user_id, $escort_id));
		}
		// FAVORITES << --
		
		// Profile Options
		
		public function getNationalityOptions()
		{
			return $this->getAdapter()->query('SELECT id, iso, title_en AS title FROM nationalities ORDER BY title_en ASC')->fetchAll();
		}
		
		public function addVacation($escort_id, $data)
		{	
			$this->_db->update('escorts', $data, $this->_db->quoteInto('id = ?', $escort_id));
		}
		
		public function removeVacation($escort_id)
		{		
			$this->_db->update('escorts', array('vac_date_from' => null, 'vac_date_to' => null), $this->_db->quoteInto('id = ?', $escort_id));
		}
		
		public function save($escort)
		{
			$client = new Cubix_Api_XmlRpc_Client();
			$escort_id = $client->call('Escorts.save', array($escort));
			$escort->setId($escort_id);
			
			return $escort;
		}

		public function getComments()
		{
			$config = Zend_Registry::get('escorts_config');
			$sql = "SELECT c.*, e.showname FROM comments c INNER JOIN escorts e ON e.id = c.escort_id GROUP BY e.id ORDER BY RAND() LIMIT ?";

			return $this->_db->query($sql, array($config['comments']['sidebarComentsCount']))->fetchAll();
		}

		public function isGOTM($escort_id)
		{
			$month = intval(date('n'));
			$year = intval(date('Y'));

			$res = $this->_db->query('SELECT gotm_year AS year, gotm_month AS month FROM escorts WHERE id = ? AND NOT (gotm_year = ? AND gotm_month = ?)', array($escort_id, $year, $month))->fetch();

			if ($res)
				return $res;
			else
				return null;
		}

		public function getEscortVotingData($escort_id)
		{
			$sql = "SELECT votes_sum, votes_count, voted_members FROM escorts WHERE id = ?";

			return $this->_db->query($sql, $escort_id)->fetch();
		}

		public function getEscortLastReview($escort_id)
		{
			$sql = '
				SELECT SQL_CALC_FOUND_ROWS
					r.username, r.looks_rating, r.services_rating, r.is_fake_free, r.meeting_place, r.duration, r.duration_unit, r.price, r.currency,
					r.fuckometer, r.s_kissing, r.s_blowjob, r.s_cumshot, r.s_69, r.s_anal, r.s_anal, r.s_sex, r.s_attitude, r.s_conversation, r.s_breast, r.s_multiple_sex,
					r.s_availability, r.s_photos, r.services_comments, r.review, UNIX_TIMESTAMP(r.meeting_date) AS meeting_date, UNIX_TIMESTAMP(r.creation_date) AS creation_date
				FROM reviews r			
				WHERE r.status = 2 AND r.is_deleted = 0 AND r.escort_id = ?
				ORDER BY r.creation_date DESC
				LIMIT 1
			';

			$countSql = 'SELECT FOUND_ROWS() AS count';

			$item = $this->_db->query($sql, array($escort_id))->fetch();
			$count = $this->_db->query($countSql)->fetch();
			$count = $count->count;

			return array($item, $count);
		}

		public function getBubbleText($escort_id)
		{
			$sql = "SELECT bubble_text FROM escorts WHERE id = ?";

			return $this->_db->query($sql, $escort_id)->fetch();
		}

		public function getAgency($escort_id)
		{
			$sql = "SELECT agency_slug AS slug, agency_name AS name FROM escorts WHERE id = ?";

			return $this->_db->query($sql, $escort_id)->fetch();
		}
		
		public static function getCityByEscortId($escort_id)
		{
			$sql = '
				SELECT c.id, c.title_' . Cubix_I18n::getLang() . ' AS title
				FROM escorts e
				INNER JOIN cities c ON c.id = e.city_id
				WHERE e.id = ?
			';
			
			return self::db()->fetchRow($sql, array($escort_id));
		}

		public static function hit($escort_id)
		{
			$cache = self::_getCache();

			$data = $cache->load(self::HITS_CACHE_KEY);       

			if ( !is_array($data) ) {
				$data = array();
			}
					
			if ( !isset($data[$escort_id]) || !isset($data[$escort_id][1]) ) {
				$data[$escort_id] = array(time(), 0);
			}

					$data[$escort_id][1] = $data[$escort_id][1] + 2;

			$total = 0;
			foreach ( $data as $escort_id => $row ) {
				$total += $data[$escort_id][1];
			}

			$cache->save($data, self::HITS_CACHE_KEY);

			$cache_limit = Zend_Registry::get('system_config');
			$errors = array();
			if ( $total >= $cache_limit['hitsCacheLimit'] ) {
				
				$client = Cubix_Api_XmlRpc_Client::getInstance();

				/*$c = 0;
				while ( $c <= 5 ) {*/
					try {

						$result = $client->call('Escorts.updateHitsCount', array($data));

						if ( $result === true ) {
							foreach ( $data as $escort_id => $row) {
															if (is_array($row)) {
																	$date = @$row[0];
																	$count = @$row[1];
																	self::getAdapter()->query('UPDATE escorts SET hit_count = (IFNULL(hit_count, 0) + ?) WHERE id = ?', array($count, $escort_id));
															}
							}

							$cache->remove(self::HITS_CACHE_KEY);
							return;
						}

						

						/*ob_start();
						var_dump($result);
						$result = ob_get_clean();

						$errors[] = 'Try #' . $c . ' Failed Got from API: ' . $result;
						$c++;*/
					}
					catch ( Exception $e) {
						/*$errors[] = 'Try #' . $c . ' Failed with Exception: ' . serialize($e);
											
						$c++;*/
					}
				//}
							
				if ( count($errors) ) {
					Cubix_Debug::log(implode("\n", $errors), 'ERROR');
				}
							return;
			}
		}

		/* Gallery Viewer */
		public static function hit_gallery($escort_id)
		{
			$cache = self::_getCache();

			$data = $cache->load(self::HITS_GALLERY_CACHE_KEY);

			if ( !is_array($data) ) {
				$data = array();
			}

			if ( !isset($data[$escort_id]) || !isset($data[$escort_id][1]) ) {
				$data[$escort_id] = array(time(), 0);
			}
					$data[$escort_id][1] = $data[$escort_id][1]+1;

			$total = 0;
			foreach ( $data as $escort_id => $row ) {
				$total += $data[$escort_id][1];
			}

			$cache->save($data, self::HITS_GALLERY_CACHE_KEY);
			$cache_limit = Zend_Registry::get('system_config');
			$errors = array();

			if ( $total >= $cache_limit['hitsCacheLimit'] ) {
				$client = Cubix_Api_XmlRpc_Client::getInstance();
				try {
					$result = $client->call('Escorts.updateGalleryHitsCount', array($data));
					if ( $result === true ) {
						$cache->remove(self::HITS_GALLERY_CACHE_KEY);
						return;
					}
				}
				catch ( Exception $e) {
				}
				if ( count($errors) ) {
					Cubix_Debug::log(implode("\n", $errors), 'ERROR');
				}
				return;
			}
			return;
		}

		public static function getCachedHits($escort_id)
		{
			$data = self::_getCache()->load(self::HITS_CACHE_KEY);

			if ( is_array($data) && isset($data[$escort_id]) ) {
				return $data[$escort_id][1];
			}

			return 0;
		}

		public static function getHitsCount($escort_id)
		{
			$client = Cubix_Api_XmlRpc_Client::getInstance();

			$cached = self::getCachedHits($escort_id);
			//$real = $client->call('Escorts.getHitsCount', array($escort_id));
			$real = intval(self::getAdapter()->fetchOne('SELECT hit_count FROM escorts WHERE id = ?', $escort_id));

			return $real + $cached;
		}

		public static function resetHits()
		{
			$cache = self::_getCache();
			$cache->remove(self::HITS_CACHE_KEY);
		}

		public static function getAllCachedHits()
		{
			$cache = self::_getCache();

			$data = $cache->load(self::HITS_CACHE_KEY);
			
			if ( ! $data ) $data = array();

			
			/*$client = Cubix_Api_XmlRpc_Client::getInstance();
			
			$c = 0;
			while ( $c <= 5 ) {
				try {
					$result = $client->call('Escorts.updateHitsCount', array($data));
					if ( $result === true ) {
						$cache->remove(self::HITS_CACHE_KEY);
						break;
					}

					ob_start();
					var_dump($result);
					$result = ob_get_clean();

					$errors[] = 'Try #' . $c . ' Failed Got from API: ' . $result;
					$c++;
				}
				catch ( Exception $e) {
					$errors[] = 'Try #' . $c . ' Failed with Exception: ' . serialize($e);
					$c++;
				}
			}
			if ( count($errors) ) {
				Cubix_Debug::log(implode("\n", $errors), 'ERROR');
			}*/

			return $data;
		}

		protected static $_cache;

		const HITS_CACHE_KEY = 'v2_escort_profile_views_a6';
		const HITS_GALLERY_CACHE_KEY = 'v2_escort_gallery_views_a6';

		/**
		 *
		 * @return Zend_Cache_Backend_Memcached
		 */
		protected static function _getCache()
		{
			return Zend_Registry::get('cache');
		}

		public function getProfile($escort_id)
		{
			return new Model_Escort_Profile(array('id' => $escort_id));
		}
		
		public function getProfiles($escort_ids)
		{
			if (strlen($escort_ids) > 0)
				return self::getAdapter()->query("SELECT * FROM escort_profiles WHERE escort_id IN (" . $escort_ids . ")")->fetchAll();
			
			return array();
		}

		static public function getMonthGirls($page, $per_page, $history = false)
		{
			$cache_key = 'ef_' . Cubix_Application::getId() . '_girls_of_the_month' . ($history ? '_history' : '') . '_page_' . $page;
			$cache = Zend_Registry::get('cache');

			$client = Cubix_Api::getInstance();
			
			if ( ! $result = $cache->load($cache_key) ) {
				$result = $client->call('getMonthGirls', array(Cubix_I18n::getLang(), $page, $per_page, $history));
							/*if(isset($_GET['test'])){
									return $history;
							}*/
				foreach ( $result['escorts'] as $i => $escort ) {
					$result['escorts'][$i] = new Model_EscortV2Item($escort);
				}
				
				$cache->save($result, $cache_key, array(), 3600);
			}

			return $result;
		}

		static public function getCurrentGOTM()
		{
			$cache_key = 'a6_' . Cubix_Application::getId() . '_current_gotm';
			$cache = Zend_Registry::get('cache');

			$client = Cubix_Api::getInstance();

			if ( ! $escort = $cache->load($cache_key) ) {
				try {
					$escort = $client->call('getCurrentGOTM', array(Cubix_I18n::getLang()));
				} catch ( Exception $e ) {}
				if ( ! $escort ) return false;
				
				$escort = new Model_EscortV2Item($escort);
				$cache->save($escort, $cache_key, array(), 3600);
			}

			return $escort;
		}
		
		public function getAllGOTD($region_slug = 'deutschschweiz')
		{
			$cache_key = 'a6_' . Cubix_Application::getId() . '_current_gotd' . $region_slug;
			$cache = Zend_Registry::get('cache');

			if ( ! $escorts = $cache->load($cache_key) ) {
				try {
					self::getAdapter()->query('SET NAMES `utf8`');
					$escorts = self::getAdapter()->fetchAll('
						SELECT
							e.id, e.showname,
							
							ep.hash AS photo_hash, ep.ext AS photo_ext, ep.status AS photo_status,
							
							epg.hash AS photo_hash_g,
							epg.ext AS photo_ext_g,
							epg.status AS photo_status_g,

							16 AS application_id,
							e.age,
							c.title_de AS city,
							e.gotd_city_id
						FROM escorts e
						INNER JOIN cities c ON c.id = e.gotd_city_id
						INNER JOIN regions r ON r.id = c.region_id
						LEFT JOIN escort_photos epg ON epg.escort_id = e.id AND epg.gotd = 1
						LEFT JOIN escort_photos ep ON ep.escort_id = e.id AND ep.is_main = 1
						WHERE FIND_IN_SET(15, e.products) AND r.slug = ?
						GROUP BY e.id
					', array($region_slug),Zend_DB::FETCH_ASSOC);
				} catch ( Exception $e ) {}
				if ( ! $escorts ) return false;
				
				$cache->save($escorts, $cache_key, array(), 3600);
			}

			$escort = false;
			$escorts_count = count($escorts);
			
			if ( $escorts_count ) {
				$id = rand (0, $escorts_count - 1);
				
				if ( isset($escorts[$id]) ) {
					$escort = $escorts[$id];
					
					$photo_hash_g = $escort['photo_hash_g'];
					$photo_ext_g = $escort['photo_ext_g'];
					$photo_status_g = $escort['photo_status_g'];
					
					if ( $photo_hash_g ) {
						$escort['photo_hash'] = $photo_hash_g;
						$escort['photo_ext'] = $photo_ext_g;
						$escort['photo_status'] = $photo_status_g;
					}
					
					$escort = new Model_EscortV2Item($escort);
				}
				
				
			}
			
			return $escort;
		}


			static public function getAllAgencies(){

			$sql = '
				SELECT agency_id,agency_name,agency_slug FROM escorts WHERE agency_id IS NOT NULL GROUP BY agency_id ORDER BY agency_name
			';

			$result = self::db()->fetchAll($sql);

			return $result;
			}
		
		static public function getRegionSlugById($escort_id) 
		{
			$sql = '
				SELECT r.slug AS region_slug FROM escorts e
				INNER JOIN cities c ON c.id = e.city_id
				INNER JOIN regions r ON r.id = c.region_id
				WHERE e.id = ?
			';

			$result = self::db()->fetchOne($sql, $escort_id);

			return $result;
			}

		public static function getLateNightGirls($page)
		{
			$perpage = 8;
			
			if (date('G') < 2)
				$date = 2;
			else
				$date = date('G');
				$today = date('N');
				$page = ($page-1)*$perpage;
			$escorts = self::getAdapter()->query("
				SELECT SQL_CALC_FOUND_ROWS e.id AS escort_id, e.showname, e.close_time, e.photo_hash AS hash, e.photo_ext AS ext, e.available_24_7
				FROM escorts_in_cities eic
				INNER JOIN escorts e ON e.id = eic.escort_id
				WHERE eic.gender = 1 AND ((e.available_24_7 = 1 AND e.night_escort = 1) OR (e.is_now_open = 1 AND e.close_time >= ? AND e.close_time < 24)
				OR (e.id IN (SELECT * FROM (SELECT escort_id FROM escort_working_times WHERE day_index='$today' AND night_escorts='1' LIMIT $page, $perpage) AS a)))
				GROUP BY e.id ORDER BY e.ordering LIMIT ?, ?
			", array($date,$page, $perpage))->fetchAll();

			$count = self::getAdapter()->fetchOne('SELECT FOUND_ROWS()');
			
			/*$count = self::getAdapter()->fetchOne('
				SELECT DISTINCT(COUNT(e.id))
				FROM escorts_in_cities eic
				INNER JOIN escorts e ON e.id = eic.escort_id
				WHERE eic.gender = 1 AND ((e.available_24_7 = 1 AND e.night_escort = 1) OR (e.is_now_open = 1 AND e.close_time >= ? AND e.close_time < 24))
			', array($date));*/

			return array($escorts, $count);
		}

		private static function _mapSorting($param)
		{
			$map = array(
				'price-asc' => 'e.incall_price ASC',
				'price-desc' => 'e.incall_price DESC',
				'random' => 'eic.is_premium DESC, eic.ordering DESC',
				'alpha' => 'e.showname ASC',
				'most-viewed' => 'e.hit_count DESC',
				'newest' => 'e.date_registered DESC',
				'last-modified' => 'e.date_last_modified DESC'
			);

			$order = 'e.ordering DESC';
			if ( isset($map[$param]) ) {
				$order = $map[$param];
			}
			elseif ( false !== strpos($param, 'FIELD') ) {
				$order = $param;
			}

			return $order;
		}
		
		public function getAlertEscrots($escorts)
		{
					$escorts = self::getAdapter()->fetchAll('
				SELECT e.id, e.showname, e.photo_hash, e.photo_ext AS photo_ext, 2 as application_id, e.verified_status, e.is_new 
				FROM escorts e 
				WHERE e.id IN (' . $escorts . ')
			',array());
			
			$newEscorts = array();
			
					if ( $escorts ){
							foreach ( $escorts as &$escort ){
									$escort->application_id = Cubix_Application::getId();
									$escort->photo_status = 3;
									$escort = new Model_EscortV2Item($escort);
					$newEscorts[$escort->id] = $escort;
							}
					}

					return $newEscorts;
			}
		
		public function getChat11OnlineEscorts($page = 1, $per_page = 10)
		{
			$cache_key = 'a6_' . Cubix_Application::getId() . '_chat_1_1_online_escorts';
			$cache = Zend_Registry::get('cache');

			if ( ! $result = $cache->load($cache_key) ) 
			{
				$db2 = Zend_Db::factory('mysqli', array(
					'host' => '172.16.0.101',
					'username' => 'a6_cubix',
					'password' => '123456',
					'dbname' => 'and6ch_arrow'
				));

				$sql = "
					SELECT  
						e.id AS escort_id
					FROM users
					LEFT JOIN escorts e ON users.id = e.user_id
					JOIN arrowchat_status ON users.id = arrowchat_status.userid 
					WHERE ('" . time() . "' - arrowchat_status.session_time - 60 < 120) AND users.user_type = 'escort'
					GROUP BY e.id
				";

				$escs = $db2->query($sql)->fetchAll();
				$arr_indep = array();

				foreach ($escs as $esc)
					$arr_indep[] = $esc['escort_id'];
				
				$sql = "
					SELECT  
						e.id AS escort_id
					FROM users
					LEFT JOIN escorts e ON users.id = e.user_id
					JOIN arrowchat_status ON users.id = arrowchat_status.userid 
					WHERE ('" . time() . "' - arrowchat_status.session_time - 60 < 120) AND users.user_type = 'agency'
					GROUP BY e.id
				";

				$escs = $db2->query($sql)->fetchAll();
				$arr_ag = array();

				foreach ($escs as $esc)
					$arr_ag[] = $esc['escort_id'];

				if (count($arr_indep) > 0 || count($arr_ag) > 0)
				{
					$lng = Cubix_I18n::getLang();
					
					if (count($arr_indep) > 0)
					{
						$escs_str_indep = implode(',', $arr_indep);
						$escorts_indep = self::getAdapter()->fetchAll("
							SELECT SQL_CALC_FOUND_ROWS
							e.id AS id, e.gender, e.showname, e.photo_hash, e.user_id,
							e.photo_ext AS photo_ext, " . Cubix_Application::getId() . " AS application_id, e.is_online, ct.title_en AS city
							FROM escorts e
							INNER JOIN cities ct ON ct.id = e.city_id
							WHERE e.id IN (" . $escs_str_indep . ") AND FIND_IN_SET(1, e.products) > 0
							LIMIT ?,?",array(($page - 1) * $per_page, $per_page));

						$count_indep = self::getAdapter()->fetchOne('SELECT FOUND_ROWS()');
					}
					else
					{
						$escorts_indep = array();
						$count_indep = 0;
					}
					
					if (count($arr_ag) > 0)
					{
						$escs_str_ag = implode(',', $arr_ag);
						$escorts_ag = self::getAdapter()->fetchAll("
							SELECT SQL_CALC_FOUND_ROWS
							e.id AS id, e.gender, e.showname, e.photo_hash, e.user_id,
							e.photo_ext AS photo_ext, " . Cubix_Application::getId() . " AS application_id, e.is_online, ct.title_en AS city
							FROM escorts e
							INNER JOIN cities ct ON ct.id = e.city_id
							INNER JOIN agencies_online_escorts ao ON ao.escort_id = e.id
							WHERE e.id IN (" . $escs_str_ag . ") AND FIND_IN_SET(1, e.products) > 0
							LIMIT ?,?",array(($page - 1) * $per_page, $per_page));

						$count_ag = self::getAdapter()->fetchOne('SELECT FOUND_ROWS()');
					}
					else
					{
						$escorts_ag = array();
						$count_ag = 0;
					}
					
					$escorts = array_merge($escorts_indep, $escorts_ag);
					
					$count = $count_indep + $count_ag;
					$escs_ids_arr = array();
					
					if ( $escorts ){
						foreach ( $escorts as &$escort ){
							$escort->application_id = Cubix_Application::getId();
							$escort->photo_status = 3;
							$escort = new Model_EscortV2Item($escort);
							$escs_ids_arr[] = $escort->id;
						}
					}
					
					$escs_ids = implode(',', $escs_ids_arr);
				}
				else
				{
					$escorts = null;
					$count = 0;
					$escs_ids = '';
				}
				
				$result = array('escorts' => $escorts, 'count' => $count, 'ids' => $escs_ids);

				$cache->save($result, $cache_key, array(), 180);
			}

				return $result;
		}

		public function getOnlineEscrots($page = 1, $per_page = 10, &$count = 0){
			$lng = Cubix_I18n::getLang();
			
			$escorts = self::getAdapter()->fetchAll('
				SELECT SQL_CALC_FOUND_ROWS
					e.id AS id, e.gender, e.showname, e.photo_hash, e.user_id,
					e.photo_ext AS photo_ext, ' . Cubix_Application::getId() . ' AS application_id, e.is_online, ct.title_de AS city
				FROM users_last_refresh_time ulrt
				INNER JOIN escorts e ON e.user_id = ulrt.user_id AND e.gender = 1
				INNER JOIN cities ct ON ct.id = e.city_id
				WHERE ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -10 MINUTE) AND FIND_IN_SET(1, e.products) > 0
				ORDER BY ulrt.refresh_date DESC
				LIMIT ?,?',array(($page - 1) * $per_page, $per_page));

			$count = self::getAdapter()->fetchOne('SELECT FOUND_ROWS()');

			if ( $escorts ){
					foreach ( $escorts as &$escort ){
							$escort->application_id = Cubix_Application::getId();
							$escort->photo_status = 3;
							$escort = new Model_EscortV2Item($escort);
					}

			}

		return $escorts;
	}

	public static function getChatOnlineUsersCount(){
			return self::getAdapter()->fetchOne('
		SELECT online
		FROM online_in_chat');
	}
		
	public function getChatNodeOnlineEscorts($page = 1, $per_page = 10)
	{
		//$user_ids = file_get_contents(Cubix_Application::getById()->url . '/node-chat/get-online-escorts');
		$cache_native = Zend_Registry::get('cache_native');
		$user_ids = $cache_native->get(Cubix_Application::getId() . '_chat_online_escorts');
		$user_ids = json_decode($user_ids);
		
		if (count($user_ids) > 0)
		{
			$user_ids_str = implode(',', $user_ids);
		
			$escorts = self::getAdapter()->fetchAll("
				SELECT SQL_CALC_FOUND_ROWS
					e.id AS id, e.gender, e.showname, e.photo_hash, e.user_id,
					e.photo_ext AS photo_ext, " . Cubix_Application::getId() . " AS application_id, e.is_online, ct.title_en AS city
				FROM escorts e
				INNER JOIN cities ct ON ct.id = e.city_id
				WHERE e.user_id IN (" . $user_ids_str . ") AND FIND_IN_SET(1, e.products) > 0
				GROUP BY e.id
				LIMIT ?,?",array(($page - 1) * $per_page, $per_page));
			
			$count = self::getAdapter()->fetchOne('SELECT FOUND_ROWS()');

			$escs_ids_arr = array();

			if ( $escorts ){
				foreach ( $escorts as &$escort ){
					$escort->application_id = Cubix_Application::getId();
					$escort->photo_status = 3;
					$escort = new Model_EscortV2Item($escort);
					$escs_ids_arr[] = $escort->id;
				}
			}

			$escs_ids = implode(',', $escs_ids_arr);
		}
		else
		{
			$escorts = null;
			$count = 0;
			$escs_ids = '';
		}

		$result = array('escorts' => $escorts, 'count' => $count, 'ids' => $escs_ids);

		return $result;
	}

	public static function getChatOnlineEscorts(){
		$escorts = self::getAdapter()->fetchAll('
			SELECT e.id, e.showname,e.photo_hash, e.photo_ext
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			WHERE e.gender = 1 AND e.is_online = 1 AND eic.is_base = 1 GROUP BY e.id');
		if ( $escorts ){
			foreach ( $escorts as &$escort ){
				$escort->application_id = Cubix_Application::getId();
				$escort->photo_status = 3;
				$escort = new Model_EscortV2Item($escort);
			}
		}

		$count = self::getAdapter()->fetchOne('
			SELECT COUNT(DISTINCT(e.id))
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			WHERE e.gender = 1 AND e.is_online = 1 AND eic.is_base = 1 ');

			return array($escorts, $count);
	}

	public static function getHappyHourEscorts()
	{

		$cache_key = 'ef_' . Cubix_Application::getId() . '_current_hh_escorts';
		$cache = Zend_Registry::get('cache');

		if ( ! $result = $cache->load($cache_key) ) {
			$escorts = self::getAdapter()->fetchAll('
				SELECT e.id, e.showname, e.photo_hash, e.photo_ext,
				UNIX_TIMESTAMP(e.hh_date_from) AS hh_date_from, UNIX_TIMESTAMP(e.hh_date_to) AS hh_date_to
				FROM escorts_in_cities eic
				INNER JOIN escorts e ON e.id = eic.escort_id
				WHERE eic.gender = 1 AND e.hh_is_active = 1 AND eic.is_base = 1 GROUP BY e.id ORDER BY RAND() LIMIT 0 ,2');
			if ( $escorts ){
				foreach ( $escorts as &$escort ){
					$escort->application_id = Cubix_Application::getId();
					$escort->photo_status = 3;
					$escort = new Model_EscortV2Item($escort);
				}
			}

			$count = self::getAdapter()->fetchOne('
				SELECT COUNT(DISTINCT(e.id))
				FROM escorts_in_cities eic
				INNER JOIN escorts e ON e.id = eic.escort_id
				WHERE eic.gender = 1 AND e.hh_is_active = 1 AND eic.is_base = 1');
			
			$result =  array($escorts, $count);

			$cache->save($result, $cache_key, array(), 60);
		}

		return $result;
	}

	public static function getColorByGotdCityId($city_id)
	{
		$color = 'red';
		
		// Zurich sector
		if ( $city_id == 45 || $city_id == 17 ) {
			$color = 'blue';
		}
		//Basel und Umgebung
		else if ( $city_id == 19 ) {
			$color = 'red';
		}
		//W'thur -Thurgau-St.Gallen
		else if ( $city_id == 18 ) {
			$color = 'green';
		}
		// Bern sector
		else if ( $city_id == 21 || $city_id == 22 ) {
			$color = 'orange';
		}
		//Luzern - Innerschweiz
		else if ( $city_id == 24 ) {
			$color = 'pink';
		}
		//Mittelland - AG - SO
		else if ( $city_id == 20 ) {
			$color = 'turquois';
		}
		//	Biel - Grenchen - Jura | Wallis | Graubünden - Glarus sectors
		else if ( $city_id == 25 || $city_id == 26 || $city_id == 23 ) {
			$color = 'yellow';
		}
		
		return $color;
	}

        public static function getColorByVotdCityId($city_id)
        {
            $color = 'red';

            // Zurich sector
            if ( $city_id == 45 || $city_id == 17 ) {
                $color = 'blue';
            }
            //Basel und Umgebung
            else if ( $city_id == 19 ) {
                $color = 'red';
            }
            //W'thur -Thurgau-St.Gallen
            else if ( $city_id == 18 ) {
                $color = 'pink';
            }
            // Bern sector
            else if ( $city_id == 21 || $city_id == 22 ) {
                $color = 'blue';
            }
            //Luzern - Innerschweiz
            else if ( $city_id == 24 ) {
                $color = 'pink';
            }
            //Mittelland - AG - SO
            else if ( $city_id == 20 ) {
                $color = 'red';
            }
            //	Biel - Grenchen - Jura | Wallis | Graubünden - Glarus sectors
            else if ( $city_id == 25 || $city_id == 26 || $city_id == 23 ) {
                $color = 'blue';
            }

            return $color;
        }

	public function getBlockWebsite($escort_id)
	{
		return $this->_db->fetchRow('SELECT block_website, cam_booking_status FROM escorts WHERE id = ?', $escort_id);
	}

	public function isSuspicious($user_id)
	{
		$s = $this->_db->fetchOne('SELECT is_suspicious FROM escorts WHERE user_id = ?', $user_id);
		
		if ($s == 0)
			return false;
		else
			return true;
	}

	public function getAgencyPaidEscorts($user_id)
	{
		return $this->_db->query('SELECT id, showname, cam_booking_status FROM escorts WHERE user_id = ? AND FIND_IN_SET(1, products) > 0', $user_id)->fetchAll();
	}

	public function getAgencyOnlineEscorts($user_id)
	{
		return $this->_db->query('SELECT * FROM agencies_online_escorts WHERE user_id = ?', $user_id)->fetchAll();
	}

	public function addAgencyOnlineEscorts($user_id, $escorts)
	{
		$this->_db->delete('agencies_online_escorts', $this->_db->quoteInto('user_id = ?', $user_id));
		
		if (count($escorts) > 0)
		{
			foreach ($escorts as $esc)
			{
				$this->_db->insert('agencies_online_escorts', array('user_id' => $user_id, 'escort_id' => intval($esc)));
			}
		}
	}

	public function getCoord($escort_id)
	{
		return $this->_db->query('SELECT latitude, longitude FROM escorts WHERE id = ?', $escort_id)->fetch();
	}

	public function getLoginEscorts()
	{
		return $this->_db->query('
			SELECT ulrt.escort_id, e.showname 
			FROM users_last_refresh_time ulrt
			INNER JOIN escorts e ON e.user_id = ulrt.user_id
			WHERE ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -10 MINUTE) AND (e.agency_id IS NULL OR LENGTH(e.agency_id) = 0)
			ORDER BY ulrt.refresh_date DESC
		')->fetchAll();
	}

	public function getNewEscortsCities($region)
	{

        $g_where = Cubix_Countries::blacklistCountryWhereCase();

		return $this->_db->query('			
			SELECT ct.title_' . Cubix_I18n::getLang() . ' AS title, ct.slug, cn.iso, e.city_id 
			FROM escorts_in_cities eic 
			INNER JOIN escorts e ON e.id = eic.escort_id 
			INNER JOIN cities ct ON ct.id = e.city_id 
			INNER JOIN countries cn ON cn.id = ct.country_id 
			LEFT JOIN regions r ON r.id = eic.region_id 
			WHERE r.slug = ? ' . $g_where . '  AND e.is_new = 1 AND (e.gender = 1 OR e.gender = 3) AND eic.is_upcoming = 0 
			GROUP BY e.id 
			ORDER BY ct.id DESC
		', $region)->fetchAll();
	}

	public function getApps($escort_id)
	{
		return $this->_db->query('
			SELECT
				viber, whatsapp, telegram
			FROM escorts
			WHERE id = ?
		', $escort_id)->fetch();
	}

	public function getVideoChatInfo($escort_id, $duration){
		return $this->_db->fetchRow('SELECT id, showname FROM escorts WHERE id = ?', array($escort_id));
	}
	
	public function getAgencyId($escort_id){
		return parent::getAdapter()->fetchOne('SELECT agency_id FROM escorts WHERE id = ?', $escort_id);
	}

	public function isPseudoEscort($escort_id)
	{
		return parent::getAdapter()->fetchOne('SELECT TRUE FROM escorts WHERE id = ? AND pseudo_escort = 1', $escort_id);
	}
	
	public function eligible_to_have_5_days($escort_id)
	{
		return parent::getAdapter()->fetchRow('SELECT escort_id FROM free_5_days WHERE used is null AND escort_id = ?',  $escort_id);
	}

	public static function activate_5_days($escort_id)
	{
		return parent::getAdapter()->update('free_5_days', array('activation_date' => new Zend_Db_Expr('NOW()'),'used' => 1) , parent::getAdapter()->quoteInto('escort_id = ?', $escort_id));
	}

    public static function escortBoysExist(){
            $countSql = '
				SELECT
					COUNT(DISTINCT(e.id)) AS `count`
				FROM escorts e
				INNER JOIN escorts_in_cities eic ON eic.escort_id = e.id
				INNER JOIN countries c ON c.id = e.country_id
				INNER JOIN cities cc ON cc.id = eic.city_id OR e.tour_city_id = cc.id
				LEFT JOIN regions r ON r.id = cc.region_id
				LEFT JOIN cityzones cz ON cz.id = e.cityzone_id
				WHERE  e.gender = 2';
        return parent::getAdapter()->fetchOne($countSql);
	}


		/**
		 * Check escort have or not video
		 *
		 * @param integer $escortId
		 * @return boolean
		 *
		 */
	public static function hasVideo($escortId){
			if(!is_numeric($escortId) || $escortId == 0 ) return false;

			$client = new Cubix_Api_XmlRpc_Client();

			return $client->call('Escorts.hasVideo', array($escortId));
	}
	
	public function getPaxumEmail($escort_id){
		$client = new Cubix_Api_XmlRpc_Client();

		$result = $client->call('Escorts.getPaxumEmail', array($escort_id));
		
		return $result;	
	}
	
	public function updateCamStatus($escort_id, $data){
		$client = new Cubix_Api_XmlRpc_Client();

		$result = $client->call('Escorts.updateCamStatus', array($escort_id, $data));
		
		return $result;	
	}
}

/**
 * Merges any number of arrays / parameters recursively, replacing
 * entries with string keys with values from latter arrays.
 * If the entry or the next value to be assigned is an array, then it
 * automagically treats both arguments as an array.
 * Numeric entries are appended, not replaced, but only if they are
 * unique
 *
 * calling: result = array_merge_recursive_distinct(a1, a2, ... aN)
**/

function array_merge_recursive_distinct () {
  $arrays = func_get_args();
  $base = array_shift($arrays);
  if(!is_array($base)) $base = empty($base) ? array() : array($base);
  foreach($arrays as $append) {
    if(!is_array($append)) $append = array($append);
    foreach($append as $key => $value) {
      if(!array_key_exists($key, $base) and !is_numeric($key)) {
        $base[$key] = $append[$key];
        continue;
      }
      if(is_array($value) or is_array($base[$key])) {
        $base[$key] = array_merge_recursive_distinct($base[$key], $append[$key]);
      } else if(is_numeric($key)) {
        if(!in_array($value, $base)) $base[] = $value;
      } else {
        $base[$key] = $value;
      }
    }
  }
  return $base;
}
