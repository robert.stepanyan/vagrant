<?php
class Model_Twispay
{
	 
	public function __construct()
	{
	 
	}

	public function generateChecksum($data, $key)
	{
		// - taking the list of the submitted fields, order this list ascending by the field names;
		// - if the value of a field itself is a list of values, recursively order the list of values by their key names;
		ksort($data, SORT_STRING);

		// - with the ordered list of fields and field values, build a URL encoded query string, according to RFC 1738;
		// $qs = http_build_query($data);
		
		// - with the result of the previous steps, generate a HMAC (hash-based message authentication code) using the SHA-512 hashing algoritm and your secret key;
		// $hh = hash_hmac('sha512', $qs, $key , true);
		
		// - taking the generated HMAC in binary format, encode it in base64.
		// $checksum = base64_encode($hh);
		$checksum = base64_encode(hash_hmac('sha512', http_build_query($data), $key, true));
		return $checksum;
	}

	function decrypt($data, $key)
    {
    	// - the value of the `result` parameter need to be BASE64 decoded;
        $data = base64_decode($data);

        // - with the result, take the first 32 characters, that's the IV (initialization vector);
        $iv = substr($data, 0, 32);

        // - take the rest of the result and the IV, using RIJNDAEL 256 cypher, mode CBC, decrypt the value;
        $crypted = substr($data, 32);

        // - the resulted decrypted value is the data, in JSON format.
        return mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $crypted, MCRYPT_MODE_CBC, $iv);
    }




}

