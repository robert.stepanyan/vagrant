<?php

class Model_VideoChatRequests extends Cubix_Model
{
	public function add($data)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('CamBooking.save', array($data));
	}
	
	public function update($id, $data)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('CamBooking.update', array($id, $data));
	}
	
	public function notificationsCam($escort, $membername,  $additional, $request_id)
	{
		$duration = $additional['duration'];
		$phone = $additional['contact_phone_parsed'];
		$customer_email = $additional['email'];

		 // $phone = '0037455927724';
		 // $customer_email = 'mihranhayrapetyan@gmail.com';

		$token_count = $additional['amount']*10;
		if ($additional['schedule_time'] == 'ASAP' || strlen($additional['schedule_time']) > 10)
        {
            $schedule_time = Cubix_I18n::translateByLng('de', $additional['schedule_time']);
        }else{
            $schedule_time = Cubix_I18n::translateByLng('de', 'at_time')." ".$additional['schedule_time'];
        }

		$text = Cubix_I18n::translateByLng('de', 'cam_sms', array('membername'=> $membername,'time'=>$additional['duration'], 'amount'=> $additional['amount'], 'count'=> $token_count,'schedule_time' => $schedule_time, 'count_per_min' => round($token_count / $additional['duration'])));
		
		$conf = Zend_Registry::get('system_config');
		$conf = $conf['sms'];
		$SMS_DeliveryNotificationURL = $conf['DeliveryNotificationURL'];
		$SMS_NonDeliveryNotificationURL = $conf['NonDeliveryNotificationURL'];
		$SMS_BufferedNotificationURL = $conf['BufferedNotificationURL'];
		
		// Get from number
		if(isset($phone)){
			$originator = $phone_from = Cubix_Application::getPhoneNumber(Cubix_Application::getId());
		
			$sms_id = Cubix_Api::getInstance()->call('outbox', array($escort['escort_id'], $phone, $originator, $text, Cubix_Application::getId(), null, 1));
						
			$sms = new Cubix_SMS($conf['userkey'], $conf['password']);
			$sms->setBufferedNotificationURL($SMS_BufferedNotificationURL);
			$sms->setDeliveryNotificationURL($SMS_DeliveryNotificationURL);
			$sms->setNonDeliveryNotificationURL($SMS_NonDeliveryNotificationURL);
			$sms->setOriginator($originator);
			$sms->setRecipient($phone, $sms_id);
			$sms->setContent($text);
			$sms->sendSMS();
		}
	
		
		
		$client = new Cubix_Api_XmlRpc_Client();
		$client->call('VideoChatRequests.update', array($request_id, array('sms_text' => $text)) );
		
		//SENDING EMAIL TO CUSTOMER 
		$email_subject = __('cam_email_subject', array('showname'=> $escort['showname']) );
		Cubix_Email::sendTemplate('universal_message', $customer_email , array(
			'subject' => $email_subject,
			'message' => $text,
            'user_id' => $additional['user_id'],
            'backend_user' => $additional['backend_user']
		));
	}
	
	public function checkBySession($id, $session){
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('CamBooking.checkBySession', array($id, $session));
	}
	
	public function getStatus($request_id){
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('CamBooking.getStatus', array($request_id));
	}
	
	public function getData($escort_id){
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('CamBooking.getData', array($escort_id));
	}
	
	public function getPrice($escort_id, $duration){
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('CamBooking.getPrice', array($escort_id, $duration));
	}
	
	public function getForSuccess($request_id){
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('CamBooking.getForSuccess', array($request_id));
	}
	// public function sendSms($escort, $name, $skype, $customer_email, $request_id)
	// {
	// 	$phone =  $escort['phone'];
	// 	//$phone = '0037499555538';
	// 	//var_dump($escort['phone'], $escort['email']);
	// 	if($escort['user_type'] == 'agency'){
	// 		$sms_text = Cubix_I18n::translateByLng('de', 'video_call_agency_sms_text', array(
	// 			'user' => $escort['username'],
	// 			'escort' => $escort['showname'],
	// 			'name' => $name,
	// 			'skype' => $skype,
	// 			'duration' => $escort['duration'],
	// 		));
			
	// 		$email_content = Cubix_I18n::translateByLng('de', 'video_call_agency_email_text', array(
	// 			'user' => $escort['username'],
	// 			'escort' => $escort['showname'],
	// 			'name' => $name,
	// 			'skype' => $skype,
	// 			'duration' => $escort['duration'],
	// 		));
	// 	}
	// 	else{
	// 		$sms_text = Cubix_I18n::translateByLng('de', 'video_call_indep_sms_text', array(
	// 			'name' => $name,
	// 			'skype' => $skype,
	// 			'duration' => $escort['duration'],
	// 		));
			
	// 		$email_content = Cubix_I18n::translateByLng('de', 'video_call_indep_email_text', array(
	// 			'user' => $escort['username'],
	// 			'name' => $name,
	// 			'skype' => $skype,
	// 			'duration' => $escort['duration'],
	// 		));
	// 	}
		
	// 	$conf = Zend_Registry::get('system_config');
	// 	$conf = $conf['sms'];

		
	// 	$SMS_DeliveryNotificationURL = $conf['DeliveryNotificationURL'];
	// 	$SMS_NonDeliveryNotificationURL = $conf['NonDeliveryNotificationURL'];
	// 	$SMS_BufferedNotificationURL = $conf['BufferedNotificationURL'];
		
	// 	// Get from number
	// 	$originator = $phone_from = Cubix_Application::getPhoneNumber(Cubix_Application::getId());
	// 	$sms_id = Cubix_Api::getInstance()->call('outbox', array($escort['id'], $phone, $originator, $sms_text, Cubix_Application::getId(), null, 1));
		
						
	// 	$sms = new Cubix_SMS($conf['userkey'], $conf['password']);
	// 	$sms->setBufferedNotificationURL($SMS_BufferedNotificationURL);
	// 	$sms->setDeliveryNotificationURL($SMS_DeliveryNotificationURL);
	// 	$sms->setNonDeliveryNotificationURL($SMS_NonDeliveryNotificationURL);
	// 	$sms->setOriginator($originator);
	// 	$sms->setRecipient($phone, $sms_id);
	// 	$sms->setContent($sms_text);
	// 	$sms->sendSMS();
		
	// 	$client = new Cubix_Api_XmlRpc_Client();
	// 	$client->call('CamBooking.update', array($request_id, array('sms_text' => $sms_text)) );
		
	// 	Cubix_Email::sendTemplate('universal_message', $escort['email'], array(
	// 		'subject' => 'Neue Skype-Video-Buchung!',
	// 		'message' => $email_content,
	// 		'user_id' =>  $escort['user_id'],
	// 		'backend_user' => $escort['sales_user_id']
	// 	));
		
	// 	//SENDING EMAIL TO CUSTOMER 
	// 	$email_content = Cubix_I18n::translateByLng('de', 'video_call_cutomer_email_text', array(
	// 		'name' => $name,
	// 		'skype' => $skype,
	// 		'duration' => $escort['duration'],
	// 	));
		
	// 	Cubix_Email::sendTemplate('universal_message', $customer_email , array(
	// 		'subject' => 'Deine Skype-Videobuchung mit '. $escort['showname'].' ('.$escort['id'].')',
	// 		'message' => $email_content
	// 	));
		
	// }
}
