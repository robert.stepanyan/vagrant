<?php

class Model_Credits extends Cubix_Model
{
	// Credit => Amount
	public static $bundles = [
		99 => 1, 
		50 => 50, 
		100 => 100,
		500 => 500,
		1000 => 1000
	];
	
	public function addRequest($data)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Credits.addRequest', array($data));
	}
}
