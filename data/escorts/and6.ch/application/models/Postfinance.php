<?php
class Model_Postfinance
{
	
	public $submitUrl = "https://e-payment.postfinance.ch/ncol/prod/orderstandard_utf8.asp";	
	public $submitUrlTest = "https://e-payment.postfinance.ch/ncol/test/orderstandard_utf8.asp";	
	public $PSPID = "and6com";
	public $PSPID_TEST = "and6comTEST";
	public $currency = "CHF";
	public $shaInPassphrase = "Hrzlp-/436)89wWr";
	public $shaOutPassphrase = "Hrzlp-/436)89wWr";

	public function __construct()
	{
	 
	}

	public function generateChecksum($data, $key)
	{
		// - All parameters have to be arranged alphabetically
		// - All parameter names should be in UPPERCASE
		ksort($data, SORT_STRING);

		$dataString = '';
		foreach($data as $key=>$value)
		{
			$dataString .= $key.'='.$value.$this->shaInPassphrase;
		}
		
		$checksum = hash('sha256', $dataString);
		return $checksum;
	}
}

