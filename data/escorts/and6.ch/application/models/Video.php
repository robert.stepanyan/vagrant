<?php


class Model_Video extends Cubix_Model {
	protected $_table = 'escorts';
	private $db;
	
	public $dimensions = array(
		"720" => array(
			'height' => 720, 'bitrate' => 1200
		),
		"480" => array(
			'height' => 480, 'bitrate' => 600
		),
		"360" => array(
			'height' => 360, 'bitrate' => 250
		),
        "240" => array(
			'height' => 240, 'bitrate' => 200
		)
	);
	
	public function __construct() 
	{
		
		$this->db = Zend_Registry::get('db');
	}

    public function getMainPhoto()
    {
        return new Model_Escort_PhotoItem(array(
            'application_id' => $this->application_id,
            'escort_id' => $this->id,
            'hash' => $this->photo_hash,
            'ext' => $this->photo_ext,
            'photo_status' => $this->photo_status
        ));
    }

	public function GetEscortVideo($escort_id, $private = TRUE)
	{
		$escort_id = $this->db->quote($escort_id);
		if($private)
		{	
			$client = Cubix_Api_XmlRpc_Client::getInstance();
			return  $client->call('Application.GetVideo',array($escort_id));
		}
		else
		{
			$sql_video = "SELECT * FROM video WHERE escort_id = ?";
			$video = parent::_fetchAll($sql_video, $escort_id);
			
			$photo = array();
			if(!empty($video))
			{
				$v_id = array();
				foreach ($video as $v)
				{
					$v_id[] = $v->id;
				}
				$v_id = implode(',', $v_id);
				$sql_image = "SELECT * FROM video_image  WHERE video_id IN ($v_id) ";
				$photo =  parent::_fetchAll($sql_image);
			}
			return array($photo,$video);
		}
	}
	
	public function GetEscortHasApprovedVideo($escort_id)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		return  $client->call('Application.GetEscortHasApprovedVideo',array($escort_id));
	}

	public function GetEscortsVideoArray(array $escorts)
	{	
		$escorts = implode(',', $escorts);
		$sql = "SELECT video,`hash`,width,height,escort_id,ext FROM video_image AS img 
				JOIN (SELECT video,escort_id,id FROM video WHERE escort_id IN($escorts) ORDER BY `date` DESC ) AS v 
				ON v.id=img.video_id 
				GROUP BY escort_id ORDER BY img.`date` DESC ";
		return parent::_fetchAll($sql);
	}
	
	public function RemoveV2($video_id,$escort_id)
	{	
		$client = Cubix_Api_XmlRpc_Client::getInstance();	
		$affected_video_id = $client->call('Application.RemoveVideo',array($escort_id, $video_id));
		if($affected_video_id){
			$this->db->query("DELETE FROM video WHERE escort_id = ? AND id = ? ",array($escort_id, $video_id ));
		}
		return $affected_video_id;
		
	}
	
	public function GetAgencyEscort($agency_id)
	{	
		$escorts =parent::_fetchAll("SELECT showname,id FROM  escorts WHERE agency_id=?  ",$agency_id);

		if(!empty($escorts))
		{	
			$client = Cubix_Api_XmlRpc_Client::getInstance();
			$escorts=$client->call('Application.GetAgencyEscortVideo',array($agency_id,$escorts));
		}
		return $escorts;
	}
	
	public function GetAgencyEscorts($id,$escort_id)
	{	
		$escort_id = $this->db->quote($escort_id);
		$escorts = parent::_fetchAll("SELECT showname,id FROM escorts WHERE agency_id='$id' AND id=$escort_id ");
		return empty($escorts)?FALSE:TRUE;
	}


	public function UserVideoCount($escort_id)
	{	
		$escort_id = $this->db->quote($escort_id);
		$count= parent::_fetchRow("SELECT count(id) AS quantity FROM video WHERE escort_id=$escort_id  GROUP BY escort_id ");
		return $count->quantity;
	}
	
	public function getManifestData($escort_id)
	{
		return parent::_fetchRow("SELECT video, height, width FROM video WHERE escort_id = ?", array($escort_id));
	}

	public function getVideos($filter, $page = 1, $per_page = 40 , $exclude = array())
	{
		$where = '';
		$order_by = '';
		$select = '';
		$where .= Cubix_Countries::blacklistCountryWhereCase();

		if (!empty($exclude)) {
			$ids = implode(',', $exclude);
			$where = ' AND v.id  NOT IN ('.$ids.')';
		}

		if(isset($filter['city_id']) && $filter['city_id']){
			$where .= " AND (eic.city_id = " . $filter['city_id'] . " OR v.votd_city_id = " . $filter['city_id'] . ")";
			$order_by .= "v.is_votd DESC, ";
			$select .= "v.is_votd, ";
		}
		
		$sql_data = "
			SELECT SQL_CALC_FOUND_ROWS ". $select ." e.showname, e.age, e.city_id, v.escort_id, v.video, "
				. Cubix_I18n::getTblField('c.title') . " as city , vi.hash, vi.ext, v.vote_count
			FROM video v 
			INNER JOIN escorts e ON v.escort_id = e.id
			INNER JOIN escorts_in_cities eic ON eic.escort_id = e.id
			INNER JOIN cities c ON eic.city_id = c.id
			INNER JOIN video_image vi ON v.id = vi.video_id
			WHERE e.has_video = 1" .$where ;

		$sql_data .= "
			GROUP BY v.id
			ORDER BY ". $order_by ."v.date DESC 
			LIMIT " . ($page - 1) * $per_page . ', ' . $per_page;

		$data = self::getAdapter()->fetchAll($sql_data);
		$count = self::getAdapter()->fetchOne('SELECT FOUND_ROWS()');
		
		foreach($data as $video){
			$photo = new Cubix_ImagesCommonItem($video);
			$video->photo = $photo->getUrl('video_thumb_m');
		}
		
		return array('data' => $data, 'count' => $count);
	}
	
	public function getAll($page = 1, $per_page = 50, $city_id = null, $exclude = array())
	{
		$lng = Cubix_I18n::getLang();
		$city_title = 'c.title_' . $lng;
		$where = '';

        $select = '';
		if ($city_id) {
			$select .= "v.is_votd, ";
		}
		$sql_count  = "
			SELECT " . $select . " v.id, c.id, COUNT(*) AS v_count, $city_title AS city_title

			FROM video v
			INNER JOIN escorts e ON v.escort_id = e.id
			INNER JOIN escorts_in_cities eic ON eic.escort_id = e.id
			INNER JOIN cities c ON eic.city_id = c.id
			WHERE e.has_video = 1

		";

		if ($city_id) {
			$sql_count .= " AND (c.id = " . $city_id . " OR v.votd_city_id = " . $city_id . ")";
		}
		
		if (!empty($exclude)) {
			$ids = implode(',', $exclude);
			$where = ' AND v.id  NOT IN ('.$ids.')';
		}

		$sql_count .= "
			GROUP BY c.id
		";

		$total_video_data = self::getAdapter()->fetchAll($sql_count);


		$sql_data = "
			SELECT " . $select . " e.showname, v.id AS video_id, v.escort_id, v.video, 
				UNIX_TIMESTAMP(v.date), vi.hash, vi.ext, c.*

			FROM video v 
			INNER JOIN escorts e ON v.escort_id = e.id
			INNER JOIN escorts_in_cities eic ON eic.escort_id = e.id
			INNER JOIN cities c ON eic.city_id = c.id
			INNER JOIN video_image vi ON v.id = vi.video_id

			WHERE e.has_video = 1" . $where;

		if ($city_id) {
			$sql_data .= " AND (c.id = " . $city_id . " OR v.votd_city_id = " . $city_id . ")";
			$votd_order = " is_votd DESC, ";
		}

		$sql_data .= "
			GROUP BY v.id
			ORDER BY " . $votd_order . "e.ordering ASC
			LIMIT " . ($page - 1) * $per_page . ', ' . $per_page;	
		
		$page_data = self::getAdapter()->fetchAll($sql_data);

		return array('page_data' => $page_data, 'total_video_data' => $total_video_data);
	}

	public static function getVideoVoteCount($video_id)
	{	
		$sql = "SELECT fingerprint FROM video_votes WHERE video_id = ?";
		return self::getAdapter()->fetchAll($sql, array($video_id));
	}

	public function voteForVideo($video_id, $fingerprint)
    {	
		$prevoted_video_id = $this->db->fetchOne('SELECT video_id FROM video_votes WHERE fingerprint = ?', $fingerprint);

		$sql = "INSERT INTO video_votes VALUES(?, ?) ON DUPLICATE KEY UPDATE video_id = ?";
		$this->db->query($sql, array($video_id, $fingerprint, $video_id ));

		return $prevoted_video_id;
    }

    public static function getTopVideos($count = 5) {

		$cache = Zend_Registry::get('cache');
		$cache_key = 'video_top_5';

		if (!$videos = $cache->load($cache_key)) {

			$sql = 'SELECT v.escort_id as id, vv.video_id, COUNT(DISTINCT vv.fingerprint) as vote_count, e.showname, vi.hash, vi.ext
				FROM 
				video_votes vv
				INNER JOIN video v ON v.id = vv.video_id
				INNER JOIN video_image vi ON vi.video_id = vv.video_id
				INNER JOIN escorts e ON v.escort_id = e.id
				INNER JOIN escorts_in_cities eic ON eic.escort_id = v.escort_id
				
				GROUP BY vv.video_id
				ORDER BY vote_count DESC
				LIMIT ' . $count;
			
			$videos = self::db()->fetchAll($sql);

				foreach($videos as $video){
				$photo = new Cubix_ImagesCommonItem($video);
				$video->photo = $photo->getUrl('video_thumb');
				
			}

			$cache->save($videos, $cache_key); 

		}
		
		return $videos;
	}	

	public function getRandomVotd(){
		$where = '';
		$where .= Cubix_Countries::blacklistCountryWhereCase();
		
		$sql = "
			SELECT  v.is_votd, e.showname, e.age, e.city_id, " . Cubix_I18n::getTblField('c.title') . " as city, v.id AS video_id, v.escort_id, v.video, 
				UNIX_TIMESTAMP(v.date), vi.hash, vi.ext, c.*

			FROM video v 
			INNER JOIN escorts e ON v.escort_id = e.id
			INNER JOIN escorts_in_cities eic ON eic.escort_id = e.id
			INNER JOIN cities c ON eic.city_id = c.id
			INNER JOIN video_image vi ON v.id = vi.video_id

			WHERE e.has_video = 1 AND v.is_votd = 1
			GROUP BY v.id
			ORDER BY rand() limit 1";

		$data = self::getAdapter()->fetchRow($sql);

		return $data;
	}

	public function getVotdByCityId($city_id)
	{
		$where = '';
		$where .= Cubix_Countries::blacklistCountryWhereCase();
		
		$where .= "AND v.votd_city_id = " . $city_id;
		
		$sql_data = "
			SELECT v.is_votd, e.showname, v.escort_id, v.video, vi.hash, vi.ext, v.vote_count
			FROM video v 
			INNER JOIN escorts e ON v.escort_id = e.id
			INNER JOIN video_image vi ON v.id = vi.video_id
			WHERE e.has_video = 1 " .$where ;

		
			
		$data = self::getAdapter()->fetchRow($sql_data);
		if($data){
			$photo = new Cubix_ImagesCommonItem($data);
			$data->photo = $photo->getUrl('video_thumb_m');
		}
		
		return $data;
	}
}
