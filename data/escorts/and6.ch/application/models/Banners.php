<?php

class Model_Banners
{
    const STATUS_PENDING    = 1;
    const STATUS_PAID       = 2;
    const STATUS_REJECTED   = 3;

	public static function getForMainPage($zone_name = null, $region_slug = null)
	{
		if ( ! $zone_name || ! $region_slug ) return "";

		$banner_mapping = array(
			'deutschschweiz' => array(
				'main_top_left' => '<div class="cF21Dg27" id="x37" data-limit="4"></div> <!-- DEUTSCHSCHWEIZ_home_middle_175x60 () -->',
				'main_top_right' => '<div id="x3439" class="top_right_banners cF21Dg27" data-limit="4"></div> <!-- DEUTSCHSCHWEIZ_home_right_top_354x255 () -->', 
				'main_middle' => '<div class="cF21Dg27" id="x38" data-limit="8"></div> <!-- DEUTSCHSCHWEIZ_home_left_175x125 () -->', 
				'main_bottom' => '<div class="cF21Dg27 small" data-limit="1" id="x323135"></div><div class="cF21Dg27 big" data-limit="1" id="x323136"></div>', 
				'main_popup' => '<div class="cF21Dg27" id="x3530" data-limit="1"></div> <!-- DEUTSCHSCHWEIZ_580x380 () -->', 
				'gotd_place' => '<div class="cF21Dg27" id="x3130" data-limit="1"><a href="http://www.sexyjobs.ch" target="_blank"><img src="./img/sexyjob_banner.png" width="175" height="175" /></a></div> <!-- DEUTSCHSCHWEIZ_GOTD_PLACE -->', 
			),
			'romandie' => array(
				'main_top_left' => '<div class="cF21Dg27" id="x3735" data-limit="4"></div> <!-- ROMANDIE_home_middle_175x60 () -->',
				'main_top_right' => '<div id="x3736" class="top_right_banners cF21Dg27" data-limit="4"></div> <!-- ROMANDIE_home_right_top_354x255 () -->', 
				'main_middle' => '<div class="cF21Dg27" id="x3734" data-limit="8"></div> <!-- ROMANDIE_home_left_175x125 () -->', 
				'main_bottom' => '<div class="cF21Dg27" data-limit="10" id="x3733"></div> <!-- ROMANDIE_home_bottom_175x60 () -->', 
				'main_popup' => '<div class="cF21Dg27" id="x3731" data-limit="1"></div> <!-- ROMANDIE_580x380 () -->', 
				'gotd_place' => '<div class="cF21Dg27" id="x3732" data-limit="1"><img src="./img/rom_gotd_place' . rand(1, 2) . '.gif" width="194" height="268" /></div>', 
			),
			'ticino' => array(
				'main_top_left' => '<div class="cF21Dg27" id="x3831" data-limit="4"></div> <!-- TICINO_home_middle_175x60 () -->',
				'main_top_right' => '<div id="x3832" class="top_right_banners cF21Dg27" data-limit="4"></div> <!-- TICINO_home_right_top_354x255 () -->', 
				'main_middle' => '<div class="cF21Dg27" id="x3830" data-limit="8"></div> <!-- TICINO_home_left_175x125 () -->', 
				'main_bottom' => '<div class="cF21Dg27" data-limit="10" id="x3739"></div> <!-- TICINO_home_bottom_175x60 () -->', 
				'main_popup' => '<div class="cF21Dg27" id="x3737" data-limit="1"></div> <!-- TICINO_580x380 () -->',
				'gotd_place' => '<div class="cF21Dg27" id="x3738" data-limit="1"><a href="http://www.sexyjobs.ch" target="_blank"><img src="./img/sexyjob_banner.png" width="175" height="175" /></a></div>', 
			)
		);

		if ( isset($banner_mapping[$region_slug][$zone_name]) && strlen($banner_mapping[$region_slug][$zone_name]) ) {
			return $banner_mapping[$region_slug][$zone_name];
		}

		return "";
	}

	public static function getForCityPage($zone_name = null, $city_id = null)
	{
		if ( ! $zone_name || ! $city_id ) return ""; 

		$banner_mapping = array(
			'right_banners_175_375' => array(
				//deutschschweiz				
				17 => '<div class="cF21Dg27" id="x313531"></div>', //zurich-umgebung
				18 => '<div class="cF21Dg27" id="x313435"></div>', //wthur-thurgau-st-gallen
				19 => '<div class="cF21Dg27" id="x3835"></div>', //basel-und-umgebung
				20 => '<div class="cF21Dg27" id="x313330"></div>', //mittelland-aargau
				21 => '<div class="cF21Dg27" id="x3931"></div>', //bern-city
				22 => '<div class="cF21Dg27" id="x3934"></div>', //bern-umgeb-oberland
				23 => '<div class="cF21Dg27" id="x3937"></div>', //biel-bienne-grenchen				
				24 => '<div class="cF21Dg27" id="x313234"></div>', //luzern-innerschweiz				
				25 => '<div class="cF21Dg27" id="x313132"></div>', //graub-nden-glarus				
				26 => '<div class="cF21Dg27" id="x313438"></div>', //wallis				
				44 => '<div class="cF21Dg27" id="x313333"></div>', //mittelland-solothurn
				45 => '<div class="cF21Dg27" id="x313533"></div>', //zurich-city

				//romandie
				28 => '<div class="cF21Dg27" id="x313039"></div>', //geneve-et-region				
				29 => '<div class="cF21Dg27" id="x313135"></div>', //lausanne-renens-moudon				
				30 => '<div class="cF21Dg27" id="x313336"></div>', //montreux-vevey-aigle				
				31 => '<div class="cF21Dg27" id="x313237"></div>', //sierre-sion-martigny-bas-valais				
				32 => '<div class="cF21Dg27" id="x313339"></div>', //neuchatel-et-region-jura				
				33 => '<div class="cF21Dg27" id="x313432"></div>', //vaud-payerne-yverdon				
				34 => '<div class="cF21Dg27" id="x313036"></div>', //fribourg-bulle
				35 => '<div class="cF21Dg27" id="x313030"></div>', //bienne

				//ticino
				36 => '<div class="cF21Dg27" id="x3838"></div>', //bellinzona
				37 => '<div class="cF21Dg27" id="x313138"></div>', //locarno
				38 => '<div class="cF21Dg27" id="x313231"></div>', //lugano
				39 => '<div class="cF21Dg27" id="x313033"></div>', //chiasso
				42 => '<div class="cF21Dg27" id="x313533"></div>', //cadenazzo [NO ZONE]
				43 => '<div class="cF21Dg27" id="x313533"></div>', //riazzino [NO ZONE]
			),
			'right_banners_175_250' => array(
				//deutschschweiz				
				17 => '<div class="cF21Dg27" id="x313530"></div>', //zurich-umgebung				
				18 => '<div class="cF21Dg27" id="x313434"></div>', //wthur-thurgau-st-gallen				
				19 => '<div class="cF21Dg27" id="x3834"></div>', //basel-und-umgebung				
				20 => '<div class="cF21Dg27" id="x313239"></div>', //mittelland-aargau				
				21 => '<div class="cF21Dg27" id="x3930"></div>', //bern-city				
				22 => '<div class="cF21Dg27" id="x3933"></div>', //bern-umgeb-oberland				
				23 => '<div class="cF21Dg27" id="x3936"></div>', //biel-bienne-grenchen				
				24 => '<div class="cF21Dg27" id="x313233"></div>', //luzern-innerschweiz				
				25 => '<div class="cF21Dg27" id="x313131"></div>', //graub-nden-glarus				
				26 => '<div class="cF21Dg27" id="x313437"></div>', //wallis
				44 => '<div class="cF21Dg27" id="x313332"></div>', //mittelland-solothurn
				45 => '<div class="cF21Dg27" id="x313534"></div>', //zurich-city [zurich nord]

				//romandie
				28 => '<div class="cF21Dg27" id="x313038"></div>', //geneve-et-region				
				29 => '<div class="cF21Dg27" id="x313134"></div>', //lausanne-renens-moudon
				30 => '<div class="cF21Dg27" id="x313335"></div>', //montreux-vevey-aigle				
				31 => '<div class="cF21Dg27" id="x313236"></div>', //sierre-sion-martigny-bas-valais				
				32 => '<div class="cF21Dg27" id="x313338"></div>', //neuchatel-et-region-jura				
				33 => '<div class="cF21Dg27" id="x313431"></div>', //vaud-payerne-yverdon				
				34 => '<div class="cF21Dg27" id="x313035"></div>', //fribourg-bulle
				35 => '<div class="cF21Dg27" id="x3939"></div>', //bienne

				//ticino
				36 => '<div class="cF21Dg27" id="x3837"></div>', //bellinzona
				37 => '<div class="cF21Dg27" id="x313137"></div>', //locarno
				38 => '<div class="cF21Dg27" id="x313230"></div>', //lugano				
				39 => '<div class="cF21Dg27" id="x313032"></div>', //chiasso
				42 => '<div class="cF21Dg27" id="x313533"></div>', //cadenazzo [NO ZONE]
				43 => '<div class="cF21Dg27" id="x313533"></div>', //riazzino [NO ZONE]
			),
			'right_banners_175_125' => array(
				//deutschschweiz				
				17 => '<div class="cF21Dg27" id="x313439"></div>', //zurich-umgebung
				18 => '<div class="cF21Dg27" id="x313433"></div>', //wthur-thurgau-st-gallen				
				19 => '<div class="cF21Dg27" id="x3833"></div>', //basel-und-umgebung
				20 => '<div class="cF21Dg27" id="x313238"></div>', //mittelland-aargau				
				21 => '<div class="cF21Dg27" id="x3839"></div>', //bern-city
				22 => '<div class="cF21Dg27" id="x3932"></div>', //bern-umgeb-oberland				
				23 => '<div class="cF21Dg27" id="x3935"></div>', //biel-bienne-grenchen				
				24 => '<div class="cF21Dg27" id="x313232"></div>', //luzern-innerschweiz								
				25 => '<div class="cF21Dg27" id="x313130"></div>', //graub-nden-glarus
				26 => '<div class="cF21Dg27" id="x313436"></div>', //wallis
				44 => '<div class="cF21Dg27" id="x313331"></div>', //mittelland-solothurn
				45 => '<div class="cF21Dg27" id="x313532"></div>', //zurich-city

				//romandie
				28 => '<div class="cF21Dg27" id="x313037"></div>', //geneve-et-region				
				29 => '<div class="cF21Dg27" id="x313133"></div>', //lausanne-renens-moudon				
				30 => '<div class="cF21Dg27" id="x313334"></div>', //montreux-vevey-aigle
				31 => '<div class="cF21Dg27" id="x313235"></div>', //sierre-sion-martigny-bas-valais				
				32 => '<div class="cF21Dg27" id="x313337"></div>', //neuchatel-et-region-jura
				33 => '<div class="cF21Dg27" id="x313430"></div>', //vaud-payerne-yverdon				
				34 => '<div class="cF21Dg27" id="x313034"></div>', //fribourg-bulle
				35 => '<div class="cF21Dg27" id="x3938"></div>', //bienne

				//ticino
				36 => '<div class="cF21Dg27" id="x3836"></div>', //bellinzona
				37 => '<div class="cF21Dg27" id="x313136"></div>', //locarno
				38 => '<div class="cF21Dg27" id="x313139"></div>', //lugano				
				39 => '<div class="cF21Dg27" id="x313031"></div>', //chiasso
				42 => '<div class="cF21Dg27" id="x313533"></div>', //cadenazzo [NO ZONE]
				43 => '<div class="cF21Dg27" id="x313533"></div>', //riazzino [NO ZONE]
			),
			'mobile_banners_265_82' => array(
				//deutschschweiz				
				17 => '<div class="cF21Dg27" id="x3639" data-limit="1"></div>', //zurich-umgebung
				18 => '<div class="cF21Dg27" id="x3637" data-limit="1"></div>', //wthur-thurgau-st-gallen				
				19 => '<div class="cF21Dg27" id="x3531" data-limit="1"></div>', //basel-und-umgebung
				20 => '<div class="cF21Dg27" id="x3631" data-limit="1"></div>', //mittelland-aargau				
				21 => '<div class="cF21Dg27" id="x3533" data-limit="1"></div>', //bern-city
				22 => '<div class="cF21Dg27" id="x3132" data-limit="1"></div>', //bern-umgeb-oberland				
				23 => '<div class="cF21Dg27" id="x3133" data-limit="1"></div>', //biel-bienne-grenchen				
				24 => '<div class="cF21Dg27" id="x3630" data-limit="1"></div>', //luzern-innerschweiz								
				25 => '<div class="cF21Dg27" id="x3536" data-limit="1"></div>', //graub-nden-glarus
				26 => '<div class="cF21Dg27" id="x3638" data-limit="1"></div>', //wallis
				44 => '<div class="cF21Dg27" id="x3632" data-limit="1"></div>', //mittelland-solothurn
				45 => '<div class="cF21Dg27" id="x3730" data-limit="1"></div>', //zurich-city

				//romandie
				28 => '<div class="cF21Dg27" id="x3535" data-limit="1"></div>', //geneve-et-region				
				29 => '<div class="cF21Dg27" id="x3537" data-limit="1"></div>', //lausanne-renens-moudon				
				30 => '<div class="cF21Dg27" id="x3633" data-limit="1"></div>', //montreux-vevey-aigle
				31 => '<div class="cF21Dg27" id="x3635" data-limit="1"></div>', //sierre-sion-martigny-bas-valais				
				32 => '<div class="cF21Dg27" id="x3634" data-limit="1"></div>', //neuchatel-et-region-jura
				33 => '<div class="cF21Dg27" id="x3636" data-limit="1"></div>', //vaud-payerne-yverdon				
				34 => '<div class="cF21Dg27" id="x3534" data-limit="1"></div>', //fribourg-bulle
				35 => '<div class="cF21Dg27" id="x3131" data-limit="1"></div>', //bienne

				//ticino
				36 => '<div class="cF21Dg27" id="x3532" data-limit="1"></div>', //bellinzona
				37 => '<div class="cF21Dg27" id="x3538" data-limit="1"></div>', //locarno
				38 => '<div class="cF21Dg27" id="x3539" data-limit="1"></div>', //lugano				
				39 => '<div class="cF21Dg27" id="x31" data-limit="1"></div>', //chiasso
				42 => '<div class="cF21Dg27" id="x313533" data-limit="1"></div>', //cadenazzo [NO ZONE]
				43 => '<div class="cF21Dg27" id="x313533" data-limit="1"></div>', //riazzino [NO ZONE]
			),
			'row_banners_desktop_526_213' => array(
				-1  => '<div class="banner-row cF21Dg27"></div>',
				17 => '<div class="banner-row cF21Dg27" id="x323332" data-limit="1"></div>', //zurich-umgebung
				18 => '<div class="banner-row cF21Dg27" id="x323336" data-limit="1"></div>', //wthur-thurgau-st-gallen
				19 => '<div class="banner-row cF21Dg27" id="x323333" data-limit="1"></div>', //basel-und-umgebung
				20 => '<div class="banner-row cF21Dg27" id="x323334" data-limit="1"></div>', //mittelland-aargau
				21 => '<div class="banner-row cF21Dg27" id="x323337" data-limit="1"></div>', //bern-city
				22 => '<div class="banner-row cF21Dg27" id="x323338" data-limit="1"></div>', //bern-umgeb-oberland
				23 => '<div class="banner-row cF21Dg27" id="x323432" data-limit="1"></div>', //biel-bienne-grenchen				
				24 => '<div class="banner-row cF21Dg27" id="x323339" data-limit="1"></div>', //luzern-innerschweiz				
				25 => '<div class="banner-row cF21Dg27" id="x323430" data-limit="1"></div>', //graub-nden-glarus				
				26 => '<div class="banner-row cF21Dg27" id="x323431" data-limit="1"></div>', //wallis				
				44 => '<div class="banner-row cF21Dg27" id="x323335" data-limit="1"></div>', //mittelland-solothurn
				45 => '<div class="banner-row cF21Dg27" id="x323331" data-limit="1"></div>', //zurich-city
			),
			'row_banners_mobile_300_250' => array(
				-1  => '<div class="cF21Dg27"></div>',
				17 => '<div class="cF21Dg27" id="x323433" data-limit="1" ></div>', //zurich-umgebung
				18 => '<div class="cF21Dg27" id="x323437" data-limit="1"></div>', //wthur-thurgau-st-gallen
				19 => '<div class="cF21Dg27" id="x323434" data-limit="1"></div>', //basel-und-umgebung
				20 => '<div class="cF21Dg27" id="x323435" data-limit="1"></div>', //mittelland-aargau
				21 => '<div class="cF21Dg27" id="x323438" data-limit="1"></div>', //bern-city
				22 => '<div class="cF21Dg27" id="x323439" data-limit="1"></div>', //bern-umgeb-oberland
				23 => '<div class="cF21Dg27" id="x323533" data-limit="1"></div>', //biel-bienne-grenchen				
				24 => '<div class="cF21Dg27" id="x323530" data-limit="1"></div>', //luzern-innerschweiz				
				25 => '<div class="cF21Dg27" id="x323531" data-limit="1"></div>', //graub-nden-glarus				
				26 => '<div class="cF21Dg27" id="x323532" data-limit="1"></div>', //wallis				
				44 => '<div class="cF21Dg27" id="x323436" data-limit="1"></div>', //mittelland-solothurn
				45 => '<div class="cF21Dg27" id="x323330" data-limit="1"></div>', //zurich-city
			),
			
					
			
		);

		if ( isset($banner_mapping[$zone_name][$city_id]) && strlen($banner_mapping[$zone_name][$city_id]) ) {
			return $banner_mapping[$zone_name][$city_id];
		}

		return "";
	}

	static public function getAllBanners(){
		return array_merge( self::getAll_265_82(), self::getAll_175_125(), self::getAll_175_250(), self::getAll_175_375() );
	}

	static public function getByGroups( $sizes = array() ){
		$sizes_st = array(
			'265_82',
			'175_125',
			'175_250',
			'175_375'
		);

		if( count(array_intersect( $sizes, $sizes_st ) ) != count( $sizes ) || !count( $sizes ) ) return array();
		$general = array();

		foreach( $sizes as $size ){
			$func = 'getAll_' . $size;
			if( function_exists( $func ) ){
				$general = array_merge( $general, $func() );
			}

			return $general;
		}
	}

    public static function getAll_265_82(){
        return array(
            //deutschschweiz
            17 => '<div class="cF21Dg27" id="x3639" data-limit="100"></div>', //zurich-umgebung
            18 => '<div class="cF21Dg27" id="x3637" data-limit="100"></div>', //wthur-thurgau-st-gallen
            19 => '<div class="cF21Dg27" id="x3531" data-limit="100"></div>', //basel-und-umgebung
            20 => '<div class="cF21Dg27" id="x3631" data-limit="100"></div>', //mittelland-aargau
            21 => '<div class="cF21Dg27" id="x3533" data-limit="100"></div>', //bern-city
            22 => '<div class="cF21Dg27" id="x3132" data-limit="100"></div>', //bern-umgeb-oberland
            23 => '<div class="cF21Dg27" id="x3133" data-limit="100"></div>', //biel-bienne-grenchen
            24 => '<div class="cF21Dg27" id="x3630" data-limit="100"></div>', //luzern-innerschweiz
            25 => '<div class="cF21Dg27" id="x3536" data-limit="100"></div>', //graub-nden-glarus
            26 => '<div class="cF21Dg27" id="x3638" data-limit="100"></div>', //wallis
            44 => '<div class="cF21Dg27" id="x3632" data-limit="100"></div>', //mittelland-solothurn
            45 => '<div class="cF21Dg27" id="x3730" data-limit="100"></div>', //zurich-city

            //romandie
            28 => '<div class="cF21Dg27" id="x3535" data-limit="100"></div>', //geneve-et-region
            29 => '<div class="cF21Dg27" id="x3537" data-limit="100"></div>', //lausanne-renens-moudon
            30 => '<div class="cF21Dg27" id="x3633" data-limit="100"></div>', //montreux-vevey-aigle
            31 => '<div class="cF21Dg27" id="x3635" data-limit="100"></div>', //sierre-sion-martigny-bas-valais
            32 => '<div class="cF21Dg27" id="x3634" data-limit="100"></div>', //neuchatel-et-region-jura
            33 => '<div class="cF21Dg27" id="x3636" data-limit="100"></div>', //vaud-payerne-yverdon
            34 => '<div class="cF21Dg27" id="x3534" data-limit="100"></div>', //fribourg-bulle
            35 => '<div class="cF21Dg27" id="x3131" data-limit="100"></div>', //bienne

            //ticino
            36 => '<div class="cF21Dg27" id="x3532" data-limit="100"></div>', //bellinzona
            37 => '<div class="cF21Dg27" id="x3538" data-limit="100"></div>', //locarno
            38 => '<div class="cF21Dg27" id="x3539" data-limit="100"></div>', //lugano
            39 => '<div class="cF21Dg27" id="x31" data-limit="100"></div>', //chiasso
        );
    }

    public static function getAll_175_125(){
        return array(
            //deutschschweiz
            17 => '<div class="cF21Dg27" id="x313439"></div>', //zurich-umgebung
            18 => '<div class="cF21Dg27" id="x313433"></div>', //wthur-thurgau-st-gallen
            19 => '<div class="cF21Dg27" id="x3833"></div>', //basel-und-umgebung
            20 => '<div class="cF21Dg27" id="x313238"></div>', //mittelland-aargau
            21 => '<div class="cF21Dg27" id="x3839"></div>', //bern-city
            22 => '<div class="cF21Dg27" id="x3932"></div>', //bern-umgeb-oberland
            23 => '<div class="cF21Dg27" id="x3935"></div>', //biel-bienne-grenchen
            24 => '<div class="cF21Dg27" id="x313232"></div>', //luzern-innerschweiz
            25 => '<div class="cF21Dg27" id="x313130"></div>', //graub-nden-glarus
            26 => '<div class="cF21Dg27" id="x313436"></div>', //wallis
            44 => '<div class="cF21Dg27" id="x313331"></div>', //mittelland-solothurn
            45 => '<div class="cF21Dg27" id="x313532"></div>', //zurich-city

            //romandie
            28 => '<div class="cF21Dg27" id="x313037"></div>', //geneve-et-region
            29 => '<div class="cF21Dg27" id="x313133"></div>', //lausanne-renens-moudon
            30 => '<div class="cF21Dg27" id="x313334"></div>', //montreux-vevey-aigle
            31 => '<div class="cF21Dg27" id="x313235"></div>', //sierre-sion-martigny-bas-valais
            32 => '<div class="cF21Dg27" id="x313337"></div>', //neuchatel-et-region-jura
            33 => '<div class="cF21Dg27" id="x313430"></div>', //vaud-payerne-yverdon
            34 => '<div class="cF21Dg27" id="x313034"></div>', //fribourg-bulle
            35 => '<div class="cF21Dg27" id="x3938"></div>', //bienne

            //ticino
            36 => '<div class="cF21Dg27" id="x3836"></div>', //bellinzona
            37 => '<div class="cF21Dg27" id="x313136"></div>', //locarno
            38 => '<div class="cF21Dg27" id="x313139"></div>', //lugano
            39 => '<div class="cF21Dg27" id="x313031"></div>', //chiasso
            42 => '<div class="cF21Dg27" id="x313533"></div>', //cadenazzo [NO ZONE]
            43 => '<div class="cF21Dg27" id="x313533"></div>', //riazzino [NO ZONE]
        );
    }

    public static function getAll_175_250(){
        return array(
            //deutschschweiz
            17 => '<div class="cF21Dg27" id="x313530"></div>', //zurich-umgebung
            18 => '<div class="cF21Dg27" id="x313434"></div>', //wthur-thurgau-st-gallen
            19 => '<div class="cF21Dg27" id="x3834"></div>', //basel-und-umgebung
            20 => '<div class="cF21Dg27" id="x313239"></div>', //mittelland-aargau
            21 => '<div class="cF21Dg27" id="x3930"></div>', //bern-city
            22 => '<div class="cF21Dg27" id="x3933"></div>', //bern-umgeb-oberland
            23 => '<div class="cF21Dg27" id="x3936"></div>', //biel-bienne-grenchen
            24 => '<div class="cF21Dg27" id="x313233"></div>', //luzern-innerschweiz
            25 => '<div class="cF21Dg27" id="x313131"></div>', //graub-nden-glarus
            26 => '<div class="cF21Dg27" id="x313437"></div>', //wallis
            44 => '<div class="cF21Dg27" id="x313332"></div>', //mittelland-solothurn
            45 => '<div class="cF21Dg27" id="x313534"></div>', //zurich-city [zurich nord]

            //romandie
            28 => '<div class="cF21Dg27" id="x313038"></div>', //geneve-et-region
            29 => '<div class="cF21Dg27" id="x313134"></div>', //lausanne-renens-moudon
            30 => '<div class="cF21Dg27" id="x313335"></div>', //montreux-vevey-aigle
            31 => '<div class="cF21Dg27" id="x313236"></div>', //sierre-sion-martigny-bas-valais
            32 => '<div class="cF21Dg27" id="x313338"></div>', //neuchatel-et-region-jura
            33 => '<div class="cF21Dg27" id="x313431"></div>', //vaud-payerne-yverdon
            34 => '<div class="cF21Dg27" id="x313035"></div>', //fribourg-bulle
            35 => '<div class="cF21Dg27" id="x3939"></div>', //bienne

            //ticino
            36 => '<div class="cF21Dg27" id="x3837"></div>', //bellinzona
            37 => '<div class="cF21Dg27" id="x313137"></div>', //locarno
            38 => '<div class="cF21Dg27" id="x313230"></div>', //lugano
            39 => '<div class="cF21Dg27" id="x313032"></div>', //chiasso
            42 => '<div class="cF21Dg27" id="x313533"></div>', //cadenazzo [NO ZONE]
            43 => '<div class="cF21Dg27" id="x313533"></div>', //riazzino [NO ZONE]
        );
    }

    public static function getAll_175_375(){
        return array(
            //deutschschweiz
            17 => '<div class="cF21Dg27" id="x313531"></div>', //zurich-umgebung
            18 => '<div class="cF21Dg27" id="x313435"></div>', //wthur-thurgau-st-gallen
            19 => '<div class="cF21Dg27" id="x3835"></div>', //basel-und-umgebung
            20 => '<div class="cF21Dg27" id="x313330"></div>', //mittelland-aargau
            21 => '<div class="cF21Dg27" id="x3931"></div>', //bern-city
            22 => '<div class="cF21Dg27" id="x3934"></div>', //bern-umgeb-oberland
            23 => '<div class="cF21Dg27" id="x3937"></div>', //biel-bienne-grenchen
            24 => '<div class="cF21Dg27" id="x313234"></div>', //luzern-innerschweiz
            25 => '<div class="cF21Dg27" id="x313132"></div>', //graub-nden-glarus
            26 => '<div class="cF21Dg27" id="x313438"></div>', //wallis
            44 => '<div class="cF21Dg27" id="x313333"></div>', //mittelland-solothurn
            45 => '<div class="cF21Dg27" id="x313533"></div>', //zurich-city

            //romandie
            28 => '<div class="cF21Dg27" id="x313039"></div>', //geneve-et-region
            29 => '<div class="cF21Dg27" id="x313135"></div>', //lausanne-renens-moudon
            30 => '<div class="cF21Dg27" id="x313336"></div>', //montreux-vevey-aigle
            31 => '<div class="cF21Dg27" id="x313237"></div>', //sierre-sion-martigny-bas-valais
            32 => '<div class="cF21Dg27" id="x313339"></div>', //neuchatel-et-region-jura
            33 => '<div class="cF21Dg27" id="x313432"></div>', //vaud-payerne-yverdon
            34 => '<div class="cF21Dg27" id="x313036"></div>', //fribourg-bulle
            35 => '<div class="cF21Dg27" id="x313030"></div>', //bienne

            //ticino
            36 => '<div class="cF21Dg27" id="x3838"></div>', //bellinzona
            37 => '<div class="cF21Dg27" id="x313138"></div>', //locarno
            38 => '<div class="cF21Dg27" id="x313231"></div>', //lugano
            39 => '<div class="cF21Dg27" id="x313033"></div>', //chiasso
            42 => '<div class="cF21Dg27" id="x313533"></div>', //cadenazzo [NO ZONE]
            43 => '<div class="cF21Dg27" id="x313533"></div>', //riazzino [NO ZONE]
        );
    }

	public static function getMobilePopup(){
		$popupBanners = array(
			//deutschschweiz
			'<div class="cF21Dg27" id="x323137"></div>'
		);

		return self::array_random( $popupBanners );
	}

    /**
     * @param null $id
     * @return array|mixed
     */
    public static function getBannersToOrder($id = null)
    {
        $map = array(
            1 => [
                'id'       => 1,
                'duration' => 30, // days
                'price'    => 1350,
                'currency' => 'CHF',
                'texts'    => [
                    'title'    => __('bp_large_banner'),
                    'duration' => 1 . ' ' . __('month'),
                ],
                'img' => '/img/banner_price/v2/desk-home-large-banner.png'
            ],
            2 => [
                'id'       => 2,
                'duration' => 7, // days
                'price'    => 270,
                'currency' => 'CHF',
                'texts'    => [
                    'title'    => __('bp_medium_banner'),
                    'duration' => 1 . ' ' . __('week'),
                ],
                'img' => '/img/banner_price/v2/desk-main-medium-banner.png'
            ],
            3 => [
                'id'       => 3,
                'duration' => 7, // days
                'price'    => 450,
                'currency' => 'CHF',
                'texts'    => [
                    'title'    => __('bp_large_banner'),
                    'duration' => 1 . ' ' . __('week'),
                ],
                'img' => '/img/banner_price/v2/desk-home-large-banner.png'
            ],
            4 => [
                'id'       => 4,
                'duration' => 30, // days
                'price'    => 1215,
                'currency' => 'CHF',
                'texts'    => [
                    'title'    => __('bp_large_banner'),
                    'duration' => 1 . ' ' . __('month'),
                ],
                'img' => '/img/banner_price/v2/desk-home-large-banner.jpg'
            ],
            5 => [
                'id'       => 5,
                'duration' => 30, // days
                'price'    => 450,
                'currency' => 'CHF',
                'texts'    => [
                    'title'    => __('bp_small_banner'),
                    'duration' => 1 . ' ' . __('month'),
                ],
                'img' => '/img/banner_price/v2/desk-area-small-banner.png'

            ],
            6 => [
                'id'       => 6,
                'duration' => 1, // days
                'price'    => 220,
                'currency' => 'CHF',
                'texts'    => [
                    'title'    => __('bp_large_area_banner'),
                    'duration' => 1 . ' ' . __('day'),
                ],
                'img' => '/img/banner_price/v2/desk-area-large-area-banner.png'
            ],
            /*7 => [
                'id'       => 7,
                'duration' => 7, // days
                'price'    => 1,
                'currency' => 'CHF',
                'texts'    => [
                    'title'    => 'Eddie\'s blessing',
                    'duration' => 2 . ' ' . __('days'),
                ],
                'img' => '/img/banner_price/v2/desk-area-small-banner.png'
            ],*/
        );

        return !empty($id) ? $map[$id] : $map;
    }

    /**
     * @param $orderData
     * @return mixed
     * @throws Exception
     */
    public function createOrder($orderData)
    {
        $client = new Cubix_Api_XmlRpc_Client();
        return $client->call('Banners.createOrder', array($orderData));
    }

    /**
     * @param $orderId
     * @return mixed
     * @throws Exception
     */
    public function getOrderById($orderId)
    {
        $client = new Cubix_Api_XmlRpc_Client();
        return $client->call('Banners.getOrderById', array($orderId));
    }

    /**
     * @param $orderId
     * @param array $optionalFieldsToUpdate
     * @return mixed
     * @throws Exception
     */
    public function markOrderAsPaid($orderId, $optionalFieldsToUpdate = array())
    {
        $client = new Cubix_Api_XmlRpc_Client();
        return $client->call('Banners.updateOrder', array($orderId, array_merge($optionalFieldsToUpdate, ['status' => self::STATUS_PAID])));
    }

    /**
     * @param $orderId
     * @return mixed
     * @throws Exception
     */
    public function markOrderAsRejected($orderId)
    {
        $client = new Cubix_Api_XmlRpc_Client();
        return $client->call('Banners.updateOrder', array($orderId, ['status' => self::STATUS_REJECTED]));
    }

	private function array_random($arr, $num = 1) {
		shuffle($arr);

		$r = array();
		for ($i = 0; $i < $num; $i++) {
			$r[] = $arr[$i];
		}
		return $num == 1 ? $r[0] : $r;
	}
}
