<?php

class Model_CamBooking extends Cubix_Model
{
	
	const STATUS_ENABLED = 1;
	const STATUS_DISABLED = 0;

	public function add($data)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('CamBooking.save', array($data));
	}
	
	public function update($id, $data)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('CamBooking.update', array($id, $data));
	}
	
	public function checkBySession($id, $session){
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('CamBooking.checkBySession', array($id, $session));
	}
	
	public function getStatus($request_id){
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('CamBooking.getStatus', array($request_id));
	}
	
	public function getData($escort_id){
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('CamBooking.getData', array($escort_id));
	}
	
	public function getPrice($escort_id, $duration){
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('CamBooking.getPrice', array($escort_id, $duration));
	}
	
	public function getForSuccess($request_id){
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('CamBooking.getForSuccess', array($request_id));
	}

	public function get_price_by_min($minutes){
		if(!is_int($minutes))die('argument is  invalid');
		$prices =parent::_fetchAll("SELECT * FROM cam_booking_prices WHERE duration=?  ",$minutes);
		return $prices;
	}

	public function save($data)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$status = $client->call('CamBooking.saveCamSettings', array($data));
		return $status;
	}
}
