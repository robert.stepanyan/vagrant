<?php

class Model_Members extends Cubix_Model
{
	public function save($member)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$member_id = $client->call('Members.saveMember', array( (array) $member));
		$member->setId($member_id);

		return $member;
	}
	
	public function getEscortAlerts($user_id, $escort_id)
	{		
		if ( ! $user_id || ! $escort_id ) return array();
		
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Members.getEscortAlerts', array($user_id, $escort_id));
	}
	
	public function memberAlertsSave($user_id, $escort_id, $events, $extra = null)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Members.alertsSave', array($user_id, $escort_id, $events, $extra));
	}

    public function getUsernameById($id)
    {
        $client = new Cubix_Api_XmlRpc_Client();
        return $client->call('Members.getUsernameById', array($id));
    }
	
	public function getAlerts($user_id)
	{
		//$client = new Cubix_Api_XmlRpc_Client();
		//return $client->call('Members.getAlerts', array($user_id));
		$client = Cubix_Api::getInstance();
		return $client->call('getMemberAlerts', array($user_id));
	}

	public function getMembersInfoByUsername($username)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Members.getMembersInfoByUsername', array($username));
	}
	
	/* favorites */
	public function getFavorites($user_id, $filter, $page, $per_page)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Members.getFavorites', array($user_id, $filter, $page, $per_page));
	}
	
	public function getFavoritesTop10($user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Members.getFavoritesTop10', array($user_id));
	}
	
	public function updateFavoritesComment($data)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Members.updateFavoritesComment', array($data));
	}
	
	public function removeFromFavorites($user_id, $escort_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Members.removeFromFavorites', array($user_id, $escort_id));
	}
	
	public function reorderingTop10($user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Members.reorderingTop10', array($user_id));
	}
	
	public function addToFavorites($user_id, $escort_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Members.addToFavorites', array($user_id, $escort_id));
	}
	
	public function isInFavorites($user_id, $escort_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Members.isInFavorites', array($user_id, $escort_id));
	}
	
	public function favoritesAddToTop($user_id, $escort_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Members.favoritesAddToTop', array($user_id, $escort_id));
	}
	
	public function favoritesRemoveFromTop($user_id, $escort_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Members.favoritesRemoveFromTop', array($user_id, $escort_id));
	}
	
	public function favoritesMoveUp($user_id, $escort_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Members.favoritesMoveUp', array($user_id, $escort_id));
	}
	
	public function favoritesMoveDown($user_id, $escort_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Members.favoritesMoveDown', array($user_id, $escort_id));
	}
	
	public function changeFavCommentType($user_id, $f_id, $type)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Members.changeFavCommentType', array($user_id, $f_id, $type));
	}
	
	public function getFavoriteRequests($user_id, $owner_user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Members.getFavoriteRequests', array($user_id, $owner_user_id));
	}
	
	public function sendFavRequest($user_id, $f_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Members.sendFavRequest', array($user_id, $f_id));
	}
	
	public function getFavPendingRequestsCount($user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Members.getFavPendingRequestsCount', array($user_id));
	}
	
	public function getFavPendingRequests($user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Members.getFavPendingRequests', array($user_id));
	}
	
	public function shareFavComment($user_id, $req_id, $type)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Members.shareFavComment', array($user_id, $req_id, $type));
	}
	/* end favorites */
	
	public function addTokenRequest($data)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Members.addTokenRequest', array($data));
	}
	
	public function updateTokenRequest($request_id, $user_id, $tokens, $data)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Members.updateTokenRequest', array($request_id, $user_id, $tokens, $data));
	}
	
	public function checkStatus($id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Members.checkStatus', array($id));
	}
	
	public function checkVideoCallStatus($id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Members.checkVideoCallStatus', array($id));
	}
	
}
