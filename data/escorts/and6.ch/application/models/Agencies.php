<?php

class Model_Agencies extends Cubix_Model
{
	protected $_table = 'agencies';
	protected $_itemClass = 'Model_AgencyItem';
	
	public static function hit($agency_id)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		
		$result = $client->call('Agencies.updateHitsCount', array($agency_id));
		
		return $result;
	}
	
	/*public function getIdByName($name)
	{
		$sql = "
			SELECT id
			FROM agencies a
			WHERE name = ?
		";
		
		$agency = parent::_fetchRow($sql, $name);
		
		if ( $agency )
			return $agency->id;
		
		return null;
	}*/
	
	public function getIdByName($name)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		
		$result = $client->call('Agencies.getByName', array($name));
		
		return $result;
	}
	
	/*public function get($agency_id)
	{
		$sql = "
			SELECT a.id, a.user_id, a.name, a.phone, a.phone_instructions, a.email, a.web, a.last_modified, a.logo_hash, a.logo_ext, u.application_id, u.date_registered AS creation_date, a.hit_count
			FROM agencies a
			LEFT JOIN users u ON u.id = a.user_id
			WHERE a.id = ?
		";
		
		return parent::_fetchRow($sql, $agency_id);
	}*/
	
	public function get($agency_id)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		
		$result = $client->call('Agencies.get', array($agency_id));
		
		return $result;
	}
	
	public function getByUserId($user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$agency = $client->call('Agencies.getByUserId', array($user_id));
		
		$agency = new Model_AgencyItem($agency);
		
		return $agency;
	}

    public function getAll($page = 1, $page_size = 20, $city_id = null, $sorting = null, $country_id = null, &$count = 0, $list_type = 'simple', $geo_data = array(), $online_now = false, $agency_filter = null, $agency_has_esc_type = 1, $ag_city_id = null)
    {
        $lng = Cubix_I18n::getLang();

        $page = (int) $page;
        if ( $page < 1 ) $page = 1;
        $limit = ($page_size * ($page - 1)) . ', ' . $page_size;

        $where = " WHERE 1 ";

        if ( $city_id ) {
            $where .= " AND eic.city_id = " . (int) $city_id;
        }

        if ( $ag_city_id ) {
            $where .= " AND cd.city_id = " . (int) $ag_city_id;
        }

        if ( $country_id ) {
            $where .= " AND e.country_id = " . (int) $country_id;
        }

        if ($agency_filter)
        {
            $where .= " AND cd.club_name LIKE '" . $agency_filter . "%'";
        }

        $order = self::_mapSorting($sorting);
        $sql = '
			SELECT SQL_CALC_FOUND_ROWS
				cd.agency_id AS id,
				cd.address,
				cd.zip,
				cd.club_name AS name,
				cd.is_new,
				UNIX_TIMESTAMP(cd.date_registered) AS date_registered,
				cd.user_id,			
				ct.slug AS city_slug,
				ct.id AS city_id,
				ct.title_' . $lng . ' AS city,			
				cr.id AS country_id,
				cr.slug AS country_slug,
				cr.title_' . $lng . ' AS country,				
				cd.photo_hash AS logo_hash,
				cd.photo_ext AS logo_ext,				
				16 AS application_id,			
				cd.email,
				cd.entrance,
				cd.wellness,
				cd.food,
				cd.outdoor,
				cd.web AS website,				
				cd.phone_country_id,				
				cd.phone AS phone_number,
				cd.hit_count,				
				cd.last_modified AS date_last_modified,
				cd.is_premium,				
				cd.about_' . $lng . ' AS about,
				cd.club_name AS agency_name,
				cd.club_slug AS agency_slug,
				cd.verified_escorts_count,
				cd.allow_show_online,
				cd.agency_id AS agency_id
			FROM club_directory cd
			LEFT JOIN cities ct ON ct.id = cd.city_id
			LEFT JOIN countries cr ON cr.id = cd.country_id
			' . $where . ' AND cd.escorts_count > 0 
			GROUP BY cd.agency_id
			ORDER BY ' . $order . '
			LIMIT ' . $limit . '
		';

        $agencies = self::db()->fetchAll($sql);
        $count = self::db()->fetchOne('SELECT FOUND_ROWS()');

        if ( count($agencies) ) {
            foreach ($agencies as $key => $agency)
            {
                $params = array('e.agency_id = ?' => $agency->id,'e.type = ?' => $agency_has_esc_type);
                if ( $country_id ) {
                    $params['e.country_id = ?'] = $country_id;
                }
                $escorts_count = 0;
                if($A_Escorts = Model_Escort_List::getAgencyEscorts($params, 'newest', 1, 8, $escorts_count)){
                    $agency->a_escorts = $A_Escorts;
                    $agency->escorts_count = $escorts_count;
                }else{
                    unset($agencies[$key]);
                }
            }
        }

        //print_r($agencies); die;

        return $agencies;
    }

    public function getCitiesMapped(){
        $lng = Cubix_I18n::getLang();

        $where = " WHERE 1";

        $sql = '
			SELECT
				cd.agency_id AS id,
				cd.club_name AS name,
				COUNT( * ) as agencies_count,
				ct.slug AS city_slug,
				ct.id AS city_id,
				ct.title_' . $lng . ' AS city,
				cr.id AS country_id,
				cr.slug AS country_slug,
				16 AS application_id,
				cr.title_' . $lng . ' AS country
			FROM club_directory cd
			LEFT JOIN cities ct ON ct.id = cd.city_id
			LEFT JOIN countries cr ON cr.id = cd.country_id
			' . $where . ' AND cd.escorts_count > 0 AND ct.region_id = 1
			GROUP BY cd.city_id
		';

        return self::db()->fetchAll($sql);
    }

	public function save($agency)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$agency_data = array(
			'user_id' => $agency->user_id,
			'name' => $agency->name,
			'country_id' => $agency->country_id,
			'slug' => $this->clear_string($agency->name)
 		);
		$agency_id = $client->call('Agencies.save', array($agency_data));
		$agency->setId($agency_id);
		
		return $agency;
	}
	
	private function clear_string($str)
	{
		$str = strtolower($str);

		$str = preg_replace("#[^0-9a-z]#", '-', $str);

		$str = preg_replace("#\s+#", '-', $str);

		$str = ___clean_str($str);

		return $str;
	}
	
	public function loadAgencyPhotos($agency_id, $page = 1, $per_page = 4, &$count = null)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		
		$photos = $client->call('Agencies.getPhotos', array($agency_id, $page, $per_page));
		$count = $photos['count'];
		
		$images = new Cubix_Images();
		
		$phs = $photos['list'];
		
		foreach ($phs as $k => $photo)
		{
			$image = array('hash' => $photo['hash'], 'ext' => $photo['ext']);
			$image = new Cubix_Images_Entry($image);
			$image->setCatalogId('agencies');
			$image_url = $images->getAgencyUrl($image, $agency_id);
			$phs[$k]['image_url'] = $image_url;
		}
		
		return $phs;
	}

    private static function _mapSorting($param)
    {
        $map = array(
            'alpha' => 'cd.is_premium DESC, cd.club_name ASC',
            'random' => 'cd.is_premium DESC, RAND()',
            'last_modified' => 'cd.is_premium DESC, cd.last_modified DESC',
            'premium' => 'cd.is_premium DESC',
            'by-city' => 'ct.title_en ASC',
            'by-country' => 'cr.title_en ASC',
            'most-viewed' => 'cd.hit_count DESC',
            'close-to-me' => 'distance ASC',
            'last-connection' => 'ulrt.refresh_date DESC',
            'newest' => 'cd.date_registered DESC',
        );

        $order = 'cd.club_name ASC';
        if ( isset($map[$param]) ) {
            $order = $map[$param];
        }
        elseif ( false !== strpos($param, 'FIELD') ) {
            $order = $param;
        }

        return $order;
    }
}
