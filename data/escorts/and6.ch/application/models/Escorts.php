<?php
class Model_Escorts extends Cubix_Model
{
	protected $_table = 'escorts';
	protected $_itemClass = 'Model_EscortItem';
	
	const VERIFIED_STATUS_NOT_VERIFIED = 1;
	const VERIFIED_STATUS_VERIFIED = 2;
	const VERIFIED_STATUS_VERIFIED_RESET = 3;

	const ESCORT_STATUS_NO_PROFILE 			= 1;		// 000000001
	const ESCORT_STATUS_NO_ENOUGH_PHOTOS 	= 2;		// 000000010
	const ESCORT_STATUS_NOT_APPROVED 		= 4;		// 000000100
	const ESCORT_STATUS_OWNER_DISABLED 		= 8;		// 000001000
	const ESCORT_STATUS_ADMIN_DISABLED 		= 16;		// 000010000
	const ESCORT_STATUS_ACTIVE 				= 32;		// 000100000
	const ESCORT_STATUS_IS_NEW				= 64;		// 001000000
	const ESCORT_STATUS_PROFILE_CHANGED		= 128;		// 010000000
	const ESCORT_STATUS_SUSPICIOUS_DELETED	= 2048;		// 01000000000


	const PRODUCT_NATIONAL_LISTING			= 1;
	const PRODUCT_INTERNATIONAL_DIRECTORY	= 2;
	const PRODUCT_GIRL_OF_THE_MONTH			= 3;
	const PRODUCT_MAIN_PREMIUM_SPOT			= 4;
	const PRODUCT_CITY_PREMIUM_SPOT			= 5;
	const PRODUCT_TOUR_PREMIUM_SPOT			= 6;
	const PRODUCT_TOUR_ABILITY				= 7;
	const PRODUCT_NO_REVIEWS				= 8;
	const PRODUCT_ADDITIONAL_CITY			= 9;
	const PRODUCT_SEARCH					= 10;	
	
	public function getShownameById($id)
	{
		return parent::getAdapter()->fetchOne('SELECT showname FROM escorts WHERE id = ?', $id);
	}
	
	public function getIdByShowname($showname)
	{
		$sql = '
			SELECT e.id,
				eph.hash AS photo_hash,
				eph.ext AS photo_ext,
				u.application_id,
				e.agency_id,
				eph.status AS photo_status
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id			
			INNER JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1
			LEFT JOIN cities c ON c.id = e.city_id
			 
			WHERE showname = ?
		';
		
		return parent::_fetchRow($sql, $showname);
	}

	
	/**
	 * @param int $user_id
	 * @return Model_EscortItem
	 */
	public function getByUserId($user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$escort = $client->call('Escorts.getByUserId', array($user_id));
		
		if ( ! $escort ) {
			return null;
		}

		$escort = (array) $escort;

//		$additional = parent::getAdapter()->fetchRow('
//			SELECT photo_hash, photo_ext, photo_status FROM escorts WHERE id = ?
//		', array($escort['id']), Zend_Db::FETCH_ASSOC);
//		if ( ! $additional ) {
//			$additional = array('photo_hash' => null, 'photo_ext' => null, 'photo_status' => null);
//		}

		$escort = array_merge($escort, array('application_id' => Cubix_Application::getId()));

		return new Model_EscortItem($escort);
		
		/*
		$item = parent::_fetchRow('
			SELECT e.id, u.email, ep.contact_phone, e.verified_status, u.application_id
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN escort_profiles ep ON ep.escort_id = e.id
			WHERE e.user_id = ?
		', $user_id);
		
		return $item;*/
	}

	public function existsByShowname($showname, $escort_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Escorts.existsByShowname', array($showname, $escort_id));
	}
	
	public function getById($id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$escort = $client->call('Escorts.getById', array($id));
		
		if ( isset($escort['error']) ) {
			print_r($escort);die;
		}
		
		$escort = new Model_EscortItem($escort);
		$escort->setAdapter(self::getAdapter());
		
		return $escort;
	}
	
	public function get($shownameOrId, $cache_key = null)
	{
		/*$sql = '
			SELECT e.id, e.showname, c.' . Cubix_I18n::getTblField('title') . ' AS city_title,
				eph.hash AS photo_hash,
				eph.ext AS photo_ext,
				u.application_id,
				ep.contact_email,
				u.username,
				e.agency_id,
				eph.status AS photo_status
			FROM escorts e
			INNER JOIN escort_profiles ep ON ep.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1
			LEFT JOIN cities c ON c.id = e.city_id
			WHERE e.id = ?
		';*/

		// return parent::_fetchRow($sql, $escort_id);

		/*
		$escort = unserialize(self::getAdapter()->fetchOne('SELECT ep.data FROM escort_profiles ep INNER JOIN escorts e ON e.id = ep.escort_id WHERE e.' . ( is_numeric($shownameOrId) ? 'id' : 'showname' ) . ' = ? AND lang_id = ?', array($shownameOrId, Cubix_I18n::getLang())));
		if ( $escort ) {
			$escort = new Model_EscortItem($escort);
			$escort->setAdapter(self::getAdapter());

			return $escort;
		}
		else {
			return null;
		}
		 */

		/*$client = new Cubix_Api_XmlRpc_Client();
		$client->setSkipSystemLookup(false);
		$escort = $client->call('Escorts.getProfile', array($shownameOrId, Cubix_I18n::getLang()));*/

		/*$escort = new Model_EscortItem($escort);
		$escort->setAdapter(self::getAdapter());*/

		$cache = Zend_Registry::get('cache');

		$escort = unserialize(self::getAdapter()->fetchOne('SELECT ep.data FROM escort_profiles ep INNER JOIN escorts e ON e.id = ep.escort_id WHERE e.' . ( is_numeric($shownameOrId) ? 'id' : 'showname' ) . ' = ? AND lang_id = ?', array($shownameOrId, Cubix_I18n::getLang())));
		if ( $escort ) {
			$escort = new Model_EscortItem($escort);
			$escort->setAdapter(self::getAdapter());
			
			return $escort;
		}
		else {
			
			$client = new Cubix_Api_XmlRpc_Client();
			$client->setSkipSystemLookup(false);

			if ( $cache_key ) {
				if ( ! $escort = $cache->load($cache_key) ) {
					$escort = $client->call('Escorts.getProfile', array($shownameOrId, Cubix_I18n::getLang(), true));
					$escort = new Model_EscortItem($escort);
					$cache->save($escort, $cache_key, array());
				}
			}
			else {
				$escort = $client->call('Escorts.getProfile', array($shownameOrId, Cubix_I18n::getLang(), true));
				$escort = new Model_EscortItem($escort);
			}
		}

		return $escort;
	}

	public function getAll($params, &$count = 0)
	{
		$config = Zend_Registry::get('escorts_config');
		
		/* --> Construct where clause */
		$where_arr = array();
		if ( isset($params['filter']) ) {
			$params['filter'] = array_merge($params['filter'], array(
				// 'u.status = ?' => array(1),
				// 'u.application_id = ?' => array(Cubix_Application::getId())
			));
			
			$where_arr = array();
			
			if ( is_integer($params['filter']['f.user_id = ?']) )
			{
				$what = ' , f.user_id AS fav_user_id ';
				$join = ' INNER JOIN favorites f ON f.escort_id = e.id ';
			}
			
			foreach ( $params['filter'] as $exp => $value ) {
				if ( ! is_array($value) ) {
					$value = array($value);
				}
				
				$where_arr[$exp] = $value;
			}
			
			$where = implode(' AND ', array_keys($where_arr));
		}
		/* <-- */
		
		/* --> Construct ordering */
		$order = 'ORDER BY e.date_registered DESC';
		
		if ( isset($params['order']) ) {
			$order = 'ORDER BY ' . $params['order'];
		}
		/* <-- */
		
		/* --> Construct limit */
		$limit = 'LIMIT 0, ' . $config['perPage'];
		
		if ( isset($params['limit']) ) {
			$limit = 'LIMIT ' . ($params['limit']['page'] - 1) * (($params['limit']['perPage']) ? $params['limit']['perPage'] : $config['perPage']) . ', ' . (($params['limit']['perPage']) ? $params['limit']['perPage'] : $config['perPage']);
		}
		
		/* <-- */
		$sql = '
			SELECT
				e.id,
				e.showname,
				e.age,
				UNIX_TIMESTAMP(e.date_registered) AS date_registered,
				cc.' . Cubix_I18n::getTblField('title') . ' AS city,
				
				e.rates,
				
				e.availability,
				e.is_new,
				e.verified_status,
				e.photo_hash,
				e.photo_ext,
				e.photo_status,
				' . Cubix_Application::getId() . ' AS application_id,
				e.city_id,
				e.tour_city_id,
				cc1.' . Cubix_I18n::getTblField('title') . ' AS tour_city,
				e.tour_date_from,
				e.tour_date_to,
				IF(enh.expires_at >= NOW(), 1, 0) as is_new_h,
				e.hit_count
				' . $what . '
			FROM escorts e
			
	    	INNER JOIN escort_cities ec ON ec.escort_id = e.id

			INNER JOIN escort_products eprod ON eprod.escort_id = e.id AND eprod.product_id = ' . self::PRODUCT_NATIONAL_LISTING . ' 

			INNER JOIN countries c ON c.id = e.country_id
			INNER JOIN cities cc ON cc.id = ec.city_id OR e.tour_city_id = cc.id
			
			LEFT JOIN cities cc1 ON cc1.id = e.tour_city_id
			
			LEFT JOIN regions r ON r.id = cc.region_id
			LEFT JOIN cityzones cz ON cz.id = e.cityzone_id
			LEFT JOIN escorts_new_history enh on enh.escort_id = e.id 
			' . $join . '
			' . ( strlen($where) ? 'WHERE ' . $where  : '' ) . '
			GROUP BY e.id
			' . $order . '
			' . $limit . '
		';
		
		$countSql = '
			SELECT 
				COUNT(DISTINCT(e.id)) AS count
			FROM escorts e
			
			INNER JOIN escort_cities ec ON ec.escort_id = e.id

			INNER JOIN escort_products eprod ON eprod.escort_id = e.id AND eprod.product_id = ' . self::PRODUCT_NATIONAL_LISTING . '

			INNER JOIN countries c ON c.id = e.country_id
			INNER JOIN cities cc ON cc.id = ec.city_id OR e.tour_city_id = cc.id
			LEFT JOIN regions r ON r.id = cc.region_id
			LEFT JOIN cityzones cz ON cz.id = e.cityzone_id
			' . $join . '
			' . ( strlen($where) ? 'WHERE ' . $where  : '' ) . '
		';
		
		$bind = array();
		foreach ( $where_arr as $value ) {
			// If an empty array supplied in $where_arr for a value, dont add it
			if ( ! count($value) ) continue;
			
			$bind = array_merge($bind, $value);
		}
		
		$count = self::getAdapter()->fetchOne($countSql, $bind);
		
		return parent::_fetchAll($sql, $bind);
	}

	public function getCount($params)
	{
		/* --> Construct where clause */
		$where_arr = array();
		if ( isset($params['filter']) ) {
			$params['filter'] = array_merge($params['filter'], array(
				// 'u.status = ?' => array(1),
				// 'u.application_id = ?' => array(Cubix_Application::getId())
			));

			$where_arr = array();

			if ( is_integer($params['filter']['f.user_id = ?']) ) {
				$what = ' , f.user_id AS fav_user_id ';
				$join = ' INNER JOIN favorites f ON f.escort_id = e.id ';
			}

			foreach ( $params['filter'] as $exp => $value ) {
				if ( ! is_array($value) ) {
					$value = array($value);
				}

				$where_arr[$exp] = $value;
			}

			$where = implode(' AND ', array_keys($where_arr));
		}
		/* <-- */

		$countSql = '
			SELECT
				COUNT(DISTINCT(e.id)) AS count
			FROM escorts e

			INNER JOIN escort_cities ec ON ec.escort_id = e.id

			INNER JOIN escort_products eprod ON eprod.escort_id = e.id AND eprod.product_id = ' . self::PRODUCT_NATIONAL_LISTING . '

			INNER JOIN countries c ON c.id = e.country_id
			INNER JOIN cities cc ON cc.id = ec.city_id OR e.tour_city_id = cc.id
			LEFT JOIN regions r ON r.id = cc.region_id
			LEFT JOIN cityzones cz ON cz.id = e.cityzone_id
			' . $join . '
			' . ( strlen($where) ? 'WHERE ' . $where  : '' ) . '
		';

		$bind = array();
		foreach ( $where_arr as $value ) {
			// If an empty array supplied in $where_arr for a value, dont add it
			if ( ! count($value) ) continue;

			$bind = array_merge($bind, $value);
		}

		return parent::getAdapter()->fetchOne($countSql, $bind);
	}

	public function getAllTours($params, &$count, $upcoming_tours = false)
	{
		return array();

		$config = Zend_Registry::get('escorts_config');
		
		/* --> Construct where clause */
		$where_arr = array();
		if ( isset($params['filter']) ) {
			$params['filter'] = array_merge($params['filter'], array(
				// 'u.status = ?' => array(1),
				// 'u.application_id = ?' => array(Cubix_Application::getId())
			));
			
			$where_arr = array();
			
			if ( is_integer($params['filter']['f.user_id = ?']) )
			{
				$what = ' , f.user_id AS fav_user_id ';
				$join = ' INNER JOIN favorites f ON f.escort_id = e.id ';
			}
			
			foreach ( $params['filter'] as $exp => $value ) {
				if ( ! is_array($value) ) {
					$value = array($value);
				}
				
				$where_arr[$exp] = $value;
			}
			
			$where = implode(' AND ', array_keys($where_arr));
		}
		/* <-- */
		
		/* --> Construct ordering */
		$order = 'ORDER BY e.date_registered DESC';
		
		if ( isset($params['order']) ) {
			$order = 'ORDER BY ' . $params['order'];
		}
		/* <-- */
		
		/* --> Construct limit */
		$limit = 'LIMIT 0, ' . $config['perPage'];
		
		if ( isset($params['limit']) ) {
			$limit = 'LIMIT ' . ($params['limit']['page'] - 1) * (($params['limit']['perPage']) ? $params['limit']['perPage'] : $config['perPage']) . ', ' . (($params['limit']['perPage']) ? $params['limit']['perPage'] : $config['perPage']);
		}
		
		/* <-- */
		if ( ! $upcoming_tours )
		{
			$sql = '
				SELECT
					e.id,
					e.showname,
					e.age,
					UNIX_TIMESTAMP(e.date_registered) AS date_registered,
					cc.' . Cubix_I18n::getTblField('title') . ' AS city,
					
					e.rates,
					
					e.availability,
					e.is_new,
					if(enh.expires_at >= NOW(), 1, 0) as is_new_h, 
					e.verified_status,
					e.photo_hash,
					e.photo_ext,
					e.photo_status,
					' . Cubix_Application::getId() . ' AS application_id,
					
					e.tour_date_from,
					e.tour_date_to
				FROM escorts e
				
				INNER JOIN countries c ON c.id = e.country_id
				INNER JOIN cities cc ON cc.id = e.tour_city_id
				INNER JOIN escort_products eprod ON eprod.escort_id = e.id AND eprod.product_id = ' . self::PRODUCT_TOUR_ABILITY . '
				LEFT JOIN regions r ON r.id = cc.region_id
				LEFT JOIN cityzones cz ON cz.id = e.cityzone_id
				LEFT JOIN escorts_new_history enh on enh.escort_id = e.id
				
				' . ( strlen($where) ? 'WHERE ' . $where  : '' ) . '
				GROUP BY e.id
				' . $order . '
				' . $limit . '
			';		
			
			$countSql = '
				SELECT 
					COUNT(DISTINCT(e.id)) AS count
				FROM escorts e
				
				INNER JOIN countries c ON c.id = e.country_id
				INNER JOIN cities cc ON cc.id = e.tour_city_id
				INNER JOIN escort_products eprod ON eprod.escort_id = e.id AND eprod.product_id = ' . self::PRODUCT_TOUR_ABILITY . '
				LEFT JOIN regions r ON r.id = cc.region_id
				LEFT JOIN cityzones cz ON cz.id = e.cityzone_id
				
				' . ( strlen($where) ? 'WHERE ' . $where  : '' ) . '
			';
		}
		else {
			$sql = '
				SELECT 
					ut.*, 
					e.photo_hash, 
					e.photo_ext,
					e.photo_status, 
					e.showname, 
					e.is_new, 
					if(enh.expires_at >= NOW(), 1, 0) as is_new_h, 
					e.verified_status, 
					' . Cubix_Application::getId() . ' AS application_id,
					cc.' . Cubix_I18n::getTblField('title') . ' AS city
				FROM upcoming_tours ut
				INNER JOIN escorts e ON e.id = ut.id
				INNER JOIN cities cc ON cc.id = ut.tour_city_id
				INNER JOIN escort_products eprod ON eprod.escort_id = e.id AND eprod.product_id = ' . self::PRODUCT_TOUR_ABILITY . '
				LEFT JOIN regions r ON r.id = cc.region_id
				LEFT JOIN cityzones cz ON cz.id = e.cityzone_id
				LEFT JOIN escorts_new_history enh on enh.escort_id = e.id 
				' . ( strlen($where) ? 'WHERE ' . $where  : '' ) . '
				GROUP BY e.id
				' . $order . '
				' . $limit . '
			';			
			
			$countSql = '
				SELECT 
					COUNT(DISTINCT(e.id)) AS count
				FROM upcoming_tours ut				
				INNER JOIN escorts e ON e.id = ut.id
				INNER JOIN escort_products eprod ON eprod.escort_id = e.id AND eprod.product_id = ' . self::PRODUCT_TOUR_ABILITY . '
				INNER JOIN cities cc ON cc.id = e.tour_city_id		
				LEFT JOIN regions r ON r.id = cc.region_id
				LEFT JOIN cityzones cz ON cz.id = e.cityzone_id	
				' . ( strlen($where) ? 'WHERE ' . $where  : '' ) . '
			';
			//echo $countSql;die;
		}
		
		$bind = array();
		foreach ( $where_arr as $value ) {
			// If an empty array supplied in $where_arr for a value, dont add it
			if ( ! count($value) ) continue;
			
			$bind = array_merge($bind, $value);
		}
		
		// echo $sql;die;
		// print_r($bind);die;
		
		$db = self::getAdapter();
		
		$count = $db->fetchOne($countSql, $bind);
		
		return parent::_fetchAll($sql, $bind);
	}
	
	/*public function remove($id = array())
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$client->call('Escorts.delete', array($escort_id));
		
		self::getAdapter()->delete('escorts', self::getAdapter()->quoteInto('id = ?', $id));
		self::getAdapter()->delete('escort_photos', self::getAdapter()->quoteInto('escort_id = ?', $id));
		self::getAdapter()->delete('escort_cities', self::getAdapter()->quoteInto('escort_id = ?', $id));
	}*/
	
	public function getNationalities()
	{
		$sql = "SELECT id, " . Cubix_I18n::getTblField('title') . " AS title FROM nationalities";

		return parent::_fetchAll($sql);
	}
	
	// -- >> FAVORITES
	public function addToFavorites($user_id, $escort_id)
	{
		$sql = 'INSERT INTO favorites (user_id, escort_id, application_id) VALUES(?, ?, ?)';
		
		$this->getAdapter()->query($sql, array($user_id, $escort_id, Cubix_Application::getId()));
	}
	
	public function removeFromFavorites($user_id, $escort_id)
	{
		$this->getAdapter()->query('DELETE FROM favorites WHERE user_id = ? AND escort_id = ?', array($user_id, $escort_id));
	}
	// FAVORITES << --
	
	// Profile Options
	
	public function getNationalityOptions()
	{
		return $this->getAdapter()->query('SELECT id, iso, title_en AS title FROM nationalities ORDER BY title_en ASC')->fetchAll();
	}
	
	public function addVacation($escort_id, $data)
	{	
		$this->_db->update('escorts', $data, $this->_db->quoteInto('id = ?', $escort_id));
	}
	
	public function removeVacation($escort_id)
	{		
		$this->_db->update('escorts', array('vac_date_from' => null, 'vac_date_to' => null), $this->_db->quoteInto('id = ?', $escort_id));
	}
	
	public function save($escort)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$escort_id = $client->call('Escorts.save', array( (array) $escort));
		$escort->setId($escort_id);
		
		return $escort;
	}

	public function getComments()
	{
		$config = Zend_Registry::get('escorts_config');
		$sql = "SELECT c.*, e.showname FROM comments c INNER JOIN escorts e ON e.id = c.escort_id GROUP BY e.id ORDER BY RAND() LIMIT ?";

		return $this->_db->query($sql, array($config['comments']['sidebarComentsCount']))->fetchAll();
	}

	public static function hit($escort_id)
	{
		$cache = self::_getCache();

		$data = $cache->load(self::HITS_CACHE_KEY);

		if ( ! is_array($data) ) {
			$data = array();
		}

		if ( ! isset($data[$escort_id]) ) {
			$data[$escort_id] = 0;
		}

		$data[$escort_id]++;

		$cache->save($data, self::HITS_CACHE_KEY);

		/*$cache_limit = Zend_Registry::get('system_config');
		$errors = array();
		if ( $total >= $cache_limit['hitsCacheLimit'] ) {
			$client = Cubix_Api_XmlRpc_Client::getInstance();

			$c = 0;
			while ( $c <= 5 ) {
				try {
					$result = $client->call('Escorts.updateHitsCount', array($data));
					if ( $result === true ) {
						$cache->remove(self::HITS_CACHE_KEY);
						break;
					}

					ob_start();
					var_dump($result);
					$result = ob_get_clean();

					$errors[] = 'Try #' . $c . ' Failed Got from API: ' . $result;
					$c++;
				}
				catch ( Exception $e) {
					$errors[] = 'Try #' . $c . ' Failed with Exception: ' . serialize($e);
					$c++;
				}
			}
			
			if ( count($errors) ) {
				Cubix_Debug::log(implode("\n", $errors), 'ERROR');
			}
		}*/
	}

	public static function getCachedHits($escort_id)
	{
		$data = self::_getCache()->load(self::HITS_CACHE_KEY);

		if ( is_array($data) && isset($data[$escort_id]) ) {
			return $data[$escort_id];
		}

		return 0;
	}

	public static function getHitsCount($escort_id)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();

		$cached = self::getCachedHits($escort_id);
		// $real = $client->call('Escorts.getHitsCount', array($escort_id));
		$real = intval(self::getAdapter()->fetchOne('SELECT hit_count FROM escorts WHERE id = ?', $escort_id));

		return $real + $cached;
	}

	public static function resetHits()
	{
		$cache = self::_getCache();
		$cache->remove(self::HITS_CACHE_KEY);
	}

	public static function getAllCachedHits()
	{
		$cache = self::_getCache();

		$data = $cache->load(self::HITS_CACHE_KEY);
		
		if ( ! $data ) $data = array();

		
		/*$client = Cubix_Api_XmlRpc_Client::getInstance();
		
		$c = 0;
		while ( $c <= 5 ) {
			try {
				$result = $client->call('Escorts.updateHitsCount', array($data));
				if ( $result === true ) {
					$cache->remove(self::HITS_CACHE_KEY);
					break;
				}

				ob_start();
				var_dump($result);
				$result = ob_get_clean();

				$errors[] = 'Try #' . $c . ' Failed Got from API: ' . $result;
				$c++;
			}
			catch ( Exception $e) {
				$errors[] = 'Try #' . $c . ' Failed with Exception: ' . serialize($e);
				$c++;
			}
		}
var_dump($errors);
		if ( count($errors) ) {
			Cubix_Debug::log(implode("\n", $errors), 'ERROR');
		}*/

		return $data;
	}

	protected static $_cache;

	const HITS_CACHE_KEY = 'v2_escort_profile_views_a6';

	/**
	 *
	 * @return Zend_Cache_Backend_Memcached
	 */
	protected static function _getCache()
	{
		return Zend_Registry::get('cache');
	}

	public function getProfile($escort_id)
	{
		return new Model_Escort_Profile(array('id' => $escort_id));
	}

    public function checkHasVideo($escort_id)
    {
        $sql = 'SELECT TRUE FROM escorts WHERE id = ? AND has_video = 1';


        return self::getAdapter()->fetchOne($sql,array($escort_id));
    }
}
