<?php

class Model_EscortV3Item extends Cubix_Model_Item
{
	public function __construct($rowData = array())
	{
		parent::__construct($rowData);

		$this->setThrowExceptions(true);
	}

	public function getMainPhoto()
	{
		return new Model_Escort_PhotoItem(array(
			'application_id' => $this->application_id,
			'escort_id' => $this->id,
			'hash' => $this->photo_hash,
			'ext' => $this->photo_ext,
			'photo_status' => $this->photo_status
		));
	}
}
