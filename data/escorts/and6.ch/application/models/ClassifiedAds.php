<?php

class Model_ClassifiedAds extends Cubix_Model
{
	public function getAllCitiesForFilter($country_id)
	{
		return parent::_fetchAll('
			SELECT c.id, ' . Cubix_I18n::getTblField('c.title') . ' AS title 
			FROM cantons c
			INNER JOIN classified_ads_cities ca ON ca.city_id = c.id
			GROUP BY ca.city_id
			ORDER BY title ASC
		');
	}
	
	public function getAllCategoriesForFilter()
	{
		$result = parent::_fetchAll('
			SELECT category_id
			FROM classified_ads 
			GROUP BY category_id
		');
		$ids = array();
		foreach ($result as $value) {
			$ids[] = $value->category_id;
		}
		return $ids;
	}
	//protected $_table = 'comments';
	//protected $_itemClass = 'Model_CommentItem';
	public function getList($filter = array(), $page = 1, $perPage = 20, &$count = null)
	{
		if( ! $page ) $page = 1;
		
		$limit = ' LIMIT ' . ($page - 1) * $perPage . ', ' . $perPage;
		$fields = '';
		$where = '';
		$order = '';
								
		$where = '';
		$order = ' is_premium DESC, premium_period DESC, approvation_date DESC ';
		
		if ( isset($filter['category']) && $filter['category'] ) {
			$where .= parent::quote(' AND ca.category_id = ?', $filter['category']);
		}
		
		if ( isset($filter['city']) && $filter['city'] ) {
			$where .= parent::quote(' AND cac.city_id = ?', $filter['city']);
		}
		
		if ( isset($filter['text']) && $filter['text'] ) {
			$where .= parent::quote(' AND ca.search_text LIKE ?', '%' . $filter['text'] . '%');
		}

		$sql = "
			SELECT SQL_CALC_FOUND_ROWS ca.*, c." . Cubix_I18n::getTblField('title') . " AS city_title
			FROM classified_ads ca
			INNER JOIN classified_ads_cities cac ON cac.ad_id = ca.id
			INNER JOIN cantons c ON c.id = cac.city_id
			WHERE 1 {$where}
			GROUP BY ca.id
			ORDER BY {$order}
			{$limit}
		";
		//echo $sql;die;
		$items = $this->getAdapter()->fetchAll($sql);
		$count = $this->getAdapter()->fetchOne('SELECT FOUND_ROWS()');
		
		if ( count($items) ) {
			foreach ( $items as $i => $item ) {
				$items[$i]->images = $this->getAdapter()->fetchAll('
					SELECT 
						id AS image_id, hash, ext, ' . Cubix_Application::getId() . ' AS application_id 
					FROM classified_ads_images
					WHERE ad_id = ?
				', array($item->id));
			}
		}
		
		return $items;
	}	
	
	public function save($data)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$ad_id = $client->call('ClassifiedAds.save', array($data));
		
		return $ad_id;
	}
	
	public function get($id)
	{
		$sql = "
			SELECT
				ca.*, c." . Cubix_I18n::getTblField('title') . " AS city_title
			FROM classified_ads ca
			INNER JOIN classified_ads_cities cac ON cac.ad_id = ca.id
			INNER JOIN cantons c ON c.id = cac.city_id
			WHERE ca.id = ?
		";
		
		$ad = $this->getAdapter()->fetchRow($sql, array($id));
		
		$ad_sql = '
			SELECT
				id AS image_id, hash, ext, ' . Cubix_Application::getId() . ' AS application_id 
			FROM classified_ads_images
			WHERE ad_id = ?
		';
		
		$ad->images = $this->getAdapter()->fetchAll($ad_sql, array($ad->id));
		
		return $ad;
	}
	
	public function checkAdByPhone($phone)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$ad_id = $client->call('ClassifiedAds.checkAdByPhone', array($phone));
		
		return $ad_id;
	}

	public function checkAdByIp($ip)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$data = $client->call('ClassifiedAds.checkAdByIp', array($ip));
		
		return $data['count'];
	}

	public function getPackageById($id)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$package = $client->call('ClassifiedAds.getPackageById', array($id));
		
		return $package;
	}

	public function getPrev($id, $filter = array())
	{
		$where = '';
		if(count($filter)){
			if($filter['category']){
				$where .= " AND ca.category_id = '". $filter['category']."'";;
			}
			if($filter['city']){
				$where .= " AND cac.city_id = '". $filter['city']."'";
			}
			if($filter['text']){
				$where .= " AND ca.search_text LIKE  '%". $filter['text'] .'%'."'";;
			}
		}

		$sql = "
			SELECT id
			FROM classified_ads ca
			INNER JOIN classified_ads_cities cac ON cac.ad_id = ca.id
			WHERE id = (SELECT min(id) FROM classified_ads WHERE id > ?)". $where
		;

		$prev_ad = $this->getAdapter()->fetchRow($sql, array($id));
		return $prev_ad;
	}

	public function getNext($id, $filter = array())
	{
		$where = '';
		if(count($filter)){
			if($filter['category']){
				$where .= " AND ca.category_id = '". $filter['category']."'";;
			}
			if($filter['city']){
				$where .= " AND cac.city_id = '". $filter['city']."'";
			}
			if($filter['text']){
				$where .= " AND ca.search_text LIKE  '%". $filter['text'] .'%'."'";;
			}
		}
		$sql = "
			SELECT id
			FROM classified_ads ca
			INNER JOIN classified_ads_cities cac ON cac.ad_id = ca.id
			WHERE id = (SELECT max(id) FROM classified_ads WHERE id < ?) ". $where
		;

		$next_ad = $this->getAdapter()->fetchRow($sql, array($id));
		return $next_ad;
	}

	public function getForWidget()
	{
		
		$limit = ' LIMIT 0, 10';
								
		$order = ' is_premium DESC, premium_period DESC, approvation_date DESC ';

		$sql = "
			SELECT ca.*, c." . Cubix_I18n::getTblField('title') . " AS city_title
			FROM classified_ads ca
			INNER JOIN classified_ads_cities cac ON cac.ad_id = ca.id
			INNER JOIN cantons c ON c.id = cac.city_id
			INNER JOIN classified_ads_images cai ON cai.ad_id = ca.id
			WHERE 1
			GROUP BY ca.id
			ORDER BY {$order}
			{$limit}
		";
		$items = $this->getAdapter()->fetchAll($sql);
		foreach ($items as $i => $item) {
			$items[$i]->image = $this->getAdapter()->fetchRow('
					SELECT 
						id AS image_id, hash, ext, ' . Cubix_Application::getId() . ' AS application_id 
					FROM classified_ads_images
					WHERE ad_id = ? LIMIT 1
				', array($item->id));
		}
		
		return $items;
	}	
}
