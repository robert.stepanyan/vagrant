<?php
class Model_Escorts_New_History extends Cubix_Model
{
	protected $_table = 'escorts_new_history';
	

	public function __construct(){
		$this->_db = Zend_Registry::get('db');
	}

	public function save($data){
		
		$client = new Cubix_Api_XmlRpc_Client();

		if(in_array($data['package_id'], array(19)) && $this->checkEscortGets1DayNew($data['escort_id']) && !$this->checkEscortLast60Days($data['escort_id'])){
			// $sql = "INSERT into escorts_new_history (escort_id, expires_at, type) VAlUES (".$data['escort_id'].", NOW() + INTERVAL 1 DAY, 3)";

			return $client->call('Escorts.setNewHistory', array($data['escort_id'], 1, 3));

		} elseif($this->checkEscortLast60Days($data['escort_id'])){
			// $sql = "INSERT into escorts_new_history (escort_id, expires_at, type) VALUES (".$data['escort_id'].", NOW() + INTERVAL 5 DAY, 2)";

			return $client->call('Escorts.setNewHistory', array($data['escort_id'], 5, 2));

		}

	}

	public function checkEscortLast60Days($escort_id){
		
		$client = new Cubix_Api_XmlRpc_Client();

		// $sql = "SELECT expires_at as expires_at from escorts_new_history where escort_id = '".$escort_id."' order by id DESC LIMIT 1 ";
		// $last_p = $this->_db->query($sql)->fetch();
		$last_p = $client->call('Escorts.checkEscortLast60Days', array($escort_id));

		$date = date ( 'Y-m-d' );
		$date = strtotime ( '-60 days' , strtotime($date));
		$date = date ( 'Y-m-d h:m:s' , $date );
		return $date > $last_p['expires_at'];
	}

	public function checkEscortGets1DayNew($escort_id){
		
		$client = new Cubix_Api_XmlRpc_Client();

		// $sql = "SELECT count(*) as `count` from escorts_new_history where escort_id = '".$escort_id."' AND expires_at > NOW() - INTERVAL 5 DAY";
		// $count = $this->_db->query($sql)->fetch();
		$count = $client->call('Escorts.checkEscortGets1DayNew', array($escort_id));
		return $count < 5;
	}
}
