<?php

class Model_Mobile_FakeCities extends Cubix_Model 
{
	public function getAll()
	{
		$sql = '
			SELECT f.id, f.' . Cubix_I18n::getTblField('title') . ' as title, f.slug, a.area_id
			FROM f_cities f
			INNER JOIN f_cities_area a
			ON f.id = a.f_city_id
			ORDER BY title
		';

		return parent::_fetchAll($sql);
	}

	static public function getBySlug( $slug ){
		$sql = '
			SELECT fc.id, fc.' . Cubix_I18n::getTblField('title') . ' as title, fc.slug, fca.area_id, c.' . Cubix_I18n::getTblField('title') . ' as city_title
			FROM f_cities fc
			INNER JOIN f_cities_area fca
			ON fc.id = fca.f_city_id
			INNER JOIN cities c
			ON fca.area_id = c.id
			WHERE fc.slug = "' . $slug . '"
		';

		return parent::_fetchAll($sql);
	}
}
