<?php

class Mobile_ClassifiedAdsController extends Zend_Controller_Action
{
	/**
	 * @var Zend_Session_Namespace
	 */
	public static $linkHelper;
	protected $_session;
	protected $_perPages = array(10, 25, 50, 100);
	const CURRENCY = 'CHF';

	public function init()
	{
		self::$linkHelper = $this->view->getHelper('GetLink');
		$this->view->layout()->setLayout('mobile-classified-ads');
		$this->client = new Cubix_Api_XmlRpc_Client();
		$this->_session = new Zend_Session_Namespace('classified-ads');
		$this->model = new Model_ClassifiedAds();
		
		$this->view->per_pages = $this->_perPages;

	}
	
	public function indexAction()
	{

		$defines = $this->view->defines = Zend_Registry::get('defines');

        $this->view->cities = $this->model->getAllCitiesForFilter(Cubix_Application::getById()->country_id);

		$this->view->perPage = $perPage = $this->_getParam('pp', 24);
		
		if ( ! in_array($perPage, $this->_perPages) ) {
			$this->view->perPage = $perPage = 24;
		}
		$this->view->page = $page = 1;
		//get Classifield adds list which is not empty EFNET 1038
        $this->view->defines = $this->listClassifieldAdds($defines['classified_ad_categories']);
		$this->view->ads = $ads = $this->model->getList($filter, $page, $perPage, $count);

		$this->view->count = $count;

		//print_r($this->view->ads);
	}
	
	public function adAction()
	{
		$id = (int)$this->_request->id;
		
		$this->view->ad = $this->model->get($id);
		
		if ( ! isset($this->view->ad->id) ) {
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: ' . $this->view->getLink('classified-ads-index'));
			die;
		}
	}

	public function printAction()
	{
		$this->view->layout()->disableLayout();
		
		$ad_id = (int) $this->_request->id;
		
		$this->view->ad = $this->model->get($ad_id);
	}
	
	public function ajaxFilterAction()
	{
		$defines = $this->view->defines = Zend_Registry::get('defines');
		
		$this->view->cities = $this->model->getAllCitiesForFilter(Cubix_Application::getById()->country_id);
		
		$this->view->layout()->disableLayout();
		$req = $this->_request;
        //get Classifield adds list which is not empty EFNET 1038
        $this->view->defines = $this->listClassifieldAdds($defines['classified_ad_categories']);
		$this->view->category = $category = (int)$req->category;
		$this->view->city = $city = (int)$req->city;
		$this->view->text = $text = $req->text;
	}
	
	public function ajaxListAction()
	{
		$defines = $this->view->defines = Zend_Registry::get('defines');
		$m_cities = new Cubix_Geography_Cities();
		$this->view->cities = $m_cities->ajaxGetAll(null, Cubix_Application::getById()->country_id);
		
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$this->view->perPage = $perPage = $this->_getParam('pp', 24);
		
		if ( ! in_array($perPage, $this->_perPages) ) {
			$this->view->perPage = $perPage = 24;
		}
		
		$page = $req->page;
		if ( $page < 1 ) {
			$page = 1;
		}
		$this->view->page = $page;
		
		$filter = array();
		
		$category = $req->category;
		$city = (int)$req->city;
		$text = $req->text;
		
		if ( $category ) {
			$filter['category'] = $category;
		}
		
		if ( $city ) {
			$filter['city'] = $city;
		}
		
		if ( strlen($text) ) {
			$filter['text'] = $text;
		}
		
		$this->view->ads = $this->model->getList($filter, $page, $perPage, $count);
		$this->view->count = $count;
	}
	
	public function placeAdAction()
	{
		$defines = $this->view->defines = Zend_Registry::get('defines');
		$blackListModel = new Model_BlacklistedWords();
		$m_cities = new Model_Cantons();
		$this->view->cities = $m_cities->getAll();
		$steps = array(1, 2, 3, 'finish');
		$edit_mode = false;
		$req = $this->_request;
		$step = $req->getParam('step', 1);
		$edit = (int) $req->getParam('edit');
		
		$plan = $req->getParam('plan', false);
		
		$data = array();
		
		$ip = Cubix_Geoip::getIP();

		if ( !in_array($step, $steps) ) {
			$step = 1;
		}
		
		if ( isset($this->_session->data) && count($this->_session->data) && $edit ) {
			$edit_mode = true;
		}
		
		if ( ! $req->isPost() && ! $edit ) {
			$this->_session->unsetAll();
		}
		
		if ( ! isset($this->_session->data) && ! count($this->_session->data) && ($step == 2 || $step == 3 || $step == 'finish') ) {
			$step = 1;
		}
		
		
		if ( $edit_mode ) {
			$data = $this->_session->data;
		}
		
		$photos = array();
		if ( count($this->_session->photos) ) {
			$photos = $this->_session->photos;
		}
		
		$errors = array();
		if ( $req->isPost() ) 
		{
			if ( ! $plan ) {
				try {
					$images = new Cubix_ImagesCommon();
					foreach ( $_FILES as $i => $file ) {
						if ( strlen($file['tmp_name']) ) {
						    try{
                                $photo = $images->save($file);
                            }catch (Exception $e){
						        // var_dump($e->getMessage());die;
                            }
							if ( $photo && count($photos) < 3  ) {
								$photos[$photo['image_id']] = $photo;
							}
						}
					}
				} catch ( Exception $e ) {
					$errors['photos'][] = $e->getMessage();
				}

				$category = (int) $req->category;
				$city = (int) $req->city;
				$cities =  $req->cities;
				$phone = $req->phone;
				$email = $req->email;
				$web = $req->web;
				$duration = 30;

				$title = trim($req->title);
				$text = $req->text;

				if ( ! $category ) {
					$errors['category'] = Cubix_I18n::translate('sys_error_required');
				}

				/*if ( ! $city ) {
					$errors['city'] = Cubix_I18n::translate('sys_error_required');
				}*/
				if ( ! count($cities) ) {
					$errors['city'] = Cubix_I18n::translate('sys_error_required');
				}

				if ( ! strlen($title) ) {
					$errors['title'] = Cubix_I18n::translate('sys_error_required');
				}

				$_text = strip_tags(trim($text));
				$_text = str_replace('&nbsp;', '', $_text);
				$_text = preg_replace('/\s+/', '', $_text);
				
				if ( ! strlen($text) ) {
					$errors['text'] = Cubix_I18n::translate('sys_error_required');
				}
				else if ($bl_words = $blackListModel->checkWords($text, Model_BlacklistedWords::BL_TYPE_CLASSIFIED_ADS)){
					foreach($bl_words as $bl_word){
						if($bl_word['type'] == Model_BlacklistedWords::BL_SEARCH_TYPE_EXACT){
							$pattern = '/(\W+|^)'. preg_quote($bl_word['bl_word'], '/') . '(\W+|$)/i';
							$text = preg_replace($pattern, '\1<abbr title="Blacklisted word" class = "black-listed" >' . $bl_word['bl_word'] . '</abbr>\2', $text);
						}
						else{
							$pattern = '/' . preg_quote($bl_word['bl_word'], '/') . '/i';
							$text = preg_replace($pattern, '<abbr title="Blacklisted word" class = "black-listed" >' . $bl_word['bl_word'] . '</abbr>', $text);
						}
					}
					$errors['text'] = 'You can`t use word "'.$blackListModel->getWords().'"';
					
				}
				if ( ! strlen($phone) ) {
					$errors['phone_email'] = Cubix_I18n::translate('sys_error_required');
				} else{
					$contact_phone = $phone;
					$phone_prefix = '1-41-0'; // for Switzerland only
					
					$phone_parsed = null;
					if ($contact_phone ) {
						if ( ! preg_match("/^[0-9\s\+\-\(\)]+$/i", $contact_phone) ) {
							$errors['phone_email'] = 'Invalid phone number';
						}
						elseif(preg_match("/^(\+|00)/", $contact_phone) ) {
							$errors['phone_email'] = 'Please enter phone number without country calling code';
						}

					}
					list($country_id,$phone_prefix,$ndd_prefix) = explode('-',$phone_prefix);
					$phone_parsed = preg_replace('/[^0-9]+/', '', $contact_phone);
					$phone_parsed = preg_replace('/^'.$ndd_prefix.'/', '', $phone_parsed);
					$phone_parsed = '00'.$phone_prefix.$phone_parsed;
				} 
				
				if ( $this->model->checkAdByIp($ip) >= 2) {
					$errors['top'] = Cubix_I18n::translate('sys_error_ads_per_ip_per_day');
				}
								
				
				if ( strlen($email) ) {
					$valid = new Cubix_Validator();
					
					if ( ! $valid->isValidEmail($email) ) {
						$errors['invalid_email'] = Cubix_I18n::translate('invalid_email');
					}				
				}
				if ( strlen($web) ) {
					$valid = new Cubix_Validator();
					
					if ( ! preg_match("/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i", $web) ) {
						$errors['invalid_web'] = Cubix_I18n::translate('sys_error_invalid_url');
					}								
				}

				$data = array(
					'category' => $category,
					//'city' => $city,
					'cities' => $cities,
					'phone' => $phone,
					'phone_parsed' => $phone_parsed,
					'email' => $email,
					'web' => $web,
					'duration' => $duration,
					'title' => $title,
					'text' => $text
				);
				
				$user = Model_Users::getCurrent();
				$data['user_id'] = null;
				if ( $user ) {
					$data['user_id'] = $user->id;
				}
				

				$this->_session->photos = $photos;

				if ( ! count($errors) ) {
					$this->_session->data = $data;
					$step = 2;
				}
			} else {
				//FINISH Stage
				$packages = $defines['classified_ad_packages_arr'];
				
				if ( !in_array($plan, $packages) ) {
					$this->_redirect($this->view->getLink('classified-ads-place-ad'));
				}
				
				/************************* IP ****************************/
				
				$user = Model_Users::getCurrent();
				$data['user_id'] = null;
				if ( $user ) {
					$data['user_id'] = $user->id;
				}
				$data['ip'] = $ip;
				/*********************************************************/
												
				$m_ads = new Model_ClassifiedAds();
				
				$save_data = array('data' => $data, 'photos' => $photos);
					
				$ad_id = $m_ads->save($save_data);
					
				if ( $ad_id ) {
					$this->_session->unsetAll();
				}
				
				if ( $plan == 'free' ) {
					$this->_redirect($this->view->getLink('classified-ads-success') . '?id=' . $ad_id);
				} else {
					$payment_method = $req->getParam('payment_method');
					if($payment_method == 'epg'){
						$package = $this->model->getPackageById($plan);
						
						$this->view->amount = number_format($package['price'], 2);
						$this->view->currency = self::CURRENCY;
						$this->view->reference = 'CA-'. $ad_id. '_' . $package['id'];
							
						$epg_payment = new Model_EpgGateway();
						$success_url = 'http://m.' . Cubix_Application::getById()->host . $this->view->getLink('classified-ads-success') . '?id='. $ad_id;
						$token = $epg_payment->getTokenForAuth($this->view->reference, $this->view->amount, $success_url);
						$this->client->call('OnlineBilling.storeToken', array($token, null));
						
						$this->view->redirect_url = $epg_payment->getPaymentGatewayUrlV2($token);
						$this->view->use_epg = true;
					} elseif($payment_method == 'postfinance'){
						$package = $this->model->getPackageById($plan);
						$hash  = base_convert(time(), 10, 36);
						$postfinance_payment = new Model_Postfinance();
						
						$pf_data = array(
							'PSPID' => $postfinance_payment->PSPID,
							'ORDERID' =>  'CA-'. $ad_id. '-' . $package['id'],
							'AMOUNT' =>  $package['price'] * 100,
							'LANGUAGE' => 'de_DE',
							'CURRENCY' => $postfinance_payment->currency,
							'ACCEPTURL' => 'http://m.' . Cubix_Application::getById()->host . $this->view->getLink('classified-ads-success') . '?id='. $ad_id,
							'DECLINEURL' => 'http://m.' . Cubix_Application::getById()->host . $this->view->getLink('classified-ads-error') . '?id='. $ad_id,
							'EXCEPTIONURL' => 'http://m.' . Cubix_Application::getById()->host . $this->view->getLink('classified-ads-error') . '?id='. $ad_id,
							'CANCELURL' => 'http://m.' . Cubix_Application::getById()->host . $this->view->getLink('classified-ads-error') . '?id='. $ad_id,
						);

						$pf_data['SHASIGN'] = $postfinance_payment->generateChecksum($pf_data);
						try {
							$payment_from = '<form method="post" action="' . $postfinance_payment->submitUrl . '"id=form1 name=form1>';

							foreach($pf_data as $k => $v) {
							    $payment_from .= '<input type="hidden" name="' . $k . '" value="'. $v . '">';
							}

							$payment_from .= '<input type="submit" value="" id=submit2 name=submit2>';
							$payment_from .= '</form>';

						} catch(Exception $e) {
							$message = $e->getMessage();
						}
						$this->view->use_postfinance = true;
						$this->view->postfinance_form = $payment_from;
					} elseif($payment_method == 'phone'){
						// $package = $this->model->getPackageById($plan);
						// $reference = 'SC3_' . $this->user->id . '_' . $hash;
						// $reference = 'CA_'. $ad_id. '_' . $package['id'];
						// $this->_redirect( $this->getIvrUrl($package['price'], $reference, $ad_id));
					}
				}
			}
		}
		
		$this->view->errors = $errors;
		$this->view->step = $step;
		$this->view->data = $data;
		$this->view->photos = $photos;
		$this->view->photos_count = count($photos);
		$this->view->categories = $this->listClassifieldAdds($defines['classified_ad_categories']);
		//var_dump($data);
		//var_dump($photos);
	}
	
	public function ajaxRemovePhotoAction()
	{
		$image_id = (int) $this->_request->image_id;
		
		$status = array('status' => 'error');
		if ( isset($this->_session->photos[$image_id]) ) {
			unset($this->_session->photos[$image_id]);
			$status = array('status' => 'success');
		}
		
		die(json_encode($status));
	}
	
	public function successAction()
	{
		$this->view->id = (int) $this->_request->id;
	}
	
	public function errorAction()
	{
		
	}

    //return Classifield adds list which is not empty EFNET 1038
    public function listClassifieldAdds($classifieldAdds){
        $tmpClassifieldAdds = array();

        foreach ($classifieldAdds as $key => $value){
            if($this->model->getList(array('category'=>$key))) {
                $tmpClassifieldAdds[$key] = $value;
            }
        }
        return (array)$tmpClassifieldAdds;
    }

    public function viewAction(){

        $defines = Zend_Registry::get('defines');

        $this->view->categories = $defines['classified_ad_categories'];

        $id = $this->_request->ad;
        $this->view->ad = $ad = $this->model->get($id);

    }
}