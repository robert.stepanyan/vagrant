<?php

class Mobile_CommentsController extends Zend_Controller_Action
{
	public function init()
	{
		$this->_helper->layout->setLayout('mobile-index');
		$this->model = new Model_Comments();
	}


	public function ajaxAddCommentAction()
	{
		$this->view->layout()->disableLayout();

		$config = Zend_Registry::get('escorts_config');
		$textLength = $config['comments']['textLength'];

		$req = $this->_request;
		$back_url = $req->url;

		$this->view->escort_id = $escort_id = intval($req->escort_id);
		$this->view->user_id = $user_id = intval($req->user_id);
		
		$current_user = Model_Users::getCurrent();
		$this->view->user_type  =  $user_type = $current_user->user_type;

		$comment = $req->comment;

		if ( $req->isPost() ) {

			$blackListModel = new Model_BlacklistedWords();
			$errors = array();

			if ( strlen($comment) < $textLength ) {
				$errors[] = 'comment_to_short';
			}

			if ( count($errors) > 0 ) {
				die(json_encode(array('status' => 'error', 'msgs' => $errors)));
			} else {

				$data = array(
					'user_id' => $user_id,
					'escort_id' => $escort_id,
					'status' => Model_Comments::COMMENT_NOT_APPROVED,
					'message' => $comment,
					'user_type' => $user_type
				);
				
				if($blackListModel->checkWords($data['message'])){
					$data['has_bl'] = 1;
				}
				
				// ip
				if ($_SERVER["HTTP_X_FORWARDED_FOR"] != "")
				{
					// for proxy
					$ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
				}
				else
				{
					// for normal user
					$ip = $_SERVER["REMOTE_ADDR"];
				}
				
				$data['ip'] = $ip;
				

				if (IS_MOBILE_DEVICE) {
					$data['is_mobile'] = 1;
				} else {
					$data['is_mobile'] = 0;
				}
				
				if(!$current_user) {
					$comm_sess = new Zend_Session_Namespace('comment');
					$comm_sess->data = $data;
					$comm_sess->url = $back_url;

					die(json_encode(array('status' => 'pending')));
				} else {
					$this->model->addComment($data);

					die(json_encode(array('status' => 'success')));
				}
			}
		}
	}

	public function indexAction()
	{
	    $model_escorts = new Model_EscortsV2();
		$req = $this->_request;

		
		$page = $req->getParam('page', 1);
		$per_page = 10;
		$comments_count = 1;
		$region = $req->getParam('region', 'deutschschweiz');

		if ($req->getParam('ajax')) {
			$this->view->layout()->disableLayout();
		}

		$comments_data = $this->model->getLatestComments($page, $per_page, $comments_count, $region);
		
	    $count = $comments_data['count'];
		unset($comments_data['count']);
		
		$comments = array();
		$data = array();

	    if ($comments_data) {

	        foreach( $comments_data as $k => $comment ) {

	            $comments[$k]['comments'] = $comment;

	            if(isset($comment[0])){
	                $data['hash'] = $comment[0]['hash'];
	                $data['ext'] = $comment[0]['ext'];
	                $data['escort_id'] = $k;
	                $data['application_id'] = Cubix_Application::getId();

	                $photo_url = new Model_Escort_PhotoItem($data);
	                $comments[$k]['escort']['photo'] = $photo_url->getUrl('lvthumb');
	                $comments[$k]['escort']['showname'] = $comment[0]['showname'];
					$comments[$k]['escort']['escort_id'] = $comment[0]['escort_id'];
	            }
	        }
		}
		
		$this->view->region = $region;
		$this->view->comments = $comments;
		$this->view->page = $page;
		$this->view->count = $count;
		$this->view->per_page = $per_page;
	}
}
