<?php

class Mobile_CamBookingController extends Zend_Controller_Action
{
	public function init()
	{
		 $this->model = new Model_CamBooking();
		 $this->user = Model_Users::getCurrent();
	}

	public function saveAction(){
		 
		if (is_null($this->_getParam('ajax')) ) {
			
			$form = new Cubix_Form_Data($this->_request);
			$fields = array(
				'escort_id' => 'int-nz',
				'status'=> '',
				'30min' => 'int-nz',
				'60min' => 'int-nz',
				'contact_option'=> 'int-nz'
			);

			$form->setFields($fields);

			$data = $form->getData();
			
			//permission check
			if($this->user->isEscort()){
				$escort = $this->user->getEscort();
				$escort_id = $escort->id;
				if($escort_id != $data['escort_id'])die('access denied');
			}elseif( $this->user->isAgency() ){
				$agency = $this->user->getAgency();
				$status = $agency->hasEscort( $data['escort_id'] );
				if($status === false)die('access denied #9667892');
			}else{
				die('identification error#324');
			}
			
			$validator = new Cubix_Validator();

			if($data['status'] == 'on'){
				$data['status'] = 1;
			}else{
				$data['status'] = 0;
			}

			if ( ! strlen($data['escort_id']) ) {
				$validator->setError('subject', 'id is Required!');
			}

			if ( ! strlen($data['30min']) ) {
				$validator->setError('30min', ' Price is Required!');
			}

			if ( ! strlen($data['60min']) ) {
				$validator->setError('60min', ' Price is Required!');
			}

			
			if ( $validator->isValid() ) {
			 $status = $this->model->save($data);
				die(json_encode($status));
			}else{
				die(json_encode($validator->getStatus()));
			}
		}
		else {
			$this->view->layout()->disableLayout();
			var_dump('nowaybro');die;
		}
		die;
	}

	
}