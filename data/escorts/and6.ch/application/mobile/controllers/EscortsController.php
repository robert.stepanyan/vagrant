<?php

class Mobile_EscortsController extends Zend_Controller_Action {

	public static $linkHelper;

	public function init() {
		$this->_helper->layout->setLayout('mobile-index');

		$_SESSION['request_url'] = $_SERVER['REQUEST_URI'];
		self::$linkHelper = $this->view->getHelper('GetLink');
		//die('Mobile version is temporary unavailable');
	}

    public function happyHoursAction()
    {
        $action = 'index';
        if ( ! is_null($this->_getParam('ajax')) ) {
            $action = 'list';
        }

        $this->_helper->viewRenderer->setScriptAction($action);

        $req = $this->_getParam('req');
        $param = explode('/', $req);
        $page = explode("_", ( isset($param[2]) ) ? $param[2] : '' );

        $region_slug = $this->_request->region;
        $filter = array('e.gender = ?' => 1, 'e.hh_is_active = ?' => 1, 'r.slug = ?' => $region_slug);

        $page_num = 1;
        if ( isset($page[1]) && $page[1] ) {
            $page_num = $page[1];
        }

        $count = 0;
        $escorts = Model_Escort_List::getFiltered($filter, 'random', $page_num, 10, $count);

        $this->view->count = $count;
        $this->view->escorts = $escorts;

        list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls();

        $this->view->hh_page = true;
    }

	public function ajaxBubbleAction()
	{
		$this->view->layout()->disableLayout();

		$region = $this->_request->r;
		$page = intval($this->_getParam('page', 1));

		$per_page = 1; //intval($this->_getParam('per_page', 1));
		if ( $page < 1 ) $page = 1;

		$cache = Zend_Registry::get('cache');

		$cache_key = 'mobile_widget_bubble_texts_page_' . $page . '_' . $region;
		if ( ! $bubbles = $cache->load($cache_key) ) {
			try {
				$client = Cubix_Api_XmlRpc_Client::getInstance();
				$bubbles = $client->call('Escorts.getBubbleTexts', array($page, $per_page, $region));

			}
			catch ( Exception $e ) {
				$bubbles = array('texts' => array(), 'count' => 0);
			}

			foreach ( $bubbles['texts'] as &$text ) {
				$text = new Model_EscortV2Item($text);
			}

			$cache->save($bubbles, $cache_key, array(), 300);

		}
		
		$this->view->per_page = $per_page;
		$this->view->page = $page;
		$this->view->bubbles = $bubbles;

		$this->view->region = $region;
	}

	public function statusMessagesAction(){
        $req = $this->_request;
        $city = $req->city;
        $model_cities = new Model_Cities();
        $app = Cubix_Application::getById();

        $client = Cubix_Api_XmlRpc_Client::getInstance();

        if( $app->country_id ){
            $this->view->cities = $client->call('Escorts.getCitiesWithStatusMessages', array( $app->country_id ) );
        }

        if( $req->is_ajax ){
            $this->view->layout()->disableLayout();
        }

        $page = intval( $req->page );

        $per_page = 10; //intval($this->_getParam('per_page', 1));
        if ( $page < 1 ) $page = 1;

        $cache = Zend_Registry::get('cache');

        $cache_key = 'mobile_widget_bubbles_messages_non_ajax_page_' . $page . '_' . $city;
        if ( ! $bubbles = $cache->load($cache_key) ) {
            try {

                if ( !$city && isset($req->lat) && isset($req->long) ){
                    $bubbles = $client->call('Escorts.getBubbleTextsMobile', array( $page, $per_page, null, null, array( 'long' => $req->long, 'lot' => $req->lat ) ) );
                } else {
                    $bubbles = $client->call('Escorts.getBubbleTextsMobile', array( $page, $per_page, $city ) );
                }
            }
            catch ( Exception $e ) {
                $bubbles = array('texts' => array(), 'count' => 0);
            }

            foreach ( $bubbles['texts'] as &$text ) {
                $text['city_title'] = $model_cities::getTitleById( $text['city_id'] )->title;
                $text = new Model_EscortV2Item($text);
            }

            $cache->save($bubbles, $cache_key, array(), 300);
        }

        $this->view->per_page = $per_page;
        $this->view->page = $page;
        $this->view->bubbles = $bubbles;
        $this->view->long = $req->long;
        $this->view->lat = $req->lat;

        $this->view->city = $city;
    }

    public function statusMessagesAjaxAction(){
        $req = $this->_request;
        $city = $req->city;
        $model_cities = new Model_Cities();

        $this->view->layout()->disableLayout();

        $page = intval( $req->page );

        $per_page = 10; //intval($this->_getParam('per_page', 1));
        if ( $page < 1 ) $page = 1;

        $cache = Zend_Registry::get('cache');

        $cache_key = 'mobile_widget_bubbles_messages_non_ajax_page_' . $page . '_' . $city;
        if ( ! $bubbles = $cache->load($cache_key) ) {
            try {
                $client = Cubix_Api_XmlRpc_Client::getInstance();
				
                if ( !$city && isset($req->lat) && isset($req->long) ){
                    $bubbles = $client->call('Escorts.getBubbleTextsMobile', array( $page, $per_page, null, null, array( 'long' => $req->long, 'lot' => $req->lat ) ) );
					
                } else {
                    $bubbles = $client->call('Escorts.getBubbleTextsMobile', array( $page, $per_page, $city ) );
                }

                // echo $bubbles; die;
            }
            catch ( Exception $e ) {
                $bubbles = array('texts' => array(), 'count' => 0);
            }

            foreach ( $bubbles['texts'] as &$text ) {
                $text['city_title'] = $model_cities::getTitleById( $text['city_id'] )->title;
                $text = new Model_EscortV2Item($text);
            }

            $cache->save($bubbles, $cache_key, array(), 300);
        }

        $this->view->per_page = $per_page;
        $this->view->page = $page;
        $this->view->bubbles = $bubbles;
        $this->view->long = $req->long;
        $this->view->lat = $req->lat;

        $this->view->city = $city;
    }

	public function indexAction()
    {

        $route_name = Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName();
        $redirect_params = array();
		$cache = Zend_Registry::get('cache');
		
        $trans = explode('/', $this->_getParam('req'))[1] == 'trans';
        $all_girls = explode('/', $this->_getParam('req'))[1] == 'regular';

        $city = '';
        if ($this->_getParam('req') != '/happy-hours') {
            if (!$trans && ($route_name == 'mobile-escorts' || $route_name == 'mobile-escorts-def')) {
                $params = explode('/', $this->_getParam('req'));
                foreach ($params as $param) {
                    $param = explode('_', $param);
                    if (count($param) > 1) {
                        switch ($param[0]) {
                            case 'region' :
                                $redirect_params['region'] = $param[1];
                                break;
                            case 'city' :
                                $redirect_params['city'] = $param[2];
                                $m = new Cubix_Geography_Cities();
                                $city = $m->getBySlug($redirect_params['city']);
                                if (!$city) {
                                    $this->_redirect('/', array('code' => 301));
                                    return;
                                }
                                $redirect_params['city'] = $city->city_url_slug;
                                $redirect_params['city_id'] = $city->id;
                                break;
                        }
                    }
                }

                $this->_redirect(self::$linkHelper->getLink('escorts', $redirect_params, true), array('code' => 301));
            } else {

                $city = Model_Countries::getCityById($this->_getParam('city_id'));

                if ($this->_getParam('city') != $city->url_slug) {
                    $redirect_params = array(
                        'city' => $city->url_slug,
                        'city_id' => $city->id
                    );
                    $this->_redirect(self::$linkHelper->getLink('escorts', $redirect_params, true), array('code' => 301));
                }
            }
        }
        $lng = Cubix_I18n::getLang();

        $req = $this->_getParam('req');
        $config = Zend_Registry::get('mobile_config');

        // Scroll Spy
        if ($this->_request->ajax) {
            $this->view->spy = true;
            $this->view->layout()->disableLayout();
        }

        if (preg_match('#/([0-9]+)$#', $req, $a)) {
            $a[1] = intval($a[1]);
            $req = preg_replace('#/[0-9]+$#', '/page_' . $a[1], $req);
        }

        $req = explode('/', $req);

        $params = array(
            'sort' => 'random',
            'filter' => array(/*array('field' => 'girls', 'value' => array())*/),
            'page' => 1
        );


        if ($this->_getParam('city_id')) {

            $params['filter'][] = array('field' => 'city_id', 'value' => $this->_getParam('city_id'));
            $params['city_id'] = $this->_getParam('city_id');
            $m_video = new Model_Video();
            $this->view->votd = $votd = $m_video->getVotdByCityId($this->_getParam('city_id'));
        }
        $static_page_key = 'main';
		
        foreach ($req as $r) {
            if (!strlen($r))
                continue;
            $param = explode('_', $r);
            if (count($param) < 2) {
                switch ($r) {
                    case 'nuove':
                        $static_page_key = 'nuove';
                        $param = array('sort', 'newest');
                        break;
                    case 'independantes':
                        $static_page_key = 'independantes';
                        $params['filter'][] = array('field' => 'independantes', 'value' => array());
                        continue;
                    case 'regular':
                        $static_page_key = 'regular';
                        $params['filter'][] = array('field' => 'regular', 'value' => array());
                        continue;
                    case 'agence':
                        $static_page_key = 'agence';
                        $params['filter'][] = array('field' => 'agence', 'value' => array());
                        continue;
                    case 'happy-hours':
                        $static_page_key = 'happy-hours';
                        continue;
                    case 'boys':
                        $static_page_key = 'boys';
                        $params['filter'][0] = array('field' => 'boys', 'value' => array());
                        continue;
                    case 'trans':
                        $static_page_key = 'trans';
                        $params['filter'][0] = array('field' => 'trans', 'value' => array());
                        continue;
                    case 'citytours':
                        $static_page_key = 'citytours';
                        $this->view->is_tour = $is_tour = true;
                        $params['filter'][] = array('field' => 'tours', 'value' => array());
                        continue;
                    case 'virtual':
                        $static_page_key = 'cam-booking-available';
                        $params['filter'][] = array('field' => 'cam-booking-available', 'value' => array());
                    continue;
                    case 'upcomingtours':
                        $static_page_key = 'upcomingtours';
                        $this->view->is_tour = $is_tour = true;
                        $this->view->is_upcomingtour = true;
                        $upcoming_tours = true;
                        $params['filter'][] = array('field' => 'upcomingtours', 'value' => array());
                        continue;
                    default:
                        $this->_response->setRawHeader('HTTP/1.1 404 Not Found');
                        $this->_forward('error', 'error', null, array('error_msg' => '404 Page Not Found. Please try the following links instead.'));
                        return;
                }
            }

            $param_name = $param[0];
            array_shift($param);


            $this->view->static_page_key = $static_page_key;

            if ($static_page_key == 'nuove') {
                $this->_forward('new-list');
                return;
            }

              if ($static_page_key == 'cam-booking-available') {
                    $link = $this->view->getLink('virtual');
                    $this->_redirect($link);
                return;
            }
			
            /*switch ($param_name) {
                case 'filter':
                    $has_filter = true;

                    $selected_item = $menus['filter']->getByValue(implode('_', $param));
                    if (!is_null($selected_item)) {
                        $menus['filter']->setSelected($selected_item);
                    }

                    $field = reset($param);
                    array_shift($param);

                    $value = array_slice($param, 0, 2);

                    $params['filter'][] = array('field' => $field, 'value' => $value, 'main' => TRUE);
                    break;
                case 'page':
                    $page = intval(reset($param));

                    if ($page < 1) {
                        $page = 1;
                    }


                    $params['page'] = $page;
                    break;
                case 'sort':
                    $params['sort'] = reset($param);

                    $selected_item = $menus['sort']->getByValue($params['sort']);
                    if (!is_null($selected_item)) {
                        $menus['sort']->setSelected($selected_item);
                    }
                    $has_filter = true;
                    break;
                case 'region':
                case 'state':
                    $this->_request->setParam('region', $param[0]);
                    $params['filter'][] = array('field' => 'region', 'value' => $param[0]);
                    break;
                case 'city':
                case 'zone':
                    $params[$param_name] = $param[1];
                    $params['filter'][] = array('field' => $param_name, 'value' => array($param[1]));
                    break;
                case 'fcity':
                    $params[$param_name] = $param[0];
                    $params['filter'][] = array('field' => $param_name, 'value' => array($param[0]));
                    break;
                case 'name':
                    $has_filter = true;
                    $params['filter'][] = array('field' => 'name', 'value' => array('%' . $param[0] . '%'));
                    break;
                default:
                    if (!in_array($param_name, array('nuove', 'independantes', 'regular', 'agence', 'boys', 'trans', 'citytours', 'upcomingtours', 'happy-hours'))) {
                        $this->_response->setRawHeader('HTTP/1.1 404 Not Found');
                        $this->_forward('error', 'error', null, array('error_msg' => '404 Page Not Found. Please try the following links instead.'));
                        return;
                    }
            }*/


        }

        $list_types = array('simple', 'gallery');

        if (isset($_COOKIE['list_type']) && $_COOKIE['list_type']) {
            $this->view->list_type = $list_type = $_COOKIE['list_type'];

            if (!in_array($list_type, $list_types)) {
                $this->view->list_type = $list_types[0];
            }
        } else {
            $this->view->list_type = $list_types[0];
        }

        $filter_params = array(
            'order' => 'e.date_registered DESC',
            'limit' => array('page' => 1),
            'filter' => array()
        );

        /*$filter_map = array(
            'verified' => 'e.verified_status = 2',
            'french' => 'e.nationality_id = 15',
            'age' => 'e.age < ? AND e.age > ?',
            'ethnic' => 'e.ethnicity = ?',
            'height' => 'e.height < ? AND e.height > ?',
            'weight' => 'e.weight < ? AND e.weight > ?',
            'cup-size' => 'e.cup_size = ?',
            'hair-color' => 'e.hair_color = ?',
            'eye-color' => 'e.eye_color = ?',
            'dress-size' => 'e.dress_size = ?',
            'shoe-size' => 'e.shoe_size = ?',
            'available-for-incall' => 'e.incall_type IS NOT NULL',
            'available-for-outcall' => 'e.outcall_type IS NOT NULL',
            'service-for' => 'FIND_IN_SET(?, e.sex_availability)',
            'smoker' => 'e.is_smoking = ?',
            'language' => 'FIND_IN_SET(?, e.languages)',
            'now-open' => 'e.is_now_open',
            'region' => 'r.slug = ?',
            'city' => 'ct.slug = ?',
            'fcity' => 'fct.slug = ?',
            'cityzone' => 'c.id = ?',
            'zone' => 'cz.slug = ?',
            'name' => 'e.showname LIKE ?',
            'new_arrivals' => 'e.gender = ' . GENDER_FEMALE,
            'independantes' => 'eic.is_agency = 0 AND eic.gender = ' . GENDER_FEMALE,
            'agence' => 'eic.is_agency = 1 AND eic.gender = ' . GENDER_FEMALE,
            'boys' => 'eic.gender = ' . GENDER_MALE,
            'trans' => 'eic.gender = ' . GENDER_TRANS,
            'girls' => 'eic.gender = ' . GENDER_FEMALE,
            'tours' => 'eic.is_tour = 1',
            'upcomingtours' => 'eic.is_upcoming = 1'
        );
		
        foreach ($params['filter'] as $i => $filter) {

            if (!isset($filter_map[$filter['field']]))
                continue;

            $value = $filter['value'];

            if (isset($filter['main'])) {
                if (isset($selected_filter->internal_value)) {
                    $value = $selected_filter->internal_value;
                } elseif (!is_null($item = $menus['filter']->getByValue($filter['field'] . ((isset($value[0]) && $value[0]) ? '_' . $value[0] : '')))) {
                    $value = $item->internal_value;
                }
            }

            $filter_params['filter'][$filter_map[$filter['field']]] = $value;
        }*/

        $page = intval($params['page']);


        if ($page == 0) {
            $page = 1;
        }

        $filter_params['limit']['page'] = $page;
        $count = 0;

        if (isset($params['city_id'])) {
            $this->view->city = Model_Countries::getCityById($params['city_id']);
            $m_c = new Model_Cities();
            $this->view->city_data = $m_c->getById($params['city_id']);
            $this->view->paging_city_slug = $params['city'];
            $filter_params['filter']['r.slug = ?'] = $this->view->city_data->region_slug;
            $filter_params['filter']['ct.id = ?'] = $this->view->city_data->id;
        }

        if (isset($params['fcity'])) {
            $f_city = Model_Cities::getFakeCityBySlug($params['fcity']);
            $this->view->city = $f_city->title;
        }

        $this->view->is_main_page = $is_main_page = (!isset($params['city_id']) && !isset($params['city']) && !isset($params['fcity']) && !isset($params['zone']) && !isset($params['region']));

        $s_config = Zend_Registry::get('system_config');

        $this->view->withVideo = false;
        if ($this->_request->videos )
        {
			$this->view->layout()->disableLayout();
            if ($this->_request->videos == 'on')
            {
                $filter_params['filter']['e.has_video = ?'] = 1;
            }

            $this->view->withVideo = true;
        }

        if ($is_main_page) {

            $regions = new Cubix_Geography_Regions();
            $regions = $regions->getRegionsByCountryId(Cubix_Application::getById()->country_id, $this->_request->region);

            $this->view->regions = $regions;
            //$gender = GENDER_FEMALE;
            $gender = null;
            $is_agency = null;

            $this->view->total_count = Model_Statistics::getTotalCount($gender, $is_agency, null, false);


            $region_slug = $this->_request->region;

            $region = new Cubix_Geography_Regions();
            $region = $region->getRegionBySlug($region_slug);

            $where = null;

            if ($this->_getParam('q')) {
                $query = ltrim($this->_getParam('q'), '/');
                $translit_query = Model_Statistics::translit($query);
                $where = array(
                    'fct.slug LIKE ? OR fct.title_' . $lng . ' LIKE ?' => array('%' . $translit_query . '%', '%' . $query . '%')
                );

                $fake_cities = Model_Statistics::getFakeCitiesForMobile($region->id, $gender, $is_agency, null, false, $where, null);

                if (count($fake_cities) == 1) {
                    $link = $this->view->getLink('escorts', array('city' => $fake_cities[0]->city_url_slug, 'city_id' => $fake_cities[0]->city_id, 'filter' => null, 'page' => null));
                    $this->_redirect($link);
                } else {
                    $all_cities = $fake_cities;
                }

            } else {
                $gender = GENDER_FEMALE;
                $all_cities = Model_Statistics::getCities($region->id, $gender, $is_agency, null, false, $where, null);
            }
            $this->view->all_cities = $all_cities;
            $this->view->region_slug = $region_slug;

            $this->_helper->viewRenderer->setScriptAction("main-page");
            // $this->_helper->viewRenderer->setScriptAction("escort-list");

        } else {
			
			$filters_str = '';
		
			foreach ( $filter_params['filter'] as $k => $filter ) {
				if ( ! is_array($filter) ) {
					$filters_str .= $k . '_' . $filter;
				}
				else {
					$filters_str .= $k;
					foreach($filter as $f) {
						$filters_str .= '_' . $f;
					}
				}
			}
			$geoData = Cubix_Geoip::getClientLocation();
			$cache_key = 'mobile_listing_' . Cubix_I18n::getLang() . '_' . $geoData['country_iso'] .'_'. $filter_params['order'] . '_' . $filter_params['limit']['page'] . '_' . $filters_str;
			$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
			
			if ( ! ($escorts = $cache->load($cache_key)) || ! ($count = $cache->load($cache_key . '_count')) ) {
				
				$escorts = Model_Escort_List::getFiltered($filter_params['filter'], $params['sort'], $filter_params['limit']['page'], /*$config['escorts']['perPage']*/
                500, $count, true, $this->view->city_data->region_slug, true);
				
				$cache->save($escorts, $cache_key, array());
				$cache->save($count, $cache_key . '_count', array());
			}
            
            $this->view->params = $params;

            $this->view->escorts = $escorts;

            if (count($escorts) > 0) {
                $sess_name = "prev_next";
                $sid = 'sedcard_paging_' . Cubix_Application::getId();
                $sess = new Zend_Session_Namespace($sid);

                $sess->{$sess_name} = array();
                foreach ($escorts as $escort) {
                    $sess->{$sess_name}[] = $escort->id;
                }
            }

            $this->view->page = $page;
            $this->view->count = $count;
            $this->view->is_map_list = false;

            if (!isset($this->_request->map))
                $this->_helper->viewRenderer->setScriptAction("escort-list");
            else {
                $this->_helper->viewRenderer->setScriptAction("escort-list-map");
                $arr = array();
                $ids = array();

                foreach ($escorts as $e) {
                    if ($e->latitude && $e->longitude) {
                        $e_item = new Model_Escort_PhotoItem($e);

                        $arr[$e->id] = array(
                            'escort_id' => $e->id,
                            'latitude' => $e->latitude,
                            'longitude' => $e->longitude,
                            'showname' => $e->showname,
                            'url' => $this->view->getLink('profile', array('showname' => $e->showname, 'escort_id' => $e->id)),
                            'photo' => $e_item->getUrl('w68')
                        );

                        $ids[] = $e->id;
                    }
                }

                if (count($ids) > 0) {
                    $ids_str = implode(',', $ids);

                    $m = new Model_EscortsV2();
                    $profiles = $m->getProfiles($ids_str);

                    foreach ($profiles as $p) {
                        $data = (array)unserialize($p->data);

                        $arr[$p->escort_id]['city_title'] = $data['city_title_en'] ? $data['city_title_en'] : '';
                        $arr[$p->escort_id]['zip'] = $data['zip'] ? $data['zip'] : '';
                        $arr[$p->escort_id]['address'] = $data['street'] ? $data['street'] . ' ' . $data['street_no'] : '';
                    }
                }


                $this->view->escorts = array_values($arr);
                $this->view->is_map_list = true;
            }
        }

        if ( $this->_getParam('req') == 'happy-hours' ){
            $this->_helper->viewRenderer->setScriptAction("escort-list");
            $this->view->is_happyhours = 1;

            $req = $this->_getParam('req');
            $param = explode('/', $req);
            $page = explode("_", ( isset($param[2]) ) ? $param[2] : '' );

            $region_slug = $this->_request->region;
            $filter = array('e.gender = ?' => 1, 'e.hh_is_active = ?' => 1, 'r.slug = ?' => $region_slug);

            $page_num = 1;
            if ( isset($page[1]) && $page[1] ) {
                $page_num = $page[1];
            }

            $count = 0;
            $escorts = Model_Escort_List::getFiltered($filter, 'random', $page_num, 10, $count);

            $this->view->count = $count;
            $this->view->escorts = $escorts;

            $this->view->hh_page = true;
        }

		$sess_name = 'search_list';
		$region_s = $region_slug;
		if ( $sess_name ) {
	
			$sid = 'sedcard_paging_' . Cubix_Application::getId() . '_' . $this->_request->region;
	
			$sess = new Zend_Session_Namespace($sid);
	
			$sess->{$sess_name} = array();
			foreach ( $this->view->escorts as $escort ) {
				$sess->{$sess_name}[] = $escort->id;
			}
			//var_dump($sess->{$sess_name});
		}

        $per_page = 100;
        $model_cities = new Model_Cities();

        $cache_key = 'mobile_widget_bubbles_messages_non_ajax_page_' . $page . '_' . ($city->id?$city->id:'');
        if ( ! $bubbles = $cache->load($cache_key) ) {
            try {
                $client = Cubix_Api_XmlRpc_Client::getInstance();

                if ( !$city->id && isset($req->lat) && isset($req->long) ){
                    $bubbles = $client->call('Escorts.getBubbleTextsMobile', array( $page, $per_page, null, null, array( 'long' => $req->long, 'lot' => $req->lat ) ) );

                } else {
                    $bubbles = $client->call('Escorts.getBubbleTextsMobile', array( $page, $per_page, $city->id ) );
                }
                // echo $bubbles; die;
            }
            catch ( Exception $e ) {
                $bubbles = array('texts' => array(), 'count' => 0);
            }

            foreach ( $bubbles['texts'] as &$text ) {
                $text['city_title'] = $model_cities::getTitleById( $text['city_id'] )->title;
                $text = new Model_EscortV2Item($text);
            }

            $cache->save($bubbles, $cache_key, array(), 300);
        }

        $this->view->bubbles = $bubbles;
	
	}

	public function clubDirectoryAction()
	{
		$this->_helper->layout->setLayout('mobile-cd');
		$this->view->is_map = false;

		$lng = Cubix_I18n::getLang();

		$conf = Zend_Registry::get('agencies_config');
		$this->view->perPage = $perPage = 30;
		$this->view->page = $page = intval($this->_request->getParam('page', 1));

		// Scroll Spy
		if ( $this->_request->ajax ) {
			$this->view->spy = true;
			$this->view->layout()->disableLayout();
			$this->view->first = $this->_request->getParam('first', false);
			$this->view->lat = $lat = $this->_request->getParam('lat', 0);
			$this->view->lon = $lon = $this->_request->getParam('lon', 0);
		}

		$m = new Model_ClubDirectory();
		$coord = array();

		if ( $lat && $lon ) {
			$coord = array('lat' => $lat, 'lon' => $lon);
		}
		$search = null;
		$filter = array('distance', 'working_girl');
		$escorts = $m->getList($filter, $search, $coord, $page, $perPage, $count, $area, $city);
		$this->view->escorts = $escorts['distance'];
		$this->view->count = $count;

		$this->_helper->viewRenderer->setScriptAction("club-directory");
	}

	public function clubDirectoryMapAction()
	{
		$this->_helper->layout->setLayout('mobile-cd');
		$this->view->is_map = true;

		if ( $this->_request->ajax ) {
			$this->view->layout()->disableLayout();
			$m = new Model_ClubDirectory();

			$filter = array();
			$search = null;

			$filter = $this->_request->getParam('filter', array('working_girl', 1, 3, 2, 4));

			$area = $this->_request->area;
			$city = $this->_request->city;

			$this->view->all_cities = 1;

			$map_list = $m->getMapList($filter, $search, $area, $city);

			if ( count($map_list) ) {
				foreach ( $map_list as $k => $item ) {
					if ( $item->escort_id ) {
						$map_list[$k]->url = $this->view->getLink('profile', array('showname' => $item->showname, 'escort_id' => $item->escort_id));
						$e_item = new Model_Escort_PhotoItem($item);
						$map_list[$k]->photo = $e_item->getUrl('w68');
					} else {
						$map_list[$k]->url = $this->view->getLink('agency', array('slug' => $item->club_slug, 'id' => $item->agency_id));
					}
				}
			}

			echo json_encode(array('data' => $map_list));
			die;
		}


		$this->_helper->viewRenderer->setScriptAction("club-directory-map");
	}

	public function reviewsAction() 
	{
		$lng = Cubix_I18n::getLang();



		$request = $this->_request;
		if ($request->ajax) {
			$this->view->layout()->disableLayout();
			$this->view->ajax = true;
		}
		$filter = array();

		if (isset($request->city_id) && $request->city_id) {
			$filter['city_id'] = intval($request->city_id);
			$this->view->city_id = intval($request->city_id);
		}

		if (isset($request->showname) && $request->showname) {
			$filter['showname'] = $request->showname;
			$this->view->showname = $request->showname;
		}

		if (isset($request->member_name) && $request->member_name) {
			$filter['member_name'] = $request->member_name;
			$this->view->member_name = $request->member_name;
		}

		$ord_field_v = 'creation_date';
		$ord_field = 'r.creation_date';
		$ord_dir_v = 'desc';
		$ord_dir = 'DESC';

		if (count($filter) > 0)
			$arg_filter = $filter;
		else
			$arg_filter = '-999';
		$config = Zend_Registry::get('reviews_config');
		if (isset($request->page) && intval($request->page) > 0) {
			$page = intval($request->page);
		}
		else
			$page = 1;

		is_array($arg_filter) ? $filter_str = implode('.', $arg_filter) : $filter_str = $arg_filter;

		$ret_revs = Model_Reviews::getReviews($page, $config['perPage'], $arg_filter, $ord_field, $ord_dir);
		$cities = Model_Reviews::getReviewsCities();


		$ret = array($ret_revs, $cities);

		list($ret_revs, $cities) = $ret;
		list($items, $count) = $ret_revs;
		$this->view->items = $items;
		$this->view->count = $count;
		$this->view->cities = $cities;
		$this->view->page = $page;
		$this->view->filter = $filter;

		$this->view->menuReviews = 1;
	}

	public function newListAction()
	{
		//		$this->view->layout()->setLayout('main-simple');

		$per_page = 3;

		$this->view->per_page = $per_page;

		$model = new Model_EscortsV2();

		$req = $this->_getParam('req');
		$param = explode('/', $req);
		$page = explode("_", ( isset($param[3]) ) ? $param[3] : '' );

		$region_slug_parts = ( isset($param[1]) ) ? explode('_' ,$param[1]) : array();
		$region_slug = "deutschschweiz";
		if ( isset( $region_slug_parts[1] ) ){
			$region_slug = $region_slug_parts[1];
		}


		//		$region_slug = $this->_request->region;
		$filter = array('e.gender = ?' => GENDER_FEMALE, 'r.slug = ?' => $region_slug);




		$page_num = 1;
		if ( isset($page[0]) && $page[0] ) {
			$page_num = $page[0];
		}
		$this->view->page = $page_num;
		$count = 0;
		//$escorts = Model_Escort_List::getFiltered($filter, 'newest', $page_num, 10, $count, 'new_list');

		$cache = Zend_Registry::get('cache');
		$new_cache_key = 'm_v2_new_escorts_list_' . Cubix_Application::getId() . '_lang_' . Cubix_I18n::getLang() . '_page_' . $page_num . $region_slug;
		$new_c_cache_key = 'm_v2_new_escorts_list_count_' . Cubix_Application::getId() . '_lang_' . Cubix_I18n::getLang() . '_page_' . $page_num . $region_slug;

		$count = $cache->load($new_c_cache_key);
		if ( ! $escorts = $cache->load($new_cache_key) ) {
			$escorts = Model_Escort_List::getFiltered($filter, 'newest', $page_num, $per_page, $count, 'new_list', $region_slug, true);

			if ( count($escorts) ) {
				foreach ( $escorts as $k => $escort ) {
					$cache_key = 'v2_' . $escort->showname . '_new_profile_' . Cubix_I18n::getLang() . '_page_' . $page_num . $region_slug;
					$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
					$escorts[$k]->profile = $model->get($escort->id, $cache_key);
				}
			}

			$cache->save($escorts, $new_cache_key, array());

			$cache->save($count, $new_c_cache_key, array());
		}

		$sess_name = 'new_list';
		$region_s = $region_slug;
		if ( $sess_name ) {

			$sid = 'sedcard_paging_' . Cubix_Application::getId() . '_' . $region_s;

			$sess = new Zend_Session_Namespace($sid);

			$sess->{$sess_name} = array();
			foreach ( $escorts as $escort ) {
				$sess->{$sess_name}[] = $escort->id;
			}
			//var_dump($sess->{$sess_name});
		}

		$this->view->count = $count;
		$this->view->escorts = $escorts;

		$this->view->menuNew = 1;

		$this->view->new_list_cache_key = 'new_list_' . $cache_key;
	}

	public function searchAction()
	{
		$this->_helper->layout->setLayout('mobile-index');
		$referer = str_replace( 'c', '', end( explode( '-',end( explode( '/', $_SERVER['HTTP_REFERER'])))));
		$this->view->referer = $referer;

		$this->view->defines = Zend_Registry::get('defines');

		$lng = Cubix_I18n::getLang();
		if( $this->_request->lang_id ) $lng = $this->_request->lang_id;

		//-------------------------------------------------------------
		$f_cities = Model_Mobile_FakeCities::getAll();
		$this->view->f_cities = $f_cities;
		//-------------------------------------------------------------

		$regions = new Cubix_Geography_Regions();
		$regions = $regions->getRegionsByCountryId(Cubix_Application::getById()->country_id, $this->_request->region);

		$this->view->regions = $regions;
		$gender = null;
		$is_agency = null;		
		$this->view->total_count = Model_Statistics::getTotalCount($gender, $is_agency, null, false);
        if($this->_getParam('romandie') == 'true'){
            $region_slug = 'romandie';
            $region_id = 2;
        }else{
        	  $region_slug = $this->_request->region;
              $region_id = $region->id;
        }

		$where = null;

		if ($this->_getParam('q')) {
			$query = ltrim($this->_getParam('q'), '/');
			$translit_query = Model_Statistics::translit($query);
			$where = array(
				'fct.slug LIKE ? OR fct.title_' . $lng . ' LIKE ?' => array('%' . $translit_query . '%', '%' . $query . '%')
			);
			$fake_cities = Model_Statistics::getFakeCitiesForMobile($region->id, $gender, $is_agency, null, false, $where, null);

			if (count($fake_cities) == 1) {
				$link = $this->view->getLink('escorts', array('fcity' => $fake_cities[0]->fake_city_slug, 'filter' => null, 'page' => null));
				$this->_redirect($link);
			} else {
				$all_cities = $fake_cities;
			}

		} else {
			$gender = GENDER_FEMALE;
			$all_cities = Model_Statistics::getCities($region_id, $gender, $is_agency, null, false, $where, null);
		}

		$config = Zend_Registry::get('mobile_config');
		$all_zones = Model_Statistics::getZones();

		$this->view->menuSearch = 1;
		$this->view->cities = $all_cities;
		$this->view->zones = $all_zones;

		if( $this->_getParam('search') )
		{
			$params['filter'] = array();
            $params['order'] = array();
			
			$this->view->params = $this->_request->getParams();

            if( $this->_getParam('order') ) {

                $order_direction = '';
                $order_field = 'date_registered';

                switch($this->_getParam('order')){
                    case 'lastmodified':
                       $order_field = "date_last_modified";
                       break;
                    case 'popularity':
                       $order_field = "hit_count";
                       break;
                }

                if($this->_getParam('order_direction')){
                   $order_direction = $this->_getParam('order_direction');
                }
                $params['order'] = $order_field." ".$order_direction;
				
			}

            if($this->_getParam('page')){
                $params['limit']['page'] = $this->_getParam('page');
            }

            if( $this->_getParam('publish') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'DATEDIFF(NOW(),e.date_registered) <= ?' =>  trim($this->_getParam('days'))
				));
			}

			if( $this->_getParam('showname') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.showname LIKE ?' => "%" . trim($this->_getParam('showname')) . "%"
				));
			}
			
			if( $this->_getParam('agency_slug') && $this->_getParam('agency') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.agency_slug LIKE ?' => "%" . trim($this->_getParam('agency_slug')) . "%"
				));
			}
			
			if( $this->_getParam('gender') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.gender = ?' => $this->_getParam('gender')
				));
			}

            if( $this->_getParam('virtual') ) {
                $params['filter'] = array_merge($params['filter'], array(
					'e.cam_status <> ""',
                    //'e.cam_booking_status = ?' => $this->_getParam('virtual')
                ));
            }


			if( $this->_getParam('online_now') ) {
				$params['filter'] = array_merge($params['filter'], array(
					//'@is_login IS NOT NULL' => true
					'TIMESTAMPDIFF(MINUTE, ulrt.refresh_date, NOW()) < ?' => 10
				));
				//print_r( $params['filter'] ); die;
			}

            if( $this->_getParam('vr') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.verified_status IN ('.Model_Escorts::VERIFIED_STATUS_VERIFIED .','. Model_Escorts::VERIFIED_STATUS_VERIFIED_RESET.')' => array()
                ));
            }
            if($this->_getParam('romandie') == 'true'){
                $params['filter'] = array_merge($params['filter'], array(
                    'r.slug = ?' => 'romandie'
                ));
                $this->view->city = $this->_getParam('romandie');
            }
			if( $this->_getParam('area')) {
				$params['filter'] = array_merge($params['filter'], array(
					'ct.id = ?' => $this->_getParam('area')
				));
			}

			if( $this->_getParam('city') ){
				$params['filter'] = array_merge($params['filter'], array(
					'fct.slug = ?' => $this->_getParam('city')
				));
				$this->view->city = $this->_getParam('city');
			}


			if( $this->_getParam('available_for_incall') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.incall_type IS NOT NULL' => true
				));
			}

			if( $this->_getParam('available_for_outcall') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.outcall_type IS NOT NULL' => true
				));
			}
			
			if( $this->_getParam('nationality') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.nationality_id = ?' => $this->_getParam('nationality')
				));
			}
			
			if( $this->_getParam('saf') ) {
				$params['filter'] = array_merge($params['filter'], array(					
					'FIND_IN_SET( ?, e.sex_availability)' => $this->_getParam('saf')
				));
			}

            if( $this->_getParam('price_from') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.outcall_price >= ?' => $this->_getParam('price_from')
				));
			}

            if( $this->_getParam('price_to') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.outcall_price <= ?' => $this->_getParam('price_to')
				));
			}

			if( $this->_getParam('words') ) 
			{
				if ($this->_getParam('words_type') == 1)
				{
	            	if ($pos = strpos($this->_getParam('words'), ' ')) {
	            		$sub = substr($this->_getParam('words'), 0, $pos);
	            	}
	            	else {
	            		$sub = $this->_getParam('words');
	            	}
	            	
	            	$params['filter'] = array_merge($params['filter'], array(
						'e.' . Cubix_I18n::getTblField('about') . ' = ?' =>  $sub 
					));
	            }
	            elseif ($this->_getParam('words_type') == 2) 
	            {
	            	$params['filter'] = array_merge($params['filter'], array(
						'e.' . Cubix_I18n::getTblField('about') . ' LIKE ?' => "%" . $this->_getParam('words') . "%"
					));
	            }
			}
			
			/* ------------ >>>>>>>>>>>>>>>>>>>>>>>>>>>>>> <<<<<<<<<<<<<<<<<<<<<< --------------- */
			
			if( $this->_getParam('age_from') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.age >= ?' => $this->_getParam('age_from')
				));
			}			
			if( $this->_getParam('age_to') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.age <= ?' => $this->_getParam('age_to')
				));
			}

            if( $this->_getParam('ethnicity') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.ethnicity = ?' => $this->_getParam('ethnicity')
				));
			}

            if( $this->_getParam('hair_color') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.hair_color = ?' => $this->_getParam('hair_color')
				));
			}	

			if( $this->_getParam('eye_color') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.eye_color = ?' => $this->_getParam('eye_color')
				));
			}

            if( $this->_getParam('height_from') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.height >= ?' => $this->_getParam('height_from')
				));
			}
			if( $this->_getParam('height_to') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.height <= ?' => $this->_getParam('height_to')
				));
			}	
		
			if( $this->_getParam('dress_size') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.dress_size = ?' => $this->_getParam('dress_size')
				));
			}

            if( $this->_getParam('shoe_size') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.shoe_size = ?' => $this->_getParam('shoe_size')
				));
			}	
								
			if( $this->_getParam('cup_size') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.cup_size = ?' => $this->_getParam('cup_size')
				));
			}
            
            if( $this->_getParam('pubic_hair') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.pubic_hair = ?' => $this->_getParam('pubic_hair')
				));
			}

            if( $this->_getParam('smoker') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.is_smoker = ?' => $this->_getParam('smoker')
				));
			}

			if( $this->_getParam('drinking') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.drinking = ?' => $this->_getParam('drinking')
				));
			}			
			if( $this->_getParam('language') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.languages LIKE ?' => "%".$this->_getParam('language')."%"
				));
			}			
            /* Services */
            if( $this->_getParam('services') ) {

                $serviceKey = $this->_getParam('services');
                if ($serviceKey == 'massage') {
                    $this->view->service = 'massage';
                    $serviceFilter = array(
                        Model_Escort_List::getSqlEquivalent('massage-only') => 1
                    );
                } else if ($serviceKey == 'companion') {
                    $this->view->service = 'companion';
                    $serviceFilter = array(
                        Model_Escort_List::getSqlEquivalent('outcall-only') => 1
                    );
                }
                else{
                    $serviceFilter = array(
                        'es.service_id = ?' => $this->_getParam('services')
                    );
                }

				$params['filter'] = array_merge($params['filter'], $serviceFilter);
			}


			/* Working Times */
            
            if( $this->_getParam('day_index') ) {
                $dateTimes = array();
                $counter = 0;
                foreach($this->_getParam('day_index') as $day => $value){
                    $timeFrom = $this->_getParam('time_from');
                    $timeFromM = $this->_getParam('time_from_m');
                    $timeTo = $this->_getParam('time_to');
                    $timeToM = $this->_getParam('time_to_m');
                    $dateTimes[$counter]['day_index'] = $day;
                    $dateTimes[$counter]['time_from'] = $timeFrom[$day];
                    $dateTimes[$counter]['time_from_m'] = $timeFromM[$day];
                    $dateTimes[$counter]['time_to'] = $timeTo[$day];
                    $dateTimes[$counter]['time_to_m'] = $timeToM[$day];
                    $counter++;
                }
                $params['filter'] = array_merge($params['filter'], array(
					'working_times = ?' => $dateTimes
				));
			}

            /* Working Times */

			$page = 1;
			$page_size = $config['escorts']['perPage'];
			if ($this->_getParam('page')) {
				$page = $this->_getParam('page');
			}

			$model = new Model_EscortsV2();
			$count = 0;

			$escorts = $model->getSearchAll($params, $count, $page, $page_size);

			if (count($escorts['data']) > 0) {
				// <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
				$sess_name = "prev_next";
				$sid = 'sedcard_paging_' . Cubix_Application::getId();
				$sess = new Zend_Session_Namespace($sid);

				$sess->{$sess_name} = array();
				foreach ($escorts['data'] as $escort) {
					$sess->{$sess_name}[] = $escort->showname;
				}
				// </editor-fold>
			}

			$sess_name = 'search_list';
			$region_s = $region_slug;
			if ( $sess_name ) {
	
				$sid = 'sedcard_paging_' . Cubix_Application::getId() . '_' . $region_s;
	
				$sess = new Zend_Session_Namespace($sid);
	
				$sess->{$sess_name} = array();
				foreach ( $escorts['data'] as $escort ) {
					$sess->{$sess_name}[] = $escort->id;
				}
				//var_dump($sess->{$sess_name});
			}

			$this->view->page = $page;
			$this->view->count = $escorts['count'];
			$this->view->escorts = $escorts['data'];
            $this->view->search_list = true;
			$this->view->virtual = $this->_getParam('virtual');

			$this->_helper->viewRenderer->setScriptAction("escort-list");

		}

	}

	public function videoVoteAction()
	{
		if (!$this->_request->isPost()) die;

		$this->view->layout()->disableLayout();

		$video_id = $this->_request->video_id;
		$fingerprint = $this->_request->fingerprint;

		$model_video = new Model_Video();
		$client = new Cubix_Api_XmlRpc_Client();

		$pre_video_id = $model_video->voteForVideo($video_id, $fingerprint);
		$result = $client->call('Escorts.voteForVideo', array($video_id, $pre_video_id));

		if ($result) {
			die(json_encode(array('status' => 'success')));
		} else {
			die(json_encode(array('status' => 'error')));
		}
	}

	public function videoVoteCountAction()
	{
		$this->view->layout()->disableLayout();

		$fingerprints = Model_Video::getVideoVoteCount($this->_request->video_id);
		$fp_arr = (array) $fingerprints;
		$fps = array_map(function($fp) {
			return $fp->fingerprint;
		}, $fp_arr);

		$vote_count = count($fps);

		if (isset($vote_count)) {
			die(json_encode(array('status' => 'success', 'length' => $vote_count, 'fingerprints' => $fps)));
		} else {
			die(json_encode(array('status' => 'error')));
		}
	}
		
}
