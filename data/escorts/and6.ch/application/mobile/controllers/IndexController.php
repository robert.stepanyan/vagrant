<?php

class Mobile_IndexController extends Zend_Controller_Action
{
	public function init()
	{
		$this->_helper->layout->setLayout('mobile-index');
        if(!$this->_getParam('escort_id')){
            $_SESSION['request_url'] = $_SERVER['REQUEST_URI'];
        }

		$this->user = Model_Users::getCurrent();
		$this->view->static_page_key = 'main';
        	//die('Mobile version is temporary unavailable');http://m.6annonce.com/remove-from-favorites?=11490
	}

    public function indexAction()
	{
		$this->_forward('index', 'escorts');
		return;
		$url .= 'escorts/region_';


		if ( $this->_getParam('region_slug') ) {
			$url .= $this->_getParam('region_slug');
		}else{
			$url .= 'deutschschweiz';
		}

		if($this->_getParam('q')){
            $url .= "?q=".$this->_getParam('q');
        }

		header("HTTP/1.1 301 Moved Permanently");
		header('Location: ' . $url);
		exit;



        //$gender = GENDER_FEMALE;
        $gender = null;
        $is_agency = null;

        $query = '';
        $where = null;
        if($this->_getParam('q')){
            $query = ltrim($this->_getParam('q'), '/');
            $where = array('ct.slug LIKE ?' => '%'.$query.'%');
        }

        $all_cities = Model_Statistics::getCities(null, $gender, $is_agency, null, false,$where);

        if(!$this->_getParam('all')){
            $this->view->cities = array_slice($all_cities, 0, 24);
        }else{
            $this->view->cities = $all_cities;
            $this->view->is_all = true;
        }
        $this->view->query = $query;

        $this->view->menuHome = 1;
        $this->view->total_count = Model_Statistics::getTotalCount($gender, $is_agency, null, false);
	}

    public function jsInitExactAction()
    {
        $this->view->layout()->disableLayout();
        $req = $this->_request;

        $view = $this->view->act = $req->view;

        $lang = $this->view->lang = $req->lang;
        $region = $this->view->region = $req->region;
        $region_slug = $this->view->region_slug = $req->region_slug;

        if ( $view == 'new-list' ) {

        }
        else if ( $view == 'list' ) {
            $this->view->escorts_url = $req->escorts_url;
            $this->view->filter_params = base64_decode($req->filter_params);
        }
        else if ( $view == 'profile' ) {

        }
        else if ( $view == 'profile-v2' ) {

        }
        else if ( $view == 'google-maps' ) {
            $this->view->address = $req->address;
        }
        else if ( $view == 'google-maps-v2' ) {
            $this->view->address = $req->address;
        }
        else if ( $view == 'google-maps-v2-agency' ) {
            $this->view->address = $req->address;
        }
        else if ( $view == 'side-bar' ) {
            //$this->view->per_page = $req->per_page;
            $this->view->per_page = 10;
        }
		else if ( $view == 'cams' ) {
			$this->view->is_logged_in = $req->is_logged_in;
			$this->view->is_member = $req->is_member;
			$this->view->is_escort = $req->is_escort;
		}
		
        header('Content-type: text/javascript');
        $expires = 60*60*24*14;
        header("Pragma: public");
        header("Cache-Control: maxage=" . $expires);
        header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expires) . ' GMT');

    }

    private function _orderTitles($a,$b){
        if ( is_object($a) && is_object($b) ) {
			return strnatcmp($a->city_title, $b->city_title);
		}
		elseif ( is_array($a) && is_array($b) ) {
			return strnatcmp($a["city_title"], $b["city_title"]);
		}
		else return 0;
    }



    public function favoritesAction()
	{

        $this->view->menuFavorites = 1;

		/*********************/

		$page = $this->view->page = $this->_request->page ? intval($this->_request->page) : 1;

		if (in_array($this->_request->per_page, array(12, 24, 48, 96, 1000)))
			$per_page = $this->_request->per_page;
		else
			$per_page = 12;

		$this->view->per_page = $per_page;

		$filter = array();

		if (strlen($this->_request->showname) > 0 ) {
			$showname = preg_replace('/[^-_a-z0-9\s]/i', '', trim($this->_request->showname));
			$filter['showname'] = $showname;
		}

		if (in_array($this->_request->act, array(1, 2, 3))) {
			$this->view->act = $filter['act'] = $this->_request->act;
		} else {
			$this->view->act = $filter['act'] = 1;
		}

		if (intval($this->_request->city))
		{
			$filter['city'] = intval($this->_request->city);
		}

		$model = new Model_Members();

		$ret = $model->getFavorites($this->user->id, $filter, $page, $per_page);

		$this->view->escorts = $ret['data'];
		$this->view->count = $ret['count'];

		$this->_helper->viewRenderer->setScriptAction("favorites-list");

	}



    public function escortsAction()
	{

	}

	public function gpsLocationAction()
	{
		$req = $this->_request;

		if (isset($req->lat) && isset($req->long))
		{
			 $config = Zend_Registry::get('mobile_config');
             $limit = $config['escorts']['perPage'];
			 $page = $req->page;
             $page = $this->view->page = isset($page) && is_numeric($page) ? intval($page) : 1;
			 $page = ($page - 1) * $limit;

			 $model = new Model_EscortsV2();

			 $this->view->lat = floatval($req->lat);
			 $this->view->long = floatval($req->long);
			 $escorts = $model->getEscortsByLocation($req->lat, $req->long, $limit, $page);
			 $this->view->count = $escorts['count'];
			 $this->view->escorts = $escorts['data'];
		}
		else
		{
			$this->view->error = 1;
		}
	}

    public function addToFavoritesAction(){
		$req = $this->_request;
        $escort_id = intval($req->escort_id);
        $cook_name = "favorites";
        $cook_value = array($escort_id);
        if ( ! isset( $_COOKIE[$cook_name]) ) {
            setcookie($cook_name, serialize($cook_value), strtotime('+1 year'), "/", '.' . Cubix_Application::getById(Cubix_Application::getId())->host);
        }
        else {
            $cook_data = unserialize($_COOKIE[$cook_name]);
            if ( is_array($cook_data) ) {
                if ( in_array($cook_value[0], $cook_data) ) {
                    $this->view->alredy_in_favorites = true;
                }
                else {
                    $new_data = array_merge($cook_data, $cook_value);
                    setcookie($cook_name, serialize($new_data), strtotime('+1 year'), "/", '.' . Cubix_Application::getById(Cubix_Application::getId())->host);
                }
            }
        }

        $this->_redirect($_SERVER['HTTP_REFERER']);
    }


    public function removeFromFavoritesAction(){
		$req = $this->_request;
        $escort_id = intval($req->escort_id);
        $cook_name = "favorites";
        $cook_value = array($escort_id);
        if ( isset( $_COOKIE[$cook_name]) ) {
            $cook_data = unserialize($_COOKIE[$cook_name]);
            if ( is_array($cook_data) ) {
                if ( in_array($cook_value[0], $cook_data) ) {
                    $new_data = array_diff($cook_data, $cook_value);
                    setcookie($cook_name, serialize($new_data), strtotime('+1 year'), "/", '.' . Cubix_Application::getById(Cubix_Application::getId())->host);
                }
            }
        }

        $this->_redirect($_SERVER['HTTP_REFERER']);
    }


    public function feedbackAction()
	{
		$this->_request->setParam('no_tidy', true);

		$menu = array(
            'bio' => array(
                'name' => 'bio',
                'class' => 'w15',
            ),
            'services' => array(
                'name' => 'services',
                'class' => 'w23',
            ),
            'rates' => array(
                'name' => 'rates',
                'class' => 'w18',
            ),
            'contacts' => array(
                'name' => 'contact',
                'class' => 'w24',
            ),
            'photos' => array(
                'name' => 'photos',
                'class' => 'w20',
            )
        );

		$this->view->menu = $menu;


		$model = new Model_EscortsV2();

        $this->_helper->layout->setLayout('mobile');

		// Fetch administrative emails from config
		$config = Zend_Registry::get('feedback_config');
		$site_emails = $config['emails'];


		// Get data from request
		$data = new Cubix_Form_Data($this->getRequest());
		$fields = array(
			'to' => '',
			'name' => 'notags|special',
			'email' => '',
			'message' => 'notags|special'
		);
		$data->setFields($fields);
		$data = $data->getData();

		$this->view->data = $data;

		// If the to field is invalid, that means that user has typed by
		// hand, so it's like a hacking attempt, simple die, without explanation


		if ( ! is_numeric($data['to']) && ! in_array($data['to'], array_keys($site_emails)) ) {
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			die;
			return;
		}

		// If escort id was specified, fetch it's email and showname,
		// construct the result email template and send it to escort
		if ( is_numeric($data['to']) ) {
			$escort = $model->get($data['to']);
			$this->view->escort = $escort;


			// If the escort id is invalid that means that it is hacking attempt
			if ( ! $escort ) {
				header('HTTP/1.1 301 Moved Permanently');
                header('Location: /');
                die;
                return;
			}

			$data['to_addr'] = $escort->email;
			$data['to_name'] = $escort->showname;
			$data['username'] = $escort->username;
			$email_tpl = 'escort_feedback';

            $this->view->showname = $escort->showname;
		}

		// In order when the form was posted validate fields
		if ( $this->_request->isPost() ) {
			$validator = new Cubix_Validator();

			if ( ! strlen($data['email']) ) {
				$validator->setError('email', '' /*'Email is required'*/);
			}
			elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
				$validator->setError('email', '' /*'Wrong email format'*/);
			}

			if ( ! strlen($data['message']) ) {
				$validator->setError('message','' /*'Please type the message you want to send'*/);
			}


			$result = $validator->getStatus();

            if ( ! $validator->isValid() ) {
                $this->view->errors = $result['msgs'];
			}else{


                // Set the template parameters and send it to support or to an escort
                $tpl_data = array(
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'to_addr' => $data['to_addr'],
                    'message' => $data['message'],
                    'escort_showname' => $escort->showname,
                    'escort_id' => $escort->id
                );

                if ( isset($data['username']) ) {
                    $tpl_data['username'] = $data['username'];
                }

                Cubix_Email::sendTemplate($email_tpl, $data['to_addr'], $tpl_data, isset($escort) ? $data['email'] : null);

                $this->_redirect('escort/'.$escort->showname.'?success=true#contacts');

            }
		}
	}

	public function robotsAction()
	{
		$this->_request->setParam('no_tidy', true);

		$this->view->layout()->disableLayout();
		$model = new Model_Robots();

		$robots = $model->getMobile();

		header('Content-Type: text/plain; charset=UTF-8', true);
		echo $robots;
		/*echo trim("User-agent: *
		Dissallow: /page/terms-and-conditions
		Disallow: /favorites
		Disallow: /en
		Disallow: /search");*/
		die;
	}

	public function goAction()
	{
		//A6-21
		die;
        $link = $_SERVER['QUERY_STRING'];

        if ( ! preg_match('#^https?://#', $link) ) $link = 'http://' . $link;

        $link = preg_replace('|\?.+|', '', $link);
        header('Location: ' . $link);
        die;
	}

	public function getCityzonesAction()
	{
		$this->view->layout()->disableLayout();

		$city_slug = $this->_request->city;

		$c_model = new Cubix_Geography_Cities();
		$city = $c_model->getBySlug( $city_slug );

		$cz_model = new Cubix_Geography_Cityzones();

		$all_cityzones = $cz_model->ajaxGetAll($city->id);

		$this->view->zones = $all_cityzones;
//		var_Dump($this->view->zones);
//		exit;
	}


	public function ajaxGetCitiesAction()
	{
		$this->view->layout()->disableLayout();

		$this->_request->setParam('no_tidy', true);

		$country_id = intval($this->_getParam('country_id'));
		$region_id = intval($this->_getParam('region_id'));

		$model = new Cubix_Geography_Cities();

		$this->view->regions = $model->ajaxGetAll($region_id, $country_id);

	}



	public function contactAction()
	{
		$this->_request->setParam('no_tidy', true);

		$this->_request->setParam('to', 'support');

		$model = new Model_EscortsV2();

		// Fetch administrative emails from config
		$config = Zend_Registry::get('feedback_config');
		$site_emails = $config['emails'];


		$is_ajax = ! is_null($this->_getParam('ajax'));

		// If the request is ajax, disable the layout
		if ( $is_ajax ) {
			$this->view->layout()->disableLayout();
		}

		// Get data from request
		$data = new Cubix_Form_Data($this->getRequest());
		$fields = array(
			'to' => '',
			'about' => 'int',
			'name' => 'notags|special',
			'email' => '',
			'message' => 'notags|special',
			'captcha' => ''
		);
		$data->setFields($fields);
		$data = $data->getData();

		$this->view->data = $data;
		$this->view->user = Model_Users::getCurrent();

		// If the to field is invalid, that means that user has typed by
		// hand, so it's like a hacking attempt, simple die, without explanation
		if ( ! is_numeric($data['to']) && ! in_array($data['to'], array_keys($site_emails)) ) {
			die;
		}

		// If escort id was specified, fetch it's email and showname,
		// construct the result email template and send it to escort
		if ( is_numeric($data['to']) ) {
			$escort = $model->get($data['to']);
			$this->view->escort = $escort;
			//var_dump($escort);die;
			// If the escort id is invalid that means that it is hacking attempt
			if ( ! $escort ) {
				die;
			}

			$data['to_addr'] = $escort->email;
			$data['to_name'] = $escort->showname;
			$data['username'] = $escort->username;
			$email_tpl = 'escort_feedback';
		}
		// Else get administrative email from configuration file
		else {
			$data['to_addr'] = $site_emails[$data['to']];
			$email_tpl = 'feedback_template';
		}

		// In order when the form was posted validate fields
		if ( $this->_request->isPost() ) {
			$validator = new Cubix_Validator();

			if ( ! strlen($data['email']) ) {
				$validator->setError('email', 'Email is required');
			}
			elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
				$validator->setError('email', 'Wrong email format');
			}

			if ( ! strlen($data['message']) ) {
				$validator->setError('message', 'Please type the message you want to send');
			}

			if ( ! strlen($data['captcha']) ) {
				$validator->setError('captcha', 'Captcha is required');
			} else {
				$session = new Zend_Session_Namespace('captcha');
				$orig_captcha = $session->captcha;

				if ( strtolower($data['captcha']) != $orig_captcha ) {
					$validator->setError('captcha', 'Captcha is invalid');
				}
			}

			$result = $validator->getStatus();

			if ( ! $validator->isValid() ) {
				// If the request was ajax, send the validation result as json
				if ( $is_ajax ) {
					echo json_encode($result);
					die;
				}
				// Otherwise, render the phtml and show errors in it
				else {
					$this->view->errors = $result['msgs'];
					return;
				}
			}
			$sent_by = '';
			if ( $user = Model_Users::getCurrent() )
			{
				$sent_by = $user->user_type.' '.$user->username.' wrote a message';
			}
			if ( $data['about'] ) {
				$about_escort = $model->get($data['about']);
			}
			if ( ! $about_escort ) $about_escort = (object) array('id' => '', 'showname' => '');

			// Set the template parameters and send it to support or to an escort
			$tpl_data = array(
				'name' => $data['name'],
				'email' => $data['email'],
				'to_addr' => $data['to_addr'],
				'message' => $data['message'],
				'escort_showname' => $about_escort->showname,
				'escort_id' => $about_escort->id,
				'sent_by' => $sent_by
			);

			if ( isset($data['username']) ) {
				$tpl_data['username'] = $data['username'];
			}

			Cubix_Email::sendTemplate($email_tpl, $data['to_addr'] , $tpl_data, isset($escort) ? $data['email'] : null);

			// contact me log
			Cubix_Api::getInstance()->call('contactLog', array($data['to_addr'], $user ? $user->id : null));
			//

			if ( $is_ajax ) {
				if ( isset($escort) ) {
					$result['msg'] = '
						<p style="padding:0 10px"><span class="strong" style="color: #060">' . __('message_sent_to_escort', array('showname' => $escort->showname)) . '</span></p>
					';
				}
				else {
					$result['msg'] = '
						<p style="padding:0 10px"><span class="strong" style="color: #060">Your feedback has been successfully submitted!</span></p>
					';
				}
				//<a href="#" onclick="return Cubix.Feedback.Show(this.getParent(\'.cbox-small-ii\'))">Click here</a> to hide this message</strong>
				echo json_encode($result);
				die;
			}
			else {
				$this->view->success = TRUE;
			}
		}
	}


	public function captchaAction()
	{

		$this->_request->setParam('no_tidy', true);

		$font = 'css/trebuc.ttf';

		$charset = '123456789ABCDEFGHIJKLMNPQRSTUVWXYZ';

		$code_length = 5;

		$height = 30;
		$width = 90;
		$code = '';

		for ( $i = 0; $i < $code_length; $i++ )
		{
			$code = $code . substr($charset, mt_rand(0, strlen($charset) - 1), 1);
		}

		$rgb[0] = array(204,0,0);
		$rgb[1] = array(34,136,0);
		$rgb[2] = array(51,102,204);
		$rgb[3] = array(141,214,210);
		$rgb[4] = array(214,141,205);
		$rgb[5] = array(100,138,204);

		$font_size = $height * 0.4;

		$bg = imagecreatefrompng('img/bg_captcha.png');
		$image = imagecreatetruecolor($width, $height);
		imagecopy($image, $bg, 0, 0, 0, 0, imagesx($bg), imagesy($bg));
		$background_color = imagecolorallocate($image, 255, 255, 255);
		$noise_color = imagecolorallocate($image, 20, 40, 100);

		for($i = 0; $i<$code_length; $i++)
		{
			$A[] = rand(-20, 20);
			$C[] = rand(0, 5);
			$text_color = imagecolorallocate($image, $rgb[$C[$i]][0], $rgb[$C[$i]][1], $rgb[$C[$i]][2]);
			imagettftext($image, $font_size, $A[$i], 7 + $i * 15, 20 + rand(-3, 3), $text_color, $font , $code[$i]);
		}

		$session = new Zend_Session_Namespace('captcha');
		$session->captcha = strtolower($code);

		header('Content-Type: image/png');
		imagepng($image);
		imagedestroy($image);

		die;
	}


    public function alleClubsAction()
    {
        $this->view->banners = Zend_Registry::get('banners');
    }

     public function alleClubs2Action()
    {
        $this->view->banners = Zend_Registry::get('banners');
    }

	public function faqAction(){
		$this->view->layout()->setLayout('mobile-index');
		$this->view->faq = true;
	}

	public function videoInstructionsAction()
	{
		$this->view->layout()->setLayout('mobile-video-instructions');
	}

	public function videoListAction()
	{
		if ($this->_request->ajax) {
			$this->view->layout()->disableLayout();
		} else {
			$this->view->layout()->setLayout('mobile-index');
		}


		$lng = Cubix_I18n::getLang();
		$page = $this->_request->getParam('page', 1);
		$per_page = $this->_request->getParam('per_page', 50);
		$city_id = $this->_request->getParam('city_id');

		/* ALL VIDEOS */

		$model_video = new Model_Video();
		
		$excluded_ids = array(0);

		//if(!$city_id){
		$random_votd = $model_video->getRandomVotd();



		if($random_votd){
			$image = new Cubix_ImagesCommonItem($random_votd);
			$random_votd->video_image = $image->getUrl('video_thumb');
			$excluded_ids[] = $random_votd->video_id;
		}
		//}
		$result = $model_video->getAll($page, $per_page, $city_id, $excluded_ids);

		$videos = $result['page_data'];
		$total_videos = $result['total_video_data'];

		$sess_name = 'video_list';
		if ($sess_name) {

			$sid = 'sedcard_paging_' . Cubix_Application::getId() . '_' . $this->_request->region;

			$sess = new Zend_Session_Namespace($sid);

			$sess->{$sess_name} = array();

			$city_votd = false;

			foreach ( $videos as $video ) {
				if($video->is_votd == 1){
					$city_votd = true;
				}
				$sess->{$sess_name}[] = $video->escort_id;
			}

			if(!$city_votd && $random_votd){
				array_unshift($videos, $random_votd);
			}

		}

        $escorts = array();
        $model_escort = new Model_Escorts();

        $cache = Zend_Registry::get('cache');

        $escorts_cache_key = 'escorts_list_' . Cubix_Application::getId() . '_lang_' . Cubix_I18n::getLang() . '_city_' . ($city_id ? $city_id : 'all') . '_page_' . $page;
        if ( ! $escorts = $cache->load($escorts_cache_key) ) {
            foreach ($videos as $key => $video)
            {
                $escorts[$key] = $model_escort->getById($video->escort_id);
                $escorts[$key]['has_video'] = $model_escort->checkHasVideo($video->escort_id);
                $escorts[$key]['is_votd'] = $video->is_votd;
                $escorts[$key]['city_id'] = $video->city_id;
                $escorts[$key]['age'] = $video->age;
            }

            $cache->save($escorts, $escorts_cache_key, array());

        }

        if(!$city_votd && $random_votd){
            $escorts[0] = $model_escort->getById($random_votd->escort_id);
            $escorts[0]['has_video'] = $model_escort->checkHasVideo($random_votd->escort_id);
            $escorts[0]['is_votd'] = $random_votd->is_votd;
            $escorts[0]['city_id'] = $random_votd->city_id;
            $escorts[0]['age'] = $random_votd->age;
        }

        $count_total = 0;
		foreach ($total_videos as $item) {
			$count_total += $item->v_count;
		}

		foreach($videos as &$video) {
			$image = new Cubix_ImagesCommonItem($video);
			$video->video_image = $image->getUrl('video_thumb');
		}

		$this->view->random_votd = $random_votd;

		$this->view->city_videos = $total_videos;
//		$this->view->videos = $videos;
        $this->view->escorts = $escorts;
		$this->view->count = $count_total;
		$this->view->lang = $lng;
		$this->view->page = $page;
		$this->view->total_pages = ceil($count_total / $per_page);
		$this->view->city_id = $city_id;
	}

	public function pricesAction()
	{
		$this->view->layout()->setLayout('mobile-static-page');
	}

	public function videoVotingAction()
	{
		$this->view->layout()->setLayout('mobile-plain-page');
	}

	public function videoVotingTermsAction()
	{
		$this->view->layout()->setLayout('mobile-plain-page');
	}

	public function feedbackAdsAction()
    {
        $this->_request->setParam('no_tidy', true);

        $m_class = new Model_ClassifiedAds();

        // Fetch administrative emails from config
        $config = Zend_Registry::get('feedback_config');
        $site_emails = $config['emails'];

        $is_ajax = ! is_null($this->_getParam('ajax'));

        $feedback = new Model_Feedback();

        // If the request is ajax, disable the layout
        if ( $is_ajax ) {
            $this->view->layout()->disableLayout();
        }

        // Get data from request
        $data = new Cubix_Form_Data($this->getRequest());

        $fields = array(
            'to' => '',
            'about' => 'int',
            'name' => 'notags|special',
            'email' => '',
            'message' => 'notags|special',
            'captcha' => '',
            'rand' => ''
        );
        $data->setFields($fields);
        $data = $data->getData();
        $blackListModel = new Model_BlacklistedWords();
        $this->view->data = $data;

        // If the to field is invalid, that means that user has typed by
        // hand, so it's like a hacking attempt, simple die, without explanation
        if ( ! is_numeric($data['to']) && ! in_array($data['to'], array_keys($site_emails)) ) {
            die;
        }
        // If escort id was specified, fetch it's email and showname,
        // construct the result email template and send it to escort
        if ( is_numeric($data['to']) ) {

            $ad = $m_class->get($data['to']);
            //var_dump($escort);die;
            // If the escort id is invalid that means that it is hacking attempt
            if ( ! $ad ) {
                die;
            }

            $data['to_addr'] = $ad->email;
            $data['to_name'] = $ad->title;
            $data['escort_showname'] = $ad->title;
            $email_tpl = 'escort_feedback_ads';
        }


        // In order when the form was posted validate fields
        if ( $this->_request->isPost() ) {
            $validator = new Cubix_Validator();

            if ( ! $blackListModel->checkFeedback($data) ){
                $blackListModel->mergeFeedbackData($data);
                // $validator->setError('f_message', 'Do Not repeat The Same Message' /*'Email is required'*/);
            }

            if ( ! strlen($data['email']) ) {
                $validator->setError('f_email', '' /*'Email is required'*/);
            }
            elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
                $validator->setError('f_email', '' /*'Wrong email format'*/);
            }elseif( $blackListModel->checkEmail($data['email']) ){
                $data['flag'] = FEEDBACK_FLAG_BLOCKED_BY_EMAIL;
                $data['status'] = FEEDBACK_STATUS_DISABLED;
                //$validator->setError('email', 'This email is blocked');
            }

            if ( ! strlen($data['message']) ) {
                $validator->setError('f_message','' /*'Please type the message you want to send'*/);
            }
            elseif($blackListModel->checkWords($data['message'])) {
                $data['flag'] = FEEDBACK_FLAG_BLOCKED_BY_WORD;
                $data['status'] = FEEDBACK_STATUS_DISABLED;
                $validator->setError('f_message', 'You can`t use words "'.$blackListModel->getWords() .'"');
            }

            if( isset($data['flag']) && ($data['flag'] == FEEDBACK_FLAG_BLOCKED_BY_EMAIL || $data['flag'] == FEEDBACK_FLAG_BLOCKED_BY_WORD) ){
                $data['application_id'] = Cubix_Application::getId();
            }

            if ( ! strlen($data['captcha']) ) {
                $validator->setError('captcha', '');//Captcha is required
            }
            else {
                $session = new Zend_Session_Namespace('captcha');
                $orig_captcha = $session->captcha;

                if ( strtolower($data['captcha']) != $orig_captcha ) {
                    $validator->setError('captcha', '');//Captcha is invalid
                }
            }

            $result = $validator->getStatus();

            if ( ! $validator->isValid() ) {
                // If the request was ajax, send the validation result as json
                if ( $is_ajax ) {
                    echo json_encode($result);
                    die;
                }
                // Otherwise, render the phtml and show errors in it
                else {
                    $this->view->errors = $result['msgs'];
                    return;
                }
            }
            else
            {
                $sess = new Zend_Session_Namespace('feedback');

                if ($data['rand'] != $sess->rand)
                {
                    echo json_encode(array('status' => 'success', 'msg' => '<p style="padding:0 10px"><span class="strong" style="color: #060">Your feedback has been successfully submitted!</span></p>'));
                    die;
                }

                if( isset($data['flag']) && ($data['flag'] == FEEDBACK_FLAG_BLOCKED_BY_EMAIL) ) { // IF blocked by email imitate as sent feedback
                    echo json_encode(array('status' => 'success', 'msg' => '<p style="padding:0 10px; margin-top: 50px; width: 330px; font-size: 14px"><span class="strong" style="color: #000">' . __('message_sent_to_escort', array('showname' => $escort->showname)) . '</span></p>'));
                    die;
                }
            }


            $tpl_data = array(
                'name' => $data['name'],
                'email' => $data['email'],
                'to_addr' => $data['to_addr'],
                'message' => $data['message'],
                'escort_showname' => $ad->title,
                'escort_id' => $ad->id
            );

            if ( isset($data['username']) ) {
                $tpl_data['username'] = $data['username'];
            }

            Cubix_Email::sendTemplate($email_tpl, $data['to_addr'], $tpl_data, isset($ad) ? $data['email'] : null);

            $sess->unsetAll();

            if ( $is_ajax ) {

                $result['msg'] = '
					<p style="padding:0 10px"><span class="strong" style="color: #060">Your feedback has been successfully submitted!</span></p>
				';

                echo json_encode($result);
                die;
            }
            else {
                $this->view->success = TRUE;
            }
        }
    }

}
