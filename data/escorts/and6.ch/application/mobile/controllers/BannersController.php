<?php

/**
 * @autor Eduard Hovhannisyan
 * @created 08/07/2020
 *
 * Class Mobile_BannersController
 */
class Mobile_BannersController extends Zend_Controller_Action
{
    /**
     * This is used to make a payment with testing credentials on production mode.
     *
     * @var bool
     */
    private $isDebug = false;

    /**
     * @var Model_Countries
     */
    private $modelCountres;

    /**
     * @var Model_Banners
     */
    private $modelBanners;

    /**
     * These are the ids of countries that need
     * to be listed above the others, to make it
     * easier for our active customers to find their country
     *
     * @var array
     */
    private $prioritizedCountries = [1, 216, 36, 77, 13, 101, 71, 145];

    // private $paymentMethods = [
    //     'default'       => 'twispay',
    //     //'alternative-1' => 'postfinance',
    //     //'alternative-2' => 'paysafe',
    // ];

    /**
     * @return void
     */
    public function init()
    {
        $this->view->layout()->setLayout('mobile-index');

        $this->modelCountres    = new Model_Countries();
        $this->modelBanners     = new Model_Banners();
        $this->modelPayments = new Model_PaymentMethodes();
        
        $this->view->activ_payments =  $this->modelPayments->getActivePaymentMethodes(Cubix_Application::getId());
       
    }

    /**
     * Purchasing page for /banner-purchase
     *
     * @acceptsRequests GET
     * @return void
     */
    public function purchaseAction()
    {
        // Select country codes and Order them
        // by setting Central European countries + USA CANADA at the top
        $phoneCountriesRaw = $this->modelCountres->getPhoneCountries();
        $phoneCountries = [];
        foreach ($phoneCountriesRaw as $item) {
            if (in_array(strtolower($item->id), $this->prioritizedCountries))
                array_unshift($phoneCountries, $item);
            else
                array_push($phoneCountries, $item);
        }

       // $selectedPaymentMethod = $this->paymentMethods['default'];

       // $this->view->selectedPaymentMethod = $selectedPaymentMethod;
        $this->view->phoneCountries = $phoneCountries;
        $this->view->prioritizedCountries = $this->prioritizedCountries;
       // $this->view->paymentMethods = $this->paymentMethods;
        $this->view->bannersList = Model_Banners::getBannersToOrder();
        $this->view->selectedPhoneCC = 41;
    }

    /**
     * Endpoint action to handle Twispay redirection,
     * whenever user completes payment process in https://secure.twispay.com
     *
     * @acceptsRequests GET
     * @return void
     * @throws Zend_Exception
     */
     public function twispayResponseAction()
    {
        $twispayConfigs = $this->_getTwispayConfig();
        $decryptedData = Cubix_Twispay_TwispayApi::decryptIpnResponse($this->_request->opensslResult, $twispayConfigs['key']);
		
		if($decryptedData['externalOrderId']){
			$reference = explode('-', $decryptedData['externalOrderId']);
			$order_id = $reference[1];
			$this->_request->setParam('orderId', $order_id);
		}
		
        if ($decryptedData['transactionStatus'] == 'in-progress' || $decryptedData['transactionStatus'] == 'complete-ok') {
            $this->_request->setParam('transactionId', $decryptedData['transactionId']);
			$this->_forward('payment-success');
        } else {
			$this->_forward('payment-failure');
        }
    }

    /**
     * @acceptsRequests GET
     * @return void
     * @throws Zend_Exception
     */
    public function paymentSuccessAction()
    {
		$order_id = intval($this->_request->orderId);
        $orderData = $this->modelBanners->getOrderById($order_id);
        if(!$orderData) {
            return $this->_redirect('/banner-purchase');
        }
		
        /*switch ($order['payment_method']) {
            case $this->paymentMethods['alternative-1']:
                $order['transaction_id'] = '- - -';
                break;

            case $this->paymentMethods['alternative-2']:
                $order['transaction_id'] = $this->_request->transactionId;
                break;
        }*/
		       
        $this->modelBanners->markOrderAsPaid($order_id, [
            'transaction_id' => $this->_request->transactionId
        ]);
		$orderData['transaction_id'] = $this->_request->transactionId;
        
        $banner = $this->modelBanners->getBannersToOrder($orderData['banner_id']);
        $this->notifyAdminAboutOrder($orderData, $banner);
    }

    /**
     * @acceptsRequests GET
     * @return void
     * @throws Zend_Exception
     */
    public function paymentFailureAction()
    {
        $order_id = intval($this->_request->orderId);

        if(!isset($order_id)) {
            return $this->_redirect('/banner-purchase');
        }

        $this->modelBanners->markOrderAsRejected($order_id);
    }

    public function paysafeResponseAction()
    {
        $payment = new Cubix_2000charge_Transaction();
		$result = $payment->get($this->_request->transactionId);
		
		if($result->merchantTransactionId){
			$reference = explode('-', $result->merchantTransactionId);
			$order_id = $reference[1];
			
			$this->_request->setParam('orderId', $order_id);
		}
		
        if ($result->status == 'Funded') {
            $this->_request->setParam('transactionId', $this->_request->transactionId);
			$this->_forward('payment-success');
        } else {
			$this->_forward('payment-failure');
        }
    }

    

    /**
     * Is used only for ajax request.
     *
     * @acceptsRequests POST
     * @return void
     */
    public function checkoutAction()
    {
        if (!$this->_request->isPost()) {
            header($_SERVER["SERVER_PROTOCOL"] . " 405 Method Not Allowed", true, 405);
            exit;
        }

        $validation = $this->_validateCheckoutAction($this->_request);

        if ($validation->isValid() === false) {
            echo(json_encode($validation->getStatus()));
            exit;
        }

        $orderData = [
            'phone_cc'          => $this->_request->country_code,
            'phone_number'      => $this->_request->phone,
            'email'             => $this->_request->email,
            'website'           => $this->_request->website,
            'payment_method'    => $this->_request->payment_method,
            'text'              => $this->_request->description,
            'banner_id'         => $this->_request->banner_id,
            'customer_ip'       => Cubix_Geoip::getIP(),
            'files'             => array(),
        ];

        foreach ($this->_request->images as $image) {

            if (preg_match('/^data:image\/(\w+);base64,/', $image, $type)) {
                $data = substr($image, strpos($image, ',') + 1);
                $type = strtolower($type[1]); // jpg, png, gif

                $data = base64_decode($data);

                if ($data === false) {
                    throw new \Exception('base64_decode failed');
                }
            } else {
                throw new \Exception('did not match data URI with image data');
            }

            $path = 'agency-posters/' . uniqid() . '.' . $type;
            file_put_contents($path, $data);

            $orderData['files'][] = $path;
        }

        try {
            $orderId = $this->modelBanners->createOrder($orderData);
            if (empty($orderId)) throw new \Exception('Couldn\'t create order, please try again later.');

			$paymentMethod = $this->_request->payment_method;
            $methodName = 'checkoutWith' . $paymentMethod;
            if (!method_exists($this, $methodName)) throw new \Exception('Invalid Payment method: ' . $paymentMethod);

            $result = call_user_func([$this, $methodName], $orderId);
            echo json_encode($result, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES);

        } catch
        (\Exception $e) {
            echo json_encode([
                'status' => 'error',
                'msgs' => ['general_issue' => $e->getMessage()]
            ]);
        }

        exit;
    }

    /**
     * Only $this->checkoutAction is allowed to call this method.
     * Please keep it private or protected.
     *
     * @acceptsRequests POST
     * @return array
     * @throws \Exception
     */
    private function checkoutWithTwispay($orderId)
    {
        if(!$orderId) throw new \Exception('OrderId is required');

        $reference          = 'BannerOrderA6-' . $orderId . '-' . uniqid();
        $bannerId           = $this->_request->banner_id;
        $selectedBanner     = Model_Banners::getBannersToOrder($bannerId);
        $amount             = (APPLICATION_ENV == 'development' || $this->isDebug) ? 1 : $selectedBanner['price'];

        $twispayConfigs = $this->_getTwispayConfig();

        $data = array(
            'siteId' => intval($twispayConfigs['siteid']),
            'cardTransactionMode' => 'authAndCapture',
            'backUrl' => APP_HTTP . '://' . $_SERVER['HTTP_HOST'] . '/banners/twispay-response',
            'invoiceEmail' => '',
            'customer' => [
                'identifier' => 'user-and6-guest',
                'firstName' => '',
                'username' => '',
                'email' => '',
            ],
            'order' => [
                'orderId' => $reference,
                'type' => 'purchase',
                'amount' => $amount,
                'currency' => $twispayConfigs['currency'],
                'description' => 'Dedicated payment in Desktop And6',
            ]
        );

        $base64JsonRequest = Cubix_Twispay_TwispayApi::getBase64JsonRequest($data);
        $base64Checksum = Cubix_Twispay_TwispayApi::getBase64Checksum($data, $twispayConfigs['key']);

        try {
            $formActionUrl = $twispayConfigs['url'] . '?lang=' . Cubix_I18n::getLang();
            $paymentFrom = "
                <form id=\"twispay-payment-form\" style='display: none;' action='{$formActionUrl}' method='post' accept-charset='UTF-8'>
                    <input type='hidden' name='jsonRequest' value = \"{$base64JsonRequest}\" >
                    <input type='hidden' name='checksum' value = \"{$base64Checksum}\" >
                    <input type = \"submit\" value = \"Pay\" >
                </form > ";

        } catch (Exception $e) {
            return array(
                'status' => 'error',
                'error' => $e->getMessage()
            );
        }

        return array(
            'status' => 'twispay_success',
            'form' => $paymentFrom,
        );
    }

    private function checkoutWithFaxin($orderId)
    {
        if(!$orderId) throw new \Exception('OrderId is required');
        
        $this->client = new Cubix_Api_XmlRpc_Client();

        $hash               = base_convert(time(), 10, 36);
        $reference          = 'BannerOrderA6-' . $orderId . '-' . uniqid();
        $bannerId           = $this->_request->banner_id;
        $selectedBanner     = Model_Banners::getBannersToOrder($bannerId);
        $amount             = (APPLICATION_ENV == 'development' || $this->isDebug) ? 1 : $selectedBanner['price'];
        $amount             = 100 * $amount;

        $postdata = http_build_query(array('sysorderid' => $hash, 'amount' => $amount ));
        
        $opts = array('http' => array(
            'method'  => 'POST',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => $postdata
        ));
        
        $context  = stream_context_create($opts);
        $faxin_result = file_get_contents('https://pay.faxin.ch/payment', false, $context);
        $result  = json_decode($faxin_result);

        if($result->success){
            $result->status = 1;
            $reference =  'BA-'. $orderId. '-' . $bannerId;
            $this->client->call('OnlineBillingV2.storeFaxin', array('0', $result->paymentid, $reference, $amount, 0));
            return array(
                'status' => 'faxin_success',
                'url' => $result->url
            );
        }else{
            return array(
                'status' => 'error',
                'error' => 'FAXIN payment is not available at the moment'
            );
        }

    }

    /**
     * Only $this->checkoutAction is allowed to call this method.
     * Please keep it private or protected.
     *
     * @acceptsRequests POST
     * @return array
     * @throws \Exception
     */
    private function checkoutWithPaysafe($orderId)
    {
        if(!$orderId) throw new \Exception('OrderId is required');

        try {

            $reference      = 'BannerOrderA6-' . $orderId . '-' . uniqid();
            $bannerId       = $this->_request->banner_id;
            $selectedBanner = Model_Banners::getBannersToOrder($bannerId);
            $amount         = (APPLICATION_ENV == 'development' || $this->isDebug) ? 10 : $selectedBanner['price'];

            $customer = new Cubix_2000charge_Model_Customer();
            $customer->setCountry("CH");
            $customer->setFirstName('noname');
            $customer->setLastName('noname');
			$customer->setEmail(!empty($this->_request->email) ? $this->_request->email : 'test@test.com');

            $payment = new Cubix_2000charge_Model_Payment();
            $payment->setPaymentOption("paysafe");
            $payment->setHolder('Guest Customer');

            $transaction = new Cubix_2000charge_Model_Transaction();
            $transaction->setCustomer($customer);
            $transaction->setPayment($payment);
            $transaction->setAmount($amount * 100);
            $transaction->setCurrency("CHF");
            $transaction->setIPAddress(Cubix_Geoip::getIP());
            $transaction->setMerchantTransactionId($reference);

            $successUrl = APP_HTTP . '://' . $_SERVER['HTTP_HOST'] . '/banners/paysafe-response';
            $failureUrl = APP_HTTP . '://' . $_SERVER['HTTP_HOST'] . '/banners/paysafe-response';
            $redirectUrls = new Cubix_2000charge_Model_RedirectUrls();
            $redirectUrls->setReturnUrl($successUrl);
            $redirectUrls->setCancelUrl($failureUrl);
            $transaction->setRedirectUrls($redirectUrls);

            $res = Cubix_2000charge_Transaction::post($transaction);

        } catch (Exception $e) {
            return array(
                'status' => 'error',
                'msgs' => ['general_issue' => $e->getMessage()]
            );
        }

        return array(
            'status' => 'paysafe_success',
            'url' => $res->redirectUrl,
        );
    }

    /**
     * Only $this->checkoutAction is allowed to call this method.
     * Please keep it private or protected.
     *
     * @acceptsRequests POST
     * @return array
     * @throws \Exception
     */
    private function checkoutWithPostfinance($orderId)
    {
        if(!$orderId) throw new \Exception('OrderId is required');

        try {

            $reference      = 'BannerOrderA6-' . $orderId . '-' . uniqid();
            $bannerId       = $this->_request->banner_id;
            $selectedBanner = Model_Banners::getBannersToOrder($bannerId);
            $amount         = (APPLICATION_ENV == 'development' || $this->isDebug) ? 1 : $selectedBanner['price'];

            $postfinancePayment = new Model_Postfinance();
            $successUrl = APP_HTTP . '://' . $_SERVER['HTTP_HOST'] . '/banners/payment-success';
            $failureUrl = APP_HTTP . '://' . $_SERVER['HTTP_HOST'] . '/banners/payment-failure';

            $data = array(
                'PSPID'         => $postfinancePayment->PSPID,
                'ORDERID'       => $reference,
                'AMOUNT'        => $amount * 100,
                'LANGUAGE'      => 'de_DE',
                'CURRENCY'      => $postfinancePayment->currency,
                'ACCEPTURL'     => $successUrl,
                'DECLINEURL'    => $failureUrl,
                'CANCELURL'     => $failureUrl,
            );

            if(APPLICATION_ENV == 'development' || $this->isDebug) {
                $data['PSPID'] = $postfinancePayment->PSPID_TEST;
            }

            $data['SHASIGN'] = $postfinancePayment->generateChecksum($data, null);
            $submitUrl = (APPLICATION_ENV == 'development' || $this->isDebug) ? $postfinancePayment->submitUrlTest : $postfinancePayment->submitUrl;

            $paymentFrom = '<form style="display:none" method="post" action="' . $submitUrl . '"id="postfinance_form">';

            foreach($data as $k => $v) {
                $paymentFrom .= '<input type="hidden" name="' . $k . '" value="'. $v . '">';
            }

            $paymentFrom .= '<input type="submit" value="">';
            $paymentFrom .= '</form>';

            return array(
                'status'    => 'postfinance_success',
                'form'      => $paymentFrom
            );

        } catch (Exception $e) {
            return array(
                'status' => 'error',
                'msgs' => ['general_issue' => $e->getMessage()]
            );
        }

        return array(
            'status' => 'paysafe_success',
            'url' => $res->redirectUrl,
        );
    }

    /**
     * @return mixed
     */
    private function _getTwispayConfig()
    {
        $paymentConfigs = Zend_Registry::get('payment_config');
        $twispayConfigs = $paymentConfigs['twispay'];

        if($this->isDebug) {
            return [
                'siteid' => 4466,
                'api_url' => 'https://api-stage.twispay.com',
                'url' => 'https://secure-stage.twispay.com',
                'key' => '63fcb6152d4994d38932d38b3da81080',
                'currency' => 'CHF',
            ];
        }

        return $twispayConfigs;
    }

    /**
     * Validation handler for the form
     * used when ordering a banner.
     * @return Cubix_Validator
     */
    private function _validateCheckoutAction($request)
    {
        $form = new Cubix_Form_Data($request);
        $form->setFields(array(
            'banner_id' => 'int',
            'country_code' => '',
            'phone' => 'int',
            'email' => 'notags',
            'website' => '',
            'payment_method' => '',
            'description' => '',
        ));

        $data = $form->getData();

        $validator = new Cubix_Validator();

        if (empty($data['country_code']))
            $validator->setError('phone', __('country_code_required'));
        else if (!is_numeric($data['country_code']))
            $validator->setError('phone', __('country_code_not_valid'));

        if (empty($data['phone']))
            $validator->setError('phone', __('phone_required'));
        else if (!is_numeric($data['phone']))
            $validator->setError('phone', __('phone_not_valid'));

        if (empty($data['payment_method']))
            $validator->setError('payment_method', __('payment_method_required'));

        if (empty($data['description']))
            $validator->setError('description', __('description_required'));

        if (empty($data['banner_id']))
            $validator->setError('banner', __('bp_banner_not_selected'));

        return $validator;
    }

    /**
     * Send email notification to admin,
     * with details about Purchased Banner.
     *
     * @param $orderData
     * @param $banner
     * @return bool
     */
    private function notifyAdminAboutOrder($orderData, $banner)
    {
        $banner         = (array)$banner;
        $orderData      = (array)$orderData;
        $subject        = 'Someone ordered an And6 Banner';
        $emailsToNotify = $this->isDebug ? ['badalyano@gmail.com'] : [ 'mike@and6.com', 'sharps1@protonmail.com'];

        $imagesHtml = "";
        $imagesHostname = APPLICATION_ENV === 'development' ? 'http://www.and6.ch.test' : 'https://www.and6.com';

        $html = "
            <h5><b>Order Details</b>:</h5>
            <ul>
                <li><b>Status</b>: PAID</li>
                <li><b>Transaction ID</b>: {$orderData['transaction_id']}</li>
                <li><b>Checkout using</b>: {$orderData['payment_method']}</li>
                <li><b>Phone</b>: (+{$orderData['phone_cc']}) {$orderData['phone_number']}</li>
                <li><b>Email</b>: {$orderData['email']}</li>
                <li><b>Website</b>: {$orderData['website']}</li>
                <li><b>Customer IP</b>: {$orderData['customer_ip']}</li>
                <li><b>Ordered At</b>: {$orderData['created_at']} CET</li>
            </ul>
            
            <h5><b>Banner Text</b>:</h5>
            {$orderData['text']}

            <h5><b>Banner Details</b>:</h5>
            <ul>
                <li><b>Title</b>: {$banner['texts']['title']} </li>
                <li><b>Duration</b>: {$banner['texts']['duration']} </li>
                <li><b>Price</b>: {$banner['price']} {$banner['currency']} </li>
            </ul>
        ";


        $attachments = [];
        foreach ($orderData['files'] as $key => $file) {
            $fileParts = explode('.', $file['path']);
            $attachments[] = [
                'item' => file_get_contents($file['path']),
                'name' => 'attachment_' . ($key + 1) . '.' . end($fileParts),
                'type' => end($fileParts),
            ];
        }

        $sent = true;
        foreach ($emailsToNotify as $email) {
            if (!Cubix_Email::send($email, $subject, $html, true, null, null, $attachments)) $sent = false;
        }

        return $sent;
    }
}