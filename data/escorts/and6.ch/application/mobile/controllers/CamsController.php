<?php

class Mobile_CamsController extends Zend_Controller_Action
{
	public function init()
	{	
		$this->view->layout()->setLayout('mobile/cams');
		$this->user = Model_Users::getCurrent();
	}
		
	public function indexAction()
	{
		$api_model = new Model_Api_Cams($this->user->id);
		$system = Zend_Registry::get('system_config');
		$req = $this->_request->req;
		$query = $_SERVER['QUERY_STRING'] ? '?' . $_SERVER['QUERY_STRING'] : '';
		$this->view->iframe_url = $system['cams']['url'] . $req. $query;
		$this->view->iframe_name = 'ify_'.rand(1, 1000000);
		$this->view->user_type = false;
		
		if($this->user && $this->user->isMember()){
			$fields = array(
				'display_name' => $this->user->username,
				'email' => $this->user->email
			);
						
			$api_results = $api_model->memberLogin($fields);
			if($api_results['status'] == 200){
				$this->view->token = $api_results['body']['auth_token'];
			}
			elseif($api_results['status'] == 404){
				$api_results = $api_model->memberCreate($fields);
				if($api_results['status'] == 200){
					$this->view->token = $api_results['body']['auth_token'];
				}
			}
			$this->view->username = $this->user->username;
			$this->view->user_type = 'member';
		}
		if ($this->user && $this->user->isEscort() && $this->user->has_active_package && ($this->user->escort_data['escort_status'] & 32) ) {
			
			$fields = $api_model->prepareEscortDataForApi($this->user);
			
			$api_results = $api_model->escortLogin($fields); 
			
			if($api_results['status'] == 200){
				$this->view->token = $api_results['body']['auth_token'];
			}
			elseif($api_results['status'] == 404){
				$api_results = $api_model->escortCreate($fields);
				if($api_results['status'] == 200){
					$this->view->token = $api_results['body']['auth_token'];
				}
			}
			$this->view->username = $this->user->username;
			$this->view->showname = $this->user['escort_data']['showname'];
			$this->view->avatar = $this->user['chat_info']['avatar'];
			$this->view->user_type = 'escort';
		}
		
		$this->_response->setRedirect($this->view->getLink('cams-login-redirect', array('one-time-token' => $this->view->token, 'over18' => 1, 'path' => $req )));
	}
	
	public function loginAction()
	{
		$this->view->layout()->disableLayout();
	}
	
	public function signupAction()
	{
		$this->view->layout()->disableLayout();
	}
	
	public function buyTokensAction()
	{
		$this->view->layout()->disableLayout();
		$this->user = Model_Users::getCurrent();
		$defines = Zend_Registry::get('defines');
		if(!$this->user || !$this->user->isMember()){
			die('no permission');
		}
		
		if($this->_request->isPost()){
					
			$tokens = $this->_request->tokens;
			
			if(!array_key_exists($tokens, $defines['AND6_CAM_TOKEN_CURRENCIES'])){
				die(json_encode(array('status' => 'payment_failure' )));
			}
						
			try{					
				$amount = $defines['AND6_CAM_TOKEN_CURRENCIES'][$tokens];
				
				$data = array(
					'user_id' => $this->user->id,
					'tokens' => $tokens,
					'amount' => $amount,
				);
				//$hash  = base_convert(time(), 10, 36);

				$model_member = new Model_Members();
				$id = $model_member->addTokenRequest($data);
											
				// EPG PAYMENT
					
				$epg_payment = new Model_EpgGateway();
				$reference = 'cam-' . $id . '-' . $this->user->id; 
				
				$fields = $epg_payment->getFieldsForAuth($reference, $amount, $this->view->getLink('and6-cams-payment-response'));
				$client = new Cubix_Api_XmlRpc_Client();
				$client->call('OnlineBillingV2.storeToken', array($fields['Token'], $this->user->id, 'epg'));

				echo json_encode(array( "status" => 'payment_success', "url" => $fields['RedirectUrl'] )); die;
			}
			catch(Exception $ex) {
				var_dump($ex->getMessage());
				die(json_encode(array('status' => 'payment_failure' )));
			}
		}
		else{
			$this->view->token_currencies = $defines['EF_CAM_TOKEN_CURRENCIES'];
		}
		
	}
	
	public function paymentResponseAction()
	{
		$token =  preg_replace('#[^a-zA-Z0-9.\-]#', '', $this->_request->Token);
			
		if ( ! $token ) $this->_redirect('/and6-cams');
			
		$epg_payment = new Model_EpgGateway();
		$result = $epg_payment->getTransactionStatus($token);
		
		if ($result['ResultStatus'] == "OK") {
			list($transaction_type, $request_id, $user_id) = explode('-', $result['TransactionReference']);
			$model_member = new Model_Members();
			
			if($model_member->checkStatus($request_id)){
				$this->_redirect('/and6-cams');
			}
			
			$defines = Zend_Registry::get('defines');
			$api_model = new Model_Api_Cams();
			
			$amount = $result['Amount'];
			$tokens = array_search($amount, $defines['AND6_CAM_TOKEN_CURRENCIES']);
			$payload = array(
				'purchase_amount' => (float)$amount,
				'purchase_currency' => 'CHF',
				'token_amount' => $tokens
			);
			
			$res = $api_model->memberPurchase($user_id, $payload);
			
			if($res['status'] != 200){
				$token_update = array( 
					'status' => TOKEN_PAYMENT_PAYED_NOT_RECEIVED_CAMS,
					'cams_header_status' =>  $res['status'],
					'cams_error' => json_encode($res['body']['error'])
				);
			}else{
				$token_update = array( 
					'status' => TOKEN_PAYMENT_PAYED_AND_RECEIVED_CAMS,
					'cams_header_status' =>  $res['status']
				);
			}
			$user = Model_Users::getCurrent();
			Cubix_Email::sendTemplate('token_member_purchase', $user->email, array(
				'subject' => 'Deine Zahlung für '. $tokens . ' Tokens bei and6 Cams',
				'amount' => $amount,
				'username' => $user->username,
				'tokens' => $tokens
			));
			
			
			$model_member->updateTokenRequest($request_id, $user_id, $tokens, $token_update);
			$this->view->amount = $amount;
			$this->view->tokens = $tokens;
			$this->view->success = true;
		} else {
			$this->view->success = false;
		}
	}

	public function balanceAction()
	{
		die('');
	}
}