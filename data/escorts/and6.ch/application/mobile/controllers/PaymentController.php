<?php

/**
 * @autor Eduard Hovhannisyan
 * @created 29/06/2020
 *
 * Class Mobile_PaymentController
 */
class Mobile_PaymentController extends Zend_Controller_Action
{
    /**
     * @var Array
     */
    private $paymentMethods;

    /**
     * @return void
     */
    public function init()
    {
        $this->view->layout()->setLayout('mobile-index');
        $this->paymentMethods = [
            'default' => 'twispay',
            'alt1' => 'twispay',
        ];
    }

    /**
     * Home page for /payments
     *
     * @acceptsRequests GET
     * @return void
     */
    public function indexAction()
    {
        $selectedKey = $this->getRequest()->getParam('paymentMethodKey', 'default');

        if(array_key_exists($selectedKey, $this->paymentMethods) === FALSE) return $this->_redirect($this->view->getLink('home'));

        $selectedPaymentMethod = $this->paymentMethods[$selectedKey];
        $this->view->paymentMethod = $selectedPaymentMethod;
    }


    /**
     * Endpoint action to handle Twispay redirection,
     * whenever user completes payment process in https://secure.twispay.com
     *
     * @acceptsRequests GET
     * @return void
     * @throws Zend_Exception
     */
    public function twispayResponseAction()
    {
        $twispayConfigs = Zend_Registry::get('payment_config')['twispay'];
        $decryptedData = Cubix_Twispay_TwispayApi::decryptIpnResponse($this->_request->opensslResult, $twispayConfigs['key']);

        $twispayKey = $twispayConfigs['key'];
        $twispayApiUrl = $twispayConfigs['api_url'];


        // Getting Transaction data
        $ch = curl_init($twispayApiUrl . '/transaction/' . $decryptedData['transactionId']);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization:" . $twispayKey));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $transaction = json_decode(curl_exec($ch), true);

        if ($decryptedData['transactionStatus'] == 'in-progress' || $decryptedData['transactionStatus'] == 'complete-ok') {

            $this->view->success = true;
            $this->view->response = array(
                'Transaction Id' => $decryptedData['transactionId'],
                'Card Holder' => $transaction['data']['cardHolderName'],
                'Amount' => $decryptedData['amount'],
                'Currency' => $decryptedData['currency'],
                'Date' => date('Y-m-d H:i', $decryptedData['timestamp'])
            );

        } else {
            $this->view->success = false;
        }
    }

    /**
     * Is used only for ajax request.
     *
     * @acceptsRequests GET | POST*
     * @return void
     */
    public function checkoutAction()
    {
        try {
            $paymentMethod = $this->_request->paymentMethod;

            $methodName = 'checkoutWith' . $paymentMethod;

            if (!method_exists($this, $methodName)) throw new \Exception('Invalid Payment method!');

            $result = call_user_func([$this, $methodName]);
            echo json_encode($result, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES);

        } catch (\Exception $e) {
            echo json_encode([
                'status' => 'error',
                'error' => $e->getMessage()
            ]);
        }

        exit;
    }

    /**
     * Only $this->checkoutAction is allowed to call this method.
     * Please keep it private or protected.
     *
     * @acceptsRequests POST
     * @return array
     * @throws \Exception
     */
    private function checkoutWithTwispay()
    {
        $reference = 'd-payment-bl-' . uniqid();
        $amount = $this->_request->amount;

        $paymentConfigs = Zend_Registry::get('payment_config');
        $twispayConfigs = $paymentConfigs['twispay'];

        $data = array(
            'siteId' => intval($twispayConfigs['siteid']),
            'cardTransactionMode' => 'authAndCapture',
            'backUrl' => APP_HTTP . '://' . $_SERVER['HTTP_HOST'] . '/payment/twispay-response',
            'invoiceEmail' => '',
            'customer' => [
                'identifier' => 'user-adspay-guest',
                'firstName' => '',
                'username' => '',
                'email' => '',
            ],
            'order' => [
                'orderId' => $reference,
                'type' => 'purchase',
                'amount' => $amount,
                'currency' => $twispayConfigs['currency'],
                'description' => 'Dedicated payment in Desktop and6.com',
            ]
        );

        $base64JsonRequest = Cubix_Twispay_TwispayApi::getBase64JsonRequest($data);
        $base64Checksum = Cubix_Twispay_TwispayApi::getBase64Checksum($data, $twispayConfigs['key']);

        try {
            $formActionUrl = $twispayConfigs['url'] . '?lang=' . Cubix_I18n::getLang();
            $paymentFrom = "
                <form id=\"twispay-payment-form\" style='display: none;' action='{$formActionUrl}' method='post' accept-charset='UTF-8'>
                    <input type='hidden' name='jsonRequest' value = \"{$base64JsonRequest}\" >
                    <input type='hidden' name='checksum' value = \"{$base64Checksum}\" >
                    <input type = \"submit\" value = \"Pay\" >
                </form > ";

        } catch (Exception $e) {
            return array(
                'status' => 'error',
                'error' => $e->getMessage()
            );
        }

        return array(
            'status' => 'twispay_success',
            'form' => $paymentFrom,
            'data' => $data,
            '$twispayConfigs' => $twispayConfigs
        );
    }
}
