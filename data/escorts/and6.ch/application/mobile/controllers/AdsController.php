<?php

class Mobile_AdsController extends Zend_Controller_Action
{
	private $ads = array(
		array( 'ad-popup-1.png' => 'http://s-pamela.ch' )
	);

	public function init()
	{

	}

	public function indexAction()
	{
		$this->view->layout()->disableLayout();
		$this->view->ad = array( $this->_request->img => $this->_request->href );
	}
}