<?php


class Mobile_OnlineBillingController extends Zend_Controller_Action {

	const PACKAGE_STATUS_PENDING  	= 1;
	const PACKAGE_STATUS_ACTIVE   	= 2;
	const PACKAGE_STATUS_EXPIRED  	= 3;
	const PACKAGE_STATUS_CANCELLED 	= 4;
	const PACKAGE_STATUS_UPGRADED 	= 5;
	const PACKAGE_STATUS_SUSPENDED 	= 6;

	private static $sedcardPackages = array(24,25);
	private static $dayPassesPackages = array(19,20,23);
	private static $additionalAreaPrice = 200;
	private static $gotdPrice = 90;
	private static $votdPrice = 90;
	private static $cryptoGotdPrice = 190;
	private static $cryptoVotdPrice = 190;

	protected $_session;

	public function init() {
		// < Session >

		$this->_session = new Zend_Session_Namespace('online_billing');
		$this->_session->setExpirationSeconds(60 * 60);

		// </ Session >

		// < View >
		$this->view->layout()->setLayout('mobile-self-checkout');
		$this->view->headTitle(
			__('private_area'),
			'PREPEND'
		);
		// </ View >


		// < User Authentification Check >

		$this->view->user = $this->user = Model_Users::getCurrent();

		$anonym = array('epg-response', 'success', 'failure', 'twispay-response' );


		if (!$this->user && !in_array($this->_request->getActionName(), $anonym)) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}


		if (!in_array($this->_request->getActionName(), $anonym) && !$this->user->isAgency() && !$this->user->isEscort()) {
			$this->_redirect($this->view->getLink('private'));
		}

		if ( $this->user && $this->user->isAgency() ) {
			$cache = Zend_Registry::get('cache');
			$cache_key =  'v2_user_pva_' . $this->user->id;
			if ( ! $agency = $cache->load($cache_key) ) {
					$agency = $this->user->getAgency();
					$cache->save($agency, $cache_key, array(), 300);
				}
			$this->agency = $this->view->agency = $agency;
			if( $this->agency->status != AGENCY_STATUS_ONLINE ){
				$this->_redirect($this->view->getLink('private-v2'));
			}
		}
		// </ User Authentification Check >

		// // < Session >

		// $this->_session = new Zend_Session_Namespace('online_billing');
		// $this->_session->setExpirationSeconds(60 * 60);

		// // </ Session >

		$this->client = new Cubix_Api_XmlRpc_Client();
	}

	public function indexAction() {
		if ($this->user->isAgency()) {
			$this->_redirect($this->view->getLink('online-billing-agency'));
		} else {
			$this->_redirect($this->view->getLink('online-billing-escort'));
		}
	}

	public function escortIndexAction() {
		if ($this->_session->cart){
			$this->view->existing_cart = $this->_session->cart;
		}

		if ($this->user->isAgency()) {
			$this->_redirect($this->view->getLink('online-billing-agnecy'));
		}

		$this->view->sedcardPackages = self::$sedcardPackages;
		$this->view->dayPassesPackages = self::$dayPassesPackages;
		$this->view->additionalAreaPrice = self::$additionalAreaPrice;
		$this->view->gotdPrice = self::$gotdPrice;
		$this->view->votdPrice = self::$votdPrice;
		$this->view->user = $this->user;
		
		$m_escorts = new Model_EscortsV2();
		
		//$this->view->eligible_to_have_5_days  =  $m_escorts->eligible_to_have_5_days($this->user->escort_data['escort_id']);
		//we disable free 5 pack
		$this->view->eligible_to_have_5_days  =  false;

		$this->view->escort = $escort = $m_escorts->get($this->user->escort_data['escort_id'], 	null,true);
		$m_video = new Model_Video();
		$this->view->has_approved_video = $has_approved_video = $m_video->GetEscortHasApprovedVideo($escort->id);
		$is_pseudo_escort = $this->client->call(
			'OnlineBillingV2.isPseudoEscort',
			array($escort->id)
		);

		$user_type = $is_pseudo_escort ? USER_TYPE_AGENCY : USER_TYPE_SINGLE_GIRL;
		$escort->is_pseudo_escort = $is_pseudo_escort;

		// < /Identifying escort >

		$this->view->gotd = $this->_request->gotd ? true : false;
		$this->view->votd = $this->_request->votd ? true : false;

		// < Escort Status, Packages & Package Purchase Allowance >

		$this->view->escort_packages = $escort_packages = $this->client->call(
			'OnlineBillingV2.checkIfHasPaidPackage',
			array($escort->id)
		);


		$is_package_purchase_allowed = true;

		if ( count($escort_packages) ) {
			$current_package = $escort_packages[0];
			$pending_package = $escort_packages[1];

			// if ($current_package['status'] == self::PACKAGE_STATUS_ACTIVE && !$this->_request->getParam('no_gotd')) {
			// 	$this->_redirect($this->view->getLink('online-billing-gotd'));
			// }

			if ($current_package && ( $current_package['status'] == self::PACKAGE_STATUS_PENDING|| $current_package['package_id'] == 14)) {
				$is_package_purchase_allowed = false;
			} elseif ( $pending_package && $pending_package['status'] == self::PACKAGE_STATUS_PENDING ) {
				$is_package_purchase_allowed = false;
			}
		}

		if (!($escort->escort_status & ESCORT_STATUS_ACTIVE) && !( $escort->escort_status & ESCORT_STATUS_OWNER_DISABLED) ) {
			$is_package_purchase_allowed = false;
		}

		$this->view->is_package_purchase_allowed = $is_package_purchase_allowed;

		// </ Escort Status, Packages & Package Purchase Allowance >


		// < Packages & Day Passes >

		$this->view->packages_list = $this->client->call(
			'OnlineBillingV2.getPackagesList',
			array($user_type, $escort->gender, $is_pseudo_escort, self::$sedcardPackages)
		);
		
		$this->view->day_passes_list = $this->client->call(
			'OnlineBillingV2.getPackagesList',
			array($user_type, $escort->gender, $is_pseudo_escort, self::$dayPassesPackages)
		);

		// </ Packages & Day Passes >
	}

	public function escortAdditionalAreasAction() {
		$this->view->layout()->disableLayout();

		$areas = Model_Statistics::getAllAreas();
		$excluded_areas = array(17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 44, 45);

		foreach($areas as $i => $area) {
			if(!in_array($area->id, $excluded_areas)){
				unset($areas[$i]);
			}
		}

		$this->view->additional_areas = array_values($areas);
	}

	public function escortActivationDateAction() {
        $escort_id = $this->_request->getParam('escort_id', $this->user->escort_data['escort_id']);
		$this->view->layout()->disableLayout();

		$m_escorts = new Model_EscortsV2();
		$this->view->escort = $escort = $m_escorts->get($escort_id, null,true);

		$packages = $this->client->call(
			'OnlineBillingV2.checkIfHasPaidPackage',
			array($escort->id)
		);

		// one running and one upcoming
		if (count($packages) == 2) {
			$this->view->active_package = true;
		}

		// only one package, need to check whether it's upcoming or not
		if (count($packages) == 1) {
			if ($packages[0]['status'] == self::PACKAGE_STATUS_ACTIVE) {
				$this->view->active_package = true;
				$this->view->active_package_expiration = $packages[0]['expiration_date'];
			} else if ($packages[0]['status'] == self::PACKAGE_STATUS_PENDING) {
				$this->view->pending_package = true;
			}
		} else {
			$this->view->active_package = false;
			$this->view->pending_package = false;
		}
	}

	public function escortGotdAction() {
		$this->view->layout()->disableLayout();

		$areas = Model_Statistics::getAllAreas();
		$excluded_areas = array(45, 17, 19, 44, 20, 18, 21, 22, 24, 25, 26, 23);

		foreach($areas as $i => $area) {
			if(!in_array($area->id, $excluded_areas)){
				unset($areas[$i]);
			}
		}

		$this->view->additional_areas = array_values($areas);
	}

	public function escortGotdCalendarAction() {
		$this->view->layout()->disableLayout();

		// < Gotd Calendar >

		$this->view->area_id = $area_id = $this->_request->area_id;
		$this->view->calendar_month = $calendar_month = $this->_request->calendar_month;
		$this->view->selected_dates = $selected_dates = $this->_request->selected_dates;
		$this->view->preselected_dates = $preselected_dates = $this->_request->preselected_dates;
		$this->view->available_dates = $available_dates = $this->_request->available_dates;
		$calendar = new Cubix_GotdCalendar($calendar_month, $area_id);

		// < Gotd Already Booked Dates >

		$booked_gotd_dates = $this->client->call('Billing.getGotdBookedDays');
		$booked_gotd_dates_by_area = $booked_gotd_dates[$area_id];

		// </ Gotd Already Booked Dates >

		$calendar->clearDailyHtml();

		foreach ($booked_gotd_dates_by_area as $d) {
			$calendar->addDailyHtml('booked', $d['date']);
		}

		if ($available_dates) {
			foreach ($available_dates as $s) {
				$calendar->addDailyHtml('package', $s);
			}
		}

		if ($selected_dates) {
			foreach ($selected_dates as $s) {
				$calendar->addDailyHtml('selected', $s);
			}
		}

		if ($preselected_dates) {
			foreach ($preselected_dates as $s) {
				$calendar->addDailyHtml('preselected', $s);
			}
		}

		$calendar->setStartOfWeek('Monday');

		$this->view->calendar = $calendar;

		// </ Gotd Calendar >
	}

	public function gotdAction()
	{
		$m_escorts = new Model_EscortsV2();
		$this->view->escort = $escort = $m_escorts->get($this->user->escort_data['escort_id'], 	null, true);

		$is_pseudo_escort = $this->client->call(
			'OnlineBillingV2.isPseudoEscort',
			array($escort->id)
		);

		$this->view->escort_packages = $escort_packages = $this->client->call(
			'OnlineBillingV2.checkIfHasPaidPackage',
			array($escort->id)
		);

		$current_package = $escort_packages[0];

		if ($current_package && $current_package['status'] == self::PACKAGE_STATUS_ACTIVE) {

			$available_dates = array();

			for ($i = 0; $i < $current_package['period']; $i++) {
				if (strtotime(date()) >= strtotime($current_package['period'][$i])) {
					$available_dates[] = date('Y-m-d',
						strtotime($current_package['date_activated'] . ' + ' . $i . ' days'));
				}
			}

			$this->view->available_dates = $available_dates;
		}
	}


	public function escortVotdAction() {
		$this->view->layout()->disableLayout();

		$areas = Model_Statistics::getAllAreas();
		$excluded_areas = array(45, 17, 19, 44, 20, 18, 21, 22, 24, 25, 26, 23);

		foreach($areas as $i => $area) {
			if(!in_array($area->id, $excluded_areas)){
				unset($areas[$i]);
			}
		}

		$this->view->additional_areas = array_values($areas);
	}

	public function escortVotdCalendarAction() {
		$this->view->layout()->disableLayout();

		// < Gotd Calendar >

		$this->view->area_id = $area_id = $this->_request->area_id;
		$this->view->calendar_month = $calendar_month = $this->_request->calendar_month;
		$this->view->selected_dates = $selected_dates = $this->_request->selected_dates;
		$this->view->preselected_dates = $preselected_dates = $this->_request->preselected_dates;
		$this->view->available_dates = $available_dates = $this->_request->available_dates;
		$calendar = new Cubix_VotdCalendar($calendar_month, $area_id);

		// < Gotd Already Booked Dates >

		$booked_votd_dates = $this->client->call('Billing.getVotdBookedDays');

		$booked_votd_dates_by_area = $booked_votd_dates[$area_id];

		// </ Gotd Already Booked Dates >

		$calendar->clearDailyHtml();

		foreach ($booked_votd_dates_by_area as $d) {
			$calendar->addDailyHtml('booked', $d['date']);
		}

		if ($available_dates) {
			foreach ($available_dates as $s) {
				$calendar->addDailyHtml('package', $s);
			}
		}

		if ($selected_dates) {
			foreach ($selected_dates as $s) {
				$calendar->addDailyHtml('selected', $s);
			}
		}

		if ($preselected_dates) {
			foreach ($preselected_dates as $s) {
				$calendar->addDailyHtml('preselected', $s);
			}
		}

		$calendar->setStartOfWeek('Monday');

		$this->view->calendar = $calendar;

		// </ Gotd Calendar >
	}

	public function votdAction()
	{

		$m_escorts = new Model_EscortsV2();
		$this->view->escort = $escort = $m_escorts->get($this->user->escort_data['escort_id'], 	null, true);

		$is_pseudo_escort = $this->client->call(
			'OnlineBillingV2.isPseudoEscort',
			array($escort->id)
		);

		$this->view->escort_packages = $escort_packages = $this->client->call(
			'OnlineBillingV2.checkIfHasPaidPackage',
			array($escort->id)
		);

		$current_package = $escort_packages[0];


		if ($current_package && $current_package['status'] == self::PACKAGE_STATUS_ACTIVE) {

			$available_dates = array();

			for ($i = 0; $i <= $current_package['period']; $i++) {
				if (strtotime(date()) >= strtotime($current_package['period'][$i])) {
					$available_dates[] = date('Y-m-d',
						strtotime($current_package['date_activated'] . ' + ' . $i . ' days'));
				}
			}

			$this->view->available_dates = $available_dates;
		}
	}

	// < Agency >

	public function agencyIndexAction() {
		if (!$this->user->isAgency()) {
			$this->_redirect($this->view->getLink('online-billing-escort'));
		}

		$user_type = USER_TYPE_AGENCY;

		// < Shopping Cart >
		$this->view->gotd = $this->_request->gotd ? true : false;
		$this->view->votd = $this->_request->votd ? true : false;
		$this->view->cart = $this->_session->cart;

		// </ Shopping Cart >

		// < Agency escorts >

		$agency_id = $this->user->agency_data['agency_id'];
		$escorts = $this->client->call('OnlineBillingV2.getAgencyEscortsA6', array($agency_id));
		foreach($escorts as $i => $escort) {

			$escort = new Model_EscortV2Item($escort);
			$cur_main_photo = $escort->getMainPhoto();

			$escorts[$i]['photo'] = $cur_main_photo->getUrl('ob_mobile_escorts');

			if (($escort['escort_packages'][0] &&
				$escort['escort_packages'][0]['package_id'] == 14) ||
				($escort['escort_packages'][1] &&
				$escort['escort_packages'][1]['package_id'] == 14)) {
				unset($escorts[$i]);
			}

			if($this->_request->gotd && !count($escort['escort_packages'])){
				unset($escorts[$i]);
			}

			if($this->_request->votd && (!count($escort['escort_packages']) || !$escort['has_approved_video']) ){
				unset($escorts[$i]);
			}
		}

		$this->view->escorts = $escorts;
		$cart = $this->_session->cart;
		if ($cart) {
			$this->view->in_cart_escorts = array_keys($cart);
		}



		// </ Agency escorts >
	}

	public function agencyPackageAction() {

		if ($this->user->isEscort()) {
			$this->_redirect($this->view->getLink('online-billing-escort'));
		}
		$this->view->gotd = $this->_request->gotd ? true : false;
		$this->view->votd = $this->_request->votd ? true : false;
		$user_type = USER_TYPE_AGENCY;

		$this->view->escort_id = $escort_id = $this->_request->escort_id;

		if (!$escort_id) {
			$this->_redirect($this->view->getLink('online-billing-agency'));
		}

		if ($this->_session->cart){
			$this->view->existing_cart = $this->_session->cart;
		}

		$this->view->sedcardPackages = self::$sedcardPackages;
		$this->view->dayPassesPackages = self::$dayPassesPackages;
		$this->view->additionalAreaPrice = self::$additionalAreaPrice;
		$this->view->gotdPrice = self::$gotdPrice;
		$this->view->votdPrice = self::$votdPrice;
		$this->view->user = $this->user;

		// < Identifying escort >

		$m_escorts = new Model_EscortsV2();
		$this->view->escort = $escort = $m_escorts->get($escort_id, null, true);

		// < /Identifying escort >

		// < Escort Status, Packages & Package Purchase Allowance >

		$this->view->escort_packages = $escort_packages = $this->client->call(
			'OnlineBillingV2.checkIfHasPaidPackage',
			array($escort->id)
		);
		$m_video = new Model_Video();
		$this->view->has_approved_video = $has_approved_video = $m_video->GetEscortHasApprovedVideo($escort->id);

		$is_package_purchase_allowed = true;

		if ( count($escort_packages) ) {
			$current_package = $escort_packages[0];
			$pending_package = $escort_packages[1];

			if ($current_package && ( $current_package['status'] == self::PACKAGE_STATUS_PENDING|| $current_package['package_id'] == 14)) {
				$is_package_purchase_allowed = false;
			} elseif ( $pending_package && $pending_package['status'] == self::PACKAGE_STATUS_PENDING ) {
				$is_package_purchase_allowed = false;
			}
		}

		if ( $escort->escort_status != ESCORT_STATUS_ACTIVE ) {
			$is_package_purchase_allowed = false;
		}

		$this->view->is_package_purchase_allowed = $is_package_purchase_allowed;

		// </ Escort Status, Packages & Package Purchase Allowance >


		// < Packages & Day Passes >

		$this->view->packages_list = $this->client->call(
			'OnlineBillingV2.getPackagesList',
			array($user_type, $escort->gender, false, self::$sedcardPackages)
		);

		$this->view->day_passes_list = $this->client->call(
			'OnlineBillingV2.getPackagesList',
			array($user_type, $escort->gender, false, self::$dayPassesPackages)
		);

		// </ Packages & Day Passes >
	}

	public function decisionAction() {
		if ($this->user->isEscort()) {
			$this->_redirect($this->view->getLink('online-billing-escort'));
		}

		if (!$this->_session->cart) {
			$this->_redirect($this->view->getLink('online-billing-agency'));
		}

		$this->view->cart = $this->_session->cart;

	}

	// < Agency >


	public function setCartAction() {
		$this->_session->cart = array_filter($this->_session->cart);
		if ($this->_request->isPost()) {

			if ($this->user->isEscort()){
				// Indep escort case
				unset($this->_session->cart);
				$this->_session->cart[] = $this->_request->cart;

			} else {
				// Agency case
				if (count($this->_request->cart) == 0){
					$this->_session->cart = array();
				} elseif (count($this->_session->cart) == 0) {
					$this->_session->cart = $this->_request->cart;
				} else {
					if($this->_request->page == 'select-package'){
						$escort_id = array_keys($this->_request->cart)[0];
						if (in_array($escort_id, array_keys($this->_session->cart))) {
							unset($this->_session->cart[$escort_id]);
						}

						$this->_session->cart[$escort_id] = $this->_request->cart[$escort_id];
					} else{
						$this->_session->cart = $this->_request->cart;
					}
				}
			}
			die(json_encode(array('success' => true, 'cart' => $this->_session->cart)));
		}

		die(json_encode(array('success' => false)));
	}

	public function paymentAction() {
		if (!$this->_session->cart){
			if ($this->user->isAgency()) {
				$this->_redirect($this->view->getLink('online-billing-agency'));
			} else {
				$this->_redirect($this->view->getLink('online-billing-escort'));
			}
		}

        $_model_payments = new Model_PaymentMethodes();
        $this->view->activ_payments =  $_model_payments->getActivePaymentMethodes(Cubix_Application::getId());

		// $cache = Zend_Registry::get('cache');
		// $counter = $cache->load('card_gateway_show_counter');
		// if ($counter) {
		// 	$this->view->alternative_gateway = ($counter%2 == 0) ? true : false;
		// 	$cache->save($counter + 1, 'card_gateway_show_counter');
		// } else {
		// 	$cache->save(1, 'card_gateway_show_counter');
		// 	$this->view->alternative_gateway = false;
		// }

		$this->view->alternative_gateway = true;
		
		$this->view->additionalAreaPrice = self::$additionalAreaPrice;
		$this->view->gotdPrice = self::$gotdPrice;
		$this->view->votdPrice = self::$votdPrice;
		$this->view->cryptoGotdPrice = self::$cryptoGotdPrice;
		$this->view->cryptoVotdPrice = self::$cryptoVotdPrice;

		$this->view->cart = $cart = $this->_session->cart;
		if ($this->user->isAgency()) {
			if (count($cart) == 1 && ((count(reset($cart)['areas']) == 0 && count(reset($cart)['gotds']) == 0 && count(reset($cart)['votds']) == 0 &&
				in_array(reset($cart)['package']['packageId'], self::$dayPassesPackages)) ||
				(!reset($cart)['package'] && (count(reset($cart)['gotds']) == 1 xor count(reset($cart)['votds']) == 1)))) {
				$this->view->m_payment_available = true;
			} else {
				$this->view->m_payment_available = false;
			}
		} else {
			if (count($cart) == 1 && ((count($cart[0]['areas']) == 0 && count($cart[0]['gotds']) == 0 && count($cart[0]['votds']) == 0 && in_array($cart[0]['package']['packageId'], self::$dayPassesPackages)) ||
				(!$cart[0]['package'] && (count($cart[0]['gotds']) == 1 xor count($cart[0]['votds']) == 1)))) {
				$this->view->m_payment_available = true;
			} else {
				$this->view->m_payment_available = false;
			}
		}

	}

	public function checkoutAction() {

		if (!$this->_request->isPost()) die();

		// < Payment Type Detection >

		$payment_method = $this->_request->payment_method;

		// </ Payment Type Detection >

		$cart = $this->_session->cart;

		if (!$cart || !$payment_method) die();

		if (!count($cart) && $this->user->isAgency()) {
			$this->_redirect($this->view->getLink('online-billing-agency'));
		} elseif (!count($cart) && $this->user->isEscort()){
			$this->_redirect($this->view->getLink('online-billing-escort'));
		}

		/* 
			as fucken mmgbill has limitation for reference we have to make hash shorter
			will work fine till 2038-12-24 09:45:35 -- zzzzzz after will become 7 symbols 
		*/
		$hash  = base_convert(time(), 10, 36);
		
		foreach ($cart as $key => $item) {
			$products_data = array();

			// < Areas >

			$areas = $item['areas'];

			if (count($areas)){
				foreach ($areas as $area) {
					$products_data['optional_products'] = array(18);
					$products_data['additional_areas'][] = $area['a'];
				}
			}

			// </ Areas >

			// < Gotds >
			$gotds_unsorted = $item['gotds'];
			$gotds = array();

			foreach($gotds_unsorted as $k => $v)
			{
			   $gotds[$v['a']][$k] = strtotime($v['d']);
			}

			ksort($gotds, SORT_NUMERIC);

			if (count($gotds)) {
				foreach ($gotds as $a => $d) {
					$r = $this->client->call('Billing.bookGotd', array(
							$this->user->isAgency() ? $key : $item['escort_id'],
							$a,
							$d
						)
					);

					if ($r) {
						$products_data['gotd'][] = $r;
						$self_gotd[] = $r;
					}
				}
			}

			// </ Gotds >

			// < Votds >
			$votds_unsorted = $item['votds'];
			$votds = array();

			foreach($votds_unsorted as $k => $v)
			{
			   $votds[$v['a']][$k] = strtotime($v['d']);
			}

			ksort($votds, SORT_NUMERIC);

			if (count($votds)) {
				foreach ($votds as $a => $d) {
					$r = $this->client->call('Billing.bookVotd', array(
							$this->user->isAgency() ? $key : $item['escort_id'],
							$a,
							$d
						)
					);

					if ($r) {
						$products_data['votd'][] = $r;
						$self_votd[] = $r;
					}
				}
			}

			// </ Votds >

			// < Activation Date >

			if ($item['activation']) {
				
				if (!$item['activation']['date']) {
					$products_data['activation_date'] = '';
				} else {
					$products_data['activation_date'] =
						strtotime($item['activation']['date'] . ' ' . $item['activation']['time']);
				}
			}

			// </ Activation Date >

			// < Preparing Post Data >

			$post_data[] = array(
				'user_id' => $this->user->id,
				'escort_id' => $this->user->isAgency() ?
					$key :
					$item['escort_id'],
				'agency_id' => $this->user->isAgency() ?
					$this->user->agency_data['agency_id'] :
					null,
				'hash' => $hash,
				'package_id' => $item['package']['packageId'],
				'data' => serialize($products_data),
                'is_mobile' => true
			);

			// </ Preparing Post Data >
		}

        foreach ($post_data as $shoppingCartItem) {
            if (!intval($shoppingCartItem['package_id']) && preg_match('[gotd|votd]', $shoppingCartItem['data']) === false) {
                echo json_encode(array('status' => 'error', 'message' => 'Package Not found!'));
                exit;
            }
        }

		// < Case A :: Only 1 Gotd is being puchased with ivr >
		// 
		if (count($post_data) == 1 && !$post_data['package_id'] && count($self_gotd) == 1 && count($self_votd) == 0 &&
			$payment_method == 'm') {
			echo json_encode(array(
				"status" => 'success',
				"url" => $this->getIvrGotdUrl(self::$gotdPrice, $self_gotd[0]) )
			);

			die();
		}

		// < Case A1 :: Only 1 Votd is being puchased with ivr >
		// 
		if (count($post_data) == 1 && !$post_data['package_id'] && count($self_votd) == 1 && count($self_gotd) == 0 &&
			$payment_method == 'm') {
			echo json_encode(array(
				"status" => 'success',
				"url" => $this->getIvrVotdUrl(self::$votdPrice, $self_votd[0]) )
			);

			die();
		}

		// < Case B :: All other cases
		$is_crypto = $payment_method == 'cc_coinpass' ? true : false;
		$amount = $this->client->call('OnlineBillingV2.addToShoppingCart', array(
			$post_data,
			$this->user->id,
			$this->user->isAgency() ? USER_TYPE_AGENCY : USER_TYPE_SINGLE_GIRL,
			$hash,
			$is_crypto
			)
		);

		foreach($cart as $item) {
			if ( !$item['package']['packageId'] && count($item['gotds']) ) {
				$gotd_price = $is_crypto ? self::$cryptoGotdPrice : self::$gotdPrice;
				$amount += $gotd_price * count($item['gotds']);
			}
		}

		foreach($cart as $item) {
			if ( !$item['package']['packageId'] && count($item['votds']) ) {
				$votd_price = $is_crypto ? self::$cryptoVotdPrice : self::$votdPrice;
				$amount += $votd_price * count($item['votds']);
			}
		}

		######## free 5 days for covid-19 march active girls
		######## https://sceonteam.atlassian.net/browse/A6-94
		extract($cart[0]);

		if($package['packageId'] == '26' && $amount == 0){

			if($this->user['escort_data']['escort_id'] != $escort_id){
				die(json_encode(array('success' => false)));
			}
			
			$result = $this->client->call('OnlineBillingV2.activate_free_package_a6', array( $package['packageId'], $escort_id, $hash));

			if($result){
				//$result = $this->client->call('OnlineBillingV2.activate_5_days', array($escort_id));
				$result = Model_EscortsV2::activate_5_days($escort_id);
				echo json_encode(array( "status" => 'success', "url" => $this->view->getLink('online-billing-payment-success').'?free_5_days=1' )); die;
			}else{
				die(json_encode(array('success' => false)));
			}
		}
		############


		if ( $payment_method == 'm' ) {
			$reference = 'SC3_' . $this->user->id . '_' . $hash;
			echo json_encode(array(
				"status" => 'success',
				"url" => $this->getIvrUrl($amount, $reference)
				)
			);

			die();
		} elseif ($payment_method == 'cc_postfinance') {
			$postfinance_payment = new Model_Postfinance();
			
			$data = array(
				'PSPID' => $postfinance_payment->PSPID,
				'ORDERID' =>  'SC3-' . $this->user->id . '-' . $hash,
				'AMOUNT' => $amount * 100,
				'LANGUAGE' => 'de_DE',
				'CURRENCY' => $postfinance_payment->currency
			);

			$data['SHASIGN'] = $postfinance_payment->generateChecksum($data);
			
			try {
				$payment_from = '<form method="post" action="' . $postfinance_payment->submitUrl . '"id=form1 name=form1>';

				foreach($data as $k => $v) {
				    $payment_from .= '<input type="hidden" name="' . $k . '" value="'. $v . '">';
				}

				$payment_from .= '<input type="submit" value="" id=submit2 name=submit2>';
				$payment_from .= '</form>';

			} catch(Exception $e) {
				$message = $e->getMessage();
				var_dump($message); die();
			}

			die(json_encode(array('status' => 'success', 'form' => $payment_from), JSON_HEX_TAG | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES ));

		} elseif ($payment_method == 'cc_twispay') {

            /*if ($this->user->id == 48322) {
                $amount = 1;
            }*/

            $paymentConfigs = Zend_Registry::get('payment_config');
            $twispayConfigs = $paymentConfigs['twispay'];

            $data = array(
                'siteId' => intval($twispayConfigs['siteid']),
                'cardTransactionMode' => 'authAndCapture',
                'backUrl' => APP_HTTP . '://' . $_SERVER['HTTP_HOST'] . '/online-billing/twispay-response',
                'invoiceEmail' => '',
                'customer' => [
                    'identifier' => 'user-' . $this->user->id,
                    'firstName' => $this->user->username,
                    'username' => $this->user->username,
                    'email' => $this->user->email,
                ],
                'order' => [
                    'orderId' => 'SC3-' . $this->user->id . '-' . $hash,
                    'type' => 'purchase',
                    'amount' => $amount,
                    'currency' => $twispayConfigs['currency'],
                    'description' => 'Self Checkout And6.com',
                ]
            );

            // Store the gateway token for backend to get details about the transaction
            // using this information
            $token = json_encode(['externalOrderId' => $data['order']['orderId']]);
            $isOrderCreated = $this->client->call('OnlineBillingV2.storeToken', array($token, $this->user->id, 'twispay'));

            if (!$isOrderCreated) {
                die(json_encode([
                    'status' => 'error',
                    'error'  => 'Twispay is not available at the moment',
                ]));
            }

            $base64JsonRequest = Cubix_Twispay_TwispayApi::getBase64JsonRequest($data);
            $base64Checksum = Cubix_Twispay_TwispayApi::getBase64Checksum($data, $twispayConfigs['key']);

            try {

                $paymentFrom = "
                        <form id=\"twispay-payment-form\" action='{$twispayConfigs['url']}' method='post' accept-charset='UTF-8'>
                            <input type='hidden' name='jsonRequest' value = \"{$base64JsonRequest}\" >
                            <input type='hidden' name='checksum' value = \"{$base64Checksum}\" >
                            <input type = \"submit\" value = \"Pay\" >
                        </form > ";

            } catch(Exception $e) {
                $message = $e->getMessage();
                var_dump($message); die();
            }

            die(json_encode(array('status' => 'success', 'form' => $paymentFrom), JSON_HEX_TAG | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES ));
		} elseif ($payment_method == 'cc_epg') {

				$epg_payment = new Model_EpgGateway();
				$reference = 'SC3-' . $this->user->id . '-' . $hash;

				$fields = $epg_payment->getFieldsForAuth(
					$reference,
					$amount,
					'https://m.and6.com/online-billing/epg-response'
				);

				$this->client->call('OnlineBillingV2.storeToken', array(
					$fields['Token'],
					$this->user->id)
				);


				echo json_encode(array(
					"status" => 'success',
					"url" => $fields['RedirectUrl']
				));

				die();
		}elseif ($payment_method == 'cc_faxin') {
			$amount = 100 * $amount;
			$postdata = http_build_query(array('sysorderid' => $hash, 'amount' => $amount ));

			$opts = array('http' => array(
				'method'  => 'POST',
				'header'  => 'Content-type: application/x-www-form-urlencoded',
				'content' => $postdata
			));

			//test url - paystaging.faxin.ch
			$context  = stream_context_create($opts);
			$faxin_result = file_get_contents('https://pay.faxin.ch/payment', false, $context);
			$result  = json_decode($faxin_result);
			
			if($result->success){
				$result->status = 'success';
				$reference = 'SC3-' . $this->user->id . '-' . $hash;
				$this->client->call('OnlineBillingV2.storeFaxin', array($this->user->id, $result->paymentid, $reference, $amount, 0));

				echo json_encode($result);die;
			}else{
				 die(json_encode([
                'status' => 'error',
                'error'  => 'FAXIN payments is not available at the moment',
            	]));
			}	
		}
		elseif ($payment_method == 'cc_2000charge') {
			
			$first_name = trim(preg_replace('#[^a-zA-Z]#', '', $this->user->username)); 
			$last_name = trim(preg_replace('#[^a-zA-Z]#', ' ', $this->user->chat_info['nickName']));
			
			if(empty($first_name)){
				$first_name = 'NAME';
			}

			if(empty($last_name)){
				$last_name = 'LASTNAME';
			}
			
			$reference = 'SC3-' . $this->user->id . '-' . $hash;

			$customer = new Cubix_2000charge_Model_Customer();
			$customer->setCountry("CH");
			$customer->setFirstName($first_name);
			$customer->setLastName($last_name);
			$customer->setEmail($this->user->email);
			
			$payment = new Cubix_2000charge_Model_Payment();
			$payment->setPaymentOption("paysafe");
			$payment->setHolder($first_name.' '.$last_name);

			$transaction = new Cubix_2000charge_Model_Transaction();
			$transaction->setCustomer($customer);
			$transaction->setPayment($payment);
			$transaction->setAmount($amount * 100);
			$transaction->setCurrency("CHF");
			$transaction->setIPAddress(Cubix_Geoip::getIP());
			$transaction->setMerchantTransactionId($reference);

			$host = 'https://' . $_SERVER['SERVER_NAME'];
			
			$redirectUrls = new Cubix_2000charge_Model_RedirectUrls();
			$redirectUrls->setReturnUrl($host . $this->view->getLink('online-billing-payment-success'));
			$redirectUrls->setCancelUrl($host . $this->view->getLink('online-billing-payment-failure'));
			$transaction->setRedirectUrls($redirectUrls);
			
			$res = Cubix_2000charge_Transaction::post($transaction);
			
			$this->client->call('OnlineBillingV2.storeToken', array($res->id, $this->user->id, '2000charge'));
			echo json_encode(array( 'status' => 'success', 'url' => $res->redirectUrl )); die;
		}
		elseif($payment_method == 'cc_coinpass') {
						
			$order_id = $this->client->call('OnlineBillingV2.saveCoinsomeRequest', array($this->user->id, 'SC3', $amount, $hash));
			/*if(APPLICATION_ENV == 'development'){
				$order_id .= 't';
			}*/
			$coinsome_model = new Cubix_Coinsome();

			$paymentFrom = '
					<form id="payment-form" method="POST" name="criptoForm" action="'. $coinsome_model->generateFormUrl($order_id) .'">
						<input type="hidden" name="amount" value="'. $amount .'" />
						<input type="submit" value="">	
					</form>';

			die(json_encode(array('status' => 'success', 'form' => $paymentFrom), JSON_HEX_TAG | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES ));
		}
		else {
			die(
				__('card_payment_not_specified')
			);
		}
	}

	private function getIvrUrl($amount, $client_identifier)	{
		if (!$amount || !$client_identifier ) {
			return $this->view->getLink('private-v2-advertise-now');
		}

		$amount *= 100;

		$conf = Zend_Registry::get('ivr');

		$urls = array(
			'base_url'	=> 'http://ivr.truesenses.com/?cmd=new&acc=' . $conf['acc'] . '&aid=' . $conf['aid'] . '&amo=' . $amount . '&cur=chf&ser=' . $conf['acc'] . '&lan=de&cid=' . $client_identifier,
			'success_url'	=> 'srl=' . 'http://' . $_SERVER['SERVER_NAME'] . $this->view->getLink('online-billing-success'),
			'failure_url'	=> 'frl=' . 'http://' . $_SERVER['SERVER_NAME'] . $this->view->getLink('online-billing-failure'),
			'cancel_url'	=> 'crl=' . 'http://' . $_SERVER['SERVER_NAME'] . $this->view->getLink('online-billing-select-escort')
		);

		$url = implode('&', $urls);

		return $url;
	}



	private function getIvrGotdUrl($amount, $client_identifier)
	{

		$amount *= 100;
		$conf = Zend_Registry::get('ivr');

		$urls = array(
			'base_url'	=> 'https://ivr.truesenses.com/?cmd=new&acc=' . $conf['acc'] . '&aid=' . $conf['aid'] . '&amo=' . $amount . '&cur=chf&ser=' . $conf['accGotd'] . '&lan=de&cid=' . $client_identifier,
			'success_url'	=> 'srl=' . 'http://' . $_SERVER['SERVER_NAME'] . $this->view->getLink('online-billing-v4-success'),
			'failure_url'	=> 'frl=' . 'http://' . $_SERVER['SERVER_NAME'] . $this->view->getLink('online-billing-v4-failure'),
			'cancel_url'	=> 'crl=' . 'http://' . $_SERVER['SERVER_NAME'] . $this->view->getLink('online-billing-v4-select-escort')
		);
		$url = implode('&', $urls);

		return $url;
	}

	private function getIvrVotdUrl($amount, $client_identifier)
	{

		$amount *= 100;
		$conf = Zend_Registry::get('ivr');

		$urls = array(
			'base_url'	=> 'https://ivr.truesenses.com/?cmd=new&acc=' . $conf['acc'] . '&aid=' . $conf['aid'] . '&amo=' . $amount . '&cur=chf&ser=' . $conf['accGotd'] . '&lan=de&cid=' . $client_identifier,
			'success_url'	=> 'srl=' . 'http://' . $_SERVER['SERVER_NAME'] . $this->view->getLink('online-billing-v4-success'),
			'failure_url'	=> 'frl=' . 'http://' . $_SERVER['SERVER_NAME'] . $this->view->getLink('online-billing-v4-failure'),
			'cancel_url'	=> 'crl=' . 'http://' . $_SERVER['SERVER_NAME'] . $this->view->getLink('online-billing-v4-select-escort')
		);
		$url = implode('&', $urls);

		return $url;
	}

	public function epgResponseAction()	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;

		$token = $req->Token;
		if ( ! $token ) $this->_redirect('/');
		$epg_payment = new Model_EpgGateway();
		$result = $epg_payment->getTransactionStatus($token);


		if ($result['ResultStatus'] == "OK") {
			$this->_redirect($this->view->getLink('online-billing-payment-success'));
		} else {
			$this->_redirect($this->view->getLink('online-billing-payment-failure'));
		}
	}

    public function twispayResponseAction()
    {
        $this->view->layout()->disableLayout();
        $twispayConfigs = Zend_Registry::get('payment_config')['twispay'];

        $decryptedData = Cubix_Twispay_TwispayApi::decryptIpnResponse($this->_request->opensslResult, $twispayConfigs['key']);

        if ($decryptedData['transactionStatus'] == 'in-progress' || $decryptedData['transactionStatus'] == 'complete-ok') {
            $this->_redirect($this->view->getLink('online-billing-payment-success'));
        } else {
            $this->_redirect($this->view->getLink('online-billing-payment-failure'));
        }
    }

	public function successAction() {
	}

	public function failureAction()	{
	}
	
	
	public function btcSuccessAction()
	{
		
		$this->view->layout()->disableLayout();
		$req_json = file_get_contents('php://input');
		$payResponse = json_decode($req_json);
		$order_id = intval($payResponse->orderId);
		
		$req = serialize($_REQUEST);
		Cubix_Email::send('badalyano@gmail.com', 'BTC REDIRECT MOBILE', $req, TRUE);
		/*if($order_id == 0 ){// || $payResponse->status == Cubix_Coinsome::ORDER_PENDING){ 
			header("HTTP/1.1 200 OK");
			die;
		}*/		
		
		//if($req->bitcoinpay-status){
			$this->_redirect($this->view->getLink('online-billing-payment-success'));
		/*} 
		else {
			 $this->_redirect($this->view->getLink('online-billing-payment-failure'));
		}*/
	}
	
}
