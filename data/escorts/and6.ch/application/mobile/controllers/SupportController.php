<?php

class Mobile_SupportController extends Zend_Controller_Action
{
	public function init()
	{
		$this->_request->setParam('no_tidy', true);
		
		$anonym = array();

		$this->user = Model_Users::getCurrent();
		if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}

		$this->view->layout()->setLayout('mobile-private');

		$this->view->user = $this->user;

		$this->client = Cubix_Api::getInstance();
		$this->model = new Model_Support();
	}

	public function indexAction()
	{
		$this->ajaxTicketsListAction();
		$this->view->layout()->enableLayout();
	}

	public function ticketAction()
	{
		$this->view->hideBack = true;

		if ( $this->_request->isPost() ){

		
			$req = $this->_request;
			$user = Model_Users::getCurrent();

			$ticket_id = intval($req->getParam('id'));
			$message = $req->getParam('message');

			//if ( magic_quotes_gpc() ) {
				//$message = stripslashes($message);
			//}

			$validator = new Cubix_Validator();

			if ( ! strlen($message) ) {
				$validator->setError('message', 'Message Required!');
			}

			$data = array('user_id' => $user->id, 'ticket_id' => $ticket_id);
			if ( ! $this->model->isUserTicket($data) ) {
				$this->_response->setRedirect($this->view->getLink('private-v2-support'));
				return;
			}

			if ( $validator->isValid() ) {

				$data = array(
					'ticket_id' => $ticket_id,
					'comment' => $message,
				);

				$this->model->addCommentStandart($data);


				$sales = $this->user->getSalesPerson();
				$conf = Zend_Registry::get('feedback_config');
				$email = $conf['emails']['support'];

				if ( $sales ) {
					if ( strlen($sales['email']) ) {
						$email = $sales['email'];
					}
				}

				Cubix_Email::sendTemplate('admin_support_ticket_comment_added', $email, array(
					'username' => $user->username,
					'ticket_id' => $ticket_id,
					'message' => $message
				));
			}
		}


		$req = $this->_request;
		$ticket_id = intval($this->_request->getParam('id'));

		$user = Model_Users::getCurrent();

		$data = array('user_id' => $user->id, 'ticket_id' => $ticket_id);

		if ( ! $this->model->isUserTicket($data) ) {
			$this->_response->setRedirect($this->view->getLink('private-support'));
			return;
		}

		$this->view->ticket = $this->model->getStandart($ticket_id);

		$this->ajaxCommentsListAction();
		$this->view->layout()->enableLayout();
	}


	public function addTicketAction(){
		$this->view->hideBack = true;

		$sales_info = $this->user->getSalesPerson();
		$this->view->sales_user_id = $sales_info['id'];

		if ( $this->_request->isPost() ){
		
			$lang = Cubix_I18n::getLang();
			$this->_request->setParam('lang_id', $lang);
			$req = $this->_request;
			$user = Model_Users::getCurrent();
	
			$backend_user_id = $req->getParam('sale_person');
			$subject = $req->getParam('subject', null);
			$message = $req->getParam('message');

			//if ( magic_quotes_gpc() ) {
				//$subject = stripslashes($subject);
				//$message = stripslashes($message);
			//}

			$validator = new Cubix_Validator();

			if ( ! strlen($subject) ) {
				$validator->setError('subject', 'Subject Required!');
			}

			if ( ! strlen($message) ) {
				$validator->setError('message', 'Message Required!');
			}
			/*if ( intval($backend_user_id) < 1  ) {
				$validator->setError('sale_person', 'Sale Person Required!');
			}*/

			if ( $validator->isValid() ) {

				$data = array(
					'user_id' => $user->id,
					'subject' => $subject,
					'message' => $message,
					'status' => Model_Support::STATUS_TICKET_OPENED,
					'application_id' => Cubix_Application::getId(),
					'backend_user_id' => $backend_user_id,
					'status_progress' => 1,
				);
				
				$ticket_id = $this->model->saveStandart($data);
				
				
				$sales = $this->user->getSalesPersonById($backend_user_id);

				Cubix_Email::sendTemplate('support_ticket_submited', $user->email, array(
					'username' => $user->username
				));

				$conf = Zend_Registry::get('feedback_config');
				$email = $conf['emails']['support'];

				if ( $sales ) {
					if ( strlen($sales['email']) ) {
						$email = $sales['email'];
					}
				}

				Cubix_Email::sendTemplate('admin_support_ticket_submited', $email, array(
					'username' => $user->username,
					'ticket_id' => $ticket_id,
					'subject' => $subject,
					'message' => $message
				));

				$req->setParam('ticket_id', $ticket_id);
				$this->ajaxSuccessAction();

				$this->_redirect( $this->view->getLink('private-support', array('success' => 1)) );

				$this->_helper->viewRenderer->setScriptAction('/ajax-success');
				return;
			}else{

				$result = $validator->getStatus();

				$dat['subject'] = $subject;
				$dat['message'] = $message;

				$this->view->data = $dat;

				$this->view->errors = $result['msgs'];

			}
			

		}

		
	}

	public function ajaxTicketsListAction()
	{
		$this->view->layout()->disableLayout();
		if ( $this->_request->lang ) {
			$this->_request->setParam('lang_id', $this->_request->lang);
		}

		$user = $this->user;

		$tickets = $this->model->getAll($user->id);

		if ( count($tickets['opened']) > 0 ) {
			foreach($tickets['opened'] as $k => $ticket) {
				$tickets['opened'][$k]['comments'] = $this->model->getComments($ticket['id']);
			}
		}
		
		$this->view->tickets = $tickets;
		//print_r($tickets);
	}

	public function ajaxTicketFormAction()
	{
		$this->view->layout()->disableLayout();
		$this->_request->setParam('lang_id', $this->_request->lang);
		//$this->view->sales = $this->user->getSalesAdminPersons();
		$sales_info = $this->user->getSalesPerson();
		$this->view->sales_user_id = $sales_info['id'];
	}

	public function ajaxAddTicketAction()
	{
		$this->view->layout()->disableLayout();
		$this->_request->setParam('lang_id', $this->_request->lang);
		$req = $this->_request;
		$user = $this->user;

		$backend_user_id = $req->getParam('sale_person');
		$subject = $req->getParam('subject', null);
		$message = $req->getParam('message');

		//if ( magic_quotes_gpc() ) {
			$subject = stripslashes($subject);
			$message = stripslashes($message);
		//}
		
		$validator = new Cubix_Validator();

		if ( ! strlen($subject) ) {
			$validator->setError('subject', 'Subject Required!');
		}

		if ( ! strlen($message) ) {
			$validator->setError('message', 'Message Required!');
		}
		/*if ( intval($backend_user_id) < 1  ) {
			$validator->setError('sale_person', 'Sale Person Required!');
		}*/

		if ( $validator->isValid() ) {

			$data = array(
				'user_id' => $user->id,
				'subject' => $subject,
				'message' => $message,
				'status' => Model_Support::STATUS_TICKET_OPENED,
				'application_id' => Cubix_Application::getId(),
				'backend_user_id' => $backend_user_id
			);
			
			$ticket_id = $this->model->save($data);

			$sales = $this->user->getSalesPersonById($backend_user_id);
			
			Cubix_Email::sendTemplate('support_ticket_submited', $user->email, array(
				'username' => $user->username
			));
			
			$conf = Zend_Registry::get('feedback_config');
			$email = $conf['emails']['support'];

			if ( $sales ) {
				if ( strlen($sales['email']) ) {
					$email = $sales['email'];
				}
			}

			Cubix_Email::sendTemplate('admin_support_ticket_submited', $email, array(
				'username' => $user->username,
				'ticket_id' => $ticket_id,
				'subject' => $subject,
				'message' => $message
			));
			
			$req->setParam('ticket_id', $ticket_id);
			$this->ajaxSuccessAction();
			$this->_helper->viewRenderer->setScriptAction('/ajax-success');
			return;
		}

		die(json_encode($validator->getStatus()));
	}


	public function ajaxSuccessAction()
	{
		$this->view->layout()->disableLayout();
		$this->_request->setParam('lang_id', $this->_request->lang);
		$req = $this->_request;

		$this->view->ticket_id = $req->getParam('ticket_id');
	}

	public function ajaxAddCommentAction()
	{
		$this->view->layout()->disableLayout();
		$this->_request->setParam('lang_id', $this->_request->lang);
		$req = $this->_request;
		$user = $this->user;

		$ticket_id = intval($req->getParam('id'));
		$message = $req->getParam('message');

		//if ( magic_quotes_gpc() ) {
			$message = stripslashes($message);
		//}

		$validator = new Cubix_Validator();

		if ( ! strlen($message) ) {
			$validator->setError('message', 'Message Required!');
		}

		$data = array('user_id' => $user->id, 'ticket_id' => $ticket_id);
		if ( ! $this->model->isUserTicket($data) ) {
			$this->_response->setRedirect($this->view->getLink('private-v2-support'));
			return;
		}

		if ( $validator->isValid() ) {

			$data = array(
				'ticket_id' => $ticket_id,
				'comment' => $message,
			);

			$this->model->addComment($data);


			$sales = $this->user->getSalesPerson();
			$conf = Zend_Registry::get('feedback_config');
			$email = $conf['emails']['support'];

			if ( $sales ) {
				if ( strlen($sales['email']) ) {
					$email = $sales['email'];
				}
			}

			Cubix_Email::sendTemplate('admin_support_ticket_comment_added', $email, array(
				'username' => $user->username,
				'ticket_id' => $ticket_id,
				'message' => $message
			));
		}

		die(json_encode($validator->getStatus()));
	}

	public function ajaxCommentsListAction()
	{
		$this->view->layout()->disableLayout();
		if ( $this->_request->lang ) {
			$this->_request->setParam('lang_id', $this->_request->lang);
		}

		$user = $this->user;

		$ticket_id = $this->_request->id;

		$comments = $this->model->getComments($ticket_id);

		$this->view->comments = $comments;
	}
}