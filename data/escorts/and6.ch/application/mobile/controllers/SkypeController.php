<?php

class Mobile_SkypeController extends Zend_Controller_Action
{
	public function init()
	{
		 $this->model = new Model_Skype();
		 $this->user = Model_Users::getCurrent();
	}

	public function updateAction()
	{
		if (is_null($this->_getParam('ajax')) ) {
			$form = new Cubix_Form_Data($this->_request);
			$fields = array(
				'escort_id' => 'int-nz',
				'30min' => 'int-nz',
				'60min' => 'int-nz',
				'escort_skype_username' => '',
				'paxum_email'=> '',
				'skype_payment_option'=> '',
				'bank_wire_beneficiary_name'=> '',
				'bank_wire_city'=> '',
				'bank_wire_iban'=> '',
				'bank_wire_bic'=> '',
				'status'=> ''
			);

			$form->setFields($fields);

			$data = $form->getData();
			
			//permission check
			if($this->user->isAgency()){
				$agency = $this->user->getAgency();
				if(!$agency->hasEscort( $data['escort_id'] )){ 
					die('access denied');
				}
			}
			else{
				$escort = $this->user->getEscort();
				if($escort->id != $data['escort_id']){
					die('access denied');
				}
			}

			$validator = new Cubix_Validator();
			
			if($data['status'] == 'on'){
				$data['status'] = 1;
				if ( ! strlen( $data['escort_skype_username'] ) ) {
					$validator->setError('escort_skype_username', 'Skype username is Required!');
				}

				if ( ! strlen( $data['skype_payment_option'] ) ) {
					$validator->setError('skype_payment_option', 'Option is Required!');
				}
				
				

				if( strlen( $data['skype_payment_option'] == 'paxum') ){
					if ( ! strlen( $data['paxum_email'] ) ) {
						$validator->setError('paxum_email', 'Paxum email is Required!');
					}
					elseif ( ! preg_match( '/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['paxum_email'] )  ) {
						$validator->setError('paxum_email', 'Paxum email is not valid!');
					}
				}
				elseif( strlen( $data['skype_payment_option'] == 'bank') ){
					if ( ! strlen( $data['bank_wire_beneficiary_name'] ) ) {
						$validator->setError('bank_wire_beneficiary_name', 'bank_beneficiary_name username is Required!');
					}
					if ( ! strlen( $data['bank_wire_city'] ) ) {
						$validator->setError('bank_wire_city', 'City  is Required!');
					}
					if ( ! strlen( $data['bank_wire_iban'] ) ) {
						$validator->setError('bank_wire_iban', 'IBAN  is Required!');
					}
					if ( ! strlen( $data['bank_wire_bic'] ) ) {
						$validator->setError('bank_wire_bic', 'BIC  is Required!');
					}
				}

			}else{
				$data['status'] = 0;
			}

			if ( ! strlen($data['escort_id']) ) {
				$validator->setError('subject', 'id is Required!');
			}

			if ( ! strlen($data['30min']) ) {
				$validator->setError('30min', ' Price is Required!');
			}

			if ( ! strlen($data['60min']) ) {
				$validator->setError('60min', ' Price is Required!');
			}
			
			if ( $validator->isValid() ) {
			 $status = $this->model->update($data);
				die(json_encode($status));
			}else{
				die(json_encode($validator->getStatus()));
			}

		}
		else {
			$this->view->layout()->disableLayout();
			var_dump(124134);die;
		}
		die;
	}

	public function getAgencyEscortSkypeSettingsAction(){
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$agency = $this->user->getAgency();

		$escort_id = intval($this->_getParam('escort_id'));
		$status = $agency->hasEscort( $escort_id );
		if($status === false)die('access denied #9667890');
		
		$this->view->layout()->disableLayout();
		$this->view->skype = $client->call('Skype.get', array( $escort_id ) );
		$this->view->earned_skype_sum_agency = $client->call('Skype.earnedsumagency', array(  $agency->id ) );
		
		$this->view->earned_skype_sum = $client->call('Skype.earnedsum', array(  $escort_id ) );
			
		if(is_null($this->view->skype)){
			$this->view->skype['escort_id'] = $escort_id;
			$this->view->skype['payment_option'] =  'paxum';
		}
	}
	
}