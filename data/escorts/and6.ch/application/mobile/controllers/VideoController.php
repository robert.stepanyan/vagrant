<?php


class Mobile_VideoController extends Zend_Controller_Action {

	private $user;
	protected $config;
	
	public function init()
	{
		$this->user = Model_Users::getCurrent();
		$this->view->layout()->setLayout('mobile-video'); 			 
		if (!$this->user)
		{
			$this->_redirect($this->view->getLink('signin'));
			return;
		}
		elseif($this->user['user_type'] != 'agency' && $this->user['user_type'] != 'escort' )
		{
			$this->_redirect($this->view->getLink('private'));
		}

		$this->client = Cubix_Api::getInstance();
		$this->app_id = Cubix_Application::getId();
		$cache = Zend_Registry::get('cache');
		$this->view->user = $this->user;

		$cache_key =  'v2_user_pva_' . $this->user->id;

		if ( $this->user->isAgency() ) {

			if ( ! $agency = $cache->load($cache_key) ) {
				$agency = $this->user->getAgency();
				$cache->save($agency, $cache_key, array(), 300);
			}

			$this->agency = $this->view->agency = $agency;
			$escort_id = intval($this->_getParam('escort'));

			if ( 1 > $escort_id ) die;
			$client = new Cubix_Api_XmlRpc_Client();		
			if ( ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
				die;
			}
			$this->view->escort_id = $this->escort_id = $escort_id;
			
		}
		else if ( $this->user->isEscort() ) {

			if ( ! $escort = $cache->load($cache_key) ) {
				$escort = $this->user->getEscort();
				$cache->save($escort, $cache_key, array(), 300);
			}
			$this->escort = $this->view->escort = $escort;
			$this->view->escort_id = $this->escort_id = $this->escort->id;
		}
	}

	public function testAction()
	{
		$this->_helper->layout()->disableLayout(); 
	}
	
	public function indexAction()
	{
		$video_model = new Model_Video();

		$video = $video_model->GetEscortVideo($this->escort_id);
		if($video){
			$video['photo'] = new Cubix_ImagesCommonItem($video['photo']);
		}
		$this->view->host =  Cubix_Application::getById($this->app_id)->host;
		$this->view->config =  Zend_Registry::get('videos_config');
		$this->view->video = $video;
	}
	
	public function doAction()
	{
		$this->view->user = $this->user;
		$video_model = new Model_Video();
		$client = new Cubix_Api_XmlRpc_Client();
		$config =  Zend_Registry::get('videos_config');
				
		$action = $this->_getParam('act');
		
		switch($action){
			case 'upload':
				//$host =  Cubix_Application::getById(Cubix_Application::getId())->host;
				$response = array(
					'error' => '',
					'finish' => FALSE
				);

				try {

					if($video_model->UserVideoCount($this->escort_id) < $config['VideoCount'])
					{			
						$backend_video_count = $client->call('Application.UserVideoCount',array($this->escort_id));
						$check = Cubix_Videos::CheckVideoStatus($backend_video_count, $config['VideoCount'], true);

						if(!$check)
						{	
							$video = new Cubix_Videos($this->escort_id,$config);
							$response = $video->getResponse();

							if($response['finish'] && $response['error'] === 0 ){
								$response['wait_for_video'] = 1;
								ignore_user_abort(true);
								ob_start();
								Cubix_Videos::showResponse($response);
								header('Connection', 'close');
								header('Content-Length: '.ob_get_length());	
								ob_end_flush();
								ob_flush();
								flush();
								session_write_close();
								fastcgi_finish_request();
											
								$video->ConvertToMP4();
								
								$image = $video->SaveImage();		
								if(is_array($image))
								{	
									if(!$video->SaveVideoImageFtpFrontend($image,$client)){
										Cubix_Debug::log('system error while uploading Video', 'ERROR');
									}
								}
								else{
									Cubix_Debug::log('system error while uploading Video No images ', 'ERROR');
								}
							}
						}
						else{
							$response['error'] = $check;
						}
					}
					else{
						$response['error'] = Cubix_I18n::translate('sys_error_upload_video_count', array('count' => $config['VideoCount']));
					}
				} catch ( Exception $e ) {
					 $response['error'] = $e->getMessage();

				}
				Cubix_Videos::showResponse($response);
				die;
				
			case 'delete':
				$video_id = intval($this->_getParam('video_id'));
				var_dump($video_id,$this->escort_id);
				$video_model->RemoveV2($video_id,$this->escort_id);
				die;
		}
	}
	
	public function isVideoReadyAction() {
		
		$client = new Cubix_Api_XmlRpc_Client();
		
		$pending_video = $client->call('Application.getPendingVideo', array($this->escort_id));
		
		if($pending_video){
			$config =  Zend_Registry::get('videos_config');
			$app_id = Cubix_Application::getId();
			$host =  Cubix_Application::getById($app_id)->host;
			$video_image = array(
				'application_id' => $app_id,
				'hash' => $pending_video['image_hash'],
				'ext' => $pending_video['image_ext']
			);
			$photo_model = new Cubix_ImagesCommonItem($video_image);
			$image_url = $photo_model->getUrl('m320');
			
			$response['vod'] = $config['remote_url'].$host.'/'.$this->escort_id.'/';
			$response['photo_url'] = $image_url;
			$response['video_width'] = $pending_video['width'];
			$response['video_height'] = $pending_video['height'];
			$response['video'] = $pending_video['video'];
			$response['video_id'] = $pending_video['id'];
			echo json_encode($response);die;
		}
		else{
			echo json_encode(array('status' => 0));die;
		}
		
	}
	
}
