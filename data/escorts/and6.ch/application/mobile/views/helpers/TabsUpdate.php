<?php

class Zend_View_Helper_TabsUpdate extends Zend_View_Helper_Abstract
{
	public function tabsUpdate($content, $mode)
	{
		$this->view->content = $content;
		$this->view->mode = $mode;
		return $this->view->render('mobile/tabs-update.phtml');
	}
}
