<?php

class Zend_View_Helper_GetLink
{
	protected static $_params;

	public function getLink($type = '', array $args = array(), $clean = false, $no_args = false)
	{
		$lang_id = Cubix_I18n::getLang();

		$link = '/';
//		if ( $lang_id != Cubix_Application::getDefaultLang() ) {
//			$link .= $lang_id . '/';
//		}
//		$link .= "mobile/";
		if ( isset($args['state']) && 7 != Cubix_Application::getId() ) {
			$args['region'] = $args['state'];
			unset($args['state']);
		}

		switch ( $type ) {
			case 'search':
				$link .= 'search';
				break;
			case 'escorts-list':
				$link .= 'escorts';
			break;
			case 'base-city':
				$section = isset($args['section']) ? $args['section'] . '/' : '';
				if ( strlen($section) ) unset($args['section']);

				$link .= 'escorts/' . $section . 'city_' . $args['city'];
				unset($args['city']);
			break;
			case 'escorts':
				$request = Zend_Controller_Front::getInstance()->getRequest();
				if ( empty(self::$_params) ) {

					$req = trim($request->getParam('req', ''), '/');
					$req = explode('/', $req);

					$params = array();

					foreach ($req as $r)  {
						$param = explode('_', $r);
						if ( count($param) < 2 ) {
							$params[] = $r;
							continue;
						}

						$param_name = reset($param);
						array_shift($param);

						$params[$param_name] = implode('_', $param);
					}

					self::$_params = $params;
				}

				if ( $clean ) {
					$params = array();
				}
				else {
					$params = self::$_params;
				}


				foreach ( $args as $key => $value ) {
					if ( is_null($value) ) {
						unset($params[$key]);
						continue;
					}


					$params[$key] = $value;
				}

				if ( $params['region'] ) {
					$host = explode('.', $request->getHttpHost());

					if ( $host[1] != 'and6' ) {//if has subsubdomain ex. m.ticino.and6.com
						unset($host[1]);
					}

					$subdomain = 'm.' . $params['region'];
					if ( $subdomain == 'm.deutschschweiz' ) {
						$subdomain = 'm';
					}
					$host[0] = $subdomain;
					$link = 'http://' . implode('.', $host) . '/';

					unset($params['region']);
				}
				if  ( $params['city_id'] ) {
					$link .= $params['city'] . '-c' . $params['city_id'] . '/';
					unset($params['city']);
					unset($params['city_id']);
				}



				foreach ( $params as $param => $value ) {
					if ( ! strlen($param) || ! strlen($value)) continue;
					if ( is_int($param) ) {
						if ( $value == 'tours' ) $value = 'citytours';
						$link .= 'escorts/' . $value . '/';
					}
					elseif ( strlen($param) ) {
						if ( $param == 'page' ) {
							if ( $value - 1 > 0 ) {
								$link .= ($value - 1) . '/';
							}
						}
						else {
							$link .= $param . '_' . $value . '/';
						}
					}
				}

//				$link .= 'escorts/';
//				foreach ( $args as $param => $value ) {
//
//
//
//					if ( ! strlen($param) || ! strlen($value)) continue;
//					if ( is_int($param) ) {
//						if ( $value == 'tours' ) $value = 'citytours';
//						$link .= $value . '/';
//					}
//					elseif ( strlen($param) ) {
//						if ( $param == 'page' ) {
//							if ( $value - 1 > 0 ) {
//								$link .= ($value - 1) . '/';
//							}
//						}
//						else {
//							$link .= $param . '_' . $value . '/';
//						}
//					}
//				}

				$link = rtrim($link, '/');

				$args = array();
			break;
			case 'gotm':
				$link .= 'girl-of-month';

				if ( isset($args['history']) && $args['history'] ) {
					$link .= '/history';
				}

				if ( isset($args['page']) ) {
					if ( $args['page'] != 1 ) {
						$link .= '/page_' . $args['page'];
						//$link .= '/page_' . $args['page'];
					}

					unset($args['page']);
				}
				break;
			case 'profile':

				$link .= 'sex-model/';
                if(isset($args['type']) && $args['type'] != 'bio' ){
                    $link .= $args['type'] .'/';
                }
				$link .= $args['showname'] . '-' . $args['escort_id'];



				unset($args['showname']);
                unset($args['type']);
                unset($args['escort_id']);
			break;
			case 'trans-list':
				$link .= 'search?gender=3&order=registered&order_direction=asc&search=Suchen';
				break;
            case 'massage':
				$link .= 'search?services=massage&order=registered&order_direction=asc&search=Suchen';
				break;
            case 'companion':
				$link .= 'search?services=companion&order=registered&order_direction=asc&search=Suchen';
				break;
			case 'romandie':
				$link .= 'search?romandie=true&order=registered&order_direction=asc&search=Suchen';
				break;
            case 'virtual':
                $link .= 'search?virtual=1&order=registered&order_direction=asc&search=Suchen';
                break;
            case 'boys-list':
                $link .= 'search?gender=2&order=registered&order_direction=asc&search=Suchen';
                break;
			case 'regular-list':
				$link .= 'search?gender=1&order=registered&order_direction=asc&search=Suchen';
				break;
			case 'outcall-list':
				$link .= 'search?available_for_outcall=on&order=registered&order_direction=asc&search=Suchen';
				break;
			case 'profile-hash':
                $link .= 'escort/';
				$link .= $args['showname'] . '-' . $args['escort_id'] . '#'.$args['type'];
				unset($args['showname']);
				unset($args['escort_id']);
				unset($args['type']);
			break;
			case 'forgot':
				$link .= 'private/forgot';
			break;
			case 'signup':
				$link .= 'private/signup';
			break;
			case 'signin':
				$link .= 'private/signin';
			break;
			case 'signout':
				$link .= 'private/signout';
			break;
            case 'mobile-profile-agency':
                $link .= 'private-v2';
                break;
            case 'mobile-profile-v2':
                $link .= 'private-v2/profile-v2';
                break;
            case 'mobile-profile-v2-dash':
                $link .= 'private-v2/profile-v2/dash';
                break;
            case 'mobile-profile-v2-gallery':
                $link .= 'private-v2/profile-v2/gallery';
                break;
            case 'mobile-profile-v2-success':
                $link .= 'private/register-success';
                break;
			case 'private':
				$link .= 'private';
			break;
            case 'classified-ads-index':
                $link .= 'classified-ads';
                break;
            case 'classified-ads-place-ad':
                $link .= 'classified-ads/place-ad';
                break;
            case 'classified-ads-success':
                $link .= 'classified-ads/success';
                break;
           	case 'classified-ads-error':
                $link .= 'classified-ads/error';
                break;
            case 'classified-ads-twispay-response':
                $link .= 'classified-ads/twispay-response';
                break;
            case 'classified-ads-ajax-filter':
                $link .= 'classified-ads/ajax-filter?ajax';
                break;
            case 'classified-ads-ajax-list':
                $link .= 'classified-ads/ajax-list?ajax';
                break;
            case 'classified-ads-print':
                $link .= 'classified-ads/print';
                break;
            case 'classified-ads-ad':
                $link .= 'classified-ads/ad';
                break;
			case 'online-billing':
				$link .= 'online-billing';
			break;
			case 'online-billing-agency-gotd':
				$link .= 'online-billing/agency-index?gotd=1';
			break;
			case 'online-billing-agency-votd':
				$link .= 'online-billing/agency-index?votd=1';
			break;
			case 'online-billing-escort':
				$link .= 'online-billing/escort-index';
			break;
			case 'online-billing-agency':
				$link .= 'online-billing/agency-index';
			break;
			case 'online-billing-gotd';
				$link .= 'online-billing/gotd';
				break;
			case 'online-billing-votd';
				$link .= 'online-billing/votd';
				break;
			case 'online-billing-payment-success':
				$link .= 'online-billing/success';
				break;
			case 'online-billing-payment-failure':
				$link .= 'online-billing/failure';
				break;
			case 'membership-type':
				$link .= 'private/membership-type';
			break;
			case 'feedback':
				$link .= 'feedback';
			break;
			case 'feedback-ads':
			    $link .= 'feedback-ads';
			    break;
			case 'contact':
				$link .= 'contact';
			break;
			case 'add-review-comment':
				$link .= 'private-v2/add-review-comment';
			break;
			case 'banner-zone-escort':
				$link .= 'private-v2/banner-zone-escort';
			break;
			case 'banner-zone-agency':
				$link .= 'private-v2/banner-zone-escort';
			break;
			case 'captcha':
				$link .= 'captcha?' . rand();
				//$link = '/img/sample_captcha.gif?' . rand();
			break;
			case 'viewed-escorts':
				$link .= 'escorts/viewed-escorts';
			break;
			case '100p-verify':
				$link .= 'private/verify';
			break;
			case 'video-upload':
				$link .= 'video';
			break;
			case 'video-list':
				$link .= 'video-list/' . $args['city_id'];
				unset($args['city_id']);
				break;
			case 'video-instructions':
				$link .= 'video-instructions';
			break;
			case 'video-voting-plain':
				$link .= 'plain/video-voting';
			break;
			case '100p-verify-webcam':
				$link .= 'private/verify/webcam';
			break;
			case '100p-verify-idcard':
				$link .= 'private/verify/idcard';
			break;
			case 'agency':
                $link .= 'agency/' . $args['slug'] . '-' . $args['id'];
                unset($args['slug']);
                unset($args['id']);
            break;
            case 'agencies':
                $link .= 'agencies';
                break;

			case 'private-v2-support':
				$link .= 'support';
			break;

			case 'favorites':
				$link .= 'favorites';
			break;
			case 'add-to-favorites':
				$link .= 'private/add-to-favorites';
			break;
			case 'remove-from-favorites':
				$link .= 'private/remove-from-favorites';
			break;

			// Private Area
            case 'simple-profile':
				$link .= 'private-v2/profile/simple';
				break;
			case 'edit-profile':
				$link .= 'private/profile';
			break;
			case 'edit-agency-profile':
				$link .= 'private/agency-profile';
			break;
			case 'private-member-profile':
				$link .= 'private/profile';
			break;
            case 'private-v2-happy-hour':
                $link .= 'private-v2/happy-hour';
                break;
            case 'private-v2-classified-ads':
                $link .= 'private-v2/classified-ads';
                break;
            case 'private-v2-classified-ads-ad':
                $link .= 'private-v2/classified-ads-ad';
                break;
            case 'private-classified-ads':
                $link .= 'private-v2/classified-ads';
                break;
            case 'private-classified-ads-ad':
                $link .= 'private-v2/classified-ads-ad';
                break;
            case 'private-classified-ads-ad-delete-confirm':
				$link .= 'private-v2/classified-ad-delete-confirm';
				break;
			case 'private-classified-ads-ad-delete':
				$link .= 'private-v2/classified-ad-delete';
				break;
			case 'private-classified-ads-make-premium':
				$link .= 'private-v2/classified-ads-make-premium';
				break;
			case 'private-classified-ads-premium-success':
				$link .= 'private-v2/classified-ads-premium-success';
				break;
			case 'private-classified-ads-premium-error':
				$link .= 'private-v2/classified-ads-premium-error';
				break;
			case 'classified-ads-twispay-response':
                $link .= 'private-v2/twispay-response';
                break;
            case 'private-classified-ads-ajax-list':
                $link .= 'private-v2/classified-ads-ajax-list';
                break;
            case 'escorts-happy-hour':
                $link .= 'escorts/happy-hours';
                break;

			case 'profile-data':
				$link .= 'private/profile-data';
			break;
			case 'edit-photos':
				$link .= 'private/photos';
			break;
			case 'private-reviews':
				$link .= 'private/reviews';
				break;
			case 'private-settings':
				$link .= 'private/settings';
				break;
            case 'skype-bookings':
                $link .= 'private-v2/skype-bookings';
                break;
			case 'edit-rates':
				$link .= 'private/rates';
			break;
			case 'edit-escorts':
				$link .= 'private/escorts';
			break;
			case 'delete-escort':
				$link .= 'private/delete-escort';
			break;
			case 'edit-tours':
				$link .= 'private/tours';
			break;
			case 'change-passwd':
				$link .= 'private/change-password';
			break;
			case 'private-upgrade-premium':
				$link .= 'private/upgrade';
				break;
			case 'mob-private-v2-faq':
				$link .= 'faq';
				break;
            case 'mob-status-messages':
                $link .= 'status-messages';
                break;

			case 'search':
				$link .= 'search';
			break;

			case 'mob-1':
				$link .= 'latest-actions';
			break;

			case 'latest-actions':
				$link .= 'latest-actions';
			break;

			case 'prices-static':
				$link .= 'p/prices';
			break;

			case 'gps-location':
				$link .= 'gps-location';
			break;
			case 'late-night-girls':
				$link .= 'escorts/late-night-girls';
			break;

			case 'terms':
				$link .= 'page/terms-and-conditions';
			break;

			case 'agb':
				$link .= 'page/agb';
			break;

			case 'refund-policy':
				$link .= 'page/refund-cancellation-policy';
			break;

			case 'datenschutz':
				$link .= 'page/datenschutz';
			break;

			case 'webcam-lm':
				$link .= 'page/webcam-verification-learn-more';
			break;
			case 'passport-lm':
				$link .= 'page/passport-verification-learn-more';
			break;

			case 'external-link':
				$link = Cubix_Application::getById()->url . '/go?' . $args['link'];
				unset($args['link']);
			break;
			case 'ixs-banner-url':
				$link = Cubix_Application::getById()->url.'/images/ixsGovazd/'.$args['filename'];
				unset($args['filename']);
			break;

			// V2 Private Area
			case 'private-v2':
				$link .= 'private-v2';
				break;
			case 'alerts':
				$link .= 'private-v2/alerts';
				break;
			case 'private-v2-settings':
				$link .= 'private-v2/settings';
				break;
			case 'private-v2-happy-hour':
				$link .= 'private-v2/happy-hour';
				break;
			case 'private-v2-profile':
				$link .= 'private-v2/profile';
				break;
			case 'escort-photo':
				$link .= 'escort/photo';
				break;
			case 'private-v2-plain-photos':
				$link .= 'private-v2/plain-photos';
				break;
			case 'private-v2-tours':
				$link .= 'private-v2/tours';
				break;
			case 'private-v2-ajax-tours':
				$link .= 'private-v2/ajax-tours';
				break;
			case 'private-v2-ajax-tours-add':
				$link .= 'private-v2/ajax-tours-add';
				break;
			case 'private-v2-ajax-tours-remove':
				$link .= 'private-v2/ajax-tours-remove';
				break;
			case 'private-v2-verify':
				$link .= 'private-v2/verify';
				break;
			case 'private-v2-premium':
				$link .= 'private-v2/premium';
				break;
			case 'private-support':
				$link .= 'support';
				break;
			case 'private-support-add-ticket':
				$link .= 'support/add-ticket';
				break;

			case 'private-v2-agency-profile':
				$link .= 'private-v2/agency-profile';
				break;
			case 'private-v2-member-profile':
				$link .= 'private-v2/member-profile';
				break;
			case 'private-v2-upgrade-premium':
				$link .= 'private-v2/upgrade';
				break;
			case 'ticket-open':
				$link .= 'support/ticket';
				break;
			case 'private-v2-escorts':
				$link .= 'private-v2/escorts';
				break;
            case 'private-v2-escorts-delete':
				$link .= 'private-v2/profile-delete';
				break;
			case 'reviews':
				$link .= 'evaluations';
				break;
			case 'escort-reviews':
				$link .= 'evaluations/' . $args['showname'] . '-' . $args['escort_id'];
				unset ($args['showname']);
				unset ($args['escort_id']);
				break;
			case 'member-reviews':
				$link .= 'evaluations/member/' . $args['username'];
				unset ($args['username']);
				break;
			case 'escorts-reviews':
				$link .= 'evaluations/escorts';
				break;
			case 'agencies-reviews':
				$link .= 'evaluations/agencies';
				break;
            case 'agencies-cities':
                $link .= 'agencies/city/' . $args['city_id'];
                unset($args['city_id']);
                break;
			case 'top-reviewers':
				$link .= 'evaluations/top-reviewers';
				break;
			case 'top-ladies':
				$link .= 'evaluations/top-ladies';
				break;
			case 'voting-widget':
				$link .= 'escort/' . $args['showname'] . '-' . $args['escort_id'] . '/vote';
				unset($args['showname']);
				unset($args['escort_id']);
				break;
			case 'private-v2-reviews':
				$link .= 'private-v2/reviews';
				break;
			case 'private-v2-escort-reviews':
				$link .= 'private-v2/escort-reviews';
				break;
			case 'private-v2-escorts-reviews':
				$link .= 'private-v2/escorts-reviews';
				break;
			case 'private-v2-add-review':
				$link .= 'private-v2/add-review';
				break;
			case 'reviews-search':
				$link .= 'evaluations/search';
				break;
            case 'comments':
				$link .= 'comments' . ($args['page'] ? '?page=' . $args['page'] : '');
				unset($args['page']);
				break;
            case 'morecities':
				$link .= 'allcities';
				break;
            case 'private-messaging':
                $link .= 'private-messaging';
                break;
            case 'alle-clubs':
                $link .= 'alle-clubs';
                break;
            case 'jobs':
                $link .= 'classified-ads#category=5;pp=25';
                break;
            case 'immobilien':
                $link .= 'classified-ads#category=6;pp=25';
                break;
			case 'private-v2-video-action-mobile':
				$link .= 'video/do';
				break;
			case 'private-v2-video':
				$link .= 'video';
				break;
			case 'private-v2-video-ready':
				$link .= 'video/is-video-ready';
				break;
            case 'pager':
                $uri = $args['request_url'];
                $uri = preg_replace('#mobile/#', '', $uri);
                $link .= $uri.$args['page'];
				/*if ( $lang_id != ''){
					$link .= '?ln='.$lang_id;
				}*/
                return $link;
				break;
            case 'trim':
                $uri = $args['request_url'];

//                $uri = preg_replace('#en/mobile/#', '', $uri);
                $uri = preg_replace('#mobile/#', '', $uri);
//				$uri = preg_replace('#mobile#', '', $uri);

                return $uri;
                break;
			case 'video-chat-success':
				$link .= 'escorts/video-chat-success';
			break;
			case 'video-chat-failure':
				$link .= 'escorts/video-chat-failure';
			break;
			case 'cams':
				$link .= 'and6-cams';
			break;
			case 'and6-cams':
				$link .= 'and6-cams';
			break;
			case 'and6-cams-buy-tokens':
				$link .= 'and6-cams/buy-tokens';
			break;	
			case 'and6-cams-escort-dash':
				$link .= 'and6-cams/broadcaster/dashboard';
			break;
            case 'jobs_rent':
                $link .= 'classified-ads/place-ad';
                break;
			case 'cams-broadcaster':
				$encoded_showname = strtolower(str_replace(' ', '-', $args['showname']));
				$link .= 'and6-cams/channel/' . $encoded_showname.'-'.$args['escort_id'];
				unset($args['showname']);
				unset($args['escort_id']);
			break;
			case 'and6-cams-payment-response':
				$link = 'https://m.and6.com/and6-cams/payment-response';
			break;
			case 'cams-login-redirect':
				$link = 'https://and6.escortcams.com';
				if ($args['path']) {
					$link .= $args['path'];
				}
				unset($args['path']);

		}

		if ( ! $no_args ) {
			if ( count($args) ) {
				$link .= '?';
				$params = array();
				foreach ( $args as $arg => $value ) {
					if ( ! is_array($value) ) {
						$params[] = $arg . '=' . urlencode($value);
					}
					else {
						foreach ( $value as $v ) {
							$params[] = $arg . '[]=' . $v;
						}
					}
				}

				$link .= implode('&', $params);
			}
		}

		return $link;
	}
}
