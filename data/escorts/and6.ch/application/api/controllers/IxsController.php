<?php
class Api_IxsController extends Zend_Controller_Action
{
	/**
	 * @var Zend_Db_Adapter_Mysqli
	 */
	protected $_db;
	
	public function init()
	{

		$this->_db = Zend_Registry::get('db');
		$this->view->layout()->disableLayout();

	}
	
	public function ixsBannersAction(){
			
		$this->view->layout()->disableLayout();
		//$url = 'https://ixspublic.com/deliver.php?websiteID=3&ref=http://www.escortdirectory.com/&rand=47702733';
		//$url = 'https://ixspublic.com/deliver.php?websiteID=6'; old version
		$url = 'https://ixspublic.com/api.php?websiteID=6';
		$ixspublic = file_get_contents($url);

		//preg_match_all('|(\{)("id"[^\}]+)\}|is', $ixspublic, $matches);

		//preg_match_all('|zonesJsonBuffer = ({[^;]+);|is', $ixspublic, $matches); old version
		$ixs = json_decode($ixspublic);
		
		//$images is an numeric array of images with their attributes
		$model_ixs = new Model_ixsBanners();

		$model_ixs->truncate();
		$result = true;
		
		foreach ($ixs as $zone => $images) {
			
			$mysql_insert_result = $model_ixs->insert( $ixs->$zone, $zone);
			if($mysql_insert_result){
				$images = $model_ixs->get(null,null,null,false);
				$copycount = $model_ixs->copyImages($images);
				
				if(count($images) == $copycount){
					$result = 'Images successfully synced';
				}else{
					$result = 'Error during sync';
				}
			}
		}

		$cache = Zend_Registry::get('cache');
		foreach ($cache->getIds() as $key => $cacheditem) {
			if( strpos( $cacheditem, 'ixs_banners') !== false ){
				$cache->remove($cacheditem);
			}
		}
		
		echo($result);
		die;
	}

}
