<?php

class Api_SyncController extends Zend_Controller_Action
{
	/**
	 * @var Zend_Db_Adapter_Mysqli
	 */
	protected $_db;

	protected $_debug = true;

	protected $_log;

	/**
	 * @var Cubix_Api
	 */
	protected $_client;

	public function init()
	{
		$this->_db = Zend_Registry::get('db');
		$this->_client = Cubix_Api::getInstance();

		$this->view->layout()->disableLayout();
	}

	/**
	 * @var Cubix_Cli
	 */
	private $_cli;

	public function diffAction()
	{
		error_reporting(E_ALL);
		ini_set('display_errors', 1);
		ini_set('memory_limit', '1024M');

		if ( Cubix_Cli::pidIsRunning('/var/run/a6-sync.pid') ) {
			$cli->colorize('red')->out('Big Sync is running, exitting...')->reset();
			exit(1);
		}
		
		// <editor-fold defaultstate="collapsed" desc="Init CLI">
		$log = $this->_log = new Cubix_Cli_Log('/var/log/a6-diff-sync.log');
		$cli = $this->_cli = new Cubix_Cli();
		$cli->clear();
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Prevent from over-syncing">
		Cubix_Cli::setPidFile('/var/run/a6-diff-sync.pid');
		if ( Cubix_Cli::isRunning() ) {
			$cli->colorize('red')->out('Sync is already running, exitting...')->reset();
			exit(1);
		}
		// </editor-fold>

		$sync_start_date = time();
		$start = 0; $limit = 100;
		do {
			// <editor-fold defaultstate="collapsed" desc="Fetch Data from API">
			$cli->out('Fetching data from api (max 100)...');
			$data = $this->_client->call('sync.getDiffEscorts', array($start++ * $limit, $limit));
			// </editor-fold>

			// IMPORTANT! This array will contain ids of escorts which were successuflly
			// removed or updated this array will be passed to api to mark those escorts
			// in journal as synchronized
			$success = array();

			// <editor-fold defaultstate="collapsed" desc="Process escorts which need to be removed">
			if ( count($data['removed']) > 0 ) {
				$cli->colorize('purple')
					->out('Need to remove ' . ($count = count($data['removed'])) . ' escorts...')
					->reset();
				foreach ( $data['removed'] as $i => $escort ) {
					$percent = round((($i + 1) / $count) * 100) . '%';
					// Shorthand variables
					list($escort_id, $showname) = $escort;

					try {
						$this->_removeEscort($escort_id);
						$success[] = $escort_id;
						$result = true;
					}
					catch ( Exception $e ) {
						$result = false;
						// If error occured just log the error and display output in red color
						$log->error("Unable to remove escort '$showname': " . $e->getMessage());
					}

					// Pretty output
					$cli->out('[' . ($i + 1) . '/' . $count . ']', false)
						->colorize($result ? 'green' : 'red')
						->out(' Remove ' . $showname . '...', false)
						->column(60)
						->reset()
						->out($percent, true);

				}
				
				$cli->out();
			}
			// </editor-fold>

			// <editor-fold defaultstate="collapsed" desc="Process escorts which need to be updated">
			if ( count($data['updated']) > 0 ) {
				$cli->colorize('purple')
						->out('Need to update ' . ($count = count($data['updated'])) . ' escorts...')
						->reset();
				foreach ( $data['updated'] as $i => $escort ) {
					$percent = round((($i + 1) / $count) * 100) . '%';
					// Shorthand variables
					$escort_id = $escort['profile']['id'];
					$showname = $escort['profile']['showname'];

					// <editor-fold defaultstate="collapsed" desc="Remove escort at first">
					try {
						$this->_removeEscort($escort_id);
						$result = true;
					}
					catch ( Exception $e ) {
						$result = false;
						$log->error("Unable to remove escort '$showname': " . $e->getMessage());
					}

					$cli->out('[' . ($i + 1) . '/' . $count . ']', false)
						->colorize($result ? 'green' : 'red')
						->out(' Remove ' . $showname . '...', false)
						->column(60)
						->reset()
						->out($percent, true);
					// </editor-fold>

					// If there is error removing error just ignore this one
					if ( ! $result ) continue;

					try {
						// Insert escort and all correspoinding data (services, cities, profiles, photos, etc.))
						
						$this->_insertEscort($escort);
						$success[] = $escort_id;
						$result = true;
					}
					catch ( Exception $e ) {
						$result = false;
						// Just log into file if something happend and ignore
						$log->error("Unable to insert escort '$showname': " . $e->getMessage());
					}

					// Pretty output
					$cli->out('[' . ($i + 1) . '/' . $count . ']', false)
						->colorize($result ? 'green' : 'red')
						->out(' Insert ' . $showname . '...', false)
						->column(60)
						->reset()
						->out($percent, true);
				}
				
				$cli->out();
			}
			// </editor-fold>

			// If there were successfull escort syncs
			if ( count($success) > 0 ) {
				// <editor-fold defaultstate="collapsed" desc="Finally mark escorts as sync done to prevent continious syncing">
				// We don't want any extra data to be passed to API
				$success = array_unique($success);
				// Pass to API to mark these escorts as sync done
				$this->_client->call('sync.markEscortsDone', array($success, $sync_start_date));
				// </editor-fold>

				// And clear the cache
			}
			else {
				$cli->colorize('yellow')->out('Nothing happend :)')->reset();
			}
		} while ( count($data['removed']) > 0 || count($data['updated']) > 0 );

		// At the end we need to repopulate cache table for optimized sqls
		// this table contains rows that describe in which city escort will be shown
		// and why is she there, because of upcoming, tour or just working city
		// also there are some reduntunt fields such as is_premium, is_agency,
		// gender and so on
		$cli->out()->out('Populating cache tables...', false);
		try {
			$this->_populateCacheTables();
			$cli->column(60)->colorize('green')->out('success');
		}
		catch ( Exception $e ) {
			$cli->column(60)->colorize('red')->out('error');
			$log->error($e->getMessage());
		}
		$cli->out();

		//$clear_cache = file_get_contents(Cubix_Application::getById(Cubix_Application::getId())->url . "/api/escorts/clear-cache");
		$clear_cache = Model_Applications::clearCache(); 
		$cli->reset()->out($clear_cache);

		exit;
	}

	private function _removeEscort($escort_id)
	{
		$this->_db->delete('escorts', $this->_db->quoteInto('id = ?', $escort_id));
		$this->_db->delete('escort_services', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('escort_cities', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('escort_cityzones', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('escort_photos', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('video', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('upcoming_tours', $this->_db->quoteInto('id = ?', $escort_id));
		$this->_db->delete('escort_profiles', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('premium_cities', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('escort_working_times', $this->_db->quoteInto('escort_id = ?', $escort_id));
//		$this->_db->delete('escorts_in_cities', $this->_db->quoteInto('escort_id = ?', $escort_id));
		if ( Cubix_Application::getId() == 16 ) {
			$this->_db->delete('additional_areas', $this->_db->quoteInto('escort_id = ?', $escort_id));
		}
	}

	private function _populateCacheTables()
	{
		try {
			$this->_db->query('TRUNCATE escorts_in_countries');
			$sql = '
				INSERT INTO escorts_in_countries (escort_id, country_id, gender, is_agency, is_tour, is_upcoming, is_premium)
				SELECT x.escort_id, x.country_id, x.gender, x.is_agency, x.is_tour, x.is_upcoming, x.is_premium
				FROM ((
					SELECT
						e.id AS escort_id, ct.country_id,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 0 AS is_tour, 0 AS is_upcoming,
						0 AS is_premium
					FROM cities AS ct
					INNER JOIN escort_cities ec ON ec.city_id = ct.id
					INNER JOIN escorts e ON e.id = ec.escort_id
					WHERE /*ct.country_id = 71 AND e.is_on_tour = 0 AND*/ FIND_IN_SET(1, e.products) > 0
					GROUP BY ct.country_id
				) UNION (
					SELECT
						e.id AS escort_id, ct.country_id,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 0 AS is_tour, 1 AS is_upcoming,
						e.is_tour_premium AS is_premium
					FROM upcoming_tours ut
					INNER JOIN cities ct ON ct.id = ut.tour_city_id
					INNER JOIN escorts e ON e.id = ut.id
					WHERE /*ct.country_id = 71 AND e.is_on_tour = 0 AND*/ FIND_IN_SET(7, e.products) > 0 AND
						DATE(ut.tour_date_from) > DATE(NOW()) AND ut.tour_date_from < CURDATE() + INTERVAL 7 DAY
					GROUP BY ct.country_id
				) UNION (
					SELECT
						e.id AS escort_id, ct.country_id,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 1 AS is_tour, 0 AS is_upcoming,
						e.is_tour_premium AS is_premium
					FROM escorts e
					INNER JOIN cities ct ON ct.id = e.tour_city_id
					WHERE
						e.is_on_tour = 1 AND FIND_IN_SET(7, e.products) > 0
					GROUP BY ct.country_id
				)) x
			';
			$this->_db->query($sql);
		}
		catch ( Exception $e ) {
			throw new Exception("Unable to repopulate cache table 'escorts_in_countries'\nError: {$e->getMessage()}");
		}
		
		try {
			$this->_db->query('TRUNCATE escorts_in_cityzones');
			$sql = '
				INSERT INTO escorts_in_cityzones (escort_id, city_id, cityzone_id, gender, is_agency, is_tour, is_upcoming, is_premium)
				SELECT x.escort_id, x.id AS city_id, x.cityzone_id, x.gender, x.is_agency, x.is_tour, x.is_upcoming, x.is_premium
				FROM ((
					SELECT
						cz.city_id AS id, e.id AS escort_id, cz.id AS cityzone_id,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 0 AS is_tour, 0 AS is_upcoming,
						EXISTS (SELECT 1 FROM premium_cities WHERE escort_id = e.id AND city_id = cz.id) AS is_premium
					FROM cityzones AS cz
					INNER JOIN escort_cityzones ecz ON ecz.city_zone_id = cz.id
					INNER JOIN escorts e ON e.id = ecz.escort_id
					INNER JOIN cities ct ON ct.id = cz.city_id
					WHERE /*ct.country_id = 71 AND e.is_on_tour = 0 AND*/ FIND_IN_SET(1, e.products) > 0
				) UNION (
					SELECT
						ut.tour_city_id AS id, e.id AS escort_id, cz.id AS cityzone_id,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 0 AS is_tour, 1 AS is_upcoming,
						e.is_tour_premium AS is_premium
					FROM upcoming_tours ut
					INNER JOIN cities ct ON ct.id = ut.tour_city_id
					INNER JOIN escorts e ON e.id = ut.id
					INNER JOIN escort_cityzones ecz ON ecz.escort_id = e.id
					INNER JOIN cityzones cz ON cz.id = ecz.city_zone_id AND cz.city_id = ct.id
					WHERE /*ct.country_id = 71 AND e.is_on_tour = 0 AND*/ FIND_IN_SET(7, e.products) > 0 AND
						DATE(ut.tour_date_from) > DATE(NOW()) AND ut.tour_date_from < CURDATE() + INTERVAL 7 DAY
				) UNION (
					SELECT
						e.tour_city_id AS id, e.id AS escort_id, cz.id AS cityzone_id,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 1 AS is_tour, 0 AS is_upcoming,
						e.is_tour_premium AS is_premium
					FROM escorts e
					INNER JOIN cities ct ON ct.id = e.tour_city_id
					INNER JOIN escort_cityzones ecz ON ecz.escort_id = e.id
					INNER JOIN cityzones cz ON cz.id = ecz.city_zone_id AND cz.city_id = ct.id
					WHERE
						e.is_on_tour = 1 AND FIND_IN_SET(7, e.products) > 0
				)) x
			';
			$this->_db->query($sql);
		}
		catch ( Exception $e ) {
			throw new Exception("Unable to repopulate cache table 'escorts_in_cityzones'\nError: {$e->getMessage()}");
		}
		
		try {
			$this->_db->query('TRUNCATE escorts_in_cities');
			$sql = '
				INSERT INTO escorts_in_cities (escort_id, region_id, city_id, gender, is_agency, is_tour, is_upcoming, is_premium, is_base, ordering)
				SELECT x.escort_id, x.region_id, x.id AS city_id, x.gender, x.is_agency, x.is_tour, x.is_upcoming, x.is_premium, x.is_base, x.ordering
				FROM ((
					SELECT
						ct.id AS id, ct.region_id, e.id AS escort_id, e.ordering,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 0 AS is_tour, 0 AS is_upcoming,
						EXISTS (SELECT 1 FROM premium_cities WHERE escort_id = e.id AND city_id = ct.id) AS is_premium,
						(e.city_id = ct.id) AS is_base
					FROM cities AS ct
					INNER JOIN escort_cities ec ON ec.city_id = ct.id
					INNER JOIN escorts e ON e.id = ec.escort_id
					WHERE 1 /*AND e.is_on_tour = 0*/ AND FIND_IN_SET(1, e.products) > 0
				) UNION (
					SELECT
						ut.tour_city_id AS id, ct.region_id, e.id AS escort_id, e.ordering,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 0 AS is_tour, 1 AS is_upcoming,
						e.is_tour_premium AS is_premium,
						FALSE AS is_base
					FROM upcoming_tours ut
					INNER JOIN cities ct ON ct.id = ut.tour_city_id
					INNER JOIN escorts e ON e.id = ut.id
					WHERE 1 /* e.is_on_tour = 0*/ AND FIND_IN_SET(7, e.products) > 0 AND
						DATE(ut.tour_date_from) > DATE(NOW()) AND ut.tour_date_from < CURDATE() + INTERVAL 7 DAY
				) UNION (
					SELECT
						e.tour_city_id AS id, ct.region_id, e.id AS escort_id, e.ordering,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 1 AS is_tour, 0 AS is_upcoming,
						e.is_tour_premium AS is_premium,
						TRUE AS is_base
					FROM escorts e
					INNER JOIN cities ct ON ct.id = e.tour_city_id
					WHERE
						e.is_on_tour = 1 AND FIND_IN_SET(7, e.products) > 0
				) UNION (
					SELECT
						ct.id AS id, ct.region_id, e.id AS escort_id, e.ordering,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 0 AS is_tour, 0 AS is_upcoming,
						0 AS is_premium,
						FALSE AS is_base
					FROM additional_areas aa
					INNER JOIN escorts e ON e.id = aa.escort_id
					INNER JOIN cities ct ON ct.id = aa.area_id
					WHERE 
						FIND_IN_SET(1, e.products) > 0
				)) x
			';
			$this->_db->query($sql);

			//remove all tour cities in and6 tour is just informative in profile not in listing
			$sql = '
				DELETE eic FROM escorts_in_cities AS eic
				WHERE eic.is_tour = 1 OR eic.is_upcoming = 1
			';
			$this->_db->query($sql);

			/*$sql = '
				DELETE eic FROM escorts_in_cities AS eic
				WHERE ((SELECT e.is_on_tour FROM escorts e WHERE e.id = eic.escort_id) AND eic.is_tour = 0 AND eic.is_base = 0 AND eic.is_upcoming <> 1)
			';
			$this->_db->query($sql);
			
			$sql = '
				DELETE eic, ec FROM escorts_in_cities AS eic
				INNER JOIN escorts e ON eic.escort_id = e.id
				INNER JOIN escort_cities ec ON ec.city_id = eic.city_id
				WHERE FIND_IN_SET(14, e.products) > 0 AND eic.is_base = 0
			';
			$this->_db->query($sql);*/
		}
		catch ( Exception $e ) {
			throw new Exception("Unable to repopulate cache table 'escorts_in_cities'\nError: {$e->getMessage()}");
		}
	}

	private function _insertEscort($escort)
	{
		$cli = $this->_cli;
		$escort_id = $escort['profile']['id'];
		$showname = $escort['profile']['showname'];

		// Blacklisted countries
		if ( Cubix_Application::getId() == 16 ) {
			$bl_countries_count = $this->_db->query('SELECT COUNT(id) AS count FROM countries_all')->fetch();
		}else{
			$bl_countries_count = $this->_db->query('SELECT COUNT(id) AS count FROM countries')->fetch();
		}
		$bl_countries_fields = ceil($bl_countries_count->count / 63);

		$bl_countries_set_values = array();
		for ( $bli = 1; $bli <= $bl_countries_fields; $bli++ ) {
			$_set_values = $this->_db->fetchRow('DESCRIBE escorts blacklisted_countries_' . $bli);
			$set = substr($_set_values->Type,5,strlen($_set_values->Type)-7);
			$bl_countries_set_values[$bli] = preg_split("/','/", $set);
		}


		$blacklisted_countries = $escort['profile']['blacklisted_countries'];
		unset($escort['profile']['blacklisted_countries']);
		// Blacklisted countries

		if ( isset($escort['products']) ) {
			$escort['profile']['products'] = implode(',', $escort['products']);
		}
		// Insert main escort row
		$this->_db->insert('escorts', $escort['profile']);

		// Blacklisted countries
		$set_value_update = array();
		foreach ( $bl_countries_set_values as $field_num => $set_vals ) {
			foreach( $set_vals as $set_value ) {
				if ( in_array($set_value, $blacklisted_countries) ) {
					$set_value_update[$field_num][] = $set_value;
				} else {
					$set_value_update[$field_num][] = 9999;
				}
			}
		}


		if ( count($set_value_update) ) {
			foreach($set_value_update as $k => $vals) {
				$this->_db->update('escorts', array('blacklisted_countries_' . $k => join(',', $vals)), $this->_db->quoteInto('id = ?', $escort_id));
			}
		}
		// Blacklisted countries

		// Insert escort services
		foreach ( $escort['services'] as $service ) {
			$service['escort_id'] = $escort_id;
			try {
				$this->_db->insert('escort_services', $service);
			}
			catch ( Exception $e ) {
				throw new Exception("Unable to insert service for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($service, true));
			}
		}

		// Insert escort working cities
		foreach ( $escort['cities'] as $city ) {
			$city = array('escort_id' => $escort_id, 'city_id' => $city);
			try {
				$this->_db->insert('escort_cities', $city);
			}
			catch ( Exception $e ) {
				throw new Exception("Unable to insert city for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($city, true));
			}
		}

		// Insert escort cityzones
		foreach ( $escort['cityzones'] as $cityzone ) {
			$cityzone = array('escort_id' => $escort_id, 'city_zone_id' => $cityzone);
			try {
				$this->_db->insert('escort_cityzones', $cityzone);
			}
			catch ( Exception $e ) {
				throw new Exception("Unable to insert cityzone for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($cityzone, true));
			}
		}

		// Insert escort videos
		if (isset($escort['videos']) && !empty($escort['videos']))
		{
			foreach ( $escort['videos'] as $video ) {
				
				if(isset($video['escort_id']))
				{	
					$video_id = $video['id'];
					try {
						$this->_db->insert('video', $video);
						$insert_id = $this->_db->lastInsertId();
						foreach ($escort['videos']['image'] as $key => &$img)
						{	
							if($img['video_id'] == $video_id)
							{						
								$data = $img;
								$data['video_id'] = $insert_id;
								if($key == 2 ){
									$data['is_mid'] = 1;
								}
								
								$this->_db->insert('video_image', $data) ;
							}
						}
					}
					catch ( Exception $e ) {
						throw new Exception("Unable to insert video for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($video, true));
					}
				}
			}
		}
		
		// Insert escort photos
		foreach ( $escort['photos'] as $photo ) {
			$photo['escort_id'] = $escort_id;
			try {
				$this->_db->insert('escort_photos', $photo);
			}
			catch ( Exception $e ) {
				throw new Exception("Unable to insert photo for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($photo, true));
			}
		}

		// Insert escort upcoming tours
		foreach ( $escort['tours'] as $tour ) {
			$tour['id'] = $escort_id;
			try {
				$this->_db->insert('upcoming_tours', $tour);
			}
			catch ( Exception $e ) {
				throw new Exception("Unable to insert tour for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($tour, true));
			}
		}

		// Insert escort profile
		try {
			if ( isset($escort['snapshot']) ) {
				$snapshot = array('escort_id' => $escort_id, 'data' => serialize($escort['snapshot']));
				$this->_db->insert('escort_profiles', $snapshot);
			}
		}
		catch ( Exception $e ) {
			throw new Exception("Unable to insert profile for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($snapshot, true));
		}


		// Finally if escort has premium cities insert them
		if ( isset($escort['premiums']) ) {
			foreach ( $escort['premiums'] as $city ) {
				$city = array('escort_id' => $escort_id, 'city_id' => $city);
				try {
					$this->_db->insert('premium_cities', $city);
				}
				catch ( Exception $e ) {
					throw new Exception("Unable to insert premium city for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($city, true));
				}
			}
		}

		foreach ( $escort['times'] as $time ) {
			$time['escort_id'] = $escort_id;
			try {
				$this->_db->insert('escort_working_times', $time);
			}
			catch ( Exception $e ) {
				throw new Exception("Unable to insert working time for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($time, true));
			}
		}
		
		/*if(isset($escort['escort_new_history'])){
			foreach($escort['escort_new_history'] as $new_history){
				$n_sql = "INSERT INTO escorts_new_history (escort_id, expires_at, created_at, type)
					SELECT * FROM (SELECT '".$new_history['id']."' as id, '".$new_history['escort_id']."' as escort_id, '".$new_history['expires_at']."' as expires_at, '".$new_history['created_at']."' as created_at, '".$new_history['type']."' as type) as temp 
					WHERE NOT EXISTS (SELECT id from escorts_new_history where id='".$new_history['id']."' AND escort_id='".$new_history['escort_id']."')
				";
				try{
					$this->_db->query($n_sql);
				} catch(Exception $e){
					throw new Exception("Unable to insert escort new history escort '".$new_history['escort_id']."'\nError: {$e->getMessage()}\n" . var_export($time, true));		
				}
			}
		}*/

		if ( Cubix_Application::getId() == 16 ) {
			foreach ( $escort['add_areas'] as $area ) {
				//$time['escort_id'] = $escort_id;
				try {
					$this->_db->insert('additional_areas', $area);
				}
				catch ( Exception $e ) {
					throw new Exception("Unable to insert area for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($time, true));
				}
			}
		}
	}

	public function reviewsAction()
	{
		ini_set('memory_limit', '1024M');
		ini_set("pcre.recursion_limit", "16777");
		$start_time = microtime(true);
		// <editor-fold defaultstate="collapsed" desc="Init CLI">
		$log = $this->_log = new Cubix_Cli_Log('/var/log/6a-reviews-sync.log');
		$cli = $this->_cli = new Cubix_Cli();
		$cli->clear();
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Prevent from over-syncing">
		Cubix_Cli::setPidFile('/var/run/6a-reviews-sync.pid');
		if ( Cubix_Cli::isRunning() ) {
			$cli->colorize('red')->out('Sync is already running, exitting...')->reset();
			exit(1);
		}
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Transfer Reviews">
		$chunk = 4000;
		$part = 0;
		$cli->out('Fetching reviews data from api (chunk size: ' . $chunk . ')...');
		$reviews = array();
		do {
			$cli->reset()->out('part #' . ($part + 1), false);
			$data = $this->_client->call('getEscortReviewsV2', array($part++ * $chunk, $chunk));
			if ( isset($data['error']) ) {
				$cli->column(60)->colorize('red')->out('error');
				$log->error('Error when fetching data from api: ' . $data['error']);
				continue;
			}

			$reviews = array_merge($reviews, $data['result']);
			$cli->column(60)->colorize('green')->out('success');
		} while ( count($data['result']) > 0 );
		$data = null; unset($data);
		$cli->reset()->out('Done fetching data (' . count($reviews) . ' reviews fetched).')->out();
		// </editor-fold>


		// <editor-fold defaultstate="collapsed" desc="Insert data into database">
		$cli->out('Truncate table `reviews`', false);
		try {
			$this->_db->query('TRUNCATE `reviews`');
			$cli->column(60)->colorize('green')->out('success');
		}
		catch ( Exception $e ) {
			$cli->column(60)->colorize('red')->out('error');
			$log->error('Error when truncating reviews table: ' . $e->getMessage());
			exit(1);
		}

		$cli->reset()->out('Inserting reviews...');
		$bulk_object = new Cubix_BulkInserter($this->_db, 'reviews');
		$c = 0;
		$row_limit = 5000;
		try {
			foreach ( $reviews as $review ) {
				$c++;
				if($c === 1){
					$bulk_object->addKeys(array_keys($review));
				}
				
				$bulk_object->addFields($review);				

				if( $c % $row_limit == 0){
					$sql = $bulk_object->insertBulk($cli);
					$cli->out("Bulk of ". $c . " rows inserted \n\r", false);
				}
				
			}
			
			if( $c % $row_limit !== 0 ){
				$bulk_object->insertBulk($cli);
				$cli->out("Bulk of ". $c . " rows inserted \n\r", false);
			}
		}
		catch ( Exception $e ) {
			$cli->colorize('red')->out('Could not insert reviews #----'. $e->getMessage() );
			$log->error('Error when inserting reviews # : ' . $e->getMessage());
		}
		$cli->out('Successfully inserted ' . $c . ' reviews.');
		// </editor-fold>
		$end_time = microtime(true);
        // Just print the execution time
        $cli->colorize('purple')->out('Execution took ' . round($end_time - $start_time, 4) . ' seconds')->reset()->out();
		exit(0);
	}

	public function commentsAction()
	{
		ini_set('memory_limit', '1024M');
		ini_set("pcre.recursion_limit", "16777");
		$start_time = microtime(true);
		// <editor-fold defaultstate="collapsed" desc="Init CLI">
		$log = $this->_log = new Cubix_Cli_Log('/var/log/6a-comments-sync.log');
		$cli = $this->_cli = new Cubix_Cli();
		$cli->clear();
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Prevent from over-syncing">
		Cubix_Cli::setPidFile('/var/run/6a-comments-sync.pid');
		if ( Cubix_Cli::isRunning() ) {
			$cli->colorize('red')->out('Sync is already running, exitting...')->reset();
			exit(1);
		}
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Transfer Comments">
		$chunk = 5000;
		$part = 0;
		$cli->out('Fetching comments data from api (chunk size: ' . $chunk . ')...');
		$comments = array();
		do {
			$cli->reset()->out('part #' . ($part + 1), false);
			$data = $this->_client->call('getCommentsV2', array($part++ * $chunk, $chunk));
			if ( isset($data['error']) ) {
				$cli->column(60)->colorize('red')->out('error');
				$log->error('Error when fetching data from api: ' . $data['error']);
				continue;
			}

			$comments = array_merge($comments, $data['result']);
			$cli->column(60)->colorize('green')->out('success');
		} while ( count($data['result']) > 0 );
		$data = null; unset($data);
		$cli->reset()->out('Done fetching data (' . count($comments) . ' comments fetched).')->out();
		// </editor-fold>


		// <editor-fold defaultstate="collapsed" desc="Insert data into database">
		$cli->out('Truncate table `comments`', false);
		try {
			$this->_db->query('TRUNCATE `comments`');
			$cli->column(60)->colorize('green')->out('success');
		}
		catch ( Exception $e ) {
			$cli->column(60)->colorize('red')->out('error');
			$log->error('Error when truncating comments table: ' . $e->getMessage());
			exit(1);
		}

		$cli->reset()->out('Inserting comments...');
		$c = 0;
		$bulk_object = new Cubix_BulkInserter($this->_db, 'comments');
		$row_limit = 5000;
		try {
			foreach ( $comments as $comment ) {
				$c++;
				if($c === 1){
					$bulk_object->addKeys(array_keys($comment));
				}

				$bulk_object->addFields($comment);				

				if( $c % $row_limit == 0){
					$bulk_object->insertBulk($cli);
					$cli->out("Bulk of ". $c . " rows inserted \n\r", false);
				}
			}
			
			if( $c % $row_limit !== 0 ){
				$bulk_object->insertBulk($cli);
				$cli->out("Bulk of ". $c . " rows inserted \n\r", false);
			}
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
			$cli->colorize('red')->out('Could not insert comment #' . $e->getMessage() )->reset();
			$log->error('Error when inserting comment #' . $e->getMessage());
		}
		$cli->out('Successfully inserted ' . $c . ' comments.');
		// </editor-fold>
		$end_time = microtime(true);
        // Just print the execution time
        $cli->colorize('purple')->out('Execution took ' . round($end_time - $start_time, 4) . ' seconds')->reset()->out();
		exit(0);
	}

	public function chatOnlineCronAction()
	{
		ini_set('memory_limit', '1024M');

		// <editor-fold defaultstate="collapsed" desc="Init CLI">
		$log = $this->_log = new Cubix_Cli_Log('/var/log/6a-chat-online-sync.log');
		$cli = $this->_cli = new Cubix_Cli();
		$cli->clear();
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Prevent from over-syncing">
		Cubix_Cli::setPidFile('/var/run/6a-chat-online-sync.pid');
		if ( Cubix_Cli::isRunning() ) {
			$cli->colorize('red')->out('Sync is already running, exitting...')->reset();
			exit(1);
		}
		// </editor-fold>

		$online_users = @file_get_contents('http://www.6annonce.com:35555/roomonlineusers.js?roomid=1');

		// Reset all online users
		$this->_db->update('escorts', array('is_online' => 0), $this->_db->quoteInto('is_online = ?', 1));

		if ( strlen($online_users) > 0 ) {
			$online_users = substr($online_users, 20, (strlen($online_users) - 21));

			$online_users = json_decode($online_users);

			$usernames = array();
			if ( count($online_users) > 0 ) {
				foreach ( $online_users as $user ) {
					$usernames[] = $user->name;
					echo "Escort {$user->name} is online now ! \n\r";
				}
			}
			
			$this->_db->query('UPDATE escorts SET is_online = 1 WHERE username IN (\'' . implode('\',\'', $usernames) . '\')');
		}

		die;
	}

	public function updateEscortsRandomNumbersAction()
	{
		ini_set('memory_limit', '1024M');

		// <editor-fold defaultstate="collapsed" desc="Init CLI">
		$log = $this->_log = new Cubix_Cli_Log('/var/log/update-random-escorts-' . Cubix_Application::getId() . '.log');
		$cli = $this->_cli = new Cubix_Cli();
		$cli->clear();
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Prevent from over-syncing">
		Cubix_Cli::setPidFile('/var/log/update-random-escorts-' . Cubix_Application::getId() . '.log');
		if ( Cubix_Cli::isRunning() ) {
			$cli->colorize('red')->out('Sync is already running, exitting...')->reset();
			exit(1);
		}
		// </editor-fold>

		$this->_db->query('UPDATE escorts SET ordering = RAND() * 100000');
		$this->_db->query('UPDATE escorts_in_cities SET ordering = RAND() * 100000');
		
		$cli->out('Successfully updated escort random numbers.');		
		
		
		$clear_cache = Model_Applications::clearCache();
		$cli->reset()->out($clear_cache); 
		
		exit(0);
	}

	public function updateEligibleForFreePackageListAction()
	{
		##### this sync made just for avoining API calls from PA
		ini_set('memory_limit', '1024M');

		
		$log = $this->_log = new Cubix_Cli_Log('/var/log/update-free-escorts-' . Cubix_Application::getId() . '.log');
		$cli = $this->_cli = new Cubix_Cli();
		$cli->clear();
		Cubix_Cli::setPidFile('/var/log/update-free-escorts-' . Cubix_Application::getId() . '.log');
		

		
		if ( Cubix_Cli::isRunning() ) {
			$cli->colorize('red')->out('Sync is already running, exitting...')->reset();
			exit(1);
		}
		
		$active_escorts = $this->_client->call('sync.getActiveEscortsIDS', array($escort_ids));

		$ids = '';
		foreach($active_escorts as $escort){
			$ids .= '('.$escort['id'].'),';
		}

		$ids = rtrim($ids, ',');
		
		###### adding new ids
		$result = $this->_db->query('INSERT IGNORE INTO free_5_days (escort_id) VALUES '.$ids);
		$cli->out('Successfully updated list - '.$result->rowCount().' added');				
		exit(0);
	}
	public function blockCountriesAction()
	{
		ini_set('memory_limit', '1024M');

		// <editor-fold defaultstate="collapsed" desc="Init CLI">
		$log = $this->_log = new Cubix_Cli_Log('/var/log/blocked-countries-sync.log');
		$cli = $this->_cli = new Cubix_Cli();
		$cli->clear();
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Prevent from over-syncing">
		Cubix_Cli::setPidFile('/var/run/blocked-countries-sync.pid');
		if ( Cubix_Cli::isRunning() ) {
			$cli->colorize('red')->out('Sync is already running, exitting...')->reset();
			exit(1);
		}
		// </editor-fold>
		
		// <editor-fold defaultstate="collapsed" desc="Transfer BL Countries">
		$chunk = 5000;
		$part = 0;
		$cli->out('Fetching data from api (chunk size: ' . $chunk . ')...');
		$bl_countries = array();
		do {
			$cli->reset()->out('part #' . ($part + 1), false);
			$data = $this->_client->call('getBlockedCountriesV2', array($part++ * $chunk, $chunk));
			if ( isset($data['error']) ) {
				$cli->column(60)->colorize('red')->out('error');
				$log->error('Error when fetching data from api: ' . $data['error']);
				continue;
			}

			$bl_countries = array_merge($bl_countries, $data['result']);
			$cli->column(60)->colorize('green')->out('success');
		} while ( count($data['result']) > 0 );
		$data = null; unset($data);
		$cli->reset()->out('Done fetching data (' . count($bl_countries) . ' bl countries fetched).')->out();
		// </editor-fold>
		
		// <editor-fold defaultstate="collapsed" desc="Insert data into database">
		$cli->out('Truncate table `escort_blocked_countries`', false);
		try {
			$this->_db->query('TRUNCATE escort_blocked_countries');
			$cli->column(60)->colorize('green')->out('success');
		}
		catch ( Exception $e ) {
			$cli->column(60)->colorize('red')->out('error');
			$log->error('Error when truncating escort_blocked_countries table: ' . $e->getMessage());
			exit(1);
		}

		$cli->reset()->out('Inserting Blocked Countries...');
		foreach ( $bl_countries as $bl_country ) {
			try {
				$this->_db->insert('escort_blocked_countries', $bl_country);
			}
			catch ( Exception $e ) {
				$log->error('Error when inserting Bl country of escort #' . $bl_country['escort_id'] . ': ' . $e->getMessage());
			}
		}
		$cli->out('Successfully inserted ');
		// </editor-fold>

		die;
	}
}
