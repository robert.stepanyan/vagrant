<?php

class OnlineBillingV3Controller extends Zend_Controller_Action
{
	public static $linkHelper;
	
	private static $sedcardPackages = array(24,25);
	private static $dayPassesPackages = array(19,20,23);
	private static $additionalAreaPrice = 200;
	private static $gotdPrice = 90;
	
	protected $_session;
	
	const PACKAGE_STATUS_PENDING  = 1;
	const PACKAGE_STATUS_ACTIVE   = 2;
	const PACKAGE_STATUS_EXPIRED  = 3;
	const PACKAGE_STATUS_CANCELLED = 4;
	const PACKAGE_STATUS_UPGRADED = 5;
	const PACKAGE_STATUS_SUSPENDED = 6;
	
	
	public function init()
	{	
		die;
		self::$linkHelper = $this->view->getHelper('GetLink'); 
		$this->view->layout()->setLayout('private-v2');
		
		$anonym = array('epg-response');
		
		$this->view->user = $this->user = Model_Users::getCurrent();
		
		if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}
		
		if ( ! in_array($this->_request->getActionName(), $anonym) && ! $this->user->isAgency() && ! $this->user->isEscort() ) {
			$this->_redirect($this->view->getLink('private-v2'));
		}
		
		$this->view->headTitle('Private Area', 'PREPEND');

		$this->_session = new Zend_Session_Namespace('online_billing_v3');
		$this->_session->setExpirationSeconds(60*60);
		
		$this->client = new Cubix_Api_XmlRpc_Client();
	}
	
	public function indexAction()
	{
		//die('Temporary issue, sorry for inconvenience');
		if ( $this->user->isAgency() ) {
			$this->_redirect(self::$linkHelper->getLink('online-billing-v3-select-escort'));
		} else {
			$this->_redirect(self::$linkHelper->getLink('online-billing-v3-select-package'));
		}
	}
	
	public function selectEscortAction()
	{	
		if ( ! $this->user->isAgency() ) {
			$this->_redirect(self::$linkHelper->getLink('online-billing-v3-select-package'));
		}
		
		if ( ! $this->_request->isPost() ) {
			$this->_session->selected_escort = $this->_session->selected_package = null;
			
			if ( ! $this->_request->cs ) {
				$this->_session->cart = array();
			}
			
			$this->view->cart = $this->_session->cart;
			$agency_id = $this->user->agency_data['agency_id'];
			
			$sort = $this->_request->sort ? $this->_request->sort : 'alphabetically';
			
			$this->view->sort = $sort;
			
			switch($sort){
				case 'alphabetically':
					$sort = 'e.showname ASC';
					BREAK;

				case 'id':
					$sort = 'e.id ASC';
					BREAK;

				case 'modified':
					$sort = 'e.date_last_modified DESC';
					BREAK;

				case 'newest':
					$sort = 'e.date_registered DESC';
					BREAK;
			}
			
			
			$escorts = $this->client->call('OnlineBillingV2.getAgencyEscorts', array($agency_id, $sort));
			
			foreach($escorts as $i => $escort) {
				//Refactor
				$escort = new Model_EscortV2Item($escort);
				$escorts[$i]['photo'] = $escort->getMainPhoto();
				
				
				
				if ( ( $escort['escort_packages'][0] && $escort['escort_packages'][0]['package_id'] == 14 ) ||
					( $escort['escort_packages'][1] && $escort['escort_packages'][1]['package_id'] == 14 )	) {
					unset($escorts[$i]);
				}
			}
			
			//Excluding escorts which are already in cart
			if ( count($this->_session->cart) ) {
				foreach($escorts as $i => $escort) {
					foreach($this->_session->cart as $order) {
						if ( $order['selected_escort']->id == $escort['id'] ) {
							unset($escorts[$i]);
						}
					}
				}
			}
			$this->view->escorts = $escorts;
		} else {
			if ( $this->_request->escort_id ) {
				$m_escorts = new Model_EscortsV2();
				$this->_session->selected_escort = $m_escorts->get($this->_request->escort_id);
				
				if ( $this->_request->pay_for_gotd ) {
					$this->_redirect(self::$linkHelper->getLink('online-billing-v3-select-optional-products'));
				} else {
					$this->_redirect(self::$linkHelper->getLink('online-billing-v3-select-package'));
				}
			}
		}
			
	}
	
	public function selectPackageAction()
	{
		$this->_session->selected_package = null;
		if ( $this->user->isAgency() ) {
			if ( ! $this->_session->selected_escort->id ) {
				$this->_redirect(self::$linkHelper->getLink('online-billing-v3-select-escort'));
			}
			$escort = $this->_session->selected_escort;
			$this->view->cart = $this->_session->cart;
		} else {
			$escort_id = $this->user->escort_data['escort_id'];
			$m_escorts = new Model_EscortsV2();
			$escort = $m_escorts->get($escort_id, null, true);
		}
		
		
		$this->view->escort = $escort;
		if ( $this->user->isAgency() ) {
			$user_type = USER_TYPE_AGENCY;
		} else {
			$user_type = USER_TYPE_SINGLE_GIRL;
			$is_pseudo_escort = $this->client->call('OnlineBillingV2.isPseudoEscort', array($escort->id));

			if ( $is_pseudo_escort ) {
				$user_type = USER_TYPE_AGENCY;
			}
		}
		$escort->is_pseudo_escort = $is_pseudo_escort;
		$this->_session->selected_escort = $escort;
	
		if ( ! $this->_request->isPost() ) {
			
			$this->view->existing_packages = $escort_packages = $this->client->call('OnlineBillingV2.checkIfHasPaidPackage', array($escort->id));
			
			// <editor-fold defaultstate="collapsed" desc="checking if escorts can buy package">
			$allow_buy_package = true;
			if ( count($escort_packages) ) {
				$current_package = $escort_packages[0];
				$pending_package = $escort_packages[1];

				if ( $current_package && ( $current_package['status'] == self::PACKAGE_STATUS_PENDING || $current_package['package_id'] == 14 /*Sc ABO CASE*/ ) ) {
					$allow_buy_package = false;
				} elseif ( $pending_package && $pending_package['status'] == self::PACKAGE_STATUS_PENDING ) {
					$allow_buy_package = false;
				}
			}

			if ( $escort->escort_status != ESCORT_STATUS_ACTIVE ) {
				$allow_buy_package = false;
			}

			$this->view->allow_buy_package = $allow_buy_package;
			// </editor-fold>

			$this->view->packages_list = $this->client->call('OnlineBillingV2.getPackagesList', array($user_type, $escort->gender, $is_pseudo_escort, self::$sedcardPackages));
			$this->view->day_passes_list = $this->client->call('OnlineBillingV2.getPackagesList', array($user_type, $escort->gender, $is_pseudo_escort, self::$dayPassesPackages));
		} else {
			if ( $this->_request->package_id ) {
				$selected_package = $this->client->call('OnlineBillingV2.getPackagesList', array($user_type, $escort->gender, $is_pseudo_escort, array($this->_request->package_id)));
				$this->_session->selected_package = $selected_package[0];
				if ( $this->_request->activation_date ) {
					$d = new DateTime($this->_request->activation_date);
					$this->_session->selected_package['custom_activation_date'] = $d->getTimestamp();
				}
				$this->_redirect(self::$linkHelper->getLink('online-billing-v3-select-optional-products'));
			}
		}
	}
	
	public function selectOptionalProductsAction()
	{
		$this->view->dayPassesPackages = self::$dayPassesPackages;
		try {
			if ( ! $this->_session->selected_escort->id ) {
				$this->_redirect(self::$linkHelper->getLink('online-billing-v3-select-escort'));
			}


			$this->view->booked_days = $booked_days = $this->client->call('Billing.getGotdBookedDays');
			$this->view->region_relations = $region_relations = array(
				/*45	=> array(45, 17),
				17	=> array(45, 17),

				21	=> array(21, 22),
				22	=> array(21, 22),

				44	=> array(44, 20),
				20	=> array(44, 20),*/
			);
			$this->view->add_area_price = self::$additionalAreaPrice;
			$this->view->gotd_price = self::$gotdPrice;
			$this->view->cart = $this->_session->cart;

			$this->view->escort = $escort = $this->_session->selected_escort;
			$escort_packages = $this->client->call('OnlineBillingV2.checkIfHasPaidPackage', array($escort->id));

			if ( $this->_session->selected_package ) {
				if ( count($escort_packages) ) {
					$activation_date = $escort_packages[0]['expiration_date'];
				} else {
					if ( $this->_session->selected_package['custom_activation_date'] ) {
						$activation_date = $this->_session->selected_package['custom_activation_date'];
					} else {
						$activation_date = mktime(0, 0, 0);
					}
				}
				$this->_session->selected_package['date_activated'] = $activation_date;
				$this->_session->selected_package['expiration_date'] = $activation_date + 24*60*60 * $this->_session->selected_package['period'];

				$this->view->selected_package = $this->_session->selected_package;

			} else {
				$this->view->packages = $packages = $this->client->call('Billing.getEscortPackagesForGotd', array($escort->id, Cubix_Application::getId()));
			}


			if ( $this->_request->isPost() ) {
				$this->view->additional_regions = $additional_regions = strlen($this->_request->additional_regions) ? explode(',', $this->_request->additional_regions) : array();
				$this->view->gotd_days = $gotd_days = $this->_request->gotd_days;

				if ( $this->_session->selected_package ) {
					$package = $this->_session->selected_package;
				} elseif ( count($packages) > 1 && ! strlen($this->_request->package) ) {
					$errors['package'] = Cubix_I18n::translate('selecte_package');
				} elseif ( count($packages) > 1 && strlen($this->_request->package) ) {
					$package = $packages[$this->_request->package];
				} else {
					$package = $packages[0];
				}

				if ( ! $this->_session->selected_package && ! count($gotd_days) && ! count($additional_regions) ) {
					$errors['days'] = Cubix_I18n::translate('select_product');
				}

				$g_p = array();
				$model_cities = new Model_Cities();
				foreach($gotd_days as $i => $day) {
					list($city_id, $date) = explode(':', $day);

					$gotd_days[$i] = array(
						'date'	=> $date,
						'title' => $model_cities->getById($city_id)->title,
						'city_id'	=> $city_id
					);

					// <editor-fold defaultstate="collapsed" desc="bringing date from js to php format">
					$date = explode('-', $date);
					if ( $date[1] == 12 ) {
						$date[1] = 1;
					} else {
						$date[1] += 1;
					}
					$day = mktime(5,0,0, $date[1], $date[2], $date[0]);
					// </editor-fold>

					if ( ! $g_p[$city_id] ) {
						$g_p[$city_id] = array($day);
					} else {
						$g_p[$city_id][] = $day;
					}




					//Checking date to be not booked
					$city_also_check[] = $city_id;
					if ( isset($region_relations[$city_id]) ) {
						$city_also_check = $region_relations[$city_id];
					}
					foreach($city_also_check as $ct) {
						foreach($booked_days[$ct] as $dt) {
							if ( date('Y-m-d', $day) == date('Y-m-d', strtotime($dt['date'])) ) {
								$errors['days'] = Cubix_I18n::translate('day_already_booked');
								break;
							}
						}
					}

					//Escort could have only one package per day
					foreach($booked_days as $city_id => $dt) {
						foreach($dt as $d) {
							if ( date('Y-m-d', $day) == date('Y-m-d', strtotime($d['date'])) && $escort->id == $d['escort_id'] ) {
								$errors['days'] = Cubix_I18n::translate('only_one_gotd_per_day');
							}
						}
					}

					//Checking date to be in package active range

					//If not in range of active and pending packages
					//add 5 hours to avoid daylight saving
					$package['expiration_date'] += 5 * 60 * 60;
					if ( $day <= $package['date_activated'] || $day > $package['expiration_date']  ) {
						$errors['days'] = Cubix_I18n::translate('date_must_be_in_package_range');
					}

				}
				$this->view->gotd_days = $gotd_days;
				if ( count($errors) ) {
					$this->view->gotd_errors = $errors;
				} else {

					$this->_session->cart[] = array(
						'selected_escort' => $escort,
						'selected_package' => $this->_session->selected_package ?  $this->_session->selected_package : null,
						'additional_regions'	=> $additional_regions,
						'gotd_days' => $g_p,
						'activation_date'	=> $this->_session->selected_package['custom_activation_date'] ? $this->_session->selected_package['custom_activation_date'] : null
					);
					$this->_redirect(self::$linkHelper->getLink('online-billing-v3-payment'));
				}


			}
		} catch(Exception $e) {
			
		}
		
	}
	
	public function paymentAction()
	{
		$cart = $this->_session->cart;
		
		if ( ! count($cart) ) {
			$this->_redirect(self::$linkHelper->getLink('online-billing-v3-select-escort'));
		}
		
		$city_model = new Model_Cities();
		$this->view->dayPassesPackages = self::$dayPassesPackages;
		
		$allow_pay_by_phone = false;
		if ( count($cart) == 1 ) {
			$first_key = array_keys($cart);
			$first_key = $first_key[0];
			$c = $cart[$first_key];
			
			$gotds = reset($c['gotd_days']);

			if ( in_array($c['selected_package']['id'], self::$dayPassesPackages) && count($c['additional_regions']) == 0 && count($c['gotd_days']) == 0 
				|| ( ! $c['selected_package'] && count($c['gotd_days']) == 1 && count($gotds) == 1)
			) {
				$allow_pay_by_phone = true;
			}
		}
		$this->view->allow_pay_by_phone = $allow_pay_by_phone;
		
		if ( ! $this->_request->isPost() ) {
			foreach($cart as $i => $order)  {
				$price = 0;

				if ( $order['selected_package'] ) {
					$price += (int)$order['selected_package']['price'];
				}

				if ( count($order['additional_regions']) ) {
					$cart[$i]['additional_regions_titles'] = $city_model->getByIds(implode(',', $order['additional_regions']));
					$price += self::$additionalAreaPrice * count($order['additional_regions']);
				}

				if ( count($order['gotd_days']) ) {
					$gotd_days_count = 0;
					$cities = $city_model->GetByIds(implode(',', array_keys($order['gotd_days'])));

					$g_d = array();
					foreach($cities as $city) {
						if ( ! $g_d[$city->id] ) {
							$g_d[$city->id] = array(
								'title'	=> $city->title,
								'days' => array()
							);
						}

						foreach($order['gotd_days'][$city->id] as $day) {
							$gotd_days_count +=1;
							$g_d[$city->id]['days'][] = $day;
						}

					}

					$cart[$i]['gotd_days_titles'] = $g_d;

					$price += self::$gotdPrice * $gotd_days_count;


				}
				$cart[$i]['order_price'] = $price;
			}
			$this->view->cart = $cart;
		} else {
			$this->_session->selected_escort = null;
			$this->_session->selected_package = null;
			$this->_session->cart = array();
			
			
			$post_data = array();
			$booked_gotds_order_ids = array();
			foreach($cart as $c) {
				$products_data = array();
				if ( count($c['additional_regions']) ) {
					foreach($c['additional_regions'] as $i) {
						$products_data['optional_products'] = array(18);
					}

					$products_data['additional_areas'] = $c['additional_regions'];
				}

				if ( count($c['gotd_days']) ) {

					$products_data['gotd'] = array();
					foreach($c['gotd_days'] as $city_id => $dates) {
						$result = $this->client->call('Billing.bookGotd', array($c['selected_escort']->id, $city_id, $dates));
						if ( $result ) {
							$products_data['gotd'][] = $result;
							$booked_gotds_order_ids[] = $result;
						}

					}
				}

				if ( $c['activation_date'] ) {
					$products_data['activation_date'] = $c['activation_date'];
				}

				$post_data[] = array(
					'user_id' => $this->user->id,
					'escort_id' => $c['selected_escort']->id,
					'agency_id' => $this->user->isAgency() ? $this->user->agency_data['agency_id'] : null,
					'package_id' => $c['selected_package']['id'],
					'data' => serialize($products_data)
				);
			}

			if ( ! $c['selected_package'] && count($booked_gotds_order_ids) == 1 & $this->_request->payment_method == 'phone') {
				$this->_redirect($this->getIvrGotdUrl(self::$gotdPrice, $booked_gotds_order_ids[0]));
			}


			$amount = $this->client->call('OnlineBillingV2.addToShoppingCart', array($post_data, $this->user->id, $this->user->isAgency() ? USER_TYPE_AGENCY : USER_TYPE_SINGLE_GIRL));

			if ( Cubix_Application::getId() == APP_A6 && ! $c['selected_package'] && count($c['gotd_days']) ) {
				$i = 0;
				foreach($c['gotd_days'] as $gd) {
					$i += count($gd);
				}
				$amount = self::$gotdPrice * $i;
			}
			
			if ( $this->_request->payment_method == 'phone' && $allow_pay_by_phone ) {
				$reference = 'SC3_' . $this->user->id . '_' . md5(time());
				$this->_redirect($this->getIvrUrl($amount, $reference));
			} else {
				//die("Credit Card payments are temporarily unavailable, please try later!");
				$epg_payment = new Model_EpgGateway();
				$reference = 'SC3-' . $this->user->id . '-' . md5(time());
				$token = $epg_payment->getTokenForAuth($reference, $amount, 'http://www.and6.com/online-billing-v3/epg-response');
				$this->client->call('OnlineBillingV2.storeToken', array($token, $this->user->id));

				$this->_redirect($epg_payment->getPaymentGatewayUrl($token));
			}
		}
		
	}
	
	public function epgResponseAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$token = $req->Token;
		if ( ! $token ) $this->_redirect('/');
		
		$epg_payment = new Model_EpgGateway();
		$result = $epg_payment->getTransactionStatus($token);
		
		if ( $result['ResultStatus'] == "OK" && $result['TransactionResult'] == "OK" ) {
			$this->_redirect(self::$linkHelper->getLink('online-billing-v3-success'));
		} else {
			$this->_redirect(self::$linkHelper->getLink('online-billing-v3-failure'));
		}
	}
	
	public function successAction()
	{
	}
	
	public function failureAction()
	{
		
	}
	
	public function removeFromCartAction()
	{
		$escort_id = $this->_request->escort_id;
		
		foreach($this->_session->cart as $i => $cart) {
			if ( $cart['selected_escort']->id == $escort_id ) {
				unset($this->_session->cart[$i]);
			}
		}
		
		die;
	}
	
	private function getIvrUrl($amount, $client_identifier)
	{
		if ( ! $amount || ! $client_identifier ) {
			return self::$linkHelper->getLink('private-v2-advertise-now');
		}
		
		
		$amount *= 100;
		
		
		$conf = Zend_Registry::get('ivr');
		
		$urls = array(
			'base_url'	=> 'http://ivr.truesenses.com/?cmd=new&acc=' . $conf['acc'] . '&aid=' . $conf['aid'] . '&amo=' . $amount . '&cur=chf&ser=' . $conf['acc'] . '&lan=de&cid=' . $client_identifier,
			'success_url'	=> 'srl=' . 'http://' . $_SERVER['SERVER_NAME'] . self::$linkHelper->getLink('online-billing-v3-success'),
			'failure_url'	=> 'frl=' . 'http://' . $_SERVER['SERVER_NAME'] . self::$linkHelper->getLink('online-billing-v3-failure'),
			'cancel_url'	=> 'crl=' . 'http://' . $_SERVER['SERVER_NAME'] . self::$linkHelper->getLink('online-billing-v3-select-escort')
		);
		$url = implode('&', $urls);
		
		return $url;
	}

	private function getIvrGotdUrl($amount, $client_identifier)
	{
		$amount *= 100;
		$conf = Zend_Registry::get('ivr');
		$urls = array(
			'base_url'	=> 'http://ivr.truesenses.com/?cmd=new&acc=' . $conf['acc'] . '&aid=' . $conf['aid'] . '&amo=' . $amount . '&cur=chf&ser=' . $conf['accGotd'] . '&lan=de&cid=' . $client_identifier,
			'success_url'	=> 'srl=' . 'http://' . $_SERVER['SERVER_NAME'] . self::$linkHelper->getLink('online-billing-v3-success'),
			'failure_url'	=> 'frl=' . 'http://' . $_SERVER['SERVER_NAME'] . self::$linkHelper->getLink('online-billing-v3-failure'),
			'cancel_url'	=> 'crl=' . 'http://' . $_SERVER['SERVER_NAME'] . self::$linkHelper->getLink('online-billing-v3-select-escort')
		);
		
		$url = implode('&', $urls);
		
		return $url;
	}
}
