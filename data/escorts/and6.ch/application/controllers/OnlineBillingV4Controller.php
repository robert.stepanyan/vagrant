<?php

class OnlineBillingV4Controller extends Zend_Controller_Action
{
	public static $linkHelper;

	private static $sedcardPackages = array(24,25);
	private static $dayPassesPackages = array(19,20,23);
	private static $additionalAreaPrice = 200;
	private static $gotdPrice = 90;
	private static $votdPrice = 90;
	private static $gotdCryptoPrice = 190;
	private static $votdCryptoPrice = 190;

	protected $_session;

	const PACKAGE_STATUS_PENDING  = 1;
	const PACKAGE_STATUS_ACTIVE   = 2;
	const PACKAGE_STATUS_EXPIRED  = 3;
	const PACKAGE_STATUS_CANCELLED = 4;
	const PACKAGE_STATUS_UPGRADED = 5;
	const PACKAGE_STATUS_SUSPENDED = 6;


	public function init()
	{
		self::$linkHelper = $this->view->getHelper('GetLink');
		$this->view->layout()->setLayout('private-v3');
		$anonym = array('epg-response', 'twispay-response', 'failure', 'success', 'btc-success',
			'faxin-response-success','faxin-response-cancel', 'faxin-response-failure');

		$this->view->user = $this->user = Model_Users::getCurrent();

		if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}

        if ( ! in_array($this->_request->getActionName(), $anonym) && ! $this->user->isAgency() && ! $this->user->isEscort() ) {
			$this->_redirect($this->view->getLink('private-v3'));
		}

		if ( $this->user && $this->user->isAgency() ) {
            $cache = Zend_Registry::get('cache');
			$cache_key =  'v2_user_pva_' . $this->user->id;
			if ( ! $agency = $cache->load($cache_key) ) {
				$agency = $this->user->getAgency();
				$cache->save($agency, $cache_key, array(), 300);
			}
			
			if( $agency->status != AGENCY_STATUS_ONLINE ){
				$this->_redirect($this->view->getLink('private-v2'));
			}
		}
		
		$this->view->headTitle('Private Area', 'PREPEND');

		$this->_session = new Zend_Session_Namespace('online_billing_v4');

		$this->_session->setExpirationSeconds(60*60);

		$this->client = new Cubix_Api_XmlRpc_Client();
	}

	public function indexAction()
	{
		//die('Temporary issue, sorry for inconvenience');
		if ( $this->user->isAgency() ) {
			$this->_redirect(self::$linkHelper->getLink('online-billing-v4-select-escort'));
		} else {
			$this->_redirect(self::$linkHelper->getLink('online-billing-v4-select-package'));
		}
	}

	public function selectEscortAction()
	{
		
		if ( ! $this->user->isAgency() ) {
			$this->_redirect(self::$linkHelper->getLink('online-billing-v4-select-package'));
		}
		
		$user_type = USER_TYPE_AGENCY;


			$allow_buy_package = true;
			$this->view->gotd = $this->_request->gotd ? true : false;
			$this->view->votd = $this->_request->votd ? true : false;

			$this->view->allow_buy_package = $allow_buy_package;
			$this->view->gotd_price = self::$gotdPrice;
			$this->view->votd_price = self::$votdPrice;
			$this->view->gotd_crypto_price = self::$gotdCryptoPrice;
			$this->view->votd_crypto_price = self::$votdCryptoPrice;
			$this->view->add_area_price = self::$additionalAreaPrice;
			$this->view->booked_days = $booked_days = $this->client->call('Billing.getGotdBookedDays');
			// </editor-fold>
			$this->view->votd_booked_days = $votd_booked_days = $this->client->call('Billing.getVotdBookedDays');
			$this->view->packages_list = $this->client->call('OnlineBillingV2.getPackagesList', array($user_type, null, null, self::$sedcardPackages));
			$this->view->day_passes_list = $this->client->call('OnlineBillingV2.getPackagesList', array($user_type, null,null, self::$dayPassesPackages));


		
			
		if ( ! $this->_request->isPost() ) {



			$this->_session->selected_escort = $this->_session->selected_package = null;

			if ( ! $this->_request->cs ) {
				$this->_session->cart = array();
			}
			
			$cache = Zend_Registry::get('cache');
			$counter = $cache->load('card_gateway_show_counter');
			if ($counter) {
				$this->view->alternative_gateway = ($counter%2 == 0) ? true : false;
				$cache->save($counter + 1, 'card_gateway_show_counter');
			} else {
				$cache->save(1, 'card_gateway_show_counter');
				$this->view->alternative_gateway = false;
			}
			
			$this->view->cart = $this->_session->cart;
			$agency_id = $this->user->agency_data['agency_id'];

			$sort = $this->_request->sort ? $this->_request->sort : 'alphabetically';

            $_model_payments = new Model_PaymentMethodes();
            $this->view->activ_payments =  $_model_payments->getActivePaymentMethodes(Cubix_Application::getId());

			$this->view->sort = $sort;

			switch($sort){
				case 'alphabetically':
					$sort = 'e.showname ASC';
					BREAK;

				case 'id':
					$sort = 'e.id ASC';
					BREAK;

				case 'modified':
					$sort = 'e.date_last_modified DESC';
					BREAK;

				case 'newest':
					$sort = 'e.date_registered DESC';
					BREAK;
			}


			$escorts = $this->client->call('OnlineBillingV2.getAgencyEscortsA6', array($agency_id, $sort));
			
			//if($agency_id == 47){
				//$start_time = microtime(true);
				
			//}
			try{
				foreach($escorts as $i => $escort) {
					
					if($agency_id == 47 || $agency_id == 3039){

						$escort = new Model_EscortV3Item($escort);
						/*if( $i > 200){
							$end_time = microtime(true);
							$time = round($end_time - $start_time, 4);
							//echo $time;die;
						}*/
					}else{
						$escort = new Model_EscortV2Item($escort);
					}
					$cur_main_photo = $escort->getMainPhoto();

					$escorts[$i]['photo'] = $cur_main_photo->getUrl('ob4_thumb');
					$m_video = new Model_Video();
					//$escorts[$i]['has_approved_video'] = $m_video->GetEscortHasApprovedVideo($escort->id);

					if (isset($escort['escort_packages']) && (isset($escort['escort_packages'][0]) && $escort['escort_packages'][0] && $escort['escort_packages'][0]['package_id'] == 14 ) ||
						( isset($escort['escort_packages'][1]) && $escort['escort_packages'][1] && $escort['escort_packages'][1]['package_id'] == 14 )	) {
						unset($escorts[$i]);
					}
				}
			}
			catch(Exception $e) {
					$message = $e->getMessage();
					var_dump($message); die();
				}

			if ($this->_request->ajax){
				echo json_encode($escorts); die();
			}

			$this->view->escorts = $escorts;
			$this->view->agency_id = $this->user->agency_data['agency_id'];
			$this->view->agency_escorts_count = count($escorts);

		} else {

			if ( $this->_request->escort_id ) {
				$m_escorts = new Model_EscortsV2();
				$this->_session->selected_escort = $m_escorts->get($this->_request->escort_id);

				if ( $this->_request->pay_for_gotd ) {
					$this->_redirect(self::$linkHelper->getLink('online-billing-v3-select-optional-products'));
				} else {
					$this->_redirect(self::$linkHelper->getLink('online-billing-v3-select-package'));
				}
			}
		}
		
		
	}

	public function selectPackageAction()
	{
		$start_time = microtime(true);
		if ( $this->user->isAgency() ) {
			if ( ! $this->_session->selected_escort->id ) {
				$this->_redirect(self::$linkHelper->getLink('online-billing-v4-select-escort'));
			}
			$escort = $this->_session->selected_escort;
			$this->view->cart = $this->_session->cart;
		} else {

		
			$escort_id = $this->user->escort_data['escort_id'];
			$m_escorts = new Model_EscortsV2();

			$escort = $m_escorts->get($escort_id, null, true);

			$this->view->gotd = $this->_request->gotd ? true : false;
			$this->view->votd = $this->_request->votd ? true : false;
			
			//$m_video = new Model_Video();
			//$this->view->has_approved_video = $has_approved_video = $m_video->GetEscortHasApprovedVideo($escort->id);
			if($escort->video_id){
				$this->view->has_approved_video = $has_approved_video = true;
			}else{
				$this->view->has_approved_video = $has_approved_video = false;
			}
			
			$this->view->w139 = true;
			$this->view->err = $this->_request->err ? true : false;
			
			//disabling free 5 package
			//$this->view->eligible_to_have_5_days  =  $m_escorts->eligible_to_have_5_days($escort->id);
			$this->view->eligible_to_have_5_days  = false;
		}

        $_model_payments = new Model_PaymentMethodes();
        $this->view->activ_payments =  $_model_payments->getActivePaymentMethodes(Cubix_Application::getId());

		$this->view->escort = $escort;
		if ( $this->user->isAgency() ) {
			$user_type = USER_TYPE_AGENCY;
		} else {
			$user_type = USER_TYPE_SINGLE_GIRL;
			$is_pseudo_escort = $m_escorts->isPseudoEscort($escort->id);

			if ( $is_pseudo_escort ) {
				$user_type = USER_TYPE_AGENCY;
			}
		}

		$escort->is_pseudo_escort = $is_pseudo_escort;
		$this->_session->selected_escort = $escort;
		
		if ( ! $this->_request->isPost() ) {

			// $cache = Zend_Registry::get('cache');
			// $counter = $cache->load('card_gateway_show_counter');
			// if ($counter) {
			// 	$this->view->alternative_gateway = ($counter%2 == 0) ? true : false;
			// 	$cache->save($counter + 1, 'card_gateway_show_counter');
			// } else {
			// 	$cache->save(1, 'card_gateway_show_counter');
			// 	$this->view->alternative_gateway = false;
			// }
			
			$this->view->alternative_gateway = true;

			$this->view->existing_packages = $escort_packages = $this->client->call('OnlineBillingV2.checkIfHasPaidPackage', array($escort->id));

			$allow_buy_package = true;
			if ( count($escort_packages) ) {
				$current_package = $escort_packages[0];
				$pending_package = $escort_packages[1];

				if ( $current_package && ( $current_package['status'] == self::PACKAGE_STATUS_PENDING || $current_package['package_id'] == 14) ) {
					$allow_buy_package = false;
				} elseif ( $pending_package && $pending_package['status'] == self::PACKAGE_STATUS_PENDING ) {
					$allow_buy_package = false;
				}
			}
			
			if (!($escort->escort_status & ESCORT_STATUS_ACTIVE) && !( $escort->escort_status & ESCORT_STATUS_OWNER_DISABLED) ) {
				$allow_buy_package = false;
			}
			
			$this->view->allow_buy_package = $allow_buy_package;
			
			$this->view->packages_list = $this->client->call('OnlineBillingV2.getPackagesList', array($user_type, $escort->gender, $is_pseudo_escort, self::$sedcardPackages));
			$this->view->day_passes_list = $this->client->call('OnlineBillingV2.getPackagesList', array($user_type, $escort->gender, $is_pseudo_escort, self::$dayPassesPackages));
			
		} else {

			if ( $this->_request->package_id ) {
				$selected_package = $this->client->call('OnlineBillingV2.getPackagesList', array($user_type, $escort->gender, $is_pseudo_escort, array($this->_request->package_id)));
				$this->_session->selected_package = $selected_package[0];
				if ( $this->_request->activation_date ) {
					$d = new DateTime($this->_request->activation_date);
					$this->_session->selected_package['custom_activation_date'] = $d->getTimestamp();
				}
				$this->_redirect(self::$linkHelper->getLink('online-billing-v3-select-optional-products'));
			}
		}
		
		$this->view->dayPassesPackages = self::$dayPassesPackages;
		try {

			if ( ! $this->_session->selected_escort->id ) {
				$this->_redirect(self::$linkHelper->getLink('private-v2'));
			}


			$this->view->booked_days = $booked_days = $this->client->call('Billing.getGotdBookedDays');
			$this->view->votd_booked_days = $votd_booked_days = $this->client->call('Billing.getVotdBookedDays');
			$this->view->region_relations = $region_relations = array(
				/*45	=> array(45, 17),
				17	=> array(45, 17),

				21	=> array(21, 22),
				22	=> array(21, 22),

				44	=> array(44, 20),
				20	=> array(44, 20),*/
			);
			$this->view->votd_region_relations = $votd_region_relations = array();
			$this->view->add_area_price = self::$additionalAreaPrice;
			$this->view->gotd_price = self::$gotdPrice;
			$this->view->votd_price = self::$votdPrice;
			$this->view->gotd_crypto_price = self::$gotdCryptoPrice;
			$this->view->votd_crypto_price = self::$votdCryptoPrice;
			$this->view->cart = $this->_session->cart;

			$this->view->escort = $escort = $this->_session->selected_escort;

			if($escort_packages ===  null ){
				$escort_packages = $this->client->call('OnlineBillingV2.checkIfHasPaidPackage', array($escort->id));
			}

			if ( $this->_session->selected_package ) {
				if ( count($escort_packages) ) {
					$activation_date = $escort_packages[0]['expiration_date'];
				} else {
					if ( $this->_session->selected_package['custom_activation_date'] ) {
						$activation_date = $this->_session->selected_package['custom_activation_date'];
					} else {
						$activation_date = mktime(0, 0, 0);
					}
				}
				$this->_session->selected_package['date_activated'] = $activation_date;
				$this->_session->selected_package['expiration_date'] = $activation_date + 24*60*60 * $this->_session->selected_package['period'];

				$this->view->selected_package = $this->_session->selected_package;

			}
			
			if ( $this->_request->isPost() ) {

				$this->view->additional_regions = $additional_regions = strlen($this->_request->additional_regions) ? explode(',', $this->_request->additional_regions) : array();
				$this->view->gotd_days = $gotd_days = $this->_request->gotd_days;
				$this->view->votd_days = $votd_days = $this->_request->votd_days;
				$packages = $this->client->call('Billing.getEscortPackagesForGotd', array($escort->id, Cubix_Application::getId()));
				
				if ( $this->_session->selected_package ) {
					$package = $this->_session->selected_package;
				} elseif ( count($packages) > 1 && ! strlen($this->_request->package) ) {
					$errors['package'] = Cubix_I18n::translate('selecte_package');
				} elseif ( count($packages) > 1 && strlen($this->_request->package) ) {
					$package = $packages[$this->_request->package];
				} else {
					$package = $packages[0];
				}

				if ( ! $this->_session->selected_package && ! count($gotd_days) && ! count($additional_regions) ) {
					$errors['days'] = Cubix_I18n::translate('select_product');
				}

				$g_p = array();
				$model_cities = new Model_Cities();
				foreach($gotd_days as $i => $day) {
					list($city_id, $date) = explode(':', $day);

					$gotd_days[$i] = array(
						'date'	=> $date,
						'title' => $model_cities->getById($city_id)->title,
						'city_id'	=> $city_id
					);

					$date = explode('-', $date);
					if ( $date[1] == 12 ) {
						$date[1] = 1;
					} else {
						$date[1] += 1;
					}
					$day = mktime(5,0,0, $date[1], $date[2], $date[0]);

					if ( ! $g_p[$city_id] ) {
						$g_p[$city_id] = array($day);
					} else {
						$g_p[$city_id][] = $day;
					}




					//Checking date to be not booked
					$city_also_check[] = $city_id;
					if ( isset($region_relations[$city_id]) ) {
						$city_also_check = $region_relations[$city_id];
					}
					foreach($city_also_check as $ct) {
						foreach($booked_days[$ct] as $dt) {
							if ( date('Y-m-d', $day) == date('Y-m-d', strtotime($dt['date'])) ) {
								$errors['days'] = Cubix_I18n::translate('day_already_booked');
								break;
							}
						}
					}

					//Escort could have only one package per day
					foreach($booked_days as $city_id => $dt) {
						foreach($dt as $d) {
							if ( date('Y-m-d', $day) == date('Y-m-d', strtotime($d['date'])) && $escort->id == $d['escort_id'] ) {
								$errors['days'] = Cubix_I18n::translate('only_one_gotd_per_day');
							}
						}
					}

					//Checking date to be in package active range

					//If not in range of active and pending packages
					//add 5 hours to avoid daylight saving
					$package['expiration_date'] += 5 * 60 * 60;
					if ( $day <= $package['date_activated'] || $day > $package['expiration_date']  ) {
						$errors['days'] = Cubix_I18n::translate('date_must_be_in_package_range');
					}

				}
				$this->view->gotd_days = $gotd_days;
				$v_p = array();
				foreach($votd_days as $i => $day) {
					list($city_id, $date) = explode(':', $day);

					$votd_days[$i] = array(
						'date'	=> $date,
						'title' => $model_cities->getById($city_id)->title,
						'city_id'	=> $city_id
					);

					$date = explode('-', $date);
					if ( $date[1] == 12 ) {
						$date[1] = 1;
					} else {
						$date[1] += 1;
					}
					$day = mktime(5,0,0, $date[1], $date[2], $date[0]);

					if ( ! $v_p[$city_id] ) {
						$v_p[$city_id] = array($day);
					} else {
						$v_p[$city_id][] = $day;
					}




					//Checking date to be not booked
					$v_city_also_check[] = $city_id;
					if ( isset($votd_region_relations[$city_id]) ) {
						$v_city_also_check = $votd_region_relations[$city_id];
					}
					foreach($v_city_also_check as $ct) {
						foreach($votd_booked_days[$ct] as $dt) {
							if ( date('Y-m-d', $day) == date('Y-m-d', strtotime($dt['date'])) ) {
								$v_errors['days'] = Cubix_I18n::translate('day_already_booked');
								break;
							}
						}
					}

					//Escort could have only one package per day
					foreach($votd_booked_days as $city_id => $dt) {
						foreach($dt as $d) {
							if ( date('Y-m-d', $day) == date('Y-m-d', strtotime($d['date'])) && $escort->id == $d['escort_id'] ) {
								$v_errors['days'] = Cubix_I18n::translate('only_one_votd_per_day');
							}
						}
					}

					//Checking date to be in package active range

					//If not in range of active and pending packages
					//add 5 hours to avoid daylight saving
					$package['expiration_date'] += 5 * 60 * 60;
					if ( $day <= $package['date_activated'] || $day > $package['expiration_date']  ) {
						$v_errors['days'] = Cubix_I18n::translate('date_must_be_in_package_range');
					}

				}
				$this->view->votd_days = $votd_days;
				if ( !count($errors) && !count($v_errors)) {
					$this->_session->cart[] = array(
						'selected_escort' => $escort,
						'selected_package' => $this->_session->selected_package ?  $this->_session->selected_package : null,
						'additional_regions'	=> $additional_regions,
						'gotd_days' => $g_p,
						'votd_days' => $v_p,
						'activation_date'	=> $this->_session->selected_package['custom_activation_date'] ? $this->_session->selected_package['custom_activation_date'] : null
					);
					$this->_redirect(self::$linkHelper->getLink('online-billing-v3-payment'));
				} else {
					if(count($errors)){
						$this->view->gotd_errors = $errors;
					}
					if(count($v_errors)){
						$this->view->votd_errors = $v_errors;
					}

				}
			}
		} catch(Exception $e) {

		}
	}

	public function generateCartItemAction()
	{
		$cart = $_POST;
		$phone_payment = false;
		$card_payment = false;
		//var_dump($cart);die;
		if ($cart['payment_method'] == 'phone'){
			$phone_payment = true;
		} else {
			$card_payment_gateway = $cart['payment_method'];
		}

		unset($cart['payment_method']);

		if ( ! count($cart) && $this->user->isAgency()) {
			$this->_redirect(self::$linkHelper->getLink('online-billing-v4-select-escort'));
		} elseif (!count($cart) && $this->user->isEscort()){
			$this->_redirect(self::$linkHelper->getLink('online-billing-v4-select-package'));
		}

		$post_data = array();
		$booked_gotds_order_ids = array();
		$booked_votds_order_ids = array();
		
		/* 
			as fucken mmgbill has limitation for reference we have to make hash shorter
			will work fine till 2038-12-24 09:45:35 -- zzzzzz after will become 7 symbols 
		*/
		$hash  = base_convert(time(), 10, 36);
		
		foreach($cart as $c) {
			$products_data = array();
			if ( count($c['additional_regions']) ) {
				foreach($c['additional_regions'] as $i) {
					$products_data['optional_products'] = array(18);
				}

				$products_data['additional_areas'] = $c['additional_regions'];
			}

			if ( count($c['gotd_days']) ) {
				$products_data['gotd'] = array();
				foreach ($c['gotd_days'] as $ct) {
					foreach($ct as $city_id => $dates) {
						
						foreach($dates as $key => $date){
							$dates[$key] = strtotime($date);
						}
						
						$result = $this->client->call('Billing.bookGotd', array($c['escort_id'], $city_id, $dates));
						if ( $result ) {
							$products_data['gotd'][] = $result;
							$booked_gotds_order_ids[] = $result;
						} else{
							echo json_encode(array( "status" => 0, 'msg' => 'error_gotd')); die;
						}
					}
				}

			}

			if ( count($c['votd_days']) ) {
				$products_data['votd'] = array();
				foreach ($c['votd_days'] as $ct) {
					foreach($ct as $city_id => $dates) {
						
						foreach($dates as $key => $date){
							$dates[$key] = strtotime($date);
						}
						
						$result = $this->client->call('Billing.bookVotd', array($c['escort_id'], $city_id, $dates));
						if ( $result ) {
							$products_data['votd'][] = $result;
							$booked_votds_order_ids[] = $result;
						} else{
							echo json_encode(array( "status" => 0, 'msg' => 'error_votd')); die;
						}
					}
				}

			}

			if ( $c['activation_date'] ) {
				$products_data['activation_date'] = $c['activation_date'];
			}
			$post_data[] = array(
				'user_id' => $this->user->id,
				'escort_id' => $c['escort_id'],
				'agency_id' => $this->user->isAgency() ? $this->user->agency_data['agency_id'] : null,
				'package_id' => $c['package_id'],
				'hash' => $hash,
				'data' => serialize($products_data)
			);
		}
		
		if ( count($post_data) == 1 && ! $c['package_id'] && count($booked_gotds_order_ids) == 1 && count($booked_votds_order_ids) == 0 && $phone_payment) {
			echo json_encode(array( "status" => 1, "url" => $this->getIvrGotdUrl(self::$gotdPrice, $booked_gotds_order_ids[0]) )); die;
			// $this->_redirect($this->getIvrGotdUrl(self::$gotdPrice, $booked_gotds_order_ids[0]));
		}

		if ( count($post_data) == 1 && ! $c['package_id'] && count($booked_votds_order_ids) == 1 && $phone_payment) {
			echo json_encode(array( "status" => 1, "url" => $this->getIvrVotdUrl(self::$votdPrice, $booked_votds_order_ids[0]) )); die;
			// $this->_redirect($this->getIvrGotdUrl(self::$gotdPrice, $booked_gotds_order_ids[0]));
		}


		//inch vor urbat irikunov zangel en axper chi ashxatum naum em esi redirecta anum :D prkya mez ter
        // foreach ($post_data as $shoppingCartItem) {
        // 	var_dump($shoppingCartItem);die;
        //     if (!intval($shoppingCartItem['package_id']) && preg_match('[gotd|votd]') === false) {
        //         echo json_encode(array( "status" => 0, "url" => self::$linkHelper->getLink('online-billing-v4-select-package'))); die;
        //     }
        // }

		$is_crypto = $card_payment_gateway == 'coinpass' ? true : false;
		$amount = $this->client->call('OnlineBillingV2.addToShoppingCart', array($post_data, $this->user->id, $this->user->isAgency() ? USER_TYPE_AGENCY : USER_TYPE_SINGLE_GIRL , $hash, $is_crypto) );
		
		foreach($cart as $c) {
			if ( !$c['package_id'] && count($c['gotd_days']) ) {
				$gotd_days_count = 0;


				foreach ($c['gotd_days'] as $booked_gotds) {
					foreach ($booked_gotds as $i) {
						$gotd_days_count += count($i);
					}
				}
				
				$gotd_price = $is_crypto ? self::$gotdCryptoPrice : self::$gotdPrice;
				
				$amount += $gotd_price * $gotd_days_count;
			}

			if ( !$c['package_id'] && count($c['votd_days']) ) {
				$votd_days_count = 0;


				foreach ($c['votd_days'] as $booked_votds) {
					foreach ($booked_votds as $i) {
						$votd_days_count += count($i);
					}
				}

				$votd_price = $is_crypto ? self::$votdCryptoPrice : self::$votdPrice;
				$amount += $votd_price * $votd_days_count;
			}
		}
		
		
		######## free 5 days for covid-19 march active girls
		######## https://sceonteam.atlassian.net/browse/A6-94
		extract($cart[0]);
		
		if($package_id == 26 && $amount == 0){
			if($this->user['escort_data']['escort_id'] != $escort_id){
				echo json_encode(array( "status" => 1, "url" => self::$linkHelper->getLink('online-billing-v4-success') )); die;
			}
			$result = $this->client->call('OnlineBillingV2.activate_free_package_a6', array( $package_id, $escort_id, $hash));

			if($result){
				$result = Model_EscortsV2::activate_5_days($escort_id);
				echo json_encode(array( "status" => 1, "url" => self::$linkHelper->getLink('online-billing-v4-success').'?free_5_days=1' )); die;
			}else{
				echo json_encode(array( "status" => 1, "url" => self::$linkHelper->getLink('online-billing-v4-failure') )); die;
			}
		}
		############

		if ( $phone_payment ) {
			$reference = 'SC3_' . $this->user->id . '_' . $hash;
			echo json_encode(array( "status" => 1, "url" => $this->getIvrUrl($amount, $reference) )); die;
		} else {
			if ($card_payment_gateway == 'twispay')
			{

                 /*if ($this->user->id == 48322) {
                 	$amount = 1;
                 }*/

				$paymentConfigs = Zend_Registry::get('payment_config');
				$twispayConfigs = $paymentConfigs['twispay'];

                $data = array(
                    'siteId' => intval($twispayConfigs['siteid']),
                    'cardTransactionMode' => 'authAndCapture',
                    'backUrl' => APP_HTTP . '://' . $_SERVER['HTTP_HOST'] . '/online-billing-v4/twispay-response',
                    'invoiceEmail' => '',
                    'customer' => [
                        'identifier' => 'user-' . $this->user->id,
                        'firstName' => $this->user->username,
                        'username' => $this->user->username,
                        'email' => $this->user->email,
                    ],
                    'order' => [
                        'orderId' => 'SC3-' . $this->user->id . '-' . $hash,
                        'type' => 'purchase',
                        'amount' => $amount,
                        'currency' => $twispayConfigs['currency'],
                        'description' => 'Self Checkout And6.com',
                    ]
                );

                // Store the gateway token for backend to get details about the transaction
                // using this information
                $token = json_encode(['externalOrderId' => $data['order']['orderId']]);
                $isOrderCreated = $this->client->call('OnlineBillingV2.storeToken', array($token, $this->user->id, 'twispay'));

                if (!$isOrderCreated) {
                    die(json_encode([
                        'status' => 'error',
                        'error'  => 'Twispay is not available at the moment',
                    ]));
                }

                $base64JsonRequest = Cubix_Twispay_TwispayApi::getBase64JsonRequest($data);
                $base64Checksum = Cubix_Twispay_TwispayApi::getBase64Checksum($data, $twispayConfigs['key']);

				try {

                    $paymentFrom = "
                        <form id=\"payment-form\" action='{$twispayConfigs['url']}' method='post' accept-charset='UTF-8'>
                            <input type='hidden' name='jsonRequest' value = \"{$base64JsonRequest}\" >
                            <input type='hidden' name='checksum' value = \"{$base64Checksum}\" >
                            <input type = \"submit\" value = \"Pay\" >
                        </form > ";

                } catch(Exception $e) {
					$message = $e->getMessage();
					var_dump($message); die();
				}

                die(json_encode(array('status' => 'success', 'form' => $paymentFrom), JSON_HEX_TAG | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES ));
			} elseif ($card_payment_gateway == 'postfinance') {
				$postfinance_payment = new Model_Postfinance();
				
				$data = array(
					'PSPID' => $postfinance_payment->PSPID,
					'ORDERID' =>  'SC3-' . $this->user->id . '-' . $hash,
					'AMOUNT' => $amount * 100,
					'LANGUAGE' => 'de_DE',
					'CURRENCY' => $postfinance_payment->currency
				);

				$data['SHASIGN'] = $postfinance_payment->generateChecksum($data);
				
				try {
					$payment_from = '<form method="post" action="' . $postfinance_payment->submitUrl . '"id=form1 name=form1>';

					foreach($data as $k => $v) {
					    $payment_from .= '<input type="hidden" name="' . $k . '" value="'. $v . '">';
					}

					$payment_from .= '<input type="submit" value="" id=submit2 name=submit2>';
					$payment_from .= '</form>';

				} catch(Exception $e) {
					$message = $e->getMessage();
					var_dump($message); die();
				}

				die(json_encode(array('status' => 'success', 'form' => $payment_from), JSON_HEX_TAG | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES ));

			}elseif ($card_payment_gateway == 'epg') {

				$epg_payment = new Model_EpgGateway();
				$reference = 'SC3-' . $this->user->id . '-' . $hash;
				
				$fields = $epg_payment->getFieldsForAuth($reference, $amount, 'http://www.and6.com/online-billing-v4/epg-response');
				
				$this->client->call('OnlineBillingV2.storeToken', array($fields['Token'], $this->user->id));
				echo json_encode(array( "status" => 1, "url" => $fields['RedirectUrl'] )); die;
			}
			elseif ($card_payment_gateway == 'faxin') {

				$amount = 100 * $amount;
				$postdata = http_build_query(array('sysorderid' => $hash, 'amount' => $amount ));

				$opts = array('http' => array(
					'method'  => 'POST',
					'header'  => 'Content-type: application/x-www-form-urlencoded',
					'content' => $postdata
				));
				
				//test url - paystaging.faxin.ch
				$context  = stream_context_create($opts);
				$faxin_result = file_get_contents('https://pay.faxin.ch/payment', false, $context);
				$result  = json_decode($faxin_result);
				
				if($result->success){
					$result->status = 1;
					$reference = 'SC3-' . $this->user->id . '-' . $hash;
					$this->client->call('OnlineBillingV2.storeFaxin', array($this->user->id, $result->paymentid, $reference, $amount, 0));

					echo json_encode($result);die;
				}else{
					 die(json_encode([
	                    'status' => 'error',
	                    'error'  => 'FAXIN payment is not available at the moment',
	                    'paymentid' => $result->paymentid
                	]));
				}
			}
			elseif ($card_payment_gateway == '2000charge') {
				
				$first_name = trim(preg_replace('#[^a-zA-Z]#', '', $this->user->username)); 
				$last_name = trim(preg_replace('#[^a-zA-Z]#', ' ', $this->user->chat_info['nickName']));
				
				if( empty($first_name)){
					$first_name = 'NAME';
				}

				if(empty($last_name)){
					$last_name = 'LASTNAME';
				}
					
				$reference = 'SC3-' . $this->user->id . '-' . $hash;
				
				$customer = new Cubix_2000charge_Model_Customer();
				$customer->setCountry("CH");
				$customer->setFirstName($first_name);
				$customer->setLastName($last_name);
				$customer->setEmail($this->user->email);
				
				$payment = new Cubix_2000charge_Model_Payment();
				$payment->setPaymentOption("paysafe");
				$payment->setHolder($first_name.' '.$last_name);

				$transaction = new Cubix_2000charge_Model_Transaction();
				$transaction->setCustomer($customer);
				$transaction->setPayment($payment);
				$transaction->setAmount($amount * 100);
				$transaction->setCurrency("CHF");
				$transaction->setIPAddress(Cubix_Geoip::getIP());
				$transaction->setMerchantTransactionId($reference);
				
				$host = 'https://' . $_SERVER['SERVER_NAME'];
				$redirectUrls = new Cubix_2000charge_Model_RedirectUrls();
				$redirectUrls->setReturnUrl($host . self::$linkHelper->getLink('online-billing-v4-success'));
				$redirectUrls->setCancelUrl($host . self::$linkHelper->getLink('online-billing-v4-failure'));
				$transaction->setRedirectUrls($redirectUrls);

				$res = Cubix_2000charge_Transaction::post($transaction);
				$this->client->call('OnlineBillingV2.storeToken', array($res->id, $this->user->id, '2000charge'));
				echo json_encode(array( "status" => 1, "url" => $res->redirectUrl )); die;
			}
			elseif ($card_payment_gateway == 'coinpass') {
						
				$order_id = $this->client->call('OnlineBillingV2.saveCoinsomeRequest', array($this->user->id, 'SC3', $amount, $hash));
				$coinsome_model = new Cubix_Coinsome();
				
				$paymentFrom = '
						<form id="payment-form" method="POST" name="criptoForm" action="'. $coinsome_model->generateFormUrl($order_id) .'">
							<input type="hidden" name="amount" value="'. $amount .'" />
							<input type="submit" value="">	
						</form>';

				die(json_encode(array('status' => 'success', 'form' => $paymentFrom), JSON_HEX_TAG | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES ));
			}
			else {
				die(
					__('card_payment_not_specified')
				);
			}
		}
	}

	public function paymentAction()
	{
		$cart = $this->_session->cart;

		if ( ! count($cart) ) {
			$this->_redirect(self::$linkHelper->getLink('online-billing-v3-select-escort'));
		}

		$city_model = new Model_Cities();
		$this->view->dayPassesPackages = self::$dayPassesPackages;

		$allow_pay_by_phone = false;
		if ( count($cart) == 1 ) {
			$first_key = array_keys($cart);
			$first_key = $first_key[0];
			$c = $cart[$first_key];

			$gotds = reset($c['gotd_days']);

			if ( in_array($c['selected_package']['id'], self::$dayPassesPackages) && count($c['additional_regions']) == 0 && count($c['gotd_days']) == 0
				|| ( ! $c['selected_package'] && count($c['gotd_days']) == 1 && count($gotds) == 1)
			) {
				$allow_pay_by_phone = true;
			}
		}
		$this->view->allow_pay_by_phone = $allow_pay_by_phone;

		if ( ! $this->_request->isPost() ) {
			foreach($cart as $i => $order)  {
				$price = 0;

				if ( $order['selected_package'] ) {
					$price += (int)$order['selected_package']['price'];
				}

				if ( count($order['additional_regions']) ) {
					$cart[$i]['additional_regions_titles'] = $city_model->getByIds(implode(',', $order['additional_regions']));
					$price += self::$additionalAreaPrice * cout($order['additional_regions']);
				}

				if ( count($order['gotd_days']) ) {
					$gotd_days_count = 0;
					$cities = $city_model->GetByIds(implode(',', array_keys($order['gotd_days'])));

					$g_d = array();
					foreach($cities as $city) {
						if ( ! $g_d[$city->id] ) {
							$g_d[$city->id] = array(
								'title'	=> $city->title,
								'days' => array()
							);
						}

						foreach($order['gotd_days'][$city->id] as $day) {
							$gotd_days_count +=1;
							$g_d[$city->id]['days'][] = $day;
						}

					}

					$cart[$i]['gotd_days_titles'] = $g_d;

					$price += self::$gotdPrice * $gotd_days_count;


				}
				$cart[$i]['order_price'] = $price;
			}
			$this->view->cart = $cart;
		} else {
			$this->_session->selected_escort = null;
			$this->_session->selected_package = null;
			$this->_session->cart = array();


			$post_data = array();
			$booked_gotds_order_ids = array();
			foreach($cart as $c) {
				$products_data = array();
				if ( count($c['additional_regions']) ) {
					foreach($c['additional_regions'] as $i) {
						$products_data['optional_products'] = array(18);
					}

					$products_data['additional_areas'] = $c['additional_regions'];
				}

				if ( count($c['gotd_days']) ) {

					$products_data['gotd'] = array();
					foreach($c['gotd_days'] as $city_id => $dates) {
						$result = $this->client->call('Billing.bookGotd', array($c['selected_escort']->id, $city_id, $dates));
						if ( $result ) {
							$products_data['gotd'][] = $result;
							$booked_gotds_order_ids[] = $result;
						}

					}
				}

				if ( $c['activation_date'] ) {
					$products_data['activation_date'] = $c['activation_date'];
				}

				$post_data[] = array(
					'user_id' => $this->user->id,
					'escort_id' => $c['selected_escort']->id,
					'agency_id' => $this->user->isAgency() ? $this->user->agency_data['agency_id'] : null,
					'package_id' => $c['selected_package']['id'],
					'data' => serialize($products_data)
				);
			}

			if ( ! $c['selected_package'] && count($booked_gotds_order_ids) == 1 & $this->_request->payment_method == 'phone') {
				$this->_redirect($this->getIvrGotdUrl(self::$gotdPrice, $booked_gotds_order_ids[0]));
			}


			$amount = $this->client->call('OnlineBillingV2.addToShoppingCart', array($post_data, $this->user->id, $this->user->isAgency() ? USER_TYPE_AGENCY : USER_TYPE_SINGLE_GIRL));

			if ( Cubix_Application::getId() == APP_A6 && ! $c['selected_package'] && count($c['gotd_days']) ) {
				$i = 0;
				foreach($c['gotd_days'] as $gd) {
					$i += count($gd);
				}
				$amount = self::$gotdPrice * $i;
			}

			if ( $this->_request->payment_method == 'phone' && $allow_pay_by_phone ) {
				$reference = 'SC3_' . $this->user->id . '_' . md5(time());
				$this->_redirect($this->getIvrUrl($amount, $reference));
			} else {
				$card_payment_gateway = $this->_request->payment_method;

				if ($card_payment_gateway == 'card_twispay') {
					$twispay_payment = new Model_Twispay();

					$payment_configs = Zend_Registry::get('payment_config');
					$twispay_configs = $payment_configs['twispay'];

					$data = array(
						'siteId' => intval($twispay_configs['siteid']),
						'identifier' => 'user-' . $this->user->id,
						'amount' =>  $amount,
						'currency' => $twispay_configs['currency'],
						'description' =>  'Self Checkout And6.com',
						'orderType' =>  'purchase',
						'orderId' =>  'SC3-' . $this->user->id . '-' . time(),
						'cardTransactionMode' => 'auth',
						'backUrl' => ''
					);

                    // Store the gateway token for backend to get details about the transaction
                    // using this information
                    $token = json_encode(['externalOrderId' => $data['order']['orderId']]);
                    $isOrderCreated = $this->client->call('OnlineBillingV2.storeToken', array($token, $this->user->id, 'twispay'));

                    if (!$isOrderCreated) {
                        die(json_encode([
                            'status' => 'error',
                            'error'  => 'Twispay is not available at the moment',
                        ]));
                    }

					$data['checksum'] = $twispay_payment->generateChecksum($data, $twispay_configs['key']);

					try {
						$payment_from = '<form accept-charset="UTF-8" id="twispay-payment-form" action="' . $twispay_configs['url'] . '" method="post">';

						foreach($data as $k => $v) {
						    $payment_from .= '<input type="hidden" name="' . $k . '" value="'. $v . '">';
						}
						$payment_from .= '<input type="submit" value="submit">';
						$payment_from .= '</form>';

					} catch(Exception $e) {
						$message = $e->getMessage();
						var_dump($message); die();
					}

					die(json_encode(array('status' => 'success', 'form' => $payment_from), JSON_HEX_TAG | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES ));

				} elseif ($card_payment_gateway == 'card_epg') {

					$epg_payment = new Model_EpgGateway();
					$reference = 'SC3-' . $this->user->id . '-' . md5(time());
					$fields = $epg_payment->getFieldsForAuth($reference, $amount, 'https://www.and6.com/online-billing-v3/epg-response');
					$this->client->call('OnlineBillingV2.storeToken', array($fields['Token'], $this->user->id));

					$this->_redirect($fields['RedirectUrl']);
				} else {
					die(
						__('card_payment_not_specified')
					);
				}



			}
		}
	}

	public function epgResponseAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;

		$token = $req->Token;
		if ( ! $token ) $this->_redirect('/');


		$epg_payment = new Model_EpgGateway();
		$result = $epg_payment->getTransactionStatus($token);


		if ($result['ResultStatus'] == "OK") {
			$this->_redirect(self::$linkHelper->getLink('online-billing-v4-success'));
		} else {
			$this->_redirect(self::$linkHelper->getLink('online-billing-v4-failure'));
		}
	}

	public function faxinResponseSuccessAction()
	{
		$this->view->layout()->disableLayout();
		$this->_redirect(self::$linkHelper->getLink('online-billing-v4-success'));
		die;
	}
	public function faxinResponseCancelAction()
	{
		$this->view->layout()->disableLayout();
		$this->_redirect(self::$linkHelper->getLink('online-billing-v4-failure'));
		die;
	}
	public function faxinResponseFailureAction()
	{
		$this->view->layout()->disableLayout();
		$this->_redirect(self::$linkHelper->getLink('online-billing-v4-failure'));
		die;
	}

	public function successAction()
	{
	}

	public function failureAction()
	{

	}

	public function removeFromCartAction()
	{
		$escort_id = $this->_request->escort_id;

		foreach($this->_session->cart as $i => $cart) {
			if ( $cart['selected_escort']->id == $escort_id ) {
				unset($this->_session->cart[$i]);
			}
		}

		die;
	}

	private function getIvrUrl($amount, $client_identifier)
	{
		if ( ! $amount || ! $client_identifier ) {
			return self::$linkHelper->getLink('private-v2-advertise-now');
		}


		$amount *= 100;


		$conf = Zend_Registry::get('ivr');

		$urls = array(
			'base_url'	=> 'http://ivr.truesenses.com/?cmd=new&acc=' . $conf['acc'] . '&aid=' . $conf['aid'] . '&amo=' . $amount . '&cur=chf&ser=' . $conf['acc'] . '&lan=de&cid=' . $client_identifier,
			'success_url'	=> 'srl=' . 'http://' . $_SERVER['SERVER_NAME'] . self::$linkHelper->getLink('online-billing-v4-success'),
			'failure_url'	=> 'frl=' . 'http://' . $_SERVER['SERVER_NAME'] . self::$linkHelper->getLink('online-billing-v4-failure'),
			'cancel_url'	=> 'crl=' . 'http://' . $_SERVER['SERVER_NAME'] . self::$linkHelper->getLink('online-billing-v4-select-escort')
		);
		$url = implode('&', $urls);

		return $url;
	}

	private function getIvrGotdUrl($amount, $client_identifier)
	{
		$amount *= 100;
		$conf = Zend_Registry::get('ivr');
		$urls = array(
			'base_url'	=> 'https://ivr.truesenses.com/?cmd=new&acc=' . $conf['acc'] . '&aid=' . $conf['aid'] . '&amo=' . $amount . '&cur=chf&ser=' . $conf['accGotd'] . '&lan=de&cid=' . $client_identifier,
			'success_url'	=> 'srl=' . 'http://' . $_SERVER['SERVER_NAME'] . self::$linkHelper->getLink('online-billing-v4-success'),
			'failure_url'	=> 'frl=' . 'http://' . $_SERVER['SERVER_NAME'] . self::$linkHelper->getLink('online-billing-v4-failure'),
			'cancel_url'	=> 'crl=' . 'http://' . $_SERVER['SERVER_NAME'] . self::$linkHelper->getLink('online-billing-v4-select-escort')
		);

		$url = implode('&', $urls);

		return $url;
	}

	private function getIvrVotdUrl($amount, $client_identifier)
	{
		$amount *= 100;
		$conf = Zend_Registry::get('ivr');
		$urls = array(
			'base_url'	=> 'https://ivr.truesenses.com/?cmd=new&acc=' . $conf['acc'] . '&aid=' . $conf['aid'] . '&amo=' . $amount . '&cur=chf&ser=' . $conf['accGotd'] . '&lan=de&cid=' . $client_identifier,
			'success_url'	=> 'srl=' . 'http://' . $_SERVER['SERVER_NAME'] . self::$linkHelper->getLink('online-billing-v4-success'),
			'failure_url'	=> 'frl=' . 'http://' . $_SERVER['SERVER_NAME'] . self::$linkHelper->getLink('online-billing-v4-failure'),
			'cancel_url'	=> 'crl=' . 'http://' . $_SERVER['SERVER_NAME'] . self::$linkHelper->getLink('online-billing-v4-select-escort')
		);

		$url = implode('&', $urls);

		return $url;
	}


    public function twispayResponseAction()
    {
        $this->view->layout()->disableLayout();
        $twispayConfigs = Zend_Registry::get('payment_config')['twispay'];

        $decryptedData = Cubix_Twispay_TwispayApi::decryptIpnResponse($this->_request->opensslResult, $twispayConfigs['key']);

        if ($decryptedData['transactionStatus'] == 'in-progress' || $decryptedData['transactionStatus'] == 'complete-ok') {
            $this->_redirect($this->view->getLink('online-billing-v4-success'));
        } else {
            $this->_redirect($this->view->getLink('online-billing-v4-failure'));
        }
    }
	
	public function btcSuccessAction()
	{
		
		$this->view->layout()->disableLayout();
		$req_json = file_get_contents('php://input');
		$payResponse = json_decode($req_json);
		$order_id = intval($payResponse->orderId);
		
		$req = serialize($_REQUEST);
		Cubix_Email::send('badalyano@gmail.com', 'BTC REDIRECT', $req, TRUE);
		/*if($order_id == 0 ){// || $payResponse->status == Cubix_Coinsome::ORDER_PENDING){ 
			header("HTTP/1.1 200 OK");
			die;
		}*/		
		
		//if($req->bitcoinpay-status){
			$this->_redirect($this->view->getLink('online-billing-v4-success'));
		/*} 
		else {
			$this->_redirect($this->view->getLink('online-billing-v4-failure'));
		}*/
	}
}
