<?php

class RedirectController extends Zend_Controller_Action
{
	private $host = '';
	
	public function init()
	{
		$host = $_SERVER['HTTP_HOST'];
		
		if ( $host !== 'www.and6.com' ) {
			$host = 'http://www.and6.com';
			$this->host = $host;
		}
	}

	public function indexAction()
	{
		$lang = Zend_Controller_Front::getInstance()->getRequest()->getParam('lang');

		if ( ! $lang )
		{
			$lang = 'de';
		}

		header("HTTP/1.1 301 Moved Permanently");
		header('Location: ' . $this->host . "/{$lang}"  . $_SERVER['REQUEST_URI']);
		exit;
	}
	
	public function areaAction()
	{		
		$region = Zend_Controller_Front::getInstance()->getRequest()->getParam('region');
		$area = Zend_Controller_Front::getInstance()->getRequest()->getParam('area');
		
		$area = Model_Statistics::getNewAraeaByOldTitle($area);
		
		header("HTTP/1.1 301 Moved Permanently");
		header('Location: ' . $this->host . "/escorts/region_" . $area->region_slug . "/city_ch_" . $area->area_slug);
		exit;
	}
	
	public function and6RegionAction()
	{		
		$region = Zend_Controller_Front::getInstance()->getRequest()->getParam('region');
		$region = preg_replace("#.html#", "", $region);
		
		switch ( $region ) {
			case 'Deutschschweiz':
				$region = "deutschschweiz";
				break;
			case 'Romandie':
				$region = "romandie";
				break;
			case 'Ticino':
				$region = "ticino";
				break;
		}
		
		header("HTTP/1.1 301 Moved Permanently");
		header('Location: ' . $this->host . "/escorts/region_" . $region);
		exit;
	}

	public function mainPageAction()
	{
		header("HTTP/1.1 301 Moved Permanently");
		header('Location: http://' . $_SERVER['HTTP_HOST']);
		exit;
	}

	public function reviewsAction()
	{
		$lang = Zend_Controller_Front::getInstance()->getRequest()->getParam('lang');

		if ( ! $lang )
		{
			$lang = 'de';
		}

		header("HTTP/1.1 301 Moved Permanently");
		header('Location: ' . $this->host . "/{$lang}/reviews");
		exit;
	}

	/*public function reviewsEscortAction()
	{
		$lang = Zend_Controller_Front::getInstance()->getRequest()->getParam('lang');
		$showname = Zend_Controller_Front::getInstance()->getRequest()->getParam('showname');
		$page = intval(Zend_Controller_Front::getInstance()->getRequest()->getParam('page'));

		if ( ! $lang )
		{
			$lang = 'de';
		}

		$url = $this->host . "/{$lang}/reviews/{$showname}";
//var_dump($page); die;
		if ( $page ) {
			$url .= '?page=' . $page;
		}

		header("HTTP/1.1 301 Moved Permanently");
		header("Location: {$url}");
		exit;
	}*/
	
	public function reviewsEscortAction()
	{
		$lang = Zend_Controller_Front::getInstance()->getRequest()->getParam('lang');
		$showname = Zend_Controller_Front::getInstance()->getRequest()->getParam('showname');
		$page = intval(Zend_Controller_Front::getInstance()->getRequest()->getParam('page'));

		$model = new Model_EscortsV2();
		$showname = trim($showname, '-');
		$id = $model->getIdByShowname($showname)->id;
		
		if ($id)
		{
			$url = $this->host . "/evaluations/{$showname}-{$id}";
			
			if ( $page ) {
				$url .= '?page=' . $page;
			}
		}
		else
		{
			$url = $this->host . "/";
		}

		header("HTTP/1.1 301 Moved Permanently");
		header("Location: {$url}");
		exit;
	}

	public function escortAction()
	{
		$lang = Zend_Controller_Front::getInstance()->getRequest()->getParam('lang');
		$showname = Zend_Controller_Front::getInstance()->getRequest()->getParam('showname');

		if ( ! $lang )
		{
			$lang = 'de';
		}

		header("HTTP/1.1 301 Moved Permanently");
		header('Location: ' . $this->host . "/{$lang}/escort/{$showname}");
		exit;
	}
	
	public function escort2Action()
	{
		$lang = Zend_Controller_Front::getInstance()->getRequest()->getParam('lang');
		$showname = Zend_Controller_Front::getInstance()->getRequest()->getParam('showname');
		
		$model = new Model_EscortsV2();
		$showname = trim($showname, '-');
		$id = $model->getIdByShowname($showname)->id;

		if ($id)
		{
			header("HTTP/1.1 301 Moved Permanently");
			header('Location: ' . $this->host . "/escort/{$showname}-{$id}");
		}
		else
		{
			header("HTTP/1.1 301 Moved Permanently");
			header('Location: ' . $this->host . "/");
		}
		
		exit;
	}

	public function regionsAction()
	{
		$req = Zend_Controller_Front::getInstance()->getRequest()->getParam('req');

		header("HTTP/1.1 301 Moved Permanently");
		header('Location: ' . $this->host . '/fr/escorts/region_fr_' . str_replace('_', '-', $req));
		exit;
	}

	public function geoAction()
	{
		$req = Zend_Controller_Front::getInstance()->getRequest();

		$lang = $req->getParam('lang');
		$type = $req->getParam('type');
		$geo  = $req->getParam('geo');
		$geoval = $req->getParam('geoval');
		$filter = $req->getParam('filter');
		$page = $req->getParam('page');

		$url = $this->host;
		if ( $lang ) {
			$url .= '/' . $lang;
		}

		$url .= '/escorts';

		switch ( $type ) {
			case 'nuove' :
				$type = 'nuove';
			break;
			case 'independantes' :
				$type = 'independantes';
			break;
			case 'agence' :
				$type = 'agence';
			break;
			case 'boys' :
				$type = 'boys';
			break;
			case 'trans' :
				$type = 'trans';
			break;
			case 'citytours' :
				$type = 'citytours';
			break;
			case 'upcomingtours' :
				$type = 'upcomingtours';
			break;
			default: $type = '';
		}

		if ( strlen($type) ) {
			$url .= '/' . $type;
		}

		if ( $geoval ) {
			$geoval = str_replace('_', '-', $geoval);
			$url .= '/' . $geo . '_fr_' . $geoval;
		}

		if ( ! strlen($type) && ! $geoval ) {
			$url .= '/regular';
		}

		if ( $filter ) {
			switch ( $filter ) {
				case 'incall':
				case 'outcall':
					$filter = 'filter_available-for-' . $filter;
					break;
			}
			
			if ( strlen($filter) ) {
				$url .= '/' . $filter;
			}
		}

		$page = intval($page);
		if ( $page > 0 ) {
			//$url .= '/page_' . ($page + 1);
			$url .= '/' . $page;
		}
		
		header("HTTP/1.1 301 Moved Permanently");
		header('Location: ' . $url);
		exit;
	}

	public function citiesAction()
	{
		$req = Zend_Controller_Front::getInstance()->getRequest();

		$city = $req->getParam('city');
		$lang = $req->getParam('lang');
		$type = $req->getParam('type');

		$type .= ($type) ? '/' : '';

		if ( ! $lang ) {
			$lang = 'de';
		}

		$city_slug = str_replace('_', '-', $city);

		$model = new Cubix_Geography_Cities();
		$city = $model->getBySlug($city_slug);

		$r_model = new Cubix_Geography_Regions();
		$region = $r_model->get($city->region_id);
		
		header("HTTP/1.1 301 Moved Permanently");
		header('Location: ' . $this->host . "/{$lang}/escorts/{$type}region_fr_" . $region->slug . '/city_' . $city_slug);
		exit;
	}

	public function agencyAction()
	{
		//$req = Zend_Controller_Front::getInstance()->getRequest()->getParam('agencyId');
		$url = explode('=', $_SERVER['REQUEST_URI']);
		$lng_id = explode('&', $url[1]);
		$agency_id = $url[2];

		$client = new Cubix_Api_XmlRpc_Client();
		$agency = $client->call('Agencies.getByOldId', array($agency_id));

		header("HTTP/1.1 301 Moved Permanently");

		if ( ! $agency ) {
			header("Location: ' . $this->host . '/{$lng_id[0]}");
		}
		else {
			header('Location: ' . $this->host . "/{$lng_id[0]}/agency/" . $agency['slug']);
		}
		exit;
	}
	
	public function agencies2Action()
	{
		$lang = Zend_Controller_Front::getInstance()->getRequest()->getParam('lang_id');
		$agencyName = Zend_Controller_Front::getInstance()->getRequest()->getParam('agencyName');
		$req = Zend_Controller_Front::getInstance()->getRequest()->getParam('req');
		
		if ( ! $lang )
		{
			$lang = '';
		}
		
		$model = new Model_Agencies();
		$agencyName = trim($agencyName, '-');
		$ag = $model->getIdByName($agencyName);
		$id = $ag['id'];
		
		if (!$req)
			$req = '';

		if ($id)
		{
			header("HTTP/1.1 301 Moved Permanently");
			header('Location: ' . $this->host . "/{$lang}/agency/{$agencyName}-{$id}" . $req);
		}
		else
		{
			header("HTTP/1.1 301 Moved Permanently");
			header('Location: ' . $this->host . "/");
		}
		
		exit;
	}

	public function directoryAction()
	{
		$req = Zend_Controller_Front::getInstance()->getRequest();
		$lang = $req->getParam('lang_id');

		$url = $this->host;
		
		if ( $lang == 'en' ) {
			$url .= '/' . $lang;
		}
		else {
			$url .= '/';
		}

		header("HTTP/1.1 301 Moved Permanently");
		header('Location: ' . $url);
		exit;
	}

	public function loginAction()
	{
		$req = Zend_Controller_Front::getInstance()->getRequest();
		$lang = $req->getParam('lng');

		$url = $this->host;

		if ( $lang == 'en' ) {
			$url .= '/' . $lang . '/';
		}
		else {
			$url .= '/';
		}

		$url .= 'private/signin';

		header("HTTP/1.1 301 Moved Permanently");
		header('Location: ' . $url);
		exit;
	}

	public function escortListAction()
	{
		header("HTTP/1.1 301 Moved Permanently");
		header('Location: ' . $this->host . '/');
		exit;
	}

	public function blankHtmlAction()
	{
//		die(preg_replace('#/blank\.html\??#', '/', $this->_request->getRequestUri()));
//		$this->_request->setRequestUri(preg_replace('#/blank\.html\??#', '', $this->_request->getRequestUri()));
//		$this->getFrontController()->dispatch();
//		exit;

		/*header('HTTP/1.1 301 Moved Permanently');
		header('Location: ' . preg_replace('#/blank\.html#', '', $this->_request->getRequestUri()));
		exit;*/
	}

	public function blankAction()
	{
		$uri = preg_replace('#/blank.html(.+)#', '', $_SERVER['REQUEST_URI']);

		header("HTTP/1.1 301 Moved Permanently");
		header('Location: ' . $this->host . $uri);
		exit;
	}
	
	public function adstoolAction()
	{
		header("HTTP/1.1 301 Moved Permanently");
		header('Location: ' . 'http://www.and6.com');
		exit;
	}
	
	public function hardUrlRedirectAction()
	{
		$area_id = $this->_request->area_id;
		
		$m_city = new Model_Cities();
		$city = $m_city->getById($area_id);
		
		$subdomain = 'www';
		if ( $city->region_slug != 'deutschschweiz' ) {
			$subdomain = $city->region_slug;
		}
		
		header("HTTP/1.1 301 Moved Permanently");
		header('Location: ' . 'http://' . $subdomain . '.and6.com/sex-in-' . $city->city_slug . '-city-c' . $city->id);
		exit;
	}
	
	public function onlineChatCityAction()
	{
		$req = Zend_Controller_Front::getInstance()->getRequest();
		$city = $req->getParam('city');
		
		$uri = '/escorts/city_' . $city;

		header("HTTP/1.1 301 Moved Permanently");
		header('Location: ' . $this->host . $uri);
		exit;
	}
}
