<?php

class LatestActionsController extends Zend_Controller_Action
{
	public function init()
	{
		$this->view->layout()->setLayout('latest-actions');
		
		$this->model = new Model_LatestActions();
		
		$this->client = new Cubix_Api_XmlRpc_Client();
		
		$perRows = array(3, 5);
		
		$pr = $this->_getParam('pr', 5);
		if ( !in_array($pr, $perRows) ) {
			$pr = 5;
		}
		
		$this->view->perRow = $pr;
	}
	
	public function indexAction()
	{
		$defines = $this->view->defines = Zend_Registry::get('defines');
		$this->view->cities = $this->model->getAllCitiesForFilter(Cubix_Application::getById()->country_id);
		
		$filter = array();
		
		$this->view->perPage = $perPage = 30;
		$this->view->page = $page = 1;
		
		$escorts = $this->model->getAll($filter, $page, $perPage, $count);
		
		$this->view->escorts = $escorts;
		$this->view->count = $count;
		
		$this->view->action_counts = $this->model->getActionCounts($filter);
	}
	
	public function ajaxHeaderAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$defines = $this->view->defines = Zend_Registry::get('defines');
		$this->view->cities = $this->model->getAllCitiesForFilter(Cubix_Application::getById()->country_id);
		
		$this->view->city = $city = (int)$req->city;
		$this->view->action_type = $action_type = (int)$req->action_type;
		
		$filter = array();
		
		$city = (int)$req->city;
		$action = (int)$req->action_type;
		
		if ( $city ) {
			$filter['city_id'] = $city;
		}
		
		if ( $action ) {
			$filter['action_type'] = $action;
		}
		
		$this->view->action_counts = $this->model->getActionCounts($filter);
	}
	
	public function ajaxListAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$defines = $this->view->defines = Zend_Registry::get('defines');
		
		$this->view->perPage = $perPage = 30;
		$page = $req->page;
		if ( $page < 1 ) {
			$page = 1;
		}
		$this->view->page = $page;
		
		$filter = array();
		
		$city = (int)$req->city;
		$action = (int)$req->action_type;
		
		if ( $city ) {
			$filter['city_id'] = $city;
		}
		
		if ( $action ) {
			$filter['action_type'] = $action;
		}
		
		$this->view->escorts = $this->model->getAll($filter, $page, $perPage, $count);
		$this->view->count = $count;
	}
	
	public function ajaxGetDetailsAction()
	{
		$this->view->layout()->disableLayout();
		$this->view->addScriptPath($this->view->getScriptPath('latest-actions'));
		
		$escort_id = $this->view->escort_id = (int)$this->_request->escort_id;
		$act = (int)$this->_request->act;
		
		if ( !$escort_id || ! in_array($act, Cubix_LatestActions::$_actions_arr) ) {
			die(':)');
		}
		
		switch ($act) {
			case Cubix_LatestActions::ACTION_NEW_PHOTO:
				$view_script = "photo";
				
				$this->view->photos = $this->model->getPhotoDetails($escort_id);
			break;
			case Cubix_LatestActions::ACTION_PROFILE_UPDATE:				
				$view_script = "profile";
				$m_escort = new Model_EscortsV2();
				$this->view->showname = $m_escort->getShownameById($escort_id);
				$data = $this->model->getProfileDetails($escort_id);
				
				$this->view->new_profile = $new = $data['new'];
				$this->view->old_profile = $old = $data['old'];
				//var_dump($this->view->new_profile);
				$this->view->changes = $this->model->differProfile($old, $new);
			break;
			case Cubix_LatestActions::ACTION_TOUR_UPDATE:
				$view_script = "tours";
				$m_escort = new Model_EscortsV2();
				$this->view->showname = $m_escort->getShownameById($escort_id);
				$this->view->tours = $this->model->getTourDetails($escort_id);
			break;
			case Cubix_LatestActions::ACTION_NEW_REVIEW:
				$view_script = "reviews";
				$m_escort = new Model_EscortsV2();
				$this->view->showname = $m_escort->getShownameById($escort_id);
				$this->view->reviews = $this->model->getReviewDetails($escort_id);
			break;
			case Cubix_LatestActions::ACTION_NEW_COMMENT:
				$view_script = "comments";
				
				$this->view->comments = $this->model->getCommentDetails($escort_id);
			break;
		}
		
		
		$this->_helper->viewRenderer->setScriptAction("details/" . $view_script);
	}
}