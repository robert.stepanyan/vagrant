<?php

class AgenciesController extends Zend_Controller_Action
{
	public static $linkHelper;
	
	public function init()
	{
		self::$linkHelper = $this->view->getHelper('GetLink');
	}
	
	public function indexAction()
	{

	}

    public function listAction()
    {
        $this->view->layout()->setLayout('main-simple');
        $cache = Zend_Registry::get('cache');
        //$this->view->layout()->disableLayout();
//        ini_set('display_errors', 1);
//        ini_set('display_startup_errors', 1);
//        error_reporting(E_ALL);

        $this->view->p_id = $this->_request->getParam('p_id');

        $this->view->static_page_key = 'agencies';
        if($this->_request->getParam('filter') == '2'){
            $this->view->static_page_key = 'bdsm-agencies';
        }elseif($this->_request->getParam('filter') == '3'){
            $this->view->static_page_key = 'massage-agencies';
        }

        $list_types = array('list');

        if ( isset($_COOKIE['list_type_a']) && $_COOKIE['list_type_a'] ) {
            $list_type = $_COOKIE['list_type_a'];

            if ( ! in_array($list_type, $list_types) ) {
                $list_type = $list_types[0];
            }

            $this->view->render_list_type = $list_type;
        } else {
            $list_type = $list_types[0];
            $this->view->render_list_type = $list_types[0];
        }

        //Currency rates log
        $default_currency = "USD";
        $current_currency = $this->_getParam('currency', $default_currency);

        if ( $current_currency != $_COOKIE['currency'] && $this->_request->s ) {
            setcookie("currency", $current_currency, strtotime('+1 year'), "/", "www." . preg_replace('#www.|dev.#', '', $_SERVER['HTTP_HOST']));
            $_COOKIE['currency'] = $current_currency;
        }

        if ( isset($_COOKIE['currency']) ) {
            $current_currency = $_COOKIE['currency'];
            $this->_request->setParam('currency', $current_currency);
        }

        $this->view->current_currency = $current_currency;
        //Currency rates log

        $default_sorting = "alpha";
        $current_sorting = $this->_getParam('agency_sort', $default_sorting);

        $sort_map = array(
            'newest' => 'date_registered DESC',
            'last_modified' => 'cd.last_modified DESC',
            'last-connection' => 'refresh_date DESC',
            'close-to-me' => 'cd.id ASC',
            'most-viewed' => 'hit_count DESC',
            'random' => 'RAND()',
            'by-country' => 'cr.title_en ASC',
            'by-city' => 'ct.title_en ASC',
            'alpha' => 'cd.club_name ASC',
        );

        if ( !array_key_exists($current_sorting, $sort_map) ) {
            $current_sorting = $default_sorting;
            $this->_request->setParam('agency_sort', $default_sorting);
        }

        if ( $current_sorting != $_COOKIE['agency_sort'] && $this->_request->s ) {
            setcookie("a_sorting", $current_sorting, strtotime('+1 year'), "/", "www." . preg_replace('#www.|dev.#', '', $_SERVER['HTTP_HOST']));
            setcookie("a_sortingMap", $sort_map[$current_sorting]['map'], strtotime('+1 year'), "/", "www." . preg_replace('#www.|dev.#', '', $_SERVER['HTTP_HOST']));
            $_COOKIE['a_sorting'] = $current_sorting;
            $_COOKIE['a_sortingMap'] = $sort_map[$current_sorting]['map'];
        }

        if ( isset($_COOKIE['a_sorting']) ) {
            $current_sorting = $_COOKIE['a_sorting'];
            $this->_request->setParam('agency_sort', $current_sorting);
        }

        if ( $current_sorting ) {
            $tmp = $sort_map[$current_sorting];
            unset($sort_map[$current_sorting]);
            $sort_map = array_merge(array( $current_sorting => $tmp), $sort_map);
        }

        if ( $current_sorting ) {
            $sorting = $current_sorting;
        }

        $this->view->sort_map = $sort_map;
        $this->view->current_sort = $current_sorting;


        $p_top_category = $this->view->p_top_category = $this->_request->getParam('p_top_category', 'agencies');

        $p_city_id = $this->view->city = (int) $this->_request->city;
        $online_now = $this->view->online_now = (int)$this->_request->online_now;
        $p_country_id = $this->view->country = (int) $this->_request->country;
        $agency = $this->view->agency = $this->_request->agency;

        $this->view->is_active_filter = false;

        if ($p_city_id || $p_country_id || $agency)
            $this->view->is_active_filter = true;

        $page = intval($this->_request->page);

        if ( $page == 0 ) {
            $page = 1;
        }

        $this->view->page = $page;

        $e_config = Zend_Registry::get('escorts_config');

        $per_page_def = $e_config['agencyPerPage'];

        $this->view->per_page = $per_page = 20; //$this->_getParam('per_page', $per_page_def);

        //$this->view->per_page = $per_page;

        $geoData = array();

        if ( $sorting == 'close-to-me' ) {
            $geoData = Cubix_Geoip::getClientLocation();
        }

        $m_agencies = new Model_Agencies();

        $count = 0;

        $agency_has_esc_type = 1;

        if( $this->_getParam('filter') ){
            $agency_has_esc_type = $this->_getParam('filter');
        }

        $ag_city_id = $this->view->city_id = ( $this->_request->city_id ) ? $this->_request->city_id : null;

        $agencies = $m_agencies->getAll($page, $per_page, $p_city_id, $sorting, $p_country_id, $count, $list_type, $geoData, $online_now, $agency, $agency_has_esc_type, $ag_city_id);

        $map_agencies = $this->view->map_agencies = $m_agencies->getCitiesMapped();

//        if( $_GET['test'] == 1 ){
//            echo '<pre>';
//                print_r( $map_agencies );
//            echo '</pre>';
//        }

        $this->view->count = $count;
        $this->view->page = $page;
        $this->view->per_page = $per_page;
        $this->view->agencies = $agencies;
        //$this->view->agencies = $this->_get_page( $agencies, $page, $per_page );

        if ( ! is_null($this->_getParam('ajax')) ) {
            $this->view->is_ajax = true;
            $this->view->layout()->disableLayout();
//            $escort_list = $this->view->render('agencies/list.phtml');
//            die(json_encode(array('escort_list' => $escort_list)));
        }
    }
	
	public function showAction()
	{
		if ( ! is_null($this->_getParam('ajax')) ) {
			$this->view->layout()->disableLayout();
			$this->view->ajax = true;
		}
		else {
			$this->view->layout()->setLayout('main-simple');
		}

		$agency_slug = $this->_getParam('agencyName');
		$agency_id = intval($this->_getParam('agency_id'));

		$client = new Cubix_Api_XmlRpc_Client();
		$agency = $client->call('Agencies.getInfo', array($agency_slug, $agency_id));

		if ( ! $agency ) {
			// 404
			die();
		}
		elseif ( isset($agency['error']) ) {
			print_r($agency['error']); die;
		}

		$agency = $this->view->agency = new Model_AgencyItem($agency);
		
		// load photos
		$config = Zend_Registry::get('agencies_config');
		$this->view->photos_perPage = $per_page = $config['photos']['perPage'];
		$model = new Model_Agencies();
		$this->view->photos = $model->loadAgencyPhotos($agency['id'], 1, $per_page, $count);
		
		$this->view->photos_count = $count;
		$this->view->photos_page = 1;
		$this->view->agency_id = $agency['id'];
		//
		
		// load commencts
		$this->view->comments_perPage = $per_page = $config['comments']['perPage'];
		$comments = $client->call('Agencies.getComments', array($agency_id, 1, $per_page));
		
		$this->view->comments = $comments['list'];
		$this->view->comments_count = $comments['count'];
		$this->view->comments_page = 1;
		$this->view->agency_id = $agency['id'];
		//

		/* --> Extract page from `req` parameter */
		$req = ltrim($this->_getParam('req'), '/');
		$req = explode('_', $req);

		$page = 1;
		$param_name = $req[0];

		if ( $param_name == 'page' ) {
			$page = $req[1];
		}

		if ( $page < 1 ) $page = 1;
		/* <-- */

		$config = Zend_Registry::get('escorts_config');
		
		$filter = array('e.agency_id = ?' => $agency->id);
		
		$count = 0;
		$escorts = Model_Escort_List::getFiltered($filter, 'last-modified', $page, $config['perPage'], $count);

		if ( isset($escorts[0]) && $escorts[0] ) {
			$this->view->agency_last_mod_date = $escorts[0]->date_last_modified;
		}

        /****** Agencies Cache ******/

        $cache_ag = Zend_Registry::get('cache');
        $cache_key = 'v2_agencies_all_prev_next_v2_';

        if ( !$_agencies = $cache_ag->load($cache_key) )
        {
            $m_agencies = new Model_Agencies();
            $agency_has_esc_type = 1;

            $list_types = array('list');

            if ( isset($_COOKIE['list_type_a']) && $_COOKIE['list_type_a'] ) {
                $list_type = $_COOKIE['list_type_a'];

                if ( ! in_array($list_type, $list_types) ) {
                    $list_type = $list_types[0];
                }
            } else {
                $list_type = $list_types[0];
            }

            $count = 0;
            $_agencies = $m_agencies->getAll($page, 2000, null, 'alpha', null, $count, $list_type, array(), false, null, 1);

            //$agencies = 5;
            $cache_ag->save( $_agencies, $cache_key, array() );
        }

        //$key = array_search($agency_id, array_column( $_agencies, 'id') );
        $key = array_search($agency_id, array_map(function($e) {
            return is_object($e) ? $e->id : $e['id'];
        }, $_agencies) );

        if( $_agency_prev = $_agencies[ $key - 1 ] ){
            $this->view->agency_prev = $_agency_prev;
        }

        if( $_agency_next = $_agencies[ $key + 1 ] ){
            $this->view->agency_next = $_agency_next;
        }
        /******* End Cache *******/
		
		$this->view->escorts = $escorts;
		$this->view->count = $count;
		$this->view->params = array('page' => $page);

		$this->view->no_paging = true;

		/***************************/
	}
	
	public function photosAction()
	{
		$config = Zend_Registry::get('agencies_config');
		$this->view->photos_perPage = $per_page = $config['photos']['perPage'];
		
		$page = $this->_getParam('photo_page');

		$agency_id = $this->_getParam('agency_id');
		$mode = $this->_getParam('mode');
		
		if( $mode == 'ajax' ) {
			$this->view->layout()->disableLayout();
			$this->view->agency_id = $agency_id;
		}
		
		if ( ! $page )
			$page = 1;
		
		$count = 0;
		
		$m = new Model_Agencies();
		$photos = $m->loadAgencyPhotos($agency_id, $page, $per_page, $count);
		
		$this->view->photos = $photos;
		$this->view->photos_count = $count;
		$this->view->photos_page = $page;
	}
	
	public function commentsAction()
	{
		$config = Zend_Registry::get('agencies_config');
		$this->view->comments_perPage = $per_page = $config['comments']['perPage'];
		
		$page = $this->_getParam('page');

		$agency_id = $this->_getParam('agency_id');
		$mode = $this->_getParam('mode');
		
		if( $mode == 'ajax' ) {
			$this->view->layout()->disableLayout();
			$this->view->agency_id = $agency_id;
		}
		
		if ( ! $page )
			$page = 1;
		
		$count = 0;
		
		$client = new Cubix_Api_XmlRpc_Client();
		$comments = $client->call('Agencies.getComments', array($agency_id, $page, $per_page));
		
		$this->view->comments = $comments['list'];
		$this->view->comments_count = $comments['count'];
		$this->view->comments_page = $page;
	}

    public function _get_page( array $input, $pageNum, $perPage ){
        $start = ($pageNum-1) * $perPage;
        $end = $start + $perPage;
        $count = count($input);

        if( $_GET['test'] == 1 ){
            echo $start, $end, $count;
        }

        // Conditionally return results
        if ($start < 0 || $count <= $start) {
            // Page is out of range
            return array();
        } else if ($count <= $end) {
            // Partially-filled page
            return array_slice($input, $start);
        } else {
            // Full page
            return array_slice($input, $start, $end - $start);
        }
    }

}
