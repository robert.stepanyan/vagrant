<?php

class StaticPageController extends Zend_Controller_Action
{
	public function init()
	{	
		
	}
	
	public function showAction()
	{
		$this->view->layout()->disableLayout();
		
		$slug = $this->_request->page_slug;
		$lang = Cubix_I18n::getLang();
		$app_id = Cubix_Application::getId();
		
		$model = new Model_StaticPage();
		$this->view->page = $model->getBySlug($slug, $app_id, $lang);
	}
	
	public function pageAction()
	{
		/*error_reporting(E_ALL);
		ini_set('display_errors', '1');*/
		//$display_layout = true;
		
		$has_layout = $this->_request->getParam('layout', true);
		
		if ( $has_layout === true ) {
			$this->view->layout()->setLayout('main-simple');
		}
		else {
			$this->view->layout()->disableLayout();
		}
		
		$slug = $this->_request->page_slug;

		$lang = Cubix_I18n::getLang();

		$app_id = Cubix_Application::getId();
		$model = new Model_StaticPage();


		$this->view->page = $model->getBySlug($slug, $app_id, $lang);
	}
}
