<?php

class EscortsController extends Zend_Controller_Action
{
	public static $linkHelper;
	
	public function init()
	{
		self::$linkHelper = $this->view->getHelper('GetLink'); 
	}
	
	public static function _itemDecorator($item, $is_first = FALSE, $is_hidden = FALSE)
	{

		$has_childs = isset($item->childs) && is_array($item->childs) && count($item->childs);
		
		$classes = array();
		
		if ( $is_first ) $classes[] = 'first';
		if ( $has_childs ) $classes[] = 'sub';
		
		$link = '#';
		if ( ! $has_childs ) {
			$link = self::$linkHelper->getLink('escorts', array('filter' => $item->value, 'page' => NULL));
		}
		
		$title = $item->title;
		if ( $is_first && isset($item->title) ) {
			$title = $item->parent . ' - ' . $item->title;
		}

		//$html = '<a ' . ($is_hidden ? 'style="display: none" ' : '') . '' . (count($classes) ? 'class="' . implode(' ', $classes) . '" ' : '') . 'href="#"' . ' ' . ( true ? 'onclick="return Cubix.Filter.Set({ filter: ' . ($item->value ? "'" . $item->value . "'" : 'null') . ', page: null })"' : '' ) . '>' . $title . '</a>';
		$html = '<a ' . ($is_hidden ? 'style="display: none" ' : '') . '' . (count($classes) ? 'class="' . implode(' ', $classes) . '" ' : '') . 'href="#"' . ($item->value ? ' rel="' . $item->value . '"' : '') . ' ' . ( true ? 'onclick="return Cubix.Filter.Set({ filter: ' . ($item->value ? "'" . $item->value . "'" : 'null') . ', page: null })"' : '' ) . '>' . $title . '</a>';

		if ( $is_first ) {
			$html = '<div class="input-w"><div class="i">' .
				$html .
			'</div></div>';
		}

		return $html;
	}
	
	public static function _sortItemDecorator($item, $is_first = FALSE, $is_hidden = FALSE)
	{
		$has_childs = isset($item->childs) && is_array($item->childs) && count($item->childs);
		
		$classes = array();
		
		if ( $is_first ) $classes[] = 'first';
		if ( $has_childs ) $classes[] = 'sub';
		
		$link = '#';
		if ( ! $has_childs ) {
			$link = self::$linkHelper->getLink('escorts', array('sort' => $item->value));
		}
		
		$title = $item->title;
		if ( $is_first && isset($item->title) ) {
			$title = $item->parent . ' - ' . $item->title;
		}

		//$html = '<a ' . ($is_hidden ? 'style="display: none" ' : '') . '' . (count($classes) ? 'class="' . implode(' ', $classes) . '" ' : '') . 'href="#"' . ' ' . ( true ? 'onclick="return Cubix.Filter.Set({ sort: \'' . $item->value . '\' })"' : '' ) . '>' . $title . '</a>';
		$html = '<a ' . ($is_hidden ? 'style="display: none" ' : '') . '' . (count($classes) ? 'class="' . implode(' ', $classes) . '" ' : '') . 'href="#"' . ($item->value ? ' rel="' . $item->value . '"' : '') . ' ' . ( true ? 'onclick="return Cubix.Filter.Set({ sort: \'' . $item->value . '\' })"' : '' ) . '>' . $title . '</a>';

		if ( $is_first ) {
			$html = '<div class="input-w"><div class="i">' .
				$html .
			'</div></div>';
		}

		return $html;
	}

	public static function _citiesItemDecorator($item, $is_first = false, $is_hidden = FALSE)
	{
		$has_childs = isset($item->childs) && is_array($item->childs) && count($item->childs);
		
		$classes = array();
		
		if ( $is_first ) $classes[] = 'first';
		if ( $has_childs ) $classes[] = 'sub';
		
		$link = '#';
		if ( ! $has_childs ) {
			$link = self::$linkHelper->getLink('escorts', array('sort' => $item->value));
		}
		
		$title = $item->title;
		if ( $is_first && isset($item->title) ) {
			$title = $item->parent . ' - ' . $item->title;
		}

		//$html = '<a ' . ($is_hidden ? 'style="display: none" ' : '') . '' . (count($classes) ? 'class="' . implode(' ', $classes) . '" ' : '') . 'href="#"' . ' ' . ( true ? 'onclick="return Cubix.Filter.Set({ sort: \'' . $item->value . '\' })"' : '' ) . '>' . $title . '</a>';
		$html = '<a ' . ($is_hidden ? 'style="display: none" ' : '') . '' . (count($classes) ? 'class="' . implode(' ', $classes) . '" ' : '') . 'href="#"' . ($item->value ? ' rel="' . $item->value . '"' : '') . ' ' . ( true ? 'onclick="return Cubix.Filter.Set({ cityfilter: \'' . $item->value . '\' })"' : '' ) . '>' . $title . '</a>';

		if ( $is_first ) {
			$html = '<div class="input-w"><div class="i">' .
				$html .
			'</div></div>';
		}

		return $html;
	}

	protected function _getFooterText($key = 'main')
	{
		// $model = new Model_StaticPage();

		// $lang_id = Cubix_I18n::getLang();

		// $page = $model->getBySlug('footer-text-escorts-' . $key, Cubix_Application::getId(), $lang_id);
		
		// if ( $page ) return $page->body;

		$seo = $this->view->seo('home-page-' . $key, null, array());

		if ( $seo ) {
			if ( strlen($seo->footer_text) ) {
				return $seo->footer_text;
			}
		}

		return '';
	}

	/**
	 * Chat Online Escorts Action
	 */
	public function chatOnlineAction()
	{
		$action = 'index';
		if ( ! is_null($this->_getParam('ajax')) ) {
			$action = 'list';
		}
		$this->_helper->viewRenderer->setScriptAction($action);

		$req = $this->_getParam('req');
		$param = explode('/', $req);
		$page = explode("_", ( isset($param[2]) ) ? $param[2] : '' );

		$m_e = new Model_EscortsV2();
		if (!$this->view->allowChat2())
			$res = $m_e->getChat11OnlineEscorts(1, 1000);
		else
			$res = $m_e->getChatNodeOnlineEscorts(1, 1000);
		$ids = $res['ids'];
		
		if (strlen($ids) > 0)
		{
			$filter = array(
				'e.id IN (' . $ids . ')'
			);

			$page_num = 1;
			if ( isset($page[1]) && $page[1] ) {
				$page_num = $page[1];
			}

			$count = 0;
			$escorts = Model_Escort_List::getFiltered($filter, 'random', $page_num, 50, $count);
		}
		else
		{
			$count = 0;
			$escorts = array();
		}
		$this->view->count = $count;
		$this->view->escorts = $escorts;

		list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls();

		$this->view->chat_online = true;
	}

	/**
	 * Happy Hours action
	 */
	public function happyHoursAction()
	{
		$action = 'index';
		if ( ! is_null($this->_getParam('ajax')) ) {
			$action = 'list';
		}
		$this->_helper->viewRenderer->setScriptAction($action);

		$req = $this->_getParam('req');
		$param = explode('/', $req);
		$page = explode("_", ( isset($param[2]) ) ? $param[2] : '' );

		$region_slug = $this->_request->region;
		$filter = array('e.gender = ?' => 1, 'e.hh_is_active = ?' => 1, 'r.slug = ?' => $region_slug);

		$page_num = 1;
		if ( isset($page[1]) && $page[1] ) {
			$page_num = $page[1];
		}

		$count = 0;
		$escorts = Model_Escort_List::getFiltered($filter, 'random', $page_num, 10, $count);

		$this->view->count = $count;
		$this->view->escorts = $escorts;

		list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls();

		$this->view->hh_page = true;
	}

	/**
	 * Main premium spot action
	 */
	public function mainPageAction()
	{
		$glitter = $this->_getParam('glitter');
		$this->view->glitter = $glitter;

		$this->view->layout()->setLayout('main-page');
		
		$action = 'main-page';
		if ( ! is_null($this->_getParam('ajax')) ) {
			$action = 'main-page';
		}
		$this->_helper->viewRenderer->setScriptAction($action);

		//$escorts = Model_Escort_List::getMainPremiumSpot();
		
		//$this->view->escorts = $escorts;
		$this->view->no_paging = true;
		
		$this->view->main_page = true;
		
		$regions = new Cubix_Geography_Regions();
		$regions = $regions->getRegionsByCountryId(Cubix_Application::getById()->country_id, $this->_request->region);
		$this->view->regions = $regions;
		$this->view->is_main_page = true;
		list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls();
		//$this->view->e_ids = Model_EscortsV2::getLateNightGirlsForMainPage();
		//print_r($this->view->e_ids);

		$user = Model_Users::getCurrent();
		$this->view->user = $user;
		
	}

	/**
	 * New escorts action
	 */
	public function newListAction()
	{
		$this->view->layout()->setLayout('main-simple');
		
		$model = new Model_EscortsV2();		

		$req = $this->_getParam('req');
		$param = explode('/', $req);
		$page = explode("_", ( isset($param[1]) ) ? $param[1] : '' );
		
		$region_slug = $this->_request->region;
		$filter = array('e.gender = ?' => GENDER_FEMALE, 'r.slug = ?' => $region_slug);
		
		$city_params = array();
		
		if (strpos($param[1], 'city') !== FALSE)
		{
			$page = explode("_", ( isset($param[2]) ) ? $param[2] : '' );
			$city_params = explode('_', $param[1]);
		}
		
		if (count($city_params) && $city_params[2])
		{
			$modelC = new Model_Cities();
			$c_id = $modelC->getBySlug2($city_params[2])->id;
			$filter['e.city_id = ?'] = $c_id;
			$filter['e.is_new = ?'] = 1;
		}
		
		$new_cities = $model->getNewEscortsCities($this->_request->region);
		
		$ncs = array();
		$arr = array();
		
		if ($new_cities)
		{
			foreach ($new_cities as $n)
			{
				if (!isset($arr[$n->city_id]))
					$arr[$n->city_id] = 1;
				else
					$arr[$n->city_id] ++;
				$ncs[$n->city_id] = $n;
			}
		}
		
		arsort($arr);
		$this->view->cities_counts = $arr;
		$this->view->new_cities = $ncs;
		$this->view->city_iso = $city_params[1];
		$this->view->city_slug = $city_params[2];
		$this->view->city_id = $c_id;
		$city_params[2] =  preg_replace('#[^a-zA-Z0-9_]#', '_', $city_params[2]);

		$page_num = 1;
		if ( isset($page[1]) && $page[1] ) {
			$page_num = $page[1];
		}else if (is_numeric($page[0])) {
            $page_num = intval($page[0]);
        }
		$this->view->page = $page_num;
		$count = 0;
		//$escorts = Model_Escort_List::getFiltered($filter, 'newest', $page_num, 10, $count, 'new_list');
		
		$cache = Zend_Registry::get('cache');
		$new_cache_key = 'v2_new_escorts_list_' . Cubix_Application::getId() . '_lang_' . Cubix_I18n::getLang() . '_page_' . $page_num . $region_slug . $city_params[2];
		$new_c_cache_key = 'v2_new_escorts_list_count_' . Cubix_Application::getId() . '_lang_' . Cubix_I18n::getLang() . '_page_' . $page_num . $region_slug . $city_params[2];

		$count = $cache->load($new_c_cache_key);
		if ( ! $escorts = $cache->load($new_cache_key) ) {
			$escorts = Model_Escort_List::getFiltered($filter, 'date-activated', $page_num, 10, $count, 'new_list', $region_slug);
			
			if ( count($escorts) ) {
				foreach ( $escorts as $k => $escort ) {
					$cache_key = 'v2_' . $escort->showname . '_new_profile_' . Cubix_I18n::getLang() . '_page_' . $page_num . $region_slug . '_' . $escort->id;
					$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
					$escorts[$k]->profile = $model->get($escort->id, $cache_key);
				}
			}
			
			$cache->save($escorts, $new_cache_key, array());
			
			$cache->save($count, $new_c_cache_key, array());
		}

		$sess_name = 'new_list';
		$region_s = $region_slug;
		if ( $sess_name ) {

			$sid = 'sedcard_paging_' . Cubix_Application::getId() . '_' . $region_s;

			$sess = new Zend_Session_Namespace($sid);

			$sess->{$sess_name} = array();
			foreach ( $escorts as $escort ) {
				$sess->{$sess_name}[] = $escort->id;
			}
		}
		
		$this->view->count = $count;
		$this->view->escorts = $escorts;
		
		
		$this->view->new_list_cache_key = 'new_list_' . $cache_key;
	}

	public function indexAction()
	{

		// <editor-fold defaultstate="collapsed" desc="Redirect to new URLS">
		$route_name = Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName();

		/*if (! in_array($route_name, array('escorts-filter', 'additional-filter','escorts-filter-gender', 'escort-videos' ))){
			var_dump($route_name);die;
		}*/
		//var_dump($route_name, $this->_getParam('req'), $this->_getParam('category'),$this->_getParam('city'),$this->_getParam('city_id'));
		$redirect_params = array();
		//$trans = explode('/', $this->_getParam('req'))[1] == 'trans';
		//REMOVE IF NO ERRORS OCCUR
		/*if (!$trans && ($route_name == 'escorts-filter-s' || $route_name == 'escorts-filter-def-s' )) {
			$params = explode('/', $this->_getParam('req'));
			if (count($params)  >2){
                foreach($params as $param) {
                    $param = explode('_', $param);
                    if ( count($param) > 1 ) {
                        switch($param[0]) {
                            case 'region' :
                                $redirect_params['region'] = $param[1];
                                break;
                            case 'city' :
                                $redirect_params['city'] = $param[2];
                                $m = new Cubix_Geography_Cities();
                                $city = $m->getBySlug($redirect_params['city']);
                                if ( ! $city ) {
                                    $this->_redirect('/', array('code' => 301));
                                    return;
                                }
                                $redirect_params['city'] = $city->city_url_slug;
                                $redirect_params['city_id']	= $city->id;
                                break;
                        }
                    }
                }
                $this->_redirect(self::$linkHelper->getLink('escorts', $redirect_params, true), array('code' => 301));
            }
		} else {*/
			$city = Model_Countries::getCityById($this->_getParam('city_id'));
			
			if($this->_getParam('city')){
				//this code overwrite code from I18n.php: line 90 which I have no clue why was hardcoded there ((((
				$model_cities = new Model_Cities();
				$region = $model_cities->getById($this->_getParam('city_id'));
				$this->_request->setParam('region', $region->region_slug);
			}
			if ( $this->_getParam('city') != $city->url_slug ) {
				
				if($this->_getParam('city') == 'sex-in-zurich-city'){
					$this->_redirect('/', array('code' => 301));
				}
				
				$redirect_params = array(
					'city'	=> $city->url_slug,
					'city_id'	=> $city->id
				);

				$this->_redirect(self::$linkHelper->getLink('escorts', $redirect_params, true), array('code' => 301));
			}
		//}
		// </editor-fold>
		// 
		// <editor-fold defaultstate="collapsed" desc="If it is ajax request, disable the layout and render only list.phtml">
		if ( ! is_null($this->_getParam('ajax')) ) {
			$this->view->layout()->disableLayout();
			$this->_helper->viewRenderer->setScriptAction('list');
		}
		// </editor-fold>

		$this->view->quick_links = false;
		$upcoming_tours = false;

		$req = $this->_getParam('req');
				
		$has_filter = false;
		if ( preg_match('#^/(filter|sort)_(.+)#', $req) ) {
			$req .= '/regular' . $req;
			$has_filter = true;
		}

		$this->_request->setParam('req', $req);
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Parse out parameters from "pretty" urls">
		
		$req = explode('/', $req);

		$menus = array('filter' => NULL, 'sort' => NULL, 'filter_city' => NULL);

		$cache = Zend_Registry::get('cache');
		$defs = Zend_Registry::get('definitions');

		$filterStructure = $defs['escorts_filter'];

		$menus['filter'] = new Cubix_NestedMenu(array('childs' => $filterStructure));
		$menus['filter']->setId('filter-options');
		$menus['filter']->setSelected($menus['filter']->getByValue(NULL));

		$escorts_sort = $defs['escorts_sort'];
		$escorts_sort[] = array(
			'title' => $this->view->t('sort_city'),
			'value' => 'city-asc'
		);

		$current_sorting = "random";
		if ( isset($_COOKIE['sorting']) ) {
			$current_sorting = $_COOKIE['sorting'];
		}
		
		//$menus['sort'] = new Cubix_NestedMenu(array('childs' => $defs['escorts_sort']));
		$menus['sort'] = new Cubix_NestedMenu(array('childs' => $escorts_sort));
		$menus['sort']->setId('sort-options');
		$menus['sort']->setSelected($menus['sort']->getByValue($current_sorting));

		$filter_city = $defs['escorts_cities_filter'];
		
		//removing empty cities from filter
		$cities_by_escorts_count = Model_Cities::getEscortsCountPerCity();
		foreach ( $filter_city as $key => $value ) {
			if( $value['value']=='all' )continue;
			foreach($cities_by_escorts_count as $city){
				if($city->id == $value['city_id']){
					
					if($city->escorts_count < 1){
						
						unset($filter_city[$key]);
					}
				}
			}
		}

		 $menus['filter_city'] = new Cubix_NestedMenu(array('childs' => $filter_city));
		 $menus['filter_city']->setId('city-filter-options');
		 $menus['filter_city']->setSelected($menus['filter_city']->getByValue('all'));

		$this->view->menus = $menus;

		$params = array(
			'sort' => $current_sorting,
			'filter' => array(/*array('field' => 'girls', 'value' => array())*/),
			'page' => 1
		);

		$static_page_key = 'main';
		$is_tour = false;
		$is_city_tour = false;
				
		$s_config = Zend_Registry::get('system_config');
		$this->view->showMainPremiumSpot = $s_config['showMainPremiumSpot'];

        if ( ! $s_config['showMainPremiumSpot'] )
			$static_page_key = 'regular';
		
		if ( $this->_getParam('region') ) {
			$params['filter'][] = array('field' => 'region', 'value' => $this->_getParam('region'));
			//$params['region'] = $this->_getParam('region');
		}
		if ( $this->_getParam('city') ) {
			$params['filter'][] = array('field' => 'city', 'value' => $this->_getParam('city'));
			$params['city'] = $this->_getParam('city');
		}
		
		if ( $this->_getParam('city_id') ) {
			$params['filter'][] = array('field' => 'city_id', 'value' => (int) $this->_getParam('city_id'));
			$params['city_id'] = (int) $this->_getParam('city_id');
		}
		
		$category = $this->_getParam('category');

		if(! is_null($this->_getParam('outcall')) ){
			$category = 'escorts';
		}


		if ($category) {
			
			switch( $category )
			{
				case 'nuove':
					//$params['filter'][] = array('field' => 'new_arrivals', 'value' => array());
					$static_page_key = 'nuove';
					$param = array('sort', 'newest');
				break;
				case 'independantes':
					$static_page_key = 'independantes';
					$params['filter'][] = array('field' => 'independantes', 'value' => array());
				continue;
				case 'regular':
					$static_page_key = 'regular';
					$params['filter'][] = array('field' => 'gender', 'value' => GENDER_FEMALE);
				continue;
				case 'agence':
					$static_page_key = 'agence';
					$params['filter'][] = array('field' => 'agence', 'value' => array());
				continue;
				case 'boys':
					$static_page_key = 'boys';
					$params['filter'][0] = array('field' => 'boys', 'value' => array());
				continue;
				case 'trans':
					$static_page_key = 'trans';
					$params['filter'][0] = array('field' => 'trans', 'value' => array());
				continue;
				case 'outcall':
					$static_page_key = 'outcall';
					$params['filter'][0] = array('field' => 'available-for-outcall', 'value' => array());
				continue;
					
				case 'citytours':
					$static_page_key = 'citytours';
					$this->view->is_tour = $is_tour = true;
					$is_city_tour = true;
					$params['filter'][] = array('field' => 'tours', 'value' => array());
				continue;
				case 'upcomingtours':
					$static_page_key = 'upcomingtours';
					$this->view->is_tour = $is_tour = true;
					$this->view->is_upcomingtour = true;
					$upcoming_tours = true;
					$params['filter'][] = array('field' => 'upcomingtours', 'value' => array());
				continue;
				case 'happy-hours':
					$static_page_key = 'happy-hours';
				continue;
				case 'chat-online':
					$static_page_key = 'chat-online';
				continue;
				case 'videos':
					$static_page_key = 'videos';
					$params['filter'][] = array('field' => 'videos', 'value' => array());
				continue;
				case 'virtual':
					$static_page_key = 'cam-booking-available';
					$params['filter'][] = array('field' => 'cam-booking-available', 'value' => array());
				continue;
				case 'massage':
					$static_page_key = 'massage';
					$params['filter'][] = array('field' => 'massage', 'value' => array());
				continue;
				case 'companion':
					$this->_redirect('escorts');
				continue;
				case 'escorts':
					$static_page_key = 'escorts';
					$params['filter'][] = array('field' => 'outcall', 'value' => array());
				continue;
				case 'romandie':
					$static_page_key = 'romandie';
					$this->_request->setParam('region', 'romandie');
					$params['filter'][] = array();
				continue;

				default:
					$this->_response->setRawHeader('HTTP/1.1 404 Not Found');
					$this->_forward('error', 'error', null, array('error_msg' => '404 Page Not Found. Please try the following links instead.'));
					return;
			}
		}

		if ( $static_page_key == 'nuove' ) {
			$this->_forward('new-list');
			return;
		}
		
		if ( $static_page_key == 'videos' ) {
			$this->_forward('videos');
			return;
		}
		
		$is_main_page = ($static_page_key == 'main' && ( ! isset($params['city']) && ! isset($params['zone']) /*&& ! isset($params['region'])*/ ));

		if ( $s_config['showMainPremiumSpot'] ) {
			if ( $is_main_page ) {
					return $this->_forward('main-page');
			}
		}
        
		if ( $static_page_key == 'happy-hours' ) {
			return $this->_forward('happy-hours');
		}
		
		if ( $static_page_key == 'chat-online' ) {
			return $this->_forward('chat-online');
		}

		foreach ( $req as $r ) {
			if ( ! strlen($r) ) continue;

			$param = explode('_', $r);
			
			$param_name = $param[0];
			array_shift($param);
			
			switch ( $param_name ) {
				case 'filter':
					$has_filter = true;
					/* >>> For nested menu */
					$selected_item = $menus['filter']->getByValue(implode('_', $param));
					if ( ! is_null($selected_item) ) {
						$menus['filter']->setSelected($selected_item);
					}
					/* <<< */
					
					$field = reset($param);
					array_shift($param);
					
					$value = array_slice($param, 0, 2);
					
					$params['filter'][] = array('field' => $field, 'value' => $value , 'main' => TRUE);
				break;
				case 'cityfilter':
					$field = reset($param);
					$value = array_slice($param, 0, 2);

					$selected_item = $menus['filter_city']->getByValue(implode('_', $param));
					
					if(!empty($value) && $value[0] != 'all'){
						$menus['filter_city']->setSelected($selected_item);
						$params['filter'][] = array('field' => 'city_slug', 'value' => $value , 'main' => TRUE);
					}else{
						$menus['filter_city']->setSelected($menus['filter_city']->getByValue('all'));
					}
				break;
				case 'page':
					$page = intval(reset($param));
					if ( $page < 1 ) {
						$page = 1;
					}
					$params['page'] = $page;
				break;
				case 'sort':
					$params['sort'] = reset($param);
					
					$selected_item = $menus['sort']->getByValue($params['sort']);
					if ( ! is_null($selected_item) ) {
						$menus['sort']->setSelected($selected_item);
					}
					$has_filter = true;

					$current_sorting = $params['sort'];

					if ( $current_sorting != $_COOKIE['sorting'] ) {
						setcookie("sorting", $current_sorting, strtotime('+1 year'), "/", "." . Cubix_Application::getById()->host);						
						$_COOKIE['sorting'] = $current_sorting;						
					}
				break;
				case 'region':
				case 'state':
					//$params['country'] = $param[0];
					//$params['region'] = $param[0];
					$this->_request->setParam('region', $param[0]);
					$params['filter'][] = array('field' => 'region', 'value' => $param[0]);
					$page = intval(reset($param));
					
					if ( $page < 1 ) {
						$page = 1;
					}
					$params['page'] = $page;
				break;
				case 'city':
				case 'zone':
				    $key = ($param_name == 'city') ? 'city_slug' : $param_name;
					$params[$key] = $param[1];
					$params['filter'][] = array('field' => $key, 'value' => array($param[1]));
				break;
				case 'name':
					$has_filter = true;
					$srch_str = "";
					if ( count($req) ) {
						foreach( $req as $r ) {
							if ( preg_match('#name_#', $r) ) {
								$srch_str = str_replace("name_", "", $r);
								$srch_str = str_replace("_", "\_", $srch_str);
							}
						}
					}
					
					$params['filter'][] = array('field' => 'name', 'value' => array('%' . $srch_str . '%'));
				break;
				/*default:
					if ( ! in_array($param_name, array('nuove', 'independantes', 'regular', 'agence', 'boys', 'trans', 'citytours', 'upcomingtours', 'happy-hours', 'chat-online', 'videos')) ) {
						$this->_response->setRawHeader('HTTP/1.1 404 Not Found');
						$this->_forward('error','error', null, array('error_msg' => '404 Page Not Found. Please try the following links instead.'));
						return;
					}*/
			}
		}

		// </editor-fold>
		
		$this->view->static_page_key = $static_page_key;
		

		$filter_params = array(
			'order' => 'e.date_registered DESC',
			'limit' => array('page' => 1),
			'filter' => array()
		);

		// <editor-fold defaultstate="collapsed" desc="Map parameters from url to SQL where clauses">
		$filter_map = array(
			'french' => 'e.nationality_id = 15',

			'age' => 'e.age < ? AND e.age > ?',
			'ethnic' => 'e.ethnicity = ?',
			'height' => 'e.height < ? AND e.height > ?',
			'weight' => 'e.weight < ? AND e.weight > ?',
			'cup-size' => 'e.cup_size = ?',
			'hair-color' => 'e.hair_color = ?',
			'eye-color' => 'e.eye_color = ?',
			'dress-size' => 'e.dress_size = ?',
			'shoe-size' => 'e.shoe_size = ?',
			'available-for-incall' => 'e.incall_type IS NOT NULL',
			'available-for-outcall' => 'e.outcall_type IS NOT NULL',
			'service-for' => 'FIND_IN_SET(?, e.sex_availability)',
			'smoker' => 'e.is_smoking = ?',
			'language' => 'FIND_IN_SET(?, e.languages)',
			'now-open' => 'e.is_now_open',
			'region' => 'r.slug = ?',
			//'city' => 'ct.url_slug = ?',
            'city_slug' => 'ct.slug = ?',
			'city_id'	=> 'ct.id = ?',
			'cityzone' => 'c.id = ?',
			'zone' => 'cz.slug = ?',
			'name' => 'e.showname LIKE ?',
			'vr' => 'verified_status = 2',

			'new_arrivals' => 'e.gender = ' . GENDER_FEMALE,
			'independantes' => 'eic.is_agency = 0 AND eic.gender = ' . GENDER_FEMALE,
			'agence' => 'eic.is_agency = 1 AND eic.gender = ' . GENDER_FEMALE,
			'boys' => 'eic.gender = ' . GENDER_MALE,
			'eBoys' => 'e.gender = ' . GENDER_MALE,
			'trans' => 'eic.gender = ' . GENDER_TRANS,
			'girls' => 'eic.gender = ' . GENDER_FEMALE,
			'gender' => 'eic.gender = ?',
			
			'tours' => 'eic.is_tour = 1',
			'upcomingtours' => 'eic.is_upcoming = 1',
			'cam-booking-available' => 'e.cam_status <> ""',    /*'e.cam_booking_status = 1',*/
			'massage' => Model_Escort_List::getSqlEquivalent('massage-only'),
			'outcall' => Model_Escort_List::getSqlEquivalent('outcall-only'),

			'videos' => 'e.has_video = 1'	
			
		);

		// Services SQL map
		$services_map = array(
			'de' => array(
				'svc-69-position', 'svc-anal-sex', 'svc-in-den-mund-spritzen', 'svc-ins-gesicht-spritzen',
				'svc-dildo-spiele-toys', 'svc-zungenk--sse', 'svc-girlfriend-experience--gfe-', 'svc-k--ssen',
				'svc-franz--sisch-mit', 'svc-franz--sisch-ohne', 'svc-franz--ohne-bis-zum-abschluss',
				'svc-sex-in-allen-positionen', 'svc-k--rperbesamung', 'svc-erotische-massagen', 'svc-feinmassage',
				'svc-k--ssen-bei-sympathie', 'svc-handjob'
			),
			'en' => array(
				'svc-69-position', 'svc-anal-sex', 'svc-cum-in-mouth', 'svc-cum-on-face', 'svc-dildo-play-toys',
				'svc-french-kissing', 'svc-girlfriend-experience--gfe-', 'svc-kissing', 'svc-blowjob-with-condom',
				'svc-blowjob-without-condom', 'svc-blowjob-without-condom-to-completion',
				'svc-sex-in-different-positions', 'svc-cumshot-on-body--cob-', 'svc-erotic-massage',
				'svc-intimate-massage', 'svc-kissing-if-good-chemistry', 'svc-handjob'
			),
			'es' => array(
				'svc-posici--n-69', 'svc-sexo-anal', 'svc-semen-en-la-boca', 'svc-semen-en-la-cara',
				'svc-juego-con-dildo--juguetes', 'svc-beso-franc--s', 'svc-experiencia-de-novia--gfe-', 'svc-besar',
				'svc-sexo-oral-con-cond--n', 'svc-sexo-oral-sin-cond--n', 'svc-sexo-oral-sin-cond--n-para-terminar',
				'svc-sexo-en-diferentes-posiciones', 'svc-eyaculaci--n-en-el-cuerpo--cob-', 'svc-masaje-er--tico',
				'svc-masaje---ntimo', 'svc-besar-si-hay-buena-qu--mica', 'svc-masturbaci--n-con-la-mano'
			),
			'fr' => array(
				'svc-position-69', 'svc-sexe-anal', 'svc-ejaculation-dans-la-bouche', 'svc-ejaculation-faciale',
				'svc-jeux-avec-gode-sextoys', 'svc-embrasser-avec-la-langue', 'svc-comme-avec-une-petite-amie--gfe-',
				'svc-bisous', 'svc-fellation-avec-pr--servatif', 'svc-fellation-sans-pr--servatif',
				'svc-fellation-sans-pr--servatif-jusqu----l---jaculation', 'svc-sexe-dans-diff--rentes-positions',
				'svc-ejaculation-sur-le-corps', 'svc-massage---rotique', 'svc-massage-intime',
				'svc-bisous-selon-l-alchimie', 'svc-handjob'
			),
			'hu' => array(
				'svc-69-poz--ci--', 'svc-an--l', 'svc-sz--jba---lvez--s', 'svc-arcra--lvez--s',
				'svc-vibr--tor-j--t--kok', 'svc-francia-cs--k', 'svc-bar--tn-----lm--ny--gfe-', 'svc-cs--kol--z--s',
				'svc-szop--s-gumival', 'svc-szop--s-gumi-n--lk--l', 'svc-szop--s-gumi-n--lk--l-a-v--g--ig',
				'svc-sex-t--bbf--le-poz--ci--ban', 'svc-testre--lvez--s--cob-', 'svc-erotikus-massz--zs',
				'svc-intim-massz--zs', 'svc-cs--kol--z--s-ha-j---a-k--mia', 'svc-k--zimunka'
			),
			'it' => array(
				'svc-posizione-69', 'svc-sesso-anale', 'svc-sborrata-in-bocca', 'svc-sborrata-in-faccia',
				'svc-dildo-toys', 'svc-bacio-alla-francese', 'svc-esperienza-con-fidanzata--gfe-', 'svc-bacio',
				'svc-pompino-con-preservativo', 'svc-pompino-senza-preservativo',
				'svc-pompino-senza-preservativo-completo', 'svc-sesso-in-posizioni-diverse',
				'svc-sborrata-sul-corpo--cob-', 'svc-massaggio-erotico', 'svc-massaggio-parti-intime',
				'svc-bacio-in-caso-di-buona-empatia', 'svc-sega'
			),
			'ro' => array(
				'svc-69', 'svc-sex-anal', 'svc-cum-in-mouth', 'svc-cum-on-face', 'svc-dildo-juc--rii',
				'svc-s--rut-fran--uzesc', 'svc-experien-----cu-prietena-gfe-', 'svc-s--rut', 'svc-oral-cu-prezervativ',
				'svc-oral-f--r---prezervativ', 'svc-oral-f--r---prezervativ-cu-finalizare',
				'svc-sex---n-diverse-pozi--ii', 'svc-cumshot-pe-corp--cob-', 'svc-masaj-erotic', 'svc-masaj-intim',
				'svc-s--rut-dac---exist---compatibilitate', 'svc-handjob'
			)
		);

		foreach ( $services_map[Cubix_I18n::getLang()] as $value ) $filter_map[$value] = 'es.service_id = ?';

		// <editor-fold defaultstate="collapsed" desc="Get page from request parameters">
		if ( $this->_request->page ) {
			$params['page'] = $this->_request->page;
		}
		
		$page = intval($params['page']);
		if ( $page == 0 ) {
			$page = 1;
		}

		$filter_params['limit']['page'] = $page;
		// </editor-fold>

		$selected_filter = $menus['filter']->getSelected();

		foreach ( $params['filter'] as $i => $filter ) {

			if ( ! isset($filter_map[$filter['field']]) ) continue;
			
			$value = $filter['value'];


			if ( isset($filter['main']) ) {
			
				if ( isset($selected_filter->internal_value) ) {
					$value = $selected_filter->internal_value;
				}				
				elseif ( ! is_null($item = $menus['filter']->getByValue($filter['field'] . ( (isset($value[0]) && $value[0]) ? '_' . $value[0] : '')) ) ) {
					$value = $item->internal_value;
				}

			}
			
			$filter_params['filter'][$filter_map[$filter['field']]] = $value;
		}


		foreach ( $params['filter'] as $i => $filter ) {

			if ( ! isset($filter_map[$filter['field']]) ) continue;
			
			$value = $filter['value'];


			if ( isset($filter['main']) ) {
			
				if ( isset($selected_filter->internal_value) ) {
					$value = $selected_filter->internal_value;
				}				
				elseif ( ! is_null($item = $menus['filter']->getByValue($filter['field'] . ( (isset($value[0]) && $value[0]) ? '_' . $value[0] : '')) ) ) {
					$value = $item->internal_value;
				}

			}
			
			$filter_params['filter'][$filter_map[$filter['field']]] = $value;
		}

		// Katie's request
	
		if ($static_page_key == 'main') {
			unset($filter_params['filter']['eic.gender = ?']);
		}
		

		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Sorting parameters SQL mappings">
		$sort_map = array(
			'price-asc' => 'price-asc', // will be modified from escorts
			'price-desc' => 'price-desc', // will be modified from escorts
			'random' => 'ordering DESC',
			'alpha' => 'showname ASC',
			'most-viewed' => 'hit_count DESC',
			'newest' => 'date_registered DESC',
			'last-modified' => 'date_last_modified DESC'
		);
		
		if ( isset($sort_map[$params['sort']]) ) {
			$filter_params['order'] = $sort_map[$params['sort']];
			$this->view->sort_param = $params['sort'];
		}
		// </editor-fold>
			
		$model = new Model_EscortsV2();
		$count = 0;

		// <editor-fold defaultstate="collapsed" desc="Cache key generation">
		$filters_str = '';
		
		foreach ( $filter_params['filter'] as $k => $filter ) {
			if ( ! is_array($filter) ) {
				$filters_str .= $k . '_' . $filter;
			}
			else {
				$filters_str .= $k;
				foreach($filter as $f) {
					$filters_str .= '_' . $f;
				}
			}
		}
		// </editor-fold>

		$geoData = Cubix_Geoip::getClientLocation();
		
		$cache_key = 'v2_' . Cubix_I18n::getLang() . '_' . $geoData['country_iso'] .'_'. $filter_params['order'] . '_' . $filter_params['limit']['page'] . '_' . $filters_str . (string) $has_filter;
		$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
		
		$this->view->params = $params;
		
		//SHOW UPCOMING TOURS BUTTON LOGIC
		$cache_key_tours = 'v2_upcoming_tours_count';
		$t_key = isset($params['city']) ? $params['city'] : 'total';
		$tours_count = $cache->load($cache_key_tours);
		if ( ! $tours_count || ! isset($tours_count[$t_key]) ) {
			if ( ! $tours_count ) $tours_count = array();
			$upcoming_filter = array();
			if ( isset($filter_params['filter']['ct.slug = ?']) ) {
				$upcoming_filter['ct.slug = ?'] = $filter_params['filter']['ct.slug = ?'];
			}
			
			Model_Escort_List::getTours(true, $upcoming_filter, 'random', 1, 30, $t_count);
			
			$tours_count[$t_key] = $t_count;
			$cache->save($tours_count, $cache_key_tours, array());
		}
		$this->view->show_tours_btn = false;
		if ( ( ! $is_tour || $is_city_tour ) && $tours_count[$t_key] ) {
			$this->view->show_tours_btn = true;
		}
		
		$this->view->per_page = 30;
		if (in_array($params['city_id'], array(18))) { // and wthur-thurgau-st-gallen
			$this->view->per_page = 40; // 7 rows
		} elseif ( in_array($params['city_id'], array(19))) { // basel-und-umgebung
			$this->view->per_page = 50; // 10 rows
		}

		if ( array_key_exists( 'e.is_now_open', $filter_params['filter'] ) ) {
			$filter_params['filter']['( e.is_now_open = 1 OR e.available_24_7 = 1 )'] = 1;
			unset($filter_params['filter']['e.is_now_open']);
		}
		$count = $cache->load($cache_key . '_count');
		if ( ! $escorts = $cache->load($cache_key) ) {
			if ( isset($is_tour) && $is_tour ) {
				//$premiums = $model->getAllTourMainPremiumSpot($filter_params, $count, $upcoming_tours);
				$escorts = Model_Escort_List::getTours($upcoming_tours, $filter_params['filter'], $params['sort'], $filter_params['limit']['page'], 30, $count);
			}
			else {
				$per_page = 30;
				
				if (in_array($params['city_id'], array(18))) { // and wthur-thurgau-st-gallen
					$per_page = 40; // 7 rows
				} elseif ( in_array($params['city_id'], array(19))) { // basel-und-umgebung
					$per_page = 50; // 10 rows
				}
				//$escorts = $model->getAll($filter_params, $count, 'regular_list', $has_filter);

				$escorts = Model_Escort_List::getFiltered($filter_params['filter'], $params['sort'], $filter_params['limit']['page'], $per_page, $count, 'regular_list', $this->_request->region);
			}
			
			$cache->save($escorts, $cache_key, array());
			$cache->save($count, $cache_key . '_count', array());
		}
		
		$sess_name = 'regular_list';
		$filter_name = 'regular_list_criterias';
		$region_s = $this->_request->region;
		if ( $sess_name ) {

			$sid = 'sedcard_paging_' . Cubix_Application::getId() . '_' . $region_s;

			$sess = new Zend_Session_Namespace($sid);

			$sess->{$filter_name} = array(
				'filter'	=> $filter_params['filter'],
				'sort'		=> $params['sort'],
				'page'		=> $filter_params['limit']['page'],
				'first_page'		=> $filter_params['limit']['page'],//to track first loaded page 
				'last_page'		=> $filter_params['limit']['page'],//to track last loaded page
			);

			$sess->{$sess_name} = array();
			foreach ( $escorts as $escort ) {
				$sess->{$sess_name}[] = $escort->id;
			}

		}

		setcookie('banner_popup_show', '1', 0, '/', '.and6.com');
		
		// <editor-fold defaultstate="collapsed" desc="Find and Slice Premium Escorts">
		$premiums = $premiums_fill = array();

		if ( $filter_params['order'] == 'ordering DESC' && ! $has_filter ) {
			foreach ( $escorts as $i => $escort ) {
				if ( ! $escort->is_premium && $i == 0 ) {
					break;
				}
				elseif ( $escort->is_premium && $i == count($escorts) - 1 ) {
					$premiums = $escorts;
					$escorts = $premium_fill = array();
				}

				if ( ! $escort->is_premium ) {
					$c = intval(($i - 1) / 5);
					if ( $c == 0 ) {
						$this->view->one_row = true;
					}
					$c = 5 - ($i - $c * 5);
					$this->view->fake_escorts_count = $c;

					$escorts[$i]->is_first_one = true;
					$premiums = array_slice($escorts, 0, $i);
					$premiums_fill = array_slice($escorts, $i, $c);
					$escorts = array_slice($escorts, $i + $c);

					break;
				}
			}
		}
		// </editor-fold>

		if ( $is_main_page && $filter_params['order'] == 'ordering DESC' ) {
			shuffle($escorts);
		}


		// $should_videos_be_shown = !array_filter($params['filter'], function($f) {
		// 	return !in_array($f['field'], array('region', 'city', 'city_id', 'gender', 'regular', 'trans'));
		// });


		
		
		$videos = Model_Escort_List::getLatestVideos($params['city_id']);
		$this->view->videos = $videos;
		$this->view->is_main_page = $is_main_page;
		$this->view->count = $count;
		$this->view->escorts = $escorts;
		$this->view->premiums = $premiums;
		$this->view->premiums_fill = $premiums_fill;
		$this->view->has_filter = $has_filter;
		$this->view->city_id = $params['city_id'] ? $params['city_id'] : -1;
		

		// <editor-fold defaultstate="collapsed" desc="Premiums on right side">
		if ( ! count($premiums) && ! $is_tour && $static_page_key != 'nuove' && $static_page_key != 'boys' )
		{
			$rows = ceil(count($escorts) / 5);
			$per_page = $rows - 1;
			if ( $per_page < 0 ) {
				$per_page = 0;
			}
			
			$right_premiums = Model_Escort_List::getMainPremiumSpot();
			$right_premiums = array_slice($right_premiums, 0, $per_page);
			$this->view->right_premiums = $right_premiums;
		}
		// </editor-fold>

		$this->view->html_cache_key = $cache_key . $static_page_key;
		
		//////////////////////////////////////////////////////////////
		$model = new Model_EscortsV2();
		
		if ($this->view->allowChat2())
		{
			$res = $model->getChatNodeOnlineEscorts(1, 1000);
					
			$ids = $res['ids'];
			$this->view->escort_ids = explode(',', $ids);
		}
		/////////////////////////////////////////////////
	}


    public function ajaxBubbleAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		
		$page = intval($this->_getParam('page', 1));
		$per_page = intval($this->_getParam('per_page', 10));
		if ( $page < 1 ) $page = 1;
        

		echo $this->view->bubbleTextsWidget($page, $per_page, $this->_getParam('region', 'deutschschweiz'));
    }


    public function ajaxOnlineAction()
	{
        $page = intval($this->_getParam('page', 1));
		if ( $page < 1 ) $page = 1;
        $per_page = 2;
        $count = 0;
		$this->view->layout()->disableLayout();
        $model = new Model_EscortsV2();
		
		if (!$this->view->allowChat2())
			$results = $model->getChat11OnlineEscorts($page, $per_page);
		else
			$results = $model->getChatNodeOnlineEscorts($page, $per_page);
		
		$this->view->escorts = $results['escorts'];
        $this->view->per_page = $per_page;
		$this->view->page = $page;
        $this->view->count = $results['count'];
    }

	/**
	 *
	 * @param string $r
	 */
	protected function _makeFilterArgument($r)
	{
		$param = explode('_', $r);

		if ( count($param) < 2 ) {
			switch( $r ) {
				case 'independantes':
				case 'agence':
				case 'boys':
				case 'trans':
				case 'citytours':
				case 'upcomingtours':
				return array('field' => $r, 'value' => array());
			}
		}

		$param_name = $param[0];
		array_shift($param);

		switch ( $param_name ) {
			case 'filter':
				$field = reset($param);
				array_shift($param);

				$value = array_slice($param, 0, 2);

				$filter = array('field' => $field, 'value' => $value , 'main' => TRUE);
			break;
			case 'region':
			case 'state':
				$filter = array('field' => 'region', 'value' => $param[1]);
			break;
			case 'city':
			case 'cityzone':
				$filter = array('field' => $param_name, 'value' => array($param[1]));
			break;
			case 'name':
				$filter = array('field' => 'name', 'value' => array('%' . $param[0] . '%'));
			break;
		}

		return $filter;
	}

	/**
	 * Calculates filter counts for each
	 *
	 * @param integer $count
	 * @param array $filterParams
	 * @param Cubix_NestedMenu $menu
	 * @param string $cacheKey
	 */
	protected function _calcFilterCounts($count, $filterParams, Cubix_NestedMenu $menu, $cacheKey = null)
	{
		$model = new Model_EscortsV2();
		$defs = Zend_Registry::get('defines');

		$filterMap = array(
			'svc-anal' => 'e.svc_anal = ?',
			'svc-kissing' => 'e.svc_kissing = ?',
			'svc-kissing-with-tongue' => 'e.svc_kissing = ?',
			'svc-blowjob-with-condom' => 'e.svc_blowjob = ?',
			'svc-blowjob-without-condom' => 'e.svc_blowjob = ?',
			'svc-cumshot-in-face' => 'e.svc_cumshot = ?',
			'svc-cumshot-in-mouth-swallow' => 'e.svc_cumshot = ?',
			'svc-cumshot-in-mouth-spit' => 'e.svc_cumshot = ?',

			'age' => 'e.age BETWEEN ? AND ?',
			'ethnic' => 'e.ethnicity = ?',
			'height' => 'e.height > ? AND e.height < ?',
			'weight' => 'e.weight > ? AND e.weight < ?',
			'breast-size' => 'e.breast_size = ?',
			'hair-color' => 'e.hair_color = ?',
			'eye-color' => 'e.eye_color = ?',
			'dress-size' => 'e.dress_size = ?',
			'shoe-size' => 'e.shoe_size = ?',
			'available-for' => 'e.availability = ?',
			'service-for' => 'FIND_IN_SET(?, e.sex_availability)',
			'smoker' => 'e.is_smoker = ?',
			'language' => 'FIND_IN_SET(?, e.languages)',
			'now-open' => 'e.is_now_open',
			'region' => 'r.slug = ?',
			'city' => 'cc.slug = ?',
			'cityzone' => 'c.id = ?',
			'name' => 'e.showname LIKE ?',

			'new_arrivals' => 'e.gender = ' . GENDER_FEMALE,
			'independent' => 'e.agency_id IS NULL AND e.gender = ' . GENDER_FEMALE,
			'agence' => 'e.agency_id IS NOT NULL AND e.gender = ' . GENDER_FEMALE,
			'boys' => 'e.gender = ' . GENDER_MALE,
			'trans' => 'e.gender = ' . GENDER_TRANS,
			'girls' => 'e.gender = ' . GENDER_FEMALE,

			'tours' => 'e.is_on_tour = 1',
			'upcomingtours' => 'e.tour_date_from > CURDATE()'
		);

		$counts = array();

		$filterStructure = $defs['escorts_filter'];

		foreach ( $filterStructure as $i => $filter ) {
			$fake = false;
			if ( ! isset($filter['childs']) ) {
				$filter['childs'] = array($filter);
				$fake = true;
			}
			
			foreach ( $filter['childs'] as $j => $child ) {
				$f = $this->_makeFilterArgument('filter_' . $child['value']);

				$newFilter = $filterParams;

				if ( ! isset($filterMap[$f['field']]) ) continue;

				$value = $f['value'];

				if ( isset($f['main']) && $f['main'] ) {
					if ( isset($child['internal_value']) ) {
						$value = $child['internal_value'];
					}
				}

				$newFilter['filter'][$filterMap[$f['field']]] = $value;

				$count = 0; // $model->getCount($newFilter);
				
				if ( $fake ) {
					$filterStructure[$i]['title'] .= " ($count)";
				}
				else {
					$filterStructure[$i]['childs'][$j]['title'] .= " ($count)";;
				}
			}
		}

		if ( ! is_null($cacheKey) ) {
			Zend_Registry::get('cache')->save($filterStructure, $cacheKey, array());
		}

		$menu->setStructure(array('childs' => $filterStructure));
	}

	public function profileAction()
	{	
		if ( isset($_GET['v2']) ) {
			return $this->_forward('profile-v2', 'escorts', 'default');
		}
		
		//<editor-fold defaultstate="collapsed" desc="Redirect to new URLS">
		$route_name = Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName();
		if ( $route_name == 'escort-profile-def-old' ) {
			$this->_redirect(self::$linkHelper->getLink('profile', array('showname' => $this->_getParam('escortName'), 'escort_id' => $this->_getParam('escort_id'))), array('code' => 301));
		}
		//</editor-fold>
		$route_name = Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName();
		
		$this->view->layout()->setLayout('profile');
		
		$this->view->is_profile = true;
		$this->view->is_preview = $is_preview = $this->_getParam('prev', false);
		$this->view->show_advertise_block = $show_advertise_block = $this->_getParam('show_advertise_block', false);
		
		$showname = $this->_getParam('escortName');
		$escort_id = $this->_getParam('escort_id');
		
		$model = new Model_EscortsV2();
		
		$cache_key = 'v2_' . $showname . '_' . $escort_id .  '_profile_' . Cubix_I18n::getLang();
		$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

		$this->view->profile_cache_key = 'html_profile_' . $cache_key;
		
		//$escort = $model->get($showname, $cache_key);
		$escort = $model->get($escort_id, $cache_key, $is_preview);
		
		if ($escort->showname != $showname)
		{
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			die;
		}

		$city = new Cubix_Geography_Cities();
		$city = $city->get($escort->base_city_id);
		$this->_request->setParam('region', $city->region_slug);
		$this->view->base_city = $city;
		
		if ( ! $escort || isset($escort['error']) )
		{
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			die;
			return;
		}

		//		if ( isset($escort->escort_status) && $escort->escort_status != 32 ) {
		//			$this->_forward('profile-disabled');
		//			$this->_request->setParam('city_slug', $escort->city_slug);
		//		}
		
		$add_esc_data = $model->getRevComById($escort->id);
		$block_website = $model->getBlockWebsite($escort->id);

		$escort->block_website = $block_website;
		$escort->disabled_reviews = $add_esc_data->disabled_reviews;
		$escort->disabled_comments = $add_esc_data->disabled_comments;
		$this->view->escort_id = $escort->id;
		$this->view->escort = $escort;

		if ( $escort->home_city_id ) {
			$m_city = new Cubix_Geography_Cities();
			$home_city = $m_city->get($escort->home_city_id);

			$m_country = new Cubix_Geography_Countries();
			$home_country = $m_country->get($home_city->country_id);

			$this->view->home_city_country = $home_city->{'title_' . $this->_request->lang_id} . ' (' . $home_country->{'title_' . $this->_request->lang_id} . ')';
		}

		$bt = $model->getBubbleText($escort->id);
		$this->view->bubble_text = $bt ? $bt->bubble_text : null;

		/* Last Review */
		$cache = Zend_Registry::get('cache');
		$cache_key = 'reviews_' . $escort->id;
		if ( ! $res = $cache->load($cache_key) ) {
			try {
				$res = $model->getEscortLastReview($escort->id);
				
				//$res = Cubix_Api::getInstance()->call('getEscortLastReview', array($escort->id));
			}
			catch ( Exception $e ) {
				$res = array(false, false);
			}

			$cache->save($res, $cache_key, array());
		}

		list($review, $rev_count) = $res;
		$this->view->review = $review;
		$this->view->rev_count = $rev_count;
		/* Last Review */

		$this->photosAction($escort);
		
		if ( $escort->agency_id )
			$this->agencyEscortsAction($escort->agency_id, $escort->id);

		$user = Model_Users::getCurrent();
		$this->view->user = $user;

		//$this->view->has_no_review = Model_Reviews::hasProduct($escort->id, Model_EscortsV2::PRODUCT_NO_REVIEWS);

		$paging_key = $this->_getParam('from');
		$paging_keys_map = array(
			'last_viewed_escorts',
			'search_list',
			'regular_list',
			'new_list',
			'profile_disabled_list',
			'premiums',
			'right_premiums',
			'main_premium_spot',
			'tours_list',
			'tour_main_premium_spot',
			'up_tours_list',
			'up_tour_main_premium_spot'
		);

		if ( in_array($paging_key, $paging_keys_map) ) {
			$sid = 'sedcard_paging_' . Cubix_Application::getId() . '_' . $city->region_slug;
			$ses = new Zend_Session_Namespace($sid);
		//var_dump($paging_key);
			$ses_pages = $ses->{$paging_key};
			//var_dump($ses_pages);
			$ses_index = 0;
			if ( is_array($ses_pages) ) {
				$ses_index = array_search($showname, $ses_pages);
			}

			if ( isset($ses_pages[$ses_index - 1]) ) {
				$back_showname = $this->view->back_showname = $ses_pages[$ses_index - 1];
				$this->view->back_id = $model->getIdByShowname($back_showname, $city->region_slug, $escort->city_slug)->id;
			}

			if ( isset($ses_pages[$ses_index + 1]) ) {
				$next_showname = $this->view->next_showname = $ses_pages[$ses_index + 1];
				$this->view->next_id = $model->getIdByShowname($next_showname, $city->region_slug, $escort->city_slug)->id;
			}
			$this->view->paging_key = $paging_key;
		}

		/*list($prev, $next) = Model_Escort_List::getPrevNext($escort->showname);
		
		if ( ! is_null($prev) ) {
			$this->view->back_showname = $prev;
		}
		if ( ! is_null($next) ) {
			$this->view->next_showname = $next;
		}*/

		list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls();
		
		/* Cities list */
		$app = Cubix_Application::getById();
		
		if ($app->country_id)
		{
			$modelCities = new Model_Cities();
			
			$cities = $modelCities->getByCountry($app->country_id);
		}
		else
		{
			$modelCountries = new Model_Countries();
			$modelCities = new Model_Cities();
			
			$countries = $modelCountries->getCountries();
			$this->view->countries_list = $countries;
			
			$c = array();
			
			foreach ($countries as $country)
				$c[] = $country->id;
			
			$cities = $modelCities->getByCountries($c);
		}
		
		$this->view->cities_list = $cities;
		/* End Cities list */
	}
	
	public function profileV2Action()
	{
		if ( isset($_GET['v1']) ) {
			return $this->_forward('profile', 'escorts', 'default');
		}

		//<editor-fold defaultstate="collapsed" desc="Redirect to new URLS">
		$route_name = Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName();
		if ( $route_name == 'escort-profile-def-old' ) {
			$this->_redirect(self::$linkHelper->getLink('profile', array('showname' => $this->_getParam('escortName'), 'escort_id' => $this->_getParam('escort_id'))), array('code' => 301));
		}

		//////   redirect not active escorts to home page   ///////
		if ( ! $this->_getParam('prev') ) {			
			if ( strlen($this->_getParam('f')) && $this->_getParam('f') != 'b' ) {
				header('HTTP/1.1 301 Moved Permanently');
				header('Location: /');
				die;
			}
		}
		//////////////////////////////////////////////////////////
		//</editor-fold>
		$route_name = Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName();
		
		$this->view->layout()->setLayout('profile-v2');
		$this->_helper->viewRenderer->setScriptAction("profile-v2/profile");
		
		$this->view->is_profile = true;
		$is_preview = $this->_getParam('prev', false);
		if ( strlen($this->_getParam('f')) && $this->_getParam('f') == 'b' ) {
			$is_preview = true;
		}
		$this->view->is_preview = $is_preview;
		$this->view->show_advertise_block = $show_advertise_block = $this->_getParam('show_advertise_block', false);
		
		$showname = $this->_getParam('escortName');
		$escort_id = $this->_getParam('escort_id');
		
		$model = new Model_EscortsV2();
		
		$cache_key = 'v2_' . $showname . '_' . $escort_id .  '_profile_' . Cubix_I18n::getLang();
		$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

		$this->view->profile_cache_key = 'html_profile_' . $cache_key;
		
		$from_api = false;
		$escort = $model->get($escort_id, $cache_key, $is_preview, $from_api);
		$this->view->from_api = $from_api;
		
		$blockModel = new Model_BlockedCountries();
		if ( $blockModel->checkIp($escort_id) ){
			//die('r3');
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			die;
			return;
		}

		if ($escort->showname != $showname)
		{			
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			die;
		}

		$city = new Cubix_Geography_Cities();
		$city = $city->get($escort->base_city_id);
		$this->_request->setParam('region', $city->region_slug);
		$this->view->base_city = $city;
		
		if ( ! $escort || isset($escort['error']) )
		{			
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			die;
			return;
		}

		//		if ( isset($escort->escort_status) && $escort->escort_status != 32 ) {
		//			$this->_forward('profile-disabled');
		//			$this->_request->setParam('city_slug', $escort->city_slug);
		//		}
		
		$add_esc_data = $model->getRevComById($escort->id);
		$bl = $model->getBlockWebsite($escort->id);
		
		// if($bl->skype_call_status){
		// 	//temporary get from api 
		// 	$model_video_chat = new Model_VideoChatRequests();
		// 	$this->view->video_chat_prices = $model_video_chat->getData($escort->id);
		// }

		if($bl->cam_booking_status){
			//temporary get from api 
			$model_video_chat = new Model_VideoChatRequests();
			$this->view->video_chat_prices = $model_video_chat->getData($escort->id);
			
		}
		
		$escort->block_website = $bl->block_website;
		$escort->disabled_reviews = $add_esc_data->disabled_reviews;
		$escort->disabled_comments = $add_esc_data->disabled_comments;
		$escort->cam_status =  $add_esc_data->cam_status;
		$escort->cam_preview_url =  $add_esc_data->cam_preview_url;
		$escort->cam_channel_url =  $add_esc_data->cam_channel_url;
		$this->view->escort_id = $escort->id;
		$this->view->escort = $escort;
	
		$vconfig = Zend_Registry::get('videos_config');
		$host =  Cubix_Application::getById(Cubix_Application::getId())->host;
		$video = new Model_Video();
		$escort_video = $video->GetEscortVideo($escort->id, FALSE);
			
		if(!empty($escort_video[0]))
		{      
			$photo = new Cubix_ImagesCommonItem($escort_video[0][0]);
			$this->view->photo_video = $photo->getUrl('orig');
			$this->view->gallery_video =($escort_video[1][0]->height > $escort_video[1][0]->width) ? $photo->getUrl('pp') : $photo->getUrl('pl');
			//$this->view->vwidth = $photo['width'];
			//$this->view->vheight = $photo['height'];
			$this->view->vheight = $vheight =  $escort_video[1][0]->height >= 480 ? 480 : 360; 
			$this->view->vwidth = intval( $vheight * $photo['width'] / $photo['height']);	
			//$this->view->video = $vconfig['Media'].$host.'/'.$escort->id.'/'.$escort_video[1][0]->video;
			$this->view->video_id = $escort_video[1][0]->id;
		}
		$apps = $model->getApps($escort->id);
		if($escort->skype_id){
			$apps->skype = $escort->skype_id;
		}
		$this->view->apps = $apps;
		
		if ( $escort->home_city_id ) {
			$m_city = new Cubix_Geography_Cities();
			$home_city = $m_city->get($escort->home_city_id);

			$m_country = new Cubix_Geography_Countries();
			$home_country = $m_country->get($home_city->country_id);

			$this->view->home_city_country = $home_city->{'title_' . $this->_request->lang_id} . ' (' . $home_country->{'title_' . $this->_request->lang_id} . ')';
		}

		$bt = $model->getBubbleText($escort->id);
		$this->view->bubble_text = $bt ? $bt->bubble_text : null;

		/* Last Review */
		$cache = Zend_Registry::get('cache');
		$cache_key = 'reviews_' . $escort->id;
		if ( ! $res = $cache->load($cache_key) ) {
			try {
				$res = $model->getEscortLastReview($escort->id);
				
				//$res = Cubix_Api::getInstance()->call('getEscortLastReview', array($escort->id));
			}
			catch ( Exception $e ) {
				$res = array(false, false);
			}
			
			$cache->save($res, $cache_key, array());
		}

		list($review, $rev_count) = $res;
		$this->view->review = $review;
		$this->view->rev_count = $rev_count;
		/* Last Review */

		$this->photosV2Action($escort);
		
		if ( $escort->agency_id )
			$this->agencyEscortsV2Action($escort->agency_id, $escort->id);

		$user = Model_Users::getCurrent();
		$this->view->user = $user;

		//$this->view->has_no_review = Model_Reviews::hasProduct($escort->id, Model_EscortsV2::PRODUCT_NO_REVIEWS);

		//<editor-fold defaultstate="collapsed" desc="Back Next buttons logic">
		//Modified by Vahag
		
		$paging_key = $this->_getParam('from');
		if (!strlen($paging_key)) $paging_key = 'regular_list';
		$paging_keys_map = array(
			'last_viewed_escorts',
			'search_list',
			'regular_list',
			'new_list',
			'profile_disabled_list',
			'premiums',
			'right_premiums',
			'main_premium_spot',
			'tours_list',
			'tour_main_premium_spot',
			'up_tours_list',
			'up_tour_main_premium_spot',
			'video_list'
		);

		
		if ( in_array($paging_key, $paging_keys_map) ) {
			$sid = 'sedcard_paging_' . Cubix_Application::getId() . '_' . $city->region_slug;
			$ses = new Zend_Session_Namespace($sid);

			
			$ses_pages = $ses->{$paging_key};
			$criterias = $ses->{$paging_key . '_criterias'};
			
			$ses_index = 0;
			if ( is_array($ses_pages) ) {
				$ses_index = array_search($escort_id, $ses_pages);
			}

			if ( isset($ses_pages[$ses_index - 1]) ) {
				$this->view->back_showname = $model->getShownameById($ses_pages[$ses_index - 1]);
				$this->view->back_id = $ses_pages[$ses_index - 1];
			} elseif ( $criterias['first_page'] > 1 ) { //Loading previous page escorts
				switch( $paging_key ) {
					case 'regular_list':
						$prev_page_escorts = Model_Escort_List::getFiltered($criterias['filter'], $criterias['sort'], $criterias['first_page'] - 1, 30);
						break;
				}
				
				$p_escorts_count = count($prev_page_escorts);
				if ( count($p_escorts_count) ) {
					$this->view->back_showname = $prev_page_escorts[$p_escorts_count - 1]->showname;
					$this->view->back_id = $prev_page_escorts[$p_escorts_count - 1]->id;
					//Adding previous page escorts to $ses_pages and set in session
					$ids = array();
					foreach($prev_page_escorts as $p_esc) {
						$ids[] = $p_esc->id;
					}

					$criterias['first_page'] -= 1;
					$ses->{$paging_key} = array_merge($ids, $ses_pages);
					$ses->{$paging_key . '_criterias'} = $criterias;
				}
			}

			if ( isset($ses_pages[$ses_index + 1]) ) {
				$this->view->next_showname = $model->getShownameById($ses_pages[$ses_index + 1]);
				$this->view->next_id = $ses_pages[$ses_index + 1];
			} else { //Loading next page escorts
				switch( $paging_key ) {
					case 'regular_list':
						$next_page_escorts = Model_Escort_List::getFiltered($criterias['filter'], $criterias['sort'], $criterias['last_page'] + 1, 30);
						break;
				}
				
				if ( count($next_page_escorts) ) {
					$this->view->next_showname = $next_page_escorts[0]->showname;
					$this->view->next_id = $next_page_escorts[0]->id;
					//Adding next page escorts to $ses_pages and set in session
					foreach($next_page_escorts as $p_esc) {
						$ses_pages[] = $p_esc->id;
					}

					$criterias['last_page'] += 1;
					$ses->{$paging_key} = $ses_pages;
					$ses->{$paging_key . '_criterias'} = $criterias;
				}
			}
			
			$this->view->paging_key = $paging_key;
		}
		//</editor-fold>

		list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls();
		
		/* Cities list */
		$app = Cubix_Application::getById();
		
		if ($app->country_id)
		{
			$modelCities = new Model_Cities();
			
			$cities = $modelCities->getByCountry($app->country_id);
		}
		else
		{
			$modelCountries = new Model_Countries();
			$modelCities = new Model_Cities();
			
			$countries = $modelCountries->getCountries();
			$this->view->countries_list = $countries;
			
			$c = array();
			
			foreach ($countries as $country)
				$c[] = $country->id;
			
			$cities = $modelCities->getByCountries($c);
		}
		
		$this->view->cities_list = $cities;
		/* End Cities list */
		
		$this->view->escort_user_id = $model->getUserId($escort->id);

		$client = new Cubix_Api_XmlRpc_Client();
		$escort_video_link = $client->call('Escorts.getEscortVideoLink', array('escort_id' => $escort->id));
		$this->view->escort_video_link = $escort_video_link;
	}
	
	public function photosV2Action($escort = null)
	{
		$config = Zend_Registry::get('escorts_config');
		
		$page = $this->_getParam('photo_page');

		$escort_id = $this->_getParam('escort_id');
		$mode = $this->_getParam('mode');
		
		if( $mode == 'ajax' ) {
			$this->view->layout()->disableLayout();
			$model = new Model_EscortsV2();
			
			$cache_key = 'v2_' . $escort_id .  '_profile_' . Cubix_I18n::getLang();
			$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
			
			$escort = $model->get($escort_id, $cache_key);
			$this->view->escort_id = $escort_id;
		}
		
		if ( ! $page )
			$page = 1;
		
		$count = 0;
		if ( $this->view->from_api ) {
			$photos = $escort->getPhotosApi($page, $count, true, true, true);
		}
		else {
			$photos = $escort->getPhotos($page, $count, true, false, null, null, false, true, true);
		}
		
		$this->user = Model_Users::getCurrent();
		$is_logged = (!$this->user) ? false : true;
		$show_pic =  (!$is_logged && $page > 2) ? false : true; 
		$show_load_more = (!$is_logged && $page > 1) ? false : true;
		
		$this->view->photos = $photos;
		$this->view->photos_count = $count;
		$this->view->photos_page = $page;
		$this->view->photos_perPage = $config['photos']['perPage'];
		$this->view->show_pic = $show_pic;
		$this->view->show_load_more = $show_load_more;
	}

	public function profileDisabledAction()
	{
		$model = new Model_EscortsV2();
		$count = 0;

		$this->view->no_layout_banners = true;
		$this->view->no_paging = true;
		$this->view->no_js = true;

		$params = array('e.gender = 1' => array(), 'ct.slug = ?' => array($this->_request->city_slug));
		$escorts = Model_Escort_List::getFiltered($params, 'random', 1, 30, $count);

		$this->view->count = $count;
		$this->view->escorts = $escorts;
	}
	
	public function agencyEscortsV2Action($agency_id = null, $escort_id = null)
	{
		$config = Zend_Registry::get('escorts_config');
		
		if ( ! $agency_id )
		{			
			$this->view->layout()->disableLayout();
			$agency_id = $this->view->agency_id = $this->_getParam('agency_id');
		}
		else
			$this->view->agency_id = $agency_id;
		
		if( ! $escort_id )
			$escort_id = $this->view->escort_id = $this->_getParam('escort_id');
		
		$page = intval($this->_getParam('agency_page'));
		if ( $page < 1 )
			$page = 1;

		$params = array('e.agency_id = ?' => $agency_id,'e.id <> ?' => $escort_id);

		$escorts_count = 0;
		$escorts = Model_Escort_List::getFiltered($params, 'random', $page, $config['widgetPerPageV2'], $escorts_count);

		if (count($escorts) ) {
			$this->view->a_escorts = $escorts;
			$this->view->a_escorts_count = $escorts_count;

			$this->view->agencies_page = $page;
			$this->view->widgetPerPage = $config['widgetPerPageV2'];

			$m_esc = new Model_EscortsV2();
			$agency = $m_esc->getAgency($escort_id);

			$this->view->agency = new Model_AgencyItem($agency);
		}
	}
	
	public function agencyEscortsAction($agency_id = null, $escort_id = null)
	{
		$config = Zend_Registry::get('escorts_config');
		
		if ( ! $agency_id )
		{
			$this->view->layout()->disableLayout();
			$agency_id = $this->view->agency_id = $this->_getParam('agency_id');
		}
		else
			$this->view->agency_id = $agency_id;
		
		if( ! $escort_id )
			$escort_id = $this->view->escort_id = $this->_getParam('escort_id');
		
		$page = intval($this->_getParam('agency_page'));
		if ( $page < 1 )
			$page = 1;

		$params = array('e.agency_id = ?' => $agency_id,'e.id <> ?' => $escort_id);

		$escorts_count = 0;
		$escorts = Model_Escort_List::getFiltered($params, 'random', $page, $config['widgetPerPage'], $escorts_count);

		if (count($escorts) ) {
			$this->view->a_escorts = $escorts;
			$this->view->a_escorts_count = $escorts_count;

			$this->view->agencies_page = $page;
			$this->view->widgetPerPage = $config['widgetPerPage'];

			$m_esc = new Model_EscortsV2();
			$agency = $m_esc->getAgency($escort_id);

			$this->view->agency = new Model_AgencyItem($agency);
		}
	}
	
	public function photosAction($escort = null)
	{
		$config = Zend_Registry::get('escorts_config');
		
		$page = $this->_getParam('photo_page');

		$escort_id = $this->_getParam('escort_id');
		$mode = $this->_getParam('mode');
		
		if( $mode == 'ajax' ) {
			$this->view->layout()->disableLayout();
			$model = new Model_EscortsV2();
			
			$cache_key = 'v2_' . $escort_id .  '_profile_' . Cubix_I18n::getLang();
			$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
			
			$escort = $model->get($escort_id, $cache_key);
			$this->view->escort_id = $escort_id;
		}
		
		if ( ! $page )
			$page = 1;
		
		$count = 0;
		if ( $this->view->is_preview || $this->_getParam('is_preview') ) {
			$photos = $escort->getPhotosApi($page, $count, false);
		}
		else {
			$photos = $escort->getPhotos($page, $count, false);
		}
		$this->view->photos = $photos;
		$this->view->photos_count = $count;
		$this->view->photos_page = $page;

		$this->view->photos_perPage = $config['photos']['perPage'];
	}
	
	public function privatePhotosAction($escort = null)
	{
		$config = Zend_Registry::get('escorts_config');
		
		$page = $this->_getParam('photo_page');
		$escort_id = $this->_getParam('escort_id');
		
		if( $escort_id )
		{
			$this->view->layout()->disableLayout();
			$model = new Model_EscortsV2();
			
			$cache_key = 'v2_' . $escort_id .  '_profile_' . Cubix_I18n::getLang();
			$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
			
			$escort = $model->get($escort_id, $cache_key);
			$this->view->escort_id = $escort_id;
		}
		
		if ( ! $page )
			$page = 1;
			
		$count = 0;
		$this->view->private_photos = $escort->getPhotos($page, $count, false, true);
		$this->view->private_photos_count = $count;
		$this->view->private_photos_page = $page;
		$this->view->private_photos_perPage = $config['photos']['perPage'];
	}
	
	public function viewedEscortsAction()
	{
		$this->view->layout()->disableLayout();
		$escort_id = intval($this->_getParam('escort_id'));

		if ( $escort_id )
		{
			self::_addToLatestViewedList($escort_id);
			$viewedEscorts = self::_getLatestViewedList(10, $escort_id);
		}
		else
			$viewedEscorts = self::_getLatestViewedList(10);

		if ( ! is_array($viewedEscorts) || count($viewedEscorts) == 0 )
			return false;
		
		
		
		$ve = array();
		
		$ids = array();
		
		foreach ( $viewedEscorts as $e ) {
			$ids[] = $e['id'];
		}
		
		$filter = array('e.id IN (' . implode(', ', $ids) . ')' => array());
		$order = 'FIELD(eic.escort_id, ' . implode(', ', $ids) . ')';
		
		$count = 0;
		$escorts = Model_Escort_List::getFiltered($filter, $order, 1, 10, $count);
		
		$this->view->escorts = $escorts;
	}

	public function lateNightGirlsAction()
	{
		$this->view->layout()->disableLayout();

		$page = intval($this->_request->l_n_g_page);

		if ($page < 1) $page = 1;
		
		list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls($page);

		$this->view->l_n_g_page = $page;
	}

	private function _getLateNightGirls($page = 1)
	{
		return array();
		return Model_EscortsV2::getLateNightGirls($page);
	}

	private function _addToLatestViewedList($escort_id)
	{
		$count = 11; // actually we need 10
		$escort = array('id' => $escort_id);
		$sid = 'last_viewed_escorts_' . Cubix_Application::getId();
		
		$ses = new Zend_Session_Namespace($sid);
		
		// Checking if session exists
		// creating if not exists		
		if ( false == (isset($ses->escorts) && is_array($ses->escorts)) )
		{
			$ses->escorts = array();
		}
		else 
		{
			// Checking if escort exists in stack
			// removing escort from stack if exists
			foreach ($ses->escorts as $key => $item)
			{
				if ( $escort['id'] == $item['id'] )
				{
					array_splice($ses->escorts, $key, 1);
				}
			}
		}
		
		// Pushing escort to the end of the stack
		array_unshift($ses->escorts, $escort);
		
		// Checking if
		if ( count($ses->escorts) > $count )
		{	
			array_pop($ses->escorts);
		}
		//var_dump($ses->escorts);
	}
	
	// Getting latest viewed escorts list (stack) 
	// as Array of Assoc
	public function _getLatestViewedList($max, $exclude = NULL)
	{
		$sid = 'last_viewed_escorts_' . Cubix_Application::getId();
		
		$ses = new Zend_Session_Namespace($sid);

		if ( ! is_array($ses->escorts) )
			return array();

		if ( $exclude )
		{
			$arr = $ses->escorts;
			foreach($arr as $key => $item)
			{
				if($exclude == $item['id'])
				{
					array_splice($arr, $key, 1);
				}	
			}
			$ret = array_slice($arr, 0, $max);
			
			return $arr;
		}

		$ret = array_slice($ses->escorts, 0, $max);
		
		return $ret;
	}
	
	public function searchAction()
	{
        $this->view->layout()->setLayout('main-simple');
		
		
		$this->view->quick_links = false;

		$this->view->defines = Zend_Registry::get('defines');
	
		if( $this->_getParam('search') )
		{
			$params['filter'] = array();
            $params['order'] = array();
			
			$this->view->params = array_map("htmlspecialchars", $this->_request->getParams());

            if( $this->_getParam('order') ) {

                $order_direction = '';
                $order_field = 'date_registered';

                switch($this->_getParam('order')){
                    case 'lastmodified':
                       $order_field = "date_last_modified";
                       break;
                    case 'popularity':
                       $order_field = "hit_count";
                       break;
                }

                if($this->_getParam('order_direction')){
                   $order_direction = $this->_getParam('order_direction');
                }
                $params['order'] = $order_field." ".$order_direction;
				
			}


            if($this->_getParam('page')){
                $params['limit']['page'] = $this->_getParam('page');
            }

            if( $this->_getParam('publish') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'DATEDIFF(NOW(),e.date_registered) <= ?' =>  trim($this->_getParam('days'))
				));
			}

			if( $this->_getParam('showname') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.showname LIKE ?' => "%" . trim($this->_getParam('showname')) . "%"
				));
			}
			
			if( $this->_getParam('agency_slug') && $this->_getParam('agency') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.agency_slug LIKE ?' => "%" . trim($this->_getParam('agency_slug')) . "%"
				));
			}
			
			if( $this->_getParam('gender') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.gender = ?' => $this->_getParam('gender')
				));
			}

			if( $this->_getParam('city') &&  $this->_getParam('city_slug') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'ct.slug = ?' => $this->_getParam('city_slug')
				));
			}
	
			if( $this->_getParam('available_for_incall') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.incall_type IS NOT NULL' => true
				));
			}
			if( $this->_getParam('available_for_outcall') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.outcall_type IS NOT NULL' => true
				));
			}
			
			if( $this->_getParam('nationality') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.nationality_id = ?' => $this->_getParam('nationality')
				));
			}
			
			if( $this->_getParam('saf') ) {
				$params['filter'] = array_merge($params['filter'], array(					
					'FIND_IN_SET( ?, e.sex_availability)' => $this->_getParam('saf')
				));
			}

            if( $this->_getParam('price_from') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.outcall_price >= ?' => $this->_getParam('price_from')
				));
			}

            if( $this->_getParam('price_to') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.outcall_price <= ?' => $this->_getParam('price_to')
				));
			}

			if( $this->_getParam('words') ) 
			{
				if ($this->_getParam('words_type') == 1)
				{
	            	if ($pos = strpos($this->_getParam('words'), ' ')) {
	            		$sub = substr($this->_getParam('words'), 0, $pos);
	            	}
	            	else {
	            		$sub = $this->_getParam('words');
	            	}
	            	
	            	$params['filter'] = array_merge($params['filter'], array(
						'e.' . Cubix_I18n::getTblField('about') . ' = ?' =>  $sub 
					));
	            }
	            elseif ($this->_getParam('words_type') == 2) 
	            {
	            	$params['filter'] = array_merge($params['filter'], array(
						'e.' . Cubix_I18n::getTblField('about') . ' LIKE ?' => "%" . $this->_getParam('words') . "%"
					));
	            }
			}
			
			/* ------------ >>>>>>>>>>>>>>>>>>>>>>>>>>>>>> <<<<<<<<<<<<<<<<<<<<<< --------------- */
            if( $this->_getParam('vr') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.verified_status IN ('.Model_Escorts::VERIFIED_STATUS_VERIFIED .','. Model_Escorts::VERIFIED_STATUS_VERIFIED_RESET.')' => array()
                ));
                $params['filter'] = array_merge($params['filter'], array(
                    ' NOT e.is_online ' => array()
                ));
            }

			if( $this->_getParam('age_from') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.age >= ?' => $this->_getParam('age_from')
				));
			}
			if( $this->_getParam('age_to') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.age <= ?' => $this->_getParam('age_to')
				));
			}

            if( $this->_getParam('ethnicity') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.ethnicity = ?' => $this->_getParam('ethnicity')
				));
			}

            if( $this->_getParam('hair_color') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.hair_color = ?' => $this->_getParam('hair_color')
				));
			}	

			if( $this->_getParam('eye_color') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.eye_color = ?' => $this->_getParam('eye_color')
				));
			}

            if( $this->_getParam('height_from') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.height >= ?' => $this->_getParam('height_from')
				));
			}
			if( $this->_getParam('height_to') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.height <= ?' => $this->_getParam('height_to')
				));
			}	
		
			if( $this->_getParam('dress_size') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.dress_size = ?' => $this->_getParam('dress_size')
				));
			}

            if( $this->_getParam('shoe_size') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.shoe_size = ?' => $this->_getParam('shoe_size')
				));
			}	
								
			if( $this->_getParam('cup_size') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.cup_size = ?' => $this->_getParam('cup_size')
				));
			}
            
            if( $this->_getParam('pubic_hair') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.pubic_hair = ?' => $this->_getParam('pubic_hair')
				));
			}

            if( $this->_getParam('smoker') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.is_smoking = ?' => $this->_getParam('smoker')
				));
			}

			if( $this->_getParam('drinking') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.is_drinking = ?' => $this->_getParam('drinking')
				));
			}			
			if( $this->_getParam('language') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.languages LIKE ?' => "%".$this->_getParam('language')."%"
				));
			}			
            /* Services */
            if( $this->_getParam('services') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'es.service_id = ?' => $this->_getParam('services')
				));
			}
            /* Services */
            /* Working Times */
            if( $this->_getParam('day_index') ) {
                $dateTimes = array();
                $counter = 0;
                foreach($this->_getParam('day_index') as $day => $value){
                    $timeFrom = $this->_getParam('time_from');
                    $timeFromM = $this->_getParam('time_from_m');
                    $timeTo = $this->_getParam('time_to');
                    $timeToM = $this->_getParam('time_to_m');
                    $dateTimes[$counter]['day_index'] = $day;
                    $dateTimes[$counter]['time_from'] = $timeFrom[$day];
                    $dateTimes[$counter]['time_from_m'] = $timeFromM[$day];
                    $dateTimes[$counter]['time_to'] = $timeTo[$day];
                    $dateTimes[$counter]['time_to_m'] = $timeToM[$day];
                    $counter++;
                }
                $params['filter'] = array_merge($params['filter'], array(
					'working_times = ?' => $dateTimes
				));
			}

            /* Working Times */

            $page = 1;
            $page_size = 30;
            if($this->_getParam('page')){
                $page = $this->_getParam('page');
            }
			
			$model = new Model_EscortsV2();
			$count = 0;
            
			$escorts = $model->getSearchAll($params, $count,$page,$page_size);
            $x['times'] = $dateTimes;

            $this->view->data = $x;
            $this->view->page = $page;
			$this->view->count = $escorts['count'];
			$this->view->escorts = $escorts['data'];
			$this->view->search_list = true;
		}
	}

	public function gotmAction()
	{
		$this->view->layout()->setLayout('main-simple');
		$show_history = (bool) $this->_getParam('history');

      

		$page = intval($this->_getParam('page', 1));
		if ( $page < 1 ) $page = 1;
		$this->view->per_page = $per_page = 40;
		$this->view->page = $page;

		$result = Model_EscortsV2::getMonthGirls($page, $per_page, $show_history);



		$this->view->escorts = $escorts = $result['escorts'];
		$this->view->count = $result['count'];

		if ( ! $show_history ) {
			$this->view->gotm = Model_EscortsV2::getCurrentGOTM();
		}
		else {
			$this->_helper->viewRenderer->setScriptAction('gotm-history');
		}
	}

	/**
	 * Voting Widget Action
	 */
	public function voteAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$response = array('status' => null, 'desc' => '', 'html' => '');

		$showname = $this->_getParam('showname');
		$escort_id = $this->_getParam('escort_id');
		$model = new Model_EscortsV2();
		//$escort = $model->get($showname);
		
		$cache_key = 'v2_' . $escort_id .  '_profile_' . Cubix_I18n::getLang();
		$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
		
		$escort = $model->get($escort_id, $cache_key);

		$user = Model_Users::getCurrent();

		if ( ! $user ) {
			$response['status'] = 'error';
			$response['desc'] = 'not signed in';
		}
		elseif ( 'member' != $user->user_type ) {
			$response['status'] = 'error';
			$response['desc'] = 'only members are allowed to vote';
		}
		elseif ( ! $escort ) {
			$response['status'] = 'error';
			$response['desc'] = 'invalid escort showname';
		}
		else {
			try {
				$result = $escort->vote($user->id);

				if ( $result !== true ) {
					$response['status'] = 'error';
					$response['desc'] = 'member has already voted';
				}
				else {
					$response['status'] = 'success';
					$response['html'] = $this->view->votingWidget($escort, true);
				}
			}
			catch ( Exception $e ) {
				$response['status'] = 'error';
				$response['desc'] = 'unexpected error';
			}
		}

		echo json_encode($response);
	}
	
	public function ajaxAlertsAction()
	{
		$this->view->layout()->disableLayout();
		
		$user = Model_Users::getCurrent();
		$escort_id = intval($this->_getParam('escort_id'));
		
		if ($user->user_type != 'member' || $escort_id == 0)
		{
			echo json_encode (array());
			die;
		}
		
		$model = new Model_Members();
		
		$events = $model->getEscortAlerts($user->id, $escort_id);
		
		$arr = array();
		$cities = array();
		$cities_str = '';
			
		if ($events)
		{
			foreach ($events as $event)
			{
				$arr[] = $event['event'];
				
				if ($event['event'] == ALERT_ME_CITY)
					$cities_str = $event['extra'];
			}
		}
		
		if (strlen($cities_str) > 0)
		{
			$modelCities = new Model_Cities();
			
			$cts = $modelCities->getByIds($cities_str);
			
			foreach ($cts as $c)
			{
				$cc = array('id' => $c->id, 'title' => $c->title);
				$cities[] = $cc;
			}
		}

		echo json_encode(array('events' => $arr, 'cities' => $cities));
		die;
		
	}
	
	public function ajaxAlertsSaveAction()
	{
		$this->view->layout()->disableLayout();
		
		$user = Model_Users::getCurrent();
		$escort_id = intval($this->_getParam('escort_id'));
		$events = $this->_getParam('events');
		$cities = $this->_getParam('cities');
		
		if ($user->user_type != 'member' || $escort_id == 0)
		{
			echo 'error';
			die;
		}
		
		$model = new Model_Members();
		$c = $model->memberAlertsSave($user->id, $escort_id, $events, $cities);
		
		echo json_encode(array('status' => 'success', 'count' => $c));
		die;
	}
	
	public function ajaxTellFriendAction()
	{
		$this->view->layout()->disableLayout();
		$this->view->full_url = $url = $_SERVER['HTTP_REFERER'];
		if($this->_request->isPost()){

			// Get data from request
			$data = new Cubix_Form_Data($this->getRequest());

			$fields = array(
				'name' => 'notags|special',
				'email' => '',
				'friend_name' => 'notags|special',
				'friend_email' => '',
				'message' => 'notags|special',

			);
			$data->setFields($fields);
			$data = $data->getData();

			$validator = new Cubix_Validator();

            if ( ! strlen($data['name']) ) {
				$validator->setError('name', 'Your Name is required');
			}

			if ( ! strlen($data['email']) ) {
				$validator->setError('email', 'Your Email is required');
			}
			elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
				$validator->setError('email', 'Wrong email format');
			}
			if ( ! strlen($data['friend_name']) ) {
				$validator->setError('friend_name', 'Friend Name is required');
			}

			if ( ! strlen($data['friend_email']) ) {
				$validator->setError('friend_email', 'Friend Email is required');
			}
			elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['friend_email']) ) {
				$validator->setError('friend_email', 'Wrong email format');
			}

			if ( ! strlen($data['captcha']) ) {
				$validator->setError('captcha', '');//Captcha is required
				}
			else {
				$session = new Zend_Session_Namespace('captcha');
				$orig_captcha = $session->captcha;

				if ( strtolower($data['captcha']) != $orig_captcha ) {
					$validator->setError('captcha', '');//Captcha is invalid
				}
			}

			$result = $validator->getStatus();

			if ( $validator->isValid() ) {

				// Set the template parameters and send it to support
				$email_tpl = 'send_to_friend';
				$tpl_data = array(
					'name' => $data['name'],
					'email' => $data['email'],
					'friend_name' => $data['friend_name'],
					'friend_email' => $data['friend_email'],
					'message' => $data['message'],
					'application_id' => Cubix_Application::getId(),
					'url' => $url
				);
				$data['application_id'] = Cubix_Application::getId();

				Cubix_Email::sendTemplate($email_tpl, $data['friend_email'], $tpl_data, $data['email']);
				
				$config = Zend_Registry::get('newsman_config');
				
				$list_id = $config['list_id'];
				$segment = $config['member'];
				$nm = new Cubix_Newsman($_SERVER["REMOTE_ADDR"]);
				
				$subscriber_id = $nm->subscribe($list_id, $data['email'],$data['name']);
				$is_added = $nm->bindToSegment($subscriber_id, $segment);
				$subscriber_id_friend = $nm->subscribe($list_id, $data['friend_email'],$data['friend_name']);
				$is_added = $nm->bindToSegment($subscriber_id_friend, $segment);
				
				die(json_encode($result));
			}	// Otherwise, render the phtml and show errors in it
			else {
					die(json_encode($result));

			}
		}
	}

	public function ajaxReportProblemAction()
	{
		$this->view->layout()->disableLayout();
		$url = $_SERVER['HTTP_REFERER'];
		if($this->_request->isPost()){
			// Fetch administrative emails from config
			$config = Zend_Registry::get('feedback_config');
			$support_email = $config['emails']['support'];
			$email_tpl = 'report_problem';
			// Get data from request
			$data = new Cubix_Form_Data($this->getRequest());

			$fields = array(
				'escort_id' => 'int',
				'name' => 'notags|special',
				'email' => '',
				'message' => 'notags|special',

			);
			$data->setFields($fields);
			$data = $data->getData();

			$validator = new Cubix_Validator();

            if ( ! strlen($data['name']) ) {
				$validator->setError('name', 'Your Name is required');
			}

			if ( ! strlen($data['email']) ) {
				$validator->setError('email', 'Your Email is required');
			}
			elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
				$validator->setError('email', 'Wrong email format');
			}
			if ( ! strlen($data['message']) ) {
				$validator->setError('message','Please type the report you want to send');
			}

            $result = $validator->getStatus();

			if ( $validator->isValid() ) {

				// Set the template parameters and send it to support
				$tpl_data = array(
					'name' => $data['name'],
					'email' => $data['email'],
					'message' => $data['message'],
					'application_id' => Cubix_Application::getId(),
					'url' => $url
				);
				$data['application_id'] = Cubix_Application::getId();

				try{
					Cubix_Email::sendTemplate($email_tpl, $support_email, $tpl_data, null);
					
					$user_id = null;
					$user = Model_Users::getCurrent();
										
					if ($user && $user->user_type == 'member')
						$user_id = $user->id;
					
					$client = new Cubix_Api_XmlRpc_Client();
					$client->call('Users.addProblemReport', array($data['name'], $data['email'], null, $data['message'], $data['escort_id'], $user_id));
					
					$config = Zend_Registry::get('newsman_config');
					$list_id = $config['list_id'];
					$segment = $config['member'];
					$nm = new Cubix_Newsman($_SERVER["REMOTE_ADDR"]);
					$subscriber_id = $nm->subscribe($list_id, $data['email'],$data['name']);
					$is_added = $nm->bindToSegment($subscriber_id, $segment);
				}catch ( Exception $e ) {
						die($e->getMessage());
				}
				die(json_encode($result));
			}	// Otherwise, render the phtml and show errors in it
			else {
					die(json_encode($result));

			}

         }
	}
	
	public function getFilterAction()
	{
		$params = $this->_getParam('json');
		$this->view->layout()->disableLayout();
		$menus = array('filter' => NULL, 'sort' => NULL, 'filter_city' => NULL);

		$defs = Zend_Registry::get('definitions');

		$filterStructure = $defs['escorts_filter'];

		// covid-10 
		//unset($filterStructure[10]);
		//unset($filterStructure[1]);
		$menus['filter'] = new Cubix_NestedMenu(array('childs' => $filterStructure));
		$menus['filter']->setId('filter-options');
		$menus['filter']->setSelected($menus['filter']->getByValue(NULL));
		
		$escorts_sort = $defs['escorts_sort'];
		$escorts_sort[] = array(
			'title' => $this->view->t('sort_city'),
			'value' => 'city-asc'
		);

		$current_sorting = "random";
		if ( isset($_COOKIE['sorting']) ) {
			$current_sorting = $_COOKIE['sorting'];
		}
		
		//$menus['sort'] = new Cubix_NestedMenu(array('childs' => $defs['escorts_sort']));
		$menus['sort'] = new Cubix_NestedMenu(array('childs' => $escorts_sort));
		$menus['sort']->setId('sort-options');
		$menus['sort']->setSelected($menus['sort']->getByValue($current_sorting));

		$filter_city = $defs['escorts_cities_filter'];
		
		//removing empty cities from filter
		$cities_by_escorts_count = Model_Cities::getEscortsCountPerCity();
		foreach ( $filter_city as $key => $value ) {
			if( $value['value']=='all' )continue;
			foreach($cities_by_escorts_count as $city){
				if($city->id == $value['city_id']){
					
					if($city->escorts_count < 1){
						
						unset($filter_city[$key]);
					}
				}
			}
		}

		$menus['filter_city'] = new Cubix_NestedMenu(array('childs' => $filter_city));
		$menus['filter_city']->setId('filter-city-options');
		$menus['filter_city']->setSelected($menus['filter_city']->getByValue('all'));

		$this->view->menus = $menus;
		$this->view->params = $params;
	}
	
	public function videosAction()
	{
		if($this->_request->isXmlHttpRequest()){
			$this->view->layout()->disableLayout();
			$this->_helper->viewRenderer->setScriptAction('list');
		}
		else{
			$this->view->layout()->setLayout('main');
		}
		$video_model = new Model_Video();
		$city_id = $this->_getParam('city_id') ? intval($this->_getParam('city_id')) : null ;

		$page = $this->_getParam('page') ? intval($this->_getParam('page')) : 1;
		
		$params = array(
			'city' => $this->_getParam('city'),
			'city_id' => $city_id
		);

		
		$filter = array();
		if($city_id){
			$filter['city_id'] = $city_id;
		}

		//https://sceonteam.atlassian.net/browse/A6-205
		$random_votd = $video_model->getRandomVotd();

		//injecting votd and changing paginaiton
		if( $random_votd && $random_votd->id !=  $city_id){
			$per_page = 24;
			$exclude[] = $random_votd->video_id;
		}else{
			$per_page = 24;
			$exclude = [];
		}

		

		$videos = $video_model->getVideos($filter, $page, $per_page, $exclude);


		$city_votd_found = false;

		foreach ( $videos['data'] as $video) {
			if($video->is_votd == 1){
				$city_votd_found = true;
			}
		}

		if( !$city_votd_found ){
			
			if($random_votd){
				$image = new Cubix_ImagesCommonItem($random_votd);
				$random_votd->photo = $image->getUrl('video_thumb_m');
				array_unshift($videos['data'], $random_votd);
			}
		}

        $escorts = array();
        $model_escort = new Model_Escorts();
        $cache = Zend_Registry::get('cache');

        $escorts_cache_key = 'escorts_list_' . Cubix_Application::getId() . '_lang_' . Cubix_I18n::getLang() . '_city_' . ($city_id ? $city_id : 'all') . '_page_' . $page;

        if ( ! $escorts = $cache->load($escorts_cache_key) ) {
            foreach ($videos['data'] as $key => $video)
            {
                $escorts[$key] = $model_escort->getById($video->escort_id);
                $escorts[$key]['has_video'] = $model_escort->checkHasVideo($video->escort_id);
                $escorts[$key]['is_votd'] = $video->is_votd;
                $escorts[$key]['city_id'] = $video->city_id;
                $escorts[$key]['age'] = $video->age;
            }

            $cache->save($escorts, $escorts_cache_key, array());

        }

        if(!$city_votd_found && $random_votd){
            $escorts[0] = $model_escort->getById($random_votd->escort_id);
            $escorts[0]['has_video'] = $model_escort->checkHasVideo($random_votd->escort_id);
            $escorts[0]['is_votd'] = $random_votd->is_votd;
            $escorts[0]['city_id'] = $random_votd->city_id;
            $escorts[0]['age'] = $random_votd->age;
        }

		$sess_name = 'video_list';
		if ($sess_name) {

			$sid = 'sedcard_paging_' . Cubix_Application::getId() . '_' . $this->_request->region;

			$sess = new Zend_Session_Namespace($sid);

			$sess->{$sess_name} = array();
			foreach ( $videos['data'] as $video ) {
				$sess->{$sess_name}[] = $video->escort_id;
			}
			
		}
		
		$this->view->static_page_key = 'videos';
		$this->view->videos = $videos['data'];
		$this->view->escorts = $escorts;
		$this->view->count = $videos['count'];
		$this->view->perPage = $per_page;
		$this->view->page = $page;
		$this->view->params = $params;
		
		
	}

	public function videoVoteAction()
	{
		if (!$this->_request->isPost()) die;

		$this->view->layout()->disableLayout();

		$video_id = $this->_request->video_id;
		$fingerprint = $this->_request->fingerprint;

		$model_video = new Model_Video();
		$client = new Cubix_Api_XmlRpc_Client();

		$pre_video_id = $model_video->voteForVideo($video_id, $fingerprint);

		$result = $client->call('Escorts.voteForVideo', array($video_id, $pre_video_id));
		if ($result) {
			die(json_encode(array('status' => 'success')));
		} else {
			die(json_encode(array('status' => 'error')));
		}
	}

	public function videoVoteCountAction()
	{
		$this->view->layout()->disableLayout();

		$fingerprints = Model_Video::getVideoVoteCount($this->_request->video_id);
		$fp_arr = (array) $fingerprints;
		$fps = array_map(function($fp) {
			return $fp->fingerprint;
		}, $fp_arr);

		$vote_count = count($fps);

		if (isset($vote_count)) {
			die(json_encode(array('status' => 'success', 'length' => $vote_count, 'fingerprints' => $fps)));
		} else {
			die(json_encode(array('status' => 'error')));
		}
	}
	
}