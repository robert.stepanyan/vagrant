<?php

class ClassifiedAdsController extends Zend_Controller_Action
{
	/**
	 * @var Zend_Session_Namespace
	 */
	public static $linkHelper;
	protected $_session;
	protected $_perPages = array(10, 25, 50, 100);
	const CURRENCY = 'CHF';

	public function init()
	{
		$_model_payments = new Model_PaymentMethodes();
		self::$linkHelper = $this->view->getHelper('GetLink');
		$this->view->layout()->setLayout('classified-ads');
		$this->_session = new Zend_Session_Namespace('classified-ads');
		$this->model = new Model_ClassifiedAds();
		$this->client = new Cubix_Api_XmlRpc_Client();
		$this->view->per_pages = $this->_perPages;
        $this->view->activ_payments =  $_model_payments->getActivePaymentMethodes(Cubix_Application::getId());
      
	}
	
	public function indexAction()
	{
		$defines = Zend_Registry::get('defines');
		$session_filter = array();
		$active_categories = $this->model->getAllCategoriesForFilter();
		foreach ($defines['classified_ad_categories'] as $key => $value) {
			if(!in_array($key, $active_categories)){
				unset($defines['classified_ad_categories'][$key]);
			}
		}
		$this->view->defines = $defines;

        $this->view->cities = $this->model->getAllCitiesForFilter(Cubix_Application::getById()->country_id);

		$this->view->perPage = $perPage = $this->_getParam('pp', 25);
		
		if ( ! in_array($perPage, $this->_perPages) ) {
			$this->view->perPage = $perPage = 25;
		}
		$this->view->page = $page = 1;

		//get Classifield adds list which is not empty EFNET 1038
        $filter = array();

        if (!empty($this->_request->category)){
            $filter['category']  = $this->_request->category;
        }

        $this->view->ads = $ads = $this->model->getList($filter, $page, $perPage, $count);

		$session_ads = array();
		foreach ($ads as  $ad) {
			$session_ads[] = $ad->id;
		}
		$this->_session->ads = $session_ads;
		$this->_session->filter = $session_filter;

		$this->view->count = $count;
		
	}
	
	public function adAction()
	{
		$next_ad = false;
		$prev_ad = false;

		$id = (int)$this->_request->id;
		$ads = ($this->_session->ads && count($this->_session->ads)) ? $this->_session->ads : array();
		if(empty($ads)) {
			$prev = $this->model->getPrev($id, $this->_session->filter);
			$next = $this->model->getNext($id, $this->_session->filter);
			$prev_ad = $prev ? $prev->id : false;
			$next_ad = $next ? $next->id : false;
		} else {
			$current_index = array_search($id, $ads);
			end($ads);
			$last_index = key($ads);
			$first_index = 0;
			reset($ads);
			if($current_index !== false){
				if($current_index !== $first_index){
					$prev_ad = $ads[$current_index - 1];
				}  

				if($current_index !== $last_index){
					$next_ad = $ads[$current_index + 1];
				} 

				if($current_index === $last_index){
					$next = $this->model->getNext($ads[$current_index], $this->_session->filter);
					$next_ad = $next ? $next->id : false;
					if($next_ad){
						array_push($ads, $next_ad);
					}
					$this->_session->ads = $ads;
				}
			}
		}

		$this->view->ad = $this->model->get($id);
		$this->view->next_ad = $next_ad;
		$this->view->prev_ad = $prev_ad;
		$defines = $this->view->defines = Zend_Registry::get('defines');
		if ( ! isset($this->view->ad->id) ) {
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: ' . $this->view->getLink('classified-ads-index'));
			die;
		}
	}


	public function printAction()
	{
		$this->view->layout()->disableLayout();
		
		$ad_id = (int) $this->_request->id;
		
		$this->view->ad = $this->model->get($ad_id);
	}
	
	public function ajaxFilterAction()
	{
		$defines = Zend_Registry::get('defines');
		$active_categories = $this->model->getAllCategoriesForFilter();
		foreach ($defines['classified_ad_categories'] as $key => $value) {
			if(!in_array($key, $active_categories)){
				unset($defines['classified_ad_categories'][$key]);
			}
		}
		$this->view->defines = $defines;
		
		$this->view->cities = $this->model->getAllCitiesForFilter(Cubix_Application::getById()->country_id);
		
		$this->view->layout()->disableLayout();
		$req = $this->_request;
        //get Classifield adds list which is not empty EFNET 1038
        // $this->view->defines = $this->listClassifieldAdds($defines['classified_ad_categories']);
		$this->view->category = $category = (int)$req->category;
		$this->view->city = $city = (int)$req->city;
		$this->view->text = $text = $req->text;
	}
	
	public function ajaxListAction()
	{
		$defines = $this->view->defines = Zend_Registry::get('defines');
		$this->view->cities = $this->model->getAllCitiesForFilter(Cubix_Application::getById()->country_id);
		$session_filter = array();
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$this->view->perPage = $perPage = $this->_getParam('pp', 25);
		
		if ( ! in_array($perPage, $this->_perPages) ) {
			$this->view->perPage = $perPage = 25;
		}
		
		$page = $req->page;
		if ( $page < 1 ) {
			$page = 1;
		}
		$this->view->page = $page;
		
		$filter = array();
		
		$category = $req->category;
		$city = (int)$req->city;
		$text = $req->text;
		
		if ( $category ) {
			$filter['category'] = $category;
			$session_filter['category'] = $category;
		}
		
		if ( $city ) {
			$filter['city'] = $city;
			$session_filter['city'] = $city;
		}
		
		if ( strlen($text) ) {
			$filter['text'] = $text;
			$session_filter['text'] = $text;
		}
		
		$this->view->ads = $ads = $this->model->getList($filter, $page, $perPage, $count);
		$session_ads = array();
		foreach ($ads as  $ad) {
			$session_ads[] = $ad->id;
		}
		$this->_session->ads = $session_ads;
		$this->_session->filter = $session_filter;
		$this->view->count = $count;

	}
	

	public function placeAdAction()
	{
		$req = $this->_request;
		$m_cities = new Model_Cantons();
		$this->view->cities = $m_cities->getAll();
		$m_countries = new Model_Countries();
		$this->view->phone_prefixes = $phone_prefixes = $m_countries->getPhonePrefixs();
		$this->view->defines = $defines =  Zend_Registry::get('defines');
		
		
		$errors = array();
		$step = $req->getParam('step', 1);



		$blackListModel = new Model_BlacklistedWords();
		$steps = array(1, 2, 3);
		$plans = array(1, 2, 'free');
		$payment_methods = array('epg', 'postfinance', 'twispay', 'phone', 'faxin');
		$edit_mode = false;
		
		$edit = (int) $req->getParam('edit');
		$payment_method = $req->getParam('payment_method');
		$data = count($this->_session->data) ? $this->_session->data : array();
		$photos = array();
		$orig_photos = array();
		if ( count($this->_session->photos) ) {
			$photos = $this->_session->photos;
			$orig_photos = $this->_session->photos;
		}

		$ip = Cubix_Geoip::getIP();

		if ( !in_array($step, $steps) ) {
			$step = 1;
		}
		
		if ( isset($this->_session->data) && count($this->_session->data) && $edit ) {
			$edit_mode = true;
		}
		
		if ( $step == 1 && ! $edit) {
			
			$this->_session->unsetAll();
			$data = array();
		}
		
		if($step == 1 && $edit_mode) {
			$data = $this->_session->data;
		} 

		if ( ! isset($this->_session->data) && ! count($this->_session->data) && ($step == 2 || $step == 3) ) {
			$step = 1;
		}


		if ( $req->isPost() ){
			$category = (int) $req->category;
			$this->view->accept_photo_clauses = $accept_photo_clauses = (int) $req->accept_photo_clauses;
			$cities =  $req->cities;
			$phone = $req->phone;
			$sms_available = $req->sms_available ? 1 : 0;
			$whatsapp_available = $req->whatsapp_available ? 1 : 0;
			$email = $req->email;
			$web = $req->web;
			$plan = $req->plan;
			$payment_method = $req->payment_method;
			$duration = 7;
			$title = trim($req->title);
			$text = $req->text;

			if ( ! $accept_photo_clauses ) {
				$errors['accept_photo_clauses'] = Cubix_I18n::translate('sys_error_required');
			}

			if ( ! $category ) {
				$errors['category'] = Cubix_I18n::translate('sys_error_required');
			}

			if ( ! count($cities) ) {
				$errors['cities'] = Cubix_I18n::translate('sys_error_required');
			}

			if ( ! strlen($title) ) {
				$errors['title'] = Cubix_I18n::translate('sys_error_required');
			}

			$_text = strip_tags(trim($text));
			$_text = str_replace('&nbsp;', '', $_text);
			$_text = preg_replace('/\s+/', '', $_text);
			
			if ( ! strlen($text) ) {
				$errors['text'] = Cubix_I18n::translate('sys_error_required');
			}

			if ( !$plan || !in_array($plan, $plans)) {
				$errors['plan'] = Cubix_I18n::translate('sys_error_required');
			}

			if ( ($plan == 1 || $plan == 2)  && ( !$payment_method || !in_array($payment_method, $payment_methods)) ) {
				$errors['payment_method'] = Cubix_I18n::translate('sys_error_required');
			}

			else if ($bl_words = $blackListModel->checkWords($text, Model_BlacklistedWords::BL_TYPE_CLASSIFIED_ADS)){
				foreach($bl_words as $bl_word){
					if($bl_word['type'] == Model_BlacklistedWords::BL_SEARCH_TYPE_EXACT){
						$pattern = '/(\W+|^)'. preg_quote($bl_word['bl_word'], '/') . '(\W+|$)/i';
						$text = preg_replace($pattern, '\1<abbr title="Blacklisted word" class = "black-listed" >' . $bl_word['bl_word'] . '</abbr>\2', $text);
					}
					else{
						$pattern = '/' . preg_quote($bl_word['bl_word'], '/') . '/i';
						$text = preg_replace($pattern, '<abbr title="Blacklisted word" class = "black-listed" >' . $bl_word['bl_word'] . '</abbr>', $text);
					}
				}
				$errors['text'] = 'You can`t use word "'.$blackListModel->getWords().'"';
				
			}
			if ( ! strlen($phone) ) {
				$errors['phone'] = Cubix_I18n::translate('sys_error_required');
			} else{
				$contact_phone = $phone;
				$phone_prefix_id = $req->phone_prefix_id;
				$phone_prefix = $m_countries->getPhonePrefixById($phone_prefix_id);
				$ndd_prefix = $m_countries->getPhoneNddPrefixById($phone_prefix_id);
				$phone_parsed = null;
				if ($contact_phone ) {
					if ( ! preg_match("/^[0-9\s\+\-\(\)]+$/i", $contact_phone) ) {
						$errors['phone'] = Cubix_I18n::translate('invalid_phone_number');
					}
					elseif(preg_match("/^(\+|00)/", $contact_phone) ) {
						$errors['phone'] = Cubix_I18n::translate('enter_phone_without_prefix');
					}

				}
				$phone_parsed = preg_replace('/[^0-9]+/', '', $contact_phone);
				$phone_parsed = preg_replace('/^'.$ndd_prefix.'/', '', $phone_parsed);
				$phone_parsed = '+'.$phone_prefix.$phone_parsed;
			} 
				
			
			if ( $this->model->checkAdByIp($ip) >= 2) {
				$errors['top'] = Cubix_I18n::translate('sys_error_ads_per_ip_per_day');
			}
							
			
			if ( strlen($email) ) {
				$valid = new Cubix_Validator();
				
				if ( ! $valid->isValidEmail($email) ) {
					$errors['email'] = Cubix_I18n::translate('invalid_email');
				}				
			}
			if ( strlen($web) ) {
				$valid = new Cubix_Validator();
				
				if ( ! preg_match("/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i", $web)  && ! preg_match("/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i", 'http://' . $web)) {
					$errors['invalid_web'] = Cubix_I18n::translate('sys_error_invalid_url');
				}				
			}
			$data = array(
				'category' => $category,
				'cities' => $cities,
				'phone' => $phone,
				'phone_parsed' => $phone_parsed,
				'phone_prefix_id' => $phone_prefix_id,
				'email' => $email,
				'web' => $web,
				'duration' => $duration,
				'title' => $title,
				'text' => $text,
				'plan' => $plan,
				'payment_method' => $payment_method,
				'sms_available' => $sms_available,
				'whatsapp_available' => $whatsapp_available,
				'ip' => $ip,
			);
			$user = Model_Users::getCurrent();
			$data['user_id'] = null;
			if ( $user ) {
				$data['user_id'] = $user->id;
			}

			if ( ! count($errors) ) {
				$data['ok'] = true;
				$this->_session->data = $data;
				$this->_session->orig_photos = $orig_photos;
				$step = 2;
				// $step = 3;
			}
			$this->_session->photos = $photos;
		}
		if($step == 3) {
			//FINISH Stage
			$packages = $defines['classified_ad_packages_arr'];
			
			if ( !$this->_session->data['ok'] ) {
				$this->_redirect($this->view->getLink('classified-ads-place-ad'));
			}
			$final_data = $this->_session->data;
			$photos = $this->_session->photos;
			$orig_photos = $this->_session->orig_photos;
			$plan = $final_data['plan'];
			$payment_method = $final_data['payment_method'];

			unset($final_data['plan']);
			unset($final_data['payment_method']);
			unset($final_data['ok']);
			$m_ads = new Model_ClassifiedAds();
			
			$save_data = array('data' => $final_data, 'photos' => $photos);
			$ad_id = $m_ads->save($save_data);
			if ( $ad_id ) {
				$this->_session->unsetAll();
			
				if ( $plan == 'free' ) {
					$this->_redirect($this->view->getLink('classified-ads-success') . '?id=' . $ad_id);
				} else {
					if($payment_method == 'epg'){
						$package = $this->model->getPackageById($plan);
						
						$this->view->amount = number_format($package['price'], 2);
						$this->view->currency = self::CURRENCY;
						$this->view->reference = 'CA-'. $ad_id. '_' . $package['id'];
							
						$epg_payment = new Model_EpgGateway();
						$success_url = 'http://www.' . Cubix_Application::getById()->host . $this->view->getLink('classified-ads-success') . '?id='. $ad_id;
						$token = $epg_payment->getTokenForAuth($this->view->reference, $this->view->amount, $success_url);
						$this->client->call('OnlineBilling.storeToken', array($token, null));
						
						$this->view->redirect_url = $epg_payment->getPaymentGatewayUrlV2($token);
						$this->view->use_epg = true;
						$step = 1;
					} elseif($payment_method == 'postfinance'){
						$package = $this->model->getPackageById($plan);
						$hash  = base_convert(time(), 10, 36);
						$postfinance_payment = new Model_Postfinance();
						
						$pf_data = array(
							'PSPID' => $postfinance_payment->PSPID,
							'ORDERID' =>  'CA-'. $ad_id. '-' . $package['id'],
							'AMOUNT' =>  $package['price'] * 100,
							'LANGUAGE' => 'de_DE',
							'CURRENCY' => $postfinance_payment->currency,
							'ACCEPTURL' => 'http://www.' . Cubix_Application::getById()->host . $this->view->getLink('classified-ads-success') . '?id='. $ad_id,
							'DECLINEURL' => 'http://www.' . Cubix_Application::getById()->host . $this->view->getLink('classified-ads-error') . '?id='. $ad_id,
							'EXCEPTIONURL' => 'http://www.' . Cubix_Application::getById()->host . $this->view->getLink('classified-ads-error') . '?id='. $ad_id,
							'CANCELURL' => 'http://www.' . Cubix_Application::getById()->host . $this->view->getLink('classified-ads-error') . '?id='. $ad_id,
						);

						$pf_data['SHASIGN'] = $postfinance_payment->generateChecksum($pf_data);
						try {
							$payment_form = '<form method="post" action="' . $postfinance_payment->submitUrl . '"id=form1 name=form1>';

							foreach($pf_data as $k => $v) {
							    $payment_form .= '<input type="hidden" name="' . $k . '" value="'. $v . '">';
							}

							$payment_form .= '<input type="submit" value="" id=submit2 name=submit2>';
							$payment_form .= '</form>';

						} catch(Exception $e) {
							$message = $e->getMessage();
						}
						$step = 1;
						$this->view->use_postfinance = true;
						$this->view->postfinance_form = $payment_form;
					}elseif($payment_method == 'faxin'){
						$package = $this->model->getPackageById($plan);
						$hash  = base_convert(time(), 10, 36);
						$amount = 100 * $package['price'];
						$postdata = http_build_query(array('sysorderid' => $hash, 'amount' => $amount ));

						$opts = array('http' => array(
							'method'  => 'POST',
							'header'  => 'Content-type: application/x-www-form-urlencoded',
							'content' => $postdata
						));
						
						//test url - paystaging.faxin.ch
						$context  = stream_context_create($opts);
						$faxin_result = file_get_contents('https://pay.faxin.ch/payment', false, $context);
						$result  = json_decode($faxin_result);
						
						if($result->success){
							$result->status = 1;
							$reference =  'CA-'. $ad_id. '-' . $package['id'];
							$this->client->call('OnlineBillingV2.storeFaxin', array($this->user->id, $result->paymentid, $reference, $amount, 0));
							
							$this->view->redirect_url = $result->url;
							$this->view->use_faxin = true;
							
						}else{
							die('FAXIN payment is not available at the moment');
						}
					}elseif($payment_method == 'twispay'){
						$package = $this->model->getPackageById($plan);
						$twispay_payment = new Model_Twispay();
						$client = Cubix_Api_XmlRpc_Client::getInstance();
						$payment_configs = Zend_Registry::get('payment_config');
						$twispay_configs = $payment_configs['twispay'];
						// if ($this->user->id == 48322) {
						// 	$amount = 1;
						// }
						$data = array(
							'siteId' => intval($twispay_configs['siteid']),
							'identifier' => 'user-' . $data['user_id'],
							'amount' =>  $package['price'],
							'currency' => $twispay_configs['currency'],
							'description' =>  'Classified Ads And6.com',
							'orderType' =>  'purchase',	
							'orderId' =>  'CA-'. $ad_id. '-' . $package['id'],
							'cardTransactionMode' => 'authAndCapture',
							'backUrl' => 'https://www.' . Cubix_Application::getById()->host . $this->view->getLink('classified-ads-twispay-response') . '?id='. $ad_id
						);

						$token = json_encode(['externalOrderId' => $data['orderId']]);
	                    $isOrderCreated = $client->call('OnlineBillingV2.storeToken', array($token, $this->user->id, 'twispay'));
	             		
	             		if($isOrderCreated === 1){
	             			 die(json_encode([
	                            'status' => 'error',
	                            'error'  => 'Pending transfer exists,  please contact support',
	                        ]));
	             		}

	                    if (!$isOrderCreated) {
	                        die(json_encode([
	                            'status' => 'error',
	                            'error'  => 'Twispay is not available at the moment',
	                        ]));
	                    }

						$data['checksum'] = $twispay_payment->generateChecksum($data, $twispay_configs['key']);

						try {
							$payment_form = '<form accept-charset="UTF-8" id="twispay-payment-form" action="' . $twispay_configs['url'] . '" method="post">';

							foreach($data as $k => $v) {
							    $payment_form .= '<input type="hidden" name="' . $k . '" value="'. $v . '">';
							}
							$payment_form .= '<input type="submit" value="submit" style="display:none;">';
							$payment_form .= '</form>';

						} catch(Exception $e) {
							$message = $e->getMessage();
							var_dump($message); die();
						}
						$step = 1;
						$this->view->use_twispay = true;
						$this->view->twispay_form = $payment_form;
					}
					 elseif($payment_method == 'phone'){
						$package = $this->model->getPackageById($plan);
						$reference = 'CA_'. $ad_id. '_' . $package['id'];
						$this->view->redirect_url = $this->getIvrUrl($package['price'], $reference, $ad_id);
						$this->view->use_ivr = true;
						$step = 1;
					}
				}
			} else{
				$step = 1;
				$errors['top'] = Cubix_I18n::translate('error');
			}
		}
		$this->view->errors = $errors;
		$this->view->step = $step;
		$this->view->data = $data;
		$this->view->first_city = $m_cities->getTitleById((int)$data['cities'][0])->title;
		$this->view->category = $defines['classified_ad_categories'][$data['category']];
		$this->view->photos = (isset($this->_session->photos) && count($this->_session->photos)) ? $this->_session->photos : array();
		$this->view->orig_photos = (isset($this->_session->orig_photos) && count($this->_session->orig_photos)) ? $this->_session->orig_photos : array();
// var_dump($this->view->photos);die;
	}
	
	public function ajaxRemovePhotoAction()
	{
		$image_id = (int) $this->_request->image_id;
		
		$status = array('status' => 'error');
		if ( isset($this->_session->photos[$image_id]) ) {
			unset($this->_session->photos[$image_id]);
			unset($this->_session->orig_photos[$image_id]);
			$status = array('status' => 'success');
		}
		
		die(json_encode($status));
	}
	
	public function ajaxUploadPhotoAction()
	{
		if(count($this->_session->photos) >= 3){
			echo json_encode(array('status' => 'error', 'msg' => 'Max 3 photos'));die;
		}
		// $id = time();
		// $this->_session->photos[$id] = 'https://pic.and6.com/and6.com/common/6d/d0/48/6dd0483ecaccfa7a76f7faf373febf13_thumb.png';
		// $this->_session->orig_photos[$id] = 'https://pic.and6.com/and6.com/common/6d/d0/48/6dd0483ecaccfa7a76f7faf373febf13_orig.png';
		// echo json_encode(array('status' => 'success', 'url' => 'https://pic.and6.com/and6.com/common/6d/d0/48/6dd0483ecaccfa7a76f7faf373febf13_thumb.png', 'id' => $id));die;
		try {
			$images = new Cubix_ImagesCommon();
			$file = $_FILES['image'];
			if ( strlen($file['tmp_name']) ) {
			    try{
                    $photo = $images->save($file);
                    $img = new Cubix_ImagesCommonItem($photo);
                    $id = $img->image_id;
                    $url = $img->getUrl('cl_ads_thumb');
                    $orig_url = $img->getUrl('orig');
                }catch (Exception $e){
                	echo json_encode(array('status' => 'error', 'msg' => $e->getMessage()));die;
                }
			}
		} catch ( Exception $e ) {
        	echo json_encode(array('status' => 'error', 'msg' => $e->getMessage()));die;
		}
		$this->_session->photos[$id] = $url;
		$this->_session->orig_photos[$id] = $orig_url;
		echo json_encode(array('status' => 'success', 'url' => $url, 'id' => $id));die;
	}

	public function successAction()
	{
		$this->view->id = (int) $this->_request->id;
	}
	
	public function errorAction()
	{
		
	}

    //return Classifield adds list which is not empty EFNET 1038
    public function listClassifieldAdds($classifieldAdds){
        $tmpClassifieldAdds = array();

        foreach ($classifieldAdds as $key => $value){
            if($this->model->getList(array('category'=>$key))) {
                $tmpClassifieldAdds[$key] = $value;
            }
        }
        return (array)$tmpClassifieldAdds;
    }

    private function getIvrUrl($amount, $client_identifier, $ad_id)
	{
		if ( ! $amount || ! $client_identifier ) {
			return self::$linkHelper->getLink('classified-ads-place-ad');
		}


		$amount *= 100;


		$conf = Zend_Registry::get('ivr');

		$urls = array(
			'base_url'	=> 'http://ivr.truesenses.com/?cmd=new&acc=' . $conf['accCA'] . '&aid=' . $conf['aidCA'] . '&amo=' . $amount . '&cur=chf&ser=' . $conf['serCA'] . '&lan=de&cid=' . $client_identifier,
			'success_url'	=> 'srl=' . 'http://' . $_SERVER['SERVER_NAME'] . self::$linkHelper->getLink('classified-ads-success') . '?id='. $ad_id,
			'failure_url'	=> 'frl=' . 'http://' . $_SERVER['SERVER_NAME'] . self::$linkHelper->getLink('classified-ads-error') . '?id='. $ad_id,
			'cancel_url'	=> 'crl=' . 'http://' . $_SERVER['SERVER_NAME'] . self::$linkHelper->getLink('classified-ads-place-ad')
		);
		$url = implode('&', $urls);

		return $url;
	}

    public function twispayResponseAction()	{
    
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		$twispayConfigs = Zend_Registry::get('payment_config')['twispay'];
		$twispayKey = $twispayConfigs['key'];
        $twispayApiUrl = $twispayConfigs['api_url'];

		$decryptedData = Cubix_Twispay_TwispayApi::decryptIpnResponse($this->_request->opensslResult, $twispayConfigs['key']);
		 // Getting Transaction data
        $ch = curl_init($twispayApiUrl . '/transaction/' . $decryptedData['transactionId']);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization:" . $twispayKey));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $transaction = json_decode(curl_exec($ch), true);
		
		 if ($decryptedData['transactionStatus'] == 'in-progress' || $decryptedData['transactionStatus'] == 'complete-ok') {
			// $this->_redirect($this->view->getLink('online-billing-v4-success'));
			$this->_redirect($this->view->getLink('classified-ads-success') . '?id=' . $req->id);

		} else {
			// $this->_redirect($this->view->getLink('online-billing-v4-failure'));
			$this->_redirect($this->view->getLink('classified-ads-error') . '?id=' . $req->id);
		}
	}
}