<?php

class OnlineBillingV2Controller extends Zend_Controller_Action
{
	public static $linkHelper;

	const SECRET_PREFIX = 'ipnI3Inr';
	const GATEWAY_URL = 'https://gateway.cardgateplus.com/';
	const CURRENCY = 'CHF';
	const SITEID = 3076;
	/**
	 * @var Zend_Session_Namespace
	 */
	protected $_session;
	
	const STATUS_PENDING  = 1;
	const STATUS_ACTIVE   = 2;
	const STATUS_EXPIRED  = 3;
	const STATUS_CANCELLED = 4;
	const STATUS_UPGRADED = 5;
	const STATUS_SUSPENDED = 6;

	public static $STATUS_LABELS = array(
		self::STATUS_PENDING  => 'pending',
		self::STATUS_ACTIVE   => 'active',
		self::STATUS_EXPIRED  => 'expired',
		self::STATUS_CANCELLED => 'cancelled',
		self::STATUS_UPGRADED => 'upgraded',
		self::STATUS_SUSPENDED => 'suspended'
	);

	const OPTIONAL_PRODUCT_ADDITIONAL_AREA  = 18;

	private static $addCitiesCounts = array(
		self::OPTIONAL_PRODUCT_ADDITIONAL_AREA	=> 1
	);
	
	public static $ADD_CITIES_ARR = array(
	);
	
	private static $sedcardPackages = array(11,13,14);
	private static $dayPassesPackages = array(19,20,23);
	
	public static $pack_with_prem_cities = array();

	public function init()
	{	
		die;
		self::$linkHelper = $this->view->getHelper('GetLink'); 
		$this->_request->setParam('no_tidy', true);
		$this->view->layout()->setLayout('private-v2');
		
		$anonym = array('epg-response');
		
		$this->user = Model_Users::getCurrent();
		
		if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}
		
		if ( ! in_array($this->_request->getActionName(), $anonym) && ! $this->user->isAgency() && ! $this->user->isEscort() ) {
			$this->_redirect($this->view->getLink('private-v2'));
		}
		
		$this->view->headTitle('Private Area', 'PREPEND');

		$this->_session = new Zend_Session_Namespace('online_billing');
		
		$this->client = new Cubix_Api_XmlRpc_Client();
	}
	
	public function indexAction()
	{
		if ( $this->user->isEscort() ) {
			
		}
		
		if ( $this->user->isAgency() ) {
			$user_type = USER_TYPE_AGENCY;
			$p_user_type = USER_TYPE_AGENCY;
			$gender = null;
			$p_gender = null;
			
			$this->view->packages_list = $this->client->call('OnlineBillingV2.getPackagesList', array($p_user_type, $p_gender, $is_pseudo_escort, array(11,12,13,14)));
		} else {
			$escort_id = $this->user->escort_data['escort_id'];
			
			$is_pseudo_escort = $this->client->call('OnlineBillingV2.isPseudoEscort', array($escort_id));
			
			$m_escorts = new Model_EscortsV2();
			$escort = $m_escorts->get($escort_id);
			$gender = $escort->gender;
			$p_gender = $escort->gender;
			
			$user_type = USER_TYPE_SINGLE_GIRL;
			$p_user_type = USER_TYPE_SINGLE_GIRL;
			
			if ( $is_pseudo_escort ) {
				$p_user_type = USER_TYPE_AGENCY;
			}
			
			$this->view->packages_list = $this->client->call('OnlineBillingV2.getPackagesList', array($p_user_type, $p_gender, $is_pseudo_escort, array(11, 13, 14)));
			
			// <editor-fold defaultstate="collapsed" desc="Checking if escort can buy package">
			$escort_packages = $this->client->call('OnlineBillingV2.checkIfHasPaidPackage', array($this->user->escort_data['escort_id']));
			
			$allow = true;
			if ( count( $escort_packages ) ) {
				
				$message = '';
				$current_package = $escort_packages[0];
				$pending_package = $escort_packages[1];
				
				if ( $current_package && ( $current_package['status'] == self::STATUS_PENDING || $current_package['status'] == self::STATUS_SUSPENDED ) ) {
					$allow = false;
					$message = 'online_billing_has_pending';
				} elseif ( $pending_package && $pending_package['status'] == self::STATUS_PENDING ) {
					$allow = false;
					$message = 'online_billing_has_pending';
				} elseif ( $current_package['status'] == self::STATUS_ACTIVE && $current_package['expiration_date'] - time() > 5 *24*60*60 ) {
					$allow = false;
					$message = 'online_billing_has_active';
				}
				
				if ( ! $allow ) {
					//$this->view->addScriptPath($this->view->getScriptPath('online-billing'));
					$this->_helper->viewRenderer->setScriptAction("already-has-package");
					
					$this->view->error_message = $message;
					$this->view->user_type = $user_type;
					return;
				}
			}
			// </editor-fold>
		}
		$this->view->user_type = $user_type;
		
		$this->view->pack_with_prem_cities = self::$pack_with_prem_cities;
		$this->view->addCitiesCounts = self::$addCitiesCounts;
	}
	
	public function wizardIndependentAction()
	{
		$this->view->layout()->disableLayout();
		$escort_id = $this->user->escort_data['escort_id'];
		$working_locations = $this->client->call('OnlineBillingV2.getWorkingLocations', array($escort_id));
		
		
		$is_pseudo_escort = $this->client->call('OnlineBillingV2.isPseudoEscort', array($escort_id));

		$m_escorts = new Model_EscortsV2();
		$escort = $m_escorts->get($escort_id);
		//$gender = $escort->gender;
		$gender = $m_escorts->getById($escort_id)->gender;
		
		$user_type = USER_TYPE_SINGLE_GIRL;

		if ( $is_pseudo_escort ) {
			$user_type = USER_TYPE_AGENCY;
		}
		
		if ( ! $this->_request->isPost() ) {
		
			$this->_helper->viewRenderer->setScriptAction("wizard-escort");
			
			$this->view->working_locations = $working_locations;
			
			// <editor-fold defaultstate="collapsed" desc="Checking once again if escort can buy package">
			$escort_packages = $this->client->call('OnlineBillingV2.checkIfHasPaidPackage', array($this->user->escort_data['escort_id']));
			
			$allow = true;
			$this->view->has_active_package = false;
			if ( count( $escort_packages ) ) {
				$current_package = $escort_packages[0];
				$pending_package = $escort_packages[1];
				
				if ( $current_package && ( $current_package['status'] == self::STATUS_PENDING || $current_package['status'] == self::STATUS_SUSPENDED ) ) {
					$allow = false;
				} elseif ( $pending_package && $pending_package['status'] == self::STATUS_PENDING ) {
					$allow = false;
				} elseif ( $current_package['status'] == self::STATUS_ACTIVE && $current_package['expiration_date'] - time() > 5 *24*60*60 ) {
					$allow = false;
				}
				
				if ( ! $allow ) {
					die;
				}
				
				if ( $current_package ) {
					$this->view->has_active_package = true;
				}
			}
			// </editor-fold>
			
		} else {
			
			$validator = new Cubix_Validator();
			$request = $this->_request;
			
			$optional_products = array();
			
			$prem_cities_count = 0;
			if ( $request->package_id && in_array($request->package_id, self::$pack_with_prem_cities) ) {
				$prem_cities_count += 1;
			}
			
			if ( ! $request->package_id ) {
				$validator->setError('package_id', __('package_required'));
			}
			
			if ( $request->opt_product_prem_city ) {
				$optional_products[] = $request->opt_product_prem_city;
				$prem_cities_count = $prem_cities_count + self::$addCitiesCounts[$request->opt_product_prem_city];
			}
			
			if ( count($request->premium_cities) > $prem_cities_count ) {
				$validator->setError('premium-city', __('online_billing_prem_cities_limit', array('COUNT' => $prem_cities_count)));
			}
			
			if ( count($request->premium_cities) < $prem_cities_count ) {
				$validator->setError('premium-city', __('you_need_select_premium_cities', array('COUNT' => $prem_cities_count)));
			}
			
			/*-->Checking if premium_city in working locations*/
			if ( $request->premium_cities ) {
				$working_ids = array();
				foreach($working_locations as $working_location) {
					$working_ids[] = $working_location['id'];
				}
				foreach($request->premium_cities as $city_id) {
					if ( ! in_array($city_id, $working_ids) ) {
						$validator->setError('premium-city', __('city_is_not_in_working_locations'));
						break;
					}
				}
			}
			/*<--Checking if premium_city in working locations*/

			if ( count($request->opt_products) ) {
				$optional_products = array_merge($optional_products, $request->opt_products);

				if ( in_array(18, $request->opt_products) && ! count($request->additional_areas) ) {
					$validator->setError('additional-area', __('additional_area_required'));
				}
			}
			
			if ( $validator->isValid() ) {
				$data[] = array(
					'user_id' => $this->user->id,
					'escort_id' => $escort_id,
					'agency_id' => null,
					'package_id' => $request->package_id,
					'data' => ( count($optional_products) || count($request->premium_cities) ) ? serialize(array('optional_products' => $optional_products, 'additional_areas' => $request->additional_areas)) : serialize(array())
				);

				$amount = $this->client->call('OnlineBillingV2.addToShoppingCart', array($data, $this->user->id, $user_type));
				
				$epg_payment = new Model_EpgGateway();
				$reference = 'SC-' . $this->user->id . '-' . md5(time());
				$token = $epg_payment->getTokenForAuth($reference, $amount, 'http://www.and6.com/online-billing-v2/epg-response');
				$this->client->call('OnlineBillingV2.storeToken', array($token, $this->user->id));
				
				die(json_encode(array('status' => 'success', 'url' => $epg_payment->getPaymentGatewayUrl($token))));
			}
			
			die(json_encode($validator->getStatus()));
		}
		
		$this->view->packages = $this->client->call('OnlineBillingV2.getPackagesList', array($user_type, $gender, $is_pseudo_escort));
	}
	
	public function wizardAgencyAction()
	{
		$this->view->layout()->disableLayout();
		$agency_id = $this->user->agency_data['agency_id'];
		
		$session = new Zend_Session_Namespace('self-checkout-wizard');
		
		if ( ! $this->_request->isPost() ) {
			$session->unsetAll();
			$session->cart = array();
			$this->_helper->viewRenderer->setScriptAction("wizard-agency");
			$escorts = $this->client->call('OnlineBillingV2.getAgencyEscorts', array($agency_id));
			
			// <editor-fold defaultstate="collapsed" desc="checking if escorts can buy package">
			foreach($escorts as $i => $escort) {
				$allow = true;
				if ( count( $escort['escort_packages'] ) ) {
					$current_package = $escort['escort_packages'][0];
					$pending_package = $escort['escort_packages'][1];
					
					if ( $current_package && $current_package['status'] == self::STATUS_ACTIVE ) {
						$escorts[$i]['has_active_package'] = true;
					}
					
					if ( $current_package && ( $current_package['status'] == self::STATUS_PENDING || $current_package['status'] == self::STATUS_SUSPENDED ) ) {
						$allow = false;
					} elseif ( $pending_package && $pending_package['status'] == self::STATUS_PENDING ) {
						$allow = false;
					} elseif ( $current_package['status'] == self::STATUS_ACTIVE && $current_package['expiration_date'] - time() > 5 *24*60*60 ) {
						$allow = false;
					}
					
					if ( ! $allow ) {
						unset($escorts[$i]);
					}

					if ( $current_package ) {
						
					}
				}
			}
			// </editor-fold>


			$this->view->escorts = $escorts;
			
		} else {
			$validator = new Cubix_Validator();
			$request = $this->_request;
			
			$optional_products = array();
			
			$prem_cities_count = 0;
			
			$package_id = null;
			if ( $request->package_id ) {
				$package_id = reset(explode('-', $request->package_id));
			}
			
			if ( $package_id && in_array($request->package_id, self::$pack_with_prem_cities) ) {
				$prem_cities_count += 1;
			}
			
			if ( ! $request->escort_id ) {
				$validator->setError('escort_id', __('escort_required'));
			}
			
			if ( ! $package_id ) {
				$validator->setError('package_id', __('package_required'));
			}
			
			if ( $request->opt_product_prem_city ) {
				$optional_products[] = $request->opt_product_prem_city;
				$prem_cities_count = $prem_cities_count + self::$addCitiesCounts[$request->opt_product_prem_city];
			}
			
			if ( count($request->premium_cities) > $prem_cities_count ) {
				$validator->setError('premium-city', __('online_billing_prem_cities_limit', array('COUNT' => $prem_cities_count)));
			}
			
			if ( count($request->premium_cities) < $prem_cities_count ) {
				$validator->setError('premium-city', __('you_need_select_premium_cities', array('COUNT' => $prem_cities_count)));
			}
			
			if ( count($request->opt_products) ) {
				$optional_products = array_merge($optional_products, $request->opt_products);

				if ( in_array(18, $request->opt_products) && ! count($request->additional_areas) ) {
					$validator->setError('additional-area', __('additional_area_required'));
				}
			}
			
			/*-->Checking if premium_city in working locations*/
			if ( $request->premium_cities ) {
				$working_locations = $this->client->call('OnlineBillingV2.getWorkingLocations', array($request->escort_id));
				$working_ids = array();
				foreach($working_locations as $working_location) {
					$working_ids[] = $working_location['id'];
				}
				foreach($request->premium_cities as $city_id) {
					if ( ! in_array($city_id, $working_ids) ) {
						$validator->setError('premium-city', __('city_is_not_in_working_locations'));
						break;
					}
				}
			}
			/*<--Checking if premium_city in working locations*/
			
			if ( $validator->isValid() ) {
				$session->cart[] = array(
					'user_id' => $this->user->id,
					'escort_id' => $request->escort_id,
					'agency_id' => $agency_id,
					'package_id' => $package_id,
					'data' => ( count($optional_products) ) ? serialize(array('optional_products' => $optional_products, 'additional_areas' => $request->additional_areas)) : serialize(array())
				);
				
				end($session->cart);
				$key = key($session->cart);
				
				die(json_encode(array('status' => 'success', 'cart_id' => $key)));
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	/*-->Only for agencies*/
	public function removeFromCartAction()
	{
		$cart_id = $this->_request->cart_id;
		
		$session = new Zend_Session_Namespace('self-checkout-wizard');
		
		$escort_id = $session->cart[$cart_id]['escort_id'];
		$m_escorts = new Model_EscortsV2();
		$escort = $m_escorts->get($escort_id);
		
		unset($session->cart[$cart_id]);
		die(json_encode(array(
			'status' => 'success', 
			'escort' => array('id' => $escort->id,'showname' => $escort->showname)))
		);
	}
	
	public function checkoutAction()
	{
		$session = new Zend_Session_Namespace('self-checkout-wizard');
		$user_type = USER_TYPE_AGENCY;
		
		$validator = new Cubix_Validator();
		
		if ( ! count($session->cart) ) {
			$validator->setError('cart', __('cart_is_empty'));
		}
		
		if ( $validator->isValid() ) {
			$amount = $this->client->call('OnlineBillingV2.addToShoppingCart', array($session->cart, $this->user->id, $user_type));

			$epg_payment = new Model_EpgGateway();
			$reference = 'SC-' . $this->user->id . '-' . md5(time());
			$token = $epg_payment->getTokenForAuth($reference, $amount, 'http://www.and6.com/online-billing-v2/epg-response');
			$this->client->call('OnlineBillingV2.storeToken', array($token, $this->user->id));
			
			die(json_encode(array('status' => 'success', 'url' => $epg_payment->getPaymentGatewayUrl($token))));
		}
		
		die(json_encode($validator->getStatus()));
	}
	/*<--Only for agencies*/
	
	public function getOptionalProductsAction()
	{
		$user_type = USER_TYPE_SINGLE_GIRL;
		if ( $this->user->isAgency() ) {
			$user_type = USER_TYPE_AGENCY;
		}
		
		if ( $this->user->isAgency() ) {
			$escort_id = $this->_request->escort_id;
		} else {
			$escort_id = $this->user->escort_data['escort_id'];
		}
		
		$m_escorts = new Model_EscortsV2();
		$escort = $m_escorts->get($escort_id);
		$gender = $escort->gender;
		
		$package_id = $this->_request->package_id;
		$package_id = reset(explode('-', $package_id));
		
		$is_pseudo_escort = $this->client->call('OnlineBillingV2.isPseudoEscort', array($escort_id));
		
		
		$optional_products = $this->client->call('OnlineBillingV2.getOptionalProducts', array($package_id, $user_type, $gender));
		$additional_cities = array();
		$additional_cities[] = array('id' => '', 'name' => __('no_additional_city'));
		
		if ( count($optional_products) ) {
			foreach ( $optional_products as $i => $product ) {
				$optional_products[$i]['name'] = Cubix_I18n::translate($product['name']);
				$optional_products[$i]['price'] = (int) $product['price'];
				if ( in_array($product['id'], self::$ADD_CITIES_ARR) ) {
					$additional_cities[] = $optional_products[$i];
					unset($optional_products[$i]);
				}
			}
		}
		
		if ( count($additional_cities) == 1 ) {//No additional cities opt product
			$additional_cities = array();
		}
		
		//Angel task - Do not show add cities products for diamond package
		//Reaseon: they always can change prem city with diamond package
		if ( ($package_id == 101 || $package_id == 102) && ($user_type == USER_TYPE_SINGLE_GIRL || $is_pseudo_escort) ) {
			$additional_cities = array();
		}
		
		die(json_encode(array(
			'optional_products'	=> $optional_products,
			'additional_cities'	=> $additional_cities,
			'isPackageWithPremCities'	=> in_array($package_id, self::$pack_with_prem_cities) ? true : false
		)));
	}
	
	public function getEscortDataAction()
	{
		$escort_id = $this->_request->escort_id;
		
		$user_type = USER_TYPE_AGENCY;
		
		$m_escorts = new Model_EscortsV2();
		$escort = $m_escorts->get($escort_id);
		$gender = $escort->gender;
		
		$packages = $this->client->call('OnlineBillingV2.getPackagesList', array($user_type, $gender));
		
		foreach($packages as $i => $package) {
			$packages[$i]['name'] = str_replace('Lite', '', $package['name']) . ' ' . $package['period'] . ' ' . __('days') . ' ('. $package['price'] .' CHF)';
		}
		
		$working_locations = $this->client->call('OnlineBillingV2.getWorkingLocations', array($escort_id));
		
		die(json_encode(array(
			'packages' => $packages,
			'cities'	=> $working_locations
		)));
	}
	
	
	public function epgResponseAction()
	{
		$this->view->layout()->disableLayout();
		
		$req = $this->_request;
		
		$token = $req->Token;
		if ( ! $token ) $this->_redirect('/');
		
		$epg_payment = new Model_EpgGateway();
		$result = $epg_payment->getTransactionStatus($token);
		
		if ( $result['ResultStatus'] == "OK" && $result['TransactionResult'] == "OK" ) {
			$this->_redirect($this->view->getLink('ob-successful'));
		} else {
			$this->_redirect($this->view->getLink('ob-unsuccessful'));
		}
	}
	
	public function epgGotdResponseAction()
	{
		$this->view->layout()->disableLayout();
		
		$req = $this->_request;
		
		$token = $req->Token;
		if ( ! $token ) $this->_redirect('/');
		
		$epg_payment = new Model_EpgGateway();
		$result = $epg_payment->getTransactionStatus($token);
		
		if ( $result['ResultStatus'] == "OK" && $result['TransactionResult'] == "OK" ) {
			$this->_redirect($this->view->getLink('private-v2-gotd-success'));
		} else {
			$this->_redirect($this->view->getLink('private-v2-gotd-failure'));
		}
	}
	
	public function successfulPaymentAction()
	{
		
	}
	
	public function unsuccessfulPaymentAction()
	{
		
	}
	
	public function paymentAction()
	{
		$this->view->add_area_price = 200;
		if ( ! $this->_request->isPost() ) {
			if ( $this->user->isAgency() ) {
				
				$agency_id = $this->user->agency_data['agency_id'];
				$user_type = USER_TYPE_AGENCY;
				$p_user_type = USER_TYPE_AGENCY;
				$gender = null;
				$p_gender = null;

				$this->view->packages_list = $this->client->call('OnlineBillingV2.getPackagesList', array($p_user_type, $p_gender, $is_pseudo_escort, self::$sedcardPackages));
				$this->view->day_passes_list = $this->client->call('OnlineBillingV2.getPackagesList', array($p_user_type, $p_gender, $is_pseudo_escort, self::$dayPassesPackages));
				
				$escorts = $this->client->call('OnlineBillingV2.getAgencyEscorts', array($agency_id));
				// <editor-fold defaultstate="collapsed" desc="checking if escorts can buy package">
			foreach($escorts as $i => $escort) {
				$allow = true;
				if ( count( $escort['escort_packages'] ) ) {
					$current_package = $escort['escort_packages'][0];
					$pending_package = $escort['escort_packages'][1];
					
					if ( $current_package && $current_package['status'] == self::STATUS_ACTIVE ) {
						$escorts[$i]['has_active_package'] = true;
					}
					
					if ( $current_package && ( $current_package['status'] == self::STATUS_PENDING || $current_package['status'] == self::STATUS_SUSPENDED ) ) {
						$allow = false;
					} elseif ( $pending_package && $pending_package['status'] == self::STATUS_PENDING ) {
						$allow = false;
					} elseif ( $current_package['status'] == self::STATUS_ACTIVE && $current_package['expiration_date'] - time() > 5 *24*60*60 ) {
						$allow = false;
					}
					
					if ( ! $allow ) {
						unset($escorts[$i]);
					}

					if ( $current_package ) {
						
					}
				}
			}
			// </editor-fold>
				$this->view->escorts = $escorts;
				
				
			} else {
				$escort_id = $this->user->escort_data['escort_id'];

				$is_pseudo_escort = $this->client->call('OnlineBillingV2.isPseudoEscort', array($escort_id));

				$m_escorts = new Model_EscortsV2();
				$escort = $m_escorts->get($escort_id);
				$gender = $escort->gender;
				$p_gender = $escort->gender;

				$user_type = USER_TYPE_SINGLE_GIRL;
				$p_user_type = USER_TYPE_SINGLE_GIRL;

				if ( $is_pseudo_escort ) {
					$p_user_type = USER_TYPE_AGENCY;
				}

				$this->view->packages_list = $this->client->call('OnlineBillingV2.getPackagesList', array($p_user_type, $p_gender, $is_pseudo_escort, self::$sedcardPackages));
				$this->view->day_passes_list = $this->client->call('OnlineBillingV2.getPackagesList', array($p_user_type, $p_gender, $is_pseudo_escort, self::$dayPassesPackages));

				// <editor-fold defaultstate="collapsed" desc="Checking if escort can buy package">
				$escort_packages = $this->client->call('OnlineBillingV2.checkIfHasPaidPackage', array($this->user->escort_data['escort_id']));

				$allow = true;
				if ( count( $escort_packages ) ) {

					$message = '';
					$current_package = $escort_packages[0];
					$pending_package = $escort_packages[1];

					if ( $current_package && ( $current_package['status'] == self::STATUS_PENDING || $current_package['status'] == self::STATUS_SUSPENDED ) ) {
						$allow = false;
						$message = 'online_billing_has_pending';
					} elseif ( $pending_package && $pending_package['status'] == self::STATUS_PENDING ) {
						$allow = false;
						$message = 'online_billing_has_pending';
					} elseif ( $current_package['status'] == self::STATUS_ACTIVE && $current_package['expiration_date'] - time() > 5 *24*60*60 ) {
						$allow = false;
						$message = 'online_billing_has_active';
					}

					if ( ! $allow ) {
						//$this->view->addScriptPath($this->view->getScriptPath('online-billing'));
						$this->_helper->viewRenderer->setScriptAction("already-has-package");

						$this->view->error_message = $message;
						$this->view->user_type = $user_type;
						return;
					}
				}
				// </editor-fold>
			}
			$this->view->user_type = $user_type;

			$this->view->pack_with_prem_cities = self::$pack_with_prem_cities;
			$this->view->addCitiesCounts = self::$addCitiesCounts;
		} else {
			$validator = new Cubix_Validator();
			$request = $this->_request;
			
			$optional_products = array();

			if ( $this->user->isAgency() && ! $request->escort_id ) {
				$validator->setError('escort_id', __('escort_required'));
			}
			
			if ( ! $request->package_id ) {
				$validator->setError('package_id', __('package_required'));
			}
			
			
			$optional_products = array();
			$add_areas = array();
			if ( $request->add_region ) {
				$optional_products[] = self::OPTIONAL_PRODUCT_ADDITIONAL_AREA;
				
				$add_areas = $request->additional_area;
				if ( ! count($add_areas) ) {
					$validator->setError('add_area', __('additional_area_required'));
				}
			}
			
			
			$payment_method = $request->payment_method; 
			if ( ! in_array($request->package_id, self::$dayPassesPackages) ) {
				$payment_method = 'card';
			}
			
			
			if ( $validator->isValid() ) {
				$escort_id = $this->user->isAgency() ? $request->escort_id : $this->user->escort_data['escort_id'];
				$agency_id = $this->user->isAgency() ? $this->user->agency_data['agency_id'] : null;
				$user_type = $this->user->IsAgency() ? USER_TYPE_AGENCY : USER_TYPE_SINGLE_GIRL;
				
				$escort_gender = null;
				if ( $this->user->isAgency() ) {
					$escorts = $this->client->call('OnlineBillingV2.getAgencyEscorts', array($agency_id));
					foreach($escorts as $escort) {
						if ( $escort['id'] == $escort_id ) {
							$escort_gender = $escort['gender'];
						}
					}
				} else {
					$escort_gender = $this->escort->gender;
				}
				
				if ( $payment_method == 'card' ) {
					$data[] = array(
						'user_id' => $this->user->id,
						'escort_id' => $escort_id,
						'agency_id' => $agency_id,
						'package_id' => $request->package_id,
						'data' => ( count($optional_products) ) ? serialize(array('optional_products' => $optional_products, 'additional_areas' => $add_areas)) : serialize(array())
					);

					$amount = $this->client->call('OnlineBillingV2.addToShoppingCart', array($data, $this->user->id, $user_type));
					
					$epg_payment = new Model_EpgGateway();
					$reference = 'SC-' . $this->user->id . '-' . md5(time());
					$token = $epg_payment->getTokenForAuth($reference, $amount, 'http://www.and6.com/online-billing-v2/epg-response');
					$this->client->call('OnlineBillingV2.storeToken', array($token, $this->user->id));

					die(json_encode(array('status' => 'success', 'url' => $epg_payment->getPaymentGatewayUrl($token))));
				} else {
					$application_id = Cubix_Application::getId();
					$package = $this->client->call('Billing.gePackagesWithPrice', array($application_id, $user_type, $escort_gender, $request->package_id));

					$package_data = array(
						'escort_id' => $escort_id,
						'package_id' => $request->package_id,
						'application_id'	=> $application_id,
						'base_price'	=> $package['price'],
						'price'	=> $package['price'],
						'base_period'	=> $package['period'],
						'period'	=> $package['period'],
						//'activation_type'	=> $this->_getParam('activation_type'),
						//'activation_date' => strtotime($this->_getParam('activation_date')),
						'is_agency'	=> $this->user->isAgency(),
						'gender'	=> $escort_gender

					);

					$post_data = array(
						'order_data' => array(
							'order_date'	=> date('Y-m-d H:i:s'),
							'application_id'	=> $application_id,
							'price'	=> $package['price'],
							'user_id'	=> $this->user->id
						),
						'packages_data'	=> array($package_data)
					);
					$response = $this->client->call('Billing.addPackage', array($post_data));
					
					try{
						$esc_n_h = new Model_Escort_NewHistory();
						$esc_n_h->save(array('escort_id' => $escort_id, 'package_id' => $request->package_id, 'period' => $package['period']));
					} catch(Exception $e){
						file_put_contents('debug_new_history.log', $e, FILE_APPEND);
					}

					if ( is_numeric($response) ) {

						$response = "DP_" . $response;

						die(json_encode(array('status' => 'success', 'url' => $this->getIvrUrl($package['price'], $response, $escort_id))));
					}
				}
			}
			
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	
	private function getIvrUrl($amount, $client_identifier, $escort_id = null)
	{
		if ( ! $amount || ! $client_identifier ) {
			return self::$linkHelper->getLink('private-v2-advertise-now');
		}
		
		
		$amount *= 100;
		
		
		$s_url = '';
		if ( $escort_id ) {
			$s_url .= '?escort=' . $escort_id;
		}
		
		$conf = Zend_Registry::get('ivr');
		
		$urls = array(
			'base_url'	=> 'http://ivr.truesenses.com/?cmd=new&acc=' . $conf['acc'] . '&aid=' . $conf['aid'] . '&amo=' . $amount . '&cur=chf&ser=' . $conf['acc'] . '&lan=de&cid=' . $client_identifier,
			'success_url'	=> 'srl=' . self::$linkHelper->getLink('private-v2-advertise-success') . $s_url,
			'failure_url'	=> 'frl=' . self::$linkHelper->getLink('private-v2-advertise-failure'),
			'cancel_url'	=> 'crl=' . 'http://' . $_SERVER['SERVER_NAME'] . self::$linkHelper->getLink('private-v2-advertise-now')
		);
		$url = implode('&', $urls);
		
		return $url;
	}
}
