var express = require('express');
var router = express.Router();
var request = require('superagent');
require('superagent-as-promised')(request);
var config = require('../config');
var redis = require('../lib/redis');
var debug = require('debug')('geocode:server');
var utf8 = require('utf8');
var knex = require('../lib/knex');
var utf8 = require('utf8');

/* GET home page. */
router.get('/', function(req, res, next) {
    var result;

    if (!req.query.addr) {
        res.status(400);
        return res.json({});
    }

    req.query.addr = utf8.encode(req.query.addr);

    knex('cache')
        .where({ address: req.query.addr })
        .select('latitude', 'longitude')
        .then(function(result) {
            if (result.length) {
                if (result[0].latitude != 0 && result[0].longitude != 0) {
                    res.status(200);
                    res.json({
                        lat: parseFloat(result[0].latitude),
                        lng: parseFloat(result[0].longitude)
                    });
                }
                else {
                    res.status(404);
                    res.json({ status: 'address not found' });
                }

                return null;
            }

            return request
                .get('https://maps.googleapis.com/maps/api/geocode/json?address=' + req.query.addr + '&key=' + config.api_key);
        })
        .then(function (resp) {
            if (resp === null) return null;

            /*if (err) {
                debug(err);
                debug(resp.body);
                res.status(500);
                res.json({ status: 'error' });
                return null;
            }*/
console.log(resp.body.status);
            if (resp.body.status !== 'OK' && resp.body.status !== 'ZERO_RESULTS') {
                debug(resp.body);
                res.status(500);
                console.log(12, resp.body);
                res.json({ status: 'error' });
                return null;
            }

            if (result = resp.body.results) {
                if (result.length > 0) {
                    if (result = result[0].geometry) {
                        if (result = result.location) {
                            return knex('cache')
                                .insert({
                                    address: req.query.addr,
                                    latitude: result.lat,
                                    longitude: result.lng
                                });

                        }
                    }
                }
            }

            return false;
        })
        .then(function(_result) {
            if (_result !== null && _result !== false) {
                res.status(200);
                res.json(result);
            }
            else if (_result === false) {
                res.status(404);
                res.json({ status: 'addr not found' });
                cacheNotFound(req.query.addr);
            }
        })
        .catch(next);
});

function cacheNotFound(address) {
    return knex('cache')
        .insert({
            address: address,
            latitude: 0,
            longitude: 0
        })
        .then(function() {
        });
}

module.exports = router;
