window.addEvent('domready', function(){
   MPrivateV2.init();
});

var MPrivateV2 = {
    init: function(){
        this.langPicker();
        this.langPickerService();
        this.showLimited();
        this.stepProcess();
        this.collapseRates();
        this.collapseServices();
        this.extraServices();
        this.collapseLangs();
        this.toggleArchivationPhoto();
    },

    langPicker: function(){
        var lang_pic = $$('.lng-picker'),
            lang_body = $$('[id^="container-about-"]');

        lang_pic.addEvent('change', function(){
            var _lng = this.getSelected().get('data-lng')[0];

            lang_body.addClass('none');
            $('container-about-' + _lng).removeClass('none');
        });
    },

    langPickerService: function(){
        var lang_pic = $$('.languge-picker'),
            lang_body = $$('[id^="container-service-"]');

        lang_pic.addEvent('change', function(){
            var _lng = this.getSelected().get('data-lng')[0];

            lang_body.addClass('none');
            $('container-service-' + _lng).removeClass('none');
        });
    },

    showLimited: function(){
        var limiter = $$('.divliner'),
            limit_block,
            limit_block_class;

        limiter.addEvent('click', function(){
            limit_block = this.getParent();
            limit_block_class = limit_block.get('data-class');

            if( limit_block ){
                if( limit_block.hasClass( limit_block_class ) ){
                    limit_block.removeClass( limit_block_class );
                    this.set('html', Dictionary.less_minus);
                } else {
                    limit_block.addClass( limit_block_class );
                    this.set('html', Dictionary.more_plus);
                }
            }
        });
    },

    stepProcess: function(){
        var nextStep = $('nextStep');
        if( !nextStep ) return false;

        var btn = nextStep.getElements('button'),
            form = $('generalStep');

        btn.addEvent('click', function(e){
            e.preventDefault();
            var step = this.get('data-them');

            $('themStep').set('value', step);

            /*if( step === ':back' ){
                form.set('action', 'get');
            }*/

            $('generalStep').submit();
        });
    },

    collapseRates: function(){
        var elm = $$('.add-rate-collapse');

        elm.addEvent('click', function(){
            var collapser = this.getParent().getElement('.rates');

            if( collapser.hasClass('show') ){
                collapser.removeClass('show');
                this.removeClass('show');
            } else {
                collapser.addClass('show');
                this.addClass('show');
            }
        });
    },

    collapseServices: function(){
        var elm = $$('.add-services-collapse');

        elm.addEvent('click', function(){
            var collapser = this.getParent().getElement('.collapser');

            if( collapser.hasClass('show') ){
                collapser.removeClass('show');
                this.removeClass('show');
                //this.setStyle('background', "url('/m/i/rate-add.png') left center no-repeat" );
            } else {
                collapser.addClass('show');
                this.addClass('show');
                //this.setStyle('background', "url('/m/i/minus-button.png') left center no-repeat/20px" );
            }
        });
    },


    collapseLangs: function () {
        var elm = $$('.add-lang-collapse');
        elm.addEvent('click', function(){
            var collapser = this.getParent().getElement('.more-lngs');

            if( collapser.hasClass('opened') ){
                elm.setStyle('background', "url('/m/i/minus-button.png') left center no-repeat/20px" );

            } else {
                elm.setStyle('background', "url('/m/i/rate-add.png') left center no-repeat" );
            }
        });
    },

    extraServices: function(){
        var _chks = $$('.mservices .tr input[type="checkbox"]');
        _chks.addEvent('change', function(){
            var checked = !! this.get('checked');

            if( checked ){
                this.getParent().getElement('.ext-switcher').setStyle('display', 'block');
            } else {
                this.getParent().getElement('.ext-switcher').setStyle('display', 'none');
            }
        });

        var _extra = $$('.ext-switcher');
        _extra.addEvent('click', function(){
            if( this.hasClass('done') ){
                this.getParent().getElement('.ext-value').setStyle('display', 'none').set('html', '');
                this.getParent().getElement('.extra-block input').set('value', '');
                this.removeClass('done');
                return true;
            }

            var parent = this.getParent();
            if( parent.hasClass('active') ){
                parent.getElement('.extra-block').setStyle('display', 'none');
                parent.removeClass('active');
            } else {
                parent.getElement('.extra-block').setStyle('display', 'block');
                parent.addClass('active');
            }
        });

        var _close = $$('.ext-close');
        _close.addEvent('click', function(){
            this.getParent().setStyle('display', 'none');
            this.getParent('.tr').removeClass('active');
        });

        var _ok = $$('.ext-ok');
        _ok.addEvent('click', function(){
            var value = this.getParent().getElement('input').get('value'),
                parent = this.getParent('.tr'),
                value_input = parent.getElement('.ext-value');


            if( parseInt( value ) ){
                value_input.setStyle('display', 'block').set('html', '+ ' + value + ' CHF');
                parent.getElement('.ext-switcher').addClass('done');
            } else {
                value_input.setStyle('display', 'none').set('html', '');
                parent.getElement('.ext-switcher').removeClass('done');
            }

            parent.removeClass('active');
            this.getParent().setStyle('display', 'none');
        });
    },

    toggleArchivationPhoto: function(){
        var btn = $$('.actions .is_archive','.actions .is_public');

        if(!btn) return false;  

        btn.addEvent('click', function(e){
            e.stop();
            var self = this;

            var photo_id = this.get('data-value'),
                process = this.get('data-process'),
                action = this.get('data-type'),
                data = {};

            if( !photo_id || !process || !action ) return false;

            data[ action ] = 1;
            data.a = process;
            data['photo_id'] = photo_id;
            data.is_ajax = 1;
            data.process = action;

            var __url = '/profile-v2/gallery';
            var parent = self.getParent('li');

            new Request({
                url: __url,
                method: 'POST',
                data: data,
                onRequest: function(){
                    parent.addClass('inactive');
                },
                onSuccess: function (resp) {
                    var data = JSON.encode(resp);
                    console.log(resp);

                    if( data.error ){
                        alert( data.error );
                        return false
                    } else {
                        self.addClass('hidden')

                        if(self.hasClass('is_archive')){
                            parent.getElements('.is_public').removeClass('hidden');
                            parent.addClass('archived')
                        }
                        else{
                            parent.getElements('.is_archive').removeClass('hidden');
                            parent.removeClass('archived')
                        }

                    }

                    parent.removeClass('inactive');
                }.bind(this)
            }).send();

        });
    },

    setMainPhoto: function(){
        var btn = $$('.actions .is_main');
        if( !btn ) return false;

        btn.addEvent('click', function(e){
            e.stop();
            var self = this;

            var photo_id = this.get('data-value'),
                process = this.get('data-process'),
                action = this.get('data-type'),
                data = {};

            if( !photo_id || !process || !action ) return false;

            data[ action ] = 1;
            data.a = process;
            data['photo_id[]'] = photo_id;
            data.is_ajax = 1;
            data.escort = this.getParent().get('data-key');

            var __url;

            if( this.hasClass('_create') ){
                __url = '/profile-v2/gallery-create';
            } else {
                __url = '/profile-v2/gallery';
            }

            var parent = self.getParent('li');

            new Request({
                url: __url,
                method: 'POST',
                data: data,
                onRequest: function(){
                    parent.addClass('inactive');
                },
                onSuccess: function (resp) {
                    var data = JSON.encode(resp);
                    console.log(resp);

                    if( data.error ){
                        alert( data.error );
                        return false
                    } else {
                        $$('.actions .main').removeClass('main').set('text', '');
                        self.addClass('main').set('text', Dictionary.setMain);
                        $$('li.main').removeClass('main')
                        parent.addClass('main');
                    }

                    parent.removeClass('inactive');
                }.bind(this)
            }).send();
        });
    },

    removePhoto: function(){
        var btn = $$('.actions .is_remove');

        if( !btn ) return false;

        btn.addEvent('click', function(e){
            e.stop();
            var self = this;

            var photo_id = this.get('data-value'),
                process = this.get('data-process'),
                action = this.get('data-type'),
                data = {};

            if( !photo_id || !process || !action ) return false;

            var esc_id = parseInt( $$('.escort-id')[0].get('value') );

            data[ process ] = 1;
            data.a = action;
            data['photo_id[]'] = photo_id;
            data.is_ajax = 1;

            if( esc_id ){
                data.escort = esc_id;
            }

            var __url;

            if( this.hasClass('_create') ){
                __url = '/profile-v2/gallery-create';
            } else {
                __url = '/profile-v2/gallery';
            }

            var parent = self.getParent('li');

            new Request({
                url: __url,
                method: 'POST',
                data: data,
                onRequest: function(){
                    parent.addClass('inactive');
                },
                onSuccess: function (resp) {
                    var data = JSON.encode(resp);
                    console.log(resp);

                    if( data.error ){
                        alert( data.error );
                        return false
                    } else {
                        parent.destroy();
                    }

                    parent.removeClass('inactive');
                }.bind(this)
            }).send();
        });
    },

    changeRegions: function(){
        var elm = $('regions');
        if( !elm ) return false;

         elm.addEvent('change', function(){
            var city_id = elm.get('value');

            if( !city_id ) return false;
            $('fake_city_id').set('value', city_id);
        });
    },

    showErrors: function( data ){
        console.log( data );
        if( data.length === 0 && data.constructor === Array ) return true;
        if( Object.keys(data).length === 0 && data.constructor === Object ) return true;

        $$('.form_error').removeClass('form_error');

        Object.each( data, function(value, key){
            var inp = $$('[name="' + key + '"]');
            inp.addClass('form_error');
        });

        this.showBarErrors( data );
    },

    showBarErrors: function( data ){
        var _elm = $$('.st-errors')[0];

        //Object.each( data, function(value, key){
        if( !_elm.hasClass('in') ){
            _elm.addClass('in');
        }

        //var _p = new Element('p', { 'class': 'error-item' }).set('text', MPrivateV2.errorsDic[key] + ': ' + value);
        var _p = new Element('p', { 'class': 'error-item' }).set('text', MPrivateV2.errorsDic['mob_missing_element']);

        if( _elm ) _p.inject(_elm);
        //});

        setTimeout(function(){
            _elm.empty().removeClass('in');
        }, 4000);
    },

    checkExistBlockCountry : function (city_id) {


        var btn = $$('#btn-location-add');
        if( !btn ) return false;

        var remove = function () {
            if ( this.getParent('.location') )
            {
                this.getParent('.location').destroy();
            }
        };

        btn.addEvent('click', function(e){

            var city_id = $('block_country').get('value'),
                city_title = $('block_country').getSelected().get('html'),
                exists_city_ids = ['', 'block_all', '101'];
            if ( ! city_id ) return;

            var found = false;
            $$('#countries input[type=hidden]').each(function (el) {
                var _city_id = el.get('value');
                exists_city_ids.push(_city_id);
                if ( city_id == _city_id ) {
                    found = true;
                    el.set('value', city_id);
                    el.getPrevious('label').getElement('span').set('html', city_title);
                }
            });

            if(! found){
                var location = new Element('div', {'class': 'location mb5'}).inject($('countries'));

                new Element('a', {
                    'class': 'sbtn icon-remove',
                }).set('text',"⨯").inject(new Element('div', {
                    'class': 'fleft m0'
                }).inject(location)).addEvent('click', remove);

                var label = new Element('label', {'class': 'fleft _title'}).inject(location);

                var span = new Element('span', {
                    html: city_title
                }).inject(label);


                new Element('input', {
                    type: 'hidden',
                    name: 'block_countries[]',
                    value: city_id
                }).inject(location);

                new Element('div', {'class': 'clear'}).inject(location);
            }

        });



    }
};

MPrivateV2.imageUpload = {
    input: null,
    wrapper: null,
    name_input_part: null,
    order: 1,

    init: function( input, wrapper, name = 'public_uphoto_' ){
        this.input = input;
        this.wrapper = wrapper;
        this.name_input_part = name;
        this.selectFile();
    },

    selectFile: function() {
        var self = this;

        if (window.File && window.FileReader && window.FormData) {
            var $inputField = this.input, multiple = false;
            
            $($inputField).addEvent('change', function (e) {
                
                var __length, __k;
                __length = __k = e.target.files.length;
                if( __length > 1 ) multiple = true;

                for(var l = 0; l < __length; l += 1)
                {
                    var file = e.target.files[l];
                    if (file) {

                        if (/^image\//i.test(file.type)) {
                            self.readFile(file, multiple, __k);
                        } else {
                            alert('Not a valid image!');
                        }
                    }

                    if( l < __length ){
                        __k -= 1;
                    }
                }
            });
        } else {
            alert("File upload is not supported!");
        }
    },

    readFile: function(file, m, l){
        var self = this,
            reader = new FileReader();

        reader.onloadend = function () {
            self.createThumb(reader.result, file.type, m, l);
        };

        reader.onerror = function () {
            alert('There was an error reading the file!');
        }

        reader.readAsDataURL(file);
    },

    createThumb: function(dataURL, fileType, m, l){
        var self = this,
            __is_arr = '',
            image = new Image();

        if( m ){
            __is_arr = '[]';
        }

        image.src = dataURL;

        var el = new Element('div.imgs').setStyle('background', 'url(' + dataURL + ') center center / cover no-repeat');

        var input_1 = new Element('input', {
            value: 'upload',
            name: 'a',
            type: 'hidden'
        });

        var _clone = document.getElementById( this.input ).cloneNode(true);
        _clone.value = null;
        _clone.innerHTML = _clone.innerHTML;
        _clone.inject( $$('label.' + this.name_input_part)[0] );


        var __input = $( this.input ).set('name', this.name_input_part + this.order + __is_arr).setStyle('display', 'none');
        //var __input = _clone.set('name', this.name_input_part + this.order).setStyle('display', 'none');
        var remove = new Element('span.rem-photo').addEvent('click', function(){
            this.getParent('.imgs').destroy();
        });

        this.order += 1;

        __input.removeAttribute('id');
        __input.inject(el);
        input_1.inject(el);
        remove.inject(el);

        $(el.inject( this.wrapper ));
        $(this.input).set('value', '');
        
        self.init(self.input, self.wrapper, self.name_input_part);
    },



};
