var Controls = {};


Controls.Tip = new Class({
	Implements: [Options, Events],

	options: {
		
	},

	initialize: function () {
		this.render();
		this.bind = {
			enter: this.mouseenter.bindWithEvent(this),
			leave: this.mouseleave.bindWithEvent(this)
		};
	},

	render: function () {
		this.tip = new Element('div', { 'class': 'cx-tip' }).inject($(document.body));
	},

	attach: function (el) {
		el.addEvents({
			mouseenter: this.bind.enter,
			mouseleave: this.bind.leave
		});
//		this.mouseenter({ target: el });
	},

	mouseenter: function (e) {
		var l = this.el = e.target;
		var pos = this.el.getPosition($(document.body));

		var t = this.tip;
		if ( ! l.retrieve('tip') ) {
			l.store('tip', l.get('title')).set('title', '');
		}
		
		t.set('html', l.retrieve('tip'));
		t.setStyles({
			left: pos.x + 22,
			top: pos.y - 22,
			display: 'block'
		});
	},

	mouseleave: function (e) {
		this.tip.setStyle('display', 'none');
	}
});

Controls.LngPicker = new Class({
	Implements: [Options, Events],

	options: {

	},

	el: null,
	langs: [],

	initialize: function (el, options) {
		this.setOptions(options || {});
		
		this.el = el;
		this.langs = this.el.getElements('span');
		
		this.bind = {
			click: this.click.bindWithEvent(this)
		};

		this.attach();
		
		this.click({}, this.langs[0]);
	},

	attach: function () {
		this.langs.each(function (lng) {
			lng.addEvent('click', this.bind.click.bindWithEvent(this, [lng]));
		}.bind(this));
	},

	click: function (e, el) {
		this.langs.removeClass('active');
		el.addClass('active');

		this.fireEvent('change', [el]);
	}
});

Controls.SelectBox = new Class({
	Implements: [Options, Events],

	options: {
		disabled: false,
		name: '',
		values: [
			{ value: 1, label: 'EUR' },
			{ value: 2, label: 'USD' },
			{ value: 3, label: 'GBP' },
			{ value: 4, label: 'CHF' }
		],
		selected: 1
	},

	element: {},
	list: {},
	input: {},

	selected: {},

	disable: function () {
		this.element.addClass('cx-selectbox-disabled');
	},

	enable: function () {
		this.element.removeClass('cx-selectbox-disabled');
	},

	getValue: function (value) {
		var found = false;
		this.options.values.each(function (i) {
			if ( found ) return;
			if ( value == i.value ) {
				found = i;
			}
		});

		return found;
	},

	initialize: function (o) {
		this.setOptions(o || {});
		
		this.render();

		this.bind = {
			mouseenter: this.onMouseEnter.bindWithEvent(this),
			mouseleave: this.onMouseLeave.bindWithEvent(this),
			click: this.onClick.bindWithEvent(this),
			select: this.onSelect.bindWithEvent(this),
			hide: this.onHide.bindWithEvent(this)
		};

		this.attach();

		if ( this.options.disabled ) {
			this.disable();
		}
	},

	render: function () {
		this.element = new Element('span', {
			'class': 'cx-selectbox'
		});

		this.current = new Element('em', {
			html: this.getValue(this.options.selected).label
		}).inject(this.element);

		this.list = new Element('div', {
			'class': 'cx-selectbox-list hide'
		}).inject(this.element);

		this.options.values.each(function (i, j) {
			var el = new Element('div', {
				html: i.label
			}).inject(this.list);
			el.store('value', i);
			if ( j == 0 ) {
				el.addClass('selected');
			}
		}.bind(this));

		this.input = new Element('input', {
			type: 'hidden',
			name: this.options.name,
			value: this.getValue(this.options.selected).value
		}).inject(this.element);
	},

	onMouseEnter: function (e) {

	},

	onMouseLeave: function (e) {

	},

	onClick: function (e) {
		if ( this.element.hasClass('cx-selectbox-disabled') ) return;
		e.stop();
		if ( this.list.hasClass('hide') ) {
			this.element.addClass('cx-selectbox-pushed');
			this.list.removeClass('hide');
			$(document.body).addEvent('click', this.bind.hide);
		}
		else {
			this.onHide({});
		}
	},

	onSelect: function (e) {
		this.list.getElements('div').removeClass('selected');
		this.select(e.target);
	},

	onHide: function (e) {
		this.element.removeClass('cx-selectbox-pushed');
		this.list.addClass('hide');
		$(document.body).removeEvent('click', this.bind.hide);
	},

	attach: function () {
		this.element.addEvents({
			mouseenter: this.bind.mouseenter,
			mouseleave: this.bind.mouseleave,
			click: this.bind.click
		});

		this.list.getElements('div').addEvents({
			click: this.bind.select
		});
	},

	select: function (el) {
		var value = el.retrieve('value');
		this.input.set('value', value.value);
		this.current.set('html', value.label);
		this.selected = value;
		el.addClass('selected');
	},

	getElement: function () {
		return this.element;
	},

	privateArea: function () {
		window.location.href = '.';
	}
});

document.addEvent('domready', function () {
	var tip = new Controls.Tip;

	$$('.form span.tip').each(function (el) {
		tip.attach(el);
	});

	var maskable = $$('.cbox-ii .gray-box');
	if ( ! maskable.length ) {
		maskable = $$('.agency-logo-wrapper');
	}

	if ( maskable.length ) {
		Navigation.overlay = new Cubix.Overlay(maskable[0], {
			offset: {
				left: -1,
				top: -1
			}
		});
	}
	Locker.init();
	Locker.go();
	Navigation.init();
	FormErrors.show();
	Popups.init();

});

var Navigation = {
	form: null,
	step: null,
	then: null,
	changed: false,
	initial: null,
	
	init: function () {
		
		if (!$defined($$('.form')[0]))
			return;
		this.form = $$('.form')[0];
		this.step = this.form.getElement('input[name=step]');
		this.then = this.form.getElement('input[name=then]');
		this.dont = this.form.getElement('input[name=dont]');
		

		this.initial = this.form.toQueryString();

		this.overlay = Navigation.overlay ? Navigation.overlay : new Cubix.Overlay(this.form.getParent('.cbox-ii').getFirst('.gray-box'), {
			offset: {
				left: -1,
				top: -1
			}
		});
	},

	enable: function () {
		this.overlay.enable();
	},

	disable: function () {
		this.overlay.disable();
	},

	back: function () {
		this.overlay.disable();
		this.then.set('value', ':back');
		this.form.submit();
		return false;
	},

	next: function () {
		this.overlay.disable();
		this.then.set('value', ':next');
		this.form.submit();
		return false;
	},

	go: function (step) {
		this.overlay.disable();
		this.then.set('value', step);
		this.form.submit();
		return false;
	},

	reload: function () {
		this.overlay.disable();
		this.dont.set('value', 1);
		this.form.submit();
		return false;
	},

	update: function () {
		if ( typeof(tineMCE) != "undefined" ) tinyMCE.triggerSave();
		this.overlay.disable();
		this.form.submit();
		return false;
	},

	reset: function () {
		this.overlay.disable();
		window.location.href = window.location.href;
		return false;
	},

	tab: function (tab) {
		if ( ! this.hasChanged() && ! new Hash(FormErrors.errors).getLength() ) {
			this.overlay.disable();
			return true;
		}

		if ( ! confirm('If you continue, all the changes you\'ve made will be lost!\n\nClick "Cancel" to stay on this page and click update to save current tab') ) {
			return false;
		}
		
		this.overlay.disable();
		return true;
	},

	privateArea: function () {
		window.location.href = '.';
	},

	hasChanged: function () {
		return this.form.toQueryString() != this.initial;
	}
};

var FormErrors = {
	errors: {},
	
	init: function (errors) {
		this.errors = errors || {};
	},
	
	show: function () {
		for ( var field in this.errors ) {
			var el = $$('*[name=' + field + ']')[0];
			if ( ! el ) continue;
			var inner = el.getParent('.inner');
			var clear = inner.getLast('.clear');
			new Element('div', { 'class': 'error', html: this.errors[field] }).inject(clear ? clear : inner, clear ? 'before' : 'bottom');
		}
	}
};

var CamInfoPopup = {};
CamInfoPopup.open = function() {
	if ($('CamInfoPopup')) {
		$('CamInfoPopup').setStyles({
			display: 'block',
			opacity: 0
		}).fade(1);
	}
};
CamInfoPopup.close = function() {
	if($('CamInfoPopup')){
		$('CamInfoPopup').setStyles({
			display: 'none',
			opacity: 1
		}).fade(0.7);
	}
};

var Locker = {
	cHeight:0,
	rHeight:0,
	cStatus:0,
	rStatus:0,
	
	init:function(){
		/* COMMENT */
		var commentBox = $('my-comments');
		
		if($defined(commentBox)){
			this.cHeight = commentBox.getStyle('height');
		
			cStatus = Cookie.read('comment_locker_status');
			
			if(cStatus == 1){
				this.cStatus = cStatus;
				commentBox.getPrevious('.locker').addClass('closed');
				commentBox.setStyle('height',0);
			}
		}
		
		/* REVIEW  */
		var reviewBox = $('my-reviews');
		
		if($defined(reviewBox)){
			this.rHeight = reviewBox.getStyle('height');
			
			rStatus = Cookie.read('review_locker_status');
			if(rStatus == 1){
				this.rStatus = rStatus;
				reviewBox.getPrevious('.locker').addClass('closed');
				reviewBox.setStyle('height',0);
			}
		}
	},
		
	go: function(){
		var self = this;
		$$('.locker').addEvent('click', function(){
			var box = this.getNext('div');
			var locker = this;
			var height, autoHeight, cookieName;
			
			/* COMMENT */
			if(box.get('id') == "my-comments"){
				autoHeight = self.cHeight;
				cookieName = 'comment_locker_status';
			}
			
			/* REVIEW */
			if(box.get('id') == "my-reviews"){
				autoHeight = self.rHeight;
				cookieName = 'review_locker_status';
				
			}
			
			
			if(locker.hasClass('closed')){
				height = autoHeight;
				locker.removeClass('closed');
				Cookie.write(cookieName, 0, {duration: 365});
			}
			else{
				height = 0;
				locker.addClass('closed');
				Cookie.write(cookieName, 1, {duration: 365});
			}
			var effect = new Fx.Tween(box,  {
							duration: 1000,
							transition:  Fx.Transitions.Quart.easeInOut
							
			});
			//console.log(height);
			effect.start('height', height);
		});
	}

};

var Geography = {
	loadCities: function (city, country, city_id, hidden) {

		if ( ! $defined(hidden) ) {
			hidden = false;
		}

		var selected;
		if ( hidden ) {
			selected = country.get('value');
		}
		else {
			selected = country.getSelected();
		}
		
		if ( ! selected.length ) return;
		if ( ! hidden ) {
			var country_id = selected[0].get('value');
		}
		else {
			var country_id = selected;
		}

		if ( ! country_id ) {
			city.empty();
			new Element('option', { value: '', html: '- city -' }).inject(city);
			return;
		}

		$$(country, city).set('disabled', 'disabled');

		new Request({
			url: '/' + Cubix.Lang.id + '/geography/ajax-get-cities',
			data: { country_id: country_id },
			method: 'get',
			onSuccess: function (resp) {
				resp = JSON.decode(resp, true);
				if ( ! resp ) return;
				city.empty();
				new Element('option', { value: '', html: '- city -' }).inject(city);
				
				resp.data.each(function (c) {
					new Element('option', {
						value: c.id,
						html: c.title + '&nbsp;&nbsp;',
						selected: city_id == c.id ? 'selected' : null
					}).inject(city);
				});
				$$(country, city).set('disabled', null);
			}
		}).send();
	}
};

var changeSystem = function (sys) {
	$$('input[name=measure_units]').set('value', sys);
	return Navigation.reload();
};

var LoadEscortComments = function (params) {

		var comments_container = $('ajax-escort-comments');
		$('my-comments').setStyle('height','');

		var overlay = new Cubix.Overlay(comments_container, {
			loader: _st('loader-small.gif'),
			position: '50%'
		});
		overlay.disable();

		new Request({
			url:  "/private-v2/ajax-escort-comments",
			method: 'post',
			evalScripts: true,
			data: params,
			onSuccess: function (resp) {
				comments_container.set('html','');
				comments_container.set('html',resp);
				Locker.cHeight = $('my-comments').getStyle('height');
			}
		}).send();

		return false;
};

var LoadEscortReviews = function (params) {

		var reviews_container = $('ajax-escort-reviews');
		$('my-reviews').setStyle('height','');

		var overlay = new Cubix.Overlay(reviews_container, {
			loader: _st('loader-small.gif'),
			position: '50%'
		});
		overlay.disable();

		new Request({
			url:  "/private-v2/ajax-escort-reviews",
			method: 'post',
			evalScripts: true,
			data: params,
			onSuccess: function (resp) {
				reviews_container.set('html','');
				reviews_container.set('html',resp);
				Locker.rHeight = $('my-reviews').getStyle('height');

			}
		}).send();

		return false;
};

var Popups = {
	init: function () {
		if($$('.icon--popup')) {
			$$('.icon--popup').each(function (el) {
				el.addEvent('click', function() {

					var target = el.get('data-target');

					if($$(target)) {
						$$(target).setStyles({
							display: 'flex',
							opacity: 0
						}).fade(1);
					}else{
						console.error(target, 'NOT FOUND');
					}
				})
			});
		}

		if($$('.skype-call-discover-popup .popup-footer button.dismiss')) {
			$$('.skype-call-discover-popup .popup-footer button.dismiss').addEvent('click', function() {
				var target = $(this).get('data-target');

				if($$(target)) {
					$$(target).setStyles({
						display: 'none',
						opacity: 0
					}).fade(0.5);
				}else{
					console.error(target, 'NOT FOUND');
				}
			})
		}
	},
};

var MError = {};

MError.ErrorsHandler = {
      elm: null,

    messages: {
        'have_errors': 'Exists error !',
        'city_count': 'City count',
        'city_count_test': 'City count',
        'year': 'Year !',
        'month': 'Month must be between 1 and 12',
        'day': 'Day must be between 1 and 31',
        'day_31': 'Self month doesn\'t have 31 days!',
        'day_feb': 'February doesn\'t have that many days!',
        'date_out': 'Please select currect date'
    },

    doShow: function( elm, key, dict ){
        var self = this;

        if( !dict ) {
            elm.set('html', self.messages[key]).show();
        } else {        	
            elm.set('html', key).show();
        }
        setTimeout(function() {
        	elm.set('html', '').hide();
        }, 3000 );
    },

    doHide: function(elm){
        elm.set('html', '').hide();
    }
};

    /**
    * Creates a modal singleton with the given content
    * @param {String} content
    * @param {String} url
    * @returns {String} Html element
    */

var MCubix = MCubix || {};

MCubix.Modal = (function() {

    var instance;

    function getInstance() {
        if (!instance){
            instance = init();
        }

        return instance;
    }

    function init() {
        var _modal,
        	_el = document.body,
        	_options = {
        		content: '',
        		url: '/',
        		cookieKey: ''
        	};

        function _mapOptions(data) {
        	var options = data || {};
        	
        	for (var option in options) {
				if (options[option]) {
					_options[option] = options[option];
				}
			}

        }

        function create(options) {

        	_mapOptions(options);

        	var overlay = new Element('div', {
        		id: 'modalBannerOverlay'
        	});

        	var wrapper = new Element('div', {
        		id: 'modalBanner'
        	});

        	overlay.adopt(wrapper);

            var inner = new Element('<div>', {
                html: '<a href="' + url + '">' + content + '</a>',
                class: 'responsive-support'
            });

            wrapper.adopt(inner);

        	var closeBtn = new Element('span', {

        		class: 'close-btn',
        		html: '&times;'

        	}).addEvent('click', function(e) {
        		e.stop();

        		_hide();
        	});

            wrapper.adopt(closeBtn);

            _modal = overlay;

            return this;

        }

        function _hide() {
        	var key = _options.cookieKey;
        	console.log(key);

        	if (key) {
        		Cookie.write(key, 0);
        	}

        	if (_modal instanceof HTMLElement) {
        		_modal.getElement('#modalBanner').removeClass('bounceIn').addClass('bounceOut');
        		setTimeout(function() {
        			_modal.hide();
        		}, 700);
        	}
        }

        function add() {
        	var key = _options.cookieKey;

        	if (key) {
        		if (Cookie.read(key) || Cookie.read(key) === 0) {
        			return false;
        		}
        	}

            if (_modal instanceof HTMLElement) {
                _el.adopt(_modal);
                _modal.getElement('#modalBanner').addClass('animated bounceIn');
            }

            return this;
        }

        return {
            create: create,
            show: add
        };
    }


    return {
        getInstance: getInstance
    };
})();
