/**
 * Votd constructor
 */

;(function(global) {

	'use strict';

	var Votd = function(el) {
		return this.init(el);
	};

	Votd.prototype = {

		// defaults
		votdUrl: '/online-billing/escort-votd',
		calendarUrl: '/online-billing/escort-votd-calendar',
		paymentUrl: '/online-billing/payment',
		cartUrl: '/online-billing/set-cart',


		votdPopup: null,
		calendarPopup: null,

		currentDates: [],
		savedDates: [],
		availDates: [],

		votdData: {},

		init: function(el) {
			this.el = el;

			this
				.setAvailDates()
				.initVotdPopup()
				.initCalendar()
				.bindEvents();
		},

		initVotdPopup: function() {

			if (this.votdPopup) return false;

			var 
				_self = this,
				overlay = this.buildOverlay(),
				popup = this.buildPopup({
					fn: function() {
						_self.setVotdData();
						_self.render();
						_self.votdPopup.hide();
					}
				});

			popup.inject(overlay);
			overlay.inject(this.el);

			new Request({
				url: this.votdUrl,
				method: 'GET',

				onSuccess: function(respStr) {
					popup
						.getElement('.inner-content')
						.set('html', respStr);
					
					_self.toggleBtns(
						popup.getElement('#votdcalendarBtn'),
						false
					);

					_self.votdPopup = overlay;
					global.fireEvent('popupLoaded');

				}
			}).send();

			return this;
		
		},

		initCalendar: function() {
			if (this.calendarPopup) return false;

			var 
				_self = this,
				overlay = this.buildOverlay(),

				popup = this.buildPopup({
					fn: function(){
						_self.calendarPopup.hide();
					}
				}),

				data = {
					available_dates: _self.availDates
				};

			popup.inject(overlay);
			overlay.inject(this.el);


			new Request({
				url: this.calendarUrl,
				method: 'GET',
				data: data,

				onSuccess: function(respStr) {
					popup
						.getElement('.inner-content')
						.set('html', respStr);

					_self.calendarPopup = overlay;

					global.fireEvent('calendarLoaded');
					global.fireEvent('popupLoaded');
				}
			}).send();

			return this;
		
		},

		requireCalendar: function(data) {
			var 
				areaId = this.areaId,
				wrap = this.calendarPopup.getElement('.ob-votd-calendar-wrapper'),
				calendar = this.calendarPopup.getElement('.escort-votd-calendar'),
				availDates = this.availDates,
				currentDates = this.currentDates,
				savedDates = this.savedDates,

				_data = {
					area_id: areaId,
					available_dates: availDates,
					selected_dates: currentDates,
					preselected_dates: savedDates
				};

			Object.append(data, _data);

			new Request({
				url: this.calendarUrl,
				method: 'GET',
				data: data,

				onLoadstart: function() {
					$$('.ob-votd-calendar-overlay')[0].addClass('visible');
				},

				onSuccess: function(respStr) {
					console.log(wrap);
					wrap.destroy();
					Elements.from(respStr).inject(calendar, 'before');

					global.fireEvent('calendarLoaded');
				},

				onComplete: function() {
					$$('.ob-votd-calendar-overlay')[0].removeClass('visible');
				}
					
			}).send();			

		},

		buildOverlay: function() {

			var _overlay = new Element('div', {
				class: 'mobile-ob-overlay',
				styles: {
					position: 'fixed',
					height: '100%',
					width: '100%',
					top: '0',
					left: '0',
					background: 'rgba(0,0,0,0.5)',
					zIndex : 1001,
					visibility: 'hidden',
					opacity: '0'
				}
			}).addEvent('mousewheel', function(e){
					e.stop();
				}
			);

			Object.merge(_overlay, {
				hide: this.hide,
				show: this.show
			});
			

			return _overlay;

		},

		buildPopup: function(options) {

			if (!options) options = {};

			var 
				_self = this,
				_width = parseInt(options.width) || 320,
				_height = parseInt(options.height) || 500,
				_content = options.content || null,
				_text = options.txt || 'OK',
				_actionFn = options.fn || function (){
					return false;
				},

				_outer = new Element('div', {
					styles: {
						display: 'table',
						position: 'absolute',
						height: '100%',
						width: '100%',
					}
				}),

				_middle = new Element('div', {
					styles: {
						display: 'table-cell',
						verticalAlign: 'middle'
					}
				}),

				_inner = new Element('div', {
					class: 'inner',
					styles: {
						position: 'relative',
						marginLeft: 'auto',
						marginRight: 'auto',
						width: _width + 'px',
						height: _height + 'px',
						borderRadius: '3px',
						backgroundColor: '#ffffff'
					},

					html: _content
				}),

				_innerContent = new Element('div', {
					class: 'inner-content',
					styles: {
						marginBottom: '60px',
						boxSizing: 'border-box',
					}
				});

			_innerContent.inject(_inner);
			_inner.inject(_middle);
			_middle.inject(_outer);

			var 
				_actionBtn = new Element('a', {
					class: 'ok-btn',
					styles: {
						display: 'block',
						position: 'absolute',
						width: '100%',
						height: '60px',
						backgroundColor: '#1a5cbf',
						bottom: 0,
						left: 0,
						color: '#ffffff',
						textTransform: 'uppercase',
						textDecoration: 'none',
		    			fontSize: '20px',
						fontWeight: 'bold',
						textAlign: 'center',
						lineHeight: '60px',
						borderRadius: '0 0 3px 3px'
					},

					html: _text,
					href: '#'
				}).addEvent('click', _actionFn),

				_closeBtn = new Element('a', {
					class: 'close-btn',
					styles: {
						display: 'block',
						width: '20px',
						height: '20px',
						position: 'absolute',
						top: 0,
						right: 0,
						lineHeight: '20px',
						textAlign: 'center',
						textDecoration: 'none',
						fontSize: '20px',
						fontWeight: 'bold'
					},

					html: '&times;'
				}).addEvent('click', function() {
					this.getParents('.mobile-ob-overlay')[0].hide();
				});

			_closeBtn.inject(_inner);
			_actionBtn.inject(_inner);

			return _outer;
		
		},

		bindEvents: function() {

			var 
				_self = this,
				votdBtn = this.el.getElement('#votdBtn'),
				nextBtn = this.el.getElement('.next-step-button'),
				waitOn = 2;

			global.addEvent('popupLoaded', function() {
				if (--waitOn === 0) {
					global.fireEvent('popupsLoaded');
				}
			});

			global.addEvent('popupsLoaded', function() {
				
				// Main button click event
				votdBtn.addEvent('click', function(e) {
					e.stop();

					_self.votdPopup.show();
				});

				var 
					popup = _self.votdPopup,
					calendar = _self.calendarPopup,
					areas = popup.getElement('.additional-areas'),

					locationBtn = popup.getElement('#addVotdLocation'),
					calendarBtn = popup.getElement('#votdcalendarBtn'),
					calendarCloseBtn = calendar.getElement('.close-btn');

				// Areas changing event				
				areas.addEvent('change', function() {
					var areaId = this.get('value');

					if (!areaId) {
						_self.areaId = 0;
						_self.toggleBtns([calendarBtn, locationBtn], false);
					} else {
						_self.areaId = areaId;
						_self.toggleBtns([calendarBtn, locationBtn], true);
					}

				});					

				// Calendar showing button event
				calendarBtn.addEvent('click', function(e) {
					e.stop();
					
					var data = {
						area_id: _self.areaId,
						available_dates: _self.availDates,
						selected_dates: _self.currentDates,
						preselected_dates: _self.savedDates
					};

					_self.requireCalendar(data);
					_self.calendarPopup.show();
				});

				// Calendar close button event enhancement
				calendarCloseBtn.addEvent('click', function() {
					calendar
						.getElements('tbody td.package time.selected')
						.fireEvent('click');

					_self.currentDates = [];
				});

				// Location adding button event
				locationBtn.addEvent('click', function(e) {
					if (e) e.stop();


					if (!_self.currentDates.length) return false;

					var 
						areaEl = areas.getSelected(),
						areaId = areaEl.get('value'),
						areaTitle = areaEl.get('html'),
						wrapper = popup.getElement('#selectedVotdAreas'),
						closeBtn = popup.getElement('.close-btn');

					if (!areaId) return false;

					areaEl.hide();
					areas.set('value', 0);
					areas.fireEvent('change');

					_self.currentDates.each(function(el) {
						var 
							_areaContainer = new Element('div', {
								class: 'area-container',
								styles: {
									width: '100%',
									height: '20px',
									float: 'left',
									marginTop: '10px'
								}
							}),

							_areaTitle = new Element('span', {
								styles: {
									display: 'block',
									width: 'calc(100% - 20px)',
									height: '20px',
									overflow: 'hidden',
									textOverflow: 'ellipsis',
									whiteSpace: 'nowrap',
									float: 'left'
								},

								html: '<strong>' + areaTitle + '</strong> : ' + el
							}),

							_removeBtn = new Element('a', {
								'data-id': areaId,
								class: 'additional-area-remove-btn',

								styles: {
									display: 'block',
									width: '20px',
									height: '20px',
									textAlign: 'center',
									lineHeight: '20px',
									fontSize: '20px',
									color: '#ffffff',
									float: 'left',
									textDecoration: 'none',
									borderRadius: '3px',
									backgroundColor: '#ac2767'
								},

								html: '&times;'
							});

						_removeBtn.addEvent('click', function(e) {
							if (e) e.stop();

							var 
								_id = this.get('data-id'),
								_el = this.getParent('.area-container'),
								_date = _el.getElement('span').get('html').split(' : ')[1];

							_el.destroy();

							_self.savedDates.erase(_date);

							if (!wrapper.getElements('[data-id="' + _id + '"]').length) {
								areas.getElements('[value="' + _id + '"]').show();
							}
						});

						_areaContainer.adopt(_areaTitle, _removeBtn);
						_areaContainer.inject(wrapper);
					});

					_self.savedDates.combine(_self.currentDates);
					_self.calendarPopup.getElement('.close-btn').fireEvent('click');
				});
			});

			global.addEvent('calendarLoaded', function() {

				var 
					calendar = _self.calendarPopup,
					calendarNavs = calendar.getElements('thead a'),
					days = calendar.getElements('tbody td.package time');

				// Calendar navigation buttons event
				calendarNavs.addEvent('click', function(e) {
					e.stop();

					var data = {
						calendar_month: this.get('data-month')
					};

					_self.requireCalendar(data);
				});

				// Calendar date selection
				days.addEvent('click', function() {
					var el = this,
						wrap = this.getParent('td');

					if (wrap.hasClass('preselected') || wrap.hasClass('booked')) {
						return false;
					}

					if (el.hasClass('selected') || wrap.hasClass('selected')) {
						var className = 'booked-' + el.get('datetime');

						el.removeClass('selected');
						wrap.removeClass('selected');

						calendar.getElement('.' + className).destroy();

						_self.currentDates.erase(el.get('datetime'));

						return false;

					}

					el.addClass('selected');

					var 
						selectedDate = el.get('datetime'),
						
						dateEl = new Element('span', {
							class: 'booked-' + selectedDate,

							styles: {
								display: 'inline-block',
								width: '57px',
								backgroundColor: '#ac2767',
								color: '#ffffff',
								padding: '5px',
								borderRadius: '3px',
								margin: '2px 3px',
								fontSize: '10px',
								textAlign: 'center'
							},

							html: selectedDate
						});

					dateEl.addEvent('click', function() {
						var 
							day = this,
							dayTxt = day.get('html');

						calendar.getElements('time').each(function(el) {
							if (el.get('datetime') === dayTxt) {
								el.removeClass('selected');
								el.getParent('td').removeClass('selected');
							}
						});

						_self.currentDates.erase(dayTxt);
						day.destroy();

					});
					_self.currentDates.include(selectedDate);
					dateEl.inject($('selectedVotdDates'));
				});
			});

			global.addEvent('dataLoaded', function() {
				if (!_self.votdData.votds.length) {
					_self.toggleBtns($$('.next-step-button'), false);
					
				} else {
					_self.toggleBtns($$('.next-step-button'), true);
				}
			});

			nextBtn.addEvent('click', function(e) {
				e.stop();
				if (!_self.votdData.votds || !_self.votdData.votds.length) return false;
				new Request({
					url: _self.cartUrl,
					method: 'POST',
					data: {
						cart: _self.votdData
					},

					onSuccess: function(respStr) {
						if (JSON.decode(respStr).success == true) {
							global.location.href = _self.paymentUrl;
						}
					}
				}).send();
			});
		
		},

		setVotdData: function() {
			var 
				dataHtml = $('selectedVotdAreas')
				.getElements('.area-container');

			if (!dataHtml) {
				this.votdData = {};
				global.fireEvent('dataLoaded');

				return false;
			}
			
			
			var	votds = [];
			
			dataHtml.each(function(el) {
				var
					d = el.getElement('span').get('html').split(' : ')[1],
					a = el.getElement('a').get('data-id');

				votds.include({
					a: a,
					d: d
				});
			});

			this.votdData.votds = votds;
			this.votdData.escort_id = JSON.decode(user).escort_data.escort_id;

			global.fireEvent('dataLoaded');

		},

		render: function() {
			var 
				wrap = this.el.getElement('.selected-items'),
				countHtml = this.votdData.votds.length +
					' item(s) = CHF ' +
					this.votdData.votds.length * 90;

			wrap.set('html', '');

			this.el.getElement('.count-amount').set('html', countHtml);
		},

		setAvailDates: function() {
			this.availDates = $('availDates')
				.get('value')
				.split(',')
				.filter(function(el) {
					return Date.parse(el) >= new Date().clearTime();
				});

			return this;

		},

		toggleBtns: function(btns, active) {

			var btnList = (!btns.length || btns.length < 2) ? [btns] : btns;

			btnList.each(function(btn) {
				if (active) {
					btn.addClass('active');
				} else {
					btn.removeClass('active');
				}
			});

		},

		show: function() {
			this.getElement('.inner')
				.removeClass('animated bounceOut')
				.addClass('animated bounceIn');

			this.fade('in');

		},

		hide: function() {
			this.getElement('.inner')
				.removeClass('animated bounceIn')
				.addClass('animated bounceOut');
			
			this.fade('out');

		}
	};

	if (!global.Votd) global.Votd = Votd;

})(window);