Cubix.VCPopup = {};

Cubix.VCPopup.inProcess = false;

Cubix.VCPopup.url = '';

Cubix.VCPopup.Show = function (box_height, box_width) {
	if ( Cubix.VCPopup.inProcess ) return false;

	

	var container = new Element('div', { 'id': 'video-chat-popup'}).inject(document.body);
	
	Cubix.VCPopup.inProcess = true;

	new Request({
		url: Cubix.VCPopup.url,
		method: 'get',
		onSuccess: function (resp) {
			
			Cubix.VCPopup.inProcess = false;
			container.set('html', resp);

			var close_btn = $$('#video-chat-popup .white-btn')[0];
			
			close_btn.addEvent('click', function() {
				$('video-chat-popup').destroy();
				
			});
			
			var forms = container.getElements('form');
			forms.addEvent('submit', Cubix.VCPopup.Send);
			
			container.tween('opacity', '1');
			
			
		}
	}).send();
		
	
	return false;
}

Cubix.VCPopup.Send = function (e) {
	e.stop();
	   
	var overlay = new Cubix.Overlay($('video-chat-popup'), { loader: _st('loader-small.gif'), position: '50%', no_relative: true });
	overlay.disable();

	this.set('send', {
		onSuccess: function (resp) {
			$('video-chat-popup').destroy();
			resp = JSON.decode(resp);
			if(resp.status == 'twispay_success'){
				var formHtml = resp.form;
				$$('#header')[0].set('html', $$('#header')[0].get('html') + formHtml.replace(/ \"/g, "\""));
				$('twispay-payment-form').submit();
			}
			else{
				alert('Unexpected error please try later');
			}
			overlay.enable();
		}.bind(this)
	});

	this.send();
}



Cubix.Cam = {};
Cubix.Cam.Popup = {};
Cubix.Cam.Popup.inProcess = false;


Cubix.Cam.Popup.Show = function (box_height, box_width) {
	if ( Cubix.Cam.Popup.inProcess ) return false;

	var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000' });
	page_overlay.disable();
	$$('.overlay').setStyle('display', 'block');

	var container = new Element('div', { 'id': 'cam-popup'}).setStyles({
		left: window.getWidth() / 2 - box_width / 2,
		top:  window.getHeight() / 2 - box_height / 2,
		opacity: 0,
        position: 'fixed',
        'z-index': 101,
        background: '#fff'
	}).inject(document.body);
		
	Cubix.Cam.Popup.inProcess = true;
		
	new Request({
		url: Cubix.Cam.Popup.url,
		method: 'get',
		onSuccess: function (resp) {
			
			Cubix.Cam.Popup.inProcess = false;
			container.set('html', resp);
			var close_btn = new Element('span', {class: 'close-icon'}).inject(container);
					
			close_btn.addEvent('click', Cubix.Cam.Popup.Close);
			
			if($('go-signup-button')){
				$('go-signup-button').addEvent('click', function(){
					Cubix.Cam.Popup.Close();
					Cubix.Cam.Popup.url = '/and6-cams/signup/';
					Cubix.Cam.Popup.Show(475,375);
				});
			}
			
			var forms = container.getElements('form');
			forms.addEvent('submit', Cubix.Cam.Popup.Submit);
			
			container.tween('opacity', '1');
		}
	}).send();
		
	Cubix.Cam.Popup.Close = function () {
		$('cam-popup').destroy();
		page_overlay.enable();
		$$('overlay').destroy();
		$$('.overlay').setStyle('display', 'none');
	};
	
	return false;
};

Cubix.Cam.Popup.Submit = function (e) {
	e.stop();
	   
	var page_overlay = new Cubix.Overlay($('cam-popup'), { loader: _st('loader-small.gif'), position: '50%', no_relative: true });
	page_overlay.disable();
	
	this.set('send', {
		
		onSuccess: function (resp) {
			resp = JSON.decode(resp);
			
			if(resp.status == 'error'){
				page_overlay.enable();
				$$('#cam-popup .error-notes').destroy();
				$$('#cam-popup .req').removeClass('req');
				
				function getErrorElement(el) {
					var target = el;
					if ( el.get('name') == 'terms' ) {
						target = el.getNext('label');
					}
					return new Element('div', { 'class': 'error-notes' }).inject(target, 'after');
				}

				for ( var field in resp.msgs ) {
					var input = this.getElement('*[name=' + field + ']');
					input.addClass('req');
					getErrorElement(input).set('html', resp.msgs[field]);
				}
			}
			else if(resp.status == 'twispay_success'){
				var formHtml = resp.form;
				$$('#header')[0].set('html', $$('#header')[0].get('html') + formHtml.replace(/ \"/g, "\""));
				$('twispay-payment-form').submit();
			}
			else if(resp.status == 'payment_failure'){
				alert('Unexpected error please try later');
			}
			else if(resp.signup == true){
				window.location.href = '/private/signup-success';
				return;
			}
			else{
				window.location.reload();
				return;
			}
		}.bind(this)
	});

	this.send();
	Cubix.Cam.Popup.Close = function () {
		$('cam-popup').destroy();
		page_overlay.enable();
	};
};