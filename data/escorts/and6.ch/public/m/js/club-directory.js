var ScrollSpy = new Class({

	/* implements */
	Implements: [Options,Events],

	/* options */
	options: {
		container: window,
		max: 0,
		min: 0,
		mode: 'vertical'/*,
		onEnter: $empty,
		onLeave: $empty,
		onScroll: $empty,
		onTick: $empty
		*/
	},

	/* initialization */
	initialize: function(options) {
		/* set options */
		this.setOptions(options);
		this.container = document.id(this.options.container);
		this.enters = this.leaves = 0;
		this.max = this.options.max;
		this.inside = false;

		/* make it happen */
		this.addListener();
	},

	/* a method that does whatever you want */
	addListener: function() {
		/* state trackers */
		this.container.addEvent('scroll',function(e) {
			/* if it has reached the level */
			var position = this.container.getScroll(),
				xy = position[this.options.mode == 'vertical' ? 'y' : 'x'];
			/* if we reach the minimum and are still below the max... */
			if(xy >= this.options.min && (this.max == 0 || xy <= this.max)) {
					/* trigger Enter event if necessary */
					if(!this.inside) {
						/* record as inside */
						this.inside = true;
						this.enters++;
						/* fire enter event */
						this.fireEvent('enter',[position,this.enters,e]);
					}
					/* trigger the "tick", always */
					this.fireEvent('tick',[position,this.inside,this.enters,this.leaves,e]);
			}
			/* trigger leave */
			else if(this.inside){
				this.inside = false;
				this.leaves++;
				this.fireEvent('leave',[position,this.leaves,e]);
			}
			/* fire scroll event */
			this.fireEvent('scroll',[position,this.inside,this.enters,this.leaves,e]);
		}.bind(this));
	}
});


Escorts = {};

Escorts.data = {}; // Must be set from php
Escorts.container = '';

var finish = false;
var req = {'running':false};

Escorts.Load = function (url, container) {	
	
	req = new Request({
		url: url,
		method: 'get',
		//data: data,
		onSuccess: function (resp) {			
			container.set('html', container.get('html') + resp);
			
			Escorts.data = {'city': $$('input[name=m_city]')[0].get('value'), 'page': $$('input[name=m_page]').getLast().get('value')};
			
			finish = true; 
			
			$$('div.spinner').destroy();
		}.bind(this)
	}).send();
	
	return false;
}

Escorts.pageResize = function() {
	if ( navigator.userAgent.match(/Android/i) ) {
		if ( window.getSize().x < 400 ) {
			$('page').setStyle('width', 310);
		} else {				
			$('page').setStyle('width', 620);
		}
	} else if ( navigator.userAgent.match(/iPhone/i) ) {
		
	}
};

Escorts.scrollSpy = function() {
	new ScrollSpy({
		min: window.getScrollSize().y - window.getSize().y - 800,
		onScroll: function (p) {
			//alert(req.running + '---' + finish);
			if ( req.running  ) return;
			if ( p.y > window.getScrollSize().y - window.getSize().y - 800 ) {
				Escorts.data = {'page': $$('input[name=m_page]').getLast().get('value'), 'lat':$$('input[name=lat]')[0].get('value'), 'lon':$$('input[name=lon]')[0].get('value')};
				var url = '/escorts/club-directory/' + (parseInt(Escorts.data.page) + 1) + '?ajax=1&lat=' + Escorts.data.lat + '&lon=' + Escorts.data.lon;

				var spinner = getSpinner('Loading more escorts...');
				
				spinner.inject($('page_wrapper'), 'after');
				
				Escorts.Load(url, $('page_wrapper'));
				
				finish = false;
				//Escorts.req.running = false;
			}
		},
		container: window
	});
};

var getSpinner = function (msg) {
	var el = new Element('div', {'class': 'spinner'});
	new Element('span', {html: msg}).inject(el);
	return el;
};

var firstLoadEscorts = function(position) {
	var lat = 0;
	var lon = 0;
	if ( position ) {
		lat = position.coords.latitude;
		lon = position.coords.longitude;
	}
	
	Escorts.data = {'page': 1};
	var url = '/escorts/club-directory/' + parseInt(Escorts.data.page) + '?ajax=1&first=1&lat=' + lat + '&lon=' + lon;

	var spinner = getSpinner('Loading escorts...');

	spinner.inject($('page_wrapper'), 'after');

	Escorts.Load(url, $('page_wrapper'));
};

window.addEvent('domready', function() {
	
	navigator.geolocation.getCurrentPosition(firstLoadEscorts, firstLoadEscorts);
	
	setInterval('Escorts.pageResize()', 50);
	
	//Escorts.scrollSpy();
	
	if ( navigator.userAgent.match(/iPhone/i) ) {
		window.onpageshow = function(evt) {
			// If persisted then it is in the page cache, force a reload of the page.
			if (evt.persisted) {
			document.body.style.display = "none";
			location.reload();
			}
		};
	}

});