
var Video = this.VideoSection = new Class({

	Implements: [Events, Options],

	options: {
		contentWrapper: ".video-box-wrapper"
	},

	form : null,
    videoOverlay : null,
    videoPlaying: 0,
		
	initialize: function(form, options) {
		this.setOptions(options);
		
		this.videoOverlay = new Cubix.Overlay(
			document.getElement(this.options.contentWrapper), 
			{
				color: '#fdfdfd',
				opacity: 0.6,
				position: '55%',
				loader: '/img/photo-video/ajax_small.gif'
			}
		);

		this.form = document.id(form);
		this.initUpload(this.form);
		
		if(!$$('.no-video')[0]){
			this.initRemoveVideo();
			this.clickVideo();
		}
	},
	
	initUpload: function(form){
		
		var self = this;
		
		var myUpload = new MooUpload('filecontrol', {
			action: form.get('action'),
			accept: '.mov,.flv,.wmv,.mp4*',
			method: 'html5', // Automatic upload method (Choose the best)
			multiple: false,
			deleteBtn: true,

			onAddFiles: function(){
				var items = $$('#filecontrol_listView .item');
				if(items.length > 1){
					var close = items[0].getElement('.delete');
					close.fireEvent('click', {
						stop: function(){}
					});
				}
			},

			onBeforeUpload: function(){

				form.getElement('#filecontrol_btnbStart').addClass('none').fade('out');
				form.getElement('#filecontrol_btnAddfile').addClass('none').fade('out');
				form.getElement('.progresscont').removeClass('none').fade('in');

				$$('#not88').set('html', 'onBeforeUpload');
			},
			onSelectError: function(){
				alert('error occured');
			},
			onFileUpload: function(fileindex, response){
				
				if(response.error === 0 && response.wait_for_video){
					self.videoOverlay.disable();
					self.isVideoReady();				
				}
			},

			onFileUploadError: function(filenum, response){
				$$('.mooupload_error').appendText(response.error);
			},

			onFinishUpload: function() {
				setTimeout(function() {
					var fxProg = new Fx.Tween(form.getElement('.progresscont'),{duration: 2000});
					fxProg.addEvent('complete', function(){this.element.addClass('none'); });
					fxProg.start('opacity','100','0');
					//fxProg.removeEvents('complete');
				}, 2000);
			}
		});
	},

	isVideoReady : function(){
		var self = this;
		var readyUrl = $('is-video-ready-url').get('value');
		setTimeout(function() {
			new Request.JSON({
				url: readyUrl,
				method: 'get',
				onSuccess: function (resp) {
					if(resp.status === 0){
						self.isVideoReady();
					}
					else{
						self.initSuccess(resp);
					}
				}
			}).send();
		}, 2000);
	},
	
	initSuccess : function(response){
		var item = $$('#filecontrol_listView .mooupload_readonly')[0];
		item.destroy();

		if ( $$('.no-video').length ) {
			$$('.no-video').destroy();
		}

		$('filecontrol_btnDelete').removeClass('none');
		var container = $$('.video-box')[0];
		
		var div = new Element('div', {
			'id': 'video_' + response.video_id,
			'class': 'wrapper'
		}).inject(container, 'top');

		var input_hidden = new Element('input', {
			'type':'hidden',
			'id':'video-config',
			'value': response.vod
		}).inject(div, 'top');


		var divImage = new Element('div', {
			'styles': {
				'background-image': 'url("' + response.photo_url + '")'
			},
			'class': 'video-image-container',
			'data-height' : response.video_height,
			'data-width' : response.video_width,
			'rel' : response.video
		}).inject(div, 'top');

		var strong = new Element('strong', {
			'html': 'Pending' 
		}).inject(div, 'top');

		new Element('span', {
			'class': 'video_play_btn'
		}).inject(div, 'bottom');

		this.clickVideo();
		this.initRemoveVideo();
		this.videoOverlay.enable();
	},
	
	initRemoveVideo : function() {
		self = this;
		$$('#form-upload .delete').removeClass('none').addEvent('click', function(e){
			e.stop();
			self.videoOverlay.disable();
			var videoBox = $$('.video-box')[0];
			var videoWrap = videoBox.getElement('.wrapper');
			var url = $('video-remove-url').get('value');
			new Request({
				url: url,
				method: 'get',
				data: {video_id: videoWrap.get('id').replace('video_' , '')},
				onSuccess: function (resp) {
					videoWrap.destroy();
					new Element('div', {'class' : "no-video" }).inject(videoBox, 'top');
					this.addClass('none');
					this.removeEvents('click');
					self.videoOverlay.enable();
					location.reload();
				}.bind(this)
			}).send();
		});
	},
	
	clickVideo: function() {
		var self = this;

		$$('.video-box').addEvent('click',function(){
			var player = $$('.video-box').getElement('video')[0];

			if (player) {
				if (!this.videoPlaying) {
					player.play();
					this.videoPlaying = 1;
					return false;
				}

				player.pause();
				this.videoPlaying = 0;
				return false;
			}
			
			var imgContainer = this.getElement('.video-image-container');
			imgContainer.addClass('none');

			$$('.video_play_btn')[0].addClass('none');

			var width = imgContainer.get('data-width');
			var height = imgContainer.get('data-height');

			var video = imgContainer.get('rel');
			var url = imgContainer.getStyle('background-image');
			var match = url.match(/url\(\s*(['"]?)(.*?)\1\s*\)/)[2];

			var image = match.replace('video_thumb_p','orig');
			video = $('video-config').get('value') + video + '_' + height + 'p.mp4';
			self.play(video, image, true);
		});
	},
	
	play: function(video,image,auto) {
		var videoTag = new Element('video', {
			src: video,
			width: '100%',
			autoplay: auto,
			poster: image,
			multiple: false,
		});

		var wrapper = $$('.video-box .wrapper')[0];
		videoTag.inject(wrapper);

		// var vm = new VideoModal({	
		// 	"hideFooter":true,
		// 	'offsetTop':'20%',
		// 	'width':width,
		// 	'height':height,
		// 	'onAppend':function() {	
		// 		 jwplayer('video-modal').setup({
		// 			flashplayer: '/jwplayer.flash.swf',
		// 			height:height,
		// 			width:width,
		// 			image:image,
		// 			autostart: auto,

		// 			skin: {
		// 				name: "bekle"
		// 			},

		// 			logo: {
		// 				file: "/img/photo-video/logo.png"
		// 			},

		// 			file:video
		// 		});
		// 	}
		// });

		// vm.addButton("Cancel", "btn");
		// vm.show({ 
		// 	"model":"modal", 
		// 	"title":"",
		// 	'contents':'<div id="video-modal"></div>'
		// });
	}

});

