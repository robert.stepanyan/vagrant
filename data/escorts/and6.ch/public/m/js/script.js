

/*document.addEvent('domready', function(evt) {
//    $$('a.to-list, a.to-gallery').addEvent('click', function(){
//        if( !this.hasClass('active') ){
//            MCubix.Cookies.delete( 'list_type' );
//            MCubix.Cookies.set( 'list_type', this.get('data-cookie') );
//            window.location.reload();
//        }
//    })
});*/

document.addEvent('domready', function(evt) {
    $$('a.to-list').addEvent('click', function(){
        MCubix.Cookies.delete('list-types');
        MCubix.Cookies.set('list_type', 'list');
        $('page_wrapper').addClass('simple-list').removeClass('simple-gallery');
        this.setStyles({'display': 'none', 'right': '-100px'});
        $$('a.to-gallery').setStyles({'display': 'block', 'right': '5px'});
    });

    $$('a.to-gallery').addEvent('click', function(){
        MCubix.Cookies.delete('list-types');
        MCubix.Cookies.set('list_type', 'gallery');
        $('page_wrapper').addClass('simple-gallery').removeClass('simple-list');
        this.setStyles({'display': 'none', 'right': '-100px'});
        $$('a.to-list').setStyles({'display': 'block', 'right': '5px'});
    });
});

var MCubix = {
    Ad: {},
    Cookie: {}
};

MCubix.Ad.Popup = {

    adWrapper: null,

    init: function(){
        var self = this;
        self.adWrapper = $('popupBannerOverlay');
        self.doShow();
    },

    doShow: function( wr ){
        var self = this;
        if( !self.isOpened() ){

            var counter = MCubix.Cookies.get('popupB');
            if(counter == 1){
                self.adWrapper.fade('in');
                self.loadAd();
            }

            $('wrap').addEvent('click',function () {
                if(counter < 1){
                    MCubix.Cookies.set( 'popupB', 1, 1/4 );
                }
            });

        } else {
            return false;
        }
    },

    loadAd: function(){
        var url = "/ads";
        var self = this;
        var hidden_wrap = $('kchuch');
        var _content_img = hidden_wrap.getElement('img').getProperty('src');
        var _content_link = hidden_wrap.getElement('a').getProperty('href');

        new Request({
            url: url,
            method: 'get',
            onSuccess: function ( resp ) {
                //console.log( self.adWrapper );
                resp = Elements.from( resp );
                resp.inject( self.adWrapper );

                var close_btns = $$('.close-bttn', '.close-text');
                self.doClose( close_btns, self.adWrapper );
            }.bind(this)
        }).send('img=' + _content_img + '&href=' + _content_link );
    },

    doClose: function( btt, wr ){
        btt.addEvent('click', function(){
            MCubix.Cookies.set( 'popupB', 2, 1/4 );
            wr.fade('out');
        });
    },

    isOpened: function(){
        if( MCubix.Cookies.get('popupB') == 2 ){
            return true;
        }
        return false;
    }
};

MCubix.Cookies = {
    set: function( cname, cvalue, exdays, domain, path )
    {
        var d = new Date();
        d.setTime( d.getTime() + ( exdays * 24 * 60 * 60 * 1000 ) );
        var expires = "expires=" + d.toGMTString();
        var cookie_str = cname + "=" + cvalue + "; " + expires;

        if ( domain ){
            cookie_str += "; domain=" + encodeURI ( domain );
        }

        if ( path ){
            cookie_str += "; path=" + encodeURI ( path );
        }

        document.cookie = cookie_str;
    },

    get: function( cname )
    {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for( var i=0; i<ca.length; i++ )
        {
            var c = ca[i].trim();
            if ( c.indexOf( name ) == 0 ) return c.substring( name.length,c.length );
        }
        return "";
    },

    delete: function( cname ){
        document.cookie = cname + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }
};
window.addEvent('domready', function () {
    if ( $('video-button') )
    {
        $('video-button').addEvent('click', function(e){
            e.preventDefault();

            $('wrap').addClass('loader-opacity');
            $$('.loadder').setStyle('display','block');

            var url = '';
            if ($('video-filtered'))
            {
                url = window.location.pathname+ '?videos=off';
            }else{
                url = window.location.pathname + '?videos=on';
            }

            new Request({
                url: url,
                method: 'get',
                onSuccess: function (resp) {
                    $('inject-div').set('html', resp);

                    if (!$('video-filtered'))
                    {
                        new Element('input', {
                            type: 'hidden',
                            id: 'video-filtered',
                            name: 'hasvid'
                        }).inject($('page_wrapper'));
                    }else{
                        $('video-filtered').destroy();
                    }

                    $('wrap').removeClass('loader-opacity');
                    $$('.loadder').setStyle('display','none');
                }
            }).send();
        });
    }
});