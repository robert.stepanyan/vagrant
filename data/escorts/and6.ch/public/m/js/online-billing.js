// < Self Checkout >

if ($$('.escort-online-billing').length > 0) {
	var hasApprovedVideo = $('hasApprovedVideo').get('value');
	var votsuserid = JSON.parse(user);

	var mOnlineBilling = {
		// Online Billing Container
		container: $$('.escort-online-billing')[0],
		// Online Billing Popups 
		locationPopup: null,
		startTimePopup: null,
		gotdPopup: null,
		gotdCalendarPopup: null,
		gotdSelfSelectedDates: [],

		votdPopup: null,
		votdCalendarPopup: null,
		votdSelfSelectedDates: [],

		availableGotdDates: [],
		availableVotdDates: [],

		// SedCard Package Ids
		sedcardPackages: JSON.decode($('sedcardPackages').get('value')),
		dayPassesPackages: JSON.decode($('dayPassesPackages').get('value')),
		additionalAreaPrice: parseInt($('additionalAreaPrice').get('value')),
		gotdPrice: parseInt($('gotdPrice').get('value')),
		votdPrice: parseInt($('votdPrice').get('value')),


		// Methods
		init: function() {
			
			
			this.loading();
			this.selectPackage();
			this.popupLoadedListener();
			this.getLocationPopup();
			this.getStartTimePopup();

			this.getGotdPopup();
			this.getGotdCalendarPopup();
			this.getVotdPopup();
			this.getVotdCalendarPopup();

			this.switchLocationBtn(0);
			this.switchNextStepBtn(0);
			if($$('.gotd-button').length > 0){
				this.switchGotdBtn(0);
			}
			if($$('.votd-button').length > 0){
				this.switchVotdBtn(0);
			}
			this.eventManager();
		},

		loading: function(){
			$$('#page').addClass('loading');
			let counter = 0;
			window.addEvent('scloaded', function() {
				counter++;
				if (counter == 6) {
					$$('#page').removeClass('loading');
				}
			});
		},

		popupLoadedListener: function() {
			if (typeof existingCart == 'undefined') {
				return;
			}
			var counter = 0;
			var self = this;
			var cart = existingCart[0];

			window.addEvent('popupLoaded', function() {
				counter++;
				if (counter == 6) {
					self.parseSessionCart(cart);
				}
			});
		},

		selectPackage: function() {
			var self = this,
				packageOptions = this.container.getElements('.packages-container a');

			packageOptions.addEvent('click', function(e) {
				if (e) e.stop();

				if($('startTimeBtn') && !$('startTimeBtn').hasClass('active')){
                    $('startTimeBtn').addClass('active');
				}

				if ($$('.summary-location-item-container').length > 0 ||
					$$('.summary-gotd-item-container').length > 0) {

					var msg = 'Are you sure you want to change the package? ' +
						'All selected locations will be removed.';

					if (confirm(msg)) {
						$$('.summary-location-item-container')
							.getElements('.summary-item-remove-btn').each(function(el) {
								el.fireEvent('click');
							});
						$$('.summary-gotd-item-container')
							.getElements('.summary-item-remove-btn').each(function(el) {
								el.fireEvent('click');
							});

						packageOptions.removeClass('selected');
						this.addClass('selected');

						var packageId = this.get('data-id');

						if (self.hasLocation(packageId)) {
							self.switchLocationBtn(1);
						} else {
							self.switchLocationBtn(0);
						}

						self.renderInfo();
					} else {
						return;
					}
				} else {
					packageOptions.removeClass('selected');
					this.addClass('selected');

					var packageId = this.get('data-id');

					if (self.hasLocation(packageId)) {
						self.switchLocationBtn(1);
					} else {
						self.switchLocationBtn(0);
					}

					self.renderInfo();
				}

				var availDates = self.calcAvailableGotdDates();
				if (availDates) {
					self.switchGotdBtn(1);
					self.availableGotdDates = availDates;
				} else {
					self.switchGotdBtn(0);
				}
				var availDates2 = self.calcAvailableVotdDates();
				if (availDates2 && hasApprovedVideo == 1) {
					self.switchVotdBtn(1);
					self.availableVotdDates = availDates2;
				} else {
					self.switchVotdBtn(0);
				}
			});
		},

		deselectPackages: function() {
			var packageOptions = this.container.getElements('.packages-container a');

			packageOptions.each(function(el) {
				el.removeClass('selected');
			});

			var availDates = this.calcAvailableGotdDates();
			if (availDates) {
				this.switchGotdBtn(1);
				this.availableGotdDates = availDates;
			} else {
				this.switchGotdBtn(0);
			}

			this.switchLocationBtn(0);
			this.container.getElement('.selected-items').fireEvent('cartchanged');
		},

		hasLocation: function(id) {
			for (var i = this.sedcardPackages.length - 1; i >= 0; i--) {
				if (this.sedcardPackages[i] == id) {
					return true;
					break;
				}
			}

			return false;
		},

		calcAvailableGotdDates: function() {
			var self = this,
				availableGotdDates = [],
				selectedPackageOption = this.container
				.getElements('.packages-container a.selected');
			var activePackageExp = $('activePackageExpiration').get('value');
			if (selectedPackageOption.length > 0) {
				var _duration = +selectedPackageOption.get('data-duration'),
					_startDate = self.startTimePopup
					.getElement('a.selected .activation-date').get('value') || ( activePackageExp ? (new Date( activePackageExp * 1000).clearTime()) : (new Date().clearTime()) );
				for (var i = 0; i < _duration; i++) {
					availableGotdDates.include(
						Date.parse(_startDate).clone().increment('day', i).format('%Y-%m-%d')
					);
				}
			}

			return availableGotdDates;
		},

		calcAvailableVotdDates: function() {
			var self = this,
				availableVotdDates = [],
				selectedPackageOption = this.container
				.getElements('.packages-container a.selected');
			var activePackageExp = $('activePackageExpiration').get('value');
			if (selectedPackageOption.length > 0) {
				var _duration = +selectedPackageOption.get('data-duration'),
					_startDate = self.startTimePopup
					.getElement('a.selected .activation-date').get('value') || ( activePackageExp ? (new Date( activePackageExp * 1000).clearTime()) : (new Date().clearTime()) );


				for (var i = 0; i < _duration; i++) {
					availableVotdDates.include(
						Date.parse(_startDate).clone().increment('day', i).format('%Y-%m-%d')
					);
				}
			}

			return availableVotdDates;
		},

		switchLocationBtn: function(on) {
			var self = this,
				locationBtn = $('locationBtn');

			if (locationBtn) {
				if (on) {
					locationBtn.addClass('active');
					locationBtn.removeEvents().addEvent('click', function(e) {
						e.stop();
						self.showPopup(self.locationPopup);
					});
				} else {
					locationBtn.removeClass('active');
					locationBtn.removeEvents().addEvent('click', function(e) {
						e.stop();
					})
				}
			}
		},

		switchGotdBtn: function(on) {
			var self = this,
				gotdBtn = $('gotdBtn');

			if (on) {
				gotdBtn.addClass('active');
				gotdBtn.removeEvents().addEvent('click', function(e) {
					e.stop();
					self.showPopup(self.gotdPopup);
				});
			} else {
				gotdBtn.removeClass('active');
				gotdBtn.removeEvents().addEvent('click', function(e) {
					e.stop();
				});
			}
		},

		switchVotdBtn: function(on) {
			var self = this,
				votdBtn = $('votdBtn');

			if (on) {
				votdBtn.addClass('active');
				votdBtn.removeEvents().addEvent('click', function(e) {
					e.stop();
					self.showPopup(self.votdPopup);
				});
			} else {
				votdBtn.removeClass('active');
				votdBtn.removeEvents().addEvent('click', function(e) {
					e.stop();
				});
			}
		
		},

		switchNextStepBtn: function(on) {
			var self = this,
				nextPageBtn = this.container.getElement('.next-step-button');

			if (on) {
				nextPageBtn.addClass('active');
				nextPageBtn.removeEvents().addEvent('click', function(e) {
					e.stop();
					self.goToPaymentPage();
				});
			} else {
				nextPageBtn.removeClass('active');
				nextPageBtn.removeEvents().addEvent('click', function(e) {
					e.stop();
				})
			}
		},

		switchCalendarBtn: function(on, type) {
			var self = this,
				calendarBtn = $(type + 'calendarBtn');

			if (on) {
				calendarBtn.addClass('active');
				calendarBtn.removeEvents().addEvent('click', function(e) {
					e.stop();
					if(type == 'gotd'){
						self.showPopup(self.gotdCalendarPopup);
						self.getGotdCalendarPopup();
					}else if(type == 'votd'){
						self.showPopup(self.votdCalendarPopup);
						self.getVotdCalendarPopup();
					}
				});
			} else {
				calendarBtn.removeClass('active');
				calendarBtn.removeEvents().addEvent('click', function(e) {
					e.stop();
				})
			}
		},

		switchGotdLocationBtn: function(on) {
			var self = this,
				addGotdLocation = $('addGotdLocation');

			if (on) {
				addGotdLocation.addClass('active');
				addGotdLocation.removeEvents();
				self.addGotdLocation(self.gotdPopup);
			} else {
				addGotdLocation.removeClass('active');
				addGotdLocation.removeEvents().addEvent('click', function(e) {
					if (e) e.stop();
					self.showNotification(mOBLang.mOBLocationDateErr, 'error');
				})
			}
		},

		switchVotdLocationBtn: function(on) {
			var self = this,
				addVotdLocation = $('addVotdLocation');

			if (on) {
				addVotdLocation.addClass('active');
				addVotdLocation.removeEvents();
				self.addVotdLocation(self.votdPopup);
			} else {
				addVotdLocation.removeClass('active');
				addVotdLocation.removeEvents().addEvent('click', function(e) {
					if (e) e.stop();
					self.showNotification(mOBLang.mOBLocationDateErr, 'error');
				})
			}
		},

		eventManager: function() {
			var self = this;

			if ($('startTimeBtn')) { 
                $('startTimeBtn').addEvent('click', function (e) {
					e.stop();
					if(this.hasClass('active')){
						self.showPopup(self.startTimePopup);
					}
				});
			}

			$('summaryContainer').addEvent('click', function(e) {
				e.stop();
				self.toggleSummary();
			});

			this.container.getElement('.selected-items').addEvent('cartchanged', function() {
				if (this.getElements('div').length > 0) {
					self.switchNextStepBtn(1);
				} else {
					self.switchNextStepBtn(0);
				}
			});
		},

		createPopup: function(options) {
			var self = this;
			options = options ? options : {};

			var popupHeight = parseInt(options.height) || 500,
				popupWidth = parseInt(options.width) || 320,
				popupContent = options.content || null,
				popupInnerClass = options.popupInnerClass || null,
				popupActionText = options.actionText || 'OK',
				popupActionFunction = options.actionFunction || function() {
					return false;
				}

			var _outer = new Element('div', {
					styles: {
						display: 'table',
						position: 'absolute',
						height: '100%',
						width: '100%',
					}
				}),
				_middle = new Element('div', {
					styles: {
						display: 'table-cell',
						verticalAlign: 'middle'
					}
				}),

				_inner = new Element('div', {
					class: 'inner '+popupInnerClass,
					styles: {
						position: 'relative',
						marginLeft: 'auto',
						marginRight: 'auto',
						width: popupWidth + 'px',
						height: popupHeight + 'px',
						borderRadius: '3px',
						backgroundColor: '#ffffff'

					},

					html: popupContent
				});

			_innerContent = new Element('div', {
				class: 'inner-content',
				styles: {
					marginBottom: '60px',
					boxSizing: 'border-box',
				}
			});

			_innerContent.inject(_inner);
			_inner.inject(_middle);
			_middle.inject(_outer);

			var _actionBtn = new Element('a', {
				class: 'ok-btn',
				styles: {
					display: 'block',
					position: 'absolute',
					width: '100%',
					height: '60px',
					backgroundColor: '#1a5cbf',
					bottom: 0,
					left: 0,
					color: '#ffffff',
					textTransform: 'uppercase',
					textDecoration: 'none',
					fontSize: '20px',
					fontWeight: 'bold',
					textAlign: 'center',
					lineHeight: '60px',
					borderRadius: '0 0 3px 3px'
				},

				html: popupActionText,
				href: '#'
			});

			var _closeBtn = new Element('a', {
				class: 'close-btn',
				styles: {
					display: 'block',
					width: '20px',
					height: '20px',
					position: 'absolute',
					top: 0,
					right: 0,
					lineHeight: '20px',
					textAlign: 'center',
					textDecoration: 'none',
					fontSize: '20px',
					fontWeight: 'bold'
				},

				html: '&times;'
			});

			_closeBtn.addEvent('click', function() {
				self.hidePopup(this.getParents('.mobile-ob-overlay')[0])
			});
			_actionBtn.addEvent('click', popupActionFunction);

			_closeBtn.inject(_inner);
			_actionBtn.inject(_inner);

			return _outer;
		},

		createOverlay: function() {
			var _overlay = new Element('div', {
				class: 'mobile-ob-overlay',
				styles: {
					position: 'fixed',
					height: '100%',
					width: '100%',
					top: '0',
					left: '0',
					background: 'rgba(0,0,0,0.5)',
					zIndex: 1001,
					visibility: 'hidden',
					opacity: '0'
				}
			});

			_overlay.addEvent('mousewheel', function(e) {
				e.stop();
			});

			return _overlay;
		},

		getLocationPopup: function() {
			var self = this,
				overlay = this.createOverlay(),
				popup = this.createPopup({
					actionFunction: function(e) {
						if (e) e.stop();
						self.hidePopup(self.locationPopup);
						self.renderInfo();
					}
				}),
				self = this;

			popup.inject(overlay);
			overlay.inject(this.container);

			var _request = new Request({
				url: '/online-billing/escort-additional-areas',
				method: 'get',

				onSuccess: function(responseText) {
					popup.getElement('.inner-content').set('html', responseText);
					self.addAdditionalLocation(popup);

					window.fireEvent('popupLoaded');
					window.fireEvent('scloaded');
				}
			}).send();

			this.locationPopup = overlay;
		},

		addAdditionalLocation: function(popup) {
			var self = this;

			popup.getElement('#addLocation').addEvent('click', function(e) {
				if (e) e.stop();

				var selectedArea = popup.getElement('.additional-areas').getSelected(),
					areaId = selectedArea.get('value'),
					areaTitle = selectedArea.get('html'),
					selectedAreasWrapper = popup.getElement('#selectedAreas');
				closeBtn = popup.getElement();

				if (!areaId.length) return;

				selectedArea.dispose();

				_areaContainer = new Element('div', {
					class: 'area-container',
					styles: {
						width: '100%',
						height: '20px',
						float: 'left',
						marginTop: '10px'
					}
				});

				_areaTitle = new Element('span', {
					styles: {
						display: 'block',
						width: 'calc(100% - 20px)',
						height: '20px',
						overflow: 'hidden',
						textOverflow: 'ellipsis',
						whiteSpace: 'nowrap',
						float: 'left'
					},

					html: areaTitle
				});

				_removeBtn = new Element('a', {
					'data-id': areaId,
					class: 'additional-area-remove-btn',

					styles: {
						display: 'block',
						width: '20px',
						height: '20px',
						textAlign: 'center',
						lineHeight: '20px',
						fontSize: '20px',
						color: '#ffffff',
						float: 'left',
						textDecoration: 'none',
						borderRadius: '3px',
						backgroundColor: '#ac2767'
					},

					html: '&times;'
				});

				_removeBtn.addEvent('click', function(e) {
					if (e) {
						e.stop();
					}

					var _id = this.get('data-id'),
						_title = this.getSiblings('span')[0].get('html'),
						_option = new Element('option', {
							value: _id,
							html: _title
						});

					this.getParent('.area-container').dispose();
					_option.inject(popup.getElement('select.additional-areas'));
				});

				_areaTitle.inject(_areaContainer);
				_removeBtn.inject(_areaContainer);

				_areaContainer.inject(selectedAreasWrapper);
			});

			popup.getElement('.close-btn').addEvent('click', function(e) {
				popup.getElement('#selectedAreas')
					.getElements('.area-container a').each(function(el) {
						el.fireEvent('click');
					});

				self.renderInfo();
			});
		},

		getStartTimePopup: function() {
			var self = this,
				overlay = this.createOverlay(),
				popup = this.createPopup({
					height: 350,
					actionFunction: function(e) {
                        var _activationTypeSelected = self.startTimePopup.getElement('a.selected');

						if (_activationTypeSelected.getElement('.activation-date').get('value') !== '') {
							var _date = _activationTypeSelected.getElement('.activation-date').get('value'),
								_time = _activationTypeSelected.getElement('.activation-time').get('value'),

								_parsedDate = Date.parse(_date + ' ' + _time),
								hasActivePackage = $('hasActivePackage').get('value'),
								activePackageExpiration = $('activePackageExpiration').get('value');

							if (hasActivePackage) {
								var _diff = _parsedDate.diff(Date.parse(activePackageExpiration, 'second'));

								if (_diff > 0) {
									self.showNotification(mOBLang.mOBDateGreaterErr, 'error');
									return false;
								}
							} else {

								var _diff = _parsedDate.diff(new Date(), 'second');

								if (_diff > 0) {
									self.showNotification(mOBLang.mOBDateInvalidErr, 'error');
									return false;
								}
							}
						}

						e.stop();
						self.hidePopup(self.startTimePopup);
						self.renderInfo();
					}
				}),
				self = this;

			popup.inject(overlay);
			overlay.inject(this.container);

			var _request = new Request({
				url: '/online-billing/escort-activation-date',
				method: 'get',

				onSuccess: function(responseText) {
					popup.getElement('.inner-content').set('html', responseText);
					self.selectActivationDate(popup);


					window.fireEvent('popupLoaded');
					window.fireEvent('scloaded');
				}
			}).send();

			this.startTimePopup = overlay;
		},

		selectActivationDate: function(popup) {
			var self = this,
				dateOptions = popup.getElements('.escort-activation-date a'),
				dateTimeInputs = popup.getElements('.escort-activation-date input');

			dateOptions.addEvent('click', function(e) {
				if (e) e.stop();

				dateOptions.removeClass('selected');
				this.addClass('selected');
			});

			dateTimeInputs.addEvent('click', function(e) {
				e.stopPropagation();
				dateOptions[1].fireEvent('click');
			});
		},

		getGotdPopup: function() {
			var self = this;
			overlay = this.createOverlay(),
				popup1 = this.createPopup({
					popupInnerClass: 'getGotdPopup',
					actionFunction: function() {
						self.renderInfo();
						self.hidePopup(self.gotdPopup);
					}
				});

			popup1.inject(overlay);
			overlay.inject(this.container);

			var _request = new Request({
				url: '/online-billing/escort-gotd',
				method: 'get',

				onSuccess: function(responseText) {
					
					popup1.getElement('.inner-content').set('html', responseText);
					popup1.getElement('.additional-areas')
						.addEvent('change', function() {
							if (+this.getSelected().get('value') !== 0) {
								self.gotdSelfSelectedDates = [];
								self.switchCalendarBtn(1, 'gotd');
								self.switchGotdLocationBtn(1);
								self.gotdCalendarPopup
									.getElement('#selectedGotdDates').set('html', '');
							} else {
								self.gotdSelfSelectedDates = [];
								self.switchCalendarBtn(0, 'gotd');
								self.switchGotdLocationBtn(0);
								self.gotdCalendarPopup
									.getElement('#selectedGotdDates').set('html', '');
							}
						});
					self.switchCalendarBtn(0, 'gotd');
					self.switchGotdLocationBtn(0);

					window.fireEvent('popupLoaded');
					window.fireEvent('scloaded');
				}
			}).send();

			this.gotdPopup = overlay;
		},

		getVotdPopup: function() {
			var self = this;
			overlay = this.createOverlay(),
				popup = this.createPopup({
					popupInnerClass: 'getVotdPopup',
					actionFunction: function() {
						self.renderInfo();
						self.hidePopup(self.votdPopup);
					}
				});

			popup.inject(overlay);
			overlay.inject(this.container);

			var _request = new Request({
				url: '/online-billing/escort-votd',
				method: 'get',

				onSuccess: function(responseText) {
					
					popup.getElement('.inner-content').set('html', responseText);
					popup.getElement('.additional-areas')
						.addEvent('change', function() {
							if (+this.getSelected().get('value') !== 0) {

								self.votdSelfSelectedDates = [];
								self.switchCalendarBtn(1, 'votd');
								self.switchVotdLocationBtn(1);
								self.votdCalendarPopup
									.getElement('#selectedVotdDates').set('html', '');
							} else {
								self.votdSelfSelectedDates = [];
								self.switchCalendarBtn(0, 'votd');
								self.switchVotdLocationBtn(0);
								self.votdCalendarPopup
									.getElement('#selectedVotdDates').set('html', '');
							}
						});
					self.switchCalendarBtn(0, 'votd');
					self.switchVotdLocationBtn(0);

					window.fireEvent('popupLoaded');
					window.fireEvent('scloaded');
				}
			}).send();

			this.votdPopup = overlay;
		},

		addGotdLocation: function(popup) {
			var self = this;

			popup.getElement('#addGotdLocation').addEvent('click', function(e) {
				if (e) e.stop();


				if (!self.gotdSelfSelectedDates.length) {
					self.showNotification('<span>Error</span> Select Date(s)', 'error');
					return;
				}

				var selectedArea = popup.getElement('.additional-areas').getSelected(),
					areaId = selectedArea.get('value'),
					areaTitle = selectedArea.get('html'),
					selectedGotdAreasWrapper = popup.getElement('#selectedGotdAreas'),
					additionalAreas = popup.getElement('.additional-areas'),
					closeBtn = popup.getElement('.close-btn');

				if (!areaId.length) return;

				selectedArea.dispose();

				self.gotdSelfSelectedDates.each(function(el) {
					_areaContainer = new Element('div', {
						class: 'area-container',
						styles: {
							width: '100%',
							height: '20px',
							float: 'left',
							marginTop: '10px'
						}
					});

					_areaTitle = new Element('span', {
						styles: {
							display: 'block',
							width: 'calc(100% - 20px)',
							height: '20px',
							overflow: 'hidden',
							textOverflow: 'ellipsis',
							whiteSpace: 'nowrap',
							float: 'left'
						},

						html: '<strong>' + areaTitle + '</strong> : ' + el
					});

					_removeBtn = new Element('a', {
						'data-id': areaId,
						class: 'additional-area-remove-btn',

						styles: {
							display: 'block',
							width: '20px',
							height: '20px',
							textAlign: 'center',
							lineHeight: '20px',
							fontSize: '20px',
							color: '#ffffff',
							float: 'left',
							textDecoration: 'none',
							borderRadius: '3px',
							backgroundColor: '#ac2767'
						},

						html: '&times;'
					});

					_removeBtn.addEvent('click', function(e) {
						if (e) {
							e.stop();
						}

						var _id = this.get('data-id'),
							_container = this.getParents('#selectedGotdAreas'),
							it = this,
							occur = 0;

						_container.getElements('.additional-area-remove-btn')[0].each(function(el) {
							if (el.get('data-id') == _id) occur++;
						});

						if (occur > 1) {
							this.getParent('.area-container').dispose();
						} else {
							_title = this.getSiblings('span')[0].getElement('strong').get('html');
							_option = new Element('option', {
								value: _id,
								html: _title
							});

							this.getParent('.area-container').dispose();
							_option.inject(popup.getElement('select.additional-areas'));
						}

					});

					_areaContainer.adopt(_areaTitle, _removeBtn);
					_areaContainer.inject(selectedGotdAreasWrapper);
				});
				self.gotdCalendarPopup.getElement('.close-btn').fireEvent('click');
				self.gotdSelfSelectedDates = [];
				additionalAreas.fireEvent('change');
			});

			popup.getElement('.close-btn').addEvent('click', function(e) {
				popup.getElement('#selectedGotdAreas')
					.getElements('.area-container a').each(function(el) {
						el.fireEvent('click');
					});
				self.renderInfo();
			});
		},

		addVotdLocation: function(popup) {
			var self = this;

			popup.getElement('#addVotdLocation').addEvent('click', function(e) {
				if (e) e.stop();


				if (!self.votdSelfSelectedDates.length) {
					self.showNotification('<span>Error</span> Select Date(s)', 'error');
					return;
				}

				var selectedArea = popup.getElement('.additional-areas').getSelected(),
					areaId = selectedArea.get('value'),
					areaTitle = selectedArea.get('html'),
					selectedVotdAreasWrapper = popup.getElement('#selectedVotdAreas'),
					additionalAreas = popup.getElement('.additional-areas'),
					closeBtn = popup.getElement('.close-btn');

				if (!areaId.length) return;

				selectedArea.dispose();

				self.votdSelfSelectedDates.each(function(el) {
					_areaContainer = new Element('div', {
						class: 'area-container',
						styles: {
							width: '100%',
							height: '20px',
							float: 'left',
							marginTop: '10px'
						}
					});

					_areaTitle = new Element('span', {
						styles: {
							display: 'block',
							width: 'calc(100% - 20px)',
							height: '20px',
							overflow: 'hidden',
							textOverflow: 'ellipsis',
							whiteSpace: 'nowrap',
							float: 'left'
						},

						html: '<strong>' + areaTitle + '</strong> : ' + el
					});

					_removeBtn = new Element('a', {
						'data-id': areaId,
						class: 'additional-area-remove-btn',

						styles: {
							display: 'block',
							width: '20px',
							height: '20px',
							textAlign: 'center',
							lineHeight: '20px',
							fontSize: '20px',
							color: '#ffffff',
							float: 'left',
							textDecoration: 'none',
							borderRadius: '3px',
							backgroundColor: '#ac2767'
						},

						html: '&times;'
					});

					_removeBtn.addEvent('click', function(e) {
						if (e) {
							e.stop();
						}

						var _id = this.get('data-id'),
							_container = this.getParents('#selectedVotdAreas'),
							it = this,
							occur = 0;

						_container.getElements('.additional-area-remove-btn')[0].each(function(el) {
							if (el.get('data-id') == _id) occur++;
						});

						if (occur > 1) {
							this.getParent('.area-container').dispose();
						} else {
							_title = this.getSiblings('span')[0].getElement('strong').get('html');
							_option = new Element('option', {
								value: _id,
								html: _title
							});

							this.getParent('.area-container').dispose();
							_option.inject(popup.getElement('select.additional-areas'));
						}

					});

					_areaContainer.adopt(_areaTitle, _removeBtn);
					_areaContainer.inject(selectedVotdAreasWrapper);
				});
				self.votdCalendarPopup.getElement('.close-btn').fireEvent('click');
				self.votdSelfSelectedDates = [];
				additionalAreas.fireEvent('change');
			});

			popup.getElement('.close-btn').addEvent('click', function(e) {
				popup.getElement('#selectedVotdAreas')
					.getElements('.area-container a').each(function(el) {
						el.fireEvent('click');
					});
				self.renderInfo();
			});
		},

		getGotdCalendarPopup: function() {
			this.gotdSelfSelectedDates = [];
			if (!this.gotdCalendarPopup) {
				var self = this,
					overlay = this.createOverlay(),
					popup = this.createPopup({
						actionFunction: function() {
							var _selectedDates = $('selectedGotdDates')
								.getElements('span').get('html');
							self.gotdSelfSelectedDates = _selectedDates;
							self.hidePopup(self.gotdCalendarPopup);
						}
					});

				popup.inject(overlay);
				overlay.inject(this.container);

				var _request = new Request({
					url: '/online-billing/escort-gotd-calendar',
					method: 'get',
					available_dates: self.availableGotdDates,

					onSuccess: function(responseText) {
						popup.getElement('.inner-content').set('html', responseText);
						self.gotdCalendarGoto(popup);

						window.fireEvent('popupLoaded');
						window.fireEvent('scloaded');
					}
				}).send();

				this.gotdCalendarPopup = overlay;
			} else {
				var self = this,
					areaId = this.gotdPopup
					.getElement('.additional-areas').getSelected().get('value'),
					calendarWrapper = this.gotdCalendarPopup.getElement('.ob-gotd-calendar-wrapper'),
					calendarContainer = this.gotdCalendarPopup.getElement('.escort-gotd-calendar'),
					selectedDateAreasEls = this.gotdPopup
					.getElements('#selectedGotdAreas > .area-container > span');
				this.gotdCalendarPopup.getElement('#selectedGotdDates').set('html', '');

				if (selectedDateAreasEls && selectedDateAreasEls.length > 0) {
					var preselectedDates = [];
					selectedDateAreasEls.each(function(el) {
						preselectedDates.push(el.get('html').split(':')[1].trim());
					});
				}

				var _request = new Request({
					url: '/online-billing/escort-gotd-calendar',
					method: 'get',
					data: {
						area_id: +areaId,
						available_dates: self.availableGotdDates,
						preselected_dates: preselectedDates
					},

					onLoadstart: function() {
						$$('.ob-gotd-calendar-overlay')[0].addClass('visible');
					},

					onSuccess: function(responseText) {
						calendarWrapper.dispose();

						Elements.from(responseText).inject(calendarContainer, 'before');
						self.gotdCalendarGoto(self.gotdCalendarPopup);

						self.selectGotdDates(
							self.gotdCalendarPopup.getElement('.ob-gotd-calendar-wrapper table')
						);

						$$('.ob-gotd-calendar-overlay')[0].removeClass('visible');
					}
				}).send();
			}
		},

		getVotdCalendarPopup: function() {
			this.votdSelfSelectedDates = [];
			if (!this.votdCalendarPopup) {
				var self = this,
					overlay = this.createOverlay(),
					popup = this.createPopup({
						actionFunction: function() {
							var _selectedDates = $('selectedVotdDates')
								.getElements('span').get('html');
							self.votdSelfSelectedDates = _selectedDates;
							self.hidePopup(self.votdCalendarPopup);
						}
					});

				popup.inject(overlay);
				overlay.inject(this.container);

				var _request = new Request({
					url: '/online-billing/escort-votd-calendar',
					method: 'get',
					available_dates: self.availableVotdDates,

					onSuccess: function(responseText) {
						popup.getElement('.inner-content').set('html', responseText);
						self.votdCalendarGoto(popup);

						window.fireEvent('popupLoaded');
						window.fireEvent('scloaded');
					}
				}).send();

				this.votdCalendarPopup = overlay;
			} else {
				var self = this,
					areaId = this.votdPopup
					.getElement('.additional-areas').getSelected().get('value'),
					calendarWrapper = this.votdCalendarPopup.getElement('.ob-votd-calendar-wrapper'),
					calendarContainer = this.votdCalendarPopup.getElement('.escort-votd-calendar'),
					selectedDateAreasEls = this.votdPopup
					.getElements('#selectedVotdAreas > .area-container > span');
				this.votdCalendarPopup.getElement('#selectedVotdDates').set('html', '');

				if (selectedDateAreasEls && selectedDateAreasEls.length > 0) {
					var preselectedDates = [];
					selectedDateAreasEls.each(function(el) {
						preselectedDates.push(el.get('html').split(':')[1].trim());
					});
				}

				var _request = new Request({
					url: '/online-billing/escort-votd-calendar',
					method: 'get',
					data: {
						area_id: +areaId,
						available_dates: self.availableVotdDates,
						preselected_dates: preselectedDates
					},

					onLoadstart: function() {
						$$('.ob-votd-calendar-overlay')[0].addClass('visible');
					},

					onSuccess: function(responseText) {
						calendarWrapper.dispose();

						Elements.from(responseText).inject(calendarContainer, 'before');
						self.votdCalendarGoto(self.votdCalendarPopup);

						self.selectVotdDates(
							self.votdCalendarPopup.getElement('.ob-votd-calendar-wrapper table')
						);

						$$('.ob-votd-calendar-overlay')[0].removeClass('visible');
					}
				}).send();
			}
		},

		gotdCalendarGoto: function(popup) {
			var calendar = popup.getElement('table.ob-gotd-calendar'),
				btns = popup.getElements('thead a'),
				calendarWrapper = popup.getElement('.ob-gotd-calendar-wrapper'),
				calendarContainer = popup.getElement('.escort-gotd-calendar'),
				self = this;

			var selectedDateAreasEls = this.gotdPopup
				.getElements('#selectedGotdAreas > .area-container > span');

			if (selectedDateAreasEls && selectedDateAreasEls.length > 0) {
				var preselectedDates = [];
				selectedDateAreasEls.each(function(el) {
					preselectedDates.push(el.get('html').split(':')[1].trim());
				});
			}

			btns.addEvent('click', function(e) {
				e.stop();

				new Request({
					url: '/online-billing/escort-gotd-calendar',
					method: 'get',
					data: {
						calendar_month: this.get('data-month'),
						area_id: +this.get('data-area-id'),
						selected_dates: $('selectedGotdDates')
							.getElements('span').get('html'),
						preselected_dates: preselectedDates,
						available_dates: self.availableGotdDates
					},

					onLoadstart: function() {
						$$('.ob-gotd-calendar-overlay')[0].addClass('visible');
					},

					onSuccess: function(responseText) {
						calendarWrapper.dispose();
						Elements.from(responseText).inject(calendarContainer, 'before');

						self.gotdCalendarGoto(popup);

						btns.removeClass('hidden');

						self.selectGotdDates(popup.getElement('.ob-gotd-calendar-wrapper table'));

						$$('.ob-gotd-calendar-overlay')[0].removeClass('visible');
					}
				}).send();
			});
		},

		votdCalendarGoto: function(popup) {
			var calendar = popup.getElement('table.ob-votd-calendar'),
				btns = popup.getElements('thead a'),
				calendarWrapper = popup.getElement('.ob-votd-calendar-wrapper'),
				calendarContainer = popup.getElement('.escort-votd-calendar'),
				self = this;

			var selectedDateAreasEls = this.votdPopup
				.getElements('#selectedVotdAreas > .area-container > span');

			if (selectedDateAreasEls && selectedDateAreasEls.length > 0) {
				var preselectedDates = [];
				selectedDateAreasEls.each(function(el) {
					preselectedDates.push(el.get('html').split(':')[1].trim());
				});
			}

			btns.addEvent('click', function(e) {
				e.stop();

				new Request({
					url: '/online-billing/escort-votd-calendar',
					method: 'get',
					data: {
						calendar_month: this.get('data-month'),
						area_id: +this.get('data-area-id'),
						selected_dates: $('selectedVotdDates')
							.getElements('span').get('html'),
						preselected_dates: preselectedDates,
						available_dates: self.availableGotdDates
					},

					onLoadstart: function() {
						$$('.ob-gotd-calendar-overlay')[0].addClass('visible');
					},

					onSuccess: function(responseText) {
						calendarWrapper.dispose();
						Elements.from(responseText).inject(calendarContainer, 'before');

						self.votdCalendarGoto(popup);

						btns.removeClass('hidden');

						self.selectVotdDates(popup.getElement('.ob-votd-calendar-wrapper table'));

						$$('.ob-votd-calendar-overlay')[0].removeClass('visible');
					}
				}).send();
			});
		},

		selectGotdDates: function(calendar) {
			var selectedGotdDatesContainer = $('selectedGotdDates');

			calendar.getElements('tbody td.package time').addEvent('click', function() {
				if (this.getParent('td').hasClass('preselected')) {
					return false;
				}

				if (this.hasClass('selected') || this.getParent('td').hasClass('selected')) {
					this.removeClass('selected');
					this.getParent('td').removeClass('selected')

					var selectedHtmlClass = '.booked-' + this.get('datetime');
					selectedGotdDatesContainer.getElement(selectedHtmlClass).dispose();
				} else {
					this.addClass('selected');

					var selectedDate = this.get('datetime'),
						selectedDateEl = new Element('span', {

							class: 'booked-' + selectedDate,

							styles: {
								display: 'inline-block',
								width: '57px',
								backgroundColor: '#ac2767',
								color: '#ffffff',
								padding: '5px',
								borderRadius: '3px',
								margin: '2px 3px',
								fontSize: '10px',
								textAlign: 'center'
							},

							html: selectedDate
						});

					selectedDateEl.addEvent('click', function(e) {
						if (e) {
							e.stop();
						}

						var selectedDateSingle = this,
							selectedDateElHtml = this.get('html');


						$$('.ob-gotd-calendar-wrapper table')[0]
							.getElements('time')
							.each(function(el) {
								if (el.get('datetime') == selectedDateElHtml) {
									el.removeClass('selected');
									el.getParent('td').removeClass('selected');

									selectedDateSingle.dispose();
								}
							});
					});

					selectedDateEl.inject(selectedGotdDatesContainer);
				}
			});

			var popupInner = calendar.getParents('.inner')[0];
			popupInner.getElement('.close-btn').addEvent('click', function(e) {
				popupInner.getElement('#selectedGotdDates')
					.getElements('span').each(function(el) {
						el.fireEvent('click');
					});
			});
		},

		selectVotdDates: function(calendar) {
			var selectedVotdDatesContainer = $('selectedVotdDates');

			calendar.getElements('tbody td.package time').addEvent('click', function() {
				if (this.getParent('td').hasClass('preselected')) {
					return false;
				}

				if (this.hasClass('selected') || this.getParent('td').hasClass('selected')) {
					this.removeClass('selected');
					this.getParent('td').removeClass('selected')

					var selectedHtmlClass = '.booked-' + this.get('datetime');
					selectedVotdDatesContainer.getElement(selectedHtmlClass).dispose();
				} else {
					this.addClass('selected');

					var selectedDate = this.get('datetime'),
						selectedDateEl = new Element('span', {

							class: 'booked-' + selectedDate,

							styles: {
								display: 'inline-block',
								width: '57px',
								backgroundColor: '#ac2767',
								color: '#ffffff',
								padding: '5px',
								borderRadius: '3px',
								margin: '2px 3px',
								fontSize: '10px',
								textAlign: 'center'
							},

							html: selectedDate
						});

					selectedDateEl.addEvent('click', function(e) {
						if (e) {
							e.stop();
						}

						var selectedDateSingle = this,
							selectedDateElHtml = this.get('html');


						$$('.ob-votd-calendar-wrapper table')[0]
							.getElements('time')
							.each(function(el) {
								if (el.get('datetime') == selectedDateElHtml) {
									el.removeClass('selected');
									el.getParent('td').removeClass('selected');

									selectedDateSingle.dispose();
								}
							});
					});

					selectedDateEl.inject(selectedVotdDatesContainer);
				}
			});

			var popupInner = calendar.getParents('.inner')[0];
			popupInner.getElement('.close-btn').addEvent('click', function(e) {
				popupInner.getElement('#selectedVotdDates')
					.getElements('span').each(function(el) {
						el.fireEvent('click');
					});
			});
		},

		showPopup: function(popup) {
			popup.getElement('.inner').removeClass('animated bounceOut').addClass('animated bounceIn');
			popup.fade('in');
		},

		hidePopup: function(popup) {
			popup.getElement('.inner')
				.removeClass('animated bounceIn')
				.addClass('animated bounceOut')

			popup.fade('out');
		},

		getSummaryInfo: function() {
			var packageContainer = $$('.packages-container')[0],
				activationDateContainer = $$('.escort-activation-date')[0],
				locationContainer = $$('.escort-additional-areas')[0],
				gotdContainer = $$('.escort-gotd')[0],
				votdContainer = $$('.escort-votd')[0],
				gotdLocationDate = [],
				votdLocationDate = [],
				areasLocations = [];

			if (locationContainer.getElement('#selectedAreas')
				.getElements('.area-container').length > 0) {
				var locationContainers = locationContainer.getElement('#selectedAreas')
					.getElements('.area-container');

				locationContainers.each(function(el) {
					var _areaId = el.getElement('a').get('data-id'),
						_areaTitle = el.getElement('span').get('html');

					areasLocations.push({
						a: _areaId,
						t: _areaTitle
					});
				});
			}

			if (gotdContainer.getElement('#selectedGotdAreas').getElements('a').length > 0) {
				var areaContainers = gotdContainer
					.getElement('#selectedGotdAreas').getElements('.area-container');

				areaContainers.each(function(el) {
					var _date = el.getElement('span').get('html').split(':')[1].trim(),
						_areaId = el.getElement('a').get('data-id'),
						_areaTitle = el.getElement('strong').get('html');

					gotdLocationDate.push({
						d: _date,
						a: _areaId,
						t: _areaTitle
					});
				});
			}
			if (votdContainer.getElement('#selectedVotdAreas').getElements('a').length > 0) {
				var areaContainers = votdContainer
					.getElement('#selectedVotdAreas').getElements('.area-container');

				areaContainers.each(function(el) {
					var _date = el.getElement('span').get('html').split(':')[1].trim(),
						_areaId = el.getElement('a').get('data-id'),
						_areaTitle = el.getElement('strong').get('html');

					votdLocationDate.push({
						d: _date,
						a: _areaId,
						t: _areaTitle
					});
				});
			}

			if ($$('.packages-container').length > 0) {
				return {
					package: {
						packageId: (packageContainer.getElement('a.selected')) ?
							packageContainer.getElement('a.selected').get('data-id') : 0,
						packagePrice: (packageContainer.getElement('a.selected')) ?
							parseFloat(packageContainer.getElement('a.selected').get('data-price')) : 0,
						packageCryptoPrice: (packageContainer.getElement('a.selected')) ?
							parseFloat(packageContainer.getElement('a.selected').get('data-crypto-price')) : 0,	
						packageDuration: (packageContainer.getElement('a.selected')) ?
							packageContainer.getElement('a.selected').get('data-duration') : 0,
						packageTitle: (packageContainer.getElement('a.selected')) ?
							packageContainer.getElement('a.selected')
							.getElement('.package-title').get('html') : ''
					},

					activation: {
						date: activationDateContainer.getElement('a.selected')
							.getElement('.activation-date').get('value'),
						time: activationDateContainer.getElement('a.selected')
							.getElement('.activation-time').get('value'),
					},

					areas: areasLocations,

					gotds: gotdLocationDate,

					votds: votdLocationDate

				};
			} else {
				return {
					gotds: gotdLocationDate,
					votds: votdLocationDate
				}
			}
		},

		renderInfo: function() {
			var self = this,
				_data = this.getSummaryInfo();
			if (!_data.package && !_data.gotds) return;

			var summaryContainer = this.container.getElement('.selected-items');
			summaryContainer.set('html', '');

			if (_data.package) {
				// < Selected Package >

				var _summaryPackageItemContainer = new Element('div', {
						class: 'summary-package-item-container',

						styles: {
							marginTop: '25px',
							float: 'left',
							width: '100%'
						}
					}),

					_itemName = new Element('span', {
						class: 'summary-item-name',

						styles: {
							display: 'block',
							width: 'calc(100% - 30px)',
							height: '20px',
							overflow: 'hidden',
							textOverflow: 'ellipsis',
							whiteSpace: 'nowrap',
							float: 'left',
							fontSize: '14px',
							fontWeight: 'normal',
							color: '#000000'
						},

						html: '<strong>' + _data.package.packageTitle + '</strong> - ' +
							'<span style="display: inline-block; padding: 0 5px; border-radius: 3px;' +
							' color: #ffffff; background-color: rgb(126, 137, 154);' +
							' text-shadow: 1px 1px 0 rgba(94, 104, 119, 0.68);">' + ((_data.activation.date && _data.activation.time) ?
								(_data.activation.date + ' ' + _data.activation.time) :
								mOBLang.mOBImmediately) + '</span>'
					}),

					_itemRemoveBtn = new Element('a', {
						class: 'summary-item-remove-btn',

						styles: {
							display: 'block',
							width: '20px',
							height: '20px',
							textAlign: 'center',
							lineHeight: '20px',
							fontSize: '20px',
							color: 'rgb(255, 255, 255)',
							float: 'left',
							textDecoration: 'none',
							borderRadius: '3px',
							backgroundColor: 'rgb(172, 39, 103)',
							marginRight: '10px'
						},

						html: '&times;'
					});

				_itemRemoveBtn.addEvent('click', function(e) {
					if (e) e.stop();

					if ($$('.summary-location-item-container').length > 0 ||
						$$('.summary-gotd-item-container').length > 0) {
						var msg = mOBLang.mOBRemovePackage;

						if (confirm(msg)) {
							$$('.summary-location-item-container')
								.getElements('.summary-item-remove-btn').each(function(el) {
									el.fireEvent('click');
								});
							$$('.summary-gotd-item-container')
								.getElements('.summary-item-remove-btn').each(function(el) {
									el.fireEvent('click');
								});
							$$('.summary-package-item-container').dispose();
							$$('.summary-total-container').dispose();
							self.deselectPackages();
							$$('.count-amount').set('html', '');
						}
					} else {
						this.getParent('.summary-package-item-container').dispose();
						$$('.summary-total-container').dispose();
						self.deselectPackages();
						$$('.count-amount').set('html', '');
					}
				});

				_summaryPackageItemContainer.adopt(_itemName, _itemRemoveBtn);
				_summaryPackageItemContainer.inject(summaryContainer);

				// </ Selected Package >

				// < Selected Locations > 

				_itemsElementGroup = [];

				_data.areas.each(function(el) {
					var _summaryLocationItemContainer = new Element('div', {
							class: 'summary-location-item-container',

							styles: {
								marginTop: '10px',
								float: 'left',
								width: '100%'
							}
						}),

						_areaItemName = new Element('span', {
							class: 'summary-item-name',

							styles: {
								display: 'block',
								width: 'calc(100% - 30px)',
								height: '20px',
								overflow: 'hidden',
								textOverflow: 'ellipsis',
								whiteSpace: 'nowrap',
								float: 'left',
								fontSize: '14px',
								fontWeight: 'normal',
								color: '#000000'
							},

							html: '<strong style="font-size: 16px">+</strong> ' + el.t
						}),

						_areaItemRemoveBtn = new Element('a', {
							class: 'summary-item-remove-btn',

							styles: {
								display: 'block',
								width: '20px',
								height: '20px',
								textAlign: 'center',
								lineHeight: '20px',
								fontSize: '20px',
								color: 'rgb(255, 255, 255)',
								float: 'left',
								textDecoration: 'none',
								borderRadius: '3px',
								backgroundColor: 'rgb(172, 39, 103)',
								marginRight: '10px'
							},

							html: '&times;',

							'data-id': el.a
						});


					_areaItemRemoveBtn.addEvent('click', function(e) {
						if (e) e.stop();

						var __id = this.get('data-id');

						self.locationPopup.getElement('.result-section')
							.getElements('.additional-area-remove-btn').each(function(el) {
								if (el.get('data-id') == __id) {
									el.fireEvent('click');
								}
							});

						self.renderInfo();
					});

					_summaryLocationItemContainer.adopt(_areaItemName, _areaItemRemoveBtn);
					_summaryLocationItemContainer.inject(summaryContainer);
				});

				// </ Selected Locations >
			}

			// < Selected Gotds > 

			_data.gotds.each(function(el) {
				var _summaryGotdItemContainer = new Element('div', {
						class: 'summary-gotd-item-container',

						styles: {
							marginTop: '10px',
							float: 'left',
							width: '100%'
						}
					}),

					_areaItemName = new Element('span', {
						class: 'summary-item-name',

						styles: {
							display: 'block',
							width: 'calc(100% - 110px)',
							height: '20px',
							overflow: 'hidden',
							textOverflow: 'ellipsis',
							whiteSpace: 'nowrap',
							float: 'left',
							fontSize: '14px',
							fontWeight: 'normal',
							color: '#000000'
						},

						html: '<strong style="font-size: 12px;' + ' color: #7e899a;">GOTD: </strong> ' + el.t
					}),

					_areaItemDate = new Element('span', {
						class: 'summary-item-date',

						styles: {
							display: 'block',
							width: '80px',
							float: 'left',
							fontSize: '12px',
							fontWeight: 'normal',
							color: 'rgb(172, 39, 103)',
							lineHeight: '22px',
							fontWeight: 'bold'

						},

						html: el.d
					})

				_areaItemRemoveBtn = new Element('a', {
					class: 'summary-item-remove-btn',

					styles: {
						display: 'block',
						width: '20px',
						height: '20px',
						textAlign: 'center',
						lineHeight: '20px',
						fontSize: '20px',
						color: 'rgb(255, 255, 255)',
						float: 'left',
						textDecoration: 'none',
						borderRadius: '3px',
						backgroundColor: 'rgb(172, 39, 103)',
						marginRight: '10px'
					},

					html: '&times;',

					'data-id': el.a
				});


				_areaItemRemoveBtn.addEvent('click', function(e) {
					if (e) e.stop();

					var __id = this.get('data-id'),
						__date = this.getSiblings('.summary-item-date')[0].get('html'),
						__selectedGotdDateEls = self.gotdPopup
						.getElement('#selectedGotdAreas')
						.getElements('.area-container');

					__selectedGotdDateEls.each(function(el) {
						if (el.getElement('span').get('html').split(':')[1]
							.trim() == __date && el.getElement('a')
							.get('data-id') == __id) {
							el.getElement('a').fireEvent('click');
						}
					});

					self.renderInfo();
				});

				_summaryGotdItemContainer.adopt(
					_areaItemName,
					_areaItemDate,
					_areaItemRemoveBtn
				);
				_summaryGotdItemContainer.inject(summaryContainer);
			});

			// </ Selected Gotds >

			// < Selected Votds > 

			_data.votds.each(function(el) {
				var _summaryVotdItemContainer = new Element('div', {
						class: 'summary-votd-item-container',

						styles: {
							marginTop: '10px',
							float: 'left',
							width: '100%'
						}
					}),

					_areaItemName = new Element('span', {
						class: 'summary-item-name',

						styles: {
							display: 'block',
							width: 'calc(100% - 110px)',
							height: '20px',
							overflow: 'hidden',
							textOverflow: 'ellipsis',
							whiteSpace: 'nowrap',
							float: 'left',
							fontSize: '14px',
							fontWeight: 'normal',
							color: '#000000'
						},

						html: '<strong style="font-size: 12px;' + ' color: #7e899a;">VOTD: </strong> ' + el.t
					}),

					_areaItemDate = new Element('span', {
						class: 'summary-item-date',

						styles: {
							display: 'block',
							width: '80px',
							float: 'left',
							fontSize: '12px',
							fontWeight: 'normal',
							color: 'rgb(172, 39, 103)',
							lineHeight: '22px',
							fontWeight: 'bold'

						},

						html: el.d
					})

				_areaItemRemoveBtn = new Element('a', {
					class: 'summary-item-remove-btn',

					styles: {
						display: 'block',
						width: '20px',
						height: '20px',
						textAlign: 'center',
						lineHeight: '20px',
						fontSize: '20px',
						color: 'rgb(255, 255, 255)',
						float: 'left',
						textDecoration: 'none',
						borderRadius: '3px',
						backgroundColor: 'rgb(172, 39, 103)',
						marginRight: '10px'
					},

					html: '&times;',

					'data-id': el.a
				});


				_areaItemRemoveBtn.addEvent('click', function(e) {
					if (e) e.stop();

					var __id = this.get('data-id'),
						__date = this.getSiblings('.summary-item-date')[0].get('html'),
						__selectedVotdDateEls = self.votdPopup
						.getElement('#selectedVotdAreas')
						.getElements('.area-container');

					__selectedVotdDateEls.each(function(el) {
						if (el.getElement('span').get('html').split(':')[1]
							.trim() == __date && el.getElement('a')
							.get('data-id') == __id) {
							el.getElement('a').fireEvent('click');
						}
					});

					self.renderInfo();
				});

				_summaryVotdItemContainer.adopt(
					_areaItemName,
					_areaItemDate,
					_areaItemRemoveBtn
				);
				_summaryVotdItemContainer.inject(summaryContainer);
			});

			// </ Selected Votds >

			var countAmount = $('summaryContainer').getElement('.count-amount');
			if (_data.package) {
				countAmount.set('html',
					(1 + +_data.areas.length + +_data.gotds.length + +_data.votds.length) + ' item(s) = CHF ' + (+_data.package.packagePrice + +_data.areas.length * +this.additionalAreaPrice + +_data.gotds.length * +this.gotdPrice + +_data.votds.length * +this.votdPrice) + '. ');
			} else {
				countAmount.set('html',
					( +_data.gotds.length + +_data.votds.length ) + ' item(s) = CHF ' +
					((+_data.gotds.length * +this.gotdPrice) + (+_data.votds.length * +this.votdPrice))
				) + '. ';
			}

			// if (_data.package) {
			// 	console.log('test');
			// 	var totalSummary = new Element('div', {
			// 		class: 'summary-total-container',

			// 		styles: {
			// 			marginTop: '10px',
			// 			float: 'left',
			// 			width: 'calc(100% - 10px)',
			// 			fontSize: '14px',
			// 			color: '#000000',
			// 			fontWeight: 'normal',
			// 			paddingTop: '10px',
			// 			borderTop: '1px solid #d2d2d2'
			// 		},

			// 		html: mOBLang.mOBtotal + ': ' + _data.package.packagePrice + 'CHF ' + (_data.areas.length ? ' + ' + _data.areas.length + ' &times; ' + this.additionalAreaPrice + 'CHF' : '')

			// 			+ (_data.gotds.length ? ' + ' + _data.gotds.length + ' &times; ' + this.gotdPrice + 'CHF' : '') + ' = ' +
			// 			(+_data.package.packagePrice + +_data.areas.length * +this.additionalAreaPrice + +_data.gotds.length * +this.gotdPrice + +_data.votds.length * +this.votdPrice) + 'CHF'
			// 	});
			// } else {
			// 	var totalSummary = new Element('div', {
			// 		class: 'summary-total-container',

			// 		styles: {
			// 			marginTop: '10px',
			// 			float: 'left',
			// 			width: 'calc(100% - 10px)',
			// 			fontSize: '14px',
			// 			color: '#000000',
			// 			fontWeight: 'normal',
			// 			paddingTop: '10px',
			// 			borderTop: '1px solid #d2d2d2'
			// 		},

			// 		html: '<strong>TOTAL</strong>: ' + (_data.gotds.length ? ' + ' + _data.gotds.length + ' &times; ' + this.gotdPrice + 'CHF' : '') + ' = ' +
			// 			(+_data.gotds.length * +this.gotdPrice) + 'CHF'
			// 	});
			// }

			this.container.getElement('.selected-items').fireEvent('cartchanged');
		},

		toggleSummary: function() {
			var summaryContainer = $('summaryContainer');
			$('summaryContainer').toggleClass('expanded');
		},

		showNotification: function(msg, type) {
			if ($('notificationCenter')) $('notificationCenter').dispose();
			var bgColor = '';

			switch (type) {
				case 'error':
					bgColor = '#f44336';
					break;
				case 'success':
					bgColor = '#7abf3e';
					break;
				case 'info':
					bgColor = '#fb8c00';
			}

			var notificationContainer = new Element('div', {
				id: 'notificationCenter',
				class: 'animated',

				styles: {
					width: '100%',
					height: '20px',
					position: 'fixed',
					top: 0,
					left: 0,
					backgroundColor: bgColor,
					visibility: 0,
					zIndex: 1010,
					fontFamily: 'Open Sans, sans-serif',
					color: '#ffffff',
					fontSize: '14px',
					padding: '3px',
					boxShadow: 'rgba(0, 0, 0, 0.2) 0px 2px 2px',
					borderRadius: '0 0 2px 2px'
				},

				html: msg
			});

			notificationContainer.inject(this.container, 'after');
			notificationContainer.addClass('fadeInDown');

			var notifShowTimeout = setTimeout(function() {
				notificationContainer.removeClass('fadeInDown');
				notificationContainer.dispose();
			}, 4000);

			notificationContainer.addEvent('click', function() {
				clearTimeout(notifShowTimeout);
				notificationContainer.removeClass('fadeInDown');
				notificationContainer.dispose();
			});
		},

		parseSessionCart: function(cart) {
			if (typeof cart == 'undefined') {
				return;
			}
			
			var start = cart.activation;
			var areas = cart.areas || [];
			var gotds = cart.gotds || [];
			var votds = cart.votds || [];
			var package = cart.package || null;
			var self = this;

			if (package) {
				var packages = this.container.getElement('.packages-container').getElements('a');

				packages.each(function(el) {
					if (el.get('data-id') == package.packageId) {
						el.fireEvent('click');
					}
				})
			}

			if (start) {
				if (start.date && start.time) {
					var dateOption = this.startTimePopup.getElement('.escort-activation-date')
						.getElements('a')[1];
					dateOption.getElement('input[type="date"]').set('value', start.date);
					dateOption.getElement('input[type="time"]').set('value', start.time);

					dateOption.fireEvent('click');
				}
			}

			if (areas) {
				var areasArr = [];

				areas.each(function(el) {
					areasArr.push(el.a);
				});

				var areasContainer = this.locationPopup.getElement('.additional-areas');
				var addLocationBtn = this.locationPopup.getElement('#addLocation');
				var areasOptions = this.locationPopup.getElement('.additional-areas')
					.getElements('option');
				var okBtn = this.locationPopup.getElements('a')[2];

				areasOptions.each(function(el) {

					if (areasArr.contains(el.get('value'))) {
						areasContainer.set('value', el.get('value'));
						addLocationBtn.fireEvent('click');
					}
				});

				okBtn.fireEvent('click');
			}

			if (gotds) {
				var groupedGotds = {};

				gotds.each(function(gotd) {
					if (!groupedGotds.hasOwnProperty(gotd.a)) {
						groupedGotds[gotd.a] = [];
						groupedGotds[gotd.a].push(gotd.d);
					} else {
						groupedGotds[gotd.a].push(gotd.d);
					}
				});

				Object.each(groupedGotds, function(item, key) {
					var areas = self.gotdPopup.getElement('.additional-areas');
					var areasOptions = areas.getElements('option');
					var selectedGotdDatesContainer = $('selectedGotdDates');

					areasOptions.each(function(it) {
						if (it.get('value') == key) {
							areas.set('value', key);
							areas.fireEvent('change');

							item.each(function(d) {

								var selectedDateEl = new Element('span', {

									class: 'booked-' + d,

									styles: {
										display: 'inline-block',
										width: '57px',
										backgroundColor: '#ac2767',
										color: '#ffffff',
										padding: '5px',
										borderRadius: '3px',
										margin: '2px 3px',
										fontSize: '10px',
										textAlign: 'center'
									},

									html: d
								});

								selectedDateEl.addEvent('click', function(e) {
									if (e) e.stop();

									var selectedDateSingle = this,
										selectedDateElHtml = this.get('html');


									$$('.ob-gotd-calendar-wrapper table')[0]
										.getElements('time')
										.each(function(el) {
											if (el.get('datetime') == selectedDateElHtml) {
												el.removeClass('selected');
												el.getParent('td').removeClass('selected');

												selectedDateSingle.dispose();
											}
										});
								});

								selectedDateEl.inject(selectedGotdDatesContainer);
							});
						}
					});

					self.gotdCalendarPopup.getElement('.ok-btn').fireEvent('click');
					$('addGotdLocation').fireEvent('click');
				});



				self.gotdPopup.getElement('.ok-btn').fireEvent('click');
			}

			if (votds) {
				var groupedVotds = {};

				votds.each(function(votd) {
					if (!groupedVotds.hasOwnProperty(votd.a)) {
						groupedVotds[votd.a] = [];
						groupedVotds[votd.a].push(votd.d);
					} else {
						groupedVotds[votd.a].push(votd.d);
					}
				});

				Object.each(groupedVotds, function(item, key) {
					var areas = self.votdPopup.getElement('.additional-areas');
					var areasOptions = areas.getElements('option');
					var selectedVotdDatesContainer = $('selectedVotdDates');

					areasOptions.each(function(it) {
						if (it.get('value') == key) {
							areas.set('value', key);
							areas.fireEvent('change');

							item.each(function(d) {

								var selectedDateEl = new Element('span', {

									class: 'booked-' + d,

									styles: {
										display: 'inline-block',
										width: '57px',
										backgroundColor: '#ac2767',
										color: '#ffffff',
										padding: '5px',
										borderRadius: '3px',
										margin: '2px 3px',
										fontSize: '10px',
										textAlign: 'center'
									},

									html: d
								});

								selectedDateEl.addEvent('click', function(e) {
									if (e) e.stop();

									var selectedDateSingle = this,
										selectedDateElHtml = this.get('html');


									$$('.ob-votd-calendar-wrapper table')[0]
										.getElements('time')
										.each(function(el) {
											if (el.get('datetime') == selectedDateElHtml) {
												el.removeClass('selected');
												el.getParent('td').removeClass('selected');

												selectedDateSingle.dispose();
											}
										});
								});

								selectedDateEl.inject(selectedVotdDatesContainer);
							});
						}
					});

					self.votdCalendarPopup.getElement('.ok-btn').fireEvent('click');
					$('addVotdLocation').fireEvent('click');
				});



				self.gotdPopup.getElement('.ok-btn').fireEvent('click');
			}

			this.showNotification(mOBLang.mOBCartLoaded, 'success');
		},

		goToPaymentPage: function() {

			var _data = this.getSummaryInfo();
			
			_data.escort_id = JSON.decode(user).escort_data.escort_id;

			new Request({
				url: '/online-billing/set-cart',
				method: 'post',
				data: {
					cart: _data
				},

				onSuccess: function(resp) {
					if (JSON.decode(resp).success == true) {
						window.location.href = '/online-billing/payment';
					}
				}
			}).send();
		}
	};

	mOnlineBilling.init();

}