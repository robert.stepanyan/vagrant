function loadCities(value) {
    var k = 0;
    value = !isNaN(value) ? value : false;
    if (value) {
        $$('#city option').each(function (c) {
                if ((c.getAttribute('data-area-id') !== value) && (c.getAttribute('data-area-id') !== '')) {
                    c.addClass('invisible');
                    c.removeAttribute('selected');
                    c.getParent('select').highlight();
                    $('hidden-row').removeClass('hidden-row');
                } else {
                    k++;
                    c.removeClass('invisible');
                    if (k == 1)
                        c.setAttribute('selected', '');
                    c.removeAttribute('selected');
                    c.getParent('select').highlight();
                }
            }
        );
    } else {
        $('hidden-row').addClass('hidden-row');
        $$('#city option').each(function (c) {
            k++;
            c.removeClass('invisible');
            if (k == 1) {
                c.setAttribute('selected', '');
            }
        });
    }
}

window.addEvent('domready', function () {
    var __city_id = '';
    if ($defined($('referer'))) {
        __city_id = $('referer').get('value');
    }

    if (typeof __city_id !== 'undefined') {
        if (__city_id) {
            loadCities(__city_id);
        }
    }

    if ($("olderThen")) {
        $("olderThen").addEvent('change', function () {
            $('days').setAttribute('disabled', 'disabled');
        });
    }

    if ($$('.publish').length) {
        $$('.publish').addEvent('change', function () {
            if (Number(this.value) === 0) {
                $('days').setAttribute('disabled', 'disabled');
            } else {
                $('days').removeAttribute('disabled');
            }
        });
    }

    // ADVANCED SEARCH MENU *********** START
    var adv_btn = $$('.advanced_search_menu');
    var adv_overlay = $$('.overlay');
    var adv_menu = $$('.advanced_menu');

    if (adv_menu.length) {
        adv_menu.addEvent('click', function () {
            if (!this.hasClass('active')) {
                adv_btn.addClass('active');
                adv_overlay.addClass('active');
                this.addClass('active');
            } else {
                adv_btn.removeClass('active');
                adv_overlay.removeClass('active');
                this.removeClass('active');
            }
        });
    }

    if (adv_overlay.length) {
        adv_overlay.addEvent('click', function () {
            if (this.hasClass('active')) {
                this.removeClass('active');
                adv_btn.removeClass('active');
                adv_menu.removeClass('active');
            }
        });
    }

    if ($$("li.tab-ref").length) {
        $$("li.tab-ref").addEvent('click', function (e) {
            e.stop();
            var ref = this.get('data-key');

            if (!this.hasClass('active')) {
                $$("li.tab-ref").removeClass('active');
                this.addClass('active');
                $$('tr.tab-c').addClass('none').removeClass('active');
                $(ref).addClass('active').removeClass('none');
            }
            adv_overlay.fireEvent('click')
        });
    }
    // *************** END
    if ($('slidable')) {
        new Fx.Slide('slidable').hide();
        var mySlide = new Fx.Slide('slidable');

        $('toggle').addEvent('click', function (e) {
            e = new Event(e);
            mySlide.toggle();
            e.stop();
        });
    }

    if ($$('.work-times-disabled input').length) {
        $$('.work-times-disabled input').addEvent('click', function () {

            var allTr1 = this.getParent('tr').getSiblings('tr')[0];
            var allTr2 = this.getParent('tr').getSiblings('tr')[1];

            if (this.get('checked')) {
                this.setAttribute('checked', '');
                allTr1.getElements('select').each(function (el) {
                    el.removeAttribute('disabled');
                });
                allTr2.getElements('select').each(function (el) {
                    el.removeAttribute('disabled');
                });
                this.getParent('tr').removeClass('work-times-disabled');
            } else {
                this.removeAttribute('checked');
                this.set('checked', false);
                allTr1.getElements('select').each(function (el) {
                    el.setAttribute('disabled', 'disabled');
                });
                allTr2.getElements('select').each(function (el) {
                    el.setAttribute('disabled', 'disabled');
                });
                this.getParent('tr').addClass('work-times-disabled');
            }
        });
    }

    if ($$('.btn-work-days-all').length) {
        $$('.btn-work-days-all').addEvent('click', function () {
            var table = this.getParent('table.searchavailability');
            var firstChecked = table.getElements('tr.inp input:checked')[0];

            var selectsFrom = firstChecked.getParent('tr.inp').getNext('tr.from').getElements('select');
            var selectsUntil = firstChecked.getParent('tr.inp').getNext('tr.until').getElements('select');

            table.getElements('tr.inp input').each(function (el){
                if ( el.get('checked') ) {
                    el.getParent('tr.inp').getNext('tr.from').getElements('select').each(function (sel, i) {
                        sel.set('value', selectsFrom[i].getSelected()[0].get('value')).getParent('td').highlight();
                    });
                    el.getParent('tr.inp').getNext('tr.until').getElements('select').each(function (sel, i) {
                        sel.set('value', selectsUntil[i].getSelected()[0].get('value')).getParent('td').highlight();
                    });
                }
            });
        });
    }
});