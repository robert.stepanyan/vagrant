var Cubix = {};

Cubix.Lang = {};

Cubix.ShowTerms = function(link) {
	window.open(link, 'showTerms', 'menubar=no, resizable=yes, status=no, scrollbars=yes, toolbar=no, width=640, height=480');
	return false;
};

var _log = function(data) {
	if (typeof console && typeof console.log != 'undefined') {
		console.log(data);
	}
};

var _st = function(object) {
	var parts = object.split('.');
	if (parts.length < 2) {
		return _log('invalid parameter, must contain filename with extension');
	}

	var ext = parts[parts.length - 1];

	var base_url = 'https://st.and6.com',
		prefix = '';

	switch (ext) {
		case 'jpg':
		case 'gif':
		case 'png':
			prefix = '/img/';
			break;
		case 'css':
			prefix = '/css/';
			break;
		case 'js':
			prefix = '/js/';
			break;
		default:
			return _log('invalid extension "' + ext + '"');
	}

	url = base_url + prefix + object;

	return url;
};



(function(win) {

	// NAVIGATION

	var menuModule = (function() {
		var _navs = {
			main: {
				id: 'vertical-menu-v2',
			},

			flags: {
				id: 'flags',
			}
		};

		var initMenu = function(menuName) {

			var nav = menuName || 'main',
				navEl = $(_navs[nav].id),
				page = $('page'),
				footer = $('footer_conteiner'),
				body = document.body;

			if (navEl.hasClass('closed')) {
				if (nav === 'main') {
					// body.setStyle('height', +parseInt(navEl.getStyle('height')) + 53 + 'px');
					body.setStyle('overflow', 'hidden');
					if (footer) footer.hide();
				}

				navEl.removeClass('closed');
				navEl.addClass('open');
				page.addClass('menued');
			} else {

				if (nav === 'main') {
					// body.setStyle('height', '100%');
					body.setStyle('overflow', 'visible');
					if (footer) footer.show();
				}

				navEl.addClass('closed');
				page.removeClass('menued');
			}
		};

		return {
			init: initMenu
		};
	})();

	if (!win.menuModule) win.menuModule = menuModule;

})(window);

(function(win) {
	/* COMMENT MODULE */

	var commentModule = (function() {

		var _msgColors = {
			'success': '#8BC34A',
			'failure': '#f44336'
		};

		function _construct() {

			var _commContainer = $('commContainer'),
				_commTextarea = new Element('textarea'),
				_commError = new Element('p', {
					class: 'err hidden',
					html: __('tooShortErr')
				}),
				_actionBtn;

			if (userId) {
				_actionBtn = new Element('a', {
					class: 'send-btn',
					html: __('send')
				});
			} else {
				_actionBtn = new Element('a', {
					class: 'next-btn',
					html: __('next')
				});
			}

			_actionBtn.addEvent('click', function(e) {

				e.stop();

				var text = _commTextarea.get('value');
				_sendComment(text);
			});


			_commContainer.adopt(_commTextarea, _commError, _actionBtn);

			return _commContainer;

		}

		function _sendComment(text) {
			new Request.JSON({
				url: '/comments/ajax-add-comment',
				method: 'POST',

				data: {
					escort_id: escortId,
					user_id: userId,
					comment: text,
					url: window.location.href.split('#')[0] + '#comment_success'
				},

				onSuccess: function(resp) {
					if (resp.status == 'error') {
						$$('#commContainer .err').removeClass('hidden');
					} else if (resp.status == 'pending') {
						var language = (lng == 'de') ? '' : '/' + lng;

						window.location.href = '/private/signin';
					} else {
						$('closeBtn').fireEvent('click');
						_displayMsg(_decorateMsg(
							'success',
							__('successMsg')
						));
					}


				}



			}).send();
		}

		function _getContainer() {
			var _commContainer = $('commContainer');

			if (_commContainer.getElement('textarea')) {
				return _commContainer;
			}

			return false;
		}

		function _checkHash(hash) {
			return window.location.hash == hash;
		}

		function _decorateMsg(type, msg) {
			return new Element('p', {
				html: msg,
				class: 'notification-msg',
				styles: {
					backgroundColor: _msgColors[type]
				}
			}).addEvent('click', function() {
				this.destroy();
			});
		}

		function _displayMsg(msg) {
			$(document.body).append(msg);
			msg.addClass('show');
		}

		function _hideMsg(msg) {
			msg.removeClass('show');

			setTimeout(function() {
				msg.destroy();
			}, 2000);
		}


		function init() {


			/* SUCCESS MESSAGE */

			// Checking for hash to display success message.

			if (_checkHash('#comment_success')) {
				var msg;

				setTimeout(function() {
					msg = _decorateMsg(
						'success',
						__('successMsg')
					);

					_displayMsg(msg);
				}, 500);


				setTimeout(function() {
					_hideMsg(msg);
				}, 5000);
			}

			/* END SUCCESS MESSAGE */

			var _commBtn = $('commentBtn'),
				_closeBtn = $('closeBtn');

			if (!_commBtn) return;

			_closeBtn.addEvent('click', function(e) {
				if (e) e.stop();

				var cont = _getContainer();

				if (cont) {
					cont.set('html', '');
					cont.addClass('closed');
					this.removeClass('hidden').addClass('hidden');
					_commBtn.removeClass('hidden');
				}

				return false;
			});

			_commBtn.addEvent('click', function(e) {
				if (e) e.stop();

				if (_getContainer()) return false;

				this.addClass('hidden');
				_closeBtn.removeClass('hidden');

				_construct().removeClass('closed');

			});
		}

		return {
			init: init
		};
	})();

	if (!win.commentModule) win.commentModule = commentModule;
})(window);

// COMMENTS PAGING => LOAD MORE

(function(win) {
	var CommentsLoader = function(id, props) {
		return this.init(id, props);
	};

	CommentsLoader.prototype = {
		url: '/comments',
		ops: {},

		init: function(id, props) {
			this.el = $(id);
			this.el.page = 1;

			this.opts = props;
			this.bindEvents();
		},

		bindEvents: function() {
			var _self = this;

			this.el.addEvent('click', function(e) {
				e.preventDefault();

				new Request({
					url: _self.url,
					data: {
						ajax: 1,
						page: _self.el.page
					},

					onSuccess: function(resp) {
						var htmlData = new Element('div')
							.adopt(Elements.from(resp))
							.getElement('[id="' + _self.opts.wrap + '"]');

						$(_self.opts.wrap).append(htmlData);
						_self.el.page += 1;
					}
				}).send();
			});
		}
	};

	var LoadMoreBtnSingleton = (function () {
	    var instance;
	 
	    function createInstance() {
	        var loader = new CommentsLoader('loadMoreBtn', {
	        	wrap: 'cWrap'
	        });
	        return loader;
	    }
	 
	    return {
	        getInstance: function () {
	            if (!instance) {
	                instance = createInstance();
	            }
	            return instance;
	        }
	    };
	})();

	if (!win.LoadMoreBtn) win.LoadMoreBtn = LoadMoreBtnSingleton;
})(window);

(function(win) {
	var VideoVoting = function (id, opts) {
		console.log(opts);
		return this.init(id, opts);
	};

	VideoVoting.prototype = {
		el: null,
		opts: {},

		init: function (id, opts) {
			this.el = $(id);
			this.opts = opts;
			this.build();
		},

		build: function () {
			var
				_self = this,
				activeProps = {
					class: 'video-voting-btn active',
					styles: {
						backgroundColor: '#d92b82',
						backgroundImage: 'url("/img/like.png")',
						borderRadius: '6px',
						backgroundRepeat: 'no-repeat',
						backgroundPosition: '95% center',
						backgroundSize: '18px',
						color: '#fff',
						fontSize: '15px',
						padding: '6px 40px 6px 12px',
						textDecoration: 'none',
						marginBottom: '10px'
					},
					html: window.buttonText
				},

				inactiveProps = {
					class: 'video-voting-btn voted',
					styles: {
						background: '#d179a5',
						borderRadius: '6px',
						color: '#fff',
						fontSize: '15px',
						padding: '6px 12px',
						textDecoration: 'none',
						marginBottom: '10px',
						userSelect: 'none',
						cursor: 'auto'
					},
					html: window.buttonTextVoted,
					'data-vote': 'voted'
				};

			this.votesCountBadge = new Element('span', {
				styles: {
					backgroundColor: '#3d5b87',
					borderRadius: '6px',
					color: '#fff',
					fontSize: '14px',
					padding: '6px',
					textDecoration: 'none',
					margin: '10px 0px 10px 10px',
					backgroundImage: 'url("/img/load.gif")',
					backgroundSize: '40px',
					backgroundPosition: 'center center'
				},
				html: ''
			});

			this.votesCountBadge.inject(this.el.getParent('.video-wrapper'), 'after');

			var script = new Element('script', {
				src: '/js/fingerprint2.js'
			});
			console.log(script);

			$(document.body).adopt(script);
			script.addEvent('load', function() {
				console.log('loaded');
				new Fingerprint2().get(function (result) {
					_self.fingerprint = result;
					_self._getVoteCount(function (ln, fps) {
						_self.votingAllowed = fps.indexOf(_self.fingerprint) === -1;
	
						if (_self.votingAllowed) {
							_self.btn = new Element('button', activeProps);
						} else {
							_self.btn = new Element("button", inactiveProps);
						}
	
						_self.btn.inject(_self.el.getParent('.video-wrapper'), 'after');
	
						_self.votesCountBadge.setStyle('background-image', 'none');
						_self.votesCountBadge.set('html', ln);
						_self.votesCountBadge.set('data-count', ln);
	
						_self.bindEvents();
					});
				});
			});


			return this;
		},

		bindEvents: function () {
			'use strict';

			var _self = this;

			if (this.votingAllowed) {
				this.btn.addEvent('click', function () {
					_self._vote(_self.fingerprint);
					_self.btn.removeEvents();
				});
			}
		},

		_vote: function (fp) {
			'use strict';

			var request = new Request({
				url: '/video/vote',
				method: 'POST',
				data: {
					video_id: this.opts.videoId,
					fingerprint: fp
				}
			});

			request.send();

			this.btn.setStyles({
				background: '#d179a5',
				borderRadius: '6px',
				color: '#fff',
				fontSize: '15px',
				padding: '6px 12px',
				textDecoration: 'none',
				marginBottom: '10px',
				userSelect: 'none',
				cursor: 'auto'
			});

			this.btn.set('class', 'video-voting-btn voted');
			this.btn.set('html', window.buttonTextVoted);
			this.votesCountBadge.set('html', parseInt(this.votesCountBadge.get('data-count')) + 1);
		},

		_getVoteCount: function (fn) {
			'use strict';

			var request = new Request({
				url: '/video/vote-count',
				method: 'GET',
				data: {
					video_id: this.opts.videoId,
				},

				onSuccess: function (resp) {
					var response = JSON.decode(resp);

					if (response.status === 'error') {
						console.error('Error occured.');
						return false;
					} else {
						fn(response.length, response.fingerprints);
					}
				}
			});

			request.send();
		}
	};

	if (!win.VideoVoting) win.VideoVoting = VideoVoting;
})(window);


window.addEvent('domready', function() {
	var chat_icon = $$('.chat-icon');
	if( chat_icon ){
        chat_icon.addEvent('click', function (e) {

            MobileChat.Lists.openChat();

            e.preventDefault();
            if (this.get('data-success') && this.get('data-success') === 'ok') {
                $('chat-wrapper').removeClass('hide');
                if( this.getElement('.new_messages_count') ){
                    this.getElement('.new_messages_count').destroy();
                }
            } else if (!this.get('href') && this.get('data-chat-msg')) {

            } else if (this.get('href')) {
                window.location.href = this.get('href');
            }


            MobileChat.Lists.sortingConversations();
            MobileChat.Lists.searchInConversations();
        });
	}

    var chat_online = $('chatOnline');
    if( chat_online ){
        chat_online.addEvent('click', function(e){
            var user_id = parseInt( this.get('data-id') );
            MobileChat.Lists.openChat();

            e.preventDefault();

            $('chat-wrapper').removeClass('hide');
            var elm = $('user_' + user_id);

            if( elm ){
                elm.fireEvent('mouseup');
            }
        });
	}

	if($('video-chat-box')){
		if( !headerVars.currentUser ){
				$$('#video-chat-box .vc-button').addEvent('click', function(){
					Cubix.Cam.Popup.url = '/and6-cams/login/';
					Cubix.Cam.Popup.Show(400,375);
				})
			
		}else{
			if( headerVars.currentUser.user_type == 'member'){
				$$('#video-chat-box .vc-button').addEvent('click', function(){
					var duration = $(this).get('data-type');
					var escortId = $('escort_id').get('value');
					Cubix.VCPopup.url = '/escorts/video-chat-popup?escort_id=' + escortId + '&duration=' + duration;
					Cubix.VCPopup.Show(250, 580);
				})
			}else{
				$$('#video-chat-box .vc-button').addEvent('click', function(){
					alert('Permission denied')
				})
			}
		}
	}

});