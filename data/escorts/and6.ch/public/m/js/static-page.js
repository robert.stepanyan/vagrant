;(function (window) {
	var Nav = function (params) {
		return this.init(params);
	};

	Nav.prototype = {
		isOpen: false,

		init: function (params) {
			this
				.setOpts(params)
				.bindEvents();
		},

		setOpts: function (params) {
			var _params = params || {};

			this.opts = Object.merge({}, _params);

			return this;
		},

		build: function () {
			this.nav.addClass('animated');
		},

		_close: function (btn, nav) {
			btn.removeClass('open');
			nav.removeClass('open');
		},

		_open: function(btn, nav) {
			btn.addClass('open');
			nav.addClass('open');
		},

		bindEvents: function () {
			var _this = this;
			this.opts.el.addEvent('click', function() {
				var _btn = this;
				if (!_this.isOpen) {
					_this._open(_btn, _this.opts.nav);
					_this.isOpen = true;
				} else {
					_this._close(_btn, _this.opts.nav);	
					_this.isOpen = false;
				}
			});
		}
	};

	!window.Nav && (window.Nav = Nav);

})(window);