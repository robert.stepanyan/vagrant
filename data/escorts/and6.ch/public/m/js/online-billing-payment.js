// < Payment >
if ($$('.online-billing-payment').length > 0) {
	
	var mOnlineBillingPayments = {
		container: $$('.online-billing-payment')[0],
		cryptoSelected : false,
		additionalAreaPrice: parseInt($('additionalAreaPrice').get('value')),
		gotdPrice: parseInt($('gotdPrice').get('value')),
		votdPrice: parseInt($('votdPrice').get('value')),
		gotdCryptoPrice : parseInt($('gotdCryptoPrice').get('value')),
		votdCryptoPrice : parseInt($('votdCryptoPrice').get('value')),
		
		init: function() {
			this.selectPaymentMethod();
			this.eventManager();
			this.renderInfo();
		},

		selectPaymentMethod: function() {
			var self = this,
				paymentOptions = this.container.getElements('.payment-container a');

			if (paymentOptions.length > 1) {
				var creditCardOptions = paymentOptions[1].getElements('label');
				var paymentOptionsRadio = paymentOptions[1].getElements('input');
			} else {
				var creditCardOptions = paymentOptions[0].getElements('label');
				var paymentOptionsRadio = paymentOptions[0].getElements('input');
			}

			paymentOptions.addEvent('click', function() {
				paymentOptions.removeClass('selected');
				this.addClass('selected');
				if (this.get('data-id') === 'm') {
					paymentOptionsRadio.set('checked', false);
				}
				
				// BTC payment price increase logic 
				if(this.get('data-id') == 'cc_coinpass'){
					this.cryptoSelected = true;
					$$('.chf-price').addClass('none');
					$$('.crypto-price').removeClass('none');
				}else{
					this.cryptoSelected = false;
					$$('.crypto-price').addClass('none');
					$$('.chf-price').removeClass('none');
				}
			});
		},

		eventManager: function() {
			var self = this;

			$('summaryContainer').addEvent('click', function(e) {
				e.stop();
				self.toggleSummary();
			});

			$$('.next-step-button')[0].addEvent('click', function(e) {
				e.stop();
				self.checkout();
			});
		},

		toggleSummary: function() {
			var summaryContainer = $('summaryContainer');
			$('summaryContainer').toggleClass('expanded');
		},

		renderInfo: function() {
			var self = this;
			var isLoaded = false;

			// setTimeout(function() {
			// 	var isMissing = $$('.escort-gotd').length == 0 
			// 		|| $$('.escort-additional-areas').length == 0 
			// 		|| $$('.escort-votd').length == 0;

			// 	if (isMissing) {
			// 		self.renderInfo();
			// 	} else {
			// 		isLoaded = true;
			// 	}
			// }, 1000);
			
			var summaryContainer = this.container.getElement('.selected-items');
			summaryContainer.set('html', '');
			var totalPrice = 0;
			var totalCryptoPrice = 0;
			var totalItemsAmount = 0;
			var isMultiplePackages = Object.keys(existingCart).length > 1 ? true : false;
			
			for (let [escortId, _data] of Object.entries(existingCart) ) {
				
				if (!_data.package && !_data.gotds && !_data.votds) continue;

				var escortIdInfo = isMultiplePackages  ? '<strong> ID <span style="color:rgb(172, 39, 103)">' + escortId + '<span></strong> ' : '';	

				if (_data.package) {
					// < Selected Package >

					var _summaryPackageItemContainer = new Element('div', {
							class: 'summary-package-item-container',

							styles: {
								marginTop: '25px',
								float: 'left',
								width: '100%'
							}
						}),
									
					_itemName = new Element('span', {
						class: 'summary-item-name',

						styles: {
							display: 'block',
							width: 'calc(100% - 30px)',
							height: '20px',
							overflow: 'hidden',
							textOverflow: 'ellipsis',
							whiteSpace: 'nowrap',
							float: 'left',
							fontSize: '14px',
							fontWeight: 'normal',
							color: '#000000'
						},

						html: escortIdInfo + '<strong>' + _data.package.packageTitle + '</strong> - ' +
							'<span style="display: inline-block; padding: 0 5px; border-radius: 3px;' +
							' color: #ffffff; background-color: rgb(126, 137, 154);' +
							' text-shadow: 1px 1px 0 rgba(94, 104, 119, 0.68);">' + ((_data.activation.date && _data.activation.time) ?
								(_data.activation.date + ' ' + _data.activation.time) :
								mOBLang.mOBImmediately) + '</span>'
					});

					_summaryPackageItemContainer.adopt(_itemName);
					_summaryPackageItemContainer.inject(summaryContainer);

					// </ Selected Package >

					// < Selected Locations > 
					if (_data.areas) {
						_itemsElementGroup = [];

						_data.areas.each(function(el) {
							var _summaryLocationItemContainer = new Element('div', {
									class: 'summary-location-item-container',

									styles: {
										marginTop: '10px',
										float: 'left',
										width: '100%'
									}
								}),

								_areaItemName = new Element('span', {
									class: 'summary-item-name',

									styles: {
										display: 'block',
										width: 'calc(100% - 30px)',
										height: '20px',
										overflow: 'hidden',
										textOverflow: 'ellipsis',
										whiteSpace: 'nowrap',
										float: 'left',
										fontSize: '14px',
										fontWeight: 'normal',
										color: '#000000'
									},

									html: '<strong style="font-size: 16px">+</strong> ' + el.t
								});

							_summaryLocationItemContainer.adopt(_areaItemName);
							_summaryLocationItemContainer.inject(summaryContainer);
						});
					} else {
						_data.areas = [];
					}
					// </ Selected Locations >
				}

				// < Selected Gotds > 
				if (_data.gotds) {
					_data.gotds.each(function(el) {
						var _summaryGotdItemContainer = new Element('div', {
								class: 'summary-gotd-item-container',

								styles: {
									marginTop: '10px',
									float: 'left',
									width: '100%'
								}
							}),

							_areaItemName = new Element('span', {
								class: 'summary-item-name',

								styles: {
									display: 'block',
									width: 'calc(100% - 110px)',
									height: '20px',
									overflow: 'hidden',
									textOverflow: 'ellipsis',
									whiteSpace: 'nowrap',
									float: 'left',
									fontSize: '14px',
									fontWeight: 'normal',
									color: '#000000'
								},

								html: '<strong style="font-size: 12px;' + ' color: #7e899a;">GOTD: </strong> ' + (el.t || '')
							}),

							_areaItemDate = new Element('span', {
								class: 'summary-item-date',

								styles: {
									display: 'block',
									width: '80px',
									float: 'left',
									fontSize: '12px',
									fontWeight: 'normal',
									color: 'rgb(172, 39, 103)',
									lineHeight: '22px',
									fontWeight: 'bold'

								},

								html: el.d
							});

						_summaryGotdItemContainer.adopt(
							_areaItemName,
							_areaItemDate
						);
						_summaryGotdItemContainer.inject(summaryContainer);
					});
				} else {
					_data.gotds = [];
				}
				// </ Selected Gotds >

				// < Selected Votds > 
				if (_data.votds) {
					_data.votds.each(function(el) {
						var _summaryVotdItemContainer = new Element('div', {
								class: 'summary-votd-item-container',

								styles: {
									marginTop: '10px',
									float: 'left',
									width: '100%'
								}
							}),

							_areaItemName = new Element('span', {
								class: 'summary-item-name',

								styles: {
									display: 'block',
									width: 'calc(100% - 110px)',
									height: '20px',
									overflow: 'hidden',
									textOverflow: 'ellipsis',
									whiteSpace: 'nowrap',
									float: 'left',
									fontSize: '14px',
									fontWeight: 'normal',
									color: '#000000'
								},

								html: '<strong style="font-size: 12px;' + ' color: #7e899a;">VOTD: </strong> ' + (el.t || '')
							}),

							_areaItemDate = new Element('span', {
								class: 'summary-item-date',

								styles: {
									display: 'block',
									width: '80px',
									float: 'left',
									fontSize: '12px',
									fontWeight: 'normal',
									color: 'rgb(172, 39, 103)',
									lineHeight: '22px',
									fontWeight: 'bold'

								},

								html: el.d
							});

						_summaryVotdItemContainer.adopt(
							_areaItemName,
							_areaItemDate
						);
						_summaryVotdItemContainer.inject(summaryContainer);
					});
				} else {
					_data.votds = [];
				}
				// </ Selected Votds >
					
				var totalTitle = isMultiplePackages ? escortIdInfo : mOBLang.mOBTotal;
				var summaryTotalContainerStyles = {
					marginTop: '10px',
					float: 'left',
					width: 'calc(100% - 10px)',
					fontSize: '14px',
					color: '#000000',
					fontWeight: 'normal',
					paddingTop: '10px'
				};
				
				var gotdPrice = this.cryptoSelected ? this.gotdCryptoPrice : this.gotdPrice;
				var votdPrice = this.cryptoSelected ? this.votdCryptoPrice : this.votdPrice;
				
				if (_data.package) {
					
					totalItemsAmount +=	( 1 + (+_data.areas.length) + (+_data.gotds.length) + (+_data.votds.length));
					var price = (+_data.package.packagePrice + +_data.areas.length * +this.additionalAreaPrice + +_data.gotds.length * +this.gotdPrice + +_data.votds.length * +this.votdPrice);
					var cryptoPrice = (+_data.package.packageCryptoPrice + +_data.areas.length * +this.additionalAreaPrice + +_data.gotds.length * +this.gotdCryptoPrice + +_data.votds.length * +this.votdCryptoPrice);
					
					var totalSummary = new Element('div', {
						class: 'summary-total-container',
						styles: summaryTotalContainerStyles,
						html: totalTitle + ': ' + self.generatePrices(_data.package.packagePrice , _data.package.packageCryptoPrice)  + 'CHF ' + (_data.areas.length ? ' + ' + _data.areas.length + ' &times; ' + this.additionalAreaPrice + 'CHF' : '')
							+ (_data.gotds.length ? ' + ' +  _data.gotds.length + ' &times; ' +  self.generatePrices(this.gotdPrice, this.gotdCryptoPrice) + 'CHF' : '')
							+ (_data.votds.length ? ' + ' + _data.votds.length + ' &times; ' + self.generatePrices(this.votdPrice, this.votdCryptoPrice) + 'CHF' : '') + ' = '
							+ self.generatePrices(price, cryptoPrice) + 'CHF'
					});
				} else {
					
					totalItemsAmount += ( +_data.gotds.length + +_data.votds.length ); 
					var price = ((+_data.gotds.length * + this.gotdPrice) + (+ _data.votds.length * + this.votdPrice));
					var cryptoPrice = (( +_data.gotds.length * + this.gotdCryptoPrice ) + ( +_data.votds.length * + this.votdCryptoPrice));
					var totalSummary = new Element('div', {
						class: 'summary-total-container',
						styles: summaryTotalContainerStyles,
						html: totalTitle + ': ' + (_data.gotds.length ? +_data.gotds.length + ' &times; ' + this.gotdPrice + 'CHF' : '') + '. '
							+ (_data.votds.length ? +_data.votds.length + ' &times; ' + this.votdPrice + 'CHF' : '')
							+ ' = ' + self.generatePrices(price, cryptoPrice) + 'CHF'
					});
				}
				
				totalSummary.inject($('summaryContainer'));
				totalPrice += price;
				totalCryptoPrice += cryptoPrice;
			}
			
			var countAmount = $('summaryContainer').getElement('.count-amount');
			countAmount.set('html',	totalItemsAmount + ' item(s) = CHF ' + self.generatePrices(totalPrice, totalCryptoPrice) + '. ');
			
			if(isMultiplePackages ){
				new Element('div', {
					class: 'summary-total-container',
					styles: summaryTotalContainerStyles,
					html: mOBLang.mOBTotal + ' : ' + self.generatePrices(totalPrice, totalCryptoPrice) + 'CHF'	
				}).inject($('summaryContainer'));
			}
		},
		
		generatePrices: function(price, cryptoPrice){
			var priceText = `<span class="chf-price"> ${price} </span>`;
			var cryptoPriceText = `<span class="crypto-price none" > ${cryptoPrice} </span>`;
			return priceText + cryptoPriceText;
		},
		
		checkout: function(paymentType) {
			if ($('yold_18') && $$('input[name=yold_18]:checked').length > 0) {
			var paymentMethod = $$('.payment-container .selected')[0].get('data-id');
			$$('.btn-overlay')[0].removeClass('hidden');
			
			new Request({
				url: '/online-billing/checkout',
				method: 'post',
				data: {
					payment_method: paymentMethod
				},

				onSuccess: function(resp) {
					var r = JSON.decode(resp);
					if (r.status === 'success') {
						if ((paymentMethod === 'm') || (paymentMethod === 'cc_epg' || paymentMethod === 'cc_2000charge' || paymentMethod === 'cc_faxin')) {
							window.location.href = r.url;	
						} else if (paymentMethod === 'cc_twispay') {
							var $form = $('twispayForm').set('html', r.form);
							setTimeout(function() {
								$form.getElement('form').submit();
								/* $form
									.getElement('input[type="submit"]')
									.fireEvent('click');
									*/
								}, 0);
						} else if(paymentMethod === 'cc_postfinance' || paymentMethod === 'cc_coinpass' ){
							var $form = $('postfinanceForm').set('html', r.form);
							setTimeout(function() {
								$form.getElement('form').submit();
								/* $form
									.getElement('input[type="submit"]')
									.fireEvent('click');
									*/
								}, 0);
						}
					} else {
						alert('Something went wrong');
						window.location.reload();
					}
				}
			}).send();
			}else{
				alert('Age confirmation required');
				$('18-yold-label').addClass('shaker');
				setTimeout(function(){ $('18-yold-label').removeClass('shaker'); }, 1000);
			}
		}
	};

	mOnlineBillingPayments.init();

	// </ Payment  >
}