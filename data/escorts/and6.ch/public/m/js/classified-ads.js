/* --> LocationHash */
Cubix.LocationHash = {};

Cubix.LocationHash.Set = function (hash) {
    if ( hash.length ) {
        document.location.hash = '#' + hash;
    }
    else {
        document.location.hash = '';
    }
    
    return false;
}

Cubix.LocationHash.Parse = function () {
    var hash = document.location.hash.substring(1);
    
    if ( ! hash.length ) {
        return {};
    }
    
    var params = hash.split(';');
    
    var filter = '';
    
    var not_array_params = ['page', 'category', 'city', 'text', 'pp'];
    
    params.each(function (param) {
        var key_value = param.split('=');
        
        var key = key_value[0];
        var val = key_value[1];
        
        if ( val === undefined ) return;
        
        val = val.split(',');
                
        val.each(function(it) {
            filter += key + ( ( ! not_array_params.contains(key) ) ? '[]=' : '=' ) + it + '&';
        });
    });
    
    return filter;
}

Cubix.LocationHash.Make = function (params) {
    
    var hash = '';
    var sep = ';';
    var value_glue = ',';
    
    var places = $$('#cads-filter-box, .per-page');
    
    var map = {};
    places.each(function(place) {
        var inputs = place.getElements('input[type=text]');
        inputs.append(place.getElements('input[type=hidden]'));
        //inputs.append(place.getElements('input[type=text]'));
        inputs.each(function(input) {
            if ( input.get('value').length ) {
                var index = input.get('name').replace('[]', '');
                if ( map[index] === undefined ) map[index] = new Array();
                map[index].append([input.get('value')]);
            }
        });
        
        var selects = place.getElements('select');
        selects.each(function(select) {
            
            if ( select.getSelected().get('value') != 0 ) {
                var index = select.get('name').replace('[]', '');
                if ( map[index] === undefined ) map[index] = new Array();
                map[index].append([select.getSelected().get('value')]);
            }
        });
    });
    
    map = $merge(map, params);
    
    for (var i in map) {
        hash += i + '=' + map[i].join(value_glue) + sep;
    }
    
    return hash;
}
/* <-- */

/* --> HashController */
Cubix.HashController = {
    _current: '',
    
    init: function () {
        setInterval('Cubix.HashController.check()', 100);
    },
    
    check: function () {
        var hash = document.location.hash.substring(1);
        
        if (hash != this._current) {
            this._current = hash;
                    
            var data = Cubix.LocationHash.Parse();
            
            
            Cubix.ClassifiedAds.Load(data);
            
            Cubix.ClassifiedAds.LoadFilters(data);
        }
    }
};
/* <-- */

Cubix.Filter = {};
Cubix.Filter.Set = function (filter) {
        
    Cubix.LocationHash.Set(Cubix.LocationHash.Make(filter));

    return false;
}

var initToggleAd = function() {
    var show_text = Cubix.ClassifiedAds.show_text;
    var hide_text = Cubix.ClassifiedAds.hide_text;

    $$('.show-hide-btn').addEvent('click', function(e){
        e.stop();
        var content = this.getPrevious('div.content');
        var text = content.getElements('div.text')[0];
        var shadow = text.getElements('div.shadow')[0];
        var control_box = text.getElements('div.control-box')[0];

        if ( this.hasClass('hide') ) {
            content.addClass('collapsed');
            text.addClass('collapsed');
            control_box.addClass('none');
            shadow.removeClass('none');
            this.removeClass('hide').addClass('show');
            this.set('html', show_text);
        } else {
            content.removeClass('collapsed');
            text.removeClass('collapsed');
            control_box.removeClass('none');
            shadow.addClass('none');
            this.removeClass('show').addClass('hide');
            this.set('html', hide_text);
        }           
    });
};

var initRepostButton = function () {
    $$('.repost').addEvent('click', function(e){
        e.stop();
        var adId = this.getAttribute('data-id');

        // var overlay = new Cubix.Overlay($(Cubix.ClassifiedAds.container), {
        //     loader: _st('loader-small.gif'),
        //     position: '50%'
        // });
        
        // overlay.disable();

        new Request({
            url: '/private-v2/repost-ad',
            method: 'POST',
            data: {"id":adId},
            onSuccess: function (resp) {
                
                
                window.location.reload()
                // overlay.enable();    
            }
        }).send();
    });
}
var initControlBoxBtns = function() {
    $$('.tell-friend').addEvent('click', function(e){
        e.stop();
        Cubix.RPopup.url = this.get('rel');
        Cubix.RPopup.rec_url = this.get('url');
        Cubix.RPopup.type = 'tell-friend';
        Cubix.RPopup.location = document.body;
        
        var c = this.getCoordinates(document.body);
        Cubix.RPopup.Show(c.left, c.top);
    });

    $$('.report-problem').addEvent('click', function(e){
        e.stop();
        Cubix.RPopup.url = this.get('rel');
        Cubix.RPopup.type = 'report-problem';
        Cubix.RPopup.location = document.body;
        
        var c = this.getCoordinates(document.body);
        Cubix.RPopup.Show(c.left, c.top);
    });
    
    $$('.print').addEvent('click', function(e){
        e.stop();
        var url = this.get('rel');
        
        window.open (url, "mywindow","status=0,toolbar=0,location=0,menubar=0,resizable=1,height=500,width=750,scrollbars=1");
    });
};

Cubix.ClassifiedAds = {};

Cubix.ClassifiedAds.container = 'list';
Cubix.ClassifiedAds.filter_container = 'filter-container';

Cubix.ClassifiedAds.filter_url = '';
Cubix.ClassifiedAds.list_url = '';

Cubix.ClassifiedAds.Load = function (data) {
    var url = Cubix.ClassifiedAds.list_url;//'/classified-ads/ajax-list?ajax';
    
    // var overlay = new Cubix.Overlay($(Cubix.ClassifiedAds.container), {
    //     loader: _st('loader-small.gif'),
    //     position: '50%'
    // });
    
    // overlay.disable();
    
    new Request({
        url: url,
        method: 'POST',
        data: data,
        onSuccess: function (resp) {
            
            $(Cubix.ClassifiedAds.container).set('html', resp);
            
            initToggleAd();
            initRepostButton();
            initControlBoxBtns();
            
            Slimbox.scanPage();
            
            Cubix.ClassifiedAds.InitPerPage();
            
            // overlay.enable();
        }
    }).send();
    
    return false;
}

Cubix.ClassifiedAds.LoadFilters = function (data) {
    
    var url = Cubix.ClassifiedAds.filter_url;//'/classified-ads/ajax-filter?ajax';
    
    // var overlay = new Cubix.Overlay($(Cubix.ClassifiedAds.filter_container), {
    //     loader: _st('loader-small.gif'),
    //     position: '50%'
    // });
    
    // overlay.disable();
    
    new Request({
        url: url,
        method: 'POST',
        data: data,
        onSuccess: function (resp) {            
            $(Cubix.ClassifiedAds.filter_container).set('html', resp);
            // overlay.enable();
            
            Cubix.HashController.InitAll();         
        }
    }).send();
    
    return false;
}

Cubix.ClassifiedAds.InitFilters = function(){
    $$('.search-btn').addEvent('click', function(e){
        e.stop();
        Cubix.LocationHash.Set(Cubix.LocationHash.Make());
    });
};

Cubix.ClassifiedAds.InitPerPage = function(){
    $$('.per-page select').addEvent('change', function(e){
        e.stop();
        Cubix.LocationHash.Set(Cubix.LocationHash.Make());
    });
};

Cubix.HashController.InitAll = function(){
    Cubix.HashController.init();
    
    Cubix.ClassifiedAds.InitFilters();
    
    Cubix.ClassifiedAds.InitPerPage();
};

window.addEvent('domready', function () {
    Cubix.HashController.InitAll();
    
    initToggleAd();

    initRepostButton();
    
    initControlBoxBtns();
    
    Slimbox.scanPage();

    
    
    /*if ( ! Cubix.LocationHash.Parse().length ) {
        Cubix.LocationHash.Set(Cubix.LocationHash.Make());
    }*/
});
