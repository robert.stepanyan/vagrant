var InitMap = function(position)
{
	console.log(position);

    var dest_lon,dest_lat;

    var resp = $('escort-map').get('html');
	var obj = JSON.decode(resp);

	if(obj.latitude && obj.longitude){
        var dest_lon = obj.longitude;
		var dest_lat = obj.latitude;
    }else{
		var address = $('address').get('value');
        new Request({
            url: '/geocode/',
            method: 'GET',
            async:false,
            data: { addr: address },
            onSuccess: function (resp) {
                var latLang = JSON.decode(resp);
                dest_lon = latLang['lng'];
                dest_lat = latLang['lat'];
            }

        }).send();
	}

	//var lat = 46.833333;
	//var lon = 8.333333;

	if ( position ) {
		lat = position.coords.latitude;
		lon = position.coords.longitude;
	}
	else
	{
		lat = dest_lat;
		lon = dest_lon;
	}

	var directionsDisplay = new google.maps.DirectionsRenderer();
	var directionsService = new google.maps.DirectionsService();

	var point = new google.maps.LatLng(lat, lon);

	var mapOptions = {
		center: point,
		zoom: 10,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	var map = new google.maps.Map($('escort-map-wrap'), mapOptions);

	directionsDisplay.setMap(map);

	var request = {
		origin: point,
		destination: new google.maps.LatLng(dest_lat, dest_lon),
		travelMode: google.maps.TravelMode.DRIVING
	};
	directionsService.route(request, function(result, status) {
		if (status == google.maps.DirectionsStatus.OK) {
			directionsDisplay.setDirections(result);
		}
	});

};

window.addEvent('domready', function () {
	if ($('escort-map'))
	{
		InitMap();
		// navigator.geolocation.getCurrentPosition(InitMap, InitMap);

		$('t_contacts').addClass('active');
	}
});