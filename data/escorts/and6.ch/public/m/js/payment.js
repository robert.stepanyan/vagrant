;(function (window, document) {

    function setInputFilter(textbox, inputFilter) {
        ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function (event) {
            textbox.addEventListener(event, function () {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
            });
        });
    };

    function submitPaymentForm(data) {

        $$('.payment-form__wrapper').addClass('processing');

        new Request({
            url: '/payment/checkout',
            data: data,
            method: 'post',
            onSuccess: function (response) {
                response = JSON.decode(response, true);

                $$('.payment-form__wrapper').removeClass('processing');

                switch (response.status) {
                    case 'twispay_success':
                        var formHtml = response.form;
                        $$('body')[0].set('html', $$('body')[0].get('html') + formHtml.replace(/ \"/g, "\""));
                        $('twispay-payment-form').submit();
                        break;

                    case 'error':
                        alert('Currently payment system is not available, please check back soon. ' + (response.error ? 'Details ' + response.error : ''));
                        break;
                }

            }
        }).send();
    }


    document.addEventListener("DOMContentLoaded", function () {

        var numbersOnlyInputs = document.querySelectorAll("[data-validate='true'][data-accept='number']");

        numbersOnlyInputs.forEach(function ($element) {
            setInputFilter($element, function (value) {
                return /^\d*\.?\d*$/.test(value); // Allow digits and '.' only, using a RegExp
            });
        });

        document.getElementById('pay-btn').addEventListener('click', function () {

            var data = {
                amount: document.getElementById('amount-inp').value,
                paymentMethod: document.querySelector('[name="paymentMethod"]:checked').value,
            };

            if (data.amount <= 0) {
                return alert("Invalid Amount specified");
            }

            if (!['twispay', 'ecardon'].includes(data.paymentMethod)) {
                return alert("Invalid PaymentMethod specified");
            }

            submitPaymentForm(data);
        });
    });

})(this, document);