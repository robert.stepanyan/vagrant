var MCubix = {};

MCubix.Cookies = {
    set: function( cname, cvalue, exdays, domain, path )
    {
        var d = new Date();
        d.setTime( d.getTime() + ( exdays * 24 * 60 * 60 * 1000 ) );
        var expires = "expires=" + d.toGMTString();
        var cookie_str = cname + "=" + cvalue + "; " + expires;

        if ( domain ){
            cookie_str += "; domain=" + encodeURI ( domain );
        }

        if ( path ){
            cookie_str += "; path=" + encodeURI ( path );
        }

        document.cookie = cookie_str;
    },

    get: function( cname )
    {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for( var i=0; i<ca.length; i++ )
        {
            var c = ca[i].trim();
            if ( c.indexOf( name ) == 0 ) return c.substring( name.length,c.length );
        }
        return "";
    },

    delete: function( cname ){
        document.cookie = cname + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }
};



/***************** Karo Start *****************/

var Private = {};

Private.Message = {
    init: function( showname, escort_id ){
        var self = this;
        $$('.private-messanger').addEvent('click', function(){
            var data = { "participant_id" : $(this).getProperty('data-id'), "participant_type" : 'escort'/*$(this).getProperty('data-type')*/ }
            //console.log( data );
            self.sendRequest( data, showname, escort_id );
        })
    },

    sendRequest: function( data, showname, escort_id ){
        var self = this;
        var block = $('private-message-block');
        var wrapper = $('private-message-wrapper');
        var scoller = new Fx.Scroll(window);

        new Request({
            url: '/private-messaging/send-message-ajax',
            data: data,
            method: 'get',
            onProgress: function(){
                wrapper.setStyles({ 'display' : 'block', 'opacity' : 1, 'visibility' : 'visible' });
            },
            onSuccess: function (resp) {
                //resp = JSON.decode(resp, true);
                if( resp == 'signin' ){
                    window.location.href = "/private/signin";
                }
                if ( ! resp ) return;
                //wrapper.empty();
                block.set('html', resp);
                scoller.toElement(block);
                self.sendMessage( escort_id );
            },
            onComplete: function(){

            }
        }).send();
    },

    sendMessage: function(escort_id){
        var sendButton = $$('.red-btn');

        sendButton.addEvent('click', function(e){
            e.preventDefault();

            var data = {
                'participant'	: $('participant').get("value"),
                'captcha' : $('f-captcha').get('value'),
                'message' : $('privateMessage').get('value'),
                'escort_id' : escort_id
            };

            new Request({
                url: '/private-messaging/send-message-ajax',
                data: data,
                method: 'post',
                onSuccess: function (resp) {
                    //console.log( resp );
                    resp = JSON.decode(resp, true);
                    if( resp.status == 'error' ){
                        if( typeof resp.msgs.captcha != 'undefined' ){
                            if( resp.msgs.captcha == '' ){
                                $('f-captcha').addClass('field-error');
                            } else {
                                $('f-captcha').addClass('field-error').set('value', 'incorect value');
                            }
                        } else {
                            $('f-captcha').removeClass('field-error');
                        }

                        if( resp.msgs.fmessage == '' ){
                            $('privateMessage').addClass('field-error');
                        } else {
                            $('privateMessage').removeClass('field-error');
                        }
                    } else {
                        $('privateMessage').set('value', '').removeClass('field-error');
                        $('f-captcha').set('value', '').removeClass('field-error');
                        $$('.send-success').set('html', resp.msg);
                    }
                }
            }).send();
        })
    }
};

/***************** Karo End *****************/