;(function (window) {
	var Nav = function (params) {
		return this.init(params);
	};

	Nav.prototype = {
		isOpen: false,

		init: function (params) {
			this
				.setOpts(params)
				.bindEvents();
		},

		setOpts: function (params) {
			var _params = params || {};

			this.opts = Object.merge({}, _params);

			return this;
		},

		build: function () {
			this.nav.addClass('animated');
		},

		_close: function (btn, nav) {
			btn.removeClass('open');
			nav.removeClass('open');
		},

		_open: function(btn, nav) {
			btn.addClass('open');
			nav.addClass('open');
		},

		bindEvents: function () {
			var _this = this;
			this.opts.el.addEvent('click', function() {
				var _btn = this;
				if (!_this.isOpen) {
					_this._open(_btn, _this.opts.nav);
					_this.isOpen = true;
				} else {
					_this._close(_btn, _this.opts.nav);	
					_this.isOpen = false;
				}
			});
		}
	};

	!window.Nav && (window.Nav = Nav);

})(window);



;(function (window) {
	var Filter = function (params) {
		return this.init(params);
	};

	Filter.prototype = {
		filterParams: {},

		init: function (params) {
			this.dest = params.destination || document.body;
			this.source = params.source || document.body;
			this.loadMoreBtn = params.loadMoreBtn || document.body;
			this
				.parseHash()
				.bindEvents();

			if (!this.dest.getElements('*').length) {
				this.getList();
			}
		},

		parseHash: function () {
			var 
				_this = this,
				hash = window.location.hash.substring(1),
				filters = hash.split(';');

			filters.each(function (pairStr) {
				var pair = pairStr.split('=');

				_this.filterParams[pair[0]] = pair[1];
			});

			return this;
		},

		bindEvents: function () {
			var _this = this;

			window.addEventListener('hashchange', function () {
				_this.dest.set('html', '');
				_this.loadMoreBtn.set('data-page', 0);
				_this.getList();
			});

			this.source
				.getElements('select, radio, input')
				.addEvent('change', function () {
					if (this.get('type') === 'radio') {
						var parent = this.getParent('.card-footer-item');
						parent.getSiblings('.card-footer-item').removeClass('is-danger');
						parent.addClass('is-danger');
					}

					var filterParams = {};

					_this
						.source
						.getElements('select, radio, input')
						.each(function (el) {
							if (el.get('type') === 'radio') {
								if (el.get('checked')) {
									filterParams[el.get('name')] = el.get('value');
								}
							} else {
								filterParams[el.get('name')] = el.get('value');
							}
						});

					_this.updateFiler(filterParams);
					_this.filterParams = filterParams;

			});

			this.loadMoreBtn.addEvent('click', function () {
				var 
					pageCount = $$('.page-count')[0].get('value'),
					page = +this.get('data-page') + 1;

				if (pageCount <= page) {
					this.hide();
				} else {
					this.show();
				}

				this.set('data-page', page);

				_this.filterParams.page = page;
				$$('.page-count')[0].destroy();
				_this.getList();

			});

			return this;
		},

		updateFiler: function (filterParams) {
			var hash = '#';

			Object.each(filterParams, function (value, key) {
				hash += key + '=' + value + ';';
			});

			window.location.hash = hash;
		},

		getList: function () {
			var _this = this;

			new Request({
				url: '/latest-actions/ajax-list?ajax',
				data: this.filterParams,

				onProgress: function () {
					_this.loadMoreBtn.addClass('is-loading');
				},

				onSuccess: function (resp) {
					if (0 === +_this.filterParams.page) {
						_this.dest.set('html', resp);
					} else {
						Elements.from(resp).inject(_this.dest);
					}
					
					_this.loadMoreBtn.removeClass('is-loading');
				}
			}).send();
		}
	};

	!window.Filter && (window.Filter = Filter);
})(window);