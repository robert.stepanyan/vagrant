var markersArray = new Array();
var LoadMap = function (map) {
	
	var url = '/escorts/club-directory-map?ajax=1';
	
	/*var overlay = new Cubix.Overlay($('map_canvas'), {
		loader: _st('loader-small.gif'),
		position: '50%'
	});*/
	
	//overlay.disable();
	
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {
			//clear markers	
			if (markersArray.length) 
			{
				markersArray.each(function(row) {
					row.setMap(null);
				});
				
				markersArray = new Array();
			}
			
			var obj = JSON.decode(resp);
			var c = obj.c;
			obj = obj.data;
			
			/*if ( obj.length == 0 && c == 0 ) {
				var cities_input_cont = $('city-inputs');
				var areas_input_cont = $('area-inputs');
				
				cities_input_cont.set('html', '');
				areas_input_cont.set('html', '');

				$$('input[name=all_cities]')[0].set('value', 1);

				Cubix.LocationHash.Set(Cubix.LocationHash.Make());
			}*/
			
			var image_escort = new google.maps.MarkerImage('/img/cd/wg.png',
				// This marker is 20 pixels wide by 32 pixels tall.
				new google.maps.Size(28, 47),
				// The origin for this image is 0,0.
				new google.maps.Point(0,0),
				// The anchor for this image is the base of the flagpole at 0,32.
				new google.maps.Point(0, 47)
			);
				
			var image_1 = new google.maps.MarkerImage('/img/cd/1.png',
				// This marker is 20 pixels wide by 32 pixels tall.
				new google.maps.Size(28, 47),
				// The origin for this image is 0,0.
				new google.maps.Point(0,0),
				// The anchor for this image is the base of the flagpole at 0,32.
				new google.maps.Point(0, 47)
			);
				
			var image_2 = new google.maps.MarkerImage('/img/cd/2.png',
				// This marker is 20 pixels wide by 32 pixels tall.
				new google.maps.Size(28, 47),
				// The origin for this image is 0,0.
				new google.maps.Point(0,0),
				// The anchor for this image is the base of the flagpole at 0,32.
				new google.maps.Point(0, 47)
			);
				
			var image_3 = new google.maps.MarkerImage('/img/cd/3.png',
				// This marker is 20 pixels wide by 32 pixels tall.
				new google.maps.Size(28, 47),
				// The origin for this image is 0,0.
				new google.maps.Point(0,0),
				// The anchor for this image is the base of the flagpole at 0,32.
				new google.maps.Point(0, 47)
			);
				
			var image_4 = new google.maps.MarkerImage('/img/cd/4.png',
				// This marker is 20 pixels wide by 32 pixels tall.
				new google.maps.Size(28, 47),
				// The origin for this image is 0,0.
				new google.maps.Point(0,0),
				// The anchor for this image is the base of the flagpole at 0,32.
				new google.maps.Point(0, 47)
			);

			obj.each(function(e) {
				var lat = e.latitude;
				var lon = e.longitude;
						
				var img = '';
				
				if (lat && lon) 
				{
					if (e.escort_id)
					{
						var name = "<div class='fleft' style='border: 1px solid #ccc;padding:1px;width:68px;'><a href='" + e.url + "'><img src='" + e.photo + "' width='68' height='91' /></a></div>";
							
						name += "<div class='fleft' style='margin-left: 7px;'><p style='margin: 0 0 3px 0;'><a style='font-size:14px;' href='" + e.url + "'>" + e.showname + "</a></p>";
						
						if (e.address.length)
							name += "<p>" + e.address + ((e.zip.length) ? ', ' + e.zip + ' ' : ', ') + ((e.city_title.length) ? e.city_title : '') + "</p></div>";

						img = image_escort;
					}
					else
					{
						var name = "<p style='margin: 0 0 3px 0;'><a style='font-size:14px;' href='" + e.url + "'>" + e.club_name + "</a></p>";

						if (e.address.length)
							name += "<p>" + e.address + ((e.zip.length) ? ', ' + e.zip + ' ' : ', ') + ((e.city_title.length) ? e.city_title : '') + "</p>";
						
						
						
						switch (e.filter_criteria) {
							case 1:
								img = image_1;
								break;
							case 2:
								img = image_2;
								break;
							case 3:
								img = image_3;
								break;
							case 4:
								img = image_4;
								break;
						}
					}
					//console.log(img);
					var marker = new google.maps.Marker({
						position: new google.maps.LatLng(lat, lon),
						map: map,
						icon: img
					});
					
					marker.setMap(map);
					markersArray.push(marker);
					
					var infowindow = new google.maps.InfoWindow({
						content: name
					});
					
					google.maps.event.addListener(marker, 'click', function() {
						infowindow.open(map, marker);
					});
				}
			});
			
			//overlay.enable();
		}
	}).send();
	
	return false;
}

var InitMap = function(position) {
	
	var lat = 46.833333;
	var lon = 8.333333;
	if ( position ) {
		lat = position.coords.latitude;
		lon = position.coords.longitude;
	}
	
	 var point = new google.maps.LatLng(lat, lon);
	
	var mapOptions = {
		center: point,
		zoom: 10,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	var map = new google.maps.Map($('map_canvas'), mapOptions);

	new google.maps.Marker({
		position: point,
		map: map,
		title: 'Your Location'
	});

	LoadMap(map);	
};

window.addEvent('domready', function () {
	navigator.geolocation.getCurrentPosition(InitMap, InitMap);
});