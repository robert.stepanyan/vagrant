;(function (window, document) {

    window.addEvent('domready', init);

    var Module = {
        constants: {
            maxAttachments: 3,
        },
        eventHandlers: {
            onPurchaseCancel: function () {
                $$('.attachment-item').destroy();
                $$('.position-item.active').removeClass('active');
                $$('#order-banner-form')[0].reset();

                CKEDITOR.instances.editor.setData('');
            },
            onPoisitionItemClick: function () {
                $$('.position-item.active').removeClass('active');
                $(this).addClass('active');
            },
            onPaymentOptionItemClick: function () {
                $(this).getElements('input')[0].set('checked', true);
            },
            onAttachmentAdd: function () {
                $$('#attachments')[0].click();
            },
            onAttachmentAdded: function () {
                var $input = this;
                var currentImagesCount = $$('.attachment-item').length || 0;
                var selectedImagesCount = $input.files.length || 0;

                if (currentImagesCount + selectedImagesCount > Module.constants.maxAttachments) {
                    return alert('Maximum 3 files are allowed');
                }

                for (var file of $input.files) {
                    fileToBase64(file).then(function (contents) {
                        var elm = Module.templates.buildAttachmentItem({
                            contents: contents
                        });

                        $$('.attachment-items-list')[0].adopt([elm]);
                    });
                }
            },
            onAttachmentRemove: function () {
                $(this).getParent().destroy();
            },
            onBannersPurchaseSubmit: function () {
                var data = $('order-banner-form').toQueryString().parseQueryString();

                data.description = CKEDITOR.instances.editor.getData();
                data.banner_id = $$('.position-item.active').get('data-id')[0];

                data.images = [];
                $$('.attachment-item img').each(function ($item) {
                    data.images.push($item.get('src'))
                })

                submitPaymentForm(data);

            }
        },
        templates: {
            buildAttachmentItem: function (file) {

                var attachmentItem = new Element('div.attachment-item');
                var removeBtn = new Element('button', {'type': 'button', 'class': 'remove', 'text': 'X'});
                var img = new Element('img', {'src': file.contents});

                removeBtn.addEvent('click', Module.eventHandlers.onAttachmentRemove);

                attachmentItem.adopt([img, removeBtn]);

                return attachmentItem;
            },
        },
    };

    function fileToBase64(file) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
        });
    }

    function setInputFilter(textbox, inputFilter) {
        ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function (event) {
            textbox.addEventListener(event, function () {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
            });
        });
    };

    function submitPaymentForm(data) {

        var $body = $$('body');
        $body.addClass('processing');

        $$('input, select').removeClass('invalid');

        new Request({
            url: '/banners/checkout',
            data: data,
            method: 'post',
            onSuccess: function (response) {
                response = JSON.decode(response, true);

                $body.removeClass('processing');
                $$('.helper_txt').hide();
                $$('.helper_txt').removeClass('visible');

                switch (response.status) {
                    case 'twispay_success':
                        var formHtml = response.form;
                        $body[0].set('html', $body[0].get('html') + formHtml.replace(/ \"/g, "\""));
                        $('twispay-payment-form').submit();
                        break;

                    case 'paysafe_success':
                        window.location.href = response.url;
                        break;
                    case 'faxin_success':
                        window.location.href = response.url;
                        break;
                    case 'postfinance_success':
                        var formHtml = response.form;
                        $body[0].set('html', $body[0].get('html') + formHtml.replace(/ \"/g, "\""));
                        $('postfinance_form').submit();
                        break;

                    case 'error':

                        var invalidElement = $$('.invalid');

                        for (var key in response.msgs) {
                            var $input = $$('#' + key);
                            var $helper = $$('#' + key + '_helper');

                            if ($input) {
                                $input.addClass('invalid');
                            }

                            if ($helper) {
                                $helper.set('html', response.msgs[key]);
                                $helper.addClass('visible');
                                $helper.show();
                            } else {
                                return alert(response.msgs[key])
                            }
                        }

                        var firstInvalidElement = null;
                        if ($$('.invalid') && $$('.invalid')[0]) firstInvalidElement = $$('.invalid')[0];
                        if ($$('.helper_txt.visible') && $$('.helper_txt.visible')[0]) firstInvalidElement = $$('.helper_txt.visible')[0];

                        if (firstInvalidElement) {
                            var yOffset = -80;
                            var element = firstInvalidElement;
                            var y = element.getBoundingClientRect().top + window.pageYOffset + yOffset;

                            window.scrollTo({top: y, behavior: 'smooth'});
                        }
                        break;
                }

            }
        }).send();
    }

    function bindDomEvents() {

        // Inputs for numbers only
        $$("[data-validate='true'][data-accept='number']").each(function ($element) {
            setInputFilter($element, function (value) {
                return /^\d*\.?\d*$/.test(value); // Allow digits and '.' only, using a RegExp
            });
        });

        // Purchase page > submit button
        $$('#submit-form').addEvent('click', Module.eventHandlers.onBannersPurchaseSubmit);

        // Purchase page > position block (homepage banner / Regionpage banner)
        $$('.position-item').addEvent('click', Module.eventHandlers.onPoisitionItemClick);

        // Purchase page > payment option blocks
        $$('.payment-method').addEvent('click', Module.eventHandlers.onPaymentOptionItemClick);

        // Purchase page > button to add new attachments
        $$('#add-file').addEvent('click', Module.eventHandlers.onAttachmentAdd);

        // Purchase page > Attachments input (when new attachment is added)
        $$('#attachments').addEvent('change', Module.eventHandlers.onAttachmentAdded);

        // Purchase page > cancel order
        $$('#reset-form').addEvent('click', Module.eventHandlers.onPurchaseCancel);
    }

    function makeEditorConfig() {
        var config = {};
        config.toolbarGroups = [
            {name: 'document', groups: ['mode', 'document', 'doctools']},
            {name: 'clipboard', groups: ['clipboard', 'undo']},
            {name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing']},
            {name: 'forms', groups: ['forms']},
            {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
            {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph']},
            {name: 'links', groups: ['links']},
            {name: 'insert', groups: ['insert']},
            {name: 'styles', groups: ['styles']},
            {name: 'colors', groups: ['colors']},
            {name: 'tools', groups: ['tools']},
            {name: 'others', groups: ['others']},
            {name: 'about', groups: ['about']}
        ];

        config.removeButtons = 'Source,Save,NewPage,Print,Preview,Templates,Cut,Copy,PasteText,Paste,PasteFromWord,Find,Replace,SelectAll,HiddenField,ImageButton,Button,Select,Textarea,Radio,Checkbox,Form,Undo,Redo,TextField,Outdent,Indent,CreateDiv,BidiLtr,BidiRtl,Language,Link,Flash,Unlink,Anchor,Table,Smiley,SpecialChar,PageBreak,Iframe,HorizontalRule,Subscript,Superscript,Strike,RemoveFormat,Maximize,ShowBlocks,About,Format,Styles,Font';

        return config;
    }

    function init() {
        CKEDITOR.replace('editor', makeEditorConfig());
        bindDomEvents();
    }

})(this, document);