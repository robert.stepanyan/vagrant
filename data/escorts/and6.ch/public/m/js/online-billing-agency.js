(function() {
	var escortSelectionPage = $$('.agency-online-billing.escort-selection')[0];
	
	if (escortSelectionPage) {
		var mOnlineBillingAgency = {
			container: null,
			actionBtn: null,
			defaultPerPage: 6,

			init: function() {
				this.setElsInterface();
				this.bindEventEscort();
				this.toggleBtn(this.actionBtn, 0);
			},

			setElsInterface: function() {
				this.actionBtn = escortSelectionPage.getElement('.next-step-button');
				this.container = escortSelectionPage.getElement('.escorts-container');
			},

			bindEventEscort: function() {
				var self = this;

				this.container.getElements('.escort-item').each(function(el){
					if (el.getElement('.escort-incart-overlay')) {
						return true;
					}
					el.addEvent('click', function() {
						self.deselectEscort();
						self.createOverlay(this);
						self.toggleBtn(self.actionBtn, 1, function() {
							var escortId = self.getSelectedEscortId();
							self.nextStepAction(escortId);
						});
					});
				});

				$$('.o-cont select').addEvent('change', function(e) {
					var sorted = self.sortEscorts(this.get('value'));
					self.container.set('html', '');
					self.container.adopt(sorted);
				});

				$('shownameSearch').addEvent('keyup', function() {
					if (this.get('value').length > 1) {
						self.hideAllEscorts();
						var _escorts = self.getEscortsByShowname(this.get('value'));
						_escorts.each(function(el) {
							el.removeClass('hidden');
						});

					} else {
						self.showAllEscorts();
					}
					
					self.pipePaging(self.getVisibleEscorts(), 1);
					self.updatePaging(self.getVisibleEscorts(), 1);
					self.updateFound();
				});

				if($$('.to-first')[0]){
					$$('.to-first')[0].addEvent('click', function() {
						if (this.hasClass('inactive')) return;

						self.pipePaging(self.getVisibleEscorts(), 1);
						self.updatePaging(self.getVisibleEscorts(), 1);
					});
				}

				if($$('.to-last')[0]){
					$$('.to-last')[0].addEvent('click', function() {
						if (this.hasClass('inactive')) return;

						self.pipePaging(
							self.getVisibleEscorts(), 
							Math.round(self.getVisibleEscorts().length/self.defaultPerPage)
						);

						self.updatePaging(
							self.getVisibleEscorts(),
							Math.round(self.getVisibleEscorts().length/self.defaultPerPage)
						);
					});
				}
				
				if($$('.next')[0]){
					$$('.next')[0].addEvent('click', function() {
						if (this.hasClass('inactive')) return;

						var _page = this.get('data-page');

						self.pipePaging(self.getVisibleEscorts(), +_page);
						self.updatePaging(self.getVisibleEscorts(), +_page);
					});
				}

				if($$('.previous')[0]){
					$$('.previous')[0].addEvent('click', function() {
						if (this.hasClass('inactive')) return;

						var _page = this.get('data-page');

						self.pipePaging(self.getVisibleEscorts(), +_page);
						self.updatePaging(self.getVisibleEscorts(), +_page);
					});
				}
			},

			pipePaging: function(list, page) {
				var self = this;

				list.each(function(el, i) {
					if (i < self.defaultPerPage * (page - 1)
						|| i > self.defaultPerPage * (page - 1) + self.defaultPerPage - 1) {
						el.addClass('page-hidden');
					} else {
						el.removeClass('page-hidden');
					}
				});
			},

			updatePaging: function(list, page) {
				var toLastBtn = $$('.to-last')[0];
				var nextBtn = $$('.next')[0];
				var toFirstBtn = $$('.to-first')[0];
				var prviousBtn = $$('.previous')[0];
				var pageContainer = $$('.page-container')[0];

				if (!list[list.length - 1].hasClass('page-hidden')) {
					
					toLastBtn.addClass('inactive');
					nextBtn.addClass('inactive');

					nextBtn.set('data-page', +page + 1);
					prviousBtn.set('data-page', +page);
				} else {

					
					toLastBtn.removeClass('inactive');
					nextBtn.removeClass('inactive');
				}

				if (!list[0].hasClass('page-hidden')) {
					toFirstBtn.addClass('inactive');
					prviousBtn.addClass('inactive');
				} else {

					toFirstBtn.removeClass('inactive');
					prviousBtn.removeClass('inactive');

					prviousBtn.set('data-page', +page - 1);
					nextBtn.set('data-page', +page + 1);
				}

				pageContainer.set('html', +page);
			},

			sortEscorts: function(type) {
				if (type == 'alphabetically') {
					return this.container.getElements('.escort-item').sort(function(a, b){
						return a.getElement('.showname').get('html').localeCompare(b.getElement('.showname').get('html'));					
					});
				}

				if (type == 'id') {
					return this.container.getElements('.escort-item').sort(function(a, b){
						return +a.get('data-id') > +b.get('data-id');
					});
				}

				if (type == 'modified') {
					return this.container.getElements('.escort-item').sort(function(a, b){
						return new Date(a.get('data-modified')) < new Date(b.get('data-modified'));
					});
				}

				if (type == 'newest') {
					return this.container.getElements('.escort-item').sort(function(a, b){
						return new Date(a.get('data-registered')) < new Date(b.get('data-registered'));
					});
				}
			},

			hideAllEscorts: function() {
				this.container.getElements('.escort-item').addClass('hidden');
			},

			updateFound: function() {
				$$('.escorts-count')[0].set('html', this.getShownEscortsCount());
			},

			getShownEscortsCount: function() {
				return this.container.getElements('.escort-item').filter(function(el) {
					return !el.hasClass('hidden');
				}).length;
			},

			showAllEscorts: function() {
				this.container.getElements('.escort-item').removeClass('hidden');
			},

			getVisibleEscorts: function() {
				return this.container.getElements('.escort-item').filter(function(el) {
					return !el.hasClass('hidden');
				});
			},
			

			getEscortsById: function(str) {
				return this.getAllEscorts().filter(function(escort) {
					return ~escort.getElement('.escort-id').get('html').indexOf(str);
				});
			},

			getEscortsByShowname: function(str) {
				return this.getAllEscorts().filter(function(escort) {
					return ~escort.getElement('.showname').get('html').toLowerCase().indexOf(str.toLowerCase());
				});
			},

			getAllEscorts: function() {
				return this.container.getElements('.escort-item');
			},

			getSelectedEscortId: function() {
				return this.container.getElement('.escort-overlay')
					.getParent('.escort-item').get('data-id');
			},

			deselectEscort: function(items) {
				if (!items || !items.length) {
					items = this.container.getElements('.escort-item');
				}

				items.each(function(el) {
					var elToDelete = el.getElement('.escort-overlay');

					if (elToDelete) {
						elToDelete.dispose();
					}
				})
			},

			createOverlay: function(el) {
				el.setStyle('position', 'relative');

				var _overlay = new Element('div', {
					class: 'escort-overlay',
					styles: {
					    position: 'absolute',
					    backgroundColor: 'rgba(215, 37, 123, 0.37)',
					    width: 'calc(100% - 6%)',
					    height: 'calc(100% - 10px)',
					    top: '0',
					    left: '0',
					    borderRadius: '5px',
					    margin: '0 3%',
					    border: '2px solid rgb(172, 39, 103)',
					    boxSizing: 'border-box',
					    backgroundImage: 'url("/m/i/online-billing/check-symbol.png")',
					    backgroundPosition: '50%',
					    backgroundRepeat: 'no-repeat'
					}
				});

				_overlay.addEvent('click', function() {
					return false;
				});

				_overlay.inject(el);
			},

			toggleBtn: function(btn, status, action) {
				if (status) {
					btn.addClass('active');
					btn.addEvent('click', action);
				} else {
					btn.removeClass('active');
					btn.removeEvent('click');
				}
			},

			nextStepAction: function(escortId) {
				var isGotd = $('isGotd').get('value');
				var isVotd = $('isVotd').get('value');
				var params = '';
				if(+isGotd === 1){
					params = '?gotd=1'
				}
				if(+isVotd === 1){
					params = '?votd=1'
				}
				var form = new Element('form', {
					action: '/online-billing/agency-package'+params, 
					method: 'post',
					html: '<input name="escort_id" value="' + escortId + '"/>'
				});

				form.setStyle('display', 'none');
				form.inject(this.container);
				form.submit();
			}
		};

		mOnlineBillingAgency.init();
	}

	var packageSelectionPage = $$('.agency-online-billing.package-selection')[0];

	if (packageSelectionPage) {
		var hasApprovedVideo = $('hasApprovedVideo').get('value');
		var mOnlineBilling = {

			// Online Billing Container
			container: packageSelectionPage,

			// Online Billing Popups 
			locationPopup: null,
			startTimePopup: null,
			gotdPopup: null,
			gotdCalendarPopup: null,
			gotdSelfSelectedDates: [],
			availableGotdDates: [],

			// SedCard Package Ids
			sedcardPackages: JSON.decode($('sedcardPackages').get('value')),
			dayPassesPackages: JSON.decode($('dayPassesPackages').get('value')),
			additionalAreaPrice: parseInt($('additionalAreaPrice').get('value')),
			gotdPrice: parseInt($('gotdPrice').get('value')),
			votdPrice: parseInt($('votdPrice').get('value')),

			// Methods
			init: function(){
				this.loading();
				this.selectPackage();
				this.getLocationPopup();
				this.getStartTimePopup();
				this.getGotdPopup();
				this.getGotdCalendarPopup();

				this.getVotdPopup();
				this.getVotdCalendarPopup();

				this.switchLocationBtn(0);
				this.switchNextStepBtn(0);
				this.eventManager();
			},
			loading: function(){
				$$('#page').addClass('loading');
				var counter = 0;
				var self = this;
				window.addEvent('scloaded', function(data) {
					if (data && data.hasOwnProperty('canCalculate') && data.canCalculate){
						var availDates = self.calcAvailableGotdDates();
						if (availDates.length) {
							self.switchGotdBtn(1);
							self.availableGotdDates = availDates;
						} else {
							self.switchGotdBtn(0);
						}

						var availVotdDates = self.calcAvailableVotdDates();
						if (availVotdDates.length && Number(hasApprovedVideo) === 1) {
							self.switchVotdBtn(1);
							self.availableVotdDates = availVotdDates;
						} else {
							self.switchVotdBtn(0);
						}
					}
					counter++;
					if (counter === 6) {
						$$('#page').removeClass('loading');
					}
				});
			},

			selectPackage: function() {
				var self = this,
				packageOptions = this.container.getElements('.packages-container a');

				packageOptions.addEvent('click', function(e){
					e.stop();
					if($('startTimeBtn') && !$('startTimeBtn').hasClass('active')){
                        $('startTimeBtn').addClass('active');
						
					}
					if ($$('.summary-location-item-container').length > 0 ||
						$$('.summary-gotd-item-container').length > 0 || $$('.summary-votd-item-container').length > 0){

						var msg = 'Are you sure you want to change the package? ' +
							'All selected locations will be removed.';
							
						if (confirm(msg)) {
							$$('.summary-location-item-container')
								.getElements('.summary-item-remove-btn').each(function(el){
									el.fireEvent('click');
								});
							$$('.summary-gotd-item-container')
								.getElements('.summary-item-remove-btn').each(function(el){
									el.fireEvent('click');
								});
							$$('.summary-votd-item-container')
								.getElements('.summary-item-remove-btn').each(function(el){
									el.fireEvent('click');
								});

							packageOptions.removeClass('selected');
							this.addClass('selected');

							var packageId = this.get('data-id');

							if (self.hasLocation(packageId)){
								self.switchLocationBtn(1);
							} else {
								self.switchLocationBtn(0);
							}
							
							self.renderInfo();
						} else {
							return;
						}
					} else {
						packageOptions.removeClass('selected');
						this.addClass('selected');

						var packageId = this.get('data-id');

						if (self.hasLocation(packageId)){
							self.switchLocationBtn(1);
						} else {
							self.switchLocationBtn(0);
						}

						self.renderInfo();
					}

					var availDates = self.calcAvailableGotdDates();
					if (availDates.length) {
						self.switchGotdBtn(1);
						self.availableGotdDates = availDates;
					} else {
						self.switchGotdBtn(0);
					}

					var availVotdDates = self.calcAvailableVotdDates();
					if (availVotdDates.length && hasApprovedVideo == 1) {
						self.switchVotdBtn(1);
						self.availableVotdDates = availVotdDates;
					} else {
						self.switchVotdBtn(0);
					}
				});
			},

			deselectPackages: function() {
				var packageOptions = this.container.getElements('.packages-container a');

				packageOptions.each(function(el) {
					el.removeClass('selected');
				});

				var availDates = this.calcAvailableGotdDates();
				if (availDates.length) {
					this.switchGotdBtn(1);
					this.availableGotdDates = availDates;
				} else {
					this.switchGotdBtn(0);
				}

				var availVotdDates = this.calcAvailableVotdDates();
				if (availVotdDates.length && hasApprovedVideo == 1) {
					this.switchVotdBtn(1);
					this.availableVotdDates = availVotdDates;
				} else {
					this.switchVotdBtn(0);
				}

				this.switchLocationBtn(0);
				this.container.getElement('.selected-items').fireEvent('cartchanged');
			},

			hasLocation: function(id) {
				for (var i = this.sedcardPackages.length - 1; i >= 0; i--) {
					if (this.sedcardPackages[i] == id) {
						return true;
						break;
					}
				}

				return false;
			},

			calcAvailableGotdDates: function() {
				var self = this,
					avGotdDates = $('availableGotdDates').get('value').split(',');
					var filtered = avGotdDates.filter(function (el) {
					  return el != '';
					});
					var availableGotdDates = filtered.length ? filtered : [],
					selectedPackageOption = this.container
						.getElements('.packages-container a.selected');
					var activePackageExp = $('activePackageExpiration').get('value');
					
				if (selectedPackageOption.length > 0) {
					var _duration = +selectedPackageOption.get('data-duration'),
						_startDate = self.startTimePopup
							.getElement('a.selected .activation-date').get('value') || ( activePackageExp ? (new Date( activePackageExp * 1000).clearTime()) : (new Date().clearTime()) );

					
					for (var i = 0; i < _duration ; i++) {
						availableGotdDates.include(
							Date.parse(_startDate).clone().increment('day', i).format('%Y-%m-%d')
						);
					}
				}

				return availableGotdDates;
			},

			calcAvailableVotdDates: function() {
				var self = this,
					avVotdDates = $('availableVotdDates').get('value').split(',');
					var filtered = avVotdDates.filter(function (el) {
					  return el != '';
					});
					var availableVotdDates = filtered.length ? filtered : [],
					selectedPackageOption = this.container
						.getElements('.packages-container a.selected');
					var activePackageExp = $('activePackageExpiration').get('value')

				if (selectedPackageOption.length > 0) {
					var _duration = +selectedPackageOption.get('data-duration'),
						_startDate = self.startTimePopup
							.getElement('a.selected .activation-date').get('value') || ( activePackageExp ? (new Date( activePackageExp * 1000).clearTime()) : (new Date().clearTime()) );

					
					for (var i = 0; i < _duration ; i++) {
						availableVotdDates.include(
							Date.parse(_startDate).clone().increment('day', i).format('%Y-%m-%d')
						);
					}
				}

				return availableVotdDates;
			},

			switchLocationBtn: function(on) {
				var self = this,
					locationBtn = $('locationBtn');

				if (locationBtn) {
					if (on) {
						locationBtn.addClass('active');
						locationBtn.removeEvents().addEvent('click', function(e) {
							e.stop();
							self.showPopup(self.locationPopup);
						});
					} else {
						locationBtn.removeClass('active');
						locationBtn.removeEvents().addEvent('click', function(e) {
							e.stop();
						})
					}
				}
			},

			switchGotdBtn: function(on) {
				var self = this,
					gotdBtn = $('gotdBtn');

				if (on) {
					gotdBtn.addClass('active');
					gotdBtn.removeEvents().addEvent('click', function(e) {
						e.stop();
						self.showPopup(self.gotdPopup);
					});
				} else {
					gotdBtn.removeClass('active');
					gotdBtn.removeEvents().addEvent('click', function(e) {
						e.stop();
					})
				}
			},

			switchVotdBtn: function(on) {
				var self = this,
					votdBtn = $('votdBtn');

				if (on) {
					votdBtn.addClass('active');
					votdBtn.removeEvents().addEvent('click', function(e) {
						e.stop();
						self.showPopup(self.votdPopup);
					});
				} else {
					votdBtn.removeClass('active');
					votdBtn.removeEvents().addEvent('click', function(e) {
						e.stop();
					})
				}
				
			},

			switchNextStepBtn: function(on) {
				var self = this,
					nextPageBtn = this.container.getElement('.next-step-button');

				if (on) {
					nextPageBtn.addClass('active');
					nextPageBtn.removeEvents().addEvent('click', function(e) {
						e.stop();
						self.goToPaymentPage();
					});
				} else {
					nextPageBtn.removeClass('active');
					nextPageBtn.removeEvents().addEvent('click', function(e) {
						e.stop();
					});
				}
			},

			switchGotdCalendarBtn: function(on) {
				var self = this,
					calendarBtn = $('gotdcalendarBtn');

				if (on) {
					calendarBtn.addClass('active');
					calendarBtn.removeEvents().addEvent('click', function(e) {
						e.stop();
						self.showPopup(self.gotdCalendarPopup);
						self.getGotdCalendarPopup();
					});
				} else {
					calendarBtn.removeClass('active');
					calendarBtn.removeEvents().addEvent('click', function(e) {
						e.stop();
					})
				}
			},

			switchVotdCalendarBtn: function(on) {
				var self = this,
					calendarBtn = $('votdcalendarBtn');

				if (on) {
					calendarBtn.addClass('active');
					calendarBtn.removeEvents().addEvent('click', function(e) {
						e.stop();
						self.showPopup(self.votdCalendarPopup);
						self.getVotdCalendarPopup();
					});
				} else {
					calendarBtn.removeClass('active');
					calendarBtn.removeEvents().addEvent('click', function(e) {
						e.stop();
					})
				}
			},

			switchGotdLocationBtn: function(on) {
				var self = this,
					addGotdLocation = $('addGotdLocation');

				if (on) {
					addGotdLocation.addClass('active');
					addGotdLocation.removeEvents();
					self.addGotdLocation(self.gotdPopup);
				} else {
					addGotdLocation.removeClass('active');
					addGotdLocation.removeEvents().addEvent('click', function(e) {
						e.stop();
						self.showNotification('<span>Error</span> Select Area & Dates', 'error');
					})
				}
			},

			switchVotdLocationBtn: function(on) {
				var self = this,
					addVotdLocation = $('addVotdLocation');

				if (on) {
					addVotdLocation.addClass('active');
					addVotdLocation.removeEvents();
					self.addVotdLocation(self.votdPopup);
				} else {
					addVotdLocation.removeClass('active');
					addVotdLocation.removeEvents().addEvent('click', function(e) {
						e.stop();
						self.showNotification('<span>Error</span> Select Area & Dates', 'error');
					})
				}
			},

			eventManager: function() {
				var self = this;
				
				if ($('startTimeBtn')){
                    $('startTimeBtn').addEvent('click', function (e) {
						e.stop();
						if(this.hasClass('active')){
							self.showPopup(self.startTimePopup);
						}
					});
				}

				$('summaryContainer').addEvent('click', function(e) {
					e.stop();
					self.toggleSummary();
				});

				this.container.getElement('.selected-items').addEvent('cartchanged', function() {
					if (this.getElements('div').length > 0 ) {
						self.switchNextStepBtn(1);
					} else {
						self.switchNextStepBtn(0);
					}
				});
			},

			createPopup: function(options) {
				var self = this;
				options = options ? options : {};

				var popupHeight = parseInt(options.height) || 500,
						popupWidth = parseInt(options.width) || 320,
						popupContent = options.content || null,
						popupInnerClass = options.popupInnerClass || null,
						popupActionText = options.actionText || 'OK',
						popupActionFunction = options.actionFunction || function (){
							return false;
						}

				var _outer = new Element('div', {
						styles: {
							display: 'table',
							position: 'absolute',
							height: '100%',
							width: '100%',
						}
					}),
					_middle = new Element('div', {
						styles: {
							display: 'table-cell',
							verticalAlign: 'middle'
						}
					}),

					_inner = new Element('div', {
						class: 'inner '+popupInnerClass,
						styles: {
							position: 'relative',
							marginLeft: 'auto',
							marginRight: 'auto',
							width: popupWidth + 'px',
							height: popupHeight + 'px',
							borderRadius: '3px',
							backgroundColor: '#ffffff'

						},

						html: popupContent
					});

					_innerContent = new Element('div', {
						class: 'inner-content',
						styles: {
							marginBottom: '60px',
							boxSizing: 'border-box',
						}
					});

				_innerContent.inject(_inner);
				_inner.inject(_middle);
				_middle.inject(_outer);

				var _actionBtn = new Element('a', {
					styles: {
						display: 'block',
						position: 'absolute',
						width: '100%',
						height: '60px',
						backgroundColor: '#1a5cbf',
						bottom: 0,
						left: 0,
						color: '#ffffff',
						textTransform: 'uppercase',
						textDecoration: 'none',
		    			fontSize: '20px',
						fontWeight: 'bold',
						textAlign: 'center',
						lineHeight: '60px',
						borderRadius: '0 0 3px 3px'
					},

					html: popupActionText,
					href: '#'
				});

				var _closeBtn = new Element('a', {
					class: 'close-btn',
					styles: {
						display: 'block',
						width: '20px',
						height: '20px',
						position: 'absolute',
						top: 0,
						right: 0,
						lineHeight: '20px',
						textAlign: 'center',
						textDecoration: 'none',
						fontSize: '20px',
						fontWeight: 'bold'
					},

					html: '&times;'
				});

				_closeBtn.addEvent('click', function() {
					self.hidePopup(this.getParents('.mobile-ob-overlay')[0])
				});
				_actionBtn.addEvent('click', popupActionFunction);

				_closeBtn.inject(_inner);
				_actionBtn.inject(_inner);

				return _outer;
			},

			createOverlay: function() {
				var _overlay = new Element('div', {
					class: 'mobile-ob-overlay',
					styles: {
						position: 'fixed',
						height: '100%',
						width: '100%',
						top: '0',
						left: '0',
						background: 'rgba(0,0,0,0.5)',
						zIndex : 1001,
						visibility: 'hidden',
						opacity: '0'
					}
				});

				// _overlay.addEvent('mousewheel', function(e){
				// 		e.stop();
				// 	}
				// );

				return _overlay;
			},

			getLocationPopup: function() {

				var self = this,
					overlay = this.createOverlay(),
					popup = this.createPopup({
						actionFunction: function(e){
							e.stop();
							self.hidePopup(self.locationPopup);
							self.renderInfo();
						}
					});

				popup.inject(overlay);
				overlay.inject(this.container);

				var _request = new Request({
					url: '/online-billing/escort-additional-areas',
					method: 'get',

					onSuccess: function(responseText) {
						window.fireEvent('scloaded');
						popup.getElement('.inner-content').set('html', responseText);
						self.addAdditionalLocation(popup);

					}
				}).send();

				this.locationPopup = overlay;
			},

			addAdditionalLocation: function(popup) {
				var self = this;

				popup.getElement('#addLocation').addEvent('click', function(e) {
					e.stop();

					var selectedArea = popup.getElement('.additional-areas').getSelected(),
						areaId = selectedArea.get('value'),
						areaTitle = selectedArea.get('html'),
						selectedAreasWrapper = popup.getElement('#selectedAreas');
						closeBtn = popup.getElement();

					if (!areaId.length) return;

					selectedArea.dispose();

					_areaContainer = new Element('div', {
						class: 'area-container',
						styles: {
							width: '100%',
							height: '20px',
							float: 'left',
							marginTop: '10px'
						}
					});

					_areaTitle = new Element('span', {
						styles: {
							display: 'block',
							width: 'calc(100% - 20px)',
							height: '20px',
							overflow: 'hidden',
							textOverflow: 'ellipsis',
							whiteSpace: 'nowrap',
							float: 'left'
						},

						html: areaTitle
					});

					_removeBtn = new Element('a', {
						'data-id': areaId,
						class: 'additional-area-remove-btn',

						styles: {
							display: 'block',
							width: '20px',
							height: '20px',
							textAlign: 'center',
							lineHeight: '20px',
							fontSize: '20px',
							color: '#ffffff',
							float: 'left',
							textDecoration: 'none',
							borderRadius: '3px',
							backgroundColor: '#ac2767'
						},

						html: '&times;'
					});

					_removeBtn.addEvent('click', function(e){
						if (e) {
							e.stop();
						}

						var _id = this.get('data-id'),
							_title = this.getSiblings('span')[0].get('html'),
							_option = new Element('option', {
								value: _id,
								html: _title
							});

						this.getParent('.area-container').dispose();
						_option.inject(popup.getElement('select.additional-areas'));
					});

					_areaTitle.inject(_areaContainer);
					_removeBtn.inject(_areaContainer);

					_areaContainer.inject(selectedAreasWrapper);
				});

				popup.getElement('.close-btn').addEvent('click', function(e){
					popup.getElement('#selectedAreas')
						.getElements('.area-container a').each(function(el){
							el.fireEvent('click');
						});
					
					self.renderInfo();
				});
			},

			getStartTimePopup: function() {
				var self = this,
					overlay = this.createOverlay(),
					popup = this.createPopup({
						height: 300,
						actionFunction: function(e) {
							var _activationTypeSelected = self.startTimePopup.getElement('a.selected');
							if (_activationTypeSelected.getElement('.activation-date').get('value') !== '') {
								var _date = _activationTypeSelected.getElement('.activation-date').get('value'),
									_time = _activationTypeSelected.getElement('.activation-time').get('value'),

									_parsedDate = Date.parse(_date + ' ' + _time),
									hasActivePackage = $('hasActivePackage').get('value'),
									activePackageExpiration = $('activePackageExpiration').get('value');

								if (hasActivePackage) {
									var _diff = _parsedDate.diff(Date.parse(activePackageExpiration, 'second'));

									if (_diff > 0) {
										self.showNotification("omOBLang.mOBDateGreaterErr", 'error');
										return false;
									}
								} else {
									
									var _diff = _parsedDate.diff(new Date(), 'second');

									if (_diff > 0) {
										self.showNotification("invalid date interval selected", 'error');
										return false;
									}
								}
							}

							e.stop();
							self.hidePopup(self.startTimePopup);
							self.renderInfo();
						}
					});

				popup.inject(overlay);
				overlay.inject(this.container);

				new Request({
					url: '/online-billing/escort-activation-date' + '?escort_id=' + $('agencyEscortId').get('value'),
					method: 'get',

					onSuccess: function(responseText) {
						popup.getElement('.inner-content').set('html', responseText);
						self.selectActivationDate(popup);
						window.fireEvent('scloaded', {canCalculate:1});
					}
				}).send();

				this.startTimePopup = overlay;
			},

			selectActivationDate: function(popup) {
				var self = this,
					dateOptions = popup.getElements('.escort-activation-date a'),
					dateTimeInputs = popup.getElements('.escort-activation-date input');

				dateOptions.addEvent('click', function(e){
					if (e) e.stop();

					dateOptions.removeClass('selected');
					this.addClass('selected');
				});			

				dateTimeInputs.addEvent('click', function(e) {
					e.stopPropagation();
					dateOptions[1].fireEvent('click');
				});
			},

			getGotdPopup: function() {
				var self = this;
					var overlay = this.createOverlay(),
					popup1 = this.createPopup({
						popupInnerClass: 'getGotdPopup',
						actionFunction: function() {
							self.renderInfo();
							self.hidePopup(self.gotdPopup);
						}
					});

				popup1.inject(overlay);
				overlay.inject(this.container);

				var _request = new Request({
					url: '/online-billing/escort-gotd',
					method: 'get',

					onSuccess: function(responseText) {
						window.fireEvent('scloaded');
						popup1.getElement('.inner-content').set('html', responseText);
						popup1.getElement('.additional-areas')
							.addEvent('change', function() {
								if (+this.getSelected().get('value') !== 0){
									self.gotdSelfSelectedDates = [];
									self.switchGotdCalendarBtn(1);
									self.switchGotdLocationBtn(1);
									self.gotdCalendarPopup
										.getElement('#selectedGotdDates').set('html', '');
								} else {
									self.gotdSelfSelectedDates = [];
									self.switchGotdCalendarBtn(0);
									self.switchGotdLocationBtn(0);
									self.gotdCalendarPopup
										.getElement('#selectedGotdDates').set('html', '');
								}
							});

						self.switchGotdCalendarBtn(0);
						self.switchGotdLocationBtn(0);
					}
				}).send();

				this.gotdPopup = overlay;
			},

			getVotdPopup: function() {
				var self = this;
					overlay = this.createOverlay(),
					popup = this.createPopup({
						popupInnerClass: 'getVotdPopup',
						actionFunction: function() {
							self.renderInfo();
							self.hidePopup(self.votdPopup);
						}
					});

				popup.inject(overlay);
				overlay.inject(this.container);

				var _request = new Request({
					url: '/online-billing/escort-votd',
					method: 'get',

					onSuccess: function(responseText) {
						window.fireEvent('scloaded');
						popup.getElement('.inner-content').set('html', responseText);
						
						popup.getElement('.additional-areas')
							.addEvent('change', function() {
								if (+this.getSelected().get('value') !== 0){
									self.votdSelfSelectedDates = [];
									self.switchVotdCalendarBtn(1);
									self.switchVotdLocationBtn(1);
									self.votdCalendarPopup
										.getElement('#selectedVotdDates').set('html', '');
								} else {
									self.votdSelfSelectedDates = [];
									self.switchVotdCalendarBtn(0);
									self.switchVotdLocationBtn(0);
									self.votdCalendarPopup
										.getElement('#selectedVotdDates').set('html', '');
								}
							});
						self.switchVotdCalendarBtn(0);
						self.switchVotdLocationBtn(0);
					}
				}).send();

				this.votdPopup = overlay;
			},

			addGotdLocation: function(popup) {
				var self = this;

				popup.getElement('#addGotdLocation').addEvent('click', function(e) {
					e.stop();


					if (!self.gotdSelfSelectedDates.length) {
						self.showNotification("please select date", 'error');
						return;	
					}

					var selectedArea = popup.getElement('.additional-areas').getSelected(),
						areaId = selectedArea.get('value'),
						areaTitle = selectedArea.get('html'),
						selectedGotdAreasWrapper = popup.getElement('#selectedGotdAreas'),
						additionalAreas = popup.getElement('.additional-areas'),
						closeBtn = popup.getElement('.close-btn');

					if (!areaId.length) return;

					selectedArea.dispose();

					self.gotdSelfSelectedDates.each(function(el) {
						_areaContainer = new Element('div', {
							class: 'area-container',
							styles: {
								width: '100%',
								height: '20px',
								float: 'left',
								marginTop: '10px'
							}
						});

						_areaTitle = new Element('span', {
							styles: {
								display: 'block',
								width: 'calc(100% - 20px)',
								height: '20px',
								overflow: 'hidden',
								textOverflow: 'ellipsis',
								whiteSpace: 'nowrap',
								float: 'left'
							},

							html: '<strong>' + areaTitle + '</strong> : ' + el
						});

						_removeBtn = new Element('a', {
							'data-id': areaId,
							class: 'additional-area-remove-btn',

							styles: {
								display: 'block',
								width: '20px',
								height: '20px',
								textAlign: 'center',
								lineHeight: '20px',
								fontSize: '20px',
								color: '#ffffff',
								float: 'left',
								textDecoration: 'none',
								borderRadius: '3px',
								backgroundColor: '#ac2767'
							},

							html: '&times;'
						});

						_removeBtn.addEvent('click', function(e){
							if (e) {
								e.stop();
							}

							var _id = this.get('data-id'),
								_container = this.getParents('#selectedGotdAreas'),
								it = this,
								occur = 0;

							_container.getElements('.additional-area-remove-btn')[0].each(function(el) {
								if (el.get('data-id') == _id) occur++;
							});

							if (occur > 1) {
								this.getParent('.area-container').dispose();
							} else {
								_title = this.getSiblings('span')[0].getElement('strong').get('html');
								_option = new Element('option', {
									value: _id,
									html: _title
								});

								this.getParent('.area-container').dispose();
								_option.inject(popup.getElement('select.additional-areas'));
							}

						});

						_areaContainer.adopt(_areaTitle, _removeBtn);
						_areaContainer.inject(selectedGotdAreasWrapper);
					});
					self.gotdCalendarPopup.getElement('.close-btn').fireEvent('click');
					self.gotdSelfSelectedDates = [];
					additionalAreas.fireEvent('change');
				});

				popup.getElement('.close-btn').addEvent('click', function(e){
					popup.getElement('#selectedGotdAreas')
						.getElements('.area-container a').each(function(el){
							el.fireEvent('click');
						});
					self.renderInfo();
				});
			},

			addVotdLocation: function(popup) {
				var self = this;

				popup.getElement('#addVotdLocation').addEvent('click', function(e) {
					e.stop();


					if (!self.votdSelfSelectedDates.length) {
						self.showNotification("please select date", 'error');
						return;	
					}

					var selectedArea = popup.getElement('.additional-areas').getSelected(),
						areaId = selectedArea.get('value'),
						areaTitle = selectedArea.get('html'),
						selectedVotdAreasWrapper = popup.getElement('#selectedVotdAreas'),
						additionalAreas = popup.getElement('.additional-areas'),
						closeBtn = popup.getElement('.close-btn');

					if (!areaId.length) return;

					selectedArea.dispose();

					self.votdSelfSelectedDates.each(function(el) {
						_areaContainer = new Element('div', {
							class: 'area-container',
							styles: {
								width: '100%',
								height: '20px',
								float: 'left',
								marginTop: '10px'
							}
						});

						_areaTitle = new Element('span', {
							styles: {
								display: 'block',
								width: 'calc(100% - 20px)',
								height: '20px',
								overflow: 'hidden',
								textOverflow: 'ellipsis',
								whiteSpace: 'nowrap',
								float: 'left'
							},

							html: '<strong>' + areaTitle + '</strong> : ' + el
						});

						_removeBtn = new Element('a', {
							'data-id': areaId,
							class: 'additional-area-remove-btn',

							styles: {
								display: 'block',
								width: '20px',
								height: '20px',
								textAlign: 'center',
								lineHeight: '20px',
								fontSize: '20px',
								color: '#ffffff',
								float: 'left',
								textDecoration: 'none',
								borderRadius: '3px',
								backgroundColor: '#ac2767'
							},

							html: '&times;'
						});

						_removeBtn.addEvent('click', function(e){
							if (e) {
								e.stop();
							}

							var _id = this.get('data-id'),
								_container = this.getParents('#selectedVotdAreas'),
								it = this,
								occur = 0;

							_container.getElements('.additional-area-remove-btn')[0].each(function(el) {
								if (el.get('data-id') == _id) occur++;
							});

							if (occur > 1) {
								this.getParent('.area-container').dispose();
							} else {
								_title = this.getSiblings('span')[0].getElement('strong').get('html');
								_option = new Element('option', {
									value: _id,
									html: _title
								});

								this.getParent('.area-container').dispose();
								_option.inject(popup.getElement('select.additional-areas'));
							}

						});

						_areaContainer.adopt(_areaTitle, _removeBtn);
						_areaContainer.inject(selectedVotdAreasWrapper);
					});
					self.votdCalendarPopup.getElement('.close-btn').fireEvent('click');
					self.votdSelfSelectedDates = [];
					additionalAreas.fireEvent('change');
				});

				popup.getElement('.close-btn').addEvent('click', function(e){
					popup.getElement('#selectedVotdAreas')
						.getElements('.area-container a').each(function(el){
							el.fireEvent('click');
						});
					self.renderInfo();
				});
			},

			getGotdCalendarPopup: function() {
				this.gotdSelfSelectedDates = [];
				if (!this.gotdCalendarPopup) {
					var self = this,
						overlay = this.createOverlay(),
						popup = this.createPopup({
							actionFunction: function(){
								var _selectedDates = $('selectedGotdDates')
									.getElements('span').get('html');
								self.gotdSelfSelectedDates = _selectedDates;
								self.hidePopup(self.gotdCalendarPopup);
							}
						});
					
					popup.inject(overlay);
					overlay.inject(this.container);

					var _request = new Request({
						url: '/online-billing/escort-gotd-calendar',
						method: 'get',
						available_dates: self.availableGotdDates,

						onSuccess: function(responseText) {
							window.fireEvent('scloaded');
							popup.getElement('.inner-content').set('html', responseText);
							self.gotdCalendarGoto(popup);
						}
					}).send();

					this.gotdCalendarPopup = overlay;			
				} else {
					var self = this,
						areaId = this.gotdPopup
							.getElement('.additional-areas').getSelected().get('value'),
						calendarWrapper = this.gotdCalendarPopup.getElement('.ob-gotd-calendar-wrapper'),
						calendarContainer = this.gotdCalendarPopup.getElement('.escort-gotd-calendar'),
						selectedDateAreasEls = this.gotdPopup
							.getElements('#selectedGotdAreas > .area-container > span');
					this.gotdCalendarPopup.getElement('#selectedGotdDates').set('html', '');

					if (selectedDateAreasEls && selectedDateAreasEls.length > 0) {
						var preselectedDates = [];
						selectedDateAreasEls.each(function(el) {
							preselectedDates.push(el.get('html').split(':')[1].trim());
						});
					}

					var _request = new Request({
						url: '/online-billing/escort-gotd-calendar',
						method: 'get',
						data: {
							area_id: +areaId,
							available_dates: self.availableGotdDates,
							preselected_dates: preselectedDates
						},

						onLoadstart: function() {
							$$('.ob-gotd-calendar-overlay')[0].addClass('visible');
						},

						onSuccess: function(responseText) {
							calendarWrapper.dispose();

							Elements.from(responseText).inject(calendarContainer, 'before');
							self.gotdCalendarGoto(self.gotdCalendarPopup);

							self.selectGotdDates(
								self.gotdCalendarPopup.getElement('.ob-gotd-calendar-wrapper table')
							);

							$$('.ob-gotd-calendar-overlay')[0].removeClass('visible');
						}
					}).send();			
				}
			},

			getVotdCalendarPopup: function() {
				var self = this;
				this.votdSelfSelectedDates = [];
				if (!this.votdCalendarPopup) {
					var overlay = this.createOverlay(),
						popup = this.createPopup({
							actionFunction: function(){
								self.votdSelfSelectedDates = $('selectedVotdDates').getElements('span').get('html');
								self.hidePopup(self.votdCalendarPopup);
							}
						});
					
					popup.inject(overlay);
					overlay.inject(this.container);

					new Request({
						url: '/online-billing/escort-votd-calendar',
						method: 'get',
						available_dates: self.availableVotdDates,

						onSuccess: function(responseText) {
							window.fireEvent('scloaded');
							popup.getElement('.inner-content').set('html', responseText);
							self.votdCalendarGoto(popup);
						}
					}).send();

					this.votdCalendarPopup = overlay;			
				} else {
					var areaId = this.votdPopup
							.getElement('.additional-areas').getSelected().get('value'),
						calendarWrapper = this.votdCalendarPopup.getElement('.ob-votd-calendar-wrapper'),
						calendarContainer = this.votdCalendarPopup.getElement('.escort-votd-calendar'),
						selectedDateAreasEls = this.votdPopup
							.getElements('#selectedVotdAreas > .area-container > span');
					this.votdCalendarPopup.getElement('#selectedVotdDates').set('html', '');
					var preselectedDates = [];
					if (selectedDateAreasEls && selectedDateAreasEls.length > 0) {
						selectedDateAreasEls.each(function(el) {
							preselectedDates.push(el.get('html').split(':')[1].trim());
						});
					}

					new Request({
						url: '/online-billing/escort-votd-calendar',
						method: 'get',
						data: {
							area_id: +areaId,
							available_dates: self.availableVotdDates,
							preselected_dates: preselectedDates
						},

						onLoadstart: function() {
							$$('.ob-votd-calendar-overlay')[0].addClass('visible');
						},

						onSuccess: function(responseText) {
							calendarWrapper.dispose();

							Elements.from(responseText).inject(calendarContainer, 'before');
							self.votdCalendarGoto(self.votdCalendarPopup);

							self.selectVotdDates(
								self.votdCalendarPopup.getElement('.ob-votd-calendar-wrapper table')
							);

							$$('.ob-votd-calendar-overlay')[0].removeClass('visible');
						}
					}).send();			
				}
			},

			gotdCalendarGoto: function(popup){
				var btns = popup.getElements('thead a'),
					calendarWrapper = popup.getElement('.ob-gotd-calendar-wrapper'),
					calendarContainer = popup.getElement('.escort-gotd-calendar'),
					self = this;

				var selectedDateAreasEls = this.gotdPopup
					.getElements('#selectedGotdAreas > .area-container > span');
				var preselectedDates = [];
				if (selectedDateAreasEls && selectedDateAreasEls.length > 0) {
					selectedDateAreasEls.each(function(el) {
						preselectedDates.push(el.get('html').split(':')[1].trim());
					});
				}

				btns.addEvent('click', function(e) {
					e.stop();

					new Request({
						url: '/online-billing/escort-gotd-calendar',
						method: 'get',
						data: {
							calendar_month: this.get('data-month'),
							area_id: +this.get('data-area-id'),
							selected_dates: $('selectedGotdDates')
								.getElements('span').get('html'),
							preselected_dates: preselectedDates,
							available_dates: self.availableGotdDates
						},

						onLoadstart: function() {
							$$('.ob-gotd-calendar-overlay')[0].addClass('visible');
						},

						onSuccess: function(responseText){
							calendarWrapper.dispose();
							Elements.from(responseText).inject(calendarContainer, 'before');

							self.gotdCalendarGoto(popup);

							btns.removeClass('hidden');

							self.selectGotdDates(popup.getElement('.ob-gotd-calendar-wrapper table'));

							$$('.ob-gotd-calendar-overlay')[0].removeClass('visible');
						}
					}).send();
				});
			},

			votdCalendarGoto: function(popup){
				var calendar = popup.getElement('table.ob-votd-calendar'),
					btns = popup.getElements('thead a'),
					calendarWrapper = popup.getElement('.ob-votd-calendar-wrapper'),
					calendarContainer = popup.getElement('.escort-votd-calendar'),
					self = this;

				var selectedDateAreasEls = this.votdPopup
					.getElements('#selectedVotdAreas > .area-container > span');

				if (selectedDateAreasEls && selectedDateAreasEls.length > 0) {
					var preselectedDates = [];
					selectedDateAreasEls.each(function(el) {
						preselectedDates.push(el.get('html').split(':')[1].trim());
					});
				}

				btns.addEvent('click', function(e) {
					e.stop();

					new Request({
						url: '/online-billing/escort-votd-calendar',
						method: 'get',
						data: {
							calendar_month: this.get('data-month'),
							area_id: +this.get('data-area-id'),
							selected_dates: $('selectedVotdDates')
								.getElements('span').get('html'),
							preselected_dates: preselectedDates,
							available_dates: self.availableVotdDates
						},

						onLoadstart: function() {
							$$('.ob-votd-calendar-overlay')[0].addClass('visible');
						},

						onSuccess: function(responseText){
							calendarWrapper.dispose();
							Elements.from(responseText).inject(calendarContainer, 'before');

							self.votdCalendarGoto(popup);

							btns.removeClass('hidden');

							self.selectVotdDates(popup.getElement('.ob-votd-calendar-wrapper table'));

							$$('.ob-votd-calendar-overlay')[0].removeClass('visible');
						}
					}).send();
				});
			},

			selectGotdDates: function(calendar){
				var selectedGotdDatesContainer = $('selectedGotdDates');

				calendar.getElements('tbody td.package time').addEvent('click', function(){
					if (this.getParent('td').hasClass('preselected') || this.getParent('td').hasClass('booked') ) {
						return false;
					}

					if (this.hasClass('selected') || this.getParent('td').hasClass('selected')) {
						this.removeClass('selected');
						this.getParent('td').removeClass('selected')
						
						var selectedHtmlClass = '.booked-' + this.get('datetime');
						selectedGotdDatesContainer.getElement(selectedHtmlClass).dispose();
					} else {
						this.addClass('selected');

						var selectedDate = this.get('datetime'),
							selectedDateEl = new Element('span', {
							
							class: 'booked-' + selectedDate,

							styles: {
								display: 'inline-block',
								width: '57px',
								backgroundColor: '#ac2767',
								color: '#ffffff',
							    padding: '5px',
							    borderRadius: '3px',
							    margin: '2px 3px',
							    fontSize: '10px',
							    textAlign: 'center'
							},

							html: selectedDate
						});

						selectedDateEl.addEvent('click', function(e){
							if (e) {
								e.stop();
							}

							var selectedDateSingle = this,
								selectedDateElHtml = this.get('html');

							
							$$('.ob-gotd-calendar-wrapper table')[0]
									.getElements('time')
									.each(function(el){
										if (el.get('datetime') == selectedDateElHtml) {
											el.removeClass('selected');
											el.getParent('td').removeClass('selected');

											selectedDateSingle.dispose();
										}
									});
						});

						selectedDateEl.inject(selectedGotdDatesContainer);
					}
				});

				var popupInner = calendar.getParents('.inner')[0];
				popupInner.getElement('.close-btn').addEvent('click', function(e){
					popupInner.getElement('#selectedGotdDates')
						.getElements('span').each(function(el){
							el.fireEvent('click');
						});
				});
			},

			selectVotdDates: function(calendar){
				var selectedVotdDatesContainer = $('selectedVotdDates');

				calendar.getElements('tbody td.package time').addEvent('click', function(){
					if (this.getParent('td').hasClass('preselected') || this.getParent('td').hasClass('booked') ) {
						return false;
					}

					if (this.hasClass('selected') || this.getParent('td').hasClass('selected')) {
						this.removeClass('selected');
						this.getParent('td').removeClass('selected')
						
						var selectedHtmlClass = '.booked-' + this.get('datetime');
						selectedVotdDatesContainer.getElement(selectedHtmlClass).dispose();
					} else {
						this.addClass('selected');

						var selectedDate = this.get('datetime'),
							selectedDateEl = new Element('span', {
							
							class: 'booked-' + selectedDate,

							styles: {
								display: 'inline-block',
								width: '57px',
								backgroundColor: '#ac2767',
								color: '#ffffff',
							    padding: '5px',
							    borderRadius: '3px',
							    margin: '2px 3px',
							    fontSize: '10px',
							    textAlign: 'center'
							},

							html: selectedDate
						});

						selectedDateEl.addEvent('click', function(e){
							if (e) {
								e.stop();
							}

							var selectedDateSingle = this,
								selectedDateElHtml = this.get('html');

							
							$$('.ob-votd-calendar-wrapper table')[0]
									.getElements('time')
									.each(function(el){
										if (el.get('datetime') == selectedDateElHtml) {
											el.removeClass('selected');
											el.getParent('td').removeClass('selected');

											selectedDateSingle.dispose();
										}
									});
						});

						selectedDateEl.inject(selectedVotdDatesContainer);
					}
				});

				var popupInner = calendar.getParents('.inner')[0];
				popupInner.getElement('.close-btn').addEvent('click', function(e){
					popupInner.getElement('#selectedVotdDates')
						.getElements('span').each(function(el){
							el.fireEvent('click');
						});
				});
			},

			showPopup: function(popup) {
				popup.getElement('.inner')
					.removeClass('animated bounceOut')
					.addClass('animated bounceIn');

				popup.fade('in');
			},

			hidePopup: function(popup) {
				popup.getElement('.inner')
					.removeClass('animated bounceIn')
					.addClass('animated bounceOut')
				
				popup.fade('out');
			},

			getSummaryInfo: function() {
				var packageContainer = $$('.packages-container')[0],
					activationDateContainer = $$('.escort-activation-date')[0],
					locationContainer = $$('.escort-additional-areas')[0],
					gotdContainer = $$('.escort-gotd')[0],
					votdContainer = $$('.escort-votd')[0],
					gotdLocationDate = [],
					votdLocationDate = [],
					areasLocations = [];

				if (locationContainer.getElement('#selectedAreas')
						.getElements('.area-container').length > 0) {
						var locationContainers = locationContainer.getElement('#selectedAreas')
							.getElements('.area-container');

						locationContainers.each(function(el) {
							var _areaId = el.getElement('a').get('data-id'),
								_areaTitle = el.getElement('span').get('html');
								_additionalAreaPrice = $('additionalAreaPrice').get('value');
							areasLocations.push({
								a: _areaId,
								t: _areaTitle,
								p: _additionalAreaPrice
							});
						});
				}

				if (gotdContainer.getElement('#selectedGotdAreas').getElements('a').length > 0) {
					var areaContainers = gotdContainer
						.getElement('#selectedGotdAreas').getElements('.area-container');
					
					areaContainers.each(function(el) {
						var _date = el.getElement('span').get('html').split(':')[1].trim(),
							_areaId = el.getElement('a').get('data-id'),
							_areaTitle = el.getElement('strong').get('html');
							_gotdPrice = $('gotdPrice').get('value');

						gotdLocationDate.push({
							d: _date,
							a: _areaId,
							t: _areaTitle,
							p: _gotdPrice
						});
					});
				}		
				if (votdContainer.getElement('#selectedVotdAreas').getElements('a').length > 0) {
					var areaContainers = votdContainer
						.getElement('#selectedVotdAreas').getElements('.area-container');

					areaContainers.each(function(el) {
						var _date = el.getElement('span').get('html').split(':')[1].trim(),
							_areaId = el.getElement('a').get('data-id'),
							_areaTitle = el.getElement('strong').get('html');
							_votdPrice = $('votdPrice').get('value');

						votdLocationDate.push({
							d: _date,
							a: _areaId,
							t: _areaTitle,
							p: _votdPrice
						});
					});
				}	
				if ($$('.packages-container').length > 0) {
					return {
						package: {
							packageId: (packageContainer.getElement('a.selected')) ? 
								packageContainer.getElement('a.selected').get('data-id').trim() :
								0,
							packagePrice: (packageContainer.getElement('a.selected')) ?
								parseFloat(packageContainer.getElement('a.selected').get('data-price')) :
								0,
							packageCryptoPrice: (packageContainer.getElement('a.selected')) ?
								parseFloat(packageContainer.getElement('a.selected').get('data-crypto-price')) : 
								0,		
							packageDuration: (packageContainer.getElement('a.selected')) ?
								packageContainer.getElement('a.selected').get('data-duration').trim() :
								0,
							packageTitle: (packageContainer.getElement('a.selected')) ?
								packageContainer.getElement('a.selected')
									.getElement('.package-title').get('html').trim() :
								''
						},

						activation: {
							date: activationDateContainer.getElement('a.selected')
								.getElement('.activation-date').get('value'),
							time: activationDateContainer.getElement('a.selected')
								.getElement('.activation-time').get('value'),
						},

						areas: areasLocations,

						gotds: gotdLocationDate,
						votds: votdLocationDate,
					};
				} else {
					return {
						gotds: gotdLocationDate,
						votds: votdLocationDate
					}
				}
			},

			renderInfo: function() {
				var self = this,
					_data = this.getSummaryInfo();
				if (!_data.package && !_data.gotds && !_data.votds) return;

				var summaryContainer = this.container.getElement('.selected-items');
				summaryContainer.set('html', '');

				if (_data.package) {
					// < Selected Package >
					
						var _summaryPackageItemContainer = new Element('div', {
								class: 'summary-package-item-container',

								styles: {
									marginTop: '25px',
									float: 'left',
									width: '100%'
								}
							}),

							_itemName = new Element('span',  {
								class: 'summary-item-name',

								styles: {
									display: 'block',
									width: 'calc(100% - 30px)',
									height: '20px',
									overflow: 'hidden',
									textOverflow: 'ellipsis',
									whiteSpace: 'nowrap',
									float: 'left',
									fontSize: '14px',
									fontWeight: 'normal',
									color: '#000000'
								},

								html: '<strong>' + _data.package.packageTitle + '</strong> - ' + 
								'<span style="display: inline-block; padding: 0 5px; border-radius: 3px;' + 
								' color: #ffffff; background-color: rgb(126, 137, 154);' +
								' text-shadow: 1px 1px 0 rgba(94, 104, 119, 0.68);">'
								+ ((_data.activation.date && _data.activation.time) ? 
								(_data.activation.date + ' ' + _data.activation.time) : 
								mOBLang.mOBImmediately) + '</span>'
							}),

							_itemRemoveBtn = new Element('a', {
								class: 'summary-item-remove-btn',

								styles: {
									display: 'block',
									width: '20px',
									height: '20px',
									textAlign: 'center',
									lineHeight: '20px',
									fontSize: '20px',
									color: 'rgb(255, 255, 255)',
									float: 'left',
									textDecoration: 'none',
									borderRadius: '3px',
									backgroundColor: 'rgb(172, 39, 103)',
									marginRight: '10px'
								},

								html: '&times;'
							});

							_itemRemoveBtn.addEvent('click', function(e) {
								if (e) e.stop();

								if ($$('.summary-location-item-container').length > 0 || 
									$$('.summary-gotd-item-container').length > 0){
									var msg = mOBLang.mOBRemovePackage;
									
									if (confirm(msg)) {
										$$('.summary-location-item-container')
											.getElements('.summary-item-remove-btn').each(function(el){
												el.fireEvent('click');
											});
										$$('.summary-gotd-item-container')
											.getElements('.summary-item-remove-btn').each(function(el){
												el.fireEvent('click');
											});
										$$('.summary-package-item-container').dispose();
										$$('.summary-total-container').dispose();
										self.deselectPackages();
										$$('.count-amount').set('html', '');
                                        $('startTimeBtn').removeClass('active')
									}
								} else {
									this.getParent('.summary-package-item-container').dispose();
									$$('.summary-total-container').dispose();
									self.deselectPackages();
									$$('.count-amount').set('html', '');
                                    $('startTimeBtn').removeClass('active')
								}
							});

						_summaryPackageItemContainer.adopt(_itemName, _itemRemoveBtn);
						_summaryPackageItemContainer.inject(summaryContainer);

					// </ Selected Package >

					// < Selected Locations > 
					
						_itemsElementGroup = [];

						_data.areas.each(function(el){
							var _summaryLocationItemContainer = new Element('div', {
									class: 'summary-location-item-container',

									styles: {
										marginTop: '10px',
										float: 'left',
										width: '100%'
									}
								}),

								_areaItemName = new Element('span',  {
									class: 'summary-item-name',

									styles: {
										display: 'block',
										width: 'calc(100% - 30px)',
										height: '20px',
										overflow: 'hidden',
										textOverflow: 'ellipsis',
										whiteSpace: 'nowrap',
										float: 'left',
										fontSize: '14px',
										fontWeight: 'normal',
										color: '#000000'
									},

									html: '<strong style="font-size: 16px">+</strong> ' 
										+ el.t
								}),

								_areaItemRemoveBtn = new Element('a', {
									class: 'summary-item-remove-btn',

									styles: {
										display: 'block',
										width: '20px',
										height: '20px',
										textAlign: 'center',
										lineHeight: '20px',
										fontSize: '20px',
										color: 'rgb(255, 255, 255)',
										float: 'left',
										textDecoration: 'none',
										borderRadius: '3px',
										backgroundColor: 'rgb(172, 39, 103)',
										marginRight: '10px'
									},

									html: '&times;',

									'data-id': el.a
								});


								_areaItemRemoveBtn.addEvent('click', function(e) {
									if (e) e.stop();

									var __id = this.get('data-id');

									self.locationPopup.getElement('.result-section')
										.getElements('.additional-area-remove-btn').each(function(el) {
											if (el.get('data-id') == __id) {
												el.fireEvent('click');
											}
										});

									self.renderInfo();
								});

							_summaryLocationItemContainer.adopt(_areaItemName, _areaItemRemoveBtn);
							_summaryLocationItemContainer.inject(summaryContainer);
						});

					// </ Selected Locations >
				}

				// < Selected Gotds > 

				_data.gotds.each(function(el){
					var _summaryGotdItemContainer = new Element('div', {
							class: 'summary-gotd-item-container',

							styles: {
								marginTop: '10px',
								float: 'left',
								width: '100%'
							}
						}),

						_areaItemName = new Element('span',  {
							class: 'summary-item-name',

							styles: {
							    display: 'block',
							    width: 'calc(100% - 110px)',
							    height: '20px',
							    overflow: 'hidden',
							    textOverflow: 'ellipsis',
							    whiteSpace: 'nowrap',
							    float: 'left',
							    fontSize: '14px',
							    fontWeight: 'normal',
							    color: '#000000'
							},

							html: '<strong style="font-size: 12px;' 
								+ ' color: #7e899a;">GOTD: </strong> ' 
								+ el.t
						}),

						_areaItemDate = new Element('span', {
							class: 'summary-item-date',

							styles: {
								display: 'block',
								width: '80px',
								float: 'left',
								fontSize: '12px',
								fontWeight: 'normal',
								color: 'rgb(172, 39, 103)',
								lineHeight: '22px',
								fontWeight: 'bold'

							},

							html: el.d
						}),

						_areaItemRemoveBtn = new Element('a', {
							class: 'summary-item-remove-btn',

							styles: {
							    display: 'block',
							    width: '20px',
							    height: '20px',
							    textAlign: 'center',
							    lineHeight: '20px',
							    fontSize: '20px',
							    color: 'rgb(255, 255, 255)',
							    float: 'left',
							    textDecoration: 'none',
							    borderRadius: '3px',
							    backgroundColor: 'rgb(172, 39, 103)',
							    marginRight: '10px'
							},

							html: '&times;',

							'data-id': el.a
						});


						_areaItemRemoveBtn.addEvent('click', function(e) {
							if (e) e.stop();

							var __id = this.get('data-id'),
								__date = this.getSiblings('.summary-item-date')[0].get('html'),
								__selectedGotdDateEls = self.gotdPopup
									.getElement('#selectedGotdAreas')
									.getElements('.area-container');

							__selectedGotdDateEls.each(function(el) {
								if (el.getElement('span').get('html').split(':')[1]
									.trim() == __date && el.getElement('a')
									.get('data-id') == __id) {
									el.getElement('a').fireEvent('click');
								}
							});

							self.renderInfo();
						});

					_summaryGotdItemContainer.adopt(
						_areaItemName, 
						_areaItemDate, 
						_areaItemRemoveBtn
					);
					_summaryGotdItemContainer.inject(summaryContainer);
				});

				// </ Selected Gotds >

				// < Selected Votds > 

				_data.votds.each(function(el) {
					var _summaryVotdItemContainer = new Element('div', {
							class: 'summary-votd-item-container',

							styles: {
								marginTop: '10px',
								float: 'left',
								width: '100%'
							}
						}),

						_areaItemName = new Element('span', {
							class: 'summary-item-name',

							styles: {
								display: 'block',
								width: 'calc(100% - 110px)',
								height: '20px',
								overflow: 'hidden',
								textOverflow: 'ellipsis',
								whiteSpace: 'nowrap',
								float: 'left',
								fontSize: '14px',
								fontWeight: 'normal',
								color: '#000000'
							},

							html: '<strong style="font-size: 12px;' + ' color: #7e899a;">VOTD: </strong> ' + el.t
						}),

						_areaItemDate = new Element('span', {
							class: 'summary-item-date',

							styles: {
								display: 'block',
								width: '80px',
								float: 'left',
								fontSize: '12px',
								fontWeight: 'normal',
								color: 'rgb(172, 39, 103)',
								lineHeight: '22px',
								fontWeight: 'bold'

							},

							html: el.d
						})

					_areaItemRemoveBtn = new Element('a', {
						class: 'summary-item-remove-btn',

						styles: {
							display: 'block',
							width: '20px',
							height: '20px',
							textAlign: 'center',
							lineHeight: '20px',
							fontSize: '20px',
							color: 'rgb(255, 255, 255)',
							float: 'left',
							textDecoration: 'none',
							borderRadius: '3px',
							backgroundColor: 'rgb(172, 39, 103)',
							marginRight: '10px'
						},

						html: '&times;',

						'data-id': el.a
					});


					_areaItemRemoveBtn.addEvent('click', function(e) {
						if (e) e.stop();

						var __id = this.get('data-id'),
							__date = this.getSiblings('.summary-item-date')[0].get('html'),
							__selectedVotdDateEls = self.votdPopup
							.getElement('#selectedVotdAreas')
							.getElements('.area-container');

						__selectedVotdDateEls.each(function(el) {
							if (el.getElement('span').get('html').split(':')[1]
								.trim() == __date && el.getElement('a')
								.get('data-id') == __id) {
								el.getElement('a').fireEvent('click');
							}
						});

						self.renderInfo();
					});

					_summaryVotdItemContainer.adopt(
						_areaItemName,
						_areaItemDate,
						_areaItemRemoveBtn
					);
					_summaryVotdItemContainer.inject(summaryContainer);
				});

			// </ Selected Gotds >

				var countAmount = $('summaryContainer').getElement('.count-amount');
				if (_data.package) {
					countAmount.set('html', 
						(1 + +_data.areas.length + +_data.gotds.length + +_data.votds.length) 
						+ ' item(s) = CHF ' + (+_data.package.packagePrice 
						+ +_data.areas.length * +this.additionalAreaPrice 
						+ +_data.gotds.length * +this.gotdPrice 
						+ +_data.votds.length * +this.votdPrice) + '. ');
				} else {
					countAmount.set('html',
					( +_data.gotds.length + +_data.votds.length ) + ' item(s) = CHF ' +
					((+_data.gotds.length * +this.gotdPrice) + (+_data.votds.length * +this.votdPrice)) + '. '
				) ;
				}

				// if (_data.package) {
				// 	var totalSummary = new Element('div', {
				// 			class: 'summary-total-container',

				// 			styles: {
				// 				marginTop: '10px',
				// 				float: 'left',
				// 				width: 'calc(100% - 10px)',
				// 				fontSize: '14px',
				// 				color: '#000000',
				// 				fontWeight: 'normal',
				// 				paddingTop: '10px',
				// 				borderTop: '1px solid #d2d2d2'
				// 			},

				// 			html:  mOBLang.mOBTotal + ': ' + _data.package.packagePrice + 'CHF '
				// 				+ (_data.areas.length ? ' + ' + _data.areas.length + ' &times; ' 
				// 									+ this.additionalAreaPrice +  'CHF' : '')

				// 				+ (_data.gotds.length ? ' + ' + _data.gotds.length + ' &times; ' 
				// 									+ this.gotdPrice + 'CHF' : '') + ' = ' +
				// 				(+_data.package.packagePrice + +_data.areas.length 
				// 				* +this.additionalAreaPrice + +_data.gotds.length * +this.gotdPrice) + 'CHF'
				// 		});
				// } else {
				// 	var totalSummary = new Element('div', {
				// 			class: 'summary-total-container',

				// 			styles: {
				// 				marginTop: '10px',
				// 				float: 'left',
				// 				width: 'calc(100% - 10px)',
				// 				fontSize: '14px',
				// 				color: '#000000',
				// 				fontWeight: 'normal',
				// 				paddingTop: '10px',
				// 				borderTop: '1px solid #d2d2d2'
				// 			},

				// 			html: mOBLang.mOBTotal + ': '
				// 				+ (_data.gotds.length ? ' + ' + _data.gotds.length + ' &times; ' 
				// 									+ this.gotdPrice + 'CHF' : '') + ' = ' +
				// 				(+_data.gotds.length * +this.gotdPrice) + 'CHF'
				// 		});
				// }

				this.container.getElement('.selected-items').fireEvent('cartchanged');
			},

			toggleSummary: function() {
				var summaryContainer = $('summaryContainer');
				$('summaryContainer').toggleClass('expanded');
			},

			showNotification: function(msg, type) {
				if ($('notificationCenter')) $('notificationCenter').dispose();
				var bgColor = '';

				switch(type){
					case 'error':
						bgColor = '#f44336';
						break;
					case 'success':
						bgColor = '#7abf3e';
						break;
					case 'info':
						bgColor = '#fb8c00';
				}

				var notificationContainer = new Element('div', {
					id: 'notificationCenter',
					class: 'animated',

					styles: {
						width: '100%',
						height: '20px',
						position: 'fixed',
						top: 0,
						left: 0,
						backgroundColor: bgColor,
						visibility: 0,
						zIndex: 1010,
						fontFamily: 'Open Sans, sans-serif',
						color: '#ffffff',
						fontSize: '14px',
						padding: '3px',
						boxShadow: 'rgba(0, 0, 0, 0.2) 0px 2px 2px',
						borderRadius: '0 0 2px 2px'
					},

					html: msg
				});

				notificationContainer.inject(this.container, 'after');
				notificationContainer.addClass('fadeInDown');

				var notifShowTimeout = setTimeout(function() {
					notificationContainer.removeClass('fadeInDown');
					notificationContainer.dispose();
				}, 4000);

				notificationContainer.addEvent('click', function() {
					clearTimeout(notifShowTimeout);
					notificationContainer.removeClass('fadeInDown');
					notificationContainer.dispose();
				});
			},

			goToPaymentPage: function() {
				var _data = this.getSummaryInfo();
				var escortId = $('agencyEscortId').get('value');
				var cart = {};

				cart[escortId] = _data;


				new Request({
					url: '/online-billing/set-cart',
					method: 'post',
					data: {
						cart: cart,
						page: 'select-package'
					},

					onSuccess: function(resp) {
						if (JSON.decode(resp).success == true){
							window.location.href = '/online-billing/decision/';
						}
					}
				}).send();
			}
		};

		mOnlineBilling.init();
	}

	var decisionMakingPage = $$('.agency-online-billing.decision')[0];

	if (decisionMakingPage) {
		mOnlineBillingDecision = {
			paymentUrl: '/online-billing/payment',
			escortSelectionUrl: '/online-billing/agency-index',
			existingCart: existingCart,

			init: function() {
				this.renderCart();
				this.bindBtnEvents($('continueShopping'), this.continueShopping);
				this.bindBtnEvents($('checkoutAndPay'), this.checkoutAndPayBtn);
			},

			bindBtnEvents: function(el, action) {
				var self = this;

				if (!el || !action) return;

				el.addEvent('click', function(e) {
					if (e) e.stop();
					action.call(self);
				});
			},

			continueShopping: function() {
				var cart = this.existingCart;

				this.listenSave(this.escortSelectionUrl);
				this.saveCart(cart);
			},

			checkoutAndPayBtn: function() {
				var cart = this.existingCart;

				this.listenSave(this.paymentUrl);
				this.saveCart(cart);
			},

			listenSave: function(url) {
				window.addEvent('cartSaved', function() {
					window.location.href = url;
				});
			},

			saveCart: function(data) {
				new Request({
					url: '/online-billing/set-cart',
					method: 'post',
					data: {
						cart: data,
						page: 'decision'
					},

					onSuccess: function(resp) {
						if (JSON.parse(resp).success) {
							window.fireEvent('cartSaved');
						}
					}

				}).send();
			},

			removeFromCart: function(escort, item, id, dt) {
				var cart = this.existingCart;
				var itemToMutate = cart[escort][item];


				switch (item) {
					case 'gotds':
					case 'votds':	
						itemToMutate.each(function(i) {
							if (i.a == id && i.d == dt){
								itemToMutate.erase(i);
							}
						});

						if (itemToMutate.length == 0) {
							delete cart[escort][item];
						}

						break;
					
					case 'areas':
						itemToMutate.each(function(i) {
							if (i.a == id){
								itemToMutate.erase(i);
							}
						});

						if (itemToMutate.length == 0) {
							delete cart[escort][item];
						}

						break;

					case 'package':
						delete cart[escort][item];
						delete cart[escort]['activation'];

						break;
				}

				if (Object.getLength(cart[escort]) == 0)
					 delete cart[escort];


			},

			renderCart: function() {
				var self = this;

				Object.each(this.existingCart, function(item, id) {
					
					var escortId = id;

					// Escort Package
					var escortPackage = item.package;

					if (escortPackage) {
						self.drawItem(
							escortId, 
							escortPackage.packageTitle, 
							escortPackage.packageId,
							'package'
						);
					}

					// Activation Day/Time
					var escortActivation = item.activation;

					if (escortActivation) {
					}

					// Escort Areas
					var escortAreas = item.areas;

					if (escortAreas) {
						escortAreas.each(function(el){
							self.drawItem(
								escortId,
								el.t,
								el.a,
								'areas'
							);
						});
					}

					// Escort Gotds
					var escortGotds = item.gotds;

					if (escortGotds) {
						escortGotds.each(function(el){
							self.drawItem(
								escortId,
								el.d + ' ' + el.t,
								el.a,
								'gotds',
								el.d
							);
						});	
					}

					// Escort Votds
					var escortVotds = item.votds;

					if (escortVotds) {
						escortVotds.each(function(el){
							self.drawItem(
								escortId,
								el.d + ' ' + el.t,
								el.a,
								'votds',
								el.d
							);
						});	
					}
				});
				
				self.countTotal();
			},

			drawItem: function(escortId, desc, id, type) {
				var self = this;

				var container = new Element('div', {
					class: 's-container',

					styles: {
						width: '100%',
						height: 'auto',
						float: 'left',
						marginBottom: '10px'
					}
				});

				var desc = new Element('span', {
					class: 'container-desc',

					styles: {
						display: 'block',
						width: '80%',
						height: '20px',
						float: 'left',
						fontSize: '12px',
						lineHeight: '20px',
						overflow: 'hidden',
						textOverflow: 'ellipsis',
						whiteSpace: 'nowrap'
					},

					html: '<i class="esc-id">' + escortId + '</i>'
						+ '<i class="type">' + type + '</i> ' + desc
				});

				var removeBtn = new Element('a', {
					class: 'remove-btn',
					styles: {
						display: 'block',
						marginLeft: 'calc(20% - 20px)',
						width: '20px',
						height: '20px',
						borderRadius: '3px',
						backgroundColor: 'rgb(172, 39, 103)',
						textAlign: 'center',
						lineHeight: '22px',
						textDecoration: 'none',
						color: '#ffffff',
						float: 'left',
						fontWeight: 'bold',
						fontSize: '16px'
					},

					html: '&times;',

					'data-id': id,
					'data-item': type,
					'data-escort': escortId,
					'data-date': arguments[4]
				});

				removeBtn.addEvent('click', function(e){ 
					if (e) e.stop();

					var _btn = this;
					var _item = this.get('data-item');
					var _escortId = this.get('data-escort');
					var _id = this.get('data-id');
					var _date = this.get('data-date') || false;

					if (_item == 'package') {
						var msg = 'Do you want to delete the package'
								+ ' and all its content?';

						var agreed = confirm(msg);

						if (agreed) {
							$('summaryContainer')
								.getElements('a.remove-btn').each(function(el) {
									if (_btn != el && el.get('data-escort') == _escortId) {
										el.fireEvent('click');
									}
								});
						} else {
							return false;
						}
					}

					this.getParent('.s-container').dispose();
					self.removeFromCart(_escortId, _item, _id, _date);
					self.countTotal();
				});

				container.adopt(desc, removeBtn).inject($('summaryContainer'));
			},
			
			countTotal: function(){
				var total = 0;
				
				var totalContainerBox = $$('#summaryContainer .total-container')[0];
				
				if(totalContainerBox){
					totalContainerBox.dispose();
				}
				
				Object.each(this.existingCart, function(item, id) {
					// Escort Package
					var escortPackage = item.package;

					if (escortPackage) {
						total += parseInt(escortPackage.packagePrice);
					}

					
					// Escort Areas
					var escortAreas = item.areas;

					if (escortAreas) {
						escortAreas.each(function(el){
							total += parseInt(el.p);
						});
					}

					// Escort Gotds
					var escortGotds = item.gotds;

					if (escortGotds) {
						escortGotds.each(function(el){
							total += parseInt(el.p);
						});	
					}

					// Escort Votds
					var escortVotds = item.votds;

					if (escortVotds) {
						escortVotds.each(function(el){
							total += parseInt(el.p);
						});	
					}
				});
				
				var totalContainer = new Element('div', {
					class: 'total-container clear'
				});
				
				var totalText = new Element('span', {
					class: 'total-title',
					html: 'Total '
				}).inject(totalContainer);
				
				new Element('span', {
					class: 'total-amount',
					html: total + ' CHF'
				}).inject(totalText);
				
				
				totalContainer.inject($('summaryContainer'));
			}
			
		};

		mOnlineBillingDecision.init();
	}

	var paymentPage = $$('.online-billing-payment')[0];

	if (paymentPage) {
		mOnlineBillingPayment = {
			paymentMethod: '',

			init: function() {
				this.selectPaymentMethod();
				this.bindBtnEvents($$('.next-step-button')[0], this.checkout);
			},

			selectPaymentMethod: function() {
				var self = this,
					paymentOptions = paymentPage.getElements('.payment-container a');

				paymentOptions.addEvent('click', function(e){
					e.stop();

					paymentOptions.removeClass('selected');
					self.paymentMethod = this.get('data-id');
					this.addClass('selected');
				});
			},

			bindBtnEvents: function(el, action) {
				var self = this;

				if (!el || !action) return;

				el.addEvent('click', function(e) {
					if (e) e.stop();
					action.call(self);
				});
			},	

			checkout: function() {
				if ($('yold_18') && $$('input[name=yold_18]:checked').length > 0) {
				var paymentMethod = $$('.payment-container .selected')[0].get('data-id');
				
				new Request({
					url: '/online-billing/checkout',
					method: 'post',
					data: {
						payment_method: paymentMethod
					},

					onProgress: function() {
						$$('.btn-overlay')[0].removeClass('hidden');
					},

					onSuccess: function(resp) {
						var r = JSON.decode(resp);
						if (r.status === 'success') {
							if (paymentMethod === 'm' || paymentMethod === 'cc_epg' || paymentMethod === 'cc_2000charge'|| paymentMethod === 'cc_faxin' ) {
								window.location.href = r.url;	
							} else if (paymentMethod === 'cc_twispay') {
								var $form = $('twispayForm').set('html', r.form);
								$form.getElement('input[type="submit"]').click();
							}else if (paymentMethod === 'cc_postfinance' || paymentMethod === 'cc_coinpass') {
								var $form = $('postfinanceForm').set('html', r.form);
								$form.getElement('input[type="submit"]').click();
							}
						} else {
							alert('Something went wrong');
							window.location.reload();
						}
					}
				}).send();
				}else{
					alert('Age confirmation required');
					$('18-yold-label').addClass('shaker');
					setTimeout(function(){ $('18-yold-label').removeClass('shaker'); }, 1000);
				}
			}


		}

		mOnlineBillingPayment.init();
	}

}());