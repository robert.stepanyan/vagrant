window.addEvent('domready', function () {
	// Our instance for the element with id "demo-word"
	new Autocompleter.Request.JSON($('f_showname'), '/reviews/ajax-escorts-search', {
		postVar: 'a_showname',
		indicatorClass: 'autocompleter-loading'
	}).addEvent('onSelection', function (element, selected, value, input_sh) {
		$('f_showname').set('value', selected.inputValue.name);
	});

	new Autocompleter.Request.JSON($('f_member'), '/reviews/ajax-members-search', {
		postVar: 'a_member',
		indicatorClass: 'autocompleter-loading'
	}).addEvent('onSelection', function (element, selected, value, input_sh) {
		$('f_member').set('value', selected.inputValue.name);
	});
});