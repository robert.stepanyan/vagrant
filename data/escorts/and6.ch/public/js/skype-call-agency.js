window.addEvent('domready', function(){
  expandAreaToggle();
  getEscortSkype();

})



var getEscortSkype = function(){
  $$('#select-escort-for-skype').addEvent('change', function(){
      loader(1);
      let escort_id = $$('#select-escort-for-skype').getSelected()[0].get('value')[0];
      if(escort_id == ''){loader(0);return false;}

      new Request({
        url: '/skype/get-agency-escort-skype-settings',
        method:'POST',
        noCache: true,
        evalScripts: true,
        data: {escort_id:escort_id},
        onSuccess: function(resp){
          $$('#agency-escort-skype-settings').set('html', resp);
          $$('#agency-escort-skype-settings').removeClass('none');
          pricesPairs();
          saveSettings();
          paymentOptionSwitcher();
        },
        onComplete:function(){
         loader(0);
        }
      }).send()

  })
}


var saveSettings  = function(){
  $$('.skype-booking-btn').addEvent('click', function(){
      submitSkypeSettings();
  }),

  $$('#toggle').addEvent('change', function(){
      submitSkypeSettings();
      let option = $$('#select-escort-for-skype').getSelected()[0];
      let content = option.get('data-showname');

       var checkbox = $$('input.toggle-value:checked');
       if(typeof checkbox[0]!='undefined'){
        option.set('html', content+' 🟢');
       }else{
         option.set('html', content+' ⚫');
       }
  });
}
var submitSkypeSettings = function(){
  loader(1);
  var formObjects = $('skype-form').toQueryString().parseQueryString();
  var ajax = new Request({
    url: '/skype/update',
    method:'POST',
    data: formObjects,
    onSuccess: function(resp){
    resp = JSON.decode(resp);

     if(resp.status == 'error'){
        $$('input').removeClass('skype-input-error');
        let inputName = Object.keys(resp.msgs)[0];
        let inputValues = Object.values(resp.msgs)[0];
        $(inputName).addClass('skype-input-error');
        //inputValues
        $('skype-status').set('html', '');
      }
      else{
        $('skype-status').set('html', 'Einstellungen erfolgreich gespeichert');
        $$('input').removeClass('skype-input-error');
      }
    },
    onComplete:function()
    {
     loader(0);
    }
  });

  ajax.send();
}

var pricesPairs = function(){
  $$("#price-60").addEvent('change',function() {
      var should_be_selected = $(this).getElements(":selected")[0].get('class');
    $$("#price-30").getElement('.' + should_be_selected)[0].selected=true

  }); 
  $$("#price-30").addEvent('change',function() {
      var should_be_selected = $(this).getElements(":selected")[0].get('class');
     $$("#price-60").getElement('.' + should_be_selected)[0].selected=true
  }); 
}

var expandAreaToggle = function(){
  $$('.select-escorts').addEvent('click', function(){
     $$('.skype-block-body').toggleClass('none')
     $$('.select-escort-arrow').toggleClass('rotate');
     $$('.skype-block').toggleClass('blue');
  });
}

var loader = function(status){
   if(status == 1){
     $$('.skype-overlay').removeClass('none');
      $$('.toggle-skype-btn').setStyle('pointer-events', 'none');
   }else{
     $$('.skype-overlay').addClass('none');
     $$('.toggle-skype-btn').setStyle('pointer-events', 'auto');
   }
}

var paymentOptionSwitcher = function(){
  $$("input[name=skype_payment_option]").addEvent('change', function(){
     $$(".skype-payment-option-1").toggleClass('none');
     $$(".skype-payment-option-2").toggleClass('none');
  })
}