/* --> Feedback */
Cubix.DPopup = {};


Cubix.DPopup.inProcess = false;

Cubix.DPopup.url = '';

Cubix.DPopup.Show = function (box_height, box_width) {
	if ( Cubix.DPopup.inProcess ) return false;

	var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000', width: 100, height: 100 });
	
	$$('.overlay').addClass('active');

	var y_offset = 30;

	var container = new Element('div', { 'class': 'DPopup-wrapper'}).setStyles({
		left: 0,
		top:  110,
		opacity: 0,
		width:'100%',
        position: 'absolute',
        'z-index': 999,
        background: '#fff'
	}).inject(document.body);
	
	Cubix.DPopup.inProcess = true;

	new Request({
		url: Cubix.DPopup.url,
		method: 'get',
		onSuccess: function (resp) {
			
			Cubix.DPopup.inProcess = false;
			container.set('html', resp);

			// var close_btn = new Element('div', {
			// 	html: '',
			// 	'class': 'popup-close-btn-v2'
			// }).inject(container);
			var close_btn = $('close_icon_')
			close_btn.addEvent('click', function() {
				$$('.DPopup-wrapper').destroy();
				$$('.overlay').removeClass('active');
				$('wrap').setStyles({
			 	'overflow-x': 'visible'
			 });
			});

			container.tween('opacity', '1');
			
			$('wrap').setStyles({
			 	'overflow-x': 'hidden'
			 });
			
		}
	}).send();
		
	
	return false;
}