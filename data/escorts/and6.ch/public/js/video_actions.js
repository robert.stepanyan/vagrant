
/* --> LocationHash */
Cubix.LocationHash = {};

Cubix.LocationHash.Set = function (hash) {
	if ( hash.length ) {
		document.location.hash = '#' + hash;
	}
	else {
		document.location.hash = '';
	}
	
	return false;
}

Cubix.LocationHash.Parse = function () {
	var hash = document.location.hash.substring(1);
	
	if ( ! hash.length ) {
		return {};
	}
	
	var params = hash.split(';');
	
	var filter = '';
	
	var not_array_params = ['page', 'city', 'city_id', 'action_type', 'pr'];
	
	params.each(function (param) {
		var key_value = param.split('=');
		
		var key = key_value[0];
		var val = key_value[1];
		
		if ( val === undefined ) return;
		
		val = val.split(',');
				
		val.each(function(it) {
			filter += key + ( ( ! not_array_params.contains(key) ) ? '[]=' : '=' ) + it + '&';
		});
	});
	
	return filter;
}

Cubix.LocationHash.Make = function (params) {
	
	var hash = '';
	var sep = ';';
	var value_glue = ',';
	
	var places = $$('#la-header-wrp .header');
	
	var map = {};
	places.each(function(place) {
		var inputs = place.getElements('input[type=text]');
		inputs.append(place.getElements('input[type=hidden]'));
		inputs.each(function(input) {
			if ( input.get('value').length ) {
				var index = input.get('name').replace('[]', '');
				if ( map[index] === undefined ) map[index] = new Array();
				map[index].append([input.get('value')]);
			}
		});
		
		var selects = place.getElements('select');
		selects.each(function(select) {
			
			if ( select.getSelected().get('value') != 0 ) {
				var index = select.get('name').replace('[]', '');
				if ( map[index] === undefined ) map[index] = new Array();
				map[index].append([select.getSelected().get('value')]);
			}
		});
	});
	
	map = $merge(map, params);
	
	for (var i in map) {
		hash += i + '=' + map[i].join(value_glue) + sep;
	}
	
	return hash;
}
/* <-- */

/* --> HashController */
Cubix.HashController = {
	_current: '',
	
	init: function () {
		setInterval('Cubix.HashController.check()', 100);
	},
	
	check: function () {
		
		var hash = document.location.hash.substring(1);
		
		if (hash != this._current) {
			this._current = hash;
					
			var data = Cubix.LocationHash.Parse();
			Cubix.VideoActions.Load(data);
		}
	}
};
/* <-- */

Cubix.Filter = {};
Cubix.Filter.Set = function (filter) {
		
	Cubix.LocationHash.Set(Cubix.LocationHash.Make(filter));

	return false;
}

Cubix.VideoActions = {};
Cubix.VideoActions.container = 'escort-videos';
Cubix.VideoActions.list_url = '/escorts/videos';

Cubix.VideoActions.Load = function (data) {
	
	var url = Cubix.VideoActions.list_url;
	
	var overlay = new Cubix.Overlay($(Cubix.VideoActions.container), {
		loader: _st('loader-small.gif'),
		position: '50%'
	});
	
	overlay.disable();
	
	new Request({
		url: url,
		method: 'POST',
		data: data,
		onSuccess: function (resp) {
			
			$(Cubix.VideoActions.container).set('html', resp);
			
			overlay.enable();
		}
	}).send();
	
	return false;
};


window.addEvent('domready', function () {
	Cubix.HashController.init();

});
