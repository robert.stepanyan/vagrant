window.addEvent('domready', function () {
 
  $$('.expand-collapse').addEvent('click', function(){
    $$(this).toggleClass('expended');
    $$('.expended-area').toggleClass('none');

  });
  $$('.mobile-cam-booking-mini').addEvent('click', function(){
    $$('.mobile-cam-booking-call-block').toggleClass('expended');
     $$('.expended-area').toggleClass('none');
  });
  
 
  $$("#toggle").addEvent('change', function(){
      saveSettings();
  });

   $$("input[name=skype_payment_option]").addEvent('change', function(){
     $$(".cam-booking-payment-option-1").toggleClass('none');
     $$(".cam-booking-payment-option-2").toggleClass('none');
  });

  $$('.cam-booking-booking-btn').addEvent('click', function(){
    saveSettings();
  });

   $$("#price-60").addEvent('change',function() {
      var should_be_selected = $(this).getElements(":selected")[0].get('class');
    $$("#price-30").getElement('.' + should_be_selected)[0].selected=true;
      settks();

  }); 
  $$("#price-30").addEvent('change',function() {
      var should_be_selected = $(this).getElements(":selected")[0].get('class');
     $$("#price-60").getElement('.' + should_be_selected)[0].selected=true;
       settks();
  }); 
});

var saveSettings = function(){
  loader(1);
  var formObjects = $('cam-booking-form').toQueryString().parseQueryString();
  var ajax = new Request({
    url: '/cam-booking/save',
    method:'POST',
    data: formObjects,
    onSuccess: function(resp){
    resp = JSON.decode(resp);

     if(resp.status == 'error'){
        $$('input').removeClass('cam-booking-input-error');
        let inputName = Object.keys(resp.msgs)[0];
        let inputValues = Object.values(resp.msgs)[0];
        $(inputName).addClass('cam-booking-input-error');
        //inputValues
        $('cam-booking-status').set('html', '');
      }
      else{
        $('cam-booking-status').set('html', 'Einstellungen erfolgreich gespeichert');
        $$('input').removeClass('cam-booking-input-error');
      }
    },
    onComplete:function()
    {
     loader(0);
    }
  });

  ajax.send();
}

var settks = function(){
   $$('#price30tks').set('html', $$("#price-30").getElements(":selected")[0].get("value") * 10);
   $$('#price60tks').set('html', $$("#price-60").getElements(":selected")[0].get("value") * 10);
}

var loader = function(status){
   if(status == 1){
     $$('.cam-booking-overlay').removeClass('none');
      $$('.toggle-cam-booking-btn').setStyle('pointer-events', 'none');
   }else{
     $$('.cam-booking-overlay').addClass('none');
     $$('.toggle-cam-booking-btn').setStyle('pointer-events', 'auto');
   }
}


document.addEvent('domready', function () {
    const queryString = window.location.search;
    var  urlParams = new URLSearchParams(queryString);
    var  currentPage = urlParams.get('page');
    if ($('pages-count')) {var  pagesCount = $('pages-count').get('value');}
    var  lastButton = new Element('a', {
        href: '#',
        class: 'last-button',
        'data-page':parseInt(pagesCount),
        id: 'last',
        html: '>>',
        events: {
            click: function(e){
                e.stop();
                $('cam-booking-load').addClass('loader');
                $('loader-back').addClass('loader-back');
                ajaxData(this);
                $('last').set('data-page',parseInt(pagesCount));

            },
        }
    });
    var  firstButton = new Element('a', {
        href: '#',
        class: 'first-button',
        'data-page': 1,
        id: 'first',
        html: '<<',
        events: {
            click: function(e){
                e.stop();
                $('cam-booking-load').addClass('loader');
                $('loader-back').addClass('loader-back');
                ajaxData(this);
                $('first').set('data-page',1);
            },
        }
    });
    var  nextButton = new Element('a', {
        href: '#',
        class: 'next-button',
        'data-page':parseInt(currentPage)+1,
        id: 'next',
        html: '>',
        events: {
            click: function(e){
                e.stop();
                $('cam-booking-load').addClass('loader');
                $('loader-back').addClass('loader-back');
                ajaxData(this);
                if (pagesCount >= (parseInt(currentPage)+1))
                {
                    $('next').set('data-page',parseInt(currentPage)+1);
                }
            },
        }
    });

    var previewsButton = new Element('a', {
        href: '#',
        class: 'previews-button',
        id:'previews',
        html: '<',
        'data-page':parseInt(currentPage)-1,
        events: {
            click: function(e){
                e.stop();
                $('cam-booking-load').addClass('loader');
                $('loader-back').addClass('loader-back');
                ajaxData(this);
                if ((parseInt(currentPage)-1) > 0)
                {
                    $('previews').set('data-page',parseInt(currentPage)-1);
                }
            },
        }
    });


    if( pagesCount > currentPage)
    {
        $$('.paging-list').grab(nextButton);
        $$('.paging-list').grab(lastButton);
    }else{
        $$('.paging-list').grab(previewsButton,'top');
        $$('.paging-list').grab(firstButton,'top');
    }


    $$('.page-button').addEvent('click', function(e) {
        e.stop();
        $('cam-booking-load').addClass('loader');
        $('loader-back').addClass('loader-back');
        ajaxData(this);
    });

    $$('.move').addEvent('click', function (e) {
        e.stop();
        $('cam-booking-load').addClass('loader');
        $('loader-back').addClass('loader-back');
        moveToDone(this);
    });
    var ajaxData = function ($this) {
        var page = $this.get('data-page');
        // this.set('style','color:black');
        var data = {};
        data.page = page;
        data.ajax = true;
        window.history.pushState('page'+page, 'Title', '/private-v2/cam-booking-bookings?page='+page);
        $$('.page-button').removeClass('active');
        new Request({
            url:  "/private-v2/cam-booking-bookings?ajax=true",
            method: 'post',
            evalScripts: true,
            data: data,
            onSuccess: function (resp) {
                resp = JSON.decode(resp);
                $('cam-booking-setup-area').empty();
                Array.each(resp, function(day, index){
                    var name = '';
                    if (day.name)
                    {
                        name = day.name;
                    }else{
                        name = 'Unnamed member';
                    }
                    var html = new Element('div', {
                        'class': 'cam-booking-bookings-content',
                        html: '<strong>'+name+'</strong><br>Skype Id :'+day.skype+' <br> has paid for '+day.duration+' minutes call with you.',
                    });
                    var dateTime = new Element('div', {
                        'class': 'cam-booking-bookings-date',
                        html: new Intl.DateTimeFormat('en-GB', {
                            year: 'numeric',
                            month: 'short',
                            day: 'numeric',
                            hour:'numeric',
                            hour12: false,
                            minute: 'numeric'
                        }).format(new Date(day.creation_date))
                    });
                    var moveButton = new Element('a', {
                        href: '#',
                        class: 'move-button',
                        'data-id': parseInt(day.id),
                        'data-page': parseInt(page),
                        html: 'move to last',
                        events: {
                            click: function (e) {
                                e.stop();
                                $('cam-booking-load').addClass('loader');
                                $('loader-back').addClass('loader-back');
                                moveToDone(this);
                            },
                        }
                    });
                    $('cam-booking-setup-area').grab(html);
                    $$('.cam-booking-bookings-content').grab(dateTime);
                    if (day.progress_status == 0)
                    {
                        $$('.cam-booking-bookings-content').grab(moveButton);
                    }
                });
                if( pagesCount > page)
                {
                    nextButton.set('data-page',parseInt(page)+1);
                    $$('.paging-list').grab(nextButton);
                    $$('.paging-list').grab(lastButton);
                    if (previewsButton) previewsButton.hide();
                    if (firstButton) firstButton.hide();
                    if (nextButton) nextButton.show();
                    if (lastButton) lastButton.show();
                }else{
                    previewsButton.set('data-page',parseInt(page)-1);
                    $$('.paging-list').grab(previewsButton,'top');
                    $$('.paging-list').grab(firstButton,'top');
                    if (nextButton) nextButton.hide();
                    if (lastButton) lastButton.hide();
                    if (previewsButton)
                    {
                        previewsButton.show();
                        firstButton.show();
                        firstButton.set('style','');
                        previewsButton.set('style','');
                    }
                }

                $$('.page-button').removeClass('active');


                var changable = $$('.page-button[data-page='+page+']');
                changable.addClass('active');
                $('cam-booking-load').removeClass('loader');
                $('loader-back').removeClass('loader-back');
            }
        }).send();

    };
    var moveToDone = function ($this) {
        var dataID = $this.get('data-id');
        var data = {};
        data.id = dataID;
        data.status = 1;

        new Request({
            url: '/private-v2/cam-booking-set-status',
            method: 'post',
            evalScripts: true,
            data: data,
            onSuccess: function (resp) {
                resp = JSON.decode(resp);
                console.log(resp);
                ajaxData($this);
            }
        }).send();
    };
});