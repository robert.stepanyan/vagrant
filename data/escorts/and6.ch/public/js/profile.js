window.addEvent('domready', function(){
	
	if ( $(profileVars.lang + '_chat_with_me_btn') ) {
		var chat_btn = $(profileVars.lang + '_chat_with_me_btn');

		if ( ! profileVars.currentUser ) {
		chat_btn.addEvent('click', function(e){
			e.stop();
			Cubix.Popup.Show('489', '652');
		});
		} else {
			chat_btn.addEvent('click', function(e){
				e.stop();
				//window.open('http://www.escortforum.net/index/chat?init_private=<?= $this->escort->username ?>', 'escortforum', 'height=620, width=820');
				window.open(profileVars.chatUrl, 'escortforum', 'height=620, width=820');
			});
		}
	}
	
	if ( $('sedcard-views') ) {
		var overlay = new Cubix.Overlay($('sedcard-views'), {
			loader: _st('loader-small.gif'),
			position: '50%',
			offset: {
				left: 0,
				top: -1
			}
		});

		overlay.disable();
	}

	var link = '';
	if ( profileVars.lang != profileVars.defLang ) {
		link = "&lang_id=" + profileVars.lang ;
	}
	
	/*new Request({
		url: '/index/sedcard-total-views?escort_id=' + profileVars.escortId + link,
		method: 'get',
		onSuccess: function (resp) {
			$('sedcard-views').set('html', resp);
			overlay.enable();
			if ( resp.length == 0 ){
				$('sedcard-views').setStyle('display','none');
			}
		}
	}).send();*/
	
	if ( ! profileVars.disableReview ) {
		$$('.add-review').addEvent('click', function(e){
			if ( ! profileVars.currentUser || profileVars.currentUser.user_type != 'member' ) {
				e.stop();
				Cubix.Popup.Show('489', '652');
			} else {
				window.location = profileVars.reviewsUrl;
			}
		});
	}
	
	$$('.tell-friend').addEvent('click', function(e){
		e.stop();
		Cubix.RPopup.url = this.get('href');
		Cubix.RPopup.type = this.get('class');
		Cubix.RPopup.location = $('setcard').getElement('.review-options');
		Cubix.RPopup.Show(120, 48);
	});

	$$('.report-problem').addEvent('click', function(e){
		e.stop();
		Cubix.RPopup.url = this.get('href');
		Cubix.RPopup.type = this.get('class');
		Cubix.RPopup.location = $('setcard').getElement('.review-options');
		Cubix.RPopup.Show(120, 20);
	});
	
	/*
	var overlay = new Cubix.Overlay($('voting-widget'), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: -1, top: -1
		}
	});

	var form = $('voting-widget').getElement('form');
	
	if ( form ) {
		form.set('send', {
			onSuccess: function (resp) {
				resp = JSON.decode(resp);

				if ( 'error' == resp.status ) {
					alert(resp.desc);
					overlay.enable();
					return;
				}

				$('voting-widget').getElement('.ajax-container').set('html', resp.html);

				overlay.enable();
			}
		}).addEvent('submit', function (e) {
			e.stop();

			if ( 'true' != Cubix.Comments.is_member ) {
				return Cubix.Popup.Show('489', '652');
			}

			overlay.disable();
			this.send();
		});
	}*/
});