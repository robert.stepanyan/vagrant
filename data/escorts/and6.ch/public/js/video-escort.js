window.addEvent('domready', function () {
  'use strict';

  //ClickVideo();
  //Hover();
  PlayVideo();

});

/*var ClickVideo = function()
{
  $$('.video>li').addEvent('click',function(){
    var img = $(this).getChildren('img');
    var width = img.get('owidth')[0];
    var height = img.get('oheight')[0];
    var video = img.get('rel');
    var image = img.get('src')[0].replace('m320','orig');
    video = $('edit_video').get('config')+video;
    Play(video,image,width,height,true);
  });
};*/

var Play = function (escortId, video, videoId, image, width, height, auto, source) {
  'use strict';

  var primary = "HTML5";
  /*if(Browser.Plugins.Flash && Browser.Plugins.Flash.version >= 9){
      primary = "flash";
    }*/
  /*else{
      var videoFile = video + "_360p.mp4";
      source = [{file: videoFile}];
      width = width / height * 360;
      height = 360;
      auto = false;
    }*/


     var start_width = 410;
     var start_height = 310;

     if(height > width){
       start_height = 600;
     }

      var JP = jwplayer("video-modal")
        .setup({
          primary: primary,
          flashplayer: "/jwplayer.flash.swf",
          image: image,
          width:start_width,
          height:start_height,
          autostart: auto,
          logo: {
            file: "/img/photo-video/logo.png"
          },
          skin: {
            name: "bekle"
          },
          sources: source,
          /*provider: 'rtmp',
              file:'rtmp://'+video,
              streamer:'rtmp://'+video*/
          events: {
            onReady: function () {},
            onQualityChange: function () {
              var index = JP.getCurrentQuality();
              var videoInfo = source[index];
              JP.resize(start_width, start_height);

            }
          }
        });
        // .once("complete", function () {
        //   new VideoVoting("video-modal", {
        //     videoId: videoId
        //   });
        // });

        $$('.video_overlay').destroy();
};

/*var Hover = function()
{
    var interval;
    if($$('.video'))
    $$('.video').addEvent(
       'mouseenter',function()
       {if($(this).getElement('li'))
        {  
          var id = $(this).getElement('li').get('id');
          if(!interval)
          {        
            interval = setInterval(function(){
            var current = $(id).getParent('ul').getElement('.current');
            if(current)
            {var next = current.getNext('li');
              current.removeAttribute('class')
              var html =next?next.addClass('current').get('html'):$(id).addClass('current').get('html');
              html+='<span class="video_play_btn"></span>';
              $(id).set('html',html);
            }
            },1000);
          }
        }
       }
    );
   $$('.video').addEvent('mouseleave',function(){
    clearInterval(interval);
    interval=null;
    });
};*/

var PlayVideo = function () {
  'use strict';

  if ($('video-play-wrapper')) {
    $('video-play-wrapper').addEvent('click', function () {
      var escortId = $('escort_id').get('value');
      //var prop = $('prop').get('value').split('-');
      var prop = $('prop').get('value');
      var video = $('config').get('value');
      var videoId = $('videoId').get('value');
      var image = $('img').get('value');

      AjaxEscortListVideo(escortId, video, videoId, image, prop);
    });
  }
};

var EscortListVideo = function (video, image, prop) {
  prop = prop.split('-');
  Play(video, image, prop[0], prop[1], true);

};

var AjaxEscortListVideo = function (escort_id, video, videoId, image, prop) {
  var req = new Request.JSON({
    method: "get",
    url: "/index/ajax-play-video?escort_id=" + escort_id,
    onComplete: function (response) {
      if (response) {
        prop = prop.split("-");
        Play(escort_id, video, videoId, image, prop[0], prop[1], true, response);
      }
    }
  }).send();
};

/**
 * Voting based on localstorage & cookies
 */

var VideoVoting = function (id, opts) {
  return this.init(id, opts);
};

VideoVoting.prototype = {
  el: null,
  opts: {},
  votingKey: 'videoVoting',

  init: function (id, opts) {
    this.el = $(id);
    this.opts = opts;

    this.build();
  },

  build: function () {
    var
      _self = this,
      activeProps = {
        class: 'video-voting-btn active',
        styles: {
          backgroundColor: '#d92b82',
          backgroundImage: 'url("/img/like.png")',
          borderRadius: '6px',
          backgroundRepeat: 'no-repeat',
          backgroundPosition: '95% center',
          backgroundSize: '18px',
          color: '#fff',
          fontSize: '15px',
          padding: '6px 40px 6px 12px',
          textDecoration: 'none',
          marginTop: '10px'
        },
        html: window.buttonText
      },

      inactiveProps = {
        class: 'video-voting-btn voted',
        styles: {
          background: '#d179a5',
          borderRadius: '6px',
          color: '#fff',
          fontSize: '15px',
          padding: '6px 12px',
          textDecoration: 'none',
          marginTop: '10px',
          userSelect: 'none',
          cursor: 'auto'
        },
        html: window.buttonTextVoted,
        'data-vote': 'voted'
      };

    this.votesCountBadge = new Element('span', {
      styles: {
        backgroundColor: '#3d5b87',
        borderRadius: '6px',
        color: '#fff',
        fontSize: '14px',
        padding: '6px',
        textDecoration: 'none',
        margin: '10px 0px 10px 10px',
        backgroundImage: 'url("/img/load.gif")',
        backgroundSize: '40px',
        backgroundPosition: 'center center'
      },
      html: ''
    });

    this.votesCountBadge.inject(this.el.getParent('.contents'), 'after');

    new Fingerprint2().get(function(result) {
      _self.fingerprint = result;
      _self._getVoteCount(function (ln, fps) {
      _self.votingAllowed = fps.indexOf(_self.fingerprint) === -1;

        if (_self.votingAllowed) {
          _self.btn = new Element('button', activeProps);
        } else {
          _self.btn = new Element("button", inactiveProps);
        }

        _self.btn.inject(_self.el.getParent('.contents'), 'after');

        _self.votesCountBadge.setStyle('background-image', 'none');
        _self.votesCountBadge.set('html', ln);
        _self.votesCountBadge.set('data-count', ln);

        _self.bindEvents();
      });
    });


    return this;
  },

  bindEvents: function () {
    'use strict';

    var _self = this;

    if (this.votingAllowed) {
      this.btn.addEvent('click', function () {
        _self._vote(_self.fingerprint);
        _self.btn.removeEvents();
      });
    }
  },

  _vote: function (fp) {
    'use strict';

    var request = new Request({
      url: '/video/vote',
      method: 'POST',
      data: {
        video_id: this.opts.videoId,
        fingerprint: fp
      }
    });

    request.send();

    this.btn.setStyles({
      background: '#d179a5',
      borderRadius: '6px',
      color: '#fff',
      fontSize: '15px',
      padding: '6px 12px',
      textDecoration: 'none',
      marginTop: '10px',
      userSelect: 'none',
      cursor: 'auto'
    });

    this.btn.set('class', 'video-voting-btn voted');
    this.btn.set('html', window.buttonTextVoted);
    this.votesCountBadge.set('html', parseInt(this.votesCountBadge.get('data-count')) + 1);
  },

  _getVoteCount: function (fn) {
    'use strict';

    var request = new Request({
      url: '/video/vote-count',
      method: 'GET',
      data: {
        video_id: this.opts.videoId,
      },

      onSuccess: function (resp) {
        var response = JSON.decode(resp);

        if (response.status === 'error') {
          console.error('Error occured.');
          return false;
        } else {
          fn(response.length, response.fingerprints);
        }
      }
    });

    request.send();
  }
};