Cubix.AgencyComments = {};

Cubix.AgencyComments.url = ''; // Must be set from php
Cubix.AgencyComments.container = 'comments_container';
Cubix.AgencyComments.lng = '';
Cubix.AgencyComments.autoApprove = '';

Cubix.AgencyComments.Load = function (lang, page, agency_id) {
	if ( lang == 'fr' ) lang = false;
	var url = (lang ? '/' + lang : '') + '/agencies/comments?page=' + page + '&agency_id=' + agency_id + '&mode=ajax';
	
	Cubix.AgencyComments.Show(url);
	
	return false;
}

Cubix.AgencyComments.Show = function (url) {
	var overlay = new Cubix.Overlay($(Cubix.AgencyComments.container), {
		loader: _st('loader-small.gif'),
		position: '50%'
	});
	
	overlay.disable();
	
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {			
			$(Cubix.AgencyComments.container).set('html', resp);
			overlay.enable();
		}
	}).send();
}

Cubix.AgencyComments.InitAddForm = function () {
	$$('.add-comment').removeEvents('click');
	
	if ($defined($('comment_added')))
		$('comment_added').setStyle('display', 'none');

	$$('.add-comment').addEvent('click', function (e) {
		e.stop();
		
		var agency_id = this.get('rel');
		
		if ( Cubix.AgencyComments.is_member == 'true' )
		{
			$('add-comment-form').set('html', '');
            $('comment_added').setStyle('display', 'none');

			var params = {};

			new Request({
				url: '/' + Cubix.AgencyComments.lng + '/comments-v2/ajax-add-agency-comment',
				method: 'get',
				evalScripts: true,
				data: $merge({ agency_id: agency_id }, params || {}),
				onSuccess: function (resp) {
					var height = new Element('div', { html: resp }).getHiddenHeight($('add-comment-form'));
					$('add-comment-form').setStyles({ height: 0, overflow: 'hidden' }).set('html', resp).set('tween', { duration: 500, onComplete: function () { this.element.setStyle('height', null) } }).tween('height', height);

					Cubix.AgencyComments.InitFormSubmit(agency_id);
					Cubix.AgencyComments.CloseAddForm();
				}.bind(this)
			}).send();
			
		}
		else {
			Cubix.Popup.Show('489', '652');
		}
	});
};

Cubix.AgencyComments.InitFormSubmit = function (agency_id) {
	$$('.btn-save').addEvent('click', function(e) {
		e.stop();

		var overlay = new Cubix.Overlay($$('.myprofile')[0], {
			loader: _st('loader-small.gif'),
			position: '50%'
		});

		overlay.disable();

		new Request({
			url: '/' + Cubix.AgencyComments.lng + '/comments-v2/ajax-add-agency-comment',
			method: 'post',
			evalScripts: true,
			data: this.getParent('form'),
			onSuccess: function (resp) {
				if ( Cubix.AgencyComments.isJSON(resp) ) {
					if( JSON.decode(resp) ) {
						var data = JSON.decode(resp);

						var error_map = ['comment_to_short', 'comment_to_long', 'captcha_error'];
						error_map.each(function(it){
							$(it).setStyle('display', 'none');
						});

						data.each(function(it){
							$(it).setStyle('display', 'block');
						});
					}
					overlay.enable();
				}
				else {
					if ( $('add-comment-form') ) {
						$('add-comment-form').set('html', '');
					}
					
					if (Cubix.AgencyComments.autoApprove == "0")
						$('comment_added').setStyle('display', 'block');
					else
						Cubix.AgencyComments.Load(Cubix.AgencyComments.lng, 1, agency_id);
				}
			}.bind(this)
		}).send();
	});
};

Cubix.AgencyComments.isJSON = function(str){
	 str = str.replace(/\\./g, '@').replace(/"[^"\\\n\r]*"/g, '');
	 return (/^[,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]*$/).test(str);
}

Cubix.AgencyComments.CloseAddForm = function() {
	$$('.btn-close').addEvent('click', function(e) {
		e.stop();

		$('add-comment-form').set('html', '');
	});
};

var reorderEscortsToGrid = function() {
	
	$$('.ag-box')[0].setStyle('padding', '10px 15px 10px 20px');
	var thumbs = $$('.escorts')[0].getElements('div.h');
	
	var escorts = [];
	var premium_escorts = [];
	var gotd_escort = [];
	
	thumbs.each(function(thumb) {
		if ( thumb.hasClass('prem') ) {
			premium_escorts.append([thumb]);
		} else if ( thumb.hasClass('gotd-thumb') ) {			
			gotd_escort.append([thumb]);
		} else {
			escorts.append([thumb]);
		}
		thumb.destroy();
	});

	escorts = premium_escorts.append(escorts);
	escorts = gotd_escort.append(escorts);
	
	
	$$('.escorts div.r_new').destroy();
	
	$$('.escorts')[0].getLast('div.row').addClass('last');
	
	var rows = $$('.escorts')[0].getElements('div.row');
	var i = 0;
	rows.each(function(row) {		
		for(d = 0; d < 5; d++) {
			if ( typeof escorts[i] != 'undefined' ) {
				escorts[i].inject(row, 'bottom');
				i++;
			}
		}		
	});
	
	/*if ( premium_escorts.length ) {
		var premium_box = $('premium');
		premium_escorts.each(function(esc){
			esc.inject(premium_box, 'bottom');
		});
		new Element('div', {'class':'clear'}).inject(premium_box, 'bottom');
	}*/
	
	if ( $$('.gotd-thumb')[0] ) {
		var img = $$('.gotd-thumb')[0].getElements('img')[0];
		if (Browser.ie){
			var src = img.href;
		} else {
			var src = img.get('src');
		}
		src = src.replace("_gotm_xl", "_gotm");
		img.set('src', src);
		img.set('width', 158);
		img.set('height', 99);
	}
	
	$$('img.escort-thumb').each(function(it) {
		if (Browser.ie){
			src = it.href.replace("_xl_thumb", "_thumb");
		} else {
			src = it.get('src').replace("_xl_thumb", "_thumb");
		}		

		it.set('src', src);
	});
	
	//Cubix.PhotoRoller.thumb = 'thumb';
	//Cubix.PhotoRoller.Clear();
	//Cubix.PhotoRoller.Init();
};

var reorderEscortsToXL = function(from_click) {
	
	if ( ! from_click ) {
		from_click = false;
	}
	
	$$('.ag-box')[0].setStyle('padding', '10px 5px 10px 5px');
	var rows = $$('.escorts div.row');
	var rows_count = $$('.escorts div.row').length;
	
	var thumbs_count = 0;
	rows.each(function(row) {
		thumbs_count = thumbs_count + row.getElements('div.h').length;
	});
	//console.log(thumbs_count);
	var total_row_count = Math.ceil(thumbs_count / 3);
	
	for ( i = 0; i < (total_row_count - rows_count); i++ ) {
		n_row = new Element('div', {'class':'row r_new'});
		n_row.inject($$('.escorts')[0], 'bottom');
	}
	
	var rows = $$('.escorts div.row');
	rows.each(function(it, key) {
		var escorts = rows[key].getElements('div.h');
	
		if ( escorts.length >= 3 ) {
			var k = key + 1;
			console.log('dd');
			escorts.each(function(it, i){
				if ( i >= 3 ) {
					if ( rows[k] ) {
						//it.addClass('e_new');
						rows[k].grab(it, 'top');
						
						if ( k == rows_count - 1 ) {
							rows[k].removeClass('last');
						}
						
						if ( k == total_row_count - 1 ) {
							rows[k].addClass('last');
						}
					}
				}
			});
		}
	});
	
	
	if ( from_click ) {
		if ( $$('.gotd-thumb')[0] ) {
			var img = $$('.gotd-thumb')[0].getElements('img')[0];
			if (Browser.ie){
				var src = img.href;
			} else {
				var src = img.get('src');
			}
			src = src.replace("_gotm", "_gotm_xl");
			img.set('src', src);
			img.set('width', 290);
			img.set('height', 348);
		}
		
		$$('img.escort-thumb').each(function(it){
			if (Browser.ie){
				var src = it.href;
			} else {
				var src = it.get('src');
			}
			
			if ( ! src.test('/_xl_thumb/') ) {
				src = src.replace("_thumb", "_xl_thumb");

				it.set('src', src);
			}
		});
	}
	
	//Cubix.PhotoRoller.thumb = 'xl_thumb';
	//Cubix.PhotoRoller.Clear();
	//Cubix.PhotoRoller.Init();
}

window.addEvent('domready', function () {
	Cubix.AgencyComments.InitAddForm();
	
	///////////////////////////////////////
	if ( $$('.escorts').length ) {
		if ( $$('.escorts')[0].hasClass('xl') ) {
			reorderEscortsToXL(false);
		}
	}
	
	if ( $defined($$('.list_grid_switcher')) ) {
		$$('.list_grid_switcher a').addEvent('click', function(e){
			e.stop();
			
			var cont = $$('.gl_sw')[0];
			
			if ( this.hasClass('grid_btn') ) {
				
				if ( $$('.escorts')[0].hasClass('xl') ) {
					reorderEscortsToGrid();
				}
				
				/*$$('img.escort-thumb').each(function(it){
					var src = it.get('src');
					
					src = src.replace("_xl_thumb", "_thumb");
					
					it.set('src', src);
				});*/
				
				cont.removeClass('list');
				this.removeClass('grid_btn');
				cont.removeClass('xl');
				this.addClass('grid_btn_act');
				
				this.getParent('div.list_grid_switcher').getElements('a.list_btn_act').removeClass('list_btn_act').addClass('list_btn');
				this.getParent('div.list_grid_switcher').getElements('a.xl_btn_act').removeClass('xl_btn_act').addClass('xl_btn');
				
				Cookie.write('list_type', 'grid', {domain: '.and6.com', duration: 30, path: '/'});
			}
			
			if ( this.hasClass('grid_btn_act') ) {
				return;
			}
			
			if ( this.hasClass('list_btn') ) {
				if ( $$('.escorts')[0].hasClass('xl') ) {
					reorderEscortsToGrid();
				}
				
				$$('.escorts')[0].getParent('.i').setStyle('padding', '0 18px 20px 18px');
				/*$$('img.escort-thumb').each(function(it){
					var src = it.get('src');
					
					src = src.replace("_xl_thumb", "_thumb");
					
					it.set('src', src);
				});*/
				cont.removeClass('xl');
				cont.addClass('list');
				this.removeClass('list_btn');
				this.addClass('list_btn_act');
				
				this.getParent('div.list_grid_switcher').getElements('a.grid_btn_act').removeClass('grid_btn_act').addClass('grid_btn');
				this.getParent('div.list_grid_switcher').getElements('a.xl_btn_act').removeClass('xl_btn_act').addClass('xl_btn');
				
				Cookie.write('list_type', 'list', {domain: '.and6.com', duration: 30, path: '/'});
			}
			
			if ( this.hasClass('list_btn_act') ) {
				return;
			}
			
			if ( this.hasClass('xl_btn') ) {
				
				reorderEscortsToXL(true);
				
				/*$$('img.escort-thumb').each(function(it){
					var src = it.get('src');
					
					src = src.replace("_thumb", "_xl_thumb");
					
					it.set('src', src);
				});*/
				
				cont.removeClass('list');
				cont.addClass('xl');
				this.removeClass('xl_btn');
				this.addClass('xl_btn_act');
				
				this.getParent('div.list_grid_switcher').getElements('a.grid_btn_act').removeClass('grid_btn_act').addClass('grid_btn');
				this.getParent('div.list_grid_switcher').getElements('a.list_btn_act').removeClass('list_btn_act').addClass('list_btn');
				
				Cookie.write('list_type', 'xl', {domain: '.and6.com', duration: 30, path: '/'});
			}
			
			if ( this.hasClass('xl_btn_act') ) {
				return;
			}
		});
	}
	/*if ( $defined($$('.list_grid_switcher')) ) {
		$$('.list_grid_switcher a').addEvent('click', function(e){
			e.stop();
			
			var cont = $$('.gl_sw')[0];
			
			
			if ( this.hasClass('grid_btn') ) {
				cont.removeClass('list');
				this.removeClass('grid_btn');
				this.addClass('grid_btn_act');
				
				this.getNext('a').removeClass('list_btn_act');
				this.getNext('a').addClass('list_btn');
				
				Cookie.write('list_type', 'grid', {domain: '.and6.com', duration: 30, path: '/'});
			}
			
			if ( this.hasClass('grid_btn_act') ) {
				return;
			}
			
			if ( this.hasClass('list_btn') ) {
				cont.addClass('list');
				this.removeClass('list_btn');
				this.addClass('list_btn_act');
				
				this.getPrevious('a').removeClass('grid_btn_act');
				this.getPrevious('a').addClass('grid_btn');
				
				Cookie.write('list_type', 'list', {domain: '.and6.com', duration: 30, path: '/'});
			}
			
			if ( this.hasClass('list_btn_act') ) {
				return;
			}
		});
	}*/
	
	$$('.comment_icon').addEvent('mouseenter', function(){
		var self = this;
		var commentCount = self.getNext('.comment_count').get('text');
		tooltip.show(commentCount +' '+ Cubix.CommentTip);

	});

	$$('.comment_icon').addEvent('mouseleave', function(){
		tooltip.hide();
	});

	$$('.review_icon').addEvent('mouseenter', function(){
		var self = this;
		var reviewCount = self.getNext('.review_count').get('text');
		tooltip.show(reviewCount +' '+ Cubix.ReviewTip);

	});

	$$('.review_icon').addEvent('mouseleave', function(){
		tooltip.hide();
	});
	
	$$('.h').addEvent('mouseenter', function() {
		this.removeClass('esc-hover-out');
		this.addClass('esc-hover');			
	});

	$$('.h').addEvent('mouseleave', function() {
		this.removeClass('esc-hover');
		this.addClass('esc-hover-out');
	});
});
