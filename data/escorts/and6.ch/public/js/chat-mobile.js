var MobileChat = {
    Lists: {
        checkMessage: null,
        chat: null,
        windowHeight: window.innerHeight,

        init: function( chat ){
            this.chat = chat;
            this.chatSwitcher();
            this.convChatSwitcher();
            this.tabs();
            this.closeLists();
            //this.searchInConversations();
        },

        activeKeyboard: function(el){
            var self = this,
                textWrap = $$('[class*="mobile_active_chat_"]'),
                textarea;
            
            if( textWrap ){
                textarea = textWrap.getElement('textarea');
                textarea.removeEvents('focus');

                textarea.addEvent("focus", function() {
                    // /var top = (self.windowHeight - window.innerHeight);
                    console.log(window.getPosition().x);
                }, false);
            }
        },

        sortingConversations: function(){
            var self = this,
                elms = $$('#chat-wrapper .active_chat'),
                wrapper = $('chat-container'),
                sort = [], sort_on = [], sort_off = [];

            elms.each(function(el, i){
                self.showLastMessage(el);

                if( el.get('data-status') === 'available' ){
                    sort_on.push(i);
                } else if( el.get('data-status') === 'offline' ){
                    sort_off.push(i);
                }
            });

            sort = sort_on.concat( sort_off );
            for( var j = 0; j < sort.length; j += 1 ){
                elms[ sort[j] ].inject( wrapper );
            }
        },

        showLastMessage: function( el ){
            var elms = el.getElement('.messaging').getElements('.user_message');
            if( !elms.length ) return false;

            var msgs = elms[ elms.length - 1 ],
                msg_block = msgs.getElement('.messages');

            if( msg_block && msg_block.get('html') ){
                if( el.getElement('.tab_name').getElement('>span') ){
                    el.getElement('.tab_name').getElement('>span').destroy();
                }

                el.getElement('.tab_name').set('html', el.getElement('.tab_name').get('html') + '<span class="last-msg">' + msg_block.getElement('p:last-child').get('html') + '</span>');
            }
        },

        chatSwitcher: function(){
            $$('.close-chat-mobile').addEvent('click', function () {
                $$('.open-chat-mobile').removeClass('hidden');
                $('baddy-list').addClass('hidden-m');
                $('chat-wrapper').fireEvent('chat-on');
            });

            $$('.open-chat-mobile').addEvent('click', function () {
                $$('.open-chat-mobile').addClass('hidden');
                $('chat-wrapper').fireEvent('chat-off');
                $('baddy-list').removeClass('hidden-m');
            });
        },

        convChatSwitcher: function(){
            // $$('.close-chat-mobile').addEvent('click', function () {
            //     $$('.open-chat-mobile').removeClass('hidden');
            //     $('chat-wrapper').fireEvent('chat-on');
            // });
            //
            // $$('.open-chat-mobile').addEvent('click', function () {
            //     $$('.open-chat-mobile').addClass('hidden');
            //     $('chat-wrapper').fireEvent('chat-off');
            // });
        },

        tabs: function(){
            var tabs = $$('.chat-tabs span'),
                self = this;

            tabs.addEvent('click', function(){
                tabs.removeClass('act');
                // this.addClass('act');
                $$('.active_chat').each(function(el, i){
                    if( el.getElements('.last-msg').length ){
                        el.removeClass('showen');
                    } else {
                        el.addClass('showen')
                    }
                });

                if( this.get('data-to') === 'chat' ){
                    $$('.to-chat').addClass('act');
                    $$('.active_chat').addClass('hidden');
                    $$('.baddy_listing').removeClass('hidden');
                    $$('.chat-mobile-layout').removeClass('in-conv');
                } else if( this.get('data-to') === 'conv' ){
                    $$('.active_chat .tab_header').removeEvents();

                    $$('.to-conv').addClass('act');
                    $$('.active_chat').removeClass('hidden');
                    $$('.baddy_listing').addClass('hidden');
                    self.renderAvatars();
                    $$('.chat-mobile-layout').addClass('in-conv');
                    // $$('#baddy-list .chat-tabs').setStyle();
                }

                self.openChat();
            });
        },

        closeLists: function(){
            $$('.tab_header .tab_name').addEvent('click', function(){
                $('chat-wrapper').addClass('hide');
            })
        },

        renderAvatars: function(){
            $$('#chat-container .active_chat').each( function(el, i){
                if( !el.getElement('._avatar') ) {
                    var avatar = el.getElement('.tab_header').get('data-avatar');

                    var avatar_el = new Element('span', {
                        'class': '_avatar'
                    });

                    avatar_el.inject(el);
                    avatar_el.setStyle('background', 'url(' + avatar + ') center center / 36px no-repeat');
                }
            });
        },

        openChat: function(){
            var self = this;

            $$('.active_chat').removeEvents();
            $$('.active_chat').addEvent('click', function(e){
                if( this.getParent().get('id') === 'conv-switcher' || this.getParent().get('id') === 'baddy-list' ) return false;

                e.preventDefault();

                self.openChatWindow( this );
            });

            var listener = function (event) {
                /* do something here */
            };

            /*$$('.listing li').each(function(el){
                el.removeEventListener('mouseup', listener, false);
            });*/
            $$('.listing li').removeEvents('mouseup');

            $$('.listing li').addEvent('mouseup', function(e){
                if( this.getParent().get('id') === 'conv-switcher' || this.getParent().get('id') === 'baddy-list' ) return true;

                //e.preventDefault();

                self.openChatWindow( this );
            });
        },

        openChatWindow: function( el ){
            var id, el_id_split, self = this, list_msgs = $('baddy-list'), avatar, status, username, elm_av, tab_name, span_text;
            $$('.active_chat').removeClass('hidden');

            if( el.hasClass('active_chat') ){
                el_id_split = el.get('id').split("_");
                id = el_id_split[2];
                elm_av = el.getElement('.tab_header');
                avatar = elm_av.get('data-avatar');
                status = ( elm_av.hasClass('online') ) ? 'online' : 'offline';
                tab_name = elm_av.getElement('.tab_name').clone();
                if( tab_name.getElement('span') ) tab_name.getElement('span').destroy();
                username = tab_name.get('text');
            } else if( el.classList.contains('bl-escort') || el.classList.contains('bl-member') || el.classList.contains('bl-agency') ){
                el_id_split = el.get('id').split("_");
                id = el_id_split[1];
                avatar = el.getElement('.user_avatar').get('src');
                status = ( el.getElement('.user_status').hasClass('available') ) ? 'online' : 'offline';
                username = el.getElement('.user_name').get('text');
            }

            var n_msgs = $('active_chat_' + id);
            if( n_msgs ){
                if( n_msgs.getElement('.new_messages_count') ){
                    n_msgs.removeClass('new_message');
                    new Fx.Tween(n_msgs.getElement('.new_messages_count'), {
                        duration: 400,
                        property: 'opacity',
                        onComplete: function(){
                            n_msgs.getElement('.new_messages_count').destroy();
                        }
                    }).start(0);
                }
            }

            var n_conv_msgs = $('user_' + id);
            if( n_conv_msgs ){
                if( n_conv_msgs.getElement('.new_messages_count') ){
                    n_conv_msgs.removeClass('new_message');
                    new Fx.Tween(n_conv_msgs.getElement('.new_messages_count'), {
                        duration: 400,
                        property: 'opacity',
                        onComplete: function(){
                            n_conv_msgs.getElement('.new_messages_count').destroy();
                        }
                    }).start(0);
                }
            }

            var thread_id, exists_el = $$('.mobile_active_chat_' + id );

            if( exists_el[0] ){
                thread_id = $$('.active_chat_' + id ).get('data-thread');
                exists_el.addClass('show-message').set('data-thread', thread_id);
            } else {
                //list_msgs.hide();
                list_msgs.getElement('.to-conv').fireEvent('click');
                $$('.chat-mobile-layout.in-conv').removeClass('in-conv');

                var z = { k: 0 };
                var check = null;

                $$('.chat-mobile-layout').addClass('chat-opened');

                var fns = function(){
                    if( z.k === 1 ){
                        clearInterval( check );
                        return false;
                    }

                    var chat_window = $('active_chat_' + id);
                    if( chat_window ){
                        z.k = 1;
                        chat_window = chat_window.getElement('.active_chat_content');
                        chat_window = chat_window.clone();

                        chat_window.addClass( 'mobile_active_chat_' + id );
                        chat_window.inject( $('chat-wrapper') );

                        self.chatPartlyRender( chat_window, { avatar: avatar, username: username, status: status }, id );

                        chat_window = chat_window.addClass('in');

                        setTimeout(function(){
                            chat_window.set('data-thread', thread_id).addClass('show-message');
                            var myFx = new Fx.Scroll(chat_window.getElement('.messaging')).toBottom();
                        }, 100);

                        setTimeout(function(){
                            chat_window.set( 'data-thread', $$('.active_chat_' + id ).get('data-thread') );
                        }, 100);
                    } else {

                    }
                };

                check = setInterval( fns, 30 );

                if( z.k === 1 ) clearInterval( check );
            }

            self.checkMessage = setInterval(checkForNewMessage, 1000);
            function checkForNewMessage(){
                var elm_or_wr_ap = $('active_chat_' + id);
                if( !elm_or_wr_ap ) return false;

                var origin_msg_wrap = elm_or_wr_ap.getElement('.messaging'),
                    mobile_msg_wrap = $$('.mobile_active_chat_' + id).getElement('.messaging'),
                    count_origin_el = origin_msg_wrap.getElements('.user_message').getElement('.messages').getElements('p'),
                    count_mobile_el = mobile_msg_wrap.getElements('.user_message')[0].getElement('.messages').getElements('p'),
                    count_origin = 0, count_mobile = 0;

                count_origin_el.each(function(elms){
                    count_origin += elms.length;
                });

                count_mobile_el.each(function(elms){
                    count_mobile += elms.length;
                });

                if( count_origin > count_mobile ){
                    mobile_msg_wrap.set('html', origin_msg_wrap.get('html'));

                    var myFx = new Fx.Scroll(mobile_msg_wrap[0]).toBottom();
                }
            }
        },

        chatPartlyRender: function( el, data, id ){
            var process = true, self = this, header_el, avatar_el, info_name, info_status, info_options,
                info_options_child, info_options_child_1, info_send, header_back;

            if( !( header_el = el.getElement('._chat_header') ) ){
                header_el = new Element('span', {
                    'class': '_chat_header'
                });
            } else { process = false; }

            if( !( header_back = el.getElement('._chat_back') ) ){
                header_back = new Element('span', {
                    'class': '_chat_back'
                });
            } else { process = false; }

            if( !( avatar_el = el.getElement('._chat_avatar') ) ){
                avatar_el = new Element('span', {
                    'class': '_chat_avatar'
                });
                avatar_el.setStyle('background', 'url("' + data.avatar + '") center center / cover no-repeat');
            } else { process = false; }

            if( !( info_name = el.getElement('._chat_name') ) ){
                info_name = new Element('span', {
                    'class': '_chat_name'
                });
                info_name.set('text', data.username);
            } else { process = false; }

            if( !( info_status = el.getElement('._chat_status') ) ){
                info_status = new Element('span', {
                    'class': '_chat_status'
                });
                info_status.set('text', data.status);
            } else { process = false; }

            if( !( info_options = el.getElement('._chat_options') ) ){
                info_options = new Element('span', {
                    'class': '_chat_options'
                });
            } else { process = false; }

            if( !( info_options_child = el.getElement('._chat_options_child') ) ){
                info_options_child = new Element('span', {
                    'class': '_chat_options_child'
                });
                info_options_child.set('text', 'Block User');
            } else { process = false; }

            if( !( info_options_child_1 = el.getElement('._clear_history') ) ){
                info_options_child_1 = new Element('span', {
                    'class': '_clear_history'
                });
                info_options_child_1.set('text', 'Clear History');
            } else { process = false; }

            if( !( info_send = el.getElement('._chat_send') ) ){
                info_send = new Element('span', {
                    'class': '_chat_send'
                });
                info_send.set('data-id', id);
            } else { process = false; }

            if( process ){
                info_options_child.inject( info_options );
                info_options_child_1.inject( info_options );

                header_back.inject( header_el );
                avatar_el.inject( header_el );
                info_name.inject( header_el );
                info_status.inject( header_el );
                info_options.inject( header_el );

                header_el.inject( el );
                info_send.inject( el.getElement('.textarea_inner') );
            }

            self.addChatEvents( el, id );

            return true;
        },

        addChatEvents: function( el, id ){
            this.activeKeyboard();

            var back = el.getElement('._chat_back'), self = this, origin_el = $('active_chat_' + id);
            back.addEvent('click', function(){
                el.addClass('hide-message');
                $$('.active_chat').removeClass('hidden');
                self.searchInConversations();

                clearInterval( self.checkMessage );
                self.checkMessage = null;

                setTimeout(function(){
                    $$('.chat-mobile-layout').addClass('in-conv');

                    var exist_message = el.getElement('.messaging').get('html');
                    if( exist_message ){
                        $('baddy-list').getElement('.to-conv').fireEvent('click');
                        self.showLastMessage( origin_el );
                    } else {
                        origin_el.addClass('showen');
                    }

                    $$('.active_chat').each(function(el, i){
                        if( el.getElements('.last-msg').length ){
                            el.removeClass('showen');
                        } else {
                            el.addClass('showen')
                        }
                    });

                    if( !$$('.active_chat').length || ( $$('.active_chat').length === $$('.active_chat.showen').length ) ){
                        $('baddy-list').getElement('.to-chat').fireEvent('click');
                    }

                    $$('.chat-mobile-layout').removeClass('chat-opened');
                    el.destroy();
                }, 300);
            });

            var customEmotions = {
                '>:o'	: 'upset',
                'O:)'	: 'angel',
                '3:)'	: 'devil',
                '>:('	: 'grumpy'
            };
            var emotions = {
                ':)'	: 'smile',
                ':('	: 'frown',
                ':P'	: 'tongue',
                ':D'	: 'grin',
                ':*'	: 'kiss',
                ':o'	: 'gasp',
                ';)'	: 'wink',
                ':v'	: 'pacman',
                ':/'	: 'unsure',
                ':\'('	: 'cry',
                '^_^'	: 'kiki',
                '8)'	: 'glasses',
                'B|)'	: 'sunglasses',
                '<3'	: 'heart',
                '-_-'	: 'squint',
                'o.O'	: 'confused',
                ':3'	: 'colonthree'
            };

            /*var emotion_btn = el.getElement('.emotions_btn');
            emotion_btn.addEvent('click', function(){
                var emotion_cont = el.getElement('.emotions_container');

                if( !$(this).hasClass('in') ){
                    if( !emotion_cont ){
                        var emotionsContainer = new Element('div', {
                            'class': 'emotions_container'
                        });

                        for (var i in emotions) {
                            new Element('span', {
                                'class': 'emotions emotions_' + emotions[i],
                                'rel': i
                            }).inject(emotionsContainer);
                        }

                        for (var i in customEmotions) {
                            new Element('span', {
                                'class': 'emotions emotions_' + customEmotions[i],
                                'rel': i
                            }).inject(emotionsContainer);
                        }
                        emotionsContainer.inject(el.getElement('.emotions_wrapper'));

                        var _emotions = emotionsContainer.getElements('.emotions');

                        _emotions.addEvent('click', function(){
                            var _emotion = this.get('rel');

                            var textarea = emotionsContainer.getParent().getParent().getElement('textarea');
                            textarea.set('value', textarea.get('value') + _emotion + ' ')
                        });


                    } else {
                        emotion_cont.show();
                    }
                    $(this).addClass('in');
                } else {
                    el.getElement('.emotions_container').hide();
                    $(this).removeClass('in');
                }
            });*/

            var user_options = el.getElement('._chat_options');
            user_options.addEvent('click', function(e){
                if( e.target !== this ) return false;

                this[ ( !this.hasClass('in') ) ? 'addClass' : 'removeClass' ]('in');
            });

            el.getElement('._chat_send').addEvent('click', function(){
                self.sendMessage(el);
            });

            var _chat_options_child = el.getElement('._chat_options_child');
            _chat_options_child.addEvent('click', function(){
                self.chat.blockUserMobile(id);
                hideOptionsList( el )
            });

            var _chat_options_child_1 = el.getElement('._clear_history');
            _chat_options_child_1.addEvent('click', function(){
                var thread_el = el.getElement('.messaging'),
                    thread_el_orig = origin_el.getElement('.messaging'),
                    thread_id = thread_el.get('data-thread');

                if( thread_id ){
                    self.chat.threadHistory( parseInt( thread_id ) );
                    thread_el.empty();
                    thread_el_orig.empty();
                }
                hideOptionsList( el )
            });

            function hideOptionsList( el ){
                el.getElement('._chat_options').removeClass('in');
            }

            var escort_links = $$('._chat_avatar', '._chat_name'), link;
            escort_links.addEvent('click', function(){
                link = this.getParent('.active_chat_content').getElement('.textarea_wrapper').get('data-url');

                if( link ) window.location.href = link;
            });
        },

        sendMessage: function(el){
            var textarea = el.getElement('textarea'),
                message = textarea.get('value'),
                id = el.getElement('._chat_send').get('data-id');

            textarea.set('value', '').focus()

            if( !message || !id || !this.chat ) return false;

            this.chat.sendMessageMobile( { message: message, id: id } )
        },

        searchInConversations: function(){
            var mob_search = $('baddy_list_search_mobile');
            mob_search.addEvent('focus', function() {
                if ( this.get('value') == 'Search' ) {
                    this.set('value', '');
                }
            });

            mob_search.addEvent('blur', function() {
                if ( this.get('value').length == 0 ) {
                    this.set('value', 'Search');
                }
            });

            mob_search.addEvent('keyup', function() {
                var items = $('chat-wrapper').getElements('.active_chat');

                items.each(function(item) {
                    var id = parseInt( item.get('id').replace('active_chat_', '') );

                    if ( ! item.getElement('.tab_name').get('text').toLowerCase().contains( $('baddy_list_search_mobile').get('value') ) ) {
                        $('active_chat_' + id).setStyle('display', 'none');
                    } else {
                        $('active_chat_' + id).setStyle('display', 'block');
                    }
                });
            });
        },

        unreadMessage: function(id, unreadMessagesCount){
            //console.log( id, unreadMessagesCount );

            if ( typeof unreadMessagesCount === 'undefined' ) unreadMessagesCount = 1;


            var userTab = $('user_' + id), count;
            userTab.addClass('new_message');

            if ( ! userTab.getElement('.new_messages_count') ) {
                new Element('div', {
                    'class' : 'new_messages_count',
                    html : unreadMessagesCount
                }).inject( userTab );
            } else {
                count = parseInt( userTab.getElement('.new_messages_count').get('html') );
                count += unreadMessagesCount;
                userTab.getElement('.new_messages_count').set('html', count);
            }

            var conv_userTab = $('active_chat_' + id);
            conv_userTab.addClass('new_message');

            if ( ! conv_userTab.getElement('.new_messages_count') ) {
                new Element('div', {
                    'class' : 'new_messages_count',
                    html : unreadMessagesCount
                }).inject( conv_userTab );
            } else {
                count = parseInt( conv_userTab.getElement('.new_messages_count').get('html') );
                count += unreadMessagesCount;
                conv_userTab.getElement('.new_messages_count').set('html', count);
            }

            var chat_main_icon = $('chatMainIcon');
            chat_main_icon.addClass('new_message');

            if ( ! chat_main_icon.getElement('.new_messages_count') ) {
                new Element('span', {
                    'class' : 'new_messages_count',
                    html : unreadMessagesCount
                }).inject( chat_main_icon );
            } else {
                count = parseInt( chat_main_icon.getElement('.new_messages_count').get('html') );
                count += unreadMessagesCount;
                chat_main_icon.getElement('.new_messages_count').set('html', count);
            }

            pulseElm();

            function pulseElm(){
                var pf = new PulseFade('chatMainIcon',{
                    min: .50,
                    max: 1,
                    duration: 400,
                    onComplete: function() {
                        //alert('complete!');
                    },
                    onStart: function() {
                        //alert('started!');
                    },
                    onStop: function() {
                        //alert('stopped!');
                    },
                    onTick: function() {
                        //alert('tick!');
                    }
                });

                pf.start();
            }
        }
    }
};