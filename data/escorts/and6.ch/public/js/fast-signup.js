Cubix.Lang.signup = {
	username_invalid: '',
	username_invalid_characters: '',
	username_exists: '',
	password_invalid: '',
	password2_invalid: '',
	email_invalid: '',
	email_exists: '',
	terms_required: '',
	form_errors: '',
	domain_blacklisted: ''
};

Cubix.Validator = {};

Cubix.Validator.Lang = '';

Cubix.Validator.SignUp = function () {
	if ( ! $('signup_username') ) return;

	this.getErrorElement = function (input) {
		var el = input.getNext('div.error');

		if ( ! el ) {
			el = new Element('div', {'class':'error'}).inject(input, 'after');
		}

		return el;
	};

	var self = this;

	// Check username
	$('signup_username').addEvent('blur', function () {
		var lang = Cubix.Lang.signup;

		this.removeClass('invalid');
		this.removeClass('valid');
		
		self.getErrorElement(this).addClass('none');

		if ( this.get('value').length < 6 ) {
			self.getErrorElement(this).set('html', lang.username_invalid)
			
			this.addClass('invalid');
			self.getErrorElement(this).removeClass('none');
			return;
		}

		var regex = /^[-_a-z0-9]+$/i;
		if ( ! regex.test(this.get('value')) ) {
			self.getErrorElement(this).set('html', lang.username_invalid_characters);
			
			this.addClass('invalid');
			self.getErrorElement(this).removeClass('none');
			return;
		}

		this.addClass('loading');

		new Request({
			url: '/' + Cubix.Validator.Lang + '/private/check?username=' + this.get('value'),
			method: 'get',
			onSuccess: function (resp) {
				var resp = JSON.decode(resp);

				this.removeClass('loading');

				if ( resp.status == 'found' ) {
					self.getErrorElement(this).set('html', lang.username_exists);
					
					this.addClass('invalid');
					self.getErrorElement(this).removeClass('none');
				}
				else if ( resp.status == 'not found' ) {
					self.getErrorElement(this).destroy();
					
					this.addClass('valid');
					self.getErrorElement(this).addClass('none');
				}
			}.bind(this)
		}).send();


	});

	// Check password
	$('signup_password').addEvent('blur', function () {
		var lang = Cubix.Lang.signup;

		this.removeClass('invalid');
		this.removeClass('valid');
		self.getErrorElement(this).addClass('none');

		if ( this.get('value').length < 6 ) {
			self.getErrorElement(this).set('html', lang.password_invalid);
			this.addClass('invalid');
			self.getErrorElement(this).removeClass('none');
		}
		else {
			self.getErrorElement(this).destroy();
			
			this.addClass('valid');
			self.getErrorElement(this).addClass('none');
		}
	});

	// Check password confirmation
	$('signup_password2').addEvent('blur', function () {
		var lang = Cubix.Lang.signup;

		$('signup_password').fireEvent('blur');
		if ( $('signup_password').hasClass('invalid') ) return;

		this.removeClass('invalid');
		this.removeClass('valid');
		self.getErrorElement(this).addClass('none');

		if ( this.get('value') != $('signup_password').get('value') ) {
			self.getErrorElement(this).set('html', lang.password2_invalid);
			
			this.addClass('invalid');
			self.getErrorElement(this).removeClass('none');
		}
		else {
			self.getErrorElement(this).destroy();
			
			this.addClass('valid');
			self.getErrorElement(this).addClass('none');
		}
	});
	
	// Check email
	$('signup_email').addEvent('blur', function () {
		var lang = Cubix.Lang.signup;

		this.removeClass('invalid');
		this.removeClass('valid');
		self.getErrorElement(this).addClass('none');

		var regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
		if ( ! this.get('value').length || ! regex.test(this.get('value') ) ) {
			self.getErrorElement(this).set('html', lang.email_invalid)
			
			this.addClass('invalid');
			self.getErrorElement(this).removeClass('none');
			return;
		}

		this.addClass('loading');

		new Request({
			url: '/' + Cubix.Validator.Lang + '/private/check?email=' + this.get('value'),
			method: 'get',
			onSuccess: function (resp) {
				var resp = JSON.decode(resp);

				this.removeClass('loading');

				if ( resp.status == 'found' ) {
					self.getErrorElement(this).set('html', lang.email_exists);
					this.addClass('invalid');
					self.getErrorElement(this).removeClass('none');
				}
				else if ( resp.status == 'not found' ) {
					self.getErrorElement(this).destroy();
					
					this.addClass('valid');
					self.getErrorElement(this).addClass('none');
				}
				else if ( resp.status == 'domain blacklisted' ) {
					self.getErrorElement(this).set('html', lang.domain_blacklisted);
					
					this.addClass('invalid');
					self.getErrorElement(this).removeClass('none');
				}
			}.bind(this)
		}).send();
	});
};

window.addEvent('domready', function () {
	new Cubix.Validator.SignUp();
});
