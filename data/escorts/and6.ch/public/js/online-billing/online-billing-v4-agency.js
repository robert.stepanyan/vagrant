Cubix.OnlineBillingV4 = {};

// Packages
Cubix.OnlineBillingV4.Packages = {

    // defaults

    pageBody: null,
    overlay: null,

    // parametrs

    packagesWrapper: null,
    immediateActivationOpt: null,
    specDateActivationOpt: null,
    specDateActivationFld: null,
    selectedPackage: null,
    selectedDate: null,

    // methods

    init: function() {
      this.setPackagesEls();
      this.eventManager();
    },

    setPackagesEls: function() {
        this.packagesWrapper = $('packagesWrapper');
        this.immediateActivationOpt = $('immediateActivation');
        this.specDateActivationOpt = $('specDateActivation');
        this.specDateActivationFld = $('activationDate');
        this.selectedPackage = $('selectedPackage');
        this.selectedDate = $('selectedDate');

        // body & overlay
        this.pageBody = $(document.body);
        this.overlay = $('calOverlay');
    },

    eventManager: function() {
        var self = this;
        var packageId, packagePrice, packageDuration;

        self.packagesWrapper.getElements('.package-block').each(function(el) {
            el.addEvent('click', function() {
                packageId = el.get('data-id');
                packagePrice = el.get('data-price');
                packageName =  el.get('data-name');
                packageDuration = el.get('data-duration');
				packageCryptoPrice = el.get('data-crypto-price');
				
                self.clearErrors();
                self.setPackage(packageId, packagePrice, packageCryptoPrice, packageDuration, packageName);
                self.setSelectedView(el);
            });
        });

        self.immediateActivationOpt.addEvent('click', function() {
            self.selectedDate.set('value', '');
            self.specDateActivationFld.set('value', '');

            if (!self.immediateActivationOpt.hasClass('checked')) {
                $$('.act-type').each(function(el) {
                    if (el.hasClass('checked')) {
                        el.removeClass('checked');
                    }
                });
            }

            self.immediateActivationOpt.addClass('checked');
        });

        self.specDateActivationOpt.addEvent('click', function() {
            var d = null;

            if (self.selectedDate.get('value')) {
                d = new Date(1970, 0, 1);
                d.setTime(self.selectedDate.get('value'));
                Cubix.OnlineBillingV4.PackageCalendar.packageStartDate.set('value', d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate());
            }

            Cubix.OnlineBillingV4.PackageCalendar.construct(false, false, d);
            Cubix.OnlineBillingV4.PackageCalendar.showCalendar(self.specDateActivationFld);
        });
    },

    setPackage: function(id, price, cryptoPrice, duration, name) {
        this.selectedPackage.set('value', JSON.encode({
            "id": id,
            "price": price,
			"crypto_price": cryptoPrice,
            "duration": duration,
            "name": name
        }));
    },
	
    setSelectedView: function(packageEl) {
        self.packagesWrapper.getElements('.package-block').each(function(el) {
            el.removeClass('selected');
        });

        packageEl.addClass('selected');
    },

    clearErrors: function() {
        var errors = this.packagesWrapper.getElements('.err-msg');

        if (errors && errors.length > 0) {
            errors.each(function(el) {
                el.fade('out').dispose();
            });
        }
    },

    loadOverlay: function() {
      this.overlay.fade('in');
      this.pageBody.addClass('no-scroll');
    },

    unloadOverlay: function() {
      this.overlay.fade('out');
      this.pageBody.removeClass('no-scroll');
    }
};

// Cart
Cubix.OnlineBillingV4.Cart = {
    // defaults
    indepEscortLimit: 1,

    // parameters
    selectedPackage: null,
    selectedEscort: null,
    inCartEscorts: null,
    addToCartBtn: null,
    packagesWrapper: null,
    escortsWrapper: null,
    selectedDate: null,
    activationDate: null,
    cartContent: null,
    curPackageExpirationDate: 0,
    paymentMethodsOverlay: null,

    // methods
    init: function() {
      this.setCartEls();
      this.eventManager();
      this.paymentMethodsOverlay = new Cubix.Overlay($$('.payment-type-block')[0], { has_loader: false });
    },

    setCartEls: function() {
      this.selectedDate = $('selectedDate');
      this.addToCartBtn = $('addToCart');
      this.packagesWrapper = $('packagesWrapper');
      this.escortsWrapper = $('escortsWrapper');
      this.selectedPackage = $('selectedPackage');
      this.selectedEscort = $('selectedEscort');
      this.activationDate = $('activationDate');
      this.cartContent = $('cartContent');
      this.inCartEscorts = $('inCartEscorts');
      if ($$('.package-expiration-date')[0]) {
        this.curPackageExpirationDate = parseInt($$('.package-expiration-date')[0].get('value'));
      }
    },

    eventManager: function() {
      var self = this;
      self.addToCartBtn.removeEvents();
      self.addToCartBtn.addEvent('click', function() {
        if (!self.packageValidator() || !self.escortValidator()) {
          if (!self.packageValidator()) {
            if (self.packagesWrapper.getElements('.err-msg').length == 0) {
              self.createErrTooltip(self.packagesWrapper, Cubix.OnlineBillingV4.Lang.errs.selectPackage);
            } else { 
              self.packagesWrapper.highlight();
            }
          }

          if (!self.escortValidator()) {
            if (self.escortsWrapper.getElements('.err-msg').length == 0) {
                self.createErrTooltip(self.escortsWrapper, Cubix.OnlineBillingV4.Lang.errs.selectEscort);
            } else {
                self.escortsWrapper.highlight();
            }
          }
        } else {
          self.addPackageToCart();
        }
      });
    },

    packageValidator: function() {
        return this.selectedPackage.get('value') ? true : false;
    },

    escortValidator: function() {
        return this.selectedEscort.get('value') ? true : false;
    },

    createErrTooltip: function(el, msg) {
      var x = el.getPosition().x;
      var y = el.getPosition().y;

      var errMsg = new Element('span', {
        'class': 'err-msg',
        'html': msg,
        'styles': {
          'top': 0,
          'left': '50%',
          'transform': 'translate(-50%,-100%)'
        }
      });

      new Fx.Scroll(window).toElementCenter(el);

      el.adopt(errMsg);
      errMsg.fade('in');
      errMsg.setStyle('margin-top', -10);
    },

    addPackageToCart: function() {
      var self = this;
      var packageObj = JSON.decode(this.selectedPackage.get('value'));
      var escortObj = JSON.decode(this.selectedEscort.get('value'));
      var packageDate = this.selectedDate.get('value');
      var secondsCount = packageDate;
      var d;

      if (!packageDate) {
          if (!escortObj.curPackage["0"]) {
              d = new Date();
              packageDate = d.getFullYear() + '-' + ('0' + (parseInt(d.getMonth()) + 1)).slice(-2) + '-' +
                  ('0' + d.getDate()).slice(-2) + ' ' + ('0' + d.getHours()).slice(-2) + ':' +
                  ('0' + d.getMinutes()).slice(-2);
          } else {
              d = new Date(1970, 0, 1);
              d.setTime(escortObj.curPackage["0"].expiration_date * 1000);
              packageDate = d.getFullYear() + '-' + ('0' + (parseInt(d.getMonth()) + 1)).slice(-2) + '-' +
                  ('0' + d.getDate()).slice(-2);
          }
      } else {
          d = new Date(1970, 0, 1);
          d.setTime(packageDate);
          packageDate = d.getFullYear() + '-' + ('0' + (parseInt(d.getMonth()) + 1)).slice(-2) + '-' +
              ('0' + d.getDate()).slice(-2) + ' ' + ('0' + d.getHours()).slice(-2) + ':' +
              ('0' + d.getMinutes()).slice(-2);
          secondsCount = d.getTime();
      }

      if (self.selectedPackage.get('value')) {

        var cart = $('spCartBody');
        var cartRow = new Element('tr', {
            html: '<td colspan="6"></td>',
            class: 'cart-item',
            'data-escort': escortObj.id,
            'data-base-city': escortObj.baseCityId
        }).inject(cart);
        var table = new Element('table', {
            'class': 'sp-package-table'
        });
        var tr1 = new Element('tr', {
            'class': 'sp-package-details'
        });
        var td11 = new Element('td', {
            html: '<span class="sp-delete-symbol">&#215;</span>' +
                '<span>' + packageObj.name + '</span>' +
                '</span><input type="hidden" class="package-duration" value="' + packageObj.duration + '">' +
				'<input type="hidden" class="package-price" value="' + packageObj.price + '">' +
				'<input type="hidden" class="package-crypto-price" value="' + packageObj.crypto_price + '">' +
                '<input type="hidden" class="package-id" value="' + packageObj.id + '">'
        });
        var td12 = new Element('td', {
            html: '<span><span style="color: #d22475"> #' + escortObj.id + '</span>: ' +
                  escortObj.showname + '</span><input type="hidden" class="escort-id" value="' +
                  escortObj.id + '">'
        });
        var td13 = new Element('td', {
            html: '<span>' + packageDate + ' </span>' + 
            (escortObj.curPackage["0"] ? '' : ('<span class="sp-edit">' + Cubix.OnlineBillingV4.Lang.edit + '</span>')) +
                '<input class="package-activation-date" type="hidden" value="' + secondsCount + '">'
        });
        var td14 = new Element('td', {
            html: '<span class="sp-add-gotd">'+ Cubix.OnlineBillingV4.Lang.add + '</span>' +
                '<input class="package-gotd-object" type="hidden">'
        });
        if(escortObj.hasApprovedVideo == 1){
          var td15 = new Element('td', {
              html: '<span class="sp-add-votd">'+ Cubix.OnlineBillingV4.Lang.add + '</span>' +
                  '<input class="package-votd-object" type="hidden">'
          });
        } else{
          var td15 = new Element('td', {
            html: '&nbsp; N/A' +
                          '<input class="package-votd-object" type="hidden">'
          });
        }
        var td16 = new Element('td', {
            html: ((packageObj.id == 24 || packageObj.id == 25) ? '<span class="sp-add-area">' +
                  Cubix.OnlineBillingV4.Lang.add + '</span>' : '&nbsp; N/A') +
                '<input class="area-object" type="hidden">'
        });
        var td17 = new Element('td', {
            html: '<span>' + Cubix.OnlineBillingV4.Payments.generatePrices(packageObj.price, packageObj.crypto_price) + ' CHF</span> ='
        });
        var trs = new Elements([td11, td12, td13, td14, td15, td16, td17]).inject(tr1);
        var tr2 = new Element('tr', {
            'class': 'sp-package-result'
        });
        var td21 = new Element('td', {
            html: Cubix.OnlineBillingV4.Payments.generatePrices(packageObj.price, packageObj.crypto_price) + ' CHF'
        });
        var td22 = new Element('td');
        var td23 = new Element('td');
        var td24 = new Element('td');
        var td25 = new Element('td');
        var td26 = new Element('td');
        var td27 = new Element('td', {
            html: Cubix.OnlineBillingV4.Payments.generatePrices(packageObj.price, packageObj.crypto_price) + ' CHF',
            class: 'table-total'
        });
		
        $$('.sp-gt-desc')[0].set('html', Cubix.OnlineBillingV4.Payments.generatePrices(packageObj.price, packageObj.crypto_price) + ' CHF');

        trs = new Elements([td21, td22, td23, td24, td25, td26, td27]).inject(tr2);
        trs = new Elements([tr1, tr2]).inject(table);
        cart.getLast('tr').getLast('td').adopt(table);
        table.highlight('#f0f3f9');

        // deactivating escort for future use
        var selectedEscortItm = $$('.escort-list .escort-item.selected')[0].removeClass('selected');
        selectedEscortItm.removeEvents();
        selectedEscortItm.addClass('in-cart');

        // adding escorts to cart for future disabling
        var inCartEscortsArr = JSON.decode(this.inCartEscorts.get('value')) || [];
        inCartEscortsArr.push(this.selectedEscort.get('value'));
        this.inCartEscorts.set('value', JSON.encode(inCartEscortsArr));

        $('spCart').fireEvent('cartChange');


        $$('.cart-item').getElements('.sp-delete-symbol').each(function(el) {
            el.addEvent('click', function() {
                var agencyEscortId = this.getParent('.cart-item').get('data-escort');
                var selectedEscortItm = $$('.escort-list .escort-item');

                selectedEscortItm.each(function(el) {
                    if (el.get('data-id') == agencyEscortId) {
                        el.removeClass('in-cart');

                        selectedEscortItm.removeClass('selected');

                        el.addClass('selected');

                        el.addEvent('click', function() {
                            Cubix.OnlineBillingV4.Agency.selectItm(this);
                        });

                        el.fireEvent('click');
                    }
                });

                var inCartArr = JSON.decode(self.inCartEscorts.get('value'));

                inCartArr.each(function(el) {
                    if (JSON.decode(el).id == agencyEscortId) {
                        self.inCartEscorts.set('value', JSON.encode(inCartArr.erase(el)));
                    }
                });

                var xCurGotds = this.getParent('.cart-item').getElements('.activated-gotd-days').getElements('.sp-delete-symbol');
                xCurGotds.each(function(el) {
                    el[0].fireEvent('click');
                });
                this.getParent('.cart-item').destroy();
                if ($$('.cart-item').length < 1) {
                  Cubix.OnlineBillingV4.Cart.paymentMethodsOverlay.disable();
                }


                $('spCart').fireEvent('cartChange');
                self.updateCartPrice();
            });
        });


        $$('.cart-item').getElements('.sp-add-gotd').each(function(el) {
            el.removeEvents();

            el.addEvent('click', function() {
                var packageDuration = this.getParent('table').getElements('tr')[0].getElements('td')[0].getElement('.package-duration').get('value');
                var packageStartDate = this.getParent('table').getElements('tr')[0].getElements('td')[2].getElement('.package-activation-date').get('value');

                packageStartDate = packageStartDate == '' ? (new Date()).getTime() : parseInt(packageStartDate);

                
                var packageEndDate = parseInt(packageStartDate) + parseInt(packageDuration * 24 * 60 * 60 * 1000);
                

                if (self.curPackageExpirationDate) {
                    packageDate = d.getFullYear() + '-' + ('0' + (parseInt(d.getMonth()) + 1)).slice(-2) + '-' +
                        ('0' + d.getDate()).slice(-2) + ' ' + ('0' + d.getHours()).slice(-2) + ':' +
                        ('0' + d.getMinutes()).slice(-2);
                }

                Cubix.OnlineBillingV4.GotdCalendar.gotdAreasEl.getElements('option')[0].set('selected', 'selected');
                Cubix.OnlineBillingV4.GotdCalendar.gotdSelectedArea = 0;

                if (!packageStartDate && self.curPackageExpirationDate && (this.getParent('.cart-item'))) {
                    packageStartDate = self.curPackageExpirationDate;
                    packageEndDate = parseInt(packageStartDate) + parseInt(packageDuration * 24 * 60 * 60 * 1000);
                }

                Cubix.OnlineBillingV4.GotdCalendar.setDateRange([packageStartDate, packageEndDate]);
                Cubix.OnlineBillingV4.GotdCalendar.gotdObjEl = {
                    gotdViewEl: this.getParent('td'),
                    gotdModelEl: this.getSiblings('.package-gotd-object')[0]
                };
                Cubix.OnlineBillingV4.GotdCalendar.construct();
                Cubix.OnlineBillingV4.GotdCalendar.showCalendar(this, false);

                $('spCart').fireEvent('cartChange');
            });
        });

        $$('.cart-item').getElements('.sp-add-votd').each(function(el) {
            el.removeEvents();

            el.addEvent('click', function() {
                var packageDuration = this.getParent('table').getElements('tr')[0].getElements('td')[0].getElement('.package-duration').get('value');
                var packageStartDate = this.getParent('table').getElements('tr')[0].getElements('td')[2].getElement('.package-activation-date').get('value');

                packageStartDate = packageStartDate == '' ? (new Date()).getTime() : parseInt(packageStartDate);

                var packageEndDate = parseInt(packageStartDate) + parseInt(packageDuration * 24 * 60 * 60 * 1000);

                if (self.curPackageExpirationDate) {
                    packageDate = d.getFullYear() + '-' + ('0' + (parseInt(d.getMonth()) + 1)).slice(-2) + '-' +
                        ('0' + d.getDate()).slice(-2) + ' ' + ('0' + d.getHours()).slice(-2) + ':' +
                        ('0' + d.getMinutes()).slice(-2);
                }

                Cubix.OnlineBillingV4.VotdCalendar.votdAreasEl.getElements('option')[0].set('selected', 'selected');
                Cubix.OnlineBillingV4.VotdCalendar.votdSelectedArea = 0;

                if (!packageStartDate && self.curPackageExpirationDate && (this.getParent('.cart-item'))) {
                    packageStartDate = self.curPackageExpirationDate;
                    packageEndDate = parseInt(packageStartDate) + parseInt(packageDuration * 24 * 60 * 60 * 1000);
                }

                Cubix.OnlineBillingV4.VotdCalendar.setDateRange([packageStartDate, packageEndDate]);
                Cubix.OnlineBillingV4.VotdCalendar.votdObjEl = {
                    votdViewEl: this.getParent('td'),
                    votdModelEl: this.getSiblings('.package-votd-object')[0]
                };
                Cubix.OnlineBillingV4.VotdCalendar.construct();
                Cubix.OnlineBillingV4.VotdCalendar.showCalendar(this, false);

                $('spCart').fireEvent('cartChange');
            });
        });


        $$('.sp-add-area').each(function(el) {
            el.removeEvents();

            el.addEvent('click', function() {
                Cubix.OnlineBillingV4.AreaSelector.areaViewEl = el.getParent('td');
                Cubix.OnlineBillingV4.AreaSelector.areaModelEl = el.getSiblings('.area-object')[0];
                Cubix.OnlineBillingV4.AreaSelector.escortBaseCityId = el.getParent('tr.cart-item').get('data-base-city');
                Cubix.OnlineBillingV4.AreaSelector.construct();
                Cubix.OnlineBillingV4.AreaSelector.showAreaSelector(this, false);

                $('spCart').fireEvent('cartChange');
            })
        });


        $$('.cart-item').getElements('.sp-edit').each(function(el) {
            el.removeEvents();

            el.addEvent('click', function() {
                if (this.getSiblings('input')[0].get('value')) {
                    var d = new Date(1970, 0, 1);
                    d.setTime(this.getSiblings('input')[0].get('value'));
                    Cubix.OnlineBillingV4.PackageCalendar.packageStartDate.set('value', d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate() + ' ' + d.getHours() + ':' + d.getMinutes());
                }

                Cubix.OnlineBillingV4.PackageCalendar.construct(false, false, this.getSiblings('input')[0].get('value'), {
                    view: this.getSiblings('span')[0],
                    model: this.getSiblings('input')[0]
                });
                var xCurGotds = this.getParent('.cart-item').getElements('.activated-gotd-days').getElements('.sp-delete-symbol');

                xCurGotds.each(function(el) {
                    el[0].fireEvent('click');
                });
                Cubix.OnlineBillingV4.PackageCalendar.showCalendar(this, this.getSiblings('input')[0]);

            })
        });

        $('selectedEscort').set('value', '');
      } else {
        if ($('packagesWrapper').getElements('.err-msg').length < 1) {
            self.createErrTooltip($('packagesWrapper'), Cubix.OnlineBillingV4.Lang.errs.selectPackage);
        } else {
            $('packagesWrapper').highlight();
        }
      }
      this.updateCart(packageObj, packageDate);
      this.updateCartPrice();
      this.paymentMethodsOverlay.enable();
      $('spCart').fireEvent('cartChange');
    },

    addGotdToCart: function(el) {
      var self = this;
      var packageObj = JSON.decode(decodeURIComponent(el.get('value')))[0];
      var escortId = el.getParent('.escort-item').get('data-id');
      var escortShowname = el.getParent('.escort-item').getElement('.showname').get('html');
      var packageDate = packageObj['date_activated'];
      var secondsCount = packageDate;


      var d = new Date(1970, 0, 1);
      d.setTime(packageDate);
      packageDate = d.getFullYear() + '-' + ('0' + (parseInt(d.getMonth()) + 1)).slice(-2) + '-' +
        ('0' + d.getDate()).slice(-2) + ' ' + ('0' + d.getHours()).slice(-2) + ':' +
        ('0' + d.getMinutes()).slice(-2);
      secondsCount = d.getTime();

      var cart = $('spCartBody');
      var cartRow = new Element('tr', {
        html: '<td colspan="6"></td>',
        class: 'cart-item gotd-item',
        'data-escort': escortId
      }).inject(cart);
      var table = new Element('table', {
        'class': 'sp-package-table'
      });
      var tr1 = new Element('tr', {
        'class': 'sp-package-details'
      });
      var td11 = new Element('td', {
        html: '<span class="sp-delete-symbol">&#215;</span>' +
            '<span>Gotd</span>' +
            '<input type="hidden" class="package-duration" value="' + packageObj['period'] + '">' +
            '<input type="hidden" class="package-price" value="0">' +
			'<input type="hidden" class="package-crypto-price" value="0">' +
            '<input type="hidden" class="package-id" value="">'
      });
      var td12 = new Element('td', {
        html: '<span><span style="color: #d22475"> #' + escortId + '</span>: ' + escortShowname + '</span>' +
          '<input type="hidden" class="escort-id" value="' + escortId + '">'

      });

      var td13 = new Element('td', {
        html: '<span>' + packageObj['date_activated'] + '</span>' +
              '<input class="package-activation-date" type="hidden" value="">'
      });
      var td14 = new Element('td', {
        html: '<span class="sp-add-gotd">'+ Cubix.OnlineBillingV4.Lang.add + '</span>' +
              '<input class="package-gotd-object" type="hidden">'
      });
      var td15 = new Element('td', {
        html: '&nbsp; N/A'
      });
      var td16 = new Element('td', {
        html: ((packageObj.id == 24 || packageObj.id == 25) ?
              '<span class="sp-add-area">'+ Cubix.OnlineBillingV4.Lang.add + '</span>' :
              '&nbsp; N/A') +
              '<input class="area-object" type="hidden">'
      });
      var td17 = new Element('td', {
        html: '<span></span>'
      });
      var trs = new Elements([td11, td12, td13, td14, td15, td16, td17]).inject(tr1);
      var tr2 = new Element('tr', {
        'class': 'sp-package-result'
      });
      var td21 = new Element('td', {
        html: 0 + ' CHF'
      });
      var td22 = new Element('td');
      var td23 = new Element('td');
      var td24 = new Element('td');
      var td25 = new Element('td');
      var td26 = new Element('td');
      var td27 = new Element('td', {
        html: 0 + ' CHF',
        class: 'table-total'
      });

      $$('.sp-gt-desc')[0].set('html', 0 + ' CHF');

      trs = new Elements([td21, td22, td23, td24, td25, td26, td27]).inject(tr2);
      trs = new Elements([tr1, tr2]).inject(table);
      cart.getLast('tr').getLast('td').adopt(table);
      table.highlight('#f0f3f9');

      // // adding escorts to cart for future disabling

      $$('.cart-item').getElements('.sp-delete-symbol').each(function(el) {
        el.addEvent('click', function() {
          var agencyEscortId = this.getParent('.cart-item').get('data-escort');
          var selectedEscortItm = $$('.escort-list .escort-item');

          selectedEscortItm.each(function(el) {
            if (el.get('data-id') == agencyEscortId) {
              el.getElement('.add-gotd-btn').removeAttribute('style');
              el.getElement('.add-gotd-btn').addEvent('click', function(e) {
                  e.stop();
                  Cubix.OnlineBillingV4.Agency.selectGotd(this);
              });
              
            }
          });

          var xCurGotds = this.getParent('.cart-item')
            .getElements('.activated-gotd-days')
            .getElements('.sp-delete-symbol');

          xCurGotds.each(function(el) {
            el[0].fireEvent('click');
          });

          this.getParent('.cart-item').destroy();
          $('spCart').fireEvent('cartChange');
          if ($$('.cart-item').length < 1) {
            self.paymentMethodsOverlay.disable();
          }

        })
      });

      $$('.cart-item').getElements('.sp-add-gotd').each(function(el) {
          el.removeEvents();

          el.addEvent('click', function() {
              var packageDuration = this.getParent('table')
                .getElements('tr')[0]
                .getElements('td')[0]
                .getElement('.package-duration')
                .get('value');

              var packageStartDate = this.getParent('table')
                .getElements('tr')[0]
                .getElements('td')[2]
                .getElement('.package-activation-date')
                .get('value');

              packageStartDate = packageStartDate == '' ? (new Date()).getTime() : parseInt(packageStartDate);

              
              var packageEndDate = parseInt(packageStartDate) + parseInt(packageDuration * 24 * 60 * 60 * 1000);
              

              if (self.curPackageExpirationDate) {
                packageDate = d.getFullYear() + '-' + ('0' + (parseInt(d.getMonth()) + 1)).slice(-2) + '-' +
                    ('0' + d.getDate()).slice(-2) + ' ' + ('0' + d.getHours()).slice(-2) + ':' +
                    ('0' + d.getMinutes()).slice(-2);
              }

              Cubix.OnlineBillingV4.GotdCalendar.gotdAreasEl.getElements('option')[0].set('selected', 'selected');
              Cubix.OnlineBillingV4.GotdCalendar.gotdSelectedArea = 0;

              if (!packageStartDate && self.curPackageExpirationDate && (this.getParent('.cart-item'))) {
                packageStartDate = self.curPackageExpirationDate;
                packageEndDate = parseInt(packageStartDate) + parseInt(packageDuration * 24 * 60 * 60 * 1000);
              }

              Cubix.OnlineBillingV4.GotdCalendar.setDateRange([packageStartDate, packageEndDate]);
              Cubix.OnlineBillingV4.GotdCalendar.gotdObjEl = {
                gotdViewEl: this.getParent('td'),
                gotdModelEl: this.getSiblings('.package-gotd-object')[0]
              };
              Cubix.OnlineBillingV4.GotdCalendar.construct();
              Cubix.OnlineBillingV4.GotdCalendar.showCalendar(this, false);

              $('spCart').fireEvent('cartChange');
          });
      });

      Cubix.OnlineBillingV4.GotdCalendar.gotdAreasEl.getElements('option')[0].set('selected', 'selected');
      Cubix.OnlineBillingV4.GotdCalendar.gotdSelectedArea = 0;

      if (!packageStartDate && self.curPackageExpirationDate && (this.getParent('.cart-item'))) {
          packageStartDate = self.curPackageExpirationDate;
          packageEndDate = parseInt(packageStartDate) + parseInt(packageDuration * 24 * 60 * 60 * 1000);
      }

      Cubix.OnlineBillingV4.GotdCalendar.setDateRange([(new Date).getTime(), packageObj.expiration_date * 1000]);
      Cubix.OnlineBillingV4.GotdCalendar.gotdObjEl = {
          gotdViewEl: cart.getLast('tr').getElement('.sp-add-gotd').getParent('td'),
          gotdModelEl: cart.getLast('tr').getElement('.sp-add-gotd').getParent('td').getElement('.package-gotd-object')
      };

      Cubix.OnlineBillingV4.GotdCalendar.construct();
      Cubix.OnlineBillingV4.GotdCalendar.showCalendar(cart.getLast('tr').getLast('td').getElement('table'), false);

      $('spCart').fireEvent('cartChange');

    },

    addVotdToCart: function(el) {
      var self = this;
      var packageObj = JSON.decode(decodeURIComponent(el.get('value')))[0];
      var escortId = el.getParent('.escort-item').get('data-id');
      var escortShowname = el.getParent('.escort-item').getElement('.showname').get('html');
      var packageDate = packageObj['date_activated'];
      var secondsCount = packageDate;


      var d = new Date(1970, 0, 1);
      d.setTime(packageDate);
      packageDate = d.getFullYear() + '-' + ('0' + (parseInt(d.getMonth()) + 1)).slice(-2) + '-' +
        ('0' + d.getDate()).slice(-2) + ' ' + ('0' + d.getHours()).slice(-2) + ':' +
        ('0' + d.getMinutes()).slice(-2);
      secondsCount = d.getTime();

      var cart = $('spCartBody');
      var cartRow = new Element('tr', {
        html: '<td colspan="6"></td>',
        class: 'cart-item votd-item',
        'data-escort': escortId
      }).inject(cart);
      var table = new Element('table', {
        'class': 'sp-package-table'
      });
      var tr1 = new Element('tr', {
        'class': 'sp-package-details'
      });
      var td11 = new Element('td', {
        html: '<span class="sp-delete-symbol">&#215;</span>' +
            '<span>Votd</span>' +
            '<input type="hidden" class="package-duration" value="' + packageObj['period'] + '">' +
            '<input type="hidden" class="package-price" value="0">' +
			'<input type="hidden" class="package-crypto-price" value="0">' +
            '<input type="hidden" class="package-id" value="">'
      });
      var td12 = new Element('td', {
        html: '<span><span style="color: #d22475"> #' + escortId + '</span>: ' + escortShowname + '</span>' +
          '<input type="hidden" class="escort-id" value="' + escortId + '">'

      });

      var td13 = new Element('td', {
        html: '<span>' + packageObj['date_activated'] + '</span>' +
              '<input class="package-activation-date" type="hidden" value="">'
      });
      var td14 = new Element('td', {
        html: '&nbsp; N/A'
      });
      var td15 = new Element('td', {
        html: '<span class="sp-add-votd">'+ Cubix.OnlineBillingV4.Lang.add + '</span>' +
              '<input class="package-votd-object" type="hidden">'
      });
      
      var td16 = new Element('td', {
        html: ((packageObj.id == 24 || packageObj.id == 25) ?
              '<span class="sp-add-area">'+ Cubix.OnlineBillingV4.Lang.add + '</span>' :
              '&nbsp; N/A') +
              '<input class="area-object" type="hidden">'
      });
      var td17 = new Element('td', {
        html: '<span></span>'
      });
      var trs = new Elements([td11, td12, td13, td14, td15, td16, td17]).inject(tr1);
      var tr2 = new Element('tr', {
        'class': 'sp-package-result'
      });
      var td21 = new Element('td', {
        html: 0 + ' CHF'
      });
      var td22 = new Element('td');
      var td23 = new Element('td');
      var td24 = new Element('td');
      var td25 = new Element('td');
      var td26 = new Element('td');
      var td27 = new Element('td', {
        html: 0 + ' CHF',
        class: 'table-total'
      });

      $$('.sp-gt-desc')[0].set('html', 0 + ' CHF');

      trs = new Elements([td21, td22, td23, td24, td25, td26, td27]).inject(tr2);
      trs = new Elements([tr1, tr2]).inject(table);
      cart.getLast('tr').getLast('td').adopt(table);
      table.highlight('#f0f3f9');

      // // adding escorts to cart for future disabling

      $$('.cart-item').getElements('.sp-delete-symbol').each(function(el) {
        el.addEvent('click', function() {
          var agencyEscortId = this.getParent('.cart-item').get('data-escort');
          var selectedEscortItm = $$('.escort-list .escort-item');

          selectedEscortItm.each(function(el) {
            if (el.get('data-id') == agencyEscortId) {
              el.getElement('.add-votd-btn').removeAttribute('style');
              el.getElement('.add-votd-btn').addEvent('click', function(e) {
                  e.stop();
                  Cubix.OnlineBillingV4.Agency.selectVotd(this);
              });
            }
          });

          var xCurVotds = this.getParent('.cart-item')
            .getElements('.activated-votd-days')
            .getElements('.sp-delete-symbol');

          xCurVotds.each(function(el) {
            el[0].fireEvent('click');
          });

          this.getParent('.cart-item').destroy();
          $('spCart').fireEvent('cartChange');
          if ($$('.cart-item').length < 1) {
            self.paymentMethodsOverlay.disable();
          }

        })
      });

      $$('.cart-item').getElements('.sp-add-votd').each(function(el) {
          el.removeEvents();

          el.addEvent('click', function() {
              var packageDuration = this.getParent('table')
                .getElements('tr')[0]
                .getElements('td')[0]
                .getElement('.package-duration')
                .get('value');

              var packageStartDate = this.getParent('table')
                .getElements('tr')[0]
                .getElements('td')[2]
                .getElement('.package-activation-date')
                .get('value');

              packageStartDate = packageStartDate == '' ? (new Date()).getTime() : parseInt(packageStartDate);

              
              var packageEndDate = parseInt(packageStartDate) + parseInt(packageDuration * 24 * 60 * 60 * 1000);

              if (self.curPackageExpirationDate) {
                packageDate = d.getFullYear() + '-' + ('0' + (parseInt(d.getMonth()) + 1)).slice(-2) + '-' +
                    ('0' + d.getDate()).slice(-2) + ' ' + ('0' + d.getHours()).slice(-2) + ':' +
                    ('0' + d.getMinutes()).slice(-2);
              }

              Cubix.OnlineBillingV4.VotdCalendar.votdAreasEl.getElements('option')[0].set('selected', 'selected');
              Cubix.OnlineBillingV4.VotdCalendar.votdSelectedArea = 0;

              if (!packageStartDate && self.curPackageExpirationDate && (this.getParent('.cart-item'))) {
                packageStartDate = self.curPackageExpirationDate;
                packageEndDate = parseInt(packageStartDate) + parseInt(packageDuration * 24 * 60 * 60 * 1000);
              }

              Cubix.OnlineBillingV4.VotdCalendar.setDateRange([packageStartDate, packageEndDate]);
              Cubix.OnlineBillingV4.VotdCalendar.gotdObjEl = {
                votdViewEl: this.getParent('td'),
                votdModelEl: this.getSiblings('.package-votd-object')[0]
              };
              Cubix.OnlineBillingV4.VotdCalendar.construct();
              Cubix.OnlineBillingV4.VotdCalendar.showCalendar(this, false);

              $('spCart').fireEvent('cartChange');
          });
      });

      Cubix.OnlineBillingV4.VotdCalendar.votdAreasEl.getElements('option')[0].set('selected', 'selected');
      Cubix.OnlineBillingV4.VotdCalendar.votdSelectedArea = 0;

      if (!packageStartDate && self.curPackageExpirationDate && (this.getParent('.cart-item'))) {
          packageStartDate = self.curPackageExpirationDate;
          packageEndDate = parseInt(packageStartDate) + parseInt(packageDuration * 24 * 60 * 60 * 1000);
      }

      Cubix.OnlineBillingV4.VotdCalendar.setDateRange([(new Date).getTime(), packageObj.expiration_date * 1000]);
      Cubix.OnlineBillingV4.VotdCalendar.votdObjEl = {
          votdViewEl: cart.getLast('tr').getElement('.sp-add-votd').getParent('td'),
          votdModelEl: cart.getLast('tr').getElement('.sp-add-votd').getParent('td').getElement('.package-votd-object')
      };

      Cubix.OnlineBillingV4.VotdCalendar.construct();
      Cubix.OnlineBillingV4.VotdCalendar.showCalendar(cart.getLast('tr').getLast('td').getElement('table'), false);

      $('spCart').fireEvent('cartChange');

    },

    updateCart: function(p, d) {
        this.cartContent.set('value', JSON.encode([{
            id: p.id,
            price: p.price,
            duration: p.duration,
            activation: d
        }]));
    },

    updateCartPrice: function() {
        // getting data from the fields
        var tableEls = $$('.sp-package-table');
        var detailsEls = tableEls.getElements('.sp-package-details');
        var summaryEls = tableEls.getElements('.sp-package-result');

        for (var i = tableEls.length - 1; i >= 1; i--) {
            var pPrice = parseInt(detailsEls[i].getElements('.package-price')[0].get('value'));
            var pGotdCount = detailsEls[i].getElements('.activated-gotd-days')[0].length;
            var pGotdPrice = parseInt($('gotdPrice').get('value'));
            var pVotdCount = detailsEls[i].getElements('.activated-votd-days')[0].length;
            var pVotdPrice = parseInt($('votdPrice').get('value'));
            var pAreasCount = detailsEls[i].getElements('.selected-arr-areas')[0].length;
            var pAreasPrice = parseInt($('addAreaPrice').get('value'));

			var pCryptoPrice = parseInt(detailsEls[i].getElements('.package-crypto-price')[0].get('value'));
			var pCryptoGotdPrice = parseInt($('gotdCryptoPrice').get('value'));
			var pCryptoVotdPrice = parseInt($('votdCryptoPrice').get('value'));
            // updating GOTD & Area
            if (pGotdCount > 0) {
				var pGotdPricesText = Cubix.OnlineBillingV4.Payments.generatePrices(pGotdPrice, pCryptoGotdPrice);
                summaryEls[i][0].getElements('td')[3].set('html', '+' + pGotdCount + 'x' + pGotdPricesText + ' CHF');
            } 

            if (pVotdCount > 0) {
				var pVotdPricesText = Cubix.OnlineBillingV4.Payments.generatePrices(pVotdPrice, pCryptoVotdPrice);
                summaryEls[i][0].getElements('td')[4].set('html', '+' + pVotdCount + 'x' + pVotdPricesText + ' CHF');
            }


            if (pAreasCount > 0) {
                summaryEls[i][0].getElements('td')[5].set('html', '+' + pAreasCount + 'x' + pAreasPrice + ' CHF');
            } else {
                summaryEls[i][0].getElements('td')[5].set('html', '');
            }

            var pSumTxt = summaryEls[i][0].getElements('td')[0].get('html');
            var pGotdTxt = summaryEls[i][0].getElements('td')[3].get('html');
            var pAreaTxt = summaryEls[i][0].getElements('td')[4].get('html');

            // updating overall price
            detailsEls[i][0].getElements('td')[6].set('html', pSumTxt + (pGotdTxt ? ('<br>' + pGotdTxt) : '') + (pAreaTxt ? ('<br>' + pAreaTxt) : '') + ' =');
			var totalPrice = pPrice + pGotdCount * pGotdPrice  + pVotdCount * pVotdPrice + pAreasCount * pAreasPrice;
			var totalCryptoPrice = pCryptoPrice + pGotdCount * pCryptoGotdPrice  + pVotdCount * pCryptoVotdPrice + pAreasCount * pAreasPrice;
			console.log(pCryptoPrice, pCryptoGotdPrice, pCryptoVotdPrice);
            summaryEls[i][0].getElements('td')[6].set('html', Cubix.OnlineBillingV4.Payments.generatePrices(totalPrice, totalCryptoPrice) + ' CHF');
        }

        var total = totalCrypto = 0;
        var curTotal = $$('.table-total');
        for (var i = curTotal.length - 1; i >= 0; i--) {
			total = total + parseInt(curTotal[i].getElement('.chf-price').get('html'));
			totalCrypto = totalCrypto + parseInt(curTotal[i].getElement('.crypto-price').get('html'));
        }

        $$('.sp-gt-desc')[0].set('html', Cubix.OnlineBillingV4.Payments.generatePrices(total, totalCrypto) + ' CHF');
    }
};

// Package Calendar
Cubix.OnlineBillingV4.PackageCalendar = {
    // defaults
    dateRange: [],
    curPackageExpirationDate: 0,
    packageCalendar: null,
    calendarCloseBtn: null,
    calendarBackBtn: null,
    calendarForwardBtn: null,
    monthYear: null,
    packageCalDays: null,
    daysCount: 0,

    hourField: null,
    minutesField: null,

    packageStartDate: null,
    packageStartHour: null,
    packageStartMinute: null,

    addBtn: null,

    init: function() {
      this.setDateRange([1461528000000, 1461441600000]);
      this.setDaysCount(42);
      this.getPackageCalendar();
      this.construct();
      this.eventManager();
      this.setValidation();
    },

    getPackageCalendar: function() {
        this.packageCalendar = $('packageCalendar');
        this.calendarBackBtn = $('cBackBtn');
        this.calendarForwardBtn = $('cForwardBtn');
        this.monthYear = $('cMonthYear');
        this.hourField = $('cHour');
        this.minutesField = $('cMinutes');
        this.addBtn = $('cAddAction');
        this.packageStartDate = $('packageStartDate');
        this.packageStartHour = $('packageStartHour');
        this.packageStartMinute = $('packageStartMinute');
        this.calendarCloseBtn = $('calClose');
        this.packageCalDays = $('cDays');
        if ($$('.package-expiration-date')[0]) {
            this.curPackageExpirationDate = parseInt($$('.package-expiration-date')[0].get('value'));
        }

    },

    construct: function(gCalMonth, direction, setDate, objEl) {
        var self = this;

        // adding the days list elements to the container

        this.packageCalDays.set('html', '');
        for (var i = 0; i < this.daysCount; i++) {
            this.packageCalDays.adopt(new Element('li'));
        };

        // adoping the container to the view
        this.packageCalDays.adopt(new Element('div.clear'));

        // filling the calendar dates
        // defining the first date of the month by package available first date
        var dateRangeStart = null;

        if (!gCalMonth) {
            dateRangeStart = new Date();
            var monthFirstDate = new Date(dateRangeStart.getFullYear(), dateRangeStart.getMonth(), 1);
        } else {
            dateRangeStart = monthFirstDate = new Date(gCalMonth.split(',')[1], gCalMonth.split(',')[0], 1);
        }

        // getting the first day on the calendar
        var calStartDate = new Date(monthFirstDate.getTime() - (monthFirstDate.getDay() == 0 ? 6 : monthFirstDate.getDay() - 1) * 24 * 60 * 60 * 1000);
        var cDaysFields = this.packageCalDays.getElements('li');
        var monthNames = Cubix.OnlineBillingV4.Lang.months;

        // inserting the month and year
        this.monthYear.set('html', monthNames[dateRangeStart.getMonth()] + ' ' + dateRangeStart.getFullYear());
        cDaysFields.each(function(el) {
            el.removeAttribute('class');
            el.highlight('#9fa2a8');
            el.removeEvents();
        });

        // filling the dates in the calendar
        for (var i = cDaysFields.length - 1; i >= 0; i--) {
            var d = new Date(calStartDate).increment('day', i);

            cDaysFields[i].set('html', d.getDate());
            cDaysFields[i].set('data-value', d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate());

            if (!this.curPackageExpirationDate) {
                var b = new Date();
            } else {
                var b = new Date(this.curPackageExpirationDate);
            }

            b = new Date(b.getFullYear(), b.getMonth(), b.getDate());

            if (d < b) {
                cDaysFields[i].addClass('inactive');
            }

            // if the the packageDate is set
            if (setDate && typeof(setDate) !== "undefined") {
              var a = new Date(1970, 0, 1);

              a.setTime(setDate);
              var oDate = new Date(a.getFullYear(), a.getMonth(), a.getDate());

              if (d.getTime() == oDate.getTime()) {
                  cDaysFields[i].addClass('selected');
              }

              if (oDate.getTime() == (new Date(1970, 0, 1)).getTime()) {
                  this.hourField.set('value', '');
                  this.minutesField.set('value', '');
              } else {
                  this.hourField.set('value', a.getHours());
                  this.minutesField.set('value', a.getMinutes());
              }

            } else {
              var curDt = (new Date).get('date')

              if (d.getTime() == (new Date()).clearTime().getTime()) {
                cDaysFields[i].addClass('selected');
              }

              this.hourField.set('value', (new Date()).getHours());
              this.minutesField.set('value', (new Date()).getMinutes());

              this.packageStartDate.set('value', (new Date()).format('%Y-%m-%d'));
          }

            // set Button values
            var lastMonthArr = [new Date(monthFirstDate.getTime() - 15 * 24 * 60 * 60 * 1000).getMonth(), new Date(monthFirstDate.getTime() - 15 * 24 * 60 * 60 * 1000).getFullYear()]; // 15days is just a value to get the month and year
            var nextMonthArr = [new Date(monthFirstDate.getTime() + 45 * 24 * 60 * 60 * 1000).getMonth(), new Date(monthFirstDate.getTime() + 45 * 24 * 60 * 60 * 1000).getFullYear()]; // 45days is just a value to get the month and year

            this.calendarBackBtn.set('data-value', lastMonthArr);
            this.calendarForwardBtn.set('data-value', nextMonthArr);

        }
        self.setCalAction(0, 1, objEl);
        if (objEl) {
            self.eventManager(objEl);
            self.setValidation(objEl);
        }
    },

    setDaysCount: function(days) {
        this.daysCount = days;
    },

    setCalAction: function(uptInact, initLoad, objEl) {
        var self = this;
        var cDaysFields = this.packageCalDays.getElements('li');

        cDaysFields.each(function(el) {
            el.removeEvents();
            if (!el.hasClass('inactive')) {
                if (!initLoad) {
                    el.removeAttribute('class');
                }

                el.addEvent('click', function() {

                    // to update also the inactive cells
                    if (!initLoad) {
                        self.setCalAction(1, 0, objEl);
                    } else {
                        self.setCalAction(1, 0, objEl);
                    }
                    self.packageStartDate.set('value', el.getAttribute('data-value'));
                    el.addClass('selected');
                    el.removeEvents();
                    self.checkIfValid(objEl);
                });
            }

            if (uptInact == true && el.hasClass('selected') && el.hasClass('inactive')) {
                el.removeClass('selected');
            }
        });
    },

    showCalendar: function(vEl, modelEl) {

        var self = this;

        if (modelEl) {
            this.addBtn.setAttribute('data-action', 'edit');
        } else {
            this.addBtn.setAttribute('data-action', 'create');
        }

        // setting the position of the calendar on the page
        $(document.body).scrollTo(0, vEl.getPosition().y - window.getSize().y * 0.75);

        this.packageCalendar.setStyle('top', (vEl.getPosition().y < 700) ? (vEl.getPosition().y - 493) : window.getSize().y * 0.75 - 493);
        this.packageCalendar.setStyle('left', vEl.getPosition().x - 25);

        this.packageCalendar.show();
        Cubix.OnlineBillingV4.Packages.loadOverlay();

        // adding Event listerner to document, to close calendar when 'Esc' is pressed
        document.addEvent('keyup', function(e) {
            e = e.event;
            var key = e.keyCode || e.which;
            if (key == 27) {
                self.hideCalendar();
                $$('.act-type')[0].fireEvent('click');
            }
        });

        this.minutesField.fireEvent('keyup');
        this.hourField.fireEvent('keyup');
    },

    hideCalendar: function() {
        this.packageCalendar.hide();
        Cubix.OnlineBillingV4.Packages.unloadOverlay();
    },

    setValidation: function(objEl) {
        var self = this;
        self.hourField.removeEvents();
        self.hourField.addEvent('keyup', function() {
            if (this.get('value')) {

                if (parseInt(this.get('value')) >= 0 && parseInt(this.get('value')) <= 23) {
                    self.hourField.highlight();
                    self.hourField.set('value', ('0' + parseInt(this.get('value'))).slice(-2));
                    self.packageStartHour.set('value', self.hourField.get('value'));
                    self.checkIfValid(objEl ? objEl : '');
                } else {
                    self.hourField.highlight('#F00');
                    self.hourField.set('value', 0);
                }
            }
        });

        self.minutesField.removeEvents();
        self.minutesField.addEvent('keyup', function() {
            if (this.get('value')) {
                if (parseInt(this.get('value')) >= 0 && parseInt(this.get('value')) <= 59) {
                    self.minutesField.highlight();
                    self.minutesField.set('value', ('0' + parseInt(this.get('value'))).slice(-2));
                    self.packageStartMinute.set('value', self.minutesField.get('value'));
                    self.checkIfValid(objEl ? objEl : '');
                } else {
                    self.minutesField.highlight('#F00');
                    self.minutesField.set('value', 0);
                }
            }
        });

        self.checkIfValid(objEl ? objEl : '');
    },

    checkIfValid: function(objEl) {
        var self = this;

        var year = this.packageStartDate.get('value').split('-')[0];
        var month = this.packageStartDate.get('value').split('-')[1] - 1;
        var day = this.packageStartDate.get('value').split('-')[2];

        if (this.packageStartDate.get('value') && this.packageStartHour.get('value') && this.packageStartMinute.get('value')) {
            var d = new Date(year, month, day, this.packageStartHour.get('value'), this.packageStartMinute.get('value'));
            if (this.addBtn.hasClass('inactive')) {
                this.addBtn.removeClass('inactive');
                this.addBtn.addClass('active');
                this.addBtn.removeEvents();
                this.addBtn.addEvent('click', function() {
                    if (self.addBtn.getAttribute('data-action') == 'create') {
                        self.addAction(d);
                    } else if (self.addBtn.getAttribute('data-action') == 'edit') {
                        self.addActionCart(d, objEl.view, objEl.model);
                    }

                });
            } else {
                this.addBtn.addClass('active');
                this.addBtn.removeEvents();
                this.addBtn.addEvent('click', function() {
                    if (self.addBtn.getAttribute('data-action') == 'create') {
                        self.addAction(d);
                    } else if (self.addBtn.getAttribute('data-action') == 'edit') {
                        self.addActionCart(d, objEl.view, objEl.model);
                    }
                });
            }
        }
    },

    setDateRange: function(dateRange) {
        this.dateRange = dateRange;
    },

    eventManager: function(objEl) {
        var self = this;

        this.calendarCloseBtn.removeEvents();
        this.calendarCloseBtn.addEvent('click', function() {
            self.hideCalendar();
            $$('.act-type')[0].fireEvent('click');
        });

        self.calendarBackBtn.removeEvents();
        self.calendarBackBtn.addEvent('click', function() {
            var gCalMonth = this.getAttribute('data-value');

            self.construct(gCalMonth, 'b', (new Date(self.packageStartDate.get('value') + ' ' + self.packageStartHour.get('value') + ':' + self.packageStartMinute.get('value'))).getTime(), objEl ? objEl : false);
        });

        self.calendarForwardBtn.removeEvents();
        self.calendarForwardBtn.addEvent('click', function() {
            var gCalMonth = this.getAttribute('data-value');

            self.construct(gCalMonth, 'f', (new Date(self.packageStartDate.get('value') + ' ' + self.packageStartHour.get('value') + ':' + self.packageStartMinute.get('value'))).getTime(), objEl ? objEl : false);
        });
    },

    addAction: function(d) {
        if (d < new Date()) {
            alert(Cubix.OnlineBillingV4.Lang.errs.invalidTime);
            return false;
        }

        var activationDate = parseInt(d.getFullYear()) + '-' + ('0' + (parseInt(d.getMonth()) + 1)).slice(-2) + '-' + ('0' + d.getDate()).slice(-2) + ' ' + ('0' + d.getHours()).slice(-2) + ':' + ('0' + d.getMinutes()).slice(-2);
        Cubix.OnlineBillingV4.Packages.selectedDate.set('value', d.getTime());
        Cubix.OnlineBillingV4.Packages.specDateActivationFld.set('value', activationDate);

        if (!Cubix.OnlineBillingV4.Packages.specDateActivationOpt.hasClass('checked')) {
            $$('.act-type').each(function(el) {
                if (el.hasClass('checked')) {
                    el.removeClass('checked');
                }
            });
            Cubix.OnlineBillingV4.Packages.specDateActivationOpt.addClass('checked');

        }

        this.hideCalendar();
    },

    addActionCart: function(d, viewEl, modelEl) {
        if (d < new Date()) {
            alert('Invalid time for the activation!');
            return false;
        }
        var activationDate = d.getFullYear() + '-' + ('0' + (parseInt(d.getMonth()) + 1)).slice(-2) +
                            '-' + ('0' + d.getDate()).slice(-2) + ' ' + ('0' + d.getHours()).slice(-2) +
                            ':' + ('0' + d.getMinutes()).slice(-2) + ' ';
        viewEl.set('html', activationDate);
        modelEl.set('value', d.getTime());

        var activeGotdDays = viewEl.getParent('.sp-package-table').getElements('.activated-gotd-days');

        if (activeGotdDays.length > 0) {
            activeGotdDays.each(function(el) {
                el.destroy();
            });
        }

        viewEl.getParent('.sp-package-table').getElements('.package-gotd-object')[0].set('value', '[]');
        viewEl.getParent('.sp-package-table').getElements('.sp-package-result')[0].getElements('td')[2].set('html', '');
        Cubix.OnlineBillingV4.Cart.updateCartPrice();
        $('spCart').fireEvent('cartChange');
        this.hideCalendar();
    }
};

// Gotd Calendar
Cubix.OnlineBillingV4.GotdCalendar = {
    // default
    indepGotdLimit: 0,

    // Gotd Calendar properties

    dateRange: [],
    curEscortId: 0,
    gotdAreas: [],
    daysCount: 0,
    gotdCalendarSelectedDates: [],
    gotdCalendarEl: null,
    gotdAreasEl: null,
    gotdSelectedArea: 0,
    gotdCalendarCloseBtn: null,
    gotdCalendarBackBtn: null,
    gotdCalendarMonthYearEl: null,
    gotdCalendarForwardBtn: null,
    gotdCalendarDaysEl: null,
    gotdCalendarTotalPriceEl: null,
    gotdCalendarAddBtn: null,
    gotdBookedDays: null,
    gotdCurrentBookedDates: [],
    gotdPreselectedDates: [],
    gotdObjEl: null,
    gotdPrice: 0,

    // Gotd Calendar methods

    init: function() {
      this.getGotdCalendarView();
      this.setDaysCount(42);
      this.setAreas(this.gotdAreasList.get('value'));
      this.construct();
      this.eventManager();
    },

    getGotdCalendarView: function() {
        this.gotdCalendarEl = $('gotdCalendar');
        this.gotdAreasEl = $('gotdAreas');
        this.gotdCalendarCloseBtn = $('gCalCloseBtn');
        this.gotdCalendarBackBtn = $('gCalBackBtn');
        this.gotdCalendarMonthYearEl = $('gCalMonthYear');
        this.gotdCalendarForwardBtn = $('gCalForwardBtn');
        this.gotdCalendarDaysEl = $('gCalDays');
        this.gotdCalendarTotalPriceEl = $('gotdTotalPrice');
        this.gotdCalendarAddBtn = $('gCalAddBtn');
        this.gotdCalendarSelectedDates = $('gotdSelectedDates');
        this.gotdPrice = $('gotdPrice').get('value');
        this.gotdBookedDays = $('gotdBookedDays').get('value');

        // Data from backend
        this.gotdAreasList = $('escortAreas');
    },

    construct: function(gCalMonth, direction) {

        var self = this;
        self.updatePrice(true);
        // adding the days list elements to the container
        this.gotdCalendarDaysEl.set('html', '');
        for (var i = 0; i < this.daysCount; i++) {
            this.gotdCalendarDaysEl.adopt(new Element('li'));
        }

        // adoping clearing element the container to the view
        this.gotdCalendarDaysEl.adopt(new Element('div.clear'));

        // adding available areas to the areas selector
        if (!this.gotdSelectedArea) {
            this.gotdAreasEl.getElements('option').destroy();

            // adding default option for areas
            this.gotdAreasEl.adopt(new Element('option', {
                'html': '---',
                'data-id': ''
            }));

            for (var i = this.gotdAreas.length - 1; i >= 0; i--) {
                this.gotdAreasEl.adopt(new Element('option', {
                    'html': this.gotdAreas[i].title,
                    'data-id': this.gotdAreas[i].id
                }))
            }
        }

        // filling the calendar dates
        // defining the first date of the month by package available first date
        if (!gCalMonth) {
            var dateRangeStart = new Date();
            var monthFirstDate = new Date(dateRangeStart.getFullYear(), dateRangeStart.getMonth(), 1);
        } else {
            var dateRangeStart = monthFirstDate = new Date(gCalMonth.split(',')[1], gCalMonth.split(',')[0], 1);
        }

        // getting the first day on the calendar
        var calStartDate = new Date(monthFirstDate.getTime() - (monthFirstDate.getDay() == 0 ? 6 : monthFirstDate.getDay() - 1) * 24 * 60 * 60 * 1000);
        var cDaysFields = this.gotdCalendarDaysEl.getElements('li');
        var monthNames = Cubix.OnlineBillingV4.Lang.months;

        // inserting the month and year
        this.gotdCalendarMonthYearEl.set('html', monthNames[dateRangeStart.getMonth()] + ' ' + dateRangeStart.getFullYear());
        cDaysFields.each(function(el) {
            el.removeAttribute('class');
            el.highlight('#9fa2a8');
            el.removeEvents();
        });

        // filling the dates in the calendar
        for (var i = cDaysFields.length - 1; i >= 0; i--) {
            var d = new Date(calStartDate).increment('day', i);

            cDaysFields[i].set('html', d.format('%d'));
            cDaysFields[i].set('data-value', d.format('%Y-%m-%d'));

            if (!self.gotdSelectedArea) {
                cDaysFields[i].addEvent('click', function() {
                    if (self.gotdAreasEl.getParent('div').getElements('.err-msg').length <= 0) {
                        self.createErrTooltip(self.gotdAreasEl, Cubix.OnlineBillingV4.Lang.errs.selectRegion)
                    } else {
                        self.gotdAreasEl.highlight();
                    }
                });
            } else {
                var currentPackageItem = $$('.cart-item-gotd')[0];
                var b = Date.parse(self.dateRange[0]);

                b = (new Date(b.getFullYear(), b.getMonth(), b.getDate())).getTime();

                var c = Date.parse(this.dateRange[1]);
                c = (new Date(c.getFullYear(), c.getMonth(), c.getDate())).getTime() - (1 * 24 * 60 * 60 * 1000);

                if (d.getTime() >= b && d.getTime() <= c) {
                    cDaysFields[i].set('class', 'active');
                    cDaysFields[i].addEvent('click', function() {
                        if (this.hasClass('active')) {
                            this.removeClass('active');
                            this.addClass('selected');

                            var v = self.gotdCalendarSelectedDates.get('value');
                            self.gotdCalendarSelectedDates.set('value', v + (v.length > 0 ? ',' : '') + this.get('data-value'));

                            if (self.gotdCalendarSelectedDates.get('value')) {
                                self.gotdCalendarAddBtn.removeAttribute('class');
                                self.gotdCalendarAddBtn.set('class', 'active');
                                self.gotdCalendarAddBtn.removeEvents();
                                self.updatePrice(false);
                                self.gotdCalendarAddBtn.addEvent('click', function() {
                                    self.addAction(self.gotdCalendarSelectedDates.get('value'), $('gotdAreas').getSelected().get('html'));
                                });
                            } else {
                                self.updatePrice(true);
                            }
                        } else if (this.hasClass('selected')) {
                            this.removeClass('selected');
                            this.addClass('active');

                            var x = self.gotdCalendarSelectedDates.get('value');
                            var y = x.split(',');

                            var index = y.indexOf(this.get('data-value'));

                            if (index > -1) {
                                y.splice(index, 1);
                            }

                            self.gotdCalendarSelectedDates.set('value', y.toString());

                            if (self.gotdCalendarSelectedDates.get('value')) {
                                self.gotdCalendarAddBtn.removeAttribute('class');
                                self.gotdCalendarAddBtn.set('class', 'active');
                                self.gotdCalendarAddBtn.removeEvents();

                                self.gotdCalendarAddBtn.addEvent('click', function() {
                                    self.addAction(self.gotdCalendarSelectedDates.get('value'), $('gotdAreas').getSelected().get('html'));
                                });
                                self.updatePrice(false);

                            } else {
                                self.gotdCalendarAddBtn.removeAttribute('class');
                                self.gotdCalendarAddBtn.set('class', 'inactive');
                                self.gotdCalendarAddBtn.removeEvents();
                                self.updatePrice(true);
                            }
                        }
                    });
                }

                if (self.gotdCurrentBookedDates.indexOf(d.getTime()) != -1 && d.getTime() >= b && d.getTime() <= c) {
                    cDaysFields[i].removeEvents();
                    cDaysFields[i].set('class', 'booked');
                }

                // Getting dates for the escort

                var peArr = Object.values(self.gotdPreselectedDates);

                var arrPe = [];

                if (peArr[0]) {
                    peArr.each(function(el) {
                        el.each(function(item) {
                            if (item.escort_id == self.curEscortId) {
                                arrPe.push(item.date);
                            }
                        });
                    });
                }

                var arrPd = [];

                // Getting dates for the area

                var pdObj = Object.filter(self.gotdPreselectedDates, function(value, key) {
                    return key == self.gotdSelectedArea;
                });

                if (pdObj[self.gotdSelectedArea]) {
                    pdObj[self.gotdSelectedArea].each(function(el) {
                        arrPd.push(el['date']);
                    });
                }

                // Combining the dates

                arrPd.combine(arrPe);

                if (arrPd.indexOf(d.getTime()) != -1 && d.getTime() >= b && d.getTime() <= c) {
                    cDaysFields[i].removeEvents();
                    cDaysFields[i].set('class', 'preselected');
                }
            }

            // set Button values

            var lastMonthArr = [new Date(monthFirstDate.getTime() - 15 * 24 * 60 * 60 * 1000).getMonth(), new Date(monthFirstDate.getTime() - 15 * 24 * 60 * 60 * 1000).getFullYear()]; // 15days is just a value to get the month and year
            var nextMonthArr = [new Date(monthFirstDate.getTime() + 45 * 24 * 60 * 60 * 1000).getMonth(), new Date(monthFirstDate.getTime() + 45 * 24 * 60 * 60 * 1000).getFullYear()]; // 45days is just a value to get the month and year
            this.gotdCalendarBackBtn.set('data-value', lastMonthArr);
            this.gotdCalendarForwardBtn.set('data-value', nextMonthArr);
        }
    },

    setDaysCount: function(days) {
        this.daysCount = days;
    },

    setDateRange: function(dateRange) {
        this.dateRange = dateRange;
    },

    setAreas: function(areas) {
        this.gotdAreas = JSON.decode(areas);
    },

    updatePrice: function(remove) {
        if (!remove) {
            var total = this.gotdPrice * this.gotdCalendarSelectedDates.get('value').split(',').length;
            this.gotdCalendarTotalPriceEl.set('html', Cubix.OnlineBillingV4.Lang.total + total + ' CHF');
        } else {
            this.gotdCalendarTotalPriceEl.set('html', Cubix.OnlineBillingV4.Lang.total +  '0 CHF');
        }
    },

    showCalendar: function(viewEl) {

        var self = this;

        // setting the position of the calendar on the page

        $(document.body).scrollTo(0, viewEl.getPosition().y - window.getSize().y * 0.85);

        this.gotdCalendarEl.setStyle('top', (viewEl.getPosition().y < 700) ? (viewEl.getPosition().y - 565) : window.getSize().y * 0.85 - 565);
        this.gotdCalendarEl.setStyle('left', viewEl.getPosition().x - 25);
        this.construct(false, false);
        this.curEscortId = viewEl.getParent('.cart-item').get('data-escort');
        this.gotdCalendarEl.show();
        Cubix.OnlineBillingV4.Packages.loadOverlay();

        // adding Event listerner to document, to close calendar when 'Esc' is pressed

        document.addEvent('keyup', function(e) {
            e = e.event;
            var key = e.keyCode || e.which;
            if (key == 27) {
                self.hideCalendar();
                $$('.act-type')[0].fireEvent('click');
            }
        });
    },

    hideCalendar: function() {
        this.gotdCalendarEl.hide();
        Cubix.OnlineBillingV4.Packages.unloadOverlay();
    },

    eventManager: function() {
        var self = this;

        this.gotdCalendarCloseBtn.addEvent('click', function() {
          self.hideCalendar();
          if ($$('.gotd-item')) {
            $$('.gotd-item').each(function(el) {
              if (!el.contains($$('.activated-gotd-days')[0])) {
                el.getElement('.sp-delete-symbol').fireEvent('click');
              }
            });
          }
        });

        // selecting the gotd area
        this.gotdAreasEl.addEvent('change', function() {
            var areaId = +this.getSelected().get('data-id');
            self.gotdCalendarAddBtn.removeAttribute('class');
            self.gotdCalendarAddBtn.removeEvents();

          self.gotdSelectedArea = areaId;

          if (areaId) {
            if (self.gotdAreasEl.getParent('div').getElements('.err-msg').length > 0) {
              self.gotdAreasEl.getParent('div').getElements('.err-msg').destroy();
            }
          }

          var x = JSON.decode(self.gotdBookedDays);
          var bookedDaysObj = x[areaId];

          if (bookedDaysObj) {
            var bookedDates = [];

            for (var i = bookedDaysObj.length - 1; i >= 0; i--) {
              bookedDates.push((new Date(bookedDaysObj[i]['date'])).clearTime().getTime());
            }

            self.gotdCurrentBookedDates = bookedDates;

          } else {
            self.gotdCurrentBookedDates = '';
          }

          self.gotdCalendarSelectedDates.set('value', '');
          self.construct(false, false);
        });

        // behavior of calendar back end forward buttons
        self.gotdCalendarBackBtn.addEvent('click', function() {
          var gCalMonth = this.getAttribute('data-value');
          self.gotdCalendarSelectedDates.set('value', '');
          self.construct(gCalMonth, 'b');
        });

        self.gotdCalendarForwardBtn.addEvent('click', function() {
          var gCalMonth = this.getAttribute('data-value');
          self.gotdCalendarSelectedDates.set('value', '');
          self.construct(gCalMonth, 'f');
        });
    },

    addAction: function(strDates, area) {
      var self = this,
          arrStrDatesTime = [],
          arrStrDate = strDates.split(','),
          curAreaId = $('gotdAreas').getSelected().get('data-id'),
          curPreselectedDatesArr = [];

      for (var i = arrStrDate.length - 1; i >= 0; i--) {
          this.gotdObjEl.gotdViewEl.adopt(new Element('span', {
              html: '<br><span class="sp-delete-symbol">&#215;</span> <span>' + arrStrDate[i] +
                    '<span class="gotd-city">' + area + '</span></span>',
              'data-date': arrStrDate[i],
              'data-area': curAreaId,
              class: 'activated-gotd-days'
          }));

          curPreselectedDatesArr.push({
              'date': (new Date(arrStrDate[i] + ' 00:00')).getTime(),
              'escort_id': this.curEscortId
          });
      };

      if (this.gotdPreselectedDates[parseInt(curAreaId)]) {
          this.gotdPreselectedDates[parseInt(curAreaId)].combine(curPreselectedDatesArr);
      } else {
          this.gotdPreselectedDates[parseInt(curAreaId)] = curPreselectedDatesArr;
      }

      $$('.sp-delete-symbol').each(function(el) {

          el.addEvent('click', function() {
            var d = false;
            if (el.getParent('td').getElements('.activated-gotd-days') &&
              el.getParent('td').getElements('.activated-gotd-days').length == 1 &&
              el.getParent('.gotd-item')){
              var d = true;
              var itemToDelete = el.getParent('.gotd-item').getElement('.sp-delete-symbol');
            }
            var xDate = Date.parse(el.getParent('span').get('data-date')).getTime(),
                xEscort = el.getParent('.cart-item').get('data-escort'),
                xArea = el.getParent('.activated-gotd-days').get('data-area');

            this.getSiblings('br').destroy();
            this.getParent('span').destroy();

            self.gotdPreselectedDates[xArea] = self.gotdPreselectedDates[xArea].filter(function(item, index) {
                var clause = (item.date == xDate && item.escort_id == xEscort);
                return !clause;
            });

            if (d) {
              itemToDelete.fireEvent('click');
            }
            Cubix.OnlineBillingV4.Cart.updateCartPrice();
            $('spCart').fireEvent('cartChange');
          });
      });

      var arrO = [];
      var arrPackageGotdObj = this.gotdObjEl.gotdModelEl.get('value');
      if (arrPackageGotdObj) {
          for (var i = JSON.decode(arrPackageGotdObj).length - 1; i >= 0; i--) {
              arrO.push(JSON.decode(arrPackageGotdObj)[i]);
          }
      }

      var o = {};
      o[this.gotdSelectedArea] = strDates;
      arrO.push(o);

      this.gotdObjEl.gotdModelEl.set('value', JSON.encode(arrO));

      self.gotdCalendarSelectedDates.set('value', '');
      this.hideCalendar();
      Cubix.OnlineBillingV4.Cart.updateCartPrice();
      $('spCart').fireEvent('cartChange');
      Cubix.OnlineBillingV4.Cart.paymentMethodsOverlay.enable();
      self.gotdCalendarAddBtn.removeAttribute('class');
      self.gotdCalendarAddBtn.removeEvents();
    },

    createErrTooltip: function(el, msg) {
        var x = el.getPosition().x;
        var y = el.getPosition().y;

        var errMsg = new Element('span', {
          'class': 'err-msg',
          'html': msg,
          'styles': {
            'top': 0,
            'left': '50%',
            'transform': 'translate(-50%,-100%)'
          }
        });

        el.getParent('div').adopt(errMsg);
        errMsg.fade('in');
        errMsg.setStyle('margin-top', -10);
    }
};

//Votd Calendar
Cubix.OnlineBillingV4.VotdCalendar = {
    // default
    indepVotdLimit: 0,

    // Votd Calendar properties

    dateRange: [],
    curEscortId: 0,
    votdAreas: [],
    daysCount: 0,
    votdCalendarSelectedDates: [],
    votdCalendarEl: null,
    votdAreasEl: null,
    votdSelectedArea: 0,
    votdCalendarCloseBtn: null,
    votdCalendarBackBtn: null,
    votdCalendarMonthYearEl: null,
    votdCalendarForwardBtn: null,
    votdCalendarDaysEl: null,
    votdCalendarTotalPriceEl: null,
    votdCalendarAddBtn: null,
    votdBookedDays: null,
    votdCurrentBookedDates: [],
    votdPreselectedDates: [],
    votdObjEl: null,
    votdPrice: 0,

    // Gotd Calendar methods

    init: function() {
      this.getVotdCalendarView();
      this.setDaysCount(42);
      this.setAreas(this.votdAreasList.get('value'));
      this.construct();
      this.eventManager();
    },

    getVotdCalendarView: function() {
        this.votdCalendarEl = $('votdCalendar');
        this.votdAreasEl = $('votdAreas');
        this.votdCalendarCloseBtn = $('vCalCloseBtn');
        this.votdCalendarBackBtn = $('vCalBackBtn');
        this.votdCalendarMonthYearEl = $('vCalMonthYear');
        this.votdCalendarForwardBtn = $('vCalForwardBtn');
        this.votdCalendarDaysEl = $('vCalDays');
        this.votdCalendarTotalPriceEl = $('votdTotalPrice');
        this.votdCalendarAddBtn = $('vCalAddBtn');
        this.votdCalendarSelectedDates = $('votdSelectedDates');
        this.votdPrice = $('votdPrice').get('value');
        this.votdBookedDays = $('votdBookedDays').get('value');

        // Data from backend
        this.votdAreasList = $('escortAreas');
    },

    construct: function(vCalMonth, direction) {

        var self = this;
        self.updatePrice(true);
        // adding the days list elements to the container
        this.votdCalendarDaysEl.set('html', '');
        for (var i = 0; i < this.daysCount; i++) {
            this.votdCalendarDaysEl.adopt(new Element('li'));
        }

        // adoping clearing element the container to the view
        this.votdCalendarDaysEl.adopt(new Element('div.clear'));

        // adding available areas to the areas selector
        if (!this.votdSelectedArea) {
            this.votdAreasEl.getElements('option').destroy();

            // adding default option for areas
            this.votdAreasEl.adopt(new Element('option', {
                'html': '---',
                'data-id': ''
            }));

            for (var i = this.votdAreas.length - 1; i >= 0; i--) {
                this.votdAreasEl.adopt(new Element('option', {
                    'html': this.votdAreas[i].title,
                    'data-id': this.votdAreas[i].id
                }))
            }
        }

        // filling the calendar dates
        // defining the first date of the month by package available first date
        if (!vCalMonth) {
            var dateRangeStart = new Date();
            var monthFirstDate = new Date(dateRangeStart.getFullYear(), dateRangeStart.getMonth(), 1);
        } else {
            var dateRangeStart = monthFirstDate = new Date(vCalMonth.split(',')[1], vCalMonth.split(',')[0], 1);
        }

        // getting the first day on the calendar
        var calStartDate = new Date(monthFirstDate.getTime() - (monthFirstDate.getDay() == 0 ? 6 : monthFirstDate.getDay() - 1) * 24 * 60 * 60 * 1000);
        var cDaysFields = this.votdCalendarDaysEl.getElements('li');
        var monthNames = Cubix.OnlineBillingV4.Lang.months;

        // inserting the month and year
        this.votdCalendarMonthYearEl.set('html', monthNames[dateRangeStart.getMonth()] + ' ' + dateRangeStart.getFullYear());
        cDaysFields.each(function(el) {
            el.removeAttribute('class');
            el.highlight('#9fa2a8');
            el.removeEvents();
        });

        // filling the dates in the calendar
        for (var i = cDaysFields.length - 1; i >= 0; i--) {
            var d = new Date(calStartDate).increment('day', i);

            cDaysFields[i].set('html', d.format('%d'));
            cDaysFields[i].set('data-value', d.format('%Y-%m-%d'));

            if (!self.votdSelectedArea) {
                cDaysFields[i].addEvent('click', function() {
                    if (self.votdAreasEl.getParent('div').getElements('.err-msg').length <= 0) {
                        self.createErrTooltip(self.votdAreasEl, Cubix.OnlineBillingV4.Lang.errs.selectRegion)
                    } else {
                        self.votdAreasEl.highlight();
                    }
                });
            } else {
                var currentPackageItem = $$('.cart-item-votd')[0];
                var b = Date.parse(self.dateRange[0]);

                b = (new Date(b.getFullYear(), b.getMonth(), b.getDate())).getTime();

                var c = Date.parse(this.dateRange[1]);
                c = (new Date(c.getFullYear(), c.getMonth(), c.getDate())).getTime() - (1 * 24 * 60 * 60 * 1000);

                if (d.getTime() >= b && d.getTime() <= c) {
                    cDaysFields[i].set('class', 'active');
                    cDaysFields[i].addEvent('click', function() {
                        if (this.hasClass('active')) {
                            this.removeClass('active');
                            this.addClass('selected');

                            var v = self.votdCalendarSelectedDates.get('value');
                            self.votdCalendarSelectedDates.set('value', v + (v.length > 0 ? ',' : '') + this.get('data-value'));

                            if (self.votdCalendarSelectedDates.get('value')) {
                                self.votdCalendarAddBtn.removeAttribute('class');
                                self.votdCalendarAddBtn.set('class', 'active');
                                self.votdCalendarAddBtn.removeEvents();
                                self.updatePrice(false);
                                self.votdCalendarAddBtn.addEvent('click', function() {
                                    self.addAction(self.votdCalendarSelectedDates.get('value'), $('votdAreas').getSelected().get('html'));
                                });
                            } else {
                                self.updatePrice(true);
                            }
                        } else if (this.hasClass('selected')) {
                            this.removeClass('selected');
                            this.addClass('active');

                            var x = self.votdCalendarSelectedDates.get('value');
                            var y = x.split(',');

                            var index = y.indexOf(this.get('data-value'));

                            if (index > -1) {
                                y.splice(index, 1);
                            }

                            self.votdCalendarSelectedDates.set('value', y.toString());

                            if (self.votdCalendarSelectedDates.get('value')) {
                                self.votdCalendarAddBtn.removeAttribute('class');
                                self.votdCalendarAddBtn.set('class', 'active');
                                self.votdCalendarAddBtn.removeEvents();

                                self.votdCalendarAddBtn.addEvent('click', function() {
                                    self.addAction(self.votdCalendarSelectedDates.get('value'), $('votdAreas').getSelected().get('html'));
                                });
                                self.updatePrice(false);

                            } else {
                                self.votdCalendarAddBtn.removeAttribute('class');
                                self.votdCalendarAddBtn.set('class', 'inactive');
                                self.votdCalendarAddBtn.removeEvents();
                                self.updatePrice(true);
                            }
                        }
                    });
                }

                if (self.votdCurrentBookedDates.indexOf(d.getTime()) != -1 && d.getTime() >= b && d.getTime() <= c) {
                    cDaysFields[i].removeEvents();
                    cDaysFields[i].set('class', 'booked');
                }

                // Getting dates for the escort

                var peArr = Object.values(self.votdPreselectedDates);

                var arrPe = [];

                if (peArr[0]) {
                    peArr.each(function(el) {
                        el.each(function(item) {
                            if (item.escort_id == self.curEscortId) {
                                arrPe.push(item.date);
                            }
                        });
                    });
                }

                var arrPd = [];

                // Getting dates for the area

                var pdObj = Object.filter(self.votdPreselectedDates, function(value, key) {
                    return key == self.votdSelectedArea;
                });

                if (pdObj[self.votdSelectedArea]) {
                    pdObj[self.votdSelectedArea].each(function(el) {
                        arrPd.push(el['date']);
                    });
                }

                // Combining the dates

                arrPd.combine(arrPe);

                if (arrPd.indexOf(d.getTime()) != -1 && d.getTime() >= b && d.getTime() <= c) {
                    cDaysFields[i].removeEvents();
                    cDaysFields[i].set('class', 'preselected');
                }
            }

            // set Button values

            var lastMonthArr = [new Date(monthFirstDate.getTime() - 15 * 24 * 60 * 60 * 1000).getMonth(), new Date(monthFirstDate.getTime() - 15 * 24 * 60 * 60 * 1000).getFullYear()]; // 15days is just a value to get the month and year
            var nextMonthArr = [new Date(monthFirstDate.getTime() + 45 * 24 * 60 * 60 * 1000).getMonth(), new Date(monthFirstDate.getTime() + 45 * 24 * 60 * 60 * 1000).getFullYear()]; // 45days is just a value to get the month and year
            this.votdCalendarBackBtn.set('data-value', lastMonthArr);
            this.votdCalendarForwardBtn.set('data-value', nextMonthArr);
        }
    },

    setDaysCount: function(days) {
        this.daysCount = days;
    },

    setDateRange: function(dateRange) {
        this.dateRange = dateRange;
    },

    setAreas: function(areas) {
        this.votdAreas = JSON.decode(areas);
    },

    updatePrice: function(remove) {
        if (!remove) {
            var total = this.votdPrice * this.votdCalendarSelectedDates.get('value').split(',').length;
            this.votdCalendarTotalPriceEl.set('html', Cubix.OnlineBillingV4.Lang.total + total + ' CHF');
        } else {
            this.votdCalendarTotalPriceEl.set('html', Cubix.OnlineBillingV4.Lang.total +  '0 CHF');
        }
    },

    showCalendar: function(viewEl) {

        var self = this;

        // setting the position of the calendar on the page

        $(document.body).scrollTo(0, viewEl.getPosition().y - window.getSize().y * 0.85);

        this.votdCalendarEl.setStyle('top', (viewEl.getPosition().y < 700) ? (viewEl.getPosition().y - 565) : window.getSize().y * 0.85 - 565);
        this.votdCalendarEl.setStyle('left', viewEl.getPosition().x - 25);
        this.construct(false, false);
        this.curEscortId = viewEl.getParent('.cart-item').get('data-escort');
        this.votdCalendarEl.show();
        Cubix.OnlineBillingV4.Packages.loadOverlay();

        // adding Event listerner to document, to close calendar when 'Esc' is pressed

        document.addEvent('keyup', function(e) {
            e = e.event;
            var key = e.keyCode || e.which;
            if (key == 27) {
                self.hideCalendar();
                $$('.act-type')[0].fireEvent('click');
            }
        });
    },

    hideCalendar: function() {
        this.votdCalendarEl.hide();
        Cubix.OnlineBillingV4.Packages.unloadOverlay();
    },

    eventManager: function() {
        var self = this;

        this.votdCalendarCloseBtn.addEvent('click', function() {
          self.hideCalendar();
          if ($$('.votd-item')) {
            $$('.votd-item').each(function(el) {
              if (!el.contains($$('.activated-votd-days')[0])) {
                el.getElement('.sp-delete-symbol').fireEvent('click');
              }
            });
          }
        });

        // selecting the gotd area
        this.votdAreasEl.addEvent('change', function() {
            var areaId = +this.getSelected().get('data-id');
            self.votdCalendarAddBtn.removeAttribute('class');
            self.votdCalendarAddBtn.removeEvents();

          self.votdSelectedArea = areaId;

          if (areaId) {
            if (self.votdAreasEl.getParent('div').getElements('.err-msg').length > 0) {
              self.votdAreasEl.getParent('div').getElements('.err-msg').destroy();
            }
          }

          var x = JSON.decode(self.votdBookedDays);
          var bookedDaysObj = x[areaId];

          if (bookedDaysObj) {
            var bookedDates = [];

            for (var i = bookedDaysObj.length - 1; i >= 0; i--) {
              bookedDates.push((new Date(bookedDaysObj[i]['date'])).clearTime().getTime());
            }

            self.votdCurrentBookedDates = bookedDates;

          } else {
            self.votdCurrentBookedDates = '';
          }

          self.votdCalendarSelectedDates.set('value', '');
          self.construct(false, false);
        });

        // behavior of calendar back end forward buttons
        self.votdCalendarBackBtn.addEvent('click', function() {
          var vCalMonth = this.getAttribute('data-value');
          self.votdCalendarSelectedDates.set('value', '');
          self.construct(vCalMonth, 'b');
        });

        self.votdCalendarForwardBtn.addEvent('click', function() {
          var vCalMonth = this.getAttribute('data-value');
          self.votdCalendarSelectedDates.set('value', '');
          self.construct(vCalMonth, 'f');
        });
    },

    addAction: function(strDates, area) {
      var self = this,
          arrStrDatesTime = [],
          arrStrDate = strDates.split(','),
          curAreaId = $('votdAreas').getSelected().get('data-id'),
          curPreselectedDatesArr = [];

      for (var i = arrStrDate.length - 1; i >= 0; i--) {
          this.votdObjEl.votdViewEl.adopt(new Element('span', {
              html: '<br><span class="sp-delete-symbol">&#215;</span> <span>' + arrStrDate[i] +
                    '<span class="votd-city">' + area + '</span></span>',
              'data-date': arrStrDate[i],
              'data-area': curAreaId,
              class: 'activated-votd-days'
          }));

          curPreselectedDatesArr.push({
              'date': (new Date(arrStrDate[i] + ' 00:00')).getTime(),
              'escort_id': this.curEscortId
          });
      };

      if (this.votdPreselectedDates[parseInt(curAreaId)]) {
          this.votdPreselectedDates[parseInt(curAreaId)].combine(curPreselectedDatesArr);
      } else {
          this.votdPreselectedDates[parseInt(curAreaId)] = curPreselectedDatesArr;
      }

      $$('.sp-delete-symbol').each(function(el) {

          el.addEvent('click', function() {
            var d = false;
            if (el.getParent('td').getElements('.activated-votd-days') &&
              el.getParent('td').getElements('.activated-votd-days').length == 1 &&
              el.getParent('.votd-item')){
              var d = true;
              var itemToDelete = el.getParent('.votd-item').getElement('.sp-delete-symbol');
            }
            var xDate = Date.parse(el.getParent('span').get('data-date')).getTime(),
                xEscort = el.getParent('.cart-item').get('data-escort'),
                xArea = el.getParent('.activated-votd-days').get('data-area');

            this.getSiblings('br').destroy();
            this.getParent('span').destroy();

            self.votdPreselectedDates[xArea] = self.votdPreselectedDates[xArea].filter(function(item, index) {
                var clause = (item.date == xDate && item.escort_id == xEscort);
                return !clause;
            });

            if (d) {
              itemToDelete.fireEvent('click');
            }
            Cubix.OnlineBillingV4.Cart.updateCartPrice();
            $('spCart').fireEvent('cartChange');
          });
      });

      var arrO = [];
      var arrPackageVotdObj = this.votdObjEl.votdModelEl.get('value');
      if (arrPackageVotdObj) {
          for (var i = JSON.decode(arrPackageVotdObj).length - 1; i >= 0; i--) {
              arrO.push(JSON.decode(arrPackageVotdObj)[i]);
          }
      }

      var o = {};
      o[this.votdSelectedArea] = strDates;
      arrO.push(o);

      this.votdObjEl.votdModelEl.set('value', JSON.encode(arrO));

      self.votdCalendarSelectedDates.set('value', '');
      this.hideCalendar();
      Cubix.OnlineBillingV4.Cart.updateCartPrice();
      $('spCart').fireEvent('cartChange');
      Cubix.OnlineBillingV4.Cart.paymentMethodsOverlay.enable();
      self.votdCalendarAddBtn.removeAttribute('class');
      self.votdCalendarAddBtn.removeEvents();
    },

    createErrTooltip: function(el, msg) {
        var x = el.getPosition().x;
        var y = el.getPosition().y;

        var errMsg = new Element('span', {
          'class': 'err-msg',
          'html': msg,
          'styles': {
            'top': 0,
            'left': '50%',
            'transform': 'translate(-50%,-100%)'
          }
        });

        el.getParent('div').adopt(errMsg);
        errMsg.fade('in');
        errMsg.setStyle('margin-top', -10);
    }
};
// Area Selector
Cubix.OnlineBillingV4.AreaSelector = {
    areas: [],
    areasEl: null,
    areaSelector: null,
    areaSelCloseBtn: null,
    areaAddBtn: null,
    areaModelEl: null,
    areaViewEl: null,
    escortBaseCityId: 0,


    init: function() {
      this.setAreas();
      this.getAreaSelector();
      this.eventManager();
      this.construct();
    },

    getAreaSelector: function() {
        this.areaSelector = $('areaSelector');
        this.areaSelectFld = $('areas');
        this.areaSelCloseBtn = $('areaSelCloseBtn');
        this.areaAddBtn = $('aSellAddBtn');
    },

    setAreas: function() {
        this.areas = JSON.decode($('escortAdditionalAreas').get('value'));
    },

    construct: function() {
        this.areaSelectFld.set('html', '');
        this.areaSelectFld.adopt(new Element('option', {
            html: '---',
            'data-id': ''
        }));

        for (var i = this.areas.length - 1; i >= 0; i--) {

            if (this.areaModelEl) {
                var arrAreaEl = this.areaModelEl.get('value').split(',');
                arrAreaEl.push(this.escortBaseCityId);
                var indexId = arrAreaEl.indexOf(this.areas[i].id.toString());
            } else {
                var indexId = 0;
            }

            if (indexId < 0) {
                this.areaSelectFld.adopt(new Element('option', {
                    html: this.areas[i].title,
                    'data-id': this.areas[i].id
                }));
            }
        }
    },

    addAction: function(areaArrEl) {
        var self = this;

        this.areaViewEl.adopt(new Element('span', {
            html: '<br><span class="sp-delete-symbol-area">&#215;</span><span>' + areaArrEl[0] + '</span>',
            'data-id': areaArrEl[1],
            class: 'selected-arr-areas'
        }));


        $$('.sp-delete-symbol-area').each(function(el) {
            el.addEvent('click', function() {
                var strArea = self.areaModelEl.get('value').split(',');
                var indexId = strArea.indexOf(this.getParent('span').get('data-id'));

                if (indexId > -1) {
                    strArea.splice(indexId, 1);
                    self.areaModelEl.set('value', strArea);
                }

                this.getParent('span').destroy();
                Cubix.OnlineBillingV4.Cart.updateCartPrice();
                $('spCart').fireEvent('cartChange');
            });
        });

        var arrAreas = [];
        if (this.areaModelEl.get('value')) {
            arrAreas.push(this.areaModelEl.get('value'));
        }

        arrAreas.push(areaArrEl[1]);

        this.areaModelEl.set('value', arrAreas);
        this.hideAreaSelector();

        Cubix.OnlineBillingV4.Cart.updateCartPrice();
        $('spCart').fireEvent('cartChange');
        this.areaAddBtn.removeClass('active');
        this.areaAddBtn.addClass('inactive');
        this.areaAddBtn.removeEvents();
    },

    showAreaSelector: function(vEl, cEl) {
        var self = this;

        // getting the date from model - if editing, otherwise creating new
        var d = (cEl && cEl.get('value')) ? new Date(cEl.get('value')) : new Date();

        // getting the veiw element to update html
        var relEl = vEl.getBoundingClientRect();
        if (relEl) {
            elTop = relEl.top;
            elLeft = relEl.left;
        }

        this.areaSelector.setStyle('top', elTop - 80);
        this.areaSelector.setStyle('left', elLeft - 25);
        this.areaSelector.show();
        Cubix.OnlineBillingV4.Packages.loadOverlay();

        // adding Event listerner to document, to close calendar when 'Esc' is pressed
        document.addEvent('keyup', function(e) {
            e = e.event;
            var key = e.keyCode || e.which;
            if (key == 27) {
                self.hideAreaSelector();
                $$('.act-type')[0].fireEvent('click');
            }
        });
    },

    hideAreaSelector: function() {
        this.areaSelector.hide();
        Cubix.OnlineBillingV4.Packages.unloadOverlay();
    },

    eventManager: function() {
        var self = this;

        this.areaSelCloseBtn.addEvent('click', function() {
            self.hideAreaSelector();
        });

        this.areaSelectFld.addEvent('change', function() {
            if (self.areaSelectFld.getSelected().get('data-id') != '') {
                self.enableAddBtn(true, [
                    self.areaSelectFld.getSelected().get('html'),
                    self.areaSelectFld.getSelected().get('data-id')
                ]);
            } else {
                self.enableAddBtn(false);
            }
        });
    },

    enableAddBtn: function(flag, areaArrEl) {
        var self = this;

        if (flag) {
            self.areaAddBtn.removeAttribute('class');
            self.areaAddBtn.addClass('active');
            self.areaAddBtn.removeEvents();
            self.areaAddBtn.addEvent('click', function() {
                self.addAction(areaArrEl);
            });

        } else {
            self.areaAddBtn.removeAttribute('class');
            self.areaAddBtn.addClass('inactive');
            self.areaAddBtn.removeEvents();
        }
    }
};

// Payments
Cubix.OnlineBillingV4.Payments = {
    // defaults

    // parametrs
    cardOptions: null,
    phoneOption: null,
    paymentTypes: null,
	cryptoSelected: false,
	
    // methods
    init: function() {
      this.setPayments();
      this.eventManager();
    },

    setPayments: function() {
        this.paymentTypes = $$('.paym-type');
        this.cardOptions = $$('.creditCartOpt');
        this.phoneOption = $('phoneOpt');
    },

    eventManager: function() {
        var self = this;

        // setting events for card option
        this.cardOptions.removeEvents();
        this.cardOptions.addEvent('click', function() {
            self.paymentTypes.each(function(el) {
                if (el.hasClass('checked')) {
                    el.removeClass('checked');
                }
            });

            this.addClass('checked');
			
			// BTC payment price increase logic 
			if(this.get('data-id') == 'coinpass'){
				self.cryptoSelected = true;
				$$('.chf-price').addClass('none');
				$$('.crypto-price').removeClass('none');
			}else{
				self.cryptoSelected = false;
				$$('.crypto-price').addClass('none');
				$$('.chf-price').removeClass('none');
			}
        });

        // setting events for phone option
        this.phoneOption.removeEvents();
        this.phoneOption.addEvent('click', function() {
            self.paymentTypes.each(function(el) {
                el.removeClass('checked');
            });

            this.addClass('checked');
			// BTC payment price increase logic
			self.cryptoSelected = false;
			$$('.crypto-price').addClass('none');
			$$('.chf-price').removeClass('none');
        });
    },
	
	generatePrices: function(price, cryptoPrice){
		var priceText = `<span class="chf-price ${Cubix.OnlineBillingV4.Payments.hideWrongPrice(false)}"> ${price} </span>`;
		var cryptoPriceText = `<span class="crypto-price ${Cubix.OnlineBillingV4.Payments.hideWrongPrice(true)}" > ${cryptoPrice} </span>`;
		return priceText + cryptoPriceText;
	},
	
	hideWrongPrice: function(is_crypto){
		var self = this;
		if(is_crypto === self.cryptoSelected){
			return '';
		}else{
			return 'none';
		}
	}
};

// Current Package
Cubix.OnlineBillingV4.CurrentPackage = {
    // defaults
    limit: 1,

    // parametrs
    currentPackageItem: null,
    cpExpirationDate: null,

    // methods
    init: function() {
      this.setCurrentPackageItem();
      this.eventManager();
    },

    setCurrentPackageItem: function() {
        this.currentPackageItem = $$('.cart-item-gotd')[0];
    },

    eventManager: function() {
        var self = this;
        var addGotdLnk = this.currentPackageItem.getElements('.sp-add-gotd')[0];

        addGotdLnk.addEvent('click', function() {
          var curDate = new Date();
          var expirationDate = new Date(parseInt(self.currentPackageItem.getElements('.package-expiration-date')[0].get('value')));

          Cubix.OnlineBillingV4.GotdCalendar.gotdObjEl = {
            gotdViewEl: this.getParent('td'),
            gotdModelEl: this.getSiblings('.package-gotd-object')[0]
          };

          Cubix.OnlineBillingV4.GotdCalendar.setDateRange([curDate, expirationDate]);
          Cubix.OnlineBillingV4.GotdCalendar.construct();
          Cubix.OnlineBillingV4.GotdCalendar.showCalendar(this);
        });

        var addVotdLnk = this.currentPackageItem.getElements('.sp-add-votd')[0];
        addVotdLnk.addEvent('click', function() {
          var curDate = new Date();
          var expirationDate = new Date(parseInt(self.currentPackageItem.getElements('.package-expiration-date')[0].get('value')));

          Cubix.OnlineBillingV4.VotdCalendar.votdObjEl = {
            votdViewEl: this.getParent('td'),
            votdModelEl: this.getSiblings('.package-votd-object')[0]
          };

          Cubix.OnlineBillingV4.VotdCalendar.setDateRange([curDate, expirationDate]);
          Cubix.OnlineBillingV4.VotdCalendar.construct();
          Cubix.OnlineBillingV4.VotdCalendar.showCalendar(this);
        })
    }
};

// Agency
Cubix.OnlineBillingV4.Agency = {
  escortItms: null,
  selectedItm: null,
  sortingItm: null,
  selectedEscortItm: null,
  sortingOpts: [],
  inCartEscorts: [],

  init: function() {
    var self = this;

    this.setElements();

    this.escortItms.addEvent('click', function() {
      self.selectItm(this);
    });

    if($$('.gotd-girl-overlay')){
      this.setGotdOverlay();
    }

    this.addGotdBtn.addEvent('click', function(e) {
      e.stop();
      self.selectGotd(this);
    });

    this.addVotdBtn.addEvent('click', function(e) {
      e.stop();
      self.selectVotd(this);
    });

    this.sortingItm.addEvent('change', function() {
      self.setSorting(this);
    });
  },

  setElements: function() {
    this.escortItms = $$('.escort-item');
    this.sortingItm = $('sorting');
    this.sortingOpts = $('sorting').getElements('option');
    this.loader = $$('.loader')[0];
    this.selectedEscortItm = $('selectedEscort');
    this.addGotdBtn = $$('.add-gotd-btn');
    this.addVotdBtn = $$('.add-votd-btn');
    this.isRequestGotd = $('isRequestGotd').get('value');
    this.isRequestVotd = $('isRequestVotd').get('value');
  },

  setGotdOverlay: function(){
    $$('.gotd-girl-overlay').addEvent('click', function(){
      return false;
    });
  },

  selectGotd: function(el) {
    var packageEl = el.getParent('.escort-item').getElement('.cur-escort-packages');
    Cubix.OnlineBillingV4.Cart.addGotdToCart(packageEl);
    el.removeEvents();
    el.hide();
  },

  selectVotd: function(el) {
    var packageEl = el.getParent('.escort-item').getElement('.cur-escort-packages');
    Cubix.OnlineBillingV4.Cart.addVotdToCart(packageEl);
    el.removeEvents();
    el.hide();
  },

  selectItm: function(el) {
    if (el.getParent('.escort-list').getSiblings('.err-msg')[0]) {
        el.getParent('.escort-list').getSiblings('.err-msg')[0].destroy();
    }

    if (this.selectedItm) {
      this.selectedItm.removeClass('selected');
    }

    el.addClass('selected');
    this.selectedItm = el;

    var curEscortPackagesObj = JSON.decode(decodeURIComponent(el.getElement('.cur-escort-packages').get('value')));

    if (Object.getLength(curEscortPackagesObj) > 0) {
      $$('.sp-act-type-button')[1].hide();
      $('activationDate').hide();
      $('immediateActivation').set('html', Cubix.OnlineBillingV4.Lang.afterExpire);
    } else {
      $$('.sp-act-type-button')[1].show();
      $('activationDate').show();
      $('immediateActivation').set('html', Cubix.OnlineBillingV4.Lang.immediately);
    }

    this.selectedEscortItm.set('value', JSON.encode({
      'id': el.get('data-id'),
      'showname': el.getElement('.showname').get('html'),
      'baseCityId': el.get('data-city-id'),
      'curPackage': curEscortPackagesObj,
      'hasApprovedVideo' : el.get('data-has-approved-video')
    }));
  },

  setSorting: function(el) {
      this.sortEscorts(el.getSelected()[0].get('id'));
  },

  sortEscorts: function(sorting) {
      var self = this;

      var myRequest = new Request({
          data: {
              ajax: 1,
              sort: sorting
          },
          url: '/online-billing-v4/select-escort',
          method: 'get',

          onRequest: function() {
              self.loader.addClass('visible');
          },

          onSuccess: function(responseText) {
              var escortList = $$('.escort-list')[0],
                  inCartEscorts;
              if ($('inCartEscorts').get('value')) {
                  inCartEscorts = JSON.decode($('inCartEscorts').get('value'));
              } else {
                  inCartEscorts = [];
              }

              escortList.set('html', '');

              var resp = JSON.decode(responseText),
                  escort = null,
                  inCartBool = false;

              for (var i = 0; i < resp.length; i++) {
                  escort = resp[i];

                  if (+self.isRequestGotd && !escort.escort_packages.length) continue;
                  if (+self.isRequestVotd && (!escort.escort_packages.length || !escort.has_approved_video) )continue;

                  for (var j = inCartEscorts.length - 1; j >= 0; j--) {
                      if (parseInt(escort.id) == parseInt(JSON.decode(inCartEscorts[j]).id)) {
                          inCartBool = true;
                      }
                  }
                  var divEscortItem = new Element('div', {
                      'class': 'escort-item ' + ((i >= +Cubix.OnlineBillingV4.EscortsPaging.perPage && !+self.isRequestGotd && !+self.isRequestVotd) ? 'hidden' : ''),
                      'data-id': escort.id,
                      'data-city-id': escort.city_id
                  });

                  if (escort.escort_packages.lenght) {
                      divEscortItem.addClass('active');
                  }

                  if (inCartBool) {
                      divEscortItem.addClass('in-cart');
                  }

                  var divImg = new Element('div', {
                      'class': 'img',
                      'html': '<img src="' + escort.photo +
                          '" alt="escort-photo">'
                  });

                  var aCheckbox = new Element('a', {
                      'class': 'escort-checkbox',
                      'href': 'javascript:void(0)'
                  });

                  if (+self.isRequestGotd || +self.isRequestVotd){
                    var gotdGirlOverlay = new Element('div', {
                      'class': 'gotd-girl-overlay'
                    });

                    gotdGirlOverlay.inject(divImg);
                  }

                  if (escort.escort_packages.length && !(+self.isRequestVotd) ){
                      var divGotdBtn = new Element('div', {
                          'class': 'add-gotd-btn',
                          'html': Cubix.OnlineBillingV4.Lang.addGotd,
                          'style': !!self.isRequestGotd ? (($$('.gotd-item').get('data-escort').indexOf(escort.id + []) == -1) ? 
                                  '' : 'display: none') : ''

                      });

                      divGotdBtn.inject(divImg);
                  }
                  if (escort.escort_packages.length && escort.has_approved_video && !(+self.isRequestGotd) ){
                      var divGotdBtn = new Element('div', {
                          'class': 'add-votd-btn',
                          'html': Cubix.OnlineBillingV4.Lang.addVotd,
                          'style': !!self.isRequestVotd ? (($$('.votd-item').get('data-escort').indexOf(escort.id + []) == -1) ? 
                                  '' : 'display: none') : ''

                      });

                      divGotdBtn.inject(divImg);
                  }

                  aCheckbox.inject(divImg);

                  var divInfo = new Element('div', {
                      'class': 'info-1',
                      'html': '<span class="showname">' + escort.showname + '</span>' +
                          '<span class="escort-id">Id: #' + escort.id + '</span>' +
                          '<span class="escort-package">' + ((escort.escort_packages.length) ? escort.escort_packages[0].package_name : '') +
                          '</span>' +
                          '<input type="hidden" class="cur-escort-packages" value=' + encodeURIComponent(JSON.encode(escort.escort_packages)) + '>'
                  });


                  new Elements([divImg, divInfo]).inject(divEscortItem);
                  divEscortItem.inject(escortList);
                  inCartBool = false;
              }

              self.escortItms = $$('.escort-item');
              // self.sortingItm.removeEvents();

              self.escortItms.addEvent('click', function() {
                  self.selectItm(this);
              });

              self.setGotdOverlay();

              this.addGotdBtn = $$('.add-gotd-btn');

              this.addVotdBtn = $$('.add-votd-btn');

              this.addGotdBtn.addEvent('click', function(e) {
                e.stop();
                self.selectGotd(this);
              });

              this.addVotdBtn.addEvent('click', function(e) {
                e.stop();
                self.selectVotd(this);
              });

              self.loader.removeClass('visible');

              Cubix.OnlineBillingV4.LiveSearch.setElements();
          }
      }).send();
  }
};

// Live search in agency escorts
Cubix.OnlineBillingV4.LiveSearch = {
    searchFld: null,
    escortItms: [],
    regex: '',

    init: function() {
      this.setElements();
      this.liveSearch();
    },

    setElements: function() {
        this.searchFld = $('searchEscortByName');
        this.escortItms = $$('.escort-item');
    },

    liveSearch: function() {
        var self = this;

        self.searchFld.addEvent('keyup', function() {
            self.regex = self.searchFld.get('value').toLowerCase();

            if (self.searchFld.get('value').length > 2) {
                var showname = '';

                self.escortItms.each(function(el) {
                    if (el.hasClass('hidden')) {
                        el.removeClass('hidden');
                    }
                });

                self.escortItms.each(function(el) {
                    showname = el.getElements('.showname').get('html')[0].replace(/\s/g, '').toLowerCase();

                    if (!showname.match(self.regex)) {
                        el.addClass('hidden');
                    }
                })
            } else {
                self.escortItms.each(function(el) {
                    if (el.hasClass('hidden')) {
                        el.removeClass('hidden');
                    }
                });
            }
        });
    }
};

// Checkout
Cubix.OnlineBillingV4.Checkout = {
    // parametrs
    cart: null,
    checkoutBtn: null,
    checkoutBtnBlock: null,
    dayPassesPackages: [19, 20, 23],

    // methods
    init: function() {
      Cubix.OnlineBillingV4.Cart.paymentMethodsOverlay.disable();
      this.setCheckout();
      this.addToDb();
      this.toggleCheckoutBtn();
      this.getCartChange();
    },

    setCheckout: function() {
      this.cart = $('spCart');
      this.checkoutBtn = $('spCheckout');
      this.checkoutBtnBlock = $('payment-checkout-block');
    },

    getCartChange: function() {
      var self = this;

      this.cart.addEvent('cartChange', function() {
        var packages = $$('.package-id').get('value');
        var gotds = $$('.package-gotd-object').get('value');
        var votds = $$('.package-votd-object').get('value');
        var additionalAreas = $$('.area-object').get('value');

        var allowPhonePayment = self.checkPhonePayment(packages, gotds, votds, additionalAreas);
        self.togglePhoneBtn(allowPhonePayment);
        self.toggleCheckoutBtn(packages);
      });
    },

    checkPhonePayment: function(packages, gotds,votds, additionalAreas) {
      if (packages.length == 1) {
        if ((this.dayPassesPackages.indexOf(+packages[0]) > -1 && (+gotds[0] <= 1) &&
          +additionalAreas[0].split(',') == 0) || (!+packages[0] && gotds[0] &&
          Object.values(JSON.decode(gotds[0])[0])[0].split(',').length == 1)) {
          return true;
        }
        if ((this.dayPassesPackages.indexOf(+packages[0]) > -1 && (+votds[0] <= 1) &&
          +additionalAreas[0].split(',') == 0) || (!+packages[0] && votds[0] &&
          Object.values(JSON.decode(votds[0])[0])[0].split(',').length == 1)) {
          return true;
        }
        return false;
      }
      return false;
    },

    addToDb: function() {
        this.checkoutBtn.addEvent('click', function() {
            if ($('yold_18') && $$('input[name=yold_18]:checked').length > 0) {
            var params_raw = {},
                params = {},
                cartItems = $('spCartBody').getElements('.cart-item'),
                escort_id,
                package_id,
                activation_date,
                gotd,
                area_ids,
                tempArr = [];
                tempArr1 = [];
                payment_type = $$('.payment-type-block')[0].getElement('.paym-type.checked').get('data-id'),
                payment_method = payment_type;

            cartItems.each(function(el, index) {
                escort_id = Number(el.getElement('.escort-id').get('value'));
                package_id = Number(el.getElement('.package-id').get('value'));
                activation_date = el.getElement('.package-activation-date').get('value') / 1000 || '';
                gotd = [];
                if(el.getElement('.package-gotd-object')){
                    gotd = JSON.decode(el.getElement('.package-gotd-object').get('value')) || [];
                }
                votd = [];
                if(el.getElement('.package-votd-object')){
                    votd = JSON.decode(el.getElement('.package-votd-object').get('value')) || [];
                }

                if (gotd) {
                  gotd.each(function(el) {
                    tempArr = [];

                    el[Object.keys(el)[0]].split(',').each(function(item) {
                        //tempArr.push(Date.parse(item) / 1000);
						            tempArr.push(item);
                    });

                    el[Object.keys(el)[0]] = tempArr;
                  });
                }

                if (votd) {
                  votd.each(function(el) {
                    tempArr1 = [];

                    el[Object.keys(el)[0]].split(',').each(function(item) {
                        //tempArr.push(Date.parse(item) / 1000);
                        tempArr1.push(item);
                    });

                    el[Object.keys(el)[0]] = tempArr1;
                  });
                }

                area_ids = el.getElement('.area-object').get('value');
                area_ids = (area_ids && area_ids.length > 0) ? area_ids.split(',') : false;
                params_raw = {
                    'escort_id': escort_id,
                    'additional_regions': area_ids || [],
                    'gotd_days': gotd || [],
                    'votd_days': votd || [],
                    'package_id': package_id,
                    'activation_date': activation_date
                };

                params[index] = params_raw;
            });

            params.payment_method = payment_method;
            var myRequest = new Request({
                data: params,
                url: '/online-billing-v4/generate-cart-item',
                method: 'post',

                onRequest: function() {
                    $$('.big-loader').addClass('visible');
                },

                onSuccess: function(responseText) {
                    $$('.big-loader').removeClass('visible');

                    var resp = JSON.decode(responseText);

                    if (params.payment_method === 'twispay' || params.payment_method === 'postfinance' || params.payment_method === 'coinpass') {
                        var form = $('formPlaceholder');
                        form.set('html', resp.form);
                        form.getElement('input[type="submit"]').click();
                    } else if (params.payment_method === 'epg' || params.payment_method === 'faxin') {
                        if (resp.status == 1) {
                            window.location.href = resp.url;
                        }else{
                            alert(resp.error);
                        }
                    }else {
                        window.location.href = resp.url;
                    }
                }
            }).send();
            }else{
                $('18-yold-label').addClass('shaker');
                setTimeout(function(){ $('18-yold-label').removeClass('shaker'); }, 1000);
            }

        });
    },

    togglePhoneBtn: function(allowPhonePayment) {
        if (!allowPhonePayment) {
            $('phoneOpt').getParent('.sp-paym-type-button').set('style', 'display: none');
        } else {
            $('phoneOpt').getParent('.sp-paym-type-button').set('style', 'display: inline-block');
        }
    },

    toggleCheckoutBtn: function(packages) {
        if (packages) {}

        if (packages && packages.length > 0) {
            this.checkoutBtnBlock.set('style', 'display: flex');
            this.checkoutBtn.set('style', 'display: inline-block');
        } else {
            this.checkoutBtnBlock.set('style', 'display: none');
            this.checkoutBtn.set('style', 'display: none');
        }
    }
};

// Agency Escorts Paging
Cubix.OnlineBillingV4.EscortsPaging = {
  // defaults
  perPage: 25,
  pagingWrapper: null,
  escortsCount: 0,
  curPage: 1,

  init: function(){
    this.setParams();
    this.getEscortsCount();

    var pageCount = this.calcPageCount();
    this.createPagingBtns(pageCount);
  },

  getEscortsCount: function(){
    this.escortsCount = $$('#escortsWrapper .escort-item').length;
  },

  setParams: function(){
    this.pagingWrapper = $$('.page-wrapper')[0];
  },

  createPagingBtns: function(pageCount){
    if (pageCount <= 2) return;
    var self = this;

    var previousBtn = new Element('a', {
      class: 'previous-btn none',
      html: Cubix.OnlineBillingV4.Lang.previous,
      href: 'javascript:void(0)'
    });

    previousBtn.addEvent('click', function(){
        self.goPreviousPage();
    });

    this.pagingWrapper.grab(previousBtn);

    for (var i = 0; i < pageCount; i++) {
      var navEl = new Element('a', {
        class: 'page-btn' + ((i == 0) ? ' selected' : ''),
        html: (+i + 1),
        'data-page': (+i + 1),
        href: 'javascript:void(0)'
      });

      this.pagingWrapper.grab(navEl);
    }

    var nextBtn = new Element('a', {
      class: 'next-btn' + ((pageCount == 1) ? ' none' : ''),
      html: Cubix.OnlineBillingV4.Lang.next,
      href: 'javascript:void(0)'
    });

    nextBtn.addEvent('click', function(){
      self.goNextPage();
    });

    this.pagingWrapper.grab(nextBtn);

    this.pagingWrapper.getElements('.page-btn').each(function(el){
      el.addEvent('click', function(){
        self.goToPage(el.get('data-page'));
      });
    });
  },

  goPreviousPage: function(){
    var self = this;

    this.hideAllEscorts();

    var allEscorts = $$('#escortsWrapper .escort-item');
    var escortEls = allEscorts.filter(function(item, index){
      return (index <= ((self.curPage - 1) * self.perPage - 1)) && (index > ((self.curPage - 2) * +self.perPage - 1));
    });

    this.showEscorts(escortEls);
    this.setCurPage(this.curPage - 1);
    this.togglePreviousNextBtns();
  },

  goNextPage: function(){
    var self = this;

    this.hideAllEscorts();

    var allEscorts = $$('#escortsWrapper .escort-item');

    var escortEls = allEscorts.filter(function(item, index){
      return (index > ((self.curPage) * self.perPage - 1)) && (index <= ((self.curPage + 1) * +self.perPage - 1));
    });

    this.showEscorts(escortEls);
    this.setCurPage(+self.curPage + 1);
    this.togglePreviousNextBtns();
  },

  goToPage: function(page){
    var self = this;

    this.hideAllEscorts();

    var allEscorts = $$('#escortsWrapper .escort-item');

    var escortEls = allEscorts.filter(function(item, index){
      return (index > ((page - 1) * self.perPage - 1)) && (index <= (page * +self.perPage - 1));
    });

    this.showEscorts(escortEls);
    this.setCurPage(page);
    this.togglePreviousNextBtns();
  },

  hideAllEscorts: function(){
    var allEscorts = $$('#escortsWrapper .escort-item');

    allEscorts.each(function(el){
      if (!el.hasClass('hidden')) {
        el.addClass('hidden');
      }
      if (el.hasClass('selected')) {
        el.removeClass('selected');
      }
    });
  },

  showEscorts: function(escortEls){
    escortEls.each(function(el){
      if (el.hasClass('hidden')){
        el.removeClass('hidden');
      }
    })
  },

  calcPageCount: function(){
    var pageCount = Math.ceil(this.escortsCount / this.perPage);
    return pageCount;
  },

  setCurPage: function(page){
    this.curPage = page;

    var pages = $$('.page-wrapper .page-btn');
    pages.each(function(el){
      if(el.hasClass('selected')){
        el.removeClass('selected');
      }

      if (el.get('data-page') == page){
        el.addClass('selected');
      }

    })
  },

  togglePreviousNextBtns: function(){
    var previousBtn = $$('.previous-btn')[0],
        nextBtn = $$('.next-btn')[0];

    if (this.curPage == 1){
      if (!previousBtn.hasClass('none')){
        previousBtn.addClass('none');
      }
    } else {
      if (previousBtn.hasClass('none')){
        previousBtn.removeClass('none');
      }
    }

    if (this.curPage == this.calcPageCount()){
      if (!nextBtn.hasClass('none')){
        nextBtn.addClass('none');
      }
    } else {
      if (nextBtn.hasClass('none')){
        nextBtn.removeClass('none');
      }
    }
  }
}
