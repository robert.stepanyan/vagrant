window.addEvent('domready', function () {
	Cubix.Payment.InitEvents();
	Cubix.Payment.loader = new Cubix.Overlay($$('.payment-container')[0], {loader: _st('loader-small.gif'), position: '50%', no_relative: true});
});

Cubix.Payment = {};
Cubix.Payment.loader;
Cubix.Payment.InitEvents = function() {
	$$('.package-row .tooltip').addEvent('mouseover', function(e) {
		e.target.getElements('.body').removeClass('none')
		.addEvent('mouseleave', function(b) {
			e.target.getElements('.body').addClass('none');
		});
	})
	
	$$('.package-row').addEvent('click', function(e) {
		if ( this.getElement('.chk').hasClass('checked') ) {
			this.getElements('.chk').removeClass('checked');
		} else {
			$$('.package-row .chk').removeClass('checked');
			this.getElements('.chk').addClass('checked');
		}
		
		if ( $$('.package-row .chk.checked').length ) {
			$('package-id').set('value', $$('.package-row .chk.checked')[0].getParent('li').get('data-id'));
			$$('#order-summary .escort-sedcard').set('html',$$('.package-row .chk.checked')[0].getNext('.info').getElement('.name').get('html'));
		} else {
			$('package-id').set('value', '');
			$$('#order-summary .escort-sedcard').set('html', '');
		}
		
		if ( this.getParent('.package-box').hasClass('day-passes') ) {
			$$('.payment-method.phone').removeClass('disabled');
		} else {
			$$('.payment-method.phone').addClass('disabled');
		}
		
		Cubix.Payment.summarize();
		
	});
	
	$$('.package-row.gotd').addEvent('click', function(e) {
		if ( this.getElement('.gotd-chk').hasClass('checked') ) {
			this.getElements('.gotd-chk').removeClass('checked');
		} else {
			this.getElements('.gotd-chk').addClass('checked');
		}
	});
	
	$$('#escort-select select').addEvent('change', function()  {
		Cubix.Payment.resetForm();
		
		
		if ( $$('#escort-select select :selected') && $$('#escort-select select')[0].get('value') ) {
			$$('#order-summary .escort-name').set('html', $$('#escort-select select :selected')[0].get('html'));
		} else {
			$$('#order-summary .escort-name').set('html', '-');
		}
		
		Cubix.Payment.summarize();
	});
	
	$$('#city-select .add-btn').addEvent('click', function(e) {
		var sel = $$('#city-select select')[0];
		if ( ! sel || ! sel.get('value') ) return;
		
		var id = sel.get('value');
		
		if ( $$('#city-select li[data-id="' + id + '"]').length ) {
			return;
		}
		
		var li = new Element('li', {
			'data-id': id
		});
		new Element('input', {
			type: 'hidden',
			value: id,
			name: 'additional_area[]'
		}).inject(li);
		new Element('div', {
			'class': 'remove'
		}).addEvent('click', function(e) {
			this.getParent('li').destroy();
			Cubix.Payment.summarize();
		}).inject(li);
		var name = sel.getElement(':selected').get('html');
		li.appendText(name);
		li.inject($$('#city-select ul.item-select')[0]);
		
		
		Cubix.Payment.summarize();
	});
	
	$$('#city-select input[name="add_region"]').addEvent('change', function(e) {
		if ( this.checked ) {
			$$('#city-select select').set('disabled', '').setStyle('opacity', 1);
		} else {
			$$('#city-select select').set('disabled', 'disabled').set('value', '').setStyle('opacity', 0.5);
			$$('#city-select .item-select')[0].empty();
		}
	});
	
	
	$$('.payment-method .chk').addEvent('click', function(e) {
		if ( this.getParent('.payment-method').hasClass('disabled') ) return;
		$$('.payment-method .chk').removeClass('checked');
		this.addClass('checked');
		
		if ( this.getParent('.payment-method').hasClass('phone') ) {
			$('payment-method').set('value', 'phone');
		} else {
			$('payment-method').set('value', 'card');
		}
	});
	
	$$('.payment-container form').set('send', {
		method: 'post',
		onSuccess: function(response) {
			response = JSON.decode(response);
			
			$$('.payment-container span.error').set('html', '');
			if ( response.status == 'error' ) {
				for(name in response.msgs) {
					$$('.payment-container .error-' + name)[0].set('html', response.msgs[name]);
				}
			} else {
				window.location = response.url; return;
			}
			
			Cubix.Payment.loader.enable();
		}
	});
	
	$('select-dates-btn').addEvent('click', function(e) {
		if ( ! $$('#gotd-block select')[0].get('value') ) {
			alert('Select region first');
			return;
		}
		
		$('gotd-popup').removeClass('none');
	});
	
	$$('#gotd-popup .cancel-btn').addEvent('click', function(e) {
		$('gotd-popup').addClass('none');
	});
	
	$$('#gotd-popup .ok-btn').addEvent('click', function(e) {
		$('gotd-popup').addClass('none');
	});
	
	$('buy-now-btn').addEvent('click', function() {
		Cubix.Payment.loader.disable();
		$$('.payment-container form').send();
	});
}

Cubix.Payment.resetForm = function() {
	$$('#order-summary .escort-name, #order-summary .escort-sedcard').set('html', '');
	$$('.package-row .chk, .package-row .gotd-chk').removeClass('checked');
	$('package-id').set('value', '');
	$$('#city-select li').destroy();
	$$('#city-select select').set('disabled', 'disabled').set('value', '').setStyle('opacity', '0.5');
	$$('#city-select .item-select')[0].empty();
	$$('#city-select input[name="add_region"]')[0].checked = false;
	
	$$('.payment-method.phone').addClass('disabled')
	$$('.payment-method.phone .chk').removeClass('checked');
	$$('.payment-method.credit-card .chk').addClass('checked');
	$('payment-method').set('value', 'card');
}

Cubix.Payment.summarize = function() {
	var totalPrice = 0;
	if ( $$('.package-row .chk.checked').length ) {
		totalPrice = $$('.package-row .chk.checked')[0].getParent('li').get('data-price').toInt();
	}
	
	if ( $$('#city-select [name="add_region"]')[0].checked ) {
		totalPrice += ($('add-area-price').get('value') * $$('#city-select input[name="additional_area[]"]').length).toInt();
	}
	
	$$('#total-amount .amount').set('html', totalPrice.toFixed(2) + ' CHF');
	
	$$('#total-amount .amount').highlight('#f8e7e7');
}