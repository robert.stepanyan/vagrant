Cubix.OnlineBillingV4 = {};

// Packages
Cubix.OnlineBillingV4.Packages = {

    // defaults

    pageBody: null,
    overlay: null,

    // parametrs

    packagesWrapper: null,
    immediateActivationOpt: null,
    specDateActivationOpt: null,
    specDateActivationFld: null,
    selectedPackage: null,
    selectedDate: null,

    // methods

    init: function() {
      this.setPackagesEls();
      this.eventManager();
    },

    setPackagesEls: function() {
        this.packagesWrapper = $('packagesWrapper');
        this.immediateActivationOpt = $('immediateActivation');
        this.specDateActivationOpt = $('specDateActivation');
        this.specDateActivationFld = $('activationDate');
        this.selectedPackage = $('selectedPackage');
        this.selectedDate = $('selectedDate');

        // body & overlay
        this.pageBody = $(document.body);
        this.overlay = $('calOverlay');
    },

    eventManager: function() {
        var self = this;
        var packageId, packagePrice, packageDuration;

        self.packagesWrapper.getElements('.package-block').each(function(el) {
            el.addEvent('click', function() {
                packageId = el.get('data-id');
                packagePrice = el.get('data-price');
				packageCryptoPrice = el.get('data-crypto-price');
                packageName = el.get('data-name');
                packageDuration = el.get('data-duration');

                self.clearErrors();
                self.setPackage(packageId, packagePrice, packageCryptoPrice, packageDuration, packageName);
                self.setSelectedView(el);
            });
        });

        self.immediateActivationOpt.addEvent('click', function() {
            self.selectedDate.set('value', '');
			
			if (self.specDateActivationOpt) {
				self.specDateActivationFld.set('value', '');
			}
            
            if (!self.immediateActivationOpt.hasClass('checked')) {
                $$('.act-type').each(function(el) {
                    if (el.hasClass('checked')) {
                        el.removeClass('checked');
                    }
                });
            }

            self.immediateActivationOpt.addClass('checked');
        });
        if (self.specDateActivationOpt) {
            self.specDateActivationOpt.addEvent('click', function() {

                var d = null;

                if (self.selectedDate.get('value')) {
                    d = new Date(1970, 0, 1);
                    d.setTime(self.selectedDate.get('value'));
                    Cubix.OnlineBillingV4.PackageCalendar.packageStartDate.set('value', d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate());
                }

                Cubix.OnlineBillingV4.PackageCalendar.construct(false, false, d);
                Cubix.OnlineBillingV4.PackageCalendar.showCalendar(self.specDateActivationFld);
            });
        }
    },

    setPackage: function(id, price, cryptoPrice, duration, name) {
        this.selectedPackage.set('value', JSON.encode({
            "id": id,
            "price": price,
			"crypto_price": cryptoPrice,
            "duration": duration,
            "name": name
        }));
    },

    setSelectedView: function(packageEl) {
        self.packagesWrapper.getElements('.package-block').each(function(el) {
            el.removeClass('selected');
        });

        packageEl.addClass('selected');
    },

    clearErrors: function() {
        var errors = this.packagesWrapper.getElements('.err-msg');

        if (errors && errors.length > 0) {
            errors.each(function(el) {
                el.fade('out').dispose();
            });
        }
    },

    loadOverlay: function() {
        this.overlay.fade('in');
        this.pageBody.addClass('no-scroll');
    },

    unloadOverlay: function() {
        this.overlay.fade('out');
        this.pageBody.removeClass('no-scroll');
    }
};

// Cart
Cubix.OnlineBillingV4.Cart = {
    // defaults
    indepEscortLimit: 1,

    // parameters
    selectedPackage: null,
    addToCartBtn: null,
    packagesWrapper: null,
    selectedDate: null,
    activationDate: null,
    cartContent: null,
    curPackageExpirationDate: 0,

    packageOverlay: null,
    activationOverlay: null,
    paymentMethodsOverlay: null,

    // methods
    init: function() {
      this.setCartEls();
      this.eventManager();
    },

    setCartEls: function() {
      this.selectedDate = $('selectedDate');
      this.addToCartBtn = $('addToCart');
      this.packagesWrapper = $('packagesWrapper');
      this.selectedPackage = $('selectedPackage');
      this.activationDate = $('activationDate');
      this.cartContent = $('cartContent');
      this.packageOverlay = new Cubix.Overlay(this.packagesWrapper, { has_loader: false });

      this.paymentMethodsOverlay = new Cubix.Overlay($$('.payment-type-block')[0], { has_loader: false });

      this.activationOverlay = new Cubix.Overlay($$('.activation-type-block')[0], { has_loader: false });

      if ($$('.package-expiration-date')[0]) {
        this.curPackageExpirationDate = parseInt($$('.package-expiration-date')[0].get('value'));
      }
    },

    eventManager: function() {
        var self = this;
        self.addToCartBtn.removeEvents();
        self.addToCartBtn.addEvent('click', function() { 
            if (!self.packageValidator()) {
                if ($$('.err-msg').length == 0) {
                    self.createErrTooltip(self.packagesWrapper, Cubix.OnlineBillingV4.Lang.errs.selectPackage);
                } else {
                    packagesWrapper.highlight();
                }
            } else {
                self.addPackageToCart();
            }
        });
    },

    packageValidator: function() {
        return this.selectedPackage.get('value') ? true : false;
    },

    createErrTooltip: function(el, msg) {
        var x = el.getPosition().x;
        var y = el.getPosition().y;

        var errMsg = new Element('span', {
            'class': 'err-msg',
            'html': msg,
            'styles': {
                'top': 0,
                'left': '50%',
                'transform': 'translate(-50%,-100%)'
            }
        });

        new Fx.Scroll(window).toElementCenter(el);

        el.adopt(errMsg);
        errMsg.fade('in');
        errMsg.setStyle('margin-top', -10);
    },

    addPackageToCart: function() {

        var self = this;
        var packageObj = JSON.parse(this.selectedPackage.get('value'));
        var packageDate = this.selectedDate.get('value');
        var secondsCount = packageDate;
        var d;
		
        if (!Boolean(packageDate)) { 
            if (typeof CurrentPackage !== 'undefined' && CurrentPackage[0].expiration_date) {
                d = new Date(1970, 0, 1);
                d.setTime(CurrentPackage[0].expiration_date * 1000);
                packageDate = d.getFullYear() + '-' + ('0' + (parseInt(d.getMonth()) + 1)).slice(-2) + '-' + ('0' + d.getDate()).slice(-2);
            } else {
                d = new Date();
                packageDate = d.getFullYear() + '-' + ('0' + (parseInt(d.getMonth()) + 1)).slice(-2) + '-' + ('0' + d.getDate()).slice(-2) + ' ' + ('0' + d.getHours()).slice(-2) + ':' + ('0' + d.getMinutes()).slice(-2);
			}
        } else { 
            d = new Date(1970, 0, 1);
            d.setTime(packageDate);
            packageDate = d.getFullYear() + '-' + ('0' + (parseInt(d.getMonth()) + 1)).slice(-2) + '-' + ('0' + d.getDate()).slice(-2) + ' ' + ('0' + d.getHours()).slice(-2) + ':' + ('0' + d.getMinutes()).slice(-2);
            secondsCount = d.getTime();
        }
		
        if (self.selectedPackage.get('value')) {

            //free 5 days no checkout
            if( packageObj.price == '00.00' ){
                $$('.payment_methods-container').setStyle('opacity', 0.1);
            }else{
                $$('.payment_methods-container').setStyle('opacity', 1);
            }

            var cart = $('spCartBody');
            var cartRow = new Element('tr', {
                html: '<td colspan="5"></td>',
                class: 'cart-item package-item'
            }).inject(cart);
            var table = new Element('table', {
                'class': 'sp-package-table'
            });
            var tr1 = new Element('tr', {
                'class': 'sp-package-details'
            });
            var td11 = new Element('td', {
              html: '<span class="sp-delete-symbol">&#215;</span>' +
                    '<span>' + packageObj.name + '</span>' +
                    '<input type="hidden" class="package-duration" value="' + packageObj.duration + '">' +
                    '<input type="hidden" class="package-price" value="' + packageObj.price + '">' +
					'<input type="hidden" class="package-crypto-price" value="' + packageObj.crypto_price + '">' +
                    '<input type="hidden" class="package-id" value="' + packageObj.id + '">'
            });
            var td12 = new Element('td', {
              html: '<span>' + packageDate + '</span> ' +
                    (typeof CurrentPackage !== 'undefined' ? '' : ('<span class="sp-edit">' + Cubix.OnlineBillingV4.Lang.edit + '</span>')) +
                    '<input class="package-activation-date" type="hidden" value="' + secondsCount + '">'
            });
            var td13 = new Element('td', {
              html: '<span class="sp-add-gotd">'+ Cubix.OnlineBillingV4.Lang.add +'</span>' +
                    '<input class="package-gotd-object" type="hidden">'
            });
            var hasApprovedVideo = $('hasApprovedVideo').get('value');
            if(hasApprovedVideo == 1){

                var td14 = new Element('td', {
                  html: '<span class="sp-add-votd">'+ Cubix.OnlineBillingV4.Lang.add +'</span>' +
                        '<input class="package-votd-object" type="hidden">'
                });
            } else{
                var td14 = new Element('td', {
                  html: '&nbsp; N/A' +
                          '<input class="package-votd-object" type="hidden">'
                });
            }
            var td15 = new Element('td', {
                html: ((packageObj.id == 24) ?
                ('<span class="sp-add-area">' + Cubix.OnlineBillingV4.Lang.add + '</span>'):
                ('&nbsp; N/A')) +
                '<input class="area-object" type="hidden">'
            });
            var td16 = new Element('td', {
                html: Cubix.OnlineBillingV4.Payments.generatePrices(packageObj.price, packageObj.crypto_price) + ' CHF ='
            });
            var trs = new Elements([td11, td12, td13, td14, td15,td16]).inject(tr1);
            var tr2 = new Element('tr', {
                'class': 'sp-package-result'
            });
            var td21 = new Element('td', {
                html: Cubix.OnlineBillingV4.Payments.generatePrices(packageObj.price, packageObj.crypto_price) + ' CHF'
            });
            var td22 = new Element('td');
            var td23 = new Element('td');
            var td24 = new Element('td');
            var td25 = new Element('td');
            var td26 = new Element('td', {
                html: Cubix.OnlineBillingV4.Payments.generatePrices(packageObj.price, packageObj.crypto_price) + ' CHF',
                class: 'table-total'
            });

            $$('.sp-gt-desc')[0].set('html', Cubix.OnlineBillingV4.Payments.generatePrices(packageObj.price, packageObj.crypto_price) + ' CHF');

            trs = new Elements([td21, td22, td23, td24, td25, td26]).inject(tr2);
            trs = new Elements([tr1, tr2]).inject(table);
            cart.getLast('tr').getLast('td').adopt(table);
            table.highlight('#f0f3f9');

            if (cart.getElements('.package-item').length >= this.indepEscortLimit) {
                this.addToCartBtn.removeEvents();
                this.addToCartBtn.addClass('inactive');

                this.activationOverlay.disable();
                this.packageOverlay.disable();
            }

            $('spCart').fireEvent('cartChange');

            // Adds deleting event on the cart items
            $$('.package-item').getElements('.sp-delete-symbol').each(function(el) {
                el.addEvent('click', function() {
                    this.getParent('.cart-item').destroy();
                    Cubix.OnlineBillingV4.GotdCalendar.gotdPreselectedDates = [];
                    Cubix.OnlineBillingV4.VotdCalendar.votdPreselectedDates = [];

                    if (cart.getElements('.package-item').length < self.indepEscortLimit) {
                        self.eventManager();
                        self.addToCartBtn.removeClass('inactive');

                        self.packageOverlay.enable();
                        self.activationOverlay.enable();
                        if (cart.getElements('.cart-item').length < self.indepEscortLimit) {
                            self.paymentMethodsOverlay.disable();
                        }
                        $$('.sp-gt-desc')[0].set('html', 0 + ' CHF');
                    }
                });
            });

            // Adds add gotd event on the cart items
            $$('.cart-item').getElements('.sp-add-gotd').each(function(el) {
                el.removeEvents();

                el.addEvent('click', function() {
                    var packageDuration = this.getParent('table').getElements('tr')[0].getElements('td')[0].getElements('.package-duration')[0].get('value');
                    var packageStartDate = this.getParent('table').getElements('tr')[0].getElements('td')[1].getElements('.package-activation-date')[0].get('value');
                    packageStartDate = packageStartDate == '' ? (new Date()).getTime() : parseInt(packageStartDate);
                    if(typeof CurrentPackage !== 'undefined' && CurrentPackage[0].expiration_date){
                        packageStartDate = parseInt(CurrentPackage[0].expiration_date)*1000;
                        var packageEndDate = parseInt(CurrentPackage[0].expiration_date)*1000 + parseInt(packageDuration * 24 * 60 * 60 * 1000);
                    } else{
                        var packageEndDate = parseInt(packageStartDate) + parseInt(packageDuration * 24 * 60 * 60 * 1000);
                    }


                    if (self.curPackageExpirationDate) {
                        packageDate = d.getFullYear() + '-' + ('0' + (parseInt(d.getMonth()) + 1)).slice(-2) + '-' + ('0' + d.getDate()).slice(-2) + ' ' + ('0' + d.getHours()).slice(-2) + ':' + ('0' + d.getMinutes()).slice(-2);
                    }

                    Cubix.OnlineBillingV4.GotdCalendar.gotdAreasEl.getElements('option')[0].set('selected', 'selected');
                    Cubix.OnlineBillingV4.GotdCalendar.gotdSelectedArea = 0;

                    if (!packageStartDate && self.curPackageExpirationDate && (this.getParent('.cart-item'))) {
                        packageStartDate = self.curPackageExpirationDate;
                        packageEndDate = parseInt(packageStartDate) + parseInt(packageDuration * 24 * 60 * 60 * 1000);
                    }
                    Cubix.OnlineBillingV4.GotdCalendar.setDateRange([packageStartDate, packageEndDate]);
                    Cubix.OnlineBillingV4.GotdCalendar.gotdObjEl = {
                        gotdViewEl: this.getParent('td'),
                        gotdModelEl: this.getSiblings('.package-gotd-object')[0]
                    };
                    Cubix.OnlineBillingV4.GotdCalendar.construct();
                    Cubix.OnlineBillingV4.GotdCalendar.showCalendar(this, false);
                })
            });

            // Adds add votd event on the cart items
            $$('.cart-item').getElements('.sp-add-votd').each(function(el) {
                el.removeEvents();

                el.addEvent('click', function() {
                    var packageDuration = this.getParent('table').getElements('tr')[0].getElements('td')[0].getElements('.package-duration')[0].get('value');
                    var packageStartDate = this.getParent('table').getElements('tr')[0].getElements('td')[1].getElements('.package-activation-date')[0].get('value');

                    packageStartDate = packageStartDate == '' ? (new Date()).getTime() : parseInt(packageStartDate);

                    if(typeof CurrentPackage !== 'undefined' && CurrentPackage[0].expiration_date){
                        packageStartDate = parseInt(CurrentPackage[0].expiration_date)*1000;
                        var packageEndDate = parseInt(CurrentPackage[0].expiration_date)*1000 + parseInt(packageDuration * 24 * 60 * 60 * 1000);
                    } else{
                        var packageEndDate = parseInt(packageStartDate) + parseInt(packageDuration * 24 * 60 * 60 * 1000);
                    }

                    if (self.curPackageExpirationDate) {
                        packageDate = d.getFullYear() + '-' + ('0' + (parseInt(d.getMonth()) + 1)).slice(-2) + '-' + ('0' + d.getDate()).slice(-2) + ' ' + ('0' + d.getHours()).slice(-2) + ':' + ('0' + d.getMinutes()).slice(-2);
                    }

                    Cubix.OnlineBillingV4.VotdCalendar.votdAreasEl.getElements('option')[0].set('selected', 'selected');
                    Cubix.OnlineBillingV4.VotdCalendar.votdSelectedArea = 0;

                    if (!packageStartDate && self.curPackageExpirationDate && (this.getParent('.cart-item'))) {
                        packageStartDate = self.curPackageExpirationDate;
                        packageEndDate = parseInt(packageStartDate) + parseInt(packageDuration * 24 * 60 * 60 * 1000);
                    }

                    Cubix.OnlineBillingV4.VotdCalendar.setDateRange([packageStartDate, packageEndDate]);
                    Cubix.OnlineBillingV4.VotdCalendar.votdObjEl = {
                        votdViewEl: this.getParent('td'),
                        votdModelEl: this.getSiblings('.package-votd-object')[0]
                    };
                    Cubix.OnlineBillingV4.VotdCalendar.construct();
                    Cubix.OnlineBillingV4.VotdCalendar.showCalendar(this, false);
                })
            });

            // Adds Additional area selecting event on the cart items
            $$('.sp-add-area').each(function(el) {
                el.removeEvents();

                el.addEvent('click', function() {
                    Cubix.OnlineBillingV4.AreaSelector.areaViewEl = el.getParent('td');
                    Cubix.OnlineBillingV4.AreaSelector.areaModelEl = el.getSiblings('.area-object')[0];
                    Cubix.OnlineBillingV4.AreaSelector.construct();
                    Cubix.OnlineBillingV4.AreaSelector.showAreaSelector(this, false);
                })
            });

            // Adds activation date editing on the cart item
            $$('.cart-item').getElements('.sp-edit').each(function(el) {
                el.removeEvents();

                el.addEvent('click', function() {
                    if (this.getSiblings('input')[0].get('value')) {
                        var d = new Date(1970, 0, 1);
                        d.setTime(this.getSiblings('input')[0].get('value'));
                        Cubix.OnlineBillingV4.PackageCalendar.packageStartDate.set('value', d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate());
                    }

                    Cubix.OnlineBillingV4.PackageCalendar.construct(false, false, this.getSiblings('input')[0].get('value'), {
                        view: this.getSiblings('span')[0],
                        model: this.getSiblings('input')[0]
                    });
                    Cubix.OnlineBillingV4.GotdCalendar.gotdPreselectedDates = [];
                    Cubix.OnlineBillingV4.PackageCalendar.showCalendar(this, this.getSiblings('input')[0]);
                })
            });

            this.paymentMethodsOverlay.enable();
        } else {
            if ($('packagesWrapper').getElements('.err-msg').length < 1) {
                self.createErrTooltip($('packagesWrapper'), Cubix.OnlineBillingV4.Lang.selectPackage);
            } else {
                $('packagesWrapper').highlight();
            }
        }
        this.updateCart(packageObj, packageDate);
    },

    updateCart: function(p, d) {
        this.cartContent.set('value', JSON.encode([{
            id: p.id,
            price: p.price,
            duration: p.duration,
            activation: d
        }]));
    },

    updateCartPrice: function() {
        // getting data from the fields
        var tableEls = $$('.sp-package-table');
        var detailsEls = tableEls.getElements('.sp-package-details');
        var summaryEls = tableEls.getElements('.sp-package-result');

        for (var i = tableEls.length - 1; i >= 1; i--) {
			
			var pGotdCount = detailsEls[i].getElements('.activated-gotd-days')[0].length;
			var pVotdCount = detailsEls[i].getElements('.activated-votd-days')[0].length;
			var pAreasCount = detailsEls[i].getElements('.selected-arr-areas')[0].length;
			var pAreasPrice = parseInt($('addAreaPrice').get('value'));
			var pPrice = parseInt(detailsEls[i].getElements('.package-price')[0].get('value'));
			var pGotdPrice = parseInt($('gotdPrice').get('value'));
			var pVotdPrice = parseInt($('votdPrice').get('value'));
			
			var pCryptoPrice = parseInt(detailsEls[i].getElements('.package-crypto-price')[0].get('value'));
			var pCryptoGotdPrice = parseInt($('gotdCryptoPrice').get('value'));
			var pCryptoVotdPrice = parseInt($('votdCryptoPrice').get('value'));
					
            // updating GOTD & Area
			
            if (pGotdCount > 0) {
				var pGotdPricesText = Cubix.OnlineBillingV4.Payments.generatePrices(pGotdPrice, pCryptoGotdPrice);
                summaryEls[i][0].getElements('td')[2].set('html', '+' + pGotdCount + 'x' + pGotdPricesText + ' CHF');
            }else{
				summaryEls[i][0].getElements('td')[2].set('html', '');
			}

            // updating VOTD & Area
            if (pVotdCount > 0) {
				var pVotdPricesText = Cubix.OnlineBillingV4.Payments.generatePrices(pVotdPrice, pCryptoVotdPrice);
                summaryEls[i][0].getElements('td')[3].set('html', '+' + pVotdCount + 'x' + pVotdPricesText + ' CHF');
            }else{
				summaryEls[i][0].getElements('td')[3].set('html', '');
			}


            if (pAreasCount > 0) {
                summaryEls[i][0].getElements('td')[4].set('html', '+' + pAreasCount + 'x' + pAreasPrice + ' CHF');
            } else {
                summaryEls[i][0].getElements('td')[4].set('html', '');
            }

            var pSumTxt = summaryEls[i][0].getElements('td')[0].get('html');
            var pGotdTxt = summaryEls[i][0].getElements('td')[2].get('html'); //? summaryEls[i][0].getElements('td')[3].get('html') : summaryEls[i][0].getElements('td')[2].get('html') ;
			var pVotdTxt = summaryEls[i][0].getElements('td')[3].get('html');
            var pAreaTxt = summaryEls[i][0].getElements('td')[4].get('html');

            // updating overall price
            detailsEls[i][0].getElements('td')[5].set('html', pSumTxt + (pGotdTxt ? ('<br>' + pGotdTxt ) : '') + (pVotdTxt ? ('<br>' + pVotdTxt ) : '') + (pAreaTxt ? ('<br>' + pAreaTxt) : '') + ' =');
			var totalPrice = pPrice + pGotdCount * pGotdPrice  + pVotdCount * pVotdPrice + pAreasCount * pAreasPrice;
			var totalCryptoPrice = pCryptoPrice + pGotdCount * pCryptoGotdPrice  + pVotdCount * pCryptoVotdPrice + pAreasCount * pAreasPrice;
			
            summaryEls[i][0].getElements('td')[5].set('html', Cubix.OnlineBillingV4.Payments.generatePrices(totalPrice, totalCryptoPrice) + ' CHF');
			
        }

        var total = cryptoTotal = 0;
        var curTotal = $$('.table-total');
        for (var i = curTotal.length - 1; i >= 0; i--) {
            total = total + parseInt(curTotal[i].getElement('.chf-price').get('html'));
			cryptoTotal = cryptoTotal + parseInt(curTotal[i].getElement('.crypto-price').get('html'));
        }

        $$('.sp-gt-desc')[0].set('html', Cubix.OnlineBillingV4.Payments.generatePrices(total, cryptoTotal) + ' CHF');
    }
};

// Package Calendar
Cubix.OnlineBillingV4.PackageCalendar = {
    // defaults
    dateRange: [],
    curPackageExpirationDate: 0,
    packageCalendar: null,
    calendarCloseBtn: null,
    calendarBackBtn: null,
    calendarForwardBtn: null,
    monthYear: null,
    packageCalDays: null,
    daysCount: 0,

    hourField: null,
    minutesField: null,

    packageStartDate: null,
    packageStartHour: null,
    packageStartMinute: null,

    addBtn: null,

    init: function() {
      this.setDateRange([1461528000000, 1461441600000]);
      this.setDaysCount(42);
      this.getPackageCalendar();
      this.setValidation();
      this.construct();
      this.eventManager();
    },

    getPackageCalendar: function() {
        this.packageCalendar = $('packageCalendar');
        this.calendarBackBtn = $('cBackBtn');
        this.calendarForwardBtn = $('cForwardBtn');
        this.monthYear = $('cMonthYear');
        this.hourField = $('cHour');
        this.minutesField = $('cMinutes');
        this.addBtn = $('cAddAction');
        this.packageStartDate = $('packageStartDate');
        this.packageStartHour = $('packageStartHour');
        this.packageStartMinute = $('packageStartMinute');
        this.calendarCloseBtn = $('calClose');
        this.packageCalDays = $('cDays');
        if ($$('.package-expiration-date')[0]) {
          this.curPackageExpirationDate = parseInt($$('.package-expiration-date')[0].get('value'));
        }

    },

    construct: function(gCalMonth, direction, setDate, objEl) {
      var self = this;

      // adding the days list elements to the container
      this.packageCalDays.set('html', '');
      for (var i = 0; i < this.daysCount; i++) {
          this.packageCalDays.adopt(new Element('li'));
      };

      // adoping the container to the view
      this.packageCalDays.adopt(new Element('div.clear'));

      // filling the calendar dates
      // defining the first date of the month by package available first date
      var dateRangeStart = null;

      if (!gCalMonth) {
        dateRangeStart = new Date();
        var monthFirstDate = new Date(dateRangeStart.getFullYear(), dateRangeStart.getMonth(), 1);
      } else {
        dateRangeStart = monthFirstDate = new Date(gCalMonth.split(',')[1], gCalMonth.split(',')[0], 1);
      }

      // getting the first day on the calendar
      var calStartDate = new Date(monthFirstDate.getTime() - (monthFirstDate.getDay() == 0 ? 6 : monthFirstDate.getDay() - 1) * 24 * 60 * 60 * 1000);
      var cDaysFields = this.packageCalDays.getElements('li');
      var monthNames = Cubix.OnlineBillingV4.Lang.months;

      // inserting the month and year
      this.monthYear.set('html', monthNames[dateRangeStart.getMonth()] + ' ' + dateRangeStart.getFullYear());
      cDaysFields.each(function(el) {
          el.removeAttribute('class');
          el.highlight('#9fa2a8');
          el.removeEvents();
      });

      // filling the dates in the calendar
      for (var i = cDaysFields.length - 1; i >= 0; i--) {
          var d = new Date(calStartDate).increment('day', i);
          // console.log(d);


          cDaysFields[i].set('html', d.getDate());
          cDaysFields[i].set('data-value', d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate());

          if (!this.curPackageExpirationDate) {
              var b = new Date();
          } else {
              var b = new Date(this.curPackageExpirationDate);
          }

          b = new Date(b.getFullYear(), b.getMonth(), b.getDate());

          if (d < b) {
              cDaysFields[i].addClass('inactive');
          }

          // if the the packageDate is set
          if (setDate && typeof(setDate) !== "undefined") {
            var a = new Date(1970, 0, 1);

            a.setTime(setDate);
            var oDate = new Date(a.getFullYear(), a.getMonth(), a.getDate());

            if (d.getTime() == oDate.getTime()) {
              cDaysFields[i].addClass('selected');
            }

            if (oDate.getTime() == (new Date(1970, 0, 1)).getTime()) {
              this.hourField.set('value', '');
              this.minutesField.set('value', '');
            } else {
              this.hourField.set('value', a.getHours());
              this.minutesField.set('value', a.getMinutes());
            }

          } else {
            var curDt = (new Date).get('date')

            if (d.getTime() == (new Date()).clearTime().getTime()) {
              cDaysFields[i].addClass('selected');
            }

            this.hourField.set('value', (new Date()).getHours());
            this.minutesField.set('value', (new Date()).getMinutes());

            this.packageStartDate.set('value', (new Date()).format('%Y-%m-%d'));
          }

          // set Button values
          var lastMonthArr = [new Date(monthFirstDate.getTime() - 15 * 24 * 60 * 60 * 1000).getMonth(), new Date(monthFirstDate.getTime() - 15 * 24 * 60 * 60 * 1000).getFullYear()]; // 15days is just a value to get the month and year
          var nextMonthArr = [new Date(monthFirstDate.getTime() + 45 * 24 * 60 * 60 * 1000).getMonth(), new Date(monthFirstDate.getTime() + 45 * 24 * 60 * 60 * 1000).getFullYear()]; // 45days is just a value to get the month and year

          this.calendarBackBtn.set('data-value', lastMonthArr);
          this.calendarForwardBtn.set('data-value', nextMonthArr);
      }

      self.setCalAction(0, 1, objEl);
      if (objEl) {
        self.eventManager(objEl);
        self.setValidation(objEl);
      }
    },

    setDaysCount: function(days) {
      this.daysCount = days;
    },

    setCalAction: function(uptInact, initLoad, objEl) {
        var self = this;
        var cDaysFields = this.packageCalDays.getElements('li');

        cDaysFields.each(function(el) {
            el.removeEvents();
            if (!el.hasClass('inactive')) {
                if (!initLoad) {
                    el.removeAttribute('class');
                }

                el.addEvent('click', function() {

                    // to update also the inactive cells
                    if (!initLoad) {
                        self.setCalAction(1, 0, objEl);
                    } else {
                        self.setCalAction(1, 0, objEl);
                    }
                    self.packageStartDate.set('value', el.getAttribute('data-value'));
                    el.addClass('selected');
                    el.removeEvents();
                    self.checkIfValid(objEl);
                });
            }

            if (uptInact == true && el.hasClass('selected') && el.hasClass('inactive')) {
                el.removeClass('selected');
            }
        });
    },

    showCalendar: function(vEl, modelEl) {

        var self = this;

        if (modelEl) {
            this.addBtn.setAttribute('data-action', 'edit');
        } else {
            this.addBtn.setAttribute('data-action', 'create');
        }

        // setting the position of the calendar on the page
        $(document.body).scrollTo(0, vEl.getPosition().y - window.getSize().y * 0.75);

        this.packageCalendar.setStyle('top', (vEl.getPosition().y < 700) ? (vEl.getPosition().y - 493) : window.getSize().y * 0.75 - 493);
        this.packageCalendar.setStyle('left', vEl.getPosition().x - 25);

        this.packageCalendar.show();
        Cubix.OnlineBillingV4.Packages.loadOverlay();

        // adding Event listerner to document, to close calendar when 'Esc' is pressed
        document.addEvent('keyup', function(e) {
            e = e.event;
            var key = e.keyCode || e.which;
            if (key == 27) {
                self.hideCalendar();
                $$('.act-type')[0].fireEvent('click');
            }
        });

        this.minutesField.fireEvent('keyup');
        this.hourField.fireEvent('keyup');
    },

    hideCalendar: function() {
        this.packageCalendar.hide();
        Cubix.OnlineBillingV4.Packages.unloadOverlay();
    },

    setValidation: function(objEl) {
        var self = this;
        self.hourField.removeEvents();
        self.hourField.addEvent('keyup', function() {
            if (this.get('value')) {

                if (parseInt(this.get('value')) >= 0 && parseInt(this.get('value')) <= 23) {
                    self.hourField.highlight();
                    self.hourField.set('value', ('0' + parseInt(this.get('value'))).slice(-2));
                    self.packageStartHour.set('value', self.hourField.get('value'));
                    self.checkIfValid(objEl ? objEl : '');
                } else {
                    self.hourField.highlight('#F00');
                    self.hourField.set('value', 0);
                }
            }
        });

        self.minutesField.removeEvents();
        self.minutesField.addEvent('keyup', function() {
            if (this.get('value')) {
                if (parseInt(this.get('value')) >= 0 && parseInt(this.get('value')) <= 59) {
                    self.minutesField.highlight();
                    self.minutesField.set('value', ('0' + parseInt(this.get('value'))).slice(-2));
                    self.packageStartMinute.set('value', self.minutesField.get('value'));
                    self.checkIfValid(objEl ? objEl : '');
                } else {
                    self.minutesField.highlight('#F00');
                    self.minutesField.set('value', 0);
                }
            }
        });

        self.checkIfValid(objEl ? objEl : '');
    },

    checkIfValid: function(objEl) {
      var self = this;

      var year = this.packageStartDate.get('value').split('-')[0];
      var month = this.packageStartDate.get('value').split('-')[1] - 1;
      var day = this.packageStartDate.get('value').split('-')[2];

      if (this.packageStartDate.get('value') && this.packageStartHour.get('value') && this.packageStartMinute.get('value')) {
        var d = new Date(year, month, day, this.packageStartHour.get('value'), this.packageStartMinute.get('value'));
        if (this.addBtn.hasClass('inactive')) {
          this.addBtn.removeClass('inactive');
          this.addBtn.addClass('active');
          this.addBtn.removeEvents();
          this.addBtn.addEvent('click', function() {
            if (self.addBtn.getAttribute('data-action') == 'create') {
              self.addAction(d);
            } else if (self.addBtn.getAttribute('data-action') == 'edit') {
              self.addActionCart(d, objEl.view, objEl.model);
            }
          });
        } else {
          this.addBtn.addClass('active');
          this.addBtn.removeEvents();
          this.addBtn.addEvent('click', function() {
            if (self.addBtn.getAttribute('data-action') == 'create') {
              self.addAction(d);
            } else if (self.addBtn.getAttribute('data-action') == 'edit') {
              self.addActionCart(d, objEl.view, objEl.model);
            }
          });
        }
      }
    },

    setDateRange: function(dateRange) {
        this.dateRange = dateRange;
    },

    eventManager: function(objEl) {
        var self = this;

        this.calendarCloseBtn.removeEvents();
        this.calendarCloseBtn.addEvent('click', function() {
            self.hideCalendar();
            $$('.act-type')[0].fireEvent('click');
        });

        self.calendarBackBtn.removeEvents();
        self.calendarBackBtn.addEvent('click', function() {
            var gCalMonth = this.getAttribute('data-value');

            self.construct(gCalMonth, 'b', (new Date(self.packageStartDate.get('value') + ' ' + self.packageStartHour.get('value') + ':' + self.packageStartMinute.get('value'))).getTime(), objEl ? objEl : false);
        });

        self.calendarForwardBtn.removeEvents();
        self.calendarForwardBtn.addEvent('click', function() {
            var gCalMonth = this.getAttribute('data-value');

            self.construct(gCalMonth, 'f', (new Date(self.packageStartDate.get('value') + ' ' + self.packageStartHour.get('value') + ':' + self.packageStartMinute.get('value'))).getTime(), objEl ? objEl : false);
        });
    },

    addAction: function(d) {
        if (d < new Date()) {
          alert('Invalid time for the activation!');
          return false;
        }

        var activationDate = parseInt(d.getFullYear()) + '-' + ('0' + (parseInt(d.getMonth()) + 1)).slice(-2) + '-' + ('0' + d.getDate()).slice(-2) + ' ' + ('0' + d.getHours()).slice(-2) + ':' + ('0' + d.getMinutes()).slice(-2);
        Cubix.OnlineBillingV4.Packages.selectedDate.set('value', d.getTime());
        Cubix.OnlineBillingV4.Packages.specDateActivationFld.set('value', activationDate);

        if (!Cubix.OnlineBillingV4.Packages.specDateActivationOpt.hasClass('checked')) {
            $$('.act-type').each(function(el) {
                if (el.hasClass('checked')) {
                    el.removeClass('checked');
                }
            });
            Cubix.OnlineBillingV4.Packages.specDateActivationOpt.addClass('checked');

        }

        this.hideCalendar();
    },

    addActionCart: function(d, viewEl, modelEl) {
        if (d < new Date()) {
            alert('Invalid time for the activation!');
            return false;
        }
        var activationDate = d.getFullYear() + '-' + ('0' + (parseInt(d.getMonth()) + 1)).slice(-2) +
                            '-' + ('0' + d.getDate()).slice(-2) + ' ' + ('0' + d.getHours()).slice(-2) +
                            ':' + ('0' + d.getMinutes()).slice(-2) + ' ';
        viewEl.set('html', activationDate);
        modelEl.set('value', d.getTime());

        var activeGotdDays = viewEl.getParent('.sp-package-table').getElements('.activated-gotd-days');

        if (activeGotdDays.length > 0) {
            activeGotdDays.each(function(el) {
                el.destroy();
            });
        }

        viewEl.getParent('.sp-package-table').getElements('.package-gotd-object')[0].set('value', '[]');
        viewEl.getParent('.sp-package-table').getElements('.sp-package-result')[0].getElements('td')[2].set('html', '');
        Cubix.OnlineBillingV4.Cart.updateCartPrice();

        this.hideCalendar();
    }
};

// Gotd Calendar
Cubix.OnlineBillingV4.GotdCalendar = {
    // default
    indepGotdLimit: 0,

    // Gotd Calendar properties

    dateRange: [],
    gotdAreas: [],
    daysCount: 0,
    gotdCalendarSelectedDates: [],
    gotdCalendarEl: null,
    gotdAreasEl: null,
    gotdSelectedArea: 0,
    gotdCalendarCloseBtn: null,
    gotdCalendarBackBtn: null,
    gotdCalendarMonthYearEl: null,
    gotdCalendarForwardBtn: null,
    gotdCalendarDaysEl: null,
    gotdCalendarTotalPriceEl: null,
    gotdCalendarAddBtn: null,
    gotdBookedDays: null,
    gotdCurrentBookedDates: [],
    gotdPreselectedDates: [],
    gotdObjEl: null,
    gotdPrice: 0,

    // Gotd Calendar methods

    init: function() {
      this.getGotdCalendarView();
      this.setDaysCount(42);
      this.setAreas(this.gotdAreasList.get('value'));
      this.construct();
      this.eventManager();
      this.gotdBookedDays = $('gotdBookedDays').get('value');
    },

    getGotdCalendarView: function() {
        this.gotdCalendarEl = $('gotdCalendar');
        this.gotdAreasEl = $('gotdAreas');
        this.gotdCalendarCloseBtn = $('gCalCloseBtn');
        this.gotdCalendarBackBtn = $('gCalBackBtn');
        this.gotdCalendarMonthYearEl = $('gCalMonthYear');
        this.gotdCalendarForwardBtn = $('gCalForwardBtn');
        this.gotdCalendarDaysEl = $('gCalDays');
        this.gotdCalendarTotalPriceEl = $('gotdTotalPrice');
        this.gotdCalendarAddBtn = $('gCalAddBtn');
        this.gotdCalendarSelectedDates = $('gotdSelectedDates');
        this.gotdPrice = $('gotdPrice').get('value');

        // Data from backend
        this.gotdAreasList = $('escortAreas');
    },

    construct: function(gCalMonth, direction) {

        var self = this;
        self.updatePrice(true);
        // adding the days list elements to the container
        this.gotdCalendarDaysEl.set('html', '');
        for (var i = 0; i < this.daysCount; i++) {
            this.gotdCalendarDaysEl.adopt(new Element('li'));
        }

        // adoping the container to the view
        this.gotdCalendarDaysEl.adopt(new Element('div.clear'));

        // adding available areas to the areas selector
        if (!this.gotdSelectedArea) {
            this.gotdAreasEl.getElements('option').destroy();

            this.gotdAreasEl.adopt(new Element('option', {
                'html': '---',
                'data-id': ''
            }));

              for (var i = this.gotdAreas.length - 1; i >= 0; i--) {
                this.gotdAreasEl.adopt(new Element('option', {
                    'html': this.gotdAreas[i].title,
                    'data-id': this.gotdAreas[i].id
                }))
            }
        }

        // filling the calendar dates
        // defining the first date of the month by package available first date
        if (!gCalMonth) {
            var dateRangeStart = new Date();
            var monthFirstDate = new Date(dateRangeStart.getFullYear(), dateRangeStart.getMonth(), 1);
        } else {
            var dateRangeStart = monthFirstDate = new Date(gCalMonth.split(',')[1], gCalMonth.split(',')[0], 1);
        }

        // getting the first day on the calendar
        var calStartDate = new Date(monthFirstDate.getTime() - (monthFirstDate.getDay() == 0 ? 6 : monthFirstDate.getDay() - 1) * 24 * 60 * 60 * 1000);
        var cDaysFields = this.gotdCalendarDaysEl.getElements('li');
        var monthNames = Cubix.OnlineBillingV4.Lang.months;

        // inserting the month and year
        this.gotdCalendarMonthYearEl.set('html', monthNames[dateRangeStart.getMonth()] + ' ' + dateRangeStart.getFullYear());
        cDaysFields.each(function(el) {
            el.removeAttribute('class');
            el.highlight('#9fa2a8');
            el.removeEvents();
        });

        // filling the dates in the calendar
        for (var i = cDaysFields.length - 1; i >= 0; i--) {
          var d = new Date(calStartDate).increment('day', i);

          cDaysFields[i].set('html', d.getDate());
          cDaysFields[i].set('data-value', d.getFullYear() + '-' + (parseInt(d.getMonth()) + 1) + '-' + d.getDate());

          if (!self.gotdSelectedArea) {
            cDaysFields[i].addEvent('click', function() {
              if (self.gotdAreasEl.getParent('div').getElements('.err-msg').length <= 0) {
                self.createErrTooltip(self.gotdAreasEl, Cubix.OnlineBillingV4.Lang.errs.selectRegion)
              } else {
                self.gotdAreasEl.highlight();
              }
            });
          } else {
            var currentPackageItem = $$('.cart-item-gotd')[0],
                b = new Date(self.dateRange[0]);

            b = (new Date(b.getFullYear(), b.getMonth(), b.getDate())).getTime();

            var c = new Date(this.dateRange[1]);
            c = (new Date(c.getFullYear(), c.getMonth(), c.getDate())).getTime() - (1 * 24 * 60 * 60 * 1000);

            if (d.getTime() >= b && d.getTime() <= c) {
              cDaysFields[i].set('class', 'active');
              cDaysFields[i].addEvent('click', function() {
                if (this.hasClass('active')) {
                  this.removeClass('active');
                  this.addClass('selected');

                  var v = self.gotdCalendarSelectedDates.get('value');
                  self.gotdCalendarSelectedDates.set('value', v + (v.length > 0 ? ',' : '') +
                      this.get('data-value'));

                  if (self.gotdCalendarSelectedDates.get('value')) {
                    self.gotdCalendarAddBtn.removeAttribute('class');
                    self.gotdCalendarAddBtn.set('class', 'active');
                    self.gotdCalendarAddBtn.removeEvents();

                    self.updatePrice(false);
                    self.gotdCalendarAddBtn.addEvent('click', function() {
						self.addAction(self.gotdCalendarSelectedDates.get('value'),
						self.gotdAreasEl.getSelected().get('html'));

                    });
                  } else {
                    self.updatePrice(true);
                  }

                } else if (this.hasClass('selected')) {
                  this.removeClass('selected');
                  this.addClass('active');

                  var x = self.gotdCalendarSelectedDates.get('value');
                  var y = x.split(',');

                  var index = y.indexOf(this.get('data-value'));

                  if (index > -1) {
                      y.splice(index, 1);
                  }

                  self.gotdCalendarSelectedDates.set('value', y.toString());

                  if (self.gotdCalendarSelectedDates.get('value')) {
                    self.gotdCalendarAddBtn.removeAttribute('class');
                    self.gotdCalendarAddBtn.set('class', 'active');
                    self.gotdCalendarAddBtn.removeEvents();

                    self.gotdCalendarAddBtn.addEvent('click', function() {
                        self.addAction(self.gotdCalendarSelectedDates.get('value'));
                    });
                    self.updatePrice(false);

                  } else {
                    self.gotdCalendarAddBtn.removeAttribute('class');
                    self.gotdCalendarAddBtn.set('class', 'inactive');
                    self.gotdCalendarAddBtn.removeEvents();
                    //self.updatePrice(false);
                    self.updatePrice(true);
                  }
                }
              });
            }

            if (self.gotdCurrentBookedDates.indexOf(d.getTime()) != -1 && d.getTime() >= b && d.getTime() <= c) {
                cDaysFields[i].removeEvents();
                cDaysFields[i].set('class', 'booked');
            }

            if (self.gotdPreselectedDates.indexOf(d.getTime()) != -1 && d.getTime() >= b && d.getTime() <= c) {
                cDaysFields[i].removeEvents();
                cDaysFields[i].set('class', 'preselected');
            }
          }

          // set Button values
          var lastMonthArr = [new Date(monthFirstDate.getTime() - 15 * 24 * 60 * 60 * 1000).getMonth(), new Date(monthFirstDate.getTime() - 15 * 24 * 60 * 60 * 1000).getFullYear()]; // 15days is just a value to get the month and year
          var nextMonthArr = [new Date(monthFirstDate.getTime() + 45 * 24 * 60 * 60 * 1000).getMonth(), new Date(monthFirstDate.getTime() + 45 * 24 * 60 * 60 * 1000).getFullYear()]; // 45days is just a value to get the month and year
          this.gotdCalendarBackBtn.set('data-value', lastMonthArr);
          this.gotdCalendarForwardBtn.set('data-value', nextMonthArr);
        }
    },

    setDaysCount: function(days) {
        this.daysCount = days;
    },

    setDateRange: function(dateRange) {
        this.dateRange = dateRange;
    },

    setAreas: function(areas) {
        this.gotdAreas = JSON.parse(areas);
    },

    updatePrice: function(remove) {
        if (!remove) {
            var total = this.gotdPrice * this.gotdCalendarSelectedDates.get('value').split(',').length;
            this.gotdCalendarTotalPriceEl.set('html', 'Total ' + total + ' CHF');
        } else {
            this.gotdCalendarTotalPriceEl.set('html', 'Total 0 CHF');
        }
    },

    showCalendar: function(viewEl) {

        var self = this;

        // setting the position of the calendar on the page
        $(document.body).scrollTo(0, viewEl.getPosition().y - window.getSize().y * 0.85);

        this.gotdCalendarEl.setStyle('top', (viewEl.getPosition().y < 700) ? (viewEl.getPosition().y - 565) : window.getSize().y * 0.85 - 565);
        this.gotdCalendarEl.setStyle('left', viewEl.getPosition().x - 25);
        this.construct(false, false);

        this.gotdCalendarEl.show();
        Cubix.OnlineBillingV4.Packages.loadOverlay();

        // adding Event listerner to document, to close calendar when 'Esc' is pressed
        document.addEvent('keyup', function(e) {
            e = e.event;
            var key = e.keyCode || e.which;
            if (key == 27) {
                self.hideCalendar();
                $$('.act-type')[0].fireEvent('click');
            }
        });
    },

    hideCalendar: function() {
        this.gotdCalendarEl.hide();
        Cubix.OnlineBillingV4.Packages.unloadOverlay();
    },

    eventManager: function() {
        var self = this;

        this.gotdCalendarCloseBtn.addEvent('click', function() {
          if ($$('.gotd-item').length && $$('.gotd-item .activated-gotd-days').length == 0){
            $$('.gotd-item .sp-delete-symbol')[0].fireEvent('click');
          }

          self.hideCalendar();
        });

        // selecting the gotd area
        this.gotdAreasEl.addEvent('change', function() {
            var areaId = this.getSelected().get('data-id');
            self.gotdCalendarAddBtn.removeAttribute('class');
            self.gotdCalendarAddBtn.removeEvents();
            
            self.gotdSelectedArea = areaId;

            if (areaId) {
                if (self.gotdAreasEl.getParent('div').getElements('.err-msg').length > 0) {
                    self.gotdAreasEl.getParent('div').getElements('.err-msg').destroy();
                }
            }


            var x = JSON.parse(self.gotdBookedDays);
            var bookedDaysObj = x[areaId];

            if (bookedDaysObj) {
                var bookedDates = [];

                for (var i = bookedDaysObj.length - 1; i >= 0; i--) {
                    bookedDates.push((new Date(bookedDaysObj[i]['date'])).clearTime().getTime());
                }

                self.gotdCurrentBookedDates = bookedDates;

            } else {
                self.gotdCurrentBookedDates = '';
            }

            self.gotdCalendarSelectedDates.set('value', '');
            self.construct(false, false);
        });

        // behavior of calendar back end forward buttons
        self.gotdCalendarBackBtn.addEvent('click', function() {
            var gCalMonth = this.getAttribute('data-value');
            self.gotdCalendarSelectedDates.set('value', '');
            self.construct(gCalMonth, 'b');
        });

        self.gotdCalendarForwardBtn.addEvent('click', function() {
            var gCalMonth = this.getAttribute('data-value');
            self.gotdCalendarSelectedDates.set('value', '');
            self.construct(gCalMonth, 'f');
        });
    },

    addAction: function(strDates, area) {
      var self = this,
          arrStrDatesTime = [],
          arrStrDate = strDates.split(',');

        for (var i = arrStrDate.length - 1; i >= 0; i--) {
          this.gotdObjEl.gotdViewEl.adopt(new Element('span', {
            html: '<br><span class="sp-delete-symbol">&#215;</span> <span>' + arrStrDate[i] +
                  '<span class="gotd-city">' + area + '</span></span>',
            'data-date': arrStrDate[i],
            class: 'activated-gotd-days'
          }));

          this.gotdPreselectedDates.push((new Date(arrStrDate[i] + ' 00:00')).getTime());
          $('spCart').fireEvent('cartChange');
        };

        $$('.sp-delete-symbol').each(function(el) {
          el.addEvent('click', function() {
            var xDate = el.getParent('span').get('data-date');
            this.getSiblings('br').destroy();
            this.getParent('span').destroy();

            var arrAreaGotdDates = JSON.parse(self.gotdObjEl.gotdModelEl.get('value'));

            for (var i = arrAreaGotdDates.length - 1; i >= 0; i--) {

                for (var key in arrAreaGotdDates[i]) {
                    var arrZDate = arrAreaGotdDates[i][key].split(',');

                    if (arrZDate.indexOf(xDate) > -1) {
                        arrZDate.splice(arrZDate.indexOf(xDate), 1);
                    }

                    arrAreaGotdDates[i][key] = arrZDate.toString();

                    if (arrZDate.length == 0) {
                        arrAreaGotdDates.splice(i, 1);
                    }
                }
            }

            Cubix.OnlineBillingV4.Cart.updateCartPrice();

            self.gotdObjEl.gotdModelEl.set('value', JSON.encode(arrAreaGotdDates));

            var xPreDates = [];
            self.gotdPreselectedDates = [];

            for (var i = arrAreaGotdDates.length - 1; i >= 0; i--) {
              for (var preKey in arrAreaGotdDates[i]) {
                xPreDates = arrAreaGotdDates[i][preKey].split(',');
                for (var i = xPreDates.length - 1; i >= 0; i--) {
                    self.gotdPreselectedDates.push((new Date(xPreDates[i] + ' 00:00')).getTime());
                }
              }
            }

            if ($$('.gotd-item').length) {
              if ($$('.gotd-item').getElements('.activated-gotd-days .sp-delete-symbol') &&
                  $$('.gotd-item').getElements('.activated-gotd-days .sp-delete-symbol')[0].length == 0){
                  $$('.gotd-item').getElement('.sp-delete-symbol').fireEvent('click');
              }
            }
            $('spCart').fireEvent('cartChange');
          });
        });

        var arrO = [];
        var arrPackageGotdObj = this.gotdObjEl.gotdModelEl.get('value');
        if (arrPackageGotdObj) {
            for (var i = JSON.parse(arrPackageGotdObj).length - 1; i >= 0; i--) {
                arrO.push(JSON.parse(arrPackageGotdObj)[i]);
            }
        }

        var o = {};
        o[this.gotdSelectedArea] = strDates;
        arrO.push(o);

        this.gotdObjEl.gotdModelEl.set('value', JSON.stringify(arrO));

        self.gotdCalendarSelectedDates.set('value', '');
        this.hideCalendar();

        Cubix.OnlineBillingV4.Cart.updateCartPrice();
        if (Cubix.OnlineBillingV4.Gotd.currentPackage){
          Cubix.OnlineBillingV4.Gotd.disableGotd();
        }
        self.gotdCalendarAddBtn.removeAttribute('class');
        self.gotdCalendarAddBtn.removeEvents();
        $('spCart').fireEvent('cartChange');
    },

    createErrTooltip: function(el, msg) {
        var x = el.getPosition().x;
        var y = el.getPosition().y;

        var errMsg = new Element('span', {
            'class': 'err-msg',
            'html': msg,
            'styles': {
                'top': 0,
                'left': '50%',
                'transform': 'translate(-50%,-100%)'
            }
        });

        el.getParent('div').adopt(errMsg);
        errMsg.fade('in');
        errMsg.setStyle('margin-top', -10);
    }
};

// Area Selector
Cubix.OnlineBillingV4.AreaSelector = {
    areas: [],
    areasEl: null,
    areaSelector: null,
    areaSelCloseBtn: null,
    areaAddBtn: null,
    areaModelEl: null,
    areaViewEl: null,


    init: function() {
      this.setAreas();
      this.getAreaSelector();
      this.eventManager();
      this.construct();
    },

    setAreas: function() {
      this.areas = JSON.parse($('escortAdditionalAreas').get('value'));
    },

    getAreaSelector: function() {
        this.areaSelector = $('areaSelector');
        this.areaSelectFld = $('areas');
        this.areaSelCloseBtn = $('areaSelCloseBtn');
        this.areaAddBtn = $('aSellAddBtn');
    },

    eventManager: function() {
        var self = this;

        this.areaSelCloseBtn.addEvent('click', function() {
            self.hideAreaSelector();
        });

        this.areaSelectFld.addEvent('change', function() {
            if (self.areaSelectFld.getSelected().get('data-id') != '') {
                self.enableAddBtn(true, [
                    self.areaSelectFld.getSelected().get('html'),
                    self.areaSelectFld.getSelected().get('data-id')
                ]);
            } else {
                self.enableAddBtn(false);
            }
        });
    },

    construct: function() {
        this.areaSelectFld.set('html', '');
        this.areaSelectFld.adopt(new Element('option', {
            'html': '---',
            'data-id': ''
        }));

        for (var i = this.areas.length - 1; i >= 0; i--) {

            if (this.areaModelEl) {
                var arrAreaEl = this.areaModelEl.get('value').split(',');
                var indexId = arrAreaEl.indexOf(this.areas[i].id.toString());
            } else {
                var indexId = 0;
            }

            if (indexId < 0) {
                this.areaSelectFld.adopt(new Element('option', {
                    'html': this.areas[i].title,
                    'data-id': this.areas[i].id
                }));
            }
        }
    },

    addAction: function(areaArrEl) {
        var self = this;

        this.areaViewEl.adopt(new Element('span', {
            html: '<br><span class="sp-delete-symbol-area">&#215;</span><span>' + areaArrEl[0] + '</span>',
            'data-id': areaArrEl[1],
            class: 'selected-arr-areas'
        }));


        $$('.sp-delete-symbol-area').each(function(el) {
            el.addEvent('click', function() {
                var strArea = self.areaModelEl.get('value').split(',');
                var indexId = strArea.indexOf(this.getParent('span').get('data-id'));

                if (indexId > -1) {
                    strArea.splice(indexId, 1);
                    self.areaModelEl.set('value', strArea);
                }

                this.getParent('span').destroy();
                Cubix.OnlineBillingV4.Cart.updateCartPrice();
            });
        });

        var arrAreas = [];
        if (this.areaModelEl.get('value')) {
            arrAreas.push(this.areaModelEl.get('value'));
        }

        arrAreas.push(areaArrEl[1]);

        this.areaModelEl.set('value', arrAreas);
        this.hideAreaSelector();

        Cubix.OnlineBillingV4.Cart.updateCartPrice();
        this.areaAddBtn.removeClass('active');
        this.areaAddBtn.addClass('inactive');
        this.areaAddBtn.removeEvents();
    },

    showAreaSelector: function(vEl, cEl) {
        var self = this;

        // getting the date from model - if editing, otherwise creating new
        var d = (cEl && cEl.get('value')) ? new Date(cEl.get('value')) : new Date();

        // getting the veiw element to update html
        var relEl = vEl.getBoundingClientRect();
        if (relEl) {
            elTop = relEl.top;
            elLeft = relEl.left;
        }

        this.areaSelector.setStyle('top', elTop - 80);
        this.areaSelector.setStyle('left', elLeft - 125);
        this.areaSelector.show();
        Cubix.OnlineBillingV4.Packages.loadOverlay();

        // adding Event listerner to document, to close calendar when 'Esc' is pressed
        document.addEvent('keyup', function(e) {
            e = e.event;
            var key = e.keyCode || e.which;
            if (key == 27) {
                self.hideAreaSelector();
                $$('.act-type')[0].fireEvent('click');
            }
        });
    },

    hideAreaSelector: function() {
        this.areaSelector.hide();
        Cubix.OnlineBillingV4.Packages.unloadOverlay();
    },

    enableAddBtn: function(flag, areaArrEl) {
        var self = this;

        if (flag) {
            self.areaAddBtn.removeAttribute('class');
            self.areaAddBtn.addClass('active');
            self.areaAddBtn.removeEvents();
            self.areaAddBtn.addEvent('click', function() {
                self.addAction(areaArrEl);
            });

        } else {
            self.areaAddBtn.removeAttribute('class');
            self.areaAddBtn.addClass('inactive');
            self.areaAddBtn.removeEvents();
        }
    }
};

// Payments
Cubix.OnlineBillingV4.Payments = {
    // defaults

    // parametrs
    cardOptions: null,
    phoneOption: null,
    paymentTypes: null,
	cryptoSelected: false,
	
    // methods
    init: function() {
      this.setPayments();
      this.eventManager();
    },

    setPayments: function() {
        this.paymentTypes = $$('.paym-type');
        this.cardOptions = $$('.creditCartOpt');
        this.phoneOption = $('phoneOpt');
    },

    eventManager: function() {
        var self = this;

        // setting events for card option
        this.cardOptions.removeEvents();
        this.cardOptions.addEvent('click', function() {
            self.paymentTypes.each(function(el) {
                if (el.hasClass('checked')) {
                    el.removeClass('checked');
                }
            });

            this.addClass('checked');
			
			// BTC payment price increase logic 
			if(this.get('data-id') == 'coinpass'){
				self.cryptoSelected = true;
				$$('.chf-price').addClass('none');
				$$('.crypto-price').removeClass('none');
			}else{
				self.cryptoSelected = false;
				$$('.crypto-price').addClass('none');
				$$('.chf-price').removeClass('none');
			}
        });

        // setting events for phone option
        this.phoneOption.removeEvents();
        this.phoneOption.addEvent('click', function() {
            self.paymentTypes.each(function(el) {
                el.removeClass('checked');
            });
			
            this.addClass('checked');
			// BTC payment price increase logic
			self.cryptoSelected = false;
			$$('.crypto-price').addClass('none');
			$$('.chf-price').removeClass('none');
        });
	},
	
	generatePrices: function(price, cryptoPrice){
		var priceText = `<span class="chf-price ${Cubix.OnlineBillingV4.Payments.hideWrongPrice(false)}"> ${price} </span>`;
		var cryptoPriceText = `<span class="crypto-price ${Cubix.OnlineBillingV4.Payments.hideWrongPrice(true)}" > ${cryptoPrice} </span>`;
		return priceText + cryptoPriceText;
	},
	
	hideWrongPrice: function(is_crypto){
		var self = this;
		if(is_crypto === self.cryptoSelected){
			return '';
		}else{
			return 'none';
		}
	}
	
};

// Current Package
Cubix.OnlineBillingV4.Gotd = {
  // defaults
  limit: 1,
  isEventSet: false,

  // parametrs
  currentPackage: null,
  gotdEl: null,

  // methods
  init: function() {
    this.setParams();
    this.setEls();
    this.disableGotd();
  },

  setParams: function() {
    this.currentPackage = CurrentPackage[0] || null;
  },

  setEls: function () {
      this.gotdEl = $('gotdPackage');
  },

  setGotdEvent: function(){
    var self = this;
    this.gotdEl.addEvent('click', function(){
      var gotdCartItem = self.getGotdCartItem();

      Cubix.OnlineBillingV4.GotdCalendar.gotdAreasEl.getElements('option')[0].set('selected', 'selected');
      Cubix.OnlineBillingV4.GotdCalendar.gotdSelectedArea = 0;

      if (!packageStartDate && self.curPackageExpirationDate && (this.getParent('.cart-item'))) {
        packageStartDate = self.curPackageExpirationDate;
        packageEndDate = parseInt(packageStartDate) + parseInt(packageDuration * 24 * 60 * 60 * 1000);
      }

      Cubix.OnlineBillingV4.GotdCalendar.setDateRange([(new Date).getTime(), self.currentPackage.expiration_date * 1000 ]);
      Cubix.OnlineBillingV4.GotdCalendar.gotdObjEl = {
        gotdViewEl: gotdCartItem.getElement('.sp-package-details').getElements('td')[2],
        gotdModelEl: gotdCartItem.getElement('.package-gotd-object')
      };

      Cubix.OnlineBillingV4.GotdCalendar.construct();
      Cubix.OnlineBillingV4.GotdCalendar.showCalendar($('spCart'), false);

      var xSymbol = gotdCartItem.getElements('.sp-delete-symbol');
      self.setGotdItemDeleteEvent(xSymbol);

      gotdCartItem.inject($('spCartBody'));
      $('spCart').fireEvent('cartChange');
      Cubix.OnlineBillingV4.Cart.paymentMethodsOverlay.enable();
    });
  },

  getGotdCartItem: function () {
    var self = this;
    var cartRow = new Element('tr', {
      html: '<td colspan="5"></td>',
      class: 'cart-item gotd-item'
    });

    var table = new Element('table', {
      'class': 'sp-package-table'
    });

    var tr1 = new Element('tr', {
      'class': 'sp-package-details'
    });

    var td11 = new Element('td', {
      html: '<span class="sp-delete-symbol">&#215;</span>' +
            '<span> Gotd</span>' +
            '<input type="hidden" class="package-duration" value="' + this.currentPackage.period + '">' +
            '<input type="hidden" class="package-price" value="0">' +
			'<input type="hidden" class="package-crypto-price" value="0">' +
            '<input type="hidden" class="package-id" value="' + this.currentPackage.package_id + '">'
    });

    var td12 = new Element('td', {
      html: '<span>' + this.currentPackage.date_activated + '</span>' +
            '<input class="package-activation-date" type="hidden" value="' +
            Date.parse(this.currentPackage.date_activated).getTime() + '">'
    });

    var td13 = new Element('td', {
      html: '<span class="sp-add-gotd">' + Cubix.OnlineBillingV4.Lang.add + '</span>' +
            '<input class="package-gotd-object" type="hidden">'
    });
    var td14 = new Element('td', {
      html: '&nbsp; N/A' +
                          '<input class="package-votd-object" type="hidden">'
    });
    var td15 = new Element('td', {
      html: '&nbsp; N/A' +
            '<input class="area-object" type="hidden">'
    });

    var td16 = new Element('td');

    var trs = new Elements([td11, td12, td13, td14, td15, td16]).inject(tr1);

    var tr2 = new Element('tr', {
        'class': 'sp-package-result'
    });

    var td21 = new Element('td'),
        td22 = new Element('td'),
        td23 = new Element('td'),
        td24 = new Element('td');
        td25 = new Element('td');

    var td26 = new Element('td', {
        html: '0 CHF',
        class: 'table-total'
    });

    $$('.sp-gt-desc')[0].set('html', '0 CHF');

    trs = new Elements([td21, td22, td23, td24, td25, td26]).inject(tr2);

    trs = new Elements([tr1, tr2]).inject(table);

    cartRow.getLast('td').adopt(table);

    table.getElements('.sp-add-gotd').each(function(el) {
      el.removeEvents();

      el.addEvent('click', function() {
        var packageDuration = this.getParent('table').getElements('tr')[0].getElements('td')[0].getElements('.package-duration')[0].get('value');
        var packageStartDate = this.getParent('table').getElements('tr')[0].getElements('td')[1].getElements('.package-activation-date')[0].get('value');

        packageStartDate = packageStartDate == '' ? (new Date()).getTime() : parseInt(packageStartDate);

        
        var packageEndDate = parseInt(packageStartDate) + parseInt(packageDuration * 24 * 60 * 60 * 1000);

        if (self.curPackageExpirationDate) {
          packageDate = d.getFullYear() + '-' + ('0' + (parseInt(d.getMonth()) + 1)).slice(-2) + '-' + ('0' + d.getDate()).slice(-2) + ' ' + ('0' + d.getHours()).slice(-2) + ':' + ('0' + d.getMinutes()).slice(-2);
        }

        Cubix.OnlineBillingV4.GotdCalendar.gotdAreasEl.getElements('option')[0].set('selected', 'selected');
        Cubix.OnlineBillingV4.GotdCalendar.gotdSelectedArea = 0;

        if (!packageStartDate && self.curPackageExpirationDate && (this.getParent('.cart-item'))) {
          packageStartDate = self.curPackageExpirationDate;
          packageEndDate = parseInt(packageStartDate) + parseInt(packageDuration * 24 * 60 * 60 * 1000);
        }

        Cubix.OnlineBillingV4.GotdCalendar.setDateRange([packageStartDate, packageEndDate]);
        Cubix.OnlineBillingV4.GotdCalendar.gotdObjEl = {
          gotdViewEl: this.getParent('td'),
          gotdModelEl: this.getSiblings('.package-gotd-object')[0]
        };
        Cubix.OnlineBillingV4.GotdCalendar.construct();
        Cubix.OnlineBillingV4.GotdCalendar.showCalendar(this, false);
      })
    });

    return cartRow;
  },

  setGotdItemDeleteEvent: function(el){
    var self = this;
    var cart = $('spCartBody');
    el.addEvent('click', function(){
      if (el.getParent('.gotd-item').getElements('.activated-gotd-days .sp-delete-symbol').length){
        el.getParent('.gotd-item').getElements('.activated-gotd-days .sp-delete-symbol').each(function(item){
          item.fireEvent('click');
            if (cart.getElements('.cart-item').length < Cubix.OnlineBillingV4.Cart.indepEscortLimit) {
                Cubix.OnlineBillingV4.Cart.paymentMethodsOverlay.disable();
            }
        });
      }

      el.getParent('.gotd-item').destroy();
      self.disableGotd();
    });
  },

  disableGotd: function(){
    if ($$('.gotd-item').length == this.limit){
      $('gotdPackage').removeEvents();
      this.gotdEl.setStyle('opacity', 0.5);
      this.isEventSet = false;
    } else {
      if (this.isEventSet == false){
        this.setGotdEvent();
        this.gotdEl.setStyle('opacity', 1);
        this.isEventSet = true;
      }
    }
  }
};

// Checkout
Cubix.OnlineBillingV4.Checkout = {
    // parametrs
    cart: null,
    checkoutBtn: null,
    checkoutBtnBlock: null,
    dayPassesPackages: [19, 20, 23],

    // methods
    init: function() {
      Cubix.OnlineBillingV4.Cart.paymentMethodsOverlay.disable();
      this.setCheckout();
      this.getCartChange();
      this.toggleCheckoutBtn();
      this.addToDb();
    },

    setCheckout: function() {
        this.cart = $('spCart');
        this.checkoutBtn = $('spCheckout');
        this.checkoutBtnBlock = $('payment-checkout-block');
    },

    getCartChange: function() {
        var self = this;

        this.cart.addEvent('cartChange', function() {
          var packages = $$('.package-id').get('value');
          var packages_price = $$('.package-price').get('value');
          var gotds = $$('.activated-gotd-days');
          var votds = $$('.activated-votd-days');
          var additionalAreas = $$('.area-object').get('value');

          var allowPhonePayment = self.checkPhonePayment(packages, packages_price, gotds, votds, additionalAreas);
          self.togglePhoneBtn(allowPhonePayment);
          self.toggleCheckoutBtn(packages);
        });
    },

    checkPhonePayment: function(packages, packages_price, gotds, votds, additionalAreas) {
        if (packages.length == 1) {
            if ((this.dayPassesPackages.indexOf(+packages[0]) >= 0 && gotds.length == 0 && votds.length == 0 && additionalAreas[0].split(',') == 0)
                || (packages_price[0] == 0 && gotds.length <= 1 && votds.length == 0) || (packages_price[0] == 0 && votds.length <= 1 && gotds.length == 0)) {
                return true;
            }
            return false;
        }
        return false;
    },

    addToDb: function() {
        this.checkoutBtn.addEvent('click', function() {
            if ($('yold_18') && $$('input[name=yold_18]:checked').length > 0) {
            var params_raw = {},
                params = {},
                cartItems = $('spCartBody').getElements('.cart-item'),
                escort_id,
                isOnlyGotd,
                isOnlyVotd,
                package_id,
                activation_date,
                gotd,
                area_ids,
                tempArr = [],
                tempArr1 = [],
                payment_type = $$('.payment-type-block')[0].getElement('.paym-type.checked').get('data-id'),
                payment_method = payment_type;

            cartItems.each(function(el, index) {
                isOnlyGotd = el.hasClass('gotd-item');
                isOnlyVotd = el.hasClass('votd-item');
                isPackage = el.hasClass('package-item');
                escort_id = +$('escortId').get('value');
                package_id = (!isOnlyGotd && !isOnlyVotd) ? +el.getElement('.package-id').get('value') : null;
                activation_date = el.getElement('.package-activation-date').get('value') / 1000 || '';
                gotd = [];
                votd = [];
                if(isPackage){
                    gotd = JSON.decode(el.getElement('.package-gotd-object').get('value')) || [];
                    votd = JSON.decode(el.getElement('.package-votd-object').get('value')) || [];
                } else if(isOnlyVotd){
                    votd = JSON.decode(el.getElement('.package-votd-object').get('value')) || [];
                } else if(isOnlyGotd){
                    gotd = JSON.decode(el.getElement('.package-gotd-object').get('value')) || [];
                }

                if (gotd) {
                    gotd.each(function(el) {
                        tempArr = [];

						el[Object.keys(el)[0]].split(',').each(function(item) {
							//tempArr.push(Date.parse(item) / 1000);
							tempArr.push(item);
						});
                        
                        el[Object.keys(el)[0]] = tempArr;
                    });
                }

                if (votd) {
                    votd.each(function(el) {
                        tempArr1 = [];

                        el[Object.keys(el)[0]].split(',').each(function(item) {
                            //tempArr1.push(Date.parse(item) / 1000);
                            tempArr1.push(item);
                        });
                        
                        el[Object.keys(el)[0]] = tempArr1;
                    });
                }

                area_ids = el.getElement('.area-object').get('value')  || null;
                area_ids = (area_ids && area_ids.length > 0) ? area_ids.split(',') : false;
                params_raw = {
                    'escort_id': escort_id,
                    'additional_regions': area_ids || [],
                    'gotd_days': gotd || [],
                    'votd_days': votd || [],
                    'package_id': package_id,
                    'activation_date': activation_date
                };

                params[index] = params_raw;
            });
            if (payment_method) {
              params.payment_method = payment_method;
            }

            var myRequest = new Request({
              data: params,
              url: '/online-billing-v4/generate-cart-item',
              method: 'post',

              onRequest: function() {
                $$('.big-loader').addClass('visible');
              },

              onSuccess: function(responseText) {
                var resp = JSON.decode(responseText);
                if (resp.status) {
                    $$('.big-loader').removeClass('visible');
                    if (params.payment_method === 'twispay' || params.payment_method === 'postfinance' || params.payment_method === 'coinpass') {
                        var form = $('formPlaceholder');
                        form.set('html', resp.form);
                        form.getElement('input[type="submit"]').click();
                    } else if (params.payment_method === 'epg' || params.payment_method === 'faxin') {
                        if (resp.status == 1) {
                            window.location.href = resp.url;
                        }else{
                            alert(resp.error);
                        }
                    } else {
                        window.location.href = resp.url;
                    }
                } else{
                   window.location.href = '/online-billing-v4/select-package?err=1'
                }
              }
            }).send();
            }else{
                $('18-yold-label').addClass('shaker');
                setTimeout(function(){ $('18-yold-label').removeClass('shaker'); }, 1000);
            }
        });
    },

    togglePhoneBtn: function(allowPhonePayment) {
        if (!allowPhonePayment) {
            $('phoneOpt').getParent('.sp-paym-type-button').set('style', 'display: none');
        } else {
            $('phoneOpt').getParent('.sp-paym-type-button').set('style', 'display: inline-block');
        }
    },

    toggleCheckoutBtn: function(packages) {
        if (packages) {
        }

        if (packages && packages.length > 0) {
            this.checkoutBtnBlock.set('style', 'display: flex');
            this.checkoutBtn.set('style', 'display: inline-block');
        } else {
            this.checkoutBtnBlock.set('style', 'display: none');
            this.checkoutBtn.set('style', 'display: none');
        }
    }
}

//VOTD



//VOTD CALENDAR

Cubix.OnlineBillingV4.VotdCalendar = {
    // default
    indepVotdLimit: 0,

    // Gotd Calendar properties

    dateRange: [],
    votdAreas: [],
    daysCount: 0,
    votdCalendarSelectedDates: [],
    votdCalendarEl: null,
    votdAreasEl: null,
    votdSelectedArea: 0,
    votdCalendarCloseBtn: null,
    votdCalendarBackBtn: null,
    votdCalendarMonthYearEl: null,
    votdCalendarForwardBtn: null,
    votdCalendarDaysEl: null,
    votdCalendarTotalPriceEl: null,
    votdCalendarAddBtn: null,
    votdBookedDays: null,
    votdCurrentBookedDates: [],
    votdPreselectedDates: [],
    votdObjEl: null,
    votdPrice: 0,

    // Gotd Calendar methods

    init: function() {
      this.getVotdCalendarView();
      this.setDaysCount(42);
      this.setAreas(this.votdAreasList.get('value'));
      this.construct();
      this.eventManager();
      this.votdBookedDays = $('votdBookedDays').get('value');
    },

    getVotdCalendarView: function() {
        this.votdCalendarEl = $('votdCalendar');
        this.votdAreasEl = $('votdAreas');
        this.votdCalendarCloseBtn = $('vCalCloseBtn');
        this.votdCalendarBackBtn = $('vCalBackBtn');
        this.votdCalendarMonthYearEl = $('vCalMonthYear');
        this.votdCalendarForwardBtn = $('vCalForwardBtn');
        this.votdCalendarDaysEl = $('vCalDays');
        this.votdCalendarTotalPriceEl = $('votdTotalPrice');
        this.votdCalendarAddBtn = $('vCalAddBtn');
        this.votdCalendarSelectedDates = $('votdSelectedDates');
        this.votdPrice = $('votdPrice').get('value');

        // Data from backend
        this.votdAreasList = $('escortAreas');
    },

    construct: function(vCalMonth, direction) {

        var self = this;
        self.updatePrice(true);
        // adding the days list elements to the container
        this.votdCalendarDaysEl.set('html', '');
        for (var i = 0; i < this.daysCount; i++) {
            this.votdCalendarDaysEl.adopt(new Element('li'));
        }

        // adoping the container to the view
        this.votdCalendarDaysEl.adopt(new Element('div.clear'));

        // adding available areas to the areas selector
        if (!this.votdSelectedArea) {
            this.votdAreasEl.getElements('option').destroy();

            this.votdAreasEl.adopt(new Element('option', {
                'html': '---',
                'data-id': ''
            }));

              for (var i = this.votdAreas.length - 1; i >= 0; i--) {
                this.votdAreasEl.adopt(new Element('option', {
                    'html': this.votdAreas[i].title,
                    'data-id': this.votdAreas[i].id
                }))
            }
        }

        // filling the calendar dates
        // defining the first date of the month by package available first date
        if (!vCalMonth) {
            var dateRangeStart = new Date();
            var monthFirstDate = new Date(dateRangeStart.getFullYear(), dateRangeStart.getMonth(), 1);
        } else {
            var dateRangeStart = monthFirstDate = new Date(vCalMonth.split(',')[1], vCalMonth.split(',')[0], 1);
        }

        // getting the first day on the calendar
        var calStartDate = new Date(monthFirstDate.getTime() - (monthFirstDate.getDay() == 0 ? 6 : monthFirstDate.getDay() - 1) * 24 * 60 * 60 * 1000);
        var cDaysFields = this.votdCalendarDaysEl.getElements('li');
        var monthNames = Cubix.OnlineBillingV4.Lang.months;

        // inserting the month and year
        this.votdCalendarMonthYearEl.set('html', monthNames[dateRangeStart.getMonth()] + ' ' + dateRangeStart.getFullYear());
        cDaysFields.each(function(el) {
            el.removeAttribute('class');
            el.highlight('#9fa2a8');
            el.removeEvents();
        });

        // filling the dates in the calendar
        for (var i = cDaysFields.length - 1; i >= 0; i--) {
          var d = new Date(calStartDate).increment('day', i);

          cDaysFields[i].set('html', d.getDate());
          cDaysFields[i].set('data-value', d.getFullYear() + '-' + (parseInt(d.getMonth()) + 1) + '-' + d.getDate());

          if (!self.votdSelectedArea) {
            cDaysFields[i].addEvent('click', function() {
              if (self.votdAreasEl.getParent('div').getElements('.err-msg').length <= 0) {
                self.createErrTooltip(self.votdAreasEl, Cubix.OnlineBillingV4.Lang.errs.selectRegion)
              } else {
                self.votdAreasEl.highlight();
              }
            });
          } else {
            var currentPackageItem = $$('.cart-item-votd')[0],
                b = new Date(self.dateRange[0]);

            b = (new Date(b.getFullYear(), b.getMonth(), b.getDate())).getTime();

            var c = new Date(this.dateRange[1]);
            c = (new Date(c.getFullYear(), c.getMonth(), c.getDate())).getTime() - (1 * 24 * 60 * 60 * 1000);

            if (d.getTime() >= b && d.getTime() <= c) {
              cDaysFields[i].set('class', 'active');
              cDaysFields[i].addEvent('click', function() {
                if (this.hasClass('active')) {
                  this.removeClass('active');
                  this.addClass('selected');

                  var v = self.votdCalendarSelectedDates.get('value');
                  self.votdCalendarSelectedDates.set('value', v + (v.length > 0 ? ',' : '') +
                      this.get('data-value'));

                  if (self.votdCalendarSelectedDates.get('value')) {
                    self.votdCalendarAddBtn.removeAttribute('class');
                    self.votdCalendarAddBtn.set('class', 'active');
                    self.votdCalendarAddBtn.removeEvents();

                    self.updatePrice(false);
                    self.votdCalendarAddBtn.addEvent('click', function() {
                      self.addAction(self.votdCalendarSelectedDates.get('value'),
                                     self.votdAreasEl.getSelected().get('html'));

                    });
                  } else {
                    self.updatePrice(true);
                  }

                } else if (this.hasClass('selected')) {
                  this.removeClass('selected');
                  this.addClass('active');

                  var x = self.votdCalendarSelectedDates.get('value');
                  var y = x.split(',');

                  var index = y.indexOf(this.get('data-value'));

                  if (index > -1) {
                      y.splice(index, 1);
                  }

                  self.votdCalendarSelectedDates.set('value', y.toString());

                  if (self.votdCalendarSelectedDates.get('value')) {
                    self.votdCalendarAddBtn.removeAttribute('class');
                    self.votdCalendarAddBtn.set('class', 'active');
                    self.votdCalendarAddBtn.removeEvents();

                    self.votdCalendarAddBtn.addEvent('click', function() {
                        self.addAction(self.votdCalendarSelectedDates.get('value'));
                    });
                    self.updatePrice(false);

                  } else {
                    self.votdCalendarAddBtn.removeAttribute('class');
                    self.votdCalendarAddBtn.set('class', 'inactive');
                    self.votdCalendarAddBtn.removeEvents();
                    //self.updatePrice(false);
                    self.updatePrice(true);
                  }
                }
              });
            }

            if (self.votdCurrentBookedDates.indexOf(d.getTime()) != -1 && d.getTime() >= b && d.getTime() <= c) {
                cDaysFields[i].removeEvents();
                cDaysFields[i].set('class', 'booked');
            }

            if (self.votdPreselectedDates.indexOf(d.getTime()) != -1 && d.getTime() >= b && d.getTime() <= c) {
                cDaysFields[i].removeEvents();
                cDaysFields[i].set('class', 'preselected');
            }
          }

          // set Button values
          var lastMonthArr = [new Date(monthFirstDate.getTime() - 15 * 24 * 60 * 60 * 1000).getMonth(), new Date(monthFirstDate.getTime() - 15 * 24 * 60 * 60 * 1000).getFullYear()]; // 15days is just a value to get the month and year
          var nextMonthArr = [new Date(monthFirstDate.getTime() + 45 * 24 * 60 * 60 * 1000).getMonth(), new Date(monthFirstDate.getTime() + 45 * 24 * 60 * 60 * 1000).getFullYear()]; // 45days is just a value to get the month and year
          this.votdCalendarBackBtn.set('data-value', lastMonthArr);
          this.votdCalendarForwardBtn.set('data-value', nextMonthArr);
        }
    },

    setDaysCount: function(days) {
        this.daysCount = days;
    },

    setDateRange: function(dateRange) {
        this.dateRange = dateRange;
    },

    setAreas: function(areas) {
        this.votdAreas = JSON.parse(areas);
    },

    updatePrice: function(remove) {
        if (!remove) {
            var total = this.votdPrice * this.votdCalendarSelectedDates.get('value').split(',').length;
            this.votdCalendarTotalPriceEl.set('html', 'Total ' + total + ' CHF');
        } else {
            this.votdCalendarTotalPriceEl.set('html', 'Total 0 CHF');
        }
    },

    showCalendar: function(viewEl) {

        var self = this;

        // setting the position of the calendar on the page
        $(document.body).scrollTo(0, viewEl.getPosition().y - window.getSize().y * 0.85);

        this.votdCalendarEl.setStyle('top', (viewEl.getPosition().y < 700) ? (viewEl.getPosition().y - 565) : window.getSize().y * 0.85 - 565);
        this.votdCalendarEl.setStyle('left', viewEl.getPosition().x - 25);
        this.construct(false, false);

        this.votdCalendarEl.show();
        Cubix.OnlineBillingV4.Packages.loadOverlay();

        // adding Event listerner to document, to close calendar when 'Esc' is pressed
        document.addEvent('keyup', function(e) {
            e = e.event;
            var key = e.keyCode || e.which;
            if (key == 27) {
                self.hideCalendar();
                $$('.act-type')[0].fireEvent('click');
            }
        });
    },

    hideCalendar: function() {
        this.votdCalendarEl.hide();
        Cubix.OnlineBillingV4.Packages.unloadOverlay();
    },

    eventManager: function() {
        var self = this;

        this.votdCalendarCloseBtn.addEvent('click', function() {
          if ($$('.votd-item').length && $$('.votd-item .activated-votd-days').length == 0){
            $$('.votd-item .sp-delete-symbol')[0].fireEvent('click');
          }

          self.hideCalendar();
        });

        // selecting the gotd area
        this.votdAreasEl.addEvent('change', function() {
            var areaId = this.getSelected().get('data-id');
            self.votdCalendarAddBtn.removeAttribute('class');
            self.votdCalendarAddBtn.removeEvents();
            
            self.votdSelectedArea = areaId;

            if (areaId) {
                if (self.votdAreasEl.getParent('div').getElements('.err-msg').length > 0) {
                    self.votdAreasEl.getParent('div').getElements('.err-msg').destroy();
                }
            }


            var x = JSON.parse(self.votdBookedDays);
            var bookedDaysObj = x[areaId];

            if (bookedDaysObj) {
                var bookedDates = [];

                for (var i = bookedDaysObj.length - 1; i >= 0; i--) {
                    bookedDates.push((new Date(bookedDaysObj[i]['date'])).clearTime().getTime());
                }

                self.votdCurrentBookedDates = bookedDates;

            } else {
                self.votdCurrentBookedDates = '';
            }

            self.votdCalendarSelectedDates.set('value', '');
            self.construct(false, false);
        });

        // behavior of calendar back end forward buttons
        self.votdCalendarBackBtn.addEvent('click', function() {
            var vCalMonth = this.getAttribute('data-value');
            self.votdCalendarSelectedDates.set('value', '');
            self.construct(vCalMonth, 'b');
        });

        self.votdCalendarForwardBtn.addEvent('click', function() {
            var vCalMonth = this.getAttribute('data-value');
            self.votdCalendarSelectedDates.set('value', '');
            self.construct(vCalMonth, 'f');
        });
    },

    addAction: function(strDates, area) {
      var self = this,
          arrStrDatesTime = [],
          arrStrDate = strDates.split(',');
        for (var i = arrStrDate.length - 1; i >= 0; i--) {
          this.votdObjEl.votdViewEl.adopt(new Element('span', {
            html: '<br><span class="sp-delete-symbol">&#215;</span> <span>' + arrStrDate[i] +
                  '<span class="votd-city">' + area + '</span></span>',
            'data-date': arrStrDate[i],
            class: 'activated-votd-days'
          }));

          this.votdPreselectedDates.push((new Date(arrStrDate[i] + ' 00:00')).getTime());
          $('spCart').fireEvent('cartChange');
        };

        $$('.sp-delete-symbol').each(function(el) {
          el.addEvent('click', function() {
            var xDate = el.getParent('span').get('data-date');
            this.getSiblings('br').destroy();
            this.getParent('span').destroy();

            var arrAreaVotdDates = JSON.parse(self.votdObjEl.votdModelEl.get('value'));

            for (var i = arrAreaVotdDates.length - 1; i >= 0; i--) {

                for (var key in arrAreaVotdDates[i]) {
                    var arrZDate = arrAreaVotdDates[i][key].split(',');

                    if (arrZDate.indexOf(xDate) > -1) {
                        arrZDate.splice(arrZDate.indexOf(xDate), 1);
                    }

                    arrAreaVotdDates[i][key] = arrZDate.toString();

                    if (arrZDate.length == 0) {
                        arrAreaVotdDates.splice(i, 1);
                    }
                }
            }

            Cubix.OnlineBillingV4.Cart.updateCartPrice();

            self.votdObjEl.votdModelEl.set('value', JSON.encode(arrAreaVotdDates));

            var xPreDates = [];
            self.votdPreselectedDates = [];

            for (var i = arrAreaVotdDates.length - 1; i >= 0; i--) {
              for (var preKey in arrAreaVotdDates[i]) {
                xPreDates = arrAreaVotdDates[i][preKey].split(',');
                for (var i = xPreDates.length - 1; i >= 0; i--) {
                    self.votdPreselectedDates.push((new Date(xPreDates[i] + ' 00:00')).getTime());
                }
              }
            }

            if ($$('.votd-item').length) {
              if ($$('.votd-item').getElements('.activated-votd-days .sp-delete-symbol') &&
                  $$('.votd-item').getElements('.activated-votd-days .sp-delete-symbol')[0].length == 0){
                  $$('.votd-item').getElement('.sp-delete-symbol').fireEvent('click');
              }
            }
            $('spCart').fireEvent('cartChange');
          });
        });

        var arrO = [];
        var arrPackageVotdObj = this.votdObjEl.votdModelEl.get('value');
        if (arrPackageVotdObj) {
            for (var i = JSON.parse(arrPackageVotdObj).length - 1; i >= 0; i--) {
                arrO.push(JSON.parse(arrPackageVotdObj)[i]);
            }
        }

        var o = {};
        o[this.votdSelectedArea] = strDates;
        arrO.push(o);

        this.votdObjEl.votdModelEl.set('value', JSON.stringify(arrO));

        self.votdCalendarSelectedDates.set('value', '');
        this.hideCalendar();

        Cubix.OnlineBillingV4.Cart.updateCartPrice();
        if (Cubix.OnlineBillingV4.Votd.currentPackage){
          Cubix.OnlineBillingV4.Votd.disableVotd();
        }
        self.votdCalendarAddBtn.removeAttribute('class');
        self.votdCalendarAddBtn.removeEvents();
        $('spCart').fireEvent('cartChange');
    },

    createErrTooltip: function(el, msg) {
        var x = el.getPosition().x;
        var y = el.getPosition().y;

        var errMsg = new Element('span', {
            'class': 'err-msg',
            'html': msg,
            'styles': {
                'top': 0,
                'left': '50%',
                'transform': 'translate(-50%,-100%)'
            }
        });

        el.getParent('div').adopt(errMsg);
        errMsg.fade('in');
        errMsg.setStyle('margin-top', -10);
    }
};

Cubix.OnlineBillingV4.Votd = {
  // defaults
  limit: 1,
  isEventSet: false,

  // parametrs
  currentPackage: null,
  votdEl: null,

  // methods
  init: function() {
    this.setParams();
    this.setEls();
    this.disableVotd();
  },

  setParams: function() {
    this.currentPackage = CurrentPackage[0] || null;
  },

  setEls: function () {
      this.votdEl = $('votdPackage');
  },

  setVotdEvent: function(){
    var self = this;
    this.votdEl.addEvent('click', function(){
      var votdCartItem = self.getVotdCartItem();

      Cubix.OnlineBillingV4.VotdCalendar.votdAreasEl.getElements('option')[0].set('selected', 'selected');
      Cubix.OnlineBillingV4.VotdCalendar.votdSelectedArea = 0;

      if (!packageStartDate && self.curPackageExpirationDate && (this.getParent('.cart-item'))) {
        packageStartDate = self.curPackageExpirationDate;
        packageEndDate = parseInt(packageStartDate) + parseInt(packageDuration * 24 * 60 * 60 * 1000);
      }

      Cubix.OnlineBillingV4.VotdCalendar.setDateRange([(new Date).getTime(), self.currentPackage.expiration_date * 1000 ]);
      Cubix.OnlineBillingV4.VotdCalendar.votdObjEl = {
        votdViewEl: votdCartItem.getElement('.sp-package-details').getElements('td')[3],
        votdModelEl: votdCartItem.getElement('.package-votd-object')
      };

      Cubix.OnlineBillingV4.VotdCalendar.construct();
      Cubix.OnlineBillingV4.VotdCalendar.showCalendar($('spCart'), false);

      var xSymbol = votdCartItem.getElements('.sp-delete-symbol');
      self.setVotdItemDeleteEvent(xSymbol);

      votdCartItem.inject($('spCartBody'));
      $('spCart').fireEvent('cartChange');
      Cubix.OnlineBillingV4.Cart.paymentMethodsOverlay.enable();
    });
  },

  getVotdCartItem: function () {
    var self = this;
    var cartRow = new Element('tr', {
      html: '<td colspan="5"></td>',
      class: 'cart-item votd-item'
    });

    var table = new Element('table', {
      'class': 'sp-package-table'
    });

    var tr1 = new Element('tr', {
      'class': 'sp-package-details'
    });

    var td11 = new Element('td', {
      html: '<span class="sp-delete-symbol">&#215;</span>' +
            '<span> Votd</span>' +
            '<input type="hidden" class="package-duration" value="' + this.currentPackage.period + '">' +
            '<input type="hidden" class="package-price" value="0">' +
			'<input type="hidden" class="package-crypto-price" value="0">' +
            '<input type="hidden" class="package-id" value="' + this.currentPackage.package_id + '">'
    });

    var td12 = new Element('td', {
      html: '<span>' + this.currentPackage.date_activated + '</span>' +
            '<input class="package-activation-date" type="hidden" value="' +
            Date.parse(this.currentPackage.date_activated).getTime() + '">'
    });
    var td13 = new Element('td', {
      html: '&nbsp; N/A'
    });
    var hasApprovedVideo = $('hasApprovedVideo').get('value');
    if(hasApprovedVideo == 1){

        var td14 = new Element('td', {
          html: '<span class="sp-add-votd">'+ Cubix.OnlineBillingV4.Lang.add +'</span>' +
                '<input class="package-votd-object" type="hidden">'
        });
    } else{
        var td14 = new Element('td', {
          html: '&nbsp; N/A' +
                          '<input class="package-votd-object" type="hidden">'
        });
    }
    

    var td15 = new Element('td', {
      html: '&nbsp; N/A' +
            '<input class="area-object" type="hidden">'
    });

    var td16 = new Element('td');

    var trs = new Elements([td11, td12, td13, td14, td15, td16]).inject(tr1);

    var tr2 = new Element('tr', {
        'class': 'sp-package-result'
    });

    var td21 = new Element('td'),
        td22 = new Element('td'),
        td23 = new Element('td'),
        td24 = new Element('td');
        td25 = new Element('td');

    var td26 = new Element('td', {
        html: '0 CHF',
        class: 'table-total'
    });

    $$('.sp-gt-desc')[0].set('html', '0 CHF');

    trs = new Elements([td21, td22, td23, td24, td25, td26]).inject(tr2);

    trs = new Elements([tr1, tr2]).inject(table);

    cartRow.getLast('td').adopt(table);

    table.getElements('.sp-add-votd').each(function(el) {
      el.removeEvents();

      el.addEvent('click', function() {
        var packageDuration = this.getParent('table').getElements('tr')[0].getElements('td')[0].getElements('.package-duration')[0].get('value');
        var packageStartDate = this.getParent('table').getElements('tr')[0].getElements('td')[1].getElements('.package-activation-date')[0].get('value');

        packageStartDate = packageStartDate == '' ? (new Date()).getTime() : parseInt(packageStartDate);

        
        var packageEndDate = parseInt(packageStartDate) + parseInt(packageDuration * 24 * 60 * 60 * 1000);

        if (self.curPackageExpirationDate) {
          packageDate = d.getFullYear() + '-' + ('0' + (parseInt(d.getMonth()) + 1)).slice(-2) + '-' + ('0' + d.getDate()).slice(-2) + ' ' + ('0' + d.getHours()).slice(-2) + ':' + ('0' + d.getMinutes()).slice(-2);
        }

        Cubix.OnlineBillingV4.VotdCalendar.votdAreasEl.getElements('option')[0].set('selected', 'selected');
        Cubix.OnlineBillingV4.VotdCalendar.votdSelectedArea = 0;

        if (!packageStartDate && self.curPackageExpirationDate && (this.getParent('.cart-item'))) {
          packageStartDate = self.curPackageExpirationDate;
          packageEndDate = parseInt(packageStartDate) + parseInt(packageDuration * 24 * 60 * 60 * 1000);
        }

        Cubix.OnlineBillingV4.VotdCalendar.setDateRange([packageStartDate, packageEndDate]);
        Cubix.OnlineBillingV4.VotdCalendar.votdObjEl = {
          votdViewEl: this.getParent('td'),
          votdModelEl: this.getSiblings('.package-votd-object')[0]
        };
        Cubix.OnlineBillingV4.VotdCalendar.construct();
        Cubix.OnlineBillingV4.VotdCalendar.showCalendar(this, false);
      })
    });

    return cartRow;
  },

  setVotdItemDeleteEvent: function(el){
    var self = this;
    var cart = $('spCartBody');
    el.addEvent('click', function(){
      if (el.getParent('.votd-item').getElements('.activated-votd-days .sp-delete-symbol').length){
        el.getParent('.votd-item').getElements('.activated-votd-days .sp-delete-symbol').each(function(item){
          item.fireEvent('click');
          if (cart.getElements('.cart-item').length < Cubix.OnlineBillingV4.Cart.indepEscortLimit) {
                Cubix.OnlineBillingV4.Cart.paymentMethodsOverlay.disable();
            }
        });
      }

      el.getParent('.votd-item').destroy();
      self.disableVotd();
    });
  },

  disableVotd: function(){
    if ($$('.votd-item').length == this.limit){
      $('votdPackage').removeEvents();
      this.votdEl.setStyle('opacity', 0.5);
      this.isEventSet = false;
    } else {
      if (this.isEventSet == false){
        this.setVotdEvent();
        this.votdEl.setStyle('opacity', 1);
        this.isEventSet = true;
      }
    }
  }
};

Cubix.OnlineBillingV4.VotdPopup = {
    init: function() {
        var overlay = new Cubix.Overlay($('container'), { has_loader: false });;
        $('votdPackage').addEvent('click', function(){
            overlay.disable();
            $('votd-popup').setStyle('display', 'block');
        });
        $('votd-popup-close-btn').addEvent('click', function(){
            overlay.enable();
            $('votd-popup').setStyle('display', 'none');
        });
    },
}
