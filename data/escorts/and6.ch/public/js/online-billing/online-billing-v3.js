Cubix.OnlineBillingV3 = {}

window.addEvent('domready', function() {
	Cubix.OnlineBillingV3.loader = new Cubix.Overlay($('online-billing-v3'), {loader: _st('loader-small.gif'), position: '50%', no_relative: true});
})

Cubix.OnlineBillingV3.initEscortSelect = function() {
	$$('.escort-item').addEvent('click', function(e) {
		Cubix.OnlineBillingV3.loader.disable();
		if ( e.target.hasClass('add-gotd-btn') ) {
			$('pay-for-gotd').set('value', 1);
		}
		$('escort-id').set('value', this.get('data-id'));
		$$('form')[0].submit();
	});
}


Cubix.OnlineBillingV3.initPackages = function() {
	
	var datepicker = new DatePicker($('activation-date'), {
		allowEmpty: true,
		format: "%d %b %Y %H:%M",
		timePicker: true,
		positionOffset: { x: 130, y: -60 },
		draggable: false,
		minDate: new Date()
	});
	
	$$('.activation-type-block .act-type').addEvent('click', function() {
		$$('.activation-type-block .act-type').removeClass('checked');
		this.addClass('checked');
		
		
		
		if ( this.hasClass('spec-date') ) {
			$('activation-date').removeAttribute('disabled');
		} else {
			$('activation-date').set('value', '').setAttribute('disabled', 'disabled');
		}
		
	});
	
	$$('.package-row .tooltip').addEvent('mouseover', function(e) {
		e.target.getElements('.body').removeClass('none')
		.addEvent('mouseleave', function(b) {
			e.target.getElements('.body').addClass('none');
		});
	});
	
	$$('.package-row').addEvent('click', function(e) {
		Cubix.OnlineBillingV3.loader.disable();
		$('package-id').set('value', this.get('data-id'));
		$$('form')[0].submit();
	});

	$$('.activation-type-block .tooltip').addEvent('mouseover', function(e) {
		e.target.getElements('.body').removeClass('none')
	}).addEvent('mouseleave', function(e) {
		e.target.getElements('.body').addClass('none')
	});
	
}


Cubix.OnlineBillingV3.initOptionalProducts = function() {
	$$('.additional-area-block li').addEvent('click', function() {
		if ( this.hasClass('checked') ) {
			this.removeClass('checked');
		} else {
			this.addClass('checked');
		}
	});
	
	Cubix.OnlineBillingV3.Calendar.init();
	
	$$('.buy-btn').addEvent('click', function() {
		Cubix.OnlineBillingV3.loader.disable();
		
		if ( $$('.additional-area-block').length ) {
			$('additional-regions').set('value', $$('.additional-area-block li.checked').get('rel').join(','));
		}
			$$('form')[0].submit();
	});
	
	$$('.additional-area-block li').addEvent('click', function() {
		Cubix.OnlineBillingV3.calcOptProductsPrice();
	});
	
	$$('.collapse-expand-btn').addEvent('click', function() {
		if ( this.hasClass('expand-arrow') ) {
			this.getNext('.expandable').addClass('none');
			this.removeClass('expand-arrow').addClass('collapse-arrow');
		} else {
			this.getNext('.expandable').removeClass('none');
			this.removeClass('collapse-arrow').addClass('expand-arrow');
		}
	});
}

Cubix.OnlineBillingV3.initPayment = function() {
	$$('.payment-method .chk').addEvent('click', function(e) {
		if ( this.getParent('.payment-method').hasClass('disabled') ) return;
		$$('.payment-method .chk').removeClass('checked');
		this.addClass('checked');
		
		if ( this.getParent('.payment-method').hasClass('phone') ) {
			$('payment-method').set('value', 'phone');
		} else {
			$('payment-method').set('value', 'card');
		}
	});
	
	$$('.online-billing-table .remove-row-btn').addEvent('click', function() {
		if ( confirm('Are you sure you want to remove that item from your order ?') ) {
			Cubix.OnlineBillingV3.loader.disable();
			new Request({
				url: '/online-billing-v3/remove-from-cart?escort_id=' + this.get('rel'),
				method: 'post',
				onComplete: function() {
					location.reload();
				}
			}).send();
		}
	});
	
	$$('.checkout-btn').addEvent('click', function() {
		$$('form')[0].submit();
	});
	
	$$('.payment-method .tooltip').addEvent('mouseover', function(e) {
		e.target.getElements('.body').removeClass('none')
	}).addEvent('mouseleave', function(e) {
		e.target.getElements('.body').addClass('none')
	});
}


var monthNames = ["February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December", "January"],
	weekDays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
	daysInMonth = [28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31, 31];


Cubix.OnlineBillingV3.Calendar = {};
Cubix.OnlineBillingV3.Calendar.cYear = new Date().getFullYear();
Cubix.OnlineBillingV3.Calendar.cMonth = new Date().getMonth() == 0 ? 12 : new Date().getMonth();
Cubix.OnlineBillingV3.Calendar.cDay = new Date().getDate();
Cubix.OnlineBillingV3.Calendar.selectedDays = {};
Cubix.OnlineBillingV3.Calendar.bookedDays = {};
Cubix.OnlineBillingV3.Calendar.regionRelations = {};
Cubix.OnlineBillingV3.Calendar.selectedRegion;
Cubix.OnlineBillingV3.Calendar.init = function() {
	Cubix.OnlineBillingV3.Calendar.bookedDays = JSON.decode($('gotd-booked-days').get('value'));
	Cubix.OnlineBillingV3.Calendar.regionRelations = JSON.decode($('region-relations').get('value'));
	Cubix.OnlineBillingV3.Calendar.drawMonth(Cubix.OnlineBillingV3.Calendar.cYear, Cubix.OnlineBillingV3.Calendar.cMonth, true)
	
	
	if ( $('package-ranges') ) {
		var packageRanges = $('package-ranges').get('value').split(';');
		packageRanges.each(function(range) {
			var range = range.split(':');
			var startDate = range[0].split('.'),
				duration = range[1];
			var d = Cubix.OnlineBillingV3.Calendar.dateBringToJsFormat(startDate[0], startDate[1], startDate[2])
			Cubix.OnlineBillingV3.Calendar.setActiveDays(d, duration);
		});
	}
	
	$$('#calendar .prev').addEvent('click', function() {
		if ( Cubix.OnlineBillingV3.Calendar.cMonth == 12 ) {
			Cubix.OnlineBillingV3.Calendar.cYear -= 1;
			Cubix.OnlineBillingV3.Calendar.cMonth = 11;
		} else if ( Cubix.OnlineBillingV3.Calendar.cMonth == 1 ) {
			Cubix.OnlineBillingV3.Calendar.cMonth = 12;
		} else {
			Cubix.OnlineBillingV3.Calendar.cMonth -= 1;
		}
		Cubix.OnlineBillingV3.Calendar.drawMonth(Cubix.OnlineBillingV3.Calendar.cYear, Cubix.OnlineBillingV3.Calendar.cMonth, true)
	});
	$$('#calendar .next').addEvent('click', function() {
		if ( Cubix.OnlineBillingV3.Calendar.cMonth == 11 ) {
			Cubix.OnlineBillingV3.Calendar.cYear += 1;
			Cubix.OnlineBillingV3.Calendar.cMonth = 12;
		} else if ( Cubix.OnlineBillingV3.Calendar.cMonth == 12 ) {
			Cubix.OnlineBillingV3.Calendar.cMonth = 1;
		} else {
			Cubix.OnlineBillingV3.Calendar.cMonth += 1;
		}
		Cubix.OnlineBillingV3.Calendar.drawMonth(Cubix.OnlineBillingV3.Calendar.cYear, Cubix.OnlineBillingV3.Calendar.cMonth, true)
	});
	
	$('gotd-region').addEvent('change', function() {
		Cubix.OnlineBillingV3.Calendar.reset(['booked']);
		Cubix.OnlineBillingV3.Calendar.setSelectedDays();
		
		if ( ! this.get('value') ) return;
		Cubix.OnlineBillingV3.Calendar.fillBookedDays(this.get('value'));
		
	});
	if ( $('gotd-package') ) {
		Cubix.OnlineBillingV3.Calendar.selectedRegion = $('gotd-package').get('value');
		$('gotd-package').addEvent('change', function() {
			if ( $$('input[name="gotd_days[]"]').length && ! confirm('You can select gotd only for one package per one transaction. \n\
				All your selected dates will be removed.') ) {
				this.set('value', Cubix.OnlineBillingV3.Calendar.selectedRegion);
				return;
			}
			Cubix.OnlineBillingV3.Calendar.selectedRegion = this.get('value');
			$('selected-days').empty();
			Cubix.OnlineBillingV3.Calendar.reset(['active', 'selected', 'selected-light']);

			startDate = this.getSelected()[0].get('data-activation-date');
			duration = this.getSelected()[0].get('data-period');
			startDate = startDate.split('-');

			var d = Cubix.OnlineBillingV3.Calendar.dateBringToJsFormat(startDate[0], startDate[1], startDate[2])

			Cubix.OnlineBillingV3.Calendar.setActiveDays(d, duration);
		});
	}
	
	if ( $$('#selected-days li').length ) {
		Cubix.OnlineBillingV3.Calendar.setSelectedDays();
		$$('#selected-days .remove-btn').addEvent('click', function() {
			this.getParent('li').destroy();
			Cubix.OnlineBillingV3.Calendar.setSelectedDays();
		});
	}
}

Cubix.OnlineBillingV3.Calendar.drawMonth = function(year, month, goTo) {
	if ( $$('#calendar ul[data-year="' + year + '"][data-month="' + month + '"]').length ) {
		if ( goTo ) {
			Cubix.OnlineBillingV3.Calendar.goTo(year, month);
		}
	} else {
		var ul = new Element('ul',{
			'data-year': year,
			'data-month': month,
			'class': 'none'
		});

		var monthLength = daysInMonth[month - 1],
			previousMonthLength = daysInMonth[month == 1 ? 11 : month - 2];
		
		if (month == 1) {
			if ( (year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
				monthLength = 29;
			}
		}
		
		if ( month == 12 ) {
			startWeekDay = new Date(year - 1, month, 1).getDay();
		} else {
			startWeekDay = new Date(year, month, 1).getDay();
		}
		
		if ( startWeekDay == 0 ) startWeekDay = 7;
		var daysToPrepend = startWeekDay - 1;
		var daysToAppend = 42 - monthLength - daysToPrepend;
		
		
		for(var i = previousMonthLength - daysToPrepend + 1; i <= previousMonthLength ; i++) {
			new Element('li', {
				'id': 'previous-' +  (month == 12 ? year - 1 : year) + '-' + (month == 1 ? 12 : month - 1) + '-' + i,
				html: i
			}).inject(ul);
		}
		
		for(var i = 1; i <= monthLength; i++) {
			new Element('li', {
				'id': + year + '-' + month + '-' + i,
				html: i
			}).inject(ul);
		}
		
		for(var i = 1; i <= daysToAppend; i++) {
			new Element('li', {
				'id': + 'next-' +   (month == 11 ? year + 1 : year )  + '-' + (month == 12 ?  1 : month + 1) + '-' + i,
				html: i
			}).inject(ul);
		}

		ul.inject($$('#calendar .days')[0]);

		if ( goTo ) {
			Cubix.OnlineBillingV3.Calendar.goTo(year, month);
		}
	}
}

Cubix.OnlineBillingV3.Calendar.goTo = function(year, month) {
	$$('#calendar .days ul').addClass('none');
	$$('#calendar ul[data-year="' + year + '"][data-month="' + month + '"]').removeClass('none');
	$$('#calendar .month span').set('html', year.toString() + ' ' + monthNames[month - 1]);
}

Cubix.OnlineBillingV3.Calendar.setActiveDays = function(date, duration) {
	var currDate = new Date();
	if ( currDate > date ) {
		var timeDiff = Math.abs(currDate.getTime() - date.getTime());
		var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)) - 1;
		date = currDate;
		duration -= diffDays;
	}
	
	for( var i = 0; i < duration; i++ ) {
		date.setDate(date.getDate() + (i == 0 ? 0 : 1) );
		
		Cubix.OnlineBillingV3.Calendar.drawMonth(date.getFullYear(), date.getMonth() == 0 ? 12 : date.getMonth());
		
		$$('#' + date.getFullYear() + '-' + (date.getMonth() == 0 ? 12 : date.getMonth()) + '-' + date.getDate() ).addClass('active');
	}
	
	$$('#calendar li').removeEvents();
	$$('#calendar li.active').addEvent('click',function() {
			if ( ! this.hasClass('active') || this.hasClass('selected-light')) return;
			
			if ( ! $$('.gotd-block select')[0].getSelected()[0].get('value') ) {
				alert('Select region first');
				return;
			}
			
			if ( this.hasClass('selected') ) {
				this.removeClass('selected');
				$($$('.gotd-block select')[0].getSelected()[0].get('value') + ':' + this.get('id')).destroy();
			} else {
				this.addClass('selected');
				
				
				var li = new Element('li', {
					id: $$('.gotd-block select')[0].getSelected()[0].get('value') + ':' + this.get('id')
				});
				new Element('div', {
					'class': 'remove-btn'
				}).addEvent('click', function() {
					this.getParent('li').destroy();
					Cubix.OnlineBillingV3.Calendar.setSelectedDays();
					Cubix.OnlineBillingV3.calcOptProductsPrice();
				}).inject(li);

				var disp_d_ar = this.get('id').split('-');
				disp_d_ar[1] = parseInt(disp_d_ar[1]) + 1;
				var disp_d = disp_d_ar.join('-');

				new Element('span', {
					html: disp_d
				}).inject(li);
				new Element('br', {}).inject(li);
				new Element('span', {
					html: $$('.gotd-block select')[0].getSelected()[0].get('html')
				}).inject(li);
				
				new Element('input', {
					type: 'hidden',
					value: $$('.gotd-block select')[0].getSelected()[0].get('value') + ':' + this.get('id'),
					name: 'gotd_days[]'
				}).inject(li);
				
				li.inject($('selected-days'));
			}
			
			Cubix.OnlineBillingV3.calcOptProductsPrice();
		});
}


Cubix.OnlineBillingV3.Calendar.setBookedDays = function(year, month, day) {
	Cubix.OnlineBillingV3.Calendar.drawMonth(year, month);
	
	$$('#calendar li[id="' + year + '-' + month + '-' + day + '"]')
	.addClass('booked');
}

Cubix.OnlineBillingV3.Calendar.reset = function(classes) {
	classes.each(function(cl) {
		$$('#calendar li').removeClass(cl);
	});
}

Cubix.OnlineBillingV3.Calendar.setSelectedDays = function() {
	var regionId = $('gotd-region').get('value');
	Cubix.OnlineBillingV3.Calendar.reset(['selected', 'selected-light']);
	
	var selectedDays = $$('#selected-days input').get('value');
	selectedDays.each(function(id) {
		id = id.split(':');
		if ( id[0] == regionId ) {
			$$('#calendar li[id="' + id[1] + '"]').addClass('selected');
		} else {
			$$('#calendar li[id="' + id[1] + '"]').addClass('selected-light');
		}
	});
}

Cubix.OnlineBillingV3.Calendar.fillBookedDays = function(city) {
	var month, day, year;
	var citiesToCheck = [city];
	if ( Cubix.OnlineBillingV3.Calendar.regionRelations[city] ) {
		citiesToCheck = Cubix.OnlineBillingV3.Calendar.regionRelations[city];
	}

	citiesToCheck.each(function(ct) {
		if ( Cubix.OnlineBillingV3.Calendar.bookedDays[ct] ) {
			Cubix.OnlineBillingV3.Calendar.bookedDays[ct].each(function(it) {
				date = it.date.split('-');

				month = date[1];
				day = date[2];
				year = date[0];
				var d = Cubix.OnlineBillingV3.Calendar.dateBringToJsFormat(year, month, day);

				Cubix.OnlineBillingV3.Calendar.setBookedDays(d.getFullYear(), d.getMonth(), d.getDate());
			});
		}
	});
}

Cubix.OnlineBillingV3.Calendar.dateBringToJsFormat = function(year, month, day)
{
	//Removing leading 0 if necessary
	if ( day.charAt(0) == "0" ) {
		day = day.substr(1, day.length - 1);
	}
	if ( month.charAt(0) == "0" ) {
		month = month.substr(1, month.length - 1);
	}
	
	//month logic from php to js
	month -= 1;
	
	return new Date(year, month, day);
}

Cubix.OnlineBillingV3.calcOptProductsPrice = function() 
{
	var additionalAreaPrice = $('add-area-price').get('value');
	var gotdPrice = $('gotd-price').get('value');
	
	var price = 0;
	if ( $('package-price') && $('package-price').get('value') ) {
		price += $('package-price').get('value').toInt();
	}
	
	price += $$('.additional-area-block li.checked').length * additionalAreaPrice.toInt();
	price += $$('input[name="gotd_days[]"]').length * gotdPrice.toInt();
	
	//When package selected
	$$('td.total-price').set('html', price.toFixed(2) + ' CHF');
	
	
	//Just buying gotd without package
	$$('.optional-products-total-amount span').set('html', price.toFixed(2) + ' CHF');
	
}