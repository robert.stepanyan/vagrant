var PulseFade = new Class({

    //implements
    Implements: [Options,Events],

    //options
    options: {
        min: 0,
        max: 1,
        duration: 200,
        times: 5
    },

    //initialization
    initialize: function(el,options) {
        //set options
        this.setOptions(options);
        this.element = $(el);
        this.times = 0;
    },

    //starts the pulse fade
    start: function(times) {
        if(!times) times = this.options.times * 2;
        this.running = 1;
        this.fireEvent('start').run(times -1);
    },

    //stops the pulse fade
    stop: function() {
        this.running = 0;
        this.fireEvent('stop');
    },

    //runs the shizzle
    run: function(times) {
        //make it happen
        var self = this;
        var to = self.element.get('opacity') == self.options.min ? self.options.max : self.options.min;
        self.fx = new Fx.Tween(self.element,{
            duration: self.options.duration / 2,
            onComplete: function() {
                self.fireEvent('tick');
                if(self.running && times)
                {
                    self.run(times-1);
                }
                else
                {
                    self.fireEvent('complete');
                }
            }
        }).start('opacity',to);
    }
});