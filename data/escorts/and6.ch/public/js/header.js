window.addEvent('domready', function(){
	
	var mobile_popup = $$('.mobile_popup')[0];
	
	$$('.mobile_banner').addEvent('click', function(e) {
		e.stop();
		
		if ( mobile_popup.hasClass('none') ) {
			mobile_popup.removeClass('none');
		}
		else {
			mobile_popup.addClass('none')
		}
	});

	$$('.mobile_popup_x_btn').addEvent('click', function(e) {
		e.stop();
		mobile_popup.addClass('none')
	});
	
	if ( $defined($$('.login-btn')[0]) ) {
		$$('.login-btn')[0].addEvent('click', function(e){
			e.stop();

			this.getParent('form').submit();
		});
	}
	
	var username = $$('#login-box input[name=username]')[0];
	var password = $$('#login-box input[name=password]')[0];

	if ( username && password ) {

		username.addEvent('focus', function(){
			if ( this.get('value') == headerVars.tUsername ) {
				this.set('value', '');
			}			
		});
		username.addEvent('blur', function(){
			if ( ! this.get('value').length ) {
				this.set('value', headerVars.tUsername);
			}
		});		

		var initPassword = function(pass) {
			pass.addEvent('focus', function() {
				this.destroy();
				injectNewPassword();
			});
		};

		var injectNewPassword = function() {
			var pass = new Element('input', {
				name: 'password',
				type: 'password',
				'class': 'txt fleft',
				value: ''
			}).inject(username, 'after');

			pass.addEvent('blur', function() {
				if ( ! this.get('value').length ) {
					this.destroy();
					var pass = new Element('input', {
						name: 'password',
						type: 'text',
						'class': 'txt fleft',
						value: headerVars.tPassword
					});
					pass.inject(username, 'after');
					initPassword(pass);
				}
			});

			pass.focus();
			pass.focus();
			pass.focus();
		};

		initPassword(password);
	}

	
	if ( ! headerVars.currentUser ) {
		/*$('fchat').addEvent('click', function(e){
			e.stop();
			Cubix.Popup.Show('489', '652');
		});*/
	} else {
		if ( headerVars.currentUser.user_type == 'escort' && ! headerVars.packageQueue ) {
			/*$('fchat').addEvent('click', function(e){
				e.stop();
				Cubix.ChatPopup.Show('100', '100', this, 110, 130);
			});*/
		} else {
			/*$('fchat').addEvent('click', function(e){
				e.stop();			
				window.open('http://chat.and6.com/chat7/chat.php?hash=' + headerVars.sessionId, 'and6', 'height=620, width=820');
			});*/
		}
	}
});