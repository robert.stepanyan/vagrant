Cubix.Cam = {};
Cubix.Cam.path = 'and6-cams';
Cubix.Cam.iframeDomain = 'https://and6.thecamapi.com';

Cubix.Cam.InitListeners = function(){
	window.addEventListener("message", function(message){
		if (event.origin !== Cubix.Cam.iframeDomain)
		return;
		
		let {action, data}  = message.data; 

		if(action == 'height_change'){
			if(data.height == 'full_page'){
				$('iframe-wrapper').setStyle('height', 'calc( 100vh - 61px )');
			}else{
				let headerHeight = $('header').getSize().y;
				let footerHeight = $('footer').getSize().y;
				let contentHeight = document.getSize().y - headerHeight - footerHeight;
				data.height = contentHeight > data.height ? contentHeight : data.height;
				$('iframe-wrapper').setStyle('height', data.height);
			}
		}
		else if(action == 'url_change'){
			let reg = new RegExp(Cubix.Cam.path + '(\/)?', "g");
			let urlPath = window.location.pathname.replace(reg, "");
			window.history.pushState(data.url, "", '/' + Cubix.Cam.path + data.url);
			
		}
		else if(action == 'open_reg' || action == 'open_login'){
			Cubix.Cam.Popup.url = '/and6-cams/login/';
			Cubix.Cam.Popup.Show(400,375);
		}
		else if(action == 'open_purchase'){
			Cubix.Cam.Popup.url = '/and6-cams/buy-tokens/';
			Cubix.Cam.Popup.Show(513,375);
		}else if(action == 'balance'){ console.log('balance----' + data.balance);
			if($('balance')){
				$('balance').set('html',Math.round(data.balance));
			}
		}
		else{
			console.log(message.data);
		}
	});
};

Cubix.Cam.SendLoginDetails = function(){
	const iframe = $('iframe');
	if(iframe){
		iframe.addEvent('load', function(){
			if($('cam-token')){
				let token = $('cam-token').get('value');
				this.contentWindow.postMessage({"action": "login", "data": {"token": token}}, "*");
			}
		});
	}
};

Cubix.Cam.initHeaderEvents = function(){
	
	Cubix.Cam.Overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000' });
	$('menu').addEvent('click',function(e){
		e.preventDefault();
		$(this).toggleClass('open');
		$('navigation').toggleClass('show');
	});

	$('nav').addEvent('click',function(e){
		e.preventDefault();
		$('navigation').toggleClass('show');
	});
	
	$$('.buy-tokens').addEvent('click', function(e){
		e.stop();
		if(Cubix.Cam.isLoggedIn && Cubix.Cam.isMember){
			Cubix.Cam.Popup.url = '/and6-cams/buy-tokens/';
			Cubix.Cam.Popup.Show(513,375);
		}
		else{
			Cubix.Cam.Popup.url = '/and6-cams/login/';
			Cubix.Cam.Popup.Show(400,375);
		}
	});

	$$('.not-authorized').addEvent('click', function(e){
		e.stop();
		Cubix.Cam.Popup.url = '/and6-cams/login/';
		Cubix.Cam.Popup.Show(400,375);
	});
};

Cubix.Cam.Popup = {};
Cubix.Cam.Popup.inProcess = false;
Cubix.Cam.Popup.Show = function (box_height, box_width) {
	if ( Cubix.Cam.Popup.inProcess ) return false;

	Cubix.Cam.Overlay.disable();

	var container = new Element('div', { 'id': 'cam-popup'}).setStyles({
		left: window.getWidth() / 2 - box_width / 2,
		top:  window.getHeight() / 2 - box_height / 2,
		opacity: 0,
        position: 'fixed',
        'z-index': 101,
        background: '#fff'
	}).inject(document.body);
		
	Cubix.Cam.Popup.inProcess = true;
		
	new Request({
		url: Cubix.Cam.Popup.url,
		method: 'get',
		onSuccess: function (resp) {
			
			Cubix.Cam.Popup.inProcess = false;
			container.set('html', resp);
			var close_btn = new Element('span', {class: 'close-icon'}).inject(container);
					
			close_btn.addEvent('click', Cubix.Cam.Popup.Close);
			
			if($('go-signup-button')){
				$('go-signup-button').addEvent('click', function(){
					Cubix.Cam.Popup.Close();
					Cubix.Cam.Popup.url = '/and6-cams/signup/';
					Cubix.Cam.Popup.Show(475,375);
				});
			}
			
			var forms = container.getElements('form');
			forms.addEvent('submit', Cubix.Cam.Popup.Submit);
			
			container.tween('opacity', '1');
		}
	}).send();
		
	
	return false;
};

Cubix.Cam.Popup.Submit = function (e) {
	e.stop();
	   
	var popupOverlay = new Cubix.Overlay($('popup-container'), { color: '#fdfdfd', opacity: .6, loader: '/img/photo-video/ajax_test.gif' });
	popupOverlay.disable();
	this.set('send', {
		
		onSuccess: function (resp) {
			resp = JSON.decode(resp);
			
			if(resp.status == 'error'){
				popupOverlay.enable();
				$$('#cam-popup .error-notes').destroy();
				$$('#cam-popup .req').removeClass('req');
				
				function getErrorElement(el) {
					var target = el;
					if ( el.get('name') == 'terms' || el.get('name') == 'privacy' ) {
						target = el.getNext('label');
					}
					return new Element('div', { 'class': 'error-notes' }).inject(target, 'after');
				}

				for ( var field in resp.msgs ) {
					var input = this.getElement('*[name=' + field + ']');
					input.addClass('req');
					getErrorElement(input).set('html', resp.msgs[field]);
				}
			}
			else if(resp.status == 'payment_success'){
				window.location.href = resp.url;
				return;
			}
			else if(resp.status == 'payment_failure'){
				alert('Unexpected error please try later');
			}
			else if(resp.signup == true){
				window.location.href = '/private/signup-success';
				return;
			}
			else if(resp.signin == true && resp.user_type == 'escort'){
				window.location.href = '/and6-cams/broadcaster/dashboard';
				return;
			}
			else{
				window.location.reload();
				return;
			}
		}.bind(this)
	});

	this.send();
};

Cubix.Cam.Popup.Close = function () {
	$('cam-popup').destroy();
	Cubix.Cam.Overlay.enable();
};


window.addEvent('domready', function(){
	Cubix.Cam.InitListeners();
	//Cubix.Cam.SendLoginDetails();
	Cubix.Cam.initHeaderEvents();
	
	window.addEventListener('popstate', function(event){
		let iframe = $('iframe');
		if(iframe){
			iframe.contentWindow.postMessage({"action": "navigate", "data": {"url": event.state}}, "*");
		}
	});
});

