window.addEvent('domready', function () {
	var panel = new Element('div', {
		styles: {
			position: 'fixed',
			left: 0,
			bottom: 0,
			width: document.body.getWidth(),
			height: '30px',
			background: '#555',
			border: '1px solid #333',
			opacity: .3
		}
	}).inject(document.body).addEvent('mouseenter', function () {
		this.tween('opacity', 1);
	}).addEvent('mouseleave', function () {
		this.tween('opacity', .3);
	});
	
	panel.set('tween', {
		duration: 'short'
	});
	
	var btnSave = new Element('input', {
		type: 'submit',
		name: 'save',
		value: 'Save',
		styles: {
			float: 'right',
			margin: '5px 10px 0 0'
		}
	}).inject(panel);
	
	var editables = document.getElements('.cubix-editable');
	
	editables.each(function (el) {
		var editor = new Element('textarea', {
			html: el.get('html'),
			style: el.get('style'),
			name: 'tpl_' + el.get('cubix:name')
		});
		
		editor.setStyles({
			fontSize: el.getStyle('font-size'),
			fontFamily: el.getStyle('font-family'),
			width: el.getWidth() - el.getStyle('padding-left').toInt() - el.getStyle('padding-right').toInt(),
			height: el.getHeight() - el.getStyle('padding-top').toInt() - el.getStyle('padding-bottom').toInt(),
			/*marginTop: el.getStyle('margin-top').toInt() + el.getStyle('padding-top').toInt(),
			marginRight: el.getStyle('margin-right').toInt() + el.getStyle('padding-right').toInt(),
			marginBottom: el.getStyle('margin-bottom').toInt() + el.getStyle('padding-bottom').toInt(),
			marginLeft: el.getStyle('margin-left').toInt() + el.getStyle('padding-left').toInt(),
			padding: 0,*/
			border: 'none'
		});
		
		console.log ( editor.getStyle('background') );
		
		editor.setStyle('border', '1px dashed #0f0');
		
		editor.replaces(el);
	});
	
	var els = document.body.getChildren();
	
	var form = new Element('form', {
		action: '',
		method: 'post'
	}).inject(document.body);
	
	els.inject(form);
});
