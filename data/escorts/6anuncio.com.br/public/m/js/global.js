/***************** Karo Start *****************/

var Private = {};

Private.Message = {
    init: function( showname, escort_id ){
        var self = this;
        $$('.private-messanger').addEvent('click', function(){
            var data = { "participant_id" : $(this).getProperty('data-id'), "participant_type" : 'escort'/*$(this).getProperty('data-type')*/ }
            //console.log( data );
            self.sendRequest( data, showname, escort_id );
        })
    },

    sendRequest: function( data, showname, escort_id ){
        var self = this;
        var block = $('private-message-block');
        var wrapper = $('private-message-wrapper');
        var scoller = new Fx.Scroll(window);

        new Request({
            url: '/private-messaging/send-message-ajax',
            data: data,
            method: 'get',
            onProgress: function(){
                wrapper.setStyles({ 'display' : 'block', 'opacity' : 1, 'visibility' : 'visible' });
            },
            onSuccess: function (resp) {
                //resp = JSON.decode(resp, true);
                if( resp == 'signin' ){
                    window.location.href = "/private/signin";
                }
                if ( ! resp ) return;
                //wrapper.empty();
                block.set('html', resp);
                scoller.toElement(block);
                self.sendMessage( escort_id );
            },
            onComplete: function(){

            }
        }).send();
    },

    sendMessage: function(escort_id){
        var sendButton = $$('.red-btn');

        sendButton.addEvent('click', function(e){
            e.preventDefault();

            var data = {
                'participant'	: $('participant').get("value"),
                'captcha' : $('f-captcha').get('value'),
                'message' : $('privateMessage').get('value'),
                'escort_id' : escort_id
            };

            new Request({
                url: '/private-messaging/send-message-ajax',
                data: data,
                method: 'post',
                onSuccess: function (resp) {
                    //console.log( resp );
                    resp = JSON.decode(resp, true);
                    if( resp.status == 'error' ){
                        if( typeof resp.msgs.captcha != 'undefined' ){
                            if( resp.msgs.captcha == '' ){
                                $('f-captcha').addClass('field-error');
                            } else {
                                $('f-captcha').addClass('field-error').set('value', 'incorect value');
                            }
                        } else {
                            $('f-captcha').removeClass('field-error');
                        }

                        if( resp.msgs.fmessage == '' ){
                            $('privateMessage').addClass('field-error');
                        } else {
                            $('privateMessage').removeClass('field-error');
                        }
                    } else {
                        $('privateMessage').set('value', '').removeClass('field-error');
                        $('f-captcha').set('value', '').removeClass('field-error');
                        $$('.send-success').set('html', resp.msg);
                    }
                }
            }).send();
        })
    }
};



/***************** Karo End *****************/

MCubix = {};

MCubix.ErrorsHandler = {
  elm: null,

    messages: {
        'have_errors': 'Exists error !',
        'city_count': 'City count',
        'city_count_test': 'City count',
        'year': 'Year !',
        'month': 'Month must be between 1 and 12',
        'day': 'Day must be between 1 and 31',
        'day_31': 'Self month doesn\'t have 31 days!',
        'day_feb': 'February doesn\'t have that many days!',
        'date_out': 'Please select currect date'
    },

    doShow: function( elm, key, dict ){
        var self = this;

        if( !dict ) {
            elm.set('html', self.messages[key]).show();
        } else {            
            elm.set('html', key).show();
        }
        setTimeout(function() {
            elm.set('html', '').hide();
        }, 3000 );
    },

    doHide: function(elm){
        elm.set('html', '').hide();
    }
}