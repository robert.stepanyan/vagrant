var CityPicker = new Class({
	Implements: [Options, Events],
	
	el: null,
	els: {},
	data: {},
	regions: [],
	
	initialize: function (el, opts) {
		this.el = $(el);
		this.setOptions(opts || {});
		
		this.load();
		this.render();
	},
	
	load: function () {
		new Request({
			url: this.options.data_url,
			method: 'get',
			onSuccess: function (resp) {
				resp = JSON.decode(resp);
				
				this.data = resp;
				this.renderData();
				this.fireEvent('load');
			}.bind(this)
		}).send();
	},
	
	render: function () {
		this.els.list = new Element('div', {
			'class': 'cp-list-i'
		}).inject(
			new Element('div', {
				'class': 'cp-list'
			}).inject(this.el)
		);
	},
	
	getCity: function (id) {
		for ( var i = 0; i < this.regions.length; i++ ) {
			
			var city = this.regions[i].get(id);
			
			if ( city ) return city;
		}
		
		return false;
	},
	
	renderData: function () {
		this.els.list.set('html', '');
		
		var c = 0;
		
		var regions = [];
		
		for ( var region_title in this.data ) {
			var cities = this.data[region_title];
			
			var region = new CityPicker.RegionItem({ title: region_title, count: cities.length, picker: this });
			region.getEl().inject(this.els.list);
			
			for ( var i = 0; i < cities.length; i++ ) {
				var c = cities[i];
				
				var city = new CityPicker.CityItem({ id: c.id, title: c.title, region: region, picker: this });
				
				region.add(city);
			}
			
			regions.include(region);
		}
		
		this.regions = regions;
	}
});

CityPicker.CityItem = new Class({
	Implements: [Options],
	
	options: {},
	
	initialize: function (opts) {
		this.setOptions(opts || {});
		
		this.render();
		this.init();
	},
	
	get: function (attrib) {
		return this.options[attrib];
	},
	
	el: null,
	els: {},
	
	render: function () {
		var el;
		var title = new Element('span', {
			'class': 'cp-city-title'
		}).inject(
			el = new Element('div', {
				'class': 'cp-city-item'
			})
		);
		
		this.el = el;
		
		this.els.a = new Element('a', {
			href: '#',
			onclick: 'return false',
			html: this.get('title')
		}).inject(title);
	},
	
	getEl: function () {
		return this.el;
	},
	
	init: function () {
		this.el.addEvent('click', function (e) {
			e.stop();
			if ( this.isDisabled() ) return;
			this.options.picker.fireEvent('city-select', [this]);
		}.bind(this));
	},
	
	disable: function () {
		this.els.a.addClass('cp-disabled');
	},
	
	enable: function () {
		this.els.a.removeClass('cp-disabled');
	},
	
	isDisabled: function () {
		return this.els.a.hasClass('cp-disabled');
	}
});

CityPicker.RegionItem = new Class({
	Implements: [Options],
	
	options: {},
	
	cities: null,
	
	initialize: function (opts) {
		this.setOptions(opts || {});
		
		this.render();
		this.init();
		
		this.cities = new Hash();
	},
	
	add: function (city) {
		this.cities.set(city.get('id'), city);
		city.getEl().inject(this.els.list)
	},
	
	get: function (id) {
		return this.cities.get(id);
	},
	
	remove: function (id) {
		return this.cities.erase(id);
	},
	
	el: null,
	
	els: {},
	
	render: function () {
		var el = new Element('div', {
			'class': 'cp-region-item cp-region-item-closed'
		});
		
		this.el = el;
		
		new Element('span').inject(
			new Element('a', {
				'class': 'cp-icon-plus',
				href: '#',
				onclick: 'return false',
				html: this.options.title + ' (' + this.options.count + ')'
			}).inject(
				this.els.title = new Element('span', {
					'class': 'cp-region-title'
				})
			), 'top'
		);
		
		this.els.title.inject(this.el);
		
		this.els.list = new Element('div', {
			'class': 'cp-city-list',
		}).inject(this.el);
	},
	
	getEl: function () {
		return this.el;
	},
	
	init: function () {
		this.els.title.addEvent('click', function () {
			var a = this.els.title.getFirst('a');
			if ( a.hasClass('cp-icon-plus') ) {
				a.removeClass('cp-icon-plus');
				a.addClass('cp-icon-minus');
			}
			else {
				a.removeClass('cp-icon-minus');
				a.addClass('cp-icon-plus');
			}
			
			this.el.toggleClass('cp-region-item-closed');
		}.bind(this));
	}
});
