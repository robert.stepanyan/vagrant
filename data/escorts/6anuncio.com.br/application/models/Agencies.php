<?php

class Model_Agencies extends Cubix_Model
{
	protected $_table = 'agencies';
	protected $_itemClass = 'Model_AgencyItem';
	
	public static function hit($agency_id)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		
		$client->call('Agencies.updateHitsCount', array($agency_id));
		
		// for front DB
		self::getAdapter()->query('UPDATE club_directory SET hit_count = hit_count + 1 WHERE agency_id = ?', $agency_id);
	}
	
	/*public function getIdByName($name)
	{
		$sql = "
			SELECT id
			FROM agencies a
			WHERE name = ?
		";
		
		$agency = parent::_fetchRow($sql, $name);
		
		if ( $agency )
			return $agency->id;
		
		return null;
	}*/
	
	public function getIdByName($name)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		
		$result = $client->call('Agencies.getByName', array($name));
		
		return $result;
	}
	
	public function get($agency_id)
	{
		$sql = "
			SELECT a.id, a.user_id, a.name, a.phone, a.phone_instructions, a.email, a.web, a.last_modified, a.logo_hash, a.logo_ext, u.application_id, u.date_registered AS creation_date, a.hit_count
			FROM agencies a
			LEFT JOIN users u ON u.id = a.user_id
			WHERE a.id = ?
		";
		
		return parent::_fetchRow($sql, $agency_id);
	}
	
	public function getByUserId($user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$agency = $client->call('Agencies.getByUserId', array($user_id));
		
		$agency = new Model_AgencyItem($agency);
		
		return $agency;
	}
	
	public function getAll()
	{
		
	}

	public function save($agency)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$agency_id = $client->call('Agencies.save', array( (array) $agency));
		$agency->setId($agency_id);
		
		return $agency;
	}

	public function getBySlugId($slug, $id)
	{
		$sql = '
			SELECT
				cd.agency_id AS id,
				cd.club_name AS name,
				cd.phone,
				cd.phone_country_id,
				cd.phone_instructions,
				cd.phone_1,
				cd.phone_country_id_1,
				cd.phone_instructions_1,
				cd.phone_2,
				cd.phone_country_id_2,
				cd.phone_instructions_2,
				cd.email,
				cd.web,
				cd.check_website,
				cd.block_website,
				cd.last_modified,
				cd.photo_hash AS logo_hash,
				cd.photo_ext AS logo_ext,
				cd.date_registered AS creation_date,
				cd.hit_count,
				cd.available_24_7,
				cd.is_open,
				cd.working_times,
				cd.phone_instr,
				cd.phone_instr_1,
				cd.phone_instr_2,
				' . Cubix_I18n::getTblFields('cd.about') . '
			FROM club_directory cd
			WHERE cd.club_slug = ? AND cd.agency_id = ?
		';

		return $this->db()->query($sql, array($slug, $id))->fetch();
	}
}
