<?php

class Zend_View_Helper_ListSetcard extends Zend_View_Helper_Abstract
{
	public function listSetcard($escort, $in_premium = false,$third = true,$c = 1, $thumb_size = 'thumb_v2')
	{
		$this->view->escort = $escort;
		$this->view->in_premium = $in_premium;
        $this->view->config = Zend_Registry::get('escorts_config');
        $this->view->c = $c;
		$this->view->thumb_size = $thumb_size;
		return $this->view->render('escorts/list-setcard.phtml');
	}
}
