/**
 * Created by SceonDev on 22.06.2017.
 */

Banner = {};


Banner.initSearchCity = function () {
    var search_box_wrapper = $('search-wrapper');

    var auto_city = new Autocompleter.Request.JSON('search-city', '/geography/ajax-cities-search', {
        'minLength': 1,
        postVar: 'city_search_name',
        indicatorClass: 'autocompleter-loading',
        className: 'autocompleter-choices search-city'
    }).addEvent('onSelection', function (element, selected, value, input) {

        search_box_wrapper.getElement('#ts_slug').set('value', selected.inputValue.slug);
        search_box_wrapper.getElement('#city_id').set('value', selected.inputValue.city_id);
        search_box_wrapper.getElement('#city_id').set('value', 'city_gb_'+selected.inputValue.slug);

    });

    var search_btn = $('search-city-button');
    search_btn.addEvent('click',function () {
        var city_slug =  $('city_id').get('value');
        if(ts_slug){
            window.location.href='/escorts/'+city_slug;
        }else{
            window.location.href='/';
        }
    });
};



window.addEvent('domready', function() {
    Banner.initSearchCity();
});
