/* --> LocationHash */
Cubix.LocationHash = {};

Cubix.LocationHash.Set = function (hash) {
	if ( hash.length ) {
		document.location.hash = '#' + hash;
	}
	else {
		document.location.hash = '';
	}
	
	return false;
}

Cubix.LocationHash.Parse = function () {
	var hash = document.location.hash.substring(1);
	
	if ( ! hash.length ) {
		return {};
	}
	
	var params = hash.split(';');
	
	var filter = '';
	
	var not_array_params = ['page', 'name', 'text', 'agency_sort'];
	
	params.each(function (param) {
		var key_value = param.split('=');
		
		var key = key_value[0];
		var val = key_value[1];
		
		if ( val === undefined ) return;
		
		val = val.split(',');
				
		val.each(function(it) {
			filter += key + ( ( ! not_array_params.contains(key) ) ? '[]=' : '=' ) + it + '&';
		});
	});
	
	return filter;
}

Cubix.LocationHash.Make = function (params) {
	
	var hash = '';
	var sep = ';';
	var value_glue = ',';
	
	var places = $$('.agency-list-filter');
	
	var map = {};
	places.each(function(place) {
		var inputs = place.getElements('input[type=text]');
		inputs.append(place.getElements('input[type=hidden]'));
		//inputs.append(place.getElements('input[type=text]'));
		inputs.each(function(input) {
			if ( input.get('value').length ) {
				var index = input.get('name').replace('[]', '');
				if ( map[index] === undefined ) map[index] = new Array();
				map[index].append([input.get('value')]);
			}
		});
		
		var selects = place.getElements('select');
		selects.each(function(select) {
			
			if ( select.getSelected().get('value') != 0 ) {
				var index = select.get('name').replace('[]', '');
				if ( map[index] === undefined ) map[index] = new Array();
				map[index].append([select.getSelected().get('value')]);
			}
		});
	});
	
	map = $merge(map, params);
	
	for (var i in map) {
		hash += i + '=' + map[i].join(value_glue) + sep;
	}
	
	return hash;
}
/* <-- */

/* --> HashController */
Cubix.HashController = {
	_current: '',
	
	init: function () {
		setInterval('Cubix.HashController.check()', 100);
	},
	
	check: function () {		
		var hash = document.location.hash.substring(1);
		
		if (hash != this._current) {
			this._current = hash;
					
			var data = Cubix.LocationHash.Parse();
			
			Cubix.List.Load(data);
		}
	}
};
/* <-- */

Cubix.Filter = {};
Cubix.Filter.Set = function (filter) {
		
	Cubix.LocationHash.Set(Cubix.LocationHash.Make(filter));

	return false;
}

Cubix.List = {};

Cubix.List.container = 'agency-list';
Cubix.List.list_url = '';

Cubix.List.Load = function (data) {
	
	var url = Cubix.List.list_url;
	
	var overlay = new Cubix.Overlay($(Cubix.List.container), {
		loader: _st('loader-small.gif'),
		position: '50%'
	});
	
	overlay.disable();
	
	new Request({
		url: url + '?ajax=1',
		method: 'POST',
		data: data,
		onSuccess: function (resp) {
			
			$(Cubix.List.container).set('html', resp);

			Cubix.List.InitFilters();
			Cubix.initAgencyTips();
			
			overlay.enable();			
		}
	}).send();
	
	return false;
}

Cubix.initAgencyTips = function() {

	if( ! $$('.ag-list').length ) return;

	/*if ( $$('.tip-wrap').length ) {
		$$('.tip-wrap').destroy();
	}*/	

	var myTips = new Tips('.agency-tip', {
		onShow: function(tip, el) {			
			tip.setStyles({				
				display: 'block',
				border: 'none',
				width: 'auto',
				height: 'auto'
			});
			tip.getElements('.tip')[0].set('html', '');

			new Request({
				url: '/agencies/show-tool-tip?agency_id=' + el.get('agency_id') + '&type=' + el.get('typee'),
				method: 'get',
				onSuccess: function (resp) {
					tip.getElements('.tip')[0].set('html', resp);
				}
			}).send();

		}
	});
};

Cubix.List.InitFilters = function() {
	new Mooniform($$('.agency-list-filter select'));

	$$('.agency-list-filter select').addEvent('change', function(e){
		e.stop();
		Cubix.LocationHash.Set(Cubix.LocationHash.Make());
	});

	$$('.agency-list-filter a.btn-search').addEvent('click', function(e){
		e.stop();
		Cubix.LocationHash.Set(Cubix.LocationHash.Make());
	});
};

window.addEvent('domready', function () {
	Cubix.List.InitFilters();
	Cubix.initAgencyTips();

	Cubix.HashController.init();
});
