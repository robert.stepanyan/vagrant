window.addEvent('domready', function() {
	if ($('new-cities-show-more'))
		$('new-cities-show-more').addEvent('click', function(e) {
			$$('.cities-box-big').removeClass('none');
			$('new-cities-show-more').set('style', 'display: none;');
			$('new-cities-show-less').set('style', 'display: block;');

			return false;
		});
	
	if ($('new-cities-show-less'))
		$('new-cities-show-less').addEvent('click', function(e) {
			$$('.cities-box-big').addClass('none');
			$('new-cities-show-less').set('style', 'display: none;');
			$('new-cities-show-more').set('style', 'display: block;');

			return false;
		});
});