Cubix.Analytics ={
	data:new Array(),
	submit:false,
	filter:'',
	async:true,
	Checkbox:{
		from:'undefined',
		SendCheckbox:function()
		{
			var	filter = $$(this.from+' input[type="checkbox"]:checked');
			Cubix.Analytics.data = new Array();
			if(filter)
			{	
				filter.each(function(el,index){
					Cubix.Analytics.data[index] = {};
					Cubix.Analytics.data[index]['group'] = el.getParent('.filter,.filter-popup').getElement('span').get('html');
					Cubix.Analytics.data[index]['text_value'] =  el.getParent('div').getNext('label, span').get('html');
					Cubix.Analytics.data[index]['value'] = el.get('value');
					Cubix.Analytics.data[index]['name'] =  el.get('name');
					Cubix.Analytics.data[index]['type'] = 'checkbox';
				});
				Cubix.Analytics.Send();
			}
		}
	},
	SingleText:{
		id:'undefined',
		text_value:'auto_showname',
		group:'auto_showname',
		SendSingleText:function()
		{	
			Cubix.Analytics.data = new Array();
			if(this.id!='undefined' && $$(this.id)[0].get('value').trim()!='' && $$(this.id)[0].get('value').trim()!=Cubix.Filter.search_input_text)
			{	
				Cubix.Analytics.data[0] = {};
				Cubix.Analytics.data[0]['group'] =this.group;
				Cubix.Analytics.data[0]['text_value'] = this.text_value;
				Cubix.Analytics.data[0]['value'] = $$(this.id)[0].get('value');
				Cubix.Analytics.data[0]['name'] =  $$(this.id)[0].get('name');
				Cubix.Analytics.data[0]['type'] = 'text';
				Cubix.Analytics.Send();
			}
		}
	},
	Send:function()
	{ 
		if(this.filter!='' && this.data.length>0)
			new Request({
				url:'/analytics',
				method: 'POST',
				data: {'data':this.data,'filter':this.filter},
				async:this.async,
				onSuccess:function(resp)
				{	if(Cubix.Analytics.submit==true)
					{	 
						$$(Cubix.Analytics.SingleText.id)[0].getParent('form').submit();
					}
					
					return resp;
					
				}
			}).send();
	}
	
};
window.addEvent('domready', function(){
	
	$$('.search-btn>input').addEvent('click',function(){	
		var form = $$('.search-criteria>form')[0].toQueryString().split('&');
		var length = form.length;
		Cubix.Analytics.data = new Array();

		var j = 0;
		var group ='';
		var text_value = '';
	
		for(var i = 0;i<length;i++)
		{
			var item = form[i].split('=');
			if(item[1] && item[1].trim()!='')
			{	
				var name = item[0].replace('%5B','[').replace('%5D',']');
				var input = $$('[name="'+ name+'"]')[0];

				
				var type = input.get('type');
				var value = '';
				if(type!='hidden')
				{
					
					switch(type)
					{
						case 'text':
							if(input.getPrevious('label'))
							{
								text_value= group = input.getPrevious('label').get('html');
							}
							else
							{
								text_value = input.getPrevious('span').get('html');
								group = input.getParent('div').getPrevious('label').get('html');
							}
							value = input.get('value');
						break;
						case 'select-one':
							if(input.getPrevious('label'))
							{
								group = input.getPrevious('label').get('html');
							}
							else if(input.getParent('div').getPrevious('label'))
							{
								group = input.getParent('div').getPrevious('label').get('html');
							}
							else
							{
								group = input.getParent('tr>td:first-child').getNext('td>label').get('html');
							}
							text_value = input.getSelected()[0].get('html');
							value = input.getSelected()[0].get('value');
				
						break;
						case 'radio':
							group = $$('[name="'+name+'"]:checked')[0].getParent('div').getPrevious('label').get('html');
							text_value = $$('[name="'+name+'"]:checked')[0].getNext('span').get('html');
							value = $$('[name="'+name+'"]:checked')[0].get('value');
						break;
						case 'checkbox':
							if(input.getParent('div').getPrevious('label'))
							{	
								group = input.getParent('div').getPrevious('label').get('html');
								text_value = input.getNext('span').get('html');
								value = input.get('value');
							}
									
						break;
					}
					if(value)
					{	
						Cubix.Analytics.data[j] = {};
						Cubix.Analytics.data[j]['group'] =group.trim();
						Cubix.Analytics.data[j]['text_value'] =  text_value.trim();
						Cubix.Analytics.data[j]['value'] = value.trim();
						Cubix.Analytics.data[j]['name'] =  name.trim();
						Cubix.Analytics.data[j]['type'] =type.trim();
						j++;
					}
					
				}
			}
		}
		Cubix.Analytics.filter = 'Search';
		Cubix.Analytics.SingleText.id = '.search-btn>input';
		Cubix.Analytics.submit=true;
		Cubix.Analytics.Send();
		return false;
	});
});