
var Video = this.VideoSection = new Class({

	Implements: [Events, Options],

	options: {
		contentWrapper: ".video-box-wrapper"
	},

	form : null,
    videoOverlay : null,
		
	initialize: function(form, options) {
		this.setOptions(options);
		this.videoOverlay = new Cubix.Overlay(
			document.getElement(this.options.contentWrapper), {color: '#fdfdfd', opacity: .6, position: '55%', loader: '/img/photo-video/ajax_small.gif'});
		this.form = document.id(form);
		this.initUpload(this.form);
		
		if(!$$('.no-video')[0]){
			this.initRemoveVideo();
			this.clickVideo();
		}
	},
	
	initUpload: function(form){
		
		var self = this;
		
		var myUpload = new MooUpload('filecontrol', {
			action: form.get('action'),
			accept: '.flv,.wmv,.mp4,.mov',
			method: 'html5', // Automatic upload method (Choose the best)
			multiple: false,
			deleteBtn: true,
			onAddFiles: function(){
				var items = $$('#filecontrol_listView .item');
				if(items.length > 1){
					var close = items[0].getElement('.delete');
					close.fireEvent('click', {
						stop: function(){}
					});
				}
			},
			onBeforeUpload: function(){
				form.getElement('.progresscont').removeClass('none').fade('in');
			},
			onFileUpload: function(fileindex, response){ 
				
				if(response.error == 0 && response.wait_for_video){
					self.videoOverlay.disable();
					self.isVideoReady();				
				}
			},
			onFileUploadError: function(filenum, response){
				$$('.mooupload_error').appendText(response.error);
			},
			onFinishUpload: function() {
				setTimeout(function() {
					var fxProg = new Fx.Tween(form.getElement('.progresscont'),{duration: 2000});
					fxProg.addEvent('complete', function(){this.element.addClass('none'); });
					fxProg.start('opacity','100','0');
					//fxProg.removeEvents('complete');
				}, 2000);
			}
		});
	},

	isVideoReady : function(){
		var self = this;
		var readyUrl = $('is-video-ready-url').get('value');
		setTimeout(function() {
			new Request.JSON({
				url: readyUrl,
				method: 'get',
				onSuccess: function (resp) {
					if(resp.status == 0){
						self.isVideoReady();
					}
					else{
						self.initSuccess(resp);
					}
				}
			}).send();
		}, 2000);
	},
	
	initSuccess : function(response){
		var item = $$('#filecontrol_listView .mooupload_readonly')[0];
		item.destroy();

		if ( $$('.no-video').length ) {
			$$('.no-video').destroy();
		}
		$('filecontrol_btnDelete').removeClass('none');
		var container = $$('.video-box')[0];
		var div = new Element('div', {'id': 'video_' + response.video_id, 'class': 'wrapper'}).inject(container, 'top');
		var input_hidden = new Element('input', {'type':'hidden', 'id':'video-config', 'value': response.vod}).inject(div, 'top');

		/*div.setStyles({opacity: 0, display: 'block'});
		div.tween('opacity', 1);*/

		var strong = new Element('strong', {'html': 'Pending' }).inject(div, 'top');
		var img = new Element('img', {'src': response.photo_url, 'data-height' : response.video_height, 'data-width' : response.video_width, 'rel' : response.video }).inject(div, 'top');
		new Element('span', {'class': 'video_play_btn'}).inject(div, 'bottom');

		this.clickVideo();
		this.initRemoveVideo();
		this.videoOverlay.enable();
	},
	
	initRemoveVideo : function() {
		self = this;
		$$('#form-upload .delete').removeClass('none').addEvent('click', function(e){
			e.stop();
			self.videoOverlay.disable();
			var videoBox = $$('.video-box')[0];
			var videoWrap = videoBox.getElement('.wrapper');
			var url = $('video-remove-url').get('value');
			new Request({
				url: url,
				method: 'get',
				data: {video_id: videoWrap.get('id').replace('video_' , '')},
				onSuccess: function (resp) {
					videoWrap.destroy();
					new Element('div', {'class' : "no-video" }).inject(videoBox, 'top');
					this.addClass('none');
					this.removeEvents('click');
					self.videoOverlay.enable();
				}.bind(this)
			}).send();
		});
	},
	
	clickVideo: function()
	{
		self = this;
		$$('.video-box').addEvent('click',function(){
			var img = this.getElement('img');
			var width = img.get('data-width');
			var height = img.get('data-height');
			var video = img.get('alt');
			var image = img.get('src').replace('video_thumb_p','orig');
			video = $('video-config').get('value') + video + '_' + height + 'p.mp4';
			self.play(video,image,width,height,true);
		});
	},
	
	play: function(video,image,width,height,auto)
	{
		
		var vm = new VideoModal({	
			"hideFooter":true,
			'offsetTop':'20%',
			'width':width,
			'height':height,
			'onAppend':function()	
			{	
			 jwplayer('video-modal').setup({
				flashplayer: '/jwplayer.flash.swf',
				height:height,
				width:width,
				image:image,
				autostart: auto,
				skin: {
					name: "bekle"
				},
				logo: {
					file: "/img/photo-video/logo.png"
				},
				/*provider: 'rtmp',
				file:'rtmp://'+video,
				streamer:'rtmp://'+video*/
				file:video	
				});
			}
		});
		vm.addButton("Cancel", "btn");
		vm.show({ 
			"model":"modal", 
			"title":"",
			'contents':'<div id="video-modal"></div>'
			});
	}

});

