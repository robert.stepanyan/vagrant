/* --> Filter */
Cubix.Filter = {};
Cubix.Filter.url = '';
Cubix.Filter.params;
Cubix.Filter.filter;
Cubix.Filter.selected;
Cubix.Filter.btn_text;
Cubix.Filter.search_input_text = '';
Cubix.Filter.mooInst;

window.addEvent('domready', function(){
	Cubix.Filter.getForm();
	Cubix.Filter.InitSearchInput('search');
});

Cubix.Filter.getForm = function(data){
	
	new Request({
		url:	Cubix.Filter.url,
		method: 'GET',
		data: data,
		onSuccess: function(resp){
			
			var lastViewed = $('last_viewed');
			
			if ( ! lastViewed ) {
				lastViewed = $('late_night');
			}
			
			if (lastViewed){
				if ( ! $('filter-container') ) {
					var fContainer = new Element('div', {id: 'filter-container'});
				} else {
					var fContainer = $('filter-container');
				}
				
				fContainer.set('html', resp);
				
				var filterV2 = fContainer.getElement('#filter-v2');
				
				if ( Cookie.read('showFilter') ) {
				
					/*filterV2.setStyle('opacity', 0);

					var myFx = new Fx.Tween(filterV2, {
						duration: 400,
						onComplete: function() {
							filterV2.tween('opacity', 0, 1);

							Cubix.Filter.filter = fContainer.getElement('#filter-v2');
							Cubix.Filter.Init();
							Cubix.Filter.InitPopups();

							Cubix.HashController.init();
						}
					});

					myFx.start('height', '190');*/
				
					lastViewed.grab(fContainer, 'after');

					Cubix.Filter.filter = fContainer.getElement('#filter-v2');
					Cubix.Filter.Init();
					Cubix.Filter.InitPopups();

					Cubix.HashController.init();
				} else {
					
					lastViewed.grab(fContainer, 'after');
					
					filterV2.setStyle('height', 0);
					//filterV2.setStyle('display', '');

					Cubix.Filter.filter = fContainer.getElement('#filter-v2');
					
					Cubix.Filter.Init();
					Cubix.Filter.InitPopups();

					Cubix.HashController.init();
				}			
			}			
		}
	}).send();
};

Cubix.Filter.InitSearchInput = function (input) {
	Cubix.Filter.InitSearchInput.input = $(input);
	
	this.timer = null;
	
	
	Cubix.Filter.InitSearchInput.input.addEvent('focus', function(){
		if ( this.get('value') == Cubix.Filter.search_input_text ) {
			this.set('value', '');
			this.removeClass('def-text');
		}	
	});
	Cubix.Filter.InitSearchInput.input.addEvent('blur', function(){
		if ( ! this.get('value').length ) {
			this.set('value', Cubix.Filter.search_input_text);
			this.addClass('def-text');
		}
	});
	
	Cubix.Filter.InitSearchInput.input.addEvents({
		keyup: function () {
			$clear(this.timer);
			this.timer = setTimeout('Cubix.Filter.InitSearchInput.KeyUp(Cubix.Filter.InitSearchInput.input)', 500);
		}.bind(this)
	});
}

Cubix.Filter.InitSearchInput.KeyUp = function (input) {
	
	
	var value = ( input.get('value').length && input.get('value') != Cubix.Filter.search_input_text ) ? input.get('value') : null;
	
	//if ( value ) {
		$$('input[name=name]')[0].set('value', value);
		Cubix.LocationHash.Set(Cubix.LocationHash.Make());
	//}
}

Cubix.Filter.Init = function() {
	
	var add_filters = $$('.add-filters input[type=checkbox]');

	new Mooniform(add_filters);

	add_filters.addEvent('change', function(){
		Cubix.LocationHash.Set(Cubix.LocationHash.Make());
	});

	if ( $$('input[name=name]')[0].get('value').length ) {
		Cubix.Filter.InitSearchInput.input.set('value', $$('input[name=name]')[0].get('value'));
		if ( $$('input[name=name]')[0].get('value') != Cubix.Filter.search_input_text ) {
			Cubix.Filter.InitSearchInput.input.removeClass('def-text');
		}
	}

	var mooInst = new Mooniform(Cubix.Filter.filter.getElements('div.filter input[type=checkbox], div.filter-popup input[type=checkbox]'));
	Cubix.Filter.mooInst = mooInst;
	
	Cubix.Filter.filter.getElements('div.filter input').each(function(input) {
		
		if ( ! input.getParent('div.sorting-div') ) {
			input.addEvent('click', function() {
				
				if ( input.checked ) {
					//input.getNext('span').removeClass('grey');
					Cubix.Filter.filter.getElements('.' + input.get('class')).set('checked', 'checked');
				} else {
					//input.getNext('span').addClass('grey');
					Cubix.Filter.filter.getElements('.' + input.get('class')).set('checked', '');
				}

				//Cubix.Filter.ForceGray(input);
				//Cubix.LocationHash.Set(Cubix.LocationHash.Make());
			});
		}
	});

	Cubix.Filter.filter.getElements('.m-footer .btn_search').addEvent('click', function(){
		Cubix.LocationHash.Set(Cubix.LocationHash.Make());
	});
	
	Cubix.Filter.InitReset();
	
	Cubix.Filter.InitSingleCheckbox();
	Cubix.Filter.InitHideFilters();
	
	Cubix.Filter.InitSorting();

	Cubix.Filter.InitGroupContextMenu(mooInst);
	
	if ( ! Cookie.read('showFilter') ) {
		Cubix.Filter.HideFilters();
	}

	return mooInst;
};

Cubix.Filter.InitGroupContextMenu = function(mooInst) {
	var group_items = $$('ul.group_list span.group-item');

	group_items.each(function(el){
		el.getNext('div.group p.title').addEvent('click', function(e){
			e.stop();
			this.getParent('div.group').toggleClass('none');
		});
	});

	group_items.addEvent('click', function(e){
		e.stop();
		group_items.each(function(el){
			el.getNext('div.group').addClass('none');
		});

		this.getNext('div.group').toggleClass('none');		
	});

	$$('.refine').addEvent('click', function(e){
		e.stop();

		/*this.getParent('.content').getElements('input[type=checkbox]').each(function(el){
			el.set('checked', '');
			el.fireEvent('click');
		});

		mooInst.update(this.getParent('.content').getElements('input[type=checkbox]'));*/

		Cubix.LocationHash.Set(Cubix.LocationHash.Make());
	});
};

Cubix.Filter.InitSorting = function() {

	var sorting_select = $$('.sort-by-box select[name=sort]')[0];

	new Mooniform($$('.sort-by-box select[name=sort]'));	
	
	sorting_select.addEvent('change', function() {
		Cubix.LocationHash.Set(Cubix.LocationHash.Make());
	});
};

Cubix.Filter.InitSingleCheckbox = function() {
	Cubix.Filter.filter.getElements('input.single').addEvent('click', function() {
		
		var els = this.getParent('div.list').getElements('input:checked');
		var self = this;
		
		els.each(function(it) {
			if( it.get('class') != self.get('class') )
				it.set('checked', '');
		});
	});
};

Cubix.Filter.HideFilters = function() {
	var hide_btn = $('hide-filters');
	var filter = $('filter-v2');
	var top_btn_icon = $$('.top-btn-icon')[0];
	
	Cubix.Filter.btn_text = hide_btn.get('html');
	hide_btn.set('html', hide_btn.getNext('span').get('html'));
	filter.setStyles({
		'overflow' : 'hidden',
		'height' : '0'
	});
	hide_btn.removeClass('opened');
	hide_btn.addClass('closed');
	
	top_btn_icon.removeClass('opened');
	top_btn_icon.addClass('closed');

	filter.setStyles({
		'border' : '0'
	});
}

Cubix.Filter.InitHideFilters = function() {
	var hide_btn = $('hide-filters');
	var top_btn_wrapper = $$('.top-btn-wrapper')[0];
	var top_btn_icon = $$('.top-btn-icon')[0];
	var filter = $('filter-v2');
	//var reset_btn = $('fv2_reset');
	
	hide_btn.addEvent('click', function(){
		
		/*var closeFx = new Fx.Tween(filter, {
			duration: 'short',
			onStart: function() {
				Cubix.Filter.btn_text = hide_btn.get('html');
				hide_btn.set('html', hide_btn.getNext('span').get('html'));
				filter.setStyles({
					'overflow' : 'hidden'
				});
				hide_btn.removeClass('opened');
				hide_btn.addClass('closed');
			},
			onComplete: function() {
				filter.setStyles({
					'border' : '0'
				});
				
				Cookie.dispose('showFilter');
			}			
		});
		
		var openFx = new Fx.Tween(filter, {
			duration: 'short',
			onStart: function() {
				hide_btn.set('html', Cubix.Filter.btn_text);
				filter.setStyles({
					'border' : '1px solid #C9E7FF'
				});
				hide_btn.removeClass('closed');
				hide_btn.addClass('opened');
			},
			onComplete: function() {
				filter.setStyles({
					'overflow' : null,
					'border' : '1px solid #C9E7FF',
					'display' : 'block'
				});
				
				Cookie.write('showFilter', true, {duration: 365});
			}
		});
		
		var height = 190;
		
		if ( reset_btn.hasClass('none') ) {
			height = 190;
		}*/
		
		if ( this.hasClass('opened') ) {
			Cubix.Filter.btn_text = hide_btn.get('html');
			hide_btn.set('html', hide_btn.getNext('span').get('html'));
			filter.setStyles({
				'overflow' : 'hidden',
				'height' : '0px',
				'border' : '0'
			});
			hide_btn.removeClass('opened');
			hide_btn.addClass('closed');

			top_btn_wrapper.setStyles({
				'box-shadow' : '0px 0px 5px #ececec',
				'border-bottom': '1px solid #ececec'
			});

			top_btn_icon.removeClass('opened');
			top_btn_icon.addClass('closed');

			Cookie.dispose('showFilter');
		} else {
			hide_btn.set('html', Cubix.Filter.btn_text);
			filter.setStyles({
				'overflow' : null,
				'border' : '1px solid #ececec',
				'display' : 'block',
				'height' : 'auto'
			});
			hide_btn.removeClass('closed');
			hide_btn.addClass('opened');

			top_btn_icon.removeClass('closed');
			top_btn_icon.addClass('opened');

			top_btn_wrapper.setStyles({
				'box-shadow' : '0px -3px 3px #ececec',
				'border-bottom': 'none'
			});

			Cookie.write('showFilter', true, {duration: 365});
		}
	});

	
};

Cubix.Filter.InitReset = function() {
		
	/*if ( inputs.length ) {
		//$('fv2_reset').getElements('span')[0].removeClass('passive').addClass('active');
		//$('fv2_reset').removeClass('res_pas').addClass('res_act');
		$$('.m-footer .btn_reset_all')[0].removeClass('none');
	}*/
	
	$$('.m-footer .btn_reset_all').addEvent('click', function(e){
		e.stop();
		
		Cubix.LocationHash.Set('');
		
		var inputs = Cubix.Filter.filter.getElement('#filters_body').getElements('input:checked');
		if ( inputs.length ) {
			inputs.each(function(input){
				input.set('checked', '');
				//Cubix.Filter.ForceGray(input);
			});
		}
		//$$('.m-footer .btn_reset_all')[0].addClass('none');
	});
}

Cubix.Filter.InitPopups = function() {
	Cubix.Filter.filter.getElements('a.more').addEvent('click', function(e){
		e.stop();
		
		var popup_id = this.get('id');
		var popup = $$('.' + popup_id)[0];
		var offset = {x : 44, y: -18};
		Cubix.Filter.PopupToggle(popup, this, offset);
	});
	
	if ( Cubix.Filter.filter.getElement('#other_list') ) {
		Cubix.Filter.filter.getElement('#other_list').getElements('span').addEvent('click', function(e){
			e.stop();
			var offset = {x : 55, y: -18};
			Cubix.Filter.PopupToggle($$('.f_other')[0], this, offset);
		});
	}

	Cubix.Filter.filter.getElements('span.btn_reset').addEvent('click', function() {	
		this.getParent('div.filter-popup').getElements('input').each(function(it){			
			it.set('checked', '');			
		});

		Cubix.Filter.mooInst.update(this.getParent('div.filter-popup').getElements('input[type=checkbox]'));
	});
	
	Cubix.Filter.filter.getElements('span.close, span.btn_cancel').addEvent('click', function(){
		this.getParent('div.filter-popup').addClass('none');
		
		this.getParent('div.filter-popup').getElements('input').each(function(it){
			if ( ! Cubix.Filter.selected.contains(it.get('class')) ) {
				it.set('checked', '');
			} else {
				it.set('checked', 'checked');
			}
		});
	});
	
	Cubix.Filter.filter.getElements('span.btn_ok').addEvent('click', function(){
		Cubix.LocationHash.Set(Cubix.LocationHash.Make());
		this.getParent('div.filter-popup').addClass('none');
	});
	
	Cubix.Filter.filter.getElements('div.f_other p.title').addEvent('click', function() {
		var v_slide = new Fx.Slide($(this.getParent('div').getElements('div.list')[0]));
		v_slide.toggle();
	});
	
	Cubix.Filter.filter.getElements('div.f_other div.collapsed').each(function(it){
		var v_slide = new Fx.Slide($(it));
		v_slide.hide();
	});
}

Cubix.Filter.PopupToggle = function(popup, el, offset) {
	
	var position = el.getPosition('filter-v2');


	
	Cubix.Filter.filter.getElements('div.filter-popup').each(function(it) {
		it.addClass('none');
	});
	
	popup.removeClass('none');
	var left = position.x + offset.x;
	if ( popup.getDimensions().width > 1000 ) {
		left = 0;
	}
	popup.setStyles({top: position.y + offset.y - 200, left: left})
	
	if ( popup.hasClass('f_other') ) {
		popup.getElements('div.list').each(function(it){
			var inputs = it.getElements('input:checked');
			
			if ( ! inputs.length ) {			
				var v_slide = new Fx.Slide($(it));
				v_slide.hide();
			}
		});
	}
	
	var title = el.get('rel');	
	popup.getElements('div.list').each(function(it){
		if ( it.get('rel') == title ) {
			var v_slide = new Fx.Slide($(it));
			v_slide.show();
		}
	});
	
	
	Cubix.Filter.selected = [];
	popup.getElements('input:checked').each(function(it){
		Cubix.Filter.selected.append([it.get('class')]);
	})
};

Cubix.Filter.ForceGray = function(input) {
	var filter = input.getParent('div.filter');
	var is_gray = true;
	filter.getElements('input').each(function(it){
		if ( it.checked == true ) {
			is_gray = false;
		}
	});
	
	if ( is_gray ) {
		filter.getElement('span.title').addClass('grey');
		return;
	}
	
	filter.getElement('span.title').removeClass('grey');
};





Cubix.Filter.Set = function (filter) {
		
	Cubix.LocationHash.Set(Cubix.LocationHash.Make(filter));

	return false;
}

/* --> LocationHash */
Cubix.LocationHash = {};

Cubix.LocationHash.Set = function (hash) {
	if ( hash.length ) {
		document.location.hash = '#' + hash;
	}
	else {
		document.location.hash = '';
	}
	
	return false;
}

Cubix.LocationHash.Parse = function () {
	var hash = document.location.hash.substring(1);
	
	if ( ! hash.length ) {
		return {};
	}
	
	var params = hash.split(';');
	
	var filter = '';
	
	var not_array_params = [
		'page', 'sort', 'reg', 'name', 'price_from', 'price_to',
		'age_from', 'age_to', 'f_incall', 'f_outcall',
		'video',  'review', 'is_online_now', 'verified', 'verified_contact'
	];
	
	params.each(function (param) {
		var key_value = param.split('=');
		
		var key = key_value[0];
		var val = key_value[1];
		
		if ( val === undefined ) return;
		
		val = val.split(',');
				
		val.each(function(it) {
			filter += key + ( ( ! not_array_params.contains(key) ) ? '[]=' : '=' ) + it + '&';
		});
	});
	
	return filter;
}

Cubix.LocationHash.Make = function (params) {
	
	var hash = '';
	var sep = ';';
	var value_glue = ',';
	
	var popups = Cubix.Filter.filter.getElements('div.filter-popup');
	var input_texts = Cubix.Filter.filter.getElements('input[type=text]');
	var common_inputs = Cubix.Filter.filter.getElements('.common-item input[type=checkbox]:checked');
	var add_filters = $$('.add-filters input[type=checkbox]:checked');

	var input_showname_search = $$('.showname-search input[name=name]')[0];

	var sorting_select = $$('.sort-by-box');	
	
	var map = {};
	popups.each(function(popup) {
		var inputs = popup.getElements('input:checked');
		inputs.append(popup.getElements('input[type=hidden]'));
		inputs.each(function(input) {
			var index = input.get('name').replace('[]', '');
			if ( map[index] === undefined ) map[index] = new Array();
			map[index].append([input.get('value')]);
		});
	});


	input_texts.each(function(input) {
		if ( input.get('value').length > 0 ) {
			var index = input.get('name').replace('[]', '');
			if ( map[index] === undefined ) map[index] = new Array();
			map[index].append([input.get('value')]);
		}
	});

	
	if ( input_showname_search.get('value').length > 0 && input_showname_search.get('value') != Cubix.Filter.search_input_text ) {
		var index = input_showname_search.get('name').replace('[]', '');
		if ( map[index] === undefined ) map[index] = new Array();
		map[index].append([input_showname_search.get('value')]);
	}
	

	common_inputs.each(function(input) {		
		var index = input.get('name').replace('[]', '');
		if ( map[index] === undefined ) map[index] = new Array();
		map[index].append([input.get('value')]);		
	});

	add_filters.each(function(input) {		
		var index = input.get('name').replace('[]', '');
		if ( map[index] === undefined ) map[index] = new Array();
		map[index].append([input.get('value')]);		
	});

	if ( sorting_select ) {
		map = Object.merge(map, {sort: [sorting_select.getElements('select[name=sort]')[0].getSelected()[0].get('value')]});
	}
	
	map = $merge(map, params);

	
	
	for (var i in map) {
		hash += i + '=' + map[i].join(value_glue) + sep;
	}
	
	return hash;
}
/* <-- */

/* --> HashController */
Cubix.HashController = {
	_current: '',
	
	init: function () {
		setInterval('Cubix.HashController.check()', 100);
	},
	
	check: function () {
		var hash = document.location.hash.substring(1);
		
		if (hash != this._current) {
			this._current = hash;
			
					
			var data = Cubix.LocationHash.Parse();
			//Cubix.Filter.getForm(data);
			Cubix.Escorts.Load(hash, data/*, Cubix.HashController.Callback*/);
		}
	}
};

Cubix.HashController.Callback = function () {
	Cubix.Filter.Change(Cubix.LocationHash.Parse());
	Cubix.Filter.Set(Cubix.LocationHash.Parse(), true);
}
/* <-- */