/**
 * @author: ptcong90@gmail.com
 * @update: Jul 30, 2015
 */
 
 
(function(window) {
    'use strict';

    var
    debug = false,
    demo = false,
    counter = 0,
    lastPopTime = 0,
    baseName = 'SmartPopunder_aff',
		cookieName = 'pundercamplace',
    fireMethod = 'fire', // for minimal
    parent = top != self ? top : self,
    userAgent = navigator.userAgent.toLowerCase(),
    browser = {
        win: /windows/.test(userAgent),
        mac: /macintosh/.test(userAgent),
        mobile: /iphone|ipad|android|ucbrowser/.test(userAgent),
        webkit: /webkit/.test(userAgent),
        mozilla: /mozilla/.test(userAgent) && !/(compatible|webkit)/.test(userAgent),
        chrome: /chrome/.test(userAgent),
        msie: /msie|trident\//.test(userAgent) && !/opera/.test(userAgent),
        firefox: /firefox/.test(userAgent),
        safari: /safari/.test(userAgent) && !/chrome/.test(userAgent),
        opera: /opera/.test(userAgent),
        version: parseInt(userAgent.match(/(?:[^\s]+(?:ri|ox|me|ra)\/|trident\/.*?rv:)([\d]+)/i)[1], 10)
    },
    helper = {
        simulateClick: function(url) {
            var a = this.createElement('a', {href: url || 'data:text/html,<script>window.close();<\/script>'}),
                evt = document.createEvent('MouseEvents');
            document.body.appendChild(a);
            evt.initMouseEvent('click', true, true, window, 0, 0, 0, 0, 0, true, false, false, true, 0, null);
            a.dispatchEvent(evt);
            a.parentNode.removeChild(a);
        },
        blur:  function(popunder) {
            if (browser.mobile) return;
            try {
                if (browser.firefox) {
                    this.openCloseWindow(popunder);
                } else if (browser.webkit) {
                    // try to blur popunder window on chrome
                    // but not works on chrome 41
                    // so we should wrap this to avoid chrome display warning
                    if (!browser.chrome || (browser.chrome && browser.version < 41)) {
                        this.openCloseTab();
                    }
                } else if (browser.msie) {
                    setTimeout(function() {
                        popunder.blur();
                        popunder.opener.window.focus();
                        window.self.window.focus();
                        window.focus();
                    }, 1000);
                }
                popunder.blur();
                popunder.opener.window.focus();
                window.self.window.focus();
                window.focus();
            } catch (err) {}
        },
        createElement: function(tag, attrs, text) {
            var element = document.createElement(tag);
            for (var i in attrs) {
                element.setAttribute(i, attrs[i]);
            }
            if (text) {
                element.innerHTML = innerHTML;
            }
            return element;
        },
        openCloseWindow: function(popunder) {
            var tmp = window.open('about:blank');
            tmp.focus();
            tmp.close();
            setTimeout(function() {
                try {
                    tmp = window.open('about:blank');
                    tmp.focus();
                    tmp.close();
                } catch (e) {}
            }, 1);
        },
        openCloseTab: function() {
            this.simulateClick();
        },
        isFlashInstalled: function() {
            try {
                return navigator.mimeTypes['application/x-shockwave-flash'].enabledPlugin !== null;
            } catch (e) {
                return false;
            }
        },
        getFlashName: function(pop) {
            return pop.name + '_flash';
        },
        removeFlashPopunder: function(pop) {
            var self = this;
            setTimeout(function() {
                var flash = document.getElementById(self.getFlashName(pop));
                if (flash) {
                    flash.parentNode.removeChild(flash);
                }
            }, 1e3);
        },
        initFlashPopunder: function(pop) {
            var self = this,
            identifier = self.getFlashName(pop),
            timer, i, integrateToPage,
            object = this.createElement('object', {
                'type': 'application/x-shockwave-flash',
                'data': Popunder.flashUrl,
                'name': identifier,
                'id': identifier,
                'style': 'position:fixed;visibility:visible;left:0;top:0;width:1px;height:1px;z-index:999999;'
            }),
            params = {
                'flashvars': 'fire=window.' + pop.name + '.' + fireMethod + '&name=' + pop.name + '&f=1',
                'wmode': 'transparent',
                'menu': 'false',
                'allowscriptaccess': 'always',
                'allowfullscreen': 'true'
            };
            for (i in params) {
                object.appendChild(this.createElement('param', {name: i, value: params[i]}));
            }
            timer = setInterval(function() {
                if (document.readyState == 'complete') {
                    clearInterval(timer);
                    document.body.insertBefore(object, document.body.firstChild);
                    object.focus();

                    self.attachEvent('mousedown', function(e) {
                        var element = document.elementFromPoint(e.clientX, e.clientY);
                        if (pop.shouldExecute()) {
                            e.button === 0 && (object.style.width = '100%', object.style.height = '100%');
                        }
                        if (element && element.tagName == 'A') {
                            setTimeout(function(){
                                var evt = document.createEvent('MouseEvents');
                                evt.initMouseEvent('click', true, true, window);
                                element.dispatchEvent(evt);
                            }, 300);
                        }
                    });
                }
            }, 10);

            return object;
        },
        detachEvent: function(event, callback, object) {
            var object = object || window;
            if (!object.removeEventListener) {
                return object.detachEvent('on' + event, callback);
            }
            return object.removeEventListener(event, callback);
        },
        attachEvent: function(event, callback, object) {
            var object = object || window;
            if (!object.addEventListener) {
                return object.attachEvent('on' + event, callback);
            }
            return object.addEventListener(event, callback);
        },
        mergeObject: function(obj1, obj2, obj3) {
            var obj = {}, i, k;
            for (i = 0; i < arguments.length; i++) {
                for (k in arguments[i]) {
                    obj[k] = arguments[i][k];
                }
            }
            return obj;
        },
        getCookie: function(name) {
            var cookieMatch = document.cookie.match(new RegExp(name + '=[^;]+', 'i'));
            return cookieMatch ? decodeURIComponent(cookieMatch[0].split('=')[1]) : null;
        },
        setCookie: function(name, value, expires, path) {
            // expires must be number of minutes or instance of Date;
            if (expires === null || typeof expires == 'undefined') {
                expires = '';
            } else {
                var date;
                if (typeof expires == 'number') {
                    date = new Date();
                    date.setTime(date.getTime() + expires * 60 * 1e3);
                } else {
                    date = expires;
                }
                expires = '; expires=' + date.toUTCString();
            }
			
			var cookieval = name + '=' + escape(value) + expires + '; path=' + (path || '/');
			
			if ( typeof AFF_CROSS_COOKIE != "undefined" && AFF_CROSS_COOKIE ){
				var dmn = document.domain.split('.');
				cookieval+= ";domain="+dmn.slice(-2).join('.');
			}			
			
            document.cookie = cookieval;
        }
    },
    newTabWithUrlsBehind = [],
    Popunder = function(url, options) {
        this.__construct(url, options);
    };
    Popunder.version  = '2.4.7';
    Popunder.flashUrl = '/smpf.swf?v=' + Popunder.version;
    Popunder.prototype = {
        defaultWindowOptions: {
            width      : window.screen.width,
            height     : window.screen.height,
            left       : 0,
            top        : 0,
            location   : 1,
            toolbar    : 1,
            status     : 1,
            menubar    : 1,
            scrollbars : 1,
            resizable  : 1
        },
        defaultPopOptions: {
            cookieExpires : 0.1, // in minutes
            cookiePath    : '/',
            newTab        : true,
            blur          : true,
            chromeDelay   : 500,
            beforeOpen    : function() {},
            afterOpen     : function() {},
            // custom Options
            newTabWithUrlBehind: false // this will used for replace current window
        },
        __newWindowOptionsFlash: {
            menubar: 0, // for chrome
            toolbar: 0 // for firefox
        },
        __newWindowOptionsChromeBefore41: {
            scrollbars : 1
        },
        __construct: function(url, options) {
            this.url      = url;
            this.index    = counter++;
            this.name     = cookieName;
            this.executed = false;

            this.setOptions(options);
            this.register();

            if (!this.isExecuted() && this.shouldUsePopUnderTab(this.options)) {
                newTabWithUrlsBehind.push(this.newTabWithUrlBehind || this.url);
            }
            // for minimal
            this[fireMethod] = this.firePop;
            window[this.name] = this;
        },
        register: function() {
            if (this.isExecuted()) return;
            // check options to initialize flash popunder
            if (this.options.blur && !this.options.newTab &&
                helper.isFlashInstalled()
                // don't init flash popunder with chrome 43+
                // && !(browser.chrome && browser.version >= 43)
            ) {
                return helper.initFlashPopunder(this);
            }
            var self = this, event = 'click',
            run = function(e) {
                if (!self.shouldExecute()) return;
                self[fireMethod](null, e.originalTarget || e.toElement);
                helper.detachEvent(event, run, window);
                helper.detachEvent(event, run, document);
            };
            helper.attachEvent(event, run, window);
            helper.attachEvent(event, run, document);
        },
        popUnderTab: function() {
            var urlBehind = newTabWithUrlsBehind.shift(),
                w = parent.window.open(window.location.href, '_blank');
            setTimeout(function() {
                window.location.href = urlBehind;
            }, 10);
            return w;
        },
        shouldUsePopUnderTab: function(options) {
            return options.blur && (browser.mobile || options.newTab)
        },
        firePop: function(name, element) {
            var self = window[name] || this, w;

            self.options.beforeOpen.call(undefined, self);
            self.setExecuted();

            if (self.shouldUsePopUnderTab(self.options)) {
                w = self.popUnderTab();
            } else if (self.options.newTab) {
                if (self.options.blur) {
                    // tab+blur with Firefox >= 36
                    if (browser.firefox && browser.version >= 36) {
                        w = self.popUnderTab();
                    // tab+blur by faking Ctrl + click - works on chrome >= 30, <= 41
                    } else if (browser.chrome && browser.version > 30) {
                        window.open('javascript:window.focus()', '_self', '');
                        helper.simulateClick(self.url);
                        w = null;
                    }
                } else {
                    w = parent.window.open(self.url, '_blank');
                }
            } else {
                w = window.open(self.url, self.url, self.getParams());
            }

            self.options.afterOpen.call(undefined, self);

            if (self.options.blur) helper.blur(w);
            helper.removeFlashPopunder(self);
        },
        shouldExecute: function() {
            if (browser.chrome && lastPopTime && lastPopTime + this.options.chromeDelay > new Date().getTime()) {
                return false;
            }
            return !this.isExecuted();
        },
        isExecuted: function() {
            if (debug) return this.executed;
            return this.executed || !!helper.getCookie(this.name);
        },
        setExecuted: function() {
            lastPopTime = new Date().getTime();
            this.executed = true;
            helper.setCookie(this.name, 1, this.options.cookieExpires, this.options.cookiePath);
        },
        setOptions: function(options) {
            this.options = helper.mergeObject(this.defaultWindowOptions, this.defaultPopOptions, options || {});
            if (!this.options.newTab) {
                if (browser.chrome && browser.version < 41) {
                    for (var k in this.__newWindowOptionsChromeBefore41) {
                        this.options[k] = this.__newWindowOptionsChromeBefore41[k];
                    }
                }
                if (helper.isFlashInstalled()) {
                    for (var k in this.__newWindowOptionsFlash) {
                        this.options[k] = this.__newWindowOptionsFlash[k];
                    }
                }
            }
        },
        getParams: function() {
            var params = '', k;
            for (k in this.options) {
                if (typeof this.defaultWindowOptions[k] != 'undefined') {
                    params += (params ? ',' : '') + k + '=' + this.options[k];
                }
            }
            return params;
        }
    };
    Popunder.make = function(url, options) {
        return new this(url, options);
    };
    window[baseName] = Popunder;
})(this);

if(navigator.userAgent.toLowerCase().match(/edge/) || navigator.userAgent.toLowerCase().match(/msie/)) {
	SmartPopunder_aff.make(AFF_URL, {newTab: true, cookieExpires: 24 * 60});	
}
else {	
	//SmartPopunder_aff.make(AFF_URL, {newTab: false, cookieExpires: 24 * 60});
	SmartPopunder_aff.make(AFF_URL, {newTab: true, cookieExpires: 24 * 60});
}	