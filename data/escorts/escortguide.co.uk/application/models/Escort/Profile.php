<?php

class Model_Escort_Profile extends Cubix_Model_Item
{
	protected $_idField = 'id';

	/**
	 * @var Zend_Session_Namespace
	 */
	protected $_session;

	const MODE_SESSION = 1;
	const MODE_REALTIME = 2;

	protected $_mode = self::MODE_SESSION;

	public function setMode($mode)
	{
		$this->_mode = $mode;
	}

	public function __construct($rowData)
	{
		parent::__construct($rowData);
	}

	public function load($clean = false)
	{
		if ( $clean ) {
			unset($this->_session->profile);
		}

		if ( empty($this->_session) || ! isset($this->_session->profile) ) {
			$this->_session->profile = $this->loadFromApi();
		}

		$data = $this->_session->profile;

		foreach ( $data as $key => $value ) {
			$this->$key = $value;
		}
	}

	public function loadFromApi()
	{
		$client = Cubix_Api::getInstance();

		if ( ! isset($this->id) || ! $this->id ) {
			$data = $client->call('getEscortProfileStruct');
		}
		else {
			$data = $client->call('getEscortProfileDataFront', array($this->getId()));
		}

		return $data;
	}

	public function setSession(Zend_Session_Namespace $session)
	{
		$this->_session = $session;
	}

	public function getBiography()
	{
		$this->setThrowExceptions(false);
		return $this->getData(array(
			'showname', 'gender', 'birth_date', 'ethnicity', 'nationality_id', 'tatoo', 'piercing',
			'measure_units', 'hair_color', 'hair_length', 'eye_color', 'height', 'weight', 'dress_size', 'shoe_size', 'bust', 'waist', 'hip', 'cup_size','breast_type', 'pubic_hair', 'home_city_id'
		));
	}

	public function getAboutMe()
	{
		$this->setThrowExceptions(false);
		return $this->getData(array_merge(Cubix_I18n::getTblFields('about', null, true), array(
			'is_smoking', 'is_drinking', 'characteristics'
		)));
	}

	public function getLanguages()
	{
		return $this->getData(array('langs'));
	}

	public function getWorkingCities()
	{
		$this->setThrowExceptions(false);
		return $this->getData(array(
			'country_id', 'city_id', 'cities', 'cityzones', 'zip', 'incall_type', 'incall_hotel_room', 'incall_other', 'outcall_type', 'outcall_other'
		));
	}

	public function getServices()
	{
		$this->setThrowExceptions(false);
		return $this->getData(array_merge(Cubix_I18n::getTblFields('additional_service', null, true), array(
			'sex_orientation', 'sex_availability', 'services', 'keywords'
		)));
	}
	
	public function getWorkingTimes()
	{
		$this->setThrowExceptions(false);
		return $this->getData(array(
			'available_24_7', 'night_escort', 'times','night_escorts'
		));
	}

	public function getPrices()
	{
		$this->setThrowExceptions(false);
		$data = $this->getData(array(
			'rates'
		));

		$data['rates'] = $this->reconstructRates($data['rates']);

		return $data;
	}

	public function getContactInfo()
	{
		$this->setThrowExceptions(false);
		return $this->getData(array(
			'phone_country_id','phone_number', 'phone_number_alt', 'phone_instr', 'phone_instr_no_withheld',
			'phone_instr_other', 'email', 'website', 'club_name', 'street', 'street_no'
		));
	}

	public function getAvailableApps()
	{
		$client = Cubix_Api::getInstance();
		return $client->call('getAvailableApps', array($this->id));
	}

	public function setAvailableApps($data)
	{
		$viber = $data['viber'];
		$whatsapp = $data['whatsapp'];
		$client = Cubix_Api::getInstance();
		$client->call('setAvailableApps', array($this->id, $viber, $whatsapp));
	}

	public function reconstructRates($data)
	{
		$result = array('incall' => array(), 'outcall' => array());

		foreach ( $data as $rate ) {
			if ( $rate['availability'] == 1 ) {
				$key = 'incall';
			}
			elseif ( $rate['availability'] == 2 ) {
				$key = 'outcall';
			}

			if ( $rate['type'] ) {
				$result[$key][] = array('type' => $rate['type'], 'price' => $rate['price'], 'currency' => $rate['currency_id']);
			}
			else {
				$result[$key][] = array('time' => $rate['time'], 'unit' => $rate['time_unit'], 'price' => $rate['price'], 'currency' => $rate['currency_id']);
			}
		}

		return $result;
	}

	public function update($data)
	{
		$result = true;

		$this->_session->profile = array_merge($this->_session->profile, $data);
		
		if ( self::MODE_REALTIME == $this->_mode ) {
			$result = $this->flush();
		}

		return $result;
	}

	public function flush(array $data = array())
	{
		if ( self::MODE_REALTIME != $this->_mode ) {
			if ( is_null($this->_session->profile['showname']) ) {
				return true;
			}
		}

		$client = Cubix_Api::getInstance();

		if ( ! $this->getId() ) {
			$this->setId(0);
		}


		if ( self::MODE_SESSION == $this->_mode ) {
			$err = false;

			if ( ! isset($this->_session->profile['showname']) && ! strlen($this->_session->profile['showname']) ) {
				$err = true;
			}
			
			if ( ! isset($this->_session->profile['gender']) && ! strlen($this->_session->profile['gender']) ) {
				$err = true;
			}

			if ( ! isset($this->_session->profile['langs']) && ! count($this->_session->profile['langs']) ) {
				$err = true;
			}

			if ( ! isset($this->_session->profile['country_id']) && ! strlen($this->_session->profile['country_id']) ) {
				$err = true;
			}
			if ( ! isset($this->_session->profile['city_id']) && ! strlen($this->_session->profile['city_id']) ) {
				$err = true;
			}
			if ( ! isset($this->_session->profile['cities']) && ! count($this->_session->profile['cities']) ) {
				$err = true;
			}

			if ( ! isset($this->_session->profile['sex_availability']) && ! strlen($this->_session->profile['sex_availability']) ) {
				$err = true;
			}
			
			if ( ! isset($this->_session->profile['email']) && ! strlen($this->_session->profile['email']) ) {
				$err = true;
			}

			if ( $err ) {
				unset($this->_session->profile);
				return false;
			}
		}

		$result = $client->call('updateEscortProfileV2', array($this->getId(), serialize(array_merge($this->_session->profile, $data)), $section));
		//print_r($result);die;
		unset($this->_session->profile);

		return $result;
	}
}
