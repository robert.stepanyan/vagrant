<?php

class Model_Agencies extends Cubix_Model
{
	protected $_table = 'agencies';
	protected $_itemClass = 'Model_AgencyItem';

	public static function hit($agency_id)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();

		$client->call('Agencies.updateHitsCount', array($agency_id));

		// for front DB
		self::getAdapter()->query('UPDATE club_directory SET hit_count = hit_count + 1 WHERE agency_id = ?', $agency_id);
	}
	
	/*public function getIdByName($name)
	{
		$sql = "
			SELECT id
			FROM agencies a
			WHERE name = ?
		";
		
		$agency = parent::_fetchRow($sql, $name);
		
		if ( $agency )
			return $agency->id;
		
		return null;
	}*/
	
	public function getIdByName($name)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		
		$result = $client->call('Agencies.getByName', array($name));
		
		return $result;
	}
	
	public function get($agency_id)
	{
		$sql = "
			SELECT a.id, a.user_id, a.name, a.phone, a.phone_instructions, a.email, a.web, a.last_modified, a.logo_hash, a.logo_ext, u.application_id, u.date_registered AS creation_date, a.hit_count
			FROM agencies a
			LEFT JOIN users u ON u.id = a.user_id
			WHERE a.id = ?
		";
		
		return parent::_fetchRow($sql, $agency_id);
	}
	
	public function getByUserId($user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$agency = $client->call('Agencies.getByUserId', array($user_id));
		
		$agency = new Model_AgencyItem($agency);
		
		return $agency;
	}
	
	public function getAll($filter = array(), $page = 1, $page_size = 20, $sorting = null, &$count = 0)
	{
		$lng = Cubix_I18n::getLang();

		$page = (int) $page;
		if ( $page < 1 ) $page = 1;
		$limit = ($page_size * ($page - 1)) . ', ' . $page_size;

		$where = " WHERE 1 ";

		$order = self::_mapSorting($sorting);

		if ( isset($filter['text']) && strlen($filter['text']) ) {
			$where .= " AND cd.about_en LIKE '%" . $filter['text'] . "%' ";
		}

		if ( isset($filter['name']) && strlen($filter['name']) ) {
			$where .= " AND cd.club_name LIKE '" . $filter['name'] . "%' ";
		}

		if ( isset($filter['city_slug']) && strlen($filter['city_slug']) ) {
			$where .= " AND ct.slug = '" . $filter['city_slug'] . "' ";
		}

		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				cd.agency_id AS id,
				cd.club_name AS showname,
				cd.club_slug AS club_slug,
				cd.is_new,
				UNIX_TIMESTAMP(cd.date_registered) AS date_registered,
				cd.user_id,			
				ct.slug AS city_slug,
				ct.id AS city_id,
				ct.title_' . $lng . ' AS city,			
				cr.id AS country_id,
				cr.slug AS country_slug,
				cr.title_' . $lng . ' AS country,				
				cd.photo_hash AS logo_hash,
				cd.photo_ext AS logo_ext,				
				5 AS application_id,		
				cd.email,
				cd.web AS website,				
				cd.phone_country_id,				
				cd.phone AS phone_number,
				cd.hit_count,				
				cd.last_modified AS date_last_modified,
				cd.is_premium,				
				cd.about_' . $lng . ' AS about,
				cd.club_name AS agency_name,
				cd.club_slug AS agency_slug,
				cd.escorts_count,
				cd.verified_escorts_count,
				IF (ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE), 1, 0) AS is_login, cd.allow_show_online,
				cd.agency_id AS agency_id
			FROM club_directory cd
			LEFT JOIN cities ct ON ct.id = cd.city_id
			LEFT JOIN countries cr ON cr.id = cd.country_id
			LEFT JOIN users_last_refresh_time ulrt ON ulrt.user_id = cd.user_id
			' . $where . '
			GROUP BY cd.agency_id
			ORDER BY ' . $order . '
			LIMIT ' . $limit . '
		';		

		$agencies = self::db()->fetchAll($sql);
		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');

		return $agencies;
	}

	private static function _mapSorting($param)
	{
		$map = array(
			'alpha' => 'cd.is_premium DESC, cd.club_name ASC',
			'random' => 'cd.is_premium DESC, RAND()',
			'last_modified' => 'cd.is_premium DESC, cd.last_modified DESC',
			'premium' => 'cd.is_premium DESC',
			'by-city' => 'ct.title_en ASC',
			'by-country' => 'cr.title_en ASC',
			'most-viewed' => 'cd.hit_count DESC',
			'close-to-me' => 'distance ASC',
			'last-connection' => 'ulrt.refresh_date DESC',
			'newest' => 'cd.date_registered DESC',
		);

		$order = 'cd.club_name ASC';
		if ( isset($map[$param]) ) {
			$order = $map[$param];
		}
		elseif ( false !== strpos($param, 'FIELD') ) {
			$order = $param;
		}

		return $order;
	}

	public function save($agency)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$agency_id = $client->call('Agencies.save', array( (array) $agency));
		$agency->setId($agency_id);
		
		return $agency;
	}

	public function getBySlugId($slug, $id)
	{
		$sql = '
			SELECT
				cd.agency_id AS id,
				cd.club_name AS name,
				cd.phone,
				cd.phone_country_id,
				cd.phone_instructions,
				cd.phone_1,
				cd.phone_country_id_1,
				cd.phone_instructions_1,
				cd.phone_2,
				cd.phone_country_id_2,
				cd.phone_instructions_2,
				cd.email,
				cd.web,
				cd.check_website,
				cd.block_website,
				cd.last_modified,
				cd.photo_hash AS logo_hash,
				cd.photo_ext AS logo_ext,
				cd.date_registered AS creation_date,
				cd.hit_count,
				cd.available_24_7,
				cd.is_open,
				cd.working_times,
				cd.phone_instr,
				cd.phone_instr_1,
				cd.phone_instr_2,
				' . Cubix_I18n::getTblFields('cd.about') . '
			FROM club_directory cd
			WHERE cd.club_slug = ? AND cd.agency_id = ?
		';

		return $this->db()->query($sql, array($slug, $id))->fetch();
	}

	public function getCountsById($agency_id)
	{
		return self::db()->fetchRow("SELECT escorts_count, verified_escorts_count FROM club_directory WHERE agency_id = ?", $agency_id);
	}
}