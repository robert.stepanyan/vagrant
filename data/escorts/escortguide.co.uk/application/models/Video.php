<?php


class Model_Video extends Cubix_Model {
	protected $_table = 'escorts';
	private $db;
	
	public $dimensions = array(
		"720" => array(
			'height' => 720, 'bitrate' => 1200
		),
		"480" => array(
			'height' => 480, 'bitrate' => 600
		),
		"360" => array(
			'height' => 360, 'bitrate' => 250
		)
	);
	
	public function __construct() 
	{
		
		$this->db = Zend_Registry::get('db');
	}

	public function GetEscortVideo($escort_id, $private = TRUE)
	{
		$escort_id = $this->db->quote($escort_id);
		if($private)
		{	
			$client = Cubix_Api_XmlRpc_Client::getInstance();
			return  $client->call('Application.GetVideo',array($escort_id));
		}
		else
		{
			$sql_video = "SELECT * FROM video WHERE escort_id = ?";
			$video = parent::_fetchAll($sql_video, $escort_id);
			
			$photo = array();
			if(!empty($video))
			{
				$v_id =array();
				foreach ($video as $v)
				{
					$v_id[]=$v->id;
				}
				$v_id =  implode(',', $v_id);
				$sql_image = "SELECT * FROM video_image  WHERE video_id IN($v_id) ";
				$photo =  parent::_fetchAll($sql_image);
			}
			return array($photo,$video);
		}
	}
	
	public function GetEscortsVideoArray(array $escorts)
	{	
		$escorts = implode(',', $escorts);
		$sql = "SELECT video,`hash`,width,height,escort_id,ext FROM video_image AS img 
				JOIN (SELECT video,escort_id,id FROM video WHERE escort_id IN($escorts) ORDER BY `date` DESC ) AS v 
				ON v.id=img.video_id 
				GROUP BY escort_id ORDER BY img.`date` DESC ";
		return parent::_fetchAll($sql);
	}
	
	public function RemoveV2($video_id,$escort_id)
	{	
		$client = Cubix_Api_XmlRpc_Client::getInstance();	
		$affected_video_id = $client->call('Application.RemoveVideo',array($escort_id, $video_id));
		if($affected_video_id){
			$this->db->query("DELETE FROM video WHERE escort_id = ? AND id = ? ",array($escort_id, $video_id ));
		}
		return $affected_video_id;
		
	}
	
	public function GetAgencyEscort($agency_id)
	{	
		$escorts =parent::_fetchAll("SELECT showname,id FROM  escorts WHERE agency_id=?  ",$agency_id);

		if(!empty($escorts))
		{	
			$client = Cubix_Api_XmlRpc_Client::getInstance();
			$escorts=$client->call('Application.GetAgencyEscortVideo',array($agency_id,$escorts));
		}
		return $escorts;
	}
	
	public function GetAgencyEscorts($id,$escort_id)
	{	
		$escort_id = $this->db->quote($escort_id);
		$escorts = parent::_fetchAll("SELECT showname,id FROM escorts WHERE agency_id='$id' AND id=$escort_id ");
		return empty($escorts)?FALSE:TRUE;
	}


	public function UserVideoCount($escort_id)
	{	
		$escort_id = $this->db->quote($escort_id);
		$count= parent::_fetchRow("SELECT count(id) AS quantity FROM video WHERE escort_id=$escort_id  GROUP BY escort_id ");
		return $count->quantity;
	}
	
	public function getManifestData($escort_id)
	{
		return parent::_fetchRow("SELECT video, height, width FROM video WHERE escort_id = ?", array($escort_id));
	}

	public function getVideos($filter, $page = 1, $per_page = 40)
	{
		$where = '';
		$where .= Cubix_Countries::blacklistCountryWhereCase();
		if(isset($filter['city_id']) && $filter['city_id']){
			$where .= " AND eic.city_id = " . $filter['city_id'];
		}
		
		$sql_data = "
			SELECT SQL_CALC_FOUND_ROWS e.showname, v.escort_id, v.video, "
				. Cubix_I18n::getTblField('c.title') . " as city , vi.hash, vi.ext
			FROM video v 
			INNER JOIN escorts e ON v.escort_id = e.id
			INNER JOIN escorts_in_cities eic ON eic.escort_id = e.id
			INNER JOIN cities c ON eic.city_id = c.id
			INNER JOIN video_image vi ON v.id = vi.video_id
			WHERE e.has_video = 1" .$where ;

		$sql_data .= "
			GROUP BY v.id
			ORDER BY v.date DESC 
			LIMIT " . ($page - 1) * $per_page . ', ' . $per_page;

		$data = self::getAdapter()->fetchAll($sql_data);
		$count = self::getAdapter()->fetchOne('SELECT FOUND_ROWS()');
		
		foreach($data as $video){
			$photo = new Cubix_ImagesCommonItem($video);
			$video->photo = $photo->getUrl('video_thumb_m');
		}
		
		return array('data' => $data, 'count' => $count);
	}
	
	public function getAll($page = 1, $per_page = 50, $city_id = null)
	{
		$lng = Cubix_I18n::getLang();
		$city_title = 'c.title_' . $lng;
		
		$sql_count  = "
			SELECT v.id, c.id, COUNT(*) AS v_count, $city_title AS city_title

			FROM video v
			INNER JOIN escorts e ON v.escort_id = e.id
			INNER JOIN escorts_in_cities eic ON eic.escort_id = e.id
			INNER JOIN cities c ON eic.city_id = c.id
			WHERE e.has_video = 1

		";

		if ($city_id) {
			$sql_count .= " AND c.id = " . $city_id;
		}

		$sql_count .= "
			GROUP BY c.id
		";

		$total_video_data = self::getAdapter()->fetchAll($sql_count);


		$sql_data = "
			SELECT e.showname, v.id AS video_id, v.escort_id, v.video, 
				UNIX_TIMESTAMP(v.date), vi.hash, vi.ext, c.*

			FROM video v 
			INNER JOIN escorts e ON v.escort_id = e.id
			INNER JOIN escorts_in_cities eic ON eic.escort_id = e.id
			INNER JOIN cities c ON eic.city_id = c.id
			INNER JOIN video_image vi ON v.id = vi.video_id

			WHERE e.has_video = 1";

		if ($city_id) {
			$sql_data .= " AND c.id = " . $city_id;
		}

		$sql_data .= "
			GROUP BY v.id
			ORDER BY e.ordering ASC
			LIMIT " . ($page - 1) * $per_page . ', ' . $per_page;

		$page_data = self::getAdapter()->fetchAll($sql_data);

		return array('page_data' => $page_data, 'total_video_data' => $total_video_data);
	}

}
