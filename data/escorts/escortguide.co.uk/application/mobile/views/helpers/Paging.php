<?php

class Zend_View_Helper_Paging
{
	public function paging($count, $page, $perPage, $c = 6, $lf = 2)
	{
		$pages = ceil($count / $perPage);
		
		$start_page = 1;
        $end_page = $pages;
        
        if ( $c < $pages )  {
            $start_page = 1;
            $end_page = $c;
            if ( $lf < $page ) {
                $start_page = $page - $lf;
                $end_page = $page + $lf;
                if ( $page + $lf > $pages ) {
                    $start_page = $pages - $c;
                    $end_page = $pages;
                }
            }
        }
        
        $pages_arr = array();
        
        for ($i = $start_page; $i <= $end_page; $i++) {
            $pages_arr[] = $i;
        }

        return $pages_arr;
	}
}
