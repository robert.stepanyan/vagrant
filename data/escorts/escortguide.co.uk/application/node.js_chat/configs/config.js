function Config() {
	var config = {
		production : {
			common : {
				applicationId : 5,
				listenPort : 8888,
				host: 'www.escortguide.co.uk',
				authPath : '/get_user_info.php',
				sessionChacheTime: 1000 * 60 * 10, //10 minutes
				sessionCheckInterval : 1 * 60 * 60 * 1000, //1 hour
				imagesHost : 'http://pic4.escortguide.co.uk/escortguide.co.uk/',
				avatarSize : 'lvthumb',
				noPhotoUrl : '/img/chat/img-no-avatar.gif',
				userSettingsBackupPath : 'user_settings.bk',
				key : 'JFusLsdf8A9',
				blWordsLifeTime : 12 * 60 * 60 * 1000 //12 hours
			},
			db : {
				user:     'eg_cubix_chat',
				database: 'main_v2',
				password: 'Ghhy25p4',
				host:     'backend.escortguide.co.uk'
			},
			memcache : {
				host : '127.0.0.1',
				port : 11211
			}
		},
		development: {
			common : {
				applicationId : 5,
				listenPort : 8899,
				host: 'www.escortguide.co.uk',
				authPath : '/get_user_info.php',
				sessionChacheTime: 1000 * 60 * 10, //10 minutes
				sessionCheckInterval : 1 * 60 * 60 * 1000, //1 hour
				imagesHost : 'http://pic4.escortguide.co.uk/escortguide.co.uk/',
				avatarSize : 'lvthumb',
				noPhotoUrl : '/img/chat/img-no-avatar.gif',
				userSettingsBackupPath : 'user_settings.bk',
				key : 'JFusLsdf8A9',
				blWordsLifeTime : 12 * 60 * 60 * 1000 //12 hours
			},
			db : {
				user:     'eg_cubix_chat',
				database: 'main_v2',
				password: 'Ghhy25p4',
				host:     'backend.escortguide.co.uk'
			},
			memcache : {
				host : '127.0.0.1',
				port : 11211
			}
		}
	};
	
	this.get = function(type) {
		if ( type == 'development' ) {
			return config.development;
		} else {
			return config.production;
		}
	}
}

function get(type) {
	conf = new Config();
	return conf.get(type);
}



exports.get = get;
