/*jslint es6*/
/*jslint node*/

'use strict';

module.exports = (() => {
  const configs = {
    production: {
      common: {
        applicationId: 1,
        listenPort: 8888,
        host: 'www.escortforumit.xxx',
        authPath: '/get_user_info.php',
        sessionCacheTime: 1000 * 60 * 10, //10 minutes
        sessionCheckInterval: 1 * 60 * 60 * 1000, //1 hour
        imagesHost: 'http://pic.escortforumit.xxx/escortforumit.xxx/',
        avatarSize: 'lvthumb',
        noPhotoUrl: '/img/chat/img-no-avatar.gif',
        userSettingsBackupPath: 'user_settings.bk',
        key: 'JFusLsdf8A9',
        blWordsLifeTime: 12 * 60 * 60 * 1000, //12 hours
        redisBlWordsDb: 0,
        redisBlWordsKey: 'bl-words-ef'
      },
      pushNotifications: {
        auth: 'ul46sers9wf9u12nz40mgyj6weiybjpo:3kfa34irl0h3a75jf4rpjxpeaj5pemhm',
        url: 'https://dashboard.goroost.com/api/v2/notifications'
      },
      db: {
        user: 'chat2',
        database: 'escortforum_net',
        password: 'kEHJnc74n91wEn34',
        host: 'db0'
      },
      redis: {
        host: 'db0',
        port: 6379
      },
      memcache: {
        host: 'fe01',
        port: 11211
      }
    },

    development: {
      common: {
        applicationId: 1,
        listenPort: 8888,
        instantBookListenPort: 8889,
        host: 'www.escortforumit.xxx.test',
        authPath: 'get_user_info.php',
        sessionCacheTime: 1000 * 60 * 10, //10 minutes
        sessionCheckInterval: 30 * 1000, //1 hour
        imagesHost: 'http://pic.escortforumit.xxx/escortforumit.xxx/',
        avatarSize: 'lvthumb',
        noPhotoUrl: '/img/chat/img-no-avatar.gif',
        userSettingsBackupPath: 'user_settings.bk',
        key: 'JFusLsdf8A9',
        blWordsLifeTime: 12 * 60 * 60 * 1000, //12 hours
        redisBlWordsDb: 0,
        redisBlWordsKey: 'bl-words-ef'
      },
      db: {
        user: 'sceon',
        database: 'escortforum_net',
        password: 'sceon123456',
        host: '10.10.0.55'
      },
      redis: {
        host: '127.0.0.1',
        port: 6379
      },
      memcache: {
        host: '127.0.0.1',
        port: 49664
      }
    }
  };


  const get = env_ => {
    !env_ && (env_ = 'production');

    return configs[env_];
  };

  return { get: get };
})();