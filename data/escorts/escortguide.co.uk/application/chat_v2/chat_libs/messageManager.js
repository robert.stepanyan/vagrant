  /*jslint es6*/
/*jslint node*/

'use strict';

const MySQL = require('mysql');
const Redis = require('redis');
const config = require('../configs').get('production');
const { leadWithZero, toMySQLFormat, htmlEntities } = require('./utils');

class Messages {
  constructor() {
    this.establishMySQLConnection();

    setInterval(() => {
      this.MySQLClient.ping(err => {
        if (err) {
          this.handleMySQLConnectionerr(err);
        }
      });
    }, 5000);

    this.redisClient = Redis.createClient(config.redis.port, config.redis.host);
    this.redisClient.on('error', err => {
      console.log(`Error: ${redisClient.host}:${redisClient.port} - ${err}`);
    });
  }

  // DB CONNECTION SERVICES

  establishMySQLConnection() {
    this.MySQLClient = MySQL.createConnection({
      user: config.db.user,
      database: config.db.database,
      password: config.db.password,
      host: config.db.host,
      port: 3306
    });

    this.MySQLClient.connect(err => {});
  }

  handleMySQLConnectionerr(err) {
    if (!err.fatal) {
      return false;
    }

    if (err.code !== 'PROTOCOL_CONNECTION_LOST') {
      console.log(
        'The mysql library fired a PROTOCOL_CONNECTION_LOST exception'
      );

      throw err;
    }

    console.log('Re-connecting lost connection: ' + err.stack);
    this.establishMySQLConnection();
  }

  // MESSAGES SETTERS

  storeMessagePromise(userIdFrom, userIdTo, message) {
    let self = this;
    return new Promise((resolve, reject) => {
      this.getThreadIdPromise(userIdFrom, userIdTo).then(threadId => {
        self.insertMessagePromise(userIdFrom, threadId, message).then(result => {
          return resolve(result);
        });
      });
    });
  }

  insertMessagePromise(userId, threadId, message) {
    return new Promise((resolve, reject) => {
      let date = new Date();
      let _query = `
        INSERT INTO chat_messages (user_id, thread_id, body, date) 
        VALUES(${userId}, ${threadId}, '${message}', '${toMySQLFormat(date)}')
      `;

      this.MySQLClient.query(_query, (err, result) => {
        if (err) {
          return reject(err);
        }

        return resolve(result);
      });
    });
  }

  // MESSAGES GETTERS

  getThreadIdPromise(userIdFrom, userIdTo) {
    return new Promise((resolve, reject) => {
      let _query = `
        SELECT tp1.thread_id AS threadId
        FROM chat_threads_participants tp1
        INNER JOIN chat_threads_participants tp2 ON tp2.thread_id = tp1.thread_id AND tp2.user_id = ${userIdFrom}
        WHERE tp1.user_id = ${userIdTo}
      `;

      this.MySQLClient.query(_query, (err, result) => {
        if (err) reject(err);

        if (!result || !result.length) {
          let _query = `INSERT INTO chat_threads VALUES()`;

          this.MySQLClient.query(_query, (err, result) => {
            this.createThreadParticipantsPromise(
              result.insertId,
              userIdFrom,
              userIdTo
            ).then(threadId => {
              return resolve(threadId);
            });
          });
        } else {
          return resolve(result[0].threadId);
        }
      });
    });
  }

  createThreadParticipantsPromise(threadId, userIdFrom, userIdTo) {
    return new Promise((resolve, reject) => {
      let _query1 = `
        INSERT INTO chat_threads_participants (user_id, thread_id)
        VALUES(${userIdFrom},${threadId})
      `;

      this.MySQLClient.query(_query1, (err, result) => {
        let _query2 = `
          INSERT INTO chat_threads_participants (user_id, thread_id)
          VALUES(${userIdTo}, ${threadId})
        `;

        this.MySQLClient.query(_query2, (err, result) => {
          return resolve(threadId);
        });
      });
    });
  }

  getConversationPromise(userId, participantId, dateFrom) {
    return new Promise((resolve, reject) => {
      this.getThreadIdPromise(userId, participantId).then(threadId => {
        let where = '';

        if (!dateFrom) {
          where = `WHERE thread_id = ${threadId}`;
        } else {
          where = `WHERE thread_id = ${threadId} AND (date >= '${toMySQLFormat(dateFrom)}' OR read_date IS NULL)`;
        }

        let _query = `
          SELECT user_id AS userId, body, UNIX_TIMESTAMP(date) * 1000 AS date
          FROM chat_messages ${where}
          ORDER BY id ASC
        `;

        this.MySQLClient.query(_query, (err, result) => {
          return resolve(result);
        });
      });
    });
  }

  getNewMessagesCountPromise(userId) {
    return new Promise((resolve, reject) => {
      let _query = `
        SELECT count(cm.id) AS count, u.id AS userId, u.username
        FROM chat_threads_participants ctp
        INNER JOIN chat_messages cm ON cm.thread_id = ctp.thread_id
        INNER JOIN users u ON u.id = cm.user_id WHERE ctp.user_id = ${userId} AND cm.user_id <> ${userId} AND read_date IS NULL
        GROUP BY cm.user_id
      `;

      this.MySQLClient.query(_query, (err, result) => {
        return resolve(result);
      });
    });
  }

  // MESSAGES MANIPULATIONS

  markAsReadPromise(userId, participantId) {
    return new Promise((resolve, reject) => {
      this.getThreadIdPromise(userId, participantId).then(threadId => {
        let date = new Date();
        let _query = `
          UPDATE chat_messages
          SET read_date = ${toMySQLFormat(date)}
          WHERE thread_id = ${threadId} AND user_id = ${participantId}
        `;

        this.MySQLClient.query(_query, (err, result) => {
          return resolve(true);
        });
      });
    });
  }

  clearMessage(message) {
    return htmlEntities(message)
      .trim()
      .replace(/\s+/g, ' ');
  }

  checkMessagePromise(message) {
    return new Promise((resolve, reject) => {
      this.redisClient.select(config.common.redisBlWordsDb, () => {
        this.redisClient.smembers(
          config.common.redisBlWordsKey,
          (err, result) => {
            let matches = [];

            if (err || !result.length) {
              return resolve(matches);
            }

            result.forEach(wordPairStr => {
              let wordPair = wordPairStr.split(',');
              let type = wordPair[1];
              let word = wordPair[0].toLowerCase();
              let lowerCasedMsg = message.toLowerCase();
              let match = false;

              if (type === 1) {
                if (lowerCasedMsg.indexOf(word) !== -1) {
                  match = true;
                }
              } else {
                let m = lowerCasedMsg.match('/\b' + word + '\b/i');
                if (m && m.length) match = true;
              }

              if (match) {
                matches.push(word);
              }
            });
            return resolve(matches);
          }
        );
      });
    });
  }

  // BLACKLISTED WORDS SERVICE

  initBlackListedWordsSyncPromise() {
    setInterval(() => {
      return new Promise((resolve, reject) => {
        this.syncBlackListedWordsPromise().then(() => {
          return resolve(true);
        });
      });
    }, config.common.blWordsLifeTime);
  }

  syncBlackListedWordsPromise() {
    return new Promise((resolve, reject) => {
      let query = `
        SELECT name, search_type
        FROM blacklisted_words
        WHERE types & 8
      `;

      this.MySQLClient.query(query, (err, result) => {
        if (err) {
          return reject(err);
        } else {
          this.redisClient.select(config.common.redisBlWordsDb, () => {
            this.redisClient.flushdb((err, result) => {
              result.forEach(el => {
                this.redisClient.sadd(
                  config.common.redisBlWordsKey,
                  [el.name, el.search_type].join(',')
                );
              });
            });
          });
        }
      });
    });
  }
}

module.exports = new Messages();
