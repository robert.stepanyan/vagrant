/*jslint es6*/
/*jslint node*/

"use strict";


  const leadWithZero = d => {
    let prepended = `0${Math.abs(d)}`.slice(-2);
    return d < 0 ? `-${prepended}` : prepended;
  };

  const toMySQLFormat = date => {
    return `
            ${date.getUTCFullYear()}-${leadWithZero(
      1 + date.getUTCMonth()
    )}-${leadWithZero(date.getUTCDate())} ${leadWithZero(date.getUTCHours())}:${leadWithZero(
      date.getUTCMinutes()
    )}:${leadWithZero(date.getUTCSeconds())}
        `;
  };

  const htmlEntities = str => {
      return String(str)
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/'/g, "&apos;")
        .replace(/"/g, "&quot;");
  };

  const nullifyTime = date => {
      date.setHours(0);
      date.setMinutes(0);
      date.setSeconds(0);

      return date;
  };

  module.exports = { leadWithZero, toMySQLFormat, htmlEntities, nullifyTime };