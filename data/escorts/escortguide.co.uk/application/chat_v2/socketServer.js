/*jslint es6*/
/*jslint node*/

'use strict';

let chat = {};
let instantBook = {};

const http = require('http');
const crypto = require('crypto');
const url = require('url');

const socketIo = require('socket.io');

const config = require('./configs').get('production');
const { nullifyTime, toMySQLFormat } = require("./utils/");

chat.userManager = require('./chat_libs/userManager');
chat.messageManager = require('./chat_libs/messageManager');
chat.settingsManager = require('./chat_libs/settingsManager');
chat.bridge = require('./chat_libs/bridge');
instantBook.userManager = require('./instantbook_libs/userManager');
instantBook.bookingManager = require('./instantbook_libs/bookingManager');
instantBook.request = require('superagent');


const app = http.createServer(handler).listen(config.common.listenPort);
const io = socketIo(app);
const chatIo = io.of('/chat');
const instantBookIo = io.of('/booking');

function handler(request, response) {
  let queryData = url.parse(request.url, true);
  let query = queryData.query;

  // SET AVAILABILITY

  if (queryData.pathname === '/node-chat/change-availability') {
    let uid = query.userId;
    let availability = query.availability;
    let hash = query.hash;

    if (!uid || !availability || !hash || crypto.createHash('md5').update(`${uid}-${availability}-${config.common.key}`).digest('hex') !== hash) {
      response.writeHead(200, {
        'Content-Type': 'text/plain'
      });
      response.end('sta=error');
    }

    console.log(`Changing availability for userId ${uid} to ${availability}`);

    chat.settingsManager.setAvailability(uid, availability);

    response.writeHead(200, {
      'Content-Type': 'text/plain'
    });
    response.end('sta=ok');
  }

  // GET AVAILABILITY

  if (queryData.pathname === '/node-chat/get-availability') {
    let uid = query.userId;
    let hash = query.hash;

    if (!uid || crypto.createHash('md5').update(`${uid}-${config.common.key}`).digest('hex') !== hash) {
      response.writeHead(200, {
        'Content-Type': 'text/plain'
      });
      response.end('sta=error');
    }

    let result = JSON.stringify({
      availability: chat.settingsManager.getAvailability(uid)
    });

    response.writeHead(200, {
      'Content-Type': 'text/plain'
    });
    response.end(result);
  }

  // GET ONLINE ESCORTS

  if (queryData.pathname === '/node-chat/get-online-escorts') {
    chat.userManager.getOnlineEscortsPromise().then(userIds => {
      response.writeHead(200, {
        'Content-Type': 'text/plain'
      });
      response.end(JSON.stringify(userIds));
    });
  }
}

chat.settingsManager.initSettingsBackupPromise().then(() => {
  console.log('User settings backup done');
});

chat.userManager.initOfflineUsersCheckPromise().then(userIds => {
  if (userIds.length) {
    chatIo.sockets.emit('offline-users', userIds);
  }
});

chat.messageManager.initBlackListedWordsSyncPromise(() => {
  console.log('Bl word sync is done.');
});

// SESSION CHECK: KICKING OUT USERS WITH EXPIRED SESSIONS

setInterval(() => {
  let users = chat.userManager.getUsersPrivateList();
  Object.keys(users).forEach(userId => {
    chat.bridge.getUserInfo(users[userId].info.sid, false).then(resp => {
      if (!resp.auth && users[userId]) {
        users[userId].sockets.forEach(socketId => {
          chatIo.connected[socketId] && chatIo.connected[socketId].disconnect();
        });

        users[userId].sockets = [];
        chat.userManager.addUserWithoutSockets(userId);
      }
    });
  });
}, config.common.sessionCheckInterval);

const startChat = () => {
  chatIo.on('connection', socket => {
    socket.on('auth', data => {
      chat.bridge.getUserInfo(data.sid).then(resp => {
        if (!resp.auth) {
          socket.emit('auth-complete', {
            auth: false
          });
          socket.disconnect('unauthorized');
          return false;
        }

        let userInfo = resp.data;

        userInfo.status = 'online';
        userInfo.sid = data.sid;

        if (userInfo.userType != 'escort') {
          userInfo.avatar = config.common.noPhotoUrl;
        }

        let availability = 1;

        if (data.forceEnable) {
          chat.settingsManager.setAvailability(userInfo.userId, 1);
        } else {
          availability = chat.settingsManager.getAvailability(userInfo.userId);
        }

        userInfo.settings = chat.settingsManager.getSettings(userInfo.userId);

        socket.emit('auth-complete', {
          auth: true,
          availability: availability,
          userInfo: userInfo
        });

        if (availability) {
          socket.userInfo = userInfo;

          let newUser = true;
          chat.userManager.getUserPromise(userInfo.userId).then(user => {
            if (user) {
              newUser = false
            }

            let isPrivate = !!socket.userInfo.settings.invisibility;
            chat.userManager.addUser(socket.id, userInfo, isPrivate);
            if (newUser && !isPrivate) {
              delete userInfo.settings;

              socket.broadcast.emit('new-user', userInfo);
            }

            let pubList = chat.userManager.getUsersPublicList();

            delete pubList[userInfo.userId];
            
            socket.emit('online-users-list', pubList);

            // Checking if has opened dialogs.
            // If yes getting userInfo and emiting open-dialogs event
            let openDialogs = chat.settingsManager.getOpenDialogs(userInfo.userId);

            if (openDialogs) {
              chat.userManager.getUsersPromise(openDialogs, true).then(usersObj => {
                let od = openDialogs.map(userId => {
                  if (usersObj[userId]) {
                    return {
                      userId: userId,
                      userInfo: usersObj[userId].info
                    };
                  }
                });
  
                let activeDialog = chat.settingsManager.getActiveDialog(userInfo.userId);
                socket.emit('open-dialogs', {
                  dialogs: od,
                  activeDialog: activeDialog
                });
              });
            }

            // Checking if has new messages.
            // If yes emiting new-message event.
            chat.messageManager.getNewMessagesCountPromise(userInfo.userId).then(messages => {
              if (messages && messages.length) {
                let userIds = messages.map(msg => msg.userId);
  
                chat.userManager.getUsersPromise(userIds, true).then(usersObj => {
                  messages = messages.map(msg => {
  
                    if (usersObj[msg.userId]) {
                      return msg.senderInfo = usersObj[msg.userId].info;
                    }
                  });
  
                  socket.emit('new-messages', messages);
                });
              }
            });
          });

        } else {
          socket.disconnect('status:not-available');
        }
      });
    });

    socket.on('message-sent', messageData => {
      if (!socket.userInfo) return false;

      let blockedUsers = chat.settingsManager.getBlockedUsers(messageData.userId);

      if (blockedUsers && blockedUsers.includes(socket.userInfo.userId)) {
        socket.emit('user-blocked-you', {
          dialogId: messageData.userId
        });

        return false;
      }

      chat.userManager.getUserPromise(messageData.userId, true).then(user => {

        if (user) {
          let message = chat.messageManager.clearMessage(messageData.message);

          if (message.length > 0) {
            chat.messageManager.checkMessagePromise(message).then(response => {
              if (response.length) {
                socket.emit('bl-word', {
                  dialogId: messageData.userId,
                  word: res.join(', ')
                });
              } else {

                chat.messageManager.storeMessagePromise(socket.userInfo.userId, user.info.userId, message).then(result => {
                  if (!result) return false;
                });

                let sockets = user.sockets;


                let newMessageData = {
                  body: message,
                  userId: socket.userInfo.userId,
                  date: new Date().getTime()
                }

                sockets.forEach(socketId => {
                  chatIo.to(socketId).emit('new-message', {
                    message: newMessageData,
                    senderData: socket.userInfo
                  });
                });
              }
            });
          }
        }
      });
    });

    socket.on('dialog-opened', data => {
      if (!socket.userInfo) return false;

      let date = nullifyTime(new Date());

      chat.messageManager.getConversationPromise(socket.userInfo.userId, data.userId, date).then(messages => {
        if (messages.length) {
          let senderInfo = chat.userManager.getUserPromise(data.userId, true).then(user => {
            socket.emit('message-history', {
              messages: messages,
              senderInfo: user.info
            });
          });
        }
      });

      chat.settingsManager.addOpenDialog(socket.userInfo.userId, data.userId);
    });

    socket.on('dialog-closed', data => {

    if (!socket.userInfo) return false;
      chat.settingsManager.removeOpenDialog(socket.userInfo.userId, data.userId);
    });

    socket.on('dialog-activated', data => {

      if (!socket.userInfo) return false;
      chat.settingsManager.setActiveDialog(socket.userInfo.userId, data.userId);
    });

    socket.on('dialog-deactivated', data => {
      if (!socket.userInfo) return false;
      chat.settingsManager.removeActiveDialog(socket.userInfo.userId);
    });

    socket.on('availability-changed', $availability => {
      if (!socket.userInfo) return false;

      if ($availability) {
        chat.settingsManager.setAvailability(socket.userInfo.userId, 1);
      } else {
        chat.settingsManager.setAvailability(socket.userInfo.userId, 0);

        chat.userManager.getUserPromise(socket.userInfo.userId).then(user => {
          if (user) {
          	if (Array.isArray(user.sockets)) {
	            user.sockets.forEach(socketId => {
	              chatIo.to(socketId).emit('chat-off', {});
	              chatIo.connected[socketId].disconnect('chat disabled');
	            });
          	}
          }
        });
      }
    });

    socket.on('change-options', data => {
      if (!socket.userInfo) return false;

      switch (data.option) {
        case 'show-only-escorts':

          chat.settingsManager.setShowOnlyEscorts(socket.userInfo.userId, data.value);
          break;
        case 'keep-list-open':
          chat.settingsManager.setKeepListOpen(socket.userInfo.userId, data.value);
          break;
        case 'invisibility':
          chat.settingsManager.setInvisibility(socket.userInfo.userId, data.value);

          if (data.value) {
            //If set invisibility = 1 emit users about his status and remove from public list
            socket.broadcast.emit('offline-users', [socket.userInfo.userId]);
            chat.userManager.removeUserFromPublicList(socket.userInfo.userId);
          } else {
            socket.broadcast.emit('new-user', socket.userInfo);
            chat.userManager.addUserToPublicList(socket.userInfo);
          }

          break;
      }
    });

    socket.on('block-user', data => {
      if (!socket.userInfo) return false;

      chat.settingsManager.addBlockedUser(socket.userInfo.userId, data.userId);
    });

    socket.on('unblock-user', data => {
      if (!socket.userInfo) return false;

      chat.settingsManager.removeBlockedUser(socket.userInfo.userId, data.userId);

      //If unblocked user is online emit 'online-user'
      chat.userManager.getUserPromise(data.userId).then(user => {
        if (user) {
          socket.emit('new-user', user.info);
        }
      });
    });

    socket.on('blocked-users', data => {
      if (!socket.userInfo) return false;

      let blockedUsers = chat.settingsManager.getBlockedUsers(socket.userInfo.userId);

      chat.userManager.getUsersPromise(blockedUsers, true).then(users => {

        let _users = Object.keys(users).map(userId => {
          return users[userId].info;
        });

          socket.emit('blocked-users', _users);
        });
    });

    socket.on('conversation-read', data => {
      if (!socket.userInfo) return false;

      chat.messageManager.markAsReadPromise(socket.userInfo.userId, data.userId).then(result => result);
    });

    socket.on('typing-start', data => {
      if (!socket.userInfo) return false;

      chat.userManager.getUserPromise(data.userId).then(user => {
        if (user) {
          user.sockets.forEach(socketId => {
            chatIo.to(socketId).emit('typing-start', {
              userId: socket.userInfo.userId
            });
          });
        }
      });
    });

    socket.on('typing-end', data => {
      if (!socket.userInfo) return false;

      chat.userManager.getUserPromise(data.userId).then(user => {
        if (user) {
          user.sockets.forEach(socketId => {
            chatIo.to(socketId).emit('typing-end', {
              userId: socket.userInfo.userId
            });
          });
        }
      })
    });

    socket.on('chat-with', data => {
      chat.userManager.getUserPromise(data.userId, true).then(user => {
        socket.emit('open-dialogs', {
          dialogs: [{
            userId: data.userId,
            userInfo: user.info
          }],
          activeDialog: data.userId
        });
      });
    });

    socket.on('disconnect', () => {
      if (!socket.userInfo) return;
      chat.userManager.removeSocket(socket.userInfo.userId, socket.id);
    });
  });
};

const startInstantBook = () => {
  instantBookIo.on('connection', (socket) => {
    const userType = socket.handshake.query.uType;
    const escortId = socket.handshake.query.eId;
    const token = socket.handshake.query.t;

    if (userType === 'escort') {
      instantBook.userManager.addUser(userType, escortId, socket.id);
    } else {
      instantBook.userManager.addUser(userType, token, socket.id);
    }

    socket.on('get_availability', data => {
      instantBook.userManager.getAvailableEscortsPromise(false).then(escortsObj => {
        let escortData = escortsObj[data.escortId];
        if (escortData) {
          socket.emit('escort_availability', escortData);
        } else {
          socket.emit('escort_availability', false);
        }
      });
    });

    socket.on('available_for_booking', data => {

      let notificationHash = crypto.createHash('md5').update(`${new Date().toString()}instantSault`).digest('hex');

      var availabilityData = {
        escort_id: data.escortId,
        start_date: toMySQLFormat(new Date()),
        end_date: toMySQLFormat(new Date(Date.now() + data.duration * 60 * 60 * 1000)),
        incall: data.location === 'incall' ? 1 : 0,
        outcall: data.location === 'outcall' ? 1 : 0,
        notification_hash: notificationHash
      };

      instantBook.userManager.updateUserAutoSigninHash(data.escortId, notificationHash).then(result => {
        if (!result.status || result.status != 'success') return false;
        instantBook.userManager.addAvailableEscortPromise(availabilityData).then(result => {
          if (result.status === 'success') {
            availabilityData.start_date = new Date().toISOString();
            availabilityData.end_date = new Date(Date.now() + data.duration * 60 * 60 * 1000).toISOString();
            socket.emit('available_for_booking_success', availabilityData);
          } else {
            socket.emit('available_for_booking_error');
          }
        }).catch(error => console.log(error));

      });
    });

    socket.on('available_for_booking_update', data => {
      var availabilityData = {
        escort_id: data.escortId,
        start_date: toMySQLFormat(new Date()),
        end_date: toMySQLFormat(new Date(Date.now() + data.duration * 60 * 60 * 1000)),
        incall: data.location === 'incall' ? 1 : 0,
        outcall: data.location === 'outcall' ? 1 : 0
      };

      instantBook.userManager.updateAvailableEscortPromise(availabilityData).then(result => {
        if (result.status === 'success') {
          availabilityData.start_date = new Date().toISOString();
          availabilityData.end_date = new Date(Date.now() + data.duration * 60 * 60 * 1000).toISOString();
          socket.emit('available_for_booking_success', availabilityData);
        } else {
          socket.emit('available_for_booking_error');
        }
      }).catch(error => console.log(error));
    });

    socket.on('no_longer_available_for_booking', data => {
      instantBook.userManager.deleteAvailableEscortPromise(data).then(result => {
        if (result.status === 'success') {
          socket.emit('no_longer_available_for_booking_success');
        } else {
          socket.emit('available_for_booking_error');
        }
      }).catch(error => console.log(error));
    });

    socket.on('request_booking', data => {
      data.hash = socket.id;

      instantBook.bookingManager.addBookingRequestPromise(data).then(result => {
        if (result.status === 'success') {
          data.id = result.insertId;
          instantBook.userManager.getUserList('escort')[data.escort_id].sockets.forEach(sId => {
            instantBookIo.to(sId).emit('new_booking', data);
          });

          instantBook.userManager.getAvailableEscortsPromise().then(escortsObj => {
            instantBook.request.post(config.pushNotifications.url)
              .set('Content-Type', 'application/json')
              .auth(config.pushNotifications.auth)
              .send({
                alert: 'New instant Booking request',
                url: `https://www.escortforumit.xxx/private/auto-signin?signin_hash=${escortsObj[data.escort_id].notification_hash}`,
                aliases:[data.escort_id.toString()]
              })
              .then(resp => {
                if (resp.success) {
                  console.log(`push notification sent to ${data.escort_id}`);
                }
              }).catch(err => console.log(err));

          })

          socket.emit('instant_request_success', { status: 'success', count: data.wait_time });
        } else {
          socket.emit('request_booking_error', result.msg);
        }
      }).catch((error) => {
        console.log(error);
      });
    });

    socket.on('booking_request_reply', data => {
      instantBookIo.to(data.token).emit("request_reply_guest", {
        status: data.status
      });

      instantBook.bookingManager.updateBookingRequestStatusPromise(data).then((data) => {

      }).catch((err) => {
        console.log(err);
      });

      instantBook.userManager.getAvailableEscortsPromise(false).then(data => {

      })
    });

    // remove user from the list if disconnected
    socket.on('disconnect', () => {
      instantBook.userManager.removeUser(userType, socket.id);
    });

  });
};

module.exports = {
  startChat: startChat,
  startInstantBook: startInstantBook
};