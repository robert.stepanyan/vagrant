/*jslint es6*/
/*jslint node*/

process.on('warning', (warning) => {
  console.log(warning.name);
  console.log(warning.message);
  console.log(warning.stack);
});

const socketServer = require('./socketServer');

socketServer.startChat();
socketServer.startInstantBook();
