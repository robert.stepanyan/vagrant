/*jslint es6*/
/*jslint node*/

'use strict';

const MySQL = require('mysql');
const { toMySQLFormat, toMySQLFormatWithTZ } = require('../utils');
const config = require('../configs').get('production');

class Users {
  constructor() {

    this.availableEscortsList = {};
    this.guests = {};
    this.escorts = {};


    /* MySQL connection */

    this.MySQLClient = null;

    this.establishMySQLConnection();

    setInterval(() => {
      this.MySQLClient.ping(err => {
        if (err) {
          this.handleMySQLConnectionerr(err);
        }
      });
    }, 5000);

    /* Available Escorts Update */

    setInterval(() => {
      this.initAvailableEscortsUpdatePromise();
      this.initDenyTimeoutRequestsPromise();
    }, 10 * 1000);
  }

  /* MySQL Client */

  establishMySQLConnection() {
    this.MySQLClient = MySQL.createConnection({
      user: config.db.user,
      database: config.db.database,
      password: config.db.password,
      host: config.db.host,
      port: 3306,
      timezone: 'utc'
    });

    this.MySQLClient.connect(err => {});
  }

  handleMySQLConnectionerr(err) {
    if (!err.fatal) {
      return false;
    }

    if (err.code !== 'PROTOCOL_CONNECTION_LOST') {
      console.log(
        'The mysql library fired a PROTOCOL_CONNECTION_LOST exception'
      );

      throw err;
    }

    console.log('Re-connecting lost connection: ' + err.stack);
    this.establishMySQLConnection();
  }


  /* Avaliable Escorts */

  initAvailableEscortsUpdatePromise() {
    this.getAvailableEscortsPromise(true).then(data => {
      if (Array.isArray(data)) {
        let escortObj = {};
        data.forEach(escortData => {
          escortObj[escortData.escort_id] = escortData;
        });

        this.availableEscortsList = escortObj;
      }
    });

    this.removeInavailableEscortsPromise().then(() => {});
  }

  initDenyTimeoutRequestsPromise() {
    return new Promise((resolve, reject) => {
      let _date = toMySQLFormat(new Date());
      let _query = `
      UPDATE instant_book_requests SET status = "deny"
      WHERE request_date < DATE_SUB("${_date}", INTERVAL wait_time MINUTE) AND status = "pending"
      `;
      this.MySQLClient.query(_query, (err, result) => {
        if (err) return reject(err);
        return resolve(result);
      });
    });
  }

  getAvailableEscortsPromise(fromDb = false) {
    return new Promise((resolve, reject) => {
      if (!fromDb) {
        return resolve(this.availableEscortsList);
      }

      let _date = toMySQLFormat(new Date());
      let _query = `
        SELECT escort_id, start_date, end_date, incall, outcall, notification_hash
        FROM  instant_book_escorts
        WHERE start_date < "${_date}" AND end_date > "${_date}"
      `
      this.MySQLClient.query(_query, (err, result) => {
        if (err) return reject(err);
        return resolve(result);
      })
    });
  }

  removeInavailableEscortsPromise() {
    return new Promise((resolve, reject) => {

      let _date = toMySQLFormat(new Date());
      let _query = `
        DELETE FROM instant_book_escorts
        WHERE end_date < "${_date}"
      `
      this.MySQLClient.query(_query, (err, result) => {
        if (err) reject(err);

        return resolve(result);
      })
    });
  }

  addAvailableEscortPromise(data) {
    return new Promise((resolve, reject) => {
      let _query = `INSERT INTO instant_book_escorts SET ?`

      this.MySQLClient.query(_query, data, (err, result) => {
        if (err) return reject(err);

          return resolve({status: 'success'});
      });
    });
  }

  updateUserAutoSigninHash(escortId, hash) {
    return new Promise((resolve, reject) => {
      let _query = `UPDATE users AS u 
        INNER JOIN escorts e ON e.user_id = u.id
        SET signin_hash = "${hash}"
        WHERE e.id = "${escortId}"
      `;

      this.MySQLClient.query(_query, (err, result) => {
        if (err) return reject(err);

          return resolve({status: 'success'});
      });
    });
  }

  updateAvailableEscortPromise(data) {
    return new Promise((resolve, reject) => {
      let _query = `UPDATE instant_book_escorts SET ? WHERE escort_id = ${data.escort_id}`;
      this.MySQLClient.query(_query, data, (err, result) => {
        if (err) return reject(err);

        return resolve({ status: 'success' });
      });
    });
  }

  deleteAvailableEscortPromise(data) {
    return new Promise((resolve, reject) => {
      let _query = `DELETE FROM instant_book_escorts WHERE escort_id = ${data.escortId}`;
      this.MySQLClient.query(_query, (err, result) => {
        if (err) return reject(err);

        return resolve({status: 'success'});
      });
    });
  }

  addUser(userType, id, socketId) {
    switch (userType) {
      case "guest":
      if (!this.guests[id]) {
        this.guests[id] = {};
        this.guests[id].sockets = [];
      }

      this.guests[id].sockets.push(socketId);

      break;

    case "escort":
      if (!this.escorts[id]) {
        this.escorts[id] = {};
        this.escorts[id].sockets = [];
      }

      this.escorts[id].sockets.push(socketId);

      break;
    }
  }

  removeUser(userType, socketId) {
    switch (userType) {
      case "guest":
        Object.keys(this.guests).forEach(key => {
          this.guests[key].sockets = this.guests[key].sockets.filter(
            sId => {
              return sId != socketId;
            }
          );
        });

        break;
      case "escort":
        Object.keys(this.escorts).forEach(key => {
          this.escorts[key].sockets = this.escorts[key].sockets.filter(
            sId => {
              return sId != socketId;
            }
          );
        });
        break;
    }
  }

  getUserList(userType) {
    switch (userType) {
      case 'guest':
        return this.guests;
      break;
      case 'escort':
        return this.escorts;
      break;
    }
  }
}

module.exports = new Users();