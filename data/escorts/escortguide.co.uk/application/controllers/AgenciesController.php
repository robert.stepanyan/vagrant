<?php

class AgenciesController extends Zend_Controller_Action
{
	public static $linkHelper;
	
	public function init()
	{
		self::$linkHelper = $this->view->getHelper('GetLink');
	}
	
	public function indexAction()
	{
		
	}

	public function listAction()
	{
		$this->view->layout()->setLayout('agency-list');

		if ( $this->_request->ajax ) {
			$this->view->layout()->disableLayout();			
		}

		$cache = Zend_Registry::get('cache');		

		$per_page = 20;		
		$default_sorting = "alpha";
		$current_sorting = $this->_getParam('agency_sort', $default_sorting);

		$filter_text = $this->view->text = trim($this->_request->text);
		$filter_name = $this->view->name = trim($this->_request->name);
		$filter_city_slug = $this->view->city_slug = trim($this->_request->city_slug);


		$filter = array(
			'text' => $filter_text,
			'name' => $filter_name,
			'city_slug' => $filter_city_slug
		);

		$sort_map = array(
			'newest' => 'date_registered DESC',
			'last_modified' => 'cd.last_modified DESC',
			'last-connection' => 'refresh_date DESC',			
			'most-viewed' => 'hit_count DESC',
			'random' => 'RAND()',			
			'by-city' => 'ct.title_en ASC',			
			'alpha' => 'cd.club_name ASC',			
		);

		if ( !array_key_exists($current_sorting, $sort_map) ) {
			$current_sorting = $default_sorting;
			$this->_request->setParam('agency_sort', $default_sorting);
		}

		if ( $current_sorting != $_COOKIE['agency_sort'] && $this->_request->s ) {
			setcookie("a_sorting", $current_sorting, strtotime('+1 year'), "/", "www." . preg_replace('#www.|dev.#', '', $_SERVER['HTTP_HOST']));
			setcookie("a_sortingMap", $sort_map[$current_sorting]['map'], strtotime('+1 year'), "/", "www." . preg_replace('#www.|dev.#', '', $_SERVER['HTTP_HOST']));
			$_COOKIE['a_sorting'] = $current_sorting;
			$_COOKIE['a_sortingMap'] = $sort_map[$current_sorting]['map'];
		}

		if ( isset($_COOKIE['a_sorting']) /*&& ! $this->_request->isPost()*/ ) {
			$current_sorting = $_COOKIE['a_sorting'];
			$this->_request->setParam('agency_sort', $current_sorting);
		}

		if ( $current_sorting ) {
			$tmp = $sort_map[$current_sorting];
			unset($sort_map[$current_sorting]);
			$sort_map = array_merge(array( $current_sorting => $tmp), $sort_map);
		}

		if ( $current_sorting ) {
			$sorting = $current_sorting;
		}

		$this->view->sort_map = $sort_map;
		$this->view->current_sort = $current_sorting;

		$p_top_category = $this->view->p_top_category = $this->_request->getParam('p_top_category', 'agencies');			

		$page = intval($this->_request->page);
		if ( $page == 0 ) {
			$page = 1;
		}

		$m_agencies = new Model_Agencies();

		$count = 0;
		$agencies = $m_agencies->getAll($filter, $page, $per_page, $sorting, $count);

		$this->view->count = $count;
		$this->view->page = $page;
		$this->view->perPage = $per_page;
		$this->view->agencies = $agencies;
	}

	public function showToolTipAction()
	{
		$this->view->layout()->disableLayout();

		$agency_id = (int) $this->_request->agency_id;
		$type = $this->_request->type;
		$types = array('ec', 'vec');

		if ( ! in_array($type, $types) ) die('haha');

		$m_agencies = new Model_Agencies();
		$this->view->data = $m_agencies->getCountsById($agency_id);
		$this->view->type = $type;		
	}
	
	public function showAction()
	{
		if ( ! is_null($this->_getParam('ajax')) ) {
			$this->view->layout()->disableLayout();
			$this->view->ajax = true;
		}
		else {
			$this->view->layout()->setLayout('main-simple');
		}

		$agency_slug = $this->_getParam('agencyName');
		$agency_id = intval($this->_getParam('agency_id'));

		/**************************************************/
		/*$client = new Cubix_Api_XmlRpc_Client();
		$agency = $client->call('Agencies.getInfo', array($agency_slug, $agency_id));

		if ( ! $agency ) {
			// 404
			die;
		}
		elseif ( isset($agency['error']) ) {
			print_r($agency['error']); die;
		}

		$agency = $this->view->agency = new Model_AgencyItem($agency);*/
		/**************************************************/
		$m = new Model_Agencies();
		$agency = $m->getBySlugId($agency_slug, $agency_id);

		if ( ! $agency ) {
			die;
		}

		$agency = $this->view->agency = new Model_AgencyItem($agency);
		/**************************************************/

		/* --> Extract page from `req` parameter */
		$req = ltrim($this->_getParam('req'), '/');
		$req = explode('_', $req);

		$page = 1;
		$param_name = $req[0];

		if ( $param_name == 'page' ) {
			$page = $req[1];
		}

		if ( $page < 1 ) $page = 1;
		/* <-- */
		
		$config = Zend_Registry::get('escorts_config');
		
		$filter = array('e.agency_id = ?' => $agency->id);
		
		$count = 0;
		$escorts = Model_Escort_List::getFiltered($filter, 'last-modified', $page, $config['perPage'], $count);
		
		if ( isset($escorts[0]) && $escorts[0] ) {
			$this->view->agency_last_mod_date = $escorts[0]->date_last_modified;
		}
		
		$this->view->escorts = $escorts;
		$this->view->count = $count;
		$this->view->params = array('page' => $page);
		$this->view->escort_ids = array();
		$this->view->no_paging = true;
	}	
}
