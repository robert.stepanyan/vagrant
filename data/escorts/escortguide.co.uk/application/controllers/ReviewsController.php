<?php

class ReviewsController extends Zend_Controller_Action
{
	public function init()
	{
		$this->view->layout()->setLayout('main-simple');
	}

	public function indexAction()
	{
		$lng = Cubix_I18n::getLang();
		
		$request = $this->_request;
		if ( $request->ajax ) {
			$this->view->layout()->disableLayout();
			$this->view->ajax = true;
		}
		$filter = array();

		if (isset($request->city_id) && $request->city_id )
		{
			$filter['city_id'] = intval($request->city_id);
			$this->view->city_id = intval($request->city_id);
		}

		if (isset($request->showname) && $request->showname )
		{
			$filter['showname'] = $request->showname;
			$this->view->showname = $request->showname;
		}

		if (isset($request->member_name) && $request->member_name )
		{
			$filter['member_name'] = $request->member_name;
			$this->view->member_name = $request->member_name;
		}

		if (isset($request->gender) && $request->gender)
		{
			$filter['gender'] = $request->gender;
			$this->view->gender = $request->gender;
		}

		switch ($request->ord_field)
		{
			case 'city_title':
				$ord_field_v = 'city_title';
				$ord_field = 'c.title_' . $lng;
				break;
			case 'showname':
				$ord_field_v = 'showname';
				$ord_field = 'r.showname';
				break;
			case 'agency_name':
				$ord_field_v = 'agency_name';
				$ord_field = 'r.agency_name';
				break;
			case 'looks':
				$ord_field_v = 'looks';
				$ord_field = 'r.looks_rating';
				break;
			case 'services':
				$ord_field_v = 'services';
				$ord_field = 'r.services_rating';
				break;
			case 'member':
				$ord_field_v = 'member';
				$ord_field = 'r.username';
				break;
			case 'date':
			default:
				$ord_field_v = 'creation_date';
				$ord_field = 'r.creation_date';
				break;
		}		
		
		switch ($request->ord_dir)
		{
			case 'asc':
				$ord_dir_v = 'asc';
				$ord_dir = 'ASC';
				break;
			case 'desc':
			default:
				$ord_dir_v = 'desc';
				$ord_dir = 'DESC';
				break;
		}		

		if (count($filter) > 0)
			$arg_filter = $filter;
		else
			$arg_filter = '-999';

		$config = Zend_Registry::get('reviews_config');

		if (isset($request->page) && intval($request->page) > 0)
		{
			$page = intval($request->page);
		}
		else
			$page = 1;

		is_array($arg_filter) ? $filter_str = implode('.', $arg_filter) : $filter_str = $arg_filter;
		$cache = Zend_Registry::get('cache');
		$cache_key = 'v2_reviews_list_' . $lng . '_page_' . $page . '_perpage_' . $config['perPage'] . '_filter_' . $filter_str . '_ordfield_' . $ord_field . '_orddir_' . $ord_dir;
		$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

		/*if ( ! $ret = $cache->load($cache_key) )
		{*/
			$ret_revs = Model_Reviews::getReviews($page, $config['perPage'], $arg_filter, $ord_field, $ord_dir);

			$cities = Model_Reviews::getReviewsCities();

			foreach ( $ret_revs[0] as $i => $rev ) {
				$photo_url = new Model_Escort_PhotoItem($rev);
				$ret_revs[0][$i]['photo_url'] = $photo_url->getUrl('agency_p100');
			}

			$ret = array($ret_revs, $cities);

			/*$cache->save($ret, $cache_key, array(), $config['cacheTime']);
		}*/

		list($ret_revs, $cities) = $ret;
		list($items, $count) = $ret_revs;
		$this->view->from_members_info = isset ($request->from_members_info) ? 1 : 0;
		$this->view->items = $items;
		$this->view->count = $count;
		$this->view->cities = $cities;
		$this->view->page = $page;

		$this->view->ord_field = $ord_field_v;
		$this->view->ord_dir = $ord_dir_v;
	}

	public function ajaxEscortsSearchAction()
	{
		$this->_request->setParam('no_tidy', true);

		$name = trim($this->_getParam('a_showname'));

		if ( ! strlen($name) ) {
			die(json_encode(array()));
		}

		$cache = Zend_Registry::get('cache');
		
		$cache_key = 'ajax_escort_search_' . $name;
		$cache_key = preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

		if ( ! $escorts = $cache->load($cache_key) ) {
			//$escorts = Cubix_Api::getInstance()->call('getEscortsSearch', array($name));
			$escorts = Model_Reviews::getEscortsSearch($name);
			$cache->save($escorts, $cache_key, array());
		}
		echo json_encode($escorts);
		die;
	}

	public function ajaxMembersSearchAction()
	{
		$this->_request->setParam('no_tidy', true);

		$name = trim($this->_getParam('a_member'));

		if ( ! strlen($name) ) {
			die(json_encode(array()));
		}

		$cache = Zend_Registry::get('cache');

		$cache_key = 'ajax_member_search_' . $name;
		$cache_key = preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

		if ( ! $members = $cache->load($cache_key) ) {
			$members = Cubix_Api::getInstance()->call('getMembersSearch', array($name));
			$cache->save($members, $cache_key, array());
		}
		echo json_encode($members);
		die;
	}

	public function ajaxAgenciesSearchAction()
	{
		$this->_request->setParam('no_tidy', true);

		$name = trim($this->_getParam('a_name'));

		if ( ! strlen($name) ) {
			die(json_encode(array()));
		}

		$cache = Zend_Registry::get('cache');

		$cache_key = 'ajax_agencies_search_' . $name;
		$cache_key = preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

		if ( ! $agencies = $cache->load($cache_key) ) {
			$agencies = Cubix_Api::getInstance()->call('getAgenciesSearch', array($name));
			$cache->save($agencies, $cache_key, array());
		}
		echo json_encode($agencies);
		die;
	}

	public function escortsAction()
	{
		$lng = Cubix_I18n::getLang();

		$request = $this->_request;
		if ( $request->ajax ) {
			$this->view->layout()->disableLayout();
			$this->view->ajax = true;
		}
		$filter = array();

		if (isset($request->showname) && $request->showname )
		{
			$filter['showname'] = $request->showname;
			$this->view->showname = $request->showname;
		}

		if (isset($request->country_id) && $request->country_id )
		{
			$filter['country_id'] = intval($request->country_id);
			$this->view->country_id = intval($request->country_id);
		}

		switch ($request->ord_field)
		{
			case 'showname':
			default:
				$ord_field_v = 'showname';
				$ord_field = 'r.showname';
				break;
		}

		switch ($request->ord_dir)
		{
			case 'desc':
				$ord_dir_v = 'desc';
				$ord_dir = 'DESC';
				break;
			case 'asc':
			default:
				$ord_dir_v = 'asc';
				$ord_dir = 'ASC';
				break;
		}

		if (count($filter) > 0)
			$arg_filter = $filter;
		else
			$arg_filter = '-999';

		$config = Zend_Registry::get('reviews_config');

		if (isset($request->page) && intval($request->page) > 0)
		{
			$page = intval($request->page);
		}
		else
			$page = 1;

		is_array($arg_filter) ? $filter_str = implode('.', $arg_filter) : $filter_str = $arg_filter;
		$cache = Zend_Registry::get('cache');
		$cache_key = 'v2_reviews_escorts_' . $lng . '_page_' . $page . '_perpage_' . $config['perPage'] . '_filter_' . $filter_str . '_ordfield_' . $ord_field . '_orddir_' . $ord_dir;
		$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

		/*if ( ! $ret = $cache->load($cache_key) )
		{*/
			//$ret = Cubix_Api::getInstance()->call('getEscortsReviews', array($page, $config['perPage'], $arg_filter, $ord_field, $ord_dir, $lng));
			$ret = Model_Reviews::getEscortsReviews($page, $config['perPage'], $arg_filter, $ord_field, $ord_dir);

			/*$cache->save($ret, $cache_key, array(), $config['cacheTime']);
		}*/

		list($items, $count) = $ret;

		$this->view->items = $items;
		$this->view->count = $count;
		$this->view->page = $page;

		$this->view->ord_field = $ord_field_v;
		$this->view->ord_dir = $ord_dir_v;
	}

	public function agenciesAction()
	{
		$lng = Cubix_I18n::getLang();

		$request = $this->_request;
		if ( $request->ajax ) {
			$this->view->layout()->disableLayout();
			$this->view->ajax = true;
		}
		$filter = array();

		if (isset($request->name) && $request->name)
		{
			$filter['name'] = $request->name;
			$this->view->name = $request->name;
		}

		if (isset($request->country_id) && $request->country_id )
		{
			$filter['country_id'] = intval($request->country_id);
			$this->view->country_id = intval($request->country_id);
		}

		switch ($request->ord_field)
		{
			case 'name':
			default:
				$ord_field_v = 'name';
				$ord_field = 'a.name';
				break;
		}

		switch ($request->ord_dir)
		{
			case 'desc':
				$ord_dir_v = 'desc';
				$ord_dir = 'DESC';
				break;
			case 'asc':
			default:
				$ord_dir_v = 'asc';
				$ord_dir = 'ASC';
				break;
		}

		if (count($filter) > 0)
			$arg_filter = $filter;
		else
			$arg_filter = '-999';

		$config = Zend_Registry::get('reviews_config');

		if (isset($request->page) && intval($request->page) > 0)
		{
			$page = intval($request->page);
		}
		else
			$page = 1;

		is_array($arg_filter) ? $filter_str = implode('.', $arg_filter) : $filter_str = $arg_filter;
		$cache = Zend_Registry::get('cache');
		$cache_key = 'v2_reviews_agencies_' . $lng . '_page_' . $page . '_perpage_' . $config['perPage'] . '_filter_' . $filter_str . '_ordfield_' . $ord_field . '_orddir_' . $ord_dir;
		$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

		/**/

		$params = array(
			'sort' => 'random',
			'group' => 'e.agency_id',
			'filter' => array(

			),
			'limit' => array(
				'page' => 1,
				'perPage' => 99999
			)
		);

		$model = new Model_EscortsV2();

		$escorts_count = 0;
		$escorts = $model->getAll($params, $escorts_count);

		$ag = array();
		foreach ($escorts as $item)
			$ag[] = $item->agency_id;
		
		foreach ($escorts as $item)
			$ag2[$item->agency_id] = $item->count;

		$this->view->ags = $ag2;
		$ag_str = implode(',', $ag);

		//$ret = Cubix_Api::getInstance()->call('getAgencies', array($page, $config['perPage'], $ag_str, $arg_filter, $ord_field, $ord_dir, $lng));
		
		/**/

		if ( ! $ret = $cache->load($cache_key) )
		{
			$ret = Cubix_Api::getInstance()->call('getAgencies', array($page, $config['perPage'], $ag_str, $arg_filter, $ord_field, $ord_dir, $lng));

			$cache->save($ret, $cache_key, array(), $config['cacheTime']);
		}

		list($items, $count) = $ret;

		$this->view->items = $items;
		$this->view->count = $count;
		$this->view->page = $page;

		$this->view->ord_field = $ord_field_v;
		$this->view->ord_dir = $ord_dir_v;
	}

	public function escortAction()
	{
		$lng = Cubix_I18n::getLang();

		$request = $this->_request;
		$escortName = $request->escortName;
		$escort_id = $request->escort_id;

		if ( $request->ajax ) {
			$this->view->layout()->disableLayout();
			$this->view->ajax = true;
		}

		$config = Zend_Registry::get('reviews_config');

		$modelR = new Model_Reviews();
		$escort = $modelR->getEscortInfo($escort_id);
		$escort = new Model_EscortV2Item($escort);
		
		$model = new Model_EscortsV2();
		$add_esc_data = $model->getRevComById($escort_id);
		
		$escort->disabled_reviews = $add_esc_data->disabled_reviews;
		$escort->disabled_comments = $add_esc_data->disabled_comments;
		
		$this->view->escort = $escort;
		
		if (isset($request->page) && intval($request->page) > 0)
			$page = intval($request->page);
		else
			$page = 1;

		$perpage = 2;
		
		if (isset($request->perpage))
		{
			if ($request->perpage == 'all')
				$perpage = 99999;
			elseif (intval($request->perpage) > 0)
				$perpage = intval($request->perpage);
		}

		$cache = Zend_Registry::get('cache');
		$cache_key = 'v2_reviews_escort_' . $lng . '_page_' . $page . '_perpage_' . $perpage . '_escortName_' . $escortName . '_escort_id_' . $escort_id;
		$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

		if ( ! $store = $cache->load($cache_key) )
		{
			//$topLadies = Cubix_Api::getInstance()->call('getTopLadies', array($config['topLadiesCount'], $lng));
			$topLadies = Model_Reviews::getTopLadies($config['topLadiesCount']);
			//$ret = Cubix_Api::getInstance()->call('getEscortReviews', array($escortName, $page, $perpage, $lng));
			//$ret = Model_Reviews::getEscortReviews($escortName, $page, $perpage);
			$ret = Model_Reviews::getEscortReviews($escort_id, $page, $perpage);

			$store = array($ret, $topLadies);

			$cache->save($store, $cache_key, array(), $config['cacheTime']);
		}

		list($ret, $topLadies) = $store;
		list($items, $count) = $ret;

		$topEsc = array();
		$topEscK = array();
		foreach ($topLadies as $k => $escort2)
		{
			$topEsc[$k] = $escort2['escort_id'];
			$topEscK[$escort2['escort_id']] = $k;
		}
		
		//$escort_id = $items[0]['escort_id'];
		
		if (in_array($escort_id, $topEsc))
			$this->view->inTop30 = $topEscK[$escort_id] + 1;
		
		$this->view->items = $items;
		$this->view->count = $count;
		$this->view->page = $page;
		if ($perpage == 99999) $perpage = 'all';
		$this->view->perpage = $perpage;

		if (($escort->status & ESCORT_STATUS_DELETED) || ($escort->status & ESCORT_STATUS_OWNER_DISABLED) || ($escort->status & ESCORT_STATUS_ADMIN_DISABLED))
		{
			$this->view->disable = true;
		}
	}

	public function memberAction()
	{
		$lng = Cubix_I18n::getLang();

		$request = $this->_request;
		$filter = array();

		if ( $request->ajax ) {
			$this->view->layout()->disableLayout();
			$this->view->ajax = true;
		}

		$filter['username'] = $this->view->username = $request->username;
		
		switch ($request->ord_field)
		{
			case 'city_title':
				$ord_field_v = 'city_title';
				$ord_field = 'c.title_' . $lng;
				break;
			case 'showname':
				$ord_field_v = 'showname';
				$ord_field = 'r.showname';
				break;
			case 'agency_name':
				$ord_field_v = 'agency_name';
				$ord_field = 'r.agency_name';
				break;
			case 'looks':
				$ord_field_v = 'looks';
				$ord_field = 'r.looks_rating';
				break;
			case 'services':
				$ord_field_v = 'services';
				$ord_field = 'r.services_rating';
				break;
			case 'member':
				$ord_field_v = 'member';
				$ord_field = 'r.username';
				break;
			case 'date':
			case 'member':
				$ord_field_v = 'fuckometer';
				$ord_field = 'r.fuckometer';
				break;
			case 'date':
			default:
				$ord_field_v = 'creation_date';
				$ord_field = 'r.creation_date';
				break;
		}

		switch ($request->ord_dir)
		{
			case 'asc':
				$ord_dir_v = 'asc';
				$ord_dir = 'ASC';
				break;
			case 'desc':
			default:
				$ord_dir_v = 'desc';
				$ord_dir = 'DESC';
				break;
		}

		if (count($filter) > 0)
			$arg_filter = $filter;
		else
			$arg_filter = '-999';

		$config = Zend_Registry::get('reviews_config');

		if (isset($request->page) && intval($request->page) > 0)
		{
			$page = intval($request->page);
		}
		else
			$page = 1;

		is_array($arg_filter) ? $filter_str = implode('.', $arg_filter) : $filter_str = $arg_filter;
		$cache = Zend_Registry::get('cache');
		$cache_key = 'v2_reviews_member_' . $lng . '_page_' . $page . '_perpage_' . $config['perPage'] . '_filter_' . $filter_str . '_ordfield_' . $ord_field . '_orddir_' . $ord_dir;
		$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

		/*if ( ! $ret = $cache->load($cache_key) )
		{*/
			//$ret = Cubix_Api::getInstance()->call('getReviews', array($page, $config['perPage'], $arg_filter, $ord_field, $ord_dir, $lng));
			$ret = Model_Reviews::getReviews($page, $config['perPage'], $arg_filter, $ord_field, $ord_dir);

			foreach ( $ret[0] as $i => $rev ) {
				$photo_url = new Model_Escort_PhotoItem($rev);
				$ret[0][$i]['photo_url'] = $photo_url->getUrl('agency_p100');
			}

			/*$cache->save($ret, $cache_key, array(), $config['cacheTime']);
		}*/

		list($items, $count) = $ret;

		$this->view->items = $items;
		$this->view->count = $count;
		$this->view->page = $page;

		$this->view->ord_field = $ord_field_v;
		$this->view->ord_dir = $ord_dir_v;
	}

	public function topReviewersAction()
	{
		$lng = Cubix_I18n::getLang();
		$config = Zend_Registry::get('reviews_config');
		$cache = Zend_Registry::get('cache');

		$cache_key = 'v2_reviews_top_reviewers_' . $lng . '_count_' . $config['topReviewersCount'];
		$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

		/*if ( ! $items = $cache->load($cache_key) )
		{*/
			//$items = Cubix_Api::getInstance()->call('getTopReviewers', array($config['topReviewersCount'], $lng));
			$items = Model_Reviews::getTopReviewers($config['topReviewersCount']);

			foreach ( $items as $i => $item ) {
				//$items[$i]['last_review'] = Cubix_Api::getInstance()->call('getLastReviewByMember', array($item['user_id']));
				$p_item = Model_Reviews::getLastReviewByMember($item['user_id']);
				$photo_url = new Model_Escort_PhotoItem($p_item);
				$items[$i]['last_review'] = $p_item;
				$items[$i]['last_review']['photo_url'] = $photo_url->getUrl('agency_p100');
			}

			/*$cache->save($items, $cache_key, array(), $config['cacheTime']);
		}*/

		$this->view->items = $items;
	}

	public function topLadiesAction()
	{
		$lng = Cubix_I18n::getLang();
		$config = Zend_Registry::get('reviews_config');
		$cache = Zend_Registry::get('cache');

		$cache_key = 'v2_reviews_top_ladies_' . $lng . '_count_' . $config['topLadiesCount'];
		$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

		/*if ( ! $items = $cache->load($cache_key) )
		{*/
			//$items = Cubix_Api::getInstance()->call('getTopLadies', array($config['topLadiesCount'], $lng));
			$items = Model_Reviews::getTopLadies($config['topLadiesCount']);
			
			foreach ( $items as $i => $item ) {
				$photo_url = new Model_Escort_PhotoItem($item);
				//$items[$i]['last_review'] = Cubix_Api::getInstance()->call('getLastReviewByEscort', array($item['escort_id']));
				$items[$i]['last_review'] = Model_Reviews::getLastReviewByEscort($item['escort_id']);
				$items[$i]['photo_url'] = $photo_url->getUrl('agency_p100');
			}

			/*$cache->save($items, $cache_key, array(), $config['cacheTime']);
		}*/

		$this->view->items = $items;
	}

	public function searchAction()
	{
		$lng = Cubix_I18n::getLang();
		$country_id = Cubix_Application::getById(Cubix_Application::getId())->country_id;
		$this->view->cities = Model_Reviews::getCities($country_id);

		$config = Zend_Registry::get('escorts_config');
		$request = $this->_request;

		if (isset($request->page) && intval($request->page) > 0)
		{
			$page = intval($request->page);
		}
		else
			$page = 1;

		if ($this->_request->search)
		{
			//list($items, $count) = Cubix_Api::getInstance()->call('reviewsSearch', array($this->getRequest()->getParams(), $page, $config['perPage'], $lng));
			list($items, $count) = Model_Reviews::reviewsSearch($this->getRequest()->getParams(), $page, $config['perPage']);
			$this->view->escorts = $items;
			
			$this->view->count = $count;
			$this->view->page = $page;
			$this->view->is_review = true;
			$this->view->no_paging = true;
		}
	}
}
