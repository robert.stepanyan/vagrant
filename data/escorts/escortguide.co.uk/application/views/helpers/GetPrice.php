<?php

class Zend_View_Helper_GetPrice extends Zend_View_Helper_Abstract
{
	public function getPrice($rates)
	{
		$rate = false;
		foreach ($rates[AVAILABLE_INCALL] as $r) {
			if($r->type != null){ continue; }
			if($r->availability == AVAILABLE_INCALL && $r->time_unit == TIME_HOURS && $r->time == 1){
				$rate = $r;
				break;
			}
		}
		if(!$rate){
			foreach ($rates[AVAILABLE_OUTCALL] as $r) {
				if($r->type != null){ continue; }
				if($r->availability == AVAILABLE_OUTCALL && $r->time_unit == TIME_HOURS && $r->time == 1){
					$rate = $r;
					break;
				}
			}
		}
		if(!$rate){
			foreach ($rates[AVAILABLE_INCALL] as $r) {
				if($r->type != null){ continue; }
				$rate = $r;
				break;
			}
		}
		if(!$rate){
			foreach ($rates[AVAILABLE_OUTCALL] as $r) {
				if($r->type != null){ continue; }
				$rate = $r;
				break;
			}
		}

		if($rate){
	        if(isset($_COOKIE['currency'])){
				$current_currency = $_COOKIE['currency'];
	        }else{
				$current_currency = "GBP";
	        }

			$currencies = Model_Currencies::getAllAssoc();


			$DEFINITIONS = Zend_Registry::get('defines');
			$time_units = $DEFINITIONS['time_unit_options'];
			$rateStr = '';
			if($rate->time_unit == TIME_MINUTES && $rate->time == 60){
				$rate->time_unit = TIME_HOURS;
				$rate->time = 1;
			}

			$price = '';

			if($currencies[$rate->currency_id] != $current_currency){
				$price = number_format($rate->price).' '.$current_currency;
			}else{
				$price = number_format($rate->price).' '.$rate->currency_title;	
			}

			$rateStr = $price.'/'.$rate->time.' '.substr($time_units[$rate->time_unit],0,1);
			return $rateStr;
		}

		return false;
	}
}
