var markersArray = new Array();
var LoadMap = function (map) {	
	
	var resp = $('map-escorts').get('html');
	
	//clear markers	
	if (markersArray.length) 
	{
		markersArray.each(function(row) {
			row.setMap(null);
		});

		markersArray = new Array();
	}

	var obj = JSON.decode(resp);
	
	var image_escort = new google.maps.MarkerImage('/img/cd/wg-m.png',
		// This marker is 20 pixels wide by 32 pixels tall.
		new google.maps.Size(21, 35),
		// The origin for this image is 0,0.
		new google.maps.Point(0,0),
		// The anchor for this image is the base of the flagpole at 0,32.
		new google.maps.Point(0, 35)
	);
	
	obj.each(function(e) {
		var lat = e.latitude;
		var lon = e.longitude;

		if (lat && lon) 
		{
			var name = "<div class='fleft' style='border: 1px solid #ccc;padding:1px;width:68px;'><a href='" + e.url + "'><img src='" + e.photo + "' width='68' height='91' /></a></div>";
			name += "<div class='fleft' style='margin-left: 7px; width: 170px;'><p style='margin: 0 0 3px 0;'><a style='font-size:14px;' href='" + e.url + "'>" + e.showname + "</a></p>";
			
			if (e.address.length)
				name += "<p>" + e.address + ((e.zip.length) ? ', ' + e.zip + ' ' : ', ') + ((e.city_title.length) ? e.city_title : '') + "</p></div>";
					   
			var marker = new google.maps.Marker({
				position: new google.maps.LatLng(lat, lon),
				map: map,
				icon: image_escort
			});
			
			marker.setMap(map);
			markersArray.push(marker);

			var infowindow = new google.maps.InfoWindow({
				content: name
			});

			google.maps.event.addListener(marker, 'click', function() {
				infowindow.open(map, marker);
			});
		}
	});
}

var InitMap = function(lat, lon) 
{	
	var lat_fix = 46.833333;
	var lon_fix = 8.333333;
	
	if (lat.length == 0 || lon.length == 0)
	{
		lat = lat_fix;
		lon = lon_fix;
	}
		
	var point = new google.maps.LatLng(lat, lon);
	
	var mapOptions = {
		center: point,
		zoom: 10,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	var map = new google.maps.Map($('map-wrap'), mapOptions);

	new google.maps.Marker({
		position: point,
		map: map,
		title: 'Your Location'
	});

	LoadMap(map);	
};

window.addEvent('domready', function () {
	InitMap(lat_map, lon_map);
});