Bubble = {}

Bubble.container = 'bubble-msg';
Bubble.region = 'deutschschweiz';
Bubble.overlay = '#bubble-msg .overlay';
Bubble.footerConteiner = 'footer_conteiner';
Bubble.page = 1;
Bubble.intervalID = "";

Bubble.Load = function (page) {
	
	if ( undefined == page || page < 1 ) page = Bubble.page;
	clearInterval(Bubble.intervalID);
	Bubble.page = page;
	var url = '/escorts/ajax-bubble?page=' + page + '&r=' + Bubble.region;
	
	Bubble.Show(url);
	Bubble.intervalID = setInterval( function(){
		Bubble.page++;
		var url = '/escorts/ajax-bubble?page=' + Bubble.page + '&r=' + Bubble.region;
		Bubble.Show(url);
	} , 5000);
	return false;
}

Bubble.Show = function (url) {
	$$(Bubble.overlay).setStyle('display','block');
	
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {
			if (resp.length > 0){
				$(Bubble.container).getElement('.bubble-body').set('html', resp);
				$(Bubble.container).removeClass('none');
				$$(Bubble.overlay).setStyle('display','none');
			}
			else if (Bubble.page == 1 && resp.length == 0){
				clearInterval(Bubble.intervalID);
			}
			else{
				Bubble.page = 1;
				var url = '/escorts/ajax-bubble?page=' + Bubble.page + '&r=' + Bubble.region;
				Bubble.Show(url);
			}
			
		}
	}).send();
}
Bubble.Close = function(){
	Cookie.write('no_m_bubble_text',1,{duration: 1});
	clearInterval(Bubble.intervalID);
	$('bubble-msg').destroy();
	$(Bubble.footerConteiner).removeClass('mtn205');
	$('page').removeClass('pb220');
	return false;
}
