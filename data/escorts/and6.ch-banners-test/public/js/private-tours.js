var ToursEditor = new Class({
	Implements: [Options, Events],

	options: {
		inputName: 'tours[]',
		labels: {
			norates: 'Youd don\'t have any planned or active tours'
		}
	},

	element: null,
	list: null,
	elements: {},

	currency: 2,

	store: null,

	initialize: function (element, list, options) {
		this.element = $(element);
		this.list = $(list);
		this.setOptions(options || {});

		this.elements = this.element
			.getElements('.form select, .form input, .form .btn-add')
			.associate(['country', 'city', 'id', 'date_from', 'date_from_clone', 'date_to', 'date_to_clone', 'phone', 'email', 'btnAdd']);
		
		for ( var i in this.elements ) {
			var el = this.elements[i];
			el.showError = function () {
				this.setStyle('background-image', 'none').get('tween', {property: 'background-color', duration: 'short', onComplete: function () {this.element.setStyle('background', '')}}).set('#ffcccc').start('#ffffff');
			}.bind(el);
		}

		this.elements.hiddens = {};

		this.store = new ToursStore();

		this.init();
	},

	/**
	 * @param Tour tour
	 * @param bool ajax
	 */
	add: function (tour, ajax) {
		if ( ! $defined(ajax) ) ajax = true;

		if ( ajax ) {
			this.store.add(tour, function (resp) {
				if ( 'success' == resp.status ) {
					this.load(resp.data)
				}
				else if ('error' == resp.status) {
					var errors = "";

					for ( var i in resp.msgs ) {
						errors += resp.msgs[i] + "\r\n";
					}
					alert(errors);
				}
			}.bind(this));
			
			return;
		}

		var el = tour.render();
		el.tour = tour;
		el.addEvent('click', function (e) {
			e.stop();
			this.fireEvent('select', [el, this]);
		}.bind(this));

		el.inject(this.list);
	},

	remove: function (id) {
		this.store.remove(id, function (resp) {
			this.load(resp.data)
		}.bind(this));
	},

	// Returns false if not valid data supplied
	getValues: function () {
		var els = this.elements;

		var values = {
			country: els.country.getSelected()[0].get('text').trim(),
			country_id: els.country.getSelected()[0].get('value'),
			city: els.city.getSelected()[0].get('text').trim(),
			city_id: els.city.getSelected()[0].get('value'),
			date_from: els.date_from.get('value'),
			date_to: els.date_to.get('value'),
			phone: els.phone.get('value'),
			email: els.email.get('value')
		};

		if ( this.selected ) {
			values.id = this.selected.tour.id;
		}

		if ( $('escort') ) {
			values.escort_id = $('escort').getSelected()[0].get('value');
		}

		var error = false;
		if ( ! values.country_id ) {
			error = true;
			els.country.showError();
		}

		if ( ! values.city_id ) {
			error = true;
			els.city.showError();
		}

		if ( ! values.date_from ) {
			error = true;
			els.date_from_clone.showError();
		}

		if ( ! values.date_to ) {
			error = true;
			els.date_to_clone.showError();
		}

		return error ? false : values;
	},

	init: function () {
		this.elements.btnAdd.addEvent('click', function () {
			var values = this.getValues();
			if ( ! values ) return;
			this.add(new Tour(values));
			
			// Clear selected values
			for ( var k in this.elements ) {
				if ( this.elements[k].get && this.elements[k].get('value') ) {
					this.elements[k].set('value', '');
				}
			}
			this.elements.country.fireEvent('change');
		}.bind(this));
	},

	ajaxLoad: function () {
		this.store.list(function (resp) {
			if ( resp && resp.data )
				this.load(resp.data);
		}.bind(this));
	},

	load: function (data) {
		this.list.getElements('tr').destroy();
		data.each(function (tour) {
			this.add(new Tour(tour), false);
		}.bind(this));
	}
});

var Tour = new Class({
	element: null,
	params: null,
	initialize: function (params) {
		$extend(this, params);
		this.params = params;
	},

	render: function () {
		this.element = new Element('tr');

		this.element.adopt(
			new Element('td', { html: Tour.DatePicker.format(new Date(this.date_from * 1000), 'j M Y') + '<br/>' + Tour.DatePicker.format(new Date(this.date_to * 1000), 'j M Y') }),
			new Element('td', { html: this.city + '<br/><span>' + this.country + '</span>' }),
			new Element('td', { html: this.phone + (this.phone.length ? '<br/>' : '') + this.email })
		);

		return this.element;
	},

	equal: function (rate) {
		return false;
	},

	getValue: function () {
		var params = {};
		for ( var param in this.params ) {
			params[param] = this[param];
		}

		return params;
	}
});

Tour.DatePicker = null;

var ToursStore = new Class({
	getEscortId: function () {
		if ( $('escort') ) {
			return $('escort').getSelected()[0].get('value');
		}

		return null;
	},

	add: function (tour, callback) {
		Navigation.overlay.disable();
		
		data = tour.getValue();
		new Request({
			url: ToursStore.Urls.add,
			method: 'post',
			data: $extend(data, { escort_id: this.getEscortId() }),
			onSuccess: function (resp) {
				resp = JSON.decode(resp, true);
				callback.run([resp], this);
				Navigation.overlay.enable();
			}.bind(this)
		}).send();
	},

	remove: function (tour, callback) {
		Navigation.overlay.disable();
		new Request({
			url: ToursStore.Urls.remove,
			method: 'get',
			data: { escort_id: this.getEscortId(), id: tour },
			onSuccess: function (resp) {
				resp = JSON.decode(resp, true);
				callback.run([resp], this);
				Navigation.overlay.enable();
			}.bind(this)
		}).send();
	},

	list: function (callback) {
		//Navigation.overlay.disable();
		new Request({
			url: ToursStore.Urls.list,
			method: 'get',
			data: { escort_id: this.getEscortId() },
			onSuccess: function (resp) {
				resp = JSON.decode(resp, true);
				callback.run([resp], this);
			}.bind(this)
		}).send();
	}
});

ToursStore.Urls = {
	list: '/' + Cubix.Lang.id + '/private-v2/ajax-tours',
	add: '/' + Cubix.Lang.id + '/private-v2/ajax-tours-add',
	remove: '/' + Cubix.Lang.id + '/private-v2/ajax-tours-remove'
}
