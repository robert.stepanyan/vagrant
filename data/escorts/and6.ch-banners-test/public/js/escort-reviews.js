Cubix.Rev = {};

Cubix.Rev.url = ''; // Must be set from php
Cubix.Rev.lng = '';
Cubix.Rev.container = '';

window.addEvent('domready', function(){
	var lng = Cubix.Rev.lng;
	if (lng == 'fr')
		lng = '';
	if (lng.length > 0)
		lng = '/' + lng;
	
	Cubix.Rev.container = $$('.ajax')[0];

	Cubix.Rev.initFiltration = function() {
		if ( $('r_city') ) {
			$('r_city').removeEvents('change');
			$('r_city').addEvent('change', function(){
				var sel = $('r_city').get('value');
				var url = '';

				var member = '';
				if ( $$('input[name=member]').length ) {
					member = '/member/' + $$('input[name=member]')[0].get('value');
				}

				if (sel)
					url = lng + '/evaluations' + member + '?city_id=' + sel + '&ajax=1' + '&ord_dir=' + $$('input[name=ord_dir]')[0].get('value') + '&ord_field=' + $$('input[name=ord_field]')[0].get('value');
				else
					url = lng + '/evaluations' + member + '?ajax=1';

				Cubix.Rev.doFiltrationRequest(url);
			});
		}

		if ( $('f_search') ) {
			$('f_search').removeEvents('click');
			$('f_search').addEvent('click', function(){
				var city_id = $('r_city').get('value');
				var showname = $('f_showname').get('value');
				var member_name = $('f_member').get('value');

				var url = '';

				var member = '';
				if ( $$('input[name=member]').length ) {
					member = '/member/' + $$('input[name=member]')[0].get('value');
				}

				if (showname || member_name || city_id)
					url = lng + '/evaluations' + member + '?member_name=' + member_name + '&showname=' + showname + '&city_id=' + city_id + '&ajax=1' + '&ord_dir=' + $$('input[name=ord_dir]')[0].get('value') + '&ord_field=' + $$('input[name=ord_field]')[0].get('value');
				else
					url = lng + '/evaluations' + member + '?ajax=1';

				Cubix.Rev.doFiltrationRequest(url);
			});
		}

		if ( $('f_search_escorts') ) {
			$('f_search_escorts').removeEvents('click');
			$('f_search_escorts').addEvent('click', function(){
				var country_id = $('r_country').get('value');
				var showname = $('f_showname').get('value');

				var url = '';

				if (showname || country_id)
					url = lng + '/evaluations/escorts?showname=' + showname + '&country_id=' + country_id + '&ajax=1' + '&ord_dir=' + $$('input[name=ord_dir]')[0].get('value') + '&ord_field=' + $$('input[name=ord_field]')[0].get('value');
				else
					url = lng + '/evaluations/escorts?ajax=1';

				Cubix.Rev.doFiltrationRequest(url);
			});
		}

		if ( $$('.alph').length ) {
			$$('.alph').removeEvents('click');
			$$('.alph').addEvent('click', function(e) {
				e.stop();
				var href = this.get('href');
				var letter = href.substr(1, 1);
				$('f_showname').set('value', letter);

				$('f_search_escorts').fireEvent('click');
			});
		}

		if ( $('f_search_agencies') ) {
			$('f_search_agencies').removeEvents('click');
			$('f_search_agencies').addEvent('click', function(){
				var country_id = $('r_country').get('value');
				var name = $('f_name').get('value');

				var url = '';

				if (name || country_id)
					url = lng + '/evaluations/agencies?name=' + name + '&country_id=' + country_id + '&ajax=1' + '&ord_dir=' + $$('input[name=ord_dir]')[0].get('value') + '&ord_field=' + $$('input[name=ord_field]')[0].get('value');
				else
					url = lng + '/evaluations/agencies?ajax=1';

				Cubix.Rev.doFiltrationRequest(url);
			});
		}

		if ( $$('.alph-ag').length ) {
			$$('.alph-ag').removeEvents('click');
			$$('.alph-ag').addEvent('click', function(e) {
				e.stop();
				var href = this.get('href');
				var letter = href.substr(1, 1);
				$('f_name').set('value', letter);

				$('f_search_agencies').fireEvent('click');
			});
		}
	};

	Cubix.Rev.doFiltrationRequest = function(url) {

		var overlay = new Cubix.Overlay($$('.ajax')[0], {
			loader: _st('loader-small.gif'),
			position: '50%'
		});

		overlay.disable();

		var req = new Request({
			method: 'get',
			url: url,
			onComplete: function(response) {
				$$('.ajax')[0].set('html', response);
				Cubix.Rev.initFiltration();
				Cubix.Rev.initOrdering();
				overlay.enable();
			}
		}).send();
	};

	Cubix.Rev.initOrdering = function() {

		if ( $$('.ord').length > 0 ) {
			$$('.ord').addEvent('click', function(e) {
				e.stop();

				var rel = this.get('rel');
				var page = $$('input[name=page]')[0].get('value');
				var city_id = '';
				if ( $$('input[name=city_id]').length ) {
					city_id = $$('input[name=city_id]')[0].get('value');
				}
				var showname = '';
				if ( $('f_showname') ) {
					showname = $('f_showname').get('value');
				}

				var member_name = '';
				if ( $('f_member') ) {
					member_name = $('f_member').get('value');
				}

				var arr = new Array();

				arr = rel.split('|');

				var ord_field = arr[0];
				var ord_dir = '';

				if (arr[1] == undefined)
					ord_dir = 'desc';
				else
					ord_dir = arr[1];

				if ( ord_dir == 'asc' ) {
					ord_dir = 'desc';
				}
				else {
					ord_dir = 'asc';
				}

				var member = '';
				
				if ( $$('input[name=member]').length ) {
					member = '/member/' + $$('input[name=member]')[0].get('value');
				}

				var url = lng + '/evaluations' + member + '?ord_field=' + ord_field + '&ord_dir=' + ord_dir + '&ajax=1&page=' + page + '&city_id=' + city_id + '&member_name=' + member_name + '&showname=' + showname;

				Cubix.Rev.doSortRequest(url);
			});
		}
	};

	Cubix.Rev.doSortRequest = function(url) {

		var overlay = new Cubix.Overlay($$('.ajax')[0], {
			loader: _st('loader-small.gif'),
			position: '50%'
		});

		overlay.disable();

		var req = new Request({
			method: 'get',
			url: url,
			onComplete: function(response) {
				$$('.ajax')[0].set('html', response);
				Cubix.Rev.initOrdering();
				overlay.enable();
			}
		}).send();
	};

	Cubix.Rev.initFiltration();
	Cubix.Rev.initOrdering();

	if ( $('perpage') ) {
		$('perpage').addEvent('change', function() {
			var url = $('perpage').get('rel');
			url = url + '&ajax=1';
			url = url + '&perpage=' + $('perpage').get('value');

			Cubix.Rev.Show(url);
		});
	}
});

Cubix.Rev.Load = function (el) {
	var url = el.get('href');
	url = url + '&ajax=1';

	if ($('perpage').get('value'))
		url = url + '&perpage=' + $('perpage').get('value');

	Cubix.Rev.Show(url);
	
	return false;
}

Cubix.Rev.Show = function (url) {
	
	var overlay = new Cubix.Overlay($(Cubix.Rev.container), {
		loader: _st('loader-small.gif'),
		position: '50%'
	});
	
	overlay.disable();
	
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {
			$(Cubix.Rev.container).set('html', resp);
			overlay.enable();
			Cubix.Rev.initFiltration();
			Cubix.Rev.initOrdering();
		}
	}).send();
}

/* <-- */
