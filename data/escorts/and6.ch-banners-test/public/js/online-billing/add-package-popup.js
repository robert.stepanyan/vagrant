/* --> Feedback */
Cubix.AddPackagePopup = {};
Cubix.ShoppingCart = {};

Cubix.AddPackagePopup.inProcess = false;
Cubix.AddPackagePopup.url = '';

Cubix.ShoppingCart.item;
Cubix.ShoppingCart.url = '';

Cubix.AddPackagePopup.Show = function (box_height, box_width, el) {
	if ( Cubix.AddPackagePopup.inProcess ) return false;

	var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000' });
	page_overlay.disable();

	var y_offset = -30;
	var x_offset = 100;

	var container = new Element('div', { 'class': 'online-billing-popup-wrapper'}).setStyles({
		left: window.getWidth() / 2 - box_width / 2 - x_offset,
		top:  el.getPosition().y + y_offset,
		opacity: 0,
        position: 'absolute',
        'z-index': 101,
        background: '#fff'
	}).inject(document.body);
	
	Cubix.AddPackagePopup.inProcess = true;

	new Request({
		url: Cubix.AddPackagePopup.url,
		method: 'get',
		onSuccess: function (resp) {
			
			Cubix.AddPackagePopup.inProcess = false;
			container.set('html', resp);

			var close_btn = new Element('div', {
				html: '',
				'class': 'online-billing-popup-close-btn'
			}).inject(container);

			close_btn.addEvent('click', function() {
				$$('.online-billing-popup-wrapper').destroy();
				page_overlay.enable();
			});

			container.tween('opacity', '1');
			
			if ( container.getElements('.btn-add')[0] ) {
			
				var add_btn = container.getElements('.btn-add')[0];
				var tbl_escorts_list = container.getElement('#escorts-list');
				var selbox_escorts = container.getElement('select[name=escorts]');
				var opt_products = JSON.decode(container.getElements('.json-data')[0].get('html'));
				var add_cities_products = JSON.decode(container.getElements('.json-data-add-cities')[0].get('html'));

				container.getElements('.json-data-add-cities')[0].destroy();

				add_btn.addEvent('click', function(e){
					e.stop();

					var selected = selbox_escorts.getSelected();

					if ( selected.get('value')[0] == 0 ) {
						selbox_escorts.highlight('#f8e7e7');
						//alert('Please select escort!');
						return false;
					}

					var hidden_esc_ids = container.getElements('.hidden-esc-ids');
					var already_in_list = false;
					hidden_esc_ids.each(function(input){
						if ( input.get('value') == selected.get('value')[0] ) {
							input.getParent('tr').highlight('#f8e7e7');
							already_in_list = true;
						}
					});

					if ( already_in_list ) {
						return false;
					}

					var tbody = tbl_escorts_list.getElement('tbody');
					var tr  = new Element('tr').inject(tbody);

					var first_td = new Element('td').inject(tr);
					var remove_escort = new Element('span', {
						'class': 'remove-escort'
					}).inject(first_td);
					var escort_name = new Element('span', {
						'class': 'escort-name',
						'html' : selected.get('html')[0]
					}).inject(first_td);
					var escort_hidden = new Element('input', {
						'type':'hidden',
						'value':selected.get('value')[0],
						'name':'escort_ids[]',
						'class': 'hidden-esc-ids'
					}).inject(first_td);

					var second_td = new Element('td').inject(tr);
					opt_products.each(function(product){
						var label = new Element('label').inject(second_td);
						var opt_product = new Element('input', {
							'type':'checkbox',
							'value':product.id,
							'name':'optional_products[' + selected.get('value')[0] + '][]'
						}).inject(label);
						var span = new Element('span', {
							'html': product.name + ' (+' + product.price + ' EUR)'
						}).inject(label);
					});


					/*if ( add_cities_products.length > 0 ) {

						var label = new Element('label').inject(second_td);
						var opt_product = new Element('input', {
							'checked':'checked',
							'type':'radio',
							'value':1000,
							'name':'optional_products[' + selected.get('value')[0] + '][]'
						}).inject(label);
						var span = new Element('span', {
							'html': 'No Additional City Premium Spot (+0 EUR)'
						}).inject(label);

						add_cities_products.each(function(product){
							var label = new Element('label').inject(second_td);
							var opt_product = new Element('input', {
								'type':'radio',
								'value':product.id,
								'name':'optional_products[' + selected.get('value')[0] + '][]'
							}).inject(label);
							var span = new Element('span', {
								'html': product.name + ' (+' + product.price + ' EUR)'
							}).inject(label);
						});
					}*/

					remove_escort.addEvent('click', function(e){
						e.stop();

						this.getParent('tr').destroy();
					});
				});
			}
			
			var forms = container.getElements('form');
			forms.addEvent('submit', Cubix.AddPackagePopup.Send);
			forms.each(function (form) { form.set('action', form.get('action') + (form.get('action').indexOf('?') != -1 ? '&' : '?') + 'ajax'); });
		}
	}).send();		
	
	return false;
}

Cubix.AddPackagePopup.Send = function (e) {
	e.stop();
   
	var overlay = new Cubix.Overlay($$('.online-billing-popup-wrapper')[0], { loader: _st('loader-small.gif'), position: '50%', no_relative: true });
	overlay.disable();

	this.set('send', {
		onSuccess: function (resp) {

			resp = JSON.decode(resp);

			$$('.online-billing-popup-wrapper').getElements('.error')[0].setStyle('display', 'none');

			overlay.enable();

			if ( resp.status == 'error' ) {
				$$('.online-billing-popup-wrapper').getElements('.error')[0].setStyle('display', 'block');
			}
			else if ( resp.status == 'success' ) {
                $$('.online-billing-popup-wrapper').destroy();
				$$('.overlay').destroy();
				
				Cubix.ShoppingCart.load();
			}
		}.bind(this)
	});

	this.send();
}

Cubix.ShoppingCart.load = function () {
	var page_overlay = new Cubix.Overlay(Cubix.ShoppingCart.item, { loader: _st('loader-small.gif'), position: '50%', no_relative: false, offset: { left: 0, top: -10, right: 0, bottom: 10 }, });
	page_overlay.disable();
	
	new Request({
		url: Cubix.ShoppingCart.url,
		method: 'get',
		onSuccess: function (resp) {
			Cubix.ShoppingCart.item.set('html', resp);
			
			Cubix.ShoppingCart.init();
		}
	}).send();		
};

Cubix.ShoppingCart.init = function () {	
	Cubix.ShoppingCart.item.getElements('.remove-package').addEvent('click', function(e){
		e.stop();
		
		var page_overlay = new Cubix.Overlay(Cubix.ShoppingCart.item, { loader: _st('loader-small.gif'), position: '50%', no_relative: false, offset: { left: 0, top: -10, right: 0, bottom: 10 }, });
		page_overlay.disable();
		
		new Request({
			url: Cubix.ShoppingCart.remove_url + '?shopping_cart_id=' + this.get('href'),
			method: 'get',
			onSuccess: function (resp) {
				page_overlay.enable();
				
				Cubix.ShoppingCart.load();
			}
		}).send();		
	});
	
	Cubix.ShoppingCart.item.getElements('.change-city').addEvent('click', function(e) {
		e.stop();
		
		Cubix.ChangeCityPopup.Show(300, 560, this);
	});
};