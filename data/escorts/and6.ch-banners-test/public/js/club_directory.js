/* --> LocationHash */
Cubix.LocationHash = {};

Cubix.LocationHash.Set = function (hash) {
	if ( hash.length ) {
		document.location.hash = '#' + hash;
	}
	else {
		document.location.hash = '';
	}
	
	return false;
}

Cubix.LocationHash.Parse = function () {
	var hash = document.location.hash.substring(1);
	
	if ( ! hash.length ) {
		return {};
	}
	
	var params = hash.split(';');
	
	var filter = '';
	
	var not_array_params = ['page', 'area', 'city', 'search_text', 'listing_type', 'all_cities', 'c'];
	
	params.each(function (param) {
		var key_value = param.split('=');
		
		var key = key_value[0];
		var val = key_value[1];
		
		if ( val === undefined ) return;
		
		val = val.split(',');
				
		val.each(function(it) {
			filter += key + ( ( ! not_array_params.contains(key) ) ? '[]=' : '=' ) + it + '&';
		});
	});
	
	return filter;
}

Cubix.LocationHash.Make = function (params) {
	
	var hash = '';
	var sep = ';';
	var value_glue = ',';
	
	var places = $$('#area-inputs, #city-inputs, .search-wrap, #filters');
	
	var map = {};
	places.each(function(place) {
		var inputs = place.getElements('input:checked');
		inputs.append(place.getElements('input[type=hidden]'));
		//inputs.append(place.getElements('input[type=text]'));
		inputs.each(function(input) {
			if ( input.get('value').length ) {
				var index = input.get('name').replace('[]', '');
				if ( map[index] === undefined ) map[index] = new Array();
				map[index].append([input.get('value')]);
			}
		});
	});
	
	map = $merge(map, params);
	
	for (var i in map) {
		hash += i + '=' + map[i].join(value_glue) + sep;
	}
	
	return hash;
}
/* <-- */

/* --> HashController */
Cubix.HashController = {
	_current: '',
	
	init: function () {
		setInterval('Cubix.HashController.check()', 100);
	},
	
	check: function () {
		var hash = document.location.hash.substring(1);
		
		if (hash != this._current) {
			this._current = hash;
					
			var data = Cubix.LocationHash.Parse();
			
			if ($defined($(Cubix.ClubDirectory.map_container))) {
				Cubix.HashController.InitMap(data);
			} else {
				Cubix.ClubDirectory.Load(data);
			}
			
			Cubix.ClubDirectory.LoadFilters(data);
		}
	}
};
/* <-- */

Cubix.Filter = {};
Cubix.Filter.Set = function (filter) {
		
	Cubix.LocationHash.Set(Cubix.LocationHash.Make(filter));

	return false;
}

Cubix.ClubDirectory = {};

Cubix.ClubDirectory.container = 'list';
Cubix.ClubDirectory.filter_container = 'filter-cont';
Cubix.ClubDirectory.map_container = 'map_canvas';
Cubix.ClubDirectory.default_search = '';

Cubix.ClubDirectory.Load = function (data) {
	
	var url = '/club-directory/list?ajax';
	
	var overlay = new Cubix.Overlay($(Cubix.ClubDirectory.container), {
		loader: _st('loader-small.gif'),
		position: '50%'
	});
	
	overlay.disable();
	
	new Request({
		url: url,
		method: 'POST',
		data: data,
		onSuccess: function (resp) {
			var resp = JSON.decode(resp);
			
			if ( resp.count == 0 && resp.c == 0 ) {
				var cities_input_cont = $('city-inputs');
				var areas_input_cont = $('area-inputs');
				
				cities_input_cont.set('html', '');
				areas_input_cont.set('html', '');

				$$('input[name=all_cities]')[0].set('value', 1);

				Cubix.LocationHash.Set(Cubix.LocationHash.Make());
			}
						
			$(Cubix.ClubDirectory.container).set('html', resp.body);
			overlay.enable();
			
			$$('a.d').addEvent('click', function(e){
				e.stop();
			});
		}
	}).send();
	
	return false;
}

Cubix.ClubDirectory.LoadFilters = function (data) {
	
	var url = '/club-directory/filter';
	
	var overlay = new Cubix.Overlay($(Cubix.ClubDirectory.filter_container), {
		loader: _st('loader-small.gif'),
		position: '50%'
	});
	
	overlay.disable();
	
	new Request({
		url: url,
		method: 'POST',
		data: data,
		onSuccess: function (resp) {			
			$(Cubix.ClubDirectory.filter_container).set('html', resp);
			overlay.enable();
			
			Cubix.HashController.InitAll();	
			
			if ( $('show-map-lnk') ) {
				var url = Cubix.LocationHash.Make().replace('=list', '=map');
				url = url.replace('c=0', 'c=1');
				$('show-map-lnk').href = '/club-directory/map#' + url;
			}
			
			if ( $('show-list-lnk') ) {
				var url = Cubix.LocationHash.Make().replace('=map', '=list')
				url = url.replace('c=0', 'c=1');
				$('show-list-lnk').href = '/club-directory#' + url;
			}
			
		}
	}).send();
	
	return false;
}

Cubix.ClubDirectory.InitAreas = function(){
	var areas_input_cont = $('area-inputs');
	$$('#regions a').addEvent('click', function(e){
		e.stop();
		
		areas_input_cont.set('html', '');
		
		$$('#regions a.selected, #regions li.selected').each(function(it){
			it.removeClass('selected');
		});
		
		this.addClass('selected');
		this.getParent('li').addClass('selected');

		new Element('input', {
			'type':'hidden',
			'name':'area',
			'value':this.get('rel')
		}).inject(areas_input_cont);
		
		$$('input[name=all_cities]')[0].set('value', 0);
		
		$('city-inputs').set('html', '');
		
		$$('input[name=c]')[0].set('value', 1);
		
		Cubix.LocationHash.Set(Cubix.LocationHash.Make());
	});
};

Cubix.ClubDirectory.InitCities = function(){
	var cities_input_cont = $('city-inputs');
	var areas_input_cont = $('area-inputs');
	
	$$('a.curr_city').addEvent('click', function(e){
		e.stop();
		
		cities_input_cont.set('html', '');
		
		$$('.nearest-cities a.selected').each(function(it){
			it.removeClass('selected');
		});
		
		this.addClass('selected');

		new Element('input', {
			'type':'hidden',
			'name':'city',
			'value':this.get('rel')
		}).inject(cities_input_cont);
		
		$$('input[name=all_cities]')[0].set('value', 0);
		
		$$('input[name=c]')[0].set('value', 1);
		
		areas_input_cont.set('html', '');
		
		Cubix.LocationHash.Set(Cubix.LocationHash.Make());
	});
	
	$$('a.n_city').addEvent('click', function(e){
		e.stop();
		
		cities_input_cont.set('html', '');
		
		$$('.nearest-cities a.selected').each(function(it){
			it.removeClass('selected');
		});
		
		this.addClass('selected');

		new Element('input', {
			'type':'hidden',
			'name':'city',
			'value':this.get('rel')
		}).inject(cities_input_cont);
		
		$$('input[name=all_cities]')[0].set('value', 0);
		
		$$('input[name=c]')[0].set('value', 1);
		
		Cubix.LocationHash.Set(Cubix.LocationHash.Make());
	});
};

Cubix.ClubDirectory.InitSearchInput = function(){
	if ($defined($('f-search'))){
		
		if ( Cubix.ClubDirectory.default_search != $('f-search').get('value') ) {
			$('f-search').removeClass('gray');
		}

		$('f-search').addEvent('click', function() {
			var self = this;

			if (self.hasClass('gray'))
			{
				self.removeClass('gray');
				self.set('value', '');
			}
		});

		$('f-search').addEvent('blur', function() {
			var self = this;

			if (self.get('value').length == 0)
			{
				self.addClass('gray');

				self.set('value', Cubix.ClubDirectory.default_search);
			}
		});

		$('f-search').addEvent('keyup', function() {
			var self = this;

			$('search').set('value', self.get('value'));
		});
		
		/*$('f-search').addEvent('keypress', function(e){
			  if(e.key == 'enter') { 
				e.stop(); 
				
				$$('input[name=c]')[0].set('value', 1);
				Cubix.LocationHash.Set(Cubix.LocationHash.Make());
			  }
		});*/

		$$('.send-club').addEvent('click', function(){
			$$('input[name=c]')[0].set('value', 1);
			Cubix.LocationHash.Set(Cubix.LocationHash.Make());
		});
		
		$$('.reset-club').addEvent('click', function(){
			$('search').set('value', '');
			Cubix.LocationHash.Set(Cubix.LocationHash.Make());
		});
		
		var search = $$('.search-wrap input[name=f_search_text]')[0];
		var search_h = $$('.search-wrap input[name=search_text]')[0];

		new Autocompleter.Request.JSON(search, '/club-directory/ajax-search', {
			postVar: 'search_string',
			indicatorClass: 'autocompleter-loading',
			maxChoices: 15,
			selectFirst: true
		}).addEvent('onSelection', function (element, selected, value, search) {
			if ( selected.inputValue.id != 'list_all' ) {
				search_h.set('value', selected.inputValue.name);
			}
			
			$$('input[name=c]')[0].set('value', 1);
			Cubix.LocationHash.Set(Cubix.LocationHash.Make());
		}).addEvent('onSelect', function(el, sel, selection) {
			$$('.au-img').set('style', 'display: none');
			var s = sel.getElement('span');

			if (s)
				s.set('style', 'display: inline');
		});

		/*home_city.addEvent('blur', function() {
			if ( ! this.get('value') ) {
				home_city_h.set('value', '');
			}
		});*/
	}
};

Cubix.ClubDirectory.InitFilters = function(){
	$$('#filters input').addEvent('change', function(){
		$$('input[name=c]')[0].set('value', 1);
		Cubix.LocationHash.Set(Cubix.LocationHash.Make());
	});
};

Cubix.ClubDirectory.markersArray = new Array();
Cubix.ClubDirectory.LoadMap = function (map, data) {
	
	var url = '/club-directory/ajax-map?' + data;
	
	var overlay = new Cubix.Overlay($(Cubix.ClubDirectory.container), {
		loader: _st('loader-small.gif'),
		position: '50%'
	});
	
	overlay.disable();
	
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {
			//clear markers	
			if (Cubix.ClubDirectory.markersArray.length) 
			{
				Cubix.ClubDirectory.markersArray.each(function(row) {
					row.setMap(null);
				});
				
				Cubix.ClubDirectory.markersArray = new Array();
			}
			
			var obj = JSON.decode(resp);
			var c = obj.c;
			obj = obj.data;
			
			if ( obj.length == 0 && c == 0 ) {
				var cities_input_cont = $('city-inputs');
				var areas_input_cont = $('area-inputs');
				
				cities_input_cont.set('html', '');
				areas_input_cont.set('html', '');

				$$('input[name=all_cities]')[0].set('value', 1);

				Cubix.LocationHash.Set(Cubix.LocationHash.Make());
			}
			
			var image_escort = new google.maps.MarkerImage('/img/cd/wg.png',
				// This marker is 20 pixels wide by 32 pixels tall.
				new google.maps.Size(28, 47),
				// The origin for this image is 0,0.
				new google.maps.Point(0,0),
				// The anchor for this image is the base of the flagpole at 0,32.
				new google.maps.Point(0, 47)
			);
				
			var image_1 = new google.maps.MarkerImage('/img/cd/1.png',
				// This marker is 20 pixels wide by 32 pixels tall.
				new google.maps.Size(28, 47),
				// The origin for this image is 0,0.
				new google.maps.Point(0,0),
				// The anchor for this image is the base of the flagpole at 0,32.
				new google.maps.Point(0, 47)
			);
				
			var image_2 = new google.maps.MarkerImage('/img/cd/2.png',
				// This marker is 20 pixels wide by 32 pixels tall.
				new google.maps.Size(28, 47),
				// The origin for this image is 0,0.
				new google.maps.Point(0,0),
				// The anchor for this image is the base of the flagpole at 0,32.
				new google.maps.Point(0, 47)
			);
				
			var image_3 = new google.maps.MarkerImage('/img/cd/3.png',
				// This marker is 20 pixels wide by 32 pixels tall.
				new google.maps.Size(28, 47),
				// The origin for this image is 0,0.
				new google.maps.Point(0,0),
				// The anchor for this image is the base of the flagpole at 0,32.
				new google.maps.Point(0, 47)
			);
				
			var image_4 = new google.maps.MarkerImage('/img/cd/4.png',
				// This marker is 20 pixels wide by 32 pixels tall.
				new google.maps.Size(28, 47),
				// The origin for this image is 0,0.
				new google.maps.Point(0,0),
				// The anchor for this image is the base of the flagpole at 0,32.
				new google.maps.Point(0, 47)
			);

			obj.each(function(e) {
				var lat = e.latitude;
				var lon = e.longitude;
						
				var img = '';
				
				if (lat && lon) 
				{
					if (e.escort_id)
					{
						var name = "<div class='fleft' style='border: 1px solid #ccc;padding:1px;'><a href='" + e.url + "'><img src='" + e.photo + "' width='68' height='91' /></a></div>";
							
						name += "<div class='fleft' style='margin-left: 7px;'><p style='margin: 0 0 3px 0;'><a style='font-size:14px;' href='" + e.url + "'>" + e.showname + "</a></p>";
						
						if (e.address.length)
							name += "<p>" + e.address + ((e.zip.length) ? ', ' + e.zip + ' ' : ', ') + ((e.city_title.length) ? e.city_title : '') + "</p></div>";

						img = image_escort;
					}
					else
					{
						var name = "<p style='margin: 0 0 3px 0;'><a style='font-size:14px;' href='" + e.url + "'>" + e.club_name + "</a></p>";

						if (e.address.length)
							name += "<p>" + e.address + ((e.zip.length) ? ', ' + e.zip + ' ' : ', ') + ((e.city_title.length) ? e.city_title : '') + "</p>";
						
						
						
						switch (e.filter_criteria) {
							case 1:
								img = image_1;
								break;
							case 2:
								img = image_2;
								break;
							case 3:
								img = image_3;
								break;
							case 4:
								img = image_4;
								break;
						}
					}
					//console.log(img);
					var marker = new google.maps.Marker({
						position: new google.maps.LatLng(lat, lon),
						map: map,
						icon: img
					});
					
					marker.setMap(map);
					Cubix.ClubDirectory.markersArray.push(marker);
					
					var infowindow = new google.maps.InfoWindow({
						content: name
					});
					
					google.maps.event.addListener(marker, 'click', function() {
						infowindow.open(map, marker);
					});
				}
			});
			
			overlay.enable();
		}
	}).send();
	
	return false;
}

Cubix.HashController.InitMap = function(data) {
	if ($defined($(Cubix.ClubDirectory.map_container)))
	{
		var mapOptions = {
			center: new google.maps.LatLng(46.833333, 8.333333),
			zoom: 8,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};

		var map = new google.maps.Map($(Cubix.ClubDirectory.map_container), mapOptions);
						
		Cubix.ClubDirectory.LoadMap(map, data);
	}
};

Cubix.HashController.InitClear = function() {
	var cities_input_cont = $('city-inputs');
	var areas_input_cont = $('area-inputs');
	
	$$('.all-area-city').addEvent('click', function(e){
		e.stop();
		
		cities_input_cont.set('html', '');
		areas_input_cont.set('html', '');
	
		$$('input[name=all_cities]')[0].set('value', 1);
	
		Cubix.LocationHash.Set(Cubix.LocationHash.Make());
	});
};

Cubix.HashController.InitAll = function(){
	Cubix.HashController.init();
	
	Cubix.ClubDirectory.InitAreas();
	
	Cubix.ClubDirectory.InitCities();
	
	Cubix.ClubDirectory.InitSearchInput();
	
	Cubix.ClubDirectory.InitFilters();
	
	Cubix.HashController.InitClear();
};

window.addEvent('domready', function () {
	Cubix.HashController.InitAll();	
	
	/*if (navigator.geolocation) navigator.geolocation.getCurrentPosition(function(pos) {
		var me = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
		alert(me.toString());
	}, function(error) {
		// ...
	});*/
	$$('a.d').addEvent('click', function(e){
		e.stop();
	});
	
	
	if ( ! Cubix.LocationHash.Parse().length ) {
		Cubix.LocationHash.Set(Cubix.LocationHash.Make());
	}
});
