/* --> Report */
Cubix.RPopup = {};


Cubix.RPopup.inProcess = false;

Cubix.RPopup.url = '';
Cubix.RPopup.type = '';
Cubix.RPopup.location = '';

Cubix.RPopup.Show = function (wLeft, wBottom) {
	if ( Cubix.RPopup.inProcess ) return false;

	var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000' });
	page_overlay.disable();
	
	var container = new Element('div', { 'class': 'RPopup-wrapper'}).setStyles({
		left: wLeft,
		top: wBottom,
		opacity: 0,
        position: 'absolute',
		'z-index': 101
    })

	if(Cubix.RPopup.type == 'tell-friend'){
		container.setStyles({
			background: 'url("/img/v2.1/tell-friend.png") no-repeat',
			width:'380px',
			height:'439px'
		});
	}

	else if (Cubix.RPopup.type == 'report-problem'){
		container.setStyles({
			background: 'url("/img/v2.1/report-problem.png") no-repeat',
			width:'380px',
			height:'339px'
		});
	}

	container.inject(Cubix.RPopup.location);
	Cubix.RPopup.inProcess = true;

	new Request({
		url: Cubix.RPopup.url,
		method: 'get',
		onSuccess: function (resp) {
			
			Cubix.RPopup.inProcess = false;
			container.set('html', resp);
			container.getElement('form').set('action',Cubix.RPopup.url);
			var close_btn = new Element('div', {
				html: '',
				'class': 'close-btn-x'
			}).inject(container);

			var height = container.setStyles({
				visibility: 'hidden',
				display: 'block'
			}).getHeight();
			container.setStyles({
				top: container.getCoordinates().top - height,
				visibility: null,
				display: null
			});

			close_btn.addEvent('click', function() {
				$$('.RPopup-wrapper').destroy();
				page_overlay.enable();
			});

			new Fx.Scroll(document.body).start(container.getCoordinates().left, container.getCoordinates().top - 15);

			container.tween('opacity', '1');
			
			var forms = container.getElements('form');
			forms.addEvent('submit', Cubix.RPopup.Send);
			//forms.each(function (form) { form.set('action', form.get('action') + (form.get('action').indexOf('?') != -1 ? '&' : '?') + 'ajax'); });
		}
	}).send();
		
	
	return false;
}

Cubix.RPopup.Send = function (e) {
	e.stop();
	var self = $$('.RPopup-wrapper')[0];
	   
	var overlay = new Cubix.Overlay($$('.RPopup-wrapper')[0], { loader: _st('loader-small.gif'), position: '50%', no_relative: true });
	overlay.disable();

	this.set('send', {
		onSuccess: function (resp) {
			
			resp = JSON.decode(resp);
			
			this.getElements('.invalid').removeClass('invalid');

			overlay.enable();

			if ( resp.status == 'error' ) {
				var i = 0;
				for ( var field in resp.msgs ) {
					
					var input = this.getElement('*[name=' + field + ']');
					
					input.addClass('invalid');
					if (i == 0)
					$$('.error-message').set('html', resp.msgs[field]);
					i++;
				}
			}
			else if ( resp.status == 'success' ) {

				$$('.RPopup-wrapper').destroy();
				$$('.overlay').destroy();
				
			}
		}.bind(this)
	});

	this.send();
}