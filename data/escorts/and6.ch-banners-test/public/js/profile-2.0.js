function setCookie(name, value) {
    var expiration_date = new Date();   
    expiration_date.setFullYear(expiration_date.getFullYear(), expiration_date.getMonth(), expiration_date.getDate() + 7 );
    expiration_date = expiration_date.toUTCString();
    var cookie_string = escape(name) + "=" + escape(value) + "; expires=" + expiration_date;
    document.cookie = cookie_string;
}

function deleteCookie (name) {
    var expiration_date = new Date();
    expiration_date.setYear (expiration_date.getYear() - 1);
    expiration_date = expiration_date.toUTCString();
    var cookie_string = escape (name) + "=; expires=" + expiration_date;
    document.cookie = cookie_string;
}

function getCookie(c_name) {
    var i, x, y, ARRcookies = document.cookie.split(";");
    for (i = 0; i < ARRcookies.length; i++) {
            x = ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
            y = ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
            x = x.replace(/^\s+|\s+$/g,"");
            if (x == c_name) {
                    return unescape(y);
            }
    }
    return false;
}


/*var profileTabs = function() {
	
	var hash = window.location.hash;
	
	if ( hash == "" || hash == "#" ) {
		hash = "#info";
	}
	
	if ( getCookie("hideTabs") == "true" ) {
		hash = "#info";
		$("profile-tabs").addClass("hideTabs");
		$("show_tabs").show();
		$("hide_tabs").hide();
	}
	
	$$("#profile-tabs a").getParent('li').removeClass("active");
	
	var fullHash = hash.split("=");
	var hash = fullHash[0];
	
	$$("#profile-tabs a[href="+hash+"]")[0].getParent('li').addClass("active");
		
    var el = $$("#right a[rel="+hash.replace("#", "")+"]")[0];
	if(el) {
        var scroll = new Fx.Scroll(
             $("profile-container"),
            { wait: true, duration: 700, transition: Fx.Transitions.Quad.easeInOut }
        );
        scroll.toElement(el);
    }	
};

Cubix.HashController = {
	_current: '',
	
	init: function () {
		setInterval('Cubix.HashController.check()', 100);
	},
	
	check: function () {
		var hash = document.location.hash.substring(1);
		
		if (hash != this._current) {
			this._current = hash;
					
			profileTabs();
		}
	}
};*/

initGallery();

var reorderTabs = function(){
	var tabs_count = $$("#profile-tabs li").length;
	$$("#profile-tabs li").each(function(it, index){
		it.setStyle('z-index', tabs_count - index);
	});
};

window.addEvent('domready', function(){
	
	//Cubix.HashController.init();
	
	initGallery();
	
	reorderTabs();
	
	//profileTabs();
	
	/*if ( Browser.ie && Browser.version < 9 ) {
		var $hash = window.location.hash;
		setInterval(function() {
			$hash2 = window.location.hash;
			if ( $hash != $hash2 ) {
				$hash = $hash2;
				profileTabs();
			}
		}, 50);
	} else {
		window.addEvent("hashchange", function() {
			profileTabs();
		});
	}*/
	
	/*var $leftHeight = $("left").getSize().y;
	var $profileHeight = $("profile-container").getSize().y - 4700;
	if ( $leftHeight > $profileHeight ) {
		$("profile-container").setStyle('height', $leftHeight);
	} else {
		$("profile-container").setStyle('height', $profileHeight);
	}*/
	
	
	$("show_tabs").addEvent("click", function() {
		deleteCookie('hideTabs');
		$("profile-tabs").removeClass("hideTabs");
		$(this).hide();
		$("hide_tabs").show();
	});
	$("hide_tabs").addEvent("click", function() {
		setCookie('hideTabs', 'true');
		$("profile-tabs").addClass("hideTabs");
		$(this).hide();
		$("show_tabs").show();
		window.location.hash = "";
	});
	
	/*$("contactTab").addEvent("click", function() {
		$$(".NContact")[0].show();
		$$(".NMap")[0].hide();
		$("contactTab").addClass("active");
		$("mapTab").removeClass("active");
	});
	$("mapTab").addEvent("click", function() {
		$$(".NContact")[0].hide();
		$$(".NMap")[0].show();
		$("mapTab").addClass("active");
		$("contactTab").removeClass("active");
	});*/
	
	
	if ( $(profileVars.lang + '_chat_with_me_btn') ) {
		var chat_btn = $(profileVars.lang + '_chat_with_me_btn');

		if ( ! profileVars.currentUser ) {
		chat_btn.addEvent('click', function(e){
			e.stop();
			Cubix.Popup.Show('489', '652');
		});
		} else {
			chat_btn.addEvent('click', function(e){
				e.stop();
				//window.open('http://www.escortforum.net/index/chat?init_private=<?= $this->escort->username ?>', 'escortforum', 'height=620, width=820');
				window.open(profileVars.chatUrl, 'escortforum', 'height=620, width=820');
			});
		}
	}
	
	if ( $('sedcard-views') ) {
		var overlay = new Cubix.Overlay($('sedcard-views'), {
			loader: _st('loader-small.gif'),
			position: '50%',
			offset: {
				left: 0,
				top: -1
			}
		});

		overlay.disable();
	}

	var link = '';
	if ( profileVars.lang != profileVars.defLang ) {
		link = "&lang_id=" + profileVars.lang ;
	}
	
	if ( ! profileVars.disableReview ) {
		$$('.add-review').addEvent('click', function(e){
			if ( ! profileVars.currentUser || profileVars.currentUser.user_type != 'member' ) {
				e.stop();
				Cubix.Popup.Show('489', '652');
			} else {
				window.location = profileVars.reviewsUrl;
			}
		});
	}
	
	$$('.tell-friend').addEvent('click', function(e){
		e.stop();
		Cubix.RPopup.url = this.get('rel');
		Cubix.RPopup.type = this.get('class');
		Cubix.RPopup.location = document.body;
		
		var c = this.getCoordinates(document.body);
		Cubix.RPopup.Show(c.left + 110, c.top);
	});

	$$('.report-problem').addEvent('click', function(e){
		e.stop();
		Cubix.RPopup.url = this.get('rel');
		Cubix.RPopup.type = this.get('class');
		Cubix.RPopup.location = document.body;
		
		var c = this.getCoordinates(document.body);
		Cubix.RPopup.Show(c.left + 130, c.top);
	});	
	
	$$('.go-top').addEvent('click', function(){
		var myFx = new Fx.Scroll(window,{
			offset: {
				'x': 0,
				'y': -5
			}
		});
		//myFx.toTop();
		myFx.toElement('header');
	});
	
	new ScrollSpy({
		min: 1,
		max: 200000,
		onEnter: function(position,state,enters) {
			$$('.go-top').set('styles', {
				position: 'fixed',
				bottom: '25px',
				right: '10px',
				'z-index': '100'
			});
		},
		onLeave: function(position,state,leaves) {
			$$('.go-top').set('style', '');
		},
		container: window
	});
	
	if ($defined($$('.chat_login')))
	{
		$$('.chat_login').addEvent('click', function(e) {
			e.stop();
			
			Cubix.Popup.Show('500', '520');
		});
	}
});