<?php

class Mobile_ProfileController extends Zend_Controller_Action
{
	/**
	 * @var Model_Escort_Profile
	 */
	protected $profile;

	/**
	 * @var Model_EscortItem
	 */
	protected $escort;

	/**
	 * @var Zend_Session_Namespace
	 */
	protected $session;

	public static $linkHelper;
	
	public function init()
	{
		self::$linkHelper = $this->view->getHelper('GetLink');
		
		$this->_request->setParam('no_tidy', true);

		$anonym = array();

		$this->view->user = $this->user = Model_Users::getCurrent();
		if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}

		$this->view->layout()->setLayout('mobile-private-profile');
		$this->view->addScriptPath($this->view->getScriptPath('private'));

		$this->step = $this->_getParam('step'); 

		$escort_id = intval($this->_getParam('escort'));
		if ( $this->user->isAgency() && $escort_id > 1 ) {
			$this->agency = $agency = $this->user->getAgency();
			if ( ! $agency->hasEscort($escort_id) ) {
				return $this->_redirect($this->view->getLink('private-v2-escorts'));
			}

			$escorts = new Model_Escorts();
			$this->escort = $escorts->getById($escort_id);
		}
		elseif ( $this->user->isAgency() ) {
			$this->agency = $agency = $this->user->getAgency();
			$this->escort = new Model_EscortItem(array('id' => null));
		}
		else {
			$this->escort = $this->user->getEscort();
			if ( is_null($this->escort) ) $this->escort = new Model_EscortItem(array('id' => null));
		}

        
		$this->view->escort = $this->escort;
        
        if(isset($this->escort->is_suspicious) && $this->escort->is_suspicious){
            $this->_redirect($this->view->getLink('private'));
			return;
        }


		// Determine the mode depending on if user has profile
		$this->mode = $this->view->mode = ((isset($this->escort->id) && $this->escort->id && $this->escort->hasProfile() && $this->step != 'finish') ? 'update' : 'create');

		$session_hash = '';
		if ( $this->_request->session_hash ) {
			$session_hash = $this->view->session_hash = $this->_request->session_hash;
		}
		else {
			$session_hash = $this->view->session_hash = md5(microtime() * time());
		}

		$this->session = new Zend_Session_Namespace('profile-data-' . $session_hash);


		$this->profile = $this->view->profile = $this->escort->getProfile();
		$this->profile->setSession($this->session);
		$this->profile->load();

		// If we are in update mode, then we need to set the profile model
		// update in real-time mode instead of updating session
		if ( 'update' == $this->mode ) {
			$this->profile->setMode(Model_Escort_Profile::MODE_REALTIME);
		}

		$this->defines = $this->view->defines = Zend_Registry::get('defines');
		
		$config = Zend_Registry::get('escorts_config');
		$steps = array('biography', 'about-me', 'languages', 'working-cities', 'services', 'working-times');
				
		if ($config['profile']['rates'])
			$steps[] = 'prices';
		
		$steps[] = 'contact-info';
		$steps[] = 'gallery';
		$steps[] = 'finish';
		
		$this->steps = $steps;
	}

	protected $_c = 0;

	//protected $steps = array('biography', 'about-me', 'languages', 'working-cities', 'services', 'working-times', 'prices', 'contact-info', 'gallery', 'finish');
	protected $steps = array();
	protected $_posted = false;



    public function simpleAction(){
        
            $this->view->simple = true;

            $this->view->layout()->setLayout('simple-profile');
            $this->view->addScriptPath($this->view->getScriptPath('private'));

            $this->_helper->viewRenderer->setScriptAction("simple");

            

            $this->agency = $agency = $this->user->getAgency();
			$this->escort = new Model_EscortItem(array('id' => null));

            $this->_posted = $this->_request->isPost();
            $data = array();
            $errors = array();
            $is_error = false;
            $counter = 0;

            $this->view->step = 'biography';

            foreach($this->steps as $step){
                $method = str_replace('-', ' ', $step);
                $method = ucwords($method);
                $method = str_replace(' ', '', $method);
                $method[0] = strtolower($method[0]);
                
                if($step === 'finish' && $this->_posted && !$is_error){
                   
                     $this->finishAction();
                     $this->view->step = 'finish';
//                     $this->_helper->viewRenderer->setScriptAction('finish');
					 $tid = (int)$this->view->escort_id;
					 $this->_redirect($this->view->getLink('private-v2-profile', array('step' => 'gallery', 'escort' => $tid, 'show_success' => 1), true));
                }elseif($step !== 'finish'){
					if ( $step === 'gallery' ){
						continue;
					}
                     $this->{$method . 'Action'}();
                }else{
                    continue;
                }
                
                $tmpData = $this->view->data;
                if($tmpData){
                    $data = array_merge($data,$tmpData);
                }

                $tmpErrors = $this->view->errors;
                if($tmpErrors){
                    $errors = array_merge($errors,$tmpErrors);
                    $is_error = true;
                }
                $counter++;
            }

            $this->view->data = $data;
            
            if(!$this->_posted){
                $this->view->data['phone_number'] = $agency['phone'];
                $this->view->data['email'] = $agency['email'];
                $this->view->data['website'] = $agency['web'];
            }

            $this->view->errors = $errors;
        
    }


	public function indexAction()
	{
        
//        if($this->escort->is_suspicious){
//            $this->_response->setRedirect($this->view->getLink('signin'));
//			return;
//        }

	
//		var_Dump( $_POST );
//		exit;
		//$titles = array('biography', 'about_me', 'languages', 'working_cities', 'services', 'working_times', 'prices', 'contact_info', 'gallery', 'finish' );
		$config = Zend_Registry::get('escorts_config');
		$titles = array('biography', 'about_me', 'languages', 'working_cities', 'services', 'working_times');
				
		if ($config['profile']['rates'])
			$titles[] = 'prices';
		
		$titles[] = 'contact_info';
		$titles[] = 'gallery';
		$titles[] = 'finish';
		
		foreach ( $titles as $i => $key ) {
			$titles[$i] = __('pv2_tab_' . $key);
		}

		// If the mode is update, i.e. user already has a profile revision
		// we don't need the last step "Finish"
		if ( $this->mode == 'update' ) {
			unset($this->steps[count($this->steps) - 1]);
			unset($titles[count($titles) - 1]);
		}


		$this->view->steps = array_combine($this->steps, $titles);

	

		$this->_posted = $this->_request->isPost() && (
			($this->_getParam('then') == ':next' && $this->mode == 'create') ||
			( $this->mode == 'update' )
		);
		$result = $this->_do();

		$then = $this->_getParam('then');

		// If the user clicked the update button, and the result is successfull
		// and we don't need to switch to another tab, just display
		// a user friendly message
		if ( $this->_posted && true === $result && ! strlen($then) ) {
			$this->view->status = 'The section "' . $this->view->steps[$this->view->step] . '" has been successfully updated!';
		}
		
		/*$blockModel = new Model_BlockedCountries();
		
		if ( $blockModel->checkIp($escort->id) ) {
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			die;
			return;
		}*/

		if ( false === $result || ! strlen($then) ) { return; }

		switch (true) {
			case ($then == ':next'):
				$i = array_search($this->_getParam('step'), $this->steps);
				if ( $i == count($steps) - 1 ) return;
				
				$step = $this->steps[$i + 1];
				break;
			case ($then == ':back'):
				$i = array_search($this->_getParam('step'), $this->steps);
				if ( $i == 0 ) return;

				$step = $this->steps[$i - 1];

				break;
			default:
				if ( ! in_array($then, $this->steps) ) {
					return;
				}

				$step = $then;
		}

		$this->_setParam('step', $step);
		$this->_setParam('then', '');
		$this->_posted = false;

		$this->_do();
       

		
	}

	protected function _do()
	{
		$step = $this->_getParam('step');

		// User requested editting of profile without specifying
		// the step, i.e. if he/she clicks on profile icon in private are
		// load the profile from api into current session

		if ( ! in_array($step, $this->steps) ) {
			$this->profile->load(true);
			$step = reset($this->steps);
		}

		$this->view->step = $step;

		$method = str_replace('-', ' ', $step);
		$method = ucwords($method);
		$method = str_replace(' ', '', $method);
		$method[0] = strtolower($method[0]);

		$this->_helper->viewRenderer->setScriptAction($step);
		

		if ( $method == 'gallery' ){

			if ( $this->mode == 'create' ) {
				$this->finishAction();
				$tid = (int)$this->view->escort_id;
				$this->_redirect($this->view->getLink('private-v2-profile', array('step' => 'gallery', 'escort' => $tid, 'show_success' => 1), true));
			}
		}
		
		return $this->{$method . 'Action'}();
	}

	protected function _validateBiography($validator)
	{
		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'showname' => '',
			'slogan' => 'notags|special',
			'gender' => 'int-nz',
			'age' => 'int-nz',
			'ethnicity' => 'int-nz',
			'nationality_id' => 'int-nz',
			'home_city_id' => 'int-nz',
			'hair_color' => 'int-nz',
			'eye_color' => 'int-nz',
			'measure_units' => 'int-nz',
			'height' => 'int-nz',
			'weight' => 'int-nz',
			'dress_size' => '',
			'shoe_size' => 'int-nz',
			'bust' => 'int-nz',
			'waist' => 'int-nz',
			'hip' => 'int-nz',
			'cup_size' => '',
			'pubic_hair' => 'int-nz',
			'block_countries' => 'arr-int',
		));
		$data = $form->getData();

		$defines = Zend_Registry::get('defines');

		$escorts_model = new Model_Escorts();
		$data['showname'] = preg_replace('/(\s)+/','$1', trim($data['showname']));
		if ( ! strlen($data['showname']) ) {
			$validator->setError('showname', Cubix_I18n::translate('sys_error_required'));
		}
		elseif(mb_strlen($data['showname']) > 25 ){
				$validator->setError('showname', Cubix_I18n::translate('sys_error_showname_no_longer'));
		}
		elseif ( ! preg_match('#^[-_a-z0-9\s]+$#i', $data['showname']) ) {
			$validator->setError('showname', Cubix_I18n::translate('sys_error_must_contain'));
		}
		/*elseif ( $escorts_model->existsByShowname($data['showname'], $this->escort->getId()) ) {
			$validator->setError('showname', 'An escort with same showname exists');
		}*/

		if ( ! strlen($data['home_city_id']) ) {
//			$validator->setError('home_city_a', 'Home city is required');
			$data['home_city_id'] = null;
		}

		if ( ! strlen($data['slogan']) ) {
			$data['slogan'] = null;
		}
		else if( mb_strlen($data['slogan']) > 20 ) {
			$validator->setError('slogan', Cubix_I18n::translate('sys_error_slogan_text_must_be'));
		}
		
		else{
			$data['slogan'] = $validator->urlCleaner($data['slogan'], Cubix_Application::getById()->host);
		}
		
		if ( is_null($data['gender']) ) {
			$validator->setError('gender', 'required');
		}
		else if ( ! array_key_exists($data['gender'], $defines['gender_options']) ) {
			$validator->setError('gender', Cubix_I18n::translate('sys_error_gender_is_invalid'));
		}

		$age = $data['age']; unset($data['age']);
		if ( ! is_null($age) ) {
			$data['birth_date'] = strtotime('-' . $age . ' year');
		}

		if ( ! in_array($data['measure_units'], array(METRIC_SYSTEM, ROYAL_SYSTEM)) ) {
			$data['measure_units'] = METRIC_SYSTEM;
		}

		if ( ! in_array($data['cup_size'], $this->defines['breast_size_options']) ) {
			$data['cup_size'] = null;
		}

		if ( Cubix_Application::getById()->measure_units == ROYAL_SYSTEM ) {
			if ( $data['height'] ) {
				$data['height'] = Cubix_UnitsConverter::convert(stripslashes($data['height']), 'ftin', 'cm');
			}

			if ( $data['weight'] ) {
				$data['weight'] = Cubix_UnitsConverter::convert($data['weight'], 'lbs', 'kg');
			}
		}

		foreach ( $data['block_countries'] as $i => $zone_id ) {
			if ( $zone_id != '0' ) {
				$data['block_countries'][$i] = array('country_id' => $zone_id);
			}
			else {
				unset($data['block_countries'][$i]);
			}
		}


		$data['height'] = intval($data['height']);
		if ( ! $data['height'] ) $data['height'] = null;
		$data['weight'] = intval($data['weight']);
		if ( ! $data['weight'] ) $data['weight'] = null;

		return $data;
	}

	public function biographyAction()
	{
		

        $countyModel = new Model_Countries();
		$this->view->countries = $countyModel->getCountries();
		if ( $this->_posted ) {
			$validator = new Cubix_Validator();
			
			$this->view->data = $data = $this->_validateBiography($validator);


			$home_city_id = $this->view->data['home_city_id'];

			$home_city_country = '';
			if ( $home_city_id ) {
				$m_city = new Cubix_Geography_Cities();
				$home_city = $m_city->get($home_city_id);

				$m_country = new Cubix_Geography_Countries();
				$home_country = $m_country->get($home_city->country_id);

				$home_city_country = $home_city->{'title_' . $this->_request->lang_id} . ' (' . $home_country->{'title_' . $this->_request->lang_id} . ')';
			}
			$this->view->home_city_country = $home_city_country;

			if ( $this->_getParam('dont') ) {
				return false;
			}

			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();
				
				$this->view->errors = $status['msgs'];
				return false;
			}
			
			$this->_addSlogan($data['slogan'], $this->escort->id);
			$slogan = $this->_getSlogan($this->escort->id);
			$this->view->slogan = $slogan['text'];
			$this->view->slogan_status = $slogan['status'];
			
			return $this->profile->update($data, 'biography');
		}
		else {
			$this->view->data = $this->profile->getBiography();

			$home_city_id = $this->view->data['home_city_id'];

			$home_city_country = '';
			if ( $home_city_id ) {
				$m_city = new Cubix_Geography_Cities();
				$home_city = $m_city->get($home_city_id);

				$m_country = new Cubix_Geography_Countries();
				$home_country = $m_country->get($home_city->country_id);

				$home_city_country = $home_city->{'title_' . $this->_request->lang_id} . ' (' . $home_country->{'title_' . $this->_request->lang_id} . ')';
			}

			$this->view->data['block_countries'] = $countyModel->getBlockCountries($this->escort->id);
			
//			$this->view->countries = $countyModel->getCountries();
			
			$this->view->home_city_country = $home_city_country;
			$slogan = $this->_getSlogan($this->escort->id);
			$this->view->slogan = $slogan['text'];
			$this->view->slogan_status = $slogan['status'];
		}
	}

	protected function _addSlogan($slogan, $escort_id)
	{
		$config_system = Zend_Registry::get('system_config');
		
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$client->call('Escorts.addSlogan', array($slogan, $escort_id, $config_system['sloganApprovation']));

		return true;
	}

	protected function _getSlogan($escort_id)
	{
		if ( $escort_id ) {
			$client = Cubix_Api_XmlRpc_Client::getInstance();
			$slogan = $client->call('Escorts.getSlogan', array($escort_id));

			return $slogan;
		}
		else {
			return false;
		}
	}

	protected function _validateAboutMe($validator)
	{
		$blackListModel = new Model_BlacklistedWords();
		$form = new Cubix_Form_Data($this->_request);
		$fields = array(
			'is_smoking' => 'int-nz',
			'is_drinking' => 'int-nz',
			'characteristics' => 'notags|special'
		);

		foreach ( Cubix_I18n::getLangs(true) as $lng ) {
			$fields['about_' . $lng] = 'xss';
		}

		$form->setFields($fields);
		$data = $form->getData();

		$about_me_exists = false;
		foreach ( Cubix_I18n::getLangs(true) as $lng ) {
			if(strlen($data['about_' . $lng]) > 0){
			    $about_me_exists = true;
				break;
			}
		}
		if ( !$about_me_exists ) {
				$validator->setError('about_me_text', 'About me text required');
			}
		else{
			
				foreach ( Cubix_I18n::getLangs(true) as $lng ) {
					if($bl_words = $blackListModel->checkWords($data['about_' . $lng], Model_BlacklistedWords::BL_TYPE_ABOUT)) {
						foreach($bl_words as $bl_word){
							$pattern = '/' . preg_quote($bl_word, '/') . '/';
							$data['about_' . $lng] = preg_replace($pattern, '<abbr class = "black-listed" >' . $bl_word . '</abbr>', $data['about_' . $lng]);
						}
						
						$validator->setError('about_me_text', 'You can`t use word "'.$blackListModel->getWords() .'"');
						break;
					}
				else
					$data['about_' . $lng] = $validator->urlCleaner($data['about_' . $lng], Cubix_Application::getById()->host);
			}
		
			$data['characteristics'] = $validator->urlCleaner($data['characteristics'], Cubix_Application::getById()->host);
			}
			
			
		return $data;
	}

	public function aboutMeAction()
	{


		if ( $this->_posted ) {
			$validator = new Cubix_Validator();
			$blackListModel = new Model_BlacklistedWords();
			$this->view->data = $data = $this->_validateAboutMe($validator);
			$data['about_has_bl'] = 0;
			if ( ! $validator->isValid() ) {
				
				$status = $validator->getStatus();
				
				$this->view->errors = $status['msgs'];
				return false;
			}
			/*foreach ( Cubix_I18n::getLangs(true) as $lng ) {
				if($blackListModel->checkWords($data['about_' . $lng], Model_BlacklistedWords::BL_TYPE_ABOUT)){
					$data['about_has_bl'] = 1; BREAK;
				}
			}*/
			return $this->profile->update($data, 'about-me');
		}
		else {
			$this->view->data = $this->profile->getAboutMe();
		}
	}

	public function _validateLanguages($validator)
	{
		$defines = Zend_Registry::get('defines');
		
		$langs = $this->_getParam('langs', array());
		
		if ( ! is_array($langs) ) $langs = array();

		$data = array('langs' => array());

		$invalid_lang = false;
		foreach ( $langs as $lang_id => $level ) {
			$level = intval($level); if ( $level < 1 ) continue;
			$data['langs'][] = array('lang_id' => $lang_id, 'level' => $level);

			if ( ! array_key_exists($lang_id, $defines['language_options']) ) {
				$invalid_lang = true;
			}
		}

		if ( ! count($data['langs']) ) {
			$validator->setError('language_error', Cubix_I18n::translate('sys_error_language_required'));
		}
		else if ( $invalid_lang ) {
			$validator->setError('language_error', Cubix_I18n::translate('sys_error_lang_is_invalid'));
		}

		return $data;
	}

	public function languagesAction()
	{
		if ( $this->_posted ) {
			$validator = new Cubix_Validator();

			$this->view->data = $data = $this->_validateLanguages($validator);

			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];
				return false;
			}

			return $this->profile->update($data, 'languages');
		}
		else {
			$this->view->data = $this->profile->getLanguages();
		}
	}

	protected function _validateWorkingCities($validator)
	{
		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'country_id' => 'int-nz',
			'city_id' => 'int-nz',
			'cities' => 'int-nz',
			'cityzones' => 'arr-int',
			'zip' => '',
			'incall' => 'int',
			'incall_type' => 'int-nz',
			'incall_hotel_room' => 'int-nz',
			'incall_other' => 'notags|special',
			'outcall' => 'int',
			'outcall_type' => 'int-nz',
			'outcall_other' => 'notags|special'
		));
		$data = $form->getData();

		if ( $data['cities'] ) {
			$data['cities'] = array($data['cities']);
		}

		if ( ! is_array($data['cities']) ) $data['cities'] = array();

		$invalid_city = false;
		foreach ( $data['cities'] as $i => $city_id ) {
			$data['cities'][$i] = array('city_id' => $city_id);
			if ( ! $city_id ) {
				unset($data['cities'][$i]);
			}
			/*else if ( ! Cubix_Geography_Cities::isFromApplicationCountry($city_id) ) {
				$invalid_city = true;
				unset($data['cities'][$i]);
			}*/
		}

		// Anti-Hack: Ignore trailing cities
		$data['cities'] = array_slice($data['cities'], 0, Cubix_Application::getById()->max_working_cities);

		if ( ! is_array($data['cityzones']) ) $data['cityzones'] = array();

		foreach ( $data['cityzones'] as $i => $zone_id ) {
			if ( $zone_id != '0' ) {
				$data['cityzones'][$i] = array('city_zone_id' => $zone_id);
			}
			else {
				unset($data['cityzones'][$i]);
			}
		}

		if ( ! $data['incall'] ) {
			$data['incall_type'] = null;
			$data['incall_hotel_room'] = null;
			$data['incall_other'] = null;
			$data['zip'] = null;
		}
		else{
			if ( !$data['incall_type'] ) {
				$validator->setError('zip', Cubix_I18n::translate('sys_error_choose_suboption'));
				$data['incall_type'] = 999;
			}
		}

		if ( ! $data['outcall'] ) {
			$data['outcall_type'] = null;
			$data['outcall_other'] = null;
		}


		if ( $data['incall'] && ! strlen($data['zip']) ) {
//			$validator->setError('zip', 'zip code required');
			$data['zip'] = null;
		}
		unset($data['incall']);
		unset($data['outcall']);

		if ( ! is_null($data['incall_type']) && ! $validator->validateZipCode($data['zip'], false) ) {
//			$validator->setError('zip', 'Invalid zip code');
			$data['zip'] = null;
		}

		$data['zip'] = $validator->urlCleaner($data['zip'], Cubix_Application::getById()->host);
		$data['incall_other'] = $validator->urlCleaner($data['incall_other'], Cubix_Application::getById()->host);
		$data['outcall_other'] = $validator->urlCleaner($data['outcall_other'], Cubix_Application::getById()->host);

		if ( ! is_null($data['incall_type']) && $data['incall_type'] == 2 ) {
			if ( ! isset($data['incall_hotel_room']) ) {
				$validator->setError('incall_hotel_room', Cubix_I18n::translate('sys_error_choose_hotel_room'));
			}
		}

		if ( ! $data['city_id'] || ! count($data['cities']) ) {
			$validator->setError('country_id', Cubix_I18n::translate('sys_error_base_city_required'));
		}
		/*else if ( ! Cubix_Geography_Cities::isFromApplicationCountry($data['city_id']) ) {
			$validator->setError('country_id', 'Base city is not from ' . Cubix_Application::getById()->country_title);
		}
		else if ( $invalid_city ) {
			$validator->setError('country_id', 'Selected cities are not from ' . Cubix_Application::getById()->country_title);
		}*/

		if ( ! $data['country_id'] ) {
			$validator->setError('country_id', Cubix_I18n::translate('sys_error_required'));
		}
		/*else if ( $data['country_id'] != Cubix_Application::getById()->country_id ) {
			$validator->setError('country_id', 'invalid country selected');
		}*/

		return $data;
	}

	public function workingCitiesAction()
	{

		$countries = new Cubix_Geography_Countries();
		$this->view->countries = $countries->ajaxGetAll(false);

		$cities = new Cubix_Geography_Cities();

		$country_id = Cubix_Application::getById()->country_id;

		if ( $this->user ) {
			$data = $this->profile->getWorkingCities();
			if ( isset($data['city_id']) && $data['city_id'] ) {
				$city = $cities->get($data['city_id']);
				$country_id = $city->country_id;
			}
		}
		$region_cities = $this->view->cities = $cities->ajaxGetAll(null, $country_id);

		$regions = array();
		foreach ( $region_cities as $city ) {
			if ( ! isset($regions[$city->region_title]) ) {
				$regions[$city->region_title] = array();
			}

			$regions[$city->region_title][] = $city;
		}

		ksort($regions);
		$this->view->regions = $regions;

		if ( $this->_posted ) {
			$validator = new Cubix_Validator();

			$this->view->data = $data = $this->_validateWorkingCities($validator);

			$param_cities = array();
			if ( count($data['cities']) ) {
				foreach ( $data['cities'] as $city ) {
					$param_cities[] = $city['city_id'];
				}
			}

			$this->_request->setParam('ajax_cities', implode(',', $param_cities));

			$this->getCityzonesAction();
			$this->view->layout()->enableLayout();

			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];
				return false;
			}


			$this->update_data['working-cities'] = $data;

			if ( ! $this->simple_mode )
				return $this->profile->update($data, 'working-cities');
		}
		elseif ( $this->user ) {

			$data = $this->profile->getWorkingCities();

			$param_cities = array();
			if ( count($data['cities']) ) {
				foreach ( $data['cities'] as $city ) {
					$param_cities[] = $city['city_id'];
				}
			}

			$this->_request->setParam('ajax_cities', implode(',', $param_cities));

			$this->getCityzonesAction();
			$this->view->layout()->enableLayout();

			$data['country_id'] = $country_id;

			$this->view->data = $data;
		}
	}

	public function getCityzonesAction()
	{
		$this->view->layout()->disableLayout();

		$ajax = $this->_request->ajax;
		
		$cities = $this->_request->ajax_cities;
		$cities = trim($cities, ',');

		$cities = explode(',', $cities);

		$cz_model = new Cubix_Geography_Cityzones();

		$all_cityzones = array();
		if ( count($cities) ) {
			foreach( $cities as $city ) {
				$cityzones = $cz_model->ajaxGetAll($city);
				if ( count($cityzones) ) {
					$all_cityzones = array_merge($all_cityzones, $cityzones);
				}
			}
		}

		if ( /*isset($_SERVER['HTTP_X_REQUESTED_WITH']) ||*/ $ajax ) {
			die(json_encode($all_cityzones));
		}
		else {
			$this->view->all_cityzones = $all_cityzones;
		}
	}

	public function _validateServices($validator)
	{		
		$form = new Cubix_Form_Data($this->_request);
		$fields = array(
			'sex_orientation' => 'int-nz',
			'sex_availability' => '',
			'services' => 'arr-int',
			'service_prices' => 'arr-int',
			'service_currencies' => 'arr-int'
		);

		foreach ( Cubix_I18n::getLangs(true) as $lng ) {
			$fields['additional_service_' . $lng] = 'notags|special';
		}

		$form->setFields($fields);
		$data = $form->getData();

		if ( ! is_array($data['services']) ) $data['services'] = array();

		foreach ( $data['services'] as $i => $svc ) {
			$price = isset($data['service_prices'][$svc]) ? intval($data['service_prices'][$svc]) : null;
			if ( ! $price ) $price = null;

			$currency = isset($data['service_currencies'][$svc]) ? intval($data['service_currencies'][$svc]) : null;
			if ( ! $currency ) $currency = null;
			
			$data['services'][$i] = array('service_id' => $svc, 'price' => $price, 'currency_id' => $currency);
		}

		if ( ! is_array($data['sex_availability']) ) {
			$data['sex_availability'] = array();

			$validator->setError('services_offered_for_error', Cubix_I18n::translate('sys_error_at_least_one_required'));
		}

		foreach ( $data['sex_availability'] as $i => $opt ) {
			if ( ! isset($this->defines['sex_availability_options'][$opt]) ) {
				unset($data['sex_availability'][$i]);
			}
		}
		$data['sex_availability'] = count($data['sex_availability']) ?
			implode(',', $data['sex_availability']) : null;

		unset($data['service_prices']);
		unset($data['service_currencies']);
		
		foreach ( Cubix_I18n::getLangs(true) as $lng ) {
			$data['additional_service_' . $lng] = $validator->urlCleaner($data['additional_service_' . $lng], Cubix_Application::getById()->host);
		}
		
		return $data;
	}

	public function servicesAction()
	{
		

		if ( $this->_posted ) {
			$validator = new Cubix_Validator();

			$this->view->data = $data = $this->_validateServices($validator);

			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];
				return false;
			}
			
			return $this->profile->update($data, 'services');
		}
		else {
			$data = $this->profile->getServices();

			$this->view->data = $data;
		}
	}

	protected function _validateWorkingTimes($validator)
	{
		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'available_24_7' => 'int-nz',
			'night_escort' => 'int-nz',
			'day_index' => 'arr-int',
			'time_from' => 'arr-int',
			'time_from_m' => 'arr-int',
			'time_to' => 'arr-int',
			'time_to_m' => 'arr-int',
			'vac_date_from_day' => 'int-nz',
			'vac_date_from_month' => 'int-nz',
			'vac_date_from_year' => 'int-nz',
			'vac_date_to_day' => 'int-nz',
			'vac_date_to_month' => 'int-nz',
			'vac_date_to_year' => 'int-nz'

		));
		$data = $form->getData();

		$_data = array('available_24_7' => $data['available_24_7'], 'night_escort' => $data['night_escort'],'times' => array(),'vac_date_from' => null,'vac_date_to' => null );

		for ( $i = 1; $i <= 7; $i++ ) {
			if ( isset($data['day_index'][$i]) && isset($data['time_from'][$i]) && isset($data['time_from_m']) && isset($data['time_to'][$i]) && isset($data['time_to_m'][$i]) ) {
				$_data['times'][] = array(
					'day_index' => $i,
					'time_from' => $data['time_from'][$i],
					'time_from_m' => $data['time_from_m'][$i],
					'time_to' => $data['time_to'][$i],
					'time_to_m' => $data['time_to_m'][$i]
				);
			}
		}
		if(isset($data['vac_date_from_day']) || isset($data['vac_date_from_month']) || isset($data['vac_date_from_year']) || isset($data['vac_date_to_day']) || isset($data['vac_date_to_month']) || isset($data['vac_date_to_year'])){

			$date_from = $data['vac_date_from_year']."-".$data['vac_date_from_month']."-".$data['vac_date_from_day'];
			$date_to = $data['vac_date_to_year']."-".$data['vac_date_to_month']."-".$data['vac_date_to_day'];

			if( isset($data['vac_date_from_day']) && isset($data['vac_date_from_month']) && isset($data['vac_date_from_year']) && isset($data['vac_date_to_day']) && isset($data['vac_date_to_month']) && isset($data['vac_date_to_year'])){

				if ( strtotime($date_from) >= strtotime($date_to) )
				{
					$validator->setError('vacation', Cubix_I18n::translate('sys_error_invalid_date_interval'));
				}
				else if ( strtotime($date_to) < strtotime(date('Y-m-d')) )
				{
					$validator->setError('vacation', Cubix_I18n::translate('sys_error_invalid_date_interval'));
				}
			}
			else{
				$validator->setError('vacation', Cubix_I18n::translate('sys_error_invalid_date_interval'));
			}
			$_data['vac_date_from'] = $date_from;
			$_data['vac_date_to'] = $date_to;
		}

		return $_data;
	}
	
	public function workingTimesAction()
	{
		$vacation = new Model_EscortV2Item(array('id' =>$this->escort->id  ));
		$this->view->escort_id = $this->escort->id;
		if ( $this->_posted ) {
			$validator = new Cubix_Validator();

			$this->view->data = $data = $this->_validateWorkingTimes($validator);
			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];
				return false;
			}
			//$vacation->updateVacation($data['vac_date_from'], $data['vac_date_to']);
			return $this->profile->update($data, 'working-times');
		}
		else {

			//$vac = (array) $vacation->getVacation();
			$data = $this->profile->getWorkingTimes();
			//$data = array_merge($data,$vac);
			$this->view->data = $data;

		}
	}

	protected function _validatePrices($validator)
	{
		$rates = $this->_getParam('rates');

		if ( ! is_array($rates) ) {
			$rates = array();
		}

		$data = array('rates' => array());

		//$currencies = array_keys($this->defines['currencies']);
		$currencies = array_keys(Model_Currencies::getAllAssoc());
		$units = array_keys($this->defines['time_unit_options']);

		foreach ( $rates as $availability => $_rates ) {
			if ( $availability == 'incall' ) { $availability = 1; $types = array('additional-hour', 'overnight', 'dinner-date', 'weekend'); }
			elseif ( $availability == 'outcall' ) { $availability = 2; $types = array('additional-hour', 'overnight', 'dinner-date', 'weekend'); }
			else continue;

			foreach ( $_rates as $rate ) {
				if ( get_magic_quotes_gpc() ) $rate = stripslashes($rate);
				$rate = @json_decode($rate);

				if ( is_object($rate) ) $rate = (array) $rate;
				if ( $rate === false || ! is_array($rate) ) {
					continue;
				}

				// Case when rate is standart and some data are not valid
				if ( ! isset($rate['type']) && ( ! isset($rate['time']) || ! isset($rate['unit']) || ! isset($rate['price']) || ! isset($rate['currency']) ) ) {
					continue;
				}
				// Case when rate is a type of custom and some data are not valid
				elseif ( isset($rate['type']) && ( ! isset($rate['price']) || ! isset($rate['currency']) || ! in_array($rate['type'], $types) ) ) {
					continue;
				}

				// If price is invalid
				$price = intval($rate['price']);
				if ( $price <= 0 ) continue;

				// If currency is invalid
				$currency = intval($rate['currency']);
				if ( ! in_array($currency, $currencies) ) {
					continue;
				}

				// If rate is custom validate data add only type, price and currency fields
				if ( ! isset($rate['type']) ) {
					$time = intval($rate['time']);
					if ( $time <= 0 ) continue;

					$unit = intval($rate['unit']);
					if ( ! in_array($unit, $units) ) continue;

					$data['rates'][] = array('availability' => $availability, 'time' => $time, 'time_unit' => $unit, 'price' => $price, 'currency_id' => $currency);
				}
				// Otherwize add also time and time unit
				else {
					$data['rates'][] = array('availability' => $availability, 'type' => $rate['type'], 'price' => $price, 'currency_id' => $currency);
				}
			}
		}
		
		return $data;
	}


	public function	pricesAction()
	{
		$this->_request->setParam('no_tidy', true);
		
		if ( $this->_posted ) {
			$validator = new Cubix_Validator();

			$data = $this->_validatePrices($validator);
			$this->view->data = array('rates' => $this->profile->reconstructRates($data['rates']));

			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];
				return false;
			}
			
			return $this->profile->update($data, 'prices');
		}
		else {
			$data = $this->profile->getPrices();
			$this->view->data = $data;
		}
	}

	public function _validateContactInfo($validator)
	{
		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			//'phone_prefix' => '',
			'disable_phone_prefix' => 'int',
			'phone_number' => '',
			/*'phone_number_alt' => '',*/
			'phone_instr' => 'int-nz',
			'phone_instr_no_withheld' => 'int-nz',
			'phone_instr_other' => 'notags|special',
			'email' => '',
			'website' => '',
			'club_name' => 'notags|special',
			'street' => 'notags|special',
			'street_no' => 'notags|special',
			'address_additional_info' => 'notags|special',
			'display_address' => 'int',
			'fake_city_id' => 'int',
			'fake_zip' => ''
		));
		$data = $form->getData();
		$data['display_address'] = (bool) $data['display_address'];
		$data['contact_phone_parsed'] = null;
		/*if ( ! strlen($data['phone_prefix']) ) {
			$validator->setError('phone_prefix', Cubix_I18n::translate('sys_error_country_calling_code_required'));
		}*/
		if ( ! strlen($data['phone_number']) ) {
			$validator->setError('phone_number', Cubix_I18n::translate('sys_error_phone_required'));
		}
		elseif(preg_match("/^(\+|00)/", trim($data['phone_number'])) ) {
			$validator->setError('phone_number', Cubix_I18n::translate('sys_error_enter_phone_without_country_code'));
		}
		else if (! preg_match("/^[0-9\s\+\-\(\)]+$/i", $data['phone_number'])) {
			$validator->setError('phone_number', Cubix_I18n::translate('sys_error_invalid_phone_number'));
		}



		//list($country_id,$phone_prefix,$ndd_prefix) = explode('-',$data['phone_prefix']);
		//unset($data['phone_prefix']);

		//Swizerland data
		$country_id = 1;$phone_prefix = 41; $ndd_prefix = 0;
		$data['phone_country_id'] = intval($country_id);
		/* Added Grigor */
		$phone = $this->_parsePhoneNumber($data['phone_number']);
		$data['contact_phone_parsed'] = preg_replace('/^'.$ndd_prefix.'/', '', $phone);
		$data['contact_phone_parsed'] = '00'.intval($phone_prefix).$data['contact_phone_parsed'];
		$data['phone_exists'] = $data['contact_phone_parsed'];

		$agency_id = $this->user->isAgency() ? $this->agency->id : null;


		if($this->escort->id){
			//$phone = $this->_parsePhoneNumber($data['phone_number']);


			$client = Cubix_Api_XmlRpc_Client::getInstance();
			$results = $client->call('Escorts.existsByPhone', array($data['contact_phone_parsed'], $this->escort->id, $agency_id));
			$resCount = count($results);
			if(!$this->user->isAgency() AND $resCount > 0 ){
				$validator->setError('phone_number', Cubix_I18n::translate('sys_error_phone_already_exists'));
			}
		}
		/* Added Grigor */


//        $validator->setError('phone_number', 'You cannot use the same phone number');
//        exit;
//        if ( $phone == $data['phone_number'] ) {
//            $validator->setError('phone_number_alt', 'You cannot use the same phone number');
//        }
//        else {
//            $data['phone_number_alt'] = $phone;
//        }

		/*if ( strlen($data['phone_number_alt']) ) {
			$phone = $this->_parsePhoneNumber($data['phone_number_alt'], $this->escort->id);
			if ( false === $phone ) {
				$validator->setError('phone_number_alt', 'Invalid phone number');
			}
			elseif ( $phone == $data['phone_number'] ) {
				$validator->setError('phone_number_alt', 'You cannot use the same phone number');
			}
			else {
				$data['phone_number_alt'] = $phone;
			}
		}*/

		if ( ! in_array($data['phone_instr'], array_keys($this->defines['phone_instructions'])) ) {
			$data['phone_instr'] = null;
		}

		if ( ! is_null($data['phone_instr_no_withheld']) ) {
			$data['phone_instr_no_withheld'] = 1;
		}

		if ( ! is_null($data['phone_instr_other']) && $this->_hasPhoneNumber($data['phone_instr_other']) ) {
			$validator->setError('phone_instr_other', Cubix_I18n::translate('sys_error_invalid_other'));
		}

		if ( strlen($data['email']) && ! $this->_validateEmailAddress($data['email']) ) {
			$validator->setError('email', Cubix_I18n::translate('sys_error_invalid_email_address'));
		}

		if ( strlen($data['website']) && ! $this->_validateWebsiteUrl($data['website']) ) {
			$validator->setError('website', Cubix_I18n::translate('sys_error_invalid_url'));
		}

		if ( strlen($data['website'])){
			$client = Cubix_Api_XmlRpc_Client::getInstance();
			$old_website = $client->call('Escorts.getWebsite', array($this->escort->id));

			if ($old_website != $data['website']){
				$data['website_changed'] = 1;
			}
		}

		$data['phone_instr_other'] = $validator->urlCleaner($data['phone_instr_other'], Cubix_Application::getById()->host);
		$data['club_name'] = $validator->urlCleaner($data['club_name'], Cubix_Application::getById()->host);
		$data['street'] = $validator->urlCleaner($data['street'], Cubix_Application::getById()->host);
		$data['street_no'] = $validator->urlCleaner($data['street_no'], Cubix_Application::getById()->host);
		$data['address_additional_info'] = $validator->urlCleaner($data['address_additional_info'], Cubix_Application::getById()->host);

		return $data;
	}

	public function contactInfoAction()
	{

		$countyModel = new Model_Countries();
		$this->view->countries = $countyModel->getPhoneCountries();
		if ( $this->_posted ) {

			$validator = new Cubix_Validator();

			$data = $this->view->data = $this->_validateContactInfo($validator);
			$this->view->phone_prefix_id = $data[phone_country_id];

			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];
				return false;
			}

			$this->update_data['contact-info'] = $data;
			if ( ! $this->simple_mode )
				return $this->profile->update($data, 'contact-info');
		}
		elseif ( $this->user ) {
			$data = $this->profile->getContactInfo();
			$phone_prfixes = $countyModel->getPhonePrefixs();
			$this->view->phone_prefix_id = $data[phone_country_id];
			if($data[phone_number]){
				if(preg_match('/^(\+|00)/',trim($data[phone_number])))
				{
					$phone_prefix_id = NULL;
					$phone_number = preg_replace('/^(\+|00)/', '',trim($data[phone_number]));
					foreach($phone_prfixes as $prefix)
					{
						if(preg_match('/^('.$prefix->phone_prefix.')/',$phone_number))
						{
							$phone_prefix_id = $prefix->id;
							$data[phone_number] = preg_replace('/^('.$prefix->phone_prefix.')/', '',$phone_number);
							BREAK;
						}

					}
					$this->view->phone_prefix_id = $phone_prefix_id;
				}
			}
			$this->view->data = $data;
		}
	}

	public function galleryAction()
	{
//		$this->view->layout()->setLayout('private-v2');

		$escort_id = $this->view->escort_id;

		$this->view->user = $this->user;

		$client = new Cubix_Api_XmlRpc_Client();
		$this->view->is_message = false;
		if ( $this->_getParam('show_success') ){
			$this->view->is_message = true;
		}

		if ( $this->user->isEscort() ) {
			$escort = $this->user->getEscort();
			$escort_id = $escort->id;

//			if ( !intval($this->_getParam('escort')) ){
//				$this->view->is_edit = false;
//			}

		}
		else {

			$escort_id = intval($this->_getParam('escort'));
			
			if ( 1 > $escort_id ) die;

			if ( ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
				die;
			}

			$model = new Model_Escorts();
			$this->escort = $escort = $model->getById($escort_id);
		}

		

		$photos = $this->_loadPhotos();

		$photos_ids = array();
		foreach ( $photos as $photo ) {
			$photo_ids[] = intval($photo['id']);
		}

		$action = $this->_getParam('a');
		
		$photos_min_count = Cubix_Application::getById(Cubix_Application::getId())->min_photos;

		$this->view->is_private = $is_private = (bool) intval($this->_getParam('is_private'));

		

		if ( 'set-main' == $action || ! is_null($this->_getParam('set_main')) ) {
			$ids = $this->_getParam('photo_id');
			if ( ! is_array($ids) || ! count($ids) ) {
				return $this->view->actionError = Cubix_I18n::translate('sys_error_select_at_least_on_photo');
			}
			
			$rotate_type = Model_Escort_Photos::PHOTO_ROTATE_SELECTED;
			if(count($ids) == 1){
				$rotate_type = Model_Escort_Photos::PHOTO_ROTATE_SELECTED_ONE;
			}
			
			$photo_id = reset($ids);
			if ( ! in_array($photo_id, $photo_ids) ) {
				return $this->view->actionError = Cubix_I18n::translate('sys_error_invalid_id_photo');
			}
			$photo = new Model_Escort_PhotoItem(array('id' => $photo_id ,'escort_id' => $escort_id));
			$photo->setRotatePics($ids);
			$result = $photo->setMain();
			$client->call('Escorts.setPhotoRotateType', array($escort_id, $rotate_type));
			$escort->photo_rotate_type = $rotate_type;
			$this->_loadPhotos();
		}
		elseif ( 'delete' == $action || ! is_null($this->_getParam('delete')) ) {
			$ids = $this->_getParam('photo_id');

			if ( ! is_array($ids) || ! count($ids) ) {
				return $this->view->actionError = Cubix_I18n::translate('sys_error_select_at_least_on_photo');
			}

			foreach ( $ids as $id ) {
				if ( ! in_array($id, $photo_ids) ) {
					return $this->view->actionError = Cubix_I18n::translate('sys_error_invalid_id_photo');
				}
			}

			$photo = new Model_Escort_Photos();
			$result = $photo->remove($ids);

			$this->_loadPhotos();
		}
		elseif ( 'upload' == $action || ! is_null($this->_getParam('upload')) ) {
				$set_photo = false;
				$config = Zend_Registry::get('images_config');
				$new_photos = array();
				$upload_errors = array();
				$model = new Model_Escort_Photos();

				

				foreach ( $_FILES as $i => $file )
				{
					try {

						if ( $is_private ){
							if ( strpos($i, 'public_') === 0 ){
								continue;
							}
						}else{
							if ( strpos($i, 'private_') === 0 ){
								continue;
							}
						}

						if ( ! isset($file['name']) || ! strlen($file['name']) ) {
							continue;
						}
						else {
							$set_photo = true;
						}

						$img_ext = strtolower(@end(explode('.', $file[name])));
						if (!in_array( $img_ext , $config['allowedExts'])){
							throw new Exception(Cubix_I18n::translate('sys_error_upload_img_allowed_extensions', array('extensions' => implode(', ', $config['allowedExts']))), Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
						}
						
						/*$photo_count = $model->getEscortPhotoCount($escort_id);
						if ( $photo_count >= Model_Escort_Photos::MAX_PHOTOS_COUNT ){
							throw new Exception(Cubix_I18n::translate('sys_error_upload_img_count_too_much'), Cubix_Images::ERROR_IMAGE_COUNT_LIMIT_OVER);
						}*/
						// Save on remote storage
						$images = new Cubix_Images();
						$image = $images->save($file['tmp_name'], $escort->id, $escort->application_id, strtolower(@end(explode('.', $file['name']))));

						$image = new Cubix_Images_Entry($image);
						$image->setSize('sthumb');
						$image->setCatalogId($escort->id);
						$image_url = $images->getUrl($image);

						$image_size = getimagesize($file['tmp_name']);
						$is_portrait = 0;
						if ( $image_size ) {
							if ( $image_size[0] < $image_size[1] ) {
								$is_portrait = 1;
							}
						}
						
						$photo_arr = array(
							'escort_id' => $escort->id,
							'hash' => $image->getHash(),
							'ext' => $image->getExt(),
							'type' => $is_private ? ESCORT_PHOTO_TYPE_PRIVATE : ESCORT_PHOTO_TYPE_HARD,
							'is_portrait' => $is_portrait,
							'width' => $image_size[0],
							'height' => $image_size[1]
						);

						if ( $client->call('Escorts.isPhotoAutoApproval', array($escort_id)) ) {
							$photo_arr['is_approved'] = 1;
						}
						// commented because of approved pictures bug 
						/*else if ( $client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_NO_PROFILE))) ) {
							$photo_arr['is_approved'] = 1;
						}*/

						$photo = new Model_Escort_PhotoItem($photo_arr);

						
						$photo = $model->save($photo);

						$new_photos[] = $photo;
					} catch ( Exception $e ) {
						$upload_errors[$i] = $file['name'] . ' (' . $e->getMessage() . ')';
					}
					
			}

			if ( ! $set_photo ) {
				$this->view->uploadError = Cubix_I18n::translate('sys_error_select_photo');
			}

			$this->view->newPhotos = $new_photos;
			$this->view->uploadErrors = $upload_errors;
		}
		elseif ( 'set-adj' == $action ) {
			$photo_id = intval($this->_getParam('photo_id'));

			if ( ! in_array($photo_id, $photo_ids) ) {
				die(json_encode(array('error' => 'An error occured')));
			}

			$photo = new Model_Escort_PhotoItem(array(
				'id' => $photo_id
			));

			try {
				$hash = $photo->getHash();
				$result = array(
					'x' => intval($this->_getParam('x')),
					'y' => intval($this->_getParam('y')),
					'px' => floatval($this->_getParam('px')),
					'py' => floatval($this->_getParam('py'))
				);
				$photo->setCropArgs($result);

				// Crop All images
				$size_map = array(
					'backend_thumb' => array('width' => 150, 'height' => 205),
					'medium' => array('width' => 225, 'height' => 300),
					'thumb' => array('width' => 150, 'height' => 200),
					'nlthumb' => array('width' => 120, 'height' => 160),
					'sthumb' => array('width' => 76, 'height' => 103),
					'lvthumb' => array('width' => 75, 'height' => 100),
					'agency_p100' => array('width' => 90, 'height' => 120),
					't100p' => array('width' => 117, 'height' => 97)
				);
				$conf = Zend_Registry::get('images_config');

				get_headers($conf['remote']['url'] . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash);
				// echo $conf['remote']['url'] . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash;

				$catalog = $escort_id;
				$a = array();
				if ( is_numeric($catalog) ) {
					$parts = array();

					if ( strlen($catalog) > 2 ) {
						$parts[] = substr($catalog, 0, 2);
						$parts[] = substr($catalog, 2);
					}
					else {
						$parts[] = '_';
						$parts[] = $catalog;
					}
				}
				else if ( preg_match('#^([0-9]+)/(.+)$#', $catalog, $a) ) {
					array_shift($a);
					$catalog = $a[0];

					$parts = array();

					if ( strlen($catalog) > 2 ) {
						$parts[] = substr($catalog, 0, 2);
						$parts[] = substr($catalog, 2);
					}
					else {
						$parts[] = '_';
						$parts[] = $catalog;
					}

					$parts[] = $a[1];
				}

				$catalog = implode('/', $parts);

				foreach($size_map as $size => $sm) {
					get_headers($conf['remote']['url'] . "/" . Cubix_Application::getById()->host . "/" . $catalog . "/" . $hash . "_" . $size . ".jpg?args=" . $result['x'] . ":" . $result['y']);
				}
			}
			catch ( Exception $e ) {
				die(json_encode(array('error' => 'An error occured')));
			}

			die(json_encode(array('success' => true)));
		}
		elseif ( ! is_null($this->_getParam('make_private')) || ! is_null($this->_getParam('make_public')) ) {

			if ( ! is_null($this->_getParam('make_private')) ) {
				$type = 'private';
			} elseif ( ! is_null($this->_getParam('make_public')) ) {
				$type = 'public';
			} else {
				die;
			}

			$ids = $this->_getParam('photo_id');

			if ( ! is_array($ids) || ! count($ids) ) {
				return $this->view->actionError = Cubix_I18n::translate('sys_error_select_at_least_on_photo');
			}

			foreach ( $ids as $id ) {
				if ( ! in_array($id, $photo_ids) ) {
					return $this->view->actionError = Cubix_I18n::translate('sys_error_invalid_id_photo');
				}
			}

			foreach ( $ids as $id ) {
				$photo = new Model_Escort_PhotoItem(array('id' => $id));
				$photo->make($type == 'private' ? ESCORT_PHOTO_TYPE_PRIVATE : ESCORT_PHOTO_TYPE_HARD);
			}

			$this->_loadPhotos();
		}
		elseif ( 'sort' == $action ) {
			$ids = $this->_getParam('photo_id');

			if ( ! is_array($ids) || ! count($ids) ) {
				die(Cubix_I18n::translate('sys_error_select_at_least_on_photo'));
			}

			foreach ( $ids as $id ) {
				if ( ! in_array($id, $photo_ids) ) {
					die(Cubix_I18n::translate('sys_error_invalid_id_photo'));
				}
			}

			$model = new Model_Escort_Photos();
			$model->reorder($ids);

			die;
		}elseif ( $this->_posted ){
			$data = array();
			$this->_redirect($this->view->getLink('private'));
			return;
//			return $this->profile->update($data, 'gallery');
		}
		$this->view->escort = $escort;
	}

	private function _loadPhotos()
	{
		$photos =

		$public_photos = Cubix_Api::getInstance()->call('getEscortPhotosList', array($this->escort->id, false));
		$nil = null;
		$private_photos = Cubix_Api::getInstance()->call('getEscortPhotosList', array($this->escort->id, true));

		return $this->view->photos = array_merge($public_photos, $private_photos);
	}



	protected function _parsePhoneNumber($phone)
	{
		$phone = preg_replace('/[^0-9]+/', '', $phone);

		if ( ! strlen($phone) || ! is_numeric($phone) ) {
			return false;
		}

		/*if ( '00' != substr($phone, 0, 2) ) {
			if ( ! is_null($escort_id) ) {
				$iso = $this->profile->country_id;
				if ( $iso && isset($this->defines['dial_codes'][$iso]) ) {
					$phone = '00' . $this->defines['dial_codes'][$iso] . $phone;
				}
			}
		}*/

		return $phone;
	}

	protected function _hasPhoneNumber($string)
	{
		$string = preg_replace('/[^0-9]+/', '', $string);

		if ( strlen($string) > 8 ) {
			return true;
		}

		return false;
	}

	protected function _validateEmailAddress($email)
	{
		return Cubix_Validator::validateEmailAddress($email);
	}

	protected function _validateWebsiteUrl($url)
	{
		//return (bool) preg_match('/^((http|https):\/\/)?(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)(:(\d+))?\/?/i', $url);
		if (strtolower(substr($url, 0, 7)) != 'http://' && strtolower(substr($url, 0, 8)) != 'https://' && strtolower(substr($url, 0, 4)) != 'www.')
			return false;
		else
			return true;
	}

	public function finishAction()
	{

	
		$data = array();
		if ( $this->user->isAgency() ) {
			$data['agency_id'] = $this->agency->getId();
		}

		$data['user_id'] = $this->user->getId();
		
		// Send the data from session to API, a new revision of the profile will be created!
		$res = $this->profile->flush($data);
		
		if ( ! $res ) {
			$this->_redirect($this->view->getLink('private'));
		}

		$this->view->escort_id = $res;
	}

	public function ajaxChangeCityAction()
	{
		$this->view->layout()->disableLayout();


		$city_id = intval($this->_getParam('city_id'));
		$data = $this->profile->getWorkingCities();

		$model = new Cubix_Geography_Countries();
		if ( ! $model->hasCity($data['country_id'], $city_id) ) {
			die('Invalid city id');
		}

		$data['city_id'] = $city_id;
		foreach ( $data['cities'] as $i => $city ) {
			if ( $city_id == $city['city_id'] ) {
				$data['cities'][$i]['city_id'] = $city_id;
			}
		}


		
		$result = $this->profile->update($data, 'working-cities');

		$response = array('status' => 'error');

		if ( $result ) {
			$response['status'] = 'ok';
			$response['city'] = Cubix_Geography_Cities::getTitleById($city_id);
		}

		die(json_encode($response));
	}

	public function ajaxChangeZipAction()
	{
		$zip = $this->_getParam('zip');
		$data = $this->profile->getWorkingCities();

		if ( ! $data['incall_type'] ) {
			die('There is no need for zip code');
		}

		$validator = new Cubix_Validator();

		if ( ! $validator->validateZipCode($zip, true) ) {
			die(json_encode(array('status' => 'error')));
		}

		$data['zip'] = $zip;


		$result = $this->profile->update($data, 'working-cities');

		$response = array('status' => 'error');

		if ( $result ) {
			$response['status'] = 'ok';
			$response['zip'] = $zip;
		}

		die(json_encode($response));
	}
	
	public function gotdAction()
	{
		$this->view->layout()->setLayout('private-v2');
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$this->view->booked_days = $booked_days = $client->call('Billing.getGotdBookedDays');
		
		$cities = new Cubix_Geography_Cities();
		$country_id = Cubix_Application::getById()->country_id;
		
		$this->view->cities = $cities = $cities->ajaxGetAll(null, $country_id);
		
		$is_agency = false;
		if ( $this->user->isAgency() ) {
			$is_agency = true;
			
			$escorts = $this->agency->getEscortsForGotd();
			$this->view->escorts = $escorts;
			foreach( $escorts as $k => $v ) {
				$escorts[$v['id']] = $v;
				unset($escorts[$k]);
			}
			$this->view->escorts = $escorts;
		} else {
			$this->view->escort_package = $escort_package = $client->call('Escorts.checkIfHasActivePackage', array($this->escort->id));
		}
		
		$this->view->is_agency = $is_agency;
		
		$this->view->region_relations = $region_relations = array(
		);
		
		if ( $this->_request->isPost() ) {
			$errors = array();
			$data = array(
				'city_id'	=> $this->_request->city_id,
				'escort_id' => $this->_request->escort_id,
				'date'		=> $this->_request->date
			);
			$data['date'] = explode(',', $data['date']);
			$data['date'] = mktime(0,0,0,$data['date'][1], $data['date'][2], $data['date'][0]);
			
			if ( ! $data['city_id'] ) {
				$errors['city_id'] = Cubix_I18n::translate('city_required');
			} else {
				$city_also_check[] = $data['city_id'];
				if ( isset($region_relations[$data['city_id']]) ) {
					$city_also_check = $region_relations[$data['city_id']];
				}
			}
			
			$this->view->city_also_check = $city_also_check;
			
			if ( $is_agency ) {
				if ( ! $data['escort_id'] ) {
					$errors['escort_id'] = Cubix_I18n::translate('escort_required');
				}
			} else {
				$data['escort_id'] = $this->escort->id;
			}
			
			if ( ! $data['date'] ) {
				$errors['date'] = Cubix_I18n::translate('date_required');
			} elseif ( date('Y-m-d', $data['date']) < date('Y-m-d', time()) ) {
				$errors['date'] = Cubix_I18n::translate('please_select_in_the_future');
			}
		
			
			//Checking date to be not booked
			if ( $data['city_id'] && $data['date'] ) {
				foreach($city_also_check as $ct) {
					foreach($booked_days[$ct] as $dt) {
						if ( date('Y-m-d', $data['date']) == date('Y-m-d', strtotime($dt['date'])) ) {
							$errors['date'] = Cubix_I18n::translate('day_already_booked');
							break;
						}
					}
				}
			}
			
			if ( $data['city_id'] && $data['date'] ) {
				foreach($booked_days as $city_id => $dt) {
					foreach($dt as $d) {
						if ( date('Y-m-d', $data['date']) == date('Y-m-d', strtotime($d['date'])) && $data['escort_id'] == $d['escort_id'] ) {
							$errors['date'] = Cubix_I18n::translate('only_one_gotd_per_day');
						}
					}
				}
			}
			
			//Checking date to be in package active range
			if ( $data['escort_id'] && $data['date'] ) {
				if ( $is_agency ) {
					if ( $data['date'] < $escorts[$data['escort_id']]['date_activated'] || $data['date'] > $escorts[$data['escort_id']]['expiration_date']  ) {
						$errors['date'] = Cubix_I18n::translate('date_must_be_in_package_range');
					}
				} else {
					if ( $data['date'] < $escort_pacakge['date_activated'] || $data['date'] > $escort_package['expiration_date']  ) {
						$errors['date'] = Cubix_I18n::translate('date_must_be_in_package_range');
					}
				}
			}
			
			
			$this->view->data = $data;
			
			if ( count($errors) ) {
				$this->view->gotd_errors = $errors;
			} else {
				$result = $client->call('Billing.bookGotd', array($data['escort_id'], $data['city_id'], $data['date']));
				if ( $result ) {
					$product = $client->call('Billing.getProduct', array(15));
					
					$reference = 'gotd-' . $result;
					$epg_payment = new Model_EpgGateway();
					$token = $epg_payment->getTokenForAuth($reference, $product['price'], 'http://www.6annonce.com/online-billing/epg-gotd-response');
					$client->call('OnlineBilling.storeToken', array($token, $this->user->id));
					$this->_redirect($epg_payment->getPaymentGatewayUrl($token));
				}
			}
		}
	}
	
	public function gotdSuccessAction()
	{
		$this->view->layout()->setLayout('private-v2');
		$this->view->layout()->global_btn_back_url = self::$linkHelper->getLink('private-v2');
		$this->_helper->viewRenderer->setScriptAction('gotd-responses');
		$this->view->key = 'gotd_success_message';
	}
	
	public function gotdFailureAction()
	{
		$this->view->layout()->setLayout('private-v2');
		$this->view->layout()->global_btn_back_url = self::$linkHelper->getLink('private-v2-gotd');
		$this->_helper->viewRenderer->setScriptAction('gotd-responses');
		$this->view->key = 'gotd_failure_message';
	}
}
