<?php

class Mobile_EscortsController extends Zend_Controller_Action {
	
	public static $linkHelper;
	
	public function init() {
		
		$this->_helper->layout->setLayout('mobile-index');

		$_SESSION['request_url'] = $_SERVER['REQUEST_URI'];
		self::$linkHelper = $this->view->getHelper('GetLink'); 
		//die('Mobile version is temporary unavailable');
	}
	
	public function ajaxBubbleAction()
	{
		$this->view->layout()->disableLayout();
		
		$region = $this->_request->r;
		$page = intval($this->_getParam('page', 1));
		
		$per_page = 1; //intval($this->_getParam('per_page', 1));
		if ( $page < 1 ) $page = 1;
		
		$cache = Zend_Registry::get('cache');
		
		$cache_key = 'mobile_widget_bubble_texts_page_' . $page . '_' . $region;
		if ( ! $bubbles = $cache->load($cache_key) ) {
			try {
				$client = Cubix_Api_XmlRpc_Client::getInstance();
				$bubbles = $client->call('Escorts.getBubbleTexts', array($page, $per_page, $region));
				
			}
			catch ( Exception $e ) {
				$bubbles = array('texts' => array(), 'count' => 0);
			}

			foreach ( $bubbles['texts'] as &$text ) {
				$text = new Model_EscortV2Item($text);
			}

			$cache->save($bubbles, $cache_key, array(), 300);
			
		}
		//var_dump($bubbles);
		$this->view->per_page = $per_page;
		$this->view->page = $page;
		$this->view->bubbles = $bubbles;
		
		$this->view->region = $region;
	}

	public function indexAction() 
	{
		// <editor-fold defaultstate="collapsed" desc="Redirect to new URLS">
		$route_name = Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName();
		$redirect_params = array();
		if ( $route_name == 'mobile-escorts' || $route_name == 'mobile-escorts-def' ) {
			$params = explode('/', $this->_getParam('req'));
			foreach($params as $param) {
				$param = explode('_', $param);
				if ( count($param) > 1 ) {
					switch($param[0]) {
						case 'region' :
							$redirect_params['region'] = $param[1];
							break;
						case 'city' :
							$redirect_params['city'] = $param[2];
							$m = new Cubix_Geography_Cities();
							$city = $m->getBySlug($redirect_params['city']);
							if ( ! $city ) {
								$this->_redirect('/', array('code' => 301));
								return;
							}
							$redirect_params['city'] = $city->city_url_slug;
							$redirect_params['city_id']	= $city->id;
							break;
					}
				}
			}
			$this->_redirect(self::$linkHelper->getLink('escorts', $redirect_params, true), array('code' => 301));
		} else {
			$city = Model_Countries::getCityById($this->_getParam('city_id'));
			if ( $this->_getParam('city') != $city->url_slug ) {
				$redirect_params = array(
					'city'	=> $city->url_slug,
					'city_id'	=> $city->id
				);
				$this->_redirect(self::$linkHelper->getLink('escorts', $redirect_params, true), array('code' => 301));
			}
		}
		
		// </editor-fold>
		
		
		$lng = Cubix_I18n::getLang();
		
		$req = $this->_getParam('req');
		$config = Zend_Registry::get('mobile_config');

		// Scroll Spy
		if ( $this->_request->ajax ) {
			$this->view->spy = true;
			$this->view->layout()->disableLayout();
		}
		
		if (preg_match('#/([0-9]+)$#', $req, $a)) {
			$a[1] = intval($a[1]);
			$req = preg_replace('#/[0-9]+$#', '/page_' . $a[1], $req);
		}



		$req = explode('/', $req);

		$params = array(
			'sort' => 'random',
			'filter' => array(array('field' => 'girls', 'value' => array())),
			'page' => 1
		);


		if ( $this->_getParam('city_id') ) {
			$params['filter'][] = array('field' => 'city_id', 'value' => $this->_getParam('city_id'));
			$params['city_id'] = $this->_getParam('city_id');
		}

		$static_page_key = 'main';

		foreach ($req as $r) {
			if (!strlen($r))
				continue;
			$param = explode('_', $r);
			if (count($param) < 2) {
				switch ($r) {
					case 'nuove':
						//$params['filter'][] = array('field' => 'new_arrivals', 'value' => array());
						$static_page_key = 'nuove';
						$param = array('sort', 'newest');
						break;
					case 'independantes':
						$static_page_key = 'independantes';
						$params['filter'][] = array('field' => 'independantes', 'value' => array());
						continue;
					case 'regular':
						$static_page_key = 'regular';
						$params['filter'][] = array('field' => 'regular', 'value' => array());
						continue;
					case 'agence':
						$static_page_key = 'agence';
						$params['filter'][] = array('field' => 'agence', 'value' => array());
						continue;
					case 'boys':
						$static_page_key = 'boys';
						$params['filter'][0] = array('field' => 'boys', 'value' => array());
						continue;
					case 'trans':
						$static_page_key = 'trans';
						$params['filter'][0] = array('field' => 'trans', 'value' => array());
						continue;
					case 'citytours':
						$static_page_key = 'citytours';
						$this->view->is_tour = $is_tour = true;
						$params['filter'][] = array('field' => 'tours', 'value' => array());
						continue;
					case 'upcomingtours':
						$static_page_key = 'upcomingtours';
						$this->view->is_tour = $is_tour = true;
						$this->view->is_upcomingtour = true;
						$upcoming_tours = true;
						$params['filter'][] = array('field' => 'upcomingtours', 'value' => array());
						continue;
					/* 					case 'blank.html':
					  $this->_forward('blank-html', 'redirect');
					  return; */
					default:
						$this->_response->setRawHeader('HTTP/1.1 404 Not Found');
						$this->_forward('error', 'error', null, array('error_msg' => '404 Page Not Found. Please try the following links instead.'));
						return;
				}
			}

			$param_name = $param[0];
			array_shift($param);
	
			
		
			$this->view->static_page_key = $static_page_key;

			if ( $static_page_key == 'nuove' ) {
				$this->_forward('new-list');
				return;
			}

			switch ($param_name) {
				case 'filter':
					$has_filter = true;
					/* >>> For nested menu */
					$selected_item = $menus['filter']->getByValue(implode('_', $param));
					if (!is_null($selected_item)) {
						$menus['filter']->setSelected($selected_item);
					}
					/* <<< */

					$field = reset($param);
					array_shift($param);

					$value = array_slice($param, 0, 2);

					$params['filter'][] = array('field' => $field, 'value' => $value, 'main' => TRUE);
					break;
				case 'page':
					$page = intval(reset($param));

					if ($page < 1) {
						$page = 1;
					}


					$params['page'] = $page;
					break;
				case 'sort':
					$params['sort'] = reset($param);

					$selected_item = $menus['sort']->getByValue($params['sort']);
					if (!is_null($selected_item)) {
						$menus['sort']->setSelected($selected_item);
					}
					$has_filter = true;
					break;
				case 'region':
				case 'state':
					//$params['country'] = $param[0];
					//$params['region'] = $param[0];
					$this->_request->setParam('region', $param[0]);
					$params['filter'][] = array('field' => 'region', 'value' => $param[0]);
					break;
				case 'city':
				case 'zone':
					$params[$param_name] = $param[1];
					$params['filter'][] = array('field' => $param_name, 'value' => array($param[1]));
					break;
				case 'fcity':
					$params[$param_name] = $param[0];
					$params['filter'][] = array('field' => $param_name, 'value' => array($param[0]));
					break;
				case 'name':
					$has_filter = true;
					$params['filter'][] = array('field' => 'name', 'value' => array('%' . $param[0] . '%'));
					break;
				default:
					if (!in_array($param_name, array('nuove', 'independantes', 'regular', 'agence', 'boys', 'trans', 'citytours', 'upcomingtours'))) {
						$this->_response->setRawHeader('HTTP/1.1 404 Not Found');
						$this->_forward('error', 'error', null, array('error_msg' => '404 Page Not Found. Please try the following links instead.'));
						return;
					}
			}

		
		
		}
		

		$filter_params = array(
			'order' => 'e.date_registered DESC',
			'limit' => array('page' => 1),
			'filter' => array()
		);


		$filter_map = array(
			'verified' => 'e.verified_status = 2',
			'french' => 'e.nationality_id = 15',
			'age' => 'e.age < ? AND e.age > ?',
			'ethnic' => 'e.ethnicity = ?',
			'height' => 'e.height < ? AND e.height > ?',
			'weight' => 'e.weight < ? AND e.weight > ?',
			'cup-size' => 'e.cup_size = ?',
			'hair-color' => 'e.hair_color = ?',
			'eye-color' => 'e.eye_color = ?',
			'dress-size' => 'e.dress_size = ?',
			'shoe-size' => 'e.shoe_size = ?',
			'available-for-incall' => 'e.incall_type IS NOT NULL',
			'available-for-outcall' => 'e.outcall_type IS NOT NULL',
			'service-for' => 'FIND_IN_SET(?, e.sex_availability)',
			'smoker' => 'e.is_smoking = ?',
			'language' => 'FIND_IN_SET(?, e.languages)',
			'now-open' => 'e.is_now_open',
			'region' => 'r.slug = ?',
			'city' => 'ct.slug = ?',
			'fcity'	=> 'fct.slug = ?',
			'cityzone' => 'c.id = ?',
			'zone' => 'cz.slug = ?',
			'name' => 'e.showname LIKE ?',
			'new_arrivals' => 'e.gender = ' . GENDER_FEMALE,
			'independantes' => 'eic.is_agency = 0 AND eic.gender = ' . GENDER_FEMALE,
			'agence' => 'eic.is_agency = 1 AND eic.gender = ' . GENDER_FEMALE,
			'boys' => 'eic.gender = ' . GENDER_MALE,
			'trans' => 'eic.gender = ' . GENDER_TRANS,
			'girls' => 'eic.gender = ' . GENDER_FEMALE,
			'tours' => 'eic.is_tour = 1',
			'upcomingtours' => 'eic.is_upcoming = 1'
		);


		foreach ($params['filter'] as $i => $filter) {

			if (!isset($filter_map[$filter['field']]))
				continue;

			$value = $filter['value'];

			if (isset($filter['main'])) {
				if (isset($selected_filter->internal_value)) {
					$value = $selected_filter->internal_value;
				} elseif (!is_null($item = $menus['filter']->getByValue($filter['field'] . ( (isset($value[0]) && $value[0]) ? '_' . $value[0] : '')))) {
					$value = $item->internal_value;
				}
			}

			$filter_params['filter'][$filter_map[$filter['field']]] = $value;
		}


		$page = intval($params['page']);



		if ($page == 0) {
			$page = 1;
		}

		$filter_params['limit']['page'] = $page;
		$count = 0;

		if (isset($params['city_id'])) {
			$this->view->city = Model_Countries::getCityById($params['city_id']);//$params['city'];
			$m_c = new Model_Cities();
			$this->view->city_data = $m_c->getById($params['city_id']);
			$this->view->paging_city_slug = $params['city'];
			$filter_params['filter']['r.slug = ?'] = $this->view->city_data->region_slug;
			$filter_params['filter']['ct.id = ?'] = $this->view->city_data->id;
		}
		
		if (isset($params['fcity'])) {
			$f_city = Model_Cities::getFakeCityBySlug($params['fcity']);
			$this->view->city = $f_city->title;
		}
		

		$this->view->is_main_page = $is_main_page = ( ! isset($params['city_id']) && ! isset($params['city']) && ! isset($params['fcity']) && ! isset($params['zone']) && ! isset($params['region']) );

		$s_config = Zend_Registry::get('system_config');
			
		// If we are on main premium spot forward to corresponding action
		

		if ( $is_main_page ) {
			
			$regions = new Cubix_Geography_Regions();
			$regions = $regions->getRegionsByCountryId(Cubix_Application::getById()->country_id, $this->_request->region);
			
			$this->view->regions = $regions;
			//$gender = GENDER_FEMALE;
			$gender = null;
			$is_agency = null;

			$this->view->total_count = Model_Statistics::getTotalCount($gender, $is_agency, null, false);


			$region_slug = $this->_request->region;
			
			$region = new Cubix_Geography_Regions();
			$region = $region->getRegionBySlug($region_slug);

			$where = null;
			
			if ( $this->_getParam('q') ){
				$query = ltrim($this->_getParam('q'), '/');
				$translit_query = Model_Statistics::translit($query);
				$where = array(
					'fct.slug LIKE ? OR fct.title_' . $lng . ' LIKE ?' => array('%'.$translit_query.'%', '%'.$query.'%')
				);
				
				$fake_cities = Model_Statistics::getFakeCitiesForMobile($region->id, $gender, $is_agency, null, false, $where, null);

				if ( count($fake_cities) == 1 ) {
					$link = $this->view->getLink('escorts', array('fcity' => $fake_cities[0]->fake_city_slug, 'filter' => null, 'page' => null));
					$this->_redirect($link);
				} else {
					$all_cities = $fake_cities;
				}
				
			} else {
				$gender = GENDER_FEMALE;
				$all_cities = Model_Statistics::getCities($region->id, $gender, $is_agency, null, false, $where, null);
			}
			
			$this->view->all_cities = $all_cities;
			$this->view->region_slug = $region_slug;
			$this->_helper->viewRenderer->setScriptAction("main-page");
		} else {
			$escorts = Model_Escort_List::getFiltered($filter_params['filter'], $params['sort'], $filter_params['limit']['page'], /*$config['escorts']['perPage']*/10000, $count, true, $this->view->city_data->region_slug, true);

			$this->view->params = $params;

			$this->view->escorts = $escorts;

			if ( count( $escorts ) > 0 ){
				// <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
				$sess_name = "prev_next";
				$sid = 'sedcard_paging_' . Cubix_Application::getId();
				$sess = new Zend_Session_Namespace($sid);

				$sess->{$sess_name} = array();
				foreach ( $escorts as $escort ) {
					//$sess->{$sess_name}[] = $escort->showname;
					$sess->{$sess_name}[] = $escort->id;
				}
				// </editor-fold>
			}

			$this->view->page = $page;
			$this->view->count = $count;
			$this->view->is_map_list = false;

			if (!isset($this->_request->map))
				$this->_helper->viewRenderer->setScriptAction("escort-list");
			else
			{
				$this->_helper->viewRenderer->setScriptAction("escort-list-map");
				$arr = array();
				$ids = array();
				
				foreach ($escorts as $e)
				{
					if ($e->latitude && $e->longitude)
					{
						$e_item = new Model_Escort_PhotoItem($e);
						
						$arr[$e->id] = array(
							'escort_id' => $e->id,
							'latitude' => $e->latitude,
							'longitude' => $e->longitude,
							'showname' => $e->showname,
							'url' => $this->view->getLink('profile', array('showname' => $e->showname, 'escort_id' => $e->id)),
							'photo' => $e_item->getUrl('w68')
						);
						
						$ids[] = $e->id;
					}
				}
				
				if (count($ids) > 0)
				{
					$ids_str = implode(',', $ids);
					
					$m = new Model_EscortsV2();
					$profiles = $m->getProfiles($ids_str);
					
					foreach ($profiles as $p)
					{
						$data = (array)unserialize($p->data);
						
						$arr[$p->escort_id]['city_title'] = $data['city_title_en'] ? $data['city_title_en'] : '';
						$arr[$p->escort_id]['zip'] = $data['zip'] ? $data['zip'] : '';
						$arr[$p->escort_id]['address'] = $data['street'] ? $data['street'] . ' ' . $data['street_no'] : '';
					}
				}
				
				$this->view->escorts = array_values($arr);
				$this->view->is_map_list = true;
			}
		}	
	}

	public function clubDirectoryAction()
	{
		$this->_helper->layout->setLayout('mobile-cd');
		$this->view->is_map = false;
		
		$lng = Cubix_I18n::getLang();
		
		$conf = Zend_Registry::get('agencies_config');
		$this->view->perPage = $perPage = 30;
		$this->view->page = $page = intval($this->_request->getParam('page', 1));

		// Scroll Spy
		if ( $this->_request->ajax ) {
			$this->view->spy = true;
			$this->view->layout()->disableLayout();
			$this->view->first = $this->_request->getParam('first', false);
			$this->view->lat = $lat = $this->_request->getParam('lat', 0);
			$this->view->lon = $lon = $this->_request->getParam('lon', 0);
		}
		
		$m = new Model_ClubDirectory();
		$coord = array();
		
		if ( $lat && $lon ) {
			$coord = array('lat' => $lat, 'lon' => $lon);
		}
		$search = null;
		$filter = array('distance', 'working_girl');
		$escorts = $m->getList($filter, $search, $coord, $page, $perPage, $count, $area, $city);
		$this->view->escorts = $escorts['distance'];
		$this->view->count = $count;
		
		$this->_helper->viewRenderer->setScriptAction("club-directory");
	}
	
	public function clubDirectoryMapAction()
	{
		$this->_helper->layout->setLayout('mobile-cd');		
		$this->view->is_map = true;
		
		if ( $this->_request->ajax ) {
			$this->view->layout()->disableLayout();
			$m = new Model_ClubDirectory();
				
			$filter = array();
			$search = null;

			$filter = $this->_request->getParam('filter', array('working_girl', 1, 3, 2, 4));

			$area = $this->_request->area;
			$city = $this->_request->city;

			$this->view->all_cities = 1;

			$map_list = $m->getMapList($filter, $search, $area, $city);

			if ( count($map_list) ) {
				foreach ( $map_list as $k => $item ) {
					if ( $item->escort_id ) {
						$map_list[$k]->url = $this->view->getLink('profile', array('showname' => $item->showname, 'escort_id' => $item->escort_id));
						$e_item = new Model_Escort_PhotoItem($item);
						$map_list[$k]->photo = $e_item->getUrl('w68');
					} else {
						$map_list[$k]->url = $this->view->getLink('agency', array('slug' => $item->club_slug, 'id' => $item->agency_id));
					}
				}
			}

			echo json_encode(array('data' => $map_list));
			die;
		}
		
		
		$this->_helper->viewRenderer->setScriptAction("club-directory-map");
	}
	
	public function reviewsAction() {
		$lng = Cubix_I18n::getLang();

		

		$request = $this->_request;
		if ($request->ajax) {
			$this->view->layout()->disableLayout();
			$this->view->ajax = true;
		}
		$filter = array();

		if (isset($request->city_id) && $request->city_id) {
			$filter['city_id'] = intval($request->city_id);
			$this->view->city_id = intval($request->city_id);
		}

		if (isset($request->showname) && $request->showname) {
			$filter['showname'] = $request->showname;
			$this->view->showname = $request->showname;
		}

		if (isset($request->member_name) && $request->member_name) {
			$filter['member_name'] = $request->member_name;
			$this->view->member_name = $request->member_name;
		}

		$ord_field_v = 'creation_date';
		$ord_field = 'r.creation_date';
		$ord_dir_v = 'desc';
		$ord_dir = 'DESC';

		if (count($filter) > 0)
			$arg_filter = $filter;
		else
			$arg_filter = '-999';
		$config = Zend_Registry::get('reviews_config');
		if (isset($request->page) && intval($request->page) > 0) {
			$page = intval($request->page);
		}
		else
			$page = 1;

		is_array($arg_filter) ? $filter_str = implode('.', $arg_filter) : $filter_str = $arg_filter;

		$ret_revs = Model_Reviews::getReviews($page, $config['perPage'], $arg_filter, $ord_field, $ord_dir);
		$cities = Model_Reviews::getReviewsCities();


		$ret = array($ret_revs, $cities);

		list($ret_revs, $cities) = $ret;
		list($items, $count) = $ret_revs;
		$this->view->items = $items;
		$this->view->count = $count;
		$this->view->cities = $cities;
		$this->view->page = $page;
		$this->view->filter = $filter;

		$this->view->menuReviews = 1;
	}

	public function newListAction()
	{
//		$this->view->layout()->setLayout('main-simple');

		$per_page = 3;

		$this->view->per_page = $per_page;

		$model = new Model_EscortsV2();

		$req = $this->_getParam('req');
		$param = explode('/', $req);
		$page = explode("_", ( isset($param[3]) ) ? $param[3] : '' );

		$region_slug_parts = ( isset($param[1]) ) ? explode('_' ,$param[1]) : array();
		$region_slug = "deutschschweiz";
		if ( isset( $region_slug_parts[1] ) ){
			$region_slug = $region_slug_parts[1];
		}


//		$region_slug = $this->_request->region;
		$filter = array('e.gender = ?' => GENDER_FEMALE, 'r.slug = ?' => $region_slug);


	

		$page_num = 1;
		if ( isset($page[0]) && $page[0] ) {
			$page_num = $page[0];
		}
		$this->view->page = $page_num;
		$count = 0;
		//$escorts = Model_Escort_List::getFiltered($filter, 'newest', $page_num, 10, $count, 'new_list');

		$cache = Zend_Registry::get('cache');
		$new_cache_key = 'm_v2_new_escorts_list_' . Cubix_Application::getId() . '_lang_' . Cubix_I18n::getLang() . '_page_' . $page_num . $region_slug;
		$new_c_cache_key = 'm_v2_new_escorts_list_count_' . Cubix_Application::getId() . '_lang_' . Cubix_I18n::getLang() . '_page_' . $page_num . $region_slug;

		$count = $cache->load($new_c_cache_key);
		if ( ! $escorts = $cache->load($new_cache_key) ) {
			$escorts = Model_Escort_List::getFiltered($filter, 'newest', $page_num, $per_page, $count, 'new_list', $region_slug, true);

			if ( count($escorts) ) {
				foreach ( $escorts as $k => $escort ) {
					$cache_key = 'v2_' . $escort->showname . '_new_profile_' . Cubix_I18n::getLang() . '_page_' . $page_num . $region_slug;
					$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
					$escorts[$k]->profile = $model->get($escort->id, $cache_key);
				}
			}

			$cache->save($escorts, $new_cache_key, array());

			$cache->save($count, $new_c_cache_key, array());
		}

		$sess_name = 'new_list';
		$region_s = $region_slug;
		if ( $sess_name ) {

			$sid = 'sedcard_paging_' . Cubix_Application::getId() . '_' . $region_s;

			$sess = new Zend_Session_Namespace($sid);

			$sess->{$sess_name} = array();
			foreach ( $escorts as $escort ) {
				$sess->{$sess_name}[] = $escort->showname;
			}
			//var_dump($sess->{$sess_name});
		}

		$this->view->count = $count;
		$this->view->escorts = $escorts;

		$this->view->menuNew = 1;

		$this->view->new_list_cache_key = 'new_list_' . $cache_key;
	}

}
