<?php

class MembersController extends Zend_Controller_Action
{
	public function init()
	{
		$this->view->layout()->setLayout('main-simple');
	}
	public function indexAction()
	{
		$username = $this->_request->username;
		$model = new Model_Members();
		$this->view->member_info = $member = $model->getMembersInfoByUsername($username);
		$this->view->username = $username;
		$this->view->top10 = $model->getFavoritesTop10($member['user_id']);
		
		$this->view->user = $user = Model_Users::getCurrent();
		
		if ($user)
		{
			$reqs = $model->getFavoriteRequests($user->id, $member['user_id']);
			
			$r_arr = array();
			
			if ($reqs)
			{
				foreach ($reqs as $r)
				{
					$r_arr[$r['fav_id']] = $r['status'];
				}
			}
			
			$this->view->reqs = $r_arr;
		}
	}

	public function getMemberCommentsAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
        $user_id = $req->user_id;
        $page = $req->page;
		if ( ! $page )
			$page = 1;
        $per_page = 5;
        $count = 0;
		$model = new Model_Comments();
        $comments = $model->getCommentsByUserId($user_id,$page, $per_page,$count);
		
		$this->view->comments = $comments;
        $this->view->page = $page;
        $this->view->count = $count;
        $this->view->per_page = $per_page;
    }
}
