<?php

class PrivateV2Controller extends Zend_Controller_Action
{
	/**
	 * @var Model_Escort_Profile
	 */
	protected $profile;

	/**
	 * @var Model_EscortItem
	 */
	protected $escort;

	/**
	 * @var Model_AgencyItem
	 */
	protected $agency;

	/**
	 * @var Zend_Session_Namespace
	 */
	protected $session;

	public static $linkHelper;
	
	public function init()
	{
		
		self::$linkHelper = $this->view->getHelper('GetLink'); 
		
		$cache = Zend_Registry::get('cache');
		
		$this->_request->setParam('no_tidy', true);
		
		$anonym = array();

		$this->user = Model_Users::getCurrent();

		if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}

		$this->view->layout()->setLayout('private-v2');

		$this->defines = $this->view->defines = Zend_Registry::get('defines');

		$this->client = Cubix_Api::getInstance();

		$cache_key =  'v2_user_pva_' . $this->user->id;

		if ( $this->user->isAgency() ) {
			if ( ! $agency = $cache->load($cache_key) ) {
				$agency = $this->user->getAgency();
				$cache->save($agency, $cache_key, array(), 300);
			}

			$this->agency = $this->view->agency = $agency;

			//$this->agencyDashboardAction();
			$this->view->layout()->enableLayout();
		}
		else if ( $this->user->isEscort() ) {

			if ( ! $escort = $cache->load($cache_key) ) {
				$escort = $this->user->getEscort();
				$cache->save($escort, $cache_key, array(), 300);
			}

			$this->escort = $this->view->escort = $escort;

			/* Grigor Update */
			if ( $this->escort->status & Model_Escorts::ESCORT_STATUS_SUSPICIOUS_DELETED &&
				in_array($this->_request->getActionName(), array( 'tours', 'escort-reviews', 'settings', 'support', 'happy-hour', 'client-blacklist', 'plain-photos'  )) ) {
				$this->_redirect($this->view->getLink('private-v2'));
			}
			/* Grigor Update */
		}		

		$this->view->user = $this->user;
		
		$this->view->escort = $this->escort;
	}

	protected $_c = 0;

	protected $steps = array('biography', 'about-me', 'languages', 'working-cities', 'services', 'working-times', 'prices', 'contact-info', 'finish');
	protected $_posted = false;

	public function indexAction()
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		 
		$modelSupport = new Model_Support();
		$supportUnreadsCount = $modelSupport->getUnreadsCount($this->user->id);
		 
		if ( $this->user->isAgency() ) {
			$this->_helper->viewRenderer->setScriptAction('agency-index');

			$nav = array();
			$nav[] = array('id' => 'agency-profile', 'title' => __('pv2_btn_agency_profile'), 'link' => 'private-v2-agency-profile');
			//$nav[] = array('id' => 'photos', 'title' => __('pv2_btn_add_edit_photos'), 'link' => 'private-v2-agency-plain-photos');
			$nav[] = array('id' => 'profile', 'title' => __('pv2_btn_add_escort_profile'), 'link' => 'simple-profile');
			$nav[] = array('id' => 'models', 'title' => __('pv2_btn_manage_models'), 'link' => 'private-v2-escorts');
			
			$nav[] = array('id' => 'advertise-now', 'title' => __('pv2_btn_advertise_now'), 'link' => 'private-v2-advertise-now');
			
			$nav[] = array('id' => 'buy_your_ad', 'title' => __('pv2_btn_buy_your_ad'), 'link' => 'online-billing');
			
			$nav[] = array('id' => 'tours', 'title' => __('pv2_btn_city_tours'), 'link' => 'private-v2-tours');
			$nav[] = array('id' => 'verification', 'title' => __('pv2_btn_100p_verified'), 'link' => '100p-verify');
			$nav[] = array('id' => 'myreviews', 'title' => __('pv2_btn_my_escorts_reviews'), 'link' => 'private-v2-escorts-reviews');
			$nav[] = array('id' => 'premium', 'title' => __('pv2_btn_paid_girls'), 'link' => 'private-v2-premium');
			$nav[] = array('id' => 'support', 'title' => __('support'), 'link' => 'private-v2-support', 'supportUnreadsCount' => $supportUnreadsCount);
			$nav[] = array('id' => 'settings', 'title' => __('pv2_btn_settings'), 'link' => 'private-v2-settings');
			
			if ( $this->agency->isAddAreas() ) {
				$nav[] = array('id' => 'addareas', 'title' => __('pv2_change_add_areas'), 'link' => 'private-v2-add-areas');
			} else {
				$nav[] = array('id' => 'addareas-d', 'title' => __('pv2_change_add_areas'), 'link' => 'return_false');
			}
			
			$nav[] = array('id' => 'happy_hour', 'title' => __('pv2_btn_happy_hour'), 'link' => 'private-v2-happy-hour');
			$nav[] = array('id' => 'statistics', 'title' => __('pv2_btn_statistics'), 'link' => 'private-v2-statistics');

			//if ( $this->_request->bl )
			$nav[] = array('id' => 'client_blacklist', 'title' => __('pv2_btn_client_blacklist'), 'link' => 'private-v2-client-blacklist');
			//$nav[] = array('id' => 'faq', 'title' => 'FAQ', 'link' => 'private-v2-faq');
			$nav[] = array('id' => 'gotd', 'title' => __('pv2_btn_gotd'), 'link' => 'private-v2-gotd');
			$nav[] = array('id' => 'logout', 'title' => __('pv2_btn_logout'), 'link' => 'signout');
			
			

			$this->view->navigation = $nav;
		}
		else if ( $this->user->isEscort() ) {
			$this->_helper->viewRenderer->setScriptAction('escort-index');

			// Determine the mode depending on if user has profile
			$this->mode = (($this->user->hasProfile()) ? 'update' : 'create');

			$nav = array();

            if(!$this->escort->is_suspicious){
                $nav[] = array('id' => 'profile', 'title' => __('pv2_btn_add_edit_profile'), 'link' => 'simple-profile');
				$nav[] = array('id' => 'advertise-now', 'title' => __('pv2_btn_advertise_now'), 'link' => 'private-v2-advertise-now');
            }
			
			$this->view->escort_package = $escort_package = $client->call('Escorts.checkIfHasActivePackage', array($this->escort->id));
			
			if ( ($this->escort->status & Model_Escorts::ESCORT_STATUS_SUSPICIOUS_DELETED) == 0 ) {

				$nav[] = array('id' => 'photos', 'title' => __('pv2_btn_add_edit_photos'), 'link' => 'private-v2-plain-photos');
				$nav[] = array('id' => 'tours', 'title' => __('pv2_btn_set_tours'), 'link' => 'private-v2-tours');
			}
			$nav[] = array('id' => 'verification', 'title' => __('pv2_btn_100p_verified'), 'link' => '100p-verify');
			//$nav[] = array('id' => 'premium', 'title' => 'Go Premium', 'link' => 'private-v2-premium');

			if ( ($this->escort->status & Model_Escorts::ESCORT_STATUS_SUSPICIOUS_DELETED) == 0 ) {

				$nav[] = array('id' => 'buy_your_ad', 'title' => __('pv2_btn_buy_your_ad'), 'link' => 'online-billing');
				
				if (!Model_Reviews::hasProduct($this->escort->id, Model_EscortsV2::PRODUCT_NO_REVIEWS))
					$nav[] = array('id' => 'myreviews', 'title' => __('pv2_btn_my_reviews'), 'link' => 'private-v2-escort-reviews');

				//$nav[] = array('id' => 'sedcard_stats', 'title' => __('pv2_btn_sedcard_stats'), 'link' => 'private-v2-settings');
				//$nav[] = array('id' => 'my_video', 'title' => __('pv2_btn_my_video'), 'link' => 'private-v2-settings');

				$nav[] = array('id' => 'settings', 'title' => __('pv2_btn_settings'), 'link' => 'private-v2-settings');
				$nav[] = array('id' => 'support', 'title' => __('support'), 'link' => 'private-v2-support', 'supportUnreadsCount' => $supportUnreadsCount);

				if ( $this->escort->isAddAreas() ) {
					$nav[] = array('id' => 'addareas', 'title' => __('pv2_change_add_areas'), 'link' => 'private-v2-add-areas');
				} else {
					$nav[] = array('id' => 'addareas-d', 'title' => __('pv2_change_add_areas'), 'link' => 'return_false');
				}

				$nav[] = array('id' => 'happy_hour', 'title' => __('pv2_btn_happy_hour'), 'link' => 'private-v2-happy-hour');
				$nav[] = array('id' => 'statistics', 'title' => __('pv2_btn_statistics'), 'link' => 'private-v2-statistics');

				//if ( $this->_request->bl )
				$nav[] = array('id' => 'client_blacklist', 'title' => __('pv2_btn_client_blacklist'), 'link' => 'private-v2-client-blacklist');
				//$nav[] = array('id' => 'faq', 'title' => 'FAQ', 'link' => 'private-v2-faq');
			}
			if ( $escort_package ) {
				$nav[] = array('id' => 'gotd', 'title' => __('pv2_btn_gotd'), 'link' => 'private-v2-gotd');
			}
			$nav[] = array('id' => 'logout', 'title' => __('pv2_btn_logout'), 'link' => 'signout');
			
			
			$this->view->navigation = $nav;
		}
		else if ( $this->user->isMember() ) {

			$is_premium = false;
			if ( isset($this->user->member_data) ) {
				$is_premium = $this->user->member_data['is_premium'];
			}
			
			$modelM = new Model_Members();
			$request_count = $modelM->getFavPendingRequestsCount($this->user->id);

			$this->_helper->viewRenderer->setScriptAction('member-index');

			// Determine the mode depending on if user has profile
			$this->mode = (($this->user->hasProfile()) ? 'update' : 'create');

			$nav = array();
			$nav[] = array('id' => 'profile', 'title' => __('pv2_btn_member_profile'), 'link' => 'private-v2-member-profile');

			if ( ! $is_premium )
				$nav[] = array('id' => 'upgradepremium', 'title' => __('pv2_btn_up_to_premium'), 'link' => 'private-v2-upgrade-premium');

			$nav[] = array('id' => 'myfavourites', 'title' => __('pv2_btn_my_favorites_top10'), 'link' => 'favorites', 'request_count' => $request_count);
			$nav[] = array('id' => 'myalerts', 'title' => __('pv2_btn_my_alerts'), 'link' => 'alerts');
			$nav[] = array('id' => 'myreviews', 'title' => __('pv2_btn_my_reviews'), 'link' => 'private-v2-reviews');
			$nav[] = array('id' => 'settings', 'title' => __('pv2_btn_settings'), 'link' => 'private-v2-settings');
			$nav[] = array('id' => 'support', 'title' => __('support'), 'link' => 'private-v2-support', 'supportUnreadsCount' => $supportUnreadsCount);
			//$nav[] = array('id' => 'faq', 'title' => 'FAQ', 'link' => 'private-v2-faq');
			$nav[] = array('id' => 'logout', 'title' => __('pv2_btn_logout'), 'link' => 'signout');
			
			$this->view->navigation = $nav;
		}
	}
	
	public function switchAgencyAction()
	{
		$this->view->layout()->disableLayout();
		$this->view->linked_agencies = $this->agency->getLinkedAgencies();
	}
	
	public function bubbleAgencyEscortAction()
	{
		$this->view->layout()->disableLayout();
		$this->view->escorts = $this->agency->getEscorts();
	}

	private function dateDiff($date_start, $date_end)
	{
	    $difference = $date_end - $date_start;
        $minutes = floor(($difference / 3600) * 60); // 3600 seconds in an hour
		
		return $minutes;
	}

	const HH_STATUS_ACTIVE  = 1;
	const HH_STATUS_PENDING = 2;
	const HH_STATUS_EXPIRED = 3;

	private function makeDate($week_day, $h_from, $h_to)
	{
		$month = date('m', $week_day);
		$day = date('d', $week_day);
		$year = date('Y', $week_day);

		$day_to = $day;
		if ( $h_to < $h_from ) {
			$day_to += 1;
		}

		return array('hh_date_from' => mktime($h_from, 0, 0, $month, $day, $year), 'hh_date_to' => mktime($h_to, 0, 0, $month, $day_to, $year));
	}

	public function happyHourAction()
	{
		$req = $this->_request;
		$data = $req->data;

	
		if ( $req->isPost() ) {
			$validator = new Cubix_Validator();
			$def_currency = Model_Applications::getDefaultCurrencyTitle();
			foreach ( $data as $escort_id => $d ) {

				$hh_status = Cubix_Api::getInstance()->call('getHappyHourStatus', array($escort_id));

				if ( ! isset($data[$escort_id]['hh_save']) ) {
					$data[$escort_id]['hh_save'] = 0;
				}

				/*if ( ! strlen($d['hh_motto']) )
					$validator->setError('err_motto', 'Required');*/
				
				if ( $hh_status == self::HH_STATUS_PENDING )
				{
					if ( ! isset($d['hh_week_day']) || ! isset($d['hh_hour_from']) || ! isset($d['hh_hour_to']) ) {
						$validator->setError('err_date', 'Date is Required');
					}
					else {
						$dates = $this->makeDate($d['hh_week_day'], $d['hh_hour_from'], $d['hh_hour_to']);
						$data[$escort_id]['hh_date_from'] = $dates['hh_date_from'];
						$data[$escort_id]['hh_date_to'] = $dates['hh_date_to'];
						$d['hh_date_from'] = $dates['hh_date_from'];
						$d['hh_date_to'] = $dates['hh_date_to'];


						$diff_minutes = $this->dateDiff($d['hh_date_from'], $d['hh_date_to']);

						if ( ! strlen($d['hh_date_from']) || ! strlen($d['hh_date_to']) )
							$validator->setError('err_date', 'Date is Required');
						else if ( $d['hh_date_to'] < $d['hh_date_from'] )
							$validator->setError('err_date', 'Date until must be bigger then Date From');
						else if ( $d['hh_date_from'] < time() )
							$validator->setError('err_date', 'Date must be in the future');
						else if ( ! $this->user->isAgency() && $diff_minutes > 300 )
							$validator->setError('err_date', 'The duration of happy hour could not be grater then 5 hour !');
					}

					if ( $this->user->isAgency() ) {
						$diff_hour = $diff_minutes / 60;
						$used_time = Cubix_Api::getInstance()->call('getAgencyUsedHHHours', array($this->agency->id, $escort_id));
						
						if ( $used_time + $diff_hour > 10 )
							$validator->setError('err_date', 'The duration of happy hour could not be grater then ' . (10 - $used_time) . ' hour !');
					}

					
					if ( isset($d['outcall_rates']) && count($d['outcall_rates']) > 0 ) {
						$has_hh_price = false;
						foreach ( $d['outcall_rates'] as $rate_id => $ir ) {
							if( strlen($ir) ) {

								if ( $d['old_outcall_rates'][$rate_id] < $ir + 10) {
									$validator->setError('err_hh_hour_outcall', 'please make sure to specify smaller price at least 10 '.$def_currency);
								}

								if ( ! $has_hh_price ) {
									$has_hh_price = true;
								}
							}
						}
						if ( ! $has_hh_price )
							$validator->setError('err_hh_hour_outcall', 'At least one "Happy Hour Rate" is Required for Outcall');
					}

					if ( isset($d['incall_rates']) && count($d['incall_rates']) > 0 ) {
						$has_hh_price = false;
						foreach ( $d['incall_rates'] as $rate_id => $ir ) {
							if( strlen($ir) ) {
								
								if ( $d['old_incall_rates'][$rate_id] < $ir + 10 ) {
									$validator->setError('err_hh_hour', 'please make sure to specify smaller price at least 10 '.$def_currency);
								}

								if ( ! $has_hh_price ) {
									$has_hh_price = true;
								}
							}
						}

						if ( ! $has_hh_price )
							$validator->setError('err_hh_hour', 'At least one "Happy Hour Rate" is Required for Incall');
					}
				}
			}

			if ( $validator->isValid() ) {
				//print_r($data); die;
				Cubix_Api::getInstance()->call('setHappyHour', array($data));
			}
			
			die(json_encode($validator->getStatus()));
		}
		
		if ( $this->user->isAgency() ) {
			$this->view->escorts = $escorts_a = $this->agency->getEscortsPerPage(1, 1000, Model_Escorts::ESCORT_STATUS_ACTIVE);
		}			
	}

	public function happyHourResetAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		$escort_id = intval($req->escort_id);

		$escort_data = Cubix_Api::getInstance()->call('resetHappyHour', array($escort_id));
		die;
	}

	public function happyHourFormAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;

		Cubix_I18n::setLang($req->lang_id);
		$escort_id = intval($req->escort_id);

		$hh_status = $this->view->hh_status = Cubix_Api::getInstance()->call('getHappyHourStatus', array($escort_id));

		$escort_data = Cubix_Api::getInstance()->call('getHappyHour', array($escort_id));

		if ( $hh_status == Model_EscortsV2::HH_STATUS_EXPIRED ) {
			if ( $escort_data['agency_id'] ) {
				$dateDiff = strtotime('+ 7 days', $escort_data['hh_date_to']) - time();
			}
			else {
				$dateDiff = strtotime('+ 3 days', $escort_data['hh_date_to']) - time();
			}
			$days = floor($dateDiff / (60*60*24));
			$this->view->activate_after_days = $days;
		}
		
		$this->view->escort_data = $escort_data;
		$this->view->defines = Zend_Registry::get('defines');
	}


  /* Grigor Update */

    public function profileStatusAction(){
        if ( $this->user->isEscort() ) {
            
            $escort = $this->user->getEscort();
			$escort_id = $escort->id;

            $client = Cubix_Api_XmlRpc_Client::getInstance();

            $action = $this->_request->getParam('act');
            $client->call('Escorts.profileStatus', array($escort_id,$action));
            $this->_redirect($this->view->getLink('private-v2'));
            
        }
    }

    public function profileDeleteAction(){
        if ( $this->user->isEscort() ) {
            $this->view->layout()->disableLayout();
            $escort = $this->user->getEscort();
			$escort_id = $escort->id;
           
            if ( $this->_request->isPost() ) {

                $validator = new Cubix_Validator();

                $req = $this->_request;

				if (!trim($req->comment))
                    $validator->setError('comment', 'Required');

                if ( $validator->isValid() ) {
                    $leaving_reason = $req->comment;
                    $del_hash = Cubix_Salt::generateSalt($escort->showname);

                    $client = Cubix_Api_XmlRpc_Client::getInstance();
                    $client->call('Escorts.addDelData', array($escort_id,$del_hash,$leaving_reason));

                    Cubix_Email::sendTemplate('escort_delete', $this->user->email, array(
                        'del_hash' => $del_hash,
                        'email' => $this->user->email,
                        'showname' => $escort->showname
                    ));
                }

                
                die(json_encode($validator->getStatus()));
            }
        }
    }

    public function profileRestoreAction(){
        if ( $this->user->isEscort() ) {
            $this->view->layout()->disableLayout();
            $escort = $this->user->getEscort();
			$escort_id = $escort->id;

            $client = Cubix_Api_XmlRpc_Client::getInstance();
            $client->call('Escorts.restore', array($escort_id));

            $cache = Zend_Registry::get('cache');
            $cache_key =  'v2_user_pva_' . $this->user->id;
            $cache->remove($cache_key);

            $this->_response->setRedirect($this->view->getLink('private-v2'));
        }
    }

    public function confirmDeletionAction(){
        if ( $this->user->isEscort() ) {

            $escort = $this->user->getEscort();
			$escort_id = $escort->id;
            
            $hash = $this->_getParam('hash');
            $model = new Model_EscortsV2();

            $us = $model->getById($escort_id);
           
            if ( $model->checkhash($escort_id, $hash) ) {

                $client = Cubix_Api_XmlRpc_Client::getInstance();
                $res = $client->call('Escorts.deleteTemporary', array($escort_id));

                $res = json_decode($res);

                if( isset($res->success) && $res->success ){
                    $cache = Zend_Registry::get('cache');
                    $cache_key =  'v2_user_pva_' . $this->user->id;
                    $cache->remove($cache_key);

                    $this->_response->setRedirect($this->view->getLink('private-v2'));
                }
                
            }
        }
        $this->_response->setRedirect($this->view->getLink('private-v2'));
    }
 

//    public function restoreAction(){
//        if ( $this->user->isAgency() ) {
//			$escort_id = intval($this->_getParam('escort_id'));
//			if ( ! $this->agency->hasEscort($escort_id) ) {
//				die('Permission denied!');
//			}
//		}
//		elseif ( $this->user->isEscort() ) {
//			$escort_id = $this->escort->getId();
//		}
//		if ( ! $escort_id ) die;
//
//
//        $client = Cubix_Api_XmlRpc_Client::getInstance();
//
//        $client->call('Escorts.restore', array($escort_id));
//        exit;
//    }
//
//    public function undoDeletionAction(){
//        if ( $this->user->isEscort() ) {
//
//            $escort = $this->user->getEscort();
//			$escort_id = $escort->id;
//
//            $model = new Model_EscortsV2();
//
//            $us = $model->getById($escort_id);
//
//
//            $client = Cubix_Api_XmlRpc_Client::getInstance();
//            $client->call('Escorts.restore', array($escort_id));
//
//        }
//        $this->_response->setRedirect($this->view->getLink());
//    }
    /* Grigor Update */

	public function agencyDashboardAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;

		$agency_id = $this->agency['id'];
		if ( $req->agency_id ) {
			$agency_id = $req->agency_id;
		}

		$page = 1;
		if ( $req->de_page ) {
			$page = intval($req->de_page);
		}

		if ( $page < 0 ) {
			$page = 1;
		}

		$per_page = 10;
		
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$cache = Zend_Registry::get('cache');

		$cache_key =  'v2_agency_escorts_' . $this->agency['id'] . '_page_' . $page . $per_page;
		if ( ! $escorts = $cache->load($cache_key) ) {
			$escorts = $escorts = $client->call('Agencies.getNotActiveEscorts', array($agency_id, $page, $per_page));
			$cache->save($escorts, $cache_key, array(), 300);
		}

		$d_escorts = $escorts['result'];
		$d_escorts_count = $escorts['count'];

		$this->view->dash_escorts = $d_escorts;
		$this->view->dash_escorts_count = $d_escorts_count;
		$this->view->dash_agency_id = $agency_id;
		$this->view->dash_page = $page;
		$this->view->dash_per_page = $per_page;

	}

	public function upgradeAction()
	{
		
	}

	public function settingsAction()
	{
		if ( $this->user->isAgency() ) {
			$this->_helper->viewRenderer->setScriptAction('agency-settings');
		}
		else if ( $this->user->isEscort() ) {
			$this->_helper->viewRenderer->setScriptAction('escort-settings');
		}
		else {
			$this->_helper->viewRenderer->setScriptAction('member-settings');
		}

		if ( $this->user->isMember() ) {
			$this->view->data = $this->user->getData(array(
				'recieve_newsletters'
			));
		}
		else {
			$this->view->data = $this->user->getData(array(
				'country_id', 'city_id', 'email', 'recieve_newsletters'
			));
		}		

		if ( $this->_request->isPost() ) {
			
			$validator = new Cubix_Validator();

			$form = new Cubix_Form_Data($this->_request);
			if ( $this->user->isMember() ) {
				
				$save_password = $this->_getParam('password');
				$save_newsletters = $this->_getParam('newsletters');

				$form->setFields(array(
					'password' => '',
					'new_password' => '',
					'new_password_2' => '',
					'recieve_newsletters' => ''
				));
			}
			else {

				$save_profile = $this->_getParam('profile');
				$save_password = $this->_getParam('password');
				$save_newsletters = $this->_getParam('newsletters');

				$form->setFields(array(
					'country_id' => 'int-nz',
					'city_id' => 'int-nz',
					'email' => '',
					'password' => '',
					'new_password' => '',
					'new_password_2' => '',
					'recieve_newsletters' => ''
				));
			}
			$data = $form->getData();

			switch ( true ) {
				case ! is_null($save_profile):
					if ( ! strlen($data['email']) ) {
						$validator->setError('email', 'Required');
					}
					elseif ( ! Cubix_Validator::isValidEmail($data['email']) ) {
						$validator->setError('email', 'Invalid email address');
					}
					elseif ( Cubix_Api::getInstance()->call('existsByEmail', array($data['email'], $this->user->getId())) ) {
						$validator->setError('email', 'Email already exists');
					}

					$model = new Cubix_Geography_Countries();

					if ( ! is_null($data['country_id']) && ! $model->exists($data['country_id']) ) {
						$data['country_id'] = null;
					}

					if ( ! is_null($data['city_id']) ) {
						if ( is_null($data['country_id']) || ! $model->hasCity($data['country_id'], $data['city_id']) ) {
							$data['city_id'] = null;
						}
					}
					
					if ( $validator->isValid() ) {
						$emails = array(
							'old' => $this->user->email,
							'new' => $data['email']
						);

						$this->user->updateData(array(
							'country_id' => $data['country_id'],
							'city_id' => $data['city_id'],
							'email' => $data['email']
						));
						$this->view->data = $this->user->getData(array(
							'country_id', 'city_id', 'email', 'recieve_newsletters'
						));
												
						//newsletter email log
						Cubix_Api::getInstance()->call('addNewlstterEmailLog', array($this->user->id, $this->user->user_type, 'edit', $emails));
						//
					}
					else {
						$this->view->data = $data;
					}

					break;
				case ! is_null($save_password):
					$password = $this->_getParam('old_password');
					
					$new_password = $this->_getParam('new_password');
					$new_password_2 = $this->_getParam('new_password_2');

					if ( $new_password !== $new_password_2 ) {
						$validator->setError('new_password', 'Passwords doesn\'t match');
					}
					else if ( strlen($new_password) > 0 ) {
						try {
							$this->user->updatePassword($password, $new_password);
						}
						catch ( Exception $e ) {
							$validator->setError('old_password', $e->getMessage());
						}
					}

					break;
				case ! is_null($save_newsletters):
					$flag = (bool) $this->_getParam('newsletters');

					$this->user->updateRecieveNewsletters($flag);
					$this->view->data = $this->user->getData(array(
						'country_id', 'city_id', 'email', 'recieve_newsletters'
					));

					break;
			}

			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();
				$this->view->errors = $status['msgs'];
			}
		}
	}

	public function addReviewAction()
	{
		if ($this->user->user_type != 'member')
			$this->_redirect($this->view->getLink('private-v2'));
		else
		{
			if (isset($this->_request->escort_id) && intval($this->_request->escort_id) > 0)
			{
				$escort_id = $this->view->escort_id = intval($this->_request->escort_id);

				if (Model_Reviews::hasProduct($escort_id, Model_EscortsV2::PRODUCT_NO_REVIEWS))
				{
					$this->_redirect($this->view->getLink());
					return;
				}
				else
				{
					$lng = Cubix_I18n::getLang();
					list($showname, $cities) = Cubix_Api::getInstance()->call('getEscortDetailsForReviews', array($escort_id, $lng));

					if (!$showname)
					{
						$this->_forward('error', 'error', 'default', array('error_msg' => 'Wrong URL.'));
						return;
					}

					$this->view->showname = $showname;
					$this->view->cities = $cities;
					$this->view->username = $this->user->username;
				}
			}
			else
			{
				$this->_forward('error', 'error', 'default', array('error_msg' => 'Wrong URL.'));
				return;
			}

			if ($this->_request->isPost())
			{
				$validator = new Cubix_Validator();

				$req = $this->_request;

				if (intval($req->m_date) == 0)
					$validator->setError('m_date', 'Required');

				if ($req->meeting_city == 'other')
				{
					if (!$req->country_id)
						$validator->setError('country_id', 'Required');
					
					if (!$req->city_id)
						$validator->setError('city_id', 'Required');
				}

				if (!$req->meeting_place)
					$validator->setError('meeting_place', 'Required');

				if (!trim($req->duration))
					$validator->setError('duration', 'Required');
				elseif (!is_numeric($req->duration))
					$validator->setError('duration', $this->view->t('must_be_numeric'));

				if (!$req->duration_unit)
					$validator->setError('duration_unit', 'Required');

				if (!trim($req->price))
					$validator->setError('currency', 'Required');
				elseif (!is_numeric($req->price))
					$validator->setError('currency', $this->view->t('must_be_numeric'));

				/*if (!$req->currency)
					$validator->setError('currency', 'Required');*/

				if ($req->looks_rate == '-1')
					$validator->setError('looks_rate', 'Required');

				if ($req->services_rate == '-1')
					$validator->setError('services_rate', 'Required');

				if (!$req->s_kissing)
					$validator->setError('s_kissing', 'Required');

				if (!$req->s_blowjob)
					$validator->setError('s_blowjob', 'Required');

				if (!$req->s_cumshot)
					$validator->setError('s_cumshot', 'Required');

				if (!$req->s_69)
					$validator->setError('s_69', 'Required');

				if (!$req->s_anal)
					$validator->setError('s_anal', 'Required');

				if (!$req->s_sex)
					$validator->setError('s_sex', 'Required');

				if (!$req->s_multiple_times_sex)
					$validator->setError('s_multiple_times_sex', 'Required');

				if (!$req->s_breast)
					$validator->setError('s_breast', 'Required');

				if (!$req->s_attitude)
					$validator->setError('s_attitude', 'Required');

				if (!$req->s_conversation)
					$validator->setError('s_conversation', 'Required');

				if (!$req->s_availability)
					$validator->setError('s_availability', 'Required');

				if (!$req->s_photos)
					$validator->setError('s_photos', 'Required');

				if (!trim($req->t_user_info))
					$validator->setError('t_user_info', 'Required');

				if (!trim($req->t_meeting_date))
					$validator->setError('t_meeting_date', 'Required');

				if ($req->hrs == '-1')
					$validator->setError('hrs', 'Required');

				if ($req->min == '-1')
					$validator->setError('hrs', 'Required');

				if (!trim($req->t_meeting_duration))
					$validator->setError('t_meeting_duration', 'Required');

				if (!trim($req->t_meeting_place))
					$validator->setError('t_meeting_place', 'Required');

				if (!trim($req->t_comments))
					$validator->setError('t_comments', 'Required');

				//$captcha = Cubix_Captcha::verify($this->_request->recaptcha_response_field);

				/*$captcha_errors = array(
					'invalid-site-public-key' => 'We weren\'t able to verify the public key.',
					'invalid-site-private-key' => 'We weren\'t able to verify the private key.',
					'invalid-request-cookie' => 'The challenge parameter of the verify script was incorrect.',
					'incorrect-captcha-sol' => 'The CAPTCHA solution was incorrect.',
					'verify-params-incorrect' => 'The parameters to /verify were incorrect, make sure you are passing all the required parameters.',
					'invalid-referrer' => 'reCAPTCHA API keys are tied to a specific domain name for security reasons.',
					'recaptcha-not-reachable' => 'reCAPTCHA never returns this error code. A plugin should manually return this code in the unlikely event that it is unable to contact the reCAPTCHA verify server.'
				);*/

				//var_dump($captcha);

				/*if ( strlen($captcha) > 5 ) {
					$validator->setError('captcha', $this->view->t('wrong_secure_text')  $captcha_errors[$captcha]);
				}*/
				$captcha = trim($req->captcha);
				if ( ! strlen($captcha ) ) {
					$validator->setError('captcha', 'Required');
				}
				else {
					$session = new Zend_Session_Namespace('captcha');
					$orig_captcha = $session->captcha;

					if ( strtolower($captcha) != $orig_captcha ) {
						$validator->setError('captcha', 'Captcha is invalid');
					}
				}


				$t_user_info = trim($req->t_user_info);
				$t_meeting_date = trim($req->t_meeting_date);
				$t_meeting_time = $req->hrs . ':' . $req->min;
				$t_meeting_duration = trim($req->t_meeting_duration);
				$t_meeting_place = trim($req->t_meeting_place);

				if ($validator->isValid())
				{
					if ($_SERVER["HTTP_X_FORWARDED_FOR"] != "")
					{
					   // for proxy
					   $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
					   //$proxy = $_SERVER["REMOTE_ADDR"];
					}
					else
					{
						// for normal user
						$ip = $_SERVER["REMOTE_ADDR"];
						//$proxy = '';
					}

					$req->setParam('ip', $ip);
					$req->setParam('user_id', $this->user->id);
					$req->setParam('m_date', intval($req->m_date));

					list($sms_unique, $phone_to, $mem_susp) = Cubix_Api::getInstance()->call('addReview', array($this->getRequest()->getParams()));
					Cubix_Api::getInstance()->call('SyncNotifier', array($escort_id, 29 ,array('escort_id' => $escort_id) ));
					if (strlen(trim($phone_to)) > 0 && !$mem_susp)
					{
						$text = $this->view->t('review_sms_template', array(
							'user_info' => $t_user_info,
							'showname' => $showname,
							'date' => $t_meeting_date,
							'meeting_place' => $t_meeting_place,
							'duration' => $t_meeting_duration,
							'time' => $t_meeting_time,
							'unique_number' => $sms_unique
						));

						$originator = Cubix_Application::getPhoneNumber(Cubix_Application::getId());

						$phone_from = $originator;
						$sms_id = Cubix_Api::getInstance()->call('outbox', array($escort_id, $phone_to, $phone_from, $text, Cubix_Application::getId()));

						//sms info
						$config = Zend_Registry::get('system_config');
						$sms_config = $config['sms'];
						
						$SMS_USERKEY = $sms_config['userkey'];
						$SMS_PASSWORD = $sms_config['password'];
						$SMS_DeliveryNotificationURL = $sms_config['DeliveryNotificationURL'];
						$SMS_NonDeliveryNotificationURL = $sms_config['NonDeliveryNotificationURL'];
						$SMS_BufferedNotificationURL = $sms_config['BufferedNotificationURL'];
						
						$sms = new Cubix_SMS($SMS_USERKEY, $SMS_PASSWORD);
						
						$sms->setOriginator($originator);
						$sms->addRecipient($phone_to, (string)$sms_id);
						$sms->setBufferedNotificationURL($SMS_BufferedNotificationURL);
						$sms->setDeliveryNotificationURL($SMS_DeliveryNotificationURL);
						$sms->setNonDeliveryNotificationURL($SMS_NonDeliveryNotificationURL);

						$sms->setContent($text);
						
						if (1 != $result = $sms->sendSMS())
						{

						}
						else
						{
							Cubix_Api::getInstance()->call('smsSent', array($sms_unique));
						}
					}

					$this->_redirect($this->view->getLink('private-v2-reviews'));
				}
				else
				{
					$status = $validator->getStatus();
					$this->view->errors = $status['msgs'];
					$this->view->review = $req;
				}
			}
		}
	}

	public function reviewsAction()
	{
		if ($this->user->user_type != 'member')
			$this->_redirect($this->view->getLink('private-v2'));
		else
		{
			$lng = Cubix_I18n::getLang();
			$this->view->items = Cubix_Api::getInstance()->call('getReviewsForMember', array($this->user->id, $lng));
		}
	}
	
	public function alertsAction()
	{
		if ($this->user->user_type != 'member')
			$this->_redirect($this->view->getLink('private-v2'));
		else
		{
			$model = new Model_Members();
			$items = $model->getAlerts($this->user->id);

			if ($items)
			{
				$arr = array();
				$esc_ids = array();

				foreach ($items as $item)
				{
					$arr[$item['escort_id']][] = $item['event'];
					
					if ($item['event'] == ALERT_ME_CITY)
						$arr[$item['escort_id']]['cities'] = $item['extra'];
					
					if (!in_array($item['escort_id'], $esc_ids))
						$esc_ids[] = $item['escort_id'];
				}

				$this->view->items = $arr;

				$modelE = new Model_EscortsV2();
				$this->view->escorts = $modelE->getAlertEscrots(implode(',', $esc_ids));
				
				/* Cities list */
				$app = Cubix_Application::getById();

				if ($app->country_id)
				{
					$modelCities = new Model_Cities();

					$cities = $modelCities->getByCountry($app->country_id);
				}
				else
				{
					$modelCountries = new Model_Countries();
					$modelCities = new Model_Cities();

					$countries = $modelCountries->getCountries();
					$this->view->countries_list = $countries;

					$c = array();

					foreach ($countries as $country)
						$c[] = $country->id;

					$cities = $modelCities->getByCountries($c);
				}

				$this->view->cities_list = $cities;
				/* End Cities list */
			}
		}
	}

	public function escortReviewsAction()
	{
		if ($this->user->user_type != 'escort')
			$this->_redirect($this->view->getLink('private-v2'));
		else
		{
			$lng = Cubix_I18n::getLang();
			$results = Cubix_Api::getInstance()->call('getReviewsForEscort', array($this->user->id, $lng,$escort_id, $page, $per_page ));
			$this->view->items = $results['reviews'];
		}
	}

	public function addReviewCommentAction()
	{
		$this->view->layout()->disableLayout();

		if ($this->user->user_type != 'escort' && $this->user->user_type != 'agency')
			//$this->_redirect($this->view->getLink('private-v2'));
			die();
		else
		{
			if (isset($this->_request->review_id) && intval($this->_request->review_id) > 0)
			{
				$this->view->review_id = $rev_id = $this->_request->review_id;
				$user_id = $this->user->id;
				$do = false;

				if ($this->user->user_type == 'escort')
				{
					if (Cubix_Api::getInstance()->call('checkEscortReview', array($user_id, $rev_id)))
					{
						$do = true;
					}
					else
						die;
				}
				else
				{
					if (Cubix_Api::getInstance()->call('checkEscortReviewForAgency', array($user_id, $rev_id)))
					{
						$do = true;
					}
					else
						die;
				}

				if ($do)
				{
					if ($this->_request->isPost())
					{
						$validator = new Cubix_Validator();

						if (!$this->_request->comment)
							$validator->setError('comment', 'Required');

						if ($validator->isValid())
						{
							Cubix_Api::getInstance()->call('addReviewComment', array($rev_id, $this->_request->comment));
							$result['status'] = 'success';
							$result['signin'] = true;
							echo json_encode($result);
							die;
						}
						else
						{
							$status = $validator->getStatus();
							$this->view->errors = $status['msgs'];
							if ( ! is_null($this->_getParam('ajax')) ) {
								echo(json_encode($status));
								ob_flush();
								die;
							}
						}
					}
				}
			}
			else
				die;
		}
	}

	/*public function advertiseNowAction()
	{
		$application_id = Cubix_Application::getId();
		$client = new Cubix_Api_XmlRpc_Client();
		
		if ($this->user->user_type != 'escort' && $this->user->user_type != 'agency') {
			$this->_redirect($this->view->getLink('private-v2'));
		}
		$this->view->is_agency = false;
		if ( $this->user->isAgency() ) {
			$this->view->is_agency = true;
			$this->view->escorts = $escorts = $this->agency->getEscortsWithoutPackages();
		} else {
			$this->view->has_packages = $client->call('Escorts.checkIfHasPackage', array($this->escort->id));
		}
		
		$phone_packages = $client->call('Billing.getPackages', array(1, $application_id));
		$this->view->phone_packages = $phone_packages;
		
	
		if ( $this->_request->isPost() ) {
			$escort_gender = null;
			if ( $this->view->is_agency ) {
				foreach($escorts as $escort) {
					if ( $escort['id'] == $this->_getParam('escort_id') ) {
						$escort_gender = $escort['gender'];
					}
				}
			} else {
				$escort_gender = $this->escort->gender;
				$this->_setParam('escort_id', $this->escort->id);
			}
			
			if ( $client->call('Escorts.checkIfHasPackage', array($this->escort->id)) )//Checking once again if escort does not have active, pending or suspended package
			{
				return;
			}
			
			$user_type = USER_TYPE_SINGLE_GIRL;
			if ( $this->user->user_type != 'escort' ) {
				$user_type = USER_TYPE_AGENCY;
			}
			$package = $client->call('Billing.gePackagesWithPrice', array($application_id, $user_type, $escort_gender, $this->_getParam('package_id')));
			
			$package_data = array(
				'escort_id' => $this->_getParam('escort_id'),
				'package_id' => $this->_getParam('package_id'),
				'application_id'	=> $application_id,
				'base_price'	=> $package['price'],
				'price'	=> $package['price'],
				'base_period'	=> $package['period'],
				'period'	=> $package['period'],
				'activation_type'	=> $this->_getParam('activation_type'),
				'activation_date' => $this->_getParam('activation_date'),
				'is_agency'	=> $this->view->is_agency,
				'gender'	=> $escort_gender
				
			);
			
			$post_data = array(
				'order_data' => array(
					'order_date'	=> date('Y-m-d H:m:i'),
					'application_id'	=> $application_id,
					'price'	=> $package['price'],
					'user_id'	=> $this->user->id
					//'backend_user_id'	=> $this->user->sales_user_id
				),
				'packages_data'	=> array($package_data)
			);
			
			$response = $client->call('Billing.addPackage', array($post_data));
			
			if ( is_numeric($response) ) {
				$this->_redirect($this->getIvrUrl($package['price'], $response));
			}
		}
	}
	
	public function advertiseSuccessAction()
	{
		$this->_helper->viewRenderer->setScriptAction('advertise-responses');
		$this->view->key = 'advertise_success_message';
	}
	public function advertiseFailureAction()
	{
		$this->_helper->viewRenderer->setScriptAction('advertise-responses');
		$this->view->key = 'advertise_failure_message';
	}
	public function advertiseCancelAction()
	{
		$this->_helper->viewRenderer->setScriptAction('advertise-responses');
		$this->view->key = 'advertise_cancel_message';
	}
	
	private function getIvrUrl($amount, $client_identifier)
	{
		if ( ! $amount || ! $client_identifier ) {
			return self::$linkHelper->getLink('private-v2-advertise-now');
		}
		
		$amount *= 100;
		
		$conf = Zend_Registry::get('ivr');
		$urls = array(
			'base_url'	=> 'http://ivr.truesenses.com/?cmd=new&acc=' . $conf['acc'] . '&aid=' . $conf['aid'] . '&amo=' . $amount . '&cur=chf&ser=' . $conf['acc'] . '&lan=de&cid=' . $client_identifier,
			'success_url'	=> 'srl=' . self::$linkHelper->getLink('private-v2-advertise-success'),
			'failure_url'	=> 'frl=' . self::$linkHelper->getLink('private-v2-advertise-failure'),
			'cancel_url'	=> 'crl=' . self::$linkHelper->getLink('private-v2-advertise-cancel')
		);
		$url = implode('&', $urls);

		return $url;
	}*/
	
	public function escortsReviewsAction()
	{
		if ($this->user->user_type != 'agency')
			$this->_redirect($this->view->getLink('private-v2'));
		else
		{
			$lng = Cubix_I18n::getLang();
			$results = Cubix_Api::getInstance()->call('getReviewsForAgencyEscorts', array($this->user->id, $lng, $page, $per_page ));
			$this->view->items = $results['reviews'];
		}
	}

	public function memberProfileAction()
	{
		$client = new Cubix_Api_XmlRpc_Client();

		if ( $this->user )
		{
			$user_data = $client->call('Users.getById', array($this->user->id));
			$this->view->user_data = new Model_UserItem($user_data);
			$this->view->user = $this->user;
		}

		if ( $this->_request->isPost() )
		{
			$data = array();

			$validator = new Cubix_Validator();

			$data = $this->_validateUserProfile($validator);

			$this->view->user_data = new Model_UserItem($data);

			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];

				return;
			}

			try {
				$result = $client->call('Users.updateProfile', array($this->user->id, $data));
				//newsletter email log
				$emails = array(
					'old' => $this->user->email,
					'new' => $data['email']
				);
				Cubix_Api::getInstance()->call('addNewlstterEmailLog', array($this->user->id, $this->user->user_type, 'edit', $emails));
				//
			}
			catch (Exception $e) {
				var_dump($client->getLastResponse()); die;
			}

			if ( isset($result['error']) ) {
				print_r($result['error']); die;
			}
			else {
				if ( $result !== false ) {
					$this->view->saved = true;
					//echo nl2br(print_r($result, true));
				}
			}
		}
	}

	public function _validateUserProfile(Cubix_Validator $validator)
	{
		$req = $this->_request;

		$email = $req->email;
		if ( ! strlen($email) ) {
			$validator->setError('email', 'Email is required !');
		}
		$data['email'] = $email;

		//Location
		$country = $req->country_id;

		if ( ! $country ) {
			$validator->setError('country_id', 'Country is required !');
		}
		$data['country_id'] = $country;


		$city = $req->city_id;
		if ( ! $city ) {
			$city = null;
		}
		$data['city_id'] = $city;
		
		$about_me = $req->about_me;
		$data['about_me'] = $about_me;
		return $data;
	}

	protected function _validateAgency(Cubix_Validator $validator, $agency_id)
	{
		$req = $this->_request;

		$defines = Zend_Registry::get('defines');
		$client = new Cubix_Api_XmlRpc_Client();

		$data = array();

		$showname = preg_replace('/(\s)+/','$1', trim($req->agency_name));
		if ( ! strlen($showname) ) {
			$validator->setError('agency_name', 'Required');
		}
		elseif ( ! preg_match('/^[^-][-_a-z0-9\s]+$/i', $showname) ) {
			$validator->setError('agency_name', 'Must begin with letter or number and must contain only alphanumeric characters');
		}
		$data['name'] = $showname;
		
		// Club info block
		///////////// wellness ////////////////
		if ($req->wellness == CLUB_WELLNESS_YES)
		{
			if (!$req->wellness_add)
			{
				$data['wellness'] = $req->wellness;
				$validator->setError('err_wellness', 'Wellness Required (Free / With cost)');
			}
			else
			{
				$data['wellness'] = $req->wellness_add;
			}
		}
		else
		{
			$data['wellness'] = $req->wellness;
		}
		///////////////////////////////////////

		///////////// food ////////////////////
		if ($req->food == CLUB_FOOD_YES)
		{
			if (!$req->food_add)
			{
				$data['food'] = $req->food;
				$validator->setError('err_food', 'Food and drinks Required (Free / With cost)');
			}
			else
			{
				$data['food'] = $req->food_add;
			}
		}
		else
		{
			$data['food'] = $req->food;
		}
		
		$data['entrance'] = $req->entrance;
		$data['outdoor'] = $req->outdoor;
		///////////////////////////////////////

		// Working times block
		$wds = (array) $req->work_days;
		$work_times = array();
		foreach ( $wds as $d => $nil ) {
			if ( ! ($req->work_times_from[$d]) || ! ($req->work_times_to[$d]) ) {
				$validator->setError('work_times_' . $d, 'Select time interval');
			}

			$work_times[$d] = array('from' => $req->work_times_from[$d], 'to' => $req->work_times_to[$d]);
		}
		$data['working_times'] = $work_times;

		$available_24_7 = $req->available_24_7;
		if ( ! $available_24_7 ) {
			$available_24_7 = null;
		}
		$data['available_24_7'] = $available_24_7;


		// Contacts block

		$contact_phone = $req->phone;
		//$phone_prefix = $req->phone_prefix;
		$phone_prefix = '1-41-0'; // for Switzerland only
		
		$data['contact_phone_parsed'] = null;
		if ($contact_phone ) {
			if ( ! preg_match("/^[0-9\s\+\-\(\)]+$/i", $contact_phone) ) {
				$validator->setError('phone', 'Invalid phone number');
			}
			elseif(preg_match("/^(\+|00)/", $contact_phone) ) {
				$validator->setError('phone', 'Please enter phone number without country calling code');
			}
			if ( ! ($phone_prefix) ) {
				$validator->setError('phone_prefix', 'Country calling code Required');
			}

		}
		list($country_id,$phone_prefix,$ndd_prefix) = explode('-',$phone_prefix);
		unset($data['phone_prefix']);
		$data['phone_country_id'] = intval($country_id);
		$data['contact_phone_parsed'] = $data['phone'] = preg_replace('/[^0-9]+/', '', $contact_phone);
		$data['contact_phone_parsed'] = preg_replace('/^'.$ndd_prefix.'/', '', $data['contact_phone_parsed']);
		$data['contact_phone_parsed'] = '00'.$phone_prefix.$data['contact_phone_parsed'];
		
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		/*$results = $client->call('Escorts.existsByPhone', array($data['contact_phone_parsed'], null, $agency_id));
		
		//var_dump($results);die;
		$resCount = count($results);

		if($resCount > 0) {
			$validator->setError('phone', 'Phone already exists');
			}
		*/

		$phone_instructions = $req->phone_instr;
		if ( ! $phone_instructions ) {

		}
		$data['phone_instructions'] = $phone_instructions;

		$contact_email = $req->email;
		if ( ! $contact_email ) {

		}
		$data['email'] = $contact_email;

		$contact_web = $req->web;
		if ( ! $contact_web ) {

		}
		$data['web'] = $contact_web;

		//Location
		/*$country = $req->country_id;
		if ( ! $country ) {

		}*/
		$data['country_id'] = Cubix_Application::getById()->country_id;

		$region = $req->region_id;
		if ( ! $region ) {

		}
		$data['region_id'] = $region;
		
		$fake_city_id = $req->fake_city_id;
		
		if ( ! $fake_city_id ) {

		}
		$data['fake_city_id'] = $fake_city_id;

		$zip = $req->zip;
		if ( ! $zip ) {

		}
		$data['zip'] = $zip;
		
		$address = $req->address;
		if ( ! $address ) {

		}
		$data['address'] = $address;

		/*$city = $req->city_id;
		if ( ! $city ) {

		}
		$data['city_id'] = $city;*/
		$data['city_id'] = '';
		unset($data['city_id']);
		
		$data['phone_to_all_escorts'] = $req->phone_to_all_escorts;
		
		$data['is_anonymous'] = $req->is_anonymous;
		if ( strlen($data['email']) && $client->call('Application.isDomainBlacklisted', array($data['email'])) ) {
			$validator->setError('email', 'Domain is blacklisted');
		}

		if ( strlen($data['web']) && $client->call('Application.isDomainBlacklisted', array($data['web'])) ) {
			$validator->setError('web', 'Domain is blacklisted');
		}

		/* // Sales person
		$client = Cubix_Api_XmlRpc_Client::getInstance();

		$sales_user_id = intval($req->sales_user_id);
		if ( ! $sales_user_id ) $sales_user_id = null;

		if ( ! is_null($sales_user_id) ) {
			if ( ! $client->call('Users.isSalesValid', array($sales_user_id)) ) {
				$sales_user_id = null;
			}
		}
		$data['sales_user_id'] = $sales_user_id; */
		$i18n_data = Cubix_I18n::getValues($req->getParams(), 'about', '');
			foreach ( $i18n_data as $field => $value ) {
				$i18n_data[$field] = strip_tags(trim($value));
			}
		$data = array_merge( $data,$i18n_data);
		
		return $data;
	}

	public function agencyProfileAction()
	{
		$client = new Cubix_Api_XmlRpc_Client();

		if ( $this->user->isAgency() )
		{
			$agency_data = $client->call('Agencies.getByUserId', array($this->user->id));
			$agency_id = $agency_data['id'];
			$agency_slug = $agency_data['slug'];
			$phone_prefix = $agency_data['phone_country_id'];
			$agency_data = array_merge($agency_data, $client->call('Agencies.getInfo', array($agency_slug, $agency_id)));
			
			$countyModel = new Model_Countries();

			$this->view->countries = $countyModel->getPhoneCountries();
			$this->view->sales_persons = $client->call('Users.getSalesPersons');
			$this->view->phone_prefix_id = $phone_prefix;

		}

		$this->view->user = $this->user;

		if ( $this->_request->isPost() ) {

			$data = array();

			$validator = new Cubix_Validator();

			$data = $this->_validateAgency($validator, $agency_id);

			/* // Assign escort to a sales person
			$client->call('Users.assignAgency', array($data['sales_user_id'], $agency_id));
			unset($data['sales_user_id']); */

			$phone_to_all_escorts = $data['phone_to_all_escorts'];
			unset($data['phone_to_all_escorts']);
			
			$data['application_id'] = $agency_data['application_id'];
			//$this->view->phone_prefix_id = $data['phone_country_id'];
			unset($data['phone_country_id']);
			$this->view->agency_data = new Model_AgencyItem($data);
			
			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];

				return;
			}

			try {
				$result = $client->call('Agencies.updateAgencyProfile', array($agency_id, $data));
				if($phone_to_all_escorts == 1 ){
					
					$agency_escorts = $client->call('Agencies.getAllEscorts', array($agency_id));
					$escort_data = array(
							'phone_country_id'	   => $data['phone_country_id'],
							'phone_number'         => $data['phone'],
						    'contact_phone_parsed' => $data['contact_phone_parsed'],
							'phone_exists'         => $data['contact_phone_parsed']
						);
					
					foreach($agency_escorts as $escort){
						Cubix_Api::getInstance()->call('updateEscortProfileSimple', array( $escort['id'], $escort_data) );
						
					}
					
				}
			}
			catch (Exception $e) {
				var_dump($client->getLastResponse()); die;
			}

			if ( isset($result['error']) ) {
				print_r($result['error']); die;
			}
			else {
				if ( $result !== false ) {
					$this->view->saved = true;
					echo nl2br(print_r($result, true));
				}
			}
		}
		else{
			$phone_prfixes = $countyModel->getPhonePrefixs();
			if($agency_data['phone']){
				if(preg_match('/^(\+|00)/',trim($agency_data['phone'])))
				{
					$phone_prefix_id = NULL;
					$phone_number = preg_replace('/^(\+|00)/', '',trim($agency_data['phone']));
					foreach($phone_prfixes as $prefix)
					{
						if(preg_match('/^('.$prefix->phone_prefix.')/',$phone_number))
						{
							$phone_prefix_id = $prefix->id;
							$agency_data['phone'] = preg_replace('/^('.$prefix->phone_prefix.')/', '',$phone_number);
							BREAK;
						}

					}
					$this->view->phone_prefix_id = $phone_prefix_id;
				}
			}
			$this->view->agency_data = new Model_AgencyItem($agency_data);

		}
	}


	public function agencyLogoAction()
	{
		$this->view->layout()->disableLayout();

		$client = new Cubix_Api_XmlRpc_Client();

		if ( $this->user->isAgency() )
		{
			$agency = $this->user->getAgency();
			$this->view->agency = $agency;

			if ( $this->_request->isPost() )
			{

				if ( $this->_request->a )
				{
					try {
						if ( ! isset($_FILES['logo']) || ! isset($_FILES['logo']['name']) || ! strlen($_FILES['logo']['name']) ) {
							$this->view->uploadError = 'Please select a photo to upload';
							return;
						}
						// Save on remote storage
						$images = new Cubix_Images();
						$image = $images->save($_FILES['logo']['tmp_name'], 'agencies', $agency->application_id, strtolower(@end(explode('.', $_FILES['logo']['name']))));

						$agency_data['logo_hash'] = $image['hash'];
						$agency_data['logo_ext'] = $image['ext'];

						$result = $client->call('Agencies.updateAgencyLogo', array($agency->id, $agency_data));

						$agency = $this->user->getAgency();
						$this->view->agency = $agency;

					} catch (Exception $e) {
						$this->view->uploadError = $e->getMessage();
					}
				}
				else if ( $this->_request->d ) {
					$agency_data['logo_hash'] = null;
					$agency_data['logo_ext'] = null;
					try {
						$client->call('Agencies.updateAgencyLogo', array($agency->id, $agency_data));

						$agency = $this->user->getAgency();
						$this->view->agency = $agency;
					} catch (Exception $e) {
						$this->view->uploadError = $e->getMessage();
					}
				}
			}
		}
	}

	public function toursAction()
	{
		$this->_request->setParam('no_tidy', true);

		$this->view->is_agency = $this->user->isAgency();

		$escort_id = intval($this->_getParam('escort'));
		if ( $escort_id > 0  && !is_null($this->_getParam('info')) ) {
			$model = new Model_Escorts();
			$escort = $model->getById($escort_id);

			if ( ! $escort ) {
				die(json_encode(null));
			}
			else {
				die(json_encode(array(
					'photo' => $escort->getMainPhoto()->getUrl('agency_p100', true),
					'link' => $this->view->getLink('profile', array('showname' => $escort->showname, 'escort_id' => $escort_id))
				)));
			}
		}

		if ( $this->user->isAgency() ) {
			$this->_helper->viewRenderer->setScriptAction('agency-tours');
			$this->view->escorts = $this->agency->getEscorts();
		}
		else {
			$this->_helper->viewRenderer->setScriptAction('escort-tours');
			$this->escort = $this->view->escort = $this->user->getEscort();
		}
		
		$this->view->disable_tour_history = $this->client->call('getTourHistory', array($this->user->id));
	}
	
	public function toggleTourHistoryAction()
	{
		$this->view->layout()->disableLayout();
		$o_t_v = intval($this->_request->o_t_v);
		
		$o_t_v == 1 ? $o_t_v = 0 : $o_t_v = 1;
				
		$this->client->call('updateTourHistory', array($this->user->id, $o_t_v));
	}

	public function ajaxToursAction($data = array())
	{
		$this->view->layout()->disableLayout();
		$this->_request->setParam('no_tidy', true);

		$this->view->is_agency = $this->user->isAgency();
		$this->view->tours = array();
        /* Update Grigor */
		$getall = false;
        $agency_id = 0;
        /* Update Grigor */
		if ( $this->user->isAgency() ) {
			$escort_id = intval($this->_getParam('escort_id'));
            /* Update Grigor */
            $agency =  $this->user->getAgency();
            $agency_id = $agency->id;
           
            if($escort_id == -1){
                $getall = true;
            }
            /* Update Grigor */

			if ( ! $this->agency->hasEscort($escort_id) && !$getall) return;
		}
		elseif ( $this->user->isEscort() ) {
			$escort_id = $this->escort->getId();
		}

		if ( is_null($escort_id) ) {
			return;
		}
         /* Update Grigor */
        $tours = $this->client->call('getEscortTours', array($escort_id, Cubix_I18n::getLang(), $agency_id));
        $this->view->tours = $tours[0];
		$this->view->count = $tours[1];
        /* Update Grigor */


//		echo json_encode(array_merge(array(
//			'data' => $this->client->call('getEscortTours', array($escort_id, Cubix_I18n::getLang())),
//		), $data));
//		die;
	}

	public function editTourAction()
	{
		$this->view->layout()->disableLayout();
		$this->_request->setParam('no_tidy', true);

		$tour = null;
		if ( ($tour_id = intval($this->_getParam('id'))) > 0 ) {
			$tour = $this->client->call('getEscortTour', array($tour_id, $this->_request->lang_id));
		}
		$this->view->tour = $tour;

		if ( $this->user->isAgency() ) {
			$escort_id = intval($this->_getParam('escort_id'));
			if ( $tour ) $escort_id = $tour['escort_id'];
			if ( ! $this->agency->hasEscort($escort_id) ) {
				die('Permission denied!');
			}
		}
		elseif ( $this->user->isEscort() ) {
			$escort_id = $this->escort->getId();
			if ( $tour && $escort_id != $tour['escort_id'] ) {
				die('Permission denied!');
			}
		}
		
		if ( ! $escort_id ) die;


		$model = new Model_Escorts();
		$this->view->escort = $escort = $model->getById($escort_id);

		if ( $this->_request->isPost() ) {
			$form = new Cubix_Form_Data($this->_request);
			$form->setFields(array(
				'id' => 'int-nz',
				'country_id' => 'int-nz',
				'city_id' => 'int-nz',
				'date_from' => 'int',
				'date_to' => 'int',
				'phone' => '',
				'email' => ''
			));
			$i_data = $form->getData();

			$validator = new Cubix_Validator();

			// <editor-fold defaultstate="collapsed" desc="Tour Dates Overlap Validation">
			$tours = $this->client->call('getEscortTours', array($escort_id, $this->_request->lang_id));
			$data['tours_tmp'] = $tours[0];
			/*
			if ( count($data['tours_tmp']) ) {
				foreach ( $data['tours_tmp'] as $t_k => $tr ) {
					unset($data['tours_tmp'][$t_k]['country']);
					unset($data['tours_tmp'][$t_k]['city']);
				}
			}
			$data['tours_tmp'][] = $i_data;

			$today = strtotime(date('d-m-Y', time()));

			foreach ( $data['tours_tmp'] as $i1 => $tourr ) {
				//list($id, $country_id, $city_id, $date_from, $date_to, $phone, $email) = $tour;
				$id = $tourr['id'];
				$country_id = $tourr['country_id'];
				$city_id = $tourr['city_id'];
				$date_from = $tourr['date_from'];
				$date_to = $tourr['date_to'];
				$phone = $tourr['phone'];

				//if ( $id == $i_data['id'] ) continue;

				if ( $date_to < $today ) {
					$validator->setError('date_from', 'Some date intervals are invalid or overlapped');
					break;
				}

				if ( $date_from > $date_to ) {
					$validator->setError('date_from', 'Some date intervals are invalid or overlapped');
					break;
				}

				$found = false;

				foreach ( $data['tours_tmp'] as $i2 => $t1 ) {
					if ( $i1 == $i2 ) continue;

					//list($nil, $nil, $d_f, $d_t, $phone) = $t1;
					$d_f = $t1['date_from'];
					$d_t = $t1['date_to'];

					if ( ($d_f >= $date_from && $d_f < $date_to) || ($d_t > $date_from && $d_t <= $date_to) ) {
						$found = true;

						$validator->setError('date_from', 'Some date intervals are invalid or overlapped');
						break;
					}
				}

				if ( $found ) {
					break;
				}
			}
			unset($data['tours_tmp']);
			*/
			/**************************************************************/
			if ( count($data['tours_tmp']) ) {
				foreach ( $data['tours_tmp'] as $t_k => $tr ) {
					unset($data['tours_tmp'][$t_k]['country']);
					unset($data['tours_tmp'][$t_k]['city']);
					$data['tours_tmp'][$t_k]['date_from'] = strtotime(date('d-m-Y', $data['tours_tmp'][$t_k]['date_from']));
					$data['tours_tmp'][$t_k]['date_to'] = strtotime(date('d-m-Y', $data['tours_tmp'][$t_k]['date_to']));
				}
			}
			
			$today = strtotime(date('d-m-Y', time()));
			
			if ( $i_data['date_to'] < $today || $i_data['date_from'] < $today ) {				
				$validator->setError('date_from', 'Some date intervals are invalid or overlapped');
			}

			if ( $i_data['date_from'] > $i_data['date_to'] ) {
				$validator->setError('date_from', 'Some date intervals are invalid or overlapped');
			}

			if ( $validator->isValid() ) {
				foreach ( $data['tours_tmp'] as $t ) {
					if ($t['id'] == $i_data['id']) continue;
					
					$i_data['date_from'] = strtotime(date('d-m-Y', $i_data['date_from']));
					$i_data['date_to'] = strtotime(date('d-m-Y', $i_data['date_to']));
					
					if ( ($t['date_from'] >= $i_data['date_from'] && $t['date_from'] < $i_data['date_to']) ||
							($t['date_to'] > $i_data['date_from'] && $t['date_to'] <= $i_data['date_to']) ) {
						
						$validator->setError('date_from', 'Some date intervals are invalid or overlapped');
						break;
					}
				}
				unset($data['tours_tmp']);
			}
			/**************************************************************/
			// </editor-fold>

			// <editor-fold defaultstate="collapsed" desc="Other Fields Validation">
			$model = new Cubix_Geography_Countries();
			if ( ! $model->exists($i_data['country_id']) ) {
				$validator->setError('country_id', 'Invalid country');
			}

			if ( ! $model->hasCity($i_data['country_id'], $i_data['city_id']) ) {
				$validator->setError('city_id', 'Invalid city');
			}

			$date_from = strtotime(date('d-m-Y', $i_data['date_from']));
			$date_to = strtotime(date('d-m-Y', $i_data['date_to']));

			$today = strtotime(date('d-m-Y', time()));
			if ( ! $i_data['date_from'] || ! $i_data['date_to'] ) {
				$validator->setError('date_from', 'Date required');
			}
			elseif($date_from > $date_to){
				$validator->setError('date_from', 'Invalid date range');
			}
			elseif($date_to <  $today){
				$validator->setError('date_from', 'Invalid date range');
			}

			if ( $i_data['email'] ) {
				if ( ! Cubix_Validator::validateEmailAddress($i_data['email']) ) {
					$validator->setError('email', 'Invalid email address');
				}
				/*elseif ( Cubix_Application::isDomainBlacklisted($i_data['email']) ) {
					$validator->setError('email', 'Domain is blacklisted');
				}*/
			}
			// </editor-fold>

			if ( $validator->isValid() ) {
				if ( ! $tour ) {
					$this->client->call('addEscortTour', array($escort_id, $i_data));
				}
				else {
					$this->client->call('updateEscortTour', array($escort_id, $tour_id, $i_data));
				}
			}
			
			die(json_encode($validator->getStatus()));
		}
	}

	public function removeToursAction()
	{
		$tours = (array) $this->_getParam('tours');
		foreach ( $tours as $tour_id ) {
			$tour_id = intval($tour_id);
			
			$tour = $this->client->call('getEscortTour', array($tour_id, $this->_request->lang_id));
			if ( ! $tour ) continue;

			if ( $this->user->isAgency() ) {
				if ( ! $this->agency->hasEscort($tour['escort_id']) ) {
					continue; // wrong owner
				}
			}
			elseif ( $this->user->isEscort() ) {
				if ( $this->escort->getId() != $tour['escort_id'] ) {
					continue; // wrong owner
				}
			}
			
			$this->client->call('removeEscortTour', array($tour['escort_id'], $tour['id']));
		}

		die;
	}

	public function ajaxToursAddAction()
	{
		$this->_request->setParam('no_tidy', true);

		if ( $this->user->isAgency() ) {
			$escort_id = intval($this->_getParam('escort_id'));
			if ( ! $this->agency->hasEscort($escort_id) ) die;
		}
		elseif ( $this->user->isEscort() ) {
			$escort_id = $this->escort->getId();
		}

		if ( is_null($escort_id) ) die;

		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'id' => 'int-nz',
			'country_id' => 'int-nz',
			'city_id' => 'int-nz',
			'date_from' => 'int',
			'date_to' => 'int',
			'phone' => '',
			'email' => ''
		));
		$i_data = $form->getData();

		$validator = new Cubix_Validator();

		$data['tours_tmp'] = $this->client->call('getEscortTours', array($escort_id, $this->_request->lang_id));
		if ( count($data['tours_tmp']) ) {
			foreach ( $data['tours_tmp'] as $t_k => $tr ) {
				unset($data['tours_tmp'][$t_k]['country']);
				unset($data['tours_tmp'][$t_k]['city']);
			}
		}
		$data['tours_tmp'][] = $i_data;

		$today = strtotime(date('d-m-Y', time()));

		foreach ( $data['tours_tmp'] as $i1 => $tour ) {
			//list($id, $country_id, $city_id, $date_from, $date_to, $phone, $email) = $tour;
			$id = $tour['id'];
			$country_id = $tour['country_id'];
			$city_id = $tour['city_id'];
			$date_from = $tour['date_from'];
			$date_to = $tour['date_to'];
			$phone = $tour['phone'];
			
			if ( $id == $i_data['id'] ) continue;

			if ( $date_to < $today ) {
				$validator->setError('tours', 'Some date intervals are invalid or overlapped');
				break;
			}

			if ( $date_from > $date_to ) {
				$validator->setError('tours', 'Some date intervals are invalid or overlapped');
				break;
			}

			$found = false;

			foreach ( $data['tours_tmp'] as $i2 => $t1 ) {
				if ( $i1 == $i2 ) continue;

				//list($nil, $nil, $d_f, $d_t, $phone) = $t1;
				$d_f = $t1['date_from'];
				$d_t = $t1['date_to'];

				if ( ($d_f >= $date_from && $d_f < $date_to) || ($d_t > $date_from && $d_t <= $date_to) ) {
					$found = true;

					$validator->setError('tours', 'Some date intervals are invalid or overlapped');
					break;
				}
			}

			if ( $found ) {
				break;
			}
		}
		unset($data['tours_tmp']);



		$model = new Cubix_Geography_Countries();
		if ( ! $model->exists($i_data['country_id']) ) {
			$validator->setError('country', 'Invalid country');
		}

		if ( ! $model->hasCity($i_data['country_id'], $i_data['city_id']) ) {
			$validator->setError('city', 'Invalid city');
		}

		if ( ! $i_data['date_from'] || ! $i_data['date_to'] ) {
			$validator->setError('date', 'Invalid date range');
		}

		if ( $i_data['email'] ) {
			if ( ! Cubix_Validator::validateEmailAddress($i_data['email']) ) {
				$validator->setError('email', 'Invalid email address');
			}/*
			else if (Cubix_Application::isDomainBlacklisted($i_data['email']) ) {
				$validator->setError('email', 'Domain is blacklisted');
			}*/
		}

		if ( $validator->isValid() ) {
			$this->client->call('addEscortTour', array($escort_id, $i_data));
		}
		else {
			die(json_encode($validator->getStatus()));
		}

		$this->ajaxToursAction(array('status' => 'success'));
	}

	public function ajaxToursRemoveAction()
	{
		$this->_request->setParam('no_tidy', true);

		if ( $this->user->isAgency() ) {
			$escort_id = intval($this->_getParam('escort_id'));
			if ( ! $this->agency->hasEscort($escort_id) ) die;
		}
		elseif ( $this->user->isEscort() ) {
			$escort_id = $this->escort->getId();
		}

		$tour_id = intval($this->_getParam('id'));

		$this->client->call('removeEscortTour', array($escort_id, $tour_id));

		$this->ajaxToursAction(array('status' => 'success'));
	}


	public function statisticsAction()
	{
		if ( $this->user->isAgency() ) {
			$this->view->escorts = $this->agency->getEscorts();
			$this->view->is_escort = false;
		}else{
			$this->view->escorts = array( $this->escort );
			$this->view->is_escort = true;
		}
	}

	public function ajaxGetStatisticsAction()
	{
		$this->view->layout()->disableLayout();

		$escort_id = (int) $this->_getParam('stat_escort');
		$date_from = $this->_getParam('date_from');
		$date_to = $this->_getParam('date_to');

		$order_by = $this->_getParam('sort_by');
		$order_dir = $this->_getParam('sort_dir');

		$cache = Zend_Registry::get('cache');


		// <editor-fold defaultstate="collapsed" desc="Cache key generation">
		$tkey = "";

		if ( $escort_id ){
			$tkey .= "_escort_".$escort_id;
		}

		if ( $this->agency->id ){
			$tkey .= "_agency_".$this->agency->id;
		}

//		if ( $date != "" ){
//			$tkey .= "_date_".date;
//		}

		if ( $date_from != "" ){
			$tkey .= "_date_from_".$date_from;
		}

		if ( $date_to != "" ){
			$tkey .= "_date_to_".$date_to;
		}

		if ( $order_by != "" ){
			$tkey .= "_order_by_".$order_by;
		}

		if ( $order_dir != "" ){
			$tkey .= "_order_dir_".$order_dir;
		}
		// </editor-fold>



		$cache_key = 'v2_statistics'.$tkey;

		if ( ! $items = $cache->load($cache_key) )
		{
			$items = Model_Statistics::getEscortStatistics($this->agency->id, $escort_id, $date_from,  $date_to, $order_by, $order_dir);
			$cache->save($items, $cache_key, array());
		}


		$this->view->data = $items;
//		var_dump( $this->view->data );
//		exit;
	}

	public function ajaxGetStatisticsReportAction(){

		$this->view->layout()->disableLayout();

		$type = $this->_getParam('type');
		$id = $this->_getParam('id');

		if ( $this->user->isAgency() ) {
			if ( ! $this->agency->hasEscort($id) ) {
				exit; // wrong ownere
			}
		}
		elseif ( $this->user->isEscort() ) {
			if ( $this->escort->getId() != $id ) {
				exit; // wrong owner
			}
		}


		$this->view->status = Model_Statistics::getEscortReportStatus( $id );

		$this->view->type = $type;


	}

	public function ajaxStatisticsReportAction(){
		$this->view->layout()->disableLayout();

		$status = $this->_getParam('status');
		$type = $this->_getParam('type');
		$id = $this->_getParam('id');

		if ( $this->user->isAgency() ) {
			if ( ! $this->agency->hasEscort($id) ) {
				exit; // wrong ownere
			}
		}
		elseif ( $this->user->isEscort() ) {
			if ( $this->escort->getId() != $id ) {
				exit; // wrong owner
			}
		}

		if ( is_array($status) ){
			$status = current($status);
		}
		$type = reset($type);
		if ( $type == 0 ){
			$status = -$status;
		}

		$this->view->status = $status;
		$this->view->type = $type;

		$items = Model_Statistics::updateReport($id, $status, $type, $this->user->email, $this->user->username);


	}

	private function _loadPhotos()
	{		
		$client = new Cubix_Api_XmlRpc_Client();
		
		
		//$this->adv_session = new Zend_Session_Namespace('adv_sess');
		if ( isset($this->adv_session->escort_id) ) {
			$this->escort = new Model_EscortItem(array('id' => $this->adv_session->escort_id));
		}
		
		$public_photos = $client->call('Escorts.getEscortPhotosList', array($this->escort->id, false));
		//$public_photos = Cubix_Api::getInstance()->call('getEscortPhotosList', array($this->escort->id, false));
		$nil = null;
		
		//$private_photos = Cubix_Api::getInstance()->call('getEscortPhotosList', array($this->escort->id, true));
		//$archived_photos = Cubix_Api::getInstance()->call('getEscortArchivedPhotosList', array($this->escort->id));
		$archived_photos = $client->call('Escorts.getEscortArchivedPhotosList', array($this->escort->id));

		return $this->view->photos = array_merge($public_photos, $archived_photos);
	}

	public function photosAction()
	{
		$this->_request->setParam('no_tidy', true);

		$this->view->user = $this->user;

		$client = new Cubix_Api_XmlRpc_Client();

		if ( $this->user->isEscort() ) {
			$escort = $this->user->getEscort();
			$escort_id = $escort->id;
		}
		else {
			$escort_id = intval($this->_getParam('escort'));

			if ( 1 > $escort_id ) die;

			if ( ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
				die;
			}

			$model = new Model_Escorts();
			$this->escort = $escort = $model->getById($escort_id);
		}

		$this->view->escort = $escort;
		$photos = $this->_loadPhotos();

		$photos_ids = array();
		foreach ( $photos as $photo ) {
			$photo_ids[] = intval($photo['id']);
		}

		$action = $this->_getParam('a');

		$photos_min_count = Cubix_Application::getById(Cubix_Application::getId())->min_photos;

		$this->view->is_private = $is_private = (bool) intval($this->_getParam('is_private'));

		if ( ! is_null($action) ) {
			if ( 'set-main' == $action ) {
				$ids = $this->_getParam('photo_id');
				if ( ! is_array($ids) || ! count($ids) ) {
					return $this->view->actionError = 'Please select at least one photo';
				}

				$photo_id = reset($ids);
				if ( ! in_array($photo_id, $photo_ids) ) {
					return $this->view->actionError = 'Invalid id of one of the photos';
				}
				
				$photo = new Model_Escort_PhotoItem(array('id' => $photo_id));
				$result = $photo->setMain();
				
				if ( is_array($result) ) return $this->view->actionError = Cubix_I18n::translate('sys_error_photo_is_not_approved');

				$this->_loadPhotos();
			}
			elseif ( 'delete' == $action ) {
				$ids = $this->_getParam('photo_id');

				if ( ! is_array($ids) || ! count($ids) ) {
					return $this->view->actionError = 'Please select at least one photo';
				}

				foreach ( $ids as $id ) {
					if ( ! in_array($id, $photo_ids) ) {
						return $this->view->actionError = 'Invalid id of one of the photos';
					}
				}

				$photo = new Model_Escort_Photos();
				$result = $photo->remove($ids);

//				// If count of photos is smaller than required, deactivate escort
//				$photos_count = $escort->getPhotosCount();
//				if ( $photos_count < $photos_min_count ) {
//					if ( $client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_ACTIVE))) ) {
//						$client->call('Escorts.removeStatusBit', array($escort_id, array(
//							Model_Escorts::ESCORT_STATUS_ACTIVE
//						)));
//					}
//
//					if ( ! $client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS))) ) {
//						$client->call('Escorts.setStatusBit', array($escort_id, array(
//							Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS
//						)));
//					}
//				}

				$this->_loadPhotos();
			}
			elseif ( 'upload' == $action ) {
				try {
					//echo json_encode(array('text' => 'ddd'));die;
					if ( ! isset($_FILES['Filedata']) || ! isset($_FILES['Filedata']['name']) || ! strlen($_FILES['Filedata']['name']) ) {
						$this->view->uploadError = 'Please select a photo to upload';
						return;
					}

					// Save on remote storage
					$images = new Cubix_Images();
					$image = $images->save($_FILES['Filedata']['tmp_name'], $escort->id, $escort->application_id, strtolower(@end(explode('.', $_FILES['Filedata']['name']))));

					if ( ! isset($image['hash']) ) {
						throw new Exception("Photo upload failed. Please try again!");
					}
					
					$image = new Cubix_Images_Entry($image);
					$image->setSize('sthumb');
					$image->setCatalogId($escort->id);
					$image_url = $images->getUrl($image);

					$image_size = getimagesize($_FILES['Filedata']['tmp_name']);
					
					if ( ! $image_size ) {
						throw new Exception(Cubix_I18n::translate('sys_error_upload_img') );
					}
					
					$is_portrait = 0;
					if ( $image_size ) {
						if ( $image_size[0] < $image_size[1] ) {
							$is_portrait = 1;
						}
					}
					
					$photo_arr = array(
						'escort_id' => $escort->id,
						'hash' => $image->getHash(),
						'ext' => $image->getExt(),
						'type' => $is_private ? ESCORT_PHOTO_TYPE_PRIVATE : ESCORT_PHOTO_TYPE_HARD,
						'is_portrait' => $is_portrait
					);

					if ( $client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_NO_PROFILE))) || $client->call('Escorts.isAutoApproval', array($escort_id)) ) {
						$photo_arr['is_approved'] = 1;
					}
					
					$photo = new Model_Escort_PhotoItem($photo_arr);

					$model = new Model_Escort_Photos();
					$photo = $model->save($photo);

//					// If count of photos is smaller than required, deactivate escort
//					$photos_count = $escort->getPhotosCount();
//					if ( $photos_count < $photos_min_count ) {
//						if ( $client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_ACTIVE))) ) {
//							$client->call('Escorts.removeStatusBit', array($escort_id, array(
//								Model_Escorts::ESCORT_STATUS_ACTIVE
//							)));
//						}
//
//
//						if ( ! $client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS))) ) {
//							$client->call('Escorts.setStatusBit', array($escort_id, array(
//								Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS
//							)));
//						}
//					}
//					// Otherwise activate escort
//					else {
//						//echo "cool";
//						if ( $client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS))) ) {
//							$client->call('Escorts.removeStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS)));
//						}
//
//						if ( ! $client->call('Escorts.hasStatusBit', array($escort_id, Model_Escorts::ESCORT_STATUS_ADMIN_DISABLED)) &&
//							! $client->call('Escorts.hasStatusBit', array($escort_id, Model_Escorts::ESCORT_STATUS_OWNER_DISABLED)) )
//						{
//							if (
//								(! $client->call('Escorts.hasStatusBit', array($escort_id, Model_Escorts::ESCORT_STATUS_IS_NEW)) &&
//								! $client->call('Escorts.hasStatusBit', array($escort_id, Model_Escorts::ESCORT_STATUS_NO_PROFILE)) &&
//								! $client->call('Escorts.hasStatusBit', array($escort_id, Model_Escorts::ESCORT_STATUS_PROFILE_CHANGED)))
//							) {
//								$client->call('Escorts.setStatusBit', array($escort_id, Model_Escorts::ESCORT_STATUS_ACTIVE));
//							}
//						}
//					}

					$this->view->newPhoto = $photo;

					echo json_encode(array(
						'status' => 1,
						'name' => "success"
					));
					die;

				} catch ( Exception $e ) {
					echo json_encode(array(
						'status' => 0,
						'error' => $e->getMessage()
					));
					die;
					$this->view->uploadError = $e->getMessage();
				}
			}
			elseif ( 'set-adj' == $action ) {
				$photo_id = intval($this->_getParam('photo_id'));

				if ( ! in_array($photo_id, $photo_ids) ) {
					die(json_encode(array('error' => 'An error occured')));
				}

				$photo = new Model_Escort_PhotoItem(array(
					'id' => $photo_id
				));

				try {
					$hash = $photo->getHash();
					$result = array(
						'x' => intval($this->_getParam('x')),
						'y' => intval($this->_getParam('y')),
						'px' => floatval($this->_getParam('px')),
						'py' => floatval($this->_getParam('py'))
					);
					$photo->setCropArgs($result);

					// Crop All images
					$size_map = array(
						'backend_thumb' => array('width' => 150, 'height' => 205),
						'medium' => array('width' => 225, 'height' => 300),
						'thumb' => array('width' => 150, 'height' => 200),
						'nlthumb' => array('width' => 120, 'height' => 160),
						'sthumb' => array('width' => 76, 'height' => 103),
						'lvthumb' => array('width' => 75, 'height' => 100),
						'agency_p100' => array('width' => 90, 'height' => 120),
						't100p' => array('width' => 117, 'height' => 97)
					);
					$conf = Zend_Registry::get('images_config');

					get_headers($conf['remote']['url'] . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash);
					// echo $conf['remote']['url'] . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash;

					$catalog = $escort_id;
					$a = array();
					if ( is_numeric($catalog) ) {
						$parts = array();

						if ( strlen($catalog) > 2 ) {
							$parts[] = substr($catalog, 0, 2);
							$parts[] = substr($catalog, 2);
						}
						else {
							$parts[] = '_';
							$parts[] = $catalog;
						}
					}
					else if ( preg_match('#^([0-9]+)/(.+)$#', $catalog, $a) ) {
						array_shift($a);
						$catalog = $a[0];

						$parts = array();

						if ( strlen($catalog) > 2 ) {
							$parts[] = substr($catalog, 0, 2);
							$parts[] = substr($catalog, 2);
						}
						else {
							$parts[] = '_';
							$parts[] = $catalog;
						}

						$parts[] = $a[1];
					}

					$catalog = implode('/', $parts);

					foreach($size_map as $size => $sm) {
						get_headers($conf['remote']['url'] . "/" . Cubix_Application::getById()->host . "/" . $catalog . "/" . $hash . "_" . $size . ".jpg?args=" . $result['x'] . ":" . $result['y']);
					}
				}
				catch ( Exception $e ) {
					die(json_encode(array('error' => 'An error occured')));
				}

				die(json_encode(array('success' => true)));
			}
			elseif ( 'make' == substr($action, 0, 4) ) {
				$type = substr($action, 5); if ( ! in_array($type, array('public', 'private')) ) die;
				
				$ids = $this->_getParam('photo_id');
				
				if ( ! is_array($ids) || ! count($ids) ) {
					return $this->view->actionError = 'Please select at least one photo';
				}

				foreach ( $ids as $id ) {
					if ( ! in_array($id, $photo_ids) ) {
						return $this->view->actionError = 'Invalid id of one of the photos';
					}
				}

				foreach ( $ids as $id ) {
					$photo = new Model_Escort_PhotoItem(array('id' => $id));					
					$photo->make($type == 'private' ? ESCORT_PHOTO_TYPE_PRIVATE : ESCORT_PHOTO_TYPE_HARD);
				}
				
				$this->_loadPhotos();
			}
			elseif ( 'sort' == $action ) {
				$ids = $this->_getParam('photo_id');
				
				if ( ! is_array($ids) || ! count($ids) ) {
					die('Please select at least one photo');
				}

				foreach ( $ids as $id ) {
					if ( ! in_array($id, $photo_ids) ) {
						die('Invalid id of one of the photos');
					}
				}

				$model = new Model_Escort_Photos();
				$model->reorder($ids);

				die;
			}

//			if ( 'upload' != $action ) {
//				$this->view->layout()->disableLayout();
//				$this->_helper->viewRenderer->setNoRender(true);
//				die;
//			}
		}
	}

	public function plainPhotosAction()
	{
		$this->_request->setParam('no_tidy', true);

		$this->view->user = $this->user;

		$client = new Cubix_Api_XmlRpc_Client();

		if ( $this->user->isEscort() ) {
			$escort = $this->user->getEscort();
			$escort_id = $escort->id;
		}
		else {
			$escort_id = intval($this->_getParam('escort'));

			if ( 1 > $escort_id ) die;

			if ( ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
				die;
			}
                        
			$model = new Model_Escorts();
			$this->escort = $escort = $model->getById($escort_id);
		}

		$this->view->escort = $escort;
		$photos = $this->_loadPhotos();

		$photos_ids = array();
		foreach ( $photos as $photo ) {
			$photo_ids[] = intval($photo['id']);
		}

		$action = $this->_getParam('a');

		$photos_min_count = Cubix_Application::getById(Cubix_Application::getId())->min_photos;

		$this->view->is_private = $is_private = (bool) intval($this->_getParam('is_private'));
		$this->view->is_archived = $is_archived = (bool) intval($this->_getParam('is_archived'));

		if ( 'set-main' == $action || ! is_null($this->_getParam('set_main')) ) {
			$ids = $this->_getParam('photo_id');
			if ( ! is_array($ids) || ! count($ids) ) {
				return $this->view->actionError = Cubix_I18n::translate('sys_error_select_at_least_on_photo');
			}

			$photo_id = reset($ids);
			if ( ! in_array($photo_id, $photo_ids) ) {
				return $this->view->actionError = Cubix_I18n::translate('sys_error_invalid_id_photo');
			}

			$photo = new Model_Escort_PhotoItem(array('id' => $photo_id));
			$result = $photo->setMain();
			
			if ( is_array($result) ) return $this->view->actionError = Cubix_I18n::translate('sys_error_photo_is_not_approved');

			$this->_loadPhotos();
		}
		if ( 'set-gotd' == $action || ! is_null($this->_getParam('set_gotd')) ) {
			$ids = $this->_getParam('photo_id');
			if ( ! is_array($ids) || ! count($ids) ) {
				return $this->view->actionError = Cubix_I18n::translate('sys_error_select_at_least_on_photo');
			}

			$photo_id = reset($ids);
			if ( ! in_array($photo_id, $photo_ids) ) {
				return $this->view->actionError = Cubix_I18n::translate('sys_error_invalid_id_photo');
			}

			$photo = new Model_Escort_PhotoItem(array('id' => $photo_id));
			$result = $photo->setGotd();
			
			if ( is_array($result) ) return $this->view->actionError = Cubix_I18n::translate('sys_error_photo_is_not_approved');

			$this->_loadPhotos();
		}
		elseif ( 'delete' == $action || ! is_null($this->_getParam('delete')) ) {
			$ids = $this->_getParam('photo_id');

			if ( ! is_array($ids) || ! count($ids) ) {
				return $this->view->actionError = Cubix_I18n::translate('sys_error_select_at_least_on_photo');
			}

			foreach ( $ids as $id ) {
				if ( ! in_array($id, $photo_ids) ) {
					return $this->view->actionError = Cubix_I18n::translate('sys_error_invalid_id_photo');
				}
			}

			$photo = new Model_Escort_Photos();
			$result = $photo->remove($ids);

			$this->_loadPhotos();
		}
		elseif ( 'upload' == $action || ! is_null($this->_getParam('upload')) ) {

				$set_photo = false;
				$config = Zend_Registry::get('images_config');
				$new_photos = array();
				$upload_errors = array();
				$model = new Model_Escort_Photos();
				
				foreach ( $_FILES as $i => $file )
				{
					try {
						if ( ! isset($file['name']) || ! strlen($file['name']) ) {
							continue;
						}
						else {
							$set_photo = true;
						}

						$img_ext = strtolower(@end(explode('.', $file[name])));
						if (!in_array( $img_ext , $config['allowedExts'])){
							throw new Exception(Cubix_I18n::translate('sys_error_upload_img_allowed_extensions', array('extensions' => implode(', ', $config['allowedExts']))), Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
						}
						
						
						$photo_count = $model->getEscortPhotoCount($escort_id);
						
						if ( $photo_count >= Model_Escort_Photos::MAX_PHOTOS_COUNT ){
							throw new Exception(Cubix_I18n::translate('sys_error_upload_img_count_too_much', array('img_count' => Model_Escort_Photos::MAX_PHOTOS_COUNT)), Cubix_Images::ERROR_IMAGE_COUNT_LIMIT_OVER);
						}
						//die('dd');
						// Save on remote storage
						$images = new Cubix_Images();
						$image = $images->save($file['tmp_name'], $escort->id, $escort->application_id, strtolower(@end(explode('.', $file['name']))));
						
						if ( ! isset($image['hash']) ) {
							throw new Exception("Photo upload failed. Please try again!");
						}
						
						$image = new Cubix_Images_Entry($image);
						$image->setSize('sthumb');
						$image->setCatalogId($escort->id);
						$image_url = $images->getUrl($image);
						
						$image_size = getimagesize($file['tmp_name']);
						
						if ( ! $image_size ) {
							throw new Exception(Cubix_I18n::translate('sys_error_upload_img') );
						}
						
						$is_portrait = 0;
						if ( $image_size ) {
							if ( $image_size[0] < $image_size[1] ) {
								$is_portrait = 1;
							}
						}

						$photo_arr = array(
							'escort_id' => $escort->id,
							'hash' => $image->getHash(),
							'ext' => $image->getExt(),
							'type' => $is_archived ? ESCORT_PHOTO_TYPE_ARCHIVED : ESCORT_PHOTO_TYPE_HARD,
							'is_portrait' => $is_portrait,
							'width' => $image_size[0],
							'height' => $image_size[1],
							'creation_date' => date('Y-m-d H:i:s', time())
						);

						if ( $client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_NO_PROFILE))) ) {
							$photo_arr['is_approved'] = 1;
						}

						$photo = new Model_Escort_PhotoItem($photo_arr);

						
						$photo = $model->save($photo);

						$new_photos[] = $photo;						
					} catch ( Exception $e ) {
						$upload_errors[$i] = $file['name'] . ' (' . $e->getMessage() . ')';
						Cubix_Debug::logException($e);
					}					
			}

			if ( ! $set_photo ) {
				$this->view->uploadError = Cubix_I18n::translate('sys_error_select_photo');
			}

			$this->view->newPhotos = $new_photos;
			$this->view->uploadErrors = $upload_errors;
		}
		elseif ( 'set-adj' == $action ) {
			$photo_id = intval($this->_getParam('photo_id'));

			if ( ! in_array($photo_id, $photo_ids) ) {
				die(json_encode(array('error' => 'An error occured')));
			}

			$photo = new Model_Escort_PhotoItem(array(
				'id' => $photo_id
			));

			try {
				$hash = $photo->getHash();
				$result = array(
					'x' => intval($this->_getParam('x')),
					'y' => intval($this->_getParam('y')),
					'px' => floatval($this->_getParam('px')),
					'py' => floatval($this->_getParam('py'))
				);
				$photo->setCropArgs($result);

				// Crop All images
				$size_map = array(
					'backend_thumb' => array('width' => 150, 'height' => 205),
					'medium' => array('width' => 225, 'height' => 300),
					'thumb' => array('width' => 150, 'height' => 200),
					'nlthumb' => array('width' => 120, 'height' => 160),
					'sthumb' => array('width' => 76, 'height' => 103),
					'lvthumb' => array('width' => 75, 'height' => 100),
					'agency_p100' => array('width' => 90, 'height' => 120),
					't100p' => array('width' => 117, 'height' => 97)
				);
				$conf = Zend_Registry::get('images_config');

				get_headers($conf['remote']['url'] . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash);
				// echo $conf['remote']['url'] . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash;

				$catalog = $escort_id;
				$a = array();
				if ( is_numeric($catalog) ) {
					$parts = array();

					if ( strlen($catalog) > 2 ) {
						$parts[] = substr($catalog, 0, 2);
						$parts[] = substr($catalog, 2);
					}
					else {
						$parts[] = '_';
						$parts[] = $catalog;
					}
				}
				else if ( preg_match('#^([0-9]+)/(.+)$#', $catalog, $a) ) {
					array_shift($a);
					$catalog = $a[0];

					$parts = array();

					if ( strlen($catalog) > 2 ) {
						$parts[] = substr($catalog, 0, 2);
						$parts[] = substr($catalog, 2);
					}
					else {
						$parts[] = '_';
						$parts[] = $catalog;
					}

					$parts[] = $a[1];
				}

				$catalog = implode('/', $parts);

				foreach($size_map as $size => $sm) {
					get_headers($conf['remote']['url'] . "/" . Cubix_Application::getById()->host . "/" . $catalog . "/" . $hash . "_" . $size . ".jpg?args=" . $result['x'] . ":" . $result['y']);
				}
			}
			catch ( Exception $e ) {
				die(json_encode(array('error' => 'An error occured')));
			}

			die(json_encode(array('success' => true)));
		}
		elseif ( ! is_null($this->_getParam('make_private')) || ! is_null($this->_getParam('make_public')) || ! is_null($this->_getParam('make_archived')) ) {
			
			if ( ! is_null($this->_getParam('make_private')) ) {
				$type = 'private';
			} elseif ( ! is_null($this->_getParam('make_public')) ) {
				$type = 'public';
			} elseif ( ! is_null($this->_getParam('make_archived')) ) {
				$type = 'archived';
			} else {
				die;
			}

			$ids = $this->_getParam('photo_id');

			if ( ! is_array($ids) || ! count($ids) ) {
				return $this->view->actionError = Cubix_I18n::translate('sys_error_select_at_least_on_photo');
			}

			foreach ( $ids as $id ) {
				if ( ! in_array($id, $photo_ids) ) {
					return $this->view->actionError = Cubix_I18n::translate('sys_error_invalid_id_photo');
				}
			}

			foreach ( $ids as $id ) {
				$photo = new Model_Escort_PhotoItem(array('id' => $id));
				if ( $photo->isMain() ) {
					return $this->view->actionError = Cubix_I18n::translate('sys_error_cant_archive_main_photo');
				}
				$photo->make($type == 'archived' ? ESCORT_PHOTO_TYPE_ARCHIVED : ESCORT_PHOTO_TYPE_HARD);
			}

			$this->_loadPhotos();
		}
		elseif ( 'sort' == $action ) {
			$ids = $this->_getParam('photo_id');

			if ( ! is_array($ids) || ! count($ids) ) {
				die(Cubix_I18n::translate('sys_error_select_at_least_on_photo'));
			}

			foreach ( $ids as $id ) {
				if ( ! in_array($id, $photo_ids) ) {
					die(Cubix_I18n::translate('sys_error_invalid_id_photo'));
				}
			}

			$model = new Model_Escort_Photos();
			$model->reorder($ids);

			die;
		}

//			if ( 'upload' != $action ) {
//				$this->view->layout()->disableLayout();
//				$this->_helper->viewRenderer->setNoRender(true);
//				die;
//			}
	}
	
	public function agencyPlainPhotosAction()
	{
		if ( ! $this->user->isAgency() ) {
			$this->_redirect($this->view->getLink('private-v2'));
		}
		
		$client = new Cubix_Api_XmlRpc_Client();
		$model = new Model_Agencies();
		
		$agency = $this->user->getAgency();
		$this->view->agency = $agency;
		$this->view->photos = $photos = $model->loadAgencyPhotos($agency->id, 1, 1000);
		
		if ( $this->_request->isPost() ) 
		{
			if (isset($_POST['upload']))
			{
				$set_photo = false;
				$config = Zend_Registry::get('images_config');
				$upload_errors = array();

				foreach ( $_FILES as $i => $file )
				{
					try {
						if ( ! isset($file['name']) || ! strlen($file['name']) ) {
							continue;
						}
						else {
							$set_photo = true;
						}

						$img_ext = strtolower(@end(explode('.', $file[name])));
						if (!in_array( $img_ext , $config['allowedExts'])){
							throw new Exception(Cubix_I18n::translate('sys_error_upload_img_allowed_extensions', array('extensions' => implode(', ', $config['allowedExts']))), Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
						}

						// Save on remote storage
						$images = new Cubix_Images();
						$image = $images->save($file['tmp_name'], 'agencies', Cubix_Application::getId(), strtolower(@end(explode('.', $file['name']))), $agency->id);
						
						$client->call('Agencies.addPhoto', array($agency->id, $image));
						
						$photos = $model->loadAgencyPhotos($agency->id, 1, 1000);
					} catch ( Exception $e ) {
						$upload_errors[$i] = $file['name'] . ' (' . $e->getMessage() . ')';
						Cubix_Debug::logException($e);
					}					
				}

				if ( ! $set_photo ) {
					$this->view->uploadError = Cubix_I18n::translate('sys_error_select_photo');
				}

				$this->view->uploadErrors = $upload_errors;
			}
			elseif (isset($_POST['delete']))
			{
				$ids = $this->_getParam('photo_id');

				if ( ! is_array($ids) || ! count($ids) ) {
					return $this->view->actionError = Cubix_I18n::translate('sys_error_select_at_least_on_photo');
				}
				
				$photo_ids = array();
				
				if ($photos)
					foreach ($photos as $ph)
						$photo_ids[] = $ph['id'];

				foreach ( $ids as $id ) {
					if ( ! in_array($id, $photo_ids) ) {
						return $this->view->actionError = Cubix_I18n::translate('sys_error_invalid_id_photo');
					}
				}

				$client->call('Agencies.removePhotos', array(implode(',', $ids)));
				
				$photos = $model->loadAgencyPhotos($agency->id, 1, 1000);
			}
		}
				
		$this->view->photos = $photos;
	}
	
	public function escortsAction()
	{
		if ( ! $this->user->isAgency() ) {
			$this->_redirect($this->view->getLink('private-v2'));
		}

		$this->view->s_showname = '';
		$this->view->is_active = (bool) $this->_getParam('is_active');
		
		$defs = Zend_Registry::get('definitions');
		$sort_filter_tructure  = $defs['agency_escorts_sort'];
		
		$sort_filter = new Cubix_NestedMenu(array('childs' => $sort_filter_tructure));
		$sort_filter->setId('sort-options');
		$sort_filter->setSelected( $sort_filter->getByValue('paid-package') );
		
		$page_filter_structure = $defs['agency_escorts_pages'];
		$per_page_filter = new Cubix_NestedMenu(array('childs' => $page_filter_structure));
		$per_page_filter->setId('per-page-options');
		$per_page_filter->setSelected( $per_page_filter->getByValue(9) );

		$client = new Cubix_Api_XmlRpc_Client();

		if ( $this->_request->isPost() ) {
			$action = $this->_getParam('a');

			if ( $this->user->isEscort() ) {
				$escort = $this->user->getEscort();
				$escort_id = $escort->id;
			}
			else {
				$escort_id = intval($this->_getParam('escort_id'));

				if ( 0 == $escort_id ) $escort_id = null;

				if ( ! is_null($escort_id) && ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
					die;
				}
				
							
				$model = new Model_Escorts();
				$escort = $model->getById($escort_id);
			}

			switch ( $action ) {
				case 'activate':
					$this->view->actionError = '';
					if ( $escort->status & Model_Escorts::ESCORT_STATUS_ADMIN_DISABLED ) {
						$this->view->actionError .= 'The escort is deactivated by administration!<br/>';
					}

					if ( $escort->status & Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS ) {
						$this->view->actionError .= 'You cannot activate this escort, not enough photos!<br/>';
					}


					if ( $escort->status & Model_Escorts::ESCORT_STATUS_IS_NEW || $escort->status & Model_Escorts::ESCORT_STATUS_NO_PROFILE ) {
						$this->view->actionError .= 'The profile of this escort has not been approved yet!<br/>';
					}

					if ( ! strlen($this->view->actionError) ) {
						$client->call('Escorts.activate', array($escort_id));
						
						$this->view->escorts = $escorts = $this->agency->getEscorts();
					}
					
					break;
				case 'deactivate':
					$client->call('Escorts.deactivate', array($escort_id));

					$this->view->escorts = $escorts = $this->agency->getEscorts();

					break;
				case 'delete':
					// TODO: escort products deletion. Premium package assigned escorts error.
					$client->call('Escorts.deleteTemporary', array($escort_id));
					$this->view->escorts = $escorts = $this->agency->getEscorts();
					break;
				default:
					
			}
		}

		$agency = new Model_AgencyItem(array('id' => $this->agency->getId()));
		$linked_agencies = $agency->getLinkedAgencies();
		$has_linked_agencies = false;
		if ( count( $linked_agencies  ) ) {
			$has_linked_agencies = true;
		}

		$this->view->has_linked_agencies = $has_linked_agencies;
		
        $per_page = 9;
        $cur_page = 1;

        $escorts_a = $this->agency->getEscortsPerPage($cur_page, $per_page, 1); //32 = Active 
        $escorts_i = $this->agency->getEscortsPerPage($cur_page,$per_page);//default inactive
        $escorts_d = $this->agency->getEscortsPerPage($cur_page,$per_page,-1);//default inactive
		
        $this->view->escorts_a_count = $escorts_a['escorts_count'];
        $this->view->escorts_i_count = $escorts_i['escorts_count'];
        $this->view->escorts_d_count = $escorts_d['escorts_count'];

        $this->view->escorts_per_page = $per_page;
        $this->view->current_page = $cur_page;

        $this->view->escorts_active = $escorts_a['escorts'];
        $this->view->escorts_inactive = $escorts_i['escorts'];
        $this->view->escorts_deleted = $escorts_d['escorts'];

		$this->view->agency_id = $this->agency->getId();
		$this->view->sort_filter = $sort_filter;
		$this->view->per_page_filter = $per_page_filter;

	}

	private function _getPremiumList()
	{
		return Cubix_Api::getInstance()->call('premium_getAgencyPremiumEscorts', array($this->agency->id, $this->view->lang()));
	}
	
	private function _getGotdList()
	{
		return Cubix_Api::getInstance()->call('premium_getEscortsWithGotd', array($this->agency->id, $this->view->lang()));
	}

	public function premiumAction()
	{
		if ( $this->user->isAgency() ) {
			$this->_helper->viewRenderer->setScriptAction('agency-premium');
			
			$this->view->escorts = $this->_getPremiumList();
			$this->view->gotds = $this->_getGotdList();
			
		}
		elseif ( $this->user->isEscort() ) {
			$this->_helper->viewRenderer->setScriptAction('escort-premium');
		}
	}

	public function ajaxPremiumSwitchAction()
	{
		// Require user to be an agency
		if ( ! $this->user->isAgency() ) die;

		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'from_escort_id' => 'int-nz',
			'to_escort_id' => 'int-nz',
			'city_ids' => 'arr-int'
		));
		$data = $form->getData();

		// If escort does not have any of these escorts
		if ( Cubix_Application::getId() != 16 ) {
			if ( ! $this->agency->hasEscort($data['from_escort_id']) ||
					! $this->agency->hasEscort($data['to_escort_id']) ) {
				die;
			}
		}

		$client = Cubix_Api::getInstance();
		$result = $client->call('premium_switchActivePackages', array($data['from_escort_id'], $data['to_escort_id'], $data['city_ids']));
		
		die(json_encode(array('data' => $this->_getPremiumList(), 'status' => 'success')));
	}
	
	public function ajaxGotdSwitchAction()
	{
		// Require user to be an agency
		if ( ! $this->user->isAgency() ) die;

		$from = $this->_request->from_escort_id;
		$to = $this->_request->to_escort_id;
		
		$client = Cubix_Api::getInstance();
		$result = $client->call('premium_switchGotd', array($from, $to));
		
		die(json_encode(array('data' => $this->_getGotdList(), 'status' => 'success')));
	}

    /* Update Grigor */
    public function ajaxGetAgencyEscortsAction(){
        if ( ! $this->user->isAgency() ) die;

        $this->view->layout()->disableLayout();
		
		$per_page = $this->_getParam('per_page') ? intval($this->_getParam('per_page')) : 9 ;
        $page = intval($this->_getParam('page'));
        $showname = $this->view->showname = $this->_getParam('showname');
        $isActive = intval($this->_getParam('is_active'));
		$sort = $this->view->sort = $this->_getParam('sort');
        
        $status = null;
        $view = "ajax-get-agency-escorts";
		
		switch($sort){
			case 'paid-package':
				$sort = 'p.name DESC';
				BREAK;
			
			case 'alpha':
				$sort = 'e.showname';
				BREAK;
			
			case 'escort-id':
				$sort = 'e.id';
				BREAK;
			
			case 'last-modified':
				$sort = 'e.date_last_modified';
				BREAK;
				
		}
		
        if($isActive){
           $status =  $isActive;
        }

        $client = new Cubix_Api_XmlRpc_Client();
		$escorts = $client->call('Agencies.getEscortsByStatus', array($this->agency->id,$page,$per_page,$status,false, $showname, $sort));

        $cur_page = $page;
		
		$agency = new Model_AgencyItem(array('id' => $this->agency->getId()));
		$linked_agencies = $agency->getLinkedAgencies();
		$has_linked_agencies = false;
		if ( count( $linked_agencies  ) ) {
			$has_linked_agencies = true;
		}

		$this->view->has_linked_agencies = $has_linked_agencies;
        $this->view->escorts_count = $escorts['escorts_count'];
        $this->view->escortStatus = $status;

        $this->view->escorts_per_page = $per_page;
        $this->view->current_page = $cur_page;

        $this->view->escorts = $escorts['escorts'];


        $this->_helper->viewRenderer->setScriptAction($view);


    }


    public function ajaxEscortsDoAction(){
        $action = $this->_getParam('a');
        $client = new Cubix_Api_XmlRpc_Client();


        $escort_id = intval($this->_getParam('escort_id'));

        if ( 0 == $escort_id ) $escort_id = null;

        if ( ! is_null($escort_id) && ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
            die;
        }

        $model = new Model_Escorts();
        $escort = $model->getById($escort_id);

        $return['success'] = false;
        $return['message'] = '';

        
        if($escort){
            
            switch ( $action ) {
                case 'activate':

                    if ( $escort->status & Model_Escorts::ESCORT_STATUS_ADMIN_DISABLED ) {
                        $return['message'] .= 'The escort is deactivated by administration!<br/>';
                    }

                    if ( $escort->status & Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS ) {
                        $return['message'] .= 'You cannot activate this escort, not enough photos!<br/>';
                    }


                    if ( $escort->status & Model_Escorts::ESCORT_STATUS_IS_NEW || $escort->status & Model_Escorts::ESCORT_STATUS_NO_PROFILE ) {
                        $return['message'] .= 'The profile of this escort has not been approved yet!<br/>';
                    }



                    if ( ! strlen($return['message']) ) {
                        $client->call('Escorts.activate', array($escort_id));

                        $return['success'] = true;
                    }

                    break;
                case 'deactivate':
                    $client->call('Escorts.deactivate', array($escort_id));

                    $return['success'] = true;

                    break;
                case 'delete':
                    // TODO: escort products deletion. Premium package assigned escorts error.
                    
                    $res = $client->call('Escorts.deleteTemporary', array($escort_id));
                    if($res){
                        $res = json_decode($res);
                        
                        if ( !$res->success ) {
                            $return['message'] = $res->message;
                        }
                    }
                    
                   // $this->view->escorts = $escorts = $this->agency->getEscorts();
                    break;
                case 'restore':
                    // TODO: escort restore
                    $client->call('Escorts.restore', array($escort_id));
                    $return['success'] = true;
                   // $this->view->escorts = $escorts = $this->agency->getEscorts();
                    break;
                default:

            }
        }

        die(json_encode($return));
    }
    /* Update Grigor */


	public function ajaxEscortsAction()
	{
		// Require user to be an agency
		if ( ! $this->user->isAgency() ) die;

		$escorts = Cubix_Api::getInstance()->call('premium_getLinkedAgencyNonPremiumEscorts', array($this->agency->id));
		die(json_encode($escorts));
	}
	
	public function ajaxGotdAction()
	{
		if ( ! $this->user->isAgency() ) die;

		$selected = $this->_request->sel;
		$escorts = Cubix_Api::getInstance()->call('premium_getEscortsForGotdSwitch', array($this->agency->id, $selected));
		
		die(json_encode($escorts));
	}

	public function ajaxEscortCitiesAction()
	{
		// Require user to be an agency
		if ( ! $this->user->isAgency() ) die;

		$escort_id = intval($this->_getParam('escort_id'));

		// If this escort does not belong to this agency
		if ( ! $this->agency->hasEscort($escort_id) ) die;

		$cities = Cubix_Api::getInstance()->call('premium_getEscortCities', array($escort_id, $this->view->lang()));
		$limit = Cubix_Api::getInstance()->call('premium_getEscortCitiesLimit', array($escort_id));

		die(json_encode(array('cities' => $cities, 'limit' => $limit)));
	}

	public function getBubbleTextAction()
	{
		if ( ! $this->user->isAgency() && ! $this->user->isEscort() ) {
			die();
		}

		$req = $this->_request;

		if ( $this->user->isEscort() ) {
			$escort = $this->user->getEscort();
			$escort_id = $escort->id;
		}
		else {
			$escort_id = intval($req->escort_id);
		}

		//$ivr_url = $this->getBubbleIvrUrl(5, $escort_id);
		
		/*$client = Cubix_Api_XmlRpc_Client::getInstance();
		$bubble = $client->call('Escorts.getBubbleText', array($escort_id));*/
		
		$bubble = Cubix_Api::getInstance()->call('getBubbleText', array($escort_id));
		
		if (is_array($bubble) && array_key_exists('error', $bubble))
			file_put_contents('/var/log/bbt.log', date('d M, Y H:i:s') . ' - function (getBubbleTextAction) - ' . $bubble['error'] . "\n\n", FILE_APPEND);
		
		//$bubble['ivr_url'] = $ivr_url;
		
		die(json_encode($bubble));
	}

	public function addBubbleTextAction()
	{
		$config_system = Zend_Registry::get('system_config');
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		
		if ( $this->user->isEscort() ) {
			$escort = $this->user->getEscort();
			$escort_id = $escort->id;
		}
		else {
			$escort_id = intval($this->_getParam('escort_id'));
			if ( ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
				die;
			}
		}

		$req = $this->_request;

		$text = strip_tags(mb_substr(stripslashes($req->text), 0, 1000, 'UTF-8'));
		//$text = $req->text;

		$data = array('escort_id' => $escort_id, 'text' => $text);
		if ( strlen($text) > 0 ) {
			$model_bl_words = new Model_BlacklistedWords();
			if($model_bl_words->checkWords($text, Model_BlacklistedWords::BL_TYPE_BUBBLE_TEXT)){
				echo 'You can`t use word "'.$model_bl_words->getWords().'"';
				die;
			}
			$data = array_merge($data, array('status' => 1));
		}
		else {
			$data = array_merge($data, array('status' => 0));
		}
		
		//$status = $client->call('Escorts.addBubbleText', array($data));
		//$status = $client->call('Escorts.addBubbleText', array($data, $config_system['bubbleTextApprovation']));
		
		$status = Cubix_Api::getInstance()->call('addBubbleText', array($data, $config_system['bubbleTextApprovation']));
		
		if (is_array($status) && array_key_exists('error', $status))
			file_put_contents('/var/log/bbt.log', date('d M, Y H:i:s') . ' - function (addBubbleTextAction) - ' . $status['error'] . "\n\n", FILE_APPEND);

		echo $status;

		die;
	}
	
	public function addPaidBubbleTextAction()
	{
		$config_system = Zend_Registry::get('system_config');
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		
		if ( $this->user->isEscort() ) {
			$escort = $this->user->getEscort();
			$escort_id = $escort->id;
		}
		else {
			$escort_id = intval($this->_getParam('escort_id'));
			if ( ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
				die;
			}
		}
		
		$bubble = Cubix_Api::getInstance()->call('getBubbleText', array($escort_id));

		$req = $this->_request;

		$text = strip_tags(mb_substr(stripslashes($req->text), 0, 1000, 'UTF-8'));
		//$text = $req->text;

		$data = array('escort_id' => $escort_id, 'text' => $text);
		if ( strlen($text) > 0 /*&& $bubble['text'] != $text*/ ) {
			$tmp_id = Cubix_Api::getInstance()->call('addPaidBubbleText', array($data, $config_system['bubbleTextApprovation']));
			$ivr_url = $this->getBubbleIvrUrl(5, $tmp_id);
			$ret = array('status' => 'success', 'ivr_url' => $ivr_url);
		}
		else {
			$ret = array('status' => 'error', 'msg' => Cubix_I18n::translate('change_bubble_text'));
		}		

		die(json_encode($ret));
	}
	
	public function bubbleSuccessAction()
	{
		$this->view->layout()->setLayout('private-v2'); 
		$this->view->layout()->global_btn_back_url = self::$linkHelper->getLink('private-v2');
		$this->_helper->viewRenderer->setScriptAction('bubble-status');
		$this->view->key = 'bubble_message_payment_success';
	}
	
	private function getBubbleIvrUrl($amount, $ref_id)
	{
		if ( ! $amount || ! $ref_id ) {
			die;
		}	
		
		$amount *= 100;
						
		$c_url = 'http://' . $_SERVER['SERVER_NAME'] . self::$linkHelper->getLink('private-v2');
				
		$conf = Zend_Registry::get('ivr');
		$urls = array(
			'base_url'	=> 'http://ivr.truesenses.com/?cmd=new&acc=' . $conf['acc'] . '&aid=' . $conf['aid'] . '&amo=' . $amount . '&cur=chf&ser=' . $conf['acc'] . '&lan=de&cid=' . $ref_id,
			'success_url'	=> 'srl=' . 'http://' . $_SERVER['SERVER_NAME'] . '/private-v2/bubble-success',
			'failure_url'	=> 'frl=' . 'http://' . $_SERVER['SERVER_NAME'] . self::$linkHelper->getLink('private-v2'),
			'cancel_url'	=> 'crl=' . $c_url
		);
		
		$url = implode('&', $urls);
		
		return $url;
	}
	
	public function bubbleTextLogAction()
	{
		if ($this->_request->isPost())
		{			
			file_put_contents('/var/log/bbt.log', date('d M, Y H:i:s') . ' - function (bubbleTextLogAction) - ' . 'response: ' . $this->_request->response . " - " . 'error: ' . $this->_request->error . "\n\n", FILE_APPEND);
		}
		
		die;
	}

	public function existsByShownameAction()
	{
		$model = new Model_EscortsV2();

		$req = $this->_request;

		if ( $this->user->isEscort() ) {
			$escort = $this->user->getEscort();
			$escort_id = $escort->id;
		}
		else {
			$escort_id = intval($this->_getParam('escort'));

			if ( $escort_id > 0 ) {
				if ( ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
					die;
				}
			}
		}

		//$escort_id = $req->getParam('escort_id', false);
		$showname = $req->getParam('showname', false);

		$status = $model->existsByShowname($showname, $escort_id);

		if ( $status ) {
			$stat = 'true';
		}
		else {
			$stat = 'false';
		}

		die(json_encode(array('status' => $stat)));
	}


    public function escortActivePackageAction()
	{

        $this->view->layout()->disableLayout();
		//$this->_helper->viewRenderer->setScriptAction('agency-premium');
		
        $escort_id  = $this->_request->getParam('escort_id');
        $isActive       = $this->_request->getParam('isActive');
        
        if ( $this->user->isAgency() AND $this->agency->hasEscort($escort_id) ) {
			
			$package = $this->_getEscortActivePackage($escort_id);
            $this->view->package = $package;
            $this->view->isActive = $isActive;
		}

		
	}

    private function _getEscortActivePackage($escort_id){
        return Cubix_Api::getInstance()->call('getEscortActivePackage', array($escort_id,Cubix_I18n::getLang()));
    }

	public function clientBlacklistAction()
	{
		$req = $this->_request;

		$page = 1;
		$per_page = 10;
		if($this->_getParam('page')){
			$page = $this->_getParam('page');
		}

		$criteria = $this->view->criteria = $req->client_criterias;
		
		$params = array($criteria, $page, $per_page, $req->lang_id);
		$data = Cubix_Api::getInstance()->call('getBlacklistedClients', array($params));

		$result = $this->view->result = $data['clients'];
		$count = $this->view->count = $data['count'];
		$this->view->page = $page;
		$this->view->per_page = $per_page;		
	}

	public function addClientToBlacklistAction()
	{
		$req = $this->_request;
		
		$validator = new Cubix_Validator();
		$data = array();
		
		if ( $req->isPost() )
		{
			$date = intval($req->date);
			$client_name = $req->client_name;
			$client_phone = $req->client_phone;
			$comment = $req->comment;
			$city_id = $req->city_id_h;

			if ( ! $city_id ) {
				$city_id = null;
			}

			if ( ! $date ) {
				$validator->setError('date', 'Date is Required');
			}
			else if ( $date > time() ) {
				$validator->setError('date', 'Date is on Future');
			}

			if ( ! strlen($client_name) && ! strlen($client_phone) ) {
				$validator->setError('name_or_phone', 'Client Name or Phone is Required');
			}

			if ( ! strlen($comment) ) {
				$validator->setError('err_comment', 'Comment is Required');
			}

			if ( $validator->isValid() ) {

				$data = array(
					'application_id' => Cubix_Application::getId(),
					'user_id' => $this->user->id,
					'date' => $date,
					'client_name' => $client_name,
					'city_id' => $city_id,
					'client_phone' => $client_phone,
					'comment' => $comment
				);
				
				Cubix_Api::getInstance()->call('addClientToBlacklist', array($data));
			}

			die(json_encode($validator->getStatus()));
		}
	}

	public function faqAction()
	{
		$lng = Cubix_I18n::getLang();
				
		$type = $this->user->user_type;

		$config = Zend_Registry::get('faq_config');
		$cache = Zend_Registry::get('cache');
		$cache_key = 'v2_faq_type_' . $type . '_lang_' . $lng;

		if ( ! $items = $cache->load($cache_key) )
		{
			$items = Cubix_Api::getInstance()->call('getFaqByType', array($type, $lng));
			
			$cache->save($items, $cache_key, array(), $config['cacheTime']);
		}

		$this->view->items = $items;
	}
	
	public function areasAction()
	{
		if ( $this->user->isAgency() ) {
			$user_type = 'agency';
			$items = Cubix_Api::getInstance()->call('addareas_getEscorts', array($this->agency->id, $user_type));
			$this->view->escorts = $items;
		}
		else if ( $this->user->isEscort() ) {
			$user_type = 'escort';
			
			$items = Cubix_Api::getInstance()->call('addareas_getEscorts', array($this->escort->id, $user_type));
			$this->view->escorts = $items;
		}
	}
	
	public function editAreaAction()
	{
		 $this->view->layout()->disableLayout();
		 $escort_id = $this->_request->escort_id;
		 
		 if ( $this->user->isEscort() && $this->escort->id != $escort_id ) die;
		 
		 $this->view->escort_id = $escort_id;

		 $data = $this->view->data = Cubix_Api::getInstance()->call('addareas_getEscortData', array($escort_id));
		 $add_areas = $this->view->add_areas = Cubix_Api::getInstance()->call('addareas_getAddAreas', array($escort_id, $data['order_package_id']));
		 
		 if ( $this->_request->isPost() ) {
			 
			 $validator = new Cubix_Validator();
			 
			 $add_areas = $this->_request->add_areas;
			 
			 if ( ! count($add_areas) ) {
				$validator->setError('area_id', "Please select at least one area!");
			 }
			 
			 if ( count($add_areas) > $data['add_areas_count'] ) {
				$validator->setError('area_id', "You can select only " . $data['add_areas_count'] . " area!");
			 }
			 
			 if ( $validator->isValid() ) {
				 Cubix_Api::getInstance()->call('addareas_updateEscortAreas', array($add_areas, $escort_id, $data['order_package_id']));
			 }
			 
			die(json_encode($validator->getStatus()));
		 }
	}
	
	public function suspendResumeAction()
	{
		$this->view->layout()->disableLayout();
		
		$action = $this->view->a = $this->_request->a;
		$op_id = $this->view->op_id = (int) $this->_request->op_id;
		
		$date = $this->_request->date;
		
		if ( $this->_request->isPost() ) {
			
			$validator = new Cubix_Validator();
			
			if ( $date && mktime(date('H', $date), date('i', $date), 0, date('m', $date), date('d', $date), date('Y', $date)) <= mktime(date('H', time()), date('i', time()), 0, date('m', time()), date('d', time()), date('Y', time())) ) {
				$validator->setError('date', 'Date must be in future');
			}
			
			if ( $validator->isValid() ) {
				if ( $action == 'suspend' ) {
					Cubix_Api::getInstance()->call('suspresume_SuspendPackage', array($op_id, $date));
				}
				else if ( $action == 'resume' ) {
					Cubix_Api::getInstance()->call('suspresume_ResumePackage', array($op_id, $date));
				}
			}
			
			die(json_encode($validator->getStatus()));
		}		
	}
	
	public function getLinkedAgenciesAction()
	{
		$this->view->layout()->disableLayout();
		$agency = new Model_AgencyItem(array('id' => $this->agency->id));
		
		$linked_agencies = $agency->getLinkedAgencies();
		$this->view->agencies = $linked_agencies;
		
	}
	
	public function changeEscortAgencyAction()
	{
		$agency_id = $this->_request->agencyId;
		$escort_id = $this->_request->escortId;
		
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$result = $client->call('Escorts.switchAgency', array($escort_id, $agency_id));
		die;
	}
	
	public function susresDetailsAction()
	{
		$this->view->layout()->disableLayout();
		
		$op_id = $this->view->op_id = (int) $this->_request->op_id;
		
		$this->view->details = Cubix_Api::getInstance()->call('suspresume_Details', array($op_id));
	}
	
	public static function _sortItemDecorator($item, $is_first = FALSE, $is_hidden = FALSE)
	{
		$lng = Cubix_I18n::getLang();
		$has_childs = isset($item->childs) && is_array($item->childs) && count($item->childs);
		
		$classes = array();
		
		if ( $is_first ) $classes[] = 'first';
		if ( $has_childs ) $classes[] = 'sub';
		
		$link = '#';
		if ( ! $has_childs ) {
			$link = self::$linkHelper->getLink('escorts', array('sort' => $item->value));
		}
		
		$title = $item->title;
		if ( $is_first && isset($item->title) ) {
			$title = $item->parent . ' - ' . $item->title;
		}

		$html = '<a ' . ($is_hidden ? 'style="display: none" ' : '') . '' . (count($classes) ? 'class="' . implode(' ', $classes) . '" ' : '') . 'href="#"' . ($item->value ? ' rel="' . $item->value . '"' : '') . ' ' . ( true ? 'onclick="return Cubix.Agencyescort.Load({lang: \'' .$lng. '\', page: 1 , is_active: 1, sort:\'' . $item->value .'\'})"' : '' ) . '>' . $title . '</a>';

		if ( $is_first ) {
			$html = '<div class="input-w"><div class="i">' .
				$html .
			'</div></div>';
		}

		return $html;
	}
	
	public static function _perPageItemDecorator($item, $is_first = FALSE, $is_hidden = FALSE)
	{
		$lng = Cubix_I18n::getLang();
		$has_childs = isset($item->childs) && is_array($item->childs) && count($item->childs);
		
		$classes = array();
		
		if ( $is_first ) $classes[] = 'first';
		if ( $has_childs ) $classes[] = 'sub';
		
		$link = '#';
		if ( ! $has_childs ) {
			$link = self::$linkHelper->getLink('escorts', array('sort' => $item->value));
		}
		
		$title = $item->title;
		if ( $is_first && isset($item->title) ) {
			$title = $item->parent . ' - ' . $item->title;
		}

		$html = '<a ' . ($is_hidden ? 'style="display: none" ' : '') . '' . (count($classes) ? 'class="' . implode(' ', $classes) . '" ' : '') . 'href="#"' . ($item->value ? ' rel="' . $item->value . '"' : '') . ' ' . ( true ? 'onclick="return Cubix.Agencyescort.Load({lang: \'' .$lng. '\', page: 1 , is_active: 1, per_page:\'' . $item->value .'\'})"' : '' ) . '>' . $title . '</a>';

		if ( $is_first ) {
			$html = '<div class="input-w"><div class="i">' .
				$html .
			'</div></div>';
		}

		return $html;
	}

	public function onlineAgencyEscortsAction()
	{
		$this->view->layout()->disableLayout();
		
		if ($this->user->user_type != 'agency')
			die;
		
		$m = new Model_EscortsV2();
		$this->view->escorts = $m->getAgencyPaidEscorts($this->user->id);
		$online_escorts = $m->getAgencyOnlineEscorts($this->user->id);
		$oe = array();
		
		if ($online_escorts)
		{
			foreach ($online_escorts as $o)
				$oe[] = $o->escort_id;
		}
		
		$this->view->online_escorts = $oe;
		
		if ($this->_request->isPost()) 
		{
			$escs = $this->_request->escs;
			
			if (strlen($escs) > 0)
			{
				$escs_arr = explode(',', $escs);
				
				$m->addAgencyOnlineEscorts($this->user->id, $escs_arr);
			}
			
			die;
		}
	}
	
	public function testtestAction()
	{
		for ( $i = 0; $i < 50; $i++ ) {
			
			
			$client = new Cubix_Api_XmlRpc_Client();
			//$client->setSkipSystemLookup(false);
			
			$public_photos = $client->call('Escorts.getEscortPhotosList', array(6329, false));
			$archived_photos = $client->call('Escorts.getEscortArchivedPhotosList', array(6329));
					
			echo $i . "<br/>";
		}
		
		die;
	}
	
	public function loadActiveAgencyEscortsAction()
	{
		$escorts = $this->agency->getEscorts();
		
		die(json_encode($escorts));
	}
	
	public function getRejectedVerificationAction()
	{
		$this->view->layout()->disableLayout();
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		
		if ($this->user->isEscort())
		{
			$escort_id = $this->user->getEscort()->id;
            $ret = $client->call('Escorts.getRejectedVerificationForEscort', array($escort_id));
			
			if ($ret && is_array($ret))
			{
				$ids_arr = explode(',', $ret['reason_ids']);
				$message = '';
				
				foreach ($ids_arr as $id)
				{
					if ($id == 4)
						$message .= $ret['reason_text'] . '<br><br>';
					else
						$message .= $this->view->t('verify_reject_reason_' . $id) . '<br><br>';
				}
				
				$this->view->message = $message;
				$this->view->type = 'escort';
			}
			else
				die;
		}
		elseif ($this->user->isAgency())
		{
			$user_id = $this->user->id;
			$ret = $client->call('Escorts.getRejectedVerificationForAgency', array($user_id));
			
			if ($ret && is_array($ret))
			{
				$messages = array();
				
				foreach ($ret as $sh => $r)
				{
					$ids_arr = explode(',', $r['reason_ids']);
					$message = '';
					
					foreach ($ids_arr as $id)
					{
						if ($id == 4)
							$message .= $r['reason_text'] . '<br><br>';
						else
							$message .= $this->view->t('verify_reject_reason_' . $id) . '<br><br>';
					}
					
					$messages[$sh] = $message;
				}
				
				$this->view->message = $messages;
				$this->view->type = 'agency';
			}
			else
				die;
		}
		else
			die;
	}
	
	public function setRejectedVerificationAction()
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		
		if ($this->user->isEscort())
		{
			$escort_id = $this->user->getEscort()->id;
			$client->call('Escorts.setRejectedVerificationForEscort', array($escort_id));
		}
		elseif ($this->user->isAgency())
		{
			$user_id = $this->user->id;
			$client->call('Escorts.setRejectedVerificationForAgency', array($user_id));
		}
				
		die;
	}
}
