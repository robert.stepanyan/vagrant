<?php

class AgenciesController extends Zend_Controller_Action
{
	public static $linkHelper;
	
	public function init()
	{
		self::$linkHelper = $this->view->getHelper('GetLink');
	}
	
	public function indexAction()
	{
		   
	}
	
	public function showAction()
	{
		if ( ! is_null($this->_getParam('ajax')) ) {
			$this->view->layout()->disableLayout();
			$this->view->ajax = true;
		}
		else {
			$this->view->layout()->setLayout('main-simple');
		}

		$agency_slug = $this->_getParam('agencyName');
		$agency_id = intval($this->_getParam('agency_id'));

		$client = new Cubix_Api_XmlRpc_Client();
		$agency = $client->call('Agencies.getInfo', array($agency_slug, $agency_id));

		if ( ! $agency ) {
			// 404
			die();
		}
		elseif ( isset($agency['error']) ) {
			print_r($agency['error']); die;
		}

		$agency = $this->view->agency = new Model_AgencyItem($agency);
		
		// load photos
		$config = Zend_Registry::get('agencies_config');
		$this->view->photos_perPage = $per_page = $config['photos']['perPage'];
		$model = new Model_Agencies();
		$this->view->photos = $model->loadAgencyPhotos($agency['id'], 1, $per_page, $count);
		
		$this->view->photos_count = $count;
		$this->view->photos_page = 1;
		$this->view->agency_id = $agency['id'];
		//
		
		// load commencts
		$this->view->comments_perPage = $per_page = $config['comments']['perPage'];
		$comments = $client->call('Agencies.getComments', array($agency_id, 1, $per_page));
		
		$this->view->comments = $comments['list'];
		$this->view->comments_count = $comments['count'];
		$this->view->comments_page = 1;
		$this->view->agency_id = $agency['id'];
		//

		/* --> Extract page from `req` parameter */
		$req = ltrim($this->_getParam('req'), '/');
		$req = explode('_', $req);

		$page = 1;
		$param_name = $req[0];

		if ( $param_name == 'page' ) {
			$page = $req[1];
		}

		if ( $page < 1 ) $page = 1;
		/* <-- */

		$config = Zend_Registry::get('escorts_config');
		
		$filter = array('e.agency_id = ?' => $agency->id);
		
		$count = 0;
		$escorts = Model_Escort_List::getFiltered($filter, 'last-modified', $page, $config['perPage'], $count);

		if ( isset($escorts[0]) && $escorts[0] ) {
			$this->view->agency_last_mod_date = $escorts[0]->date_last_modified;
		}
		
		$this->view->escorts = $escorts;
		$this->view->count = $count;
		$this->view->params = array('page' => $page);

		$this->view->no_paging = true;
	}
	
	public function photosAction()
	{
		$config = Zend_Registry::get('agencies_config');
		$this->view->photos_perPage = $per_page = $config['photos']['perPage'];
		
		$page = $this->_getParam('photo_page');

		$agency_id = $this->_getParam('agency_id');
		$mode = $this->_getParam('mode');
		
		if( $mode == 'ajax' ) {
			$this->view->layout()->disableLayout();
			$this->view->agency_id = $agency_id;
		}
		
		if ( ! $page )
			$page = 1;
		
		$count = 0;
		
		$m = new Model_Agencies();
		$photos = $m->loadAgencyPhotos($agency_id, $page, $per_page, $count);
		
		$this->view->photos = $photos;
		$this->view->photos_count = $count;
		$this->view->photos_page = $page;
	}
	
	public function commentsAction()
	{
		$config = Zend_Registry::get('agencies_config');
		$this->view->comments_perPage = $per_page = $config['comments']['perPage'];
		
		$page = $this->_getParam('page');

		$agency_id = $this->_getParam('agency_id');
		$mode = $this->_getParam('mode');
		
		if( $mode == 'ajax' ) {
			$this->view->layout()->disableLayout();
			$this->view->agency_id = $agency_id;
		}
		
		if ( ! $page )
			$page = 1;
		
		$count = 0;
		
		$client = new Cubix_Api_XmlRpc_Client();
		$comments = $client->call('Agencies.getComments', array($agency_id, $page, $per_page));
		
		$this->view->comments = $comments['list'];
		$this->view->comments_count = $comments['count'];
		$this->view->comments_page = $page;
	}
}
