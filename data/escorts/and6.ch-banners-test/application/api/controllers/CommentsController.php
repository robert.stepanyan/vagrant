<?php

class Api_CommentsController extends Zend_Controller_Action
{
	/**
	 * @var Zend_Db_Adapter_Mysqli
	 */
	protected $_db;
	
	public function init()
	{
		$this->_db = Zend_Registry::get('db');
		
		$this->view->layout()->disableLayout();
	}

	public function indexAction()
	{
		
	}
	
	protected function _call($method, $params = array())
	{
		$url = Cubix_Api_XmlRpc_Client::getServer() . '/?api_key=' . Cubix_Api_XmlRpc_Client::getApiKey();
		$url .= '&method=' . $method;

		foreach ( $params as $i => $param ) {
			$params[$i] = 'params[]=' . urlencode($param);
		}

		$url .= '&' . implode('&', $params);

		return unserialize(file_get_contents($url));
	}

	public function plainAction()
	{
		$this->_db->query('TRUNCATE comments');

		foreach ( $this->_call('getActiveComments') as $comment ) {
			$this->_db->insert('comments', $comment);
		}
		
		die;
	}

    public function blockedWordsAction(){
        $this->_db->query('TRUNCATE blacklisted_words');
        $words = $this->_call('getBlockedWords');
        if( count($words) > 0 ){
            foreach ( $words as $word ) {
                $this->_db->insert('blacklisted_words', $word);
            }
        }

        $this->_db->query('TRUNCATE blacklisted_emails');
        $words = $this->_call('getBlockedEmails');
        if( count($words) > 0 ){
            foreach ( $words as $word ) {
                $this->_db->insert('blacklisted_emails', $word);
            }
        }

		die;
    }

}
