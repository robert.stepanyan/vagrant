<?php

class Model_Currencies extends Cubix_Model
{
	static public function getAll()
	{
		$cache_key = Cubix_Application::getId() . '_currencies_list';
		
		if ( ! $currencies = self::cache()->load($cache_key) ) {
			$currencies = self::db()->fetchAll('
				SELECT *
				FROM currencies
				ORDER BY is_default DESC, title
			');
			self::cache()->save($currencies, $cache_key, array(),
					self::config()->currencies->cache_lifetime);
		}

		return $currencies;
	}

	const GET_TITLE = 1;
	const GET_SYMBOL = 2;

	static public function getAllAssoc($part = self::GET_TITLE)
	{
		$key = 'id';
		switch ( $part ) {
			case self::GET_TITLE:
				$key = 'title';
				break;
			case self::GET_SYMBOL:
				$key = 'symbol';
				break;
		}

		$result = array();
		foreach ( self::getAll() as $c ) {
			$result[$c->id] = $c->$key;
		}

		return $result;
	}
}
