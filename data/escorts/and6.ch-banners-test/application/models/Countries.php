<?php

class Model_Countries extends Cubix_Model {

	protected $_table = 'countries';

	public function getCountries($exclude = array()) {
		$where = '';
		if (!empty($exclude)) {
			$where = implode(',', $exclude);
			$where = ' WHERE id NOT IN (' . $where . ')';
		}
		$sql = '
			SELECT c.id,
				c.iso,
				c.slug,
				c.' . Cubix_I18n::getTblField('title') . ' as title
			FROM countries c
			' . $where . '
		';

		return parent::_fetchAll($sql);
	}

	public function getBlockCountries( $escort_id ){
		$client = Cubix_Api_XmlRpc_Client::getInstance();

		return $client->call('Escorts.getBlockCountries', array($escort_id));
	}

	public function getCountryIsoByIp( $ip ){
		if (!$ip)
        {
            if(isset($_SERVER['HTTP_CLIENT_IP']))
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            else if(isset($_SERVER['REMOTE_ADDR']))
                $ip = $_SERVER['REMOTE_ADDR'];
            else
                $ip = 'UNKNOWN';
        }

        $geoip = geoip_country_code_by_name($ip);

		return $geoip;
	}
	public function getPhonePrefixs(){
		$result =  $this->_db->fetchAll('
			SELECT id ,'.Cubix_I18n::getTblField('title') . ' as title, phone_prefix FROM countries_phone_code WHERE phone_prefix is not NULL order by phone_prefix DESC
			');
		return $result;
	}
	public function getPhoneCountries() {
		$sql = '
			SELECT cpc.id,
				cpc.' . Cubix_I18n::getTblField('title') . ' as title,
				cpc.phone_prefix,
				cpc.ndd_prefix
			FROM countries_phone_code cpc
		';
		return parent::_fetchAll($sql);
	}

	public static function  getPhonePrefixById($id){
		$db = Zend_Registry::get('db');
		$sql = '
			SELECT cpc.phone_prefix
			FROM countries_phone_code cpc
			WHERE cpc.id = ?
		';
		return $db->fetchOne($sql,$id);
	}

	public static  function getCityBySlug( $slug ){
		$db = Zend_Registry::get('db');
		$sql = '
			SELECT ct.' . Cubix_I18n::getTblField('title') . ' as title
			FROM cities ct
			WHERE ct.slug = "'.$slug.'"
		';

		return $db->fetchOne($sql);


	}
	
	public static  function getCityById( $id ){
		$db = Zend_Registry::get('db');
		$sql = '
			SELECT ct.id,  ct.' . Cubix_I18n::getTblField('title') . ' as title, slug, url_slug
			FROM cities ct
			WHERE ct.id = "'. $id .'"
		';

		return $db->fetchRow($sql);


	}

}