<?php
class Model_Auth_Adapter implements Zend_Auth_Adapter_Interface
{
	public function __construct($username, $password)
	{
		$this->username = $username;
		$this->password = $password;
	}

	public function authenticate()
	{
		$db = Zend_Registry::get('db');

		$user = $db->query('
			SELECT id, first_name, last_name, username, email, type, application_id
			FROM backend_users
			WHERE username = ? AND password = MD5(?)
		', array($this->username, $this->password))->fetch();

		if ( $user ) {
			return new Zend_Auth_Result(Zend_Auth_Result::SUCCESS, new Model_UserItem($user));
		}
		else {
			return new Zend_Auth_Result(Zend_Auth_Result::FAILURE, null);
		}
	}
}
