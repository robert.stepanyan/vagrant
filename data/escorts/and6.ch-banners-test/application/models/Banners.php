<?php

class Model_Banners
{
	public static function getForMainPage($zone_name = null, $region_slug = null)
	{
		if ( ! $zone_name || ! $region_slug ) return "";

		$banner_mapping = array(
			'deutschschweiz' => array(
				'main_top_left' => '<div class="cF21Dg27" id="x37" data-limit="4"></div> <!-- DEUTSCHSCHWEIZ_home_middle_175x60 () -->',
				'main_top_right' => '<div id="x3439" class="top_right_banners cF21Dg27" data-limit="3"></div> <!-- DEUTSCHSCHWEIZ_home_right_top_354x255 () -->', 
				'main_middle' => '<div class="cF21Dg27" id="x38" data-limit="8"></div> <!-- DEUTSCHSCHWEIZ_home_left_175x125 () -->', 
				'main_bottom' => '<div class="cF21Dg27" data-limit="10" id="x39"></div> <!-- DEUTSCHSCHWEIZ_home_bottom_175x60 () -->', 
				'main_popup' => '<div class="cF21Dg27" id="x3530" data-limit="1"></div> <!-- DEUTSCHSCHWEIZ_580x380 () -->', 
				'gotd_place' => '<div class="cF21Dg27" id="x3130" data-limit="1"></div> <!-- DEUTSCHSCHWEIZ_GOTD_PLACE -->', 
			),
			'romandie' => array(
				'main_top_left' => '<div class="cF21Dg27" id="x3735" data-limit="4"></div> <!-- ROMANDIE_home_middle_175x60 () -->',
				'main_top_right' => '<div id="x3736" class="top_right_banners cF21Dg27" data-limit="3"></div> <!-- ROMANDIE_home_right_top_354x255 () -->', 
				'main_middle' => '<div class="cF21Dg27" id="x3734" data-limit="8"></div> <!-- ROMANDIE_home_left_175x125 () -->', 
				'main_bottom' => '<div class="cF21Dg27" data-limit="10" id="x3733"></div> <!-- ROMANDIE_home_bottom_175x60 () -->', 
				'main_popup' => '<div class="cF21Dg27" id="x3731" data-limit="1"></div> <!-- ROMANDIE_580x380 () -->', 
				'gotd_place' => '<div class="cF21Dg27" id="x3732" data-limit="1"></div>', 
			),
			'ticino' => array(
				'main_top_left' => '<div class="cF21Dg27" id="x3831" data-limit="4"></div> <!-- TICINO_home_middle_175x60 () -->',
				'main_top_right' => '<div id="x3832" class="top_right_banners cF21Dg27" data-limit="3"></div> <!-- TICINO_home_right_top_354x255 () -->', 
				'main_middle' => '<div class="cF21Dg27" id="x3830" data-limit="8"></div> <!-- TICINO_home_left_175x125 () -->', 
				'main_bottom' => '<div class="cF21Dg27" data-limit="10" id="x3739"></div> <!-- TICINO_home_bottom_175x60 () -->', 
				'main_popup' => '<div class="cF21Dg27" id="x3737" data-limit="1"></div> <!-- TICINO_580x380 () -->',
				'gotd_place' => '<div class="cF21Dg27" id="x3738" data-limit="1"></div>', 
			)
		);

		if ( isset($banner_mapping[$region_slug][$zone_name]) && strlen($banner_mapping[$region_slug][$zone_name]) ) {
			return $banner_mapping[$region_slug][$zone_name];
		}

		return "";
	}

	public static function getForCityPage($zone_name = null, $city_id = null)
	{
		if ( ! $zone_name || ! $city_id ) return ""; 

		$banner_mapping = array(
			'right_banners_175_375' => array(
				//deutschschweiz				
				17 => '<div class="cF21Dg27" id="x313531"></div>', //zurich-umgebung
				18 => '<div class="cF21Dg27" id="x313435"></div>', //wthur-thurgau-st-gallen
				19 => '<div class="cF21Dg27" id="x3835"></div>', //basel-und-umgebung
				20 => '<div class="cF21Dg27" id="x313330"></div>', //mittelland-aargau
				21 => '<div class="cF21Dg27" id="x3931"></div>', //bern-city
				22 => '<div class="cF21Dg27" id="x3934"></div>', //bern-umgeb-oberland
				23 => '<div class="cF21Dg27" id="x3937"></div>', //biel-bienne-grenchen				
				24 => '<div class="cF21Dg27" id="x313234"></div>', //luzern-innerschweiz				
				25 => '<div class="cF21Dg27" id="x313132"></div>', //graub-nden-glarus				
				26 => '<div class="cF21Dg27" id="x313438"></div>', //wallis				
				44 => '<div class="cF21Dg27" id="x313333"></div>', //mittelland-solothurn
				45 => '<div class="cF21Dg27" id="x313533"></div>', //zurich-city

				//romandie
				28 => '<div class="cF21Dg27" id="x313039"></div>', //geneve-et-region				
				29 => '<div class="cF21Dg27" id="x313135"></div>', //lausanne-renens-moudon				
				30 => '<div class="cF21Dg27" id="x313336"></div>', //montreux-vevey-aigle				
				31 => '<div class="cF21Dg27" id="x313237"></div>', //sierre-sion-martigny-bas-valais				
				32 => '<div class="cF21Dg27" id="x313339"></div>', //neuchatel-et-region-jura				
				33 => '<div class="cF21Dg27" id="x313432"></div>', //vaud-payerne-yverdon				
				34 => '<div class="cF21Dg27" id="x313036"></div>', //fribourg-bulle
				35 => '<div class="cF21Dg27" id="x313030"></div>', //bienne

				//ticino
				36 => '<div class="cF21Dg27" id="x3838"></div>', //bellinzona
				37 => '<div class="cF21Dg27" id="x313138"></div>', //locarno
				38 => '<div class="cF21Dg27" id="x313231"></div>', //lugano
				39 => '<div class="cF21Dg27" id="x313033"></div>', //chiasso
				42 => '<div class="cF21Dg27" id="x313533"></div>', //cadenazzo [NO ZONE]
				43 => '<div class="cF21Dg27" id="x313533"></div>', //riazzino [NO ZONE]
			),
			'right_banners_175_250' => array(
				//deutschschweiz				
				17 => '<div class="cF21Dg27" id="x313530"></div>', //zurich-umgebung				
				18 => '<div class="cF21Dg27" id="x313434"></div>', //wthur-thurgau-st-gallen				
				19 => '<div class="cF21Dg27" id="x3834"></div>', //basel-und-umgebung				
				20 => '<div class="cF21Dg27" id="x313239"></div>', //mittelland-aargau				
				21 => '<div class="cF21Dg27" id="x3930"></div>', //bern-city				
				22 => '<div class="cF21Dg27" id="x3933"></div>', //bern-umgeb-oberland				
				23 => '<div class="cF21Dg27" id="x3936"></div>', //biel-bienne-grenchen				
				24 => '<div class="cF21Dg27" id="x313233"></div>', //luzern-innerschweiz				
				25 => '<div class="cF21Dg27" id="x313131"></div>', //graub-nden-glarus				
				26 => '<div class="cF21Dg27" id="x313437"></div>', //wallis
				44 => '<div class="cF21Dg27" id="x313332"></div>', //mittelland-solothurn
				45 => '<div class="cF21Dg27" id="x313534"></div>', //zurich-city [zurich nord]

				//romandie
				28 => '<div class="cF21Dg27" id="x313038"></div>', //geneve-et-region				
				29 => '<div class="cF21Dg27" id="x313134"></div>', //lausanne-renens-moudon
				30 => '<div class="cF21Dg27" id="x313335"></div>', //montreux-vevey-aigle				
				31 => '<div class="cF21Dg27" id="x313236"></div>', //sierre-sion-martigny-bas-valais				
				32 => '<div class="cF21Dg27" id="x313338"></div>', //neuchatel-et-region-jura				
				33 => '<div class="cF21Dg27" id="x313431"></div>', //vaud-payerne-yverdon				
				34 => '<div class="cF21Dg27" id="x313035"></div>', //fribourg-bulle
				35 => '<div class="cF21Dg27" id="x3939"></div>', //bienne

				//ticino
				36 => '<div class="cF21Dg27" id="x3837"></div>', //bellinzona
				37 => '<div class="cF21Dg27" id="x313137"></div>', //locarno
				38 => '<div class="cF21Dg27" id="x313230"></div>', //lugano				
				39 => '<div class="cF21Dg27" id="x313032"></div>', //chiasso
				42 => '<div class="cF21Dg27" id="x313533"></div>', //cadenazzo [NO ZONE]
				43 => '<div class="cF21Dg27" id="x313533"></div>', //riazzino [NO ZONE]
			),
			'right_banners_175_125' => array(
				//deutschschweiz				
				17 => '<div class="cF21Dg27" id="x313439"></div>', //zurich-umgebung
				18 => '<div class="cF21Dg27" id="x313433"></div>', //wthur-thurgau-st-gallen				
				19 => '<div class="cF21Dg27" id="x3833"></div>', //basel-und-umgebung
				20 => '<div class="cF21Dg27" id="x313238"></div>', //mittelland-aargau				
				21 => '<div class="cF21Dg27" id="x3839"></div>', //bern-city
				22 => '<div class="cF21Dg27" id="x3932"></div>', //bern-umgeb-oberland				
				23 => '<div class="cF21Dg27" id="x3935"></div>', //biel-bienne-grenchen				
				24 => '<div class="cF21Dg27" id="x313232"></div>', //luzern-innerschweiz								
				25 => '<div class="cF21Dg27" id="x313130"></div>', //graub-nden-glarus
				26 => '<div class="cF21Dg27" id="x313436"></div>', //wallis
				44 => '<div class="cF21Dg27" id="x313331"></div>', //mittelland-solothurn
				45 => '<div class="cF21Dg27" id="x313532"></div>', //zurich-city

				//romandie
				28 => '<div class="cF21Dg27" id="x313037"></div>', //geneve-et-region				
				29 => '<div class="cF21Dg27" id="x313133"></div>', //lausanne-renens-moudon				
				30 => '<div class="cF21Dg27" id="x313334"></div>', //montreux-vevey-aigle
				31 => '<div class="cF21Dg27" id="x313235"></div>', //sierre-sion-martigny-bas-valais				
				32 => '<div class="cF21Dg27" id="x313337"></div>', //neuchatel-et-region-jura
				33 => '<div class="cF21Dg27" id="x313430"></div>', //vaud-payerne-yverdon				
				34 => '<div class="cF21Dg27" id="x313034"></div>', //fribourg-bulle
				35 => '<div class="cF21Dg27" id="x3938"></div>', //bienne

				//ticino
				36 => '<div class="cF21Dg27" id="x3836"></div>', //bellinzona
				37 => '<div class="cF21Dg27" id="x313136"></div>', //locarno
				38 => '<div class="cF21Dg27" id="x313139"></div>', //lugano				
				39 => '<div class="cF21Dg27" id="x313031"></div>', //chiasso
				42 => '<div class="cF21Dg27" id="x313533"></div>', //cadenazzo [NO ZONE]
				43 => '<div class="cF21Dg27" id="x313533"></div>', //riazzino [NO ZONE]
			)
		);

		if ( isset($banner_mapping[$zone_name][$city_id]) && strlen($banner_mapping[$zone_name][$city_id]) ) {
			return $banner_mapping[$zone_name][$city_id];
		}

		return "";
	}
}
