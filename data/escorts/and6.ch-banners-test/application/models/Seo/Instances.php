<?php

class Model_Seo_Instances
{
	public static function get($entity, $primary_id)
	{
		$db = Zend_Registry::get('db');
		
		$entity = trim($entity);
		
		$title_field = Cubix_I18n::getTblField('title');
		$heading_field = Cubix_I18n::getTblField('heading');
		$heading_escort_field = Cubix_I18n::getTblField('heading_escort');
		$heading_trans_field = Cubix_I18n::getTblField('heading_trans');
		$meta_description_field = Cubix_I18n::getTblField('meta_description');
		$meta_keywords_field = Cubix_I18n::getTblField('meta_keywords');
		$footer_text_field = Cubix_I18n::getTblField('footer_text');
		
		
		$cache = Zend_Registry::get('cache');
		$new_cache_key = "seo_data_cache_" . $entity . $primary_id;
		$new_cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $new_cache_key);
		//var_dump($new_cache_key); die;
		
		if ( ! $data = $cache->load($new_cache_key) ) {	
		
			$data = $db->query('
				SELECT
					IF (LENGTH(ei.' . $title_field . '), ei.' . $title_field . ', e.' . $title_field . ') AS title,
					IF (LENGTH(ei.' . $heading_field . '), ei.' . $heading_field . ', e.' . $heading_field . ') AS heading,
					ei.' . $heading_escort_field . ' AS heading_escort,
					ei.' . $heading_trans_field . ' AS heading_trans,
					IF (LENGTH(ei.' . $meta_description_field . '), ei.' . $meta_description_field . ', e.' . $meta_description_field . ') AS description,
					IF (LENGTH(ei.' . $meta_keywords_field . '), ei.' . $meta_keywords_field . ', e.' . $meta_keywords_field . ') AS keywords,
					IF (LENGTH(ei.' . $footer_text_field . '), ei.' . $footer_text_field . ', e.' . $footer_text_field . ') AS footer_text
				FROM seo_entity_instances ei
				INNER JOIN seo_entities e ON e.id = ei.entity_id
				WHERE e.slug = ? AND ei.primary_id = ?
			', array($entity, $primary_id))->fetch();

			if ( ! $data ) {
				$data = $db->query('
					SELECT
						e.' . $title_field . ' AS title,
						e.' . $heading_field . ' AS heading,
						e.' . $meta_description_field . ' AS description,
						e.' . $meta_keywords_field . ' AS keywords,
						e.' . $footer_text_field . ' AS footer_text
					FROM seo_entities e
					WHERE e.slug = ?
				', $entity)->fetch();
			}

			if ( ! $data ) {
				$data = (object) array(
					'title' => '',
					'heading' => '',
					'heading_escort' => '',
					'heading_trans' => '',
					'meta_description' => '',
					'meta_keywords' => '',
					'footer_text' => ''
				);
			}
			
			
			$cache->save($data, $new_cache_key, array());
		}
		
		return $data;
	}
}
