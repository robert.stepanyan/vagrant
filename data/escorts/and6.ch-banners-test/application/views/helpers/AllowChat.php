<?php

class Zend_View_Helper_AllowChat
{
	public function allowChat()
	{
		//return false;
		$request = Zend_Controller_Front::getInstance()->getRequest();
		
		if ( $request->getParam('chatVersion') != 1 ) 
		{
			return false;
		}
		
		$user = Model_Users::getCurrent();
		
		if ($user)
		{
			if ($user->user_type == 'agency')
			{
				if ($user->has_active_package)
					return true;
				else
					return false;
			}
			
			if ($user->user_type == 'escort')
			{
				if ($user->has_active_package && !$user->is_susp)
					return true;
				else
					return false;
			}		
		}
		
		return TRUE;
	}
}

