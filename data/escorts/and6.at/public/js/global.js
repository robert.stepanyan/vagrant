var Cubix = {};

Cubix.Lang = {};

Cubix.ShowTerms = function (link) {
	window.open(link, 'showTerms', 'menubar=no, resizable=yes, status=no, scrollbars=yes, toolbar=no, width=640, height=480');
	return false;
}

var _log = function (data) {
	if ( console && console.log != undefined ) {
		console.log(data);
	}
}

var _st = function (object) {
	var parts = object.split('.');
	if ( parts.length < 2 ) {
		return _log('invalid parameter, must contain filename with extension');
	}

	var ext = parts[parts.length - 1];
	
	var base_url = 'http://st.and6.at',
		prefix = '';

	switch ( ext ) {
		case 'jpg':
		case 'gif':
		case 'png':
			prefix = '/img/';
			break;
		case 'css':
			prefix = '/css/';
			break;
		case 'js':
			prefix = '/js/';
			break;
		default:
			return _log('invalid extension "' + ext + '"');
	}

	url = base_url + prefix + object;

	return url;
}

// Taken from viewed-escorts.js
/* --> Viewed Escorts */
Cubix.Viewed = {};

Cubix.Viewed.url = ''; // Must be set from php
Cubix.Viewed.container = '';

window.addEvent('domready', function() {
	if ( ! $(Cubix.Viewed.container) || ! Cubix.Viewed.url ) return;
	
	new Request({
		url: Cubix.Viewed.url,
		method: 'get',
		onSuccess: function (resp) {
			if(resp.contains('rapper'))
			{
				$(Cubix.Viewed.container).setStyle('opacity', 0);
				var myFx = new Fx.Tween($(Cubix.Viewed.container), {
					duration: 400,
					onComplete: function() {
						$(Cubix.Viewed.container).set('html', resp);
						$(Cubix.Viewed.container).tween('opacity', 0, 1);
					}
				});
				myFx.start('height', '137');
			}
		}
	}).send();
});

function addRemoveFav(id,link){
	var favImg = $$(' #last_viewed .add-fav-latest[rel=' + id + ']').getElement('img');
	var action = favImg.get('alt');
	if (action == 'signin'){
		Cubix.Popup.Show('489', '652');
	}
	else{
		favImg.set('src','/img/fav-loader.gif');
		if (action == 'add'){
			var favIcon = 'remove';
		}
		else{
			var favIcon = 'add';
		}
		new Request({
			url: link,
			method: 'post',
			onSuccess: function (resp) {
				favImg.set('alt',favIcon);
				favImg.set('src','/img/'+ favIcon +'_fav_star.png');
				}
		}).send();
	}

	return false;
}

var resizeProfile = function() {
	var $leftHeight = $("left").getSize().y;
	var $profileHeight = $("profile-container").getSize().y - 4700;
	var $rightColHeight = $("right-col").getSize().y;
	
	if ( $rightColHeight > $profileHeight && $rightColHeight > $leftHeight ) {
		$("profile-container").setStyle('height', $rightColHeight);
	} else {
		if ( $leftHeight > $profileHeight ) {
			$("profile-container").setStyle('height', $leftHeight);
		} else {
			$("profile-container").setStyle('height', $profileHeight);
		}
	}
};

var initGallery = function() {
	$$("#gallery img, #gallery .lupa").removeEvents('click');
	$$("#gallery img, #gallery .lupa").addEvent("click", function() {
		
		if ( this.hasClass('noclick') ) return;
		
		if ( $('image') ) {
			$('image').destroy();
		}
		
		if ( this.hasClass('lupa') ) {
			var src = this.getPrevious('img').get('src');
		} else {
			var src = this.get('src');
		}
		
		if ( src.indexOf('_pp') != -1 )
			src = src.replace("_pp", "_orig");
		else {
			src = src.replace("_pl", "_orig");
		}
		
		im = new Image();
		im.src = src;
		im.onload = function () {
			
			h = im.height;
			w = im.width;

			oh = $(window).getSize().y;
			ow = $(window).getSize().x;

			if ( oh < h ) {
				origH = h;
				h = oh - 40;
				w = h * w / origH;
			}

			if ( ow < w ) {
				origW = w;
				w = ow - 40;
				h = h * w / origW;
			}

			leftt = (w + 20) / 2;
			topr = (h + 20) / 2;

			var divImage = new Element('div', {
				id: 'image',
				styles: {
					'margin-left': -leftt,
					'margin-top': -topr,
					'display': 'block',
					'opacity': '0'
				}
			}).inject($(document.body));

			new Element('a', {
				'html': 'x'
			}).inject(divImage);

			new Element('img', {
				'src': src,
				'width': w,
				'height': h
			}).addEvent('contextmenu', function(e) {
				e.stop();
			}).inject(divImage);

			divImage.addEvent("click", function() {
				$(this).destroy();
			});
			
			divImage.set('tween', { duration: 'short' }).fade('in');
		}.bind(this);
	});
};
/* <-- */
