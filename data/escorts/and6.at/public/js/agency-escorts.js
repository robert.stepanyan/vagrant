/* -->Agency Escorts */
Cubix.AgencyEscorts = {};

Cubix.AgencyEscorts.url = ''; // Must be set from php
Cubix.AgencyEscorts.container = '';

Cubix.AgencyEscorts.Load = function (lang, page, escort_id, agency_id) {
	
	var url = '/' + lang + '/escorts/agency-escorts?agency_page=' + page + '&escort_id=' + escort_id + '&agency_id=' + agency_id;
	
	Cubix.AgencyEscorts.Show(url);
	
	return false;
}

Cubix.AgencyEscorts.Show = function (url) {
	var overlay = new Cubix.Overlay($(Cubix.AgencyEscorts.container), {
		loader: _st('loader-small.gif'),
		position: '50%'
	});
	
	overlay.disable();
	
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {			
			$(Cubix.AgencyEscorts.container).set('html', resp);
			overlay.enable();
		}
	}).send();
}
/* <-- */
