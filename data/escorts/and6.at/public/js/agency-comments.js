Cubix.AgencyComments = {};

Cubix.AgencyComments.url = ''; // Must be set from php
Cubix.AgencyComments.container = 'comments_container';
Cubix.AgencyComments.lng = '';
Cubix.AgencyComments.autoApprove = '';

Cubix.AgencyComments.Load = function (lang, page, agency_id) {
	if ( lang == 'fr' ) lang = false;
	var url = (lang ? '/' + lang : '') + '/agencies/comments?page=' + page + '&agency_id=' + agency_id + '&mode=ajax';
	
	Cubix.AgencyComments.Show(url);
	
	return false;
}

Cubix.AgencyComments.Show = function (url) {
	var overlay = new Cubix.Overlay($(Cubix.AgencyComments.container), {
		loader: _st('loader-small.gif'),
		position: '50%'
	});
	
	overlay.disable();
	
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {			
			$(Cubix.AgencyComments.container).set('html', resp);
			overlay.enable();
		}
	}).send();
}

Cubix.AgencyComments.InitAddForm = function () {
	$$('.add-comment').removeEvents('click');
	
	if ($defined($('comment_added')))
		$('comment_added').setStyle('display', 'none');

	$$('.add-comment').addEvent('click', function (e) {
		e.stop();
		
		var agency_id = this.get('rel');
		
		if ( Cubix.AgencyComments.is_member == 'true' )
		{
			$('add-comment-form').set('html', '');
            $('comment_added').setStyle('display', 'none');

			var params = {};

			new Request({
				url: '/' + Cubix.AgencyComments.lng + '/comments-v2/ajax-add-agency-comment',
				method: 'get',
				evalScripts: true,
				data: $merge({ agency_id: agency_id }, params || {}),
				onSuccess: function (resp) {
					var height = new Element('div', { html: resp }).getHiddenHeight($('add-comment-form'));
					$('add-comment-form').setStyles({ height: 0, overflow: 'hidden' }).set('html', resp).set('tween', { duration: 500, onComplete: function () { this.element.setStyle('height', null) } }).tween('height', height);

					Cubix.AgencyComments.InitFormSubmit(agency_id);
					Cubix.AgencyComments.CloseAddForm();
				}.bind(this)
			}).send();
			
		}
		else {
			Cubix.Popup.Show('489', '652');
		}
	});
};

Cubix.AgencyComments.InitFormSubmit = function (agency_id) {
	$$('.btn-save').addEvent('click', function(e) {
		e.stop();

		var overlay = new Cubix.Overlay($$('.myprofile')[0], {
			loader: _st('loader-small.gif'),
			position: '50%'
		});

		overlay.disable();

		new Request({
			url: '/' + Cubix.AgencyComments.lng + '/comments-v2/ajax-add-agency-comment',
			method: 'post',
			evalScripts: true,
			data: this.getParent('form'),
			onSuccess: function (resp) {
				if ( Cubix.AgencyComments.isJSON(resp) ) {
					if( JSON.decode(resp) ) {
						var data = JSON.decode(resp);

						var error_map = ['comment_to_short', 'comment_to_long', 'captcha_error'];
						error_map.each(function(it){
							$(it).setStyle('display', 'none');
						});

						data.each(function(it){
							$(it).setStyle('display', 'block');
						});
					}
					overlay.enable();
				}
				else {
					if ( $('add-comment-form') ) {
						$('add-comment-form').set('html', '');
					}
					
					if (Cubix.AgencyComments.autoApprove == "0")
						$('comment_added').setStyle('display', 'block');
					else
						Cubix.AgencyComments.Load(Cubix.AgencyComments.lng, 1, agency_id);
				}
			}.bind(this)
		}).send();
	});
};

Cubix.AgencyComments.isJSON = function(str){
	 str = str.replace(/\\./g, '@').replace(/"[^"\\\n\r]*"/g, '');
	 return (/^[,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]*$/).test(str);
}

Cubix.AgencyComments.CloseAddForm = function() {
	$$('.btn-close').addEvent('click', function(e) {
		e.stop();

		$('add-comment-form').set('html', '');
	});
};

window.addEvent('domready', function () {
	Cubix.AgencyComments.InitAddForm();
	
	///////////////////////////////////////
	
	if ( $defined($$('.list_grid_switcher')) ) {
		$$('.list_grid_switcher a').addEvent('click', function(e){
			e.stop();
			
			var cont = $$('.gl_sw')[0];
			
			/*var date = new Date();
			date.setTime(date.getTime()+(30*24*60*60*1000));
			expires = date.toGMTString();*/
			
			if ( this.hasClass('grid_btn') ) {
				cont.removeClass('list');
				this.removeClass('grid_btn');
				this.addClass('grid_btn_act');
				
				this.getNext('a').removeClass('list_btn_act');
				this.getNext('a').addClass('list_btn');
				
				Cookie.write('list_type', 'grid', {domain: '.and6.at', duration: 30, path: '/'});
			}
			
			if ( this.hasClass('grid_btn_act') ) {
				return;
			}
			
			if ( this.hasClass('list_btn') ) {
				cont.addClass('list');
				this.removeClass('list_btn');
				this.addClass('list_btn_act');
				
				this.getPrevious('a').removeClass('grid_btn_act');
				this.getPrevious('a').addClass('grid_btn');
				
				Cookie.write('list_type', 'list', {domain: '.and6.at', duration: 30, path: '/'});
			}
			
			if ( this.hasClass('list_btn_act') ) {
				return;
			}
		});
	}
	
	$$('.comment_icon').addEvent('mouseenter', function(){
		var self = this;
		var commentCount = self.getNext('.comment_count').get('text');
		tooltip.show(commentCount +' '+ Cubix.CommentTip);

	});

	$$('.comment_icon').addEvent('mouseleave', function(){
		tooltip.hide();
	});

	$$('.review_icon').addEvent('mouseenter', function(){
		var self = this;
		var reviewCount = self.getNext('.review_count').get('text');
		tooltip.show(reviewCount +' '+ Cubix.ReviewTip);

	});

	$$('.review_icon').addEvent('mouseleave', function(){
		tooltip.hide();
	});
	
	$$('.h').addEvent('mouseenter', function() {
		this.removeClass('esc-hover-out');
		this.addClass('esc-hover');			
	});

	$$('.h').addEvent('mouseleave', function() {
		this.removeClass('esc-hover');
		this.addClass('esc-hover-out');
	});
});
