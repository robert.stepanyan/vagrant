Cubix.BPopup = {};


Cubix.BPopup.inProcess = false;

Cubix.BPopup.url = '';

Cubix.BPopup.Show = function (box_width, box_height) {
	if ( Cubix.BPopup.inProcess ) return false;

	var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000' });
	page_overlay.disable();

	var y_offset = 30;

	var container = new Element('div', { 'class': 'BPopup-wrapper'}).setStyles({
		left: window.getWidth() / 2 - box_width / 2,
		top:  window.getHeight() / 2 - box_height / 2 + window.getScroll().y /*- y_offset - 120*/,
		opacity: 0,
        position: 'absolute',
        'z-index': 101,
        background: '#fff'
	}).inject(document.body);
	
	Cubix.BPopup.inProcess = true;

	new Request({
		url: Cubix.BPopup.url,
		method: 'get',
		onSuccess: function (resp) {
			
			Cubix.BPopup.inProcess = false;
			container.set('html', resp);

			var close_btn = new Element('div', {
				html: '',
				'class': 'bp-close-btn'
			}).inject(container);

			close_btn.addEvent('click', function() {
				$$('.BPopup-wrapper').destroy();
				page_overlay.enable();
				
				var my = Cookie.write('banner_popup_show', '0', {domain: '.and6.at', duration: 0, path: '/'});
			});

			container.tween('opacity', '1');
		}
	}).send();
		
	
	return false;
}