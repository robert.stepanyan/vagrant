/* --> Photos */
Cubix.Photos = {};

Cubix.Photos.url = ''; // Must be set from php
Cubix.Photos.container = 'other-photos';

Cubix.Photos.Load = function (lang, page, agency_id) {
	if ( lang == 'fr' ) lang = false;
	var url = (lang ? '/' + lang : '') + '/agencies/photos?photo_page=' + page + '&agency_id=' + agency_id + '&mode=ajax';
	
	Cubix.Photos.Show(url);
	
	return false;
}

Cubix.Photos.Show = function (url) {
	var overlay = new Cubix.Overlay($(Cubix.Photos.container), {
		loader: _st('loader-small.gif'),
		position: '50%'
	});
	
	overlay.disable();
	
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {			
			$(Cubix.Photos.container).set('html', resp);
			overlay.enable();
		}
	}).send();
}
/* <-- */
