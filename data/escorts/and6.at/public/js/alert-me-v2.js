window.addEvent('domready', function(){
	var page_overlay = new Cubix.Overlay(document.body, {has_loader: false, opacity: 0.5, color: '#000', z_index: 99});
	var escort_id = $('escort_id').get('value');
	
	if ($('alm-wr'))
	{
		var alertme = $('alm-wr');

		$('alm-btn').addEvent('click', function(e){
			e.stop();

			if ( alertme.hasClass('none') ) {
				page_overlay.disable();
				alertme.removeClass('none');
				
				var c = $('info-bar-full').getPosition();
				
				alertme.setStyles({top: c.y - $('alm-wr').getHeight(), left: c.x + 680});
			}
			else {
				page_overlay.enable();
				alertme.addClass('none');
			}
		});
		
		$$('.alm-x').addEvent('click', function(e) {
			e.stop();

			page_overlay.enable();
			alertme.addClass('none')
		});
	}
	
	if ($('alm-wr-m'))
	{
		var alertme_m = $('alm-wr-m');
		var cities = Array();

		$$('.alm-btn-m').addEvent('click', function(e){
			e.stop();

			if ( alertme_m.hasClass('none') ) {
				page_overlay.disable();			
				
				var req = new Request({
					method: 'get',
					url: '/escorts/ajax-alerts?escort_id=' + escort_id,
					onComplete: function(response) {
						var object = JSON.decode(response);
						var evs = object.events;
						var cts = object.cities;
						
						if (evs.join(',') == '1,2,3,4')
						{
							$('alm-5').set('checked', 'checked');
						}
						else if (evs.join(',') == '1,2,3,4,6')
						{
							$('alm-5').set('checked', 'checked');
							$('alm-6').set('checked', 'checked');
						}
						else
						{
							evs.each(function(el) {
								$('alm-' + el).set('checked', 'checked');
							})
						}
					
						// fill cities 
						$('city-items').empty();
						$('city_id').set('value', '');
						cities = Array();
						
						cts.each(function(el) {
							cities.push(el.id);							
							addCity(el.id, el.title);
						});
						
						removeCity(cities);
						//
						checkCHB();
												
						var c = $('info-bar-full').getPosition();
						
						var height = alertme_m.setStyles({
							visibility: 'hidden',
							display: 'block'
						}).getHeight();
						
						alertme_m.setStyles({
							visibility: null,
							display: null
						});
						
						alertme_m.setStyles({top: c.y - height, left: c.x + 680});
						
						alertme_m.removeClass('none');
					}
				}).send();
			}
			else {
				page_overlay.enable();
				alertme_m.addClass('none');
			}
		});
		
		$$('.alm-x').addEvent('click', function(e) {
			e.stop();

			page_overlay.enable();
			alertme_m.addClass('none');
		});
		
		$('alm-5').addEvent('change', function(e) {
			e.stop();

			checkCHB();
		});
		
		$('alm-save').addEvent('click', function(e) {
			e.stop();
			
			var i = 0;
			var events = Array();
			
			$$("#alertme_form input[type=checkbox]:checked").each(function(el)
			{
				events[i] = el.get('id').substr(4, 1);
				i++;
			});
			
			var ev = events.join(',');
			
			if (ev == '5')
				ev = '1,2,3,4';
			else if (ev == '5,6')
				ev = '1,2,3,4,6';
			
			var cu = '';
			
			if (events.contains('6'))
			{
				var cities_str = cities.join(',');
				cu = '&cities=' + cities_str;
			}
				
			
			var req = new Request({
				method: 'get',
				url: '/escorts/ajax-alerts-save?escort_id=' + escort_id + '&events=' + ev + cu,
				onComplete: function(response) {
					var object = JSON.decode(response);
					if (object.count > 0)
					{
						if ($('alm-b-a').hasClass('none') == true)
							$('alm-b-a').removeClass('none');
						if ($('alm-b').hasClass('none') == false)
							$('alm-b').addClass('none');
					}
					else
					{
						if ($('alm-b-a').hasClass('none') == false)
							$('alm-b-a').addClass('none');
						if ($('alm-b').hasClass('none') == true)
							$('alm-b').removeClass('none');
					}
					
					page_overlay.enable();
					alertme_m.addClass('none');
				}
			}).send();
		});
		
		$$('.alm-city-btn').addEvent('click', function (e) {
			var c = $('city_id').getSelected();
			var c_id = Number.from(c.get('value').toString());
			var c_txt = c.get('text').toString();
			
			$('alm-6').set('checked', 'checked');
			
			if (c_id != null && !cities.contains(c_id))
			{
				cities.push(c_id);
				addCity(c_id, c_txt);
				removeCity(cities);
			}
		});
	}
});

function addCity(id, text)
{
	var myFirstElement  = new Element('div', {'class': 'city-item-wr'});
	var mySecondElement = new Element('a', {id: id, 'class': 'city-item'});
	mySecondElement.inject(myFirstElement);
	myFirstElement.appendText(text);
	myFirstElement.inject($('city-items'));
}

function removeCity(cities)
{
	$$('.city-item').removeEvents();
	$$('.city-item').addEvent('click', function (e) {
		var self = this;

		var id = self.get('id');
		
		removeByElement(cities, id);
		self.getParent().dispose();
	});
}

function removeByElement(arrayName, arrayElement)
{
	for (var i = 0; i < arrayName.length; i++)
	{ 
		if (arrayName[i] == arrayElement)
			arrayName.splice(i,1); 
	} 
}

function checkCHB()
{
	if ($$('input[name=any]').get('checked') == 'true')
	{
		$$('input[name=comment]').set('checked', '');
		$$('input[name=comment]').set('disabled', 'disabled');
		$$('input[name=profile]').set('checked', '');
		$$('input[name=profile]').set('disabled', 'disabled');
		$$('input[name=pictures]').set('checked', '');
		$$('input[name=pictures]').set('disabled', 'disabled');
		$$('input[name=review]').set('checked', '');
		$$('input[name=review]').set('disabled', 'disabled');
	}
	else
	{
		$$('input[name=comment]').set('disabled', '');
		$$('input[name=profile]').set('disabled', '');
		$$('input[name=pictures]').set('disabled', '');
		$$('input[name=review]').set('disabled', '');
	}
}
/* <-- */
