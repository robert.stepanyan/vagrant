Cubix.Comments = {};

Cubix.Comments.lng = '';
Cubix.Comments.escort_id = '';
Cubix.Comments.is_member = 'false';

Cubix.Comments.InitComments = function () {
	Cubix.Comments.InitAddForm();
}

Cubix.Comments.InitAddForm = function () {
	$$('.add-comment').removeEvents('click');

	$$('.add-comment').addEvent('click', function (e) {
		e.stop();

		if ( Cubix.Comments.is_member == 'true' || Cubix.Comments.is_escort == 'true')
		{
			$('add-comment-form').set('html', '');
            $('comment_added').setStyle('display', 'none');

			$$('div.reply_place').each(function(it){
				it.set('html', '');
			});

			var overlay = new Cubix.Overlay($('comments_container'), {
				loader: _st('loader-small.gif'),
				position: '50%'
			});

			overlay.disable();

			var params = {};

			if ( this.get('rel') ) {
				new Request({
					url: '/' + Cubix.Comments.lng + '/comments/ajax-add-comment',
					method: 'get',
					evalScripts: true,
					data: $merge({ escort_id: Cubix.Comments.escort_id, comment_id: this.get('rel') }, params || {}),
					onSuccess: function (resp) {
						var el = this.getParent('div.comment').getElement('div.reply_place');
						var height = new Element('div', { html: resp }).getHiddenHeight(el);
						el.setStyles({ height: 0, overflow: 'hidden' }).set('tween', { duration: 500, onComplete: function () { this.element.setStyle('height', null) } }).set('html', resp).tween('height', height);

						Cubix.Comments.InitFormSubmit();
						Cubix.Comments.CloseAddForm();

						overlay.enable();

					}.bind(this)
				}).send();
			}
			else {
				new Request({
					url: '/' + Cubix.Comments.lng + '/comments/ajax-add-comment',
					method: 'get',
					evalScripts: true,
					data: $merge({ escort_id: Cubix.Comments.escort_id }, params || {}),
					onSuccess: function (resp) {
                        
                        
						var height = new Element('div', { html: resp }).getHiddenHeight($('add-comment-form'));
						$('add-comment-form').setStyles({ height: 0, overflow: 'hidden' }).set('html', resp).set('tween', { duration: 500, onComplete: function () { this.element.setStyle('height', null) } }).tween('height', height);

						Cubix.Comments.InitFormSubmit();
						Cubix.Comments.CloseAddForm();

						overlay.enable();

					}.bind(this)
				}).send();
			}
		}
		else {
			Cubix.Popup.Show('489', '652');
		}

	});
};

Cubix.Comments.InitFormSubmit = function () {
	$$('.btn-save').addEvent('click', function(e) {
		e.stop();

		var overlay = new Cubix.Overlay($$('.myprofile')[0], {
			loader: _st('loader-small.gif'),
			position: '50%'
		});

		overlay.disable();

		new Request({
			url: '/' + Cubix.Comments.lng + '/comments/ajax-add-comment',
			method: 'post',
			evalScripts: true,
			data: this.getParent('form'),
			onSuccess: function (resp) {
				if ( Cubix.Comments.isJSON(resp) ) {
					if( JSON.decode(resp) ) {
						var data = JSON.decode(resp);

						var error_map = ['comment_to_short', 'captcha_error'];
						error_map.each(function(it){
							$(it).setStyle('display', 'none');
						});

						data.each(function(it){
							$(it).setStyle('display', 'block');
						});
					}
					overlay.enable();
				}
				else {
					$('add-comment-form').set('html', '');
                    $('comment_added').setStyle('display', 'block');

//					Cubix.Comments.LoadComments({ page: 1 });
				}

			}.bind(this)
		}).send();
	});
};

Cubix.Comments.isJSON = function(str){
	  str = str.replace(/\\./g, '@').replace(/"[^"\\\n\r]*"/g, '');
	 return (/^[,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]*$/).test(str);
}

Cubix.Comments.CloseAddForm = function() {
	$$('.btn-close').addEvent('click', function(e) {
		e.stop();

		$('add-comment-form').set('html', '');
		$$('div.reply_place').each(function(it){
			it.set('html', '');
		});
	});
};

Cubix.Comments.InitVote = function() {
	$$('a.vote-up', 'a.vote-down').addEvent('click', function(e){
		e.stop();

		if ( Cubix.Comments.is_member == 'true' )
		{
			var overlay = new Cubix.Overlay($('comments_container'), {
				loader: _st('loader-small.gif'),
				position: '50%'
			});

			overlay.disable();

			var params = [];
			if ( this.get('class') == 'vote-up' ) {
				params = { type: 'vote-up' };
			}
			else {
				params = { type: 'vote-down' };
			}

			new Request({
				url: '/' + Cubix.Comments.lng + '/comments/ajax-vote',
				method: 'get',
				evalScripts: true,
				data: $merge({ escort_id: Cubix.Comments.escort_id, comment_id: this.get('rel') }, params || {}),
				onSuccess: function (resp) {
					if ( resp.length == 0 ) {
						Cubix.Comments.LoadComments({ page: 1 });
					}
					else {
						var el = this.getParent('div.parent').getElement('div.reply_place');
						el.set('html', resp);
						overlay.enable();
					}
					//overlay.enable();
				}.bind(this)
			}).send();
		}
		else {
			Cubix.Popup.Show('489', '652');
		}
	})
};

Cubix.Comments.LoadComments = function (params) {
	var comments_container = $('comments_container');
	if(!comments_container) return false;
	var overlay = new Cubix.Overlay(comments_container, {
		loader: _st('loader-small.gif'),
		position: '50%'
	});
	
	overlay.disable();

	var tween = comments_container.get('tween');
	tween.options.duration = 300;

	new Request({
		url: '/' + Cubix.Comments.lng + '/comments/ajax-show',
		method: 'get',
		evalScripts: true,
		data: $merge({ escort_id: Cubix.Comments.escort_id }, params || {}),
		onSuccess: function (resp) {
			var height = new Element('div', { html: resp, styles: { width: comments_container.getWidth() } }).getHiddenHeight(comments_container),
				startHeight = comments_container.getHeight();

			tween.removeEvents('complete').addEvent('complete', function () {
				tween.removeEvents('complete').addEvent('complete', function () {
					tween.set('opacity', 0);
					
					tween.removeEvents('complete').addEvent('complete', function () {
						
					}).set('opacity', 0).start('opacity', 1);
					
					this.element.set('html', resp).setStyle('height', null);
					
					Cubix.Comments.InitComments();
					Cubix.Comments.InitVote();
					
					overlay.enable();
				}).set('height', startHeight).start('height', height);
			}).set('opacity', 1).start('opacity', 0);
		}
	}).send();

	return false;
}

window.addEvent('domready', function () {
	Cubix.Comments.LoadComments();
});