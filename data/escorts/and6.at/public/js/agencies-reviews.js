window.addEvent('domready', function(){
	// Our instance for the element with id "demo-word"
	new Autocompleter.Request.JSON($('f_name'), '/reviews/ajax-agencies-search', {
		postVar: 'a_name',
		indicatorClass: 'autocompleter-loading'
	}).addEvent('onSelection', function (element, selected, value, input_sh) {
		$('f_name').set('value', selected.inputValue.name);
	});
});
