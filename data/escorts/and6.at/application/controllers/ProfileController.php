<?php

class ProfileController extends Zend_Controller_Action
{
	/**
	 * @var Model_Escort_Profile
	 */
	protected $profile;

	/**
	 * @var Model_EscortItem
	 */
	protected $escort;

	/**
	 * @var Zend_Session_Namespace
	 */
	protected $session;
	protected $adv_session;

	public static $linkHelper;
	
	public function init()
	{
		self::$linkHelper = $this->view->getHelper('GetLink'); 
		$this->_request->setParam('no_tidy', true);
		
		$anonym = array('fast-profile', 'fast-advertise-now', 'advertise-success', 'advertise-failure', 'advertise-cancel', 'simple', 'gallery-create');
		
		$this->view->user = $this->user = Model_Users::getCurrent();
		
		$this->adv_session = new Zend_Session_Namespace('adv_sess');
		
		if ( $this->_request->getActionName() == 'fast-profile' && ! $this->_request->isPost() ) {
			$this->adv_session->unsetAll();
		}		
		
		if ( isset($this->adv_session->user_id) && isset($this->adv_session->ep) )
			$this->view->user = $this->user = new Model_UserItem(array('id' => $this->adv_session->user_id, 'user_type' => $this->adv_session->user_type));
			
		if ( ! isset($this->adv_session->user_id) && (! $this->user) && ! in_array($this->_request->getActionName(), $anonym) ) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}
		
		$this->view->layout()->setLayout('private-profile');
		$this->view->addScriptPath($this->view->getScriptPath('private'));

		$this->step = $this->_getParam('step'); 

		$escort_id = intval($this->_getParam('escort'));
		if ( ! $this->user && in_array($this->_request->getActionName(), $anonym) ) {
			$this->escort = new Model_EscortItem(array('id' => null));
		} elseif ( $this->user->isAgency() && $escort_id > 1 ) {
			$this->agency = $agency = $this->user->getAgency();
			if ( ! $agency->hasEscort($escort_id) ) {
				return $this->_redirect($this->view->getLink('private-v2-escorts'));
			}

			$escorts = new Model_Escorts();
			$this->escort = $escorts->getById($escort_id);
		}
		elseif ( $this->user->isAgency() ) {
			$this->agency = $agency = $this->user->getAgency();
			$this->escort = new Model_EscortItem(array('id' => null));
		}
		else {
			$this->escort = $this->user->getEscort();
			if ( is_null($this->escort) ) $this->escort = new Model_EscortItem(array('id' => null));
		}

		
		if ( isset($this->adv_session->escort_id) ) {
			$this->escort = new Model_EscortItem(array('id' => $this->adv_session->escort_id));
		}
        
		$this->view->escort = $this->escort;
        
        if(isset($this->escort->is_suspicious) && $this->escort->is_suspicious){
            $this->_redirect($this->view->getLink('private'));
			return;
        }
		

		// Determine the mode depending on if user has profile
		$this->mode = $this->view->mode = ((isset($this->escort->id) && $this->escort->id && $this->escort->hasProfile() && $this->step != 'finish') ? 'update' : 'create');

		if ( isset($this->adv_session->escort_id) ) {
			$this->mode = $this->view->mode = 'update';
		}
		
		$session_hash = '';
		if ( $this->_request->session_hash ) {
			$session_hash = $this->view->session_hash = $this->_request->session_hash;
		}
		else {
			$session_hash = $this->view->session_hash = md5(microtime() * time());
		}

		$this->session = new Zend_Session_Namespace('profile-data-' . $session_hash);

		if ( $this->escort ) {
			$this->profile = $this->view->profile = $this->escort->getProfile();
			$this->profile->setSession($this->session);
			$this->profile->load();
			//print_r($this->profile);die;
		}

		// If we are in update mode, then we need to set the profile model
		// update in real-time mode instead of updating session
		
		if ( 'update' == $this->mode ) {
			$this->profile->setMode(Model_Escort_Profile::MODE_REALTIME);
		}
		
		$this->defines = $this->view->defines = Zend_Registry::get('defines');
		
		$config = Zend_Registry::get('escorts_config');
		$steps = array('biography', 'about-me', 'languages', 'working-cities', 'services', 'working-times');
				
		if ($config['profile']['rates'])
			$steps[] = 'prices';
		
		$steps[] = 'contact-info';
		$steps[] = 'gallery';
		$steps[] = 'finish';
		
		$this->steps = $steps;
	}

	protected $_c = 0;

	//protected $steps = array('biography', 'about-me', 'languages', 'working-cities', 'services', 'working-times', 'prices', 'contact-info', 'gallery', 'finish');
	protected $steps = array();
	protected $_posted = false;
	
	public function fastProfileAction()
	{
		$this->_posted = $this->_request->isPost();
		
		//$this->adv_session = new Zend_Session_Namespace('adv_sess');
		
		if ( ! $this->_posted ) {
			$this->adv_session->unsetAll();
		}
		
		if ( $this->user && ! isset($this->adv_session->ep) ) {
			$this->_redirect($this->view->getLink('private-v2-advertise-now'));
		}
		$this->view->layout()->setLayout('simple-profile');
		$this->view->addScriptPath($this->view->getScriptPath('private'));
		$this->_helper->viewRenderer->setScriptAction("simple");
		
		$this->view->simple = true;
		
		$this->view->show_advertise_block = 0;
		$this->view->show_signup_block = 1;		
		$this->view->show_next_preview = 1;
		
		$this->view->fp_advertise_block = 1;		
		$this->adv_session->fp_advertise_block = $this->view->fp_advertise_block;
		
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$phone_packages = $client->call('Billing.getPackages', array(1, $application_id));
		$this->view->phone_packages = $phone_packages;
		
		
		
		$data = array();
		$errors = array();
		$is_error = false;
		$counter = 0;

		$this->view->step = 'biography';
		$steps = array('fast-signup', 'biography', 'about-me', 'languages', 'working-cities', 'services', 'working-times', 'prices', 'contact-info', 'finish'/*, 'gallery-simple'*/);
		
		foreach($steps as $step) {
			$method = str_replace('-', ' ', $step);
			$method = ucwords($method);
			$method = str_replace(' ', '', $method);
			$method[0] = strtolower($method[0]);

			if( $step === 'finish' && $this->_posted && ! $is_error ) {
				$this->finishAction();
				$this->view->step = 'finish';
				$eid = (int)$this->view->escort_id;
			} elseif ($step !== 'finish') {
				if ( $step === 'gallery-simple' && ! $this->_request->isPost() ){
					continue;
				}
				$this->{$method . 'Action'}();
			} else {
				continue;
			}
			
			$tmpData = $this->view->data;
			if( $tmpData ) {
				$data = array_merge($data, $tmpData);
			}
			
			$tmpErrors = $this->view->errors;
			
			if( $tmpErrors ) {
				$errors = array_merge($errors, $tmpErrors);
				$is_error = true;
			}
			$counter++;
		}
		
		if ( $this->_posted && $this->adv_session->fp_advertise_block ) {
			
			$this->adv_session->fp_package_id = $this->view->fp_package_id = $this->_request->package_id;
			$this->adv_session->fp_activation_type = $this->view->fp_activation_type = $this->_request->activation_type;
			$this->adv_session->fp_activation_date = $this->view->fp_activation_date = $this->_request->activation_date;
			
			if ( ! $this->_request->package_id ) {
				$errors['fp_package'] = Cubix_I18n::translate('please_select_pass_package');
			}
			
			if ( ! $this->_request->activation_type ) {
				$errors['fp_package_act_type'] = Cubix_I18n::translate('please_select_pass_package_act_type');
			}
			
			if ( $this->_request->activation_type == 'specific_date' && ! $this->_request->activation_date ) {
				$errors['fp_package_act_date'] = Cubix_I18n::translate('please_select_pass_package_act_date');
			}
			elseif ( $this->_request->activation_type == 'specific_date' && $this->_request->activation_date ) {
				if ( $this->_request->activation_date <= time() ) {
					$errors['fp_package_act_date_future'] = Cubix_I18n::translate('please_select_pass_package_act_date_future');
				}
			}
		}
		
		$this->view->data = $data;
		$this->view->errors = $errors;
		
		if ( $this->_posted && count($this->adv_session->photo_upload_errors) ) {
			$this->view->uploadErrors = $this->adv_session->photo_upload_errors;
		}
		
		if ( $this->_posted && ! count($this->adv_session->photos) && $this->mode == 'create' ) {
			$this->view->uploadError = Cubix_I18n::translate('sys_error_select_photo');
		}		
		else if ( $this->_posted && $this->mode == 'create' && ! $this->_hasMainPhotoInSession() ) {
			$this->view->uploadError = Cubix_I18n::translate('sys_error_please_select_main_photo');
		}
		
		if ( $this->_posted && ! count($errors) && ! $this->view->uploadError && ! $this->view->uploadErrors ) {
			if ( $eid ) {
				$this->adv_session->showname = $data['showname'];
				$this->_setParam('escortName', $data['showname']);
				
				$this->adv_session->escort_id = $eid;
				$this->_setParam('escort_id', $eid);
				
				$this->adv_session->prev = 1;
				$this->_setParam('prev', 1);
				
				$this->adv_session->show_advertise_block = 1;
				$this->_setParam('show_advertise_block', 1);
				
				$this->adv_session->user_type = $this->view->user_type = $this->_getParam('user_type');
				
				$this->adv_session->user_id = $this->view->new_user_id;
				$this->adv_session->ep = 1;
				
				$this->adv_session->username = $this->_request->signup_username;
				$this->adv_session->password = $this->_request->signup_password;
				
				$this->_moveVirtualPhotos($eid);
			}
			$this->_forward('profile-v2', 'escorts', 'default');
		}
		else if ( $this->_posted && count($errors) || $this->view->uploadError || $this->view->uploadErrors ) {
			$client = Cubix_Api_XmlRpc_Client::getInstance();
			$client->call('Users.delete', array($this->view->new_user_id));
			
			$uploaded_photos = $this->adv_session->photos;			
			$this->adv_session->unsetAll();
			$this->adv_session->photos = $uploaded_photos;
		}
	}

    public function simpleAction()
	{
		$this->_posted = $this->_request->isPost();

		if ( ! $this->_posted ) {
			unset($this->adv_session->photos);
		}
		
		if ( $this->mode == 'update' && $this->adv_session->fp_advertise_block && ! $this->posted ) {
			$this->view->fp_advertise_block = 1;
			$this->view->fp_package_id = $this->adv_session->fp_package_id;
			$this->view->fp_activation_type = $this->adv_session->fp_activation_type;
			$this->view->fp_activation_date = $this->adv_session->fp_activation_date;
		}

		$this->view->simple = true;
		$this->_setParam('simple', true);

		$this->view->show_advertise_block = $this->_getParam('show_advertise_block', 0);
		$this->view->from_advertise_block = $this->_getParam('from_advertise_block', 0);
		
		if ( isset($this->adv_session->ep) || $this->view->from_advertise_block ) {
			
			//if ( $this->_posted && ($this->adv_session->escort_id != $this->_getParam('escort_id') ) ) die('He he )');
			
			$this->view->dont_show_top_bar = true;
			$this->view->show_next_preview = true;
		}

		if ( $this->view->show_advertise_block || isset($this->adv_session->ep) || $this->view->from_advertise_block ) {
			$client = Cubix_Api_XmlRpc_Client::getInstance();
			$phone_packages = $client->call('Billing.getPackages', array(1, $application_id));
			$this->view->phone_packages = $phone_packages;
		}

		$this->view->layout()->setLayout('simple-profile');
		$this->view->addScriptPath($this->view->getScriptPath('private'));

		$this->_helper->viewRenderer->setScriptAction("simple");

		$this->agency = $agency = $this->user->getAgency();
		if ( $this->mode == 'create' ) {
			$this->escort = new Model_EscortItem(array('id' => null));
		}

		$data = array();
		$errors = array();
		$is_error = false;
		$counter = 0;

		$this->view->step = 'biography';
		$steps = array('biography', 'about-me', 'languages', 'working-cities', 'services', 'working-times', 'prices', 'contact-info');

		if ( $this->mode != 'update' ) {
			$steps = array_merge($steps, array('finish'/*, 'gallery-simple'*/));
		}
		else {
			$steps = array_merge($steps, array('gallery'));
		}

		foreach($steps as $step) {
			$method = str_replace('-', ' ', $step);
			$method = ucwords($method);
			$method = str_replace(' ', '', $method);
			$method[0] = strtolower($method[0]);

			if($step === 'finish' && $this->_posted && ! $is_error){
					$this->setSession();
					$this->finishAction();
					$this->view->step = 'finish';
					$eid = (int)$this->view->escort_id;

			} elseif ($step !== 'finish') {
				if ( $step === 'gallery-simple' && ! $this->_request->isPost() ){
					continue;
				}
				
				if ( $this->mode == 'update' )
					$this->profile->load(true);
				
				$this->{$method . 'Action'}();
			} else {					
				continue;
			}

			$tmpData = $this->view->data;
			if( $tmpData ) {
				$data = array_merge($data, $tmpData);
			}

			$tmpErrors = $this->view->errors;
			if( $tmpErrors ) {
				$errors = array_merge($errors, $tmpErrors);
				$is_error = true;
			}

			$counter++;
		}

		if ( $this->_posted && $this->adv_session->fp_advertise_block ) {
			$this->adv_session->fp_package_id = $this->view->fp_package_id = $this->_request->package_id;
			$this->adv_session->fp_activation_type = $this->view->fp_activation_type = $this->_request->activation_type;
			$this->adv_session->fp_activation_date = $this->view->fp_activation_date = $this->_request->activation_date;
			
			if ( ! $this->_request->package_id ) {
				$errors['fp_package'] = Cubix_I18n::translate('please_select_pass_package');
			}
			
			if ( ! $this->_request->activation_type ) {
				$errors['fp_package_act_type'] = Cubix_I18n::translate('please_select_pass_package_act_type');
			}
			
			if ( $this->_request->activation_type == 'specific_date' && ! $this->_request->activation_date ) {
				$errors['fp_package_act_date'] = Cubix_I18n::translate('please_select_pass_package_act_date');
			}
			elseif ( $this->_request->activation_type == 'specific_date' && $this->_request->activation_date ) {
				if ( $this->_request->activation_date <= time() ) {
					$errors['fp_package_act_date_future'] = Cubix_I18n::translate('please_select_pass_package_act_date_future');
				}
			}
		}
		
		$this->view->data = $data;

		/*if( !$this->_posted ) {
			$this->view->data['phone_number'] = $agency['phone'];
			$this->view->data['email'] = $agency['email'];
			$this->view->data['website'] = $agency['web'];
		}*/

		$this->view->errors = $errors;
		
		if ( $this->_posted && count($this->adv_session->photo_upload_errors) ) {
			$this->view->uploadErrors = $this->adv_session->photo_upload_errors;
		}
		
		if ( $this->_posted && ! count($this->adv_session->photos) && $this->mode == 'create' ) {
			$this->view->uploadError = Cubix_I18n::translate('sys_error_select_photo');
		}
		else if ( $this->_posted && $this->mode == 'create' && ! $this->_hasMainPhotoInSession() ) {
			$this->view->uploadError = Cubix_I18n::translate('sys_error_please_select_main_photo');
		}
		
		if ( $this->_getParam('cc') ) {
			return $this->_forwardToProfile($data);
		}

		if ( $this->_posted && ! count($errors) && ! $this->_getParam('upload') 
				&& ! $this->_getParam('set_main') && ! $this->_getParam('make_private')
				&& ! $this->_getParam('delete') && ! $this->_getParam('make_public') ) {

			if ( $this->adv_session->ep ) {
				return $this->_forwardToProfile($data);
			}			
			else if ( $this->_getParam('from_advertise_block') ) {
				$this->_setParam('escortName', $data['showname']);
				$this->_setParam('prev', 1);
				$this->_setParam('escort_id', ($eid) ? $eid : $this->_getParam('escort') );
				$this->_setParam('show_advertise_block', 1);
				
				if ( $this->mode == 'create' )
					$this->_moveVirtualPhotos($eid);
				
				return $this->_forward('profile-v2', 'escorts', 'default');
			}
		}
		
		$this->view->saved = 0;
		if ( $this->_posted && count($errors) == 0){
			$this->view->saved = 1; //saved
		}
		elseif($this->_posted && count($errors) > 0){
			$this->view->saved = 2; //not saved
		}
		
		if ( $this->_posted && ! count($errors) && $this->mode == 'create' && ! $this->view->uploadError && ! $this->view->uploadErrors && ! $this->_getParam('from_advertise_block') ) {
			$this->_moveVirtualPhotos($eid);
			
			$this->_redirect($this->view->getLink('private-v2'));
		}
    }
	
	protected function setSession()
	{
		$arr = array();
		foreach ($this->update_data as $data) {
			$arr = array_merge($arr, $data);
		}
		
		$this->profile->setSess($arr, true);
		
		//var_dump($arr);die;
	}
	
	private function _moveVirtualPhotos($escort_id)
	{
		$images = new Cubix_Images();
		
		$photos = $this->adv_session->photos;
		
		if ( count($photos) ) {
			foreach( $photos as $photo ) {
				$images->saveVirtualPhotos($photo->hash . '.' . $photo->ext, $escort_id, Cubix_Application::getId(), strtolower(@end(explode('.', $photo->hash . '.' . $photo->ext))), $photo->hash);
				$photo->escort_id = $escort_id;
				$model = new Model_Escort_Photos();
				$model->save($photo, $this->_hasMainPhotoInSession());
			}
		}
	}
	
	private function _forwardToProfile($data)
	{
		$this->_setParam('escortName', $data['showname']);
		$this->_setParam('escort_id', $this->adv_session->escort_id);
		$this->_setParam('prev', $this->adv_session->prev);
		$this->_setParam('show_advertise_block', $this->adv_session->show_advertise_block);
		$this->_setParam('user_type', $this->adv_session->user_type);
		$this->view->user_type = $this->adv_session->user_type;
		$this->_setParam('user_id', $this->adv_session->user_id);
		$this->view->new_user_id = $this->adv_session->user_id;

		$this->_forward('profile-v2', 'escorts', 'default');
	}

	public function indexAction()
	{
//        if($this->escort->is_suspicious){
//            $this->_response->setRedirect($this->view->getLink('signin'));
//			return;
//        }

	
//		var_Dump( $_POST );
//		exit;
		//$titles = array('biography', 'about_me', 'languages', 'working_cities', 'services', 'working_times', 'prices', 'contact_info', 'gallery', 'finish' );
		$config = Zend_Registry::get('escorts_config');
		$titles = array('biography', 'about_me', 'languages', 'working_cities', 'services', 'working_times');
				
		if ($config['profile']['rates'])
			$titles[] = 'prices';
		
		$titles[] = 'contact_info';
		$titles[] = 'gallery';
		$titles[] = 'finish';
		
		foreach ( $titles as $i => $key ) {
			$titles[$i] = __('pv2_tab_' . $key);
		}

		// If the mode is update, i.e. user already has a profile revision
		// we don't need the last step "Finish"
		if ( $this->mode == 'update' ) {
			unset($this->steps[count($this->steps) - 1]);
			unset($titles[count($titles) - 1]);
		}

		$this->view->steps = array_combine($this->steps, $titles);

		$this->_posted = $this->_request->isPost() && (
			($this->_getParam('then') == ':next' && $this->mode == 'create') ||
			( $this->mode == 'update' )
		);
		$result = $this->_do();

		
		$then = $this->_getParam('then');
     
		// If the user clicked the update button, and the result is successfull
		// and we don't need to switch to another tab, just display
		// a user friendly message
		if ( $this->_posted && true === $result && ! strlen($then) ) {
			$this->view->status = 'The section "' . $this->view->steps[$this->view->step] . '" has been successfully updated!';
		}

		if ( false === $result || ! strlen($then) ) { return; }

		switch (true) {
			case ($then == ':next'):
				$i = array_search($this->_getParam('step'), $this->steps);
				if ( $i == count($this->steps) - 1 ) return;
				
				$step = $this->steps[$i + 1];
				
				break;
			case ($then == ':back'):
				$i = array_search($this->_getParam('step'), $this->steps);
				if ( $i == 0 ) return;

				$step = $this->steps[$i - 1];

				break;
			default:
                
				if ( ! in_array($then, $this->steps) ) {
					return;
				}

				$step = $then;
		}

		$this->_setParam('step', $step);
		$this->_setParam('then', '');
		$this->_posted = false;
		
		$this->_do();

       


		
	}

	protected function _do()
	{
		$step = $this->_getParam('step');

		// User requested editting of profile without specifying
		// the step, i.e. if he/she clicks on profile icon in private are
		// load the profile from api into current session
		if ( ! in_array($step, $this->steps) ) {
			$this->profile->load(true);
			$step = reset($this->steps);
		}

		$this->view->step = $step;

		$method = str_replace('-', ' ', $step);
		$method = ucwords($method);
		$method = str_replace(' ', '', $method);
		$method[0] = strtolower($method[0]);

		$this->_helper->viewRenderer->setScriptAction($step);
		
		
		if ( $method == 'gallery' ){
			
			if ( $this->mode == 'create' ) {
				$this->finishAction();
				$tid = (int)$this->view->escort_id;
				$this->_redirect($this->view->getLink('private-v2-profile', array('step' => 'gallery', 'escort' => $tid, 'show_success' => 1), true));
			}
		}
			
		return $this->{$method . 'Action'}();
	}

	protected function _validateBiography($validator)
	{
		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'showname' => '',
			'slogan' => 'notags|special',
			'gender' => 'int-nz',
			'age' => 'int-nz',
			'ethnicity' => 'int-nz',
			'nationality_id' => 'int-nz',
			'home_city_id' => 'int-nz',
			'hair_color' => 'int-nz',
			'eye_color' => 'int-nz',
			'measure_units' => 'int-nz',
			'height' => 'int-nz',
			'weight' => 'int-nz',
			'dress_size' => '',
			'shoe_size' => 'int-nz',
			'bust' => 'int-nz',
			'waist' => 'int-nz',
			'hip' => 'int-nz',
			'cup_size' => '',
			'pubic_hair' => 'int-nz'
		));
		$data = $form->getData();

		$defines = Zend_Registry::get('defines');
		$data['showname'] = preg_replace('/(\s)+/','$1', trim($data['showname']));
		$escorts_model = new Model_Escorts();
		if ( ! strlen($data['showname']) ) {
			$validator->setError('showname', Cubix_I18n::translate('sys_error_required'));
		}
		elseif ( mb_strlen($data['showname']) > 25  ) {
			$validator->setError('showname', Cubix_I18n::translate('showname_limit', array('COUNT' => 25)));
		}
		elseif ( ! preg_match('#^[-_a-z0-9\s]+$#i', $data['showname']) ) {
			$validator->setError('showname', Cubix_I18n::translate('sys_error_must_contain'));
		}
		/*elseif ( $escorts_model->existsByShowname($data['showname'], $this->escort->getId()) ) {
			$validator->setError('showname', 'An escort with same showname exists');
		}*/

		if ( ! strlen($data['home_city_id']) ) {
//			$validator->setError('home_city_a', 'Home city is required');
			$data['home_city_id'] = null;
		}

		if ( ! strlen($data['slogan']) ) {
			$data['slogan'] = null;
		}
		else if( mb_strlen($data['slogan']) > 20 ) {
			$validator->setError('slogan', Cubix_I18n::translate('sys_error_slogan_text_must_be'));
		}
		
		else{
			$data['slogan'] = $validator->urlCleaner($data['slogan'], Cubix_Application::getById()->host);
		}
		
		if ( is_null($data['gender']) ) {
			$validator->setError('gender', Cubix_I18n::translate('sys_error_required'));
		}
		else if ( ! array_key_exists($data['gender'], $defines['gender_options']) ) {
			$validator->setError('gender', Cubix_I18n::translate('sys_error_gender_is_invalid'));
		}

		$age = $data['age']; unset($data['age']);
		if ( ! is_null($age) ) {
			$data['birth_date'] = strtotime('-' . $age . ' year');
		}

		if ( ! in_array($data['measure_units'], array(METRIC_SYSTEM, ROYAL_SYSTEM)) ) {
			$data['measure_units'] = METRIC_SYSTEM;
		}

		if ( ! in_array($data['cup_size'], $this->defines['breast_size_options']) ) {
			$data['cup_size'] = null;
		}

		if ( Cubix_Application::getById()->measure_units == ROYAL_SYSTEM ) {
			if ( $data['height'] ) {
				$data['height'] = Cubix_UnitsConverter::convert(stripslashes($data['height']), 'ftin', 'cm');
			}

			if ( $data['weight'] ) {
				$data['weight'] = Cubix_UnitsConverter::convert($data['weight'], 'lbs', 'kg');
			}
		}

		$data['height'] = intval($data['height']);
		if ( ! $data['height'] ) $data['height'] = null;
		if ( Cubix_Application::getById()->measure_units != ROYAL_SYSTEM ) {
			$data['weight'] = intval($data['weight']);
		}
		if ( ! $data['weight'] ) $data['weight'] = null;

		return $data;
	}

	public function biographyAction()
	{
		$escort_id = $this->escort->id;
		if ( isset($this->adv_session->escort_id) ) {
			$escort_id = $this->adv_session->escort_id;
		}
		
		if ( $this->_posted ) {			
			$validator = new Cubix_Validator();
			
			$this->view->data = $data = $this->_validateBiography($validator);

			$this->view->gender = $data['gender'];
			$home_city_id = $this->view->data['home_city_id'];

			$home_city_country = '';
			if ( $home_city_id ) {
				$m_city = new Cubix_Geography_Cities();
				$home_city = $m_city->get($home_city_id);

				$m_country = new Cubix_Geography_Countries();
				$home_country = $m_country->get($home_city->country_id);

				$home_city_country = $home_city->{'title_' . $this->_request->lang_id} . ' (' . $home_country->{'title_' . $this->_request->lang_id} . ')';
			}
			$this->view->home_city_country = $home_city_country;

			$this->view->slogan = $this->_getParam('slogan');
			
			if ( $this->_getParam('dont') ) {
				return false;
			}

			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();
				
				$this->view->errors = $status['msgs'];
				return false;
			}
			
			$this->_addSlogan($data['slogan'], $escort_id);
			//$this->view->slogan = $this->_getParam('slogan', $this->_getSlogan($escort_id));
			$slogan = $this->_getSlogan($this->escort->id);
			$this->view->slogan = $slogan['text'];
			$this->view->slogan_status = $slogan['status'];
			
			return $this->profile->update($data, 'biography');
		}
		elseif ( $this->user ) {
			$this->view->data = $this->profile->getBiography();

			$home_city_id = $this->view->data['home_city_id'];

			$home_city_country = '';
			if ( $home_city_id ) {
				$m_city = new Cubix_Geography_Cities();
				$home_city = $m_city->get($home_city_id);

				$m_country = new Cubix_Geography_Countries();
				$home_country = $m_country->get($home_city->country_id);

				$home_city_country = $home_city->{'title_' . $this->_request->lang_id} . ' (' . $home_country->{'title_' . $this->_request->lang_id} . ')';
			}
			$this->view->home_city_country = $home_city_country;
			$slogan = $this->_getSlogan($this->escort->id);
			$this->view->slogan = $slogan['text'];
			$this->view->slogan_status = $slogan['status'];
		}
		
		$slogan = $this->_getSlogan($this->escort->id);
		$this->view->slogan = $slogan['text'];
		$this->view->slogan_status = $slogan['status'];
	}

	protected function _addSlogan($slogan, $escort_id)
	{
		$config_system = Zend_Registry::get('system_config');
		
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$client->call('Escorts.addSlogan', array($slogan, $escort_id, $config_system['sloganApprovation']));

		return true;
	}

	protected function _getSlogan($escort_id)
	{//var_dump($escort_id); die;
		if ( $escort_id ) {
			
			$client = Cubix_Api_XmlRpc_Client::getInstance();
			$slogan = $client->call('Escorts.getSlogan', array($escort_id));
			
			return $slogan;
		}
		else {
			return false;
		}
	}

	protected function _validateAboutMe($validator)
	{
		$blackListModel = new Model_BlacklistedWords();
		$form = new Cubix_Form_Data($this->_request);
		$fields = array(
			'is_smoking' => 'int-nz',
			'is_drinking' => 'int-nz',
			'characteristics' => 'notags|special'
		);

		foreach ( Cubix_I18n::getLangs(true) as $lng ) {
			$fields['about_' . $lng] = 'xss';
		}

		$form->setFields($fields);
		$data = $form->getData();
		
		foreach ( Cubix_I18n::getLangs(true) as $lng ) {
			if($bl_words = $blackListModel->checkWords($data['about_' . $lng], Model_BlacklistedWords::BL_TYPE_ABOUT)) {
				/*foreach($bl_words as $bl_word){
					$pattern = '/' . preg_quote($bl_word, '/') . '/';
					$data['about_' . $lng] = preg_replace($pattern, '<abbr class = "black-listed" >' . $bl_word . '</abbr>', $data['about_' . $lng]);
				}*/

				$validator->setError('about_me_text', 'You can`t use word "'.$blackListModel->getWords() .'"');
				break;
			}
			else
				$data['about_' . $lng] = $validator->urlCleaner($data['about_' . $lng], Cubix_Application::getById()->host);
				$data['about_' . $lng] = $validator->specialCharsCleaner($data['about_' . $lng]);
		}
		
		$data['characteristics'] = $validator->urlCleaner($data['characteristics'], Cubix_Application::getById()->host);
					
		return $data;
	}

	public function aboutMeAction()
	{
		if ( $this->_posted ) {
			$validator = new Cubix_Validator();
			$blackListModel = new Model_BlacklistedWords();
			$this->view->data = $data = $this->_validateAboutMe($validator);

			$data['about_has_bl'] = 0;
			if ( ! $validator->isValid() ) {
				
				$status = $validator->getStatus();
				
				$this->view->errors = $status['msgs'];
				return false;
			}
			/*foreach ( Cubix_I18n::getLangs(true) as $lng ) {
				if($blackListModel->checkWords($data['about_' . $lng], Model_BlacklistedWords::BL_TYPE_ABOUT)){
					$data['about_has_bl'] = 1; BREAK;
				}
			}*/
			return $this->profile->update($data, 'about-me');
		}
		elseif ( $this->user ) {
			$this->view->data = $this->profile->getAboutMe();
		}
	}

	public function _validateLanguages($validator)
	{
		$defines = Zend_Registry::get('defines');
		
		$langs = $this->_getParam('langs', array());
		if ( ! is_array($langs) ) $langs = array();

		$data = array('langs' => array());

		$invalid_lang = false;
		foreach ( $langs as $lang_id => $level ) {
			$level = intval($level); if ( $level < 1 ) continue;
			$data['langs'][] = array('lang_id' => $lang_id, 'level' => $level);

			if ( ! array_key_exists($lang_id, $defines['language_options']) ) {
				$invalid_lang = true;
			}
		}

		/*if ( ! count($data['langs']) ) {
			$validator->setError('language_error', Cubix_I18n::translate('sys_error_language_required'));
		}
		else if ( $invalid_lang ) {
			$validator->setError('language_error', Cubix_I18n::translate('sys_error_lang_is_invalid'));
		}*/

		return $data;
	}

	public function languagesAction()
	{
		if ( $this->_posted ) {
			$validator = new Cubix_Validator();

			$this->view->data = $data = $this->_validateLanguages($validator);

			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];
				return false;
			}

			return $this->profile->update($data, 'languages');
		}
		elseif ( $this->user ) {
			$this->view->data = $this->profile->getLanguages();
		}
	}

	protected function _validateWorkingCities($validator)
	{
		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'country_id' => 'int-nz',
			'city_id' => 'int-nz',
			'cities' => 'int-nz',
			'cityzones' => 'arr-int',
			'zip' => '',
			'incall' => 'int',
			'incall_type' => 'int-nz',
			'incall_hotel_room' => 'int-nz',
			'incall_other' => 'notags|special',
			'outcall' => 'int',
			'outcall_type' => 'int-nz',
			'outcall_other' => 'notags|special'
		));
		$data = $form->getData();
		
		if ( $data['cities'] ) {
			$data['cities'] = array($data['cities']);
		}
		
		if ( ! is_array($data['cities']) ) $data['cities'] = array();
		
		$invalid_city = false;
		foreach ( $data['cities'] as $i => $city_id ) {
			$data['cities'][$i] = array('city_id' => $city_id);
			if ( ! $city_id ) {
				unset($data['cities'][$i]);
			}
			/*else if ( ! Cubix_Geography_Cities::isFromApplicationCountry($city_id) ) {
				$invalid_city = true;
				unset($data['cities'][$i]);
			}*/
		}
		
		// Anti-Hack: Ignore trailing cities
		$data['cities'] = array_slice($data['cities'], 0, Cubix_Application::getById()->max_working_cities);

		if ( ! is_array($data['cityzones']) ) $data['cityzones'] = array();

		foreach ( $data['cityzones'] as $i => $zone_id ) {
			if ( $zone_id != '0' ) {
				$data['cityzones'][$i] = array('city_zone_id' => $zone_id);
			}
			else {
				unset($data['cityzones'][$i]);
			}
		}
		
		if ( ! $data['incall'] ) {
			$data['incall_type'] = null;
			$data['incall_hotel_room'] = null;
			$data['incall_other'] = null;
			$data['zip'] = null;
		}
		else{
			if ( !$data['incall_type'] ) {
			$validator->setError('zip', Cubix_I18n::translate('sys_error_choose_suboption'));
			$data['incall_type'] = 999;
			}
		}

		if ( ! $data['outcall'] ) {
			$data['outcall_type'] = null;
			$data['outcall_other'] = null;
		}
		

		if ( $data['incall'] && ! strlen($data['zip']) ) {
//			$validator->setError('zip', 'zip code required');
            $data['zip'] = null;
		}
		unset($data['incall']);
		unset($data['outcall']);
		
		if ( ! is_null($data['incall_type']) && ! $validator->validateZipCode($data['zip'], false) ) {
//			$validator->setError('zip', 'Invalid zip code');
            $data['zip'] = null;
		}
		
		$data['zip'] = $validator->urlCleaner($data['zip'], Cubix_Application::getById()->host);
		$data['incall_other'] = $validator->urlCleaner($data['incall_other'], Cubix_Application::getById()->host);
		$data['outcall_other'] = $validator->urlCleaner($data['outcall_other'], Cubix_Application::getById()->host);
		
		if ( ! is_null($data['incall_type']) && $data['incall_type'] == 2 ) {
			if ( ! isset($data['incall_hotel_room']) ) {
				$validator->setError('incall_hotel_room', Cubix_I18n::translate('sys_error_choose_hotel_room'));
			}
		}
		if ( ! $data['city_id'] || ! count($data['cities']) ) {
			$validator->setError('country_id', Cubix_I18n::translate('sys_error_base_city_required'));
		}
		/*else if ( ! Cubix_Geography_Cities::isFromApplicationCountry($data['city_id']) ) {
			$validator->setError('country_id', 'Base city is not from ' . Cubix_Application::getById()->country_title);
		}
		else if ( $invalid_city ) {
			$validator->setError('country_id', 'Selected cities are not from ' . Cubix_Application::getById()->country_title);
		}*/

		if ( ! $data['country_id'] ) {
			$validator->setError('country_id', Cubix_I18n::translate('sys_error_required'));
		}
		/*else if ( $data['country_id'] != Cubix_Application::getById()->country_id ) {
			$validator->setError('country_id', 'invalid country selected');
		}*/
		
		return $data;
	}

	public function workingCitiesAction()
	{
		
		$countries = new Cubix_Geography_Countries();
		$this->view->countries = $countries->ajaxGetAll(false);

		$cities = new Cubix_Geography_Cities();

		$country_id = Cubix_Application::getById()->country_id;
		
		if ( $this->user ) {
			$data = $this->profile->getWorkingCities();
			if ( isset($data['city_id']) && $data['city_id'] ) {
				$city = $cities->get($data['city_id']);
				$country_id = $city->country_id;
			}
		}
		$region_cities = $this->view->cities = $cities->ajaxGetAll(null, $country_id);
		
		$regions = array();
		foreach ( $region_cities as $city ) {
			if ( ! isset($regions[$city->region_title]) ) {
				$regions[$city->region_title] = array();
			}

			$regions[$city->region_title][] = $city;
		}

		ksort($regions);
		$this->view->regions = $regions;
		
		if ( $this->_posted ) {
			$validator = new Cubix_Validator();

			$this->view->data = $data = $this->_validateWorkingCities($validator);

			$param_cities = array();
			if ( count($data['cities']) ) {
				foreach ( $data['cities'] as $city ) {
					$param_cities[] = $city['city_id'];
				}
			}

			$this->_request->setParam('ajax_cities', implode(',', $param_cities));

			$this->getCityzonesAction();
			$this->view->layout()->enableLayout();

			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];
				return false;
			}

			return $this->profile->update($data, 'working-cities');
		}
		elseif ( $this->user ) {

			$data = $this->profile->getWorkingCities();
			
			$param_cities = array();
			if ( count($data['cities']) ) {
				foreach ( $data['cities'] as $city ) {
					$param_cities[] = $city['city_id'];
				}
			}

			$this->_request->setParam('ajax_cities', implode(',', $param_cities));

			$this->getCityzonesAction();
			$this->view->layout()->enableLayout();

			$data['country_id'] = $country_id;

			$this->view->data = $data;
		}
	}

	public function getCityzonesAction()
	{
		$this->view->layout()->disableLayout();

		$cities = $this->_request->ajax_cities;
		$cities = trim($cities, ',');

		$cities = explode(',', $cities);

		$cz_model = new Cubix_Geography_Cityzones();

		$all_cityzones = array();
		if ( count($cities) ) {
			foreach( $cities as $city ) {
				$cityzones = $cz_model->ajaxGetAll($city);
				if ( count($cityzones) ) {
					$all_cityzones = array_merge($all_cityzones, $cityzones);
				}
			}
		}

		if ( isset($_SERVER['HTTP_X_REQUESTED_WITH']) ) {
			die(json_encode($all_cityzones));
		}
		else {
			$this->view->all_cityzones = $all_cityzones;
		}
	}

	public function _validateServices($validator)
	{		
		$form = new Cubix_Form_Data($this->_request);
		$fields = array(
			'sex_orientation' => 'int-nz',
			'sex_availability' => '',
			'services' => 'arr-int',
			'service_prices' => 'arr-int',
			'service_currencies' => 'arr-int'
		);

		foreach ( Cubix_I18n::getLangs(true) as $lng ) {
			$fields['additional_service_' . $lng] = 'notags|special';
		}

		$form->setFields($fields);
		$data = $form->getData();

		if ( ! is_array($data['services']) ) $data['services'] = array();

		foreach ( $data['services'] as $i => $svc ) {
			$price = isset($data['service_prices'][$svc]) ? intval($data['service_prices'][$svc]) : null;
			if ( ! $price ) $price = null;

			$currency = isset($data['service_currencies'][$svc]) ? intval($data['service_currencies'][$svc]) : null;
			if ( ! $currency ) $currency = null;
			
			$data['services'][$i] = array('service_id' => $svc, 'price' => $price, 'currency_id' => $currency);
		}

		if ( ! is_array($data['sex_availability']) ) {
			$data['sex_availability'] = array();
			//$validator->setError('services_offered_for_error', Cubix_I18n::translate('sys_error_at_least_one_required'));
		}

		foreach ( $data['sex_availability'] as $i => $opt ) {
			if ( ! isset($this->defines['sex_availability_options'][$opt]) ) {
				unset($data['sex_availability'][$i]);
			}
		}
		
		$data['sex_availability'] = count($data['sex_availability']) ? implode(',', $data['sex_availability']) : array();
		//var_dump($data);die;
		
		unset($data['service_prices']);
		unset($data['service_currencies']);
		
		foreach ( Cubix_I18n::getLangs(true) as $lng ) {
			$data['additional_service_' . $lng] = $validator->urlCleaner($data['additional_service_' . $lng], Cubix_Application::getById()->host);
		}
		
		return $data;
	}

	public function servicesAction()
	{
		if ( $this->_posted ) {
			$validator = new Cubix_Validator();

			$this->view->data = $data = $this->_validateServices($validator);

			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];
				return false;
			}
			
			return $this->profile->update($data, 'services');
		}
		elseif ( $this->user ) {
			$data = $this->profile->getServices();

			$this->view->data = $data;
		}
		
	}

	protected function _validateWorkingTimes($validator)
	{
		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'available_24_7' => 'int-nz',
			'night_escort' => 'int-nz',
			'day_index' => 'arr-int',
			'time_from' => 'arr-int',
			'time_from_m' => 'arr-int',
			'time_to' => 'arr-int',
			'time_to_m' => 'arr-int',
			'night_escorts' => 'arr-int'
		));
		$data = $form->getData();

		$_data = array('available_24_7' => $data['available_24_7'], 'night_escort' => $data['night_escort'],'times' => array(),'vac_date_from' => null,'vac_date_to' => null);

		for ( $i = 1; $i <= 7; $i++ ) {
			if ( isset($data['day_index'][$i]) && isset($data['time_from'][$i]) && isset($data['time_from_m']) && isset($data['time_to'][$i]) && isset($data['time_to_m'][$i]) ) {
				$night_escort =  in_array($i,$data['night_escorts'])?1:0;
				$_data['times'][] = array(
					'day_index' => $i,
					'time_from' => $data['time_from'][$i],
					'time_from_m' => $data['time_from_m'][$i],
					'time_to' => $data['time_to'][$i],
					'time_to_m' => $data['time_to_m'][$i],
					'night_escorts'=>$night_escort
				);
			}
		}

		return $_data;
	}

	public function workingTimesAction()
	{
		if ( $this->_posted ) {
			$validator = new Cubix_Validator();

			$this->view->data = $data = $this->_validateWorkingTimes($validator);
			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];
				return false;
			}

			return $this->profile->update($data, 'working-times');
		}
		elseif ( $this->user )  {
			$data = $this->profile->getWorkingTimes();

			$this->view->data = $data;
		}
	}

	protected function _validatePrices($validator)
	{
		$rates = $this->_getParam('rates');

		if ( ! is_array($rates) ) {
			$rates = array();
		}

		$data = array('rates' => array());

		//$currencies = array_keys($this->defines['currencies']);
		$currencies = array_keys(Model_Currencies::getAllAssoc());
		$units = array_keys($this->defines['time_unit_options']);

		foreach ( $rates as $availability => $_rates ) {
			if ( $availability == 'incall' ) { $availability = 1; $types = array('additional-hour', 'overnight', 'dinner-date', 'weekend'); }
			elseif ( $availability == 'outcall' ) { $availability = 2; $types = array('additional-hour', 'overnight', 'dinner-date', 'weekend'); }
			else continue;

			foreach ( $_rates as $rate ) {
				if ( get_magic_quotes_gpc() ) $rate = stripslashes($rate);
				$rate = @json_decode($rate);

				if ( is_object($rate) ) $rate = (array) $rate;
				if ( $rate === false || ! is_array($rate) ) {
					continue;
				}

				// Case when rate is standart and some data are not valid
				if ( ! isset($rate['type']) && ( ! isset($rate['time']) || ! isset($rate['unit']) || ! isset($rate['price']) || ! isset($rate['currency']) ) ) {
					continue;
				}
				// Case when rate is a type of custom and some data are not valid
				elseif ( isset($rate['type']) && ( ! isset($rate['price']) || ! isset($rate['currency']) || ! in_array($rate['type'], $types) ) ) {
					continue;
				}

				// If price is invalid
				$price = intval($rate['price']);
				if ( $price <= 0 ) continue;

				// If currency is invalid
				$currency = intval($rate['currency']);
				if ( ! in_array($currency, $currencies) ) {
					continue;
				}

				// If rate is custom validate data add only type, price and currency fields
				if ( ! isset($rate['type']) ) {
					$time = intval($rate['time']);
					if ( $time <= 0 ) continue;

					$unit = intval($rate['unit']);
					if ( ! in_array($unit, $units) ) continue;

					$data['rates'][] = array('availability' => $availability, 'time' => $time, 'time_unit' => $unit, 'price' => $price, 'currency_id' => $currency);
				}
				// Otherwize add also time and time unit
				else {
					$data['rates'][] = array('availability' => $availability, 'type' => $rate['type'], 'price' => $price, 'currency_id' => $currency);
				}
			}
		}
		
		return $data;
	}


	public function	pricesAction()
	{
		$this->_request->setParam('no_tidy', true);
		
		if ( $this->_posted ) {
			$validator = new Cubix_Validator();

			$data = $this->_validatePrices($validator);
			$this->view->data = array('rates' => $this->profile->reconstructRates($data['rates']));
			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];
				return false;
			}
			
			return $this->profile->update($data, 'prices');
		}
		elseif ( $this->user ) {
			$data = $this->profile->getPrices();
		} else {
			$data['rates'] = array('incall' => array(), 'outcall' => array());
		}
		
		$this->view->data = $data;
	}

	public function _validateContactInfo($validator)
	{
		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			//'phone_prefix' => '',
			'disable_phone_prefix' => 'int',
			'phone_number' => '',
			/*'phone_number_alt' => '',*/
			'phone_instr' => 'int-nz',
			'phone_instr_no_withheld' => 'int-nz',
			'phone_instr_other' => 'notags|special',
			'email' => '',
			'website' => '',
			'club_name' => 'notags|special',
			'street' => 'notags|special',
			'street_no' => 'notags|special',
			'display_address' => 'int',
			'fake_city_id' => 'int',
			'fake_zip' => ''
		));
		$data = $form->getData();
		$data['display_address'] = (bool) $data['display_address'];
        $data['contact_phone_parsed'] = null;
		/*if ( ! strlen($data['phone_prefix']) ) {
			$validator->setError('phone_prefix', Cubix_I18n::translate('sys_error_country_calling_code_required'));
		}*/
		if ( ! strlen($data['phone_number']) ) {
			$validator->setError('phone_number', Cubix_I18n::translate('sys_error_phone_required'));
		}
		elseif(preg_match("/^(\+|00)/", trim($data['phone_number'])) ) {
				$validator->setError('phone_number', Cubix_I18n::translate('sys_error_enter_phone_without_country_code'));
			}
		else if (! preg_match("/^[0-9\s\+\-\(\)]+$/i", $data['phone_number'])) {
			$validator->setError('phone_number', Cubix_I18n::translate('sys_error_invalid_phone_number'));
		}

			//list($country_id,$phone_prefix,$ndd_prefix) = explode('-',$data['phone_prefix']);
				//unset($data['phone_prefix']);
				
				//Swizerland data 		
				$country_id = 1;$phone_prefix = 41; $ndd_prefix = 0;
				$data['phone_country_id'] = intval($country_id);
/* Added Grigor */
		$phone = $this->_parsePhoneNumber($data['phone_number']);
		$data['contact_phone_parsed'] = preg_replace('/^'.$ndd_prefix.'/', '', $phone);
        $data['contact_phone_parsed'] = '00'.intval($phone_prefix).$data['contact_phone_parsed'];

		$data['phone_exists'] = $data['contact_phone_parsed'];
		
		
		$agency_id = $this->user->isAgency() ? $this->agency->id : null;


		$client = Cubix_Api_XmlRpc_Client::getInstance();
        if($this->escort->id){
            //$phone = $this->_parsePhoneNumber($data['phone_number']);


			//$client = Cubix_Api_XmlRpc_Client::getInstance();
			$results = $client->call('Escorts.existsByPhone', array($data['contact_phone_parsed'], $this->escort->id, $agency_id));
	       $resCount = is_array($results) ? count($results) : 0;
			if(!$this->user->isAgency() AND $resCount > 0 ){
                   $validator->setError('phone_number', Cubix_I18n::translate('sys_error_phone_already_exists'));
            }
        }
/* Added Grigor */


//        $validator->setError('phone_number', 'You cannot use the same phone number');
//        exit;
//        if ( $phone == $data['phone_number'] ) {
//            $validator->setError('phone_number_alt', 'You cannot use the same phone number');
//        }
//        else {
//            $data['phone_number_alt'] = $phone;
//        }

		/*if ( strlen($data['phone_number_alt']) ) {
			$phone = $this->_parsePhoneNumber($data['phone_number_alt'], $this->escort->id);
			if ( false === $phone ) {
				$validator->setError('phone_number_alt', 'Invalid phone number');
			}
			elseif ( $phone == $data['phone_number'] ) {
				$validator->setError('phone_number_alt', 'You cannot use the same phone number');
			}
			else {
				$data['phone_number_alt'] = $phone;
			}
		}*/

		if ( ! in_array($data['phone_instr'], array_keys($this->defines['phone_instructions'])) ) {
			$data['phone_instr'] = null;
		}

		if ( ! is_null($data['phone_instr_no_withheld']) ) {
			$data['phone_instr_no_withheld'] = 1;
		}

		if ( ! is_null($data['phone_instr_other']) && $this->_hasPhoneNumber($data['phone_instr_other']) ) {
			$validator->setError('phone_instr_other', Cubix_I18n::translate('sys_error_invalid_other'));
		}
		
		if ( strlen($data['email']) && ! $this->_validateEmailAddress($data['email']) ) {
			$validator->setError('email', Cubix_I18n::translate('sys_error_invalid_email_address'));
		}

		if ( strlen($data['website']) && ! $this->_validateWebsiteUrl($data['website']) ) {
			$validator->setError('website', Cubix_I18n::translate('sys_error_invalid_url'));
		}

		$contact_data = $client->call('Escorts.getWebPhone', array($this->escort->id));

		if ( strlen($data['website'])){
			$old_website = $contact_data['website'];
			if ($old_website != $data['website']){
				$data['website_changed'] = 1;
			}
		}
		if ( strlen($data['contact_phone_parsed'])){
			$old_phone = $contact_data['contact_phone_parsed'];
			if ($old_phone != $data['contact_phone_parsed']){
				$data['phone_changed'] = 1;
				$data['old_phone'] = $old_phone;
			}
		}
		
		$data['phone_instr_other'] = $validator->urlCleaner($data['phone_instr_other'], Cubix_Application::getById()->host);
		$data['club_name'] = $validator->urlCleaner($data['club_name'], Cubix_Application::getById()->host);
		$data['street'] = $validator->urlCleaner($data['street'], Cubix_Application::getById()->host);
		$data['street_no'] = $validator->urlCleaner($data['street_no'], Cubix_Application::getById()->host);
		
		return $data;
	}

	public function contactInfoAction()
	{
		$countyModel = new Model_Countries();
		$this->view->countries = $countyModel->getPhoneCountries();
		if ( $this->_posted ) {
			
			$validator = new Cubix_Validator();
			
			$data = $this->view->data = $this->_validateContactInfo($validator);
            $this->view->phone_prefix_id = $data[phone_country_id];
			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];
				return false;
			}
			
			$this->update_data['contact-info'] = $data;
			if ( ! $this->simple_mode )
				return $this->profile->update($data, 'contact-info');
			}
			elseif ( $this->user ) {
			$data = $this->profile->getContactInfo();
			//$phone_prfixes = $countyModel->getPhonePrefixs();
			$this->view->phone_prefix_id = $data[phone_country_id];
			/*if($data[phone_number]){
				if(preg_match('/^(\+|00)/',trim($data[phone_number])))
				{
					$phone_prefix_id = NULL;
					$phone_number = preg_replace('/^(\+|00)/', '',trim($data[phone_number]));
					foreach($phone_prfixes as $prefix)
					{
						if(preg_match('/^('.$prefix->phone_prefix.')/',$phone_number))
						{
							$phone_prefix_id = $prefix->id;
							$data[phone_number] = preg_replace('/^('.$prefix->phone_prefix.')/', '',$phone_number);
							BREAK;
						}

					}
					$this->view->phone_prefix_id = $phone_prefix_id;
				}
			}*/
			$this->view->data = $data;
		}
	}

	public function gallerySimpleAction()
	{		
		$escort_id = $this->view->escort_id;
		
		$set_photo = false;
		$config = Zend_Registry::get('images_config');
		$new_photos = array();
		$upload_errors = array();

		foreach ( $_FILES as $i => $file )
		{
			try {

				if ( ! isset($file['name']) || ! strlen($file['name']) ) {
					continue;
				}
				else {
					$set_photo = true;
				}

				$img_ext = strtolower(@end(explode('.', $file[name])));
				if ( ! in_array($img_ext , $config['allowedExts']) ){
					throw new Exception(Cubix_I18n::translate('sys_error_upload_img_allowed_extensions', array('extensions' => implode(', ', $config['allowedExts']))), Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
				}

				// Save on remote storage
				$images = new Cubix_Images();
				$image = $images->save($file['tmp_name'], $escort_id, Cubix_Application::getId(), strtolower(@end(explode('.', $file['name']))));

				$image = new Cubix_Images_Entry($image);
				$image->setSize('sthumb');
				$image->setCatalogId($escort_id);
				$image_url = $images->getUrl($image);


				$photo_arr = array(
					'escort_id' => $escort_id,
					'hash' => $image->getHash(),
					'ext' => $image->getExt(),
					'type' => ESCORT_PHOTO_TYPE_HARD,
					'is_approved'	=> 1,
					'creation_date' => date('Y-m-d H:i:s', time())
				);

				$photo = new Model_Escort_PhotoItem($photo_arr);

				$model = new Model_Escort_Photos();
				$photo = $model->save($photo);

				$new_photos[] = $photo;
			} catch ( Exception $e ) {
				$upload_errors[$i] = $file['name'] . ' (' . $e->getMessage() . ')';
			}
					
		}
		
		//$validator = new Cubix_Validator();
		
		if ( ! $set_photo ) {
			
			//$validator->setError('slogan', Cubix_I18n::translate('sys_error_slogan_text_must_be'));
			
			$this->view->uploadError = Cubix_I18n::translate('sys_error_select_photo');
		}
		
		/*if ( ! $set_photo || count($upload_errors) || count($this->view->errors) ) {
			//IF ERROR OCCURED DURING IMAGE UPLOAD REMOVE ESCORT
			$client = new Cubix_Api_XmlRpc_Client();
			$client->call('Escorts.delete', array($escort_id));
			if ( $this->view->new_user_id && $this->view->user_type == 'agency' ) {
				$client->call('Users.delete', array($this->view->new_user_id));
				$client->call('Agencies.delete', array($this->view->agency_id));
			}
		}*/ /*elseif ( $this->_getParam('show_advertise_block') ) {
			$this->_setParam('escort_id', $escort_id);
			if ( $this->user ) {
				$this->advertiseNowAction();
			} else {
				$this->fastAdvertiseNowAction();
			}
			//$this->_redirect($this->view->getLink('private-v2-advertise-now',array('escort_id' => $escort_id)));
		} else {
			$this->_redirect($this->view->getLink('private-v2'));
		}*/


		$this->view->newPhotos = $new_photos;
		$this->view->uploadErrors = $upload_errors;
	}
	
	public function galleryAction()
	{
//		$this->view->layout()->setLayout('private-v2');

		$escort_id = $this->view->escort_id;

		if ( $this->_getParam('escort') ) {
			$escort_id = $this->_getParam('escort');
		}
		
		$this->view->user = $this->user;

		$client = new Cubix_Api_XmlRpc_Client();
		$this->view->is_message = false;
		if ( $this->_getParam('show_success') ){
			$this->view->is_message = true;
		}

		if ( $this->user->isEscort() ) {
			
			$escort = $this->user->getEscort();
			$escort_id = $escort->id;

//			if ( !intval($this->_getParam('escort')) ){
//				$this->view->is_edit = false;
//			}

		}
		else {

			$escort_id = intval($this->_getParam('escort'));
			
			if ( 1 > $escort_id ) die;

			if ( ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
				die;
			}

			$model = new Model_Escorts();
			$this->escort = $escort = $model->getById($escort_id);
		}

		

		$this->view->escort = $escort;
		$photos = $this->_loadPhotos();

		$photos_ids = array();
		foreach ( $photos as $photo ) {
			$photo_ids[] = intval($photo['id']);
		}

		$action = $this->_getParam('a');
		
		$photos_min_count = Cubix_Application::getById(Cubix_Application::getId())->min_photos;

		$this->view->is_private = $is_private = (bool) intval($this->_getParam('is_private'));

		

		if ( 'set-main' == $action || ! is_null($this->_getParam('set_main')) ) {
			$ids = $this->_getParam('photo_id');
			if ( ! is_array($ids) || ! count($ids) ) {
				return $this->view->actionError = Cubix_I18n::translate('sys_error_select_at_least_on_photo');
			}

			$photo_id = reset($ids);
			if ( ! in_array($photo_id, $photo_ids) ) {
				return $this->view->actionError = Cubix_I18n::translate('sys_error_invalid_id_photo');
			}

			$photo = new Model_Escort_PhotoItem(array('id' => $photo_id));
			$result = $photo->setMain();
			
			if ( is_array($result) ) return $this->view->actionError = Cubix_I18n::translate('sys_error_photo_is_not_approved');

			$this->_loadPhotos();
		}
		elseif ( 'delete' == $action || ! is_null($this->_getParam('delete')) ) {
			$ids = $this->_getParam('photo_id');

			if ( ! is_array($ids) || ! count($ids) ) {
				return $this->view->actionError = Cubix_I18n::translate('sys_error_select_at_least_on_photo');
			}

			foreach ( $ids as $id ) {
				if ( ! in_array($id, $photo_ids) ) {
					return $this->view->actionError = Cubix_I18n::translate('sys_error_invalid_id_photo');
				}
			}

			$photo = new Model_Escort_Photos();
			$result = $photo->remove($ids);

			$this->_loadPhotos();
		}
		elseif ( 'upload' == $action || ! is_null($this->_getParam('upload')) ) {
				$set_photo = false;
				$config = Zend_Registry::get('images_config');
				$new_photos = array();
				$upload_errors = array();
				$model = new Model_Escort_Photos();

				foreach ( $_FILES as $i => $file )
				{
					try {

						if ( $is_private ){
							if ( strpos($i, 'public_') === 0 ){
								continue;
							}
						}else{
							if ( strpos($i, 'private_') === 0 ){
								continue;
							}
						}

						if ( ! isset($file['name']) || ! strlen($file['name']) ) {
							continue;
						}
						else {
							$set_photo = true;
						}

						$img_ext = strtolower(@end(explode('.', $file[name])));
						if (!in_array( $img_ext , $config['allowedExts'])){
							throw new Exception(Cubix_I18n::translate('sys_error_upload_img_allowed_extensions', array('extensions' => implode(', ', $config['allowedExts']))), Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
						}
						
						$photo_count = $model->getEscortPhotoCount($escort_id);
						if ( $photo_count >= Model_Escort_Photos::MAX_PHOTOS_COUNT ){
							throw new Exception(Cubix_I18n::translate('sys_error_upload_img_count_too_much', array('img_count' => Model_Escort_Photos::MAX_PHOTOS_COUNT)), Cubix_Images::ERROR_IMAGE_COUNT_LIMIT_OVER);
						}
						
						// Save on remote storage
						$images = new Cubix_Images();
						$image = $images->save($file['tmp_name'], $escort->id, $escort->application_id, strtolower(@end(explode('.', $file['name']))));

						$image = new Cubix_Images_Entry($image);
						$image->setSize('sthumb');
						$image->setCatalogId($escort->id);
						$image_url = $images->getUrl($image);

						$image_size = getimagesize($file['tmp_name']);
						$is_portrait = 0;
						if ( $image_size ) {
							if ( $image_size[0] < $image_size[1] ) {
								$is_portrait = 1;
							}
						}
						
						$photo_arr = array(
							'escort_id' => $escort->id,
							'hash' => $image->getHash(),
							'ext' => $image->getExt(),
							'type' => $is_private ? ESCORT_PHOTO_TYPE_PRIVATE : ESCORT_PHOTO_TYPE_HARD,
							'is_portrait' => $is_portrait,
							'width' => $image_size[0],
							'height' => $image_size[1],
							'creation_date' => date('Y-m-d H:i:s', time())
						);

						if ( $client->call('Escorts.isPhotoAutoApproval', array($escort_id)) ) {
							$photo_arr['is_approved'] = 1;
						}
						else if ( $client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_NO_PROFILE))) ) {
							$photo_arr['is_approved'] = 1;
						}

						$photo = new Model_Escort_PhotoItem($photo_arr);

						
						$photo = $model->save($photo);

						$new_photos[] = $photo;
					} catch ( Exception $e ) {
						$upload_errors[$i] = $file['name'] . ' (' . $e->getMessage() . ')';
					}
					
			}

			if ( ! $set_photo ) {
				$this->view->uploadError = Cubix_I18n::translate('sys_error_select_photo');
			}

			$this->view->newPhotos = $new_photos;
			$this->view->uploadErrors = $upload_errors;
		}
		elseif ( 'set-adj' == $action ) {
			$photo_id = intval($this->_getParam('photo_id'));

			if ( ! in_array($photo_id, $photo_ids) ) {
				die(json_encode(array('error' => 'An error occured')));
			}

			$photo = new Model_Escort_PhotoItem(array(
				'id' => $photo_id
			));

			try {
				$hash = $photo->getHash();
				$result = array(
					'x' => intval($this->_getParam('x')),
					'y' => intval($this->_getParam('y')),
					'px' => floatval($this->_getParam('px')),
					'py' => floatval($this->_getParam('py'))
				);
				$photo->setCropArgs($result);

				// Crop All images
				$size_map = array(
					'backend_thumb' => array('width' => 150, 'height' => 205),
					'medium' => array('width' => 225, 'height' => 300),
					'thumb' => array('width' => 150, 'height' => 200),
					'nlthumb' => array('width' => 120, 'height' => 160),
					'sthumb' => array('width' => 76, 'height' => 103),
					'lvthumb' => array('width' => 75, 'height' => 100),
					'agency_p100' => array('width' => 90, 'height' => 120),
					't100p' => array('width' => 117, 'height' => 97)
				);
				$conf = Zend_Registry::get('images_config');

				get_headers($conf['remote']['url'] . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash);
				// echo $conf['remote']['url'] . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash;

				$catalog = $escort_id;
				$a = array();
				if ( is_numeric($catalog) ) {
					$parts = array();

					if ( strlen($catalog) > 2 ) {
						$parts[] = substr($catalog, 0, 2);
						$parts[] = substr($catalog, 2);
					}
					else {
						$parts[] = '_';
						$parts[] = $catalog;
					}
				}
				else if ( preg_match('#^([0-9]+)/(.+)$#', $catalog, $a) ) {
					array_shift($a);
					$catalog = $a[0];

					$parts = array();

					if ( strlen($catalog) > 2 ) {
						$parts[] = substr($catalog, 0, 2);
						$parts[] = substr($catalog, 2);
					}
					else {
						$parts[] = '_';
						$parts[] = $catalog;
					}

					$parts[] = $a[1];
				}

				$catalog = implode('/', $parts);

				foreach($size_map as $size => $sm) {
					get_headers($conf['remote']['url'] . "/" . Cubix_Application::getById()->host . "/" . $catalog . "/" . $hash . "_" . $size . ".jpg?args=" . $result['x'] . ":" . $result['y']);
				}
			}
			catch ( Exception $e ) {
				die(json_encode(array('error' => 'An error occured')));
			}

			die(json_encode(array('success' => true)));
		}
		elseif ( ! is_null($this->_getParam('make_private')) || ! is_null($this->_getParam('make_public')) ) {

			if ( ! is_null($this->_getParam('make_private')) ) {
				$type = 'private';
			} elseif ( ! is_null($this->_getParam('make_public')) ) {
				$type = 'public';
			} else {
				die;
			}

			$ids = $this->_getParam('photo_id');

			if ( ! is_array($ids) || ! count($ids) ) {
				return $this->view->actionError = Cubix_I18n::translate('sys_error_select_at_least_on_photo');
			}

			foreach ( $ids as $id ) {
				if ( ! in_array($id, $photo_ids) ) {
					return $this->view->actionError = Cubix_I18n::translate('sys_error_invalid_id_photo');
				}
			}

			foreach ( $ids as $id ) {
				$photo = new Model_Escort_PhotoItem(array('id' => $id));
				$photo->make($type == 'private' ? ESCORT_PHOTO_TYPE_PRIVATE : ESCORT_PHOTO_TYPE_HARD);
			}

			$this->_loadPhotos();
		}
		elseif ( 'sort' == $action ) {
			$ids = $this->_getParam('photo_id');

			if ( ! is_array($ids) || ! count($ids) ) {
				die(Cubix_I18n::translate('sys_error_select_at_least_on_photo'));
			}

			foreach ( $ids as $id ) {
				if ( ! in_array($id, $photo_ids) ) {
					die(Cubix_I18n::translate('sys_error_invalid_id_photo'));
				}
			}

			$model = new Model_Escort_Photos();
			$model->reorder($ids);

			die;
		}elseif ( $this->_posted && ! $this->_getParam('simple') ) {
			$data = array();
			$this->_redirect($this->view->getLink('private'));
			return;
//			return $this->profile->update($data, 'gallery');
		}
	}
	
	public function galleryCreateAction()
	{
		$this->view->layout()->disableLayout();
	
		if ( ! is_array($this->adv_session->photos) ) {
			$this->adv_session->photos = array();
		}
		
		$escort_id = $this->view->escort_id = '0000';

		$client = new Cubix_Api_XmlRpc_Client();
		
		$this->view->is_message = false;
		if ( $this->_getParam('show_success') ){
			$this->view->is_message = true;
		}			
		
		$photos = $this->view->photos = $this->_loadPhotosFromSession();

		$photos_ids = array();
		foreach ( $photos as $photo ) {
			$photo_ids[] = intval($photo['id']);
		}

		$action = $this->_getParam('a');
		
		$photos_min_count = Cubix_Application::getById(Cubix_Application::getId())->min_photos;

		$this->view->is_private = $is_private = (bool) intval($this->_getParam('is_private'));

		if ( 'set-main' == $action || ! is_null($this->_getParam('set_main')) ) {
			$ids = $this->_getParam('photo_id');
			if ( ! is_array($ids) || ! count($ids) ) {
				return $this->view->uploadError = Cubix_I18n::translate('sys_error_select_at_least_on_photo');
			}
			
			$photo_id = reset($ids);
			if ( ! in_array($photo_id, $photo_ids) ) {
				return $this->view->uploadError = Cubix_I18n::translate('sys_error_invalid_id_photo');
			}

			$this->_setMainPhotoInSession($photo_id);

			$this->view->photos = $this->_loadPhotosFromSession();
		}
		elseif ( 'delete' == $action || ! is_null($this->_getParam('delete')) ) {
			$ids = $this->_getParam('photo_id');

			if ( ! is_array($ids) || ! count($ids) ) {
				return $this->view->uploadError = Cubix_I18n::translate('sys_error_select_at_least_on_photo');
			}

			foreach ( $ids as $id ) {
				$this->_removePhotoFromSession($id);
			}		

			$this->view->photos = $this->_loadPhotosFromSession();
		}
		elseif ( 'upload' == $action || ! is_null($this->_getParam('upload')) ) {
				$set_photo = false;
				$config = Zend_Registry::get('images_config');
				$new_photos = array();
				$upload_errors = array();

				unset($this->adv_session->photo_upload_errors);

				foreach ( $_FILES as $i => $file )
				{
					try {

						if ( $is_private ){
							if ( strpos($i, 'public_') === 0 ){
								continue;
							}
						}else{
							if ( strpos($i, 'private_') === 0 ){
								continue;
							}
						}

						if ( ! isset($file['name']) || ! strlen($file['name']) ) {
							continue;
						}
						else {
							$set_photo = true;
						}

						$img_ext = strtolower(@end(explode('.', $file[name])));
						if (!in_array( $img_ext , $config['allowedExts'])){
							throw new Exception(Cubix_I18n::translate('sys_error_upload_img_allowed_extensions', array('extensions' => implode(', ', $config['allowedExts']))), Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
						}

						// Save on remote storage
						$images = new Cubix_Images();
						$image = $images->save($file['tmp_name'], $escort_id, Cubix_Application::getId(), strtolower(@end(explode('.', $file['name']))));

						$image = new Cubix_Images_Entry($image);
						$image->setSize('sthumb');
						$image->setCatalogId($escort_id);
						$image_url = $images->getUrl($image);
						
						$image_size = getimagesize($file['tmp_name']);
						$is_portrait = 0;
						if ( $image_size ) {
							if ( $image_size[0] < $image_size[1] ) {
								$is_portrait = 1;
							}
						}
						
						$hash = $image->getHash();
						$photo_arr = array(
							'id' => $hash,
							'escort_id' => $escort_id,
							'hash' => $hash,
							'ext' => $image->getExt(),
							'type' => $is_private ? ESCORT_PHOTO_TYPE_PRIVATE : ESCORT_PHOTO_TYPE_HARD,
							'is_approved' => 1,
							'is_main' => ( $this->_hasMainPhotoInSession() || ($is_private == ESCORT_PHOTO_TYPE_PRIVATE) ) ? 0 : 1,
							'ordering' => 0,
							'args' => null,
							'is_portrait' => $is_portrait,
							'width' => $image_size[0],
							'height' => $image_size[1],
							'creation_date' => date('Y-m-d H:i:s', time())
						);

						$photo = new Model_Escort_PhotoItem($photo_arr);
						
						$this->adv_session->photos[$hash] = $photo;

						//$new_photos[] = $photo;
						
					} catch ( Exception $e ) {
						$upload_errors[$i] = $file['name'] . ' (' . $e->getMessage() . ')';
					}
					
			}

			if ( ! $set_photo ) {
				//$this->view->uploadError = Cubix_I18n::translate('sys_error_select_photo');
			}

			//$this->view->newPhotos = $new_photos;
			$this->view->uploadErrors = $upload_errors;
			$this->adv_session->photo_upload_errors = $upload_errors;
			$this->view->photos = $this->_loadPhotosFromSession();
		}
		elseif ( 'set-adj' == $action ) {
			$escort_id = '0000';
			$photo_id = $this->_getParam('photo_id');

			try {
				$hash = $photo_id;
				$result = array(
					'x' => intval($this->_getParam('x')),
					'y' => intval($this->_getParam('y')),
					'px' => floatval($this->_getParam('px')),
					'py' => floatval($this->_getParam('py'))
				);
				
				$crop_args = serialize($result);

				$this->_setCropArgsInSession($photo_id, $crop_args);
				
				// Crop All images
				$size_map = array(
					'backend_thumb' => array('width' => 150, 'height' => 205),
					'medium' => array('width' => 225, 'height' => 300),
					'thumb' => array('width' => 150, 'height' => 200),
					'nlthumb' => array('width' => 120, 'height' => 160),
					'sthumb' => array('width' => 76, 'height' => 103),
					'lvthumb' => array('width' => 75, 'height' => 100),
					'agency_p100' => array('width' => 90, 'height' => 120),
					't100p' => array('width' => 117, 'height' => 97)
				);
				$conf = Zend_Registry::get('images_config');

				get_headers($conf['remote']['url'] . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash);
				//echo $conf['remote']['url'] . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash;

				$catalog = $escort_id;
				$a = array();
				if ( is_numeric($catalog) ) {
					$parts = array();

					if ( strlen($catalog) > 2 ) {
						$parts[] = substr($catalog, 0, 2);
						$parts[] = substr($catalog, 2);
					}
					else {
						$parts[] = '_';
						$parts[] = $catalog;
					}
				}
				else if ( preg_match('#^([0-9]+)/(.+)$#', $catalog, $a) ) {
					array_shift($a);
					$catalog = $a[0];

					$parts = array();

					if ( strlen($catalog) > 2 ) {
						$parts[] = substr($catalog, 0, 2);
						$parts[] = substr($catalog, 2);
					}
					else {
						$parts[] = '_';
						$parts[] = $catalog;
					}

					$parts[] = $a[1];
				}

				$catalog = implode('/', $parts);

				foreach($size_map as $size => $sm) {
					get_headers($conf['remote']['url'] . "/" . Cubix_Application::getById()->host . "/" . $catalog . "/" . $hash . "_" . $size . ".jpg?args=" . $result['x'] . ":" . $result['y']);
				}
			}
			catch ( Exception $e ) {
				die(json_encode(array('error' => 'An error occured')));
			}

			die(json_encode(array('success' => true)));
		}
		elseif ( ! is_null($this->_getParam('make_private')) || ! is_null($this->_getParam('make_public')) ) {

			if ( ! is_null($this->_getParam('make_private')) ) {
				$type = 'private';
			} elseif ( ! is_null($this->_getParam('make_public')) ) {
				$type = 'public';
			} else {
				die;
			}

			$ids = $this->_getParam('photo_id');

			if ( ! is_array($ids) || ! count($ids) ) {
				return $this->view->uploadError = Cubix_I18n::translate('sys_error_select_at_least_on_photo');
			}

			foreach ( $ids as $id ) {
				$this->_setPhotoTypeInSession($id, $type == 'private' ? ESCORT_PHOTO_TYPE_PRIVATE : ESCORT_PHOTO_TYPE_HARD);
			}

			$this->view->photos = $this->_loadPhotosFromSession();
		}
		elseif ( 'sort' == $action ) {
			$ids = $this->_getParam('photo_id');

			$this->_reorderPhotosInSession($ids);

			die;
		}
		elseif ( $this->_posted && ! $this->_getParam('simple') ) {
			$data = array();
			$this->_redirect($this->view->getLink('private'));
			return;
		}
	}

	private function _setCropArgsInSession($photo_id, $args)
	{
		if ( isset($this->adv_session->photos[$photo_id]) ) {
			$this->adv_session->photos[$photo_id]->args = $args;
		}
	}
	
	private function _reorderPhotosInSession($photo_ids)
	{
		if ( count($photo_ids) ) {
			foreach( $photo_ids as $i => $photo_id ) {
				if ( isset($this->adv_session->photos[$photo_id]) ) {
					$this->adv_session->photos[$photo_id]->ordering = $i + 1;
				}
			}
		}
	}
	
	private function _removePhotoFromSession($photo_id)
	{
		if ( isset($this->adv_session->photos[$photo_id]) ) {
			$set_random = false;
			if ( $this->adv_session->photos[$photo_id]->is_main ) {
				$set_random = true;
			}
			
			unset($this->adv_session->photos[$photo_id]);
			
			if ( $set_random ) {
				$this->_setMainPhotoInSession();
			}
		}
	}
	
	private function _setPhotoTypeInSession($photo_id, $type)
	{
		if ( isset($this->adv_session->photos) && count($this->adv_session->photos) ) {
			$set_random = false;
			if ( $type == ESCORT_PHOTO_TYPE_PRIVATE && $this->adv_session->photos[$photo_id]->is_main ) {
				$set_random = true;
			}
			else if ( ! $this->_hasMainPhotoInSession() ) {
				$set_random = true;
			}
			
			$this->adv_session->photos[$photo_id]->type = $type;
			$this->adv_session->photos[$photo_id]->is_main = 0;
			
			if ( $set_random ) {
				$this->_setMainPhotoInSession();
			}
		}
	}
	
	private function _setMainPhotoInSession($photo_id = null)
	{
		if ( isset($this->adv_session->photos) && count($this->adv_session->photos) ) {
			if ( $photo_id ) {
				foreach ( $this->adv_session->photos as $id => $photo ) {
					$this->adv_session->photos[$id]->is_main = 0;
				}

				if ( $this->adv_session->photos[$photo_id]->type == ESCORT_PHOTO_TYPE_HARD )
					$this->adv_session->photos[$photo_id]->is_main = 1;
				
			} else {
				foreach ( $this->adv_session->photos as $id => $photo ) {
					if ( $this->adv_session->photos[$id]->type == ESCORT_PHOTO_TYPE_HARD ) {
						$this->adv_session->photos[$id]->is_main = 1;
						break;
					}
				}
			}
		}
	}
	
	private function _hasMainPhotoInSession()
	{
		$has_main = false;
		if ( isset($this->adv_session->photos) && count($this->adv_session->photos) ) {
			 foreach ( $this->adv_session->photos as $photo ) {
				 if ( $photo['type'] == ESCORT_PHOTO_TYPE_HARD && $photo['is_main'] ) {
					 $has_main = true;
				 }
			 }
		}
		
		return $has_main;
	}
	
	private function _loadPhotosFromSession()
	{		
		//$this->adv_session = new Zend_Session_Namespace('adv_sess');
		if ( isset($this->adv_session->photos) && count($this->adv_session->photos) ) {
			$this->view->newPhotos = array();
			return $this->adv_session->photos;
		}
		
		return array();
	}
	
	private function _loadPhotos()
	{		
		//$this->adv_session = new Zend_Session_Namespace('adv_sess');
		if ( isset($this->adv_session->escort_id) ) {
			$this->escort = new Model_EscortItem(array('id' => $this->adv_session->escort_id));
		}
		
		$public_photos = Cubix_Api::getInstance()->call('getEscortPhotosList', array($this->escort->id, false));
		$nil = null;
		$private_photos = Cubix_Api::getInstance()->call('getEscortPhotosList', array($this->escort->id, true));

		return $this->view->photos = array_merge($public_photos, $private_photos);
	}



	protected function _parsePhoneNumber($phone)
	{
		$phone = preg_replace('/[^0-9]+/', '', $phone);

		if ( ! strlen($phone) || ! is_numeric($phone) ) {
			return false;
		}

		/*if ( '00' != substr($phone, 0, 2) ) {
			if ( ! is_null($escort_id) ) {
				$iso = $this->profile->country_id;
				if ( $iso && isset($this->defines['dial_codes'][$iso]) ) {
					$phone = '00' . $this->defines['dial_codes'][$iso] . $phone;
				}
			}
		}*/

		return $phone;
	}

	protected function _hasPhoneNumber($string)
	{
		$string = preg_replace('/[^0-9]+/', '', $string);

		if ( strlen($string) > 8 ) {
			return true;
		}

		return false;
	}

	protected function _validateEmailAddress($email)
	{
		return Cubix_Validator::validateEmailAddress($email);
	}

	protected function _validateWebsiteUrl($url)
	{
		//return (bool) preg_match('/^((http|https):\/\/)?(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)(:(\d+))?\/?/i', $url);
		if (strtolower(substr($url, 0, 7)) != 'http://' && strtolower(substr($url, 0, 8)) != 'https://' && strtolower(substr($url, 0, 4)) != 'www.')
			return false;
		else
			return true;
	}

	public function finishAction()
	{
		$data = array();
		
		if ( isset($this->view->new_user_id) && $this->view->new_user_id ) {
			if ( $this->view->user_type == 'agency' ) {
				$data['agency_id'] = $this->view->agency_id;
			}
			$data['user_id'] = $this->view->new_user_id;
			
		} else {
			
			if ( $this->user->isAgency() ) {
				$data['agency_id'] = $this->agency->getId();
			}

			$data['user_id'] = $this->user->getId();
		}
		
		// Send the data from session to API, a new revision of the profile will be created!
		$res = $this->profile->flush($data);

		if ( ! $res ) {
			$this->_redirect($this->view->getLink('private-v2'));
		}
		$this->view->escort_id = $res;
	}

	public function ajaxChangeCityAction()
	{
		$this->view->layout()->disableLayout();


		$city_id = intval($this->_getParam('city_id'));
		$data = $this->profile->getWorkingCities();

		$model = new Cubix_Geography_Countries();
		if ( ! $model->hasCity($data['country_id'], $city_id) ) {
			die('Invalid city id');
		}

		$data['city_id'] = $city_id;
		foreach ( $data['cities'] as $i => $city ) {
			if ( $city_id == $city['city_id'] ) {
				$data['cities'][$i]['city_id'] = $city_id;
			}
		}


		
		$result = $this->profile->update($data, 'working-cities');

		$response = array('status' => 'error');

		if ( $result ) {
			$response['status'] = 'ok';
			$response['city'] = Cubix_Geography_Cities::getTitleById($city_id);
		}

		die(json_encode($response));
	}

	public function ajaxChangeZipAction()
	{
		$zip = $this->_getParam('zip');
		$data = $this->profile->getWorkingCities();

		if ( ! $data['incall_type'] ) {
			die('There is no need for zip code');
		}

		$validator = new Cubix_Validator();

		if ( ! $validator->validateZipCode($zip, true) ) {
			die(json_encode(array('status' => 'error')));
		}

		$data['zip'] = $zip;


		$result = $this->profile->update($data, 'working-cities');

		$response = array('status' => 'error');

		if ( $result ) {
			$response['status'] = 'ok';
			$response['zip'] = $zip;
		}

		die(json_encode($response));
	}
	
	public function advertiseNowAction()
	{
		$this->view->layout()->setLayout('private-v2');
        $this->_helper->viewRenderer->setScriptAction("advertise-now");
		$application_id = Cubix_Application::getId();
		$client = new Cubix_Api_XmlRpc_Client();
		
		$this->adv_session->advertise_now = 1;
		
		if ( $this->adv_session->transfer_id ) {
			$client->call('Billing.deleteOrderByTransferId', array($this->adv_session->transfer_id));
		}
		
		if ( $this->_getParam('escort_id') ) {
			$this->view->escort_id = $this->_getParam('escort_id');
		}
		
		if ( $this->user && ($this->user->user_type != 'escort' && $this->user->user_type != 'agency') ) {
			$this->_redirect($this->view->getLink('private-v2'));
		}
		$this->view->is_agency = false;
		if ( $this->user->isAgency() ) {
			$this->view->is_agency = true;
			$this->view->escorts = $escorts = $this->agency->getEscortsWithoutPackages();
		} else {
			$this->view->escort_package = $client->call('Escorts.checkIfHasPackage', array($this->escort->id));
		}
		
		$phone_packages = $client->call('Billing.getPackages', array(1, $application_id));
		$this->view->phone_packages = $phone_packages;
		
		$this->view->create_profile = $this->_getParam('create_profile', 0);
		if ( $this->_request->isPost() ) {
			$escort_gender = null;
			if ( $this->view->is_agency ) {
					foreach($escorts as $escort) {
						if ( $escort['id'] == $this->_getParam('escort_id') ) {
							$escort_gender = $escort['gender'];
						}
					}
				//}
			} else {
				$escort_gender = $this->escort->gender;
				$this->_setParam('escort_id', $this->escort->id);
			}
			

			if ( $client->call('Escorts.checkIfHasPackage', array($this->escort->id)) )//Checking once again if escort does not have active, pending or suspended package
			{
				return;
			}
			
			$user_type = USER_TYPE_SINGLE_GIRL;
			if ( $this->user->user_type != 'escort' ) {
				$user_type = USER_TYPE_AGENCY;
			}
			
			$client->call('Billing.deletePhoneTransfersAndOrders', array($this->_getParam('escort_id')));
						
			$package = $client->call('Billing.gePackagesWithPrice', array($application_id, $user_type, $escort_gender, $this->_getParam('package_id')));
			
			$package_data = array (
				'escort_id' => $this->_getParam('escort_id'),
				'package_id' => $this->_getParam('package_id'),
				'application_id'	=> $application_id,
				'base_price'	=> $package['price'],
				'price'	=> $package['price'],
				'base_period'	=> $package['period'],
				'period'	=> $package['period'],
				'activation_type'	=> $this->_getParam('activation_type'),
				'activation_date' => strtotime($this->_getParam('activation_date')),
				'is_agency'	=> $this->view->is_agency,
				'gender'	=> $escort_gender				
			);
			
			$post_data = array(
				'order_data' => array(
					'order_date'	=> date('Y-m-d H:i:s'),
					'application_id'	=> $application_id,
					'price'	=> $package['price'],
					'user_id'	=> $this->user->id,
					
					'is_free' => true
					//'backend_user_id'	=> $this->user->sales_user_id
				),
				'packages_data'	=> array($package_data)
			);
			
			$response = $client->call('Billing.addPackage', array($post_data));
			
			if ( is_numeric($response) ) {
				$this->_redirect(self::$linkHelper->getLink('private-v2-advertise-success'));
				/*$this->adv_session->transfer_id = $response;
				
				$this->adv_session->activation_type = $this->_getParam('activation_type');
				$this->adv_session->activation_date = $this->_getParam('activation_date');
				
				$this->_redirect($this->getIvrUrl($package['price'], $response, $this->_getParam('escort_id')));*/
			}
		}
	}
	
	public function fastAdvertiseNowAction()
	{
		if ( $this->_request->isPost() ) {
			
			$client = new Cubix_Api_XmlRpc_Client();
			
			if ( $this->adv_session->transfer_id ) {
				$client->call('Billing.deleteOrderByTransferId', array($this->adv_session->transfer_id));
			}
			
			if ( isset($this->adv_session->escort_id) ) {
				if ( $this->adv_session->escort_id != $this->_getParam('escort_id') ) die('He he )');
			}
			
			$application_id = Cubix_Application::getId();
			
			$escort_gender = $this->_getParam('gender');
			
			$is_agency = 0;
			$user_type = USER_TYPE_SINGLE_GIRL;
			if ( $this->_getParam('user_type') != 'escort' ) {
				$user_type = USER_TYPE_AGENCY;
				$is_agency = 1;
			}
			
			$package = $client->call('Billing.gePackagesWithPrice', array($application_id, $user_type, $escort_gender, $this->_getParam('package_id')));
			
			$package_data = array(
				'escort_id' => $this->_getParam('escort_id'),
				'package_id' => $this->_getParam('package_id'),
				'application_id'	=> $application_id,
				'base_price'	=> $package['price'],
				'price'	=> $package['price'],
				'base_period'	=> $package['period'],
				'period'	=> $package['period'],
				'activation_type'	=> $this->_getParam('activation_type'),
				'activation_date' => $this->_getParam('activation_date'),
				'is_agency'	=> $is_agency,
				'gender'	=> $escort_gender
				
			);
			
			$post_data = array(
				'order_data' => array(
					'order_date'	=> date('Y-m-d H:i:s'),
					'application_id'	=> $application_id,
					'price'	=> $package['price'],
					'user_id'	=> ($this->_getParam('new_user_id')) ? $this->_getParam('new_user_id') : $this->user->id
					//'backend_user_id'	=> $this->user->sales_user_id
				),
				'packages_data'	=> array($package_data)
			);
			
			$response = $client->call('Billing.addPackage', array($post_data));
	
			if ( is_numeric($response) ) {				
				
				$this->adv_session->transfer_id = $response;
				
				$this->adv_session->activation_type = $this->_getParam('activation_type');
				$this->adv_session->activation_date = $this->_getParam('activation_date');
				
				$this->_redirect($this->getIvrUrl($package['price'], $response, $this->_getParam('escort_id', null)));
			}
		}
	}
	
	public function advertiseSuccessAction()
	{
		$this->view->layout()->setLayout('private-v2'); 
		$this->view->layout()->global_btn_back_url = self::$linkHelper->getLink('private-v2');
		$this->_helper->viewRenderer->setScriptAction('advertise-responses');
		$this->view->key = 'advertise_success_message';
		
		//Send alert SMS to sales - start
		$esc_id = $this->adv_session->escort_id;
		if ( $this->_getParam('escort') ) {
			$esc_id = $this->_getParam('escort');
		}
		
		if ( $this->adv_session->username && $this->adv_session->password && $this->adv_session->ep ) {
			$model = new Model_Users();
			
			if ( ! $user = $model->getByUsernamePassword($this->adv_session->username, $this->adv_session->password) ) {
				
			}
			else {
				Zend_Session::regenerateId();
				$user->sign_hash = md5(rand(100000, 999999) * microtime(true));
				Model_Users::setCurrent($user);

				/*set client ID*/
				Model_Reviews::createCookieClientID($user->id);
				/**/
				
				Model_Hooks::preUserSignIn($user->id);
			}
		}
		
		$this->adv_session->unsetAll();
	}
	public function advertiseFailureAction()
	{
		$this->view->layout()->setLayout('private-v2');
		$this->view->layout()->global_btn_back_url = self::$linkHelper->getLink('private-v2-advertise-now');
		$this->_helper->viewRenderer->setScriptAction('advertise-responses');
		$this->view->key = 'advertise_failure_message';
		
		//$this->adv_session = new Zend_Session_Namespace('adv_sess');
		$this->adv_session->unsetAll();
	}
	public function advertiseCancelAction()
	{
		$this->view->layout()->setLayout('private-v2');
		$this->view->layout()->global_btn_back_url = self::$linkHelper->getLink('private-v2-advertise-now');
		$this->_helper->viewRenderer->setScriptAction('advertise-responses');
		$this->view->key = 'advertise_cancel_message';
		
		//$this->adv_session = new Zend_Session_Namespace('adv_sess');
		$this->adv_session->unsetAll();
	}
	
	private function getIvrUrl($amount, $client_identifier, $escort_id = null)
	{
		if ( ! $amount || ! $client_identifier ) {
			return self::$linkHelper->getLink('private-v2-advertise-now');
		}
		
		//Fixing prices.
		//For ex. price is actually 25 but we're sending to system 35
		if ( $amount == 25 ) {
			$amount = 35;
		} elseif ( $amount == 70 ) {
			$amount = 100;
		}
		
		$amount *= 100;
		
		$s_url = '';
		if ( $escort_id ) {
			$s_url .= '?escort=' . $escort_id;
		}
		
		if ( $this->adv_session->advertise_now ) {
			$c_url = 'http://' . $_SERVER['SERVER_NAME'] . self::$linkHelper->getLink('private-v2-advertise-now');
		}
		else {
			$c_url = self::$linkHelper->getLink('private-v2-advertise-cancel') . '?cc=1&escort=';
			if( $this->adv_session->escort_id ) {
				$c_url .= $this->adv_session->escort_id;
				
			}
			else {
				$c_url .= $escort_id;
				/*$this->adv_session->escort_id = $escort_id;
				$this->adv_session->prev = 1;*/
			}
		}
		
		$conf = Zend_Registry::get('ivr');
		$urls = array(
			'base_url'	=> 'http://ivr.truesenses.com/?cmd=new&acc=' . $conf['acc'] . '&aid=' . $conf['aid'] . '&amo=' . $amount . '&cur=chf&ser=' . $conf['acc'] . '&lan=de&cid=' . $client_identifier,
			'success_url'	=> 'srl=' . self::$linkHelper->getLink('private-v2-advertise-success') . $s_url,
			'failure_url'	=> 'frl=' . self::$linkHelper->getLink('private-v2-advertise-failure'),
			'cancel_url'	=> 'crl=' . $c_url
		);
		$url = implode('&', $urls);
		
		//var_dump($urls); die;
		
		return $url;
	}
	
	public function fastSignupAction()
	{
		$type = $this->_getParam('type');
		
		$this->view->type = $type;
		$signup_i18n = $this->view->signup_i18n = (object) array(
			'username_invalid' => $this->view->t('username_invalid'),//'Username must be at least 6 characters long',
			'username_invalid_characters' => $this->view->t('username_invalid_characters'),//'Only "a-z", "0-9", "-" and "_" are allowed',
			'username_exists' => $this->view->t('username_exists'),//'Username already exists, please choose another',
			
			'password_invalid' => $this->view->t('password_invalid'),//'Password must contain at least 6 characters',
			'password2_invalid' => $this->view->t('password2_invalid'),//'Passwords must be equal',
			
			'email_invalid' => $this->view->t('email_invalid'),//'Email is invalid, please provide valid email address',
			'email_exists' => $this->view->t('email_exists'),//'Email already exists, please enter another',
			
			'terms_required' => $this->view->t('terms_required'),//'You must agree with terms and conditions',
			'form_errors' => $this->view->t('form_errors'),//'Please check errors in form',
			'terms_required' => $this->view->t('terms_required'),//'You have to accept terms and conditions before continue',
			'domain_blacklisted' => $this->view->t('domain_blacklisted'),//'Domain is blacklisted',
            
			'user_type' => $this->view->t('choose_businnes_type'),//'Choose your business'
		);
		
		if ( $this->_request->isPost() ) {
			$user_type = $this->_getParam('user_type', $type);
            $user_t = $this->_getParam('user_type');
			
            $validator = new Cubix_Validator();

			/*if ( ! in_array($user_type, array('escort', 'agency')) ) {
                die;  
			*/
			
            if ( ! $user_t || ! in_array($user_t, array('escort', 'agency')) ) {
                 /* Update Grigor */
                $validator->setError('user_type_error', $signup_i18n->user_type);
                /* Update Grigor */

			}
			
			$data = new Cubix_Form_Data($this->_request);
			$fields = array(
				'signup_username' => '',
				'signup_password' => '',
				'signup_password2' => '',
				'signup_email' => '',
				'terms' => 'int',
				'recaptcha_response_field' => ''
			);
			$data->setFields($fields);
			$data = $data->getData();
			
			$data['username'] = $data['signup_username']; unset($data['signup_username']);
			$data['password'] = $data['signup_password']; unset($data['signup_password']);
			$data['password2'] = $data['signup_password2']; unset($data['signup_password2']);
			$data['email'] = $data['signup_email']; //unset($data['signup_email']);
			
			$data['username'] = substr($data['username'], 0, 24);
			$data['user_type'] = $user_t;
			$this->view->data = $data;
			
			
			$client = new Cubix_Api_XmlRpc_Client();
			$model = new Model_Users();
			
			if ( strlen($data['username']) < 6 ) {
				$validator->setError('signup_username', $signup_i18n->username_invalid);
			}
			elseif ( ! preg_match('/^[-_a-z0-9]+$/i', $data['username']) ) {
				$validator->setError('signup_username', $signup_i18n->username_invalid_characters);
			}
			elseif (stripos($data['username'], 'admin') !== FALSE) {
				$validator->setError('signup_username', $signup_i18n->username_exists);
			}
			elseif ( $client->call('Users.getByUsername', array($data['username'])) ) {
				$validator->setError('signup_username', $signup_i18n->username_exists);
			}

			
			if ( strlen($data['password']) < 6 ) {
				$validator->setError('signup_password', $signup_i18n->password_invalid);
			}
			elseif ( $data['password'] != $data['password2'] ) {
				$validator->setError('signup_password2', $signup_i18n->password2_invalid);
			}
			
			if ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
				$validator->setError('signup_email', $signup_i18n->email_invalid);
			}
			elseif( $client->call('Application.isDomainBlacklisted', array($data['email'])) )
			{
				$validator->setError('signup_email', $signup_i18n->domain_blacklisted);
			}
			elseif ( $client->call('Users.getByEmail', array($data['email'])) ) {
				$validator->setError('signup_email', $signup_i18n->email_exists);
			}
			
			
			/*if ( ! $data['terms'] ) {
				$validator->setError('terms', $signup_i18n->terms_required);
			}*/

			$this->view->errors = array();
			$result = $validator->getStatus();

			if ( ! $validator->isValid() ) {
				$this->view->errors = $result['msgs'];
			}
			else {
				// will set the most free sales user id
				
				// Users model
				$user = new Model_UserItem(array(
					'username' => $data['username'],
					'email' => $data['email'],
					'user_type' => $user_type,
					'status'	=> 1
				));

				//Model_Hooks::preUserSignUp($user);
				$user->sales_user_id = 100; // IVR sales user id
				
				$salt_hash = Cubix_Salt::generateSalt($data['email']);
				$user->salt_hash = $salt_hash;
				$user->password = Cubix_Salt::hashPassword($data['password'], $salt_hash);
				
				$new_user = $model->save($user);
				
				if ( $user_type == 'agency' ) {
					$m_agencies = new Model_Agencies();
					
					$agency = new Model_AgencyItem(array(
						'user_id' => $user->new_user_id,
						'name' => ucfirst($user->username)
					));
					// will set current app country id
					Model_Hooks::preAgencySignUp($agency);
					
					$agency = $m_agencies->save($agency);
				}
				
				$this->view->new_user_id = $user->new_user_id;
				$this->view->user_type = $user_type;
				$this->view->agency_id = $agency->id;
			}
		}
	}
	
	public function gotdAction()
	{
		$this->view->layout()->setLayout('private-v2');
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$this->view->booked_days = $booked_days = $client->call('Billing.getGotdBookedDays');
		
		$cities = new Cubix_Geography_Cities();
		$country_id = Cubix_Application::getById()->country_id;
		
		$cities = $cities->ajaxGetAll(null, $country_id);
		
		$grouped_cities = array();
		foreach ( $cities as $city ) {
			if ( ! isset($grouped_cities[$city->region_title]) ) {
				$grouped_cities[$city->region_title] = array();
			}

			$grouped_cities[$city->region_title][] = $city;
		}
		$this->view->grouped_cities = $grouped_cities;
		
		$is_agency = false;
		if ( $this->user->isAgency() ) {
			$is_agency = true;
			
			$escorts = $this->agency->getEscortsForGotd();
			$this->view->escorts = $escorts;
			foreach( $escorts as $k => $v ) {
				$escorts[$v['id']] = $v;
				unset($escorts[$k]);
			}
			$this->view->escorts = $escorts;
		} else {
			$this->view->escort_package = $escort_package = $client->call('Escorts.checkIfHasActivePackage', array($this->escort->id));
		}
		
		$this->view->is_agency = $is_agency;
		
		$this->view->region_relations = $region_relations = array(
			45	=> array(45, 17),
			17	=> array(45, 17),

			44	=> array(44, 20),
			20	=> array(44, 20),
		);
		
		if ( $this->_request->isPost() ) {
			$errors = array();
			$data = array(
				'city_id'	=> $this->_request->city_id,
				'escort_id' => $this->_request->escort_id,
				'date'		=> $this->_request->date
			);
			$data['date'] = explode(',', $data['date']);
			$data['date'] = mktime(0,0,0,$data['date'][1], $data['date'][2], $data['date'][0]);
			
			if ( ! $data['city_id'] ) {
				$errors['city_id'] = Cubix_I18n::translate('city_required');
			} else {
				$city_also_check[] = $data['city_id'];
				if ( isset($region_relations[$data['city_id']]) ) {
					$city_also_check = $region_relations[$data['city_id']];
				}
			}
			
			$this->view->city_also_check = $city_also_check;
			
			if ( $is_agency ) {
				if ( ! $data['escort_id'] ) {
					$errors['escort_id'] = Cubix_I18n::translate('escort_required');
				}
			} else {
				$data['escort_id'] = $this->escort->id;
			}
			
			if ( ! $data['date'] ) {
				$errors['date'] = Cubix_I18n::translate('date_required');
			} elseif ( date('Y-m-d', $data['date']) < date('Y-m-d', time()) ) {
				$errors['date'] = Cubix_I18n::translate('please_select_in_the_future');
			}
		
			
			//Checking date to be not booked
			if ( $data['city_id'] && $data['date'] ) {
				foreach($city_also_check as $ct) {
					foreach($booked_days[$ct] as $dt) {
						if ( date('Y-m-d', $data['date']) == date('Y-m-d', strtotime($dt['date'])) ) {
							$errors['date'] = Cubix_I18n::translate('day_already_booked');
							break;
						}
					}
				}
			}
			
			if ( $data['city_id'] && $data['date'] ) {
				foreach($booked_days as $city_id => $dt) {
					foreach($dt as $d) {
						if ( date('Y-m-d', $data['date']) == date('Y-m-d', strtotime($d['date'])) && $data['escort_id'] == $d['escort_id'] ) {
							$errors['date'] = Cubix_I18n::translate('only_one_gotd_per_day');
						}
					}
				}
			}
			
			//Checking date to be in package active range
			if ( $data['escort_id'] && $data['date'] ) {
				if ( $is_agency ) {
					if( $_GET['is_test'] ) {
						var_dump($escorts);
						var_dump($data['date']);
					}
					if ( $data['date'] < $escorts[$data['escort_id']]['date_activated'] || $data['date'] > $escorts[$data['escort_id']]['expiration_date']  ) {
						$errors['date'] = Cubix_I18n::translate('date_must_be_in_package_range');
					}
				} else {
					if ( $data['date'] < $escort_pacakge['date_activated'] || $data['date'] > $escort_package['expiration_date']  ) {
						$errors['date'] = Cubix_I18n::translate('date_must_be_in_package_range');
					}
				}
			}
			
			
			$this->view->data = $data;
			
			if ( count($errors) ) {
				$this->view->gotd_errors = $errors;
			} else {
				$result = $client->call('Billing.bookGotd', array($data['escort_id'], $data['city_id'], $data['date']));
				if ( $result ) {
					$product = $client->call('Billing.getProduct', array(15));
					$this->_redirect($this->getIvrGotdUrl($product['price'], $result));
				}
			}
		}
	}
	
	private function getIvrGotdUrl($amount, $client_identifier)
	{
		$amount *= 100;
		$conf = Zend_Registry::get('ivr');
		
		$urls = array(
			'base_url'	=> 'http://ivr.truesenses.com/?cmd=new&acc=' . $conf['acc'] . '&aid=' . $conf['aid'] . '&amo=' . $amount . '&cur=chf&ser=' . $conf['accGotd'] . '&lan=de&cid=' . $client_identifier,
			'success_url'	=> 'srl=' . self::$linkHelper->getLink('private-v2-gotd-success'),
			'failure_url'	=> 'frl=' . self::$linkHelper->getLink('private-v2-gotd-failure'),
			'cancel_url'	=> 'crl=' . self::$linkHelper->getLink('private-v2-gotd-cancel')
		);
		
		$url = implode('&', $urls);
		
		return $url;
	}
	
	public function gotdSuccessAction()
	{
		$this->view->layout()->setLayout('private-v2'); 
		$this->view->layout()->global_btn_back_url = self::$linkHelper->getLink('private-v2');
		$this->_helper->viewRenderer->setScriptAction('gotd-responses');
		$this->view->key = 'gotd_success_message';
	}
	
	public function gotdFailureAction()
	{
		$this->view->layout()->setLayout('private-v2');
		$this->view->layout()->global_btn_back_url = self::$linkHelper->getLink('private-v2-gotd');
		$this->_helper->viewRenderer->setScriptAction('gotd-responses');
		$this->view->key = 'gotd_failure_message';
	}
	
	public function gotdCancelAction()
	{
		$this->view->layout()->setLayout('private-v2');
		$this->view->layout()->global_btn_back_url = self::$linkHelper->getLink('private-v2-gotd');
		$this->_helper->viewRenderer->setScriptAction('gotd-responses');
		$this->view->key = 'gotd_cancel_message';
	}
	
}
