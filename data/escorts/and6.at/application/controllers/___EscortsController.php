<?php

class EscortsController extends Zend_Controller_Action
{
	public static $linkHelper;
	
	public function init()
	{
		self::$linkHelper = $this->view->getHelper('GetLink');
	}
	
	public static function _itemDecorator($item, $is_first = FALSE, $is_hidden = FALSE)
	{
		$has_childs = isset($item->childs) && is_array($item->childs) && count($item->childs);
		
		$classes = array();
		
		if ( $is_first ) $classes[] = 'first';
		if ( $has_childs ) $classes[] = 'sub';
		
		$link = '#';
		if ( ! $has_childs ) {
			$link = self::$linkHelper->getLink('escorts', array('filter' => $item->value, 'page' => NULL));
		}
		
		$title = $item->title;
		if ( $is_first && isset($item->title) ) {
			$title = $item->parent . ' - ' . $item->title;
		}
		
		return '<a ' . ($is_hidden ? 'style="display: none" ' : '') . '' . (count($classes) ? 'class="' . implode(' ', $classes) . '" ' : '') . 'href="' . $link . '"' . ($item->value ? ' rel="' . $item->value . '"' : '') . ' ' . ( true ? 'onclick="return Cubix.Filter.Set({ filter: ' . ($item->value ? "'" . $item->value . "'" : 'null') . ', page: null })"' : '' ) . '>' . $title . '</a>';
	}
	
	public static function _sortItemDecorator($item, $is_first = FALSE, $is_hidden = FALSE)
	{
		$has_childs = isset($item->childs) && is_array($item->childs) && count($item->childs);
		
		$classes = array();
		
		if ( $is_first ) $classes[] = 'first';
		if ( $has_childs ) $classes[] = 'sub';
		
		$link = '#';
		if ( ! $has_childs ) {
			$link = self::$linkHelper->getLink('escorts', array('sort' => $item->value));
		}
		
		$title = $item->title;
		if ( $is_first && isset($item->title) ) {
			$title = $item->parent . ' - ' . $item->title;
		}
		
		return '<a ' . ($is_hidden ? 'style="display: none" ' : '') . '' . (count($classes) ? 'class="' . implode(' ', $classes) . '" ' : '') . 'href="' . $link . '"' . ($item->value ? ' rel="' . $item->value . '"' : '') . ' ' . ( true ? 'onclick="return Cubix.Filter.Set({ sort: \'' . $item->value . '\' })"' : '' ) . '>' . $title . '</a>';
	}

	protected function _getFooterText($key = 'main')
	{
		// $model = new Model_StaticPage();

		// $lang_id = Cubix_I18n::getLang();

		// $page = $model->getBySlug('footer-text-escorts-' . $key, Cubix_Application::getId(), $lang_id);
		
		// if ( $page ) return $page->body;

		$seo = $this->view->seo('home-page-' . $key, null, array());

		if ( $seo ) {
			if ( strlen($seo->footer_text) ) {
				return $seo->footer_text;
			}
		}

		return '';
	}

	public function indexAction()
	{
		if ( ! is_null($this->_getParam('ajax')) ) {
			$this->view->layout()->disableLayout();
		}
		
		$this->view->quick_links = true;
		$upcoming_tours = false;

		$req = $this->_getParam('req');

		/* --> Fix for old urls */
		$a = array();
		if ( preg_match('#(/(city|region)_[a-z]{2}_)([^/]+)#', $req, $a) ) {
			array_shift($a); $a[2] = str_replace('_', '-', $a[2]);
			$req = preg_replace('#(/(city|region)_[a-z]{2}_)([^/]+)#', "$1{$a[2]}", $req);
		}
		/* <-- */

		$req = explode('/', $req);


		$menus = array('filter' => NULL, 'sort' => NULL);

		$cache = Zend_Registry::get('cache');
		$defs = Zend_Registry::get('definitions');

		$filterStructure = $defs['escorts_filter'];

		$menus['filter'] = new Cubix_NestedMenu(array('childs' => $filterStructure));
		$menus['filter']->setId('filter-options');
		$menus['filter']->setSelected($menus['filter']->getByValue(NULL));
		
		$menus['sort'] = new Cubix_NestedMenu(array('childs' => $defs['escorts_sort']));
		$menus['sort']->setId('sort-options');
		$menus['sort']->setSelected($menus['sort']->getByValue('random'));
		
		$params = array(
			'sort' => 'random',
			'filter' => array(array('field' => 'girls', 'value' => array())),
			'page' => 1
		);

		$static_page_key = 'main';

		foreach ( $req as $r ) {
			if ( ! strlen($r) ) continue;
			$param = explode('_', $r);

			if ( count($param) < 2 ) {
				switch( $r )
				{
					case 'new':
						//$params['filter'][] = array('field' => 'new_arrivals', 'value' => array());
						$static_page_key = 'new';
						$param = array('sort', 'newest');
					break;
					case 'independent':
						$params['filter'][] = array('field' => 'independent', 'value' => array());
					continue;
					case 'agency':
						$params['filter'][] = array('field' => 'agency', 'value' => array());
					continue;
					case 'boys':
						$static_page_key = 'boys';
						$params['filter'][0] = array('field' => 'boys', 'value' => array());
					continue;
					case 'trans':
						$static_page_key = 'trans';
						$params['filter'][0] = array('field' => 'trans', 'value' => array());
					continue;
					case 'tours':
						$static_page_key = 'tours';
						$this->view->is_tour = $is_tour = true;
						$params['filter'][] = array('field' => 'tours', 'value' => array());
					continue;
					case 'upcomingtours':
						$this->view->is_tour = $is_tour = true;
						$this->view->is_upcomingtour = true;
						$upcoming_tours = true;
						$params['filter'][] = array('field' => 'upcomingtours', 'value' => array());
					continue;
/*					case 'blank.html':
						$this->_forward('blank-html', 'redirect');
					return;*/
					default:
						$this->_response->setRawHeader('HTTP/1.1 404 Not Found');
						$this->_forward('error', 'error', null, array('error_msg' => '404 Page Not Found. Please try the following links instead.'));
					return;
				}
			}
			
			$param_name = $param[0];
			array_shift($param);
			
			switch ( $param_name ) {
				case 'filter':
					/* >>> For nested menu */
					$selected_item = $menus['filter']->getByValue(implode('_', $param));
					if ( ! is_null($selected_item) ) {
						$menus['filter']->setSelected($selected_item);
					}
					/* <<< */
					
					$field = reset($param);
					array_shift($param);
					
					$value = array_slice($param, 0, 2);
					
					$params['filter'][] = array('field' => $field, 'value' => $value , 'main' => TRUE);
				break;
				case 'page':
					$page = intval(reset($param));
					if ( $page < 1 ) {
						$page = 1;
					}
					$params['page'] = $page;
				break;
				case 'sort':
					$params['sort'] = reset($param);
					
					$selected_item = $menus['sort']->getByValue($params['sort']);
					if ( ! is_null($selected_item) ) {
						$menus['sort']->setSelected($selected_item);
					}
				break;
				case 'region':
				case 'state':
					$params['country'] = $param[0];
					$params['region'] = $param[1];
					$params['filter'][] = array('field' => 'region', 'value' => $param[1]);
				break;
				case 'city':
				case 'cityzone':
					$params[$param_name] = $param[1];
					$params['filter'][] = array('field' => $param_name, 'value' => array($param[1]));
				break;
				case 'name':
					$params['filter'][] = array('field' => 'name', 'value' => array('%' . $param[0] . '%'));
				break;
				default:
					if ( ! in_array($param_name, array('new', 'independent', 'agency', 'boys', 'trans', 'tours', 'upcomingtours')) ) {
						$this->_response->setRawHeader('HTTP/1.1 404 Not Found');
						$this->_forward('error','error', null, array('error_msg' => '404 Page Not Found. Please try the following links instead.'));
						return;
					}
			}
		}

		// Get footer text for seo manager
		// $this->view->layout()->footer_text = $this->_getFooterText($static_page_key);
		$this->view->static_page_key = $static_page_key;
		
		//print_r($params);
		
		$filter_params = array(
			'order' => 'e.date_registered DESC',
			'limit' => array('page' => 1),
			'filter' => array()
		);
		
		$filter_map = array(
			/*SVC*/
			'svc-anal' => 'e.svc_anal = ?',
			'svc-kissing' => 'e.svc_kissing = ?',
			'svc-kissing-with-tongue' => 'e.svc_kissing = ?',
			'svc-blowjob-with-condom' => 'e.svc_blowjob = ?',
			'svc-blowjob-without-condom' => 'e.svc_blowjob = ?',
			'svc-cumshot-in-face' => 'e.svc_cumshot = ?',
			'svc-cumshot-in-mouth-swallow' => 'e.svc_cumshot = ?',
			'svc-cumshot-in-mouth-spit' => 'e.svc_cumshot = ?',
			/*SVC*/

			'verified' => 'e.verified_status = 2',

			'age' => 'e.age BETWEEN ? AND ?',
			'ethnic' => 'e.ethnicity = ?',
			'height' => 'e.height > ? AND e.height < ?',
			'weight' => 'e.weight > ? AND e.weight < ?',
			'breast-size' => 'e.breast_size = ?',
			'hair-color' => 'e.hair_color = ?',
			'eye-color' => 'e.eye_color = ?',
			'dress-size' => 'e.dress_size = ?',
			'shoe-size' => 'e.shoe_size = ?',
			'available-for' => 'e.availability = ?',
			'service-for' => 'FIND_IN_SET(?, e.sex_availability)',
			'smoker' => 'e.is_smoker = ?',
			'language' => 'FIND_IN_SET(?, e.languages)',
			'now-open' => 'e.is_now_open',
			'region' => 'r.slug = ?',
			'city' => 'cc.slug = ?',
			'cityzone' => 'c.id = ?',
			'name' => 'e.showname LIKE ?',

			'new_arrivals' => 'e.gender = ' . GENDER_FEMALE,
			'independent' => 'e.agency_id IS NULL AND e.gender = ' . GENDER_FEMALE,
			'agency' => 'e.agency_id IS NOT NULL AND e.gender = ' . GENDER_FEMALE,
			'boys' => 'e.gender = ' . GENDER_MALE,
			'trans' => 'e.gender = ' . GENDER_TRANS,
			'girls' => 'e.gender = ' . GENDER_FEMALE,
			
			'tours' => 'e.is_on_tour = 1',
			'upcomingtours' => 'e.tour_date_from > CURDATE()'
		);
		
		/* --> If we are on the city tours page the behaviour of city filter is different */
		foreach ( $params['filter'] as $filter ) {
			if ( 'tours' == $filter['field'] ) {
				//$filter_map['city'] = 'cc.'
			}
		}
		/* <-- */
		
		/* --> Get page from request */
		$page = intval($params['page']);
		if ( $page == 0 ) {
			$page = 1;
		}
		$filter_params['limit']['page'] = $page;
		/* <-- */
		
		/* --> Construct parameters for getAll method */
		$selected_filter = $menus['filter']->getSelected();
		
		foreach ( $params['filter'] as $filter ) {
			if ( ! isset($filter_map[$filter['field']]) ) continue;
			
			$value = $filter['value'];
			
			if ( isset($filter['main']) ) {
				if ( isset($selected_filter->internal_value) ) {
					$value = $selected_filter->internal_value;
				}
			}
			
			$filter_params['filter'][$filter_map[$filter['field']]] = $value;
		}
		/* <-- */
		
		/* --> Init sorting */
		$sort_map = array(
			'random' => 'e.ordering DESC',
			'alpha' => 'e.showname ASC',
			'most-viewed' => 'e.hit_count DESC',
			'newest' => 'e.date_registered DESC',
			'last-modified' => 'e.date_last_modified DESC'
		);
		
		if ( isset($sort_map[$params['sort']]) ) {
			$filter_params['order'] = $sort_map[$params['sort']];
			$this->view->sort_param = $params['sort'];
		}
		/* <-- */
		
		$model = new Model_Escorts();
		$count = 0;

		/* --> Cache key generation */
		$filters_str = '';
		
		foreach ( $filter_params['filter'] as $k => $filter ) {
			if ( ! is_array($filter) ) {
				$filters_str .= $k . '_' . $filter;
			}
			else {
				$filters_str .= $k;
				foreach($filter as $f) {
					$filters_str .= '_' . $f;
				}
			}
		}

		$cache_key = 'v2_' . Cubix_I18n::getLang() . '_' . $filter_params['order'] . '_' . $filter_params['limit']['page'] . '_' . $filters_str;
		$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

		$countCacheKey = 'v2_' . Cubix_I18n::getLang() . '_' . $filters_str;
		$countCacheKey =  preg_replace('#[^a-zA-Z0-9_]#', '_', $countCacheKey);
		/* <-- */

		$count = $cache->load($cache_key . '_count');
		if ( ! $escorts = $cache->load($cache_key) )
		{
			if ( $is_tour ) {
				$escorts = $model->getAllTours($filter_params, $count, $upcoming_tours);
			}
			else {
				$escorts = $model->getAll($filter_params, $count);
			}
			//echo "not -cache";
			$cache->save($escorts, $cache_key, array());
			$cache->save($count, $cache_key . '_count', array());
		}

		if ( 'random' == $params['sort'] ) {
			shuffle($escorts);
		}

		/* --> Calculate Filter Menu Counts */
		if ( is_null($this->_getParam('ajax')) ) {
			if ( ! $filterStructure = $cache->load($countCacheKey) ) {
				$this->_calcFilterCounts($count, $filter_params, $menus['filter'], $countCacheKey);
			}
			else {
				$menus['filter']->setStructure(array('childs' => $filterStructure));
			}
		}
		/* <-- */

		/* --> If it is ajax request, disable the layout and render only list.phtml */
		if ( ! is_null($this->_getParam('ajax')) ) {
			$this->view->layout()->disableLayout();
			$this->_helper->viewRenderer->setScriptAction('list');
		}
		/* <-- */
		
		$this->view->params = $params;
		$this->view->count = $count;
		$this->view->escorts = $escorts;
		$this->view->menus = $menus;
	}

	/**
	 *
	 * @param string $r
	 */
	protected function _makeFilterArgument($r)
	{
		$param = explode('_', $r);

		if ( count($param) < 2 ) {
			switch( $r ) {
				case 'independent':
				case 'agency':
				case 'boys':
				case 'trans':
				case 'tours':
				case 'upcomingtours':
				return array('field' => $r, 'value' => array());
			}
		}

		$param_name = $param[0];
		array_shift($param);

		switch ( $param_name ) {
			case 'filter':
				$field = reset($param);
				array_shift($param);

				$value = array_slice($param, 0, 2);

				$filter = array('field' => $field, 'value' => $value , 'main' => TRUE);
			break;
			case 'region':
			case 'state':
				$filter = array('field' => 'region', 'value' => $param[1]);
			break;
			case 'city':
			case 'cityzone':
				$filter = array('field' => $param_name, 'value' => array($param[1]));
			break;
			case 'name':
				$filter = array('field' => 'name', 'value' => array('%' . $param[0] . '%'));
			break;
		}

		return $filter;
	}

	/**
	 * Calculates filter counts for each
	 *
	 * @param integer $count
	 * @param array $filterParams
	 * @param Cubix_NestedMenu $menu
	 * @param string $cacheKey
	 */
	protected function _calcFilterCounts($count, $filterParams, Cubix_NestedMenu $menu, $cacheKey = null)
	{
		$model = new Model_Escorts();
		$defs = Zend_Registry::get('defines');

		$filterMap = array(
			'svc-anal' => 'e.svc_anal = ?',
			'svc-kissing' => 'e.svc_kissing = ?',
			'svc-kissing-with-tongue' => 'e.svc_kissing = ?',
			'svc-blowjob-with-condom' => 'e.svc_blowjob = ?',
			'svc-blowjob-without-condom' => 'e.svc_blowjob = ?',
			'svc-cumshot-in-face' => 'e.svc_cumshot = ?',
			'svc-cumshot-in-mouth-swallow' => 'e.svc_cumshot = ?',
			'svc-cumshot-in-mouth-spit' => 'e.svc_cumshot = ?',

			'age' => 'e.age BETWEEN ? AND ?',
			'ethnic' => 'e.ethnicity = ?',
			'height' => 'e.height > ? AND e.height < ?',
			'weight' => 'e.weight > ? AND e.weight < ?',
			'breast-size' => 'e.breast_size = ?',
			'hair-color' => 'e.hair_color = ?',
			'eye-color' => 'e.eye_color = ?',
			'dress-size' => 'e.dress_size = ?',
			'shoe-size' => 'e.shoe_size = ?',
			'available-for' => 'e.availability = ?',
			'service-for' => 'FIND_IN_SET(?, e.sex_availability)',
			'smoker' => 'e.is_smoker = ?',
			'language' => 'FIND_IN_SET(?, e.languages)',
			'now-open' => 'e.is_now_open',
			'region' => 'r.slug = ?',
			'city' => 'cc.slug = ?',
			'cityzone' => 'c.id = ?',
			'name' => 'e.showname LIKE ?',

			'new_arrivals' => 'e.gender = ' . GENDER_FEMALE,
			'independent' => 'e.agency_id IS NULL AND e.gender = ' . GENDER_FEMALE,
			'agency' => 'e.agency_id IS NOT NULL AND e.gender = ' . GENDER_FEMALE,
			'boys' => 'e.gender = ' . GENDER_MALE,
			'trans' => 'e.gender = ' . GENDER_TRANS,
			'girls' => 'e.gender = ' . GENDER_FEMALE,

			'tours' => 'e.is_on_tour = 1',
			'upcomingtours' => 'e.tour_date_from > CURDATE()'
		);

		$counts = array();

		$filterStructure = $defs['escorts_filter'];

		foreach ( $filterStructure as $i => $filter ) {
			$fake = false;
			if ( ! isset($filter['childs']) ) {
				$filter['childs'] = array($filter);
				$fake = true;
			}
			
			foreach ( $filter['childs'] as $j => $child ) {
				$f = $this->_makeFilterArgument('filter_' . $child['value']);

				$newFilter = $filterParams;

				if ( ! isset($filterMap[$f['field']]) ) continue;

				$value = $f['value'];

				if ( isset($f['main']) && $f['main'] ) {
					if ( isset($child['internal_value']) ) {
						$value = $child['internal_value'];
					}
				}

				$newFilter['filter'][$filterMap[$f['field']]] = $value;

				$count = $model->getCount($newFilter);
				
				if ( $fake ) {
					$filterStructure[$i]['title'] .= " ($count)";
				}
				else {
					$filterStructure[$i]['childs'][$j]['title'] .= " ($count)";;
				}
			}
		}

		if ( ! is_null($cacheKey) ) {
			Zend_Registry::get('cache')->save($filterStructure, $cacheKey, array());
		}

		$menu->setStructure(array('childs' => $filterStructure));
	}

	public function profileAction()
	{
		$this->view->layout()->setLayout('profile');
		
		$showname = $this->_getParam('escortName');
		
		$model = new Model_Escorts();
		
		$cache_key = 'v2_' . $showname . '_profile_' . Cubix_I18n::getLang();
		$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

		
		$escort = $model->get($showname, $cache_key);

		if ( ! $escort || isset($escort['error']) )
		{
			/*$this->_response->setRawHeader('HTTP/1.1 404 Not Found');
			$this->_forward('error','error', null, array('error_msg' => 'The escort page you are looking for does not exist on the server. Please try the following links instead.', 'param' => 'Showname: ' . $showname));
			*/
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			die;
			return;
			//header('Location: /');
		}

		$this->view->escort_id = $escort->id; 
		$this->view->escort = $escort;
		
		$this->photosAction($escort);
		$this->privatePhotosAction($escort);
		
		if ( $escort->agency_id )
			$this->agencyEscortsAction($escort->agency_id, $escort->id);
	}
	
	public function agencyEscortsAction($agency_id = null, $escort_id = null)
	{
		$config = Zend_Registry::get('escorts_config');
		
		if ( ! $agency_id )
		{
			$this->view->layout()->disableLayout();
			$agency_id = $this->view->agency_id = $this->_getParam('agency_id');
		}
		else
			$this->view->agency_id = $agency_id;
		
		if( ! $escort_id )
			$escort_id = $this->view->escort_id = $this->_getParam('escort_id');
		
		$page = intval($this->_getParam('agency_page'));
		if ( $page < 1 )
			$page = 1;
			
		/*$model = new Model_Agencies();
		$agency = $model->get($agency_id);
		
		$this->view->agency = $agency;
		
		$params = array(
			'sort' => 'random',
			'filter' => array(
				'e.agency_id = ?' => $agency_id
			),
			'limit' => array(
				'page' => $page,
				'perPage' => $config['widgetPerPage']
			)
		);
		
		$count = 0;
		$escorts = $agency->getEscorts($params, $count, $escort_id);*/
		
		$client = new Cubix_Api_XmlRpc_Client();
		$agency = $client->call('Agencies.getEscorts', array($agency_id, $page, $config['widgetPerPage'], $escort_id));
		
		if ( $agency['escorts_count'] != 0 ) {
			$escorts = $agency['escorts'];
			$count = $agency['escorts_count'];

			foreach ( $escorts as $i => $escort ) {
				$escorts[$i] = new Model_EscortItem($escort);
			}

			$this->view->agency = new Model_AgencyItem($agency);

			$this->view->a_escorts = $escorts;
			$this->view->a_escorts_count = $count;

			$this->view->agencies_page = $page;
			$this->view->widgetPerPage = $config['widgetPerPage'];
		}
	}
	
	public function photosAction($escort = null)
	{
		$config = Zend_Registry::get('escorts_config');
		
		$page = $this->_getParam('photo_page');

		$escort_id = $this->_getParam('escort_id');
		
		if( $escort_id )
		{
			$this->view->layout()->disableLayout();
			$model = new Model_Escorts();
			$escort = $model->get($escort_id);
			$this->view->escort_id = $escort_id;
		}
		
		if ( ! $page )
			$page = 1;
		
		$count = 0;
		$this->view->photos = $escort->getPhotos($page, $count, false);
		$this->view->photos_count = $count;
		$this->view->photos_page = $page;
		$this->view->photos_perPage = $config['photos']['perPage'];
	}
	
	public function privatePhotosAction($escort = null)
	{
		$config = Zend_Registry::get('escorts_config');
		
		$page = $this->_getParam('photo_page');
		$escort_id = $this->_getParam('escort_id');
		
		if( $escort_id )
		{
			$this->view->layout()->disableLayout();
			$model = new Model_Escorts();
			$escort = $model->get($escort_id);
			$this->view->escort_id = $escort_id;
		}
		
		if ( ! $page )
			$page = 1;
			
		$count = 0;
		$this->view->private_photos = $escort->getPhotos($page, $count, false, true);
		$this->view->private_photos_count = $count;
		$this->view->private_photos_page = $page;
		$this->view->private_photos_perPage = $config['photos']['perPage'];
	}
	
	public function viewedEscortsAction()
	{
		
		$this->view->layout()->disableLayout();
		$escort_id = intval($this->_getParam('escort_id'));
		
		if ( $escort_id )
		{
			self::_addToLatestViewedList($escort_id);
			$viewedEscorts = self::_getLatestViewedList(10, $escort_id);
		}
		else
			$viewedEscorts = self::_getLatestViewedList(10);

		if ( ! is_array($viewedEscorts) || count($viewedEscorts) == 0 )
			return false;
		
		$escort = new Model_Escorts();
		
		$ve = array();
		
		$ids = array();
		foreach ( $viewedEscorts as $e ) {
			$ids[] = $e['id'];
		}
		
		$escorts = $escort->getAll(array(
			'order' => 'FIELD(e.id, ' . implode(', ', $ids) . ')',
			'filter' => array(
				'e.id IN (' . implode(', ', $ids) . ')' => array()
			)
		));
		
		$this->view->escorts = $escorts;		
	}
	
	private function _addToLatestViewedList($escort_id)
	{
		$count = 11; // actually we need 10
		$escort = array('id' => $escort_id);
		$sid = 'last_viewed_escorts_' . Cubix_Application::getId();
		
		$ses = new Zend_Session_Namespace($sid);
		
		// Checking if session exists
		// creating if not exists		
		if ( false == (isset($ses->escorts) && is_array($ses->escorts)) )
		{
			$ses->escorts = array();
		}
		else 
		{
			// Checking if escort exists in stack
			// removing escort from stack if exists
			foreach ($ses->escorts as $key => $item)
			{
				if ( $escort['id'] == $item['id'] )
				{
					array_splice($ses->escorts, $key, 1);
				}
			}
		}
		
		// Pushing escort to the end of the stack
		array_unshift($ses->escorts, $escort);
		
		// Checking if
		if ( count($ses->escorts) > $count )
		{	
			array_pop($ses->escorts);
		}
	}
	
	// Getting latest viewed escorts list (stack) 
	// as Array of Assoc
	public function _getLatestViewedList($max, $exclude = NULL)
	{
		$sid = 'last_viewed_escorts_' . Cubix_Application::getId();
		
		$ses = new Zend_Session_Namespace($sid);

		if ( ! is_array($ses->escorts) )
			return array();
		
		if ( $exclude )
		{
			$arr = $ses->escorts;
			foreach($arr as $key => $item)
			{
				if($exclude == $item['id'])
				{
					array_splice($arr, $key, 1);
				}	
			}
			$ret = array_slice($arr, 0, $max);
			
			return $arr;
		}

		$ret = array_slice($ses->escorts, 0, $max);
		
		return $ret;
	}
	
	public function searchAction()
	{
		$this->view->layout()->setLayout('main-simple');		
		
		$this->view->quick_links = false;
		
		if( $this->_getParam('SEARCH1') || $this->_getParam('SEARCH2') || $this->_getParam('SEARCH3') )
		{
			$params['filter'] = array();
			
			$this->view->params = $this->_request->getParams();
			
			// ---> SEARCH FORM1
			if( $this->_getParam('show_name') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.showname LIKE ?' => "%" . trim($this->_getParam('show_name')) . "%"
				));
			}			
			if( $this->_getParam('gender') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.gender = ?' => $this->_getParam('gender')
				));
			}			
			if( $this->_getParam('sel_country') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.country_id = ?' => $this->_getParam('sel_country')
				));
			}
			if( $this->_getParam('city') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.city_id = ?' => $this->_getParam('city')
				));
			}			
			if( $this->_getParam('words') ) 
			{
				if ($this->_getParam('words_type') == 'word') 
				{
	            	if ($pos = strpos($this->_getParam('words'), ' ')) {
	            		$sub = substr($this->_getParam('words'), 0, $pos);
	            	}
	            	else {
	            		$sub = $this->_getParam('words');
	            	}
	            	
	            	$params['filter'] = array_merge($params['filter'], array(
						'ep.' . Cubix_I18n::getTblField('about') . ' LIKE ?' => "%" . $sub . "%"
					));
	            }
	            elseif ($this->_getParam('words_type') == 'phrase') 
	            {
	            	$params['filter'] = array_merge($params['filter'], array(
						'ep.' . Cubix_I18n::getTblField('about') . ' LIKE ?' => "%" . $this->_getParam('words') . "%"
					));
	            }
			}			
			// <----
			
			
			// ---> SEARCH FORM2
			if( $this->_getParam('age_from') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'FLOOR((TO_DAYS(NOW())- TO_DAYS(ep.birth_date)) / 365.25) >= ?' => $this->_getParam('age_from')
				));
			}			
			if( $this->_getParam('age_to') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'FLOOR((TO_DAYS(NOW())- TO_DAYS(ep.birth_date)) / 365.25) <= ?' => $this->_getParam('age_to')
				));
			}			
			if( $this->_getParam('eye_color') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'ep.eye_color = ?' => $this->_getParam('eye_color')
				));
			}			
			if( $this->_getParam('breast_size') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'ep.breast_size = ?' => $this->_getParam('breast_size')
				));
			}			
			if( $this->_getParam('dress_size') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'ep.dress_size = ?' => $this->_getParam('dress_size')
				));
			}			
			if( $this->_getParam('ethnicity') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'ep.ethnicity = ?' => $this->_getParam('ethnicity')
				));
			}			
			if( $this->_getParam('hair_color') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'ep.hair_color = ?' => $this->_getParam('hair_color')
				));
			}			
			if( $this->_getParam('bust') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'ep.bust = ?' => $this->_getParam('bust')
				));
			}			
			if( $this->_getParam('waist') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'ep.waist = ?' => $this->_getParam('waist')
				));
			}			
			if( $this->_getParam('hip') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'ep.hip = ?' => $this->_getParam('hip')
				));
			}			
			if( $this->_getParam('shoe') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'ep.shoe_size = ?' => $this->_getParam('shoe')
				));
			}			
			if( $this->_getParam('smoker') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'ep.is_smoker = ?' => $this->_getParam('smoker')
				));
			}			
			if( $this->_getParam('available_for') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'ep.availability = ?' => $this->_getParam('available_for')
				));
			}			
			if( $this->_getParam('saf') ) {
				$params['filter'] = array_merge($params['filter'], array(					
					'FIND_IN_SET( ?, ep.sex_availability)' => $this->_getParam('saf')
				));
			}
			// <-----
			
			// ---> SEARCH FORM3
			if( $this->_getParam('kissing') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'ep.svc_kissing = ?' => $this->_getParam('kissing')
				));
			}			
			if( $this->_getParam('blowjob') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'ep.svc_blowjob = ?' => $this->_getParam('blowjob')
				));
			}			
			if( $this->_getParam('cumshot') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'ep.svc_cumshot = ?' => $this->_getParam('cumshot')
				));
			}
			if( $this->_getParam('69') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'ep.svc_69 = ?' => $this->_getParam('69')
				));
			}
			if( $this->_getParam('anal') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'ep.svc_anal = ?' => $this->_getParam('anal')
				));
			}
			// <-----
			
			$model = new Model_Escorts();
			$count = 0;
			$escorts = $model->getAll($params, $count);
			
										
			$this->view->count = $count;
			$this->view->escorts = $escorts;
		}
	}
}
