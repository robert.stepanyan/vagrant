<?php

class ClubDirectoryController extends Zend_Controller_Action
{
	public function init()
	{
		$this->view->layout()->setLayout('main-simple');
	}
	
	public function indexAction()
	{
		$m = new Model_ClubDirectory();
		
		$conf = Zend_Registry::get('agencies_config');
		$this->view->perPage = $perPage = $conf['club_directory']['perPage'];
		$this->view->page = $page = 1;
		
		$this->view->filter = $filter = array('working_girl', 1, 3, 2, 4);
		
		$geoData = Cubix_Geoip::getClientLocation();
		
		/*$geoData['latitude'] = 47.4167;
		$geoData['longitude'] = 8.4333;
		$geoData['country_iso'] = 'CH';
		$geoData['city'] = 'Adliswil';*/
		
		if ( isset($geoData['city']) ) {
			
			$curr_city = $m->isCityExist($geoData['city']);
			
			if ( $curr_city ) {
				$this->view->current_city = $curr_city;
				
				$this->view->n_cities = $m->getNearestCities($geoData['latitude'], $geoData['longitude'], 3, $curr_city->city_id);
			} else if ( $geoData['country_iso'] == 'CH' ) {
				$m->logGeoData($geoData);
			}
		}
		
		$coord = array();
		
		$this->view->ordered_array = $m->getList($filter, null, $coord, $page, $perPage, $count);
		$this->view->count = $count;
		
		$this->view->listing_type = true;
	}
		
	public function listAction()
	{
		$this->view->layout()->disableLayout();
		
		$m = new Model_ClubDirectory();
		
		$conf = Zend_Registry::get('agencies_config');
		$this->view->perPage = $perPage = $conf['club_directory']['perPage'];
		$this->view->page = $page = intval($this->_request->getParam('page', 1));
		
		$filter = array();
		$search = null;
		$coord = array();
		
		$geoData = Cubix_Geoip::getClientLocation();
		
		/*$geoData['latitude'] = 47.4167;
		$geoData['longitude'] = 8.4333;
		$geoData['country_iso'] = 'CH';
		$geoData['city'] = 'Adliswil';*/
		
		if (!is_null($geoData))
		{
			if ($geoData['latitude'] && $geoData['longitude'])
			{
				$coord = array('lat' => $geoData['latitude'], 'lon' => $geoData['longitude']);
			}
		}
		
		//$coord = array('lat' => 47.4167, 'lon' => 8.4333);
		
		$filter = $this->_request->getParam('filter', array());
		
		if (strlen(trim($this->_request->search_text)))
			$search = trim($this->_request->search_text);
		
		$area = $this->_request->area;
		$city = $this->_request->city;
		$this->view->all_cities = $this->_request->all_cities;
		
		$this->view->ordered_array = $m->getList($filter, $search, $coord, $page, $perPage, $count, $area, $city);
		$this->view->count = $count;
		
		
		$this->view->listing_type = true;
		
		if ( ! is_null($this->_getParam('ajax')) ) {
			$list = $this->view->render('club-directory/list.phtml');
			
			die(json_encode(array('body' => $list, 'count' => $count, 'c' => $this->_request->c)));
		}
	}
	
	public function mapAction()
	{
		$this->view->addScriptPath($this->view->getScriptPath('club-directory'));
		$this->_helper->viewRenderer->setScriptAction("index");
		
		$m = new Model_ClubDirectory();
				
		$geoData = Cubix_Geoip::getClientLocation();
				
		/*$geoData['latitude'] = 47.4167;
		$geoData['longitude'] = 8.4333;
		$geoData['country_iso'] = 'CH';
		$geoData['city'] = 'Adliswil';*/
		
		if ( isset($geoData['city']) ) {
			
			$curr_city = $m->isCityExist($geoData['city']);
			
			if ( $curr_city ) {
				$this->view->current_city = $curr_city;
				
				$this->view->n_cities = $m->getNearestCities($geoData['latitude'], $geoData['longitude'], 3, $curr_city->city_id);
			} else if ( $geoData['country_iso'] == 'CH' ) {
				$m->logGeoData($geoData);
			}
		}
		
		$this->view->filter = $filter = array('working_girl', 1, 3, 2, 4);
		$this->view->listing_type = false;
	}
	
	public function ajaxMapAction()
	{
		$this->view->listing_type = false;
		$this->view->layout()->disableLayout();
		
		$m = new Model_ClubDirectory();
				
		$filter = array();
		$search = null;
				
		$filter = $this->_request->getParam('filter', array('working_girl', 1, 3, 2, 4));
		
		if (strlen(trim($this->_request->search_text)))
			$search = trim($this->_request->search_text);
		
		$area = $this->_request->area;
		$city = $this->_request->city;
		
		$this->view->all_cities = $this->_request->all_cities;
		
		$map_list = $m->getMapList($filter, $search, $area, $city);
		
		if ( count($map_list) ) {
			foreach ( $map_list as $k => $item ) {
				if ( $item->escort_id ) {
					$map_list[$k]->url = $this->view->getLink('profile', array('showname' => $item->showname, 'escort_id' => $item->escort_id));
					$e_item = new Model_Escort_PhotoItem($item);
					$map_list[$k]->photo = $e_item->getUrl('w68');
				} else {
					$map_list[$k]->url = $this->view->getLink('agency', array('slug' => $item->club_slug, 'id' => $item->agency_id));
				}
			}
		}
		
		echo json_encode(array('data' => $map_list, 'c' => $this->_request->c));
		die;
	}
	
	public function filterAction()
	{
		$this->view->layout()->disableLayout();
		
		$m = new Model_ClubDirectory();
		
		if (strlen(trim($this->_request->search_text))) {
			$this->view->search_text = trim($this->_request->search_text);
		}
		
		$area = $this->view->area = $this->_request->area;
		$city = $this->view->city = $this->_request->city;
		
		if ( $city && ! $area ) {
			$area = $this->view->area = $m->getAreaByCitySlug($city);
		}
		
		$this->view->filter = $this->_request->getParam('filter', array());
		
		$this->view->listing_type = ($this->_request->listing_type == 'map') ? false : true;
		
		$geoData = Cubix_Geoip::getClientLocation();
		
		/*$geoData['latitude'] = 47.4167;
		$geoData['longitude'] = 8.4333;
		$geoData['country_iso'] = 'CH';
		$geoData['city'] = 'Adliswil';*/
		
		if ( isset($geoData['city']) ) {
			
			$curr_city = $m->isCityExist($geoData['city']);
			
			if ( $curr_city ) {
				$this->view->current_city = $curr_city;
				
				$this->view->n_cities = $m->getNearestCities($geoData['latitude'], $geoData['longitude'], 3, $curr_city->city_id, $area);
			} else if ( $geoData['country_iso'] == 'CH' ) {
				$m->logGeoData($geoData);
			}
		}
		
		
		$this->view->all_cities = $this->_request->all_cities;
		
		
		
		if ( $this->_request->isPost() ) {
			$this->view->post = true;
		}
	}
}
