<?php

class Model_Plugin_I18n extends Zend_Controller_Plugin_Abstract
{
	public function __construct()
	{
		
	}

	public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request)
	{
		if ( ! isset($_COOKIE['banner_popup_show']) ) {
			setcookie('banner_popup_show', '1', 0, '/', '.and6.at');
		}
		
		$user = Model_Users::getCurrent();
		
		if ( ! is_null($user) ) {
			Model_Users::setLastRefreshTime($user);
		}

		$this->rememberMeSignIn();
	}

	public function rememberMeSignIn()
	{
		$cookie_name = 'signin_remember_' . Cubix_Application::getId();

		if ( isset($_COOKIE[$cookie_name]) ) {
			$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
			$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
			$key = '_-_SignInRememberMe_-_' . Cubix_Application::getId();

			$crypt_login_data = $_COOKIE[$cookie_name];
			$crypt_login_data = base64_decode($crypt_login_data);
			$login_data = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $crypt_login_data, MCRYPT_MODE_ECB, $iv);

			$login_data = @unserialize($login_data);
			
			try {
				if ( is_array($login_data) ) {

					$model = new Model_Users();

					if ( ! $user = $model->getByUsernamePassword($login_data['username'], $login_data['password']) ) {
						$this->view->errors['username'] = 'Wrong username/password combination';
						return;
					}
					//print_r($user);die;
					if ( STATUS_ACTIVE != $user->status ) {
						$this->view->errors['username'] = 'Your account is not active yet';
						return;
					}

					Zend_Session::regenerateId();
					$user->sign_hash = md5(rand(100000, 999999) * microtime(true));
					Model_Users::setCurrent($user);

					/*set client ID*/
					Model_Reviews::createCookieClientID($user->id);
					/**/


					/*if ( ! Models_Auth::getAuth()->hasIdentity() ) {
						Models_Auth::auth($login_data['username'], $login_data['password'], Estate_Application::ID);
					}*/
				}
			}
			catch (Exception $e) {

			}
		}
	}
	
	public function preDispatch(Zend_Controller_Request_Abstract $request)
    {    	
    	if ( $request->getControllerName() != 'escorts' || $request->getActionName() != 'index' ) {    		
    		header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			die;
    	}


		if ( ! defined('IS_CLI') ) {
			$host = $request->getHttpHost();
			$host = explode('.', $host);
			$region = $subdomain = $host[0];

			if ( $region == 'www' ) {
				$region = 'austria';
			}

			$request->setParam('region', $region);
		}
		/*$config = Zend_Registry::get('system_config');

		if ($request->getModuleName() != 'api' )
		{
			if ( $request->getControllerName() != 'private' && $request->getActionName() != 'activate' )
			{
				if ( $config['showSplash'] )
				{
					if ( ( ! isset($_COOKIE['18']) || ! $_COOKIE['18'] ) &&
						($request->getControllerName() != 'index' && $request->getActionName() != 'splash') &&
						($request->getControllerName() != 'error' && $request->getActionName() != 'error') )
					{

						$then = $_SERVER['REQUEST_URI'];
						if ( $_SERVER['REQUEST_URI'] == '' )
							$then = '/';

						header('HTTP/1.1 301 Moved Permanently');
						header('Location: /splash?then=' . urlencode($then));
						die;
					}
				}
				elseif ( ($request->getControllerName() == 'index' && $request->getActionName() == 'splash') )
				{
					header('HTTP/1.1 301 Moved Permanently');
					header('Location: /');
					die;
				}
			}
		}*/

		/*if ( $_SERVER['HTTP_HOST'] != 'www.6annonce.com' && 
				($request->getControllerName() != 'redirect' ||
				($request->getControllerName() != 'escorts' && $request->getActionName() != 'profile')) &&
				($request->getModuleName() != 'api') ) {
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: http://www.6annonce.com' . $_SERVER['REQUEST_URI']);
			exit;
		}*/
		
		//1 - disable chat
		//2 - node chat
		if ( ! $request->getParam('chatVersion') ) {
			$request->setParam('chatVersion', 2);
		}
    }

	public function routeShutdown($request)
	{
		if ( isset($_GET['debug_mode']) ) {
			//var_dump($GLOBALS);
			die('debug_mode');
		}
		$db = Zend_Registry::get('db');
		
		$lang_id = $request->lang_id;
		
		$req = $request->req;
		
		// Redirect to region by Browser language
		if( ! isset($_SERVER['HTTP_X_REQUESTED_WITH']) && empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest' && ! defined('IS_CLI')) 
		{			
			$host = $request->getHttpHost();
			$host = explode('.', $host);
			$subdomain = $host[0];
			if ( $this->_request->getControllerName() == 'escorts' && $this->_request->getActionName() != 'search' && ! preg_match('#ticino|romandie|www#', $subdomain) && ! preg_match("#escort#", $_SERVER['REQUEST_URI']) ) 
			{				
				$l = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
				
				$subdomain == 'www';
				
				if ( $l == "fr" ) {
					$subdomain = 'romandie';
				}
				else if ( $l == "it" ) {
					$subdomain = 'ticino';
				}
				
				$host[0] = $subdomain;
				$host = implode('.', $host);
				header('HTTP/1.1 301 Moved Permanently');
				header('Location: ' . $host);
				
			}
		}
		
		$default_lang_id = 'de';
		if (preg_match('#ticino#', $req) ) {
			$default_lang_id = 'it';
		}
		else if (preg_match('#romandie#', $req) ) {
			$default_lang_id = 'fr';
		}
		
		// Set default language by escort region
		if ( ! defined('IS_CLI') && Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName() == 'escort-profile-def' ) {
			$escort_id = $request->escort_id;
			$showname = $request->escortName;
			
			$region_slug = Model_EscortsV2::getRegionSlugById($escort_id);
			
			if ('ticino' == $region_slug) {
				$default_lang_id = 'it';
			}
			else if ('romandie' == $region_slug) {
				$default_lang_id = 'fr';
			}
			else if ('austria' == $region_slug) {
				$default_lang_id = 'de';
			}
		}
		
		$cook_ln = $request->ln;
		if (  $cook_ln ) {
			if ( $cook_ln != $default_lang_id ) {
				setcookie('ln', $cook_ln, time() + 31536000, '/', '.and6.at');
				$_COOKIE['ln'] = $cook_ln;
			}
			else {
				setcookie('ln', "", time() - 3600, '/', '.and6.at');
				$_COOKIE['ln'] = null;
			}
		}
		
		$lngs = array('en', 'de', 'it', 'fr');
		
		if ( isset($_COOKIE['ln']) && $_COOKIE['ln'] ) {
			
			if ( ! in_array($_COOKIE['ln'], $lngs) ) {
				$default_lang_id = 'de';
				setcookie('ln', "", time() - 3600, '/', '.and6.at');
				$_COOKIE['ln'] = null;
			}
			else {			
				$default_lang_id = $_COOKIE['ln'];
			}
		}
		
		
		//$default_lang_id = 'de';//$db->fetchOne('SELECT lang_id FROM application_langs WHERE is_default = 1');;

		if ( ! $default_lang_id ) {
			$this->_request->setParam('error_msg', 'This Site is temporary off');
			$this->_request->setModuleName('default');
			$this->_request->setControllerName('error');
			$this->_request->setActionName('no-default-lang');
			return;
		}
		
		/*if ( ! is_null($lang_id) && $lang_id == $default_lang_id && ! $request->isPost() ) {
			$uri = trim($_SERVER['REQUEST_URI'], '/');
			$uri = explode('/', $uri);
			array_shift($uri);
			$uri = implode('/', $uri);
			
			header('HTTP/1.1 301 Moved Permamently');
			header('Location: /' . $uri);
			die;
		}*/
		/*
		$cache = Zend_Registry::get('cache');
		$cache_key =  $lang_id . '_application_get_true_from_app_langs__';
		if ( ! $is_lng_exist = $cache->load($cache_key) ) {
			$is_lng_exist = $db->fetchOne('SELECT TRUE FROM application_langs WHERE application_id = ? AND lang_id = ?', array(Cubix_Application::getId(), $lang_id));
			$cache->save($is_lng_exist, $cache_key, array());
		}
		
		if ( is_null($lang_id) || ! $is_lng_exist ) {
			//$lang_id = $db->fetchOne('SELECT lang_id FROM application_langs ORDER BY FIELD(lang_id, "en", "de") LIMIT 1');
			
			$request->setParam('lang_id', $default_lang_id);
			
		}*/
		
		$request->setParam('lang_id', $default_lang_id);
		
		if ( $this->_request->ajax_lng ) {
			$request->setParam('lang_id', $this->_request->ajax_lng);
		}

		Cubix_I18n::init();
		
		require 'Cubix/defines.php';
		Zend_Registry::set('definitions', $DEFINITIONS);
	}
}
