<?php

class Model_Agencies extends Cubix_Model
{
	protected $_table = 'agencies';
	protected $_itemClass = 'Model_AgencyItem';
	
	public static function hit($agency_id)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		
		$result = $client->call('Agencies.updateHitsCount', array($agency_id));
		
		return $result;
	}
	
	/*public function getIdByName($name)
	{
		$sql = "
			SELECT id
			FROM agencies a
			WHERE name = ?
		";
		
		$agency = parent::_fetchRow($sql, $name);
		
		if ( $agency )
			return $agency->id;
		
		return null;
	}*/
	
	public function getIdByName($name)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		
		$result = $client->call('Agencies.getByName', array($name));
		
		return $result;
	}
	
	/*public function get($agency_id)
	{
		$sql = "
			SELECT a.id, a.user_id, a.name, a.phone, a.phone_instructions, a.email, a.web, a.last_modified, a.logo_hash, a.logo_ext, u.application_id, u.date_registered AS creation_date, a.hit_count
			FROM agencies a
			LEFT JOIN users u ON u.id = a.user_id
			WHERE a.id = ?
		";
		
		return parent::_fetchRow($sql, $agency_id);
	}*/
	
	public function get($agency_id)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		
		$result = $client->call('Agencies.get', array($agency_id));
		
		return $result;
	}
	
	public function getByUserId($user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$agency = $client->call('Agencies.getByUserId', array($user_id));
		
		$agency = new Model_AgencyItem($agency);
		
		return $agency;
	}
	
	public function getAll()
	{
		
	}

	public function save($agency)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$agency_data = array(
			'user_id' => $agency->user_id,
			'name' => $agency->name,
			'country_id' => $agency->country_id,
			'slug' => $this->clear_string($agency->name)
 		);
		$agency_id = $client->call('Agencies.save', array($agency_data));
		$agency->setId($agency_id);
		
		return $agency;
	}
	
	private function clear_string($str)
	{
		$str = strtolower($str);

		$str = preg_replace("#[^0-9a-z]#", '-', $str);

		$str = preg_replace("#\s+#", '-', $str);

		$str = ___clean_str($str);

		return $str;
	}
	
	public function loadAgencyPhotos($agency_id, $page = 1, $per_page = 4, &$count = null)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		
		$photos = $client->call('Agencies.getPhotos', array($agency_id, $page, $per_page));
		$count = $photos['count'];
		
		$images = new Cubix_Images();
		
		$phs = $photos['list'];
		
		foreach ($phs as $k => $photo)
		{
			$image = array('hash' => $photo['hash'], 'ext' => $photo['ext']);
			$image = new Cubix_Images_Entry($image);
			$image->setCatalogId('agencies');
			$image_url = $images->getAgencyUrl($image, $agency_id);
			$phs[$k]['image_url'] = $image_url;
		}
		
		return $phs;
	}
}
