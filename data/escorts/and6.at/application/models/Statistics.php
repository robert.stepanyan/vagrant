<?php

class Model_Statistics extends Cubix_Model
{
	public static function getTotalCount($gender = null, $is_agency = null, $is_tour = null, $is_upcoming = null)
	{
		$where = array(
			'eic.gender = ?' => $gender,
			'eic.is_agency = ?' => $is_agency,
			'eic.is_tour = ?' => $is_tour,
			'eic.is_upcoming = ?' => $is_upcoming
		);

		$where = self::getWhereClause($where, true);

		$lng = Cubix_I18n::getLang();
		$sql = '
			SELECT
				COUNT(DISTINCT(eic.escort_id)) AS escort_count
			FROM escorts_in_cities eic
			INNER JOIN cities ct ON ct.id = eic.city_id
			LEFT JOIN regions r ON r.id = ct.region_id
			' . (!is_null($where) ? 'WHERE ' . $where : '') . '
		';

		return self::db()->fetchOne($sql);
	}

	static public function getCountries($gender = null, $is_agency = null, $is_tour = null, $is_upcoming = null)
	{
		$where = array(
			'eic.gender = ?' => $gender,
			'eic.is_agency = ?' => $is_agency,
			'eic.is_tour = ?' => $is_tour,
			'eic.is_upcoming = ?' => $is_upcoming
		);

		$where = self::getWhereClause($where, true);

		$lng = Cubix_I18n::getLang();
		$sql = '
			SELECT
				eic.country_id, COUNT(DISTINCT(eic.escort_id)) AS escort_count,
				c.title_' . $lng . ' AS country_title, c.iso
			FROM escorts_in_countries eic
			INNER JOIN countries c ON c.id = eic.country_id
			' . (!is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.country_id;
		';

		$result = self::db()->fetchAll($sql);
		usort($result, array('Model_Statistics', '_orderByEscortCount'));

		return $result;
	}

	public static function getCities($region_id = null, $gender = null, $is_agency = null, $is_tour = null, $is_upcoming = null,$wheretmp = null, $limit = null, $region_slug = null)
	{
		$where = array(
				'eic.region_id = ?' => $region_id,
				'(eic.gender = ? OR eic.gender = ?)' => array($gender, GENDER_TRANS),
				'eic.is_agency = ?' => $is_agency,
				'eic.is_tour = ?' => $is_tour,
				'eic.is_upcoming = ?' => $is_upcoming,
				'r.slug = ?' => $region_slug
		);

		if($wheretmp){
			$where = array_merge($wheretmp,$where);
		}

		$where = self::getWhereClause($where, true);

		$lng = Cubix_I18n::getLang();
		$sql = '
				SELECT
						eic.city_id, COUNT(DISTINCT(eic.escort_id)) AS escort_count,
						ct.id AS city_slug, ct.title_' . $lng . ' AS city_title, ct.slug AS city_slug, ct.url_slug AS city_url_slug,
						r.title_' . $lng . ' AS region_title, r.slug AS region_slug, c.iso AS country_iso
				FROM escorts_in_cities eic
				INNER JOIN cities ct ON ct.id = eic.city_id
				LEFT JOIN regions r ON r.id = ct.region_id
				INNER JOIN countries c ON c.id = ct.country_id
				' . (!is_null($where) ? 'WHERE ' . $where : '') . '
				GROUP BY eic.city_id
				ORDER BY ct.order_id ASC
		';
		//echo $sql;
		if ( $limit ) {
			$sql .= " LIMIT " . $limit;
		}

		self::db()->query('SET NAMES utf8');
		$result = self::db()->fetchAll($sql);
		if ( $region_slug ) {
			usort($result, array('Model_Statistics', '_orderByEscortCount'));
		}

		return $result;
	}
	
	public static function getFakeCitiesForMobile($region_id = null, $gender = null, $is_agency = null, $is_tour = null, $is_upcoming = null,$wheretmp = null, $limit = null)
	{
		$where = array(
			'ct.region_id = ?' => $region_id,
			'(e.gender = ? OR e.gender = ?)' => array($gender, GENDER_TRANS),
			/*'eic.is_agency = ?' => $is_agency,
			'eic.is_tour = ?' => $is_tour,
			'eic.is_upcoming = ?' => $is_upcoming*/
		);

        if( $wheretmp ) {
            $where = array_merge($wheretmp, $where);
        }
		
		$where = self::getWhereClause($where, true);

		$lng = Cubix_I18n::getLang();
		$sql = '
			SELECT
				e.f_city_id, fct.title_' . $lng . ' AS fake_city_title, fct.slug AS fake_city_slug, COUNT(DISTINCT(e.id)) AS escort_count,
				ct.id AS city_slug, ct.title_' . $lng . ' AS city_title, ct.slug AS city_slug, ct.url_slug AS city_url_slug,
				r.title_' . $lng . ' AS region_title, r.slug AS region_slug, c.iso AS country_iso
			FROM escorts e
			INNER JOIN f_cities fct ON fct.id = e.f_city_id
			INNER JOIN f_cities_area fca ON fca.f_city_id = fct.id
			INNER JOIN cities ct ON ct.id = fca.area_id
			LEFT JOIN regions r ON r.id = ct.region_id
			INNER JOIN countries c ON c.id = ct.country_id
			' . ( ! is_null($where ) ? 'WHERE ' . $where : '') . '
			GROUP BY ct.id
			ORDER BY ct.id ASC
		';
		//echo $sql;
		if ( $limit ) {
			$sql += " LIMIT " . $limit;
		}

		$result = self::db()->fetchAll($sql);
		//usort($result, array('Model_Statistics', '_orderByEscortCount'));

		return $result;
	}

	public static function getCitiesByCountry($gender = null, $is_agency = null, $is_tour = null, $is_upcoming = null, $region_slug = null)
	{
		$where = array(
			'(eic.gender = ? OR eic.gender = ?)' => array($gender, GENDER_TRANS),
			'eic.is_agency = ?' => $is_agency,
			'eic.is_tour = ?' => $is_tour,
			'eic.is_upcoming = ?' => $is_upcoming,
			'r.slug = ?' => $region_slug
		);

		$where = self::getWhereClause($where, true);

		/*$cache = Zend_Registry::get('cache');
		$cache_key = "cities_by_countries_app_" . Cubix_Application::getId() . "_" . $gender . $is_agency . $is_tour . $is_upcoming . $region_slug . "_cities";
		
		if ( ! $cities = $cache->load($cache_key) ) {*/
			
			$lng = Cubix_I18n::getLang();
			$sql = '
				SELECT
					eic.city_id, COUNT(DISTINCT(eic.escort_id)) AS escort_count,
					ct.id AS city_id, ct.title_' . $lng . ' AS city_title, ct.slug AS city_slug, ct.url_slug AS city_url_slug,
					r.title_' . $lng . ' AS region_title, ct.country_id, c.title_' . $lng . ' AS country_title
				FROM escorts_in_cities eic
				INNER JOIN cities ct ON ct.id = eic.city_id
				LEFT JOIN regions r ON r.id = ct.region_id
				INNER JOIN countries c ON c.id = ct.country_id
				' . (!is_null($where) ? 'WHERE ' . $where : '') . '
				GROUP BY eic.city_id
				ORDER BY ct.order_id;
			';

			$cities = self::db()->fetchAll($sql);
			
		/*	$cache->save($cities, $cache_key, array());
		}*/
		
		
		
		// divide cities array into array of arrays by city's country id
		$result = array();
		foreach ( $cities as $city ) {
			if ( ! isset($result[$city->country_id]) )
				$result[$city->country_id] = array();
			$result[$city->country_id][] = $city;
		}

		/*$cache_key = "cities_by_countries_app_" . Cubix_Application::getId() . "_" . $gender . $is_agency . $is_tour . $is_upcoming . $region_slug . "_countries";
		if ( ! $countries = $cache->load($cache_key) ) {*/
			// find cities for each country from $result array
			$countries = self::getCountries($gender, $is_agency, $is_tour, $is_upcoming);
			
			/*$cache->save($countries, $cache_key, array());
		}*/
		
		
		foreach ( $countries as &$country ) {
			if ( isset($result[$country->country_id])) {
				$country->cities = $result[$country->country_id];
			}
		}

		return $countries;
	}

	public static function getRegions($gender = null, $is_agency = null, $is_tour = null, $is_upcoming = null)
	{
		$where = array(
			'eic.gender = ?' => $gender,
			'eic.is_agency = ?' => $is_agency,
			'eic.is_tour = ?' => $is_tour,
			'eic.is_upcoming = ?' => $is_upcoming
		);

		$where = self::getWhereClause($where, true);

		$lng = Cubix_I18n::getLang();
		$sql = '
			SELECT
				COUNT(DISTINCT(eic.escort_id)) AS escort_count,
				r.title_' . $lng . ' AS region_title, r.slug AS region_slug
			FROM escorts_in_cities eic
			INNER JOIN cities ct ON ct.id = eic.city_id
			LEFT JOIN regions r ON r.id = ct.region_id
			' . (!is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.region_id;
		';

		$result = self::db()->fetchAll($sql);
		usort($result, array('Model_Statistics', '_orderByEscortCount'));

		return $result;
	}

	public static function getZones($city_id = null, $gender = null, $is_agency = null, $is_tour = null, $is_upcoming = null)
	{
		$where = array(
			'eic.city_id = ?' => $city_id,
			'eic.gender = ?' => $gender,
			'eic.is_agency = ?' => $is_agency,
			'eic.is_tour = ?' => $is_tour,
			'eic.is_upcoming = ?' => $is_upcoming
		);

		$where = self::getWhereClause($where, true);

		$lng = Cubix_I18n::getLang();
		$sql = '
			SELECT
				COUNT(DISTINCT(eic.escort_id)) AS escort_count,
				ct.title_' . $lng . ' AS city_title, ct.slug AS city_slug,
				cz.title_' . $lng . ' AS zone_title, cz.slug AS zone_slug
			FROM escorts_in_cityzones eic
			INNER JOIN cityzones cz ON cz.id = eic.cityzone_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			' . (!is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.cityzone_id;
		';

		$result = self::db()->fetchAll($sql);

		return $result;
	}

	public static function _orderByEscortCount($a, $b)
	{
		$a = (int) $a->escort_count; $b = (int) $b->escort_count;
		if ( $a == $b ) return 0;
		return $a < $b ? 1 : -1;
	}

	public static function ajaxSearchArray()
	{
		
	}
	
	public static function getFakeCities()
	{
		$lng = Cubix_I18n::getLang();
		$sql = '
			SELECT
				fc.id, fc.title_' . $lng . ' AS title, fc.slug AS slug, fc.zip
			FROM f_cities fc
			ORDER BY fc.title_en ASC
		';

		$result = self::db()->fetchAll($sql);

		return $result;
	}
	
	public static function getNewAraeaByOldTitle($old_area)
	{
		$sql = '
			SELECT
				c.id, c.slug AS area_slug, r.slug AS region_slug
			FROM cities c
			INNER JOIN regions r ON r.id = c.region_id
			WHERE c.old_title = ?
		';

		$result = self::db()->fetchRow($sql, array($old_area));

		return $result;
	}

	public static function getEscortStatistics($agency_id = null, $escort_id = null, $date_from = null, $date_to = null, $order_by = null, $order_dir = null) {
		$client = Cubix_Api::getInstance();
		$data = array();
		$data['agency_id'] = $agency_id;
		$data['escort_id'] = $escort_id;
		$data['date_from'] = $date_from;
		$data['date_to'] = $date_to;
		$data['order_by'] = $order_by;
		$data['order_dir'] = $order_dir;

//		var_Dump( $client->call('getStatistics', array( $data )) );
//		exit;
		return $client->call('getStatistics', array( $data ));
	}

	public static function updateReport( $escort_id, $status = null, $type = null, $email = null, $username = null ){
		$client = Cubix_Api::getInstance();
		$data = array();

		$data['escort_id'] = $escort_id;
		$data['status'] = $status;
		$data['type'] = $type;
		$data['email'] = $email;
		$data['username'] = $username;

		return $client->call('updateReport', array( $data ));
	}

	public static function getEscortReportStatus( $escort_id ){
		$client = Cubix_Api::getInstance();

		return $client->call('getReportStatus', array( $escort_id ));
	}
}
