<?php

class Model_Cities extends Cubix_Model 
{
	protected $_table = 'cities';

	public function getByCountry($id) 
	{
		$sql = '
			SELECT c.id, c.' . Cubix_I18n::getTblField('title') . ' as title
			FROM cities c
			WHERE country_id = ' . $id . '
			ORDER BY title
		';

		return parent::_fetchAll($sql);
	}
	
	public function getByCountries($ids) 
	{
		$ids_str = implode(',', $ids);
		$sql = '
			SELECT c.id, c.country_id, c.' . Cubix_I18n::getTblField('title') . ' as title
			FROM cities c
			WHERE country_id IN (' . $ids_str . ')
			ORDER BY title
		';

		return parent::_fetchAll($sql);
	}
	
	public function getByIds($ids)
	{
		return parent::_fetchAll('SELECT id, ' . Cubix_I18n::getTblField('title') . ' as title FROM cities WHERE id IN (' . $ids . ') ORDER BY title ASC');
	}
	
	public function getBySlug2($slug)
	{
		return parent::_fetchRow('SELECT * FROM cities WHERE slug = ?', array($slug));
	}
	
	public function getBySlug($slug)
	{
		return parent::_fetchRow('
			SELECT 
				c.id, c.' . Cubix_I18n::getTblField('title') . ' as title,
				r.id AS region_id, r.slug AS region_slug, c.slug
			FROM cities c
			INNER JOIN regions r ON r.id = c.region_id
			WHERE c.slug = ?', array($slug)
		);
	}
	
	public function getById($city_id)
	{
		return parent::_fetchRow('
			SELECT 
				c.id, c.slug AS city_slug, c.' . Cubix_I18n::getTblField('title') . ' as title,
				r.id AS region_id, r.slug AS region_slug
			FROM cities c
			INNER JOIN regions r ON r.id = c.region_id
			WHERE c.id = ?', array($city_id)
		);
	}
	
	public function getFakeCityBySlug($slug)
	{
		return parent::_fetchRow('SELECT id, ' . Cubix_I18n::getTblField('title') . ' as title FROM f_cities WHERE slug = ?', array($slug));
	}
}
