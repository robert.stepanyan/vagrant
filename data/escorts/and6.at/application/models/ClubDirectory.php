<?php

class Model_ClubDirectory extends Cubix_Model
{
	public function getList($filter = array(), $search = null, $coord = array(), $page = 1, $perPage = 20, &$count = null, $area = null, $city = null)
	{
		if( ! $page ) $page = 1;
		
		$limit = ' LIMIT ' . ($page - 1) * $perPage . ', ' . $perPage;
		$fields = '';
		$where = '';
		$order = '';
				
		if (array_key_exists("lat", $coord) && strlen($coord['lat']) > 0 && array_key_exists("lon", $coord) && strlen($coord['lon']) > 0)
			$fields = '
				((2 * 6371 * 
					ATAN2(
					SQRT(
						POWER(SIN((RADIANS(' . $coord['lat'] . ' - latitude))/2), 2) +
						COS(RADIANS(latitude)) *
						COS(RADIANS(' . $coord['lat'] . ')) *
						POWER(SIN((RADIANS(' . $coord['lon'] . ' - longitude))/2), 2)
					),
					SQRT(1-(
						POWER(SIN((RADIANS(' . $coord['lat'] . ' - latitude))/2), 2) +
						COS(RADIANS(latitude)) *
						COS(RADIANS(' . $coord['lat'] . ')) *
						POWER(SIN((RADIANS(' . $coord['lon'] . ' - longitude))/2), 2)
					))
					)
				)) AS distance 
			';
		else
			$fields = 'NULL AS distance ';
				
		$filter_criteria = array();
		
		if (!in_array(AGENCY_FILTER_CRITERIA_CLUB, $filter))
			$filter_criteria[] = "filter_criteria <> " . AGENCY_FILTER_CRITERIA_CLUB;
		
		if (!in_array(AGENCY_FILTER_CRITERIA_KONTAKTBAR, $filter))
			$filter_criteria[] = "filter_criteria <> " . AGENCY_FILTER_CRITERIA_KONTAKTBAR;
		
		if (!in_array(AGENCY_FILTER_CRITERIA_LAUFHAUS, $filter))
			$filter_criteria[] = "filter_criteria <> " . AGENCY_FILTER_CRITERIA_LAUFHAUS;
		
		if (!in_array(AGENCY_FILTER_CRITERIA_NIGHTCLUB, $filter))
			$filter_criteria[] = "filter_criteria <> " . AGENCY_FILTER_CRITERIA_NIGHTCLUB;
		
		if (strlen($search))
			$where .= " AND search_string LIKE '%{$search}%'";
			
		if (!in_array('working_girl', $filter)) {
			if ( count($filter_criteria) ) {
				$where .= "AND ((" . implode(' AND ', $filter_criteria) . ") AND escort_id IS NULL)";
			} else {
				$where .= 'AND (escort_id IS NULL)';
			}
		}
		else
			if ( count($filter_criteria) ) {
				$where .= "AND ((" . implode(' AND ', $filter_criteria) . ") OR escort_id IS NOT NULL)";
			} else {
				$where .= 'AND (escort_id IS NOT NULL OR filter_criteria IS NOT NULL )';
			}
			
		if (in_array('distance', $filter))
			$order = '(CASE WHEN distance IS NULL then 1 ELSE 0 END), distance, COALESCE(club_name, showname) ';
		else {
			$order = ' city_title_en ASC ';
			//$order = 'COALESCE(club_name, showname) ';
		}
		
		if ( $area ) {
			$where .= " AND area_slug = '{$area}' ";
		}
		
		if ( $city ) {
			$where .= " AND city_slug = '{$city}' ";
		}
		
		$sql = "
			SELECT /*SQL_CALC_FOUND_ROWS*/ *, 25 AS application_id, photo_hash AS hash, photo_ext AS ext, escort_id AS catalog_id, {$fields} 
			FROM club_directory
			WHERE 1 {$where}
			ORDER BY {$order}
			/*{$limit}*/
		";
		
		$items = $this->getAdapter()->query($sql)->fetchAll();
		//$count = $this->getAdapter()->fetchOne('SELECT FOUND_ROWS()');
		
		$ordered_array = array();
		if (in_array('distance', $filter)) {
			if ( count($items) ) {
				foreach ( $items as $k => $item ) {
					$ordered_array['distance'][] = $item;
				}
			}
		} else {
			if ( count($items) ) {
				foreach ( $items as $k => $item ) {
					$ordered_array[$item->city_slug][] = $item;
				}
			}
		}
		
		return $ordered_array;
	}
	
	public function getLogoUrl($logo_hash, $logo_ext)
	{
		if ( $logo_hash ) {
			$images_model = new Cubix_Images();

			return $images_model->getUrl(new Cubix_Images_Entry(array(
				'application_id' => APP_A6_AT,
				'catalog_id' => 'agencies',
				'size' => 'agency_thumb',
				'hash' => $logo_hash,
				'ext' => $logo_ext
			)));
		}
		
		return NULL;
	}
	
	public function getNearestCities($lat, $lon, $count = 3, $excl_city, $area)
	{
		$where = ' ';
		if ( $excl_city ) {
			$where .= ' AND fc.id <> ' . $excl_city . ' ';
		}
		
		if ( $area ) {
			$where .= ' AND c.slug = "' . $area . '" ';
		}
		
		$sql = 'SELECT 
					fc.id, fc.' . Cubix_I18n::getTblField('title') . ' AS title, fc.slug, fc.id, ((2 * 6371 * 
						ATAN2(
						SQRT(
							POWER(SIN((RADIANS(' . $lat . ' - fc.latitude))/2), 2) +
							COS(RADIANS(fc.latitude)) *
							COS(RADIANS(' . $lat . ')) *
							POWER(SIN((RADIANS(' . $lon . ' - fc.longitude))/2), 2)
						),
						SQRT(1-(
							POWER(SIN((RADIANS(' . $lat . ' - fc.latitude))/2), 2) +
							COS(RADIANS(fc.latitude)) *
							COS(RADIANS(' . $lat . ')) *
							POWER(SIN((RADIANS(' . $lon . ' - fc.longitude))/2), 2)
						))
						)
					)) AS distance 
				FROM f_cities fc
				INNER JOIN club_directory cd ON cd.city_id = fc.id
				
				INNER JOIN f_cities_area fca ON fca.f_city_id = fc.id
				INNER JOIN cities c ON c.id = fca.area_id

				WHERE 1 ' . $where . '
				GROUP BY fc.id
				ORDER BY (CASE WHEN distance IS NULL then 1 ELSE 0 END), distance
				LIMIT ?
			';
		
		return $this->getAdapter()->query($sql, $count)->fetchAll();
	}
	
	public function getMapList($filter = array(), $search = null, $area = null, $city = null)
	{
		$where = '';
				
		$filter_critaeria = array();
		
		if (!in_array(AGENCY_FILTER_CRITERIA_CLUB, $filter))
			$filter_criteria[] = "filter_criteria <> " . AGENCY_FILTER_CRITERIA_CLUB;
		
		if (!in_array(AGENCY_FILTER_CRITERIA_KONTAKTBAR, $filter))
			$filter_criteria[] = "filter_criteria <> " . AGENCY_FILTER_CRITERIA_KONTAKTBAR;
		
		if (!in_array(AGENCY_FILTER_CRITERIA_LAUFHAUS, $filter))
			$filter_criteria[] = "filter_criteria <> " . AGENCY_FILTER_CRITERIA_LAUFHAUS;
		
		if (!in_array(AGENCY_FILTER_CRITERIA_NIGHTCLUB, $filter))
			$filter_criteria[] = "filter_criteria <> " . AGENCY_FILTER_CRITERIA_NIGHTCLUB;
		
		if (strlen($search))
			$where .= " AND search_string LIKE '%{$search}%'";
			
		if (!in_array('working_girl', $filter)) {
			if ( count($filter_criteria) ) {
				$where .= "AND ((" . implode(' AND ', $filter_criteria) . ") AND escort_id IS NULL)";
			} else {
				$where .= 'AND (escort_id IS NULL)';
			}
		}
		else
			if ( count($filter_criteria) ) {
				$where .= "AND ((" . implode(' AND ', $filter_criteria) . ") OR escort_id IS NOT NULL)";
			} else {
				$where .= 'AND (escort_id IS NOT NULL OR filter_criteria IS NOT NULL )';
			}
		
		if ( $area ) {
			$where .= " AND area_slug = '{$area}' ";
		}
		
		if ( $city ) {
			$where .= " AND city_slug = '{$city}' ";
		}	
			
		$sql = '
			SELECT 
				*, ' . Cubix_I18n::getTblField('city_title') . ' AS city_title, ' . Cubix_I18n::getTblField('area_title') . ' AS area_title,
				25 AS application_id, photo_hash AS hash, photo_ext AS ext, escort_id AS catalog_id
			FROM club_directory
			WHERE latitude IS NOT NULL AND longitude IS NOT NULL ' . $where . '
		';
			
		//echo $sql;
		
		$items = $this->getAdapter()->query($sql)->fetchAll();
		
		return $items;
	}
	
	public function isCityExist($geo_city)
	{
		if ( ! $geo_city ) return false;
		
		return $this->getAdapter()->fetchRow('
			SELECT 
				fc.id AS city_id, fc.slug AS city_slug, fc.' . Cubix_I18n::getTblField('title') . ' AS city_title,
				c.id AS area_id, c.slug AS area_slug, c.' . Cubix_I18n::getTblField('title') . ' AS area_title
			FROM f_cities fc 
			LEFT JOIN f_cities_area fca ON fca.f_city_id = fc.id
			LEFT JOIN cities c ON c.id = fca.area_id
			WHERE fc.title_en = ?
		', array($geo_city));
	}
	
	public function getCityBySlug($city_slug)
	{		
		return $this->getAdapter()->fetchRow('
			SELECT 
				fc.id AS city_id, fc.slug AS city_slug, fc.' . Cubix_I18n::getTblField('title') . ' AS city_title
			FROM f_cities fc 			
			WHERE fc.slug = ?
		', array($city_slug));
	}
	
	public function logGeoData($geo_data)
	{
		$this->getAdapter()->insert('geo_data_log', array('geo_data' => serialize($geo_data)));
	}
	
	public function getAreaByCitySlug($city_slug)
	{
		$sql = '
			SELECT 				
				c.slug AS area_slug
			FROM f_cities fc 
			LEFT JOIN f_cities_area fca ON fca.f_city_id = fc.id
			LEFT JOIN cities c ON c.id = fca.area_id
			WHERE fc.slug = ?
		';
		
		return $this->getAdapter()->fetchOne($sql, array($city_slug));
	}
	
	public static function getAreas($region_slug = null)
	{
		$where = null;
		
		if ( $region_slug ) {
			$where = ' r.slug = "' . $region_slug . '"';
		}
		
		$lng = Cubix_I18n::getLang();
		$sql = '
			SELECT
				cd.city_id, COUNT(DISTINCT(cd.id)) AS escort_count,
				ct.id AS area_id, ct.title_' . $lng . ' AS area_title, ct.slug AS area_slug, ct.url_slug AS area_url_slug,
				r.title_' . $lng . ' AS region_title, ct.country_id, c.title_' . $lng . ' AS country_title
			FROM club_directory cd
			INNER JOIN cities ct ON ct.id = cd.area_id
			LEFT JOIN regions r ON r.id = ct.region_id
			INNER JOIN countries c ON c.id = ct.country_id
			' . (!is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY ct.id
			ORDER BY ct.order_id;
		';
		
		$areas = self::db()->fetchAll($sql);
		
		return $areas;
	}
}
