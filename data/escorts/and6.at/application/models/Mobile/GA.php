<?php

class Model_Mobile_GA extends Cubix_Model_Item
{
	const GA_ACCOUNT = "MO-11411323-1";
	const GA_PIXEL = "/ga.php";

	public static function googleAnalyticsGetImageUrl()
	{
		return "";
		$url = "";
		$url .= self::GA_PIXEL . "?";
		$url .= "utmac=" . self::GA_ACCOUNT;
		$url .= "&utmn=" . rand(0, 0x7fffffff);

		$referer = $_SERVER["HTTP_REFERER"];
		$query = $_SERVER["QUERY_STRING"];
		$path = $_SERVER["REQUEST_URI"];

		if (empty($referer)) {
		$referer = "-";
		}
		$url .= "&utmr=" . urlencode($referer);

		if (!empty($path)) {
		$url .= "&utmp=" . urlencode($path);
		}

		$url .= "&guid=ON";

		return str_replace("&", "&amp;", $url);
	}

}
