<?php

class Model_SupportItem extends Cubix_Model_Item
{
	/**
	 *
	 * @var Cubix_Api
	 */
	protected $client;

	public function __construct($rowData)
	{
		parent::__construct($rowData);
		$this->client = Cubix_Api::getInstance();
	}

	

	/**
	 * Need this func for ability to serialize this object
	 *
	 * @author GuGo
	 */
	public function __wakeup()
	{
		$this->setAdapter(Model_Users::getAdapter());
		$this->client = Cubix_Api::getInstance();
	}
}
