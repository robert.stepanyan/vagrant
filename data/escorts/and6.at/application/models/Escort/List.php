<?php

class Model_Escort_List extends Cubix_Model
{
	public static function getMainPremiumSpot()
	{
		$lng = Cubix_I18n::getLang();

		if (date('G') < 2)
			$date = 2;
		else
			$date = date('G');

		$sql = '
			SELECT
				/* escorts table */
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id,
				e.rates, e.incall_type, e.outcall_type, e.is_new, e.verified_status,
				e.photo_hash, e.photo_ext, e.photo_status,
				e.products, e.slogan, e.date_last_modified,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.is_suspicious,e.comment_count, e.review_count,
				
				ct.title_' . $lng . ' AS city,

				/* seo_entity_instances table */
				IF (sei.primary_id IS NULL, se.title_' . $lng . ', sei.title_' . $lng . ') AS alt,

				IF (e.available_24_7 = 1 OR (e.is_now_open = 1 AND e.close_time >= "' . $date . '"), 1, 0) AS is_late_night,
				/* misc */
				1 AS is_premium, ' . Cubix_Application::getId() . ' AS application_id, e.hh_is_active, e.is_online, 
				e.about_' . $lng . ' AS about, UNIX_TIMESTAMP(e.date_last_modified) AS date_last_modified
			FROM escorts e
			INNER JOIN escorts_in_cities eic ON eic.escort_id = e.id AND eic.is_base = 1
			INNER JOIN cities ct ON ct.id = eic.city_id
			LEFT JOIN seo_entity_instances sei ON sei.primary_id = e.id
			LEFT JOIN seo_entities se ON (se.id = sei.entity_id) OR (se.application_id = 2 AND se.slug = "escort")
			WHERE
				eic.gender = ' . GENDER_FEMALE . ' AND e.is_main_premium_spot = 1
			GROUP BY e.id
		';

		$cache = Zend_Registry::get('cache');
		$cache_key = 'main_premium_spot_' . Cubix_Application::getId() . '_lang_' . Cubix_I18n::getLang();

		if ( ! $escorts = $cache->load($cache_key) ) {
			
			$escorts = self::db()->fetchAll($sql);

			$cache->save($escorts, $cache_key, array());
		}

		// <editor-fold defaultstate="collapsed" desc="Make Escort Alt for SEO">
		$tpl_data = array(
			'app_title' => Cubix_Application::getById()->title
		);

		foreach ( $escorts as $i =>  $escort ) {
			$additional = array(
				'showname' => $escort->showname,
				'city' => $escort->city
			);

			foreach ( $tpl_data + $additional as $tpl_var => $tpl_value ) {
				$escorts[$i]->alt = str_replace('%' . $tpl_var . '%', $tpl_value, $escorts[$i]->alt);
			}
		}
		// </editor-fold>

		shuffle($escorts);

		// <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
		$sid = 'sedcard_paging_' . Cubix_Application::getId();
		$sess = new Zend_Session_Namespace($sid);

		$sess->main_premium_spot = array();
		foreach ( $escorts as $escort ) {
			$sess->main_premium_spot[] = $escort->showname;
		}
		// </editor-fold>


		return $escorts;
	}

	public static function getFiltered($filter, $ordering = 'random', $page = 1, $page_size = 50, &$count = 0, $sess_name = false, $region_slug = 'austria')
	{
		$lng = Cubix_I18n::getLang();

		if (date('G') < 2)
			$date = 2;
		else
			$date = date('G');

		if ( 'price' == substr($ordering, 0, 5) ) {
			$filter[] = 'e.incall_price IS NOT NULL';
		}
		
		// Only escorts with base city (or city tour) will be shown in this list
		if ( ! isset($filter['ct.id = ?']) && ! isset($filter['r.slug = ?']) ) {
			$filter[] = 'eic.is_base = 1';
		}
		else {
			$filter[] = 'eic.is_upcoming = 0';
		}
//print_r($filter);
		
		$order = self::_mapSorting($ordering);

		$page = (int) $page;
		if ( $page < 1 ) $page = 1;
		
		// GOTD selection
		$gotd_escort = false;
		$exclude = ' ';
		//if ( $page == 1 ) {
			
			$in_city = false;
			if ( isset($filter['ct.id = ?']) ) {
				$in_city = true;
				$city_id = $filter['ct.id = ?'];
			}
				
			$gender = GENDER_FEMALE;
			if ( isset($filter['eic.gender = 3']) ) {
				$gender = GENDER_TRANS;
			}
			else if ( isset($filter['eic.gender = 2']) ) {
				$gender = GENDER_MALE;
			}
			
			if ( $in_city ) 
			{
				$g_where = ' AND ct.id = ? ';
				$bind = array($city_id);
				if ( $city_id == 45 || $city_id == 17 ) {
					$g_where = ' AND (ct.id = ? OR ct.id = ?) ';
					$bind = array(45, 17);
				}
				else if ( $city_id == 23 || $city_id == 25 || $city_id == 26 ) {
					$g_where = ' AND (ct.id = ? OR ct.id = ? OR ct.id = ?) ';
					$bind = array(23, 25, 26);
				}
				else if ( $city_id == 22 || $city_id == 21 ) {
					$g_where = ' AND (ct.id = ? OR ct.id = ?) ';
					$bind = array(22, 21);
				}
				$sqlGOTDinCity = '
					SELECT
						e.id, e.showname, e.age,
						e.hit_count,
						ct.title_' . $lng . ' AS city,
						e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,
							
						epg.hash AS photo_hash_g,
						epg.ext AS photo_ext_g,
						epg.status AS photo_status_g,

						1 AS is_gotd, e.gotd_city_id, fct.title_' . $lng . ' AS f_city, afct.title_' . $lng . ' AS af_city, fct.id AS f_city_id, afct.id AS af_city_id
					FROM escorts e
					INNER JOIN cities ct ON ct.id = e.gotd_city_id
					INNER JOIN regions r ON r.id = ct.region_id
					LEFT JOIN f_cities fct ON fct.id = e.f_city_id
					LEFT JOIN f_cities afct ON afct.id = e.a_fake_city_id
					LEFT JOIN escort_photos epg ON epg.escort_id = e.id AND epg.gotd = 1
					WHERE FIND_IN_SET(15, e.products) > 0 ' . $g_where . ' AND (e.gender = ' . $gender . ' OR e.gender = ' . GENDER_TRANS . ') AND r.slug = "' . $region_slug . '"
				';

				$gotd_escort = self::db()->fetchRow($sqlGOTDinCity, $bind);
				
				// Get random GOTD
				if ( ! $gotd_escort ) {
					$sqlRandomGOTD = '
						SELECT
							e.id, e.showname, e.age,
							e.hit_count,
							ct.title_' . $lng . ' AS city,
							e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,

							epg.hash AS photo_hash_g,
							epg.ext AS photo_ext_g,
							epg.status AS photo_status_g,

							1 AS is_gotd, e.gotd_city_id, fct.title_' . $lng . ' AS f_city, afct.title_' . $lng . ' AS af_city, fct.id AS f_city_id, afct.id AS af_city_id
						FROM escorts e				
						INNER JOIN cities ct ON ct.id = e.gotd_city_id
						INNER JOIN regions r ON r.id = ct.region_id
						LEFT JOIN f_cities fct ON fct.id = e.f_city_id
						LEFT JOIN f_cities afct ON afct.id = e.a_fake_city_id
						LEFT JOIN escort_photos epg ON epg.escort_id = e.id AND epg.gotd = 1
						WHERE FIND_IN_SET(15, e.products) > 0 AND (e.gender = ' . $gender . ' OR e.gender = ' . GENDER_TRANS . ') AND r.slug = "' . $region_slug . '"
						/*ORDER BY e.ordering*/
					';

					$gotd_escort = self::db()->fetchAll($sqlRandomGOTD);
					
					if ( $gotd_escort ) {
						shuffle($gotd_escort);

						$gotd_escort = $gotd_escort[0];
					}
				}

				if ( $gotd_escort ) {
					
					$photo_hash_g = $gotd_escort->photo_hash_g;
					$photo_ext_g = $gotd_escort->photo_ext_g;
					$photo_status_g = $gotd_escort->photo_status_g;

					if ( $photo_hash_g ) {
						$gotd_escort->photo_hash = $photo_hash_g;
						$gotd_escort->photo_ext = $photo_ext_g;
						$gotd_escort->photo_status = $photo_status_g;
					}
					
					if ( $page == 1 )
						$page_size = $page_size - 1;
					
					$count = 1;
					$exclude = ' AND e.id <> ' . $gotd_escort->id . ' ';
				}
			}			
		//}
		
		$start = ($page_size * ($page - 1));
		if ( $gotd_escort && $page > 1 ) {
			$start -= 1;
			unset($gotd_escort);
		}
		
		$limit = $start . ', ' . $page_size;
		
		$j = "";
		$f = "";
		if ( isset($filter['f.user_id = ?']) ) {
			$j = " INNER JOIN favorites f ON f.escort_id = e.id ";
			$f = " , f.user_id AS fav_user_id ";
		}
		
		if ( isset($filter['eic.gender = 2']) ) {	
			unset($filter['eic.gender = 2']);
			$filter['(eic.gender = 2 OR eic.gender = 3)'] = array();
		}
		
		if ( isset($filter['eic.gender = 1']) ) {	
			unset($filter['eic.gender = 1']);
			$filter['(eic.gender = 1 OR eic.gender = 3)'] = array();
		}
		
		/*if ( isset($filter['e.gender = 2']) ) {	
			unset($filter['e.gender = 2']);
			$filter['(e.gender = 2 OR e.gender = 3)'] = array();
		}*/
		
		if ( isset($filter['e.gender = ?']) ) {	
			unset($filter['e.gender = ?']);
			$filter['(e.gender = 1 OR e.gender = 3)'] = array();
		}
		$where = self::getWhereClause($filter, true);
		//$order = str_replace('eic.is_premium', 'FIND_IN_SET(15, e.products)', $order);
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id,
				UNIX_TIMESTAMP(e.date_activated) AS date_activated,
				ct.title_' . $lng . ' AS city, e.rates, e.incall_type, e.outcall_type, e.is_new,
				e.verified_status, e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.comment_count, e.review_count,

				IF (sei.primary_id IS NULL, se.title_' . $lng . ', sei.title_' . $lng . ') AS alt,
				IF (e.available_24_7 = 1 OR (e.is_now_open = 1 AND e.close_time >= "' . $date . '"), 1, 0) AS is_late_night,
				e.hit_count, e.slogan, e.incall_price,
				e.date_last_modified,
				/*IF(eic.is_tour = 1 AND eic.is_premium = 1, 0, eic.is_premium) AS is_premium,*/ 
				IF(eic.is_tour = 1, 0, eic.is_premium) AS is_premium,
				e.hh_is_active, e.is_suspicious, e.is_online ' . $f . ',
				/*IF( FIND_IN_SET(15, e.products), 1, 0 ) AS is_gotd,*/ e.about_' . $lng . ' AS about, UNIX_TIMESTAMP(e.date_last_modified) AS date_last_modified,
				0 AS is_gotd, fct.title_' . $lng . ' AS f_city, afct.title_' . $lng . ' AS af_city, fct.id AS f_city_id, afct.id AS af_city_id
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			LEFT JOIN f_cities fct ON fct.id = e.f_city_id
			LEFT JOIN f_cities afct ON afct.id = e.a_fake_city_id
			LEFT JOIN regions r ON r.id = eic.region_id
			' . $j . '
			LEFT JOIN escort_cityzones ecz ON ecz.escort_id = eic.escort_id
			LEFT JOIN cityzones cz ON cz.id = ecz.city_zone_id
			LEFT JOIN seo_entity_instances sei ON sei.primary_id = eic.escort_id
			LEFT JOIN seo_entities se ON (se.id = sei.entity_id) OR (se.application_id = 2 AND se.slug = "escort")
			LEFT JOIN escort_services es ON es.escort_id = e.id
			' . (! is_null($where) ? 'WHERE ' . $where . $exclude : '') . ' 
			GROUP BY eic.escort_id
			ORDER BY ' . $order . '
			LIMIT ' . $limit . '
		'; 
		//echo $sql;
		
		$escorts = self::db()->fetchAll($sql);
		$count = $count + self::db()->fetchOne('SELECT FOUND_ROWS()');
		
		if ( $gotd_escort ) {
			$escorts = array_merge(array($gotd_escort), $escorts);
		}
		
		// <editor-fold defaultstate="collapsed" desc="Make Escort Alt for SEO">
		$tpl_data = array(
			'app_title' => Cubix_Application::getById()->title
		);

		foreach ( $escorts as $i =>  $escort ) {
			$additional = array(
				'showname' => $escort->showname,
				'city' => $escort->city
			);

			foreach ( $tpl_data + $additional as $tpl_var => $tpl_value ) {
				$escorts[$i]->alt = str_replace('%' . $tpl_var . '%', $tpl_value, $escorts[$i]->alt);
			}
		}
		// </editor-fold>

		/*if ( $sess_name ) {
			// <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
			$sid = 'sedcard_paging_' . Cubix_Application::getId() . '_' . $region_slug;
			
			$sess = new Zend_Session_Namespace($sid);

			$sess->{$sess_name} = array();
			foreach ( $escorts as $escort ) {
				$sess->{$sess_name}[] = $escort->showname;
			}
			// </editor-fold>
			//var_dump($sess->{$sess_name});
		}*/

		return $escorts;
	}

	public static function getTours($is_upcoming, $filter, $ordering = 'random', $page = 1, $page_size = 50, &$count = 0)
	{
		$lng = Cubix_I18n::getLang();

		if ( 'price' == substr($ordering, 0, 5) ) {
			$filter[] = 'e.incall_price IS NOT NULL';
		}

		if ( isset($filter['eic.gender = 1']) ) unset($filter['eic.gender = 1']);
		
		if ( ! $is_upcoming ) {
			$filter[] = 'eic.is_tour = 1';
		}
		else {
			$filter[] = 'eic.is_upcoming = 1';
		}

		$where = self::getWhereClause($filter, true);
		$order = self::_mapSorting($ordering);

		$page = (int) $page;
		if ( $page < 1 ) $page = 1;
		$limit = ($page_size * ($page - 1)) . ', ' . $page_size;

		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id,
				ct.title_' . $lng . ' AS city, e.rates, e.incall_type, e.outcall_type, e.is_new,
				e.verified_status, e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,
				IF (sei.primary_id IS NULL, IF(se1.id IS NOT NULL, se1.title_' . $lng . ', se2.title_' . $lng . '), sei.title_' . $lng . ') AS alt,
				e.hit_count, e.slogan, e.incall_price, e.comment_count, e.review_count,
				e.date_last_modified,
				eic.is_premium, e.is_online, e.about_' . $lng . ' AS about, UNIX_TIMESTAMP(e.date_last_modified) AS date_last_modified
				' .
				(! $is_upcoming ? ', e.tour_date_from, e.tour_date_to' : // if
				($is_upcoming ? ', ut.tour_date_from, ut.tour_date_to' : '')) // else
				. '
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			LEFT JOIN seo_entity_instances sei ON sei.primary_id = eic.escort_id
			LEFT JOIN seo_entities se1 ON se1.id = sei.entity_id
			LEFT JOIN seo_entities se2 ON se2.slug = "escort" AND se2.application_id = 2
			LEFT JOIN escort_services es ON es.escort_id = e.id
			' . ($is_upcoming ? 'INNER JOIN upcoming_tours ut ON ut.id = eic.escort_id AND ut.tour_city_id = eic.city_id' : '') . '
			' . (! is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.escort_id
			ORDER BY ' . $order . '
			LIMIT ' . $limit . '
		';

		$escorts = self::db()->fetchAll($sql);
		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');

		// <editor-fold defaultstate="collapsed" desc="Make Escort Alt for SEO">
		$tpl_data = array(
			'app_title' => Cubix_Application::getById()->title
		);

		foreach ( $escorts as $i =>  $escort ) {
			$additional = array(
				'showname' => $escort->showname,
				'city' => $escort->city
			);

			foreach ( $tpl_data + $additional as $tpl_var => $tpl_value ) {
				$escorts[$i]->alt = str_replace('%' . $tpl_var . '%', $tpl_value, $escorts[$i]->alt);
			}
		}
		// </editor-fold>
		
		// <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
		$sid = 'sedcard_paging_' . Cubix_Application::getId();
		$sess = new Zend_Session_Namespace($sid);
		
		if ( ! $is_upcoming ) {
			$sess->tours_list = array();
			foreach ( $escorts as $escort ) {
				$sess->tours_list[] = $escort->showname;
			}
		}
		else {
			$sess->up_tours_list = array();
			foreach ( $escorts as $escort ) {
				$sess->up_tours_list[] = $escort->showname;
			}
		}
		// </editor-fold>

		return $escorts;
	}

	private static function _mapSorting($param)
	{
		$map = array(
			'price-asc' => 'e.incall_price ASC',
			'price-desc' => 'e.incall_price DESC',
			'random' => 'eic.is_premium DESC, eic.ordering DESC',
			'alpha' => 'e.showname ASC',
			'most-viewed' => 'e.hit_count DESC',
			'newest' => 'e.is_new DESC, e.date_activated DESC',
			'date-activated' => 'e.date_activated DESC',
			'last-modified' => 'e.date_last_modified DESC',
			'city-asc' => 'ct.slug ASC'
		);
		
		$order = 'e.ordering DESC';
		if ( isset($map[$param]) ) {
			$order = $map[$param];
		}
		elseif ( false !== strpos($param, 'FIELD') ) {
			$order = $param;
		}

		return $order;
	}

	public static function getPrevNext($showname)
	{
		return array(null, null);
		// Retrieve criterias from session
		$sid = 'v2_sedcard_paging_' . Cubix_Application::getId();
		$sess = new Zend_Session_Namespace($sid);

		$page = $sess->page;
		$page_size = $sess->page_size;
		$pages_count = $sess->pages_count;

		list($filter, $ordering) = $sess->criteria;
		foreach ( $sess->escorts as $i => $name ) {
			if ( $name == $showname ) {
				$prev_i = $i - 1;
				$next_i = $i + 1;

				break;
			}
		}

		echo "Prev_i: $prev_i, Next_i: $next_i\n";
		print_r($sess->escorts);

		if ( ! isset($prev_i) && ! isset($next_i) ) {
			return array(null, null);
		}

		// This is first escort, we need to fetch data from prev page
		if ( isset($prev_i) && (! isset($sess->escorts[$prev_i]) || $prev_i < 0) && $page > 1 ) {
			$sess->escorts = array_slice($sess->escorts, 0, $page_size * 2);

			$escorts = call_user_func_array(
				array(__CLASS__, $sess->callback),
				array($filter, $ordering, $page - 1, $page_size)
			);
			foreach ( $escorts as $i => $escort ) {
				$escorts[$i] = $escort->showname;
			}

			$sess->escorts = array_merge($escorts, $sess->escorts);
			
			$prev_i = $page_size - 1;
		}

		
		echo "Escorts after 'first' logic\n";
		echo "Prev_i: $prev_i, Next_i: $next_i\n";
		print_r($sess->escorts);
		
		// If this last escort on page, we need to fetch data from next page
		if ( isset($next_i) &&  (! isset($sess->escorts[$next_i]) || $next_i > count($sess->escorts) - 1 ) && $page < $pages_count ) {
			if ( count($sess->escorts) >= $page_size * 3 ) {
				$sess->escorts = array_slice($sess->escorts, $page_size, $page_size * 2);
				$next_i -= $page_size;
			}
			
			$escorts = call_user_func_array(
				array(__CLASS__, $sess->callback),
				array($filter, $ordering, $page + 1, $page_size)
			);
			foreach ( $escorts as $i => $escort ) {
				$escorts[$i] = $escort->showname;
			}

			$sess->escorts = array_merge($sess->escorts, $escorts);
		}

		echo "Escorts after 'last' logic\n";
		echo "Prev_i: $prev_i, Next_i: $next_i\n";
		print_r($sess->escorts);
		
		return array(isset($prev_i) ? $sess->escorts[$prev_i] : null, isset($next_i) ? $sess->escorts[$next_i] : null);
	}
}
