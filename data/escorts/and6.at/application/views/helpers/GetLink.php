<?php

class Zend_View_Helper_GetLink
{
	protected static $_params;
	
	public function getLink($type = '', array $args = array(), $clean = false, $no_args = false)
	{
		$lang_id = Cubix_I18n::getLang();

		$link = '/';
//		if ( $lang_id != Cubix_Application::getDefaultLang() ) {
//			$link .= $lang_id . '/';
//		}
		
		if ( isset($args['state']) && 7 != Cubix_Application::getId() ) {
			$args['region'] = $args['state'];
			unset($args['state']);
		}
		
		switch ( $type ) {
			case 'forum':
				$link = 'http://www.escort-annonce.com/forum/?lang=' . $lang_id;
				break;
			case 'chat':
				$link = 'http://chat.escort-annonce.com/';
				break;
			case 'cams':
				$link = 'http://escort-annonce.streamray.com/';
				break;
			case 'search':
				$link .= 'search';
				break;
			case 'escorts-list':
				$link .= 'escorts';
			break;
			case 'base-city':
				$request = Zend_Controller_Front::getInstance()->getRequest();
				$region = $request->region;
				$section = isset($args['section']) ? $args['section'] . '/' : '';
				if ( strlen($section) ) unset($args['section']);

				$link .= 'escorts/region_' . $region . '/' . $section . 'city_' . $args['city'];
				unset($args['city']);
			break;
			case 'escorts':
				$request = Zend_Controller_Front::getInstance()->getRequest();
				
				if ( empty(self::$_params) ) {
					$req = trim($request->getParam('req', ''), '/');
					
					$req = explode('/', $req);
					
					$params = array();
					
					foreach ($req as $r)  {
						$param = explode('_', $r);
						if ( count($param) < 2 ) {
							$params[] = $r;
							continue;
						}
						
						$param_name = reset($param);
						array_shift($param);
						$params[$param_name] = implode('_', $param);
					}
					if ( $request->getParam('city') ) {
						$params['city'] = $request->getParam('city');
					}
					if ( $request->getParam('city_id') ) {
						$params['city_id'] = $request->getParam('city_id');
					}
					self::$_params = $params;
				}
				
				if ( $clean ) {
					$params = array();
				}
				else {
					$params = self::$_params;
				}
				
				foreach ( $args as $key => $value ) {
					if ( is_null($value) ) {
						unset($params[$key]);
						continue;
					}
					
					$params[$key] = $value;
				}
				
				//$link .= 'escorts/';
				
				if ( $params['region'] ) {
					$host = explode('.', $request->getHttpHost());
					$subdomain = $params['region'];
					if ( $subdomain == 'austria' ) {
						$subdomain = 'www';
					}
					$host[0] = $subdomain;
					$link = 'http://' . implode('.', $host) . '/';

					unset($params['region']);
				}
				if  ( $params['city_id'] ) {
					$link .= $params['city'] . '-c' . $params['city_id'] . '/';
					unset($params['city']);
					unset($params['city_id']);
				}
				
				foreach ( $params as $param => $value ) {
					if ( ! strlen($param) || ! strlen($value)) continue;
					if ( is_int($param) ) {
						if ( $value == 'tours' ) $value = 'citytours';
						$link .= 'escorts/' . $value . '/';
					}
					elseif ( strlen($param) ) {
						if ( $param == 'page' ) {
							if ( $value - 1 > 0 ) {
								$link .= ($value - 1) . '/';
							}
						} 
						else {
							$link .= $param . '_' . $value . '/';
						}
					}
				}
				
				$link = rtrim($link, '/');
				
				$args = array();
			break;
			case 'club_directory_map':
				$link .= 'club-directory/map';
				break;
			case 'club_directory':
				$link .= 'club-directory';
				break;
			case 'gotm':
				$link .= 'girl-of-month';

				if ( isset($args['history']) && $args['history'] ) {
					$link .= '/history';
				}
				
				if ( isset($args['page']) ) {
					if ( $args['page'] != 1 ) {
						$link .= '/page_' . $args['page'];
						//$link .= '/page_' . $args['page'];
					}
					
					unset($args['page']);
				}
				break;
			case 'profile':
				$link .= 'sex-model/' . $args['showname'] . '-' . $args['escort_id'];
				unset($args['showname']);
				unset($args['escort_id']);
			break;
			case 'forgot':
				$link .= 'private/forgot';
			break;
			case 'signup':
				$link .= 'private/signup-';
				
				if ( ! isset($args['type']) ) {
					$args['type'] = 'member';
				}
				
				$link .= $args['type'];
			break;
			case 'signin':
				$link .= 'private/signin';
			break;
			case 'signout':
				$link .= 'private/signout';
			break;
			case 'private':
				$link .= 'private';
			break;
			case 'membership-type':
				$link .= 'private/membership-type';
			break;
			case 'feedback':
				$link .= 'feedback';
			break;
			case 'feedback-agency':
				$link .= 'feedback-agency';
			break;
			case 'contact':
				$link .= 'contact';
			break;
			case 'add-review-comment':
				$link .= 'private-v2/add-review-comment';
			break;
			case 'captcha':
				$link .= 'captcha?' . rand();
				//$link = '/img/sample_captcha.gif?' . rand();
			break;
			case 'viewed-escorts':
				$link .= 'escorts/viewed-escorts';
			break;
			case '100p-verify':
				$link .= 'private/verify';
			break;
			case '100p-verify-webcam':
				$link .= 'private/verify/webcam';
			break;
			case '100p-verify-idcard':
				$link .= 'private/verify/idcard';
			break;
			case 'agency':
				$link .= 'agency/' . $args['slug'] . '-' . $args['id'];
				unset($args['slug']);
				unset($args['id']);
			break;
			
			case 'favorites':
				$link .= 'private/favorites';
			break;
			case 'add-to-favorites':
				$link .= 'private/add-to-favorites';
			break;
			case 'remove-from-favorites':
				$link .= 'private/remove-from-favorites';
			break;
			
			// Private Area
            case 'simple-profile':
				$link .= 'private-v2/profile/simple';
				break;
			case 'fast-profile':
				$link .= 'fast-profile';
				break;
			case 'edit-profile':
				$link .= 'private/profile';
			break;
			case 'edit-agency-profile':
				$link .= 'private/agency-profile';
			break;
			case 'profile-data':
				$link .= 'private/profile-data';
			break;
			case 'edit-photos':
				$link .= 'private/photos';
			break;
			case 'edit-rates':
				$link .= 'private/rates';
			break;
			case 'edit-escorts':
				$link .= 'private/escorts';
			break;
			case 'delete-escort':
				$link .= 'private/delete-escort';
			break;
			case 'edit-tours':
				$link .= 'private/tours';
			break;
			case 'change-passwd':
				$link .= 'private/change-password';
			break;
			
			case 'search':
				$link .= 'search';
			break;

			case 'late-night-girls':
				$link .= 'escorts/late-night-girls';
			break;
			
			case 'terms':
				$link .= 'page/terms-and-conditions';
			break;
			case 'webcam-lm':
				$link .= 'page/webcam-verification-learn-more';
			break;
			case 'passport-lm':
				$link .= 'page/passport-verification-learn-more';
			break;
			
			case 'external-link':
				$link = '/go?' . $args['link'];
				unset($args['link']);
			break;
			// V2 Private Area
			case 'private-v2':
				$link .= 'private-v2';
				break;
			case 'private-v2-settings':
				$link .= 'private-v2/settings';
				break;
			case 'private-v2-statistics':
				$link .= 'private-v2/statistics';
				break;
			case 'private-v2-happy-hour':
				$link .= 'private-v2/happy-hour';
				break;
			case 'private-v2-client-blacklist':
				$link .= 'private-v2/client-blacklist';
				break;
			case 'private-v2-add-client-to-blacklist':
				$link .= 'private-v2/add-client-to-blacklist';
				break;
			case 'private-v2-profile':
				$link .= 'private-v2/profile';
				break;
			case 'private-v2-advertise-now':
				$link .= 'private-v2/profile/advertise-now';
				break;
			case 'private-v2-advertise-success':
				$link = 'http://' . $_SERVER['SERVER_NAME'] . '/private-v2/profile/advertise-success';
				break;
			case 'private-v2-advertise-failure':
				$link = 'http://' . $_SERVER['SERVER_NAME'] . '/private-v2/profile/advertise-failure';
				break;
			case 'private-v2-advertise-cancel':
				$link = 'http://' . $_SERVER['SERVER_NAME'] . '/private-v2/profile/simple';
				break;
			case 'private-v2-gotd':
				$link .= 'private-v2/profile/gotd';
				break;
			case 'private-v2-gotd-success':
				$link = 'http://' . $_SERVER['SERVER_NAME'] . '/private-v2/profile/gotd-success';
				break;
			case 'private-v2-gotd-failure':
				$link = 'http://' . $_SERVER['SERVER_NAME'] . '/private-v2/profile/gotd-failure';
				break;
			case 'private-v2-gotd-cancel':
				$link = 'http://' . $_SERVER['SERVER_NAME'] . '/private-v2/profile/gotd';
				break;
			case 'private-v2-photos':
				$link .= 'private-v2/photos';
				break;
			case 'profile-gallery':
				$link .= 'profile/gallery';
				break;
			case 'private-v2-plain-photos':
				$link .= 'private-v2/plain-photos';
				break;
			case 'private-v2-agency-plain-photos':
				$link .= 'private-v2/agency-plain-photos';
				break;
			case 'private-v2-tours':
				$link .= 'private-v2/tours';
				break;
			case 'private-v2-ajax-tours':
				$link .= 'private-v2/ajax-tours';
				break;
			case 'private-v2-ajax-tours-add':
				$link .= 'private-v2/ajax-tours-add';
				break;
			case 'private-v2-ajax-tours-remove':
				$link .= 'private-v2/ajax-tours-remove';
				break;
			case 'private-v2-verify':
				$link .= 'private-v2/verify';
				break;
			case 'private-v2-faq':
				$link .= 'private-v2/faq';
				break;
			case 'private-v2-premium':
				$link .= 'private-v2/premium';
				break;
			case 'private-v2-add-areas':
				$link .= 'private-v2/areas';
				break;
			case 'private-v2-support':
				$link .= 'support';
				break;
			case 'private-v2-get-rejected-verification':
				$link .= 'private-v2/get-rejected-verification';
				break;
			case 'private-v2-set-rejected-verification':
				$link .= 'private-v2/set-rejected-verification';
				break;
			case 'alerts':
				$link .= 'private-v2/alerts';
				break;
			case 'private-v2-agency-profile':
				$link .= 'private-v2/agency-profile';
				break;
			case 'private-v2-member-profile':
				$link .= 'private-v2/member-profile';
				break;
			case 'private-v2-upgrade-premium':
				$link .= 'private-v2/upgrade';
				break;
			case 'ticket-open':
				$link .= 'support/ticket';
				break;
			case 'private-v2-escorts':
				$link .= 'private-v2/escorts';
				break;
            case 'private-v2-escorts-delete':
				$link .= 'private-v2/profile-delete';
				break;
             case 'private-v2-escorts-restore':
				$link .= 'private-v2/profile-restore';
				break;
			case 'reviews':
				$link .= 'evaluations';
				break;
			case 'escort-reviews':
				$link .= 'evaluations/' . $args['showname'] . '-' . $args['escort_id'];
				unset ($args['showname']);
				unset ($args['escort_id']);
				break;
			case 'member-reviews':
				$link .= 'evaluations/member/' . $args['username'];
				unset ($args['username']);
				break;
			case 'escorts-reviews':
				$link .= 'evaluations/escorts';
				break;
			case 'agencies-reviews':
				$link .= 'evaluations/agencies';
				break;
			case 'top-reviewers':
				$link .= 'evaluations/top-reviewers';
				break;
			case 'top-ladies':
				$link .= 'evaluations/top-ladies';
				break;
			case 'voting-widget':
				$link .= 'escort/' . $args['showname'] . '-' . $args['escort_id'] . '/vote';
				unset($args['showname']);
				unset($args['escort_id']);
				break;
			case 'private-v2-reviews':
				$link .= 'private-v2/reviews';
				break;
			case 'private-v2-escort-reviews':
				$link .= 'private-v2/escort-reviews';
				break;
			case 'private-v2-escorts-reviews':
				$link .= 'private-v2/escorts-reviews';
				break;
			case 'private-v2-add-review':
				$link .= 'private-v2/add-review';
				break;
			case 'reviews-search':
				$link .= 'evaluations/search';
				break;
            case 'comments':
				$link .= 'comments/' . $args['reg'];
				unset($args['reg']);
				break;
			case 'member-info':
				$link .= 'member/'.$args['username'];
				unset($args['username']);
				break;
			case 'member-comments':
				$link .= 'members/get-member-comments';
				break;
			case 'tell-friend':
				$link .= 'escorts/ajax-tell-friend';
				break;
			case 'report-problem':
				$link .= 'escorts/ajax-report-problem';
				break;
			case 'get-filter':
				$link .= 'escorts/get-filter?ajax_lng=' . $lang_id;
				break;
			case 'switch-agency':
				$link .= 'private/switch-agency';
				break;
			case '/':
				$link = '/';
				break;
			case null:
				return $this;

		}

		if ( ! $no_args ) {
			if ( count($args) ) {
				$link .= '?';
				$params = array();
				foreach ( $args as $arg => $value ) {
					if ( ! is_array($value) ) {
						$params[] = $arg . '=' . urlencode($value);
					}
					else {
						foreach ( $value as $v ) {
							$params[] = $arg . '[]=' . $v;
						}
					}
				}

				$link .= implode('&', $params);
			}
		}
		
		return $link;
	}
	
	public function getParams()
	{
		return self::$_params;
	}
}
