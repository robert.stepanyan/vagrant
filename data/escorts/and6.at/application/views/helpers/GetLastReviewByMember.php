<?php

class Zend_View_Helper_GetLastReviewByMember
{
	public function getLastReviewByMember($user_id)
	{
		return Cubix_Api::getInstance()->call('getLastReviewByMember', array($user_id));
	}
}

