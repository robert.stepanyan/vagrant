<?php

class Zend_View_Helper_Time
{
	protected function timee($insertdate)
	{
        $tm = explode(" ", $insertdate);
        $days = explode("-", $tm[0]);
        $hours = explode(":", $tm[1]);

        $now = time();
		$u_insertdate = strtotime($insertdate);
		$inserted = mktime(date("H", $u_insertdate), date('i', $u_insertdate), date("s", $u_insertdate), date("m", $u_insertdate), date("d", $u_insertdate), date('Y', $u_insertdate));
		//$inserted = mktime($hours[0],$hours[1],$hours[2],$days[1],$days[2],$days[0]);
        $diff = $now - $inserted;

        $activedays = abs(floor($diff / 86400));
        $activehours = abs(floor(($diff % 86400) / 3600));
        $activeminutes = abs(floor((($diff % 86400) % 3600) / 60));

        if(date("m-d-Y", strtotime($insertdate)) == date("m-d-Y"))
        {
        	$rslt = Cubix_I18n::translate('today');
        }

        if ($activedays >= 2) {
           //$rslt = str_replace('%TIME%', $activedays . " " . ((1 < $activedays)? $LNG['days'] : $LNG['day']), $LNG['time_ago']);
			if ( $activedays > 1 )
				$rslt = Cubix_I18n::translate('days_ago', array('DAYS' => $activedays));
			else
				$rslt = Cubix_I18n::translate('day_ago', array('DAYS' => $activedays));
        }
        elseif (0 < $activedays && 2 > $activedays) {
            $rslt = Cubix_I18n::translate('yesterday');
        }
        else {
            if (0 < $activehours) {
                //$rslt = str_replace('%TIME%', $activehours . " " . ((1 < $activehours)? $LNG['hours'] : $LNG['hour']) . " " . $activeminutes . "  " . ((1 < $activeminutes)? strtolower($LNG['minutes']) : strtolower($LNG['minute'])), $LNG['time_ago']);
                //$rslt = str_replace('%TIME%', $activehours . " " . ((1 < $activehours)? $LNG['hours'] : $LNG['hour']) . " " . $activeminutes . "  " . ((1 < $activeminutes)? strtolower($LNG['minutes']) : strtolower($LNG['minute'])), $LNG['time_ago']);
				if ( $activehours > 1 )
					$rslt = Cubix_I18n::translate('hours_ago', array('HOURS' => $activehours));
				else
					$rslt = Cubix_I18n::translate('hour_ago', array('HOURS' => $activehours));
            }
            else {
                //$rslt = str_replace('%TIME%', $activeminutes . " " . ((1 < $activeminutes)? $LNG['minutes'] : $LNG['minute']), $LNG['time_ago']);
				if ( $activeminutes > 1 )
					$rslt = Cubix_I18n::translate('minutes_ago', array('MINUTES' => $activeminutes));
				else
					$rslt = Cubix_I18n::translate('minute_ago', array('MINUTES' => $activeminutes));
            }
        }

        return $rslt;
    }
}

