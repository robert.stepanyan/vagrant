function Config() {
	var config = {
		production : {
			common : {
				applicationId : 25,
				listenPort : 8899,
				host: 'www.and6.at',
				authPath : '/get_user_info.php',
				sessionCacheTime: 1000 * 60 * 10, //10 minutes
				sessionCheckInterval : 1 * 60 * 60 * 1000, //1 hour
				imagesHost : 'http://pic.and6.at/and6.at/',
				avatarSize : 'lvthumb',
				noPhotoUrl : '/img/chat/img-no-avatar.gif',
				userSettingsBackupPath : 'user_settings.bk',
				key : 'JFusLsdf8A9',
				blWordsLifeTime : 12 * 60 * 60 * 1000, //12 hours
				redisBlWordsDb: 0,
				redisBlWordsKey: 'bl-words-a6at'
			},
			db : {
				user:     'a6_at_chat',
				database: 'and6.at_backend',
				password: 'kjuyGh15sS(kl45',
				host:     'www2.a6.at'
			},
			memcache : {
				host : '172.16.0.11',
				port : 11211
			}
		},
		development: {
			common : {
				applicationId : 25,
				listenPort : 8899,
				host: 'www.escortforum.net.dev',
				authPath: '/get_user_info.php',
				sessionCacheTime: 1000 * 60 * 10, //10 minutes
				sessionCheckInterval : 1 * 60 * 60 * 1000, //1 hour
				imagesHost : 'http://pic.escortforum.net/escortforum.net/',
				avatarSize : 'lvthumb',
				noPhotoUrl : '/img/chat/img-no-avatar.gif',
				userSettingsBackupPath : 'user_settings.bk',
				key : 'JFusLsdf8A9',
				blWordsLifeTime : 12 * 60 * 60 * 1000, //12 hours
				redisBlWordsDb: 0,
				redisBlWordsKey: 'bl-words-a6at'
			},
			db : {
				user:     'sceon',
				database: 'escortforum.net_backend',
				password: '123456',
				host:     '192.168.0.1'
			},
			memcache : {
				host : '172.16.0.11',
				port : 11211
			}
		}
	};
	
	this.get = function(type) {
		if ( type == 'development' ) {
			return config.development;
		} else {
			return config.production;
		}
	}
}

function get(type) {
	conf = new Config();
	return conf.get(type);
}



exports.get = get;
