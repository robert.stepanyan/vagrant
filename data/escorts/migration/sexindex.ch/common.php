<?php

//define('DB_HOST', '172.16.0.91'/*'maindb'*/);
//define('DB_USER', 'root');
//define('DB_PASS', 'Ghhy25p4');
//define('DB_PRIMARY', 'main');
//define('DB_SECONDARY', 'main_v2');

define('DB_HOST', '192.168.0.6'/*'maindb'*/);
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_PRIMARY', 'escortforum_old');
define('DB_SECONDARY', 'escortforum_fresh');


//define('DB_HOST', '192.168.0.1');
//define('DB_USER', 'root');
//define('DB_PASS', '123456');
//define('DB_PRIMARY', 'escort_main');
//define('DB_SECONDARY', 'escortforum_migrated');

class Db
{
	protected $_link;
	
	public function __construct($params)
	{
		extract($params);
		$this->connect($host, $user, $pass, $name);
	}
	
	public function connect($host, $user, $pass, $name)
	{
		$this->_link = @mysql_connect($host, $user, $pass, true);
		
		if ( ! $this->_link ) {
			throw new Db_Exception('Could not connect to database server');
		}
		
		if ( ! mysql_select_db($name, $this->_link) ) {
			throw new Db_Exception('Could not select database');
		}
	}
	
	public function close()
	{
		@mysql_close($this->_link);
	}
	
	public function query($sql)
	{
		$result = mysql_query($sql, $this->_link);
		
		if ( mysql_errno($this->_link) > 0 ) {
			throw new Db_Exception('MySQL Error: ' . mysql_error($this->_link));
		}
		
	/*	if ($last_id = mysql_insert_id())
		   return $last_id;*/
		
		return $result;
	}

	public function fetchAll($sql)
	{
		$result = $this->query($sql);
		
		return new Db_Result($result);
	}
	
	public function fetchRow($sql)
	{
		$result = $this->query($sql);
		
		return mysql_fetch_assoc($result);
	}
	
	public function fetchOne($sql)
	{
		$result = $this->query($sql);
		
		$row = mysql_fetch_row($result);
		
		if ( ! $row ) return null;
		
		return $row[0];
	}
	
	public function escape($str)
	{
		if ( is_null($str) ) return 'NULL';
		if ( is_numeric($str) ) return $str;
		if ( is_bool($str) ) return $str === TRUE ? 'TRUE' : 'FALSE';
		if ( $str instanceof Db_Expr ) return $str->__toString();
		
		return '"' . mysql_real_escape_string($str, $this->_link) . '"';
	}
	
	//
	
	public function insert($table, $data)
	{
		$sql = 'INSERT INTO `' . $table . '`(' . implode(', ', array_keys($data)) . ') VALUES (';
		
		foreach ( $data as $field => $value ) {
			$data[$field] = $this->escape($value);
		}
		
		$sql .= implode(', ', $data) . ')';
		
		return $this->query($sql);
	}

	public function lastInsertId()
	{
		return mysql_insert_id($this->_link);
	}
}

class Db_Result extends ArrayIterator
{
	protected $_result;
	
	protected $_count;
	
	protected $_position = 0;
	
	public function __construct($result)
	{
		$this->_result = $result;
		$this->_count = mysql_num_rows($this->_result);
	}
	
	public function rewind() {
		$this->_position = 0;
	}
	
	public function next() {
		++$this->_position;
	}
	
	public function valid() {
		return $this->_position < $this->_count;
	}
	
	public function current() {
		mysql_data_seek($this->_result, $this->_position);
		
		return mysql_fetch_assoc($this->_result);
	}
	
	public function key() {
		return $this->_position;
	}
}

class Db_Expr
{
	protected $_str;
	
	public function __construct($str)
	{
		$this->_str = $str;
	}
	
	public function __toString()
	{
		return $this->_str;
	}
}

class Db_Exception extends Exception {}

global $pDb, $sDb;

$pDB = new Db(array('host' => DB_HOST, 'user' => DB_USER, 'pass' => DB_PASS, 'name' => DB_PRIMARY));
$pDB->query('SET NAMES `utf8`');
// $sDB = new Db(array('host' => DB_HOST, 'user' => DB_USER, 'pass' => DB_PASS, 'name' => DB_SECONDARY));
// $sDB = new Db(array('host' => DB_HOST, 'user' => DB_USER, 'pass' => '123456', 'name' => 'escortforum_migrated'));
$sDB = new Db(array('host' => '192.168.0.1', 'user' => 'root', 'pass' => '123456', 'name' => 'escortforum_fresh'));
$sDB->query('SET NAMES `utf8`');
