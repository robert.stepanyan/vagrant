<?php

// WARNING: If you add another nationality,
// you must add its ISO code to the $NATIONALITIES in include/nationalities.inc.php.

$LNG['nationality_al'] = "Albanian";
$LNG['nationality_us'] = "American";
$LNG['nationality_ar'] = "Argentinian";
$LNG['nationality_au'] = "Australian";
$LNG['nationality_at'] = "Austrian";
$LNG['nationality_be'] = "Belgian";
$LNG['nationality_br'] = "Brazilian";
$LNG['nationality_gb'] = "British";
$LNG['nationality_bg'] = "Bulgarian";
$LNG['nationality_ca'] = "Canadian";
$LNG['nationality_cn'] = "Chinese";
$LNG['nationality_cz'] = "Czech";
$LNG['nationality_dk'] = "Danish";
$LNG['nationality_nl'] = "Dutch";
$LNG['nationality_fr'] = "French";
$LNG['nationality_de'] = "German";
$LNG['nationality_gr'] = "Greek";
$LNG['nationality_hu'] = "Hungarian";
$LNG['nationality_in'] = "Indian";
$LNG['nationality_ie'] = "Irish";
$LNG['nationality_il'] = "Israeli";
$LNG['nationality_it'] = "Italian";
$LNG['nationality_jm'] = "Jamaican";
$LNG['nationality_jp'] = "Japanese";
$LNG['nationality_my'] = "Malaysian";
$LNG['nationality_md'] = "Moldovian";
$LNG['nationality_pl'] = "Polish";
$LNG['nationality_pt'] = "Portuguese";
$LNG['nationality_ro'] = "Romanian";
$LNG['nationality_ru'] = "Russian";
$LNG['nationality_sk'] = "Slovak";
$LNG['nationality_es'] = "Spanish";
$LNG['nationality_se'] = "Swedish";
$LNG['nationality_ch'] = "Swiss";
$LNG['nationality_th'] = "Thai";
$LNG['nationality_ua'] = "Ukrainian";
$LNG['nationality_ve'] = "Venezuelan";

$LNG['nationality_by'] = "Belarussian";
$LNG['nationality_ee'] = "Estonian";
$LNG['nationality_fi'] = "Finnish";
$LNG['nationality_hr'] = "Croatian";
$LNG['nationality_kr'] = "Korean";
$LNG['nationality_lt'] = "Lithuanian";
$LNG['nationality_lv'] = "Latvian";
$LNG['nationality_me'] = "Montenegrian";
$LNG['nationality_mx'] = "Mexican";
$LNG['nationality_no'] = "Norwegian";
$LNG['nationality_ph'] = "Filipino";
$LNG['nationality_rs'] = "Serbian";
$LNG['nationality_sg'] = "Singaporean";
$LNG['nationality_si'] = "Slovenian";
$LNG['nationality_cu'] = "Cuban";
$LNG['nationality_co'] = "Colombian";
$LNG['nationality_pr'] = "Puerto Rican";
$LNG['nationality_vn'] = "Vietnamese";
?>