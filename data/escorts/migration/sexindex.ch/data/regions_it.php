<?php
$LNG['region_ch_basel_stadt_land'] = "Basel Stadt + Land";
$LNG['region_ch_bern_umgebung'] = "Bern Umgebung";
$LNG['region_ch_bern_stadt'] = "Bern Stadt";
$LNG['region_ch_luzern_Innerschweiz'] = "Luzern/Innerschweiz";
$LNG['region_ch_aargau_mitteland'] = "Aargau/Mitteland";
$LNG['region_ch_stgallen_ostschweiz'] = "St. Gallen/Ostschweiz";
$LNG['region_ch_lugano_tessin'] = "Lugano/Tessin";
$LNG['region_ch_genf_westschweiz'] = "Genf/Westschweiz";
$LNG['region_ch_zurich_umgebung'] = "Zürich Umgebung";
$LNG['region_ch_zurich_city'] = "Zürich City";

$LNG['region_gb_south_east'] = "South East";
$LNG['region_gb_east_midlands'] = "East Midlands";
$LNG['region_gb_south_west'] = "South West";
$LNG['region_gb_north_east'] = "North East";
$LNG['region_gb_east_anglia'] = "East Anglia";
$LNG['region_gb_north_west'] = "North West";
$LNG['region_gb_west_midlands'] = "West Midlands";
$LNG['region_gb_yorkshire'] = "Yorkshire";
$LNG['region_gb_scotland_wales'] = "Scotland & Wales";




$LNG['region_it_abruzzo'] = "Abruzzo";
$LNG['region_it_basilicata'] = "Basilicata";
$LNG['region_it_calabria'] = "Calabria";
$LNG['region_it_campania'] = "Campania";
$LNG['region_it_friuli_venezia_giulia'] = "Friuli Venezia Giulia";
$LNG['region_it_lazio'] = "Lazio";
$LNG['region_it_liguria'] = "Liguria";
$LNG['region_it_lombardy'] = "Lombardia";
$LNG['region_it_marche'] = "Marche";
$LNG['region_it_piedmont'] = "Piemonte";
$LNG['region_it_puglia'] = "Puglia";
$LNG['region_it_emilia_romagna'] = "Emilia Romagna";
$LNG['region_it_sardegna'] = "Sardegna";
$LNG['region_it_sicilia'] = "Sicilia";
$LNG['region_it_tuscany'] = "Toscana";
$LNG['region_it_trentino_alto_adige'] = "Trentino Alto Adige";
$LNG['region_it_umbria'] = "Umbria";
$LNG['region_it_valle_d_aosta'] = "Valle D'Aosta";
$LNG['region_it_veneto'] = "Veneto";
$LNG['region_it_molise'] = "Molise";
?>