<?php

// WARNING: If you add another nationality,
// you must add its ISO code to the $NATIONALITIES in include/nationalities.inc.php.

$LNG['nationality_al'] = "Albanienne";
$LNG['nationality_us'] = "Americaine";
$LNG['nationality_ar'] = "Argentinienne";
$LNG['nationality_au'] = "Australienne";
$LNG['nationality_at'] = "Autrichienne";
$LNG['nationality_be'] = "Belge";
$LNG['nationality_br'] = "Brazilienne";
$LNG['nationality_gb'] = "Anglaise";
$LNG['nationality_bg'] = "Bulgare";
$LNG['nationality_ca'] = "Canadienne";
$LNG['nationality_cn'] = "Chinoise";
$LNG['nationality_cz'] = "Tchèque";
$LNG['nationality_dk'] = "Danoise";
$LNG['nationality_nl'] = "Hollandaise";
$LNG['nationality_fr'] = "Française";
$LNG['nationality_de'] = "Allemande";
$LNG['nationality_gr'] = "Grèque";
$LNG['nationality_hu'] = "Hongroise";
$LNG['nationality_in'] = "Indienne";
$LNG['nationality_ie'] = "irlandaise";
$LNG['nationality_il'] = "Israelienne";
$LNG['nationality_it'] = "Italien";
$LNG['nationality_jm'] = "Jamaïcaine";
$LNG['nationality_jp'] = "Japonnaise";
$LNG['nationality_my'] = "Malaysienne";
$LNG['nationality_md'] = "Moldovian";
$LNG['nationality_pl'] = "Polonaise";
$LNG['nationality_pt'] = "Portugaise";
$LNG['nationality_ro'] = "Roumaine";
$LNG['nationality_ru'] = "Russe";
$LNG['nationality_sk'] = "Slovak";
$LNG['nationality_es'] = "Espagnole";
$LNG['nationality_se'] = "Suèdoise";
$LNG['nationality_ch'] = "Suisse";
$LNG['nationality_th'] = "Thaïlandaise";
$LNG['nationality_ua'] = "Ukrainienne";
$LNG['nationality_ve'] = "Vénézuélienne";

$LNG['nationality_by'] = "Belarussian";
$LNG['nationality_ee'] = "Estonian";
$LNG['nationality_fi'] = "Finnish";
$LNG['nationality_hr'] = "Croatian";
$LNG['nationality_kr'] = "Korean";
$LNG['nationality_lt'] = "Lithuanian";
$LNG['nationality_lv'] = "Latvian";
$LNG['nationality_me'] = "Montenegrian";
$LNG['nationality_mx'] = "Mexican";
$LNG['nationality_no'] = "Norwegian";
$LNG['nationality_ph'] = "Filipino";
$LNG['nationality_rs'] = "Serbian";
$LNG['nationality_sg'] = "Singaporean";
$LNG['nationality_si'] = "Slovenian";
$LNG['nationality_cu'] = "Cuban";
$LNG['nationality_co'] = "Colombian";
$LNG['nationality_pr'] = "Puerto Rican";
$LNG['nationality_vn'] = "Vietnamese";
?>