#!/usr/local/bin/php
<?php

set_time_limit(0);

require_once('common.php');

define('APP_ID',				7);
define('APP_NEW_ID',			7);
define('APP_HOST',				'escortguide.com');
define('APP_URL',				'http://www.escortguide.com');
define('APP_TITLE',				'EscortGuide.com');
define('APP_PHONE',				'+447624802546');
define('APP_COUNTRY',			216);
define('APP_API_KEY',			'abb8369000d6afd615a4f3d5919a442e');
define('APP_COUNTRY_ISO',		'us');
define('APP_MIN_PHOTOS',		1);
define('APP_USER_SALT_HASH',	'Z3a6C200pa');
define('APP_CURRENCY_ID',		1);
define('APP_CURRENCY_SYMBOL',	'$');
define('APP_CURRENCY_TITLE',	'USD');
define('APP_BACKEND_URL',		'http://backend.escortguide.com');

$app_langs = array('en');

function _get_geography()
{
	global $pDB, $sDB;

	$_countries = $sDB->fetchAll('SELECT id, iso FROM countries');
	$countries = array();
	foreach ( $_countries as $country ) {
		$countries[$country['iso']] = $country['id'];
	}

	$_cities = $sDB->fetchAll('SELECT id, internal_id FROM cities');
	$cities = array();
	foreach ( $_cities as $city ) {
		$cities[$city['internal_id']] = $city['id'];
	}

	return array('countries' => $countries, 'cities' => $cities);
}

function stage_internal()
{
	global $pDB, $sDB, $app_langs;

	/* >>> application info */
	$sDB->query('SET FOREIGN_KEY_CHECKS=0');
	$sDB->query('DELETE FROM applications WHERE id = ' . APP_NEW_ID);
	$sDB->insert('applications', array(
		'id' 			=> APP_NEW_ID,
		'host'		 	=> APP_HOST,
		'url' 			=> APP_URL,
		'title' 		=> APP_TITLE,
		'phone_number' 	=> APP_PHONE,
		'country_id' 	=> APP_COUNTRY,
		'api_key' 		=> APP_API_KEY,
		'min_photos'	=> APP_MIN_PHOTOS,
		'currency_id' => APP_CURRENCY_ID,
		'currency_symbol' => APP_CURRENCY_SYMBOL,
		'currency_title' => APP_CURRENCY_TITLE,
		'backend_url' => APP_BACKEND_URL
	));

	$sDB->query('DELETE FROM application_langs WHERE application_id = ' . APP_NEW_ID);
	foreach ( $app_langs as $lang_id ) {
		$sDB->insert('application_langs', array(
			'application_id' => APP_NEW_ID,
			'lang_id' => $lang_id
		));
	}
	/* <<< */

	/* >>> backend users */
	$users = $pDB->fetchAll('SELECT * FROM esc_admin_users WHERE application_id = ' . APP_ID);

	$sDB->query('DELETE FROM backend_users WHERE application_id = ' . APP_NEW_ID);
	foreach ( $users as $old_user ) {
		if ( 99 == $old_user['admin_type'] ) {
			$type = 'superadmin';
		}
		elseif ( 1 == $old_user['admin_type'] ) {
			$type = 'admin';
		}
		elseif ( 3 == $old_user['admin_type'] ) {
			$type = 'data entry';
		}

		$user = array(
			'id' => $old_user['id'],
			'username' => $old_user['login'],
			'password' => $old_user['password'],
			'email' => $old_user['email'],
			'type' => $type,
			'is_disabled' => ! $old_user['status'],
			'application_id' => APP_NEW_ID
		);

		$sDB->insert('backend_users', $user);
	}
	/* <<< */
}

function stage_members($mode)
{
	global $pDB, $sDB;

	$sDB->query('DELETE m, u FROM members m INNER JOIN users u ON u.id = m.user_id WHERE u.application_id = ' . APP_NEW_ID);

	if ( MODE_CLEAN_ONLY == $mode ) return;

	extract(_get_geography());

	$members = $pDB->fetchAll('
		SELECT u.*, u.id AS user_id, u.email AS user_email, u.status AS user_status, m.city, m.country_iso, m.about_me
		FROM esc_member_details m
		INNER JOIN esc_users u ON u.id = m.id
		WHERE u.application_id = ' . APP_ID . '
	');

	$members_count = $pDB->fetchOne('
		SELECT COUNT(u.id)
		FROM esc_member_details m
		INNER JOIN esc_users u ON u.id = m.id
		WHERE u.application_id = ' . APP_ID . '
	');

	$c = 0;
	foreach ( $members as $old_member ) {
		$user = array(
			'id' => $old_member['user_id'],
			'email' => $old_member['user_email'],
			'username' => $old_member['login'],
			'password' => $old_member['password'],
			'salt_hash' => APP_USER_SALT_HASH,
			'user_type' => 'member',
			'activation_hash' => null,
			'date_registered' => $old_member['creation_date'],
			'status' => $old_member['user_status'],
			'disabled' => 0,
			'date_last_refreshed' => null,
			'sales_user_id' => $old_member['admin_user_id'],
			'application_id' => APP_NEW_ID,
			'city_id' => $cities[$old_member['city']],
			'country_id' => $countries[$old_member['country_iso']],
			'last_ip' => $old_member['last_ip'],
		);

		try {
			$sDB->insert('users', $user);
		}
		catch ( Exception $e ) {
			echo $e->getMessage() . "\r\n";
		}

		$sDB->insert('members', array(
			'user_id' => $old_member['user_id'],
			'country_id' => $countries[$old_member['country_iso']],
			'city_id' => $cities[$old_member['city']],
			'email' => $old_member['user_email'],
			'about_me' => $old_member['about_me']
		));

		$c++;
		if ( $c % 500 == 0 ) echo "[$c/$members_count] Member '{$user['username']}'\r\n";
	}

	echo "$c members has been migrated in total\r\n";
}

function stage_fix_clubs()
{
	global $pDB, $sDB;

	extract(_get_geography());

	$clubs = $pDB->fetchAll('
		SELECT u.*, u.email AS user_email, a.*
		FROM esc_clubs a
		INNER JOIN esc_users u ON u.id = a.user_id
	');

	$clubs_count = $pDB->fetchOne('
		SELECT COUNT(DISTINCT(a.user_id))
		FROM esc_clubs a
		INNER JOIN esc_users u ON u.id = a.user_id
	');

	foreach ( $clubs as $club ) {
		$sDB->query('DELETE FROM users WHERE id = ' . $club['user_id']);
	}

	$c = 0;
	foreach ( $clubs as $old_club ) {
		$user = array(
			'id' => $old_club['user_id'],
			'email' => $old_club['user_email'],
			'username' => $old_club['login'],
			'password' => $old_club['password'],
			'salt_hash' => APP_USER_SALT_HASH,
			'user_type' => 'agency',
			'activation_hash' => null,
			'date_registered' => $old_club['creation_date'],
			'status' => $old_club['status'],
			'disabled' => 0,
			'date_last_refreshed' => null,
			'sales_user_id' => $old_club['admin_user_id'],
			'application_id' => APP_ID,
			'city_id' => $cities[$old_club['city']],
			'country_id' => APP_COUNTRY,
			'last_ip' => $old_club['last_ip'],
			'is_club' => true
		);

		$sDB->insert('users', $user);

		$agency = array(
			'user_id' => $old_club['user_id'],
			'last_modified' => null,
			'country_id' => $countries[$old_club['country_iso']],
			'city_id' => $cities[$old_club['city']],
			'name' => $old_club['name'],
			'email' => $old_club['user_email'],
			'web' => $old_club['web'],
			'phone' => $old_club['phone'],
			'phone_instructions' => $old_club['phone_instructions'],
			'logo_hash' => null,
			'logo_ext' => null,
			'hit_count' => 0
		);

		$sDB->insert('agencies', $agency);

		$agency_id = $sDB->lastInsertId();

		$sDB->query('UPDATE escorts SET agency_id = ' . $agency_id . ' WHERE user_id = ' . $agency['user_id']);

		$c++;
		echo "[$c/$clubs_count] Agency '{$agency['name']}'\r\n";
	}

	echo "$c agencies has been migrated in total\r\n";
}

function stage_agencies($mode)
{
	global $pDB, $sDB;

	$sDB->query('DELETE a, u FROM agencies a LEFT JOIN users u ON u.id = a.user_id WHERE u.application_id = ' . APP_ID);

	if ( MODE_CLEAN_ONLY == $mode ) return;

	extract(_get_geography());

	$agencies = $pDB->fetchAll('
		SELECT u.*, u.email AS user_email, a.*
		FROM esc_agencies a
		INNER JOIN esc_users u ON u.id = a.user_id
	');

	$agencies_count = $pDB->fetchOne('
		SELECT COUNT(DISTINCT(a.user_id))
		FROM esc_agencies a
		INNER JOIN esc_users u ON u.id = a.user_id
	');

	$c = 0;
	foreach ( $agencies as $old_agency ) {
		$user = array(
			'id' => $old_agency['user_id'],
			'email' => $old_agency['user_email'],
			'username' => $old_agency['login'],
			'password' => $old_agency['password'],
			'salt_hash' => APP_USER_SALT_HASH,
			'user_type' => 'agency',
			'activation_hash' => null,
			'date_registered' => $old_agency['creation_date'],
			'status' => $old_agency['status'],
			'disabled' => 0,
			'date_last_refreshed' => null,
			'sales_user_id' => $old_agency['admin_user_id'],
			'application_id' => APP_NEW_ID,
			'city_id' => $cities[$old_agency['city']],
			'country_id' => APP_COUNTRY,
			'last_ip' => $old_agency['last_ip'],
		);

		try {
			$sDB->insert('users', $user);
		}
		catch (Exception $e) {
			echo $e->getMessage() . "\r\n";
		}

		$agency = array(
			'user_id' => $old_agency['user_id'],
			'last_modified' => null,
			'country_id' => $countries[$old_agency['country_iso']],
			'city_id' => $cities[$old_agency['city']],
			'name' => $old_agency['name'],
			'email' => $old_agency['email'],
			'web' => $old_agency['web'],
			'phone' => $old_agency['phone'],
			'phone_instructions' => $old_agency['phone_instructions'],
			'logo_hash' => null,
			'logo_ext' => null,
			'hit_count' => 0
		);

		$sDB->insert('agencies', $agency);

		$c++;
		echo "[$c/$agencies_count] Agency '{$agency['name']}'\r\n";
	}

	echo "$c agencies has been migrated in total\r\n";
}

function stage_escorts($mode)
{
	global $pDB, $sDB;


	$sDB->query('SET FOREIGN_KEY_CHECKS = 0');

	// Delete all independent escorts
	$sDB->query('
		DELETE e, ep, ec, er, el, ewt, u
		FROM escorts e
		LEFT JOIN users u ON u.id = e.user_id
		LEFT JOIN escort_profiles ep ON ep.escort_id = e.id
		LEFT JOIN escort_cities ec ON ec.escort_id = e.id
		LEFT JOIN escort_rates er ON er.escort_id = e.id
		LEFT JOIN escort_langs el ON el.escort_id = e.id
		LEFT JOIN escort_working_times ewt ON ewt.escort_id = e.id
		WHERE u.user_type = "escort" AND u.application_id = ' . APP_NEW_ID . '
	');

	// Delete all agency escorts without deleting corresponding user ('cause it is agency user)
	$sDB->query('
		DELETE e, ep, ec, er, el, ewt
		FROM escorts e
		LEFT JOIN users u ON u.id = e.user_id
		LEFT JOIN escort_profiles ep ON ep.escort_id = e.id
		LEFT JOIN escort_cities ec ON ec.escort_id = e.id
		LEFT JOIN escort_rates er ON er.escort_id = e.id
		LEFT JOIN escort_langs el ON el.escort_id = e.id
		LEFT JOIN escort_working_times ewt ON ewt.escort_id = e.id
		WHERE u.user_type = "agency" AND u.application_id = ' . APP_NEW_ID . '
	');

	if ( MODE_CLEAN_ONLY == $mode ) return;

	extract(_get_geography());

	$escorts = $pDB->fetchAll('
		SELECT  u.*, u.id AS user_id, u.email AS user_email, u.status AS user_status, u.creation_date AS u_creation_date, e.*, e.status AS escort_status, e.creation_date AS c_d, e.country_iso AS escort_country_iso, ed.*, ec.city_id AS city
		FROM esc_escorts e
		INNER JOIN esc_escort_details ed ON ed.id = e.id
		INNER JOIN esc_users u ON u.id = e.user_id
		LEFT JOIN esc_escort_cities ec ON ec.escort_id = e.id
		WHERE (u.user_type = 1 OR u.user_type = 2 OR u.user_type = 4) AND e.country_iso = "' . APP_COUNTRY_ISO .'"
		GROUP BY e.id
	');

	$escorts_count = $pDB->fetchOne('
		SELECT COUNT(DISTINCT(e.id))
		FROM esc_escorts e
		INNER JOIN esc_escort_details ed ON ed.id = e.id
		INNER JOIN esc_users u ON u.id = e.user_id
		LEFT JOIN esc_escort_cities ec ON ec.escort_id = e.id
		WHERE (u.user_type = 1 OR u.user_type = 2 OR u.user_type = 4) AND e.country_iso = "' . APP_COUNTRY_ISO .'"
	');

	$_nationalities = $sDB->fetchAll('SELECT id, iso FROM nationalities');
	$nationalities = array();
	foreach ( $_nationalities as $nat ) {
		$nationalities[$nat['iso']] = $nat['id'];
	}

	$c = 0;
	$e = 0;
	$ee = 0;
	foreach ( $escorts as $old_escort ) {
		$c++;

		// Insert user only if this is an independent escort
		if ( 1 == $old_escort['user_type'] ) {
		 	$user = array(
				'id' => $old_escort['user_id'],
				'email' => $old_escort['user_email'],
				'username' => $old_escort['login'],
				'password' => $old_escort['password'],
				'salt_hash' => APP_USER_SALT_HASH,
				'user_type' => 'escort',
				'activation_hash' => null,
				'date_registered' => $old_escort['c_d'],
				'status' => $old_escort['user_status'],
				'disabled' => 0,
				'date_last_refreshed' => $old_escort['new_refresh'],
				'sales_user_id' => $old_escort['admin_user_id'],
				'application_id' => APP_NEW_ID,
				'city_id' => $cities[$old_escort['city']],
				'country_id' => APP_COUNTRY,
				'last_ip' => $old_escort['last_ip'],
			);

			//try {
				try {
				$sDB->insert('users', $user);
				/*if ($old_escort['showname'] == '22_NORA')
				{
				 print_r($user);
				 sleep(20);
				}*/
				unset($user);
				}
				catch (Exception $e) {
					echo 'User: ' . $e->getMessage() . "\r\n";
				}

			/*}
			catch ( Exception $e ) {
				print_r($old_escort);
				//die;
			}*/
		}

		if ( 0 == $old_escort['100_verify'] || 3 == $old_escort['100_verify'] ) {
			$verify_status = 1;
		}
		elseif ( 1 == $old_escort['100_verify'] ) {
			$verify_status = 2;
		}
		elseif ( 2 == $old_escort['100_verify'] ) {
			$verify_status = 3;
		}

		$escort = array(
			'id' => $old_escort['id'],
			'user_id' => $old_escort['user_id'],
			'agency_id' => null,
			'showname' => $old_escort['showname'],
			'gender' => $old_escort['gender'],
			'date_registered' => $old_escort['c_d'],
			'country_id' => $countries[$old_escort['country_iso']],
			'city_id' => $cities[$old_escort['city']],
			'cityzone_id' => null,
			'verified_status' => $verify_status,
			'date_last_modified' => null,
			'status' => $old_escort['escort_status']
		);

		/* >>> Check if this is agency escort */
		if ( 2 == $old_escort['user_type'] ) {
			$agency_id = $sDB->fetchOne('SELECT id FROM agencies WHERE user_id = ' . $old_escort['user_id']);
			if ( ! $agency_id ) continue;

			$e++;

			$escort['agency_id'] = $agency_id;
		}
		else {
			$ee++;
		}
		/* <<< */

		try {
			$sDB->insert('escorts', $escort);
		}
		catch (Exception $e) {
			echo 'Escort: ' . $e->getMessage() . "\r\n";
			// sleep(5);
			continue;
		}

		$escort_profile = array(
			'escort_id' => $old_escort['id'],
			'height' => $old_escort['height'],
			'weight' => $old_escort['weight'],
			'bust' => $old_escort['bust'],
			'waist' => $old_escort['waist'],
			'hip' => $old_escort['hip'],
			'shoe_size' => $old_escort['shoe_size'],
			'breast_size' => $old_escort['breast_size'],
			'dress_size' => $old_escort['dress_size'],
			'is_smoker' => $old_escort['smoker'],
			'hair_color' => $old_escort['hair_color'],
			'eye_color' => $old_escort['eye_color'],
			'birth_date' => $old_escort['birth_date'],
			'characteristics' => $old_escort['characteristics'],
			'contact_phone' => $old_escort['contact_phone'],
			'contact_phone_parsed' => $old_escort['contact_phone_parsed'],
			'phone_instructions' => $old_escort['phone_instructions'],
			'contact_email' => $old_escort['contact_email'],
			'contact_web' => $old_escort['contact_web'],
			'contact_zip' => $old_escort['contact_zip'],
			'measure_units' => $old_escort['metric_system'],
			'availability' => $old_escort['available_for'],
			'sex_availability' => $old_escort['sex_availability'],
			'svc_kissing' => $old_escort['s_kissing'],
			'svc_blowjob' => $old_escort['s_blowjob'],
			'svc_cumshot' => $old_escort['s_cumshot'],
			'svc_69' => $old_escort['s_69'],
			'svc_anal' => $old_escort['s_anal'],
			'svc_additional' => $old_escort['svc_additional'],
			'ethnicity' => $old_escort['ethnic'],
			'nationality_id' => $nationalities[$old_escort['nationality_iso']],
			'admin_verified' => $old_escort['admin_verified'],
			'hit_count' => $old_escort['hit_count'],
			'_photos' => $old_escort['photos'],
			'_photos_private' => $old_escort['photos_private'],
			'_photos_verified' => $old_escort['photos_verified'],
			'_photos_private_verified' => $old_escort['photos_private_verified']
		);

		$sDB->insert('escort_profiles', $escort_profile);

		if ( isset($old_escort['contact_zip']) && $old_escort['contact_zip'] ) {
			$sDB->insert('escort_zips', array('escort_id' => $old_escort['id'], 'zip' => $old_escort['contact_zip']));
		}

		echo "[$c/$escorts_count] Escort '{$escort['showname']}' has been migrated\r\n";
	}

	echo "\r\n------- Total Agency Escorts: $e, Total Independent Escorts: $ee ------\r\n";

	echo "\r\n$c escorts has been migrated in total\r\n";

	echo "\r\nMigrating escort cities";

	$_cities = $pDB->fetchAll('
		SELECT ec.escort_id, ec.city_id
		FROM esc_escorts e
		INNER JOIN esc_escort_details ed ON ed.id = e.id
		INNER JOIN esc_users u ON u.id = e.user_id
		INNER JOIN esc_escort_cities ec ON ec.escort_id = e.id AND ec.city_id IS NOT NULL
		WHERE (u.user_type = 1 OR u.user_type = 2 OR u.user_type = 4) AND e.country_iso = "' . APP_COUNTRY_ISO .'"
	');

	foreach ( $_cities as $city ) {
	   if ( $city['city_id'] )
	   {
	      //var_dump($city);
	      //echo "\r\n";
		try {
		$sDB->insert('escort_cities', array(
			'escort_id' => $city['escort_id'],
			'city_id' => $cities[$city['city_id']]
		));
		}
		catch (Exception $e) {
			echo $e->getMessage() . "\r\n";
		}
	   }
	}

	echo " - done\r\n";

	echo "Migrating escort rates";

	$rates = $pDB->fetchAll('
		SELECT er.*
		FROM esc_escorts e
		INNER JOIN esc_escort_details ed ON ed.id = e.id
		INNER JOIN esc_users u ON u.id = e.user_id
		INNER JOIN esc_escort_rates er ON er.escort_id = e.id
		WHERE (u.user_type = 1 OR u.user_type = 2 OR u.user_type = 4) AND e.country_iso = "' . APP_COUNTRY_ISO .'"
	');

	foreach ( $rates as $rate ) {
		try {
			$sDB->insert('escort_rates', array(
				'escort_id' => $rate['escort_id'],
				'availability' => $rate['available_for'],
				'time' => intval($rate['time']),
				'time_unit' => $rate['time_unit'],
				'price' => $rate['price'],
				'currency_id' => $rate['currency_id']
			));
		}
		catch ( Exception $e ) {
			echo $e->getMessage() . "\r\n";
		}
	}

	echo "- done\r\n";

	echo "Migrating escort languages";

	$langs = $pDB->fetchAll('
		SELECT el.*
		FROM esc_escorts e
		INNER JOIN esc_escort_details ed ON ed.id = e.id
		INNER JOIN esc_users u ON u.id = e.user_id
		INNER JOIN esc_escort_languages el ON el.escort_id = e.id
		WHERE (u.user_type = 1 OR u.user_type = 2 OR u.user_type = 4) AND e.country_iso = "' . APP_COUNTRY_ISO .'"
	');

	foreach ( $langs as $lang ) {
		try {
		$sDB->insert('escort_langs', array(
			'escort_id' => $lang['escort_id'],
			'level' => $lang['level'],
			'lang_id' => $lang['lng_iso']
		));
		}
		catch (Exception $e) {
			echo $e->getMessage() . "\r\n";
		}
	}

	echo "- done\r\n";

	echo "Migrating escort working times";

	$wts = $pDB->fetchAll('
		SELECT ewt.*
		FROM esc_escorts e
		INNER JOIN esc_escort_details ed ON ed.id = e.id
		INNER JOIN esc_users u ON u.id = e.user_id
		INNER JOIN esc_escort_working_times ewt ON ewt.escort_id = e.id
		WHERE (u.user_type = 1 OR u.user_type = 2 OR u.user_type = 4) AND e.country_iso = "' . APP_COUNTRY_ISO .'"
	');

	foreach ( $wts as $wt ) {
		try {
		$sDB->insert('escort_working_times', array(
			'escort_id' => $wt['escort_id'],
			'day_index' => $wt['day'],
			'time_from' => $wt['time_from'],
			'time_to' => $wt['time_to']
		));
		}
		catch (Exception $e) {
			echo $e->getMessage() . "\r\n";
		}
	}

	echo "- done\r\n";
}

function stage_tours($mode)
{
	global $pDB, $sDB;

	// Delete all independent escorts
	$sDB->query('
		DELETE t
		FROM tours t
		INNER JOIN escorts e ON e.id = t.escort_id
		INNER JOIN users u ON u.id = e.user_id
		WHERE u.application_id = ' . APP_NEW_ID . '
	');

	if ( MODE_CLEAN_ONLY == $mode ) return;

	extract(_get_geography());

	$tours = $pDB->fetchAll('
		SELECT t.id, t.escort_id, t.date_from, t.date_to, t.email, t.phone, t.country_iso, t.city_id
		FROM esc_tours t
		INNER JOIN esc_escorts e ON e.id = t.escort_id
		INNER JOIN esc_escort_details ed ON ed.id = e.id
		INNER JOIN esc_users u ON u.id = e.user_id
		WHERE (u.user_type = 1 OR u.user_type = 2 OR u.user_type = 4) AND e.country_iso = "' . APP_COUNTRY_ISO .'"
	');

	$tours_count = $pDB->fetchOne('
		SELECT COUNT(t.id)
		FROM esc_tours t
		INNER JOIN esc_escorts e ON e.id = t.escort_id
		INNER JOIN esc_escort_details ed ON ed.id = e.id
		INNER JOIN esc_users u ON u.id = e.user_id
		WHERE (u.user_type = 1 OR u.user_type = 2 OR u.user_type = 4) AND e.country_iso = "' . APP_COUNTRY_ISO .'"
	');

	$c = 0;
	foreach ( $tours as $old_tour ) {
		$c++;

		if ( ! $countries[$old_tour['country_iso']] ||
			! $cities[$old_tour['city_id']] ) continue;

		$tour = array(
			'id' => $old_tour['id'],
			'escort_id' => $old_tour['escort_id'],
			'date_from' => $old_tour['date_from'],
			'date_to' => $old_tour['date_to'],
			'email' => $old_tour['email'],
			'phone' => $old_tour['phone'],
			'country_id' => $countries[$old_tour['country_iso']],
			'city_id' => $cities[$old_tour['city_id']]
		);

		try {
			$sDB->insert('tours', $tour);
		}
		catch ( Exception $e ) {
			echo 'Could not insert tour with id: ' . $tour['id'] . "\nReason: " . $e->getMessage() . "\n\n";
		}

		// echo "[$c/$escorts_count] Escort '{$escort['showname']}' has been migrated\r\n";
	}

	echo "\r\n$c tours has been migrated in total\r\n";
}

function stage_comments()
{
	global $pDB, $sDB;

	$comments = $pDB->fetchAll('
		SELECT *
		FROM esc_comments c
		INNER JOIN esc_escorts e ON e.id = c.escort_id
		WHERE e.country_iso = "' . APP_COUNTRY_ISO .'"
	');

	$c = 0;
	foreach ( $comments as $old_comment ) {
		$c++;
		$comment = array(
			'id' => $old_comment['id'],
			'user_id' => $old_comment['user_id'],
			'escort_id' => $old_comment['escort_id'],
			'time' => $old_comment['time'],
			'is_reply_to' => $old_comment['is_reply_to'],
			'status' => $old_comment['status'],
			'message' => $old_comment['message'],
			'thumbup_count' => $old_comment['thumbup_count'],
			'thumbdown_count' => $old_comment['thumbdown_count']
		);

		try {
			$sDB->insert('comments', $comment);
		}
		catch ( Exception $e ) {
			echo 'Could not insert comment with id: ' . $comment['id'] . "\nReason: " . $e->getMessage() . "\n\n";
		}

		// echo "[$c/$escorts_count] Escort '{$escort['showname']}' has been migrated\r\n";
	}

	echo "\r\n$c Comments has been migrated in total\r\n";
}

function stage_photos($mode)
{
	global $sDB, $pDB;

	$sDB->query('
		DELETE
			ep
		FROM escort_photos ep
		INNER JOIN escorts e ON e.id = ep.escort_id
		WHERE e.country_id = ' . APP_COUNTRY . '
	');

	if ( MODE_CLEAN_ONLY == $mode ) return;

	$escorts = $sDB->fetchAll('
		SELECT
			ep.escort_id, ep._photos, ep._photos_private, ep._photos_verified, ep._photos_private_verified
		FROM escort_profiles ep
		INNER JOIN escorts e ON e.id = ep.escort_id
		WHERE e.country_id = ' . APP_COUNTRY . '
	');

	foreach ( $escorts as $escort ) {
		list($escort_id, $photos, $photos_private, $verified, $veriried_private) = array_values($escort);
		$photos 			= explode(',', $photos);
		$photos_private 	= explode(',', $photos_private);
		$verified 			= explode(',', $verified);
		$verified_private 	= explode(',', $verified_private);

		$_photos = array();

		// Migrate general photos
		foreach ( $photos as $i => $photo ) {
			if ( ! is_numeric($photo) ) continue;

			$type = 1;
			if ( $pDB->fetchOne('SELECT is_soft FROM escort_photos WHERE escort_id = ' . $escort_id . ' AND photo_id = ' . $photo) ) {
				$type = 2;
			}

			$_photos[] = array(
				'escort_id' => $escort_id,
				'hash' =>  md5($escort_id . $ordering . $photo) /* md5(microtime(true) * rand(10000, 99999)) */,
				'ext' => 'jpg',
				'type' => $type,
				'status' => in_array($photo, $verified) ? 1 : 3,
				'is_main' => $i == 0,
				'ordering' => $i + 1,
				'_inter_index' => $photo
			);
		}

		// Migrate private photos
		foreach ( $photos_private as $i => $photo ) {
			if ( ! is_numeric($photo) ) continue;

			$_photos[] = array(
				'escort_id' => $escort_id,
				'hash' => md5($escort_id . $ordering . $photo) /* md5(microtime(true) * rand(10000, 99999)) */,
				'ext' => 'jpg',
				'type' => 3,
				'status' => in_array($photo, $verified_private) ? 1 : 3,
				'ordering' => $i + 1,
				'_inter_index' => $photo
			);
		}

		foreach ( $_photos as $photo ) {
			$sDB->insert('escort_photos', $photo);
		}
	}
}

function stage_dlphotos()
{
	global $sDB;

	$photos = $sDB->fetchAll('
		SELECT e.showname, ep.escort_id, ep._inter_index, ep.hash, ep.ext, ep.type
		FROM escort_photos ep
		INNER JOIN escorts e ON e.id = ep.escort_id
		WHERE e.country_id = ' . APP_COUNTRY . '
	');

	$photos_count = $sDB->fetchOne('
		SELECT COUNT(ep.id)
		FROM escort_photos ep
		INNER JOIN escorts e ON e.id = ep.escort_id
		WHERE e.country_id = ' . APP_COUNTRY . '
	');

	$c = 0;
	foreach ( $photos as $photo ) {
		$file = _photos_get_path($photo);
		$c++;

		echo "[$c/$photos_count] (" . round($c / $photos_count * 100)  . "%) Escort '{$photo['showname']}: '";

		if ( is_file($file) ) {
			echo "skipped: $file\r\n";
			continue;
		}

		echo "download: $file - ";

		_photos_make_path($file);

		file_put_contents($file, file_get_contents('http://pic.' . APP_HOST . '/' . $photo['escort_id'] . '/' . ($photo['type'] == 3 ? 'private/' : '') . $photo['_inter_index'] . '_orig.jpg'));

		echo "done\r\n";
	}
}

define('ESCORT_STATUS_NO_PROFILE', 			1);		// 000000001
define('ESCORT_STATUS_NO_ENOUGH_PHOTOS', 	2);		// 000000010
define('ESCORT_STATUS_NOT_APPROVED', 		4);		// 000000100
define('ESCORT_STATUS_OWNER_DISABLED', 		8);		// 000001000
define('ESCORT_STATUS_ADMIN_DISABLED', 		16);	// 000010000
define('ESCORT_STATUS_ACTIVE',				32);	// 000100000
define('ESCORT_STATUS_IS_NEW',				64);	// 001000000
define('ESCORT_STATUS_PROFILE_CHANGED',		128);	// 010000000

define('STATUS_NO_PROFILE',                 -2);
define('STATUS_NO_PHOTOS',                  -1);
define('STATUS_WAITING_APPROVAL',           -5);
define('STATUS_OWNER_DISABLED',             -3);
define('STATUS_ADMIN_DISABLED',             -4);
define('STATUS_ACTIVE',                     1);

function stage_fixstatus()
{
	global $sDB;

	$escorts = $sDB->fetchAll('
		SELECT e.id, e.showname, COUNT(DISTINCT(eph.id)) AS photos_count, e.status
		FROM escorts e
		INNER JOIN users u ON u.id = e.user_id
		INNER JOIN escort_profiles ep ON ep.escort_id = e.id
		LEFT JOIN escort_photos eph ON eph.escort_id = e.id AND eph.type <> 3
		WHERE e.country_id = ' . APP_COUNTRY . '
		GROUP BY e.id
	');

	foreach ( $escorts as $escort ) {
		$status = 0;
		$ss = array();

		if ( APP_MIN_PHOTOS > $escort['photos_count'] || STATUS_NO_PHOTOS == $escort['status'] ) {
			$status |= ESCORT_STATUS_NO_ENOUGH_PHOTOS;
			$ss[] = 'no_enough_photos';
		}

		if ( STATUS_NO_PROFILE == $escort['status'] ) {
			$status |= ESCORT_STATUS_NO_PROFILE;
			$ss[] = 'no_profile';
		}

		if ( STATUS_WAITING_APPROVAL == $escort['status'] && ! ( $status & ESCORT_STATUS_NO_PHOTOS ) ) {
			$status |= ESCORT_STATUS_NOT_APPROVED;
			$ss[] = 'not_approved';
		}

		if ( STATUS_ADMIN_DISABLED == $escort['status'] ) {
			$status |= ESCORT_STATUS_ADMIN_DISABLED;
			$ss[] = 'admin_disabled';
		}

		if ( STATUS_OWNER_DISABLED == $escort['status'] ) {
			$status |= ESCORT_STATUS_OWNER_DISABLED;
			$ss[] = 'owner_disabled';
		}

		if ( $status & ESCORT_STATUS_NO_PROFILE && 0 == $escort['photos_count'] ) {
			$status |= ESCORT_STATUS_IS_NEW;
			$ss[] = 'is_new';
		}

		if ( ! ( $status & ESCORT_STATUS_NO_ENOUGH_PHOTOS ) && STATUS_ACTIVE == $escort['status'] ) {
			$status |= ESCORT_STATUS_ACTIVE;
			$ss[] = 'active';
		}

		echo $escort['showname'] . ': ' . implode(' ', $ss) . "\r\n";

		$sDB->query('UPDATE escorts SET status = ' . $status . ' WHERE id = ' . $escort['id']);
	}
}

function stage_translations($mode, $section)
{
	$section_func = 'stage_translations_section_' . $section;
	if ( ! is_callable($section_func) ) {
		die('Invalid section');
	}

	call_user_func($section_func);
}

function stage_translations_section_countries()
{
	global $sDB, $app_langs;

	$countries = $sDB->fetchAll('SELECT id, iso FROM countries');

	foreach ( $app_langs as $lang_id ) {
		require('data/countries_' . $lang_id . '.php');

		foreach ( $countries as $country ) {
			$title = isset($LNG['country_' . $country['iso']]) ? '"' . $LNG['country_' . $country['iso']] . '"' : 'NULL';
			$sDB->query('UPDATE countries SET title_' . $lang_id . ' = ' . $title . ' WHERE id = ' . $country['id']);
		}
	}
}

function stage_translations_section_regions()
{
	global $sDB, $app_langs;

	$regions = $sDB->fetchAll('
		SELECT r.id, r.state, r.slug, c.iso
		FROM regions r
		INNER JOIN countries c ON c.id = r.country_id
	');

	/*
	foreach ( $regions as $region ) {
		if ( preg_match('/^region\-it\-/', $region['slug']) ) {
			$sDB->query('UPDATE regions SET slug = "' . preg_replace('/^region\-it\-/', '', $region['slug']) . '" WHERE id = ' . $region['id']);
		}
	}
	*/

	foreach ( $app_langs as $lang_id ) {
		require('data/regions_' . $lang_id . '.php');

		foreach ( $regions as $region ) {
			$type = is_null($region['state']) ? 'region' : 'state';
			$key = $type . '_' . $region['iso'] . '_' . str_replace('-', '_', $region['slug']);

			$title = isset($LNG[$key]) ? '"' . $LNG[$key] . '"' : '"' . $key . '"';

			$sDB->query('UPDATE regions SET title_' . $lang_id . ' = ' . $title . ' WHERE id = ' . $region['id']);
		}
	}
}

function stage_translations_section_cities()
{
	global $sDB, $app_langs;

	$cities = $sDB->fetchAll('
		SELECT ct.id, ct.slug, c.iso
		FROM cities ct
		INNER JOIN countries c ON c.id = ct.country_id
	');

	/*
	foreach ( $cities as $city ) {
		if ( preg_match('/^city\-us\-/', $city['slug']) ) {
			$sDB->query('UPDATE cities SET slug = "' . preg_replace('/^city\-us\-/', '', $city['slug']) . '" WHERE id = ' . $city['id']);
		}
	}
	*/

	foreach ( $app_langs as $lang_id ) {
		require('data/cities_' . $lang_id . '.php');

		foreach ( $cities as $city ) {
			$key = 'city_' . $city['iso'] . '_' . str_replace('-', '_', $city['slug']);
			$title = isset($LNG[$key]) ? '"' . $LNG[$key] . '"' : 'NULL'/* '"' . $key . '"' */;

			$sDB->query('UPDATE cities SET title_' . $lang_id . ' = ' . $title . ' WHERE id = ' . $city['id']);
		}
	}
}

function stage_cityzones()
{
	global $sDB;

	$sDB->query('TRUNCATE TABLE cityzones');

	$cities = $sDB->fetchAll('
		SELECT ct.id, ct.slug
		FROM cities ct
	');

	$app_langs = $sDB->fetchAll('
		SELECT id
		FROM langs
	');

	require('data/city_zones.php');
	$c = 0;
	foreach ( $cities as $city ) {
		foreach ( $LNG as $z_k => $zone ) {
			if( preg_match('/zone_(it|gb)_' . $city['slug'] . '/', $z_k) ) {

				$a = array();
				preg_match('/^zone_(it|gb)_(.+)$/i', $z_k, $a);
				$slug = str_replace('_', '-', $a[2]);


				$data = array('city_id' => $city['id'], 'slug' => $slug);
				foreach ( $app_langs as $lang_id ) {
					$data = array_merge($data, array('title_' . $lang_id['id'] => $zone));
				}

				$sDB->insert('cityzones',  $data);

				$c++;
				echo $c . " - City ID: " . $city['id'] . ' - ' . $city['slug'] . ', Cityzone: '. $zone . ' - ' . $z_k . "\r\n";
			}
		}
	}
}

function stage_fix_cityzones()
{
	global $pDB, $sDB;

	require('data/city_zones.php');

	$sDB->query('TRUNCATE TABLE escort_cityzones');

	$escort_city_zones = $pDB->fetchAll(
		'SELECT * FROM esc_escort_cityzones'
	);

	$c = 0;
	foreach ( $escort_city_zones as $esc_city_zone ) {

		$zone_slug = $pDB->fetchOne('SELECT name FROM esc_city_zones WHERE id = ' . $esc_city_zone['city_zone_id']);

		$zone_name = $LNG[$zone_slug];

		$new_zone_id = $sDB->fetchOne('SELECT id FROM cityzones WHERE title_en = "' . $zone_name . '"');

		if ( $new_zone_id ) {
			$data = array('escort_id' => $esc_city_zone['escort_id'], 'city_zone_id' => $new_zone_id);
			$sDB->insert('escort_cityzones', $data);
			$c++;
			echo "Escort ID: " . $esc_city_zone['escort_id'] . ", Zone: " . $zone_name . "\n\r";
		}
		else {
			echo "Cityzone with id " . $esc_city_zone['city_zone_id'] . " not found \n\r";
		}
	}
	echo $c . " escort cityzones has been inserted. \r\n";
}

function stage_orders()
{
	global $sDB, $pDB, $app_langs;

	$sDB->query('
		DELETE o, op, opp FROM orders o
		INNER JOIN order_packages op ON op.order_id = o.id
		INNER JOIN order_package_products opp ON opp.order_package_id = op.id
	');

	$orders = $pDB->fetchAll('
		SELECT o.*, e.gender FROM esc_orders o
		INNER JOIN esc_users u ON u.id = o.user_id AND u.user_type = 1
		INNER JOIN esc_escorts e ON e.user_id = u.id
		GROUP BY o.id
	');

	$count = $pDB->fetchOne('
		SELECT COUNT(DISTINCT(o.id)) FROM esc_orders o
		INNER JOIN esc_users u ON u.id = o.user_id
		INNER JOIN esc_escorts e ON e.user_id = u.id
	');

	$c = 0;
	foreach ($orders as $order) {
		$c++;
		echo "[$c/$count]\n";

		$new_order = array(
			'id' => $order['id'],
			'user_id' => $order['user_id'],
			'backend_user_id' => $order['admin_user_id'],
			'order_details' => $order['order_details'],
			'payment_date' => $order['payment_time'],
			'status' => $order['status'],
			'order_date' => $order['order_time'],
			'discount' => $order['discount'],
			'discount_fixed' => $order['eur_discount'],
			'surcharge_fixed' => $order['surcharge_fixed'],
			'system_order_id' => $order['system_order_number'],
			'price' => $order['price']
		);

		$sDB->insert('orders', $new_order);

		$order_package = array(
			'order_id' => $order['id'],
			'escort_id' => $order['escort_id'],
			'package_id' => $order['package_id'],
			'application_id' => $order['application_id'],
			'period' => $order['package_period'],
			'status' => $order['package_activated'],
			'activation_date' => $order['package_activation_date'],
			'price' => $order['price']
		);

		$order_package_id = $sDB->insert('order_packages', $order_package);

		foreach ( explode(',', $order['products']) as $product_id ) {
			$product_id = intval(trim($product_id));

			if ( 0 == $product_id ) continue;

			$product = array(
				'order_package_id' => $order_package_id,
				'product_id' => $product_id,
				'type' => 1,
				'price' => _orders_get_product_price($product_id, $order)
			);

			$sDB->insert('order_package_products', $product);
		}

		foreach ( explode(',', $order['addon_products']) as $product_id ) {
			$product_id = intval(trim($product_id));

			if ( 0 == $product_id ) continue;

			$product = array(
				'order_package_id' => $order_package_id,
				'product_id' => $product_id,
				'type' => 2,
				'price' => _orders_get_product_price($product_id, $order)
			);

			$sDB->insert('order_package_products', $product);
		}
	}
}


function _orders_get_product_price($product_id, $order)
{
	global $sDB;

	$price = $sDB->fetchOne('
		SELECT pp.price FROM product_prices pp
		WHERE
			pp.product_id = ' . $product_id . ' AND
			pp.user_type = 1 AND pp.gender = ' . $order['gender'] . ' AND
			application_id = ' . $order['application_id'] . '
	');

	return floatval($price);
}










function _photos_get_path($photo)
{
	$catalog = $photo['escort_id'];

	$parts = array();
	if ( strlen($catalog) > 2 ) {
		$parts[] = substr($catalog, 0, 2);
		$parts[] = substr($catalog, 2);
	}
	else {
		$parts[] = '_';
		$parts[] = $catalog;
	}

	if ( 3 == $photo['type'] ) {
		$parts[] = 'private';
	}

	$catalog = implode('/', $parts);

	$path = 'pics/' . APP_HOST . '/' . $catalog . '/' . $photo['hash'] . '.' . $photo['ext'];

	return $path;
}

function _photos_make_path($path)
{
	$path = explode('/', $path);
	$path = array_slice($path, 0, count($path));

	for ( $i = 1; $i < count($path); $i++ ) {
		if ( ! is_dir($p = implode('/', array_slice($path, 0, $i))) ) {
			mkdir($p);
		}
	}
}








define('MODE_CLEAN_ONLY', 1);
$mode_map = array(
	null => null,
	'clean_only' => MODE_CLEAN_ONLY
);

$stage = isset($_REQUEST['stage']) ? $_REQUEST['stage'] : null;
$mode = isset($_REQUEST['mode']) ? $_REQUEST['mode'] : null;

if ( is_null($stage) ) {
	$stage = @$_SERVER['argv'][1];
}

if ( is_null($mode) ) {
	$mode = @$_SERVER['argv'][2];
}

$mode = $mode_map[$mode];

$args = array_slice($_SERVER['argv'], 1);

$func = 'stage_' . $stage;

if ( ! is_callable($func) ) {
	die('Invalid migration stage');
}

echo '---> ' . ucfirst($stage) . ' stage has been started' . "\r\n\r\n";

call_user_func_array($func, array($mode) + $args);
