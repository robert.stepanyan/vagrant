<?php



$LNG['city_ar_buenos_aires'] = "Buenos Aires";

$LNG['city_at_innsbruck'] = "Innsbruck";

$LNG['city_at_klagenfurt'] = "Klagenfurt";

$LNG['city_at_vienna'] = "Vienna";

$LNG['city_au_brisbane'] = "Brisbane";

$LNG['city_au_melbourne'] = "Melbourne";

$LNG['city_au_perth'] = "Perth";

$LNG['city_au_sydney'] = "Sydney";

$LNG['city_be_antwerpen'] = "Antwerpen";

$LNG['city_be_brussels'] = "Brussels";

$LNG['city_bg_sofia'] = "Sofia";

$LNG['city_br_rio_de_janeiro'] = "Rio de Janeiro";

$LNG['city_br_sao_paulo'] = "São Paulo";

$LNG['city_by_minsk'] = "Minsk";

$LNG['city_ca_montreal'] = "Montreal";

$LNG['city_ca_toronto'] = "Toronto";

$LNG['city_ca_vancouver'] = "Vancouver";

$LNG['city_ch_basel'] = "Basle";

$LNG['city_ch_bern'] = "Berne";

$LNG['city_ch_freiburg'] = "Freiburg";

$LNG['city_ch_genf'] = "Geneva";

$LNG['city_ch_luzern'] = "Lucerne";

$LNG['city_ch_neuenburg'] = "Neuenburg";

$LNG['city_ch_schaffhausen'] = "Schaffhausen";

$LNG['city_ch_schwyz'] = "Schwyz";

$LNG['city_ch_solothurn'] = "Solothurn";

$LNG['city_ch_stgallen'] = "St. Gall";

$LNG['city_ch_zug'] = "Zug";

$LNG['city_ch_zurich'] = "Zurich";

//CUBIX Added By David
$LNG['city_ch_paffnau'] = "Paffnau";
$LNG['city_ch_emmenbrucke'] = "Emmenbrücke";
$LNG['city_ch_visp'] = "Visp";
$LNG['city_ch_gumligen'] = "Gümligen";
$LNG['city_ch_interlaken'] = "Interlaken";
$LNG['city_ch_lenzburg'] = "Lenzburg";
$LNG['city_ch_brugg'] = "Brugg";
$LNG['city_ch_brunnen'] = "Brunnen ";
//


$LNG['city_co_bogota'] = "Bogota";

$LNG['city_cu_havana'] = "Havana";

$LNG['city_cy_nikosia'] = "Nikosia";

$LNG['city_cz_benesov'] = "Benešov";

$LNG['city_cz_beroun'] = "Beroun";

$LNG['city_cz_blansko'] = "Blansko";

$LNG['city_cz_breclav'] = "Breclav";

$LNG['city_cz_brno'] = "Brno";

$LNG['city_cz_ceskalipa'] = "Ceská Lípa";

$LNG['city_cz_ceskebudejovice'] = "Ceské Budejovice";

$LNG['city_cz_ceskykrumlov'] = "Ceský Krumlov";

$LNG['city_cz_cheb'] = "Cheb";

$LNG['city_cz_chomutov'] = "Chomutov";

$LNG['city_cz_chrudim'] = "Chrudim";

$LNG['city_cz_decin'] = "Decín";

$LNG['city_cz_domazlice'] = "Domažlice";

$LNG['city_cz_frydekmistek'] = "Frýdek-Místek";

$LNG['city_cz_havlickuvbrod'] = "Havlíckuv Brod";

$LNG['city_cz_hodonin'] = "Hodonín";

$LNG['city_cz_hradeckralove'] = "Hradec Králové";

$LNG['city_cz_jablonecnadnisou'] = "Jablonec nad Nisou";

$LNG['city_cz_jesenik'] = "Jeseník";

$LNG['city_cz_jicin'] = "Jicín";

$LNG['city_cz_jihlava'] = "Jihlava";

$LNG['city_cz_jindrichuvhradec'] = "Jindrichuv Hradec";

$LNG['city_cz_karlovyvary'] = "Karlovy Var";

$LNG['city_cz_karvina'] = "Karviná";

$LNG['city_cz_kladno'] = "Kladno";

$LNG['city_cz_klatovy'] = "Klatovy";

$LNG['city_cz_kolin'] = "Kolín";

$LNG['city_cz_kromeriz'] = "Kromeríž";

$LNG['city_cz_kutnahora'] = "Kutná Hora";

$LNG['city_cz_liberec'] = "Liberec";

$LNG['city_cz_litomerice'] = "Litomerice";

$LNG['city_cz_louny'] = "Louny";

$LNG['city_cz_melnik'] = "Melník";

$LNG['city_cz_mladaboleslav'] = "Mladá Boleslav";

$LNG['city_cz_most'] = "Most";

$LNG['city_cz_nachod'] = "Náchod";

$LNG['city_cz_novyjicin'] = "Nový Jicín";

$LNG['city_cz_nymburk'] = "Nymburk";

$LNG['city_cz_olomouc'] = "Olomouc";

$LNG['city_cz_opava'] = "Opava";

$LNG['city_cz_ostrava'] = "Ostrava";

$LNG['city_cz_pardubice'] = "Pardubice";

$LNG['city_cz_pelhrimov'] = "Pelhrimov";

$LNG['city_cz_pisek'] = "Písek";

$LNG['city_cz_plzen'] = "Plzen";

$LNG['city_cz_prachatice'] = "Prachatice";

$LNG['city_cz_prague'] = "Prague";

$LNG['city_cz_prerov'] = "Prerov";

$LNG['city_cz_pribram'] = "Príbram";

$LNG['city_cz_prostejov'] = "Prostejov";

$LNG['city_cz_rakovnik'] = "Rakovník";

$LNG['city_cz_rokycany'] = "Rokycany";

$LNG['city_cz_rychnovnadkneznou'] = "Rychnov nad Knežnou";

$LNG['city_cz_semily'] = "Semily";

$LNG['city_cz_sokolov'] = "Sokolov";

$LNG['city_cz_strakonice'] = "Strakonice";

$LNG['city_cz_sumperk'] = "Šumperk";

$LNG['city_cz_svitavy'] = "Svitavy";

$LNG['city_cz_tabor'] = "Tábor";

$LNG['city_cz_tachov'] = "Tachov";

$LNG['city_cz_teplice'] = "Teplice";

$LNG['city_cz_trebic'] = "Trebíc";

$LNG['city_cz_trutnov'] = "Trutnov";

$LNG['city_cz_uherskehradiste'] = "Uherské Hradište";

$LNG['city_cz_ustinadlabem'] = "Ústí nad Labem";

$LNG['city_cz_ustinadorlici'] = "Ústí nad Orlicí";

$LNG['city_cz_vsetin'] = "Vsetín";

$LNG['city_cz_vyskov'] = "Vyškov";

$LNG['city_cz_zdarnadsazavou'] = "Ždár nad Sázavou";

$LNG['city_cz_zlin'] = "Zlín";

$LNG['city_cz_znojmo'] = "Znojmo";

$LNG['city_de_berlin'] = "Berlin";

$LNG['city_de_cologne'] = "Cologne";

$LNG['city_de_dusseldorf'] = "Dusseldorf";

$LNG['city_de_frankfurt_am_main'] = "Frankfurt-am-main";

$LNG['city_de_hamburg'] = "Hamburg";

$LNG['city_de_hannover'] = "Hannover";

$LNG['city_de_munich'] = "Munich";

$LNG['city_de_stuttgart'] = "Stuttgart";

$LNG['city_dk_copenhagen'] = "Copenhagen";

$LNG['city_ee_tallinn'] = "Tallinn";

$LNG['city_es_barcelona'] = "Barcelona";

$LNG['city_es_canary_islands'] = "Canary Islands";

$LNG['city_es_madrid'] = "Madrid";

$LNG['city_es_marbella'] = "Marbella";

$LNG['city_fi_helsinki'] = "Helsinki";

$LNG['city_fr_aix_en_provence'] = "Aix-En-Provence";

$LNG['city_fr_amiens'] = "Amiens";

$LNG['city_fr_angers'] = "Angers";

$LNG['city_fr_antibes'] = "Antibes";

$LNG['city_fr_auray'] = "Auray";

$LNG['city_fr_avignon'] = "Avignon";

$LNG['city_fr_belfort'] = "Belfort";

$LNG['city_fr_besancon'] = "Besancon";

$LNG['city_fr_biarritz'] = "Biarritz";

$LNG['city_fr_bordeaux'] = "Bordeaux";

$LNG['city_fr_brest'] = "Brest";

$LNG['city_fr_caen'] = "Caen";

$LNG['city_fr_cannes'] = "Cannes";

$LNG['city_fr_chateauroux'] = "Chateauroux";

$LNG['city_fr_clermont_ferrand'] = "Clermont-Ferrand";

$LNG['city_fr_deauville'] = "Deauville";

$LNG['city_fr_dijon'] = "Dijon";

$LNG['city_fr_frejus'] = "Fréjus";

$LNG['city_fr_grenoble'] = "Grenoble";

$LNG['city_fr_le_havre'] = "Le Havre";

$LNG['city_fr_le_mans'] = "Le Mans";

$LNG['city_fr_lille'] = "Lille";

$LNG['city_fr_limoges'] = "Limoges";

$LNG['city_fr_lorient'] = "Lorient";

$LNG['city_fr_lyon'] = "Lyon";

$LNG['city_fr_marseille'] = "Marseille";

$LNG['city_fr_metz'] = "Metz";

$LNG['city_fr_montargis'] = "Montargis";

$LNG['city_fr_montpellier'] = "Montpellier";

$LNG['city_fr_mulhouse'] = "Mulhouse";

$LNG['city_fr_nantes'] = "Nantes";

$LNG['city_fr_narbonne'] = "Narbonne";

$LNG['city_fr_nice'] = "Nice";

$LNG['city_fr_nimes'] = "Nimes";

$LNG['city_fr_niort'] = "Niort";

$LNG['city_fr_orleans'] = "Orleans";

$LNG['city_fr_paris'] = "Paris";

$LNG['city_fr_perpignan'] = "Perpignan";

$LNG['city_fr_poitiers'] = "Poitiers";

$LNG['city_fr_quimper'] = "Quimper";

$LNG['city_fr_reims'] = "Reims";

$LNG['city_fr_rennes'] = "Rennes";

$LNG['city_fr_saint_etienne'] = "Saint-Etienne";

$LNG['city_fr_saint_tropez'] = "Saint Tropez";

$LNG['city_fr_strasbourg'] = "Strasbourg";

$LNG['city_fr_toulon'] = "Toulon";

$LNG['city_fr_toulouse'] = "Toulouse";

$LNG['city_fr_tours'] = "Tours";

$LNG['city_fr_vannes'] = "Vannes";

$LNG['city_fr_villeurbanne'] = "Villeurbanne";

$LNG['city_gb_belfast'] = "Belfast";

$LNG['city_gb_birmingham'] = "Birmingham";

$LNG['city_gb_cambridge'] = "Cambridge";

$LNG['city_gb_cardiff'] = "Cardiff";

$LNG['city_gb_edinburgh'] = "Edinburgh";

$LNG['city_gb_glasgow'] = "Glasgow";

$LNG['city_gb_leeds'] = "Leeds";

$LNG['city_gb_london'] = "London";

$LNG['city_gb_manchester'] = "Manchester";

$LNG['city_gb_yorkshire'] = "Yorkshire";

$LNG['city_gr_athens'] = "Athens";

$LNG['city_hr_zagreb'] = "Zagreb";

$LNG['city_hu_budapest'] = "Budapest";

$LNG['city_ie_dublin'] = "Dublin";

$LNG['city_ie_limerick'] = "Limerick";

$LNG['city_il_jerusalem'] = "Jerusalem";

$LNG['city_il_tel_aviv'] = "Tel Aviv";

$LNG['city_is_reykjavik'] = "Reykjavik";

$LNG['city_it_agrigento'] = "Agrigento";

$LNG['city_it_alba'] = "Alba";

$LNG['city_it_alessandria'] = "Alessandria";

$LNG['city_it_ancona'] = "Ancona";

$LNG['city_it_arezzo'] = "Arezzo";

$LNG['city_it_arona'] = "Arona";

$LNG['city_it_ascoli_piceno'] = "Ascoli Piceno";

$LNG['city_it_asti'] = "Asti";

$LNG['city_it_bari'] = "Bari";

$LNG['city_it_bergamo'] = "Bergamo";

$LNG['city_it_bologna'] = "Bologna";

$LNG['city_it_bolzano'] = "Bolzano";

$LNG['city_it_brescia'] = "Brescia";

$LNG['city_it_brianza'] = "Brianza";

$LNG['city_it_cagliari'] = "Cagliari";

$LNG['city_it_caltanisetta'] = "Caltanisetta";

$LNG['city_it_catania'] = "Catania";

$LNG['city_it_catanzaro'] = "Catanzaro";

$LNG['city_it_como'] = "Como";

$LNG['city_it_cremona'] = "Cremona";

$LNG['city_it_cuneo'] = "Cuneo";

$LNG['city_it_desenzano'] = "Desenzano";

$LNG['city_it_dormelletto'] = "Dormelletto";

$LNG['city_it_empoli'] = "Empoli";

$LNG['city_it_faenza'] = "Faenza";

$LNG['city_it_ferrara'] = "Ferrara";

$LNG['city_it_florence'] = "Florence";

$LNG['city_it_foggia'] = "Foggia";

$LNG['city_it_forli'] = "Forli";

$LNG['city_it_frosinone'] = "Frosinone";

$LNG['city_it_genova'] = "Genova";

$LNG['city_it_grosseto'] = "Grosseto";

$LNG['city_it_la_spezia'] = "La Spezia";

$LNG['city_it_lago_maggiore'] = "Lago Maggiore";

$LNG['city_it_lake_garda'] = "Lake Garda";

$LNG['city_it_latina'] = "Latina";

$LNG['city_it_lecce'] = "Lecce";

$LNG['city_it_lecco'] = "Lecco";

$LNG['city_it_legnano'] = "Legnano";

$LNG['city_it_livorno'] = "Livorno";

$LNG['city_it_lodi'] = "Lodi";

$LNG['city_it_lucca'] = "Lucca";

$LNG['city_it_lugano'] = "Lugano";

$LNG['city_it_macerata'] = "Macerata";

$LNG['city_it_mantova'] = "Mantova";

$LNG['city_it_marina_di_massa'] = "Marina di Massa";

$LNG['city_it_messina'] = "Messina";

$LNG['city_it_milan'] = "Milan";

$LNG['city_it_modena'] = "Modena";

$LNG['city_it_monopoli'] = "Monopoli";

$LNG['city_it_montecatini'] = "Montecatini";

$LNG['city_it_naples'] = "Naples";

$LNG['city_it_novara'] = "Novara";

$LNG['city_it_olbia'] = "Olbia";

$LNG['city_it_padova'] = "Padova";

$LNG['city_it_palermo'] = "Palermo";

$LNG['city_it_parma'] = "Parma";

$LNG['city_it_pavia'] = "Pavia";

$LNG['city_it_perugia'] = "Perugia";

$LNG['city_it_pescara'] = "Pescara";

$LNG['city_it_piacenza'] = "Piacenza";

$LNG['city_it_pisa'] = "Pisa";

$LNG['city_it_pistoia'] = "Pistoia";

$LNG['city_it_pordenone'] = "Pordenone";

$LNG['city_it_portocervo'] = "Portocervo";

$LNG['city_it_portofino'] = "Portofino";

$LNG['city_it_portorotondo'] = "Portorotondo";

$LNG['city_it_prato'] = "Prato";

$LNG['city_it_ragusa'] = "Ragusa";

$LNG['city_it_ravenna'] = "Ravenna";

$LNG['city_it_reggio_calabria'] = "Reggio Calabria";

$LNG['city_it_region_emilia'] = "Region Emilia";

$LNG['city_it_riccione'] = "Riccione";

$LNG['city_it_rimini'] = "Rimini";

$LNG['city_it_rivoltella'] = "Rivoltella";

$LNG['city_it_rome'] = "Rome";

$LNG['city_it_saluzzo'] = "Saluzzo";

$LNG['city_it_san_bonifacio'] = "San Bonifacio";

$LNG['city_it_sanremo'] = "Sanremo";

$LNG['city_it_saronno'] = "Saronno";

$LNG['city_it_savona'] = "Savona";

$LNG['city_it_siena'] = "Siena";

$LNG['city_it_siracusa'] = "Siracusa";

$LNG['city_it_sirmione'] = "Sirmione";

$LNG['city_it_sondrio'] = "Sondrio";

$LNG['city_it_taormina'] = "Taormina";

$LNG['city_it_trento'] = "Trento";

$LNG['city_it_treviso'] = "Treviso";

$LNG['city_it_trieste'] = "Trieste";

$LNG['city_it_turin'] = "Turin";

$LNG['city_it_udine'] = "Udine";

$LNG['city_it_varese'] = "Varese";

$LNG['city_it_venice'] = "Venice";

$LNG['city_it_vercelli'] = "Vercelli";

$LNG['city_it_verona'] = "Verona";

$LNG['city_it_versilia'] = "Versilia";

$LNG['city_it_viareggio'] = "Viareggio";

$LNG['city_it_vicenza'] = "Vicenza";

$LNG['city_jp_tokyo'] = "Tokyo";

$LNG['city_li_vaduz'] = "Vaduz";

$LNG['city_lt_vilnius'] = "Vilnius";

$LNG['city_lu_luxembourg'] = "Luxembourg";

$LNG['city_lv_riga'] = "Riga";

$LNG['city_mc_monaco'] = "Monaco";

$LNG['city_mx_mexico_city'] = "Mexico City";

$LNG['city_nl_amsterdam'] = "Amsterdam";

$LNG['city_no_oslo'] = "Oslo";

$LNG['city_pl_cracovie'] = "Cracovie";

$LNG['city_pl_poznan'] = "Poznañ";

$LNG['city_pl_varsovie'] = "Varsovie";

$LNG['city_pl_warsaw'] = "Warsaw";

$LNG['city_pt_lisbon'] = "Lisbon";

$LNG['city_pt_madeira'] = "Madeira";

$LNG['city_ro_bucharest'] = "Bucharest";

$LNG['city_ru_belgorod'] = "Belgorod";

$LNG['city_ru_moscow'] = "Moscow";

$LNG['city_ru_saint_petersburg'] = "Saint Petersburg";

$LNG['city_se_stockholm'] = "Stockholm";

$LNG['city_si_ljubljana'] = "Ljubljana";

$LNG['city_sk_bratislava'] = "Bratislava";

$LNG['city_tr_ankara'] = "Ankara";

$LNG['city_tr_istanbul'] = "Istanbul";

$LNG['city_ua_kiev'] = "Kiev";

$LNG['city_ua_odessa'] = "Odessa";

$LNG['city_ve_caracas'] = "Caracas";

$LNG['city_za_cape_town'] = "Cape Town";



$LNG['city_it_avellino'] = "Avellino";

$LNG['city_it_salerno'] = "Salerno";

$LNG['city_it_potenza'] = "Potenza";

$LNG['city_it_taranto'] = "Taranto";

$LNG['city_fr_colmar'] = "Colmar";

$LNG['city_fr_nancy'] = "Nancy";



$LNG['city_gb_aberdeen'] = "Aberdeen";

$LNG['city_gb_barnsley'] = "Barnsley";

$LNG['city_gb_bath'] = "Bath";

$LNG['city_gb_bedford'] = "Bedford";

$LNG['city_gb_birkenhead'] = "Birkenhead";

$LNG['city_gb_blackburn'] = "Blackburn";

$LNG['city_gb_blackpool'] = "Blackpool";

$LNG['city_gb_bolton'] = "Bolton";

$LNG['city_gb_bournemouth'] = "Bournemouth";

$LNG['city_gb_bradford'] = "Bradford";

$LNG['city_gb_brighton'] = "Brighton";

$LNG['city_gb_burnley'] = "Burnley";

$LNG['city_gb_bury'] = "Bury";

$LNG['city_gb_carlisle'] = "Carlisle";

$LNG['city_gb_cheltenham'] = "Cheltenham";

$LNG['city_gb_chester'] = "Chester";

$LNG['city_gb_cleethorpes'] = "Cleethorpes";

$LNG['city_gb_corby'] = "Corby";

$LNG['city_gb_coventry'] = "Coventry";

$LNG['city_gb_crawley'] = "Crawley";

$LNG['city_gb_darlington'] = "Darlington";

$LNG['city_gb_derby'] = "Derby";

$LNG['city_gb_doncaster'] = "Doncaster";

$LNG['city_gb_dover'] = "Dover";

$LNG['city_gb_dudley'] = "Dudley";

$LNG['city_gb_durham'] = "Durham";

$LNG['city_gb_exeter'] = "Exeter";

$LNG['city_gb_falmouth'] = "Falmouth";

$LNG['city_gb_glastonbury'] = "Glastonbury";

$LNG['city_gb_grantham'] = "Grantham";

$LNG['city_gb_guildford'] = "Guildford";

$LNG['city_gb_halifax'] = "Halifax";

$LNG['city_gb_harrogate'] = "Harrogate";

$LNG['city_gb_hastings'] = "Hastings";

$LNG['city_gb_hertford'] = "Hertford";

$LNG['city_gb_high_wycombe'] = "High Wycombe";

$LNG['city_gb_holmfirth'] = "Holmfirth";

$LNG['city_gb_huddersfield'] = "Huddersfield";

$LNG['city_gb_hull'] = "Hull";

$LNG['city_gb_ipswich'] = "Ipswich";

$LNG['city_gb_jedburgh'] = "Jedburgh";

$LNG['city_gb_kettering'] = "Kettering";

$LNG['city_gb_knutsford'] = "Knutsford";

$LNG['city_gb_lancaster'] = "Lancaster";

$LNG['city_gb_leicester'] = "Leicester";

$LNG['city_gb_lichfield'] = "Lichfield";

$LNG['city_gb_lincoln'] = "Lincoln";

$LNG['city_gb_liverpool'] = "Liverpool";

$LNG['city_gb_loughborough'] = "Loughborough";

$LNG['city_gb_mablethorpe'] = "Mablethorpe";

$LNG['city_gb_macclesfield'] = "Macclesfield";

$LNG['city_gb_maidstone'] = "Maidstone";

$LNG['city_gb_malham'] = "Malham";

$LNG['city_gb_middlesbrough'] = "Middlesbrough";

$LNG['city_gb_milton'] = "Milton";

$LNG['city_gb_keynes'] = "Keynes";

$LNG['city_gb_newark'] = "Newark";

$LNG['city_gb_newbury'] = "Newbury";

$LNG['city_gb_newcastle'] = "Newcastle";

$LNG['city_gb_newmarket'] = "Newmarket";

$LNG['city_gb_newport'] = "Newport";

$LNG['city_gb_north_shields'] = "North Shields";

$LNG['city_gb_northampton'] = "Northampton";

$LNG['city_gb_norwich'] = "Norwich";

$LNG['city_gb_nottingham'] = "Nottingham";

$LNG['city_gb_nuneaton'] = "Nuneaton";

$LNG['city_gb_oldham'] = "Oldham";

$LNG['city_gb_otley'] = "Otley";

$LNG['city_gb_oxford'] = "Oxford";

$LNG['city_gb_peterborough'] = "Peterborough";

$LNG['city_gb_plymouth'] = "Plymouth";

$LNG['city_gb_portsmouth'] = "Portsmouth";

$LNG['city_gb_preston'] = "Preston";

$LNG['city_gb_reading'] = "Reading";

$LNG['city_gb_redditch'] = "Redditch";

$LNG['city_gb_retford'] = "Retford";

$LNG['city_gb_richmond'] = "Richmond";

$LNG['city_gb_ripley'] = "Ripley";

$LNG['city_gb_ripon'] = "Ripon";

$LNG['city_gb_rochdale'] = "Rochdale";

$LNG['city_gb_rochester'] = "Rochester";

$LNG['city_gb_rotherham'] = "Rotherham";

$LNG['city_gb_rugby'] = "Rugby";

$LNG['city_gb_runcorn'] = "Runcorn";

$LNG['city_gb_salford'] = "Salford";

$LNG['city_gb_sandown'] = "Sandown";

$LNG['city_gb_scarborough'] = "Scarborough";

$LNG['city_gb_scunthorpe'] = "Scunthorpe";

$LNG['city_gb_seaford'] = "Seaford";

$LNG['city_gb_sellafield'] = "Sellafield";

$LNG['city_gb_sheffield'] = "Sheffield";

$LNG['city_gb_shrewsbury'] = "Shrewsbury";

$LNG['city_gb_skegness'] = "Skegness";

$LNG['city_gb_southampton'] = "Southampton";

$LNG['city_gb_southport'] = "Southport";

$LNG['city_gb_st_helens'] = "St Helens";

$LNG['city_gb_stafford'] = "Stafford";

$LNG['city_gb_stockport'] = "Stockport";

$LNG['city_gb_stoke'] = "Stoke";

$LNG['city_gb_sunderland'] = "Sunderland";

$LNG['city_gb_surrey'] = "Surrey";

$LNG['city_gb_swansea'] = "Swansea";

$LNG['city_gb_tamworth'] = "Tamworth";

$LNG['city_gb_telford'] = "Telford";

$LNG['city_gb_wakefield'] = "Wakefield";

$LNG['city_gb_walsall'] = "Walsall";

$LNG['city_gb_warrington'] = "Warrington";

$LNG['city_gb_warwick'] = "Warwick";

$LNG['city_gb_watford'] = "Watford";

$LNG['city_gb_weston_super_mare'] = "Weston-Super-Mare";

$LNG['city_gb_wetherby'] = "Wetherby";

$LNG['city_gb_whitby'] = "Whitby";

$LNG['city_gb_wigan'] = "Wigan";

$LNG['city_gb_wilmslow'] = "Wilmslow";

$LNG['city_gb_winchester'] = "Winchester";

$LNG['city_gb_windsor'] = "Windsor";

$LNG['city_gb_wolverhampton'] = "Wolverhampton";

$LNG['city_gb_worcester'] = "Worcester";

$LNG['city_gb_workington'] = "Workington";

$LNG['city_gb_worksop'] = "Worksop";

$LNG['city_gb_yeovil'] = "Yeovil";

$LNG['city_gb_york'] = "York";

$LNG['city_fr_annecy'] = "Annecy";

$LNG['city_it_benevento'] = "Benevento";

$LNG['city_it_cosenza'] = "Cosenza";

$LNG['city_fr_monaco'] = "Monaco";

$LNG['city_it_caserta'] = "Caserta";

$LNG['city_it_aosta'] = "Aosta";

$LNG['city_it_imperia'] = "Imperia";

$LNG['city_es_valencia'] = "Valencia";

$LNG['city_es_sevilla'] = "Sevilla";

$LNG['city_es_bilbao'] = "Bilbao";

$LNG['city_es_cadiz'] = "Cadiz";

$LNG['city_es_coruna'] = "Coruna";

$LNG['city_es_cordova'] = "Cordova";

$LNG['city_es_granada'] = "Granada";

$LNG['city_es_leon'] = "Leon";

$LNG['city_es_lugo'] = "Lugo";

$LNG['city_es_alava'] = "Alava";

$LNG['city_es_malaga'] = "Malaga";

$LNG['city_es_murcia'] = "Murcia";

$LNG['city_es_orense'] = "Orense";

$LNG['city_es_pontevedra'] = "Pontevedra";

$LNG['city_es_salamanca'] = "Salamanca";

$LNG['city_es_almeria'] = "Almeria";

$LNG['city_es_tenerife'] = "Tenerife";

$LNG['city_es_alicante'] = "Alicante";

$LNG['city_es_valladolid'] = "Valladolid";

$LNG['city_es_vizcaia'] = "Vizcaia";

$LNG['city_es_zaragoza'] = "Zaragoza";

$LNG['city_it_chiavari'] = "Chiavari";

$LNG['city_it_mestre'] = "Mestre";

$LNG['city_fr_auxerre'] = "Auxerre";

$LNG['city_fr_macon'] = "Mâcon";

$LNG['city_gr_thessaloniki'] = "Thessaloniki";

$LNG['city_fr_versailles'] = "Versailles";

$LNG['city_fr_rouen'] = "Rouen";

$LNG['city_fr_evreux'] = "Evreux";

$LNG['city_fr_chambery'] = "Chambéry";

$LNG['city_fr_courchevel'] = "Courchevel";

$LNG['city_fr_megeve'] = "Megève";

$LNG['city_it_biella'] = "Biella";

$LNG['city_fr_hyeres'] = "Hyères";

$LNG['city_fr_la_ciotat'] = "La Ciotat";

$LNG['city_fr_aubagne'] = "Aubagne";

$LNG['city_it_viterbo'] = "Viterbo";

$LNG['city_fr_agen'] = "Agen";

$LNG['city_fr_ajaccio'] = "Ajaccio";

$LNG['city_fr_albi'] = "Albi";

$LNG['city_fr_alencon'] = "Alençon";

$LNG['city_fr_angouleme'] = "Angoulême";

$LNG['city_fr_arles'] = "Arles";

$LNG['city_fr_auch'] = "Auch";

$LNG['city_fr_aurillac'] = "Aurillac";

$LNG['city_fr_avoriaz'] = "Avoriaz";

$LNG['city_fr_bastia'] = "Bastia";

$LNG['city_fr_beauvais'] = "Beauvais";

$LNG['city_fr_bergerac'] = "Bergerac";

$LNG['city_fr_beziers'] = "Béziers";

$LNG['city_fr_blois'] = "Blois";

$LNG['city_fr_bourg_en_bresse'] = "Bourg-En-Bresse"; 

$LNG['city_fr_cahors'] = "Cahors";

$LNG['city_fr_calais'] = "Calais";

$LNG['city_fr_calvi'] = "Calvi";

$LNG['city_fr_carcassonne'] = "Carcassonne";

$LNG['city_fr_chalon_sur_saone'] = "Chalon-sur-Saône";

$LNG['city_fr_charleville_mezieres'] = "Charleville-Mezieres";

$LNG['city_fr_chartres'] = "Chartres";

$LNG['city_fr_chatellerault'] = "Châtellerault";

$LNG['city_fr_cherbourg'] = "Cherbourg";

$LNG['city_fr_cognac'] = "Cognac";

$LNG['city_fr_digne_les_bains'] = "Digne-Les-Bains";

$LNG['city_fr_dole'] = "Dole";

$LNG['city_fr_dunkerque'] = "Dunkerque";

$LNG['city_fr_epinal'] = "Epinal";

$LNG['city_fr_foix'] = "Foix";

$LNG['city_fr_gap'] = "Gap";

$LNG['city_fr_grasse'] = "Grasse";

$LNG['city_fr_gueret'] = "Guéret"; 

$LNG['city_fr_hendaye'] = "Hendaye";

$LNG['city_fr_la_rochelle'] = "La Rochelle"; 

$LNG['city_fr_la_roche_sur_yon'] = "La Roche-Sur-Yon";  

$LNG['city_fr_laval'] = "Laval";

$LNG['city_fr_lens'] = "Lens";

$LNG['city_fr_le_puy_en_velay'] = "Le Puy-En-Velay";

$LNG['city_fr_manosque'] = "Manosque";

$LNG['city_fr_mende'] = "Mende";

$LNG['city_fr_menton'] = "Menton";

$LNG['city_fr_montauban'] = "Montauban";

$LNG['city_fr_montlucon'] = "Montluçon";

$LNG['city_fr_mont_de_marsan'] = "Mont de Marsan";

$LNG['city_fr_montelimar'] = "Montélimar";

$LNG['city_fr_nevers'] = "Nevers";

$LNG['city_fr_pau'] = "Pau";

$LNG['city_fr_perigueux'] = "Périgueux";

$LNG['city_fr_porto_vecchio'] = "Porto-Vecchio";

$LNG['city_fr_rodez'] = "Rodez";

$LNG['city_fr_roubaix'] = "Roubaix";

$LNG['city_fr_saint_brieuc'] = "Saint-Brieuc";

$LNG['city_fr_saint_dizier'] = "Saint-Dizier";

$LNG['city_fr_saint_quentin'] = "Saint-Quentin";

$LNG['city_fr_salon_de_provence'] = "Salon-De-Provence";

$LNG['city_fr_tarbes'] = "Tarbes";

$LNG['city_fr_thionville'] = "Thionville";

$LNG['city_fr_troyes'] = "Troyes";

$LNG['city_fr_valence'] = "Valence";

$LNG['city_fr_vesoul'] = "Vesoul";

$LNG['city_it_monza'] = "Monza";

$LNG['city_it_rapallo'] = "Rapallo";

$LNG['city_it_tropea'] = "Tropea";

$LNG['city_it_scalea'] = "Scalea";

$LNG['city_it_capo_vaticano'] = "Capo Vaticano";

$LNG['city_it_monopoli'] = "Monopoli";



$LNG['city_ch_aarau'] = "Aarau";

$LNG['city_ch_adliswil'] = "Adliswil";

$LNG['city_ch_affoltern_am_albis'] = "Affoltern am Albis";

$LNG['city_ch_allschwil'] = "Allschwil";

$LNG['city_ch_altstatten'] = "Altstätten";

$LNG['city_ch_amriswil'] = "Amriswil";

$LNG['city_ch_arbon'] = "Arbon";

$LNG['city_ch_baar'] = "Baar";

$LNG['city_ch_baden'] = "Baden";

$LNG['city_ch_bellinzona'] = "Bellinzona";

$LNG['city_ch_biel_bienne'] = "Biel/Bienne";

$LNG['city_ch_binningen'] = "Binningen";

$LNG['city_ch_birsfelden'] = "Birsfelden";

$LNG['city_ch_brig_glis'] = "Brig-Glis";

$LNG['city_ch_buchs'] = "Buchs";

$LNG['city_ch_bulach'] = "Bülach";

$LNG['city_ch_bulle'] = "Bulle";

$LNG['city_ch_burgdorf'] = "Burgdorf";

$LNG['city_ch_carouge'] = "Carouge";

$LNG['city_ch_davos'] = "Davos";

$LNG['city_ch_delemont'] = "Delémont";

$LNG['city_ch_dietikon'] = "Dietikon";

$LNG['city_ch_dubendorf'] = "Dübendorf";

$LNG['city_ch_ebikon'] = "Ebikon";

$LNG['city_ch_ecublens'] = "Ecublens";

$LNG['city_ch_einsiedeln'] = "Einsiedeln";

$LNG['city_ch_emmen'] = "Emmen";

$LNG['city_ch_frauenfeld'] = "Frauenfeld";

$LNG['city_ch_freienbach'] = "Freienbach";

$LNG['city_ch_gland'] = "Gland";

$LNG['city_ch_gossau'] = "Gossau";

$LNG['city_ch_grenchen'] = "Grenchen";

$LNG['city_ch_herisau'] = "Herisau";

$LNG['city_ch_horgen'] = "Horgen";

$LNG['city_ch_horw'] = "Horw";

$LNG['city_ch_cham'] = "Cham";

$LNG['city_ch_chene_bougeries'] = "Chêne-Bougeries";

$LNG['city_ch_chur'] = "Chur";

$LNG['city_ch_illnau_effretikon'] = "Illnau-Effretikon";

$LNG['city_ch_ittigen'] = "Ittigen";

$LNG['city_ch_jona'] = "Jona";

$LNG['city_ch_kloten'] = "Kloten";

$LNG['city_ch_koniz'] = "Köniz";

$LNG['city_ch_kreuzlingen'] = "Kreuzlingen";

$LNG['city_ch_kriens'] = "Kriens";

$LNG['city_ch_kusnacht'] = "Küsnacht";

$LNG['city_ch_kussnacht'] = "Küssnacht";

$LNG['city_ch_la_chaux_de_fonds'] = "La Chaux-de-Fonds";

$LNG['city_ch_la_tour_de_peilz'] = "La Tour-de-Peilz";

$LNG['city_ch_lancy'] = "Lancy";

$LNG['city_ch_langenthal'] = "Langenthal";

$LNG['city_ch_lausanne'] = "Lausanne";

$LNG['city_ch_le_locle'] = "Le Locle";

$LNG['city_ch_liestal'] = "Liestal";

$LNG['city_ch_littau'] = "Littau";

$LNG['city_ch_locarno'] = "Locarno";

$LNG['city_ch_lugano'] = "Lugano";

$LNG['city_ch_lyss'] = "Lyss";

$LNG['city_ch_martigny'] = "Martigny";

$LNG['city_ch_meilen'] = "Meilen";

$LNG['city_ch_meyrin'] = "Meyrin";

$LNG['city_ch_monthey'] = "Monthey";

$LNG['city_ch_montreux'] = "Montreux";

$LNG['city_ch_morges'] = "Morges";

$LNG['city_ch_munchenstein'] = "Münchenstein";

$LNG['city_ch_munsingen'] = "Münsingen";

$LNG['city_ch_muri'] = "Muri";

$LNG['city_ch_muttenz'] = "Muttenz";

$LNG['city_ch_nyon'] = "Nyon";

$LNG['city_ch_oftringen'] = "Oftringen";

$LNG['city_ch_olten'] = "Olten";

$LNG['city_ch_onex'] = "Onex";

$LNG['city_ch_opfikon'] = "Opfikon";

$LNG['city_ch_ostermundigen'] = "Ostermundigen";

$LNG['city_ch_pratteln'] = "Pratteln";

$LNG['city_ch_prilly'] = "Prilly";

$LNG['city_ch_pully'] = "Pully";

$LNG['city_ch_regensdorf'] = "Regensdorf";

$LNG['city_ch_reinach'] = "Reinach";

$LNG['city_ch_renens'] = "Renens";

$LNG['city_ch_rheinfelden'] = "Rheinfelden";

$LNG['city_ch_riehen'] = "Riehen";

$LNG['city_ch_richterswil'] = "Richterswil";

$LNG['city_ch_ruti'] = "Rüti";

$LNG['city_ch_sarnen'] = "Sarnen";

$LNG['city_ch_schlieren'] = "Schlieren";

$LNG['city_ch_sierre'] = "Sierre";

$LNG['city_ch_sion'] = "Sion";

$LNG['city_ch_spiez'] = "Spiez";

$LNG['city_ch_stafa'] = "Stäfa";

$LNG['city_ch_stans'] = "Stans";

$LNG['city_ch_steffisburg'] = "Steffisburg";

$LNG['city_ch_thalwil'] = "Thalwil";

$LNG['city_ch_thonex'] = "Thônex";

$LNG['city_ch_thun'] = "Thun";

$LNG['city_ch_uster'] = "Uster";

$LNG['city_ch_uzwil'] = "Uzwil";

$LNG['city_ch_vernier'] = "Vernier";

$LNG['city_ch_versoix'] = "Versoix";

$LNG['city_ch_vevey'] = "Vevey";

$LNG['city_ch_volketswil'] = "Volketswil";

$LNG['city_ch_wallisellen'] = "Wallisellen";

$LNG['city_ch_wadenswil'] = "Wädenswil";

$LNG['city_ch_wettingen'] = "Wettingen";

$LNG['city_ch_wetzikon'] = "Wetzikon";

$LNG['city_ch_wil'] = "Wil";

$LNG['city_ch_winterthur'] = "Winterthur";

$LNG['city_ch_wohlen'] = "Wohlen";

$LNG['city_ch_worb'] = "Worb";

$LNG['city_ch_yverdon_les_bains'] = "Yverdon-les-Bains";

$LNG['city_ch_zofingen'] = "Zofingen";

$LNG['city_ch_zollikon'] = "Zollikon";



$LNG['city_it_conegliano'] = "Conegliano";

$LNG['city_it_lido_di_jesolo'] = "Lido di Jesolo";



$LNG['city_it_nuoro'] = "Nuoro";

$LNG['city_it_oristano'] = "Oristano";

$LNG['city_it_sassari'] = "Sassari";

$LNG['city_it_segrate'] = "Segrate";

$LNG['city_it_rovigo'] = "Rovigo";

$LNG['city_ch_schwerzenbach'] = "Schwerzenbach";

$LNG['city_it_costa_azzurra'] = "Costa Azzurra";

$LNG['city_ch_herdern'] = "Herdern";

$LNG['city_es_ibiza'] = "Ibiza";

$LNG['city_es_mallorca'] = "Mallorca";

$LNG['city_ch_oberengstringen'] = "Oberengstringen";

$LNG['city_ch_glattbrugg'] = "Glattbrugg";

$LNG['city_ch_sempach'] = "Sempach";

$LNG['city_ch_rothrist'] = "Rothrist";

$LNG['city_ch_pfeffikon'] = "Pfeffikon";

$LNG['city_ch_schafisheim'] = "Schafisheim";

$LNG['city_ch_aarburg'] = "Aarburg";

$LNG['city_ch_romanshorn'] = "Romanshorn";



$LNG['city_ch_killwangen'] = "Killwangen";

$LNG['city_ch_marstetten'] = "Märstetten";

$LNG['city_ch_bad_ragaz'] = "Bad-Ragaz";

$LNG['city_ch_altendorf'] = "Altendorf";

$LNG['city_ch_oensingen'] = "Oensingen";

$LNG['city_ch_weinfelden'] = "Weinfelden";

$LNG['city_ch_neuenhof'] = "Neuenhof";

$LNG['city_ch_birmensdorf'] = "Birmensdorf";

$LNG['city_ch_dietlikon'] = "Dietlikon";

$LNG['city_ch_urdorf'] = "Urdorf";

$LNG['city_ch_eschenbach'] = "Eschenbach";

$LNG['city_it_cattolica'] = "Cattolica";

$LNG['city_it_pesaro'] = "Pesaro";

$LNG['city_ch_kirchberg'] = "Kirchberg";

$LNG['city_fr_royan'] = "Royan";

$LNG['city_ch_lyssach'] = "Lyssach";

$LNG['city_es_sabadell'] = "Sabadell";

$LNG['city_es_terrassa'] = "Terrassa";

$LNG['city_it_brindisi'] = "Brindisi";

$LNG['city_fr_annemasse'] = "Annemasse";

$LNG['city_ch_trubbach'] = "Trübbach";

$LNG['city_ch_hegnau'] = "Hegnau";

$LNG['city_ch_faulensee'] = "Faulensee";

$LNG['city_ch_neuchatel'] = "Neuchâtel";



$LNG['city_md_chisinau'] = "Chişinău";

$LNG['city_ch_pfaffikon'] = "Pfäffikon";

$LNG['city_it_lamezia'] = "Lamezia";

$LNG['city_it_imola'] = "Imola";

$LNG['city_ch_bremgarten'] = "Bremgarten";

$LNG['city_th_bangkok'] = "Bangkok";

$LNG['city_th_phuket'] = "Phuket";



$LNG['city_fr_briancon'] = "Briançon";

$LNG['city_fr_melun'] = "Melun";

$LNG['city_fr_fontainebleau'] = "Fontainebleau";

$LNG['city_fr_pontoise'] = "Pontoise";

$LNG['city_fr_marne_la_vallee'] = "Marne la Vallée";

$LNG['city_fr_saint_germain_en_laye'] = "Saint-Germain-en-Laye";

$LNG['city_fr_boulogne_billancourt'] = "Boulogne-Billancourt";

$LNG['city_fr_evry'] = "Evry";

$LNG['city_fr_montreuil'] = "Montreuil";

$LNG['city_fr_creteil'] = "Créteil";

$LNG['city_fr_nanterre'] = "Nanterre";

$LNG['city_fr_palaiseau'] = "Palaiseau";

$LNG['city_fr_arras'] = "Arras";

$LNG['city_fr_meaux'] = "Meaux";

$LNG['city_fr_provins'] = "Provins";

$LNG['city_fr_torcy'] = "Torcy";



$LNG['city_ch_thayngen'] = "Thayngen";



$LNG['city_de_aachen'] = "Aachen";

$LNG['city_de_augsburg'] = "Augsburg";

$LNG['city_de_bielefeld'] = "Bielefeld";

$LNG['city_de_bochum'] = "Bochum";

$LNG['city_de_bonn'] = "Bonn";

$LNG['city_de_braunschweig'] = "Braunschweig";

$LNG['city_de_bremen'] = "Bremen";

$LNG['city_de_chemnitz'] = "Chemnitz";

$LNG['city_de_dortmund'] = "Dortmund";

$LNG['city_de_dresden'] = "Dresden";

$LNG['city_de_duisburg'] = "Duisburg";

$LNG['city_de_essen'] = "Essen";

$LNG['city_de_gelsenkirchen'] = "Gelsenkirchen";

$LNG['city_de_karlsruhe'] = "Karlsruhe";

$LNG['city_de_krefeld'] = "Krefeld";

$LNG['city_de_leipzig'] = "Leipzig";

$LNG['city_de_mannheim'] = "Mannheim";

$LNG['city_de_monchengladbach'] = "Mönchengladbach";

$LNG['city_de_munster'] = "Münster";

$LNG['city_de_nurnberg'] = "Nürnberg";

$LNG['city_de_wiesbaden'] = "Wiesbaden";

$LNG['city_de_wuppertal'] = "Wuppertal";


//CUBIX Added by David
$LNG['city_us_annapolis'] = "Annapolis";
$LNG['city_us_arizona_city'] = "Arizona City";
$LNG['city_us_buffalo'] = "Buffalo";
$LNG['city_us_long_island'] = "Long Island";
$LNG['city_us_reno'] = "Reno";
$LNG['city_us_st_louis'] = "St Louis";
$LNG['city_us_tampa'] = "Tampa";
//


$LNG['city_us_albuquerque'] = "Albuquerque";

$LNG['city_us_arlington'] = "Arlington";

$LNG['city_us_atlanta'] = "Atlanta";

$LNG['city_us_austin'] = "Austin";

$LNG['city_us_baltimore'] = "Baltimore";

$LNG['city_us_boston'] = "Boston";

$LNG['city_us_charlotte'] = "Charlotte";

$LNG['city_us_chicago'] = "Chicago";

$LNG['city_us_cleveland'] = "Cleveland";

$LNG['city_us_colorado_springs'] = "Colorado Springs";

$LNG['city_us_columbus'] = "Columbus";

$LNG['city_us_dallas'] = "Dallas";

$LNG['city_us_denver'] = "Denver";

$LNG['city_us_detroit'] = "Detroit";

$LNG['city_us_el_paso'] = "El Paso";

$LNG['city_us_fort_worth'] = "Fort Worth";

$LNG['city_us_fresno'] = "Fresno";

$LNG['city_us_honolulu'] = "Honolulu";

$LNG['city_us_houston'] = "Houston";

$LNG['city_us_indianapolis'] = "Indianapolis";

$LNG['city_us_jacksonville'] = "Jacksonville";

$LNG['city_us_kansas_city'] = "Kansas City";

$LNG['city_us_las_vegas'] = "Las Vegas";

$LNG['city_us_long_beach'] = "Long Beach";

$LNG['city_us_los_angeles'] = "Los Angeles";

$LNG['city_us_louisville'] = "Louisville";

$LNG['city_us_memphis'] = "Memphis";

$LNG['city_us_mesa'] = "Mesa";

$LNG['city_us_miami'] = "Miami";

$LNG['city_us_milwaukee'] = "Milwaukee";

$LNG['city_us_minneapolis'] = "Minneapolis";

$LNG['city_us_nashville'] = "Nashville";

$LNG['city_us_new_orleans'] = "New Orleans";

$LNG['city_us_new_york_city'] = "New York City";

$LNG['city_us_oakland'] = "Oakland";

$LNG['city_us_oklahoma_city'] = "Oklahoma City";

$LNG['city_us_omaha'] = "Omaha";

$LNG['city_us_orange_county'] = "Orange County";

$LNG['city_us_orlando'] = "Orlando";

$LNG['city_us_philadelphia'] = "Philadelphia";

$LNG['city_us_phoenix'] = "Phoenix";

$LNG['city_us_portland'] = "Portland";

$LNG['city_us_sacramento'] = "Sacramento";

$LNG['city_us_san_antonio'] = "San Antonio";

$LNG['city_us_san_diego'] = "San Diego";

$LNG['city_us_san_francisco'] = "San Francisco";

$LNG['city_us_san_jose'] = "San Jose";

$LNG['city_us_seattle'] = "Seattle";

$LNG['city_us_tucson'] = "Tucson";

$LNG['city_us_tulsa'] = "Tulsa";

$LNG['city_us_virginia_beach'] = "Virginia Beach";

$LNG['city_us_washington'] = "Washington";



$LNG['city_fr_brive_la_gaillarde'] = "Brive-la-Gaillarde";

$LNG['city_fr_cagnes_sur_mer'] = "Cagnes sur Mer";

$LNG['city_fr_neuilly_sur_seine'] = "Neuilly-sur-Seine";

$LNG['city_fr_la_defense'] = "La Defense";

$LNG['city_fr_levallois_perret'] = "Levallois-Perret";

$LNG['city_fr_courbevoie'] = "Courbevoie";



$LNG['city_it_crotone'] = "Crotone";

$LNG['city_it_ventimiglia'] = "Ventimiglia";

$LNG['city_it_terni'] = "Terni";

$LNG['city_it_teramo'] = "Teramo";



$LNG['city_fr_dourdan'] = "Dourdan";

$LNG['city_fr_maisons_laffitte'] = "Maisons-Laffitte";

$LNG['city_fr_rambouillet'] = "Rambouillet";

$LNG['city_fr_sartrouville'] = "Sartrouville";

$LNG['city_fr_villefranche_sur_saone'] = "Villefranche-sur-Saône";

$LNG['city_fr_bois_colombes'] = "Bois-Colombes";

$LNG['city_fr_bourges'] = "Bourges";

$LNG['city_fr_chatou'] = "Chatou";

$LNG['city_fr_clichy'] = "Clichy";

$LNG['city_fr_saint_ouen'] = "Saint Ouen";

$LNG['city_fr_asnieres'] = "Asnières";

$LNG['city_fr_beaune'] = "Beaune";

$LNG['city_fr_roissy_cdg_airport'] = "Roissy (CDG airport)";

$LNG['city_fr_saint_cloud'] = "Saint-Cloud";

$LNG['city_fr_suresnes'] = "Suresnes";

$LNG['city_fr_moulins'] = "Moulins";

$LNG['city_fr_juvisy'] = "Juvisy";

$LNG['city_fr_libourne'] = "Libourne";

$LNG['city_fr_pontarlier'] = "Pontarlier";

$LNG['city_fr_guingamp'] = "Guingamp";



$LNG['city_ch_spreitenbach'] = "Spreitenbach";

$LNG['city_ch_othmarsingen'] = "Othmarsingen";

$LNG['city_ch_zollikofen'] = "Zollikofen";

$LNG['city_ch_mullheim'] = "Müllheim";

$LNG['city_ch_brutisellen'] = "Brütisellen";

$LNG['city_ch_egerkingen'] = "Egerkingen";

$LNG['city_ch_heerbrugg'] = "Heerbrugg";

$LNG['city_ch_reinach_ag'] = "Reinach AG";

$LNG['city_ch_hergiswil'] = "Hergiswil";

$LNG['city_ch_hochstetten'] = "Höchstetten";



$LNG['city_it_fortedeimarmi'] = "Forte dei Marmi";

$LNG['city_it_milanomarittima'] = "Milano Marittima";

$LNG['city_it_matera'] = "Matera";

$LNG['city_it_barletta'] = "Barletta";

$LNG['city_it_andria'] = "Andria";

$LNG['city_it_trani'] = "Trani";

$LNG['city_it_busto_arsizio'] = "Busto Arsizio";

$LNG['city_it_trapani'] = "Trapani";

$LNG['city_it_cesena'] = "Cesena";

$LNG['city_it_ischia'] = "Ischia";

$LNG['city_it_alassio'] = "Alassio";

$LNG['city_it_fidenza'] = "Fidenza";

$LNG['city_it_salsomaggiore_terme'] = "Salsomaggiore Terme";



$LNG['city_gb_bristol'] = "Bristol";

$LNG['city_gb_canterbury'] = "Canterbury";

$LNG['city_gb_chichester'] = "Chichester";

$LNG['city_gb_dorchester'] = "Dorchester";

$LNG['city_gb_ely'] = "Ely";

$LNG['city_gb_gatwick'] = "Gatwick";

$LNG['city_gb_gloucester'] = "Gloucester";

$LNG['city_gb_heathrow'] = "Heathrow";

$LNG['city_gb_hereford'] = "Hereford";

$LNG['city_gb_kingston'] = "Kingston";

$LNG['city_gb_loughton'] = "Loughton";

$LNG['city_gb_luton'] = "Luton";

$LNG['city_gb_maidenhead'] = "Maidenhead";

$LNG['city_gb_north_shields'] = "North Shields";

$LNG['city_gb_salisbury'] = "Salisbury";

$LNG['city_gb_southampton'] = "Southampton";

$LNG['city_gb_st_albans'] = "St.Albans";

$LNG['city_gb_stansted'] = "Stansted";

$LNG['city_gb_stoke_on_trent'] = "Stoke-on-trent";

$LNG['city_gb_swindon'] = "Swindon";

$LNG['city_gb_truro'] = "Truro";

$LNG['city_gb_wells'] = "Wells";

$LNG['city_gb_westminster'] = "Westminster";

$LNG['city_gb_woking'] = "Woking";

$LNG['city_gb_bracknell'] = "Bracknell";

$LNG['city_gb_burton_upon_trent'] = "Burton-upon-Trent";

$LNG['city_gb_chelmsford'] = "Chelmsford";

$LNG['city_gb_suffolk'] = "Suffolk";

$LNG['city_gb_leamington_spa'] = "Leamington Spa";

$LNG['city_gb_aberdeen'] = "Aberdeen";

$LNG['city_gb_edinburgh'] = "Edinburgh";

$LNG['city_gb_glasgow'] = "Glasgow";

$LNG['city_gb_inverness'] = "Inverness";

$LNG['city_gb_livingston'] = "Livingston";

$LNG['city_gb_newport'] = "Newport";

$LNG['city_gb_swansea'] = "Swansea";

// Cubix Added By Tiko
$LNG['city_it_ivrea'] = "Ivrea";
$LNG['city_it_san_benedetto_del_tronto'] = "San Benedetto del Tronto";
$LNG['city_ch_rapperswil_jona'] = "Rapperswil-Jona";
$LNG['city_it_vibo_valentia'] = "Vibo Valentia";
$LNG['city_it_senigallia'] = "Senigallia";
$LNG['city_ae_dubai'] = "Dubai";
// Cubix added EF-231
$LNG['city_ch_kaiserstuhl'] = "Kaiserstuhl";
$LNG['city_ch_zurzach'] = "Zurzach";
$LNG['city_ch_ottenbach'] = "Ottenbach";
$LNG['city_ch_zetzwil'] = "Zetzwil";
$LNG['city_ch_kemptthal_lindau'] = "Kemptthal-Lindau";
$LNG['city_ch_lachen'] = "Lachen";
$LNG['city_ch_brunnen'] = "Brunnen";
$LNG['city_ch_hunzenschwil'] = "Hunzenschwil";
$LNG['city_ch_st_moritz'] = "St.Moritz";
$LNG['city_ch_laufenburg'] = "Laufenburg";
// Cubix added EF-251
$LNG['city_it_tortona'] = "Tortona";
$LNG['city_it_pinerolo'] = "Pinerolo";
?>