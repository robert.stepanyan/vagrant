<?php

// WARNUNG: Wenn Du eine andere Nationalität wählst,
// füge entsprechenden ISO Code $NATIONALITIES zu include/nationalities.inc.php.

$LNG['nationality_al'] = "Albanisch";
$LNG['nationality_us'] = "Amerikanisch";
$LNG['nationality_ar'] = "Argentinisch";
$LNG['nationality_au'] = "Australisch";
$LNG['nationality_at'] = "Oesterreich";
$LNG['nationality_be'] = "Belgisch";
$LNG['nationality_br'] = "Brasilianisch";
$LNG['nationality_gb'] = "Britisch";
$LNG['nationality_bg'] = "Bulgarisch";
$LNG['nationality_ca'] = "Kanadisch";
$LNG['nationality_cn'] = "Chinesisch";
$LNG['nationality_cz'] = "Tschechisch";
$LNG['nationality_dk'] = "Dänisch";
$LNG['nationality_nl'] = "Holländisch";
$LNG['nationality_fr'] = "Französisch";
$LNG['nationality_de'] = "Deutsch";
$LNG['nationality_gr'] = "Griechisch";
$LNG['nationality_hu'] = "Ungarisch";
$LNG['nationality_in'] = "Indisch";
$LNG['nationality_ie'] = "Irisch";
$LNG['nationality_il'] = "Israelisch";
$LNG['nationality_it'] = "Italienisch";
$LNG['nationality_jm'] = "Jamaikanisch";
$LNG['nationality_jp'] = "Japanisch";
$LNG['nationality_my'] = "Malaysisch";
$LNG['nationality_md'] = "Moldawisch";
$LNG['nationality_pl'] = "Polnisch";
$LNG['nationality_pt'] = "Portugiesisch";
$LNG['nationality_ro'] = "Rumänisch";
$LNG['nationality_ru'] = "Russisch";
$LNG['nationality_sk'] = "Slovakisch";
$LNG['nationality_es'] = "Spanisch";
$LNG['nationality_se'] = "Schwedisch";
$LNG['nationality_ch'] = "Schweiz";
$LNG['nationality_th'] = "Thai";
$LNG['nationality_ua'] = "Ukrainisch";
$LNG['nationality_ve'] = "Venezuelanisch";

$LNG['nationality_by'] = "Weissrussisch";
$LNG['nationality_ee'] = "Estisch";
$LNG['nationality_fi'] = "Finnländisch";
$LNG['nationality_hr'] = "Kroatisch";
$LNG['nationality_kr'] = "Koreanisch";
$LNG['nationality_lt'] = "Litauisch";
$LNG['nationality_lv'] = "Lettisch";
$LNG['nationality_me'] = "Montenegro";
$LNG['nationality_mx'] = "Mexikanisch";
$LNG['nationality_no'] = "Norwegisch";
$LNG['nationality_ph'] = "Phillipinisch";
$LNG['nationality_rs'] = "Serbisch";
$LNG['nationality_sg'] = "Singapur";
$LNG['nationality_si'] = "Slowenisch";
$LNG['nationality_cu'] = "Kubanisch";
$LNG['nationality_co'] = "Kolumbianisch";
$LNG['nationality_pr'] = "Puerto Rican";
$LNG['nationality_vn'] = "Vietnamese";
?>