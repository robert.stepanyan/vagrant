<?php

// WARNING: If you add another nationality,
// you must add its ISO code to the $NATIONALITIES in include/nationalities.inc.php.

$LNG['nationality_al'] = "Albana";
$LNG['nationality_us'] = "Americana";
$LNG['nationality_ar'] = "Argentina";
$LNG['nationality_au'] = "Australiana";
$LNG['nationality_at'] = "Austriaca";
$LNG['nationality_be'] = "Belga";
$LNG['nationality_br'] = "Brazileira";
$LNG['nationality_gb'] = "Britanica";
$LNG['nationality_bg'] = "Bulgara";
$LNG['nationality_ca'] = "Canadiense";
$LNG['nationality_cn'] = "China";
$LNG['nationality_cz'] = "Checo";
$LNG['nationality_dk'] = "Danesa";
$LNG['nationality_nl'] = "Holandesa";
$LNG['nationality_fr'] = "Francesa";
$LNG['nationality_de'] = "Alemana";
$LNG['nationality_gr'] = "Griega";
$LNG['nationality_hu'] = "Hungara";
$LNG['nationality_in'] = "India";
$LNG['nationality_ie'] = "Irlandesa";
$LNG['nationality_il'] = "Israelita";
$LNG['nationality_it'] = "Italiana";
$LNG['nationality_jm'] = "Jamaicana";
$LNG['nationality_jp'] = "Japonesa";
$LNG['nationality_my'] = "Malasia";
$LNG['nationality_md'] = "Moldova";
$LNG['nationality_pl'] = "Polaca";
$LNG['nationality_pt'] = "Portuguesa";
$LNG['nationality_ro'] = "Rumana";
$LNG['nationality_ru'] = "Rusa";
$LNG['nationality_sk'] = "Eslovaca";
$LNG['nationality_es'] = "Espańola";
$LNG['nationality_se'] = "Sueca";
$LNG['nationality_ch'] = "Suiza";
$LNG['nationality_th'] = "Tailandesa";
$LNG['nationality_ua'] = "Ukraniana";
$LNG['nationality_ve'] = "Venezolana";

$LNG['nationality_by'] = "Belarussian";
$LNG['nationality_ee'] = "Estonian";
$LNG['nationality_fi'] = "Finnish";
$LNG['nationality_hr'] = "Croatian";
$LNG['nationality_kr'] = "Korean";
$LNG['nationality_lt'] = "Lithuanian";
$LNG['nationality_lv'] = "Latvian";
$LNG['nationality_me'] = "Montenegrian";
$LNG['nationality_mx'] = "Mexican";
$LNG['nationality_no'] = "Norwegian";
$LNG['nationality_ph'] = "Filipino";
$LNG['nationality_rs'] = "Serbian";
$LNG['nationality_sg'] = "Singaporean";
$LNG['nationality_si'] = "Slovenian";
$LNG['nationality_cu'] = "Cuban";
$LNG['nationality_co'] = "Colombian";
$LNG['nationality_pr'] = "Puerto Rican";
$LNG['nationality_vn'] = "Vietnamese";
?>