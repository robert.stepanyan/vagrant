<?php

// WARNING: If you add another nationality,
// you must add its ISO code to the $NATIONALITIES in include/nationalities.inc.php.

$LNG['nationality_al'] = "Αλβανία";
$LNG['nationality_us'] = "Αμερική";
$LNG['nationality_ar'] = "Αργεντινή";
$LNG['nationality_au'] = "Αυστραλία";
$LNG['nationality_at'] = "Αυστρία";
$LNG['nationality_be'] = "Βέλγιο";
$LNG['nationality_br'] = "Βραζιλία";
$LNG['nationality_gb'] = "Αγγλία";
$LNG['nationality_bg'] = "Βουλγαρία";
$LNG['nationality_ca'] = "Καναδά";
$LNG['nationality_cn'] = "Κίνα";
$LNG['nationality_cz'] = "Τσεχία";
$LNG['nationality_dk'] = "Δανία";
$LNG['nationality_nl'] = "Ολλανδία";
$LNG['nationality_fr'] = "Γαλλία";
$LNG['nationality_de'] = "Γερμανία";
$LNG['nationality_gr'] = "Ελλάδα";
$LNG['nationality_hu'] = "Ουγγαρία";
$LNG['nationality_in'] = "Ινδία";
$LNG['nationality_ie'] = "Ιρλανδία";
$LNG['nationality_il'] = "Εβραίος";
$LNG['nationality_it'] = "Ιταλός";
$LNG['nationality_jm'] = "Τζαμάικα";
$LNG['nationality_jp'] = "Ιαπωνία";
$LNG['nationality_my'] = "Μαλαισία";
$LNG['nationality_md'] = "Μολδαβία";
$LNG['nationality_pl'] = "Πολωνία";
$LNG['nationality_pt'] = "Πορτογαλία";
$LNG['nationality_ro'] = "Ρουμανία";
$LNG['nationality_ru'] = "Ρωσία";
$LNG['nationality_sk'] = "Σλοβακία";
$LNG['nationality_es'] = "Ισπανία";
$LNG['nationality_se'] = "Σουηδία";
$LNG['nationality_ch'] = "Ελβετία";
$LNG['nationality_th'] = "Ταϊλάνδη";
$LNG['nationality_ua'] = "Ουκρανός";
$LNG['nationality_ve'] = "Βενεζουέλα";
$LNG['nationality_by'] = "Λευκορωσία";
$LNG['nationality_ee'] = "Εσθονία";
$LNG['nationality_fi'] = "Φιλανδία";
$LNG['nationality_hr'] = "Κροατία";
$LNG['nationality_kr'] = "Κορέα";
$LNG['nationality_lt'] = "Λιθουανία";
$LNG['nationality_lv'] = "Λετονία";
$LNG['nationality_me'] = "Μαυροβούνιο";
$LNG['nationality_mx'] = "Μεξικό";
$LNG['nationality_no'] = "Νορβηγία";
$LNG['nationality_ph'] = "Φιλιππίνες";
$LNG['nationality_rs'] = "Σερβία";
$LNG['nationality_sg'] = "Σιγκαπούρη";
$LNG['nationality_si'] = "Σλοβενία";
$LNG['nationality_cu'] = "Κούβα";
$LNG['nationality_co'] = "Colombian";
$LNG['nationality_pr'] = "Puerto Rican";
$LNG['nationality_vn'] = "Vietnamese";
?>