<?php

// WARNING: If you add another country,
// you must add its ISO code to the $COUNTRIES in include/countries.inc.php.

$LNG['country_al'] = "Albania";
$LNG['country_dz'] = "Algeria";
$LNG['country_as'] = "American Samoa";
$LNG['country_ad'] = "Andorra";
$LNG['country_ao'] = "Angola";
$LNG['country_ai'] = "Anguilla";
$LNG['country_aq'] = "Antarctica";
$LNG['country_ag'] = "Antigua und Barbados";
$LNG['country_ar'] = "Argentina";
$LNG['country_am'] = "Armenia";
$LNG['country_aw'] = "Aruba";
$LNG['country_au'] = "Australia";
$LNG['country_at'] = "Austria";
$LNG['country_az'] = "Azerbaijan";
$LNG['country_bs'] = "Bahamas";
$LNG['country_bh'] = "Bahrain";
$LNG['country_bd'] = "Bangladesh";
$LNG['country_bb'] = "Barbados";
$LNG['country_by'] = "Belarus";
$LNG['country_be'] = "Belgium";
$LNG['country_bz'] = "Belize";
$LNG['country_bj'] = "Benin";
$LNG['country_bm'] = "Bermuda";
$LNG['country_bt'] = "Bhutan";
$LNG['country_bo'] = "Bolivia";
$LNG['country_ba'] = "Bosnia and Herzegowina";
$LNG['country_bw'] = "Botswana";
$LNG['country_bv'] = "Bouvet Island";
$LNG['country_br'] = "Brazil";
$LNG['country_bn'] = "Brunei";
$LNG['country_bg'] = "Bulgaria";
$LNG['country_bf'] = "Burkina Faso";
$LNG['country_bi'] = "Burundi";
$LNG['country_kh'] = "Cambodia";
$LNG['country_cm'] = "Cameroon";
$LNG['country_ca'] = "Canada";
$LNG['country_cv'] = "Cape Verde";
$LNG['country_ky'] = "Cayman Islands";
$LNG['country_cf'] = "Central African Republic";
$LNG['country_td'] = "Chad";
$LNG['country_cl'] = "Chile";
$LNG['country_cn'] = "China";
$LNG['country_cx'] = "Christmas Island";
$LNG['country_cc'] = "Cocos (Keeling) Islands";
$LNG['country_co'] = "Colombia";
$LNG['country_km'] = "Comoros";
$LNG['country_cg'] = "Congo";
$LNG['country_ck'] = "Cook Islands";
$LNG['country_cr'] = "Costa Rica";
$LNG['country_ci'] = "Cote d'Ivoire";
$LNG['country_hr'] = "Croatia";
$LNG['country_cu'] = "Cuba";
$LNG['country_cy'] = "Cyprus";
$LNG['country_cz'] = "Czech Republic";
$LNG['country_dk'] = "Denmark";
$LNG['country_dj'] = "Djibouti";
$LNG['country_dm'] = "Dominica";
$LNG['country_do'] = "Dominican Republic";
$LNG['country_tl'] = "East Timor";
$LNG['country_ec'] = "Ecuador";
$LNG['country_eg'] = "Egypt";
$LNG['country_sv'] = "El Salvador";
$LNG['country_gq'] = "Equatorial Guinea";
$LNG['country_er'] = "Eritrea";
$LNG['country_ee'] = "Estonia";
$LNG['country_et'] = "Ethiopia";
$LNG['country_fk'] = "Falkland Islands";
$LNG['country_fo'] = "Faroe Islands";
$LNG['country_fj'] = "Fiji";
$LNG['country_fi'] = "Finland";
$LNG['country_fr'] = "France";
$LNG['country_gf'] = "French Guiana";
$LNG['country_pf'] = "French Polynesia";
$LNG['country_ga'] = "Gabon";
$LNG['country_gm'] = "Gambia";
$LNG['country_ge'] = "Georgia";
$LNG['country_de'] = "Germany";
$LNG['country_gh'] = "Ghana";
$LNG['country_gi'] = "Gibraltar";
$LNG['country_gr'] = "Greece";
$LNG['country_gl'] = "Greenland";
$LNG['country_gd'] = "Grenada";
$LNG['country_gp'] = "Guadeloupe";
$LNG['country_gu'] = "Guam";
$LNG['country_gt'] = "Guatemala";
$LNG['country_gn'] = "Guinea";
$LNG['country_gw'] = "Guinea-Bissau";
$LNG['country_gy'] = "Guyana";
$LNG['country_ht'] = "Haiti";
$LNG['country_va'] = "Vatican";
$LNG['country_hn'] = "Honduras";
$LNG['country_hk'] = "Hong Kong";
$LNG['country_hu'] = "Hungary";
$LNG['country_is'] = "Iceland";
$LNG['country_in'] = "India";
$LNG['country_id'] = "Indonesia";
$LNG['country_ir'] = "Iran";
$LNG['country_iq'] = "Iraq";
$LNG['country_ie'] = "Ireland";
$LNG['country_il'] = "Israel";
$LNG['country_it'] = "Italia";
$LNG['country_jm'] = "Jamaica";
$LNG['country_jp'] = "Japan";
$LNG['country_jo'] = "Jordan";
$LNG['country_kz'] = "Kazakhstan";
$LNG['country_ke'] = "Kenya";
$LNG['country_ki'] = "Kiribati";
$LNG['country_kp'] = "Korea, North";
$LNG['country_kr'] = "Korea, South";
$LNG['country_kw'] = "Kuwait";
$LNG['country_kg'] = "Kyrgyzstan";
$LNG['country_lv'] = "Latvia";
$LNG['country_lb'] = "Lebanon";
$LNG['country_ls'] = "Lesotho";
$LNG['country_lr'] = "Liberia";
$LNG['country_ly'] = "Libya";
$LNG['country_li'] = "Liechtenstein";
$LNG['country_lt'] = "Lituania";
$LNG['country_lu'] = "Luxembourg";
$LNG['country_mo'] = "Macau";
$LNG['country_mk'] = "Macedonia";
$LNG['country_mg'] = "Madagascar";
$LNG['country_mw'] = "Malawi";
$LNG['country_my'] = "Malaysia";
$LNG['country_mv'] = "Maldives";
$LNG['country_ml'] = "Mali";
$LNG['country_mt'] = "Malta";
$LNG['country_mh'] = "Marshall Islands";
$LNG['country_mq'] = "Martinique";
$LNG['country_mr'] = "Mauritania";
$LNG['country_mu'] = "Mauritius";
$LNG['country_yt'] = "Mayotte";
$LNG['country_mx'] = "Mexico";
$LNG['country_fm'] = "Micronesia";
$LNG['country_md'] = "Moldova";
$LNG['country_mc'] = "Monaco";
$LNG['country_mn'] = "Mongolia";
$LNG['country_ms'] = "Montserrat";
$LNG['country_ma'] = "Morocco";
$LNG['country_mz'] = "Mozambique";
$LNG['country_mm'] = "Myanmar";
$LNG['country_na'] = "Namibia";
$LNG['country_nr'] = "Nauru";
$LNG['country_np'] = "Nepal";
$LNG['country_nl'] = "Netherlands";
$LNG['country_an'] = "Netherlands Antilles";
$LNG['country_nc'] = "New Caledonia";
$LNG['country_nz'] = "New Zealand";
$LNG['country_ni'] = "Nicaragua";
$LNG['country_ne'] = "Niger";
$LNG['country_ng'] = "Nigeria";
$LNG['country_nu'] = "Niue";
$LNG['country_nf'] = "Norfolk Island";
$LNG['country_no'] = "Norway";
$LNG['country_om'] = "Oman";
$LNG['country_pk'] = "Pakistan";
$LNG['country_pw'] = "Palau";
$LNG['country_ps'] = "Palestinian Territory";
$LNG['country_pa'] = "Panama";
$LNG['country_pg'] = "Papua New Guinea";
$LNG['country_py'] = "Paraguay";
$LNG['country_pe'] = "Peru";
$LNG['country_ph'] = "Philippines";
$LNG['country_pn'] = "Pitcairn";
$LNG['country_pl'] = "Poland";
$LNG['country_pt'] = "Portugal";
$LNG['country_pr'] = "Puerto Rico";
$LNG['country_qa'] = "Qatar";
$LNG['country_re'] = "Reunion";
$LNG['country_ro'] = "Romania";
$LNG['country_ru'] = "Russian Federation";
$LNG['country_rw'] = "Rwanda";
$LNG['country_kn'] = "Saint Kitts and Nevis";
$LNG['country_lc'] = "Saint Lucia";
$LNG['country_vc'] = "Saint Vincent";
$LNG['country_ws'] = "Samoa";
$LNG['country_sm'] = "San Marino";
$LNG['country_st'] = "Sao Tome and Principe";
$LNG['country_sa'] = "Saudi Arabia";
$LNG['country_sn'] = "Senegal";
$LNG['country_cs'] = "Serbia and Montenegro";
$LNG['country_sc'] = "Seychelles";
$LNG['country_sl'] = "Sierra Leone";
$LNG['country_sg'] = "Singapore";
$LNG['country_sk'] = "Slovakia";
$LNG['country_si'] = "Slovenia";
$LNG['country_sb'] = "Solomon Islands";
$LNG['country_so'] = "Somalia";
$LNG['country_za'] = "South Africa";
$LNG['country_gs'] = "South Georgia";
$LNG['country_es'] = "Spain";
$LNG['country_lk'] = "Sri Lanka";
$LNG['country_sh'] = "St. Helena";
$LNG['country_pm'] = "St. Pierre and Miquelon";
$LNG['country_sd'] = "Sudan";
$LNG['country_sr'] = "Suriname";
$LNG['country_sz'] = "Swaziland";
$LNG['country_se'] = "Sweden";
$LNG['country_ch'] = "Switzerland";
$LNG['country_sy'] = "Syrian Arab Republic";
$LNG['country_tj'] = "Tajikistan";
$LNG['country_tz'] = "Tanzania";
$LNG['country_th'] = "Thailand";
$LNG['country_tg'] = "Togo";
$LNG['country_tk'] = "Tokelau";
$LNG['country_to'] = "Tonga";
$LNG['country_tt'] = "Trinidad und Tobago";
$LNG['country_tn'] = "Tunisia";
$LNG['country_tr'] = "Turkey";
$LNG['country_tm'] = "Turkmenistan";
$LNG['country_tv'] = "Tuvalu";
$LNG['country_ug'] = "Uganda";
$LNG['country_ua'] = "Ukraine";
$LNG['country_ae'] = "United Arab Emirates";
$LNG['country_gb'] = "United Kingdom";
$LNG['country_us'] = "USA";
$LNG['country_uy'] = "Uruguay";
$LNG['country_uz'] = "Uzbekistan";
$LNG['country_vu'] = "Vanuatu";
$LNG['country_ve'] = "Venezuela";
$LNG['country_vn'] = "Vietnam";
$LNG['country_vg'] = "Virgin Islands (British)";
$LNG['country_vi'] = "Virgin Islands (U.S.)";
$LNG['country_eh'] = "Western Sahara";
$LNG['country_ye'] = "Yemen";
$LNG['country_yu'] = "Yugoslavia";
$LNG['country_zm'] = "Zambia";
$LNG['country_zw'] = "Zimbabwe";

?>
