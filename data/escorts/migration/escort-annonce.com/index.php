#!/usr/bin/php
<?php

set_time_limit(0);

require_once('common.php');

/*$sDB->query('UPDATE applications SET currency_symbol = "€" WHERE id IN (1, 2, 3, 4, 6)');
$sDB->query('UPDATE applications SET currency_symbol = "£" WHERE id = 5');
$sDB->query('UPDATE applications SET currency_symbol = "$" WHERE id = 7');*/


define('APP_ID', 			2);
define('APP_HOST', 			'6annonce.com');
define('APP_URL', 			'http://www.6annonce.com');
define('APP_TITLE', 		'6annonce.com');
define('APP_PHONE', 		null);
define('APP_COUNTRY', 		71);
define('APP_API_KEY', 		'188e18301e3c4d071cdc2c1aed9515eb');
define('APP_COUNTRY_ISO', 	'fr');
define('APP_MIN_PHOTOS', 	1);
define('APP_USER_SALT_HASH',	'Z3a6C200pa');
define('APP_CURRENCY_ID',		2);
define('APP_CURRENCY_SYMBOL',	'');
define('APP_CURRENCY_TITLE',	'EUR');


$app_langs = array('fr', 'en');

/*OKA*/
function _get_geography()
{
	global $pDB, $sDB;
	
	$_countries = $sDB->fetchAll('SELECT id, iso FROM countries');
	$countries = array();
	foreach ( $_countries as $country ) {
		$countries[$country['iso']] = $country['id'];
	}
	
	$_cities = $sDB->fetchAll('SELECT id, internal_id FROM cities');
	$cities = array();
	foreach ( $_cities as $city ) {
		$cities[$city['internal_id']] = $city['id'];
	}
	
	return array('countries' => $countries, 'cities' => $cities);
}

/*OKA*/
function stage_internal()
{
	global $pDB, $sDB, $app_langs;

	/* >>> application info */
	$sDB->query('SET FOREIGN_KEY_CHECKS = 0');
	
	$sDB->query('DELETE FROM applications WHERE id = ' . APP_ID);
	$sDB->insert('applications', array(
		'id' 			=> APP_ID,
		'host'		 	=> APP_HOST,
		'url' 			=> APP_URL,
		'title' 		=> APP_TITLE,
		'phone_number' 	=> APP_HOST,
		'country_id' 	=> APP_COUNTRY,
		'api_key' 		=> APP_API_KEY,
		'min_photos'	=> APP_MIN_PHOTOS,
		'currency_id' => APP_CURRENCY_ID,
		'currency_symbol' => APP_CURRENCY_SYMBOL,
		'currency_title' => APP_CURRENCY_TITLE
	));
	
	$sDB->query('DELETE FROM application_langs WHERE application_id = ' . APP_ID);
	foreach ( $app_langs as $lang_id ) {
		$data = array(
			'application_id' => APP_ID,
			'lang_id' => $lang_id
		);
		if ( $lang_id == APP_COUNTRY_ISO ) {
			$data = array_merge($data, array('is_default' => 1));
		}
		$sDB->insert('application_langs', $data);
	}
	/* <<< */
	
	/* >>> backend users */
	$users = $pDB->fetchAll('SELECT * FROM esc_admin_users WHERE application_id = ' . APP_ID);

//	$sDB->query('ALTER TABLE backend_users ADD internal_id INT(10) UNSIGNED DEFAULT NULL');

	$sDB->query('DELETE FROM backend_users WHERE application_id = ' . APP_ID);
	foreach ( $users as $old_user ) {
		if ( 99 == $old_user['admin_type'] ) {
			$type = 'superadmin';
		}
		elseif ( 1 == $old_user['admin_type'] ) {
			$type = 'admin';
		}
		elseif ( 3 == $old_user['admin_type'] ) {
			$type = 'data entry';
		}

		$is_user = $sDB->fetchOne('SELECT TRUE FROM backend_users WHERE id = ' . $old_user['id']);

		if ( $is_user ) {
			$user = array(
				'username' => $old_user['login'],
				'password' => $old_user['password'],
				'email' => $old_user['email'],
				'type' => $type,
				'is_disabled' => ! $old_user['status'],
				'application_id' => APP_ID,
				
				'internal_id' => $old_user['id']
			);
		}
		else {
			$user = array(
				'id' => $old_user['id'],
				'username' => $old_user['login'],
				'password' => $old_user['password'],
				'email' => $old_user['email'],
				'type' => $type,
				'is_disabled' => ! $old_user['status'],
				'application_id' => APP_ID
			);
		}
		
		$sDB->insert('backend_users', $user);
	}
	/* <<< */
}

/*OKA*/
function stage_members($mode)
{
	global $pDB, $sDB;
	
	$sDB->query('DELETE m, u FROM members m INNER JOIN users u ON u.id = m.user_id WHERE u.application_id = ' . APP_ID);
	
	if ( MODE_CLEAN_ONLY == $mode ) return;
	
	extract(_get_geography());
	
	$members = $pDB->fetchAll('
		SELECT u.*, u.id AS user_id, u.email AS user_email, u.status AS user_status, m.city, m.country_iso, m.about_me
		/* PREMIUMS */, u.pay_status, u.paid_until, u.recuring

		FROM esc_member_details m
		INNER JOIN esc_users u ON u.id = m.id
		WHERE u.application_id = ' . APP_ID . '
	');
	
	$members_count = $pDB->fetchOne('
		SELECT COUNT(u.id)
		FROM esc_member_details m
		INNER JOIN esc_users u ON u.id = m.id
		WHERE u.application_id = ' . APP_ID . '
	');
	
	$c = 0;
	foreach ( $members as $old_member ) {
		$user = array(
			'id' => $old_member['user_id'],
			'email' => $old_member['user_email'],
			'username' => $old_member['login'],
			'password' => $old_member['password'],
			'salt_hash' => APP_USER_SALT_HASH,
			'user_type' => 'member',
			'activation_hash' => null,
			'date_registered' => $old_member['creation_date'],
			'status' => $old_member['user_status'],
			'disabled' => 0,
			'date_last_refreshed' => null,
			'application_id' => APP_ID,
			'sales_user_id' => null/*$old_member['admin_user_id']*/,
			'city_id' => $cities[$old_member['city']],
			'country_id' => @$countries[$old_member['country_iso']],
			'last_ip' => $old_member['last_ip'],
		);

		if ( isset($old_member['admin_user_id']) ) {
			if ( ! $sDB->fetchOne('SELECT TRUE FROM backend_users WHERE id = ' . $old_member['admin_user_id']) ) {
				$user = array_merge($user, array('sales_user_id' => $sDB->fetchOne('SELECT id FROM backend_users WHERE internal_id = ' . $old_member['admin_user_id'])));
			}
		}

		if ( $sDB->fetchOne('SELECT TRUE FROM users WHERE username = "' . $old_member['login'] . '"') ) {
			$user = array_merge($user, array('username' => '***' . $old_member['login']) );
		}

		if ( $sDB->fetchOne('SELECT TRUE FROM users WHERE id = "' . $old_member['user_id'] . '"') ) {
			unset($user['id']);
			$user = array_merge($user, array('__id' => $old_member['id']) );
		}
		
		try {
			$sDB->insert('users', $user);
		}
		catch ( Exception $e ) {
			echo $e->getMessage() . "\r\n";
		}

		if ( isset($user['__id']) ) {
			$new_user_id = $sDB->lastInsertId();
		}
		else {
			$new_user_id = $user['id'];
		}

		try {
			$sDB->insert('members', array(
				'user_id' => $new_user_id,
				'country_id' => @$countries[$old_member['country_iso']],
				'city_id' => $cities[$old_member['city']],
				'email' => $old_member['user_email'],
				'about_me' => $old_member['about_me'],
				'is_premium' => $old_member['pay_status'] == 1 ? 1 : 0,
				'date_expires' => $old_member['paid_until'],
				'is_recurring' => $old_member['recuring']
			));
		}
		catch ( Exception $e ) {
			echo $e->getMessage() . "\r\n";
		}
		
		$c++;
		if ( $c % 500 == 0 ) echo "[$c/$members_count] Member '{$user['username']}'\r\n";
	}
	
	echo "$c members has been migrated in total\r\n";
}

function stage_fix_clubs()
{
	global $pDB, $sDB;

	extract(_get_geography());

	$clubs = $pDB->fetchAll('
		SELECT u.*, u.email AS user_email, a.*
		FROM esc_clubs a
		INNER JOIN esc_users u ON u.id = a.user_id
	');

	$clubs_count = $pDB->fetchOne('
		SELECT COUNT(DISTINCT(a.user_id))
		FROM esc_clubs a
		INNER JOIN esc_users u ON u.id = a.user_id
	');

	foreach ( $clubs as $club ) {
		$sDB->query('DELETE FROM users WHERE id = ' . $club['user_id']);
		$sDB->query('DELETE FROM agencies WHERE user_id = ' . $club['user_id']);
	}

	$c = 0;
	foreach ( $clubs as $old_club ) {
		$user = array(
			'id' => $old_club['user_id'],
			'email' => $old_club['user_email'],
			'username' => $old_club['login'],
			'password' => $old_club['password'],
			'salt_hash' => APP_USER_SALT_HASH,
			'user_type' => 'agency',
			'activation_hash' => null,
			'date_registered' => $old_club['creation_date'],
			'status' => $old_club['status'],
			'disabled' => 0,
			'date_last_refreshed' => null,
			'sales_user_id' => $old_club['admin_user_id'],
			'application_id' => APP_ID,
			'city_id' => $cities[$old_club['city']],
			'country_id' => APP_COUNTRY,
			'last_ip' => $old_club['last_ip'],
			'is_club' => true
		);

		$sDB->insert('users', $user);

		$agency = array(
			'user_id' => $old_club['user_id'],
			'last_modified' => null,
			'country_id' => $countries[$old_club['country_iso']],
			'city_id' => $cities[$old_club['city']],
			'name' => $old_club['name'],
			'email' => $old_club['user_email'],
			'web' => $old_club['web'],
			'phone' => $old_club['phone'],
			'phone_instructions' => $old_club['phone_instructions'],
			'logo_hash' => null,
			'logo_ext' => null,
			'hit_count' => 0
		);

		$sDB->insert('agencies', $agency);

		$agency_id = $sDB->lastInsertId();

		$sDB->query('UPDATE escorts SET agency_id = ' . $agency_id . ' WHERE user_id = ' . $agency['user_id']);

		$c++;
		echo "[$c/$clubs_count] Agency '{$agency['name']}'\r\n";
	}

	echo "$c agencies has been migrated in total\r\n";
}

/*OKA*/
function stage_agencies($mode)
{
	global $pDB, $sDB;

	$sDB->query('DELETE a, u FROM agencies a LEFT JOIN users u ON u.id = a.user_id WHERE u.application_id = ' . APP_ID);

	if ( MODE_CLEAN_ONLY == $mode ) return;
	
	extract(_get_geography());

	$agencies = $pDB->fetchAll('
		SELECT u.*, u.email AS user_email, a.*, u.application_id AS u_app_id, a.application_id AS a_app_id
		FROM esc_agencies a
		INNER JOIN esc_users u ON u.id = a.user_id
		/*WHERE a.application_id = "' . APP_ID . '" OR a.country_iso = "' . APP_COUNTRY_ISO . '"*/
	');

	$agencies_count = $pDB->fetchOne('
		SELECT COUNT(DISTINCT(a.user_id))
		FROM esc_agencies a
		INNER JOIN esc_users u ON u.id = a.user_id
		/*WHERE a.application_id = "' . APP_ID . '" OR a.country_iso = "' . APP_COUNTRY_ISO . '"*/
	');

	//$sDB->query('ALTER TABLE agencies ADD __id INT(10) UNSIGNED DEFAULT NULL');

	$c = 0;
	foreach ( $agencies as $old_agency ) {
		$application_id = ($old_agency['a_app_id'] ? $old_agency['a_app_id'] : $old_agency['u_app_id']);

		$user = array(
			'id' => $old_agency['user_id'],
			'email' => $old_agency['user_email'],
			'username' => $old_agency['login'],
			'password' => $old_agency['password'],
			'salt_hash' => APP_USER_SALT_HASH,
			'user_type' => 'agency',
			'activation_hash' => null,
			'date_registered' => $old_agency['creation_date'],
			'status' => $old_agency['status'],
			'disabled' => 0,
			'date_last_refreshed' => null,
			'sales_user_id' => $old_agency['admin_user_id'],
			'application_id' => APP_ID /*($application_id == 1 ? 9 : $application_id)*/,
			'city_id' => $cities[$old_agency['city']],
			'country_id' => $countries[$old_agency['country_iso']],
			'last_ip' => $old_agency['last_ip'],
		);

		if ( $old_agency['admin_user_id'] ) {
			if ( ! $sDB->fetchOne('SELECT TRUE FROM backend_users WHERE id = ' . $old_agency['admin_user_id']) ) {
				$user = array_merge($user, array('sales_user_id' => $sDB->fetchOne('SELECT id FROM backend_users WHERE internal_id = ' . $old_agency['admin_user_id'])));
			}
		}

		if ( $sDB->fetchOne('SELECT TRUE FROM users WHERE username = "' . $old_agency['login'] . '"') ) {
			$user = array_merge($user, array('username' => '***' . $old_agency['login']) );
		}

		if ( $sDB->fetchOne('SELECT TRUE FROM users WHERE id = "' . $old_agency['user_id'] . '"') ) {
			unset($user['id']);
			$user = array_merge($user, array('__id' => $old_agency['id']) );
		}

		try {
			$sDB->insert('users', $user);
		}
		catch (Exception $e) {
			echo $e->getMessage() . "\r\n";
		}

		if ( isset($user['__id']) ) {
			$new_user_id = $sDB->lastInsertId();
		}
		else {
			$new_user_id = $user['id'];
		}

		if ( ! strlen($old_agency['name']) ) {
			$old_agency['name'] = ucfirst($old_agency['login']);
		}

		$agency = array(
			'user_id' => $new_user_id,
			'last_modified' => null,
			'country_id' => $countries[$old_agency['country_iso']],
			'city_id' => $cities[$old_agency['city']],
			'name' => $old_agency['name'],
			'email' => $old_agency['email'],
			'web' => $old_agency['web'],
			'phone' => $old_agency['phone'],
			'phone_instructions' => $old_agency['phone_instructions'],
			'logo_hash' => null,
			'logo_ext' => null,
			'hit_count' => 0,
			'__id' => $old_agency['id']
		);

		try {
			$sDB->insert('agencies', $agency);
		}
		catch (Exception $e) {
			echo $e->getMessage() . "\r\n";
		}

		$c++;
		echo "[$c/$agencies_count] Agency '{$agency['name']}'\r\n";
	}

	echo "$c agencies has been migrated in total\r\n";
}

/*OKA*/
function stage_escorts($mode)
{
	global $pDB, $sDB;
	$sDB->query('SET FOREIGN_KEY_CHECKS = 0');
	// Delete all independent escorts
	$sDB->query('
		DELETE e, ep, ec, er, el, ewt, u
		FROM escorts e
		LEFT JOIN users u ON u.id = e.user_id
		LEFT JOIN escort_profiles ep ON ep.escort_id = e.id
		LEFT JOIN escort_cities ec ON ec.escort_id = e.id
		LEFT JOIN escort_rates er ON er.escort_id = e.id
		LEFT JOIN escort_langs el ON el.escort_id = e.id
		LEFT JOIN escort_working_times ewt ON ewt.escort_id = e.id
		WHERE u.user_type = "escort" AND u.application_id = ' . APP_ID . '
	');
	
	// Delete all agency escorts without deleting corresponding user ('cause it is agency user)
	$sDB->query('
		DELETE e, ep, ec, er, el, ewt
		FROM escorts e
		LEFT JOIN users u ON u.id = e.user_id
		LEFT JOIN escort_profiles ep ON ep.escort_id = e.id
		LEFT JOIN escort_cities ec ON ec.escort_id = e.id
		LEFT JOIN escort_rates er ON er.escort_id = e.id
		LEFT JOIN escort_langs el ON el.escort_id = e.id
		LEFT JOIN escort_working_times ewt ON ewt.escort_id = e.id
		WHERE u.user_type = "agency" AND u.application_id = ' . APP_ID . '
	');
	
	if ( MODE_CLEAN_ONLY == $mode ) return;
	
	extract(_get_geography());
	
	$escorts = $pDB->fetchAll('
		SELECT
			u.*, u.id AS user_id, u.email AS user_email, u.status AS user_status,
			u.creation_date AS u_creation_date, e.*, e.status AS escort_status,
			e.creation_date AS c_d, e.country_iso AS escort_country_iso,
			e.verified AS auto_approval,
			ed.*, ec.city_id AS city, e.votes_sum, e.votes_count
		FROM esc_escorts e
		INNER JOIN esc_escort_details ed ON ed.id = e.id
		INNER JOIN esc_users u ON u.id = e.user_id
		LEFT JOIN esc_escort_cities ec ON ec.escort_id = e.id
		WHERE (u.user_type = 1 OR u.user_type = 2/* OR u.user_type = 4*/) /*AND e.country_iso = "' . APP_COUNTRY_ISO .'"*/ AND u.application_id = "' . APP_ID . '"
		GROUP BY e.id
	');
	
	$escorts_count = $pDB->fetchOne('
		SELECT COUNT(DISTINCT(e.id))
		FROM esc_escorts e
		INNER JOIN esc_escort_details ed ON ed.id = e.id
		INNER JOIN esc_users u ON u.id = e.user_id
		LEFT JOIN esc_escort_cities ec ON ec.escort_id = e.id
		WHERE (u.user_type = 1 OR u.user_type = 2/* OR u.user_type = 4*/) /*AND e.country_iso = "' . APP_COUNTRY_ISO .'"*/ AND u.application_id = "' . APP_ID . '"
	');
	
	$_nationalities = $sDB->fetchAll('SELECT id, iso FROM nationalities');
	$nationalities = array();
	foreach ( $_nationalities as $nat ) {
		$nationalities[$nat['iso']] = $nat['id'];
	}
	
	$c = 0;
	$e = 0;
	$ee = 0;
	/*
		define('USER_TYPE_SINGLE_GIRL',             1);
		define('USER_TYPE_AGENCY',                  2);
		define('USER_TYPE_MEMBER',                  3);
		define('USER_TYPE_CLUB',                    4);
	 */

	$sDB->query('SET NAMES `utf8`');

	foreach ( $escorts as $old_escort ) {
		$c++;
		/*if ( 1 != $old_escort['user_type'] ) {
			$c++;
		}
		
		continue;*/

		// Insert user only if this is an independent escort
		if ( 1 == $old_escort['user_type']/* || 4 == $old_escort['user_type']*/ ) {
		 	$user = array(
				'id' => $old_escort['user_id'],
				'email' => $old_escort['user_email'],
				'username' => $old_escort['login'],
				'password' => $old_escort['password'],
				'salt_hash' => APP_USER_SALT_HASH,
				'user_type' => 'escort',
				'activation_hash' => null,
				'date_registered' => $old_escort['c_d'],
				'status' => $old_escort['user_status'],
				'disabled' => 0,
				'date_last_refreshed' => $old_escort['new_refresh'],
				'sales_user_id' => $old_escort['admin_user_id'],
				'application_id' => APP_ID,
				'city_id' => $cities[$old_escort['city']],
				'country_id' => APP_COUNTRY,
				'last_ip' => $old_escort['last_ip'],
			);

			if ( $old_escort['admin_user_id'] ) {
				if ( ! $sDB->fetchOne('SELECT TRUE FROM backend_users WHERE id = ' . $old_escort['admin_user_id']) ) {
					$user = array_merge($user, array('sales_user_id' => $sDB->fetchOne('SELECT id FROM backend_users WHERE internal_id = ' . $old_escort['admin_user_id'])));
				}
			}

			if ( $sDB->fetchOne('SELECT TRUE FROM users WHERE username = "' . $old_escort['login'] . '"') ) {
				$user = array_merge($user, array('username' => '***' . $old_escort['login']) );
			}

			if ( $sDB->fetchOne('SELECT TRUE FROM users WHERE id = "' . $old_escort['user_id'] . '"') ) {
				unset($user['id']);
				$user = array_merge($user, array('__id' => $old_escort['user_id']) );
			}
			
			try {
				$sDB->insert('users', $user);
			}
			catch (Exception $e) {
				echo 'User Invalid: ' . $e->getMessage() . "\r\n";
				var_dump($user);
				continue;
			}
		}

		if ( 0 == $old_escort['100_verify'] || 3 == $old_escort['100_verify'] ) {
			$verify_status = 1;
		}
		elseif ( 1 == $old_escort['100_verify'] ) {
			$verify_status = 2;
		}
		elseif ( 2 == $old_escort['100_verify'] ) {
			$verify_status = 3;
		}

		if ( isset($user['__id']) ) {
			$new_user_id = $sDB->lastInsertId();
		}
		else {
			$new_user_id = $old_escort['user_id'];
		}

		$escort = array(
			'user_id' => $new_user_id,
			'agency_id' => null,
			'showname' => $old_escort['showname'],
			'gender' => $old_escort['gender'],
			'date_registered' => $old_escort['c_d'],
			'country_id' => $countries[$old_escort['country_iso']],
			'city_id' => $cities[$old_escort['city']],
			'cityzone_id' => null,
			'verified_status' => $verify_status,
			'date_last_modified' => null,
			'status' => $old_escort['escort_status'],
			'votes_sum' => $old_escort['votes_sum'],
			'votes_count' => $old_escort['votes_count'],
			'auto_approval' => $old_escort['auto_approval'],
			'__id' => $old_escort['id']
		);

		if ( $sDB->fetchOne('SELECT TRUE FROM escorts WHERE showname = "' . $old_escort['showname'] . '"') ) {
			$escort = array_merge($escort, array('showname' => '***' . $old_escort['showname']) );
		}

		/* >>> Check if this is agency escort */
		if ( 2 == $old_escort['user_type'] ) {
			try {
				//var_dump($old_escort['user_id']);
				$agency_id = $sDB->fetchOne('SELECT id FROM agencies WHERE __id = "' . $old_escort['user_id'] . '"');
				//var_dump($agency_id);
				//sleep(5);
			}
			catch (Exception $e) {
				var_dump($new_user_id); die;
				echo 'Select Agency ID: ' . $e->getMessage() . "\r\n";
			}

			if ( ! $agency_id ) continue;

			$e++;

			$escort['agency_id'] = $agency_id;
		}
		else {
			$ee++;
		}
		/* <<< */

		try {
			$sDB->insert('escorts', $escort);
		}
		catch (Exception $e) {
			echo 'Escort: ' . $e->getMessage() . "\r\n";
			continue;
		}
		
		$new_escort_id = $sDB->lastInsertId();

		$escort_profile = array(
			'escort_id' => $new_escort_id,
			'height' => $old_escort['height'],
			'weight' => $old_escort['weight'],
			'bust' => $old_escort['bust'],
			'waist' => $old_escort['waist'],
			'hip' => $old_escort['hip'],
			'shoe_size' => $old_escort['shoe_size'],
			'breast_size' => $old_escort['breast_size'],
			'dress_size' => $old_escort['dress_size'],
			'is_smoker' => $old_escort['smoker'],
			'hair_color' => $old_escort['hair_color'],
			'eye_color' => $old_escort['eye_color'],
			'birth_date' => $old_escort['birth_date'],
			'characteristics' => $old_escort['characteristics'],
			'contact_phone' => $old_escort['contact_phone'],
			'contact_phone_parsed' => $old_escort['contact_phone_parsed'],
			'phone_instructions' => $old_escort['phone_instructions'],
			'contact_email' => $old_escort['contact_email'],
			'contact_web' => $old_escort['contact_web'],
			'contact_zip' => $old_escort['contact_zip'],
			'measure_units' => $old_escort['metric_system'],
			'availability' => $old_escort['available_for'],
			'sex_availability' => $old_escort['sex_availability'],
			'svc_kissing' => $old_escort['s_kissing'],
			'svc_blowjob' => $old_escort['s_blowjob'],
			'svc_cumshot' => $old_escort['s_cumshot'],
			'svc_69' => $old_escort['s_69'],
			'svc_anal' => $old_escort['s_anal'],
			'svc_additional' => $old_escort['svc_additional'],
			'ethnicity' => $old_escort['ethnic'],
			'nationality_id' => $nationalities[$old_escort['nationality_iso']],
			'admin_verified' => $old_escort['admin_verified'],
			'hit_count' => $old_escort['hit_count'],
			'_photos' => $old_escort['photos'],
			'_photos_private' => $old_escort['photos_private'],
			'_photos_verified' => $old_escort['photos_verified'],
			'_photos_private_verified' => $old_escort['photos_private_verified']
		);

		/*Add "Allow Null" for fields svc_additional_gr,(fr,en,it,de) in online DB*/

		try {
			$sDB->insert('escort_profiles', $escort_profile);
		}
		catch (Exception $e) {
			echo 'Escort Profile: ' . $e->getMessage() . "\r\n";
			continue;
		}

		echo "[$c/$escorts_count] Escort '{$escort['showname']}' has been migrated\r\n";
	}
	/*var_dump($c);
die;*/
	echo "\r\n------- Total Agency Escorts: $e, Total Independent Escorts: $ee ------\r\n";

	echo "\r\n$c escorts has been migrated in total\r\n";


	echo "\r\nMigrating escort cities";

	$_cities = $pDB->fetchAll('
		SELECT ec.escort_id, ec.city_id
		FROM esc_escorts e
		/*INNER JOIN esc_escort_details ed ON ed.id = e.id*/
		INNER JOIN esc_users u ON u.id = e.user_id
		INNER JOIN esc_escort_cities ec ON ec.escort_id = e.id AND ec.city_id IS NOT NULL
		WHERE (u.user_type = 1 OR u.user_type = 2/* OR u.user_type = 4*/) /*AND e.country_iso = "' . APP_COUNTRY_ISO .'"*/ AND u.application_id = "' . APP_ID . '"
	');

	foreach ( $_cities as $city ) {
	   if ( $city['city_id'] )
	   {
			try {

				$escort_id = $sDB->fetchOne('SELECT id FROM escorts WHERE __id = ' . $city['escort_id']);

				if ( $escort_id ) {
					$sDB->insert('escort_cities', array(
						'escort_id' => $escort_id,
						'city_id' => $cities[$city['city_id']]
					));
				}
			}
			catch (Exception $e) {
				echo $e->getMessage() . "\r\n";
			}
	   }
	}

	echo " - done\r\n";
	

	echo "Migrating escort rates";

	$rates = $pDB->fetchAll('
		SELECT er.*
		FROM esc_escorts e
		INNER JOIN esc_escort_details ed ON ed.id = e.id
		INNER JOIN esc_users u ON u.id = e.user_id
		INNER JOIN esc_escort_rates er ON er.escort_id = e.id
		WHERE (u.user_type = 1 OR u.user_type = 2/* OR u.user_type = 4*/) /*AND e.country_iso = "' . APP_COUNTRY_ISO .'"*/ AND u.application_id = "' . APP_ID . '"
	');

	foreach ( $rates as $rate ) {
		try {

			$escort_id = $sDB->fetchOne('SELECT id FROM escorts WHERE __id = ' . $rate['escort_id']);

			if ( $escort_id ) {
				$sDB->insert('escort_rates', array(
					'escort_id' => $escort_id,
					'availability' => $rate['available_for'],
					'time' => intval($rate['time']),
					'time_unit' => $rate['time_unit'],
					'price' => $rate['price'],
					'currency_id' => APP_CURRENCY_ID
				));
			}
		}
		catch ( Exception $e ) {
			echo $e->getMessage() . "\r\n";
		}
	}

	echo "- done\r\n";

	echo "Migrating escort languages";

	$langs = $pDB->fetchAll('
		SELECT el.*
		FROM esc_escorts e
		INNER JOIN esc_escort_details ed ON ed.id = e.id
		INNER JOIN esc_users u ON u.id = e.user_id
		INNER JOIN esc_escort_languages el ON el.escort_id = e.id
		WHERE (u.user_type = 1 OR u.user_type = 2/* OR u.user_type = 4*/) /*AND e.country_iso = "' . APP_COUNTRY_ISO .'"*/ AND u.application_id = "' . APP_ID . '"
	');

	foreach ( $langs as $lang ) {
		try {

			$escort_id = $sDB->fetchOne('SELECT id FROM escorts WHERE __id = ' . $lang['escort_id']);

			if ( $escort_id ) {
				$sDB->insert('escort_langs', array(
					'escort_id' => $escort_id,
					'level' => $lang['level'],
					'lang_id' => $lang['lng_iso']
				));
			}
		}
		catch (Exception $e) {
			echo $e->getMessage() . "\r\n";
		}
	}

	echo "- done\r\n";

	echo "Migrating escort working times";

	$wts = $pDB->fetchAll('
		SELECT ewt.*
		FROM esc_escorts e
		INNER JOIN esc_escort_details ed ON ed.id = e.id
		INNER JOIN esc_users u ON u.id = e.user_id
		INNER JOIN esc_escort_working_times ewt ON ewt.escort_id = e.id
		WHERE (u.user_type = 1 OR u.user_type = 2/* OR u.user_type = 4*/) /*AND e.country_iso = "' . APP_COUNTRY_ISO .'"*/ AND u.application_id = "' . APP_ID . '"
	');

	foreach ( $wts as $wt ) {
		try {

			$escort_id = $sDB->fetchOne('SELECT id FROM escorts WHERE __id = ' . $wt['escort_id']);

			if ( $escort_id ) {
				$sDB->insert('escort_working_times', array(
					'escort_id' => $escort_id,
					'day_index' => $wt['day'],
					'time_from' => $wt['time_from'],
					'time_from_m' => '',
					'time_to' => $wt['time_to'],
					'time_to_m' => ''
				));
			}
		}
		catch (Exception $e) {
			echo $e->getMessage() . "\r\n";
		}
	}

	echo "- done\r\n";
}

function stage_fixinternational()
{
	global $pDB, $sDB, $app_langs;

	//$sDB->query('ALTER TABLE escorts ADD is_international TINYINT(10) UNSIGNED DEFAULT 0');
	
	$escorts = $sDB->fetchAll('SELECT id, city_id, country_id FROM escorts');

	foreach ( $escorts as $escort ) {
		if ( $escort['country_id'] != APP_COUNTRY ) {
			$sDB->query('UPDATE escorts SET home_city_id = ' . $escort['city_id'] . ', city_id = 431, country_id = ' . APP_COUNTRY . ', is_international = 1 WHERE id = ' . $escort['id']);

			$sDB->query('DELETE FROM escort_cities WHERE escort_id = ' . $escort['id']);

			$sDB->insert('escort_cities', array(
				'escort_id' => $escort['id'],
				'city_id' => 431
			));
		}
	}
}

/*OKA*/
function stage_tours($mode)
{
	global $pDB, $sDB;
	
	// Delete all independent escorts
	$sDB->query('
		DELETE t
		FROM tours t
		INNER JOIN escorts e ON e.id = t.escort_id
		INNER JOIN users u ON u.id = e.user_id
		WHERE u.application_id = ' . APP_ID . '
	');
	
	if ( MODE_CLEAN_ONLY == $mode ) return;
	
	extract(_get_geography());
	
	$tours = $pDB->fetchAll('
		SELECT t.id, t.escort_id, t.date_from, t.date_to, t.email, t.phone, t.country_iso, t.city_id
		FROM esc_tours t
		INNER JOIN esc_escorts e ON e.id = t.escort_id
		INNER JOIN esc_escort_details ed ON ed.id = e.id
		INNER JOIN esc_users u ON u.id = e.user_id
		WHERE (u.user_type = 1 OR u.user_type = 2 /*OR u.user_type = 4*/) /*AND e.country_iso = "' . APP_COUNTRY_ISO .'"*/ AND u.application_id = "' . APP_ID . '"
	');
	
	$tours_count = $pDB->fetchOne('
		SELECT COUNT(t.id)
		FROM esc_tours t
		INNER JOIN esc_escorts e ON e.id = t.escort_id
		INNER JOIN esc_escort_details ed ON ed.id = e.id
		INNER JOIN esc_users u ON u.id = e.user_id
		WHERE (u.user_type = 1 OR u.user_type = 2 /*OR u.user_type = 4*/) /*AND e.country_iso = "' . APP_COUNTRY_ISO .'"*/ AND u.application_id = "' . APP_ID . '"
	');
	//echo $tours_count; die;
	$c = 0;
	foreach ( $tours as $old_tour ) {
		$c++;
		
		if ( ! $countries[$old_tour['country_iso']] || 
			! $cities[$old_tour['city_id']] ) continue;

		$escort_id = $sDB->fetchOne('SELECT id FROM escorts WHERE __id = ' . $old_tour['escort_id']);

		$tour = array(
			/*'id' => $old_tour['id'],*/
			'escort_id' => $escort_id,
			'date_from' => $old_tour['date_from'],
			'date_to' => $old_tour['date_to'],
			'email' => $old_tour['email'],
			'phone' => $old_tour['phone'],
			'country_id' => $countries[$old_tour['country_iso']],
			'city_id' => $cities[$old_tour['city_id']]
		);
		
		try {
			if ( $escort_id ) {
				$sDB->insert('tours', $tour);
			}
		}
		catch ( Exception $e ) {
			echo 'Could not insert tour with id: ' . $tour['id'] . "\nReason: " . $e->getMessage() . "\n\n";
			continue;
		}
		
		// echo "[$c/$escorts_count] Escort '{$escort['showname']}' has been migrated\r\n";
	}
	
	echo "\r\n$c tours has been migrated in total\r\n";
}

function stage_comments()
{
	global $pDB, $sDB;

	$comments = $pDB->fetchAll('
		SELECT *
		FROM esc_comments c
		INNER JOIN esc_escorts e ON e.id = c.escort_id
		INNER JOIN esc_users u ON u.id = e.user_id
		WHERE /*e.country_iso = "' . APP_COUNTRY_ISO .'"*/ u.application_id = "' . APP_ID . '"
	');

	$c = 0;
	foreach ( $comments as $old_comment ) {
		$c++;

		$escort_data = $sDB->fetchRow('SELECT id, user_id FROM escorts WHERE __id = ' . $old_comment['escort_id']);

		$comment = array(
			/*'id' => $old_comment['id'],*/
			'user_id' => $escort_data['user_id'],
			'escort_id' => $escort_data['id'],
			'time' => $old_comment['time'],
			'is_reply_to' => $old_comment['is_reply_to'],
			'status' => $old_comment['status'],
			'message' => $old_comment['message'],
			'thumbup_count' => $old_comment['thumbup_count'],
			'thumbdown_count' => $old_comment['thumbdown_count']
		);

		

		try {
			if ( $escort_data ) {
				$sDB->insert('comments', $comment);
			}
		}
		catch ( Exception $e ) {
			echo 'Could not insert comment with id: ' . $comment['id'] . "\nReason: " . $e->getMessage() . "\n\n";
		}

		// echo "[$c/$escorts_count] Escort '{$escort['showname']}' has been migrated\r\n";
	}

	echo "\r\n$c Comments has been migrated in total\r\n";
}

/*OKA*/
function stage_photos($mode)
{
	global $sDB, $pDB;
	
	$sDB->query('
		DELETE
			ep
		FROM escort_photos ep
		INNER JOIN escorts e ON e.id = ep.escort_id
		INNER JOIN users u ON u.id = e.user_id
		WHERE /*e.country_id = "' . APP_COUNTRY . '"*/ u.application_id = "' . APP_ID . '"
	');
	
	if ( MODE_CLEAN_ONLY == $mode ) return;
	
	$escorts = $sDB->fetchAll('
		SELECT
			ep.escort_id, ep._photos, ep._photos_private, ep._photos_verified, ep._photos_private_verified
		FROM escort_profiles ep
		INNER JOIN escorts e ON e.id = ep.escort_id
		INNER JOIN users u ON u.id = e.user_id
		WHERE /*e.country_id = "' . APP_COUNTRY . '"*/ u.application_id = "' . APP_ID . '"
	');
	
	foreach ( $escorts as $escort ) {
		list($escort_id, $photos, $photos_private, $verified, $veriried_private) = array_values($escort);
		$photos 			= explode(',', $photos);
		$photos_private 	= explode(',', $photos_private);
		$verified 			= explode(',', $verified);
		$verified_private 	= explode(',', $verified_private);
		
		$_photos = array();
		
		// Migrate general photos
		foreach ( $photos as $i => $photo ) {
			if ( ! is_numeric($photo) ) continue;

			$type = 1;
			if ( $pDB->fetchOne('SELECT is_soft FROM escort_photos WHERE escort_id = ' . $escort_id . ' AND photo_id = ' . $photo) ) {
				$type = 2;
			}

			$_photos[] = array(
				'escort_id' => $escort_id,
				'hash' =>  md5($escort_id . $ordering . $photo) /* md5(microtime(true) * rand(10000, 99999)) */,
				'ext' => 'jpg',
				'type' => $type,
				'status' => in_array($photo, $verified) ? 1 : 3,
				'is_main' => $photo == 0,
				'ordering' => $i + 1,
				'_inter_index' => $photo,
				'is_approved' => 1
			);
		}
		
		// Migrate private photos
		foreach ( $photos_private as $i => $photo ) {
			if ( ! is_numeric($photo) ) continue;
			
			$_photos[] = array(
				'escort_id' => $escort_id,
				'hash' => md5($escort_id . $ordering . $photo) /* md5(microtime(true) * rand(10000, 99999)) */,
				'ext' => 'jpg',
				'type' => 3,
				'status' => in_array($photo, $verified_private) ? 1 : 3,
				'ordering' => $i + 1,
				'_inter_index' => $photo,
				'is_approved' => 1
			);
		}
		
		foreach ( $_photos as $photo ) {
			try {
				$sDB->insert('escort_photos', $photo);
			}
			catch ( Exception $e ) {
				continue;
				//var_dump($photo); die;
			}
		}
	}
}

function stage_dlphotos()
{
	global $sDB;
	
	$photos = $sDB->fetchAll('
		SELECT e.showname, ep.escort_id, ep._inter_index, ep.hash, ep.ext, ep.type
		FROM escort_photos ep
		INNER JOIN escorts e ON e.id = ep.escort_id
		WHERE e.country_id = ' . APP_COUNTRY . '
	');
	
	$photos_count = $sDB->fetchOne('
		SELECT COUNT(ep.id)
		FROM escort_photos ep
		INNER JOIN escorts e ON e.id = ep.escort_id
		WHERE e.country_id = ' . APP_COUNTRY . '
	');
	
	$c = 0;
	foreach ( $photos as $photo ) {
		$file = _photos_get_path($photo);
		$c++;
		
		echo "[$c/$photos_count] (" . round($c / $photos_count * 100)  . "%) Escort '{$photo['showname']}: '";
		
		if ( is_file($file) ) {
			echo "skipped: $file\r\n";
			continue;
		}
		
		echo "download: $file - ";

		_photos_make_path($file);
		
		file_put_contents($file, file_get_contents('http://pic.' . APP_HOST . '/' . $photo['escort_id'] . '/' . ($photo['type'] == 3 ? 'private/' : '') . $photo['_inter_index'] . '_orig.jpg'));

		echo "done\r\n";
	}
}

define('ESCORT_STATUS_NO_PROFILE', 			1);		// 000000001
define('ESCORT_STATUS_NO_ENOUGH_PHOTOS', 	2);		// 000000010
define('ESCORT_STATUS_NOT_APPROVED', 		4);		// 000000100
define('ESCORT_STATUS_OWNER_DISABLED', 		8);		// 000001000
define('ESCORT_STATUS_ADMIN_DISABLED', 		16);	// 000010000
define('ESCORT_STATUS_ACTIVE',				32);	// 000100000
define('ESCORT_STATUS_IS_NEW',				64);	// 001000000
define('ESCORT_STATUS_PROFILE_CHANGED',		128);	// 010000000

define('STATUS_NO_PROFILE',                 -2);
define('STATUS_NO_PHOTOS',                  -1);
define('STATUS_WAITING_APPROVAL',           -5);
define('STATUS_OWNER_DISABLED',             -3);
define('STATUS_ADMIN_DISABLED',             -4);
define('STATUS_ACTIVE',                     1);

function stage_fixstatus()
{
	global $sDB, $pDB;

	$sDB->query('ALTER TABLE escorts ADD is_super_old TINYINT(1) UNSIGNED DEFAULT 0');

	$escorts = $sDB->fetchAll('
		SELECT e.id, e.showname, COUNT(DISTINCT(eph.id)) AS photos_count, e.status, e.__id
		FROM escorts e
		INNER JOIN users u ON u.id = e.user_id
		INNER JOIN escort_profiles ep ON ep.escort_id = e.id
		LEFT JOIN escort_photos eph ON eph.escort_id = e.id AND eph.type <> 3
		WHERE /*e.country_id = ' . APP_COUNTRY . '*/ u.application_id = "' . APP_ID . '"
		GROUP BY e.id
	');
	
	foreach ( $escorts as $escort ) {
		$status = 0;
		$ss = array();

		


		if ( APP_MIN_PHOTOS > $escort['photos_count'] || STATUS_NO_PHOTOS == $escort['status'] ) {
			$status |= ESCORT_STATUS_NO_ENOUGH_PHOTOS;
			$ss[] = 'no_enough_photos';
		}
		
		if ( STATUS_NO_PROFILE == $escort['status'] ) {
			$status |= ESCORT_STATUS_NO_PROFILE;
			$ss[] = 'no_profile';
		}
		
		if ( STATUS_WAITING_APPROVAL == $escort['status'] && ! ( $status & ESCORT_STATUS_NO_PHOTOS ) ) {
			$status |= ESCORT_STATUS_NOT_APPROVED;
			$ss[] = 'not_approved';
		}
		
		if ( STATUS_ADMIN_DISABLED == $escort['status'] ) {
			$status |= ESCORT_STATUS_ADMIN_DISABLED;
			$ss[] = 'admin_disabled';
		}
		
		if ( STATUS_OWNER_DISABLED == $escort['status'] ) {
			$status |= ESCORT_STATUS_OWNER_DISABLED;
			$ss[] = 'owner_disabled';
		}
		
		if ( $status & ESCORT_STATUS_NO_PROFILE && 0 == $escort['photos_count'] ) {
			$status |= ESCORT_STATUS_IS_NEW;
			$ss[] = 'is_new';
		}
		
		if ( ! ( $status & ESCORT_STATUS_NO_ENOUGH_PHOTOS ) && STATUS_ACTIVE == $escort['status'] ) {
			$status |= ESCORT_STATUS_ACTIVE;
			$ss[] = 'active';
		}
		
		//echo $escort['showname'] . ': ' . implode(' ', $ss) . "\n";
		$sql = '
			SELECT TRUE
			FROM esc_escort_packages ep
			WHERE
				ep.escort_id = ' . $escort['__id'] . ' AND 
				ep.application_id = 2 AND
				ep.package_id = 7 AND
				ep.expiration < NOW()
		';
		
		if ( true == $pDB->fetchOne($sql) ) {
			if ( ! ($status & ESCORT_STATUS_ADMIN_DISABLED) ) {
				$status |= ESCORT_STATUS_ADMIN_DISABLED;
				if ( $status & ESCORT_STATUS_ACTIVE ) {
					$status ^= ESCORT_STATUS_ACTIVE;
				}
			}

			$sDB->query('UPDATE escorts SET is_super_old = 1, free_comment = "This escort had expired default package (#7 Bronze Normal), so this escort has been disabled during migration" WHERE id = ' . $escort['id']);
		}

		$sDB->query('UPDATE escorts SET status = ' . $status . ' WHERE id = ' . $escort['id']);
	}
}

function stage_translations($mode, $section)
{
	$section_func = 'stage_translations_section_' . $section;
	if ( ! is_callable($section_func) ) {
		die('Invalid section');
	}
	
	call_user_func($section_func);
}

function stage_translations_section_countries()
{
	global $sDB, $app_langs;
	
	$countries = $sDB->fetchAll('SELECT id, iso FROM countries');
	
	foreach ( $app_langs as $lang_id ) {
		require('data/countries_' . $lang_id . '.php');
		
		foreach ( $countries as $country ) {
			$title = isset($LNG['country_' . $country['iso']]) ? '"' . $LNG['country_' . $country['iso']] . '"' : 'NULL';
			$sDB->query('UPDATE countries SET title_' . $lang_id . ' = ' . $title . ' WHERE id = ' . $country['id']);
		}
	}
}

function stage_translations_section_regions()
{
	global $sDB, $app_langs;
	
	$regions = $sDB->fetchAll('
		SELECT r.id, r.state, r.slug, c.iso
		FROM regions r
		INNER JOIN countries c ON c.id = r.country_id
	');
	
	/*
	foreach ( $regions as $region ) {
		if ( preg_match('/^region\-it\-/', $region['slug']) ) {
			$sDB->query('UPDATE regions SET slug = "' . preg_replace('/^region\-it\-/', '', $region['slug']) . '" WHERE id = ' . $region['id']);
		}
	}
	*/
	
	foreach ( $app_langs as $lang_id ) {
		require('data/regions_' . $lang_id . '.php');
		
		foreach ( $regions as $region ) {
			$type = is_null($region['state']) ? 'region' : 'state';
			$key = $type . '_' . $region['iso'] . '_' . str_replace('-', '_', $region['slug']);
			
			$title = isset($LNG[$key]) ? '"' . $LNG[$key] . '"' : '"' . $key . '"';
			
			$sDB->query('UPDATE regions SET title_' . $lang_id . ' = ' . $title . ' WHERE id = ' . $region['id']);
		}
	}
}

function stage_translations_section_cities()
{
	global $sDB, $app_langs;
	
	$cities = $sDB->fetchAll('
		SELECT ct.id, ct.slug, c.iso
		FROM cities ct
		INNER JOIN countries c ON c.id = ct.country_id
	');
	
	/*
	foreach ( $cities as $city ) {
		if ( preg_match('/^city\-us\-/', $city['slug']) ) {
			$sDB->query('UPDATE cities SET slug = "' . preg_replace('/^city\-us\-/', '', $city['slug']) . '" WHERE id = ' . $city['id']);
		}
	}
	*/
	
	foreach ( $app_langs as $lang_id ) {
		require('data/cities_' . $lang_id . '.php');
		
		foreach ( $cities as $city ) {
			$key = 'city_' . $city['iso'] . '_' . str_replace('-', '_', $city['slug']);
			$title = isset($LNG[$key]) ? '"' . $LNG[$key] . '"' : 'NULL'/* '"' . $key . '"' */;
			
			$sDB->query('UPDATE cities SET title_' . $lang_id . ' = ' . $title . ' WHERE id = ' . $city['id']);
		}
	}
}

/* reviews */
function stage_reviews()
{
	global $sDB, $pDB;

	$sql = 'SELECT * FROM esc_reviews WHERE application_id = 2';
	$res = $pDB->query($sql);

	while($row = mysql_fetch_object($res))
	{
		switch ($row->currency)
		{
			case 2:
				$curr = 3;
				break;
			case 3:
				$curr = 1;
				break;
			case 1:
			default:
				$curr = 2;
				break;
		}

		switch ($row->approved)
		{
			case 0:
				$status = 1;
				break;
			case 1:
				$status = 2;
				break;
			case -4:
			default:
				$status = 3;
				break;
		}
		
		$escort_id = $sDB->fetchOne('SELECT id FROM escorts WHERE __id = ' . $row->escort_id);
		if ( ! $escort_id ) {
			echo 'Invalid escort_id #' . $row->escort_id . ', review_id: ' . $row->id . "\n";
			continue;
		}

		$city_id = $sDB->fetchOne('SELECT id FROM cities WHERE internal_id = ' . $row->city);
		if ( ! $city_id ) {
			echo 'Invalid city_id #' . $row->city . ', review_id: ' . $row->id . "\n";
			continue;
		}

		try {
			$sDB->insert('reviews', array(
				'user_id' => $row->user_id,
				'escort_id' => $escort_id,
				'meeting_date' => $row->meeting_date,
				'duration' => $row->duration,
				'duration_unit' => $row->duration_unit,
				'price' => $row->price,
				'currency' => $curr,
				'looks_rating' => $row->looks_rating,
				'services_rating' => $row->services_rating,
				'services_comments' => $row->services_comments,
				'review' => $row->review,
				'fuckometer' => $row->fuckometer,
				'creation_date' => $row->creation_date,
				'last_modified' => $row->last_modified,
				'city' => $city_id,
				'is_fake_free' => $row->fake_free,
				'city_is_in_escort_profile' => $row->city_is_in_escort_profile,
				'meeting_place' => $row->meeting_place,
				's_kissing' => $row->s_kissing,
				's_blowjob' => $row->s_blowjob,
				's_cumshot' => $row->s_cumshot,
				's_69' => $row->s_69,
				's_anal' => $row->s_anal,
				's_sex' => $row->s_sex,
				's_attitude' => $row->s_attitude,
				's_conversation' => $row->s_conversation,
				's_breast' => $row->s_breast,
				's_multiple_sex' => $row->s_multiple_sex,
				's_availability' => $row->s_availability,
				's_photos' => $row->s_photos,
				't_user_info' => $row->t_user_info,
				't_meeting_date' => $row->t_meeting_date,
				't_meeting_place' => $row->t_meeting_place,
				't_comments' => $row->t_comments,
				'sms_status' => $row->sms_status,
				'sms_number' => $row->sms_number,
				'ip' => $row->ip,
				'is_suspicious' => $row->is_suspicious,
				'application_id' => 2,
				'status' => $status,
				'is_deleted' => 0,
				'deleted_date' => new Db_Expr('NULL'),
				'reason' => ''
			));
		}
		catch ( Exception $e ) {
			echo $e->getMessage() . "\r\n";
		}

		$c++;
	}
}
/**/

function stage_fixgotm()
{
	global $pDB, $sDB;

	$sDB->query('DELETE FROM girl_of_month');
	$gotms = $pDB->fetchAll('SELECT * FROM esc_girl_of_month WHERE application_id = ' . APP_ID);

	foreach ( $gotms as $gotm ) {
		$eid = $sDB->fetchOne('SELECT id FROM escorts WHERE __id = ' . $gotm['escort_id']);
		if ( ! $eid ) { echo "Invalid escort id #{$gotm['escort_id']}\n"; var_dump($gotm); continue; }
		$gotm['escort_id'] = $eid;
		$sDB->insert('girl_of_month', $gotm);
	}
	
	return true;
}


function _billing_validate_user_id($user_id)
{
	global $sDB;

	return (bool) $sDB->fetchOne('SELECT TRUE FROM users WHERE id = ' . $user_id);
}

function _billing_validate_escort_id($escort_id)
{
	global $sDB;

	return (bool) $sDB->fetchOne('SELECT TRUE FROM escorts WHERE id = ' . $escort_id);
}

function _billing_validate_admin_id($backend_user_id)
{
	global $sDB;

	return (bool) $sDB->fetchOne('SELECT TRUE FROM escorts WHERE id = ' . $backend_user_id);
}

function _billing_validate_package_id($package_id)
{
	global $sDB;

	return (bool) $sDB->fetchOne('SELECT TRUE FROM packages WHERE id = ' . $package_id);
}

function _billing_validate_product_id($product_id)
{
	global $sDB;

	return (bool) $sDB->fetchOne('SELECT TRUE FROM products WHERE id = ' . $product_id);
}

function _billing_is_transfer_western($tid)
{
	global $pDB;

	return $pDB->fetchRow('SELECT * FROM esc_western_transfers WHERE transfer_id = ' . $tid);
}

function _billing_is_transfer_cash($tid)
{
	global $pDB;

	return $pDB->fetchRow('SELECT * FROM esc_cash_payments WHERE transfer_id = ' . $tid);
}

function _billing_is_transfer_cc($tid)
{
	global $pDB;

	return $pDB->fetchRow('
		SELECT bd.* FROM esc_bank_details bd
		INNER JOIN esc_order_transfers ot ON ot.transfer_id = bd.transfer_id
		INNER JOIN esc_orders o ON o.id = ot.order_id
		WHERE bd.transfer_id = ' . $tid . ' AND o.payment_type_id = 3'
	);
}

function _billing_is_transfer_bank($tid)
{
	global $pDB;

	return $pDB->fetchRow('SELECT * FROM esc_bank_details WHERE transfer_id = ' . $tid);
}



/*function _billing_is_transfer_cash($id)
{
	global $pDB;

	return (bool) $pDB->fetchOne('SELECT TRUE FROM esc_cash_payments WHERE transfer_id = ' . $tid);
}*/

function stage_orders()
{
	global $sDB, $pDB, $app_langs;

	/*$sDB->query('
		DELETE o, op, opp, t, tc, tr, `to` FROM orders o
		LEFT JOIN order_packages op ON op.order_id = o.id
		LEFT JOIN order_package_products opp ON opp.order_package_id = op.id
		LEFT JOIN transfer_orders `to` ON to.order_id = o.id
		LEFT JOIN transfers t ON t.id = to.transfer_id
		LEFT JOIN transfer_confirmations tc ON tc.transfer_id = t.id
		LEFT JOIN transfer_rejections tr ON tr.transfer_id = t.id
	');*/

	extract(_get_geography());

	$orders = $pDB->fetchAll('
		SELECT o.*, e.gender FROM esc_orders o
		INNER JOIN esc_users u ON u.id = o.user_id
		INNER JOIN esc_escorts e ON e.user_id = u.id
		GROUP BY o.id
	');

	$count = $pDB->fetchOne('
		SELECT COUNT(DISTINCT(o.id)) FROM esc_orders o
		INNER JOIN esc_users u ON u.id = o.user_id
		INNER JOIN esc_escorts e ON e.user_id = u.id
	');

	$skipped = array('user' => 0, 'escort' => 0, 'admin' => 0, 'package' => 0, 'application' => 0);
	$c = 0;
	foreach ($orders as $order) {
		$c++;
		echo "[$c/$count] ";

		try {
			echo 'Order #' . $order['id'] . "...\n";

			if ( ! _billing_validate_user_id($order['user_id']) ) {
				echo "\tSkip reason: missing user #" . $order['user_id'] . "\n";
				$skipped['user']++;
				continue;
			}

			$backend_user_id = $order['admin_user_id'];
			if ( $id = $sDB->fetchOne('SELECT id FROM backend_users WHERE internal_id = ' . $backend_user_id) ) {
				$backend_user_id = $id;
			}

			if ( ! _billing_validate_admin_id($backend_user_id) ) {
				echo "\tSkip reason: missing sales user #" . $backend_user_id . "\n";
				$skipped['admin']++;
				continue;
			}

			$escort_id = $order['escort_id'];
			if ( $id = $sDB->fetchOne('SELECT id FROM escorts WHERE __id = ' . $escort_id) ) {
				$escort_id = $id;
			}

			if ( ! _billing_validate_escort_id($escort_id) ) {
				echo "\tSkip reason: missing escort #" . $order['escort_id'] . "\n";
				$skipped['escort']++;
				continue;
			}

			if ( ! _billing_validate_package_id($order['package_id']) ) {
				echo "\tSkip reason: missing package #" . $order['package_id'] . "\n";
				$skipped['package']++;
				continue;
			}

			if ( $order['application_id'] != APP_ID ) {
				echo "\tSkip reason: different application (#" . $order['application_id'] . ")\n";
				$skipped['application']++;
				continue;
			}

			$new_order = array(
				'id' => $order['id'],
				'user_id' => $order['user_id'],
				'backend_user_id' => $backend_user_id,
				'order_details' => $order['order_details'],
				'payment_date' => $order['payment_time'],
				'status' => $order['status'],
				'order_date' => $order['order_time'],
				'system_order_id' => (string) $order['system_order_number'],
				'price' => doubleval($order['price']),
				'activation_condition' => in_array($order['package_activated'], array(5, 8)) ? $order['package_activated'] : 5,
				'application_id' => APP_ID
			);

			try {
				$sDB->insert('orders', $new_order);
			}
			catch ( Exception $e ) {
				$new_order['system_order_id'] = null;
				$sDB->insert('orders', $new_order);
			}

			$status = $order['package_activated'];

			
			if ( in_array($status, array(5, 6, 7, 8)) || strtotime($order['package_activation_date']) > time() ) {
				$status = 1;
			}
			elseif ( in_array($status, array(1, 3, 4)) ) {
				$status = 4;
			}
			elseif ( $status == 2 ) {
				$status = 2;
			}

			$expiration_date = strtotime('+' . $order['package_period'] . ' days', strtotime($order['package_activation_date']));
			/*echo $expiration_date;
			die();*/
			if ( $expiration_date < time() ) {
				$status = 3;
			}

			$order_package = array(
				'order_id' => $order['id'],
				'escort_id' => $escort_id,
				'package_id' => $order['package_id'],
				'application_id' => $order['application_id'],
				'discount' => doubleval($order['discount']),
				'discount_fixed' => doubleval($order['eur_discount']),
				'surcharge' => doubleval($order['surcharge_fixed']),
				'period' => $order['package_period'],
				'status' => $status,
				'date_activated' => strtotime($order['package_activation_date']) < time() ? $order['package_activation_date'] : null,
				'activation_date' => strtotime($order['package_activation_date']) > time() ? $order['package_activation_date'] : null,
				'base_price' => doubleval($order['price']),
				'price' => doubleval($order['price']),
				'activation_type' => in_array($order['package_activated'], array(6, 7)) ? $order['package_activated'] : 9,
				'expiration_date' => date('Y-m-d', $expiration_date)
			);

			$sDB->insert('order_packages', $order_package);
			$order_package_id = $sDB->lastInsertId();

			// <editor-fold defaultstate="collapsed" desc="Package Additional Paramteres">
			$details = $order['order_details'];
			// I've seen some invalid data, where instead of newlines were \n symbols
			$details = str_replace('\n', "\n", $details);
			$details = explode("\n", $details);
			foreach ( $details as $i => $d ) {
				$d = trim($d);
				if ( ! strlen($d) || false === strpos($d, ':') ) {
					continue;
				}
				
				list($prod, $params) = explode(':', $d);
				switch ( $prod ) {
					case 'tour_premium_spot':
						$tour_ids = explode(',', $params);
						foreach ( $tour_ids as $tour_id ) {
							$tour_id = intval($tour_id);
							if ( $tour_id > 0 ) {
								try {
									$sDB->insert('premium_tours', array(
										'order_package_id' => $order_package_id,
										'tour_id' => $tour_id
									));
								}
								catch ( Exception $e ) {
									// Probably the exception occured because
									// there is already a record with same
									// order_package_id and tour_id, so just ignore
								}
							}
						}
						break;
					case 'city_premium_spot':
						$city_ids = explode(',', $params);
						foreach ( $city_ids as $city_id ) {
							$city_id = intval($city_id);
							if ( $tour_id > 0 ) {
								try {
									$sDB->insert('premium_escorts', array(
										'order_package_id' => $order_package_id,
										'city_id' => $city_id
									));
								}
								catch ( Exception $e ) {
									// Probably the exception occured because
									// there is already a record with same
									// order_package_id and city_id, so just ignore
								}
							}
						}
						break;
					default:
						echo 'Found an unknown product "' . $prod . '" with parameters "' . $params . '"' . "\n";
						break;
				}
				
			}
			// </editor-fold>


			foreach ( explode(',', $order['products']) as $product_id ) {
				$product_id = intval(trim($product_id));

				if ( 0 == $product_id || $product_id > 10 ) continue;

				$product = array(
					'order_package_id' => $order_package_id,
					'product_id' => $product_id,
					'is_optional' => 0,
					'price' => _orders_get_product_price($product_id, $order)
				);

				$sDB->insert('order_package_products', $product);
			}

			foreach ( explode(',', $order['addon_products']) as $product_id ) {
				$product_id = intval(trim($product_id));

				if ( 0 == $product_id || $product_id > 10 ) continue;

				$product = array(
					'order_package_id' => $order_package_id,
					'product_id' => $product_id,
					'is_optional' => 1,
					'price' => _orders_get_product_price($product_id, $order)
				);

				$sDB->insert('order_package_products', $product);
			}

			// Going to migrate transfers
			$transfers = $pDB->fetchAll('
				SELECT t.*
				FROM esc_transfers t
				INNER JOIN esc_order_transfers ot ON ot.transfer_id = t.id
				WHERE ot.order_id = ' . $order['id'] . '
			');

			// insert into transfers, transfer_rejections, transfer_confirmations, transfer_orders
			foreach ( $transfers as $transfer ) {
				$backend_user_id = $transfer['admin_user_id'];
				if ( $id = $sDB->fetchOne('SELECT id FROM backend_users WHERE internal_id = ' . $backend_user_id) ) {
					$backend_user_id = $id;
				}

				if ( ! _billing_validate_admin_id($backend_user_id) ) {
					echo "\tTRANSFERS: Skip reason: missing sales user #" . $backend_user_id . "\n";
					$skipped['admin']++;
					continue;
				}

				$new_transfer = array(
					'id' => $transfer['id'],
					'user_id' => $order['user_id'],
					'status' => $transfer['status'],
					'date_transfered' => $transfer['creation_date'],
					'comment' => $transfer['comment'],
					'backend_user_id' => $backend_user_id,
					'amount' => $order['price'],
					'application_id' => APP_ID
				);
				
				switch ( true ) {
					case ($more = _billing_is_transfer_western($transfer['id'])):
						$new_transfer = array_merge($new_transfer, array(
							'transfer_type_id' => 2,
							'first_name' => $more['first_name'],
							'last_name' => $more['last_name'],
							'city' => $more['city'],
							'country_id' => $countries[$more['country']],
							'mtcn' => $more['mtcn'],
							'pull_person_id' => $more['pull_person_id']
						));

						if ( 2 == $transfer['status'] ) {
							$confirm = array(
								'transfer_id' => $transfer['id'],
								'amount' => $more['eur'],
								'date' => $more['pull_date'],
								'backend_user_id' => $backend_user_id
							);
						}
						elseif ( 3 == $transfer['status'] ) {
							$reject = array(
								'transfer_id' => $transfer['id'],
								'date' => $transfer['reject_date'],
								'comment' => $transfer['reject_reason'],
								'backend_user_id' => $backend_user_id
							);
						}

						break;
					case ($more = _billing_is_transfer_cash($transfer['id'])):
						$new_transfer = array_merge($new_transfer, array(
							'transfer_type_id' => 4
						));

						if ( 2 == $transfer['status'] ) {
							$confirm = array(
								'transfer_id' => $transfer['id'],
								'amount' => $more['eur'],
								'date' => $more['recieve_date'],
								'backend_user_id' => $backend_user_id
							);
						}
						elseif ( 3 == $transfer['status'] ) {
							$reject = array(
								'transfer_id' => $transfer['id'],
								'date' => $transfer['reject_date'],
								'comment' => $transfer['reject_reason'],
								'backend_user_id' => $backend_user_id
							);
						}

						break;
					case ($more = _billing_is_transfer_cc($transfer['id'])):
						$new_transfer = array_merge($new_transfer, array(
							'transfer_type_id' => 3,
							'first_name' => $more['first_name'],
							'last_name' => $more['last_name'],
							'country_id' => $countries[$more['country']],
							'var_symbol' => $more['variable_symbol'],
							'transaction_id' => $more['transaction_id']
						));

						if ( 2 == $transfer['status'] ) {
							$confirm = array(
								'transfer_id' => $transfer['id'],
								'amount' => $more['eur'],
								'date' => $more['recieve_date'],
								'backend_user_id' => $backend_user_id
							);
						}
						elseif ( 3 == $transfer['status'] ) {
							$reject = array(
								'transfer_id' => $transfer['id'],
								'date' => $transfer['reject_date'],
								'comment' => $transfer['reject_reason'],
								'backend_user_id' => $backend_user_id
							);
						}

						break;
					case ($more = _billing_is_transfer_bank($transfer['id'])):
						$new_transfer = array_merge($new_transfer, array(
							'transfer_type_id' => 1,
							'first_name' => $more['first_name'],
							'last_name' => $more['last_name'],
							'country_id' => $countries[$more['country']],
							'var_symbol' => $more['variable_symbol'],
							'transaction_id' => $more['transaction_id']
						));

						if ( 2 == $transfer['status'] ) {
							$confirm = array(
								'transfer_id' => $transfer['id'],
								'amount' => $more['eur'],
								'date' => $more['recieve_date'],
								'backend_user_id' => $backend_user_id
							);
						}
						elseif ( 3 == $transfer['status'] ) {
							$reject = array(
								'transfer_id' => $transfer['id'],
								'date' => $transfer['reject_date'],
								'comment' => $transfer['reject_reason'],
								'backend_user_id' => $backend_user_id
							);
						}

						break;
					default:
						var_dump($transfer);
						echo "TRANSFERS: Skip reason: unknown transfer type\n";
						continue;
						break;
				}
			}

			if ( ! $sDB->fetchOne('SELECT TRUE FROM transfers WHERE id = ' . $new_transfer['id']) ) {
				$sDB->insert('transfers', $new_transfer);
				if ( isset($confirm) ) $sDB->insert('transfer_confirmations', $confirm);
				if ( isset($reject) ) $sDB->insert('transfer_rejections', $reject);
			}

			$sDB->insert('transfer_orders', array(
				'order_id' => $order['id'],
				'transfer_id' => $transfer['id']
			));

		}
		catch (Exception $e) {
//			var_dump($transfer);
//			var_dump($more);
//
//			var_dump($new_transfer);
//			var_dump($confirm);
//			var_dump($reject);
			echo $e->getMessage() . "\n";
			echo $e->__toString() . "\n";
			continue;
		}
	}

	// Fix Expiration Dates
	$sDB->query('UPDATE order_packages SET expiration_date = DATE_ADD(activation_date, INTERVAL period DAY)');

	print_r($skipped);
	echo "\n\n";
}


function _orders_get_product_price($product_id, $order)
{
	global $sDB;

	$price = $sDB->fetchOne('
		SELECT pp.price FROM product_prices pp
		WHERE
			pp.product_id = ' . $product_id . ' AND
			pp.user_type = 1 AND pp.gender = ' . $order['gender'] . ' AND
			application_id = ' . $order['application_id'] . '
	');

	return floatval($price);
}










function _photos_get_path($photo)
{
	$catalog = $photo['escort_id'];
	
	$parts = array();
	if ( strlen($catalog) > 2 ) {
		$parts[] = substr($catalog, 0, 2);
		$parts[] = substr($catalog, 2);
	}
	else {
		$parts[] = '_';
		$parts[] = $catalog;
	}
	
	if ( 3 == $photo['type'] ) {
		$parts[] = 'private';
	}
	
	$catalog = implode('/', $parts);
	
	$path = 'pics/' . APP_HOST . '/' . $catalog . '/' . $photo['hash'] . '.' . $photo['ext'];
	
	return $path;
}

function _photos_make_path($path)
{
	$path = explode('/', $path);
	$path = array_slice($path, 0, count($path));
	
	for ( $i = 1; $i < count($path); $i++ ) {
		if ( ! is_dir($p = implode('/', array_slice($path, 0, $i))) ) {
			mkdir($p);
		}
	}
}








define('MODE_CLEAN_ONLY', 1);
$mode_map = array(
	null => null,
	'clean_only' => MODE_CLEAN_ONLY
);

$stage = isset($_REQUEST['stage']) ? $_REQUEST['stage'] : null;
$mode = isset($_REQUEST['mode']) ? $_REQUEST['mode'] : null;

if ( is_null($stage) ) {
	$stage = @$_SERVER['argv'][1];
}

if ( is_null($mode) ) {
	$mode = @$_SERVER['argv'][2];
}

$mode = $mode_map[$mode];

$args = array_slice($_SERVER['argv'], 1);

$func = 'stage_' . $stage;

if ( ! is_callable($func) ) {
	die('Invalid migration stage');
}

echo '---> ' . ucfirst($stage) . ' stage has been started' . "\r\n\r\n";

call_user_func_array($func, array($mode) + $args);
