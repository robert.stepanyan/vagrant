<?php

class Zend_View_Helper_GetLink
{
	protected static $_params;
	
	public function getLink($type = '', array $args = array(), $clean = false, $no_args = false)
	{
		$lang_id = Cubix_I18n::getLang();

		$link = '/';
		if ( $lang_id != Cubix_Application::getDefaultLang() ) {
			$link .= $lang_id . '/';
		}
		
		if ( isset($args['state']) && 7 != Cubix_Application::getId() ) {
			$args['region'] = $args['state'];
			unset($args['state']);
		}
		
		switch ( $type ) {
			case 'forum':
				$link = 'http://forum.escortforum.net';
				break;
			case 'chat':
				$link = 'http://chat.escort-annonce.com/';
				break;
			case 'cams':
				$link = 'http://escortforumit.streamray.com/';
				break;
			case 'film':
				$link = 'http://video.escortforum.net/';
				break;
			case 'search':
				$link .= 'search';
				break;
			case 'escorts-list':
				$link .= 'escorts';
			break;
			case 'glossary':
				$link .= 'glossary';
				break;
			case 'base-city':
				$section = isset($args['section']) ? $args['section'] . '/' : '';
				if ( strlen($section) ) unset($args['section']);

				$link .= 'escorts/' . $section . 'city_' . $args['city'];
				unset($args['city']);
			break;
			case 'escorts':
				if ( empty(self::$_params) ) {
					$request = Zend_Controller_Front::getInstance()->getRequest();
					$req = trim($request->getParam('req', ''), '/');
					$req = explode('/', $req);
					
					$params = array();
					
					foreach ($req as $r)  {
						$param = explode('_', $r);
						if ( count($param) < 2 ) {
							$params[] = $r;
							continue;
						}
						
						$param_name = reset($param);
						array_shift($param);
						$params[$param_name] = implode('_', $param);
					}
					
					self::$_params = $params;
				}
				
				if ( $clean ) {
					$params = array();
				}
				else {
					$params = self::$_params;
				}
				
				foreach ( $args as $key => $value ) {
					if ( is_null($value) ) {
						unset($params[$key]);
						continue;
					}
					
					$params[$key] = $value;
				}
				
				$link .= 'escorts/';
				foreach ( $params as $param => $value ) {
					if ( ! strlen($param) || ! strlen($value)) continue;
					if ( is_int($param) ) {
						//if ( $value == 'tours' ) $value = 'citytours';
						$link .= $value . '/';
					}
					elseif ( strlen($param) ) {
						if ( $param == 'page' ) {
							if ( $value - 1 > 0 ) {
								$link .= ($value - 1) . '/';
							}
						}
						else {
							$link .= $param . '_' . $value . '/';
						}
					}
				}
				
				$link = rtrim($link, '/');
				
				$args = array();
			break;

			case 'seo_ef_link';

                if ( $clean ) {
                    $params = array();
                }
                else {
                    $params = self::$_params;
                }

                foreach ( $args as $key => $value ) {
                    if ( is_null($value) ) {
                        unset($params[$key]);
                        continue;
                    }

                    $params[$key] = $value;
                }

                $link = 'https://www.escortforumit.xxx/escorts/';

                foreach ( $params as $param => $value ) {
                    if ( ! strlen($param) || ! strlen($value)) continue;
                    if ( is_int($param) ) {
                        //if ( $value == 'tours' ) $value = 'citytours';
                        $link .= $value . '/';
                    }
                    elseif ( strlen($param) ) {
                        if ( $param == 'page' ) {
                            if ( $value - 1 > 0 ) {
                                $link .= ($value - 1) . '/';
                            }
                        }
                        else {
                            $link .= $param . '_' . $value . '/';
                        }
                    }
                }

                $link = rtrim($link, '/');

                $args = array();
			break;

			case 'gotm':
				$link .= 'girl-of-month';

				if ( isset($args['history']) && $args['history'] ) {
					$link .= '/history';
				}
				
				if ( isset($args['page']) ) {
					if ( $args['page'] != 1 ) {
						$link .= '/page_' . $args['page'];
						//$link .= '/page_' . $args['page'];
					}
					
					unset($args['page']);
				}
				break;
			case 'blog':
				$link .= 'blog';
				break;
			case 'blog-archive':
				$link .= 'blog/' . $args['month'] . '.' . $args['year'];

				unset($args['month']);
				unset($args['year']);
				break;
			case 'blog-post':
				$link .= 'blog/' . $args['slug'] . '-' . $args['id'];
				unset($args['slug']);
				unset($args['id']);
				break;
			case 'profile':
				$link .= 'accompagnatrici/' . $args['showname'] . '-' . $args['escort_id'];
				unset($args['showname']);
				unset($args['escort_id']);
			break;
			case 'forgot':
				$link .= 'private/forgot';
			break;
			case 'ajax-forgot':
				$link .= 'private/ajax-forgot';
				break;
			case 'forgot-two':
				$link .= 'private/forgot-two';
			break;
			case 'signup':
				$link .= 'private/signup-';
				
				if ( ! isset($args['type']) ) {
					$args['type'] = 'member';
				}
				
				$link .= $args['type'];
			break;
			case 'signin':
				$link .= 'private/signin';
			break;
			case 'signout':
				$link .= 'private/signout';
			break;
			case 'planner':
				$link .= 'planner';
			break;
			case 'cityalerts':
				$link .= 'city-alerts';
			break;
			case 'private':
				$link .= 'private';
			break;
			case 'membership-type':
				$link .= 'private/membership-type';
			break;
			case 'feedback':
				$link .= 'feedback';
			break;
			case 'contact':
				$link .= 'contact';
            break;
            case 'report':
				$link .= 'compliance';
            break;
			case 'ajax-contact':
				$link .= 'ajax-contact';
			break;

			case 'add-review-comment':
				$link .= 'private-v2/add-review-comment';
			break;
			case 'captcha':
				$link .= 'captcha?' . rand();
				//$link = '/img/sample_captcha.gif?' . rand();
			break;
			case 'viewed-escorts':
				$link .= 'escorts/viewed-escorts';
			break;
			case '100p-verify':
				$link .= 'private/verify';
			break;
			case '100p-verify-webcam':
				$link .= 'private/verify/webcam';
			break;
			case '100p-verify-idcard':
				$link .= 'private/verify/idcard';
			break;
			case 'agency':
				$link .= 'agency/' . $args['slug'] . '-' . $args['id'];
				unset($args['slug']);
				unset($args['id']);
			break;
			
			case 'favorites':
				$link .= 'private/favorites';
			break;
			case 'add-to-favorites':
				$link .= 'private/add-to-favorites';
			break;
			case 'remove-from-favorites':
				$link .= 'private/remove-from-favorites';
			break;

            case 'link-ef':
                $link = 'https://www.escortforumit.xxx/';
                break;
			
			// Private Area
            case 'simple-profile':
				$link .= 'private-v2/profile/simple';
				break;
			case 'fill-contact-info':
				$link .= 'private-v2/ajax-fill-contact-info';
				break;
			case 'edit-profile':
				$link .= 'private/profile';
			break;
			case 'edit-agency-profile':
				$link .= 'private/agency-profile';
			break;
			case 'profile-data':
				$link .= 'private/profile-data';
			break;
			case 'edit-photos':
				$link .= 'private/photos';
			break;
			case 'edit-rates':
				$link .= 'private/rates';
			break;
			case 'edit-escorts':
				$link .= 'private/escorts';
			break;
			case 'delete-escort':
				$link .= 'private/delete-escort';
			break;
			case 'edit-tours':
				$link .= 'private/tours';
			break;
			case 'change-passwd':
				$link .= 'private/change-password';
			break;
			
			case 'search':
				$link .= 'search';
			break;

			case 'late-night-girls':
				$link .= 'escorts/late-night-girls';
			break;
			
			case 'terms':
				$link .= 'page/terms-and-conditions';
			break;
			case 'privacy':
				$link .= 'page/privacy-policy';
			break;
			case 'webcam-lm':
				$link .= 'page/webcam-verification-learn-more';
			break;
			case 'passport-lm':
				$link .= 'page/passport-verification-learn-more';
			break;
			
			case 'external-link':
				$link = '/go?' . $args['link'];
				unset($args['link']);
			break;
			// V2 Private Area
			case 'private-v2':
				$link .= 'private-v2';
				break;
			case 'private-v2-settings':
				$link .= 'private-v2/settings';
				break;
			case 'private-v2-statistics':
				$link .= 'private-v2/statistics';
				break;
			case 'private-v2-happy-hour':
				$link .= 'private-v2/happy-hour';
				break;
			case 'private-v2-client-blacklist':
				$link .= 'private-v2/client-blacklist';
				break;
			case 'private-v2-add-client-to-blacklist':
				$link .= 'private-v2/add-client-to-blacklist';
				break;
			case 'private-v2-profile':
				$link .= 'private-v2/profile';
				break;
			case 'private-v2-photos':
				$link .= 'private-v2/photos';
				break;
			case 'private-v2-plain-photos':
				$link .= 'private-v2/plain-photos';
				break;
			case 'private-v2-tours':
				$link .= 'private-v2/tours';
				break;
			case 'private-v2-ajax-tours':
				$link .= 'private-v2/ajax-tours';
				break;
			case 'private-v2-ajax-tours-add':
				$link .= 'private-v2/ajax-tours-add';
				break;
			case 'private-v2-ajax-tours-remove':
				$link .= 'private-v2/ajax-tours-remove';
				break;
			case 'private-v2-verify':
				$link .= 'private-v2/verify';
				break;
			case 'private-v2-faq':
				$link .= 'private-v2/faq';
				break;
			case 'private-v2-premium':
				$link .= 'private-v2/premium';
				break;
			case 'private-v2-support':
				$link .= 'support';
				break;
			case 'private-v2-get-urgent-message':
				$link .= 'private-v2/get-urgent-message';
				break;
			case 'private-v2-set-urgent-message':
				$link .= 'private-v2/set-urgent-message';
				break;
			case 'private-v2-get-rejected-verification':
				$link .= 'private-v2/get-rejected-verification';
				break;
			case 'private-v2-set-rejected-verification':
				$link .= 'private-v2/set-rejected-verification';
				break;
			case 'alerts':
				$link .= 'private-v2/alerts';
				break;
			case 'private-v2-agency-profile':
				$link .= 'private-v2/agency-profile';
				break;
			case 'private-v2-member-profile':
				$link .= 'private-v2/member-profile';
				break;
			case 'private-v2-upgrade-premium':
				$link .= 'private-v2/upgrade';
				break;
			case 'ticket-open':
				$link .= 'support/ticket';
				break;
			case 'private-v2-escorts':
				$link .= 'private-v2/escorts';
				break;
            case 'private-v2-escorts-delete':
				$link .= 'private-v2/profile-delete';
				break;
			case 'reviews':
				$link .= 'recensioni';
				break;
			case 'escort-reviews':
				$link .= 'recensioni/' . $args['showname'] . '-' . $args['escort_id'];
				unset ($args['showname']);
				unset ($args['escort_id']);
				break;
			case 'member-reviews':
				$link .= 'recensioni/member/' . $args['username'];
				unset ($args['username']);
				break;
			case 'escorts-reviews':
				$link .= 'recensioni/escorts';
				break;
			case 'agencies-reviews':
				$link .= 'recensioni/agencies';
				break;
			case 'top-reviewers':
				$link .= 'recensioni/top-reviewers';
				break;
			case 'top-ladies':
				$link .= 'recensioni/top-ladies';
				break;
			case 'voting-widget':
				$link .= 'escort/' . $args['showname'] . '-' . $args['escort_id'] . '/vote';
				unset($args['showname']);
				unset($args['escort_id']);
				break;
			case 'private-v2-reviews':
				$link .= 'private-v2/reviews';
				break;
			case 'private-v2-escort-reviews':
				$link .= 'private-v2/escort-reviews';
				break;
			case 'private-v2-escorts-reviews':
				$link .= 'private-v2/escorts-reviews';
				break;
			case 'private-v2-add-review':
				$link .= 'private-v2/add-review';
				break;
			case 'private-v2-add-reply';
				$link .= 'private-v2/add-reply';
				break;
			case 'private-v2-ajax-escort-comments';
				$link .= 'private-v2/ajax-escort-comments';
				break;
			case 'reviews-search':
				$link .= 'recensioni/search';
				break;
            case 'comments':
				$link .= 'comments';
				break;
			case 'signup-as-vip-member':
				$link .= 'private/signup-vip-member?type=vip-member';
				break;
			case 'members-choice':
				$link .= 'members-choice';
				break;
			case 'member-info':
				$link .= 'member/'.$args['username'];
				unset($args['username']);
				break;
			case 'member-comments':
				$link .= 'members/get-member-comments';
				break;
			case 'tell-friend':
				$link .= 'escorts/ajax-tell-friend';
				break;
			case 'report-problem':
				$link .= 'escorts/ajax-report-problem';
				break;
			case 'get-filter':
				$link .= 'escorts/get-filter';
				break;
			case 'get-filter-v2':
				$link .= 'escorts/get-filter-v2';
				break;
			case 'ob-buy-premium';
				$link = 'https://www.escortitalia.com/private-v2';
				break;

		}

		if ( ! $no_args ) {
			if ( count($args) ) {
				$link .= '?';
				$params = array();
				foreach ( $args as $arg => $value ) {
					if ( ! is_array($value) ) {
						$params[] = $arg . '=' . urlencode($value);
					}
					else {
						foreach ( $value as $v ) {
							$params[] = $arg . '[]=' . $v;
						}
					}
				}

				$link .= implode('&', $params);
			}
		}
		
		return $link;
	}
}
