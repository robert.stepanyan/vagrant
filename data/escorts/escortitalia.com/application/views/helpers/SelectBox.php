<?php

class Zend_View_Helper_SelectBox
{
	public function selectBox($name, $id, array $data, $class = '', $selected = NULL, $empty = FALSE)
	{
		$html = '<select id="' . $id . '" name="' . $name . '" class="' . $class . '">';

		if ($empty !== FALSE) $html .= '<option value="">- ' . $empty . ' -&nbsp;&nbsp;</option>';

		foreach ($data as $k => $v) {
			$html .= '<option value="' . $k . '"' . (($k == $selected) && ! is_null($selected) ? ' selected="selected"' : '') . '>' . $v . '&nbsp;&nbsp;</option>';
		}

		$html .= '</select>';

		return $html;
	}
}
