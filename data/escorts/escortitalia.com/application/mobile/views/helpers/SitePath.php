<?php

class Zend_View_Helper_SitePath extends Zend_View_Helper_Abstract
{
	protected static $_path;
	
	public function sitePath()
	{
		if ( empty(self::$_path) ) {
			self::$_path = new ArrayObject(array(
			));
		}
		
		return $this;
	}

	public function append($title, $url)
	{
		self::$_path[] = array('title' => $title, 'url' => $url);
		
		return $this;
	}

	public function prepend($title, $url)
	{
		$value = array('title' => $title, 'url' => $url);
		$values = self::$_path->getArrayCopy();
		array_unshift($values, $value);
		self::$_path->exchangeArray($values);
		//if ( isset(self::$_path[1]) ) self::$_path[] = self::$_path[1];
		//self::$_path[1] = array('title' => $title, 'url' => $url);
		
		return $this;
	}
	
	public function offset($index, $title, $url)
	{
		/*if ( isset(self::$_path[1]) ) self::$_path[] = self::$_path[1];
		self::$_path[1] = array('title' => $title, 'url' => $url);*/
		$value = array('title' => $title, 'url' => $url);
		self::$_path->exchangeArray(array($value));
		
		return $this;
	}
	
	public function get()
	{
		$arr = self::$_path->getArrayCopy();
		//array_unshift($arr, array('title' => Cubix_Application::getById()->title, 'url' => $this->view->getLink()));

        //print_r($arr); die();

		return $arr;
	}

	public function __toString()
	{
//		$this->prepend(__('home'), '/');
		$this->prepend('<img class="home-icon-breadcr" src="/m/i/breadcrumb-home.png" alt="home-link" />', '/');
		return $this->view->render('mobile-path.phtml');
	}
}
