<?php

class Mobile_EscortsController extends Zend_Controller_Action
{
	public function init()
	{
		$this->_helper->layout->setLayout('mobile-list-index');

        $_SESSION['request_url'] = $_SERVER['REQUEST_URI'];
		//die('Mobile version is temporary unavailable');
	}

    public function indexAction()
	{

        if ( $this->getRequest()->isXmlHttpRequest() ) {
            if ( $this->getRequest()->isPost() ) {
                $this->_helper->layout()->disableLayout();
            }
        } else {
            $this->_helper->layout->setLayout('mobile-index');
        }


        $req = $this->_getParam('req');

        $config = Zend_Registry::get('mobile_config');
        

        if ( preg_match('#/([0-9]+)$#', $req, $a) ) {
			$a[1] = intval($a[1]);
			$req = preg_replace('#/[0-9]+$#', '/page_' . $a[1], $req);
		}

    
        
        $req = explode('/', $req);


        $params = array(
            'sort' => 'random',
            'filter' => array(array('field' => 'girls', 'value' => array())),
            'page' => 1
        );

		$static_page_key = 'main';

        foreach ( $req as $r ) {
			if ( ! strlen($r) ) continue;
			$param = explode('_', $r);
			if ( count($param) < 2 ) {
				switch( $r )
				{
					case 'nuove':
						//$params['filter'][] = array('field' => 'new_arrivals', 'value' => array());
						$static_page_key = 'nuove';
						$param = array('sort', 'newest');
					break;
					case 'independantes':
						$static_page_key = 'independantes';
						$params['filter'][] = array('field' => 'independantes', 'value' => array());
					continue;
					case 'regular':
						$static_page_key = 'regular';
						$params['filter'][] = array('field' => 'regular', 'value' => array());
					continue;
					case 'agence':
						$static_page_key = 'agence';
						$params['filter'][] = array('field' => 'agence', 'value' => array());
					continue;
					case 'boys':
						$static_page_key = 'boys';
						$params['filter'][0] = array('field' => 'boys', 'value' => array());
					continue;
					case 'trans':
						$static_page_key = 'trans';
						$params['filter'][0] = array('field' => 'trans', 'value' => array());
					continue;
					case 'citytours':
						$static_page_key = 'citytours';
						$this->view->is_tour = $is_tour = true;
						$params['filter'][] = array('field' => 'tours', 'value' => array());
					continue;
					case 'upcomingtours':
						$static_page_key = 'upcomingtours';
						$this->view->is_tour = $is_tour = true;
						$this->view->is_upcomingtour = true;
						$upcoming_tours = true;
						$params['filter'][] = array('field' => 'upcomingtours', 'value' => array());
					continue;
/*					case 'blank.html':
						$this->_forward('blank-html', 'redirect');
					return;*/
					default:
						$this->_response->setRawHeader('HTTP/1.1 404 Not Found');
						$this->_forward('error', 'error', null, array('error_msg' => '404 Page Not Found. Please try the following links instead.'));
						return;
				}
			}

            //            if( $this->_getParam('sort') == 'newest' ){
//                die('5555');
            //            }

			$param_name = $param[0];
			array_shift( $param );

            //print_r( $param ); die();

			switch ( $param_name ) {
				case 'filter':
					$has_filter = true;
					/* >>> For nested menu */
					$selected_item = $menus['filter']->getByValue(implode('_', $param));
					if ( ! is_null($selected_item) ) {
						$menus['filter']->setSelected($selected_item);
					}
					/* <<< */

					$field = reset($param);
					array_shift($param);

					$value = array_slice($param, 0, 2);

					$params['filter'][] = array('field' => $field, 'value' => $value , 'main' => TRUE);
				break;
				case 'page':
					$page = intval(reset($param));
                    
					if ( $page < 1 ) {
						$page = 1;
					}

					$params['page'] = $page;
				break;
				case 'sort':
					$params['sort'] = reset($param);

					$selected_item = $menus['sort']->getByValue($params['sort']);
					if ( ! is_null($selected_item) ) {
						$menus['sort']->setSelected($selected_item);
					}

					$has_filter = true;
				break;
				case 'region':
				case 'state':
					$params['country'] = $param[0];
					$params['region'] = $param[1];
					$params['filter'][] = array('field' => 'region', 'value' => $param[1]);
				break;
				case 'city':
				case 'zone':
					$params[$param_name] = $param[1];
					$params['filter'][] = array('field' => $param_name, 'value' => array($param[1]));
				break;
				case 'name':
					$has_filter = true;
					$params['filter'][] = array('field' => 'name', 'value' => array('%' . $param[0] . '%'));
				break;
				default:
					if ( ! in_array($param_name, array('nuove', 'independantes', 'regular', 'agence', 'boys', 'trans', 'citytours', 'upcomingtours')) ) {
						$this->_response->setRawHeader('HTTP/1.1 404 Not Found');
						$this->_forward('error','error', null, array('error_msg' => '404 Page Not Found. Please try the following links instead.'));
						return;
					}
			}
        }



		$this->view->static_page_key = $static_page_key;
		
            $filter_params = array(
                'order' => 'e.date_registered DESC',
                'limit' => array('page' => 1),
                'filter' => array()
            );


            $filter_map = array(
                'verified' => 'e.verified_status = 2',
                'french' => 'e.nationality_id = 15',

                'age' => 'e.age < ? AND e.age > ?',
                'ethnic' => 'e.ethnicity = ?',
                'height' => 'e.height < ? AND e.height > ?',
                'weight' => 'e.weight < ? AND e.weight > ?',
                'cup-size' => 'e.cup_size = ?',
                'hair-color' => 'e.hair_color = ?',
                'hair-length' => 'e.hair_length = ?',
                'eye-color' => 'e.eye_color = ?',
                'dress-size' => 'e.dress_size = ?',
                'shoe-size' => 'e.shoe_size = ?',
                'available-for-incall' => 'e.incall_type IS NOT NULL',
                'available-for-outcall' => 'e.outcall_type IS NOT NULL',
                'service-for' => 'FIND_IN_SET(?, e.sex_availability)',
                'smoker' => 'e.is_smoking = ?',
                'language' => 'FIND_IN_SET(?, e.languages)',
                'now-open' => 'e.is_now_open',
                'region' => 'r.slug = ?',
                'city' => 'ct.slug = ?',
                'cityzone' => 'c.id = ?',
                'zone' => 'cz.slug = ?',
                'name' => 'e.showname LIKE ?',

                'new_arrivals' => 'e.gender = ' . GENDER_FEMALE,
                'independantes' => 'eic.is_agency = 0 AND eic.gender = ' . GENDER_FEMALE,
                'agence' => 'eic.is_agency = 1 AND eic.gender = ' . GENDER_FEMALE,
                'boys' => 'eic.gender = ' . GENDER_MALE,
                'trans' => 'eic.gender = ' . GENDER_TRANS,
                'girls' => 'eic.gender = ' . GENDER_FEMALE,

                'tours' => 'eic.is_tour = 1',
                'upcomingtours' => 'eic.is_upcoming = 1'
            );


            foreach ( $params['filter'] as $i => $filter ) {

                if ( ! isset($filter_map[$filter['field']]) ) continue;

                $value = $filter['value'];

                if ( isset($filter['main']) ) {
                    if ( isset($selected_filter->internal_value) ) {
                        $value = $selected_filter->internal_value;
                    }
                    elseif ( ! is_null($item = $menus['filter']->getByValue($filter['field'] . ( (isset($value[0]) && $value[0]) ? '_' . $value[0] : '')) ) ) {
                        $value = $item->internal_value;
                    }

                }

                $filter_params['filter'][$filter_map[$filter['field']]] = $value;
            } 

        $page = intval($params['page']);


        if ( $page == 0 ) {
            $page = 1;
        }

        $filter_params['limit']['page'] = $page;
        $count = 0;

        if(isset($params['city'])){
            $this->view->city = ucfirst($params['city']);
        }

        $this->view->params = $params;

        $per_page = 14 /* For mobile version, default $e_config['perPage']*/;

        $segmented = $this->view->segmented = $this->_getParam('segmented', 0);

        if($this->getRequest()->getMethod() == 'GET'){
            if( isset($_COOKIE['page']) ){
                $config['escorts']['perPage'] = $_COOKIE['page'] * $per_page;
                $filter_params['limit']['page'] = 1;
            }
        } else {
//            $per_page = $filter_params['limit']['page'] * $per_page;
//            $filter_params['limit']['page'] = 1;
        }


        /****************************/
        if( $this->_getParam('sort') ){
            switch( $this->_getParam('sort') ){
                case 'newest':
                    $params['sort'] = 'newest'; break;
                case 'tours':
                    $filter_params['filter']['tours'] = array('field' => 'tours', 'value' => array());
                case 'random':
                    $params['sort'] = 'random'; break;
            }
        }
        /****************************/

		$escorts = Model_Escort_List::getFiltered($filter_params['filter'], $params['sort'], $filter_params['limit']['page'], $config['escorts']['perPage'], $count, true);

        $this->view->escorts = $escorts;
        $this->view->page = $page;
        $this->view->count = $count;

        $this->_helper->viewRenderer->setScriptAction("escort-list");

	}


	public function reviewsAction() {
		$lng = Cubix_I18n::getLang();

		$request = $this->_request;
		if ( $request->ajax ) {
			$this->view->layout()->disableLayout();
			$this->view->ajax = true;
		}
		$filter = array();

		if ( isset($request->city_id) && $request->city_id ) {
			$filter['city_id'] = intval($request->city_id);
			$this->view->city_id = intval($request->city_id);
		}

		if ( isset($request->showname) && $request->showname ) {
			$filter['showname'] = $request->showname;
			$this->view->showname = $request->showname;
		}

		if ( isset($request->member_name) && $request->member_name ) {
			$filter['member_name'] = $request->member_name;
			$this->view->member_name = $request->member_name;
		}

		$ord_field_v = 'creation_date';
		$ord_field = 'r.creation_date';
		$ord_dir_v = 'desc';
		$ord_dir = 'DESC';

		if ( count($filter) > 0 )
			$arg_filter = $filter;
		else
			$arg_filter = '-999';
		$config = Zend_Registry::get('reviews_config');

		if ( isset($request->page) && intval($request->page) > 0 ) {
			$page = intval($request->page);
		} else { $page = 1; }

		is_array( $arg_filter ) ? $filter_str = implode( '.', $arg_filter ) : $filter_str = $arg_filter;

		$ret_revs = Model_Reviews::getReviews( $page, $config['perPage'], $arg_filter, $ord_field, $ord_dir );
		$cities = Model_Reviews::getReviewsCities();


		$ret = array( $ret_revs, $cities );

		list( $ret_revs, $cities ) = $ret;
		list( $items, $count ) = $ret_revs;
		$this->view->items = $items;
		$this->view->count = $count;
		$this->view->cities = $cities;
		$this->view->page = $page;
		$this->view->filter = $filter;

		$this->view->menuReviews = 1;
	}

    public function ajaxGetTopSearchAction()
    {
        $count = 0;

        $filter_params['filter'] = array('e.showname LIKE ?' => $this->_request->getParam('top_search_name', null) . '%');

        $all_escorts = Model_Escort_List::getFiltered( $filter_params['filter'], 'alpha', 1, 5, $count, false, 'regular_list' );

        $_all_escorts = array();
        foreach ( $all_escorts as $k => $escort ) {
            //print_r($escort); die;
            $_all_escorts[$k] = array(
                'id' => $escort->id,
                'type' => 'escort',
                'group' => $this->view->t('escorts'),
                'slug' => $escort->showname,
                'city' => $escort->city,
                'name' => $escort->showname
            );
        }

        $all_cities = Model_Statistics::getMobCities(null, null, null, null, null, null, null, $this->_request->getParam('top_search_name', null));

        $fn_order_title = create_function('$a, $b', '
			if ( is_object($a) && is_object($b) ) {
				return strnatcmp($a->city_title, $b->city_title);
			}
			elseif ( is_array($a) && is_array($b) ) {
				return strnatcmp($a["city_title"], $b["city_title"]);
			}
			else return 0;
		');

        usort($all_cities, $fn_order_title);
        foreach ( $all_cities as $k => $city ) {
            $all_cities[$k] = array(
                'type' => 'city',
                'group' => $this->view->t('cities'),
                'country_iso' => Cubix_Application::getById()->iso,
                'slug' => $city->city_slug,
                'id' => $city->city_id,
                'name' => $city->city_title . ' (' . $city->escort_count . ')' . ' (' . $city->country_title . ')'
            );
        }

        $merged = array_merge($_all_escorts, $all_cities);

        die(json_encode($merged));
    }

    public function getCaptchaStatusAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $request = $this->_request;
        $hash = $request->hash;

        $session = new Zend_Session_Namespace('captcha');
        $orig_captcha = $session->captcha;

        //die($hash);

        if ( strtolower( $hash ) != $orig_captcha ) {
            echo 'false';
        } else {
            echo 'true';
        }

    }

    public function contactFormAction()
    {
        $request = $this->_request;

        $this->view->id = $request->escort_id;
        $this->view->type = $request->type;
        $this->view->layout()->disableLayout();
    }

}
