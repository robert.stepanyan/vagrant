<?php

class StaticPageController extends Zend_Controller_Action
{
	public function init()
	{
        $this->view->layout()->setLayout('static-page');
	}
	
	public function showAction()
	{
		$slug = $this->_request->page_slug;
		$lang = Cubix_I18n::getLang();
		$app_id = Cubix_Application::getId();
		
		$model = new Model_StaticPage();
		$this->view->page = $model->getBySlug($slug, $app_id, $lang);

		$this->_attachSeoAttributes('page-' . $slug);
    }

    /**
     * @return void
     * @param $key
     */
    private function _attachSeoAttributes($key)
    {
        $seo = $this->view->seo($key, null, array());

        if ($seo) {
            if (strlen($seo->title)) {
                $this->view->headTitle($seo->title, 'SET');
            }

            if (strlen($seo->keywords)) {
                $this->view->headMeta()->setName('keywords', $seo->keywords);
            }

            if (strlen($seo->description)) {
                $this->view->headMeta()->setName('description', $seo->description);
            }
        }
    }
}
