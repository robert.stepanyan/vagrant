<?php

class OnlineBillingController extends Zend_Controller_Action
{
	const PACKAGE_STATUS_PENDING  = 1;
	const PACKAGE_STATUS_ACTIVE   = 2;
	

	public function init()
	{	
		$this->view->layout()->setLayout('private-v2');
		$this->client = new Cubix_Api_XmlRpc_Client();
		
		$anonym = array('epg-response', 'mmg-response');
		
		$this->user = Model_Users::getCurrent();
		
		if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}
		
		if ( ! $this->user->isAgency() && ! $this->user->isEscort() ) {
			$this->_redirect($this->view->getLink('private-v2'));
		}
	}
	
	public function buyPremiumAction()
	{
		if ( ! $this->user->isEscort() ) {
			$this->_redirect($this->view->getLink('private-v2'));	
		}

		$escort_id = $this->user->escort_data['escort_id'];
		if ( $this->_request->isPost() ) {

			$package_id = intval($this->_request->package);
			$payment_type = $this->_request->payment_type;
			
			if( ! in_array( $payment_type, array('epg', 'mmg') ) ) { echo  'Not supported payment type';die; }
			
			$is_pseudo_escort = $this->client->call('OnlineBillingV2.isPseudoEscort', array($escort_id));

			$m_escorts = new Model_EscortsV2();
			$gender = $m_escorts->getById($escort_id)->gender;

			$user_type = USER_TYPE_SINGLE_GIRL;

			if ( $is_pseudo_escort ) {
				$user_type = USER_TYPE_AGENCY;
			}

			$hash  = base_convert(time(), 10, 36);
			
			$data[] = array(
				'user_id' => $this->user->id,
				'escort_id' => $escort_id,
				'agency_id' => null,
				'package_id' => $package_id,
				'hash' => $hash,
				'data' => serialize(array())
			);

			$amount = $this->client->call('OnlineBillingV2.addToShoppingCart', array($data, $this->user->id, $user_type, $hash));
			
			if($payment_type == 'epg'){
				$epg_payment = new Model_EpgGateway();
			
				$reference = 'SC-' . $this->user->id . '-' . $hash;
				$token = $epg_payment->getTokenForAuth($reference, $amount , APP_HTTP.'://www.escortitalia.com/online-billing/epg-response');
				
				$this->client->call('OnlineBillingV2.storeToken', array($token, $this->user->id));

				$this->_redirect($epg_payment->getPaymentGatewayUrl($token));
			}elseif($payment_type == 'mmg'){
				$mmgBill = new Model_MmgBillAPIV2();
				
				$hosted_url = $mmgBill->getHostedPageUrl($amount, 'SCZ' . $this->user->id .'Z'.$hash, APP_HTTP.'://www.escortitalia.com/online-billing/mmg-response');
				$this->_redirect($hosted_url);die;
			}else{
				die;
			}
		

		} else {
			$escort_packages = $this->client->call('OnlineBillingV2.checkIfHasPaidPackage', array($escort_id));
			
			$allow = true;
			if ( count( $escort_packages ) ) {
				
				$message = '';
				$current_package = $escort_packages[0];
				$pending_package = $escort_packages[1];
				
				if ( $current_package && ( $current_package['status'] == PACKAGE_STATUS_PENDING ) ) {
					$allow = false;
					$message = 'online_billing_has_pending';
				} elseif ( $pending_package && $pending_package['status'] == self::PACKAGE_STATUS_PENDING ) {
					$allow = false;
					$message = 'online_billing_has_pending';
				}
				
				if ( ! $allow ) {
					$this->_helper->viewRenderer->setScriptAction("already-has-package");
					$this->view->error_message = $message;
					return;
				}
			}

		}
	}
	
	public function epgResponseAction()
	{
		try {
			$this->view->layout()->disableLayout();

			$req = $this->_request;

			$token = $req->Token;
			if ( ! $token ) return $this->_redirect('/');

			$epg_payment = new Model_EpgGateway();
			$result = $epg_payment->getTransactionStatus($token);
			if($req->test == 1){
				var_dump($result);die;
			}
			if ( $result['ResultStatus'] == "OK" ) {
				$this->_forward('ob-successful');
			} else {
				$this->_forward('ob-unsuccessful');
			}
		} catch(Exception $ex) {
			throw $ex;
		}
	}

	public function mmgResponseAction()
	{
		try {
			$this->view->layout()->disableLayout();

			$req = $this->_request;

			if (isset($req->txn_status) and $req->txn_status == 'APPROVED') {
				$this->_forward('ob-successful');
			} else {
				$this->_forward('ob-unsuccessful');
			}
		} catch(Exception $ex) {
			throw $ex;
		}
	}
	
	public function obSuccessfulAction()
	{
		
	}
	
	public function obUnsuccessfulAction()
	{
		
	}
}
