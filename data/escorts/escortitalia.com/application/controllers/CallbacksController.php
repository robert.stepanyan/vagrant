<?php

class CallbacksController extends Zend_Controller_Action
{
	/**
	 *
	 * @var Cubix_Api
	 */
	protected $_client;

	protected $_member_id;

	public function eurobillPhpAction()
	{
		$this->view->layout()->disableLayout();

		$this->_client = Cubix_Api::getInstance();

		$r = (object) $_REQUEST;
		$callback = new Cubix_Eurobill_Callback((object) array(
			'username' => $r->user_username,
			'action' => $r->action,
			'plan' => $r->account_account
		));
		

		$callback->registerHook('validate', array($this, 'handleValidate'));
		$callback->registerHook('payment', array($this, 'handlePayment'));
		$callback->registerHook('expiration', array($this, 'handleExpiration'));

		try {
			$callback->handle();
		}
		catch ( Exception $e ) {
			exit('ERROR');
		}

		exit('SUCCESS');
	}

	public function handleValidate($callback)
	{
		$member_id = $this->_client->call('members.getIdByUsername', array($callback->get('username')));
		if ( false === $member_id ) {
			throw new Cubix_Eurobill_Callback_UsernameDoesntExist("User with username {$callback->get('username')} does not exist");
		}
		$this->_member_id = $member_id;
	}

	public function handlePayment($callback)
	{
		switch ( (int) $callback->get('plan') ) {
			case 4:
				$duration = 30;
				break;
			case 5:
				$duration = 180;
				break;
			case 6:
				$duration = 365;
				break;
		}
		$expires = strtotime('+' . $duration . ' days');

		$this->_client->call('members.setPremium', array($this->_member_id, $expires, false));
	}

	public function handleExpiration()
	{
		$this->_client->call('members.setPremiumExpired', array($this->_member_id));
	}
}
