<?php

class CityAlertsController extends Zend_Controller_Action 
{
	private $_model;
	private $user;
		
	public function init() 
	{
		$this->user = $this->view->user = Model_Users::getCurrent();
		$this->view->layout()->setLayout('private-v2');
		
		if ( ! $this->user ) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}
		else if ($this->user->user_type != 'member')
		{
			$this->_redirect($this->view->getLink('private-v2'));
			return;
		}
		
		$this->_model = new Model_CityAlerts();
	}
	
	public function indexAction() 
	{
		
	}
	
	public function ajaxGetAction() 
	{
		$this->view->layout()->disableLayout();
		
		$this->view->items = $this->_model->getAll($this->user->id);
	}
	
	public function removeAction() 
	{
		$this->view->layout()->disableLayout();
		
		if ($this->_request->isPost())
		{
			$id = intval($this->_request->id);
			
			if (!$id) die;
			
			$this->_model->remove($id, $this->user->id);
		}
		
		die;
	}
	
	public function addAction() 
	{
		$this->view->layout()->disableLayout();
				
		$city_id = intval($this->_request->city_id);
		$escorts = intval($this->_request->escorts);
		$agencies = intval($this->_request->agencies);
		$notification = intval($this->_request->notification);
		$period = intval($this->_request->period);
				
		if (!in_array($notification, array(1, 2))) 
			$notification = 1;
		
		if (!in_array($period, array(CITY_ALERT_PERIOD_DAY, CITY_ALERT_PERIOD_WEEK))) 
			$period = CITY_ALERT_PERIOD_DAY;
		
		if ($this->_request->isPost())
		{
			$validator = new Cubix_Validator();
						
			if (!$city_id)
				$validator->setError('city_id', 'Required');
			elseif ($this->_model->checkCityExists($city_id, $this->user->id))
				$validator->setError('city_id', 'exists');
			
			if (!$escorts && !$agencies)
				$validator->setError('about', 'Required');
									
			if ( $validator->isValid() ) 
			{
				$data = array(
					'user_id' => $this->user->id,
					'city_id' => $city_id,
					'escorts' => $escorts,
					'agencies' => $agencies,
					'notification' => $notification,
					'period' => $period,
					'date' => new Zend_Db_Expr('NOW()')
				);
								
				$this->_model->add($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
}

?>
