<?php

class IndexController extends Zend_Controller_Action
{
	public function indexAction()
	{
		$this->_forward('index', 'escorts');
	}

	protected function _ajaxError()
	{
		die(json_encode(array('result' => 'error')));
	}

	public function ajaxContactAction()
	{
		$this->view->layout()->disableLayout();

		if ( $this->_request->isPost() ) {

			$validator = new Cubix_Validator();

			$data = new Cubix_Form_Data($this->_request);
			$fields = array(
				'name' => '',
				'email' => '',
				'message' => ''
			);
			$data->setFields($fields);
			$data = $data->getData();

			if ( ! strlen($data['email']) ) {
				$validator->setError('email', 'Email is required');
			} elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
				$validator->setError('email', 'Wrong email format');
			}

			if ( ! strlen($data['message']) ) {
				$validator->setError('message', 'Message is required');
			}

			$result = $validator->getStatus();

			if ( ! $validator->isValid() ) {

				if ( ! is_null($this->_getParam('ajax')) ) {

					//$result['status'] = 'success';

					echo(json_encode($result));
					ob_flush();
					die;
				}
			}
			else {

				// Fetch administrative emails from config
				$config = Zend_Registry::get('feedback_config');
				$site_emails = $config['emails'];

				$email_tpl = 'feedback_template';
				$data['to_addr'] = $site_emails['support'];

				$tpl_data = array(
					'name' => $data['name'],
					'email' => $data['email'],
					'to_addr' => $data['to_addr'],
					'message' => $data['message'],
					//'escort_showname' => $about_escort->showname,
					//'escort_id' => $about_escort->id,
					//'sent_by' => $sent_by
				);

				Cubix_Email::sendTemplate($email_tpl, $data['to_addr'], $tpl_data, isset($escort) ? $data['email'] : null);

				echo json_encode($result);
				die;
			}
		}
	}

	public function contactAction()
	{
		$this->view->layout()->setLayout('main-simple');
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$this->view->laura_skype_status = $client->call('Users.getSalesSkypeStatusById', array(181));
		$this->view->alex_skype_status = $client->call('Users.getSalesSkypeStatusById', array(187));
		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$fields = array(
				'name' => '',
				'email' => '',
				'message' => '',
			);
            $g_recaptcha_response = $this->_getParam('g-recaptcha-response');
			$data->setFields($fields);
			$data = $data->getData();
			$error = array();

			if ( ! strlen($data['email']) ) {
				$error['email'] = true;
			} elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
				$error['email'] = true;
			}

			if ( ! strlen($data['message']) ) {
				$error['message'] = true;
			}

            if (  $g_recaptcha_response == '' )  {
                $error['g_recaptcha_response'] = true;
            }

			if ( count($error) ) {

				$this->view->data = $data;
				$this->view->error = $error;
			}
			else {

				// Fetch administrative emails from config
				$config = Zend_Registry::get('feedback_config');
				$site_emails = $config['emails'];

				$email_tpl = 'feedback_template';
				$data['to_addr'] = $site_emails['support'];

				$tpl_data = array(
					'name' => $data['name'],
					'email' => $data['email'],
					'to_addr' => $data['to_addr'],
					'message' => $data['message']
				);

				Cubix_Email::sendTemplate($email_tpl, $data['to_addr'], $tpl_data, isset($escort) ? $data['email'] : null);

				$this->view->ok = true;
			}
		}
	}

    /**
     * Route endpoint handler for /compliance
     *
     * @HttpMethods GET | POST
     * @author Eduard Hovhannisyan
     */
    public function reportAction()
    {
        $session = new Zend_Session_Namespace('report');
        $this->view->layout()->setLayout('main-simple');

        $preventRequest = !empty($session->sent);

        if ($session->sent) {
            $this->view->ok = TRUE;
            $session->sent  = NULL;
        }

        if (!$preventRequest AND $this->_request->isPost()) {

            // Send reports to all emails in the array below
            $reportEmailsTo = [
                'info@escortitalia.com'
            ];

            $data   = new Cubix_Form_Data($this->_request);
            $fields = [
                'link'    => '',
                'email'   => '',
                'comment' => '',
            ];
            $data->setFields($fields);
            $data  = $data->getData();
            $error = [];

            if (!strlen($data['email'])) {
                $error['email'] = TRUE;
            } elseif (!preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email'])) {
                $error['email'] = TRUE;
            }

            if (!strlen(trim($data['comment']))) {
                $error['comment'] = TRUE;
            }

            if (!strlen(trim($data['comment']))) {
                $error['link'] = TRUE;
            }

            if (count($error)) {
                $this->view->data  = $data;
                $this->view->error = $error;
            } else {

                foreach ($reportEmailsTo as $email) {
                    $subject = 'New Report on EscortItalia.com';

                    $body = '<ul>';
                    $body .= "<li><b>Link to ad</b>: {$data['link']}</li>";
                    $body .= "<li><b>Email</b>: {$data['email']}</li>";
                    $body .= "<li><b>Comment</b>: {$data['comment']}</li>";
                    $body .= '</ul>';

                    Cubix_Email::send($email, $subject, $body, TRUE);
                }

                $session->sent = TRUE;
                $this->_redirect($this->view->getLink('report'));
            }
        }
    }

	public function ajaxContactSuccessAction()
	{
		$this->view->layout()->disableLayout();
	}

	public function chatOnlineCronAction()
	{
		ini_set('memory_limit', '1024M');

		$db = Zend_Registry::get('db');

		$xml = file_get_contents("php://input");
		$online_users = simplexml_load_string($xml);

		// Reset all online users
		$db->update('escorts', array('is_online' => 0), $db->quoteInto('is_online = ?', 1));

		$user_ids = array();
		foreach ( $online_users->children() as $user ) {
			$user_ids[] = (string) $user;
		}

		if ( count($user_ids) > 0 ) {
			$db->query('UPDATE escorts SET is_online = 1 WHERE user_id IN (\'' . implode('\',\'', $user_ids) . '\')');
		}


		die;
	}

	public function chatOnlineBtnAction()
	{
		$this->view->layout()->disableLayout();

		$req = $this->_request;

		$username = $req->username;
		try {
			$online_users = file_get_contents('http://www.escortforum.net:35555/roomonlineusers.js?roomid=1');

			if ( strlen($online_users) > 0 ) {
				$online_users = substr($online_users, 20, (strlen($online_users) - 21));

				$online_users = json_decode($online_users);

				if ( count($online_users) > 0 ) {
					foreach ( $online_users as $user ) {
						if( $user->name == $username ) {
							$this->view->show_btn = true;
							break;
						}
					}
				}
			}
			else {
				$this->view->show_btn = false;
			}
		}
		catch ( Exception $e ) {

		}
	}

	public function escortPhotosAction()
	{
		$id = intval($this->_getParam('id'));
		if ( $id <= 0 ) $this->_ajaxError();

		$escort = new Model_EscortItem();
		$escort->setId($id);

		$photos = $escort->getPhotos();

		$result = array();
		foreach ( $photos as $photo ) {
			$result[] = $photo->hash . '.' . $photo->ext;
		}

		die(json_encode(array('result' => 'success', 'data' => $result)));
	}

	public function checkHitsCacheAction()
	{
		$config = Zend_Registry::get('system_config');
		$data = Model_Escorts::getAllCachedHits();

		echo 'Cache Limit: ' . $config['hitsCacheLimit'] . "\n";
		echo 'Total Count: ' . count($data) . "\n\n";

		print_r($data);
		die;
	}

	public function splashAction()
	{
		$this->view->layout()->disableLayout();

		//$escorts = array(142241, 142256, 142257, 142260);

		//Model_Escorts::hit($escorts[rand(0, 3)]);

		/*$this->view->then = $this->_request->then;

		if ( $this->_request->enter ) {
			$domain = preg_replace('#^(.+?)([^\.]+)\.([^\.]+)$#', '.$2.$3', $_SERVER['HTTP_HOST']);
			setcookie('18', '1', null, '/', $domain);
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: ' . $this->view->then);
		}*/
	}

	public function feedbackAction()
	{
		$this->_request->setParam('no_tidy', true);

		$model = new Model_EscortsV2();

		$user = Model_Users::getCurrent();

		$is_premium = false;
		if ( isset($user->member_data) ) {
			$is_premium = $user->member_data['is_premium'];
		}

		$this->view->is_premium = $is_premium;

		// Fetch administrative emails from config
		$config = Zend_Registry::get('feedback_config');
		$site_emails = $config['emails'];

		$this->view->layout()->setLayout('main-simple');

		$is_ajax = ! is_null($this->_getParam('ajax'));

        $feedback = new Model_Feedback();

		// If the request is ajax, disable the layout
		if ( $is_ajax ) {
			$this->view->layout()->disableLayout();
		}

		// Get data from request
		$data = new Cubix_Form_Data($this->getRequest());
		$fields = array(
			'to' => '',
			'about' => 'int',
			'name' => 'notags|special',
			'email' => '',
			'message' => 'notags|special',
			'captcha' => '',
			'rand' => ''
		);
		$data->setFields($fields);
		$data = $data->getData();

        $blackListModel = new Model_BlacklistedWords();
		$this->view->data = $data;

		// If the to field is invalid, that means that user has typed by
		// hand, so it's like a hacking attempt, simple die, without explanation
		if ( ! is_numeric($data['to']) && ! in_array($data['to'], array_keys($site_emails)) ) {
			die;
		}

		// If escort id was specified, fetch it's email and showname,
		// construct the result email template and send it to escort
		if ( is_numeric($data['to']) ) {
			$escort = $model->get($data['to']);
			$this->view->escort = $escort;
			//var_dump($escort);die;
			// If the escort id is invalid that means that it is hacking attempt
			if ( ! $escort ) {
				die;
			}

			$data['to_addr'] = $escort->email;
			$data['to_name'] = $escort->showname;
			$data['escort_showname'] = $escort->showname;
			$email_tpl = 'escort_feedback';
		}
		// Else get administrative email from configuration file
		else {
			$data['to_addr'] = $site_emails[$data['to']];
			$email_tpl = 'feedback_template';
		}

		// In order when the form was posted validate fields
		if ( $this->_request->isPost() ) {
			$validator = new Cubix_Validator();

			if ( !$blackListModel->checkFeedback($data) ){
                $blackListModel->mergeFeedbackData($data);
                //$validator->setError('message', 'Do Not repeat The Same Message' /*'Email is required'*/);
            }


			if ( ! strlen($data['email']) ) {
				$validator->setError('email', '' /*'Email is required'*/);
			}
			elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
				$validator->setError('email', '' /*'Wrong email format'*/);
			}elseif( $blackListModel->checkEmail($data['email']) ){
                $data['flag'] = FEEDBACK_FLAG_BLOCKED_BY_EMAIL;
                $data['status'] = FEEDBACK_STATUS_DISABLED;
                $validator->setError('email', 'This email is blocked');
            }

			if ( ! strlen($data['message']) ) {
				$validator->setError('message','' /*'Please type the message you want to send'*/);
			}elseif($blackListModel->checkWords($data['message'])){
                $data['flag'] = FEEDBACK_FLAG_BLOCKED_BY_WORD;
				$data['status'] = FEEDBACK_STATUS_DISABLED;
                $validator->setError('message', 'You can`t use words "'.$blackListModel->getWords() .'"');
            }

            if( isset($data['flag']) && ($data['flag'] == FEEDBACK_FLAG_BLOCKED_BY_EMAIL || $data['flag'] == FEEDBACK_FLAG_BLOCKED_BY_WORD) ){
                $feedback->addFeedback($data);
            }

			/*$captcha = Cubix_Captcha::verify($this->_request->recaptcha_response_field);

			$captcha_errors = array(
				'invalid-site-public-key' => 'We weren\'t able to verify the public key.',
				'invalid-site-private-key' => 'We weren\'t able to verify the private key.',
				'invalid-request-cookie' => 'The challenge parameter of the verify script was incorrect.',
				'incorrect-captcha-sol' => 'The CAPTCHA solution was incorrect.',
				'verify-params-incorrect' => 'The parameters to /verify were incorrect, make sure you are passing all the required parameters.',
				'invalid-referrer' => 'reCAPTCHA API keys are tied to a specific domain name for security reasons.',
				'recaptcha-not-reachable' => 'reCAPTCHA never returns this error code. A plugin should manually return this code in the unlikely event that it is unable to contact the reCAPTCHA verify server.'
			);*/

			//var_dump($captcha);

			if ( ! strlen($data['captcha']) ) {
				$validator->setError('captcha', '');//Captcha is required
				}
			else {
				$session = new Zend_Session_Namespace('captcha');
				$orig_captcha = $session->captcha;

				if ( strtolower($data['captcha']) != $orig_captcha ) {
					$validator->setError('captcha', '');//Captcha is invalid
				}
			}


			$result = $validator->getStatus();

			if ( ! $validator->isValid() ) {
				// If the request was ajax, send the validation result as json
				if ( $is_ajax ) {
					echo json_encode($result);
					die;
				}
				// Otherwise, render the phtml and show errors in it
				else {
					$this->view->errors = $result['msgs'];
					return;
				}
			}
			else
			{
				$sess = new Zend_Session_Namespace('feedback');

				if ($data['rand'] != $sess->rand)
				{
					echo json_encode(array('status' => 'success', 'msg' => '<p style="padding:0 10px"><span class="strong" style="color: #060">Your feedback has been successfully submitted!</span></p>'));
					die;
				}

				if( isset($data['flag']) && ($data['flag'] == FEEDBACK_FLAG_BLOCKED_BY_EMAIL) ) { // IF blocked by email imitate as sent feedback
					echo json_encode(array('status' => 'success', 'msg' => '<p style="padding:0 10px; margin-top: 50px; width: 330px; font-size: 14px"><span class="strong" style="color: #000">' . __('message_sent_to_escort', array('showname' => $escort->showname)) . '</span></p>'));
					die;
				}
			}

			if ( $data['about'] ) {
				$about_escort = $model->get($data['about']);
			}
			if ( ! $about_escort ) $about_escort = (object) array('id' => '', 'showname' => '');

			$data['application_id'] = Cubix_Application::getId();

			if ($config['approvation'] == 1)
			{
				$data['flag'] = FEEDBACK_FLAG_NOT_SEND;
				$data['status'] = FEEDBACK_STATUS_NOT_APPROVED;
			}
			else
			{
				$data['flag'] = FEEDBACK_FLAG_SENT;
				$data['status'] = FEEDBACK_STATUS_APPROVED;

				// Set the template parameters and send it to support or to an escort
				$tpl_data = array(
					'name' => $data['name'],
					'email' => $data['email'],
					'to_addr' => $data['to_addr'],
					'message' => $data['message'],
					'escort_showname' => $escort->showname,
					'escort_id' => $escort->id
				);

				if ( isset($data['username']) ) {
					$tpl_data['username'] = $data['username'];
				}

				Cubix_Email::sendTemplate($email_tpl, $data['to_addr'], $tpl_data, isset($escort) ? $data['email'] : null);
			}

			$feedback->addFeedback($data);
			$sess->unsetAll();

			if ( $is_ajax ) {
				if ( isset($escort) ) {
					if ( $is_premium ) {
						$result['msg'] = '
							<p style="padding:0 10px; margin-top: 50px; width: 330px; font-size: 14px"><span class="strong" style="color: #FFF">' . __('message_sent_to_escort', array('showname' => $escort->showname)) . '</span></p>
						';
					}
					else {
						$result['msg'] = '
							<p style="padding:0 10px; margin-top: 50px; width: 330px; font-size: 14px"><span class="strong" style="color: #000">' . __('message_sent_to_escort', array('showname' => $escort->showname)) . '</span></p>
						';
					}
				}
				else {
					$result['msg'] = '
						<p style="padding:0 10px"><span class="strong" style="color: #060">Your feedback has been successfully submitted!</span></p>
					';
				}
				//<a href="#" onclick="return Cubix.Feedback.Show(this.getParent(\'.cbox-small-ii\'))">Click here</a> to hide this message</strong>
				echo json_encode($result);
				die;
			}
			else {
				$this->view->success = TRUE;
			}
		}
	}

	public function captchaAction()
	{

		$this->_request->setParam('no_tidy', true);

		$font = 'css/trebuc.ttf';

		$charset = '123456789ABCDEFGHIJKLMNPQRSTUVWXYZ';

		$code_length = 5;

		$height = 30;
		$width = 90;
		$code = '';

		for ( $i = 0; $i < $code_length; $i++ )
		{
			$code = $code . substr($charset, mt_rand(0, strlen($charset) - 1), 1);
		}

		$rgb[0] = array(204,0,0);
		$rgb[1] = array(34,136,0);
		$rgb[2] = array(51,102,204);
		$rgb[3] = array(141,214,210);
		$rgb[4] = array(214,141,205);
		$rgb[5] = array(100,138,204);

		$font_size = $height * 0.4;

		$bg = imagecreatefrompng('img/bg_captcha.png');
		$image = imagecreatetruecolor($width, $height);
		imagecopy($image, $bg, 0, 0, 0, 0, imagesx($bg), imagesy($bg));
		$background_color = imagecolorallocate($image, 255, 255, 255);
		$noise_color = imagecolorallocate($image, 20, 40, 100);

		for($i = 0; $i<$code_length; $i++)
		{
			$A[] = rand(-20, 20);
			$C[] = rand(0, 5);
			$text_color = imagecolorallocate($image, $rgb[$C[$i]][0], $rgb[$C[$i]][1], $rgb[$C[$i]][2]);
			imagettftext($image, $font_size, $A[$i], 7 + $i * 15, 20 + rand(-3, 3), $text_color, $font , $code[$i]);
		}

		$session = new Zend_Session_Namespace('captcha');
		$session->captcha = strtolower($code);

		header('Content-Type: image/png');
		imagepng($image);
		imagedestroy($image);

		die;
	}

	public function goAction()
	{
		$link = $_SERVER['QUERY_STRING'];
		if ( ! preg_match('#^http://#', $link) ) $link = 'http://' . $link;

		header('Location: ' . $link);
		die;
	}

	public function robotsAction()
	{
		$this->_request->setParam('no_tidy', true);

		$this->view->layout()->disableLayout();
		$model = new Model_Robots();

		$robots = $model->get();

		header('Content-Type: text/plain; charset=UTF-8', true);
		echo $robots;
		die;
	}

	public function benchmarkAction()
	{
		$db = Zend_Registry::get('db');
		$results = $db->fetchAll('SELECT * FROM debug_benchmark');

		$this->view->results = $results;
	}

	public function listDateImgAction()
	{
		$date = $this->_getParam('date');
		$date = intval($date);
		$date = ( date('d.m.Y') == date('d.m.Y', $date) ? $this->view->t('today_new') : date('d.m.Y', $date) );

		$im = imagecreatetruecolor(54, 15);
		imagefill($im, 0, 0, imagecolorallocate($im, 210, 210, 210));
		imagecolortransparent($im, imagecolorallocate($im, 210, 210, 210));

		imagettftext($im, 8, 0, 0, 13, imagecolorallocate($im, 0, 0, 0), 'css/arial.ttf', $date);

		header('Content-Type: image/gif');
		imagegif($im);
		imagedestroy($im);
		die;
	}

	public function photoViewerAction()
	{
		if ( $user = Model_Users::getCurrent() ) {
			$member_data = $user->member_data;
		}

		$response = array('status' => 'success', 'msg' => '', 'data' => array());

		$model = new Model_EscortsV2();

		$showname = $this->_getParam('showname');
		$escort_id = $this->_getParam('escort_id');
		//$escort = $model->get($showname);
		$escort = $model->get($escort_id);

		if ( ! $escort ) {
			$response['status'] = 'error';
			$response['msg'] = 'Showname is required';
		}
		else {
			$photos = $escort->getPhotos(null);

			Model_EscortsV2::hit_gallery( $escort->id );

			foreach ( $photos as $i => $photo ) {
				$response['data'][$i] = array(
					'id' => $photo->id,
					'private' => ($photo->type == 3) && (! isset($member_data) || ! $member_data['is_premium']),
					'h' => $photo->getUrl('orig'),
					'b' => $photo->getUrl('w514'),
					's' => $photo->getUrl('sthumb')
				);
			}
		}

		$this->_helper->json($response);
	}

	public function chatAction()
	{
		$this->view->layout()->disableLayout();
	}

	public function premiumListXmlAction()
	{
		$escorts = Model_Escort_List::getMainPremiumSpot(7);

		$writer = new XMLWriter();

		$writer->openMemory();

		$writer->startDocument('1.0');

		$writer->setIndent(4);

			$writer->startElement('escorts');
				if ( count($escorts) > 0 ) {
					foreach ( $escorts as $escort ) {
						 $writer->startElement('escort');
							$writer->writeElement('showname', $escort->showname);
							$writer->writeElement('url', "http://www.escortforum.net/accompagnatrici/" . $escort->showname);

							$available = '';
							if ( $escort->incall_type && $escort->outcall_type ) {
								$available = 'incall/outcall';
							}
							elseif ( $escort->incall_type && ! $escort->outcall_type ) {
								$available = 'incall';
							}
							elseif ( ! $escort->incall_type && $escort->outcall_type ) {
								$available = 'outcall';
							}

							$writer->writeElement('available', $available);

							$writer->writeElement('city', $escort->city);

							$writer->startElement('photos');
								$escort = new Model_EscortV2Item($escort);
								$writer->writeElement('photo', $escort->getMainPhoto()->getUrl('thumb'));
							$writer->endElement();
						 $writer->endElement();
					}
				}
			$writer->endElement();

		$writer->endDocument();

		echo $writer->flush(true);

		die;
	}

	public function autologinChatAction()
	{
		$user = Model_Users::getCurrent();

		if ( session_id() == $_REQUEST['uid'] && $user ) {
			$xml = '<login result="OK">';
		}
		else {
			$xml = '<login result="FAIL" error="error_authentication_failed">';
		}

		$xml .= '<userData>';
		$xml .= '<id>' . $user->id . '</id> ';
		$xml .= '<name><![CDATA[' . $user->username . ']]></name>';
		if ( $user->user_type == 'escort' || $user->user_type == 'agency' ) {
			$xml .= '  <gender>female</gender>';
		}
		else {
			$xml .= '  <gender>male</gender>';
		}
		$xml .= '<level>regular</level>';
		$xml .= '</userData>';
		$xml .= '<friends> </friends>';
		$xml .= '<blocks> </blocks>';
		$xml .= '</login>';

		echo $xml;

		die;
	}

	public function noChatAction()
	{
		$this->view->layout()->disableLayout();
	}

	public function sedcardTotalViewsAction(){
		$this->view->layout()->disableLayout();

		$cache = Zend_Registry::get('cache');

		$escort_id = (int)$this->_request->escort_id;

		if ( !$escort_id ){
			exit;
		}

		$cache_key = "sedcart_view_data_total_".$escort_id;


		$tmp_data = $cache->load(Model_EscortsV2::HITS_CACHE_KEY);

		$total_count = 0;
		if ( $tmp_data ){
			$total_count = (int)$tmp_data[$escort_id][1];
		}

		if ( ( ! $total = $cache->load( $cache_key ) ) || $total_count < 2  ) {
			$client = Cubix_Api_XmlRpc_Client::getInstance();
			$data['escort_id'] = $escort_id;
			$total = $client->call('Escorts.getTotalViews', array($escort_id));

			$cache->save($total, $cache_key, array(), 600);
		}

		$this->view->escort_id = $escort_id;

		$this->view->totalViews = $total + $total_count;

		$e_model = new Model_EscortsV2();
		if ( ! $e_model->isNotNewEscort($escort_id) ) {
			$this->view->dont_show = true;
		}
	}

	public function oEAction()
	{
		$m = new Model_EscortsV2();

		$escorts = $m->getLoginEscorts();

		$arr = array();

		if ($escorts)
		{
			foreach ($escorts as $e)
			{
				$arr[$e->escort_id] = $e->showname;
			}
		}

		echo json_encode($arr);
		die;
	}

	public function jsInitAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;

		$view = $this->view->act = $req->view;

		$req->setParam('lang_id', $req->lang);

		$lang = $this->view->lang = $req->lang;

		if ( $view == 'main-page' ) {

		}
		else if ( $view == 'main-simple' ) {

		}
		else if ( $view == 'main' ) {

		}
		else if ( $view == 'private' ) {

		}
		else if ( $view == 'private-v2' ) {

		}
		else if ( $view == 'profile' ) {

		}
		else if ( $view == 'profile-v2' ) {

		}

		/*header('Content-type: text/javascript');
		$expires = 60*60*24*14;
		header("Pragma: public");
		header("Cache-Control: maxage=" . $expires);
		header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expires) . ' GMT');*/

	}

	public function phoneImgAction()
	{
		$escort_id = (int) $this->_request->escort_id;
		$agency_id = (int) $this->_request->agency_id;
		$font_size = (int) $this->_request->fs;
		$width = (int) $this->_request->w;
		$height = (int) $this->_request->h;
		$v = (int) $this->_request->v;
		$color = $this->_request->c;
		$add = intval($this->_request->add);

		if (!in_array($add, array(1, 2)))
			$add = NULL;

		$colors = explode(',', $color);

		if ( $escort_id ) {
			$model = new Model_EscortsV2();
			$cache_key = 'v2_' . $escort_id .  '_profile_' . Cubix_I18n::getLang();
			$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
			$escort = $model->get($escort_id, $cache_key);
			$contacts = $escort->getContacts();

			if (!$add) {
				$filtered_phone =  (isset($contacts->phone_country_id) && !$contacts->disable_phone_prefix) ? '+'.Model_Countries::getPhonePrefixById($contacts->phone_country_id) . $contacts->phone_number : $contacts->phone_number;
			}
			else {
				$add_phone = $model->getAdditionalPhone($escort_id, $add);
				$filtered_phone =  (isset($add_phone->phone_country_id) && !$add_phone->disable_phone_prefix) ? '+' . Model_Countries::getPhonePrefixById($add_phone->phone_country_id) . $add_phone->phone_additional : $add_phone->phone_additional;
			}
		} else if ( $agency_id ) {
			$m_agencies = new Model_Agencies();
			$agency = $m_agencies->getLocalById($agency_id);
			$filtered_phone =  (isset($agency->phone_country_id)) ? '+'.Model_Countries::getPhonePrefixById($agency->phone_country_id) . $agency->phone : $agency->phone;
		}

		// Set the content-type
		header('Content-Type: image/png');

		// Create the image
		$im = imagecreatetruecolor($width, $height);

		// Create some colors
		$white = imagecolorallocate($im, 255, 255, 255);

		array_unshift($colors, $im);

		$text_color = call_user_func_array('imagecolorallocate', $colors);//($im, 33, 115, 161);

		imagefilledrectangle($im, 0, 0, 399, 29, $white);
		imagecolortransparent($im, $white);

		// Replace path by your own font path
		$font = 'css/ArchivoNarrow.ttf';

		// Add the text
		imagettftext($im, $font_size, 0, 0, $v, $text_color, $font, $filtered_phone);

		// Using imagepng() results in clearer text compared with imagejpeg()
		imagepng($im);
		imagedestroy($im);
		die;
	}

	public function emailTestAction()
	{
		var_dump(Cubix_Email::sendTemplate('universal_message', 'dave.kasparov@gmail.com', array(
			'subject' => "subject",
			'message' => "message",
		)));

		die("Email sent!");
	}
}
