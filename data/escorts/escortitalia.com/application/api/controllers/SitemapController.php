<?php

define('CLI_LINE_SEPARATOR', str_repeat('- ', 40));

/**
 * @author Eduard Hovhannisyan
 *
 * Class Api_SitemapController
 */
class Api_SitemapController extends Zend_Controller_Action
{
    /**
     * @var Zend_Db_Adapter_Mysqli
     */
    protected $_db;

    /**
     * @var Cubix_Cli
     */
    private $cli;

    /**
     * @var array
     */
    private $staticPages;

    /**
     * @var array
     */
    private $sitemaps;

    /**
     * @var array
     */
    private $langs;

    /**
     * @var string
     */
    private $hostname;

    /**
     * @return void
     */
    public function init()
    {
        $this->_db = Zend_Registry::get('db');

        $this->_cli = new Cubix_Cli();
        $this->_cli->clear();

        ini_set('memory_limit', '1024M');

        $this->staticPages = [
            //'privacy-policy' => ['url' => 'page/privacy-policy'],
            //'terms'          => ['url' => 'page/terms-and-conditions'],
            'contact' => ['url' => 'contact'],
            'blog'    => ['url' => 'blog'],
            'nuove'   => ['url' => 'escorts/nuove'],
        ];

        $this->sitemaps = [
            'PROFILES'     => 'profiles-sitemap',
            'STATIC_PAGES' => 'static-pages-sitemap',
            'CITIES'       => 'cities-sitemap'
        ];

        $this->langs = Cubix_Application::getLangs();

        $this->hostname = $this->_getHostname();
        $this->sitemapsBasePath = '../public';
    }

    /**
     * Create Sitemap.xml files and subdirectories
     * if needed.
     *
     * @return null;
     */
    public function buildAction()
    {
        /**
         * Prevent redundant calling for the same action
         */
        if (Cubix_Cli::pidIsRunning('/var/run/eit-sitemap-build.pid')) {
            $this->_cli->colorize('red')->out('Big Sync is running, exitting...');
            exit(1);
        }

        $fh = fopen($this->sitemapsBasePath . "/sitemap.xml", 'w+') or die("can't open file sitemap.xml");

        $time = date("Y-m-d") . "T" . date("H:i:s+00:00");

        $xmlContents = '';

        $xmlContents .= '<?xml version="1.0" encoding="UTF-8"?>';
        $xmlContents .= "<sitemapindex xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'>";

        foreach ($this->sitemaps as $name) {
            $xmlContents .= "<sitemap>";
            $xmlContents .= "    <loc>{$this->hostname}/sitemaps/{$name}.xml</loc>";
            $xmlContents .= "    <lastmod>{$time}</lastmod>";
            $xmlContents .= "</sitemap>";
        }

        $xmlContents .= '</sitemapindex>';

        $result = fwrite($fh, $xmlContents);
        fclose($fh);

        if ($result > 0) {
            $this->_cli->colorize('green')->out('Index Sitemap created!');
        } else {
            $this->_cli->colorize('red')->out('Couldn\'t create index Sitemap');
            exit;
        }

        if (!file_exists($this->sitemapsBasePath . '/sitemaps')) {
            mkdir($this->sitemapsBasePath . '/sitemaps', 0777, true);
            $this->_cli->colorize('green')->out('Sitemaps directory created!');
        }

        $tasks = [
            'Profiles'     => '_createProfilesSitemap',
            'Cities'       => '_createCitiesSitemap',
            'Static pages' => '_createStaticPagesSitemap',
        ];
        foreach ($tasks as $name => $task) {
            $result = $this->{$task}();

            $this->_cli->colorize('purple')->out(CLI_LINE_SEPARATOR);

            if ($result) {
                $this->_cli->colorize('green')->out("Sitemap for $name is created successfully ");
            } else {
                $this->_cli->colorize('red')->out('Failed to create Sitemap for ' . $name);
                exit;
            }
        }

        $this->_cli
            ->colorize('purple')->out(CLI_LINE_SEPARATOR)
            ->colorize('green')->out('Done, enjoy you day ♥')
            ->colorize('purple')->out(CLI_LINE_SEPARATOR);
        exit;
    }

    /**
     * Creates sitemap for static pages like privacy policy/terms and conditions etc.
     *
     * @return bool|int
     */
    private function _createStaticPagesSitemap()
    {
        $sitemapPath = $this->sitemapsBasePath . '/sitemaps/' . $this->sitemaps['STATIC_PAGES'] . '.xml';

        $xmlContents = '';
        $xmlContents .= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        $xmlContents .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';

        foreach ($this->staticPages as $key => $page) {
            $xmlContents .= "    <url>";
            $xmlContents .= "        <loc>{$this->hostname}/{$page['url']}</loc>";
            $xmlContents .= "    </url>";
        }

        $xmlContents .= "</urlset>";

        $fh = fopen($sitemapPath, 'w+') or die("can't open file $sitemapPath");
        $result = fwrite($fh, $xmlContents);
        fclose($fh);

        return $result;
    }

    /**
     * Creates sitemap for city pages
     *
     * @return bool|int
     */
    private function _createCitiesSitemap()
    {
        $sitemapPath = $this->sitemapsBasePath . '/sitemaps/' . $this->sitemaps['CITIES'] . '.xml';

        $countryISO = Cubix_Application::getById()->iso;

        $xmlContents = '';
        $xmlContents .= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        $xmlContents .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';

        $cities = [
            [
                'prefix' => '',
                'list'   => Model_Statistics::getCities(null),
            ],
            [
                'prefix' => '/boys',
                'list'   => Model_Statistics::getCities(null, GENDER_MALE),
            ],
            [
                'prefix' => '/trans',
                'list'   => Model_Statistics::getCities(null, GENDER_TRANS),
            ],
        ];

        foreach ($cities as $cityItem) {
            foreach ($cityItem['list'] as $city) {
                foreach ($this->langs as $lng) {
                    $url = $this->hostname . '/' . $lng->id . '/escorts' . $cityItem['prefix'] . '/city_' . $countryISO . '_' . $city->city_slug;

                    $xmlContents .= "    <url>";
                    $xmlContents .= "        <loc>$url</loc>";
                    $xmlContents .= "    </url>";
                }
            }
        }


        $xmlContents .= "</urlset>";

        $fh = fopen($sitemapPath, 'w+') or die("can't open file $sitemapPath");
        $result = fwrite($fh, $xmlContents);
        fclose($fh);

        return $result;
    }

    /**
     * Creates sitemap for escort profiles
     *
     * @return bool|int
     */
    private function _createProfilesSitemap()
    {
        $sitemapPath = $this->sitemapsBasePath . '/sitemaps/' . $this->sitemaps['PROFILES'] . '.xml';

        // Note! this is quick solution
        // 07/14/2020 EscortItalia does not have so many escorts (~110 overall)
        // If the amount of profiles will grow, this wont work!
        $escorts = $this->_db->fetchAll('SELECT id, showname FROM escorts LIMIT 1000');

        $xmlContents = '';
        $xmlContents .= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        $xmlContents .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';

        foreach ($escorts as $escort) {
            foreach ($this->langs as $lng) {
                $url = $this->hostname . '/' . $lng->id . '/accompagnatrici/' . $escort->showname . '-' . $escort->id;

                $xmlContents .= "    <url>";
                $xmlContents .= "        <loc>$url</loc>";
                $xmlContents .= "    </url>";
            }
        }

        $xmlContents .= "</urlset>";

        $fh = fopen($sitemapPath, 'w+') or die("can't open file $sitemapPath");
        $result = fwrite($fh, $xmlContents);
        fclose($fh);

        return $result;
    }

    /**
     * @return string
     */
    private function _getHostname()
    {
        return (APP_HTTP . '://' . APP_HOSTNAME);
    }
}