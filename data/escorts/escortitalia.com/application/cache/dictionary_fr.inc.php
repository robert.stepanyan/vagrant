<?php
$LNG['ladies'] = "Ladies";
$LNG['latest_join'] = "";
$LNG['width_comments'] = "Width Comments";
$LNG['100_fake_free_review'] = "100% Réel";
$LNG['100_fake_free_review_desc'] = "L'évaluation Réelle à 100% est si l'agence/escorte ont confirmé par sms que la rencontre a eu lieu, de cette façon. les deux parties conviennent s'être rencontrées, l'évaluation est 100% Réelle!";
$LNG['24_7'] = "24/7";
$LNG['PROD_additional_city'] = "Additional city";
$LNG['PROD_city_premium_spot'] = "City premium spot";
$LNG['PROD_girl_of_the_month'] = "Fille du mois";
$LNG['PROD_international_directory'] = "Annuaire International";
$LNG['PROD_main_premium_spot'] = "Principal lieu de travaille";
$LNG['PROD_national_listing'] = "listing national";
$LNG['PROD_no_reviews'] = "pas de revues";
$LNG['PROD_search'] = "recherche";
$LNG['PROD_tour_ability'] = "Tour ability";
$LNG['PROD_tour_premium_spot'] = "&quot|On tour&quot| premium spot";
$LNG['_all'] = "All";
$LNG['about_me'] = "À propos de Moi";
$LNG['about_meeting'] = "À propos de la rencontre";
$LNG['account_activated'] = "Votre compte a été activé avec succès, Svp branchez-vous maintenant et ajoutez votre profil/escorte. <a href='%sys_url%/login.php'>Cliquez-ici</a> pour vous brancher.";
$LNG['account_blocked_desc'] = "<p>Votre compte a été suspendu en</p> 
raison de l'une des raisons suivantes :: 
<ul> 
<li>Insulte envers un membre ou escorte</li> 
<li>Discrimination envers un membre ou escorte</li> 
<li>Avoir fait une fausse évaluation ou faux message</li> 
<li>Instigation des usagers envers autrui</li> 
<li>Avoir fait la promotion outrancière d'une ou des agences ou escortes</li> 
</ul> 
<p>Si vous voulez nous faire-part de vos commentaires ou version des faits, contactez-nous à : <a href='mailto:%sys_contact_email%'>%sys_contact_email%</a></p> 
<p>NOTE: Svp ne pas vous enregistrer de nouveau sans nous contacter, nous effacerons vos messages/évaluations et bloquerons votre adresse IP</p>";
$LNG['account_not_verified'] = "Ce compte n'a pas été vérifié.";
$LNG['account_type'] = "Type de Compte";
$LNG['active'] = "Active";
$LNG['add'] = "Ajouter";
$LNG['add_escort'] = "Ajouter un profil d'escorte";
$LNG['add_escort_profile'] = "Ajouter un profil d'escorte";
$LNG['add_profile_now'] = "Ajoutez votre profil avec photo MAINTENANT!";
$LNG['add_review'] = "Ajouter une évaluation";
$LNG['add_third_party_girl'] = "Ajouter une escorte de tierce partie à la base de données";
$LNG['add_watch'] = "Ajoutez à votre liste de Favoris";
$LNG['add_watch_success'] = "<b>%NAME%</b> a été AJOUTÉE avec succès à 
votre liste de Favoris<a href='javascript:history.back()'>Cliquez ici 
pour retourner.</a>";
$LNG['additional'] = "Additionel";
$LNG['address'] = "Rue ou Avenue";
$LNG['age'] = "Age";
$LNG['age_between'] = "Agée entre";
$LNG['agencies'] = "Agencies";
$LNG['agencies_match'] = "Agences répondant aux critères";
$LNG['agency'] = "Agence";
$LNG['agency_city_tour_not_allowed'] = "Seul les filles QUI NE SONT PAS en période d'éssai sont listées. La fonction \"Tour de Ville\" n'est pas accessible à celles en période d'éssai. Svp commandez votre espace Privilège, pour activer cette fonction pour elles.<a href='mailto:%sys_contact_email%'>Cliquez-ici pour commander</A>.";
$LNG['agency_data'] = "Données sur l'Agence";
$LNG['agency_denied_review'] = "Refusé";
$LNG['agency_denied_review_desc'] = "L'agence a indiqué que selon son site, les données fournies ne corespondent à aucun de leurs client. La plupart du temps c'est une fausse évaluation et vous ne devriez pas vous fier à celle-ci parce que l'agence/escorte ne sait pas si l'évalutation est bonne ou mauvaise par la confirmation!";
$LNG['agency_girls'] = "Filles d' agence";
$LNG['agency_name'] = "Nom de l'Agence";
$LNG['agency_profile'] = "Profil d'Agence";
$LNG['agency_without_name'] = "Agence sans nom";
$LNG['ajaccio'] = "Ajaccio";
$LNG['all'] = "Tous...";
$LNG['all_cities'] = "Toute les villes...";
$LNG['all_countries'] = "Tous les Pays...";
$LNG['all_escorts'] = "All escorts";
$LNG['all_origins'] = "Toutes origines...";
$LNG['all_reviews'] = "Toutes les évaluations";
$LNG['already_registered_on_another_site'] = "Le Login existe déjà. Si vous avez déjà un compte sur %SITE% avec le même nom, vous n'avez pas besoin de vous enregistrer à nouveau. Merci de cliquer ici et loguez vous avec le même nom et mot de passe que vous utilisez sur %SITE%.";
$LNG['already_reviewed'] = "Vous avez déjà évalué cette escorte! Seulement une évaluation par escorte est possible, Svp mettez à jour votre <a href='%LINK%'>Évaluation Actuelle</a> si vous le désirez!";
$LNG['already_voted'] = "Vous avez deja vote pour cette escorte.";
$LNG['also_provided_services'] = "Elle a aussi rendu ces services";
$LNG['anal'] = "Anal";
$LNG['are_you_escort'] = "Êtes-vous une escorte indépendante, une Agence ou un Club Privé?";
$LNG['at_her_apartment'] = "À son appartement";
$LNG['at_her_hotel'] = "À son Hôtel";
$LNG['at_my_flat_or_house'] = "Chez Moi";
$LNG['at_my_hotel'] = "À mon Hôtel";
$LNG['attention'] = "Attention !";
$LNG['attitude'] = "Attitude";
$LNG['availability'] = "Disponibilité";
$LNG['available_for'] = "Disponible pour";
$LNG['basic'] = "Base";
$LNG['back_to_actual_tours'] = "Back to actual tours";
$LNG['back_to_reviews'] = "";
$LNG['beauty'] = "";
$LNG['become_premium_member_and_view_private_photos'] = "<a href='%LINK%'>Become a premium member NOW</a> and view uncensored and special photos of this girl!";
$LNG['bio'] = "Bio";
$LNG['birth_date'] = "Date de Naissance";
$LNG['black'] = "Noir";
$LNG['blond'] = "Blonde";
$LNG['blowjob'] = "Oral";
$LNG['blowjob_with_condom'] = "Oral avec Condom";
$LNG['blowjob_without_condom'] = "Oral sans Condom";
$LNG['blue'] = "Bleus";
$LNG['bombshellf'] = "Super Canon/Pétard";
$LNG['book_email_to_escort'] = "<pre> 
Cher %ESCORT%, 
un membre Privilège de %sys_site_title% souhaiterait vous rencontrer le %DATE%. 
Ce membre a droit à l'escompte de %DISCOUNT%%, que vous consentez à nos membres de confiance. 
Details: 
Usager : %USERNAME% 
Nom Complet : %FULLNAME% 
Date désirée : %DATE% 
Durée : %DURATION% 
Téléphone : %PHONE% 
Courriel : %EMAIL% 
Commentaires : %COMMENTS% 
Code de sécurité: %CODE% 
Ce code est utilisé afin de vérifier le Statut du Membre. Ce membre est obligé de vous donner ce code sur demande si vous désirez vérifier son droit à l'escompte offert. 
Salutations, 
<a href=\"%sys_url%\">%sys_site_title%</a> 
</pre>";
$LNG['book_this_girl'] = "Réservez cette escorte";
$LNG['booking_desc'] = "Merci de votre intérêt envers %NAME%. En tant que Membre Privilège, vous avez droit à un escompte de %DISCOUNT%% lors de votre réservation avec cette escorte. Svp, utilisez le formulaire ci-dessous afin de réserver cette escorte et demander votre escompte!";
$LNG['booking_mail_subject'] = "RESERVATION %SITE%";
$LNG['booking_mail_text'] = "<pre> 
Cher %NAME%, 
Nous avons traitée avec succès votre demande de réservation pour %ESCORT_NAME% pour une rencontre le %DATE%. 
Cette escorte entrera en contact avec vous sous peu afin de finaliser les détails de votre rencontre. 
En tant que Membre Privilège de %SITE%, vous avez droit à un escompte de %DISCOUNT%% sur le prix de cette escorte. 
Dans le cas de n'importe quelle complication, Svp gardez dans un endroit sûr ce numéro de sécurité : %VERIFICATION_ID% 
Ce numéro peut être utilisé pour confirmer votre statut de Membre Privilège et recevoir votre escompte. 
Nous vous souhaitons un agréable moment en compagnie de l'escorte de votre choix ! 
L'équipe de %sys_site_title% 
</pre>";
$LNG['boys'] = "Boys";
$LNG['boys_trans'] = "Garçon/Trans";
$LNG['breast'] = "Poitrine";
$LNG['breast_size'] = "Poitrine";
$LNG['brown'] = "Brunette";
$LNG['bruntal'] = "Bruntál";
$LNG['bust'] = "Buste";
$LNG['by'] = "par";
$LNG['calabria'] = "Calabria";
$LNG['can_have_max_cities'] = "Vous pouvez avoir un maximum de %NUMBER% régions définies";
$LNG['cancel'] = "Annuler";
$LNG['cant_be_empty'] = "ne peut être vide";
$LNG['cant_go_ontour_to_homecity'] = "Escort en deplacement elle ne peut pas recevoir dans sa ville de résidence";
$LNG['castelfranco'] = "Castelfranco";
$LNG['chalons-en-champagne'] = "Chalons-en-Champagne";
$LNG['change_all_escorts'] = "Appliquez le changement à tous les profils d'escortes";
$LNG['change_passwd'] = "Changer Mot de Passe";
$LNG['char_desc'] = "tatoo etc.";
$LNG['characteristics'] = "Caractéristique";
$LNG['chiavari'] = "Chiavari";
$LNG['choose'] = "Choisir...";
$LNG['choose_availability'] = "Choose availability...";
$LNG['choose_another_city'] = "Choisir une autre ville";
$LNG['choose_another_region'] = "Choose another region";
$LNG['choose_city'] = "Choisir une Ville...";
$LNG['choose_country'] = "Choisir un Pays...";
$LNG['choose_currency'] = "Choisir devises...";
$LNG['choose_escort'] = "Choisir escorte...";
$LNG['choose_escort_to_promote'] = "Choisissez une escorte à promouvoir";
$LNG['choose_language'] = "Choisir langues";
$LNG['choose_level'] = "Choisir niveau...";
$LNG['choose_ethnic'] = "Choisir groupe ethnique";
$LNG['choose_nationality'] = "Choisir nationalité...";
$LNG['choose_state'] = "Choisir État/Province...";
$LNG['choose_this_user'] = "Choisir ce Membre";
$LNG['choose_time'] = "Choisir heures...";
$LNG['choose_your_city'] = "Choisissez une ville";
$LNG['choose_your_country'] = "Choisir votre Pays";
$LNG['choosen_escort'] = "Escorte choisie: %NAME%";
$LNG['cities'] = "Villes";
$LNG['city'] = "Ville";
$LNG['city_escort_notify'] = "Soyez informé par courriel si une escorte est ajoutée dans votre ville";
$LNG['city_of_meeting'] = "Ville de la rencontre";
$LNG['claim_your_discount'] = "Demandez votre escompte MAINTENANT et réservez cette escorte";
$LNG['comment_review'] = "";
$LNG['comment'] = "Comment";
$LNG['comments'] = "Commentaires";
$LNG['comments_to_her_services'] = "Commentaires sur ses servcies";
$LNG['comments_to_her_services_desc'] = "Décrivez la part sexuelle, ex : Services spéciaux (S/M, toys, etc).";
$LNG['company_name'] = "Nom de l'Agence";
$LNG['conf_password'] = "Confirmer le mot de passe";
$LNG['confirmation_email_1'] = " 
Bonjour ou Bonsoir 
Vous venez de vous inscrire sur %sys_site_title% 
Pour valider votre inscription vous devez mettre notre bannière sur votre site.( si vous en avez un) 
Vous les trouverez à cette adresse : http://www.%sys_site_title%.com/contact.php 
Nous allons prochainement visiter votre site, une fois que vous aurez mis notre bannière nous validerons votre site. 
";
$LNG['confirmation_email_2'] = "s'il vous plaît, garder cette information dans un endroit sécurisé.";
$LNG['confirmation_email_subject'] = "%sys_site_title% Enregistrement";
$LNG['contact'] = "Contact";
$LNG['contact_her'] = "Contactez directement cette escorte";
$LNG['contact_her_desc'] = "Vous êtes en train de contacter %NAME%. Ce courriel viendra du domaine de %SITE% afin de certifier votre statut de Membre Privilège de %SITE%";
$LNG['contact_her_mail_header'] = "<pre> 
************************************************************************************* 
Voici le message d'un Membre Privilège certifié de %SITE%. Ce membre est digne de confiance et acquitte ses frais de membre de façon régulière. 
************************************************************************************* 
</pre>";
$LNG['contact_her_mail_subject'] = "Message d'un Membre Privilège de %SITE%";
$LNG['contact_info'] = "Info contact";
$LNG['contact_text'] = " Étant la plus importante agence d'escorte en %sys_app_country_title%, nous offrons maintenant de l'espace publicitaire sur notre site. Nous avons limité le nombre de sites externes à 10. Ce qui apporte un excellent nombre de clic pour chaque bannières.<BR /> 
<BR /> 
Quelques statistiques:<BR /> 
<BR /> 
Visites -> En moyenne, plus de 3 millions par jour<BR /> 
visiteurs uniques -> 25,000 et plus par jour en moyenne<BR /> 
Membres -> Près de 15,000, avec de plus en plus ajoutés chaque jours 
<BR/><BR /> 
Si vous aimeriez tenter votre chance maintenant et discuter des 
détails, simplement nous contacter. Nous vous répondrons aussitôt 
<BR /><BR /> 
<a href='mailto:%sys_contact_email%'>%sys_contact_email%</a> 
Tél.: 0036 1203 1808 (10:00 - 20:00) 
</pre> 
";
$LNG['contact_us'] = "Contactez-Nous";
$LNG['continue'] = "Continuer";
$LNG['conversation'] = "Conversation";
$LNG['countries'] = "Pays";
$LNG['country'] = "Pays";
$LNG['country_tw'] = "Taiwan";
$LNG['couples'] = "Les couples";
$LNG['created'] = "Crée le";
$LNG['cumshot'] = "Éjaculation";
$LNG['cumshot_on_body'] = "Éjaculation sur le corps";
$LNG['cumshot_on_face'] = "Éjaculation sur le visage";
$LNG['curr_password'] = "Mot de Passe actuel";
$LNG['curr_password_empty_err'] = "Entrez votre Mot de Passe actuel";
$LNG['currency'] = "Devises";
$LNG['currently_listed_sites'] = "Actuellement nous avons listés les sites pour votre choix. Svp choisir un site.";
$LNG['date'] = "Date";
$LNG['day'] = "jour";
$LNG['day_of_meeting'] = "Date de la rencontre";
$LNG['day_phone'] = "Téléphone de jour";
$LNG['days'] = "jours";
$LNG['default_city'] = "Ville affichée par défaut après branchement";
$LNG['delete_rates'] = "Effacer tous les tarifs";
$LNG['delete_sel_pcis'] = "Effacer la photo sélectionnée";
$LNG['delete_selected_reviews'] = "Effacer les évaluation sélectionnées";
$LNG['delete_selected_tour'] = "Effacer la tournée sélectionnée";
$LNG['desired_date'] = "Date désirée";
$LNG['discount'] = "escompte";
$LNG['discount_code'] = "Code d'escompte";
$LNG['discount_code_txt'] = "Pour réclamer votre escompte de 5%, entrez svp votre code d'escompte, si vous en avez reçu un de nos partenaires.";
$LNG['discount_desc'] = "Sur cette page vous trouverez toutes les escortes qui offrent des <B>escompte</B> à nos Membres Privilège.";
$LNG['discounted_girls'] = "Escompte";
$LNG['dont_forget_variable_symbol'] = " 
<p>Svp N'OUBLIEZ PAS d'inclure votre symbole variable: 
<big>%ESCORT_ID%</big>.<br /> 
Sans lui nous ne pourrons pas identifier votre paiement.<br /> 
Merci.</p> 
";
$LNG['dont_know'] = "Ne sais pas";
$LNG['dress_size'] = "Taille de vêtement";
$LNG['duration'] = "Durée";
$LNG['easy_to_get_appointment'] = "Facile de réserver";
$LNG['edit'] = "Edition";
$LNG['edit_escort'] = "Changer profil d'escorte";
$LNG['edit_gallery'] = "Éditer gallerie";
$LNG['edit_languages'] = "Éditer langues";
$LNG['edit_locations'] = "Éditer régions";
$LNG['edit_private_photos'] = "Edit private photos";
$LNG['edit_private_photos_desc'] = "Please upload non-censored (without hiding any part of the picture) or glamour (special photos, which you do not want to offer to the wide public) photos here, which only our Premium paying users will be able to view. This way we ensure, that your sensitive photos, are only being seen by high-class customers, which expressed already their interest by paying the membership fee.";
$LNG['edit_profile'] = "Changer profil";
$LNG['edit_rates'] = "Éditer tarifs";
$LNG['edit_review'] = "Éditer évaluation";
$LNG['edit_settings'] = "Changer Préférences";
$LNG['edit_units_in'] = "Éditer unités en";
$LNG['email'] = "Courriel";
$LNG['email_domain_blocked_err'] = "Nous sommes désolés, %DOMAIN% n'acepte pas de courriel de nous en ce moment. Svp, choisissez une adresse de courriel diférente ou contactez %DOMAIN% afin qu'ils acceptent notre correspondance.";
$LNG['email_lng'] = "Langue utilisée dans les courriels";
$LNG['email_not_registered'] = "Désolés, cette adresse de courriel n’est pas enregistrée à notre site.";
$LNG['empty_login_err'] = "Nom d'Usager ne peut être vide";
$LNG['empty_passwd_err'] = "Mot de Passe ne peut être vide";
$LNG['empty_watchlist'] = "Vous n'avez aucun item sur votre Liste de Favoris:";
$LNG['english'] = "Anglais";
$LNG['enthusiastic'] = "Enthousiaste";
$LNG['error'] = "Erreur";
$LNG['error_adding_watch'] = "Il y a eu une erreur de l'Ajout/Effacement d'un item à/de votre liste de Favoris. Svp, contactez un administrateur.";
$LNG['esc_mb_chng_subject'] = "Changement d'adhésion sur %sys_site_title%";
$LNG['esc_mb_chng_to_free_email'] = "<pre> 
Cher %LOGIN%, 
L'adhésion de votre profil d'escorte avec le nom %SHOWNAME% a été changé pour \"Gratuit\". 
Vous avez perdu les avantages d'une adhésion payante. 
Améliorez votre adhésion maintenant et obtenez: 
- La participation à la rotation des escortes sur la page principale, signifiant que vous ne stagnerez jamais aux dernières pages où peu d'utilisateur peuvent vous voir. Tous les profils payés sont changés à chaque heure, ainsi chaque profil a la même chance d'apparaître à la première page d'une ville particulière ou même d'une page principale de site. 
- Vous pourrez participer à la compétition \"Copine du Mois\"</li> 
- Vous aurez le chance d'être dans la liste d'escortes du TOP 10 
<!-- De plus, profitez de cette offre spéciale : <p 
style='color:red'>Annonce imprimée dans l'édition du mois suivant du <a 
target='_blank' href='http://www.redmag.it'>Redmag</a> magazine en 
Italie</p>--> 
Pour améliorer votre adhésion, vous pouvez répondre à ce courriel ou visitez le site et cliquer sure le bouton \"Améliorer\". 
L'équipe de %sys_site_title%</pre>";
$LNG['esc_mb_chng_to_normal_email'] = "<pre> 
Cher %LOGIN%, 
L'adhésion de votre profil d'escorte avec le nom %SHOWNAME% a été changé pour \"normal \". Nouvelle date d'échéance: %EXPIRE% 
Dorénavant vous bénéficiez des avantages suivants: 
- La participation à la rotation des escortes sur la page principale, 
signifiant que vous ne stagnerez jamais aux dernières pages où peu 
d'utilisateur peuvent vous voir. Tous les profils payés sont changés à 
chaque heure, ainsi chaque profil a la même chance d'apparaître à la 
première page d'une ville particulière ou même d'une page principale de 
site. 
- Vous pourrez participer à la compétition \"Copine du Mois\"</li> 
- Vous aurez le chance d'être dans la liste d'escortes du TOP 10 
<!-- De plus, profitez de cette offre spéciale : <p 
style='color:red'>Annonce imprimée dans l'édition du mois suivant du <a 
target='_blank' href='http://www.redmag.it'>Redmag</a> magazine en 
Italie</p>--> 
L'équipe de %sys_site_title% 
</pre> 
";
$LNG['esc_mb_chng_to_premium_email'] = "<pre> 
Cher %LOGIN%, 
L'adhésion de votre profil d'escorte avec le nom %SHOWNAME% a été promu à \"Privilège\". Nouvelle date d'échéance: %EXPIRE% 
Dorénavant vous bénéficiez des avantages suivants: 
- EN CONSTRUCTION 
<!-- De plus, profitez de cette offre spéciale : <p 
style='color:red'>Annonce imprimée dans l'édition du mois suivant du <a 
target='_blank' href='http://www.redmag.it'>Redmag</a> magazine en 
Italie</p>--> 
L'équipe de %sys_site_title%</pre>";
$LNG['esc_mb_chng_to_trial_email'] = "<pre> 
Cher %LOGIN%, 
L'adhésion de votre profil d'escorte avec le nom %SHOWNAME% a été changée pour \"Essai\". Nouvelle date d'échéance: %EXPIRE% 
On vous a accordé une adhésion d'essai sur %sys_site_title%. Durant cette période, vous recevrez tous les privilèges d'une adhésion payante, qui inclus: 
- La participation à la rotation des escortes sur la page principale, signifiant que vous ne stagnerez jamais aux dernières pages où peu d'utilisateur peuvent vous voir. Tous les profils payés sont changés à chaque heure, ainsi chaque profil a la même chance d'apparaître à la première page d'une ville particulière ou même d'une page principale de site. 
- Vous pourrez participer à la compétition \"Copine du Mois\"</li> 
- Vous aurez le chance d'être dans la liste d'escortes du TOP 10 
<!-- De plus, profitez de cette offre spéciale : <p 
style='color:red'>Annonce imprimée dans l'édition du mois suivant du <a 
target='_blank' href='http://www.redmag.it'>Redmag</a> magazine en 
Italie</p>--> 
Pour améliorer votre adhésion, vous pouvez répondre à ce courriel ou visiter le site et cliquer sure le bouton \"Améliorer\". 
L'équipe de %sys_site_title%</pre>";
$LNG['esc_mb_chng_to_vip_email'] = "<pre>Cher %LOGIN%, 
L'adhésion de votre profil d'escorte avec le nom %SHOWNAME% a été 
changé pour \"VIP\". 
Ce profil n'expirera pas et partage les mêmes avantages qu'une adhésion 
payée normale. 
L'équipe de %sys_site_title% 
</pre> 
";
$LNG['esc_mb_memb_expired_email'] = "<pre>Cher %LOGIN%, 
l'adhésion de votre profil d'escorte avec le nom %SHOWNAME% a expiré. 
Vous avez perdu vos privilèges et votre compte a été suspendu. 
Si vous désirez renouveller votre abonnement, Svp contactez-nous au %sys_contact_email%. 
L'équipe de %sys_site_title% 
</pre> 
";
$LNG['esc_mb_memb_expired_email_subj'] = "Adhésion Expirée";
$LNG['esc_mb_memb_expires_soon_email'] = "<pre> 
Cher %LOGIN%, 
L'adhésion de votre profil d'escorte avec le nom %SHOWNAME% expire dans %DAYS% jours. 
Si vous souhaitez renouveller votre adhésion, Svp contactez-nous à %sys_contact_email%. 
L'équipe de %sys_site_title%</pre>";
$LNG['esc_mb_memb_expires_soon_email_subj'] = "Votre adhésion expire bientôt";
$LNG['esc_to_upg'] = "Profile d'escorte à améliorer (adhésion)";
$LNG['esc_upg_promo'] = " 
<ul> 
<li> La participation à la rotation des escortes sur la page 
principale, signifiant que vous ne stagnerez jamais aux dernières pages 
où peu d'utilisateur peuvent vous voir. Tous les profils payés sont 
changés à chaque heure, ainsi chaque profil a la même chance 
d'apparaître à la première page d'une ville particulière ou même d'une 
page principale de site.</li> 
<li> Vous pourrez participer à la compétition \"Copine du Mois\"</li> 
<li> Vous aurez le chance d'être dans la liste d'escortes du TOP 10 
</li> 
</ul>";
$LNG['esc_upg_promo_txt'] = "<a href='/private/upgrade_escort.php'>Améliorez Maintenant!</a> 
<!-- Et profitez de cette offre spéciale : <p style='color:red'>Annonce 
imprimée dans l'édition du mois suivant du <a target='_blank' 
href='http://www.redmag.it'>Redmag</a> magazine en Italie</p>-->";
$LNG['esc_upgrade_payment_details'] = " 
Merci de votre décision d'adhérer à %sys_site_title%.<br /> 
Veuiller transférer %AMOUNT% Euros à notre compte et votre profil sera mis à jour automatiquement sur réception du paiement.<br /> 
<p><strong>Détails Banquaires:</strong><br /> 
SMD MEDIA:<br /> 
Conto corrente n. 6152293433/25<br /> 
CAB: 03069<br /> 
ABI: 10910<br /> 
Banca Intesa - filiale 2390<br /> 
Via G. Rubini n. 6, 22100 Como</p> 
Si vous éprouvez des problèmes ou pour plus d'information, Svp, 
n'hésitez pas à communiquer avec nous à <a href='mailto:%sys_contact_email%'>%sys_contact_email%</a>. 
";
$LNG['escort'] = "escorte";
$LNG['escort_active'] = "Votre profil d'escorte est ACTIF. <a href='%LINK%'>Cliquez-ici</a> pour le désactiver.";
$LNG['escort_added_continue_review'] = "Escorte ajoutée avec succès. <a href='%LINK%'>Cliquez ici</a> pour continuer avec une évaluation.";
$LNG['escort_city_tour_not_allowed'] = "Vous n'êtes pas autorisé à utiliser la fonction \"Tour de Ville\" durant votre période d'éssai. <a href='mailto:%sys_contact_email%'>Cliquez-ici pour commander</A>.";
$LNG['escort_data_form_text'] = "Entrez seulement l'information que vous voulez afficher sur le site.";
$LNG['escort_inactive'] = "Cette escort est actuellement inactive!";
$LNG['escort_modification_disabled'] = "At this moment we are performing scheduled maintenance. No escort modifications are accepted. Please come back later. We are sorry for the inconvenience.";
$LNG['escort_name_active'] = "Le profil d'escorte %ESCORT% est ACTIF. <a href='%LINK%'>Cliquez-ici</a> pour le désactiver.";
$LNG['escort_name_not_active'] = "Le profil d'escorte %ESCORT% est INACTIF. <a href='%LINK%'>Cliquez-ici</a> pour l'activer.";
$LNG['escort_name_waiting_for_approval'] = "Le profil d'escorte %ESCORT% est en attente d'aprobation de l'administration.";
$LNG['escort_not_active'] = "Votre profil d'escorte est INACTIF. <a href='%LINK%'>Cliquez-ici</a> pour l'activer.";
$LNG['escort_not_found_use_search'] = "escorte %NAME% non identifiée, Svp utiliser la recherche pour en spécifier une.";
$LNG['escort_not_in_db_add_her'] = "Si l'escorte n'est pas dans la base de données, Svp <a href='%LINK%'>ajoutez-la</a>.";
$LNG['escort_profile_added'] = "Le profil de l'escorte a été ajouté avec succès à la base de données !<BR /> Svp <a href='/private/edit_gallery.php'>Ajoutez des photos à votre profil.</a>";
$LNG['escort_profile_updated'] = "Le profil de l'escorte a été mis à 
jour avec succès ! <BR /> 
<a href='/private/index.php'>Retour en arrière.</a>";
$LNG['escort_waiting_for_approval'] = "Votre profil d'escorte est en attente d'aprobation de l'administration.";
$LNG['escorts'] = "Escortes";
$LNG['escorts_in_city'] = "Escorte à %CITY%";
$LNG['escorts_match'] = "Escortes répondant aux critères";
$LNG['evening_phone'] = "Téléphone Autre";
$LNG['expires'] = "Expire";
$LNG['eye_color'] = "Couleur des yeux";
$LNG['eyes'] = "Yeux";
$LNG['fake_free_review_confirm_mail_01'] = " 
    <p>Hi %SHOWNAME%,<br /> 
    un membre de %sys_site_title% a postée une évaluation de vous sur notre site. Dans le but de prévenir une fausse évaluation, nous vous demandons de vérifier si vous avez bel et bien eu ce client et Svp nous confirmer ou rejeter l'information de ce courriel! Svp, simplement répondre à ce courriel avec la mention OUI ou NON. OUI veut dire que vous avez bien rencontré ce client!</p> 
    <p>Détails sur le Client:</p> 
";
$LNG['fake_free_review_confirm_mail_02'] = " 
    <p>Répondez maintenent avec la mention OUI ou NON (Oui pour Vrai et Non pour Faux)</p> 
    <p>Votre équipe de %sys_site_title%</p> 
";
$LNG['fake_free_review_confirm_mail_subj'] = "%sys_site_title% Confirmation d'évaluation 100% Réele.";
$LNG['fake_photo'] = "Photo pas réelle!";
$LNG['fax'] = "Télécopieur";
$LNG['female'] = "Femme";
$LNG['ff_guests'] = "<b>SEULEMENT %DAYS% JOURS</b><br>avant le tirage!</td></tr> <tr><td style='color:#000000;'><a href='%LINK%' rel='nofollow'>ENREGISTREZ VOUS</a> comme Membre Privilège - et courrez la chance de baiser<br><a href='%LINK%' rel='nofollow'><span style='COLOR: red;'><b><u>GRATUITEMENT VOTRE ESCORTE FAVORITE!</u></b></span></a>*</td></tr>";
$LNG['ff_guests_today'] = "<b>AUJOURD'HUI</b><br> Nous allons annoncer l'heureux Gagnant du Mois de <B>Loterie Baise Gratuite</B>! C'est peut-^tre VOUS? Tentez votre chance et</td></tr> <tr><td style='color:#000000;'><a href='%LINK%' rel='nofollow'>ENREGISTREZ VOUS</a> comme Membre Privilège - pour être du tirage ! *</td></tr>";
$LNG['ff_nonpaid'] = "<b>SEULEMENT %DAYS% JOURS</b><br>avant le tirage!</td></tr> <tr><td style='color:#000000;'><a href='%LINK%' rel='nofollow'>AMÉLIOREZ VOTRE ADHÉSION</a> comme Membre Privilège - et courrez la chance de baiser <br><a href='%LINK%' rel='nofollow'><span style='COLOR: red;'><b><u>GRATUITEMENT VOTRE ESCORTE FAVORITE!</u></b></span></a>*</td></tr>";
$LNG['ff_nonpaid_today'] = "<b>AUJOURD'HUI</b><br> Nous allons annoncer l'heureux Gagnant du Mois de <B>Loterie Baise Gratuite</B>! C'est peut-être VOUS? Tentez votre chance et</td></tr> <tr><td style='color:#000000;'><a href='%LINK%' rel='nofollow'>AMÉLIOREZ VOTRE ADHÉSION COMME MEMBRE PRIVIL�?GE</a> pour être du tirage ! *</td></tr>";
$LNG['ff_paid'] = " <b>SEULEMENT %DAYS% JOURS</b><br>avant le tirage !</td></tr>";
$LNG['ff_paid_today'] = " <b>AUJOURD'HUI</b><br>Nous allons annoncer l'heureux Gagnant du Mois de <B>Loterie Baise Gratuite</B>! C'est peut-être VOUS?</td></tr>";
$LNG['file_uploaded'] = "Photo envoyée avec succès.";
$LNG['finish_reg'] = "Terminer l'enregistrement";
$LNG['first_name'] = "Prénom";
$LNG['fluent'] = "Fluent";
$LNG['forgot_email_1'] = "Cher %USER%, vous avez demandé un rappel de votre Mot de Passe à l'aide du formulaire 'Mot de Passe oublié' sur %SITE%.";
$LNG['forgot_email_2'] = "Voici vos informations de branchement :";
$LNG['forgot_email_subject'] = "%sys_site_title% demande de Mot de Passe";
$LNG['forgot_passwd'] = "Mot de Passe oublié?";
$LNG['forgot_password'] = "Mot de Passe oublié";
$LNG['free'] = "Gratuit";
$LNG['free_fuck_desc'] = "À la fin de chaque mois, nous allons piger au hasard un Membre Privilège chanceux qui pourra baiser gratuitement n'importe quelle escorte de son choix sur %sys_site_title%. Nous paieront toutes les dépenses de l'escorte pour vous !<BR /> <BR /> Félicitations à tous nos gagnants! Nous espérons que vous avez passé un moment mémorable en compagnie de votre escorte préférée... !";
$LNG['free_fuck_lotto'] = "Loterie Baise Gratuite";
$LNG['free_fuck_note'] = "Note: Valeur jusqu'à concurrence de 300 Euros ou rencontre d'une heure. Si vous choisissez une demoiselle à 250 Euros, il n'y aura pas de remboursement de 50 Euros. Ce prix n'est pas monnayable ni échangeable. Les détails seront envoyés au gagnant.";
$LNG['free_mb_desc'] = "Votre profil est actuellement affiché sur le bas des dernières pages, qui a comme conséquence le bas nombre des Membres capables le voir. Améliorez votre adhésion maintenant et obtenez:";
$LNG['free_mb_desc_ag'] = "%COUNT% escortes - <a href='/private/upgrade_escort.php'>Améliorez Maintenant</a> afin de les faire connaître aux clients !";
$LNG['friday'] = "Vendredi";
$LNG['friendly'] = "Amicale";
$LNG['from'] = "de";
$LNG['fuckometer'] = "Baiso-mètre";
$LNG['fuckometer_desc'] = "Le Baiso-Mètre est une estimation indiquant l'exécution de l'escorte basée sur les évaluations. Il est employé pour signaliser les meilleures escortes en action.";
$LNG['fuckometer_range'] = "Échelle Baiso-Mètre";
$LNG['fuckometer_rating'] = "Évaluation Baiso-Mètre";
$LNG['fuckometer_short'] = "BM";
$LNG['fuckometer_toplist'] = "Fuckometer toplist";
$LNG['full_name'] = "Nom au complet";
$LNG['gays'] = "Les gays";
$LNG['gender'] = "Sexe";
$LNG['genuine_photo'] = "Photo 100% réelle";
$LNG['get_trusted_review'] = "Évaluation de confiance";
$LNG['get_trusted_review_desc'] = "Recevez une évaluation 100% digne de confiance! Svp remplir le formulaire ci-dessous!<br /> 
Comment ça fonctionne ? <a href='%LINK%'>cliquez ici</a>!";
$LNG['girl'] = "Escorte";
$LNG['girl_booked'] = "Vous avez réservée %NAME% pour le %DATE%.";
$LNG['girl_no_longer_listed'] = "Cette escorte n’est plus listée sur 
%SITE%";
$LNG['girl_of'] = "Demoiselle de";
$LNG['girl_of_the_month_history'] = "Historique Copine du Mois";
$LNG['girl_on_tour_1'] = "Escorte en tournée à %CITY% - %COUNTRY%";
$LNG['girl_on_tour_2'] = "Depuis %FROM_DATE% jusqu'à %TO_DATE%";
$LNG['girl_on_tour_3'] = ": %PHONE%";
$LNG['girl_on_tour_4'] = ": %EMAIL%";
$LNG['girl_origin'] = "Origine de l'escorte";
$LNG['girls_in_italy'] = "Toutes les escortes listées ici sont maintenant en %sys_app_country_title%! Reservez-en une MAINTENANT!";
$LNG['girls_international'] = "Escortes disponible à l'international, veuillez noter que si vous désirez en rencontrer une en %sys_app_country_title%, vous devez dans la plupart des cas la réserver pour au moins 12 heure!";
$LNG['go_to_escort_profile'] = "Voir ce profil";
$LNG['go_to_reviews_overview'] = "Aller aux évaluations";
$LNG['gray'] = "Gris";
$LNG['green'] = "Verts";
$LNG['hair'] = "Cheveux";
$LNG['hair_color'] = "Couleur de cheveux";
$LNG['hard_to_believe_its_her'] = "Difficile de croire que c'est elle";
$LNG['hard_to_book'] = "Difficile de réserver";
$LNG['height'] = "Taille";
$LNG['help_system'] = "Aide";
$LNG['help_watch_list'] = "Vous pouvez ajouter n'importe quel usager, escorte ou agence à votre Liste de Favoris. Cette liste est utilisée à des fins de notification courriel lors d'événements spéciaux. 
Si une <b>escorte</b> est sur votre liste, vous recevrez ces alertes couriels: 
<ul> 
<li>'Escorte' a reçu une nouvelle évaluation</li> 
<li>'Escorte' est partie en tournée</li> 
<li>'Escorte' est partie en ou est de retour de vacances</li> 
</ul> 
Si vous avez une <b>Agence</b> sur votre liste, vous recevrez ces alertes couriels: 
<ul> 
<li>'Agence' a ajouté une nouvelle escorte</li> 
</ul> 
Si un/une <b>Membre</b> est sur votre liste, vous recevrez ces alertes couriels: 
<ul> 
<li>'Membre' a soumis une nouvelle évaluation</li> 
</ul> 
Vous pouvez avoir autant d'items que vous le désirez sur votre Liste de Favioris. 
Pour gérer facilement votre liste, cliquez sur \"Liste de Favoris\" dans le menu une fois que vous êtes branché.";
$LNG['hip'] = "Hanches";
$LNG['home'] = "Accueil";
$LNG['home_page'] = "Accueil";
$LNG['hour'] = "heure";
$LNG['hours'] = "heures";
$LNG['howto_set_main'] = "Pour définir une photo maîtresse, cliquez dessus.";
$LNG['i_agree_with_terms'] = "J'ai lu et j'accepte les conditions générales";
$LNG['in'] = "à";
$LNG['in_face'] = "Sur le visage";
$LNG['in_mouth_spit'] = "Dans la bouche mais n'avale pas";
$LNG['in_mouth_swallow'] = "Dans la bouche et avale";
$LNG['incall'] = "Reçoit";
$LNG['independent'] = "Indépendant";
$LNG['independent_escorts_from'] = "Escorte Indépendante de";
$LNG['independent_girls'] = "Indépendantes";
$LNG['info'] = "Info";
$LNG['intelligent'] = "Intelligente";
$LNG['international'] = "International";
$LNG['international_directory'] = "International";
$LNG['invalid_birth_date'] = "Date de naissance invalide";
$LNG['invalid_curr_password'] = "Mot de Passe actuel Incorrect";
$LNG['invalid_desired_date'] = "Date désirée invalide";
$LNG['invalid_discount_code'] = "Votre code d'escompte est pas valide.";
$LNG['invalid_email'] = "Courriel invalide";
$LNG['invalid_email_info'] = "Notre systeme a identifié votre adress email comme non valide. Il n'est pas possible de vous envoyer un email. Votre compte a été temporairement bloqué. Merci de nous transmettre une adresse email valide. Nous vous enverrons un email de vérification ainsi qu'un lien de confirmation. Apres reception de cet email et validation en cliquant sur le lien, votre compte fonctionnera de nouveau. <br /><br /> Merci pour votre compréhension.";
$LNG['invalid_file_type'] = "Type de fichier invalide. Seuls les fichiers JPG sont acceptés.";
$LNG['invalid_meeting_date'] = "Date de rencontre invalide";
$LNG['invalid_showname'] = "Invalid showname, please use only alphanumeric characters.";
$LNG['invalid_user_type'] = "Type d'usager invalide";
$LNG['invitation'] = "Bienvenue sur notre Service d'escorte! Svp, choisissez une ville où vous recherchez une escorte.";
$LNG['invitation_1'] = "Dans les pages suivantes, vous trouverez les meilleures escortes, clubs privés et agences d'escorte dans votre région - %sys_app_country_title% et international. Vous trouverez les adresses de contact et photos pour chacune des demoiselles.";
$LNG['invitation_2'] = "Nous vous souhaitons la bienvenue sur notre site Internet. Pour toute question ou information, n'hésitez pas à nous contacter : <a href='mailto:%sys_contact_email%'>%sys_contact_email%</a>";
$LNG['joined'] = "Joined";
$LNG['kiss'] = "Embrasse";
$LNG['kiss_with_tongue'] = "Embrasse avec la langue (French Kiss)";
$LNG['kissing'] = "Embrasser";
$LNG['kissing_with_tongue'] = "Embrassade avec la langue (french kiss)";
$LNG['language'] = "Langues";
$LNG['language_problems'] = "Problèmes de Language";
$LNG['languages'] = "Langues";
$LNG['languages_changed'] = "Langues changées avec succès";
$LNG['last'] = "Dernier";
$LNG['last_name'] = "Nom de Famille";
$LNG['latest_10_reviews'] = "Les 10 dernières évaluations des escortes";
$LNG['latest_10_third_party_reviews'] = "Les 10 dernières évaluations";
$LNG['level'] = "Niveau";
$LNG['limitrofi'] = "Limitrofi";
$LNG['links'] = "Liens";
$LNG['loano'] = "Loano";
$LNG['locations_changed'] = "Régions changées avec succès";
$LNG['logged_in'] = "Vous êtes branché sous: ";
$LNG['login'] = "Se connecter";
$LNG['enter'] = "Se connecter";
$LNG['login_and_password_alphanum'] = "Les Nom d'Usager et Mot de Passe ne peuvent contenir que des caractères alpha numériques (0-9, a-z, A-Z, _).";
$LNG['login_and_signup_disabled'] = "At this moment we are performing scheduled maintenance. No new registrations and site logins are accepted. Please come back later. We are sorry for the inconvenience.";
$LNG['login_exists'] = "Ce Nom d'Usager existe déjà. Svp choisissez en un autre...";
$LNG['login_problems_contact'] = "Si vous avez encore des problèmes, n'hésitez pas à communiquer avec nous : <a href='mailto:%sys_contact_email%'>%sys_contact_email%</a>";
$LNG['logout'] = "Sortir";
$LNG['looking_for_girls_in_your_city'] = "Vous recherchez des filles (escorts) dans votre ville?";
$LNG['looking_for_girls_in_your_region'] = "Looking for girls in your region?";
$LNG['looks'] = "Apparence";
$LNG['looks_and_services'] = "Apparence et Services";
$LNG['looks_range'] = "Échelle apparence";
$LNG['looks_rating'] = "Évaluation Apparence";
$LNG['lower_date'] = "La date À ne peut être avant la date DE dans l'échelle de date";
$LNG['lower_fuckometer'] = "Échelle Baiso-Mètre est incorrecte";
$LNG['lower_looks_rating'] = "Échelle apparence est incorrecte";
$LNG['lower_services_rating'] = "Échelle services est incorrecte";
$LNG['male'] = "Homme";
$LNG['meeting_costs'] = "Prix de la rencontre";
$LNG['meeting_date'] = "Date de rencontre";
$LNG['meeting_date_range'] = "Échelle dates de rencontre";
$LNG['meeting_dress_comments'] = "Info supplémentaires (Endroit, habillement, commentaires, etc...)";
$LNG['meeting_length'] = "Durée de la rencontre";
$LNG['member'] = "Membre";
$LNG['member_name'] = "Nom du membre";
$LNG['member_profit_01'] = "Soyez un Membre Plein Privilèges du forum et prtagez vos commentaires et expériences";
$LNG['member_profit_02'] = "Écrivez des évaluations à propos des escortes";
$LNG['member_profit_03'] = "Soyez informé à propos des nouvelles escortes sur %SITE%";
$LNG['member_profits'] = "En tant que membre, vous avez accès à";
$LNG['membership_change_subject'] = "Changement d'abonnement sur %sys_site_title%";
$LNG['membership_change_to_free_mail'] = " 
    <p>Cher %LOGIN%,<br /> 
    Votre abonnemnt en tant que \"Membre Privilège\" de %sys_site_title% est expiré ainsi que les avantages qui s'y rattachent. Vous êtes maintenant un membre \"Gratuit\".</p> 
    <p>Si vous souhaitez renouveller votre adhésion, Svp faites-le à partir du site Internet ou contactez-nous par courriel.</p> 
    <p>Améliorez votre adhésion et recevez :</p> 
    <ul> 
        <li>Tarifs escomptés pour rencontre avec toutes les filles listées <a href='%LINK%'>ICI</a></li> 
        <li>Votre chance à tous les mois de gagner a notre \"Loterie Baise Gratuite\" où vous courrez la chance de baiser la fille de votre choix à nos frais!</li> 
        <li>Accès au Babillar Privilège</li> 
        <li>Messages Privés</li> 
        <li>Liste de Favoris pour Escortes, Usagers et Agences</li> 
        <li>Recherche Avancée</li> 
        <li>Pas d'annonces</li> 
   </ul> 
    <p>Pour toute question ou information, n'hésitez pas à nous contacter. Nous répondrons promptement</p> 
    <p>Bien à vous<br /> 
    L'équipe de %sys_site_title%</p> 
";
$LNG['membership_change_to_premium_mail'] = " 
    <p>Cher %LOGIN%,<br /> 
Nous avons traitée avec succès votre demande d'abonnemnt en tant que \"Membre Privilège\" de %sys_site_title%. Merci de votre confiance.</p> 
    <p>Voici vos informations de branchement:<br /> 
    Nom d'usager: %LOGIN%</p> 
    <p>(Svp Gardez cette information dans un endroit sûr)</p> 
    <p>Encore une fois, vos avantages en tant que *Membre Privilège* sont:</p> 
    <ul> 
        <li>Tarifs escomptés pour rencontre avec toutes les filles listées <a href='%LINK%'>ICI</a></li> 
        <li>Votre chance à tous les mois de gagner a notre \"Loterie Baise Gratuite\" où vous courrez la chance de baiser la fille de votre choix à nos frais!</li> 
        <li>Accès au Babillar Privilège</li> 
        <li>Messages Privés</li> 
        <li>Liste de Favoris pour Escortes, Usagers et Agences</li> 
        <li>Recherche Avancée</li> 
        <li>Pas d'annonces</li> 
    </ul> 
    <p>Pour toute question ou information, n'hésitez pas à nous contacter. Nous répondrons promptement</p> 
    <p>Bien à vous<br /> 
    L'équipe de %sys_site_title%</p> 
";
$LNG['membership_type'] = "Type d'abonnement";
$LNG['membership_types'] = "Types d'adhésion";
$LNG['men'] = "Les hommes";
$LNG['menu'] = "Menu";
$LNG['message'] = "Message";
$LNG['message_for'] = "Message pour %NAME%";
$LNG['message_not_sent'] = "Echec lors de l'envoi du message.";
$LNG['message_sent'] = "Votre message a été envoyé à %NAME%.";
$LNG['metric'] = "Système Métrique";
$LNG['metric_desc'] = "centimetres, kilogrames, ...";
$LNG['middle_name'] = "Initiale";
$LNG['minutes'] = "minutes";
$LNG['modified'] = "Modifié le";
$LNG['modify_escort'] = "Éditer escorte";
$LNG['monday'] = "Lundi";
$LNG['month'] = "mois";
$LNG['month_1'] = "Janvier";
$LNG['month_10'] = "Octobre";
$LNG['month_11'] = "Novembre";
$LNG['month_12'] = "Décembre";
$LNG['month_2'] = "Février";
$LNG['month_3'] = "Mars";
$LNG['month_4'] = "Avril";
$LNG['month_5'] = "Mai";
$LNG['month_6'] = "Juin";
$LNG['month_7'] = "Juillet";
$LNG['month_8'] = "Août";
$LNG['month_9'] = "Septembre";
$LNG['months'] = "mois";
$LNG['more_info'] = "Plus d'Info...";
$LNG['more_new_girls'] = "More new girls";
$LNG['more_news'] = "Plus de Nouvelles...";
$LNG['multiple_times_sex'] = "Plusieurs relations sexuelles";
$LNG['my_escort_profile'] = "Mon profil d'escorte";
$LNG['my_escorts'] = "Mes escortes";
$LNG['my_languages'] = "Langues parlées";
$LNG['my_locations'] = "Mes régions de travail";
$LNG['my_new_review_notify_agency'] = "Soyez informé par courriel si une de vos escorte reçoit une nouvelle évaluation.";
$LNG['my_new_review_notify_single'] = "Soyez informé par courriel si  vous recevez une nouvelle évaluation.";
$LNG['my_photos'] = "Mes photos";
$LNG['my_rates'] = "Mes tarifs";
$LNG['my_real_profile'] = "Mon profil réel";
$LNG['my_reviews'] = "Mes évaluations";
$LNG['name'] = "Nom";
$LNG['name_of_the_lady'] = "Nom de l'escorte";
$LNG['name_of_the_site'] = "Nom du site";
$LNG['ethnic'] = "Groupe ethnique";
$LNG['nationality'] = "Nationalité";
$LNG['need_help'] = "Besoin d'aide ?";
$LNG['need_to_be_registered_to_review'] = "Vous devez être enregistré à %SITE% pour écrire une évaluation sur une demoiselle, <a href='%LINK%'>Enregistrement GRATUIT!</a>";
$LNG['never'] = "jamais";
$LNG['new'] = "Nouveau";
$LNG['new_arrivals'] = "Dernière inscrite";
$LNG['new_email'] = "Nouvel Email";
$LNG['new_entries'] = "Nouvelles entrées";
$LNG['new_escort_email_by_agency_body'] = "<pre> 
L'Agence \"%AGENCY%\" a ajouté une nouvelle escorte: %ESCORT% ! 
Vous recevez ce courriel parce que vous avez cette Agence dans votre Liste de Favoris. 
Cliquez sur ce lien : 
<a href=\"%LINK%\">%LINK%</a> pour voir la fiche de cette escorte. 
L'équipe de %sys_site_title% 
</pre>";
$LNG['new_escort_email_by_agency_subject'] = "L'Agence %AGENCY% a ajouté une nouvelle escorte !";
$LNG['new_password'] = "Nouveau Mot de Passe";
$LNG['new_password_empty_err'] = "Nouveau Mot de Passe ne peut être vide";
$LNG['new_review_email_agency_body'] = "<pre> 
Le Membre %USER% a posté une nouvelle évaluation de votre escorte %ESCORT% ! 
Cliquez sur ce lien : 
<a href=\"%LINK%\">%LINK%</a> pour lire l'évaluation. 
L'équipe de %sys_site_title% 
</pre>";
$LNG['new_review_email_agency_subject'] = "Votre escorte une nouvelle évaluation!";
$LNG['new_review_email_by_escort_body'] = "<pre> 
L'escorte %ESCORT% a reçu une nouvelle évaluation du Membre %USER% ! 
Vous recevez ce courriel parce que vous avez cette escorte dans votre Liste de Favoris. 
Cliquez sur ce lien : 
<a href=\"%LINK%\">%LINK%</a> pour lire l'évaluation. 
L'équipe de %sys_site_title% 
</pre>";
$LNG['new_review_email_by_escort_subject'] = "%ESCORT% a reçu une nouvelle évaluation !";
$LNG['new_review_email_by_user_body'] = "<pre> 
Le Membre %USER% a posté une nouvelle évaluation sur l'escorte %ESCORT% ! 
Vous recevez ce courriel parce que vous avez ce Membre dans votre Liste de Favoris. 
Cliquez sur ce lien : 
<a href=\"%LINK%\">%LINK%</a> pour lire l'évaluation. 
L'équipe de %sys_site_title% 
</pre>";
$LNG['new_review_email_by_user_subject'] = "Le Membre %USER% a posté une nouvelle évaluation !";
$LNG['new_review_email_escort_body'] = "<pre> 
Le Membre %USER% a posté une nouvelle évaluation de vous ! 
Cliquez sur ce lien : 
<a href=\"%LINK%\">%LINK%</a> pour lire l'évaluation. 
L'équipe de %sys_site_title% 
</pre>";
$LNG['new_review_email_escort_subject'] = "Vous avez une nouvelle évaluation!";
$LNG['news'] = "Nouvelles";
$LNG['newsletter'] = "Bulletin de Nouvelles";
$LNG['next'] = "Suivant";
$LNG['next_step'] = "Étape suivante";
$LNG['no'] = "Non";
$LNG['no_blowjob'] = "Pas d'Oral";
$LNG['no_cumshot'] = "Pas d'éjaculation";
$LNG['no_esc_sel_err'] = "Vos devez sélectionner une escorte";
$LNG['no_escort_languages'] = "Vous n'avez pas de langues définie. <a href='%LINK%'>Cliquez-ici</a> pour en choisir une.";
$LNG['no_escort_main_pic'] = "Vous n'avez pas de photo maîtresse définie. <a href='%LINK%'>Cliquez-ici</a> pour en choisir une.";
$LNG['no_escort_name_languages'] = "%ESCORT% n'a pas de langues définie. <a href='%LINK%'>Cliquez-ici</a> pour en choisir une.";
$LNG['no_escort_name_main_pic'] = "%ESCORT% n'a pas de photo maîtresse définie. <a href='%LINK%'>Cliquez-ici</a> pour en choisir une.";
$LNG['no_escort_name_photos'] = "L'escorte %ESCORT% n'a aucune photo valide. Svp <a href='%LINK%'>Cliquez-ici</a> pour mettre à jour votre gallerie.";
$LNG['no_escort_photos'] = "Vous n'avez aucune photo valide dans votre profil. Svp <a href='%LINK%'>Cliquez-ici</a> pour mettre à jour votre gallerie.";
$LNG['no_escort_profile'] = "Vous n'avez pas de profil d'escorte actif. <a href='%LINK%'>Cliquez-ici</a> pour en ajouter un.";
$LNG['no_escorts_found'] = "Aucune escorte trouvée";
$LNG['no_escorts_profile'] = "Vous n'avez aucun profil d'escorte défini. Svp <a href='%LINK%'>Cliquez-ici</a> pour en ajouter un.";
$LNG['no_kiss'] = "n'embrasse pas";
$LNG['no_kissing'] = "Pas d'embrassade";
$LNG['no_only_once'] = "Non, seulement qu'une";
$LNG['no_premium_escorts'] = "N'est pas escorte premium.";
$LNG['no_reviews_found'] = "Pas d'évaluation trouvées";
$LNG['no_sex_avail_chosen'] = "You have to select at least 1 sexual availability";
$LNG['no_users_found'] = "Pas de Membre trouvé";
$LNG['no_votes'] = "Pas de votes";
$LNG['norm_memb'] = "1 Mois d'adhésion régulière pour 50 Euros";
$LNG['normal'] = "Normal";
$LNG['normal_mb_desc_ag'] = "%COUNT% escortes (La première expire dans %DAYS% jours)";
$LNG['normal_review'] = "Normale";
$LNG['normal_review_desc'] = "Évaluation normale veut dire : L'agence/escorte n'ont pas répondu aux sms/couriels ainsi l'évaluation n'est pas confirmée mais n'est pas également niée! Ceci signifie que celle-ci peut être considérée comme vraie!";
$LNG['not_approved'] = "Non approuvé";
$LNG['not_available'] = "Not available";
$LNG['not_logged'] = "Pas Branché";
$LNG['not_logged_desc'] = "Vous devez être branché pour utiliser cette fonction.";
$LNG['not_verified_msg'] = " 
<p>Votre compte n'e pas encore été vérifié.<br /> Afin d'être un membre à part entière de %sys_site_title% vous devez vérifier votre adresse de courriel. Si vous n'avez toujours pas reçu votre message d'activation, alors Svp <a href=\"%LINK%\">cliquez ici pour renvoyer votre demande d'activation</a>.</p> 
<p>Note: veuillez vérifier également votre \"Dossier Pourriels (spam)\", malheureusement certains fournisseurs mettent nos courriers dans celui-ci. Merci pour votre compréhension.</p>";
$LNG['not_yet_member'] = "Pas encore Membre? Enregistrez-vous, C'est Gratuit!";
$LNG['not_yet_premium_signup_now'] = "Vous n'êtes pas un Membre Privilège ? <a href='%LINK%'>ENREGISTREZ VOUS</a> et sauvez beaucoup d'argent!";
$LNG['note'] = "Note";
$LNG['notify_comon'] = "<pre>Vous recevez ce courriel parce que vous êtes enregistré à %sys_site_title% et avez choisi de recevoir ces alertes courriels. 
Pour changer vos préférences, Svp branchez-vous à votre compte et cliquez sur \"Préférences\" dans le menu.</pre>";
$LNG['now'] = "MAINTENANT";
$LNG['number'] = "No.";
$LNG['number_of_reviews'] = "Nombre d'évaluations";
$LNG['offers_discounts'] = "Escompte offert";
$LNG['on_tour_in'] = "en tour à";
$LNG['only'] = "seulement";
$LNG['only_fake_free_reviews'] = "Seulement les évaluations 100% vrai";
$LNG['only_for_premium_members'] = "only for premium members";
$LNG['only_girls_with_reviews'] = "Seulement les escortes avec évaluations";
$LNG['only_reviews_from_user'] = "Évaluations par des membresseulement";
$LNG['only_reviews_not_older_then'] = "Seulement évaluation plusrécentes que";
$LNG['optional'] = "optionel";
$LNG['oral_without_condom'] = "Sucer sans capote";
$LNG['order_premium_spot_now'] = "Commandez votre espace Privilège Maintenant!";
$LNG['origin'] = "";
$LNG['origin_escortforum'] = "Forum Escortes";
$LNG['origin_non_escortforum'] = "Forum Non-Escortes";
$LNG['other'] = "Autre";
$LNG['other_boys_from'] = "Autres escortes de";
$LNG['other_escorts_from'] = "Autres escortes de";
$LNG['boys_from'] = "Escortes de";
$LNG['escorts_from'] = "Escortes de";
$LNG['outcall'] = "Déplace";
$LNG['overall_rating'] = "Evaluation générale";
$LNG['partners'] = "partenaires";
$LNG['passive'] = "Passive";
$LNG['passwd_has_been_sent'] = "Votre Mot de Passe vous a été envoyé à %EMAIL%";
$LNG['passwd_succesfuly_sent'] = "Votre Mot de Passe vous a été envoyé par courriel.";
$LNG['password'] = "Mot de Passe";
$LNG['password_changed'] = "Le Mot de Passe a été changé avec succès";
$LNG['password_invalid'] = "Mot de Passe invalide. Il doit contenir au moins 6 caractères.";
$LNG['password_missmatch'] = "Les Mots de Passe ne concordent pas";
$LNG['percent_discount'] = "Escompte de %DISCOUNT%%";
$LNG['phone'] = "Téléphone";
$LNG['phone_instructions'] = "Consignes pour le téléphone ";
$LNG['photo_correct'] = "Photo correct";
$LNG['photos'] = "Photos";
$LNG['pic_delete_failed'] = "L'effacement de la photo a échoué";
$LNG['pic_deleted'] = "Photo effacée avec succès.";
$LNG['place'] = "Place";
$LNG['please_choose_services'] = "Svp choisir les services reçus par la demoiselle; Cela a une influence sur son Baiso-Mètre! Merci de votre collboration!";
$LNG['please_fill_form'] = "Please fill out the following form";
$LNG['please_provide_email'] = "Svp entrez l'adresse de courriel utlisée lors de votre enregistrement à %SITE%";
$LNG['please_select'] = "Svp Choisir...";
$LNG['plus2'] = "2+";
$LNG['poshy'] = "Hautaine";
$LNG['posted_reviews'] = "Jusqu'à maintenant, vous avez posté %NUMBER% évaluations.";
$LNG['posted_to_forum'] = "Jusqu'à maintenant, vous avez posté %NUMBER% messages sur le forum.";
$LNG['premium'] = "Privilège";
$LNG['premium_escorts'] = "Escortes Privilège";
$LNG['premium_mb_desc_ag'] = "%COUNT% escortes (La première expire dans %DAYS% jours)";
$LNG['premium_user'] = "Membre Privilège";
$LNG['previous'] = "Précédent";
$LNG['price'] = "Prix";
$LNG['priv_apart'] = "Appartement privé";
$LNG['private_menu'] = "Menu Privé";
$LNG['problem_report'] = "Rapportez un problème";
$LNG['problem_report_back'] = "Cliquez ici pour retourner au profil de cette escorte";
$LNG['problem_report_no_accept'] = "Merci, votre rapport de problème a été accepté";
$LNG['problem_report_no_name'] = "Svp. Entrez votre nom";
$LNG['problem_report_no_report'] = "Svp. Entrez votre Rapport de problème";
$LNG['prof_city_expl'] = "Ville d'origine de l'escorte/Modèle, pas nécéssairement sa localité de travail. Les Régions de travail peuvent êtres définies lors des prochaines étapes.";
$LNG['prof_country_expl'] = "Pays d'origine de l'escorte/Modèle, pas nécéssairement sa localité de travail. Les Régions de travail peuvent êtres définies lors des prochaines étapes.";
$LNG['profile'] = "Profil";
$LNG['profile_changed'] = "Profil changé avec succès";
$LNG['promote_this_girl'] = "Promouvoir cette fille";
$LNG['promote_yourself'] = "Annoncez-Vous";
$LNG['provided_services'] = "Services rendus";
$LNG['publish_date'] = "Date de publication";
$LNG['rank'] = "Place ";
$LNG['rate'] = "Tarif";
$LNG['rate_looks'] = "Évaluez son apparence";
$LNG['rate_services'] = "Évaluez ses services";
$LNG['rates'] = "Tarifs";
$LNG['rates_changed'] = "Tarifs changés avec succès";
$LNG['rates_deleted'] = "Tarifs effacés avec succès";
$LNG['rating'] = "Évaluation";
$LNG['re_verify_sent'] = "Nous vous avons renvoyé le message d'activation. Veuillez vérifier votre courriel et suivez les instructions incluses dans celui-ci.";
$LNG['read_this_review'] = "Lire cette évaluation";
$LNG['read_whole_article'] = "Lire l'article au complet";
$LNG['real'] = "Réels";
$LNG['real_data_form_text'] = "Svp, fournir de l'information réelle. 
Gardez à l'esprit que cette information ne sera pas incluse dans votre 
profil et ne sera affichée nulle part sur le site. Elle sera gardée de 
façon strictement confidentielle et servira à l'usage interne 
seulement.";
$LNG['real_name'] = "Nom Réel";
$LNG['recalibrate_now'] = "Recalibrer Maintenant!";
$LNG['receive_newsletter'] = "Recevez le Bulletin de Nouvelles de %sys_site_title%";
$LNG['red'] = "Rousse";
$LNG['reference'] = "Comment nous avez-vous trouvé?";
$LNG['regards'] = "A bientôt, l'équipe de %SITE%.";
$LNG['region'] = "Région";
$LNG['register'] = "Enregistrez-Vous";
$LNG['registration'] = "Inscription";
$LNG['rem_watch'] = "Effacer de votre liste de Favoris";
$LNG['rem_watch_success'] = "<b>%NAME%</b> a été EFFACÉE avec succès à votre liste de Favoris<a href='javascript:history.back()'>Cliquez ici pour retourner.</a>";
$LNG['remove'] = "Enlever";
$LNG['renew_now'] = "Renouveler Maintenant !";
$LNG['required_fields'] = "*Les champs obligatoires sont identifiés par un *";
$LNG['rev_resume_title'] = "";
$LNG['review'] = "Évaluation";
$LNG['review_changed'] = "Évaluation changée avec succès";
$LNG['review_guide_and_rules'] = "<strong>Guide and Rules:</strong><br /> 
Nous vous demandons de respecter ces règles, Svp ne pas faire de fausses évaluations! Chaque évaluation vous aide vous et tous les membres à recevoir de meilleurs services! 
<ul> 
<li>Svp spécifiez tous les services offerts par l'escorte</li> 
<li>Ajoutez de nouvelles escortes à la base de donnée, si vous ne trouvez pas une escorte en particulier, ajoutez la à notre base de données</li> 
<li>Svp donnez une description de votre expérience avec elle, les évaluations trop courtes comme \"elle est super\" seront effacées</li> 
<li>Svp, ne pas évaluer une escorte plus haut que 8.0 si elle n'offre pas au moins: 
	<ul> 
	 <li>Sexe Oral sans condom</li> 
	 <li>Attitude sexy et enthousiaste</li> 
	 <li>Embrasse avec la langue (french kiss)</li> 
	</ul> 
</ul>";
$LNG['review_language'] = "Langue de évaluations";
$LNG['review_sms'] = "Vous êtes passés en revue sur %sys_site_title%! Répondez nous simplement par sms avec: oui, pour vrai et non, pour faux! Dans 2 sms vous obtenez les petits groupes de votre client!";
$LNG['review_status'] = "Status de l'Évaluation";
$LNG['review_this_girl'] = "Évaluer cette escorte";
$LNG['reviews'] = "Évaluations";
$LNG['reviews_to_recalibrate'] = " 
Cher %NAME%,<br /> 
Vous avez %REVIEWS% évaluations à recalibrer. Svp aidez-nous à recalibrer toutes vos évaluations en fournissant des informations plus détaillées sur la dame que vous avez rencontrée.";
$LNG['rouen'] = "Rouen";
$LNG['royal'] = "Système Impérial";
$LNG['royal_desc'] = "pouces, pieds, ...";
$LNG['rss_channel_desc'] = "Newest escorts";
$LNG['salon'] = "Boîte de Nuit";
$LNG['sardegna'] = "Sardegna";
$LNG['saturday'] = "Samedi";
$LNG['sauna'] = "Sauna";
$LNG['save_changes'] = "Sauvegarder les changements";
$LNG['score'] = "Score";
$LNG['search'] = "Recherche";
$LNG['search_desc'] = "Bienvenue à l'outil de recherche simple de %sys_site_title%. Pour plus d'option de recherche, cliquez sur <a href='%LINK%'>Recherche Avancée</a>.";
$LNG['search_desc2'] = "Bienvenue à l'outil de recherche avancé de %sys_site_title%. pour moins d'option de recherche, cliquez sur <a href='%LINK%'>Recherche Simple</a>.";
$LNG['search_escorts'] = "Chercher";
$LNG['search_girl'] = "";
$LNG['search_agency'] = "";
$LNG['search_name'] = "Nom de l'escorte/agence";
$LNG['search_reviews'] = "Recherche évaluation";
$LNG['search_reviews_back'] = "Retour au formulaire de recherche";
$LNG['search_reviews_no_params'] = "Svp, entrez au moins un critère de recherche";
$LNG['search_reviews_no_result'] = "Désolé, aucun résultat n'a été trouvé, Svp Éssayez encore";
$LNG['search_users'] = "Rechercher un Membre";
$LNG['see_escorts_soon_on_tour'] = "Cliquez ici pour voir les escorts qui passent bientôt en %sys_app_country_title%";
$LNG['see_independent_preview'] = "Aperçu de toutes les escortes indépendantes";
$LNG['select_as_main'] = "Voulez-vous définir cette photo comme photo maîtresse?";
$LNG['select_your_service_requirements'] = "Choisir les servicesrequis";
$LNG['send_message'] = "Envoyer le Message";
$LNG['service_comments'] = "Commentaires sur le service";
$LNG['services'] = "Services";
$LNG['services_range'] = "Échelle services";
$LNG['services_rating'] = "Évaluation Services";
$LNG['services_she_was_willing'] = "Les services qu'elle était prête à rendre";
$LNG['settings'] = "Paramètres";
$LNG['settings_update_error'] = "Erreur lors du changement des Préférences";
$LNG['settings_updated'] = "Préférences changées avec succès.";
$LNG['sex'] = "Pénétration";
$LNG['sex_available_to'] = "Sexually available to";
$LNG['shoe_size'] = "Chaussures";
$LNG['show_all_reviews'] = "Montrer toutes les évaluations";
$LNG['show_email'] = "Afficher le Courriel sur le site?";
$LNG['showname'] = "Nom de l'escorte";
$LNG['show_name'] = "Nom de d'escorte/Modèle";
$LNG['show_only_girls_on_tour_in'] = "Montre moi uniquement les filles en tour";
$LNG['show_site_reviews'] = "Montrer le évaluations de %SITE%";
$LNG['show_third_party_reviews'] = "Montrer les évaluations de tiers ";
$LNG['showname_exists'] = "Ce Nom d'escorte/modèle existe déjà, Svp choisissez-en un autre";
$LNG['sign_up'] = "Enregistrement";
$LNG['sign_up_for_free'] = "Inscription !";
$LNG['signup_activated_01'] = "Votre compte a été activé avec succès!";
$LNG['signup_activated_02'] = "Félicitations!<br /> 
Vous êtes maintenant Membre de %SITE%.<br /> 
Vos informations de branchement ont été envoyées à votre adresse de courriel.";
$LNG['signup_activated_03'] = "Cliquez-ici pour vous brancher.";
$LNG['signup_and_win'] = "Signup today as a Premium Member and <strong>WIN A FREE FUCK OF THE VALUE OF 300 EURO!</strong>";
$LNG['signup_confirmation_email'] = "<pre> 
Cher %USER%, 
Merci pour votre enregsitrement à %SITE%. Vous êtes maintenant Membre de %SITE%. 
Voici vos informations de branchement. Svp Gardez cette information dans un endroit sûr pour référence future : 
Nom d'Usager: %USER% 
Site: %SITE% 
Bienvenue! 
Nous avons hâte de lire vos commentaires et évaluations! 
l'Équipe de %SITE%.</pre>";
$LNG['signup_confirmation_email_subject'] = "Enregistrement à %SITE%";
$LNG['signup_error_email_missing'] = "Erreur: Vous n'avez pas entré d'adresse de Courriel. Svp Entrez un couriel valide.";
$LNG['signup_error_email_used'] = "Erreur: Cette adresse de courriel existe déjà sur %SITE%. Svp choisissez en une autre...";
$LNG['signup_error_login_missing'] = "Erreur; Vous n'avez pas entré de Nom d'Usager.";
$LNG['signup_error_login_used'] = "Erreur: Ce Nom d'Usager existe déjà. Svp choisissez en un autre...";
$LNG['signup_error_password_invalid'] = "Erreur: Mot de Passe invalide. Il doit contenir au moins 6 caratères.";
$LNG['signup_error_password_missing'] = "Erreur; Vous n'avez pas entré de mot de Passe.";
$LNG['signup_error_password_wrong'] = "Erreur: Les Mots de Passe ne concordent pas. Les Mots de Passe doivent être entrés exactement de la même manière dans chacun des champs.";
$LNG['signup_error_terms_missing'] = "vous devez accepter les termes et conditions afin d'ętre membre de notre site.";
$LNG['signup_successful_01'] = "Enregistrement comme Membre de %SITE% reussi.";
$LNG['signup_successful_02'] = "Merci pour votre inscription!";
$LNG['signup_successful_03'] = "Pour compléter votre enregistrement, vérifiez votre courriel.";
$LNG['signup_successful_04'] = "ATTENTION:<br/>
<small>Veuillez vï¿½rifier votre 'dossier pourriels' de votre boï¿½te si vous n'obtenez aucun couriels, dans certains cas il peut y avoir un dï¿½lai de quelques heures avant que vous ne le receviez.</small>";
$LNG['signup_verification_email'] = "<pre> 
Cher %USER%, 
Merci pour votre enregistrement à %SITE%. 
Pour confirmer votre enregistrement, vous devez d'abord cliquer sur le 
lien ci-dessous pour activer votre compte. 
<a href=\"%LINK%\">%LINK%</a> 
Site Internet: %SITE% 
l'Équipe de %SITE%.</pre>";
$LNG['signup_verification_email_subject'] = "Enregistrement à %SITE%";
$LNG['silicon'] = "Silicone";
$LNG['single_girl'] = "Fille seule";
$LNG['sitemap'] = "Emplacement sur la carte";
$LNG['smoker'] = "Fumeur";
$LNG['somewhere_else'] = "Autre part";
$LNG['soon'] = "Bientôt";
$LNG['state'] = "État ou Province";
$LNG['step_1'] = "Étape 1";
$LNG['step_2'] = "Étape 2";
$LNG['submit'] = "Envoyer";
$LNG['subscribe'] = "Abonnement";
$LNG['successfuly_logged_in'] = "Vous êtes maintenant branché.";
$LNG['sucks'] = "Non recommendable";
$LNG['sunday'] = "Dimanche";
$LNG['swallow'] = "Avale";
$LNG['system_error'] = "Erreur Système, Svp contactez le service à la clientèle.";
$LNG['terms_and_conditions'] = "Termes et conditions";
$LNG['terms_text'] = "";
$LNG['terni'] = "Terni";
$LNG['text_of_message'] = "Texte du Message";
$LNG['thank_you'] = "Merci";
$LNG['thanks_for_registration'] = "Merci pour votre enregistrement! Pour terminer, Svp vérifiez votre courriel et suivez les instructions qui y sont incluses.";
$LNG['this_escort_has'] = "";
$LNG['third_party_escort_reviews'] = "Évaluation d'escortes tierce partie";
$LNG['thursday'] = "Jeudi";
$LNG['time'] = "Heure";
$LNG['title'] = "";
$LNG['to'] = "á";
$LNG['to_change_use_menu'] = "Pour changer/éditer vos données et préférences, Svp utilisez le menu privé sur le dessus.";
$LNG['today_new'] = "Today new";
$LNG['top20'] = "Copine du Mois";
$LNG['top20_desc'] = "Votez pour votre favorite et faites en \"La Copine du Mois\"! À chaque fin de mois, l'escorte avec le plus de votes sera la nouvelle Copine du Mois! 
<br /></br /> Votez MAINTENANT ! Les votes vont de 1 (pire) à 10 (meilleure) !";
$LNG['top_10_ladies'] = "Le TOP 10 des escortes";
$LNG['top_10_reviewers'] = "Le TOP 10 des lecteurs";
$LNG['top_30_ladies'] = "";
$LNG['top_30_reviewers'] = "";
$LNG['tour_add'] = "Ajouter tournée";
$LNG['tour_already'] = "Vous avez déjà une tournée durant cette période";
$LNG['tour_contact'] = "Contact de la tournée";
$LNG['tour_duration'] = "Durée de la tournée";
$LNG['tour_email'] = "Courriel de la tournée";
$LNG['tour_empty_country'] = "Pays ne peut être vide";
$LNG['tour_empty_from_date'] = "Date \"De\" ne peut être vide";
$LNG['tour_empty_to_date'] = "Date \"À\" ne peut être vide";
$LNG['tour_girl'] = "escorte/modèle de la tournée";
$LNG['tour_location'] = "Région de la tournée";
$LNG['tour_lower_date'] = "Date \"À\" ne peut être avant la Date \"De\"";
$LNG['tour_no_escorts'] = "Svp, Ajouter un profil d'escorte en premier ";
$LNG['tour_phone'] = "Téléphone de la tournée";
$LNG['tour_saved'] = "Données de tournée sauvegardées";
$LNG['tour_update'] = "Éditer tournée";
$LNG['tour_updated'] = "Données de tournée changées";
$LNG['tours'] = "Tournées de Ville";
$LNG['trans'] = "Les transsexuels";
$LNG['trial'] = "Essai";
$LNG['trial_mb_desc'] = " 
<p>Vous avez reçu un abonnemnt en période d'éssai sur %sys_site_title%. Durant cette période, vous bénéficiez de ces avantages:</p> 
<ul> 
    <li>Profil sur la page principale et sur la page de la ville que vous avez choisie</li> 
    <li>Les utilisateurs peuvent écrire des évaluations à propos de vous</li> 
    <li>Participation au TOP 10 et \"Copine du Mois\"</li> 
    <li>Participation à la rotation des escortes sur la page principale, 
signifiant que vous ne stagnerez jamais aux dernières pages où peu 
d'utilisateur peuvent vous voir.</li> 
</ul> 
<p>et Plus !</p> 
";
$LNG['trial_mb_desc2'] = " 
<p> 
Après l'adhésion d'essai, vous devrez acheter un emplacement Privilège afin de conserver vos fonctionalités, de plus recevez notre <strong style='color: red;'>Offre Spéciale</strong>! 
<ul> 
    <li>Vous pourrez utiliser la fonction \"Tour de Ville\" vous donnant plusieurs outils de promotions</li> 
    <li><strong style='color: red;'>Annonce imprimée dans l'édition du mois suivant du magazine <a target='_blank' href='http://www.redmag.it'>Redmag</a> en Italie</p>)</li> 
</ul> 
<p><a href='mailto:%sys_contact_email%'>Cliquez-ici pour commander !</a></p> 
";
$LNG['trial_mb_desc_ag'] = "%COUNT% escortes (La première expire dans %DAYS% jours)";
$LNG['trustability'] = "Digne de confiance";
$LNG['trusted_review_popup'] = " 
<p>Vous nous donnez les détails à propos de la rencontre, et nous les transmettons directement à l'escorte. L'escorte ne peut voir votre évaluation et ne peu svoir en aucun moment si votre évaluation est bonne ou mauvaise après qu'elle aie confirmé vous avoir rencontrer. Cette évaluation sera affichée comme 100% réelle.</p> 
<p>Svp, donnez quelques information à propos de vous et de l'endroit où vous avez rencontrer l'escorte, veuillez noter que cette information est seulement reçu par l'escorte par sms ou courriel. Vos données sur notre site ne seront pas conservées. Si par exemple vous avez réservé avec votre vrai nom ou nom d'usager, alors utilisez seulement ce nom, si vous avez réservé avec votre courriel, alors utilisez cette adresse., etc...</p> 
<p>Exemples:</p> 
<ol> 
<li>gianni escortforum@hotmail.com, lundi 17.Jan 05 20:00pm</li> 
<li>pinky 333 333 454, Mercredi 16. Feb. 05 vers 08:00pm à l'hôtel 
ibis milano</li> 
</ol>";
$LNG['trusted_user_mail_subject'] = "Membre de confiance de %SITE%";
$LNG['trusted_user_mail_text'] = "<pre> 
Cher %NAME%, 
Merci de poster sur %SITE% ! 
Vous êtes maintenant un Membre Privilège, et vos messages n'auront plus besoin de vérification de la part de l'administration 
Nous vous souhaitons la bienvenue et aussi beaucoup de plaisir ! 
L'équipe de %SITE% 
</pre>";
$LNG['tuesday'] = "Mardi";
$LNG['tuscany'] = "Toscane";
$LNG['type'] = "Type";
$LNG['ugly'] = "Plutôt Moche";
$LNG['unfriendly'] = "Peu Amicale";
$LNG['unsubscribe'] = "Désabonner";
$LNG['upcoming_tours'] = "Upcoming tours";
$LNG['update_languages'] = "Changer langues";
$LNG['update_locations'] = "Changer les Régions";
$LNG['update_rates'] = "Changer tarifs";
$LNG['upg_esc_memb'] = "Améliorez votre Adhésion";
$LNG['upg_esc_memb_desc'] = "Merci de votre décision améliorer votre 
adhésion à %sys_site_title% !<BR /> 
Veuillez choisir le type et la durée de l'adhésion et cliquez alors sur bouton \"<B>UPGRADE NOW</B>\" pour être redirigé vers notre fournisseur de paiement:";
$LNG['upgrade_membership'] = "Améliorez votre adhésion";
$LNG['upgrade_now'] = "AMÉLIORER MAINTENANT!";
$LNG['upload_failed'] = "l'envoi de la photo a échoué.";
$LNG['upload_picture'] = "Envoyer photo";
$LNG['upload_private_photo'] = "Upload private photo";
$LNG['url'] = "URL";
$LNG['user'] = "User";
$LNG['username'] = "Nom d'utilisateur";
$LNG['users_match'] = "Membre répondants aux critères";
$LNG['vacation_add'] = "Ajouter vacances";
$LNG['vacation_end'] = "Retour de vacances";
$LNG['vacation_no_escorts'] = "Svp, ajouter un profil d'escorte en premier ";
$LNG['vacation_start'] = "Début vacances";
$LNG['vacations'] = "Vacances";
$LNG['vacations_active'] = "%NAME% est en vacances <strong>depuis %FROM_DATE% jusqu'à %TO_DATE%</strong>";
$LNG['vacations_active_soon'] = "%NAME% est en vacances, elle reviendra <strong>Bientôt</strong>";
$LNG['vacations_currently_date'] = "Cette escorte est présentement en vacances, elle reviendra le %DATE%.";
$LNG['vacations_currently_soon'] = "Cette escorte est présentement en vacances, elle reviendra <strong>Bientôt</strong>.";
$LNG['vacations_empty_to_date'] = "Retour de vacances ne peut être vide";
$LNG['invalid_vacation_dates'] = "Vacation dates are not valid";
$LNG['vacations_lower_date'] = "Date \"Retour de Vacances\" ne peut 
être avant la Date \"Début Vacances\"";
$LNG['vacations_return'] = "Retour de vacances";
$LNG['verification_email'] = "Cher %USER%, merci pour votre enregistrement avec %SITE%. pour confirmer votre enregistrement, cliquez sur ce lien.";
$LNG['verification_email_subject'] = "%sys_site_title% Vérification du Compte";
$LNG['very_simple'] = "Très simple";
$LNG['view_gallery'] = "Voir gallerie";
$LNG['view_my_private_photos'] = "View my private photos";
$LNG['view_my_profile'] = "Voir mon profil";
$LNG['view_my_public_photos'] = "View my public photos";
$LNG['view_profile'] = "Voir profile";
$LNG['view_reviews'] = "Ajouter/Voir évaluations";
$LNG['view_site_escort_reviews'] = "Voir les évaluations d'escorte de %SITE%";
$LNG['view_third_party_escort_reviews'] = "Voir les évaluations d'escorte de tiers";
$LNG['vip'] = "VIP";
$LNG['vip_mb_desc_ag'] = "%COUNT% escortes";
$LNG['vote'] = "Voter";
$LNG['vote_her_looks'] = "Voter son apparence";
$LNG['votes'] = "Votes";
$LNG['waist'] = "Taille";
$LNG['watch_list'] = "Liste de Favoris";
$LNG['web'] = "Site Internet";
$LNG['wednesday'] = "Mercredi";
$LNG['week'] = "semaine";
$LNG['weeks'] = "semaines";
$LNG['weight'] = "Poids";
$LNG['welcome'] = "Bienvenue à %sys_site_title%";
$LNG['whats_new'] = "Quoi de Neuf ?";
$LNG['when_you_met_her'] = "Quand vous la rencontrez";
$LNG['when_you_met_her_desc'] = "Date et Heure";
$LNG['when_you_met_lady'] = "Quand avez-vous rencontré cette escorte";
$LNG['where_did_you_meet_her'] = "Où l'avez-vous rencontrée";
$LNG['where_you_met_her'] = "Où vous la rencontrez";
$LNG['where_you_met_her_desc'] = "hôtel, chez elle, chez vous";
$LNG['white'] = "Blanc";
$LNG['whole'] = "Entier";
$LNG['wlc_city'] = "Régions de travail (ville)";
$LNG['wlc_country'] = "Localité de Travail (Pays)";
$LNG['women'] = "Les femmes";
$LNG['working_locations'] = "Lieux de travail";
$LNG['working_time'] = "Heures de travail";
$LNG['write_here_name_of_site'] = "Svp Inscrire ici le nom du site si ce n'est pas le site d'une escorte! Attention: annoncez seulement le site ici, Merci!";
$LNG['wrong_login'] = "Usager Incorrect";
$LNG['wrong_login_or_password'] = "Mot de Passe Incorrect";
$LNG['wrong_time_interval'] = "Intervalle éronnée pour les heures de travail de %DAY%.";
$LNG['year'] = "année";
$LNG['years'] = "ans";
$LNG['yellow'] = "Jaune";
$LNG['yes'] = "Oui";
$LNG['yes_more_times'] = "Oui, plusieurs fois";
$LNG['you_are_premium'] = "Vous êtes Membre Privilège";
$LNG['you_have_log'] = "Vous devez être branché pour utiliser cette fonction. <a href='%LINK%'>Cliquez ici</a> pour vous brancher ou <a href='%LINK2%'>ici</a> pour vous enregistrer, <a href='%LINK2%'>C'est GRATUIT!</a>";
$LNG['your_comment_added'] = "";
$LNG['you_have_voted_for'] = "Vous avez voté pour %ESCORT%! Merci.";
$LNG['you_r_watching_agencies'] = "Ces <b>agences</b> sont sur votre Liste de Favoris:";
$LNG['you_r_watching_escorts'] = "Ces <b>escortes</b> sont sur votre Liste de Favoris:";
$LNG['you_r_watching_users'] = "Ces <b>membres</b> sont sur votre Liste de Favoris:";
$LNG['your_booking_text'] = "Détails de la réservation";
$LNG['your_data'] = "Vos données";
$LNG['your_discount'] = "Votre Escompte: %DISCOUNT%%";
$LNG['your_full_name'] = "Votre Nom complet";
$LNG['your_info'] = "Vos Info";
$LNG['your_info_desc'] = "courriel, téléphone, nom";
$LNG['your_login_name'] = "Votre Nom d'Usager";
$LNG['your_name'] = "Votre Nom";
$LNG['your_report'] = "Votre rapport de problème";
$LNG['zip'] = "Code Postal";
$LNG['69'] = "69";
$LNG['tours_in_other_countries'] = "Tours in other countries";
$LNG['brunette'] = "Châtain foncé";
$LNG['hazel'] = "Hazel";
$LNG['profile_or_main_page_links_only'] = "Add links only to your profile/ main webpage of  your agency or to your personal website.";
$LNG['competitors_links_will_be_deleted'] = "All third-party advertising/ competitors links will be deleted by administrator!";
$LNG['domain_blacklisted'] = "Domain blacklisted";
$LNG['phone_blocked'] = "Phone number is blocked";
$LNG['girls'] = "Filles";
$LNG['inform_about_changes'] = "Thank you. We will inform you about changes on %sys_site_title%";
$LNG['escort_photos_over_18'] = "Pour vous inscrire sur le site vous devez avoir plus de 18 ans le jour de l’inscription.";
$LNG['captcha_please_enter'] = "Entrer svp";
$LNG['captcha_here'] = "ici";
$LNG['captcha_verification_failed'] = "Vérification des textes échoue.";
$LNG['my_awards'] = "My Awards";
$LNG['escort_girls'] = "Escort Girls";
$LNG['locally_available_in_countries'] = "Nous sommes egalement présent dans les pays suivant";
$LNG['domina'] = "Domina";
$LNG['dominas'] = "Dominas";
$LNG['club'] = "Club";
$LNG['club_name'] = "Nom du club";
$LNG['club_profile'] = "Profil d'Club";
$LNG['studio'] = "Studio";
$LNG['studio_girls'] = "Studio Girls";
$LNG['all_regions'] = "All regions...";
$LNG['see_all_escorts_of_this_agency'] = "See all escorts of this agency";
$LNG['see_all_escorts_of_this_club'] = "See all escorts of this club";
$LNG['invalid_height'] = "Grandeur invalide";
$LNG['invalid_weight'] = "Poids inválida";
$LNG['subscribe_to_newsletter'] = "Subscribe to our newsletter!";
$LNG['inform_every_week'] = "We will inform you every week about escorts on %sys_site_title%";
$LNG['clubs_match'] = "Clubs matching criteria";
$LNG['welcome_description'] = "";
$LNG['advertise_now'] = "";
$LNG['autumn_action'] = "";
$LNG['escort_girls_description'] = "";
$LNG['independent_girls_description'] = "";
$LNG['studio_girls_description'] = "";
$LNG['index_bottom_text'] = "";
$LNG['escort_girls_in_city'] = "Escort Girls in %CITY%";
$LNG['independent_girls_in_city'] = "Private Girls in %CITY%";
$LNG['studio_girls_in_city'] = "Studio Girls in %CITY%";
$LNG['new_arrivals_in_city'] = "New Girls in %CITY%";
$LNG['all_girls_in_city'] = "All girls in %CITY%";
$LNG['see_all_girls_from_agency'] = "click to see all girls from this agency";
$LNG['see_all_independent_girls'] = "click to see all independent girls";
$LNG['last_modified'] = "Modifiés en dernier";
$LNG['advertise_with_us'] = "Annoncer avec nous";
$LNG['nearest_cities_to_you'] = "Les villes les plus proches de vous";
$LNG['nearest_cities_to_city'] = "Les villes les plus proches de %CITY%";
$LNG['cities_in_region'] = "Cities in %REGION%";
$LNG['local'] = "Local";
$LNG['local_directory'] = "Club/Escort listing";
$LNG['exchange_banners'] = "If do you want interchange banners and/or links with us, email us to <a href='mailto:%sys_contact_email%'>%sys_contact_email%</a>";
$LNG['our_network_in_europe'] = "Our network in Europe";
$LNG['agencies_and_independent_in_spain'] = "Agencies and independent girls in Spain";
$LNG['directories_and_guide_in_spain'] = "Directories, forum and erotic guide in Spain";
$LNG['friends'] = "Friends";
$LNG['escort_service'] = "Service d'escorte";
$LNG['studio_and_private_girls'] = "Studio y privé escorte";
$LNG['advertising'] = "Advertising";
$LNG['premium_girls'] = "Premium girls";
$LNG['submit_form_to_signup'] = "Submit this form to signup and get Promoted from largest escort directory!";
$LNG['signup_now_to_largest_escort_directory'] = "Signup now for <span class='pink'>FREE</span> to the largest escort directory worldwide. We currently have over <span class='pink'>3 million clients</span> monthly on our network. Start using our popularity to promote yourself now <span class='pink'>FOR FREE!</span>";
$LNG['signing_on_all_local_sites'] = "By signing up, your profile will automatically be displayed on all these local country sites. Your login data will work on all the sites - you just have to signup once.";
$LNG['please_choose_your_business'] = "Veuillez choisir votre offre";
$LNG['independent_privat'] = "Independent Escort / Privat Girl";
$LNG['escort_agency'] = "Escort Agency";
$LNG['club_bordell_strip'] = "Club / Bordell / Sauna Club / Stripclub (Tabledance), Massage Studio";
$LNG['please_write_numbers_from_field'] = "Please write the numbers like in the field";
$LNG['just_moment_away_from_escort_directory'] = "You are just moment away from having access to largest escort directory!";
$LNG['choose_your_region'] = "Choose your region";
$LNG['setcards_online'] = "%COUNT% escorts online";
$LNG['only_escorts'] = "only escorts";
$LNG['only_private_girls'] = "only private girls";
$LNG['only_studio_girls'] = "only studio girls";
$LNG['most_popular_cities'] = "Villes les plus populaires";
$LNG['renew_your_profile'] = "Your profile is expired. If you would like to renew it, contact your Sales person or write us at <a href='mailto:%sys_contact_email%'>%sys_contact_email%</a>";
$LNG['relax'] = "Relax";
$LNG['email_unsubscribed'] = "E-mail %EMAIL% is removed from %sys_site_title% newsletter.";
$LNG['vip_girls'] = "VIP girls";
$LNG['crop_photo'] = "Crop photo";
$LNG['show_thumbnails_on'] = "Show thumbnails on";
$LNG['photo_was_cropped'] = "Photo was cropped.";
$LNG['crop_feature_teaser'] = " 
    <p><strong style='color: red;'>ATTENTION</strong><br /> 
    Nous venons de lancer des nouveautés. Toutes les photos sur la page principale et dans la partie « international » ont maintenant la même taille. Par conséquent, nous aimerions vous demander de vérifier votre principale photo sélectionnée. S'il vous plaît connectez vous sur votre compte et sélectionnez « edit gallery » et suivez les instructions.</p> 
";
$LNG['crop_feature_description'] = " 
    <p><strong style='color: red;'>ATTENTION:</strong><br /> 
    Car avec notre nouveau système toutes les principales photos apparaissent à la même taille sur notre site. Cela donne un aspect plus professionnel au site.</p> 
    <p>Comment çà marche?</p> 
    <ol> 
        <li>Contrôlez maintenant votre principale photo. Si tout à l'air de bien fonctionner vous n'avez pas besoin d'aller à l'étape n°2. Si quelque chose ne va pas (par exemple la photo est coupée) suivez les instructions de l'étape n°2.</li> 
        <li>Cliquez sur la photo qui n'est pas bonne.</li> 
        <li>Au milieu de la photo vous pouvez constater maintenant un cadre qui représente la façon dont sera présentée votre photo sur la page principale. Vous pouvez bouger le cadre tout autour jusqu'à ce que vous ayez choisi la partie de la photo que vous voulez. Cliquez sur l'image récoltée.</li> 
        <li>Vous avez fini. Vous pouvez examiner votre photo dans la partie « edit gallery main page ». Si tout à l'air bien, vous avez fini.</li> 
    </ol> 
";
$LNG['we_only_sell_advertisement'] = "<strong>%sys_site_title%</strong> est un site d'information et de publicité et n'a aucune connexion ou lien avec quelques sites que ce soient ou individus mentionnés ici. Nous sommes seulement un espace de publicité, nous ne sommes pas une agence d'escorte, ni même dans l'escorting ou le business de la prostitution. Nous ne prenons aucune responsabilité dans le contenu ou les actions de tierces parties (sites webs ou individus) sur lesquels vous auriez des informations telles que les emails ou les contacts téléphoniques.";
$LNG['user_messages_review_waiting_for_approval'] = "Your review about %MESSAGE% is waiting for verification. Please be patient";
$LNG['user_messages_favorite_girl_review'] = "Your favorite girl <strong>%MESSAGE%</strong> has new review. <a href='%LINK%'>click here to see</a>";
$LNG['user_messages_review_approval'] = "Your review <strong>%MESSAGE%</strong> was approved by administrator.";
$LNG['user_messages_review_deleted'] = "Your review <strong>%MESSAGE%</strong>  was deleted by administrator. Reason given: %LINK%";
$LNG['user_messages_new_discount'] = "%MESSAGE% is offering you a special EF discount. <a href='%LINK%'>Check her out!</a>";
$LNG['user_messages_deleted_discount'] = "%MESSAGE% is not offering you a special EF discount anymore.";
$LNG['user_messages_favorite_disabled'] = "Your favorite girl <strong>%MESSAGE%</strong> is no longer with us! We are sorry! Maybe time to find a new favorite?";
$LNG['user_messages_favorite_activated'] = "Your favorite girl <strong>%MESSAGE%</strong> joined us again!";
$LNG['user_messages_girl_of_month'] = "Girl of the month %MONTH%/%YEAR% is: <strong>%MESSAGE%</strong>!! Congratulation %MESSAGE%!!!";
$LNG['user_messages_free_fuck_winner'] = "<strong>You are the WINNER of the freefuck lottery contact us.</strong>";
$LNG['user_messages_free_fuck_others'] = "The winner of the free fuck lottery of month %MONTH%/%YEAR% is: <strong>%MESSAGE%</strong>!! Congratulation %MESSAGE%!!!";
$LNG['user_messages_favorite_region_girls'] = "New girls from your favourite region joined EF since your last visit. %LINK%";
$LNG['user_messages_favorite_region_vip'] = "There is new VIP %MESSAGE% from your favourite region since your last visit. %LINK%";
$LNG['user_email_review_approved'] = "Hello %TARGET_0%
Your review %TARGET_1% was approved by administrator.
%sys_site_title% Team";
$LNG['escort_email_review_approved'] = "Hello %TARGET_0%
Review %TARGET_1% about you was approved by administrator.
%sys_site_title% Team";
$LNG['user_email_favorites_review_approved'] = "Hello %TARGET_0%
Your favorite girl  %TARGET_1% has new review.
%sys_site_title% Team";
$LNG['agency_email_review_approved'] = "Hello %TARGET_0%
Review about %TARGET_1% was approved by administrator.
%sys_site_title% Team";
$LNG['user_email_subject_review_approval_favorites'] = "%sys_site_title% new review";
$LNG['user_email_subject_review_approval'] = "%sys_site_title% review approved";
$LNG['escort_email_subject_review_approval'] = "%sys_site_title% review approved";
$LNG['agency_email_subject_review_approval'] = "%sys_site_title% review approved";
$LNG['escort_email_review_new'] = "Hello %TARGET_0%
New review %TARGET_1% about you is waiting for approval.
%sys_site_title% Team";
$LNG['agency_email_review_new'] = "Hello %TARGET_0%
Your escort %TARGET_1% has new review waiting for approval.
%sys_site_title% Team";
$LNG['user_email_review_new'] = "Hello %TARGET_0%
Your review about %TARGET_1% is waiting for verification. Please be patient.
%sys_site_title% Team";
$LNG['user_email_subject_review_new'] = "%sys_site_title% review waiting for approval";
$LNG['escort_email_subject_review_new'] = "%sys_site_title% new review waiting for approval";
$LNG['agency_email_subject_review_new'] = "%sys_site_title% new review waiting for approval";
$LNG['user_email_review_deleted'] = "Hello %TARGET_0%
Your review <strong>%TARGET_1%</strong>  was deleted by administrator. Reason given: %TARGET_2%
%sys_site_title% Team";
$LNG['escort_email_review_deleted'] = "Hello %TARGET_0%
The review <strong>%TARGET_1%</strong> about you was deleted by administrator. Reason given: %TARGET_2%
%sys_site_title% Team";
$LNG['agency_email_review_deleted'] = "Hello %TARGET_0%
The review about <strong>%TARGET_1%</strong> was deleted by administrator. Reason given: %TARGET_2%
%sys_site_title% Team";
$LNG['user_email_subject_review_delete'] = "%sys_site_title% review deleted";
$LNG['escort_email_subject_review_delete'] = "%sys_site_title% review deleted";
$LNG['agency_email_subject_review_delete'] = "%sys_site_title% review deleted";
$LNG['user_email_discount_new'] = "Hello %TARGET_0%
%TARGET_1% is offering you a special EF discount.
%sys_site_title% Team";
$LNG['escort_email_discount_new'] = "Hello %TARGET_0%
New discount was assigned to you.
%sys_site_title% Team";
$LNG['agency_email_discount_new'] = "Hello %TARGET_0%
%TARGET_1% is now offering a special EF discount.
%sys_site_title% Team";
$LNG['user_email_subject_discount_new'] = "%sys_site_title% new discount";
$LNG['escort_email_subject_discount_new'] = "%sys_site_title% new discount";
$LNG['agency_email_subject_discount_new'] = "%sys_site_title% new discount";
$LNG['user_email_discount_deleted'] = "Hello %TARGET_0%
Your escort %TARGET_1% is not offering a special EF discount anymore.
%sys_site_title% Team";
$LNG['user_email_subject_discount_deleted'] = "%sys_site_title% discount expired";
$LNG['user_email_escort_deactivated'] = "Hello %TARGET_0%
Your favorite girl <strong>%TARGET_1%</strong> is no longer with us! We are sorry! Maybe time to find a new favorite?
%sys_site_title% Team";
$LNG['escort_email_escort_deactivated'] = "Hello %TARGET_0%
Your account has been deactivated.
%sys_site_title% Team";
$LNG['agency_email_escort_deactivated'] = "Hello %TARGET_0%
Account of %TARGET_1% has been deactivated.
%sys_site_title% Team";
$LNG['user_email_subject_favorites_deactivated'] = "%sys_site_title% escort deactivated";
$LNG['escort_email_subject_favorites_deactivated'] = "%sys_site_title% account deactivated";
$LNG['agency_email_subject_favorites_deactivated'] = "%sys_site_title% account deactivated";
$LNG['user_email_escort_activated'] = "Hello %TARGET_0%
Your favorite girl <strong>%TARGET_1%</strong> joined us again!
%sys_site_title% Team";
$LNG['escort_email_escort_activated'] = "Hello %TARGET_0%
Your account has been activated.
%sys_site_title% Team";
$LNG['agency_email_escort_activated'] = "Hello %TARGET_0%
Account of %TARGET_1% has been activated.
%sys_site_title% Team";
$LNG['agency_email_subject_favorites_activated'] = "%sys_site_title% account activated";
$LNG['user_email_girl_of_the_month'] = "Hello %TARGET_0%
Girl of the month %TARGET_1% is: <strong>%TARGET_2%</strong>!! Congratulation %TARGET_2%!!!
%sys_site_title% Team";
$LNG['escort_email_girl_of_the_month'] = "Hello %TARGET_0%
Girl of the month %TARGET_1% is: <strong>%TARGET_2%</strong>!! Congratulation %TARGET_2%!!!
%sys_site_title% Team";
$LNG['escort_email_girl_of_the_month_winner'] = "Hello %TARGET_0%
You are new girl of the month %TARGET_1%! Congratulation %TARGET_0%!!!
%sys_site_title% Team";
$LNG['user_email_subject_girl_of_the_month'] = "%sys_site_title% girl of the month";
$LNG['escort_email_subject_girl_of_the_month'] = "%sys_site_title% girl of the month";
$LNG['user_email_free_fuck_winner'] = "Hello %TARGET_0%
<strong>You are the WINNER of the freefuck lottery contact us.</strong>
%sys_site_title% Team";
$LNG['user_email_free_fuck_others'] = "Hello %TARGET_0%
The winner of the free fuck lottery of month %TARGET_1% is: <strong>%TARGET_2%</strong>!! Congratulation %TARGET_2%!!!
%sys_site_title% Team";
$LNG['user_email_subject_free_fuck_winner'] = "%sys_site_title% freefuck lottery winner";
$LNG['user_email_subject_free_fuck_others'] = "%sys_site_title% freefuck lottery";
$LNG['user_email_new_girls_in_region'] = "Hello %TARGET_0%
New girls from your favourite region joined EF.
%sys_site_title% Team";
$LNG['user_email_subject_new_girls_in_region'] = "%sys_site_title% new girls";
$LNG['user_email_new_vip_girls_in_region'] = "Hello %TARGET_0%
There is new VIP %TARGET_1% from your favourite region since your last visit. %TARGET_2%
%sys_site_title% Team";
$LNG['user_email_subject_new_vip_girls_in_region'] = "%sys_site_title% new VIP girls";
$LNG['loft'] = "Loft";
$LNG['loft_escorts_photo_limit_reached'] = "Loft escorts can have max %LIMIT% photos in gallery.";
$LNG['loft_escorts_cant_go_on_tour'] = "Loft escorts can't go on tour.";
$LNG['email_exists'] = "Email exists";
$LNG['members_login'] = "Se connecter";
$LNG['signup_now'] = "Enregistrez-vous";
$LNG['live_now'] = "Live now";
$LNG['girl_has_video'] = "Girl has video";
$LNG['girl_has_reviews'] = "Girl has reviews";
$LNG['100%_real_photos'] = "100% real photos";
$LNG['happy_hour'] = "Happy hour";
$LNG['icons_explanation'] = "Icons explanation";
$LNG['offer_incall'] = "Offre incall";
$LNG['offer_outcall'] = "Offer outcall";
$LNG['ebony_only'] = "Brunes";
$LNG['asian_only'] = "Asiatiques";
$LNG['blondes_only'] = "Blondes";
$LNG['add_to_my_favourites'] = "Add to my favourites";
$LNG['remove_from_favourites'] = "Remove from favourites";
$LNG['add_view_reviews'] = "Add/View reviews";
$LNG['about_escort'] = "About %SHOWNAME%";
$LNG['contact_me'] = "Contact me";
$LNG['now_open'] = "Ouvrir maintenant";
$LNG['closed'] = "Closed";
$LNG['viewed'] = "vue";
$LNG['quick_links'] = "Liens rapides";
$LNG['free_signup'] = "Free signup";
$LNG['signup_profit'] = " 
<ul> 
    <li>add <strong>comments</strong></li> 
    <li class='fsa-odd'>manage <strong>favorite</strong> list</li> 
    <li>write <strong>reviews</strong></li> 
    <li class='fsa-odd'>access to <strong>advanced search</strong></li> 
    <li><strong>watch</strong> list</li> 
</ul> 
";
$LNG['sort_by'] = "Par ordre";
$LNG['random'] = "Random";
$LNG['alphabetically'] = "Alphabétique";
$LNG['most_viewed'] = "Plus vus";
$LNG['newest_first'] = "Les nouveaux en premier";
$LNG['narrow_your_search'] = "Affinez votre recherche";
$LNG['with_reviews'] = "With reviews";
$LNG['search_by_name_agency'] = "Recherche";
$LNG['comments_responses'] = "Commentaires &amp;  Reponses";
$LNG['post_comment'] = "Post a text comment";
$LNG['back'] = "Retour";
$LNG['reply'] = "Reply";
$LNG['spam'] = "Spam";
$LNG['only_registered_users_can_add_comments'] = "Only registered users can add comments. Please <a href='%VALUE%'>login here</a>.";
$LNG['your_comment'] = "Your comment";
$LNG['close'] = "Close";
$LNG['message_waiting_for_approval'] = "Thank you. Your message is waiting for approval now.";
$LNG['ago'] = "ago";
$LNG['time_ago'] = "%TIME% ago";
$LNG['yesterday'] = "Yesterday";
$LNG['minute'] = "minute";
$LNG['spanking'] = "Fessée";
$LNG['domination'] = "Domination";
$LNG['fisting'] = "Fist-fucking";
$LNG['massage'] = "Massage";
$LNG['role_play'] = "Role play";
$LNG['bdsm'] = "BDSM";
$LNG['hardsports'] = "Hardsports";
$LNG['hummilation'] = "Hummilation";
$LNG['rimming'] = "Rimming";
$LNG['less_than'] = "moins de %NUMBER%";
$LNG['more_than'] = "plus de %NUMBER%";
$LNG['free_signup_advantages'] = "The signup on <span class='red'>%sys_site_title%</span> is absolutely <span class='red-big'>FREE</span> and has many advantages.";
$LNG['reset'] = "Reset";
$LNG['private_area'] = "Private area";
$LNG['choose_your_choice'] = "Choose your choice";
$LNG['base_city'] = "Ville de base";
$LNG['additional_cities'] = "Villes additionnelles";
$LNG['zone'] = "Zone";
$LNG['actual_tour_data'] = "NOW in %CITY% (%COUNTRY%)";
$LNG['upcoming_tour_data'] = "in %DAYS% in %CITY% (%COUNTRY%), from %FROM_DATE% - %TO_DATE%";
$LNG['beta_warning'] = "%sys_site_title% is in Beta phase. We are constantly working on the site and make regular updates and improvements.";
$LNG['ethnic_white'] = "Caucasien";
$LNG['ethnic_black'] = "Noir";
$LNG['ethnic_asian'] = "Asiatique";
$LNG['ethnic_latin'] = "Latin";
$LNG['ethnic_african'] = "African";
$LNG['ethnic_indian'] = "Indien";
$LNG['members_signup'] = "Members signup";
$LNG['click_here_your_signup'] = "Click here for your singup.";
$LNG['your_email'] = "Your e-mail";
$LNG['missing_message'] = "Missing message";
$LNG['your_message_sent'] = "Your message was sent.";
$LNG['signup'] = "Signup";
$LNG['signup_to_largest_escort_directory'] = "Signup now for <span class='red-big'>FREE</span> to the largest escort directory worldwide. We currently have over <span class='red-big'>3 million clients</span> monthly on our network. Start using our popularity to promote yourself now <span class='red-big'>FOR FREE</span>!";
$LNG['one_signup_all_sites'] = "By signing up, your profile will be automatically displayed on all these local country sites. Your login data will work on all the sites - you just have to signup once.";
$LNG['email_verification_subject'] = "%sys_site_title% account verification";
$LNG['email_verification_text'] = "<b>Dear %USER%,</b><br /><br /> 
<b>Merci de vous être inscrit sur %sys_site_title%</b>.<br /><br /> 
Il n'y a plus qu'une étape à passer pour activer votre compte, il suffit de cliquer
sur le lien ci-dessous ou le copier/coller dans votre navigateur.<br /><br /> 
<b><a href='%LINK%' style='color: #dd0000;'>%LINK%</a></b><br /><br /> 
REMARQUE: <span style='font-size: 11px;'> Si vous rencontrez des problèmes, veuillez nous contacter sur<b><a href='mailto:%sys_contact_email%' style='color: #dd0000;'>%sys_contact_email%</a></b>.</span><br /><br /> 
<b>Bien cordialement</b><br /> 
<b>%sys_site_title% Team</b>";
$LNG['email_confirmation_subject'] = "%sys_site_title% registration";
$LNG['email_confirmation_text'] = "Dear %USER%,</b><br /><br /> 
Thank you for registering at %sys_site_title%.<br /> 
Your registration is now complete and you are an active member of %sys_site_title%.<br /><br /> 
<b>Following is your login information. Please keep this information on a safe place.<br /> 
<span style='display: block; float: left; width: 75px;'>Login:</span> <span style='color: #dd0000;'>%USER%</span><br /> 
<span style='display: block; float: left; width: 75px;'>Web:</span> <a href='%sys_url%' style='color: #dd0000;'>%sys_url%</a></b><br /><br /> 
Have fun on the site!<br /><br /> 
<b>Yours faithfully</b><br />  
<b>%sys_site_title% Team</b>";
$LNG['contact_us_text1'] = "In case if you wish to contact us regarding general enquiries, feedback or advertising, choose from the following options";
$LNG['phone_international'] = "Phone International";
$LNG['contact_us_text2'] = "Alternatively please use our contact form below";
$LNG['your_message'] = "Your Message";
$LNG['link_exschange'] = "Link Exchange";
$LNG['contact_us_text3'] = "If you would like to exchange links, please email the webmaster (e-mail).<br />Please copy our banner and place it on your server before contacting us regarding link exchange.";
$LNG['delete_profile'] = "Delete Profile";
$LNG['confirm_delete_profile'] = "Are you sure you want to delete the escort profile ?";
$LNG['delete_escort'] = "Delete escort profile";
$LNG['escort_profile_deleted'] = "Escort profile has been successfuly deleted";
$LNG['escort_profile_deleted_error'] = "Error: Escort profile has not been deleted";
$LNG['same_city'] = "Working city and tour city can not be same.";
$LNG['overnight'] = "Heures de nuit";
$LNG['directory_for_female_escorts'] = "NGE_SITE_TITLE is a UK escort directory for female escorts, escort agencies, independent escorts, trans and couples. 
On escortguide.co.uk you can find the perfect escort in London, Manchester, Leeds and all other areas in the United Kingdom. 
We update our escort directory on a daily basis - you will find new female escorts in london and from your area every day. 
If you are an escort agency or a female independent escort based in London or the UK, simply click on the 
signup link to register and upload your escort profile to escortguide.co.uk. ";
$LNG['popularity_views'] = "Popularity / Views";
$LNG['last_modified_date'] = "Modifié en dernier";
$LNG['registered_date'] = "Registered date";
$LNG['word'] = "";
$LNG['phrase'] = "";
$LNG['colour'] = "";
$LNG['doesnt_matter'] = "";
$LNG['specific_words'] = "";
$LNG['kissing_no'] = "No Kissing";
$LNG['blowjob_no'] = "No Blowjob";
$LNG['cumshot_no'] = "No Cumshot";
$LNG['cumshot_in_mouth_spit'] = "Dans la bouche mais n'avale pas";
$LNG['cumshot_in_mouth_swallow'] = "Dans la bouche et avale";
$LNG['cumshot_in_face'] = "Sur le visage";
$LNG['with_comments'] = "with comments";
$LNG['nickname'] = "Nickname";
$LNG['without_reviews'] = "Without reviews";
$LNG['not_older_then'] = "Not older then";
$LNG['order_results'] = "Order results";
$LNG['select_your_girl_agency_requirements'] = "Select your girl/agency requirements";
$LNG['girls_details_bios'] = "Girls details/bio’s";
$LNG['cm'] = "cm";
$LNG['ft'] = "ft";
$LNG['kg'] = "kg";
$LNG['lb'] = "lb";
$LNG['girls_origin'] = "Girls origin";
$LNG['ethnicity'] = "Groupe ethnique";
$LNG['measurements'] = "Measurements";
$LNG['open_hours'] = "Open hours";
$LNG['my_favorites'] = "My Favorites";
$LNG['learn_more_webcam'] = "If you have a webcam plus an account at
Skype or MSN, this is the easiest way to
get your photos verified.
Just click on the 'Webcam session request' link,
choose a date and a time and wait for our
confirmation. At the requested time you will
then have an short online appointment (video chat)
with one of our adminstrators to approve that it's
you on the photos. After this session your
photos will be 100% verified. ";
$LNG['learn_more_passport'] = "If you choose this option you need to scan your
passport or any other official document such as
identity card or driving license and upload it
to our system along with 1-2 additional private photos.";
$LNG['verify_welcome'] = "Bienvenue!";
$LNG['verify_welcome_text'] = "Bienvenue dans le processus de vérification à 100 % de l'authenticité des photos. Utilisez cette chance et faites vérifier l'authenticité à 100 % de photos grâce à ce système.  En seulement quelques étapes, vous allez améliorer la crédibilité de votre profil et obtenir encore plus d'utilisateurs. Vous obtiendrez également un autocollant pour votre profil qui indiquera aux utilisateurs que vos photos sont 100 % authentiques. De plus, votre profil sera listé dans une catégorie supplémentaire dans le menu. ";
$LNG['verify_enroll'] = "Adhérez dès maintenant ! ";
$LNG['verify_enroll_text'] = "Nous fournissons actuellement les <strong>deux options</strong> suivantes pour vérifier à 100 % vos photos : ";
$LNG['verify_enroll_1'] = "Session webcam avec notre modérateur";
$LNG['verify_enroll_2'] = "Copie du passeport ";
$LNG['verify_learn_more'] = "Learn more...";
$LNG['verify_start_now'] = "<span>DÉMARREZ </span> dès maintenant le processus";
$LNG['verify_steps'] = "STEP GUIDLINE:";
$LNG['verify_select_2_options'] = "Please select one of the following 2 options for the process:";
$LNG['verify_select_1'] = "Demande de session webcam avec notre modérateur";
$LNG['verify_select_1_note'] = "<strong>Remarque:</strong> vous devez avoir une webcam et un compte Skype ou MSN";
$LNG['verify_select_2'] = "Copie du passeport  - vous avez besoin d'un passeport ou d'une carte d'identité en cours de validité";
$LNG['verify_select_2_note'] = "<strong>Remarque:</strong> pour accélérer le processus, nous vous recommandons de charger 1-2 photos privées en plus de la copie du passeport. ";
$LNG['verify_important'] = "IMPORTANT - A LIRE ATTENTIVEMENT :  toute tentative de fraude ou de violation du processus donnera lieu à un ban à vie du réseau EF. Toutes vos publicités payantes seront désactivées et ne seront pas remboursées.";
$LNG['verify_sms_message'] = "Videochat confirmation for the photo verification process: 
Date: #date#, Time: #time#. You will be contacted by our administrator. #title#";
$LNG['verify_email_subject'] = "Online appointment confirmation";
$LNG['verify_email_message'] = "Hereby we confirm your online appointment for the photo verification process at:<br /><br />
Date: #date#<br />
Time: #time#<br /><br />
You will be contacted by one of our administrators. Kind regards.<br />#title#";
$LNG['reject_sms_message'] = "Please choose 3 new dates for the videochat session (photo verification). We could not confirm your requested dates. Just login and choose again. #title#";
$LNG['reject_email_subject'] = "100% photo verification process / please enter new dates";
$LNG['reject_email_message'] = "Dear #showname#<br /><br />
Unfortunately all suggested dates for the video chat session to verify your pictures could not been confirmed. Please login again to your private area and choose three new
dates. We would like to appologize for any inconvenience caused.<br /><br />
Kind regards<br />
#title#";
$LNG['reg_has_been_succ'] = "Votre inscription a été effectuée avec succès !";
$LNG['verify_attention'] = "Important note: For uploading the photos please use solely the form below. Do not send the photos by e-mail. Only send an e-mail to the sales representative in case you experience any problems uploading the pictures. ";
$LNG['city_zones_in'] = "";
$LNG['contact_showname'] = "";
$LNG['masturbation'] = "Masturbation";
$LNG['signup_today'] = "";
$LNG['temp_announce'] = "";
$LNG['latin_only'] = "Latines";
$LNG['seo_h1_uk'] = "";
$LNG['escort_is_premium'] = "";
$LNG['100_verify'] = "";
$LNG['verify_global_title'] = "";
$LNG['verify_webcam_session'] = "Session webcam";
$LNG['verify_webcam_request'] = "Demande une session webcam avec notre modérateur, fournis au moins deux rendez-vous, en cas d'annulation, informes-en le modérateur. ";
$LNG['verify_appointment'] = "N° de demande de rendez-vous ";
$LNG['verify_preffered'] = "Horaire préféré";
$LNG['verify_comment_field'] = "Champ de commentaire";
$LNG['verify_webcam_software'] = "Webcam software:<br /><span>(choisis soit Skype soit MSN)</span>";
$LNG['verify_type_username'] = "Username";
$LNG['verify_select_form'] = "Veuillez sélectionner la forme selon laquelle vous souhaiteriez recevoir confirmation de votre demande de rendez-vous.";
$LNG['verify_recommended'] = "recommandé";
$LNG['verify_over_sms'] = "Par téléphone / SMS";
$LNG['verify_congratulations'] = "Félicitations!";
$LNG['verify_congratulations_text'] = "";
$LNG['verify_already_pending'] = "";
$LNG['verify_rejected'] = "";
$LNG['verify_passport_upload'] = "";
$LNG['verify_passport_upload_text'] = "Vous avez choisi de fournir un passeport ou une carte d'identité pour le processus de vérification. Veuillez noter que nous ne pouvons accepter les courriels avec les copies de passeport ou de carte d'identité. Vous devez les envoyer via la fenêtre de chargement ici. En cas de difficultés, veuillez contacter notre agent commercial.";
$LNG['verify_upload_now'] = "Chargez dès maintenant vos documents ici ";
$LNG['verify_provide_pics'] = "TIP: Fournissez 1-2 photos privées, ainsi vous augmenterez et accélérerez le processus de vérification de l'authenticité à 100 %. ";
$LNG['verify_select_notify'] = "";
$LNG['verify_image_no'] = "";
$LNG['verify_uploaded'] = "";
$LNG['verify_uploaded_pictures'] = "";
$LNG['verify_uploaded_pictures_text'] = "";
$LNG['verify_uploaded_comment_field'] = "Champ de commentaire";
$LNG['verify_congratulations_text2'] = "";
$LNG['verify_genuine'] = "";
$LNG['verify_welcome_agency'] = "";
$LNG['verify_choose_profile'] = "";
$LNG['verify_unverified'] = "";
$LNG['ad_reg_confirm'] = "";
$LNG['email_sig'] = "";
$LNG['atten_add_review'] = "Attention, si vous êtes déjà membre d' %sys_site_title%,  cliquez ici pour vous logger.";
$LNG['tour_rules_desc'] = "<br>";
$LNG['more_escorts_by'] = "Plus d'escortes par";
$LNG['service_for'] = "Service pour";
$LNG['time_overnight'] = "Heures de nuit";
$LNG['time_hours'] = "hours";
$LNG['time_minutes'] = "minutes";
$LNG['signout'] = "Signout";
$LNG['date_from'] = "Date de";
$LNG['date_to'] = "Date to";
$LNG['biography'] = "Biography";
$LNG['dbl_click_to_set_base_city'] = "Double click la ville de base";
$LNG['no_tours_defined'] = "No tours defined";
$LNG['edit_tours'] = "Edit tours";
$LNG['tip'] = "TIP";
$LNG['double_click_to_main'] = "Double click to set main image";
$LNG['drag_to_adjust'] = "Drag to adjust image";
$LNG['no_rates_defined'] = "No rates defined";
$LNG['100_verification'] = "100% Verification";
$LNG['edit_profile_data'] = "Edit profile data";
$LNG['apartment'] = "Apartment";
$LNG['incall_outcall'] = "Incall / Outcall";
$LNG['language_en'] = "anglais";
$LNG['language_fr'] = "français";
$LNG['language_de'] = "allemand";
$LNG['language_it'] = "italien";
$LNG['language_pt'] = "portugais";
$LNG['language_ru'] = "russe";
$LNG['language_es'] = "espagnol";
$LNG['language_gr'] = "Greek";
$LNG['congratulations'] = "Félicitations!";
$LNG['successfully_signed_up'] = "You successfully signed up as a member of <a href='%SITE_URL%'>%SITE_NAME%</a> Thank you for singing up!";
$LNG['complete_your_registration'] = "NOTE: To complete your registration please check your email.";
$LNG['attention_2'] = "ATTENTION";
$LNG['account_succesfully_activated'] = "Votre compte a été activé avec succès!";
$LNG['full_member'] = "Vous êtes désormais un membre à part entière de notre site. Vos paramètres de connexion vous ont été envoyés par mail. ";
$LNG['click_to_signin'] = "<a href='%LINK%'>Cliquez ici</a> pour vous connecter maintenant sur votre compte. ";
$LNG['lbs'] = "lbs";
$LNG['status_active'] = "Active";
$LNG['edit_agency_profile'] = "Edit agency profile";
$LNG['edit_escorts'] = "Edit escorts";
$LNG['upload_logo'] = "Upload logo";
$LNG['location'] = "Région";
$LNG['feedback_form_name'] = "Name";
$LNG['feedback_form_email'] = "Your e-mail";
$LNG['feedback_form_message'] = "Message";
$LNG['additional_city'] = "Additional city";
$LNG['city_premium_spot'] = "City premium spot";
$LNG['girl_of_the_month'] = "Girl of the month";
$LNG['main_premium_spot'] = "Main premium spot";
$LNG['national_listing'] = "National listing";
$LNG['no_reviews'] = "No reviews";
$LNG['tour_ability'] = "Tour ability";
$LNG['tour_premium_spot'] = "Tour premium spot";
$LNG['agency_profile_updated'] = "Agency profile updated";
$LNG['escort_added_to_favorites'] = "";
$LNG['escort_in_city'] = "";
$LNG['viewed_short'] = "vue";
$LNG['escort_removed_from_favorites'] = "";
$LNG['more_than_years'] = "plus de %NUMBER% ans";
$LNG['comment_is_short'] = "Comment text is too short ( min. length %LENGTH% )";
$LNG['no_comment'] = "Pas de commentaires jusqu'a présent";
$LNG['already_comment_voted'] = "You have voted this comment already.";
$LNG['disabled'] = "Disabled";
$LNG['captcha_is_wrong'] = "Captcha text is wrong";
$LNG['additional_city_premium_spot_1_city'] = "Additional City Premium Spot - 1 City";
$LNG['additional_city_premium_spot_2_cities'] = "Additional City Premium Spot - 2 Cities";
$LNG['additional_city_premium_spot_3_cities'] = "Additional City Premium Spot - 3 Cities";
$LNG['add_comment'] = "Add a comment";
$LNG['sexindex_bottom_text'] = "Gemäss Statistik sind Begriffe die etwas mit Sex und Erotik zu tun haben die meist gesuchten im Internet. 98 Prozent der Männer waren gemäss Umfragen schon mindestens einmal auf Pornoseiten. Beliebt sind auch Suchbegriffe wie Kontaktanzeigen, Bordell, Sex Clubs, Escort, ficken, Transen etc. In der Schweiz gibt es eine Vielzahl an Sex Seiten und Sexführern. Und manchmal ist es schwierig den Ueberblick zu behalten. Denn wirklich gute Sex-Angebote, wo man auch das findet was man sucht, sind rar. Oder ist es Ihnen nicht auch schon so ergangen? Sie tippen bei einer Suchmaschine den Begriff „Bordell“ ein und landen, statt auf einem Sexführer, auf einer kostenpflichtigen Seite mit Kontaktanzeigen? Oder Sie haben Lust auf schnellen Sex und suchen ein Bordell in Ihrer Nähe wo Sie richtig geil ficken können? Statt dessen werden Sie auf eine Seite geführt wo Sie nur ein kleines Sexangebot haben und wenige Escort Girls / Sex Clubs aufgeführt sind. Wie gesagt – es ist ziemlich schwierig. Einige Seiten haben sich etabliert und decken gewisse Sparten ab. Für Transen Kontakte zum Beispiel eignet sich das Portal happysex hervorragend. Denn happysex hat in den meisten Regionen der Schweiz Kontaktanzeigen bzw. Sexanzeigen von Transen. Auch Bordelle, Escort Girls und Sex Clubs werden aufgeführt. Jedoch nimmt happysex im direkten Vergleich zu anderen Sex Seiten vom Design und Aufbau her einer der hinteren Plätze ein. Bei uns auf Sexindex legen wir wert auf Aktualität der Sex Kontakt Anzeigen und Sedcards der Escort Girls. Wir nehmen regelmässig neue Bordelle, Sexstudios, private Frauen, Escorts und Salons in unsere Datenbank auf. Alle Sedcards sind mit Fotos und Details und sind einheitlich präsentiert. Auch die Werbung versuchen wir dezent zu halten – jedoch brauchen wir die Banner um für Sie weiterhin ein gratis Sexführer zu bleiben. In diesem Sinne – ficken ist die schönste Nebensache der Welt – und ob Sie auf sexindex oder happysex fündig werden spielt schlussendlich keine Rolle. Happy hunting!";
$LNG['incall_rates'] = "Incall Rates";
$LNG['latest_viewed'] = "Latest Girls Viewed";
$LNG['outcall_rates'] = "Outcall Rates";
$LNG['signin_success'] = "Signin success !";
$LNG['hits'] = "Hits";
$LNG['signup_for_free'] = "<p>This and other functions are only available for registered users.</p>
<p>Signup now FOR FREE:</p>
";
$LNG['100_verified'] = "100% verified";
$LNG['co_uk_bottom_text'] = "";
$LNG['male_escorts'] = "";
$LNG['tv_ts'] = "";
$LNG['service_69_position'] = "69";
$LNG['service_anal_sex'] = "Sexe anal";
$LNG['service_cum_in_mouth'] = "Ejaculation dans la bouche";
$LNG['service_cum_on_face'] = "Ejaculation faciale";
$LNG['service_dildo_play_toys'] = "Jeux avec gode/sextoys";
$LNG['service_french_kissing'] = "Embrasser avec la langue";
$LNG['service_girlfriend_experience_gfe'] = "Expérience avec la copine (GFE)";
$LNG['service_kissing'] = "Embrasser";
$LNG['service_oral_with_condom'] = "Sucer avec capote";
$LNG['service_oral_without_condom'] = "Sucer sans capote";
$LNG['service_oral_without_condom_to_completion'] = "Sucer sans capote et jouir";
$LNG['service_sex_in_different_positions'] = "Sexe dans différentes positions";
$LNG['service_ball_licking_and_sucking'] = "Lécher et sucer les testicules";
$LNG['service_deep_throat'] = "Gorge profonde";
$LNG['service_dirtytalk'] = "Paroles cochonnes";
$LNG['service_double_penetration'] = "Double pénétration";
$LNG['service_extraball'] = "Plusieurs éjaculations";
$LNG['service_facesitting'] = "S'asseoir sur le visage";
$LNG['service_fisting'] = "Fist-fucking";
$LNG['service_full_body_sensual_massage'] = "Massage sensuel intégral";
$LNG['service_gangbang'] = "Gang-bang";
$LNG['service_kamasutra'] = "Kamasutra";
$LNG['service_lesbian_sex_games'] = "Jeux sexuels lesbiens";
$LNG['service_lingerie'] = "Lingerie";
$LNG['service_masturbate'] = "Masturbation";
$LNG['service_oral_without_condom_swallow'] = "Sucer sans préservativ et avaler";
$LNG['service_outdoor_sex'] = "Sexe à l'extérieur";
$LNG['service_pornstar_experience_pse'] = "Expérience de star du porno (PSE)";
$LNG['service_private_photos'] = "Photos privées";
$LNG['service_prostate_massage'] = "Massage de la prostate";
$LNG['service_rimming_give'] = "Lécher l'anus (actif)";
$LNG['service_rimming_recieve'] = "Lécher l'anus (passif)";
$LNG['service_snowballing'] = "Snowballing";
$LNG['service_striptease_lapdance'] = "Striptease/Lapdance";
$LNG['service_tantric'] = "Tantrique";
$LNG['service_bdsm'] = "BDSM";
$LNG['service_bondage'] = "Bondage";
$LNG['service_clinic_sex'] = "Sexe médical";
$LNG['service_fetish'] = "Fétischisme";
$LNG['service_foot_fetish'] = "Fétischisme des pieds";
$LNG['service_golden_shower_give'] = "Douche dorée (donneur)";
$LNG['service_golden_shower_recieve'] = "Douche dorée (receveur)";
$LNG['service_lather_latex_pvc'] = "Cuir / latex / PVC";
$LNG['service_mistress_hard'] = "Maîtresse (hard)";
$LNG['service_mistress_soft'] = "Maîtresse (soft)";
$LNG['service_role_play_and_fantasy'] = "Jeux de rôles et fantaisies";
$LNG['service_spanking_give'] = "Fessée (donneur)";
$LNG['service_spanking_recieve'] = "Fessée (receveur)";
$LNG['service_strap_on'] = "Gode-ceinture";
$LNG['service_submissive_slave_hard'] = "Soumission/esclave (hard)";
$LNG['service_submissive_slave_soft'] = "Soumission/esclave (soft)";
$LNG['new_setcards_online'] = "%COUNT% new escort listings";
$LNG['use_our_contact_form'] = "Alternatively please use our contact form below";
$LNG['heterosexual'] = "Hétérosexuelle";
$LNG['bisexual'] = "Bisexuelle";
$LNG['homosexual'] = "Homosexuelle";
$LNG['cup_size'] = "Cup";
$LNG['pubic_hair_shaved_completely'] = "Entièrement rasée";
$LNG['pubic_hair_shaved_mostly'] = "Partiellement rasée";
$LNG['pubic_hair_trimmed'] = "Circoncis";
$LNG['pubic_hair_all_natural'] = "Entièrement naturel";
$LNG['ocasionally'] = "Occasionnellement ";
$LNG['drinking'] = "Boit de l'alcool";
$LNG['orientation'] = "Orientation";
$LNG['dinner-date'] = "dinner date";
$LNG['phone_alternative'] = "Phone alternative";
$LNG['sms_only'] = "Uniquement SMS";
$LNG['phone_instr_no_withheld'] = "Aucun numéro cache";
$LNG['phone_instructions_other'] = "Autres consignes pour le téléphone";
$LNG['club_agency_name'] = "Club/Agency name";
$LNG['street'] = "Street";
$LNG['pubic_hair'] = "Epilation du maillot";
$LNG['hotel_room'] = "Chambre d'hôtel";
$LNG['additional-hour'] = "Toute heure additionnelle";
$LNG['weekend'] = "weekend";
$LNG['club_studio'] = "Club/Studio";
$LNG['private_apartment'] = "Appartement privé";
$LNG['hotel_visits_only'] = "Uniquement visites à l'hôtel";
$LNG['home_visits_only'] = "Uniquement visites à la maison";
$LNG['hotel_and_home_visits'] = "Visites à l'hôtel et à la maison";
$LNG['pv2_tab_biography'] = "Biographie";
$LNG['pv2_top_bar_btn_back'] = "Retour";
$LNG['pv2_top_bar_btn_tours'] = "Tours";
$LNG['pv2_top_bar_btn_photos'] = "Photos";
$LNG['pv2_tab_about_me'] = "A propos de moi";
$LNG['pv2_tab_languages'] = "Langues";
$LNG['pv2_tab_working_cities'] = "Lieux de travail";
$LNG['pv2_tab_services'] = "Service";
$LNG['pv2_tab_working_times'] = "Heures de travail";
$LNG['pv2_tab_prices'] = "Tarifs";
$LNG['pv2_tab_contact_info'] = "Coordonnées";
$LNG['pv2_btn_save'] = "Save";
$LNG['pv2_btn_reset'] = "Reset";
$LNG['ethnic_mixed'] = "Métisse";
$LNG['pv2_btn_settings'] = "Paramètres";
$LNG['pv2_btn_add_edit_profile'] = "Mon profil";
$LNG['pv2_btn_add_edit_photos'] = "Organiser les sorties";
$LNG['pv2_btn_set_tours'] = "Tourné de ville";
$LNG['pv2_btn_100p_verified'] = "100% Verified";
$LNG['pv2_btn_logout'] = "Logout";
$LNG['pv2_label_country'] = "Pays";
$LNG['pv2_label_city'] = "Ville";
$LNG['pv2_settings_title'] = "Modifier profil";
$LNG['pv2_text_change_metric_system_to_royal'] = "<strong>ATTENTION: </strong>all units are displayed in Metric System. If you would like to change it to Royal System, <a href='#' onclick='return changeSystem(2)'>click here</a>";
$LNG['pv2_text_change_metric_system_to_metric'] = "<strong>ATTENTION: </strong>all units are displayed in Royal System. If you would like to change it to Metric System, <a href='#' onclick='return changeSystem(1)'>click here</a>";
$LNG['pv2_tip_showname'] = "Name which will appear on the site on your setcard";
$LNG['pv2_tip_special_characteristics'] = "Décrivez-vous et écrivez des informations supplémentaires. Veuillez mentionner toute caractéristique particulière comme par exemple si vous avez des tatouages ou des piercings.  ";
$LNG['pv2_tip_about_me'] = "Décrivez-vous et écrivez des informations supplémentaires. Veuillez mentionner toute caractéristique particulière comme par exemple si vous avez des tatouages ou des piercings. ";
$LNG['pv2_tip_country'] = "Si vous choisissez un pays autre que  %COUNTRY%, votre<br/>profil sera listé à l'échelle internationale sur EscortDirectory.com";
$LNG['pv2_note_max_cities'] = "<strong>Remarque: </strong> si vous proposez un service étendu, vous pouvez choisir jusqu'à 5 villes. Pour déterminer votre ville de base, double-cliquez tout simplement dessus.";
$LNG['pv2_tip_available_for'] = "Please mention where you offer your services";
$LNG['pv2_label_incall_other'] = "Autres ( veuillez fournir des détails)";
$LNG['pv2_label_outcall_other'] = "Autres ( veuillez fournir des détails)";
$LNG['pv2_label_working_schedule'] = "Mon emploi du temps est le suivant";
$LNG['pv2_label_no_incall_rates'] = "Vous n'avez pas encore fixé vos honoraires. ";
$LNG['pv2_label_no_outcall_rates'] = "Vous n'avez pas encore fixé vos honoraires. ";
$LNG['pv2_title_public_photos'] = "Photos publiques de %SHOWNAME%";
$LNG['pv2_text_public_photos'] = "Veuillez charger au moins 3 photos pour votre book. Autrement, votre profil ne pourra être accepté. ";
$LNG['pv2_title_private_photos'] = "Photos privées de %SHOWNAME%";
$LNG['pv2_text_private_photos'] = "Vous pouvez charger des photos privées qui seront uniquement visibles pour les membres premium et ne seront pas publiées sur votre book. ";
$LNG['pv2_escort_heading'] = "WELCOME %showname% TO YOUR PRIVATE AREA";
$LNG['pv2_escort_top_text'] = "Bienvenue dans votre zone privée.
Depuis cette zone, vous pouvez gérer vos profils d'escortes, organiser les tournées ou les vacances. Vous y êtes également informé en permanence des actualités et des mises à jour. 
";
$LNG['pv2_main_nav'] = "Navigation principale";
$LNG['pv2_label_email'] = "Courriel";
$LNG['pv2_settings_profile'] = "Profil";
$LNG['pv2_settings_change_password'] = "Modifier le mot de passe";
$LNG['pv2_label_enter_current_password'] = "Votre mot de passe";
$LNG['pv2_label_enter_new_password'] = "Votre nouveaux mot de passe";
$LNG['pv2_label_reenter_new_password'] = "Confirmer le mot de passe";
$LNG['pv2_settings_newsletters'] = "Newsletter";
$LNG['pv2_settings_newsletters_question'] = "Voulez-vous obtenir le newsletter de %sitename%?";
$LNG['pv2_option_yes'] = "Oui";
$LNG['pv2_option_no'] = "Non";
$LNG['pv2_section_basic_bio'] = "Basic BIO";
$LNG['pv2_label_showname'] = "Showname";
$LNG['pv2_label_gender'] = "Sexe";
$LNG['pv2_label_age'] = "Age";
$LNG['pv2_label_ethnicity'] = "Groupe ethnique";
$LNG['pv2_label_nationality'] = "Nationalilté";
$LNG['pv2_section_vital_statistics'] = "Mensurations";
$LNG['pv2_label_hair_color'] = "Couleur de cheveux";
$LNG['pv2_label_eye_color'] = "Couleur des yeux";
$LNG['pv2_label_height'] = "Taille";
$LNG['pv2_label_weight'] = "Poids";
$LNG['pv2_label_dress_size'] = "Taille de vêtement";
$LNG['pv2_label_shoe_size'] = "Taille de chaussures";
$LNG['pv2_label_bust_waist_hip'] = "Bust-Waist-Hip";
$LNG['pv2_label_cup_size'] = "Cup";
$LNG['pv2_label_pubic_hair'] = "Epilation du maillot";
$LNG['color_blond'] = "Blond";
$LNG['color_light_brown'] = "Châtain clair";
$LNG['color_brunette'] = "Châtain foncé";
$LNG['color_black'] = "Brun";
$LNG['color_red'] = "Roux";
$LNG['color_brown'] = "Brown";
$LNG['color_green'] = "Green";
$LNG['color_blue'] = "Bleus";
$LNG['color_gray'] = "Gray";
$LNG['pv2_section_about_me'] = "A propos de moi";
$LNG['pv2_section_additional_information'] = "Informations supplémentaires";
$LNG['pv2_label_smoking'] = "Fumeur";
$LNG['pv2_label_drinking'] = "Boit de l'alcool";
$LNG['pv2_label_special_characteristics'] = "Caractéristiques spéciales";
$LNG['pv2_section_spoken_languages'] = "Langues";
$LNG['pv2_label_basic'] = "Basiques";
$LNG['pv2_label_fair'] = "Moyennes";
$LNG['pv2_label_good'] = "Bonnes";
$LNG['pv2_label_excellent_native'] = "Excellentes/langue maternelle";
$LNG['pv2_label_display_more_languages'] = "afficher plus langues";
$LNG['pv2_section_working_locations'] = "Lieux de travail";
$LNG['pv2_section_available_for'] = "Disponible pour";
$LNG['pv2_label_incall'] = "Incall";
$LNG['pv2_label_outcall'] = "Outcall";
$LNG['pv2_label_private_apartment'] = "Appartement privé";
$LNG['pv2_label_hotel_room'] = "Chambre d'hôtel";
$LNG['pv2_label_club_studio'] = "Club / Studio";
$LNG['pv2_label_hotel_visits_only'] = "Uniquement visites à l'hôtel";
$LNG['pv2_label_home_visits_only'] = "Uniquement visites à la maison";
$LNG['pv2_label_hotel_and_home_visits'] = "Visites à l'hôtel et à la maison";
$LNG['pv2_section_sexual_orientation'] = "Orientation sexuelles";
$LNG['pv2_section_services_offered_for'] = "Services offerts pour";
$LNG['pv2_tip_sexual_orientation'] = "";
$LNG['pv2_section_provided_services'] = "Services disponible";
$LNG['pv2_title_most_common_services'] = "Les plus demandés";
$LNG['pv2_title_extra_services'] = "En supplément";
$LNG['pv2_title_fetish_bizzare_services'] = "Fétischisme / SM";
$LNG['pv2_label_surcharge'] = "surcharge";
$LNG['pv2_label_i_am_available_24_7'] = "Je suis disponible 24 h sur 24, 7 j/7 ";
$LNG['pv2_section_working_times'] = "Heures de travail";
$LNG['pv2_label_or'] = "ou";
$LNG['pv2_label_select_all'] = "Tout sélectionner";
$LNG['pv2_label_from'] = "de";
$LNG['pv2_label_to'] = "à";
$LNG['pv2_section_rates'] = "Tarifs";
$LNG['pv2_section_incall_rates'] = "Tarifs incall";
$LNG['pv2_label_time'] = "Heure";
$LNG['pv2_label_unit'] = "Unité";
$LNG['pv2_label_amount'] = "Montant";
$LNG['pv2_label_currency'] = "Devise";
$LNG['pv2_section_outcall_rates'] = "Tarifs outcall";
$LNG['pv2_label_quick_options'] = "Quick Options";
$LNG['pv2_label_overnight'] = "Heures de nuit";
$LNG['pv2_label_any_additional_hour'] = "Toute heure additionnelle";
$LNG['pv2_label_dinner_date'] = "dinner date";
$LNG['pv2_label_weekend'] = "weekend";
$LNG['pv2_label_your_incall_rates'] = "Vos tarifs incall";
$LNG['pv2_label_your_outcall_rates'] = "Vos tarifs outcall";
$LNG['pv2_section_contact_information'] = "Coordonnées";
$LNG['pv2_section_contact'] = "Contact";
$LNG['pv2_label_phone_number'] = "Numéro de téléphone";
$LNG['pv2_label_alternative_phone'] = "Autre numéro de téléphone";
$LNG['pv2_label_phone_instructions'] = "Consignes pour le téléphone";
$LNG['phone_instr_sms_call'] = "SMS et appel";
$LNG['phone_instr_sms_only'] = "Uniquement SMS";
$LNG['phone_instr_no_sms'] = "pas de SMS";
$LNG['pv2_label_no_withheld_numbers'] = "Aucun numéro cache";
$LNG['pv2_label_phone_instr_other'] = "Autres";
$LNG['pv2_section_web'] = "Web";
$LNG['pv2_label_email_address'] = "Courriel";
$LNG['pv2_label_website_url'] = "Site web / URL";
$LNG['pv2_section_address_details'] = "Coordonnées postales";
$LNG['pv2_label_club_name'] = "Nom du club";
$LNG['pv2_label_street_n'] = "Rue / no";
$LNG['pv2_section_photos'] = "Photos";
$LNG['pv2_section_my_tours'] = "Mes tours";
$LNG['pv2_title_add_your_tours'] = "Ajouter tours";
$LNG['pv2_label_view_profile'] = "voir profil";
$LNG['pv2_title_planned_active_tours'] = "Tours prévus / actifs";
$LNG['pv2_label_range'] = "Triage";
$LNG['pv2_label_location'] = "Région";
$LNG['pv2_label_contact'] = "Contact";
$LNG['pv2_label_from_date'] = "Date de";
$LNG['pv2_label_until_date'] = "Date à";
$LNG['pv2_label_tour_phone'] = "Tour téléphone";
$LNG['pv2_label_tour_email'] = "Tour Email";
$LNG['pv2_agency_top_text'] = "Bienvenue dans votre zone privée.
Depuis cette zone, vous pouvez gérer vos profils d'escortes, organiser les tournées ou les vacances. Vous y êtes également informé en permanence des actualités et des mises à jour. 
";
$LNG['pv2_agency_heading'] = "BIENVENUE %agency_name% DANS VOTRE ZONE PRIVE";
$LNG['pv2_btn_add_escort_profile'] = "Add Escort Profile";
$LNG['pv2_btn_agency_profile'] = "Agency Profile";
$LNG['pv2_btn_manage_models'] = "Manage Models";
$LNG['pv2_btn_city_tours'] = "City Tours";
$LNG['pv2_tab_finish'] = "Terminer";
$LNG['pv2_btn_next_step'] = "Étape suivante";
$LNG['pv2_btn_go_back'] = "En retour";
$LNG['pv2_label_zip_postal_code'] = "Code postal";
$LNG['pv2_btn_finish'] = "Terminer";
$LNG['pv2_btn_private_area'] = "Private Area";
$LNG['pv2_agency_create_profile_text'] = "<p>Remplissez uniquement les informations nécessaires grâce au formulaire pratique étape par étape. Toutes les informations peuvent être modifiées à tout moment.</p>";
$LNG['verify_form_confirmed'] = "Veuillez sélectionner la forme selon laquelle vous souhaiteriez recevoir confirmation de votre demande de rendez-vous.";
$LNG['verify_picture_upload'] = "Passport Photo Upload";
$LNG['pv2_section_manage_models'] = "Administrer escortes";
$LNG['pv2_title_your_active_models'] = "Vos escortes actives";
$LNG['pv2_title_your_inactive_models'] = "Vos escortes inactives";
$LNG['verify_number'] = "no.";
$LNG['pv2_member_heading'] = "BIENVENUE %username% DANS VOTRE DOMAINE RESERVE";
$LNG['pv2_btn_member_profile'] = "Member Profile";
$LNG['pv2_label_your_username'] = "Votre nom d'utilisateur";
$LNG['pv2_section_user_profile'] = "Profil d'utilisateur";
$LNG['pv2_member_top_text'] = "";
$LNG['pv2_btn_my_favorites'] = "My Favorites";
$LNG['incall_services'] = "Incall escort services";
$LNG['outcall_services'] = "Outcall";
$LNG['sms_and_call'] = "SMS et appel";
$LNG['pv2_escort_create_profile_text'] = "<p>Remplissez uniquement les informations nécessaires grâce au formulaire pratique étape par étape. Toutes les informations peuvent être modifiées à tout moment.</p>";
$LNG['languages_sa'] = "Arabic";
$LNG['languages_be'] = "Belgian";
$LNG['languages_bg'] = "Bulgarian";
$LNG['languages_cn'] = "Chinese";
$LNG['languages_cz'] = "Czech";
$LNG['languages_hr'] = "Croatian";
$LNG['languages_nl'] = "Dutch";
$LNG['languages_fi'] = "Finnisch";
$LNG['languages_gr'] = "Greek";
$LNG['languages_hu'] = "Hungarian";
$LNG['languages_in'] = "Indian";
$LNG['languages_jp'] = "Japanese";
$LNG['languages_lv'] = "Latvinian";
$LNG['languages_pl'] = "Polish";
$LNG['languages_ro'] = "Romanian";
$LNG['languages_tr'] = "Turkish";
$LNG['language_tr'] = "Turkish";
$LNG['no_sms'] = "pas de SMS";
$LNG['escort_profile_disabled'] = "Escort profile is disabled";
$LNG['pv2_weekday_monday'] = "Lundi";
$LNG['pv2_weekday_tuesday'] = "Mardi";
$LNG['pv2_weekday_wednesday'] = "Mercredi";
$LNG['pv2_weekday_thursday'] = "Jeudi";
$LNG['pv2_weekday_friday'] = "Vendredi";
$LNG['pv2_weekday_saturday'] = "Samedi";
$LNG['pv2_weekday_sunday'] = "Dimanche";
$LNG['pv2_label_zip_code'] = "Code postal";
$LNG['su2_choose_membership'] = "Sélectionne ton type d'abonnement !";
$LNG['su2_thank_you_for_interest'] = "Nous vous remercions pour votre intérêt pour notre site. Veuillez choisir ci-dessous le type de compte que vous désirez et suivre tout simplement les étapes suivantes.";
$LNG['services_extra_charge'] = "";
$LNG['su2_escort_text'] = "<p class=\"text\">Tu cherches la meilleure des publicités?</p>
			<p>Inscris-toi dès maintenant gratuitement dans le plus grand répertoire d'escortes au monde. Actuellement, nous comptons plus de 4 millions de clients par mois dans notre réseau. Utilise notre popularité pour faire toi-même ta publicité dès maintenant.  <span class=\"red\">GRATUITE!</span></p>

			<ul>
				<li>Charger facilement son book</li>
				<li>Utilisateurs ciblés de notre réseau</li>
				<li>Inscription gratuite</li>
			</ul>";
$LNG['su2_member_text'] = "<p class=\"text\">Tu cherches des filles chaudes?</p>
			<p>L'inscription à %sitename% est entièrement gratuite et comporte de nombreux avantage. </p>

			<ul>
				<li>Ajouter des commentaires</li>
				<li>Gérer ta liste de favoris</li>
				<li>Accès à la recherche avancée</li>
				<li>Consulter la liste </li>
			</ul>";
$LNG['su2_please_fill_out_form'] = "Veuillez remplir le formulaire suivant !";
$LNG['su2_choose_your_business'] = "Veuillez choisir votre offre";
$LNG['su2_independent_escort'] = "Independent Escort / Private Girl";
$LNG['su2_agency_escort'] = "Escort Agency / Club";
$LNG['su2_username'] = "Nom d'utilisateur";
$LNG['su2_password'] = "Mot de passe";
$LNG['su2_confirm_password'] = "Confirmer le mot de passe";
$LNG['su2_email'] = "Email";
$LNG['su2_congratulations'] = "Félicitations, c'est presque terminé. ";
$LNG['su2_thank_you'] = "Merci pour votre inscription!";
$LNG['su2_email_sent_text'] = "Un courriel vient d'être envoyé à votre adresse e-mail avec un lien de confirmation. Afin de compléter votre inscription, cliquez sur ce lien ou copier/coller-le dans la barre d'adresse de votre navigateur. ";
$LNG['su2_note_text'] = "<span class=\"red\">Remarque:</span> parfois, nos courriels arrivent dans le dossier des spams. ";
$LNG['p_address'] = "Adresse";
$LNG['pv2_to_get_attention'] = "To get attention from more customers and to be more successful in low season times, %sitename% offers you to set every week two happy hours up to 5 hours!";
$LNG['pv2_happy_hour_guideline'] = "Happy hour guideline";
$LNG['pv2_ul_agency_text_1'] = "You can set your happy hour prices and the date / time in the form below";
$LNG['pv2_ul_agency_text_2'] = "There's a maximum happy hour period of 5 hours";
$LNG['pv2_ul_agency_text_3'] = "The happy hour feature can only be used twice a week or for two girls regardless the time periods are less than 5 hours";
$LNG['pv2_hh_note_text'] = "NOTE: You can chooe a maximum of 2 girls";
$LNG['pv2_label_motto'] = "What is the motto of your happy hour?";
$LNG['pv2_tip_motto'] = "if you like you can give your happy hour a motto (e.g. “I’m bored and horny” or “Best<br/> offer ever”). Write whatever you like which could rise attention to the customer.";
$LNG['pv2_label_date'] = "Date";
$LNG['pv2_label_time_from'] = "heure de";
$LNG['pv2_label_time_until'] = "heure à";
$LNG['pv2_label_save_hh'] = "Save this happy hour for every week";
$LNG['pv2_tip_save_hh'] = "If you choose this option, the system<br/> activates your settings for the happy<br/> hour automatically every week.";
$LNG['pv2_happy_hour_rates'] = "Happy hour rates";
$LNG['pv2_current_incall_rates'] = "Votre tarifs incall";
$LNG['pv2_current_outcall_rates'] = "Votre tarifs outcall";
$LNG['pv2_section_happy_hour'] = "Happy Hour";
$LNG['pv2_section_agency_happy_hour_step_1'] = "Step 1: Please select the girl(s) for happy hour";
$LNG['pv2_section_agency_happy_hour_step_2'] = "Step 2: Select Happy Hour";
$LNG['pv2_section_escort_happy_hour_step_1'] = "Please set your happy hour";
$LNG['pv2_escort_to_get_attention'] = "To get attention from more customers and to be more successful in low season times, %sitename% offers you to set every week a happy hour up to 5 hours !";
$LNG['pv2_ul_escort_text_1'] = "You can set your happy hour prices and the date / time in the form below";
$LNG['pv2_ul_escort_text_2'] = "There's a maximum happy hour period of 5 hours";
$LNG['pv2_ul_escort_text_3'] = "The happy hour feature can only be used once a week regardless of the time period is less than 5 hours.";
$LNG['pv2_btn_happy_hour'] = "Happy Hour";
$LNG['profile_mention_text'] = "Please mention %sitename% when you call";
$LNG['sprt_open_tickets_list'] = "Open support tickets are listed below";
$LNG['sprt_closed_tickets_list'] = "All closed support tickets are listed below";
$LNG['sprt_no_opened_tickets'] = "No opened tickets";
$LNG['sprt_no_closed_tickets'] = "No closed tickets";
$LNG['support'] = "Assistance technique";
$LNG['sprt_date'] = "Date";
$LNG['sprt_subject'] = "Subject";
$LNG['sprt_ticket'] = "Ticket";
$LNG['sprt_form_text'] = "If you are experiencing difficulties with your personal account feel free to enter a support ticket into our system.";
$LNG['sprt_your_message'] = "Your message";
$LNG['sprt_thank_you'] = "Thank you for your message.";
$LNG['sprt_ticket_number'] = "Your ticket number is: #%ticket_id%<br/>
		(please use this number for future reference)";
$LNG['sprt_usually_you_will'] = "Usually you will get an answer to your request within 24 hours<br/>
		during the week, on weekends it may take longer.";
$LNG['sprt_replied_on'] = "%name% replied on %date%";
$LNG['sprt_you'] = "you";
$LNG['sprt_ticket_n'] = "Ticket Nr";
$LNG['sprt_reply_to_message'] = "Reply to this message";
$LNG['sprt_support_ticket'] = "Ticket";
$LNG['other_city_escorts'] = "Other <a href='%city_url%'>%city_name% Escorts</a>";
$LNG['send_message_to'] = "Send message to %showname%";
$LNG['message_sent_to_escort'] = "Your message has been successfully sent to %showname%!";
$LNG['added'] = "added %date%";
$LNG['hour_rate'] = "%number% hour rate";
$LNG['pv2_profile_was_saved'] = "Your profile was successfully saved!";
$LNG['pv2_profile_needs_to_be_approved'] = "<strong>Note</strong> that your profile needs to be approved by our administrators and before approval your changes will not appear on the website.";
$LNG['pv2_add_now_photos'] = "Add now photos for this profile, click here";
$LNG['pv2_up_browse_files'] = "Browse Files";
$LNG['pv2_up_clear_list'] = "Clear List";
$LNG['pv2_up_start_upload'] = "Start Upload";
$LNG['pv2_up_overall_progress'] = "Overall Progress";
$LNG['pv2_up_file_progress'] = "File Progress";
$LNG['visiting'] = "visitant";
$LNG['status'] = "Status";
$LNG['this_girl_is_right_now_on_tour'] = "This girl is right now on tour";
$LNG['available_in'] = "Available in";
$LNG['top20_colored'] = "Copine du <span style=\"color: #f00\">Mois</span>";
$LNG['top20_history'] = "Historique Copine du <span style=\"color: #f00\">Mois</span>";
$LNG['last_review'] = "%showname% Reviews";
$LNG['currently'] = "Currently";
$LNG['latest_by'] = "La derniere par";
$LNG['french'] = "Francaise Origin";
$LNG['whole_france'] = "Whole France";
$LNG['over_count_reviews'] = "Over %count% reviews.";
$LNG['go_reviews_overview'] = "Go to reviews overview";
$LNG['go_escort_profile'] = "Go to escort's profile";
$LNG['about_the_meeting'] = "About the meeting";
$LNG['she_provided_services'] = "She also provided these services";
$LNG['signed_up'] = "signed up";
$LNG['member_reviews'] = "Member Reviews";
$LNG['top_count_ladies'] = "Top %count% Ladies";
$LNG['last_reviewer'] = "Last reviewer";
$LNG['top_count_reviewers'] = "Top %count% Reviewers";
$LNG['reviews_search'] = "Reviews search";
$LNG['france_only'] = "France only";
$LNG['only_100_fake_free_reviews'] = "Only 100% fake free reviews";
$LNG['pv2_btn_my_reviews'] = "My reviews";
$LNG['msg_no_review'] = "You have not any reviews.";
$LNG['pv2_section_escort'] = "Escorte";
$LNG['pv2_label_date_of_the_meeting'] = "Date de la rencontre";
$LNG['pv2_label_city_of_the_meeting'] = "Ville de la rencontre";
$LNG['pv2_label_other'] = "OTHER";
$LNG['pv2_label_meeting_place'] = "Où l'avez-vous rencontrée";
$LNG['pv2_label_meeting_length'] = "Durée de la rencontre";
$LNG['pv2_label_meeting_costs'] = "Prix de la rencontre";
$LNG['pv2_label_rate_her_looks'] = "Évaluez son apparence";
$LNG['pv2_label_rate_her_services'] = "Évaluez ses services";
$LNG['pv2_section_services'] = "Services";
$LNG['pv2_label_kissing'] = "Embrassade";
$LNG['pv2_label_blowjob'] = "Oral";
$LNG['pv2_label_cumshot'] = "Éjaculation";
$LNG['pv2_label_69'] = "69";
$LNG['pv2_label_anal'] = "Anal";
$LNG['pv2_label_sex'] = "Pénétration";
$LNG['pv2_label_multiple_times_sex'] = "Plusieurs relations sexuelles";
$LNG['pv2_label_breast'] = "Poitrine";
$LNG['pv2_label_attitude'] = "Attitude";
$LNG['pv2_label_conversation'] = "Conversation";
$LNG['pv2_label_availability'] = "Disponibilité";
$LNG['pv2_label_photos'] = "Photos";
$LNG['pv2_label_services_comments'] = "Commentaires sur ses services";
$LNG['pv2_section_review'] = "Évaluation";
$LNG['pv2_section_get_trusted_review'] = "Plus d'informations";
$LNG['pv2_label_trusted_review_example'] = "Svp compléter le formulaire ci-dessous";
$LNG['pv2_label_t_your_info'] = "Vos Info";
$LNG['pv2_label_t_user_info_example'] = "(votre Nickname)";
$LNG['pv2_label_t_meeting_date'] = "Quand vous la rencontrez";
$LNG['pv2_label_t_meeting_date_example'] = "(date)";
$LNG['pv2_label_t_meeting_time'] = "À quel heure vous la rencontrez";
$LNG['pv2_label_t_meeting_time_example'] = "";
$LNG['pv2_label_t_meeting_duration'] = "Durée de rendez-vous";
$LNG['pv2_label_t_meeting_duration_example'] = "";
$LNG['pv2_label_t_meeting_place'] = "Où l'avez-vous rencontrée";
$LNG['pv2_label_t_meeting_place_example'] = "(hôtel, chez elle, chez vous)";
$LNG['pv2_label_t_comments'] = "Commentaires";
$LNG['pv2_label_t_comments_example'] = "";
$LNG['pv2_label_services_comments_example'] = "Décrivez la part sexuelle, ex : Services spéciaux (S/M, toys, etc).";
$LNG['easy'] = "Facile de réserver";
$LNG['hard'] = "Difficile de réserver";
$LNG['genuine'] = "Photo 100% réelle";
$LNG['fake'] = "Fake";
$LNG['hard_to_belive'] = "Difficile de croire que c'est elle";
$LNG['at_my_flat'] = "Chez Moi";
$LNG['at_her_apartament'] = "À son appartement";
$LNG['her_hotel'] = "À son Hôtel";
$LNG['my_hotel'] = "À mon Hôtel";
$LNG['pv2_label_slogan'] = "Slogan";
$LNG['pv2_tip_slogan'] = "Put here a slogan or keyword wich describes you and/or <br/> your service the best. Maximum 20 letters allowed.";
$LNG['user_ip_duplicity'] = "member duplicity";
$LNG['same_escort_and_ip'] = "escort duplicity";
$LNG['cookie_id_duplicity'] = "cookie duplicity";
$LNG['review_not_approved'] = "Not approved";
$LNG['review_approved'] = "Approved";
$LNG['review_disabled'] = "Disabled";
$LNG['sms_not_sent'] = "Not sent";
$LNG['sms_sent'] = "Sent";
$LNG['sms_confirmed'] = "Confirmed";
$LNG['sms_rejected'] = "Rejected";
$LNG['sms_mismatch'] = "Mismatch";
$LNG['more_days'] = "more days";
$LNG['i_am_right_now_on_tour'] = "I am right now on tour";
$LNG['i_am_coming_soon_to_tour'] = "I am coming soon to tour";
$LNG['review_sms_template'] = "New review! %user_info% met you on %date% at %meeting_place% for %duration% (%time%). Please confirm with 'Yes' or 'No' and include %unique_number% in your reply.";
$LNG['wt_24_7_text'] = "I am available 24/7";
$LNG['over_count_agencies'] = "Over %count% agencies.";
$LNG['spnsors_6annonce'] = "Spnsors 6annonce.com";
$LNG['100_photos_verified'] = "100% photos vérifiées!";
$LNG['100_photos_verified_text'] = "Toutes les photos sont authentiques y approuvé et confirmé l’équipe 6annonce.com.";
$LNG['suspicious_gallery'] = "Suspicious Gallery!";
$LNG['suspicious_gallery_text'] = "The photos provided don't look real and may be fake.";
$LNG['pv2_btn_up_to_premium'] = "PREMIUM UPGRADE";
$LNG['choose_your_card'] = "Choisissez votre mode de paiement";
$LNG['pay_credit_card'] = "payez par carte de crÉdit";
$LNG['latest_status_messages'] = "Latest status messages";
$LNG['display'] = "Display";
$LNG['all_rev'] = "All";
$LNG['remember_me'] = "Souvenez-vous de moi";
$LNG['pv2_add_escort_tour'] = "Add escort tour";
$LNG['your_tour_schedule'] = "Your tour schedule";
$LNG['commented'] = "Commented";
$LNG['rev_add_comment'] = "Add Comment";
$LNG['pv2_btn_my_escorts_reviews'] = "My Escorts Reviews";
$LNG['see_all_reviews'] = "See all reviews";
$LNG['cityzones'] = "Cityzones";
$LNG['regions'] = "Regions";
$LNG['slider_text'] = "Use slider to rank escort";
$LNG['forum'] = "Forum";
$LNG['cams'] = "Cams";
$LNG['chat'] = "Chat";
$LNG['pv2_label_home_city'] = "Ville";
$LNG['home_city'] = "Ville";
$LNG['save_me_the_login'] = "save me the login";
$LNG['vip_m_title_1'] = "Accès à la section complète de commentaires";
$LNG['vip_m_title_2'] = "Options de recherche avancée";
$LNG['vip_m_title_3'] = "Aucune publicité";
$LNG['vip_m_title_4'] = "Accès Photo Privée";
$LNG['vip_m_text_4'] = "De nombreuses escortes ont déposé des photos privées. Ces photos sont affichées uniquement pour un public sélectionné.
En tant que membre VIP, vous avez accès à toutes les photos privées de toutes les escortes du site.";
$LNG['vip_m_title_5'] = "Accès Forum Premium";
$LNG['vip_m_text_5'] = "En tant que membre VIP sur notre site, vous avez le privilège d'accéder au forum premium VIP dédié
uniquement à vous.";
$LNG['vip_m_title_6'] = "Icône Premium Sur Le Forum";
$LNG['vip_m_text_6'] = "Icône Premium Sur Le Forum   (premium icon in forum)

En tant que membre premium, vous aurez en plus de votre avatar personnel un drapeau spécial VIP.";
$LNG['vip_m_title_7'] = "Loterie Mensuelle";
$LNG['vip_m_text_7'] = "Chaque mois, nous sélectionnons un membre premium qui gagne un rendez-vous gratuit avec n'importe quelle fille de notre
site.  La valeur de ce rendez-vous est limitée à 300 Euro. Pas de paiement liquide.";
$LNG['vip_m_title_8'] = "Icône Vip Dans La Section Commentaires";
$LNG['vip_m_text_8'] = "Lorsque vous écrivez des commentaires en tant que membre VIP, vos commentaires sont plus fiables et confirmés
plus rapidement. De plus, votre nom d'utilisateur s'affiche en couleur or et est signalé par l'icône VIP
premium.";
$LNG['vip_m_title_9'] = "Formulaire Contact Vip";
$LNG['vip_m_text_9'] = "Les membres premium ont un formulaire de contact VIP spécial pour entrer en contact avec les escortes par email.
Le message sera signalé comme 'message VIP'.";
$LNG['vip_m_title_10'] = "Plus De Crédibilité";
$LNG['vip_m_text_10'] = "Contacter une escorte en tant que membre VIP a cet avantage que l'escorte voit que le message
vient d'un membre VIP premium de confiance. Cela augmente la crédibilité et accélère
le processus comme vous êtes privilégié.";
$LNG['choose_your_membership'] = "Choose your Membership!";
$LNG['vip_m_bottom_text'] = "<p>*Note: this value is up to 300 euro or 1h booking, so if you choose a ladies who costs 250 euro,</p>
		<p>their will be not payout of the 50 euro, this pirce canot be changed into cash or anything else.</p>
		<p>Details about the procedure receives the winner.</p>";
$LNG['description'] = "Description";
$LNG['normal_member'] = "Normal Member";
$LNG['premium_member'] = "Premium Member";
$LNG['euro'] = "euro";
$LNG['vip_m_page_title'] = "Inscription membre";
$LNG['vip_m_top_text'] = "<p>Nous vous remercions de vous être intéressé à notre site et de nous avoir rejoint !</p>
	<p>Inscrivez-vous comme membre premium et ayez la chance de gagner un <strong class='red'>FREE FUCK d'une valeur de 300 Euro !</strong></p>
	<p>* Choisissez l'escorte de vos rêves et nous vous l'offrirons le rendez-vous !</p>

	<p class='mt20'>Inscrivez-vous maintenant et recevez en tant que membre premium tous les avantages pour seulement 7,95 Euro /mois !</p>";
$LNG['example'] = "Example";
$LNG['mouse_over_to_see_details'] = "Mouse over to see details";
$LNG['available_24_7'] = "Available 24/7";
$LNG['pv2_remaining_cities'] = "<!-- Please do not touch strong tag, it is used by javascript -->
<strong>Attention: </strong>You can choose <strong id='cities-remain'>%count%</strong> more cities";
$LNG['p_not_approved'] = "not approved";
$LNG['today_last_day'] = "Today last day";
$LNG['bust-waist-hip'] = "Bust-Waist-Hip";
$LNG['see_my_sedcard'] = "See my sedcard";
$LNG['sign_in_bottom_text'] = " ";
$LNG['girl_is_on_tour_in'] = "Escorte en tournée à";
$LNG['share_mood'] = "Partagez vos émotions, pensées, sentiments ou souhaits du jour avec la communauté!
Entrez simplement un message personnel dans la case de messagerie instantanée ci-dessous - il sera
visible immédiatement sur votre sedcard et à travers le site et attirera beaucoup d'attention!
";
$LNG['select_escort'] = "Selectionnez escorte";
$LNG['instant_live_messages'] = "Messagerie instantanée";
$LNG['your_premium_models'] = "Vos modeles premium";
$LNG['available_premium_packages'] = "Available Premium Packages";
$LNG['offering_wide_rage'] = "... vous propose un large eventail de forfaits haut de gamme. 
Jetez un coup d'oeil et choisissez celui qui repond le mieux a vos besoins. 
Si vous avez des questions ou si vous desirez reserver un forfait haut de gamme:";
$LNG['package'] = "Package";
$LNG['days_main_site'] = "Days Main Site";
$LNG['city_tour_option'] = "City Tour Option";
$LNG['international_listing'] = "International Listing";
$LNG['your_sales_representative'] = "Votre representant commercial";
$LNG['office_hours'] = "Heures d'ouverture";
$LNG['mon-fri'] = "Mon-Fri";
$LNG['from_date'] = "Date de";
$LNG['to_date'] = "Jusque date";
$LNG['no_tours'] = "Aucune visite n'a ete choisie";
$LNG['delete_selected_tours'] = "Supprimez la(les) visite(s) selectionnee(s)";
$LNG['your_password_updated'] = "Votre mot de passe a ete mis a jour.";
$LNG['display_address_on_website'] = "Afficher l'adresse sur le site?";
$LNG['not_found'] = "pas trouve";
$LNG['unrated'] = "non commentee";
$LNG['use_slider_rank_escort'] = "Use slider to rank escort ";
$LNG['pv2_start_process'] = "Start the Process Now!";
$LNG['today'] = "Aujourd'hui";
$LNG['pv2_alert_profile_is_not_approved'] = "Your profile is not approved yet! Please wait for approval by administrators";
$LNG['pv2_alert_no_profile_yet'] = "You don't have a profile yet, please click <a href='%LINK%'>here</a> to add new profile!";
$LNG['pv2_alert_profile_admin_disabled'] = "Your profile is disabled by our administrators !";
$LNG['pv2_alert_profile_not_enough_photos'] = "You don't have enough photos to go online !";
$LNG['pv2_alert_package_info'] = "Current active package is '%PACKAGE_NAME%' - expires in %DAYS% days";
$LNG['pv2_alert_ag_no_profile_yet'] = "don't have a profile yet, please click <a href='%LINK%'>here</a> to add new profile!";
$LNG['pv2_alert_ag_profile_admin_disabled'] = "profile is disabled by our administrators !";
$LNG['pv2_alert_ag_is_not_approved'] = "profile is not approved yet! Please wait for approval by administrators";
$LNG['pv2_alert_ag_profile_not_enough_photos'] = "don't have enough photos to go online !";
$LNG['pv2_btn_premium_girls'] = "Premium Models";
$LNG['pv2_alert_ag_owner_disabled'] = "profile is owner disabled !";
$LNG['msg_escort_no_reviews'] = "Cette escorte n'a pas de commentaires.";
$LNG['pv2_alert_one_per_day_limit'] = "Notice: You are allowed to change your bubble text only once per day!";
$LNG['middle_text'] = "6annonce.com est un site d'information et de publicité et n'a aucune connexion ou lien avec quelques sites que ce soient ou individus mentionnés ici. Nous sommes seulement un espace de publicité, nous ne sommes pas une agence d'escorte, ni même dans l'escorting ou le business de la prostitution. Nous ne prenons aucune responsabilité dans le contenu ou les actions de tierces parties (sites webs ou individus) sur lesquels vous auriez des informations telles que les emails ou les contacts téléphoniques.

";
$LNG['new_domain_text'] = "6annonce.com est un site d'information et de publicité et n'a aucune connexion ou lien avec quelques sites que ce soient ou individus mentionnés ici. Nous sommes seulement un espace de publicité, nous ne sommes pas une agence d'escorte, ni même dans l'escorting ou le business de la prostitution. Nous ne prenons aucune responsabilité dans le contenu ou les actions de tierces parties (sites webs ou individus) sur lesquels vous auriez des informations telles que les emails ou les contacts téléphoniques.";
$LNG['escorte_a'] = "Escorte à";
$LNG['pv2_edit_escort_tour'] = "Edit Tour";
$LNG['disable_my_profile'] = "Disable my profile";
$LNG['enable_my_profile'] = "Enable my profile";
$LNG['delete'] = "Delete";
$LNG['pv2_label_secure_text'] = "Parole de sécurité";
$LNG['wrong_secure_text'] = "Parole fausse";
$LNG['open_now'] = "open now";
$LNG['available_now'] = "available now";
$LNG['hh_top_text'] = "%showname% Offers Now HAPPY HOUR prices from %date_from%%date_from_a% until %date_to%%date_to_a%!";
$LNG['save'] = "Save";
$LNG['hh_add_hour'] = "Add. heure";
$LNG['hh_rates'] = "Happy Hour Rates";
$LNG['hh_please_add_some_rates'] = "Please add some rates to your profile, before setting \"Happy Hours\" !";
$LNG['hh_what_is_motto'] = "Quel est le slogan de votre happy hour?";
$LNG['hh_info_motto'] = "Si vous le souhaitez, vous pouvez créer un slogan pour votre happy hour (p.ex. 'Je m'ennuie tellement' ou 'la meilleure promotion!'). Écrivez
le slogan qui vous aidera à attirer l'attention du consommateur.";
$LNG['hh_info_save'] = "Si vous choisissez cette option, le système se charge d'activer automatiquement les paramètres de votre happy hour chaque
semaine.";
$LNG['hh_save_label'] = "Sauvegarder cette happy hour pour chaque semaine";
$LNG['hh_set_your_hh'] = "Veuillez activer votre happy hour";
$LNG['hh_happy_hour_rates'] = "happy hour rates";
$LNG['hh_your_current_outcall_rates_are'] = "Your current outcall rates are";
$LNG['hh_your_current_incall_rates_are'] = "Your current incall rates are";
$LNG['hh_max_2_escort'] = "Remarque: vous pouvez choisir au maximum deux tranches d'horaires";
$LNG['hh_please_select_escort'] = "Étape no 1: Veuillez sélectionner les horaires de votre happy hour";
$LNG['hh_agency_escorts'] = "Agency escorts";
$LNG['hh_gray_top_text'] = "Pour attirer l'attention d'un nombre plus important de consommateurs et pour avoir plus de succès en période creuse, notre site vous offre
la possibilité de fixer chaque semaine une happy hour pouvant durer jusqu'à 5 heures!";
$LNG['hh_happy_hour_guideline'] = "Consignes brèves pour l'happy hour";
$LNG['hh_list_txt_1'] = "Vous pouvez fixer vos prix,  la date / les horaires de votre happy hour en remplissant le formulaire ci-dessous";
$LNG['hh_list_txt_2'] = "La durée maximale d'une période de happy hour est de 5 heures";
$LNG['hh_list_txt_3'] = "La fonction happy hour ne peut être utilisée qu'une seule fois par semaine, même si la durée est de moins de 5 heures";
$LNG['week_day'] = "Week day";
$LNG['hour_from'] = "Hour from";
$LNG['hour_until'] = "Hour until";
$LNG['latest_comments'] = "Latest Comments";
$LNG['advanced_bio'] = "ADVANCED (BIO)";
$LNG['order_direction'] = "Order Direction";
$LNG['free_text'] = "Free text";
$LNG['exact_match'] = "exact match";
$LNG['and_or'] = "and / or";
$LNG['ASC'] = "Asc";
$LNG['DESC'] = "Desc";
$LNG['hh_status_expired'] = "This \"Happy Hour\" is expired. You can reactivate it after %DAYS% days !";
$LNG['hh_status_active'] = "This \"Happy Hour\" is active now ! ";
$LNG['hh_you_have_active_hh'] = "This escort has \"Happy Hour\" defined. Please be careful if you edit any rate the \"Happy Hour\" will be automatically removed !";
$LNG['att_photos_count'] = "Attention: Veuillez charger au moins 3 photos pour chaque sedcard. Autrement, votre profil ne pourra être accepté.";
$LNG['lc_latest_comments'] = "Latest Comments";
$LNG['lc_posted_by'] = "Posted by";
$LNG['lc_comments'] = "comments";
$LNG['lc_read_all_my_comments'] = "read all my comments";
$LNG['verify_idcard_session'] = "IDcard Session";
$LNG['verify_request_confirmed'] = "100% verification process request is confirmed !";
$LNG['hh_internal_text'] = "<span class=\"red\">Nouvelle</span>: Avez-vous déjà essayé notre fonctionnalité unique nommée
\"Happy Hour\" ? Cette fonctionnalité a été créée pour vous aider
à attirer plus de clients en période creuse. Si vous
remarquez que le dimanche après-midi, par exemple, est toujours
très calme, vous pouvez définir des tarifs \"Happy Hour\" (tels que 10 ou
20 euros de réduction sur vos tarifs habituels). Dès que
vous activez une Happy Hour, un logo animé apparaît sur votre profil et vous figurerez également sur une liste dans une sous-page distincte qui attire d'avantage l'attention des clients sur votre composite. 
<br/><br/>
La Happy Hour est très facile à activer : cliquez simplement sur 
l'icône correspondante et remplissez le formulaire. Essayez-la ! Elle est
facile à utiliser et en vaut totalement la peine. 
";
$LNG['hh_escorts'] = "Happy Hour Escortes";
$LNG['suspicious_internal_text'] = "<span class=\"red\">Attention</span>: 6annonce.com désire éviter les photos trompeuses affichées
sur les profils. Malheureusement, vos photos ont été suspectées d'être trompeuses par un des administrateurs de notre site.

Pour cette raison, nous vous informons que votre sedcard deviendra inactive dans 48 heures. Vous pouvez éviter cette situation uniquement si vous participez au processus permettant la vérification des photos à 100%.

Veuillez noter que si vous ne suivez pas les consignes ci-dessus,
votre sedcard sera perdue et aucun remboursement
ne sera effectué. Pour le processus permettant la vérification des photos à 100%, vous avez un délai total de 9 jours,  délai pendant lequel votre profil peut être annulé ou non. Ce délai passé, votre compte sera irrévocablement annulé.";
$LNG['no_hh_text'] = "Oups ... il n'existe pas actuellement d'escortes pendant les <span class=\"r\">«happy hours»</span>. Veuillez vérifier à nouveau plus tard.";
$LNG['suspicious_internal_text_agency'] = "<span class=\"red\">Attention:</span> 6annonce.com desire eviter les photos trompeuses affichees sur les profils. Malheureusement, les photos de %shownames%
ont ete suspectees d'etre trompeuses par un des administrateurs de notre site. Pour cette raison, nous vous informons que le sedcard de %shownames% deviendra inactive dans 48 heures. Vous pouvez
eviter cette situation uniquement si vous participez au processus permettant la verification des photos a 100%. Veuillez noter que si vous ne suivez pas les consignes ci-dessus, le sedcard de %shownames% sera perdue et aucun remboursement
ne sera effectue. Pour le processus permettant la verification des photos a 100%, vous avez un delai total de 9 jours,  delai pendant lequel le profil de %showname% peut etre annule ou non. Ce delai passe, le compte de %shownames% sera irrevocablement annule.";
$LNG['more'] = "More";
$LNG['more_escorts_in_whole_france'] = "More than %NUMBER% escorts in whole France";
$LNG['search_your_city'] = "Rechercher ville";
$LNG['back_to_top'] = "Back to top";
$LNG['no_services_found'] = "No services found";
$LNG['no_tours_found'] = "No tours found";
$LNG['no_rates_found'] = "No rates found";
$LNG['pv2_btn_client_blacklist'] = "Client blacklist";
$LNG['favorites'] = "Favorites";
$LNG['bl_top_text_1'] = "Nous recevons parfois des réactions d'agents d'escorte qui se plaignent
au sujet de clients grossiers, ivres ou agressifs - et de clients qui ne veulent pas payer ou dérobent de l'argent. C'est pourquoi nous avons décidé de créer une liste noire des mauvais clients, accessible pour toutes les agences et escortes. Vous pouvez simplement consulter cette liste  ou effectuer une recherche spécifique à l'aide du formulaire de recherche ci-dessous. ";
$LNG['bl_top_text_2'] = "Si vous-même avez connu une mauvaise expérience quelle qu'elle soit, aidez les autres à éviter de rencontrer ces mauvais clients en ajoutant autant d'informations que possible à la liste noire.";
$LNG['pv2_title_client_blacklist'] = "Liste noire de mauvais clients";
$LNG['bl_make_entry'] = "Pour effectuer une saisie - Cliquez ici";
$LNG['bl_search_input_label'] = "Search after phone number, name or free text";
$LNG['client_name'] = "Client Name";
$LNG['client_phone_number'] = "Client Phone Number";
$LNG['experience_comment'] = "Experience / Comment
";
$LNG['phone_number'] = "Téléphone";
$LNG['full_site'] = "Full site";
$LNG['answer'] = "Réponse";
$LNG['question'] = "Question";
$LNG['m_popup_top_text'] = "Version mobile de notre site disponible maintenant!";
$LNG['m_popup_bottom_text'] = "<ul>
	<li>Fonctionne sur toutes appareil mobile</li>
	<li>Toutes les fonctiones imporantes sont disponibles</li>
	<li>Accès direct, pas besoin de télécharger</li>
	<li>Usage gratuit</li>
</ul>

<p>Entrer <span>m.6annone.com</span> dans votre appareil mobile - c'est tout!</p>";
$LNG['vote_me_title'] = "Compétition pour la fille du mois";
$LNG['vote_me_text'] = "Votez moi pour la fille du mois";
$LNG['vote_me_button'] = "Cliquez ici - merci";
$LNG['su2_banner_text'] = "Please be aware that in order to be listed on our site you
need to place one of the follwing banners in top position
on your homepage. ";
$LNG['su2_banner_text2'] = "Just copy / paste the html-code:";
$LNG['online_chat'] = "Online Chat";
$LNG['chat_text_1'] = "<span class=\"red\">NOUVEAUTÉ:</span> Fonction Chat. 6annonce.com vous offre la possibilité de discuter en ligne avec d'autres utilisateurs / vos clients! De nombreuses options sont disponibles, comme: discussion privée, message personnalisé, message murmuré, ignorer etc.!";
$LNG['chat_text_2'] = "Lorsque vous cliquez sur l'icône ci-dessous,  votre sedcard indiquera que vous êtes disponible pour discuter en live. De plus, vous apparaîtrez dans un menu séparé et vous attirerez l'attention de plus de clients - ce qui signifie plus de boulots!";
$LNG['chat_member_text'] = "<span class=\"red\">NEW:</span> Fonction chat. 6annonce.com vous offre la possibilité de discuter en ligne avec d'autres utilisateurs et les escortes! De nombreuses options sont disponibles, comme: iscussion privée, message personnalisé, message murmuré, ignorer etc.!";
$LNG['tomorrow'] = "demain";
