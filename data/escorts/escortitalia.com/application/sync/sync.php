<?php

define('IS_CLI', 1);

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/..'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../../library'),
    get_include_path(),
)));

error_reporting(E_ALL & ~E_STRICT & ~E_NOTICE);
ini_set('display_errors', 'On');

try {
	require_once('Zend/Console/Getopt.php');
    $opts = new Zend_Console_Getopt(array(
		'help|h'		=> 'Displays usage information.',
		'action|a=s'	=> 'Action to perform in format of module.controller.action',
		'verbose|v'		=> 'Verbose messages will be dumped to the default output.',
		'debug|d'		=> 'Enables debug mode.',
		'staging|s'  => 'Staging production.'
    ));
	
    $opts->parse();
}
catch (Zend_Console_Getopt_Exception $e) {
    exit($e->getMessage() ."\n\n". $e->getUsageMessage());
}

if( isset($opts->h) ) {
    echo $opts->getUsageMessage();
    exit;
}

if ( isset($opts->a) ) {
	if ( isset($opts->d) ) {
		define('APPLICATION_ENV', 'development');
		define('APP_HTTP', 'http');
		define('APP_HOSTNAME', 'www.escortitalia.com.test');
	}
	elseif ( isset($opts->s) ) {
		define('APPLICATION_ENV', 'staging');
        define('APP_HTTP', 'https');
    }else {
        define('APP_HTTP', 'https');
        define('APP_HOSTNAME', 'www.escortitalia.com');
    }

	/** Zend_Application */
	require_once 'Zend/Application.php';

	// Define application environment
	defined('APPLICATION_ENV')
		|| define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

	// Create application, bootstrap, and run
	$application = new Zend_Application(
		APPLICATION_ENV,
		APPLICATION_PATH . '/configs/application.ini'
	);

	

	$application->bootstrap();
	
    $reqRoute = array_reverse(explode('.', $opts->a));
    @list($action, $controller, $module) = $reqRoute;
	
    $request = new Cubix_Controller_Request_Cli($action, $controller, $module, array(
		'lang_id' => 'en'
	));

    $front = $application->getBootstrap()->getResource('FrontController');

	$front->setRequest($request);
	$front->setRouter(new Cubix_Controller_Router_Cli());
	$front->setResponse(new Zend_Controller_Response_Cli());
	$front->throwExceptions(true);

	$application->run();
	
}
