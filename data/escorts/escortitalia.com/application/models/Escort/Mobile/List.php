<?php

class Model_Escort_Mobile_List extends Cubix_Model
{
	public static function getMainPremiumSpot()
	{
		$lng = Cubix_I18n::getLang();

		if (date('G') < 2)
			$date = 2;
		else
			$date = date('G');

		$sql = '
			SELECT
				/* escorts table */
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id,
				e.rates, e.incall_type, e.outcall_type, e.is_new, e.verified_status,
				e.photo_hash, e.photo_ext, e.photo_status,
				e.products, e.slogan, e.date_last_modified,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.is_suspicious,e.comment_count, e.review_count,
				
				ct.title_' . $lng . ' AS city,

				/* seo_entity_instances table */
				IF (sei.primary_id IS NULL, se.title_' . $lng . ', sei.title_' . $lng . ') AS alt,

				IF (e.available_24_7 = 1 OR (e.is_now_open = 1 AND e.close_time >= "' . $date . '"), 1, 0) AS is_late_night,
				/* misc */
				1 AS is_premium, ' . Cubix_Application::getId() . ' AS application_id, e.hh_is_active, e.is_online
			FROM escorts e
			INNER JOIN escorts_in_cities eic ON eic.escort_id = e.id AND eic.is_base = 1
			INNER JOIN cities ct ON ct.id = eic.city_id
			LEFT JOIN seo_entity_instances sei ON sei.primary_id = e.id
			LEFT JOIN seo_entities se ON (se.id = sei.entity_id) OR (se.application_id = 2 AND se.slug = "escort")
			WHERE
				eic.gender = ' . GENDER_FEMALE . ' AND e.is_main_premium_spot = 1
			GROUP BY e.id
		';

		$cache = Zend_Registry::get('cache');
		$cache_key = 'main_premium_spot_' . Cubix_Application::getId() . '_lang_' . Cubix_I18n::getLang();

		if ( ! $escorts = $cache->load($cache_key) ) {
			
			$escorts = self::db()->fetchAll($sql);
			
			/* seo heading escorts */
			if ($escorts)
			{
				$es = array();
				foreach ($escorts as $e)
				{
					$es[] = $e->id;
				}
				$es_str = implode(',', $es);

				$query = "SELECT e.id, sei.heading_escort_{$lng} as heading_escort
					FROM escorts e
					INNER JOIN escorts_in_cities eic ON eic.escort_id = e.id AND eic.is_base = 1
					INNER JOIN seo_entity_instances sei ON sei.primary_id = eic.city_id
					INNER JOIN seo_entities se ON se.application_id = " . Cubix_Application::getId() . " AND se.slug = 'city' AND sei.entity_id = se.id
					WHERE e.id IN ({$es_str})
				";
				$result = self::db()->fetchAll($query);

				foreach ($result as $r)
				{				
					foreach ($escorts as $e)
					{
						if ($e->id == $r->id)
						{
							$e->heading_escort = $r->heading_escort;
							break;
						}
					}
				}
			}
			/**/

			$cache->save($escorts, $cache_key, array());
		}

		// <editor-fold defaultstate="collapsed" desc="Make Escort Alt for SEO">
		$tpl_data = array(
			'app_title' => Cubix_Application::getById()->title
		);

		foreach ( $escorts as $i =>  $escort ) {
			$additional = array(
				'showname' => $escort->showname,
				'city' => $escort->city
			);

			foreach ( $tpl_data + $additional as $tpl_var => $tpl_value ) {
				$escorts[$i]->alt = str_replace('%' . $tpl_var . '%', $tpl_value, $escorts[$i]->alt);
			}
		}
		// </editor-fold>

		shuffle($escorts);

		// <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
		$sid = 'sedcard_paging_' . Cubix_Application::getId();
		$sess = new Zend_Session_Namespace($sid);

		$sess->main_premium_spot = array();
		foreach ( $escorts as $escort ) {
			$sess->main_premium_spot[] = $escort->showname;
		}
		// </editor-fold>


		return $escorts;
	}

	public static function getFiltered($filter, $ordering = 'random', $page = 1, $page_size = 50, &$count = 0, $sess_name = false, $only_vip = false, $s_filter_params = array(), $is_grouped = false, $geo_data = array(), $list_type = 'simple')
	{
		$lng = Cubix_I18n::getLang();

		if (date('G') < 2)
			$date = 2;
		else
			$date = date('G');

		if ( 'price' == substr($ordering, 0, 5) ) {
			$filter[] = 'e.incall_price IS NOT NULL';
		}

		if ( isset($s_filter_params['gender']) && count($s_filter_params['gender']) ) {
			$filter[] = 'e.gender IN ( ' . implode(',', $s_filter_params['gender']) . ' )';
		}

		if ( isset($s_filter_params['service']) && count($s_filter_params['service']) ) {
			$filter[] = 'es.service_id IN ( ' . implode(',', $s_filter_params['service']) . ' )';
		}
		
		// Only escorts with base city (or city tour) will be shown in this list
		if ( ! isset($filter['ct.slug = ?']) && ! isset($filter['r.slug = ?']) ) {
			$filter[] = 'eic.is_base = 1';
		}
		else {
			$filter[] = 'eic.is_upcoming = 0';
		}

		$where = self::getWhereClause($filter, true);
		$order = self::_mapSorting($ordering, $is_grouped);

		$page = (int) $page;
		if ( $page < 1 ) $page = 1;
		$limit = ($page_size * ($page - 1)) . ', ' . $page_size;
		
		$j = "";
		$f = "";
		if ( isset($filter['f.user_id = ?']) ) {
			$j = " INNER JOIN favorites f ON f.escort_id = e.id ";
			$f = " , f.user_id AS fav_user_id ";
		}

		$group_by = " GROUP BY eic.escort_id ";

		/* IF AGENCY GROUP BY AGENCY ID FOR MIXED VIEW */
		if ( ! isset($filter['eic.is_agency = 1']) ) {
			$j .= " LEFT JOIN club_directory cd ON cd.agency_id = e.agency_id ";
		} else if ( isset($filter['eic.is_agency = 1']) ) {
			$j .= " INNER JOIN club_directory cd ON cd.agency_id = e.agency_id ";
		}

		/* GET AGENCY INFO */
		$f = "
			, cd.phone AS agency_phone, cd.web AS agency_web, cd.is_premium AS is_premium_agency, cd.phone_country_id AS agency_phone_country_id, cd.escorts_count,
			cd.about_" . $lng . " AS agency_about, act.title_" . $lng . " AS agency_city_title, acr.title_" . $lng . " AS agency_country_title,
			cd.last_modified AS agency_date_last_modified, cd.email AS agency_email, cd.photo_hash AS a_photo_hash, cd.photo_ext AS a_photo_ext
		 ";

		$j .= " LEFT JOIN cities act ON act.id = cd.city_id ";
		$j .= " LEFT JOIN countries acr ON acr.id = cd.country_id ";

		$group_by = " GROUP BY (CASE WHEN e.agency_id IS NOT NULL THEN e.agency_id ELSE e.id END) ";
		/* IF AGENCY GROUP BY AGENCY ID FOR MIXED VIEW */


		if ( ! $is_grouped ) {
			$order = str_replace('cd.is_premium DESC,', '', $order);
		}

		$fields = ' NULL AS distance ';
		if ( $ordering == 'close-to-me' && count($geo_data) ) {
			if (array_key_exists("latitude", $geo_data) && strlen($geo_data['latitude']) > 0 && array_key_exists("longitude", $geo_data) && strlen($geo_data['longitude']) > 0) {
				$fields = '
					((2 * 6371 *
						ATAN2(
						SQRT(
							POWER(SIN((RADIANS(' . $geo_data['latitude'] . ' - ct.latitude))/2), 2) +
							COS(RADIANS(ct.latitude)) *
							COS(RADIANS(' . $geo_data['latitude'] . ')) *
							POWER(SIN((RADIANS(' . $geo_data['longitude'] . ' - ct.longitude))/2), 2)
						),
						SQRT(1-(
							POWER(SIN((RADIANS(' . $geo_data['latitude'] . ' - ct.latitude))/2), 2) +
							COS(RADIANS(ct.latitude)) *
							COS(RADIANS(' . $geo_data['latitude'] . ')) *
							POWER(SIN((RADIANS(' . $geo_data['longitude'] . ' - ct.longitude))/2), 2)
						))
						)
					)) AS distance
				';
			}
		}

		//$order = str_replace('eic.is_premium', 'IF(eic.is_tour = 1, 0, eic.is_premium)', $order);
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id,
				UNIX_TIMESTAMP(e.date_activated) AS date_activated, ct.title_' . $lng . ' AS city, e.rates,
				e.incall_type, e.outcall_type, e.is_new, cr.title_' . $lng . ' AS country, e.verified_status,
				e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.comment_count, e.review_count,
				e.hit_count, e.slogan, e.incall_price, e.date_last_modified, eic.is_premium, e.is_vip, e.hh_is_active,
				e.is_suspicious, e.is_online ' . $f . ', ct.id AS city_id, e.about_' . $lng . ' AS about, e.agency_name,
				e.agency_id, ' . $fields . '
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			INNER JOIN countries cr ON cr.id = e.country_id
			' . $j . '
			LEFT JOIN escort_services es ON es.escort_id = e.id
			' . (! is_null($where) ? 'WHERE ' . $where : '') . '
			' . $group_by . '
			ORDER BY ' . $order . '
			LIMIT ' . $limit . '
		';


		$escorts = self::db()->fetchAll($sql);
		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');

		$_ids = array();

		if ( count($escorts) ) {
			foreach($escorts as $escort) {
				$_ids[] = $escort->id;
			}

			$profile_sql = '
				SELECT ep.escort_id, ep.data FROM escort_profiles ep WHERE ep.escort_id IN (' . implode(', ', $_ids) . ')
			';

			self::db()->query('SET NAMES `utf8`');
			$_escort_profiles = self::db()->fetchAll($profile_sql);



			$escort_profiles = array();
			foreach($_escort_profiles as $profile) {
				$escort_profiles[$profile->escort_id] = new Model_EscortV2Item(unserialize($profile->data));
			}



			foreach($escorts as $k => $escort) {
				$escort_profiles[$escort->id]->agency_name = $escort->agency_name;
				$escort_profiles[$escort->id]->is_premium = $escort->is_premium;
				$escorts[$k]->profile_data = $escort_profiles[$escort->id];
			}


			$grouped_escorts = array();
			if ( $is_grouped ) {

				if ( $page == 1 ) {

					$premium_escorts = self::getFiltered($filter, 'premiums', 1, 500);

					if ( count($premium_escorts) ) {
						foreach( $premium_escorts as  $k => $p_escort ) {
							if ( $p_escort->is_premium || $p_escort->is_premium_agency ) {
								if ( $p_escort->agency_id ) {

									$_filter = array_merge(array('e.agency_id = ?' => $p_escort->agency_id), $filter);
									$agency_escorts = self::getAgencyEscorts($_filter, 'random');

									if ( count($agency_escorts) ) {
										$premium_escorts[$k]->agency_escorts = $agency_escorts;
									}
								}

								$grouped_escorts['premiums'][] = $p_escort;
							}
						}
					}
				}

				foreach($escorts as $k => $escort) {
					$contacts = $escort->profile_data->getContacts();

					/*if ( $ordering == 'random' && ($escort->is_premium || $escort->is_premium_agency) ) {
						if ( $escort->agency_id ) {
							$_filter = array_merge(array('e.agency_id = ?' => $escort->agency_id), $filter);
							$agency_escorts = self::getFiltered($_filter, 'random', 1, 500);

							if ( count($agency_escorts) ) {
								$escorts[$k]->agency_escorts = $agency_escorts;
							}
						}

						$grouped_escorts['premiums'][] = $escort;
					} else {*/

						if ( $escort->agency_id ) {
							$__filter = array_merge(array('e.agency_id = ?' => $escort->agency_id), $filter);
							$agency_escorts = self::getAgencyEscorts($__filter, 'random');
							if ( count($agency_escorts) ) {
								$escorts[$k]->agency_escorts = $agency_escorts;
							}
						}

						$group_field = $contacts->{'base_city_' . $lng};
						if ( $ordering == 'by-country' && ! isset($filter['ct.id = ?']) && ! isset($filter['cr.id = ?']) ) {
							$group_field = $escort->country;
						}

						$grouped_escorts['city_groups'][$group_field][] = $escort;

						if ( $ordering != 'close-to-me' ) {
							ksort($grouped_escorts['city_groups']);
						}
					//}
				}

				if ( $list_type == 'simple' ) {
					$escorts = $grouped_escorts;
				}
			}
		}

		return $escorts;
	}

	public static function getAgencyEscorts($filter, $ordering = 'random')
	{
		$lng = Cubix_I18n::getLang();


		$where = self::getWhereClause($filter, true);
		$order = self::_mapSorting($ordering);

		$sql = '
			SELECT
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id,
				UNIX_TIMESTAMP(e.date_activated) AS date_activated,
				ct.title_' . $lng . ' AS city, e.rates, e.incall_type, e.outcall_type, e.is_new,
				cr.title_' . $lng . ' AS country,
				e.verified_status, e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.comment_count, e.review_count,
				e.hit_count, e.slogan, e.incall_price,
				e.date_last_modified,
				/*IF(eic.is_tour = 1, 0, eic.is_premium) AS is_premium,*/
				eic.is_premium,
				e.is_vip,
				e.hh_is_active, e.is_suspicious, e.is_online, ct.id AS city_id, e.about_' . $lng . ' AS about,
				e.agency_name, e.agency_id
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			INNER JOIN countries cr ON cr.id = e.country_id
			/*LEFT JOIN regions r ON r.id = eic.region_id*/
			/*LEFT JOIN escort_cityzones ecz ON ecz.escort_id = eic.escort_id
			LEFT JOIN cityzones cz ON cz.id = ecz.city_zone_id
			LEFT JOIN seo_entity_instances sei ON sei.primary_id = eic.escort_id
			LEFT JOIN seo_entities se ON (se.id = sei.entity_id) OR (se.application_id = 2 AND se.slug = "escort")*/
			LEFT JOIN escort_services es ON es.escort_id = e.id
			' . (! is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.escort_id
			ORDER BY ' . $order . '
		';

		$escorts = self::db()->fetchAll($sql);

		$_ids = array();

		if ( count($escorts) ) {
			foreach($escorts as $escort) {
				$_ids[] = $escort->id;
			}

			$profile_sql = '
				SELECT ep.escort_id, ep.data FROM escort_profiles ep WHERE ep.escort_id IN (' . implode(', ', $_ids) . ')
			';

			self::db()->query('SET NAMES `utf8`');
			$_escort_profiles = self::db()->fetchAll($profile_sql);



			$escort_profiles = array();
			foreach($_escort_profiles as $profile) {
				$escort_profiles[$profile->escort_id] = new Model_EscortV2Item(unserialize($profile->data));
			}



			foreach($escorts as $k => $escort) {
				$escort_profiles[$escort->id]->agency_name = $escort->agency_name;
				$escort_profiles[$escort->id]->is_premium = $escort->is_premium;
				$escorts[$k]->profile_data = $escort_profiles[$escort->id];
			}
		}

		return $escorts;
	}

	public static function getTours($is_upcoming, $filter, $ordering = 'random', $page = 1, $page_size = 50, &$count = 0, $s_filter_params = array(), $is_grouped = false, $geo_data = array(), $list_type = 'simple')
	{
		$lng = Cubix_I18n::getLang();

		if ( isset($filter['eic.gender = 1']) ) unset($filter['eic.gender = 1']);
		
		if ( ! $is_upcoming ) {
			$filter[] = 'eic.is_tour = 1';
		}
		else {
			$filter[] = 'eic.is_upcoming = 1';
		}

		$where = self::getWhereClause($filter, true);
		$order = self::_mapSorting($ordering);

		$page = (int) $page;
		if ( $page < 1 ) $page = 1;
		$limit = ($page_size * ($page - 1)) . ', ' . $page_size;

		$fields = ' , NULL AS distance ';
		if ( $ordering == 'close-to-me' && count($geo_data) ) {
			if (array_key_exists("latitude", $geo_data) && strlen($geo_data['latitude']) > 0 && array_key_exists("longitude", $geo_data) && strlen($geo_data['longitude']) > 0) {
				$fields = '
					 , ((2 * 6371 *
						ATAN2(
						SQRT(
							POWER(SIN((RADIANS(' . $geo_data['latitude'] . ' - ct.latitude))/2), 2) +
							COS(RADIANS(ct.latitude)) *
							COS(RADIANS(' . $geo_data['latitude'] . ')) *
							POWER(SIN((RADIANS(' . $geo_data['longitude'] . ' - ct.longitude))/2), 2)
						),
						SQRT(1-(
							POWER(SIN((RADIANS(' . $geo_data['latitude'] . ' - ct.latitude))/2), 2) +
							COS(RADIANS(ct.latitude)) *
							COS(RADIANS(' . $geo_data['latitude'] . ')) *
							POWER(SIN((RADIANS(' . $geo_data['longitude'] . ' - ct.longitude))/2), 2)
						))
						)
					)) AS distance
				';
			}
		}

		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id,
				UNIX_TIMESTAMP(e.date_activated) AS date_activated, ct.title_' . $lng . ' AS city, e.rates,
				e.incall_type, e.outcall_type, e.is_new, cr.title_' . $lng . ' AS country, e.verified_status,
				e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.comment_count, e.review_count,
				e.hit_count, e.slogan, e.incall_price, e.date_last_modified, eic.is_premium, e.is_vip, e.hh_is_active,
				e.is_suspicious, e.is_online, ct.id AS city_id, e.about_' . $lng . ' AS about, e.agency_name,
				/*ctt.title_' . $lng . ' AS tour_city, crt.title_' . $lng . ' AS tour_country,*/
				eic.is_tour ' .
				(! $is_upcoming ? ', e.tour_date_from, e.tour_date_to' : // if
				($is_upcoming ? ', ut.tour_date_from, ut.tour_date_to' : '')) // else
				. $fields . '
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			INNER JOIN countries cr ON cr.id = ct.country_id
			/*LEFT JOIN cities ctt ON ctt.id = e.tour_city_id
			LEFT JOIN countries crt ON crt.id = ctt.country_id*/
			LEFT JOIN escort_services es ON es.escort_id = e.id
			' . ($is_upcoming ? 'INNER JOIN upcoming_tours ut ON ut.id = eic.escort_id' : '') . '
			' . (! is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.escort_id
			ORDER BY ' . $order . '
			LIMIT ' . $limit . '
		';

		$escorts = self::db()->fetchAll($sql);
		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');

		$_ids = array();

		if ( count($escorts) ) {
			foreach($escorts as $escort) {
				$_ids[] = $escort->id;
			}

			$profile_sql = '
				SELECT ep.escort_id, ep.data FROM escort_profiles ep WHERE ep.escort_id IN (' . implode(', ', $_ids) . ')
			';

			self::db()->query('SET NAMES `utf8`');
			$_escort_profiles = self::db()->fetchAll($profile_sql);



			$escort_profiles = array();
			foreach($_escort_profiles as $profile) {
				$escort_profiles[$profile->escort_id] = new Model_EscortV2Item(unserialize($profile->data));
			}



			foreach($escorts as $k => $escort) {
				$escort_profiles[$escort->id]->agency_name = $escort->agency_name;
				$escort_profiles[$escort->id]->is_premium = $escort->is_premium;
				$escort_profiles[$escort->id]->is_tour = $escort->is_tour;
				$escort_profiles[$escort->id]->tour_city = $escort->city;
				$escort_profiles[$escort->id]->tour_country = $escort->country;
				$escort_profiles[$escort->id]->tour_date_from = $escort->tour_date_from;
				$escorts[$k]->profile_data = $escort_profiles[$escort->id];
			}


			$grouped_escorts = array();
			if ( $is_grouped ) {

				foreach($escorts as $k => $escort) {
					$contacts = $escort->profile_data->getContacts();

					/*if ( $ordering == 'random' && ($escort->is_premium || $escort->is_premium_agency) ) {
						if ( $escort->agency_id ) {
							$_filter = array_merge(array('e.agency_id = ?' => $escort->agency_id), $filter);
							$agency_escorts = self::getFiltered($_filter, 'random', 1, 500);

							if ( count($agency_escorts) ) {
								$escorts[$k]->agency_escorts = $agency_escorts;
							}
						}

						$grouped_escorts['premiums'][] = $escort;
					} else {*/

					/*if ( $escort->agency_id ) {
						$__filter = array_merge(array('e.agency_id = ?' => $escort->agency_id), $filter);
						$agency_escorts = self::getAgencyEscorts($__filter, 'random');
						if ( count($agency_escorts) ) {
							$escorts[$k]->agency_escorts = $agency_escorts;
						}
					}*/

					$group_field = $escort->city;
					if ( $ordering == 'by-country' && ! isset($filter['ct.id = ?']) && ! isset($filter['cr.id = ?']) ) {
						$group_field = $escort->country;
					}

					$grouped_escorts['city_groups'][$group_field][] = $escort;

					if ( $ordering != 'close-to-me' ) {
						ksort($grouped_escorts['city_groups']);
					}
					//}
				}

				if ( $list_type == 'simple' ) {
					$escorts = $grouped_escorts;
				}
			}
		}

		return $escorts;
	}

	private static function _mapSorting($param, $is_grouped = false)
	{
		$map = array(
			'close-to-me' => 'eic.is_premium DESC, distance ASC, eic.ordering DESC',


			'by-city' => 'eic.is_premium DESC, ct.title_en ASC, eic.ordering DESC',
			'by-country' => ' eic.is_premium DESC, cr.title_en ASC, eic.ordering DESC',
			'random' => 'e.is_vip DESC, eic.is_premium DESC, eic.ordering DESC',
			'alpha' => 'eic.is_premium DESC, e.showname ASC',
			'most-viewed' => 'eic.is_premium DESC, e.hit_count DESC',
			'newest' => 'eic.is_premium DESC, e.date_activated DESC, e.id DESC',


			'premiums' => 'eic.is_premium DESC, cd.is_premium DESC, eic.ordering DESC'
		);

		if ( $is_grouped ) {
			$map = array(
				'close-to-me' => 'distance ASC, eic.ordering DESC',


				'by-city' => 'ct.title_en ASC, eic.ordering DESC',
				'by-country' => 'cr.title_en ASC, eic.ordering DESC',
				'random' => 'e.is_vip DESC, eic.is_premium DESC, cd.is_premium DESC, eic.ordering DESC',
				'alpha' => 'e.showname ASC',
				'most-viewed' => 'e.hit_count DESC',
				'newest' => 'e.date_activated DESC, e.id DESC',


				'premiums' => 'eic.is_premium DESC, cd.is_premium DESC, eic.ordering DESC'
			);
		}
		
		$order = 'e.ordering DESC';
		if ( isset($map[$param]) ) {
			$order = $map[$param];
		}
		elseif ( false !== strpos($param, 'FIELD') ) {
			$order = $param;
		}

		return $order;
	}

	public static function getPrevNext( $showname )
	{
		return array(null, null);
		// Retrieve criterias from session
		$sid = 'v2_sedcard_paging_' . Cubix_Application::getId();
		$sess = new Zend_Session_Namespace($sid);

		$page = $sess->page;
		$page_size = $sess->page_size;
		$pages_count = $sess->pages_count;

		list($filter, $ordering) = $sess->criteria;
		foreach ( $sess->escorts as $i => $name ) {
			if ( $name == $showname ) {
				$prev_i = $i - 1;
				$next_i = $i + 1;

				break;
			}
		}

		echo "Prev_i: $prev_i, Next_i: $next_i\n";
		//print_r($sess->escorts);

		if ( ! isset($prev_i) && ! isset($next_i) ) {
			return array(null, null);
		}

		// This is first escort, we need to fetch data from prev page
		if ( isset($prev_i) && (! isset($sess->escorts[$prev_i]) || $prev_i < 0) && $page > 1 ) {
			$sess->escorts = array_slice($sess->escorts, 0, $page_size * 2);

			$escorts = call_user_func_array(
				array(__CLASS__, $sess->callback),
				array($filter, $ordering, $page - 1, $page_size)
			);
			foreach ( $escorts as $i => $escort ) {
				$escorts[$i] = $escort->showname;
			}

			$sess->escorts = array_merge($escorts, $sess->escorts);
			
			$prev_i = $page_size - 1;
		}

		
		echo "Escorts after 'first' logic\n";
		echo "Prev_i: $prev_i, Next_i: $next_i\n";
		//print_r($sess->escorts);
		
		// If this last escort on page, we need to fetch data from next page
		if ( isset($next_i) &&  (! isset($sess->escorts[$next_i]) || $next_i > count($sess->escorts) - 1 ) && $page < $pages_count ) {
			if ( count($sess->escorts) >= $page_size * 3 ) {
				$sess->escorts = array_slice($sess->escorts, $page_size, $page_size * 2);
				$next_i -= $page_size;
			}
			
			$escorts = call_user_func_array(
				array(__CLASS__, $sess->callback),
				array($filter, $ordering, $page + 1, $page_size)
			);
			foreach ( $escorts as $i => $escort ) {
				$escorts[$i] = $escort->showname;
			}

			$sess->escorts = array_merge($sess->escorts, $escorts);
		}

		echo "Escorts after 'last' logic\n";
		echo "Prev_i: $prev_i, Next_i: $next_i\n";
		//print_r($sess->escorts);
		
		return array(isset($prev_i) ? $sess->escorts[$prev_i] : null, isset($next_i) ? $sess->escorts[$next_i] : null);
	}
	
	public static function getVIPEscorts($filter, $ordering = 'random', $page = 1, $page_size = 50, &$count = 0, $sess_name = false, $only_vip = false, $is_tour = false)
	{
		$lng = Cubix_I18n::getLang();

		if (date('G') < 2)
			$date = 2;
		else
			$date = date('G');

		if ( 'price' == substr($ordering, 0, 5) ) {
			$filter[] = 'e.incall_price IS NOT NULL';
		}
		
		if ( $only_vip ) {
			$filter[] = 'FIND_IN_SET(17, e.products) > 0';

			if ( isset($filter['ct.slug = ?']) && ! $is_tour ) {
				$country_id = Model_Statistics::getCountryByCitySlug($filter['ct.slug = ?'][0]);
				$filter[] = 'e.country_id = ' . $country_id;
				unset($filter['ct.slug = ?']);
			}
		}
		
		// Only escorts with base city (or city tour) will be shown in this list
		if ( ! isset($filter['ct.slug = ?']) && ! isset($filter['r.slug = ?']) ) {
			$filter[] = 'eic.is_base = 1';
		}
		else {
			$filter[] = 'eic.is_upcoming = 0';
		}

		$where = self::getWhereClause($filter, true);
		$order = self::_mapSorting($ordering);

		$page = (int) $page;
		if ( $page < 1 ) $page = 1;
		$limit = ($page_size * ($page - 1)) . ', ' . $page_size;
		
		$j = "";
		$f = "";
		if ( isset($filter['f.user_id = ?']) ) {
			$j = " INNER JOIN favorites f ON f.escort_id = e.id ";
			$f = " , f.user_id AS fav_user_id ";
		}

		$order = str_replace('eic.is_premium', 'IF(eic.is_tour = 1, 0, eic.is_premium)', $order);
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id,
				UNIX_TIMESTAMP(e.date_activated) AS date_activated,
				ct.title_' . $lng . ' AS city, e.rates, e.incall_type, e.outcall_type, e.is_new,
				e.verified_status, e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.comment_count, e.review_count,

				IF (sei.primary_id IS NULL, se.title_' . $lng . ', sei.title_' . $lng . ') AS alt,
				IF (e.available_24_7 = 1 OR (e.is_now_open = 1 AND e.close_time >= "' . $date . '"), 1, 0) AS is_late_night,
				e.hit_count, e.slogan, e.incall_price,
				e.date_last_modified,
				/*IF(eic.is_tour = 1 AND eic.is_premium = 1, 0, eic.is_premium) AS is_premium,*/ 
				IF(eic.is_tour = 1, 0, eic.is_premium) AS is_premium,
				IF(FIND_IN_SET(17, e.products) > 0, 1, 0) AS is_vip,
				e.hh_is_active, e.is_suspicious, e.is_online ' . $f . ', ct.id AS city_id, e.about_' . $lng . ' AS about
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			INNER JOIN countries cr ON cr.id = e.country_id
			LEFT JOIN regions r ON r.id = eic.region_id
			' . $j . '
			LEFT JOIN escort_cityzones ecz ON ecz.escort_id = eic.escort_id
			LEFT JOIN cityzones cz ON cz.id = ecz.city_zone_id
			LEFT JOIN seo_entity_instances sei ON sei.primary_id = eic.escort_id
			LEFT JOIN seo_entities se ON (se.id = sei.entity_id) OR (se.application_id = 2 AND se.slug = "escort")
			LEFT JOIN escort_services es ON es.escort_id = e.id
			' . (! is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.escort_id
			ORDER BY ' . $order . '
			LIMIT ' . $limit . '
		'; 
		
		$escorts = self::db()->fetchAll($sql);
		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');
		
		
		// <editor-fold defaultstate="collapsed" desc="Make Escort Alt for SEO">
		$tpl_data = array(
			'app_title' => Cubix_Application::getById()->title
		);

		foreach ( $escorts as $i =>  $escort ) {
			$additional = array(
				'showname' => $escort->showname,
				'city' => $escort->city
			);

			foreach ( $tpl_data + $additional as $tpl_var => $tpl_value ) {
				$escorts[$i]->alt = str_replace('%' . $tpl_var . '%', $tpl_value, $escorts[$i]->alt);
			}
		}
		// </editor-fold>

		if ( $sess_name ) {
			// <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
			$sid = 'sedcard_paging_' . Cubix_Application::getId();
			$sess = new Zend_Session_Namespace($sid);

			$sess->{$sess_name} = array();
			foreach ( $escorts as $escort ) {
				$sess->{$sess_name}[] = $escort->id;
			}
			
			$sess->{$sess_name . '_criterias'} = array(
				'filter'	=> $filter,
				'sort'	=> $ordering,
				'page'		=> $page
			);
			// </editor-fold>
		}

		return $escorts;
	}
}
