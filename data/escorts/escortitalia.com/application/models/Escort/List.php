<?php

class Model_Escort_List extends Cubix_Model
{
	public static function getActiveFilter($filter, $where_query = array(), $is_upcomingtour = false, $is_tour = false)
	{
		$cache = Zend_Registry::get('cache');

		if ( isset($filter['e.showname LIKE ?']) ) {
			unset($filter['e.showname LIKE ?']);
		}

		if ( $is_tour ) {
			if ( isset($filter['eic.gender = 1']) ) unset($filter['eic.gender = 1']);

			if ( ! $is_upcomingtour ) {
				$filter[] = 'eic.is_tour = 1';
			}
			else {
				$filter[] = 'eic.is_upcoming = 1';
			}
		} else {
			// Only escorts with base city (or city tour) will be shown in this list
			if ( ! isset($filter['ct.slug = ?']) && ! isset($filter['r.slug = ?']) ) {
				$filter[] = 'eic.is_base = 1';
			}
			else {
				$filter[] = 'eic.is_upcoming = 0';
			}
		}

		$where = self::getWhereClause($filter, true);

		$wqa = array();
		if ( count($where_query) ) {
			foreach( $where_query as $wq ) {

				if ( strlen($wq['query']) ) {
					$where_query = ' AND ' . $wq['filter_query'];

					$where .= $where_query;
					$wqa[] = array('filter_query' => $wq['filter_query'], 'query' => $wq['query'] );
				}
			}
		}

		$metadata_cache_key = 'metadata_cache_key_' . Cubix_Application::getId();
		$metadata = $cache->load($metadata_cache_key);
		if ( ! $metadata ) {
			$metadata = self::db()->describeTable('escort_filter_data');

			$cache->save($metadata, $metadata_cache_key, array());
		}

		$column_names = array_keys($metadata);
		unset($column_names[0]);

		$select = array();
		foreach ( $column_names as $column ) {
			$select[] = 'SUM(' . $column . ') AS ' . $column;
		}

		$join = " ";

		if ( isset($filter['cz.slug = ?']) ) {
			$join = "
				INNER JOIN escorts_in_cityzones eicz ON eicz.escort_id = fd.escort_id
				INNER JOIN cityzones cz ON cz.id = eicz.cityzone_id
			";
		}

		$sql = '
			SELECT
				' . implode(', ', $select) . '
			FROM ( SELECT fd.*
			FROM escort_filter_data fd
			INNER JOIN escorts_in_cities eic ON eic.escort_id = fd.escort_id
			INNER JOIN cities ct ON eic.city_id = ct.id
			' . $join . '
			' . (! is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY fd.escort_id ) f
		';

		$sql_cache_key = strtolower('filter_row_' . Cubix_Application::getId() . '_' . preg_replace('#=|\.| |\'|\"|\-|\(|\)#', '_', $where));
		$row = $cache->load($sql_cache_key);
		//var_dump($row);
		if ( ! $row ) {
			$row = self::db()->fetchRow($sql);
			$cache->save($row, $sql_cache_key, array());
			//echo "not cache";
		}

		//echo $sql;
		return array('result' => $row, 'global_filter' => $filter , 'where_query' => $wqa, 'is_upcoming' => $is_upcomingtour, 'is_tour' => $is_tour );
	}

	public static function getMainPremiumSpot($limit = null)
	{
		$lng = Cubix_I18n::getLang();

		if (date('G') < 2)
			$date = 2;
		else
			$date = date('G');

		$sql = '
			SELECT
				/* escorts table */
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered,
				e.rates, e.incall_type, e.outcall_type, e.is_new, e.verified_status,
				e.photo_hash, e.photo_ext, e.photo_status,
				e.products, e.slogan, e.date_last_modified,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.is_suspicious,e.comment_count, e.review_count,
				
				ct.title_' . $lng . ' AS city,

				/* seo_entity_instances table */
				IF (sei.primary_id IS NULL, se.title_' . $lng . ', sei.title_' . $lng . ') AS alt,

				IF (e.available_24_7 = 1 OR (e.is_now_open = 1 AND e.close_time >= "' . $date . '"), 1, 0) AS is_late_night,
				/* misc */
				1 AS is_premium, 9 AS application_id, e.hh_is_active, e.is_online, e.about_' . $lng . ' AS about, UNIX_TIMESTAMP(e.date_last_modified) AS date_last_modified
			FROM escorts e
			INNER JOIN escorts_in_cities eic ON eic.escort_id = e.id AND eic.is_base = 1
			INNER JOIN cities ct ON ct.id = eic.city_id
			LEFT JOIN seo_entity_instances sei ON sei.primary_id = e.id
			LEFT JOIN seo_entities se ON (se.id = sei.entity_id) OR (se.application_id = 1 AND se.slug = "escort")
			WHERE
				eic.gender = ' . GENDER_FEMALE . ' AND e.is_main_premium_spot = 1
			GROUP BY e.id
		';
		
		if ( $limit ) {
			$sql .= " LIMIT " . $limit;
		}
		
		$rand_size = 50;

		$cache = Zend_Registry::get('cache');
		$cache_key = 'main_premium_spot_' . Cubix_Application::getId() . '_lang_' . Cubix_I18n::getLang();
		
		if ( $limit ) {
			$cache_key .= $cache_key . '_xml_premium' . rand(1, $rand_size);
		}

		if ( ! $escorts = $cache->load($cache_key) ) {
			
			$escorts = self::db()->fetchAll($sql);
			
			/* seo heading escorts */
			if ($escorts)
			{
				$es = array();
				foreach ($escorts as $e)
				{
					$es[] = $e->id;
				}
				$es_str = implode(',', $es);

				$query = "SELECT e.id, sei.heading_escort_{$lng} as heading_escort
					FROM escorts e
					INNER JOIN escorts_in_cities eic ON eic.escort_id = e.id AND eic.is_base = 1
					INNER JOIN seo_entity_instances sei ON sei.primary_id = eic.city_id
					INNER JOIN seo_entities se ON se.application_id = " . Cubix_Application::getId() . " AND se.slug = 'city' AND sei.entity_id = se.id
					WHERE e.id IN ({$es_str})
				";
				$result = self::db()->fetchAll($query);

				foreach ($result as $r)
				{				
					foreach ($escorts as $e)
					{
						if ($e->id == $r->id)
						{
							$e->heading_escort = $r->heading_escort;
							break;
						}
					}
				}
			}
			/**/

			if ( ! $limit ) {
				$cache->save($escorts, $cache_key, array());
			}
			else {
				$cache->save($escorts, $cache_key, array(), 1800);
			}
		}

		// <editor-fold defaultstate="collapsed" desc="Make Escort Alt for SEO">
		$tpl_data = array(
			'app_title' => Cubix_Application::getById()->title
		);

		foreach ( $escorts as $i =>  $escort ) {
			$additional = array(
				'showname' => $escort->showname,
				'city' => $escort->city
			);

			foreach ( $tpl_data + $additional as $tpl_var => $tpl_value ) {
				$escorts[$i]->alt = str_replace('%' . $tpl_var . '%', $tpl_value, $escorts[$i]->alt);
			}
		}
		// </editor-fold>

		shuffle($escorts);

		// <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
		$sid = 'sedcard_paging_' . Cubix_Application::getId();
		$sess = new Zend_Session_Namespace($sid);

		$sess->main_premium_spot = array();
		foreach ( $escorts as $escort ) {
			$sess->main_premium_spot[] = $escort->showname;
		}
		// </editor-fold>


		return $escorts;
	}
	
	public static function getCityPremiumSpot($filter, $ordering = 'random', $sess_name = false)
	{
		$lng = Cubix_I18n::getLang();

		if (date('G') < 2)
			$date = 2;
		else
			$date = date('G');

		if ( 'price' == substr($ordering, 0, 5) ) {
			$filter[] = 'e.incall_price IS NOT NULL';
		}
		
		// Only escorts with base city (or city tour) will be shown in this list
		if ( ! isset($filter['ct.slug = ?']) && ! isset($filter['r.slug = ?']) ) {
			$filter[] = 'eic.is_base = 1';
		}
		else {
			$filter[] = 'eic.is_upcoming = 0';
		}

		$filter[] = 'eic.is_premium = 1';
		$filter[] = 'eic.is_tour = 0';
		
		$where = self::getWhereClause($filter, true);
		$order = self::_mapSorting($ordering);
		
		
		//$order = str_replace('eic.is_premium', 'IF(eic.is_tour = 1, 0, eic.is_premium)', $order);
		$sql = '
			SELECT
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered,
				ct.title_' . $lng . ' AS city, e.rates, e.incall_type, e.outcall_type, e.is_new,
				e.verified_status, e.photo_hash, e.photo_ext, e.photo_status, 9 AS application_id,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price,

				IF (sei.primary_id IS NULL, se.title_' . $lng . ', sei.title_' . $lng . ') AS alt,
				IF (e.available_24_7 = 1 OR (e.is_now_open = 1 AND e.close_time >= "' . $date . '"), 1, 0) AS is_late_night,
				e.hit_count, e.slogan, e.incall_price,
				e.date_last_modified,
				IF(eic.is_tour = 1, 0, eic.is_premium) AS is_premium,
				IF (FIND_IN_SET(14, e.products) = 0, 0, 1) AS is_diamond,
				e.hh_is_active, e.is_suspicious, e.is_online, e.is_on_tour, e.tour_date_to, ctt.title_' . $lng . ' AS tour_city,
				e.about_' . $lng . ' AS about, UNIX_TIMESTAMP(e.date_last_modified) AS date_last_modified, ct.id AS city_id
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			LEFT JOIN cities ctt ON ctt.id = e.tour_city_id
			INNER JOIN regions r ON r.id = eic.region_id
			LEFT JOIN escort_cityzones ecz ON ecz.escort_id = eic.escort_id
			LEFT JOIN cityzones cz ON cz.id = ecz.city_zone_id
			LEFT JOIN seo_entity_instances sei ON sei.primary_id = eic.escort_id
			LEFT JOIN seo_entities se ON (se.id = sei.entity_id) OR (se.application_id = 1 AND se.slug = "escort")
			LEFT JOIN escort_services es ON es.escort_id = e.id
			' . (! is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.escort_id
			ORDER BY ' . $order . '
		';
		//echo $sql;die;
		$escorts = self::db()->fetchAll($sql);
		//$count = self::db()->fetchOne('SELECT FOUND_ROWS()');

		// <editor-fold defaultstate="collapsed" desc="Make Escort Alt for SEO">
		$tpl_data = array(
			'app_title' => Cubix_Application::getById()->title
		);

		foreach ( $escorts as $i =>  $escort ) {
			$additional = array(
				'showname' => $escort->showname,
				'city' => $escort->city
			);

			foreach ( $tpl_data + $additional as $tpl_var => $tpl_value ) {
				$escorts[$i]->alt = str_replace('%' . $tpl_var . '%', $tpl_value, $escorts[$i]->alt);
			}
		}
		// </editor-fold>

		if ( $sess_name ) {
			// <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
			$sid = 'sedcard_paging_' . Cubix_Application::getId();
			$sess = new Zend_Session_Namespace($sid);

			$sess->{$sess_name} = array();
			foreach ( $escorts as $escort ) {
				$sess->{$sess_name}[] = $escort->showname;
			}
			// </editor-fold>
		}

		return $escorts;
	}

	public static function getFiltered($filter, $ordering = 'random', $page = 1, $page_size = 50, &$count = 0, $sess_name = false, $only_regular = false, $where_query = array())
	{
		$lng = Cubix_I18n::getLang();

		if (date('G') < 2)
			$date = 2;
		else
			$date = date('G');

		if ( 'price' == substr($ordering, 0, 5) ) {
			$filter[] = 'e.incall_price IS NOT NULL';
		}
		
		// Only escorts with base city (or city tour) will be shown in this list
		if ( ! isset($filter['ct.slug = ?']) && ! isset($filter['r.slug = ?']) ) {
			$filter[] = 'eic.is_base = 1';
		}
		else {
			$filter[] = 'eic.is_upcoming = 0';
		}
		
		if ( $only_regular ) {
			$filter[] = 'eic.is_premium = 0';
		}

		$where = self::getWhereClause($filter, true);
		$order = self::_mapSorting($ordering);

		$page = (int) $page;
		if ( $page < 1 ) $page = 1;
		$limit = ($page_size * ($page - 1)) . ', ' . $page_size;
		
		$j = "";
		$f = "";
		if ( isset($filter['f.user_id = ?']) ) {
			$j = " INNER JOIN favorites f ON f.escort_id = e.id ";
			$f = " , f.user_id AS fav_user_id ";
		}

		if ( count($where_query) ) {
			foreach( $where_query as $wq ) {
				if ( strlen($wq['query']) ) {
					$where_query = ' AND ' . $wq['query'];
					$where .= $where_query;
				}
			}
		}

		$order = str_replace('eic.is_premium', 'IF(eic.is_tour = 1, 0, eic.is_premium)', $order);
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered,
				UNIX_TIMESTAMP(e.date_activated) AS date_activated,
				ct.title_' . $lng . ' AS city, e.rates, e.incall_type, e.outcall_type, e.is_new,
				e.verified_status, e.photo_hash, e.photo_ext, e.photo_status, 9 AS application_id,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.comment_count, e.review_count,

				/*IF (sei.primary_id IS NULL, se.title_' . $lng . ', sei.title_' . $lng . ') AS alt,*/
				IF (e.available_24_7 = 1 OR (e.is_now_open = 1 AND e.close_time >= "' . $date . '"), 1, 0) AS is_late_night,
				e.hit_count, e.slogan, e.incall_price,
				e.date_last_modified,
				IF(eic.is_tour = 1, 0, eic.is_premium) AS is_premium,
				IF (FIND_IN_SET(4, e.products) = 0, 0, 1) AS is_main_premium_spot,
				IF (FIND_IN_SET(14, e.products) = 0, 0, 1) AS is_diamond,
				e.hh_is_active, e.is_suspicious, e.is_online ' . $f . ', e.is_on_tour, e.tour_date_to, ctt.title_' . $lng . ' AS tour_city, 
				e.about_' . $lng . ' AS about, UNIX_TIMESTAMP(e.date_last_modified) AS date_last_modified, ct.id AS city_id,

				e.phone_instr, e.phone_instr_no_withheld, e.phone_instr_other, e.phone_country_id, e.disable_phone_prefix, e.phone_number
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			LEFT JOIN cities ctt ON ctt.id = e.tour_city_id
			INNER JOIN regions r ON r.id = eic.region_id
			' . $j . '
			LEFT JOIN escort_cityzones ecz ON ecz.escort_id = eic.escort_id
			LEFT JOIN cityzones cz ON cz.id = ecz.city_zone_id
			/*LEFT JOIN seo_entity_instances sei ON sei.primary_id = eic.escort_id
			LEFT JOIN seo_entities se ON (se.id = sei.entity_id) OR (se.application_id = 1 AND se.slug = "escort")*/
			/*LEFT JOIN escort_services es ON es.escort_id = e.id*/
			' . (! is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.escort_id
			ORDER BY ' . $order . '
			LIMIT ' . $limit . '
		';

		//echo $sql;die;
		//if ($_GET['ttt'] == 1)
		//	echo $sql;

		$escorts = self::db()->fetchAll($sql);
		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');

		/*$_ids = array();

		if ( count($escorts) ) {
			foreach($escorts as $escort) {
				$_ids[] = $escort->id;
			}

			$profile_sql = '
				SELECT ep.escort_id, ep.data FROM escort_profiles ep WHERE ep.escort_id IN (' . implode(', ', $_ids) . ')
			';
			$_escort_profiles = self::db()->fetchAll($profile_sql);

			$escort_profiles = array();
			foreach($_escort_profiles as $profile) {
				$escort_profiles[$profile->escort_id] = new Model_EscortV2Item(unserialize($profile->data));
			}

			foreach($escorts as $k => $escort) {
				$escorts[$k]->profile_data = $escort_profiles[$escort->id];
			}
		}*/



		// <editor-fold defaultstate="collapsed" desc="Make Escort Alt for SEO">
		/*$tpl_data = array(
			'app_title' => Cubix_Application::getById()->title
		);

		foreach ( $escorts as $i =>  $escort ) {
			$additional = array(
				'showname' => $escort->showname,
				'city' => $escort->city
			);

			foreach ( $tpl_data + $additional as $tpl_var => $tpl_value ) {
				$escorts[$i]->alt = str_replace('%' . $tpl_var . '%', $tpl_value, $escorts[$i]->alt);
			}
		}*/
		// </editor-fold>

		return $escorts;
	}

	public static function getTours($is_upcoming, $filter, $ordering = 'random', $page = 1, $page_size = 50, &$count = 0, $where_query = array())
	{
		$lng = Cubix_I18n::getLang();

		if ( 'price' == substr($ordering, 0, 5) ) {
			$filter[] = 'e.incall_price IS NOT NULL';
		}

		if ( isset($filter['eic.gender = 1']) ) unset($filter['eic.gender = 1']);
		
		if ( ! $is_upcoming ) {
			$filter[] = 'eic.is_tour = 1';
		}
		else {
			$filter[] = 'eic.is_upcoming = 1';
		}

		$where = self::getWhereClause($filter, true);
		$order = self::_mapSorting($ordering);

		$page = (int) $page;
		if ( $page < 1 ) $page = 1;
		$limit = ($page_size * ($page - 1)) . ', ' . $page_size;

		if ( count($where_query) ) {
			foreach( $where_query as $wq ) {
				if ( strlen($wq['query']) ) {
					$where_query = ' AND ' . $wq['query'];
					$where .= $where_query;
				}
			}
		}

		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered,
				ct.title_' . $lng . ' AS city, e.rates, e.incall_type, e.outcall_type, e.is_new,
				e.verified_status, e.photo_hash, e.photo_ext, e.photo_status, 9 AS application_id,
				/*IF (sei.primary_id IS NULL, IF(se1.id IS NOT NULL, se1.title_' . $lng . ', se2.title_' . $lng . '), sei.title_' . $lng . ') AS alt,*/
				e.hit_count, e.slogan, e.incall_price, e.comment_count, e.review_count,
				e.date_last_modified, IF (FIND_IN_SET(14, e.products) = 0, 0, 1) AS is_diamond,
				eic.is_premium, e.is_online, e.about_' . $lng . ' AS about, UNIX_TIMESTAMP(e.date_last_modified) AS date_last_modified
				' .
				(! $is_upcoming ? ', e.tour_date_from, e.tour_date_to' : // if
				($is_upcoming ? ', ut.tour_date_from, ut.tour_date_to' : '')) // else
				. '
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			/*LEFT JOIN seo_entity_instances sei ON sei.primary_id = eic.escort_id
			LEFT JOIN seo_entities se1 ON se1.id = sei.entity_id
			LEFT JOIN seo_entities se2 ON se2.slug = "escort" AND se2.application_id = 1*/
			/*LEFT JOIN escort_services es ON es.escort_id = e.id*/
			' . ($is_upcoming ? 'INNER JOIN upcoming_tours ut ON ut.id = eic.escort_id' : '') . '
			' . (! is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.escort_id
			ORDER BY ' . $order . '
			LIMIT ' . $limit . '
		';

		$escorts = self::db()->fetchAll($sql);
		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');

		/*$_ids = array();

		if ( count($escorts) ) {
			foreach($escorts as $escort) {
				$_ids[] = $escort->id;
			}

			$profile_sql = '
				SELECT ep.escort_id, ep.data FROM escort_profiles ep WHERE ep.escort_id IN (' . implode(', ', $_ids) . ')
			';
			$_escort_profiles = self::db()->fetchAll($profile_sql);

			$escort_profiles = array();
			foreach($_escort_profiles as $profile) {
				$escort_profiles[$profile->escort_id] = new Model_EscortV2Item(unserialize($profile->data));
			}

			foreach($escorts as $k => $escort) {
				$escorts[$k]->profile_data = $escort_profiles[$escort->id];
			}
		}*/

		// <editor-fold defaultstate="collapsed" desc="Make Escort Alt for SEO">
		/*$tpl_data = array(
			'app_title' => Cubix_Application::getById()->title
		);

		foreach ( $escorts as $i =>  $escort ) {
			$additional = array(
				'showname' => $escort->showname,
				'city' => $escort->city
			);

			foreach ( $tpl_data + $additional as $tpl_var => $tpl_value ) {
				$escorts[$i]->alt = str_replace('%' . $tpl_var . '%', $tpl_value, $escorts[$i]->alt);
			}
		}*/
		// </editor-fold>

		return $escorts;
	}

	private static function _mapSorting($param)
	{
		$map = array(
			'price-asc' => 'e.incall_price ASC',
			'price-desc' => 'e.incall_price DESC',
			'random' => 'is_diamond DESC, is_main_premium_spot DESC, eic.is_premium DESC, eic.ordering DESC',
			'alpha' => 'e.showname ASC',
			'most-viewed' => 'e.hit_count DESC',
			'newest' => 'e.is_new DESC, COALESCE(e.date_activated, e.date_registered) DESC',
			'last-modified' => 'e.date_last_modified DESC'
		);
		
		$order = 'e.ordering DESC';
		if ( isset($map[$param]) ) {
			$order = $map[$param];
		}
		elseif ( false !== strpos($param, 'FIELD') ) {
			$order = $param;
		}

		return $order;
	}

	public static function getPrevNext( $showname )
	{
		return array(null, null);
		// Retrieve criterias from session
		$sid = 'v2_sedcard_paging_' . Cubix_Application::getId();
		$sess = new Zend_Session_Namespace($sid);

		$page = $sess->page;
		$page_size = $sess->page_size;
		$pages_count = $sess->pages_count;

		list($filter, $ordering) = $sess->criteria;
		foreach ( $sess->escorts as $i => $name ) {
			if ( $name == $showname ) {
				$prev_i = $i - 1;
				$next_i = $i + 1;

				break;
			}
		}

		echo "Prev_i: $prev_i, Next_i: $next_i\n";
		//print_r($sess->escorts);

		if ( ! isset($prev_i) && ! isset($next_i) ) {
			return array(null, null);
		}

		// This is first escort, we need to fetch data from prev page
		if ( isset($prev_i) && (! isset($sess->escorts[$prev_i]) || $prev_i < 0) && $page > 1 ) {
			$sess->escorts = array_slice($sess->escorts, 0, $page_size * 2);

			$escorts = call_user_func_array(
				array(__CLASS__, $sess->callback),
				array($filter, $ordering, $page - 1, $page_size)
			);
			foreach ( $escorts as $i => $escort ) {
				$escorts[$i] = $escort->showname;
			}

			$sess->escorts = array_merge($escorts, $sess->escorts);
			
			$prev_i = $page_size - 1;
		}

		
		echo "Escorts after 'first' logic\n";
		echo "Prev_i: $prev_i, Next_i: $next_i\n";
		//print_r($sess->escorts);
		
		// If this last escort on page, we need to fetch data from next page
		if ( isset($next_i) &&  (! isset($sess->escorts[$next_i]) || $next_i > count($sess->escorts) - 1 ) && $page < $pages_count ) {
			if ( count($sess->escorts) >= $page_size * 3 ) {
				$sess->escorts = array_slice($sess->escorts, $page_size, $page_size * 2);
				$next_i -= $page_size;
			}
			
			$escorts = call_user_func_array(
				array(__CLASS__, $sess->callback),
				array($filter, $ordering, $page + 1, $page_size)
			);
			foreach ( $escorts as $i => $escort ) {
				$escorts[$i] = $escort->showname;
			}

			$sess->escorts = array_merge($sess->escorts, $escorts);
		}

		echo "Escorts after 'last' logic\n";
		echo "Prev_i: $prev_i, Next_i: $next_i\n";
		//print_r($sess->escorts);
		
		return array(isset($prev_i) ? $sess->escorts[$prev_i] : null, isset($next_i) ? $sess->escorts[$next_i] : null);
	}

	//escort type ( 0 = ALL, 1 - premium, 2 regular)
	public static function getCityEscortsCount($city, $escort_type = 0 )
	{
		$where_type = "";
		if( $escort_type == 1){
			$where_type = ' AND eic.is_premium = 1 AND eic.is_tour = 0 ';
		}
		elseif($escort_type == 2){
			$where_type = ' AND eic.is_premium = 0';
		}

		$sql = '
			SELECT COUNT(DISTINCT(eic.escort_id))
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			INNER JOIN regions r ON r.id = eic.region_id
			WHERE eic.is_upcoming = 0 AND e.gender = ' . GENDER_FEMALE. ' AND ct.slug = "'. $city.'"'.$where_type. '
		';
		
		return self::db()->fetchOne($sql);
	}
}
