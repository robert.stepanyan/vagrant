<?php
class Model_MmgBillAPIV2
{
    private $payment_link = 'https://secure.mmgbill.com/payment/?';
    private $merchantId = '';
    private $secret_key = '';
    private $ts = 'd'; //Stands for dynamic system
    private $currencyCode = '978';

    public function __construct()
    {
        $envConfKey = APPLICATION_ENV == 'production' ? 'production' : 'test';
        $config = [
            'test' => [
                'merchantId' => 'M1572',
                'secret_key' => 'qaRxBH6laojLkfkIbbUiRupyJLL0rcfv',
                'api_user' => null, //'24e64d0c75ca36f24b3467677fc965c9aee87667e47e8aeccbbb2ea69d6ec230',
                'api_pass' => null, //'voOexQiaqgNY3JFedNfQ4J6AKf9hTUJp',
            ],
            'production' => [
                'merchantId' => 'M1627',
                'secret_key' => 'I4LPkLSlVFNvSmJFkDcfhyRKUulfGu3H',
                'api_user' => 'd1ab111ffbfb186ce0460dc3c472d067e4e32c73a7765abb36183f11e4cce0b9',
                'api_pass' => '248YxtIkTXVti6Bl7ldDslA3lH53qSem',
            ]
        ];

        $this->merchantId = $config[$envConfKey]['merchantId'];
        $this->secret_key = $config[$envConfKey]['secret_key'];
        $this->api_user = $config[$envConfKey]['api_user'];
        $this->api_pass = $config[$envConfKey]['api_pass'];
    }

    public function getHostedPageUrl($amount, $transaction_id, $postback_url)
    {
        $c1 = 'escortitalia:' . $amount;
        $amount = $amount * 100;

        $array_request = array(
            'mid' => $this->merchantId,
            'ts' => $this->ts,
            'ti_mer' => $transaction_id,
            'txn_type' => 'SALE',
            'cu_num' => $this->currencyCode,
            'amc' => $amount,
            'surl' => $postback_url,
            'c2' => 'escortitalia.com'
        );

        /* create the hash signature */
        $sh = $this->create_hash($array_request, $this->secret_key);

        /* create the payment link */
        $payment_link = $this->create_payment_link($array_request, $this->payment_link, $sh);

        return $payment_link;
    }

    /* function to create the hash signature */
    private function create_hash ($array_request, $secret_key)
    {
        $hash_string = "";
        ksort($array_request);
        foreach ($array_request as $key => $value) {
            $hash_string .= "|" . $key . $value;
        }

        return hash_hmac("sha256", $hash_string, $secret_key, false);
    }

    /* function to create payment link */
    private function create_payment_link($array_request, $payment_link, $sh)
    {
        foreach ($array_request as $key => $value) {
            $payment_link .= $key . "=" . $value . "&";
        }

        return $payment_link .= "sh=" . $sh;
    }


}
