<?php

class Model_Blog extends Cubix_Model
{
	public function getPosts($filter, $page, $per_page)
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS 
				b.id, b.slug, b.title_en, ' . Cubix_I18n::getTblField('b.title') . ' AS title, b.short_post_en,
				' . Cubix_I18n::getTblField('b.short_post') . ' AS short_post, UNIX_TIMESTAMP(b.date) AS date, i.hash, i.ext
			FROM blog_posts b
			LEFT JOIN blog_images i ON i.post_id = b.id
			WHERE 1
		';

		if (strlen($filter['search']))
		{
			$sql .= '
			     AND (
			     b.title_en LIKE "%' . mysql_escape_string($filter['search']) . '%"
			     OR b.short_post_en LIKE "%' . mysql_escape_string($filter['search']) . '%"
			     OR b.post_en LIKE "%' . mysql_escape_string($filter['search']) . '%"
			     OR ' . Cubix_I18n::getTblField('b.title') . ' LIKE "%' . mysql_escape_string($filter['search']) . '%"
			     OR ' . Cubix_I18n::getTblField('b.short_post') . ' LIKE "%' . mysql_escape_string($filter['search']) . '%"
			     OR ' . Cubix_I18n::getTblField('b.post') . ' LIKE "%' . mysql_escape_string($filter['search']) . '%"
			     )
			';
		}

		if (strlen($filter['month']) && strlen($filter['year']))
		{
			$sql .= ' AND MONTH(b.date) = ' . $filter['month'] . ' AND YEAR(b.date) = ' . $filter['year'];
		}

		$sql .= '
			GROUP BY b.id
			ORDER BY b.date DESC
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page;

		$data = parent::getAdapter()->query($sql)->fetchAll();
		$count = parent::getAdapter()->fetchOne('SELECT FOUND_ROWS()');

		if ($data)
		{
			foreach ($data as $k => $v)
			{
				$photo = new Cubix_ImagesCommonItem(array('hash' => $v->hash, 'ext' => $v->ext, 'application_id' => Cubix_Application::getId()));

				$data[$k]->photo = $photo;
			}
		}

		$items = array(
			'data'	=> $data,
			'count'	=> $count
		);

		return $items;
	}


    public function getTwBlogMetaData() {
        $db = Zend_Registry::get('db');
        $blg_data = $db->fetchOne('SELECT tw_blg_data FROM applications');
        return unserialize($blg_data);
    }

	public function getRandomPosts($ex_ids, $count)
	{
		$sql = '
			SELECT * FROM (SELECT
				b.id, b.slug, b.title_en, ' . Cubix_I18n::getTblField('b.title') . ' AS title, b.short_post_en,
				' . Cubix_I18n::getTblField('b.short_post') . ' AS short_post, UNIX_TIMESTAMP(b.date) AS date, i.hash, i.ext
			FROM blog_posts b
			LEFT JOIN blog_images i ON i.post_id = b.id
			WHERE b.id NOT IN (' . $ex_ids . ')
			GROUP BY b.id
			ORDER BY RAND()
			LIMIT ' . $count . ') t ORDER BY date DESC';

		$data = parent::getAdapter()->query($sql)->fetchAll();

		if ($data)
		{
			foreach ($data as $k => $v)
			{
				$photo = new Cubix_ImagesCommonItem(array('hash' => $v->hash, 'ext' => $v->ext, 'application_id' => Cubix_Application::getId()));

				$data[$k]->photo = $photo;
			}
		}

		return $data;
	}
	
	public function getArchiveMonthList()
	{
		return parent::getAdapter()->query('
			SELECT MONTH(date) AS month, YEAR(date) AS year, COUNT(month_count) AS count
			FROM blog_posts 
			WHERE DATE_FORMAT(date, "%m-%Y") <> ?
			GROUP BY month_count 
			ORDER BY month_count DESC
		', date('m') . '-' . date('Y'))->fetchAll();
	}

	public function getPost($id, $slug)
	{
		$data = parent::getAdapter()->query('
			SELECT
				id, title_en, ' . Cubix_I18n::getTblField('title') . ' AS title, post_en, slug,
				' . Cubix_I18n::getTblField('post') . ' AS post, UNIX_TIMESTAMP(date) AS date
				FROM blog_posts
				WHERE id = ? AND slug = ?
		', array($id, $slug))->fetch();

		$photos = parent::getAdapter()->query('SELECT hash, ext FROM blog_images WHERE post_id = ? ORDER BY image_id', $id)->fetchAll();

		foreach ($photos as $k => $photo)
		{
			$photos[$k] = new Cubix_ImagesCommonItem(array('hash' => $photo->hash, 'ext' => $photo->ext, 'application_id' => Cubix_Application::getId()));
		}

		return array('data' => $data, 'photos' => $photos);
	}
	
	public function getComments($post_id, $page, $per_page, $per_page_replies)
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS 
				id, post_id, comment, name, UNIX_TIMESTAMP(date) AS date
			FROM blog_comments
			WHERE post_id = ? AND is_reply = 0 
		';
		
		$sql .= '
			ORDER BY date DESC
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page;
		
		$data = parent::getAdapter()->query($sql, array($post_id))->fetchAll();
		$count = parent::getAdapter()->fetchOne('SELECT FOUND_ROWS()');
		
		if ($data)
		{
			foreach ($data as $k => $v)
			{
				$data[$k]->replies = $this->getReplies($post_id, $v->id, 1, $per_page_replies);
			}
		}
				
		$items = array(
			'data'	=> $data,
			'count'	=> $count
		);
		
		return $items;
	}
	
	public function addComment($data)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Blog.addComment', array($data));
	}
	
	public function getPostIdByCommentId($comment_id)
	{
		return parent::getAdapter()->fetchOne('SELECT post_id FROM blog_comments WHERE id = ?', $comment_id);
	}
	
	public function getReplies($post_id, $comment_id, $page, $per_page)
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS 
				id, post_id, comment, name, UNIX_TIMESTAMP(date) AS date, is_reply, by_admin
			FROM blog_comments
			WHERE post_id = ? AND is_reply = ? 
		';
		
		$sql .= '
			ORDER BY date DESC
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page;
		
		$data = parent::getAdapter()->query($sql, array($post_id, $comment_id))->fetchAll();
		$count = parent::getAdapter()->fetchOne('SELECT FOUND_ROWS()');
						
		$items = array(
			'data'	=> $data,
			'count'	=> $count
		);
		
		return $items;
	}

	public function updateViewCount($id)
	{
		$this->getAdapter()->query('UPDATE blog_posts SET view_count = view_count + 1 WHERE id = ?', array($id));

		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$client->call('Blog.updateViewCount', array($id));
	}
}
