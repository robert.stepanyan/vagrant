var RatesEditor = new Class({
	Implements: [Options, Events],

	options: {
		inputName: 'rates[]',
		labels: {
			norates: 'Youd don\'t have set any rates yet'
		}
	},

	element: null,
	list: null,
	elements: {},

	currency: 2,

	store: null,

	initialize: function (element, list, options) {
		this.element = $(element);
		this.list = $(list);
		this.setOptions(options || {});

		this.elements = this.element
			.getElements('td input[type=text], td select, td input[type=hidden], td div.btn-add')
			.associate(['time', 'unit', 'price', 'currency', 'btnAdd']);
		for ( var i in this.elements ) {
			var el = this.elements[i];
			el.showError = function () {
				//this.setStyle('background-image', 'none').get('tween', { property: 'background-color', duration: 'short', onComplete: function () { this.element.setStyle('background', '') } }).set('#ffcccc').start('#ffffff');
			}.bind(el);
		}
		
		this.elements.hiddens = {};

		this.store = new RatesStore().addEvents({
			change: function (store) {
				if ( store.getLength() > 0 && this.list.getNext('p') ) {
					this.list.setStyle('display', 'block').getNext('p').destroy();
				}
				else if ( store.getLength() == 0 && ! this.list.getNext('p') ) {
					new Element('p', { html: this.options.labels.norates }).inject(
						this.list.setStyle('display', 'none'), 'after'
					);
				}
			}.bind(this),
			add: function (rate) {
				this.elements.hiddens[rate.id] = new Element('input', {
					type: 'hidden',
					name: this.options.inputName,
					value: JSON.encode(rate.getValue())
				}).inject(this.list);
			}.bind(this),
			erase: function (rate) {
				this.elements.hiddens[rate.id].destroy();
			}.bind(this)
		});

		this.init();
	},

	add: function (rate) {
		var found = this.store.find(rate);
		if ( found ) {
		}

		var el = rate.render();
		new Element('div', {
			'class': 'btn-remove',
			events: {
				click: function () {
					this.remove(rate.id);
				}.bind(this)
			}
		}).inject(el);

		if ( ! found ) {
			el.inject(this.list);
		}
		else {
			var found = this.store.erase(found.id);
			el.replaces(found.element);
		}


		el.highlight();

		this.store.add(rate);
	},

	remove: function (id) {
		var rate = this.store.erase(id);
		if ( ! rate ) return;
		rate.element.destroy();
	},

	// Returns false if not valid data supplied
	getValues: function () {
		var els = this.elements;

		var values = {
			time: els.time.get('value'),
			unit: els.unit.getSelected()[0].get('value'),
			price: els.price.get('value'),
			currency: els.currency.get('value')
		};

		var error = false;
		if ( ! values.time.test('^[0-9]+$') ) {
			error = true;
			els.time.showError();
		}

		if ( ! values.price.test('^[0-9]+$') ) {
			error = true;
			els.price.showError();
		}

		return error ? false : values;
	},

	init: function () {
		this.elements.btnAdd.addEvent('click', function () {
			var values = this.getValues();
			if ( ! values ) return;
			this.add(new Rate.Basic(values));
		}.bind(this));
	},

	load: function (data) {
		
		data.each(function (rate) {
			
			if ( rate.type ) {
				var type = rate.type.camelCase();
				type = type.substring(0, 1).toUpperCase() + type.substring(1);
				this.add(new Rate[type](rate));
			}
			else {
				this.add(new Rate.Basic(rate));
			}
		}.bind(this));
	}
});

RatesEditor.Currencies = {};

var RatesStore = new Class({
	Implements: [Events],
	rates: null,

	initialize: function () {
		this.rates = new Hash();
	},

	add: function (rate) {
		this.rates.set(rate.id, rate);
		this.fireEvent('change', [this]);
		this.fireEvent('add', [rate]);
	},

	get: function (id) {
		return this.rates.get(id);
	},

	erase: function (id) {
		var found = this.get(id);
		if ( ! found ) return;
		this.rates.erase(id);
		this.fireEvent('change', [this]);
		this.fireEvent('erase', [found]);
		return found;
	},

	find: function (rate) {
		var found = false;
		this.rates.each(function (_rate) {
			if ( found ) return;
			found = _rate.equal(rate) ? _rate : false;
		});

		return found;
	},

	getLength: function () {
		return this.rates.getLength();
	}
});

RatesStore.getId = function () {
	if ( ! $defined(RatesStore._id) ) {
		RatesStore._id = 0;
	}

	return RatesStore._id++;
}

var Rate = {};
Rate.Base = new Class({
	element: null,
	params: null,
	initialize: function (params) {
		this.id = RatesStore.getId();

		$extend(this, params);
		this.params = params;
	},

	render: function () {
		this.element = new Element('li');

		return this.element;
	},

	equal: function (rate) {
		return false;
	},

	getValue: function () {
		var params = {};
		for ( var param in this.params ) {
			params[param] = this[param];
		}

		return params;
	}
});

Rate.Basic = new Class({
	Extends: Rate.Base,

	render: function () {
		var el = this.parent();
		
		return el.adopt(
			new Element('div', { html: this.time }),
			new Element('div', { html: RatesEditor.Units[this.unit] }),
			new Element('div', { html: this.price }),
			new Element('div', { html: RatesEditor.Currencies[this.currency] })
		);
	},

	equal: function (rate) {
		if (this.time == rate.time && this.unit == rate.unit ) {
			return true;
		}
		else {
			return false;
		}
	}
});

Rate.AdditionalHour = new Class({
	Extends: Rate.Base,

	initialize: function (params) {
		params.type = 'additional-hour';
		this.parent(params);
	},

	render: function (rate) {
		var el = this.parent();

		return el.adopt(
			new Element('div', { html: 'additional hour' }),
			new Element('div', { html: this.price }),
			new Element('div', { html: RatesEditor.Currencies[this.currency] })
		);
	},

	equal: function (rate) {
		if (this.type == rate.type ) {
			return true;
		}
		else {
			return false;
		}
	}
});

Rate.Overnight = new Class({
	Extends: Rate.Base,

	initialize: function (params) {
		params.type = 'overnight';
		this.parent(params);
	},

	render: function () {
		var el = this.parent();

		return el.adopt(
			new Element('div', { html: 'overnight' }),
			new Element('div', { html: this.price }),
			new Element('div', { html: RatesEditor.Currencies[this.currency] })
		);
	},

	equal: function (rate) {
		if (this.type == rate.type ) {
			return true;
		}
		else {
			return false;
		}
	}
});

Rate.DinnerDate = new Class({
	Extends: Rate.Base,

	initialize: function (params) {
		params.type = 'dinner-date';
		this.parent(params);
	},

	render: function () {
		var el = this.parent();

		return el.adopt(
			new Element('div', { html: 'dinner date' }),
			new Element('div', { html: this.price }),
			new Element('div', { html: RatesEditor.Currencies[this.currency] })
		);
	},

	equal: function (rate) {
		if (this.type == rate.type ) {
			return true;
		}
		else {
			return false;
		}
	}
});

Rate.Weekend = new Class({
	Extends: Rate.Base,

	initialize: function (params) {
		params.type = 'weekend';
		this.parent(params);
	},

	render: function () {
		var el = this.parent();

		return el.adopt(
			new Element('div', { html: 'weekend' }),
			new Element('div', { html: this.price }),
			new Element('div', { html: RatesEditor.Currencies[this.currency] })
		);
	},

	equal: function (rate) {
		if (this.type == rate.type ) {
			return true;
		}
		else {
			return false;
		}
	}
});

RatesEditor.Incall = new Class({
	Extends: RatesEditor,

	options: {
		inputName: 'rates[incall][]',
		labels: {
			norates: 'Youd don\'t have set any incall rates yet'
		}
	},

	init: function () {
		this.parent();

		$extend(this.elements,
			this.element.getElements('.quick-options .btn-add').associate(['btnAdditionalHour', 'btnOvernight', 'btnDinnerDate', 'btnWeekend'])
		);

		this.elements.btnAdditionalHour.addEvent('click', function () {
			var values = this._getValues(this.elements.btnAdditionalHour);
			if ( ! values ) return;
			var rate = new Rate.AdditionalHour({
				type: 'additional-hour',
				price: values.price,
				currency: values.currency
			});

			this.add(rate);
		}.bind(this));

		this.elements.btnOvernight.addEvent('click', function () {
			var values = this._getValues(this.elements.btnOvernight);
			if ( ! values ) return;
			var rate = new Rate.Overnight({
				type: 'overnight',
				price: values.price,
				currency: values.currency
			});

			this.add(rate);
		}.bind(this));

		this.elements.btnDinnerDate.addEvent('click', function () {
			var values = this._getValues(this.elements.btnDinnerDate);
			if ( ! values ) return;
			var rate = new Rate.DinnerDate({
				type: 'dinner-date',
				price: values.price,
				currency: values.currency
			});

			this.add(rate);
		}.bind(this));

		this.elements.btnWeekend.addEvent('click', function () {
			var values = this._getValues(this.elements.btnWeekend);
			if ( ! values ) return;
			var rate = new Rate.Weekend({
				type: 'weekend',
				price: values.price,
				currency: values.currency
			});

			this.add(rate);
		}.bind(this));
	},

	_getValues: function (el) {
		var price = el.getParent().getElement('input[type=text]');

		var values =  {
			price: price.get('value'),
			currency: el.getParent().getElement('input[type=hidden]').get('value')
		};

		if ( ! values.price.test('^[0-9]+$') ) {
			//price.setStyle('background-image', 'none').get('tween', { property: 'background-color', duration: 'short', onComplete: function () { this.element.setStyle('background', '') } }).set('#ffcccc').start('#ffffff');
			return false;
		}

		return values;
	}
});

RatesEditor.Outcall = new Class({
	Extends: RatesEditor.Incall,

	options: {
		inputName: 'rates[outcall][]',
		labels: {
			norates: 'Youd don\'t have set any outcall rates yet'
		}
	},

	init: function () {
		this.parent();

		$extend(this.elements,
			this.element.getElements('.quick-options .btn-add').associate(['btnAdditionalHour', 'btnOvernight'/*, 'btnDinnerDate', 'btnWeekend'*/])
		);

		/*this.elements.btnDinnerDate.addEvent('click', function () {
			var values = this._getValues(this.elements.btnDinnerDate);
			if ( ! values ) return;
			var rate = new Rate.DinnerDate({
				type: 'dinner-date',
				price: values.price,
				currency: values.currency
			});

			this.add(rate);
		}.bind(this));

		this.elements.btnWeekend.addEvent('click', function () {
			var values = this._getValues(this.elements.btnWeekend);
			if ( ! values ) return;
			var rate = new Rate.Weekend({
				type: 'weekend',
				price: values.price,
				currency: values.currency
			});

			this.add(rate);
		}.bind(this));*/
	}
});
