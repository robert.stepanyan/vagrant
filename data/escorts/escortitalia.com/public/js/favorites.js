/* --> Favorites */
Cubix.Favorites = {};

Cubix.Favorites.containerMain = 'common-favs';
Cubix.Favorites.url = '/private/ajax-favorites';
Cubix.Favorites.containerTop = 'top-favs';
Cubix.Favorites.urlTop = '/private/ajax-favorites-top';
Cubix.Favorites.search_input_text = 'search by showname';
Cubix.Favorites.showname = "";
Cubix.Favorites.allowScroll = true;

Cubix.Favorites.Init = function () {
	Cubix.Favorites.InitSearchInput('search');
	
	if(Cookie.read('favoriteAct')){
		Cubix.Favorites.Show({page: 1, act: Cookie.read('favoriteAct')}, false);
	}else{
		Cubix.Favorites.Show({page: 1}, false);
	}
	
	Cubix.Favorites.ShowTop();
	
	return false;
}

Cubix.Favorites.InitSearchInput = function (input) {
	Cubix.Favorites.InitSearchInput.input = $(input);
	
	this.timer = null;
		
	Cubix.Favorites.InitSearchInput.input.addEvent('focus', function() {
		if ( this.get('value') == Cubix.Favorites.search_input_text ) {
			this.set('value', '');
			this.removeClass('def-text');
		}	
	});
	
	Cubix.Favorites.InitSearchInput.input.addEvent('blur', function() {
		if ( ! this.get('value').length ) {
			this.set('value', Cubix.Favorites.search_input_text);
			this.addClass('def-text');
		}
	});
	
	Cubix.Favorites.InitSearchInput.input.addEvents({
		keyup: function () {
			$clear(this.timer);
			this.timer = setTimeout('Cubix.Favorites.InitSearchInput.KeyUp(Cubix.Favorites.InitSearchInput.input)', 500);
		}.bind(this)
	});
}

Cubix.Favorites.InitSearchInput.KeyUp = function (input) {	
	var value = ( input.get('value').length && input.get('value') != Cubix.Favorites.search_input_text ) ? input.get('value') : '';
	
	Cubix.Favorites.Show({showname: value, page: 1}, false);
}

Cubix.Favorites.Show = function(data, allowScroll) {
	$('fav-msg').set('class', '');
	$('fav-msg').set('html', '');
	$('fav-msg').addClass('none');
	
	var container = Cubix.Favorites.containerMain;
	data.showname = data.showname ? data.showname : Cubix.Favorites.showname;
	
	if ($('city').get('value'))
		data.city = $('city').get('value');
	
	if (data.showname == Cubix.Favorites.showname)
	{
		if ($('search').get('value').length > 0 && $('search').get('value') != Cubix.Favorites.search_input_text)
			data.showname = $('search').get('value');
	}
	
	if (!data.act)
	{
		data.act = $('active-action').get('value');
	}
	else
	{
		$('active-action').set('value', data.act);
	}
	
	if (!data.per_page)
	{
		data.per_page = $('page-action').get('value');
	}
	else
	{
		$('page-action').set('value', data.per_page);
	}
		
	var myScroll = new Fx.Scroll(window);
	var overlay = new Cubix.Overlay($(container), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	overlay.disable();
	
	new Request({
		url: Cubix.Favorites.url,
		method: 'get',
		data: data,
		onSuccess: function (resp) {			
			$(container).set('html', resp);
			
			/* personal comment */
			openPC();
			/**/
			
			/* active action */
			$('act-all').set('class', '');
			$('act-active').set('class', '');
			$('act-inactive').set('class', '');

			if (data.act == 1)
			{
				$('act-all').addClass('sel');
			}
			else if (data.act == 2)
			{
				$('act-active').addClass('sel');
			}
			else if (data.act == 3)
			{
				$('act-inactive').addClass('sel');
			}
			/**/
			
			/* page action */
			$('page-12').set('class', '');
			$('page-24').set('class', '');
			$('page-48').set('class', '');
			$('page-96').set('class', '');
			$('page-all').set('class', '');
			
			if (data.per_page == 12)
			{
				$('page-12').addClass('sel');
			}
			else if (data.per_page == 24)
			{
				$('page-24').addClass('sel');
			}
			else if (data.per_page == 48)
			{
				$('page-48').addClass('sel');
			}
			else if (data.per_page == 96)
			{
				$('page-96').addClass('sel');
			}
			else if (data.per_page == 1000)
			{
				$('page-all').addClass('sel');
			}
			/**/
			
			overlay.enable();
			
			if (allowScroll) {
				myScroll.toElement($('favorites'));
			}
		}
	}).send();
	
	return false;
}

function openPC()
{
	$$('.plus').addEvent('click', function() {
		var self = this;

		self.removeClass('plus');
		self.addClass('minus');

		var controlBox = self.getParent().getParent();
		controlBox.setStyle('height', '218px');
		var form = self.getNext('form');
		form.removeClass('none');
		
		closePC();
	});
}

function closePC()
{
	$$('.minus').addEvent('click', function() {
		var self = this;

		self.removeClass('minus');
		self.addClass('plus');

		var controlBox = self.getParent().getParent();
		controlBox.setStyle('height', '100px');
		var form = self.getNext('form');
		form.addClass('none');
		
		openPC();
	});
}

Cubix.Favorites.ShowTop = function() {
	var container = Cubix.Favorites.containerTop;
	
	var myScroll = new Fx.Scroll(window);
	var overlay = new Cubix.Overlay($(container), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	overlay.disable();
	
	new Request({
		url: Cubix.Favorites.urlTop,
		method: 'get',
		onSuccess: function (resp) {			
			$(container).set('html', resp);
			overlay.enable();			
		}
	}).send();
	
	return false;
}

Cubix.Favorites.AddToTop10 = function(escortId) {
	var myScroll = new Fx.Scroll(window);
	
	var data = {
		escort_id: escortId
	}
	
	new Request.JSON({
		url: '/private/ajax-favorites-add-to-top',
		method: 'post',
		data: data,
		onSuccess: function (resp) {
			if (resp.success) {
				Cubix.Favorites.Show({page: 1}, false);
				Cubix.Favorites.ShowTop();
				
				$('fav-msg').removeClass('none');
				$('fav-msg').addClass('msg-suc');
				$('fav-msg').set('html', resp.success);
				
				myScroll.toElement($('favorites'));
			}
			else
			{
				$('fav-msg').removeClass('none');
				$('fav-msg').addClass('msg-err');
				$('fav-msg').set('html', resp.error);
				
				myScroll.toElement($('favorites'));
			}
		}
	}).send();
	
	return false;
}

Cubix.Favorites.AddComment = function(form) {
	var data = {
		comment: form.getElement('textarea').get('value'),
		escort_id: form.getElement('input').get('value')
	} 
	
	var formOverlay = new Cubix.Overlay(form, {
		loader: _st('loader-circular-tiny.gif'),
		position: '50%'
	});
	
	formOverlay.disable();
	
	new Request.JSON({
		url: '/private/add-fav-comment',
		method: 'post',
		data: data,
		onSuccess: function (resp) {
			if (resp.success) {
				formOverlay.enable();
				Cubix.Favorites.ShowTop();
			}
		}
	}).send();
	
	return false;
}

Cubix.Favorites.Remove = function(escortId) {
	var myScroll = new Fx.Scroll(window);
	
	var data = {
		escort_id: escortId
	}
	
	new Request.JSON({
		url: '/private/remove-from-favorites',
		method: 'post',
		data: data,
		onSuccess: function (resp) {
			if (resp.success) {
				Cubix.Favorites.Show({page: 1}, false);
				Cubix.Favorites.ShowTop();
				
				$('fav-msg').removeClass('none');
				$('fav-msg').addClass('msg-suc');
				$('fav-msg').set('html', resp.success);
				
				myScroll.toElement($('favorites'));
			}
		}
	}).send();
	
	return false;
}

Cubix.Favorites.Up = function(escortId) {	
	var data = {
		escort_id: escortId
	}
	
	new Request.JSON({
		url: '/private/ajax-favorites-up',
		method: 'post',
		data: data,
		onSuccess: function (resp) {
			if (resp.success) {
				Cubix.Favorites.ShowTop();
			}
		}
	}).send();
	
	return false;
}

Cubix.Favorites.Down = function(escortId) {	
	var data = {
		escort_id: escortId
	}
	
	new Request.JSON({
		url: '/private/ajax-favorites-down',
		method: 'post',
		data: data,
		onSuccess: function (resp) {
			if (resp.success) {
				Cubix.Favorites.ShowTop();
			}
		}
	}).send();
	
	return false;
}

Cubix.Favorites.RemoveFromTop10 = function(escortId) {
	var myScroll = new Fx.Scroll(window);
	
	var data = {
		escort_id: escortId
	}
	
	new Request.JSON({
		url: '/private/ajax-favorites-remove-from-top',
		method: 'post',
		data: data,
		onSuccess: function (resp) {
			if (resp.success) {
				Cubix.Favorites.Show({page: 1}, false);
				Cubix.Favorites.ShowTop();
				
				$('fav-msg').removeClass('none');
				$('fav-msg').addClass('msg-suc');
				$('fav-msg').set('html', resp.success);
				
				myScroll.toElement($('favorites'));
			}
		}
	}).send();
	
	return false;
}

Cubix.Favorites.ChangeType = function (f_id) {
	var overlay = new Cubix.Overlay($('top-item-' + f_id), {
		loader: _st('loader-circular-tiny.gif'),
		position: '50%'
	});
	
	overlay.disable();
	
	var new_val = $$('input[name=type_' + f_id + ']:checked')[0].get('value');
	var data = {
		new_val: new_val,
		f_id: f_id
	};
	
	new Request.JSON({
		url: '/private/change-fav-comment-type',
		method: 'post',
		data: data,
		onSuccess: function (resp) {
			if (resp.success) {
				if (new_val == 1)
				{
					$('blocked-' + f_id).removeClass('none');
					
					if ($('c-' + f_id))
					{
						$('c-' + f_id).addClass('none');
					}
				}
				else if (new_val == 2)
				{
					$('blocked-' + f_id).addClass('none');
					
					if ($('c-' + f_id))
					{
						$('c-' + f_id).removeClass('none');
					}
				}
				
				overlay.enable();
			}
		}
	}).send();
}

Cubix.Favorites.OpenPopup = function (f_id) {
	var myScroll = new Fx.Scroll(window);
	
	$('fr-popup-' + f_id).removeClass('none');
	
	myScroll.toElement($('fr-popup-' + f_id));
}

Cubix.Favorites.X = function (f_id) {
	$('fr-popup-' + f_id).addClass('none');
	
	return false;
}
Cubix.Favorites.Share = function (req_id, f_id, type) {
	var overlay = new Cubix.Overlay($('fav-popup-middle-wrap-' + f_id), {
		loader: _st('loader-circular-tiny.gif'),
		position: '50%'
	});
	
	overlay.disable();
	
	var data = {
		req_id: req_id,
		type: type
	};
	
	new Request.JSON({
		url: '/private/share-fav-comment',
		method: 'post',
		data: data,
		onSuccess: function (resp) {
			if (resp.success) {
				$('r-row-' + req_id).destroy();
				var c = parseInt($('c-' + f_id).get('html'));
				c--;

				if (c > 0)
					$('c-' + f_id).set('html', c);
				else
				{
					$('c-' + f_id).destroy();
					$('fr-popup-' + f_id).destroy();
				}

				overlay.enable();
			}
		}
	}).send();
		
	return false;
}