window.addEvent('domready', function(){

	if ( $defined($$('.registration-link')) ) {
		$$('.registration-link').addEvent('click', function(e){
			e.stop();

			Cubix.RegPopup.Show(636, 993);
		});
	}

	if ( $defined($$('.forgot-pass-btn')) ) {
		$$('.forgot-pass-btn').addEvent('click', function(e){
			e.stop();

			Cubix.PassPopup.Show(523, 366);
		});
	}

	if ( $defined($$('.contact-link')) ) {
		$$('.contact-link').addEvent('click', function(e){
			e.stop();

			Cubix.ContactPopup.Show(517, 767);
		});
	}

	if ( $defined($$('.login-btn')[0]) ) {
		$$('.login-btn')[0].addEvent('click', function(e){
			e.stop();

			this.getParent('form').submit();
		});
	}

	var username = $$('.signin-box input[name=username]')[0];
	var password = $$('.signin-box input[name=password]')[0];

	if ( username && password ) {

		username.addEvent('focus', function(){
			if ( this.get('value') == headerVars.tUsername ) {
				this.set('value', '');
			}			
		});
		username.addEvent('blur', function(){
			if ( ! this.get('value').length ) {
				this.set('value', headerVars.tUsername);
			}
		});		

		var initPassword = function(pass) {
			pass.addEvent('focus', function() {
				this.destroy();
				injectNewPassword();
			});
		};

		var injectNewPassword = function() {
			var pass = new Element('input', {
				name: 'password',
				type: 'password',
				'class': 'txt-input fleft mr16 w193 password',
				value: ''
			}).inject(username, 'after');

			pass.addEvent('blur', function() {
				if ( ! this.get('value').length ) {
					this.destroy();
					var pass = new Element('input', {
						name: 'password',
						type: 'text',
						'class': 'txt-input fleft mr16 w193 password',
						value: headerVars.tPassword
					});
					pass.inject(username, 'after');
					initPassword(pass);
				}
			});

			pass.focus();
			pass.focus();
			pass.focus();
		};

		initPassword(password);
	}
});