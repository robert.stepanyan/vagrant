/* --> Escorts */
Cubix.Escorts = {};

Cubix.Escorts.url = '/escorts'; // Must be set from php
Cubix.Escorts.city_search_title = ''; // Must be set from php
Cubix.Escorts.citySearchVars = {citiesData:[], linkTypeCity:'', linkTypeRegion:''}; // Must be set from php

Cubix.Escorts.GetRequestUrl = function (url) {
	var uri = new URI(document.location.href);
	// uri = uri.get('directory') + uri.get('file');
	uri = Cubix.Escorts.url;
	uri = uri.replace(/\/$/, '');

	url = uri + '/' + url;

	if ( -1 == url.indexOf('?') ) {
		url += '?';
	}
	else {
		url += '&';
	}

	url += 'ajax';

	if ( url == '/escorts/?ajax' ) url = '?ajax'; //TO AVOID REDIRECTS IN FILTERING

	return url;
};

Cubix.Escorts.s_page = 1;
Cubix.Escorts.no_escorts = false;

Cubix.Escorts.ss;

Cubix.Escorts.scrollSpyLoadInit = function() {
	var finish = false;

	var req = function(){
		//console.log(Cubix.Escorts.s_page);
		Cubix.Filter.Set({ page: [Cubix.Escorts.s_page], segmented: ['1'] });

		finish = true;
	};

	Cubix.Escorts.ss = new ScrollSpy({
		min: window.getScrollSize().y - window.getSize().y - 250,
		onEnter: function(position,state,enters) {
			if ( ! Cubix.Escorts.no_escorts ) {
				req(++Cubix.Escorts.s_page);

				if ( $('escorts') )
					container = $('escorts');
				else if ( $('new-escorts') )
					container = $('new-escorts');

				getSpinner().inject(container, 'bottom');
			}
		},
		onLeave: function(position,state,leaves) {

		},
		container: window
	});

	var getSpinner = function () {
		if ( ! $$('div.spinner').length  ) {
			var el = new Element('div', { 'class': 'spinner' });
			new Element('span', { html: 'Loading more escorts... '}).inject(el);

			return el;
		}
		return $$('div.spinner')[0];
	};
};

/*Cubix.Escorts.initProfilePhotos = function(id) {

	var el = $('gallery-box-' + id);

	var sdd = new noobSlide({
		box: el,
		items: el.getElements('span'),
		size: 296,
		interval: 3000,
		fxOptions: {
			duration: 450,
			transition: Fx.Transitions.Cubic.easeInOut,
			wait: false
		},
		addButtons: {
			previous: $('prev-' + id),
			//play: $('play1'),
			//stop: $('stop1'),
			next: $('next-' + id)
		}
	});
};

Cubix.Escorts.closeOpenedProfiles = function() {
	var all_opened_profiles = $$('.list-profile.opened');

	if ( $('escorts') )
		$('escorts').setStyle('padding-bottom', '50px');
	else if ( $('new-escorts') )
		$('new-escorts').setStyle('padding-bottom', '50px');



	all_opened_profiles.each(function(it){
		it.setStyle('overflow', 'hidden');
		it.set('tween', {
			duration: 400,
			transition: 'cubic:in:out',
			property: 'height',
			wait:true,
			onComplete: function() {
				it.addClass('none');
				it.removeClass('opened');
			}
		}).tween(515, 0);
	});
};

Cubix.Escorts.initProfileXButton = function(profile_cont) {
	profile_cont.getElements('a.close').addEvent('click', function(e){
		e.stop();

		if ( $('escorts') )
			$('escorts').setStyle('padding-bottom', '50px');
		else if ( $('new-escorts') )
			$('new-escorts').setStyle('padding-bottom', '50px');

		$$('.list-profile.opened').each(function(it){

			it.setStyle('overflow', 'hidden');

			it.set('tween', {
				duration: 400,
				transition: 'cubic:in:out',
				property: 'height',
				wait:true,
				onComplete: function() {
					it.addClass('none');
					it.removeClass('opened');

					Cubix.Escorts.ss.options.min = window.getScrollSize().y - window.getSize().y - 250 - 550;
				}
			}).tween(515, 0);
		});
	});
};

Cubix.Escorts.loadGallery = function(escort_id, cont) {
	new Request({
		url: '/escorts/ajax-load-gallery?e_id=' + escort_id,
		method: 'get',
		onSuccess: function (resp) {
			cont.set('html', resp);
		}.bind(this)
	}).send();
};*/

/*Cubix.Escorts.initProfiles = function() {

	$$('a.viewButton, div.escort a, div.escort-wrapper a.showname').removeEvents();
	$$('a.viewButton, div.escort a, div.escort-wrapper a.showname').addEvent('click', function(e){
		e.stop();

		var self = this;

		if ( $('escorts') )
			var escorts_cont = $('escorts');
		else if ( $('new-escorts') )
			var escorts_cont = $('new-escorts');

		//Cubix.Filter.Set({ e_id: [self.get('rel')] });

		var profile_cont = $$('.profile-wrapper-' + self.get('rel'))[0];

		profile_cont.setStyle('overflow', 'hidden');

		Cubix.Escorts.closeOpenedProfiles();

		if ( profile_cont.hasClass('opened') ) return;

		profile_cont.removeClass('none');

		Cubix.Comments.container = profile_cont.getElements('.ajax-comments-container')[0];
		Cubix.Comments.escort_id = self.get('rel');
		Cubix.Comments.LoadComments();

		Cubix.Escorts.loadGallery(self.get('rel'), profile_cont.getElements('.TGallery')[0]);

		new Fx.Tween(profile_cont, {
			duration: 400,
			transition: 'cubic:in:out',
			property: 'height',
			onComplete: function() {
				new Fx.Scroll(window, {
					duration: 400,
					wait: true,
					offset: {
						x: 0,
						y: 0 //-180
					},
					onComplete: function() {
						profile_cont.setStyle('overflow', 'visible');
						escorts_cont.setStyle('padding-bottom', '260px');
						//profile_tween.start(0, 515);
					}
				}).toElement(profile_cont);

				profile_cont.addClass('opened');

				Cubix.Escorts.initProfilePhotos(self.get('rel'));

				Slimbox.scanPage();

				Cubix.Escorts.initProfileXButton(profile_cont);
			}
		}).start(0, 515);

		var tabs = profile_cont.getElements('.tabs')[0];
		var contents = tabs.getNext('div.profile-data');

		Cubix.Escorts.initProfileTabs(tabs, contents);
	});

};

Cubix.Escorts.initProfileTabs = function(tabs, contents) {
	contents.getElements('.top-cont').addClass('none');

	tabs.getElements('ul li').each(function(it, index){
		it.addEvent('click', function(){
			contents.getElements('.top-cont').addClass('none');

			tabs.getElements('ul li.active').removeClass('active');

			contents.getElements('.' + this.get('title')).removeClass('none');
			this.addClass('active');
		});

		if ( it.hasClass('active') ) {
			contents.getElements('.' + it.get('title')).removeClass('none');

			//var vert= new ScrollerBar(contents.getElement('#' + it.get('title')));
			//console.log('dd');
		}
	});
};*/

Cubix.Escorts.initToggleFilter = function() {
	if ( $$('a.toggle-filter') ) {
		$$('a.toggle-filter').addEvent('click', function(e){
			e.stop();

			var f_container = $('filter-container');

			f_container.toggleClass('none');
		});
	}
};

Cubix.Escorts.initToolTips = function() {

	$$('.icon-comments').addEvent('mouseenter', function(){
		var self = this;
		var commentCount = self.getNext('.icon-comments-count');
		tooltip.show(commentCount.get('html') + ' ' + Cubix.CommentTip);
	});

	$$('.icon-comments').addEvent('mouseleave', function(){
		tooltip.hide();
	});

	$$('.icon-reviews').addEvent('mouseenter', function(){
		var self = this;
		var reviewCount = self.getNext('.icon-reviews-count');
		tooltip.show(reviewCount.get('html') + ' ' + Cubix.ReviewTip);
	});

	$$('.icon-reviews').addEvent('mouseleave', function(){
		tooltip.hide();
	});
};

Cubix.Escorts.Load = function (url, data, callback) {
	if ( $('escorts') )
		this.container = $('escorts');
	else if ( $('new-escorts') )
		this.container = $('new-escorts');

	var data_obj = {};
	if ( data.length ) {
		var data_obj = data.parseQueryString();

		if ( data_obj.segmented == 0 ) {
			var overlay = new Cubix.Overlay(this.container, {});
			overlay.disable();
		}
	}

	var f_overlay = new Cubix.Overlay($('filter-v2'), {loader: _st('loader-tiny.gif'), position: '90px', offset: {bottom: 0, top: 0}});
	f_overlay.disable();


	new Request({
		url: Cubix.Escorts.url + '?ajax',
		method: 'post',
		data: data,
		onSuccess: function (resp) {
			resp = JSON.decode(resp);

			if ( resp.escort_list.length > 10 ) {
				if ( String(data).test('page=') ) {

					if ( data_obj.segmented == 1 ) {
						this.container.set('html', this.container.get('html') + resp.escort_list);
					} else {
						this.container.set('html', resp.escort_list);
					}

					Cubix.Escorts.s_page = data_obj.page;
				} else {
					Cubix.Escorts.s_page = 1;
					this.container.set('html', resp.escort_list);
				}

				Cubix.Escorts.no_escorts = false;

			} else {
				if ( String(data).test('name=') ) {
					if ( String(data).test('page=') ) {
						this.container.set('html', this.container.get('html') + resp.escort_list);
					} else {
						this.container.set('html', resp.escort_list);
					}
				}

				Cubix.Escorts.no_escorts = true;
			}

			if ( data_obj.name ) {
				Cubix.Filter.search_input_search_text = data_obj.name;
			} else {
				Cubix.Filter.search_input_search_text = Cubix.Filter.search_input_text;
			}


			if (String(data).test('segmented=1')) {
				Cubix.Filter.Set({ page: [Cubix.Escorts.s_page], segmented: ['0'] });
			}

			$$('img.lazyme').each(function(it){

				var self = it;

				it.getParent('a').addClass('loading');

				var myImage = Asset.image(it.get('src'), {
					id: 'myImage',
					title: 'myImage',
					onLoad: function() {
						self.getParent('a').removeClass('loading');
						self.removeClass('lazyme');
					}
				});
			});

			// reinit profile after segment loaded
			if ( this.container.getElements('.opened')[0] ) {
				var opened_profile_cont = this.container.getElements('.opened')[0];
				var tabs = opened_profile_cont.getElements('.tabs')[0];
				var contents = tabs.getNext('div.profile-data');


				Cubix.Escorts.initProfileXButton(opened_profile_cont);
				Cubix.Escorts.initProfileTabs(tabs, contents);
				Cubix.Escorts.initProfilePhotos(opened_profile_cont.get('rel'));
				Slimbox.scanPage();

				Cubix.Comments.container = opened_profile_cont.getElements('.ajax-comments-container')[0];
				Cubix.Comments.escort_id = opened_profile_cont.get('rel');
				Cubix.Comments.LoadComments();
			}

			//Cubix.Escorts.initProfiles();

			this.container.getElements('div.spinner').destroy();

			$('filter-container').set('html', resp.filter_body);
			Cubix.Filter.filter = $('filter-container').getElement('#filter-v2');
			Cubix.Filter.InitSearchInput('search');
			Cubix.Filter.Init();
			Cubix.Filter.InitPopups();
			Cubix.HashController.init();

			Cubix.Escorts.initToolTips();

			Slimbox.scanPage();

			Cubix.Escorts.ss.options.min = window.getScrollSize().y - window.getSize().y - 250;

			if ( data_obj.segmented == 0 ) {
				overlay.enable();
			}

			f_overlay.enable();

			if ( $defined(callback) ) {
				callback.call(this, resp);
			}
		}.bind(this)
	}).send();

	return false;
}
/* <-- */

window.addEvent('domready',function() {
	Cubix.Escorts.scrollSpyLoadInit();
	Cubix.Escorts.initToggleFilter();

	//Cubix.Escorts.initProfiles();

	Cubix.Escorts.initToolTips();

	if ( $('sort-select') ) {
		$('sort-select').fancySelect({
			showText: true,
			showImages: false,
			legacyEvents: true
		});
	}

	if ($defined($('go-top'))) {
		$('go-top').addEvent('click', function(){
			var myFx = new Fx.Scroll(window,{
				offset: {
					'x': 0,
					'y': -5
				}
			});
			myFx.toElement('top-header');
		});
	}

	var page_dim = $('page').getCoordinates();

	new ScrollSpy({
		min: 1,
		max: 100000,
		onEnter: function(position,state,enters) {
			$('go-top').set('styles', {
				display: 'block',
				position: 'fixed',
				bottom: '40px',
				left: page_dim.right + 20,
				'z-index': '100'
			});
		},
		onLeave: function(position,state,leaves) {
			$('go-top').set('style', '');
		},
		container: window
	});


	var input = $('txt_city-search');
	if ( input && Cubix.Escorts.citySearchVars.citiesData.length > 0 ) {

		$('main-city-search').removeClass('none');

		new Autocompleter.Local(input, Cubix.Escorts.citySearchVars.citiesData, {
			'minLength': 1,
			postVar: 'city_search_name',
			indicatorClass: 'autocompleter-loading'
		}).addEvent('onSelection', function (element, selected, value, input) {
				element.getParent().getElement('#s_slug').set('value', selected.inputValue.slug);
				element.getParent().getElement('#s_type').set('value', selected.inputValue.type);
			});

		$('main-city-search').addEvent('submit', function(){doSearch($(this));})

		input.addEvent('focus', function(){
			if ( this.get('value') == Cubix.Escorts.city_search_title ) {
				this.set('value', '');
			}
		});

		input.addEvent('blur', function(){
			if ( ! this.get('value').length ) {
				this.set('value', Cubix.Escorts.city_search_title);
			}
		});

		var doSearch = function (form) {
			var slug = form.getElement('#s_slug').get('value');
			// If no item is selected just do nothing
			if ( ! slug.length ) return false;

			var type = form.getElement('#s_type').get('value');

			var action = '';
			if ( type == 'city' ) {
				action = Cubix.Escorts.citySearchVars.linkTypeCity + slug;
			}
			else if ( type == 'region' ) {
				action = Cubix.Escorts.citySearchVars.linkTypeRegion + slug;
			}

			form.set('action', action);
			//input.set('value', '');
			input.set('disabled', 'disabled');
			return true;
		};

		$('search-by-city').addEvent('click', function(e){
			e.stop();

			$('main-city-search').fireEvent('submit');
			$('main-city-search').submit();

		});
	}
});

