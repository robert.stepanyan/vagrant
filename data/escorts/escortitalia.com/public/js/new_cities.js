window.addEvent('domready', function() {
	if ($('show-more'))
		$('show-more').addEvent('click', function(e) {
			$$('.cities-box-big').removeClass('none');
			$('show-more').set('style', 'display: none;');
			$('show-less').set('style', 'display: block;');

			return false;
		});
	
	if ($('show-less'))
		$('show-less').addEvent('click', function(e) {
			$$('.cities-box-big').addClass('none');
			$('show-less').set('style', 'display: none;');
			$('show-more').set('style', 'display: block;');

			return false;
		});
});