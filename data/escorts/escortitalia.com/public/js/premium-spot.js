window.addEvent('domready',function() {
	var finish = false;
	var page = 1;
	var req = function(){
		Cubix.Filter.Set({ page: [page] });

		finish = true;
		/*method: 'get',
		url: Cubix.Escorts.GetRequestUrl(''),
		data: { page: 1 },
		onSuccess: function (resp) {
			ss.options.min = window.getScrollSize().y - window.getSize().y - 150;
			var target = $('escorts');
			target.getElements('.spinner').destroy();

			var rows = new Element('div', { html: resp }).getElements('.row');
			if ( rows.length == 0 ) { finish = true; }
			rows.each(function (row) {
				row.inject(target);
			});
		}*/
	};

	window.addEvent('escortsFilterChange', function () { page = 1; finish = false });

	var ss = new ScrollSpy({
		min: window.getScrollSize().y - window.getSize().y - 150,
		onScroll: function (p) {
			if ( req.running || finish ) return;
			if ( p.y > window.getScrollSize().y - window.getSize().y - 150 ) {
				if ( $('escorts').getElements('.row').length > 0 ) {
					//var hash = document.location.hash.substring(1);
					//req.options.url = Cubix.Escorts.GetRequestUrl(hash);
					//req.options.data.page = ++page;
					req(++page);
					$('escorts').getElements('.row.last').removeClass('last');
					getSpinner().inject($('escorts'));
					//req.send();
				}
			}
		},
		container: window
	});

	var getSpinner = function () {
		var el = new Element('div', { 'class': 'spinner' });
		new Element('span', { html: 'Loading more escorts... '}).inject(el);
		return el;
	};
});
