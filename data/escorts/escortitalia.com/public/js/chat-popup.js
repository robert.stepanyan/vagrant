/* --> Feedback */
Cubix.ChatPopup = {};

Cubix.ChatPopup.inProcess = false;

Cubix.ChatPopup.url = '';

Cubix.ChatPopup.Show = function (box_height, box_width) {
	if ( Cubix.ChatPopup.inProcess ) return false;

	var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000' });
	page_overlay.disable();

	var y_offset = 30;

	var container = new Element('div', { 'class': 'ChatPopup-wrapper'}).setStyles({
		left: window.getWidth() / 2 - box_width / 2,
		top:  window.getHeight() / 2 - box_height / 2 + window.getScroll().y - y_offset+130,
		opacity: 0,
        position: 'absolute',
        'z-index': 101,
        background: '#fff'
	}).inject(document.body);
	
	Cubix.ChatPopup.inProcess = true;

	new Request({
		url: Cubix.ChatPopup.url,
		method: 'get',
		onSuccess: function (resp) {
			
			Cubix.ChatPopup.inProcess = false;
			container.set('html', resp);

			var close_btn = new Element('div', {
				html: '',
				'class': 'chat-close-btn-v2'
			}).inject(container);

			close_btn.addEvent('click', function() {
				$$('.ChatPopup-wrapper').destroy();
				page_overlay.enable();
			});

			container.tween('opacity', '1');
		}
	}).send();
		
	
	return false;
}