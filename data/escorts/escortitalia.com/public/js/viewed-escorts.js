/* --> Viewed Escorts */
Cubix.Viewed = {};

Cubix.Viewed.url = ''; // Must be set from php
Cubix.Viewed.container = '';

window.addEvent('domready', function() {
	if ( Cubix.Viewed.url.length ) {
		new Request({
			url: Cubix.Viewed.url,
			method: 'get',
			onSuccess: function (resp) {
				if(resp.contains('rapper'))
				{
					new Element('div', {'class':'sep'}).inject($(Cubix.Viewed.container), 'before');
					$(Cubix.Viewed.container).setStyle('opacity', 0);
					var myFx = new Fx.Tween($(Cubix.Viewed.container), {
						duration: 400,
						onComplete: function() {
							$(Cubix.Viewed.container).set('html', resp);
							$(Cubix.Viewed.container).tween('opacity', 0, 1);
						}
					});
					myFx.start('height', '137');
				}
			}
		}).send();
	}
});

function addRemoveFav(id,link){
	var favImg = $$(' #last_viewed .add-fav-latest[rel=' + id + ']').getElement('img');
	var action = favImg.get('alt');
	if (action == 'signin'){
		Cubix.Popup.Show('500', '520');
	}
	else{
		favImg.set('src','/img/fav-loader.gif');
		if (action == 'add'){
			var favIcon = 'remove';
		}
		else{
			var favIcon = 'add';
		}
		new Request({
			url: link,
			method: 'post',
			onSuccess: function (resp) {
				favImg.set('alt',favIcon);
				favImg.set('src','/img/'+ favIcon +'_fav_star.png');
				}
		}).send();
	}

	return false;
}
/* <-- */
