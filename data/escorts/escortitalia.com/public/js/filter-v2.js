/* --> Filter */
Cubix.Filter = {};
Cubix.Filter.url = '';
Cubix.Filter.params;
Cubix.Filter.filter;
Cubix.Filter.selected;
Cubix.Filter.btn_text;
Cubix.Filter.search_input_text = '';
Cubix.Filter.search_input_search_text = '';

window.addEvent('domready', function(){
	Cubix.Filter.getForm();
	Cubix.Filter.InitSearchInput('search');
});

Cubix.Filter.getForm = function(data){
	new Request({
		url:	Cubix.Filter.url,
		method: 'GET',
		data: data,
		onSuccess: function(resp){
			var filterBox = $('filter-box');

			
			if (filterBox){
				if ( ! $('filter-container') ) {
					var fContainer = new Element('div', {id: 'filter-container'});
				} else {
					var fContainer = $('filter-container');
				}

				fContainer.set('html', resp);

				fContainer.inject(filterBox, 'bottom');

				fContainer.addClass('none');

				Cubix.Filter.filter = filterBox.getElement('#filter-v2');
				Cubix.Filter.Init();
				Cubix.Filter.InitPopups();

				Cubix.HashController.init();

				new Drag.Move($('filter-container'), {

					//container: window,

					//droppables: $$('.drop'),

					onEnter: function(element, droppable){
						//droppable.setStyle('background', '#E79D35');
					},

					onLeave: function(element, droppable){
						//droppable.setStyle('background', '#6B7B95');
					},

					onDrop: function(element, droppable){
						Cookie.write('filter_coordinates', JSON.encode(fContainer.getCoordinates($('filter-box'))), {domain: 'escortitalia.com.dev', duration: 365});
					}

				});

				///var filterV2 = fContainer.getElement('#filter-v2');
				
				//if ( Cookie.read('showFilter') ) {
				
					/*filterV2.setStyle('opacity', 0);

					var myFx = new Fx.Tween(filterV2, {
						duration: 400,
						onComplete: function() {
							filterV2.tween('opacity', 0, 1);

							Cubix.Filter.filter = fContainer.getElement('#filter-v2');
							Cubix.Filter.Init();
							Cubix.Filter.InitPopups();

							Cubix.HashController.init();
						}
					});

					myFx.start('height', '170');

					filterBox.grab(fContainer, 'after');*/
				/*} else {

					filterBox.grab(fContainer, 'after');
					
					filterV2.setStyle('height', 0);
					filterV2.setStyle('display', 'none');

					Cubix.Filter.filter = fContainer.getElement('#filter-v2');
					
					Cubix.Filter.Init();
					Cubix.Filter.InitPopups();

					Cubix.HashController.init();
				}*/
			}			
		}
	}).send();
};

Cubix.Filter.InitSearchInput = function (input) {
	if ($(input))
	{
		Cubix.Filter.InitSearchInput.input = $(input);
	
		this.timer = null;


		Cubix.Filter.InitSearchInput.input.addEvent('focus', function(){
			if ( this.get('value') == Cubix.Filter.search_input_text ) {
				this.set('value', '');
				//this.removeClass('def-text');
			}	
		});
		Cubix.Filter.InitSearchInput.input.addEvent('blur', function(){
			if ( ! this.get('value').length ) {
				this.set('value', Cubix.Filter.search_input_text);
				//this.addClass('def-text');
			}
		});

		Cubix.Filter.InitSearchInput.input.addEvents({
			keyup: function () {
				$clear(this.timer);
				this.timer = setTimeout('Cubix.Filter.InitSearchInput.KeyUp(Cubix.Filter.InitSearchInput.input)', 500);
			}.bind(this)
		});
	}
}

Cubix.Filter.InitSearchInput.KeyUp = function (input) {
	
	
	var value = ( input.get('value').length && input.get('value') != Cubix.Filter.search_input_text ) ? input.get('value') : null;
	
	//if ( value ) {
		$$('input[name=name]')[0].set('value', value);
		Cubix.LocationHash.Set(Cubix.LocationHash.Make());
	//}
}

Cubix.Filter.Init = function() {
	
	if ( Cubix.Filter.InitSearchInput.input && Cubix.Filter.search_input_search_text && Cubix.Filter.search_input_search_text.length ) {
		Cubix.Filter.InitSearchInput.input.set('value', Cubix.Filter.search_input_search_text);
		//Cubix.Filter.InitSearchInput.input.removeClass('def-text');
	}

	var filter_coordinates = Cookie.read('filter_coordinates');
	var filter_container = Cubix.Filter.filter.getParent('#filter-container');

	if ( filter_coordinates ) {
		filter_coordinates = JSON.decode(filter_coordinates);
		filter_container.setStyles({
			'top':filter_coordinates.top,
			'left':filter_coordinates.left
		});
	}

	var has_class_none = false;
	if ( filter_container.hasClass('none') ) {
		has_class_none = true;
	}

	if ( has_class_none ) {
		filter_container.setStyle('height', '0px');
		filter_container.removeClass('none');
	}

	var filter_width = 0;
	Cubix.Filter.filter.getElements('div.filter').each(function(el){
		filter_width += (el.getSize().x);
	});

	filter_container.setStyle('width', filter_width + 2);

	filter_container.setStyle('height', '215px');

	if ( has_class_none ) {
		filter_container.addClass('none');
	}

	Cubix.Filter.filter.getElements('div.filter input').each(function(input) {
		
		if ( ! input.getParent('div.sorting-div') ) {
			input.addEvent('click', function() {
				
				if ( input.checked ) {
					input.getNext('span').removeClass('grey');
					Cubix.Filter.filter.getElements('.' + input.get('class')).set('checked', 'checked');
				} else {
					input.getNext('span').addClass('grey');
					Cubix.Filter.filter.getElements('.' + input.get('class')).set('checked', '');
				}

				Cubix.Filter.ForceGray(input);
				Cubix.LocationHash.Set(Cubix.LocationHash.Make());
			});
		}
	});
	
	Cubix.Filter.InitReset();
	
	Cubix.Filter.InitSingleCheckbox();
	Cubix.Filter.InitHideFilters();
	
	Cubix.Filter.InitSorting();
	
	/*if ( ! Cookie.read('showFilter') ) {
		Cubix.Filter.HideFilters();
	}*/
};

/*Cubix.Filter.InitSorting = function() {
	return;
	var sorting_bar = $('sorting-box');
	var sort_inputs = sorting_bar.getElements('input');


	sort_inputs.addEvent('change', function() {

		var els = this.getParent('div.list').getElements('input:checked');
		var self = this;

		var sorting_popup = $$('.f_sorting')[0];

		els.each(function(it) {
			if( it.get('class') != self.get('class') )
				it.set('checked', '');
		});

		sorting_popup.getElements('input').each(function(it){
			it.set('checked', '');
		})

		if ( this.checked ) {
			Cubix.Filter.filter.getElements('.' + this.get('class')).set('checked', 'checked');
		}

		Cubix.LocationHash.Set(Cubix.LocationHash.Make());
	});
};*/

Cubix.Filter.InitSorting = function() {

	var sorting_bar = $('sorting-box');
	var sort_select = sorting_bar.getElements('select[name=sort]');


	sort_select.addEvent('change', function() {
		Cubix.LocationHash.Set(Cubix.LocationHash.Make());
	});
};

Cubix.Filter.InitSingleCheckbox = function() {
	Cubix.Filter.filter.getElements('input.single').addEvent('click', function() {
		
		var els = this.getParent('div.list').getElements('input:checked');
		var self = this;
		
		els.each(function(it) {
			if( it.get('class') != self.get('class') )
				it.set('checked', '');
		});
	});
};

Cubix.Filter.HideFilters = function() {
return;
	var hide_btn = $('hide-filters');
	var filter = $('filter-v2');
	
	Cubix.Filter.btn_text = hide_btn.get('html');
	hide_btn.set('html', hide_btn.getNext('span').get('html'));
	filter.setStyles({
		'overflow' : 'hidden',
		'height' : '0'
	});
	hide_btn.removeClass('opened');
	hide_btn.addClass('closed');
	
	filter.setStyles({
		'border' : '0'
	});
}

Cubix.Filter.InitHideFilters = function() {
	var filter = $('filter-v2');
	var parent = filter.getParent('#filter-container');
	var hide_btn = filter.getElements('a.close')[0];
	
	hide_btn.addEvent('click', function(e){
		e.stop();

		parent.addClass('none');
	});
};

Cubix.Filter.InitReset = function() {
	var inputs = Cubix.Filter.filter.getElement('#filters_body').getElements('input:checked');
		
	if ( inputs.length ) {
		//$('fv2_reset').getElements('span')[0].removeClass('passive').addClass('active');
		//$('fv2_reset').removeClass('res_pas').addClass('res_act');
		$('fv2_reset').removeClass('none');
	}
	
	$('fv2_reset').getElements('span').addEvent('click', function(e){
		e.stop();
		
		Cubix.LocationHash.Set('');
		$('fv2_reset').addClass('none');
	});
}

Cubix.Filter.InitPopups = function() {
	Cubix.Filter.filter.getElements('a.more').addEvent('click', function(e){
		e.stop();
		
		var popup_id = this.get('id');
		var popup = $$('.' + popup_id)[0];
		var offset = {x : 44, y: -18};
		Cubix.Filter.PopupToggle(popup, this, offset);
	});
	
	if ( Cubix.Filter.filter.getElement('#other_list') ) {
		Cubix.Filter.filter.getElement('#other_list').getElements('span').addEvent('click', function(e){
			e.stop();
			var offset = {x : 55, y: -18};
			Cubix.Filter.PopupToggle($$('.f_other')[0], this, offset);
		});
	}
	
	Cubix.Filter.filter.getElements('span.close, span.btn_cancel').addEvent('click', function(){
		this.getParent('div.filter-popup').addClass('none');
		
		this.getParent('div.filter-popup').getElements('input').each(function(it){
			if ( ! Cubix.Filter.selected.contains(it.get('class')) ) {
				it.set('checked', '');
			} else {
				it.set('checked', 'checked');
			}
		});
	});
	
	Cubix.Filter.filter.getElements('span.btn_ok').addEvent('click', function(){
		Cubix.LocationHash.Set(Cubix.LocationHash.Make());
		this.getParent('div.filter-popup').addClass('none');
	});
	
	Cubix.Filter.filter.getElements('div.f_other p.title').addEvent('click', function() {
		var v_slide = new Fx.Slide($(this.getParent('div').getElements('div.list')[0]));
		v_slide.toggle();
	});
	
	Cubix.Filter.filter.getElements('div.f_other div.collapsed').each(function(it){
		var v_slide = new Fx.Slide($(it));
		v_slide.hide();
	});
}

Cubix.Filter.PopupToggle = function(popup, el, offset) {
	
	var position = el.getPosition('filter-v2');
	
	Cubix.Filter.filter.getElements('div.filter-popup').each(function(it) {
		it.addClass('none');
	});
	
	popup.removeClass('none');
	popup.setStyles({top: position.y + offset.y, left: position.x + offset.x})
	
	if ( popup.hasClass('f_other') ) {
		popup.getElements('div.list').each(function(it){
			var inputs = it.getElements('input:checked');
			
			if ( ! inputs.length ) {			
				var v_slide = new Fx.Slide($(it));
				v_slide.hide();
			}
		});
	}
	
	var title = el.get('rel');	
	popup.getElements('div.list').each(function(it){
		if ( it.get('rel') == title ) {
			var v_slide = new Fx.Slide($(it));
			v_slide.show();
		}
	});
	
	
	Cubix.Filter.selected = [];
	popup.getElements('input:checked').each(function(it){
		Cubix.Filter.selected.append([it.get('class')]);
	})
};

Cubix.Filter.ForceGray = function(input) {
	var filter = input.getParent('div.filter');
	var is_gray = true;
	filter.getElements('input').each(function(it){
		if ( it.checked == true ) {
			is_gray = false;
		}
	});
	
	if ( is_gray ) {
		filter.getElement('span.title').addClass('grey');
		return;
	}
	
	filter.getElement('span.title').removeClass('grey');
};





Cubix.Filter.Set = function (filter) {

	if ( ! Cubix.Filter.filter ) {
		return;
	}

	Cubix.LocationHash.Set(Cubix.LocationHash.Make(filter));

	return false;
}

/* --> LocationHash */
Cubix.LocationHash = {};

Cubix.LocationHash.Set = function (hash) {
	if ( hash.length ) {
		document.location.hash = '#' + hash;
	}
	else {
		document.location.hash = '';
	}
	
	return false;
}

Cubix.LocationHash.Parse = function () {
	var hash = document.location.hash.substring(1);
	
	if ( ! hash.length ) {
		return {};
	}
	
	var params = hash.split(';');
	
	var filter = '';
	
	var not_array_params = ['page', 'sort', 'reg', 'name', 'segmented', 'e_id'];
	
	params.each(function (param) {
		var key_value = param.split('=');
		
		var key = key_value[0];
		var val = key_value[1];
		
		if ( val === undefined ) return;
		
		val = val.split(',');
				
		val.each(function(it) {
			filter += key + ( ( ! not_array_params.contains(key) ) ? '[]=' : '=' ) + it + '&';
		});
	});
	
	return filter;
}

Cubix.LocationHash.Make = function (params) {
	
	var hash = '';
	var sep = ';';
	var value_glue = ',';

	var popups = Cubix.Filter.filter.getElements('div.filter-popup');

	var sort_box = $('sorting-box');
	var sorting_select = sort_box.getElements('select[name=sort]')[0];
	var by_showname = sort_box.getElement('#search');

	var map = {};
	popups.each(function(popup) {
		var inputs = popup.getElements('input:checked');
		inputs.append(popup.getElements('input[type=hidden]'));
		inputs.each(function(input) {
			var index = input.get('name').replace('[]', '');
			if ( map[index] === undefined ) map[index] = new Array();
			map[index].append([input.get('value')]);
		});
	});

	if ( sorting_select ) {
		map = $merge(map, {sort: sorting_select.getSelected().get('value')});
	}

	if (by_showname && by_showname.get('value') != Cubix.Filter.search_input_text ) {
		map = $merge(map, {name: [by_showname.get('value')]});
	}

	map = $merge(map, params);

	for (var i in map) {
		hash += i + '=' + map[i].join(value_glue) + sep;
	}
	
	return hash;
}
/* <-- */

/* --> HashController */
Cubix.HashController = {
	_current: '',
	
	init: function () {
		setInterval('Cubix.HashController.check()', 100);
	},
	
	check: function () {
		var hash = document.location.hash.substring(1);

		hash = hash.replace(/segmented=(.+)/, "");
		hash = hash.replace(/e_id=(.+)/, "");
		this._current = this._current.replace(/segmented=(.+)/, "");
		this._current = this._current.replace(/e_id=(.+)/, "");

		if (hash != this._current) {
			this._current = hash;
			
					
			var data = Cubix.LocationHash.Parse();
			//Cubix.Filter.getForm(data);
			//console.log(data);
			Cubix.Escorts.Load(hash, data/*, Cubix.HashController.Callback*/);
		}
	}
};

Cubix.HashController.Callback = function () {
	Cubix.Filter.Change(Cubix.LocationHash.Parse());
	Cubix.Filter.Set(Cubix.LocationHash.Parse(), true);
}
/* <-- */