
$(document).ajaxStart( function() {
    $('.overlay').css('display', 'block');
}).ajaxComplete( function(){
    $('.overlay').css('display', 'none');
});

MCubix.Functions.List = {
    disable_scroll: function( overlay ){
        var self = this;

        if( overlay ){
            $('.overlay').css( 'display', 'block' );
            //$.mobile.loading( 'show' );


//            if ( window.addEventListener ) {
//                window.addEventListener( 'DOMMouseScroll', self._wheel(), false );
//            }
//
//            window.onmousewheel = document.onmousewheel = self._wheel();
//            document.onkeydown = self._keydown;
        } else {
            $('.overlay').css( 'display', 'none' );
            //$.mobile.loading( 'hide' );


//            if ( window.addEventListener ) {
//                window.addEventListener( 'DOMMouseScroll', self._wheel(), true );
//            }

            //window.onmousewheel = document.onmousewheel = self._wheel();
        }

        //document.onkeydown = self._keydown();
    },

    enable_scroll: function(){
        var self = this;

        if ( window.removeEventListener ) {
            window.removeEventListener( 'DOMMouseScroll', self._wheel(), false );
        }

        window.onmousewheel = document.onmousewheel = document.onkeydown = null;
    },

    _wheel: function(e){
        var self = this;

        self._preventDefault(e);
    },

    _preventDefault: function(e){
        e = e || window.event;

        if ( e.preventDefault ){
            e.preventDefault();
        }

        e.returnValue = false;
    },

    _keydown: function(){
        var self = this;

        for ( var i = keys.length; i--; ) {
            if ( e.keyCode === keys[i] ) {
                self._preventDefault(e);
                return;
            }
        }
    },

    hashParse: function(){
        var hash = document.location.hash.substring(1);

        if ( ! hash.length ) {
            return {};
        }

        var params = hash.split('|');

        var filter = '';

        var not_array_params = ['segmented', 'top_search_input', 'ts_slug', 'ts_type', 'page', 'agency_sort', 'sort', 'list_type', 'reg', 'name', 'showname', 'agency', 'agency_slug', 'city', 'top_name', 'city_slug', 'phone', 'ethnicity', 'language', 'hair_color', 'hair_length', 'eye_color', 'age_from', 'age_to', 'orientation', 'incall', 'outcall'];

        $.each( params, function ( _key, param ) {
            var key_value = param.split('=');

            var key = key_value[0];
            var val = key_value[1];

            if ( val === undefined ) return;

            val = val.split(',');

            $.each( val, function(_key, it) {
                filter += key + ( ( jQuery.inArray( key, not_array_params ) < 0 ) ? '[]=' : '=' ) + it + '&';
            });
        });

        return this.parseQueryString( filter + 'ajax=1' );
    },

    parseQueryString: function(url) {
        var result = {};
        var qs = url.substring(url.lastIndexOf("?"));
        qs = qs.replace('?', '');
        var pairs = qs.split('&');
        $.each(pairs, function (i, v) {
            var pair = v.split('=');
            result[pair[0]] = pair[1];
        });
        return result;
    }

};

//////////////////////////////////////////////////////////////// Cookies

MCubix.Cookies = {
    set: function( cname, cvalue, exdays, domain, path )
    {
        var d = new Date();
        d.setTime( d.getTime() + ( exdays * 24 * 60 * 60 * 1000 ) );
        var expires = "expires=" + d.toGMTString();
        var cookie_str = cname + "=" + cvalue + "; " + expires;

        if ( domain ){
            cookie_str += "; domain=" + encodeURI ( domain );
        }

        if ( path ){
            cookie_str += "; path=" + encodeURI ( path );
        }

        document.cookie = cookie_str;
    },

    get: function( cname )
    {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for( var i=0; i<ca.length; i++ )
        {
            var c = ca[i].trim();
            if ( c.indexOf( name ) == 0 ) return c.substring( name.length,c.length );
        }
        return "";
    },

    delete: function( cname ){
        document.cookie = cname + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }
};