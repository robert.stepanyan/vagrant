Cubix.ContactPopup = {};


Cubix.ContactPopup.inProcess = false;

Cubix.ContactPopup.url = '';

Cubix.ContactPopup.Show = function (box_height, box_width) {
	if ( Cubix.ContactPopup.inProcess ) return false;

	var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000', z_index: 200 });
	page_overlay.disable();

	var y_offset = 30;

	var container = new Element('div', { 'class': 'contact-popup-wrapper'}).setStyles({
		left: window.getWidth() / 2 - box_width / 2,
		top:  window.getHeight() / 2 - box_height / 2 + window.getScroll().y - y_offset,
		opacity: 0
	}).inject(document.body);

	Cubix.ContactPopup.inProcess = true;

	new Request({
		url: ((Cubix.ContactPopup.lang == 'en') ? '/en' : '') + Cubix.ContactPopup.url,
		method: 'get',
		onSuccess: function (resp) {

			Cubix.ContactPopup.inProcess = false;
			container.set('html', resp);

			var close_btn = new Element('div', {
				html: '',
				'class': 'contact-popup-close-btn-v2'
			}).inject(container);

			close_btn.addEvent('click', function() {
				$$('.contact-popup-wrapper').destroy();
				page_overlay.enable();
			});

			container.tween('opacity', '1');

			var forms = container.getElements('form');
			forms.addEvent('submit', Cubix.ContactPopup.Send);
			forms.each(function (form) { form.set('action', form.get('action') + (form.get('action').indexOf('?') != -1 ? '&' : '?') + 'ajax'); });
		}
	}).send();


	return false;
}

Cubix.ContactPopup.Send = function (e) {
	e.stop();

	var overlay = new Cubix.Overlay($$('.contact-popup-wrapper')[0], { loader: _st('loader-small.gif'), position: '50%', no_relative: true });
	overlay.disable();

	this.get('send').removeEvents('success');
	this.set('send', {
		onSuccess: function (resp) {
			resp = JSON.decode(resp);

			this.getElements('.icon').removeClass('error');

			overlay.enable();

			if ( resp.status == 'error' ) {
				for ( var field in resp.msgs ) {
					var input = this.getElement('*[name=' + field + ']');
					input.getNext('.icon').addClass('error');
				}
			}
			else if ( resp.status == 'success' ) {
				$$('.contact-popup-wrapper')[0].destroy();
				$$('.overlay').addClass('overlay-hidden');

				Cubix.ContactSuccessPopup.Show(207, 525);
			}
		}.bind(this)
	});

	this.send();
}


Cubix.ContactSuccessPopup = {};
Cubix.ContactSuccessPopup.inProcess = false;
Cubix.ContactSuccessPopup.lang;
Cubix.ContactSuccessPopup.url = '';

Cubix.ContactSuccessPopup.Show = function (box_height, box_width) {
	if ( Cubix.ContactSuccessPopup.inProcess ) return false;

	var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000', z_index: 200 });
	page_overlay.disable();

	var y_offset = 30;

	var container = new Element('div', { 'class': 'contact-success-popup-wrapper'}).setStyles({
		left: window.getWidth() / 2 - box_width / 2,
		top:  window.getHeight() / 2 - box_height / 2 + window.getScroll().y - y_offset,
		opacity: 0
	}).inject(document.body);

	Cubix.ContactSuccessPopup.inProcess = true;

	new Request({
		url: ((Cubix.ContactPopup.lang == 'en') ? '/en' : '') + Cubix.ContactPopup.url + '-success?ajax=1',
		method: 'get',
		onSuccess: function (resp) {

			Cubix.ContactSuccessPopup.inProcess = false;
			container.set('html', resp);

			var close_btn = new Element('div', {
				html: '',
				'class': 'contact-popup-close-btn-v2'
			}).inject(container);

			close_btn.addEvent('click', function() {
				$$('.contact-success-popup-wrapper').destroy();
				page_overlay.enable();
			});

			container.tween('opacity', '1');
		}
	}).send();


	return false;
}