Cubix.RegPopup = {};


Cubix.RegPopup.inProcess = false;

Cubix.RegPopup.lang;
Cubix.RegPopup.url = '';

Cubix.RegPopup.Show = function (box_height, box_width) {
	if ( Cubix.RegPopup.inProcess ) return false;

	var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000', z_index: 200 });
	page_overlay.disable();

	var y_offset = 30;

	var container = new Element('div', { 'class': 'reg-popup-wrapper'}).setStyles({
		left: window.getWidth() / 2 - box_width / 2,
		top:  window.getHeight() / 2 - box_height / 2 + window.getScroll().y - y_offset,
		opacity: 0
	}).inject(document.body);
	
	Cubix.RegPopup.inProcess = true;

	new Request({
		url: ((Cubix.RegPopup.lang == 'en') ? '/en' : '') + Cubix.RegPopup.url + '?ajax=1',
		method: 'get',
		onSuccess: function (resp) {
			
			Cubix.RegPopup.inProcess = false;
			container.set('html', resp);

			var user_types = container.getElements('input[name=user_type]');
			container.getElements('.top')[0].getElements('div').addEvent('click', function(){
				user_types.each(function(it){
					it.set('checked', '');
				});

				this.getElements('input[type=radio]').set('checked', 'checked');
			});

			//validateUsername(container);
			//validateEmail(container);

			var close_btn = new Element('div', {
				html: '',
				'class': 'reg-popup-close-btn-v2'
			}).inject(container);

			close_btn.addEvent('click', function() {
				$$('.reg-popup-wrapper').destroy();
				page_overlay.enable();
			});

			container.tween('opacity', '1');
			
			var forms = container.getElements('form');
			forms.addEvent('submit', Cubix.RegPopup.Send);
			forms.each(function (form) { form.set('action', form.get('action') + (form.get('action').indexOf('?') != -1 ? '&' : '?') + 'ajax'); });
		}
	}).send();
		
	
	return false;
}

Cubix.RegPopup.Send = function (e) {
	e.stop();

	
	var overlay = new Cubix.Overlay($$('.reg-popup-wrapper')[0], { loader: _st('loader-small.gif'), position: '50%', no_relative: true });
	overlay.disable();

	this.get('send').removeEvents('success');
	this.set('send', {
		onSuccess: function (resp) {
			resp = JSON.decode(resp);
			
			this.getElements('div.icon').removeClass('error').removeClass('esuccess');
			this.getElements('div.tip-wrapper').addClass('none');

			overlay.enable();
			
			if ( resp.status == 'error' ) {

				this.getElements('div.icon').addClass('esuccess');

				for ( var field in resp.msgs ) {

					if ( this.getElement('*[name=' + field + ']') ) {
						var input = this.getElement('*[name=' + field + ']');

						if ( input.getNext('div.tip-wrapper') ) {
							var tip_wrapper = input.getNext('div.tip-wrapper');
							if ( resp.msgs[field].length ) {
								tip_wrapper.removeClass('none');
								tip_wrapper.getChildren('div.etip').set('html', resp.msgs[field]);
								new Element('div', {
									'class':'pntik'
								}).inject(tip_wrapper.getChildren('div.etip')[0]);
							}
						}
						input.getNext('div.icon').removeClass('esuccess').addClass('error');
					}

				}
			}
			else if ( resp.status == 'success' ) {
				$$('.reg-popup-wrapper')[0].destroy();
				$$('.overlay').addClass('overlay-hidden');

				Cubix.RegSuccessPopup.Show(500, 500);
			}
		}.bind(this)
	});
	
	this.send();
}


/*var validateUsername = function(el)
{
	if (el.getElement('#username')){
		el.getElement('#username').addEvent('blur', function () {
			this.removeClass('invalid');
			this.removeClass('valid');

			if ( this.get('value').length < 6 ) {
				this.addClass('invalid');
				this.removeClass('valid');
				return;
			}

			var regex = /^[-_a-z0-9]+$/i;
			if ( ! regex.test(this.get('value')) ) {
				this.addClass('invalid');
				this.removeClass('valid');
				return;
			}

			this.addClass('loading');

			new Request({
				url: '/private/check?username=' + this.get('value'),
				method: 'get',
				onSuccess: function (resp) {
					var resp = JSON.decode(resp);
					this.removeClass('loading');
					if ( resp.status == 'found' ) {
						this.addClass('invalid');
						this.removeClass('valid');
					}
					else if ( resp.status == 'not found' ) {
						this.addClass('valid');
						this.removeClass('invalid');
					}
				}.bind(this)
			}).send();
		});
	}
}

var validateEmail = function(el)
{
	if (el.getElement('#email')){
		el.getElement('#email').addEvent('blur', function ()
		{
			this.removeClass('invalid');
			this.removeClass('valid');

			var regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
			if ( ! this.get('value').length || ! regex.test(this.get('value') ) ) {
				this.addClass('invalid');
				this.removeClass('valid');
				return;
			}

			this.addClass('loading');

			new Request({
				url: '/private/check?email=' + this.get('value'),
				method: 'get',
				onSuccess: function (resp) {
					var resp = JSON.decode(resp);

					this.removeClass('loading');

					if ( resp.status == 'found' ) {
						this.addClass('invalid');
						this.removeClass('valid');
					}
					else if ( resp.status == 'not found' ) {
						this.removeClass('invalid');
						this.addClass('valid');
					}
					else if ( resp.status == 'domain blacklisted' ) {
						this.addClass('invalid');
						this.removeClass('valid');
					}
				}.bind(this)
			}).send();
		});
	}
}*/


Cubix.RegSuccessPopup = {};
Cubix.RegSuccessPopup.inProcess = false;
Cubix.RegSuccessPopup.lang;
Cubix.RegSuccessPopup.url = '';

Cubix.RegSuccessPopup.Show = function (box_height, box_width) {
	if ( Cubix.RegSuccessPopup.inProcess ) return false;

	var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000', z_index: 200 });
	page_overlay.disable();

	var y_offset = 30;

	var container = new Element('div', { 'class': 'reg-success-popup-wrapper'}).setStyles({
		left: window.getWidth() / 2 - box_width / 2,
		top:  window.getHeight() / 2 - box_height / 2 + window.getScroll().y - y_offset,
		opacity: 0
	}).inject(document.body);

	Cubix.RegSuccessPopup.inProcess = true;

	new Request({
		url: ((Cubix.RegSuccessPopup.lang == 'en') ? '/en' : '') + Cubix.RegSuccessPopup.url + '?ajax=1',
		method: 'get',
		onSuccess: function (resp) {

			Cubix.RegSuccessPopup.inProcess = false;
			container.set('html', resp);

			var close_btn = new Element('div', {
				html: '',
				'class': 'reg-popup-close-btn-v2'
			}).inject(container);

			close_btn.addEvent('click', function() {
				$$('.reg-success-popup-wrapper').destroy();
				page_overlay.enable();
			});

			container.tween('opacity', '1');
		}
	}).send();


	return false;
}