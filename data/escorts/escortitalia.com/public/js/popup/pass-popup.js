Cubix.PassPopup = {};


Cubix.PassPopup.inProcess = false;

Cubix.PassPopup.url = '';

Cubix.PassPopup.Show = function (box_height, box_width) {
	if ( Cubix.PassPopup.inProcess ) return false;

	var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000', z_index: 200 });
	page_overlay.disable();

	var y_offset = 30;

	var container = new Element('div', { 'class': 'pass-popup-wrapper'}).setStyles({
		left: window.getWidth() / 2 - box_width / 2,
		top:  window.getHeight() / 2 - box_height / 2 + window.getScroll().y - y_offset,
		opacity: 0
	}).inject(document.body);
	
	Cubix.PassPopup.inProcess = true;

	new Request({
		url: ((Cubix.PassPopup.lang == 'en') ? '/en' : '') + Cubix.PassPopup.url,
		method: 'get',
		onSuccess: function (resp) {
			
			Cubix.PassPopup.inProcess = false;
			container.set('html', resp);

			var close_btn = new Element('div', {
				html: '',
				'class': 'pass-popup-close-btn-v2'
			}).inject(container);

			close_btn.addEvent('click', function() {
				$$('.pass-popup-wrapper').destroy();
				page_overlay.enable();
			});

			container.tween('opacity', '1');
			
			var forms = container.getElements('form');
			forms.addEvent('submit', Cubix.PassPopup.Send);
			forms.each(function (form) { form.set('action', form.get('action') + (form.get('action').indexOf('?') != -1 ? '&' : '?') + 'ajax'); });
		}
	}).send();
		
	
	return false;
}

Cubix.PassPopup.Send = function (e) {
	e.stop();
	
	var overlay = new Cubix.Overlay($$('.pass-popup-wrapper')[0], { loader: _st('loader-small.gif'), position: '50%', no_relative: true });
	overlay.disable();

	this.get('send').removeEvents('success');
	this.set('send', {
		onSuccess: function (resp) {
			resp = JSON.decode(resp);
			
			this.getElements('p.err').set('html', '');

			overlay.enable();
			
			if ( resp.status == 'error' ) {
				for ( var field in resp.msgs ) {
					var input = this.getElement('*[name=' + field + ']');
					input.getNext('p.err').set('html', resp.msgs[field]);
				}
			}
			else if ( resp.status == 'success' ) {
				$$('.pass-popup-wrapper')[0].destroy();
				$$('.overlay').addClass('overlay-hidden');

				Cubix.PassSuccessPopup.Show(217, 524);
			}
		}.bind(this)
	});
	
	this.send();
}


Cubix.PassSuccessPopup = {};
Cubix.PassSuccessPopup.inProcess = false;
Cubix.PassSuccessPopup.lang;
Cubix.PassSuccessPopup.url = '';

Cubix.PassSuccessPopup.Show = function (box_height, box_width) {
	if ( Cubix.PassSuccessPopup.inProcess ) return false;

	var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000', z_index: 200 });
	page_overlay.disable();

	var y_offset = 30;

	var container = new Element('div', { 'class': 'pass-success-popup-wrapper'}).setStyles({
		left: window.getWidth() / 2 - box_width / 2,
		top:  window.getHeight() / 2 - box_height / 2 + window.getScroll().y - y_offset,
		opacity: 0
	}).inject(document.body);

	Cubix.PassSuccessPopup.inProcess = true;

	new Request({
		url: ((Cubix.PassPopup.lang == 'en') ? '/en' : '') + Cubix.PassPopup.url + '-success?ajax=1',
		method: 'get',
		onSuccess: function (resp) {

			Cubix.PassSuccessPopup.inProcess = false;
			container.set('html', resp);

			var close_btn = new Element('div', {
				html: '',
				'class': 'pass-popup-close-btn-v2'
			}).inject(container);

			close_btn.addEvent('click', function() {
				$$('.pass-success-popup-wrapper').destroy();
				page_overlay.enable();
			});

			container.tween('opacity', '1');
		}
	}).send();


	return false;
}