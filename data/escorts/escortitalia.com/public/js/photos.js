/* --> Photos */
Cubix.Photos = {};

Cubix.Photos.url = ''; // Must be set from php
Cubix.Photos.container = '';

Cubix.Photos.Load = function (lang, page, escort_id) {
	if ( lang == 'fr' ) lang = false;
	var url = (lang ? '/' + lang : '') + '/escorts/photos?photo_page=' + page + '&escort_id=' + escort_id + '&mode=ajax';
	
	Cubix.Photos.Show(url);
	
	return false;
}

var GoPremiumPopup = {
	//imgUrl: '/img/v2.1/fr_img_popup-vip-member.png',
	//anchorUrl: '/private/signup-vip-member?type=vip-member',
	rendered: false,
	popup: null,
	anchor: null,
	overlay: null,
	close: null,

	render: function (target) {
		this.overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000', z_index: 200 });
		this.popup = new Element('div', { 'class': 'go-premium-popup' }).hide().inject($(document.body));
		this.anchor = new Element('a', { href: this.anchorUrl }).inject(this.popup);
		this.close = new Element('span', { 'class': 'close', events: { click: this.hide.bind(this) } }).inject(this.popup);
		var image = new Asset.image(this.imgUrl, {
			onload: function () {
				image.inject(this.anchor);
				this.show(target);
			}.bind(this)
		});

		this.rendered = true;
	},

	show: function (target) {
		this.overlay.disable();

		var tco = $(target).getCoordinates($(document.body));
		this.popup.setStyles({
			top: tco.top + tco.height / 2 - 217,
			left: tco.right
		});

		this.popup.show();

		var co = this.popup.getCoordinates();
		this.close.setStyles({
			top: 5,
			right: 23
		});
	},

	hide: function () {
		this.popup.hide();
		this.overlay.enable();
	},

	open: function (target) {
		if ( ! this.rendered ) this.render(target);
		else this.show(target);
	}
};

Cubix.Photos.Show = function (url) {
	var overlay = new Cubix.Overlay($(Cubix.Photos.container), {
		loader: _st('loader-small.gif'),
		position: '50%'
	});
	
	overlay.disable();
	
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {			
			$(Cubix.Photos.container).set('html', resp);
			overlay.enable();

			$$('.for-premium').addEvent('click', function(e){
				e.stop();
				GoPremiumPopup.open(this);
			});
		}
	}).send();
}
/* <-- */

window.addEvent('domready', function(){
	$$('.for-premium').addEvent('click', function(e){
		e.stop();
		GoPremiumPopup.open(this);
	});
});