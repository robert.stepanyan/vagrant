Cubix.Comments = {};

Cubix.Comments.lng = '';
Cubix.Comments.escort_id = '';
Cubix.Comments.is_member = 'false';
Cubix.Comments.container;

Cubix.Comments.InitComments = function () {
	Cubix.Comments.InitAddForm();
}

Cubix.Comments.InitAddForm = function () {
	var add_comments_btn = Cubix.Comments.container.getElements('.add-comment');
	add_comments_btn.removeEvents('click');

	add_comments_btn.addEvent('click', function (e) {
		e.stop();

		/*if ( Cubix.Comments.is_member == 'true' && Cubix.Comments.isConfirmedMember == 'false' ) {
			Cubix.InfoPopup.Show();
			return;
		}*/

		if ( Cubix.Comments.is_member == 'true' || Cubix.Comments.is_escort == 'true' )
		{
			Cubix.Comments.container.getElements('.add-comment-form')[0].set('html', '');
			Cubix.Comments.container.getElements('.comment_added')[0].setStyle('display', 'none');

			Cubix.Comments.container.getElements('.reply_place').each(function(it){
				it.set('html', '');
			});

			var overlay = new Cubix.Overlay(Cubix.Comments.container.getElements('.comments_container')[0], {
				loader: _st('loader-small.gif'),
				position: '50%'
			});

			//overlay.disable();

			var params = {};

			var add_comment_url = '/' + Cubix.Comments.lng + '/comments-v2/ajax-add-comment';

			if ( Cubix.Comments.lng == 'it' ) add_comment_url = '/comments-v2/ajax-add-comment';

			if ( this.get('rel') ) {
				new Request({
					url: add_comment_url,
					method: 'get',
					evalScripts: true,
					data: $merge({ escort_id: Cubix.Comments.escort_id, comment_id: this.get('rel') }, params || {}),
					onSuccess: function (resp) {
						var el = this.getParent('span.commenter').getNext('span.comment_body').getElement('div.reply_place');
						var height = new Element('div', { html: resp }).getHiddenHeight(el);
						el.setStyles({ height: 0, overflow: 'hidden' }).set('tween', { duration: 500, onComplete: function () { this.element.setStyle('height', null) } }).set('html', resp).tween('height', height);

						Cubix.Comments.InitFormSubmit();
						Cubix.Comments.CloseAddForm();

						overlay.enable();

					}.bind(this)
				}).send();
			}
			else {
				new Request({
					url: add_comment_url,
					method: 'get',
					evalScripts: true,
					data: $merge({ escort_id: Cubix.Comments.escort_id }, params || {}),
					onSuccess: function (resp) {
                        
                        
						var height = new Element('div', { html: resp }).getHiddenHeight(Cubix.Comments.container.getElements('.add-comment-form')[0]);
						Cubix.Comments.container.getElements('.add-comment-form')[0].setStyles({ height: 0, overflow: 'hidden' }).set('html', resp).set('tween', { duration: 500, onComplete: function () { this.element.setStyle('height', null) } }).tween('height', height);

						Cubix.Comments.InitFormSubmit();
						Cubix.Comments.CloseAddForm();

						overlay.enable();

					}.bind(this)
				}).send();
			}
		}
		else {
			Cubix.Popup.Show('489', '652');
		}

	});
};

Cubix.Comments.InitFormSubmit = function () {

	Cubix.Comments.container.getElements('.btn-save').addEvent('click', function(e) {
		e.stop();

		var overlay = new Cubix.Overlay(Cubix.Comments.container.getElements('.myprofile')[0], {
			loader: _st('loader-small.gif'),
			position: '50%'
		});

		//overlay.disable();

		var add_comment_url = '/' + Cubix.Comments.lng + '/comments-v2/ajax-add-comment';

		if ( Cubix.Comments.lng == 'it' ) add_comment_url = '/comments-v2/ajax-add-comment';

		new Request({
			url: add_comment_url,
			method: 'post',
			evalScripts: true,
			data: this.getParent('form'),
			onSuccess: function (resp) {
				if ( Cubix.Comments.isJSON(resp) ) {
					if( JSON.decode(resp) ) {
						var data = JSON.decode(resp);

						var error_map = ['comment_to_short', 'captcha_error'];
						error_map.each(function(it){
							Cubix.Comments.container.getElement('#' + it).setStyle('display', 'none');
						});

						data.each(function(it){
							Cubix.Comments.container.getElement('#' + it).setStyle('display', 'block');
						});
					}
					overlay.enable();
				}
				else {
					
					if ( Cubix.Comments.container.getElements('.add-comment-form')[0] ) {
						Cubix.Comments.container.getElements('.add-comment-form')[0].set('html', '');
					}
					
					if ( this.getParent('div.reply_place') ) {
						this.getParent('div.reply_place').set('html', '');
					}
					Cubix.Comments.container.getElements('.comment_added')[0].setStyle('display', 'block');

//					Cubix.Comments.LoadComments({ page: 1 });
				}

			}.bind(this)
		}).send();
	});
};

Cubix.Comments.isJSON = function(str){
	  str = str.replace(/\\./g, '@').replace(/"[^"\\\n\r]*"/g, '');
	 return (/^[,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]*$/).test(str);
}

Cubix.Comments.CloseAddForm = function() {
	Cubix.Comments.container.getElements('.btn-close').addEvent('click', function(e) {
		e.stop();

		Cubix.Comments.container.getElements('.add-comment-form')[0].set('html', '');
		Cubix.Comments.container.getElements('.reply_place').each(function(it){
			it.set('html', '');
		});
	});
};

Cubix.Comments.InitVote = function() {

	Cubix.Comments.container.getElements('a.vote-up, a.vote-down').addEvent('click', function(e){
		e.stop();
		
		/*if ( Cubix.Comments.is_member == 'true' && Cubix.Comments.isConfirmedMember == 'false' ) {
			Cubix.InfoPopup.Show();
			return;
		}*/
		
		if ( Cubix.Comments.is_member == 'true' )
		{
			var overlay = new Cubix.Overlay(Cubix.Comments.container.getElements('.comments_container')[0], {
				loader: _st('loader-small.gif'),
				position: '50%'
			});

			//overlay.disable();

			var params = [];
			if ( this.get('class') == 'vote-up' ) {
				params = { type: 'vote-up' };
			}
			else {
				params = { type: 'vote-down' };
			}

			var vote_comment_url = '/' + Cubix.Comments.lng + '/comments-v2/ajax-vote';

			if ( Cubix.Comments.lng == 'it' ) vote_comment_url = '/comments-v2/ajax-vote';

			new Request({
				url: vote_comment_url,
				method: 'get',
				evalScripts: true,
				data: $merge({ escort_id: Cubix.Comments.escort_id, comment_id: this.get('rel') }, params || {}),
				onSuccess: function (resp) {
					if ( resp.length == 0 ) {
						Cubix.Comments.LoadComments({ page: 1 });
					}
					else {
						var el = this.getParent('span.commenter').getNext('span.comment_body').getElements('.reply_place')[0];
						el.set('html', resp);
						overlay.enable();
					}
					//overlay.enable();
				}.bind(this)
			}).send();
		}
		else {
			Cubix.Popup.Show('489', '652');
		}
	})
};

Cubix.Comments.LoadComments = function (params) {

	var comments_container = Cubix.Comments.container.getElements('.comments_container')[0];

	if( ! comments_container ) { return false; }
	
	var overlay = new Cubix.Overlay(comments_container, {
		loader: _st('loader-small.gif'),
		position: '50%'
	});
	
	overlay.disable();

	var tween = comments_container.get('tween');
	tween.options.duration = 300;

	var comments_url = '/' + Cubix.Comments.lng + '/comments-v2/ajax-show';

	if ( Cubix.Comments.lng == 'it' ) comments_url = '/comments-v2/ajax-show';

	new Request({
		url: comments_url,
		method: 'get',
		evalScripts: true,
		data: $merge({ escort_id: Cubix.Comments.escort_id }, params || {}),
		onSuccess: function (resp) {
			var height = new Element('div', { html: resp, styles: { width: comments_container.getWidth() } }).getHiddenHeight(comments_container),
				startHeight = comments_container.getHeight();

			tween.removeEvents('complete').addEvent('complete', function () {
				tween.removeEvents('complete').addEvent('complete', function () {
					tween.set('opacity', 0);
					
					tween.removeEvents('complete').addEvent('complete', function () {
						
					}).set('opacity', 0).start('opacity', 1);
					
					this.element.set('html', resp).setStyle('height', null);
					
					Cubix.Comments.InitComments();
					Cubix.Comments.InitVote();
					
					overlay.enable();
					
				}).set('height', startHeight).start('height', height);
			}).set('opacity', 1).start('opacity', 0);
		}
	}).send();

	return false;
};