Cubix.Escorts = {};


Cubix.Escorts.initProfilePhotos = function(id) {

	var el = $('gallery-box-' + id);

	var sdd = new noobSlide({
		box: el,
		items: el.getElements('span'),
		size: 296,
		interval: 3000,
		fxOptions: {
			duration: 450,
			transition: Fx.Transitions.Cubic.easeInOut,
			wait: false
		},
		addButtons: {
			previous: $('prev-' + id),
			//play: $('play1'),
			//stop: $('stop1'),
			next: $('next-' + id)
		}
	});
};

Cubix.Escorts.initProfiles = function() {

	$$('.show-escort-profile').removeEvents();
	$$('.show-escort-profile').addEvent('click', function(e){
		e.stop();

		var self = this;

		var profile_cont = $$('.profile-wrapper-' + self.get('rel'))[0];

		var all_opened_profiles = $$('.list-profile.opened');

		all_opened_profiles.each(function(it){
				it.set('tween', {
					duration: 400,
					transition: 'cubic:in:out',
					property: 'height',
					wait:true,
					onComplete: function() {
						it.addClass('none');
						it.removeClass('opened');
					}
				}).tween(515, 0);
		});

		if ( profile_cont.hasClass('opened') ) return;

		profile_cont.removeClass('none');

		Cubix.Comments.container = profile_cont.getElement('#Comments');
		Cubix.Comments.escort_id = self.get('rel');
		Cubix.Comments.LoadComments();

		new Fx.Tween(profile_cont, {
			duration: 400,
			transition: 'cubic:in:out',
			property: 'height',
			onComplete: function() {
				new Fx.Scroll(window, {
					duration: 400,
					wait: true,
					onComplete: function() {
						//profile_tween.start(0, 515);
					}
				}).toElement(profile_cont);

				profile_cont.addClass('opened');

				Cubix.Escorts.initProfilePhotos(self.get('rel'));

				Slimbox.scanPage();

				profile_cont.getElements('a.close').addEvent('click', function(e){
					e.stop();

					$$('.list-profile.opened').each(function(it){
						it.set('tween', {
							duration: 400,
							transition: 'cubic:in:out',
							property: 'height',
							wait:true,
							onComplete: function() {
								it.addClass('none');
								it.removeClass('opened');
							}
						}).tween(515, 0);
					});
				});
			}
		}).start(0, 515);

		var tabs = profile_cont.getElements('.tabs')[0];
		var contents = tabs.getNext('div.profile-data');

		Cubix.Escorts.initProfileTabs(tabs, contents);
	});

};

Cubix.Escorts.initProfileTabs = function(tabs, contents) {
	contents.getElements('.top-cont').addClass('none');

	tabs.getElements('ul li').each(function(it, index){
		it.addEvent('click', function(){
			contents.getElements('.top-cont').addClass('none');

			tabs.getElements('ul li.active').removeClass('active');

			contents.getElement('#' + this.get('title')).removeClass('none');
			this.addClass('active');
		});

		if ( it.hasClass('active') ) {
			contents.getElement('#' + it.get('title')).removeClass('none');

			//var vert= new ScrollerBar(contents.getElement('#' + it.get('title')));
			//console.log('dd');
		}
	});
};




window.addEvent('domready',function() {

	Cubix.Escorts.initProfiles();

	/*if ($defined($('go-top'))) {
		$('go-top').addEvent('click', function(){
			var myFx = new Fx.Scroll(window,{
				offset: {
					'x': 0,
					'y': -5
				}
			});
			myFx.toElement('top-header');
		});
	}

	var page_dim = $('page').getCoordinates();

	new ScrollSpy({
		min: 1,
		max: 100000,
		onEnter: function(position,state,enters) {
			$('go-top').set('styles', {
				display: 'block',
				position: 'fixed',
				bottom: '40px',
				left: page_dim.right + 20,
				'z-index': '100'
			});
		},
		onLeave: function(position,state,leaves) {
			$('go-top').set('style', '');
		},
		container: window
	});*/
});

