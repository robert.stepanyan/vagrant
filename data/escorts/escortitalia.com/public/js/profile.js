Cubix.resizeProfile = function() {
	if ( $$('.comments-block')[0] ) {
	
		var leftSide = $('left');
		var profile = $('profile-container');
		var comments = $$('.comments-block')[0];

		if ( ! profile.get('rel') ) {
			profile.set('rel', profile.getSize().y);
		}

		var commentsHeight = comments.setStyles({
			visibility: 'hidden',
			display: 'block'
		}).getHeight();

		comments.setStyles({
			visibility: null,
			display: null
		})	

		var profileHeight = 0;

		profileHeight = (profile.get('rel')*1) + (commentsHeight*1) - 4700;


		if ( leftSide.getSize().y > profileHeight ) {
			profile.setStyle('height', left.getSize().y);
		} else {
			profile.setStyle('height', profileHeight);
		}
	} else {
		var $leftHeight = $("left").getCoordinates().height;
		var $profileHeight = $("profile-container").getCoordinates().height - 4600;

		if ( $leftHeight > $profileHeight ) {
			$("profile-container").setStyle('height', $leftHeight);
		} else {
			$("profile-container").setStyle('height', $profileHeight);
		}
	}
};

Cubix.initProfileV2 = function() {

	if ( ! $$('.profile-tabs').length ) return;
	
	Cubix.resizeProfile();

	var profile = $('profile-container');

	$$('#profile-tabs li a').addEvent('click', function(e){
		e.stop();

		$$('.profile-tabs li.active').removeClass('active');

		var tab = this.get('href');

		var el = $$("#right a[rel="+ tab.replace("#", "") +"]")[0];
		
		this.getParent('li').addClass('active');

		if(el) {

	        var scroll = new Fx.Scroll(
	             $("profile-container"),
	            {wait: false, duration: 700, transition: Fx.Transitions.Quad.easeInOut}
	        );
	        scroll.toElement(el);
	    }	
	});

	if ( profile.getElements('.ajax-comments-container').length ) {
		Cubix.Comments.container = profile.getElements('.ajax-comments-container')[0];
		Cubix.Comments.escort_id = profile.getElements('.comments-block')[0].get('e-id');
		Cubix.Comments.LoadComments();
	}

    var elm = $('profile-stat-info');
    if ( elm ) {

        var overlay = new Cubix.Overlay( elm, {
            loader: _st('loader-circular-tiny.gif'),
            position: '50%',
            offset: {
                left: 0,
                top: -1
            }
        });

        overlay.disable();

        var link = '';
        if ( profileVars.lang != profileVars.defLang ) {
            link = "&lang_id=" + profileVars.lang ;
        }

        new Request({
            url: '/index/sedcard-total-views?escort_id=' + profileVars.escortId + link,
            method: 'get',
            onSuccess: function (resp) {

                elm.set('html', resp);
                overlay.enable();
                if ( resp.length == 0 ){
                    elm.setStyle('display','none');
                }
            }
        }).send();
    }
};


window.addEvent('domready', function(){
	Cubix.initProfileV2();
	
	if ($defined($('go-top'))) {
		$('go-top').addEvent('click', function(){
			var myFx = new Fx.Scroll(window,{
				offset: {
					'x': 0,
					'y': -5
				}
			});
			myFx.toElement('top-header');
		});
	}

	var page_dim = $('page').getCoordinates();

	new ScrollSpy({
		min: 1,
		max: 100000,
		onEnter: function(position,state,enters) {
			$('go-top').set('styles', {
				display: 'block',
				position: 'fixed',
				bottom: '40px',
				left: page_dim.right + 20,
				'z-index': '100'
			});
		},
		onLeave: function(position,state,leaves) {
			$('go-top').set('style', '');
		},
		container: window
	});
});