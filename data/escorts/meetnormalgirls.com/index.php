<?php

define('URL_FORM_HTML', 'http://api.singlesnet.com/api/1.2/signup/core?key=4f9c8e8fb1850fda3e5d03e608cf5a25&format=xhtml');
define('URL_FORM_POST', 'http://api.singlesnet.com/api/1.2/signup/submit/core?key=4f9c8e8fb1850fda3e5d03e608cf5a25&format=xml&campaign_id=6500');



$core_key = null;
$message = null;
$errors = array();

if ("POST" == $_SERVER['REQUEST_METHOD']) {

	// post data to api
	$ch = curl_init (URL_FORM_POST);
	curl_setopt ($ch, CURLOPT_POST, true);
	curl_setopt ($ch, CURLOPT_POSTFIELDS, $_POST);
	curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec ($ch);
	
	// parse api reply
	$p = xml_parser_create();
	xml_parse_into_struct($p, $result, $vals, $index);
	xml_parser_free($p);
	
	foreach ($vals as $val) {
		if ('SUCCESS' == $val['tag']) {
			$message = "Thank you for filling the form.";
		}
	
		if ('FAILURE' == $val['tag'] && 'open' == $val['type']) {
			$core_key = $val['attributes']['CORE_KEY'];
		}
		
		if ('MESSAGE' == $val['tag']) {
			$errors[] = $val['value'];
		}
	}
}



$html_form = file_get_contents(URL_FORM_HTML . (($core_key)? "&core_key=$core_key" : ""));



?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Meet Up With Hot Girls!</title>
	<link rel="stylesheet" type="text/css" href="/css/reset.css" />
	<link rel="stylesheet" type="text/css" href="/css/style.css" />
</head>
<body>
	<div id="header">
		<div class="wrapper">
			<h1>Meet Up With Hot Girls</h1>
		</div>
	</div>

	<div id="page">
		<div class="wrapper">
			<div id="content"><div id="content-in1"><div id="content-in2">
			
			<?php if ($message) { ?>
			
				<p><?php echo $message?></p>
			
			<?php } else { ?>

				<?php if (0 < count($errors)) { ?>

					<ul class="errors">
						<li><?php echo implode("</li><li>", $errors)?></li>
					</ul>
	
				<?php } ?>
			
				<form action="" method="post">
					<?php echo $html_form?>
					<p><input type="submit" value="Join Now!" /></p>
				</form>

			<?php } ?>

			</div></div></div>
		</div>
	</div>

	<div id="footer">
		<div class="wrapper">
			<p><small>Disclaimer: All members and persons appearing on this site are 18 years of age or older.</small></p>
		</div>
	</div>
</body>
</html>
