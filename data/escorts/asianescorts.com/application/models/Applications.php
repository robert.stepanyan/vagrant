<?php

class Model_Applications
{
	public static function getAll()
	{
		$db = Zend_Registry::get('db');
		
		return $db->query('
			SELECT a.id, a.host, c.iso AS country_iso, c.' . Cubix_I18n::getTblField('title') . ' AS country_title
			FROM applications a
			INNER JOIN countries c ON c.id = a.country_id
			ORDER BY c.iso DESC
		')->fetchAll();
	}

	public static function getDefaultCurrencyTitle()
	{
		$db = Zend_Registry::get('db');
		$currency = $db->fetchOne('
				SELECT currency_title
				FROM applications
				WHERE id = '.Cubix_Application::getId()
			);
		return $currency;
	}

	public static function clearCache()
	{
		// Clear whole cache except hits count
		$cache = Zend_Registry::get('cache');

		$keys = array('v2_escort_profile_views_%s', 'v2_banners_cache_%s', 'v2_%s_banners_cache','latest_action_date_%s');
		$apps = array(
			array('ae', 11),
			array('it', 9),
		);
		$saved = array();
		foreach ( $apps as $app ) {
			list($name, $aid) = $app;
			foreach ( $keys as $key ) {
				$key = sprintf($key, $name);
				$cache->setKeyPrefix($aid);
				$value = $cache->load($key);
				if ( ! isset($saved[$aid]) ) {
					$saved[$aid] = array();
				}
				if ( $value )
					$saved[$aid][$key] = $value;
			}
		}

		$cache->clean();

		foreach ( $saved as $aid => $data ) {
			foreach ( $data as $key => $value ) {
				
				if ( $value ) {
					$cache->setKeyPrefix($aid);
					$cache->save($value, $key);
				}
			}
		}

		$cache->setKeyPrefix(false);

		$sid = 'sedcard_paging_' . Cubix_Application::getId();
		$ses = new Zend_Session_Namespace($sid);
		$ses->unsetAll();

		return 'Cache Cleared';
	}
}
