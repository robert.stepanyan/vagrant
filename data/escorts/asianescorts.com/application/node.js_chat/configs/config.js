function Config() {
	var config = {
		production : {
			common : {
				applicationId : 11,
				listenPort : 8888,
				host: 'www.asianescorts.com',
				authPath : '/get_user_info.php',
				sessionCacheTime: 1000 * 60 * 10, //10 minutes
				sessionCheckInterval : 1 * 60 * 60 * 1000, //1 hour
				imagesHost : 'https://pic.asianescorts.com/asianescorts.com/',
				avatarSize : 'lvthumb',
				noPhotoUrl : '/img/chat/img-no-avatar.gif',
				userSettingsBackupPath : 'user_settings.bk',
				key : 'JFusLsdf8A9',
				blWordsLifeTime : 12 * 60 * 60 * 1000, //12 hours
				redisBlWordsDb: 0,
				redisBlWordsKey: 'bl-words-ae'
			},
			db : {
				user:     'ae_cubix',
				database: 'asianescorts',
				password: 'AEserrGfg^^%5GF',
				host:     'www2.ae.com'
			},
			memcache : {
				host : '172.16.0.11',
				port : 11211
			}
		},
		development: {
			common : {
				applicationId : 11,
				listenPort : 8888,
				host: 'www.escortforum.net.dev',
				authPath: '/get_user_info.php',
				sessionCacheTime: 1000 * 60 * 10, //10 minutes
				sessionCheckInterval : 1 * 60 * 60 * 1000, //1 hour
				imagesHost : 'http://pic.escortforum.net/escortforum.net/',
				avatarSize : 'lvthumb',
				noPhotoUrl : '/img/chat/img-no-avatar.gif',
				userSettingsBackupPath : 'user_settings.bk',
				key : 'JFusLsdf8A9',
				blWordsLifeTime : 12 * 60 * 60 * 1000, //12 hours
				redisBlWordsDb: 0,
				redisBlWordsKey: 'bl-words-ae'
			},
			db : {
				user:     'sceon',
				database: 'escortforum.net_backend',
				password: '123456',
				host:     '192.168.0.1'
			},
			memcache : {
				host : '172.16.0.11',
				port : 11211
			}
		}
	};
	
	this.get = function(type) {
		if ( type == 'development' ) {
			return config.development;
		} else {
			return config.production;
		}
	}
}

function get(type) {
	conf = new Config();
	return conf.get(type);
}



exports.get = get;
