<?php

class Zend_View_Helper_GetDaysInBetween
{
	public function getDaysInBetween($start, $end)
	{

		$day = 86400; // Day in seconds
        $format = 'Y-m-d'; // Output format (see PHP date funciton)
        $sTime = /*strtotime(*/$start/*)*/; // Start as time
        $eTime = /*strtotime(*/$end/*)*/; // End as time
        $numDays = round(($eTime - $sTime) / $day);
        $days = array();

        // Return days
        return abs($numDays);
	}
}
