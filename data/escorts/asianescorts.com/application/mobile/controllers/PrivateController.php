<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Mobile_PrivateController extends Zend_Controller_Action{

	protected $profile;

	public function init()
	{
		$this->view->layout()->setLayout('mobile-private');

		$anonym = array('signin', 'signup', 'forgot');

		$this->user = Model_Users::getCurrent();
		if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}


		if ( in_array($this->_request->getActionName(), array('upgrade')) ) {
			return;
		}
		
	}


	public function indexAction(){
		$is_premium = false;
			if ( isset($this->user->member_data) ) {
			$this->view->is_premium = $is_premium = $this->user->member_data['is_premium'];
		}

		$nav = array();
		$nav[] = array('id' => 'profile', 'title' => __('pv2_btn_member_profile'), 'link' => 'private-member-profile');

		if ( ! $is_premium )
			$nav[] = array('id' => 'upgradepremium', 'title' => __('pv2_btn_up_to_premium'), 'link' => 'private-upgrade-premium');

		$nav[] = array('id' => 'myfavourites', 'title' => __('pv2_btn_my_favorites'), 'link' => 'favorites');
		$nav[] = array('id' => 'myalerts', 'title' => __('pv2_btn_my_alerts'), 'link' => 'alerts');
		$nav[] = array('id' => 'myreviews', 'title' => __('pv2_btn_my_reviews'), 'link' => 'private-reviews');
		$nav[] = array('id' => 'settings', 'title' => __('pv2_btn_settings'), 'link' => 'private-settings');
		$nav[] = array('id' => 'support', 'title' => 'Support', 'link' => 'private-support');
		//$nav[] = array('id' => 'faq', 'title' => 'FAQ', 'link' => 'private-v2-faq');
		$nav[] = array('id' => 'logout', 'title' => __('pv2_btn_logout'), 'link' => 'signout');

		$this->view->navigation = $nav;
		$this->view->hideBack = true;
	}

	public function signinAction()
	{
		$this->view->data = array('username' => '');
		$this->view->errors = array();
		
		if ( $this->_request->isPost() ) {

			$username = trim($this->_getParam('username'));
			$password = trim($this->_getParam('password'));


			$this->view->data['username'] = $username;

			$validator = new Cubix_Validator();

			if ( ! strlen($username) ) {
				$validator->setError('username', 'Username is required');
			}


			if ( ! strlen($password) ) {
				$validator->setError('password', 'Password is required');
			}


			if ( strlen($username) && strlen($password) ) {
				$model = new Model_Users();

				if ( ! $user = $model->getByUsernamePassword($username, $password) ) {
					$validator->setError('username', __('wrong_user_pass'));
				}

				if ( $user ) {
//					if ( STATUS_ACTIVE != $user->status ) {
//						$validator->setError('username', 'Your account is not active yet');
//					}

                    if($this->_hasStatusBit($user->escort_data['escort_status'],ESCORT_STATUS_ADMIN_DISABLED) || $this->_hasStatusBit($user->escort_data['escort_status'],ESCORT_STATUS_DELETED)){
                        $validator->setError('status_error', __('status_error'));
                    }

                    if($user->status == -1){
                        $validator->setError('verification_error', __('verification_error'));
                    }
                    
				}
				
				if ( $user->user_type != 'member' ){
					$validator->setError('username2', __('m_access_for_members'));
				}
			}

			$result = $validator->getStatus();

			


			if ( ! $validator->isValid() ) {
				$this->view->errors = $result['msgs'];
			}
			else {
				
				if ( $user->user_type == 'agency' ) {
					$_SESSION['fc_gender'] = 2;
				}
				else if ( $user->user_type == 'member' ) {
					$_SESSION['fc_gender'] = 1;
				}
				else if ( $user->user_type == 'escort' ) {
					$_SESSION['fc_gender'] = 2;
				}

				$_SESSION['fc_username'] = $username;
				$_SESSION['fc_password'] = $password;

				Zend_Session::regenerateId();
				$user->sign_hash = md5(rand(100000, 999999) * microtime(true));
				Model_Users::setCurrent($user);

				/*set client ID*/
				Model_Reviews::createCookieClientID($user->id);
				/**/

				Model_Hooks::preUserSignIn($user->id);

				$this->_response->setRedirect($this->view->getLink('private'));
			}
		}
		
	}


	public function upgradeAction()
	{
		
		$sess = new Zend_Session_Namespace('private');

		if ( $this->user && ! $this->user->isMember() ) {
			header('Location: /');
			die;
		}

		if ( ! $this->user && ! $sess->want_premium ) {
			header('Location: /' . Cubix_I18n::getLang() . '/private/signin');
			die;
		}

		if ( $this->user ) {
			$this->view->user = $this->user;
		}
		elseif ( $sess->want_premium ) {
			$this->view->user = $this->user = $sess->want_premium;
		}

		if ( $this->_request->isPost() ) {
			$plan = (int) $this->_getParam('plan');
			switch ( $plan ) {
				case 2:
					$amount = 8995;
					break;
				default:
					$amount = 995;
			}
			$this->_redirect(Cubix_CGP::getPaymentUrl(array(
				'ref' => md5('m_' . $this->user->member_data['id']),
				'email' => $this->user->email,
				'amount' => $amount
			)));
		}
	}

	public function upgradeSuccessAction()
	{

	}

	public function upgradeFailedAction()
	{

	}


	public function settingsAction()
	{

		$this->view->data = $this->user->getData(array(
			'recieve_newsletters'
		));

		if ( $this->_request->isPost() ) {

			$validator = new Cubix_Validator();

			$form = new Cubix_Form_Data($this->_request);
			if ( $this->user->isMember() ) {

				$save_password = $this->_getParam('password');
				$save_newsletters = $this->_getParam('newsletters');

				$form->setFields(array(
					'password' => '',
					'new_password' => '',
					'new_password_2' => '',
					'recieve_newsletters' => ''
				));
			}
			$data = $form->getData();
		
			switch ( true ) {
				
				case ! is_null($save_password):
					$password = $this->_getParam('old_password');

					$new_password = $this->_getParam('new_password');
					$new_password_2 = $this->_getParam('new_password_2');

					if ( $new_password == $this->user->username) {
						$validator->setError('new_password', __('username_equal_password'));
						$this->view->usernameError = 	__('username_equal_password');
					}else{
						if ( $new_password !== $new_password_2 ) {
							$validator->setError('new_password', 'Passwords doesn\'t match');
						}
						else if ( strlen($new_password) > 0 ) {
							try {
									
								$this->user->updatePassword($password, $new_password);

							}
							catch ( Exception $e ) {

								$validator->setError('old_password', $e->getMessage());
								
							}
						}
					}

					break;
				case ! is_null($save_newsletters):
					$flag = (bool) $this->_getParam('newsletters');

					$this->user->updateRecieveNewsletters($flag);
					$this->view->data = $this->user->getData(array(
						'country_id', 'city_id', 'email', 'recieve_newsletters'
					));

					break;
			}

			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();
				$this->view->errors = $status['msgs'];
			}
		}
	}


	public function profileAction()
	{
		$client = new Cubix_Api_XmlRpc_Client();

		if ( $this->user )
		{
			$user_data = $client->call('Users.getById', array($this->user->id));
			$this->view->user_data = new Model_UserItem($user_data);
			$this->view->user = $this->user;
		}

		if ( $this->_request->isPost() )
		{
			$data = array();

			$validator = new Cubix_Validator();

			$data = $this->_validateUserProfile($validator);

			$this->view->user_data = new Model_UserItem($data);

			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];

				return;
			}

			try {
				$result = $client->call('Users.updateProfile', array($this->user->id, $data));
				//newsletter email log
				$emails = array(
					'old' => $this->user->email,
					'new' => $data['email']
				);
				Cubix_Api::getInstance()->call('addNewlstterEmailLog', array($this->user->id, $this->user->user_type, 'edit', $emails));
				//
			}
			catch (Exception $e) {
				var_dump($client->getLastResponse()); die;
			}

			if ( isset($result['error']) ) {
				print_r($result['error']); die;
			}
			else {
				if ( $result !== false ) {
					$this->view->saved = true;
					//echo nl2br(print_r($result, true));
				}
			}
		}
	}


	public function signoutAction()
	{
		Model_Users::setCurrent(NULL);
		Zend_Session::regenerateId();

		$cookie_name = 'signin_remember_' . Cubix_Application::getId();
		if ( isset($_COOKIE[$cookie_name]) && $_COOKIE[$cookie_name] ) {
			setcookie($cookie_name, null, strtotime('- 1 year'), "/");
		}

		$this->_response->setRedirect($this->view->getLink());
	}

	public function reviewsAction()
	{
		if ($this->user->user_type != 'member')
			$this->_redirect($this->view->getLink('private'));
		else
		{
			$lng = Cubix_I18n::getLang();
			$this->view->items = Cubix_Api::getInstance()->call('getReviewsForMember', array($this->user->id, $lng));
		}
	}



	public function _validateUserProfile(Cubix_Validator $validator)
	{
		$req = $this->_request;

		$email = $req->email;
		if ( ! strlen($email) ) {
			$validator->setError('email', 'Email is required !');
		}
		$data['email'] = $email;

		//Location
		$country = $req->country_id;

		if ( ! $country ) {
			$validator->setError('country_id', 'Country is required !');
		}
		$data['country_id'] = $country;


		$city = $req->city_id;
		if ( ! $city ) {
			$city = null;
		}
		$data['city_id'] = $city;

		return $data;
	}


	public function alertsAction()
	{
		if ($this->user->user_type != 'member')
			$this->_redirect($this->view->getLink('private'));
		else
		{

			$model = new Model_Members();

			if ( $this->_request->isPost() ){
				if ( $this->_getParam('save') ){
					$action = 'save';
				}

				if ( $this->_getParam('remove') ){
					$action = 'remove';
				}

				if ( $this->_getParam('addcity') ){
					$action = 'addcity';
				}
				
			
				switch ($action){
					
//						var_Dump( $this->_request->getParams() );
//						break;
					case 'remove':
					case 'addcity':
					case 'save':
					default:
						$escort_id = intval($this->_getParam('escort_id'));

						if ( $escort_id ){
							$cities = "";
							$events_array = $this->_getParam('events');
							$cities_array = $this->_getParam('cities');
							$new_city = $this->_getParam('city_id');

							$events_a = array_keys( $events_array[$escort_id] );

							$alert_city = false;
							$cities = null;

							if ( is_array($cities_array) ){
								$cities_a = array_keys( $cities_array[$escort_id] );
								
								if ( in_array(ALERT_ME_CITY, $events_a) ){
									$alert_city = true;
									if ( in_array($new_city, $cities_a) ){
										break;
									}else{
										array_push($cities_a, $new_city);
									}
									
								}

								$cities = implode(',', $cities_a);
							}elseif ($new_city){
								if ( in_array(ALERT_ME_CITY, $events_a) ){
									$alert_city = true;
									$cities = $new_city;
								}
							}

							if ( in_array(ALERT_ME_ANY_CHANGE, $events_a) ){
								if ( $alert_city ){
									$events_a = array('1,2,3,4,6');
								}else{
									$events_a = array('1,2,3,4');
								}
								
							}



							$events = implode(',', $events_a);

							if ( $action == 'remove'){
								$events = '';
							}

							$c = $model->memberAlertsSave($this->user->id, $escort_id, $events, $cities);
						
						}
						break;
				}
	
				$escort_id = intval($this->_getParam('escort_id'));
				$events = $this->_getParam('events');
				$cities = $this->_getParam('cities');

				
			}

			$items = $model->getAlerts($this->user->id);

			if ($items)
			{
				$arr = array();
				$esc_ids = array();

				foreach ($items as $item)
				{
					$arr[$item['escort_id']][] = $item['event'];

					if ($item['event'] == ALERT_ME_CITY)
						$arr[$item['escort_id']]['cities'] = $item['extra'];

					if (!in_array($item['escort_id'], $esc_ids))
						$esc_ids[] = $item['escort_id'];
				}

				$this->view->items = $arr;

				$modelE = new Model_EscortsV2();
				$this->view->escorts = $modelE->getAlertEscrots(implode(',', $esc_ids));

				/* Cities list */
				$app = Cubix_Application::getById();

				if ($app->country_id)
				{
					$modelCities = new Model_Cities();

					$cities = $modelCities->getByCountry($app->country_id);
				}
				else
				{
					$modelCountries = new Model_Countries();
					$modelCities = new Model_Cities();

					$countries = $modelCountries->getCountries();
					$this->view->countries_list = $countries;

					$c = array();

					foreach ($countries as $country)
						$c[] = $country->id;

					$cities = $modelCities->getByCountries($c);
				}

				$this->view->cities_list = $cities;
				/* End Cities list */
			}
		}
	}



	public function signupAction()
	{
	

		$session = new Zend_Session_Namespace('captcha');

		$type = $this->_getParam('type');

		$this->view->type = $type;
		$signup_i18n = $this->view->signup_i18n = (object) array(
			'username_invalid' => 'Username must be at least 6 characters long',
			'username_invalid_characters' => 'Only "a-z", "0-9", "-" and "_" are allowed',
			'username_exists' => 'Username already exists, please choose another',
			'password_invalid' => 'Password must contain at least 6 characters',
			'password2_invalid' => 'Passwords must be equal',
			'email_invalid' => 'Email is invalid, please provide valid email address',
			'email_exists' => 'Email already exists, please enter another',
			'terms_required' => 'You must agree with terms and conditions',
			'form_errors' => 'Please check errors in form',
			'terms_required' => 'You have to accept terms and conditions before continue',
			'domain_blacklisted' => 'Domain is blacklisted',
            'user_type' => 'Choose your business',
			'promotion_code' => 'Invalid Promotion Code',
			'username_equal_password' => __('username_equal_password')
		);

		if ( $this->_request->isPost() ) {
			if ($this->_getParam('phone')){
				die;
			}
			$user_type = $this->_getParam('user_type', $type);
            $user_t = $this->_getParam('user_type');

            $validator = new Cubix_Validator();

			if ( ! in_array($user_type, array('member', 'vip-member', 'escort', 'agency')) ) {
                die;
			}

            if ($user_type == 'escort' &&  ! in_array($user_t, array('member', 'vip-member', 'escort', 'agency')) ) {
                /* Update Grigor */
                $validator->setError('user_type', $signup_i18n->user_type);
                /* Update Grigor */

			}

			$data = new Cubix_Form_Data($this->_request);
			$fields = array(
				'promotion_code' => '',
				'username' => '',
				'password' => '',
				'password2' => '',
				'email' => '',
				'terms' => 'int',
				// 'captcha' => ''
			);
			$data->setFields($fields);
			$data = $data->getData();

			$data['username'] = substr($data['username'], 0, 24);
			$data['user_type'] = $user_t;
			$this->view->data = $data;



			$client = new Cubix_Api_XmlRpc_Client();
			$model = new Model_Users();

			if ( strtolower($data['password']) == strtolower($data['username'])) {
				$validator->setError('password', $signup_i18n->username_equal_password);	
			}

			if ( strlen($data['username']) < 6 ) {
				$validator->setError('username', $signup_i18n->username_invalid);
			}
			elseif ( ! preg_match('/^[-_a-z0-9]+$/i', $data['username']) ) {
				$validator->setError('username', $signup_i18n->username_invalid_characters);
			}
			elseif (stripos($data['username'], 'admin') !== FALSE) {
				$validator->setError('username', $signup_i18n->username_exists);
			}
			elseif ( $client->call('Users.getByUsername', array($data['username'])) ) {
				$validator->setError('username', $signup_i18n->username_exists);
			}

			if ( strlen($data['promotion_code']) ) {
				if ( ! in_array($data['promotion_code'], array('123456', '9988')) ) {
					$validator->setError('promotion_code', $signup_i18n->promotion_code);
				}
			}


			if ( strlen($data['password']) < 6 ) {
				$validator->setError('password', $signup_i18n->password_invalid);
			}
			elseif ( $data['password'] != $data['password2'] ) {
				$validator->setError('password2', $signup_i18n->password2_invalid);
			}

			if ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
				$validator->setError('email', $signup_i18n->email_invalid);
			}
			elseif( $client->call('Application.isDomainBlacklisted', array($data['email'])) )
			{
				$validator->setError('email', $signup_i18n->domain_blacklisted);
			}
			elseif ( $client->call('Users.getByEmail', array($data['email'])) ) {
				$validator->setError('email', $signup_i18n->email_exists);
			}

			if ( ! $data['terms'] ) {
				$validator->setError('terms', $signup_i18n->terms_required);
			}

		

			// if ( ! strlen($data['captcha']) ) {
			// 	$validator->setError('captcha', 'Captcha is required');
			// }
			// else {
			// 	$session = new Zend_Session_Namespace('captcha');
			// 	$orig_captcha = $session->captcha;


			// 	if ( strtolower($data['captcha']) != $orig_captcha ) {
			// 		$validator->setError('captcha', 'Captcha is invalid');
			// 	}
			// }

			$this->view->errors = array();
			$result = $validator->getStatus();

			if ( ! $validator->isValid() ) {
				$this->view->errors = $result['msgs'];
			}
			else {

				// Users model
				$user = new Model_UserItem(array(
					'username' => $data['username'],
					'email' => $data['email'],
					'user_type' => $user_type == 'vip-member' ? 'member' : $user_type
				));

				if (in_array($data['promotion_code'], array('123456', '9988')))
					$user->sales_user_id = 16;
				else
					Model_Hooks::preUserSignUp($user);

				$salt_hash = Cubix_Salt::generateSalt($data['email']);
				$user->salt_hash = $salt_hash;
				$user->password = Cubix_Salt::hashPassword($data['password'], $salt_hash);

				$new_user = $model->save($user);

				if ( 'escort' == $user_type ) {
					$m_escorts = new Model_Escorts();

					$escort = new Model_EscortItem(array(
						'user_id' => $user->new_user_id
					));

					// will set current app country id
					Model_Hooks::preEscortSignUp($escort);

					$escort = $m_escorts->save($escort);

					// will update escort status bits using api
					Model_Hooks::postEscortSignUp($escort);
				}
				elseif ( 'agency' == $user_type ) {
					$m_agencies = new Model_Agencies();

					$agency = new Model_AgencyItem(array(
						'user_id' => $user->new_user_id,
						'name' => ucfirst($user->username)
					));

					// will set current app country id
					Model_Hooks::preAgencySignUp($agency);

					$agency = $m_agencies->save($agency);

					//
					Model_Hooks::postAgencySignUp($agency);
				}
				elseif ( 'member' == $user_type || 'vip-member' == $user_type ) {
					unset($this->_session->want_premium);
					$m_members = new Model_Members;

					$member = new Model_MemberItem(array(
						'user_id' => $user->new_user_id,
						'email'   => $user->email
					));

					$this->view->n_member = $m_members->save($member);
				}

				//newsletter email log
				$emails = array(
					'old' => null,
					'new' => $user->email
				);
				Cubix_Api::getInstance()->call('addNewlstterEmailLog', array($user->new_user_id, $user_type, 'add', $emails));
				//

				// will send activation email
				$user->reg_type = $user_type;
				Model_Hooks::postUserSignUp($user);

				if ( ! is_null($this->_getParam('ajax')) ) {
					$result['msg'] = "
						<h1 style='margin-bottom:20px'><img src='/img/" . Cubix_I18n::getLang() . "_h1_memberssignup.gif' alt='Members signup' title='Members signup' /></h1>
						<div style='width:520px; background: none; margin-bottom:0' class='cbox-small'>
							<h3>". Cubix_I18n::translate('congratulations') . "</h3>
							<p>" . Cubix_I18n::translate('successfully_signed_up', array('SITE_URL' => Cubix_Application::getById()->url, 'SITE_NAME' => Cubix_Application::getById()->title)) . "</p>

							<h3>" . Cubix_I18n::translate('complete_your_registration') . "</h3>
							<p>&nbsp;</p>

							<h4>
								" . Cubix_I18n::translate('signup_successful_04') . "
							</h4>
						</div>
					";
					$result['signup'] = true;
					echo json_encode($result);
					die;
				}
				else {
					if ( $user_type == 'vip-member' ) {
						$this->_session->want_premium = (object) array('member_data' => array('id' => $this->view->n_member->getId()), 'email' => $user->email);
						header('Location: /' . Cubix_I18n::getLang() . '/private-v2/upgrade');
						exit;
						//$this->_helper->viewRenderer->setScriptAction('signup-success-cc');
					}
					else {
						$this->_helper->viewRenderer->setScriptAction('signup-success');
					}
				}
			}
		}
	}


	public function forgotAction()
	{
		$this->view->data = array('email' => '');
		$this->view->errors = array();

		if ( $this->_request->isPost() ) {
			$this->view->data['email'] = $email = trim($this->_getParam('email'));

			if ( ! strlen($email) ) {
				return;
			}

			$model = new Model_Users();
			if ( ! ($user = $model->forgotPassword($email)) ) {
				$this->view->errors['email'] = 'This email is not registered on our site';
				return;
			}

			Cubix_Email::sendTemplate('forgot_verification', $email, array(
				'username' => $user['username'],
				'hash' => $user['email_hash']
			));

			$this->_helper->viewRenderer->setScriptAction('forgot-success');
		}
	}


	public function ajaxGetCitiesAction()
	{
		$this->view->layout()->disableLayout();

		$this->_request->setParam('no_tidy', true);

		$country_id = intval($this->_getParam('country_id'));
		$region_id = intval($this->_getParam('region_id'));

		$model = new Cubix_Geography_Cities();

		$this->view->regions = $model->ajaxGetAll($region_id, $country_id);

	}


    private function _hasStatusBit($old_status, $status)
    {
        $result = true;
        $result = ($old_status & $status) && $result;
        return $result;
    }

}
?>
