<?php

class ErrorController extends Zend_Controller_Action
{
	public function errorAction()
	{
		$content = '';
		$errors = $this->_getParam ('error_handler');
		$error_msg = $this->_getParam ('error_msg');
		$param = $this->_getParam ('param');

		switch ($errors->type) {
			case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER :
			case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION :
			case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE :

				$redirect_page = Cubix_Api_XmlRpc_Client::getInstance()->call('Application.getRedirectPage', array($_SERVER['REQUEST_URI'], $_SERVER['HTTP_REFERER']));
				if ( $redirect_page && strlen($redirect_page ) ) {
					header('HTTP/1.1 301 Moved Permanently');
					header('Location: ' . $redirect_page);
					die;
				}

				$this->_response->setRawHeader('HTTP/1.1 404 Not Found');

				$content .= "<h1>404 Page not found!</h1>" . PHP_EOL;
				$content .= "<p>The page you requested was not found.</p>";

				break ;
			default :
				$exception = $errors->exception;

				if ( $exception ) {
					$content .= "<h1>Error!</h1>" . PHP_EOL;
					$content .= "<p>An unexpected error occurred with your request. Please try again later.</p>";
					//$content .= "<pre class=\"debug-exception\">{$exception->__toString()}</pre>";

					/*$log = new Zend_Log(
						new Zend_Log_Writer_Stream(APPLICATION_PATH . '/logs/frontend-exceptions.log')
					);
					$log->debug(
						$exception->getMessage() . PHP_EOL . $exception->getTraceAsString()
					);*/
					Cubix_Debug::log('URL: ' . $_SERVER['REQUEST_URI'] . ", " . $_SERVER['HTTP_REFERER'] . "\n" . $exception->getMessage() . PHP_EOL . $exception->getTraceAsString(), 'ERROR');
				}
				else if ( $error_msg ) {
					$content = "<h1>" . $error_msg . "</h1>";

					if ( $param ) {
						$param = " (" . $param . ")";
					}

					Cubix_Debug::log('URL: ' . $_SERVER['REQUEST_URI'] . ", " . $_SERVER['HTTP_REFERER'] . "\n" . $error_msg . $param, 'ERROR');
				}
				break ;
		}
//die($content);
		$this->getResponse()->clearBody();
		$this->view->content = $content;
	}

	public function noDefaultLangAction()
	{
		header('HTTP/1.1 503 Service Temporarily Unavailable');

		$error_msg = $this->_getParam('error_msg');

		$m = ' ( No Default Language Defined. Please resync. )';

		$this->view->content = $content = "<h1>" . $error_msg . $m . "</h1>";

		if ( $param ) {
			$param = " (" . $param . ")";
		}

		Cubix_Debug::log($error_msg . $param . $m, 'ERROR');

		$content = $this->view->render('error/no-default-lang.phtml');
		echo $content;

		die;
	}
}
