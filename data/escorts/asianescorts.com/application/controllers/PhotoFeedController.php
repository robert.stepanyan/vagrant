<?php

class PhotoFeedController extends Zend_Controller_Action
{
	public $_sort_values;
	public $_date_values;

	public function init()
	{
		$this->view->layout()->setLayout('photo-feed');

		$this->model = new Model_PhotoFeed();
		$this->client = new Cubix_Api_XmlRpc_Client();
				
		$this->view->perRow = 5;

		$this->view->sort_values = $this->_sort_values = array(
			'r' => __('random'),
			'n' => __('newest_added'),
			'mv' => __('most_voted'),
			'lv' => __('least_voted'),
			'ra' => __('rating_asc'),
			'rd' => __('rating_desc'),
		);

		$this->view->date_values = $this->_date_values = array(
			't' => __('today'),
			'y' => __('yesterday'),
			'tw' => __('this_week'),
			'lw' => __('last_week'),
			'tm' => __('this_month'),
			'lm' => __('last_month'),
		);
	}
	
	public function indexAction()
	{
		$cache = Zend_Registry::get('cache');

		//$defines = $this->view->defines = Zend_Registry::get('defines');
		
		/*$cache_key_cities = 'photo_feed_cities' . Cubix_I18n::getLang() . Cubix_Application::getId();
		if ( ! $cities = $cache->load($cache_key_cities) ) {
			$cities = $this->model->getAllCitiesForFilter();

            $cache->save($cities, $cache_key_cities, array());
        }
		
		$this->view->cities = $cities;*/
		
		$filter = array();

		$this->view->perPage = $perPage = 65;
		$this->view->page = $page = 1;

		$cache_key = 'photo_feed_' . Cubix_I18n::getLang() . Cubix_Application::getId() . $page . $perPage;
		$cache_key .= Cubix_Countries::blacklistCountryCacheKeyCase();
        $cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
        $cache_key_count =  $cache_key . '_count';
		
        $count = $cache->load($cache_key_count);

		if ( ! $photos = $cache->load($cache_key) ) {
			$photos = $this->model->getAll($filter, "r", $page, $perPage, $count);

            $cache->save($photos, $cache_key, array());
            $cache->save($count, $cache_key_count, array());
        }
		

        shuffle($photos);

		$this->view->photos = $photos;
		$this->view->count = $count;
	}

	public function ajaxHeaderAction()
	{
		$cache = Zend_Registry::get('cache');	
		$this->view->layout()->disableLayout();
		$req = $this->_request;
	
		/*$cache_key_cities = 'photo_feed_cities' . Cubix_I18n::getLang() . Cubix_Application::getId();
		if ( ! $cities = $cache->load($cache_key_cities) ) {
			$cities = $this->model->getAllCitiesForFilter();

            $cache->save($cities, $cache_key_cities, array());
        }

        $this->view->cities = $cities;*/
		
		$this->view->city = $city = (int)$req->city;
		$this->view->country = $country = (int)$req->country;
		$this->view->only_unrated = $only_unrated = (int)$req->only_unrated;
		$this->view->sort = $sort = $req->sort;
		$this->view->date = $date = $req->date;
	}
	
	public function ajaxListAction()
	{
		$cache = Zend_Registry::get('cache');

		$this->view->layout()->disableLayout();
		$req = $this->_request;

		$this->view->perPage = $perPage = 65;
		$page = $req->page;
		$this->view->sort = $sort = $req->sort;
		
		if ( $page < 1 ) 
			$page = 1;
		
		$this->view->page = $page;
		
		$filter = array();
		
		$this->view->city = $city = (int)$req->city;
		$this->view->country = $country = (int)$req->country;
		$this->view->only_unrated = $only_unrated = (int)$req->only_unrated;
		$this->view->date = $date = $req->date;

		if ( $city ) {
			$filter['city_id'] = $city;
		}

		if ( $country ) {
			$filter['country_id'] = $country;
		}
		
		if ( $only_unrated ) {
			$filter['only_unrated'] = $only_unrated;
		}

		if ( $date ) {
			if ( ! array_key_exists($date, $this->view->date_values) ) {
				$this->view->date = '';
			} else {
				$filter['date'] = $date;
			}
		}
		
		$cache_key = 'photo_feed_' . Cubix_I18n::getLang() . Cubix_Application::getId() . $page . $perPage . $city . $country . $only_unrated . $date . $sort;
		$cache_key .= Cubix_Countries::blacklistCountryCacheKeyCase();
        $cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
        $cache_key_count =  $cache_key . '_count';
		
        $count = $cache->load($cache_key_count);

		if ( ! $photos = $cache->load($cache_key) ) {
			$photos = $this->model->getAll($filter, $sort, $page, $perPage, $count);

            $cache->save($photos, $cache_key, array());
            $cache->save($count, $cache_key_count, array());
        }

        if ( $sort == "r" ) {
        	shuffle($photos);
        }

		$this->view->photos = $photos;
		$this->view->count = $count;
	}

	public function ajaxVoteAction() 
	{
		$this->view->layout()->disableLayout();

		$escort_id = (int) $this->_request->escort_id;
		$photo_id = (int) $this->_request->photo_id;
		$rate = (int) $this->_request->rate;

		if ( $rate < 1 ) $rate = 1; elseif ( $rate > 10 ) $rate = 10;

		$ip = $this->model->getIp();

		if ( $ip ) {
			$is_api_rated = $this->client->call('ratePhotoFeedPhoto', array($photo_id, $rate, $ip, $escort_id));

			if ( $is_api_rated ) {
				$this->model->ratePhoto($photo_id, $rate, $ip, $escort_id);
				$this->model->_ratePhoto($photo_id, $rate);
			}
		}

		$this->view->is_photo_rated = $this->model->_checkIsPhotoRated($photo_id);

		$big_photo = $this->model->getPhoto($escort_id, $photo_id);
        $this->view->big_photo = new Model_Escort_PhotoItem($big_photo);
	}

	public function ajaxVotingBoxAction()
	{
		$this->view->layout()->disableLayout();

		$req = $this->_request;

		$this->view->escort_id = $escort_id = (int) $req->escort_id;
		$this->view->exclude_photo_id = $photo_id = (int) $req->photo_id;

		//$this->model->_ratePhoto($photo_id, 8);


		if ( ! $escort_id || ! $photo_id ) die(':)');

		$model = new Model_EscortsV2();

		$cache_key = 'v2_' . $escort_id .  '_profile_' . Cubix_I18n::getLang() . Cubix_Application::getId();
        $cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

        $escort = $model->get($escort_id, $cache_key);
        $escort = new Model_EscortV2Item($escort);
        $this->view->escort = $escort;

        $_tours = $escort->getCitytours();
        $tours = array('current' => array()); 
        if ( $_tours ) {
	        foreach ( $_tours as $tour ) {
				if ( $tour->is_in_tour ) {
					$tours['current'][] = $tour;
				}
			}

			if ( count($tours['current']) > 0 ) $tour = $tours['current'][0];
			
			$this->view->tour = $tour;
		}

		if ( $escort->agency_id ) {
			$this->view->agency = $model->getAgency($escort_id);
		}

        $big_photo = $this->model->getPhoto($escort_id, $photo_id);
        $this->view->big_photo = new Model_Escort_PhotoItem($big_photo);

        // show you rating in voting box (check if in cookie exist this photo)
        $ip = $this->model->getIp();
        $this->view->is_photo_rated = $this->model->_checkIsPhotoRated($photo_id);
        $this->view->is_photo_rated_by_ip = $this->model->_checkIsPhotoRatedByIp($photo_id, $ip);

        $p_count = 0;
        $photos = $this->model->getPhotos($photo_id, $escort_id, 1, 5, $p_count);
        $this->view->photos = $photos;
        $this->view->photos_count = $p_count;
        $this->view->photos_page = 1;
        $this->view->photos_perPage = 5;
	}

	public function ajaxGetMorePhotosAction()
	{
		$this->view->layout()->disableLayout();

		$req = $this->_request;

        $this->view->escort_id = $escort_id = (int) $this->_getParam('escort_id');
        $this->view->exclude_photo_id = $exclude_photo_id = (int) $this->_getParam('exclude_photo_id');
        $page = (int) $this->_getParam('page', 1);

        $pp = 5;
        $count = 0;
        $photos = $this->model->getPhotos($exclude_photo_id, $escort_id, $page, $pp, $count);
        $this->view->photos = $photos;
        $this->view->photos_count = $count;
        $this->view->photos_page = $page;
        $this->view->photos_perPage = $pp;
	}
}