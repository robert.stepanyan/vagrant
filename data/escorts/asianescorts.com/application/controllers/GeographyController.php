<?php

class GeographyController extends Zend_Controller_Action
{
	public function indexAction()
	{

	}

	public function ajaxGetCountriesAction()
	{
		$this->_request->setParam('no_tidy', true);

		$model = new Cubix_Geography_Countries();
		echo json_encode(array(
			'data' => $model->ajaxGetAll($this->_getParam('has_regions', TRUE))
		));
		die;
	}

	public function ajaxGetCitiesAction()
	{
		$this->_request->setParam('no_tidy', true);

		$country_id = intval($this->_getParam('country_id'));
		$region_id = intval($this->_getParam('region_id'));

		$model = new Cubix_Geography_Cities();

		echo json_encode(array(
			'data' => $model->ajaxGetAll($region_id, $country_id)
		));
		die;
	}

	public function ajaxCitiesSearchAction()
	{
		$this->_request->setParam('no_tidy', true);

		$name = trim($this->_getParam('city_search_name'));

		if ( ! strlen($name) ) {
			die(json_encode(array()));
		}

		$cache = Zend_Registry::get('cache');
		$model = new Cubix_Geography_Cities();

		$cache_key = 'ajax_city_search_' . $name;
		$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

		if ( ! $cities = $cache->load($cache_key) ) {
			$cities = $model->ajaxSearch($name, Cubix_Application::getById()->country_id);
			$cache->save($cities, $cache_key, array());
		}
		echo json_encode($cities);
		die;
	}

	public function ajaxHomeCitiesSearchAction()
	{
		$this->_request->setParam('no_tidy', true);

		$name = trim($this->_getParam('city_search_name'));
		$lng = trim($this->_getParam('lng'));

		if ( ! strlen($name) ) {
			die(json_encode(array()));
		}		
		$model = new Cubix_Geography_Cities();

		$cities = $model->ajaxGetAllCities($name, $lng);

		$tmp_cities = array();
		if ( count($cities) ) {
			foreach ( $cities as $i => $city ) {
				$tmp_cities[$i] = array('id' => $city->id, 'name' => $city->title . ' (' . $city->country_title . ')');
			}
		}

		echo json_encode($tmp_cities);
		die;
	}

	public function ajaxGetCitizonesAction()
	{
		$this->_request->setParam('no_tidy', true);

		$city_id = intval($this->_getParam('city_id'));

		$model = new Cubix_Geography_Cityzones();

		echo json_encode(array(
			'data' => $model->ajaxGetAll($city_id)
		));
		die;
	}

	public function ajaxGetCitiesGroupedAction()
	{
		$this->_request->setParam('no_tidy', true);

		$country_id = intval($this->_getParam('country_id'));
		$region_id = intval($this->_getParam('region_id'));

		if ( 1 > $country_id && 1 > $region_id ) {
			die;
		}

		$model = new Cubix_Geography_Cities();

		$cities = $model->ajaxGetAll($region_id, $country_id);
		$has_regions = true;
		$regions = array();
		foreach ( $cities as $city ) {
			if ( ! isset($regions[$city->region_title]) ) {
				$regions[$city->region_title] = array();
			}

//			$has_regions = $city->has_regions;
			$regions[$city->region_title][] = $city;
		}

		ksort($regions);

		foreach ( $regions as $k => $region ) {
			usort($regions[$k], create_function('$a1, $a2', 'return ($a1["title"] == $a2["title"]) ? 1 : ($a1["title"] > $a2["title"] ? 1 : 0);'));
		}

		$is_json = ! is_null($this->_getParam('json'));
		if ( $is_json ) {
			die(json_encode($regions));
		}

		$html = '';

		if ( $has_regions ) {
			foreach ( $regions as $title => $cities ) {
				$html .= '<optgroup label="' . $title . '">';
				foreach ( $cities as $city ) {
					$html .= '<option value="' . $city->id .'">' . $city->title . '</option>';
				}
				$html .= '</optgroup>';
			}
		}
		else {
			foreach ( $cities as $city ) {
				$html .= '<option value="' . $city->id .'">' . $city->title . '</option>';
			}
		}

		echo $html;
		die;
	}
}
