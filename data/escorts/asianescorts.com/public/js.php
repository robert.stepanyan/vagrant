<?php
$CACHE_OFFSET = 7 * 24 * 60 * 60;
$JS_LOCATION  = "js/combined/";
$CSS_LOCATION = "css/combined/";

function getMaxModifTime( $js_sources ){
	$max_modif_time = null;
	foreach( $js_sources as $source ){
		$m_time = @filemtime($source);
		if( is_null($max_modif_time) ){
			$max_modif_time = $m_time;
		}
		if( $m_time > $max_modif_time ){
			$max_modif_time = $m_time;
		}
	}
	return $max_modif_time;
}


function writeToFile( $js_sources, $file_path ){
	//ATTENTION !!! Minify class is changed by Vahag. CLIENT CACHE VALIDATION IS DISABLED.
	$output = Minify::serve('Files', array(
        'files'  => $js_sources
        ,'quiet' => true
        //,'lastModifiedTime' => $lastModified
        ,'encodeOutput' => false
    ));
	$js = $output['content'];
	//echo $js;

	$f = @fopen($file_path, 'w');
	if (!$f) {
		echo 'Problem while writing to file.' . $file_path ; die;
	} else {
		$bytes = fwrite($f, $js);
		fclose($f);
	}
}


require dirname(__FILE__) . '/min/config.php';

set_include_path($min_libPath . PATH_SEPARATOR . get_include_path());

require_once 'Minify.php';

	//JS FILES
	$js_sources = array(
		'js/mootools.js',
		'js/global.js',
		'js/popup.js',
		'js/header.js',
		'js/email-collecting.js'

	);

	$other_sources = array(
		'listing' => array(
			'js/late_night_girls.js',
			'js/overlay.js',
			'js/photo-roller.js',
			'js/escorts.js',
			'js/filter-v2.js',
			'js/analytics.js',
			'js/Autocompleter.js',
			'js/bubble.js',
			'js/online.js',
			'js/tooltip.js',
            'js/scrollspy.js'
		),
		'profile' => array(
			'js/late_night_girls.js',
			'js/comments.js',
			'js/overlay.js',
			'js/highslide.js',
			'js/photo-roller.js',
			'js/feedback.js',
			'js/r-popup.js',
			'js/add-to-favorites.js',
			'js/photos.js',
			'js/profile.js',
			'js/photo-viewer.js',
			'js/agency-escorts.js',
			'js/alert-me.js',
			'js/bubble.js',
			'js/online.js'
		),
		'profile-v2' => array(
			'js/slimbox.js',
			'js/profile-2.0.js',
			'js/MooHashChange.js',
			'js/late_night_girls.js',
			'js/overlay.js',
			'js/comments-v2.js',			
			'js/feedback.js',
			'js/r-popup.js',
			'js/add-to-favorites.js',
			'js/alert-me-v2.js',
			'js/bubble.js',
			'js/online.js',
			'js/add-to-favorites.js',
		),
		'reviews' => array(
			'js/escort-reviews.js',
			'js/overlay.js',
			'js/analytics.js',
			'js/bubble.js',
			'js/online.js'
			/*,
			'js/Autocompleter.js',
			'js/reviews.js',
			'js/reviews_search.js',
			,*/
			
		),
		'classified-ads' => array(
            'js/slimbox.js',
            'js/overlay.js',
            'js/classified_ads.js',
            'js/r-popup.js',
            'js/feedback.js',
        ),
		'latest-actions' => array(			
			'js/overlay.js',			
			'js/latest_actions.js'
		),
		'photo-feed' => array(
			'js/overlay.js',			
			'js/photo_feed.js',				
		),
		'sign-up' => array(
		)
	);



	//CSS FILES
	$css_sources = array(
		'css/chat.css'
	);

	$css_other = array(
		'reviews' => array(
			'css/v2.1.css',
			'css/popup-v2.css',
			//'css/no-sidebar.css',
			'css/splash-sexindex.css',
			'css/reviews.css'
		),
		'profile' => array(
			'css/highslide.css',
			'css/v2.1.css',
			'css/popup-v2.css',
			'css/v2.1-setcard.css',
			'css/splash-sexindex.css',
			'css/photo-viewer.css'
		),
		'profile-v2' => array(
			'css/highslide.css',
			'css/v2.1.css',
			'css/popup-v2.css',
			'css/v2.2-setcard.css',
			'css/splash-sexindex.css'
		),
		'listing'	=> array(
			'css/v2.1.css',
			'css/splash-sexindex.css',
			'css/popup-v2.css'
		),
		'sign-up' => array(
			'css/v2.1.css',
			'css/splash-sexindex.css',
			'css/private.css',
			'css/signup.css',
			'css/signup-pm.css'
		),
		'classified-ads' => array(
            'css/v2.1.css',
            'css/splash-sexindex.css',
            'css/popup-v2.css',
            'css/classified-ads.css',
        ),
		'latest-actions' => array(
			'css/v2.1.css',
			'css/splash-sexindex.css',
			'css/popup-v2.css',
			'css/latest-actions.css',
		),
		'photo-feed' => array(
			'css/v2.1.css',
			'css/splash-sexindex.css',
			'css/popup-v2.css',
			'css/photo-feed.css',
		)
	);

	$load_key = $_GET['load_key'];
	$type = $_GET['type'];
	
		
	/* Validate all get params */
	$types = array('css', 'js');

	if ( ! in_array($type, $types) ) {
		die('Invalid type was specified');
	}

	if ( $type == 'css' ) {
		if ( ! array_key_exists($load_key, $css_other) ) {
			die('Invalid load_key was specified');		
		} 
	} elseif ( $type == 'js' ) {
		if ( ! array_key_exists($load_key, $other_sources) ) {
			die('Invalid load_key was specified');		
		} 
	}
	/* Validate all get params */

	
	$SOURCE_LOCATION = ( $type == 'css' ) ? $CSS_LOCATION : $JS_LOCATION;
	if ( strlen($load_key) ) {
		$SOURCE_LOCATION .= '_' . $load_key . '.' . $type;
	} else {
		$SOURCE_LOCATION .= $type == 'js' ? '_scripts.js' : '_styles.css';
	}
	
	if ( $type == 'js' ) {
		$_sources = array_merge($js_sources, ( strlen($load_key) &&  isset( $other_sources[$load_key]) ? $other_sources[$load_key] : array()) );
	} elseif ( $type == 'css' ) {
		$_sources = array_merge($css_sources, ( strlen($load_key) &&  isset($css_other[$load_key]) ? $css_other[$load_key] : array()) );
	}
	if( file_exists($SOURCE_LOCATION)  ){
		$file_modif_time = @filemtime( $SOURCE_LOCATION );
		if( getMaxModifTime($_sources) > $file_modif_time ){
			writeToFile($_sources, $SOURCE_LOCATION);
		}else{
			// no need to write again			
		}		
	}else{
		writeToFile($_sources, $SOURCE_LOCATION);
	}

	$js = @file_get_contents( $SOURCE_LOCATION );
	
	if ( $type == 'js' ) {
		header ('content-type: text/javascript; charset: UTF-8');
	} elseif ( $type == 'css' ) {
		header ('content-type: text/css;');
	}
	//USER CACHE VALIDATION
	if( getMaxModifTime($_sources) < $file_modif_time && $_SERVER['HTTP_CACHE_CONTROL'] == 'max-age=0' && array_key_exists("HTTP_IF_MODIFIED_SINCE",$_SERVER) ){
		header("HTTP/1.1 304 Not Modified");
	} else {
		header ('last-modified: ' . gmdate ('D, d M Y H:i:s', getMaxModifTime($_sources) ) . ' GMT');
		header ('cache-control: max-age=0');
	}
	header ('expires: ' . gmdate ('D, d M Y H:i:s', time() + $CACHE_OFFSET ) . ' GMT');
	header ('date: ' . gmdate ('D, d M Y H:i:s', time() ) . ' GMT');

	
	ob_start ('ob_gzhandler');
	echo $js;
	ob_flush();
