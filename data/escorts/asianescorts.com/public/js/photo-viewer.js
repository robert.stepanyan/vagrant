var PhotoViewer = new Class({
	Implements: [Options, Events],

	overlay: null,
	request: null,

	initialize: function (options) {
		this.setOptions(options || {});

		this.overlay = new Cubix.Overlay($(document.body), { color: '#000', has_loader: false, opacity: .5 });
		this.request = new Request({
			method: 'get',
			data: {},
			onSuccess: function (response) {
				this.data = JSON.decode(response).data;
				if ( ! this.data.length ) return;
				this.pages = Math.ceil(this.data.length / 7);
				this.render();
				this.init();
				this.fireEvent('load', [this.data]);

				$$('.highslide').addEvent('contextmenu',function(e) {
					e.stop();
				});
				
			}.bind(this)
		});
	},

	visible: false,

	open: function () {
		this.overlay.disable();
		var el = this.el('dialog'), size = { w: el.getWidth(), h: el.getHeight() },
			s = { left: $(document.body).getWidth() / 2 - size.w / 2, top: window.getScroll().y + $(document.body).getHeight() / 2 - size.h / 2, visibility: 'visible' };
		if ( s.top < 30 ) s.top = 30;
		el.setStyles(s);
		el.set('opacity', 1);
		this.visible = true;
		this.overlay.fit();
	},

	close: function (e) {
		if ( undefined != e ) e.stop();
		this.el('dialog').setStyle('visibility', 'hidden');
		this.overlay.enable();
		this.el('dialog').set('opacity', 0);
		this.visible = false;
	},

	load: function (url, data, id) {
		this.sel_id = id;
		if ( this.data ) {
			if ( this.sel_id > 0 ) index = PhotoViewer.byId(id)
			else index = 0;
			this.sel = index;
			return this.select(index);
		}

		this.overlay.disable();
		$extend(this.request.options, {
			data: data || {},
			url: url
		});
		this.request.send();
	},

	pages: null,
	sel: 0,
	itemWidth: 0,

	render: function () {
		var clear = new Element('div', { 'class': 'clear' });
		// Main dialog
		var dialog = new Element('div', {
			'class': 'photo-viewer',
			opacity: 0
		}).inject($(document.body));
		this.el('dialog', dialog);

		// Close button
		var close = new Element('a', {
			'class': 'close',
			href: '#'
		});
		this.el('close', close);

		// Title
		var title = new Element('h4', { html: this.options.title });

		// Main photo
		var main = new Element('div', {
			'class': 'main'
		});
		var a = new Element('a', { href: '#', 'class': 'highslide', onClick: 'return hs.expand(this)' }).inject(main);
		var img = new Element('img', { src: '', width: 514, height: 492 }).addClass('middleimg').inject(a);
		this.el('image', img);

		// Current image navigation
		var cur = new Element('div', { 'class': 'cur' });
		var prev = new Element('a', { 'class': 'prev' });
//		new Element('img', { src: _st('s.gif'), width: 16, height: 16 }).inject(prev);
		var next = new Element('a', { 'class': 'next' });
		var index = new Element('div', { 'class': 'span' });
		$$([prev, index, next]).inject(cur);
		this.el('current', index);
		this.el('prev', prev);
		this.el('next', next);

		var left = new Element('a', { 'class': 'left' });
		var right = new Element('a', { 'class': 'right' });
		this.el('left', left);
		this.el('right', right);
		var slider = new Element('div', { 'class': 'slider' });
		var wrap = new Element('div', { 'class': 'wrap' }).inject(slider);
		this.el('slider', wrap);
		var els = [];

		this.data.each(function (i, index) {
			var a = new Element('a', { href: '#'});
			i.index = index;
			a.store('pv:image', i);
			var img = new Element('img', { src: i.s }).inject(a);
			els.push(a);

			if ( i['private'] ) {
				new Element('span', { 'class': 'priv' }).addEvent('click', function (e) { e.stop(); GoPremiumPopup.open(this); }).inject(a);
			}
			else {
				/*a.addEvent('mouseenter', function () {
					this.select(index, false);
				}.bind(this));*/
			}
		}.bind(this));
		$$(els).inject(wrap);
		this.el('images', $$(els));

		//var text = new Element('div', { 'class': 'text', html: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.' });

		// Assemble all elements
		$$([close, title, main, cur, left, slider, right, clear.clone()/*, text*/]).inject(dialog);

		this.itemWidth = els[0].getWidth() + parseInt(els[0].getStyle('margin-right'));

		this.loader = new Element('div', {
			styles: {
				position: 'absolute',
				background: '#fff url("' + _st('loader-small.gif') + '") no-repeat 50% 50%',
				opacity: .5,
				left: 0,
				top: 0
			}
		}).inject(this.el('image').getParent('div'));
		var t = this;
		$extend(this.loader, {
			reposition: function () {
				var i = t.el('image'), c = i.getCoordinates(i.getParent('div'));
				delete c['right']; delete c['bottom'];
				this.setStyles(c);
				this.setStyle('height', c.height + 3);
			}.bind(this.loader)
		});
		this.enable();
	},

	loading: false,

	select: function (index, slide) {
		var t = this, img = this.el('images')[index];
		if ( ! img ) return;
		var i = img.retrieve('pv:image');
		
		if ( this.loading /*|| img.hasClass('sel')*/ ) return;

		if ( i['private'] ) {
			if ( this.sel < index ) {
				index++;
			}
			else {
				index--;
			}
			
			this.select(index, slide);
			return;
		}

		this.sel = index;
		this.el('images').removeClass('sel');
		img.addClass('sel');

		if ( index == 0 ) {
			this.el('prev').set('class', 'prev-d');
		}
		else {
			this.el('prev').set('class', 'prev');
		}

		if ( index == this.data.length - 1 ) {
			this.el('next').set('class', 'next-d');
		}
		else {
			this.el('next').set('class', 'next');
		}

		this.el('current').set('html', this.sel + 1 + ' of ' + this.data.length);

		this.disable();
		this.loading = true;
		var at = new Asset.image(i.b, {
			onload: function () {
				$(this).replaces(t.el('image'));
				t.el('image', $(this));
				t.el('image').getParent().set('href', i.h);
				t.loading = false;
				t.enable();
				if ( ! t.visible ) t.open();
			}
		}).addClass('middleimg');

		if ( undefined == slide || slide ) {
			this.slideTo(index);
		}
	},

	next: function () {
		if ( this.sel < this.data.length - 1 ) {
			this.select(this.sel + 1);
		}
	},

	prev: function () {
		if ( this.sel > 0 ) {
			this.select(this.sel - 1);
		}
	},

	margin: 0,

	slideTo: function (i) {
		if ( this.center < this.data.length - 1 - 3 && this.data.length > 7 ) {
			this.el('right').setStyle('visibility', 'visible');
		}
		else {
			this.el('right').setStyle('visibility', 'hidden');
		}

		if ( this.center > 3 && this.data.length > 7 ) {
			this.el('left').setStyle('visibility', 'visible');
		}
		else {
			this.el('left').setStyle('visibility', 'hidden');
		}


		if ( this.data.length <= 7 ) return;

		this.center = i;

		var c = 3;
		if ( i + 3 > this.data.length - 1 ) {
			i = this.data.length - 4;
		}
		else if ( i < 3 ) {
			c = i;
		}

		margin = -1 * (i - c) * this.itemWidth;

		this.fx.start('margin-left', margin);
	},

	center: 0,

	left: function () {
		if ( this.center > this.data.length - 3 ) {
			this.center = 3;
		}

		this.slideTo(this.center - 7);
	},

	right: function () {
		if ( this.center < 3 ) {
			this.center = 3;
		}

		this.slideTo(this.center + 7);
	},

	init: function() {
		this.fx = new Fx.Tween(this.el('slider'), {
			duration: 'short',
			link: 'chain'//,
//			transition: 'linear'
		});
		
		var t = this;
		this.el('images').addEvent('click', function (e) {
			e.stop();
			var i = this.retrieve('pv:image');
			t.select(i.index);
		});

		this.el('prev').addEvent('click', this.prev.bind(this));
		this.el('next').addEvent('click', this.next.bind(this));

		this.el('left').addEvent('click', this.left.bind(this));
		this.el('right').addEvent('click', this.right.bind(this));

		this.el('close').addEvent('click', this.close.bindWithEvent(this));

		if ( this.sel_id > 0 ) this.sel = PhotoViewer.byId(this.sel_id)
		else this.sel = 0;
		this.select(this.sel);
	},

	elements: {},

	el: function (name, value) {
		if ( undefined != value ) {
			return this.elements[name] = value;
		}

		return this.elements[name];
	},

	disable: function () {
		this.loader.reposition();
		this.loader.setStyle('display', 'block');
	},

	enable: function () {
		this.loader.setStyle('display', 'none');
	}
});

document.addEvent('domready', function () {
	var showname = window.Escort.showname;
	var escort_id = window.Escort.escort_id;
	var pv = new PhotoViewer({ title: showname });
	PhotoViewer.Show = function (id) {
		id = undefined != id ? id : 0;
		this.load('/index/photo-viewer', { escort_id: escort_id }, id);
		return false;
	}.bind(pv);

	PhotoViewer.byId = function (id) {
		try {
			this.data.each(function (i, index) {
				if ( i.id == id ) throw index;
			});
		} catch (index) {
			return index;
		}

		return false;
	}.bind(pv);
});
