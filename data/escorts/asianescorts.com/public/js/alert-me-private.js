window.addEvent('domready', function(){	
	var cities = Array();
	
	$$('.cts').get('value').each(function(el) {
		cities.push(el);
	})
		
	$$('.alertme-save').get('rel').each(function(el) {
		checkCHB(el);
	})
	
	$$('.alm-any').addEvent('click', function(e){
		var arr = Array();
		arr = this.get('name').split('_');
		
		checkCHB(arr[1]);
	});
	
	$$('.alertme-city-btn').addEvent('click', function (e) {
		var self = this;
		var escort_id = Number.from(self.get('rel').toString());
		var c = $('city_id_' + escort_id).getSelected();
		var c_id = Number.from(c.get('value').toString());
		var c_txt = c.get('text').toString();
		
		$('alm-6-' + escort_id).set('checked', 'checked');

		if (c_id != null && !cities.contains(escort_id + '-' + c_id))
		{
			cities.push(escort_id + '-' + c_id);
			addCity(escort_id, c_id, c_txt);
			removeCity(cities);
		}
	});
	
	removeCity(cities);
	
	$$('.alertme-save').addEvent('click', function(e){
		e.stop();
		
		var escort_id = this.get('rel');
		var i = 0;
		var events = Array();
		var arr = Array();
		var cts = Array();
		var tmp = Array();
		
		$$("#form-" + escort_id + " input[type=checkbox]:checked").each(function(el) {
			arr = el.get('id').substr(4).split('-');
			events[i] = arr[0];
			i++;
		});
		
		i = 0;
		
		cities.each(function(el) {
			tmp = el.split('-');
			
			if (tmp[0] == escort_id)
			{
				cts[i] = tmp[1];
				i++;
			}
		});
		
		var cu = '';
		
		if (cts.length > 0)
			cu = '&cities=' + cts.join(',');
		
		var ev = events.join(',');
		
		if (ev == '5')
			ev = '1,2,3,4';
		else if (ev == '5,6')
			ev = '1,2,3,4,6';
		
		var req = new Request({
			method: 'get',
			url: '/escorts/ajax-alerts-save?escort_id=' + escort_id + '&events=' + ev + cu,
			onComplete: function(response) {
				window.location = document.URL;
			}
		}).send();
		
		return false;
	});
	
	$$('.alertme-remove').addEvent('click', function(e){
		e.stop();
		
		var escort_id = this.get('rel');
				
		var req = new Request({
			method: 'get',
			url: '/escorts/ajax-alerts-save?escort_id=' + escort_id + '&events=',
			onComplete: function(response) {
				window.location = document.URL;
			}
		}).send();
		
		return false;
	});
});

function addCity(escort_id, id, text)
{
	var myFirstElement  = new Element('div', {'class': 'city-item-wr'});
	var mySecondElement = new Element('a', {id: id, 'class': 'city-item'});
	var myHiddenElement = new Element('input', {type: 'hidden', value: escort_id + '-' + id, 'class': 'cts'});
	mySecondElement.inject(myFirstElement);
	myFirstElement.appendText(text);
	myHiddenElement.inject(myFirstElement);
	myFirstElement.inject($('city-items-' + escort_id));
}

function removeCity(cities)
{
	$$('.city-item').removeEvents();
	$$('.city-item').addEvent('click', function (e) {
		var self = this;

		var id = self.get('id');
		var escort_id = self.get('rel');
		
		removeByElement(cities, escort_id + '-' + id);
		self.getParent().dispose();
	});
}

function removeByElement(arrayName, arrayElement)
{
	for (var i = 0; i < arrayName.length; i++)
	{ 
		if (arrayName[i] == arrayElement)
			arrayName.splice(i,1); 
	} 
}

function checkCHB(escort_id)
{
	if ($$('input[name=5_' + escort_id + ']').get('checked') == 'true')
	{
		$$('input[name=1_' + escort_id + ']').set('checked', '');
		$$('input[name=1_' + escort_id + ']').set('disabled', 'disabled');
		$$('input[name=2_' + escort_id + ']').set('checked', '');
		$$('input[name=2_' + escort_id + ']').set('disabled', 'disabled');
		$$('input[name=3_' + escort_id + ']').set('checked', '');
		$$('input[name=3_' + escort_id + ']').set('disabled', 'disabled');
		$$('input[name=4_' + escort_id + ']').set('checked', '');
		$$('input[name=4_' + escort_id + ']').set('disabled', 'disabled');
	}
	else
	{
		$$('input[name=1_' + escort_id + ']').set('disabled', '');
		$$('input[name=2_' + escort_id + ']').set('disabled', '');
		$$('input[name=3_' + escort_id + ']').set('disabled', '');
		$$('input[name=4_' + escort_id + ']').set('disabled', '');
	}
}
/* <-- */
