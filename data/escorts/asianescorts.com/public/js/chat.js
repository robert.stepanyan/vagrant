function Chat(sessionId) {
	var socket,
		forceEnable,
		onlineUsers = [], //For easily sorting users
		onlineUsersObj = {},//For easily getting user by id
		selfUser = {},
		browser = Browser.name;
		browserVersion = Browser.version;
	
	var tabSpace,
		windowSize,
		tabWidth = 149,
		tabsCanOpened,
		openTabs = 0,
		tabsFromLeft = tabsFromRight = 0;
		
	var audio;
		
	var monthNames = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
	
	var messages = {
		'offlineMessage' : 'This user is offline. They will receive the message next login',
		'blackListedWordsMessage' : 'You can`t use words "%words%"',
		'userBlockedYou' : 'Message not sent. This user has blocked you.'
	};
	
	var customEmotions = {
		'>:o'	: 'upset',
		'O:)'	: 'angel',
		'3:)'	: 'devil',
		'>:('	: 'grumpy'
	};
	var emotions = {
		':)'	: 'smile',
		':('	: 'frown',
		':P'	: 'tongue',
		':D'	: 'grin',
		':*'	: 'kiss',
		':o'	: 'gasp',
		';)'	: 'wink',
		':v'	: 'pacman',
		':/'	: 'unsure',
		':\'('	: 'cry',
		'^_^'	: 'kiki',
		'8)'	: 'glasses',
		'B|)'	: 'sunglasses',
		'<3'	: 'heart',
		'-_-'	: 'squint',
		'o.O'	: 'confused',
		':3'	: 'colonthree'
	};
	
	var userTabSettings = [
		{type : 'checkable', id : 'block-user', name : 'Block User'}
	];
	
	var originalTitle = document.title;
	var blinkTimer;
	
	this.connect = function()
	{
		if ( typeof socket == 'undefined' ) {
			var protocol = (location.protocol === 'https:') ? 'https' : 'http';
			var host = protocol + '://www.asianescorts.com';
			socket = io.connect(host);
		} else {
			socket.socket.connect();
		}
	}
	
	this.initListeners = function()
	{
		socket.on('auth-complete', function(data) {
			signedOffToolbar();
			if ( data.auth ) {
				if ( data.availability ) {
					availableToolbar();
					selfUser = data.userInfo;
					prepareSettings();
				} else {
					notAvailableToolbar();
				}
			} else {
				
			}
			
			$('chat-container').removeClass('hidden');
		});
		socket.on('connect', function() {
			if ( typeof forceEnable == 'undefined' ) {
				forceEnable = false;
			}
			
			socket.emit('auth', {
				sid: sessionId, 
				forceEnable: forceEnable
			});
			forceEnable = false;
		});
		
		socket.on('online-users-list', function(users) {
			onlineUsers = [];
			onlineUsersObj = users;
			for(var i in users) {
				//If user is blocked do not do anything
				if ( typeof selfUser.settings.blockedUsers != 'undefined' && selfUser.settings.blockedUsers.indexOf(users[i].userId) >= 0 ) {
					continue;
				}
				onlineUsers.push(users[i]);
			}
			sortOnlineUsers();
			fillOnlineUsers();
		});
		
		socket.on('new-user', function(userInfo) {
			//If user is blocked do not do anything
			if (  typeof selfUser.settings != 'undefined' && typeof selfUser.settings.blockedUsers != 'undefined' && selfUser.settings.blockedUsers.indexOf(userInfo.userId) >= 0 ) {
				return;
			}
			
			insertUser(userInfo);
			if ( $('user_tab_' + userInfo.userId) ) {
				$$('#user_tab_' + userInfo.userId).removeClass('away').removeClass('offline').addClass('available');
			}
		});
		
		socket.on('offline-users', function(userIds) {
			userIds.each(function(userId) {
				removeUser(userId);
				if ( $('user_tab_' + userId) ) {
					$('user_tab_' + userId).removeClass('away').removeClass('available').addClass('offline');
				}
			});
		});
		
		socket.on('open-dialogs', function(data) {
			//Creating open tabs
			data.dialogs.each(function(dialog) {
				createChatTab(dialog.userInfo);
			});
			
			//Open active dialog
			if ( data.activeDialog ) {
				openUserTab(data.activeDialog, true);
			}
		});
		
		socket.on('new-message', function(data) {
			createChatTab(data.senderData);
			markTabUnread(data.senderData.userId);
			addMessage(data.senderData.userId, data.senderData, data.message);
			blinkPageTitle('New message from ' + data.senderData.nickName);
			playAudio('new-message');
		});
		
		socket.on('new-messages', function(messages) {
			messages.each(function(message) {
				if ( message.senderInfo ) {
					createChatTab(message.senderInfo);
					markTabUnread(message.userId, message.count);
				}
			});
		});
		
		socket.on('message-history', function(data) {
			fillHistory(data.senderInfo, data.messages)
		});
		
		socket.on('typing-start', function(data) {
			userTab = $('user_tab_' + data.userId);
			if ( userTab ) {
				userTab.addClass('typing');
			}
		});
		socket.on('typing-end', function(data) {
			$('user_tab_' + data.userId).removeClass('typing');
		});
		
		socket.on('chat-off', function(data) {
			notAvailableToolbar();
		});
		
		socket.on('bl-word', function(data) {
			showMessage(data.dialogId, messages['blackListedWordsMessage'].replace('%words%', data.word));
		});
		
		socket.on('user-blocked-you', function(data) {
			showMessage(data.dialogId, messages['userBlockedYou']);
		});
		
		socket.on('blocked-users', function(users) {
			$('blocked-users-list').empty();
			
			new Element('option', {
				value: '',
				html: 'Select'
			}).inject($('blocked-users-list'));
			
			users.each(function(user) {
				new Element('option', {
					value: user.userId,
					html: user.nickName
				}).inject($('blocked-users-list'));
			});
		});
	}
	
	this.initEvents = function()
	{
		var self = this;
		
		$('chat-on').addEvent('click', function() {
			self.enableChat();
		});
		
		$('chat-off').addEvent('click', function() {
			self.disableChat();
		});
		
		$('baddy-list-tab').removeEvents('click');
		$('baddy-list-tab').addEvent('click', function() {
			if ( $('baddy-list').hasClass('closed') ) {
				showBaddyList();
			} else {
				hideBaddyList();
			}
		});
		
		$$('#baddy-list .tab_header').addEvent('click', function(e) {
			if ( e.target.className.indexOf('tab_header') != -1 ) {
				hideBaddyList();
			}
		});
		
		$$('#baddy-list .tab_header .close_button, #baddy-list .tab_header .minimize_button').addEvent('click', function(e) {
			hideBaddyList();
		});
		
		window.addEvent('resize', function() {
			prepareToolbar();
		});
		
		var tabFxInAction = false;
		var tabFx = new Fx.Elements($('tabs_roller'), {
			onComplete: function(){
				tabFxInAction = false;
			}
		});
		
		$('user_tabs_scroll_left').addEvent('click', function() {
			if ( tabsFromLeft > 0 && ! tabFxInAction ) {
				/*-->Hiding all open tabs*/
				$$('.chat_base .user_tab').removeClass('expand');
				$$('.active_chat').addClass('hidden').removeClass('showen');
				/*<--Hiding all open tabs*/
				
				tabFxInAction = true
				var curMargin = $('tabs_roller').getStyle('margin-right').toInt();
				tabFx.start({
					0 : {
						'margin-right' : [curMargin, curMargin - tabWidth]
					}
				});
				
				//$('tabs_roller').setStyle('margin-right', '-' + tabWidth);
				tabsFromLeft -= 1;
				tabsFromRight += 1;
				$$('#user_tabs_scroll_left b')[0].set('html', tabsFromLeft);
				$$('#user_tabs_scroll_right b')[0].set('html', tabsFromRight);
			}
		})
		$('user_tabs_scroll_right').addEvent('click', function() {
			if ( tabsFromRight > 0 && ! tabFxInAction ) {
				/*-->Hiding all open tabs*/
				$$('.chat_base .user_tab').removeClass('expand');
				$$('.active_chat').addClass('hidden').removeClass('showen');
				/*<--Hiding all open tabs*/
				
				tabFxInAction = true;
				var curMargin = $('tabs_roller').getStyle('margin-right').toInt();
				tabFx.start({
					0: {
						'margin-right' : [curMargin, curMargin + tabWidth]
					}
				});
				tabsFromLeft += 1;
				tabsFromRight -= 1;
				$$('#user_tabs_scroll_left b')[0].set('html', tabsFromLeft);
				$$('#user_tabs_scroll_right b')[0].set('html', tabsFromRight);
			}
		});
		
		$('baddy_list_search').addEvent('focus', function() {
			if ( this.get('value') == 'Search' ) {
				this.set('value', '');
			}
		});
		$('baddy_list_search').addEvent('blur', function() {
			if ( this.get('value').length == 0 ) {
				this.set('value', 'Search');
			}
		});
		
		//Search logic
		$('baddy_list_search').addEvent('keyup', function() {
			onlineUsers.each(function(user) {
				if ( ! user.nickName.toLowerCase().contains($('baddy_list_search').get('value')) || ( selfUser.settings.showOnlyEscorts == 1 && user.userType != 'escort' ) ) {
					$('user_' + user.userId).setStyle('display', 'none');
				} else {
					$('user_' + user.userId).setStyle('display', 'block');
				}
			});
		});
		
		$$('#baddy-list .settings_button').addEvent('click', function() {
			if ( this.hasClass('selected') ) {
				hideSettingsList();
			} else {
				showSettingsList();
			}
		});
	}
	
		
	this.chatWith = function(userId)
	{
		if ( userId == selfUser.userId ) return false;
		
		if ( typeof onlineUsersObj[userId] != 'undefined' ) {
			createChatTab(onlineUsersObj[userId], true);
		} else {
			socket.emit('chat-with', {
				userId : userId
			});
		}
	}
	
	this.getStatus = function()
	{
		return selfUser.status;
	}
	
	this.isOnline = function(userId)
	{
		if ( typeof onlineUsersObj[userId] != 'undefined' ) {
			return true;
		} else {
			return false;
		}
	}
	
	this.enableChat = function()
	{
		var self = this;
		availableToolbar();
		forceEnable = true;
		self.connect();
	}
	
	this.disableChat = function()
	{
		var self = this;
		notAvailableToolbar();
		socket.emit('availability-changed', 0);
		//socket.disconnect();
	}
	
	showBaddyList = function()
	{
		$('baddy-list').removeClass('closed');
		$('baddy-list').removeClass('hidden');
		$('baddy-list-tab').addClass('expand');
		$('baddy_list_search').focus();
	}
	
	hideBaddyList = function()
	{
		$('baddy-list').addClass('closed');
		$('baddy-list').addClass('hidden');
		$('baddy-list-tab').removeClass('expand');
		$('baddy-list .')
		
		hideSettingsList();
	}
	
	prepareToolbar = function()
	{
		windowSize = document.getSize().x;
		w = windowSize - 32;//subtract margins from left and right
		$$('.chat_base').setStyle('width', w);
		$('chat-wrapper').setStyle('width', windowSize);
		
		tabSpace = w - 312 - 31 - 70;//subtract buddy list, turn off chat , pre/next  width
		
		tabsCanOpened = Math.floor(tabSpace / tabWidth);
		$('tabs').setStyle('width', tabsCanOpened * tabWidth);
		
		$$('.baddy_listing')[0].setStyle('max-height', document.getSize().y - 100);
		resizeTabBar();
	}
	
	availableToolbar = function()
	{
		$('chat-wrapper').fireEvent('chat-on');
		
		$$('.chat_base.signed_off').addClass('hidden');
		$$('.chat_base.signed_on').removeClass('hidden');
		//$$('.baddy_list').addClass('hidden');
		$$('.baddy_list_tab').removeClass('expand').removeClass('hidden');
		$$('.open_chat ').addClass('hidden');
	}
	
	notAvailableToolbar = function()
	{
		openTabs = 0,
		tabsFromLeft = tabsFromRight = 0;
		
		$('chat-wrapper').fireEvent('chat-off');
		$$('.user_tab').destroy();
		$$('.active_chat').destroy();
		
		$$('.baddy_list').addClass('hidden');
		$$('.chat_base.signed_off').addClass('hidden');
		$$('.baddy_list_tab').addClass('hidden');
		$$('.chat_base.signed_on').removeClass('hidden');
		$$('.open_chat ').removeClass('hidden');
		
		$('user_tabs_scroll_left').setStyle('display', 'none');
		$('user_tabs_scroll_right').setStyle('display', 'none');
	}
	
	signedOffToolbar = function()
	{
		$$('.chat_base.signed_off').removeClass('hidden');
		$$('.chat_base.signed_on').addClass('hidden');
		$$('.open_chat ').addClass('hidden');
	}
	
	
	updateOnlineUsersCount = function(count)
	{
		$('online-users-count').set('html', count);
	}
	
	fillOnlineUsers = function()
	{
		$$('.baddy_listing ul.listing')[0].empty();
		updateOnlineUsersCount(onlineUsers.length)
		onlineUsers.each(function(user) {
			insertUserBuddyList(user);
		});
	}
	
	insertUser = function(userInfo)
	{
		beforeId = false;
		onlineUsers.each(function(u, i) {
			if ( userInfo.nickName.toLowerCase() <= u.nickName.toLowerCase() && ! beforeId ) {
				beforeId = u.userId;
				onlineUsers.splice(i, 0, userInfo); // inserting user in onlineUsers list
			}
		});
		
		if ( ! beforeId ) {
			onlineUsers.push(userInfo);
		}
		
		onlineUsersObj[userInfo.userId] = userInfo;
		//draw user in buddy list before beforeId. 
		//if beforeId is false insert at bottom.
		insertUserBuddyList(userInfo, beforeId);
		updateOnlineUsersCount(onlineUsers.length);
	}
	
	removeUser = function(userId)
	{
		onlineUsers.each(function(u, i) {
			if ( u.userId == userId ) onlineUsers.splice(i, 1);//removing from onlineUsers list
		});
		delete onlineUsersObj[userId];
		
		removeUserBuddyList(userId)//removing from buddy list
		updateOnlineUsersCount(onlineUsers.length);
	}
	
	insertUserBuddyList = function(userInfo, beforeId)
	{	
		li = getUserRow(userInfo);
		if ( beforeId ) {
			li.inject($('user_' + beforeId), 'before');
		} else {
			li.inject($$('.baddy_listing ul.listing')[0]);
		}
		attachUserRowEvents(li);
	}
	
	removeUserBuddyList = function(userId)
	{
		if ( ! $('user_' + userId) ) return;
		$('user_' + userId).destroy();
	}
	
	getUserRow = function(user) 
	{
		var li = new Element('li', {
			id : 'user_' + user.userId,
			'class' : 'bl-' + user.userType
		});
		new Element('img').setProperties({
			'class' :  'user_avatar',
			src	: user.avatar
		}).inject(li);
		new Element('span').setProperties({
			'class' : 'user_name',
			html : user.nickName
		}).inject(li);
		new Element('span').setProperties({
			'class' : 'user_status available'
		}).inject(li);
		
		//If in settings showonlyescorts = 1 set display = none
		if ( user.userType != 'escort' && user.userType != 'agency' && typeof selfUser.settings.showOnlyEscorts != 'undefined' && selfUser.settings.showOnlyEscorts ) {
			li.setStyle('display', 'none');
		}
		
		return li;
	}
	
	sortOnlineUsers = function()
	{
		onlineUsers.sort(function(a, b) {
			if (a.nickName.toLowerCase() < b.nickName.toLowerCase()) {
				return -1;
			}
			if ( a.nickName.toLowerCase() > b.nickName.toLowerCase() ) {
				return 1;
			}

			return 0;
		});
	}

	//When user_row adds in baddy list, adding to this element eventss 
	attachUserRowEvents = function(el)
	{
		el.addEvent('click', function() {
			userId = el.get('id').replace('user_', '');
			createChatTab(onlineUsersObj[userId], true);
		});
	}
	
	
	createChatTab = function(user, isOpened)
	{
		if ( typeof isOpened == 'undefined' ) isOpened = false;
		
		if ( $('user_tab_' + user.userId) ) {
			if ( isOpened ) {
				openUserTab(user.userId);
			}
		} else {
			//Emiting about opened dialog
			socket.emit('dialog-opened', {
				userId: user.userId
			});
			
			shortNickName = user.nickName;
			if ( shortNickName.length >= 12) {
				shortNickName = shortNickName.substr(0, 12) + '...';
			}
			
			/*-->drawing chat tab*/
			chatInnerButton = new Element('div', {
				'class' : 'chat_inner_button'
			});
			userTabName = new Element('span', {
				'class': 'user_tab_name'
			});

			new Element('b', {
				'html': shortNickName
			}).inject(userTabName);

			userTabName.inject(chatInnerButton);

			new Element('div', {
				'class' : 'user_status'
			}).inject(chatInnerButton);

			new Element('div', {
				'class' : 'close_tab cpoint'
			}).inject(chatInnerButton);
			
			userTabClass = 'user_tab cpoint';
			if ( user.status == 'online' ) {
				userTabClass += ' available';
			}
			if ( user.status == 'offline' ) {
				userTabClass += ' offline'
			}
			
			userTab = new Element('div', {
				'class' : userTabClass,
				'id'	: 'user_tab_' + user.userId
			});

			chatInnerButton.inject(userTab);
			/*<--drawing chat tab*/
			
			/*-->drawing chat opened window*/
			activeChat = new Element('div', {
				'class' : 'active_chat hidden',
				'id'	: 'active_chat_' + user.userId
			});
			tabHeader = new Element('div', {
				'class' : 'tab_header cpoint'
			});
			
			shortNickName = user.nickName;
			if ( shortNickName.length >= 20) {
				shortNickName = shortNickName.substr(0, 20) + '...';
			}
			
			if ( typeof user.link != 'undefined' && user.link.length ) {
				new Element('a', {
					'class' : 'tab_name',
					href : user.link,
					html: shortNickName
				}).inject(tabHeader);
			} else {
				new Element('span', {
					'class' : 'tab_name',
					'html' : shortNickName
				}).inject(tabHeader);
			}
			
			
			new Element('div', {
				'class' : 'close_button showen cpoint'
			}).inject(tabHeader);
			
			new Element('div', {
				'class' : 'settings_button cpoint'
			}).inject(tabHeader);
			
			new Element('div', {
				'class' : 'minimize_button cpoint'
			}).inject(tabHeader);
			
			tabHeader.inject(activeChat);
			
			
			/*-->Drawing user settings list*/
			var settingsList = new Element('div', {
				'class' : 'settings_list'
			});
			var ul = new Element('ul').inject(settingsList);
			
			userTabSettings.each(function(obj) {
				new Element('span', {html : obj.name}).inject(
					new Element('a', {'class': obj.id}).inject(
						new Element('li', {'class' : obj.type}).inject(ul)
				))
			});
			settingsList.inject(activeChat);
			/*<--Drawing user settings list*/
			
			
			activeChatContent = new Element('div', {
				'class' : 'active_chat_content'
			});
			
			new Element('div', {
				'class' : 'messaging'
			}).inject(activeChatContent);
			
			var textareaWrapper = new Element('div', {
				'class' : 'textarea_wrapper'
			});
			
			var textareaInner = new Element('div', {
				'class' :  'textarea_inner'
			});
			textareaWrapper.inject(textareaInner);
			
			new Element('textarea', {
				'cols' : '30',
				'rows' : '10'
			}).inject(textareaWrapper);
			
			//Tmp solution. ff can't getScroll if overflow hidden
			if ( browser == 'firefox' ) {
				textareaWrapper.getElement('textarea').setStyle('overflow', 'auto');
			}
			
			emotionsWrapper = new Element('div', {
				'class' : 'emotions_wrapper'
			});
			
			new Element('span', {
				'class' : 'emotions_btn'
			}).inject(emotionsWrapper);
			
			emotionsWrapper.inject(textareaInner);
			
			textareaInner.inject(activeChatContent);
			
			activeChatContent.inject(activeChat);
			
			userTab.inject($('tabs_roller'));
			activeChat.inject($('chat-container'));
			
			/*<--drawing chat opened window*/
			
			
			if ( isOpened ) {
				openUserTab(user.userId);
			}
			
			resizeTabBar('add');
			
			attachUserTabEvents(user.userId);
		}
	}
	
	//Attaching events to a new user tab
	attachUserTabEvents = function(id)
	{	
		var activeChat = $('active_chat_' + id);
		var textarea = activeChat.getElement('textarea');
		
		$$('#user_tab_' + id + ' .close_tab, #active_chat_' + id + ' .close_button').addEvent('click', function() {
			closeUserTab(id);
		});
		
		$$('#active_chat_' + id + ' .tab_header, #user_tab_' + id ).addEvent('click', function(e) {
			if ( e.target.className.indexOf('tab_name') >= 0 || e.target.className.indexOf('settings_button') >= 0 ) {
				return;
			}
			
			if ( $('user_tab_' + id).hasClass('expand') ) {
				hideUserTab(id);
			} else {
				openUserTab(id);
			}
		});

		
		var typing = false,
		timer;
		
		textarea.addEvent('keypress', function(e) {
			/*--> Detect typing start/end */
			if ( ! typing ) {
				socket.emit('typing-start', {
					userId : id
				});
				typing = true;
			}
			
			clearTimeout(timer);
			timer = setTimeout(function(){ 
				socket.emit('typing-end', {
					userId : id
				});
				typing = false;
			}, 3*1000);
			/*<-- Detect typing start/end */
			
			if ( e.code == 13 && ! e.shift ) {
				e.stop();
				
				message = this.get('value');
				
				socket.emit('message-sent', {
					userId : id,
					message: message
				});
				
				//After enter empty text area and set default height
				this.set('value', '');
				this.setStyle('height', 18);
				
				message = {
					body : message,
					date : new Date().getTime(),
					userId : selfUser.userId
				}
				
				addMessage(id, selfUser, message);
				
				//If client is sending message and participant is offline show notification
				if ( $('user_tab_' + id).hasClass('offline') ) {
					showMessage(id, messages.offlineMessage);
				}
			}
			
			
			/*-->dynamically increasing textarea height*/
			scrollSize = this.getScroll().y;
			height = this.getHeight();
			if ( scrollSize ) {
				offset = height + scrollSize;
				this.setStyle('height', offset);
				this.getParent('.textarea_wrapper').scrollTo(0, offset);
				if ( this.getParent('.textarea_wrapper').getHeight() > 100 ) {
					activeChat.getElement('.emotions_wrapper').setStyle('right', 13);
				}
			}
			/*<--dynamically increasing textarea height*/
		});
		
		/*-->User settings events*/
		activeChat.getElement('.block-user').addEvent('click', function() {
			blockUser(id);
		});
		/*<--User settings events*/
		
		$$('#active_chat_' + id).getElement('.emotions_btn').addEvent('click', function() {
			if ( this.hasClass('opened') ) {
				hideEmotionsTab(id);
			} else {
				openEmotionsTab(id);
			}
			
		});
		
		$$('#active_chat_' + id + ' .settings_button').addEvent('click', function() {
			if ( this.hasClass('selected') ) {
				hideUserTabSettingsList(id);
			} else {
				showUserTabSettingsList(id);
			}
		});
		
	}
	
	showMessage = function(id, message)
	{
		activeChatContent = $('active_chat_' + id ).getElement('.active_chat_content');
		
		if ( ! activeChatContent.getElement('.message_box') ) {
			messageBox = new Element('div', {
				'class' : 'message_box'
			});
			messageBox.inject(activeChatContent);
		}
		messageBox.set('html', message);
		
		isAlreadyOpened = false;
		var messageFx = new Fx.Elements(messageBox, {
			onComplete: function(){
				if ( ! isAlreadyOpened ) {
					isAlreadyOpened = true;
					setTimeout(function() {
						messageFx.start({
							0 : {
								top: [0, (-1) * $$('.message_box')[0].getSize().y]
							}
						});
					}, 5 * 1000);
				}
			}
		}).start({
			0 : {
				top: [-1 * $$('.message_box')[0].getSize().y, 0]
			}
		});
		
	}
	
	//Opening user tab for messaging by closing all others
	openUserTab = function(id, scroll)
	{
		if ( typeof scroll == 'undefined' ) scroll = true;
		
		socket.emit('dialog-activated', {
			userId: id
		});
		
		//first Closing all opened tabs
		$$('.chat_base .user_tab').removeClass('expand');
		$$('.active_chat').addClass('hidden').removeClass('showen');
		
		var tabFx = new Fx.Elements($('tabs_roller'), {
			onComplete: function(){
				showUserTab(id);
			}
		});
		
		/*-->Scrolling tabs to the right position*/
		var index;
		$$('.user_tab').each(function(el, i) {
			if ( el.get('id') == 'user_tab_' + id ) {
				index = i;
			}
		});
		
		//If in visible part just calling showUserTab
		if ( index >= tabsFromRight && index < tabsFromRight + tabsCanOpened ) {
			showUserTab(id);
			return;
		}
		
		if ( ! scroll ) return;//if no need scroll just return
		
		curMargin = $('tabs_roller').getStyle('margin-right').toInt();
		
		//If tab is on the right side scroll then open
		if ( index < tabsFromRight   ) {
			tabFx.start({
				0 : {
					'margin-right' : [curMargin, (-1) * index * tabWidth]
				}
			});
			
			s = tabsFromRight - index;//tabs count shifted to the left
			tabsFromLeft += s;
			tabsFromRight -= s;
		}
		
		//if tab is on the left side
		if ( index >= tabsCanOpened + tabsFromRight ) {
			s = (index + 1) - tabsCanOpened - tabsFromRight;
			tabsFromLeft -= s;
			tabsFromRight += s;
			
			tabFx.start({
				0 : {
					'margin-right' : [curMargin, curMargin -  (s * tabWidth)]
				}
			});
		}
		
		$$('#user_tabs_scroll_left b')[0].set('html', tabsFromLeft);
		$$('#user_tabs_scroll_right b')[0].set('html', tabsFromRight);
		
	/*<--Scrolling tabs to the right position*/
	}
	
	showUserTab = function(id)
	{
		userTab = $('user_tab_' + id);
		activeChat = $('active_chat_' + id);
		
		userTab.addClass('expand');
		
		offset = windowSize - $('user_tab_' + id).getPosition().x - tabWidth - 1;//1px for borders
		activeChat.setStyle('right', offset);
		
		activeChat.removeClass('hidden').addClass('showen');
		
		activeChat.getElement('.messaging').scrollTo(0, activeChat.getElement('.messaging').getScrollSize().y);
		activeChat.getElement('textarea').focus();
		
		
		if ( userTab.hasClass('new_message') ) {
			userTab.removeClass('new_message');
			userTab.getElement('.new_messages_count').destroy();
			
			socket.emit('conversation-read', {
				userId : id
			});
		}
	}
	
	//Closing user tab
	hideUserTab = function(id)
	{
		socket.emit('dialog-deactivated', {
			userId: id
		});
		$('user_tab_' + id).removeClass('expand');
		$('active_chat_' + id).removeClass('showen').addClass('hidden');
		$('active_chat_' + id).setStyle('right', '');
		
		hideUserTabSettingsList(id);
	}
	
	getMessageRow = function(userInfo)
	{
		var userMessage = new Element('div', {
			'class' : 'user_message ' + userInfo.userId
		})
		var userAvatar = new Element('div', {
			'class' : 'user_avatar'
		});
		new Element('img', {
			src : userInfo.avatar,
			width: 32,
			height: 32
		}).inject(userAvatar);
		userAvatar.inject(userMessage);
		new Element('div', {
			'class' : 'messages'
		}).inject(userMessage);
		new Element('div', {
			'class': 'clear'
		}).inject(userMessage);
		
		return userMessage
	}
	
	addMessage = function(id, senderInfo, message)
	{
		
		//first of all clearing message
		message.body = clearMessage(message.body);
		
		if ( message.body.length == 0 ) return;
		
		userTab = $('user_tab_' + id);
		activeChat = $('active_chat_' + id);
		
		//If tab opened and is not self message emit message-read
		if ( userTab.hasClass('expand') && senderInfo.userId != selfUser.userId ) {
			socket.emit('conversation-read', {
				userId : senderInfo.userId
			});
		}
		
		if ( typeof isNewMessage == 'undefined' ) {
			isNewMessage = false;
		}
		if ( typeof newMessagesCount == 'undefined' ) {
			newMessagesCount = 1;
		}
		
		lastUserMessage = activeChat.getElements('.user_message').getLast();
		
		
		//if messaging is empty or last message is not his, create new user_message raw
		//else add to existing one
		if ( lastUserMessage && ( lastUserMessage.hasClass(message.userId) && message.date - parseInt(lastUserMessage.get('id')) <= 60*1000 ) ) {
			new Element('p', {
				'html': message.body
			}).inject(lastUserMessage.getElement('.messages'));
		} else {
			if ( message.userId == selfUser.userId ) {
				userMessage = getMessageRow(selfUser);
			} else {
				userMessage = getMessageRow(senderInfo);
			}
			
			
			
			new Element('span', {
				'class' : 'date',
				'html' : getTimeForMessage(new Date(message.date))
			}).inject(userMessage);
			userMessage.set('id', message.date);
			
			new Element('p', {
				'html': message.body
			}).inject(userMessage.getElement('.messages'));

			userMessage.inject(activeChat.getElement('.messaging'));

			//Adding events to userMessage
			userMessage.addEvent('mouseover', function() {
				this.getElement('.date').setStyle('display', 'inline');
			});
			userMessage.addEvent('mouseout', function() {
				this.getElement('.date').setStyle('display', 'none');
			});
		}


		//scroll down messaging
		activeChat.getElement('.messaging').scrollTo(0, activeChat.getElement('.messaging').getScrollSize().y);
	}
	
	markTabUnread = function(id, unreadMessagesCount)
	{
		userTab = $('user_tab_' + id);
		if ( userTab.hasClass('expand') ) return; //Mark only closed tabs
		
		if ( typeof unreadMessagesCount == 'undefined' ) unreadMessagesCount = 1;
		
		userTab.addClass('new_message');
		if ( ! userTab.getElement('.new_messages_count') ) {
			new Element('div', {
				'class' : 'new_messages_count',
				html : unreadMessagesCount
			}).inject(userTab.getElement('.chat_inner_button'));
		} else {
			count = parseInt(userTab.getElement('.new_messages_count').get('html'));
			count += unreadMessagesCount;
			userTab.getElement('.new_messages_count').set('html', count);
		}
	}
	
	fillHistory = function(senderInfo, messages)
	{
		userTab = $('user_tab_' + senderInfo.userId);
		activeChat = $('active_chat_' + senderInfo.userId);
		
		activeChat.getElement('.messaging').empty();
		if ( userTab ) {
			messages.each(function(message) {
				addMessage(senderInfo.userId, senderInfo, message)
			});
		}
	}
	
	getTimeForMessage = function(date)
	{
		curDate = new Date();
		
		hours = date.getHours();
		minutes = ('0' + date.getMinutes()).slice(-2);//two number format
		
		d = hours + ':' + minutes;
		
		if ( curDate.getDate() != date.getDate() ) {
			d += ' ' + date.getDate() + 'th ' + monthNames[date.getMonth()];
		}
		
		if ( curDate.getYear() != date.getYear() ) {
			d += ' ' + date.getYear();
		}
		
		return d;
	}
	
	clearMessage = function(message)
	{
		//htmlentities
		message = htmlEntities(message);
		
		//Replacing one more whitespaces
		message = message.trim();
		message = message.replace(/\s+/g, ' ');
		
		//replacing urls with html links
		var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
		message = message.replace(exp,"<a href='$1' target='_blank'>$1</a>");
		
		message = replaceEmotions(message);
		
		return message;
	}
	
	htmlEntities = function(str) {
		return String(str).replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/&/g, '&amp;');
	}
	
	//Open smiles list
	openEmotionsTab = function(id)
	{
		activeChat = $('active_chat_' + id);
		textarea = activeChat.getElement('textarea');
		
		activeChat.getElement('.emotions_btn').addClass('opened');
		if ( activeChat.getElement('.emotions_container') ) {
			activeChat.getElement('.emotions_container').setStyle('display', 'block');
			return;
		}
		
		emotionsContainer = new Element('div', {
			'class' : 'emotions_container'
		});
		for(i in emotions) {
			new Element('span', {
				'class' : 'emotions emotions_' + emotions[i],
				'rel' : i
			}).inject(emotionsContainer);
		}
		for(i in customEmotions) {
			new Element('span', {
				'class' : 'emotions emotions_' + customEmotions[i],
				'rel' : i
			}).inject(emotionsContainer);
		}
		emotionsContainer.inject(activeChat.getElement('.emotions_wrapper'));
		
		//Adding events to the emotions spans
		activeChat.getElements('.emotions_container span').addEvent('click', function() {
			textarea.set('value', textarea.get('value') + this.get('rel'));//Adding smile to text
			textarea.focus();
			hideEmotionsTab(id);
		});
	}
	
	hideEmotionsTab = function(id)
	{
		activeChat.getElement('.emotions_container').setStyle('display', 'none');
		activeChat.getElement('.emotions_btn').removeClass('opened');
	}
	
	replaceEmotions = function(message)
	{
		//First replacing custom emotions
		for ( i in customEmotions) {
			
			//For smiles cleaned by htmlentities
			while(message.indexOf(htmlEntities(i)) != -1) {
				message = message.replace(htmlEntities(i), '<span class="emotions emotions_' + customEmotions[i] + '"></span>');
			}
			
			while( message.indexOf(i) != -1 ) {
				message = message.replace(i, '<span class="emotions emotions_' + customEmotions[i] + '"></span>');
			}
		}
		for ( i in emotions ) {
			//small fix for. for this smile :/ (http://)
			if ( i == ':/' && (message.indexOf('http://') != -1 || message.indexOf('https://') != -1) ) {
				continue;
			}
			
			while(message.indexOf(i) != -1) {
				message = message.replace(i, '<span class="emotions emotions_' + emotions[i] + '"></span>');
			}
			
			//For smiles cleaned by htmlentities
			while(message.indexOf(htmlEntities(i)) != -1) {
				message = message.replace(htmlEntities(i), '<span class="emotions emotions_' + emotions[i] + '"></span>');
			}
		}
		return message;
	}
	
	function RegExpEscape(text) {
		return text.replace(/[\\=!^$*+?.:|(){}[\]]/g, "\\$&");
	}
	
	resizeTabBar = function(action)
	{
		if ( typeof action == 'undefined' ) action = 'resize';

		//getting open tab id and hidding it
		var openDialogId = false;
		if ( $$('.user_tab.expand').length ) {
			openDialogId = $$('.user_tab.expand')[0].get('id').replace('user_tab_', '');
			$('user_tab_' + openDialogId).removeClass('expand');
			$('active_chat_' + openDialogId).removeClass('showen').addClass('hidden');
		}
	
		if ( action == 'add' ) {
			openTabs += 1;
			if ( openTabs > tabsCanOpened ) {
				tabsFromLeft += 1;
				$$('#user_tabs_scroll_left b')[0].set('html', tabsFromLeft);
				$('user_tabs_scroll_left').setStyle('display', 'block');
				$('user_tabs_scroll_right').setStyle('display', 'block');
			}
		}
		
		if ( action == 'remove' ) {
			
			if ( openTabs > tabsCanOpened ) {
				if ( tabsFromLeft > 0 ) {
					tabsFromLeft -= 1;
				} else {
					if ( tabsFromRight > 0 ) {
						tabsFromRight -= 1;
						curMargin = $('tabs_roller').getStyle('margin-right').toInt();
						$('tabs_roller').setStyle('margin-right', curMargin + tabWidth);
					}
				}
				
				$$('#user_tabs_scroll_left b')[0].set('html', tabsFromLeft);
				$$('#user_tabs_scroll_right b')[0].set('html', tabsFromRight);
			}
			openTabs -= 1;
			if ( openTabs <= tabsCanOpened ) {
				$$('#user_tabs_scroll_right b')[0].set('html', 0);
				$$('#user_tabs_scroll_left b')[0].set('html', 0);
				$('user_tabs_scroll_left').setStyle('display', 'none');
				$('user_tabs_scroll_right').setStyle('display', 'none');
			}
		}
		
		if ( action == 'resize' ) {
			if ( openTabs > tabsCanOpened ) {

				diff = openTabs - tabsCanOpened;

				if ( tabsFromRight > tabsFromLeft ) {
					$$('#user_tabs_scroll_right b')[0].set('html', diff);
					$$('#user_tabs_scroll_left b')[0].set('html', 0);
					$('tabs_roller').setStyle('margin-right', (-1) * diff * tabWidth);
					tabsFromRight = diff;
					tabsFromLeft = 0;
				} else {
					$$('#user_tabs_scroll_left b')[0].set('html', diff);
					$$('#user_tabs_scroll_right b')[0].set('html', 0);
					$('tabs_roller').setStyle('margin-right', '');
					tabsFromLeft = diff;
					tabsFromRight = 0;
				}
				$('user_tabs_scroll_left').setStyle('display', 'block');
				$('user_tabs_scroll_right').setStyle('display', 'block');
			} else {
				if ( tabsFromLeft > 0 ) {
					$$('#user_tabs_scroll_left b')[0].set('html', 0);
				}
				if ( tabsFromRight > 0 ) {
					$('tabs_roller').setStyle('margin-right', '');
					$$('#user_tabs_scroll_right b')[0].set('html', 0);
				}
				$('user_tabs_scroll_left').setStyle('display', 'none');
				$('user_tabs_scroll_right').setStyle('display', 'none');
			}
		}
		
		//Afer changes open it back
		if ( openDialogId ) {
			openUserTab(openDialogId, false);
		}
	}
	
	blinkPageTitle = function(message)
	{
		if ( ! originalTitle.length ) return;
		if ( typeof blinkTimer != 'undefined' ) clearInterval(blinkTimer);
		blinkTimer = setInterval(function() {
			if ( document.title == message ) {
				document.title = originalTitle;
			} else {
				document.title = message;
			}
		}, 1000);
		window.addEvent('focus', function() {
			if ( typeof blinkTimer != 'undefined' ) clearInterval(blinkTimer);
			document.title = originalTitle;
			this.removeEvents('click');
		});
	}
	
	prepareSettings = function()
	{
		if ( typeof selfUser.settings == 'undefined' ) selfUser.settings = {};
		
		if ( typeof selfUser.settings.showOnlyEscorts != 'undefined' && selfUser.settings.showOnlyEscorts ) {
			$('show-only-escorts').getParent('li').addClass('checked');
		}
		if ( typeof selfUser.settings.keepListOpen != 'undefined' && selfUser.settings.keepListOpen ) {
			$('keep-list-open').getParent('li').addClass('checked');
			showBaddyList();
		}
		if ( typeof selfUser.settings.invisibility != 'undefined' && selfUser.settings.invisibility ) {
			$('invisibility').getParent('li').addClass('checked');
			showBaddyList();
		}
		
		$('show-only-escorts').addEvent('click', function() {
			var li = this.getParent('li');
			if ( li.hasClass('checked') ) {
				li.removeClass('checked');
				$$('#baddy-list .listing li.bl-member').setStyle('display', 'block');

				selfUser.settings.showOnlyEscorts = 0;
				socket.emit('change-options', {option: 'show-only-escorts', value : 0});
			} else {
				li.addClass('checked');
				$$('#baddy-list .listing li.bl-member').setStyle('display', 'none');

				selfUser.settings.showOnlyEscorts = 1;
				socket.emit('change-options', {option: 'show-only-escorts', value : 1});
			}
		});
		
		$('keep-list-open').addEvent('click', function() {
			var li = this.getParent('li');
			if ( li.hasClass('checked') ) {
				li.removeClass('checked');

				selfUser.settings.keepListOpen = 0;
				socket.emit('change-options', {option: 'keep-list-open', value : 0});
			} else {
				li.addClass('checked');

				selfUser.settings.keepListOpen = 1;
				socket.emit('change-options', {option: 'keep-list-open', value : 1});
			}
		});
		
		$('invisibility').addEvent('click', function() {
			var li = this.getParent('li');
			if ( li.hasClass('checked') ) {
				li.removeClass('checked');

				selfUser.settings.invisibility = 0;
				socket.emit('change-options', {option: 'invisibility', value : 0});
			} else {
				li.addClass('checked');

				selfUser.settings.keepListOpen = 1;
				socket.emit('change-options', {option: 'invisibility', value : 1});
			}
		});
		
		$('manage-block-list').addEvent('click', function() {
			$('baddy-list').getElement('.settings_list ul').setStyle('display', 'none');
			$('baddy-list').getElement('.settings_list .unblock_list').setStyle('display', 'block');
			
			socket.emit('blocked-users');
		});
		
		$('unblock-user-btn').addEvent('click', function() {
			var userId = $('blocked-users-list').getSelected()[0].get('value');
			if ( userId ) {
				$('blocked-users-list').getSelected().destroy();
				selfUser.settings.blockedUsers.splice(selfUser.settings.blockedUsers.indexOf(userId), 1);
				socket.emit('unblock-user', {
					userId: userId
				});
			}
		});
	}
	
	hideSettingsList = function()
	{
		$('baddy-list').getElement('.settings_button').removeClass('selected');
		$$('#baddy-list .settings_list').setStyle('display', 'none');
		
		$$('#baddy-list .settings_list ul').setStyle('display', 'block');
		$$('#baddy-list .settings_list .unblock_list').setStyle('display', 'none');
	}
	showSettingsList = function()
	{
		$('baddy-list').getElement('.settings_button').addClass('selected');
		$$('#baddy-list .settings_list').setStyle('display', 'block');
	}
	
	hideUserTabSettingsList = function(id)
	{
		$('active_chat_' + id).getElement('.settings_button').removeClass('selected');
		$('active_chat_' + id).getElement('.settings_list').setStyle('display', 'none');
	}
	showUserTabSettingsList = function(id)
	{
		$('active_chat_' + id).getElement('.settings_button').addClass('selected');
		$('active_chat_' + id).getElement('.settings_list').setStyle('display', 'block');
	}
	
	closeUserTab = function(id)
	{
		$('user_tab_' + id).destroy();
		$('active_chat_' + id).destroy();
		resizeTabBar('remove');
		socket.emit('dialog-closed', {
			userId: id
		});
	}
	
	blockUser = function(id)
	{
		closeUserTab(id);
		socket.emit('block-user', {
			userId: id
		});
		
		selfUser.settings.blockedUsers.push(id);
		removeUser(id);
	}
	
	initAudio = function()
	{
		if ( "Audio" in window && browser.indexOf('safari') == -1 ){
			audio = new Audio();
			if( !!(audio.canPlayType && audio.canPlayType('audio/ogg; codecs="vorbis"').replace(/no/, '')) )
				audio.ext = 'ogg';
			else if( !!(audio.canPlayType && audio.canPlayType('audio/mpeg;').replace(/no/, '')) )
				audio.ext = 'mp3';
			else if( !!(audio.canPlayType && audio.canPlayType('audio/mp4; codecs="mp4audio.40.2"').replace(/no/, '')) )
				audio.ext = 'm4a';
			else
				audio.ext = 'mp3';

			return;
		}

	}
	
	playAudio = function(name)
	{
		if ( audio && audio.play ) {
			audio.src = "/sounds/" + name + '.' + audio.ext;
			audio.play()
		}
	}
	
	prepareToolbar();
	this.connect();
	this.initListeners();
	this.initEvents();
	initAudio();
}
