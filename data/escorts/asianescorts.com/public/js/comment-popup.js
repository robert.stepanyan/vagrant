/* --> Feedback */
Cubix.CommentPopup = {};


Cubix.CommentPopup.inProcess = false;

Cubix.CommentPopup.url = '';

Cubix.CommentPopup.Show = function (box_height, box_width, comment_id,escort_id) {
	if ( Cubix.CommentPopup.inProcess ) return false;

	var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000' });
	page_overlay.disable();

	var y_offset = 30;

	var container = new Element('div', { 'class': 'CommentPopup-wrapper'}).setStyles({
		left: window.getWidth() / 2 - box_width / 2,
		top:  window.getHeight() / 2 - box_height / 2 + window.getScroll().y - y_offset+130,
		opacity: 0,
        position: 'absolute',
        'z-index': 101,
        background: '#fff'
	}).inject(document.body);
	
	Cubix.CommentPopup.inProcess = true;

	new Request({
		url: Cubix.CommentPopup.url,
		method: 'get',
		onSuccess: function (resp) {
			
			Cubix.CommentPopup.inProcess = false;
			container.set('html', resp);

			var close_btn = new Element('div', {
				html: '',
				'class': 'popup-close-btn-v2'
			}).inject(container);

			close_btn.addEvent('click', function() {
				$$('.CommentPopup-wrapper').destroy();
				page_overlay.enable();
			});

			container.tween('opacity', '1');
			$('comment_id').set('value',comment_id);
			$('escort_id').set('value',escort_id);
			var forms = container.getElements('form');
			forms.addEvent('submit', Cubix.CommentPopup.Send);
			forms.each(function (form) { form.set('action', form.get('action') + (form.get('action').indexOf('?') != -1 ? '&' : '?') + 'ajax'); });
		}
	}).send();
		
	
	return false;
}

Cubix.CommentPopup.Send = function (e) {
	e.stop();
	var overlay = new Cubix.Overlay($$('.CommentPopup-wrapper')[0], { loader: _st('loader-small.gif'), position: '50%', no_relative: true });
	overlay.disable();
	function getErrorElement(el) {
		var error = el.getPrevious('.error-profile');
		if ( error ) return error;
		var target = el;
		return new Element('div', { 'class': 'error-profile' }).inject(target, 'before');
	}
	this.set('send', {
		onSuccess: function (resp) {
			this.getElements('.error-profile').destroy();
			resp = JSON.decode(resp);
			overlay.enable();

			if ( resp.status == 'error' ) {
				for ( var field in resp.msgs ) {
					var input = this.getElement('*[name=' + field + ']');
					getErrorElement(input).set('html', resp.msgs[field]);
				}
			}
			else if ( resp.status == 'success' ) {
				$$('.CommentPopup-wrapper').destroy();
				$$('.overlay').destroy();
				
         }
		}.bind(this)
	});

	this.send();
}