/* --> Feedback */
Cubix.Feedback = {};

Cubix.Feedback.url = ''; // Must be set from php

Cubix.Feedback.inProcess = false;

Cubix.Feedback.Show = function (element, escort_id) {
	if ( Cubix.Feedback.inProcess ) return false;
	
	var old_container = element.getElement('.contact-wrapper');
	
	if ( old_container ) {
		old_container.destroy();
		return false;
	}
	
	var container = new Element('div', { 'class': 'contact-wrapper' }).inject(element);
	
	Cubix.Feedback.inProcess = true;
	new Request({
		url: Cubix.Feedback.url + '?to=' + ($defined(escort_id) ? escort_id : 'support') + '&ajax',
		method: 'get',
		onSuccess: function (resp) {
			Cubix.Feedback.inProcess = false;
			container.set('html', resp);
			
			var form = container.getElement('form');
			form.addEvent('submit', Cubix.Feedback.Send);
			form.set('action', form.get('action') + '?ajax');
		}
	}).send();
	
	
	
	return false;
}

Cubix.Feedback.Send = function (e) {
	e.stop();
	
	function getErrorElement(el) {
		var error = el.getNext('.error');
		if ( error ) return error;
		
		return new Element('div', { 'class': 'error' }).inject(el, 'after');
	}
	
	this.set('send', {
		onSuccess: function (resp) {
			resp = JSON.decode(resp);
			
			this.getElements('.error').destroy();
			this.getElements('.invalid').removeClass('invalid');
			
			if ( resp.status == 'error' ) {
				for ( var field in resp.msgs ) {
					var input = this.getElement('*[name=' + field + ']');
					input.addClass('invalid');
					getErrorElement(input).set('html', resp.msgs[field]);
				}
				
				// Regenerate new captcha
				var img = this.getElement('img'),
					src = img.get('src');
				
				src = src.substr(0, src.indexOf('?'));
				src = src + '?' + Math.random();
				
				img.set('src', src);
			}
			else if ( resp.status == 'success' ) {
				this.getParent().set('html', resp.msg);
			}
		}.bind(this)
	});
	
	this.send();
}
/* <-- */
