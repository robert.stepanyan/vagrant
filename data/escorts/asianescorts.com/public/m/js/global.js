MCubix = {};

MCubix.ErrorsHandler = {
  elm: null,

    messages: {
        'have_errors': 'Exists error !',
        'city_count': 'City count',
        'city_count_test': 'City count',
        'year': 'Year !',
        'month': 'Month must be between 1 and 12',
        'day': 'Day must be between 1 and 31',
        'day_31': 'Self month doesn\'t have 31 days!',
        'day_feb': 'February doesn\'t have that many days!',
        'date_out': 'Please select currect date'
    },

    doShow: function( elm, key, dict ){
        var self = this;
        
        if( !dict ) {
            elm.set('html', self.messages[key]).show();
        } else {            
            elm.set('html', key).show();
        }

        setTimeout(function() {
            elm.set('html', '').hide();
        }, 3000 );
    },

    doHide: function(elm){
        elm.set('html', '').hide();
    }
}