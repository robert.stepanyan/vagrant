/*jslint es6*/
/*jslint node*/

'use strict';

const MySQL = require('mysql');
const validator = require('validator');
const config = require('../configs').get('production');
const { toMySQLFormat } = require("../utils/");

class Bookings {
  constructor() {
    this.establishMySQLConnection();

    setInterval(() => {
      this.MySQLClient.ping(err => {
        if (err) {
          this.handleMySQLConnectionerr(err);
        }
      });
    }, 5000);
  }

  establishMySQLConnection() {
    this.MySQLClient = MySQL.createConnection({
      user: config.db.user,
      database: config.db.database,
      password: config.db.password,
      host: config.db.host,
      port: 3306
    });

    this.MySQLClient.connect(err => { });
  }

  handleMySQLConnectionerr(err) {
    if (!err.fatal) {
      return false;
    }

    if (err.code !== 'PROTOCOL_CONNECTION_LOST') {
      console.log(
        'The mysql library fired a PROTOCOL_CONNECTION_LOST exception'
      );

      throw err;
    }

    console.log('Re-connecting lost connection: ' + err.stack);
    this.establishMySQLConnection();
  }

  addBookingRequestPromise(data) {
    return new Promise((resolve, reject) => {
    let err = {};

    if (!validator.isNumeric(data.hour.toString())) {
      err.hour = 'hours must be numeric';
    }

    if (!validator.isNumeric(data.min.toString())) {
      err.min = 'minutes must be numeric';
    }

    if (!data.duration || !validator.isNumeric(data.duration.toString())) {
      err.duration = 'duration must be numeric';
    }

    if (!data.location || (data.location.toString() !== "incall" && data.location.toString() !== "outcall")) {
      err.duration = "location must be either incall or outcall";
    }

    if (!data.phone || !data.phone.toString().match(/[\d ]* ?/g)) {
      err.phone = 'invalid phone number';
    }

    if (!data.wait_time || !validator.isNumeric(data.wait_time.toString())) {
      err.wait_time = 'wait time must be numeric';
    }
    if (data.contact_type && Array.isArray(data.contact_type)) {
      let filtered = [];

      data.contact_type.forEach(cType => filtered.concat(['phone_call', 'sms', 'viber', 'whatsapp'].includes(cType)));

      if (filtered.length) {
        err.contact_type = 'contact types must be one of following: phone_call, sms, viber, whatsapp';
      } else {
        data.contact_type = data.contact_type.join(',');
      }
    } else {
      err.contact_type = 'at least one of contact types have to be selected';
    }

    if (!data.escort_id) {
      err.escort_id = 'escort id must be numeric';
    }

    if (Object.keys(err).length) {
      return resolve({status: 'error', msg: err });
    }

    data.request_date = toMySQLFormat(new Date());
    data.status = 'pending';

    let _query = `INSERT INTO instant_book_requests SET ?`;

      this.MySQLClient.query(_query, data, (error, result, fields) => {

        if (error) {
          return reject(error);
        }

        return resolve({status: 'success', msg: fields, insertId: result.insertId});
      });
    });
  }

  updateBookingRequestStatusPromise(data) {
    return new Promise((resolve, reject) => {
      let _data = {
        status: data.status
      };

      let _query = `UPDATE instant_book_requests SET ? WHERE id = ${data.id}`;

      this.MySQLClient.query(_query, _data, (err, result) => {
        if (err) return reject(err);

        return resolve(result);
      });
    });

  }
}

module.exports = new Bookings();