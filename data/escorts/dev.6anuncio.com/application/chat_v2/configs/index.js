/*jslint es6*/
/*jslint node*/

'use strict';

module.exports = (() => {
  const configs = {
    production: {
      common: {
        applicationId: 17,
        listenPort: 8877,
        host: 'www.6anuncio.com',
        authPath: '/get_user_info.php',
        sessionCacheTime: 1000 * 60 * 10, //10 minutes
        sessionCheckInterval: 1 * 60 * 60 * 1000, //1 hour
        imagesHost: 'https://pic.6anuncio.com/6anuncio.com/',
        avatarSize: 'lvthumb',
        noPhotoUrl: '/img/chat/img-no-avatar.gif',
        userSettingsBackupPath: 'user_settings.bk',
        key: 'JFusLsdf8A9',
        blWordsLifeTime: 12 * 60 * 60 * 1000, //12 hours
        redisBlWordsDb: 0,
        redisBlWordsKey: 'bl-words-ef'
      },
      pushNotifications: {
        auth: 'ul46sers9wf9u12nz40mgyj6weiybjpo:3kfa34irl0h3a75jf4rpjxpeaj5pemhm',
        url: 'https://dashboard.goroost.com/api/v2/notifications'
      },
      db: {
        user: '6a_cubix',
        database: '6anuncio_com_br',
        password: 'sdfsretsDH584GF',
        host: 'backend.6anuncio.com',
		port: 3306
      },
      redis: {
        host: '127.0.0.1',
        port: 6379
      },
      memcache: {
        host: '/run/memcached/memcached.sock',
        port: 0
      }
    },

    development: {
      common: {
        applicationId: 1,
        listenPort: 8888,
        instantBookListenPort: 8889,
        host: 'www.escortguide.co.uk.test',
        authPath: 'get_user_info.php',
        sessionCacheTime: 1000 * 60 * 10, //10 minutes
        sessionCheckInterval: 30 * 1000, //1 hour
       imagesHost: 'http://pic.escortguide.co.uk/escortguide.co.uk/',
        avatarSize: 'lvthumb',
        noPhotoUrl: '/img/chat/img-no-avatar.gif',
        userSettingsBackupPath: 'user_settings.bk',
        key: 'JFusLsdf8A9',
        blWordsLifeTime: 12 * 60 * 60 * 1000, //12 hours
        redisBlWordsDb: 0,
        redisBlWordsKey: 'bl-words-ef'
      },
      db: {
        user: 'sceon_eg_co_uk',
        database: 'escortguide_co_uk_backend',
        password: 'Ghhy25p4',
        host: '127.0.0.1'
      },
      redis: {
        host: '127.0.0.1',
        port: 6379
      },
      memcache: {
        host: '127.0.0.1',
        port: 49664
      }
    }
  };


  const get = env_ => {
    !env_ && (env_ = 'production');

    return configs[env_];
  };

  return { get: get };
})();
