/*jslint es6*/
/*jslint node*/

'use strict';

const http = require('http');
const crypto = require('crypto');
const url = require('url');

const socketIo = require('socket.io');

const config = require('../configs').get('production');
const { nullifyTime } = require("./utils");
const userManager = require('./userManager');
const messageManager = require('./messageManager');
const settingsManager = require('./settingsManager');
const bridge = require('./bridge');

const app = http.createServer(handler).listen(config.common.listenPort);
const io = socketIo(app);

function handler(request, response) {
  let queryData = url.parse(request.url, true);
  let query = queryData.query;

  // SET AVAILABILITY

  if (queryData.pathname === '/node-chat/change-availability') {
    let uid = query.userId;
    let availability = query.availability;
    let hash = query.hash;

    if (!uid || !availability || !hash || crypto.createHash('md5').update(`${uid}-${availability}-${config.common.key}`).digest('hex') !== hash) {
      response.writeHead(200, {
        'Content-Type': 'text/plain'
      });
      response.end('sta=error');
    }

    console.log(`Changing availability for userId ${uid} to ${availability}`);

    settingsManager.setAvailability(uid, availability);

    response.writeHead(200, {
      'Content-Type': 'text/plain'
    });
    response.end('sta=ok');
  }

  // GET AVAILABILITY

  if (queryData.pathname === '/node-chat/get-availability') {
    let uid = query.userId;
    let hash = query.hash;

    if (!uid || crypto.createHash('md5').update(`${uid}-${config.common.key}`).digest('hex') !== hash) {
      response.writeHead(200, {
        'Content-Type': 'text/plain'
      });
      response.end('sta=error');
    }

    let result = JSON.stringify({
      availability: settingsManager.getAvailability(uid)
    });

    response.writeHead(200, {
      'Content-Type': 'text/plain'
    });
    response.end(result);
  }

  // GET ONLINE ESCORTS

  if (queryData.pathname === '/node-chat/get-online-escorts') {
    userManager.getOnlineEscortsPromise().then(userIds => {
      response.writeHead(200, {
        'Content-Type': 'text/plain'
      });
      response.end(JSON.stringify(userIds));
    });
  }
}

settingsManager.initSettingsBackupPromise().then(() => {
  console.log('User settings backup done');
});

userManager.initOfflineUsersCheckPromise().then(userIds => {
  if (userIds.length) {
    io.sockets.emit('offline-users', userIds);
  }
});

messageManager.initBlackListedWordsSyncPromise(() => {
  console.log('Bl word sync is done.');
});

// SESSION CHECK: KICKING OUT USERS WITH EXPIRED SESSIONS

setInterval(() => {
  let users = userManager.getUsersPrivateList();
  Object.keys(users).forEach(userId => {
    bridge.getUserInfo(users[userId].info.sid, false).then(resp => {
      if (!resp.auth && users[userId]) {
        users[userId].sockets.forEach(socketId => {
          io.sockets.connected(socketId).disconnect('Logged off');
        });

        users[userId].sockets = [];
        userManager.addUserWithoutSockets(userId);
      }
    });
  });
}, config.common.sessionCheckInterval);

const start = () => {
  io.on('connection', socket => {
    socket.on('auth', data => {
      bridge.getUserInfo(data.sid).then(resp => {
		console.log(resp);  
        if (!resp.auth) {
          socket.emit('auth-complete', {
            auth: false
          });
          socket.disconnect('unauthorized');
          return false;
        }

        let userInfo = resp.data;

        userInfo.status = 'online';
        userInfo.sid = data.sid;

        if (userInfo.userType != 'escort') {
          userInfo.avatar = config.common.noPhotoUrl;
        }

        let availability = 1;

        if (data.forceEnable) {
          settingsManager.setAvailability(userInfo.userId, 1);
        } else {
          availability = settingsManager.getAvailability(userInfo.userId);
        }

        userInfo.settings = settingsManager.getSettings(userInfo.userId);

        socket.emit('auth-complete', {
          auth: true,
          availability: availability,
          userInfo: userInfo
        });

        if (availability) {
          socket.userInfo = userInfo;

          let newUser = true;
          userManager.getUserPromise(userInfo.userId).then(user => {
            if (user) {
              newUser = false
            }

            let isPrivate = !!socket.userInfo.settings.invisibility;
            userManager.addUser(socket.id, userInfo, isPrivate);
            if (newUser && !isPrivate) {
              delete userInfo.settings;

              socket.broadcast.emit('new-user', userInfo);
            }

            let pubList = userManager.getUsersPublicList();

            delete pubList[userInfo.userId];
            
            socket.emit('online-users-list', pubList);

            // Checking if has opened dialogs.
            // If yes getting userInfo and emiting open-dialogs event
            let openDialogs = settingsManager.getOpenDialogs(userInfo.userId);

            if (openDialogs) {
              userManager.getUsersPromise(openDialogs, true).then(usersObj => {
                let od = openDialogs.map(userId => {
                  if (usersObj[userId]) {
                    return {
                      userId: userId,
                      userInfo: usersObj[userId].info
                    };
                  }
                });
  
                let activeDialog = settingsManager.getActiveDialog(userInfo.userId);
                socket.emit('open-dialogs', {
                  dialogs: od,
                  activeDialog: activeDialog
                });
              });
            }

            // Checking if has new messages.
            // If yes emiting new-message event.
            messageManager.getNewMessagesCountPromise(userInfo.userId).then(messages => {
              if (messages && messages.length) {
                let userIds = messages.map(msg => msg.userId);
  
                userManager.getUsersPromise(userIds, true).then(usersObj => {
                  messages = messages.map(msg => {
  
                    if (usersObj[msg.userId]) {
                      return msg.senderInfo = usersObj[msg.userId].info;
                    }
                  });
  
                  socket.emit('new-messages', messages);
                });
              }
            });
          });

        } else {
          socket.disconnect('status:not-available');
        }
      });
    });

    socket.on('message-sent', messageData => {
      if (!socket.userInfo) return false;

      let blockedUsers = settingsManager.getBlockedUsers(messageData.userId);

      if (blockedUsers && blockedUsers.includes(socket.userInfo.userId)) {
        socket.emit('user-blocked-you', {
          dialogId: messageData.userId
        });

        return false;
      }

      userManager.getUserPromise(messageData.userId, true).then(user => {

        if (user) {
          let message = messageManager.clearMessage(messageData.message);

          if (message.length > 0) {
            messageManager.checkMessagePromise(message).then(response => {
              if (response.length) {
                socket.emit('bl-word', {
                  dialogId: messageData.userId,
                  word: res.join(', ')
                });
              } else {

                messageManager.storeMessagePromise(socket.userInfo.userId, user.info.userId, message).then(result => {
                  if (!result) return false;
                });

                let sockets = user.sockets;


                let newMessageData = {
                  body: message,
                  userId: socket.userInfo.userId,
                  date: new Date().getTime()
                }

                sockets.forEach(socketId => {
                  io.to(socketId).emit('new-message', {
                    message: newMessageData,
                    senderData: socket.userInfo
                  });
                });
              }
            });
          }
        }
      });
    });

    socket.on('dialog-opened', data => {
      if (!socket.userInfo) return false;

      let date = nullifyTime(new Date());

      messageManager.getConversationPromise(socket.userInfo.userId, data.userId, date).then(messages => {
        if (messages.length) {
          let senderInfo = userManager.getUserPromise(data.userId, true).then(user => {
            socket.emit('message-history', {
              messages: messages,
              senderInfo: user.info
            });
          });
        }
      });

      settingsManager.addOpenDialog(socket.userInfo.userId, data.userId);
    });

    socket.on('dialog-closed', data => {
      
      if (!socket.userInfo) return false;
      settingsManager.removeOpenDialog(socket.userInfo.userId, data.userId);
    });

    socket.on('dialog-activated', data => {

      if (!socket.userInfo) return false;
      settingsManager.setActiveDialog(socket.userInfo.userId, data.userId);
    });

    socket.on('dialog-deactivated', data => {
      if (!socket.userInfo) return false;
      settingsManager.removeActiveDialog(socket.userInfo.userId);
    });

    socket.on('availability-changed', $availability => {
      if (!socket.userInfo) return false;

      if ($availability) {
        settingsManager.setAvailability(socket.userInfo.userId, 1);
      } else {
        settingsManager.setAvailability(socket.userInfo.userId, 0);

        userManager.getUserPromise(socket.userInfo.userId).then(user => {
          if (user) {
            user.sockets.forEach(socketId => {
              io.to(socketId).emit('chat-off', {});
              io.sockets.connected(socketId).disconnect('chat disabled');
            });
          }
        });
      }
    });

    socket.on('change-options', data => {
      if (!socket.userInfo) return false;

      switch (data.option) {
        case 'show-only-escorts':

          settingsManager.setShowOnlyEscorts(socket.userInfo.userId, data.value);
          break;
        case 'keep-list-open':
          settingsManager.setKeepListOpen(socket.userInfo.userId, data.value);
          break;
        case 'invisibility':
          settingsManager.setInvisibility(socket.userInfo.userId, data.value);

          if (data.value) {
            //If set invisibility = 1 emit users about his status and remove from public list
            socket.broadcast.emit('offline-users', [socket.userInfo.userId]);
            userManager.removeUserFromPublicList(socket.userInfo.userId);
          } else {
            socket.broadcast.emit('new-user', socket.userInfo);
            userManager.addUserToPublicList(socket.userInfo);
          }

          break;
      }
    });

    socket.on('block-user', data => {
      if (!socket.userInfo) return false;

      settingsManager.addBlockedUser(socket.userInfo.userId, data.userId);
    });

    socket.on('unblock-user', data => {
      if (!socket.userInfo) return false;

      settingsManager.removeBlockedUser(socket.userInfo.userId, data.userId);

      //If unblocked user is online emit 'online-user'
      userManager.getUserPromise(data.userId).then(user => {
        if (user) {
          socket.emit('new-user', user.info);
        }
      });
    });

    socket.on('blocked-users', data => {
      if (!socket.userInfo) return false;

      let blockedUsers = settingsManager.getBlockedUsers(socket.userInfo.userId);

      userManager.getUsersPromise(blockedUsers, true).then(users => {

        let _users = Object.keys(users).map(userId => {
          return users[userId].info;
        });

          socket.emit('blocked-users', _users);
        });
    });

    socket.on('conversation-read', data => {
      if (!socket.userInfo) return false;

      messageManager.markAsReadPromise(socket.userInfo.userId, data.userId)
          .then(result => result)
          .catch(err => console.log(err));

    });

    socket.on('typing-start', data => {
      if (!socket.userInfo) return false;

      userManager.getUserPromise(data.userId).then(user => {
        if (user) {
          user.sockets.forEach(socketId => {
            io.to(socketId).emit('typing-start', {
              userId: socket.userInfo.userId
            });
          });
        }
      });
    });

    socket.on('typing-end', data => {
      if (!socket.userInfo) return false;

      userManager.getUserPromise(data.userId).then(user => {
        if (user) {
          user.sockets.forEach(socketId => {
            io.to(socketId).emit('typing-end', {
              userId: socket.userInfo.userId
            });
          });
        }
      })
    });

    socket.on('chat-with', data => {
      userManager.getUserPromise(data.userId, true).then(user => {
        socket.emit('open-dialogs', {
          dialogs: [{
            userId: data.userId,
            userInfo: user.info
          }],
          activeDialog: data.userId
        });
      });
    });

    socket.on('disconnect', () => {
      if (!socket.userInfo) return;
      userManager.removeSocket(socket.userInfo.userId, socket.id);
    });
  });
}

exports.start = start;