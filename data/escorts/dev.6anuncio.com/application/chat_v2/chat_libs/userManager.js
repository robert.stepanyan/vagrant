/*jslint es6*/
/*jslint node*/

'use strict';

const MySQL = require('mysql');
const Memcached = require('memcached');
const config = require('../configs').get('production');

class Users {
  constructor() {
    this.usersPrivateList = {};
    this.usersPublicList = {};
    this.usersWithoutSockets = {};
    this.MySQLClient = null;

    this.establishMySQLConnection();

    this.memcachedClient = new Memcached(
      `${config.memcache.host}:${config.memcache.port}`
    );

    setInterval(() => {
      this.MySQLClient.ping(err => {
        if (err) {
          this.handleMySQLConnectionerr(err);
        }
      });
    }, 5000);
  }

  // USER CONNECTIVITY SERVICE

  initOfflineUsersCheckPromise() {
    return new Promise((resolve, reject) => {
      setInterval(() => {
        let removedUserIds = [];

        Object.keys(this.usersWithoutSockets).forEach(userId => {
          if (
            this.usersPrivateList[userId] &&
            !this.usersPrivateList[userId].sockets.length &&
            new Date() - this.usersWithoutSockets[userId] >= 2 * 1000
          ) {
            removedUserIds.push(this.removeUser(userId));
          }
        });

        return resolve(removedUserIds);
      }, 10000);
    });
  }

  // DB CONNECTION SERVICES

  establishMySQLConnection() {
    this.MySQLClient = MySQL.createConnection({
      user: config.db.user,
      database: config.db.database,
	  password: config.db.password,
      host: config.db.host,
      port: config.db.port
    });

    this.MySQLClient.connect(err => {});
  }

  handleMySQLConnectionerr(err) {
    if (!err.fatal) {
      return false;
    }

    if (err.code !== 'PROTOCOL_CONNECTION_LOST') {
      console.log(
        'The mysql library fired a PROTOCOL_CONNECTION_LOST exception'
      );

      throw err;
    }

    console.log('Re-connecting lost connection: ' + err.stack);
    this.establishMySQLConnection();
  }

  // USER LIST GETTERS

  getUsersPrivateList() {
    return this.usersPrivateList;
  }

  getUsersPublicList() {
    return Object.assign({}, this.usersPublicList);
  }

  // USER GETTERS
  getUserActiveSockets(userId){
     let user = this.usersPrivateList[userId];

     if(user){
        return user.sockets;
     }
  }

  getUserPromise(userId, offline, pm) {

    return new Promise((resolve, reject) => {
      let user = this.usersPrivateList[userId];

      if (user) {
        return resolve(user);
      }

      if (!offline) {
        return resolve(false);
      }

      this.getOfflineUserPromise(userId,pm).then(data => resolve(data)).catch(err => reject(err));
    });
  }

  getUsersPromise(userIds, offline) {
    return new Promise((resolve, reject) => {
      if (!userIds || !userIds.length) {
        return resolve({});
      }

      let usersObj = {};
      let promises = userIds.map(userId => this.getOfflineUserPromise(userId, (offline) ? offline : false));
      Promise.all(promises).then(data => {
        if (data.length) {
          data.forEach(user => {
              // console.log(user);
            if (user) {
              usersObj[user.info.userId] = user;
            }
          });

          return resolve(usersObj);
        }
      }).catch(err => reject(err));
    });
  }

    getPrivateUsers(user_id) {
        return new Promise((resolve, reject) => {
            let _query = `
           SELECT 
              ctp.user_id as userId
            FROM chat_threads_participants c
                INNER JOIN chat_threads_participants ctp ON ctp.thread_id = c.thread_id
                INNER JOIN users u ON u.id = ctp.user_id
                INNER JOIN chat_messages cm ON cm.thread_id = ctp.thread_id
                LEFT JOIN escorts e ON e.user_id = u.id
                LEFT JOIN agencies a ON a.user_id = u.id
                
                WHERE c.user_id = ${user_id} && c.user_id <> ctp.user_id
            GROUP BY ctp.user_id
            ORDER BY ctp.id DESC
      `;

            this.MySQLClient.query(_query, (err, user_ids) => {
                if (err) return err;

                user_ids = user_ids.map( (user) => {
                    return user.userId;
                });

                this.getUsersPromise(user_ids, true).then((users) => {

                    let users_list = user_ids.map( ( user_id ) => {
                        if(users[user_id]){
                            return users[user_id].info;
                        }
                    });

                    return resolve(users_list);
                }).catch((err) => {
                    return reject(err);
                });
            });
        });
    }

  getOfflineUserPromise(userId, from_pm ) {
    return new Promise((resolve, reject) => {
      let _query = `SELECT id, user_type
        FROM users
        WHERE id = ${userId}`;

      this.MySQLClient.query(_query, (err, result) => {

         if (err) {
          return reject(err);
        }



        if (Array.isArray(result) && (!result.length || from_pm)) {

          switch (result[0].user_type) {
            case 'escort':
              this.getEscortPromise(userId).then(data => resolve(data)).catch(err=> reject(err));
              break;
            case 'member':
              this.getMemberPromise(userId).then(data => resolve(data)).catch(err=> reject(err));
              break;
            case 'agency':
              this.getAgencyPromise(userId).then(data => resolve(data)).catch(err=> reject(err));
              break;
          }
        }
      });
    });
  }

  getOnlineEscortsPromise() {
    return new Promise((resolve, reject) => {
      let onlineEscorts = Object.keys(this.usersPublicList).filter(userId => {
        return this.usersPublicList[userId].userType === 'escort';
      });

      return resolve(onlineEscorts);
    });
  }

  getEscortImageUrl(eId, hash, ext) {
    let pathParts = [];
    let escortId = eId.toString();

    if (escortId.length > 2) {
      pathParts = pathParts
        .concat(escortId.substr(0, 2))
        .concat(escortId.substr(2));
    } else {
      pathParts = pathParts.concat('_').concat(escortId);
    }

    let dirPath = pathParts.join('/');

    return `${config.common.imagesHost}/${dirPath}/${hash}_${
      config.common.avatarSize
      }.${ext}`;
  }

  getEscortPromise(userId) {
    return new Promise((resolve, reject) => {
      let _query = `
        SELECT e.id, e.showname, ep.hash, ep.ext
        FROM escorts e
        INNER JOIN escort_photos ep ON ep.escort_id = e.id AND is_main = 1
        WHERE e.user_id = ${userId}
      `;

      this.MySQLClient.query(_query, (err, result) => {
        if (err || !result.length) {
          return resolve(null);
        } else {
          return resolve({
            info: {
              nickName: `${result[0].showname} (Escort)`,
              userId: userId,
              userType: 'escort',
              avatar: this.getEscortImageUrl(
                result[0].id,
                result[0].hash,
                result[0].ext
              ),
              status: 'offline',
              link: `/escort/${result[0].showname}-${result[0].id}`
            },
            sockets: []
          });
        }
      });
    });
  }

  getMemberPromise(userId) {
    return new Promise((resolve, reject) => {
      let _query = `
        SELECT username
        FROM users
        WHERE id = ${userId}
      `;

      this.MySQLClient.query(_query, (err, result) => {
        if (err) return reject(err);

        if (!result.length) {
          return resolve(null);
        } else {
          return resolve({
            info: {
              nickName: `${result[0].username}`,
              userId: userId,
              userType: 'member',
              avatar: config.common.noPhotoUrl,
              status: 'offline',
              link: `/member/${result[0].username}`
            },
            sockets: []
          });
        }
      });
    });
  }

  getAgencyPromise(userId) {
    return new Promise((resolve, reject) => {
      let _query = `
        SELECT username
        FROM users
        WHERE id = ${userId}
      `;

      this.MySQLClient.query(_query, (err, result) => {
        if (err || !result.length) {
          return resolve(null);
        } else {
          return resolve({
            info: {
              nickName: `${result[0].username}`,
              userId: userId,
              userType: 'agency',
              avatar: config.common.noPhotoUrl,
              status: 'offline'
            },
            sockets: []
          });
        }
      });
    });
  }

  // USER SETTERS

  addUser(socketId, info, isPrivate) {
    if (!this.usersPrivateList[info.userId]) {
      this.usersPrivateList[info.userId] = {
        sockets: [],
        info: info
      };
    }

    if (!isPrivate) {
      this.addUserToPublicList(info);
    }

    this.addSocket(info.userId, socketId);

    this.updateOnlineEscortsPromise();
  }

  removeUser(userId) {
    delete this.usersPrivateList[userId];
    delete this.usersPublicList[userId];
    delete this.usersWithoutSockets[userId];

    return userId;
  }

  addUserToPublicList(info) {
    this.usersPublicList[info.userId] = info;
  }

  removeUserFromPublicList(userId) {
    delete this.usersPublicList[userId];
  }

  addSocket(userId, socketId) {
    if (!this.usersPrivateList[userId]) return false;
    this.usersPrivateList[userId].sockets = this.usersPrivateList[userId].sockets.concat(socketId);

    if (this.usersWithoutSockets[userId]) {
      delete this.usersWithoutSockets[userId];
    }
  }

  removeSocket(userId, socketId) {
    if (!this.usersPrivateList[userId]) return false;

    this.usersPrivateList[userId].sockets = this.usersPrivateList[
      userId
    ].sockets.filter(socket => socket !== socketId);
    if (this.usersPrivateList[userId].sockets.lenght === 0) {
      this.usersWithoutSockets[userId] = new Date();
    }
  }

  addUserWithoutSockets(userId) {
    this.usersWithoutSockets[userId] = new Date();
  }

  updateOnlineEscortsPromise() {
    return new Promise((resolve, reject) => {
      this.getOnlineEscortsPromise().then(data => {
        this.memcachedClient.set(`${config.common.applicationId}_chat_online_escorts`, JSON.stringify(data), 1000, (err, result) => {
            if (err) {
              console.log(`ERROR: ${err}`);
              return reject(err);
            }

            return resolve(true);
          });
      });
    });
  }
}

module.exports = new Users();
