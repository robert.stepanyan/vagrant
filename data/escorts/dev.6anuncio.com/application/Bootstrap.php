<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	public function __construct($application)
	{
		require('../application/models/Plugin/Activity.php');
		require('../application/models/Plugin/Block.php');
		parent::__construct($application);
	}

	public function run()
	{
		//Zend_Layout::startMvc();

		// Initialize hooks to avoid code confusion
		Model_Hooks::init();

		parent::run();
	}

	protected function _initRoutes()
	{
		require('../../library/Cubix/Security/Plugin.php');
		require('../application/models/Plugin/I18n.php');

		$this->bootstrap('frontController');
		$front = $this->getResource('frontController');

		//Add module dir to the controllers for default routes...
		$front->addModuleDirectory( APPLICATION_PATH . '/modules' );

		$router = $front->getRouter();
		//$router->removeDefaultRoutes();

		$router->addRoute(
			'index-controller',
			new Zend_Controller_Router_Route('/index/:action',
			array(
				'module' => 'default',
				'controller' => 'index',
				'action' => 'index'
			))
		);

		$router->addRoute(
			'index-controller-lng',
			new Zend_Controller_Router_Route(':lang_id/index/:action',
			array(
				'module' => 'default',
				'controller' => 'index',
				'action' => 'index'
			),
			array(
				'lang_id' => '[a-z]{2}'
			))
		);
		
		$router->addRoute(
			'home-page',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})?/?',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'index',
				), array(
					1 => 'lang_id',
				)
			)
		);
		
		$router->addRoute(
			'escorts-filter',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?escorts(/.+)?',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'index'
				),
				array(
					2 => 'lang_id',
					3 => 'req'
				)
			)
		);

		$router->addRoute(
			'acompanhantes-filter',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?acompanhantes(/.+)?',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'index'
				),
				array(
					2 => 'lang_id',
					3 => 'req'
				)
			)
		);
		
		$router->addRoute(
			'get-filter',
			new Zend_Controller_Router_Route_Regex('(([a-z]{2})/)?escorts/get-filter',
			array(
				'module' => 'default',
				'controller' => 'escorts',
				'action' => 'get-filter'
			), array(
				2 => 'lang_id'
			))
		);


        $router->addRoute(
            'ajax-report-problem',
            new Zend_Controller_Router_Route_Regex('(([a-z]{2})/)?escorts/ajax-report-problem',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'ajax-report-problem'
                ), array(
                    2 => 'lang_id'
                ))
        );

        $router->addRoute(
            'ajax-susp-photo',
            new Zend_Controller_Router_Route_Regex('(([a-z]{2})/)?escorts/ajax-susp-photo',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'ajax-susp-photo'
                ), array(
                    2 => 'lang_id'
                ))
        );
		
		$router->addRoute(
			'escort-profile',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?escort/(.+)?\-([0-9]+)$',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'profile'
				),
				array(
					2 => 'lang_id',
					3 => 'showname',
					4 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'acompanhante-profile',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?acompanhante/(.+)?\-([0-9]+)$',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'profile'
				),
				array(
					2 => 'lang_id',
					3 => 'showname',
					4 => 'escort_id'
				)
			)
		);

        $router->addRoute(
            'escort-profile-vote',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?escort/(.+)?\-([0-9]+)/vote$',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'vote'
                ),
                array(
                    2 => 'lang_id',
                    3 => 'showname',
                    4 => 'escort_id'
                )
            )
        );
		
		$router->addRoute(
			'agencies-list',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?agencies',
				array(
					'module' => 'default',
					'controller' => 'agencies',
					'action' => 'list'
				),
				array(
					2 => 'lang_id'					
				)
			)
		);
		
		$router->addRoute(
			'agency-profile',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?agency/(.+)?\-([0-9]+)$',
				array(
					'module' => 'default',
					'controller' => 'agencies',
					'action' => 'show',
					'lang_id' => null
				),
				array(
					2 => 'lang_id',
					3 => 'agencyName',
					4 => 'agency_id'
				)
			)
		);

		$router->addRoute(
			'agency-escorts-list',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?agency/(.+)?\-([0-9]+)/escorts',
				array(
					'module' => 'default',
					'controller' => 'agencies',
					'action' => 'escorts',
					'lang_id' => null
				),
				array(
					2 => 'lang_id',
					3 => 'agencyName',
					4 => 'agency_id'
				)
			)
		);
		
		$router->addRoute(
			'reviews',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2}))?(/)?reviews',
			array(
				'module' => 'default',
				'controller' => 'reviews',
				'action' => 'index'
			),
                array(
                    1=>'lang_id'
                ))
		);

		$router->addRoute(
			'reviews-add',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2}))?(/)?reviews/add-review/([0-9]+)',
			array(
				'module' => 'default',
				'controller' => 'reviews',
				'action' => 'add-review'
			),array(
					1 => 'lang_id',
					4 => 'escort_id',
			))
		);

		$router->addRoute(
			'reviews-escort',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2}))?(/)?reviews/(.+)?\-([0-9]+)',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'escort'
				),
				array(
				    1=>'lang_id',
					4 => 'escortName',
					5 => 'escort_id'
				)
			)
		);

        $router->addRoute(
            'member-info',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?member/(.+)?',
                array(
                    'module' => 'default',
                    'controller' => 'members',
                    'action' => 'index'
                ), array(
                    2 => 'lang_id',
                    3 => 'username'
                )
            )
        );

		$router->addRoute(
			'comments',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2}))?(/)?comments',
			array(
				'module' => 'default',
				'controller' => 'comments',
				'action' => 'index'
			),
                array(
                    1=>'lang_id'
                ))
		);

        $router->addRoute(
            'add-comment',
            new Zend_Controller_Router_Route('/comments/add-comment',
                array(
                    'module' => 'default',
                    'controller' => 'comments',
                    'action' => 'ajax-add-comment'
                ))
        );

		$router->addRoute(
			'classified-ads',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2}))?(/)?classified-ads',
				array(
					'module' => 'default',
					'controller' => 'classified-ads',
					'action' => 'index'
				),
				array(
					1 => 'lang_id',
				)
			)
		);

		$router->addRoute(
			'classified-ads-place-ad',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2}))?/classified-ads/place-ad',
				array(
					'module' => 'default',
					'controller' => 'classified-ads',
					'action' => 'place-ad'
				),
				array(
					1 => 'lang_id',
				)
			)
		);

		$router->addRoute(
			'classified-ads-success',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2}))?/classified-ads/success',
				array(
					'module' => 'default',
					'controller' => 'classified-ads',
					'action' => 'success'
				),
				array(
					1 => 'lang_id',
				)
			)
		);

		$router->addRoute(
			'classified-ads-error',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2}))?/classified-ads/error',
				array(
					'module' => 'default',
					'controller' => 'classified-ads',
					'action' => 'error'
				),
				array(
					1 => 'lang_id',
				)
			)
		);

		$router->addRoute(
			'classified-ads-details',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?classified-ads/ad/([0-9]+)',
				array(
					'module' => 'default',
					'controller' => 'classified-ads',
					'action' => 'ad'
				),
				array(
					2 => 'lang_id',
					3 => 'id'
				)
			)
		);
		

		$router->addRoute(
			'advertise',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2}))?(/)?advertise',
			array(
				'module' => 'default',
				'controller' => 'index',
				'action' => 'advertise'
			),
                array(
                    1 => 'lang_id',
                ))
		);

		$router->addRoute(
			'advertise-mmg',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2}))?(/)?govazd-mmg',
			array(
				'module' => 'default',
				'controller' => 'index',
				'action' => 'advertise-mmg'
			),
                array(
                    1 => 'lang_id',
                ))
		);

		$router->addRoute(
			'advertise-mmg-response',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2}))?(/)?advertise-mmg-response',
			array(
				'module' => 'default',
				'controller' => 'index',
				'action' => 'advertise-mmg-response'
			),
                array(
                    1 => 'lang_id',
                ))
		);

		$router->addRoute(
			'advertise-payment-email',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2}))?(/)?advertise-payment-email',
			array(
				'module' => 'default',
				'controller' => 'index',
				'action' => 'advertise-payment-email'
			),
                array(
                    1 => 'lang_id',
                ))
		);

        $router->addRoute(
            'feedback',
            new Zend_Controller_Router_Route('/feedback',
                array(
                    'module' => 'default',
                    'controller' => 'index',
                    'action' => 'feedback'
                ))
        );

        $router->addRoute(
            'external-link',
            new Zend_Controller_Router_Route(
                'go',
                array(
                    'module' => 'default',
                    'controller' => 'index',
                    'action' => 'go'
                )
            )
        );

        $router->addRoute(
            'govazd-contact',
            new Zend_Controller_Router_Route('/govazd-contact-send-mail',
                array(
                    'module' => 'default',
                    'controller' => 'index',
                    'action' => 'govazd-contact-send-mail'
                ))
        );

        $router->addRoute(
            'check-user-type',
            new Zend_Controller_Router_Route(
                '/check-user-type',
                array(
                    'module' => 'default',
                    'controller' => 'index',
                    'action' => 'check-user-type'
                )
            )
        );
		
        $router->addRoute(
            'contacts-us',
            new Zend_Controller_Router_Route('/contacts/:action/*',
                array(
                    'module' => 'default',
                    'controller' => 'contacts',
                    'action' => 'index'
                ))
        );
        $router->addRoute(
            'contacts-us-lang',
            new Zend_Controller_Router_Route('/:lang_id/contacts/:action/*',
                array(
                    'module' => 'default',
                    'controller' => 'contacts',
                    'action' => 'index'
                ),
                array(
                    'lang_id'=>'[a-z]{2}'
                ))
        );

        $router->addRoute(
            'pm',
            new Zend_Controller_Router_Route('/pm/:action/*',
                array(
                    'module' => 'default',
                    'controller' => 'pm',
                    'action' => 'index'
                ))
        );
        $router->addRoute(
            'pm-lang',
            new Zend_Controller_Router_Route('/:lang_id/pm/:action/*',
                array(
                    'module' => 'default',
                    'controller' => 'pm',
                    'action' => 'index'
                ),
                array(
                    'lang_id'=>'[a-z]{2}'
                ))
        );
        
        $router->addRoute(
			'account-actions',
			new Zend_Controller_Router_Route('/account/:action/*',
			array(
				'module' => 'default',
				'controller' => 'account'
			))
		);

        $router->addRoute(
			'account-actions-lang',
			new Zend_Controller_Router_Route('/:lang_id/account/:action/*',
			array(
				'module' => 'default',
				'controller' => 'account',
                'lang_id'=>null
			),
                array(
                    'lang_id' => '[a-z]{2}'
                ))
		);

		$router->addRoute(
			'private-change-pass',
			new Zend_Controller_Router_Route_Regex('(([a-z]{2})/)?account/change-pass/([-_a-z0-9]+)/([a-f0-9]{32})',
			array(
				'module' => 'default',
				'controller' => 'account',
				'action' => 'change-pass',
				'lang_id' => null
			), array(
				2 => 'lang_id',
				3 => 'username',
				4 => 'hash'
			))
		);

		$router->addRoute(
			'account-activate',
			new Zend_Controller_Router_Route_Regex('(.+)?/account/activate/(.+)?/([a-f0-9]{32})',
			array(
				'module' => 'default',
				'controller' => 'account',
				'action' => 'activate'
			), array(
				1 => 'user_type',
				2 => 'email',
				3 => 'hash'
			))
		);


        $router->addRoute(
            'private-actions',
            new Zend_Controller_Router_Route('/private/:controller/:action',
                array(
                    'module' => 'private',
                    'controller' => ':controller',
                    'action'=>':action',
                ))
        );

        $router->addRoute(
            'private-actions-lang',
            new Zend_Controller_Router_Route('/:lang_id/private/:controller/:action',
                array(
                    'module' => 'private',
                    'controller' => ':controller',
                    'action'=>':action',
                    'lang_id'=>null
                ),
                array(
                    'lang_id'=>'[a-z]{2}'
                ))
        );

        $router->addRoute(
            'private-actions-index',
            new Zend_Controller_Router_Route('private/:action/*',
                array(
                    'module' => 'private',
                    'controller' => 'index',
                    'action'=>'index',
                ),
                array(
                    'action'=>'(happy-hour|settings|profile-status|index|client-blacklist|user-notifications|add-review|reviews|alerts|escort-reviews|add-review-comment|escorts-reviews|member-profile|agency-profile|agency-logo|ajax-reviews|statistics|ajax-get-statistics|ajax-get-statistics-report|ajax-statistics-report|photos|plain-photos|escorts|premium|ajax-premium-switchajax-get-agency-escorts|ajax-escorts-do|ajax-escorts|ajax-escortCities|add-bubble-text|exists-by-showname|escort-active-package|add-client-to-blacklist|attached-image-blacklist|faq|vip-member-cancel|ajax-escort-comments|add-reply|ajax-fill-contactInfo|ajax-all-rotatable|user-notifications|get-urgent-message|set-urgent-message|get-rejected-verification|set-rejected-verification|show-escorts|ajax-alerts-save|happy-hour-form|escort-status)'
                ))
        );

        $router->addRoute(
            'private-actions-index-lang',
            new Zend_Controller_Router_Route('/:lang_id/private/:action/*',
                array(
                    'module' => 'private',
                    'controller' => 'index',
                    'action'=>'index',
                    'lang_id'=>':lang_id'
                ),
                array(
                    'lang_id'=>'[a-z]{2}',
                    'action'=>'(happy-hour|settings|index|client-blacklist|user-notifications|add-review|reviews|alerts|escort-reviews|add-review-comment|escorts-reviews|member-profile|agency-profile|agency-logo|ajax-reviews|statistics|ajax-get-statistics|ajax-get-statistics-report|ajax-statistics-report|photos|plain-photos|escorts|premium|ajax-premium-switchajax-get-agency-escorts|ajax-escorts-do|ajax-escorts|ajax-escortCities|add-bubble-text|exists-by-showname|escort-active-package|add-client-to-blacklist|attached-image-blacklist|faq|vip-member-cancel|ajax-escort-comments|add-reply|ajax-fill-contactInfo|ajax-all-rotatable|user-notifications|get-urgent-message|set-urgent-message|get-rejected-verification|set-rejected-verification|show-escorts|ajax-alerts-save|happy-hour-form|escort-status)'
                ))
        );



       $router->addRoute(
			'static-pages',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/page/(.+)?',
				array(
					'module' => 'default',
					'controller' => 'static-page',
					'action' => 'show'
				),
				array(
					1 => 'lang_id',
					2 => 'page_slug'
				)
			)
		);

		$router->addRoute(
			'static-pages',
			new Zend_Controller_Router_Route_Regex(
				'^page/(.+)?',
				array(
					'module' => 'default',
					'controller' => 'static-page',
					'action' => 'show'
				),
				array(
					1 => 'page_slug'
				)
			)
		);



        $router->addRoute(
            'girl-of-month',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?girl-of-month(/history)?(/page_([0-9]+))?',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'gotm'
                ), array(

                )
            )
        );

		
		$router->addRoute(
			'links',
			new Zend_Controller_Router_Route('links',
			array(
				'module' => 'default',
				'controller' => 'index',
				'action' => 'links'
			))
		);

		$router->addRoute(
			'v1',
			new Zend_Controller_Router_Route('v1',
			array(
				'module' => 'default',
				'controller' => 'index',
				'action' => 'v1'
			))
		);

		//POPUNDER DEF
		$pundr_plain_route_def = new Zend_Controller_Router_Route('*');
		$pundr_host_route_def = new Zend_Controller_Router_Route_Hostname('www.cloudsrv4.com', 
			array(
				'module' => 'default',
				'controller' => 'index',
				'action' => 'popunder'
			));
		
		$router->addRoute('pundr-domain', $pundr_host_route_def->chain($pundr_plain_route_def));
		
		$router->addRoute(
			'popunder',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?popunder',
				array(
					'module' => 'default',
					'controller' => 'index',
					'action' => 'popunder'
				),
				array(
					2 => 'lang_id',
				)
			)
		);
		
		$router->addRoute(
			'email-collecting',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?index/email-collecting',
				array(
					'module' => 'default',
					'controller' => 'index',
					'action' => 'email-collecting'
				), array(
					2 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'bubble',
			new Zend_Controller_Router_Route_Regex('(([a-z]{2}))?(/)?bubble',
			array(
				'module' => 'default',
				'controller' => 'bubble',
				'action' => 'index'
			))
		);
	}

	protected function _initAutoload()
	{
		$moduleLoader = new Zend_Application_Module_Autoloader(array(
			'namespace' => '',
			'basePath' => APPLICATION_PATH
		));

		$autoloader = Zend_Loader_Autoloader::getInstance();
		$autoloader->registerNamespace('Cubix_');


		return $moduleLoader;
	}

	protected function _initDatabase()
	{
		$this->bootstrap('db');
		$db = $this->getResource('db');
		$db->setFetchMode(Zend_Db::FETCH_OBJ);

		Zend_Registry::set('db', $db);
		Cubix_Model::setAdapter($db);

		$db->query('SET NAMES `utf8`');
	}

	protected function _initConfig()
	{
		Zend_Registry::set('images_config', $this->getOption('images'));
		Zend_Registry::set('videos_config', $this->getOption('videos'));
		Zend_Registry::set('escorts_config', $this->getOption('escorts'));
        Zend_Registry::set('mobile_config', $this->getOption('mobile'));
		Zend_Registry::set('reviews_config', $this->getOption('reviews'));
		Zend_Registry::set('faq_config', $this->getOption('faq'));
		Zend_Registry::set('feedback_config', $this->getOption('feedback'));
		Zend_Registry::set('system_config', $this->getOption('system'));
		Zend_Registry::set('gateway_config', $this->getOption('gateway'));
		Zend_Registry::set('newsman_config', $this->getOption('newsman'));
		Zend_Registry::set('statistics', $this->getOption('statistics'));
	}

	protected function _initViewHelpers()
	{
		$this->bootstrap('layout');
		$layout = $this->getResource('layout');
		$view = $layout->getView();

		$view->doctype('XHTML1_STRICT');

		$view->headMeta()
			->appendHttpEquiv('Content-Type', 'text/html; charset=utf-8')
			->appendHttpEquiv('Cache-Control', 'no-cache');
		$view->headTitle()->setSeparator(' - ');
		
		$view->addHelperPath( APPLICATION_PATH . '/views/helpers' );

		
	}

	protected function _initDefines()
	{

	}

	protected function _initCache()
	{
		$frontendOptions = array(
			'lifetime' => null,
			'automatic_serialization' => true
		);

		$backendOptions = array(
			'servers' => array(
				array(
					'host' => '127.0.0.1',
					'port' => '11211',
					'persistent' =>  true
				)
			),
		);

		if ( defined('IS_DEBUG') && IS_DEBUG ) {
			$cache = Zend_Cache::factory('Core', 'Blackhole', $frontendOptions, array());
		}
		else {
			$cache = Zend_Cache::factory('Core', new Cubix_Cache_Backend_Memcached($backendOptions), $frontendOptions);
		}

		Zend_Registry::set('cache', $cache);
		
		/*************************************/
		$servers = array('host' => '127.0.0.1',  'port' => 11211);		
		$memcache = new Memcache();
		$memcache->addServer($servers['host'], $servers['port']);
		
		Zend_Registry::set('cache_native', $memcache);
	}

	protected function _initBanners()
	{
		if ( defined('IS_CLI') && IS_CLI ) return;

		$banners = array();

		/*$cache = Zend_Registry::get('cache');

		$config = Zend_Registry::get('system_config');

		$banners['banners']['top_banners'] = array();
		$banners['banners']['right_banners'] = array();
		$banners['banners']['right_banners_10'] = array();

		if ( ! $banners = $cache->load('v2_banners_cache') ) {
			for ($i = 0; $i < 4; $i++) {
				$banners['banners']['top_banners'][] = Cubix_Banners::GetBanner(13);
			}

			for ($i = 0; $i < 10; $i++) {
				$banners['banners']['right_banners'][] = Cubix_Banners::GetBanner(13);
			}		

			
			$banners['banners']['right_banners_120'][] = Cubix_Banners::GetBanner(7);
			
			$banners['banners']['right_banners_comment'][] = Cubix_Banners::GetBanner(8);
			
			// links
			for ($i = 0; $i < 50; $i++) {
				$banners['banners']['links'][] = Cubix_Banners::GetBanner(14);
			}
			//
	
			$cache->save($banners, 'v2_banners_cache', array(), $config['bannersCacheLifeTime']);
		}*/
		
		Zend_Registry::set('banners', $banners);
	}

	protected function _initApi()
	{
		$api_config = $this->getOption('api');
		Zend_Registry::set('api_config', $api_config);
		Cubix_Api_XmlRpc_Client::setApiKey($api_config['key']);
		Cubix_Api_XmlRpc_Client::setServer($api_config['server']);
	}



}
