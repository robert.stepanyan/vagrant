<?php

class Api_FeaturedGirlsController extends Zend_Controller_Action
{
    /**
     * @var Zend_Db_Adapter_Mysqli
     */
    protected $_db;

    /**
     * @var Cubix_Api_XmlRpc_Client
     */
    protected $client;

    /**
     * @var
     */
    protected $model;

    /**
     * @var Cubix_Cli
     */
    private $cli;


    public function init()
    {
        $this->client = Cubix_Api_XmlRpc_Client::getInstance();
        $this->model = new Model_ixsBanners();
        $this->cli = new Cubix_Cli();

        $this->_db = Zend_Registry::get('db');
        $this->view->layout()->disableLayout();
    }

    public function syncWithBackendAction()
    {
        $this->cli->clear();
        $this->cli->colorize('yellow')->out('Getting Featured Girls data. ')->reset();
        $rows = $this->client->call('Escorts.getFeaturedGirls');
        if (isset($row['error'])) {
            $this->cli->colorize('red')->out($rows['error'])->reset();
            exit();
        }
        $this->cli->colorize('green')->out(count($rows) . ' Featured Girls data found.')->reset();

        $this->updateFeaturedGirlsData($rows);

        return true;
    }

    private function updateFeaturedGirlsData($rows)
    {
        $this->_db->update('escorts_in_cities', [
            'is_featured' => 0,
        ]);

        foreach ($rows as $row) {
            $this->cli->colorize('yellow')->out('Updating Escort: ' . $row['escort_id'])->reset();

            if (empty($row['city_id']) && isset($row['country_id'])) {
                // The case is when in every city of certain country the girl must be featured

                $this->_db->update('escorts_in_cities', [
                    'is_featured' => 1,
                ], [
                    $this->_db->quoteInto('escort_id = ?', $row['escort_id']),
                ]);

            } else if (!empty($row['city_id'])) {

                $this->_db->update('escorts_in_cities', [
                    'is_featured' => 1,
                ], [
                    $this->_db->quoteInto('escort_id = ?', $row['escort_id']),
                    $this->_db->quoteInto('city_id = ?', $row['city_id'])
                ]);

            }else {
                $this->cli->colorize('red')->out('Couldn\'t update Escort: ' . $row['escort_id'])->reset();
            }
        }

        $this->cli->colorize('green')->out('Done enjoy :)')->reset();
        exit();
    }


}
