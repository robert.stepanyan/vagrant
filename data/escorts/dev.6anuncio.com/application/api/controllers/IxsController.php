<?php
class Api_IxsController extends Zend_Controller_Action
{
	/**
	 * @var Zend_Db_Adapter_Mysqli
	 */
	protected $_db;
	
	public function init()
	{

		$this->_db = Zend_Registry::get('db');
		$this->view->layout()->disableLayout();

	}
	
	public function ixsBannersAction(){
		
		$this->view->layout()->disableLayout();
		$url = 'https://ixspublic.com/deliver.php?websiteID=4';
		$ixspublic = file_get_contents($url);

		//preg_match_all('|(\{)("id"[^\}]+)\}|is', $ixspublic, $matches);

		// bazza lcneluc zone id a petq vor chei drel poxum ei bayc mnac kisat kati eresic))
		preg_match_all('|zonesJsonBuffer = ({[^;]+);|is', $ixspublic, $matches);
		$ixs = json_decode($matches[1][0]);

		//$images is an numeric array of images with their attributes
		$model_ixs = new Model_ixsBanners();

		$model_ixs->truncate();
		$result = true;
		
		foreach ($ixs as $zone => $images) {
			$mysql_insert_result = $model_ixs->insert( $ixs->$zone, $zone);
			if($mysql_insert_result){
				$images = $model_ixs->get();
				$copycount = $model_ixs->copyImages($images);
				
				if(count($images) == $copycount){
					$result = 'Images successfully synced';
				}else{
					$result = 'Error during sync';
				}
			}
		}
		echo($result);
		die;
	}

}
