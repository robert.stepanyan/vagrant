<?php

class Private_CityAlertsController extends Zend_Controller_Action
{
	private $_model;
	private $user;
		
	public function init() 
	{
		$this->user = $this->view->user = Model_Users::getCurrent();
		$this->view->layout()->disableLayout();
		
		if ( ! $this->user ) {
            if ( is_null($this->_getParam('ajax')) ) {
                $this->_redirect($this->view->getLink('signin'));
                return;
            }else{
                die(json_encode(
                    array(
                        'status' => 'error',
                        'redirect_to_signin' => $this->view->getLink('signin')
                    )
                )
                );
            }
		}
		else if ($this->user->user_type != 'member')
		{
			$this->_redirect($this->view->getLink('private-v2'));
			return;
		}
		
		$this->_model = new Model_CityAlerts();
	}
	
	public function indexAction() 
	{
        $this->view->items = $this->_model->getAll($this->user->id);
	}

	
	public function removeAction() 
	{
		if ($this->_request->isPost())
		{
			$id = intval($this->_request->id);
			
			if (!$id) die;
			
			$this->_model->remove($id, $this->user->id);
		}
		
		die;
	}
	
	public function addAction() 
	{

		$city_id = intval($this->_request->city_id);
		$escorts = intval($this->_request->escorts);
		$agencies = intval($this->_request->agencies);
		$notification = intval($this->_request->notification);
		$period = intval($this->_request->period);
				
		if (!in_array($notification, array(1, 2))) 
			$notification = 1;
		
		if (!in_array($period, array(CITY_ALERT_PERIOD_DAY, CITY_ALERT_PERIOD_WEEK))) 
			$period = CITY_ALERT_PERIOD_DAY;
		
		if ($this->_request->isPost())
		{
			$validator = new Cubix_Validator();
						
			if (!$city_id)
				$validator->setError('city_id', 'Please choose city !');
			elseif ($this->_model->checkCityExists($city_id, $this->user->id))
				$validator->setError('city_id', 'This city already exists');
			
			if (!$escorts && !$agencies)
				$validator->setError('user_type', 'Please choose at least one option !');
									
			if ( $validator->isValid() ) 
			{
				$data = array(
					'user_id' => $this->user->id,
					'city_id' => $city_id,
					'escorts' => $escorts,
					'agencies' => $agencies,
					'notification' => $notification,
					'period' => $period,
					'date' => new Zend_Db_Expr('NOW()')
				);
								
				$this->_model->add($data);

				return $this->_forward('index');
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
}

?>
