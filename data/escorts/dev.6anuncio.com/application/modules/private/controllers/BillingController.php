<?php

class Private_BillingController extends Zend_Controller_Action {
	

	const PACKAGE_STATUS_PENDING  	= 1;
	const PACKAGE_STATUS_ACTIVE   	= 2;
	const PACKAGE_STATUS_EXPIRED  	= 3;
	const PACKAGE_STATUS_CANCELLED 	= 4;
	const PACKAGE_STATUS_UPGRADED 	= 5;
	const PACKAGE_STATUS_SUSPENDED 	= 6;
	const GENDER = 1;

	private static $packages = array(109,110,111,112,113,114);
	protected $_session;

	public function init() {

		$this->_session = new Zend_Session_Namespace('online_billing');
   		$this->_session->setExpirationSeconds(60 * 60);

		$this->view->layout()->disableLayout();
		$this->view->user = $this->user = Model_Users::getCurrent();

    	$action = $this->_request->getActionName();
		$anonym_actions = array('success', 'failure' );

		if (!$this->user && !in_array($action, $anonym_actions)) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}

		if (!in_array($action, $anonym_actions) && !$this->user->isAgency() && !$this->user->isEscort()) {
			$this->_redirect($this->view->getLink('private'));
		}

		$this->client = new Cubix_Api_XmlRpc_Client();
	}

	public function indexAction() 
	{

		
		if ($this->user->isEscort()) {
			$escorts_model = new Model_Escorts();
			$escort_id = $this->user->escort_data['escort_id'];

			// $escort = $escorts_model->getforselfcheckout($escort_id, null, true);
			$escort = $escorts_model->get($escort_id, null, true);

			$is_pseudo_escort = $this->client->call(
				'OnlineBillingV2.isPseudoEscort', array($escort_id)
			);

			$user_type = $is_pseudo_escort ? USER_TYPE_AGENCY : USER_TYPE_SINGLE_GIRL;
			$escort->is_pseudo_escort = $is_pseudo_escort;

			$escort_packages = $this->client->call(
				'OnlineBillingV2.checkIfHasPaidPackage', array($escort_id)
			);


			$is_package_purchase_allowed = true;

			if (count($escort_packages)) {
				list($ongoing_package, $pending_package) = $escort_packages;

				if ($ongoing_package && ($ongoing_package['status'] == self::PACKAGE_STATUS_PENDING)) {
					$is_package_purchase_allowed = false;
				}
			}

			if ($escort->escort_status != ESCORT_STATUS_ACTIVE) {
				$is_package_purchase_allowed = false;
			}

			$available_packages = $this->client->call(
				'OnlineBillingV2.getPackagesList',
				array($user_type, $escort->gender, $is_pseudo_escort, self::$packages)
			);
			die(json_encode(array(
				'user_type' => 'escort',
				'escort' => $escort,
				'escort_packages' => $escort_packages,
				'is_package_purchase_allowed' => $is_package_purchase_allowed,
				'available_packages' => $available_packages
			)));
		} elseif ($this->user->isAgency()) {
			$user_type = USER_TYPE_AGENCY;
			$agency_id = $this->user->agency_data['agency_id'];

			$agency_escorts = $this->client->call('OnlineBillingV2.getAgencyEscortsED', array($agency_id));

			foreach($agency_escorts as $i => $escort) {

				$escort = new Model_EscortItem($escort);

				$cur_main_photo = $escort->getMainPhoto();

				$agency_escorts[$i]['photo'] = $cur_main_photo->getUrl('backend_smallest');

				if ($escort['gender'] !== 1) {
					unset($escort);
				}
			}

			$available_packages = $this->client->call(
				'OnlineBillingV2.getPackagesList',
				array($user_type, self::GENDER, $is_pseudo_escort, self::$packages)
			);
			
			die(json_encode(array(
				'user_type' => 'agency',
				'agency' => $this->user->agency_data,
				'escorts' => $agency_escorts,
				'available_packages' => $available_packages
			)));
		}
	}

	public function checkoutAction() {
		if (!$this->_request->isPost()) die();
		$request = $this->_request;


		$hash  = base_convert(time(), 10, 36);
		$cart = $request->cart;
		$user_id = $this->user->id;
		$user_type = $this->user->isAgency() ? USER_TYPE_AGENCY : USER_TYPE_SINGLE_GIRL;

		$post_data = array();

		// ESCORT
		if ($this->user->isEscort()) {
			$data = array();
			if ($cart[0]['activation_date']) {
				$data['activation_date'] = $cart[0]['activation_date'];
			}

			$post_data[] = array(
				'user_id' => $user_id,
				'escort_id' => $cart[0]['escortId'],
				'agency_id' => null,
				'package_id'=> $cart[0]['packageId'],
				'data' => serialize($data),
				'hash' => $hash
			);
		}

		// AGENCY

		if ($this->user->isAgency()) {
			foreach($cart as $package) {
				$data = array();

				if ($package['activation_date']) {
					$data['activation_date'] = $package['activation_date'];
				}

				$post_data[] = array(
					'user_id' => $user_id,
					'escort_id' => $package['escortId'],
					'agency_id' => null,
					'package_id'=> $package['packageId'],
					'data' => serialize($data),
					'hash' => $hash
				);
			}
		}


		$amount = $this->client->call(
			'OnlineBillingV2.addToShoppingCartED',
			array(
				$post_data,
				$user_id,
				$user_type,
				$hash
			)
		);

        $mmgBill = new Model_MmgBillAPIV2();

        $hosted_url = $mmgBill->getHostedPageUrl(
			$amount, 'SCZ' . $this->user->id . 'Z' . $hash ,
			false,
			'https://www.6anuncio.com/' . $this->view->getLink('ob-mmg-postback'));

		die(json_encode(array('status' => 'success', 'url' =>  $hosted_url)));
	}

	public function successAction() {
	}
	public function mmgPostbackAction() {
		$this->view->layout()->enableLayout();
		$this->view->transaction_status = $this->_request->txn_status;
		$this->view->mmg_errno = $this->_request->mmg_errno;
	}
	public function failureAction()	{
	}

}