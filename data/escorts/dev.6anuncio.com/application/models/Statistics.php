<?php

class Model_Statistics extends Cubix_Model
{
	public static function getTotalCount($gender = null, $is_agency = null, $is_tour = null, $is_upcoming = null)
	{
		$where = array(
			'eic.gender = ?' => $gender,
			'eic.is_agency = ?' => $is_agency,
			'eic.is_tour = ?' => $is_tour,
			'eic.is_upcoming = ?' => $is_upcoming
		);

		$where = self::getWhereClause($where, true);

		$lng = Cubix_I18n::getLang();
		$sql = '
			SELECT
				COUNT(DISTINCT(eic.escort_id)) AS escort_count
			FROM escorts_in_cities eic
			INNER JOIN cities ct ON ct.id = eic.city_id
			INNER JOIN regions r ON r.id = ct.region_id
			' . (!is_null($where) ? 'WHERE ' . $where : '') . '
		';

		return self::db()->fetchOne($sql);
	}

	public static function getCities($region_id = null, $gender = null, $is_agency = null, $is_tour = null, $is_upcoming = null,$wheretmp = null, $limit = null)
	{
	    if($gender <= 0) $gender = null;
		$where = array(
			'eic.region_id = ?' => $region_id,
			'eic.gender = ?' => $gender,
			'eic.is_agency = ?' => $is_agency,
			'eic.is_tour = ?' => $is_tour,
			'eic.is_upcoming = ?' => $is_upcoming
		);

		$join = '';
        if($wheretmp){
            $where = array_merge($wheretmp,$where);
            $join = 'INNER JOIN escorts e ON e.id = eic.escort_id ';
        }

		$where = self::getWhereClause($where, true);

		$lng = Cubix_I18n::getLang();

		$sql = '
			SELECT
				eic.city_id, COUNT(DISTINCT(eic.escort_id)) AS escort_count,
				ct.title_' . $lng . ' AS city_title, ct.slug AS city_slug,
				r.title_' . $lng . ' AS region_title
			FROM escorts_in_cities eic
			'.$join.'
			INNER JOIN cities ct ON ct.id = eic.city_id
			LEFT JOIN regions r ON r.id = ct.region_id
			' . (!is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.city_id
			ORDER BY escort_count DESC
		';

		if ( $limit ) {
			$sql .= " LIMIT " . $limit;
		}
		$result = self::db()->fetchAll($sql);


		return $result;
	}

	public static function getAgencyCities()
	{
		$lng = Cubix_I18n::getLang();
		$sql = '
			SELECT
				cd.city_id, COUNT(DISTINCT(cd.id)) AS escort_count,
				ct.title_' . $lng . ' AS city_title, ct.slug AS city_slug,
				r.title_' . $lng . ' AS region_title
			FROM club_directory cd
			INNER JOIN cities ct ON ct.id = cd.city_id
			INNER JOIN regions r ON r.id = ct.region_id
			GROUP BY cd.city_id
		';

		$result = self::db()->fetchAll($sql);
		usort($result, array('Model_Statistics', '_orderByEscortCount'));

		return $result;
	}

	public static function getAgencyTotalCount()
	{		
		$sql = '
			SELECT
				COUNT(DISTINCT(cd.id)) AS escort_count
			FROM club_directory cd
			LEFT JOIN cities ct ON ct.id = cd.city_id
			LEFT JOIN regions r ON r.id = ct.region_id
		';

		return self::db()->fetchOne($sql);
	}

	public static function getRegions($gender = null, $is_agency = null, $is_tour = null, $is_upcoming = null)
	{
		$where = array(
			'eic.gender = ?' => $gender,
			'eic.is_agency = ?' => $is_agency,
			'eic.is_tour = ?' => $is_tour,
			'eic.is_upcoming = ?' => $is_upcoming
		);

		$where = self::getWhereClause($where, true);

		$lng = Cubix_I18n::getLang();
		$sql = '
			SELECT
				COUNT(DISTINCT(eic.escort_id)) AS escort_count,
				r.title_' . $lng . ' AS region_title, r.slug AS region_slug
			FROM escorts_in_cities eic
			INNER JOIN cities ct ON ct.id = eic.city_id
			INNER JOIN regions r ON r.id = ct.region_id
			' . (!is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.region_id;
		';

		$result = self::db()->fetchAll($sql);
		usort($result, array('Model_Statistics', '_orderByEscortCount'));

		return $result;
	}

	public static function getZones($city_id = null, $gender = null, $is_agency = null, $is_tour = null, $is_upcoming = null, $wheretmp = null)
	{
		$where = array(
			'eic.city_id = ?' => $city_id,
			'eic.gender = ?' => $gender,
			'eic.is_agency = ?' => $is_agency,
			'eic.is_tour = ?' => $is_tour,
			'eic.is_upcoming = ?' => $is_upcoming
		);

		$join = '';

        if($wheretmp){
            $where = array_merge($wheretmp,$where);
            $join = 'INNER JOIN escorts e ON e.id = eic.escort_id ';
        }

		$where = self::getWhereClause($where, true);

		$lng = Cubix_I18n::getLang();
		$sql = '
			SELECT
				COUNT(DISTINCT(eic.escort_id)) AS escort_count,
				ct.title_' . $lng . ' AS city_title, ct.slug AS city_slug,
				cz.title_' . $lng . ' AS zone_title, cz.slug AS zone_slug
			FROM escorts_in_cityzones eic
			'.$join.'
			INNER JOIN cityzones cz ON cz.id = eic.cityzone_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			' . (!is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.cityzone_id
			ORDER BY COUNT(DISTINCT(eic.escort_id)) DESC;
		';

		$result = self::db()->fetchAll($sql);

		return $result;
	}

	public static function _orderByEscortCount($a, $b)
	{
		$a = (int) $a->escort_count; $b = (int) $b->escort_count;
		if ( $a == $b ) return 0;
		return $a < $b ? 1 : -1;
	}

	public static function ajaxSearchArray()
	{
		
	}

	public static function getEscortStatistics($agency_id = null, $escort_id = null, $date_from = null, $date_to = null, $order_by = null, $order_dir = null) {
		$client = Cubix_Api::getInstance();
		$data = array();
		$data['agency_id'] = $agency_id;
		$data['escort_id'] = $escort_id;
		$data['date_from'] = $date_from;
		$data['date_to'] = $date_to;
		$data['order_by'] = $order_by;
		$data['order_dir'] = $order_dir;

//		var_Dump( $client->call('getStatistics', array( $data )) );
//		exit;
		return $client->call('getStatistics', array( $data ));
	}

	public static function updateReport( $escort_id, $status = null, $type = null, $email = null, $username = null ){
		$client = Cubix_Api::getInstance();
		$data = array();

		$data['escort_id'] = $escort_id;
		$data['status'] = $status;
		$data['type'] = $type;
		$data['email'] = $email;
		$data['username'] = $username;

		return $client->call('updateReport', array( $data ));
	}

	public static function getEscortReportStatus( $escort_id ){
		$client = Cubix_Api::getInstance();

		return $client->call('getReportStatus', array( $escort_id ));
	}
	
	public static function addEmailStatistics()
	{
		$item = self::db()->query('SELECT * FROM email_collecting_statistics WHERE date = DATE(NOW())')->fetch();
		
		if ($item)
		{
			self::db()->update('email_collecting_statistics', array('count' => ($item->count + 1)), self::db()->quoteInto('date = ?', $item->date));
		}
		else
		{
			self::db()->insert('email_collecting_statistics', array('date' => date('Y-m-d'), 'count' => 1));
		}
	}

	public static function getNearestCitiesByCoordinates($coord = array(), $limit = 1, $exclude_city_id = null)
	{
		$lng = Cubix_I18n::getLang();

		$fields = '';

		$where = "";
		if ( $exclude_city_id ) {
			$where = " AND c.id <> " . $exclude_city_id;
			$country_id = Model_Countries::getCountryIdByCityId($exclude_city_id);
			$where .= " AND cr.id = " . $country_id;
		}

		if (array_key_exists("lat", $coord) && strlen($coord['lat']) > 0 && array_key_exists("lon", $coord) && strlen($coord['lon']) > 0)
			$fields = '
				((2 * 6371 *
					ATAN2(
					SQRT(
						POWER(SIN((RADIANS(' . $coord['lat'] . ' - c.latitude))/2), 2) +
						COS(RADIANS(c.latitude)) *
						COS(RADIANS(' . $coord['lat'] . ')) *
						POWER(SIN((RADIANS(' . $coord['lon'] . ' - c.longitude))/2), 2)
					),
					SQRT(1-(
						POWER(SIN((RADIANS(' . $coord['lat'] . ' - c.latitude))/2), 2) +
						COS(RADIANS(c.latitude)) *
						COS(RADIANS(' . $coord['lat'] . ')) *
						POWER(SIN((RADIANS(' . $coord['lon'] . ' - c.longitude))/2), 2)
					))
					)
				)) AS distance
			';
		else
			$fields = 'NULL AS distance ';

		$sql = '
			SELECT DISTINCT
				c.id AS city_id, c.title_' . $lng . ' AS city_title, c.slug AS city_slug, ' . $fields . ',
				cr.id AS country_id, cr.title_' . $lng . ' AS country_title
			FROM cities c
			INNER JOIN escorts_in_cities eic ON eic.city_id = c.id
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN countries cr ON cr.id = c.country_id
			WHERE c.latitude IS NOT NULL AND c.longitude IS NOT NULL ' . $where . '
			GROUP BY eic.escort_id
			ORDER BY distance
		';


		if ( $limit ) {
			$sql .= ' LIMIT ' . $limit;
		}

		$result = self::db()->fetchAll($sql);

		return $result;
	}
}
