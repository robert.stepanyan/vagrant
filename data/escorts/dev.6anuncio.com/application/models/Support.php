<?php

class Model_Support extends Cubix_Model
{
	protected $_table = 'support_tickets';
	protected $_itemClass = 'Model_SupportItem';

	const STATUS_TICKET_OPENED = 1;
	const STATUS_TICKET_CLOSED = 2;
	
	const STATUS_NEW_UNREAD = 1;
	const STATUS_NOT_REPLIED = 2;
	const STATUS_READ_REPLIED = 3;
	const STATUS_NEW_REPLY = 4;
	
	public function get($ticket_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$data = $client->call('Support.getAttached', array($ticket_id));

		return $data;
	}

	public function isUserTicket($data)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Support.isUserTicket', array($data));
	}
	
	public function isClosed($ticket_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Support.isClosed', array($ticket_id));
	}
	
	public function getAll($user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$data = $client->call('Support.getAll', array($user_id));

		$tickets = array('opened' => array(), 'closed' => array());
		foreach ( $data as $ticket ) {
			if ( $ticket['status'] == self::STATUS_TICKET_OPENED ) {
				$tickets['opened'][] = $ticket;
			}
			else {
				$tickets['closed'][] = $ticket;
			}
		}

		return $tickets;
	}

	public function save($data)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$ticket_id = $client->call('Support.insertAttached', array($data));

		return $ticket_id;
	}

	public function addComment($data)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$data = $client->call('Support.addCommentAttached', array($data));

		return $data;
	}

	public function getComments($ticket_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$data = $client->call('Support.getCommentsAttached', array($ticket_id));

		return $data;
	}
	
	public function read($ticket_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$client->call('Support.read', array($ticket_id));
	}
	
	public function comment_read($ticket_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$client->call('Support.comment_read', array($ticket_id));
	}
	
	public function getUnreadsCount($user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$data = $client->call('Support.getUnreadsCount', array($user_id));

		return $data;
	}

    public function addAttached($data)
    {
        $client = new Cubix_Api_XmlRpc_Client();
        $id = $client->call('Support.addAttached', array($data));
        $result = $client->call('Support.getAttachedFile', array($id));

        $document_model = new Cubix_Documents();

        $url = $document_model->getUrl(new Cubix_Images_Entry(array(
            'application_id' => Cubix_Application::getId(),
            'catalog_id' => 'attached',
            'hash' => $result[0]['hash'],
            'ext' => $result[0]['ext']
        )));

        $result[0]['file_url'] = $url;

        return $result[0];
    }
}
