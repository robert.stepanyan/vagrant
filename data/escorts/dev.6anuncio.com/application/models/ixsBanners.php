<?php
class Model_ixsBanners extends Cubix_Model
{
	public function insert($images, $zone){
		
		$sql = "INSERT INTO ixs_banners ( `id`, `ixs_id`, `zone_id`, `zone_rotate`, `zone_rotation`,
										 `banner_id`, `filename`, `target`,`title` , `link`, `cmp_id`,
										 `user_id`, `size`, `md5_hash`)";
		$sql_value = 'VALUES ';	

		foreach ($images as $image) {
			
			$image_arr = (array) $image;
			$image_arr['zone_id'] = $zone;
		
			$image_data[0] = (isset($image_arr['id']) ? $image_arr['id']  : '' );
			$image_data[1] = (isset($image_arr['zone_id']) ? $image_arr['zone_id']  : '' );
			$image_data[2] = (isset($image_arr['zone_rotate']) ? $image_arr['zone_rotate']  : '' );
			$image_data[3] = (isset($image_arr['zone_rotation']) ? $image_arr['zone_rotation']  : '' );
			$image_data[4] = (isset($image_arr['banner_id']) ? $image_arr['banner_id']  : '' );
			$image_data[5] = (isset($image_arr['filename']) ? $image_arr['filename']  : '' );
			$image_data[6] = (isset($image_arr['target']) ? $image_arr['target']  : '' );
			$image_data[7] = (isset($image_arr['title']) ? $image_arr['title']  : 'notitle' );
			$image_data[8] = (isset($image_arr['link']) ? $image_arr['link']  : '' );
			$image_data[9] = (isset($image_arr['cmp_id']) ? $image_arr['cmp_id']  : '' );
			$image_data[10] = (isset($image_arr['user_id']) ? $image_arr['user_id']  : '' );
			$image_data[11] = (isset($image_arr['size']) ? $image_arr['size']  : '' );
			$image_data[12] = (isset($image_arr['md5_hash']) ? $image_arr['md5_hash']  : '' );
		
			$sql_value .='(null,';
			//$last_key = end(array_keys($image));
			
			foreach ($image_data as $key => $value) {
				$sql_value .= '"'.$value.'"';
				if ($key < 12) {
			        $sql_value .= ',';
			    }
			}
			$sql_value .='),';

			unset($image_data[$key]);
		}
			
		$sql .= $sql_value;

		
		try{
			$this->getAdapter()->query(rtrim($sql, ','));
			return TRUE;
		}catch(Exception $e ){
			 var_dump($e->getMessage());
		}
	}
	
	static public function get($zone_id = null, $user_id = null, $size = null)
	{
		$where = array(
			'ixs.zone_id = ?' => $zone_id,
			'ixs.user_id = ?' => $user_id,
			'ixs.size = ?' => $size
		);

		$where = self::getWhereClause($where, true);
		$sql = '
			SELECT *
			FROM ixs_banners ixs
			' . (!is_null($where) ? 'WHERE ' . $where : '');

		$result = parent::_fetchAll($sql);
		return $result;
	}

	public static function get_right_sidebar_banners($zone_id = null, $rand_count = false )
	{
		
		if ( isset($zone_id) ) {
			$model_ixs = new Model_ixsBanners();
			$banners = $model_ixs->get($zone_id);

			if($rand_count){
				$random_banners = array();
				$random_els = array_rand($banners, $rand_count);
				if($rand_count == 1){
					$random_banners[] = $banners[$random_els];
					return $random_banners;
				}else{
					foreach($random_els as $random_el){
						$random_banners[] = $banners[$random_el];
					}	
					$banners = $random_banners;	
				}
					
			}
			
			return $banners;

		}

		return "";
	}

	public function truncate(){
		$sql = 'truncate table ixs_banners';
		if($this->getAdapter()->query($sql)){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function copyImages($imgs){
		$copycount = 0;
		foreach ($imgs as $img) {
			
			if (!file_exists('./images/ixsBanners/'.$img->filename)) {

				$sourceurl = 'https://adzz.io/uploads/'.$img->filename;
				
				$result = copy($sourceurl, './images/ixsBanners/'.$img->filename);
				if(!$result){
					return 'image copy error';
				}
			}
			$copycount++;
		}
		return $copycount;
	}
}
