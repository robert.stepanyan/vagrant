<?php

class Model_Plugin_I18n extends Zend_Controller_Plugin_Abstract
{
	public function __construct()
	{
		
	}

	public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request)
	{
		$user = Model_Users::getCurrent();
		
		if ( ! is_null($user) ) {
			Model_Users::setLastRefreshTime($user);
		}

		//$this->rememberMeSignIn();
	}

	/*public function rememberMeSignIn()
	{
		$cookie_name = 'signin_remember_' . Cubix_Application::getId();

		if ( isset($_COOKIE[$cookie_name]) ) {
			$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
			$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
			$key = '_-_SignInRememberMe_-_' . Cubix_Application::getId();

			$crypt_login_data = $_COOKIE[$cookie_name];
			$crypt_login_data = base64_decode($crypt_login_data);
			$login_data = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $crypt_login_data, MCRYPT_MODE_ECB, $iv);

			$login_data = @unserialize($login_data);
			
			try {
				if ( is_array($login_data) ) {

					$model = new Model_Users();

					if ( ! $user = $model->getByUsernamePassword($login_data['username'], $login_data['password']) ) {
						$this->view->errors['username'] = 'Wrong username/password combination';
						return;
					}
					//print_r($user);die;
					if ( STATUS_ACTIVE != $user->status ) {
						$this->view->errors['username'] = 'Your account is not active yet';
						return;
					}

					Zend_Session::regenerateId();
					$user->sign_hash = md5(rand(100000, 999999) * microtime(true));
					Model_Users::setCurrent($user);

					//set client ID
					Model_Reviews::createCookieClientID($user->id);
					//


					//if ( ! Models_Auth::getAuth()->hasIdentity() ) {
						//Models_Auth::auth($login_data['username'], $login_data['password'], Estate_Application::ID);
					//}
				}
			}
			catch (Exception $e) {

			}
		}
	}*/
	
	public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
		if ( ! isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $request->getActionName() != 'js-init-exact' && $request->getActionName() != 'js-init' ) {
			if ( isset($_COOKIE['ec_count']) ) {
				$count = ($_COOKIE['ec_count'] < 3) ? $_COOKIE['ec_count'] + 1 : 1;				
				setcookie("ec_count", $count, 0, "/", ".6anuncio.com");
			} else {
				setcookie("ec_count", 1, 0, "/", ".6anuncio.com");
			}
		}
				
		//1 - disable chat
		//2 - node chat
		if ( ! $request->getParam('chatVersion') ) {
			$request->setParam('chatVersion', 2);
		}
    }

	public function routeShutdown($request)
	{
		$db = Zend_Registry::get('db');
		$lang_id = $request->lang_id;
		
		$default_lang_id = 'pt';
		$lngs = Cubix_I18n::getLangs();
			
			
		if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) ){
			
			if($lang_id) {
				//&& $_COOKIE['ln'] != $lang_id
				if (in_array($lang_id, $lngs) ) {
					setcookie('ln', $lang_id, time() + 31536000, '/', APP_DOMAIN);
					$_COOKIE['ln'] = $lang_id;
					$default_lang_id = $lang_id;
					/*$uri = trim($_SERVER['REQUEST_URI'], '/');
					$uri = explode('/', $uri);
					array_shift($uri);
					$uri = implode('/', $uri);
					header('Location: /' . $uri);
					die;*/
				}
			}
			elseif(isset($_COOKIE['ln'])  ) {

				if ( ! in_array($_COOKIE['ln'], $lngs) ) {
					setcookie('ln', "", time() - 3600, '/', APP_DOMAIN);
					$_COOKIE['ln'] = null;
				}
				else{
					$default_lang_id = $_COOKIE['ln'];

				}
				
			}
			else{

				$geo_location = Cubix_Geoip::getClientLocation();

				if($geo_location){

					$geo_lang = strtolower($geo_location['country_iso']);
					$geo_lang = in_array($geo_lang, array('gb','us')) ? 'en' : $geo_lang;

					if ( in_array($geo_lang, $lngs) ) {
						$default_lang_id = $geo_lang;
					}
				}
				setcookie('ln', $default_lang_id, time() + 31536000, '/', APP_DOMAIN);
				$_COOKIE['ln'] = $default_lang_id;
			}
		}
		else{
			if (in_array($lang_id, $lngs) ) {
				$default_lang_id = $lang_id;
			} elseif(isset($_COOKIE['ln'])  ) {

				if ( ! in_array($_COOKIE['ln'], $lngs) ) {
					setcookie('ln', "", time() - 3600, '/', APP_DOMAIN);
					$_COOKIE['ln'] = null;
				}
				else{
					$default_lang_id = $_COOKIE['ln'];

				}
				
			}
		}
						
		$request->setParam('lang_id', $default_lang_id);
			
		Cubix_I18n::init();

		require 'Cubix/defines.php';		
		Zend_Registry::set('definitions', $DEFINITIONS);
	}
}
