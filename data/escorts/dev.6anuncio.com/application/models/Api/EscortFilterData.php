<?php


class Model_Api_EscortFilterData extends Cubix_Model {
	
	private static $DEFINITIONS;
	private static $filter;
	

	private static $fields_map = array(
		'height' => array(
			'type' => 'range',
			'options_key'	=> 'height',
			'variable'	=> 'height'
		),
		'weight' => array(
			'type' => 'range',
			'options_key'	=> 'weight',
			'variable'	=> 'weight'
		),
		'age' => array(
			'type' => 'range',
			'options_key'	=> 'age',
			'variable'	=> 'age'
		),
		'eye_color' => array(
			'type'	=> 'exact',
			'options_key'	=> 'eye-color',
			'variable'	=> 'eye_color'
		),
		'hair_color' => array(
			'type'	=> 'exact',
			'options_key'	=> 'hair-color',
			'variable'	=> 'hair_color'
		),
		'hair_length' => array(
			'type'	=> 'exact',
			'options_key'	=> 'hair-length',
			'variable'	=> 'hair_length'
		),
		'cup_size' => array(
			'type'	=> 'exact',
			'options_key'	=> 'cup-size',
			'variable'	=> 'cup_size'
		),
		'smoker' => array(
			'type'	=> 'exact',
			'options_key'	=> 'smoker',
			'variable'	=> 'is_smoking'
		),
		'ethnicity' => array(
			'type'	=> 'exact',
			'options_key'	=> 'ethnicity',
			'variable'	=> 'ethnicity'
		)
	);

	public function __construct()
	{
		$DEFINITIONS = Zend_Registry::get('defines');
		self::$filter = require('Cubix/filter-v2.php');
	}
	
	public function getData($data)
	{
		$filter_row = array(
			'escort_id'	=> $data->id
		);
		
		//Getting escort services from local(6annonce_www) db
		$services = parent::db()->fetchAll('SELECT service_id FROM escort_services WHERE escort_id = ?', array($data->id));
		
		//Getting escort row from local(6annonce_www) db
		$esc_row = parent::db()->fetchRow('SELECT nationality_id, verified_status, sex_orientation FROM escorts WHERE id = ?', array($data->id));

		$keywords = parent::db()->fetchAll('SELECT keyword_id FROM escort_keywords WHERE escort_id = ?', array($data->id));
		
		//Transforming birth_date to age
		if ( $data->birth_date ) {
			$timestamp = $data->birth_date;
			
			
			
			$curr_time = time();
			$age = ($timestamp < 0 ) ? ($curr_time + ($timestamp * -1)) : $curr_time - $timestamp;
			$year = 60 * 60 * 24 * 365;
			$age_in_years = floor($age / $year);
			
			//list($year, $month, $day) = explode('-', date('Y-m-d', $data->birth_date));
			
			$data->age = $age_in_years;
		}
		
		foreach(self::$fields_map as $field_prefix => $map) {
			$var = $data->{$map['variable']};
			if ( $var ) { // ISSET field in profile
				foreach(self::$filter[$map['options_key']] as $opt) {
					if ( $map['type'] == 'range' ) { // IF filter option type is range ex. "100-150"
						$values = explode('-', $opt['value']);

						if ( $var >= $values[0] && $var <= $values[1] ) {
							$key = preg_replace('#-#', '_', $opt['value']);
							$filter_row[$field_prefix . '_' . $key] = 1;
							break;
						}
					} elseif ( 'exact' ) {
						foreach(self::$filter[$map['options_key']] as $opt) {

							if ( $var == $opt['value'] ) {
								$key = strtolower(preg_replace('#-#', '_', $opt['value']));
								$filter_row[$field_prefix . '_' . $key] = 1;
							}
						}
					}
				}
			}
		}

		//Custom fields
		if ( $data->langs && count($data->langs) ) {
			foreach($data->langs as $lng) {
				foreach(self::$filter['language'] as $lang) {
					if ( $lng->id == $lang['value'] ) {
						$filter_row['language_' . $lng->id] = 1;
					}
				}
			}
		}
		
		if ( $data->sex_availability ) {
			$sex_availability = explode(',', $data->sex_availability);
			foreach(self::$filter['service-for'] as $srv_for) { 
				if ( in_array($srv_for['value'], $sex_availability) ) {
					$filter_row['service_for_' . $srv_for['value']] = 1;
				}
			}
		}
		
		if ( $data->outcall_type && $data->outcall_type != AVAILABILITY_OUTCALL_OTHER ) {
			$filter_row['available_for_o_' . $data->outcall_type] = 1;
		}
		if ( $data->incall_type && $data->incall_type != AVAILABILITY_INCALL_OTHER && $data->incall_type != AVAILABILITY_INCALL_CLUB_STUDIO ) {
			$filter_row['available_for_i_' . $data->incall_type] = 1;
		}
		
		foreach($services as $srv) {
			if ( $srv->service_id > 0 ) {
				$filter_row['services_' . $srv->service_id] = 1;
			}
		}

		foreach($keywords as $k) {
            if ( $k->keyword_id > 0 ) {
                $filter_row['keywords_' . $k->keyword_id] = 1;
            }
        }

       // foreach($orientation as $o) {
            if ( $esc_row->sex_orientation > 0 ) {
                $filter_row['orientation_' . $esc_row->sex_orientation] = 1;
            }
     //   }
		
		// for 100% verification status
		if ( $esc_row->verified_status == 2 ) {
			$filter_row['verified_2'] = 1;
		}
		
		if ( $esc_row->nationality_id ) {
			$filter_row['nationality_' . $esc_row->nationality_id] = 1;
		}
		
		
		
		return $filter_row;
	}
}
