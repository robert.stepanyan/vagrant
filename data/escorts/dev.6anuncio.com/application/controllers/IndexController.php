<?php

class IndexController extends Zend_Controller_Action
{
	public function indexAction()
	{
		$this->_forward('index', 'escorts');
	}

	protected function _ajaxError()
	{
		die(json_encode(array('result' => 'error')));
	}

	public function chatOnlineCronAction()
	{
		ini_set('memory_limit', '1024M');
		
		$db = Zend_Registry::get('db');

		$xml = file_get_contents("php://input");
		
		if (preg_match("#presenceFull#", $xml) ) {
		
			$online_users = simplexml_load_string($xml);
			
			// Reset all online users
			$db->update('escorts', array('is_online' => 0), $db->quoteInto('is_online = ?', 1));

			$user_ids = array();
			foreach ( $online_users->children() as $user ) {
				$user_ids[] = (string) $user;
			}

			if ( count($user_ids) > 0 ) {
				$db->query('UPDATE escorts SET is_online = 1 WHERE user_id IN (\'' . implode('\',\'', $user_ids) . '\')');
			}

			$db->query('UPDATE online_in_chat SET online = ?', array(count($user_ids)));
		}
		
		die;
	}
	
	public function chatOnlineBtnAction()
	{
		$this->view->layout()->disableLayout();

		$req = $this->_request;

		$username = $req->username;
		try {
			$online_users = file_get_contents('http://www.6annonce.com:35555/roomonlineusers.js?roomid=1');
			
			if ( strlen($online_users) > 0 ) {
				$online_users = substr($online_users, 20, (strlen($online_users) - 21));

				$online_users = json_decode($online_users);

				if ( count($online_users) > 0 ) {
					foreach ( $online_users as $user ) {
						if( $user->name == $username ) {
							$this->view->show_btn = true;
							break;
						}
					}
				}
			}
			else {
				$this->view->show_btn = false;
			}
		}
		catch ( Exception $e ) {

		}
	}

	public function escortPhotosAction()
	{
		$id = intval($this->_getParam('id'));
		if ( $id <= 0 ) $this->_ajaxError();

		$escort = new Model_EscortItem();
		$escort->setId($id);

		$photos = $escort->getPhotos();

		$result = array();
		foreach ( $photos as $photo ) {
			$result[] = $photo->hash . '.' . $photo->ext;
		}
		
		die(json_encode(array('result' => 'success', 'data' => $result)));
	}

	public function checkHitsCacheAction()
	{
		$config = Zend_Registry::get('system_config');
		$data = Model_Escorts::getAllCachedHits();

		echo 'Cache Limit: ' . $config['hitsCacheLimit'] . "\n";
		echo 'Total Count: ' . count($data) . "\n\n";

		print_r($data);
		die;
	}

	public function splashAction()
	{
		$this->view->layout()->disableLayout();

		//$escorts = array(142241, 142256, 142257, 142260);

		//Model_Escorts::hit($escorts[rand(0, 3)]);

		/*$this->view->then = $this->_request->then;
		
		if ( $this->_request->enter ) {
			$domain = preg_replace('#^(.+?)([^\.]+)\.([^\.]+)$#', '.$2.$3', $_SERVER['HTTP_HOST']);
			setcookie('18', '1', null, '/', $domain);
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: ' . $this->view->then);
		}*/
	}

    public function feedbackAction()
    {
        $this->isAgency = false;
        $this->view->layout()->disableLayout();

        $this->_request->setParam('no_tidy', true);

        $model = new Model_Escorts();

        $user = Model_Users::getCurrent();

        $is_premium = false;
        if (isset($user->member_data)) {
            $is_premium = $user->member_data['is_premium'];
        }

        $this->view->is_premium = $is_premium;

        // Fetch administrative emails from config
        $config = Zend_Registry::get('feedback_config');
        $site_emails = $config['emails'];


        $feedback = new Model_Feedback();

        // Get data from request
        $data = new Cubix_Form_Data($this->getRequest());
        $fields = array(
            'to' => '',
            'about' => 'int',
            'name' => 'notags|special',
            'email' => '',
            'message' => 'notags|special',
            'rand' => '',
            'agency_id' => 'int'
        );

        $is_ad_feedback = false;
        if ($this->_getParam('ad_id'))
        {
            $fields['ad_id'] = '';
            $is_ad_feedback = true;
        }

        $data->setFields($fields);
        $data = $data->getData();

        $blackListModel = new Model_BlacklistedWords();
        $this->view->data = $data;

        // If the to field is invalid, that means that user has typed by
        // hand, so it's like a hacking attempt, simple die, without explanation
        if (!is_numeric($data['to']) && !in_array($data['to'], array_keys($site_emails))) {
            die;
        }

        // If escort id was specified, fetch it's email and showname,
        // construct the result email template and send it to escort
        
        if( is_int($data['agency_id']) && $data['agency_id'] > 0 ){
            $this->isAgency = true;
            $m_agencies = new Model_Agencies();
            $agency_escort = $model->get($data['to']);
            $this->view->agency = $model  = $m_agencies->getById($data['agency_id']);
            if (!$model) {
                die;
            }

            $data['to_addr'] = $model->email;
            $data['to_name'] = $model->name;
            $data['agency_id'] = $model->id;
            $data['agency_name'] = $model->name;

            $email_tpl = 'agency_feedback';
        }else if (is_numeric($data['to'])) {
            if ( $is_ad_feedback )
            {
                $classified_ad_model = new Model_ClassifiedAds();
                $ad = $this->view->ad = $classified_ad_model->get($data['ad_id']);
                if (!$ad) {
                    die;
                }

                $data['to_addr'] = $ad->email;

                $email_tpl = 'ad_feedback';
            }else{
                $this->view->escort = $model = $model->get($data['to']);
                $this->view->escort = $model;

                // If the escort id is invalid that means that it is hacking attempt
                if (!$model) {
                    die;
                }

                $data['to_addr'] = $model->email;
                $data['to_name'] = $model->showname;
                $data['escort_id'] = $model->id;

                $email_tpl = 'escort_feedback';
            }
        } // Else get administrative email from configuration file
        else {
            $data['to_addr'] = $site_emails[$data['to']];
            $email_tpl = 'feedback_template';
        }

        // In order when the form was posted validate fields
        if ($this->_request->isPost()) {
            $validator = new Cubix_Validator();

            if (!$blackListModel->checkFeedback($data)) {
                $blackListModel->mergeFeedbackData($data);
            }


            if (!strlen($data['email'])) {
                $validator->setError('email', 'Email is required');
            } elseif (!preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email'])) {
                $validator->setError('email', 'Wrong email format');
            } elseif ($blackListModel->checkEmail($data['email'])) {
                $data['flag'] = FEEDBACK_FLAG_BLOCKED_BY_EMAIL;
                $data['status'] = FEEDBACK_STATUS_DISABLED;
                $validator->setError('email', 'This email is blocked');
            }

            if (!strlen($data['message'])) {
                $validator->setError('message', 'Please type the message you want to send');
            } elseif ($blackListModel->checkWords($data['message'])) {
                $data['flag'] = FEEDBACK_FLAG_BLOCKED_BY_WORD;
                $data['status'] = FEEDBACK_STATUS_DISABLED;
                $validator->setError('message', 'You can`t use words "' . $blackListModel->getWords() . '"');
            }

            if (isset($data['flag']) && ($data['flag'] == FEEDBACK_FLAG_BLOCKED_BY_EMAIL || $data['flag'] == FEEDBACK_FLAG_BLOCKED_BY_WORD)) {
                $data['application_id'] = Cubix_Application::getId();
                $feedback->addFeedback($data);
            }




            $result = $validator->getStatus();

            if (!$validator->isValid()) {
                die(json_encode($result));
            } else {
                $sess = new Zend_Session_Namespace('feedback');

                if ($data['rand'] != $sess->rand) {
                    die(json_encode(array('status' => 'success', 'msg' => 'Your feedback has been successfully submitted!')));
                }

                if (isset($data['flag']) && ($data['flag'] == FEEDBACK_FLAG_BLOCKED_BY_EMAIL)) { // IF blocked by email imitate as sent feedback
                    die(json_encode(array('status' => 'success', 'msg' => __('message_sent_to_escort', array('showname' => (!$this->isAgency) ? $model->showname : $model->name)))));
                }
            }


            $data['application_id'] = Cubix_Application::getId();

            if ($config['approvation'] == 1) {
                $data['flag'] = FEEDBACK_FLAG_NOT_SEND;
                $data['status'] = FEEDBACK_STATUS_NOT_APPROVED;
            } else {
                $data['flag'] = FEEDBACK_FLAG_SENT;
                $data['status'] = FEEDBACK_STATUS_APPROVED;

                // Set the template parameters and send it to support or to an escort
                $tpl_data = array(
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'to_addr' => $data['to_addr'],
                    'message' => $data['message']
                );

                if(!$this->isAgency){
                    $tpl_data['escort_showname'] = $model->showname;
                    $tpl_data['escort_id'] = $model->id;
                }else{
                    $tpl_data['agency_name'] = $model->name;
                    $tpl_data['escort_showname'] = $agency_escort->showname;
                    $tpl_data['agency_id'] = $model->id;
                    $tpl_data['escort_url']= 'https://www.'.Cubix_Application::getById()->host.'/escort/'.$agency_escort->showname.'-'.$agency_escort->id;
                }
                $feedbacks_count = $feedback->getBySenderAndReceiverEmails($tpl_data['email'], $tpl_data['to_addr'], 96);
                $total_feedbacks_count = $feedback->getBySenderEmail($tpl_data['email'], 24);

                $limit_validator = new Cubix_Validator();

                if($total_feedbacks_count >= 10 || $feedbacks_count >= 2){
                    $limit_validator->setError('limit', 'riched');

                }
                $result = $limit_validator->getStatus();

                if (!$limit_validator->isValid()) {
                    $result['msg'] = 'Your limit is reached, please try later';
                    die(json_encode($result));
                }
                $ststststs = Cubix_Email::sendTemplate($email_tpl, $data['to_addr'], $tpl_data, $data['email']);

                $config = Zend_Registry::get('newsman_config');

                if ($config) {
                    $list_id = $config['list_id'];
                    $segment = $config['member'];
                    $nm = new Cubix_Newsman($_SERVER["REMOTE_ADDR"]);

                    $subscriber_id = $nm->subscribe($list_id, $data['email'], $data['name']);
                    $is_added = $nm->bindToSegment($subscriber_id, $segment);
                }
            }

            $feedback->addFeedback($data,$this->isAgency);
            $sess->unsetAll();


            if (isset($escort)) {
                if ($is_premium) {
                    $result['msg'] = __('message_sent_to_escort', array('showname' => $escort->showname));
                } else {
                    $result['msg'] = __('message_sent_to_escort', array('showname' => $escort->showname));
                }
            } else {
                $result['msg'] = 'Your feedback has been successfully submitted';
            }

            die(json_encode($result));
			}

			die('error');
	}

    public function feedbackAdsAction()
    {
        $this->_request->setParam('no_tidy', true);

        $m_class = new Model_ClassifiedAds();

        // Fetch administrative emails from config
        $config = Zend_Registry::get('feedback_config');
        $site_emails = $config['emails'];

        $is_ajax = ! is_null($this->_getParam('ajax'));

        $feedback = new Model_Feedback();

        // If the request is ajax, disable the layout
        if ( $is_ajax ) {
            $this->view->layout()->disableLayout();
        }

        // Get data from request
        $data = new Cubix_Form_Data($this->getRequest());

        $fields = array(
            'to' => '',
            'about' => 'int',
            'name' => 'notags|special',
            'email' => '',
            'message' => 'notags|special',
            'captcha' => '',
            'rand' => ''
        );
        $data->setFields($fields);
        $data = $data->getData();

        $blackListModel = new Model_BlacklistedWords();
        $this->view->data = $data;

        // If the to field is invalid, that means that user has typed by
        // hand, so it's like a hacking attempt, simple die, without explanation
        if ( ! is_numeric($data['to']) && ! in_array($data['to'], array_keys($site_emails)) ) {
            die;
        }
        // If escort id was specified, fetch it's email and showname,
        // construct the result email template and send it to escort
        if ( is_numeric($data['to']) ) {

            $ad = $m_class->get($data['to']);
            //var_dump($escort);die;
            // If the escort id is invalid that means that it is hacking attempt
            if ( ! $ad ) {
                die;
            }

            $data['to_addr'] = $ad->email;
            $data['to_name'] = $ad->title;
            $data['escort_showname'] = $ad->title;
            $email_tpl = 'escort_feedback_ads';
        }


        // In order when the form was posted validate fields
        if ( $this->_request->isPost() ) {
            $validator = new Cubix_Validator();

            if ( ! $blackListModel->checkFeedback($data) ){
                $blackListModel->mergeFeedbackData($data);
                // $validator->setError('f_message', 'Do Not repeat The Same Message' /*'Email is required'*/);
            }

            if ( ! strlen($data['email']) ) {
                $validator->setError('email', '' /*'Email is required'*/);
            }
            elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
                $validator->setError('email', '' /*'Wrong email format'*/);
            }elseif( $blackListModel->checkEmail($data['email']) ){
                $data['flag'] = FEEDBACK_FLAG_BLOCKED_BY_EMAIL;
                $data['status'] = FEEDBACK_STATUS_DISABLED;
                //$validator->setError('email', 'This email is blocked');
            }

            if ( ! strlen($data['message']) ) {
                $validator->setError('message','' /*'Please type the message you want to send'*/);
            }
            elseif($blackListModel->checkWords($data['message'])) {
                $data['flag'] = FEEDBACK_FLAG_BLOCKED_BY_WORD;
                $data['status'] = FEEDBACK_STATUS_DISABLED;
                $validator->setError('message', 'You can`t use words "'.$blackListModel->getWords() .'"');
            }

            if( isset($data['flag']) && ($data['flag'] == FEEDBACK_FLAG_BLOCKED_BY_EMAIL || $data['flag'] == FEEDBACK_FLAG_BLOCKED_BY_WORD) ){
                $data['application_id'] = Cubix_Application::getId();
            }

            if ( ! strlen($data['captcha']) ) {
                $validator->setError('captcha', '');//Captcha is required
            }
            else {
                $session = new Zend_Session_Namespace('captcha');
                $orig_captcha = $session->captcha;

                if ( strtolower($data['captcha']) != $orig_captcha ) {
                    $validator->setError('captcha', '');//Captcha is invalid
                }
            }

            $result = $validator->getStatus();

            if ( ! $validator->isValid() ) {
                // If the request was ajax, send the validation result as json
                if ( $is_ajax ) {
                    echo json_encode($result);
                    die;
                }
                // Otherwise, render the phtml and show errors in it
                else {
                    $this->view->errors = $result['msgs'];
                    return;
                }
            }
            else
            {
                $sess = new Zend_Session_Namespace('feedback');

                if ($data['rand'] != $sess->rand)
                {
                    echo json_encode(array('status' => 'success', 'msg' => '<p style="padding:0 10px"><span class="strong" style="color: #060">Your feedback has been successfully submitted!</span></p>'));
                    die;
                }

                if( isset($data['flag']) && ($data['flag'] == FEEDBACK_FLAG_BLOCKED_BY_EMAIL) ) { // IF blocked by email imitate as sent feedback
                    echo json_encode(array('status' => 'success', 'msg' => '<p style="padding:0 10px; margin-top: 50px; width: 330px; font-size: 14px"><span class="strong" style="color: #000">' . __('message_sent_to_escort', array('showname' => $escort->showname)) . '</span></p>'));
                    die;
                }
            }


            $tpl_data = array(
                'name' => $data['name'],
                'email' => $data['email'],
                'to_addr' => $data['to_addr'],
                'message' => $data['message'],
                'escort_showname' => $ad->title,
                'escort_id' => $ad->id
            );

            if ( isset($data['username']) ) {
                $tpl_data['username'] = $data['username'];
            }

            Cubix_Email::sendTemplate($email_tpl, $data['to_addr'], $tpl_data, isset($ad) ? $data['email'] : null);

            $sess->unsetAll();

            if ( $is_ajax ) {

                $result['msg'] = '
					<p style="padding:0 10px"><span class="strong" style="color: #060">Your feedback has been successfully submitted!</span></p>
				';

                echo json_encode($result);
                die;
            }
            else {
                $this->view->success = TRUE;
            }
        }
    }

	public function contactAction()
	{
		$this->_request->setParam('no_tidy', true);

		$this->_request->setParam('to', 'support');

		$model = new Model_Escorts();

		// Fetch administrative emails from config
		$config = Zend_Registry::get('feedback_config');
		$site_emails = $config['emails'];

		$this->view->layout()->setLayout('main-simple');

		$is_ajax = ! is_null($this->_getParam('ajax'));

		// If the request is ajax, disable the layout
		if ( $is_ajax ) {
			$this->view->layout()->disableLayout();
		}

		// Get data from request
		$data = new Cubix_Form_Data($this->getRequest());
		$fields = array(
			'to' => '',
			'about' => 'int',
			'name' => 'notags|special',
			'email' => '',
			'message' => 'notags|special',
			'captcha' => ''
		);
		$data->setFields($fields);
		$data = $data->getData();

		$this->view->data = $data;

		// If the to field is invalid, that means that user has typed by
		// hand, so it's like a hacking attempt, simple die, without explanation
		if ( ! is_numeric($data['to']) && ! in_array($data['to'], array_keys($site_emails)) ) {
			die;
		}

		// If escort id was specified, fetch it's email and showname,
		// construct the result email template and send it to escort
		if ( is_numeric($data['to']) ) {
			$escort = $model->get($data['to']);
			$this->view->escort = $escort;
			//var_dump($escort);die;
			// If the escort id is invalid that means that it is hacking attempt
			if ( ! $escort ) {
				die;
			}

			$data['to_addr'] = $escort->email;
			$data['to_name'] = $escort->showname;
			$data['username'] = $escort->username;
			$email_tpl = 'escort_feedback';
		}
		// Else get administrative email from configuration file
		else {
			$data['to_addr'] = $site_emails[$data['to']];
			$email_tpl = 'feedback_template';
		}

		// In order when the form was posted validate fields
		if ( $this->_request->isPost() ) {
			$validator = new Cubix_Validator();

			if ( ! strlen($data['email']) ) {
				$validator->setError('email', 'Email is required');
			}
			elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
				$validator->setError('email', 'Wrong email format');
			}

			if ( ! strlen($data['message']) ) {
				$validator->setError('message', 'Please type the message you want to send');
			}

			if ( ! strlen($data['captcha']) ) {
				$validator->setError('captcha', 'Captcha is required');
			}
			else {
				$session = new Zend_Session_Namespace('captcha');
				$orig_captcha = $session->captcha;

				if ( strtolower($data['captcha']) != $orig_captcha ) {
					$validator->setError('captcha', 'Captcha is invalid');
				}
			}

			$result = $validator->getStatus();

			if ( ! $validator->isValid() ) {
				// If the request was ajax, send the validation result as json
				if ( $is_ajax ) {
					echo json_encode($result);
					die;
				}
				// Otherwise, render the phtml and show errors in it
				else {
					$this->view->errors = $result['msgs'];
					return;
				}
			}
			
			$sent_by = '';
			if ( $user = Model_Users::getCurrent() )
			{
				$sent_by = $user->user_type.' '.$user->username.' wrote a message';
			}

			if ( $data['about'] ) {
				$about_escort = $model->get($data['about']);
			}
			if ( ! $about_escort ) $about_escort = (object) array('id' => '', 'showname' => '');

			// Set the template parameters and send it to support or to an escort
			$tpl_data = array(
				'name' => $data['name'],
				'email' => $data['email'],
				'to_addr' => $data['to_addr'],
				'message' => $data['message'],
				'escort_showname' => $about_escort->showname,
				'escort_id' => $about_escort->id,
				'sent_by' => $sent_by
			);

			if ( isset($data['username']) ) {
				$tpl_data['username'] = $data['username'];
			}

			Cubix_Email::sendTemplate($email_tpl, $data['to_addr'], $tpl_data, isset($escort) ? $data['email'] : null);
			
			// contact me log
			Cubix_Api::getInstance()->call('contactLog', array($data['to_addr'], $user ? $user->id : null));
			//

			if ( $is_ajax ) {
				if ( isset($escort) ) {
					$result['msg'] = '
						<p style="padding:0 10px"><span class="strong" style="color: #060">' . __('message_sent_to_escort', array('showname' => $escort->showname)) . '</span></p>
					';
				}
				else {
					$result['msg'] = '
						<p style="padding:0 10px"><span class="strong" style="color: #060">Your feedback has been successfully submitted!</span></p>
					';
				}
				//<a href="#" onclick="return Cubix.Feedback.Show(this.getParent(\'.cbox-small-ii\'))">Click here</a> to hide this message</strong>
				echo json_encode($result);
				die;
			}
			else {
				$this->view->success = TRUE;
			}
		}
	}
	
	public function captchaAction()
	{
		
		$this->_request->setParam('no_tidy', true);

		$font = 'css/trebuc.ttf';
		
		$charset = '123456789ABCDEFGHIJKLMNPQRSTUVWXYZ';
		
		$code_length = 5;
		
		$height = 30;
		$width = 90;
		$code = '';
		
		for ( $i = 0; $i < $code_length; $i++ ) 
		{
			$code = $code . substr($charset, mt_rand(0, strlen($charset) - 1), 1);
		}
		
		$rgb[0] = array(204,0,0);
		$rgb[1] = array(34,136,0);
		$rgb[2] = array(51,102,204);
		$rgb[3] = array(141,214,210);
		$rgb[4] = array(214,141,205);
		$rgb[5] = array(100,138,204);
		
		$font_size = $height * 0.4;
		
		$bg = imagecreatefrompng('img/bg_captcha.png');
		$image = imagecreatetruecolor($width, $height);
		imagecopy($image, $bg, 0, 0, 0, 0, imagesx($bg), imagesy($bg));
		$background_color = imagecolorallocate($image, 255, 255, 255);
		$noise_color = imagecolorallocate($image, 20, 40, 100);
		
		for($i = 0; $i<$code_length; $i++)
		{
			$A[] = rand(-20, 20);
			$C[] = rand(0, 5);
			$text_color = imagecolorallocate($image, $rgb[$C[$i]][0], $rgb[$C[$i]][1], $rgb[$C[$i]][2]);
			imagettftext($image, $font_size, $A[$i], 7 + $i * 15, 20 + rand(-3, 3), $text_color, $font , $code[$i]);
		}
		
		$session = new Zend_Session_Namespace('captcha');
		$session->captcha = strtolower($code);
		
		header('Content-Type: image/png');
		imagepng($image);
		imagedestroy($image);
		
		die;
	}
	
	/*public function goAction()
	{
		$link = $_SERVER['QUERY_STRING'];
        if ( ! preg_match('#^(http://|https://)#', $link) ) $link = 'http://' . $link;
		
		header('Location: ' . $link);
		die;
	}*/

	public function robotsAction()
	{
		$this->_request->setParam('no_tidy', true);

		$this->view->layout()->disableLayout();
		$model = new Model_Robots();

		$robots = $model->get();

		header('Content-Type: text/plain; charset=UTF-8', true);
		echo $robots;
		die;
	}

	public function benchmarkAction()
	{
		$db = Zend_Registry::get('db');
		$results = $db->fetchAll('SELECT * FROM debug_benchmark');

		$this->view->results = $results;
	}

	public function listDateImgAction()
	{
		$date = $this->_getParam('date');
		$date = intval($date);
		$date = ( date('d.m.Y') == date('d.m.Y', $date) ? $this->view->t('today_new') : date('d.m.Y', $date) );

		$im = imagecreatetruecolor(54, 15);
		imagefill($im, 0, 0, imagecolorallocate($im, 210, 210, 210));
		imagecolortransparent($im, imagecolorallocate($im, 210, 210, 210));
		
		imagettftext($im, 8, 0, 0, 13, imagecolorallocate($im, 0, 0, 0), 'css/arial.ttf', $date);

		header('Content-Type: image/gif');
		imagegif($im);
		imagedestroy($im);
		die;
	}

	public function photoViewerAction()
	{
		if ( $user = Model_Users::getCurrent() ) {
			$member_data = $user->member_data;
		}
		
		$response = array('status' => 'success', 'msg' => '', 'data' => array());

		$model = new Model_Escorts();

		$showname = $this->_getParam('showname');
		$escort_id = $this->_getParam('escort_id');
		//$escort = $model->get($showname);
		$escort = $model->get($escort_id);

		if ( ! $escort ) {
			$response['status'] = 'error';
			$response['msg'] = 'Showname is required';
		}
		else {
			$photos = $escort->getPhotos(null);

			Model_Escorts::hit_gallery( $escort->id );

			foreach ( $photos as $i => $photo ) {
				$response['data'][$i] = array(
					'id' => $photo->id,
					'private' => ($photo->type == 3) && (! isset($member_data) || ! $member_data['is_premium']),
					'h' => $photo->getUrl('orig'),
					'b' => $photo->getUrl('w514'),
					's' => $photo->getUrl('sthumb')
				);
			}
		}

		$this->_helper->json($response);
	}

	public function chatAction()
	{
		$this->view->layout()->disableLayout();
	}
	
	public function autologinChatAction()
	{
		$this->view->layout()->disableLayout();
		//file_put_contents('/tmp/sms-callback.log', var_export($this->_request, true) );
		
		$user = Model_Users::getCurrent();

		/*$xml = '<?xml version="1.0"?>';*/
		$xml = '';
		
		if ( session_id() == $_REQUEST['uid'] && $user ) {
			$xml .= '<login result="OK">';
		}
		else {
			$xml .= '<login result="FAIL" error="error_authentication_failed">';
		}
		
		$xml .= '<userData>';
		$xml .= '<id>' . $user->id . '</id> ';
		$xml .= '<name><![CDATA[' . $user->username . ']]></name>';
		if ( $user->user_type == 'escort' || $user->user_type == 'agency' ) {
			$xml .= '  <gender>female</gender>';
		}
		else {
			$xml .= '  <gender>male</gender>';
		}
		$xml .= '<level>regular</level>';
		$xml .= '</userData>';
		$xml .= '<friends> </friends>';
		$xml .= '<blocks> </blocks>';
		$xml .= '</login>';
		
		echo $xml;
		
		header("Content-type: text/xml; charset: UTF-8");
		
		exit;
	}

	public function noChatAction()
	{
		$this->view->layout()->disableLayout();
	}

	public function sedcardTotalViewsAction(){
		$this->view->layout()->disableLayout();

		$cache = Zend_Registry::get('cache');

		$escort_id = (int)$this->_request->escort_id;

		if ( !$escort_id ){
			exit;
		}

		$cache_key = "sedcart_view_data_total_".$escort_id;


		$tmp_data = $cache->load(Model_Escorts::HITS_CACHE_KEY);
		$total_count = 0;
		if ( $tmp_data ){
			$total_count = (int)$tmp_data[$escort_id][1];
		}

		if ( (! $total = $cache->load($cache_key)) || $total_count < 2  ) {
			$client = Cubix_Api_XmlRpc_Client::getInstance();
			$data['escort_id'] = $escort_id;
			$total = $client->call('Escorts.getTotalViews', array($escort_id));

			$cache->save($total, $cache_key, array(), 600);
		}

		$this->view->escort_id = $escort_id;
		$this->view->totalViews = $total + $total_count;
		
		$e_model = new Model_Escorts();
		if ( ! $e_model->isNotNewEscort($escort_id) ) {
			$this->view->dont_show = true;
		}
	}
	
	public function linksAction()
	{
		
		$this->view->layout()->topBar_title = $this->view->t('links');
	}

	
    public function v1Action()
    {
       $this->_redirect('/', array('code' => 301));
    }

	public function govazdContactAction()
	{
		$this->view->layout()->disableLayout();

		$req = $this->_request;
		$this->view->package = $req->package;
		$this->view->days = $req->days;
		$this->view->price = $req->price;
	}
	
	public function govazdContactSendMailAction()
	{
		
		$this->view->layout()->disableLayout();

		$req = $this->_request;

		$package = $this->view->package = $req->package;
		$days = $this->view->days = $req->days;
		$price = $this->view->price = $req->price;
		

		if (!$package) die;
					
		if ( $req->isPost() ) {
			$data = new Cubix_Form_Data($this->getRequest());

			$fields = array(
				'name' => 'notags|special',
				'email' => '',
				'message' => 'notags|special',
				'package' => 'notags|special',
				'days' => 'notags|special',
				'price' => 'notags|special',
				'g-recaptcha-response' => ''
			);

			$data->setFields($fields);
			$data = $data->getData();

			$validator = new Cubix_Validator();

			if ( ! strlen($data['name']) ) {
				$validator->setError('name', 'Name is required');
			}
			if ( ! strlen($data['email']) ) {
				$validator->setError('email', 'Email is required');
			} elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
				$validator->setError('email', 'Wrong email format');
			} 

			if ( ! strlen($data['message']) ) {
				$validator->setError('message','Please type the message you want to send' /*'Please type the message you want to send'*/);
			} 

			if ( ! strlen($data['g-recaptcha-response']) ) {
				$validator->setError('g-recaptcha-response', 'Captcha is required');//
			}

			$result = $validator->getStatus();

			if (!$validator->isValid()) {
				echo(json_encode($result));
				ob_flush();	die;
			}
			else {
				$config = Zend_Registry::get('feedback_config');
				$site_emails = $config['emails'];
				$email_tpl = "advertise_inquiry";
				
				$data['application_id'] = Cubix_Application::getId();

				$message = "<p>Package - {$data['package']}</p><p>Days - {$data['days']}</p><p>Price - {$data['price']}</p><p>-----------</p><p>{$data['message']}</p>";

				$tpl_data = array(
					'name' => $data['name'],
					'email' => $data['email'],
					'message' => $message
				);

				Cubix_Email::sendTemplate($email_tpl, 'support@6anuncio.com', $tpl_data);
				echo json_encode($result);
				die;
			}

			$session->unsetAll();
		}

	}
	
	
	public function jsInitAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$view = $this->view->act = $req->view;
		
		$req->setParam('lang_id', $req->lang);
		
		$lang = $this->view->lang = $req->lang;
		
		if ( $view == 'main-page' ) {
			$banners = $this->view->banners = Zend_Registry::get('banners');
		}
		
		else if ( $view == 'main' ) {
			$gender = intval($req->gender);
			$is_agency = intval($req->is_agency);
			$is_tour = intval($req->is_tour);
			$is_upcoming = intval($req->is_upcoming);
			$this->view->filter_params = $req->filter_params;

			$this->view->all_cities = Model_Statistics::getCities(null, $gender, $is_agency, $is_tour, $is_upcoming);
		}
		else if ( $view == 'private' ) {
			
		}
		else if ( $view == 'private-v2' ) {
			
		}
		
		else if ( $view == 'profile' ) {
			$this->view->escort_id = $req->escort_id;
			$this->view->has_no_review = $req->has_no_review;
			$this->view->is_member = $req->is_member;
			$this->view->is_escort = $req->is_escort;
		}
		
		header('Content-type: text/javascript');
		$expires = 60*60*24*14;
		header("Pragma: public");
		header("Cache-Control: maxage=" . $expires);
		header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expires) . ' GMT');
		
	}
	
	public function jsInitExactAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$view = $this->view->act = $req->view;
		
		$req->setParam('lang_id', $req->lang);
		
		$lang = $this->view->lang = $req->lang;
		
		if ( $view == 'new-list' ) {

		}
		else if ( $view == 'list' ) {
			$this->view->escorts_url = $req->escorts_url;
			$this->view->filter_params = $req->filter_params;
			$this->view->is_upcomingtour = $req->is_upcomingtour;
			$this->view->is_tour = $req->is_tour;
		}
		else if ( $view == 'profile' ) {
			
		}
		else if ( $view == 'comment-tabs' ) {
			
		}
		else if ( $view == 'comment-tabs-reversed' ) {
			
		}
		else if ( $view == 'splash' ) {
			
		}	
		else if ( $view == 'signup' ) {
			$this->view->type = $this->_request->type;
		}			
		else if ( $view == 'main' ) {
			$gender = $req->gender;
			$is_agency = $req->is_agency;
			
			$cache = Zend_Registry::get('cache');
			$cache_key = 'auto_comp_cities_' . Cubix_Application::getId() . '_' . $gender . '_' . $is_agency;
			
			$this->view->data = $cache->load($cache_key);
		}
		
		header('Content-type: text/javascript');
		$expires = 60*60*24*14;
		header("Pragma: public");
		header("Cache-Control: maxage=" . $expires);
		header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expires) . ' GMT');
	}
	
	public function emailCollectingAction()
	{
		$this->view->layout()->disableLayout();
		
		$LIST_ID = 109;
		$SEGMENT = 261;
		
		$email = $this->_request->email;
		
		$city = '';
		$cities = Model_Statistics::getCities();
		$new_cities = array_slice($cities, 0, 6);
		
		if ($this->_request->cs)
		{
			$slug = $this->_request->cs;
			$m = new Model_Cities();
			$city = $m->getBySlug($slug);
		}
		
		if ($city)
		{
			foreach ($new_cities as $k => $v)
			{
				if ($v->city_id == $city->city_id)
				{
					unset($new_cities[$k]);
					break;
				}
			}
			
			if (count($new_cities) == 6)
				unset($new_cities[5]);
			
			array_unshift($new_cities, $city);
		}
				
		$this->view->cities = $new_cities;
		
		$m_escorts = new Model_Escort_List();
		$filter = array('e.gender = ?' => GENDER_FEMALE );
		$escorts = $m_escorts->getFiltered($filter, 'random', 1, 5);
		
		foreach ( $escorts as $k => $escort ) {
			$escorts[$k] = new Model_EscortItem($escort);
		}
		
		$this->view->escorts = $escorts;
		
		if ( $this->_request->isPost() ) {
			
			$error = '';
			
			if ( ! $email ) {
				$error = Cubix_I18n::translate('email_required');
			} else if ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $email) ) {
				$error = Cubix_I18n::translate('wrong_email');
			}
			
			if (strlen($error) ) {
				die(json_encode(array('status' => 'error', 'msg' => $error)));
			} else {
				$m_newsman = new Cubix_Newsman($_SERVER["REMOTE_ADDR"]);
				$subscriber_id = $m_newsman->subscribe($LIST_ID, $email, "dd", "dd");

				//$m_newsman->bindToSegment($subscriber_id, $SEGMENT);
                if($subscriber_id){
                    $client = new Cubix_Api_XmlRpc_Client();        
                    $client->call('Application.subscriberLog', array($email));
                }
				
				// add into statistics
				Model_Statistics::addEmailStatistics();
				//
				
				die(json_encode(array('status' => 'success', 'msg'=> "You have successfully subscribed")));
			}
		}
	}

	public function emailCollectingPopupAction()
	{
		$this->view->layout()->disableLayout();

		if ( $this->_request->isPost() ) {
			$email = $this->_request->email;
			$error = '';
			
			if ( ! $email ) {
				$error = Cubix_I18n::translate('email_required');
			} else if ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $email) ) {
				$error = Cubix_I18n::translate('invalid_email');
			}
			
			if (strlen($error) ) {
				die(json_encode(array('status' => 'error', 'msg' => $error)));
			} else {
				Cubix_Email::sendTemplate('newsletter_subscribe_confirmation', $email, array(
					'link'	=> Cubix_Application::getById()->url . $this->view->getLink('email-collecting-popup-confirmed', array('email' => $email, 'hash' => md5(str_repeat($email, 3))))
				), 'support@6anuncio.com', 'Eguk News');
				// info@6anuncio.com changed to support@6anuncio.com ANUN-43
				die(json_encode(array('status' => 'success', 'html' => $this->view->render('index/email-collecting-success.phtml'))));

			}
		} else {
			if ( $this->_request->ec_confirm == 1 ) {
				$email = $this->_request->email;
				$hash = $this->_request->hash;

				if ( md5(str_repeat($email, 3)) != $hash )	{
					setcookie('ecr', 0, time() + 60*60*24*365, '/', 'www.' . Cubix_Application::getById()->host);
				} else {
					setcookie('ecr', 1, time() + 60*60*24*365, '/', 'www.' . Cubix_Application::getById()->host);
					$LIST_ID = 109;
					$SEGMENT = 261;

					$m_newsman = new Cubix_Newsman($_SERVER["REMOTE_ADDR"]);
					$subscriber_id = $m_newsman->subscribe($LIST_ID, $email, "dd", "dd");
					$m_newsman->bindToSegment($subscriber_id, $SEGMENT);

					Model_Statistics::addEmailStatistics();
				}
				
				return $this->_redirect('/');
			} elseif ( isset($this->_request->ec_status) ) {
				if ( $this->_request->ec_status != 1 )  $this->view->error = true;
				$this->_helper->viewRenderer->setScriptAction('email-collecting-confirmed');

				unset($_COOKIE['ecr']);
			    setcookie('ecr', null, -1, '/', 'www.' . Cubix_Application::getById()->host);
			}
		}
	}

	public function oEAction()
	{
		$m = new Model_Escorts();
		
		$escorts = $m->getLoginEscorts();
		
		$arr = array();
		
		if ($escorts)
		{
			foreach ($escorts as $e)
			{
				$arr[$e->escort_id] = $e->showname;
			}
		}
		
		echo json_encode($arr);
		die;
	}
	
	public function paymentFakeAction()
	{
		header('Location:https://www.adsgate123.com/payment.php');
		exit;
	}

	public function phoneImgAction()
	{
		$escort_id = (int) $this->_request->escort_id;
		$agency_id = (int) $this->_request->agency_id;
		$ad_id = (int) $this->_request->ad_id;
		$font_size = (int) $this->_request->fs;
		$width = (int) $this->_request->w;
		$height = (int) $this->_request->h;
		$v = (int) $this->_request->v;
		$color = $this->_request->c;
		$tr = (int) $this->_request->tr;
		$phone = $this->_request->p;
		$add = intval($this->_request->add);

		if (!in_array($add, array(1, 2)))
			$add = NULL;

		$colors = explode(',', $color);		

		if ( ! strlen($phone) ) {
			if ( $escort_id ) {
				$model = new Model_Escorts();
				$cache_key = 'v2_' . $escort_id .  '_profile_' . Cubix_I18n::getLang();
				$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
				$escort = $model->get($escort_id, $cache_key);
				$contacts = $escort->getContacts();

				if (!$add) {
					$filtered_phone =  (isset($contacts->phone_country_id) && !$contacts->disable_phone_prefix) ? '+'.Model_Countries::getPhonePrefixById($contacts->phone_country_id) . ( substr($contacts->phone_number, 0,1) == 0 ? substr($contacts->phone_number, 1) : $contacts->phone_number ) : $contacts->phone_number;
				}
				else {
					$add_phone = $model->getAdditionalPhone($escort_id, $add);
					$filtered_phone =  (isset($add_phone->phone_country_id) && !$add_phone->disable_phone_prefix) ? '+' . Model_Countries::getPhonePrefixById($add_phone->phone_country_id) . $add_phone->phone_additional : $add_phone->phone_additional;
				}
			} else if ( $agency_id ) {
				$m_agencies = new Model_Agencies();
				$agency = $m_agencies->getLocalById($agency_id);
				$filtered_phone =  (isset($agency->phone_country_id)) ? '+'.Model_Countries::getPhonePrefixById($agency->phone_country_id) . $agency->phone : $agency->phone;
			} else if ($ad_id) {
				$m_class = new Model_ClassifiedAds();
				$filtered_phone = $m_class->get($ad_id)->phone;
			}
		} else {
			$filtered_phone = $phone;
		}

		// Set the content-type
		header('Content-Type: image/png');

		/*$expires = 60*60*24*1;
		header("Pragma: public");
		header("Cache-Control: maxage=" . $expires);
		header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expires) . ' GMT');*/

		// Create the image
		$im = imagecreatetruecolor($width, $height);

		// Create some colors
		$white = imagecolorallocate($im, 255, 255, 255);
		$grey = imagecolorallocate($im, 128, 128, 128);
		$black = imagecolorallocate($im, 0, 0, 0);

		array_unshift($colors, $im);
		
		$text_color = call_user_func_array('imagecolorallocate', $colors);

		if ( $tr ) {
			imagefilledrectangle($im, 0, 0, 399, 29, $black);
			imagecolortransparent($im, $black);
		} else {
			imagefilledrectangle($im, 0, 0, 399, 29, $white);
		}
	
		// Replace path by your own font path
		$font = 'css/arial.ttf';

		// Add some shadow to the text
		//imagettftext($im, $font_size, 0, 1, $v + 1, $grey, $font, $filtered_phone);

		// Add the text
		imagettftext($im, $font_size, 0, 0, $v, $text_color, $font, $filtered_phone);

		// Using imagepng() results in clearer text compared with imagejpeg()
		imagepng($im);
		imagedestroy($im);
		die;
	}
	
	public function popunderAction()
	{

        $this->_response->setRawHeader('HTTP/1.1 404 Not Found');
        $this->_forward('error', 'error', null, array('error_msg' => '404 Page Not Found. Please try the following links instead.'));


//		$this->view->layout()->disableLayout();
//		$m = new Model_Escorts();
//		$this->view->escorts = $m->getPopunderEscorts();
	}

	public function checkUserTypeAction(){

	    $this->view->layout()->disableLayout();
	    $user = Model_Users::getCurrent();

	    if($user){
	        die($user->user_type);
        }

        die(false);
    }

    public function advertiseAction()
    {
        if(isset($_SESSION['message'])){
            $this->view->message = $_SESSION['message'];
            unset($_SESSION['message']);
        }
    }

    public function advertiseMmgAction()
    {
        $this->view->layout()->disableLayout();
        $request = $this->_request;

        if (!$this->_request->isPost() && !is_int($request->price)) die();
      
        $mmgBill = new Model_MmgBillAPIV2();
        if( empty($request->price) ) {

            $amount = '';
        } else {

            $amount = stripslashes($request->price);
        }
        
        $_SESSION['payment_mmg_amount'] = $amount;
        $hosted_url = $mmgBill->getHostedPageUrl($amount,time(), false, 'https://www.6anuncio.com'.$this->view->getLink('advertise-mmg-response'));

        if (filter_var($hosted_url, FILTER_VALIDATE_URL) === FALSE) {
            $status = 'not-valid-url';
        }else{
            $status = 'success';
        }

        die(json_encode(array('status' => $status, 'url' =>  $hosted_url)));

    }

    public function advertiseMmgResponseAction()
    {
        $request = $this->_request;
        $payment_result = [];

        if (isset($request->txn_status) and $request->txn_status == 'APPROVED') {
            $payment_result['status'] = $request->txn_status;
            $payment_result['merchant'] = $request->mid;
            $payment_result['ti_mmg'] = $request->ti_mmg;
            $payment_result['ti_mer'] = $request->ti_mer;
            $payment_result['amount'] = isset($_SESSION['payment_mmg_amount']) ? $_SESSION['payment_mmg_amount'] : 0;

            if ($request->mmg_errno != '0000')
                $payment_result['error'] = $request->mmg_errno;

        } else {
            $payment_result['status'] = 'REJECTED';
        }


        $this->view->payment_status = $payment_result['status'];
        $this->view->payment_result = $payment_result;
    }
    
    public function advertisePaymentEmailAction(){
        
        $request = $this->_request;

        $payment_result = (isset($request->payment_data) && !empty($request->payment_data)) ? unserialize($request->payment_data) : [];
        $payment_result['email'] = isset($request->email) ? $request->email : '- - -';
        $payment_result['Link'] = isset($request->link) ? $request->link : '- - -';
        
        $current_user = Model_Users::getCurrent();

        if(!empty($current_user)){

            if ( $current_user->user_type == 'agency' ) {
                $payment_result['User'] = $current_user->agency_data['agency_id'];
            }
            else if ( $current_user->user_type == 'escort' ) {
                $payment_result['User'] = $current_user->escort_data['escort_id'];
            }
            
        }else{
            $payment_result['User'] = 'Was not authenticated';
        }
    
        $payment_details_string = '<ul>';
        foreach($payment_result as $key => $val){
            $payment_details_string .= "<li> <b> $key </b> $val </li>";
        }
        $payment_details_string .= '</ul>';

        $to = 'support@6anuncio.com';
        // $to = 'edhovhannisyan97@gmail.com';
        
        $mail_sent = Cubix_Email::sendTemplate('payment_details', $to, array(
            'details' => $payment_details_string
        ));

        if($mail_sent){
            $message = [
                'type' => "success",
                'text' => "Email has been sent."
            ];
        }else{
            $message = [
                'type' => "danger",
                'text' => "Couldn't send email, please contact our team."
            ];
        }

        $_SESSION['message'] = $message;
        unset($_SESSION['payment_mmg_amount']);
        $this->_redirect('/advertise');
    }
}