<?php

class ClassifiedAdsController extends Zend_Controller_Action
{
	/**
	 * @var Zend_Session_Namespace
	 */
	protected $_session;
	
	protected $_perPages = array(14, 25, 50, 100);

	public function init()
	{
		$this->_session = new Zend_Session_Namespace('classified-ads');
		$this->model = new Model_ClassifiedAds();
		$this->view->per_pages = $this->_perPages;
		$this->view->ads_count_by_categories = $this->model->ads_count_by_categories();
		
	}
	
	public function indexAction()
	{ 
		
		$defines = $this->view->defines = Zend_Registry::get('defines');
		$this->view->cities = $this->model->getAllCitiesForFilter(Cubix_Application::getById()->country_id);
//		print_r($this->model->getAllCitiesForFilter(Cubix_Application::getById()->country_id));die;
		
		$this->view->perPage = $perPage = $this->_getParam('pp', 14);
		
		if ( ! in_array($perPage, $this->_perPages) ) {
			$this->view->perPage = $perPage = 12;
		}
		$req = $this->_request;
		
		$this->view->perPage = $perPage = $this->_getParam('pp', 24);
		
		if ( ! in_array($perPage, $this->_perPages) ) {
			$this->view->perPage = $perPage = 24;
		}
		$page = $req->page;
		if ( $page < 1 ) {
			$page = 1;
		}
		$this->view->page = $page;
		
		$filter = array();

		$category = $req->category;
		$city = (int)$req->city;
		$text = $req->text;
		
		if ( $category ) {
			$filter['category'] = $category;
		}
		
		if ( $city ) {
			$filter['city'] = $city;
		}
		
		if ( strlen($text) ) {
			$filter['text'] = $text;
		}

		$this->view->ads = $this->model->getList($filter, $page, $perPage, $count);
		$this->view->count = $count;

		if($req->ajax){
            $escort_list =  $this->view->render('classified-ads/ajax-list.phtml');
            die(json_encode(array('escort_list' => $escort_list)));
        }
		
		//print_r($this->view->ads);
	}

	public function adAction()
	{
		$id = (int)$this->_request->id;
		
		$this->view->ad = $this->model->get($id);

        $this->model->updateViewCount($id);
		
		if ( ! isset($this->view->ad->id) ) {
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: ' . $this->view->getLink('classified-ads-index'));
			die;
		}
	}

	public function printAction()
	{
		$this->view->layout()->disableLayout();
		
		$ad_id = (int) $this->_request->id;
		
		$this->view->ad = $this->model->get($ad_id);
	}
	
	public function ajaxFilterAction()
	{
		$defines = $this->view->defines = Zend_Registry::get('defines');
		$this->view->cities = $this->model->getAllCitiesForFilter(Cubix_Application::getById()->country_id);
		
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$this->view->category = $category = (int)$req->category;
		$this->view->city = $city = (int)$req->city;
		$this->view->text = $text = $req->text;
	}
	
	public function ajaxListAction()
	{
		$defines = $this->view->defines = Zend_Registry::get('defines');
		$m_cities = new Cubix_Geography_Cities();
		$this->view->cities = $m_cities->ajaxGetAll(null, Cubix_Application::getById()->country_id);
		
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$this->view->perPage = $perPage = $this->_getParam('pp', 25);
		
		if ( ! in_array($perPage, $this->_perPages) ) {
			$this->view->perPage = $perPage = 25;
		}
		$page = $req->page;
		if ( $page < 1 ) {
			$page = 1;
		}
		$this->view->page = $page;
		
		$filter = array();
		
		$category = $req->category;
		$city = (int)$req->city;
		$text = $req->text;
		
		if ( $category ) {
			$filter['category'] = $category;
		}
		
		if ( $city ) {
			$filter['city'] = $city;
		}
		
		if ( strlen($text) ) {
			$filter['text'] = $text;
		}
		
		$this->view->ads = $this->model->getList($filter, $page, $perPage, $count);
		$this->view->count = $count;
	}
	
	public function placeAdAction()
	{
		header("Cache-Control: no cache");
		session_cache_limiter("private_no_expire");
		$defines = $this->view->defines = Zend_Registry::get('defines');
		$m_cities = new Cubix_Geography_Cities();
		$blackListModel = new Model_BlacklistedWords();
		$this->view->cities = $m_cities->ajaxGetAll(null, Cubix_Application::getById()->country_id);
		
		$steps = array(1, 2, 3, 'finish');
		//$edit_mode = false;
		$req = $this->_request;
		$step = $req->getParam('step', 1);

		$edit = (int) $req->getParam('edit');
		$plan = $req->getParam('plan', false);

		$data = array();
		
		if ( !in_array($step, $steps) ) {
			$step = 1;
		}
		
		if ( !$req->isPost() ) {
			$this->_session->unsetAll();
		}else{
			$data = $this->_session->data;
		}
		
		$photos = array();
		

		if ( count($this->_session->photos) ) {
			$photos = $this->_session->photos;
		}
 
		if(!$edit && isset($_FILES)){
			if(isset($req->session_pic0) && $req->session_pic0 == 0){
				$pics_mustbe_deleted[] = key(array_slice($photos, 0, 1, true));
			}
			if(isset($req->session_pic1) && $req->session_pic1 == 0){
				$pics_mustbe_deleted[] = key(array_slice($photos, 1, 1, true));
			}
			if(isset($req->session_pic2) && $req->session_pic2 == 0){
				$pics_mustbe_deleted[] = key(array_slice($photos, 2, 1, true));
			}

			foreach ($pics_mustbe_deleted as $pic_id) {
				unset($photos[$pic_id]);
			}
		}

		$errors = array();
	
		if ( $req->isPost() && !$edit ) 
		{
			
			if ( ! $plan ) {
				try {
					$images = new Cubix_ImagesCommon();
					
					foreach ( $_FILES as $i => $file ) {
						if ( strlen($file['tmp_name']) ) {
							$photo = $images->save($file);
							if ( $photo && count($photos) < 3  ) {
								$photos[$photo['image_id']] = $photo;
							}
						}
					}

					

				} catch ( Exception $e ) {
					$errors['photos'][] = $e->getMessage();
				}
				$category = (int) $req->category;
				$city = (int) $req->city;
				$cities = array($req->city2, $req->city3 );

				$phone = $req->phone;
				$country_prefix = $req->country_prefix;
				$email = $req->email;
				$duration = (int) $req->duration;
				
				$title = trim($req->title);
				
				$text = $req->text;
			

				if ( ! $category ) {
					$errors['category'] = Cubix_I18n::translate('sys_error_required');
				}

				if ( ! $city ) {
					$errors['city'] = Cubix_I18n::translate('sys_error_required');
				}
				if ( ! count($cities) ) {
					$errors['city'] = Cubix_I18n::translate('sys_error_required');
				}

				if ( ! strlen($title) ) {
					$errors['title'] = Cubix_I18n::translate('sys_error_required');
				}
				
				// Removing all image tags exclude that does not have: class="emoji-icon-inline"
				$text = strip_tags(html_entity_decode($text),  '<img>');
				$text = preg_replace('#<img(?!.*?class="emoji-icon-inline").*?>#', '', $text);
				
				
				
				
				if ( ! strlen($text) ) {
					$errors['text'] = Cubix_I18n::translate('sys_error_required');
				}
				else if ($bl_words = $blackListModel->checkWords($text, Model_BlacklistedWords::BL_TYPE_CLASSIFIED_ADS)){

					foreach($bl_words as $bl_word){
						$pattern = '/' . preg_quote($bl_word, '/') . '/';
						$text = preg_replace($pattern, '<abbr title="Blacklisted word" class = "black-listed" >' . $bl_word . '</abbr>', $text);
					}
					$errors['text'] = 'You can`t use word "'.$blackListModel->getWords().'"';
					
				}
				if ( ! strlen($email) && ! strlen($phone) ) {
					$errors['phone_email'] = Cubix_I18n::translate('sys_error_phone_email_required');
				}

				if ( strlen($email) ) {
					$valid = new Cubix_Validator();
					
					if ( ! $valid->isValidEmail($email) ) {
						$errors['invalid_email'] = Cubix_I18n::translate('invalid_email');
					}				
				}

				$data = array(
					'category' => $category,
					'city' => $city,
					'cities' => $cities,
					'phone' => $phone,
					'email' => $email,
					'duration' => $duration,
					'title' => $title,
					'text' => $text,
                    'country_prefix' => $country_prefix
				);
				
				$user = Model_Users::getCurrent();
				$data['user_id'] = null;
			
				if ( $user ) {
					$data['user_id'] = $user->id;
				}
				

				$this->_session->photos = $photos;

				if ( ! count($errors) ) {
					$this->_session->data = $data;
					$step = 2;
				}
			} else {

				/************************* IP ****************************/
				$ip = Cubix_Geoip::getIP();
				
				$data['ip'] = $ip;
				/*********************************************************/

				$m_ads = new Model_ClassifiedAds();		
				
				$data['cities'][] = (string) $data['city'];
				$data['cities'] = array_unique($data['cities']);

				if($data['cities'][0] == $data['cities'][1] || $data['cities'][0] < 0 &&  $data['cities'][1] < 0 ){
					unset($data['cities'][0]);
				}

				if(is_null($data['cities'][0])) {
                    unset($data['cities'][0]);
                }
				
				$ad_id = $m_ads->save( array('data' => $data, 'photos' => $photos));
				
				if(is_array($ad_id)){
					$this->_redirect($this->view->getLink('classified-ads-error') . '?id=' . $ad_id);
				}

				if ( $ad_id ) {
					$this->_session->unsetAll();
				}
				
				if ( $plan == 'free' ) {
				    if(is_array($ad_id)){
                        var_dump($ad_id);die;
                    }
					$this->_redirect($this->view->getLink('classified-ads-success') . '?id=' . $ad_id);
				} else {
					
				}
			}
		}

		$model_country = new Model_Countries();

		$this->view->countries_phone_prefix = $model_country->getPhonePrefixs();
		$this->view->errors = $errors;
		$this->view->step = $step;
		$this->view->data = $data;
		$this->view->photos = $photos;
		$this->view->photos_count = count($photos);
		
		
	}
	
	public function ajaxRemovePhotoAction()
	{
		$image_id = (int) $this->_request->image_id;
		
		$status = array('status' => 'error');
		if ( isset($this->_session->photos[$image_id]) ) {
			unset($this->_session->photos[$image_id]);
			$status = array('status' => 'success');
		}
		
		die(json_encode($status));
	}
	
	public function successAction()
	{
		$this->view->id = (int) $this->_request->id;
	}
	
	public function errorAction()
	{
		
	}
}