<?php

class EscortsController extends Zend_Controller_Action
{
	public static $linkHelper;
	
	public function init()
	{
		self::$linkHelper = $this->view->getHelper('GetLink'); 
	}
	
	
	public function indexAction()
	{
		$this->view->req = $req = $this->_getParam('req');
		$config = Zend_Registry::get('escorts_config');
		
		$cache = Zend_Registry::get('cache');
		if (preg_match('#^\/[0-9]#', $req) ) {
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			die;
		}

		if (strpos($req, '/new/city_') !== false)
		{
			$ar = explode('/', $req);
			unset($ar[1]);
			$uri = implode('/', $ar);
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /escorts' . $uri);
			die;
		}

		if (strlen($_SERVER['REQUEST_URI']) > 1)
		{
			$a = explode('/', $_SERVER['REQUEST_URI']);

			if (!end($a))
			{
				$new_url = substr_replace($_SERVER['REQUEST_URI'], "", -1);
				header('HTTP/1.1 301 Moved Permanently');
				header('Location: ' . $new_url);
				die;
			}
		}
		/**/
		
		/* redirect regions pages to index */
		if (strpos($req, '/region_') !== false)
		{
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			die;
		}
		/**/

		// <editor-fold defaultstate="collapsed" desc="If it is ajax request, disable the layout and render only list.phtml">
		if ( ! is_null($this->_getParam('ajax')) ) {
			$this->view->layout()->disableLayout();
			$this->_helper->viewRenderer->setScriptAction('list');

			$f_price_from = $this->view->f_price_from = (int) $this->_request->price_from;
			$f_price_to = $this->view->f_price_to   = (int) $this->_request->price_to;

			$f_age_from = $this->view->f_age_from = (int) $this->_request->age_from;
			$f_age_to = $this->view->f_age_to   = (int) $this->_request->age_to;

			$f_incall = $this->view->f_incall = (int) $this->_request->f_incall;
			$f_outcall = $this->view->f_outcall   = (int) $this->_request->f_outcall;

			//$f_video = $this->view->f_video   = (int) $this->_request->video;
			$f_verified_contact = $this->view->f_verified_contact   = (int) $this->_request->verified_contact;
			$f_verified = $this->view->f_verified   = (int) $this->_request->verified;
			$f_review = $this->view->f_review   = (int) $this->_request->review;
			$f_video = $this->view->f_video = (int) $this->_request->video;
			$f_independent = $this->view->f_independent = (int) $this->_request->f_independent;
			$f_agency = $this->view->f_agency = (int) $this->_request->f_agency;
			$f_gender = $this->view->f_gender = (int) $this->_request->f_gender;
			$f_city = $this->view->city_id = $this->_request->city_id;

			$external_filters = array(
				'price-from' => $f_price_from,
				'price-to' => $f_price_to,
				'age-from' => $f_age_from,
				'age-to' => $f_age_to,
				'incall' => $f_incall,
				'outcall' => $f_outcall,
				'f_agency' => $f_agency,
				'f_independent' => $f_independent,

				//'video' => $f_video,
				'verified-contact' => $f_verified_contact,
				'verified' => $f_verified,
				'review' => $f_review,
                'video' => $f_video,
                'name' => $this->_request->name
			);
		}
		// </editor-fold>
		
		
		//$this->_request->setParam('req', $req);
		// </editor-fold>
		
		$params = array(
			'sort' => 'newest',
			'filter' => array(array('field' => 'girls', 'value' => array())),
			'page' => 1
		);

		$static_page_key = 'main';
		$is_tour = 0;
		$is_city_tour = false;
		$is_upcoming = 0;
		$gender = GENDER_FEMALE;
		$is_agency = 0;
		
		$req = explode('/', $req);

		if($f_city){
		    $req[] = 'city_gb_'.$f_city;
        }


		$view_type_for_videos = false;

		foreach ( $req as $r ) {
			if ( ! strlen($r) ) continue;
			$param = explode('_', $r);
			
			if ( count($param) < 2 ) {
				
				switch( $r )
				{
					case 'nuove':
						$static_page_key = 'nuove';
						$params = array('sort' => 'new-escorts-list');
						$params['filter'][] = array('field' => 'new_arrivals', 'value' => array());
                        $gender = null;
					break;
					case 'independents':
						$static_page_key = 'independents';
						$params['filter'][] = array('field' => 'independents', 'value' => array());
						$gender = GENDER_FEMALE;
					continue;
					case 'regular':
						$static_page_key = 'regular';
						$params['filter'][] = array('field' => 'regular', 'value' => array());
					continue;
					case 'agency':
						$static_page_key = 'agency';
						$is_agency = 1;
						$params['filter'][] = array('field' => 'agency', 'value' => array());
					continue;
					case 'boys':
						$static_page_key = 'boys';
						$params['filter'][0] = array('field' => 'boys', 'value' => array());
						$gender = GENDER_MALE;
					continue;
					case 'trans':
						$static_page_key = 'trans';
						$params['filter'][0] = array('field' => 'trans', 'value' => array());
						$gender = GENDER_TRANS;
					continue;
					case 'citytours':
						$static_page_key = 'citytours';
						$is_tour = 1;
						$is_city_tour = 1;
						$params['filter'][] = array('field' => 'tours', 'value' => array());
					continue;
					case 'upcomingtours':
						$static_page_key = 'upcomingtours';
						$is_tour = 1;
						$is_upcoming = 1;
						$params['filter'][] = array('field' => 'upcomingtours', 'value' => array());
					continue;
					case 'happy-hours':
						$static_page_key = 'happy-hours';
                        $params['filter'][] = array('field' => 'happy-hours', 'value' => array());
					continue;
					case 'chat-online':
						$static_page_key = 'chat-online';
					continue;
					case 'videos':
                        $view_type_for_videos = true;
						$static_page_key = 'videos';
						$params['filter'][] = array('field' => 'videos', 'value' => array());
					continue;
					default:
						$this->_response->setRawHeader('HTTP/1.1 404 Not Found');
						$this->_forward('error', 'error', null, array('error_msg' => '404 Page Not Found. Please try the following links instead.'));
						return;
				}
			}
			else{
				$param_name = $param[0];
				array_shift($param);

				switch ( $param_name ) {
					case 'region':
					case 'state':
						$params['country'] = $param[0];
						$params['region'] = $param[1];
						$params['filter'][] = array('field' => 'region', 'value' => $param[1]);
					break;
					case 'city':
					case 'zone':
						$params[$param_name] = $param[1];
						$params['filter'][] = array('field' => $param_name, 'value' => array($param[1]));
					break;
					case 'name':
						$has_filter = true;

						$srch_str = "";
						if ( count($req) ) {
							foreach( $req as $r ) {
								if ( preg_match('#name_#', $r) ) {
									$srch_str = str_replace("name_", "", $r);
									$srch_str = str_replace("_", "\_", $srch_str);
								}
							}
						}

						$params['filter'][] = array('field' => 'name', 'value' => array('%' . $srch_str . '%'));

					break;
					/*default:
						if ( ! in_array($param_name, array('nuove', 'independents', 'regular', 'agency', 'boys', 'trans', 'citytours', 'upcomingtours', 'happy-hours', 'chat-online')) ) {
							$this->_response->setRawHeader('HTTP/1.1 404 Not Found');
							$this->_forward('error','error', null, array('error_msg' => '404 Page Not Found. Please try the following links instead.'));
							return;
						}*/
				}
			}
		}

		$filter_params = array(
			'order' => 'e.date_registered DESC',
			'limit' => array('page' => 1),
			'filter' => array()
		);
		
		// FOR ADVANCED FILTER		
		if ( $this->_request->isPost() && $this->_request->name ) {
			$has_filter = true;
			$name = $this->_request->name;
			$srch_str = "";
			
			$srch_str = str_replace("_", "\_", $name);	

			//$params['filter'][] = array('field' => 'name', 'value' => array('%' . $srch_str . '%'));
			//$filter_params['filter']['e.showname LIKE ?'] = '%' . $srch_str . '%';			
			$filter_params['filter']['(e.showname LIKE ? OR e.agency_name LIKE ?)'] = array('%' . $srch_str . '%', '%' . $srch_str . '%');
		}
		
		// FOR ADVANCED FILTER
		
		$this->view->static_page_key = $static_page_key;

        //Katie stupid Logic | EGUK-396
		$_female = ($f_gender) ? $f_gender : GENDER_FEMALE;
		$_trans =  ($f_gender) ? $f_gender : GENDER_TRANS ;
		$_male = ($f_gender) ? $f_gender : GENDER_MALE;

		$rio_url = false;
		$recife_url = false;
		if(strtolower($_SERVER['REQUEST_URI']) == '/escorts/city_br_rio-de-janeiro'){
		    $this->view->rio_url = $rio_url  = true;
        }
        if(strtolower($_SERVER['REQUEST_URI']) == '/escorts/city_br_recife'){
            $this->view->recife_url = $recife_url  = true;
        }

		if(!$f_city && isset($params['city'])){
		    $this->view->city_id = $params['city'];
        }
		// <editor-fold defaultstate="collapsed" desc="Map parameters from url to SQL where clauses">
		$filter_map = array(
			'verified' => 'e.verified_status = 2',
			'brazil' => 'e.nationality_id = 7',
			'videos' => 'e.has_video = 1',
			'happy-hours' => 'e.hh_is_active = 1',
			'age' => 'e.age < ? AND e.age > ?',
			'ethnic' => 'e.ethnicity = ?',
			'height' => 'e.height < ? AND e.height > ?',
			'weight' => 'e.weight < ? AND e.weight > ?',
			'cup-size' => 'e.cup_size = ?',
			'hair-color' => 'e.hair_color = ?',
			'hair-length' => 'e.hair_length = ?',
			'eye-color' => 'e.eye_color = ?',
			'dress-size' => 'e.dress_size = ?',
			'shoe-size' => 'e.shoe_size = ?',
			'available-for-incall' => 'e.incall_type IS NOT NULL',
			'available-for-outcall' => 'e.outcall_type IS NOT NULL',
			'service-for' => 'FIND_IN_SET(?, e.sex_availability)',
			'smoker' => 'e.is_smoking = ?',
			'language' => 'FIND_IN_SET(?, e.languages)',
			'now-open' => 'e.is_now_open',
			
			'region' => 'r.slug = ?',
			'city' => 'ct.slug = ?',
			'cityzone' => 'c.id = ?',
			'zone' => 'cz.slug = ?',
			'name' => 'e.showname LIKE ?',

			// 'new_arrivals' => 'e.gender = ' . $_female,
			'new_arrivals' => 'DATE(e.date_registered) >= DATE(NOW() - INTERVAL 3 MONTH)',
			'independents' => 'eic.is_agency = 0 AND eic.gender = ' . $_female,
			'agency' => 'eic.is_agency = 1 AND eic.gender = ' . $_female,
			'boys' => 'eic.gender = ' . $_male,
			'trans' => 'eic.gender = ' . $_trans,
			'girls' => 'eic.gender = ' . $_female,
						
			'tours' => 'eic.is_tour = 1',
			'upcomingtours' => 'eic.is_upcoming = 1'
		);

		// <editor-fold defaultstate="collapsed" desc="Get page from request parameters">
		if ( $this->_request->page ) {
			$params['page'] = $this->_request->page;
		}
		
		$page = intval($params['page']);
		if ( $page == 0 ) {
			$page = 1;
		}

		$filter_params['limit']['page'] = $page;
		// </editor-fold>

		foreach ( $params['filter'] as $i => $filter ) {

			if ( ! isset($filter_map[$filter['field']]) ) continue;
			
			$value = $filter['value'];
			$filter_params['filter'][$filter_map[$filter['field']]] = $value;
		}
		// </editor-fold>

		// 3 girls from every agency EGUK-446
		if ( $static_page_key == 'main' ) {
			$external_filters['show_on_main'] = 1;
		}
		///

		if ( $static_page_key != 'nuove' ) {
			$default_sorting = "lm";
			$current_sorting = $this->_request->getParam('sort', $default_sorting);

			$sort_types = array(
				'n' => array('title' => 'newest_first', 'map' => 'newest'),
				//'lcv' => array('title' => 'last_contact_verified', 'map' => 'last-contact-verified'),
				'pa' => array('title' => 'price_ascending', 'map' => 'price-asc'),
				'pd' => array('title' => 'price_descending', 'map' => 'price-desc'),
				'lm' => array('title' => 'last_modified', 'map' => 'last-modified'),
				//'lc' => array('title' => 'last_connection', 'map' => 'last-connection'),
				'mv' => array('title' => 'most_viewed', 'map' => 'most-viewed'),
				'r' => array('title' => 'random', 'map' => 'random'),
				'a' => array('title' => 'alphabetically', 'map' => 'alpha'),			
				'ca' => array('title' => 'city_ascending', 'map' => 'city-asc'),
			);

			if ( !array_key_exists($current_sorting, $sort_types) ) {
				$current_sorting = $default_sorting;
				//$this->_request->setParam('sort', $default_sorting);
			}

			if ( $current_sorting != $_COOKIE['sorting'] && $this->_request->isPost() ) {
				setcookie("sorting", $current_sorting, strtotime('+1 year'), "/", "." . Cubix_Application::getById()->host);
				$_COOKIE['sorting'] = $current_sorting;
			}

			if ( isset($_COOKIE['sorting']) && !$current_sorting) {
				$current_sorting = $_COOKIE['sorting'];
				//$this->_request->setParam('sort', $current_sorting);
			}
						
			$tmp = $sort_types[$current_sorting];
			unset($sort_types[$current_sorting]);
			$sort_types = array_merge(array( $current_sorting => $tmp), $sort_types);
			
			$params['sort'] = $sort_types[$current_sorting]['map'];
		}
		else{
			$sort_types = null;
			$current_sorting = 'new-escorts-list';
		}
		$this->view->sort_types = $sort_types;
		$this->view->current_sorting = $current_sorting;
		
		$model = new Model_Escorts();
		$count = 0;
		
		// <editor-fold defaultstate="collapsed" desc="If it is ajax request, disable the layout and render only list.phtml">
		if ( ! is_null($this->_getParam('ajax')) ) {
			
//			$current_filter = $this->getFilterAction();
//			$existing_filters = Model_Escort_List::getActiveFilter($filter_params['filter'], $current_filter, $is_upcoming, $is_tour);
			//var_dump($existing_filters); die;
			$where_query = $this->getFilterAction();

			$have_filter = false;
			if ( count($where_query) ) {
				foreach ( $where_query as $q ) {
					if ( strlen($q['query']) ) {
						$have_filter = true;
						break;
					}
				}
			}

			if ( count($external_filters) ) {
				foreach ($external_filters as $key => $value) {
					if ( $value ) {
						$have_filter = true;
						break;
					}
				}
			}

			$this->view->have_filter = $have_filter;
			
			$filter_body = $this->view->render('escorts/get-filter.phtml');
			
			$this->view->layout()->disableLayout();
			$this->_helper->viewRenderer->setScriptAction('list');
		}
		// </editor-fold>
		
		// <editor-fold defaultstate="collapsed" desc="Cache key generation FOR NEW FILTER">
		$filters_str = '';
		$f_params = $this->_request->getParams();
		$f_params_exclude = array('lang_id', 'module', 'action', 'controller', 'ajax');

		foreach ( $filter_params['filter'] as $k => $filter ) {
			if ( ! is_array($filter) ) {
				$filters_str .= $k . '_' . $filter;
			}
			else {
				$filters_str .= $k;
				foreach($filter as $f) {
					$filters_str .= '_' . $f;
				}
			}
		}

		foreach ( $f_params as $k => $value ) {
			if ( ! in_array($k, $f_params_exclude) ) {
				if ( ! is_array($value) ) {
				$filters_str .= $k . '_' . $value;
				}
				else {
					$filters_str .= $k;
					foreach($value as $f) {
						$filters_str .= '_' . $f;
					}
				}
			}
		}
		// </editor-fold>
		
		$external_filters_string = "";
		if ( count($external_filters) ) {
			foreach ($external_filters as $key => $value) {
				$external_filters_string .= $key . "_" . $value;
			}
		}

		$cache_key = 'v2_' . Cubix_I18n::getLang() . '_' . 	$params['sort'] . '_' . $filter_params['limit']['page'] . '_' . sha1($filters_str . (string) $has_filter . $external_filters_string);
		$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
		$this->view->params = $params;        

		$this->view->is_main_page = $is_main_page = ($static_page_key == 'main' && ( ! isset($params['city']) && ! isset($params['zone']) && ! isset($params['region']) ));

		$this->view->filter_params = base64_encode(json_encode($filter_params['filter']));	
				
		/*if ( $is_main_page ) {
			return $this->_forward('main-agency-split');
		}*/


		if ( $static_page_key == 'chat-online' ) {
			return $this->_forward('chat-online');
		}
		
		//SHOW UPCOMING TOURS BUTTON LOGIC
		$cache_key_tours = 'v2_upcoming_tours_count';
		
		$t_key = isset($params['city']) ? $params['city'] : 'total';
		$tours_count = $cache->load($cache_key_tours);
		if ( ! $tours_count || ! isset($tours_count[$t_key]) ) {
			if ( ! $tours_count ) $tours_count = array();
			$upcoming_filter = array();
			if ( isset($filter_params['filter']['ct.slug = ?']) ) {
				$upcoming_filter['ct.slug = ?'] = $filter_params['filter']['ct.slug = ?'];
			}
			
			Model_Escort_List::getTours(true, $upcoming_filter, 'random', 1, 45, $t_count, $where_query, $external_filters);
			
			$tours_count[$t_key] = $t_count;
			$cache->save($tours_count, $cache_key_tours, array());
		}
		$this->view->show_tours_btn = false;
		if ( ( ! $is_tour || $is_city_tour ) && $tours_count[$t_key] ) {
			$this->view->show_tours_btn = true;
		}
		
		/* Ordering by city from GeoIp */
		if ($params['sort'] == 'random')
		{
			$modelCities = new Model_Cities();
			
			$ret = $modelCities->getByGeoIp();
						
			if (is_array($ret))
			{				
				$cache_key .= '_city_id_' . $ret['city_id'];
				$params['sort'] = $ret['ordering'];
			}
		}
		/**/

		$cache_key .= Cubix_Countries::blacklistCountryCacheKeyCase();

		if ( ! $escorts = $cache->load($cache_key) ) {
			if ( isset($is_tour) && $is_tour ) {
				$escorts = Model_Escort_List::getTours($is_upcoming, $filter_params['filter'], $params['sort'], $filter_params['limit']['page'], $config['perPage'], $count, $where_query, $external_filters);
			}
			else {

				$escorts = Model_Escort_List::getFiltered($filter_params['filter'], $params['sort'], $filter_params['limit']['page'], $config['perPage'], $count, 'regular_list', $where_query, false, $external_filters);

			}
			$cache->save($escorts, $cache_key, array());
			$cache->save($count, $cache_key . '_count', array());
		}else{
			$count = $cache->load($cache_key . '_count');
		}

		if(isset($_GET['test'])) {
		    echo "<pre>";
		    print_r($escorts);
		    die;
        }


		$this->view->filter_params = base64_encode(json_encode($filter_params['filter']));
			
		$this->view->count = $count;
		$this->view->escorts = $escorts;
				
		$this->view->has_filter = $has_filter;
		$this->view->gender = $gender;
		$this->view->is_agency = $is_agency;
		$this->view->is_tour = $is_tour;
		$this->view->is_upcoming = $is_upcoming;

            $this->view->view_type = ($_COOKIE['view-type'] && !$view_type_for_videos) ? $_COOKIE['view-type'] : 'grid';
        $this->view->video_page = $view_type_for_videos;
		
		/* watched escorts */
		$watched_escorts = $this->_watchedEscorts($escorts);
		$this->view->watched_escorts = $watched_escorts;
		/**/
		
		$this->view->html_cache_key = $cache_key . $static_page_key;
		
		//////////////////////////////////////////////////////////////		
		if ($this->view->allowChat2())
		{
			$model = new Model_Escorts();
			$res = $model->getChatNodeOnlineEscorts(1, 1000);
			$ids = $res['ids'];
			$this->view->escort_ids = explode(',', $ids);

		}
		/////////////////////////////////////////////////
		if ( ! is_null($this->_getParam('ajax')) ) {
			$this->view->is_ajax = true;
			$escort_list = $this->view->render('escorts/list.phtml');
			die(json_encode(array('filter_body' => $filter_body, 'escort_list' => $escort_list, 'count' => $count)));
		}
	}
	
	/**
	 * Chat Online Escorts Action
	 */
	public function chatOnlineAction()
	{
		$action = 'index';
		if ( ! is_null($this->_getParam('ajax')) ) {
			$action = 'list';
		}
		$this->_helper->viewRenderer->setScriptAction($action);

		$req = $this->_getParam('req');
		$param = explode('/', $req);
		$page = explode("_", ( isset($param[2]) ) ? $param[2] : '' );

		$ids = '';
	
		if ($this->view->allowChat2())
		{
			$m_e = new Model_Escorts();
			$res = $m_e->getChatNodeOnlineEscorts(1, 1000);
			$ids = $res['ids'];
		}
		
		if (strlen($ids) > 0)
		{
			$filter = array(
				'e.id IN (' . $ids . ')'
			);

			$page_num = 1;
			if ( isset($page[1]) && $page[1] ) {
				$page_num = $page[1];
			}

			$count = 0;
			$escorts = Model_Escort_List::getFiltered($filter, 'random', $page_num, 50, $count);
		}
		else
		{
			$count = 0;
			$escorts = array();
		}

		$this->view->count = $count;
		$this->view->escorts = $escorts;

		list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls();

		$this->view->chat_online = true;
	}

	/**
	 * Happy Hours action
	 */
	/*public function happyHoursAction()
	{
		$action = 'index';
		if ( ! is_null($this->_getParam('ajax')) ) {
			$action = 'list';
		}
		$this->_helper->viewRenderer->setScriptAction($action);

		$req = $this->_getParam('req');
		$param = explode('/', $req);
		$page = explode("_", ( isset($param[2]) ) ? $param[2] : '' );

		$filter = array('e.gender = ?' => 1, 'e.hh_is_active = ?' => 1);

		$page_num = 1;
		if ( isset($page[1]) && $page[1] ) {
			$page_num = $page[1];
		}

		$count = 0;
		$escorts = Model_Escort_List::getFiltered($filter, 'random', $page_num, 10, $count);

		$this->view->count = $count;
		$this->view->escorts = $escorts;

		list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls();

		$this->view->hh_page = true;
	}*/

	private function _watchedEscorts($escorts)
	{
		$ids = array();
		$watched_escorts = array();

		foreach ($escorts as $e)
		{
			$ids[] = $e->id;
		}

		if ($ids)
		{
			$ids_str = trim(implode(',', $ids), ',');
			$user = Model_Users::getCurrent();
			$modelM = new Model_Members();

			if ($user)
			{
				$escorts_watched_type = $user->escorts_watched_type;

				if (!$escorts_watched_type) $escorts_watched_type = 1;

				$res = $modelM->getFromWatchedEscortsForListing($ids_str, $escorts_watched_type, $user->id, session_id());
			}
			else
			{
				$res = $modelM->getFromWatchedEscortsForListing($ids_str, WATCH_TYPE_PER_SESSION, null, session_id());
			}

			if ($res)
			{
				foreach ($res as $r)
				{
					$watched_escorts[] = $r->escort_id;
				}
			}
		}

		return $watched_escorts;
	}
		
	public function getFilterAction($existing_filters = array())
	{
		$this->view->layout()->disableLayout();
        $defines = Zend_Registry::get('defines');
        $this->view->genders = $defines['gender_options'];
		
		if ( ! is_null($this->_getParam('ajax')) && ! is_null($this->_getParam('filter_params')) ) {
			$filter_params = (array) json_decode(base64_decode($this->_request->filter_params));
			$is_upcomingtour = $this->_request->is_upcomingtour;
			$is_tour = $this->_request->is_tour;
			
			$existing_filters = Model_Escort_List::getActiveFilter($filter_params, $current_filter, $is_upcomingtour, $is_tour);
		}
		
		

		$mapper = new Cubix_Filter_Mapper();
		
		//$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'n', 'filter' => 'Nationality', 'field' => 'e.nationality_id', 'filterField' => 'fd.nationality', 'queryJoiner' => 'OR', 'weight' => 10)) );
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array(
			'name' => 's', 'filter' => 'Service', 
			'field' => 'es.service_id', 'filterField' => 'fd.services', 
			'queryJoiner' => 'OR', 'weight' => 10)
		));
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array(
			'name' => 'k', 'filter' => 'Keyword', 
			'field' => 'ek.keyword_id', 'filterField' => 'fd.keywords', 
			'queryJoiner' => 'OR', 'weight' => 11)
		));
		/*$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array(
			'name' => 'af', 'filter' => 'AvailableFor', 
			'field' => 'e.available_for_type', 'queryJoiner' => 'OR', 'weight' => 20)
		));*/
		// $mapper->setParam(new Cubix_Filter_Mapper_Parameter(array(
		// 	'name' => 'ai', 'filter' => 'Incall', 'group' => 'availability', 'groupWeight' => 8,
		// 	'field' => 'e.available_for_type', 'queryJoiner' => 'OR', 'weight' => 21)
		// ));
		// $mapper->setParam(new Cubix_Filter_Mapper_Parameter(array(
		// 	'name' => 'ao', 'filter' => 'Outcall', 'group' => 'availability', 'groupWeight' => 8,
		// 	'field' => 'e.available_for_type', 'queryJoiner' => 'OR', 'weight' => 20)
		// ));
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array(
			'name' => 'l', 'filter' => 'Language', 'group' => 'Others', 'groupWeight' => 20,
			'field' => 'e.languages', 'filterField' => 'fd.language', 
			'queryJoiner' => 'AND', 'weight' => 30)
		));
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array(
			'name' => 'sf', 'filter' => 'ServiceFor', 'group' => 'Others', 'groupWeight' => 20,
			'field' => 'e.sex_availability', 'filterField' => 'fd.service_for', 
			'queryJoiner' => 'OR', 'weight' => 31)
		));
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array(
			'name' => 'hc', 'filter' => 'HairColor', 'group' => 'Physical Appearance', 'groupWeight' => 10,
			'field' => 'e.hair_color', 'filterField' => 'fd.hair_color', 
			'queryJoiner' => 'OR', 'weight' => 130)
		));
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array(
			'name' => 'hl', 'filter' => 'HairLength', 'group' => 'Physical Appearance', 'groupWeight' => 10,
			'field' => 'e.hair_length', 'filterField' => 'fd.hair_length', 
			'queryJoiner' => 'OR', 'weight' => 131)
		));
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array(
			'name' => 'cs', 'filter' => 'CupSize', 'group' => 'Physical Appearance', 'groupWeight' => 10,
			'field' => 'e.cup_size', 'filterField' => 'fd.cup_size', 
			'queryJoiner' => 'OR', 'weight' => 40)
		));
		/*$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array(
			'name' => 'a', 'filter' => 'Age', 'field' => 'e.age', 'filterField' => 'fd.age', 'weight' => 50)
		));*/
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array(
			'name' => 'e', 'filter' => 'Ethnicity', 'group' => 'Physical Appearance', 'groupWeight' => 10,
			'field' => 'e.ethnicity', 'filterField' => 'fd.ethnicity', 
			'queryJoiner' => 'OR', 'weight' => 70)
		));

		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array(
			'name' => 'so', 'filter' => 'Orientation', 'group' => 'Others', 'groupWeight' => 20,
			'field' => 'e.sex_orientation', 'filterField' => 'fd.orientation', 
			'queryJoiner' => 'OR', 'weight' => 32)
		));
		
		
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array(
			'name' => 'h', 'filter' => 'Height', 'group' => 'Physical Appearance', 'groupWeight' => 10,
			'field' => 'e.height', 'filterField' => 'fd.height', 'weight' => 90)
		));
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array(
			'name' => 'w', 'filter' => 'Weight', 'group' => 'Physical Appearance', 'groupWeight' => 10,
			'field' => 'e.weight', 'filterField' => 'fd.weight', 'weight' => 100)
		));
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array(
			'name' => 'ec', 'filter' => 'EyeColor', 'group' => 'Physical Appearance', 'groupWeight' => 10,
			'field' => 'e.eye_color', 'filterField' => 'fd.eye_color', 'queryJoiner' => 'OR', 'weight' => 110)
		));
		
		$filter = new Cubix_FilterV2( array('mapper' => $mapper->getAll(), 'request' => $this->_request, 'existingFilters' => $existing_filters) );

		$this->view->filter = $filter->renderEG();
		$this->view->name = $this->_request->name;
		return $filter->makeQuery();
	}
	
	

    /*public function ajaxBubbleAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		
		$page = intval($this->_getParam('page', 1));
		if ( $page < 1 ) $page = 1;
        $per_page = 7;

		echo $this->view->bubbleTextsWidget($page, $per_page);
    }

    public function ajaxOnlineAction()
	{
        $page = intval($this->_getParam('page', 1));
		if ( $page < 1 ) $page = 1;
        $per_page = 5;
		$this->view->layout()->disableLayout();
                
		if ($this->view->allowChat2())
		{	
			$model = new Model_Escorts();
			$results = $model->getChatNodeOnlineEscorts($page, $per_page);

			$this->view->escorts = $results['escorts'];
			$this->view->per_page = $per_page;
			$this->view->page = $page;
			$this->view->count = $results['count'];
		}
    }*/

	public function profileAction()
	{
		//////   redirect not active escorts to home page   ///////
		$m = new Model_Escorts();
		if ($this->_getParam('f') != 'b' && !$m->getShownameById($this->_getParam('escort_id')))
		{
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			die;
		}
		//////////////////////////////////////////////////////////
		//</editor-fold>
		//$route_name = Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName();
		
		$this->_helper->viewRenderer->setScriptAction("profile/profile");
		
		$this->view->is_profile = true;
		$this->view->is_preview = $is_preview = $this->_getParam('prev', false);
		$this->view->show_advertise_block = $show_advertise_block = $this->_getParam('show_advertise_block', false);
		
		$showname = $this->_getParam('showname');
		$escort_id = $this->_getParam('escort_id');
		
		$model = new Model_Escorts();
		
		$cache_key = 'escort_profile_' . $showname . '_' . $escort_id .  '_' . Cubix_I18n::getLang();
		$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

		$this->view->profile_cache_key = 'html_profile_' . $cache_key;
		
		$escort = $model->get($escort_id, $cache_key, $is_preview);
		
		$escort->total_views = $this->sedcardTotalViews($escort_id);
		$this->view->HandVerificationDate = $escort->getHandVerificationDate();
		
		if ( ! $escort || isset($escort['error']) )
		{
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			die;
			return;
		}
		
		if ($escort->showname != $showname)
		{
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			die;
		}

		$city = new Cubix_Geography_Cities();
		$city = $city->get($escort->base_city_id);
		$this->_request->setParam('region', $city->region_slug);
		$this->view->base_city = $city;
		
		if ($escort->gender == GENDER_MALE){
			$this->_request->setParam('req', "boys");
		}
		elseif ($escort->gender == GENDER_TRANS){
			$this->_request->setParam('req', "trans");
		}
		
		$add_esc_data = $model->getRevComById($escort->id);
		$bl = $model->getBlockWebsite($escort->id);

		$escort->block_website = $bl->block_website;
		$escort->check_website = $bl->check_website;
		$escort->bad_photo_history = $bl->bad_photo_history;
		$escort->disabled_reviews = $add_esc_data->disabled_reviews;
		$escort->disabled_comments = $add_esc_data->disabled_comments;
        $escort->hand_verification = $escort->getHandVerificationDate();
		$this->view->escort_id = $escort->id;
		$this->view->escort = $escort;

		$this->view->filtered_phone =  (isset($escort->phone_country_id) && !$escort->disable_phone_prefix) ? '+'. Model_Countries::getPhonePrefixById($escort->phone_country_id) . ltrim($escort->phone_number, '0') : $escort->phone_number; 
		$this->view->add_phones = $model->getAdditionalPhones($escort->id);
		$this->view->apps = $model->getApps($escort->id);
		
		$this->view->videoConfig = Zend_Registry::get('videos_config');
		$host =  Cubix_Application::getById(Cubix_Application::getId())->host;
		$video = new Model_Video();
		$escort_video = $video->GetEscortVideo($escort->id, 1, ($escort->agency_id) ? $escort->agency_id : null, FALSE);

		if(!empty($escort_video[0]))
		{
			$photo = new Cubix_ImagesCommonItem($escort_video[0][1]);
			$this->view->photo_video = $photo->getUrl('orig');
            $this->view->video = $escort_video[1][0];
            $this->view->host = Cubix_Application::getById(Cubix_Application::getId())->host;
		}
		
		if ( $escort->home_city_id ) {
			$m_city = new Cubix_Geography_Cities();
			$home_city = $m_city->get($escort->home_city_id);

			$m_country = new Cubix_Geography_Countries();
			$home_country = $m_country->get($home_city->country_id);

			$this->view->home_city_country = $home_city->{'title_' . $this->_request->lang_id} . ' (' . $home_country->{'title_' . $this->_request->lang_id} . ')';
		}

		$bt = $model->getBubbleText($escort->id);
		$this->view->bubble_text = $bt ? $bt->bubble_text : null;

		/* Reviews */
		$cache = Zend_Registry::get('cache');
		$cache_key = 'reviews_' . $escort->id;
		if ( ! $res = $cache->load($cache_key) ) {
			try {
				$res = $model->getEscortLastReviews($escort->id, 3);
				
				//$res = Cubix_Api::getInstance()->call('getEscortLastReview', array($escort->id));
			}
			catch ( Exception $e ) {
				$res = array(false, false);
			}
			
			$cache->save($res, $cache_key, array());
		}
		
		list($reviews, $rev_count) = $res;
		$this->view->reviews = $reviews;
		$this->view->rev_count = $rev_count;
		
		/* Comments */
		$model_comments = new Model_Comments();
		$com_count = 0;
		$comments = $model_comments->getEscortComments(1, 3, $com_count, $escort_id);
		
		$this->view->comments = $comments;
		$this->view->com_count = $com_count;
		
		$this->photosV2Action($escort);
		
		if ( $escort->agency_id )
			$this->agencyEscortsAction($escort->agency_id, $escort->id,6);

		$user = Model_Users::getCurrent();
		$this->view->user = $user;

		//<editor-fold defaultstate="collapsed" desc="Back Next buttons logic">
		//Modified by Vahag
		
		$paging_key = $this->_getParam('from');

		if(is_null($paging_key)){
            $paging_key = 'regular_list';
        }

		$paging_keys_map = array(
			'last_viewed_escorts',
			'search_list',
			'regular_list',
			'new_list',
			'profile_disabled_list',
			'premiums',
			'main_premium_spot',
			'tours_list',
			'tour_main_premium_spot',
			'up_tours_list',
			'up_tour_main_premium_spot'
		);

		if ( in_array($paging_key, $paging_keys_map) ) {
			$sid = 'sedcard_paging_' . Cubix_Application::getId();
			$ses = new Zend_Session_Namespace($sid);

			$ses_pages = $ses->{$paging_key};
			$criterias = $ses->{$paging_key . '_criterias'};

			if ( ! isset($criterias['first_page']) ) {
				$criterias['first_page'] = $criterias['page'];
			}
			if ( ! isset($criterias['last_page']) ) {
				$criterias['last_page'] = $criterias['page'];
			}

			$ses_index = 0;
			if ( is_array($ses_pages) ) {
				$ses_index = array_search($escort_id, $ses_pages);
			}
			
			
			if ( isset($ses_pages[$ses_index - 1]) ) {
				$this->view->back_showname = $model->getShownameById($ses_pages[$ses_index - 1]);
				$this->view->back_id = $back_id = $ses_pages[$ses_index - 1];
			} elseif ( $criterias['first_page'] > 1 ) { //Loading previous page escorts
				switch( $paging_key ) {
					case 'regular_list' :
						$prev_page_escorts = Model_Escort_List::getFiltered($criterias['filter'], $criterias['sort'], $criterias['first_page'] - 1);
						break;
					case 'tours_list':
						$prev_page_escorts = Model_Escort_List::getTours($criterias['is_upcoming'], $criterias['filter'], $criterias['sort'], $criterias['first_page'] - 1);
						break;
				}
				
				$p_escorts_count = count($prev_page_escorts);
				if ( count($p_escorts_count) ) {
					$this->view->back_showname = $prev_page_escorts[$p_escorts_count - 1]->showname;
					$this->view->back_id = $prev_page_escorts[$p_escorts_count - 1]->id;
					//Adding previous page escorts to $ses_pages and set in session
					$ids = array();
					foreach($prev_page_escorts as $p_esc) {
						$ids[] = $p_esc->id;
					}

					$criterias['first_page'] -= 1;
					$ses->{$paging_key} = array_merge($ids, $ses_pages);
					$ses->{$paging_key . '_criterias'} = $criterias;
				}
			}

			if ( isset($ses_pages[$ses_index + 1]) ) {
				$this->view->next_showname = $model->getShownameById($ses_pages[$ses_index + 1]);
				$this->view->next_id = $next_id = $ses_pages[$ses_index + 1];
			} else { //Loading next page escorts
				switch( $paging_key ) {
					case 'regular_list' :
						$next_page_escorts = Model_Escort_List::getFiltered($criterias['filter'], $criterias['sort'], $criterias['last_page'] + 1);
						break;
					case 'main_premium_spot':
						$next_page_escorts = array();
						break;
					case 'tours_list':
						$next_page_escorts = Model_Escort_List::getTours($criterias['is_upcoming'], $criterias['filter'], $criterias['sort'], $criterias['last_page'] + 1);
						break;
				}
				
				if ( count($next_page_escorts) ) {
					$this->view->next_showname = $next_page_escorts[0]->showname;
					$this->view->next_id = $next_page_escorts[0]->id;
					//Adding next page escorts to $ses_pages and set in session
					foreach($next_page_escorts as $p_esc) {
						$ses_pages[] = $p_esc->id;
					}

					$criterias['last_page'] += 1;
					$ses->{$paging_key} = $ses_pages;
					$ses->{$paging_key . '_criterias'} = $criterias;
				}
			}
			$this->view->paging_key = $paging_key;
		}
		//</editor-fold>

		/*list($prev, $next) = Model_Escort_List::getPrevNext($escort->showname);
		
		if ( ! is_null($prev) ) {
			$this->view->back_showname = $prev;
		}
		if ( ! is_null($next) ) {
			$this->view->next_showname = $next;
		}*/

		//list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls();
		
		/* Cities list */
		$app = Cubix_Application::getById();
		
		if ($app->country_id)
		{
			$modelCities = new Model_Cities();
			
			$cities = $modelCities->getByCountry($app->country_id);
		}
		else
		{
			$modelCountries = new Model_Countries();
			$modelCities = new Model_Cities();
			
			$countries = $modelCountries->getCountries();
			$this->view->countries_list = $countries;
			
			$c = array();
			
			foreach ($countries as $country)
				$c[] = $country->id;
			
			$cities = $modelCities->getByCountries($c);
		}
		
		$this->view->cities_list = $cities;
		/* End Cities list */
		
		$this->view->escort_user_id = $model->getUserId($escort->id);

		/* watched */
		$model_m = new Model_Members();
		$sess_id = session_id();
		$user_id = null;
		$cur_user = Model_Users::getCurrent();

		if ($cur_user)
		{
			$user_id = $cur_user->id;

			$has = $model_m->getFromWatchedEscortsForUser($sess_id, $escort_id, $user_id, date('Y-m-d'));

			if ($has)
			{
				$model_m->updateToWatchedEscortsForUser($sess_id, $escort_id, $user_id);
			}
			else
			{
				$model_m->addToWatchedEscorts($sess_id, $escort_id, $user_id);
			}
		}
		else
		{
			$has = $model_m->getFromWatchedEscorts($sess_id, $escort_id);

			if ($has)
			{
				$model_m->updateToWatchedEscorts($sess_id, $escort_id);
			}
			else
			{
				$model_m->addToWatchedEscorts($sess_id, $escort_id);
			}
		}
		
		$except_arr[] = $escort->id;
		$agency_girls_ids = array();
		if($this->view->a_escorts){
			foreach($this->view->a_escorts as $agency_escort) {
				$agency_girls_ids[] = $agency_escort->id;
			}
		}
		$except_arr = array_merge($agency_girls_ids, $except_arr);
		if(is_null($escort->gender))$escort->gender = 1;
	
		if($cur_user){
			$watched_escorts = $model_m->getWatchedEscortsIDSForUser($sess_id, $user_id);	
			foreach ($watched_escorts as $watched_escort) {
				$except_arr[] = $watched_escort->escort_id;
			}
		}
		
		$city_coordinates = Model_Cities::getCoordinatesById( $escort->base_city_id );

		
		$nearest_cities = Model_Statistics::getNearestCitiesByCoordinates(
			array('lat' => $city_coordinates->latitude, 'lon' => $city_coordinates->longitude), 6);

		$nearest_cities_ids = array();
		foreach ($nearest_cities as $nearest_city) {
			$nearest_cities_ids[] = $nearest_city->city_id;
		}
		$nearest_cities_ids[] = $escort->base_city_id;
		
		$this->view->recommended = $m->recommendedEscorts( $escort->gender, $except_arr , $nearest_cities_ids ,false,6);
	}
	
	public function photosV2Action($escort = null)
	{
		$config = Zend_Registry::get('escorts_config');
		
		$page = $this->_getParam('photo_page');

		$escort_id = $this->_getParam('escort_id');
		$mode = $this->_getParam('mode');
		
		if( $mode == 'ajax' ) {
			$this->view->layout()->disableLayout();
			$model = new Model_Escorts();
			$escort = $model->get($escort_id);
			$this->view->escort_id = $escort_id;
		}
		
		if ( ! $page )
			$page = 1;
		
		$count = 0;
		if ( $this->view->is_preview || $this->_getParam('is_preview') ) {
			$photos = $escort->getPhotosApi($page, $count, true, true, true);
		}
		else {
			$count = 100;
			$photos = $escort->getPhotos($page, $count, true, false, null, null, false, true, true);
			
			$user = Model_Users::getCurrent();
			if($user->user_type == 'member'){
				$photosPrivate = $escort->getPhotos($page, $count, true, true, null, null, false, true, true);
				$photos = array_merge($photos,$photosPrivate);
			}
		}
		$this->view->photos = $photos;
		$this->view->photos_count = $count;
		$this->view->photos_page = $page;

		$this->view->photos_perPage = $config['photos']['perPage'];
	}

	public function agencyEscortsAction($agency_id = null, $escort_id = null,$limit = false)
	{
		$config = Zend_Registry::get('escorts_config');
		
		if ( ! $agency_id )
		{			
			$this->view->layout()->disableLayout();
			$agency_id = $this->view->agency_id = $this->_getParam('agency_id');
		}
		else
			$this->view->agency_id = $agency_id;
		
		if( ! $escort_id )
			$escort_id = $this->view->escort_id = $this->_getParam('escort_id');
		
		$page = intval($this->_getParam('agency_page'));
		if ( $page < 1 )
			$page = 1;

		$params = array('e.agency_id = ?' => $agency_id,'e.id <> ?' => $escort_id);

		$escorts_count = 0;
		$escorts = Model_Escort_List::getFiltered($params, 'random', $page, ($limit) ? $limit : $config['widgetPerPageV2'], $escorts_count);

		if (count($escorts) ) {
			$this->view->a_escorts = $escorts;
			$this->view->a_escorts_count = $escorts_count;

			$this->view->agencies_page = $page;
			$this->view->widgetPerPage = $config['widgetPerPageV2'];

			$m_esc = new Model_Escorts();
			$agency = $m_esc->getAgency($escort_id);

			$this->view->agency = new Model_AgencyItem($agency);
		}
	}

	public function viewedEscortsAction()
	{
		$this->view->layout()->disableLayout();
		$escort_id = intval($this->_getParam('escort_id'));

		if ( $escort_id )
		{
			self::_addToLatestViewedList($escort_id);
			$viewedEscorts = self::_getLatestViewedList(10, $escort_id);
		}
		else
			$viewedEscorts = self::_getLatestViewedList(10);

		if ( ! is_array($viewedEscorts) || count($viewedEscorts) == 0 )
			return false;
		
		
		
		$ve = array();
		
		$ids = array();
		
		foreach ( $viewedEscorts as $e ) {
			$ids[] = $e['id'];
		}
		
		$filter = array('e.id IN (' . implode(', ', $ids) . ')' => array());
		$order = 'FIELD(eic.escort_id, ' . implode(', ', $ids) . ')';
		
		$count = 0;
		$escorts = Model_Escort_List::getFiltered($filter, $order, 1, 10, $count);
		
		$this->view->escorts = $escorts;
	}

	public function lateNightGirlsAction()
	{
		$this->view->layout()->disableLayout();

		$page = intval($this->_request->l_n_g_page);

		if ($page < 1) $page = 1;
		
		list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls($page);

		$this->view->l_n_g_page = $page;
	}

	private function _getLateNightGirls($page = 1)
	{
		return Model_Escorts::getLateNightGirls($page);
	}

    public function ajaxReportProblemAction()
    {
        $this->view->layout()->disableLayout();
        $url = $_SERVER['HTTP_REFERER'];
        if($this->_request->isPost()){
            // Fetch administrative emails from config
            $config = Zend_Registry::get('feedback_config');
            $support_email = $config['emails']['support'];
            $email_tpl = 'report_problem';
            // Get data from request
            $data = new Cubix_Form_Data($this->getRequest());

            $fields = array(
                'escort_id' => 'int',
                'name' => 'notags|special',
                'email' => '',
                'message' => 'notags|special',

            );
            $data->setFields($fields);
            $data = $data->getData();

            $validator = new Cubix_Validator();

            if ( ! strlen($data['name']) ) {
                $validator->setError('name', 'Your Name is required');
            }

            if ( ! strlen($data['email']) ) {
                $validator->setError('email', 'Your Email is required');
            }
            elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
                $validator->setError('email', 'Wrong email format');
            }
            if ( ! strlen($data['message']) ) {
                $validator->setError('message','Please type the report you want to send');
            }

            $result = $validator->getStatus();

            if ( $validator->isValid() ) {

                // Set the template parameters and send it to support
                $tpl_data = array(
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'message' => $data['message'],
                    'application_id' => Cubix_Application::getId(),
                    'url' => $url
                );
                $data['application_id'] = Cubix_Application::getId();

                try{
                    Cubix_Email::sendTemplate($email_tpl, $support_email, $tpl_data, null);

                    $user_id = null;
                    $user = Model_Users::getCurrent();

                    if ($user && $user->user_type == 'member')
                        $user_id = $user->id;

                    $client = new Cubix_Api_XmlRpc_Client();
                    $client->call('Users.addProblemReport', array($data['name'], $data['email'], null, $data['message'], $data['escort_id'], $user_id));
                }catch ( Exception $e ) {
                    die($e->getMessage());
                }
                die(json_encode($result));
            }
            else {
                die(json_encode($result));

            }

        }
    }

    public function ajaxSuspPhotoAction()
    {
        $this->view->layout()->disableLayout();

        if ($this->_request->isPost())
        {
            // Get data from request
            $data = new Cubix_Form_Data($this->getRequest());

            $fields = array(
                'escort_id' => 'int',
                'link_1' => '',
                'link_2' => '',
                'link_3' => '',
                'comment' => 'notags|special'
            );

            $data->setFields($fields);
            $data = $data->getData();

            $validator = new Cubix_Validator();

            if (!strlen($data['link_1']))
            {
                $validator->setError('link_1', $this->view->t('photo_link_required'));
            }
            elseif (!$validator->isValidURL($data['link_1']))
            {
                $validator->setError('link_1', $this->view->t('photo_link_not_valid'));
            }

            if (strlen($data['link_2']) && !$validator->isValidURL($data['link_2']))
            {
                $validator->setError('link_2', $this->view->t('photo_link_not_valid'));
            }

            if (strlen($data['link_3']) && !$validator->isValidURL($data['link_3']))
            {
                $validator->setError('link_3', $this->view->t('photo_link_not_valid'));
            }

            if (! strlen($data['comment']))
            {
                $validator->setError('comment', $this->view->t('comment_required'));
            }

            $result = $validator->getStatus();

            if ($validator->isValid())
            {
                $data['application_id'] = Cubix_Application::getId();
                $data['status'] = SUSPICIOUS_PHOTOS_REQUEST_PENDING;

                $user = Model_Users::getCurrent();
                $user_id = null;

                if ($user)
                    $user_id = $user->id;

                $data['user_id'] = $user_id;

                $client = new Cubix_Api_XmlRpc_Client();
                $client->call('Users.addSuspPhoto', array($data));
//                die(json_encode())
            }

            die(json_encode($result));
        }
    }

	/*private function _addToLatestViewedList($escort_id)
	{
		$count = 11; // actually we need 10
		$escort = array('id' => $escort_id);
		$sid = 'last_viewed_escorts_' . Cubix_Application::getId();
		
		$ses = new Zend_Session_Namespace($sid);
		
		// Checking if session exists
		// creating if not exists		
		if ( false == (isset($ses->escorts) && is_array($ses->escorts)) )
		{
			$ses->escorts = array();
		}
		else 
		{
			// Checking if escort exists in stack
			// removing escort from stack if exists
			foreach ($ses->escorts as $key => $item)
			{
				if ( $escort['id'] == $item['id'] )
				{
					array_splice($ses->escorts, $key, 1);
				}
			}
		}
		
		// Pushing escort to the end of the stack
		array_unshift($ses->escorts, $escort);
		
		// Checking if
		if ( count($ses->escorts) > $count )
		{	
			array_pop($ses->escorts);
		}
		//var_dump($ses->escorts);
	}*/
	
	// Getting latest viewed escorts list (stack) 
	// as Array of Assoc
	/*public function _getLatestViewedList($max, $exclude = NULL)
	{
		$sid = 'last_viewed_escorts_' . Cubix_Application::getId();
		
		$ses = new Zend_Session_Namespace($sid);

		if ( ! is_array($ses->escorts) )
			return array();

		if ( $exclude )
		{
			$arr = $ses->escorts;
			foreach($arr as $key => $item)
			{
				if($exclude == $item['id'])
				{
					array_splice($arr, $key, 1);
				}	
			}
			$ret = array_slice($arr, 0, $max);
			
			return $arr;
		}

		$ret = array_slice($ses->escorts, 0, $max);
		
		return $ret;
	}*/

    public function gotmAction() {

        http_response_code(301);
        header('Location: /');
        exit;

    }

	
	/*public function gotmAction()
	{
		$this->view->layout()->setLayout('main-simple');
		$show_history = (bool) $this->_getParam('history');

		$page = intval($this->_getParam('page', 1));
		if ( $page < 1 ) $page = 1;
		$this->view->per_page = $per_page = 40;
		$this->view->page = $page;

		$result = Model_Escorts::getMonthGirls($page, $per_page, $show_history);

		$this->view->escorts = $escorts = $result['escorts'];
		$this->view->count = $result['count'];

		if ( ! $show_history ) {
			$this->view->gotm = Model_Escorts::getCurrentGOTM();
		}
		else {
			$this->_helper->viewRenderer->setScriptAction('gotm-history');
		}
	}*/

	/*public function gotmCurrentAction()
	{
		$this->view->layout()->disableLayout();

		try {
			$gotm = Model_Escorts::getCurrentGOTM();

			if ($gotm)
			{
				$escort = new Model_EscortsItem();
				$escort->setId($gotm->id);
				$gotm_photos = $escort->getPhotos();
			}
		}
		catch ( Exception $e ) { $gotm = false; }

		$this->view->gotm = $gotm;
		$this->view->gotm_photos = $gotm_photos;
	}*/

	/**
	 * Voting Widget Action
	 */
	public function voteAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$response = array('status' => null, 'desc' => '', 'html' => '');

		$showname = $this->_getParam('showname');
		$escort_id = $this->_getParam('escort_id');
		$model = new Model_Escorts();
		//$escort = $model->get($showname);
		$escort = $model->get($escort_id);

		$user = Model_Users::getCurrent();

		if ( ! $user ) {
			$response['status'] = 'error';
			$response['desc'] = 'Please sign in as member.';
		}
		elseif ( 'member' != $user->user_type ) {
			$response['status'] = 'dinaid';
			$response['desc'] = 'Only members are allowed to vote';
		}
		elseif ( ! $escort ) {
			$response['status'] = 'error';
			$response['desc'] = 'Invalid escort showname';
		}
		else {
			try {
				$result = $escort->vote($user->id);

				if ( $result !== true ) {
					$response['status'] = 'voted';
					$response['desc'] = 'You have already voted for this escort.';
				}
				else {
					$response['status'] = 'success';
					$response['desc'] =  'Your vote has been successfully placed';

					$response['html'] = $this->view->votingWidget($escort, true);
				}
			}
			catch ( Exception $e ) {
				var_dump($e);die;
				$response['status'] = 'error';
				$response['desc'] = 'unexpected error';
			}
		}

		echo json_encode($response);
	}

	public function getAdvancedFilterAction(){
	    $this->view->layout()->disableLayout();
        $filter = new Cubix_FilterV2( array('mapper' => $mapper->getAll(), 'request' => $this->_request, 'existingFilters' => '') );
        $this->view->filter = $filter->renderEG();
        $this->_helper->viewRenderer->setScriptAction("get-filter");
    }

    public function sedcardTotalViews($escort_id){

        if (!is_numeric($escort_id)) {
            return 1;
        }

        $client = Cubix_Api_XmlRpc_Client::getInstance();
        $total = $client->call('Escorts.getTotalViews', array($escort_id));


        return (int) ($total) ? number_format($total) : 1;

    }
}

