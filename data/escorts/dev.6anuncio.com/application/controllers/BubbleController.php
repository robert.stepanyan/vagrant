<?php
class BubbleController extends Zend_Controller_Action
{	
	public function indexAction()
	{
		if($this->_getParam('ajax')){
			$this->view->layout()->disableLayout();
			$page = $this->_request->page;
			$count = 100;
				
			$client = Cubix_Api_XmlRpc_Client::getInstance();
			try {
				$bubbles = $client->call('Escorts.getBubbleTexts', array($page, $count));
			}
			catch ( Exception $e ) {
				$bubbles = array('texts' => array(), 'count' => 0);
			}
			$this->view->bubbles = $bubbles;
			$this->view->page = $page;
			
		}
	}
}
