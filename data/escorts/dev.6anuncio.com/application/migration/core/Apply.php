<?php

/**
 * Created by Zhora.
 * User: Zhora
 * Date: 04.07.2018
 * Time: 11:44
 */
require_once "Base.php";

class Apply extends Base
{
    private $all_migrations = [];
    private $new_migrations = [];

    public function __construct()
    {
        parent::__construct();

        $this->all_migrations = array_slice(scandir('executable'),2);
        $this->prepareRequest();
        $this->run();
    }


    public function prepareRequest(){
        $this->checkingTable();

        $sql = 'SELECT version from migration';
        $query = $this->_instance->_db->prepare($sql);
        $query->execute();

        $exists_migrations = (array) $query->fetchAll(PDO::FETCH_ASSOC);

        $exists_migrations = array_map(function ($migration){
            return $migration['version'];
        },$exists_migrations);


        foreach ($this->all_migrations as $migration) {
            $migration = basename($migration, '.php');

            if(!in_array($migration,$exists_migrations)){
                $this->new_migrations[] = $migration;

            }
        }
    }

    public function run() {

        foreach ($this->new_migrations as $migration){

            require_once dirname(__DIR__).'/executable/'.$migration.'.php';
            $migration_model = new $migration();

            if($migration_model->up()){
                $this->insertMigration($migration);
            }
        }

        if(count($this->new_migrations) == 0){
            echo "New migrations doesn't exists \n\r ";
        }


    }

    public function checkingTable(){
        $sql = "CREATE TABLE IF NOT EXISTS  `migration` (
                      `version` varchar(180) NOT NULL,
                      `apply_time` int(11) DEFAULT NULL
                    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        $this->_instance->_db->query($sql);
    }

    public function insertMigration($migration_name){
        $sql = "insert into migration (version,apply_time) values ('{$migration_name}',NOW())";
        $query = $this->_instance->_db->prepare($sql);
        $query->execute();
    }
}