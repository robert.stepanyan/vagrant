<?php
/**
 * Created by Zhora.
 * User: Zhora
 * Date: 04.07.2018
 * Time: 13:49
 */


class Migration {
    private $name;

    public function __construct($name) {
        $this->name = $name;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function formatFile(){
        return "<?php
        
require_once dirname(__DIR__).'/core/Base.php';
        
class {$this->name} extends Base{
                
    public function up(){
        try{
            \$sql = 'Put Your Sql Here !';
            \$query = \$this->_instance->_db->prepare(\$sql);
            \$query->execute();
            
            echo  \"{$this->name} migration is done !\\n\\r \";
            return true;
        }catch (Exception \$e){
            var_dump(\$e->getMessage());
            return false;
        }
    } 
}
         
";
    }


}