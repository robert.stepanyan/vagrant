<?php
/**
 * Created by Zhora.
 * User: Zhora
 * Date: 04.07.2018
 * Time: 13:45
 */
require_once "AbstractMigrationBuilder.php";
require_once "Migration.php";

class MigrationBuilder extends AbstractMigrationBuilder{

    private $file;
    private $name;
    private $file_name;
    private $file_data;

    public function __construct($name) {
        $this->name = $name;
        $this->buildFile();
        $this->saveFile();
    }

    function buildFile() {
       $this->file_name = 'm_'.uniqid().'_'.$this->name;
       $this->file = new Migration($this->file_name);
       $this->file_data = $this->file->formatFile();
    }

    function saveFile(){
        try{
            file_put_contents('./executable/'.$this->file_name.'.php',$this->file_data);
        }catch (Exception $e){
            var_dump($e->getMessage());
        }
    }
}