<?php

/**
 * Created by Zhora.
 * User: Zhora
 * Date: 04.07.2018
 * Time: 11:48
 */
class DBConnection
{
    public $_db;
    private $_config;
    static $_instance;

    private function __construct()
    {

        $this->initConfigs();

        $this->_db = new PDO("mysql:host={$this->_config['host']};dbname={$this->_config['dbname']}", $this->_config['user'], $this->_config['password']);

        $this->_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    private function __clone()
    {
    }

    public static function getInstance()
    {
        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function initConfigs()
    {

        if ( $_SERVER['USER'] == 'vagrant' ) {
            //Development Configs
            $this->_config = array(
                'host' => '10.10.0.67',
                'dbname' => 'escortguide_v2',
                'user' => 'sceon',
                'password' => 'sceon123456',
            );
        } else {
            //Production Configs
            $this->_config = array(
                'host' => 'localhost',
                'dbname' => 'escortguide_v2',
                'user' => 'root',
                'password' => '',
            );
        }
    }
}