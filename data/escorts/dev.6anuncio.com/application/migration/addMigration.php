<?php

require_once "core/builder/MigrationBuilder.php";

$options = getopt('n:');

if(!isset($options['n']) || is_null($options['n'])) {
    die("ERROR : Please write migration name Example | php addMigration.php -n=migration-name \n\r");
}


new MigrationBuilder($options['n']);



