<?php

class Zend_View_Helper_AllowChat2
{
	public function allowChat2()
	{
		$request = Zend_Controller_Front::getInstance()->getRequest();
		
		if ( $request->getParam('chatVersion') != 2 ) 
		{
			return false;
		}
		
		return true;
	}
}

