<?php
class Zend_View_Helper_HumanTimingDaily extends Zend_View_Helper_Abstract
{
	function humanTimingDaily($time){

	    $time = time() - $time; // to get the time since that moment
	    $time = ( $time < 1 ) ? 1 : $time;
	  
	    $tokens = array (
			31536000 => 'y',
			2592000 => 'm’th',
			604800 => 'w',
			86400 => 'd',
			3600 => 'h',
			60 => 'm',
			1 => 's'
	    );

	    foreach ( $tokens as $unit => $text ) {
	        if ( $time < $unit ) continue;
	        $numberOfUnits = floor($time / $unit);

	        if($format){
	        	 // You can add "s" when unit is multiple 
	        	$text .= (($numberOfUnits>1)?'s':'');
	        }
	       
	      	$howlong = $numberOfUnits . $text. ' ago';
	      	
	      	if($text == 's' || $text == 'm' || $text == 'h'){
				$howlong = __('escorts_added_today'); //escorts_added_today
	      	}

	      	if($text == 'd'){
	      		if($numberOfUnits>1){
	      			$howlong = __('escorts_added_days_ago', array( 'DAYS' => $numberOfUnits )); //'Escorts added '.$numberOfUnits.' days ago'; //escorts_added_days_ago %DAYS%
	      		}else{
	      			$howlong = __('escorts_added_yesterday'); //escorts_added_yesterday
	      		}
	      	}

	      	//__( 'agencies_n_girls',[ 'count'=>3 ] )

	      	if($text == 'w'){
	      		if($numberOfUnits>1){
	      			$howlong = __('escorts_added_week_ago', array( 'WEEKS' => $numberOfUnits )); //escorts_added_week_ago
	      		}else{
	      			$howlong = __('escort_added_one_week_ago'); //escort_added_one_week_ago
	      		}
	      	}

	      	if($text == 'm’th'){
	      		if($numberOfUnits>1){
	      			$howlong = __('escorts_added_months_ago', array( 'MONTHS' => $numberOfUnits )); //escorts_added_months_ago
	      		}else{
	      			$howlong = __('escorts_added_one_month_ago'); //escorts_added_one_month_ago
	      		}
	      	}

	      	if($text == 'y'){
	      		if($numberOfUnits>1){
	      			$howlong = __('escorts_added_years_ago', array( 'YEARS' => $numberOfUnits )); //escorts_added_years_ago
	      		}else{
	      			$howlong = __('escorts_added_one_year_ago'); //escorts_added_one_year_ago
	      		}
	      	}

	        return $howlong;
	    }
	}
}