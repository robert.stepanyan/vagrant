<?php

class Zend_View_Helper_ListSetcard extends Zend_View_Helper_Abstract
{
	public function listSetcard($escort, $in_premium = false, $c = 1, $thumb_size = 'thumb_v2',$is_video = false)
	{
		$this->view->escort = $escort;
		$this->view->is_video = $is_video;
		$this->view->in_premium = $in_premium;
        $this->view->config = Zend_Registry::get('escorts_config');
        $this->view->c = $c;
        $this->view->thumb_size = $thumb_size;
		return $this->view->render('escorts/list-setcard.phtml');
	}
}
