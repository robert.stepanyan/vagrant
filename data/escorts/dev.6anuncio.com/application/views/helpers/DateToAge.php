<?php

class Zend_View_Helper_DateToAge extends Zend_View_Helper_Abstract
{
	public function dateToAge($timestamp)
	{
		return Cubix_Utils::dateToAge($timestamp);
	}
}
