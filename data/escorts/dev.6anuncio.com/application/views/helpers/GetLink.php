<?php

class Zend_View_Helper_GetLink
{
	protected static $_params;
	
	public function getLink($type = '', array $args = array(), $clean = false, $no_args = false)
	{
		$lang_id = Cubix_I18n::getLang();

		$link = '/';
		/*if ( $lang_id != Cubix_Application::getDefaultLang() ) {
			$link .= $lang_id . '/';
		}*/
		
		if ( isset($args['state']) && 7 != Cubix_Application::getId() ) {
			$args['region'] = $args['state'];
			unset($args['state']);
		}
		
		switch ( $type ) {

			case 'photo-feed-index':
				$link .= 'photo-feed';
				break;
			case 'photo-feed-ajax-list':
				$link .= 'photo-feed/ajax-list?ajax';
				break;
			case 'photo-feed-ajax-header':
				$link .= 'photo-feed/ajax-header?ajax';
				break;
			case 'photo-feed-ajax-voting-box':
				$link .= 'photo-feed/ajax-voting-box/';
				break;

			case 'photo-feed-ajax-voting-box':
				$link .= 'photo-feed/ajax-voting-box/';
				break;
			case 'photo-feed-ajax-more-photos':
				$link .= 'photo-feed/ajax-get-more-photos/';
				break;
			case 'photo-feed-ajax-vote':
				$link .= 'photo-feed/ajax-vote/';
				break;			

			case 'latest-actions-index':
				$link .= 'latest-actions';
				break;
			case 'latest-actions-ajax-header':
				$link .= 'latest-actions/ajax-header?ajax';
				break;
			case 'latest-actions-ajax-list':
				$link .= 'latest-actions/ajax-list?ajax';
				break;
			case 'latest-actions-ajax-get-details':
				$link .= 'latest-actions/ajax-get-details?ajax';
				break;
			case 'glossary':
				$link .= 'glossary';
				break;
            case 'feedback-ads':
                $link .= 'feedback-ads';
                break;
            case 'classified-ads-index':
                $link .= 'classified-ads';
                break;
            case 'classified-ads-place-ad':
                $link .= 'classified-ads/place-ad';
                break;
            case 'classified-ads-success':
                $link .= 'classified-ads/success';
                break;
             case 'classified-ads-error':
                $link .= 'classified-ads/error';
                break;
            case 'classified-ads-ajax-filter':
                $link .= 'classified-ads/ajax-filter?ajax';
                break;
            case 'classified-ads-ajax-list':
                $link .= 'classified-ads/ajax-list?ajax';
                break;
            case 'classified-ads-print':
                $link .= 'classified-ads/print';
                break;
            case 'classified-ads-ad':
				$link .= 'classified-ads/ad/' . $args['id'];
				unset($args['id']);
				unset($args['title']);
				break;
			case 'planner':
				$link .= 'planner';
			break;
			case 'advertise':
				$link .= 'advertise';
			break;
			case 'advertise-mmg-response':
				$link .= 'advertise-mmg-response';
			break;
			case 'contact-us':
				$link .= 'contacts/contact-us';
			break;
			case 'cityalerts':
				$link .= 'city-alerts';
			break;
			case 'get-filter':
				$link .= 'escorts/get-filter';
				break;	
			
			case 'forum':
				$link = 'http://www.escort-annonce.com/forum/?lang=' . $lang_id;
				break;
			case 'chat':
				$link = 'http://chat.escort-annonce.com/';
				break;
			case 'cams':
				$link = 'http://escort-annonce.streamray.com/';
				break;
			case 'search':
				$link .= 'search';
				break;
			case 'escorts-list':
				$link .= 'escorts';
			break;
			case 'base-city':
				$section = isset($args['section']) ? $args['section'] . '/' : '';
				if ( strlen($section) ) unset($args['section']);

				$link .= 'escorts/' . $section . 'city_' . $args['city'];
				unset($args['city']);
			break;
			case 'escorts':
				if ( empty(self::$_params) ) {
					$request = Zend_Controller_Front::getInstance()->getRequest();
					$req = trim($request->getParam('req', ''), '/');
					$req = explode('/', $req);
					
					$params = array();
					
					foreach ($req as $r)  {
						$param = explode('_', $r);
						if ( count($param) < 2 ) {
							$params[] = $r;
							continue;
						}
						
						$param_name = reset($param);
						array_shift($param);
						$params[$param_name] = implode('_', $param);
					}
					
					self::$_params = $params;
				}
				
				if ( $clean ) {
					$params = array();
				}
				else {
					$params = self::$_params;
				}
				
				foreach ( $args as $key => $value ) {
					if ( is_null($value) ) {
						unset($params[$key]);
						continue;
					}
					
					$params[$key] = $value;
				}
				
				if($lang_id == 'pt'){
					$link .= 'acompanhantes/';
				} else{
					$link .= 'escorts/';
				}
				foreach ( $params as $param => $value ) {
					if ( ! strlen($param) || ! strlen($value)) continue;
					if ( is_int($param) ) {
						if ( $value == 'tours' ) $value = 'citytours';
						$link .= $value . '/';
					}
					elseif ( strlen($param) ) {
						if ( $param == 'page' ) {
							if ( $value - 1 > 0 ) {
								$link .= ($value - 1) . '/';
							}
						}
						else {
							$link .= $param . '_' . $value . '/';
						}
					}
				}
				
				$link = rtrim($link, '/');
				
				$args = array();
			break;
			case 'gotm':
				$link .= 'girl-of-month';

				if ( isset($args['history']) && $args['history'] ) {
					$link .= '/history';
				}
				
				if ( isset($args['page']) ) {
					if ( $args['page'] != 1 ) {
						$link .= '/page_' . $args['page'];
						//$link .= '/page_' . $args['page'];
					}
					
					unset($args['page']);
				}
				break;
			case 'profile':
				if($lang_id == 'pt'){
					$link .= 'acompanhante/' . $args['showname'] . '-' . $args['escort_id'];
				} else{
					$link .= 'escort/' . $args['showname'] . '-' . $args['escort_id'];
				}
				unset($args['showname']);
				unset($args['escort_id']);
			break;
			case 'forgot':
				$link .= 'account/forgot';
			break;
			case 'signup':
				$link .= 'account/signup';
			break;
			case 'signin':
				$link .= 'account/signin';
			break;
			case 'signout':
				$link .= 'account/signout';
			break;
			case 'feedback':
				$link .= 'feedback';
			break;
			case 'contact':
				$link .= 'contact';
			break;
			case 'links':
				$link .= 'links';
			break;
		
			case 'captcha':
				$link .= 'captcha?' . rand();
				//$link = '/img/sample_captcha.gif?' . rand();
			break;
			case 'viewed-escorts':
				$link .= 'escorts/viewed-escorts';
			break;
			
			
			case 'agency-profile':
				$link .= 'agency/' . $args['agency_slug'] . '-' . $args['agency_id'];
				unset($args['agency_slug']);
				unset($args['agency_id']);
			break;
			case 'agency-escorts':
				$link .= 'agency/' . $args['agency_slug'] . '-' . $args['agency_id'] . '/escorts';
				unset($args['agency_slug']);
				unset($args['agency_id']);
				break;
			
			case 'search':
				$link .= 'search';
			break;

			case 'late-night-girls':
				$link .= 'escorts/late-night-girls';
			break;
			
			case 'terms':
				$link .= 'page/terms-and-conditions';
			break;
			case 'webcam-lm':
				$link .= 'page/webcam-verification-learn-more';
			break;
			case 'passport-lm':
				$link .= 'page/passport-verification-learn-more';
			break;
			
			case 'external-link':
				$link = '/go?' . $args['link'];
				unset($args['link']);
			break;

			case 'ixs-banner-url':
				$link = 'https://www.6anuncio.com/images/ixsBanners/'.$args['filename'];
				unset($args['filename']);
			break;

			// Private Area
			case 'private':
				$link .= 'private#index';
				break;
			case 'private_no_index':
				$link .= 'private';
				break;
			case 'private-settings':
				$link .= 'private/settings';
				break;
			case 'private-statistics':
				$link .= 'private/statistics';
				break;
			case 'private-happy-hour':
				$link .= 'private/happy-hour';
				break;
			case 'private-client-blacklist':
				$link .= 'private/client-blacklist';
				break;
			case 'private-add-client-to-blacklist':
				$link .= 'private/add-client-to-blacklist';
				break;
			case 'private-profile':
				$link .= 'private/profile#index';
				break;
			case 'private-complete-profile':
				$link .= 'private#profile/index';
				break;
			case 'private-photos':
				$link .= 'private/photos';
				break;
			case 'private-plain-photos':
				$link .= 'private/plain-photos';
				break;
			case 'private-tours':
				$link .= 'private/tours/index';
				break;
			case 'private-ajax-tours':
				$link .= 'private/ajax-tours';
				break;
			case 'private-ajax-tours-add':
				$link .= 'private/ajax-tours-add';
				break;
			case 'private-ajax-tours-remove':
				$link .= 'private/ajax-tours-remove';
				break;
			case 'private-verify':
				$link .= 'private/verify';
				break;
			case 'private-faq':
				$link .= 'private/faq';
				break;
			case 'private-premium':
				$link .= 'private/premium';
				break;
			case 'private-100-verification':
				$link .= 'private/verify#index';
				break;
			case 'private-v2-settings':
				$link .= 'private#settings';
				break;
			case 'private-v2-escort-status':
				$link .= 'private#escort-status';
				break;	
            case 'private-100-verification-start':
                $link .= 'private/verify#idcard';
                break;
            case 'private-v2-client-blacklist':
                $link .= 'private#client-blacklist';
                break;
			case 'private-support':
				$link .= 'private/support#index';
				break;
			case 'private-premium':
				$link .= 'private#premium';
				break;
			case 'private-city-alerts':
				$link .= 'private/city-alerts#index';
				break;
			case 'private-get-urgent-message':
				$link .= 'private/get-urgent-message';
				break;
			case 'private-set-urgent-message':
				$link .= 'private/set-urgent-message';
				break;
			case 'private-get-rejected-verification':
				$link .= 'private/get-rejected-verification';
				break;
			case 'private-set-rejected-verification':
				$link .= 'private/set-rejected-verification';
				break;
			case 'alerts':
				$link .= 'private/alerts';
				break;
			case 'private-agency-profile':
				$link .= 'private#agency-profile';
				break;
			case 'private-member-profile':
				$link .= 'private#member-profile';
				break;
			case 'private-member-reviews':
				$link .= 'private#reviews';
				break;
			case 'private-upgrade-premium':
				$link .= 'private/upgrade';
				break;
			case 'ticket-open':
				$link .= 'support/ticket';
				break;
			case 'private-escorts':
				$link .= 'private#escorts';
				break;
			case 'private-gallery':
				$link .= 'private/profile#gallery';
				break;
			case 'private-video':
				$link .= 'private/profile#video';
				break;
			case 'private-show-escorts':
				$link .= 'private#show-escorts';
				break;
			case 'private-billing':
				$link .= 'private/billing/index/';
				break;
			case 'ob-mmg-postback':
				$link .= 'private/billing/mmg-postback';	
				break;	
            case 'private-escorts-delete':
				$link .= 'private/profile-delete';
				break;
             case 'private-escorts-restore':
				$link .= 'private/profile-restore';
				break;
			case 'reviews':
				$link .= 'reviews';
				break;
			case 'escort-reviews':
				$link .= 'reviews/' . $args['showname'] . '-' . $args['escort_id'];
				unset ($args['showname']);
				unset ($args['escort_id']);
				break;
			case 'escort-add-review':
				$link .= 'reviews/add-review/' . $args['escort_id'];
				
				unset ($args['escort_id']);
				break;
            case 'pm':
                $link .= 'pm#' . $args['user_id'];

                unset ($args['user_id']);
                break;
			case 'member-reviews':
				$link .= 'reviews/member/' . $args['username'];
				unset ($args['username']);
				break;
            case 'member-info':
                $link .= 'member/' . $args['username'];
                unset ($args['username']);
                break;
			case 'escorts-reviews':
				$link .= 'reviews/escorts';
				break;
			case 'agencies':
				$link .= 'agencies';
				break;
			case 'agencies-reviews':
				$link .= 'reviews/agencies';
				break;
			case 'top-reviewers':
				$link .= 'reviews/top-reviewers';
				break;
			case 'top-ladies':
				$link .= 'reviews/top-ladies';
				break;
			case 'voting-widget':
				$link .= 'escort/' . $args['showname'] . '-' . $args['escort_id'] . '/vote';
				unset($args['showname']);
				unset($args['escort_id']);
				break;
			case 'private-reviews':
				$link .= 'private/reviews';
				break;
			case 'private-escort-reviews':
				$link .= 'private/escort-reviews';
				break;
			case 'private-escorts-reviews':
				$link .= 'private/escorts-reviews';
				break;
			case 'private-add-review':
				$link .= 'private/add-review';
				break;
			case 'private-add-reply';
				$link .= 'private/add-reply';
				break;
			case 'private-ajax-escort-comments';
				$link .= 'private/ajax-escort-comments';
				break;
			case 'reviews-search':
				$link .= 'reviews/search';
				break;
            case 'comments':
				$link .= 'comments';
				break;
			case 'signup-as-vip-member':
				$link .= 'private/signup-vip-member?type=vip-member';
				break;
			case 'members-choice':
				$link .= 'members-choice';
				break;
			case 'member-info':
				$link .= 'member/'.$args['username'];
				unset($args['username']);
				break;
			case 'member-comments':
				$link .= 'members/get-member-comments';
				break;
			case 'tell-friend':
				$link .= 'escorts/ajax-tell-friend';
				break;
			case 'susp-photo':
				$link .= 'escorts/ajax-susp-photo';
				break;
			case 'report-problem':
				$link .= 'escorts/ajax-report-problem';
				break;
			case 'email-collecting-popup-confirmed':
				$args['ec_confirm'] = 1;
				$link .= 'index/email-collecting-popup';
				break;
			case 'videos':
				$link .= 'escorts/videos';
				break;
			case 'host':
				$link .= '';
			break;
            
		}

		if ( ! $no_args ) {
			if ( count($args) ) {
				$link .= '?';
				$params = array();
				foreach ( $args as $arg => $value ) {
					if ( ! is_array($value) ) {
						$params[] = $arg . '=' . urlencode($value);
					}
					else {
						foreach ( $value as $v ) {
							$params[] = $arg . '[]=' . $v;
						}
					}
				}

				$link .= implode('&', $params);
			}
		}
		
		return $link;
	}
}
