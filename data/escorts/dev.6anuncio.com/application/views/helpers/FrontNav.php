<?php
class Zend_View_Helper_FrontNav extends Zend_View_Helper_Abstract
{
	public function frontNav($hide = array())
	{		
		$navs = array();
		$user_loged = Model_Users::getCurrent();
		$is_loged = (!$user_loged ? false : true );
         
		$navs['escorts'] = array( 'title' => __('escorts'), 'icon' => '',  'link' => '');

		$navs['escorts']['sub']['new_escorts'] =  array('title' => __('new_escorts'), 'icon' => '',
			'link' => $this->view->GetLink('escorts', array('nuove'), true) );

		$navs['escorts']['sub']['independent_Ladies'] = array( 'title' => __('independent_ladies'), 'icon' => '',
			'link' => $this->view->GetLink('escorts', array('independents'), true)	);

		$navs['escorts']['sub']['men'] = array( 'title' => __('men'), 'icon' => '',
			'link' => $this->view->GetLink('escorts', array('boys'), true) );

		$navs['escorts']['sub']['trans'] = array( 'title' => __('trans'), 'icon' => '',
			'link' => $this->view->GetLink('escorts', array('trans'), true) );

		$navs['escorts']['sub']['city_tours'] = array( 'title' => __('tours'), 'icon' => '',
			'link' => $this->view->GetLink('escorts', array('tours'), true) );


        if(!$is_loged){
            $navs['login_register'] = array( 'title' => __('login').' / '. __('register'), 'icon' => '',
                'link' => $this->view->GetLink('signin'));
        }

        $navs['advertise_with_us'] = array( 'title' => __('prices'), 'icon' => '', 'class' => '',
            'link' => $this->view->GetLink('advertise'));
         $navs['videos'] = array( 'title' => __('videos'), 'icon' => '',  'is_new' => true,
            'link' => $this->view->GetLink('videos'));

        $navs['classifies_ads'] = array( 'title' => __('c_ads_bottom_title'), 'icon' => '',
            'link' => $this->view->GetLink('classified-ads-index') );

		$navs['updates'] = array( 'title' => __('updates'), 'icon' => '',  'link' => '', 'class' => '');

		$navs['updates']['sub']['reviews'] = array( 'title' => __('reviews'), 'icon' => '',
			'link' => $this->view->GetLink('reviews') );

		$navs['updates']['sub']['comments'] = array( 'title' => __('comments'), 'icon' => '',
			'link' => $this->view->GetLink('comments'));

		$navs['contact'] = array( 'title' => __('contact'), 'icon' => '',
		 'link' => $this->view->GetLink('contact-us') );

        $navs['termsAndConditions'] = array( 'title' => __('terms_abbr'), 'icon' => '',
            'link' => $this->view->GetLink('terms') );

        if($is_loged){
        	$navs['dashboard'] = array( 'title' => __('dashboard'), 'icon' => '',
                'link' => $this->view->GetLink('private'));
			$navs['log_out'] = array( 'title' => __('logout'), 'icon' => '', 'link' => $this->view->GetLink('signout'));
		}

		if(is_array($hide)){
			foreach ($hide as $key) {
				unset($navs[$key]);
			}
		}else{
			unset($navs[$hide]);
		}
		

		return $navs;
	}

}