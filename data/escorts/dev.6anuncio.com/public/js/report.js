/**
 * Created by Zhora on 26.10.2017.
 */

var Report = {
    init: function () {
        this.initFakeForm();
        this.initProblemForm();
        this.initWriteEmail();
    },


    initProblemForm: function () {
        $('#problem-form').submit(function (event) {
            var self = $(this);
            event.preventDefault();

            var data = self.serializeArray();

            data.push({name: 'escort_id', value: $(document.activeElement).attr('data-id')});

            $.ajax({
                url: '/escorts/ajax-report-problem',
                type: 'POST',
                data: data,
                dataType: 'json',
                success: function (resp) {
                    if (resp.status == 'error') {

                        $('.has-danger').removeClass('has-danger');
                        $('.error-report').html(' ');

                        $.each(resp['msgs'], function (key, value) {
                            $('#' + key + '-error').html(value).parent().addClass('has-danger');
                        });
                        return false;
                    }

                    $('.has-danger').removeClass('has-danger');
                    $('.error-report').html(' ');

                    self.closest('.modal').modal('hide');
                    Notify.alert('success', 'Your report has been submitted!');
                }
            });
        });
    },

    initFakeForm: function () {
        $('#fake-form').submit(function (event) {
            var self = $(this);
            event.preventDefault();

            var data = self.serializeArray();

            data.push({name: 'escort_id', value: $(document.activeElement).attr('data-id')});

            $.ajax({
                url: '/escorts/ajax-susp-photo',
                type: 'POST',
                data: data,
                dataType: 'json',
                success: function (resp) {
                    if (resp.status == 'error') {

                        $('.has-danger').removeClass('has-danger');
                        $('.error-report').html(' ');

                        $.each(resp['msgs'], function (key, value) {
                            $('#' + key + '-error').html(value).parent().addClass('has-danger');
                        });
                        return false;
                    }

                    $('.has-danger').removeClass('has-danger');
                    $('.error-report').html(' ');

                    self.closest('.modal').modal('hide');
                    Notify.alert('success', 'Your information has been submitted!');
                }
            });
        });
    },

    initWriteEmail:function () {
        $('#write_email_to_escort_form').submit(function (event) {
            var self = $(this);
            event.preventDefault();

            var data = self.serializeArray();

            data.push({name: 'escort_id', value: $(document.activeElement).attr('data-id')});

            $.ajax({
                url: '/feedback',
                type: 'POST',
                data: data,
                dataType: 'json',
                success: function (resp) {
                    if (resp.status == 'error') {
                        if(resp.msgs.limit == 'riched'){
                            self.closest('.modal').modal('hide');
                            Notify.alert('danger', resp.msg);
                            return false;
                        }

                        $('.has-danger').removeClass('has-danger');
                        $('.error-report').html(' ');

                        $.each(resp['msgs'], function (key, value) {
                            $('#' + key + '-feedback-error').html(value).parent().addClass('has-danger');
                        });
                        return false;
                    }

                    $('.has-danger').removeClass('has-danger');
                    $('.error-report').html(' ');

                    self.closest('.modal').modal('hide');
                    Notify.alert('success', 'Your email was sent successfully!');
                }
            });
        });
    }

};

$(document).ready(function () {
    Report.init();
});
