function Chat(sessionId) {

    var socket,
        forceEnable,
        onlineUsers = [], //For easily sorting users
        onlineUsersObj = {}, //For easily getting user by id
        pmUsersObj = {},
        selfUser = {},
        browser = $.browser.name;

    var openDialogsUsers = {},
        skippedDialogs = {};


    var audio;

    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    var messages = {
        'offlineMessage': 'This user is offline. They will receive the message next login',
        'blackListedWordsMessage': 'You can`t use words "%words%"',
        'userBlockedYou': 'Message not sent. This user has blocked you.'
    };

    var originalTitle = document.title;
    var blinkTimer;

    this.connect = function () {

        if (typeof socket == 'undefined') {
            socket = io('/chat');
        } else {
            socket.connect();
        }
    };

    this.initListeners = function () {
        if(location.hash){
            $('body').addClass('keyboard-open');
            $('#mobile-chat-tab').click();
            $('.chat-write-msg').removeClass('d-none');
        }
        socket.on('auth-complete', function (data) {
            
            signedOffToolbar();
            if (data.auth) {

                if (data.availability) {
                    availableToolbar();
                    selfUser = data.userInfo;
                    prepareSettings();
                    getPrivateUsers();
                    visibilityEscortsList(true);
                } else {
                    notAvailableToolbar();
                    visibilityEscortsList(false);
                }
            } else {

            }

            $('chat-container').removeClass('hidden');
        });
        socket.on('connect', function () {
            if (typeof forceEnable == 'undefined') {
                forceEnable = true;
            }


            $('.chat-contacts,.mCSB_container ').empty();

            socket.emit('auth', {
                sid: sessionId,
                forceEnable: forceEnable,
                pm:true
            });
            forceEnable = false;
        });

        socket.on('redirect-to-unread-thread', function (msgs) {
            window.location.href = "/pm#"+msgs[0].userId;
        });

        socket.on('online-users-list', function (users) {
            onlineUsers = [];
            onlineUsersObj = users;
            for (var i in users) {
                //If user is blocked do not do anything
                if (!users[i] || (typeof selfUser.settings.blockedUsers != 'undefined' && selfUser.settings.blockedUsers.indexOf(users[i].userId.toString()) >= 0)) {
                    continue;
                }
                onlineUsers.push(users[i]);
            }
            sortOnlineUsers();
            fillOnlineUsers();
        });

        socket.on('new-user', function (userInfo) {
            //If user is blocked do not do anything
            if (typeof selfUser.settings != 'undefined' && typeof selfUser.settings.blockedUsers != 'undefined' && selfUser.settings.blockedUsers.indexOf(userInfo.userId) >= 0) {
                return;
            }

            if($('#user_'+userInfo.userId)){
                $('#user_'+userInfo.userId).remove();
            }
            insertUser(userInfo);
            if ($('#user_tab_' + userInfo.userId)) {
                $('#user_tab_' + userInfo.userId).removeClass('away').removeClass('offline').addClass('available');
            }
        });

        socket.on('offline-users', function (userIds) {
            userIds.forEach(function (userId) {
                removeUser(userId);
            });
        });

        socket.on('new-message', function (data) {
            // createChatTab(data.senderData);
            markTabUnread(data.senderData.userId);
            addMessage(data.senderData.userId, data.senderData, data.message);
            blinkPageTitle('New message from ' + data.senderData.nickName);
            playAudio('new-message');
            scrollToBottom($('.chat-discussion-content'));
        });

        socket.on('new-messages', function (messages) {
            setTimeout(function(){
                messages.forEach(function (message) {
                    if (message) {
                        // createChatTab(message.senderInfo);
                        markTabUnread(message.userId, message.count);
                    }
                });
            },500);
        });

        socket.on('message-history', function (data) {
            if(data.pm){
                data.senderInfo = pmUsersObj[data.userId];
            }
            fillHistory(data.senderInfo, data.messages);
        });

       socket.on('unread-messages-count', function (unread_messages) {
            if(unread_messages.length > 0){
                $.each(unread_messages, function(k, unread_message){
                    $('.notification-count').html(unread_messages.length);
                })
            }else{
                $('.notification-count').html('');
            }
        });

        socket.on('typing-start', function (data) {
            userTab = $('#user_tab_' + data.userId);
            if (userTab.length) {
                $('<div/>')
                    .append('<div id="typing_' + data.userId + '" class="typing"><span class="ion-chatbubble-working"></span></div>')
                    .appendTo(userTab.find('.chat-discussion-content'));
            }
        });
        socket.on('typing-end', function (data) {
            $('#user_tab_' + data.userId).find('.typing').remove();
        });

        socket.on('chat-off', function (data) {
            notAvailableToolbar();
        });

        socket.on('bl-word', function (data) {
            showMessage(data.dialogId, messages['blackListedWordsMessage'].replace('%words%', data.word));
        });

        socket.on('user-blocked-you', function (data) {
            showMessage(data.dialogId, messages['userBlockedYou']);
        });

        socket.on('blocked-users', function (users) {

            var blocked_users_area = $('#blocked-users-list');

            blocked_users_area.empty();

            if(!users.length){
                $('<div/>',{'class':'alert alert-info',role:'alert'})
                    .html("You don't have blocked users !")
                    .appendTo(blocked_users_area);
                return;
            }

            var blocked_users_table = $('<div/>').html(`<table class="table"><tbody></tbody></table>`);

            users.forEach(function (user) {
                $('<tr/>')
                    .append(`<td>${user.nickName}</td><td><button class="btn btn-primary btn-sm unblock-user" id="${user.userId}">Unblock</button></td>`)
                    .appendTo(blocked_users_table.find('tbody'));
            });

            blocked_users_table.appendTo(blocked_users_area);
        });

        socket.on('private-users-list',function (users) {
            if(!users) return;

            users.forEach(function (user) {

               if(!user || (typeof selfUser.settings.blockedUsers != 'undefined' && selfUser.settings.blockedUsers.indexOf(user.userId.toString()) >= 0) || onlineUsersObj[user.userId]) return;

               pmUsersObj[user.userId] = user;
               insertUserPmBuddyList(user);
            });

            $('.chat-contacts').mCustomScrollbar({theme:"dark", autoHideScrollbar: true, scrollButtons:{ enable: true }});

        });

        socket.on('add-private-user', function (user) {
            if(!user) return;

            pmUsersObj[user.userId] = user;

            insertUserPmBuddyList(user,true);

            $('#user_'+user.userId).trigger('click');

        });

        socket.on('conversation-deleted',function () {
            // Notify.alert('success','Your conversation has been deleted!');
        });

    }

    this.initEvents = function () {
        var self = this;

        $('#chat-switcher').on('change', function () {

                socket.emit('change-options', {
                    option: 'invisibility',
                    value: ($(this).is(':checked')) ? 0 : 1
                });
            $('#chat-switcher-m').trigger('click');

        });

        $('.chat-notification-block').on('click', function () {
             socket.emit('latest-unread-msg');
        });

        // $('#chat-switcher-m').on('change', function () {
        //    $('#chat-switcher').trigger('click');
        // });

        $('#show-unread-only').on('change',function () {
                var is_checked = $(this).is(':checked');
                $('.chat-contacts .users-tab').each(function () {
                    if(is_checked){
                        if(!$(this).find('.new_messages_count').length){
                            $(this).removeClass('d-flex').addClass('d-none');
                        }
                    }else{
                        $(this).removeClass('d-none').addClass('d-flex');
                    }
                });

        });

        $('#baddy-list-tab').off('click');
        $('#baddy-list-tab').on('click', function () {
            if ($('#baddy-list').hasClass('closed')) {
                showBaddyList();
            } else {
                hideBaddyList();
            }
        });

        $('#baddy-list .tab_header').on('click', function (e) {
            if (e.target.className.indexOf('tab_header') != -1) {
                hideBaddyList();
            }
        });

        $('.chat-contact-list .ion-close-round').on('click', function (e) {
            hideBaddyList();
        });


        $('.chat-contact-list').on('click', function (e) {
            if ($(this).hasClass('minimize')) {
                showBaddyList();
            }
        });


        //Search logic
        $('.baddy_list_search').on('keyup',function () {
            if (!Object.keys(pmUsersObj).length && !Object.keys(onlineUsersObj).length) return;

            var users = Object.assign(pmUsersObj,onlineUsersObj);

            var search = $(this).val().toLowerCase();

            $.each(users,function (i, user) {

                if (user.nickName.toLocaleLowerCase().indexOf(search) === -1 || (selfUser.settings.showOnlyEscorts == 1 && user.userType != 'escort')) {
                    $('#user_' + user.userId).removeClass('d-flex').hide();
                } else {
                    $('#user_' + user.userId).addClass('d-flex').show();
                }
            });
        });

        $('.block-user').on('click', function () {
            var id = $('.current-chat').attr('data-id');
            blockUser(id);
        });

        $('.delete-conversation').on('click',function () {
            var id = $('.current-chat').attr('data-id');
            deleteConversation(id);
        });

        $('.chat-col-body').click(function () {
            $('#message-type-area').focus();
        });

    }


    this.chatWith = function (userId) {
        if (userId == selfUser.userId) return false;

        if (typeof onlineUsersObj[userId] != 'undefined') {
            createChatTab(onlineUsersObj[userId], true);
        } else {
            socket.emit('chat-with', {
                userId: userId
            });
        }
    }

    this.getStatus = function () {
        return selfUser.status;
    }

    this.isOnline = function (userId) {
        if (typeof onlineUsersObj[userId] != 'undefined') {
            return true;
        } else {
            return false;
        }
    }

    this.enableChat = function () {
        var self = this;
        availableToolbar();
        forceEnable = true;
        self.connect();
        console.log('chat enabled');
    }

    this.disableChat = function () {
        var self = this;
        notAvailableToolbar();
        socket.emit('availability-changed', 0);
        socket.disconnect();
        hideSkippedTabs();
    }

    hideSkippedTabs = function () {
        $('.more-open-tabs').remove();
        skippedDialogs = {};
        openDialogsUsers = {};
        $('#chat-container').html('');
    }

    showBaddyList = function () {
        $('#baddy-list').removeClass('closed');
        $('#baddy-list').removeClass('hidden');
        $('#baddy-list-tab').addClass('expand');
        $('.baddy_list_search').focus();

        $('.chat-contact-list').removeClass('minimize');
        $('.chat-info-online-escorts').show();
    }

    hideBaddyList = function () {
        $('.chat-contact-list .chat-info-online-escorts').slideUp('50', function () {
            $('.chat-contact-list').addClass('minimize');
        });
    }

    availableToolbar = function () {
        $('.minimize').removeClass('minimize');
        $('#chat-wrapper').trigger('chat-on');
        $('.chat_base.signed_off').addClass('hidden');
        $('.chat_base.signed_on').removeClass('hidden');
        //$('.baddy_list').addClass('hidden');
        $('.baddy_list_tab').removeClass('expand').removeClass('hidden');
        $('.open_chat ').addClass('hidden');
    }

    notAvailableToolbar = function () {

        $('chat-wrapper').trigger('chat-off');
        $('.chat-discussion').remove();
        $('.chat-contact-list .chat-info-online-escorts').slideUp('50', function () {
            $('.chat-contact-list').addClass('minimize');
        });

        $('.baddy_list').addClass('hidden');
        $('.chat_base.signed_off').addClass('hidden');
        $('.baddy_list_tab').addClass('hidden');
        $('.chat_base.signed_on').removeClass('hidden');
        $('.open_chat ').removeClass('hidden');
        $('.mCSB_container').html(' ');

    }

    signedOffToolbar = function () {
        $('.chat_base.signed_off').removeClass('hidden');
        $('.chat_base.signed_on').addClass('hidden');
        $('.open_chat ').addClass('hidden');
    }


    updateOnlineUsersCount = function (count) {
        $('.online-users-count').html(count);
        // $('.notification-count').html(count);
    }

    fillOnlineUsers = function () {
        //$('.baddy_listing ul.listing')[0].empty();

        onlineUsers.forEach(function (user,i) {
            if(user.nickName.trim() === '(Escort)'){
                onlineUsers.splice(i, 1);
                return;
            }

            insertUserBuddyList(user);
        });
        updateOnlineUsersCount(onlineUsers.length);
    }

    insertUser = function (userInfo) {
        beforeId = false;
        onlineUsers.forEach(function (u, i) {
            if (userInfo.nickName.toLowerCase() <= u.nickName.toLowerCase() && !beforeId) {
                beforeId = u.userId;
                onlineUsers.splice(i, 0, userInfo); // inserting user in onlineUsers list
            }
        });

        if (!beforeId) {
            onlineUsers.push(userInfo);
        }

        onlineUsersObj[userInfo.userId] = userInfo;
        //draw user in buddy list before beforeId.
        //if beforeId is false insert at bottom.

        insertUserBuddyList(userInfo, beforeId);
        updateOnlineUsersCount(onlineUsers.length);
    }

    removeUser = function (userId) {
        onlineUsers.forEach(function (u, i) {
            if (u.userId == userId) onlineUsers.splice(i, 1); //removing from onlineUsers list
        });
        delete onlineUsersObj[userId];

        removeUserBuddyList(userId) //removing from buddy list
        updateOnlineUsersCount(onlineUsers.length);
    }

    insertUserBuddyList = function (userInfo, beforeId) {

        section = getUserRow(userInfo);

        section.appendTo($('#mCSB_1_container'));

        attachUserRowEvents(section);
    };

    insertUserPmBuddyList = function (userInfo,by_url) {

        section = getUserRow(userInfo,true);

        if(!by_url){
            section.appendTo($('.chat-contacts'));
        }else{
            section.appendTo($('#mCSB_2_container'));
        }

        attachUserRowEvents(section,true);
    };

    removeUserBuddyList = function (userId) {
        if (!$('#user_' + userId).length) return;

        $('#user_' + userId).remove();

    }

    getUserRow = function (user) {

        var section = $('<div/>', {id: 'user_' + user.userId, 'class': 'users-tab online-escort d-flex bl-' + user.userType})
            .append('<div class="online-escort-photo">' +
                '<img class="user_avatar" src="' + user.avatar + '" ' +
                '</div>');

        $('<div class="online-escort-info d-flex flex-column justify-content-center w-100 pl-2"></div>')
            .append('<div class="online-escort-name text-primary">' + user.nickName + '</div><div class="online-escort-location"><span class="user_status available"></span></div>')
            .appendTo(section);


        $('<div>', {'class' : 'user-type-label'}).append(user.userType).appendTo(section);



        //If in settings showonlyescorts = 1 set display = none
        if (user.userType != 'escort' && user.userType != 'agency' && typeof selfUser.settings.showOnlyEscorts != 'undefined' && selfUser.settings.showOnlyEscorts) {
            section.css('display', 'none');
        }

        return section;
    }

    sortOnlineUsers = function () {
        onlineUsers.sort(function (a, b) {
            if (a.nickName.toLowerCase() < b.nickName.toLowerCase()) {
                return -1;
            }
            if (a.nickName.toLowerCase() > b.nickName.toLowerCase()) {
                return 1;
            }

            return 0;
        });
    }

    //When user_row adds in baddy list, adding to this element eventss
    attachUserRowEvents = function (el,pm) {
 
        el.on('click', function () {
            $('.users-tab').removeClass('active-user-tab');
            el.addClass('active-user-tab');
            
            if($(window).width() < 576){
                $('body').addClass('keyboard-open');
            }

            $('#mobile-chat-tab').trigger('click');

            socket.emit('update-unread-messages-count');

            userId = el.attr('id').replace('user_', '');

            var message_container = $('.messaging');

            message_container.LoadingOverlay("show", {color : "rgba(255, 255, 255, 0.9)", zIndex: 1000});

            if(pm){
                loadPrivateMessage(pmUsersObj[userId]);
            }else{
                loadChat(onlineUsersObj[userId]);
            }

            message_container.LoadingOverlay("hide");
        });
    }

    loadChat = function(user){
       
        //Emiting about opened dialog
        socket.emit('dialog-opened', {
            userId: user.userId
        });

        showUserTab(user.userId);

        openDialogsUsers[user.userId] = user;
        
        $('.current-chat')
            .attr("id", 'user_tab_' +user.userId)
            .attr("data-id", user.userId);


        $('.current-chat .chat-col-head .chat-escorts-status, .current-chat .chat-write-msg').removeClass('d-none');
        $('.chat-block-actions').addClass('d-flex').removeClass('d-none');
        $('.current-chat .current-chat-buddy-name').html(user.nickName);
        $('.current-chat .chat-escorts-status').removeClass('away').addClass('online');
        attachUserTabEvents(user.userId);
    }

    loadPrivateMessage = function (user) {
        //Emiting about opened dialog
        socket.emit('dialog-opened-pm', {
            userId: user.userId
        });

        showUserTab(user.userId);

        $('.current-chat')
            .attr("id", 'user_tab_' +user.userId)
            .attr("data-id", user.userId);


        $('.current-chat .chat-col-head .chat-escorts-status, .current-chat .chat-write-msg').removeClass('d-none');
        $('.chat-block-actions').addClass('d-flex').removeClass('d-none');
        $('.current-chat .current-chat-buddy-name').html(user.nickName);
        $('.current-chat .chat-escorts-status').removeClass('online').addClass('away');
        attachPmUserTabEvents(user.userId,true);
    };

    //Attaching events to a new user tab
    attachUserTabEvents = function (id) {
       
        var activeChat = $('#user_tab_' + id);
        var textarea = activeChat.find('textarea');

        $('#user_tab_'+id + ' .send-message-button').on('click',function () {
            var e = $.Event( "keypress", { which: 13 } );
            $(this).prev().trigger(e);
        });

        var typing = false,
            timer;

        textarea.unbind('keypress');

        textarea.on('keypress', function (e) {
            textarea.next().addClass('msg-ready-to-sent');
            /*--> Detect typing start/end */
            if (!typing) {
                socket.emit('typing-start', {
                    userId: id
                });
                typing = true;
            }

            clearTimeout(timer);
            timer = setTimeout(function () {
                socket.emit('typing-end', {
                    userId: id
                });
                typing = false;
            }, 3 * 1000);
            /*<-- Detect typing start/end */

            if (e.which == 13 && !e.shiftKey) {
                e.preventDefault();

                message = $(this).val();

                socket.emit('message-sent', {
                    userId: id,
                    message: message
                });

                textarea.next().removeClass('msg-ready-to-sent');

                //After enter empty text area and set default height
                $(this).val('');
                // $(this).css('height', 18);

                message = {
                    body: message,
                    date: new Date().getTime(),
                    userId: selfUser.userId
                }

                addMessage(id, selfUser, message, true);

                //If client is sending message and participant is offline show notification
                if ($('#user_tab_' + id).hasClass('offline')) {
                    showMessage(id, messages.offlineMessage);
                }
            }

        });

        $('.back-to-conversation').on('click', function () {
            $('body').removeClass('keyboard-open');
            $('#mobile-chat-conversations').trigger('click');
            $('#navbarToggleExternalContent').collapse('hide');
        });
        /*<--User settings events*/

    }


    //Attaching events to a new user tab
    attachPmUserTabEvents = function (id,pm) {

        var activeChat = $('#user_tab_' + id);
        var textarea = activeChat.find('textarea');

        $('#user_tab_'+id + ' .send-message-button').on('click',function () {
            var e = $.Event( "keypress", { which: 13 } );
            $(this).prev().trigger(e);
        });

        var typing = false,
            timer;

        textarea.unbind('keypress');

        textarea.on('keypress', function (e) {

            if (e.which == 13 && !e.shiftKey) {
                e.preventDefault();

                message = $(this).val();

                socket.emit('message-sent', {
                    userId: id,
                    message: message,
                    pm:pm
                });

                //After enter empty text area and set default height
                $(this).val('');
                // $(this).css('height', 18);

                message = {
                    body: message,
                    date: new Date().getTime(),
                    userId: selfUser.userId
                }

                addPrivateMessage(id, selfUser, message, true);
            }

        });

        $('.back-to-conversation').on('click', function () {
            $('body').removeClass('keyboard-open');
            $('#mobile-chat-conversations').trigger('click');
        });
        /*<--User settings events*/

    }

    showMessage = function (id, message) {
        activeChatContent = $('active_chat_' + id).getElement('.active_chat_content');

        if (!activeChatContent.getElement('.message_box')) {
            messageBox = new Element('div', {
                'class': 'message_box'
            });
            messageBox.inject(activeChatContent);
        }
        messageBox.set('html', message);

        isAlreadyOpened = false;
        var messageFx = new Fx.Elements(messageBox, {
            onComplete: function () {
                if (!isAlreadyOpened) {
                    isAlreadyOpened = true;
                    setTimeout(function () {
                        messageFx.start({
                            0: {
                                top: [0, (-1) * $('.message_box')[0].getSize().y]
                            }
                        });
                    }, 5 * 1000);
                }
            }
        }).start({
            0: {
                top: [-1 * $('.message_box')[0].getSize().y, 0]
            }
        });

    }

    showUserTab = function (id) {

        socket.emit('dialog-activated', {
            userId: id
        });

        userTab = $('#user_' + id);
        activeChat = $('#user_tab_' + id);

        if (userTab.length) {
            if (userTab.find('.new_messages_count').length) {
                userTab.find('.new_messages_count').remove();
                
                socket.emit('conversation-read', {
                    userId: id
                });

                socket.emit('update-unread-messages-count');
            }
        }
    }

    //Closing user tab
    hideUserTab = function (id) {
        socket.emit('dialog-deactivated', {
            userId: id
        });
    }

    getMessageRow = function (userInfo, received) {

        var userMessage = $('<div/>', {'class': `d-flex user_message ${userInfo.userId}`});

        var userAvatar = $('<div/>', {'class': 'user_avatar'});

        if (received) {
            userMessage.addClass('msg-to-you');
            $('<img/>', {src: userInfo.avatar, width: 25, height: 25}).appendTo(userAvatar);
            userAvatar.appendTo(userMessage);
            $('<div/>', {'class': 'messages msg-to-you-content px-2 mx-1'}).appendTo(userMessage);
        } else {
            userMessage.addClass('msg-from-you text-right my-2 px-2 justify-content-end');
            $('<div/>', {'class': 'messages'}).appendTo(userMessage);
        }

        return userMessage
    };

    addMessage = function (id, senderInfo, message, scroll_down) {

        //first of all clearing message
        message.body = clearMessage(message.body);

        if (message.body.length == 0) return;

        userTab = $('#user_tab_' + id);
        activeChat = $('#user_tab_' + id + ' .chat-col-body');
       
        //If tab opened and is not self message emit message-read
        
        
       if (userTab.lenght > 0 && senderInfo.userId != selfUser.userId) {
            socket.emit('conversation-read', {
                userId: senderInfo.userId
            });
             socket.emit('update-unread-messages-count');
        }

        if (typeof isNewMessage == 'undefined') {
            isNewMessage = false;
        }
        if (typeof newMessagesCount == 'undefined') {
            newMessagesCount = 1;
        }

        lastUserMessage = activeChat.find('.user_message').last();


        //if messaging is empty or last message is not his, create new user_message raw
        //else add to existing one
        if (lastUserMessage && (lastUserMessage.hasClass(message.userId) && message.date - parseInt(lastUserMessage.attr('id')) <= 60 * 1000)) {

            $('<p/>')
                .html(message.body)
                .appendTo(lastUserMessage.find('.messages'));

        } else {
            if (message.userId == selfUser.userId) {
                userMessage = getMessageRow(selfUser, false);
            } else {
                userMessage = getMessageRow(senderInfo, true);
            }


            userMessage.attr('id', message.date);

            //Print message Content
            $('<p/>', {"class": "m-0"}).html(message.body).appendTo(userMessage.find('.messages'));

            //Print date
            // $('<p/>', {'class': 'm-0 date'}).html(getTimeForMessage(new Date(message.date))).appendTo(userMessage.find('.messages'));

            userMessage.appendTo(activeChat.find('.messaging'));
        }

        $('#typing_' + senderInfo.userId).remove();

        if (scroll_down) {
            scrollToBottom(activeChat.find('.chat-discussion-content'))
        }
    }

    addPrivateMessage = function (id, senderInfo, message, scroll_down) {

        //first of all clearing message
        message.body = clearMessage(message.body);

        if (message.body.length == 0) return;

        userTab = $('#user_tab_' + id);
        activeChat = $('#user_tab_' + id + ' .chat-col-body');

        //If tab opened and is not self message emit message-read
        if (userTab.lenght && senderInfo.userId != selfUser.userId) {
            socket.emit('conversation-read', {
                userId: senderInfo.userId
            });
        }

        if (typeof isNewMessage == 'undefined') {
            isNewMessage = false;
        }
        if (typeof newMessagesCount == 'undefined') {
            newMessagesCount = 1;
        }

        lastUserMessage = activeChat.find('.user_message').last();


        //if messaging is empty or last message is not his, create new user_message raw
        //else add to existing one
        if (lastUserMessage && (lastUserMessage.hasClass(message.userId) && message.date - parseInt(lastUserMessage.attr('id')) <= 60 * 1000)) {

            $('<p/>')
                .html(message.body)
                .appendTo(lastUserMessage.find('.messages'));

        } else {
            if (message.userId == selfUser.userId) {
                userMessage = getMessageRow(selfUser, false);
            } else {
                userMessage = getMessageRow(senderInfo, true);
            }

            userMessage.attr('id', message.date);

            //Print message Content
            $('<p/>', {"class": "m-0"}).html(message.body).appendTo(userMessage.find('.messages'));

            userMessage.appendTo(activeChat.find('.messaging'));
        }

        $('#typing_' + senderInfo.userId).remove();

        if (scroll_down) {
            scrollToBottom(activeChat.find('.chat-discussion-content'))
        }
    }

    markTabUnread = function (id, unreadMessagesCount) {
        userTab = $('#user_' + id);

        if (typeof unreadMessagesCount == 'undefined') unreadMessagesCount = 1;

        if(!userTab.hasClass('active-user-tab')){
            if (!userTab.find('.new_messages_count').length) {
                $('<div/>', {'class': 'new_messages_count px-1'})
                    .html(unreadMessagesCount)
                    .appendTo(userTab);
            } else {
                count = parseInt(userTab.find('.new_messages_count').html());
                count += unreadMessagesCount;
                userTab.find('.new_messages_count').html(count);
            }
            socket.emit('update-unread-messages-count');
        }
    };

    fillHistory = function (senderInfo, messages) {
        userTab = $('#user_tab_' + senderInfo.userId);
        activeChat = $('#user_tab_' + senderInfo.userId + ' .chat-col-body');
      
        activeChat.find('.messaging').empty();

        if (userTab.length) {
            messages.forEach(function (message) {
                addMessage(senderInfo.userId, senderInfo, message)
            });
           
            scrollToBottom($('#user_tab_' + senderInfo.userId +' .chat-discussion-content'));
        }
    }

    getTimeForMessage = function (date) {
        curDate = new Date();

        hours = date.getHours();
        minutes = ('0' + date.getMinutes()).slice(-2); //two number format

        d = hours + ':' + minutes;

        if (curDate.getDate() != date.getDate()) {
            d += ' ' + date.getDate() + 'th ' + monthNames[date.getMonth()];
        }

        if (curDate.getYear() != date.getYear()) {
            d += ' ' + date.getYear();
        }

        return d;
    }

    clearMessage = function (message) {
        //htmlentities
        message = htmlEntities(message);

        //Replacing one more whitespaces
        message = message.trim();
        message = message.replace(/\s+/g, ' ');

        //replacing urls with html links
        var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
        message = message.replace(exp, "<a href='$1' target='_blank'>$1</a>");


        return message;
    }

    htmlEntities = function (str) {
        return String(str).replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/&/g, '&amp;');
    }


    function RegExpEscape(text) {
        return text.replace(/[\\=!^$*+?.:|(){}[\]]/g, "\\$&");
    }


    function scrollToBottom(selector) {
        selector.animate({scrollTop: selector.prop("scrollHeight") + 500}, 0);
    }

    blinkPageTitle = function (message) {
        if (!originalTitle.length) return;
        if (typeof blinkTimer != 'undefined') clearInterval(blinkTimer);
        blinkTimer = setInterval(function () {
            if (document.title == message) {
                document.title = originalTitle;
            } else {
                document.title = message;
            }
        }, 1000);
        window.focus(function () {
            if (typeof blinkTimer != 'undefined') clearInterval(blinkTimer);
            document.title = originalTitle;
            this.removeEvents('click');
        });
    }

    prepareSettings = function () {
        if (typeof selfUser.settings == 'undefined') selfUser.settings = {};

        if (typeof selfUser.settings.showOnlyEscorts != 'undefined' && selfUser.settings.showOnlyEscorts) {
            $('#show-only-escorts').parent().addClass('checked');
        }
        if (typeof selfUser.settings.keepListOpen != 'undefined' && selfUser.settings.keepListOpen) {
            $('#keep-list-open').closest('li').addClass('checked');
            showBaddyList();
        }

        if (typeof selfUser.settings.invisibility != 'undefined' && selfUser.settings.invisibility) {
            setTimeout(function () {
                $('#chat-switcher,#chat-switcher-m').prop('checked',false);
            },100);
        }

        $( "#blocked-users-modal" ).on('shown.bs.modal', function () {
            socket.emit('blocked-users');
        });

        $('body').on('click','.unblock-user', function () {
            var userId = $(this).attr('id');
            if (userId) {
                $(this).closest('tr').remove();

                if(!$('#blocked-users-list tr').length){
                    $('<div/>',{'class':'alert alert-info',role:'alert'})
                        .html("You don't have blocked users !")
                        .appendTo($('#blocked-users-list'));
                }

                selfUser.settings.blockedUsers.splice(selfUser.settings.blockedUsers.indexOf(userId), 1);
                socket.emit('unblock-user', {
                    userId: userId
                });
            }
        });
    }


    blockUser = function (id) {

        if(!id) return;

        socket.emit('block-user', {
            userId: id
        });

        if(selfUser.settings.blockedUsers === undefined){
            selfUser.settings.blockedUsers = [];
        }

        selfUser.settings.blockedUsers.push(id);

        removeUser(id);

        $('.current-chat-buddy-name').html('Please select escort to start chat');
        $('.messaging').empty();
        $('.chat-write-msg').addClass('d-none');
        $('.chat-block-actions').removeClass('d-flex').addClass('d-none');
        $('.back-to-conversation').trigger('click');

    }

    deleteConversation = function (id) {

        if(!id) return;

        if(confirm('Are you sure ?')){
            socket.emit('delete-conversation',{userId:id});
            $('.messaging').empty();
        }

    }

    initAudio = function () {
        if ("Audio" in window && browser.indexOf('safari') == -1) {
            audio = new Audio();
            if (!!(audio.canPlayType && audio.canPlayType('audio/ogg; codecs="vorbis"').replace(/no/, '')))
                audio.ext = 'ogg';
            else if (!!(audio.canPlayType && audio.canPlayType('audio/mpeg;').replace(/no/, '')))
                audio.ext = 'mp3';
            else if (!!(audio.canPlayType && audio.canPlayType('audio/mp4; codecs="mp4audio.40.2"').replace(/no/, '')))
                audio.ext = 'm4a';
            else
                audio.ext = 'mp3';

            return;
        }

    }

    playAudio = function (name) {
        if (audio && audio.play) {
            audio.src = "/sounds/" + name + '.' + audio.ext;
            audio.play()
        }
    }

    getPrivateUsers = function () {
        var pm_user_id = null;

        socket.emit('get-private-users',{
            user_id : selfUser.userId
        },function () {
            if (location.hash) {
                pm_user_id = location.hash.substring(1);

                if (onlineUsersObj[pm_user_id] || pmUsersObj[pm_user_id]) {
                    $('#user_' + pm_user_id).trigger('click');
                } else {
                    socket.emit('get-private-user', {
                        userId: pm_user_id
                    });
                }
            }
        });
    }

    function visibilityEscortsList(active) {
        if (active) {
            $('.chat-info-online-escorts').css('display', 'block');
            $('#chat-switcher').prop('checked', true);
        } else {
            $('#chat-switcher').prop('checked', false);
        }

    }

    this.connect();
    this.initListeners();
    this.initEvents();
    initAudio();
}