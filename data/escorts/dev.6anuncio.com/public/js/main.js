$(document).ready(function () {
    $('div.flags > a').on('click', function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var lng = $(this).data('lang');
        var d = new Date();
        d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = "ln=" + lng + ";" + expires + ";path=/;domain=."+location.host+";";

        if($(this).parent().hasClass( "private_area_flags" )){
            $("body").LoadingOverlay("show", {color : "rgba(239, 243, 249, 0.80)", zIndex: 1000   });
            window.location.reload();
        }else{
            href = window.location.href;
            if(lng == 'pt'){
                href = href.replace("escorts", "acompanhantes");
                href = href.replace("escort", "acompanhante");
            }else{
                href = href.replace("acompanhantes", "escorts");
                href = href.replace("acompanhante", "escort");
            }
            window.location.href = href;
        }
    });

    $('li.flags > a').on('click', function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var lng = $(this).data('lang');
        href = window.location.href;
        if(lng == 'pt'){
            href = href.replace("escorts", "acompanhantes");
            href = href.replace("escort", "acompanhante");
        }else{
            href = href.replace("acompanhantes", "escorts");
            href = href.replace("acompanhante", "escort");
        }
        var d = new Date();
        d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = "ln=" + lng + ";" + expires + ";path=/;domain=."+location.host+";";
        window.location.href = href;
    });
    // Dummy hijacking back button of chrome
// ------------------------------------
    (function (window, location) {
        if (!document.referrer.includes(location.host)) {

            var _url = location.toString().replace(location.hash, "");

            history.replaceState(null, document.title, _url + "#!/history");
            history.pushState(null, document.title, _url);

            window.addEventListener("popstate", function () {
                if (location.hash === "#!/history") {
                    history.replaceState(null, document.title, location.pathname);
                    setTimeout(function () {
                        location.replace("/");
                    }, 0);
                }
            }, false);
        }
    }(window, location));
// ------------------------------------
});
