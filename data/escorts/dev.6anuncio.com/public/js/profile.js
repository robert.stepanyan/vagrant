 'use strict';
     $('._report').on('click', function () {
         var modal = $(this).attr('data-target');
         $(modal).modal();
     });

     $('.write_me').on('click',function () {
         $('#write_email_to_escort').modal();
     });

     $('.add_comment').on('click',function () {
         var user = checkUserType();
         var referrer = $('input[name="referrer"]').val();

         if(!user){
             window.location.href = "/account/signin?referrer=escort/" + referrer;
             return false;
         }else if(user !== 'member'){
             Notify.alert('danger',"Please <a href='/account/signin'>sign in</a> as a member!");
             return false;
         }

         $('#add_comment_modal').modal();
     });

     $('#add_comment_form').submit(function (event) {
         event.preventDefault();
         var _self = $(this);
         var user = checkUserType();

         if(!user){
             window.location.href = "/account/signin";
             return false;
         }else if(user !== 'member'){
             Notify.alert('danger',"Please <a href='/account/signin'>sign in</a> as a member!");
             return false;
         }

         var data = $(this).serializeArray();

         $.ajax({
             url:'/comments/add-comment',
             type:'POST',
             data:data,
             dataType:'json',
             success:function (resp) {

                 if(resp.success){
                     Notify.alert('success', headerVars.dictionary.comment_success);
                     $('#add_comment_modal').modal('hide');
                     return false;
                 }

                 _self.find('.error').html(' ').parent().removeClass('has-danger');

                 $.each(resp.errors , function (key, value) {
                    $('#'+ key).html(value).parent().addClass('has-danger');
                 });

             }
         });
     });

    $(document).ready(function(){


          if($('#video-jw').length){
              jwplayer.key="2Jl4iHdNg8jOvK4EtIxZ6XHOcYIYFfWQOP2O9g==";  
             var config = $('#video_config');

             var image = config.attr('src');
             var url = config.attr('data-url');
             var video = config.attr('data-video');
             var height = config.attr('data-height');
             var width = config.attr('data-width');

             video = url + video + '_' + height + 'p.mp4';

             var isMobile = ($(window).width() < 574);

             // if (isMobile < 574) {
                 // $('.video-modal').modal();
             // } else {
             //     $('.video-modal').modal().find('.modal-dialog')
             // }

             jwplayer('video-jw').setup({
                 // height: (isMobile) ? 330 : height,
                 aspectratio: "16:9",
                 width: '100%',
                 image: image,
                 autostart: false,
                 skin: {
                     name: "bekle"
                 },
                 logo: {
                     file: "/img/video/logo.png"
                 },
                 file: video
             });
          }
    })
     $('#anchors a').click(function (e) {

         e.preventDefault();
         $('#anchors li a.active').removeClass('active');
         $('#anchors .report_container.active').removeClass('active');
         var tab = $(this).attr('href');
         var el = tab.replace("#", "");
         if ($(this).hasClass('report')) {
             $('.report_container').addClass('active');
         }
         $(this).addClass('active');

         if (!$("#"+el).length) return false;

         $('html, body').animate({
             scrollTop: ($("#"+el).offset()).top - 75
         }, 1000);
     });

     function checkUserType() {
         var result = false;

         $.ajax({
            url:'/check-user-type',
            type:'POST',
            async:false,
            success:function (resp) {
                result = resp;
            }
         });

         return result;
     }
     $('.not_authorized').on('click', function(event) {
       event.preventDefault();
        $('#LoginPopup').modal('toggle');
     });
     $('.add_vote').on('click', function () {
         var self = $(this);
         var url = $(this).attr('data-href');

         $.ajax({
             url: url,
             type: "POST",
             dataType: 'json',
             success: function (resp) {
              
               if(resp.redirect){
                   window.location.href = resp.redirect;
                   return false;
               }
               switch(resp.status) {
                 case 'dinaid':
                   Notify.alert('warning', resp.desc);
                   break;
                 case 'error':
                   $('#LoginPopup').modal('toggle');
                   break;
                 case 'voted':
                     Notify.alert('warning', resp.desc);
                   break;
                case 'success':
                     Notify.alert('success', resp.desc);
                   break;
                   
               }
               

               self.addClass('disabled');
             //  Notify.alert('success', resp.desc);
             }
         });
     });


  $(".toggle-accordion").on("click", function() {
    var accordionId = $(this).attr("accordion-id"),
    numPanelOpen = $(accordionId + ' .collapse.show').length;

    $(this).toggleClass("active");
    if (numPanelOpen == 0) {
      openAllPanels(accordionId);
    } else {
      closeAllPanels(accordionId);
    }
  })

  openAllPanels = function(aId) {
     $(aId + ' .panel-collapse:not(".show")').collapse('show');
   }
  closeAllPanels = function(aId) {
    $(aId + ' .panel-collapse.show').collapse('hide');
  }

    $('#accordion .panel-title').on('show.bs.collapse', function () {
      var windowWidth = $(window).width()+17;
      if(windowWidth <= 992){
        closeAllPanels('#accordion');
      }
     })

     $('#accordion .panel-collapse').on('shown.bs.collapse', function () {
        var windowWidth = $(window).width()+17;
        if(windowWidth <= 992){
          $('html, body').animate({
           scrollTop: $("#"+this.id).offset().top-120
          }, 500);
        }
     })

 
    function brekpointProfile(){
       var windowWidth = $(window).width()+17;
       if(windowWidth <= 992){
        closeAllPanels('#accordion');
        
         $('#accordion .panel-title').off('click' );
      }else{
         openAllPanels('#accordion');
         $('#accordion .panel-title').on('click', function(){
           event.stopPropagation();
         });
      }
    }

  brekpointProfile();
  $( window ).resize(function() {
   brekpointProfile();
 });

  initAgencyEscortsSlider();
  function initAgencyEscortsSlider() {
     $('.agency-escorts-slider').slick({
    
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1
   
   });
  }


 
  /* profile slider */
  var loadedPhotosCount = 0;
  var photosCount = $('.pImg').length;
 
  $('.pImg').on('load', function() {
    loadedPhotosCount++;
    if (loadedPhotosCount == photosCount) {
      initProfileSlider();
      
    }
  }).each(function() {
    if (this.complete) {
      $(this).trigger('load');
    }
  });



function sliderFullscreenOpen(arrow, element) {
  arrow.addClass('fullscreen-out');
    $('.pSlider').addClass('fullscreen-slider');   
    $('html,body').css({'overflow': 'hidden'});
    $('body').css({'touch-action':'auto'})
}

function sliderFullscreenClose(arrow) {
  arrow.removeClass('fullscreen-out');

  $('.pSlider').removeClass('fullscreen-slider');
  $('html,body').removeAttr('style');
}

function sliderNavigate(slider) {
  var index = slider.slick('slickCurrentSlide');
  slider.slick('slickGoTo', index, true);
}


function initProfileSlider() {

  var slider = $('.pSlider').slick({
    speed: 300,
    slidesToShow: 2,
    infinite: true,
    variableWidth: true,
    slidesToScroll: 1,
    responsive: [
    {
      breakpoint: 576,
      settings: {
        slidesToShow: 1,
        infinite: true,
        variableWidth: false,
        slidesToScroll: 1
      }
    },
  ]
 });

  $('.slick-slide').click(function() {
     
   
    var index = $(this).data("slick-index");
    var current = slider.slick('slickCurrentSlide');
    var last = slider.slideCount - 1;

    if( current === 0 && index === last ) {
       slider.slick('slickGoTo', -1);
    } else if ( current === last && index === 0 ) {
       slider.slick('slickGoTo', last + 1);
    } else {
       slider.slick('slickGoTo', index);
    }

});

  $('.slick-slide').on('mousedown', function(edown) {
    if (edown.button == 0 && 
      !$('.fullscreenToggle').hasClass('fullscreen-out')) {

      var handle;

      $('.slick-slide').on('mouseup', handle = function(eup) {
        $('.slick-slide').off('mouseup', handle);

        if (Math.abs(edown.screenX - eup.screenX) < 10 && 
          Math.abs(edown.screenY - eup.screenY) < 10) {
            sliderFullscreenOpen($('.fullscreenToggle'), $(".slick-current"));
            sliderNavigate(slider);
        }
      });
    }
  });

  $(window).on('keydown', function(e) {
    if (e.keyCode == 37) {
      slider.slick('slickPrev');
    }

    if (e.keyCode == 39) {
      slider.slick('slickNext');
    }

    if (e.keyCode == 27 && $('.fullscreenToggle').hasClass('fullscreen-out')) {
      sliderFullscreenClose($('.fullscreenToggle'));
      sliderNavigate(slider);
    }
  });

  $(window).on('scroll' , function () {
     if(window.pageYOffset > 300){
         $('.back_to_top').removeClass('d-none').delay(1000);
     }else{
         $('.back_to_top').addClass('d-none').delay(1000);
     }
  });

    $('#container-slider').bind('DOMMouseScroll', function(e){
      console.log('test');
       if(e.originalEvent.detail > 0) {
          slider.slick('slickNext');
       }else {
          slider.slick('slickPrev');
       }
     
       return false;
   });

   //IE, Opera, Safari
   $('#container-slider').bind('mousewheel', function(e){
       if(e.originalEvent.wheelDelta < 0) {
          slider.slick('slickNext');
       }else {
          slider.slick('slickPrev');
       }
      
       return false;
   });


  $('.fullscreenToggle').click(function() {
   if (!$('.fullscreenToggle').hasClass('fullscreen-out')) {
      sliderFullscreenOpen($('.fullscreenToggle'), $(".slick-current"));
      // sliderNavigate(slider);
      if($(this).data('slidePos')){
         slider.slick('slickGoTo', $(this).data('slidePos')-1);
      }
   } else {
      sliderFullscreenClose($('.fullscreenToggle'), $(".slick-current"));
       sliderNavigate(slider);
   }
  });
}
$(document).ready(function(){
  if(location.href.match('#headingReviews') !== null){
        setTimeout(function(){$('#collapsecomments').collapse('show')}, 1000);
    }
})
