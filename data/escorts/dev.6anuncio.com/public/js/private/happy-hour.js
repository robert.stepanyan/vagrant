var HappyHour = {

    init: function () {
        this.initForm();
        this.initRemoveBtn();
    },
    initForm: function () {
        if ($('#is_agency').val()) {
            this.initAgency();
        } else {
            this.initEscort();
        }
    },

    initAgency: function () {
        var self = this;

        self.initExistingHappyHours();

        $('select[name=sel_escort]').change(function (e) {

            var cont = $('.hh_form .body');
            var selected_cont = $('.selected_escorts');


            var value = $(this).val();
            var showname = $(this).find("option:selected").text();

            if ($('.escort_form').length == 2) {
                alert('You can choose a maximum of 2 girls');
                return;
            }

            var found = false;

            if (selected_cont.length) {
                selected_cont.find('div').each(function () {
                    if ($(this).find('a').attr('rel') === value) {
                        found = true;
                    }
                });
            }

            if (value != "0" && !found) {
                $.ajax({
                    url: '/private/happy-hour-form?escort_id=' + value,
                    type: 'post',
                    beforeSend:function () {
                        $('#set-happy-hour').LoadingOverlay('show',true);
                    },
                    success: function (resp) {
                        var escort_form = $('<div class="card p-4 escort_form"><div class="card-body"><div class="escort_form"></div></div></div>');

                        escort_form.attr('id', 'esc_' + value);


                        escort_form.appendTo(cont);

                        escort_form.html(resp);

                        if (escort_form.find('.hh_ov').length) {

                        }


                        var sel_div = $('<div><a href="#" class="btn-remove" rel="' + value + '"> <i class="fa fa-ban text-danger"></i></a><span> ' + showname + '</span></div>');

                        sel_div.appendTo(selected_cont);


                        self.initRemoveBtn($('a[rel="' + value + '"]'));

                        if (escort_form.find('#hh_save_btn').length) {
                            var save_btn = escort_form.find('#hh_save_btn');
                            var form = escort_form.find('form');
                            self.initSave(save_btn, form, escort_form);
                        }

                        $('#set-happy-hour').LoadingOverlay('hide',true);

                    }
                });

            }
        });
    },

    initEscort: function () {
        var cont = $('.hh_form .body');

        var self = this;

        var escort_id = $('#escort').val();

        $.ajax({
            url: '/private/happy-hour-form?escort_id=' + escort_id,
            type: 'post',
            success: function (resp) {

                var escort_form = $('<div class="card p-4 escort_form"><div class="card-body"><div class="escort_form"></div></div></div>');

                escort_form.attr('id', 'esc_' + escort_id);

                escort_form.appendTo(cont);

                escort_form.html(resp);

                var save_btn = escort_form.find('#hh_save_btn');

                var form = escort_form.find('form');

                if (save_btn) {
                    self.initSave(save_btn, form, escort_form);
                }

                if ($('.hh_ov').length) {
                    $('.hh_ov').LoadingOverlay('show', {image: false});
                }
            }
        });
    },

    initRemoveBtn: function (btn) {
        btn.click(function (e) {
            e.preventDefault();

            if ($(this).attr('class') != 'btn-remove') return;

            if (confirm('Are you sure you want to delete this escort "Happy Hour" ?')) {
                $.ajax({
                    url: '/private/happy-hour-reset?escort_id=' + btn.attr('rel'),
                    type: 'post',
                    beforeSend:function () {
                        $('#set-happy-hour').LoadingOverlay('show',true);
                    },
                    success: function () {
                        var btn_remove_rel = $('a[rel="' + btn.attr('rel'));
                        if (btn_remove_rel.length) {
                            $('#esc_' + btn.attr('rel')).remove();
                            btn_remove_rel.closest('div').remove();
                        }
                        $('#set-happy-hour').LoadingOverlay('hide',true);
                    }
                });
            }
        });
    },

    initSave: function (btn, form, cont) {
        btn.click(function (e) {
            e.preventDefault();


            var data = form.serialize();

            $.ajax({
                url: '/private/happy-hour',
                type: 'post',
                beforeSend:function () {
                    form.LoadingOverlay('show',true);
                },
                data: data,
                success: function (resp) {
                    var data = JSON.parse(resp);

                    $('.err').html(' ');

                    if (data.status === 'error') {
                        for (d in data.msgs) {
                            form.find('.' + d).html(data.msgs[d]);
                        }
                    }

                    form.LoadingOverlay('hide',true);

                    if(data.status === 'success'){
                        Notify.alert('success', headerVars.notifyAlertSuccess);
                    }
                }
            });
        });
    },

    initExistingHappyHours: function () {
        var self = this;

        $('.selected_escorts').find('div').each(function () {
            var value = $(this).find('a').attr('rel');
            var showname = $(this).find('span').attr('html');
            var cont = $('.hh_form .body');

            self.initRemoveBtn($(this).find('a'));

            $.ajax({
                url: '/private/happy-hour-form?escort_id=' + value,
                type: 'post',
                beforeSend:function () {
                    $('#set-happy-hour').LoadingOverlay('show',true);
                },
                success: function (resp) {
                    var escort_form = $('<div class="card p-4 escort_form"><div class="card-body"><div class="escort_form"></div></div></div>');

                    escort_form.attr('id', 'esc_' + value);

                    escort_form.appendTo(cont);

                    escort_form.html(resp);

                    var save_btn = escort_form.find('#hh_save_btn');

                    var form = escort_form.find('form');

                    self.initSave(save_btn, form, escort_form);

                    $('#set-happy-hour').LoadingOverlay('hide',true);

                    if($('.hh_ov').length){
                        $('.hh_ov').LoadingOverlay('show',{image:false});
                    }

                }
            });
        });
    }
};


$(document).ready(function () {
    HappyHour.init();
});