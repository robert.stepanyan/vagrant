var Verification = {

    init:function () {
        this.initUploader();
        this.initEscortsProfiles();
    },

    addUploader: function () {
        var uploaders_count = $('#uploaders input[type=file]').length;

        if(uploaders_count > 9) return false;

        $('<div/>').append('<label class="custom-file my-1 w-75">Image no. ' + (uploaders_count + 1) + ' <input id="uploader-' + (uploaders_count + 1)  + '" type="file" name="pic[' + uploaders_count + ']" class="custom-file-input"><span class="custom-file-control  form-control-file"></span> </label>').appendTo($('#uploaders'));
    },

    initUploader:function () {
        $('.custom-file-control').attr('data-before', headerVars.dictionary.browse);
        $('.custom-file-control').attr('data-after', headerVars.dictionary.choose_files);

        $('#verification-form').submit(function (event) {

           event.preventDefault();

           var data = new FormData(document.querySelector('#verification-form'));

           $("#ui-view").LoadingOverlay("show", true);
           $.ajax({
               url:'/private/verify/idcard',
               type:'POST',
               processData: false,
               contentType: false,
               data:data,
               success:function (data) {
                   $('#ui-view').html(data).LoadingOverlay("hide", true);
                   Verification.init();
               }
           });
        });


        $('#overview-form').submit(function () {
            event.preventDefault();

            var data = $('#overview-form').serializeArray();
            $("#ui-view").LoadingOverlay("show", true);

            $.ajax({
               url:'/private/verify/idcard-overview',
               type:'POST',
               data: data,
               success:function (data) {
                   $('#ui-view').html(data).LoadingOverlay("hide", true);
                   Verification.init();
               }
            });

        });


        $('body').on('change','.custom-file-input',function(){
            $(this).next('.form-control-file').addClass("selected").html($(this).val());
        });
    },
    initEscortsProfiles: function () {
        $(window).resize(function () {
            if( $(window).width() < 567 ) {
                $('#verification_escorts a[data-ajax-url]').each(function () {
                    $(this).attr('data-ajax-url-disabled',$(this).attr('data-ajax-url'));
                    $(this).removeAttr('data-ajax-url');
                });
            }else{
                $('#verification_escorts a[data-ajax-url-disabled]').each(function () {
                    $(this).attr('data-ajax-url',$(this).attr('data-ajax-url-disabled'));
                    $(this).removeAttr('data-ajax-url-disabled');
                });
            }
        });

        $('.escort-thumb').click(function () {
           $('.escort-thumb').removeClass('selected_verify_escort');
           $(this).addClass('selected_verify_escort');
           $('#verify_escort_mobile').removeClass('btn-default').addClass('btn-success');
        });

        $('#verify_escort_mobile').click(function () {

            if($(this).attr('data-ajax-url')) return false;

            var selected_escort = $('.selected_verify_escort');

            if(! selected_escort.length) return false;

            var url = $('.selected_verify_escort').find('a[data-ajax-url-disabled]').attr('data-ajax-url-disabled');
            $(this).attr('data-ajax-url',url);
            $(this).trigger('click');

        });

        $(window).trigger('resize');

    }

};


$(document).ready(function () {
   Verification.init();
});
