var Settings = {
    ln : 'pt',
    init: function () {
        var ln = location.pathname.split('/')[1];
        if (ln === 'en'){
            this.ln = ln
        }
        this.initForm();
        this.initCountries();
        this.initNewsletters();
        this.initProfileStatus();
    },

    initProfileStatus: function(){
        $('#profile-status').on('click', function(){

            $("#profile-status").prop("checked", true);
            if( $(this).attr('value') == 0){

                $.confirm({
                    title: headerVars.dictionary.attention_2,
                    content: headerVars.dictionary.profile_disabled_confirm,
                    buttons: {
                        confirm:{
                            text: headerVars.dictionary.pv2_option_yes,
                            action: function(){
                                $(".profile-status").LoadingOverlay("show", {color : "rgba(239, 243, 249, 0.80)", zIndex: 1000 });
                                if($('#profile-status').attr('value') == 0) {
                                    var action = 'disable';
                                } else {
                                    var action = 'enable';
                                }
                                $.ajax({
                                    type: "GET",
                                    url: "private/profile-status?act=" + action,
                                    success: function(response){
                                        if(response){
                                            if(action == 'enable'){
                                                $('#profile-status').val("0");
                                                $('.status-profile').text("active");
                                                $('.active-block').find('.alert-warning').removeClass('alert-warning').addClass('alert-success');
                                            }else{
                                                $('#profile-status').val("1");
                                                $('.status-profile').text("owner disabled");
                                                $('.active-block').find('.alert-success').removeClass('alert-success').addClass('alert-warning');
                                            }
                                        }
                                    },

                                    complete: function(e){
                                        $(".profile-status").LoadingOverlay("hide", true);
                                        $("#profile-status").prop("checked", false);
                                    }
                                })
                            }
                        },
                        cancel:{
                            text: headerVars.dictionary.pv2_option_no,

                            action: function () {
                                $("#profile-status").prop("checked", true);
                            }
                        }


                    }
                });
            }else{
                $(".profile-status").LoadingOverlay("show", {color : "rgba(239, 243, 249, 0.80)", zIndex: 1000 });
                if($('#profile-status').attr('value') == 0) {
                    var action = 'disable';
                } else {
                    var action = 'enable';
                }
                $.ajax({
                    type: "GET",
                    url: "private/profile-status?act=" + action,
                    success: function(response){
                        if(response){
                            if(action == 'enable'){
                                $('#profile-status').val("0");
                                $('.status-profile').text("active");
                                $('.active-block').find('.alert-warning').removeClass('alert-warning').addClass('alert-success');
                            }else{
                                $('#profile-status').val("1");
                                $('.status-profile').text("owner disabled");
                                $('.active-block').find('.alert-success').removeClass('alert-success').addClass('alert-warning');
                            }
                        }
                    },

                    complete: function(e){
                        $(".profile-status").LoadingOverlay("hide", true);
                    }
                })
            }



        })
    },

    initCountries: function () {
        $('select#country,select#city').select2({
            theme:'bootstrap'
        });

        $("#country").change(function () {
            var country_id = $(this).val();

            $.ajax({
                url: '/geography/ajax-get-cities',
                type: 'POST',
                data: {country_id:country_id},
                dataType:'json',
                beforeSend:function () {
                    $('#profile-settings').LoadingOverlay("show", true);
                },
                success: function (data) {

                    var city_area = $('#city');

                    city_area.find('option').remove();

                    $.each(data['data'],function (key,city) {
                        $('<option/>',{value:city.id,html:city.title}).appendTo(city_area);
                    });

                    $('#profile-settings').LoadingOverlay("hide", true);
                }
            });
        });
    },


    initForm:function () {
        $('#settings-form').submit(function (event) {
            event.preventDefault();

            var activeElement = $(document.activeElement);
            var action = activeElement.attr('id');
            var actionValue = activeElement.val();

            $('#action').attr({name:action,value:actionValue});

            var data = $(this).serializeArray();
            var url = '';
            if(Settings.ln !== 'pt'){
                url += '/en'
            }
            url += '/private/settings';
            $.ajax({
                url: url,
                type: 'POST',
                data: data,
                beforeSend:function () {
                    activeElement.closest('.card-body').LoadingOverlay("show", true);
                },
                success: function (data) {
                    $('#ui-view').html(data);
                    activeElement.closest('.card-body').LoadingOverlay("hide", true);

                    Settings.init();
                }
            });
        });

    },

    initNewsletters: function () {

        $('#newslatter_save').click(function () {
            $(this).closest('form').submit();
        })

    }

};


$(document).ready(function () {
    Settings.init();
});