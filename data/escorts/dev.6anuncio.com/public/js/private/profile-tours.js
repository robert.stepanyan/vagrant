var Tours = {
 init:function() { 
  var self = this;
  this.initTours();
  this.initHistorySwitcher();
  this.addEditTour();
  

  $('#tour-escort').on('change',  function(){
   self.initTours();
   if($(this).val() != -1){
    $('#addTour').removeClass('btn-secondary').addClass('btn-success');
 }else{
    $('#addTour').removeClass('btn-success').addClass('btn-secondary');
 }

})
},

initTours: function(){
  var escortID = $('#tour-escort').find(":selected").val();

  this.getTours(escortID, 0);
  if($('#tours-history-switcher').val() == 0){
    this.getTours(escortID, 1);
  }
},

initHistorySwitcher: function(){
   var self = this;
   $('#tours-history-switcher').on('click',  function(event) {
      var SwitcherValue = $(this);
      $.ajax({
         url: 'private/tours/toggle-tour-history',
         type: 'POST',
         data: {
            o_t_v: SwitcherValue.val(),
         },
         success: function(){
            setUpUrl('tours/index/');
         }
      })
   });
},

addEditTour: function(tourID){
  var self = this;
  $('#addTour').on('click',  function(event) {
      var escortID = $('#tour-escort').find(":selected").val();
      if(escortID == -1){
        $('#tour-escort').popover('show');
      }else{
        $('#tour-escort').popover('hide');
        $('.tours-block').LoadingOverlay("show", {color : "rgba(239, 243, 249, 0.80)", zIndex: 1000   });
        self.getPopup(escortID);
      }
   });
},

saveTour: function(tour){
  var self = this;
  $.ajax({
   'url':'/private/tours/edit-tour',
   type: 'POST',
   data: tour,
   success:function ( response ) { 
    try{
      response = JSON.parse(response);
      if(typeof response =='object' && response['status'] == 'success'){
        $('.add-tour-form-modal').modal('hide');
        self.getTours();
        Notify.alert('success',"Tour has been successfully saved!");
     }else{
        if(typeof response =='object' && response['status'] == 'error' ){
           $.each( response['msgs'], function( key, value ) {
             $( "[name='"+key+"']" ).addClass('is-invalid');
             $('#validation-'+key).html(value);
          });
        }  
     } 
  }catch (e) {
    Notify.alert('error',"Unexpected error when adding tour");
   }
}
})

},

getTours: function(escortid, history, page){ 
  $('#escorts-tours').popover('hide');
  var self = this;
  history = typeof history !== 'undefined' ? history : 0;
  page = typeof page !== 'undefined' ? page : 1;

  if(typeof escortid == "undefined"){
    escortid = $('#tour-escort').find(":selected").val()
 }

 $('#tour-escort').popover('hide');
 var tours_container_block = $(".tours-block");
 var tours_history_container_block = $(".tours-history-block");

 var tours_container = $("#ajax-target");
 var tours_history_container = $("#ajax-target-old");
 
 $.ajax({
  'url':'/private/tours/ajax-tours',
  'type':'POST',
  'data':{
    escort_id: escortid,
    old_tour: history,
    page: page
 },
 beforeSend:function () {
   if(history){
      tours_history_container_block.LoadingOverlay("show", {color : "rgba(239, 243, 249, 0.80)"   });
   }else{
     tours_container_block.LoadingOverlay("show", {color : "rgba(239, 243, 249, 0.80)"   });
   }
},
success:function (data) {

 if(history){
    tours_history_container.html(data);
    self.initPagination('.old-tours-pagination');
    //var table = $('#escorts-tours-old').DataTable({"bInfo" : false, "bLengthChange": false });
 }else{

    tours_container.html(data);
    self.initPagination('.tours-pagination');
    //var table = $('#escorts-tours').DataTable({"bInfo" : false, "bLengthChange": false });
 }
},
complete: function (){
  if(history){
    tours_history_container_block.LoadingOverlay("hide", true);
 }else{
    self.initActions();
    setTimeout(function(){tours_container_block.LoadingOverlay("hide", true)});
    ;
 }
}

});
},

initPagination: function( pagination_wrapper ){
  var self = this;
  
    $( pagination_wrapper + ' .page-item .page-link').on('click',  function(e){
      e.preventDefault();
        
        var page = parseInt($(this).attr('data-page'));
        var isTourOld = parseInt($(pagination_wrapper).attr('data-is-tour-old'));
        var escortID = $('#tour-escort').find(":selected").val();
        self.getTours(escortID, parseInt(isTourOld), page);
    })
},

initActions: function(){
  var self = this
 // var table = $('#escorts-tours').DataTable();
  var selected_row = 0;
  $('.tours-list').on( 'click', '.tour', function () {
      if(this.id == 0)return false;
      $('#escorts-tours').popover('hide');
      if ( $(this).hasClass('selected') ) {
        $(this).removeClass('selected');
        selected_row = 0;
        $('.delete-tour').addClass('disabled');
        $('.delete-tour.mobile').addClass('d-none');
       
        $('.edit-tour').addClass('disabled');
        $('.edit-tour.mobile').addClass('d-none');
      }
      else {
         $('.tours-list .tour').removeClass('selected');
         $(this).addClass('selected');
         selected_row = $(this);
        $('.delete-tour').removeClass('disabled');
        $('.delete-tour.mobile').removeClass('d-none');
        $('.edit-tour').removeClass('disabled');
        $('.edit-tour.mobile').removeClass('d-none');
      }
   });

    // DELETE
    $('.delete-tour').click( function () {
      if(selected_row != 0 ){
         var tourdeleteConfirm = confirm("Are you sure you want to delete " + selected_row.data('showname') +"'s tour");
         if (tourdeleteConfirm == true) {
            $.ajax({
              url: '/private/tours/remove-tours',
              data: {
                 tours: selected_row.attr('id'),
              },
              success:function (data) {
                Notify.alert('success',"Tour has been successfully deleted!");
                self.getTours();
             },
             error:function (data) {
              alert('Unexpected error');
           },
        })
         }
      }else{
       $('#escorts-tours').popover('show');
    }
 });

    // EDIT
    $('.edit-tour').click( function () {
       if(selected_row != 0 ){
         escortID  = 0;
         $('.tours-block').LoadingOverlay("show", {color : "rgba(239, 243, 249, 0.80)", zIndex: 1000   });
         self.getPopup(escortID, selected_row.attr('id'));
         $('.tours-block').LoadingOverlay("hide");
         selected_row.trigger('click');
        
      }else{
        $('#escorts-tours').popover('show');
      }
   });
 },
 getPopup: function(escortID, tourid){
   var self = this;
   tourid = typeof tourid !== 'undefined' ? tourid : 0;

   var get = '';
   if(escortID != 0){
      get += 'escort_id='+escortID;
   }else if(tourid != 0){
      get += 'id='+tourid;
   }else{
      return false;
   }

   $.ajax({
      'url':'/private/tours/edit-tour?'+get,
      type: 'GET',
      success:function (data) {
       $('#inner-notifications').append(data);
       $('.add-tour-form-modal').modal('show');

       $('.input-daterange ').datepicker({
          language: "en-GB",
          startDate: new Date(),
          todayHighlight: true,
          autoclose: true,
          format: 'dd M yyyy',
       });

       $('.tour-times-block .time').timepicker({
        'showDuration': true,
        'timeFormat': 'H:i'
     });

       $('.tour-times-block ').datepair();

       $('#tourtime').on('change', function(event) {
        if(this.checked) {
          $('.tour-times-block input').attr('readonly', false);
       }else{
          $('.tour-times-block input').val('');
          $('.tour-times-block input').attr('readonly', true);
       }
    });

       $('.submit-btn-add-tour').on('click',  function(event) {
        event.preventDefault();
        var form = $('#add-tour-form').serializeArray();

        self.saveTour(form);
     });

       $('.add-tour-form-modal').on('hidden.bs.modal', function(){
         $('#inner-notifications').html('');
      })
    },
    complete: function(){  
      $('.tours-block').LoadingOverlay('hide');
   }
})

 }
};

$(document).ready(function(){
 Tours.init(); 
}); 





