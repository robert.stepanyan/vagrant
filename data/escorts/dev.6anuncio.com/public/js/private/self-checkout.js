'use strict';

var Cubix = Cubix || {};

Cubix.SelfCheckout = (function(global) {

  var Bootstrap = function() {};
  Bootstrap.prototype = (function() {
    function run (userType, data) {
      var stateController = new StateController(userType, data);
      new ViewRenderer(stateController);
    }

    return {
      run: run
    };
  })();

  var StateController = function(userType, data) {
    return this.construct(userType, data);
  };

  StateController.prototype = {
    construct: function(userType, data) {
      //UK TIME
      moment.tz.setDefault("America/Sao_Paulo");

      this.userType = userType;
      this.pageView = 1;

      if (userType === 'escort') {
        this.userData = data.escort;
        this.premium_packages = data.escort_packages;
        this.status_owner_disabled = 8;
      }

      if (userType === 'agency') {
        this.searchTerm = '';
        this.escortsPage = 1;
        this.escortsPerPage = 10;
        this.userData = data.agency;
        this.escorts = data.escorts.reduce(function(a, escort) {
          a[escort.id] = escort;
          return a;
        }, {});
        this.visibleEscorts = data.escorts;
        this.agencyDiscounts = data.agency_discounts.reduce(function(a, obj) {
          var o = {};

          o.range = [obj.value_from, obj.value_to];
          o.discount = obj.discount;

          return a = a.concat(o);
        }, []);
      }

      this.available_packages = data.available_packages;
      this.cart = {};
    },
  };

  var _draw_notification = function(reason){
    var $main_container = $('#ui-view');
    
    switch(reason) {
        case 'owner-disabled':
             $main_container.html('<div class="alert alert-danger" role="alert">Your profile is owner disabled. In order to continue, you must activate your ad</div><button id="profile-status-activate" class="btn m-auto btn-outline-success btn-lg active fixed-button">Activate now</button>')
                $('#profile-status-activate').on('click', function(){
                $(".main").LoadingOverlay("show", {color : "rgba(239, 243, 249, 0.80)", zIndex: 1000   });
                
                $.ajax({
                 type: "GET",
                 url: "private/profile-status?act=enable",
                 success: function(response){
                    if(response){
                     $main_container.html('<div class="alert alert-success" role="alert">Your account has been successfully activated.</div><button onClick="window.location.reload()"  class="btn m-auto btn-outline-success btn-lg active fixed-button">Upgrade now</button>')
                    }else{
                     $main_container.html('<div class="alert alert-danger" role="alert">Unexpected error please contact support</div>')
                    }
                 },
                 complete: function(e){
                   $(".main").LoadingOverlay("hide", true);
                 }
              })
            })
            break;
        case 'escort-gender':
          $main_container.html('<div class="alert alert-danger" role="alert">You can not buy a package. Please contact support</div><button data-ajax-url="/private/support#index" class="btn m-auto btn-outline-success btn-lg active fixed-button">Support</button>')
        break;
        case 'complete_7_steps':
          $main_container.html('<div class="alert alert-danger" role="alert">In order to upgrade your ad, you need to complete your profile</div><button data-ajax-url="/private/profile#index" class="btn m-auto btn-outline-success btn-lg active fixed-button">Complete profile</button>')
        break;
        case 'escorts_no_found':
          $main_container.html('<div class="alert alert-danger" role="alert">In order to upgrade your ads, you need to add profiles</div><button data-ajax-url="/private/profile#index" class="btn m-auto btn-outline-success btn-lg active fixed-button">Add Profile</button>')
        break;
         
        default:
            console.log('nothing');
    }
  };

  var Cart = (function () {
    var instance;

    function createInstance() {
      var obj = {};
      obj.premium_packages = {};
      obj.dateCreated = Date.now();

      return obj;
    }

    return {
      getInstance: function () {
        if (!instance) {
          instance = createInstance();
        }
        return instance;
      }
    };
  })();

   var CartItemBuilder = function() {
     return this.construct();
   };

   CartItemBuilder.prototype = {
     construct: function() {
      return this;
     },
     setAvailablePackages: function(premium_packages) {
       this.available_packages = premium_packages;

       return this;
     },

     setEscort: function(id) {
       this.escortId = id;

       return this;
     },

     setType: function(type) {

      this.type = type;

      return this;
     },

     setGender: function(gender) {
       this.gender = gender;
       return this;
     },

     setDuration: function(duration) {
      this.duration = duration;

      return this;
     },

     setActivationDate: function(date) {
      this.activationDate = date;

      return this;
     },


    setBaseCityId: function(cityId) {
    this.baseCityId = cityId;

    return this;
    },

     setShowname: function(showname) {
      this.showname = showname;
     },

     getAmount: function() {

      var _name = this.type + ' ' + this.duration;
      var _gender = this.gender;

      

      var premium_packages = this.available_packages.filter(function(p) {

        if (p.name == _name && p.available_for == _gender) {
          return p;
        }
      });
          
       return premium_packages[0].price;
     },

     getForCart: function() {

      var _name = this.type + ' ' + this.duration;
      var _gender = this.gender;
      var premium_package = this.available_packages.filter(function (p) {
         if (p.name == _name && p.available_for == _gender) {
           return p;
         }
       });
      
       return {
         type: this.type,
         duration: this.duration,
         premium_cities: [this.baseCityId],
         escortId: this.escortId,
         showname: this.showname,
         packageId: premium_package[0].id,
         activation_date: this.activationDate,
         price: premium_package[0].price
       };
     }
   };

  var ViewRenderer = function(state) {
    return this.construct(state);
  };

  ViewRenderer.prototype = {
    construct: function(state) {
      var vr = this;
      vr.state = state;
      vr._draw();
    },

    _draw: function() {
      var vr = this;

      var $main_container = $('#ui-view');
      $main_container.html( '')

      if (vr.state.userType === 'escort') {
        var escortView = vr._drawEscortView();
        $main_container.append(escortView);
        vr._bindEvents();
      } else if (vr.state.userType === 'agency') {
        var agencyView = vr._drawAgencyView();
       
        $main_container.append(agencyView);
        vr._bindAgencyEvents();
      }
    },

    _drawEscortView: function() {
      var vr = this;
      var $container = $('<div/>', {
        class: "self-checkout-container row no-gutters"
      });

      /** PAGE 1 */
      var $page1 = $('<div/>', {
        'data-component': 'page',
        'data-id': 1,
        'data-name': 'escortslist',
        'class': 'page card col-12',
        'css': {
          'display': this.state.pageView !== 1 ? 'none' : 'block'
        }
      });

      var $headerBar = $('<div/>', {
        'data-component': 'bar',
        'data-name': 'headerbar',
        'class': 'bar card-header'
      });

      var $title = $('<h2/>', {
        'text': headerVars.dictionary.upgrade_now,
        'data-component': 'heading',
        'data-name': 'title',
        'class': 'title'
      });

      $headerBar.append($title);

      var $escortsList = $('<div/>', {
        'data-component': 'list',
        'data-name': 'escortslist',
        'class': 'escorts-list-wrapper'
      });

      var $premium_packageItem = this._drawpremium_packageItem();
      $escortsList.append($premium_packageItem);

      $page1.append($headerBar, $escortsList);

      /** PAGE 2 */
      var $page2 = $('<div/>', {
        'data-component': 'page',
        'data-id': 2,
        'data-name': 'cart',
        'class': 'page col-12 p-static',
        'css': {
          'display': this.state.pageView !== 2 ? 'none' : 'block'
        }
      });

      var $titleBar = $('<div/>', {
        'data-component': 'bar',
        'data-name': 'titlebar',
        'class': 'bar--inverted card-header'
      });

      var $title = $('<div/>', {
        'text': headerVars.dictionary.review_premium_package,
        'data-component': 'heading',
        'data-name': 'title',
        'class': 'title',
      });
      $titleBar.append($title);

      var $cartTable = $('<table/>', {
        'data-component': 'table',
        'data-name': 'carttable',
        'class': 'table cart-review'
      });

      var $tableHead = $('<tr/>', {
        'data-component': 'thead',
        'data-name': 'tablehead',
        'class': 'head'
      });

      $.each([  headerVars.dictionary.start_date, headerVars.dictionary.__duration, headerVars.dictionary.__price, ''],function(idx,el) {
        $tableHead.append($('<td/>', {
          'data-component': 'td',
          'data-name': 'colname',
          'class': 'td columnHead'+ el,
          'text': el
        }))
      });

      $cartTable.append($tableHead);
      if (this.state.cart.premium_packages && Object.keys(this.state.cart.premium_packages).length) {
        $.each(this.state.cart.premium_packages, function(idx, p) {

          var $tableRow = $('<tr/>', {
            'data-component': 'tr',
            'data-name': 'tablerow',
            'class': 'body'
          });
        
          $tableRow.append($('<td/>', {
            'data-component': 'td',
            'data-name': 'cartdata',
            'class': 'td',
            'text': p.activation_date ? p.activation_date : headerVars.dictionary.asap
          }));

          $tableRow.append($('<td/>', {
            'data-component': 'td',
            'data-name': 'cartdata',
            'class': 'td',
            'text': p.duration + ' ' + headerVars.dictionary.__days
          }));

          $tableRow.append($('<td/>', {
            'data-component': 'td',
            'data-name': 'cartdata',
            'class': 'td',
            'text': 'USD ' + p.price
          }));

          $tableRow.append($('<td/>', {
            'data-component': 'td',
            'data-name': 'toolbox',
            'class': 'action-buttons td d-flex justify-content-end actions',
            'html': '<i class="button--edit fa fa-edit fa-2x text-primary " data-id="' + idx + '">' +
              '</i><i class="button--delete fa fa-remove fa-2x text-danger" data-id="' + idx + '"></i>'
          }));

          $cartTable.append($tableRow);
        });
      }

      var $cartFooter = $('<div/>', { 
        'data-component': 'footer',
        'data-name': 'cartfooter',
        'class': 'footer--cart d-flex justify-content-between',
      });

      if (this.state.cart.premium_packages && Object.keys(this.state.cart.premium_packages).length) {
        var totalAmount = Object.values(this.state.cart.premium_packages).reduce(function(a, el) {
          a += +el.price;

          return a;
        }, 0);

      }
      var $totalAmount = $('<div/>', {
        class: 'align-self-center',
        'html': '<div class="total-amount-data">Total: <strong>USD ' + (totalAmount ? (totalAmount + '.00') : 0) + '</strong></div>'
      });

      var $checkoutBtn = $('<button/>', {
        'data-component': 'button',
        'data-name': 'buy-button',
        'class': 'modal-button--checkout fixed-button py-3',
        'text': headerVars.dictionary.proceed_to_checkout
      }).on('click', function() {
        var cartData = Object.values(vr.state.cart.premium_packages);
        //convert date as needed for shopping card

        if(cartData[0].activation_date){
          var dateMoment = moment.utc(cartData[0].activation_date).format('YYYY-MM-DD');
          cartData[0].activation_date = dateMoment;
        }

        $.ajax({
          url: 'private/billing/checkout',
          method: 'POST',
          dataType: 'json',
          data: {
            cart: cartData,
          },
          success: function(resp) {
            if (resp.status == 'success') {
              window.location.href = resp.url;
            }
          }
        });
      });

      $cartFooter.append($totalAmount, $checkoutBtn);

      $page2.append($titleBar, $cartTable, $cartFooter);
        var $footer_text = $('<div/>', {
        class: 'footer-notification w-100',
        'html': '<div class="row no-gutters"><div class="col-12 col-md-5 p-3 d-none d-md-block">' + headerVars.dictionary.free_ad_not_enough + '</div><div class="col-2 justify-content-center d-none d-md-flex"><div class="vertical-line" style=""></div></div><div class="col-12 col-md-5 p-3"><img src="https://www.6anuncio.com/images/bankpic.png" height="35px" width="110px">' + headerVars.dictionary.self_checkout_payment_details + '<p class="d-md-none font-weight-bold pt-3">For checkout you will be directed to a secure payment page managed by our online payments partner, mmgbill. All information will be transmitted securely for the payment.</p></div></div>'
      });
      $container.append($page1, $page2, $footer_text);
    


     
      return $container;
    },

    _drawpremium_packageItem: function() {
      var $tr = $('<div/>', {
        class: 'tr'
      });

      if (!this.state.premium_packages.length) {
        var $tdInfo = $('<div/>', {
          text: headerVars.dictionary.free_basic_ad,
          class: 'desc--row my-auto d-flex'
        });
      } else {
        var infopremium_packages = this.state.premium_packages.reduce(function(a, p) {
          
          if (p.expiration_date == null) {
            return ((a ? a + ' | ' : '') + '<span>Your <b>'+ p.package_name +'</b> will be activated shortly.</span>');
          }else{
            return ((a ? a + ' | ' : '') + '<span>You have ' + p.package_name + '  until ' + moment.unix(p.expiration_date).format("D MMM YYYY")  + '. </span>');
          }
          
        }, '');

        var $tdInfo = $('<div/>', {
          html: infopremium_packages,
          class: 'desc--row my-auto d-flex'
        });
      }

      var $tdBtnWrapper1 = $('<div/>', {
        'data-component': 'td',
        'data-name': 'button-wrapper',
        'class': 'wrapper--button'
      });

      var $newpremium_packageBtn = $('<button/>', {
        'data-component': 'button',
        'data-name': 'purchase-button',
        'class': 'button--purchase fixed-button' ,
        'text': headerVars.dictionary.buy_premium_package
      });

      $tdBtnWrapper1.append($newpremium_packageBtn);

      if (!this._canBuy(this.state.premium_packages)) {
        $tdBtnWrapper1.addClass('disabled');

        var $tooltip = $('<div/>', {
          'class': 'tooltip',
          'text': 'You already have a package. You cannot buy more.'
        });
        $tdBtnWrapper1.append($tooltip);

        $newpremium_packageBtn.addClass('disabled');
      }

      $tr.append($tdInfo, $tdBtnWrapper1);

      return $tr;
    },

    _drawAgencyView: function() {
      var vr = this;

      var $container = $('<div/>', {
        'class': "self-checkout-container row no-gutters"
      });

      /** PAGE 1 */
      var $page1 = $('<div/>', {
        'data-component': 'page',
        'data-id': 1,
        'data-name': 'escortslist',
        'class': 'page card col-12',
        'css': {
          'display': this.state.pageView !== 1 ? 'none' : 'block'
        }
      });

      var $headerBar = $('<div/>', {
        'data-component': 'bar',
        'data-name': 'headerbar',
        'class': 'bar card-header'
      });

      var $title = $('<div/>', {
        'text': 'Your Escorts',
        'data-component': 'heading',
        'data-name': 'title',
        'class': 'title'
      });

      var $searchBar = $('<div/>', {
        'data-component': 'input',
        'data-name': 'searchbar',
        'class': 'search-bar',
        'html': '<input type="text" placeholder="Search" value="' + vr.state.searchTerm + '" />',
      });

      var $sortBar = $('<div/>', {
        'data-component': 'select',
        'data-name': 'sortbar',
        'class': 'sort-bar d-flex',
        'html': '<select></select>'
      });

      var $cartIcon = $('<i>', {
        'class': 'fa fa-cart-plus fa-2x go-to-cart px-3'
      }).on('click', function(e) {
        e.preventDefault();

        vr.state.pageView = 2;
        new ViewRenderer(vr.state);
      });

      if (vr.state.cart.premium_packages && Object.keys(vr.state.cart.premium_packages).length) {
        $cartIcon.append($('<span>', { text: Object.keys(vr.state.cart.premium_packages).length, class: 'cart-packages-count' }));
      }

      var $sortOptions = ['Alphabetically', 'By escort ID', 'Latest modified', 'Newest first'].map(function(type) {
        return $('<option/>', {
          'text': type,
          'data-id': type.toLowerCase().replace(/ /g, '-')
        });
      });
      $sortBar.find('select').append($sortOptions);
      $sortBar.append($cartIcon);

      $headerBar.append($title, $searchBar, $sortBar);

      var $escortsList = $('<div/>', {
        'data-component': 'list',
        'data-name': 'escortslist',
        'class': 'escorts-list-wrapper agency'
      });
      var term = vr.state.searchTerm;
      if (term) {
        vr.state.visibleEscorts = Object.values(vr.state.escorts).filter(function(escort) {
          if ( ~escort.showname.toLowerCase().search(term.toLowerCase()) || ~escort.id.toString().search(term)) return escort;
        });
      } else {
        vr.state.visibleEscorts = Object.values(vr.state.escorts);
      }

      $.each(vr.state.visibleEscorts, function(idx,escort) {
        var $escortItem = vr._drawEscortItem(escort);
        $escortsList.append($escortItem);
      });

      $page1.append($headerBar, $escortsList);

      /** PAGE 2 */
      var $page2 = $('<div/>', {
        'data-component': 'page',
        'data-id': 2,
        'data-name': 'cart',
        'class': 'page col-12',
        'css': {
          'display': this.state.pageView !== 2 ? 'none' : 'block'
        }
      });

      var $titleBar = $('<div/>', {
        'data-component': 'bar',
        'data-name': 'titlebar',
        'class': 'bar--inverted card-header'
      });

      var $title = $('<div/>', {
        'text': headerVars.dictionary.review_premium_package,
        'data-component': 'heading',
        'data-name': 'title',
        'class': 'title',
      });

      var $backToEscortList = $('<a/>', {
        'html': '<i class="fa fa-arrow-left d-md-none"></i><span class="d-none d-md-block">&larr; Back To Escorts</span>',
        'href': '#',
        'data-component': 'heading',
        'class': 'go-to-page px-3',
      }).on('click', function(e) {
        e.preventDefault();
        vr.state.pageView = 1;
        new ViewRenderer(vr.state);
      });
      $titleBar.append($title, $backToEscortList);

      var $cartTable = $('<table/>', {
        'data-component': 'table',
        'data-name': 'carttable',
        'class': 'table agency-cart-review'
      });

      var $tableHead = $('<tr/>', {
        'data-component': 'thead',
        'data-name': 'tablehead',
        'class': 'head'
      });

      $.each([ 'Escort Name', 'Activation Date', 'Duration', 'Price', '' ], function(idx, el) {
        $tableHead.append($('<td/>', {
          'data-component': 'td',
          'data-name': 'colname',
          'class': 'td columnHead'+el,
          'text': el
        }))
      });

      $cartTable.append($tableHead);

      if (this.state.cart.premium_packages && Object.keys(this.state.cart.premium_packages).length) {

         $.each(this.state.cart.premium_packages, function(idx, p) {
          var $tableRow = $('<tr/>', {
            'data-id': p.escortId,
            'id': p.escortId,
            'data-component': 'tr',
            'data-name': 'tablerow',
            'class': ''
          }).on('click', function(event) {
              $.each($('.agency-cart-review tr td.action-buttons'), function(index, el) {
                $(el).addClass('d-none');
              });

              if($(this).hasClass('selected-escort')){
                 $(this).removeClass('selected-escort');
              }else{
                 $(this).addClass('selected-escort');
                 $(this).find('.action-buttons').removeClass('d-none');
              }

              $.each($('.agency-cart-review tr').not('#'+$(this).data('id')), function(index, el) {
                $(el).removeClass('selected-escort');
              });
 
          });;

          $tableRow.append($('<td/>', {
            'data-component': 'td',
            'data-name': 'cartdata',
            'class': 'td',
            'text': p.showname
          }));

          $tableRow.append($('<td/>', {
            'data-component': 'td',
            'data-name': 'cartdata',
            'class': 'td',
            'text': p.activation_date ? p.activation_date : headerVars.dictionary.asap
          }));

          $tableRow.append($('<td/>', {
            'data-component': 'td',
            'data-name': 'cartdata',
            'class': 'td',
            'text': p.duration + ' ' + headerVars.dictionary.__days
          }));

          $tableRow.append($('<td/>', {
            'data-component': 'td',
            'data-name': 'cartdata',
            'class': 'td',
            'text': 'USD ' + p.price
          }));

          $tableRow.append($('<td/>', {
            'data-component': 'td',
            'data-name': 'toolbox ',
            'data-actions-escort-id': p.escortId,
            'class': 'td d-none justify-content-end actions action-buttons td  justify-content-end actions',
            'html': '<i class="button--edit fa fa-edit fa-2x text-primary" data-id="' + idx + '">' +
              '</i><i class="button--delete fa fa-remove fa-2x text-danger" data-id="' + idx + '"></i>'
          }));

        

          $cartTable.append($tableRow);
        });
      }else{
        var $tableRow = $('<tr/>', {
            'data-component': 'tr',
            'data-name': 'tablerow',
            'class': '',
            'html': '<p class="pl-3">Cart is empty</p>'
          })
          $cartTable.append($tableRow);
      }

      var $cartFooter = $('<div/>', { 
        'data-component': 'footer',
        'data-name': 'cartfooter',
        'class': 'footer--cart d-flex justify-content-between flex-column flex-md-row pt-5',
      });
      var totalAmount = 0;
      var totalAmount = 0;
      var discountedAmount = 0;
      var discountPct = 0;
      var discounts = this.state.agencyDiscounts;
      var packageCnt = this.state.cart.premium_packages ? Object.keys(this.state.cart.premium_packages).length : 0;
     
      if (packageCnt) {
        if (discounts) {
          $.each(discounts, function(index, el) {
              if (el.range[1]) {
              if (packageCnt >= el.range[0] && packageCnt < el.range[1]) {
                discountPct = el.discount;
                return false;
              }
            } else {
              if (packageCnt >= el.range[0]) {
                discountPct = el.discount;
                return false;
              }
            }
          });
          
        }
      } 

      if (this.state.cart.premium_packages && Object.keys(this.state.cart.premium_packages).length) {
        discountedAmount = Object.keys(this.state.cart.premium_packages).reduce(function(a, key) {
          var el = vr.state.cart.premium_packages[key];
          totalAmount += +el.price;
          a += +el.price - (el.price * discountPct / 100);
          return a;
        }, 0);
      }

      discountedAmount = discountedAmount.toFixed(2);

     /** Total Price El */

      var $totalAmount = $('<div>', {
        'html': '<p class="total-amount-data">Price:' + '<strong>USD ' + totalAmount.toFixed(2)  + '</strong></p>',
        'class': 'total-amount-wrapper'
      });

      var $discountAmount = $('<div>', {
        'html': '<p class="discount-amount-data">Discount: <strong>USD ' + (totalAmount - discountedAmount).toFixed(2)  + '</strong></p>',
        'class': 'discount-amount-wrapper'
      });

      var $toPayAmount = $('<div>', {
        'html': '<p class="to-pay-amount-data">Total:' + ' <strong>USD ' + discountedAmount  + '</strong></p>',
        'class': 'to-pay-amount-wrapper'
      });

      var $checkoutBtn = $('<button>', {
        'data-component': 'button',
        'data-name': 'buy-button',
        'class': 'modal-button--checkout fixed-button py-3',
        'text': headerVars.dictionary.proceed_to_checkout
      }).on('click', function() {
        
        var packages = vr.state.cart.premium_packages;

        var cartData = Object.keys(packages).map(function(k) { 
          if (discountPct) {
            packages[k].discount = discountPct;
          }

          return vr.state.cart.premium_packages[k]; 
        });

        $.each(cartData, function(index, el) {
            if(cartData[index].activation_date){
                var dateMoment = moment.utc(cartData[index].activation_date).format('YYYY-MM-DD');
                cartData[index].activation_date = dateMoment;
            }
        });
      
        $.ajax({
          url: 'private/billing/checkout',
          method: 'POST',
          dataType: 'json',
          data: {
            cart: cartData,
          },
          success: function(resp) {
            if (resp.status == 'success') {
              window.location.href = resp.url;
            }
          }
        })
      });

      $cartFooter.append($totalAmount,$discountAmount, $toPayAmount, $checkoutBtn);

      $page2.append($titleBar, $cartTable, $cartFooter);

      $container.append($page1, $page2);

      return $container;
    },

    _drawEscortItem: function (escort) {
      var inCart = false;
      var cartPackages = this.state.cart.premium_packages;
      if (cartPackages) {
        var inCartEscorts = Object.keys(cartPackages).map(function (idx) {
          return cartPackages[idx].escortId;
        });

        var escortPackagePair = Object.keys(cartPackages).reduce(function (a, idx) {
          a[cartPackages[idx].escortId] = idx;

          return a;
        }, {});

        inCart = inCartEscorts.includes(escort.id.toString());
      }

      var $tr = $('<div/>', {
        'class': 'tr'
      });

      var $photo = $('<img/>', {
        'data-component': 'image',
        'data-name': 'image',
        'src': escort.photo,
        'css': {
          'width': '80px',
          'height': '80px',
          'border-radius': '50%',
          'object-fit': 'cover',
          'object-position': 'center right'
        },
        'class': inCart ? 'in-cart' : ''
      });

      var $escortInfo = $('<div/>', {
        'data-component': 'td',
        'data-name': 'escortinfo',
        'class': 'info--escort',
        'html': '<p class="info--escort--showname"><b>' + escort.showname + '</b><br>ID: #'+escort.id+'</p>' 
      });

    if (!inCart) {
        if (!escort.escort_packages.length) {
          var $tdInfo = $('<div/>', {
            'text': 'Your currently have a free basic ad with minimum visibility.',
            'class': 'desc--row my-auto d-flex'
          });
        } else {
          var infoPackages = escort.escort_packages.reduce(function (a, p) {
            if (p.expiration_date == null) {
            return ((a ? a + ' | ' : '') + '<span> <b>'+ p.package_name +'</b> will be activated shortly.</span>');
            }else{
              return ((a ? a + ' | ' : '') + '<span>Has ' + p.package_name + '  until ' + moment.unix(p.expiration_date).format("D MMM YYYY")  + '. </span>');
            }
          }, '');

          var $tdInfo = $('<div/>', {
            'html': infoPackages,
            'class': 'desc--row my-auto d-flex'
          });
        }
      } else {
        var $tdInfo = $('<div>', {
          'text': 'Is in your cart',
          'class': 'desc--row in-cart align-center align-md-left'
        });
      }

      var $tdBtnWrapper1 = $('<div/>', {
        'data-component': 'td',
        'data-name': 'button-wrapper',
        'class': 'wrapper--button'
      });
      var $tdBtnWrapper2 = $tdBtnWrapper1.clone();

      var $newPackageBtn = null;
      
      if (inCart) {
        $newPackageBtn = $('<button/>', {
          'data-component': 'button',
          'data-name': 'purchase-button',
          'title': 'Remove from the cart',
          'data-id': escortPackagePair[escort.id],
          'class': 'button--remove-from-cart button--remove',
          'html': 'Remove &times;'
        });
      } else {
        
        $newPackageBtn = $('<button/>', {
          'data-component': 'button',
          'data-name': 'purchase-button',
          'data-id': escort.id,
          'title': !escort.escort_packages.length ? 'Upgrade now' : 'This Escort is Upgraded',
          'class': !escort.escort_packages.length ? 'button--purchase inverted' : 'button--purchase disabled',
          'text': !escort.escort_packages.length ? 'Upgrade now' : 'Upgraded'
        });
      }
      $tdBtnWrapper2.append($newPackageBtn);
      $escortInfo.prepend($photo);
      $tr.append($escortInfo, $tdInfo, $tdBtnWrapper2);

      return $tr;
    },

    _bindEvents: function() {
      var vr = this;
      
      $('.button--purchase:not(.disabled)').on('click', function() {
        var modalContent = vr._getModalContent(null, null);

        var $modal = new Modal({
          close: true,
          header: modalContent.$header,
          body: modalContent.$body
        });
        $modal.show();

        var cartItem = vr._getCartItem($modal);
        $modal.find('.amount').html( 'USD ' + cartItem.getAmount());

        var $pikaday = new Pikaday({
          format: 'D MMM YYYY',
          minDate : moment().toDate(),
          defaultDate: moment().toDate(),
          formatSubmit: 'yyyy/mm/dd',
          field: $('#premium_packageActivationDate')[0],
          container: $('#ModalOverlay #render-pikaday')[0],
          trigger: $('#psd_scheduled')[0],
         
          onSelect: function(selected_date) {
            console.log(selected_date);
          },
        });

        $modal.find('.modal-button--amount').on('click', function (e) {
          e.preventDefault();

          var cartItem = vr._getCartItem($modal);

          var itemForCart = cartItem.getForCart();
            
          var cartInstance = Cart.getInstance();
          var hash = 'p' + Date.now().toString(16);
          cartInstance.premium_packages[hash] = itemForCart;
          vr.state.cart = cartInstance;
          vr.state.pageView = 2;
          $modal.hide();
          new ViewRenderer(vr.state); 
        });

        $modal.find('input[type="radio"]').on('change', function() {
          var cartItem = vr._getCartItem($modal);
          $modal.find('.amount').html( 'USD ' + cartItem.getAmount());
        });

      });


      $('.button--edit').on('click', function() {
        var premium_packageUid = $(this).data('id');
        var modalContent = vr._getModalContent(null, vr.state.cart.premium_packages[premium_packageUid]);

        var $modal = new Modal({
          close: true,
          header: modalContent.$header,
          body: modalContent.$body
        });
        $modal.show();

        var cartItem = vr._getCartItem($modal);
        $modal.find('.amount').html( 'USD ' + cartItem.getAmount());

        new Pikaday({
             format: 'D MMM YYYY',
          formatSubmit: 'yyyy/mm/dd',
          field: $('#premium_packageActivationDate')[0],
          container: $('#ModalOverlay #render-pikaday')[0],
          trigger: $('#psd_scheduled')[0],
          minDate : moment().toDate(),
        });

        $modal.find('.modal-button--amount').on('click', function (e) {
          e.preventDefault();
          var cartItem = vr._getCartItem($modal);
          var itemForCart = cartItem.getForCart();

          var cartInstance = Cart.getInstance();
          cartInstance.premium_packages[premium_packageUid] = itemForCart;
          vr.state.cart = cartInstance;
          vr.state.pageView = 2;
          $modal.hide();
          new ViewRenderer(vr.state);
        });

        $modal.find('input[type="radio"]').on('change', function() {
          var cartItem = vr._getCartItem($modal);
          $modal.find('.amount').html( 'USD ' + cartItem.getAmount());
        });
      });

      $('.button--delete').on('click', function() {
        var premium_packageUid = $(this).data('id');
        delete vr.state.cart.premium_packages[premium_packageUid];
        if (Object.keys(vr.state.cart.premium_packages).length === 0) {
          vr.state.pageView = 1;
        } else {
          vr.state.pageView = 2;
        }

        new ViewRenderer(vr.state);
      });
    },

    _bindAgencyEvents: function() {
      var vr = this;

      $('.button--purchase:not(.disabled)').on('click', function() {
        var escortId = $(this).data('id');
        var escort = vr.state.escorts[escortId];

        var modalContent = vr._getModalContent(escort, null);

        var $modal = new Modal({
          close: true,
          header: modalContent.$header,
          body: modalContent.$body
        });
        $modal.show();

        var cartItem = vr._getCartItem($modal);
        $modal.find('.amount').html( 'USD ' + cartItem.getAmount());
       
       var $pikaday = new Pikaday({
          format: 'D MMM YYYY',
          minDate : moment().toDate(),
          formatSubmit: 'yyyy/mm/dd',
          field: $('#premium_packageActivationDate')[0],
          container: $('#ModalOverlay #render-pikaday')[0],
          trigger: $('#psd_scheduled')[0],
        });

        $modal.find('.modal-button--amount').on('click', function (e) {
          e.preventDefault();
          var cartItem = vr._getCartItem($modal);
          var itemForCart = cartItem.getForCart();

          var cartInstance = Cart.getInstance();
          var hash = 'p' + Date.now().toString(16);
          cartInstance.premium_packages[hash] = itemForCart;
          vr.state.cart = cartInstance;

          $modal.hide();
          new ViewRenderer(vr.state);

          var infoModalContent = vr._getInfoModalContent();


          var $modalMsg = new Modal({
            header: infoModalContent.$header,
            body: infoModalContent.$body
          });

          $modalMsg.find('.modal-button--continue').on('click', function() {
            vr.state.pageView = 1;
            $modalMsg.hide();
            new ViewRenderer(vr.state);
          });

          $modalMsg.find('.modal-button--gotocart').on('click', function() {
            vr.state.pageView = 2;
            $modalMsg.hide();
            new ViewRenderer(vr.state);
          });

          $modalMsg.show()
        });

        $modal.find('input[type="radio"]').on('change', function() {
          var cartItem = vr._getCartItem($modal);
          $modal.find('.amount').html( 'USD ' + cartItem.getAmount());
        });
      });

      $('.button--edit').on('click', function() {
        var premium_packageUid = $(this).data('id');
        var modalContent = vr._getModalContent(vr.state.escorts[vr.state.cart.premium_packages[premium_packageUid].escortId], vr.state.cart.premium_packages[premium_packageUid]);

        var $modal = new Modal({
          close: true,
          header: modalContent.$header,
          body: modalContent.$body
        });
        $modal.show();

        var cartItem = vr._getCartItem($modal);
        $modal.find('.amount').html( 'USD ' + cartItem.getAmount());

        new Pikaday({
          format: 'D MMM YYYY',
          formatSubmit: 'yyyy/mm/dd',
          field: $('#premium_packageActivationDate')[0],
          container: $('#ModalOverlay #render-pikaday')[0],
          trigger: $('#psd_scheduled')[0],
          minDate: moment().toDate(),
        });

        $modal.find('.modal-button--amount').on('click', function (e) {
          e.preventDefault();

          var cartItem = vr._getCartItem($modal);
          var itemForCart = cartItem.getForCart();
          var cartInstance = Cart.getInstance();
          cartInstance.premium_packages[premium_packageUid] = itemForCart;
          vr.state.cart = cartInstance;
          vr.state.pageView = 2;
          $modal.hide();
          new ViewRenderer(vr.state);
        });

        $modal.find('input[type="radio"]').on('change', function() {
          var cartItem = vr._getCartItem($modal);
          $modal.find('.amount').html( 'USD ' + cartItem.getAmount());
        });
      });

       $('.button--delete').on('click', function() {
        var premium_packageUid = $(this).data('id');
        delete vr.state.cart.premium_packages[premium_packageUid];
        if (Object.keys(vr.state.cart.premium_packages).length === 0) {
          vr.state.pageView = 1;
        } else {
          vr.state.pageView = 2;
        }

        new ViewRenderer(vr.state);
      });

        $('.button--remove-from-cart').on('click', function () {
        var packageUid = $(this).data('id');
        delete vr.state.cart.premium_packages[packageUid];

        new ViewRenderer(vr.state);
      });

      $('.search-bar').find('input').on('input', function() {
        vr.state.searchTerm = $(this).val();
        new ViewRenderer(vr.state);
      });

      $('.sort-bar').find('select').on('change', function() {
        var selectedOpt = $(this).find(":selected").data('id');

        switch (selectedOpt) {
          case 'alphabetically':
            vr.state.visibleEscorts.sort(function(a, b) {
              return ('' + a.showname).localeCompare(b.showname);
            });
            $('.escorts-list-wrapper.agency').html( '')
            $.each(vr.state.visibleEscorts, function(idx, escort) {
              var $escortItem = vr._drawEscortItem(escort);
              $('.escorts-list-wrapper.agency').append($escortItem);
            });
            break;
          case 'by-escort-id':
            vr.state.visibleEscorts.sort(function(a, b) {
              return (a.id - b.id);
            });

            $('.escorts-list-wrapper.agency').html( '')
            $.each(vr.state.visibleEscorts, function(idx, escort) {
              var $escortItem = vr._drawEscortItem(escort);
              $('.escorts-list-wrapper.agency').append($escortItem);
            });
            break;
          case 'latest-modified':
            vr.state.visibleEscorts.sort(function(a, b) {
              return new Date(b.date_last_modified) - new Date(a.date_last_modified);
            });

            $('.escorts-list-wrapper.agency').html( '')
           $.each(vr.state.visibleEscorts, function(idx, escort) {
              var $escortItem = vr._drawEscortItem(escort);
              $('.escorts-list-wrapper.agency').append($escortItem);
            });
            break;
          case 'newest-first':
            vr.state.visibleEscorts.sort(function(a, b) {
              return new Date(b.date_registered) - new Date(a.date_registered);
            });

            $('.escorts-list-wrapper.agency').html( '')
            $.each(vr.state.visibleEscorts, function(idx, escort) {
              var $escortItem = vr._drawEscortItem(escort);
              $('.escorts-list-wrapper.agency').append($escortItem);
            });
            break;
        }
        vr._bindAgencyEvents();

        // vr.state.visibleEscorts.sort()
      });
      
      var $searchBar = $('.search-bar').find('input');

      if (vr.state.searchTerm) {
        $searchBar.focus();

        setTimeout(function(){
          $searchBar.get(0).selectionStart = $searchBar.get(0).selectionEnd = 10000; }, 0);
      } else {
        //$searchBar.focus();
      }
    },

    _getInfoModalContent: function() {
      var vr = this;
      var $header = $('<h6/>', {
        'class': 'card-header',
        'text': 'Package added to a cart'
      });

      var modalTxt = '';
      var packageCnt = Object.keys(vr.state.cart.premium_packages).length;
      var modalTxt = '';

      if (vr.state.agencyDiscounts) {
        var discounts = vr.state.agencyDiscounts.sort(function (a, b) {
          return a.range[0] > b.range[0] ? 1 : -1
        });
        for (var i = 0; i < discounts.length; i++) {
          var el = discounts[i];
          
          if (packageCnt < el.range[0]) {
            modalTxt = '<h6 class="p-4">Buy '+ (el.range[0]-packageCnt) +' more packages to get '+el.discount +'% off.</h6>';
            break;
          }
        }
      }
      var $body = $('<div/>', {
        'class': 'card-body',
        'html': '<div>' + modalTxt + '</div>'
      });

      var $footer = $('<div/>', {
        'data-component': 'footer',
        'data-name': 'modalfooter',
        'class': 'footer d-flex justify-content-center py-3 flex-column align-items-center'
      });

    

      var $goToCartButton = $('<button/>', {
        'data-component': 'button',
        'data-name': 'gotocartbutton',
        'class': 'modal-button--gotocart btn btn-primary my-2 btn-lg',
        'text': 'Go to cart'
      });

      var $continueButton = $('<button/>', {
        'data-component': 'button',
        'data-name': 'continuebutton',
        'class': 'modal-button--continue btn btn-success my-2 btn-lg',
        'text': 'Continue shopping'
      });

      $footer.append($goToCartButton, $continueButton);
      $body.append($footer);

      return {
        $header: $header,
        $body: $body
      };
    },

    _getCartItem: function($container) {
      var premium_packageType = $container.find('input[name="premium_package_name"]:checked').val();
      var escortId = $container.find('input[name="escort_id"]').val();
      var premium_packageDuration = $container.find('input[name="premium_package_duration"]:checked').val();
      var premium_packageGender = $container.find('input[name="premium_package_gender"]').val();
      var packageBaseCity = $container.find('input[name="package_base_city"]').val();
      var escortShowname = $container.find('input[name="escort_showname"]').val();
      
      var premium_packageActivationDate = $container.find('input[name="premium_package_start_date"]:checked').val();
      var activationDate = premium_packageActivationDate == 'scheduled' ? $('#premium_packageActivationDate').val() : null;

      var cartItem = new CartItemBuilder();
      

      cartItem
        .setAvailablePackages(this.state.available_packages)
        .setEscort(escortId)
        .setGender(premium_packageGender)
        .setType(premium_packageType)
        .setDuration(premium_packageDuration)
        .setActivationDate(activationDate)
        .setBaseCityId(packageBaseCity);

      if (escortShowname) {
        cartItem.setShowname(escortShowname);
      }

      return cartItem;
    },

    _getModalContent: function(escort, premium_package) {

      var vr = this;
      var showname = '';

      if(escort){
        showname = '(' + escort.showname + ')';
      }

      var $header = $('<h6/>', {
        'class': 'modal-header',
        'text': !premium_package ? headerVars.dictionary.set_package : 'Edit the package ' + showname
       
      });

      var $body = $('<div/>', {
        'class': 'modal-body'
      });

      var $bodyRow1 = $('<tr/>', {
        'data-component': 'tr',
        'data-name': 'tablerow',
        'class': ''
      });

      var $bodyRow1 = $bodyRow1.clone().addClass(' d-flex d-flex flex-column flex-md-row');
      var $bodyRow2 = $bodyRow1.clone().addClass('duration d-flex d-flex flex-column flex-md-row');
      var $bodyRow3 = $bodyRow1.clone().addClass('flex-column flex-sm-row premium-package-datetime d-none d-md-flex');
      var $bodyRow4 = $bodyRow1.clone().addClass('paddingless d-flex');

      var countryISO = escort ?  escort.country_iso : vr.state.userData.country_iso;
      

      if (escort) {
        var inputs = '<input type="hidden" name="escort_showname" value="' + escort.showname + '" />';
      }else{
        var inputs ='';
      }

      $bodyRow4.append($('<div/>', {
        'data-component': 'td',
        'data-name': 'premium_packagedata',
        'class': 'td sc-input-block',
        'html': '<input type="hidden" name="premium_package_gender" value="' + (escort ? escort.gender : vr.state.userData.gender) + '" />' +
          '<input type="hidden" name="escort_id" value="' + (escort ? escort.id : vr.state.userData.id) + '" />' +
          '<input type="hidden" name="package_base_city" value="' + (escort ? escort.base_city_id : vr.state.userData.base_city_id) + '" />'
          + inputs
      }));

      $.each(['VIP','Premium'],function(idx,premium_packageName) {
        $bodyRow1.append($('<div/>', {
          'data-component': 'td',
          'data-name': 'premium_packagedata',
          'class': 'td sc-input-block scheduled',
          'html': '<input type="radio" id="pn_' + premium_packageName + '" name="premium_package_name" value="' + premium_packageName + '" /><label class="" for="pn_' + premium_packageName + '">' + premium_packageName + '</label>'
        }));
      });

      if (premium_package) {
        $bodyRow1.find('#pn_' + premium_package.type).attr('checked', true);
      } else {
        $bodyRow1.find('input[type="radio"]').filter(':first').attr('checked', true);
      }

      $.each(['15', '30', '90'], function(idx,duration) {

        $bodyRow2.append($('<div/>', {
          'data-component': 'td',
          'data-name': 'premium_packagedata',
          'class': 'td sc-input-block',
          'html': '<input type="radio" id="pd_'+ duration + '" name="premium_package_duration" value="' + duration + '" /><label for="pd_'+ duration + '"><span class="d-md-none">Premium </span>' + duration + ' <span class="">' + headerVars.dictionary.__days + '</span></label>'
        }));

      });

      if (premium_package) {

        $bodyRow2.find('#pd_' + premium_package.duration).attr('checked', true);
      } else {

        $bodyRow2.find('input[type="radio"]').filter(':first').attr('checked', true);
      }


      $.each([{ title: headerVars.dictionary.starts_now, subtitle: ' ' + headerVars.dictionary.after_package_ends, dataClass: 'Starts now'}, { title: headerVars.dictionary.Scheduled, subtitle: headerVars.dictionary.starts_at, dataClass: 'Scheduled' }]
        ,function(idx,startDate) {
          if (startDate.dataClass == 'Starts now') {
            
          }
        var sd = startDate.dataClass.toLowerCase().replace(/ /g, '-');
        $bodyRow3.append($('<div/>', {
          'data-component': 'td',
          'data-name': 'premium_packagedata',
          'class': 'td sc-input-block',
          'html': '<input type="radio" id="psd_'+ sd + '" name="premium_package_start_date" value="' + sd + '"/>' +
            '<label class="psd_'+ sd + '" for="psd_'+ sd + '">' + startDate.title + '<div class="text-muted">' + startDate.subtitle + '</span></label>' +
            (startDate.dataClass !== 'Starts now' ? '<input type="text" disabled id="premium_packageActivationDate"/><div id="render-pikaday"></div>' : '')
        }));
      });
      
      if (premium_package) {
        $bodyRow3.find('#psd_' + (!premium_package.activationDate ? 'starts-now' : 'scheduled')).attr('checked', true);
      } else {
        $bodyRow3.find('input[type="radio"]').filter(':first').attr('checked', true);
      }

      var $footer = $('<div/>', {
        'data-component': 'footer',
        'data-name': 'modalfooter',
        'class': 'footer d-flex justify-content-between pt-5'
      });

       var $amountTitle = $('<div/>', {
        'class': 'total-amount',
        'text': 'Total:'
      });

      var $amount = $('<div/>', {
        'data-component': 'block',
        'data-name': 'amount',
        'class': 'amount',
        'text': ''
      });

      var $buyButton = $('<button/>', {
        'data-component': 'button',
        'data-name': 'buy-button',
        'class': 'modal-button--amount btn btn-success rounded ml-auto btn-lg',
        'text': !premium_package ? headerVars.dictionary.add_cart : 'DONE'
      });

      $footer.append($amountTitle,$amount, $buyButton);

      var $bodyRow2Title = $('<h6/>', {
        'class': 'duration_title d-block d-md-none py-2',
        'text': 'Duration'
      });

      $body.append( $bodyRow4, $bodyRow1, $bodyRow2Title, $bodyRow2,  $bodyRow3, $footer);

      return {
        $header: $header,
        $body: $body
      }
    },

    _canBuy: function(premium_packages) {
      var STATUS_PENDING = 1;
      var STATUS_ACTIVE = 2;
      
      if (premium_packages.length === 1 && premium_packages[0].status === STATUS_PENDING ) {
        return false;
      }

      if (premium_packages.length === 1 && premium_packages[0].status === STATUS_ACTIVE ) {
        return false;
      }

      if (premium_packages.length === 2) {
        return false;
      }

      return true;
    }
  }


  function Modal (params) {
    /**
     * params should contain { header, body }
     */
    return this.construct(params);
  };
  Modal.prototype = {
    construct: function (params) {
      this.params = params || {
        header: $('<div>'),
        body: $('<div>')
      };
  
      this.build().bindEvents();
  
      this.html.show = this._show;
      this.html.hide = this._hide;
  
      return this.html;
    },

    _buildOverlay: function () {
      return $('<div/>', {
        id: this.params.id || 'ModalOverlay',
        class: 'modal show',
        css: {
             display: 'block'
        }
      });
    },

    _buildModal: function () {
      return  $('<div/>', {
        class: 'modal-dialog'
      });
    },

    _buildCloseBtn: function() {
      return $('<div/>', {
        class: 'close-btn p-2',
        html: '&times;'
      });
    },
  
    build: function () {
      var Overlay = this._buildOverlay(),
        Modal = this._buildModal(),  
        dialog = $('<div/>', {class: "modal-dialog"});
    
      if (this.params.close) {
        this.closeBtn = this._buildCloseBtn();
        Modal.append(this.closeBtn);
      }
  
      Modal.append(this.params.header);
      Modal.append(this.params.body);
      Modal.wrapInner("<div class='modal-content'></div>");
    
      Overlay.append(Modal);
     
      this.Overlay = Overlay;
  
      $(document.body).append(Overlay);
  
      return this;
    },
  
    bindEvents: function () {
      var _self = this;
  
      if (this.closeBtn) {
        this.closeBtn.on('click', function() {
          _self.Overlay.remove();
        });
      }
  
      return this;
    },
  
    _show: function () {
      this.find('.Modal').addClass('shown');
    },
  
    _hide: function () {
      this.remove();
    },
  
    get html() {
      return this.Overlay;
    }
  };


  function loadPage () {
    var $container = $('#ui-view');
    $container.LoadingOverlay("show", {color : "rgba(239, 243, 249, 0.80)", zIndex: 1000   });
    
    $.ajax({
      url: 'private/billing/index',
      dataType: 'json',
      success: function(data){
        
        var bootstrap = new Bootstrap;
        
        if(data.user_type == 'escort'){
          if((data.escort_status & 8) == 8){
              _draw_notification('owner-disabled');
          }else if((data.escort_status & 67) == 67 && (data.escort_status & 128) != 128){
            _draw_notification('complete_7_steps');
          }
          else{
              bootstrap.run(data.user_type, data);
          }
        }else if(data.user_type == 'agency'){
           if(data.escorts.length > 0){
              bootstrap.run(data.user_type, data);
           }else{
              _draw_notification('escorts_no_found');
           }
           
        }
        return 0;
      },
      complete: function(){
         $container.LoadingOverlay("hide", true);
      }
    })
  };

  return {
    load: loadPage
  };
})(window);
Cubix.SelfCheckout.load();