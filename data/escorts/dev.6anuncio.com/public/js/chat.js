function Chat(sessionId) {

    var socket,
        forceEnable,
        onlineUsers = [], //For easily sorting users
        onlineUsersObj = {}, //For easily getting user by id
        selfUser = {},
        browser = $.browser.name;
    browserVersion = $.browser.version;

    var tabsCanOpened = 5,
        openDialogsUsers = {},
        skippedDialogs = {};

    var audio;

    var ismobile = ($( document ).width() < 993)?true:false;
    
    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    var messages = {
        'offlineMessage': 'This user is offline. They will receive the message next login',
        'blackListedWordsMessage': 'You can`t use words "%words%"',
        'userBlockedYou': 'Message not sent. This user has blocked you.'
    };

    var originalTitle = document.title;
    var blinkTimer;

    this.connect = function () {
		console.log('Connecting to socket');
        if (typeof socket == 'undefined') {
            socket = io('/chat');
        } else {
            socket.connect();
        }
    };

    this.initListeners = function () {
        socket.on('auth-complete', function (data) {
            signedOffToolbar();
            if (data.auth) {

                if (data.availability) {
                    availableToolbar();
                    selfUser = data.userInfo;
                    prepareSettings();
                    visibilityEscortsList(true);
                } else {
                    notAvailableToolbar();
                    visibilityEscortsList(false);
                }
            } else {

            }

            $('chat-container').removeClass('hidden');
        });
        socket.on('connect', function () {
            if (typeof forceEnable == 'undefined') {
                forceEnable = false;
            }

            $('.mCSB_container ').empty();

            socket.emit('auth', {
                sid: sessionId,
                forceEnable: forceEnable
            });
            forceEnable = false;
        });

        socket.on('online-users-list', function (users) {
            onlineUsers = [];
            onlineUsersObj = users;
            for (var i in users) {
                //If user is blocked do not do anything
                if (typeof selfUser.settings.blockedUsers != 'undefined' && selfUser.settings.blockedUsers.indexOf(users[i].userId) >= 0) {
                    continue;
                }
                onlineUsers.push(users[i]);
            }
            sortOnlineUsers();
            fillOnlineUsers();
        });

        socket.on('new-user', function (userInfo) {
            //If user is blocked do not do anything
            if (typeof selfUser.settings != 'undefined' && typeof selfUser.settings.blockedUsers != 'undefined' && selfUser.settings.blockedUsers.indexOf(userInfo.userId) >= 0) {
                return;
            }

            insertUser(userInfo);
            if ($('#user_tab_' + userInfo.userId)) {
                $('#user_tab_' + userInfo.userId).removeClass('away').removeClass('offline').addClass('available');
            }
        });

        socket.on('offline-users', function (userIds) {
            userIds.forEach(function (userId) {
                removeUser(userId);
            });
        });

        socket.on('open-dialogs', function (data) {

            //Creating open tabs
            data.dialogs.forEach(function (dialog) {
                if(!dialog) return;
                
                createChatTab(dialog.userInfo);
            });

            //Open active dialog
            if (data.activeDialog) {
                openUserTab(data.activeDialog, true);
            }
        });

        socket.on('redirect-to-unread-thread', function (msgs) {
            if(typeof msgs[0] !== 'undefined' ){
                window.location.href = "/pm#"+msgs[0].userId;
            }else{
                window.location.href = "/pm#";
            }
        });
        
        socket.on('new-message', function (data) {
            createChatTab(data.senderData);
            markTabUnread(data.senderData.userId);
            addMessage(data.senderData.userId, data.senderData, data.message);
            blinkPageTitle('New message from ' + data.senderData.nickName);
            playAudio('new-message');
            scrollToBottom($('#user_tab_' + data.senderData.userId + ' .active_chat_content'));
        });

        socket.on('new-messages', function (messages) {

            messages.forEach(function (message) {
                if (message && message.senderInfo) {
                    createChatTab(message.senderInfo);
                    markTabUnread(message.userId, message.count);
                }
            });
        });

        socket.on('message-history', function (data) {
            fillHistory(data.senderInfo, data.messages);
        });

        socket.on('unread-messages-count', function (unread_messages) {
            if(unread_messages.length > 0){
                $.each(unread_messages, function(k, unread_message){
                    userTab = $('#user_tab_' + unread_message.userId);
                    if (userTab.children().hasClass('minimize')) {
                        $('.notification-count').html(unread_messages.length);
                    }
                    $('.mobile-notification-count').html(unread_messages.length);
                })
            }else{
                $('.notification-count').html('');
                $('.mobile-notification-count').html('');
            }
        });


        socket.on('typing-start', function (data) {
            userTab = $('#user_tab_' + data.userId);
            if (userTab.length) {
                $('<div/>')
                    .append('<div id="typing_' + data.userId + '" class="typing"><span class="ion-chatbubble-working"></span></div>')
                    .appendTo(userTab.find('.chat-discussion-content'));
            }
        });
        socket.on('typing-end', function (data) {
            $('#user_tab_' + data.userId).find('.typing').remove();
        });

        socket.on('chat-off', function (data) {
            notAvailableToolbar();
        });

        socket.on('bl-word', function (data) {
            showMessage(data.dialogId, messages['blackListedWordsMessage'].replace('%words%', data.word));
        });

        socket.on('user-blocked-you', function (data) {
            showMessage(data.dialogId, messages['userBlockedYou']);
        });

        socket.on('blocked-users', function (users) {
            $('blocked-users-list').empty();

            new Element('option', {
                value: '',
                html: 'Select'
            }).inject($('blocked-users-list'));

            users.each(function (user) {
                new Element('option', {
                    value: user.userId,
                    html: user.nickName
                }).inject($('blocked-users-list'));
            });
        });
    }

    this.initEvents = function () {
        var self = this;

        $('#chat-switcher').on('click', function () {
            if ($(this).is(':checked')) {
                self.enableChat();
            } else {
                self.disableChat();
            }

        });

        $('.chat-notification-block').on('click', function () {
             socket.emit('latest-unread-msg');
        });


        $('#baddy-list-tab').off('click');
        $('#baddy-list-tab').on('click', function () {
            if ($('#baddy-list').hasClass('closed')) {
                showBaddyList();
            } else {
                hideBaddyList();
            }
        });

        $('#baddy-list .tab_header').on('click', function (e) {
            if (e.target.className.indexOf('tab_header') != -1) {
                hideBaddyList();
            }
        });

        $('.chat-contact-list .ion-close-round').on('click', function (e) {
            hideBaddyList();
        });


        $('.chat-contact-list').on('click', function (e) {
            if ($(this).hasClass('minimize')) {
                showBaddyList();
            }
        });

        $('#baddy_list_search').focus(function () {
            if ($(this).val() === 'Search') {
                $(this).val('');
            }
        });
        $('#baddy_list_search').blur(function () {
            if (!$(this).val()) {
                $(this).val('Search');
            }
        });

        //Search logic
        $('#baddy_list_search').keyup(function () {
            if (!onlineUsers.length) return;
            onlineUsers.forEach(function (user) {

                if (user.nickName.indexOf($('#baddy_list_search').val()) === -1 || (selfUser.settings.showOnlyEscorts == 1 && user.userType != 'escort')) {
                    $('#user_' + user.userId).removeClass('d-flex').hide();
                } else {
                    $('#user_' + user.userId).addClass('d-flex').show();
                }
            });
        });

    }


    this.chatWith = function (userId) {
        if (userId == selfUser.userId) return false;

        if (typeof onlineUsersObj[userId] != 'undefined') {
            createChatTab(onlineUsersObj[userId], true);
        } else {
            socket.emit('chat-with', {
                userId: userId
            });
        }
    }

    this.getStatus = function () {
        return selfUser.status;
    }

    this.isOnline = function (userId) {
        if (typeof onlineUsersObj[userId] != 'undefined') {
            return true;
        } else {
            return false;
        }
    }

    this.enableChat = function () {
        var self = this;
        availableToolbar();
        forceEnable = true;
        self.connect();
    }

    this.disableChat = function () {
        var self = this;
        notAvailableToolbar();
        socket.emit('availability-changed', 0);
        socket.disconnect();
        hideSkippedTabs();
    }

    hideSkippedTabs = function () {
        $('.more-open-tabs').remove();
        skippedDialogs = {};
        openDialogsUsers = {};
        $('#chat-container').html('');
    }

    showBaddyList = function () {
        $('#baddy-list').removeClass('closed');
        $('#baddy-list').removeClass('hidden');
        $('#baddy-list-tab').addClass('expand');
        $('#baddy_list_search').focus();

        $('.chat-contact-list').removeClass('minimize');
        $('.chat-info-online-escorts').show();
    }

    hideBaddyList = function () {
        $('.chat-contact-list .chat-info-online-escorts').slideUp('50', function () {
            $('.chat-contact-list').addClass('minimize');
        });
    }

    availableToolbar = function () {
        $('.minimize').removeClass('minimize');
        $('#chat-wrapper').trigger('chat-on');
        $('.chat_base.signed_off').addClass('hidden');
        $('.chat_base.signed_on').removeClass('hidden');
        //$('.baddy_list').addClass('hidden');
        $('.baddy_list_tab').removeClass('expand').removeClass('hidden');
        $('.open_chat ').addClass('hidden');
    }

    notAvailableToolbar = function () {

        $('chat-wrapper').trigger('chat-off');
        $('.chat-discussion').remove();
        $('.chat-contact-list .chat-info-online-escorts').slideUp('50', function () {
            $('.chat-contact-list').addClass('minimize');
        });

        $('.baddy_list').addClass('hidden');
        $('.chat_base.signed_off').addClass('hidden');
        $('.baddy_list_tab').addClass('hidden');
        $('.chat_base.signed_on').removeClass('hidden');
        $('.open_chat ').removeClass('hidden');
        $('.mCSB_container').html(' ');

    }

    signedOffToolbar = function () {
        $('.chat_base.signed_off').removeClass('hidden');
        $('.chat_base.signed_on').addClass('hidden');
        $('.open_chat ').addClass('hidden');
    }


    updateOnlineUsersCount = function (count) {
        $('.online-users-count').html(count);
        // $('.notification-count').html(count);
    }

    fillOnlineUsers = function () {

        onlineUsers.forEach(function (user,i) {
            if(user.nickName.trim() === '(Escort)'){
                onlineUsers.splice(i, 1);
                return;
            }

            insertUserBuddyList(user);
        });
        updateOnlineUsersCount(onlineUsers.length);
    }

    insertUser = function (userInfo) {
        beforeId = false;
        onlineUsers.forEach(function (u, i) {
            if (userInfo.nickName.toLowerCase() <= u.nickName.toLowerCase() && !beforeId) {
                beforeId = u.userId;
                onlineUsers.splice(i, 0, userInfo); // inserting user in onlineUsers list
            }
        });

        if (!beforeId) {
            onlineUsers.push(userInfo);
        }

        onlineUsersObj[userInfo.userId] = userInfo;
        //draw user in buddy list before beforeId.
        //if beforeId is false insert at bottom.

        insertUserBuddyList(userInfo, beforeId);
        updateOnlineUsersCount(onlineUsers.length);
    }

    removeUser = function (userId) {
        onlineUsers.forEach(function (u, i) {
            if (u.userId == userId) onlineUsers.splice(i, 1); //removing from onlineUsers list
        });
        delete onlineUsersObj[userId];

        removeUserBuddyList(userId) //removing from buddy list
        updateOnlineUsersCount(onlineUsers.length);
    }

    insertUserBuddyList = function (userInfo, beforeId) {
        section = getUserRow(userInfo);

        section.appendTo($('.mCSB_container'));

        attachUserRowEvents(section);
    };

    removeUserBuddyList = function (userId) {
        if (!$('#user_' + userId).length) return;

        $('#user_' + userId).remove();

    }

    getUserRow = function (user) {

        var section = $('<div/>', {id: 'user_' + user.userId, 'class': 'online-escort d-flex bl-' + user.userType})
            .append('<div class="online-escort-photo">' +
                '<img class="user_avatar" src="' + user.avatar + '" ' +
                '</div>');

        $('<div class="online-escort-info d-flex flex-column justify-content-center w-100 pl-2"></div>')
            .append('<div class="online-escort-name">' + user.nickName + '</div><div class="online-escort-location"><span class="user_status available"></span></div>')
            .appendTo(section);


        //If in settings showonlyescorts = 1 set display = none
        if (user.userType != 'escort' && user.userType != 'agency' && typeof selfUser.settings.showOnlyEscorts != 'undefined' && selfUser.settings.showOnlyEscorts) {
            section.css('display', 'none');
        }

        return section;
    }

    sortOnlineUsers = function () {
        onlineUsers.sort(function (a, b) {
            if (a.nickName.toLowerCase() < b.nickName.toLowerCase()) {
                return -1;
            }
            if (a.nickName.toLowerCase() > b.nickName.toLowerCase()) {
                return 1;
            }

            return 0;
        });
    }

    //When user_row adds in baddy list, adding to this element eventss
    attachUserRowEvents = function (el) {
        el.on('click', function () {
            userId = el.attr('id').replace('user_', '');
            createChatTab(onlineUsersObj[userId], true);
        });
    }


    createChatTab = function (user, isOpened, from_shift_tab) {
        if (typeof isOpened == 'undefined') isOpened = false;

        if ($('#user_tab_' + user.userId).length) {
            if (isOpened) {
                openUserTab(user.userId);
            }
        } else {
            //Emiting about opened dialog
            socket.emit('dialog-opened', {
                userId: user.userId
            });

            socket.emit('conversation-read', {
                userId: user.userId
            });

            openDialogsUsers[user.userId] = user;

            shortNickName = user.nickName;
            if (shortNickName.length >= 12) {
                shortNickName = shortNickName.substr(0, 12) + '...';
            }

            userTabClass = 'user_tab cpoint';
            if (user.status == 'online') {
                userTabClass += ' available';
            }

            let activeChatCol = $('<div/>', {
                "class": `col-2 o p-0 user-chat-tab`,
                'id': 'user_tab_' + user.userId,
                "data-id": user.userId
            });

            activeChat = $('<div/>',
                {"class": "chat-block chat-discussion active_chat", 'id': 'active_chat_' + user.userId})
                .appendTo(activeChatCol);

            tabHeader = $('<div/>', {
                'class': 'tab_header cpoint chat-block-head d-flex '
            }).click(function () {
                if ($(this).parent().hasClass('minimize')) {
                    $(this).parent().removeClass('minimize');
                    showUserTab(user.userId);
                } else {
                    $(this).parent().addClass('minimize');
                    hideUserTab(user.userId);
                }
            });

            shortNickName = user.nickName;
            if (shortNickName.length >= 20) {
                shortNickName = shortNickName.substr(0, 20) + '...';
            }

            if (typeof user.link != 'undefined' && user.link.length) {
                //escort with link name
                $('<div/>', {'class': 'chat-online-escorts-title'})
                    .append(`<a class="text-white" href="${user.link}">${shortNickName}</a>`)
                    .appendTo(tabHeader);

            } else {
                //escort name
                $('<div/>', {'class': 'chat-online-escorts-title'})
                    .append(`<span>${shortNickName}</span>`)
                    .appendTo(tabHeader);

            }
            //Online icon
            $('<div/>', {'class': 'chat-escorts-status ' + user.status}).appendTo(tabHeader);

            //ACtive tab options
            $('<div/>', {'class': 'chat-block-actions ml-auto'})
                .append(`<span class="messages-count"></span>`)
                .append(`<i class="ion-close-round close_button showen cpoint"></i>`)
                .appendTo(tabHeader);


            tabHeader.appendTo(activeChat);

            activeChatContent = $('<div/>', {'class': 'active_chat_content position-relative'})
                .append(`<div class="messaging chat-discussion-content"></div>`)
                .appendTo(activeChat);


            var textareaWrapper = $('<div/>', {"class": "chat-write-msg textarea_inner"})
                .append(`<div class="input-group mb-2 mr-sm-2 mb-sm-0 textarea_wrapper">
					    <textarea class="form-control no-resize" maxlength="200" placeholder="Type a message"  rows="1"></textarea>
						<div class="input-group-addon send-message-button"><i class="ion-android-send"></i></div>
					</div>`)
                .appendTo(activeChat);

            //Tmp solution. ff can't getScroll if overflow hidden
            if (browser == 'firefox') {
                textareaWrapper.find('textarea').css('overflow', 'auto');
            }

            activeChatCol.appendTo($('#chat-container'));

            if (skippedDialogs[user.userId]) {
                delete skippedDialogs[user.userId];
            }
            checkOpenTabs(user, from_shift_tab);

            /*<--drawing chat opened window*/


            if (isOpened) {
                openUserTab(user.userId);
            }

            attachUserTabEvents(user.userId);
        }
    }

    checkOpenTabs = function (user, from_shift_tab) {

        //get opened dialogs count
        var open_tabs = Object.keys(openDialogsUsers).length;

        //checking if opentabs less then 5 remove more chats icon and exit
        if (open_tabs <= tabsCanOpened) {
            $('.more-open-tabs').remove();
            return;
        }
        ;

        //checking if opentabs more then 5 add more chats icon
        if (!$('.more-open-tabs').length) {
            createSkippedUsersTab();
        }

        //when function is calling from closeUserTab we don't need remove tab because tab already removed)
        if (!from_shift_tab) {
            var first_chat_tab = $('.user-chat-tab').first();

            var skippedUserId = first_chat_tab.attr('data-id');

            if (openDialogsUsers[skippedUserId]) {
                skippedDialogs[skippedUserId] = openDialogsUsers[skippedUserId];
            }

            first_chat_tab.remove();
        }

        //Set skipped users count
        $('.more-open-tabs .chat-icon')
            .html(`<span class="skipped-users-count"> ${Object.keys(skippedDialogs).length}</span>`);


        $('#skipped-users-area').html("");

        var ul = $('<ul/>', {"class": 'skipped-users-list p-0'});

        //skipped users list generator
        $.each(skippedDialogs, function (i, el) {
            var li = $('<li/>', {"class": "p-1 skipped-user-row", "data-id": el.userId});

            $('<span/>', {"class": "px-1 show-user-tab"})
                .html(el.nickName)
                .click(function () {
                    //setting event for open skipped user tab
                    var data_id = $(this).parent().attr('data-id');

                    if($('#user_' + data_id).length){
                        $('#user_' + data_id).trigger('click');
                    }else{
                        createChatTab(skippedDialogs[data_id], true);
                    }

                })
                .appendTo(li);

            $('<span/>', {"class": "px-2 delete-skipped-tab"})
                .html('<i class="ion-close-round close_button showen cpoint"></i>')
                .click(function () {
                    //setting event for close skipped user tab
                    var wrapper = $(this).parent();
                    var data_id = wrapper.attr('data-id');
                    wrapper.remove();

                    delete skippedDialogs[data_id];
                    delete openDialogsUsers[data_id];

                    if (!Object.keys(skippedDialogs).length) {
                        $('.more-open-tabs').remove()
                    } else {
                        $('.more-open-tabs .chat-icon')
                            .html(`<span class="skipped-users-count"> ${Object.keys(skippedDialogs).length}</span>`);
                    }

                })
                .appendTo(li);


            li.appendTo(ul);

        });

        ul.appendTo($('#skipped-users-area'));

    };

    shiftOpenTabs = function () {
        if (Object.keys(skippedDialogs).length < 1) return;

        var lastSkippedUserId;

        for (lastSkippedUserId in skippedDialogs);

        createChatTab(openDialogsUsers[lastSkippedUserId], false, true);

        delete skippedDialogs[lastSkippedUserId];

    };

    createSkippedUsersTab = function () {
        var wrapper = $('<div/>', {"class": "col-2 text-right more-open-tabs"})
            .append(`<div id="skipped-users-area" style="display: none"></div>`);

        $('<span/>', {"class": "ion-chatboxes chat-icon"}).click(function () {
            $('#skipped-users-area').toggle();
        }).appendTo(wrapper);

        wrapper.prependTo('#chat-container');
    };

    //Attaching events to a new user tab
    attachUserTabEvents = function (id) {
        var activeChat = $('#user_tab_' + id);

        var textarea = activeChat.find('textarea');

        $('#user_tab_' + id + ' .close_tab, #active_chat_' + id + ' .close_button').on('click', function () {
            closeUserTab(id);
        });

        $('#user_tab_' + id + ' .close_tab, #active_chat_' + id + ' .minimize_button').on('click', function () {
            closeUserTab(id);
        });

        $('#active_chat_' + id + ' .tab_header').on('click', function (e) {
            if (e.target.className.indexOf('tab_name') >= 0 || e.target.className.indexOf('settings_button') >= 0) {
                return;
            }

            if ($('#user_tab_' + id).hasClass('expand')) {
                hideUserTab(id);
            } else {
                openUserTab(id);
            }
        });

        $('#active_chat_'+id + ' .send-message-button').on('click',function () {
            var e = $.Event( "keypress", { which: 13 } );
            $(this).prev().trigger(e);
        });

        var typing = false,
            timer;

        textarea.on('keypress', function (e) {
            textarea.next().addClass('msg-ready-to-sent');
            /*--> Detect typing start/end */
            if (!typing) {
                socket.emit('typing-start', {
                    userId: id
                });
                typing = true;

            }

            clearTimeout(timer);
            timer = setTimeout(function () {
                socket.emit('typing-end', {
                    userId: id
                });
                typing = false;
            }, 3 * 1000);
            /*<-- Detect typing start/end */

            if (e.which == 13 && !e.shiftKey) {
                e.preventDefault();

                message = $(this).val();

                socket.emit('message-sent', {
                    userId: id,
                    message: message
                });

                textarea.next().removeClass('msg-ready-to-sent');

                //After enter empty text area and set default height
                $(this).val('');
                // $(this).css('height', 18);

                message = {
                    body: message,
                    date: new Date().getTime(),
                    userId: selfUser.userId
                }

                addMessage(id, selfUser, message, true);

                //If client is sending message and participant is offline show notification
                if ($('#user_tab_' + id).hasClass('offline')) {
                    showMessage(id, messages.offlineMessage);
                }
            }

        });

        /*-->User settings events*/
        activeChat.find('.block-user').on('click', function () {
            blockUser(id);
        });
        /*<--User settings events*/

    }

    showMessage = function (id, message) {
        activeChatContent = $('active_chat_' + id).getElement('.active_chat_content');

        if (!activeChatContent.getElement('.message_box')) {
            messageBox = new Element('div', {
                'class': 'message_box'
            });
            messageBox.inject(activeChatContent);
        }
        messageBox.set('html', message);

        isAlreadyOpened = false;
        var messageFx = new Fx.Elements(messageBox, {
            onComplete: function () {
                if (!isAlreadyOpened) {
                    isAlreadyOpened = true;
                    setTimeout(function () {
                        messageFx.start({
                            0: {
                                top: [0, (-1) * $('.message_box')[0].getSize().y]
                            }
                        });
                    }, 5 * 1000);
                }
            }
        }).start({
            0: {
                top: [-1 * $('.message_box')[0].getSize().y, 0]
            }
        });

    }

    //Opening user tab for messaging by closing all others
    openUserTab = function (id, scroll) {
        if (typeof scroll == 'undefined') scroll = true;

        socket.emit('dialog-activated', {
            userId: id
        });

        //first Closing all opened tabs
        $('.chat_base .user_tab').removeClass('expand');
        $('.active_chat').addClass('hidden').removeClass('showen');

        showUserTab(id);
    }

    showUserTab = function (id) {
        userTab = $('#user_tab_' + id);
        activeChat = $('#active_chat_' + id);

        if (userTab.length) {

            activeChat.find('textarea').focus();
            if (userTab.find('.new_messages_count').length) {
                userTab.find('.messages-count').removeClass('ion-chatbox');
                userTab.find('.new_messages_count').remove();

                socket.emit('conversation-read', {
                    userId: id
                });

                socket.emit('update-unread-messages-count');

                scrollToBottom(activeChat.find('.active_chat_content'));
            }
        }
    }

    //Closing user tab
    hideUserTab = function (id) {
        socket.emit('dialog-deactivated', {
            userId: id
        });
    }

    getMessageRow = function (userInfo, received) {

        var userMessage = $('<div/>', {'class': `d-flex user_message ${userInfo.userId}`});

        var userAvatar = $('<div/>', {'class': 'user_avatar'});

        if (received) {
            userMessage.addClass('msg-to-you');
            $('<img/>', {src: userInfo.avatar, width: 25, height: 25}).appendTo(userAvatar);
            userAvatar.appendTo(userMessage);
            $('<div/>', {'class': 'messages msg-to-you-content px-2 mx-1'}).appendTo(userMessage);
        } else {
            userMessage.addClass('msg-from-you text-right my-2 px-2 justify-content-end');
            $('<div/>', {'class': 'messages'}).appendTo(userMessage);
        }

        return userMessage
    };

    addMessage = function (id, senderInfo, message, scroll_down) {

        //first of all clearing message
        message.body = clearMessage(message.body);

        if (message.body.length == 0) return;

        userTab = $('#user_tab_' + id);
        activeChat = $('#active_chat_' + id);

        //If tab opened and is not self message emit message-read and only for desktop 
        if (!userTab.children().hasClass('minimize') && senderInfo.userId != selfUser.userId) {
                if(!ismobile){
                     socket.emit('conversation-read', {
                    userId: senderInfo.userId
                });
            }
        }

        if (typeof isNewMessage == 'undefined') {
            isNewMessage = false;
        }
        if (typeof newMessagesCount == 'undefined') {
            newMessagesCount = 1;
        }

        lastUserMessage = activeChat.find('.user_message').last();


        //if messaging is empty or last message is not his, create new user_message raw
        //else add to existing one
        if (lastUserMessage && (lastUserMessage.hasClass(message.userId) && message.date - parseInt(lastUserMessage.attr('id')) <= 60 * 1000)) {

            $('<p/>')
                .html(message.body)
                .appendTo(lastUserMessage.find('.messages'));

        } else {
            if (message.userId == selfUser.userId) {
                userMessage = getMessageRow(selfUser, false);
            } else {
                userMessage = getMessageRow(senderInfo, true);
            }


            userMessage.attr('id', message.date);

            //Print message Content
            $('<p/>', {"class": "m-0"}).html(message.body).appendTo(userMessage.find('.messages'));

            //Print date
            // $('<p/>', {'class': 'm-0 date'}).html(getTimeForMessage(new Date(message.date))).appendTo(userMessage.find('.messages'));

            userMessage.appendTo(activeChat.find('.messaging'));
        }

        $('#typing_' + senderInfo.userId).remove();

        if (scroll_down) {
            scrollToBottom(activeChat.find('.active_chat_content'))
        }
    }

    markTabUnread = function (id, unreadMessagesCount) {
        userTab = $('#user_tab_' + id);

        if (!userTab.find('#active_chat_' + id).hasClass('minimize')) return; //Mark only closed tabs

        if (typeof unreadMessagesCount == 'undefined') unreadMessagesCount = 1;

        if (!userTab.find('.new_messages_count').length) {
            $('<span/>', {'class': 'new_messages_count px-1'})
                .html(unreadMessagesCount)
                .appendTo(userTab.find('.messages-count'));
               

        } else {
            count = parseInt(userTab.find('.new_messages_count').html());
            count += unreadMessagesCount;
            userTab.find('.new_messages_count').html(count);
          
        }
    };

    fillHistory = function (senderInfo, messages) {
        userTab = $('#user_tab_' + senderInfo.userId);
        activeChat = $('#active_chat_' + senderInfo.userId);

        activeChat.find('.messaging').empty();
        if (userTab.length) {
            messages.forEach(function (message) {
                addMessage(senderInfo.userId, senderInfo, message)
            });
            scrollToBottom($('#active_chat_' + senderInfo.userId).find('.active_chat_content'));
        }
    }

    getTimeForMessage = function (date) {
        curDate = new Date();

        hours = date.getHours();
        minutes = ('0' + date.getMinutes()).slice(-2); //two number format

        d = hours + ':' + minutes;

        if (curDate.getDate() != date.getDate()) {
            d += ' ' + date.getDate() + 'th ' + monthNames[date.getMonth()];
        }

        if (curDate.getYear() != date.getYear()) {
            d += ' ' + date.getYear();
        }

        return d;
    }

    clearMessage = function (message) {
        //htmlentities
        message = htmlEntities(message);

        //Replacing one more whitespaces
        message = message.trim();
        message = message.replace(/\s+/g, ' ');

        //replacing urls with html links
        var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
        message = message.replace(exp, "<a href='$1' target='_blank'>$1</a>");


        return message;
    }

    htmlEntities = function (str) {
        return String(str).replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/&/g, '&amp;');
    }

    function scrollToBottom(selector) {
        selector.animate({scrollTop: selector.prop("scrollHeight") + 500}, 0);
    }

    blinkPageTitle = function (message) {
        if (!originalTitle.length) return;
        if (typeof blinkTimer != 'undefined') clearInterval(blinkTimer);
        blinkTimer = setInterval(function () {
            if (document.title == message) {
                document.title = originalTitle;
            } else {
                document.title = message;
            }
        }, 1000);
        window.focus(function () {
            if (typeof blinkTimer != 'undefined') clearInterval(blinkTimer);
            document.title = originalTitle;
            this.removeEvents('click');
        });
    }

    prepareSettings = function () {
        if (typeof selfUser.settings == 'undefined') selfUser.settings = {};

        if (typeof selfUser.settings.showOnlyEscorts != 'undefined' && selfUser.settings.showOnlyEscorts) {
            $('#show-only-escorts').parent().addClass('checked');
        }
        if (typeof selfUser.settings.keepListOpen != 'undefined' && selfUser.settings.keepListOpen) {
            $('#keep-list-open').closest('li').addClass('checked');
            showBaddyList();
        }
        if (typeof selfUser.settings.invisibility != 'undefined' && selfUser.settings.invisibility) {
            $('#invisibility').closest('li').addClass('checked');
        }

        $('#show-only-escorts').unbind('click');

        $('#show-only-escorts').click(function () {
            var li = $(this).closest('li');
            if (li.hasClass('checked')) {
                li.removeClass('checked')
                    .find('span')
                    .html('');

                $('#baddy-list .bl-member')
                    .addClass('d-flex');

                selfUser.settings.showOnlyEscorts = 0;
                socket.emit('change-options', {
                    option: 'show-only-escorts',
                    value: 0
                });
            } else {
                li.addClass('checked')
                    .find('span')
                    .append('<i class="ion-checkmark-round"></i>');

                $('#baddy-list .bl-member')
                    .removeClass('d-flex')
                    .hide();


                selfUser.settings.showOnlyEscorts = 1;
                socket.emit('change-options', {
                    option: 'show-only-escorts',
                    value: 1
                });
            }
        });

        $('#keep-list-open').on('click', function () {
            var li = $(this).closest('li');
            if (li.hasClass('checked')) {
                li.removeClass('checked');

                selfUser.settings.keepListOpen = 0;
                socket.emit('change-options', {
                    option: 'keep-list-open',
                    value: 0
                });
            } else {
                li.addClass('checked');

                selfUser.settings.keepListOpen = 1;
                socket.emit('change-options', {
                    option: 'keep-list-open',
                    value: 1
                });
            }
        });

        $('#invisibility').on('click', function () {
            var li = $(this).closest('li');
            if (li.hasClass('checked')) {
                li.removeClass('checked');

                selfUser.settings.invisibility = 0;
                socket.emit('change-options', {
                    option: 'invisibility',
                    value: 0
                });
            } else {
                li.addClass('checked');

                selfUser.settings.keepListOpen = 1;
                socket.emit('change-options', {
                    option: 'invisibility',
                    value: 1
                });
            }
        });

        $('manage-block-list').on('click', function () {
            $('#baddy-list').find('.settings_list ul').css('display', 'none');
            $('#baddy-list').find('.settings_list .unblock_list').css('display', 'block');

            socket.emit('blocked-users');
        });

        $('unblock-user-btn').on('click', function () {
            var userId = $('blocked-users-list').getSelected()[0].get('value');
            if (userId) {
                $('blocked-users-list').getSelected().destroy();
                selfUser.settings.blockedUsers.splice(selfUser.settings.blockedUsers.indexOf(userId), 1);
                socket.emit('unblock-user', {
                    userId: userId
                });
            }
        });
    }

    showUserTabSettingsList = function (id) {
        $('#active_chat_' + id).find('.settings_button').addClass('selected');
        $('#active_chat_' + id).find('.settings_list').css('display', 'block');
    }

    closeUserTab = function (id) {
        $('#user_tab_' + id).remove();
        $('#active_chat_' + id).remove();
        // resizeTabBar('remove');
        socket.emit('dialog-closed', {
            userId: id
        });

        delete openDialogsUsers[id];
        delete skippedDialogs[id];

        shiftOpenTabs();
    }

    blockUser = function (id) {
        closeUserTab(id);
        socket.emit('block-user', {
            userId: id
        });

        selfUser.settings.blockedUsers.push(id);
        removeUser(id);
    }

    initAudio = function () {
        if ("Audio" in window && browser.indexOf('safari') == -1) {
            audio = new Audio();
            if (!!(audio.canPlayType && audio.canPlayType('audio/ogg; codecs="vorbis"').replace(/no/, '')))
                audio.ext = 'ogg';
            else if (!!(audio.canPlayType && audio.canPlayType('audio/mpeg;').replace(/no/, '')))
                audio.ext = 'mp3';
            else if (!!(audio.canPlayType && audio.canPlayType('audio/mp4; codecs="mp4audio.40.2"').replace(/no/, '')))
                audio.ext = 'm4a';
            else
                audio.ext = 'mp3';

            return;
        }

    }

    playAudio = function (name) {
        if (audio && audio.play) {
            audio.src = "/sounds/" + name + '.' + audio.ext;
            audio.play()
        }
    }

    function visibilityEscortsList(active) {
        if (active) {
            $('.chat-info-online-escorts').css('display', 'block');
            $('#chat-switcher').prop('checked', true);
        } else {
            $('#chat-switcher').prop('checked', false);
        }

    }

    this.connect();
    this.initListeners();
    this.initEvents();
    initAudio();
}