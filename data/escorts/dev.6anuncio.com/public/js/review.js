$( document ).ready(function() {
//Agency Profile Escort Slider
var loadedPhotosCount = 0;
var photosCount = $('.review-template-slider img').length;
var ln = location.pathname.split('/')[1];
if(ln !== 'en'){
    ln = 'pt'
}
$('.review-template-slider img').on('load', function() {
  loadedPhotosCount++;
  if (loadedPhotosCount == photosCount) {
    $('.review-template-slider img').show('slow');
    initreviewEscortSlider();
    
  }
}).each(function() {
  if (this.complete) {
    $(this).trigger('load');
  }
});



var loadedPhotosCount = 0;
  var photosCount = $('.pImg').length;
 
  $('.pImg').on('load', function() {
    loadedPhotosCount++;
    if (loadedPhotosCount == photosCount) {
      initreviewEscortSlider();
      
    }
  }).each(function() {
    if (this.complete) {
      $(this).trigger('load');
    }
  });



function sliderFullscreenOpen(arrow, element) {
  arrow.addClass('fullscreen-out');
    $('.pSlider').addClass('fullscreen-slider');   
    $('html,body').css({'overflow': 'hidden'});
    $('body').css({'touch-action':'auto'})
}

function sliderFullscreenClose(arrow) {
  arrow.removeClass('fullscreen-out');

  $('.pSlider').removeClass('fullscreen-slider');
  $('html,body').removeAttr('style');
}

function sliderNavigate(slider) {
  var index = slider.slick('slickCurrentSlide');
  slider.slick('slickGoTo', index, true);
}

//review template ad slider
function initreviewEscortSlider() {

   var slider = $('.pSlider').slick({
    speed: 300,
    slidesToShow: 2,
    infinite: true,
    variableWidth: true,
    slidesToScroll: 1,
    responsive: [
    {
      breakpoint: 576,
      settings: {
        slidesToShow: 1,
        infinite: true,
        variableWidth: false,
        slidesToScroll: 1
      }
    },
  ]
 });

  $('.slick-slide').click(function() {
     
   
    var index = $(this).data("slick-index");
    var current = slider.slick('slickCurrentSlide');
    var last = slider.slideCount - 1;

    if( current === 0 && index === last ) {
       slider.slick('slickGoTo', -1);
    } else if ( current === last && index === 0 ) {
       slider.slick('slickGoTo', last + 1);
    } else {
       slider.slick('slickGoTo', index);
    }

});

  $('.slick-slide').on('mousedown', function(edown) {
    if (edown.button == 0 && 
      !$('.fullscreenToggle').hasClass('fullscreen-out')) {

      var handle;

      $('.slick-slide').on('mouseup', handle = function(eup) {
        $('.slick-slide').off('mouseup', handle);

        if (Math.abs(edown.screenX - eup.screenX) < 10 && 
          Math.abs(edown.screenY - eup.screenY) < 10) {
            sliderFullscreenOpen($('.fullscreenToggle'), $(".slick-current"));
            sliderNavigate(slider);
        }
      });
    }
  });

  $(window).on('keydown', function(e) {
    if (e.keyCode == 37) {
      slider.slick('slickPrev');
    }

    if (e.keyCode == 39) {
      slider.slick('slickNext');
    }

    if (e.keyCode == 27 && $('.fullscreenToggle').hasClass('fullscreen-out')) {
      sliderFullscreenClose($('.fullscreenToggle'));
      sliderNavigate(slider);
    }
  });

  $(window).on('scroll' , function () {
     if(window.pageYOffset > 300){
         $('.back_to_top').removeClass('d-none').delay(1000);
     }else{
         $('.back_to_top').addClass('d-none').delay(1000);
     }
  });

    $('.pSlider').bind('DOMMouseScroll', function(e){
      console.log('test');
       if(e.originalEvent.detail > 0) {
          slider.slick('slickNext');
       }else {
          slider.slick('slickPrev');
       }
     
       return false;
   });

   //IE, Opera, Safari
   $('.pSlider').bind('mousewheel', function(e){
       if(e.originalEvent.wheelDelta < 0) {
          slider.slick('slickNext');
       }else {
          slider.slick('slickPrev');
       }
      
       return false;
   });


  $('.fullscreenToggle').click(function() {
   if (!$('.fullscreenToggle').hasClass('fullscreen-out')) {
      sliderFullscreenOpen($('.fullscreenToggle'), $(".slick-current"));
      // sliderNavigate(slider);
      if($(this).data('slidePos')){
         slider.slick('slickGoTo', $(this).data('slidePos')-1);
      }
   } else {
      sliderFullscreenClose($('.fullscreenToggle'), $(".slick-current"));
       sliderNavigate(slider);
   }
  });
}
//slider end  

var cityfilter = {
    init: function(){
    $( "[name='country_id']" ).on('change', function(){
       $(".country-city").LoadingOverlay("show", {color : "rgba(239, 243, 249, 0.80)", zIndex: 1000   });
      
       $.ajax({
          type: "POST",
          url: '/'+ln+"/geography/ajax-get-cities?country_id=" + $(this).val(),
         
          success: function(response, status, xhr){
           try {
            response = JSON.parse(response);
           
            if(typeof response =='object'  ){
               $( "[name='city_id']" ).children('option').remove();
               $( "[name='city_id']" ).append('<option value=""> --- </option>');
               $.each( response['data'] , function( key, value ){
                
               $( "[name='city_id']" ).append('<option value="' + value.id + '">' + value.title +'</option>');
             });
            }
           }catch (e) {
            
           }finally{
            $(".country-city").LoadingOverlay("hide", true);
           }
       }
     });
        
    });
  },
};

var loadReviews = {
    dataArea : '',
    init: function(){
        this.dataArea = $('#reviews_by_country');
        var self = this;

        if(!self.dataArea) return false;

        var city_id = $('#data_city_id').val();
        var without_escort = $('#escort_id').val();

        $.ajax({
           url:'/'+ln+'/reviews',
           type:'get',
           beforeSend:function () {
               self.dataArea.LoadingOverlay('show', true);
           },
           data:{
               ajax:true,
               fromEscortReview:true,
               perPage:5,
               r_city:city_id,
               without_escort:without_escort
           },
           success :function (resp) {
               self.dataArea.html(resp);
               self.dataArea.LoadingOverlay('hide', true);
           }
        });
    }
};

loadReviews.init();
cityfilter.init();
var language = '';
switch (ln) {
    case 'en':
        language = "en-GB";
        break;
    case 'pt':
        language = 'pt-PT';
        break;
    default:
        language = 'pt-PT';
        break;
}
$('#m_date, #t_meeting_date').datepicker({
    language: language,
    endDate: new Date(),
    todayHighlight: true,
    autoclose: true,
    format: 'dd M yyyy',
 });

});
