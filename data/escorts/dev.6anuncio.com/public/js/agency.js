window.onload = function() {
        //Agency Profile Escort Slider
        var loadedPhotosCount = 0;
        var photosCount = $('.agnecy-escorts-carousel img').length;
       
        $('.agnecy-escorts-carousel img').on('load', function() {
          loadedPhotosCount++;
          if (loadedPhotosCount == photosCount) {
            $('.agnecy-escorts-carousel img').css('visibility','visible')
            initAgencyProfileSlider();
            
          }
        }).each(function() {
          if (this.complete) {
            $(this).trigger('load');
          }
        });

        function initAgencyProfileSlider(){
		
          $('.agnecy-escorts-carousel').slick({
              infinite: true,
              dots: true,
              slidesToShow: 4,
              slidesToScroll: 4,
              responsive: [
              {
                breakpoint: 768,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2,
                  infinite: true,
                  dots: true
                }
              },
              {
                breakpoint: 576,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2
                }
              },
              {
                breakpoint: 480,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2
                }
              }
           
            ]
          
          });
        }
          
        //Agency Profile Escort Slider END

        $('.agency_write_me').on('click',function () {
            $('#write_email_to_agency').modal();
        });

    $('#write_email_to_agency_form').submit(function (event) {
        event.preventDefault();

        var self = $(this);

        var data = self.serializeArray();

        data.push({name: 'agency_id', value: $(document.activeElement).attr('data-id')});

        $.ajax({
            url: '/feedback',
            type: 'POST',
            data: data,
            dataType: 'json',
            success: function (resp) {
                if (resp.status == 'error') {

                    $('.has-danger').removeClass('has-danger');
                    $('.error-report').html(' ');

                    $.each(resp['msgs'], function (key, value) {
                        $('#' + key + '-feedback-error').html(value).parent().addClass('has-danger');
                    });
                    return false;
                }

                $('.has-danger').removeClass('has-danger');
                $('.error-report').html(' ');

                self.closest('.modal').modal('hide');
                Notify.alert('success', 'Your email was sent successfully!');
            }
        });
    });
 
  }

