$( document ).ready(function() {
        //classified  ad slider
        var loadedPhotosCount = 0;
        var photosCount = $('.classified-ad-slider img').length;
       
        $('.classified-ad-slider img').on('load', function() {
          loadedPhotosCount++;
          if (loadedPhotosCount == photosCount) {
            
            $('.classified-ad-slider ').css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1}, 1000);
            classifiedAdSlider();
          }
        }).each(function() {
          if (this.complete) {
            $(this).trigger('load');
          }
        });

        function classifiedAdSlider() {
         
           $('.classified-ad-slider').slick({
            infinite: false,
            slidesToShow: 3,
            slidesToScroll: 2,
            centerPadding: '50px',
             responsive: [
              {
                breakpoint: 576,
                settings: {
                   centerPadding: '0px',
                  infinite: true,
                  centerMode: true,
                  slidesToShow: 1,
                  slidesToScroll: 1,
                
                  
                }
              },
            ]
         
         });
        }
        // classified ad slider end
        $('input[name="email"]').blur(function () {
            if(this.value.length == 0){
                $('input[name="phone"]').attr('required','required');
            }else{
                $('input[name="phone"]').removeAttr('required');
            }
        });

  /* profile slider */
  var loadedPhotosCount2 = 0;
  var photosCount2 = $('.pImg').length;
 
  $('.pImg').on('load', function() {
    loadedPhotosCount2++;
    if (loadedPhotosCount2 == photosCount2) {
      initProfileSlider();
      
    }
  }).each(function() {
    if (this.complete) {
      $(this).trigger('load');
    }
  });




function sliderFullscreenOpen(arrow, element) {
  arrow.addClass('fullscreen-out');
    $('.pSlider').addClass('fullscreen-slider');   
    $('html,body').css({'overflow': 'hidden'});
    $('body').css({'touch-action':'auto'})
}

function sliderFullscreenClose(arrow) {
  arrow.removeClass('fullscreen-out');

  $('.pSlider').removeClass('fullscreen-slider');
  $('html,body').removeAttr('style');
}

function sliderNavigate(slider) {
  var index = slider.slick('slickCurrentSlide');
  slider.slick('slickGoTo', index, true);
}


function initProfileSlider() {

  var slider = $('.pSlider').slick({
    speed: 300,
    slidesToShow: 2,
    infinite: true,
    variableWidth: true,
    slidesToScroll: 1,
    responsive: [
    {
      breakpoint: 576,
      settings: {
        slidesToShow: 1,
        infinite: true,
        variableWidth: false,
        slidesToScroll: 1
      }
    },
  ]
 });

  $('.slick-slide').click(function() {
     
   
    var index = $(this).data("slick-index");
    var current = slider.slick('slickCurrentSlide');
    var last = slider.slideCount - 1;

    if( current === 0 && index === last ) {
       slider.slick('slickGoTo', -1);
    } else if ( current === last && index === 0 ) {
       slider.slick('slickGoTo', last + 1);
    } else {
       slider.slick('slickGoTo', index);
    }

});

  $('.slick-slide').on('mousedown', function(edown) {
    if (edown.button == 0 && 
      !$('.fullscreenToggle').hasClass('fullscreen-out')) {

      var handle;

      $('.slick-slide').on('mouseup', handle = function(eup) {
        $('.slick-slide').off('mouseup', handle);

        if (Math.abs(edown.screenX - eup.screenX) < 10 && 
          Math.abs(edown.screenY - eup.screenY) < 10) {
            sliderFullscreenOpen($('.fullscreenToggle'), $(".slick-current"));
            sliderNavigate(slider);
        }
      });
    }
  });

  $(window).on('keydown', function(e) {
    if (e.keyCode == 37) {
      slider.slick('slickPrev');
    }

    if (e.keyCode == 39) {
      slider.slick('slickNext');
    }

    if (e.keyCode == 27 && $('.fullscreenToggle').hasClass('fullscreen-out')) {
      sliderFullscreenClose($('.fullscreenToggle'));
      sliderNavigate(slider);
    }
  });

  $(window).on('scroll' , function () {
     if(window.pageYOffset > 300){
         $('.back_to_top').removeClass('d-none').delay(1000);
     }else{
         $('.back_to_top').addClass('d-none').delay(1000);
     }
  });

    $('#container-slider').bind('DOMMouseScroll', function(e){
      console.log('test');
       if(e.originalEvent.detail > 0) {
          slider.slick('slickNext');
       }else {
          slider.slick('slickPrev');
       }
     
       return false;
   });

   //IE, Opera, Safari
   $('#container-slider').bind('mousewheel', function(e){
       if(e.originalEvent.wheelDelta < 0) {
          slider.slick('slickNext');
       }else {
          slider.slick('slickPrev');
       }
      
       return false;
   });


  $('.fullscreenToggle').click(function() {
   if (!$('.fullscreenToggle').hasClass('fullscreen-out')) {
      sliderFullscreenOpen($('.fullscreenToggle'), $(".slick-current"));
      // sliderNavigate(slider);
      if($(this).data('slidePos')){
         slider.slick('slickGoTo', $(this).data('slidePos')-1);
      }
   } else {
      sliderFullscreenClose($('.fullscreenToggle'), $(".slick-current"));
       sliderNavigate(slider);
   }
  });
}

});

$('.write_to_ad').on('click',function () {
    $('#write_email_to_ad').modal();
});

$('#write_email_to_ad').on('submit',function (event) {
    event.preventDefault();

    var data = $('form#write_email_to_ad').serializeArray();

    data.push({name: 'ad', value: $(document.activeElement).attr('data-id')});

    $.ajax({
        url: '/feedback',
        type: 'POST',
        data: data,
        dataType: 'json',
        success: function (resp) {
            if (resp.status === 'error') {
                if(resp.msgs.limit === 'riched'){
                    $('#write_email_to_ad').modal('hide');
                    Notify.alert('danger', resp.msg);
                    return false;
                }

                $('.has-danger').removeClass('has-danger');
                $('.error-report').html(' ');

                $.each(resp['msgs'], function (key, value) {
                    $('#' + key + '-feedback-error').html(value).parent().addClass('has-danger');
                });
                return false;
            }

            $('.has-danger').removeClass('has-danger');
            $('.error-report').html(' ');

            $('#write_email_to_ad').modal('hide');
            Notify.alert('success', 'Your email was sent successfully!');
        }
    });
});