$( document ).ready(function() {
  $('.btn-buy button').on( 'click', function(){
  	var price = $(this).data('price');
  	$('#govazd').LoadingOverlay("show");
  
  	$.ajax({
  	  method: "POST",
	  url: "/govazd-mmg",
	  data: {price: price},
	   success: function(response, status, xhr){
           try {
            response = JSON.parse(response);
           
            if(typeof response =='object'  ){
             if(response.status == 'success'){
             	window.location.href = response.url;
             }
            }
           }catch (e) {
            
           }finally{
            $('#govazd').LoadingOverlay('hide');
           }
       }

	});
   
  })
});