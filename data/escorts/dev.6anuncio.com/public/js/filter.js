var Sceon = Sceon || {};
Sceon.firstLoad = true;

/* LOCATION */

Sceon.Location = (function(global, S, $) {

	'use strict';

	var 
		_primitiveParams = [
			'page', 'sort', 'reg', 'name', 'price_from', 'price_to',
			'age_from', 'age_to', 'f_incall', 'f_outcall',
			'video',  'review', 'is_online_now', 'verified', 'verified_contact', 'f_agency' , 'f_independent'
		],
		_sep = ';', _eq = '=', _enum = ',', _hashEvt = 'hashChanged';


	/**
	 * Sets the window location hash
	 * @param String hash
	 * @return Boolean result
	 */
	function setHash(hash) {

		if (hash && hash.length) {
			document.location.hash = hash;
			return true;
		}else if (hash === '') {
            history.pushState("", document.title, window.location.pathname + window.location.search);
        }

		return false;

	}

	function replaceHash(key, val) {
		var hash = document.location.hash;

		if (!hash) {
			document.location.hash = key + _eq + val;
			return true;
		}

		var params = hash.replace('#', '').split(_sep);
	}

	/**
	 * Builds query string from hash
	 * @return String query string
	 */
	function getQuery() {

		var hash = document.location.hash.substring(1);
	
		if (!hash.length) {
			return {};
		}

		var 
			params = hash.split(_sep),
			filter = '';

		$.each(params, function(i, param) {

			var 
				pair = param.split(_eq),
				key = pair[0],
				val = pair[1];

			if (typeof val === 'undefined') return false;

			$.each(val.split(','), function(i, el) {
				filter += key + ($.inArray(key, _primitiveParams) ? '[]=' : '=') + el + '&';
			});
		});
		
		return filter;

	}

	/**
	 * Building hash from params and selects
	 * @return String hash text
	 */
	function buildHash(filters) {
		var 
			hash = '',
			map = {};

		$.each(filters, function(i, el) {
			var 
				key = i.replace('[]', '');

			if (typeof map[key] === 'undefined')
				map[key] = [];

			map[key].push(el);

		});

		for (var i in map) {
			hash += i + _eq + map[i].join(_enum) + _sep;
		}
		return hash;

	}

	function getHash() {
		return global.location.hash.replace('#', '');
	}

	function clearHash() {

		document.location.hash = '';

	}

	return {
		getHash: getHash,
		setHash: setHash,
		getQuery: getQuery,
		buildHash: buildHash
	};

})(window, Sceon, jQuery);


/* FILTER */

Sceon.Filter = (function(global, S, $) {
	var 
		_params = S.pageParams,
		_hashEvt = 'hashChanged',
		ln = location.pathname.split('/')[1];
	if(ln !== 'en'){
		ln = 'pt'
	}
	/* Cities filter */

	function CitiesFilter($el, $dest) {
		return this.init($el, $dest);
	}

	CitiesFilter.prototype = {
		options: {
			showCount: 5
		},

		init: function($el, $dest) {
			this.$el = $el;
			this.$dest = $dest;
			this.cities = _params.cities;
			this.bindEvents();
		},

		bindEvents: function() {
			var _self = this;

			this.$el.on('keyup', function(e) {
				var val = $(this).val().toLowerCase();

				// if (val.length < 1) {
				// 	val = '';
				// 	_self.$dest.hide();
				// 	return false;
				// }

				_self.filtered = [];

				$.each(_self.cities, function(i, city) {
					var 
						title = city.city_title,
						startPos = title.toLowerCase().indexOf(val),
						endPos = startPos + val.length + 1;


					if (~startPos) {
						var highlighted = _self.highlightMatch(title, startPos, endPos);

						_self.filtered.push({
							slug: city.city_slug,
							title: highlighted,
							count: city.escort_count
						});
					}
				});

				_self
					.decorateOptions()
					.displayMatched();
			});

			
		},

		decorateOptions: function() {
			// if (!this.filtered || !this.filtered.length) return this;

			var 
				_self = this,
				part = this.filtered.slice(0, this.options.showCount);

			_self.rowHtml = '';


			$.each(part, function(i, city) {
				_self.rowHtml += [
				'<a class="d-flex justify-content-start city-info" href="/escorts/city_gb_'+ city.slug + '" >',
					'<div class="p-1"><i class="ion-chevron-right"></i></div>',
					'<div class="p-1 city-name">' + city.title + '</div>',
					'<div class="p-1 city-escorts-count ml-auto">' + city.count + '</div>',
				'</a>'
				].join('');

			});

			if(!this.filtered || !this.filtered.length){
                _self.rowHtml = '<div class="text-center py-2">No city found !</div>';
			}

			return this;
		},

		highlightMatch: function(str, startPos, endPos) {
			var splitted = str.split('');

			splitted.splice(startPos, 0, '<strong>');
			splitted.splice(endPos, 0, '</strong>');

			return splitted.join('');
		},

		displayMatched: function() {
			this.$dest.html(this.rowHtml.toString());
			this.$dest.show('slow');
		}
	};

	/* Escorts filter */

	function EscortsFilter($filterCont, props) {
		return this.init($filterCont, props);
	}

	EscortsFilter.prototype = {
		props: {},
		renderedAdvancedFilter:false,

		primitiveParams: [
			'page', 'sort', 'reg', 'name', 'price_from', 'price_to',
			'age_from', 'age_to', 'f_incall', 'f_outcall',
			'video',  'review', 'is_online_now', 'verified', 'verified_contact' ,'video','sort' ,'member_name','showname',
			'r_city','text','category','city','agency_sort','city_slug','city_id','f_gender'
		],

		escortsFilterUrl: '/escorts?ajax',

		controls: ['input','select[data-filter="true"]'],
		actionBtns: ['button.btn-primary'],
		resetBtns: ['button.btn-secondary'],

		tempFilter: {},

		init: function($filterCont, props) {
			//if data-object=false we are creating new object for advanced filter
			$('.advanced-search-button').attr("data-object","true");

			this.container = $filterCont;
			

			this
				.setProps(props)
				.parseHash()
				.loadData()
				.bindEvents();
		},

		setProps: function (props) {
			this.props = $.extend(this.props, props);
			return this;
		},

		parseHash: function() {
			var 
				_self = this,
				filter = {},
				hash = Sceon.Location.getHash(),
				splittedHash = hash.split(';').filter(function(el) {
					return el !== '';
				});

				$.each(splittedHash, function(i, el) {
					var 
						pair = el.split('='),
						key = !~$.inArray(pair[0], _self.primitiveParams) ? (pair[0] + '[]') : pair[0],
						val = !~$.inArray(pair[0], _self.primitiveParams) ? (pair[1].split(',')) : pair[1];
					
					filter[key] = val;
				});
			
			this.tempFilter = filter;

			return this;
		},

		loadData: function() {
            if (Object.keys(this.tempFilter).length > 0) this.renderTempFilter();

            if(this.props.htmlArea){
                if((Sceon.firstLoad && (location.pathname === '/' || location.pathname === '/en')) || location.hash !== ""){
                    this.sendRequest();
                }
            }
			if(this.props.page !== 'main'){
                $('#collapsesearch').removeClass('filters').find('.advanced-search-button').attr('data-redirect','true');
			}

			
			return this;
		},

		setFilterParam: function(pair) {
			if (this.tempFilter[pair.name]) {
				if (!~$.inArray(pair.name, this.primitiveParams)) {
					this.tempFilter[pair.name].push(pair.value);
				} else {
					this.tempFilter[pair.name] = pair.value;

					if (this.tempFilter[pair.name] === '') {
						delete this.tempFilter[pair.name];
					}
				}
			} else {
				if (!~$.inArray(pair.name, this.primitiveParams)) {
					this.tempFilter[pair.name] = [pair.value];
				} else {
					this.tempFilter[pair.name] = pair.value;
				}
			}

			return this;
		},

		removeFilterParam: function(pair) {
			if (!~$.inArray(pair.name, this.primitiveParams)) {
				this.tempFilter[pair.name] = $.grep(this.tempFilter[pair.name], function(el) {
					return el != pair.value;
				});

				if (!this.tempFilter[pair.name].length) {
					delete this.tempFilter[pair.name];
				}
			} else {
				delete this.tempFilter[pair.name];
			}

			return this;
		},

		renderTempFilter: function($control) {
			var 
				_self = this,
				$controls = this.container.find(this.controls.join(','));
			

			$.each($controls, function() {
				var $control = $(this);

				if ($control.is(':checkbox') || $control.is(':radio')) {
					$control.prop('checked', false);
				} else if($control.is('select')){

				} else {
					$control.val('');
				}
			})

			$.each(this.tempFilter, function(name, values) {
				if (!~$.inArray(name, _self.primitiveParams)) {
					$.each(values, function(i, val) {

						var $el = _self.container.find('[name="' + name + '"][value="' + val + '"]');
						if ($el !== $control) $el.prop('checked', true);
					})
				} else {

					var $el = _self.container.find('[name="' + name + '"]');

					if ($el.attr('type') === 'text' || $el.attr('type') === 'number') {
						$el.val(values);
					} else if($el.is('select')) {
						$el.find('option').each(function () {
							if($(this).val() === values){
                                $(this).prop('selected',true);
							}
                        });
					}else{
                        $el.prop('checked', true);
					}
				}
			});

			return this;
		},

		bindEvents: function() {
			
			var _self = this;
			this.container.on('change', this.controls.join(','), function(e) {
				e.preventDefault();
				
				var 
					$control = $(this),
					pair = {
						name: $control.attr('name'), 
						value: $control.val()
					};

				if ($control.attr('type') === 'checkbox' && !$control.is(':checked')) {
					_self.removeFilterParam(pair);
				} else {
					_self.setFilterParam(pair);
				}

				//Delete page param for set it 1
                delete _self.tempFilter['page'];

				if($control.attr('data-scenario') === 'trigger'){
					$('#do_search').click();
				}

				_self.renderTempFilter($control);
			});

			this.container.on('click', this.actionBtns.join(','), function() {
				var 
					$btn = $(this),
					hash = Sceon.Location.buildHash(_self.tempFilter);

				Sceon.Location.setHash(hash);

				if($btn.attr('data-redirect')){
					window.location.href = '/'+location.hash;
				}else{
                    _self.sendRequest();
                }

			});

			this.container.on('click', this.resetBtns.join(','), function() {
				var $btn = $(this);
				
				if ($btn.data('role') && $btn.data('role') === 'reset') {

					var 
						$parentModal = $btn.closest('.modal-content'),
						$wrapper = $parentModal.length ? $parentModal : this.$container,
						$controls = $wrapper.find( _self.controls.join(',') );
						
					var $controls_obj = [];

					$.each($controls, function() {
						var $control = $(this);
						
						switch( $control.attr('type') || $control.prop("tagName").toLowerCase() ) {
							case 'text':
								$control.val('');
								break;
							case 'checkbox':
								$control.prop( 'checked', false );
								break;
							case 'select':
								$control.prop('selectedIndex', 0);
								break;
							case 'radio':
								$control.prop('checked', false);
							
								break;
						}
						
						$controls_obj.push( $control );						
					});
					_self.tempFilter = {};
				}

				if($btn.data('role') && $btn.data('role') === 'reset-all'){
					$controls = _self.container.find( _self.controls.join(',') );
					$.each($controls, function() {
						var $control = $(this);
						
						switch( $control.attr('type') || $control.prop("tagName").toLowerCase() ) {
							case 'text':
								$control.val('');
								break;
							case 'checkbox':
								$control.prop( 'checked', false );
								break;
							case 'select':
								$control.prop('selectedIndex', 0);
								break;
							case 'radio':
								$control.prop('checked', false);
							
								break;
						}
						
											
					});
					_self.tempFilter = {};
				}
				
			});
			
			return this;
		},

		sendRequest: function() {


			var _self = this;
			var url = (ln === 'en')  ? '/'+ln+this.props.filterUrl : this.props.filterUrl;
			$.ajax({
				url: url,
				method: 'GET',
				dataType: 'json',
				data: this.tempFilter,

				beforeSend: function() {
					if(!Sceon.firstLoad){
						var width = $( window ).width();

						if(!(width < 992 && _self.props.filterUrl.includes('escorts?ajax'))) {
							_self.props.htmlArea.LoadingOverlay("show", { color : "rgba(149, 149, 149, 0.90)", zIndex: 1000});
						} 
                    }
				},
				success: function(resp) {
					
					if (!resp) {
						console.warn('Error occured.');
						return false;
					}


					var $escortList = resp.escort_list;
					if(_self.props.page === 'main'){
                        var $filterBody = resp.filter_body;
                        $('#filter-container').html($filterBody).hide().fadeIn(1000);
                    }else{
                        _self.getAdvancedPage();
					}

					if(_self.props.htmlArea){
						// if((Sceon.firstLoad && (location.pathname === '/' || location.pathname === '/en')) || location.hash !== ""){
							var width = $( window ).width();
							var checkPagination = false;

							if(Object.keys(_self.tempFilter).length == 1) {
								if(Object.keys(_self.tempFilter)[0] == "page") {
									checkPagination = true;
								}
							} else {
								$.each(_self.tempFilter, function(key, val) {
									if(key == "page" && val != 1) {
										checkPagination = true;
									} 
								});
							}

							$('.scroll-page').LoadingOverlay("hide", true);

							if(width < 992 && _self.props.filterUrl.includes('escorts?ajax') && checkPagination) {
								$('#escorts-sceleton').hide();

								if(_self.props.htmlArea.find("#escorts-list").length == 0) {
									_self.props.htmlArea.append($escortList);
								} else {
									_self.props.htmlArea.find("#escorts-list").append($escortList);
									$el = _self.props.htmlArea.find(".escorts-list").slice(1);
									$el.replaceWith($el.html());
								}
								
								_self.props.htmlArea.find(".no-escort").hide();
							} else {

								_self.props.htmlArea.html('').append($escortList);

							}
							
                        // }
					}

                    $('#collapsesearch').collapse('hide');

				},

				error: function(resp) {
                    _self.props.htmlArea.LoadingOverlay("hide", {color : "rgba(239, 243, 249, 0.80)", zIndex: 1000});
				}
			}).done(function () {
                if(!Sceon.firstLoad && _self.props.htmlArea) {
                    _self.props.htmlArea.LoadingOverlay("hide");

                }
                Sceon.firstLoad = false;


				$(".bubble-texts").html("");
				$(".bubble-text-count").html(0);
			});
		},

		getAdvancedPage :function () {
			if(this.renderedAdvancedFilter) return false;

			var _self = this;
			var url = (ln === 'en') ? '/'+ln+'/escorts/get-filter?ajax=true&filter_params=true' : '/escorts/get-filter?ajax=true&filter_params=true';
			$.ajax({
				url:url,
				type:'POST',
				async:false,
				success:function (resp) {
					$('#collapsesearch').removeClass('filters').find('.advanced-search-button').attr('data-redirect','true');
                    $('#filter-container').html(resp).hide().fadeIn(1000);
                    _self.renderedAdvancedFilter = true;
                }
			});
        }
	};

	/* Pagination filter */

	function PaginationFilter(E) {
		return this.init(E);
	}

	PaginationFilter.prototype = {
		Escorts : {},
		Page : 1,

		init: function(E) {

        	this.Escorts = E;
			this.bindEvents();
		},

		bindEvents: function() {
			var _self = this;
			var width = $( window ).width();
			var tempPage = _self.Escorts.tempFilter.page;

			if(width < 992 && _self.Escorts.props.filterUrl.includes('escorts?ajax')) {
				$(document).on('appear', '.scroll-page', function(e, $affected) {
					if(tempPage != undefined) {
						var page = parseInt(tempPage) + 1;
						tempPage = undefined;
					} else {
						var page = parseInt( $('.scroll-page').attr('id'));
					}

					e.preventDefault();
					$('.scroll-page').LoadingOverlay("show", {color : "", zIndex: 1000   });
					$('.scroll-page').attr('id', page + 1 );
					_self.Page = page;
					var hash_link = S.Location.getHash();

	                var hash_obj = {};

	                var hash = hash_link.split(';').filter(function(el) {
	                    return el !== '';
	                });

	                $.each(hash, function(i, el) {
	                    var
	                        pair = el.split('='),
	                        key = !~$.inArray(pair[0], _self.Escorts.primitiveParams) ? (pair[0] + '[]') : pair[0],
	                        val = !~$.inArray(pair[0], _self.Escorts.primitiveParams) ? (pair[1].split(',')) : pair[1];

	                    hash_obj[key] = val;
	                });

	                var tmp_hash = '';
	                var obj_length = Object.keys(hash_obj).length;

	                if ( !obj_length ) {

	                	tmp_hash = 'page=' + _self.Page + ';';

	                } else if (('page' in hash_obj) && obj_length > 1) {

	                	tmp_hash = window.location.hash.replace(/\;page(.*?)=(.*?)\;/g, ';page='+ _self.Page+';');

	                } else if (('page' in hash_obj) && obj_length === 1) {

	                	tmp_hash = 'page=' + _self.Page + ';';

	                } else {

	                    tmp_hash = hash_link + 'page=' + _self.Page + ';';

	                }

	                S.Location.setHash(tmp_hash);

	                hash_obj['page'] = _self.Page;

	                _self.Escorts.tempFilter = hash_obj;

	                _self.Escorts.loadData();


	            });
			} else {
				$(document).on('click','.pagination a',function (e) {
					e.preventDefault();
					$(".bubble-page").remove();
					_self.Page = $(this).attr('data-page');
					var hash_link = S.Location.getHash();

	                var hash_obj = {};

	                var hash = hash_link.split(';').filter(function(el) {
	                    return el !== '';
	                });

	                $.each(hash, function(i, el) {
	                    var
	                        pair = el.split('='),
	                        key = !~$.inArray(pair[0], _self.Escorts.primitiveParams) ? (pair[0] + '[]') : pair[0],
	                        val = !~$.inArray(pair[0], _self.Escorts.primitiveParams) ? (pair[1].split(',')) : pair[1];

	                    hash_obj[key] = val;
	                });

	                var tmp_hash = '';
	                var obj_length = Object.keys(hash_obj).length;

	                if ( !obj_length ) {

	                	tmp_hash = 'page=' + _self.Page + ';';

	                } else if (('page' in hash_obj) && obj_length > 1) {

	                	tmp_hash = window.location.hash.replace(/\;page(.*?)=(.*?)\;/g, ';page='+ _self.Page+';');

	                } else if (('page' in hash_obj) && obj_length === 1) {

	                	tmp_hash = 'page=' + _self.Page + ';';

	                } else {

	                    tmp_hash = hash_link + 'page=' + _self.Page + ';';

	                }

	                S.Location.setHash(tmp_hash);

	                hash_obj['page'] = _self.Page;

	                _self.Escorts.tempFilter = hash_obj;

	                _self.Escorts.loadData();

	                $('html,body').animate({scrollTop:0},1000);



	            });
			}
			
		}
	};

	return {
		Cities: CitiesFilter,
		Escorts: EscortsFilter,
		Pagination: PaginationFilter
	};
})(window, Sceon, jQuery);

var Cities = new Sceon.Filter.Cities($('#citySearch'), $('#top-citites, #top-cities'));
//var Escort = new Sceon.Filter.Escorts($('.filters'));


$(document).ready(function () {
    if(!$('button[data-object="false"]').length) return false;
    var Escort = new Sceon.Filter.Escorts($('.filters'), {
        filterUrl: "/escorts?ajax=true",
        page: 'another'
    });
});







// Cubix.Filter.Set = function (filter) {
		
// 	Cubix.LocationHash.Set(Cubix.LocationHash.Make(filter));

// 	return false;
// };

// /* --> HashController */
// Cubix.HashController = {
// 	_current: '',
	
// 	init: function () {
// 		setInterval('Cubix.HashController.check()', 100);
// 	},
	
// 	check: function () {
// 		var hash = document.location.hash.substring(1);
		
// 		if (hash != this._current) {
// 			this._current = hash;
			
					
// 			var data = Cubix.LocationHash.Parse();
// 			//Cubix.Filter.getForm(data);
// 			Cubix.Escorts.Load(hash, data/*, Cubix.HashController.Callback*/);
// 		}
// 	}
// };

// Cubix.HashController.Callback = function () {
// 	Cubix.Filter.Change(Cubix.LocationHash.Parse());
// 	Cubix.Filter.Set(Cubix.LocationHash.Parse(), true);
// }
/* <-- */