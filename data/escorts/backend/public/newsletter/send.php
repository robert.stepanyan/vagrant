<?php

require_once 'init.php';

/***********************************************************************************************************/

//get newsletter which will be send
$select = 'SELECT from_email, subject, body, application_id, groups, no_in_groups, members, send_time, id FROM newsletters WHERE status = 1 ORDER BY send_time ASC LIMIT 1';
$res = mysql_query($select);
$row = mysql_fetch_row($res);
$num = mysql_num_rows($res);

if (!$num)
	die('There are no any newsletters'); 

$from_email = $row[0];
$subject = $row[1];
$body = $row[2];
$application_id = $row[3];
$groups = $row[4];
$no_in_groups = $row[5];
$members = $row[6];
$send_time = $row[7];
$id = $row[8];

$body = preg_replace("#({EDITABLE_[\s\S]+?})#", '', $body);
$body = preg_replace("#({/EDITABLE_[\s\S]+?})#", '', $body);

$sql = 'SELECT backend_url AS url FROM applications WHERE id = ' . $application_id;
$res = mysql_query($sql);
$row = mysql_fetch_array($res);
$site_url = $row['url'];

$body = preg_replace_callback('/href="([^"]+?)"/i', "set_url", $body);

$email_info = array('subject' => $subject, 'body' => $body, 'from_email' => $from_email);

if (strtotime($send_time) > time())
	die('There are no any newsletters for sending now');

$sql = 'UPDATE newsletters SET status = 2 WHERE id = ' . $id;
mysql_query($sql);

//get subscribers list with current groups
if (strlen($groups) > 0)
{
	$sql = '
		SELECT s.email, s.name
		FROM newsletter_subscribers s
		LEFT JOIN newsletter_subscribers_groups s_g ON s.id = s_g.subscriber_id
		WHERE s.status = 1 AND s_g.group_id IN (' . $groups . ')
		GROUP BY s.email
	';
	$res = mysql_query($sql);

	$count = send($res, $email_info);
}
//

//get subscribers list without groups
if (intval($no_in_groups) == 1)
{
	$sql = '
		SELECT s.email, s.name
		FROM newsletter_subscribers s
		LEFT JOIN newsletter_subscribers_groups s_g ON s.id = s_g.subscriber_id
		WHERE s.status = 1 AND s_g.group_id IS NULL
		GROUP BY s.email
	';
	$res = mysql_query($sql);

	$count = send($res, $email_info);
}
//

//get members list
if (intval($no_in_groups) == 1)
{
	$sql = '
		SELECT email, username
		FROM users
		WHERE status = 1 AND user_type = \'member\' AND application_id = ' . $application_id . '
	';
	$res = mysql_query($sql);

	$count = send($res, $email_info);
}
//

$sql = 'UPDATE newsletters SET status = 3, emails_count = ' . $count . ' WHERE id = ' . $id;
mysql_query($sql);

die ('Newsletter sent');

/* functions */

function set_url($matches)
{
	global $id, $site_url;

	return 'href="' . $site_url . '/newsletter/newsletter.php?u=' . urlencode($matches[1]) . "&i=" . $id . '"';
}

function send($res, $email_info)
{
	if (mysql_num_rows($res) == 0)
		return ;

	$i = 0;

	while ($row = mysql_fetch_array($res))
	{
		$email = $row[0];
		$name = $row[1];

		if (strlen($name) == 0)
			$name = 'Member';

		$body = str_replace('{NAME}', $name, $email_info['body']);
		
		$mail = new Zend_Mail();

		$mail->addTo($email);
		$mail->setSubject($email_info['subject']);
		$mail->setFrom($email_info['from_email']);
		$mail->setBodyHtml($body);

		if ($mail->send())
			$i ++;
		//$i ++;
		/*if (Cubix_Email::send($email, $email_info['subject'], $body, true, $email_info['from_email']))
			$i ++;*/
	}

	return $i;
}