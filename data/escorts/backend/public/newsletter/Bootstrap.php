<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	public function run()
	{
		// Zend_Layout::startMvc();

		parent::run();
	}
	
	
	protected function _initAutoload()
	{
		$moduleLoader = new Zend_Application_Module_Autoloader(array(
			'namespace' => '',
			'basePath' => APPLICATION_PATH
		));

		$autoloader = Zend_Loader_Autoloader::getInstance();
		$autoloader->registerNamespace('Cubix_');

		return $moduleLoader;
	}
	
	protected function _initDatabase()
	{
		$this->bootstrap('db');
		$db = $this->getResource('db');
		$db->setFetchMode(Zend_Db::FETCH_OBJ);

		Zend_Registry::set('db', $db);
		Cubix_Model::setAdapter($db);
	}
			
	protected function _initConfig()
	{
		Zend_Registry::set('system_config', $this->getOption('system'));
		Zend_Registry::set('feedback_config', $this->getOption('feedback'));
		Zend_Registry::set('newsletter_config', $this->getOption('newsletter'));
	}
}
