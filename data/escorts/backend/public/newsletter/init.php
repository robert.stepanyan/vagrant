<?php

//session_start();
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../../library'),
    //get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

$config = $application->getOption('resources');
$db_config = $config['db']['params'];

require_once 'Zend/Mail.php';

if (!$link = mysql_connect($db_config['host'], $db_config['username'], $db_config['password']))
	die('Error With Mysql Connection.');

if (!mysql_select_db($db_config['dbname'], $link))
	die('Error With Selecting DB');