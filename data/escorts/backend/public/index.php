<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 'On');

if ( $_SERVER['HTTP_HOST'] == "backend.escortforum.net" || $_SERVER['HTTP_HOST'] == "backend.escortforum.pw" || $_SERVER['HTTP_HOST'] == "backend.escortforumit.ch" ) {
	header("HTTP/1.1 301 Moved Permanently");
	header('Location: http://backend.escortforumit.xxx' . $_SERVER['REQUEST_URI']);
	exit;
}

define('IN_BACKEND', true);

$app_env = 'production';
if ( preg_match('#.test$#', $_SERVER['SERVER_NAME']) ) {
	$app_env = 'development';
}
elseif ( preg_match('#dev.sceon.(int|am)$#', $_SERVER['SERVER_NAME']) ) {
	$app_env = 'staging';
}
define('APPLICATION_ENV', $app_env);

if ( isset($_POST['PHPSESSID']) ) session_id($_POST['PHPSESSID']);

if ( false !== strpos($_SERVER['SERVER_SOFTWARE'], 'lighttpd') ) {
	$_lighty_url = $base_url.$_SERVER['REQUEST_URI'];
	$_lighty_url = @parse_url($_lighty_url);

	$_SERVER['QUERY_STRING'] = @$_lighty_url['query'];

	parse_str($_lighty_url['query'], $_lighty_query);

	foreach ($_lighty_query as $key => $val) {
		$_GET[$key] = $_REQUEST[$key] = $val;
	}
}

require '../../library/Cubix/Debug.php';
require '../../library/Cubix/Debug/Benchmark.php';

if ( isset($_REQUEST['benchmark']) ) Cubix_Debug::setDebug(true);
Cubix_Debug_Benchmark::setLogType(Cubix_Debug_Benchmark::LOG_TYPE_DB);
Cubix_Debug_Benchmark::setApplication('backend');
Cubix_Debug_Benchmark::start($_SERVER['REQUEST_URI']);

//session_start();
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
/*defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));*/

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../../library'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

$config_file = 'config.xml';

if ( preg_match('#\.(([^.]+?)\.(co\.uk|com|xxx|ch|net|))((\.test))?$#', $_SERVER['SERVER_NAME'], $a) ) {
	array_shift($a);
	$hostname = reset($a);
	$config_file = $hostname . '.ini';
	define('HOSTNAME', $hostname);
	
}

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/' . $config_file
);

ob_start("ob_gzhandler");
$application->bootstrap()
            ->run();

Cubix_Debug_Benchmark::end($_SERVER['REQUEST_URI']);

if ( Cubix_Debug::isDebug() ) {
	Cubix_Debug_Benchmark::log();
}
