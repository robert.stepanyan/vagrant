var Cubix = {};

Cubix.Overlay = new Class({
	Implements: [Events, Options],
	
	lastOverflow: '',
	lastPosition: '',
	
	options: {
		classPrefix: 'cubix-ui-overlay-',
		showLoader: true,
		type: 'dark',
		zIndex: 30,
		offset: [0, 0, 0, 0],
		styles: {}
	},

	loader: null,

	initialize: function (el, options) {
		this.el = $(el);
		
		options = options || {};
		this.setOptions(options);
		
		var overlay = new Element('div', {
			'class': this._getClass() + ' ' + this._getClass('fit') + ' ' + this._getClass(this.options.type),
			styles: {
				'z-index': this.options.zIndex,
				display: 'none'
			}
		}).inject(this.el).setStyles(this.options.styles);
		
		if ( this.options.showLoader ) {
			this.loader = new Element('div', {
				'class': this._getClass('icon') +  ' ' + this._getClass('fit')
			}).inject(overlay);
		}
		
		this.overlay = overlay;
	},
	
	enable: function () {
		this.el.setStyles({
			overflow: this.lastOverflow,
			position: this.lastPosition
		});
		this.overlay.setStyle('display', 'none');
		/*this.overlay.get('tween', { property: 'opacity', duration: 'short', onComplete: function () {
			this.overlay.setStyle('display', 'none');
		}.bind(this) }).start('opacity', 0);*/
	},
	
	disable: function () {
		if ( this.overlay.getStyle('display') == 'block' ) return;
		
		this.overlay.setStyle('z-index', this.options.zIndex);
		
		this.lastOverflow = this.el.getStyle('overflow');
		this.lastPosition = this.el.getStyle('position');
		
		this.el.setStyle('position', 'relative');

		var offset = this.options.offset;
		this.overlay.setStyles({
			top: 0 + offset[0],
			width: this.el.getWidth() + offset[1],
			height: this.el.getScrollHeight() + offset[2],
			left: 0 + offset[3]
		});

		this.overlay.setStyle('display', 'block');

		this.fit();
		//this.overlay.get('tween', { property: 'opacity',  duration: 'short' }).start(1);
	},
	
	_getClass: function (cls) {
		if ( ! cls ) return this.options.classPrefix.substring(0, this.options.classPrefix.length - 1);
		return this.options.classPrefix + cls;
	},
	
	fit: function () {
		this.overlay.setStyle('height', this.el.getScrollHeight() + this.options.offset[2]);
	}
});

Cubix.Overlay.Box = new Class({
	Extends: Cubix.Overlay,

	initialize: function (el, options) {
		this.parent(el, options);

		if ( this.options.showLoader ) {
			this.loader.destroy();
			this.loader = new Element('div', {
				'class': this._getClass('box'),
				'html': 'Please wait...'
			}).inject(this.overlay);
		}
	},

	fit: function () {
		this.loader.setStyles({
			left: this.overlay.getWidth() / 2 - this.loader.getWidth() / 2,
			top: this.overlay.getHeight() / 2 - this.loader.getHeight() / 2
		});
	}
})

Cubix.alert = function (title, text) {
	Cubix._roar.alert(title, text);
}

window.addEvent('domready', function () {
	if ( $defined(Cubix.ready) ) {
		Cubix.ready();
	}

	Cubix._roar = new Roar({
		position: 'bottomRight'
	});

	/*var counter = 0;
	
	var checker = function () {
		counter++;
		
		if ( counter == 4 ) {
			if ( Cubix.ready ) {
				Cubix.ready.call();
			}
		}
	};
	
	new Asset.javascript('/js/cubix-shadow-v1.0.source.js', { onload: checker });
	new Asset.javascript('/js/cubix-grid-v2.0.source.js', { onload: checker });
	new Asset.javascript('/js/cubix-popup-v3.0.source.js', { onload: checker });
	new Asset.javascript('/js/cubix-button-v1.0.source.js', { onload: checker });*/
	
});

Number.implement({
	numberFormat: function(decimals, dec_point, thousands_sep) {
		decimals = Math.abs(decimals) + 1 ? decimals : 2;
		dec_point = dec_point || '.';
		thousands_sep = thousands_sep || '';

		var matches = /(-)?(\d+)(\.\d+)?/.exec((isNaN(this) ? 0 : this) + ''); // returns matches[1] as sign, matches[2] as numbers and matches[3] as decimals
		var remainder = matches[2].length > 3 ? matches[2].length % 3 : 0;

		return (matches[1] ? matches[1] : '') + (remainder ? matches[2].substr(0, remainder) + thousands_sep : '') + matches[2].substr(remainder).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep) +
			(decimals ? dec_point + (+matches[3] || 0).toFixed(decimals).substr(2) : '');
	}
});
