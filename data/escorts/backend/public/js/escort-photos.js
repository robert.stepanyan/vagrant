var Escort = {};
var imageWidth = 150;
var imageHeight = 205;
Escort.Photos = function (popup) {
	this.popup = popup;

	this.content = this.popup.content;
	this.overlay = new Cubix.Overlay($('photo-tabs'), {showLoader: false});
	
	this.init_mootabs = function () {
		/* >>> Handle Private and Public photos parameters */
		this.tabs = new mootabs('photo-tabs', {width: null, height: null});
		this.active_tab = 'public';
		
		this.tabs.addEvent('activate', function (tab) {
			if ( tab.get('title') == 'Private' ) {
				//this.uploader.setData({type: 3});
				this.setUploaderAction(3);
				this.active_tab = 'private';
			}
			else if ( tab.get('title') == 'Public' ) {
				//this.uploader.setData({type: 1});
				
				if($defined($('upload-soft-photos'))){
					this.setUploaderAction(2);
				}
				else{
					this.setUploaderAction(1);
				}
				this.active_tab = 'public';
			} else if ( tab.get('title') == 'Disabled' ) {
				//this.uploader.setData({type: 4});
				this.setUploaderAction(4);
				this.active_tab = 'disabled';
			} else if ( tab.get('title') == 'Archived' ) {
				//this.uploader.setData({type: 4});
				this.setUploaderAction(5);
				this.active_tab = 'archived';
			}
			else if ( tab.get('title') == 'Photo-history' ) {
				this.active_tab = 'Photo-history';

				window.getHistoryPhotos($('ph_escort_id').get('value'), false);
			}
			else if ( tab.get('title') == 'All-Pix' ) {
				this.active_tab = 'All-Pix';

                new Request({
                    url: '/escorts-v2/photo-galleries?id=' + $('ph_escort_id').get('value'),
                    method: 'get',
                    onSuccess: function (resp) {
                        $('photo-galleries').set('html', resp);
						new mootabs('gallery-tabs', { width: null, height: 'auto' });
                    }.bind(this)
                }).send();
			}
			
			if (this.active_tab != 'Photo-history'){
				$(tab.get('title')).getElements('li').each(function (li) {li.cropper.reinitLimits();});
			}
			
		}.bind(this));
		/* <<< */
	};

	window.getHistoryPhotos = function (escortId, overlay, page = 1) {
		new Request({
			url: '/escorts-v2/photo-history?id=' + escortId +'&page='+ page,
			method: 'get',
			onSuccess: function (resp) {
				if (overlay === true) {
					this.overlay.enable();
				}
				$$('.photo-history').set('html', resp);
			}.bind(this)
		}).send();
	};
	
	this.init_uploader = function () {
		var self = this;
		
		$('filecontrol').set('html', '');

		this.uploader = new MooUpload('filecontrol', {    
			action: '/escorts-v2/upload-html5?escort_id=' + this.popup.escort_id,// Server side upload script    
			method: 'html5',		// Use only HTML5 method		
			blocksize: 999999999,	// Load per one chunk
			onAddFiles: function() {},
			onFinishUpload: function() {},
			onFileUpload: function(fileindex, resp){
				if ( ! resp.error && resp.finish ) {
					var container = 'public';
					if ( resp.photo.type == 3 ) container = 'private';
					if ( resp.photo.type == 4 ) container = 'disabled';
					if ( resp.photo.type == 5 ) container = 'archived';

					var photo = self.render_photo(resp.photo).
						set('opacity', 0).
						inject(self.content.getElement('.photos-' + container).getElement('.clear'), 'before').
						fade(1);

					if ( container == 'public' ) {
						if($defined($('upload-soft-photos'))){
							self.mark_photo(photo, 'soft');
						}
						else{
							self.mark_photo(photo, 'hard');
						}
						
					}

					self.sortables.addItems(photo);
				} else if ( resp.error ) {
					if ( ! $$('#filecontrol_listView ul.errors').length ) {
						new Element('ul', {
							'class' : 'errors'
						}).inject($('filecontrol_listView'));
					}
					
					
					var li = new Element('li', {});
					
					li.inject($$('#filecontrol_listView ul.errors')[0]);
					
					new Element('span', {
						'html' : resp.name + ' - '
					}).inject(li);
					new Element('span', {
						'class' : 'mooupload_error',
						'html' : resp.msg
					}).inject(li);
					
					
					
				}
				
				self.overlay.enable();
				window.fireEvent('resize');
			}
		});
		
		/*this.uploader = new Cubix.Uploader({
			url: '/escorts-v2/upload-photo?escort_id=' + this.popup.escort_id
		});
		
		this.uploader.addEvent('fileUploaded', function (resp) {
			if ( resp.status == 'success' ) {
				var container = 'public';
				if ( resp.photo.type == 3 ) container = 'private';
				if ( resp.photo.type == 4 ) container = 'disabled';
				
				var photo = this.render_photo(resp.photo).
					set('opacity', 0).
					inject(this.content.getElement('.photos-' + container).getElement('.clear'), 'before').
					tween('opacity', 1);
				
				if ( container == 'public' ) {
					this.mark_photo(photo, 'hard');
				}
				
				this.sortables.addItems(photo);
			}
			
			this.overlay.enable();
			window.fireEvent('resize');
		}.bind(this));*/
	};
	
	this.setUploaderAction = function(type) {
		this.uploader.options.action = '/escorts-v2/upload-html5?escort_id=' + this.popup.escort_id;
		if ( type ) {
			this.uploader.options.action += '&type=' + type;
		}
		
	}
	
	/* >>> Photos initialization */
	

	this.render_photo = function (photo) {
		var preventCache = Number.random(1,100000);
		var container = new Element('table', {
			'class': 'photo'
		});
 
		var content = new Element('td', {}).inject(new Element('tr', {}).inject(container));
		var wrapper = new Element('div', {'class': 'wrapper'}).inject(content);

		var cropper =  new Element('div', {'class': 'border'}).inject(wrapper);
		new Element('div',{'class':'border_bg',
					styles:{'background-image':'url('+photo.image_url+ '?cache='+preventCache+')'}
					}).inject(cropper);
		var wrapper_chunks = new Element('div', {'class': 'chunks'}).inject(wrapper);
		wrapper.data = photo;
		
		new Element('div', {'class': 'status '}).inject(wrapper);
		var actions_div = new Element('div', {'class': 'actions actionutyun', hash: photo.hash, ext: photo.ext}).inject(wrapper_chunks);
		var rotate_left = new Element('div', {'class': 'rotate-left'}).inject(actions_div);
		var rotate_right = new Element('div', {'class': 'rotate-right'}).inject(actions_div);

		new Element('div', {'class': 'handler'}).inject(wrapper_chunks);
		rotate_left.removeEvents();
		rotate_left.addEvent('click', function(){
			var x = this;
			var degree = 90;
			var actions = this.getParent('.actions');
			var chunks = this.getParent('.chunks');
			var borderBg = this.getParent('.wrapper').getElement('.border').getElement('.border_bg');
			var hash = actions.get('hash');
			var ext = actions.get('ext');
			var downloadLink = chunks.getNext('a');
			var image = chunks.getNext('img');
			new Request({
				url: '/escorts-v2/rotate-photo',
				method: 'post',
				data: { degree: degree, escort_id: popup.escort_id, hash: hash, ext: ext, photo_id: photo.id },
				onSuccess: function (resp) {
					resp = JSON.decode(resp);

					if ( resp.error ) {
						alert('An error occured');
					}
					else {
						var preventCache = Number.random(1,100000);
						var re = /((\?|\#).*)$/;

						newDownloadLink = downloadLink.get('href').replace(re, "");
						downloadLink.set('href',  newDownloadLink + '?cache=' + preventCache + '&origano=true');
						newImage = image.get('src').replace(re, "");
						image.set('src',  newImage + '?cache=' + preventCache).onload = function(){
							var li = x.getParent('li');
                                li.cropper.reinitLimits();
                            }.bind(self);
						borderBg.setStyle('background-image', 'url('+newImage + '?cache=' + preventCache+')' );
						
					}
				}
			}).send();
		});
		rotate_right.removeEvents();
		rotate_right.addEvent('click', function(){
			var x = this;
			var degree = -90;
			var actions = this.getParent('.actions');
			var chunks = this.getParent('.chunks');
			var borderBg = this.getParent('.wrapper').getElement('.border').getElement('.border_bg');
			var hash = actions.get('hash');
			var ext = actions.get('ext');
			var downloadLink = chunks.getNext('a');
			var image = chunks.getNext('img');
			
			new Request({
				url: '/escorts-v2/rotate-photo',
				method: 'post',
				data: { degree: degree, escort_id: popup.escort_id, hash: hash, ext: ext, photo_id: photo.id },
				onSuccess: function (resp) {
					resp = JSON.decode(resp);

					if ( resp.error ) {
						alert('An error occured');
					}
					else {
						var preventCache = Number.random(1,100000);
						var re = /((\?|\#).*)$/;
						
						newDownloadLink = downloadLink.get('href').replace(re, "");
						downloadLink.set('href',  newDownloadLink + '?cache=' + preventCache + '&origano=true');
						newImage = image.get('src').replace(re, "");
						image.set('src',  newImage + '?cache=' + preventCache).onload = function(){
								var li = x.getParent('li');
                             	li.cropper.reinitLimits();
                            }.bind(self);
						borderBg.setStyle('background-image', 'url('+newImage + '?cache=' + preventCache+')' );
						li.cropper.reinitLimits();
						
					}
				}
			}).send();
		});

		var checkbox = new Element('input', {
			type: 'checkbox',
			name: 'photos[]',
			value: photo.id,
			events: {
				change: function () {
					if ( checkbox.get('checked') ) {
						checkbox.set('checked', true);
					}
					else {
						checkbox.set('checked', false);
					}
				}
			}
		}).inject(new Element('div', {'class': 'chk'}).inject(wrapper_chunks));
			
		
		var img = new Element('img', {
			src: photo.image_url+ '?cache='+preventCache
			//'width':photo.is_portrait==1?imageWidth:'',
			//'height':photo.is_portrait==0?imageHeight:''
	
//			events: {
//				click: function () {
//					if ( checkbox.get('checked') ) {
//						checkbox.set('checked', false);
//					}
//					else {
//						checkbox.set('checked', true);
//					}
//				}
//			}
		}).inject(wrapper);
		//console.log(photo);
		var a = new Element('a', {
			href: photo.orig_url + '?origano=true'+ '&cache='+preventCache,
			html: 'download',
			target: '_blank',
			'class': 'down_link'

		}).inject(wrapper);

		var li = new Element('li');
		
		li.data = photo;
		
		container.inject(li);
		
		if ( photo.is_verified ) {
			this.mark_photo(li, 'p100');
		}

		if ( photo.gotd ) {
			this.mark_photo(li, 'gotd');
		}
		
		if ( photo.profile_boost ) {
			this.mark_photo(li, 'profile_boost');
		}

		if ( ! photo.is_approved ) {
			////////
			this.mark_photo(li, 'not_approved');
		}

		if ( ! photo.double_approved  && $defined($('btn-photos-public-double-approve'))) {
			this.mark_photo(li, 'auto_approved');
		}

		if ( photo.is_rotatable ) {
			this.mark_photo(li, 'rotatable');
		}
		
		if ( photo.is_popunder ) {
			this.mark_photo(li, 'popunder');
		}
		
		if ( photo.is_suspicious == 1 ) {
			this.mark_photo(li, 'suspicious');
		}

		if ( photo.is_valentines == 1 ) {
			this.mark_photo(li, 'valentines');
		}

		else if (photo.is_suspicious == 2) {
			this.mark_photo(li, 'investigation');
		}
		
		if ( photo.is_main && photo.type != 3 ) {
			this.mark_photo(li, 'main');
		}



		if ( photo.type == 1 ) {
			this.mark_photo(li, 'hard');
		}
		else if ( photo.type == 2 ) {
			this.mark_photo(li, 'soft');
		}

		if ( photo.retouch_status == 1 ) {
			this.mark_photo(li, 'retouch');
		}
		else if (photo.retouch_status == 3){
			this.mark_photo(li, 'no_retouch');
		}

		li.cropper = new Cropper(wrapper, imageWidth, imageHeight).addEvent('complete', saveAdjustment);

		window.fireEvent('resize');

		return li;
	}.bind(this);

	this.mark_photo = function (photos, Flag) {
		if ( ! photos ) return;

		if ( $type(photos) == 'element' ) photos = [photos];

		if ( Flag == 'main' ) {
			var tab = this.active_tab;
			this.content.getElement('.photos-' + tab).getElements('li').each(function (el) {
				el.getElement('td').removeClass('main');
				el.data.is_main = 0;
			});
		}



		photos.each(function (photo) {
			var status = photo.getElement('.status');
			var flags = new Hash();

			status.getElements('div').each(function (el) {
				flags.set(el.get('class'), el);
			});

			if ( Flag == 'hard' ) {
				if ( flags.get('hard') ) return;
				flag = new Element('div', {'class': 'hard', html: 'hard'});
				if ( flags.get('soft') ) flag.replaces(flags.get('soft'));
				else flag.injectBottom(status);
			}
			else if ( Flag == 'soft' ) {
				if ( flags.get('soft') ) return;
				flag = new Element('div', {'class': 'soft', html: 'soft'});
				if ( flags.get('hard') ) flag.replaces(flags.get('hard'));
				else flag.injectBottom(status);
			}
			else if ( Flag == 'valentines' ) {
				if ( flags.get('valentines') ) return;
				flag = new Element('div', {'class': 'valentines', html: 'Valentines'});
				flag.injectBottom(status);
			}
			else if ( Flag == 'p100' ) {
				if ( flags.get('p100') ) {flags.get('p100').destroy();return;}
				new Element('div', {'class': 'p100'}).injectTop(status);
			}
			else if ( Flag == 'gotd' ) {
				if ( flags.get('gotd') ) {flags.get('gotd').destroy();return;}
				this.content.getElements('.gotd').destroy();
				new Element('div', {'class': 'gotd', html: 'GOTD'}).injectTop(status);
			}
			else if ( Flag == 'profile_boost' ) {
				if ( flags.get('profile_boost') ) {flags.get('profile_boost').destroy();return;}
				this.content.getElements('.profile_boost').destroy();
				new Element('div', {'class': 'profile_boost', html: 'Profile Boost'}).injectTop(status);
			}
			else if ( Flag == 'popunder' ) {
				if ( flags.get('popunder') ) {flags.get('popunder').destroy();return;}
				new Element('div', {'class': 'popunder', html: 'popunder'}).injectTop(status);
			}
			else if ( Flag == 'private' ) {
				if ( flags.get('hard') ) flags.get('hard').destroy();
				if ( flags.get('soft') ) flags.get('soft').destroy();
				photo.getElement('.wrapper').removeClass('main');
				return;
			}
			else if ( Flag == 'main' ) {
				photo.getElement('td').addClass('main');
				photo.data.is_main = 1;
			}
			else if ( Flag == 'rotatable' ) {
				
				if(photo.data.is_main != 1){
					photo.getElement('td').addClass('rotatable');
				}
			}
			else if ( Flag == 'not_approved' ) {
				//var tab = this.is_private ? 'private' : 'public';
				//this.content.getElement('.photos-' + tab).getElements('li').each(function (el) {
					if ( photo.getElement('.wrapper').get('not_approved') ) {photo.getElement('.wrapper').get('not_approved').destroy();return;}
					flag = new Element('div', {'class': 'not_approved', html: 'not approved'});
					flag.injectBottom(photo.getElement('.wrapper'));
				//});
			}
			else if ( Flag == 'auto_approved' ) {
					if ( photo.getElement('.wrapper').get('auto_approved') ) {photo.getElement('.wrapper').get('auto_approved').destroy();return;}
					flag = new Element('div', {'class': 'auto_approved', html: 'auto approved'});
					flag.injectBottom(photo.getElement('.wrapper'));
			}
			else if ( Flag == 'retouch' ) {
				if ( flags.get('retouch') ) {flags.get('retouch').destroy();return;}
				new Element('div', {'class': 'retouch', html: 'retouch'}).injectTop(status);
				if ( photo.getElement('.wrapper').getElement('.not_approved') ) {photo.getElement('.wrapper').getElement('.not_approved').destroy();return;}
			}
			else if ( Flag == 'no_retouch' ) {
				//if ( flags.get('no_retouch') ) {flags.get('no_retouch').destroy();return;}
				new Element('div', {'class': 'retouched', html: 'no retouch'}).injectTop(status);
			}
			else if ( Flag == 'suspicious' ) {
				if ( flags.get('suspicious') ) {flags.get('suspicious').destroy();return;}
				new Element('div', {'class': 'suspicious', html: 'suspicious'}).injectTop(status);
			}
			else if ( Flag == 'investigation' ) {
				if ( flags.get('investigation') ) {flags.get('investigation').destroy();return;}
				new Element('div', {'class': 'suspicious', html: 'investigation'}).injectTop(status);
			}
		}.bind(this));
	};
	
	this.init_photos = function () {
		/* >>> Get the json data from containers */
		['public', 'private', 'disabled', 'archived'].each(function (container) {
			if ( ! this.content.getElement('.photos-' + container) ) return;
			container = this.content.getElement('.photos-' + container);
			var photos = JSON.decode(container.get('text'));
			container.empty();
			var clearer = new Element('div', {'class': 'clear'}).inject(container);
			
			photos.each(function (photo) {
				this.render_photo(photo).inject(clearer, 'before');
				window.fireEvent('resize');
			}.bind(this));
		}.bind(this));
		/* <<< */
	};
	/* <<< */
	
	this.init_sorting = function (list) {
		var self = this;
		
		this.sortables = new Sortables(list, {
			clone: function (event, element, list) {
				if ( event.event.button ) {this.reset();return new Element('div');}
				
				var pos = element.getPosition(element.getOffsetParent());
				pos.x += 5;
				pos.y += 5;
				
				return element.getElement('img').clone(true).setStyles({
					'margin': '0px',
					'position': 'absolute',
					'z-index': 100,
					'visibility': 'hidden',
					'opacity': .7,
					'width': element.getElement('img').getStyle('width')
				}).inject(this.list).position(pos);
			},
			
			opacity: 1,
			revert: true,
			handle: '.handler',
			
			onStart: function (el) {
				this.drag.addEvent('enter', function () {
					this.started = true;
				}.bind(this));
			},
			
			onComplete: function (el) {
				if ( ! this.started ) return;
				else this.started = false;
				
				self.actions.save_order();
			}
		});
	};
	
	this.init_popunder = function(){
		this.popunderPopup = new Cubix.Popup.Ajax({
			abar: [
				{ caption: 'Save', icon: 'accept', action: function(){
					
					new Request({
						url: '/escorts-v2/save-popunder',
						data: {
							'photo_id'  : this.popunderPopup.content.getElements('input[name=photo_id]')[0].get('value'),
							'escort_id' : this.popunderPopup.content.getElements('input[name=escort_id]')[0].get('value')
						},
						method: 'post',
						onSuccess: function (resp) {
							this.mark_photo(this.popunderPopup.element,'popunder');
							this.popunderPopup.close();
						}.bind(this)
					}).send();
				}.bind(this) },
				{ caption: 'Close', icon: 'cancel', action: Cubix.Popup.Actions.Close }
			],
			width: 554,
			zIndex: function () { return Cubix.Popup.zIndex + 15 }
		});
	}
	
	this.init_uploader();
	this.init_mootabs();
	this.init_photos();
	this.init_sorting($$('.photos-public')[0]);
	this.init_sorting($$('.photos-private')[0]);
	this.init_popunder();
	this.actions = {};
		
	this.actions.make_public_private = function (type) {
		var dest_tab;
		if ( type == 'private' ) {
			dest_tab = 'Public';
		}
		else if ( type == 'public' ) {
			dest_tab = 'Private';
		}
		
		var selection = this.get_selection(type);
		
		this.request_action(dest_tab.toLowerCase(), selection, function (resp) {
			var change_main = false;
			if ( type == 'public' ) {
				selection.els.each(function (el) {
					
					/*if ( el.getElement('.photo').data.is_main ) {*/
					if ( el.getElement('.photo').getElement('div.wrapper').hasClass('main') ) {
						change_main = true;
					}
				}.bind(this));
			}
			
			selection.els.tween({'opacity': [1, 0]}, function () {
				this.mark_photo(selection.els, ( type == 'private' ? 'hard' : 'private' ));
				this.tabs.activate(dest_tab);
				selection.els.each(function (el) {el.getElements('input[type=checkbox]').set('checked', null);});
				selection.els.inject(this.content.getElement('.photos-' + dest_tab.toLowerCase()).getElement('.clear'), 'before');
				selection.els.tween({'opacity': [0, 1]});
				
				if ( change_main ) {
					this.mark_photo(this.content.getElement('.photos-' + type).getFirst('li'), 'main');
				}
				
				window.fireEvent('resize');
			}.bind(this));
		}.bind(this));
	}.bind(this);
	
	// Refactored function for make_public_private.
	this.actions.changeType = function (from, to) {
		var dest_tab = to.charAt(0).toUpperCase() + to.slice(1);
		var selection = this.get_selection(from);
		
		var change_main = false;
		if ( from == 'public' ) {
			selection.els.each(function (el) {
				if ( el.getElement('.photo').getElement('div.wrapper').getParent('td').hasClass('main') ) {
					change_main = true;
				}
			}.bind(this));
		}

		this.request_action(to, selection, function (resp) {
			
			selection.els.tween({'opacity': [1, 0]}, function () {
				switch ( to ) {
					case 'public' :
						this.mark_photo(selection.els, 'hard');
						break;
					case 'public-soft':
						this.mark_photo(selection.els, 'soft');
						dest_tab = 'Public';
						to = 'public';
						break;
					case 'private' :
						this.mark_photo(selection.els, 'private');
						break;
					case 'disabled' :
						this.mark_photo(selection.els, 'private');
						break;
					case 'archived' :
						this.mark_photo(selection.els, 'archived');
						break;
				}
				this.tabs.activate(dest_tab);
				selection.els.each(function (el) {el.getElements('input[type=checkbox]').set('checked', null);});
				selection.els.inject(this.content.getElement('.photos-' + to).getElement('.clear'), 'before');

				if(from == 'public' && to == 'disabled') {
					selection.els.each(function (el) {
						if(el.getElement('.not_approved')) {
							el.getElement('.not_approved').destroy();
						}
					});
				}

				selection.els.tween({'opacity': [0, 1]});
				
				if ( change_main ) {
					this.mark_photo(this.content.getElement('.photos-' + from).getFirst('li'), 'main');
				}
				
				window.fireEvent('resize');
			}.bind(this));
		}.bind(this));
	}.bind(this);
	

	this.actions.select_all = function (tab) {
		$$('#' + tab + ' .photos input[type=checkbox]').each(function(it){
			if ( it.get('checked') ) {
				it.set('checked', '');
			}
			else {
				it.set('checked', 'checked');
			}
		});
		/*this.request_action('verified', selection, function (resp) {
			selection.els.each(function (el) {
				this.mark_photo(el, 'p100');
			}.bind(this));

			selection.els.each(function (el) { el.getElements('input[type=checkbox]').set('checked', null); });
		}.bind(this));*/
	}.bind(this);

	this.actions.select_not_approved = function (tab) {
		$$('#' + tab + ' .photos input[type=checkbox]').each(function(it){
			if( $defined(it.getParent('div.wrapper').getElements('div.not_approved')[0]) ) {
				if ( it.get('checked') ) {
					it.set('checked', '');
				}
				else {
					it.set('checked', 'checked');
				}
			}
		});
		/*this.request_action('verified', selection, function (resp) {
			selection.els.each(function (el) {
				this.mark_photo(el, 'p100');
			}.bind(this));

			selection.els.each(function (el) { el.getElements('input[type=checkbox]').set('checked', null); });
		}.bind(this));*/
	}.bind(this);
	
	this.actions.toggle_verified = function () {
		var selection = this.get_selection();
		
		this.request_action('verified', selection, function (resp) {
			selection.els.each(function (el) {
				this.mark_photo(el, 'p100');
			}.bind(this));
			
			selection.els.each(function (el) {el.getElements('input[type=checkbox]').set('checked', null);});
		}.bind(this));
	}.bind(this);

	this.actions.toggle_valentines = function () {
		var selection = this.get_selection();
		var tab = this.active_tab;
		if (selection.els.length > 1){
			alert('Maximum allowed 1 photo to toggle as Valentines');
			return;
		}
		this.request_action('valentines', selection, function (resp) {
			selection.els.each(function (el) {
				if (el.getElement('.valentines'))
				{
					el.getElement('.valentines').destroy();
					$$('.photos-'+ tab +' input[type=checkbox]').each(function(it){
						if( $defined(it.getParent('div.wrapper').getElements('div.status')[0]) ) {
							var elem = it.getParent('div.wrapper').getElements('div.status')[0];
							if (elem.getElement('.valentines'))
							{
								elem.getElement('.valentines').destroy()
							}
						}
					});
				}else{
					$$('.photos-'+ tab +' input[type=checkbox]').each(function(it){
						if( $defined(it.getParent('div.wrapper').getElements('div.status')[0]) ) {
							var elem = it.getParent('div.wrapper').getElements('div.status')[0];
							if (elem.getElement('.valentines'))
							{
								elem.getElement('.valentines').destroy();
							}
						}
					});
					this.mark_photo(el, 'valentines');
				}

			}.bind(this));

			selection.els.each(function (el) {el.getElements('input[type=checkbox]').set('checked', null);});
		}.bind(this));
	}.bind(this);

	this.actions.toggle_retouch = function () {
		var selection = this.get_selection();
		
		if (!selection.ids.length){
			var tab = this.active_tab;
			if($$('.photos-'+ tab).getElements('div.retouch')[0].length){
				$$('.photos-'+ tab +' input[type=checkbox]').each(function(it){
					if( $defined(it.getParent('div.wrapper').getElements('div.retouch')[0]) ) {
						it.set('checked', 'checked');
						
					}
				});
				return;
			}
			
		}
				
		this.request_action('retouch', selection, function (resp) {
			selection.els.each(function (el) {
				this.mark_photo(el, 'retouch');
			}.bind(this));
			
			selection.els.each(function (el) {el.getElements('input[type=checkbox]').set('checked', null);});
		}.bind(this));
	}.bind(this);

	this.actions.add_to_collecting = function () {
		var selection = this.get_selection();
		
		if (selection.ids.length){
			new Request({
	            url: '/newsletter/email-collecting/add-collecting-images?escort_id=' + $('ph_escort_id').get('value') +'&ids=' + selection.ids,
	            method: 'get',
	            onSuccess: function (resp) {
	            	return;
	            }.bind(this)
	        }).send();
		}

	}.bind(this);

	this.actions.toggle_approve = function () {
		var selection = this.get_selection();
		
		this.request_action('approve', selection, function (resp) {
			selection.els.each(function (el) {
				if ( el.getElement('.wrapper').getElement('.not_approved') ) {
					el.getElement('.wrapper').getElement('.not_approved').destroy();
				}
				else {
					this.mark_photo(el, 'not_approved');
				}
			}.bind(this));

			selection.els.each(function (el) {el.getElements('input[type=checkbox]').set('checked', null);});
		}.bind(this));
	}.bind(this);

	this.actions.double_approve = function () {
		var selection = this.get_selection();

		this.request_action('double_approve', selection, function (resp) {
			selection.els.each(function (el) {
				if ( el.getElement('.wrapper').getElement('.auto_approved') ) {
					el.getElement('.wrapper').getElement('.auto_approved').destroy();
				}
				
			}.bind(this));

			selection.els.each(function (el) {el.getElements('input[type=checkbox]').set('checked', null);});
			
		}.bind(this));
	}.bind(this);
	
	this.actions.set_main = function () {
		var selection = this.get_selection();

		if ( ! selection.els[0].getElement('.wrapper').getElement('.not_approved') ) {
			this.request_action('set-main', selection, function (resp) {
				this.mark_photo(selection.els[0], 'main'); 

				selection.els.each(function (el) {el.getElements('input[type=checkbox]').set('checked', null);});
			}.bind(this));
		}
		else {
			alert('You can\'t use unapproved photo for main image !');
		}
	}.bind(this);
	
	this.actions.set_rotatable = function () {
		var selection = this.get_selection();
		var tab = this.active_tab;		
		this.content.getElement('.photos-' + tab).getElements('li').each(function (el) {
				el.getElement('td').removeClass('rotatable');
				el.data.is_rotatable = 0;
			
		});
		this.request_action('rotatable', selection, function (resp) {
			selection.els.each(function (el) {
				this.mark_photo(el, 'rotatable');
			}.bind(this));
			
			selection.els.each(function (el) {el.getElements('input[type=checkbox]').set('checked', null);});
		}.bind(this));
	}.bind(this);
	
	this.actions.make_soft_hard = function (type) {
		var selection = this.get_selection();
		this.request_action(type, selection, function (resp) {
			selection.els.each(function (el) {
				this.mark_photo(el, type);
			}.bind(this));
			
			selection.els.each(function (el) {el.getElements('input[type=checkbox]').set('checked', null);});
		}.bind(this));
	}.bind(this);
	
	this.actions.remove = function () {
		var selection = this.get_selection();
		
		this.request_action('remove', selection, function (resp) {
			selection.els.each(function (el) {
				el.getElements('input[type=checkbox]').set('checked', null);
			}.bind(this));
			
			selection.els.tween({'opacity': [1, 0], 'width': 0}, function () {
				this.sortables.removeItems.pass(selection.els, this.sortables).call();
				selection.els.destroy();
			}.bind(this));
		}.bind(this));
	}.bind(this);

	this.actions.remove_permanently = function () {
		

		var chks = $$('.history_photo_remove_action');
		
		var result = {ids: []};
		
		chks.getElements('input[type=checkbox]').each(function (chk) {
			if ( chk.get('checked')[0] ) {
				result.ids.include(chk.get('value')[0]);
			}
		});
		
		if ( ! result.ids.length ) {
			alert('Please select some photos and then continue');
			return;
		}
		this.overlay.disable();
		new Request({
                url: '/escorts-v2/photo-history-remove-permanently',
                method: 'POST',
                data: {
                	photos: result.ids,
                	escort_id: this.popup.escort_id
                },
                onSuccess: function (resp) {
                	window.getHistoryPhotos(this.popup.escort_id, true)
                    
                }.bind(this)
            }).send();
		
	}.bind(this);	
	
	this.actions.save_order = function () {
		var selection = {ids: []};
		var tab = this.active_tab;
		
		this.content.getElement('.photos-' + tab).getElements('input[type=checkbox]').each(function (el) {
			selection.ids.include(el.get('value'));
		});
		
		this.request_action('sort', selection, function () {
			
		});
	}.bind(this);
	
	this.actions.set_gotd = function () {
		var selection = this.get_selection();

		if ( ! selection.els[0].getElement('.wrapper').getElement('.not_approved') ) {
			this.request_action('set-gotd', selection, function (resp) {
				this.mark_photo(selection.els[0], 'gotd'); 

				selection.els.each(function (el) {el.getElements('input[type=checkbox]').set('checked', null);});
			}.bind(this));
		}
		else {
			alert('You can\'t use unapproved photo for gotd image !');
		}
	}.bind(this);
	
	this.actions.set_profile_boost = function () {
		var selection = this.get_selection();

		if ( ! selection.els[0].getElement('.wrapper').getElement('.not_approved') ) {
			this.request_action('set-profile-boost', selection, function (resp) {
				this.mark_photo(selection.els[0], 'profile_boost'); 

				selection.els.each(function (el) {el.getElements('input[type=checkbox]').set('checked', null);});
			}.bind(this));
		}
		else {
			alert('You can\'t use unapproved photo for profile boost image !');
		}
	}.bind(this);
	
	
	this.actions.large_popunder = function(){
		var selection = this.get_selection();
		if ( ! selection.ids.length ) {
			alert('Please select photo');
			return;
		}
		
		selection.els.each(function (el) {
			el.getElements('input[type=checkbox]').set('checked', null);
		}.bind(this));
		
		this.popunderPopup.setTitle('Crop Photo'),
		this.popunderPopup.load('/escorts-v2/set-popunders?photo_id=' + selection.ids[0], this.popunderPopup.open);
		this.popunderPopup.element = selection.els[0];
		this.popunderPopup.addEvent('loaded', function () {
			var cropWidth = 500;
			var cropHeight = 163;  // 31*cropWidth/95 FOR IMAGE 950 x 310
			new Cropper('crop-wrapper', cropWidth, cropHeight).addEvent('complete', saveAdjustment);
		});
		this.popunderPopup.addEvent('close', function (e) {
			if ( $defined($('crop-wrapper')) ) {
				$('crop-wrapper').destroy();
			}
		});
		
	}.bind(this);
	
	this.actions.remove_large_popunder = function(){
		var selection = this.get_selection();
		
		this.request_action('unset-large-popunder', selection, function (resp) {
				selection.els.each(function (el) {
					this.mark_photo(el, 'popunder');
					el.getElements('input[type=checkbox]').set('checked', null);
				}.bind(this));
			}.bind(this));
	}.bind(this);
	
	this.request_action = function (action, selection, callback) {
		if ( ! selection.ids.length ) {
			alert('Please select some photos and then continue');
			return;
		}
		
		if ( action == 'remove' && ! confirm('Are you sure you want delete these photos?') ) {
			return;
		}
		
		this.overlay.disable();
		
		var ids = [];
		for ( i = 0; i < selection.ids.length; i++ ) {
			ids.include('photos[]=' + selection.ids[i]);
		}
		
		if(action == 'public-soft'){
			action = 'public';
		}
		new Request({
			url: '/escorts-v2/do-photos?act=' + action + '&' + ids.join('&'),
			method: 'get',
			onSuccess: function (resp) {
				(callback.bind(this))(resp);
				this.overlay.enable();

				if ( action == 'approve' ) {
					this.popup.close();
				}

			}.bind(this)
		}).send();
	};
	
	this.get_selection = function (tab) {
		if ( ! $defined(tab) ) {
			var tab = this.active_tab;
		}
		
		var chks = this.content.getElement('.photos-' + tab);
		
		var result = {ids: [], els: $$()};
		
		chks.getElements('input[type=checkbox]').each(function (chk) {
			if ( chk.get('checked') ) {
				result.ids.include(chk.get('value'));
				result.els.include(chk.getParent('li'));
			}
		});
		
		result.els.tween = function (opt, callback) {
			var opts = {};
			for ( var i = 0; i < result.els.length; i++ ) {
				opts[i] = opt;
			}
			
			new Fx.Elements(result.els, {
				onComplete: callback
			}).start(opts);
		}
		
		return result;
	};
	
	
	// In Public Tab
	if ( $defined($('btn-photos-public-gotd')) ) {
		new Cubix.Button($('btn-photos-public-gotd'), {onClick: function () {
			this.actions.set_gotd('public');
		}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'GOTD'});
	}
	
	if ( $defined($('btn-photos-public-profile-boost')) ) {
		new Cubix.Button($('btn-photos-public-profile-boost'), {onClick: function () {
			this.actions.set_profile_boost('public');
		}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Profile Boost'});
	}
	
	new Cubix.Button($('btn-photos-public-make-private'), {onClick: function () {
		this.actions.changeType('public', 'private');
	}.bind(this), icon: 'key', classes: 'float-left ml5 mb5', caption: 'Make Private'});
	
	if ( $defined($('btn-photos-public-make-soft')) ) {
		new Cubix.Button($('btn-photos-public-make-soft'), {onClick: function () {
			this.actions.make_soft_hard('soft');
		}.bind(this), classes: 'float-left ml5 mb5', caption: 'Make Soft'});
	}
	if ( $defined($('btn-photos-public-make-hard')) ) {
		new Cubix.Button($('btn-photos-public-make-hard'), {onClick: function () {
			this.actions.make_soft_hard('hard');
		}.bind(this), classes: 'float-left ml5 mb5', caption: 'Make Hard'});
	}
	if ( $defined($('btn-photos-public-remove')) ) {
		new Cubix.Button($('btn-photos-public-remove'), {onClick: function () {
			this.actions.remove();
		}.bind(this), icon: 'delete', classes: 'float-left ml5 mb5', caption: 'Remove'});
	}
	if ( $defined($('btn-photos-public-make-verified')) ) {
		new Cubix.Button($('btn-photos-public-make-verified'), {onClick: function () {
			this.actions.toggle_verified();
		}.bind(this), icon: 'rosette', classes: 'float-left ml5 mb5', caption: 'Tog. Verification'});
	}
	if ( $defined($('btn-photos-valentines')) ) {
		new Cubix.Button($('btn-photos-valentines'), {onClick: function () {
				this.actions.toggle_valentines();
			}.bind(this), classes: 'float-left ml5 mb5', caption: 'Tog./Block Valentines'});
	}

	new Cubix.Button($('btn-photos-public-approve'), {onClick: function () {
		this.actions.toggle_approve();
	}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Tog. Approvement'});
	
	new Cubix.Button($('btn-photos-public-set-main'), {onClick: function () {
		this.actions.set_main();
	}.bind(this), icon: 'accept', classes: 'float-left ml5 mb5', caption: 'Set Main'});
	
	new Cubix.Button($('btn-photos-public-set-rotatable'), {onClick: function () {
		this.actions.set_rotatable();
	}.bind(this), icon: 'accept-yellow', classes: 'float-left ml5 mb5', caption: 'Set Rotatable'});
	
	new Cubix.Button($('btn-photos-public-select-all'), {onClick: function () {
		this.actions.select_all('Public');
	}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Select All'});

	if ( $defined($('btn-photos-public-not-approved')) ) {
		new Cubix.Button($('btn-photos-public-not-approved'), {onClick: function () {
			this.actions.select_not_approved('Public');
		}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Not Approved'});
	}

	if ( $defined($('btn-photos-public-double-approve')) ) {
		new Cubix.Button($('btn-photos-public-double-approve'), {onClick: function () {
			this.actions.double_approve('Public');
		}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Approve'});
	}
	
	new Cubix.Button($('btn-photos-public-make-disabled'), {onClick: function () {
		this.actions.changeType('public', 'disabled');
	}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Disable'});

	if ( $defined($('btn-photos-public-make-retouch')) ) {
		new Cubix.Button($('btn-photos-public-make-retouch'), {onClick: function () {
			this.actions.toggle_retouch();
		}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Retouch'});
	}

	if ( $defined($('btn-add-to-collecting-popup')) ) {
		new Cubix.Button($('btn-add-to-collecting-popup'), {onClick: function () {
			 this.actions.add_to_collecting();
		}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Add to coll.'});
	}
	
	if ( $('btn-photos-public-move-archive') ) {
		new Cubix.Button($('btn-photos-public-move-archive'), {onClick: function () {
			this.actions.changeType('public', 'archived');
		}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Move to Archive'});
	}
	
	if ( $('btn-photos-public-large-popunder') ) {
		new Cubix.Button($('btn-photos-public-large-popunder'), {onClick: function () {
			this.actions.large_popunder();
		}.bind(this), icon: 'photo', classes: 'float-left ml5 mb5', caption: 'Set Wide Popunder'});
	}
	
	if ( $('btn-photos-public-remove-large-popunder') ) {
		new Cubix.Button($('btn-photos-public-remove-large-popunder'), {onClick: function () {
			this.actions.remove_large_popunder();
		}.bind(this), icon: 'photo_delete', classes: 'float-left ml5 mb5', caption: 'Unset Wide Popunder'});
	}
	// In Private Tab
	
	if ( $defined($('btn-photos-private-gotd')) ) {
		new Cubix.Button($('btn-photos-private-gotd'), {onClick: function () {
			this.actions.set_gotd('private');
		}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'GOTD'});
	}
	if ( $defined($('btn-photos-private-remove')) ) {
		new Cubix.Button($('btn-photos-private-remove'), {onClick: function () {
			this.actions.remove();
		}.bind(this), icon: 'delete', classes: 'float-left ml5 mb5', caption: 'Remove'});
	}
	if ( $defined($('btn-photos-private-make-public')) ) {
		new Cubix.Button($('btn-photos-private-make-public'), {onClick: function () {
			this.actions.changeType('private', 'public');
		}.bind(this), icon: 'group', classes: 'float-left ml5 mb5', caption: 'Make Public'});
	}
	
	if ( $defined($('btn-photos-private-make-public-soft')) ) {
		new Cubix.Button($('btn-photos-private-make-public-soft'), {onClick: function () {
			this.actions.changeType('private', 'public-soft');
		}.bind(this), icon: 'group', classes: 'float-left ml5 mb5', caption: 'Make Public'});
	}

	if ( $defined($('btn-photos-private-make-verified')) ) {
		new Cubix.Button($('btn-photos-private-make-verified'), {onClick: function () {
			this.actions.toggle_verified();
		}.bind(this), icon: 'rosette', classes: 'float-left ml5 mb5', caption: 'Toggle Verification'});
	}

	if ( $defined($('btn-photos-archived-approve')) ) {
		new Cubix.Button($('btn-photos-archived-approve'), {onClick: function () {
			this.actions.toggle_approve();
		}.bind(this), icon: 'rosette', classes: 'float-left ml5 mb5', caption: 'Toggle Approvement'});
	}
	
	if ( $defined($('btn-photos-private-approve')) ) {
		new Cubix.Button($('btn-photos-private-approve'), {onClick: function () {
			this.actions.toggle_approve();
		}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Toggle Approvement'});
	}
	
	if ( $defined($('btn-photos-private-select-all')) ) {
		new Cubix.Button($('btn-photos-private-select-all'), {onClick: function () {
			this.actions.select_all('Private');
		}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Select All'});
	}
	
	if ( $defined($('btn-photos-private-not-approved')) ) {
		new Cubix.Button($('btn-photos-private-not-approved'), {onClick: function () {
			this.actions.select_not_approved('Private');
		}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Not Approved'});
	}

	if ( $defined($('btn-photos-private-double-approve')) ) {
		new Cubix.Button($('btn-photos-private-double-approve'), {onClick: function () {
			this.actions.double_approve('Private');
		}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Approve'});
	}
	
	if ( $defined($('btn-photos-private-make-disabled')) ) {
		new Cubix.Button($('btn-photos-private-make-disabled'), {onClick: function () {
			this.actions.changeType('private', 'disabled');
		}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Disable'});
	}

	if ( $defined($('btn-photos-private-make-retouch')) ) {
		new Cubix.Button($('btn-photos-private-make-retouch'), {onClick: function () {
			this.actions.toggle_retouch();
		}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Retouch'});
	}
	
	if ( $('btn-photos-private-move-archive') ) {
		new Cubix.Button($('btn-photos-private-move-archive'), {onClick: function () {
			this.actions.changeType('private', 'archived');
		}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Move to Archive'});
	}

	// IN disabled tab
	
	
	new Cubix.Button($('btn-photos-disabled-select-all'), {onClick: function () {
		this.actions.select_all('Disabled');
	}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Select All'});
	if ( $defined($('btn-photos-disabled-remove')) ) {
		new Cubix.Button($('btn-photos-disabled-remove'), {onClick: function () {
			this.actions.remove();
		}.bind(this), icon: 'delete', classes: 'float-left ml5 mb5', caption: 'Remove'});
	}
	new Cubix.Button($('btn-photos-disabled-make-private'), {onClick: function () {
		this.actions.changeType('disabled', 'private');
	}.bind(this), icon: 'key', classes: 'float-left ml5 mb5', caption: 'Make Private'});

	if ( $defined($('btn-photos-disabled-make-public')) ) {
		new Cubix.Button($('btn-photos-disabled-make-public'), {onClick: function () {
			this.actions.changeType('disabled', 'public');
		}.bind(this), icon: 'group', classes: 'float-left ml5 mb5', caption: 'Make Public'});
	}
	
	if ( $defined($('btn-photos-disabled-make-public-soft')) ) {
		new Cubix.Button($('btn-photos-disabled-make-public-soft'), {onClick: function () {
			this.actions.changeType('disabled', 'public-soft');
		}.bind(this), icon: 'group', classes: 'float-left ml5 mb5', caption: 'Make Public'});
	}
	
	if ( $('btn-photos-archived-select-all') ) {
		new Cubix.Button($('btn-photos-archived-select-all'), {onClick: function () {
			this.actions.select_all('Archived');
		}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Select All'});
	}
	if ( $defined($('btn-photos-archived-remove')) ) {
		new Cubix.Button($('btn-photos-archived-remove'), {onClick: function () {
			this.actions.remove();
		}.bind(this), icon: 'delete', classes: 'float-left ml5 mb5', caption: 'Remove'});
	}
	if ( $defined($('btn-photos-archived-make-public-soft')) ) {
		new Cubix.Button($('btn-photos-archived-make-public-soft'), {onClick: function () {
			this.actions.changeType('archived', 'public-soft');
		}.bind(this), icon: 'group', classes: 'float-left ml5 mb5', caption: 'Make Public'});
	}
	if ( $defined($('btn-photos-archived-make-public')) ) {
		new Cubix.Button($('btn-photos-archived-make-public'), {onClick: function () {
			this.actions.changeType('archived', 'public');
		}.bind(this), icon: 'group', classes: 'float-left ml5 mb5', caption: 'Make Public'});
	}
	if ( $defined($('btn-photos-archived-make-disabled')) ) {
		new Cubix.Button($('btn-photos-archived-make-disabled'), {onClick: function () {
			this.actions.changeType('archived', 'disabled');
		}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Disable'});
	}	

	// In Photo History tab
	if ( $defined($('btn-photos-remove-permanently')) ) {
		new Cubix.Button($('btn-photos-remove-permanently'), {onClick: function () {
			this.actions.remove_permanently();
		}.bind(this), icon: 'delete', classes: 'float-left ml5 mb5', caption: 'Remove permanently'});
	}
	if ( $defined($('btn-photos-public-select-all')) ) {
		new Cubix.Button($('btn-photos-public-select-all'), {onClick: function () {
			this.actions.select_all('Photo-history');
		}.bind(this), icon: '', classes: 'float-left ml5 mb5', caption: 'Select All'});
	}

	window.fireEvent('resize');
};

var Cropper = new Class({
	Implements: [Events],
	el: null,
	els: {},
	bind: {},
	wrap:null,
	disabled: false,
	max: {x: 0, y: 0},
	mouse: {start: {x: 0, y: 0}, now: {x: 0, y: 0}, diff: {x: 0, y: 0}},

	moved: false,

	reinitLimits: function () {
		this.max = {
			x: this.els.img.getWidth() - this.el.getWidth(),
			y: this.els.img.getHeight() - this.el.getHeight()
		};
		
	},

	initialize: function (el,imageWidth, imageHeight ) {
		
		this.wrap = $(el);
		this.el = $(el).getElement('.border');
		this.els.img = $(el).getElement('img');
		this.imageWidth = imageWidth;
		this.imageHeight = imageHeight;
		this.els.img.ondragstart = function () {return false;};
		this.wrap.data = this.wrap.data ? this.wrap.data : JSON.decode(this.wrap.get('data'));
		this.url = '/escorts-v2/do-photos';
		if(this.wrap.data.type == "popunder"){
			this.url = '/escorts-v2/crop-popunder';
		}
		var initial = this.wrap.data.args;

		this.els.img.onload = function () {
			this.bind = {
					start: this.handleStart.bindWithEvent(this),
					move: this.handleMove.bindWithEvent(this),
					end: this.handleEnd.bindWithEvent(this)	
				};
			if(this.wrap.data.is_portrait == 1)
			{	
				var y = this.getHeight() - this.imageHeight;
				var x = 0;
			}
			else
			{
				var x = this.getWidth() - this.imageWidth;
				var y = 0;
			}
			this.max = {
				x:x,
				y:y
			};
			
			this.init();
			if ( initial ) {

				this.setInitial(initial.x, initial.y);
			}
			this.el.setStyles({'width':this.imageWidth - 4,'height':this.imageHeight - 4});	
		}.bind(this);
		
	},

	init: function () {
		this.el.addEvent('mousedown', this.bind.start);
	},

	handleStart: function (e) {
		e.stop();

		if ( this.disabled ) return;
		
		document.addEvent('mousemove', this.bind.move);
		document.addEvent('mouseup', this.bind.end)
		
		this.mouse.start = e.page;

		this.mouse.start.x -= this.mouse.diff.x;
		this.mouse.start.y -= this.mouse.diff.y;
	},

	handleMove: function (e) {
		this.moved = true;
		
		this.mouse.now = e.page;

		var x = this.mouse.now.x - this.mouse.start.x,
			 y = this.mouse.now.y - this.mouse.start.y;
		
		this.mouse.diff = {x: x, y: y};

		if ( this.mouse.diff.x < 0 ) this.mouse.diff.x = 0;
		if ( this.mouse.diff.y < 0) this.mouse.diff.y = 0;
		
		if ( this.mouse.diff.x >= this.max.x) {
			this.mouse.diff.x =  this.max.x;
		}

		if (this.mouse.diff.y >= this.max.y ) {
			this.mouse.diff.y = this.max.y;
		}
		
		this.update();
	},

	handleEnd: function (e) {
		document.removeEvent('mousemove', this.bind.move);
		document.removeEvent('mouseup', this.bind.end);

		if ( this.moved ) {
			this.fireEvent('complete', [this.mouse.diff, this.imageWidth, this.imageHeight]);
		}
	},

	update: function () {
		this.mouse.diff.x =  Math.abs(this.mouse.diff.x);
		this.mouse.diff.y = Math.abs(this.mouse.diff.y);
		
		this.el.setStyles({
			left: this.mouse.diff.x,
			top: this.mouse.diff.y
		});
		this.el.getElement('div.border_bg').setStyles({
			'background-position':(-this.mouse.diff.x-2)+'px'+' '+(-this.mouse.diff.y-2)+'px'
		})
	},

	enable: function () {
		this.wrap.setStyles({'opacity': 1,'cursor':'auto'});
		this.el.setStyles({'cursor':'move'});
		this.disabled = false;
	},

	disable: function () {
		this.wrap.setStyles({'opacity': 0.5,'cursor':'wait'});
		this.el.setStyles({'cursor':'wait'});
		this.disabled = true;
	},

	revert: function () {
		this.mouse.diff = this.initial;
		this.update();
	},

	initial: {},


	setInitial: function (x, y) {

		this.mouse.diff = {x: x, y: y};
		this.initial = {x: x, y: y}

		this.update();
	},
	getHeight:function()
	{	
		var ratio = this.els.img.width/this.els.img.naturalWidth; 
		return this.els.img.naturalHeight * ratio;
	},
	getWidth:function()
	{	
		var ratio = this.els.img.height/this.els.img.naturalHeight; 
		return this.els.img.naturalWidth * ratio; 
	}
});


var saveAdjustment = function (args, imageWidth, imageHeight) {
	this.disable();

	args = $extend({
		px: args.x /imageWidth,
		py: args.y / imageHeight,
		'rwidth':imageWidth,
		'rheight':imageHeight
	}, args);
	//console.log(args.x,args.y);
	//console.log(this.wrap.data);
	var resize = this.wrap.data.is_portrait == 0 ? this.els.img.width : this.els.img.height;
	new Request({
		url: this.url,
		method: 'get',
		data: $extend({act: 'adjust', photo_id:this.wrap.data.id,'resize':resize}, args),
		onSuccess: function (resp) {
			resp = JSON.decode(resp);

			if ( resp.error ) {
				alert('An error occured');
				this.revert();
			}
			else {
				this.setInitial(args.x, args.y);
			}

			this.enable();
		}.bind(this)
	}).send()
}