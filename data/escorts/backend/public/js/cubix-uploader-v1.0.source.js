Cubix.Uploader = new Class({
	Implements: [Events, Options],
	
	swf: null,
	
	initialize: function (opt) {
		opt = opt || {};
		this.setOptions(opt);
		
		var self = this;
		
		var up = this.swf = new FancyUpload2($('demo-status'), $('demo-list'), {
			verbose: false,
			url: this.options.url,
			appendCookieData: true,
			zIndex: 9999,
			path: '/js/fancyupload/Swiff.Uploader.swf',
			typeFilter: (this.options.fileFilter == undefined) ? {
				'Images (*.jpg, *.jpeg, *.gif, *.png)': '*.jpg; *.jpeg; *.gif; *.png'
				}: this.options.fileFilter,
			target: $('demo-browse'),
			onLoad: function () {
				this.target.addEvents({
					click: function() {
						return false;
					},
					mouseenter: function() {
						this.addClass('hover');
					},
					mouseleave: function() {
						this.removeClass('hover');
						this.blur();
					},
					mousedown: function() {
						this.focus();
					}
				});
				$('demo-clear').addEvent('click', function() {
					up.remove();
					return false;
				});
				$('demo-upload').addEvent('click', function() {
					up.start();
					return false;
				});
			},
			onSelectSuccess: function () {
				window.fireEvent('resize');
			},
			onFileSuccess: function (file, response) {
				var resp = JSON.decode(response, true) || {};
				self.fireEvent('fileUploaded', [resp]);
				
				if (resp.status == 'success') {
					file.element.addClass('file-success');
					file.remove();
					// file.info.set('html', '<strong>Image was uploaded.</strong>');
				}
				else {
					file.element.addClass('file-failed');
					file.info.set('html', /*'<strong>An error occured:</strong>' + ' ' + */resp.msg);
				}
			},
			onComplete: function() {
				self.fireEvent('allfilesUploaded');
			},
			onFail: function(error) {
				switch (error) {
					case 'hidden':
						alert('To enable the embedded uploader, unblock it in your browser and refresh (see Adblock).');
						break;
					case 'blocked':
						alert('To enable the embedded uploader, enable the blocked Flash movie (see Flashblock).');
						break;
					/*case 'empty':
						alert('A required file was not found, please be patient and we fix this.');
						break;*/
					case 'flash':
						alert('To enable the embedded uploader, install the latest Adobe Flash plugin.')
				}
			}
		});
	},
	
	setData: function (_data) {
		var data = this.swf.options.data || {};
		this.swf.setOptions({ data: $extend(data, _data) });
	},
	
	destroy: function () {
		this.swf.box.destroy();
		this.swf.toElement().destroy();
	}
});

/*var ScriptsLoadComplete = function () {
	
};*/

/*window.addEvent('domready', function() { // wait for the content
	var counter = 0;
	var loadCallback = function () {
		if ( 3 <= ++counter ) {
			ScriptsLoadComplete.call();
		}
	};
	
	new Asset.javascript('/js/fancyupload/Swiff.Uploader.js', { onload: loadCallback });
	new Asset.javascript('/js/fancyupload/Fx.ProgressBar.js', { onload: loadCallback });
	new Asset.javascript('/js/fancyupload/FancyUpload2.js', { onload: loadCallback });
});*/
