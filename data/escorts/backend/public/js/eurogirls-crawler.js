$(document).ready(function(){
	var esc_data = {};
	var finished = false;
	var page = 1;
	var link = '';
	var phones = {};
	var count = 0;

	fetchTourEscorts(page, finished)

	function fetchTourEscorts(page, finished){
		if(!finished){
			var data = [];
			fetch('custom-crawl-data?page='+page)
			.then(response => response.json())
			.then(html => {
				$('#eurogirls').html(html);
				var new_html = $('#eurogirls #main-content').html()
				$('#eurogirls').html(new_html);
				$('#eurogirls .headline').remove();
				$('#eurogirls .about').remove();
				$('#eurogirls .filter-country-city').remove();
				$('#eurogirls .js-nextprev-pagination').remove();
				$('#eurogirls .list-items').children('div').each(function(item){
					link = $(this).find('.js-nextprev-item').attr('href');
					data.push(link)
				})
			})
			.then(() => {
				console.log(data)
				console.log('page '+page+' data length '+ data.length)
					if(data.length > 0){
						data.forEach(function(link, index){
							parsePage(link, index);
						})
					} else {
						fetchTourEscorts(++page, true);
					}
			})
			.then(() => {
				setTimeout(function(){
					fetchTourEscorts(++page, finished);
				// fetchTourEscorts(++page, true);
				}, 250000)
			})
		} else {
			console.log(esc_data)
			console.log('finished')
			console.log('total escorts parsed '+ esc_data.length)
			console.log(phones)
			$.ajax({
				type: "POST",
				url: 'custom-crawl-csv',
				data: {
					data: esc_data
				},
				success: function(r){
					alert('Escort tours have been imported');
					window.location.href = "/importer"
				}
			})
		}
	}

	function parsePage(link, index){

		setTimeout(function(){

			fetch('custom-crawl-post?link='+link)
			.then(response => response.json())
			.then(n_h => {
				$('#eurogirls').html(n_h);
				$('#eurogirls #js-over18').remove();
				$('#eurogirls .overlay').remove();
			})
			.then(() => {
				setTimeout(function(){
					$('.js-phone').click();
				}, 2000)
			})
			.then(() => {
				setTimeout(function(){
					let d = $('#eurogirls').find('.tours div').html();
					let esc_d = [];
					if(d != undefined){
						d.split('<br>').each(function(str){
							if(str.length > 3){
								let date = str.split('\nin\n');
								if(date.length > 1){
									let start = date[0].substring(
											date[0].indexOf("m") + 2,
											date[0].lastIndexOf(" to")
										).trim();
									let end  = date[0].split(' to')[1].trim();
									let city = date[1].substring(
											date[1].indexOf("in ") + 1,
											date[1].lastIndexOf(",")
										).trim();
									let obj = {
										start: start,
										end: end,
										city: city
									};
									esc_d.push(obj);
								}	
							}
						})
					}
					if(esc_d.length > 0){
						let phone = $('.js-phone:first').attr('href');
						// if(phone != undefined && phone != '#'){
							// esc_d.push({phone: phone.split('tel:+')[1]})
							// esc_data[link] = esc_d;
						// } else {
							// esc_d.push({phone: null})
						// }
						console.log(esc_d)
						esc_data[link] = esc_d;
						console.log(link+' link '+ phone+' count '+ ++count)

					}
				}, 3000)
			})
		},5000*index*page)
	}
})