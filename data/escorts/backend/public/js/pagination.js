window.pgCurrentpage = 1;

window.changePage = function (page, numPages)
{
    window.getHistoryPhotos($('ph_escort_id').get('value'), false, page);
    document.querySelector(`.pg-num-${window.pgCurrentpage}`).addClass('pg-active');

    let pg_first = document.getElementById("pg-first"),
        pg_prev = document.getElementById("pg-prev"),
        pg_next = document.getElementById("pg-next"),
        pg_last = document.getElementById("pg-last");

    if (page < 1) page = 1;
    if (page > numPages) page = numPages;

    if (page === 1) {
        pg_prev.classList.add('pg-disabled');
        pg_first.classList.add('pg-disabled');
    } else {
        pg_prev.classList.remove('pg-disabled');
        pg_first.classList.remove('pg-disabled');
    }

    if (page === numPages) {
        pg_next.classList.add('pg-disabled');
        pg_last.classList.add('pg-disabled');
    } else {
        pg_next.classList.remove('pg-disabled');
        pg_last.classList.remove('pg-disabled');
    }
}

$(document.body).addEvent("click:relay(#pager .pg-item)", function(event) {
    if (!this.hasClass('pg-disabled')) {
        const numPages = document.getElementById('pg-pages-count').value;

        this.getParent('ul').getElements('li').removeClass('pg-active');

        let elemId = event.target.closest('li').getAttribute('id');

        if (elemId === 'pg-prev') {
            if (window.pgCurrentpage > 1) {
                window.pgCurrentpage -= 1;
                event.target.setAttribute('data-page', window.pgCurrentpage);
            }
        } else if (elemId === 'pg-next') {
            if (window.pgCurrentpage < numPages) {
                window.pgCurrentpage += 1;
                event.target.setAttribute('data-page', window.pgCurrentpage);
            }
        } else {
            if (elemId !== 'pg-first' && elemId !== 'pg-last') {
                this.addClass('pg-active');
            }
            window.pgCurrentpage = parseInt(event.target.getAttribute('data-page'));
        }

        window.changePage(window.pgCurrentpage, numPages);
    }
});
