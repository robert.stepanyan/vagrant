Cubix.SelectBox = new Class({
	Implements: [Options, Events],

	options: {
		disabled: false,
		name: '',
		values: [
			{ value: 1, label: 'EUR' },
			{ value: 2, label: 'USD' },
			{ value: 3, label: 'GBP' },
			{ value: 4, label: 'CHF' }
		],
		selected: 1
	},

	element: {},
	list: {},
	input: {},

	selected: {},

	disable: function () {
		this.element.addClass('cx-selectbox2-disabled');
	},

	enable: function () {
		this.element.removeClass('cx-selectbox2-disabled');
	},

	getValue: function (value) {
		var found = false;
		this.options.values.each(function (i) {
			if ( found ) return;
			if ( value == i.value ) {
				found = i;
			}
		});

		return found;
	},

	initialize: function (o) {
		this.setOptions(o || {});
		
		this.render();

		this.bind = {
			mouseenter: this.onMouseEnter.bindWithEvent(this),
			mouseleave: this.onMouseLeave.bindWithEvent(this),
			click: this.onClick.bindWithEvent(this),
			select: this.onSelect.bindWithEvent(this),
			hide: this.onHide.bindWithEvent(this)
		};

		this.attach();

		if ( this.options.disabled ) {
			this.disable();
		}
	},

	render: function () {
		this.element = new Element('span', {
			'class': 'cx-selectbox2'
		});

		this.current = new Element('em', {
			html: this.getValue(this.options.selected).label
		}).inject(this.element);

		this.list = new Element('div', {
			'class': 'cx-selectbox2-list hide'
		}).inject(this.element);

		this.options.values.each(function (i, j) {
			var el = new Element('div', {
				html: i.label
			}).inject(this.list);
			el.store('value', i);
			if ( j == this.options.selected - 1 ) {
				el.addClass('selected');
			}
		}.bind(this));

		this.input = new Element('input', {
			type: 'hidden',
			name: this.options.name,
			value: this.getValue(this.options.selected).value
		}).inject(this.element);
	},

	onMouseEnter: function (e) {

	},

	onMouseLeave: function (e) {

	},

	onClick: function (e) {
		if ( this.element.hasClass('cx-selectbox2-disabled') ) return;
		e.stop();
		if ( this.list.hasClass('hide') ) {
			this.element.addClass('cx-selectbox2-pushed');
			this.list.removeClass('hide');
			$(document.body).addEvent('click', this.bind.hide);
		}
		else {
			this.onHide({});
		}
	},

	onSelect: function (e) {
		this.list.getElements('div').removeClass('selected');
		this.select(e.target);
	},

	onHide: function (e) {
		this.element.removeClass('cx-selectbox2-pushed');
		this.list.addClass('hide');
		$(document.body).removeEvent('click', this.bind.hide);
	},

	attach: function () {
		this.element.addEvents({
			mouseenter: this.bind.mouseenter,
			mouseleave: this.bind.mouseleave,
			click: this.bind.click
		});

		this.list.getElements('div').addEvents({
			click: this.bind.select
		});
	},

	select: function (el) {
		var value = el.retrieve('value');
		this.input.set('value', value.value);
		this.current.set('html', value.label);
		this.selected = value;
		el.addClass('selected');
	},

	getElement: function () {
		return this.element;
	}
});