Cubix.Shadow = new Class({
	Implements: [Events, Options],
	
	el: null,
	
	options: {
		classPrefix: 'cx-shadow-',
		imgUrl: '',
		color: 'white'
	},
	
	initialize: function (el, options) {
		this.el = $(el);
		
		options = options || {};
		this.setOptions(options);
		
		this._decorate(this.el);
	},
	
	_decorate: function (el) {
		//var wr = this._createElement(this.options.color).wraps(el);
		
		var tbl = new Element('table', { 'class': this._getClass() + ' ' + this._getClass(this.options.color) });
		var topTr = new Element('tr', { 'class': this._getClass('row-top') }).inject(tbl);
		
		new Element('td', { 'class': this._getClass('top-left') }).inject(topTr);
		new Element('td', { 'class': this._getClass('top') }).inject(topTr);
		new Element('td', { 'class': this._getClass('top-right') }).inject(topTr);
		
		var midTr = new Element('tr', { 'class': this._getClass('row-mid') }).inject(tbl);
		
		new Element('td', { 'class': this._getClass('left') }).inject(midTr);
		var contTd = new Element('td', { 'class': this._getClass('wrapper') }).inject(midTr);
		new Element('td', { 'class': this._getClass('right') }).inject(midTr);
		
		var botTr = new Element('tr', { 'class': this._getClass('row-bot') }).inject(tbl);
		
		new Element('td', { 'class': this._getClass('bottom-left') }).inject(botTr);
		new Element('td', { 'class': this._getClass('bottom') }).inject(botTr);
		new Element('td', { 'class': this._getClass('bottom-right') }).inject(botTr);

		this.tbl = tbl;

		tbl.inject(el.getParent());
		el.inject(contTd);
	},
	
	_getClass: function (what) {
		return (what ? this.options.classPrefix + what : this.options.classPrefix.substring(0, this.options.classPrefix.length - 1));
	},
	
	_createElement: function (what, type) {
		type = type || 'div';
		return new Element(type, {
			'class': this.options.classPrefix + what
		});
	}
});
