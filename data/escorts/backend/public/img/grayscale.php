<?php
$file = @$_GET['i'];
$file = preg_replace('#^/img/#', '', $file);

$ext = @end(explode('.', $file));
$gd_ext = _gdExtFromFileExt($ext);

header('Content-type: ' . $gd_ext['mime']);

list($width, $height) = getimagesize($file);
$func = 'imagecreatefrom' . $gd_ext['ext'];
$source = $func($file);

imagealphablending($source, false);
imagesavealpha($source, true);

imagefilter($source, IMG_FILTER_GRAYSCALE);
imagefilter($source, IMG_FILTER_BRIGHTNESS, 10);

$func = 'image' . $gd_ext['ext'];
$func($source);

function _gdExtFromFileExt($file_ext) {
	switch ($file_ext) {
		case 'jpg':
		case 'jpeg':
			$mime = 'image/jpeg';
			$ext = 'jpeg';
		break;
		case 'png':
			$mime = 'image/png';
			$ext = 'png';
		break;
		case 'gif':
			$mime = 'image/gif';
			$ext = 'gif';
		break;
	}

	if ( ! isset($mime) || ! isset($ext) ) {
		throw new Exception('Unknown extension ' . $file_ext);
	}

	return array('mime' => $mime, 'ext' => $ext);
}
