<?php

class AnalyticsController extends Zend_Controller_Action
{
	private $model ;
	protected $user;
	public $config;
	private $app_id;
	public function init()
	{	
		Model_Analytics::$archive = $this->_request->archive;
		$this->model = new Model_Analytics();
		$this->user = Zend_Auth::getInstance()->getIdentity();
		$this->app_id = Cubix_Application::getId();

	}
	

	public function indexAction()
	{	
		$configs = Zend_Registry::get('analytics_config');
		$link = mysql_connect($configs['host'],$configs['username'],$configs['password']) or die('Error With Mysql  Connection(Analytics): '.mysql_error());
		mysql_select_db($configs['dbname'],$link) or die('Error With Selecting Analytics DB: '.$configs['dbname'].' error: '. mysql_error($link));
		$this->view->tables = mysql_query("SHOW TABLES FROM {$configs['dbname']} ");
		
	}
	public function getGroupsAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		if(isset($this->_request->filter) && ($filter = trim($this->_request->filter))!='')
		{
	    	$data = $this->model->GetGroups($filter,$this->app_id);
			die(json_encode($data));
		}
		die;
	}
	public function dataAction()
	{	
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$req = $this->_request;
		if(isset($req->select_filter))
		{	
			$sort = isset($req->sort_field)?$req->sort_field:'quantity';
			$dir = isset($req->sort_dir) && $sort!='' && in_array($req->sort_dir,array('asc','desc'))?$req->sort_dir:'DESC';
			$page = isset($req->page) && is_numeric($req->page)?intval($req->page):1;
			$per_page = isset($req->per_page) && is_numeric($req->per_page)?intval($req->per_page):10;
			$arr = array();
			$arr['application_id'] = $this->app_id;
			if($req->select_filter != '-all-'){
				$arr['filter'] = $req->select_filter;
			} 
			$arr['group'] = $req->select_group;
			$arr['text_value'] = $req->text_value;
			$arr['value'] = $req->value;
			$arr['user_id'] = $req->user_id;
			$arr['date_from'] = $req->select_date_from;
			$arr['date_to'] = $req->select_date_to;
			$data = $this->model->GetFilterStatistic($this->model->CreateWhere($arr),$page,$per_page,$sort,$dir);
			die(json_encode($data));
		}
	}
	public function editAction()
	{	
		$this->view->layout()->disableLayout();
	}

	public function groupDetailsViewAction()
	{
		$this->view->layout()->disableLayout();
	}


	public function getDetailAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$req = $this->_request;
		if(isset($req->group) && isset($req->filter) && isset($req->date) && isset($req->name))
		{	
			$sort = isset($req->sort_field)?$req->sort_field:'year_date';
			$dir = isset($req->sort_dir) && $sort!='' && in_array($req->sort_dir,array('asc','desc'))?$req->sort_dir:'DESC';
			$page = isset($req->page) && is_numeric($req->page)?intval($req->page):1;
			$per_page = isset($req->per_page) && is_numeric($req->per_page)?intval($req->per_page):10;
			$arr['application_id'] = $this->app_id;
			$arr['filter'] = $req->filter;
			$arr['group'] = $req->group;
			$arr['text_value'] = $req->item;
			$arr['group'] = $req->group;
			$arr['country'] = $req->country;
			$arr['year_date'] = $req->date;
			$arr['name'] = $req->name;
			$arr['value'] = $req->item_value;
			$data = $this->model->GetDetail($this->model->CreateWhere($arr),$page,$per_page,$sort,$dir);
			die(json_encode($data));
		}
		else
		{
			die;
		}
	}
	
	public function groupdataAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$req = $this->_request;
		if(isset($req->group) && isset($req->filter))
		{	
			$arr['application_id'] = $this->app_id;
			$arr['filter'] = $req->filter;
			$arr['group'] = $req->group;
			$arr['text_value'] = $req->text_value;
			$arr['value'] = $req->value;
			$arr['user_id'] = $req->user_id;
			$arr['date_from'] = $req->select_date_group_from;
			$arr['date_to'] = $req->select_date_group_to;
			$sort = isset($req->sort_field)?$req->sort_field:'quantity';
			$dir = isset($req->sort_dir) && $sort!='' && in_array($req->sort_dir,array('asc','desc'))?$req->sort_dir:'DESC';
			$page = isset($req->page) && is_numeric($req->page)?intval($req->page):1;
			$per_page = isset($req->per_page) && is_numeric($req->per_page)?intval($req->per_page):10;	
			$data = $this->model->GetGroupItemStatistic($this->model->CreateWhere($arr),$page,$per_page,$sort,$dir);
			echo json_encode($data);
		}
	}

	
	
}

