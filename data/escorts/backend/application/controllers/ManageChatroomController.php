<?php

/**
 * Class ManageChatroomController
 *
 * @author Eduard Hovhannisyan
 */
class ManageChatroomController extends Zend_Controller_Action
{
    /**
     * @var Model_Chatroom
     */
    protected $model;

    public function init()
    {
        $this->model = new Model_Chatroom();
    }

    public function indexAction()
    {

    }

    public function dataAction()
    {
        $req = $this->_request;
        $filter = array(
            'id' => $req->id,
            'query' => $req->query,
            'username' => $req->username,
            'date_from' => $req->date_from,
            'date_to' => $req->date_to,
        );

        $count = 0;
        $data = $this->model->getConversations(
            $req->page,
            $req->per_page,
            $filter,
            $req->sort_field,
            $req->sort_dir,
            $count
        );

        echo json_encode(array('data' => $data, 'count' => $count));
        die;
    }

    public function threadAction()
    {
        $this->view->layout()->disableLayout();
        $threadId = intval($this->_getParam('id'));
        $page = intval($this->_getParam('page'));

        if ($page <= 0) $page = 1;
        $perPage = 10;

        $data = $this->model->getMessagesByThreadId($threadId, $page, $perPage);

        $this->view->page = $page;
        $this->view->threadId = $threadId;
        $this->view->perPage = $perPage;
        $this->view->messages = $data['messages'];
        $this->view->totalMessagesCount = $data['totalCount'];
        $this->view->hasMoreMessages = $data['hasMore'];
        $this->view->attachmentsHost = APPLICATION_ENV === 'development' ? 'http://beneluxxx.com.test:8080/chatroom/attachments/' : 'https://www.beneluxxx.com/chatroom/attachments/';
    }


    protected function accumulateArchiveAction()
    {
        $this->model->archiveOldMessages();
    }
}
