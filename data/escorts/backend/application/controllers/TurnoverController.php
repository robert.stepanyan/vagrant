<?php

class TurnoverController extends Zend_Controller_Action {
	public function init()
	{
		$this->model = new Model_Payments();
	}
	
	public function indexAction() 
	{
		$this->view->defined_dates = array('today' => 'Today',
														'yesterday' => 'Yesterday',
														'last_7_days' => 'Last 7 days',
														'this_month' => 'This month',
														'last_month' => 'Last month'
												);
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		$success_statuses = Cubix_CGP_Callback::$success_statuses;
		$rec_statuses =  Cubix_CGP_Callback::$recurring_statuses;
		$filter = array(
			'date_from' => $req->date_from,
			'date_to' => $req->date_to,
			'member_name' => $req->member_name
		);
		$turnover = Model_Currencies::getDefaultCurrency()->title.' '. $this->model->getTurnover($filter) / 100;
		if (isset($req->transaction)){
			$filter['trans-success'] = 1;
		}
		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter,
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		foreach ( $data as $i => $item ) {
			$data[$i]->status_success = in_array($item->status, $success_statuses) ? 1 : 0;
			$data[$i]->status_rec = $item->amount == 995 ? 1 : 0;
		}
		$this->view->data = array(
			'data' => $data,
			'count' => $count,
			'turnover' => $turnover
		);
		
		
	
	}
	
}
