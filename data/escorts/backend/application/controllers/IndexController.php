<?php

class IndexController extends Zend_Controller_Action {
	/**
	 * The default action - show the home page
	 */
	public function indexAction()
	{
		
		//$this->_redirect($this->view->baseUrl() . '/escorts');
	}

	public function loginChatAction()
	{
		$this->view->layout()->disableLayout();

		$login = $this->_request->login;
		$password = $this->_request->password;

		if ( ! $password ) die;
		
		$auth = new Model_Auth_Adapter($login, $password, Cubix_Application::getId());
		$user_id = $auth->chatLoginAuth();

		if ( $user_id ) {
			$xml = '<login result="OK">';
		}
		else {
			$xml = '<login result="FAIL" error="error_authentication_failed">';
		}
		
		$xml .= '<userData>';
		$xml .= '<id>' . $user_id . '</id> ';
		$xml .= '<name><![CDATA[' . $login . ']]></name>';
		$xml .= '<level>regular</level>';
		$xml .= '</userData>';
		$xml .= '<friends> </friends>';
		$xml .= '<blocks> </blocks>';
		$xml .= '</login>';
		
		echo $xml;
		
		header("Content-type: text/xml; charset: UTF-8");
		
		exit;
	}

	public function quickEmailAction()
	{
		$this->view->layout()->disableLayout();
		
		$req = $this->_request;
		
		$email = $this->view->email = $req->email;
		$ids = $this->view->ids = $req->id;
		$user_ids = $this->view->user_ids = $req->user_id;
		$agency_ids = $this->view->agency_ids = $req->agency_id;
		$from = $req->from;
		$from_name = $req->from_name ? : null;
		$subject = $req->subject;
		$message = $req->message;
		$email_signature = $req->email_signature;
		$use_email_signature = $req->use_email_signature;
        $is_member = $req->member;
        $is_escort = $req->escort;
        $is_agency = $req->agency;
        if ($is_member)
        {
            $user_type = 'member';
        }elseif ($is_agency)
        {
            $user_type = 'agency';
        }elseif ($is_escort)
        {
            $user_type = 'escort';
        }


		$validator = new Cubix_Validator();

		$bu_model = new Model_BOUsers();
		$sales_user_name = $this->view->sales_user_name = $req->sales_user_name;
		$bu_user = Zend_Auth::getInstance()->getIdentity();

		$this->view->email_signature = $bu_model->get($bu_user->id)->email_signature;

		if ( $email_signature != $this->view->email_signature ) {
			$bu_model->update($bu_user->id, array('email_signature' => $email_signature));
		}

		if ( $req->isPost() ) {
			if ( $from ) {
				if (!$validator->isValidEmail($from))
					$validator->setError('from', 'Email is not valid');
			}
			else
				$from = null;

			if ( ! strlen($subject) ) {
				$validator->setError('subject', 'Required');
			}

			if ( ! strlen($message) ) {
				$validator->setError('message', 'Required');
			}

			$email_data = array(
				'subject' => $subject,
				'message' => $message,
				'signature' => ''
			);
			
			if ( $use_email_signature ) {
				$email_data['signature'] = $email_signature;
			}

			if ( $validator->isValid() ) {
				setcookie('quick_email', $from, time() + 365 * 24 * 60 * 60 /* one year */, '/', Cubix_Application::getById()->backend_url);
				$m = new Model_Users();
				$me = new Model_SMS_EmailOutbox();

				if($email){
					Cubix_Email::sendTemplate('universal_message', $email, $email_data, $from, $from_name);
					$u = $m->getByEmail($email);

					$data = array(
						'email' => $email,
						'subject' => $subject,
						'text' => $message,
						'backend_user_id' => $bu_user->id,
						'date' => new Zend_Db_Expr('NOW()'),
						'user_id' => $u->id,
						'application_id' => Cubix_Application::getId()
					);
					
					$me->add($data);
				}
				elseif($ids){
										
					foreach($ids as $id){
						$u = $m->getByEscortId($id);
						
						Cubix_Email::sendTemplate('universal_message', $u->email, $email_data, $from, $from_name);
						$data = array(
							'email' => $u->email,
							'subject' => $subject,
							'text' => $message,
							'backend_user_id' => $bu_user->id,
							'date' => new Zend_Db_Expr('NOW()'),
							'user_id' => $u->id,
							'application_id' => Cubix_Application::getId()
						);
					
						$me->add($data);
					}
				}
				elseif($user_ids){
						
					foreach($user_ids as $id){
						$u = $m->get($id);
						
						Cubix_Email::sendTemplate('universal_message', $u->email, $email_data, $from, $from_name);
						$data = array(
							'email' => $u->email,
							'subject' => $subject,
							'text' => $message,
							'backend_user_id' => $bu_user->id,
							'date' => new Zend_Db_Expr('NOW()'),
							'user_id' => $u->id,
							'application_id' => Cubix_Application::getId()
						);
					
						$me->add($data);
					}
				}
				elseif(count($agency_ids)){
					foreach($agency_ids as $id){
						$u = $m->getByAgencyId($id);
						
						Cubix_Email::sendTemplate('universal_message', $u->email, $email_data, $from, $from_name);
						$data = array(
							'email' => $u->email,
							'subject' => $subject,
							'text' => $message,
							'backend_user_id' => $bu_user->id,
							'date' => new Zend_Db_Expr('NOW()'),
							'user_id' => $u->id,
							'application_id' => Cubix_Application::getId()
						);
					
						$me->add($data);
					}
				}
			}

			die(json_encode($validator->getStatus()));
		}
		else
		{
			$this->view->from_email = $_COOKIE['quick_email'];
			$m_et = new Model_SMS_EmailTemplates();
			$bu = null;
			
			if (!in_array($bu_user->type, array('admin', 'superadmin', 'data entry plus')))
				$bu = $bu_user->id;
			
			$this->view->templates = $m_et->getList($bu,$user_type);
		}
	}
	
	public function countriesAction()
	{
		//
	}

	public function regionsAction()
	{

	}

	public function citiesAction()
	{

	}

	public function cityzonesAction()
	{

	}
	
	public function restoreAction()
	{
		/*Model_Escorts::restore();*/
		echo 'done';die;
	}
	
	public function goAction()
	{
		$link = $_SERVER['QUERY_STRING'];
		if ( ! preg_match('#^http://#', $link) ) $link = 'http://' . $link;
		
		header('Location: ' . $link);
		die;
	}
	
	public function chatStatusAction()
	{		
		$user_id = $this->_request->user_id;
		$conf = Zend_Registry::get('system_config');
		$key = $conf['chat']['key'];
		
		if ($this->_request->isPost()) 
		{
			$status = $this->_request->status;
			$hash = md5($user_id . '-' . $status . '-' .  $key);
			file_get_contents(Cubix_Application::getById()->url . '/node-chat/change-availability?userId=' . $user_id . '&availability=' . $status . '&hash=' . $hash);
			
			$status = !$status;
			echo intval($status);
		}
		else
		{
			$hash = md5($user_id . '-' . $key);
			$st = file_get_contents(Cubix_Application::getById()->url . '/node-chat/get-availability?userId=' . $user_id . '&hash=' . $hash);
			$st = json_decode($st);
			
			if ($st->availability === 1)
				echo 0;
			elseif ($st->availability === 0)
				echo 1;
			else
				echo 2;
		}
		
		die;
	}


	public function testMembersEmailsAction()
	{
		$this->view->layout()->disableLayout();

		$template_id = 'follow_alert_v3';
		$html = $this->view->render('index/test-members-email-en.phtml');
		Cubix_Email::sendTemplate($template_id, 'tigranyeng@gmail.com', array('html' => $html));

		$html = $this->view->render('index/test-members-email-it.phtml');
		Cubix_Email::sendTemplate($template_id, 'tigranyeng@gmail.com', array('html' => $html));

		$html = $this->view->render('index/test-members-email-fr.phtml');
		Cubix_Email::sendTemplate($template_id, 'tigranyeng@gmail.com', array('html' => $html));
		var_dump('sent');die;
	}

}
