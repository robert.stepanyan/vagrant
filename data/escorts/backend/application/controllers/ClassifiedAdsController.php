<?php

class ClassifiedAdsController extends Zend_Controller_Action
{
	protected $_session;
	
	public function init()
	{
		$this->model = new Model_ClassifiedAds();
		
		$this->_session = new Zend_Session_Namespace('classified-ads-backend');
		
		/*if( empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
			if ( ! $_GET['dd'] ) die('Classified ads is temporarily unavailable!');
		}*/
	}
	
	public function indexAction() 
	{
		
	}
	
	public function adsDataAction()
	{
		$req = $this->_request;
		$auth = Zend_Auth::getInstance();
		$be_user = $auth->getIdentity();
		

		
		$filter = array(
			'application_id' => $req->application_id,
			'ca.id' => $req->id,
			'ca.title' => $req->title,
			'ca.text' => $req->text,
			'ca.phone' => $req->phone,
			'ca.email' => $req->email,
			'ca.ip' => $req->ip,
			'ca.status' => $req->status,
			'ca.category_id' => $req->category_id,
			'ca.city_id' => $req->city_id,
			'ca.period' => $req->period,
			'ca.is_premium' => $req->is_premium,
			'ca.whatsapp_available' => $req->whatsapp_available,
			'ca.sms_available' => $req->sms_available,

			'ct.country_id' => $req->country,
			'ct.city_id' => $req->city,
			
			'filter_by' => $req->filter_by,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to,
		);
		
		$data = $this->model->getAllAds(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);

		$DEFINITIONS = Zend_Registry::get('defines');
		
		$statuses = array(
			Model_ClassifiedAds::AD_STATUS_PENDING => 'PENDING',
			Model_ClassifiedAds::AD_STATUS_ACTIVE => 'APPROVED',
			Model_ClassifiedAds::AD_STATUS_EXPIRED => 'EXPIRED'
		);
		
		$arr_key = 'classified_ad_categories';
		if ( Cubix_Application::getId() == APP_BL ) {
			$arr_key = 'classified_ad_categories_bl';
		}
		elseif (Cubix_Application::getId() == APP_ED)
		{
			$arr_key = 'classified_ad_categories_ed';
		}
		
		$helper = $this->view->getHelper('Geoip');
		$model_escorts = new Model_EscortsV2();


//		foreach ( $data as $i => $item ) {
//			// if( $be_user->id == 70){
//			// 	var_dump($item);die;
//			// }
//			$data[$i]->category_title = $DEFINITIONS[$arr_key][$item->category_id];
//			$data[$i]->status_title = $statuses[$item->status];
//			$data[$i]->duration_title = $DEFINITIONS['classified_ad_durations'][$item->period];
//			$data[$i]->ip = mb_convert_encoding($helper->Geoip($item->ip), "UTF-8");
//			//trim($item->phone)
//			if(false ){
//				$phone_number = preg_replace('/[^0-9]+/', '',$item->phone);
//				$matched_ids = $model_escorts->getIdByPhoneLike($phone_number);
//				if(count($matched_ids) > 1){
//					$data[$i]->phone_escort_id = "Multiple";
//				}
//				elseif(count($matched_ids) == 1){
//					$data[$i]->phone_escort_id = $matched_ids[0]->id;
//				}
//				else{
//					$data[$i]->phone_escort_id = "";
//				}
//			}
//			else{
//				$data[$i]->phone_escort_id = "";
//			}
//
//		}
		
		if ($req->gallery_view) {
			foreach ( $data as $i => $item ) {
				$classified_ad = $this->model->getAdById($item->id);
				$photos = $classified_ad['photos'];

				$this->view->data_photos = $photos;
				$html = $this->view->render('classified-ads/data-photos.phtml');
				$item->photos_html = $html;
			}
		}
		

		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
	
	public function adsAction()
	{
		$arr_key = 'classified_ad_categories';
		if ( Cubix_Application::getId() == APP_BL ) {
			$arr_key = 'classified_ad_categories_bl';
		}
		elseif ( Cubix_Application::getId() == APP_ED ) {
			$arr_key = 'classified_ad_categories_ed';
		}
		$this->view->arr_key = $arr_key;
		
		$m_cities = new Model_Geography_Cities();
		$non_country_apps = array(30, 11);
		if (in_array(Cubix_Application::getId(), $non_country_apps) ) {
			$this->view->cities = $m_cities->ajaxGetAllCountryByAppId(Cubix_Application::getId());
		} 
		elseif (Cubix_Application::getId() == APP_A6)  {
			$m_cantons = new Model_Cantons;
			$this->view->cities = $m_cantons->getAll();
		} 
		else {
			$this->view->cities = $m_cities->ajaxGetAll(null, Cubix_Application::getById()->country_id);
		}
	}
	
	public function adCreateAction()
	{		
		$arr_key = 'classified_ad_categories';
		if ( Cubix_Application::getId() == APP_BL ) {
			$arr_key = 'classified_ad_categories_bl';
		}
		elseif ( Cubix_Application::getId() == APP_ED ) {
			$arr_key = 'classified_ad_categories_ed';
		}

		if(Cubix_Application::getId() == APP_EG_CO_UK){
            $country_model = new Model_Countries();
            $this->view->countries = $country_model->getPhoneCountries();
        }

		$this->view->arr_key = $arr_key;
		
		$req = $this->_request;
		$m_cities = new Model_Geography_Cities();
		$non_country_apps = array(30, 11);
		if (in_array(Cubix_Application::getId(), $non_country_apps) ) {
			$this->view->cities = $m_cities->ajaxGetAllCountryByAppId(Cubix_Application::getId());
		} 
		elseif(Cubix_Application::getId() == APP_A6){
			$m_cantons = new Model_Cantons;
			$this->view->cities = $m_cantons->getAll();
			$m_countries = new Model_Countries();
			$this->view->phone_prefixes = $phone_prefixes = $m_countries->getPhonePrefixs();
		}
		else {
			$this->view->cities = $m_cities->ajaxGetAll(null, Cubix_Application::getById()->country_id);
		}
		
		if ( $req->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);

			$params = array(
				'category_id' => 'int',
				//'city_id' => 'int',
				'phone' => '',
				'email' => '',
				'period' => 'int',
				'title' => '',
				'text' => '',
			);

            if(Cubix_Application::getId() == APP_EG_CO_UK){
                $params['country_prefix'] = 'int';
            }

             if(Cubix_Application::getId() == APP_A6){
                $params['web'] = '';
                $params['sms_available'] = '';
                $params['whatsapp_available'] = '';
                $params['phone_prefix_id'] = '';
            }

			$data->setFields($params);
			
			$data = $data->getData();

			if(Cubix_Application::getId() == APP_A6){
                $data['whatsapp_available'] = $data['whatsapp_available'] == 'on' ? 1 : 0;
                $data['sms_available'] = $data['sms_available'] == 'on' ? 1 : 0;
            }		
            	
			$data['cities'] = $req->cities;
			
			$validator = new Cubix_Validator();
			
			if ( ! $data['category_id'] ) {
				$validator->setError('category_id', 'Required');
			}
			
			if ( ! count($data['cities']) ) {
				$validator->setError('city_id', 'Required');
			}
			
			if ( ! strlen(trim($data['title'])) ) {
				$validator->setError('title', 'Required');
			}
			
			$_text = strip_tags(trim($data['text']));
			$_text = str_replace('&nbsp;', '', $_text);
			$_text = preg_replace('/\s\s+/', '', $_text);
				
			if ( ! strlen($_text) ) {
				$validator->setError('text', 'Required');
			}
			
			if ( ! strlen($data['phone']) && ! strlen($data['email']) ) {
				$validator->setError('phone', 'Email or Phone Required');
			}

			if(Cubix_Application::getId() == APP_A6 && strlen($data['phone'])){

				$contact_phone = $data['phone'];
				$phone_prefix_id = $data['phone_prefix_id'];
				$phone_prefix = $m_countries->getPhonePrefixById($phone_prefix_id);
				$ndd_prefix = $m_countries->getPhoneNddPrefixById($phone_prefix_id);
				$phone_parsed = null;
				if ($contact_phone ) {
					if ( ! preg_match("/^[0-9\s\+\-\(\)]+$/i", $contact_phone) ) {
						$validator->setError('phone', 'Invalid phone number');
					}
					elseif(preg_match("/^(\+|00)/", $contact_phone) ) {
						$validator->setError('phone', 'Enter phone without prefix');
					}

				}
				$phone_parsed = preg_replace('/[^0-9]+/', '', $contact_phone);
				$phone_parsed = preg_replace('/^'.$ndd_prefix.'/', '', $phone_parsed);
				$phone_parsed = '+'.$phone_prefix.$phone_parsed;
				$data['phone_parsed'] = $phone_parsed;

			}
			if ( strlen($data['email']) && ! $validator->isValidEmail($data['email']) ) {
				$validator->setError('email', 'Invalid Email');
			}

			if (Cubix_Application::getId() == APP_A6 && strlen($data['web']) && (! preg_match("/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i", $data['web'])  && ! preg_match("/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i", 'http://' . $data['web'])) ){
				$validator->setError('web', 'Invalid Web URL');
			}

			if ( $validator->isValid() ) {
				
				$data['status'] = Model_ClassifiedAds::AD_STATUS_ACTIVE;			
				$data['approvation_date'] = new Zend_Db_Expr('NOW()');
				$data['expiration_date'] = new Zend_Db_Expr('DATE_ADD(NOW(), INTERVAL ' . $data['period'] . ' DAY)');
				
				$data['search_text'] = $data['title'] . ' ' . $data['text'] . ' ' . $data['phone'] . ' ' . $data['email'];
				
				$id = $this->model->saveAd($data, $this->_session->photos);
				if($id){
					Model_Activity::getInstance()->log('cads_create', array('ad_id' => $id));
				}
				if (Cubix_Application::getId() == APP_A6){
					$m_updates = new Model_ClassifiedAdsUpdates();
					$m_updates->addEvent($id, Model_ClassifiedAdsUpdates::EVENT_TYPE_UPDATED);
				}
				$this->_session->unsetAll();
			}
			
			die(json_encode($validator->getStatus()));
		} else {
			$this->_session->unsetAll();
		}
	}
	
	public function adEditAction()
	{
		
		$arr_key = 'classified_ad_categories';
		if ( Cubix_Application::getId() == APP_BL ) {
			$arr_key = 'classified_ad_categories_bl';
		}
		elseif ( Cubix_Application::getId() == APP_ED ) {
			$arr_key = 'classified_ad_categories_ed';
		}

        if(Cubix_Application::getId() == APP_EG_CO_UK){
            $country_model = new Model_Countries();
            $this->view->countries = $country_model->getPhoneCountries();
        }

		$this->view->arr_key = $arr_key;
		
		$req = $this->_request;
		$m_cities = new Model_Geography_Cities();
		$non_country_apps = array(30, 11);
		if (in_array(Cubix_Application::getId(), $non_country_apps) ) {
			$this->view->cities = $m_cities->ajaxGetAllCountryByAppId(Cubix_Application::getId());
		} 
		elseif(Cubix_Application::getId() == APP_A6){
			$m_cantons = new Model_Cantons;
			$this->view->cities = $m_cantons->getAll();
			$m_countries = new Model_Countries();
			$this->view->phone_prefixes = $phone_prefixes = $m_countries->getPhonePrefixs();
			// var_dump($phone_prefixes);die;
		}
		else {
			$this->view->cities = $m_cities->ajaxGetAll(null, Cubix_Application::getById()->country_id);
		}
		
		$this->view->id = $id = (int) $req->id;
		
		$this->view->ad_data = $ad_data = $this->model->getAdById($id);
		// var_dump($ad_data);die;
		$act = $req->act;
		if ( $req->ft ) {
			$this->_session->unsetAll();
		}
		if ( count($ad_data['photos']) ) {
			foreach( $ad_data['photos'] as $photo ) {
				$photo = (array) $photo;
				$this->_session->photos[$photo['image_id']] = $photo;
			}
		}
		
		if ( $req->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$params = array(
				'category_id' => 'int',
				//'city_id' => 'int',
				'phone' => '',
				'email' => '',
				'period' => 'int',
				'title' => '',
				'text' => '',
			);

            if(Cubix_Application::getId() == APP_EG_CO_UK){
                $params['country_prefix'] = 'int';
            }

            if(Cubix_Application::getId() == APP_A6){
                $params['web'] = '';
                $params['sms_available'] = '';
                $params['whatsapp_available'] = '';
                $params['phone_prefix_id'] = '';
            }

			$data->setFields($params);
			
			$data = $data->getData();
			if(Cubix_Application::getId() == APP_A6){
                $data['whatsapp_available'] = $data['whatsapp_available'] == 'on' ? 1 : 0;
                $data['sms_available'] = $data['sms_available'] == 'on' ? 1 : 0;
            }
			$data['cities'] = $req->cities;
			
			$validator = new Cubix_Validator();
			
			if ( ! $data['category_id'] ) {
				$validator->setError('category_id', 'Required');
			}
			
			if ( ! count($data['cities']) ) {
				$validator->setError('city_id', 'Required');
			}
			
			if ( ! strlen(trim($data['title'])) ) {
				$validator->setError('title', 'Required');
			}
			
			$_text = strip_tags(trim($data['text']));
			$_text = str_replace('&nbsp;', '', $_text);
			$_text = preg_replace('/\s+/', '', $_text);
			
			if ( ! strlen($_text) ) {
				$validator->setError('text', 'Required');
			}
			
			if ( ! strlen($data['phone']) && ! strlen($data['email']) ) {
				$validator->setError('phone', 'Email or Phone Required');
			}
			
			if(Cubix_Application::getId() == APP_A6 && strlen($data['phone'])){

				$contact_phone = $data['phone'];
				$phone_prefix_id = $data['phone_prefix_id'];
				$phone_prefix = $m_countries->getPhonePrefixById($phone_prefix_id);
				$ndd_prefix = $m_countries->getPhoneNddPrefixById($phone_prefix_id);
				$phone_parsed = null;
				if ($contact_phone ) {
					if ( ! preg_match("/^[0-9\s\+\-\(\)]+$/i", $contact_phone) ) {
						$validator->setError('phone', 'Invalid phone number');
					}
					elseif(preg_match("/^(\+|00)/", $contact_phone) ) {
						$validator->setError('phone', 'Enter phone without prefix');
					}

				}
				$phone_parsed = preg_replace('/[^0-9]+/', '', $contact_phone);
				$phone_parsed = preg_replace('/^'.$ndd_prefix.'/', '', $phone_parsed);
				$phone_parsed = '+'.$phone_prefix.$phone_parsed;
				$data['phone_parsed'] = $phone_parsed;

			}
			if ( strlen($data['email']) && ! $validator->isValidEmail($data['email']) ) {
				$validator->setError('email', 'Invalid Email');
			}

			if (Cubix_Application::getId() == APP_A6 && strlen($data['web']) && (! preg_match("/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i", $data['web'])  && ! preg_match("/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i", 'http://' . $data['web'])) ){
				$validator->setError('web', 'Invalid Web URL');
			}
			if ( $validator->isValid() ) {
				if ( $act == 'approve' ) {
					$data['status'] = Model_ClassifiedAds::AD_STATUS_ACTIVE;
					Model_Activity::getInstance()->log('cads_approve', array('ad_id' => $id));
                    if ( strlen($data['email']) ) {
                        Cubix_Email::sendTemplate('cad_ad_approved', $data['email'], array('subject' => 'dd'));
                    }
				} else if ( $act == 'disable' ) {
					$data['is_disabled'] = 1;
					$data['status'] = Model_ClassifiedAds::AD_STATUS_EXPIRED;
					Model_Activity::getInstance()->log('cads_disable', array('ad_id' => $id));
				} else if ( $act == 'activate' ) {
					$data['is_disabled'] = 0;
					$data['status'] = Model_ClassifiedAds::AD_STATUS_PENDING;
					Model_Activity::getInstance()->log('cads_activate', array('ad_id' => $id));
				}
				
				if ( $ad_data['data']->approvation_date ) {
					$data['expiration_date'] = new Zend_Db_Expr('DATE_ADD(approvation_date, INTERVAL ' . $data['period'] . ' DAY)');
				} else {
					$data['approvation_date'] = new Zend_Db_Expr('NOW()');
					$data['expiration_date'] = new Zend_Db_Expr('DATE_ADD(NOW(), INTERVAL ' . $data['period'] . ' DAY)');
				}
				
				$data['search_text'] = $data['title'] . ' ' . $data['text'] . ' ' . $data['phone'] . ' ' . $data['email'];
				$this->model->saveAd($data, $this->_session->photos, $id);
				
				$this->_session->unsetAll();
				if (Cubix_Application::getId() == APP_A6){
					$m_updates = new Model_ClassifiedAdsUpdates();
					$m_updates->addEvent($id, Model_ClassifiedAdsUpdates::EVENT_TYPE_UPDATED);
				}
			}
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function adRemoveAction()
	{
		$id = $this->_request->id;
		$this->model->removeAd($id);
		if (Cubix_Application::getId() == APP_A6){
			$m_updates = new Model_ClassifiedAdsUpdates();
			$m_updates->addEvent($id, Model_ClassifiedAdsUpdates::EVENT_TYPE_REMOVED);
		}
		die;
	}

	public function deleteAction(){
        if ( is_array($this->_request->id) ) {
            $ad_ids = $this->_request->id;
        }
        else {
            $ad_ids = array(intval($this->_request->id));
        }

        foreach ($ad_ids as $id){
            if (!intval($id)){continue;}
            $this->model->removeAd($id);
            Model_Activity::getInstance()->log('cads_delete', array('ad_id' => $id));
        }
        if (Cubix_Application::getId() == APP_A6){
			$m_updates = new Model_ClassifiedAdsUpdates();
        	if ( is_array($this->_request->id) ) {
	            foreach($this->_request->id as $id){
					$m_updates->addEvent(intval($id), Model_ClassifiedAdsUpdates::EVENT_TYPE_REMOVED);
	            }
	        } else{
					$m_updates->addEvent(intval($id), Model_ClassifiedAdsUpdates::EVENT_TYPE_REMOVED);
	        }
		}
        die;
    }
	
	public function packageDataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'application_id' => $req->application_id
		);
		
		$data = $this->model->getAllPackages(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
	
	public function pkgAction()
	{
		
	}
	
	public function packageRemoveAction()
	{
		$id = $this->_request->id;
		$this->model->removePackage($id);
		die;
	}
	
	public function packageCreateAction()
	{
		$req = $this->_request;
		
		if ( $req->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);

			$params = array(
				'name' => '',
				'price' => 'double',
				'period' => 'int'
			);

			$data->setFields($params);
			
			$data = $data->getData();
						
			$validator = new Cubix_Validator();
			
			if ( ! $data['name'] ) {
				$validator->setError('name', 'Required');
			}
			
			if ( ! $data['price'] ) {
				$validator->setError('price', 'Required');
			}
			
			if ( ! $data['period'] ) {
				$validator->setError('period', 'Required');
			}
			
			if ( $validator->isValid() ) {
				$this->model->savePackage($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function packageEditAction()
	{
		$req = $this->_request;
		$id = $this->view->id = (int) $req->id;
		
		if ( ! $id ) return;
		
		$this->view->data = $this->model->getPackageById($id);
		
		if ( $req->isPost() ) {
			
			$data = new Cubix_Form_Data($this->_request);

			$params = array(
				'name' => '',
				'price' => 'double',
				'period' => 'int'
			);

			$data->setFields($params);

			$data = $data->getData();
						
			$validator = new Cubix_Validator();
			
			if ( ! $data['name'] ) {
				$validator->setError('name', 'Required');
			}
			
			if ( ! $data['price'] ) {
				$validator->setError('price', 'Required');
			}
			
			if ( ! $data['period'] ) {
				$validator->setError('period', 'Required');
			}
			
			if ( $validator->isValid() ) {
				$this->model->savePackage($data, $id);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function transfersDataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'application_id' => $req->application_id,
			'cat.id' => $req->id,
			'cat.ad_id' => $req->ad_id,
			'cat.first_name' => $req->first_name,
			'cat.last_name' => $req->last_name,
			'cat.transaction_id' => $req->transaction_id,
			'cat.amount' => $req->amount,
			'cat.status' => $req->status,
			'cat.is_paid' => $req->is_paid,
			
			'filter_by' => $req->filter_by,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to,
		);
		
		$data = $this->model->getAllTransfers(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		$DEFINITIONS = Zend_Registry::get('defines');
		
		$statuses = array(
			Model_ClassifiedAds::AD_TRANSFER_STATUS_PENDING => 'PENDING',
			Model_ClassifiedAds::AD_TRANSFER_STATUS_CONFIRMED => 'CONFIRMED',
			Model_ClassifiedAds::AD_TRANSFER_STATUS_REJECTED => 'REJECTED'
		);
		
		foreach ( $data as $i => $item ) {
			$data[$i]->status_title = $statuses[$item->status];
			$data[$i]->is_paid_title = ($item->is_paid) ? 'PAID' : 'NOT PAID';
		}
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
	
	public function transfersAction()
	{
		
	}
	
	public function uploadHtml5Action()
	{
		$tmp_dir = sys_get_temp_dir();
		
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$ad_id = intval($this->_getParam('ad_id'));
		
		try {						
			if ( count($this->_session->photos) >= 8 ) {
				throw new Exception("You've reached maximum amount of pictures ( 8 )", Cubix_Images::ERROR_IMAGE_COUNT_LIMIT_OVER);
			}
			
			$response = array(
				'id'	=> $_SERVER['HTTP_X_FILE_ID'],
				'name'	=> $_SERVER['HTTP_X_FILE_NAME'],
				'size'	=> $_SERVER['HTTP_X_FILE_SIZE'],
				'error'	=> 0,
				'msg' => '',
				'finish'	=> FALSE
			);
			
			$file = $tmp_dir . DIRECTORY_SEPARATOR . $response['name'];
			file_put_contents($file, file_get_contents('php://input'));
			
			$file = array(
				'tmp_name' => $file,
				'name' => $response['name']
			);
			
			$images = new Cubix_ImagesCommon();
			$photo = $images->save($file);
			
			$img = new Cubix_ImagesCommonItem($photo);
			
			$response['image_id'] = $img->getImageId();
			$response['photo'] = $img->getUrl('backend_thumb');
			$response['finish'] = TRUE;
			
			$this->_session->photos[$photo['image_id']] = $photo;
			
		} catch (Exception $e) {
			$response['error']	= 1;
			$response['msg']	= $e->getMessage();
		}		
		
		echo json_encode($response);die;
	}

	public function uploadPhotoAction()
	{
		$tmp_dir = sys_get_temp_dir();
		
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		try {
		
			$images = new Cubix_ImagesCommon();
			foreach ( $_FILES as $i => $file ) {
				if ( strlen($file['tmp_name']) ) {
					$photo = $images->save($file);				
				}
			}

			echo json_encode($photo);
		} catch( Exception $e ) {
		    //test classified ads sync
            echo json_encode(array(
                'error'=>$e->getMessage(),
                'file'=>$e->getFile(),
                'line'=>$e->getLine(),
                'trace'=>$e->getTraceAsString(),
            ));
		}
	}
	
	public function ajaxRemovePhotoAction()
	{
		$image_id = (int) $this->_request->image_id;
		
		$status = array('status' => 'error');
		if ( isset($this->_session->photos[$image_id]) ) {
			unset($this->_session->photos[$image_id]);
			
			$this->model->removeAdImages($image_id);
			
			$status = array('status' => 'success');
		}
		
		die(json_encode($status));
	}
	
	public function makePremiumAction()
	{
		$req = $this->_request;
		$this->view->packages = $packages = $this->model->getPackagesForSelectBox();
		
		$ad_id = $this->view->id = (int) $req->id;
		
		$package_id = (int) $req->package_id;
		$wo_transfer = (int) $req->wo_transfer;
		
		$first_name = $req->first_name;
		$last_name = $req->last_name;
		$transaction_id = $req->transaction_id;
		$amount = $req->amount;
		
		if ( $req->isPost() ) {
			$validator = new Cubix_Validator();
			
			if ( ! $wo_transfer ) {
				if ( ! strlen($first_name) ) {
					$validator->setError('first_name', 'Required');
				}
				
				if ( ! strlen($last_name) ) {
					$validator->setError('last_name', 'Required');
				}
				
				if ( ! strlen($transaction_id) ) {
					$validator->setError('transaction_id', 'Required');
				}
				
				if ( strlen($transaction_id) && $this->model->isTransactionIdExist($transaction_id) ) {
					$validator->setError('transaction_id', 'Dublicate Transaction ID');
				}
				
				if ( ! $package_id ) {
					$validator->setError('package_id', 'Required');
				}
			}
			
			if ( $validator->isValid() ) {
				$package = $this->model->getPackageById($package_id);
				
				$this->model->makePremium($ad_id, $package->period);
				if (Cubix_Application::getId() == APP_A6){
					$m_updates = new Model_ClassifiedAdsUpdates();
					$m_updates->addEvent($ad_id, Model_ClassifiedAdsUpdates::EVENT_TYPE_UPDATED);
				}
				if ( ! $wo_transfer ) {
					$auth = Zend_Auth::getInstance();
					$be_user = $auth->getIdentity();
					
					$tr_data = array(
						'ad_id' => $ad_id,
						'status' => Model_ClassifiedAds::AD_TRANSFER_STATUS_CONFIRMED,
						'first_name' => $first_name,
						'last_name' => $last_name,
						'transaction_id' => $transaction_id,
						'amount' => $package->price,
						'application_id' => Cubix_Application::getId(),
						'backend_user_id' => $be_user->id,
						'date_confirmed' => new Zend_Db_Expr('NOW()'),
						'package_name' => $package->name,
						'package_price' => $package->price,
						'package_period' => $package->period,
						'is_paid' => 1
					);
					$this->model->addTransfer($tr_data);
				}
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function transferRejectAction()
	{
		$tr_id = (int) $this->_request->id;
		$this->model->rejectTransfer($tr_id);
		
		die;
	}
	
	public function transferConfirmAction()
	{
		$req = $this->_request;
		
		$this->view->id = $tr_id = (int) $req->id;
		
		$this->view->data = $tr_data = $this->model->getTransferById($tr_id);
		
		$comment = $req->comment;
		
		if ( $req->isPost() ) {
			$validator = new Cubix_Validator();
			
			if ( $validator->isValid() ) {
				$data = array(
					'comment' => $comment,
				);
				$this->model->confirmTransfer($data, $tr_id);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function transferEditAction()
	{
		$req = $this->_request;
		
		$this->view->id = $tr_id = (int) $req->id;
		
		$this->view->data = $tr_data = $this->model->getTransferById($tr_id);
		
		$first_name = $req->first_name;
		$last_name = $req->last_name;
		$transaction_id = $req->transaction_id;
		
		if ( $req->isPost() ) {
			$validator = new Cubix_Validator();			
			
			if ( ! strlen($first_name) ) {
				$validator->setError('first_name', 'Required');
			}

			if ( ! strlen($last_name) ) {
				$validator->setError('last_name', 'Required');
			}

			if ( ! strlen($transaction_id) ) {
				$validator->setError('transaction_id', 'Required');
			}
			
			if ( strlen($transaction_id) && $this->model->isTransactionIdExist($transaction_id, $tr_id) ) {
				$validator->setError('transaction_id', 'Dublicate Transaction ID');
			}
			
			if ( $validator->isValid() ) {
				$data = array(
					'first_name' => $first_name,
					'last_name' => $last_name,
					'transaction_id' => $transaction_id,
				);
				$this->model->editTransfer($data, $tr_id);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}

    public function setCropArgsAction(){
        $model = new Model_ClassifiedAds();

        $photo_id = intval($this->_getParam('photo_id'));

        $photo = $model->getPhoto( $photo_id );

        //print_r($photo); die();

        //$hash = $photo->getHash();

        $result = array(
            'x' => intval($this->_getParam('x')),
            'y' => intval($this->_getParam('y')),
            'px' => round(floatval($this->_getParam('px')), 4),
            'py' => round(floatval($this->_getParam('py')), 4),
            'rwidth'=>intval($this->_getParam('rwidth')),
            'rheight'=>intval($this->_getParam('rheight'))
        );

        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $model->setCropArgs( $result, $photo_id );

        // Crop all images
        $size_map = array(
            'cads_orig' => array('width' => 1024, 'height' => 1024, 'action' => 'fitbox'),
            'cads_thumb' => array('width' => 102, 'height' => 136, 'use_crop_args' => true),
            'cads_small' => array('width' => 85, 'height' => 110, 'use_crop_args' => true),
        );
        $conf = Zend_Registry::get('images_config');

        get_headers($conf['remote']['url'] . '/get_image.php?a=clear_cache_common&app=' . Cubix_Application::getById($this->_getParam('application_id'))->host . /*'&eid=' . $photo->escort_id . */'&hash=' . $photo->hash);

        $parts = array();
        $catalog = $photo->hash;

        $parts[] = substr($catalog, 0, 2);
        $parts[] = substr($catalog, 2, 2);
        $parts[] = substr($catalog, 4, 2);

        $catalog = implode('/', $parts);
        $resize = (int)$this->_getParam('resize');

        if($photo->is_portrait=='0')
        {
            $kwidth = floatval($photo->width/$resize);
            $height = $photo->height;
            $x = $result['x']*$kwidth+2;
            $y = 0;
            $width =$kwidth*$result['rwidth'];
        }
        else
        {	$kheight = floatval($photo->height/$resize);
            $width =$photo->width;
            $x = 0;
            $y = $result['y']*$kheight+2;
            $height =$kheight*$result['rheight'];
        }

        foreach($size_map as $size => $sm) {
            //echo $conf['remote']['url'] . '/' . Cubix_Application::getById($this->_getParam('application_id'))->host . '/common/' . $catalog . '/' . $photo->hash . '_' . $size . '.jpg?args=' . $x . ':' . $y.'&cwidth='.$width.'&cheight='.$height;
            // echo $conf['remote']['url'] . '/' . Cubix_Application::getById($this->_getParam('application_id'))->host . '/common/' . $catalog . '/' . $photo->hash . '_' . $size . '.jpg?args=' . $result['x'] . ':' . $result['y'] . '<br />';
            get_headers($conf['remote']['url'] . '/' . Cubix_Application::getById($this->_getParam('application_id'))->host . '/common/' . $catalog . '/' . $photo->hash . '_' . $size . '.jpg?args=' . $x . ':' . $y.'&cwidth='.$width.'&cheight='.$height);
        }

        echo json_encode(array('success' => true));
    }

	public function feedbacksAction()
	{

	}

	public function feedbacksDataAction()
	{
		$req = $this->_request;

		$filter = array(
			'ca_id' => $req->ca_id,
			'email' => $req->email,
			'to_addr' => $req->to_addr,
			'flag' => $req->flag,
			'status' => $req->status,
			'ip' => $req->ip,
			'city' => $req->city,
			'application_id' => $req->application_id
		);

		$data = $this->model->getAllFeedbacks(
			$req->page,
			$req->per_page,
			$filter,
			$req->sort_field,
			$req->sort_dir,
			$count
		);

		$DEFINITIONS = Zend_Registry::get('defines');

		foreach ( $data as $i => $item )
		{
			$data[$i]->flag_name = $DEFINITIONS['feedback_flags'][$item->flag];
			$data[$i]->status_name = $DEFINITIONS['feedback_status'][$item->status];
			$data[$i]->ip = $this->view->Geoip($item->ip);
		}

		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

	public function feedbackViewAction()
	{
		$this->view->item = $item = $this->model->getFeedback($this->_getParam('id'));

		if ( $this->_request->isPost() )
		{
			$validator = new Cubix_Validator();
			$act = $this->_request->act;

			$data = array('id' => $item->id);

			if ($act == 'disable')
			{
				if ($item->status == FEEDBACK_STATUS_APPROVED)
				{
					$validator->setError('err', 'You can not disable, feedback already approved.');
				}
				elseif ($item->status == FEEDBACK_STATUS_NOT_APPROVED)
				{
					$data['status'] = FEEDBACK_STATUS_DISABLED;

					if ( $validator->isValid() )
					{
						$this->model->updateFeedback($data);
					}
				}
			}
			elseif ($act == 'approve')
			{
				$data['status'] = FEEDBACK_STATUS_APPROVED;
				$data['flag'] = FEEDBACK_FLAG_SENT;

				if ( $validator->isValid() )
				{
					$this->model->updateFeedback($data);

					$email_tpl = 'escort_feedback';
					$tpl_data = array(
						'name' => $item->name,
						'email' => $item->email,
						'showname' => 'ad owner',
						'message' => $item->message
					);

					Cubix_Email::sendTemplate($email_tpl, $item->to_addr, $tpl_data, $item->email);
				}
			}

			die(json_encode($validator->getStatus()));
		}
	}

	public function feedbacksDeleteAction()
	{
		set_time_limit(0);

		if ( $this->_request->isPost() )
		{
			$ids = $this->_request->id;

			if (is_array($ids) && count($ids) > 0)
			{
				foreach ($ids as $id)
				{
					$item = $this->model->getFeedback($id);

					if ($this->_request->act == 'disable')
					{
						if ($item->status == FEEDBACK_STATUS_NOT_APPROVED)
						{
							$data = array('id' => $item->id);

							$data['status'] = FEEDBACK_STATUS_DISABLED;
							$data['flag'] = FEEDBACK_FLAG_NOT_SEND;

							$this->model->updateFeedback($data);
						}
					}
					else
					{
						if ($item->status != FEEDBACK_STATUS_APPROVED)
						{
							$data = array('id' => $item->id);

							$data['status'] = FEEDBACK_STATUS_APPROVED;
							$data['flag'] = FEEDBACK_FLAG_SENT;

							$this->model->updateFeedback($data);

							$email_tpl = 'escort_feedback';
							$tpl_data = array(
								'name' => $item->name,
								'email' => $item->email,
								'showname' => 'ad owner',
								'message' => $item->message
							);

							Cubix_Email::sendTemplate($email_tpl, $item->to_addr, $tpl_data, $item->email);
						}
					}
				}
			}
		}

		die;
	}

	

}
