<?php

class CronController extends Zend_Controller_Action
{
	// Email trigger types
	const TRIGGER_SIGNUP = 1; //recurring

	const TRIGGER_APPROVED_PROFILE_PENNDING_AC = 2;
	const TRIGGER_APPROVED_PROFILE_MISSING_AC = 3;
	const TRIGGER_APPROVED_PROFILE_APPROVED_AC = 4;
	const TRIGGER_APPROVED_PROFILE_REJECTED_AC = 5;

	const TRIGGER_WIZARD = 6; //recurring

	const TRIGGER_APPROVED_AC_APPROVED_PROFILE = 7;
	const TRIGGER_APPROVED_AC_PENNDING_PROFILE = 8;
	const TRIGGER_REJECTED_AC = 9;

	const TRIGGER_COMPLETED_PROFILE_MISSING_AC = 10; // recurring

	/**
	 *
	 * @var Zend_Db_Adapter_Mysqli
	 */
	private $_db;

	public function init()
	{
		$this->_db = Zend_Registry::get('db');
	}

	public function trialPackageExpiresAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$cli = new Cubix_Cli();
		$pid_file = '/var/run/trial-package-expires-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}

		$m_cron = new Model_Cron();
		$m_cron->ProcessTrialPackageExpiresEmail();
	}
	
	public function gotdAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$cli = new Cubix_Cli();
		$pid_file = '/var/run/gotd-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}

		$m_cron = new Model_Cron();
		$m_cron->ProcessGotdEscorts();
	}

	public function votdAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$cli = new Cubix_Cli();
		$pid_file = '/var/run/votd-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}

		$m_cron = new Model_Cron();
		$m_cron->ProcessVotd();
	}
	
	public function happyHourAction()
	{
		echo 'happy hour cron started';
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		
		$cli = new Cubix_Cli();
		$pid_file = '/var/run/happy-hour-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}

		$m_cron = new Model_Cron();
		$m_cron->ProcessHappyHour();
		die("done ");
	}

    public function deleteEscortsAction(){
        $this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

        $sql = '
			SELECT e.id as e_id,u.id as u_id,e.agency_id
			FROM escorts e
            INNER JOIN users u
            ON e.user_id = u.id
			WHERE e.status & ? AND DATE_SUB(now(), INTERVAL +48 HOUR) >= deleted_date
		';

		$result = $this->_db->fetchAll($sql, array(Cubix_EscortStatus::ESCORT_STATUS_TEMPRARY_DELETED));

        if( count($result) > 0 ){
            foreach ( $result as $res ){
                if( !$res->agency_id ){
                    $this->_db->update('users',   array('status' => USER_STATUS_DELETED), $this->_db->quoteInto('id = ?', $res->u_id));
                }
                $status = new Cubix_EscortStatus($res->e_id);
                $status->setStatus(Cubix_EscortStatus::ESCORT_STATUS_DELETED);
                $status->removeStatusBit(Cubix_EscortStatus::ESCORT_STATUS_TEMPRARY_DELETED);
                $status->removeStatusBit(Cubix_EscortStatus::ESCORT_STATUS_ADMIN_DISABLED);
                $status->save();

				$event = Cubix_SyncNotifier::EVENT_ESCORT_STATUS_UPDATED;
				Cubix_SyncNotifier::notify($res->e_id, $event, array('old' => 'Temporary Deleted', 'new' => 'Completely Deleted'));
				
            }
        }
        
        echo "Effected Rows ".count($result);
        exit;
    }

	public function escortsOnTourAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$cli = new Cubix_Cli();
		$pid_file = '/var/run/escorts-on-tour-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}

		// Calculate the execution time
		$start_time = microtime(true);

		$cli->clear();

		// Pass this to sql instead of NOW() function, this technique will made
		// the query cachable
		$date = date('Y-m-d');

		$disable_when_tour_ends_apps = array(APP_ED, APP_EG_CO_UK);

		$where = " ";
		if ( in_array(Cubix_Application::getId(), $disable_when_tour_ends_apps) ) {
			$where = " AND e.disable_when_tour_ends = 0 ";
		}

		// Select previouse(cur_tour_id) and current tour id(tour_id) for all active escorts
		$sql = '
			SELECT e.id, cur_tour_id, t.id AS tour_id
			FROM escorts e
			LEFT JOIN tours t ON t.escort_id = e.id AND DATE(t.date_from) <= ? AND DATE(t.date_to) >= ?
			WHERE e.status & 32 ' . $where . '
		';
		$result = $this->_db->fetchAll($sql, array($date, $date));

		$changed = false;
		foreach ( $result as $escort ) {
			// If escort tour has change (expired or activated) store new tour id
			// to be able to compare with when next time this script executed
			if ( $escort->cur_tour_id != $escort->tour_id ) {
				$this->_db->update('escorts', array('cur_tour_id' => $escort->tour_id), $this->_db->quoteInto('id = ?', (int) $escort->id));

				// And also notify sync about changes
				$event = null !== $escort->tour_id ? Cubix_SyncNotifier::EVENT_ESCORT_TOUR_ACTIVATED :
						Cubix_SyncNotifier::EVENT_ESCORT_TOUR_DEACTIVATED;
				Cubix_SyncNotifier::notify($escort->id, $event, array('old' => $escort->cur_tour_id, 'new' => $escort->tour_id));

				$cli->out($escort->id . ': tour changed from ' . (! $escort->cur_tour_id ? '"no tour"' : $escort->cur_tour_id) . ' to ' . (! $escort->tour_id ? '"no tour"' : $escort->tour_id));
				
				// Set this flag to know if anything changed
				$changed = true;
			}
		}
		
		if ( ! $changed ) {
			$cli->colorize('yellow')->out('Nothing happened... :)')->reset()->out();
		}

		$end_time = microtime(true);

		// Just print the execution time
		$cli->colorize('purple')->out('Execution took ' . round($end_time - $start_time, 4) . ' seconds')->reset()->out();
	}


	public function escortsOnTourChangeEscortStatusAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$cli = new Cubix_Cli();
		$pid_file = '/var/run/escorts-on-tour-change-escort-status' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}

		$disable_when_tour_ends_apps = array(APP_ED, APP_EG_CO_UK);
		if ( ! in_array(Cubix_Application::getId(), $disable_when_tour_ends_apps) ) {
			return false;
		}

		// Calculate the execution time
		$start_time = microtime(true);

		$cli->clear();

		// Pass this to sql instead of NOW() function, this technique will made
		// the query cachable
		$date = date('Y-m-d');

		// Select previouse(cur_tour_id) and current tour id(tour_id) for all active escorts
		$sql = '
			SELECT e.id, cur_tour_id, t.id AS tour_id
			FROM escorts e
			LEFT JOIN tours t ON t.escort_id = e.id AND DATE(t.date_from) <= ? AND DATE(t.date_to) >= ?
			WHERE (e.status & 32 OR e.status & 8) AND e.disable_when_tour_ends = 1
		';
		$result = $this->_db->fetchAll($sql, array($date, $date));

		$changed = false;
		foreach ( $result as $escort ) {
			// If escort tour has change (expired or activated) store new tour id
			// to be able to compare with when next time this script executed
			if ( $escort->cur_tour_id != $escort->tour_id ) {
				$this->_db->update('escorts', array('cur_tour_id' => $escort->tour_id), $this->_db->quoteInto('id = ?', (int) $escort->id));

				// And also notify sync about changes
				$event = null !== $escort->tour_id ? Cubix_SyncNotifier::EVENT_ESCORT_TOUR_ACTIVATED :
						Cubix_SyncNotifier::EVENT_ESCORT_TOUR_DEACTIVATED;
				Cubix_SyncNotifier::notify($escort->id, $event, array('old' => $escort->cur_tour_id, 'new' => $escort->tour_id));

				$_status = new Cubix_EscortStatus($escort->id);

				// If escort tour is activated, change escort status to Active
				if ( $event == Cubix_SyncNotifier::EVENT_ESCORT_TOUR_ACTIVATED ) {
					$_status->setStatus(Cubix_EscortStatus::ESCORT_STATUS_ACTIVE);
					$cli->out($escort->id . ': status changed to Active');
				} elseif ( $event == Cubix_SyncNotifier::EVENT_ESCORT_TOUR_DEACTIVATED ) { // If escort tour is end, change escort status to Owner Disabled
					$_status->setStatus(Cubix_EscortStatus::ESCORT_STATUS_OWNER_DISABLED);
					$cli->out($escort->id . ': status changed to Owner Disabled');
				}
				$_status->save();

				$cli->out($escort->id . ': tour changed from ' . (! $escort->cur_tour_id ? '"no tour"' : $escort->cur_tour_id) . ' to ' . (! $escort->tour_id ? '"no tour"' : $escort->tour_id));
				
				// Set this flag to know if anything changed
				$changed = true;
			}
		}
		
		if ( ! $changed ) {
			$cli->colorize('yellow')->out('Nothing happened... :)')->reset()->out();
		}

		$end_time = microtime(true);

		// Just print the execution time
		$cli->colorize('purple')->out('Execution took ' . round($end_time - $start_time, 4) . ' seconds')->reset()->out();
	}

	
	public function analyticsCronAction()
	{	
			
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	if(in_array(Cubix_Application::getId(),array(APP_6A,APP_EF,APP_EG,APP_EG_CO_UK,APP_6C,APP_BL,APP_6B,APP_AE)))
	{	
		$configs = Zend_Registry::get('analytics_config');
		$cli = new Cubix_Cli();
		if(!in_array(date('d'),$configs['cron_days']))
		{
			$cli->out('AnalyticsCron action days only :'.implode(',', $configs['cron_days']));
			exit(1);
		}
		
		$pid_file = '/var/run/analytics-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$start = 0 ;
		$limit = 1000;
		$sleep = 2;//seconds
		
		$db_analityc = Zend_Db::factory('mysqli', array(
				'host' => $configs['host'],
				'username' => $configs['username'],
				'password' => $configs['password'],
				'dbname' => $configs['dbname']
			));
		$backend_db= Zend_Registry::get('db');
		
		
		if(date('d')=='05')
		{
			$table = 'analytics_'.str_replace('-', '_', date('Y-m-d'));
		}
		else
		{	
			$get_last_table =  $db_analityc->fetchRow("SELECT table_name FROM information_schema.tables WHERE table_schema = '{$configs['dbname']}' ORDER BY create_time DESC");
			$table = $get_last_table['table_name'];
		}
		$count = $backend_db->fetchRow("SELECT count(*) AS q FROM analytics ")->q;
		$check_table = $db_analityc->fetchAll("SHOW TABLES LIKE '$table'");
		$get_dbname  = Zend_Registry::get('db_connection');
		$dbname = $get_dbname['db']['params']['dbname'];
		if(empty($check_table))
		{	
			$create_table = "CREATE TABLE $table LIKE $dbname.analytics";
			$db_analityc->query($create_table);
		}
	
		while($start<=$count)
		{
			$db_analityc->query("INSERT IGNORE INTO $table SELECT * FROM $dbname.analytics  LIMIT $start,$limit");
			$start+=$limit;
			$limit+=$limit;
			sleep($sleep);
		}
		if(date('d')=='01')
		{
			$backend_db->query(" TRUNCATE TABLE  $dbname.analytics ");
		}
	else{
			$backend_db->query(" DELETE FROM  $dbname.analytics ");
		}
	}
	}
	
	public function escortsOpenCloseAction()
	{
	 	set_time_limit(0);
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$cli = new Cubix_Cli();
		$pid_file = '/var/run/escorts-open-close-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}

		// Calculate the execution time
		$start_time = microtime(true);

		$cli->clear();

		$hour = date('G');
		$day = date('N');
//		$day--;
//		if ( $day < 1 ) $day = 7;

		$sql = '
			SELECT e.id, e.is_open AS was_open, IF(ewt.escort_id IS NULL, 0, 1) AS is_now_open
			FROM escorts e
			LEFT JOIN escort_working_times ewt ON
				ewt.escort_id = e.id AND (
					(
						ewt.day_index = ' . $day . '
						AND ewt.time_from = 0
						AND ewt.time_to = 0
					)
					OR
					(
						ewt.day_index = ' . $day . '
						AND ewt.time_from <= ' . $hour . '
						AND ewt.time_to >= ' . $hour . '
					)
					OR
					(
						ewt.day_index = ' . $day . '
						AND ewt.time_from <= ' . $hour . '
						AND ewt.time_from > ewt.time_to
					)
					OR
					(
						ewt.day_index = ' . ( (1 == $day) ? 7 : $day - 1 ) . '
						AND ewt.time_to >= ' . $hour . '
						AND ewt.time_from  > ewt.time_to
					)
				)
			WHERE e.status & 32
		';
		$result = $this->_db->fetchAll($sql);

		$changed = false;
		foreach ( $result as $escort ) {
			if ( $escort->was_open != $escort->is_now_open ) {
				$this->_db->update('escorts', array('is_open' => $escort->is_now_open), $this->_db->quoteInto('id = ?', (int) $escort->id));

				if(!in_array(Cubix_Application::getId(), array(APP_EF, APP_A6,APP_6A))){
					// And also notify sync about changes
					$event = null !== $escort->tour_id ? Cubix_SyncNotifier::EVENT_ESCORT_OPEN : Cubix_SyncNotifier::EVENT_ESCORT_CLOSED;
					Cubix_SyncNotifier::notify($escort->id, $event, array('old' => $escort->was_open, 'new' => $escort->is_now_open));
				}
				
				$cli->out($escort->id . ': is ' . ($escort->is_now_open ? 'open' : 'closed'));

				// Set this flag to know if anything changed
				$changed = true;
			}
		}

		if ( ! $changed ) {
			$cli->colorize('yellow')->out('Nothing happened... :)')->reset()->out();
		}

		$end_time = microtime(true);

		// Just print the execution time
		$cli->colorize('purple')->out('Execution took ' . round($end_time - $start_time, 4) . ' seconds')->reset()->out();
	}

	public function premiumMembersExpirationAction()
	{
		set_time_limit(0);
		$members = Model_Members::getExpiredMembers();
		
		echo "*** First stage: Mark expired members ***\n";
		echo str_repeat('-', 80) . "\n\n";
		
		if ( ! count($members) ) {
			echo "No expired members has been found.";
		}

		foreach ( $members as $member ) {
			echo "Processing member #{$member->id}: {$member->username}...";
			ob_flush();
			try {
				Model_Members::setPremiumExpired($member->id);
				echo "\nMember has been marked as expired.\n\n";
			}
			catch ( Exception $e ) {
				Model_Members::logFail($member->ref, null, 'Failed setting memeber as expired: ' . $e->__toString(), Model_Members::STATUS_PREMIUM_EXPIRATION_FAILED);
				echo " [fail]\n";
			}
		}

		echo "\n\n\n";

		$members = Model_Members::getExpiredRecurringMembers();
		
		echo "*** Second stage: Make recurring requests for recurring members ***\n";
		echo str_repeat('-', 80) . "\n\n";

		foreach ( $members as $member ) {
			echo "Processing member #{$member->id}: {$member->username}...";
			ob_flush();
			try {
				$result = Cubix_CGP::doRecurringRequest(Cubix_CGP::getSiteId(), $member->transaction_id, $member->amount, $member->ref);

				$is_test = Cubix_CGP::isTest();
				
				// make a security hash to bypass check in callbackAction
				$hash = '';
				if ( $is_test ) $hash .= 'TEST';
				$hash .= $result->transaction_id;
				$hash .= $result->currency;
				$hash .= $result->amount;
				$hash .= $member->ref;
				$hash .= $result->status;
				$hash .= Cubix_CGP::getSecurityHash();
				$hash = md5($hash);

				$request = new Zend_Controller_Request_Http();
				$request->setParams(array(
					'hash' => $hash,
					'transaction_id' => $result->transaction_id,
					'is_test' => $is_test,
					'ref' => $member->ref,
					'status' => (int) $result->status,
					'currency' => $result->currency,
					'amount' => (int) $result->amount,
				));

				require_once  APPLICATION_PATH . DIRECTORY_SEPARATOR . '/controllers/GatewayController.php';
				$controller = new GatewayController($request, $this->_response);
				$resp = $controller->callbackAction();

				echo " [" . ($resp ? 'success' : 'fail') . "]\n";
			}
			catch ( Exception $e ) {
				Model_Members::logFail($member->ref, null, 'Failed making recurring transaction: ' . $e->__toString(), Model_Members::STATUS_TRANSACTION_RECURRING_ERROR);
				echo " [fail]\n";
			}
		}

		echo "\n\n\n";
		
		exit(0);
	}
	public function sendCommentsAction()
	{
		set_time_limit(0);
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$cli = new Cubix_Cli();
		$pid_file = '/var/run/send-escort-comments-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}

		// Pass this to sql instead of NOW() function, this technique will made
		// the query cachable
		$date = date('Y-m-d');

		// Select previouse(cur_tour_id) and current tour id(tour_id) for all active escorts
		$sql = '
			SELECT ec.id, ec.comment , bu.email FROM escort_comments as ec
			INNER JOIN backend_users bu ON bu.id = ec.sales_user_id
			WHERE DATE(ec.remember_me) <= ? AND ec.email_sent = 0
		';
		try{
			$results = $this->_db->fetchAll($sql, $date);
			$subject = 'Reminder';
			foreach ( $results as $result ) {
				$body = '<html><body><div style="border: 1px solid #c8e7ff; background-color: #FFF; padding: 7px 20px">'.$result->comment.'</div></body></html>';
				$this->_db->update('escort_comments', array('email_sent' => 1), $this->_db->quoteInto('id = ?', $result->id));
				Cubix_Email::send($result->email,$subject, $body, true);
				$cli->out(' Email sent to ' . $result->email);
								
			}
		}
		catch ( Exception $e ) {
			echo $e->__toString();
			echo " [fail]\n";
		}
		exit;
	}
	
	public function alertmeAction()
	{
		set_time_limit(0);
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$cli = new Cubix_Cli();
		$pid_file = '/var/run/alert-me-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}
		
		try {
			$items = $this->_db->query('
				SELECT ae.id, ae.escort_id, ae.`event`, ae.date, ae.`data`, ae.sent, ae.sent_date, au.user_id, au.extra, u.email, u.username, e.showname 
				FROM alert_events ae
				INNER JOIN alert_users au ON au.escort_id = ae.escort_id AND au.`event` = ae.`event` 
				INNER JOIN users u ON u.id = au.user_id AND u.status = 1
				INNER JOIN escorts e ON e.id = ae.escort_id AND /*(e.status & 32) = 32*/ e.status = 32
				WHERE ae.sent = 0 AND au.status = 1 
			')->fetchAll();

			$arr = array();

			foreach ($items as $item)
			{
				$arr[$item->escort_id. '-' . $item->user_id . '-' . $item->event][] = $item->data;
				$arr[$item->escort_id. '-' . $item->user_id . '-' . $item->event]['info'] = $item;
			}

			$ids = array();

			foreach ($arr as $v)
			{
				$info = $v['info'];
				unset($v['info']);

				$id = $info->id;
				$email = $info->email;
				$showname = $info->showname;
				$escort_id = $info->escort_id;
				$username = $info->username;
				$date = $info->date;
				$i = count($v);
				$linked_showname = '<a href="' . Cubix_Application::getById()->url . '/escort/' . $showname . '-' . $escort_id . '">' . $showname . '</a>';

				if (!in_array($id, $ids))
					$ids[] = $id;

				if ($info->event == ALERT_ME_NEW_COMMENT)
				{
					$template_id = 'alertme_comment';
				}
				elseif ($info->event == ALERT_ME_MODIFICATION_ON_PROFILE)
				{
					$template_id = 'alertme_profile';
				}
				elseif ($info->event == ALERT_ME_NEW_PICTURES)
				{
					$template_id = 'alertme_pictures';
				}
				elseif ($info->event == ALERT_ME_NEW_REVIEW)
				{
					$template_id = 'alertme_review';
				}
				elseif ($info->event == ALERT_ME_CITY)
				{
					if (strlen($info->extra) > 0)
					{
						$extra = explode(',', $info->extra);
						$w_cities = array();
						$t_cities = array();

						foreach ($v as $v_item)
						{
							$vv = json_decode($v_item);
							
							if ($vv->type == 'working')
							{
								if (in_array($vv->id, $extra))
									$w_cities[] = intval($vv->id);
							}
							elseif ($vv->type == 'tour')
							{
								if (in_array($vv->id, $extra))
									$t_cities[] = intval($vv->id);
							}
						}
						
						if (count($w_cities) > 0)
						{
							$wcs = $this->_db->query('SELECT title_en AS title FROM cities WHERE id IN (' . implode(',', $w_cities) . ')')->fetchAll();
							$w_titles = array();
							
							foreach ($wcs as $w)
								$w_titles[] = $w->title;
							
							$w_titles_str = implode(', ', $w_titles);
							
							$template_id = 'alertme_working_cities';
							Cubix_Email::sendTemplate($template_id, $email, array('showname' => $showname, 'escort_id' => $escort_id, 'username' => $username, 'count' => $i, 'date' => date('d F, Y', strtotime($date)), 'city' => $w_titles_str));
							$cli->out(' Email sent to ' . $email);
						}
						
						if (count($t_cities) > 0)
						{
							$tcs = $this->_db->query('SELECT title_en AS title FROM cities WHERE id IN (' . implode(',', $t_cities) . ')')->fetchAll();
							$t_titles = array();
							
							foreach ($tcs as $t)
								$t_titles[] = $t->title;
							
							$t_titles_str = implode(', ', $t_titles);
							
							$template_id = 'alertme_tour_cities';
							Cubix_Email::sendTemplate($template_id, $email, array('showname' => $showname, 'escort_id' => $escort_id, 'username' => $username, 'count' => $i, 'date' => date('d F, Y', strtotime($date)), 'city' => $t_titles_str));
							$cli->out(' Email sent to ' . $email);
						}
					}
					
				}

				//send email

				if ($info->event != ALERT_ME_CITY)
				{
					Cubix_Email::sendTemplate($template_id, $email, array('showname' => $showname, 'escort_id' => $escort_id, 'username' => $username, 'count' => $i, 'date' => date('d F, Y', strtotime($date))));
					$cli->out(' Email sent to ' . $email);
				}
			}

			$str_ids = implode(',', $ids);

			if (count($ids) > 0)
			{
				$this->_db->query('UPDATE alert_events SET sent = 1, sent_date = now() WHERE id IN (' . $str_ids . ') AND date < NOW()');
				$this->_db->query('UPDATE alert_events SET sent = 2 WHERE sent = 0 AND date < NOW()');
			}
			
			$cli->out('done!');
		}
		catch ( Exception $e ) {
			echo $e->__toString();
			echo " [fail]\n";
		}
		exit;
	}
	
	public function followAlertAction()
	{
		set_time_limit(0);
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$cli = new Cubix_Cli();
		$pid_file = '/var/run/follow-alert-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}
		$email_data = array();
		try {

			$events = $this->_db->query('
				SELECT f.id ,f.user_id, f.type, f.event_id, u.username
				FROM follow_alerts f
				LEFT JOIN users u ON u.id = f.user_id
				WHERE f.status = 0
			')->fetchAll();

			$followersId = array();

			foreach ($events as $event)
			{	
				$followedUsername = $event->username;

				$eventData = false;
				switch ($event->type) {
					case FOLLOW_ALERT_REVIEW:
						$eventData = $this->_db->query('
							SELECT r.status, r.escort_id, e.showname
							FROM reviews r
							LEFT JOIN escorts e ON e.id = r.escort_id
							WHERE r.id = '.$event->event_id.' AND r.status = 2'
						)->fetch();
						$eventTypeSql = ' AND fo.review = 1 ';
						break;
					case FOLLOW_ALERT_COMMENT:
						$eventData = $this->_db->query('
							SELECT c.status, c.escort_id, e.showname
							FROM comments c
							LEFT JOIN escorts e ON e.id = c.escort_id
							WHERE c.id = '.$event->event_id.' AND c.status = 1'
						)->fetch();
						$eventTypeSql = ' AND fo.comment = 1 ';
						break;
					case FOLLOW_ALERT_PINBOARD_ENTRY:
						$eventData = false;
						//$eventTypeSql = ' AND fo.pin_entry = 1 ';
						break;
					case FOLLOW_ALERT_PINBOARD_REPLAY:
						$eventData = false;
						//$eventTypeSql = ' AND fo.pin_response = 1 ';
						break;
				}


				if(!$eventData){
					continue;
				}

				$smsLangQ = ', u.sms_lng';
				if(Cubix_Application::getId() == APP_BL){
					$smsLangQ = '';
				}

				$followers = $this->_db->query('
					SELECT fo.follower_id, fo.escort_id, u.username, u.email, e.showname '.$smsLangQ.'
					FROM follow fo
					LEFT JOIN users u ON u.id = fo.follower_id
					LEFT JOIN escorts e ON e.user_id = fo.follower_id
					WHERE fo.status = 1 AND fo.followed_id = '.$event->user_id.' '.$eventTypeSql
				)->fetchAll();


				foreach ($followers as $follower) {
					// if need show user name LIKE Hello dear Username
					// if(!empty($follower->showname)){
					// 	$followerUsername = $follower->showname;
					// }else{
					// 	$followerUsername = $follower->username;
					// }

					if(!empty($follower->email)){
						$email = $follower->email;
					}else{
						continue;	
					}
					if(Cubix_Application::getId() == APP_EF){
						switch ($event->type) {
							case FOLLOW_ALERT_REVIEW:
									$link = '<a href="' . Cubix_Application::getById()->url . '/recensioni/' . $eventData->showname . '-' . $eventData->escort_id . '">' . Cubix_I18n::translateByLng($follower->sms_lng, "follow_email_read") . '</a>';
									$subject = Cubix_I18n::translateByLng($follower->sms_lng, "follow_new_review_subject");
									$message = Cubix_I18n::translateByLng($follower->sms_lng, "follow_new_review_body", array('membername'=> $followedUsername, 'link'=> $link));
									$email_data['subject'] = $subject; 
									$email_data['message'] = $message;
								break;
							case FOLLOW_ALERT_COMMENT:
									$link = '<a href="' . Cubix_Application::getById()->url . '/accompagnatrici/' . $eventData->showname . '-' . $eventData->escort_id . '#comments">' . Cubix_I18n::translateByLng($follower->sms_lng, "follow_email_read") . '</a>';
									$subject = Cubix_I18n::translateByLng($follower->sms_lng, "follow_new_comment_subject");
									$message = Cubix_I18n::translateByLng($follower->sms_lng, "follow_new_comment_body", array('membername'=> $followedUsername, 'link'=> $link));
									$email_data['subject'] = $subject; 
									$email_data['message'] = $message;
								break;
							// case FOLLOW_ALERT_PINBOARD_ENTRY:
							// 		$link = '<a href="' . Cubix_Application::getById()->url . '/private/pinboard/?p_id='.$eventData->id.'">' . Cubix_I18n::translateByLng($follower->sms_lng, "follow_email_read") . '</a>';
							// 		$subject = Cubix_I18n::translateByLng($follower->sms_lng, "follow_new_pinboard_post_subject");
							// 		$message = Cubix_I18n::translateByLng($follower->sms_lng, "follow_new_pinboard_post_body", array('membername'=> $followedUsername, 'link'=> $link));
							// 		$email_data['subject'] = $subject; 
							// 		$email_data['message'] = $message;
							// 	break;
							// case FOLLOW_ALERT_PINBOARD_REPLAY:
							// 		$link = '<a href="' . Cubix_Application::getById()->url . '/private/pinboard/?p_id='.$eventData->post_id.'">' . Cubix_I18n::translateByLng($follower->sms_lng, "follow_email_read") . '</a>';
							// 		$subject = Cubix_I18n::translateByLng($follower->sms_lng, "follow_new_pinboard_reply_subject");
							// 		$message = Cubix_I18n::translateByLng($follower->sms_lng, "follow_new_pinboard_reply_body", array('membername'=> $followedUsername, 'link'=> $link));
							// 		$email_data['subject'] = $subject; 
							// 		$email_data['message'] = $message;
							// 	break;
						}
						Cubix_Email::sendTemplate('universal_message', $email, $email_data);
					}

					if(Cubix_Application::getId() == APP_BL){
						switch ($event->type) {
							case FOLLOW_ALERT_REVIEW:
								$link = '<a href="' . Cubix_Application::getById()->url . '/evaluations/' . $eventData->showname . '-' . $eventData->escort_id . '">' . Cubix_I18n::translateByLng('en', "follow_email_read") . '</a>';
								$subject = Cubix_I18n::translateByLng('en', "follow_new_review_subject");
								$message = Cubix_I18n::translateByLng('en', "follow_new_review_body", array('membername'=> $followedUsername, 'link'=> $link));
								$email_data['subject'] = $subject; 
								$email_data['message'] = $message;
							break;
							case FOLLOW_ALERT_COMMENT:
								$link = '<a href="' . Cubix_Application::getById()->url . '/escort/' . $eventData->showname . '-' . $eventData->escort_id . '#comments">' . Cubix_I18n::translateByLng('en', "follow_email_read") . '</a>';
								$subject = Cubix_I18n::translateByLng('en', "follow_new_comment_subject");
								$message = Cubix_I18n::translateByLng('en', "follow_new_comment_body", array('membername'=> $followedUsername, 'link'=> $link));
								$email_data['subject'] = $subject; 
								$email_data['message'] = $message;
							break;
						}
						Cubix_Email::sendTemplate('universal_message', $email, $email_data);
					}

					// $cli->out(' Email sent to ' . $email);

				}
				$this->_db->query('UPDATE follow_alerts SET status = 1 WHERE id = '.$event->id);
			}
			$cli->out('done!');
		}
		catch ( Exception $e ) {
			echo $e->__toString();
			echo " [fail]\n";
		}
		exit;
	}

	public function followEventsAction()
	{
		set_time_limit(0);
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$current_time = date("Y-m-d H:i:s");
		
		$cli = new Cubix_Cli();
		$pid_file = '/var/run/follow-events-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
			
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}

		$start_time = microtime(true);

		try {
			$translation_keys = [
				'falert_title', 
				'view_profile_button_text',
				'falert_escort_visits',
				'falert_escort_tours', 
				'falert_escort_reviews',
				'falert_escort_profiles',
				'falert_escort_prices',
				'falert_escort_pics',
				'falert_escort_comments',
				'falert_button_text', 
				'falert_account_button_text',
				'falert_agency_visits',
				'falert_agency_upcoming_visits',
				'falert_agency_trans_profiles',
				'falert_agency_reviews',
				'falert_agency_male_profiles',
				'falert_agency_female_profiles',
				'falert_agency_comments'
            ];

			$app_langs = Cubix_Application::getLangs();
			$langs = array();
			foreach ($app_langs as $key =>$app_lang) {
				$langs[] = $app_lang->id;
			}
			$translations = array();
			foreach ($translation_keys as $translation_key) {
				foreach ($langs as $lang) {
					$translations[$lang][$translation_key] = Cubix_I18n::translateByLng($lang, $translation_key, array('city' => '#city#'));
				}
			}

			$users = array();
			
			$sql = "SELECT fu.email, u.email as user_email, u.lang as user_lang, fu.lang as fu_lang
						FROM follow_users as fu 
						LEFT JOIN users u ON u.id = fu.user_id AND u.status = ?
						GROUP BY fu.user_id, fu.email
					";

			$tmp_users = $this->_db->fetchAll($sql, array(USER_STATUS_ACTIVE));

			foreach ($tmp_users as $key => $tmp_user) {
				$email = $tmp_user->email ? $tmp_user->email : $tmp_user->user_email;
				$lang = $tmp_user->user_lang ? $tmp_user->user_lang : ($tmp_user->fu_lang ? $tmp_user->fu_lang : 'en');
				$users[$email] = array();
				$users[$email]['lang'] = $lang;
			}

			// ESCORT COMMENTS
			$sql = "SELECT fe.id, fe.rel_id, fe.rel_id2, fe.event,
					fu.user_id, fu.id as follow_id, 
					IF (fu.email IS NULL, u.email, fu.email) AS email,
					e.id as escort_id, e.showname, eph.hash, eph.ext
					FROM follow_events fe
					INNER JOIN follow_users fu ON fu.type_id = fe.type_id AND fu.type = fe.type
					INNER JOIN follow_users_events fue ON fue.follow_user_id = fu.id AND fue.event = fe.event
					INNER JOIN escorts e ON e.id = fe.type_id   
					INNER JOIN comments c ON c.id = fe.rel_id
					INNER JOIN escort_photos eph on eph.escort_id = fe.type_id AND is_main = 1
					LEFT JOIN users u ON u.id = fu.user_id AND u.status = ?
					WHERE fe.event = ? AND c.status = ? AND e.status & 32 AND fe.sent = 0 AND fu.status = 1
					GROUP BY email, fe.type_id
				";
			$follow_comments = $this->_db->fetchAll($sql,array(USER_STATUS_ACTIVE, FOLLOW_ESCORT_NEW_COMMENT, COMMENT_ACTIVE ));
			
			foreach ($follow_comments as $key => $follow_comment) {
				if($follow_comment->email !== null){
					$users[$follow_comment->email]['escorts'][$follow_comment->escort_id]['types'][] = $translations[$users[$follow_comment->email]['lang']]['falert_escort_comments'];
					$img = new Model_Escort_PhotoItem(array(
						'application_id' => Cubix_Application::getId(),
						'escort_id' => $follow_comment->escort_id,
						'hash' => $follow_comment->hash,
						'ext' => $follow_comment->ext,
					));
					$users[$follow_comment->email]['escorts'][$follow_comment->escort_id]['showname'] = $follow_comment->showname;
					$users[$follow_comment->email]['escorts'][$follow_comment->escort_id]['photo'] = $img->getUrl('thumb_ed_v3_nomark');
					$users[$follow_comment->email]['escorts'][$follow_comment->escort_id]['link'] = Cubix_Application::getById()->url .'/escort/'. $follow_comment->showname .'-'. $follow_comment->escort_id;
				}
			}

			// ESCORT REVIEWS
			$sql = "SELECT fe.id, fe.rel_id, fe.rel_id2, fe.event,
					fu.user_id, fu.id as follow_id,
					IF (fu.email IS NULL, u.email, fu.email) AS email,
					e.id as escort_id, e.showname, eph.hash, eph.ext
					FROM follow_events fe
					INNER JOIN follow_users fu ON fu.type_id = fe.type_id AND fu.type = fe.type
					INNER JOIN follow_users_events fue ON fue.follow_user_id = fu.id AND fue.event = fe.event
					INNER JOIN escorts e ON e.id = fe.type_id   
					INNER JOIN reviews r ON r.id = fe.rel_id
					INNER JOIN escort_photos eph on eph.escort_id = fe.type_id AND is_main = 1
					LEFT JOIN users u ON u.id = fu.user_id AND u.status = ?
					WHERE fe.event = ? AND r.status = ? AND e.status & 32 AND fe.sent = 0 AND fu.status = 1
					GROUP BY email, fe.type_id
				";
			$follow_reviews = $this->_db->fetchAll($sql,array(USER_STATUS_ACTIVE, FOLLOW_ESCORT_NEW_REVIEW, REVIEW_APPROVED ));

			foreach ($follow_reviews as $key => $follow_review) {
				if($follow_review->email !== null){
					$users[$follow_review->email]['escorts'][$follow_review->escort_id]['types'][] = $translations[$users[$follow_review->email]['lang']]['falert_escort_reviews'];
					$img = new Model_Escort_PhotoItem(array(
						'application_id' => Cubix_Application::getId(),
						'escort_id' => $follow_review->escort_id,
						'hash' => $follow_review->hash,
						'ext' => $follow_review->ext,
					));
					$users[$follow_review->email]['escorts'][$follow_review->escort_id]['showname'] = $follow_review->showname;
					$users[$follow_review->email]['escorts'][$follow_review->escort_id]['photo'] = $img->getUrl('thumb_ed_v3_nomark');
					$users[$follow_review->email]['escorts'][$follow_review->escort_id]['link'] = Cubix_Application::getById()->url .'/escort/'. $follow_review->showname .'-'. $follow_review->escort_id;
				}
			}

			//ESCORT PICTURES
			$sql = "SELECT fe.id, fe.rel_id, fe.rel_id2, fe.event,
					fu.user_id, fu.id as follow_id,	e.id as escort_id, e.showname,
					IF (fu.email IS NULL, u.email, fu.email) AS email,
					eph.hash, eph.ext
					FROM follow_events fe
					INNER JOIN follow_users fu ON fu.type_id = fe.type_id AND fu.type = fe.type
					INNER JOIN follow_users_events fue ON fue.follow_user_id = fu.id AND fue.event = fe.event
					INNER JOIN escorts e ON e.id = fe.type_id   
					INNER JOIN escort_photos eph on eph.escort_id = fe.type_id AND is_main = 1
					INNER JOIN escort_photos eph2 on eph2.id = fe.rel_id 
					LEFT JOIN users u ON u.id = fu.user_id AND u.status = ?
					WHERE fe.event = ? AND eph2.type IN (1,2) AND eph2.is_approved = 1 AND e.status & 32 AND fe.sent = 0 AND fu.status = 1
					GROUP BY email, fe.type_id
				";
			$follow_pics = $this->_db->fetchAll($sql,array(USER_STATUS_ACTIVE, FOLLOW_ESCORT_NEW_PICTURE));
			
			foreach ($follow_pics as $key => $follow_pic) {
				if($follow_pic->email !== null){
					$users[$follow_pic->email]['escorts'][$follow_pic->escort_id]['types'][] = $translations[$users[$follow_pic->email]['lang']]['falert_escort_pics'];
					$img = new Model_Escort_PhotoItem(array(
						'application_id' => Cubix_Application::getId(),
						'escort_id' => $follow_pic->escort_id,
						'hash' => $follow_pic->hash,
						'ext' => $follow_pic->ext,
					));
					$users[$follow_pic->email]['escorts'][$follow_pic->escort_id]['showname'] = $follow_pic->showname;
					$users[$follow_pic->email]['escorts'][$follow_pic->escort_id]['photo'] = $img->getUrl('thumb_ed_v3_nomark');
					$users[$follow_pic->email]['escorts'][$follow_pic->escort_id]['link'] = Cubix_Application::getById()->url .'/escort/'. $follow_pic->showname .'-'. $follow_pic->escort_id;
				}
			}

			// FOLLOW ESCORT PROFILES
			$sql = "SELECT fe.id, fe.rel_id, fe.rel_id2, fe.event,
					fu.user_id, fu.id as follow_id,
					e.id as escort_id, e.showname,
					IF (fu.email IS NULL, u.email, fu.email) AS email,
					eph.hash, eph.ext
					FROM follow_events fe
					INNER JOIN follow_users fu ON fu.type_id = fe.type_id AND fu.type = fe.type
					INNER JOIN follow_users_events fue ON fue.follow_user_id = fu.id AND fue.event = fe.event
					INNER JOIN escorts e ON e.id = fe.type_id   
					INNER JOIN escort_photos eph on eph.escort_id = fe.type_id AND is_main = 1
					LEFT JOIN users u ON u.id = fu.user_id AND u.status = ?
					WHERE fe.event = ? AND e.status & 32 AND fe.sent = 0 AND fu.status = 1
					GROUP BY email, fe.type_id
				";
			$follow_profiles = $this->_db->fetchAll($sql,array(USER_STATUS_ACTIVE, FOLLOW_ESCORT_NEW_PROFILE));
						

			foreach ($follow_profiles as $key => $follow_profile) {
				if($follow_profile->email !== null){
					$users[$follow_profile->email]['escorts'][$follow_profile->escort_id]['types'][] = $translations[$users[$follow_profile->email]['lang']]['falert_escort_profiles'];
					$img = new Model_Escort_PhotoItem(array(
						'application_id' => Cubix_Application::getId(),
						'escort_id' => $follow_profile->escort_id,
						'hash' => $follow_profile->hash,
						'ext' => $follow_profile->ext,
					));
					$users[$follow_profile->email]['escorts'][$follow_profile->escort_id]['showname'] = $follow_profile->showname;
					$users[$follow_profile->email]['escorts'][$follow_profile->escort_id]['photo'] = $img->getUrl('thumb_ed_v3_nomark');
					$users[$follow_profile->email]['escorts'][$follow_profile->escort_id]['link'] = Cubix_Application::getById()->url .'/escort/'. $follow_profile->showname .'-'. $follow_profile->escort_id;
				}
			}

			// FOLLOW ESCORT PRICES
			$sql = "SELECT fe.id, fe.rel_id, fe.rel_id2, fe.event,
					fu.user_id, fu.id as follow_id,
					e.id as escort_id, e.showname,
					IF (fu.email IS NULL, u.email, fu.email) AS email,
					eph.hash, eph.ext
					FROM follow_events fe
					INNER JOIN follow_users fu ON fu.type_id = fe.type_id AND fu.type = fe.type
					INNER JOIN follow_users_events fue ON fue.follow_user_id = fu.id AND fue.event = fe.event
					INNER JOIN escorts e ON e.id = fe.type_id   
					INNER JOIN escort_photos eph on eph.escort_id = fe.type_id AND is_main = 1
					LEFT JOIN users u ON u.id = fu.user_id AND u.status = ?
					WHERE fe.event = ? AND e.status & 32 AND fe.sent = 0 AND fu.status = 1
					GROUP BY email, fe.type_id
				";
			$follow_prices = $this->_db->fetchAll($sql, array(USER_STATUS_ACTIVE, FOLLOW_ESCORT_NEW_PRICE ));
								
				
			foreach ($follow_prices as $key => $follow_price) {
				if($follow_price->email !== null){
					$users[$follow_price->email]['escorts'][$follow_price->escort_id]['types'][] = $translations[$users[$follow_price->email]['lang']]['falert_escort_prices'];
					$img = new Model_Escort_PhotoItem(array(
						'application_id' => Cubix_Application::getId(),
						'escort_id' => $follow_price->escort_id,
						'hash' => $follow_price->hash,
						'ext' => $follow_price->ext,
					));
					$users[$follow_price->email]['escorts'][$follow_price->escort_id]['showname'] = $follow_price->showname;
					$users[$follow_price->email]['escorts'][$follow_price->escort_id]['photo'] = $img->getUrl('thumb_ed_v3_nomark');
					$users[$follow_price->email]['escorts'][$follow_price->escort_id]['link'] = Cubix_Application::getById()->url .'/escort/'. $follow_price->showname .'-'. $follow_price->escort_id;
				}
			}

			
			// FOLLOW_ESCORT_NEW_CITY CITY
			$sql = "SELECT fe.id, fe.rel_id, fe.rel_id2, fe.event,
					fu.user_id, fu.id as follow_id,
					IF (fu.email IS NULL, u.email, fu.email) AS email,
					e.id as escort_id, e.showname, eph.hash, eph.ext, ci.title_en as city, ci.slug as city_slug, co.title_en as country
					FROM follow_events fe
					INNER JOIN follow_users fu ON fu.type_id = fe.type_id AND fu.type = fe.type
					INNER JOIN follow_users_events fue ON fue.follow_user_id = fu.id AND fue.event = fe.event
					INNER JOIN escorts e ON e.id = fe.type_id   
					INNER JOIN escort_photos eph on eph.escort_id = fe.type_id AND is_main = 1
					INNER JOIN escort_cities ec ON ec.escort_id = e.id AND ec.city_id = fe.rel_id
					INNER JOIN cities ci ON ci.id = ec.city_id
					INNER JOIN countries co ON co.id = ci.country_id
					LEFT JOIN users u ON u.id = fu.user_id AND u.status = ?
					WHERE fe.event = ? AND fe.rel_id2 = ? AND e.status & 32 AND fe.sent = 0 AND fu.status = 1 AND fue.extra LIKE CONCAT('%|',fe.rel_id, '|%')
					GROUP BY email, fe.type_id
				";
			$follow_cities = $this->_db->fetchAll($sql,array(USER_STATUS_ACTIVE, FOLLOW_ESCORT_NEW_CITY, FOLLOW_NEW_CITY_CITY ));

			

			foreach ($follow_cities as $key => $follow_city) {
				if($follow_city->email !== null){
					$users[$follow_city->email]['escorts'][$follow_city->escort_id]['types'][] = str_replace('#city#', $follow_city->city, $translations[$users[$follow_city->email]['lang']]['falert_escort_visits']);
					$img = new Model_Escort_PhotoItem(array(
						'application_id' => Cubix_Application::getId(),
						'escort_id' => $follow_city->escort_id,
						'hash' => $follow_city->hash,
						'ext' => $follow_city->ext,
					));
					$users[$follow_city->email]['escorts'][$follow_city->escort_id]['showname'] = $follow_city->showname;
					$users[$follow_city->email]['escorts'][$follow_city->escort_id]['photo'] = $img->getUrl('thumb_ed_v3_nomark');
					$users[$follow_city->email]['escorts'][$follow_city->escort_id]['link'] = Cubix_Application::getById()->url .'/escort/'. $follow_city->showname .'-'. $follow_city->escort_id;
				}
			}
			
			// FOLLOW_ESCORT_NEW_CITY TOUR 
			$sql = "SELECT fe.id, fe.rel_id, fe.rel_id2, fe.event,
					fu.user_id, fu.id as follow_id,
					IF (fu.email IS NULL, u.email, fu.email) AS email,
					e.id as escort_id, e.showname,
					IF (DATE(t.date_from) < NOW(),0 , 1) AS is_upcoming, DATE(t.date_from) as tour_from, DATE(t.date_to) as tour_to,
					eph.hash, eph.ext, ci.title_en as city , ci.slug as city_slug , co.title_en as country
					FROM follow_events fe
					INNER JOIN follow_users fu ON fu.type_id = fe.type_id AND fu.type = fe.type
					INNER JOIN follow_users_events fue ON fue.follow_user_id = fu.id AND fue.event = fe.event
					INNER JOIN escorts e ON e.id = fe.type_id   
					INNER JOIN escort_photos eph on eph.escort_id = fe.type_id AND is_main = 1
					INNER JOIN tours t ON t.escort_id = e.id AND t.city_id = fe.rel_id
					INNER JOIN cities ci ON ci.id = t.city_id
					INNER JOIN countries co ON co.id = ci.country_id
					LEFT JOIN users u ON u.id = fu.user_id AND u.status = ?
					WHERE fe.event = ? AND fe.rel_id2 = ? AND e.status & 32 AND fe.sent = 0 AND fu.status = 1 AND fue.extra LIKE CONCAT('%|',fe.rel_id, '|%')
					GROUP BY email, fe.type_id
				";
			$follow_tours = $this->_db->fetchAll($sql, array(USER_STATUS_ACTIVE, FOLLOW_ESCORT_NEW_CITY, FOLLOW_NEW_CITY_TOUR ));

			
			foreach ($follow_tours as $key => $follow_tour) {
				if($follow_tour->email !== null){
					$users[$follow_tour->email]['escorts'][$follow_tour->escort_id]['types'][] = str_replace('#city#', $follow_tour->city, $translations[$users[$follow_tour->email]['lang']]['falert_escort_tours']);
					$img = new Model_Escort_PhotoItem(array(
						'application_id' => Cubix_Application::getId(),
						'escort_id' => $follow_tour->escort_id,
						'hash' => $follow_tour->hash,
						'ext' => $follow_tour->ext,
					));
					$users[$follow_tour->email]['escorts'][$follow_tour->escort_id]['showname'] = $follow_tour->showname;
					$users[$follow_tour->email]['escorts'][$follow_tour->escort_id]['photo'] = $img->getUrl('thumb_ed_v3_nomark');
					$users[$follow_tour->email]['escorts'][$follow_tour->escort_id]['link'] = Cubix_Application::getById()->url .'/escort/'. $follow_tour->showname .'-'. $follow_tour->escort_id;
				}
			}

			
			// FOLLOW AGENCY COMMENTS
			$sql = "SELECT fe.id, fe.type_id , fe.rel_id, fe.event,
					fu.user_id,  fu.id as follow_id,
					IF (fu.email IS NULL, u.email, fu.email) AS email,
					a.id as agency_id, a.name as agency_showname, a.slug, a.logo_hash, a.logo_ext
					FROM follow_events fe
					INNER JOIN follow_users fu ON fu.type_id = fe.type_id AND fu.type = fe.type
					INNER JOIN follow_users_events fue ON fue.follow_user_id = fu.id AND fue.event = fe.event
					INNER JOIN agencies a ON a.id = fe.type_id
					INNER JOIN agency_comments ac ON ac.id = fe.rel_id
					LEFT JOIN users u ON u.id = fu.user_id AND u.status = ?
					LEFT JOIN users u2 ON u2.id = ac.user_id
					WHERE fe.event = ? AND ac.status = ? AND fe.sent = 0 AND fu.status = 1
					GROUP BY email, fe.type_id
				";
			$follow_agency_comments = $this->_db->fetchAll($sql,array(USER_STATUS_ACTIVE, FOLLOW_AGENCY_NEW_COMMENT, AGENCY_COMMENT_STATUS_ACTIVE ));


			foreach ($follow_agency_comments as $key => $follow_agency_comment) {
				if($follow_agency_comment->email !== null){
					$users[$follow_agency_comment->email]['agencies'][$follow_agency_comment->agency_id]['types'][] = $translations[$users[$follow_agency_comment->email]['lang']]['falert_agency_comments'];
					$img = new Model_AgencyItem(array(
						'application_id' => Cubix_Application::getId(),
						'logo_hash' => $follow_agency_comment->hash,
						'logo_ext' => $follow_agency_comment->ext,
					));
					$users[$follow_agency_comment->email]['agencies'][$follow_agency_comment->agency_id]['showname'] = $follow_agency_comment->agency_showname;
					$users[$follow_agency_comment->email]['agencies'][$follow_agency_comment->agency_id]['photo'] = $img->getLogoUrl('thumb_ed_v3_nomark');
					$users[$follow_agency_comment->email]['agencies'][$follow_agency_comment->agency_id]['link'] = Cubix_Application::getById()->url .'/agency/'. $follow_agency_comment->slug .'-'. $follow_agency_comment->agency_id;
				}
			}

			// FOLLOW AGENCY REVIEWS 
			$sql = "SELECT fe.id, fe.type_id , fe.rel_id, fe.rel_id2, fe.event,
					fu.user_id, fu.id as follow_id,
					IF (fu.email IS NULL, u.email, fu.email) AS email,
					e.showname, e.id as escort_id, eph.hash, eph.ext, a.id as agency_id, a.name as agency_showname, a.slug, a.logo_hash, a.logo_ext
					FROM follow_events fe
					INNER JOIN follow_users fu ON fu.type_id = fe.type_id AND fu.type = fe.type
					INNER JOIN follow_users_events fue ON fue.follow_user_id = fu.id AND fue.event = fe.event
					INNER JOIN escorts e ON e.id = fe.rel_id2
					INNER JOIN escort_photos eph on eph.escort_id = e.id AND is_main = 1
					INNER JOIN reviews r ON r.id = fe.rel_id
					INNER JOIN agencies a ON a.id = fe.type_id
					LEFT JOIN users u ON u.id = fu.user_id AND u.status = ?
					WHERE fe.event = ? AND r.status = ? AND e.status & 32 AND fe.sent = 0 AND fu.status = 1
					GROUP BY email, fe.type_id
				";
			$follow_agency_reviews = $this->_db->fetchAll($sql,array(USER_STATUS_ACTIVE, FOLLOW_AGENCY_NEW_REVIEW, REVIEW_APPROVED ));
			

			foreach ($follow_agency_reviews as $key => $follow_agency_review) {
				if($follow_agency_review->email !== null){
					$users[$follow_agency_review->email]['agencies'][$follow_agency_review->agency_id]['types'][] = $translations[$users[$follow_agency_review->email]['lang']]['falert_agency_reviews'];
					$img = new Model_AgencyItem(array(
						'application_id' => Cubix_Application::getId(),
						'logo_hash' => $follow_agency_review->hash,
						'logo_ext' => $follow_agency_review->ext,
					));
					$users[$follow_agency_review->email]['agencies'][$follow_agency_review->agency_id]['showname'] = $follow_agency_review->agency_showname;
					$users[$follow_agency_review->email]['agencies'][$follow_agency_review->agency_id]['photo'] = $img->getLogoUrl('thumb_ed_v3_nomark');
					$users[$follow_agency_review->email]['agencies'][$follow_agency_review->agency_id]['link'] = Cubix_Application::getById()->url .'/agency/'. $follow_agency_review->slug .'-'. $follow_agency_review->agency_id;
				}
			}
			

			// FOLLOW AGENCY NEW FEMALE PROFILE  
			$sql = "SELECT fe.id, fe.type, fe.rel_id, fe.rel_id2, fe.event,
					fu.user_id, fu.id as follow_id, e.showname,
					IF (fu.email IS NULL, u.email, fu.email) AS email,
					eph.hash, eph.ext, a.id as agency_id, a.name as agency_showname, a.slug, a.logo_hash, a.logo_ext
					FROM follow_events fe
					INNER JOIN follow_users fu ON fu.type_id = fe.type_id AND fu.type = fe.type
					INNER JOIN follow_users_events fue ON fue.follow_user_id = fu.id AND fue.event = fe.event
					INNER JOIN agencies a ON a.id = fe.type_id
					INNER JOIN escorts e ON e.id = fe.rel_id
					INNER JOIN escort_photos eph on eph.escort_id = fe.type_id AND is_main = 1
					LEFT JOIN users u ON u.id = fu.user_id AND u.status = ?
					WHERE fe.event = ? AND e.status & 32 AND fe.sent = 0 AND fu.status = 1
					GROUP BY email, fe.type_id
				";
			
			$follow_new_female_profiles = $this->_db->fetchAll($sql,array(USER_STATUS_ACTIVE, FOLLOW_AGENCY_NEW_FEMALE));
			

			foreach ($follow_new_female_profiles as $key => $follow_new_female_profile) {
				if($follow_new_female_profile->email !== null){
					$users[$follow_new_female_profile->email]['agencies'][$follow_new_female_profile->agency_id]['types'][] = $translations[$users[$follow_new_female_profile->email]['lang']]['falert_agency_female_profiles'];
					$img = new Model_AgencyItem(array(
						'application_id' => Cubix_Application::getId(),
						'logo_hash' => $follow_new_female_profile->hash,
						'logo_ext' => $follow_new_female_profile->ext,
					));
					$users[$follow_new_female_profile->email]['agencies'][$follow_new_female_profile->agency_id]['showname'] = $follow_new_female_profile->agency_showname;
					$users[$follow_new_female_profile->email]['agencies'][$follow_new_female_profile->agency_id]['photo'] = $img->getLogoUrl('thumb_ed_v3_nomark');
					$users[$follow_new_female_profile->email]['agencies'][$follow_new_female_profile->agency_id]['link'] = Cubix_Application::getById()->url .'/agency/'. $follow_new_female_profile->slug .'-'. $follow_new_female_profile->agency_id;
				}
			}

			// FOLLOW AGENCY NEW MALE PROFILE  
			$sql = "SELECT fe.id, fe.type, fe.rel_id, fe.rel_id2, fe.event,
					fu.user_id, fu.id as follow_id, e.showname,
					IF (fu.email IS NULL, u.email, fu.email) AS email,
					eph.hash, eph.ext, a.id as agency_id, a.name as agency_showname, a.slug, a.logo_hash, a.logo_ext
					FROM follow_events fe
					INNER JOIN follow_users fu ON fu.type_id = fe.type_id AND fu.type = fe.type
					INNER JOIN follow_users_events fue ON fue.follow_user_id = fu.id AND fue.event = fe.event
					INNER JOIN agencies a ON a.id = fe.type_id
					INNER JOIN escorts e ON e.id = fe.rel_id
					INNER JOIN escort_photos eph on eph.escort_id = fe.type_id AND is_main = 1
					LEFT JOIN users u ON u.id = fu.user_id AND u.status = ?
					WHERE fe.event = ? AND e.status & 32 AND fe.sent = 0 AND fu.status = 1
					GROUP BY email, fe.type_id
				";
			
			$follow_new_male_profiles = $this->_db->fetchAll($sql,array(USER_STATUS_ACTIVE, FOLLOW_AGENCY_NEW_MALE));
			

			foreach ($follow_new_male_profiles as $key => $follow_new_male_profile) {
				if($follow_new_male_profile->email !== null){
					$users[$follow_new_male_profile->email]['agencies'][$follow_new_male_profile->agency_id]['types'][] = $translations[$users[$follow_new_male_profile->email]['lang']]['falert_agency_male_profiles'];
					$img = new Model_AgencyItem(array(
						'application_id' => Cubix_Application::getId(),
						'logo_hash' => $follow_new_male_profile->hash,
						'logo_ext' => $follow_new_male_profile->ext,
					));
					$users[$follow_new_male_profile->email]['agencies'][$follow_new_male_profile->agency_id]['showname'] = $follow_new_male_profile->agency_showname;
					$users[$follow_new_male_profile->email]['agencies'][$follow_new_male_profile->agency_id]['photo'] = $img->getLogoUrl('thumb_ed_v3_nomark');
					$users[$follow_new_male_profile->email]['agencies'][$follow_new_male_profile->agency_id]['link'] = Cubix_Application::getById()->url .'/agency/'. $follow_new_male_profile->slug .'-'. $follow_new_male_profile->agency_id;
				}
			}

			// FOLLOW AGENCY NEW TRANS PROFILE  
			$sql = "SELECT fe.id, fe.type, fe.rel_id, fe.rel_id2, fe.event,
					fu.user_id, fu.id as follow_id, e.showname,
					IF (fu.email IS NULL, u.email, fu.email) AS email,
					eph.hash, eph.ext, a.id as agency_id, a.name as agency_showname, a.slug, a.logo_hash, a.logo_ext
					FROM follow_events fe
					INNER JOIN follow_users fu ON fu.type_id = fe.type_id AND fu.type = fe.type
					INNER JOIN follow_users_events fue ON fue.follow_user_id = fu.id AND fue.event = fe.event
					INNER JOIN agencies a ON a.id = fe.type_id
					INNER JOIN escorts e ON e.id = fe.rel_id
					INNER JOIN escort_photos eph on eph.escort_id = fe.type_id AND is_main = 1
					LEFT JOIN users u ON u.id = fu.user_id AND u.status = ?
					WHERE fe.event = ? AND e.status & 32 AND fe.sent = 0 AND fu.status = 1
					GROUP BY email, fe.type_id
				";
			
			$follow_new_trans_profiles = $this->_db->fetchAll($sql,array(USER_STATUS_ACTIVE, FOLLOW_AGENCY_NEW_TRANS));
			

			foreach ($follow_new_trans_profiles as $key => $follow_new_trans_profile) {
				if($follow_new_trans_profile->email !== null){
					$users[$follow_new_trans_profile->email]['agencies'][$follow_new_trans_profile->agency_id]['types'][] = $translations[$users[$follow_new_trans_profile->email]['lang']]['falert_agency_trans_profiles'];
					$img = new Model_AgencyItem(array(
						'application_id' => Cubix_Application::getId(),
						'logo_hash' => $follow_new_trans_profile->hash,
						'logo_ext' => $follow_new_trans_profile->ext,
					));
					$users[$follow_new_trans_profile->email]['agencies'][$follow_new_trans_profile->agency_id]['showname'] = $follow_new_trans_profile->agency_showname;
					$users[$follow_new_trans_profile->email]['agencies'][$follow_new_trans_profile->agency_id]['photo'] = $img->getLogoUrl('thumb_ed_v3_nomark');
					$users[$follow_new_trans_profile->email]['agencies'][$follow_new_trans_profile->agency_id]['link'] = Cubix_Application::getById()->url .'/agency/'. $follow_new_trans_profile->slug .'-'. $follow_new_trans_profile->agency_id;
				}
			}



			// FOLLOW AGENCY NEW_CITY CITY
			$sql = "SELECT fe.id, fe.type, fe.rel_id, fe.rel_id2, fe.event,
					fu.user_id, fu.id as follow_id,
					IF (fu.email IS NULL, u.email, fu.email) AS email,
					e.showname, e.id as escort_id, eph.hash, eph.ext,
					ci.title_en as city, ci.slug as city_slug, co.title_en as country, a.id as agency_id, a.name as agency_showname, a.slug, a.logo_hash, a.logo_ext
					FROM follow_events fe
					INNER JOIN follow_users fu ON fu.type_id = fe.type_id AND fu.type = fe.type
					INNER JOIN follow_users_events fue ON fue.follow_user_id = fu.id AND fue.event = fe.event
					INNER JOIN agencies a ON a.id = fe.type_id
					INNER JOIN escorts e ON e.id = fe.rel_id3   
					INNER JOIN escort_photos eph on eph.escort_id = e.id AND is_main = 1
					INNER JOIN escort_cities ec ON ec.escort_id = e.id AND ec.city_id = fe.rel_id
					INNER JOIN cities ci ON ci.id = ec.city_id
					INNER JOIN countries co ON co.id = ci.country_id
					LEFT JOIN users u ON u.id = fu.user_id AND u.status = ?
					WHERE fe.event = ? AND fe.rel_id2 = ? AND e.status & 32 AND fe.sent = 0 AND fu.status = 1 AND fue.extra LIKE CONCAT('%|',fe.rel_id, '|%')
					GROUP BY email, fe.type_id
				";
			$follow_agency_cities = $this->_db->fetchAll($sql,array(USER_STATUS_ACTIVE, FOLLOW_AGENCY_NEW_CITY, FOLLOW_NEW_CITY_CITY ));
			

			foreach ($follow_agency_cities as $key => $follow_agency_city) {
				if($follow_agency_city->email !== null){

					$users[$follow_agency_city->email]['agencies'][$follow_agency_city->agency_id]['types'][] = str_replace('#city#', $follow_agency_city->city, $translations[$users[$follow_agency_city->email]['lang']]['falert_agency_visits']);


					$img = new Model_AgencyItem(array(
						'application_id' => Cubix_Application::getId(),
						'logo_hash' => $follow_agency_city->hash,
						'logo_ext' => $follow_agency_city->ext,
					));
					$users[$follow_agency_city->email]['agencies'][$follow_agency_city->agency_id]['showname'] = $follow_agency_city->agency_showname;
					$users[$follow_agency_city->email]['agencies'][$follow_agency_city->agency_id]['photo'] = $img->getLogoUrl('thumb_ed_v3_nomark');
					$users[$follow_agency_city->email]['agencies'][$follow_agency_city->agency_id]['link'] = Cubix_Application::getById()->url .'/agency/'. $follow_agency_city->slug .'-'. $follow_agency_city->agency_id;
				}
			}

			// FOLLOW AGENCY NEW_CITY TOUR 
			$sql = "SELECT fe.id, fe.type, fe.rel_id, fe.rel_id2, fe.event,
					fu.user_id, fu.id as follow_id,
					e.showname, e.id as escort_id, eph.hash, eph.ext,
					IF (fu.email IS NULL, u.email, fu.email) AS email,
					IF (DATE(t.date_from) < NOW(),0 , 1) AS is_upcoming, DATE(t.date_from) as tour_from, DATE(t.date_to) as tour_to,
					ci.title_en as city, ci.slug as city_slug, co.title_en as country, a.id as agency_id, a.name as agency_showname, a.slug, a.logo_hash, a.logo_ext
					FROM follow_events fe
					INNER JOIN follow_users fu ON fu.type_id = fe.type_id AND fu.type = fe.type
					INNER JOIN follow_users_events fue ON fue.follow_user_id = fu.id AND fue.event = fe.event
					INNER JOIN agencies a ON a.id = fe.type_id
					INNER JOIN escorts e ON e.id = fe.rel_id3
					INNER JOIN escort_photos eph on eph.escort_id = fe.type_id AND is_main = 1
					INNER JOIN tours t ON t.escort_id = e.id AND t.city_id = fe.rel_id
					INNER JOIN cities ci ON ci.id = t.city_id
					INNER JOIN countries co ON co.id = ci.country_id
					LEFT JOIN users u ON u.id = fu.user_id AND u.status = ?
					WHERE fe.event = ? AND fe.rel_id2 = ? AND e.status & 32 AND fe.sent = 0 AND fu.status = 1 AND fue.extra LIKE CONCAT('%|',fe.rel_id, '|%')
					GROUP BY email, fe.type_id
				";
			$follow_agency_tours = $this->_db->fetchAll($sql,array(USER_STATUS_ACTIVE, FOLLOW_AGENCY_NEW_CITY, FOLLOW_NEW_CITY_TOUR ));

			foreach ($follow_agency_tours as $key => $follow_agency_tour) {
				if($follow_agency_tour->email !== null){
					if($follow_agency_tour->is_upcoming){
						$users[$follow_agency_tour->email]['agencies'][$follow_agency_tour->agency_id]['types'][] = str_replace('#city#', $follow_agency_tour->city, $translations[$users[$follow_agency_tour->email]['lang']]['falert_agency_upcoming_visits']);


					} else{
						$users[$follow_agency_tour->email]['agencies'][$follow_agency_tour->agency_id]['types'][] = str_replace('#city#', $follow_agency_tour->city, $translations[$users[$follow_agency_tour->email]['lang']]['falert_agency_visits']);
					}

					$img = new Model_AgencyItem(array(
						'application_id' => Cubix_Application::getId(),
						'logo_hash' => $follow_agency_tour->hash,
						'logo_ext' => $follow_agency_tour->ext,
					));
					$users[$follow_agency_tour->email]['agencies'][$follow_agency_tour->agency_id]['showname'] = $follow_agency_tour->agency_showname;
					$users[$follow_agency_tour->email]['agencies'][$follow_agency_tour->agency_id]['photo'] = $img->getLogoUrl('thumb_ed_v3_nomark');
					$users[$follow_agency_tour->email]['agencies'][$follow_agency_tour->agency_id]['link'] = Cubix_Application::getById()->url .'/agency/'. $follow_agency_tour->slug .'-'. $follow_agency_tour->agency_id;
				}
			}
			
			
			$this->_db->update('follow_events', array('sent' => 1, 'sent_date' => new Zend_Db_Expr('NOW()') ), array( 'sent = ?' => 0 , 'date <= ?' => $current_time) );
			$template_id = 'follow_alert_v3';
			$it = 0;
			foreach ($users as $email => $emailData) {
				if((count($emailData['escorts']) || count($emailData['agencies'])) && isset($emailData['lang'])){
					$it++;

					$merged_data = array();
					if(count($emailData['escorts'])){
						foreach ($emailData['escorts'] as $key => $value) {
							$merged_data[] = $value;
						}
					}
					if(count($emailData['agencies'])){
						foreach ($emailData['agencies'] as $key => $value) {
							$merged_data[] = $value;
						}
					}
					$final_data = array_chunk($merged_data, 6);

					foreach ($final_data as $fd) {
						$this->view->data = $fd;
						$this->view->title = $translations[$emailData['lang']]['falert_title'];
						$this->view->button_text = $translations[$emailData['lang']]['falert_button_text'];
                        $this->view->view_profile_button_text = $translations[$emailData['lang']]['view_profile_button_text'];
						$this->view->account_button_text = $translations[$emailData['lang']]['falert_account_button_text'];
						$html = $this->view->render('cron/follow-alert-ed.phtml');

						Cubix_Email::sendTemplate($template_id, $email, array('html' => $html), null, null, $emailData['lang']);
						
						$cli->out(' Email sent to   ', false);
						$cli->colorize('green')->out($email)->reset();
						$cli->colorize('yellow')->out($emailData['lang'])->reset();
					}
				}
			}
			
						
			$end_time = microtime(true);
			// Just print the execution time
			$cli->colorize('purple')->out('Execution took ' . round($end_time - $start_time, 4) . ' seconds')->reset()->out();
		} catch (Exception $e) {
			echo $e->getMessage();
			echo " [fail]\n";
		}
		die;
	}

	
	private function sendFollowEmail($data, $template_id, $cli)
	{
		$DEFINITIONS = Zend_Registry::get('defines');
		$escort_model = new Model_Escorts();
		
		$email = $data->email ? $data->email : $data->user_email;
		if(is_null($email)){
			return;
		}	

		$email_data = array(
			'member' => $data->member ? $data->member : "dear follower",
		);
		
		if(!in_array($data->event, array(FOLLOW_AGENCY_NEW_COMMENT, FOLLOW_AGENCY_NEW_REVIEW))){
			
			$about_text = strip_tags($data->about_en);

			if ( mb_strlen($about_text) > 120 ){
				$about_text = _shorten($about_text, 120);
			}

			$rates = $escort_model->getsmallestRate($data->escort_id);
			if($rates){
				$rate = $rates->price .' '. $DEFINITIONS['currencies'][$rates->currency_id] .' / '. $rates->time ." ". $DEFINITIONS['time_unit_options'][$rates->time_unit];
			}
			else{
				$rate = "";
			}

			$img = new Model_Escort_PhotoItem(array(
				'application_id' => Cubix_Application::getId(),
				'escort_id' => $data->escort_id,
				'hash' => $data->hash,
				'ext' => $data->ext,
			));
			
			$email_data['escort'] = $data->showname; 
			$email_data['type'] = $data->agency_name ? $data->agency_name : 'Independent';
			$email_data['age'] = $data->birth_date ? ( date('Y') - date("Y", $data->birth_date))  : '';
			$email_data['gender'] = $DEFINITIONS['gender_options'][$data->gender];
			$email_data['category'] = $DEFINITIONS['ESCORT_TYPES'][$data->category_type];
			$email_data['rate'] = $rate;
			$email_data['about_me'] = $about_text;
			$email_data['url'] = Cubix_Application::getById()->url .'/escort/'. $data->showname .'-'. $data->escort_id;
			$email_data['photo'] = $img->getUrl('follow_email_thumb');
		}
		elseif($data->event == FOLLOW_AGENCY_NEW_REVIEW){
			$email_data['escort'] = $data->showname;
			$email_data['review_url'] = Cubix_Application::getById()->url .'/escort/'. $data->showname .'-'. $data->escort_id;
		}
		else{
			$email_data['comment_url'] = Cubix_Application::getById()->url .'/agency/'. $data->slug .'-'. $data->type_id;
		}
		
		if(in_array($data->event, array(FOLLOW_ESCORT_NEW_COMMENT, FOLLOW_ESCORT_NEW_REVIEW, FOLLOW_AGENCY_NEW_COMMENT, FOLLOW_AGENCY_NEW_REVIEW))){
			$email_data['member2'] = $data->member2;
		}
		
		if(in_array($data->event, array(FOLLOW_AGENCY_NEW_COMMENT,FOLLOW_AGENCY_NEW_REVIEW, FOLLOW_AGENCY_NEW_CITY, FOLLOW_AGENCY_NEW_FEMALE,FOLLOW_AGENCY_NEW_MALE,FOLLOW_AGENCY_NEW_TRANS))){
			$email_data['agency'] = $data->agency_name;
		}
		
		if(in_array($data->event, array(FOLLOW_ESCORT_NEW_CITY,FOLLOW_AGENCY_NEW_CITY, FOLLOW_AGENCY_NEW_FEMALE, FOLLOW_AGENCY_NEW_MALE, FOLLOW_AGENCY_NEW_TRANS))){
			$email_data['city'] = $data->city;
			$email_data['country'] = $data->country;
		}
		
		if($template_id == "follow_escort_new_upcoming_visit_v1" || $template_id == "follow_agency_new_upcoming_visit_v1"){
			$email_data['date_from'] = $data->tour_from;
			$email_data['date_to'] = $data->tour_to;
		}
		
		if($data->event == FOLLOW_ESCORT_NEW_PICTURE){
			$email_data['count'] = $data->photo_count;
		}
		
		$cli->out("Sending to ".$email.' - '. $email_data['member']);
		Cubix_Email::sendTemplate($template_id, $email, $email_data);
		$this->_db->update('follow_events',array('sent' => 1, 'sent_date' => new Zend_Db_Expr('NOW()'), ), $this->_db->quoteInto('id = ?', $data->id));
	}
	

	public function cityAlertsEdAction()
	{
		$DEFINITIONS = Zend_Registry::get('defines');

		$currencies = Model_Currencies::getAllAssoc();
		$time_units = $DEFINITIONS['time_unit_options'];

		set_time_limit(0);
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$cli = new Cubix_Cli();
		$pid_file = '/var/run/city-alerts-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}

		try {
			$items = $this->_db->query('
				SELECT cae.id, cae.escort_id, e.agency_id, ep.birth_date, cae.city_id, c.title_en AS city_title, co.id AS country_id, co.title_en AS country_title, e.showname, e.type, e.gender
				FROM city_alerts_events cae 
				INNER JOIN cities c ON c.id = cae.city_id 
				INNER JOIN countries co ON co.id = c.country_id 
				LEFT JOIN escorts e ON e.id = cae.escort_id
				LEFT JOIN escort_profiles_v2 ep ON ep.escort_id = cae.escort_id
				WHERE cae.sent = 0 AND cae.agency_id IS NULL AND cae.application_id = ?
			', Cubix_Application::getId())->fetchAll();

			if ($items)
			{
				$escort_url = 'escort';
				
				$glob_users = array();
				
				foreach ($items as $item)
				{	
					$image = false;

					//Get Photo
					////////////
					$photo = $this->_db->fetchRow('SELECT hash, ext FROM escort_photos WHERE escort_id = ? ORDER BY is_main DESC', $item->escort_id);
					if($photo){
						$p = array('application_id' => Cubix_Application::getId(), 'escort_id' => $item->escort_id, 'hash' => $photo->hash, 'ext' => $photo->ext);
						$po = new Model_Escort_PhotoItem($p);
						$image = $po->getUrl('thumb_ed_v3_nomark');
					}

					//Get rate
					$rate = false;
					$rate = $this->_db->fetchRow('SELECT * FROM escort_rates WHERE escort_id = ? AND availability = '.AVAILABLE_INCALL.' AND time_unit = '.TIME_HOURS.' AND time = 1  AND type IS NULL', $item->escort_id);
					if(!$rate){
						$rates = $this->_db->fetchAll('SELECT * FROM escort_rates WHERE escort_id = ? AND availability = '.AVAILABLE_INCALL.' AND type IS NULL ORDER BY TIME ASC', $item->escort_id);
						if($rates){
							$rate = $rates[0];	
						}
					}
					if(!$rate){
						$rate = $this->_db->fetchRow('SELECT * FROM escort_rates WHERE escort_id = ? AND availability = '.AVAILABLE_OUTCALL.' AND time_unit = '.TIME_HOURS.' AND time = 1  AND type IS NULL', $item->escort_id);
						if(!$rate){
							$rates = $this->_db->fetchAll('SELECT * FROM escort_rates WHERE escort_id = ? AND availability = '.AVAILABLE_OUTCALL.' AND type IS NULL ORDER BY TIME ASC', $item->escort_id);
							if($rates){
								$rate = $rates[0];	
							}
						}	
					}

					$rateText = ''; 

					if($rate){
						$rateText = $rate->price.' '.$currencies[$rate->currency_id].'/'.$rate->time.' '.$time_units[$rate->time_unit];
					}
					

					//Get escort rate
					$escort_category = strtolower($DEFINITIONS['ESCORT_TYPES'][$item->type]);
					$escort_gender = strtolower($DEFINITIONS['gender_options'][$item->gender]);

					if ( Cubix_Application::getId() != APP_ED ) {
                            $alerts = $this->_db->query('
                            SELECT ca.id as alert_id, ca.user_id, ca.escort_type, ca.escort_gender, ca.send_email, ca.subject, u.email, u.username as membername 
                            FROM city_alerts_v2 ca 
                            LEFT JOIN city_alerts_cities cc ON cc.alert_id = ca.id
                            INNER JOIN users u ON u.id = ca.user_id AND u.status = 1
                            WHERE cc.city_id = ? AND ca.enabled = 1
                        ', $item->city_id)->fetchAll();
                    }else{
                        $alerts = $this->_db->query('
                            SELECT ca.id as alert_id, ca.user_id, ca.escort_type, ca.escort_gender, ca.send_email, ca.subject, u.email, u.username as membername 
                            FROM city_alerts_v2 ca 
                            LEFT JOIN city_alerts_cities cc ON cc.alert_id = ca.id
                            INNER JOIN users u ON u.id = ca.user_id AND u.status = 1
                            WHERE cc.city_id = ? AND ca.enabled = 1 AND ca.escort_gender+1 = ? AND ca.escort_type+1 = ?
                        ', array( $item->city_id, $item->gender, $item->type))->fetchAll();
                    }
					if($item->agency_id){
						$agency = $this->_db->fetchRow('SELECT name FROM agencies WHERE id = ?', $item->agency_id);
						$independent_or_agency = "Agency:".$agency->name;
					}else{
						$independent_or_agency = "Independent escort";
					}

					$age = Cubix_Utils::dateToAge($item->birth_date);

					if ($alerts)
					{
						foreach ($alerts as $alert)
						{
						    if ( Cubix_Application::getId() != APP_ED ) {
                                if ($alert->escort_type != 0) {
                                    switch ($alert->escort_type) {
                                        case 1:
                                            if ($item->type != ESCORT_TYPE_ESCORT) continue;
                                            break;
                                        case 2:
                                            if ($item->type != ESCORT_TYPE_BDSM) continue;
                                            break;
                                        case 3:
                                            if ($item->type != ESCORT_TYPE_MASSAGE) continue;
                                            break;
                                        default:
                                            continue;
                                            break;
                                    }
                                }

                                switch ($alert->escort_gender) {
                                    case 0:
                                        if ($item->gender != GENDER_FEMALE) continue;
                                        break;
                                    case 1:
                                        if ($item->gender != GENDER_MALE) continue;
                                        break;
                                    case 2:
                                        if ($item->gender != GENDER_TRANS) continue;
                                        break;
                                    default:
                                        continue;
                                        break;
                                }
                            }
							$clear_alert = (array)$alert;
							unset($clear_alert['user_id']);
							unset($clear_alert['email']);
							$glob_users[$alert->user_id . '$' . $alert->email][$item->country_title][$item->city_title][] = array(
								'alert' => $clear_alert, 
								'event_id' => $item->id,
								'escort_id' => $item->escort_id,
								'escort' => Cubix_Application::getById()->url . '/' . $escort_url . '/' . $item->showname . '-' . $item->escort_id, 
								'city' => $item->city_title,
								'city_id' => $item->city_id,
								'country' => $item->country_title,
								'country_id' => $item->country_id,
								'showname' => $item->showname,
								'escort_category' => $escort_category,
								'escort_gender' => $escort_gender,
								'membername' => $alert->membername,
								'agency_name' => $item->agency_name,
								'independent_or_agency' => $independent_or_agency,
								'rateText' => $rateText,
								'image' => $image,
								'age' => $age,
							);
						}
					}
				}
			}

			$ids = array();
			if ($glob_users)
			{
				foreach ($glob_users as $user => $alert)
				{
					// print_r($alert);die;
					$ar = explode('$', $user);
					$user_id = $ar[0];
					$email = $ar[1];
					
					$city_alerts = array();
					
					$template_id = 'city_alert_new';
					$emailData = array();
					foreach ($alert as $country => $cities)
					{		
						foreach ($cities as $city => $city_a)
						{
							foreach ($city_a as $k =>  $a)
							{
								if (!in_array($a['event_id'], $ids))
									$ids[] = $a['event_id'];
								

								if($a['alert']['send_email'] == 1){
									$emailData[$country][$city][$k]['showname'] = $a['showname'];
									$emailData[$country][$city][$k]['member'] = $a['membername'];
									$emailData[$country][$city][$k]['escort_link'] = $a['escort'];
									$emailData[$country][$city][$k]['category'] = $a['escort_category'];
									$emailData[$country][$city][$k]['gender'] = $a['escort_gender'];
									$emailData[$country][$city][$k]['city'] = $a['city'];
									$emailData[$country][$city][$k]['city_id'] = $a['city_id'];
									$emailData[$country][$city][$k]['country'] = $a['country'];
									$emailData[$country][$city][$k]['country_id'] = $a['country_id'];
									$emailData[$country][$city][$k]['image'] =  $a['image'];
									$emailData[$country][$city][$k]['independent_or_agency'] = $a['independent_or_agency'];
									$emailData[$country][$city][$k]['rate'] = $a['rateText'];
									$emailData[$country][$city][$k]['age'] = $a['age'];
								
								}
								$listData = array();
								$listData['user_id'] = $user_id;
								$listData['alert_id'] = $a['alert']['alert_id'];
								$listData['escort_id'] = $a['escort_id'];
								$listData['city_id'] = $a['city_id'];

								$this->_db->insert('user_city_alert',$listData);
								
							}
						}
					}
					if(count($emailData < 3) || ( count($emailData >= 3) && date('A') == 'PM' ) ){

						foreach ($emailData as $countryName => $data) {
							if(count($data)){
								$this->view->alert_data = $data;
								$html = $this->view->render('cron/city-alert-ed.phtml');
                                Cubix_Email::sendTemplate($template_id, $email, array('html' => $html));
                            }

						}	
						$cli->out(' Email sent to ' . $email);
					}
				}
			}
			
			$str_ids = implode(',', $ids);

			// TODO: ONYX Need remove before life
			// exit();
			if (count($ids) > 0)
			{
				$this->_db->query('UPDATE city_alerts_events SET sent = 1, sent_date = now() WHERE id IN (' . $str_ids . ')');
			}
			
			$this->_db->query('UPDATE city_alerts_events SET sent = 2 WHERE sent = 0');
			
			$cli->out('done!');
		}
		catch ( Exception $e ) {
			echo $e->__toString();
			echo " [fail]\n";
		}
		exit;
	}

	public function cityAlertsV2Action()
	{
		$DEFINITIONS = Zend_Registry::get('defines');

		$currencies = Model_Currencies::getAllAssoc();
		$time_units = $DEFINITIONS['time_unit_options'];

		set_time_limit(0);
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$cli = new Cubix_Cli();
		$pid_file = '/var/run/city-alerts-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}

		try {
			$items = $this->_db->query('
				SELECT cae.id, cae.escort_id, e.agency_id, ep.birth_date, cae.city_id, c.title_en AS city_title, co.title_en AS country_title, e.showname, e.type, e.gender
				FROM city_alerts_events cae 
				INNER JOIN cities c ON c.id = cae.city_id 
				INNER JOIN countries co ON co.id = c.country_id 
				LEFT JOIN escorts e ON e.id = cae.escort_id
				LEFT JOIN escort_profiles_v2 ep ON ep.escort_id = cae.escort_id
				WHERE cae.sent = 0 AND cae.agency_id IS NULL AND cae.application_id = ?
			', Cubix_Application::getId())->fetchAll();


			if ($items)
			{
				$escort_url = 'escort';
				
				$glob_users = array();
				
				foreach ($items as $item)
				{	
					$image = false;

					//Get Photo
					////////////
					$photo = $this->_db->fetchRow('SELECT hash, ext FROM escort_photos WHERE escort_id = ? ORDER BY is_main DESC', $item->escort_id);
					if($photo){
						$p = array('application_id' => Cubix_Application::getId(), 'escort_id' => $item->escort_id, 'hash' => $photo->hash, 'ext' => $photo->ext);
						$po = new Model_Escort_PhotoItem($p);
						$image = $po->getUrl('thumb_ed_v3_nomark');
					}

					//Get rate
					$rate = false;
					$rate = $this->_db->fetchRow('SELECT * FROM escort_rates WHERE escort_id = ? AND availability = '.AVAILABLE_INCALL.' AND time_unit = '.TIME_HOURS.' AND time = 1  AND type IS NULL', $item->escort_id);
					if(!$rate){
						$rates = $this->_db->fetchAll('SELECT * FROM escort_rates WHERE escort_id = ? AND availability = '.AVAILABLE_INCALL.' AND type IS NULL ORDER BY TIME ASC', $item->escort_id);
						if($rates){
							$rate = $rates[0];	
						}
					}
					if(!$rate){
						$rate = $this->_db->fetchRow('SELECT * FROM escort_rates WHERE escort_id = ? AND availability = '.AVAILABLE_OUTCALL.' AND time_unit = '.TIME_HOURS.' AND time = 1  AND type IS NULL', $item->escort_id);
						if(!$rate){
							$rates = $this->_db->fetchAll('SELECT * FROM escort_rates WHERE escort_id = ? AND availability = '.AVAILABLE_OUTCALL.' AND type IS NULL ORDER BY TIME ASC', $item->escort_id);
							if($rates){
								$rate = $rates[0];	
							}
						}	
					}

					$rateText = ''; 

					if($rate){
						$rateText = $rate->price.' '.$currencies[$rate->currency_id].'/'.$rate->time.' '.$time_units[$rate->time_unit];
					}
					

					//Get escort rate
					$escort_category = strtolower($DEFINITIONS['ESCORT_TYPES'][$item->type]);
					$escort_gender = strtolower($DEFINITIONS['gender_options'][$item->gender]);

					$alerts = $this->_db->query('
						SELECT ca.id as alert_id, ca.user_id, ca.escort_type, ca.escort_gender, ca.send_email, ca.subject, u.email, u.username as membername 
						FROM city_alerts_v2 ca 
						LEFT JOIN city_alerts_cities cc ON cc.alert_id = ca.id
						INNER JOIN users u ON u.id = ca.user_id AND u.status = 1
						WHERE cc.city_id = ? AND ca.enabled = 1
					', $item->city_id)->fetchAll();

					if($item->agency_id){
						$agency = $this->_db->fetchRow('SELECT name FROM agencies WHERE id = ?', $item->agency_id);
						$independent_or_agency = "Agency:".$agency->name;
					}else{
						$independent_or_agency = "Independent escort";
					}

					$age = Cubix_Utils::dateToAge($item->birth_date);

					if ($alerts)
					{
						foreach ($alerts as $alert)
						{
							if($alert->escort_type != 0){
								switch ($alert->escort_type) {
									case 1:
										if($item->type != ESCORT_TYPE_ESCORT)continue;
										break;
									case 2:
										if($item->type != ESCORT_TYPE_BDSM)continue;
										break;
									case 3:
										if($item->type != ESCORT_TYPE_MASSAGE)continue;
										break;
									default:
										continue;
										break;	
								}
							}

							switch ($alert->escort_gender) {
								case 0:
									if($item->gender != GENDER_FEMALE)continue;
									break;
								case 1:
									if($item->gender != GENDER_MALE)continue;
									break;
								case 2:
									if($item->gender != GENDER_TRANS)continue;
									break;
								default:
									continue;
									break;	
							}
							
							$clear_alert = (array)$alert;
							unset($clear_alert['user_id']);
							unset($clear_alert['email']);

							$glob_users[$alert->user_id . '$' . $alert->email][] = array(
								'alert' => $clear_alert, 
								'event_id' => $item->id,
								'escort_id' => $item->escort_id,
								'escort' => Cubix_Application::getById()->url . '/' . $escort_url . '/' . $item->showname . '-' . $item->escort_id, 
								'city' => $item->city_title,
								'city_id' => $item->city_id,
								'country' => $item->country_title,
								'showname' => $item->showname,
								'escort_category' => $escort_category,
								'escort_gender' => $escort_gender,
								'membername' => $alert->membername,
								'agency_name' => $item->agency_name,
								'independent_or_agency' => $independent_or_agency,
								'rateText' => $rateText,
								'image' => $image,
								'age' => $age,
							);
						}
					}
				}
			}

			$ids = array();
			
			if ($glob_users)
			{				
				foreach ($glob_users as $user => $alert)
				{
					$ar = explode('$', $user);
					$user_id = $ar[0];
					$email = $ar[1];
					
					$city_alerts = array();
					
					$template_id = 'city_alert';
					foreach ($alert as $a)
					{				
						if (!in_array($a['event_id'], $ids))
							$ids[] = $a['event_id'];
						

						$emailData = array();
						$emailData['showname'] = $a['showname'];
						$emailData['member'] = $a['membername'];
						$emailData['escort_link'] = $a['escort'];
						$emailData['category'] = $a['escort_category'];
						$emailData['gender'] = $a['escort_gender'];
						$emailData['city'] = $a['city'];
						$emailData['country'] = $a['country'];
						$emailData['image'] =  $a['image'];
						$emailData['independent_or_agency'] = $a['independent_or_agency'];
						$emailData['rate'] = $a['rateText'];
						$emailData['age'] = $a['age'];
						
						$listData = array();
						$listData['user_id'] = $user_id;
						$listData['alert_id'] = $a['alert']['alert_id'];
						$listData['escort_id'] = $a['escort_id'];
						$listData['city_id'] = $a['city_id'];

						$this->_db->insert('user_city_alert',$listData);
						if($a['alert']['send_email'] == 1){
							// TODO: ONYX Need remove before life
							//Cubix_Email::sendTemplate($template_id, $email, $emailData);
						}
					}
					
					$cli->out(' Email sent to ' . $email);
				}
			}
			
			$str_ids = implode(',', $ids);

			// TODO: ONYX Need remove before life
			exit();
			if (count($ids) > 0)
			{
				$this->_db->query('UPDATE city_alerts_events SET sent = 1, sent_date = now() WHERE id IN (' . $str_ids . ')');
			}
			
			$this->_db->query('UPDATE city_alerts_events SET sent = 2 WHERE sent = 0');
			
			$cli->out('done!');
		}
		catch ( Exception $e ) {
			echo $e->__toString();
			echo " [fail]\n";
		}
		exit;
	}

	public function cityAlertsAction()
	{
		set_time_limit(0);
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$cli = new Cubix_Cli();
		$pid_file = '/var/run/city-alerts-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}
		
		try {
			$items = $this->_db->query('
				SELECT cae.id, cae.escort_id, cae.agency_id, cae.city_id, c.title_en AS city_title, e.showname, a.name AS agency_name, a.slug AS agency_slug 
				FROM city_alerts_events cae 
				INNER JOIN cities c ON c.id = cae.city_id 
				LEFT JOIN escorts e ON e.id = cae.escort_id 
				LEFT JOIN agencies a ON a.id = cae.agency_id 
				WHERE cae.sent = 0 AND cae.application_id = ?
			', Cubix_Application::getId())->fetchAll();
			
			if ($items)
			{
				$escort_url = 'escort';
				
				if (in_array(Cubix_Application::getId(), array(APP_EF, APP_EI)))
				{
					$escort_url = 'accompagnatrici';
				}
				
				$glob_users = array();
				
				foreach ($items as $item)
				{
					$alerts = $this->_db->query('
						SELECT ca.user_id, ca.escorts, ca.agencies, u.email 
						FROM city_alerts ca 
						INNER JOIN users u ON u.id = ca.user_id AND u.status = 1
						WHERE ca.city_id = ?
					', $item->city_id)->fetchAll();
					
					if ($alerts)
					{
						foreach ($alerts as $alert)
						{
							if (($alert->escorts && $item->escort_id) || ($alert->agencies && $item->agency_id))
							{
								$clear_alert = (array)$alert;
								unset($clear_alert['user_id']);
								unset($clear_alert['email']);
								
								$glob_users[$alert->user_id . '$' . $alert->email][] = array(
									'alert' => $clear_alert, 
									'event_id' => $item->id,
									'escort' => '<a href="' . Cubix_Application::getById()->url . '/' . $escort_url . '/' . $item->showname . '-' . $item->escort_id . '">' . $item->showname . '</a>', 
									'agency' => '<a href="' . Cubix_Application::getById()->url . '/agency/' . $item->agency_slug . '-' . $item->agency_id . '">' . $item->agency_name . '</a>', 
									'has_agency' => $item->agency_id ? TRUE : FALSE,
									'has_escort' => $item->escort_id ? TRUE : FALSE,
									'city' => $item->city_title
								);
							}
						}
					}
				}
			}
			
			$ids = array();
			
			if ($glob_users)
			{				
				foreach ($glob_users as $user => $alert)
				{
					$ar = explode('$', $user);
					$email = $ar[1];
					
					$city_alerts = array();
										
					foreach ($alert as $a)
					{
						if ($a['has_escort'])
							$city_alerts[$a['city']]['escorts'][] = $a['escort'];
						
						if ($a['has_agency'])
							$city_alerts[$a['city']]['agencies'][] = $a['agency'];
						
						if (!in_array($a['event_id'], $ids))
							$ids[] = $a['event_id'];
					}
					
					$body = '';
					
					foreach ($city_alerts as $city => $v)
					{
						$body .= 'City: ' . $city . '<br>';
						
						if (!in_array(Cubix_Application::getId(), array(APP_EM, APP_PC)) && isset($v['agencies']) && count($v['agencies']))
						{
							$body .= 'Agency(s): ' . implode(', ', $v['agencies']) . '<br>';
						}
						
						if (isset($v['escorts']) && count($v['escorts']))
						{
							$body .= 'Escort(s): ' . implode(', ', $v['escorts']) . '<br>';
						}
						
						$body .= '<br><br><br>';
					}
					
					$subject = 'City Alert Notification';
					
					Cubix_Email::send($email, $subject, $body, TRUE);
					$cli->out(' Email sent to ' . $email);
				}
			}
			
			$str_ids = implode(',', $ids);

			if (count($ids) > 0)
			{
				$this->_db->query('UPDATE city_alerts_events SET sent = 1, sent_date = now() WHERE id IN (' . $str_ids . ')');
			}
			
			$this->_db->query('UPDATE city_alerts_events SET sent = 2 WHERE sent = 0');
			
			$cli->out('done!');
		}
		catch ( Exception $e ) {
			echo $e->__toString();
			echo " [fail]\n";
		}
		exit;
	}
	
	public function suspendResumeAction()
	{
		set_time_limit(0);
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$cli = new Cubix_Cli();
		$pid_file = '/var/run/suspend-resume-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}
		
		$active_to_suspend = $this->_db->query('
			SELECT op.id, op.escort_id 
			FROM order_packages op
			WHERE op.status = 2 AND op.sus_res_date IS NOT NULL AND op.sus_res_date <= NOW()
		')->fetchAll();
		
		$m_pack = new Model_Billing_Packages();
		
		if ( count($active_to_suspend) > 0 ) {
			echo count($active_to_suspend) . " Active Packages found to suspend \n";
			foreach ($active_to_suspend as $package) {
				$m_pack->suspend($package->id, null);
				
				echo "Package #" . $package->id . " Suspended \n";
			}
		}
		else {
			echo "No Active Packages found to suspend \n";
		}
		
		echo "\n\n";
		
		$suspend_to_active = $this->_db->query('
			SELECT op.id, op.escort_id
			FROM order_packages op
			WHERE op.status = ? AND op.sus_res_date IS NOT NULL AND op.sus_res_date <= NOW()
		', array(Model_Packages::STATUS_SUSPENDED))->fetchAll();
		
		if ( count($suspend_to_active) > 0 ) {
			echo count($suspend_to_active) . " Suspended Packages found to activate \n";
			foreach ($suspend_to_active as $package) {
				$m_pack->resume($package->id, null);
				
				echo "Package #" . $package->id . " Activated \n";
			}
		}
		else {
			echo "No Suspended Packages found to Activate \n";
		}
	}


	public function reportsPerMonthAction(){
//		set_time_limit(0);
//		$this->view->layout()->disableLayout();
//		$this->_helper->viewRenderer->setNoRender();
		$cli = new Cubix_Cli();
//		$pid_file = '/var/run/send-escort-statistics-' . Cubix_Application::getId() . '.pid';
//		$cli->setPidFile($pid_file);
//		if ( $cli->isRunning() ) {
//			$cli->out('Action is already running... exitting...');
//			exit(1);
//		}

		$where2 = '1';
		$date_from = date("Y-m-01", strtotime("-1 month") );	
		$date_to = date("Y-m-t", strtotime("-1 month") );



		if ( $date_from ) {
			$where2 .= ' AND DATE(date) >= "'. $date_from.'"';
		}

		if ( $date_to ) {
			$where2 .= ' AND DATE(date) <= "'.$date_to.'"';
		}
		

		$sql = '
			SELECT e.showname,
			e.id,
			v_c as sedcard_view_count,
			m_c as message_count,
			p_c as photo_view_count,
			tmp.type,
			tmp.`status`,
			u.email,
			u.username
		FROM escorts e
		INNER JOIN statistics_reports sr ON sr.escort_id = e.id AND (sr.status & 2)
		INNER JOIN users u ON u.id = e.user_id 
		LEFT JOIN
		(
		SELECT s.escort_id,SUM(p_c) as p_c,SUM(m_c) as m_c,SUM(v_c) as v_c,SUM(type) as type,SUM(type) as `status` FROM
		(
		SELECT escort_id,NULL as p_c ,NULL as m_c,SUM(count) as v_c,NULL type, NULL `status` FROM
		escort_hits_daily ehd WHERE '.$where2.'  GROUP BY escort_id
		UNION
		SELECT escort_id,NULL as p_c ,COUNT(id) as m_c,NULL as v_c,NULL as type, NULL `status` FROM
		escort_feedbacks ef WHERE '.$where2.' GROUP BY escort_id
		/*UNION
		SELECT escort_id,SUM(count) as p_c,NULL as m_c,NULL as v_c,NULL as type, NULL `status` FROM
		escort_photo_hits_daily ephd WHERE '.$where2.' GROUP BY escort_id */
		
		) s
		 GROUP BY s.escort_id ) as tmp
		ON tmp.escort_id = e.id
		WHERE  NOT e.status & 256
		GROUP BY e.id
		';


		try{
			$tmpArray = array();

			$results = $this->_db->fetchAll($sql);
			foreach ( $results as $result ){
				$tmpArray[$result->email][] = $result;
			}

			$ratio = 1;
			if ( Zend_Registry::isRegistered('statistics') ){
				$configs = Zend_Registry::get('statistics');
				if ( isset( $configs['ratio'] ) ){
					$ratio = $configs['ratio'];
				}
			}

			$project = Cubix_Application::getById();
			
			foreach ( $tmpArray as $email => $email_item ){
				$output = '';
				if ( is_array($email_item) ){
					foreach ( $email_item as $item ){
						$output .= '<tr>
							<td>
								<a href="'.@$project->url.'/escort/'.$item->showname.'-'.$item->id.'" style="color: #a3171e">
									'.$item->showname.'
								</a>
							</td>
							<td>
								'.number_format( ceil( $item->sedcard_view_count * $ratio ), 0,'.',',').'
							</td>'.
							/*<td>
								'.number_format( ceil( $item->photo_view_count * $ratio ) , 0,'.',',').'
							</td>*/
							'<td>
								'.number_format( ceil( $item->message_count ) , 0,'.',',').'
							</td>
						</tr>';
						$username = $item->username;
						
					}
					
					$template_id = 'statistic_report';
					Cubix_Email::sendTemplate($template_id, $email, array('username' => $username,'statistics' => $output, 'time_frame' => $date_from.' - '.$date_to));
					$cli->out(' Email sent to ' . $email);
				}
			}
			exit;
			$subject = 'Reminder';
			foreach ( $results as $result ) {
				$body = '<html><body><div style="border: 1px solid #c8e7ff; background-color: #FFF; padding: 7px 20px">'.$result->comment.'</div></body></html>';
				$this->_db->update('escort_comments', array('email_sent' => 1), $this->_db->quoteInto('id = ?', $result->id));
				Cubix_Email::send($result->email,$subject, $body, true);
				$cli->out(' Email sent to ' . $result->email);
			}
		}
		catch ( Exception $e ) {
			echo $e->__toString();
			echo " [fail]\n";
		}
		exit;
	}



	public function reportsPerWeekAction(){
//		set_time_limit(0);
//		$this->view->layout()->disableLayout();
//		$this->_helper->viewRenderer->setNoRender();
		$cli = new Cubix_Cli();
//		$pid_file = '/var/run/send-escort-statistics-' . Cubix_Application::getId() . '.pid';
//		$cli->setPidFile($pid_file);
//		if ( $cli->isRunning() ) {
//			$cli->out('Action is already running... exitting...');
//			exit(1);
//		}

		$where2 = '1';
		$date_from = date("Y-m-d", strtotime("-1 week") );
		$date_to = date("Y-m-d", time() );//date("Y-m-t", strtotime("-1 week") );


		if ( $date_from ) {
			$where2 .= ' AND DATE(date) >= "'. $date_from.'"';
		}

		if ( $date_to ) {
			$where2 .= ' AND DATE(date) <= "'.$date_to.'"';
		}


		$sql = '
			SELECT e.showname,
			e.id,
			v_c as sedcard_view_count,
			m_c as message_count,
			p_c as photo_view_count,
			tmp.type,
			tmp.`status`,
			u.email,
			u.username
		FROM escorts e
		INNER JOIN statistics_reports sr ON sr.escort_id = e.id AND (sr.status & 1)
		INNER JOIN users u ON u.id = e.user_id 
		LEFT JOIN
		(
		SELECT s.escort_id,SUM(p_c) as p_c,SUM(m_c) as m_c,SUM(v_c) as v_c,SUM(type) as type,SUM(type) as `status` FROM
		(
		SELECT escort_id,NULL as p_c ,NULL as m_c,SUM(count) as v_c,NULL type, NULL `status` FROM
		escort_hits_daily ehd WHERE '.$where2.'  GROUP BY escort_id
		UNION
		SELECT escort_id,NULL as p_c ,COUNT(id) as m_c,NULL as v_c,NULL as type, NULL `status` FROM
		escort_feedbacks ef WHERE '.$where2.' GROUP BY escort_id
		/*UNION
		SELECT escort_id,SUM(count) as p_c,NULL as m_c,NULL as v_c,NULL as type, NULL `status` FROM
		escort_photo_hits_daily ephd WHERE '.$where2.' GROUP BY escort_id*/

		) s
		 GROUP BY s.escort_id ) as tmp
		ON tmp.escort_id = e.id
		WHERE  NOT e.status & 256
		GROUP BY e.id
		';


		try{
			$tmpArray = array();

			$results = $this->_db->fetchAll($sql);
			foreach ( $results as $result ){
				$tmpArray[$result->email][] = $result;
			}


			$ratio = 1;
			if ( Zend_Registry::isRegistered('statistics') ){
				$configs = Zend_Registry::get('statistics');
				if ( isset( $configs['ratio'] ) ){
					$ratio = $configs['ratio'];
				}
			}
			
			$project = Cubix_Application::getById();
			
			foreach ( $tmpArray as $email => $email_item ){
				$output = '';
				if ( is_array($email_item) ){
					foreach ( $email_item as $item ){
						$output .= '<tr>
							<td>
								<a href="'.@$project->url.'/escort/'.$item->showname.'-'.$item->id.'" style="color: #a3171e">
									'.$item->showname.'
								</a>
							</td>
							<td>
								'.number_format( ceil( $item->sedcard_view_count * $ratio ), 0,'.',',').'
							</td>'.
							/*<td>
								'.number_format( ceil( $item->photo_view_count * $ratio ) , 0,'.',',').'
							</td>*/
							'<td>
								'.number_format( ceil( $item->message_count ) , 0,'.',',').'
							</td>
						</tr>';
						$username = $item->username;
                        if ($item->id == 103824)
                        {
                            Cubix_Email::sendTemplate('statistic_report', 'vardanina@gmail.com', array('username' => $username,'statistics' => $output, 'time_frame' => $date_from.' - '.$date_to));
                        }

					}
					
					
					$template_id = 'statistic_report';
					Cubix_Email::sendTemplate($template_id, $email, array('username' => $username,'statistics' => $output, 'time_frame' => $date_from.' - '.$date_to));
					$cli->out(' Email sent to ' . $email);
				}
			}
		}
		catch ( Exception $e ) {
			echo $e->__toString();
			echo " [fail]\n";
		}
		exit;
	}


	public function removeDublicateMembersAction(){
		$sql = "SELECT id, username, email FROM users WHERE username IN (SELECT username FROM (SELECT COUNT(u.id) as u_c,email, username ,u.id FROM users u
		WHERE 1 AND user_type = 'member' AND u.status <> -5 GROUP BY username HAVING u_c > 1) x) AND users.status <> -5 ORDER BY username";
		
		$tmpArray = array();

		$results = $this->_db->fetchAll($sql);
		foreach ( $results as $result ){
			$tmpArray[ strtolower($result->username) ][] = $result;
		}

		$counter = 0;
		if ( count($tmpArray) > 0 ){
			foreach ( $tmpArray as $username => $tmp ){
				unset($tmp[0]);
				$counter2 = 1;
				if ( count( $tmp ) > 0 ){
					
					foreach ( $tmp as $user ){
						if ( $user->email != '' ){
							$counter++;
							
							$old = $user->username;
							$new = $user->username.$counter2;
//							
//							$body = str_replace('%old%', $old, $body);
//							$body = str_replace('%new%', $new, $body);

							$counter2++;
							
							$email = $user->email;
							
							#SELECT COUNT(*) as ce, users.`status`, email FROM users WHERE user_type = 'member' AND users.`status` = -1 GROUP BY email HAVING ce > 1
							$this->_db->update('users', array('username' => $new), $this->_db->quoteInto('id = ?', $user->id));
							
							
							$template_id = 'username_change';
							
//							$email = "grigor.ambrumyan@gmail.com";
							
							Cubix_Email::sendTemplate( $template_id, $email, array('new' => $new,'old' => $old ));
							
							
//							SELECT COUNT(id) as e_c, username, email FROM users WHERE username IN (SELECT username FROM (SELECT COUNT(u.id) as u_c,email, username ,u.id FROM users u
//							WHERE 1 AND user_type = 'member' AND u.status <> -5 GROUP BY username HAVING u_c > 1) x) AND users.status <> -5 GROUP BY email HAVING e_c > 1 ORDER BY username
							
							echo "Mail Sent to $email, Username updated from $old -> $new \n";
//							exit;
						}
					}
				}
//				
			}
		}
		var_Dump( $counter );

		exit;
//		Cubix_Email::send($email, $subject, $body, $htmlEmail = false, $from = null, $from_name = null);


	}
	
	public function fixPhotosHasBigAction()
	{
		set_time_limit(0);
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$cli = new Cubix_Cli();
		$pid_file = '/var/run/fix-photos-hasbig-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}
		
		/*$sql = "SELECT * FROM escorts";
		
		$escorts = $this->_db->fetchAll($sql);
		
		if ( count($escorts) ) {
			foreach( $escorts as $escort ) {
				
				$photos = $this->_db->fetchAll('SELECT width, height FROM escort_photos WHERE escort_id = ?', array($escort->id));
				
				if( count($photos) ) {
					$p_min_width = 400;			
					$p_min_height = 600;			
					$l_min_width = 500;
					$l_min_height = 375;
					
					$photos_small = false;
					foreach ( $photos as $photo ) {
						
						if ( $photo->width < $photo->height ) {
							if ( $photo->width < $p_min_width || $photo->height < $p_min_height ) {
								$photos_small = true;							
							}
						} else {
							if ( $photo->width < $l_min_width || $photo->height < $l_min_height ) {
								$photos_small = true;			
							}
						}
						
						if ( $photos_small ) {
							$this->_db->update('escorts', array('has_big_photos' => 0), $this->_db->quoteInto('id = ?', $escort->id));
							$cli->out("Escort #" . $escort->id . " photos are small !" . "\n");
							break;
						}
						
					}
				}
			}
		}*/
		
		$sql = "SELECT * FROM escorts WHERE has_big_photos = 0";
		
		$escorts = $this->_db->fetchAll($sql);
		
		if ( count($escorts) ) {
			foreach( $escorts as $escort ) {
				
				$photos = $this->_db->fetchAll('SELECT width, height FROM escort_photos WHERE escort_id = ?', array($escort->id));
				
				if( count($photos) ) {
					$p_min_width = 400;			
					$p_min_height = 600;
					$l_min_width = 500;			
					$l_min_height = 375;
					
					$photos_big_count = 0;
					foreach ( $photos as $photo ) {
						
						if ( $photo->width < $photo->height ) {
							if ( $photo->width >= $p_min_width && $photo->height >= $p_min_height ) {
								$photos_big_count++;
							}
						} else {
							if ( $photo->width >= $l_min_width && $photo->height >= $l_min_height ) {
								$photos_big_count++;
							}
						}						
					}
					
					if ( $photos_big_count == count($photos) ) {
						$this->_db->update('escorts', array('has_big_photos' => 1), $this->_db->quoteInto('id = ?', $escort->id));
						$cli->out("Escort #" . $escort->id . " photos are big !" . "\n");
					}
						
				}
			}
		}
	}
	
	public function fixPhotosAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$cli = new Cubix_Cli();
		$pid_file = '/var/run/fix-photos-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}
		
		$sql = "SELECT ep.* FROM escorts e
		inner join users u ON u.id = e.user_id
		inner join escort_photos ep on ep.escort_id = e.id
		where u.application_id = 69";
		
		$photos = $this->_db->fetchAll($sql);
		//var_dump($photos); die;
		if ( count($photos) ) {
			foreach( $photos as $photo ) {
				
				$photo_info = $this->getRealPhoto($photo->escort_id, $photo->hash, $photo->ext);
				
				if( $photo_info ) {
					
					if ( $photo_info['0'] < $photo_info['1'] ) {
						$this->_db->update('escort_photos', array('is_portrait' => 1), $this->_db->quoteInto('id = ?', $photo->id));
						$cli->out("Photo " . $photo->id . " is portrait! Width: " . $photo_info['0'] . " | Height: " . $photo_info['1'] . "\n");
					}
					
					$this->_db->update('escort_photos', array('width' => $photo_info[0], 'height' => $photo_info[1]), $this->_db->quoteInto('id = ?', $photo->id));
					$cli->out("Photo width " . $photo_info[0] . " and height: " . $photo_info[1] . "!" . "\n");
					
				}
			}
		}
	}
	
	public function getRealPhoto($escort_id, $hash, $ext)
	{
		if ( !$escort_id ) return false;
		
		$catalog = $escort_id;
		$a = array();
		if ( is_numeric($catalog) ) {
			$parts = array();

			if ( strlen($catalog) > 2 ) {
				$parts[] = substr($catalog, 0, 2);
				$parts[] = substr($catalog, 2);
			}
			else {
				$parts[] = '_';
				$parts[] = $catalog;
			}
		}
		else if ( preg_match('#^([0-9]+)/(.+)$#', $catalog, $a) ) {
			array_shift($a);
			$catalog = $a[0];

			$parts = array();

			if ( strlen($catalog) > 2 ) {
				$parts[] = substr($catalog, 0, 2);
				$parts[] = substr($catalog, 2);
			}
			else {
				$parts[] = '_';
				$parts[] = $catalog;
			}

			$parts[] = $a[1];
		}

		$catalog = implode('/', $parts);
		$staging = "pic4.and6.ch.dev.sceon.am";
		$normal = "pic.escortdirectory.com";
		//echo "http://" . $normal . "/" . Cubix_Application::getById()->host . "/" . $catalog . "/" . $hash . "." . $ext . "?origano=true"; die;
		return getimagesize("http://" . $normal . "/" . Cubix_Application::getById()->host . "/" . $catalog . "/" . $hash . "." . $ext . "?origano=true");
	}
	
	public function removeTemporaryBlockedEmailsAction()
	{
		//Remove all temporary blocked emails which blocked more than 48 hours ago
		//Temporary blocked emails has block_date
		
		set_time_limit(0);
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$cli = new Cubix_Cli();
		$pid_file = '/var/run/remove-temporary-block-emails-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}
		
		$this->_db->query('DELETE FROM blacklisted_emails WHERE block_date IS NOT NULL AND UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(block_date) > 2 * 24 * 60 * 60');
		die('done');
	}
	
	public function moveServicesRatesToAboutAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$cli = new Cubix_Cli();
		$pid_file = '/var/run/moveServicesRatesToAboutAction-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}
		
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db;
		
		$services_arr = array(
			1 => '69',
			2 => 'A+/A-Level',
			3 => 'CIM',
			4 => 'CIF',
			5 => 'Toys',
			6 => 'FK',
			7 => 'GFE',
			8 => 'Kissing',
			9 => 'BJ',
			10 => 'BBBJ',
			11 => 'BBBJTC',
			12 => '-',
			52 => 'COB',
			53 => 'Erotic Massage',
			54 => '-',
			55 => '-',

			13 => '-',
			14 => 'DT',
			15 => 'Dirtytalk',
			16 => 'DP',
			17 => 'MSOG',
			18 => '-',
			19 => '-',
			20 => 'Full Body Sensual Massage',
			21 => '-',
			22 => '-',
			23 => '-',
			24 => 'Lingerie',
			25 => 'DIY',
			26 => 'BBBJTCWS',
			27 => '-',
			28 => 'PSE',
			29 => 'Private Photos',
			30 => '-',
			31 => 'AR(A)',
			32 => 'AR(P)',
			33 => '-',
			34 => 'PS',
			35 => '-',
			56 => 'Foam Massage',
			57 => 'Russian',
			58 => '-',
			59 => '-',
			60 => 'Shower option',
			61 => 'Whirlpool',
			62 => '-',

			36 => 'BDSM',
			37 => '-',
			38 => '-',
			39 => 'Fetish',
			40 => 'Foot Fetish',
			41 => 'GS(A)',
			42 => 'GS(P)',
			43 => 'Leather/Latex/PVC',
			44 => '-',
			45 => '-',
			46 => 'Role Play and Fantasy',
			47 => 'Spanking (give)',
			48 => 'Spanking (receive)',
			49 => '-',
			50 => '-',
			51 => '-',
			63 => '-',
			64 => '-',
			65 => '-',
		);

		$escorts_sql = '
			SELECT e.id AS escort_id, e.showname
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			/*INNER JOIN escort_services es ON es.escort_id = e.id and service_id <> 0*/
			WHERE u.application_id = ? AND NOT e.status & ?
			GROUP BY e.id
			ORDER BY e.id ASC
		';
		
		$escorts = $db->fetchAll($escorts_sql, array(Cubix_Application::getId(), ESCORT_STATUS_DELETED));
		$app_langs = Cubix_Application::getLangs();
		
		$e_count = count($escorts);
		foreach ( $escorts as $i => $escort ) {
			$services = $db->fetchAll('SELECT service_id FROM escort_services WHERE escort_id = ?', array($escort->escort_id));
			
			$services_str = '';
			$services_count = count($services);
			
			$tmp_srv_array = array();
			if ( $services_count ) {
				foreach ( $services as $service ) {
					
					if ( isset($services_arr[$service->service_id]) && $services_arr[$service->service_id] != '-' ) {
						
						//$services_str .= $services_arr[$service->service_id] . ( ( $i <= $services_count ) ? ', ' : '' );
						
						$tmp_srv_array[] = $services_arr[$service->service_id];
					}
				}
			}
			
			if ( count($tmp_srv_array) ) {
				$services_str = implode(', ', $tmp_srv_array);
			}
			
			$snapshot = $db->fetchRow('SELECT data FROM escort_profiles_snapshots_v2 WHERE escort_id = ?', array($escort->escort_id));
				
			if ( $snapshot ) {
				$snapshot = unserialize($snapshot->data);


				$rates = $snapshot->rates;

				$rates = $this->_prepareRates($rates);
				//var_dump($rates);
				$rates_str = '';
				if ( isset($rates[AVAILABLE_INCALL]) ) {
					$rates_str .= ' Incall: ';
					foreach ( $rates[AVAILABLE_INCALL] as $i_rate ) {
						if ( $i_rate->time == 0 ) {
							$rates_str .= $i_rate->type . ' ' . $i_rate->price . ' roses, ';
						} else {
							$rates_str .= $i_rate->time . ' ' . $i_rate->time_unit_title . ' ' . $i_rate->price . ' roses, ';
						}
					}
				}

				if ( isset($rates[AVAILABLE_OUTCALL]) ) {
					$rates_str .= ' Outcall: ';
					foreach ( $rates[AVAILABLE_OUTCALL] as $o_rate ) {
						if ( $o_rate->time == 0 ) {
							$rates_str .= $o_rate->type . ' ' . $o_rate->price . ' roses, ';
						} else {
							$rates_str .= $o_rate->time . ' ' . $o_rate->time_unit_title . ' ' . $o_rate->price . ' roses, ';
						}
					}
				}

				$services_str .= $rates_str;
			}
			
			if ( strlen($services_str) ) {
					
				$ab = array();
				foreach ( $app_langs as $lang ) {
					$about_text = $db->fetchOne('SELECT about_' . $lang->id . ' FROM escort_profiles_v2 WHERE escort_id = ?', array($escort->escort_id));
					$about_text .= ' ' . $services_str . ' ';
					
					
					$ab['about_' . $lang->id] = $about_text;
					
					if ( $snapshot ) {					
						
						$snapshot->{'about_' . $lang->id} = $snapshot->{'about_' . $lang->id} . ' ' . $services_str . ' ';						
					}
				}
				
				if ( $snapshot ) {
					
					$db->update('escort_profiles_snapshots_v2', array('data' => serialize($snapshot)), $db->quoteInto('escort_id = ?', array($escort->escort_id)));
					$cli->out($i + 1 . '/' . $e_count . " | escort #" . $escort->escort_id . " showname:" . $escort->showname . " SNAPSHOT\n");
				}
				
				if ( count($ab) ) {
					$db->update('escort_profiles_v2', $ab, $db->quoteInto('escort_id = ?', $escort->escort_id));
					$cli->out($i + 1 . '/' . $e_count . " | escort #" . $escort->escort_id . " showname:" . $escort->showname . " PROFILE\n");
				}
				
				$revisions = $db->fetchAll('SELECT revision, data FROM profile_updates_v2 WHERE escort_id = ? ORDER BY revision DESC LIMIT 3', array($escort->escort_id));
				if ( count($revisions) ) {
					foreach ( $revisions as $revision ) {
						$rev = unserialize($revision->data);
						
						foreach ( $app_langs as $lang ) {
							$rev['about_' . $lang->id] = $rev['about_' . $lang->id] . ' ' . $services_str . ' ';
						}
						
						$db->update('profile_updates_v2', array('data' => serialize($rev)), array($db->quoteInto('escort_id = ?', array($escort->escort_id), $db->quoteInto('revision = ?', $revision->revision))));
						$cli->out($i + 1 . '/' . $e_count . " | escort #" . $escort->escort_id . " showname:" . $escort->showname . " REVISION #" . $revision->revision . "\n");
					}
				}
				
			}
			
			//$cli->out($i + 1 . '/' . $e_count . " | escort #" . $escort->escort_id . " showname:" . $escort->showname . "\n");
			
		}
		
		die('Done!');
	}
	
	protected function _prepareRates($rates)
	{
		$DEFINITIONS = Zend_Registry::get('defines');
		
		//$currencies = $DEFINITIONS['currencies'];
		$currencies = Model_Currencies::getAllAssoc();
		$time_units = $DEFINITIONS['time_unit_options'];
		
		if ( ! is_array($rates) ) {
			return array();
		}
		
		foreach ( $rates as $i => $rate ) {
			$rates[$i] = (object) $rate;
		}
		
		

		$ret = array();
		foreach ( $rates as $rate ) {
			$new_values = array(
				'currency_title'  => $currencies[$rate->currency_id],
				'time_unit_title' => $time_units[$rate->time_unit]
			);
			$rate = (object) array_merge((array) $rate, $new_values);
			@$ret[$rate->availability][] = $rate;
		}

		$ret = $this->_sortRates($ret);

		return $ret;
	}
	
	protected function _sortRates($rates)
	{
		$ret = array();
		
		foreach ($rates as $k => $rate)
		{
			$new_array = array();
			$over = false;
			$b_weekend = false;
			$b_dinner = false;
			$b_add_hour = false;

			$overnight = null;
			$add_hour = null;
			$weekend = null;
			$dinner_date = null;

			foreach ($rate as $r)
			{				
				if ($r->type == 'overnight')
				{
					$overnight = $r;
					$over = true;
				}
				elseif ($r->type == 'additional-hour')
				{
					$add_hour = $r;
					$b_add_hour = true;
				}
				elseif ($r->type == 'weekend')
				{
					$weekend = $r;
					$b_weekend = true;
				}
				elseif ($r->type == 'dinner-date')
				{
					$dinner_date = $r;
					$b_dinner = true;
				}
				elseif ($r->time_unit == TIME_DAYS)
				{
					$new_array[($r->time * 1440)] = $r;
				}
				elseif ($r->time_unit == TIME_HOURS)
				{
					$new_array[($r->time * 60)] = $r;
				}
				elseif ($r->time_unit == TIME_MINUTES)
				{
					$new_array[$r->time] = $r;
				}
			}

			ksort($new_array);

			$sorted = array();

			foreach ($new_array as $rate)
			{
				$sorted[] = $rate;
			}

			if ($over)
				$sorted[] = $overnight;
			if ($b_add_hour)
				$sorted[] = $add_hour;
			if ($b_dinner)
				$sorted[] = $dinner_date;
			if ($b_weekend)
				$sorted[] = $weekend;
			$sorted = (object) $sorted;
			$ret[@$rate->availability] = $sorted;
		}
		
		return $ret;
	}
	
	public function blToGirlforumAction()
	{
		set_time_limit(0);
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$cli = new Cubix_Cli();
		$pid_file = '/var/run/bl-to-girlforum-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}
		
		try {
			$db_from = $this->_db;
			
			$db_to = Zend_Db::factory('mysqli', array(
				'host' => 'db0',
				'username' => 'girlforum',
				'password' => '3M7Q3d675>4]s',
				'dbname' => 'girlforum_escortforum_net'
			));

			$db_to->query('SET NAMES `utf8`');
			
			$items = $db_from->query('
				SELECT 
					b.id, c.title_en AS c_title, b.date, b.client_name, b.client_phone, b.comment 
				FROM blacklisted_clients b 
				LEFT JOIN cities c ON b.city_id = c.id 
				WHERE b.is_approved = 1 AND b.sent_to_girlforum = 0 
				ORDER BY b.date
			')->fetchAll();

			$i = 0;
			$ids = array();
			
			if ($items)
			{
				foreach ($items as $item)
				{	
					$post = '';

					if ($item->client_name)
					{
						$post .= '<strong>Nome</strong><br/>';
						$post .= $item->client_name . '<br/>';
					}

					if ($item->c_title)
					{
						$post .= '<strong>Citta</strong><br/>';
						$post .= $item->c_title . '<br/>';
					}

					if ($item->client_phone)
					{
						$post .= '<strong>Numero di telefono</strong><br/>';
						$post .= $item->client_phone . '<br/>';
					}

					if ($item->comment)
					{
						$post .= '<strong>Commenta</strong><br/>';
						$post .= $item->comment;
					}

					$db_to->insert('posts', array(
						'author_id' => 2, //administrator
						'author_name' => 'administrator',
						'use_sig' => 1,
						'use_emo' => 1,
						'ip_address' => '127.0.0.1',
						'post_date' => strtotime($item->date),
						'post' => $post,
						'topic_id' => 19, // topic: client blacklist
						'new_topic' => 0
					));

					$ids[] = $item->id;

					$i ++;
				}
				
				$count = $db_to->fetchOne('SELECT COUNT(pid) FROM posts WHERE topic_id = 19');

				$db_to->update('topics', array(
					'posts' => $count,
					'last_post' => time()
				), $db_to->quoteInto('tid = ?', 19));

				$count = $db_to->fetchOne('SELECT COUNT(pid) FROM posts WHERE author_id = 2');

				$db_to->update('members', array(
					'posts' => $count
				), $db_to->quoteInto('member_id = ?', 2));

				$count = $db_to->fetchOne('SELECT COUNT(pid) FROM posts WHERE topic_id = 19');

				$db_to->update('forums', array(
					'posts' => $count
				), $db_to->quoteInto('id = ?', 9));

				$db_from->query('UPDATE blacklisted_clients SET sent_to_girlforum = 1 WHERE id IN (' . implode(',', $ids) . ')');
			}

			$cli->out('Count = ' . $i);

			$cli->out(' Done!');
		}
		catch ( Exception $e ) {
			echo $e->__toString();
			echo " [fail]\n";
		}
		exit;
	}
	
	
	public function photoRotateAction()
	{ 
		set_time_limit(0);
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$cli = new Cubix_Cli();
		$pid_file = '/var/run/photo-rotate-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}
		
		try {
			$chunk = 4000;
			$part = 0;
			$cli->out('Fetching escorts data from database (chunk size: ' . $chunk . ')...');

			$photos_model = new Model_Escort_Photos();
			do {
				$cli->reset()->out('part #' . ($part + 1), false);
				$escorts = $this->_db->fetchAll('SELECT id, showname from escorts e WHERE 
					( e.photo_rotate_type = '.Model_Escort_Photos::PHOTO_ROTATE_ALL.' OR e.photo_rotate_type = '.Model_Escort_Photos::PHOTO_ROTATE_SELECTED.') AND 
					(e.status & ' . ESCORT_STATUS_ACTIVE  . ' AND NOT e.status & ' . ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_ADMIN_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_DELETED . ' AND NOT e.status & ' . ESCORT_STATUS_NO_ENOUGH_PHOTOS . ') LIMIT ' . $part++ * $chunk . ', ' . $chunk);
				$cli->out('escorts count : ' . count($escorts) );
				foreach($escorts as $escort)
				{
					$photos = $photos_model->getRotatablePicsByEscortId($escort->id);
					
					if(!is_null($photos)){
						
						$next_main_image = 0;
						$rotate_done = 0;
						$i = 0;
						$cli->out('Rotating Photos of escort - '. $escort->showname.' - '. $escort->id );
						foreach($photos['results'] as $photo){
							$i++;
							if($i == 1){
								$first_photo_id = $photo->id;
							}
							if($next_main_image == 1){
								$photos_model->get($photo->id)->setMain();
								$rotate_done = 1;
								BREAK;
							}
							if($photo->is_main == 1){
								$next_main_image = 1;
							}
							if($photos['count'] == $i && $rotate_done == 0 ){
								$photos_model->get($first_photo_id)->setMain();
							}	
						}
					}


				}

			} while ($escorts);
			$cli->column(60)->colorize('green')->out('success');
			$cli->reset()->out('Rotating Main Images Done ')->out();
			unset($escorts);
			
			
		}
		catch ( Exception $e ) {
			echo $e->__toString();
			echo " [fail]\n";
		}
		exit;
	}
	
	public function classifiedAdsAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$cli = new Cubix_Cli();
		$pid_file = '/var/run/classified-ads-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}

		$m_cron = new Model_Cron();
		$m_cron->ProcessClassifiedAds();
	}
	
	public function onlineNewsAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$on_news_model =  new Model_Newsletter_OnlineNews();
		$cli = new Cubix_Cli();
		$pid_file = '/var/run/online-news-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}
		
		echo "*** expired online news ***\n\n";
		$exp_news = $on_news_model->getExpiredNews();
		if ( ! count($exp_news) ) {
			echo "No expired news has been found.";
		}
		else{
			foreach($exp_news as $exp){
				$on_news_model->setStatus($exp->id, Model_Newsletter_OnlineNews::STATUS_EXPIRED);
			}
		}
		echo "\n\n\n\n";
		echo "*** pending online news ***\n\n";
		$pen_news = $on_news_model->getPendingNews();
		if ( ! count($pen_news) ) {
			echo "No pending news has been found.";
		}
		else{
			foreach($pen_news as $pen){
				$on_news_model->setStatus($pen->id, Model_Newsletter_OnlineNews::STATUS_ACTIVE);
				BREAK;
			}
		}
	}
	
	public function usersSystemDisableAction()
	{
		set_time_limit(0);
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$cli = new Cubix_Cli();
		$pid_file = '/var/run/users-system-disable-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}
		
		try {
			$items_e = $this->_db->query("
				SELECT id, username, email 
				FROM users 
				WHERE DATE(DATE_ADD(last_login_date, INTERVAL 28 DAY)) = DATE(NOW()) AND user_type IN ('escort')
			")->fetchAll();
			
			if ($items_e)
			{
				$cli->out('28 days ... sending notifications');
				$k = 1;
				
				$me = new Model_SMS_EmailOutbox();
				$mt = new Model_System_EmailTemplates();
				
				$template = $mt->getByIdEn('users_system_disable', Cubix_Application::getId());
				$subject = $template->subject;
				$message = $template->body;
				
				foreach ($items_e as $i)
				{
					if (!in_array($i->email, array('no@no.com', 'non@no.com', 'non@non.com')))
					{
						Cubix_Email::sendTemplate('users_system_disable', $i->email, array('username' => $i->username));
						$cli->out($k . '. username: ' . $i->username . ', id: ' . $i->id . ', email: ' . $i->email . ' - SENT');
						
						$data = array(
							'email' => $i->email,
							'subject' => $subject,
							'text' => $message,
							'date' => new Zend_Db_Expr('NOW()'),
							'user_id' => $i->id,
							'application_id' => Cubix_Application::getId()
						);

						$me->add($data);
					}
					else
						$cli->out($k . '. username: ' . $i->username . ', id: ' . $i->id . ', email: ' . $i->email . ' - NOT SENT');
					
					$k ++;
				}
			}
			
			$items_d = $this->_db->query("
				SELECT u.id, u.username, u.email, e.id AS escort_id 
				FROM users u
				LEFT JOIN escorts e ON u.id = e.user_id
				WHERE DATE(DATE_ADD(u.last_login_date, INTERVAL 30 DAY)) = DATE(NOW()) AND u.user_type IN ('escort')
			")->fetchAll();
			
			if ($items_d)
			{
				$cli->out('30 days ... disabling users');
				$k = 1;
				
				foreach ($items_d as $i)
				{
					if ($i->escort_id)
					{
						$_status = new Cubix_EscortStatus($i->escort_id);
						$_status->setStatus(Cubix_EscortStatus::ESCORT_STATUS_OWNER_DISABLED);
						$_status->save();
					}
					
					// system disabled
					$mu = new Model_Users();
					$mu->setSystemDisabled($i->id, 1);
					
					$cli->out($k . '. username: ' . $i->username . ', id: ' . $i->id . ', email: ' . $i->email);
					
					$k ++;
				}
			}
						
			$cli->out('done!');
		}
		catch ( Exception $e ) {
			echo $e->__toString();
			echo " [fail]\n";
		}
		exit;
	}
	
	public function topFavoritesHistoryAction()
	{
		set_time_limit(0);
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$cli = new Cubix_Cli();
		$pid_file = '/var/run/top-favorites-history-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}
		
		try {
			$cli->out('Starting TRUNCATE favorites_history table');
			$this->_db->query('TRUNCATE favorites_history');
			$cli->out('Starting add data to favorites_history table');
			$this->_db->query('
				INSERT INTO favorites_history
				(SELECT escort_id, COUNT(user_id) AS count
				FROM favorites
				WHERE rank > 0
				GROUP BY escort_id
				ORDER BY count DESC)
			');
						
			$cli->out('done!');
		}
		catch ( Exception $e ) {
			echo $e->__toString();
			echo " [fail]\n";
		}
		exit;
	}
	
	public function unsetXmasTryAction()
	{
		set_time_limit(0);
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$this->_db->query('update users SET xmas_try = 0');
		die('X-MAS tries are unset');
	}
	
	public function anibisAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$cli = new Cubix_Cli();
		$pid_file = '/var/run/anibis-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}

		try {
			$ftp_conf = array(
				'host' => 'ftp.anibis.ch',
				'login' => 'and6',
				'password' => 'Imp0rt4&six!',
				'passive' => 1,
				'root_dir' => ''
			);
			
			$cli->out('Getting escorts for sending to Anibis.ch - Free Listing');
			
			$sql = '
				 SELECT 
					e.id, c.title_de AS city, ep.address_additional_info, ct.iso AS country_code, ep.email, e.showname, e.gender, ep.phone_number, ep.street, 
					ep.street_no, ep.zip, ep.about_anibis, ep.ethnicity, n.title_de AS nationality, ep.hair_color, ep.eye_color, ep.height, ep.weight, ep.dress_size, 
					ep.shoe_size, ep.bust, ep.waist, ep.hip, ep.cup_size, ep.pubic_hair, ep.is_smoking, ep.is_drinking, ep.sex_orientation, ep.sex_availability, 
					DATE(u.date_registered) AS date_registered, op.expiration_date, er.price, cu.title AS currency 
				 FROM escorts e 
				 LEFT JOIN cities c ON c.id = e.city_id 
				 INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id 
				 LEFT JOIN escort_rates er ON er.escort_id = e.id AND er.time = 1 AND er.time_unit = 2 
				 LEFT JOIN currencies cu ON cu.id = er.currency_id 
				 LEFT JOIN nationalities n ON n.id = ep.nationality_id 
				 INNER JOIN countries ct ON ct.id = e.country_id 
				 INNER JOIN users u ON u.id = e.user_id 
				 INNER JOIN order_packages op ON op.escort_id = e.id 
				 INNER JOIN order_package_products opp ON opp.order_package_id = op.id 
				 WHERE e.status = 32 AND LENGTH(ep.about_anibis) > 0 AND op.status = 2 AND opp.product_id = 19 
				 GROUP BY e.id 
				 ORDER BY e.id 
			';
			
			$regular_escorts = $this->_db->query($sql)->fetchAll();
			
			if ($regular_escorts)
			{
				foreach ($regular_escorts as $k => $escort)
				{
					$ps = $this->_db->query('SELECT hash, ext FROM escort_photos WHERE escort_id = ? ORDER BY is_main DESC', $escort->id)->fetchAll();
					
					if ($ps)
					{
						foreach ($ps as $i => $photo) {
							$p = array('application_id' => Cubix_Application::getId(), 'escort_id' => $escort->id, 'hash' => $photo->hash, 'ext' => $photo->ext);
							$po = new Model_Escort_PhotoItem($p);
							$escort->photos[] = $po->getUrl('orig');
						}
					}
				}
				
				$cli->out('Generating XML file from sending to Anibis.ch - Free Listing');
				
				$xml = $this->_generateAnibixXML($regular_escorts);
				
				$local_regular_file = '../application/tmp_files/and6_express.xml';
				
				file_put_contents($local_regular_file, $xml);
				
				$ftp = new Cubix_Images_Storage_Ftp($ftp_conf);
				
				// Connect to ftp
				$ftp->connect();

				// Save the local file on remote storage
				$ftp->store($local_regular_file, 'and6_express.xml');
				$ftp->disconnect();
			}
			else
			{
				$cli->out('No escorts for free listing');
			}
						
			$cli->out('Getting escorts for sending to Anibis.ch - Premium');
			
			$sql = '
				 SELECT 
					e.id, c.title_de AS city, ep.address_additional_info, ct.iso AS country_code, ep.email, e.showname, e.gender, ep.phone_number, ep.street, 
					ep.street_no, ep.zip, ep.about_anibis, ep.ethnicity, n.title_de AS nationality, ep.hair_color, ep.eye_color, ep.height, ep.weight, ep.dress_size, 
					ep.shoe_size, ep.bust, ep.waist, ep.hip, ep.cup_size, ep.pubic_hair, ep.is_smoking, ep.is_drinking, ep.sex_orientation, ep.sex_availability, 
					DATE(u.date_registered) AS date_registered, op.expiration_date, er.price, cu.title AS currency 
				 FROM escorts e 
				 LEFT JOIN cities c ON c.id = e.city_id 
				 INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id 
				 LEFT JOIN escort_rates er ON er.escort_id = e.id AND er.time = 1 AND er.time_unit = 2 
				 LEFT JOIN currencies cu ON cu.id = er.currency_id 
				 LEFT JOIN nationalities n ON n.id = ep.nationality_id 
				 INNER JOIN countries ct ON ct.id = e.country_id 
				 INNER JOIN users u ON u.id = e.user_id 
				 INNER JOIN order_packages op ON op.escort_id = e.id 
				 INNER JOIN order_package_products opp ON opp.order_package_id = op.id 
				 WHERE e.status = 32 AND LENGTH(ep.about_anibis) > 0 AND op.status = 2 AND opp.product_id IN (20, 21) 
				 GROUP BY e.id 
				 ORDER BY e.id 
			';
			
			$premium_escorts = $this->_db->query($sql)->fetchAll();
			
			if ($premium_escorts)
			{
				foreach ($premium_escorts as $k => $escort)
				{
					$ps = $this->_db->query('SELECT hash, ext FROM escort_photos WHERE escort_id = ? ORDER BY is_main DESC', $escort->id)->fetchAll();
					
					if ($ps)
					{
						foreach ($ps as $i => $photo) {
							$p = array('application_id' => Cubix_Application::getId(), 'escort_id' => $escort->id, 'hash' => $photo->hash, 'ext' => $photo->ext);
							$po = new Model_Escort_PhotoItem($p);
							$escort->photos[] = $po->getUrl('orig');
						}
					}
				}
				
				$cli->out('Generating XML file from sending to Anibis.ch - Premium Listing');
				
				$xml = $this->_generateAnibixXML($premium_escorts);
				
				$local_premium_file = '../application/tmp_files/and6_premium.xml';
				
				file_put_contents($local_premium_file, $xml);
				
				$ftp = new Cubix_Images_Storage_Ftp($ftp_conf);
				
				// Connect to ftp
				$ftp->connect();

				// Save the local file on remote storage
				$ftp->store($local_premium_file, 'and6_premium.xml');
				$ftp->disconnect();
			}
			else
			{
				$cli->out('No escorts for premium listing');
			}
			
			$cli->out('done!');
		}
		catch ( Exception $e ) {
			echo $e->__toString();
			echo " [fail]\n";
		}
		exit;
	}
	
	private function _generateAnibixXML($escorts)
	{
		$defines = Zend_Registry::get('defines');
		$xml = '';
		
		$xml .= '<?xml version="1.0" encoding="utf-8" standalone="no"?>' . "\n";
		$xml .= '<ArrayOfDataObject xmlns="http://schemas.datacontract.org/2004/07/Xmedia.XChange.Interface">' . "\n";
		
		foreach ($escorts as $e)
		{
			$xml .= "\t" . '<DataObject>' . "\n";
			
			$xml .= "\t\t" . '<Addresses>' . "\n";
			$xml .= "\t\t\t" . '<Address>' . "\n";
			$xml .= "\t\t\t\t" . '<AddressType>Location</AddressType>' . "\n";
			$xml .= "\t\t\t\t" . '<City><![CDATA[' . $e->city . ']]></City>' . "\n";
			
			if ($e->address_additional_info)
				$xml .= "\t\t\t\t" . '<Comment><![CDATA[' . $e->address_additional_info . ']]></Comment>' . "\n";
			
			$xml .= "\t\t\t\t" . '<CountryCode>' . strtoupper($e->country_code) . '</CountryCode>' . "\n";
			
			if ($e->email)
				$xml .= "\t\t\t\t" . '<EMail>' . $e->email . '</EMail>' . "\n";
			
			$xml .= "\t\t\t\t" . '<Firstname>' . $e->showname . '</Firstname>' . "\n";
			$e->gender == 1 ? $gender = 'Female' : ($e->gender == 2 ? $gender = 'Male' : $gender = 'Undefined');
			$xml .= "\t\t\t\t" . '<Gender>' . $gender . '</Gender>' . "\n";
			
			if ($e->phone_number)
				$xml .= "\t\t\t\t" . '<PhoneBusiness>' . $e->phone_number . '</PhoneBusiness>' . "\n";
			
			if ($e->street)
				$xml .= "\t\t\t\t" . '<Street><![CDATA[' . $e->street . ' ' . $e->street_no . ']]></Street>' . "\n";
			
			$xml .= "\t\t\t\t" . '<UniqueKey>' . Cubix_Application::getId() . $e->id . '</UniqueKey>' . "\n";
			
			if ($e->zip)
				$xml .= "\t\t\t\t" . '<Zipcode>' . $e->zip . '</Zipcode>' . "\n";
			
			$xml .= "\t\t\t" . '</Address>' . "\n";
			$xml .= "\t\t" . '</Addresses>' . "\n";
			
			$xml .= "\t\t" . '<AdvertMain>' . "\n";
			$xml .= "\t\t\t" . '<Country>CH</Country>' . "\n";
			$xml .= "\t\t\t" . '<Description><![CDATA[' . $e->about_anibis . ']]></Description>' . "\n";
			$xml .= "\t\t\t" . '<Language>de</Language>' . "\n";
			
			if ($e->currency && $e->price)
			{
				$xml .= "\t\t\t" . '<Price>' . "\n";
				$xml .= "\t\t\t\t" . '<Currency>' . $e->currency . '</Currency>' . "\n";
				$xml .= "\t\t\t\t" . '<TextAddon>1 hour</TextAddon>' . "\n";
				$xml .= "\t\t\t\t" . '<Value>' . $e->price . '</Value>' . "\n";
				$xml .= "\t\t\t" . '</Price>' . "\n";
			}
			
			$xml .= "\t\t\t" . '<Section>Erotic</Section>' . "\n";
			$xml .= "\t\t\t" . '<Title><![CDATA[' . $e->showname . ' aus ' . $e->city . ']]></Title>' . "\n";
			$xml .= "\t\t\t" . '<Type>Sell</Type>' . "\n";
			$xml .= "\t\t\t" . '<UniqueKey>' . Cubix_Application::getId() . $e->id . '</UniqueKey>' . "\n";
			$xml .= "\t\t" . '</AdvertMain>' . "\n";
			
			if ($e->photos)
			{
				$xml .= "\t\t" . '<Attachments>' . "\n";
				$k = 1;
				
				foreach ($e->photos as $ph)
				{	
					$xml .= "\t\t\t" . '<Attachment>' . "\n";
					$xml .= "\t\t\t\t" . '<DataType>Image</DataType>' . "\n";
					$xml .= "\t\t\t\t" . '<Path>' . $ph . '</Path>' . "\n";
					$xml .= "\t\t\t\t" . '<Position>' . $k . '</Position>' . "\n";
					$xml .= "\t\t\t\t" . '<SourceType>Url</SourceType>' . "\n";
					$xml .= "\t\t\t" . '</Attachment>' . "\n";
					$k ++;
				}
				
				$xml .= "\t\t" . '</Attachments>' . "\n";
			}
			
			$xml .= "\t\t" . '<Attributes>' . "\n";
			
			if ($e->ethnicity)
			{
				$xml .= "\t\t\t" . '<Attribute>' . "\n";
				$xml .= "\t\t\t\t" . '<Name>Ethnic</Name>' . "\n";
				$xml .= "\t\t\t\t" . '<Value>' . $defines['ethnic_options'][$e->ethnicity] . '</Value>' . "\n";
				$xml .= "\t\t\t" . '</Attribute>' . "\n";
			}
			
			if ($e->nationality)
			{
				$xml .= "\t\t\t" . '<Attribute>' . "\n";
				$xml .= "\t\t\t\t" . '<Name>Nationality</Name>' . "\n";
				$xml .= "\t\t\t\t" . '<Value>' . $e->nationality . '</Value>' . "\n";
				$xml .= "\t\t\t" . '</Attribute>' . "\n";
			}
			
			if ($e->hair_color)
			{
				$xml .= "\t\t\t" . '<Attribute>' . "\n";
				$xml .= "\t\t\t\t" . '<Name>Hair</Name>' . "\n";
				$xml .= "\t\t\t\t" . '<Value>' . $defines['hair_color_options'][$e->hair_color] . '</Value>' . "\n";
				$xml .= "\t\t\t" . '</Attribute>' . "\n";
			}
			
			if ($e->eye_color)
			{
				$xml .= "\t\t\t" . '<Attribute>' . "\n";
				$xml .= "\t\t\t\t" . '<Name>Eyes</Name>' . "\n";
				$xml .= "\t\t\t\t" . '<Value>' . $defines['eye_color_options'][$e->eye_color] . '</Value>' . "\n";
				$xml .= "\t\t\t" . '</Attribute>' . "\n";
			}
			
			if ($e->height)
			{
				$xml .= "\t\t\t" . '<Attribute>' . "\n";
				$xml .= "\t\t\t\t" . '<Name>Height</Name>' . "\n";
				$xml .= "\t\t\t\t" . '<Value>' . $e->height . ' cm' . '</Value>' . "\n";
				$xml .= "\t\t\t" . '</Attribute>' . "\n";
			}
			
			if ($e->weight)
			{
				$xml .= "\t\t\t" . '<Attribute>' . "\n";
				$xml .= "\t\t\t\t" . '<Name>Weight</Name>' . "\n";
				$xml .= "\t\t\t\t" . '<Value>' . $e->weight . ' kg' . '</Value>' . "\n";
				$xml .= "\t\t\t" . '</Attribute>' . "\n";
			}
			
			if ($e->dress_size)
			{
				$xml .= "\t\t\t" . '<Attribute>' . "\n";
				$xml .= "\t\t\t\t" . '<Name>Dress Size</Name>' . "\n";
				$xml .= "\t\t\t\t" . '<Value>' . $e->dress_size . '</Value>' . "\n";
				$xml .= "\t\t\t" . '</Attribute>' . "\n";
			}
			
			if ($e->shoe_size)
			{
				$xml .= "\t\t\t" . '<Attribute>' . "\n";
				$xml .= "\t\t\t\t" . '<Name>Shoe size</Name>' . "\n";
				$xml .= "\t\t\t\t" . '<Value>' . $e->shoe_size . '</Value>' . "\n";
				$xml .= "\t\t\t" . '</Attribute>' . "\n";
			}
			
			if ($e->bust && $e->waist && $e->hip)
			{
				$xml .= "\t\t\t" . '<Attribute>' . "\n";
				$xml .= "\t\t\t\t" . '<Name>Bust-Waist-Hip</Name>' . "\n";
				$xml .= "\t\t\t\t" . '<Value>' . $e->bust . '-' . $e->waist . '-' . $e->hip . '</Value>' . "\n";
				$xml .= "\t\t\t" . '</Attribute>' . "\n";
			}
			
			if ($e->cup_size)
			{
				$xml .= "\t\t\t" . '<Attribute>' . "\n";
				$xml .= "\t\t\t\t" . '<Name>Cup size</Name>' . "\n";
				$xml .= "\t\t\t\t" . '<Value>' . $e->cup_size . '</Value>' . "\n";
				$xml .= "\t\t\t" . '</Attribute>' . "\n";
			}
			
			if ($e->pubic_hair)
			{
				$xml .= "\t\t\t" . '<Attribute>' . "\n";
				$xml .= "\t\t\t\t" . '<Name>Pubic hair</Name>' . "\n";
				$xml .= "\t\t\t\t" . '<Value>' . $defines['pubic_hair_options'][$e->pubic_hair] . '</Value>' . "\n";
				$xml .= "\t\t\t" . '</Attribute>' . "\n";
			}
			
			if ($e->is_smoking)
			{
				$xml .= "\t\t\t" . '<Attribute>' . "\n";
				$xml .= "\t\t\t\t" . '<Name>Smoking</Name>' . "\n";
				$xml .= "\t\t\t\t" . '<Value>' . $defines['y_n_oc_options'][$e->is_smoking] . '</Value>' . "\n";
				$xml .= "\t\t\t" . '</Attribute>' . "\n";
			}
			
			if ($e->is_drinking)
			{
				$xml .= "\t\t\t" . '<Attribute>' . "\n";
				$xml .= "\t\t\t\t" . '<Name>Drinking</Name>' . "\n";
				$xml .= "\t\t\t\t" . '<Value>' . $defines['y_n_oc_options'][$e->is_drinking] . '</Value>' . "\n";
				$xml .= "\t\t\t" . '</Attribute>' . "\n";
			}
			
			if ($e->sex_orientation)
			{
				$xml .= "\t\t\t" . '<Attribute>' . "\n";
				$xml .= "\t\t\t\t" . '<Name>Orientation</Name>' . "\n";
				$xml .= "\t\t\t\t" . '<Value>' . $defines['sexual_orientation_options'][$e->sex_orientation] . '</Value>' . "\n";
				$xml .= "\t\t\t" . '</Attribute>' . "\n";
			}
			
			if ($e->sex_availability)
			{
				$xml .= "\t\t\t" . '<Attribute>' . "\n";
				$xml .= "\t\t\t\t" . '<Name>Service for</Name>' . "\n";
				$xml .= "\t\t\t\t" . '<Value>' . $defines['sex_availability_options'][$e->sex_availability] . '</Value>' . "\n";
				$xml .= "\t\t\t" . '</Attribute>' . "\n";
			}
			
			$xml .= "\t\t" . '</Attributes>' . "\n";
			
			$xml .= "\t\t" . '<Source>' . "\n";
			$xml .= "\t\t\t" . '<DateCreated>' . $e->date_registered . '</DateCreated>' . "\n";
			$xml .= "\t\t\t" . '<DateExpiring>' . $e->expiration_date . '</DateExpiring>' . "\n";
			$xml .= "\t\t\t" . '<DeepLink>http://www.and6.ch/sex-model/' . $e->showname . '-' . $e->id . '</DeepLink>' . "\n";
			$xml .= "\t\t\t" . '<Domain>www.and6.ch</Domain>' . "\n";
			$xml .= "\t\t\t" . '<LastFound>' . date('Y-m-d') . '</LastFound>' . "\n";
			$xml .= "\t\t\t" . '<Path1>Erotic</Path1>' . "\n";
			$xml .= "\t\t\t" . '<Path2>Escort</Path2>' . "\n";
			
			$path3 = '';
			
			if ($e->gender == 1) $path3 = 'Girl';
			elseif ($e->gender == 2) $path3 = 'Boy';
			elseif ($e->gender == 3) $path3 = 'Trans';
			
			if (strlen($path3))
				$xml .= "\t\t\t" . '<Path3>Girl</Path3>' . "\n";
			
			$xml .= "\t\t" . '</Source>' . "\n";
			
			$xml .= "\t" . '</DataObject>' . "\n";
		}
		
		$xml .= '</ArrayOfDataObject>';
		
		return $xml;
	}

	public function premiumAgenciesAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$cli = new Cubix_Cli();
		$pid_file = '/var/run/premium-agencies-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}
		
		$sql = "
			SELECT id, premium_end_date FROM agencies WHERE is_premium = 1
		";
		
		$items = $this->_db->fetchAll($sql);
		
		if ( count($items) ) {
			foreach( $items as $item ) {				
				
				if( strtotime($item->premium_end_date) < time() ) {					
										
					$this->_db->update('agencies', array('is_premium' => 0, 'premium_end_date' => null, 'premium_days' => null), $this->_db->quoteInto('id = ?', $item->id));
					$cli->out("Agency Premium Expired #" . $item->id . "!" . "\n");
					
				}
			}
		}
	}
	
	public function ageNotVerifyAction(){
		
        $this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

        $sql = '
			SELECT e.id
			FROM escorts e
            WHERE e.age_verify_disabled = 0 AND DATE_SUB(now(), INTERVAL +24 HOUR) >= e.age_verify_date AND e.need_age_verification <> 0 AND e.need_age_verification <> ?
		';
		
		$result = $this->_db->fetchAll($sql, array(Model_Verifications_AgeVerification::VERIFY));
		
        if( count($result) > 0 ){
            foreach ( $result as $res ){
				$this->_db->update('escorts', array('age_verify_disabled' => 1), $this->_db->quoteInto('id = ?', (int) $res->id));
                $status = new Cubix_EscortStatus($res->id);
                $status->setStatusBit(Cubix_EscortStatus::ESCORT_STATUS_NEED_AGE_VERIFICATION );
                $status->save();

				$event = Cubix_SyncNotifier::EVENT_ESCORT_STATUS_UPDATED;
				Cubix_SyncNotifier::notify($res->id, $event, array('old' => 'Age Verification request sent', 'new' => 'Disabled after 24hour'));
				
            }
        }
        
        echo "Effected Rows ".count($result);
        exit;
    }
	
	public function plannerAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$cli = new Cubix_Cli();
		$pid_file = '/var/run/planner-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}
		
		$planners = $this->_db->query('
			SELECT p.id, p.alert_time_unit, p.date, p.alert_time, p.alert_type, p.type, p.text, u.email, ep.contact_phone_parsed AS phone, e.showname, u.user_type 
			FROM planner p 
			INNER JOIN users u ON u.id = p.user_id 
			LEFT JOIN escorts e ON e.id = p.escort_id 
			LEFT JOIN escort_profiles_v2 ep ON ep.escort_id = p.escort_id 
			WHERE p.application_id = ? AND p.alert = ? AND p.alert_sent = 0 AND p.date > NOW()
		', array(Cubix_Application::getId(), PLANNER_ALERT_YES))->fetchAll();
		
		if ($planners)
		{
			$send_email = array();
			$send_sms = array();
			
			foreach ($planners as $p)
			{
				if ($p->alert_time_unit == PLANNER_ALERT_TIME_DAY)
				{
					$d = date('Y-m-d', strtotime($p->date));
					$dd = date('Y-m-d', strtotime("+" . $p->alert_time . " day"));
					
					if ($d == $dd)
					{
						$alert_type = explode(',', $p->alert_type);
						
						if (in_array(PLANNER_ALERT_TYPE_EMAIL, $alert_type))
						{
							$send_email[] = array(
								'id' => $p->id, 
								'email' => $p->email, 
								'type' => $p->type, 
								'text' => $p->text, 
								'date' => date('d M Y, H:i', strtotime($p->date)), 
								'user_type' => $p->user_type,
								'showname' => $p->showname
							);
						}
						
						if (in_array(PLANNER_ALERT_TYPE_SMS, $alert_type))
						{
							$send_sms[] = array(
								'id' => $p->id, 
								'phone' => $p->phone, 
								'type' => $p->type, 
								'date' => date('d M Y, H:i', strtotime($p->date)),
								'user_type' => $p->user_type,
								'showname' => $p->showname
							);
						}
					}
				}
				elseif ($p->alert_time_unit == PLANNER_ALERT_TIME_HOUR)
				{
					$d = date('Y-m-d H', strtotime($p->date));
					$dd = date('Y-m-d H', strtotime("+" . $p->alert_time . " hour"));
					
					if ($d == $dd)
					{
						$alert_type = explode(',', $p->alert_type);
						
						if (in_array(PLANNER_ALERT_TYPE_EMAIL, $alert_type))
						{
							$send_email[] = array(
								'id' => $p->id, 
								'email' => $p->email, 
								'type' => $p->type, 
								'text' => $p->text, 
								'date' => date('d M Y, H:i', strtotime($p->date)), 
								'user_type' => $p->user_type,
								'showname' => $p->showname
							);
						}
						
						if (in_array(PLANNER_ALERT_TYPE_SMS, $alert_type))
						{
							$send_sms[] = array(
								'id' => $p->id, 
								'phone' => $p->phone, 
								'type' => $p->type, 
								'date' => date('d M Y, H:i', strtotime($p->date)),
								'user_type' => $p->user_type,
								'showname' => $p->showname
							);
						}
					}
				}
				elseif ($p->alert_time_unit == PLANNER_ALERT_TIME_MINUTE)
				{
					$d = date('Y-m-d H:i', strtotime($p->date));
					$dd = date('Y-m-d H:i', strtotime("+" . $p->alert_time . " minute"));
					
					if ($d == $dd)
					{
						$alert_type = explode(',', $p->alert_type);
						
						if (in_array(PLANNER_ALERT_TYPE_EMAIL, $alert_type))
						{
							$send_email[] = array(
								'id' => $p->id, 
								'email' => $p->email, 
								'type' => $p->type, 
								'text' => $p->text, 
								'date' => date('d M Y, H:i', strtotime($p->date)), 
								'user_type' => $p->user_type,
								'showname' => $p->showname
							);
						}
						
						if (in_array(PLANNER_ALERT_TYPE_SMS, $alert_type))
						{
							$send_sms[] = array(
								'id' => $p->id, 
								'phone' => $p->phone, 
								'type' => $p->type, 
								'date' => date('d M Y, H:i', strtotime($p->date)),
								'user_type' => $p->user_type,
								'showname' => $p->showname
							);
						}
					}
				}
			}
			
			$defines = Zend_Registry::get('defines');
			
			if (count($send_email))
			{
				$ids = array();
				
				foreach ($send_email as $s)
				{
					$ids[] = $s['id'];
					
					if ($s['email'])
					{
						$body = 'Reminder: ' . $defines['planner_types'][$s['type']] . ' in ' . $s['date'];
						
						if ($s['user_type'] == 'agency')
							$body .= ' of escort ' . $s['showname'];
						
						$body .= '. Event: ' . $s['text'];
						
						Cubix_Email::send($s['email'], 'My schedule reminder', $body, TRUE);
						
						$cli->out('Email sent to: ' . $s['email']);
					}
				}
				
				$ids_str = implode(',', $ids);
				
				$this->_db->query('UPDATE planner SET alert_sent = 1 WHERE id IN (' . $ids_str . ')');
			}
			
			if (count($send_sms))
			{
				$ids = array();
				
				foreach ($send_sms as $s)
				{
					$ids[] = $s['id'];
					
					if ($s['phone'])
					{
						$body = 'Reminder: ' . $defines['planner_types'][$s['type']] . ' in ' . $s['date'];
						
						if ($s['user_type'] == 'agency')
							$body .= ' of escort ' . $s['showname'];
						
						$originator = Cubix_Application::getPhoneNumber(Cubix_Application::getId());
						$config = Zend_Registry::get('system_config');

						if (Cubix_Application::getId() != APP_6B)
						{
                            $model_sms = new Model_SMS_Outbox();
                            $data['phone_from'] = $originator;
                            $data['application_id'] = Cubix_Application::getId();
                            $b_user = Zend_Auth::getInstance()->getIdentity();
                            $data['sender_sales_person'] = $b_user->id;
                            $data['phone_number'] = $s['phone'];
                            $data['text'] = $body;
                            //
							$sms_config = $config['sms'];
							$SMS_USERKEY = $sms_config['userkey'];
							$SMS_PASSWORD = $sms_config['password'];
							$SMS_DeliveryNotificationURL = $sms_config['DeliveryNotificationURL'];
							$SMS_NonDeliveryNotificationURL = $sms_config['NonDeliveryNotificationURL'];
							$SMS_BufferedNotificationURL = $sms_config['BufferedNotificationURL'];

                            $model_sms->save($data);

							$sms = new Cubix_SMS($SMS_USERKEY, $SMS_PASSWORD);

							$sms->setOriginator($originator);
							$sms->addRecipient($s['phone']);
							$sms->setBufferedNotificationURL($SMS_BufferedNotificationURL);
							$sms->setDeliveryNotificationURL($SMS_DeliveryNotificationURL);
							$sms->setNonDeliveryNotificationURL($SMS_NonDeliveryNotificationURL);

							$sms->setContent($body);

							$sms->sendSMS();
						}
						else
						{
//							$sms_config = $config['sms_6b'];
//							$SMS_USERKEY = $sms_config['user'];
//							$SMS_PASSWORD = $sms_config['password'];

							$sms = Cubix_6B_SMS::getInstance();
							$response = $sms->send_sms($s['phone'], $body,'',date("d/M/Y"));
							if ($response->getStatus() == Cubix_6B_SMSResponse::STATUS_MESSAGE_SENT){
                                $cli->out('SMS sent to: ' . $s['phone']);
                            }else{
                                $cli->out('SMS not sent to: ' . $s['phone'].' error -> '. $response->getStatusMessage());
                            }
//							$sms->setDestination($s['phone']);
//							$sms->setContent($body);
//
//							$sms->send();
						}

						$cli->out('SMS sent to: ' . $s['phone']);
					}
				}
				
				$ids_str = implode(',', $ids);
				
				$this->_db->query('UPDATE planner SET alert_sent = 1 WHERE id IN (' . $ids_str . ')');
			}
		}
		else
		{
			$cli->out('No events found.');
		}
		
		$cli->out('Done');
	}

	
	const RESULT_ESCORT_ALREADY_TAKEN = 1;
	const RESULT_ESCORT_TAKEN_SUCCESSFULLY = 2;
	const RESULT_ESCORT_VALIDATION_ERROR = 4;
	const RESULT_ESCORT_PHOTO_SUCCESS = 5;
	const RESULT_ESCORT_PHOTO_FAIL = 6;
	const RESULT_ESCORT_DOESNT_EXIST = 7;
	const RESULT_ESCORT_OLD_PHOTO_REMOVED = 8;
	
	const RESULT_AGENCY_ALREADY_TAKEN = 91;
	const RESULT_AGENCY_TAKEN_SUCCESSFULLY = 92;
	const RESULT_AGENCY_ALREADY_EXISTS = 93;
	const RESULT_AGENCY_VALIDATION_ERROR = 94;
	const RESULT_AGENCY_LOGO_SUCCESS = 95;
	const RESULT_AGENCY_LOGO_FAIL = 96;
	const RESULT_AGENCY_DOESNT_EXIST = 97;
	
	public function syncWithEdTestAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$cli = new Cubix_Cli();
		$client = new Cubix_EscortCopier_Client(APP_ED, true);
		
		while(true) {
			$cli->out('Sending request.   ', false);
			$res = $client->call('CopyListener.ping');
			if ( $res == 'OK' ) {
				$cli->out('OK');
			} else {
				$cli->out('FAILED');
			}
			
			sleep(2);
		}
		
	}
	
	public function syncWithEdAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$cli = new Cubix_Cli();
		$pid_file = '/var/run/sync_with_ed-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}

		$client = new Cubix_EscortCopier_Client(APP_ED, true);
		$escort_model = new Model_EscortsV2();
		$agency_model = new Model_Agencies();
		$packages_model = new Model_Billing_Packages();
		$country_model = new Model_Countries();
		
		
		$agency_errors = $escort_errors = array();
		
		
		$timer_start = microtime(true);
		
		// <editor-fold defaultstate="collapsed" desc="Updating agencies data">
		$agencies = $this->_db->query('
			SELECT a.id, a.name, a.user_id
			FROM agencies a
			INNER JOIN agency_projects_sync_log apsl ON apsl.agency_id = a.id
			WHERE apsl.last_sync_date < a.last_modified AND sync_app_id = ?
		', array(APP_ED))->fetchAll();
		// </editor-fold>
		
		$cli->out(count($agencies) . ' agencies to update');

        foreach ($agencies as $agency) {
            $cli->out('Updating ' . $agency->name, false);
            $data = $agency_model->getById($agency->id);
            try {
                $result = $client->call('CopyListener.takeCopyOfAgency', array($data, true));
            } catch (Exception $e) {
                $body = $client->getClient()->getLastResponse()->getBody();
                $agency_errors[$agency->id] = "Agency sync failed with Actual response:" . $body;
                continue;
            }


            switch ($result->result) {
                case self::RESULT_AGENCY_TAKEN_SUCCESSFULLY:
                    //Updating last_sync_date of agency
                    $this->_db->query('
						UPDATE agency_projects_sync_log
						SET last_sync_date = NOW()
						WHERE sync_app_id = ? AND agency_id = ?
					', array(APP_ED, $agency->id));

                    // copy agency logo
                    list($logo_hash, $logo_ext) = $this->_db->fetchRow('
						SELECT logo_hash, logo_ext
						FROM agencies
						WHERE id = ?
					', $agency->id, Zend_Db::FETCH_NUM);
                    if ($logo_hash && $logo_ext) {
                        $image = new Cubix_Images_Entry();
                        $image->hash = $logo_hash;
                        $image->ext = $logo_ext;
                        $image->catalog_id = 'agencies';
                        $image->application_id = $data['agency_data']->application_id;

                        $images = new Cubix_Images();
                        $orig_logo_url = $images->getUrl($image);
                        try {
                            $result = $client->call('CopyListener.takeCopyOfAgencyLogo', array($agency->id, $data['agency_data']->application_id, $orig_logo_url));
                        } catch (Exception $e) {
                            $body = $client->getClient()->getLastResponse()->getBody();
                            $agency_errors[$agency->id] = "Logo copying failed with Actual response:" . $body;
                            continue;
                        }
                        switch ($result->result) {
                            case self::RESULT_AGENCY_LOGO_SUCCESS:
                                break;
                            case self::RESULT_AGENCY_LOGO_FAIL:
                                $agency_errors[$agency->id] = "Logo copying failed: " . $result->error;
                                break;
                        }
                    }

                    $cli->out(" Updated.");
                    break;
                case self::RESULT_AGENCY_VALIDATION_ERROR:
                    $err = array();
                    foreach ($result->errors as $key => $msg) {
                        $err[] = "Agency {$data['agency_data']->name} ".$key." :: ".addslashes($data['agency_data'][$key])." > ".$msg;
                    }
                    $agency_errors[$agency->id] = implode(', ', $err);
                    $cli->out(' Validation error');
                    break;
				case self::RESULT_AGENCY_ALREADY_EXISTS:
					$cli->out(' Agency already exists');
                    break;
				default:
					$cli->out(' Default '. $result->result);
            }
        }


		// <editor-fold defaultstate="collapsed" desc="Getting escorts which have been changed">
		
		//Taking escorts which have been synced already and have changes. 
		//and escorts which are never synced but has package with product ED LISTING
		//escorts which had never been synced(never had ED LISTING product) excluded

        //Get escort Ids for sync
        $eventTypes = implode(',', array(
            Cubix_SyncNotifier::EVENT_ESCORT_UPDATED,
            Cubix_SyncNotifier::EVENT_ESCORT_STATUS_UPDATED,
            Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_ADDED,
            Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED,
            Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_DELETED,
            Cubix_SyncNotifier::EVENT_ESCORT_PHONE_CHANGED,
            Cubix_SyncNotifier::EVENT_PACKAGE_ACTIVATED,
            Cubix_SyncNotifier::EVENT_PACKAGE_DEACTIVATED,
            Cubix_SyncNotifier::EVENT_ED_LISTING_PRODUCT_ADDED,
            Cubix_SyncNotifier::EVENT_ED_LISTING_PRODUCT_DELETED
        ));

        $ids = $this->_db->fetchCol("
            SELECT
	           DISTINCT escort_id
            FROM
	           journal AS j
            INNER JOIN escorts e ON j.escort_id = e.id
            WHERE
	           e.status & 32
               AND j.event_type IN ($eventTypes)
        ");

        $escorts = array();

        if (count($ids) > 0){
            try{

                $escorts = $this->_db->query('
			        SELECT 
			        	e.id, e.showname, e.agency_id, psl.last_sync_date, e.status, psl.package_status,
			        	apsl.last_sync_date AS agency_last_sync_date, a.last_modified AS agency_last_modified,
			        	op.order_id, op.package_id, op.status AS package_status, opp.order_package_id, op.expiration_date,
			        	if(opp.order_package_id AND opp2.order_package_id IS NULL AND op.order_id IS NOT NULL, 1, 0) has_premium_package, c.slug AS country_slug
			        FROM journal j
			        INNER JOIN escorts e ON j.escort_id = e.id
			        LEFT JOIN escorts_projects_sync_log psl ON psl.escort_id = e.id
			        LEFT JOIN agency_projects_sync_log apsl ON apsl.agency_id = e.agency_id AND apsl.sync_app_id = ?
			        LEFT JOIN agencies a ON a.id = apsl.agency_id
			        LEFT JOIN order_packages op ON op.escort_id = e.id AND op.status = ?
			        LEFT JOIN order_package_products opp ON op.id = opp.order_package_id AND opp.product_id = ?
			        LEFT JOIN order_package_products opp2 ON op.id = opp2.order_package_id AND opp2.product_id = ?
			        LEFT JOIN countries c ON c.id = e.country_id
			        WHERE e.id IN ('.implode(',', $ids).') AND (
			        	(psl.escort_id IS NULL AND opp.order_package_id AND opp2.order_package_id IS NULL AND op.order_id IS NOT NULL) OR 
			        	(j.date > psl.last_sync_date AND opp.order_package_id AND opp2.order_package_id IS NULL AND op.order_id IS NOT NULL) OR
			        	(j.date > psl.last_sync_date AND j.event_type IN (' . implode(',', array(Cubix_SyncNotifier::EVENT_PACKAGE_ACTIVATED, Cubix_SyncNotifier::EVENT_PACKAGE_DEACTIVATED, Cubix_SyncNotifier::EVENT_ED_LISTING_PRODUCT_ADDED, Cubix_SyncNotifier::EVENT_ED_LISTING_PRODUCT_DELETED)) . '))
			        	)	
			        GROUP BY e.id
		        ', array(APP_ED, Model_Packages::STATUS_ACTIVE, PRODUCT_ED_LISTING, PRODUCT_NO_ED_LISTING, ))->fetchAll();
            }catch (Exception $exception){
                Cubix_Email::send('narek.amirkhanyan92@gmail.com', 'ERROR sync With Ed App id ' . Cubix_Application::getId(), var_export(Cubix_Api_Error::Exception2Array($exception), true), true);
                return;
            }
        }

		// </editor-fold>
		
		
		$escorts_count = count($escorts);

		$cli->out($escorts_count . ' escorts to update');
		
		$chunk_size = 10;
		$chunks = ceil($escorts_count / $chunk_size);
		for( $i = 1; $i <= $chunks; $i++ ) {
			if ( $chunks == 1 || $i == $chunks ) {
				$cli->out('Getting profiles ' . $escorts_count . '/' . $escorts_count);
			} else {
				$cli->out('Getting profiles ' . $chunk_size * $i . '/' . $escorts_count);
			}
			
			if (  $i == $chunks ) $chunk_size = $escorts_count - $chunk_size * ($i - 1);
			
			for( $y = 0; $y < $chunk_size; $y++ ) {
				$_escort = $escorts[($i - 1) * $chunk_size + $y];
				
				// <editor-fold defaultstate="collapsed" desc="If agency escort, move agency first">
				if ( $_escort->agency_id && is_null($_escort->agency_last_sync_date ) ) {
					$orig_agency_id = $_escort->agency_id;
					$data = $agency_model->getById($_escort->agency_id);
					
					$cli->out('Moving agency.', false);
					try{
                        $result = $client->call('CopyListener.takeCopyOfAgency', array($data, true));
                    }catch (Exception $exception){
                        $body = $client->getClient()->getLastResponse()->getBody();
                        $agency_errors[$_escort->agency_id] = "Agency sync failed with Actual response:" . $body;
                        continue;
                    }


					switch ( $result->result ) {
						case self::RESULT_AGENCY_ALREADY_TAKEN:
						case self::RESULT_AGENCY_ALREADY_EXISTS:
							$cli->out('Already exists');
							//$progress[] = "Agency {$data['agency_data']->name} has been already copied";
							break;
						case self::RESULT_AGENCY_TAKEN_SUCCESSFULLY:
							//$progress[] = "Agency {$data['agency_data']->name} has been copied successfully, new id is {$result->id}";

							$this->_db->insert('agency_projects_sync_log', 
									array(
										'sync_app_id'	=> APP_ED,
										'agency_id'		=> $_escort->agency_id,
										'last_sync_date' => new Zend_Db_Expr('NOW()')
									)
							);
							
							// copy agency logo {{{
							list($logo_hash, $logo_ext) = $this->_db->fetchRow('
								SELECT logo_hash, logo_ext
								FROM agencies
								WHERE id = ?
							', $orig_agency_id, Zend_Db::FETCH_NUM);
							if ( $logo_hash && $logo_ext ) {
								$image = new Cubix_Images_Entry();
								$image->hash = $logo_hash;
								$image->ext = $logo_ext;
								$image->catalog_id = 'agencies';
								$image->application_id = $data['agency_data']->application_id;

								$images = new Cubix_Images();
								$orig_logo_url = $images->getUrl($image);
								try{
                                    $result = $client->call('CopyListener.takeCopyOfAgencyLogo', array($orig_agency_id, $data['agency_data']->application_id, $orig_logo_url));
                                }catch (Exception $e){
                                    $body = $client->getClient()->getLastResponse()->getBody();
                                    $agency_errors[$_escort->agency_id] = "Logo copying failed with Actual response:" . $body;
                                    continue;
                                }
								switch ( $result->result ) {
									case self::RESULT_AGENCY_LOGO_SUCCESS:
										$cli->out("Moved.");
										break;
									case self::RESULT_AGENCY_LOGO_FAIL:
										$progress[] = "Agency {$data['agency_data']->name} logo copying failed: " . $result->error;
										break;
								}
							}
							// }}}
							break;
						case self::RESULT_AGENCY_VALIDATION_ERROR:
							$cli->out('Validation error', false);
							$err = array();
							foreach($result->errors as $key => $msg) {
                                $err[] = "Agency {$data['agency_data']->name} ".$key." :: ".addslashes($data['agency_data'][$key])." > ".$msg;
							}
							$agency_errors[$_escort->agency_id] = implode(', ', $err);
							//$progress[] = "Agency {$data['agency_data']->name} validation failed: " . print_r($result->errors, true);
							break;
					}
				}
				// </editor-fold>
				
				
				
				$escort = $escort_model->getProfile($_escort->id);
				$add_data = $escort_model->getAdditional($_escort->id);
				$working_locations = $escort_model->getWorkingLocations($_escort->id);
				$tours = $escort_model->getTours($_escort->id);
				
				$escort = array_merge((array)$escort, (array)$add_data);
				
				$escort['working_locations'] = $working_locations;
				$escort['tours'] = $tours;
				$escort['country_slug'] = $_escort->country_slug;
				$escort['block_countries'] = $country_model->getBlockCountries($_escort->id);
				
				//Check if escort has Diamond package show in NEW section
				if ( $_escort->has_premium_package && $_escort->status == ESCORT_STATUS_ACTIVE && in_array($_escort->package_id, array(101,102)) ) {
					$escort['mark_not_new'] = 0;
					$cli->out('with diamond ---', false);
				}
				else{
					$escort['mark_not_new'] = 1;
					$cli->out('no diamond ---', false);
				}				
				$cli->out(' -ID-'.$_escort->id.'- ', false);
				$result = $client->call('CopyListener.takeCopyOfEscort', array($escort, true));
				
				$errors = $warnings = array();
				$cli->out($_escort->showname, false);
				switch ( $result->result ) {
					case self::RESULT_AGENCY_DOESNT_EXIST:
						$cli->out("Agency {$data['agency_data']->name} doesn't exist in target application. Probably agency copy failed before.");
						$errors[] = "Agency {$data['agency_data']->name} doesn't exist in target application. Probably agency copy failed before.";
						$cli->out('Failed');
						break;
					case self::RESULT_ESCORT_TAKEN_SUCCESSFULLY:
						$cli->out(' moved.', false);
						
						if ( count($result->warnings) ) {
							foreach ( $result->warnings as $warning ) {
								$warnings[] = $warning;
							}
						}
						
						
						//If escort is active and has ED LISTING product activate TRANSIT PACKAGE in ED
						//Otherwise remove package, no matter it has now active TRANSIT PACKAGE or not
						$package_status = 0;
						
						if ( $_escort->has_premium_package && $_escort->status & ESCORT_STATUS_ACTIVE ) {
							$date_diff = strtotime($_escort->expiration_date) - time();
							
							$data = array(
								'id'	=> $result->id,//new escort id
								'premium_cities'	=> $packages_model->getPremiumCities($_escort->order_package_id),
								'package_duration'	=> floor($date_diff/(60*60*24)) + 1
							);
							$p_result = $client->call('CopyListener.activateTransitPackage', array($data));
							if($p_result === true){
								$cli->out('Already has a paid package ...', false);
							}
							else{
								$cli->out('Transit Package Activated'. $p_result. ' ...', false);
							}
							
							$package_status = 1;
						} else/*if ( is_null($_escort->order_package_id) && ! is_null($_escort->last_sync_date) )*/ {
							$cli->out(' Removing Transit Package ...', false);
							$p_result = $client->call('CopyListener.removeTransitPackage', array($_escort->id, Cubix_Application::getId()));
						}
						
						$need_photo_sync = true; 
						
						if ( is_null($_escort->last_sync_date) ) {
							$this->_db->insert('escorts_projects_sync_log', 
									array(
										'sync_app_id'	=> APP_ED,
										'escort_id'		=> $_escort->id,
										'last_sync_date' => new Zend_Db_Expr('NOW()'),
										'package_status'	=> $package_status
									)
							);
							} else {
							$this->_db->query('
								UPDATE escorts_projects_sync_log
								SET last_sync_date = NOW(), package_status = ?
								WHERE sync_app_id = ? AND escort_id = ?
							', array($package_status, APP_ED, $_escort->id));
							
							$need_photo_sync = $escort_model->hadPhotoModification($_escort->id, $_escort->last_sync_date);
							
								/*$photo_model = new Model_Escort_Photos();
								foreach($photo_data as $photo){
									$add_info = json_decode($photo->data);
									$photo_row = $photo_model->get($add_info->id);
									if($photo->event_type == Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_ADDED || $photo->event_type == Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED){
										$cli->out(' Adding photo '.$add_info->id. '----' , false);
										
										$result = $client->call('CopyListener.takeCopyOfEscortPhoto', array(
											$escort->id,
											(array) $photo_row,
											$escort->application_id,
											$photo_row->getUrl(),
											$photo->type == ESCORT_PHOTO_TYPE_PRIVATE
										));
									}
									elseif($photo->event_type == Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_DELETED){
										$cli->out(' Removing photo '.$add_info->id. '----' , false);		
									}
								}*/
							
							//$need_photo_sync = false; 
						}
												
						if($need_photo_sync){
							
							// copy escort photos
                            /* @var $escort Model_EscortV2Item */
							$escort = $escort_model->get($_escort->id);
							/* @var $photos Model_Escort_PhotoItem[] */
							$photos = $escort->getPhotosForEDSync();
							//$photos = $escort->getPhotos(null, null, true, true);
							$unsuccessful_count = 0;
							
							$result = $client->call('CopyListener.removeOldEscortPhotos', array(
								$escort->id,
								$escort->application_id,
							));
							
							$cli->out(' Moving All photos...', false);
							
							foreach ( $photos as $photo ) {
								try {
									$result = $client->call('CopyListener.takeCopyOfEscortPhoto', array(
										$escort->id,
										(array) $photo,
										$escort->application_id,
										$photo->getUrl(),
										$photo->type == ESCORT_PHOTO_TYPE_PRIVATE
									));

									if ( $result->result != self::RESULT_ESCORT_PHOTO_SUCCESS ) {
										$unsuccessful_count++;
									}
								} catch(Exception $ex) {
									$cli->out($ex->getMessage());
								}
							}

							if ( $unsuccessful_count > 0 ) {
								$cli->out(count($photos) - $unsuccessful_count . '/' . count($photos));
							}
						}
						$cli->out('Done.');
						
						break;
					
					default:
						if ( count($result->errors) ) {
							$cli->out(' ID:' . $_escort->id . ' There were validation errors.');
							foreach ( $result->errors as $key => $msg ) {
								$errors[] = "Escort ID ".$key." :: ".addslashes($escort[$key])." > ".$msg;
							}
						}
						break;
				}
				
				/*if ( $errors ) {
					var_dump('Errors', $errors);die;
				}
				
				if ( $warnings ) {
					var_dump($warnings);die;
				}*/
				
				if ( count($errors) || count($warnings) ) {
					$escort_errors[$_escort->id] = array('new_escort_id' => $result->id);
					
					if ( count($errors) ) {
						$escort_errors[$_escort->id]['errors'] = implode("\n", $errors);
					}
					
					if ( count($warnings) ) {
						$escort_errors[$_escort->id]['warnings'] = implode("\n", $warnings);
					}
				}
			}
		}
		
		$cli->out('***Escorts done***');
		
		
		$timer_end = microtime(true);
		
		$cli->out('Duration ' . round(($timer_end - $timer_start) / 60) . ' minutes.');
		
		$report = '';
		foreach($agency_errors as $id => $error) {
			$report .= 'Agency ' . $id . ': ' . $error . "<br>";
		}
		
		foreach($escort_errors as $id => $error) {
			$report .= 'Escort Source ID - ' . $id . ", Target ID - " . $error['new_escort_id'] . "<br>";
			if ( $error['errors'] ) {
				$report .= 'Errors: ' . $error['errors'] . "<br>";
			}
			
			if ( $error['warnings'] ) {
				$report .= 'Warnings: ' . $error['warnings'] . "<br>";
			}
		}
		Cubix_Email::send('badalyano@gmail.com', date('Y-m-d', time()) . ' ED sync report. App id ' . Cubix_Application::getId(), $report, true);
		//Cubix_Email::send('narek.amirkhanyan92@gmail.com', date('Y-m-d', time()) . ' sync With Ed report. App id ' . Cubix_Application::getId(), $report, true);
		/*foreach ( array('bamboo88@hushmail.me', 'sharp17s@hushmail.com') as $email ) {
			Cubix_Email::send($email, date('Y-m-d', time()) . ' sync report. App id ' . Cubix_Application::getId(), $report, true);
		}*/
	}

	/*public function fixVerifyAction(){
		
        $this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

        $sql = '
			SELECT e.id
			FROM escorts e
            WHERE e.age_verify_disabled = 1 AND e.need_age_verification = ?
		';
		
		$result = $this->_db->fetchAll($sql, array(Model_Verifications_AgeVerification::VERIFY));
		
        if( count($result) > 0 ){
            foreach ( $result as $res ){
				$this->_db->update('escorts', array('age_verify_disabled' => 0), $this->_db->quoteInto('id = ?', (int) $res->id));
                $status = new Cubix_EscortStatus($res->id);
                $status->removeStatusBit(Cubix_EscortStatus::ESCORT_STATUS_NEED_AGE_VERIFICATION );
                $status->save();

				$event = Cubix_SyncNotifier::EVENT_ESCORT_STATUS_UPDATED;
				Cubix_SyncNotifier::notify($res->id, $event, array('old' => 'Age Verification request sent', 'new' => 'Disabled after 24hour'));
				
            }
        }
        
        echo "Effected Rows ".count($result);
        exit;
    }*/

    public function getNotWorkingSiteEscortsAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		
		try{
			$this->view->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender();
			$cli = new Cubix_Cli();
			$pid_file = '/var/run/get-escorts-not-working-sites-' . Cubix_Application::getId() . '.pid';
			$cli->setPidFile($pid_file);
			
			if ( $cli->isRunning() ) {
				$cli->out('Action is already running... exitting...');
				exit(1);
			}
			$cli->out('------------sync started ------ ');
			$db = $this->_db;

			$sql_escorts = "
				SELECT e.id, e.showname, u.id AS user_id, ep.website
				FROM escorts e
				INNER JOIN users u ON u.id = e.user_id
				INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
				WHERE 1 /*u.user_type = 'escort'*/ AND ep.website IS NOT NULL AND ep.website <> '' AND u.status = 1 AND e.status & 32
			";

			$escorts = $db->fetchAll($sql_escorts);

			foreach ($escorts as $i => $escort) {

				ini_set('user_agent', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT
				5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $escort->website);
				curl_setopt($ch, CURLOPT_TIMEOUT, 20);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

				if ( curl_exec($ch) === false ) {
					$cli->out($i . " / " . count($escorts) . ".) Checking escort " . $escort->showname . "! Website : " . $escort->website . " : NOT WORKING \n");
					$db->query('UPDATE escorts SET site_not_working = 1 WHERE id = ?', $escort->id);
				} else {
					$cli->out($i . " / " . count($escorts) . ".) Checking escort " . $escort->showname . "! Website : " . $escort->website . " : WORKING \n");
					$db->query('UPDATE escorts SET site_not_working = 0 WHERE id = ?', $escort->id);
				}

			}
		// for agencies's websites

			$sql_agencies = "
				SELECT a.id, a.name, a.web
				FROM agencies a
				WHERE a.web IS NOT NULL AND a.web <> '' AND a.status = 1
			";

			$agencies = $db->fetchAll($sql_agencies);

			foreach ($agencies as $i => $agency) {

				ini_set('user_agent', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT
				5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $agency->web);
				curl_setopt($ch, CURLOPT_TIMEOUT, 20);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

				if ( curl_exec($ch) === false ) {
					$cli->out($i . " / " . count($agencies) . ".) Checking agency " . $agency->name . "! Website : " . $agency->web . " : NOT WORKING \n");
					$db->query('UPDATE agencies SET site_not_working = 1 WHERE id = ?', $agency->id);
				} else {
					$cli->out($i . " / " . count($escorts) . ".) Checking agency " . $agency->name . "! Website : " . $agency->website . " : WORKING \n");
					$db->query('UPDATE agencies SET site_not_working = 0 WHERE id = ?', $agency->id);
				}

			}
		} catch(Exception $e) {
			$cli->out("error : " . $e->__toString() . " \n");
			exit;
		}
		exit;
	}

	public function journalBackupAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');

		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$cli = new Cubix_Cli();
		$pid_file = '/var/run/journal-backup-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}

		$db = $this->_db;

		$backup_table_name = 'journal_' . date('Y_m_d_H_i', time());

		$cli->out("Creting backup journal table : " . $backup_table_name . " \n");

		try {
			$db->query('CREATE TABLE ' . $backup_table_name . ' LIKE journal');
		} catch(Exception $e) {
			$cli->out("Journal table with name : " . $backup_table_name . " already exists \n");
			exit;
		}

		$cli->out("Inserting data from journal table to : " . $backup_table_name . "  \n");
		$db->query('INSERT INTO ' . $backup_table_name . ' SELECT * FROM journal');

		$cli->out("Truncating main journal table \n");
		$db->query('TRUNCATE journal');

		$cli->out("Journal backup complete! \n");

  		exit;
	}

	public function setMembersRatingAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');

		$DEFINITIONS = Zend_Registry::get('defines');

		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$cli = new Cubix_Cli();
		$pid_file = '/var/run/member-rating-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}

		$db = $this->_db;		
		//$cli->out("Creting backup journal table : " . $backup_table_name . " \n");

		$members = $db->fetchAll('
			SELECT
				m.id, m.user_id, TIMESTAMPDIFF(MONTH, u.date_registered, NOW()) AS months_reged,
				(SELECT COUNT(id) FROM reviews WHERE status = 2 AND user_id = m.user_id) AS reviews_count,
				(SELECT COUNT(id) FROM comments WHERE status = 1 AND user_id = m.user_id) AS comments_count,
				(SELECT COUNT(user_id) FROM users_last_login WHERE user_id = m.user_id) AS logins_count
			FROM members m
			INNER JOIN users u ON u.id = m.user_id
			WHERE u.user_type = "member" AND m.rating_changed_by_admin = 0 AND u.status = 1
		');

		$count = count($members);

		if ( $count ) {
			foreach ($members as $i => $member) {
				//$logins_number = $db->fetchOne('SELECT COUNT(user_id) FROM users_last_login WHERE user_id = ?', array($member->user_id));

				$member_rating = false;

				if ( $member->months_reged > 6 && $member->logins_count > 50 && $member->reviews_count > 1 && $member->comments_count > 1 ) {
					$member_rating = MEMBER_RATING_TRUSTED;
				}

				if ( $member->months_reged > 12 && $member->logins_count > 100 && $member->reviews_count > 5 && $member->comments_count > 5 ) {
					$member_rating = MEMBER_RATING_JUNIOR;
				}

				if ( $member->months_reged > 24 && $member->logins_count > 200 && $member->reviews_count > 10 && $member->comments_count > 10 ) {
					$member_rating = MEMBER_RATING_SENIOR;
				}

				if ( $member->months_reged > 36 && $member->logins_count > 400 && $member->reviews_count > 20 && $member->comments_count > 20 ) {
					$member_rating = MEMBER_RATING_DIAMOND;
				}

				if ( $member->months_reged > 48 && $member->logins_count > 800 && $member->reviews_count > 60 && $member->comments_count > 60 ) {
					$member_rating = MEMBER_RATING_VIP;
				}

				if ( $member->months_reged > 60 && $member->logins_count > 1500 && $member->reviews_count > 120 && $member->comments_count > 120 ) {
					$member_rating = MEMBER_RATING_VIP_PLUS;
				}

				if ( $member_rating ) {
					$cli->out("(" . $i . "/" . $count . ") -- Updating member rating MEMBER_ID: " . $member->id . ", RATING: " . $DEFINITIONS['member_ratings'][$member_rating] . " \n");

					$db->update('members', array('rating' => $member_rating), $db->quoteInto('id = ?', $member->id));
				}
			}
		}

		$cli->out("Complete! \n");

  		exit;
	}
	
	public function verificationTimeLimitAction()
	{
		
		set_time_limit(0);
		ini_set('memory_limit', '1024M');

		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$cli = new Cubix_Cli();
		$pid_file = '/var/run/verification_time_limit' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}
				
        $sql = '
			SELECT MAX(vr.id) as id  , vr.escort_id FROM verify_requests vr
			INNER JOIN escorts e ON e.id = vr.escort_id 
			WHERE  e.verified_status = ? AND e.stays_verified = 0  AND vr.status = ?
			GROUP BY vr.escort_id	
			HAVING DATE_SUB(now(), INTERVAL +6 MONTH) > MAX(vr.date)
		';
		
		$result = $this->_db->fetchAll($sql, array(Model_Escorts::VERIFIED,Model_Verifications_Requests::VERIFY));
		$model = new Model_Escort_Photos();
        if( count($result) > 0 ){
            foreach ( $result as $res ){
				$model->clearVerification($res->escort_id);
				$photos = $model->getPhotosIds($res->escort_id);
				foreach ( $photos as $photo ) {
					Cubix_SyncNotifier::notify($res->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED, array('id' => $photo->id, 'action' => 'verification cleared 6 months Time Limit'));
				}
				$this->_db->update('escorts', array('verified_status' => Model_EscortsV2::NOT_VERIFIED, 'need_idcard_video' => 0), $this->_db->quoteInto('id = ?', $res->escort_id));
				$this->_db->update('verify_requests', array('is_timout' => 1, 'is_read' => 0), $this->_db->quoteInto('id = ?', $res->id) );
				$cli->out($res->escort_id. " ------ done");
			}
		}
        
		$cli->out("Effected Rows ".count($result). "\n");
        exit;
    }

    public function sendExactDateSmsAction()
	{		
		set_time_limit(0);
		ini_set('memory_limit', '1024M');

		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$cli = new Cubix_Cli();
		$pid_file = '/var/run/send_exact_date_sms' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}

		$conf = Zend_Registry::get('system_config');
		$current_app_id = intval($this->_getParam('application_id'));
		$originator = Cubix_Application::getPhoneNumber($current_app_id);

		$conf = $conf['sms'];
		$sms = new Cubix_SMS($conf['userkey'], $conf['password']);
		$sms->setOriginator($originator);
				
        $sql = '
			SELECT 
				so.*
			FROM sms_outbox so
			WHERE NOW() >= so.exact_send_date AND so.status = 166
		';
		
		$result = $this->_db->fetchAll($sql);
		
        if( count($result) > 0 ){
            foreach ( $result as $r_sms ) {
                $model_sms = new Model_SMS_Outbox();
        		$sms->setRecipient($r_sms->phone_to, $r_sms->id);
				$sms->setBufferedNotificationURL($conf['BufferedNotificationURL']);
				$sms->setDeliveryNotificationURL($conf['DeliveryNotificationURL']);
				$sms->setNonDeliveryNotificationURL($conf['NonDeliveryNotificationURL']);

				$sms->setContent($r_sms->text);
                //insert Sms in outbox
                $data['phone_from'] = $r_sms->phone_from;
                $data['e_ids'] = $r_sms->escort_ids;
                $data['escort_id'] = $r_sms->escort_id;
                $data['application_id'] = Cubix_Application::getId();
                $data['sender_sales_person'] = $r_sms->sender_sales_person;
                $data['phone_number'] = $r_sms->phone_to;
                $data['text'] = $r_sms->text;
                $data['is_spam'] = $r_sms->is_spam;

                $model_sms->save($data);

				if (1 != $result = $sms->sendSMS()) {

				}

				$this->_db->update('sms_outbox', array('status' => 0, 'exact_send_date' => null), $this->_db->quoteInto('id = ?', $r_sms->id));

				$cli->out($r_sms->id. " ------ done");
			}
		}
        
        exit;
    }

    public function syncClassifiedAdsAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
				
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$cli = new Cubix_Cli();
		$pid_file = '/var/run/sync_classified_ads_' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}

		$apps = array(
			'n2n6fp5t571b7c8qkdn17tliff5707ie' => 'escortforumit.xxx',
			'1fgzkv28mly8e034c496iq0ptttetp90' => 'beneluxxx.com',
			's4ovt76cgw09vzin88y2ohyxavgzofvm' => 'escortguide.co.uk',
			'2eiqhw3tax3dzzuw5c72xzrvb1woer66' => '6anuncio.com',
			'k1xnwugea07ycccpa29k5j8q770dc22m' => 'asianescorts.com',
		);

		
		foreach ($apps as $api_key => $host) {
			
			$http_prot = 'https';
			if($host == '6anuncio.com'){
				$http_prot = 'http';
			}
			$serverUrl = "{$http_prot}://server.{$host}";

			Cubix_Api_XmlRpc_Client::setServer($serverUrl);
			Cubix_Api_XmlRpc_Client::setApiKey($api_key);

			$client = Cubix_Api::getInstance();
			try{
                $ads = $client->call('classified.getActiveAds', array());
            }catch (Exception $e){
			    var_dump($e->getMessage(), $serverUrl);
			    continue;
            }

			
			$ad_ids = array();
			if ( count($ads) ) {
				$cli->out($host . ' classified ads!' . "\n");

				foreach ( $ads as $ad ) {
					$cities = $ad['cities'];
					$images = $ad['images'];

					$ad_ids[] = $ad['id'];
					unset($ad['id']);
					unset($ad['is_sync_done']);
					unset($ad['cities']);
					unset($ad['images']);
					$ad['is_sync'] = 1;
					$ad['application_id'] = APP_ED;

                    // <editor-fold defaultstate="collapsed" desc="EDIR-845: classified ads duration: only ED">
                    $startDate = date_create($ad['approvation_date']);
                    $expirationDate = date_create($ad['expiration_date']);

                    $interval = date_diff($startDate, $expirationDate);
                    $dayDiff = $interval->format('%a');

                    if ($dayDiff > 30){
                        $ad['expiration_date'] = date('Y-m-d H:i:s',strtotime('+30 days',strtotime($ad['approvation_date'])));
                    }
                    // </editor-fold>
					$this->_db->insert('classified_ads', $ad);
					$new_ad_id = $this->_db->lastInsertId();

					$cli->out('Inserting ' . $host . ' classified ad: ' . $new_ad_id . "\n");

					if ( count($cities) ) {
						foreach ( $cities as $city ) {
							$new_city = $this->_db->fetchRow('SELECT id FROM cities WHERE title_en = ?', array($city['title']));
							if ( $new_city ) {
								$this->_db->insert('classified_ads_cities', array('ad_id' => $new_ad_id, 'city_id' => $new_city->id, 'is_sync' => 1));
							}
						}
					}
					
					if ( count($images) ) {
						// set back to ED
						Cubix_Api_XmlRpc_Client::setServer('http://server.escortdirectory.com');
						Cubix_Api_XmlRpc_Client::setApiKey('9p30yrtt4j1xbyzwscg5rkecz78vsitr');
						

						foreach ($images as $p => $photo) {
							$photo['application_id'] = APP_ED;
							
							$photo = new Model_Escort_PhotoItem($photo);
							$photo = $photo->toJSON('backend_thumb_cropper', $photo);
							$photo_url = preg_replace('#escortdirectory.com#', $host, $photo['orig_url']);
							if($host == '6anuncio.com'){
								$photo_url = str_replace('https', 'http', $photo_url);
							}else {
                                $photo_url = str_replace('http', 'https', $photo_url);
                            }
							$cli->out('photo_url'.$photo_url);
							$http = new Zend_Http_Client($photo_url, array(
							'maxredirects' => 0,
							'timeout'      => 100));
							$http->setParameterGet('origano', 'true');
							try {
								$http->request('GET');
								$binary = $http->getLastResponse()->getBody();
								$code = $http->getLastResponse()->getStatus();
							}
							catch ( Exception $e ) {
								var_dump(array('result' => self::RESULT_ESCORT_PHOTO_FAIL, 'message' => 'Could not get escort original photo', 'error' => $e->__toString()));
								continue;
							}
							
							$path = sys_get_temp_dir();
							$tmp_file = tempnam($path, 'ECEscortPhoto');
							file_put_contents($tmp_file, $binary);

							$_FILES['Filedata']['tmp_name'] = $tmp_file;
							$_FILES['Filedata']['name'] = $http->getUri()->getPath();

							$request = new Zend_Controller_Request_Http();
							//$request->setParam('escort_id', $new_escort_id);						
							$cli->out('dispatching image');
							$result = $this->doDispatch($request, 'default', 'classified-ads', 'upload-photo');
							@unlink($tmp_file);
							if (!$result->image_id){
							    die;
                            }
							$this->_db->insert('classified_ads_images', array('ad_id' => $new_ad_id, 'image_id' => $result->image_id, 'is_sync' => 1));
						}
					}
				}
				// again set to source API
				Cubix_Api_XmlRpc_Client::setServer($serverUrl);
				Cubix_Api_XmlRpc_Client::setApiKey($api_key);
				$cl_ids = $client->call('classified.setIsSyncDone', array($ad_ids));
				$cli->out('Sync mark done in ' . $host . ' classified ad ids :  '.var_export($ad_ids, true) . '------' . var_export($cl_ids, true) . "\n");
			}
		}

		die('done');
	}

	public function doDispatch($request, $module, $controller, $action)
	{
		//$request->setParam('request_mode', 'copy-listener');

		$request->setModuleName($module);
		$request->setControllerName($controller);
		$request->setActionName($action);

		$front = Zend_Controller_Front::getInstance();
		$dispatcher = $front->getDispatcher();

		$response = new Zend_Controller_Response_Http();
		$dispatcher->dispatch($request, $response);
		$plain = $response->getBody();
		$result = json_decode($plain);

		return $result;
	}
	
	public function updateChatFakeUsersAction()
	{
		$cache_id = Cubix_Application::getId() . '_chat_fake_users';
		$f_cache = new Memcache();
		$f_cache->connect("172.16.1.51", 11211);
		$app = Cubix_Application::getById();
		
		$escorts = $this->_db->fetchAll('
			SELECT e.showname, e.id, e.user_id, ep.hash AS photo_hash, ep.ext AS photo_ext, cfus.date_to
			FROM chat_fake_users_schedule cfus
			INNER JOIN escorts e ON e.user_id = cfus.user_id
			INNER JOIN escort_photos ep ON ep.escort_id = e.id AND ep.is_main = 1
			WHERE NOW() > cfus.date_from AND NOW() < cfus.date_to
		');
		
		$data = array();
		
		foreach($escorts as $escort) {
			$parts = array();
			if ( strlen($escort->id) > 2 ) {
				$parts[] = substr($escort->id, 0, 2);
				$parts[] = substr($escort->id, 2);
			}
			else {
				$parts[] = '_';
				$parts[] = $escort->id;
			}
			$catalog = implode('/', $parts);
			
			$data[] = array(
				'nickName'	=> $escort->showname . ' (Escort)',
				'userId'	=> $escort->user_id,
				'userType'	=> 'escort',
				'timeToo'	=> strtotime($escort->date_to)*1000, // chat fake users session time in miliseconds
				'link'		=> '/escort/' . $escort->showname . '-' . $escort->id,
				'avatar'	=> 'http://pic.' . $app->host . '/' . $app->host . '/' . $catalog . '/' . $escort->photo_hash . '_lvthumb.' . $escort->photo_ext
			);
		}
		$deleted = $f_cache->delete($cache_id);
		echo "delete $cache_id : result {$deleted}\r\n";
		$seted = $f_cache->set($cache_id, serialize(json_encode($data)), 0, 600);
        echo "set new  $cache_id : result {$seted}\r\n";
		die;
	}

	public function removeExpiredBlockIpAction(){
		
		$sql = 'SELECT ip,user_id FROM block_ip WHERE exp_time < "'.time().'"';

		$results = $this->_db->fetchAll($sql);
		foreach ( $results as $result ){
			$this->_db->update('users',   array('fail_login_count' => 0, 'fail_login_time'=> null), $this->_db->quoteInto('id = ?', $result->user_id));	
		}

		$this->_db->query('DELETE FROM block_ip WHERE exp_time < "'.time().'"');

		die;
	}
	
	public function march8Action()
	{
		$model = new Model_Lotteries_MarchLottery();
		$model->clearCounts();
		die('counts cleared');		
	}
	
	public function easterAction()
	{
		$model = new Model_Lotteries_EasterLottery();
		$model->clearCounts();
		die('counts cleared');		
	}
	
	public function xmasAction()
	{
		$model = new Model_Lotteries_XmasLottery();
		$model->clearCounts();
		die('counts cleared');		
	}

    public function accountConfirmEmailAction()
    {

        $sql = "
	        SELECT  u.id as id,u.email as email,'escort' as user_type,u.username as username,e.id as escort_id
	                    FROM escorts e
	                    INNER JOIN users u on u.id = e.user_id
	                    WHERE (DATE(u.date_registered) = CURDATE() - INTERVAL 1 DAY) and (not e.status & 128 and not e.status & 32)  and u.status = 1
	                   
	                    
	        UNION

	              SELECT  u.id as id,u.email as email,'agency' as user_type,u.username as username,a.id as agency_id
	                    FROM agencies a
	                    INNER JOIN users u on u.id = a.user_id
	                    WHERE (DATE(u.date_registered) = CURDATE() - INTERVAL 1 DAY) and u.status = 1
	                         
	                      
	        UNION

	              SELECT  u.id as id,u.email as email,'member' as user_type,u.username as username
	                    FROM members m
	                    INNER JOIN users u on u.id = m.user_id
	                    WHERE (DATE(u.date_registered) = CURDATE() - INTERVAL 1 DAY ) and u.status = 1
	                     
	        ";
        $cli = new Cubix_Cli();
        try {

            $result = $this->_db->query($sql)->fetchAll();
            $ids = array();
            foreach ($result as $user) {
                if (in_array($user->id, $ids)) {
                    continue;
                } else {
                    array_push($ids, $user->id);
                }
                switch ($user->user_type) {
                    case 'escort':
                        Cubix_Email::sendTemplate('account_confirmed_escort_v1', $user->email, array(
                            'username' => $user->username,
                            'user_id' => $user->escort_id//EDIR-710
                        ));
                        break;
                    case 'agency':
                        Cubix_Email::sendTemplate('account_confirmed_agency_v1', $user->email, array(
                            'agency' => $user->username,
                            'user_id' => $user->agency_id//EDIR-710
                        ));
                        break;

                    case 'member':
                        Cubix_Email::sendTemplate('account_confirmed_member_v1', $user->email, array(
                            'username' => $user->username
                        ));
                        break;
                }
                $cli->out($user->id. ' - email sended' . " \n ");
            }
        } catch (Exception $e) {
            var_dump($e->getMessage());
        };
        die;
    }


    public function emailTriggerCronAction() {    	
		$sql = "
			SELECT id, user_id, trigger_type, escort_id, params FROM email_triggers
			WHERE send_count > 0 AND UNIX_TIMESTAMP(DATE_SUB(now(), INTERVAL send_interval DAY)) > UNIX_TIMESTAMP(last_sent)
		";

		$emailTriggers = $this->_db->fetchAll($sql);


		foreach ($emailTriggers as $key => $value) {

			if($this->emailTriggerSendAction($value->user_id,$value->trigger_type,$value->escort_id,$value->params)){
				$this->_db->query('UPDATE email_triggers SET send_count = (send_count - 1), last_sent = now() WHERE id = '.$value->id);
			}
		}

		$this->_db->delete('email_triggers',"send_count = 0");

		die;
    }
  	 /**
     * @param string $trigger_id
     * @param int $user_id
     */

    public function emailTriggerSendAction($user_id, $trigger_type, $escort_id, $params) {

    	if($escort_id){
    		$data = $this->_db->fetchRow('
              SELECT
              	e.id,
                u.username,
                e.showname,
                u.sms_lng,
                e.agency_id,
                u.email
              FROM
                users u
              INNER JOIN escorts e ON e.user_id = u.id
              WHERE
                e.id = ?', $escort_id
            );
	    	$lang = 'it';
	    	if($data->sms_lng == 'en'){
	    		$lang = 'en';
	    	}
    	}

        switch ($trigger_type) {
        	case self::TRIGGER_SIGNUP :

        		$data = $this->_db->fetchRow('
	              SELECT
	                u.username,
	                u.sms_lng,
	                u.email,
	                u.user_type
	              FROM
	                users u
	              WHERE
	                u.id = ?', $user_id
	            );

        		$lang = 'it';
		    	if($data->sms_lng == 'en'){
		    		$lang = 'en';
		    	}

        		if(isset($params)){
        			$params = unserialize($params);
        		}

                $email_data = array(
                    'user' => $data->username,
                    'activation_hash' => $params['ACTIVATION_HASH'],
                    'email' => $data->email,
                    'reg_type' => $params['reg_type'],
                    'site' => 'https://www.escortforumit.xxx',
                );


                $template_id = $data->user_type == 'escort' ? 'trigger_signup_escort' : 'trigger_signup_agency';

                Cubix_Email::sendTemplate($template_id, $data->email, $email_data, $from_addr = null, $from_name = null, $lang);
            	break;
	        case self::TRIGGER_APPROVED_PROFILE_PENNDING_AC :

	            $email_data = array(
	                'user' => $data->username,
	                'showname' => $data->showname,
	                'site' => 'https://www.escortforumit.xxx/private/signin'
	            );

	            $template_id = is_null($data->agency_id) ? 'trigger_profile_pending_ac_escort' : 'trigger_profile_pending_ac_agency';

	            Cubix_Email::sendTemplate($template_id, $data->email, $email_data, $from_addr = null, $from_name = null, $lang);

           		break;
           	case self::TRIGGER_APPROVED_PROFILE_MISSING_AC :

	            $email_data = array(
	                'user' => $data->username,
	                'showname' => $data->showname,
	                'age_certif_link' => 'https://www.escortforumit.xxx/private-v2/escort-age-verification'
	            );

	            $template_id = is_null($data->agency_id) ? 'trigger_profile_missing_ac_escort' : 'trigger_profile_missing_ac_agency';

	            Cubix_Email::sendTemplate($template_id, $data->email, $email_data, $from_addr = null, $from_name = null, $lang);

           		break;
           	case self::TRIGGER_APPROVED_PROFILE_APPROVED_AC :

	            $email_data = array(
	                'user' => $data->username,
	                'showname' => $data->showname,
	                'package_link' => 'https://www.escortforumit.xxx/online-billing-v2',
	            );

	            $template_id = is_null($data->agency_id) ? 'trigger_profile_approved_ac_escort' : 'trigger_profile_approved_ac_agency';

	            Cubix_Email::sendTemplate($template_id, $data->email, $email_data, $from_addr = null, $from_name = null, $lang);

       			break;
       		case self::TRIGGER_APPROVED_PROFILE_REJECTED_AC :

	            $email_data = array(
	                'user' => $data->username,
	                'showname' => $data->showname,
	                'url' => 'https://www.escortforumit.xxx/private/signin',
	            );

	            $template_id = is_null($data->agency_id) ? 'trigger_profile_rejected_ac_escort' : 'trigger_profile_rejected_ac_agency';

	            Cubix_Email::sendTemplate($template_id, $data->email, $email_data, $from_addr = null, $from_name = null, $lang);

       			break;
       		case self::TRIGGER_WIZARD :

	           $data = $this->_db->fetchRow('
	              SELECT
	                u.username,
	                u.sms_lng,
	                u.email,
	                u.user_type
	              FROM
	                users u
	              WHERE
	                u.id = ?', $user_id
	            );

        		$lang = 'it';
		    	if($data->sms_lng == 'en'){
		    		$lang = 'en';
		    	}

        		if(isset($params)){
        			$params = unserialize($params);
        		}

                $email_data = array(
                    'user' => $data->username,
                    'site' => 'https://www.escortforumit.xxx',
                );


                $template_id = 'trigger_wizard_not_completed';

                Cubix_Email::sendTemplate($template_id, $data->email, $email_data, $from_addr = null, $from_name = null, $lang);

       			break;
       		case self::TRIGGER_APPROVED_AC_APPROVED_PROFILE :

	            $email_data = array(
	                'user' => $data->username,
	                'showname' => $data->showname,
	                'url' => 'https://www.escortforumit.xxx/online-billing-v2',
	            );

	            $template_id = is_null($data->agency_id) ? 'trigger_ac_approved_profile_escort' : 'trigger_ac_approved_profile_agency';

	            Cubix_Email::sendTemplate($template_id, $data->email, $email_data, $from_addr = null, $from_name = null, $lang);

       			break;
       		case self::TRIGGER_APPROVED_AC_PENNDING_PROFILE :

	            $email_data = array(
	                'user' => $data->username,
	                'showname' => $data->showname,
	            );

	            $template_id = is_null($data->agency_id) ? 'trigger_ac_pending_profile_escort' : 'trigger_ac_pending_profile_agency';

	            Cubix_Email::sendTemplate($template_id, $data->email, $email_data, $from_addr = null, $from_name = null, $lang);

       			break;
       		case self::TRIGGER_REJECTED_AC :
       			
       			$email_data = array(
	                'user' => $data->username,
	                'showname' => $data->showname,
	                'age_certif_link' => 'https://www.escortforumit.xxx/private-v2/escort-age-verification'
	            );

	            $template_id = is_null($data->agency_id) ? 'trigger_ac_irrelevant_profile_escort' : 'trigger_ac_irrelevant_profile_agency';

	            Cubix_Email::sendTemplate($template_id, $data->email, $email_data, $from_addr = null, $from_name = null, $lang);

           		break;
           	case self::TRIGGER_COMPLETED_PROFILE_MISSING_AC :
       			
       			$email_data = array(
	                'user' => $data->username,
	                'showname' => $data->showname,
	                'age_certif_link' => 'https://www.escortforumit.xxx/private-v2/escort-age-verification'
	            );

	            $template_id = is_null($data->agency_id) ? 'trigger_missing_ac_escort' : 'trigger_missing_ac_agency';

	            Cubix_Email::sendTemplate($template_id, $data->email, $email_data, $from_addr = null, $from_name = null, $lang);

           		break;
        }
	    return true;
    }

    public function sendBirthDayEmailAction()
    {
			$this->view->layout()->disableLayout();

			$sql = "SELECT
			e.showname AS showname,
			epv2.email AS email,
			e.id AS id
			FROM
				escorts e
			INNER JOIN escort_profiles_v2 epv2 ON e.id = epv2.escort_id
			WHERE
				epv2.birth_date IS NOT NULL
			AND epv2.email <> \"\"
			AND STR_TO_DATE( CONCAT(YEAR(CURDATE()), '-', MONTH(epv2.birth_date), '-', DAY(epv2.birth_date) ), '%Y-%m-%d' ) = CURDATE()
			AND e.need_age_verification = ?
		";

			$result = $this->_db->query($sql, array(Model_Verifications_AgeVerification::VERIFY))->fetchAll();

			if (!$result) {
					return;
			}

			$model_packages = new Model_Packages();
			$escortV2 = new Model_EscortsV2();
			foreach ($result as $user) {
					if(($escortV2->hasStatusBit($user->id, Model_EscortsV2::ESCORT_STATUS_ACTIVE) || $escortV2->hasStatusBit($user->id, Model_EscortsV2::ESCORT_STATUS_OWNER_DISABLED))
							&& $model_packages->hasActivePackage($user->id)) {
							Cubix_Email::sendTemplate('birthday_message', $user->email, array(
									'showname' => $user->showname
							));
					}
				}

				Cubix_Email::sendTemplate('birthday_message', 'yerematemyan@gmail.com', array('showname' => 'Yerem'));

			die();
    }

    public function sendUpdateProfilesEmailAction(){
        $this->view->layout()->disableLayout();
        $template_id = 'update_profile_info';
        $sql = "SELECT
                u.email,
                u.username,
                IFNULL(u.lang,'en') as lang
                from users as u
                where u.user_type != 'member'
                and (u.last_login_date is null
                or date(u.last_login_date) < DATE_ADD(NOW(), interval -60 DAY ) )";
        $UsersDetails = $this->_db->query($sql)->fetchAll(Zend_Db::FETCH_OBJ);
        if (!empty($UsersDetails)){
            foreach ($UsersDetails as $userDetails){
                $emailData = array(
                    'username'=>$userDetails->username,
                );
                $lang = ($userDetails->lang) ? $userDetails->lang : 'en';
                if($userDetails->email){
                    Cubix_Email::sendTemplate($template_id,$userDetails->email, $emailData, $from_addr = null, $from_name = null, $lang);
                }

            }

        }
        return true;
    }


    public function statisticsAction(){
        $st = microtime(true);
        ini_set("max_execution_time", -1);
        echo "start caching statistics\r\n";
        $this->view->layout()->disableLayout();
        $app_id = Cubix_Application::getId();
        $dashboard = new Model_Dashboard();
        $cache =Zend_Registry::get('cache');
        $cache_key = 'v2_views_of_profile_' . $app_id;
        echo "getting statistics data\r\n";
        $statistics = $dashboard->getStatistics($app_id);
        echo "save into cache\r\n";
        $result = $cache->save($statistics, $cache_key, array(), 7200);
        echo ($result) ? "cache updated\r\n" : "something went wrong\r\n";
        echo "took". (microtime(true) - $st). "\r\n";
        die;
    }

    public function tagesinserateAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
    	$model = new Model_ClassifiedAds();
		$pageCount = $this->getTagesinseratePagesCount();

		$adIds = $this->getTagesinserateAdIds($pageCount);
		
		foreach ($adIds as $adKey => $adId) {
			$ad_exists = $this->_db->fetchOne('select true from classified_ads where inserat_id = ' . $adId);
			if(!$ad_exists){

				$tmp_data = $this->getTagesinserateAdData($adKey);
		        try {
		        	$ad_data = $this->prepareTagesinserateAd($tmp_data['ad'], $adId);
		        	if($ad_data){
		        		$photos = array();
			        	if(count($tmp_data['photo_urls'])){
			        		foreach($tmp_data['photo_urls'] as $photo_url){
			        			$photo = $this->saveTagesinseratePhoto($photo_url);
			        			if($photo && count($photos) < 3){
			        				$photos[$photo['image_id']] = $photo;
			        			}
			        		}
			        	}
						$res = $model->saveAd($ad_data, $photos);
		        	}
		        } catch (Exception $e) {
					var_dump($e->getMessage());die;
		        	
		        }
		        echo($res);
				echo('continue');
			} else{
		        echo('ad exists');

			}
       
		}
		
		
		echo('end');
        die;
	}

	private function getTagesinseratePagesCount()
	{
		libxml_use_internal_errors(true);
        $html = file_get_contents('http://tagesinserate.ch/adstool/index.php');
        $doc = new \DOMDocument();
        $doc->loadHTML($html);
        $xpath = new \DOMXpath($doc);
        $href = $xpath->query('//td[contains(@class,"paging")]//a')->item(0)->getAttribute('href');
        $hrefArr = explode('=',$href);
        $pagesCount = intval(ceil((int)end($hrefArr)/20));
        return $pagesCount;
	}

	private function getTagesinserateAdIds($pageCount)
	{
		libxml_use_internal_errors(true);
		$adIds = array();
		for ($i=0; $i < $pageCount - 1; $i++) { 
			
			$idsArray = array();
			$html = file_get_contents('http://tagesinserate.ch/adstool/index.php?pageNum_rsSelAnzeigen='. $i);
			$doc = new \DOMDocument();
	        $doc->loadHTML($html);
	        $xpath = new \DOMXpath($doc);
	        $href = $xpath->query('//a[contains(@class,"bold")]');
	        foreach($href as $h){
	        	$hrefArr = explode('?', $h->getAttribute('href'));
	        	$id = (int)substr(substr($hrefArr[1],2),0,6);
	        	$idsArray[] = $id;
	        }
	        $idsArray = array_unique($idsArray);
			$adIds = array_merge($adIds, $idsArray);
		}
        return $adIds;
	}

	private function getTagesinserateAdData($id)
	{
		libxml_use_internal_errors(true);
        $html = file_get_contents('http://tagesinserate.ch/adstool/kleinanzeigen_detail.php?pageNum_rsSelAnzeigen='.$id);
        $doc = new \DOMDocument();
        $doc->loadHTML($html);
        $xpath = new \DOMXpath($doc);
        $rows = $xpath->query('//td[contains(@class,"tableAnzeigeHeader")]');
        $images = $xpath->query('//img[contains(@class,"imgBorder")]');
        $tmp_data = array();
        foreach ($rows as $key => $row) {
        	$parent = $row->parentNode;
        	$itemArr = array();
        	foreach ($parent->childNodes as $key => $value) {
        		$text = trim($value->nodeValue);
        		if($text == 'E-Mail senden'){
        			$itemArr[] = $doc->saveHtml($value->childNodes->item(0));

        		}else{
        			$itemArr[] = $text;
        		}
        	}
        	$tmp_data['ad'][$itemArr[0]] = $itemArr[2];
        }
        foreach ($images as $image) {
        	$tmp_data['photo_urls'][] = 'http://tagesinserate.ch/'.$image->getAttribute('src');
        }
        return $tmp_data;
	}

	private function prepareTagesinserateAd($data, $id)
	{

		$categories = array(
			'Sie sucht' 	=>	1,
			'Er sucht' 		=>	2,
			'TS / TV' 		=>	4,
			'Diverse' 		=>	7,
			'Fetisch' 		=>	14,
			'Stellen' 		=>	5,
			'Immobilien'	=>	6,
			'Gay' 			=>	15,
			'Paare' 		=>	3,
			'GangBang' 		=>	9,
		);

		$cantons = array(
			'Aargau' => 19,
			'Appenzell I.Rh.' => 16,
			'Appenzell A.Rh.' => 15,
			'Bern' => 2,
			'Basel' =>	12,
			'Freiburg' => 10,
			'Genf' => 25,
			'Glarus' => 8,
			'Graubünden' => 18,
			'Jura' => 26,
			'Luzern' => 3,
			'Neuenburg' => 24,
			'Nidwalden' => 7,
			'Obwalden' => 6,
			'Solothurn' => 11,
			'St. Gallen' => 17,
			'Schaffhausen' => 14,
			'Schwyz' => 5,
			'Thurgau' => 20,
			'Tessin' => 21,
			'Uri' => 4,
			'Waadtland' => 22,
			'Wallis' => 23,
			'Zug' => 9,
			'Zürich' => 1,
			
		);
		if(!array_key_exists($data['Ort:'], $cantons)){
			return false;
		}
		$email = $this->_db->fetchOne('select inserat_email from anzeigen_privat where Inserat_id = ' . $id);
		$result = array(
			'category_id' => $categories[$data['Rubrik:']],
			'cities' => array($cantons[$data['Ort:']]),
			'phone' => $data['Tel.:'] ? : null,
			'phone_parsed' => $data['Tel.:'] ? : null,
			'phone_prefix_id' => 1,
			'sms_available' => 0,
			'whatsapp_available' => 0,
			'email' => $email ? : null,
			'web' => $data['Homepage:'] ?  $data['Homepage:']  : null,
			'period' => 7,
			'title' => preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $data['Titel:']),
			'text' => preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $data['Text:']),
			'status' => 2,
			'user_id' => null,
			'is_disabled' => 0,
			'search_text' => preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $data['Titel:']) . ' ' . preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $data['Text:']),
			'creation_date' => new Zend_Db_Expr('NOW()'),
			'approvation_date' => new Zend_Db_Expr('NOW()'),
			'expiration_date' => new Zend_Db_Expr('DATE_ADD(NOW(), INTERVAL 7 DAY)'),
			'application_id' => Cubix_Application::getId(),
			'inserat_id' => $id,
		);
		if($result['phone_parsed']){
			if(substr( $result['phone_parsed'], 0, 1 ) === "+"){
				$result['phone'] = substr($result['phone'],3);
			}
			elseif(substr( $result['phone_parsed'], 0, 2 ) == "00"){
				$result['phone'] = substr($result['phone'],4);
				$result['phone_parsed'] = '+' . substr($result['phone_parsed'],2);
			}
			elseif(substr( $result['phone_parsed'], 0, 1 ) == "0"){
				$result['phone'] = substr($result['phone'],1);
				$result['phone_parsed'] = '+41' . substr($result['phone_parsed'],1);
			}
			else{
				$result['phone_parsed'] = '+41' . $result['phone_parsed'];

			}
		}

		return $result;
	}

	private function saveTagesinseratePhoto($url)
	{
		try{
			$ext = end(explode('.', $url));
			$tmp_dir = sys_get_temp_dir();
			$filename = time() . uniqid() . '.' . $ext;
			$img = $tmp_dir . '/' . $filename;
			file_put_contents($img, file_get_contents($url));
			$images = new Cubix_ImagesCommon();
			$file = array();
			$file['tmp_name'] = $img;
			$file['name'] = $filename;
            $photo = $images->save($file);
            unlink($img);
        }catch (Exception $e){
            unlink($img);
	        return false;
        }
		return $photo;
	}

    public function fillCAEscortIdByPhoneAction() {

        set_time_limit(0);
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $app_id = Cubix_Application::getId();
        $cli = new Cubix_Cli();

        if ($app_id != APP_A6){
            $cli->out('Action for APP_A6 only');
            exit(1);
        }
        $pid_file = '/var/run/phone_escort_id_' .$app_id . '.pid';
        $cli->setPidFile($pid_file);
        if ( $cli->isRunning() ) {
            $cli->out('Action is already running ... exitting...');
            exit(1);
        }
        $model_ClassifiedAds = new Model_ClassifiedAds();
        $page = 1;
        $perPage = 9999999;

        $data = $model_ClassifiedAds->getAllAds(
            $page,
            $perPage,
            array('by_phone_escort_id' => 'empty'),
            'id',
            'ASC',
            $count
        );
        $cli->out("-- start -- ".date("Y-m-d H:i"));
        if (!$count){
            $cli->out('nothing to do');
        }
        foreach ($data as $item) {
            if (trim($item->phone)) {
                $phone_number = preg_replace('/[^0-9]+/', '', $item->phone);
                $matched_ids = Model_ClassifiedAds::getAdapter()->fetchAll(
                    "SELECT
                         	ep.escort_id
                         FROM
                         	escort_profiles_v2 ep
                         WHERE
                         	ep.contact_phone_parsed LIKE ?
                         AND ep.contact_phone_parsed IS NOT NULL",
                    "%".$phone_number."%");
                if (count($matched_ids) > 1) {
                    $phone_escort_id = "Multiple";
                }
                elseif (count($matched_ids) == 1) {
                    $phone_escort_id = $matched_ids[0]->id;
                }
                else {
                    $phone_escort_id = "";
                }
            }
            else {
                $phone_escort_id = "";
            }
            try {
                Model_ClassifiedAds::getAdapter()
                    ->update(
                        'classified_ads',
                        array('phone_escort_id' => $phone_escort_id),
                        Model_ClassifiedAds::quote('id = ?', $item->id)
                    );
                $cli->out("{$item->id} add updated (phone_escort_id => '{$phone_escort_id}')");
            } catch (Exception $exception) {
                $cli->out("[fail] {$exception->getMessage()}");
            }
        }
        $cli->out("-- end -- ".date("Y-m-d H:i"));
        die;
	}

	//EGUK-616
	//send email to escorts every 31 days after the last log in
	public function sendEmailToEscortsByLastLoginAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$model = new Model_EscortsV2();
		$escorts = $model->getActiveEscortsByLastLoginInterval();
		if(count($escorts)){
			foreach ($escorts as $escort) {
				$tpl_data = array(
					'firstname' => $escort['showname']
				);
				$res = Cubix_Email::sendTemplate('for_escorts_who_didnt_login', $escort['email'], $tpl_data);
				echo($escort['showname']);
			}	
		}
		echo('Escorts count');
		echo(count($escorts));
        die;
	}

	public function updateNonDetectedIpsCountryFixAction()
    {
        $db = Zend_Registry::get('db');

        // Init Cli
        // ----------------------------
        $cli = $this->_cli = new Cubix_Cli();
        $cli->clear();
        // ----------------------------

        $cli->colorize('green');
        $cli->out('Fetching Ips with missing Country Id');
        

        $step = 10;
        $done = $errors = $pointer = 0;
        $sql = "SELECT * FROM users_last_login WHERE country_id IS NULL";

        // Collect all countries at once, to not request each time inside while loop
        // -------------------------------
        $countryRows = $db->fetchAll('SELECT id, iso, title_en FROM countries');
        $countries = [];
        foreach($countryRows as $countryRow) {
            $countries[$countryRow->iso] = $countries[$countryRow->title_en] = $countryRow;
        }
        unset($countryRows);
        // -------------------------------

        define('LINE_SEPARATOR', '------------------------------------');

        // For testing
        // -----------------------------
        /*$ip = '197.210.64.192';
        $geoData = Model_Geoip::getClientLocation($ip);
        var_dump($geoData);
        exit;*/
        // -----------------------------

        do {

            $skip = ($pointer++) * $step;
            $take = $step;

            $runnableSql = $sql . " LIMIT {$skip}, {$take}";
            $rows = $db->fetchAll($runnableSql);

            $i = count($rows);
            while ($i--) {
                $cli->out($done . ' Ips Fixed | ' . $errors . ' ERRORS');
                $geoData = Model_Geoip::getClientLocation($rows[$i]->ip);

                if(!empty($geoData)) {
                    $countryId = $countries[$geoData['country_iso']]->id;
                    if(empty($countryId)) {
                        $countryId = $countries[$geoData['country']]->id;
                    }

                    if(!empty($countryId)) {
                        $tmpSql = "UPDATE IGNORE users_last_login SET country_id = {$countryId} 
                        WHERE user_id = {$rows[$i]->user_id} 
                        AND login_date = '{$rows[$i]->login_date}' LIMIT 1";
                    }else{
                        $errors++;
                        $cli->out(LINE_SEPARATOR);
                        var_dump($geoData);
                        $cli->out(LINE_SEPARATOR);
                    }

                    try {
                        $db->query($tmpSql);
                        $done++;
                    }catch(\Exception $e) {
                        $cli->out($tmpSql);
                    }
                }else {
                    $errors++;
                }

            }

            if (count($rows) < $step) break;

        } while (count($rows) > 0);

        $cli->out('DONE ! ))')->reset();
        exit;
    }
	
	public function markCamTrustedAction()
	{
		$db = Zend_Registry::get('db');

        $cli = $this->_cli = new Cubix_Cli();
        $cli->clear();
        // ----------------------------

        $cli->colorize('green');
        $cli->out('Fetching Payed Not Trusted Mebers');
		
		$member_ids = $db->fetchOne('SELECT GROUP_CONCAT(m.id) FROM token_requests tr
						INNER JOIN members m ON m.user_id = tr.user_id
						WHERE tr.status = 2 AND m.cam_trusted = 0 
						AND DATE_SUB(now(), INTERVAL + 72 HOUR) >= tr.creation_date');
		
		if($member_ids){
			$db->query('UPDATE members SET cam_trusted = 1 WHERE id IN('. $member_ids .') ');
		}
		$cli->out('DONE ! ))')->reset();
		
        exit;
	}
	
	public function phoneVerificationAction()
	{
		$cli = $this->_cli = new Cubix_Cli();
        $cli->clear();
        // ----------------------------

        $cli->colorize('green');
        $cli->out('Fetching failed phone verification escorts');
		
		$verification_sales = [
			196 => [
				'name' => 'jolyne',
				'phone' => '+41767704542'
			],
			197 => [
				'name' => 'Carrie',
				'phone' => '+41767431209'
			],
			201 => [
				'name' => 'Andrew',
				'phone' => '+41798071084'
			] 
		];
		
		$escorts = $this->_db->fetchAll('SELECT e.id as escort_id, e.showname, u.email, u.sales_user_id FROM escorts_phone_verification epv
					INNER JOIN escorts e ON e.id = epv.escort_id
					INNER JOIN users u ON u.id = e.user_id
					WHERE epv.status = 0 AND e.last_hand_verification_date IS NULL 
					AND epv.notification_sent = 0 AND DATE_SUB(now(), INTERVAL +24 HOUR) >= epv.date
					GROUP BY epv.escort_id');
		
		if(count($escorts) == 0){
			die('No notifications yet');
		}
		
		foreach($escorts as $escort){
			try{
				$cli->out('Sending to '.$escort->showname );
				if( array_key_exists($escort->sales_user_id, $verification_sales)){
					$sales_name = $verification_sales[$escort->sales_user_id]['name'];
					$sales_phone = $verification_sales[$escort->sales_user_id]['phone'];
					Cubix_Email::sendTemplate('failed_phone_verification', $escort->email, array('showname' => $escort->showname, 'sales_name' => $sales_name, 'sales_phone' => $sales_phone));
				}
				else{
					Cubix_Email::sendTemplate('failed_phone_verification_other_sales', $escort->email, array('showname' => $escort->showname));
				}
				
				$this->_db->update('escorts_phone_verification', array('notification_sent' => 1),	$this->_db->quoteInto('escort_id = ?', $escort->escort_id));
						
			}
			catch (Exception $exception) {
                $cli->out("[fail] {$exception->getMessage()}");
            }
		}
		
		die('sent successfully');
		//
	}		

	public function importReviewsAction()
    {
        set_time_limit(0);
        $cli = new Cubix_Cli();
        $cli->clear();
        $pid_file = '/var/run/import-reviews-' . Cubix_Application::getId() . '.pid';
        $cli->setPidFile($pid_file);
        if ( $cli->isRunning() ) {
            $cli->out('Action is already running... exitting...');
            exit(1);
        }

        try {
            $last_id = 1;

            $existing_reviews_query = $this->_db->fetchAll('SELECT reviewId FROM reviews_imported');
            $existing_reviews = array();
            foreach($existing_reviews_query as $review)
            {
                $existing_reviews[] = $review->reviewId;
            }

            do{
                $url = 'https://backend.escortrankings.uk/reviews/getLastReviews?siteToken=erGH78BNM%vb*79NUI&startingId='.$last_id;
                
                $c = curl_init();
                curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($c, CURLOPT_URL, $url);
                curl_setopt($c, CURLOPT_POST, 1);
                curl_setopt($c, CURLOPT_POSTFIELDS, data);

                $contents = json_decode(curl_exec($c));
                if(curl_errno($c)) {
                    $cli->out(curl_error($c));
                    exit(1);
                }
                curl_close($c);

                $data = $contents->message;

                if (count($data) > 0)
                {
                    foreach ($data as $key => $val)
                    {
                        if (!in_array($val->reviewId,$existing_reviews)) {
                            $found_escorts = $this->_db->fetchAll("SELECT
                                                            e.id escort_id, 
                                                            ep.website
                                                        FROM
                                                            escorts e
                                                            LEFT JOIN escort_profiles_v2 ep ON e.id = ep.escort_id 
                                                        WHERE
                                                            ep.contact_phone_parsed IN (?,?) AND ep.contact_phone_parsed IS NOT NULL AND CHAR_LENGTH(ep.contact_phone_parsed) > 5 AND TRIM(ep.contact_phone_parsed) <> '' ", array('0044' . $val->profilePhoneNumber, '0044' . $val->profilePhoneNumber2)
                            );

                            if (count($found_escorts) > 1) {
                                foreach ($found_escorts as $escort) {
                                    if (strlen($val->profileWebsite1) > 0 && $escort->website === $val->profileWebsite1) {
                                        $val->escort_id = $escort->escort_id;
                                        $this->_db->insert('reviews_imported', (array)$val);
                                    }
                                }
                            } else if (count($found_escorts) == 1) {
                                if($val->profileType == 'agency')
                                {
                                    if (strlen($val->profileWebsite1) > 0 && $escort->website === $val->profileWebsite1) {
                                        $val->escort_id = $found_escorts[0]->escort_id;
                                        $this->_db->insert('reviews_imported', (array)$val);
                                    }
                                }else{
                                    $val->escort_id = $found_escorts[0]->escort_id;
                                    $this->_db->insert('reviews_imported', (array)$val);
                                }
                            }
                        }
                    }
                }
                $last_id = $data[count($data)-1]->reviewId;
            }while( count($data) >= 100 );

            $cli->out('done');
            exit(1);
        } catch (Exception $e) {
            $cli->out($cli->out("[fail] {$e->getMessage()}"));
            exit(1);
        }

    }


    public function automaticSmsForContactVerificationAction()
    {
        set_time_limit(0);
        $cli = new Cubix_Cli();
        $cli->clear();
        $pid_file = '/var/run/automatic-sms-for-contact-verification' . Cubix_Application::getId() . '.pid';
        $cli->setPidFile($pid_file);
        if ( $cli->isRunning() ) {
            $cli->out('Action is already running... exitting...');
            exit(1);
        }

        try {

            $sql = 'SELECT
                            e.id,
                            u.id as user_id,
                            e.showname,
                            e.agency_id,
                            e.`status`,
                            u.email,
                            c.slug as city,
                            bu.username as bu_username,
                            bu.id as bu_id,
                            ep.contact_phone_parsed as phone,
                            UNIX_TIMESTAMP(op.date_activated) as date_activated
                        FROM
                            escorts e
                            INNER JOIN users u ON u.id = e.user_id
                            INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
                            INNER JOIN order_packages op ON op.escort_id = e.id AND op.application_id = 5
                            INNER JOIN cities c ON c.id = e.city_id
                            LEFT JOIN backend_users bu ON bu.id = u.sales_user_id 
                        WHERE
                            ~ e.STATUS & ? 
                            AND ~ e.STATUS & ? 
                            AND ( e.STATUS & ? )
                            AND e.country_id = 215
                            AND e.agency_id IS NULL 
                            AND e.last_hand_verification_date IS NULL 
                            AND op.id IN (
                            SELECT
                                MIN(id) 
                            FROM
                                order_packages 
                            WHERE
                                escort_id = e.id 
                                AND date_activated IS NOT NULL AND op.application_id = 5
                            )
                            AND ep.contact_phone_parsed IS NOT NULL
                            AND u.application_id = 5
                            LIMIT ?,? ';

            $start = 0;
            $limit = 100;

            $template_id = 'alertme_verify_phone';
            $login_url = 'https://www.escortguide.co.uk/account/signin';
            $originator = Cubix_Application::getPhoneNumber(Cubix_Application::getId());
            $model_sms = new Model_SMS_Outbox();
            $model_email_outbox = new Model_SMS_EmailOutbox();

            do{
                $escorts = $this->_db->fetchAll($sql,array(ESCORT_STATUS_DELETED,ESCORT_STATUS_INACTIVE,ESCORT_STATUS_ACTIVE,$start++ * $limit,$limit));
//                echo '<pre>';var_dump($escorts);die;
                foreach ($escorts as $escort)
                {
                    $comment_data = array();
                    $sms_data = array();
                    $phone = preg_replace('/^00/', '+', $escort->phone);
                    $code = rand(100000, 999999);
                    $sms_data['phone_number'] = $phone;
                    $sms_data['phone_from'] = $originator;
                    $sms_data['escort_id'] = $comment_data['escort_id'] = $escort->id;
                    $sms_data['is_spam'] = false;
                    $comment_data['comment'] = 'auto sms and mail for confirmation';
                    $comment_data['sales_user_id'] = $escort->bu_id;
                    $comment_data['date'] = date('Y-m-d H:i:s');


                    switch ($escort->bu_username) {
                        case 'jolyne':
                            $bu_data = 'Jolyne +41767704542';
                            $sms_sales = 'Jolyne +41767704542, jolyne@escortguide.co.uk';
                            break;
                        case 'carrie':
                            $bu_data = 'Carrie +41767431209';
                            $sms_sales = 'Carrie +41767431209, carrie@escortguide.co.uk';
                            break;
                        default:
                            $bu_data = 'Jolyne +41767704542 or Carrie +41767431209';
                            $sms_sales = 'info@escortguide.co.uk';
                            break;
                    }

                    $sms_text1 = 'Hi, you still haven’t confirmed your phone number for your escort ad in ' . ucfirst($escort->city) . ' on escortguide.co.uk. Simply reply to this text or write us at info@escortguide.co.uk with code "' . $code . '"';
//                    $sms_text2 = 'Hi, your provider ad is listed for free on escortguide.co.uk with this number. Please confirm that this is the correct number. Reply to this text or e-mail us with the code "' . $code . '". If you want your login details or need help please contact us at: ' . $sms_sales;
                    $sms_text2 = 'Hi, your provider ad is listed for free on escortguide.co.uk. Please confirm that the phone number is correct by replying to this text or by mail at info@escortguide.co.uk with code "' . $code . '".';
                    $last_sms = $this->_db->fetchRow('SELECT UNIX_TIMESTAMP(MAX( date )) as last_sms_date  FROM escort_comments WHERE escort_id = ? AND `comment` LIKE "%auto sms and mail for confirmation%"',array($escort->id));
                    $warnedInLastWeek = $this->_db->fetchOne('SELECT true FROM escort_comments WHERE escort_id = ? AND DATE(`date`) > CURDATE() - INTERVAL 5 DAY AND `comment` LIKE "%auto sms and mail for confirmation%"', array($escort->id));

                    if ($escort->date_activated <= strtotime("-20day") && $escort->date_activated >= strtotime("-21day")) {
                        $sms_data['text'] = $sms_text1;
                        $this->_db->update('escorts', array('phone_sms_verification_code' => $code), $this->_db->quoteInto('id = ?', (int)$escort->id));
                        $model_sms->saveAndSend($sms_data);
                        if ($escort->email != 'no@no.com' && Cubix_Email::sendTemplate($template_id, $escort->email , array('bu_data' => $bu_data, 'phone' => $phone, 'url' => $login_url))) {
                            $data = array(
                                'email' => $escort->email,
                                'subject' => __('phone_verification_request_email_subject'),
                                'text' => __('phone_verification_request_email_text', array('phone' => $phone, 'bu_data' => $bu_data)),
                                'date' => new Zend_Db_Expr('NOW()'),
                                'user_id' => $escort->user_id,
                                'application_id' => Cubix_Application::getId()
                            );
                            $model_email_outbox->add($data);
                        }
                        Model_EscortsV2::insertEscortComments($comment_data);
                    }elseif($escort->date_activated <= strtotime("-11day") && $escort->date_activated >= strtotime("-12day")) {
                        $sms_data['text'] = $sms_text1;
                        $this->_db->update('escorts', array('phone_sms_verification_code' => $code), $this->_db->quoteInto('id = ?', (int)$escort->id));
                        $model_sms->saveAndSend($sms_data);
                        if ($escort->email != 'no@no.com' && Cubix_Email::sendTemplate($template_id, $escort->email , array('bu_data' => $bu_data, 'phone' => $phone, 'url' => $login_url))) {
                            $data = array(
                                'email' => $escort->email,
                                'subject' => __('phone_verification_request_email_subject'),
                                'text' => __('phone_verification_request_email_text', array('phone' => $phone, 'bu_data' => $bu_data)),
                                'date' => new Zend_Db_Expr('NOW()'),
                                'user_id' => $escort->user_id,
                                'application_id' => Cubix_Application::getId()
                            );
                            $model_email_outbox->add($data);
                        }
                        Model_EscortsV2::insertEscortComments($comment_data);
                    }elseif($escort->date_activated <= strtotime("-7day") && $escort->date_activated >= strtotime("-8day")) {
                        $sms_data['text'] = $sms_text2;
                        $this->_db->update('escorts', array('phone_sms_verification_code' => $code), $this->_db->quoteInto('id = ?', (int)$escort->id));
                        $model_sms->saveAndSend($sms_data);
                        if ($escort->email != 'no@no.com' && Cubix_Email::sendTemplate($template_id, $escort->email , array('bu_data' => $bu_data, 'phone' => $phone, 'url' => $login_url))) {
                            $data = array(
                                'email' => $escort->email,
                                'subject' => __('phone_verification_request_email_subject'),
                                'text' => __('phone_verification_request_email_text', array('phone' => $phone, 'bu_data' => $bu_data)),
                                'date' => new Zend_Db_Expr('NOW()'),
                                'user_id' => $escort->user_id,
                                'application_id' => Cubix_Application::getId()
                            );
                            $model_email_outbox->add($data);
                        }
                        Model_EscortsV2::insertEscortComments($comment_data);
                    }elseif($escort->date_activated <= strtotime("-4day") && $escort->date_activated >= strtotime("-5day")) {
                        $sms_data['text'] = $sms_text2;
                        $this->_db->update('escorts', array('phone_sms_verification_code' => $code), $this->_db->quoteInto('id = ?', (int)$escort->id));
                        $model_sms->saveAndSend($sms_data);
                        if ($escort->email != 'no@no.com' && Cubix_Email::sendTemplate($template_id, $escort->email , array('bu_data' => $bu_data, 'phone' => $phone, 'url' => $login_url))) {
                            $data = array(
                                'email' => $escort->email,
                                'subject' => __('phone_verification_request_email_subject'),
                                'text' => __('phone_verification_request_email_text', array('phone' => $phone, 'bu_data' => $bu_data)),
                                'date' => new Zend_Db_Expr('NOW()'),
                                'user_id' => $escort->user_id,
                                'application_id' => Cubix_Application::getId()
                            );
                            $model_email_outbox->add($data);
                        }
                        Model_EscortsV2::insertEscortComments($comment_data);
                    }elseif($escort->date_activated <= strtotime("-2day") && $escort->date_activated >= strtotime("-3day")) {
                        $sms_data['text'] = $sms_text2;
                        $this->_db->update('escorts', array('phone_sms_verification_code' => $code), $this->_db->quoteInto('id = ?', (int)$escort->id));
                        $model_sms->saveAndSend($sms_data);
                        if ($escort->email != 'no@no.com' && Cubix_Email::sendTemplate($template_id, $escort->email, array('bu_data' => $bu_data, 'phone' => $phone, 'url' => $login_url))) {
                            $data = array(
                                'email' => $escort->email,
                                'subject' => __('phone_verification_request_email_subject'),
                                'text' => __('phone_verification_request_email_text', array('phone' => $phone, 'bu_data' => $bu_data)),
                                'date' => new Zend_Db_Expr('NOW()'),
                                'user_id' => $escort->user_id,
                                'application_id' => Cubix_Application::getId()
                            );
                            $model_email_outbox->add($data);
                        }
                        Model_EscortsV2::insertEscortComments($comment_data);
                    }elseif($escort->date_activated <= strtotime("-21day") && !$warnedInLastWeek && $last_sms && $last_sms->last_sms_date && $last_sms->last_sms_date <= strtotime("-7day") ) {
                        $sms_data['text'] = $sms_text1;
                        $this->_db->update('escorts', array('phone_sms_verification_code' => $code), $this->_db->quoteInto('id = ?', (int)$escort->id));
                        $model_sms->saveAndSend($sms_data);
                        if ($escort->email != 'no@no.com' && Cubix_Email::sendTemplate($template_id, $escort->email , array('bu_data' => $bu_data, 'phone' => $phone, 'url' => $login_url))) {
                            $data = array(
                                'email' => $escort->email,
                                'subject' => __('phone_verification_request_email_subject'),
                                'text' => __('phone_verification_request_email_text', array('phone' => $phone, 'bu_data' => $bu_data)),
                                'date' => new Zend_Db_Expr('NOW()'),
                                'user_id' => $escort->user_id,
                                'application_id' => Cubix_Application::getId()
                            );
                            $model_email_outbox->add($data);
                        }
                        Model_EscortsV2::insertEscortComments($comment_data);
                    }
                }

            }while( count($escorts) > 1 );

            $cli->out('done');
            exit(1);
        } catch (Exception $e) {
            $cli->out($cli->out("[fail] {$e->getMessage()}"));
            Cubix_Email::send('vardanina@gmail.com', 'EGUK AUTOMATIC SMS EMAIL FAILED', $e->__toString());
            exit(1);
        }

    }

    public function automaticSmsForContactVerificationEdAction()
    {
        set_time_limit(0);
        $cli = new Cubix_Cli();
        $cli->clear();
        $pid_file = '/var/run/automatic-sms-for-contact-verification' . Cubix_Application::getId() . '.pid';
        $cli->setPidFile($pid_file);
        if ( $cli->isRunning() ) {
            $cli->out('Action is already running... exitting...');
            exit(1);
        }


        try {

            $sql = 'SELECT
                            e.id,
                            u.id as user_id,
                            e.showname,
                            e.agency_id,
                            e.`status`,
                            u.email,
                            c.slug as city,
                            bu.username as bu_username,
                            bu.id as bu_id,
                            ep.contact_phone_parsed as phone,
                            UNIX_TIMESTAMP(op.date_activated) as date_activated,
                            u.lang,
                            u.user_type,
                            co.iso as country_iso
                        FROM
                            escorts e
                            INNER JOIN users u ON u.id = e.user_id
                            INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
                            INNER JOIN order_packages op ON op.escort_id = e.id AND op.application_id = '.Cubix_Application::getId().'
                            INNER JOIN cities c ON c.id = e.city_id
                            INNER JOIN countries co ON co.id = e.country_id
                            LEFT JOIN backend_users bu ON bu.id = u.sales_user_id 
                        WHERE
                            ~ e.STATUS & ? 
                            AND ~ e.STATUS & ? 
                            AND ( e.STATUS & ? )
                            AND e.agency_id IS NULL 
                            AND e.last_hand_verification_date IS NULL 
                            AND e.country_id NOT IN(68,10) 
                            AND op.id IN (
                            SELECT
                                MIN(id) 
                            FROM
                                order_packages 
                            WHERE
                                escort_id = e.id 
                                AND date_activated IS NOT NULL AND op.application_id = '.Cubix_Application::getId().'
                            )
                            AND ep.contact_phone_parsed IS NOT NULL
                            AND u.application_id = '.Cubix_Application::getId().'
                            AND bu.id <> 96
	                        AND u.last_login_date IS NULL
	                        ORDER BY e.id ASC
                            LIMIT ?,? ';

            $start = 0;
            $limit = 100;

            $template_id = 'alertme_verify_phone';
            $login_url = 'https://www.escortdirectory.com/account/signin';
            $originator = Cubix_Application::getPhoneNumber(Cubix_Application::getId());
            $model_sms = new Model_SMS_Outbox();
            $model_email_outbox = new Model_SMS_EmailOutbox();
            $i = 0;
            do{
                $escorts = $this->_db->fetchAll($sql,array(ESCORT_STATUS_DELETED,ESCORT_STATUS_INACTIVE,ESCORT_STATUS_ACTIVE,$start++ * $limit,$limit));

                foreach ($escorts as $escort)
                {

                    $comment_data = array();
                    $sms_data = array();
                    $phone = preg_replace('/^00/', '+', $escort->phone);
                    $sms_data['phone_number'] = $phone;
                    $sms_data['phone_from'] = $originator;
                    $sms_data['escort_id'] = $comment_data['escort_id'] = $escort->id;
                    $sms_data['is_spam'] = false;
                    $comment_data['comment'] = 'auto sms and mail for confirmation';
                    $comment_data['sales_user_id'] = $escort->bu_id;
                    $comment_data['date'] = date('Y-m-d H:i:s');

                    $hasSales = true;
                    switch ($escort->bu_id) {
                        case 196:
                            $bu_username = 'Jolyne';
                            $bu_email = 'jolyne@escortdirectory.com';
                            $bu_phone = '+41767704542';
                            $bu_skype = 'sales_84780';
                            break;
                        case 197:
                            $bu_username = 'Carrie';
                            $bu_email = 'carrie@escortdirectory.com';
                            $bu_phone = '+41767431209';
                            $bu_skype = 'carrie.smith3110';
                            break;
                        case 201:
                            $bu_username = 'Andrew';
                            $bu_email = 'andrew@escortdirectory.com';
                            $bu_phone = '+41798071084';
                            $bu_skype = 'andrew.williams29_1';
                            break;
                        case 213:
                            $bu_username = 'Barbara';
                            $bu_email = 'barbara@escortdirectory.com';
                            $bu_phone = '+41767704542';
                            $bu_skype = '+41767704542';
                            break;
                        default:
                            $hasSales = false;
                            $bu_username = 'Jolyne';
                            $bu_email = 'jolyne@escortdirectory.com';
                            $bu_phone = '+41767704542 or +41767431209';
                            $bu_skype = '+41767704542';
                            break;
                    }

                    $germanyLangCountries = array('de','at','ch');
                    $frenchLangCountries = array('fr','be');
                    $portugueseLangCountries = array('br','pt');
                    $spanishLangCountries = array('es','mx','ar');

                    $lastSentLang = $model_sms->getLastSmsLang($escort->id);

                    $lng_ = 'en';
                    if ( in_array($escort->country_iso,$germanyLangCountries) )
                    {
                        $lng_ = ($lastSentLang && $lastSentLang == 'de')?'en':'de';
                    }elseif (in_array($escort->country_iso,$frenchLangCountries))
                    {
                        $lng_ = ($lastSentLang && $lastSentLang == 'fr')?'en':'fr';
                    }elseif (in_array($escort->country_iso,$portugueseLangCountries))
                    {
                        $lng_ = ($lastSentLang && $lastSentLang == 'pt')?'en':'pt';
                    }elseif (in_array($escort->country_iso,$spanishLangCountries))
                    {
                        $lng_ = ($lastSentLang && $lastSentLang == 'es')?'en':'es';
                    }elseif ($escort->country_iso == 'hu')
                    {
                        $lng_ = ($lastSentLang && $lastSentLang == 'hu')?'en':'hu';
                    }elseif ($escort->country_iso == 'ro')
                    {
                        $lng_ = ($lastSentLang && $lastSentLang == 'ro')?'en':'en';
                    }

                    $emailAdditionForSales = $hasSales ? __('automatic_sms_has_sales_addition', array('bu_username' => $bu_username, 'bu_phone' => $bu_phone, 'bu_skype' => $bu_skype, 'bu_email' => $bu_email)) : null;
                    $adUrl = 'https://escortdirectory.com/escort/' . preg_replace('/\s+/', '%20', $escort->showname) . '-' . $escort->id . '/';
                    $last_sms = $this->_db->fetchRow('SELECT UNIX_TIMESTAMP(MAX( date )) as last_sms_date  FROM escort_comments WHERE escort_id = ? AND `comment` LIKE "%auto sms and mail for confirmation%"', array($escort->id));
                    $warnedInLastWeek = $this->_db->fetchOne('SELECT true FROM escort_comments WHERE escort_id = ? AND DATE(`date`) > CURDATE() - INTERVAL 5 DAY AND `comment` LIKE "%auto sms and mail for confirmation%"', array($escort->id));

                    $sms_data['lang'] = $lng_;
                    $sms_sent = false;
                    $email_sent = false;
                    if ($escort->date_activated <= strtotime("-11day") /*&& $escort->date_activated >= strtotime("-12day")*/ && !$warnedInLastWeek ) {
                        $template_id = 'alertme_verify_phone_expire_soon';
                        $sms_data['text'] = $model_sms->getAutomaticSmsText($escort,$lng_);
                        $model_sms->saveAndSend($sms_data);
                        if ($escort->email && $escort->email != 'no@no.com' && Cubix_Email::sendTemplate($template_id, $escort->email, array('user_type' => $escort->user_type, 'city' => ucfirst($escort->city), 'originator' => $originator, 'ad_url' => $adUrl, 'has_sales' => $emailAdditionForSales, 'login_url' => $login_url), null, null, $lng_)) {
                            $data = array(
                                'email' => $escort->email,
                                'subject' => Cubix_I18n::translateByLng($lng_, 'phone_verification_request_email_subject2', array('user_type' => $escort->user_type)),
                                'text' => Cubix_I18n::translateByLng($lng_, 'phone_verification_request_email_text2', array('user_type' => $escort->user_type, 'city' => ucfirst($escort->city), 'originator' => $originator, 'ad_url' => $adUrl, 'has_sales' => $emailAdditionForSales, 'login_url' => $login_url)),
                                'date' => new Zend_Db_Expr('NOW()'),
                                'user_id' => $escort->user_id,
                                'backend_user_id' => $escort->bu_id,
                                'template_id' => $template_id,
                                'application_id' => Cubix_Application::getId()
                            );
                            $model_email_outbox->add($data);
                            $email_sent = true;
                        }
                        $sms_sent = true;
                        Model_EscortsV2::insertEscortComments($comment_data);
                    } elseif ($escort->date_activated <= strtotime("-7day") && $escort->date_activated >= strtotime("-8day")) {
                        $sms_data['text'] = $model_sms->getAutomaticSmsText($escort,$lng_);
                        $model_sms->saveAndSend($sms_data);
                        if ($escort->email && $escort->email != 'no@no.com' && Cubix_Email::sendTemplate($template_id, $escort->email, array('user_type' => $escort->user_type, 'city' => ucfirst($escort->city), 'originator' => $originator, 'ad_url' => $adUrl, 'has_sales' => $emailAdditionForSales, 'login_url' => $login_url), null, null, $lng_)) {
                            $data = array(
                                'email' => $escort->email,
                                'subject' => Cubix_I18n::translateByLng($lng_, 'phone_verification_request_email_subject', array('user_type' => $escort->user_type)),
                                'text' => Cubix_I18n::translateByLng($lng_, 'phone_verification_request_email_text', array('user_type' => $escort->user_type, 'city' => ucfirst($escort->city), 'originator' => $originator, 'ad_url' => $adUrl, 'has_sales' => $emailAdditionForSales, 'login_url' => $login_url)),
                                'date' => new Zend_Db_Expr('NOW()'),
                                'user_id' => $escort->user_id,
                                'backend_user_id' => $escort->bu_id,
                                'template_id' => $template_id,
                                'application_id' => Cubix_Application::getId()
                            );
                            $model_email_outbox->add($data);
                            $email_sent = true;
                        }
                        $sms_sent = true;
                        Model_EscortsV2::insertEscortComments($comment_data);
                    } elseif ($escort->date_activated <= strtotime("-5day") && $escort->date_activated >= strtotime("-6day")) {
                        $sms_data['text'] = $model_sms->getAutomaticSmsText($escort,$lng_);
                        $model_sms->saveAndSend($sms_data);
                        if ($escort->email && $escort->email != 'no@no.com' && Cubix_Email::sendTemplate($template_id, $escort->email, array('user_type' => $escort->user_type, 'city' => ucfirst($escort->city), 'originator' => $originator, 'ad_url' => $adUrl, 'has_sales' => $emailAdditionForSales, 'login_url' => $login_url), null, null, $lng_)) {
                            $data = array(
                                'email' => $escort->email,
                                'subject' => Cubix_I18n::translateByLng($lng_, 'phone_verification_request_email_subject', array('user_type' => $escort->user_type)),
                                'text' => Cubix_I18n::translateByLng($lng_, 'phone_verification_request_email_text', array('user_type' => $escort->user_type, 'city' => ucfirst($escort->city), 'originator' => $originator, 'ad_url' => $adUrl, 'has_sales' => $emailAdditionForSales, 'login_url' => $login_url)),
                                'date' => new Zend_Db_Expr('NOW()'),
                                'user_id' => $escort->user_id,
                                'backend_user_id' => $escort->bu_id,
                                'template_id' => $template_id,
                                'application_id' => Cubix_Application::getId()
                            );
                            $model_email_outbox->add($data);
                            $email_sent = true;
                        }
                        $sms_sent = true;
                        Model_EscortsV2::insertEscortComments($comment_data);
                    } elseif ($escort->date_activated <= strtotime("-3day") && $escort->date_activated >= strtotime("-4day")) {
                        $sms_data['text'] = $model_sms->getAutomaticSmsText($escort,$lng_);
                        $model_sms->saveAndSend($sms_data);
                        if ($escort->email && $escort->email != 'no@no.com' && Cubix_Email::sendTemplate($template_id, $escort->email, array('user_type' => $escort->user_type, 'city' => ucfirst($escort->city), 'originator' => $originator, 'ad_url' => $adUrl, 'has_sales' => $emailAdditionForSales, 'login_url' => $login_url), null, null, $lng_)) {
                            $data = array(
                                'email' => $escort->email,
                                'subject' => Cubix_I18n::translateByLng($lng_, 'phone_verification_request_email_subject', array('user_type' => $escort->user_type)),
                                'text' => Cubix_I18n::translateByLng($lng_, 'phone_verification_request_email_text', array('user_type' => $escort->user_type, 'city' => ucfirst($escort->city), 'originator' => $originator, 'ad_url' => $adUrl, 'has_sales' => $emailAdditionForSales, 'login_url' => $login_url)),
                                'date' => new Zend_Db_Expr('NOW()'),
                                'user_id' => $escort->user_id,
                                'backend_user_id' => $escort->bu_id,
                                'template_id' => $template_id,
                                'application_id' => Cubix_Application::getId()
                            );
                            $model_email_outbox->add($data);
                            $email_sent = true;
                        }
                        $sms_sent = true;
                        Model_EscortsV2::insertEscortComments($comment_data);
                    } elseif ($escort->date_activated <= strtotime("-1day") && $escort->date_activated >= strtotime("-2day")) {
                        $sms_data['text'] = $model_sms->getAutomaticSmsText($escort,$lng_);
                        $model_sms->saveAndSend($sms_data);
                        if ($escort->email && $escort->email != 'no@no.com' && Cubix_Email::sendTemplate($template_id, $escort->email, array('user_type' => $escort->user_type, 'city' => ucfirst($escort->city), 'originator' => $originator, 'ad_url' => $adUrl, 'has_sales' => $emailAdditionForSales, 'login_url' => $login_url), null, null, $lng_)) {
                            $data = array(
                                'email' => $escort->email,
                                'subject' => Cubix_I18n::translateByLng($lng_, 'phone_verification_request_email_subject', array('user_type' => $escort->user_type)),
                                'text' => Cubix_I18n::translateByLng($lng_, 'phone_verification_request_email_text', array('user_type' => $escort->user_type, 'city' => ucfirst($escort->city), 'originator' => $originator, 'ad_url' => $adUrl, 'has_sales' => $emailAdditionForSales, 'login_url' => $login_url)),
                                'date' => new Zend_Db_Expr('NOW()'),
                                'user_id' => $escort->user_id,
                                'backend_user_id' => $escort->bu_id,
                                'template_id' => $template_id,
                                'application_id' => Cubix_Application::getId()
                            );
                            $model_email_outbox->add($data);
                            $email_sent = true;
                        }
                        $sms_sent = true;
                        Model_EscortsV2::insertEscortComments($comment_data);
                    } elseif ($escort->date_activated <= strtotime("-11day") && !$warnedInLastWeek && $last_sms && $last_sms->last_sms_date && ($last_sms->last_sms_date <= strtotime("-2day") || $last_sms->last_sms_date <= strtotime("-4day") || $last_sms->last_sms_date <= strtotime("-7day") || $last_sms->last_sms_date <= strtotime("-11day"))) {
                        $template_id = 'alertme_verify_phone_expire_soon';
                        $sms_data['text'] = $model_sms->getAutomaticSmsText($escort,$lng_);
                        $model_sms->saveAndSend($sms_data);
                        if ($escort->email && $escort->email != 'no@no.com' && Cubix_Email::sendTemplate($template_id, $escort->email, array('user_type' => $escort->user_type, 'city' => ucfirst($escort->city), 'originator' => $originator, 'ad_url' => $adUrl, 'has_sales' => $emailAdditionForSales, 'login_url' => $login_url), null, null, $lng_)) {
                            $data = array(
                                'email' => $escort->email,
                                'subject' => Cubix_I18n::translateByLng($lng_, 'phone_verification_request_email_subject', array('user_type' => $escort->user_type)),
                                'text' => Cubix_I18n::translateByLng($lng_, 'phone_verification_request_email_text', array('user_type' => $escort->user_type, 'city' => ucfirst($escort->city), 'originator' => $originator, 'ad_url' => $adUrl, 'has_sales' => $emailAdditionForSales, 'login_url' => $login_url)),
                                'date' => new Zend_Db_Expr('NOW()'),
                                'user_id' => $escort->user_id,
                                'backend_user_id' => $escort->bu_id,
                                'template_id' => $template_id,
                                'application_id' => Cubix_Application::getId()
                            );
                            $model_email_outbox->add($data);
                            $email_sent = true;
                        }
                        $sms_sent = true;
                        Model_EscortsV2::insertEscortComments($comment_data);
                    }

                    if ( $sms_sent )
                    {
                        $i++;
                        $cli->out('Warning SMS was sent to ' . $escort->user_type . ' to phone: ' . $sms_data['phone_number']);

                        if ($email_sent)
                            $cli->out('Warning Email was sent to ' . $escort->user_type . ' to email: ' . $escort->email );
                    }

                }

            }while( count($escorts) > 1 && $i <= 300 );

            $cli->out('done');
            exit(1);
        } catch (Exception $e) {
            $cli->out($cli->out("[fail] {$e->getMessage()}"));
            Cubix_Email::send('vardaninan@gmail.com', 'EDIR AUTOMATIC SMS EMAIL FAILED', $e->__toString());
            exit(1);
        }

    }


    public function syncEgukWithEdAction()
    {
        set_time_limit(0);
        ini_set('memory_limit', '1024M');
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $cli = new Cubix_Cli();
        $pid_file = '/var/run/sync_eguk_with_ed-' . Cubix_Application::getId() . '.pid';
        $cli->setPidFile($pid_file);

        if ( $cli->isRunning() ) {
            $cli->out('Action is already running... exitting...');
            exit(1);
        }

        $client = new Cubix_EscortCopier_Client(APP_ED, true);
        $escort_model = new Model_EscortsV2();
        $packages_model = new Model_Billing_Packages();
        $country_model = new Model_Countries();

        $timer_start = microtime(true);

        // <editor-fold defaultstate="collapsed" desc="Getting escorts which have been changed">

        //Taking escorts which have been synced already and have changes.
        //and escorts which are never synced but has package with product ED LISTING
        //escorts which had never been synced(never had ED LISTING product) excluded

        //Get escort Ids for sync
        $eventTypes = implode(',', array(
            Cubix_SyncNotifier::EVENT_ESCORT_CREATED,
            Cubix_SyncNotifier::EVENT_ESCORT_UPDATED,
            Cubix_SyncNotifier::EVENT_ESCORT_STATUS_UPDATED,
            Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_ADDED,
            Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED,
            Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_DELETED,
            Cubix_SyncNotifier::EVENT_ESCORT_PHONE_CHANGED,
            Cubix_SyncNotifier::EVENT_PACKAGE_ACTIVATED,
            Cubix_SyncNotifier::EVENT_PACKAGE_DEACTIVATED,
            Cubix_SyncNotifier::EVENT_ED_LISTING_PRODUCT_ADDED,
            Cubix_SyncNotifier::EVENT_ED_LISTING_PRODUCT_DELETED
        ));
        $first_sync = false;

        if ($first_sync)
        {
            $ids = $this->_db->fetchCol("
                SELECT
                   DISTINCT e.id
                FROM
                   escorts e
                INNER JOIN users u ON u.id = e.user_id
                WHERE
                   e.status & ?
                   AND ~ e.status & ?
                   AND ~ e.status & ?
                   AND e.agency_id IS NULL
                   AND e.country_id = 215
                   AND u.application_id = ?
                   AND e.sync_aprooved = 1
            ",array(ESCORT_STATUS_ACTIVE,ESCORT_STATUS_DELETED,ESCORT_STATUS_INACTIVE,APP_EG_CO_UK));
        }else{
            $ids = $this->_db->fetchCol("
                SELECT
                   DISTINCT escort_id
                FROM
                   journal AS j
                INNER JOIN escorts e ON j.escort_id = e.id
                INNER JOIN users u ON e.user_id = u.id
                WHERE
                   e.status & ?
                   AND ~ e.status & ?
                   AND ~ e.status & ?
                   AND e.agency_id IS NULL
                   AND e.country_id = 215
                   AND u.application_id = ?
                   AND e.sync_aprooved = 1
                   AND j.event_type IN ($eventTypes)
            ",array(ESCORT_STATUS_ACTIVE,ESCORT_STATUS_DELETED,ESCORT_STATUS_INACTIVE,APP_EG_CO_UK));
        }

        $escorts = array();

        if (count($ids) > 0){
            try{
                if (!$first_sync)
                {
                    $escorts = $this->_db->fetchAll('
			        SELECT
			        	e.id, e.showname, e.agency_id, psl.last_sync_date, e.status, psl.package_status,psl.last_sync_date AS last_sync_date,
			        	op.order_id, op.package_id, op.status AS package_status, op.id as order_package_id, op.expiration_date,
			        	if(op.order_id IS NOT NULL, 1, 0) has_premium_package, c.slug AS country_slug,c.id AS country_id, e.last_hand_verification_date
			        FROM journal j
			        INNER JOIN escorts e ON j.escort_id = e.id
			        LEFT JOIN escorts_projects_sync_log psl ON psl.escort_id = e.id AND psl.sync_app_id = ?
			        LEFT JOIN order_packages op ON op.escort_id = e.id AND op.status = ?
			        LEFT JOIN countries c ON c.id = e.country_id
			        WHERE e.id IN ('.implode(',', $ids).') AND (psl.escort_id IS NULL OR j.date > psl.last_sync_date)
			        	AND e.agency_id IS NULL
			        GROUP BY e.id
		        ', array(APP_ED, Model_Packages::STATUS_ACTIVE ));
                }else{
                    $escorts = $this->_db->fetchAll('
			        SELECT
			        	e.id, e.showname, e.agency_id, e.status, psl.last_sync_date,psl.package_status,
			        	op.order_id, op.package_id, op.status AS package_status, opp.order_package_id, op.expiration_date,
			        	if(opp.order_package_id AND opp2.order_package_id IS NULL AND op.order_id IS NOT NULL, 1, 0) has_premium_package, c.slug AS country_slug,c.id AS country_id, e.last_hand_verification_date
			        FROM escorts e
			        LEFT JOIN escorts_projects_sync_log psl ON psl.escort_id = e.id AND psl.sync_app_id = ?
			        LEFT JOIN order_packages op ON op.escort_id = e.id AND op.status = ?
			        LEFT JOIN order_package_products opp ON op.id = opp.order_package_id AND opp.product_id = ?
			        LEFT JOIN order_package_products opp2 ON op.id = opp2.order_package_id AND opp2.product_id = ?
			        LEFT JOIN countries c ON c.id = e.country_id
			        WHERE e.id IN ('.implode(',', $ids).')
			            AND e.agency_id IS NULL
			        GROUP BY e.id
			        ORDER BY e.id DESC
		        ', array(APP_ED,Model_Packages::STATUS_ACTIVE, PRODUCT_ED_LISTING, PRODUCT_NO_ED_LISTING ));
                }

            }catch (Exception $exception){
                Cubix_Email::send('vardanina@gmail.com', 'EGUK -> EDIR Escorts copy failed', $exception->__toString());
                return;
            }
        }
        // </editor-fold>


        $escorts_count = count($escorts);
        $successfullyMoved = '';
        $cli->out($escorts_count . ' escorts to update');

        $chunk_size = 10;
        $chunks = ceil($escorts_count / $chunk_size);

        for( $i = 1; $i <= $chunks; $i++ ) {
            if ( $chunks == 1 || $i == $chunks ) {
                $cli->out('Getting profiles ' . $escorts_count . '/' . $escorts_count);
            } else {
                $cli->out('Getting profiles ' . $chunk_size * $i . '/' . $escorts_count);
            }

            if (  $i == $chunks ) $chunk_size = $escorts_count - $chunk_size * ($i - 1);

            for( $y = 0; $y < $chunk_size; $y++ ) {
                $_escort = $escorts[($i - 1) * $chunk_size + $y];

                $escort = $escort_model->getProfile($_escort->id);

                $add_data = $escort_model->getAdditional($_escort->id);
                $working_locations = $escort_model->getWorkingLocations($_escort->id);
                $tours = $escort_model->getTours($_escort->id);

                $escort = array_merge((array)$escort, (array)$add_data);
                $escort['working_locations'] = $working_locations;
                $escort['tours'] = $tours;
                $escort['country_slug'] = $_escort->country_slug;
                $escort['country_id'] = $_escort->country_id;
                $escort['keywords'] = $escort_model->getEscortKeywords($_escort->id);
                $escort['last_hand_verification_date'] = $_escort->last_hand_verification_date;
                $escort['block_countries'] = $country_model->getBlockCountries($_escort->id);

                $cli->out(' -ID-'.$_escort->id.'- ', false);
                $result = $client->call('CopyListener.takeCopyOfEscort', array($escort, true));
                $errors = $warnings = array();
                $cli->out($_escort->showname, false);
                switch ( $result->result ) {
                    case self::RESULT_AGENCY_DOESNT_EXIST:
                        /*$cli->out("Agency {$data['agency_data']->name} doesn't exist in target application. Probably agency copy failed before.");
                        $errors[] = "Agency {$data['agency_data']->name} doesn't exist in target application. Probably agency copy failed before.";
                        $cli->out('Failed');*/
                        break;
                    case self::RESULT_ESCORT_TAKEN_SUCCESSFULLY:
                        $cli->out(' moved.', false);
                        if ( count($result->warnings) ) {
                            foreach ( $result->warnings as $warning ) {
                                $warnings[] = $warning;
                            }
                        }
                        if ($this->_db->fetchOne('SELECT TRUE FROM escorts_projects_sync_log WHERE escort_id = '.$_escort->id))
                        {
                            $successfullyMoved .= 'Updated successfully/ UK_id: '.$_escort->id.' ED_id: '.$result->id. ',<br>';
                        }else{
                            $successfullyMoved .= 'Moved successfully/ UK_id: '.$_escort->id.' ED_id: '.$result->id. ',<br>';
                        }                        //If escort is active and has ED LISTING product activate TRANSIT PACKAGE in ED
                        //Otherwise remove package, no matter it has now active TRANSIT PACKAGE or not
                        $package_status = 0;
                        if ( $_escort->has_premium_package && $_escort->status & ESCORT_STATUS_ACTIVE ) {
                            $date_diff = strtotime($_escort->expiration_date) - time();

                            $data = array(
                                'id'	=> $result->id,//new escort id
                                'premium_cities'	=> $packages_model->getPremiumCities($_escort->order_package_id),
                                'package_duration'	=> floor($date_diff/(60*60*24)) + 1
                            );
                            $p_result = $client->call('CopyListener.activateTransitPackage', array($data));
                            if($p_result === true){
                                $cli->out('Already has a paid package ...', false);
                            }
                            else{
                                $cli->out('Transit Package Activated'. $p_result. ' ...', false);
                            }

                            $package_status = 1;
                        } else/*if ( is_null($_escort->order_package_id) && ! is_null($_escort->last_sync_date) )*/ {
                            $cli->out(' Removing Transit Package ...', false);
                            $p_result = $client->call('CopyListener.removeTransitPackage', array($_escort->id, Cubix_Application::getId()));
                        }

                        $need_photo_sync = true;

                        if ( is_null($_escort->last_sync_date) ) {
                            $this->_db->insert('escorts_projects_sync_log',
                                array(
                                    'sync_app_id'	=> APP_ED,
                                    'escort_id'		=> $_escort->id,
                                    'last_sync_date' => new Zend_Db_Expr('NOW()'),
                                    'package_status'	=> $package_status
                                )
                            );
                        } else {
                            $this->_db->query('
								UPDATE escorts_projects_sync_log
								SET last_sync_date = NOW(), package_status = ?
								WHERE sync_app_id = ? AND escort_id = ?
							', array($package_status, APP_ED, $_escort->id));

                            $need_photo_sync = $escort_model->hadPhotoModification($_escort->id, $_escort->last_sync_date);

                        }

                        if($need_photo_sync){

                            // copy escort photos
                            /* @var $escort Model_EscortV2Item */
                            $escort = $escort_model->get($_escort->id);
                            /* @var $photos Model_Escort_PhotoItem[] */
                            $photos = $escort->getPhotosForEDSync();
                            //$photos = $escort->getPhotos(null, null, true, true);
                            $unsuccessful_count = 0;

                            $result = $client->call('CopyListener.removeOldEscortPhotos', array(
                                $escort->id,
                                $escort->application_id,
                            ));

                            $cli->out(' Moving All photos...', false);

                            foreach ( $photos as $photo ) {
                                try {
                                    $result = $client->call('CopyListener.takeCopyOfEscortPhoto', array(
                                        $escort->id,
                                        (array) $photo,
                                        $escort->application_id,
                                        $photo->getUrl(),
                                        $photo->type == ESCORT_PHOTO_TYPE_PRIVATE
                                    ));
                                    if ( $result->result != self::RESULT_ESCORT_PHOTO_SUCCESS ) {
                                        $unsuccessful_count++;
                                    }
                                } catch(Exception $ex) {
                                    $cli->out($ex->getMessage());
                                }
                            }

                            if ( $unsuccessful_count > 0 ) {
                                $cli->out(count($photos) - $unsuccessful_count . '/' . count($photos));
                            }
                        }
                        $cli->out('Done.');
                        break;

                    default:
                        if ( count($result->errors) ) {
                            $cli->out(' ID:' . $_escort->id . ' There were validation errors.');
                            foreach ( $result->errors as $key => $msg ) {
                                $errors[] = "Escort ID ".$key." :: ".addslashes($escort[$key])." > ".$msg;
                            }
                        }
                        break;
                }

                if ( count($errors) || count($warnings) ) {
                    $escort_errors[$_escort->id] = array('new_escort_id' => $result->id);

                    if ( count($errors) ) {
                        $escort_errors[$_escort->id]['errors'] = implode("\n", $errors);
                    }

                    if ( count($warnings) ) {
                        $escort_errors[$_escort->id]['warnings'] = implode("\n", $warnings);
                    }
                }
            }
        }

        $cli->out('***Escorts done***');


        $timer_end = microtime(true);

        $cli->out('Duration ' . round(($timer_end - $timer_start) / 60) . ' minutes.');

        $report = '';

        foreach($escort_errors as $id => $error) {
            $report .= 'Escort Source ID - ' . $id . ", Target ID - " . $error['new_escort_id'] . "<br>";
            if ( $error['errors'] ) {
                $report .= 'Errors: ' . $error['errors'] . "<br>";
            }

            if ( $error['warnings'] ) {
                $report .= 'Warnings: ' . $error['warnings'] . "<br>";
            }
        }
        Cubix_Email::send('vardaninan@gmail.com', date('Y-m-d', time()) . ' ED sync report. App id ' . Cubix_Application::getId(), $report, true);
        Cubix_Email::send('vardaninan@gmail.com', ' Escorts Copied from EGUK to ED ', $successfullyMoved, true);
        Cubix_Email::send('sharp1@protonmail.com', ' Escorts Copied from EGUK to ED ', $successfullyMoved, true);
        Cubix_Email::send('info@escortguide.co.uk', ' Escorts Copied from EGUK to ED ', $successfullyMoved, true);
    }

    public function unsubscribeEscortsNewsletterAction()
    {
        set_time_limit(0);
        $cli = new Cubix_Cli();
        $cli->clear();
        $pid_file = '/var/run/unsubscribe-deleted-disabled-escorts-newsletter' . Cubix_Application::getId() . '.pid';
        $cli->setPidFile($pid_file);
        if ( $cli->isRunning() ) {
            $cli->out('Action is already running... exitting...');
            exit(1);
        }

        try {
            $where = '';
            if (Cubix_Application::getId() == APP_EG_CO_UK)
                $where .= ' AND e.country_id = 215';

            $sql = 'SELECT
                        u.email
                    FROM
                        escorts AS e
                        INNER JOIN users u ON u.id = e.user_id
                    WHERE
                        e.agency_id IS NULL
                        AND (e.STATUS & ? 
                        OR e.`status` & ? )
                        '.$where.'
                    GROUP BY e.id
                            LIMIT ?,? ';

            $start = 0;
            $limit = 100;

            do{
                $escorts = $this->_db->fetchAll($sql,array(ESCORT_STATUS_ADMIN_DISABLED,ESCORT_STATUS_DELETED,$start++ * $limit,$limit));
//                echo '<pre>';var_dump($escorts);die;
                foreach ($escorts as $escort)
                {
                    $m_n_i = new Cubix_Newsman_Ids();
                    $n_ids = $m_n_i->get(Cubix_Application::getId());

                    $list_id = reset(array_keys($n_ids));

                    $nm = new Cubix_Newsman($_SERVER["REMOTE_ADDR"]);
                    $nm->unsubscribe($list_id, $escort->email);
                }

            }while( count($escorts) > 1 );

            $cli->out('done');
            exit(1);
        } catch (Exception $e) {
            $cli->out($cli->out("[fail] {$e->getMessage()}"));
            Cubix_Email::send('vardanina@gmail.com', 'EGUK DIS-DEL ESCORTS UNSUBSCRIBING FAILED', $e->__toString());
            exit(1);
        }
    }

    public function copyKeywordsAction()
    {
        set_time_limit(0);
        ini_set('memory_limit', '1024M');
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $cli = new Cubix_Cli();
        $pid_file = '/var/run/copy-keywords-messengers-' . Cubix_Application::getId() . '.pid';
        $cli->setPidFile($pid_file);

        if ($cli->isRunning()) {
            $cli->out('Action is already running... exitting...');
            exit(1);
        }
        $db = Zend_Registry::get('db');


        if ($this->_request->isPost() && IS_CLI != 1) {
            $data = new Cubix_Form_Data($this->_request);

            $data->setFields(array(
                'escort_id' => 'int',
                'keywords' => 'arr-int',
                'messengers' => 'arr',
                'last_check_date' => 'arr'
            ));

            $data = $data->getData();
            $ed_escort_id = $db->fetchOne("SELECT escort_id FROM copied_escorts WHERE orig_app_id = ".APP_EG_CO_UK." AND orig_escort_id = " . $data['escort_id']);
            if ($ed_escort_id) {
                if (count($data['keywords'])) {
                    foreach ($data['keywords'] as $keyword) {
                        if (!$db->fetchOne("SELECT TRUE FROM escort_keywords WHERE escort_id = ? AND keyword_id = ?", array($ed_escort_id, $keyword))) {
                            $db->insert('escort_keywords', array('escort_id' => $ed_escort_id, 'keyword_id' => $keyword));
                        }
                    }
                }

                if (count($data['messengers'])){
                    if (!$data['messengers']['birth_date'] && array_key_exists('birth_date', $data['messengers']))
                        unset($data['messengers']['birth_date']);

                    $db->update('escort_profiles_v2', $data['messengers'] ,$db->quoteInto('escort_id = ?', $ed_escort_id));
                }

                if (count($data['last_check_date']) && $data['last_check_date']['last_hand_verification_date']){
                    $db->update('escorts', $data['last_check_date'] ,$db->quoteInto('id = ?', $ed_escort_id));
                }

                echo json_encode(array('status' => 'success', 'message' => 'Success', 'escort_id' => $ed_escort_id));return;
            } else {
                echo json_encode(array('status' => 'error', 'message' => 'No such escort found'));return;
            }
        } else {
            try {
                $client = new Cubix_EscortCopier_Client(APP_ED, true);
                $ids = $db->fetchCol("SELECT escort_id FROM escorts_projects_sync_log WHERE sync_app_id = ".APP_ED." AND UNIX_TIMESTAMP(last_sync_date) > UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 2 DAY))");
                $escort_model = new Model_EscortsV2();
//                echo '<pre>';var_dump($ids);die;
                foreach ($ids as $id) {
                    $keywords = $escort_model->getEscortKeywords($id);
                    $messengers = $escort_model->getEscortMessengers($id,Cubix_Application::getId());
                    $lastCheckDate = $escort_model->getEscortLastCheckDate($id);

                    $result = $client->call('CopyListener.takeCopyOfEscortKeywordsMessengers', array($id, $keywords, $messengers, $lastCheckDate));
                    if ($result->result == 'success') {
                        $cli->out($result->result . ', ' . $result->message . ', ' . $result->escort_id);
                    } else {
                        $cli->out($result->result . ', ' . $result->message . ' ');
                    }
                }
                $cli->out('done');
                exit(1);
            } catch (Exception $e) {
                $cli->out($cli->out("[fail] {$e->getMessage()}"));
                Cubix_Email::send('vardanina@gmail.com', 'Keywords copy failed', $e->__toString());
                exit(1);
            }
        }
    }


    public function copyStatusesAction()
    {
        set_time_limit(0);
        ini_set('memory_limit', '1024M');
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $cli = new Cubix_Cli();
        $pid_file = '/var/run/merge-statuses-' . Cubix_Application::getId() . '.pid';
        $cli->setPidFile($pid_file);

        if ($cli->isRunning()) {
            $cli->out('Action is already running... exitting...');
            exit(1);
        }
        $db = Zend_Registry::get('db');


        if ($this->_request->isPost() && IS_CLI != 1) {
            $data = new Cubix_Form_Data($this->_request);

            $data->setFields(array(
                'escort_id' => 'int',
                'status' => 'int',
            ));

            $data = $data->getData();
            $ed_escort_id = $db->fetchOne("SELECT escort_id FROM copied_escorts WHERE orig_app_id = ".APP_EG_CO_UK." AND orig_escort_id = " . $data['escort_id']);
            if ($ed_escort_id) {
                $first_time_login = $db->fetchOne("SELECT true FROM users u INNER JOIN escorts e on e.user_id = u.id WHERE u.last_login_date IS NOT NULL AND e.id = " . $ed_escort_id);
                $statuses = array('Active' => ESCORT_STATUS_ACTIVE,'Owner Disabled' => ESCORT_STATUS_OWNER_DISABLED, 'Admin disabled' => ESCORT_STATUS_ADMIN_DISABLED,'inactive' => ESCORT_STATUS_INACTIVE);
                if (!$first_time_login) {
                    $old_status = $db->fetchRow("SELECT status FROM escorts WHERE id = " . $ed_escort_id);
                    $old = '';
                    $new = '';
                    foreach ($statuses as $title => $val)
                    {
                        if ($old_status->status == $val)
                            $old = $title;

                        if ($data['status'] == $val)
                            $new = $title;

                    }

                    if($old_status->status != $data['status'])
                    {
                        try {
                            $db->update('escorts', array('status' => $data['status']),$db->quoteInto('id = ?', $ed_escort_id));
                            $event = Cubix_SyncNotifier::EVENT_ESCORT_STATUS_UPDATED;
                            Cubix_SyncNotifier::notify($ed_escort_id, $event, array('old' => $old, 'new' => $new));
                            echo json_encode(array('status' => 'success', 'message' => 'Success', 'escort_id' => $ed_escort_id));return;
                        }catch (Exception $e)
                        {
                            echo json_encode(array('status' => 'error', 'message' => $e->getMessage()));return;
                        }
                    }else{
                        echo json_encode(array('status' => 'success', 'message' => 'Same status','escort_id' => $ed_escort_id));return;
                    }
                }
            } else {
                echo json_encode(array('status' => 'success', 'message' => 'No such escort found'));return;
            }
        } else {
            try {
                $client = new Cubix_EscortCopier_Client(APP_ED, true);
                $ids = $db->fetchAll("SELECT
                                            e.id AS escort_id,
                                            e.`status` 
                                        FROM
                                            escorts_projects_sync_log epsl
                                            INNER JOIN escorts e ON e.id = epsl.escort_id
                                            INNER JOIN journal j ON j.escort_id = e.id 
                                        WHERE
                                            epsl.sync_app_id = ".APP_ED." 
                                            AND j.id IN (SELECT MAX(id) FROM journal WHERE escort_id = e.id AND j.event_type = ".Cubix_SyncNotifier::EVENT_ESCORT_STATUS_UPDATED." )
                                            AND UNIX_TIMESTAMP( j.date ) > UNIX_TIMESTAMP(DATE_SUB( NOW(), INTERVAL 2 DAY )) AND (e.status & ".ESCORT_STATUS_OWNER_DISABLED." OR e.status &".ESCORT_STATUS_INACTIVE." OR e.status & ".ESCORT_STATUS_ACTIVE." OR e.status & ".ESCORT_STATUS_ADMIN_DISABLED.") AND ~e.status & ".ESCORT_STATUS_NO_ENOUGH_PHOTOS ." AND ~e.status & ". ESCORT_STATUS_PROFILE_CHANGED);

                foreach ($ids as $escort) {
                    $result = $client->call('CopyListener.takeCopyOfEscortStatuses', array( $escort->escort_id, $escort->status ));
                    if ($result->result == 'success') {
                        $cli->out($result->result . ', ' . $result->message . ', ' . $result->escort_id);
                    } else {
                        $cli->out($result->result . ', ' . $result->message . ' ');
                    }
                }
                $cli->out('done');
                exit(1);
            } catch (Exception $e) {
                $cli->out($cli->out("[fail] {$e->getMessage()}"));
                Cubix_Email::send('vardanina@gmail.com', 'Statuses copy failed', $e->__toString());
                exit(1);
            }
        }
    }

    public function changeBackendUserStatusAction()
    {
        set_time_limit(0);
        ini_set('memory_limit', '1024M');
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $cli = new Cubix_Cli();
        $pid_file = '/var/run/check-backend-statuses-' . Cubix_Application::getId() . '.pid';
        $cli->setPidFile($pid_file);

        if ($cli->isRunning()) {
            $cli->out('Action is already running... exitting...');
            exit(1);
        }
        $db = Zend_Registry::get('db');

        try {
            $activeBoUsers =  $db->fetchAll("SELECT
                                                bus.sales_id as id
                                            FROM
                                                backend_users_sessions bus 
                                            WHERE
                                                bus.last_refresh_time < ( NOW() - INTERVAL 30 MINUTE ) 
                                                AND DATE( bus.start_time ) = CURDATE() 
                                                AND bus.id IN ( SELECT MAX( id ) FROM backend_users_sessions WHERE sales_id = bus.sales_id ) 
                                            GROUP BY
                                                id DESC");
           
            if (count($activeBoUsers))
            {
                foreach ($activeBoUsers as $bu_user)
                {
                    $this->_db->update('backend_users',   array('is_online' => 0), $this->_db->quoteInto('id = ?', $bu_user->id));
                }
            }
            $cli->out('done');
            exit(1);
        } catch (Exception $e) {
            $cli->out($cli->out("[fail] {$e->getMessage()}"));
            Cubix_Email::send('vardaninan@gmail.com', 'Statuses offline failed', $e->__toString());
            exit(1);
        }
    }

    public function changeExpiredEscortsStatusesAction()
    {
        set_time_limit(0);
        ini_set('memory_limit', '1024M');
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $cli = new Cubix_Cli();
        $pid_file = '/var/run/check-backend-statuses-' . Cubix_Application::getId() . '.pid';
        $cli->setPidFile($pid_file);

        if ($cli->isRunning()) {
            $cli->out('Action is already running... exitting...');
            exit(1);
        }
        try {
            $sql = 'SELECT
                            e.id 
                        FROM
                            escorts e
                            INNER JOIN users u ON e.user_id = u.id 
                        WHERE
                            u.expiration_date IS NOT NULL 
                            AND UNIX_TIMESTAMP( u.expiration_date ) < UNIX_TIMESTAMP(
                            NOW()) 
                            AND (e.last_hand_verification_date IS NULL OR u.last_login_date IS NULL)
                            AND ~ e.status & ' . Cubix_EscortStatus::ESCORT_STATUS_INACTIVE;

            $escortsIdsToAffect = $this->_db->fetchCol($sql);
            if (count($escortsIdsToAffect))
            {
                foreach ($escortsIdsToAffect as $escort_id)
                {
                    $status = new Cubix_EscortStatus($escort_id);
                    $status->setStatus(Cubix_EscortStatus::ESCORT_STATUS_INACTIVE);
                    $status->save();
                    $cli->out('Escort ID: '. $escort_id .' status become Inactive after expiration date.');
                    Cubix_Email::send('vardaninan@gmail.com',ucfirst(Cubix_Application::getId()).' Escort ID:'. $escort_id .' status become Inactive after expiration date.', ucfirst(Cubix_Application::getId()).' Escort ID:'. $escort_id .' status become Inactive after expiration date.', true);
                }
            }else{
                $cli->out('0 Afecter escorts');
            }
            $cli->out('done');
            exit(1);
        }catch (Exception $e) {
            $cli->out($cli->out("[fail] {$e->getMessage()}"));
            Cubix_Email::send('vardaninan@gmail.com', 'Change Expired Escorts Statuses failed', $e->__toString());
            exit(1);
        }
    }

    public function importEdCsvAction()
    {
        set_time_limit(0);
        ini_set('memory_limit', '1024M');
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $cli = new Cubix_Cli();
        $pid_file = '/var/run/import-ed-csv' . Cubix_Application::getId() . '.pid';
        $cli->setPidFile($pid_file);

        if ($cli->isRunning()) {
            $cli->out('Action is already running... exitting...');
            exit(1);
        }
        $row = 1;
        if (($handle = fopen("./transfers/import.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                echo "<p> $num fields in line $row: <br /></p>\n";
                $row++;
                for ($c=0; $c < $num; $c++) {
                    echo $data[$c] . "<br />\n";
                    if (is_numeric($data[$c]) && strlen($data[$c]) > 3) {
                        $escort_id = $data[$c];
                        $comment = $data[$c + 1];

                        $status = new Cubix_EscortStatus($escort_id);
                        $status->setStatus(Cubix_EscortStatus::ESCORT_STATUS_INACTIVE);
                        $status->save();

                        $this->_db->insert('escort_comments', array(
                            'escort_id' => $escort_id,
                            'comment' => $comment,
                            'date' => new Zend_Db_Expr('NOW()'),
                        ));
                        $cli->out('Escort ID: ' . $escort_id . ', status Changed to Inactive and inserted comment "' . $comment . '",');
                    }
                }
            }
            fclose($handle);
        }
    }

    public function crowlSlummiAction()
    {
        set_time_limit(0);
        ini_set('memory_limit', '1024M');
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $cli = new Cubix_Cli();
        $pid_file = '/var/run/import-crowl' . Cubix_Application::getId() . '.pid';
        $cli->setPidFile($pid_file);

        if ($cli->isRunning()) {
            $cli->out('Action is already running... exitting...');
            exit(1);
        }
        $services_compare = array(
            'Beso negro'=> 31,
            'Beso blanco' => 33,
            'Eyaculación femenina' => 65,
            'Fetichismo' => 39,
            'Garganta profunda' => 14,
            'Lluvia dorada' => 41,
            'Strap on' => 49,
            'Besos con lengua' => 6,
            'Eyaculación cuerpo' => 52,
            'Eyaculación facial' => 4,
            'Masajes eróticos' => 53,
            'Trato de novia' => 7,
            'Actriz porno' => 28,
            'Sexo anal' => 2,
            'Fantasías y disfraces' => 46,
            'Dúplex' => 23,
            'Sado duro' => 36
        );

        $countries = array(34 => 'spain');
        foreach ($countries as $country_key => $country) {
            $country_iso = substr($country, 0, 2);
            $csv_file = './importer-csv/slummi.csv';
            if (file_exists($csv_file)) {
                $data = $fields = array();
                $i = 0;
                $handle = @fopen($csv_file, "r");
                if ($handle) {
                    $cli->out('Importing Country:' . $country);
                    while (($row = fgetcsv($handle, 4096)) !== false) {
                        if (empty($fields)) {
                            $fields = $row;
                            continue;
                        }
                        foreach ($row as $k => $value) {
                            $data[$i][$fields[$k]] = $value;
                        }
                        $i++;
                    }
                    if (!feof($handle)) {
                        echo "Error: unexpected fgets() fail\n";
                    }
                    fclose($handle);

                    foreach ($data as $escort_data) {
                        $single_escort = array();
                        $single_escort['country_title'] = $country;
                        $single_escort['web_site'] = 'https://www.slummi.com';
                        if ($escort_data['phone1'] && $escort_data['phone1'] != "null")
                        {
                            $model = new Model_Importer();
                            $trimmed_phone = str_replace("\xc2\xa0", '', $escort_data['phone1']);
                            $cli->out('Importing Escort with phone:' . $trimmed_phone);
                            if( preg_match('/\+61/', $trimmed_phone) )
                            {
                                $trimmed_phone = str_replace('+61', '', $trimmed_phone);
                            }elseif( preg_match("/\+{$country_key}/", $trimmed_phone) )
                            {
                                $trimmed_phone = str_replace("+{$country_key}", '', $trimmed_phone);
                            }elseif (substr( $trimmed_phone, 0, 1 ) === "0"){
                                $trimmed_phone = ltrim($trimmed_phone, '0');
                            }elseif ( preg_match('/\+1/', $trimmed_phone) )
                            {
                                $trimmed_phone = str_replace('+1', '', $trimmed_phone);
                            }elseif ( preg_match('/\+44/', $trimmed_phone) )
                            {
                                $trimmed_phone = str_replace('+44', '', $trimmed_phone);
                            }
                            $trimmed_phone = str_replace(' ','',$trimmed_phone);
                            $escort_data['phone'] = $trimmed_phone;
                            if (!$model->existsByPhone($escort_data['phone']) && !$model->existsImportByPhone($escort_data['phone']) ) {
                                $cli->out('Crowling escort ' . $escort_data['web-scraper-start-url']);
                                if ($escort_data['gender']) {
                                    switch ($escort_data['gender']) {
                                        case "Male":
                                            $gender = 2;
                                            break;
                                        case "Transe":
                                            $gender = 3;
                                            break;
                                        default:
                                            $gender = 1;
                                    }
                                    $single_escort['gender'] = $gender;
                                }else{
                                    $single_escort['gender'] = 1;
                                }

                                $single_escort['online'] = '';
                                if ($escort_data['online'] && $escort_data['online'] != "null")
                                    $single_escort['online'] = strtotime($escort_data['online']);

                                if ($escort_data['website'] && $escort_data['website'] != "null")
                                    $single_escort['website'] = $escort_data['website'];

                                $single_escort['phone'] = $escort_data['phone'];
                                $single_escort['url'] = $escort_data['escort-href'];
                                $single_escort['name'] = strtok($escort_data['name3'], ' ');
                                $single_escort['city'] = explode('/',$escort_data['escort-href'])[4];
                                $single_escort['country'] = 'es';
                                if( $escort_data['services'] && $escort_data['services'] != "null" )
                                {
                                    $services_arr = array();
                                    $services = explode(',',str_replace(']','',str_replace('[','',$escort_data['services'])));
                                    foreach ($services_compare as $srv => $k)
                                    {
                                        foreach ($services as $service)
                                        {
                                            if (preg_match("/{$srv}/",str_replace('"}','',str_replace('{"services":"','',$service))))
                                            {
                                                $services_arr[] = $k;
                                            }
                                        }
                                    }
                                    $single_escort['services'] = $services_arr;
                                }
                                $single_escort['age'] = $escort_data['age']? explode(' ', $escort_data['age'])[0] : '20';

                                if ($escort_data['weight'] && $escort_data['weight'] != "null")
                                    $single_escort['weight'] = $escort_data['weight'];
                                if ($escort_data['height'] && $escort_data['height'] != "null")
                                    $single_escort['height'] = $escort_data['height'];
                                if ($escort_data['height'] && $escort_data['height'] != "null")
                                    $single_escort['height'] = $escort_data['height'];
                                if ($escort_data['about'] && $escort_data['about'] != "null")
                                {
                                    $regexEmoticons = '/[\x{1F6}-\x{1F999}]|[\x{1F9}-\x{1F9FF}]|[\x{1FA}-\x{1FAFF}]/u';
                                    $description_matches = preg_replace($regexEmoticons, '', $escort_data['about']);
                                    $single_escort['about_me'] = $description_matches;
                                }
                                $has_enough_photos = false;
                                if ($escort_data['photos'] && $escort_data['photos'] != "null" && strlen($escort_data['photos']) > 10)
                                {
                                    $photos_array = explode('},',$escort_data['photos']);
                                    $photos = array();

                                    foreach ($photos_array as $key => $photo) {
                                        if($key < 10)
                                        {
                                            $current_photo = str_replace('{photos:,photos-src:','',str_replace('400x592','1000x700',str_replace('100x100','1000x700',str_replace('"','',str_replace('[','',str_replace('{"Photos":"","Photos-src":"', '', str_replace('}]','',$photo)))))));
                                            if(!preg_match('/\.png/',$current_photo))
                                                $photos[] = $current_photo;
                                        }
                                    }
                                    if ( preg_match('#(test\.sceon\.am|\.test)$#', $_SERVER['SERVER_NAME']) > 0 ) {
                                        if ( count($photos) > 2 ) {
                                            $has_enough_photos = true;
                                            if (!is_dir('./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'slum/')) {
                                                mkdir('./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'slum/');
                                            }
                                            $phone = $single_escort['phone'];

                                            if (is_dir('./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'slum/' . $phone . '/')) {
                                                rmdir('./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'slum/' . $phone . '/');
                                            }
                                            mkdir('./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'slum/' . $phone . '/');
                                            exec ('find ./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'slum/' . $phone . '/ -type f -exec chmod 0777 {} +');
                                            foreach ($photos as $photo_key => $images_match) {
                                                $image_link = $images_match;
                                                $image = file_get_contents($image_link);
                                                $single_escort['images'][$photo_key] = $phone . '-' . rand(10000, 99999) . '.jpg';
                                                $single_escort['image_url'] = './crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'slum/' . $phone . '/' . $single_escort['images'][$photo_key] ;
                                                $single_escort['image_urls'][$photo_key] = './crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'slum/' . $phone . '/' . $single_escort['images'][$photo_key] ;
                                                file_put_contents($single_escort['image_url'], $image);
                                                $src = './img/logo-' . Cubix_Application::getId() . 'eu.png';
                                                $ext = preg_match('/jpg/',$single_escort['image_url'])? 'jpg' : 'png';
                                                self::eurogirlsWatermarkImage($single_escort['image_url'], $src, $single_escort['image_url']);
                                            }
                                        }
                                    }else{
                                        if ( count($photos) > 2 ) {
                                            $has_enough_photos = true;
                                            if (!is_dir('/data/edir-crwl-images-slum')) {
                                                mkdir('/data/edir-crwl-images-slum');
                                            }
                                            $phone = $single_escort['phone'];

                                            if (is_dir('/data/edir-crwl-images-slum/' . $phone . '/')) {
                                                rmdir('/data/edir-crwl-images-slum/' . $phone . '/');
                                            }
                                            mkdir('/data/edir-crwl-images-slum/' . $phone . '/');
                                            exec ('find /data/edir-crwl-images-slum/' . $phone . '/ -type f -exec chmod 0777 {} +');
                                            foreach ($photos as $photo_key => $images_match) {
                                                $image_link = $images_match;
                                                $image = file_get_contents($image_link);
                                                $single_escort['images'][$photo_key] = $phone . '-' . rand(10000, 99999) . '.jpg';
                                                $single_escort['image_url'] = '/data/edir-crwl-images-slum/' . $phone . '/' . $single_escort['images'][$photo_key] ;
                                                $single_escort['image_urls'][$photo_key] = '/data/edir-crwl-images-slum/' . $phone . '/' . $single_escort['images'][$photo_key] ;
                                                file_put_contents($single_escort['image_url'], $image);
                                                $src = './img/logo-' . Cubix_Application::getId() . 'eu.png';
                                                self::eurogirlsWatermarkImage($single_escort['image_url'], $src, $single_escort['image_url']);
                                            }
                                        }
                                    }

                                }
                                $escort_model = new Model_Importer();

                                if( $has_enough_photos )
                                {
                                    $escort_model->save($single_escort);
                                    $cli->out('Importing new escort data.');
                                    $cli->out('..............................................................................');
                                }else{
                                    $cli->out('no enough photos for import');
                                    $cli->out('..............................................................................');
                                }
                            }else{
                                $cli->out('Phone number ' . $escort_data['phone'] .'already exist');
                                $cli->out('..............................................................................');
                            }
                        }
                    }
                }

            }
        }
    }

    public function crowlEuroGirlsAction()
    {
        set_time_limit(0);
        ini_set('memory_limit', '1024M');
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $cli = new Cubix_Cli();
        $pid_file = '/var/run/import-crowl' . Cubix_Application::getId() . '.pid';
        $cli->setPidFile($pid_file);

        if ($cli->isRunning()) {
            $cli->out('Action is already running... exitting...');
            exit(1);
        }

        $countries = array(61 => 'australia', 32 => 'belgium',357 => 'cyprus',420 => 'czech-republic',45 => 'denmark',352 => 'luxembourg',36 => 'hungary',31 => 'netherlands',47 => 'norway',48 => 'poland',351 => 'portugal',34 => 'spain',421 => 'slovakia',46 => 'sweden',41 => 'switzerland',380 => 'ukraine',973 => 'bahrain',1 => 'canada',62 => 'indonesia',81 => 'japan',63 => 'philippines',974 => 'qatar',966 => 'saudi-arabia',968 =>  'oman', 82 => 'south-korea',65 => 'singapore',971 => 'uae');
        foreach ($countries as $country_key => $country) {
            $country_iso = substr($country, 0, 2);
            $csv_file = './importer-csv/' . $country . '.csv';
            if (file_exists($csv_file)) {
                $data = $fields = array();
                $i = 0;
                $handle = @fopen($csv_file, "r");
                if ($handle) {
                    $cli->out('Importing Country:' . $country);
                    while (($row = fgetcsv($handle, 4096)) !== false) {
                        if (empty($fields)) {
                            $fields = $row;
                            continue;
                        }
                        foreach ($row as $k => $value) {
                            $data[$i][$fields[$k]] = $value;
                        }
                        $i++;
                    }
                    if (!feof($handle)) {
                        echo "Error: unexpected fgets() fail\n";
                    }
                    fclose($handle);

                    foreach ($data as $escort_data) {
                        $single_escort = array();
                        $single_escort['country_title'] = $country;
                        $single_escort['web_site'] = 'https://www.eurogirlsescort.com';
                        if ($escort_data['phone'] && $escort_data['phone'] != "null")
                        {
                            $model = new Model_Importer();
                            $trimmed_phone = str_replace("\xc2\xa0", '', $escort_data['phone']);
                            $cli->out('Importing Escort with phone:' . $trimmed_phone);
                            if( preg_match('/\+61/', $trimmed_phone) )
                            {
                                $trimmed_phone = str_replace('+61', '', $trimmed_phone);
                            }elseif( preg_match("/\+{$country_key}/", $trimmed_phone) )
                            {
                                $trimmed_phone = str_replace("+{$country_key}", '', $trimmed_phone);
                            }elseif (substr( $trimmed_phone, 0, 1 ) === "0"){
                                $trimmed_phone = ltrim($trimmed_phone, '0');
                            }elseif ( preg_match('/\+1/', $trimmed_phone) )
                            {
                                $trimmed_phone = str_replace('+1', '', $trimmed_phone);
                            }elseif ( preg_match('/\+44/', $trimmed_phone) )
                            {
                                $trimmed_phone = str_replace('+44', '', $trimmed_phone);
                            }

                            $escort_data['phone'] = $trimmed_phone;
                            if (!$model->existsByPhone($escort_data['phone']) && !$model->existsImportByPhone($escort_data['phone']) ) {
                                $cli->out('Crowling escort ' . $escort_data['web-scraper-start-url']);
                                if ($escort_data['gender']) {
                                    switch ($escort_data['gender']) {
                                        case "Male":
                                            $gender = 2;
                                            break;
                                        case "Transe":
                                            $gender = 3;
                                            break;
                                        default:
                                            $gender = 1;
                                    }
                                    $single_escort['gender'] = $gender;
                                }
                                if ($escort_data['website'] && $escort_data['website'] != "null")
                                    $single_escort['website'] = $escort_data['website'];

                                $single_escort['phone'] = $escort_data['phone'];
                                $single_escort['url'] = $escort_data['escort-href'];
                                $single_escort['name'] = str_replace(',
independent','',strtok($escort_data['name'], ' '));
                                $single_escort['name'] = str_replace(',
agency','',$single_escort['name']);
                                $location = explode('/', $escort_data['location']);
                                if (count($location) > 1) {
                                    $single_escort['city'] = trim($location[0]);
                                    if ($country == 'danmark')
                                    {
                                        $single_escort['country'] = 'dk';
                                    }elseif($country == 'netherlands'){
                                        $single_escort['country'] = 'nl';
                                    }elseif($country == 'poland'){
                                        $single_escort['country'] = 'pl';
                                    }elseif($country == 'portugal'){
                                        $single_escort['country'] = 'pt';
                                    }elseif($country == 'spain'){
                                        $single_escort['country'] = 'es';
                                    }elseif($country == 'slovakia'){
                                        $single_escort['country'] = 'sk';
                                    }elseif($country == 'switzerland'){
                                        $single_escort['country'] = 'ch';
                                    }elseif($country == 'sweden'){
                                        $single_escort['country'] = 'se';
                                    }elseif($country == 'ukraine'){
                                        $single_escort['country'] = 'ua';
                                    }elseif($country == 'bahrain'){
                                        $single_escort['country'] = 'bh';
                                    }elseif($country == 'indonesia'){
                                        $single_escort['country'] = 'id';
                                    }elseif($country == 'japan'){
                                        $single_escort['country'] = 'jp';
                                    }elseif($country == 'south-korea'){
                                        $single_escort['country'] = 'kr';
                                    }elseif($country == 'singapore'){
                                        $single_escort['country'] = 'sg';
                                    }elseif($country == 'uae'){
                                        $single_escort['country'] = 'ae';
                                    }else{
                                        $single_escort['country'] = trim($country_iso);
                                    }

                                }
                                if( $escort_data['services'] )
                                {
                                    $services = explode(',',str_replace(']','',str_replace('[','',$escort_data['services'])));
                                    $services_arr = array();
                                    foreach ($services as $service) {
                                        $trimmed_service = str_replace('{"services":"','',$service);
                                        $trimmed_service = str_replace('"}','',$trimmed_service);
                                        $services_arr[] = $trimmed_service;
                                    }
                                    $single_escort['services'] = $services_arr;
                                }
                                if ($escort_data['age'] && $escort_data['age'] != "null")
                                    $single_escort['age'] = $escort_data['age'];
                                if ($escort_data['weight'] && $escort_data['weight'] != "null")
                                    $single_escort['weight'] = $escort_data['weight'];
                                if ($escort_data['height'] && $escort_data['height'] != "null")
                                    $single_escort['height'] = $escort_data['height'];
                                if ($escort_data['height'] && $escort_data['height'] != "null")
                                    $single_escort['height'] = $escort_data['height'];
                                if ($escort_data['about'] && $escort_data['about'] != "null")
                                {
                                    $regexEmoticons = '/[\x{1F6}-\x{1F999}]|[\x{1F9}-\x{1F9FF}]|[\x{1FA}-\x{1FAFF}]/u';
                                    $description_matches = preg_replace($regexEmoticons, '', $escort_data['about']);
                                    $single_escort['about_me'] = $description_matches;
                                }
                                $has_enough_photos = false;
                                if ($escort_data['Photos'] && $escort_data['Photos'] != "null")
                                {
                                    $photos_array = explode('},',$escort_data['Photos']);
                                    $photos = array();
                                    foreach ($photos_array as $key => $photo) {
                                        if($key < 10)
                                        {
                                            $current_photo = str_replace('400x592','1000x700',str_replace('100x100','1000x700',str_replace('"','',str_replace('[','',str_replace('{"Photos":"","Photos-src":"', '', str_replace('}]','',$photo))))));
                                            if(!preg_match('/\.png/',$current_photo))
                                                $photos[] = $current_photo;
                                        }
                                    }
                                    if ( preg_match('#(test\.sceon\.am|\.test)$#', $_SERVER['SERVER_NAME']) > 0 ) {
                                        if ( count($photos) > 2 ) {
                                            $has_enough_photos = true;
                                            if (!is_dir('./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/')) {
                                                mkdir('./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/');
                                            }
                                            $phone = $single_escort['phone'];

                                            if (is_dir('./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/' . $phone . '/')) {
                                                rmdir('./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/' . $phone . '/');
                                            }
                                            mkdir('./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/' . $phone . '/');
                                            exec ('find ./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/' . $phone . '/ -type f -exec chmod 0777 {} +');
                                            foreach ($photos as $photo_key => $images_match) {
                                                $image_link = $images_match;
                                                $image = file_get_contents($image_link);
                                                $single_escort['images'][$photo_key] = $phone . '-' . rand(10000, 99999) . '.jpg';
                                                $single_escort['image_url'] = './crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/' . $phone . '/' . $single_escort['images'][$photo_key] ;
                                                $single_escort['image_urls'][$photo_key] = './crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/' . $phone . '/' . $single_escort['images'][$photo_key] ;
                                                file_put_contents($single_escort['image_url'], $image);
                                                $src = './img/logo-' . Cubix_Application::getId() . 'eu.png';
                                                $ext = preg_match('/jpg/',$single_escort['image_url'])? 'jpg' : 'png';
                                                self::eurogirlsWatermarkImage($single_escort['image_url'], $src, $single_escort['image_url']);
                                            }
                                        }
                                    }else{
                                        if ( count($photos) > 2 ) {
                                            $has_enough_photos = true;
                                            if (!is_dir('/data/edir-crwl-images')) {
                                                mkdir('/data/edir-crwl-images');
                                            }
                                            $phone = $single_escort['phone'];

                                            if (is_dir('/data/edir-crwl-images/' . $phone . '/')) {
                                                rmdir('/data/edir-crwl-images/' . $phone . '/');
                                            }
                                            mkdir('/data/edir-crwl-images/' . $phone . '/');
                                            exec ('find /data/edir-crwl-images/' . $phone . '/ -type f -exec chmod 0777 {} +');
                                            foreach ($photos as $photo_key => $images_match) {
                                                $image_link = $images_match;
                                                $image = file_get_contents($image_link);
                                                $single_escort['images'][$photo_key] = $phone . '-' . rand(10000, 99999) . '.jpg';
                                                $single_escort['image_url'] = '/data/edir-crwl-images/' . $phone . '/' . $single_escort['images'][$photo_key] ;
                                                $single_escort['image_urls'][$photo_key] = '/data/edir-crwl-images/' . $phone . '/' . $single_escort['images'][$photo_key] ;
                                                file_put_contents($single_escort['image_url'], $image);
                                                $src = './img/logo-' . Cubix_Application::getId() . 'eu.png';
                                                self::eurogirlsWatermarkImage($single_escort['image_url'], $src, $single_escort['image_url']);
                                            }
                                        }
                                    }

                                }
                                $escort_model = new Model_Importer();

                                if( $has_enough_photos )
                                {
                                    $escort_model->save($single_escort);
                                    $cli->out('Importing new escort data.');
                                    $cli->out('..............................................................................');
                                }else{
                                    $cli->out('no enough photos for import');
                                    $cli->out('..............................................................................');
                                }
                            }else{
                                $cli->out('Phone number ' . $escort_data['phone'] .'already exist');
                                $cli->out('..............................................................................');
                            }
                        }
                    }
                }

            }
        }
    }

    public function crowlEuroGirlsHomeAction()
    {
        set_time_limit(0);
        ini_set('memory_limit', '1024M');
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $cli = new Cubix_Cli();
        $pid_file = '/var/run/import-crowl' . Cubix_Application::getId() . '.pid';
        $cli->setPidFile($pid_file);

        if ($cli->isRunning()) {
            $cli->out('Action is already running... exitting...');
            exit(1);
        }

            $csv_file = './importer-csv/eurogirls_homepage.csv';
            if (file_exists($csv_file)) {
                $data = $fields = array();
                $i = 0;
                $handle = @fopen($csv_file, "r");
                if ($handle) {
                    $cli->out('Importing Homepage');
                    while (($row = fgetcsv($handle, 4096)) !== false) {
                        if (empty($fields)) {
                            $fields = $row;
                            continue;
                        }
                        foreach ($row as $k => $value) {
                            $data[$i][$fields[$k]] = $value;
                        }
                        $i++;
                    }
                    if (!feof($handle)) {
                        echo "Error: unexpected fgets() fail\n";
                    }
                    fclose($handle);

                    foreach ($data as $escort_data) {
                        $single_escort = array();
                        $location = explode('/',$escort_data['location']);
                        $single_escort['country_title'] = ltrim($location[1]);
                        $single_escort['web_site'] = 'https://www.eurogirlsescort.com';
                        if ($escort_data['phone'] && $escort_data['phone'] != "null")
                        {
                            $model = new Model_Importer();
                            $trimmed_phone = str_replace("\xc2\xa0", '', $escort_data['phone']);
                            $cli->out('Importing Escort with phone:' . $trimmed_phone);
                            $country_data = $model->getCountryDataByTitle(strtolower($single_escort['country_title']));

                            if( preg_match("/\+{$country_data->phone_prefix}/", $trimmed_phone) )
                            {
                                $trimmed_phone = str_replace("+{$country_data->phone_prefix}", '', $trimmed_phone);
                            }

                            $escort_data['phone'] = $trimmed_phone;
                            if (preg_match('/\+/' , $trimmed_phone))
                            {
                                preg_match('/flag\-icon\-([^"]+)/',$escort_data['prefix'],$prefix_matche);
                                $prefix = $model->getPrefixDataByIso($prefix_matche[1]);
                                $escort_data['phone'] = str_replace("+{$prefix}", '', $trimmed_phone);
                            }

                            if (!$model->existsByPhone($escort_data['phone']) && !$model->existsImportByPhone($escort_data['phone']) ) {
                                $cli->out('Crowling escort ' . $escort_data['web-scraper-start-url']);
                                if ($escort_data['gender']) {
                                    switch ($escort_data['gender']) {
                                        case "Male":
                                            $gender = 2;
                                            break;
                                        case "Transe":
                                            $gender = 3;
                                            break;
                                        default:
                                            $gender = 1;
                                    }
                                    $single_escort['gender'] = $gender;
                                }
                                if ($escort_data['website'] && $escort_data['website'] != "null")
                                    $single_escort['website'] = $escort_data['website'];

                                $single_escort['phone'] = $escort_data['phone'];
                                $single_escort['url'] = $escort_data['escort-href'];
                                $single_escort['name'] = str_replace(',
independent','',strtok($escort_data['name'], ' '));
                                $single_escort['name'] = str_replace(',
agency','',$single_escort['name']);
                                if (count($location) > 1) {
                                    $single_escort['city'] = trim($location[0]);
                                    $country = strtolower($single_escort['country_title']);
                                    $single_escort['country'] = $country_data->iso;
                                }
                                if( $escort_data['services'] )
                                {
                                    $services = explode(',',str_replace(']','',str_replace('[','',$escort_data['services'])));
                                    $services_arr = array();
                                    foreach ($services as $service) {
                                        $trimmed_service = str_replace('{"services":"','',$service);
                                        $trimmed_service = str_replace('"}','',$trimmed_service);
                                        $services_arr[] = $trimmed_service;
                                    }
                                    $single_escort['services'] = $services_arr;
                                }

                                if ($escort_data['age'] && $escort_data['age'] != "null")
                                    $single_escort['age'] = $escort_data['age'];
                                if ($escort_data['weight'] && $escort_data['weight'] != "null")
                                    $single_escort['weight'] = $escort_data['weight'];
                                if ($escort_data['height'] && $escort_data['height'] != "null")
                                    $single_escort['height'] = $escort_data['height'];
                                if ($escort_data['height'] && $escort_data['height'] != "null")
                                    $single_escort['height'] = $escort_data['height'];
                                if ($escort_data['about'] && $escort_data['about'] != "null")
                                {
                                    $regexEmoticons = '/[\x{1F6}-\x{1F999}]|[\x{1F9}-\x{1F9FF}]|[\x{1FA}-\x{1FAFF}]/u';
                                    $description_matches = preg_replace($regexEmoticons, '', $escort_data['about']);
                                    $single_escort['about_me'] = $description_matches;
                                }
                                $has_enough_photos = false;
                                if ($escort_data['Photos'] && $escort_data['Photos'] != "null")
                                {
                                    $photos_array = explode('},',$escort_data['Photos']);
                                    $photos = array();
                                    foreach ($photos_array as $key => $photo) {
                                        if($key < 10)
                                        {
                                            $current_photo = str_replace('400x592','1000x700',str_replace('100x100','1000x700',str_replace('"','',str_replace('[','',str_replace('{"Photos":"","Photos-src":"', '', str_replace('}]','',$photo))))));
                                            if(preg_match('/\.jpg/',$current_photo) && !preg_match('/base64/',$current_photo))
                                            {
                                                $photos[] = $current_photo;
                                            }
                                        }
                                    }
                                    if ( preg_match('#(test\.sceon\.am|\.test)$#', $_SERVER['SERVER_NAME']) > 0 ) {
                                        if ( count($photos) > 2 ) {
                                            $has_enough_photos = true;
                                            if (!is_dir('./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/home/')) {
                                                mkdir('./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/home/');
                                            }
                                            $phone = $single_escort['phone'];

                                            if (is_dir('./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/home/' . $phone . '/')) {
                                                rmdir('./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/home/' . $phone . '/');
                                            }
                                            mkdir('./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/home/' . $phone . '/');
                                            foreach ($photos as $photo_key => $images_match) {
                                                $image_link = $images_match;
                                                $image = file_get_contents($image_link);
                                                $single_escort['images'][$photo_key] = $phone . '-' . rand(10000, 99999) . '.jpg';
                                                $single_escort['image_url'] = './crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/home/' . $phone . '/' . $single_escort['images'][$photo_key] ;
                                                $single_escort['image_urls'][$photo_key] = './crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/home/' . $phone . '/' . $single_escort['images'][$photo_key] ;
                                                file_put_contents($single_escort['image_url'], $image);
                                                $src = './img/logo-' . Cubix_Application::getId() . 'eu.png';
                                                $ext = preg_match('/jpg/',$single_escort['image_url'])? 'jpg' : 'png';
                                                self::eurogirlsWatermarkImage($single_escort['image_url'], $src, $single_escort['image_url']);
                                            }
                                        }
                                    }else{
                                        if ( count($photos) > 2 ) {
                                            $has_enough_photos = true;
                                            if (!is_dir('/data/edir-crwl-images/home/')) {
                                                mkdir('/data/edir-crwl-images/home/');
                                            }
                                            $phone = $single_escort['phone'];

                                            if (is_dir('/data/edir-crwl-images/home/' . $phone . '/')) {
                                                rmdir('/data/edir-crwl-images/home/' . $phone . '/');
                                            }
                                            mkdir('/data/edir-crwl-images/home/' . $phone . '/');
                                            exec ('find /data/edir-crwl-images/home/' . $phone . '/ -type f -exec chmod 0777 {} +');
                                            foreach ($photos as $photo_key => $images_match) {
                                                $image_link = $images_match;
                                                $image = file_get_contents($image_link);
                                                $single_escort['images'][$photo_key] = $phone . '-' . rand(10000, 99999) . '.jpg';
                                                $single_escort['image_url'] = '/data/edir-crwl-images/home/' . $phone . '/' . $single_escort['images'][$photo_key] ;
                                                $single_escort['image_urls'][$photo_key] = '/data/edir-crwl-images/home/' . $phone . '/' . $single_escort['images'][$photo_key] ;
                                                file_put_contents($single_escort['image_url'], $image);
                                                $src = './img/logo-' . Cubix_Application::getId() . 'eu.png';
                                                self::eurogirlsWatermarkImage($single_escort['image_url'], $src, $single_escort['image_url']);
                                            }
                                        }
                                    }

                                    if( $has_enough_photos )
                                    {
                                        $escort_model = new Model_Importer();
                                        $escort_model->save($single_escort);
                                        $cli->out('Importing new escort data.');
                                        $cli->out('..............................................................................');
                                    }else{
                                        $cli->out('no enough photos for import');
                                        $cli->out('..............................................................................');
                                    }

                                }
                            }else{
                                $cli->out('Phone number ' . $escort_data['phone'] .'already exist');
                                $cli->out('..............................................................................');
                            }
                        }
                    }
                }
        }
    }

    public function crowlSex4uAction()
    {
        set_time_limit(0);
        ini_set('memory_limit', '1024M');
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $cli = new Cubix_Cli();
        $pid_file = '/var/run/import-crowl-sex4u' . Cubix_Application::getId() . '.pid';
        $cli->setPidFile($pid_file);

        if ($cli->isRunning()) {
            $cli->out('Action is already running... exitting...');
            exit(1);
        }

        $csv_file = './importer-csv/sex4u.csv';
        if (file_exists($csv_file)) {
            $data = $fields = array();
            $i = 0;
            $handle = @fopen($csv_file, "r");
            if ($handle) {
                $cli->out('Importing Homepage');
                while (($row = fgetcsv($handle, 4096)) !== false) {
                    if (empty($fields)) {
                        $fields = $row;
                        continue;
                    }
                    foreach ($row as $k => $value) {
                        $data[$i][$fields[$k]] = $value;
                    }
                    $i++;
                }
                if (!feof($handle)) {
                    echo "Error: unexpected fgets() fail\n";
                }
                fclose($handle);

                foreach ($data as $escort_data) {
                    $single_escort = array();
                    $single_escort['country_title'] = 'Switzerland';
                    $single_escort['web_site'] = 'https://sex4u.ch';
                    if ($escort_data['phone'] && $escort_data['phone'] != "null")
                    {
                        $model = new Model_Importer();
                        $trimmed_phone = str_replace(" ", '', $escort_data['phone']);

                        $cli->out('Importing Escort with phone:' . (int)$trimmed_phone);
                        $country_data = $model->getCountryDataByTitle(strtolower($single_escort['country_title']));

                        if( preg_match("/\+{$country_data->phone_prefix}/", $trimmed_phone) )
                        {
                            $trimmed_phone = str_replace("+{$country_data->phone_prefix}", '', $trimmed_phone);
                        }

                        $escort_data['phone'] = (int)$trimmed_phone;

                        if (!$model->existsByPhone($escort_data['phone']) && !$model->existsImportByPhone($escort_data['phone']) ) {
                            $cli->out('Crowling escort ' . $escort_data['web-scraper-start-url']);
                            if ($escort_data['gender']) {
                                switch ($escort_data['gender']) {
                                    case "Male":
                                        $gender = 2;
                                        break;
                                    case "Transe":
                                        $gender = 3;
                                        break;
                                    default:
                                        $gender = 1;
                                }
                                $single_escort['gender'] = $gender;
                            }
                            if ($escort_data['website'] && $escort_data['website'] != "null")
                                $single_escort['website'] = $escort_data['website'];

                            $single_escort['url'] = $escort_data['escort-href'];
                            if($escort_data['name'] && $escort_data['name'] != "null")
                            {
                                $single_escort['name'] = $escort_data['name'];
                            }elseif ($escort_data['name2'] && $escort_data['name2'] != "null")
                            {
                                $name_array = explode(' ',$escort_data['name2']);
                                if($name_array[1] == 'TS' || $name_array[0] == 'TS')
                                {
                                    $single_escort['name'] = $name_array[0] .' '. $name_array[1];
                                }else
                                {
                                    $single_escort['name'] = $name_array[0];
                                }
                            }

                            $single_escort['phone'] = $escort_data['phone'];
                            if( $escort_data['services'] )
                            {
                                $services = explode(',',str_replace(']','',str_replace('[','',$escort_data['services'])));
                                $services_arr = array();
                                foreach ($services as $service) {
                                    $trimmed_service = str_replace('{"services":"','',$service);
                                    $trimmed_service = str_replace('"}','',$trimmed_service);
                                    $services_arr[] = $trimmed_service;
                                }
                                $single_escort['services'] = $services_arr;
                            }

                            if ($escort_data['age'] && $escort_data['age'] != "null")
                                $single_escort['age'] = $escort_data['age'];
                            if ($escort_data['weight'] && $escort_data['weight'] != "null")
                                $single_escort['weight'] = $escort_data['weight'];
                            if ($escort_data['height'] && $escort_data['height'] != "null")
                                $single_escort['height'] = $escort_data['height'];
                            if ($escort_data['height'] && $escort_data['height'] != "null")
                                $single_escort['height'] = $escort_data['height'];
                            if ($escort_data['about'] && $escort_data['about'] != "null")
                            {
                                $preAbout = explode('},{',$escort_data['about']);
                                $aboutText = '';
                                foreach ($preAbout as $about)
                                {
                                    $aboutText .= str_replace('"}]','',str_replace('[{','',str_replace('"about":"','',$about))).',<br>';
                                }
                                $regexEmoticons = '/[\x{1F6}-\x{1F999}]|[\x{1F9}-\x{1F9FF}]|[\x{1FA}-\x{1FAFF}]/u';
                                $description_matches = preg_replace($regexEmoticons, '', $aboutText);
                                $description_matches = ltrim(trim(str_replace('"','',$description_matches)));
                                $about = preg_replace('/<[^>]*>/', '', $description_matches);
                                $about = preg_replace("/[\n\r]/", "", $about);

                                $about = preg_replace('/\s\s+/', ' ', $about);
                                $about = str_replace('*','',$about);
                                $single_escort['about_me'] = $about;
                            }
                            $has_enough_photos = false;
                            if ($escort_data['Photos'] && $escort_data['Photos'] != "null")
                            {
                                $photos_array = explode('},',$escort_data['Photos']);
                                $photos = array();

                                foreach ($photos_array as $key => $photo) {
                                    if($key < 10)
                                    {
                                        $current_photo = str_replace('400x592','1000x700',str_replace('100x100','1000x700',str_replace('"','',str_replace('[','',str_replace('{"Photos":"","Photos-src":"', '', str_replace('}]','',$photo))))));
                                        if(preg_match('/\.jpg/',$current_photo) && !preg_match('/base64/',$current_photo))
                                        {
                                            $photos[] = $current_photo;
                                        }
                                    }
                                }
                                if ( preg_match('#(test\.sceon\.am|\.test)$#', $_SERVER['SERVER_NAME']) > 0 ) {
                                    if ( count($photos) > 2 ) {
                                        $has_enough_photos = true;
                                        if (!is_dir('./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/sex4u/')) {
                                            mkdir('./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/sex4u/');
                                        }
                                        $phone = $single_escort['phone'];

                                        if (is_dir('./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/sex4u/' . $phone . '/')) {
                                            rmdir('./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/sex4u/' . $phone . '/');
                                        }
                                        mkdir('./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/sex4u/' . $phone . '/');
                                        foreach ($photos as $photo_key => $images_match) {
                                            $image_link = str_replace('cut_','',$images_match);
                                            $image = file_get_contents($image_link);
                                            $single_escort['images'][$photo_key] = $phone . '-' . rand(10000, 99999) . '.jpg';
                                            $single_escort['image_url'] = './crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/sex4u/' . $phone . '/' . $single_escort['images'][$photo_key] ;
                                            $single_escort['image_urls'][$photo_key] = './crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/sex4u/' . $phone . '/' . $single_escort['images'][$photo_key] ;
                                            file_put_contents($single_escort['image_url'], $image);
                                            $src = './img/logo-' . Cubix_Application::getId() . '.png';
                                            $ext = preg_match('/jpg/',$single_escort['image_url'])? 'jpg' : 'png';
                                            self::eurogirlsWatermarkImage($single_escort['image_url'], $src, $single_escort['image_url']);
                                        }
                                    }
                                }else{
                                    if ( count($photos) > 2 ) {
                                        $has_enough_photos = true;
                                        if (!is_dir('./crwl-images/sex4u/')) {
                                            mkdir('./crwl-images/sex4u/');
                                        }
                                        $phone = $single_escort['phone'];

                                        if (is_dir('./crwl-images/sex4u/' . $phone . '/')) {
                                            rmdir('./crwl-images/sex4u/' . $phone . '/');
                                        }
                                        mkdir('./crwl-images/sex4u/' . $phone . '/');
                                        foreach ($photos as $photo_key => $images_match) {
                                            $image_link = str_replace('cut_','',$images_match);
                                            $image = file_get_contents($image_link);
                                            $single_escort['images'][$photo_key] = $phone . '-' . rand(10000, 99999) . '.jpg';
                                            $single_escort['image_url'] = './crwl-images/sex4u/' . $phone . '/' . $single_escort['images'][$photo_key] ;
                                            $single_escort['image_urls'][$photo_key] = './crwl-images/sex4u/' . $phone . '/' . $single_escort['images'][$photo_key] ;
                                            file_put_contents($single_escort['image_url'], $image);
                                            $src = './img/logo-' . Cubix_Application::getId() . '.png';
                                            self::eurogirlsWatermarkImage($single_escort['image_url'], $src, $single_escort['image_url']);
                                        }
                                    }
                                }

                                if( $has_enough_photos )
                                {
                                    $escort_model = new Model_Importer();
                                    $escort_model->save($single_escort);
                                    $cli->out('Importing new escort data.');
                                    $cli->out('..............................................................................');
                                }else{
                                    $cli->out('no enough photos for import');
                                    $cli->out('..............................................................................');
                                }

                            }
                        }else{
                            $cli->out('Phone number ' . $escort_data['phone'] .'already exist');
                            $cli->out('..............................................................................');
                        }
                    }
                }
            }
        }
    }

    public function importCrowlAction()
    {
        set_time_limit(0);
        ini_set('memory_limit', '1024M');
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $cli = new Cubix_Cli();
        $pid_file = '/var/run/import-crowl' . Cubix_Application::getId() . '.pid';
        $cli->setPidFile($pid_file);

        if ($cli->isRunning()) {
            $cli->out('Action is already running... exitting...');
            exit(1);
        }
        $urls = array('https://www.de.fgirl.ch/filles/suisse/transsexuel-travesti/','https://www.en.fgirl.ch/filles/');
        $escorts = array();
        $imported_escorts = array();
        foreach ($urls as $url) {
            $i = 1;
            do {
                if ($i == 1)
                {
                    $cli->out('Starting croul ' . $url . ' URL');
                    $html = file_get_contents($url);
                }else{
                    $cli->out('Starting croul ' . $url . '?page=' . $i . ' URL');
                    $html = file_get_contents($url.'?page='.$i);
                }
                preg_match_all('$\<a.+?profile-card-img.+?href=\"([^"]+)\"$', $html, $matches);
                $escort_profiles_paths = $matches[1];
                foreach ($escort_profiles_paths as $escort_profile_path) {
                    $single_escort = array();
                    $cli->out('Crowling escort ' . 'https://www.en.fgirl.ch' . $escort_profile_path );
                    $personal_content = file_get_contents('https://www.en.fgirl.ch' . $escort_profile_path);
                    $preName = str_replace('/filles/','',$escort_profile_path);
                    $preName = str_replace('/','',$preName);
                    $name = explode('-',$preName)[0];
                    $gender = 1;
                    if (preg_match('/ts\-/',$preName) || preg_match('/\-ts/',$preName) || preg_match('/trans/',$preName))
                    {
                        $gender = 3;
                    }

                    $name = preg_replace('/[^a-zA-Z0-9\']/', '', $name);
                    $name = str_replace("'", '', $name);
                    $single_escort['gender'] = $gender;
                    $single_escort['name'] = $name;
                    $single_escort['web_site'] = 'https://www.en.fgirl.ch';
                    $single_escort['url'] = 'https://www.en.fgirl.ch' . $escort_profile_path;
                    preg_match_all('$fa-map-marker".+?\<\/i\>(.+?)\<\/div>$is', $personal_content, $city_matches);
                    $single_escort['city'] = $city_matches[1][0];
                    preg_match_all('$fa-birthday-cake".+?\<\/i\>.+?(\d+).+?\<\/div>$is', $personal_content, $age_matches);
                    $single_escort['age'] = $age_matches[1][0];
                    preg_match_all('$fa-palette".+?\<\/i\>(.+?)\<\/div>$is', $personal_content, $hair_matches);
                    $single_escort['hair'] = $hair_matches[1][0];
                    preg_match_all('$fa-record-vinyl".+?\<\/i\>(.+?)\<\/div>$is', $personal_content, $tits_matches);
                    $single_escort['tits'] = $tits_matches[1][0];
                    preg_match_all('$fa-venus-mars".+?\<\/i\>(.+?)\<\/div>$is', $personal_content, $type_matches);
                    $single_escort['profile_type'] = $type_matches[1][0];
                    preg_match_all('$fa-flag".+?\<\/i\>(.+?)\<\/div>$is', $personal_content, $ethnic_matches);
                    $single_escort['ethnicity'] = $ethnic_matches[1][0];
                    preg_match_all('$fa-eye".+?\<\/i\>(.+?)\<\/div>$is', $personal_content, $eyes_matches);
                    $single_escort['eyes'] = $eyes_matches[1][0];
                    preg_match_all('$fa-ruler-vertical".+?\<\/i\>(.+?)\<\/div>$is', $personal_content, $height_matches);
                    $height_matches = str_replace(array('m'), ',', $height_matches[1]);
                    $height_matches = str_replace(array('+', '-', '/'), '', $height_matches);
                    $single_escort['height'] = $height_matches[0];
                    preg_match_all('$fa-weight".+?\<\/i\>(.+?)\<\/div>$is', $personal_content, $size_matches);
                    $single_escort['size'] = $size_matches[1][0];
                    preg_match_all('$fa-cut".+?\<\/i\>(.+?)\<\/div>$is', $personal_content, $pubic_hair_matches);
                    $single_escort['pubic_hair'] = $pubic_hair_matches[1][0];
                    $opts = [
                        "http" => [
                            "method" => "GET",
                            "header" => "Accept-language: en\r\n" .
                                "x-requested-with: XMLHttpRequest\r\n"
                        ]
                    ];
                    $context = stream_context_create($opts);
                    $file = file_get_contents('https://www.en.fgirl.ch' . $escort_profile_path . 'call', false, $context);
                    preg_match_all('$\<a.+?btn-block.+?href=\"tel:\+([^"]+)\"$', $file, $phone_matches);
                    $phone = $phone_matches[1][0];
                    $phone = str_replace('41','',$phone);
                    $phone = str_replace(' ','',$phone);
                    $single_escort['phone'] = $phone;
                    $cli->out( 'Phone Number is ' . $phone );
                    $model = new Model_Importer();
                    preg_match_all('$Location
      \<\/h2\>(.+?)\<\/p>$is', $personal_content, $location_matches);
                    $single_escort['location'] = strip_tags(trim(preg_replace('/\s\s+/', ' ', $location_matches[1][0])));
                    if (!$model->existsByPhone($phone) && !preg_match('/\(Valais\)/',$single_escort['location']))
                    {
                        preg_match_all('$services-list"\>(.+?)\<\/ul>$is', $personal_content, $service_matches_ul);
                        preg_match_all('$\<li\>(.+?)\<\/li\>$is', $service_matches_ul[1][0], $service_matches);
                        $services_arr = array();
                        foreach ($service_matches[0] as $service) {
                            $services_arr[] = preg_replace("/\s+/", "", strip_tags($service));
                        }
                        $single_escort['services'] = $services_arr;
                        preg_match_all('$Description
      \<\/h2\>(.+?)\<\/div>$is', $personal_content, $description_matches);
                        $regexEmoticons = '/[\x{1F6}-\x{1F999}]|[\x{1F9}-\x{1F9FF}]|[\x{1FA}-\x{1FAFF}]/u';
                        $description_matches = preg_replace($regexEmoticons, '', $description_matches[1]);
                        $single_escort['about_me'] = $description_matches;
                        preg_match_all('$Spoken languages
          \<\/h2\>(.+?)\<div class\=\"card$is', $personal_content, $lang_matches);
                        $lang_matches = str_replace(' mb-2', '', $lang_matches[1][0]);
                        $langs_html = explode('<div class="media">', $lang_matches);
                        $langs = array();
                        foreach ($langs_html as $key => $html) {
                            if ($key != 0) {
                                $lang = array();
                                $lang['stars'] = substr_count($html, "fas fa-star");
                                $lang['title'] = str_replace(' ', ',', trim(preg_replace('/\s\s+/', ' ', strip_tags($html))));
                                $langs[] = $lang;
                            }
                        }
                        $single_escort['langs'] = $langs;
                        $single_escort['country'] = 'ch';


                        preg_match_all('$poster\=\"(.+?)\"$is', $personal_content, $video_matches);
                        if (strlen($video_matches[1][0])) {
                            $video_link = 'https://www.en.fgirl.ch/' . str_replace('.thumb.jpg', '', $video_matches[1][0]);
                            $video = file_get_contents($video_link);
                            $single_escort['video'] = $phone . '-' . rand(10000, 99999) . '.mp4';
                            $single_escort['video_url'] = './crwl-videos'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/' . $phone . '/' . $single_escort['video'];
                            if (!is_dir('./crwl-videos'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/')) {
                                mkdir('./crwl-videos'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/');
                            }
                            if (is_dir('./crwl-videos'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/' . $phone . '/')) {
                                rmdir('./crwl-videos'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/' . $phone . '/');
                            }
                            mkdir('./crwl-videos'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/' . $phone . '/');
                            exec ('find ./crwl-videos'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/' . $phone . '/ -type f -exec chmod 0777 {} +');
                            file_put_contents($single_escort['video_url'], $video);
                        }
                        preg_match_all('$\<a.+?image-hover.+?href=\"([^"]+)\"$', $personal_content, $images_matches);

                        if (count($images_matches[1])) {
                            if (!is_dir('./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/')) {
                                mkdir('./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/');
                            }
                            if (is_dir('./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/' . $phone . '/')) {
                                rmdir('./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/' . $phone . '/');
                            }
                            mkdir('./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/' . $phone . '/');
                            exec ('find ./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/' . $phone . '/ -type f -exec chmod 0777 {} +');
                            foreach ($images_matches[1] as $key => $images_match) {
                                $image_link = 'https://www.en.fgirl.ch' . $images_match;
                                $image = file_get_contents($image_link);
                                $single_escort['images'][$key] = $phone . '-' . rand(10000, 99999) . '.jpg';
                                $single_escort['image_url'] = './crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/' . $phone . '/' . $single_escort['images'][$key] . '.jpg';
                                $single_escort['image_urls'][$key] = './crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/' . $phone . '/' . $single_escort['images'][$key] . '.jpg';
                                file_put_contents($single_escort['image_url'], $image);
                                $src = './img/logo-' . Cubix_Application::getId() . '.png';
                                $this->watermark_image($single_escort['image_url'], $src, $single_escort['image_url']);
                            }
                        }

                        $escorts[] = $single_escort;

                        $model->save($single_escort);
                        $cli->out('Importing new escort data.');
                        $imported_escorts[] = 'https://www.en.fgirl.ch' . $escort_profile_path . '|';
                    }else{
                        $cli->out('Escort with phone number ' . $phone . ' already exist.');
                    }

                }

            }while($i++ < 100);
        }
        if (count($imported_escorts))
        {
            Cubix_Email::send('sharps1@protonmail.com','For And6.com were crawled https://www.en.fgirl.ch', "Escorts were crawled: " . implode('<br>',$imported_escorts), true);
            Cubix_Email::send('vardaninan@gmail.com','For And6.com were crawled https://www.en.fgirl.ch', "Escorts were crawled: " . implode('<br>',$imported_escorts), true);
        }
        die;
    }

    public function crowlVivaStreetAction()
    {
        set_time_limit(0);
        ini_set('memory_limit', '1024M');
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $cli = new Cubix_Cli();
        $pid_file = '/var/run/import-crowl' . Cubix_Application::getId() . '.pid';
        $cli->setPidFile($pid_file);

        if ($cli->isRunning()) {
            $cli->out('Action is already running... exitting...');
            exit(1);
        }
        $urls = array('https://www.vivastreet.co.uk/escort/gb/q/independent?type=independent');
        $escorts = array();
        $imported_escorts = array();

        foreach ($urls as $url) {

            /* crawling only first 10 pages */
            $explodedUrl = explode('q', $url);
            for ($i = 1; $i <= 10; $i++) {

                /* creating dynamic part of URL for crawling */
                if ($i !== 1) {
                    $url = $explodedUrl[0]."q+$i".$explodedUrl[1];
                }
                else {
                    $url = $explodedUrl[0].'q'.$explodedUrl[1];
                }

                $cli->out('Starting crawling URL ----   ' . $url );
                $html = file_get_contents($url);

                /* new line in CLI for more readability,  can`t do it by cli out */
                echo "</br></br>";

                preg_match_all('$\<a.+?clad__ad_link.+?href=\"([^"]+)\"$', $html, $matches);
                $escort_profiles_paths = $matches[1];

                foreach ($escort_profiles_paths as $escort_profile_path) {

                    $single_escort = array();

                    /* new line in CLI for more readability,  can`t do it by cli out */
                    echo "</br>.................................</br>";
                    $cli->out('Crawling escort ' . $escort_profile_path );
                    /* new line in CLI for more readability,  can`t do it by cli out */
                    echo "</br></br>";

                    $personal_content = file_get_contents($escort_profile_path);

                    /* crawling one escort for testing */
//                    $personal_content = file_get_contents('https://www.vivastreet.co.uk/escort/cathays-cf24/amelie-new-escort-in-town-for-few-days/238682016');

                    $dom = new DOMDocument;
                    $dom->loadHTML($personal_content);

                    $opts = [
                        "http" => [
                            "method" => "GET",
                            "header" => "Accept-language: en\r\n" .
                                "x-requested-with: XMLHttpRequest\r\n"
                        ]
                    ];

                    $phone = $dom->getElementById('phone-button-dt')->getAttribute('data-phone-number');
                    $single_escort['phone'] = $phone;

                    $cli->out( 'Phone Number is ' . $phone );
                    /* new line in CLI for more readability,  can`t do it by cli out */
                    echo "</br></br>";


                    $model = new Model_Importer();

                    if (!$model->existsByPhone($single_escort['phone']) && !$model->existsImportByPhone($single_escort['phone']) )
                    {

                        preg_match_all('/<h1.*?>(.*)<\/h1>/msi',$personal_content, $subResult);
                        $name = $subResult[1][0];
                        $single_escort['name'] = $this->clarifyStringByRegex($name);

                        preg_match_all("/<td.*?>(.+?)<\/td>/is", $personal_content, $matches_P_D);
                        $result = $matches_P_D[1];


                        if ($this->clarifyStringByRegex($result[5]) === ' Female ')
                        {
                            $gender = 1;
                        }
                        else if ($this->clarifyStringByRegex($result[5]) === ' Male ' ) {
                            $gender = 2;
                        }
                        else {
                            $gender = 3;
                        }

                        $single_escort['gender'] = $gender;

                        $single_escort['web_site'] = 'https://www.vivastreet.co.uk/';

                        $single_escort['url'] = $escort_profile_path;

                        $age = $result[7];
                        preg_match_all('!\d+!', $age, $age_matches);
                        $single_escort['age'] = $age_matches[0][0];


                        $ethnicity = $result[9];
                        switch ($this->clarifyStringByRegex($ethnicity)) {
                            case ' Mixed ':
                                $single_escort['ethnicity'] = ETHNIC_MIXED;
                                break;
                            case ' Caucasian ':
                                $single_escort['ethnicity'] = ETHNIC_CAUCASIAN;
                                break;
                            case ' Latina/o ':
                                $single_escort['ethnicity'] = ETHNIC_LATIN;
                                break;
                            case ' Asian ':
                                $single_escort['ethnicity'] = ETHNIC_ASIAN;
                                break;
                            default:
                                $single_escort['ethnicity'] = '';
                        }


                        /* getting postcode */
                        $single_escort['postcode'] = '';
                        if (trim($result[0]) == "Postcode") {
                            $single_escort['postcode'] = $this->clarifyStringByRegex(strip_tags(trim($result[1])));
                        }


                        /* getting nationality, some profiles does not have nationality information */
                        $single_escort['nationality'] = '';
                        if (array_search(' Nationality ', $result)) {
                            $kay = array_search(' Nationality ', $result);
                            $single_escort['nationality'] = trim($result[$kay + 1]);
                        }

                        $finder = new DomXPath($dom);
                        $services_provided = $finder->query("//*[contains(@class, 'service-true')]");

                        $services_arr = array();
                        foreach ($services_provided as $service) {
                            $services_arr[] = $service->textContent;
                        }
                        $single_escort['services'] = $services_arr;

                        $shortDescription = $finder->query("//*[contains(@class, 'shortdescription')]");
                        $single_escort['about_me'] = $this->clarifyStringByRegex($shortDescription->item(0)->textContent);

                        /* we can`t get some data by DOM, and we are trying to get by preg_match result*/
                        $languages = $result[11];
                        $lang_array =  explode(',', $languages);
                        if (!empty($lang_array)) {
                            $languages_array = array();
                            foreach ($lang_array as $key => $lang) {
                                $langs['stars'] = 4;
                                $langs['title'] = $lang;
                                $languages_array[] = $langs;
                            }
                            $single_escort['langs'] = $languages_array;
                        }

                        $single_escort['country'] = 'united-kingdom';


                        $photoThumbnails = $dom->getElementsByTagName('img');
                        if (!empty($photoThumbnails)) {

                            if (!is_dir('./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/')) {
                                mkdir('./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/');
                            }
                            if (is_dir('./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/' . $phone . '/')) {
                                rmdir('./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/' . $phone . '/');
                            }
                            mkdir('./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/' . $phone . '/');
                            exec ('find ./crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/' . $phone . '/ -type f -exec chmod -R 0777 {} +');

                            foreach ($photoThumbnails as $key => $img) {
                                if ( $img->getAttribute('class') == 'photo-thumbnail') {
                                	$srcset = $img->getAttribute('data-srcset');
                                    if (!empty($srcset)) {
                                        if(strpos( $srcset , 'vivastreet_gb')) {

                                            $smallImageLink = explode('?', $img->getAttribute('data-srcset'))[0];
                                            $largeImageLink = str_replace('vip', 'large', $smallImageLink);

                                            $image = file_get_contents($largeImageLink);

                                            $single_escort['images'][$key] = $phone . '-' . rand(10000, 99999);
                                            $single_escort['image_url'] = './crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/' . $phone . '/' . $single_escort['images'][$key] . '.jpg';
                                            $single_escort['image_urls'][$key] = './crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/' . $phone . '/' . $single_escort['images'][$key] . '.jpg';

                                            file_put_contents($single_escort['image_url'], $image);

                                            /* crop image bottom for deleting watermark*/
                                            $this->imageCrop($single_escort['image_url'], 0, 60);

                                        }
                                    }
                                }
                            }
                        }

//                    preg_match_all('$fa-map-marker".+?\<\/i\>(.+?)\<\/div>$is', $personal_content, $city_matches);
//                    $single_escort['city'] = $city_matches[1][0];

//                    preg_match_all('$Location \<\/h2\>(.+?)\<\/p>$is', $personal_content, $location_matches);
//                    $single_escort['location'] = strip_tags(trim(preg_replace('/\s\s+/', ' ', $location_matches[1][0])));

                        $escorts[] = $single_escort;

                        $model->save($single_escort);
                        $cli->out('Importing new escort data.');
                        /* new line in CLI for more readability,  can`t do it by cli out */
                        echo "</br>.................................</br>";

                        $imported_escorts[] = $escort_profile_path . '|';
                    }else{
                        $cli->out('Escort with phone number ' . $phone . ' already exist.');
                        /* new line in CLI for more readability,  can`t do it by cli out */
                        echo "</br>.................................</br>";
                    }
                }
            }
        }
//        if (count($imported_escorts))
//        {
//            Cubix_Email::send('sharps1@protonmail.com','For And6.com were crawled https://www.en.fgirl.ch', "Escorts were crawled: " . implode('<br>',$imported_escorts), true);
//            Cubix_Email::send('vardaninan@gmail.com','For And6.com were crawled https://www.en.fgirl.ch', "Escorts were crawled: " . implode('<br>',$imported_escorts), true);
//        }
        die;
    }

    public function crowlLadiesDeAction()
    {
        set_time_limit(0);
        ini_set('memory_limit', '1024M');
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $cli = new Cubix_Cli();
        $pid_file = '/var/run/import-crowl-ladies-de' . Cubix_Application::getId() . '.pid';
        $cli->setPidFile($pid_file);

        if ($cli->isRunning()) {
            $cli->out('Action is already running... exitting...');
            exit(1);
        }

        $services_compare = array(
            'Körperküsse'=> 8,
            'Duschservice' => 60,
            'Badeservice' => 67,
            'DS aktiv' => 50,
            'DS passive' => 51,
            'gekonnter Striptease' => 34,
            'LS / Duo' => 69,
            'Fuß- / Schuherotik' => 72,
            'Massagen' => 53,
        );

        $urls = array('https://www.ladies.de/sex-anzeigen');
        $escorts = array();
        foreach ($urls as $url) {
            $i = 1;
            do {
                if ($i == 1)
                {
                    $cli->out('Starting croul ' . $url . ' URL');
                    $html = file_get_contents($url);
                }else{
                    $cli->out('Starting croul ' . $url . '?page=' . $i . ' URL');
                    $html = file_get_contents($url.'?page='.$i);
                }
                preg_match_all('$\<a href\=\"\/sex\-anzeigen([^"]+)\" class\=\"ordered popunder_element$', $html, $matches);
                $escort_profiles_paths = $matches[1];
                if ($escort_profiles_paths)
                {
                    foreach ($escort_profiles_paths as $escort_profile_path) {
                        $single_escort = array();
                        $cli->out('Crowling escort ' . 'https://www.ladies.de/sex-anzeigen' . $escort_profile_path );
                        $personal_content = file_get_contents('https://www.ladies.de/sex-anzeigen' . $escort_profile_path);
                        preg_match('$kuenstlername\"\>([^"]+)\<\/h2\>$is', $personal_content, $name_match);
                        $name = $name_match[1];
                        $single_escort['name'] = $name;
                        preg_match('$itemprop\=\"telephone\"\>([^"]+)\<\/strong\>$', $personal_content, $phone_match);
                        if ( strlen($phone_match[1]) < 2 )
                            preg_match('$class\=\"important\"\>\<strong\>([^"]+)\<\/strong\>\<\/p\>$', $personal_content, $phone_match);
                        $phone = str_replace('-','',$phone_match[1]);
                        $phone = substr($phone,1);
                        $model = new Model_Importer();
                        if (strlen($phone) > 1 && !$model->existsByPhone($phone) && !$model->existsImportByPhone($phone) )
                        {
                            $single_escort['country'] = 'de';
                            $single_escort['full_phone'] = $phone_match[1];
                            $single_escort['phone'] = $phone;
                            preg_match('$\<span class\=\"single\-line\"\>männlich\<\/([^"]+)\>$', $personal_content, $male_match);
                            $gender = preg_match('$männlich$',$male_match[0]) ? 2 : 1;
                            $single_escort['gender'] = $gender;
                            $single_escort['web_site'] = 'https://www.ladies.de';
                            $single_escort['url'] = 'https://www.ladies.de/sex-anzeigen' . $escort_profile_path;
                            preg_match('$addressLocality\"\>\<span class\=\"important\"\>([^"]+)\<\/span\>$', $personal_content, $city_matches);
                            $city = '';
                            if ($city_matches[1] && strlen($city_matches[1]) > 1)
                            {
                                $city = $city_matches[1];
                            }else{
                                preg_match('$data-ort=\"([^"]+)\">
					\<p\>
						                            							
							\<span\>\<span class=\"important\"\>$', $personal_content, $city_matches);
                                if (strlen($city_matches[1]) > 1)
                                    $city = $city_matches[1];
                            }
                            $city = preg_replace('/<[^>]*>/', '', $city);
                            $city = explode(' ', $city)[0];
                            $single_escort['city'] = $city;
                            preg_match('$Alter\<\/strong\>\<\/div\>
										\<div class\=\"col\-xs\-7\"\>
													\<span class\=\"single\-line\"\>([^"]+) Jahre$', $personal_content, $age_matches);
                            $single_escort['age'] = intval($age_matches[1]);
                            preg_match('$Größe\<\/strong\>\<\/div\>
										\<div class\=\"col\-xs\-7\"\>
													\<span class\=\"single\-line\"\>([^"]+) cm$', $personal_content, $height_matches);
                            $single_escort['height'] = intval($height_matches[1]);
                            preg_match('$Service\<\/strong\>
				\<\/p\>

									\<p class\=\"attribute\-item\"\>
						([^"]+)					\<\/p\>
									\<p class\=\"attribute\-item\"\>
						([^"]+)					\<\/p\>
									\<\/div\>
									\<p class\=\"attribute\-item\"\>
						([^"]+)					\<\/p\>
									\<p class\=\"attribute\-item\"\>
						([^"]+)					\<\/p\>
									\<p class\=\"attribute\-item\"\>
						([^"]+)					\<\/p\>
									\<p class\=\"attribute\-item\"\>
						([^"]+)					\<\/p\>
									\<p class\=\"attribute\-item\"\>
						([^"]+)					\<\/p\>
									\<p class\=\"attribute\-item\"\>
						([^"]+)					\<\/p\>
									\<p class\=\"attribute\-item\"\>
						([^"]+)					\<\/p\>
									\<p class\=\"attribute\-item\"\>
						([^"]+)					\<\/p\>
									\<p class\=\"attribute\-item\"\>
						([^"]+)					\<\/p\>
									\<p class\=\"attribute\-item\"\>
						([^"]+)					\<\/p\>
									\<\/div\>$', $personal_content, $service_matches);
                            unset($service_matches[0]);
                            if( count($service_matches ) > 0)
                            {
                                $services_arr = array();
                                foreach ($services_compare as $srv => $k)
                                {
                                    foreach ($service_matches as $service)
                                    {
                                        if (preg_match("/{$srv}/",$service))
                                        {
                                            $services_arr[] = $k;
                                        }
                                    }
                                }
                                $single_escort['services'] = $services_arr;
                            }
                            $single_escort['services'] = $service_matches;
                            preg_match('$einzelansicht-text[^>]+>(.+?)\<a class\=\"btn\-more\-text more\-link hidden-print\"$is', $personal_content, $about_matches);
                            $about = preg_replace('/<[^>]*>/', '', $about_matches[1]);
                            $about = preg_replace("/[\n\r]/", "", $about);

                            $about = preg_replace('/\s\s+/', ' ', $about);
                            $about = str_replace('*','',$about);
                            if ($about)
                            {
                                $regexEmoticons = '/[\x{1F6}-\x{1F999}]|[\x{1F9}-\x{1F9FF}]|[\x{1FA}-\x{1FAFF}]/u';
                                $description_matches = preg_replace($regexEmoticons, '', $about);
                                $single_escort['about_me'] = $description_matches;
                            }
                            preg_match_all('$class\=\"gallery\_item\" href\=\"([^"]+)\">
	
		\<div class\=\"fotothumb\-item$', $personal_content, $photo_matches);
                            $has_enough_photos = false;
                            if (count($photo_matches[1]) > 3)
                            {
                                $has_enough_photos = true;
                                if ( preg_match('#(test\.sceon\.am|\.test)$#', $_SERVER['SERVER_NAME']) > 0 ) {
                                    if (!is_dir('./crwl-images-ladies-de')) {
                                        mkdir('./crwl-images-ladies-de');
                                    }
                                    if (is_dir('./crwl-images-ladies-de/' . $phone . '/')) {
                                        rmdir('./crwl-images-ladies-de/' . $phone . '/');
                                    }
                                    mkdir('./crwl-images-ladies-de/' . $phone . '/');
                                    exec('find ./crwl-images' . (Cubix_Application::getId() == APP_A6 ? '' : '-' . Cubix_Application::getId()) . '/' . $phone . '/ -type f -exec chmod 0777 {} +');
                                    foreach ($photo_matches[1] as $key => $images_match) {
                                        $image_link = $images_match;
                                        $image = file_get_contents(str_replace('-F', '-A', $image_link));
                                        $single_escort['images'][$key] = $phone . '-' . rand(10000, 99999) . '.jpg';
                                        $single_escort['image_url'] = './crwl-images-ladies-de/' . $phone . '/' . $single_escort['images'][$key] . '.jpg';
                                        $single_escort['image_urls'][$key] = './crwl-images-ladies-de/' . $phone . '/' . $single_escort['images'][$key] . '.jpg';
                                        file_put_contents($single_escort['image_url'], $image);
//                                $src = './img/logo-' . Cubix_Application::getId() . '.png';
//                                self::eurogirlsWatermarkImage($single_escort['image_url'], $src, $single_escort['image_url']);
                                    }
                                }else{
                                    if (!is_dir('/data/crwl-images-ladies-de')) {
                                        mkdir('/data/crwl-images-ladies-de');
                                    }
                                    if (is_dir('/data/crwl-images-ladies-de/' . $phone . '/')) {
                                        rmdir('/data/crwl-images-ladies-de/' . $phone . '/');
                                    }
                                    mkdir('/data/crwl-images-ladies-de/' . $phone . '/');
                                    exec('find /data/crwl-images-ladies-de/' . $phone . '/ -type f -exec chmod 0777 {} +');
                                    foreach ($photo_matches[1] as $key => $images_match) {
                                        $image_link = $images_match;
                                        $image = file_get_contents(str_replace('-F', '-A', $image_link));
                                        $single_escort['images'][$key] = $phone . '-' . rand(10000, 99999) . '.jpg';
                                        $single_escort['image_url'] = '/data/crwl-images-ladies-de/' . $phone . '/' . $single_escort['images'][$key] . '.jpg';
                                        $single_escort['image_urls'][$key] = '/data/crwl-images-ladies-de/' . $phone . '/' . $single_escort['images'][$key] . '.jpg';
                                        file_put_contents($single_escort['image_url'], $image);
//                                $src = './img/logo-' . Cubix_Application::getId() . '.png';
//                                self::eurogirlsWatermarkImage($single_escort['image_url'], $src, $single_escort['image_url']);
                                    }
                                }
                            }
                            $escort_model = new Model_Importer();
                            if($has_enough_photos) {
                                $escort_model->save($single_escort);
                                $cli->out('Importing new escort data.');
                                $cli->out('..............................................................................');
                            } else {
                                $cli->out('no enough photos for import');
                                $cli->out('..............................................................................');
                            }

                        }else{
                            $cli->out('Escort with phone number ' . $phone . ' already exist.');
                        }
                    }
                }


            }while($i++ < 220);
            die;
        }
    }

    public function crowlBooksusiAction()
    {
        set_time_limit(0);
        ini_set('memory_limit', '1024M');
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $cli = new Cubix_Cli();
        $pid_file = '/var/run/import-crowl-booksussi' . Cubix_Application::getId() . '.pid';
        $cli->setPidFile($pid_file);

        if ($cli->isRunning()) {
            $cli->out('Action is already running... exitting...');
            exit(1);
        }
        $services_compare = array(
            'Blow job without Condom'=> 9,
            'Cum on Tits' => 33,
            'Titfuck' => 57,
            'Toys' => 5,
            'Girlfriend' => 7,
            'Swallowing' => 26,
            'Strip' => 34,
            'kissing' => 8,
            'Fetisch' => 39,
            'Foot' => 72,
            'Golden shower active' => 41,
            'Relaxing Massage' => 54,
            'Erotic Massage' => 53,
            'Sexo anal' => 2,
            'Classic Massage' => 54,
            'Face Sitting' => 18,
            'Trampling' => 71,
            'Deep Throat' => 14,
            'Cum in Mouth' => 3,
            'Rimming active' => 31,
            'Anal/Prostata Massage' => 58,
            'Ball busting' => 17,
            'Bondage' => 37,
            'Spanking' => 47,
            'Lesbian sex' => 23,
        );

        $countries = array(24 => 'germany',4 => 'austria');
        foreach ($countries as $country_key => $country) {
            switch ($country)
            {
                case 'germany':
                    $iso = 'de';
                    break;
                case 'austria':
                    $iso = 'st';
                    break;
            }

            $csv_file = './importer-csv/booksusi.csv';
            if (file_exists($csv_file)) {
                $data = $fields = array();
                $i = 0;
                $handle = @fopen($csv_file, "r");
                if ($handle) {
                    $cli->out('Importing Country:' . $country);
                    while (($row = fgetcsv($handle, 4096)) !== false) {
                        if (empty($fields)) {
                            $fields = $row;
                            continue;
                        }
                        foreach ($row as $k => $value) {
                            $data[$i][$fields[$k]] = $value;
                        }
                        $i++;
                    }
                    if (!feof($handle)) {
                        echo "Error: unexpected fgets() fail\n";
                    }
                    fclose($handle);
                    foreach ($data as $escort_data) {
                        $single_escort = array();
                        $single_escort['country_title'] = $country;
                        $single_escort['web_site'] = 'https://booksusi.com';
                        if ($escort_data['phone'] && $escort_data['phone1'] != "null")
                        {
                            $model = new Model_Importer();
                            $trimmed_phone = str_replace("tel:", '', $escort_data['phone']);
                            $trimmed_phone = substr($trimmed_phone, 4);
                            $cli->out('Importing Escort with phone:' . $trimmed_phone);
                            $trimmed_phone = str_replace(' ','',$trimmed_phone);
                            $escort_data['phone'] = $trimmed_phone;
                            if (!$model->existsByPhone($escort_data['phone']) && !$model->existsImportByPhone($escort_data['phone']) ) {
                                $cli->out('Crowling escort ' . $escort_data['web-scraper-start-url']);
                                if ($escort_data['gender']) {
                                    switch ($escort_data['gender']) {
                                        case "Male":
                                            $gender = 2;
                                            break;
                                        case "Transe":
                                            $gender = 3;
                                            break;
                                        default:
                                            $gender = 1;
                                    }
                                    $single_escort['gender'] = $gender;
                                }else{
                                    $single_escort['gender'] = 1;
                                }

                                $single_escort['online'] = '';
                                if ($escort_data['online'] && $escort_data['online'] != "null")
                                    $single_escort['online'] = strtotime($escort_data['online']);

                                if ($escort_data['website'] && $escort_data['website'] != "null")
                                    $single_escort['website'] = $escort_data['website'];

                                $single_escort['phone'] = $escort_data['phone'];
                                $single_escort['url'] = $escort_data['escort-href'];
                                $single_escort['name'] = strtok($escort_data['name3'], ' ');
                                $single_escort['city'] = explode('/',$escort_data['escort-href'])[4];
                                if($escort_data['country'])
                                {
                                    switch ($escort_data['country'])
                                    {
                                        case 'AT':
                                            $single_escort['country'] = 'at';
                                            break;
                                        case 'DE':
                                            $single_escort['country'] = 'de';
                                            break;
                                    }
                                }

                                if( $escort_data['services'] && $escort_data['services'] != "null" )
                                {
                                    $services_arr = array();
                                    $services = explode(',',str_replace(']','',str_replace('[','',$escort_data['services'])));
                                    foreach ($services_compare as $srv => $k)
                                    {
                                        foreach ($services as $service)
                                        {
                                            if (preg_match("/{$srv}/",str_replace('"}','',str_replace('{"services":"','',$service))))
                                            {
                                                $services_arr[] = $k;
                                            }
                                        }
                                    }
                                    $single_escort['services'] = $services_arr;
                                }

                                $single_escort['age'] = $escort_data['age'] ? $escort_data['age'] : '20';

                                if ($escort_data['weight'] && $escort_data['weight'] != "null")
                                    $single_escort['weight'] = $escort_data['weight'];
                                if ($escort_data['height'] && $escort_data['height'] != "null")
                                    $single_escort['height'] = $escort_data['height'];
                                if ($escort_data['height'] && $escort_data['height'] != "null")
                                    $single_escort['height'] = $escort_data['height'];
                                if ($escort_data['about'] && $escort_data['about'] != "null")
                                {
                                    $regexEmoticons = '/[\x{1F6}-\x{1F999}]|[\x{1F9}-\x{1F9FF}]|[\x{1FA}-\x{1FAFF}]/u';
                                    $description_matches = preg_replace($regexEmoticons, '', $escort_data['about']);
                                    $single_escort['about_me'] = $description_matches;
                                }
                                $has_enough_photos = false;
                                if ($escort_data['photos'] && $escort_data['photos'] != "null" && strlen($escort_data['photos']) > 10)
                                {
                                    $photos_array = explode('},',$escort_data['photos']);
                                    $photos = array();

                                    foreach ($photos_array as $key => $photo) {
                                        if($key < 10)
                                        {
                                            $current_photo = str_replace('{photos:,photos-src:','',str_replace('400x592','1000x700',str_replace('100x100','1000x700',str_replace('"','',str_replace('[','',str_replace('{"Photos":"","Photos-src":"', '', str_replace('}]','',$photo)))))));
                                            if(!preg_match('/\.png/',$current_photo))
                                                $photos[] = $current_photo;
                                        }
                                    }
                                    if ( preg_match('#(test\.sceon\.am|\.test)$#', $_SERVER['SERVER_NAME']) > 0 ) {
                                        if ( count($photos) > 2 ) {
                                            $has_enough_photos = true;
                                            if (!is_dir('./crwl-images-booksusi/')) {
                                                mkdir('./crwl-images-booksusi/');
                                            }
                                            $phone = $single_escort['phone'];

                                            if (is_dir('./crwl-images-booksusi/' . $phone . '/')) {
                                                rmdir('./crwl-images-booksusi/' . $phone . '/');
                                            }
                                            mkdir('./crwl-images-booksusi/' . $phone . '/');
                                            $i = 1000;
                                            foreach ($photos as $photo_key => $images_match) {
                                                $image_link = $images_match;
//                                                $image = file_get_contents($image_link);
                                                $single_escort['images'][$photo_key] = $phone .'-'. $i++.'.jpg';
                                                $single_escort['image_url'] = './crwl-images-booksusi/' . $phone . '/' . $single_escort['images'][$photo_key] ;
                                                $single_escort['image_urls'][$photo_key] = './crwl-images-booksusi/' . $phone . '/' . $single_escort['images'][$photo_key] ;
//                                                file_put_contents($single_escort['image_url'], $image);
//                                                $src = './img/logo-' . Cubix_Application::getId() . 'eu.png';
//                                                $ext = preg_match('/jpg/',$single_escort['image_url'])? 'jpg' : 'png';
//                                                self::eurogirlsWatermarkImage($single_escort['image_url'], $src, $single_escort['image_url']);
                                            }
                                        }
                                    }else{
                                        if ( count($photos) > 2 ) {
                                            $has_enough_photos = true;
//                                            if (!is_dir('/data/crwl-images-booksusi')) {
//                                                mkdir('/data/crwl-images-booksusi');
//                                            }
                                            $phone = $single_escort['phone'];

//                                            if (is_dir('/data/crwl-images-booksusi/' . $phone . '/')) {
//                                                rmdir('/data/crwl-images-booksusi/' . $phone . '/');
//                                            }
//                                            mkdir('/data/crwl-images-booksusi/' . $phone . '/');

                                            $i = 1000;
                                            foreach ($photos as $photo_key => $images_match) {
                                                $image_link = $images_match;
//                                                $image = file_get_contents($image_link);
                                                $single_escort['images'][$photo_key] = $phone .'-'. $i++.'.jpg';
                                                $single_escort['image_url'] = '/data/crwl-images-booksusi/' . $phone . '/' . $single_escort['images'][$photo_key] ;
                                                $single_escort['image_urls'][$photo_key] = '/data/crwl-images-booksusi/' . $phone . '/' . $single_escort['images'][$photo_key] ;
//                                                file_put_contents($single_escort['image_url'], $image);
//                                                $src = './img/logo-' . Cubix_Application::getId() . 'eu.png';
//                                                self::eurogirlsWatermarkImage($single_escort['image_url'], $src, $single_escort['image_url']);
                                            }
                                        }
                                    }

                                }

                                $escort_model = new Model_Importer();

                                if( $has_enough_photos )
                                {
                                    $escort_model->save($single_escort);
                                    $cli->out('Importing new escort data.');
                                    $cli->out('..............................................................................');
                                }else{
                                    $cli->out('no enough photos for import');
                                    $cli->out('..............................................................................');
                                }
                            }else{
                                $cli->out('Phone number ' . $escort_data['phone'] .'already exist');
                                $cli->out('..............................................................................');
                            }
                        }
                    }
                }

            }
        }
    }

    public function watermark_image($target, $wtrmrk_file, $newcopy) {
        $watermark = imagecreatefrompng($wtrmrk_file);
        imagealphablending($watermark, false);
        imagesavealpha($watermark, true);
        $img = imagecreatefromjpeg($target);
        $img_w = imagesx($img);
        $img_h = imagesy($img);
        $wtrmrk_w = imagesx($watermark);
        $wtrmrk_h = imagesy($watermark);
        $dst_x = ($img_w / 1.9) - ($wtrmrk_w / 1.9); // For centering the watermark on any image
        $dst_y = ($img_h / 1.5) - ($wtrmrk_h / 1.8); // For centering the watermark on any image
        imagecopy($img, $watermark, $dst_x, $dst_y, 0, 0, $wtrmrk_w, $wtrmrk_h);
        imagejpeg($img, $newcopy, 100);
        imagedestroy($img);
        imagedestroy($watermark);
    }

    public function imageCrop($targetUrl, $fromWidth = 0, $fromHeight = 0) {

        $img = imagecreatefromjpeg($targetUrl);

        $img_w = imagesx($img);
        $img_h = imagesy($img);

        $newWidth = $img_w - $fromWidth;
        $newHeight = $img_h - $fromHeight;

        $area = ["x"=>0, "y"=>0, "width"=>$newWidth, "height"=>$newHeight];
        $crop = imagecrop($img, $area);
        imagejpeg($crop, $targetUrl, 100);
    }

    public function clarifyStringByRegex($string) {

        // Match Emoticons
        $regex_emoticons = '/[\x{1F600}-\x{1F64F}]/u';
        $clear_string = preg_replace($regex_emoticons, '', $string);

        // Match Miscellaneous Symbols and Pictographs
        $regex_symbols = '/[\x{1F300}-\x{1F5FF}]/u';
        $clear_string = preg_replace($regex_symbols, '', $clear_string);

        // Match Transport And Map Symbols
        $regex_transport = '/[\x{1F680}-\x{1F6FF}]/u';
        $clear_string = preg_replace($regex_transport, '', $clear_string);

        // Match Miscellaneous Symbols
        $regex_misc = '/[\x{2600}-\x{26FF}]/u';
        $clear_string = preg_replace($regex_misc, '', $clear_string);

        // Match Dingbats
        $regex_dingbats = "~[^a-zA-Z0-9_ !@#$%^&*();\\\/|<>\"'+.,:?=-]~";
        $clear_string = preg_replace($regex_dingbats, '', $clear_string);

        return $clear_string;
    }

    public static function eurogirlsWatermarkImage($target, $wtrmrk_file, $newcopy) {
        $watermark = imagecreatefrompng($wtrmrk_file);
        imagealphablending($watermark, false);
        imagesavealpha($watermark, true);
        $img = imagecreatefromjpeg($target);
        $img_w = imagesx($img);
        $img_h = imagesy($img);
        $wtrmrk_w = imagesx($watermark);
        $wtrmrk_h = imagesy($watermark);
        if($img_w > $img_h)
        {
            $dst_x = ($img_w / 1.8) - ($wtrmrk_w / 1.5);
            $dst_y = ($img_h / 1.8) - ($wtrmrk_h / 1.5);
        }elseif($img_w < $img_h){
            $dst_x = ($img_w / 1.9) - ($wtrmrk_w / 1.9);
            $dst_y = ($img_h / 1.7) - ($wtrmrk_h / 1.5);
        }else{
            $dst_x = ($img_w / 2) - ($wtrmrk_w / 2);
            $dst_y = ($img_h / 1.9) - ($wtrmrk_h / 1.8);
        }
        imagecopy($img, $watermark, $dst_x, $dst_y, 0, 0, $wtrmrk_w, $wtrmrk_h);
        imagejpeg($img, $newcopy, 100);
        imagedestroy($img);
        imagedestroy($watermark);
    }


    public function fixNewsmanSubscribersAction()
    {
        set_time_limit(0);
        $app_id = Cubix_Application::getId();
        $cli = new Cubix_Cli();
        $cli->clear();
        $pid_file = '/var/run/fix-newsman-subscribers-' . $app_id . '.pid';
        $cli->setPidFile($pid_file);
        if ( $cli->isRunning() ) {
            $cli->out('Action is already running... exitting...');
            exit(1);
        }

        $newsman_lists = new Cubix_Newsman_Ids();
        $user_statuses = array(USER_STATUS_ACTIVE, USER_STATUS_DISABLED, USER_STATUS_DELETED);

        $client = new Cubix_Newsman($_SERVER["REMOTE_ADDR"]);
        $app_newsman_lists = $newsman_lists->get($app_id);

        $not_found_code = 121; /* Could not load subscriber error code */

        foreach ($app_newsman_lists as $app_list_id => $app_newsman_segments) {
            $chunk = 1000;
            $part = 0;
            do{
                $app_users = $this->_db->fetchAll("SELECT id, email, user_type, `status`, recieve_newsletters
                    FROM users
                    WHERE application_id = ". $app_id ." and 
                          status IN ('". implode("','", $user_statuses) ."') and
                          user_type IN ('". implode("','", array_keys($app_newsman_segments)) ."')
                    ORDER BY user_type DESC LIMIT " . $part++ * $chunk . ",". $chunk . " 
            ");
                $cli->out("Processing chunk: ". $part ."-" . $chunk);
                $cli->out("------------------------------------------------------------------------");
                foreach ($app_users as $app_user) {
                    if ($subscriber = $client->getSubscriberByEmail($app_list_id, $app_user->email)) {
                        $need_to_subscribe = $need_to_unsubscribe = false;
                        $params = array(
                            'user_id' => $app_user->id,
                            'user_status' => $app_user->status
                        );

                        if (isset($subscriber['err'])) {
                            if (isset($subscriber['code']) && $subscriber['code'] == $not_found_code) {
                                if ($app_user->status == USER_STATUS_ACTIVE && $app_user->recieve_newsletters == 1) {
                                    $need_to_subscribe = true;
                                    $params['status_before'] = '';
                                }
                            }
                        } else {
                            $params['status_before'] = $subscriber['status'];

                            if (in_array($app_user->status, array(USER_STATUS_DISABLED, USER_STATUS_DELETED)) && $subscriber['status'] == 'subscribed') {
                                $need_to_unsubscribe = true;
                            } elseif ($app_user->status == USER_STATUS_ACTIVE) {
                                if ($app_user->recieve_newsletters == 1 && $subscriber['status'] == 'unsubscribed') {
                                    $need_to_subscribe = true;
                                } elseif ($app_user->recieve_newsletters == 0 && $subscriber['status'] == 'subscribed') {
                                    $need_to_unsubscribe = true;
                                }
                            }

                            if ($need_to_unsubscribe) {
                                $cli->out('Unsubscribing '.$app_user->email);
                                $params['status_after'] = 'unsubscribed';
                                if (isset($subscriber['segments']) && is_array($subscriber['segments']) && count($subscriber['segments']) > 0) {
                                    foreach ($subscriber['segments'] as $subscriber_segment) {
                                        if (in_array($subscriber_segment['segment_id'], $app_newsman_segments)) {
                                            $remove_from_segment = $client->removeFromSegment($subscriber['subscriber_id'], $subscriber_segment['segment_id']);
                                            if (isset($remove_from_segment['err'])) {
                                                $cli->out($app_user->email." didn't removed from segment - " . $subscriber_segment['segment_id'] . ', because' . $remove_from_segment['message']);
                                            } else {
                                                $cli->out($app_user->email." removed from segment - " . $subscriber_segment['segment_id']);
                                            }
                                        }
                                    }
                                }

                                $unsubscribe = $client->saveUnsubscribe($subscriber['list_id'], $subscriber['email']);
                                if (isset($unsubscribe['err'])) {
                                    $cli->out($app_user->email." didn't unsubscribed" . ', because' . $unsubscribe['message']);
                                } else {
                                    $cli->out($app_user->email.' unsubscribed');
                                }
                            }
                        }

                        if ($need_to_subscribe) {
                            $cli->out('Subscribing '.$app_user->email);
                            $subscriber_id = $client->saveSubscribe($app_list_id, $app_user->email);
                            if (isset($subscriber_id['err'])) {
                                $cli->out($app_user->email." didn't subscribed" . ', because ' . $subscriber_id['message']);
                            } else {
                                $cli->out($app_user->email.' subscribed');
                            }

                            $segment_id = $app_newsman_segments[$app_user->user_type];
                            $add_to_segment = $client->addToSegment($subscriber_id, $segment_id);
                            if (isset($add_to_segment['err'])) {
                                $cli->out($app_user->email." didn't added to segment - " . $segment_id .', because ' . $add_to_segment['message']);
                            } else {
                                $cli->out($app_user->email.' added to segment - '. $segment_id);
                            }
                            $params['status_after'] = 'subscribed';
                        }

                        if (count($params) == 4) {
                            $this->_db->insert('users_subscribe_log', $params);
                        }
                    }
                    $cli->out("-----------------------------------");
                }
            }while($app_users);
        }

        exit("Fixing finished \n");
    }

    public function changeNotRespondedEscortsStatusesAction()
    {
        set_time_limit(0);
        ini_set('memory_limit', '1024M');
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $cli = new Cubix_Cli();
        $pid_file = '/var/run/check-backend-not-verified-escorts-statuses-' . Cubix_Application::getId() . '.pid';
        $cli->setPidFile($pid_file);
        if ($cli->isRunning()) {
            $cli->out('Action is already running... exitting...');
            exit(1);
        }
        try {
            $sql = "SELECT
                            e.id,
                            ep.contact_phone_parsed AS phone 
                        FROM
                            escorts e
                            INNER JOIN users u ON e.user_id = u.id
                            INNER JOIN escort_responses er ON er.escort_id = e.id
                            INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
                            INNER JOIN order_packages op ON op.escort_id = e.id 
                            AND op.STATUS = 2 
                        WHERE
                            u.last_login_date IS NULL 
                            AND e.last_hand_verification_date IS NULL 
                            AND u.email = 'no@no.com' 
                            AND 4 < ( SELECT COUNT( id ) FROM escort_comments WHERE `comment` LIKE '%auto sms and mail for confirmation%' AND escort_id = e.id ) 
                            AND 4 < ( SELECT COUNT( escort_id ) FROM escort_responses WHERE escort_id = e.id ) AND UNIX_TIMESTAMP(DATE_SUB( NOW(), INTERVAL 14 DAY )) > UNIX_TIMESTAMP( op.date_activated ) 
                        GROUP BY
                            e.id" ;

            $escortsToAffect = $this->_db->fetchALL($sql);
            if (count($escortsToAffect))
            {
                $comment = '5 sms and 5 calls, no answer';
                $originator = Cubix_Application::getPhoneNumber(Cubix_Application::getId());

                foreach ($escortsToAffect as $escort)
                {
                    $sms_data = array();
                    $sms_data['text'] = 'Your escort ad on escortguide.co.uk has expired and is not visible to clients anymore. If you want to keep it active, please contact us at info@escortguide.co.uk.';

                    $smsWasSent = $this->_db->fetchOne('SELECT TRUE 
                                                                    FROM
                                                                        sms_outbox 
                                                                    WHERE
                                                                        escort_id = ?
                                                                        AND text LIKE "%'.$sms_data['text'].'%"',array($escort->id));
                    if(!$smsWasSent)
                    {
                        $status = new Cubix_EscortStatus($escort->id);
                        $status->setStatus(Cubix_EscortStatus::ESCORT_STATUS_INACTIVE);
                        $status->save();
                        $this->_db->insert('escort_comments', array(
                            'escort_id' => $escort->id,
                            'comment' => $comment,
                            'date' => new Zend_Db_Expr('NOW()'),
                        ));
                        $model_sms = new Model_SMS_Outbox();

                        $sms_data['phone_number'] = $escort->phone;
                        $sms_data['phone_from'] = $originator;
                        $sms_data['escort_id'] = $escort->id;
                        $sms_data['is_spam'] = false;
                        $sms_data['lang'] = 'en';
                        $model_sms->saveAndSend($sms_data);
                    }

                    $cli->out('Escort ID: '. $escort->id .' status become Inactive after 5 sms and 5 calls, no answer.');
                    Cubix_Email::send('vardaninan@gmail.com',ucfirst(Cubix_Application::getId()).' Escort ID:'. $escort->id .' status become Inactive after 5 sms and 5 calls, no answer.', ucfirst(Cubix_Application::getId()).' Escort ID:'. $escort->id .' status become Inactive after 5 sms and 5 calls, no answer.', true);
                }
            }else{
                $cli->out('0 Afecter escorts');
            }
            $cli->out('done');
            exit(1);
        }catch (Exception $e) {
            $cli->out($cli->out("[fail] {$e->getMessage()}"));
            Cubix_Email::send('vardaninan@gmail.com', 'Change Expired Escorts Statuses failed', $e->__toString());
            exit(1);
        }
    }


    /*
	 * JIRA Task https://sceonteam.atlassian.net/browse/A6-267
	 */
    public function fixProfileUpdateCitiesAction()
    {
    	die;
        $cli = new Cubix_Cli();
        $cli->clear();

        $db = $this->_db;
        $cities = $db->fetchAll("SELECT id FROM cities");
        $c_ids = array();

        foreach ($cities as $city) {
            $c_ids[] = $city->id;
        }

        $chunk = 1000;
        $part = 0;
        $count = 0;

        do {
            $cli->out("Processing chunk: ". $part ."-" . $chunk);
            $sql = "SELECT pu.id, pu.escort_id, pu.`data`
                FROM profile_updates_v2 AS pu LIMIT " . $part++ * $chunk . ",". $chunk . " ";
            $profile_updates = $db->fetchAll($sql);
            $cli->out("------------------------------------------------------------------------");

            foreach ($profile_updates as $profile_update) {
                $data = unserialize($profile_update->data);
                if (!is_null($data['city_id']) && $data['city_id'] != '' && !in_array($data['city_id'], $c_ids)) {
                    $count += 1;
                    $cli->out('id => '. $profile_update->id . ', city_id => '. $data['city_id']);
                    $cli->out("----------------------------------------------------------------");
                    $db->insert('profile_updates_v2_fix_city_ids', array(
                        'profile_update_id' => $profile_update->id,
                        'city_id' => $data['city_id']
                    ));

                    $profile_cities = array();

                    foreach ($data['cities'] as $city) {
                        if (in_array($city['city_id'], $c_ids)) {
                            $profile_cities[] = $city;
                        }
                    }

                    $data['cities'] = $profile_cities;
                    $data['city_id'] = null;

                    $db->update('profile_updates_v2', array(
                        'data' => serialize($data)
                    ), $db->quoteInto('id = ?', $profile_update->id));

                    $escort_city = $db->fetchOne("SELECT city_id FROM escorts WHERE id = ?", array($profile_update->escort_id));
                    if (!is_null($escort_city) && $escort_city !== '' && !in_array($escort_city, $c_ids)) {
                        $db->update('escorts', array('city_id' => null), $db->quoteInto('id = ?', $profile_update->escort_id));
                    }
                }
            }
        } while ($profile_updates);

        $cli->out('Count of all profiles which '. $count);
        die;
    }

     public function fixCitiesAction(){
     	$cli = new Cubix_Cli();
        $cli->clear();

        $db = $this->_db;
        $escorts = $db->fetchAll("select id from escorts where city_id is null");
        $count = 0;
        foreach($escorts as $escort){
        	
        	$profile_updates = $db->fetchROW("select * from profile_updates_v2 where escort_id = ".$escort->id." and status = 2 order by revision desc limit 1");
        	
        	if($profile_updates){
        		
        		$data = unserialize($profile_updates->data);
        		if($data){
        			if (!is_null($data['city_id']) && $data['city_id'] != '') {
        				$count++;
        				$cli->out('escort id => '. $escort->id . ', city_id => '. $data['city_id']);
        				$db->update('escorts', array('city_id' => $data['city_id']), $db->quoteInto('id = ?',$escort->id));
        			}
        			
        		}
        		
        	}
        	
        	
        }

        $cli->out("end - ".$count); die;
       
     }

    public function unreadMessagesNoticeAction()
    {
        if (Cubix_Application::getId() == APP_BL) {
            $sql = 'SELECT u.email, u.username, cm.date message_date, cm.user_id, cm.id message_id
                    FROM chat_messages cm
                    INNER JOIN users u ON u.id = cm.user_id
                    WHERE cm.read_date IS NULL AND cm.`date` <= DATE_SUB(CURDATE(), INTERVAL 24 HOUR)
                    LIMIT ?, ?';

            $start = 0;
            $limit = 500;

            do {
                $user_with_unread_messages = $this->_db->fetchAll($sql, array($start++ * $limit, $limit));
                foreach ($user_with_unread_messages as $user_unread_msg) {
                    $message_sql = "SELECT true 
                                    FROM unread_chat_messages_notify 
                                    WHERE user_id = ? AND chat_message_id = ? AND UNIX_TIMESTAMP(`notified_date`) <= DATE_SUB(CURDATE(), INTERVAL 3 DAY) 
                                    GROUP BY user_id 
                                    ORDER BY id DESC";
                    $user_last_notified_msg = $this->_db->fetchOne($message_sql, array($user_unread_msg->user_id, $user_unread_msg->message_id));

                    if (!$user_last_notified_msg) {
                        $this->_db->insert('unread_chat_messages_notify',
                            array(
                                'user_id' => $user_unread_msg->user_id,
                                'chat_message_id' => $user_unread_msg->message_id,
                                'notified_date' => date('Y-m-d H:i:s')
                            )
                        );

                        Cubix_Email::sendTemplate('unread_message', $user_unread_msg->email, array(
                            'showname' => $user_unread_msg->username,
                            'url' => Cubix_Application::getWebSiteUrl() . '/private-v2'
                        ));
                    }
                }
            } while ($user_with_unread_messages);
        }
    }
}
