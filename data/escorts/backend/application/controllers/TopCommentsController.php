<?php

class TopCommentsController extends Zend_Controller_Action
{
	protected $model;
	protected $defines;

	public function init()
	{
		$this->model = new Model_TopComments();
	}

	public function indexAction()
	{

	}

	public function dataAction()
	{
		$req = $this->_request;
        
		$filter = array(
			'showname' => $req->showname, 
			'escort_id' => $req->escort_id, 
			'username' => $req->username, 
			'status' => $req->status, 
			'type' => $req->type, 
			'application_id' => $req->application_id 
		);
        
		$data = $this->model->getAll(
			$req->page,
			$req->per_page,
			$filter,
			$req->sort_field,
			$req->sort_dir,
			$count
		);
		
		foreach ( $data as $i => $item ) 
		{
			$type = $item->type;
			
			$data[$i]->type = $item->type == 1 ? 'Public' : 'Private';
			
			if ($type == 2)
				$data[$i]->status = '-';
			else
				$data[$i]->status = $item->comment_status == 1 ? 'Pending' : ($item->comment_status == 2 ? 'Show' : 'Hidden');
		}

		die(json_encode(array(
			'data' => $data,
			'count' => $count
		)));
	}

	public function viewAction()
	{
		$id = $this->_request->id;
		
		$this->view->item = $item = $this->model->get($id);
	}
	
	public function setStatusAction()
	{
		$id = $this->_request->id;
		$status = $this->_request->status;
		
		if (is_array($id))
		{
			foreach ($id as $i)
			{
				$this->model->update($i, $status);
			}
		}
		else
			$this->model->update($id, $status);
	
		die;
	}
}
