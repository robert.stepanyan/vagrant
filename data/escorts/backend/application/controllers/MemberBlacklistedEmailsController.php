<?php

class MemberBlacklistedEmailsController extends Zend_Controller_Action
{
	protected $model;

	public function init()
	{
		$this->model = new Model_MemberBlacklistedEmails();
	}

	public function indexAction()
	{

	}

	public function dataAction()
	{
		$req = $this->_request;

		$filter = array('name' => $req->name );
        
		$data = $this->model->getAll(
			$req->page,
			$req->per_page,
			$filter,
			$req->sort_field,
			$req->sort_dir,
			$count
		);

		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

	public function deleteAction()
	{
      
        if($this->_request->isPost()){
            $ids = $this->_request->getParam('id');
            if(count($ids) > 0 && $ids){
                foreach( $ids as $id ) {
                    $this->model->delete($id);
                }
            }
        }
        exit;
	}

    public function createAction(){
        $this->view->layout()->disableLayout();

        if($this->_request->isPost()){
			
			$validator = new Cubix_Validator();
			
			$data = array(
					'name' => trim($this->_request->getParam('name')),
					'block_date' => new Zend_Db_Expr('NOW()')
				);
			
			if ( $this->model->wordExists($data['name']) ) {
				$validator->setError('name', 'this word already exists');
			}

			if ( $validator->isValid() ) {
				
				$this->model->save($data);
			}
			die(json_encode($validator->getStatus()));
        }        
    }

    public function editAction(){
        $this->view->layout()->disableLayout();
		
        $id = $this->_request->getParam('id');
        $this->view->blockedWork = $this->model->get($id);
        
        if($this->_request->isPost()){
			$validator = new Cubix_Validator();
            $data = array(
				'name' => trim($this->_request->getParam('name'))
			);
            if ( $this->model->wordExists($data['name'],$id) ) {
				$validator->setError('name', 'this word already exists');
			}

			if ( $validator->isValid() ) {
				
				$this->model->save($data,$id);
			}
			die(json_encode($validator->getStatus()));
        }

        $this->view->id = $id;
    }

    
}
