<?php

class PaymentsController extends Zend_Controller_Action {
	public function init()
	{
		$this->model = new Model_Payments();
	}
	
	public function indexAction() 
	{
		$this->view->statuses = array(
			'' => 'ALL' ,
			'1' => 'Recurring' ,
			'0' => 'No Recurring'
		);
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		$success_statuses = Cubix_CGP_Callback::$success_statuses;
		$rec_statuses =  Cubix_CGP_Callback::$recurring_statuses;
		$filter = array(
			'member_name' => $req->member_name,
			'is_rec' => $req->is_rec
		);
		
		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter,
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		foreach ( $data as $i => $item ) {
			$data[$i]->status_success = in_array($item->status, $success_statuses) ? 1 : 0;
			$data[$i]->status_rec = $item->amount == 995 ? 1 : 0;
		}
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
		//var_dump($this->view->data); die;
		
	
	}
	public function detailsAction()
	{
		$details = $this->model->getRawByID($this->_request->id);
		$this->view->details = $details;
		
	}
}
