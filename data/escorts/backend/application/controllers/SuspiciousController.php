<?php

class SuspiciousController extends Zend_Controller_Action {

	public function init()
	{
		$this->model = new Model_Suspicious();
	}
	
	public function indexAction() 
	{
		
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'showname' => $req->showname,
			'escort_id' => $req->escort_id,
			'status' => $req->status,
			'type' => $req->type,
			'application_id' => $req->application_id
		);
		
		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		$defines = Zend_Registry::get('defines');
			

		foreach ($data as $i => $item)
		{
			$data[$i]->status = $defines['suspicious_photos_requests'][$item->status];
			

			if ( Cubix_Application::getId() == APP_ED ) {
				if($data[$i]->type == 1){
					$data[$i]->type = 'Bad Phone Number';	
				}elseif ($data[$i]->type == 2) {
					$data[$i]->type = 'Other problem';
				}else{
					$data[$i]->type = 'Photo';
				}
			}

			$e_s = new Cubix_EscortStatus($item->escort_id);
			$data[$i]->escort_status = $e_s->getStatusText($item->escort_status);
			$data[$i]->verified_status = $this->model->getEscortVerifiedStatus($item->escort_id);
		}
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
	
	public function viewAction()
	{
		$id = $this->_request->id;
		
		$this->view->data = $this->model->get($id);	
	}
	
	public function setStatusAction()
	{
		$id = intval($this->_request->id);
		$status = intval($this->_request->status);
		
		$this->model->setStatus($id, $status);
				
		die;
	}
}
