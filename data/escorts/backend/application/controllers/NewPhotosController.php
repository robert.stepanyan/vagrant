<?php

class NewPhotosController extends Zend_Controller_Action
{
	private $model ;
	protected $user;
	public function init()
	{		
		$this->model = new Model_NewPhotos();
		$this->user = Zend_Auth::getInstance()->getIdentity();
	}
	

	public function indexAction()
	{	
	}
	public function dataAction()
	{
		$this->view->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender();
		$req = $this->_request;

		
		$sort = isset($req->sort_field)?$req->sort_field:'creation_date';
		$dir = isset($req->sort_dir) && $sort!='' && in_array($req->sort_dir,array('asc','desc'))?$req->sort_dir:'DESC';
		$page = isset($req->page) && is_numeric($req->page)?intval($req->page):1;
		$per_page = isset($req->per_page) && is_numeric($req->per_page)?intval($req->per_page):10;	
		echo json_encode($this->model->GetlastPhoto($req->id,$req->showname,$req->select_date_from,$req->select_date_to,$page,$per_page,$sort,$dir));
	}
	
	public function editAction()
	{
		$this->view->layout()->disableLayout();
	    $this->_helper->viewRenderer->setRender('edit');
		$req = $this->_request;
		if(isset($req->id) && is_numeric($req->id))
		{	
			$escort = $this->model->get($req->id);
			$count = count($escort);
			$photos = array('public' => array(), 'private' => array());
			$this->view->date_from = strtotime($req->date_from);
			$this->view->date_to = strtotime($req->date_to); 
			for( $i=0;$i<$count;$i++)
			{
				if ( $escort[$i]->type == ESCORT_PHOTO_TYPE_PRIVATE ) {
					$photos['private'][] = new Model_Escort_PhotoItem($escort[$i]);
				} elseif ( $escort[$i]->type != ESCORT_PHOTO_TYPE_DISABLED ) {
					$photos['public'][] = new Model_Escort_PhotoItem($escort[$i]);
				}
			}
			$this->view->escort = $photos;
		}
	}
	
	public function  disableAction()
	{			 
	    $this->_helper->layout()->disableLayout(); 
		$this->_helper->viewRenderer->setNoRender(true);
		$req = $this->_request;
		if(isset($req->photos) && is_array($req->photos) &&!empty($req->photos))
		{		
			$this->model->disable($req->photos);
			echo 'ok';
				
		}
		else
		{
			die;
		}
	}
	
	
}

