<?php

class AlertmeController extends Zend_Controller_Action {
	public function init()
	{
		$this->model = new Model_Alertme();
	}
	
	public function indexAction() 
	{
		
	}
	
	public function dataAction()
	{
		$req = $this->_request;
				
		$filter = array(
			'username' => $req->username,
			'showname' => $req->showname
		);
		
		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter,
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count,
			$dif_members_count
		);
		
		$DEFINITIONS = Zend_Registry::get('defines');
				
		foreach ( $data as $i => $item ) {
			$arr = $this->model->getEvents($item->user_id, $item->escort_id);
			
			if (in_array(1, $arr) && in_array(2, $arr) && in_array(3, $arr) && in_array(4, $arr))
				$arr = array(5);
			
			$events = array();
			$str = '<ul style="padding: 0 10px">';
			
			foreach ($arr as $ar)
			{
				$str .= '<li>' . $DEFINITIONS['alert_me'][$ar] . '</li>';
			}
			
			$str .= '</ul>';
			
			$data[$i]->event = $str;
		}
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count,
			'dif_members_count' => $dif_members_count
		);	
	}
}
