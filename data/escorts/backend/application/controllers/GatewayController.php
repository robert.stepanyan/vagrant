<?php

class GatewayController extends Zend_Controller_Action
{
	private $_known_amounts = array(995 => array(true, 30), 8995 => array(false, 365));

	public function callbackAction()
	{
		$this->view->layout()->disableLayout();
		
		$r = $this->_request;

		$callback = new Cubix_CGP_Callback((object) array(
			'transaction' => (object) array(
				'id' => (int) $r->transaction_id,
				'fee' => $r->transaction_fee,
				'fee_bank' => $r->transaction_fee_bank,
				'fee_cgp' => $r->transaction_fee_cgp
			),

			'customer' => (object) array(
				'firstname' => $r->customer_firstname,
				'lastname' => $r->customer_lastname,
				'company' => $r->customer_company,
				'address' => $r->customer_address,
				'zipcode' => $r->customer_zipcode,
				'countrycode' => $r->customer_countrycode,
				'phonenumber' => $r->customer_phonenumber,
				'email' => $r->customer_email,
				'ip_address' => $r->customer_ip_address
			),

			'is_test' => (bool) $r->is_test,
			'ref' => $r->ref,
			'status' => (int) $r->status,
			'currency' => $r->currency,
			'amount' => (int) $r->amount,
			'billing_option' => $r->billin_option,

			'card_type' => $r->card_type,
			'cardnumber_masked' => $r->cardnumber_masked,
			'card_expirydate' => $r->card_expirydate,
			'threed_secure' => $r->threed_secure,
			'hash' => $r->hash
		));

		try {
			$callback->registerHook('validate', array($this, 'handleValidate'));
			$callback->registerHook('success', array($this, 'handleSuccess'));
			$callback->registerHook('fail', array($this, 'handleFail'));

			$callback->handle();
		}
		catch ( Exception $e ) {
			$callback->setError($e);
			echo $callback->get('transaction')->id . '.' . $callback->get('status');
			return false;
		}

		echo $callback->get('transaction')->id . '.' . $callback->get('status');
		return true;
	}

	public function handleSuccess($callback)
	{
		$ref = $callback->get('ref');

		list($is_recurring, $days) = $this->_known_amounts[(int) $callback->get('amount')];
		$date = strtotime('+' . $days . ' days');
		Model_Members::setPremiumByRef($ref, $date, $is_recurring);
		
		Model_Members::logSuccess(
			$callback->get('ref'),
			$callback->getId(),
			'Successful transaction with status ' . $callback->get('status')
		);

		return true;
	}

	public function handleFail($callback)
	{ 
		Model_Members::logFail(
			$callback->get('ref'),
			$callback->getId(),
			'Unsuccessful transaction with status ' . $callback->get('status')
		);
		Model_Members::incrementFailedRecurring($callback->get('ref'));
		
	}

	public function handleValidate($callback)
	{
		$amounts = array_keys($this->_known_amounts);
		if ( ! in_array((int) $callback->get('amount'), $amounts) ) {
			throw new Cubix_CGP_Callback_Exception_AmountUnknown("It is " . $callback->get('amount') . ', but must be one of ' . implode(', ', $amounts));
		}

		if ( Cubix_CGP::getCurrency() != $callback->get('currency') ) {
			throw new Cubix_CGP_Callback_Exception_CurrencyInvalid('It is ' . $callback->get('currency') . ' instead of ' . Cubix_CGP::getCurrency());
		}
	}
}