<?php

class SettingsController extends Zend_Controller_Action
{
	
	
	private  $model;
	private $session;
	
	public function init()
	{	
		$this->model = new Model_Settings();
		$this->session = Zend_Auth::getInstance()->getIdentity();
		if(!$this->_request->isXmlHttpRequest)
		{	
			$this->view->private_msg_count = Model_Settings::GetNewMsgCount($this->session->id);
			if($this->session->type!='superadmin')
			$this->view->penalty_count = Model_Settings::GetNewPenaltyCount($this->session->id);
		}

	}
	
	public function getNoticeAction()
	{	$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		if(!$this->_request->isXmlHttpRequest)
		{	
			$private_msg_count = Model_Settings::GetNewMsgCount($this->session->id);
			if($private_msg_count>0)
				echo $private_msg_count;
			elseif($this->session->type!='superadmin')
			{
				echo Model_Settings::GetNewPenaltyCount($this->session->id);
			}
		
				
		}
	}

	public function indexAction()
	{	

	
	}
	
	/* ChangePass Actions */
	
	public function changePassViewAction()
	{
		$this->view->layout()->disableLayout();
	}
	public function changePassAction()
	{	$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		if($this->_request->isXmlHttpRequest() && $this->_request->isPost())
		{	$current_pass = md5(trim($this->_request->current_pass));
			if($this->session->password != $current_pass)
				die('3');
	
			$new_pass=trim($this->_request->new_pass);
			$rep_pass = trim($this->_request->repeat_pass);
			if($new_pass!=$rep_pass)
				die('2');
			$new_pass=md5($new_pass);
			if($current_pass!=$new_pass)
			$affected = $this->model->ChangePass($new_pass,$this->session->id);
			else 
				die('1');
			if($affected==1)
			$this->session->password = $new_pass;
			echo $affected;
		}
	}
	
	public function skypeStatusAction()
	{	
		$this->view->layout()->disableLayout();
		
		$this->view->status = $this->model->getSkypeStatus($this->session->id);
		$this->view->statuses = array(
			'online' => array(
				'name' => "I'm online",
				'value' =>  Model_Settings::SKYPE_STATUS_ONLINE
			),
			'away' => array(
				'name' => "I'm away",
				'value' =>  Model_Settings::SKYPE_STATUS_AWAY
			),
			'busy' => array(
				'name' => "Do not disturb",
				'value' =>  Model_Settings::SKYPE_STATUS_BUSY
			),
			'offline' => array(
				'name' => "I'm offline",
				'value' =>  Model_Settings::SKYPE_STATUS_OFFLINE
			)
		);
		if($this->_request->isPost())
		{	
			$status = intval($this->_request->status);
			$this->model->saveSkypeStatus($this->session->id, $status);
			die;
		}
	}
	/* Message Actions */
	
	public function newMsgAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$req = $this->_request;
		if($req->isXmlHttpRequest() && $req->isPost() && isset($req->users) && is_numeric($req->users) && $this->session->id!=$req->users && ($msg = trim($req->msg))!='' && ($title = trim($req->title))!='')
		{	$msg = strip_tags($msg);
			echo $this->model->NewMsg($this->session->id,$req->users,$title,$msg);
		}
		else
		die;
	}

	
	public function getMsgAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$req = $this->_request;
		$sort_field = isset($req->sort_field)?$req->sort_field:'date';
		$sort_dir = isset($req->sort_dir)  && in_array($req->sort_dir,array('asc','desc'))?$req->sort_dir:'DESC';
		$page = isset($req->page) && is_numeric($req->page)?intval($req->page):1;
		$per_page = isset($req->per_page) && is_numeric($req->per_page)?intval($req->per_page):10;
		$arr = array();
		$arr['date_from'] = $req->date_filter_from;
		$arr['date_to'] = $req->date_filter_to;
		$arr['type'] = $req->type;
		if(isset($req->unread))
		{	
			$arr['read'] =0;
			$arr['to'] = $this->session->id;
		}
		$result = $this->model->GetMsg($arr,$this->session->id,$page,$per_page,$sort_field,$sort_dir,$req->filter_users,$req->radio);
		$bu_model = new Model_BOUsers();
		$bu_users = array();
		
		
		foreach($bu_model->getBoUsers() as $bu){
			$bu_users[$bu->id] = $bu->username;
		}
		
		foreach ($result['data'] as &$mes){
			$mes->from_name = $mes->from == $this->session->id ? ' You ' : $bu_users[$mes->from];
			$mes->to_name = $mes->to == $this->session->id ? ' Me ' :$bu_users[$mes->to];
		}
		echo json_encode($result);
	}
	
	public function getCurrentMsgAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		if(isset($req->id)&& is_numeric($req->id) && is_numeric($req->from) && is_numeric($req->to))
		{	
			if($req->from == $this->session->id)
				$user_id = $req->to;
			elseif($req->to == $this->session->id)
				$user_id = $req->from;
			elseif($this->session->type == 'superadmin' || $this->session->type == 'admin'){
				$user_id = $req->from;
			}
				//die;
			
			$this->view->current = $this->model->GetCurrentMsg($req->id,$user_id,$this->session->id);
			$this->view->my_id = $this->session->id;
			
		}
	}
	
	public function writeMsgViewAction()
	{
		$this->view->layout()->disableLayout();
		$this->view->users =  Model_Settings::GetUsers($this->session->id);
	}

	
	public function privateMsgAction()
	{		
		$this->view->new_msg_count = Model_Settings::GetNewMsgCount($this->session->id);
		$this->_helper->viewRenderer->setRender('index');
		$this->view->user_types =  Model_Settings::GetTypes();
		$this->view->user_id = $this->session->id;
		$this->view->users =  Model_Settings::GetUsers();
		$this->view->viewName = $this->_getParam("action");
	
	}
	
	public function removeMsgAction()
	{	$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$req = $this->_request;
		if($req->isXmlHttpRequest() && $req->isPost()  && is_numeric($req->id)  && is_numeric($req->from) && is_numeric($req->to))
		{	if($req->from==$this->session->id)
			{
				$from = $this->session->id;
				$to = $req->to;
			}
			elseif($req->to==$this->session->id)
			{
				$to = $this->session->id;
				$from = $req->from;
			}
			else
				die;
			
			echo $this->model->RemoveMsg($req->id,$from,$to,$this->session->id);
		}
	}
	/* Holidays Actions */
	
	public function holidaysAction()
	{	
		$this->view->new_msg_count = Model_Settings::GetNewMsgCount($this->session->id);
		$this->_helper->viewRenderer->setRender('index');
		$this->view->user_types = Model_Settings::GetTypes();
		$this->view->users = Model_Settings::GetUsers();
		$this->view->super =$this->session->type=='superadmin'?1:0;
		$this->view->username = $this->session->username;
		$this->view->user_id = $this->session->id; 
		$this->view->viewName = $this->_getParam("action");
	}
	public function getHollidaysAction()
	{	
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$req = $this->_request;
		$sort_field = isset($req->sort_field)?$req->sort_field:'date';
		$sort_dir = isset($req->sort_dir)  && in_array($req->sort_dir,array('asc','desc'))?$req->sort_dir:'DESC';
		$page = isset($req->page) && is_numeric($req->page)?intval($req->page):1;
		$per_page = isset($req->per_page) && is_numeric($req->per_page)?intval($req->per_page):10;
		$arr = array();
		$arr['backend_user_id'] = $req->filter_users;
		$arr['date_from'] = $req->date_filter_from;
		$arr['date_to'] = $req->date_filter_to;
		$arr['is_approved'] = $req->status;
		$arr['type'] = $req->type;
		die(json_encode($this->model->GetHolidays($arr,$page,$per_page,$sort_field,$sort_dir)));
	}
	public function addHolidayViewAction()
	{	
		$this->view->layout()->disableLayout();
		$this->view->super =$this->session->type==='superadmin'?1:0;
		//if($this->session->type=='superadmin')
		//{
			$this->view->users = Model_Settings::GetUsers();
		//}
	}
	
	public function editAction()
	{	$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setRender('add-holiday-view');
		$this->view->users = Model_Settings::GetUsers();

		if(isset($this->_request->id) && is_numeric($this->_request->id))
		{	$this->view->super =$this->session->type==='superadmin'?1:0;
			$this->view->current = $this->model->GetCurrentHoliday($this->_request->id);
		}
	}
	public function saveHolidayAction()
	{	
			$this->view->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender(true);
			if($this->_request->isXmlHttpRequest() && $this->_request->isPost()	&& ($date_from = trim($this->_request->from))!='' && ($date_to = trim($this->_request->to))!='')
			{	
				$date_from = date('Y-m-d',$date_from);
				$date_to = date('Y-m-d',$date_to);
				$backup_user_id = $this->_request->backup_user_id;
				if (!$backup_user_id) $backup_user_id = null;
				if($date_from>$date_to)
					die('3');
				
				if(isset($this->_request->current) && is_numeric($this->_request->current) && is_numeric($this->_request->cur_user))
				{		
						$result = $this->model->Accept($this->_request->cur_user,$this->_request->current,$date_from,$date_to);
						if($result=='1')
						{
							$email = Model_Settings::GetUserEmail($req->user_id);
							if(isset($email))
							{	
								$msg = ' Your holiday from: '.$date_from.' to: '.$date_to.' has been approved';
								$email_data = array(
									'subject' => 'Holiday',
									'message' => $msg,
									'signature' => ''
								);
								Cubix_Email::sendTemplate('universal_message', $email, $email_data,$this->session->email);
							}
						}
						die("$result");
				}
				elseif($this->session->type=='superadmin')
				{
					if(!isset($this->_request->users) || ($user =  trim($this->_request->users))=='')
					die('2');
					$approve = 1;
				}
				else
				{
					$approve = 0;
					$user = $this->session->id;
				}
				$result = $this->model->SaveHoliday($user,$date_from,$date_to,$approve, $backup_user_id);
				if($approve=='1' && $result=='1')
				{
					$email = Model_Settings::GetUserEmail($req->user_id);
					if(isset($email))
					{	
						$msg = ' Holliday has been added from: '.$date_from.' to: '.$date_to;
						$email_data = array(
							'subject' => 'Holiday',
							'message' => $msg,
							'signature' => ''
						);
						Cubix_Email::sendTemplate('universal_message', $email, $email_data,$this->session->email);
					}
				}
				
				die("$result");
			}
	}
	public function toggleActiveAction()
	{	
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		if($this->_request->isXmlHttpRequest() && isset($this->_request->id) && is_numeric($this->_request->id) && $this->session->type=='superadmin')
		{
			$result = $this->model->Active($this->_request->id, $this->_request->disable);
			if($result=='1')
			{	$email = Model_Settings::GetUserEmail($req->user_id);
				$date_from = date('Y-m-d',$req->date_from);
				$date_to = date('Y-m-d',$req->date_to);
				if(isset($email))
				{	$status = $this->_request->disable=='1'?'approved':'denied';
					$msg = ' Your holiday from: '.$date_from.' to: '.$date_to.' has beeen '.$status;
					$email_data = array(
						'subject' => 'Holiday',
						'message' => $msg,
						'signature' => ''
					);
					Cubix_Email::sendTemplate('universal_message', $email, $email_data,$this->session->email);
				}
			}
		}
		else
		{
			die;
		}
	}
	public function removeHolidayAction()
	{	$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		if($this->_request->isXmlHttpRequest() && isset($this->_request->id) && is_numeric($this->_request->id))
		{
			$user_id = $this->session->type!='superadmin'?$this->session->id:0;
			$result =  $this->model->RemoveHoliday($this->_request->id,$user_id);
			if($result=='1')
			{
				$email = Model_Settings::GetUserEmail($user_id);
				if(isset($email))
				{	
					$msg =' Your holiday from: '.$date_from.' to: '.$date_to.' has beeen denied';
					$email_data = array(
						'subject' => 'Holiday',
						'message' => $msg,
						'signature' => ''
					);
					Cubix_Email::sendTemplate('universal_message', $email, $email_data,$this->session->email);
				}
				
			}
			die("$result");
		}
	}
	/* Penality Actions */
	
	public function penaltyAction()
	{
		$this->view->viewName = $this->_getParam("action");
		$this->_helper->viewRenderer->setRender('index');
		$this->view->user_types = Model_Settings::GetTypes(true);
		$this->view->users = Model_Settings::GetUsers($this->session->id);
		$this->view->admin = $this->session->type==='superadmin' || $this->session->type==='admin' ?1:0;
		$this->view->super =$this->session->type==='superadmin'?1:0;
		
	}
	
	public function getReasonTplAction()
	{	
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$req = $this->_request;
		if($this->session->type=='superadmin' && is_numeric($req->id))
		{
			die(json_encode($this->model->GetTpl($req->id)));
		}
	}
	public function getPenaltyAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$req = $this->_request;
		$sort_field = isset($req->sort_field)?$req->sort_field:'date';
		$sort_dir = isset($req->sort_dir)  && in_array($req->sort_dir,array('asc','desc'))?$req->sort_dir:'DESC';
		$page = isset($req->page) && is_numeric($req->page)?intval($req->page):1;
		$per_page = isset($req->per_page) && is_numeric($req->per_page)?intval($req->per_page):10;
		$arr = array();
		$arr['backend_user_id'] = $req->filter_users;
		$arr['type'] = $req->type;
		$bu_id =$this->session->type=='superadmin' || $this->session->type=='admin'?NULL:$this->session->id;
		$result = $this->model->GetPenalty($arr,$bu_id,$page,$per_page,$sort_field,$sort_dir);
		$data = array('count'=>$result['count'],'data'=>$result['total'],'last_month'=>array(),'this_month'=>array());
		$lastMonth = date("Y-m-d", strtotime("-1 month"));
		foreach ($result['data'] as &$v)
		{	$v->date = date("Y-m-d",strtotime($v->date));

			if($lastMonth>$v->date)
			{
				$data['last_month'][] = $v;
			}
			else
				$data['this_month'][] = $v;
		}
		
		echo json_encode($data);
	}
	
	public function newPenaltyViewAction()
	{
		$this->view->layout()->disableLayout();
		if($this->session->type=='superadmin')
		{
			$this->view->users = Model_Settings::GetUsers($this->session->id);
			$this->view->penalty_types = Model_Settings::GetPenaltyTypes();
		}
	    else 
		die;	
	}
	public function newPenaltyAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$req = $this->_request;
		if($this->session->type=='superadmin' && is_numeric($req->users) && is_numeric($req->set_penalty) && is_numeric($req->pen_escort_id) && ($reason=trim($req->reason))!='')
		{	
			$reason_name = isset($req->save_tpl) && trim($req->reason_title)!=''?$req->reason_title:NULL;
			$reason = strip_tags($reason);
		
			$from_escort = isset($req->from_escort)?false:true;
			$result = $this->model->SavePenalty($req->users,$req->pen_escort_id,$req->set_penalty,$reason,$reason_name,$from_escort);
			if($result=='0')
			{
				
				$email = Model_Settings::GetUserEmail($req->users);
				if(isset($email))
				{	$msg = ' You have new penalty '.$req->set_penalty.' &#8364; for this reason: '.$reason.'(Escort ID #'.$req->pen_escort_id.' )';
					$email_data = array(
						'subject' => 'Penalty',
						'message' => $msg,
						'signature' => ''
					);
					Cubix_Email::sendTemplate('universal_message', $email, $email_data, $this->session->email);
				}
			}
			die("$result");
		}
		else
			die;
	}
	public function getCurrentViewAction()
	{	
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		if(isset($req->id) && is_numeric($req->id) && $req->read==0 && $req->user_id==$this->session->id)
		$this->view->current = $this->model->SetRead($req->id,$this->session->id);
	}
	public function getUserPenaltyViewAction()
	{	
		$this->view->layout()->disableLayout();
		$this->view->penalty_types = Model_Settings::GetPenaltyTypes();
	}
	public function getUserPenaltyAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$req = $this->_request;
		if(isset($req->user_id) && is_numeric($req->user_id))
		{
			$sort_field = isset($req->sort_field)?$req->sort_field:'date';
			$sort_dir = isset($req->sort_dir)  && in_array($req->sort_dir,array('asc','desc'))?$req->sort_dir:'DESC';
			$page = isset($req->page) && is_numeric($req->page)?intval($req->page):1;
			$per_page = isset($req->per_page) && is_numeric($req->per_page)?intval($req->per_page):10;
			$arr = array();
			$arr['backend_user_id'] = $req->user_id;
			$arr['escort_id'] = $req->filter_escort_id;
			$arr['date_from'] = $req->date_filter_from;
			$arr['date_to'] = $req->date_filter_to;
			$arr['penalty'] = $req->penalty;
			$arr['reason_name'] = $req->reason_name;
			$arr['status'] = $req->status;
			$arr['showname'] = $req->filter_escort_showname;
			die(json_encode($this->model->GetCurrentPenalty($arr,$page,$per_page,$sort_field,$sort_dir)));
		}
	}
	public function getCurrentPenaltyAction()
	{
		$this->view->layout()->disableLayout();

	}


	public function removePenaltyAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$req = $this->_request;
		if($this->session->type=='superadmin' && isset($req->id) && is_numeric($req->id))
		{
			echo $this->model->RemovePenalty($req->id);
		}
	}
}
