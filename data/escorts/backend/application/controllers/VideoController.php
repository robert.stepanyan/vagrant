<?php

class VideoController extends Zend_Controller_Action
{
	private $model ;
	protected $user;
	public $config;
	public function init()
	{	
		$this->model = new Model_Video();
		$this->user = Zend_Auth::getInstance()->getIdentity();
		$this->config = Zend_Registry::get('videos_config');
		$this->view->config = $this->config;
	}
	
	public function indexAction()
	{	
		$this->view->host =  Cubix_Application::getById(Cubix_Application::getId())->host;
	}
	
	public function dataAction()
	{
		$host = Cubix_Application::getById(Cubix_Application::getId())->host;

		$this->view->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender();
		$req = $this->_request;
		$sort = isset($req->sort_field)?$req->sort_field:'date';
		$dir = isset($req->sort_dir) && $sort!='' && in_array($req->sort_dir,array('asc','desc'))?$req->sort_dir:'DESC';
		$page = isset($req->page) && is_numeric($req->page)?intval($req->page):1;
		$per_page = isset($req->per_page) && is_numeric($req->per_page)?intval($req->per_page):10;	
		$this->view->status = $req->select_status;
		if (Cubix_Application::getId() == APP_EF) {
			$this->view->cover_status = $req->cover_status;
		}

		$result = $this->model->GetEscortVideo(
			$req->id, 
			$req->select_date_from, 
			$req->select_date_to, 
			$page, 
			$per_page, 
			$sort, 
			$dir, 
			$req->select_status,
			Cubix_Application::getId() == APP_EF?$req->cover_status:null
		);

		if (Cubix_Application::getId() == APP_A6) {
			foreach ($result['data'] as &$d) {
				$d->video_link = $this->config['Media'] . $host . '/' . $d->escort_id  .'/'. $d->video . '_' . $d->height . 'p.mp4';
			}
		}


		die(json_encode($result));
	}
	
	public function editAction()
	{
		$this->view->layout()->disableLayout();
	    $this->_helper->viewRenderer->setRender('edit');
	
		$req = $this->_request;

		if(isset($req->video_id) && is_numeric($req->video_id))
		{	
			$this->view->video	= $this->model->editVideo(null, $req->video_id);
			
			$status = Model_Video::GetVideoStatusCount($req->escort_id);
			$video = new Cubix_ParseVideo(null,$req->id,$this->config);
			$this->view->hide = $video->CheckVideoStatus($status) !== false ? 1 : 0;
			$this->view->escort_id = $req->_escort_id;
		}
	}
		
	public function removeAction()
	{	
		$this->_helper->layout()->disableLayout(); 
		$this->_helper->viewRenderer->setNoRender(true);
		if(isset($_POST['check']) && is_array($_POST['check']) &&!empty($_POST['check'])  &&  isset($_POST['escort_id']))
		{			
			$this->model->Remove($_POST['escort_id'],$_POST['check']);
		}
		die('1');
	}	
		
	public function  setStatusAction()
	{			 
	    $this->_helper->layout()->disableLayout(); 
		$this->_helper->viewRenderer->setNoRender(true);
		$app_id = Cubix_Application::getId();
		
		
		if(isset($_POST['check']) && is_array($_POST['check']) &&!empty($_POST['check']) && isset($_POST['approve']) && isset($_POST['escort_id']))
		{		
			if(is_numeric($_POST['approve']))
			{	
				$escorts = new Model_Escorts();
				$escort =  $escorts->get($_POST['escort_id']);
				
				if($_POST['approve'] == 0){
					$approve = 'disable';
					if($app_id == APP_ED){
						Cubix_Email::sendTemplate( 'video_disabled_v1', $escort->email, 
						array('showname' => $escort->showname ) ); 
					}
				}
				else{
					if($app_id == APP_ED){
						Cubix_Email::sendTemplate( 'video_approved_v1', $escort->email, 
							array('showname' => $escort->showname, 'url' => 'http://www.'.Cubix_Application::getById()->host.'/escort/'.$escort->showname.'-'.$escort->id  )
						);
					} 
					
					$video_id = reset($_POST['check']);
					if ($app_id == APP_EF){
                        $approve = 'approve' ;
                        $result = $this->model->setStatus($_POST['check'],$approve,$_POST['escort_id'],$this->config['VideoCount']);
                        ignore_user_abort(true);
                        ob_start();
                        if($result > 0){
                            echo'1';
                        }
                        ob_end_flush();
                        ob_flush();
                        flush();
                        session_write_close();
                        fastcgi_finish_request();
                        $this->getRequest()->setParam('video_id', $video_id);
					    $this->setDimensionsAction();
                        die;
                    }else{
                        $path = str_replace('public','application',$_SERVER['DOCUMENT_ROOT']);
                        exec("{$this->config['bin_path']} $path/cli.php -a default.video.set-dimensions -p $app_id -v $video_id > /dev/null &");
                    }
					
					$approve = 'approve' ;
				}
			}
			else
				$approve = 'pending';
		
			$result = $this->model->setStatus($_POST['check'],$approve,$_POST['escort_id'],$this->config['VideoCount']);
		
			if($result > 0){
				die('1');
			}
		}
	}
	
	public function uploadVideoMp4Action()
	{	
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$escort_id =$this->_request->escort_id;
		$response = array(
			'error' => '',
			'finish' => FALSE
		);

		try {
				
			$video_count = $this->model->GetVideoStatusCount($escort_id);
			$check = Cubix_Videos::CheckVideoStatus($video_count, $this->config['VideoCount']);
			
			if(!$check)
			{	
				$video = new Cubix_Videos($escort_id, $this->config);
				$response = $video->getResponse();


				if($response['finish'] && $response['error'] === 0 ){
					$response['wait_for_video'] = 1;
					ignore_user_abort(true);
					ob_start();
					Cubix_Videos::showResponse($response);
					//header('Connection', 'close');
					//header('Content-Length: '.ob_get_length());
                    ob_end_flush();
                    ob_flush();
                    flush();
                    session_write_close();
                    fastcgi_finish_request();

                    $video->ConvertToMP4();

					$image = $video->SaveImage();
					
					if(is_array($image))
					{	
						if($video_hash = $video->SaveFtpVideoBackend()){
							$data = array(
								'escort_id' => $escort_id,
								'video' => $video_hash,
								'status' => 'approve',
								'height' => $video->max_height,
								'width' => $video->max_width
							);
							
							$video_id = $this->model->SaveVideo($data);
							if(is_numeric($video_id))
							{	
								$count = count($image);
								
								for($i = 0; $i < $count; $i++)
								{	
									
									if(isset($image[$i]['image_id'])){
										$video_img = array();
										$video_img['video_id'] = $video_id;
										$video_img['image_id'] = $image[$i]['image_id'];
										$this->model->SaveVideoImage( $escort_id, $video_img);
									}
								}
								
								$app_id = Cubix_Application::getId();
								if ($app_id == APP_EF){
									$this->getRequest()->setParam('video_id', $video_id);
									$this->setDimensionsAction();
								}else{

									$path = str_replace('public','application',$_SERVER['DOCUMENT_ROOT']);
									exec("{$this->config['bin_path']} $path/cli.php -a default.video.set-dimensions -p $app_id -v $video_id > /dev/null &");
								}
							}
						}
						else{
							throw new Exception('system error while uploading Video');
							//$response['error'] = Cubix_I18n::translate('sys_error_while_upload');
						}
					}
					else{
						throw new Exception($image);
						//$response['error'] = $image;
					}
				}
			}
			else{
				$response['error'] = $check;
			}
		} catch ( Exception $e ) {
			$response['error'] = $e->getMessage();
		}
		Cubix_Videos::showResponse($response);
		die;
	}

	public function  setCoverStatusAction()
	{			 
	    $this->_helper->layout()->disableLayout(); 
		$this->_helper->viewRenderer->setNoRender(true);
		$app_id = Cubix_Application::getId();

		if(isset($_POST['type']) && isset($_POST['escort_id']) && isset($_POST['video_id']))
		{	
			$escort_id = $_POST['escort_id'];
			$video_id = $_POST['video_id'];
			$type = $_POST['type'];

			if($type == 'approve') {	
				$cover_status = Model_Video::COVER_STATUS_VERIFIED;
			} elseif($type == 'reject') {
				$cover_status = Model_Video::COVER_STATUS_REJECTED;
			} 
		
			$result = $this->model->setCoverStatus($video_id, $escort_id, $cover_status);

			if($result > 0){
				die('1');
			}
		}
	}

	public function removeCoverAction()
	{	
		$this->_helper->layout()->disableLayout(); 
		$this->_helper->viewRenderer->setNoRender(true);
		if(isset($_POST['video_id']) && isset($_POST['escort_id']))
		{		
			$escort_id = $_POST['escort_id'];
			$video_id = $_POST['video_id'];	
			$this->model->removeCover($video_id, $escort_id);
		}
		die('1');
	}

	public function uploadCoverAction()
	{	
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$config =  Zend_Registry::get('images_config');	
		$escort_id = $this->_request->escort_id;
		$video_id = $this->_request->video_id;

		try {
			$response = Model_Video::HTML5_upload_cover();

			if($response['error'] == 0 && $response['finish']){
				$error = false;
				$extension = end(explode(".", $response['name']));  

				if (!in_array($extension, $config['allowedExts']  ) )
				{
				    $error = ' Not allowed file extension.';
				}

				if ($error) {
					echo json_encode(array(
						'finish' => 0,
						'error' => $error
					));
					die;
				} else {
				    // Save on remote storage

				    $hash = md5(microtime(TRUE) * mt_rand(1, 1000000)); 

				    $image_data = array(
				    	'cover_hash' => $hash,
				    	'cover_ext' => $extension,
				    	'cover_status' => Model_Video::COVER_STATUS_PENDING,
				    	'escort_id' => $escort_id,
				    	'video_id' => $video_id
				    );					

				    $images = new Cubix_ImagesCommon();
				    $images->_storeToCoverPicServer($image_data, $response['tmp_name']);

				    $this->model->saveCover($image_data);

				    $app_id = Cubix_Application::getId();
				    $cover_arr = array(
				    	'hash' => $hash, 
				    	'ext' => $extension,
				    	'application_id' => $app_id
				    );
				    $cover = new Cubix_ImagesCommonItem($cover_arr);

				    echo json_encode(array(
				    	'finish' => 1,
				    	'cover_url' => $cover->getUrl('m320'),
				    	'cover_orig_url' => $cover->getUrl('orig')
				    ));
				    die;
				}
			} 
			$this->model->showResponse($response);
			die;
		} catch ( Exception $e ) {
			echo json_encode(array(
				'finish' => 0,
				'error' => $e->getMessage()
			));
			die;
		}
	}
	
	public function isVideoReadyAction() 
	{
		$escort_id = $this->_request->escort_id;
		$up_video = $this->model->getPendingVideo($escort_id);
		
		if($up_video){
			$app_id = Cubix_Application::getId();
			$host =  Cubix_Application::getById($app_id)->host;
			$video_image = array(
				'application_id' => $app_id,
				'hash' => $up_video->image_hash,
				'ext' => $up_video->image_ext
			);
			$photo_model = new Cubix_ImagesCommonItem($video_image);
			$image_url = $photo_model->getUrl('m320');
			
			$response['vod'] = $this->config['remote_url'].$host.'/'.$this->escort_id.'/';
			$response['photo_url'] = $image_url;
			$response['video_width'] = $up_video->width;
			$response['video_height'] = $up_video->height;
			$response['video'] = $up_video->video;
			$response['video_id'] = $up_video->id;
			echo json_encode($response);die;
		}
		else{
			echo json_encode(array('status' => 0));die;
		}
		
	}
	
	public function reasonAction()
	{
		
	}
	
	public function rejectAction()
	{
		$status = $this->_getParam('status');
		$escort_id = $this->_getParam('escort_id');
		$request_id = intval($this->_getParam('request_id'));
		$text = $this->_getParam('text');
		$reasons = $this->_getParam('reasons');
		$notify_options = $this->_getParam('notify_options');
		$app_id = Cubix_Application::getId();
		$config = Zend_Registry::get('system_config');
		$originator = Cubix_Application::getPhoneNumber(Cubix_Application::getId());

		$m = new Model_Escorts();
		$esc = $m->get($escort_id);
		//$showname = $esc->showname;
		$email = $esc->email;
		$subject = 'Video rejected';
		$this->model->setStatus($request_id,$status,$escort_id,$this->config['VideoCount']);
		/* send email */
		$email_data = array(
			'subject' => $subject,
			'message' => $text,
			'signature' => '',
			'application_title' => Cubix_Application::getById()->title,
			'escort_name' => $esc->showname,
			'escort_id' => $esc->id,
		);
		
		
		if($app_id == APP_ED){
			 if(is_numeric($esc->agency_id)){
				$email_template = 'video_rejected_agency_v1';
			}else{
				$email_template = 'video_rejected_v1';
			}
			Cubix_Email::sendTemplate( $email_template, $esc->email, 
					array(
						'escort_showname' => $esc->showname,
						'showname' => $esc->showname,
						'agency' => $esc->agency_name,
						'url' => 'http://www.'.Cubix_Application::getById()->host.'/escort/'.$esc->showname.'-'.$esc->id,
						'subject' => $subject,
						'reason' => $text
					)
			); 
			
		}elseif($app_id == APP_EF){
			$show_popup = 0;
			$dictionary = new Model_System_Dictionary();
			$langs = Cubix_Application::getLangs(Cubix_Application::getId());
				if (in_array('popup', $notify_options)) {
					$show_popup = 1;
					$reasons_arr = array();
					foreach ($langs as $key => $lang) {
						foreach($reasons as $reason){
								if($reason != 'video_verify_reject_reason_1'){
									$value = 'value_'.$lang->id;
									$reasons_arr[$lang->id][] = $dictionary->getById($reason)->$value . '<br/><br/>';
								}
						}
						$reasons_arr[$lang->id][] = $text . '<br/><br/>';
					}
					$serialized_reason = serialize($reasons_arr);
					$this->model->setRejectReasons($request_id, $serialized_reason, $show_popup, $escort_id, $this->config['VideoCount']);
				}
				if (in_array('mail', $notify_options)) {
					foreach ($langs as $key => $lang) {
						foreach($reasons as $reason){
								if($reason != 'video_verify_reject_reason_1'){
									$value = 'value_'.$lang->id;
									$email_data['video_verify_reject_reasons_' . $lang->id] = $email_data['video_verify_reject_reasons_' . $lang->id] . '<br/><br/>' .$dictionary->getById($reason)->$value;
								}
						}
						$email_data['video_verify_reject_reasons_' . $lang->id] = $email_data['video_verify_reject_reasons_' . $lang->id] . '<br/><br/>' .$text;
					}
					Cubix_Email::sendTemplate('video_rejected', $email, $email_data);
				}
				if (in_array('sms', $notify_options)) {
							$m2 = new Model_EscortsV2();
							$esc2 = $m2->get($escort_id);
							$phone = $esc2->contact_phone_parsed;
							if(null != $phone){
								$body = "Salve, il suo video su escortforumit.xxx è stato rifiutato. La prego di effettuare l'accesso per scoprire il motivo";
								$model_sms = new Model_SMS_Outbox();
	                            $data['phone_from'] = $originator;
	                            $data['application_id'] = Cubix_Application::getId();
	                            $b_user = Zend_Auth::getInstance()->getIdentity();
	                            $data['sender_sales_person'] = $b_user->id;
	                            $data['phone_number'] = $phone;
	                            $data['text'] = $body;
	                            //
								$sms_config = $config['sms'];
								$SMS_USERKEY = $sms_config['userkey'];
								$SMS_PASSWORD = $sms_config['password'];
								$SMS_DeliveryNotificationURL = $sms_config['DeliveryNotificationURL'];
								$SMS_NonDeliveryNotificationURL = $sms_config['NonDeliveryNotificationURL'];
								$SMS_BufferedNotificationURL = $sms_config['BufferedNotificationURL'];

	                            $model_sms->save($data);

								$sms = new Cubix_SMS($SMS_USERKEY, $SMS_PASSWORD);

								$sms->setOriginator($originator);
								$sms->addRecipient($phone);
								$sms->setBufferedNotificationURL($SMS_BufferedNotificationURL);
								$sms->setDeliveryNotificationURL($SMS_DeliveryNotificationURL);
								$sms->setNonDeliveryNotificationURL($SMS_NonDeliveryNotificationURL);

								$sms->setContent($body);
								$sms->sendSMS();
							}
							
				}
		}else{
			Cubix_Email::sendTemplate('universal_message', $email, $email_data);
		}
		
		die(json_encode(array('status' => 'success')));
		
	}
	
	public function rotateVideoAction()
	{	
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$escort_id = $this->_getParam('escort_id');
		$video_id = $this->_getParam('video_id');
		$side = $this->_getParam('side');
		$video_name = $this->model->getVideoById($video_id);
		$config = Zend_Registry::get('videos_config');
		
		//REMOTE PATH 
		$host = Cubix_Application::getById(Cubix_Application::getId())->host;
		$remote = '/'.$host.'/'.$escort_id. '/'. $video_name;
		
		//LOCAL PATH
		$tmp_dir = sys_get_temp_dir();
		$name = uniqid().'.flv';
		$dir = $tmp_dir.'/escort/';
		if(!is_dir($dir)){
			mkdir($dir);
		}
		$local = $dir.$name;
		
		$video_ftp = new Cubix_VideosCommon();
		$video_ftp->download($local, $remote);
		$video = new Cubix_ParseVideo($local,$escort_id,$config);
		$local = $video->rotate($side);
		
		$image = $video->SaveImage(true);
		if(is_array($image))
		{	
			if(!$video->SaveFtpVideoBackend($local,$name))
			{
				$error = Cubix_I18n::translate('sys error while uploading file');
			}
			else
			{
				$data = array('escort_id' => $escort_id,'video' => $name,'status'=>'approve');
				$r_video_id = $this->model->SaveVideo($data);
				if(is_numeric($r_video_id))
				{	
					$count = count($image);
					for($i = 0; $i < $count; $i++)
					{	
						if(isset($image[$i]['image_id'])){
							$video_img = array();
							$video_img['video_id'] = $r_video_id;
							$video_img['image_id'] = $image[$i]['image_id'];
							$this->model->SaveVideoImage($escort_id,$video_img);
						}
					}
					
					$this->model->deleteById($video_id);
					$photo = array(	'escort_id' => $escort_id,
						'hash' => $image[0]['hash'],
						'width' => $image[0]['width'],
						'ext'=>'jpg',
						'height' => $image[0]['height']);
					$photo = new Cubix_ImagesCommonItem($photo);
					
					$response =array(
						'src'=>$photo->getUrl('m320'),
						'width'=>$image[0]['width'],
						'height' => $image[0]['height'],
						'video' => $name,
						'video_id'=>$r_video_id,
						'finish'=>true,
						'date'=>date('d M Y')
					);
					die(json_encode($response));
				}
			}
		}
		else{
			$error = $image;
		}
		
		die($error);
	}
	
	public function setDimensionsAction()
	{
		$video_id = intval($this->_request->video_id);
		$this->model->setDimensions($video_id);
		die;
	}		
}

