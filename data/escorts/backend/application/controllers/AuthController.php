<?php

class AuthController extends Zend_Controller_Action
{
	/**
	 * @var Zend_Auth
	 */
	protected $_auth;
	
	public function init()
	{
		$this->_auth = Zend_Auth::getInstance();
	}
	
	public function indexAction()
	{
		header('Cubix-redirect: /auth');
		$this->_forward('login');
	}

	public function loginAction()
	{
		$use_light_version = (int) $this->_request->getParam('use_light_version', 0);

		$host = preg_replace('/^(backend|backoffice)./', '', $_SERVER['HTTP_HOST']);
		
		if( APPLICATION_ENV == 'development' ){
			$host = str_replace('.test', '', $host);
		}

		$this->view->application = Cubix_Application::getByHost($host);
		
		if ( $this->_auth->hasIdentity() ) {
			$this->_redirect($this->view->baseUrl() . '/escorts');
		}
		elseif ( $this->_request->isPost() ) {
			$username = strtolower(trim($this->_request->username));
			$password = $this->_request->password;

			if ( strlen($username) !== 0 && strlen($password) !== 0 ) {
				$adapter = new Model_Auth_Adapter($username, $password, $this->view->application->id, $use_light_version);
				$result = $this->_auth->authenticate($adapter);
                
				if( ! $result->isValid() ) {
					/*switch ( $result->getCode() ) {
					case Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID:
						$this->view->errors = 'user credentials not found';
					}*/
					$this->view->error = true;
				} else {
					Model_Activity::getInstance()->log('signin');
                    $sales_id = $result->getIdentity()->getId();
                    Model_BOUsersSessions::setStart($sales_id);
					
					$ses_filter = new Zend_Session_Namespace('back_user');
					$ses_filter->user_type = $result->getIdentity()->type;
					$ses_filter->is_auth = true;
					if (!in_array($result->getIdentity()->type, array('journal manager', 'call center'))){
                        $this->_redirect($this->view->baseUrl() . '/dashboard');
                    }
					elseif($result->getIdentity()->type == 'journal manager'){
                        $this->_redirect($this->view->baseUrl() . '/journal');
                    }elseif ($result->getIdentity()->type == 'call center'){
                        $this->_redirect($this->view->baseUrl() . '/escorts-v2');
                    }
				}
			}
			else {
				$this->view->error = true;
			}
		}
	}

	public function logoutAction()
	{
		if ( $this->_auth->hasIdentity() ) {
			$backend_user = Zend_Auth::getInstance()->getIdentity();

			if ( Cubix_Application::getId() == APP_EF && $backend_user->type == 'sales manager' ) {
				$settings =  new Model_Settings();
				$settings->saveSkypeStatus($backend_user->id,4);
			}

			Model_Activity::getInstance()->log('signout');
			$this->_auth->clearIdentity();
		}
		
		$this->_redirect($this->view->baseUrl() . '/auth');
	}
}
