<?php

function replace___($m)
{
	$id = strtolower($m[1]);
	$id = preg_replace('#[^a-z0-9]#', ' ', $id);
	while ( strpos($id, '  ') !== false ) {
		$id = str_replace('  ', ' ', $id);
	}
	$id = 'service_' . trim(str_replace(' ', '_', $id), '_');

	return 'Cubix_I18n::translate(\'' . $id . '\')';
}

class EscortsController extends Zend_Controller_Action
{
	/**
	 * @var Model_Escorts
	 */
	protected $model;
	
	/**
	 * @var Model_UserItem
	 */
	protected $user;

	public function init()
	{
		// $db = Zend_Registry::get('db');

		// $defines = Zend_Registry::get('defines');
		// $services = $defines['services_flat'];

		// $src = file_get_contents('../../library/Cubix/services.php');
		// $src = preg_replace_callback("#Cubix_I18n::translate\('([^']+)'\)#", 'replace___', $src);
		// file_put_contents('../../library/Cubix/services.php', $src);

		$this->model = new Model_Escorts();
		$this->user = Zend_Auth::getInstance()->getIdentity();
	}
	
	public function indexAction()
	{
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('default_escorts_data');
		$this->view->filter_params = $ses_filter->filter_params;
		/* FILTER PARAMS */
	}
	
	public function changeStatusAction()
	{
		
	}

	public function dataAction()
	{
		$req = $this->_request;

		$bu_user = Zend_Auth::getInstance()->getIdentity();

		if ( $bu_user->type == 'sales manager' || $bu_user->type == 'sales clerk' ) {
			$req->setParam('sales_user_id', $bu_user->id);
		}
		else if ($bu_user->type == 'admin') {
			$req->setParam('application_id', $bu_user->application_id);
		}

		$filter = array(
			// Personal
			'e.id = ?' => $req->escort_id,
			'u.application_id = ?' => $req->application_id,

			'e.showname' => $req->showname,
			'u.username' => $req->username,
			'ep.contact_email' => $req->email,
			'ep.contact_phone' => $req->phone,
			'ep.contact_web' => $req->web,
			'u.last_ip' => $req->last_ip,
			
			'u.sales_user_id = ?' => $req->sales_user_id,
			
			'e.gender = ?' => $req->gender,
			
			'e.country_id = ?' => $req->country,
			'c.region_id = ?' => $req->region,
			'ec.city_id = ?' => $req->city,
			'ecz.city_zone_id = ?' => $req->cityzone,
		
			// Status
			'e.status' => $req->status,
			
			'u.disabled = ?' => $req->disabled,
			'ep.admin_verified = ?' => $req->verified,
		
			'e.verified_status = ?' => $req->only_verified ? Model_Escorts::STATUS_VERIFIED: null,
            'e.verified_status = ?' => $req->not_verified ? Model_EscortsV2::STATUS_NOT_VERIFIED: null,
            'only_city_tour' => $req->only_city_tour,
		
			// Date
			'filter_by' => $req->filter_by,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to,

			'excl_status' => Model_Escorts::ESCORT_STATUS_DELETED
		);

		// Independent and Agency girl filtering
		$escort_type_filter = $this->_getParam('escort_type');
		if ( $escort_type_filter == 'independent' ) {
			$filter[] = 'e.agency_id IS NULL';
		}
		elseif ( $escort_type_filter == 'agency' ) {
			$filter[] = 'e.agency_id IS NOT NULL';
		}

		// Revisions filter fix
		if ( $this->_getParam('sort_field') == 'revs_count' ) {
			$this->_setParam('sort_field', 'COUNT(DISTINCT(pu.revision))');
		}

		$model = new Model_Escorts();
		$data = $model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		$DEFINITIONS = Zend_Registry::get('defines');
		
		foreach ($data as $d)
		{
			$stat = array();
			foreach( $DEFINITIONS['escort_status_options'] as $key => $e_status )
			{
				if ( $this->model->hasStatusBit($d['id'], $key) )
				{
					$stat[] = $e_status;
				}
			}

			$d['status'] = implode(', ', $stat);//$DEFINITIONS['escort_status_options'][$d['status']];

			// $d['main_photo'] = $d->getMainPhoto()->getUrl('agency_p100');
		}
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

    public function  skypeAction(){

        if (in_array(Cubix_Application::getId(), array(APP_EF, APP_A6))){
            $ses_filter = new Zend_Session_Namespace('default_escorts_skype-data');
            $this->view->filter_params = $ses_filter->filter_params;
        }else{
            $this->_redirect($this->view->baseUrl() . '/escorts');
        }


    }

    public function skypeDataAction(){

        if (in_array(Cubix_Application::getId(), array(APP_EF, APP_A6))){
            $req = $this->_request;


            $filter = array(
                'e.showname' => $req->showname,
                'e.id' => $req->escort_id,
                'u.application_id' => $req->application_id
            );

            $model = new Model_Escorts();
            $count = 0;
            $data = $model->getSkypeData(
                $this->_request->page,
                $this->_request->per_page,
                $filter,
                $this->_request->sort_field,
                $this->_request->sort_dir,
                $count
            );

            echo json_encode(array(
                'data' => $data,
                'count' => $count
            ));
            die;
        }else{
            $this->_redirect($this->view->baseUrl() . '/escorts');
        }


    }

    public function skypeDetailsAction(){

        if (in_array(Cubix_Application::getId(), array(APP_EF, APP_A6))){
            $escort_id = $this->_getParam('id' );
            $model = new Model_Escorts();

            $escort_data =  $model->getEscortShowname($escort_id);

            $data =  $model->getSkypeHistoryData($escort_id);
            $this->view->layout()->disableLayout();


            $this->view->escort = $escort_data;
            $this->view->data = $data;
        }else{
            $this->_redirect($this->view->baseUrl() . '/escorts');
        }


    }

    public function skypeEditAction(){

        if (in_array(Cubix_Application::getId(), array(APP_EF, APP_A6))){
            $this->view->layout()->disableLayout();
            $escort_id = $this->_getParam('id' );
            $model = new Model_Escorts();

            if ( $this->_request->isPost() ){
                $update_data = array();
                $update_data['status'] = 1;
                if($this->_request->status == 2){
                    $update_data['status'] = 0;
                }
                $update_data['escort_skype_username'] = $this->_request->escort_skype_username;
                $update_data['paxum_email'] = $this->_request->paxum_email;
                $update_data['price_30'] = $this->_request->price_30;
                $update_data['price_60'] = $this->_request->price_60;
                $update_data['escort_id'] = $escort_id;
                $model->updateEscortSkype($update_data);
                echo json_encode(array('status' => 'success'));die;
            }else{
                $escort_data =  $model->getEscortShowname($escort_id);
                $data =  $model->getEscortSkypeData($escort_id);
                $this->view->escort = $escort_data;
                $this->view->data = $data;
            }
        }else{
            $this->_redirect($this->view->baseUrl() . '/escorts');
        }





    }


	public function vacationAction()
	{
			/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('default_escorts_vacation-data');
		$this->view->filter_params = $ses_filter->filter_params;
		/* FILTER PARAMS */
	}
	
	public function removeVacationAction()
	{
		$model = new Model_Escorts();
		
		$model->removeVacation($this->_request->escort_id);
		
		die;
	}
	
	public function vacationDataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'e.showname' => $req->showname,
			'e.id' => $req->escort_id,
			'u.application_id' => $req->application_id
		);		
		
		$model = new Model_Escorts();
		$count = 0;
		$data = $model->getVacations(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		echo json_encode(array(
			'data' => $data,
			'count' => $count
		));
		die;
	}
	
	protected function _validate($edit_mode = false, &$data = array())
	{
		$escort = new stdClass();
		if ( $edit_mode ) {
			$id = intval($this->_getParam('id'));
			$escort = $this->model->get($id);
		}
		
		$data = new Cubix_Form_Data($this->_request);
		$fields = array(
			// Login Info Tab
			'agency_id' => 'int',
			'country_id' => 'int',
			'application_id' => 'int',
			'sales_user_id' => 'int',
			'username' => '', 
			'password' => '', 
			'showname' => '',
			'status' => 'int',
			
			// Biography Tab
			'ethnicity' => 'int',
			'gender' => 'int',
			'nationality_id' => 'int',
			'birth_date' => 'int', // timestamp
			
			'measure_units' => 'int',
			'height' => 'int',
			'weight' => 'int',
			'bust_waist_hip' => '',
			'hair_color' => 'int',
			'hair_length' => 'int',
			'eye_color' => 'int',
			'shoe_size' => 'int',
			'breast_size' => '',
			'dress_size' => 'int',
			'is_smoker' => 'int',
			
			'characteristics' => 'notags|special',
			'availability' => 'int',
			'sex_availability' => 'arr-int',
			'langs' => '',
			
			// Services Tab
			'svc_kissing' => 'int',
			'svc_blowjob' => 'int',
			'svc_cumshot' => 'int',
			'svc_69' => 'int',
			'svc_anal' => 'int',
			// 'svc_additional' => 'notags|special',
			
			// Working Tab
			'locations' => 'arr-int',
			'base_city_id' => 'int',
			'vac_date_from' => 'date', // timestamp
			'vac_date_to' => 'date', // timestamp
			'work_days' => 'arr-int',
			'work_times_from' => 'arr-int',
			'work_times_to' => 'arr-int',
			'rates' => '',
			
			// Contacts Tab
			'admin_verified' => 'int',
			'contact_phone' => '',
			'phone_instructions' => '',
			'email' => '',
			'contact_zip' => '',
			'web_address' => '',
		
			// Tours Tab
			'tours' => '',

			'pseudo_escort' => ''
		);
		
		if ( $edit_mode ) {
			$fields['id'] = 'int';
		}
		
		$data->setFields($fields);
		$data = $data->getData();
		
		if ( $edit_mode ) {
			$data['application_id'] = $escort->application_id;
		}
		
		$i18n_data = Cubix_I18n::getValues($this->_request->getParams(), 'about', '');
		foreach ( $i18n_data as $field => $value ) {
			$i18n_data[$field] = htmlspecialchars(strip_tags(trim($value)));
		}
		$data = array_merge($data, $i18n_data);

		$i18n_data = Cubix_I18n::getValues($this->_request->getParams(), 'svc_additional', '');
		foreach ( $i18n_data as $field => $value ) {
			$i18n_data[$field] = htmlspecialchars(strip_tags(trim($value)));
		}
		$data = array_merge($data, $i18n_data);


		$validator = new Cubix_Validator();
		
		/* --> Login Info Tab */
		if ( ! $data['country_id'] ) {
			$validator->setError('country_id', 'Required');
		}

		$app = Cubix_Application::getById($this->_request->application_id);

		if ( count($data['locations']) > $app->max_working_cities ) {
			$validator->setError('locations', 'You can add maximum ' . $app->max_working_cities . ' working locations');
		}
		
		/*if ( ! $data['application_id'] && ! $edit_mode ) {
			$validator->setError('application_id', 'Required');
		}*/
		
		if ( ! isset($data['tours']) || ! is_array($data['tours']) ) $data['tours'] = array();
		
		$data['tours_tmp'] = array();
		foreach ( $data['tours'] as $i => $tour ) {
			list($country_id, $city_id, $date_from, $date_to, $phone) = explode(':', $tour);
			$data['tours_tmp'][$i] = array($country_id, $city_id, strtotime(date('d-m-Y', $date_from)), strtotime(date('d-m-Y', $date_to)));
		}
		
		$today = strtotime(date('d-m-Y', time()));
		
		foreach ( $data['tours_tmp'] as $i1 => $tour ) {
			list($country_id, $city_id, $date_from, $date_to, $phone) = $tour;
			
			if ( $date_to < $today ) {
				$validator->setError('tours', 'Some date intervals are invalid or overlapped');
				break;
			}
			
			if ( $date_from >= $date_to ) {
				$validator->setError('tours', 'Some date intervals are invalid or overlapped');
				break;
			}
			
			$found = false;
			
			foreach ( $data['tours_tmp'] as $i2 => $t1 ) {
				if ( $i1 == $i2 ) continue;
				
				list($nil, $nil, $d_f, $d_t, $phone) = $t1;
				
				if ( ($d_f >= $date_from && $d_f < $date_to) || ($d_t > $date_from && $d_t <= $date_to) ) {
					$found = true;
					
					$validator->setError('tours', 'Some date intervals are invalid or overlapped');
					break;
				}
			}
			
			if ( $found ) {
				break;
			}
		}
		
		// $validator->setError('tour_box', 'Required');
		
		if ( ! $data['sales_user_id'] && ! $data['agency_id'] ) {
			$validator->setError('sales_user_id', 'Required');
		}

		if ( $edit_mode )
		{
			if ( $data['status'] )
			{
				if ( $data['status'] == Model_Escorts::ESCORT_STATUS_ACTIVE ) {
					if ( $this->model->hasStatusBit($id, Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS) ) {
						$validator->setError('status', 'You can\'t set Active status.<br/>No enougth photos.');
					}
					elseif ( $this->model->hasStatusBit($id, Model_Escorts::ESCORT_STATUS_NO_PROFILE) ) {
						$validator->setError('status', 'You can\'t set Active status.<br/>Escort has no profile.');
					}
					elseif ( $this->model->hasStatusBit($id, Model_Escorts::ESCORT_STATUS_PROFILE_CHANGED) ) {
						$validator->setError('status', 'You can\'t set Active status.<br/>Escort\'s profile has not approved.');
					}
				}
			}
			else
			{
				$validator->setError('status', 'Required');
			}
		}
		
		if ( ! $data['agency_id'] ) {
			if ( ! strlen($data['username']) ) {
				$validator->setError('username', 'Required');
			}
			elseif ( ($escort->username != $data['username']) && $this->model->existsByUsername($data['username']) ) {
				$validator->setError('username', 'Already exists');
			}
			
			if ( ! $edit_mode && ! strlen($data['password']) ) {
				$validator->setError('password', 'Required');
			}
		}
		
		if ( ! strlen($data['showname']) ) {
			$validator->setError('showname', 'Required');
		}
		elseif ( ! preg_match('/^[-_a-z0-9]+$/i', $data['showname']) ) {
			$validator->setError('showname', 'Allowed only: -, _, a-z, 0-9');
		}
		elseif ( ($escort->showname != $data['showname']) && $this->model->existsByShowname($data['showname']) ) {
			$validator->setError('showname', 'Already exists');
		}
		
		if ( ! is_array($data['sex_availability']) || ! count($data['sex_availability']) ) {
			$validator->setError('sex_availability', 'Required');
		}
		/* <-- */
		
		/* --> Biography Tab */
		if ( ! $data['gender'] ) {
			$validator->setError('gender', 'Required');
		}
		
		if ( ! $data['birth_date'] ) {
			// Commented temporary: $validator->setError('birth_date', 'Required');
		}

		if ( $data['height'] || $data['weight'] )
		{
			if ( ! $data['measure_units'] ) {
				$validator->setError('measure_units', 'Required');
			}
		}
		
		if ( strlen($datap['bust_waist_hip']) && ! preg_match('#^[0-9]+-[0-9]+-[0-9]+$#', $data['bust_waist_hip']) ) {
			$validator->setError('bust_waist_hip', 'Wrong format<br/> (90-60-90)');
		}
		
		if ( ! is_array($data['langs']) || ! count($data['langs']) ) {
			$validator->setError('langs', 'At least one is required');
		}
		/* <-- */
		
		/* --> Working Tab */
		if ( ! $data['locations'] ) {
			$validator->setError('locations', 'At least one is required');
		}
		elseif ( ! $data['base_city_id'] ) {
			$validator->setError('locations', 'Please select the base city below');
		}
		
		$vac_date_from = $data['vac_date_from'];
		$vac_date_to = $data['vac_date_to'];
		
		if ( ($vac_date_from && ! $vac_date_to) || ($vac_date_to && ! $vac_date_from) || ($vac_date_to < $vac_date_from) ) {
			$validator->setError('vac_date', 'Wrong interval');
		}
		
		if ( strlen($data['email']) && Cubix_Application::isDomainBlacklisted($data['email']) ) {
			$validator->setError('email', 'Domain is blacklisted');
		}
		
		if ( strlen($data['web_address']) && Cubix_Application::isDomainBlacklisted($data['web_address']) ) {
			$validator->setError('web_address', 'Domain is blacklisted');
		}
		
		foreach ( $this->view->langs() as $lang)
		{
			if ( strlen($data['about_' . $lang]) && Cubix_Application::isDomainBlacklisted($data['about_' . $lang]) ) {
				$validator->setError('about_' . $lang, 'Domain is blacklisted');
			}
		}
		
		for ( $i = 1; $i <= 7; $i++ ) {
			if ( isset($data['work_days'][$i]) && ( ! @$data['work_times_from'][$i] || ! @$data['work_times_to'][$i] ) ) {
				$validator->setError('work_times_' . $i, '');
			}
		}

		if ( ! $data['availability'] ) {
			$validator->setError('availability', 'Required');
		}

		if ( ! $data['contact_phone'] && ! $data['email'] ) {
			$validator->setError('email_phone_error', 'Email or Phone field is Required');
		}

		if ( ! $data['pseudo_escort'] ) {
			$data['pseudo_escort'] = 0;
		}

		/*if ( ! $data['email'] ) {
			$validator->setError('email', 'Required');
		}*/
		
		/* <-- */
		
		/* --> Contacts Tab */
		// TODO: Validation
		/* <-- */

		return $validator;
	}
	
	public function createAction()
	{
		if ( $this->_request->isPost() ) {
			$data = array();
			$validator = $this->_validate(false, $data);
			
			$escort_id = null;
			
			if ( $validator->isValid() ) {
				$m_saver = new Model_Escort_Saver();
				$escort_id = $m_saver->save(new Model_EscortItem(
					$data
				));

				$this->model->setStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS);
			}
			
			die(json_encode($validator->getStatus() + array('escort_id' => $escort_id)));
		}
	}

	public function editAction()
	{
		$id = intval($this->_getParam('id'));
		
		if ( ! $this->user->hasAccessToEscort($id) ) die('Permission denied');
		
		if ( $this->_request->isPost() ) {
			$data = array();
			$validator = $this->_validate(true, $data);
			
			if ( $validator->isValid() ) {
				$escort = new Model_EscortItem($data);
				$m_saver = new Model_Escort_Saver();
				$m_saver->save($escort);
			}
			
			die(json_encode($validator->getStatus()));
		}
		else {
			$this->view->escort = $escort = $this->model->get($id);
			
			$photos = $escort->getPhotos();
			
			$private_photos = array();
			foreach ($photos as $k => $photo)
			{
				if ( $photo->type == ESCORT_PHOTO_TYPE_PRIVATE )
				{
					$private_photos[] = $photo;
					unset($photos[$k]); 
				}
			}
			
			$this->view->photos = $photos;
			$this->view->private_photos = $private_photos;
			
			$this->view->tour_data = $this->model->getTours($id);
		}
	}

	public function deleteAction()
	{
		$escort_id = intval($this->_request->id);
		
		if ( ! $this->user->hasAccessToEscort($id) ) die('Permission denied');
		
		$this->model->delete($escort_id);
		die;
	}

	public function photosAction()
	{
		$this->view->id = $id = intval($this->_getParam('id'));
		
		if ( ! $this->user->hasAccessToEscort($id) ) die('Permission denied');
		
		
		/* GRAB PHOTOS FROM ORIGINAL SERVER */
		set_time_limit(0);
		
		/*$config = Zend_Registry::get('images_config');
		$url = trim($config['remote']['url'], '/');
		get_headers($url . '/migrate_photos.php?id=' . $id);*/
		// ------------------------
		
		
		/*$images = new Cubix_Images();
		
		
		
		try {
			$images->save('C:\Users\GuGo\Desktop\grabber\lia-19\6\4.jpg', 1);
		} catch (Exception $e) {
			
		}
		*/
		
		$escort = $this->model->get($id);
		$this->view->escort = $escort;
		
		$photos = array('public' => array(), 'private' => array());
		
		foreach ( $escort->getPhotos() as $photo ) {
			if ( $photo->type == ESCORT_PHOTO_TYPE_PRIVATE ) {
				$photos['private'][] = $photo;
			}
			else {
				$photos['public'][] = $photo;
			}
		}
		
		$this->view->photos = $photos;
	}
	
	public function doPhotosAction()
	{
		$action = $this->_getParam('act');
		$model = new Model_Escort_Photos();
		$ids = $this->_getParam('photos');
		
		$photos_min_count = Cubix_Application::getById($this->_getParam('application_id'))->min_photos;

		$escort_id = $model->get(end($ids))->escort_id;
		// if ( ! is_array($ids) ) die;
		
		switch ($action)  {
			case 'public':
			case 'private':
				foreach ( $ids as $id ) {
					$id = intval($id);
					if ( ! $id ) continue;
					
					$photo = $model->get($id);
					$photo->{'make' . ucfirst($action)}();
				}
			break;
			case 'soft':
			case 'hard':
				foreach ( $ids as $id ) {
					$id = intval($id);
					if ( ! $id ) continue;
					
					$photo = $model->get($id);
					$photo->{'make' . ucfirst($action)}();
				}
			break;
			case 'verified':
				foreach ( $ids as $id ) {
					$id = intval($id);
					if ( ! $id ) continue;
					
					$photo = $model->get($id);
					$photo->toggleVerified();
				}
			break;
			case 'remove':

				foreach ( $ids as $i => $id ) {
					$ids[$i] = intval($id);
					if ( ! $ids[$i] ) continue;
				}
				
				$images = array();
				
				foreach ( $ids as $id ) {
					$photo = $model->get($id);
					$images[] = new Cubix_Images_Entry(array(
						'application_id' => $photo->application_id,
						'catalog_id' => $photo->escort_id,
						'hash' => $photo->hash,
						'ext' => $photo->ext
					));
				}
				
				$images_model = new Cubix_Images();
				$images_model->remove($images);

				$escort_id = $model->get(end($ids))->escort_id;
				$escort = $this->model->get($escort_id);
				
				$model->remove($ids);

				$photos_count = $escort->getPhotosCount();

				if ( $photos_count < $photos_min_count )
				{
					//if ( ! $this->model->hasStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS) ) {
						$this->model->setStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS);
					if ( $this->model->hasStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_ACTIVE) ) {
						$this->model->removeStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_ACTIVE);
					}
				}
			break;
			case 'sort':
				foreach ( $ids as $i => $id ) {
					$model->get($id)->reorder($i + 1);
				}
			break;
			case 'set-main':
				$id = end($ids);
				$model->get($id)->setMain();
				$escort_id = $model->get(end($ids))->escort_id;
				$escort = $this->model->get($escort_id);
				$m_snapshot = new Model_Escort_Snapshot(array('id' => $escort_id));
				$m_snapshot->snapshotProfile();
				
			break;
			case 'adjust':
				$photo_id = intval($this->_getParam('photo_id'));

				$photo = $model->get($photo_id);
				
				$hash = $photo->getHash();
				$result = array(
					'x' => intval($this->_getParam('x')),
					'y' => intval($this->_getParam('y')),
					'px' => floatval($this->_getParam('px')),
					'py' => floatval($this->_getParam('py'))
				);
				$photo->setCropArgs($result);

				// Crop all images
				$size_map = array(
					'backend_thumb' => array('width' => 150, 'height' => 205),
					'backend_smallest' => array('width' => 64, 'height' => 64),
					'medium' => array('width' => 225, 'height' => 300),
					'thumb' => array('width' => 150, 'height' => 200),
					'nlthumb' => array('width' => 120, 'height' => 160),
					'sthumb' => array('width' => 76, 'height' => 103),
					'lvthumb' => array('width' => 75, 'height' => 100),
					't100p' => array('width' => 117, 'height' => 97)
				);
				$conf = Zend_Registry::get('images_config');
				
				get_headers($conf['remote']['url'] . '/get_image.php?a=clear_cache&app=' . Cubix_Application::getById($this->_getParam('application_id'))->host . '&eid=' . $photo->escort_id . '&hash=' . $photo->hash);
				// echo $conf['remote']['url'] . '/get_image.php?a=clear_cache&app=' . Cubix_Application::getById($this->_getParam('application_id'))->host . '&eid=' . $photo->escort_id . '&hash=' . $photo->hash;

				$catalog = $photo->escort_id;
				
				$a = array();
				if ( is_numeric($catalog) ) {
					$parts = array();

					if ( strlen($catalog) > 2 ) {
						$parts[] = substr($catalog, 0, 2);
						$parts[] = substr($catalog, 2);
					}
					else {
						$parts[] = '_';
						$parts[] = $catalog;
					}
				}
				else if ( preg_match('#^([0-9]+)/(.+)$#', $catalog, $a) ) {
					array_shift($a);
					$catalog = $a[0];

					$parts = array();

					if ( strlen($catalog) > 2 ) {
						$parts[] = substr($catalog, 0, 2);
						$parts[] = substr($catalog, 2);
					}
					else {
						$parts[] = '_';
						$parts[] = $catalog;
					}

					$parts[] = $a[1];
				}

				$catalog = implode('/', $parts);

				foreach($size_map as $size => $sm) {
					// echo $conf['remote']['url'] . '/' . Cubix_Application::getById($this->_getParam('application_id'))->host . '/' . $catalog . '/' . $hash . '_' . $size . '.jpg?args=' . $result['x'] . ':' . $result['y'] . '<br />';
					get_headers($conf['remote']['url'] . '/' . Cubix_Application::getById($this->_getParam('application_id'))->host . '/' . $catalog . '/' . $hash . '_' . $size . '.jpg?args=' . $result['x'] . ':' . $result['y']);
				}
				
				echo json_encode(array('success' => true));
			break;
		}
		
		$this->addSnapshotsV2($escort_id);
		ob_flush();
		die;
	}
	
	public function uploadPhotoAction()
	{
		$escort_id = intval($this->_getParam('escort_id'));
		$is_private = intval($this->_getParam('is_private'));
	
		$photos_min_count = Cubix_Application::getById($this->_getParam('application_id'))->min_photos;

		if ( ! $this->user->hasAccessToEscort($escort_id) ) return json_encode(array('status' => 'error', 'msg' => 'Permission denied'));
		
		try {
			$escort = $this->model->get($escort_id);
			
			if ( ! $escort ) {
				throw new Exception('Wrong id supplied');
			}
			
			// Save on remote storage
			$images = new Cubix_Images();
			$image = $images->save($_FILES['Filedata']['tmp_name'], $escort->id, $escort->application_id, strtolower(@end(explode('.', $_FILES['Filedata']['name']))));
			
			$image = new Cubix_Images_Entry($image);
			$image->setSize('backend_thumb');
			$image->setCatalogId($escort_id);
			$image_url = $images->getUrl($image);
			
			$photo = new Model_Escort_PhotoItem(array(
				'escort_id' => $escort_id,
				'hash' => $image->getHash(),
				'ext' => $image->getExt(),
				'type' => $is_private ? ESCORT_PHOTO_TYPE_PRIVATE : ESCORT_PHOTO_TYPE_HARD
			));
			
			$model = new Model_Escort_Photos();
			$photo = $model->save($photo);

			$photos_count = $escort->getPhotosCount();

			$m_escorts = new Model_Escorts();
			
			if ( $photos_count < $photos_min_count )
			{
				if ( ! $m_escorts->hasStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS) ) {
					$m_escorts->setStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS);
				}
				$m_escorts->removeStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_ACTIVE);
			}
			else {
				if ( $m_escorts->hasStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS) ) {
					$m_escorts->removeStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS);
				}

				if ( ! $m_escorts->hasStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_ADMIN_DISABLED) &&
					! $m_escorts->hasStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_OWNER_DISABLED) )
				{
					if (
						(! $m_escorts->hasStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_IS_NEW) &&
						! $m_escorts->hasStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_NO_PROFILE) &&
						! $m_escorts->hasStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_PROFILE_CHANGED))
					)
					{
						$m_escorts->setStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_ACTIVE);
						
						//$m_snapshot = new Model_Escort_Snapshot(array('id' => $escort_id));

						//$m_snapshot->snapshotProfile();
						$this->addSnapshotsV2($escort_id);
					}
				}
			}
			
			$result = array(
				'status' => 'success',
				'photo' => $photo->toJSON('backend_thumb_cropper')
			);
		} catch (Exception $e) {
			$result = array('status' => 'error', 'msg' => '');
			
			$result['msg'] = $e->getMessage();
		}
		
		die(json_encode($result));
	}

	public function revisionsAction()
	{
		$this->view->layout()->disableLayout();
		
		$escort_id = intval($this->_getParam('escort_id'));

		if ( ! $this->user->hasAccessToEscort($escort_id) ) die('Permission denied');
		
		if ( ! $escort_id ) die;
		
		$this->view->escort_id = $escort_id;

		$m_profile = new Model_Escort_Profile(array('id' => $escort_id));
		
		/* --> Process Actions */
		$a = $this->_getParam('a');
		if ( ! is_null($a) ) {
			if ( 'decline' == $a ) {
				$latest_revision = $m_profile->getProfileLatestRevision(Model_Escort_Profile::REVISION_STATUS_NOT_APPROVED);
				$m_profile->updateProfileRevision($latest_revision, Model_Escort_Profile::REVISION_STATUS_DECLINED);
			}
			elseif ( 'apply' == $a ) {
				$latest_revision = $m_profile->getProfileLatestRevision(Model_Escort_Profile::REVISION_STATUS_NOT_APPROVED);
				$revision = $m_profile->getProfileRevision($latest_revision);
				
				$m_profile->applyProfileRevision($revision->data);
				$m_profile->updateProfileRevision($latest_revision, Model_Escort_Profile::REVISION_STATUS_APPROVED);
				
				if ( $this->model->hasStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_IS_NEW) ) {
					$this->model->removeStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_IS_NEW);
				}
				
				if ( $this->model->hasStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_PROFILE_CHANGED) ) {
					$this->model->removeStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_PROFILE_CHANGED);
				}

				if ( $this->model->hasStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_NO_PROFILE) ) {
					$this->model->removeStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_NO_PROFILE);
				}

				if ( ! $this->model->hasStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_ADMIN_DISABLED) &&
					! $this->model->hasStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_OWNER_DISABLED) )
				{
					if (
						(! $this->model->hasStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_IS_NEW) &&
						! $this->model->hasStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_NO_PROFILE) &&
						! $this->model->hasStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_PROFILE_CHANGED))
					)
					{
						$this->model->setStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_ACTIVE);
						Zend_Registry::get('BillingHooker')->notify('escort_activated', array($escort_id));
					}
				}

				/*if ( ! $this->model->hasStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_ACTIVE) ) {
					$this->model->setStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_ACTIVE);
				}*/
			}
			
			die;
		}
		/* <-- */
		
		/*
		if ( ! is_null($only_changes = $this->_getParam('only_changes')) ) {
			setcookie('escorts_revisions_only_changes', $only_changes, strtotime('+1 year'), '/');
		}
		else {
			$only_changes = intval($_COOKIE['escorts_revisions_only_changes']);
		}
		
		$this->view->only_changes = $only_changes;
		*/
		
		$this->view->revisions = $m_profile->getProfileRevisions();
		
		$this->view->model = $this->model;
		
		$revision = intval($this->_getParam('revision'));
		
		if ( ! $revision ) {}
		
		$this->_helper->viewRenderer->setScriptAction('preview');
		
		if ( ! $revision ) {
			$revision = $m_profile->getProfileLatestRevision();
		}
		
		$old_revision = $revision - 1;
		
		$new = $m_profile->getProfileRevision($revision);
		$old = $m_profile->getProfileRevision($old_revision);
		
		if ( ! $old ) {
			$old = clone $new;
			$old->date_updated = null;
			$old->revision = '-';
			foreach ( $old->data as $field => $value ) {
				if ( ! is_array($value) ) $value = '';
				else $value = array();
				
				$old->data[$field] = $value;
			}
		}
		
		if ( ! $new ) {
			$this->_setParam('revision', $old->revision);
			$this->_setParam('no_changes', true);
			return $this->_forward('revisions');
		}
		
		$this->view->old = $old;
		$this->view->new = $new;
		
		$this->view->diff = $m_profile->differProfile($old->data, $new->data);
		
		return;
		
		/*else {
			$this->view->no_changes = $this->_getParam('no_changes', false);
			$this->view->profile = $m_profile->getProfileRevision($escort_id, $revision);
			$this->_helper->viewRenderer->setScriptAction('preview');
		}*/
	}
	
	public function previewAction()
	{
		
	}

	public function ordersDataAction()
	{
		$this->view->layout()->disableLayout();

		$m_orders = new Model_Billing_Orders();
		$req = $this->_request;

		$data = new Cubix_Form_Data($this->_request);
		
		$data->setFields(array(
			'page' => 'int',
			'per_page' => 'int',
			'sort_field' => '',
			'sort_dir' => '',			
			'user_id' => ''
		));
		
		$data = $data->getData();

		$filter = array();

		if ( strlen($data['user_id']) > 0 ) {
			$filter['u.id = ?'] = $data['user_id'];
		}

		$data = $m_orders->getAll(
			$filter,
			$req->page,
			$req->per_page,
			$req->sort_field,
			$req->sort_dir
		);

		die(json_encode($data));
	}

	public function salesWorkAction()
	{
		$bu_user = Zend_Auth::getInstance()->getIdentity();

		if ( 'data entry' == $bu_user->type || 'data entry plus' == $bu_user->type ) {
			return $this->_forward('permission-denied', 'error');
		}

		$this->view->layout()->disableLayout();
		
		$req = $this->_request;
		$this->view->user_id = $user_id = intval($req->user_id);
		$escort_id = intval($req->escort_id);

		if ( $escort_id ) {
			$escort = $this->model->get($escort_id);
		}
		else {
			$escort = $this->model->getByUserId($user_id);
		}
		

		$this->view->escort = $escort;
		
		$active_packages = $escort->getActivePackages();

		$this->view->total_orders_price = $escort->getTotalOrdersPrice();
		$this->view->total_orders_paid = $escort->getTotalOrdersPrice(true);
		

		if ( $escort->agency_id ) {
			$this->view->is_agency_escort = true;
		}

		$m_o_packages = new Model_Billing_Packages();
		$act_packages = array();		
		if ( count($active_packages) > 0 )
		{
			foreach ( $active_packages as $k => $active_package )
			{
				$package = $m_o_packages->get($active_package->id);

				if ( ! is_null($active_package->order_id) ) {
					$products = $package->getProducts();
					$opt_products = $package->getProducts(Model_Products::TYPE_OPTIONAL_PRODUCT);
				}
				else {
					$products = $package->getDefaultProducts();
					$opt_products = $package->getDefaultProducts(Model_Products::TYPE_OPTIONAL_PRODUCT);
				}


				if ( count($products) > 0 ) {
					foreach( $products as $k => $product )
					{
						$products[$k]->name = Cubix_I18n::translate($product->name);
					}
				}

				if ( count($opt_products) > 0 ) {
					foreach( $opt_products as $k => $product )
					{
						$opt_products[$k]->name = Cubix_I18n::translate($product->name);
					}
				}

				$active_package->package_products = $products;
				$active_package->package_opt_products = $opt_products;

				$act_packages[] = $active_package;
			}
		}
		$this->view->active_packages = array('data' => $act_packages);
		//print_r($act_packages);
	}

	public function getOrdersTotalAction()
	{
		$this->view->layout()->disableLayout();

		$req = $this->_request;
		$escort_id = intval($req->escort_id);

		$m_escort = new Model_Escorts();

		$escort = $m_escort->get($escort_id);

		$data = array(
			'total_orders_price' => $escort->getTotalOrdersPrice(),
			'total_orders_paid' => $escort->getTotalOrdersPrice(true)
		);

		die(json_encode($data));
	}

	public function addSnapshotsAction()
	{
		$m_snapshot = new Model_Escort_Snapshot();
		$m_snapshot->getNoSnapshotEscorts();

		die;
	}

	public function addSnapshotsV2($escort_id)
	{
		$m_snapshot = new Model_Escort_SnapshotV2(array('id' => $escort_id));
		$m_snapshot->snapshotProfileV2();
	}
}
