<?php

class ChatController extends Zend_Controller_Action
{
	public function init()
	{
		if(Cubix_Application::getId() == APP_BL){
			$this->model = new Model_ChatV2();
		}
		else{
			$this->model = new Model_Chat();
		}
	}
	
	public function indexAction() 
	{
		
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'f_user_id' => $req->user_f_h,
			't_user_id' => $req->user_t_h,
			'message' => $req->message,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to,
			'both' => $req->both
		);
		
		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		foreach ( $data as $i => $item ) 
		{
			if ($item->is_suspicious)
			{
				$m = '<div style="float:left;position:relative;padding-right:10px" class="member_comment">
				<img src="/img/famfamfam/asterisk_red.png" style="cursor:pointer">
				<div style="position: absolute; top: -5px; left: 40px; width: 200px; background-color: #EEE; padding: 10px; border: 1px solid #FF0000; font-size: 12px; font-weight: bold; color:#FF0000" class="none">' . $item->suspicious_reason . '</div>
				</div>
				<div style="float:left">
				<a class="p-member" rel="' .  $item->user_id . '" href="#">' .  $item->username . ' (Member)</a>
				</div>';
			}
			else 
			{
				$m = '<a class="p-member" rel="' .  $item->user_id . '" href="#">' .  $item->username . ' (Member)</a>';
			}
			
			if ($item->user_type == 'escort')
				$user_from = '<a class="p-escort" rel="' .  $item->escort_id . '" href="#">' .  $item->showname . ' (Escort - ' . $item->escort_id . ')</a>';
			elseif ($item->user_type == 'agency')
				$user_from = '<a class="p-agency" rel="' .  $item->agency_id . '" href="#">' .  $item->agency_name . ' (Agency)</a>';
			else
				$user_from = $m;
			
			$data[$i]->user_from = $user_from;
			
			$u = $this->model->getUserInfo($item->user_id_to);
			
			if ($u->user_type == 'escort')
				$user_to = '<a class="p-escort" rel="' .  $u->escort_id . '" href="#">' .  $u->showname . ' (Escort - ' . $u->escort_id . ')</a>';
			elseif ($u->user_type == 'agency')
				$user_to = '<a class="p-agency" rel="' .  $u->agency_id . '" href="#">' .  $u->agency_name . ' (Agency)</a>';
			else
				$user_to = '<a class="p-member" rel="' . $item->user_id_to . '" href="#">' .  $u->username . ' (Member)</a>';
			
			$data[$i]->user_to = $user_to;
			
			if ($item->read_date)
				$data[$i]->user_read = 1;
			else
				$data[$i]->user_read = 0;
		}
				
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
	
	public function listAction()
	{
		$user = trim($this->_getParam('user'));
		
		if ( ! strlen($user) ) {
			die(json_encode(array()));
		}

		$list = $this->model->getUsersList($user);

		$tmp = array();
		
		if ( count($list) ) 
		{
			foreach ( $list as $i => $l ) 
			{
				if ($l->user_type == 'escort')
				{
					$name = $l->showname . ' (Escort - ' . $l->escort_id . ')';
				}
				elseif ($l->user_type == 'agency')
				{
					$name = $l->agency_name . ' (Agency)';
				}
				else
				{
					$name = $l->username . ' (Member)';
				}
				
				$tmp[$i] = array('id' => $l->id, 'name' => $name);
			}
		}

		echo json_encode($tmp);
		die;
	}
	
	public function deleteAction()
	{
		$ids = $this->_request->id;
		
		if (is_array($ids))
			$this->model->delete($ids);
		
		die;
	}
}
