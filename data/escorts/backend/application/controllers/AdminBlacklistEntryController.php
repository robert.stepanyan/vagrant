<?php

class AdminBlacklistEntryController extends Zend_Controller_Action {

	public function init()
	{
		$this->model = new Model_AdminBlacklistEntry();
	}
	
	public function indexAction() 
	{
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('default_admin_entry_data');
		$this->view->filter_params = $ses_filter->filter_params;
		/* FILTER PARAMS */
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'comment' => $req->comment,
			'status' => $req->status
		);
		

		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		if ( count($data) ) {
			foreach ( $data as $i => $d ) {
				$data[$i]->images = 'a';
				$p = 0;
				$images = $this->model->getImages($d->id);
				if($images){
					foreach ($images as $image) {
						$img = new Cubix_ImagesCommonItem($image);
						$data[$i]->attach[$p]['thumb'] = $img->getUrl('lvthumb');
						$data[$i]->attach[$p]['original'] = $img->getUrl('orig');
						$p++;
					}

				}
			}
		}

		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
	
	public function createAction()
	{
		if( ! $this->_request->isPost() )
		{
			
		} else {
			$data = new Cubix_Form_Data($this->_request);
						
			$data->setFields(array(
				'comment' => '',
				'attach' => 'arr-int'
			));
			
			$data = $data->getData();
						
			$validator = new Cubix_Validator();
						
			if ( ! strlen($data['comment']) ) {
				$validator->setError('comment', 'Required');
			}

			$data['is_approved'] = 1;
			
			
			switch ($this->_request->act)
			{
				case 'approve':
					$data['is_approved'] = 1;
				break;
				case 'save':
					unset($data['status']);
				break;
			}

			if ( $validator->isValid() ) {
				$this->model->insert($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}

	
	public function editAction()
	{
		if( ! $this->_request->isPost() )
		{

			$data = $this->model->getById($this->_request->id);

			if ( $data) {
				$p = 0;
				$images = $this->model->getImages($data->id);

				if($images){
					foreach ($images as $image) {
						$img = new Cubix_ImagesCommonItem($image);
						$data->images[$p]['id'] = $image->image_id;
						$data->images[$p]['thumb'] = $img->getUrl('lvthumb');
						$data->images[$p]['original'] = $img->getUrl('orig');
						$p++;
					}

				}
			}

			$this->view->data = $data;
			
		}
		else 
		{
			$data = new Cubix_Form_Data($this->_request);
			
			$data->setFields(array(
				'id' => 'int',
				'comment' => '',
				'attach' => 'arr-int'
			));
			
			$data = $data->getData();
						
			$validator = new Cubix_Validator();
						
			if ( ! strlen($data['comment']) ) {
				$validator->setError('comment', 'Required');
			}

			
			switch ($this->_request->act)
			{
				case 'approve':
					$data['is_approved'] = 1;
				break;
				case 'save':
					unset($data['status']);
				break;
			}

			if ( $validator->isValid() ) {
				$this->model->save($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}

	public function toggleActiveAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->model->toggle($id);
	}
	
	public function removeAction()
	{
		$id = intval($this->_request->id);
		$this->model->remove($id);
		
		die;
	}

	public function removeImageAction()
	{	
		$id = intval($this->_request->id);
		$this->model->removeImage($id);
		
		die;
	}

	public function attachedImageAction()
	{
		$tmp_dir = sys_get_temp_dir();

		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$images = new Cubix_ImagesCommon();


		$file = $tmp_dir . DIRECTORY_SEPARATOR . $_SERVER['HTTP_X_FILE_NAME'];
		file_put_contents($file, file_get_contents('php://input'));
		
		if ( ! filesize($file) || filesize($file) != $_SERVER['HTTP_X_FILE_SIZE'] ) {
			throw new Exception("Photo upload failed. Please try again!");
		}

		$data['tmp_name'] = $file;
		$data['name'] = $_SERVER['HTTP_X_FILE_NAME'];

		$photo = $images->save($data);

		if (!$photo) {

			$return = array(
				'status' => '0',
				'error' => $error
			);

		} else {
			$return = array(
				'status' => '1',
				'id' => $photo['image_id'],
				'finish' => TRUE,
				'error' => false
			);
		}

		echo json_encode($return);
	}
}