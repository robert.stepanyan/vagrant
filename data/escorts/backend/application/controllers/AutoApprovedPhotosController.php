<?php

class AutoApprovedPhotosController extends Zend_Controller_Action {

	public function init()
	{
		$this->model = new Model_AutoApprovePhoto();
	}
	
	public function indexAction() 
	{
		

	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'showname' => $req->showname,
			'escort_id' => $req->escort_id,
			'application_id' => $req->application_id
		);

		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);

		$DEFINITIONS = Zend_Registry::get('defines');
		$escortsV2_model = new Model_EscortsV2();
		foreach ($data as $d)
		{
			$stat = array();
			foreach( $DEFINITIONS['escort_status_options'] as $key => $e_status )
			{
				if ( $escortsV2_model->hasStatusBit($d->id, $key) )
				{
					$stat[] = $e_status;
				}
			}

			$d->status = implode(', ', $stat);//$DEFINITIONS['escort_status_options'][$d['status']];

		}
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
	
	public function uploadAction()
	{
		if( ! $this->_request->isPost() )
		{
			//$escort_id = $this->_request->escort_id;
			//$slogans = new Model_Slogans();
			
			//$this->view->data = $slogans->getByEscortId($escort_id);
		}
		else 
		{
			/*$data = new Cubix_Form_Data($this->_request);
						
			$data->setFields(array(
				'escort_id' => 'int',
				'slogan' => ''
			));
			
			$data = $data->getData();
						
			$validator = new Cubix_Validator();
						
			

			if ( $validator->isValid() ) {
				$this->model->save($data);
			}
			
			
			die(json_encode($validator->getStatus()));*/
		}
	}
	
	public function removeAction()
	{
		$escort_id = intval($this->_request->escort_id);
		$this->model->remove($escort_id);
		die;
	}
}
