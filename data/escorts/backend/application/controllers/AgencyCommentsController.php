<?php

class AgencyCommentsController extends Zend_Controller_Action {

	const MINIMUM_CHARS_COUNT = 35;

	public function init()
	{
		$this->model = new Model_AgencyComments();
	}
	
	public function indexAction() 
	{
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('default_agency_comments_data');
		$this->view->filter_params = $ses_filter->filter_params;
		/* FILTER PARAMS */
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'agency_name' => $req->agency_name,
			'agency_id' => $req->agency_id,
			'username' => $req->username,
			'application_id' => $req->application_id,
			'status' => $req->status,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to,
			'comment' => $req->comment
		);
		
		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);

		$DEFINITIONS = Zend_Registry::get('defines');

		foreach ( $data as $i => $item ) {
			$data[$i]->status = $DEFINITIONS['agency_comment_status'][$item->status];
			$data[$i]->comment = mb_substr($item->comment, 0, 50, 'UTF-8') . ' ...';
		}
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
	
	public function editAction()
	{
		if( ! $this->_request->isPost() )
		{
			$data = $this->model->getById($this->_request->id);
			$this->view->data = $data;
		}
		else 
		{
			$data = new Cubix_Form_Data($this->_request);
						
			$data->setFields(array(
				'id' => 'int',
				'comment' => ''
			));
			
			$data = $data->getData();
						
			$validator = new Cubix_Validator();
						
			if ( strlen($data['comment']) < self::MINIMUM_CHARS_COUNT ) {
				$validator->setError('comment', 'Minimum ' . self::MINIMUM_CHARS_COUNT . ' chars');
			}
			
			switch ($this->_request->act)
			{
				case 'approve':
					$data['status'] = AGENCY_COMMENT_STATUS_ACTIVE;
					break;
				case 'disable':
					$data['status'] = AGENCY_COMMENT_STATUS_DISABLED;
					break;
			}

			if ( $validator->isValid() ) {
				$this->model->save($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}

	public function removeAction()
	{
		$id = intval($this->_request->id);
		$this->model->remove($id);
		
		die;
	}
}
