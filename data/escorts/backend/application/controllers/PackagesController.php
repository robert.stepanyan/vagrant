<?php

class PackagesController extends Zend_Controller_Action
{
	/**
	 * @var $products Model_Products
	 */
	public $products;

    /**
     * @var $model Model_Packages
     */
	public $model;

	public function init()
	{
		$this->model = new Model_Packages();
	}
			
	public function indexAction()
	{
		
	}

	public function dataAction()
	{
		$req = $this->_request;

		$filter = array(
			'p.application_id' => $req->app_id
			/*'u.username' => $req->username,
			
			'u.email' => $req->email,
			'u.last_ip' => $req->last_ip,
			'u.status' => $req->status*/
		);

		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page,
			$filter,
			$this->_request->sort_field,
			$this->_request->sort_dir,
			$count
		);

		$DEFINITIONS = Zend_Registry::get('defines');

		foreach ( $data as $i => $item ) {
			$data[$i]->available_for = $DEFINITIONS['prod_available'][$item->available_for];

			$products = $item->getPackageProducts();
			$addon_products = $item->getOptionalProducts();

			$prod_list = '';
			foreach ($products as $prod )
			{
				$prod_list .= '<p>' . $prod['name'] . '</p>';
			}

			$addon_prod_list = '';
			foreach ($addon_products as $prod )
			{
				$addon_prod_list .= '<p>' . $prod['name'] . '</p>';
			}

			$data[$i]->product_list = $prod_list;
			$data[$i]->addon_product_list = $addon_prod_list;
		}

		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

	public function createAction()
	{
		$req = $this->_request;
		$package_id = $req->id;
		$this->view->app_id = $req->getParam('app_id');

		$m_products = new Model_Products();
		$this->view->product_list = $products = $m_products->getAllNames($req->getParam('application_id'));

		if ( $req->isPost() )
		{
			$validator = new Cubix_Validator();

			$app_id = $req->getParam('app_id');
			$name = $req->getParam('name');
			$available_for = $req->getParam('available_for');
			$period = $req->getParam('period');
			$is_active = ($req->getParam('is_active')) ? $req->getParam('is_active') : 0;
			$is_default = ($req->getParam('is_default')) ? $req->getParam('is_default') : 0;
			$is_premium = ($req->getParam('is_premium')) ? $req->getParam('is_premium') : 0;

			$prices = $req->getParam('prices');
			$products = $req->getParam('products');
			$opt_products = $req->getParam('opt_products');

			if ( ! $products ) {
				$validator->setError('product', 'Plaese add a product.');
			}

			if ( ! $name ) {
				$validator->setError('name', 'Required');
			}

			if ( ! $app_id ) {
				$validator->setError('app_id', 'Required');
			}

			if ( ! $period ) {
				$validator->setError('period', 'Required');
			}
			elseif ($period && intval($period) == 0) {
				$validator->setError('period', 'Must be a numeric');
			}

			$data = array(
				'package_data' => array(
					'name' => $name,
					'application_id' => $app_id,
					'available_for' => $available_for,
					'period' => $period,
					'is_active' => $is_active,
					'is_default' => $is_default,
					'is_premium' => $is_premium,
				),
				'prices'		=> $prices,
				'products'		=> $products,
				'opt_products'	=> $opt_products
			);

			if ( $validator->isValid() ) {
				$this->model->save($data);
			}

			die(json_encode($validator->getStatus()));
		}
	}

	public function editAction()
	{
		$req = $this->_request;
		$package_id = $req->id;
		$this->view->app_id = $req->app_id;

		$m_products = new Model_Products();
		$this->view->product_list = $products = $m_products->getAllNames($req->application_id);

		$this->view->package = $package = $this->model->get($package_id);
		$this->view->package_prices = $package->getPrices();
		$this->view->package_products = $package->getPackageProducts();
		$this->view->optional_products = $package->getOptionalProducts();

		if ( $req->isPost() )
		{
			$validator = new Cubix_Validator();

			$id = $req->id;
			$app_id = $req->app_id;
			$available_for = $req->available_for;
			$period = $req->period;
			$is_active = ($req->is_active) ? $req->is_active : 0;
			$is_default = ($req->is_default) ? $req->is_default : 0;
			$is_premium = ($req->is_premium) ? $req->is_premium : 0;
			
			$prices = $req->prices;
			$products = $req->products;
			$opt_products = $req->opt_products;

			if ( ! $products ) {
				$validator->setError('product', 'Plaese add a product.');
			}

			/*if ( ! $app_id ) {
				$validator->setError('app_id', 'Required');
			}*/

			if ( ! $period ) {
				$validator->setError('period', 'Required');
			}
			elseif ($period && intval($period) == 0) {
				$validator->setError('period', 'Must be a numeric');
			}

			$data = array(
				'package_data' => array(
					'id' => $id,
					'application_id' => $app_id,
					'available_for' => $available_for,
					'period' => $period,
					'is_active' => $is_active,
					'is_default' => $is_default,
					'is_premium' => $is_premium,
				),
				'prices'		=> $prices,
				'products'		=> $products,
				'opt_products'	=> $opt_products
			);

			if ( $validator->isValid() ) {
				$this->model->update($data);
			}

			die(json_encode($validator->getStatus()));
		}
	}

	public function ajaxGetPricesAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$app_id = $req->app_id;
		$prod_id = $req->product_id;

		$this->view->product = $product = $this->model->get($prod_id);
		$this->view->product_prices = $product->getPrices($app_id);
	}

	public function removeAction()
	{
		$req = $this->_request;

		$id = $req->id;

		$this->model->remove($id);
	}

	public function toggleActiveAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->model->toggle($id, 'active');
	}
	
	public function toggleDefaultAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->model->toggle($id, 'default');
	}
}