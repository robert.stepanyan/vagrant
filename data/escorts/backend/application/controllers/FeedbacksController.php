<?php

class FeedbacksController extends Zend_Controller_Action
{
	protected $model;

	public function init()
	{
		$this->model = new Model_Feedback();
	}

	public function indexAction()
	{

	}

	public function dataAction()
	{
		$req = $this->_request;
        
		$filter = array(
			'escort_showname' => $req->showname, 
			'escort_id' => $req->escort_id, 
			'email' => $req->email, 
			'to_addr' => $req->to_addr, 
			'flag' => $req->flag, 
			'status' => $req->status, 
			'ip' => $req->ip, 
			'city' => $req->city, 
			'application_id' => $req->application_id 
		);
        
		$data = $this->model->getAll(
			$req->page,
			$req->per_page,
			$filter,
			$req->sort_field,
			$req->sort_dir,
			$count
		);
		
		$DEFINITIONS = Zend_Registry::get('defines');
		
		foreach ( $data as $i => $item ) 
		{
			$data[$i]->flag_name = $DEFINITIONS['feedback_flags'][$item->flag];
			$data[$i]->status_name = $DEFINITIONS['feedback_status'][$item->status];
			$data[$i]->ip = $this->view->Geoip($item->ip);
		}

		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

	public function viewAction()
	{
		$this->view->item = $item = $this->model->get($this->_getParam('id'));
		
		if ( $this->_request->isPost() )
		{
			$validator = new Cubix_Validator();
			$act = $this->_request->act;
			
			$data = array('id' => $item->id);
			
			if ($act == 'disable')
			{
				if ($item->status == FEEDBACK_STATUS_APPROVED)
				{
					$validator->setError('err', 'You can not disable, feedback already approved.');
				}
				elseif ($item->status == FEEDBACK_STATUS_NOT_APPROVED)
				{
					$data['status'] = FEEDBACK_STATUS_DISABLED;

					if ( $validator->isValid() ) 
					{
						$this->model->update($data);
					}
				}
			}
			elseif ($act == 'approve')
			{
				/*if ($item->status == FEEDBACK_STATUS_DISABLED)
				{
					$validator->setError('err', 'You can not approve, feedback already disabled.');
				}
				elseif ($item->status == FEEDBACK_STATUS_NOT_APPROVED)
				{
					if ($item->flag == FEEDBACK_FLAG_BLOCKED_BY_WORD)
					{
						$validator->setError('err', 'You can not approve, feedback blocked by word.');
					}
					elseif ($item->flag == FEEDBACK_FLAG_BLOCKED_BY_EMAIL)
					{
						$validator->setError('err', 'You can not approve, feedback blocked by email.');
					}
					elseif ($item->flag == FEEDBACK_FLAG_NOT_SEND)
					{
						$data['status'] = FEEDBACK_STATUS_APPROVED;
						$data['flag'] = FEEDBACK_FLAG_SENT;
						
						if ( $validator->isValid() ) 
						{
							$this->model->update($data);
														
							$email_tpl = 'escort_feedback';
							$tpl_data = array(
								'name' => $item->name,
								'email' => $item->email,
								'escort_showname' => $item->escort_showname,
								'message' => $item->message
							);
							
							Cubix_Email::sendTemplate($email_tpl, $item->to_addr, $tpl_data, $item->email);
						}
					}
				}*/
				
				$data['status'] = FEEDBACK_STATUS_APPROVED;
				$data['flag'] = FEEDBACK_FLAG_SENT;

				if ( $validator->isValid() ) 
				{
					$this->model->update($data);

					$email_tpl = 'escort_feedback';
					$tpl_data = array(
						'name' => $item->name,
						'email' => $item->email,
						'escort_showname' => $item->escort_showname,
						'message' => $item->message
					);

					Cubix_Email::sendTemplate($email_tpl, $item->to_addr, $tpl_data, $item->email);
				}
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function deleteAction()
	{
		set_time_limit(0);
		
		if ( $this->_request->isPost() )
		{
			$ids = $this->_request->id;
			
			if (is_array($ids) && count($ids) > 0)
			{
				foreach ($ids as $id)
				{
					$item = $this->model->get($id);
					
					if ($this->_request->act == 'disable')
					{
						if ($item->status == FEEDBACK_STATUS_NOT_APPROVED)
						{
							$data = array('id' => $item->id);

							$data['status'] = FEEDBACK_STATUS_DISABLED;
							$data['flag'] = FEEDBACK_FLAG_NOT_SEND;

							$this->model->update($data);
						}
					}
					else
					{
						if ($item->status != FEEDBACK_STATUS_APPROVED)
						{
							$data = array('id' => $item->id);

							$data['status'] = FEEDBACK_STATUS_APPROVED;
							$data['flag'] = FEEDBACK_FLAG_SENT;

							$this->model->update($data);

							$email_tpl = 'escort_feedback';
							$tpl_data = array(
								'name' => $item->name,
								'email' => $item->email,
								'escort_showname' => $item->escort_showname,
								'message' => $item->message
							);

							Cubix_Email::sendTemplate($email_tpl, $item->to_addr, $tpl_data, $item->email);
						}
					}
				}
			}
		}
		
		die;
	}
}
