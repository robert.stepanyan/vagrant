<?php

class BalanceController extends Zend_Controller_Action {
	/**
	 * The default action - show the home page
	 */
	public function indexAction()
	{
		
		//$this->_redirect($this->view->baseUrl() . '/escorts');
    }

    public function editAction(){
        $bc_user = Zend_Auth::getInstance()->getIdentity();
        if ( in_array($bc_user->type, array('superadmin','admin','sales manager', 'moderator plus', 'moderator')) ){ 
            $user_id = (int)$this->_request->getParam('user_id');
            $balance = (float)$this->_request->getParam('balance');
            try{
                $model = new Model_Users();
                $rowData['balance'] = $balance;
                $rowData['id'] = $user_id;
                $item = new Model_UserItem($rowData);
                $model->save($item);
                $return = array('status' => 1,'msg' =>  'Success','data' => $balance);
				
				
				if ( Cubix_Application::getId() == APP_A6 ) {
					file_put_contents('balance.log', Zend_Auth::getInstance()->getIdentity()->username . " - " . $balance . " - " . $user_id . "\n", FILE_APPEND);
				}
				
            }catch (Exception $ex){
                $return = array('status' => 0,'msg' =>  'Error Try Again');
            }
            echo json_encode($return);
            die();
        }
    }	
}
