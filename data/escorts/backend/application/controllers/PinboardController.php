<?php

class PinboardController extends Zend_Controller_Action 
{
	public function init()
	{
		$this->model = new Model_Pinboard();
	}
	
	public function indexAction() 
	{
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('default_pinboard_data');
		$this->view->filter_params = $ses_filter->filter_params;
		/* FILTER PARAMS */
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'id' => $req->id,
			'status' => $req->status,
			'user_type' => $req->user_type,
			'application_id' => $req->application_id,
			'escort_id' => $req->escort_id,
			'showname' => $req->showname,
			'agency_name' => $req->agency_name,
			'username' => $req->username
		);
		
		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		$DEFINITIONS = Zend_Registry::get('defines');
				
		foreach ( $data as $i => $item ) {
			$data[$i]->status = $DEFINITIONS['pinboard_statues'][$item->status];
			$data[$i]->reply_link = '
				<a href="' . $this->view->baseUrl() . '/pinboard-replies?pid=' . $item->id . '" style="display: block; text-align: center;" target="_blank">
					<img src="/img/cubix-button/icons/msg_send.png" />
				</a>
			';
		}
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
	
	public function removeAction()
	{
		$id = intval($this->_request->id);
		$this->model->remove($id);
		
		die;
	}
	
	public function editAction()
	{
		if( ! $this->_request->isPost() )
		{
			$this->view->data = $this->model->get($this->_request->id);
			$this->view->replies = $replies = $this->model->getReplies($this->_request->id);
			$this->view->replies_count = count($replies);
		}
		else 
		{
			$data = new Cubix_Form_Data($this->_request);
						
			$data->setFields(array(
				'id' => 'int',
				'title' => '',
				'post' => ''
			));
			
			$data = $data->getData();
						
			$validator = new Cubix_Validator();
						
			if ( !strlen($data['title']) ) {
				$validator->setError('title', 'Required');
			}
			
			if ( !strlen($data['post']) ) {
				$validator->setError('post', 'Required');
			}
			
			switch ($this->_request->act)
			{
				case 'approve':
					$data['status'] = PINBOARD_STATUS_APPROVED;
					break;
				case 'disable':
					$data['status'] = PINBOARD_STATUS_DISABLED;
					break;
				case 'close':
					$data['status'] = PINBOARD_STATUS_CLOSED;
					break;
			}

			if ( $validator->isValid() ) {
				$this->model->save($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
}
