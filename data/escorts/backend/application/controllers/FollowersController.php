<?php

class FollowersController extends Zend_Controller_Action {
    /* @var $model Model_Followers*/
    protected $model;
	public function init()
	{
		$this->model = new Model_Followers();
	}
	
	public function indexAction() 
	{
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('default_followers_data');
		$this->view->filter_params = $ses_filter->filter_params;
		/* FILTER PARAMS */
	}
	
	public function dataAction()
	{
		
		$req = $this->_request;
		
		$filter = array(
			'user_id' => intval($req->user_id),
			'escort_id' => intval($req->escort_id),
			'agency_id' => intval($req->agency_id),
			'username' => $req->username,
			'email' => $req->email,
			'type' => $req->type
		);

		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);

		foreach ( $data as $i => $item ) {
					
			$data[$i]->follow_type = $item->user_id ? 'registered user' : 'follows by email';
		}
				
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
		
	}
	
	public function editAction()
	{
		if( ! $this->_request->isPost() )
		{
			$follow_events = $this->model->getEvents($this->_request->id);
			$follow_data = $this->model->getFollowData($this->_request->id);
			
			$model_cities = new Model_Geography_Cities();
			$city_event = ($follow_data->type == "escort") ? FOLLOW_ESCORT_NEW_CITY : FOLLOW_AGENCY_NEW_CITY ;
			if(isset($follow_events[$city_event]) && strlen($follow_events[$city_event]['extra']) > 0){
				$cities_array = $this->model->decode($follow_events[$city_event]['extra']);
				$cities = $model_cities->getByIds(implode(',', $cities_array));
			}
			else{
				$cities = array();
			}
			
			$this->view->follow_data = $follow_data;
			$this->view->follow_events = $follow_events;
			$this->view->cities = $cities;
		}
		else 
		{
			$data = new Cubix_Form_Data($this->_request);

			$fields = array(
				'follow_user_id' => 'int',
				'events' => 'arr-int',
				'cities' => 'arr-int',
				'short_comment' => 'notags|special',
			);

			$data->setFields($fields);
			$post_data = $data->getData();
			
			$validator = new Cubix_Validator();
			
			if(count($post_data['events']) == 0){
				$validator->setError('no_event', 'select alert');
			}


			if((in_array(FOLLOW_ESCORT_NEW_CITY,$post_data['events']) || in_array(FOLLOW_AGENCY_NEW_CITY , $post_data['events'] )) && count($post_data['cities']) == 0 ){
				$validator->setError('city_id', 'select city');
			}
			
			if ($validator->isValid() ) {
				$follow_user_data = array('short_comment' => $post_data['short_comment']);
				if($this->_request->lang !== null){
					$follow_user_data['lang'] = $this->_request->lang;
				}
				$this->model->editFollowData($post_data['follow_user_id'], $follow_user_data, $post_data['events'], $this->model->encode($post_data['cities']));
			}
			
			die(json_encode($validator->getStatus()));
		}
	}

	public function editFollowAction()
	{
		$this->view->layout()->disableLayout();
		$model = new Model_Follow();
		
		$req = $this->_request;
				
		if ( $req->isPost() ) {
			$data = new Cubix_Form_Data($this->getRequest());

			$fields = array(
				'follow_user_id' => 'int',
				'user_type' => '',
				'user_type_id' => 'int',
				'events' => 'arr-int',
				'cities' => 'arr-int',
				'short_comment' => 'notags|special',
				'follow_type' => 'int',
				'follow_email' => 'notags|special'
			);

			$data->setFields($fields);
			$post_data = $data->getData();
			
			if(!in_array($post_data['user_type'], array('escort',"agency"))){
				die;
			}
			
			$validator = new Cubix_Validator();
						
			if(count($post_data['events']) == 0){
				$validator->setError('user_type_id', Cubix_I18n::translate('follow_select_alert'));
			}
			elseif(in_array(FOLLOW_ESCORT_NEW_CITY,$post_data['events']) && count($post_data['cities']) == 0 ){
				$validator->setError('user_type_id', Cubix_I18n::translate('follow_select_city'));
			}			
			$result = $validator->getStatus();

			if ( ! $validator->isValid() ) {
				echo(json_encode($result));	die;
			}
			else {
									
				$follow_data = array(
					'user_id' => $this->user->id,
					'type' => $post_data['user_type'],
					'type_id' => $post_data['user_type_id'],
					'short_comment' => $validator->removeEmoji($post_data['short_comment'])
				);
				
				$model->edit($post_data['follow_user_id'], $follow_data, $post_data['events'], $model->encode($post_data['cities']));
											
				echo json_encode($result);
				die;
			}
		}
		else{
			$model_cities = new Model_Cities();
			$follow_user_id = intval($req->follow_user_id);
			$id = $this->view->id = intval($req->id);
			$type = $this->view->type = $req->type;

			if ( ! $id || ! $type ) die;

			$follow_array = $model->getFollowEvents($follow_user_id);
			$this->view->follow_data = $follow_array['data'];
			$this->view->follow_events = $follow_array['events'];
			$this->view->follow_user_id = $follow_user_id;
			
			$city_event = ($type == "escort") ? FOLLOW_ESCORT_NEW_CITY : FOLLOW_AGENCY_NEW_CITY ;
			if(isset($follow_array['events'][$city_event]) && strlen($follow_array['events'][$city_event]['extra']) > 0){
				$cities_array = $model->decode($follow_array['events'][$city_event]['extra']);
				$cities = $model_cities->getByIds(implode(',', $cities_array));
			}
			else{
				$cities = array();
			}
			
			$this->view->cities = $cities;
			$contries_model = new Cubix_Geography_Countries();
			$this->view->countries = $contries_model->ajaxGetAll(false);
		}
	}

	public function toggleEventsAction(){
	    $follow_id = $this->_request->getParam('id');
	    $status = $this->_request->getParam('status');
	    if (intval($follow_id) >0 && in_array($status,[0,1])){
	        $new_follow_status = $status ? 0: 1;
            $this->model->updateFollow($follow_id,array('status'=>$new_follow_status));
            die(json_encode(array(
                'success'=>true,
                'status' => $new_follow_status
            )));
        }else{
	        die(json_encode(array('status'=>false)));
        }

    }
}
