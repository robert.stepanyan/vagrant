<?php

class RetouchPhotosController extends Zend_Controller_Action {

	public function init()
	{
		$this->model = new Model_RetouchPhotos();
	}
	
	public function indexAction() 
	{
		
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'showname' => $req->showname,
			'escort_id' => $req->escort_id,
			'application_id' => $req->application_id
		);

		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);

		$DEFINITIONS = Zend_Registry::get('defines');
		$escortsV2_model = new Model_EscortsV2();
		foreach ($data as $d)
		{
			$stat = array();
			foreach( $DEFINITIONS['escort_status_options'] as $key => $e_status )
			{
				if ( $escortsV2_model->hasStatusBit($d->id, $key) )
				{
					$stat[] = $e_status;
				}
			}

			$d->status = implode(', ', $stat);

		}
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
	
	public function photosListAction()
	{
		$this->view->layout()->disableLayout();
		
		$escort_id = (int) $this->_request->escort_id;
		
		$this->view->photos = $this->model->getRetouchPhotos($escort_id);
		$m_e = new Model_EscortsV2();
		$this->view->showname = $m_e->get($escort_id)->showname;
	}
	
	public function retouchPhotoAction()
	{
		$this->view->layout()->disableLayout();
		
		$photo_id = (int) $this->_request->photo_id;
		
		$this->view->photo = $this->model->getRetouchPhoto($photo_id);
	}
	
	public function replacePhotoAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$escort_id = intval($this->_getParam('escort_id'));		
		$photo_id = intval($this->_getParam('photo_id'));		
	
		$conf = Zend_Registry::get('images_config');
		
		try {
			$m_escort = new Model_EscortsV2();
			$escort = $m_escort->get($escort_id);
			
			$file = Cubix_Utils::HTML5_upload();
			
			$image_size = getimagesize($file['tmp_name']);
			$ext = strtolower(@end(explode('.', $file['name'])));
			
			/* VALIDATING IMAGE SIZE */
			$max_sidesize = $conf['maxImageSize'];
			$min_sidesize = $conf['minImageSize'];

			$new_profile_apps = array(APP_A6, APP_EF, APP_BL, APP_6A, APP_6B, APP_A6_AT);

			if ( in_array(Cubix_Application::getId(), $new_profile_apps) ) {
				$p_min_width = 400;			
				$p_min_height = 600;			
				$l_min_width = 500;
				$l_min_height = 375;

				if ( $image_size[0] < $image_size[1] ) {
					if ( $image_size[0] < $p_min_width || $image_size[1] < $p_min_height ) {
						throw new Exception(Cubix_I18n::translate('sys_error_upload_img_size_min_a6', array('min_width' => $p_min_width, 'min_height' => $p_min_height, 'ratio' => Cubix_I18n::translate('portrait'))), Cubix_Images::ERROR_IMAGE_TOO_SMALL);
					}
				} else {
					if ( $image_size[0] < $l_min_width || $image_size[1] < $l_min_height ) {
						throw new Exception(Cubix_I18n::translate('sys_error_upload_img_size_min_a6', array('min_width' => $l_min_width, 'min_height' => $l_min_height, 'ratio' => Cubix_I18n::translate('landscape'))), Cubix_Images::ERROR_IMAGE_TOO_SMALL);
					}
				}
			} else {
				if ( $image_size[0] < $min_sidesize || $image_size[1] < $min_sidesize ) {
					throw new Exception(Cubix_I18n::translate('sys_error_upload_img_size_min', array('min_sidesize' => $min_sidesize)), Cubix_Images::ERROR_IMAGE_TOO_SMALL);
				}
			}

			if ( $image_size[0] > $max_sidesize || $image_size[1] > $max_sidesize ) {
				throw new Exception(Cubix_I18n::translate('sys_error_upload_img_size_max', array('max_sidesize' => $max_sidesize)), Cubix_Images::ERROR_IMAGE_TOO_BIG);
			}
			/* VALIDATING IMAGE SIZE */
			
			if ( ! $escort ) {
				throw new Exception('Wrong escort id supplied');
			}
			
			$m_ret_photos = new Model_RetouchPhotos();
			$replace_photo = $m_ret_photos->getRetouchPhoto($photo_id);

			$images = new Cubix_Images();
			
			// Remove old photo from server
			$images->remove(new Cubix_Images_Entry(array(
				'ext' => $replace_photo->ext,
				'hash' => $replace_photo->hash,
				'catalog_id' => $escort_id,
				'application_id' => $escort->application_id
			)));
			
			get_headers($conf['remote']['url'] . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $replace_photo->hash);
			
			// Save on remote storage
			$images = new Cubix_Images();
			$image = $images->save($file['tmp_name'], $escort->id, $escort->application_id, $ext, null, ( Cubix_Application::getId() != APP_EF ) ? $replace_photo->hash : null );

			$image = new Cubix_Images_Entry($image);
			$image->setSize('w514');
			$image->setCatalogId($escort_id);

			$is_portrait = 0;
			if ( $image_size ) {
				if ( $image_size[0] < $image_size[1] ) {
					$is_portrait = 1;
				}
			}
			
			$model = new Model_Escort_Photos();
			$photo = array(
				'escort_id' => $escort_id,
				'hash' => $image->getHash(),
				'is_approved' => 1,
				'ext' => $image->getExt(),		
				'is_portrait' => $is_portrait,
				'width' => $image_size[0],
				'height' => $image_size[1],
				'retouch_status' => Model_RetouchPhotos::RETOUCHED,
				'is_main'	=> $replace_photo->is_tmp_main ? 1 : 0
			);
			
			if ( $replace_photo->is_tmp_main ) {
				$m_ret_photos->resetMainPhotos($escort_id);
			}
			
			$photo = $model->update($photo_id, $photo);
			
			//because of cache problem 
			get_headers($photo->getUrl('w514'));
			get_headers($photo->getUrl('backend_thumb_cropper'));
			//
			$status = new Cubix_EscortStatus($escort_id);
			$status->setStatusBit(Cubix_EscortStatus::ESCORT_STATUS_ACTIVE);
			$status->save();

			Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_ADDED, array('id' => $photo->id, 'action' => 'photos uploaded'));
			Cubix_AlertEvents::notify($escort_id, ALERT_ME_NEW_PICTURES, array('id' => $photo->id));
			
			Cubix_LatestActions::addAction($escort_id, Cubix_LatestActions::ACTION_NEW_PHOTO);
			Cubix_LatestActions::addDirectActionDetail($escort_id, Cubix_LatestActions::ACTION_NEW_PHOTO, $photo->id);
			
			$m_snapshot = new Model_Escort_SnapshotV2(array('id' => $escort_id));
			$m_snapshot->snapshotProfileV2();

			$result = array(
				'status' => 'success',
				'photo' => $photo->toJSON('w514'),
				'id' => $photo->id
			);
		} catch (Exception $e) {
			$result = array('status' => 'error', 'msg' => '');
			
			$result['msg'] = $e->getMessage();
		}
		
		echo json_encode($result);
	}
	
	public function downloadAllAction()
	{
		if ($this->_request->isPost())
		{
			$host = Cubix_Application::getById()->host;
			$escort_id = $this->_request->escort_id;
			$showname = $this->_request->showname;
			$photos = $this->_request->photos;
			
			$images_config = Zend_Registry::get('images_config'); 
			
			$url = $images_config['remote']['url'];
			
			$client = new Zend_Http_Client($url . '/download_images.php', array(
				'maxredirects' => 0,
				'timeout'      => 30)
			);
			
			$client->setParameterPost(array(
				'host'  => $host,
				'escort_id'   => $escort_id,
				'showname'   => $showname,
				'photos' => $photos
			));
			
			$response = $client->request('POST');
			
			echo $response->getBody();
			die;
		}
	}
	
	public function noRetouchAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$photo_id = intval($this->_getParam('photo_id'));
		$escort_id = intval($this->_getParam('escort_id'));
		
		$model = new Model_Escort_Photos();
		$model->update($photo_id, array('retouch_status' => Model_RetouchPhotos::NO_NEED_RETOUCH));
		$status = new Cubix_EscortStatus($escort_id);
		$status->setStatusBit(Cubix_EscortStatus::ESCORT_STATUS_ACTIVE);
		$status->save();

		Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_ADDED, array('id' => $photo->id, 'action' => 'photos uploaded'));
		Cubix_AlertEvents::notify($escort_id, ALERT_ME_NEW_PICTURES, array('id' => $photo->id));

		Cubix_LatestActions::addAction($escort_id, Cubix_LatestActions::ACTION_NEW_PHOTO);
		Cubix_LatestActions::addDirectActionDetail($escort_id, Cubix_LatestActions::ACTION_NEW_PHOTO, $photo->id);

		$m_snapshot = new Model_Escort_SnapshotV2(array('id' => $escort_id));
		$m_snapshot->snapshotProfileV2();	
	}
	public function retouchAction()
	{	$this->view->layout()->disableLayout();
		$photo_id = $this->_request->photo_id;
		$escort_id = $this->_request->escort_id;
		$action = $this->_request->action_;
		if(isset($photo_id) && isset($escort_id) && isset($action))
		{	
			if(method_exists($this->model,$action))
			{
				echo $this->model->$action($escort_id,$photo_id);
			}
			die;
		}
	}
}
