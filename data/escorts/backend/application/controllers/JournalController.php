<?php

class JournalController extends Zend_Controller_Action
{
	public function init()
	{
		$this->model = new Model_Journal();
		$this->view->backup_tables = $this->model->getBackupTables();
	}
	
	public function indexAction()
	{
		//var_dump(Zend_Auth::getInstance()->getIdentity());
		//Cubix_SyncNotifier::notify(99, 3);
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$user = Zend_Auth::getInstance()->getIdentity();
		
		$filter = array(
			'e.showname' => $req->showname,
			'e.escort_id' => $req->escort_id,
			'u.username' => $req->username,
			'u.application_id' => $req->app_id,			
			'j.event_type' => $req->event_type,
			'bu.sales_user_id' => $req->sales_user_id,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to,
			'backup_table' => $req->backup_table
		);
		if ( $user->type == 'sales manager' || $user->type == 'sales clerk' ) {
			$filter['sales_id'] = $user->id;
		}
		
		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		foreach ( $data as $i => $item ) {
			if ( strlen($item->data) ) {
				//$data[$i]->data = 'ddd';
				$arr = json_decode($item->data);
				
				$str = '';
				foreach ( $arr as $key => $value ) {
					if(is_numeric($value) || is_string($value)){
						$str .= '<span>' . $key . ': </span> <b>' . $value . '</b><br/>';
					}
				}
				//var_dump($str);
				$data[$i]->data = $str;
			}
		}
		
		if ($count > 50000)
			$count = 50000;
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
}
