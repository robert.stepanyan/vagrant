<?php

class CommentsController extends Zend_Controller_Action {

	const MINIMUM_CHARS_COUNT = 10;

	public function init()
	{
		$this->model = new Model_Comments();
	}
	
	public function indexAction() 
	{
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('default_comments_data');
		$ses_filter->filter_params['status'] = -3;
		$this->view->filter_params = $ses_filter->filter_params;
		/* FILTER PARAMS */
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		$filter = array(
			'showname' => $req->showname,
			'mode' => $req->mode,
			'escort_id' => $req->escort_id,
			'escort_id_f' => $req->escort_id_f,
			'username' => $req->username,
			'application_id' => $req->application_id,
			'status' => $req->status,
			'is_active_escort' => $req->is_active_escort,
			'has_active_package' => $req->has_active_package,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to,
			'is_suspicious' => $req->is_suspicious,
			'comment' => $req->comment,
			'disable_type' => $req->disable_type,
			'is_mobile' => $req->is_mobile
		);
		
		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);

		$DEFINITIONS = Zend_Registry::get('defines');
		$modelRev = new Model_Reviews();
		
		foreach ( $data as $i => $item ) {
			$data[$i]->status = $DEFINITIONS['comment_status_options'][$item->status];
			//$data[$i]->is_suspicious = $DEFINITIONS['comment_suspicious_options'][$item->is_suspicious];
			if ($item->is_suspicious == COMMENT_SUSPICIOUS_USER_IP_DUPLICITY)
				$d = '<a href="http://' . Cubix_Application::getById()->backend_url . '/dashboard/dublicities?member=' . $item->username . '" target="_blank">' . $DEFINITIONS['comment_suspicious_options'][$item->is_suspicious] . '</a>';
			else
				$d = '<a href="http://' . Cubix_Application::getById()->backend_url . '/dashboard/cookie-dublicities?member=' . $item->username . '" target="_blank">' . $DEFINITIONS['comment_suspicious_options'][$item->is_suspicious] . '</a>';
			$data[$i]->is_suspicious = $d;
			$data[$i]->message = mb_substr($item->message, 0, 50, 'UTF-8') . ' ...';

			$data[$i]->c = '';
			$data[$i]->cities = '';

			if ($item->is_m_suspicious)
			{
				if (Cubix_Application::getId() == 1)
					$cl = 'red';
				else
					$cl = 'pink';
				$data[$i]->username = '<span class="' . $cl . ' bold">' . $item->username . '</span>';
				$data[$i]->c = 'c4';
			}
			elseif ($item->new_member)
			{
				if (Cubix_Application::getId() == 1)
					$cl = 'green';
				else
					$cl = 'red';
				$data[$i]->username = '<span class="' . $cl . ' bold">' . $item->username . '</span>';
				$data[$i]->c = 'c1';
			}
			elseif ($this->model->fakeByPhone($item->user_id))
			{
				if (Cubix_Application::getId() == 1)
					$cl = 'blue';
				else
					$cl = 'red';
				$data[$i]->username = '<span class="' . $cl . ' bold">' . $item->username . '</span>';
				$data[$i]->c = 'c2';
			}
			elseif (($cities = $modelRev->fakeByCity($item->user_id)) != false)
			{
				if (Cubix_Application::getId() == 1)
					$cl = 'pink';
				else
					$cl = 'red';
				$data[$i]->username = '<span class="' . $cl . ' bold">' . $item->username . '</span>';
				$data[$i]->c = 'c3';
				$data[$i]->cities = $cities;
			}
		}
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
	
	public function editAction()
	{
		
		if( ! $this->_request->isPost() )
		{
			$modelEscorts = new Model_Escorts();
			$m_d = new Model_Dublicities();
			
			$data = $this->model->getById($this->_request->id);
			$data->ip = Model_Users::getLastLogin($data->user_id)->ip;
			
			$escort = $modelEscorts->get($data->escort_id);
			
			$data->escort_ip = Model_Users::getLastLogin($escort->user_id)->ip;
			$this->view->data = $data;
			$this->view->comment_count = $this->model->getUserCommentsCount($data->user_id);
			$this->view->review_count = $this->model->getUserReviewsCount($data->user_id);
			$this->view->dublCount = $m_d->getForUserCount($data->user_id);
		}
		else 
		{
			
			$data = new Cubix_Form_Data($this->_request);
			$modelEscorts = new Model_Escorts();	
			
			$data->setFields(array(
				'id' => 'int',
				'user_id' => 'int',
				'escort_id' => 'int',
				'status' => 'int',
				'comment' => ''
				
			));
			
			$data = $data->getData();
			
			$escort = $modelEscorts->get($data['escort_id']);	
			
			$validator = new Cubix_Validator();
			$m_user = new Model_Users();
			
			if ( strlen($data['comment']) < self::MINIMUM_CHARS_COUNT ) {
				$validator->setError('comment', 'Minimum ' . self::MINIMUM_CHARS_COUNT . ' chars');
			}
			
			switch ($this->_request->act)
			{
				case 'approve':
					$data['status'] = COMMENT_ACTIVE;
					Cubix_LatestActions::addAction($data['escort_id'], Cubix_LatestActions::ACTION_NEW_COMMENT);
					
					
					if(Cubix_Application::getId() == APP_ED){
						Cubix_FollowEvents::notify($data['escort_id'], 'escort', FOLLOW_ESCORT_NEW_COMMENT, array('rel_id' => $data['id']), true);
						
						 $shortcodes['comment'] = $data['comment'];
				
						if(is_numeric($escort->agency_id)){
							$email_template = 'comment_published_agency_v1';
							$shortcodes['agency'] = $escort->agency_name;
							$shortcodes['escort_showname'] = $escort->showname;
						}else{
							$email_template = 'comment_published_escort_v1';
							$shortcodes['showname'] = $escort->showname;
							$shortcodes['link_to_sedcard'] = 'http://'.Cubix_Application::getById()->host.'/escort/'. $escort->showname.'-'.$escort->id;
						}
						
						Cubix_Email::sendTemplate($email_template, $escort->email, $shortcodes);  
						
						//sending email to the author of COMMENT
						if($us = $m_user->get( $data['user_id'] )){
							$shortcodes['username'] = $us->username;
							$shortcodes['showname'] = $escort->showname;
							$shortcodes['url'] = 'http://'.Cubix_Application::getById()->host.'/escort/'.$escort->showname.'-'.$escort->id;
							Cubix_Email::sendTemplate('comment_published_member_v1', $us->email, $shortcodes);  
						} 
					} 
					break;
				case 'reject':
					$data['status'] = COMMENT_DISABLED;
					break;
			}

			if ( $validator->isValid() ) {
				$this->model->save($data);
				
				if ( isset($data['status']) && $data['status'] == COMMENT_ACTIVE ) {
					Cubix_LatestActions::addDirectActionDetail($data['escort_id'], Cubix_LatestActions::ACTION_NEW_COMMENT, $data['id']);
				}
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	public function replyAction()
	{
		if( ! $this->_request->isPost() )
		{
			$data = $this->model->getById($this->_request->id);
			$data->ip = Model_Users::getLastLogin($data->user_id)->ip;
			$this->view->data = $data;
			$this->view->comment_count = $this->model->getUserCommentsCount($data->user_id);
			$this->view->review_count = $this->model->getUserReviewsCount($data->user_id);
		}
		else
		{
			$data = new Cubix_Form_Data($this->_request);

			$data->setFields(array(
				'id' => 'int',
				'user_id' => 'int',
				'escort_id' => 'int',
				'reply' => ''
			));

			$data = $data->getData();

			$validator = new Cubix_Validator();

			/*if ( strlen($data['comment']) < self::MINIMUM_CHARS_COUNT ) {
				$validator->setError('comment', 'Minimum ' . self::MINIMUM_CHARS_COUNT . ' chars');
			}*/
			if ( ! strlen($data['reply']) ) {
				$validator->setError('reply', 'Required');
			}

			if ( $validator->isValid() ) {
				$this->model->reply($data);
			}

			die(json_encode($validator->getStatus()));
		}
	}
	
	public function removeAction()
	{
		$id = intval($this->_request->id);
		$this->model->remove($id);
		
		die;
	}
	
	public function disabledReasonAction()
	{
		if (!$this->_request->isPost())
		{
			$this->view->data = $data = $this->model->getById($this->_request->id);
		}
		else
		{
			$data = new Cubix_Form_Data($this->_request);

			$data->setFields(array(
				'id' => 'int',
				'disable_type' => 'int',
				'reason' => ''
			));

			$data = $data->getData();
			
			if (!$data['disable_type'])
				$data['disable_type'] = COMMENT_DISABLE_TYPE_INTERNAL;

			$validator = new Cubix_Validator();
			if (Cubix_Application::getId() == APP_6A || Cubix_Application::getId() == APP_EF){
				if ( ! $data['reason'] ) {
					$validator->setError('reason', 'Required');
				}
			}

			if ( $validator->isValid() ) {
				$this->model->disabledReason($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
}
