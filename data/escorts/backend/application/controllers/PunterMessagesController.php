<?php

class PunterMessagesController extends Zend_Controller_Action
{


	private $model;
	protected $user;

	public function init()
	{		
		$this->model = new Model_PunterMessages();
		$this->user = Zend_Auth::getInstance()->getIdentity();
	}


	public function indexAction()
	{
	}

	public function puntersDataAction()
	{
		$data = $this->model->getPunters(
			$count,
			$this->_request->status,
			$this->_request->id,
			$this->_request->user_id,
			$this->_request->page,
			$this->_request->per_page
		);
		echo json_encode(array(
			'data' => $data,
			'count' => $count
			)
		);die;
	}
	
	public function punterEditAction()
	{
		$data = $this->model->getPunterById($this->_request->id);
		$this->view->data = $data;
		
		$user_id = $this->_request->user_id;
		$message = $this->_request->message;
		$status = $this->_request->status;

		if($this->_request->isPost()){
			
			$validator = new Cubix_Validator();

			$validator = new Cubix_Validator();
			if ( !isset($user_id) || $user_id == '') {
				$validator->setError('user_id', 'Required');
			}
			if ( !isset($message) || $message == '') {
				$validator->setError('message', 'Required');
			}
			if ( !isset($status) || $status == '') {
				$validator->setError('status', 'Required');
			}
			if($validator->isValid()){
				$this->model->updatePunter($this->_request->id, $user_id, $message, $status);
			}
			die(json_encode($validator->getStatus()));

		}
	}

	public function punterRemoveAction(){

	}
	
	public function  disableAction()
	{			 
	    
	}

	public function createAction()
	{

	}

}

?>
