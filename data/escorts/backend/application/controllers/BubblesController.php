<?php

class BubblesController extends Zend_Controller_Action {

	public function init()
	{
		$this->model = new Model_Bubbles();
	}
	
	public function indexAction() 
	{
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('default_bubbles_data');
		$this->view->filter_params = $ses_filter->filter_params;
		/* FILTER PARAMS */

		$country_model = new Model_Countries();
		$countries = $country_model->getAll();

		$this->view->rest_countries_id = $this->view->all_countries_id = array();
		foreach($countries as $country){

			if($country->iso == 'us')
				$this->view->usa = $country->id;
			elseif($country->iso == 'fr')
				$this->view->france = $country->id;
			else
				$this->view->rest_countries_id[] = $country->id;

			$this->view->all_countries_id[] = $country->id;

		}

		$this->view->all_countries_id = join(',', $this->view->all_countries_id);
		$this->view->rest_countries_id = join(',', $this->view->rest_countries_id);
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'showname' => $req->showname,
			'escort_id' => $req->escort_id,
			'status' => $req->status,
			'application_id' => $req->application_id,
			'country' => $req->country,
		);

        if(in_array(Cubix_Application::getId(), array(APP_ED)))
        {
            $filter['bubble_text'] = $req->bubble_text;
        }

		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
	
	public function editAction()
	{
		if( ! $this->_request->isPost() )
		{
			$id = $this->_request->id;
			$bubbles = new Model_Bubbles();
			
			$this->view->data = $bubbles->getById($id);			
		}
		else 
		{
			$data = new Cubix_Form_Data($this->_request);
						
			$data->setFields(array(
				'id' => 'int',
				'escort_id' => 'int',
				'text' => ''
			));
			
			$data = $data->getData();
						
			$validator = new Cubix_Validator();
						
			if ( ! strlen($data['text']) ) {
				$validator->setError('text', 'Text is required');
			}

			if ( $validator->isValid() ) {
				$this->model->save($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function removeAction()
	{
		$id = intval($this->_request->id);
		$this->model->remove($id);
		
		die;
	}

    public function bulkRemoveAction()
    {
        $ids = $this->_request->id;
        foreach ( $ids as $id)
        {
            $this->model->remove($id);
        }

        die;
    }

	public function toggleAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->model->toggle($id);
		
		die;
	}
	
	public function setStatusAction()
	{
		$bb_ids = $this->_request->id;
		$status = intval($this->_request->status);
		
		foreach($bb_ids as $bb_id)
		{
			$this->model->setStatus($bb_id, $status);
			if($status == 1){

                $bubbles = new Model_Bubbles();

                $data = $bubbles->getById($bb_id);

                Cubix_SyncNotifier::notify($data->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_BUBBLE_TEXT, array(
                    'new' => $data->text,
                    'action' => 'changed'
                ));
            }
		}
		
		die;
	}
}
