<?php

class PrivateMessagesController extends Zend_Controller_Action
{
	protected $model;

	public function init()
	{
		$this->model = new Model_PrivateMessages();
	}

	public function indexAction()
	{

	}

	public function dataAction()
	{
		$req = $this->_request;
        
		$filter = array(
			'showname'	=> $req->showname, 
			'escort_id' => $req->escort_id, 
			'username'	=> $req->username,
			'status'	=> $req->status,
			'flag'		=> $req->flag
		);
        
		$data = $this->model->getAll(
			$req->page,
			$req->per_page,
			$filter,
			$req->sort_field,
			$req->sort_dir,
			$count
		);
		
		$DEFINITIONS = Zend_Registry::get('defines');
		foreach ( $data as $i => $item ) 
		{
			$data[$i]->status_name = $DEFINITIONS['pm_status'][$item->status];
			$data[$i]->flag_name = $DEFINITIONS['pm_flags'][$item->flag];
			$reciever_data = $this->model->getReceiverData($data[$i]->thread_id,$data[$i]->escort_id, $data[$i]->user_id);
			$data[$i]->reciever =  $reciever_data->showname ? " escort: <strong>". $reciever_data->showname."</strong>" : "member: <strong>". $reciever_data->username."</strong>";
			
		}
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

	public function viewAction()
	{
		$this->view->item = $item = $this->model->get($this->_getParam('id'));
		$this->view->reciever_data = $this->model->getReceiverData($item->thread_id,$item->escort_id, $this->member_id);
		
		if ( $this->_request->isPost() )
		{
			$validator = new Cubix_Validator();
			$act = $this->_request->act;
			
			$data = array('id' => $item->id);
			
			if ($act == 'disable')
			{
				if ($item->status == PM_STATUS_APPROVED)
				{
					$validator->setError('err', 'You can not disable, message already approved.');
				} elseif ($item->status == PM_STATUS_NOT_APPROVED) {
					$data['status'] = PM_STATUS_DISABLED;

					if ( $validator->isValid() ) 
					{
						$this->model->update($data);
					}
				}
			}
			elseif ($act == 'approve')
			{
				$data['status'] = PM_STATUS_APPROVED;
				$data['flag']	= PM_FLAG_SENT;

				if ( $validator->isValid() ) 
				{
					$this->model->update($data);
					
					if ( $validator->isValid() ) 
					{
						$this->model->update($data);
						
						if(Cubix_Application::getId() == APP_6A )
						{
							$receiver_data = $this->model->getReceiverData($item->thread_id,$item->escort_id,$item->user_id);
							
							if($receiver_data->recieve_pm_email){
								$from_username = $item->showname ? $item->showname : $item->username;
								$to_username =  $receiver_data->showname ? $receiver_data->showname : $receiver_data->username;
								$message_text = $receiver_data->recieve_pm_email_text ? $item->message : "";
								$email_data = array(
									'from_username' => $from_username,
									'to_username' => $to_username,
									'message' => $message_text,
								);
								Cubix_Email::sendTemplate('private_message_notification',$receiver_data->email,$email_data);
							}
						}
					}
				}
			}
			
			$this->model->updateLastMessageData($item);
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function deleteAction()
	{
		set_time_limit(0);
		
		if ( $this->_request->isPost() )
		{
			$ids = $this->_request->id;
			
			if (is_array($ids) && count($ids) > 0)
			{
				foreach ($ids as $id)
				{
					$item = $this->model->get($id);
					
					if ($this->_request->act == 'disable')
					{
						if ($item->status == PM_STATUS_NOT_APPROVED)
						{
							$data = array('id' => $item->id);

							$data['status'] = PM_STATUS_DISABLED;

							$this->model->update($data);
						}
					}
					else
					{
						if ($item->status != PM_STATUS_APPROVED)
						{
							$data = array('id' => $item->id);
							$data['status'] = PM_STATUS_APPROVED;
							$data['flag'] = PM_FLAG_SENT;

							$this->model->update($data);
						}
					}
					
					$this->model->updateLastMessageData($item);
				}
			}
		}
		
		die;
	}

    protected function accumulateArchiveAction()
    {
        $this->model->archiveOldConversations();
    }
}
