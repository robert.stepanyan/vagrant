<?php

class BlockedUsernameController extends Zend_Controller_Action
{
	protected $model;

	public function init()
	{
		$this->model = new Model_BlockedUsername();
	}

	public function indexAction()
	{

	}

	public function dataAction()
	{
		$req = $this->_request;

		$filter = array('word' => $req->word );
       
		$data = $this->model->getAll(
			$req->page,
			$req->per_page,
			$filter,
			$req->sort_field,
			$req->sort_dir,
			$count
		);

		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

	public function deleteAction()
	{
        if($this->_request->isPost()){
            $ids = $this->_request->getParam('id');
            if(count($ids) > 0 && $ids){
                foreach( $ids as $id ) {
                    $this->model->delete($id);
                }
            }
        }
        exit;
	}

    public function createAction(){
		
        $this->view->layout()->disableLayout();

        if($this->_request->isPost()){
            $data['word'] = trim($this->_request->getParam('word'));					
			$validator = new Cubix_Validator();
				
			if ( ! strlen($data['word']) ) {
				$validator->setError('word', 'Name is required');
			}
			elseif($this->model->exists($data['word'])){
				$validator->setError('word', 'Word already exist');
			}

			if ( $validator->isValid() ) {
				$this->model->save($data);
			}
			
            die(json_encode($validator->getStatus()));
        }        
    }

    public function editAction(){
		
        $this->view->layout()->disableLayout();

        //$model = new Model_BlacklistedWords();

        $id = $this->_request->getParam('id');
        $this->view->blockedUsername = $blockedWork = $this->model->get($id);
		
        if($this->_request->isPost()){
            $data['word'] = trim($this->_request->getParam('word'));

            
            $validator = new Cubix_Validator();
						
			if ( ! strlen($data['word']) ) {
				$validator->setError('word', 'Name is required');
			}
			elseif($this->model->exists( $data['word'], $id)){
				$validator->setError('word', 'Word already exist');
			}

			if ( $validator->isValid() ) {
				$this->model->save($data, $id);
			}
			
			die(json_encode($validator->getStatus()));
        }

        $this->view->id = $id;
    }
}
