<?php

class VipSpotsController extends Zend_Controller_Action {

	protected $model;

	public function init()
	{
		$this->model = new Model_VipSpots();
	}
	
	public function indexAction() 
	{
		
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		$filter = array(
			'escort_id' => $req->escort_id,
			'country' => $req->country,
			'city' => $req->city
		);

		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

	public function removeAction()
	{
		$id = intval($this->_request->id);
		$this->model->remove($id);
		die;
	}
	
	public function createAction()
	{
		if ( $this->_request->isPost() )
		{
			$data = new Cubix_Form_Data($this->_request);
			$fields = array(
				'escort_id' => '',
				'place' => '',
				'country' => '',
				'city' => '',
				'date_from' => '',
				'date_to' => ''
			);
			$data->setFields($fields);
			$data = $data->getData();
			
			$validator = new Cubix_Validator();
			
			if ( ! strlen($data['place']) ) {
				$validator->setError('place', 'This field is required');
			}

			if ( ! strlen($data['country']) ) {
				$validator->setError('country', 'This field is required');
			}

			if ( ! strlen($data['date_from']) ) {
				$validator->setError('date_from', 'This field is required');
			}

			if ( ! strlen($data['escort_id']) ) {
				$validator->setError('escort_id', 'This field is required');
			}
			else {
				if ( $this->model->checkEscortId($data['escort_id']) ) {
					$validator->setError('escort_id', 'This one already exists');
				}
			}

			if( strlen($data['place']) && strlen($data['city']) ) {
				if ( $this->model->checkPlaceAndCity($data['place'], $data['city']) ) {
					$validator->setError('place', 'This place in this city already exists');
				}
			}
			
			if ( $validator->isValid() ) {
				$this->model->save($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	

}
