<?php

class ImporterController extends Zend_Controller_Action
{
    public $escortData;
    public $measure_units;
    private $errors;
    protected $user;

	public function init()
	{
//        ini_set('display_errors', 1);
//        ini_set('display_startup_errors', 1);
//        error_reporting(E_ALL);

		$this->model = new Model_Importer();
        $this->user = Zend_Auth::getInstance()->getIdentity();
        $this->view->super = $this->user->type=='superadmin'?1:0;
        $this->measure_units = $this->view->measure_units = Cubix_Application::getById($this->_getParam('application_id'))->measure_units;
	}
	
	public function indexAction() 
	{
	    if (Cubix_Application::getId() == APP_ED)
        {
            $this->view->countries = $this->model->getAvailableCountries();
        }
	}
	
	public function dataAction()
	{
		$req = $this->_request;

		$filter = array(
			'website' => $req->website,
			'phone' => str_replace(' ','',$req->phone),
            'status' => $req->status,
            'date_from' => $req->date_from,
            'date_to' => $req->date_to
		);

		if ( Cubix_Application::getId() == APP_ED )
        {
            $filter['country'] = $req->country;
        }

		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);

		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

	public function editAction()
	{
		if (!$this->_request->isPost())
		{
			$m = new Model_Importer();

			$this->view->data = $data = $this->model->getById($this->_request->id);
			$this->view->last_ip = $m->getLastIP($data->escort_id);
//			$this->view->dublCount = $m_d->getForUserCount($data->user_id);
			$this->view->currencies = Model_Currencies::getAll();
		}
		else
		{
			$data = new Cubix_Form_Data($this->_request);

			$fields = array(
//                'import_url' => $req->import_url,
                'website' => $req->website,
                'phone' => $req->phone,
            );

            if(Cubix_Application::getId() == APP_ED){
                $fields['quality'] = 'int';
                $fields['location_review'] = '';
            }

			$data->setFields($fields);

			$data = $data->getData();

			$validator = new Cubix_Validator();

			switch ($this->_request->act)
			{
				case 'approve':
					$data['status'] = REVIEW_APPROVED;
					Cubix_SyncNotifier::notify($data['esc_id'], Cubix_SyncNotifier::EVENT_ESCORT_REVIEW_APPROVED, array('escort_id' => $data['esc_id'], 'review_id' =>$data['id'] ));
					Cubix_LatestActions::addAction($data['esc_id'], Cubix_LatestActions::ACTION_NEW_REVIEW);

					Cubix_LatestActions::addDirectActionDetail($data['esc_id'], Cubix_LatestActions::ACTION_NEW_REVIEW, $data['id']);

					if(Cubix_Application::getId() == APP_ED){
						$m = new Model_Escorts();

						$agency_id = $m->getAgencyId($data['esc_id']);
						$client = new Cubix_Api_XmlRpc_Client();
						$review  = $client->call('Reviews.getReviewbyID', array($data['id']));

						$shortcodes = array(
							'showname' => $review['showname'],
							'link_to_sedcard' => 'http://www.'.Cubix_Application::getById()->host.'/reviews/'.$review['showname'].'-'.$review['id'],
							'agency' => $review['agency_name'],
							'escort_showname' => $review['showname'],
							'username' => $review['author_username']
						);

						if($agency_id){
							Cubix_FollowEvents::notify($agency_id, 'agency', FOLLOW_AGENCY_NEW_REVIEW, array('rel_id' => $data['id'],'rel_id2' => $data['esc_id']), true);
							Cubix_Email::sendTemplate('review_published_agency_v1', $review['owner_email'], $shortcodes);
						}else{
							Cubix_Email::sendTemplate('review_published_escort_v1', $review['owner_email'], $shortcodes);
						}
						Cubix_Email::sendTemplate('review_published_member_v1', $review['author_email'], $shortcodes);

						Cubix_FollowEvents::notify($data['esc_id'], 'escort', FOLLOW_ESCORT_NEW_REVIEW, array('rel_id' => $data['id']) , null, null, true);
					}
					break;
				case 'need_modification':
					$data['status'] = REVIEW_NEED_MODIFICATION;
					Cubix_SyncNotifier::notify($data['esc_id'], Cubix_SyncNotifier::EVENT_ESCORT_REVIEW_NEED_MODIFICATION, array('escort_id' => $data['esc_id'], 'review_id' =>$data['id'] ));
					break;
				case 'reject':
					$data['status'] = REVIEW_DISABLED;
					Cubix_SyncNotifier::notify($data['esc_id'], Cubix_SyncNotifier::EVENT_ESCORT_REVIEW_DISABLED, array('escort_id' => $data['esc_id'], 'review_id' =>$data['id'] ));
					break;
			}

			if ($validator->isValid())
			{
				if (!$data['is_deleted'])
				{
					$this->model->save($data);
				}
			}

			die(json_encode($validator->getStatus()));
		}
	}
	
	public function removeAction()
	{
        $id = $this->_request->id;
        $phone = $this->_request->phone;

        $video_path = './crwl-videos'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/' . $phone . '/' ;
        $image_path = './crwl-images'. (Cubix_Application::getId() == APP_A6 ? '' : '-'.Cubix_Application::getId()).'/' . $phone . '/' ;

        exec ('find '.$video_path.' -type f -exec chmod 0777 {} +');
        exec ('find '.$image_path.' -type f -exec chmod 0777 {} +');

        if (is_dir($video_path))
            self::deleteDir($video_path);

        if (is_dir($image_path))
            self::deleteDir($image_path);


        return $this->model->remove($id);
	}

    public static function deleteDir($dir) {
        $dhandle = opendir($dir);

        if ($dhandle) {
            while (false !== ($fname = readdir($dhandle))) {
                if (is_dir( "{$dir}/{$fname}" )) {
                    if (($fname != '.') && ($fname != '..')) {
                        echo "<p class=success>Deleting Files in the Directory: {$dir}/{$fname}</p>";
                        deleteDir("$dir/$fname");
                    }
                } else {
                    echo "<p class=success>Deleting File: {$dir}/{$fname} </p>";
                    unlink("{$dir}/{$fname}");
                }
            }
            closedir($dhandle);
        }

        rmdir($dir);
    }

    public function createAction()
    {
        $m_agencies = new Model_Agencies();
        $country_model = new Model_Countries();

        $this->view->agency_list = $m_agencies->getList($this->_getParam('application_id'));
        $this->view->countries = $country_model->getPhoneCountries();
        $this->view->block_countries = in_array(Cubix_Application::getId(), array(APP_BL,APP_EI,APP_A6)) ? $country_model->getCountriesForBl() : $country_model->getCountries();

        $id = intval($this->_getParam('id'));

        $model_import = new Model_Importer();
        $crowledData = $model_import->getById($id);

        $phone = $crowledData->phone;
        $phone = str_replace('+41','',$phone);

        $this->view->phone = str_replace(' ','',$phone);
        $data = unserialize($crowledData->data);

        $this->view->data = $data;
        $this->view->importId = $id;
        $this->view->app_country = Model_Applications::getCountryId();

        $filter_templates = array('application_id' => intval(Cubix_Application::getId()) );
        $bu_user = Zend_Auth::getInstance()->getIdentity();

        if (!in_array($bu_user->type, array('superadmin', 'admin')))
            $filter_templates['sales_user_id'] = $bu_user->id;

        $this->view->escort_templates = Model_EscortTemplates::getTemplates($filter_templates);

        if($this->_request->getParam('isFast')){
            $this->renderScript('importer/fast-create.phtml');
        }
    }

    public function importAction()
    {
        $m_agencies = new Model_Agencies();
        $country_model = new Model_Countries();
        $this->view->agency_list = $m_agencies->getList($this->_getParam('application_id'));
        $this->view->countries = $country_model->getPhoneCountries();
        $this->view->block_countries = in_array(Cubix_Application::getId(), array(APP_BL,APP_EI,APP_A6)) ? $country_model->getCountriesForBl() : $country_model->getCountries();

        $this->view->app_country = Model_Applications::getCountryId();
        $filter_templates = array('application_id' => intval(Cubix_Application::getId()) );
        $bu_user = Zend_Auth::getInstance()->getIdentity();
        if (!in_array($bu_user->type, array('superadmin', 'admin')))
            $filter_templates['sales_user_id'] = $bu_user->id;

        $this->view->escort_templates = Model_EscortTemplates::getTemplates($filter_templates);
        $id = intval($this->_getParam('id'));
        if ( $this->_request->isPost() ) {

            $validator = new Cubix_Validator();

            $data = $this->__validate(false, $validator);

            $escort_id = null;

            if ( $validator->isValid() ) {
                unset($data['status']);
                $m_saver = new Model_Escort_SaverV2();


                if ( Cubix_Application::getId() == APP_ED) {
                    $data['backend_escort'] = 1;
                    $data['type'] = $data['login']['type'];

                    if($data['sync_escort'] == 1 && !$data['last_hand_verification_date']){
                        $data['last_hand_verification_date'] = new Zend_Db_Expr('NOW()');
                        $data['hand_verification_sales_id'] = $data['login']['sales_user_id'];
                    }elseif (isset($data['last_hand_verification_date']) && strlen($data['last_hand_verification_date']) == 0)
                    {
                        unset($data['last_hand_verification_date']);
                    }
                }

                $expirationDate = null;
                if( $data['exp_date'] )
                {
                    $expirationDate = $data['exp_date'];
                    unset($data['exp_date']);
                }

                if(in_array(Cubix_Application::getId(), array(APP_ED,APP_EG_CO_UK,APP_EG,APP_AE))){
                    if ( $data['agency_id'] ) {
                        $source_url = $this->_request->source_url;
                        $source_url = str_replace(' ', '', $source_url);
                        if(strlen($source_url) < 1){
                            $source_url = null;
                        }
                        $data['source_url'] = $source_url;
                    }
                }

                $escort_id = $m_saver->save(new Model_EscortV2Item($data));

                $auto_approval = $data['login']['auto_approval'];
                if ( Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT ) {
                    $data['phone_changed'] = 1;
                    unset($data['fake_city_id']);
                    unset($data['fake_zip']);
                }

                if(in_array(Cubix_Application::getId(), array(APP_ED,APP_EG_CO_UK,APP_EG,APP_AE))){
                    unset($data['source_url']);
                }

                unset($data['login']);
                unset($data['vacation']);
                unset($data['tours']);
                unset($data['block_website']);
                unset($data['check_website']);
                unset($data['is_pornstar']);
                unset($data['bad_photo_history']);
                unset($data['stays_verified']);
                unset($data['premium_cities']);
                unset($data['mark_not_new']);
                unset($data['last_hand_verification_date']);
                unset($data['hand_verification_sales_id']);

                $m_escorts = new Model_Escort_ProfileV2(array('id' => $escort_id));

                unset($data['phone_prefix']);

                unset($data['hh_date_from']);
                unset($data['hh_date_to']);
                unset($data['hh_motto']);
                unset($data['hh_save']);
                unset($data['hh_rates']);
                unset($data['has_hh']);
                unset($data['no_renew_sms']);
                unset($data['show_popunder']);

                unset($data['at_exact_date']);
                unset($data['activation_date']);

                //Detecting if escort created through applications sync
                unset($data['sync_escort']);

                unset($data['backend_escort']);
                unset($data['disable_when_tour_ends']);

                $m_escorts->update($data);

                /*if ( Cubix_Application::getId() == APP_ED ) {
                    unset($data['contact_phone_free']);
                }*/

                if ( ! $auto_approval ) {
                    $m_escorts->saveRevision();
                }

                $_status = new Cubix_EscortStatus($escort_id);
                $_status->save();

                //$this->_forward('index', 'cron', 'billing');

                $mOrders = new Model_Billing_Orders();

                $user_id = $this->model->getUserId($escort_id);

                if(in_array(Cubix_Application::getId(),array(APP_ED,APP_EG_CO_UK)))
                {
                    if($expirationDate)
                    {
                        $mu = new Model_Users();
                        $mu->setExpirationDate( $user_id, $expirationDate ,'edit');
                    }
                }

                $mOrders->processUserOrders($user_id);

                if( Cubix_Application::getId() == APP_ED || Cubix_Application::getId() == 33 || Cubix_Application::getId() == 22 ) {
                    $client = Cubix_Api_XmlRpc_Client::getInstance();
                    //$results = $client->call('Escorts.updateTravelCountries', array($escort_id, $this->_request->travel_countries));
                    $results = $client->call('Escorts.updateTravelPlace', array($escort_id, $this->_request->travel_place));
                }

                //Update profile type Single or DUO
                if ( in_array(Cubix_Application::getId(), array(APP_EF, APP_6A)) ) {
                    $profile_type = (int) $this->_request->profile_type;
                    $this->model->updateField($escort_id, 'profile_type', $profile_type);
                }
            }

            $this->view->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender();

            echo json_encode($validator->getStatus() + array('escort_id' => $escort_id));
            return;
        }

        if($this->_request->getParam('isFast')){
            $this->renderScript('importer/fast-import.phtml');
        }
    }


    protected function _validateLogin($edit_mode = false, $validator)
    {
        $escort = new stdClass();
        $blackListModel = new Model_BlacklistedWords();

        //Request from andy for a6. username, password, reg email can be changed
        //Only by admins and super-admins
        $can_change_username = true;

        $data = $this->escortData;

        $data['application_id'] = Cubix_Application::getId();


        if ( ! $data['photo_auto_approval'] ) {
            $data['photo_auto_approval'] = 0;
        }

        $data['login']['photo_auto_approval'] = $data['photo_auto_approval'];
        unset($data['photo_auto_approval']);



        if ( ! $can_change_username && $escort->email != $data['reg_email'] ) {
            $this->errors[] = array('reg_email', 'No permissions');
        } elseif ( ! strlen($data['reg_email']) ) {
            $this->errors[] = array('reg_email', 'Required');
        }
        elseif(!$validator->isValidEmail($data['reg_email'])){
            $this->errors[] = array('reg_email', 'Invalid Email');
        }
        elseif ( strlen($data['email']) && Cubix_Application::isDomainBlacklisted($data['email']) ) {
            $this->errors[] = array('email', 'Domain is blacklisted');
        }

        $data['login']['reg_email'] = $data['reg_email'];
        unset($data['reg_email']);

        if ( ! $data['admin_verified'] ) {
            $data['admin_verified'] = 1;
        }

        if ( ! $data['auto_approval'] ) {
            $data['auto_approval'] = 0;
        }

        if ( ! $data['im_is_blocked'] ) {
            $data['im_is_blocked'] = 0;
        }

        if ( ! $data['slogan'] ) {
            $data['slogan'] = null;
        }
        else{
            $data['slogan'] = $validator->removeEmoji($data['slogan']);
        }

        if ( ! $data['is_suspicious'] ) {
            $data['is_suspicious'] = 0;
            $data['suspicious_comment'] = null;
            $data['susp_comment_show'] = 0;
        }

        else if ( $data['is_suspicious'] && ! $data['suspicious_comment'] ) {
            $this->errors[] = array('suspicious_comment', 'Required');
        }
        else
            $data['susp_comment_show'] = intval($data['susp_comment_show']);




        if ( ! $data['free_comment'] ) {
            $data['free_comment'] = null;
        }

        if ( ! $data['due_date'] ) {
            $data['due_date'] = null;
        }

        $escort_id = $this->_getParam('id');

        $model_packages = new Model_Packages();
        $bu_user = Zend_Auth::getInstance()->getIdentity();
        if ( ! strlen($data['showname']) ) {
            $this->errors[] = array('showname', 'Required');
        }
        elseif ( mb_strlen($data['showname']) > 25  ) {
            $this->errors[] = array('showname', 'Showname must not be <br/> longer then 25 chars.');
        }
        elseif ( ! preg_match('/^[-_a-z0-9\s]+$/i', $data['showname']) ) {
            $this->errors[] = array('showname', 'Allowed only: -, _, a-z, 0-9');
        }
        if(Cubix_Application::getId() == APP_A6){
            if($model_packages->hasActivePackage($escort_id) && $this->model->getShownameByEscortId($escort_id) != $data['showname'] && $bu_user->type != 'superadmin' ){
                $this->errors[] = array('showname', 'Has Active Package');
            }
        }



        if ( ! $data['agency_id'] ) {

            if ( ! $can_change_username && $escort->username != $data['username'] ) {
                $this->errors[] = array('username', 'No permissions');
            } elseif ( ! strlen($data['username']) ) {
                $this->errors[] = array('username', 'Required');
            }
            elseif ( $this->model->existsByUsername($data['username']) ) {
                    $escort->username = $data['username'].'_girl';
//                    $this->errors[] = array('username', 'Already exists');
            }

            if ( ! $can_change_username && strlen($data['password']) ) {
                $this->errors[] = array('password', 'No permissions');
            } elseif ( ! $edit_mode && ! strlen($data['password']) ) {
                $this->errors[] = array('password', 'Required');
            }
            elseif (strlen($data['password']) && $data['password'] == $data['username']){
                $this->errors[] = array('password', "Can't use username as password");
            }
        }
        $bc_user = Zend_Auth::getInstance()->getIdentity();
        if($bc_user->type != 'moderator' && $bc_user->type != 'moderator plus'){
            if ( ! $data['sales_user_id'] && ! $data['agency_id'] ) {
                $this->errors[] = array('sales_user_id', 'Required');
            }
        }



        if ( ! $data['country_id'] ) {
            $this->errors[] = array('country_id', 'Required');
        }


        $data['login']['username'] = $data['username'];
        unset($data['username']);
        $data['login']['password'] = $data['password'];
        unset($data['password']);
        $data['login']['sales_user_id'] = $data['sales_user_id'];
        unset($data['sales_user_id']);
        $data['login']['data_entry_id'] = $data['data_entry_id'];
        unset($data['data_entry_id']);
        $data['login']['agency_id'] = $data['agency_id'];
        unset($data['agency_id']);
        $data['login']['pseudo_escort'] = $data['pseudo_escort'];
        unset($data['pseudo_escort']);
        $data['login']['application_id'] = $data['application_id'];
        unset($data['application_id']);
        $data['login']['id'] = $data['id'];
        unset($data['id']);
        $data['login']['is_suspicious'] = $data['is_suspicious'];
        unset($data['is_suspicious']);
        $data['login']['susp_comment_show'] = $data['susp_comment_show'];
        unset($data['susp_comment_show']);
        $data['login']['suspicious_comment'] = $data['suspicious_comment'];
        unset($data['suspicious_comment']);
        $data['login']['slogan'] = $data['slogan'];
        //unset($data['slogan']);
        $data['login']['auto_approval'] = $data['auto_approval'];
        unset($data['auto_approval']);

        $data['login']['im_is_blocked'] = $data['im_is_blocked'];
        unset($data['im_is_blocked']);

        return $data;
    }

    protected function _validateBiography($edit_mode = false, $validator)
    {
        $data = $this->escortData;
        $blackListModel = new Model_BlacklistedWords();
        if ( ! $data['gender'] ) {
            $this->errors[] = array('gender', 'Required');
        }

        if(strlen($data['characteristics']) > 0){
            $data['characteristics'] = $validator->removeEmoji($data['characteristics']);
            if($blackListModel->checkWords($data['characteristics'] ,Model_BlacklistedWords::BL_TYPE_SPECIAL_CHAR )){
                $this->errors[] = array('characteristics', 'Has Bl. word '.$blackListModel->getWords() );
            }
        }

        if (!$data['birth_date']) {
            if (!in_array($this->user->type, ['admin', 'superadmin']) && array_key_exists('birth_date', $data))
                unset($data['birth_date']);
        }
        else {
            if( is_numeric($data['birth_date']) ){
                //birth day is a age of escort and we generate birthday, very stupid and old shit
                //$data['birth_date'] = $data['birth_date'] ? new Zend_Db_Expr('FROM_UNIXTIME(' . $data['birth_date'] . ')') : null;
                $data['birth_date'] = strtotime(date('Y-m-d', strtotime(date('Y') - $data['birth_date'] . '-01-01') ));
            }elseif (strlen($data['birth_date']) == 0)
            {
                unset($data['birth_date']);
            }
        }

        foreach ( $data['block_countries'] as $i => $zone_id ) {
            if ( $zone_id != '0' ) {
                $data['block_countries'][$i] = array('country_id' => $zone_id);
            }
            else {
                unset($data['block_countries'][$i]);
            }
        }

        /*if ( ! $data['measure_units'] && ($data['height'] || $data['weight']) ) {
            $this->errors[] = array('measure_units', 'Required');
        }*/

        $data['measure_units'] = METRIC_SYSTEM;
        if ( $this->measure_units == ROYAL_SYSTEM ) {
            $data['measure_units'] = ROYAL_SYSTEM;
            if ( $data['height'] ) {
                $rx = '/^([0-9]+)(\s*\'\s*([0-9]+)\s*(\"|\'\')?\s*)?$/';
                if ( ! preg_match($rx, $data['height'], $a) ) {
                    $this->errors[] = array('height', 'invalid format');
                }
                else {
                    $data['height'] = Cubix_UnitsConverter::convert($data['height'], 'ftin', 'cm');
                }
            }

            if ( $data['weight'] ) {
                $data['weight'] = Cubix_UnitsConverter::convert($data['weight'], 'lbs', 'kg');
            }
        }

        $data['height'] = intval($data['height']);
        if ( ! $data['height'] ) $data['height'] = null;
        if ( $this->measure_units != ROYAL_SYSTEM ) {
            $data['weight'] = intval($data['weight']);
        }
        if ( ! $data['weight'] ) $data['weight'] = null;

        $i18n_data = Cubix_I18n::getValues($this->_request->getParams(), 'about', '');
        $a_exits_data = array();
        $security = new Cubix_Security();
        foreach ( $i18n_data as $field => $value ) {
            if (strlen($value)) $a_exits_data[] = $field;

            if ( Cubix_Application::getId() != APP_A6 && Cubix_Application::getId() != APP_A6_AT ) {
                $value = $security->clean_html($value);
            }
            else {
                $value = strip_tags($value);
            }

            if($blackListModel->checkWords($value,Model_BlacklistedWords::BL_TYPE_ABOUT )){
                $this->errors[] = array('about_types', 'Has Bl. word '.$blackListModel->getWords() );
            }
            if(Cubix_Application::getId() == APP_EF){

                $value = preg_replace('/[[:^print:]]/', '', $value);
                $wrong_letter['symbol'][] = $this->checkSymbols($value);

                $checkedData = trim($this->stripAsci(strip_tags($value)));

                for ($i = 0 ;$i < strlen($checkedData);$i++){
                    if(!(mb_detect_encoding($checkedData[$i],'UTF-8'))){
                        $prev_letter = ($i == 0) ? "":mb_substr($checkedData,strpos($checkedData,$checkedData[$i]) - 1,1,'utf-8');
                        $next_letter = ($i == strlen($checkedData))?"" : mb_substr($checkedData,strpos($checkedData,$checkedData[$i]) + 1,1,'utf-8');
                        $current_letter  = mb_substr($checkedData,strpos($checkedData,$checkedData[$i]),1,'utf-8');
                        $wrong_letter['letters'] = $prev_letter .''. $current_letter.''.$next_letter;
                        break;
                    }
                }


                $checkSymbol = implode($wrong_letter['symbol']);

                if($wrong_letter['letters']){

                    $this->errors[] = array('about_types', Cubix_I18n::translate('error_language_letter',array('LENGTH'=>200)).' - '. utf8_encode($wrong_letter['letters']));
                }
                elseif(strlen($checkSymbol) > 1){
                    $this->errors[] = array('about_types', Cubix_I18n::translate('error_symbol',array('LENGTH'=>200)) . ' - '.$checkSymbol);
                }

            }

            $value = $validator->removeEmoji($value);
            $i18n_data[$field] = $security->xss_clean($value);

        }

        if (in_array(Cubix_Application::getId(), array(APP_ED, APP_EG_CO_UK, APP_6B)))
        {

            if (!count($a_exits_data) && $data['sync_escort'] != 1)
                $this->errors[] = array('about_types', 'About me required' );
        }

        $data = array_merge($data, $i18n_data);

        if (Cubix_Application::getId() == APP_A6)
        {
            $data['about_anibis'] = $this->_request->about_anibis;
        }

        if (Cubix_Application::getId() == APP_6A || Cubix_Application::getId() == APP_EF)
        {
            $i18n_data_soft = Cubix_I18n::getValues($this->_request->getParams(), 'about_soft', '');
            foreach ( $i18n_data_soft as $field => $value ) {
                $value = strip_tags($value);
                $i18n_data[$field] = $security->xss_clean($value);
            }
            $data = array_merge($data, $i18n_data_soft);

        }
        /*if ( strlen($datap['bust_waist_hip']) && ! preg_match('#^[0-9]+-[0-9]+-[0-9]+$#', $data['bust_waist_hip']) ) {
            $this->errors[] = array('bust_waist_hip', 'Wrong format<br/> (90-60-90)');
        }*/

        $langs = array();
        if ( ! is_array($data['langs']) || ! count($data['langs']) ) {
            if ( Cubix_Application::getId() != APP_A6 && Cubix_Application::getId() != APP_A6_AT ) {
                $this->errors[] = array('langs', 'At least one is required');
            }
        }
        else {
            foreach ( $data['langs'] as $i => $lang ) {
                $arr = explode(':', $lang);
                $langs[$i] = array('lang_id' => $arr[0], 'level' => $arr[1]);
            }
            unset($data['langs']);
            $data['langs'] = $langs;
        }

        return $data;
    }

    protected function _validateServices($edit_mode = false, $validator)
    {
        $data = $this->escortData;

        $DEFINITIONS = Zend_Registry::get('defines');

        if ( is_array($data['services']) ) {
            foreach ( $data['services'] as $i => $svc ) {
                $data['services'][$i] = array('service_id' => $svc);
                if ( $data['service_prices'][$svc] ) {
                    $price = floatval($data['service_prices'][$svc]);
                    $currency = (isset($data['service_currencies'][$svc])) ? $data['service_currencies'][$svc] : $this->view->currency('id');
                    if ( $price && $price > 0 ) {
                        $data['services'][$i] = array_merge($data['services'][$i], array('price' => $price, 'currency_id' => $currency));
                    }
                }
            }
        }
        unset($data['service_prices']);
        unset($data['service_currencies']);

        if ( is_array($data['sex_availability']) ) {
            foreach ( $data['sex_availability'] as $i => $opt ) {
                if ( ! isset($DEFINITIONS['sex_availability_options'][$opt]) ) {
                    unset($data['sex_availability'][$i]);
                }
            }
            $data['sex_availability'] = count($data['sex_availability']) ?
                implode(',', $data['sex_availability']) : null;
        }

        $i18n_data = Cubix_I18n::getValues($this->_request->getParams(), 'additional_service', '');
        foreach ( $i18n_data as $field => $value ) {
            $i18n_data[$field] = htmlspecialchars(strip_tags(trim($value)));
        }
        $data = array_merge($data, $i18n_data);

        return $data;
    }

    protected function _validateWorkingTimes($edit_mode = false, $validator)
    {
        $data = $this->escortData;

        $_data = array('available_24_7' => $data['available_24_7'],'night_escort' => $data['night_escort'], 'times' => array());

        for ( $i = 1; $i <= 7; $i++ ) {
            if ( isset($data['day_index'][$i]) && isset($data['time_from'][$i]) && isset($data['time_from_m']) && isset($data['time_to'][$i]) && isset($data['time_to_m'][$i]) ) {
                $night_escort =  in_array($i,$data['night_escorts'])?1:0;
                $_data['times'][] = array(
                    'day_index' => $i,
                    'time_from' => $data['time_from'][$i],
                    'time_from_m' => $data['time_from_m'][$i],
                    'time_to' => $data['time_to'][$i],
                    'time_to_m' => $data['time_to_m'][$i],
                    'night_escorts' =>$night_escort
                );
            }
        }

        return $_data;
    }

    protected function _validateWorking($edit_mode = false, $validator)
    {

        $data = $this->escortData;

        $app = Cubix_Application::getById(Cubix_Application::getId());

        if ( count($data['locations']) > $app->max_working_cities ) {
            $this->errors[] = array('locations', 'You can add maximum ' . $app->max_working_cities . ' working locations');
        }

        if ( ! $data['base_city_id'] ) {
            //if ( Cubix_Application::getId() != APP_A6 && Cubix_Application::getId() != APP_A6_AT ) {
            $this->errors[] = array('locations', 'Please select the base city below');
            //}
        }

        $cities = array();
        if ( $data['locations'] ) {
            foreach($data['locations'] as $i => $city) {
                $cities[$i] = array('city_id' => $city);
            }
        }
        $data['cities'] = $cities;
        unset($data['locations']);

        $data['city_id'] = $data['base_city_id'];
        unset($data['base_city_id']);

        if ( $data['incall_type'] == 2 && ! $data['incall_hotel_room'] ) {
            $this->errors[] = array('err_incall', 'Please select hotel room');
        }

        $cityzones = array();
        if ($data['z-locations'] && count($data['z-locations']) ) {
            foreach($data['z-locations'] as $i => $zone) {
                if ($zone == 0) {
                    break;
                }
                $cityzones[$i] = array('city_zone_id' => $zone);
            }
        }
        $data['cityzones'] = $cityzones;
        unset($data['z-locations']);


        if ( ! $data['incall'] ) {
            $data['incall_type'] = null;
            $data['incall_hotel_room'] = null;
            $data['incall_other'] = null;
            $data['zip'] = null;
        }
        else{
            if ( !$data['incall_type'] ) {
                $this->errors[] = array('err_incall', 'Please select Incall suboption');
            }
        }
        unset($data['incall']);

        if ( ! $data['outcall'] ) {
            $data['outcall_type'] = null;
            $data['outcall_other'] = null;
        }
        unset($data['outcall']);

        $vac_date_from = $data['vac_date_from'];
        $vac_date_to = $data['vac_date_to'];

        if ( ($vac_date_from && ! $vac_date_to) || ($vac_date_to && ! $vac_date_from) || ($vac_date_to < $vac_date_from) ) {
            $this->errors[] = array('vac_date', 'Wrong interval');
        }

        $data['vacation'] = array(
            'vac_date_from' => $data['vac_date_from'],
            'vac_date_to' => $data['vac_date_to'],
        );

        $escort_id = $this->_getParam('id');

        $modelPackages = new Model_Billing_Packages();
        $result = $modelPackages->checkIfHasAddAreas($escort_id);

        if ( $result ) {
            if ( ! count($data['premium_cities']) ) {
                $this->errors[] = array('premium_cities', 'Please select at least one area!');
            }

            if ( count($data['premium_cities']) > $result->add_areas_count ) {
                $this->errors[] = array('premium_cities', "You can select only " . $result->add_areas_count . " area!");
            }
        }

        unset($data['vac_date_from']);
        unset($data['vac_date_to']);

        //print_r($data);die;

        return $data;
    }

    protected function _validateContacts($edit_mode = false, $validator)
    {

        $data = $this->escortData;

        $data['show_phone'] =  $data['show_phone'] == 0 ? 2 : 1;

        $escort_id = intval($this->_getParam('id'));

        if ( strlen($data['email']) && Cubix_Application::isDomainBlacklisted($data['email']) ) {
            $this->errors[] = array('email', 'Domain is blacklisted');
        }

        if ( strlen($data['website']) && Cubix_Application::isDomainBlacklisted($data['website']) ) {
            $this->errors[] = array('website', 'Domain is blacklisted');
        }
        elseif (strlen($data['website']) && strtolower(substr($data['website'], 0, 7)) != 'http://' && strtolower(substr($data['website'], 0, 8)) != 'https://' && strtolower(substr($data['website'], 0, 4)) != 'www.')
        {
            $this->errors[] = array('website', 'Website must start with http://, https:// or www.');
        }


        $blackListModel = new Model_BlacklistedWords();
        if($blackListModel->checkWords($data['phone_instr_other'] ,Model_BlacklistedWords::BL_TYPE_OTHER )){
            $this->errors[] = array('phone_instr_other', 'Has Bl. word '.$blackListModel->getWords() );
        }
        if ( Cubix_Application::getId() != APP_A6 && Cubix_Application::getId() != APP_A6_AT ) {
            unset($data['fake_city_id']);
            unset($data['fake_zip']);
        }

        if ( Cubix_Application::getId() != APP_ED ) {
            unset($data['prefered_contact_methods']);
        }else{
            if(isset($data['prefered_contact_methods'])){
                foreach ($data['prefered_contact_methods'] as $k => $method) {
                    if(!strlen($method)){
                        unset($data['prefered_contact_methods'][$k]);
                    }
                }
                foreach ($data['prefered_contact_methods'] as $method) {
                    if($method == 6){
                        $data['prefered_contact_methods'] = array('6');
                        break;
                    }
                }
                $data['prefered_contact_methods'] = serialize(array_unique(array_values($data['prefered_contact_methods'])));
            }
        }

        $data['contact_phone_parsed'] = $data['phone_exists'] = $data['phone_country_id'] = null;
        if ( $data['phone_number'] ) {
            if ( ! preg_match("/^[0-9\s\+\-\(\)]+$/i", $data['phone_number']) ) {
                $this->errors[] = array('email_phone_error', 'Invalid phone number');
            }
            elseif(preg_match("/^(\+|00)/", $data['phone_number']) ) {
                $this->errors[] = array('email_phone_error', 'Please enter phone number without country calling code');
            }
            if ( ! ($data['phone_prefix']) ) {
                $this->errors[] = array('email_phone_error', 'Country calling code Required');
            }
            list($country_id, $phone_prefix,$ndd_prefix) = explode('-',$data['phone_prefix']);

            if($data['phone_number']){
                $data['contact_phone_parsed'] = preg_replace('/[^0-9]/', '', $data['phone_number']);
                $data['contact_phone_parsed'] = preg_replace('/^'.$ndd_prefix.'/', '', $data['contact_phone_parsed']);
                $data['contact_phone_parsed'] = '00'.$phone_prefix.$data['contact_phone_parsed'];

                if ( Cubix_Application::getId() == APP_ED ) {
                    $data['contact_phone_free'] = preg_replace('/[^0-9]/', ' ', $data['phone_number']);
                    //$data['contact_phone_free'] = preg_replace('/^'.$ndd_prefix.'/', '', $data['contact_phone_free']);
                    //$data['contact_phone_free'] = '00'.$phone_prefix.$data['contact_phone_free'];
                }
            }else{
                $data['contact_phone_parsed'] = null;

                if ( Cubix_Application::getId() == APP_ED ) {
                    $data['contact_phone_free'] = null;
                }
            }
            $data['phone_country_id'] = 1;
            $data['phone_exists'] = $data['contact_phone_parsed'];

            $results = $this->model->existsByPhone($data['phone_number']);
            if($results) {
                $this->errors[] = array('phone_error', 'Phone already exists');
            }



        }

        if ( $data['phone_additional_1'] ) {
            if ( ! preg_match("/^[0-9\s\+\-\(\)]+$/i", $data['phone_additional_1']) ) {
                $this->errors[] = array('email_phone_error_1', 'Invalid phone number');
            }

            elseif(preg_match("/^(\+|00)/", $data['phone_additional_1']) ) {
                $this->errors[] = array('email_phone_error_1', 'Please enter phone number without country calling code');
            }
            if ( ! ($data['phone_prefix_1']) ) {
                $this->errors[] = array('email_phone_error_1', 'Country calling code Required');
            }

            list($country_id, $phone_prefix, $ndd_prefix) = explode('-',$data['phone_prefix_1']);
            $data['phone_country_id_1'] = $country_id;
        }

        if ( $data['phone_additional_2'] ) {
            if ( ! preg_match("/^[0-9\s\+\-\(\)]+$/i", $data['phone_additional_2']) ) {
                $this->errors[] = array('email_phone_error_2', 'Invalid phone number');
            }

            elseif(preg_match("/^(\+|00)/", $data['phone_additional_2']) ) {
                $this->errors[] = array('email_phone_error_2', 'Please enter phone number without country calling code');
            }
            if ( ! ($data['phone_prefix_2']) ) {
                $this->errors[] = array('email_phone_error_2', 'Country calling code Required');
            }

            list($country_id, $phone_prefix, $ndd_prefix) = explode('-',$data['phone_prefix_2']);
            $data['phone_country_id_2'] = $country_id;
        }

        //        if(!$data['block_website']){
        //            $data['block_website'] = null;
        //        }



        return $data;
    }

    protected function __validate($edit_mode = false, $validator)
    {
        $data = array();
        $this->errors = array();

        $login_data = $this->_validateLogin($edit_mode, $validator);
        $biography_data = $this->_validateBiography($edit_mode, $validator);
        $services_data = $this->_validateServices($edit_mode, $validator);
        $working_times_data = $this->_validateWorkingTimes($edit_mode, $validator);
        $working_data = $this->_validateWorking($edit_mode, $validator);
        $contact_data = $this->_validateContacts($edit_mode, $validator);

        $data = array_merge(
            $data,
            $login_data,
            $biography_data,
            $services_data,
            $working_times_data,
            $working_data,
            $contact_data
        );

        return $data;
    }

    public function importEscortsFromJsonAction()
    {
        set_time_limit(0);
        ini_set('memory_limit', '1024M');
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $cli = new Cubix_Cli();
        $pid_file = '/var/run/import-json' . Cubix_Application::getId() . '.pid';
        $cli->setPidFile($pid_file);

        if ($cli->isRunning()) {
            $cli->out('Action is already running... exitting...');
            exit(1);
        }

        $DEFINITIONS = Zend_Registry::get('defines');

        $auth = Zend_Auth::getInstance();
        $adapter = new Model_Auth_Adapter('importer', 'zd2zTXb4X3SnWUJ', 16, false);
        $auth->authenticate($adapter);
        $this->user = Zend_Auth::getInstance()->getIdentity();

//        $file_data = file_get_contents('./crwl-json/facegirl-10.json');
        $i = 6;
        do{
            $file_data = file_get_contents('./crwl-json/facegirl-newpictures-'.$i.'.json');
            $jsoned_data = json_decode($file_data);
            foreach ($jsoned_data as $escort_data)
            {
                $this->errors = array();
                switch ($escort_data->gender) {
                    case "Men":
                        $gender = 2;
                        break;
                    case "trans":
                        $gender = 3;
                        break;
                    default:
                        $gender = 1;
                        break;
                }
                if (preg_match('/Ts/',$escort_data->name))
                {
                    $gender = 3;
                }
                $escort_data->name = preg_replace('/[^a-zA-Z0-9\']/', '', $escort_data->name);
                $escort_data->name = str_replace("'", '', $escort_data->name);

                $phone = str_replace('+41','',$escort_data->contactinformation->phonenumber);
                $phone = str_replace(' ','',$phone);

                $city_model = new Model_Geography_Cities();
                if (in_array($escort_data->contactinformation->regions[0],array('Plainpalais','Genf','Cornavin','Eaux-vives','Servette','Champel')))
                {
                    $city_id = 47;
                }elseif ($escort_data->contactinformation->regions[0] == 'Lausanne'){
                    $city_id = 48;
                }elseif ($escort_data->contactinformation->regions[0] == 'La Chaux-de-Fonds'){
                    $city_id = 49;
                }elseif ($escort_data->contactinformation->regions[0] == 'Renens'){
                    $city_id = 48;
                }elseif (in_array($escort_data->contactinformation->regions[0],array('Neuenburg','Neuchâtel'))){
                    $city_id = 49;
                }elseif ($escort_data->contactinformation->regions[0] == 'Vétroz'){
                    $city_id = 26;
                }elseif ($escort_data->contactinformation->regions[0] == 'Châtel-Saint-Denis'){
                    $city_id = 51;
                }elseif ($escort_data->contactinformation->regions[0] == 'Freiburg'){
                    $city_id = 22;
                }else{
                    $city_id = $city_model->getIdByTitleEn(str_replace(' ','',$escort_data->contactinformation->regions[0]));
                }
                if (count($escort_data->services))
                {
                    $services = array();
                    $servicesNames = $DEFINITIONS['services'];
                    foreach ($servicesNames as $group)
                    {
                        foreach ($group as $key => $name)
                        {
                            foreach ($escort_data->services as $service)
                            {
                                foreach (explode(' ',$service) as $sv)
                                {
                                    if (preg_match('/'.strtolower($sv).'/',strtolower($name)))
                                        $services[] = array('service_id' => $key);
                                }

                            }
                        }
                    }
                }
                $about_me = '';
                if ($escort_data->description)
                {
                    $validator = new Cubix_Validator();
                    $about_me = $validator->specialCharsCleaner($escort_data->description);
                }

                $langs = array();
                $langs[] = array('lang_id' => 'fr','level' => 3);

                $data = $arrayData = array(
                    'showname' => $escort_data->name,
                    'username' => str_replace(' ','_',strtolower($escort_data->name)),
                    'sales_user_id' => 76,
                    'password' => str_replace(' ','_',strtolower($escort_data->name)).str_replace('+','',$phone),
                    'reg_email' => 'no-2@no.com',
                    'country_id' => 1,
                    'phone_number' => $phone,
                    'base_city_id' => $city_id,
                    'gender' => intval($gender),
                    'birth_date' => strtotime(date('Y-m-d', strtotime(date('Y') - $escort_data->age . '-01-01') )),
                    'services' => $services,
                    'about_fr' => $about_me,
                    'phone_prefix' => '1-41-0',
                    'check_website' => 0,
                    'stays_verified' => 0,
                    'pseudo_escort' => 1,
                    'block_website' => 0,
                    'langs' => $langs,
                    'show_popunder' => 0,
                    'bad_photo_history' => 0,
                    'is_pornstar' => 0,
                    'night_escort' => 0,
                    'locations' => array($city_id),
                    'phone_additional_2' => '',
                    'incall' => 0,
                    'outcall' => 0,
                    'disable_phone_prefix' => 1,
                    'disable_phone_prefix_1' => 0,
                    'disable_phone_prefix_2' => 0,
                    'phone_sms_verified' => 0,
                    'viber' => 0,
                    'show_phone' => 1
                );
                $this->escortData = $data;
                $validator = new Cubix_Validator();

                $data = $this->__validate(false, $validator);
                $escort_id = null;

                if ( empty($this->errors) && $city_id) {

                    unset($data['status']);
                    $m_saver = new Model_Escort_SaverV2();

                    $escort_id = $m_saver->save(new Model_EscortV2Item($data));

                    $data['phone_changed'] = 1;

                    unset($data['login']);
                    unset($data['vacation']);
                    unset($data['block_website']);
                    unset($data['check_website']);
                    unset($data['stays_verified']);
                    unset($data['base_city_id']);
                    unset($data['username']);
                    unset($data['sales_user_id']);
                    unset($data['password']);
                    unset($data['reg_email']);
                    unset($data['check_website']);
                    unset($data['locations']);
                    unset($data['incall']);
                    unset($data['outcall']);
                    unset($data['disable_phone_prefix']);
                    unset($data['disable_phone_prefix_1']);
                    unset($data['disable_phone_prefix_2']);
                    unset($data['phone_additional_2']);
                    unset($data['phone_sms_verified']);
                    unset($data['viber']);
                    unset($data['show_phone']);

                    $m_escorts = new Model_Escort_ProfileV2(array('id' => $escort_id));

                    unset($data['phone_prefix']);
                    unset($data['show_popunder']);

                    $m_escorts->update($data);

                    if ( true ) {
                        $m_escorts->saveRevision();
                    }

                    $_status = new Cubix_EscortStatus($escort_id);
                    $_status->save();

                    $mOrders = new Model_Billing_Orders();

                    $user_id = $this->model->getUserId($escort_id);


                    $mOrders->processUserOrders($user_id);

                    if(count($escort_data->photos))
                    {
                        foreach ($escort_data->photos as $photo)
                        {
                            $decoded = base64_decode($photo->base64img);
                            if (!is_dir('./crwl-images/')) {
                                mkdir('./crwl-images/');
                                chmod('./crwl-images/',0777);
                            }
                            if (is_dir('./crwl-images/'.$phone.'/')) {
                                rmdir('./crwl-images/'.$phone.'/');
                            }
                            mkdir('./crwl-images/'.$phone.'/');
                            chmod('./crwl-images/'.$phone.'/',0777);
                            $img = './crwl-images/'.$phone.'/'.$phone.'-'.rand(10000,99999).'.jpg';
                            file_put_contents($img,$decoded);

                            if ($escort_id)
                            {
                                $this->model->addEscortCities($city_id,$escort_id);
                                $this->uploadHtml5Action($escort_id,$img);
                            }
                        }
                    }
                    if ($escort_id)
                    {
                        $status_object = new Cubix_EscortStatus(intval($escort_id));
                        $status_object ->setStatus(Cubix_EscortStatus::ESCORT_STATUS_ACTIVE);
                        $status_object ->save();
                    }
                    $cli->out('Escort ID: '.$escort_id.', with phone number '.$phone.' was inserted successfully<br>');
                }else{
                    $this->errors[] = array('phone' => $phone,'url' => $escort_data->additional->ad->url);
                    $txt = serialize($this->errors);
                    file_put_contents('./crwl-json/log4.txt', $txt , FILE_APPEND);
                    $cli->out('Failed to import escort, with phone number '.$phone.'.<br>');
                }
            }


                $this->view->layout()->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
        }while($i++ > 6);
    }

    public function watermark_image($target, $wtrmrk_file, $newcopy) {
        $watermark = imagecreatefrompng($wtrmrk_file);
        imagealphablending($watermark, false);
        imagesavealpha($watermark, true);
        $img = imagecreatefromjpeg($target);
        $img_w = imagesx($img);
        $img_h = imagesy($img);
        $wtrmrk_w = imagesx($watermark);
        $wtrmrk_h = imagesy($watermark);
        $dst_x = ($img_w / 1.9) - ($wtrmrk_w / 1.9); // For centering the watermark on any image
        $dst_y = ($img_h / 1.5) - ($wtrmrk_h / 1.8); // For centering the watermark on any image
        imagecopy($img, $watermark, $dst_x, $dst_y, 0, 0, $wtrmrk_w, $wtrmrk_h);
        imagejpeg($img, $newcopy, 100);
        imagedestroy($img);
        imagedestroy($watermark);
    }

    public function testAction()
    {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $src = './img/logo.png';
        $img = './crwl-images/41795366519/41795366519-83468.jpg';;


        $this->watermark_image($img,$src, $img);

    }

    public function uploadHtml5Action($escort_id, $file, $type = 1)
    {


        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $model_escorts = new Model_EscortsV2();

        $type = intval($this->_getParam('type'));
        $type = $type == 0 ? 1 : $type;

        try {
            if ( ! $this->user->hasAccessToEscort($escort_id) ) {
                throw new Exception('Permission denied');
            }

            $escort = $model_escorts->get($escort_id);

            if ( ! $escort ) {
                throw new Exception('Wrong id supplied');
            }

            $model_photos = new Model_Escort_Photos();
            $photo_count = $model_photos->getEscortPhotoCount($escort_id);
            if ( $photo_count >= Model_Escort_Photos::MAX_PHOTOS_COUNT ){
                throw new Exception("You've reached maximum amount of pictures ( 30 )", Cubix_Images::ERROR_IMAGE_COUNT_LIMIT_OVER);
            }


            // Save on remote storage
            $images = new Cubix_Images();
            $image = $images->save($file, $escort->id, $escort->application_id);

            if ( ! isset($image['hash']) ) {
                throw new Exception("Photo upload failed. Please try again!");
            }

            $image = new Cubix_Images_Entry($image);
            $image->setSize('backend_thumb');
            $image->setCatalogId($escort_id);
            $image_url = $images->getUrl($image);



            $image_size = getimagesize($file);

            if ( ! filesize($file) ) {
                throw new Exception("Photo upload failed. Please try again!");
            }

            $is_portrait = 0;
            if ( $image_size ) {
                if ( $image_size[0] < $image_size[1] ) {
                    $is_portrait = 1;
                }
            }

            $photo = new Model_Escort_PhotoItem(array(
                'escort_id' => $escort_id,
                'hash' => $image->getHash(),
                'ext' => $image->getExt(),
                'type' => $type,
                'is_approved' => 1,
                'is_portrait' => $is_portrait,
                'width' => $image_size[0],
                'height' => $image_size[1]
            ));

            $model = new Model_Escort_Photos();
            $photo = $model->save($photo);

            $m_escorts = new Model_EscortsV2();

            $m_escorts->setEscortVerifiedStatus($escort_id);

            $status = new Cubix_EscortStatus($escort_id);
            $status->setStatusBit(Cubix_EscortStatus::ESCORT_STATUS_ACTIVE);
            $status->save();

            Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_ADDED, array('id' => $photo->id, 'action' => 'photos uploaded'));
            Cubix_AlertEvents::notify($escort_id, ALERT_ME_NEW_PICTURES, array('id' => $photo->id));

            Cubix_LatestActions::addAction($escort_id, Cubix_LatestActions::ACTION_NEW_PHOTO);

            Cubix_LatestActions::addDirectActionDetail($escort_id, Cubix_LatestActions::ACTION_NEW_PHOTO, $photo->id);

            $m_snapshot = new Model_Escort_SnapshotV2(array('id' => $escort_id));
            $m_snapshot->snapshotProfileV2();

            $response['photo'] = $photo->toJSON('backend_thumb_cropper');
            $response['finish'] = TRUE;

        } catch (Exception $e) {
            $response['error']	= 1;
            $response['msg']	= $e->getMessage();
        }



        echo json_encode($response);
    }

    public function removePhotoAction()
    {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $path = $this->_getParam('path');
        exec ('find '.$path.' -type f -exec chmod 0777 {} +');

        if (is_file($path) && unlink($path))
        {
            die(json_encode(array('status' => 'success','msg' => 'Successfully removed','path' => $path)));
        }else{
            die(json_encode(array('status' => 'error','msg' => 'No such file')));
        }

    }

    public function updateStatusAction()
    {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $import_id = $this->_getParam('import_id');
        $status = $this->_getParam('status');

        $model_import = new Model_Importer();
        $model_import->updateJunk($import_id,$status);

    }

    public function customCrawlAction(){
        if (Cubix_Application::getId() == APP_ED)
        {
            return $this->renderScript('importer/custom-tours-crawl.phtml');
        }
    }

    public function customUrlCrawlAction(){
        if(Cubix_Application::getId() == APP_ED){
            return $this->renderScript('importer/custom-url-crawl.phtml');
        }
    }

    public function customCrawlDataAction(){
        if (Cubix_Application::getId() == APP_ED)
        {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLOPT_URL, 'https://www.eurogirlsescort.com/escort-tours/?profile-paginator-page='.$this->_request->page);

            $response = curl_exec($ch);

            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            curl_close($ch);
            $header = substr($response, 0, $header_size);
            $body = substr($response, $header_size);

            echo json_encode($body);
            die;
        }
    }

    public function customCrawlPostAction(){
        if (Cubix_Application::getId() == APP_ED)
        {
            $link = $this->_getParam('link');
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLOPT_URL, 'https://www.eurogirlsescort.com'.$link);
            $response = curl_exec($ch);
            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            curl_close($ch);
            $header = substr($response, 0, $header_size);
            $body = substr($response, $header_size);
            curl_close($ch);
            echo json_encode($body);
            die;
        }
    }

    public function customCrawlCsvAction(){
        if (Cubix_Application::getId() == APP_ED)
        {
            $db = Zend_Registry::get('db');
            $city_m = new Model_Geography_Cities();
            $req = $this->_request;
            $file = fopen('eurogirls-city-tour.csv', 'w');
            fputcsv($file, array('link', 'phone'));
            foreach($req->data as $esc_link => $esc_tours){
                $phone = null;
                // fputcsv($file, array('https://www.eurogirlsescort.com'.$esc_link, $esc_tours[0]['phone']));

                if(isset($esc_tours['phone'])){
                    $phone = $esc_tours['phone'];
                    unset($esc_tours['phone']);
                }
                foreach($esc_tours as $esc_tour){
                    $import_url = 'https://www.eurogirlsescort.com'.$esc_link;
                    $esc = $db->fetchOne("SELECT escort_id FROM importer_data WHERE import_url='".$import_url."'");
                    $date_from = date("Y-m-d 00:00:00", strtotime($esc_tour['start']));
                    $date_to = date("Y-m-d 00:00:00", strtotime($esc_tour['end']));
                    fputcsv($file, array('https://www.eurogirlsescort.com'.$esc_link, $phone, $esc));
                    // if(true){
                    if(isset($esc) && $esc){
                        // $esc = 461344;
                        $sql = "INSERT into tours (escort_id, date_from, date_to, country_id, city_id, is_imported) 
                            SELECT * FROM (SELECT ".$esc." as escort_id, '".$date_from."' as date_from, '".$date_to."' as date_to, ct.country_id as country_id, ct.id as city_id, 1 as imported from cities as ct where title_en LIKE '".$esc_tour['city']."') as temp WHERE NOT EXISTS (SELECT id from tours where escort_id=".$esc." AND date_from='".$date_from."' AND date_to='".$date_to."') LIMIT 1";

                        $db->query($sql);
                    }
                }
            }
            fclose($file);
            echo json_encode(array('status' => 'success'));die;
        }
    }

    public function customCrawlUrlAction(){
        if(Cubix_Application::getId() == APP_ED)
        {
            $link = $this->_getParam('link');
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLOPT_URL, $link);
            $response = curl_exec($ch);
            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            curl_close($ch);
            $header = substr($response, 0, $header_size);
            $body = substr($response, $header_size);
            curl_close($ch);
            echo json_encode($body);
            die;
        }
    }

    public function customCrawlUrlImportAction(){
        if(Cubix_Application::getId() == APP_ED)
        {
            $db = Zend_Registry::get('db');
            $req = $this->_request;
            $escort_photos = $req->data['images'];
            unset($req->data['images']);
            $single_escort = $req->data;

            if(!isset($single_escort['phone']) || $single_escort['phone'] == ""){
                echo json_encode(array('error' => 'Escort cannot be imported no phone'));die;
            }

            $created_esc = $db->query('SELECT id, escort_id FROM importer_data WHERE phone = ?', array($single_escort['phone']))->fetch();
            if(isset($created_esc->escort_id)){
                echo json_encode(array('duplicate' => 'Escort already imported and created, id is '.$created_esc->escort_id));die;
            } else if(isset($created_esc->id)){
                echo json_encode(array('duplicate' => 'Escort already imported, and phone is  '.$single_escort['phone']));die;
            }

            $has_enough_photos = false;

            if ($escort_photos && $escort_photos != "null")
            {
                $photos_array = $escort_photos;
                $photos = array();
                foreach ($photos_array as $key => $photo) {
                    if($key < 10)
                    {
                        $current_photo = str_replace('400x592','1000x700',str_replace('100x100','1000x700',str_replace('"','',$photo)));
                        if(!preg_match('/\.png/',$current_photo))
                            $photos[] = $current_photo;
                    }
                }
                if ( preg_match('#(test\.sceon\.am|\.test)$#', $_SERVER['SERVER_NAME']) > 0 ) {
                    if ( count($photos) > 2 ) {
                        $has_enough_photos = true;
                        if (!is_dir('./crwl-images'. '-'.Cubix_Application::getId().'/')) {
                            mkdir('./crwl-images'. '-'.Cubix_Application::getId().'/', 0755);
                        }
                        $phone = $req->data['phone'];

                        if (is_dir('./crwl-images'. '-'.Cubix_Application::getId().'/' . $phone . '/')) {
                            rmdir('./crwl-images'. '-'.Cubix_Application::getId().'/' . $phone . '/');
                        }
                        mkdir('./crwl-images'. '-'.Cubix_Application::getId().'/' . $phone . '/', 0755);
                        exec ('find ./crwl-images'. '-'.Cubix_Application::getId().'/' . $phone . '/ -type f -exec chmod 0777 {} +');
                        foreach ($photos as $photo_key => $images_match) {
                            $image_link = $images_match;
                            $image = file_get_contents($image_link);
                            $single_escort['images'][$photo_key] = $phone . '-' . rand(10000, 99999) . '.jpg';
                            $single_escort['image_url'] = './crwl-images'. '-'.Cubix_Application::getId().'/' . $phone . '/' . $single_escort['images'][$photo_key] ;
                            $single_escort['image_urls'][$photo_key] = './crwl-images'. '-'.Cubix_Application::getId().'/' . $phone . '/' . $single_escort['images'][$photo_key] ;
                            file_put_contents($single_escort['image_url'], $image);

                            /*if escort from eurogirls vebsite we use slim watermark */
                            if (in_array($single_escort['web_site'], array('https://www.eurogirlsescort.com'))){
                                $src = './img/logo-' . Cubix_Application::getId() . 'eu-slim.png';
                                $ext = preg_match('/jpg/',$single_escort['image_url'])? 'jpg' : 'png';
                                self::eurogirlsWatermarkSlimImage($single_escort['image_url'], $src, $single_escort['image_url']);
                            }
                            else {
                                $src = './img/logo-' . Cubix_Application::getId() . 'eu.png';
                                $ext = preg_match('/jpg/',$single_escort['image_url'])? 'jpg' : 'png';
                                self::eurogirlsWatermarkImage($single_escort['image_url'], $src, $single_escort['image_url']);
                            }
                        }
                    }
                }else{
                    if ( count($photos) > 2 ) {
                        $has_enough_photos = true;
                        if (!is_dir('/data/edir-crwl-images')) {
                            mkdir('/data/edir-crwl-images', 0755);
                        }
                        $phone = $single_escort['phone'];

                        if (is_dir('/data/edir-crwl-images/' . $phone . '/')) {
                            rmdir('/data/edir-crwl-images/' . $phone . '/');
                        }
                        mkdir('/data/edir-crwl-images/' . $phone . '/', 0755);
                        exec ('find /data/edir-crwl-images/' . $phone . '/ -type f -exec chmod 0777 {} +');
                        foreach ($photos as $photo_key => $images_match) {
                            $image_link = $images_match;
                            $image = file_get_contents($image_link);
                            $single_escort['images'][$photo_key] = $phone . '-' . rand(10000, 99999) . '.jpg';
                            $single_escort['image_url'] = '/data/edir-crwl-images/' . $phone . '/' . $single_escort['images'][$photo_key] ;
                            $single_escort['image_urls'][$photo_key] = '/data/edir-crwl-images/' . $phone . '/' . $single_escort['images'][$photo_key] ;
                            file_put_contents($single_escort['image_url'], $image);


                            /*if escort from eurogirls vebsite we use slim watermark */
                            if (in_array($single_escort['web_site'], array('https://www.eurogirlsescort.com'))){
                                $src = './img/logo-' . Cubix_Application::getId() . 'eu-slim.png';
                                self::eurogirlsWatermarkSlimImage($single_escort['image_url'], $src, $single_escort['image_url']);
                            }
                            else {
                                $src = './img/logo-' . Cubix_Application::getId() . 'eu.png';
                                self::eurogirlsWatermarkImage($single_escort['image_url'], $src, $single_escort['image_url']);
                            }
                        }
                    }
                }
            }
            $escort_model = new Model_Importer();

            if( $has_enough_photos )
            {
                $escort_model->save($single_escort);
                echo json_encode(array("success" => "Model imported"));die;
            } else {
                echo json_encode(array("error" => "something went wrong"));die;
            }
            var_dump($single_escort);die;
        }
    }


    public function saveWatermarkedImagesAction()
    {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        if (!$this->_request->isPost()) die('Wrong request type');

        $images = $this->_request->images;

        foreach ($images as $image)
        {
            $watermark = $image['watermark'];
            $main_path = $image['main_path'];
            $watermarkPosition = $image['watermarkPosition'];
            if (strlen($watermarkPosition))
            {

                $positionArray = explode(';',$watermarkPosition);

                $x_left = str_replace('px','',str_replace('left: ','',$positionArray[0]));
                $y_top = str_replace('px','',str_replace(' top: ','',$positionArray[1]));

                self::newWatermarkImage($main_path, $watermark, $main_path, $x_left ,$y_top);
            }
        }

        die;
    }

    public static function newWatermarkImage($target, $wtrmrk_file, $newcopy, $x = null, $y = null) {
        $watermark = imagecreatefrompng($wtrmrk_file);
        imagealphablending($watermark, false);
        imagesavealpha($watermark, true);
        $img = imagecreatefromjpeg($target);
        $img_w = imagesx($img);
        $img_h = imagesy($img);
        $wtrmrk_w = imagesx($watermark);
        $wtrmrk_h = imagesy($watermark);
        $dst_x = $x;
        $dst_y = $y;
        imagecopy($img, $watermark, $dst_x + (($img_w - 450) / 2), $dst_y + (($img_h - 450) / 2), 0, 0, $wtrmrk_w, $wtrmrk_h);
        imagejpeg($img, $newcopy, 100);
        imagedestroy($img);
        imagedestroy($watermark);
    }

    public static function eurogirlsWatermarkImage($target, $wtrmrk_file, $newcopy) {
        $watermark = imagecreatefrompng($wtrmrk_file);
        imagealphablending($watermark, false);
        imagesavealpha($watermark, true);
        $img = imagecreatefromjpeg($target);
        $img_w = imagesx($img);
        $img_h = imagesy($img);
        $wtrmrk_w = imagesx($watermark);
        $wtrmrk_h = imagesy($watermark);
        if($img_w > $img_h)
        {
            $dst_x = ($img_w / 1.8) - ($wtrmrk_w / 1.5);
            $dst_y = ($img_h / 1.8) - ($wtrmrk_h / 1.5);
        }elseif($img_w < $img_h){
            $dst_x = ($img_w / 1.9) - ($wtrmrk_w / 1.9);
            $dst_y = ($img_h / 1.7) - ($wtrmrk_h / 1.5);
        }else{
            $dst_x = ($img_w / 2) - ($wtrmrk_w / 2);
            $dst_y = ($img_h / 1.9) - ($wtrmrk_h / 1.8);
        }
        imagecopy($img, $watermark, $dst_x, $dst_y, 0, 0, $wtrmrk_w, $wtrmrk_h);
        imagejpeg($img, $newcopy, 100);
        imagedestroy($img);
        imagedestroy($watermark);
    }

    public static function eurogirlsWatermarkSlimImage($target, $wtrmrk_file, $newcopy) {
        $watermark = imagecreatefrompng($wtrmrk_file);
        imagealphablending($watermark, false);
        imagesavealpha($watermark, true);
        $img = imagecreatefromjpeg($target);
        $img_w = imagesx($img);
        $img_h = imagesy($img);
        $wtrmrk_w = imagesx($watermark);
        $wtrmrk_h = imagesy($watermark);
        /*if($img_w > $img_h)
        {
            $dst_x = ($img_w / 1.8) - ($wtrmrk_w / 1.44);
            $dst_y = ($img_h / 1.8) - ($wtrmrk_h / 1.44);
        }elseif($img_w < $img_h){
            $dst_x = ($img_w / 1.9) - ($wtrmrk_w / 1.9);
            $dst_y = ($img_h / 1.9) - ($wtrmrk_h);
        }else{
            $dst_x = ($img_w / 2) - ($wtrmrk_w / 1.8);
            $dst_y = ($img_h / 1.9) - ($wtrmrk_h / 1.8);
        }*/
        $dst_x = ($img_w / 2) - $wtrmrk_w/2;
        $dst_y = ($img_h / 2) - $wtrmrk_h/2;

        imagecopy($img, $watermark, $dst_x, $dst_y, 0, 0, $wtrmrk_w, $wtrmrk_h);
        imagejpeg($img, $newcopy, 100);
        imagedestroy($img);
        imagedestroy($watermark);
    }

    public function sexNewsCustomUrlCrawlAction()
    {
        if (Cubix_Application::getId() == APP_ED) {
            return $this->renderScript('importer/sex-news-url-crawl.phtml');
        }
    }

    public function sexBombuaCustomUrlCrawlAction()
    {
        if (Cubix_Application::getId() == APP_ED) {
            return $this->renderScript('importer/sex-bombua-url-crawl.phtml');
        }
    }

    public function sexBombuaCrawlUrlImportAction()
    {
        if (Cubix_Application::getId() == APP_ED) {
            $db = Zend_Registry::get('db');
            $req = $this->_request;

            $res = [];
            $res['success'] = false;

            $url = $req->data['url'];
            $website = $req->data['web_site'];


            if (!is_null($url) && !is_null($website)) {

                if (!strpos($url, 'redirect')) {
                    $single_escort = array();

                    $personal_content = file_get_contents($url);

                    /* crawling one escort for testing */
//                            $personal_content = file_get_contents('https://sexbombua.com/prostitute-in-ukraine/allochka_396.html');

                    $dom = new DOMDocument;
                    $dom->loadHTML($personal_content);

                    $finderEscort = new DomXPath($dom);

                    if (!is_null($finderEscort->query("//*[contains(@class, 'contact_with_us')]")->item(0))) {
                        $escortphone = $finderEscort->query("//*[contains(@class, 'contact_with_us')]")->item(0)->getElementsByTagName('span')->item(0)->nodeValue;
                    } else {
                        $escortphone = 'empty';
                    }

                    if ($escortphone != 'empty') {

                        $single_escort['phone'] = str_replace('-', '', $escortphone);

                        $model = new Model_Importer();

                        if (!$model->existsByPhone($single_escort['phone']) && !$model->existsImportByPhone($single_escort['phone'])) {

                            $escortBio = $finderEscort->query("//*[contains(@class, 'twocolumn')]");

                            $name = explode('-', $finderEscort->query("//*[contains(@class, 'content')]")->item(0)->getElementsByTagName('h1')->item(0)->nodeValue);
                            $single_escort['name'] = $this->clarifyStringByRegex($name[0]);

                            foreach ($escortBio as $section) {
                                $bioSection = $section->getElementsByTagName('li');

                                foreach ($bioSection as $row) {
                                    if (strpos($this->clarifyStringByRegex($row->nodeValue), 'Age') === 0) {
                                        $single_escort['age'] = $this->clarifyStringByRegex($row->getElementsByTagName('span')->item(0)->textContent);
                                    }

                                    if (strpos($this->clarifyStringByRegex($row->nodeValue), 'Height (cm)') === 0) {
                                        $single_escort['height'] = $this->clarifyStringByRegex($row->getElementsByTagName('span')->item(0)->textContent);
                                    }

                                    if (strpos($this->clarifyStringByRegex($row->nodeValue), 'Weight (kg)') === 0) {
                                        $single_escort['weight'] = $this->clarifyStringByRegex($row->getElementsByTagName('span')->item(0)->textContent);
                                    }

                                    if (strpos($this->clarifyStringByRegex($row->nodeValue), 'Bust') === 0) {
                                        $single_escort['bust'] = $this->clarifyStringByRegex($row->getElementsByTagName('span')->item(0)->textContent);
                                    }
                                }
                            }


                            $single_escort['gender'] = 1;

                            $single_escort['web_site'] = 'https://sexbombua.com/';

                            $single_escort['url'] = $url;

                            $single_escort['ethnicity'] = ETHNIC_WHITE;

                            $single_escort['nationality'] = 'ukrainian';

                            $single_escort['country'] = 'ua';

                            $single_escort['city'] = 'kiev';

                            $finder = new DomXPath($dom);

                            $services_arr = array();
                            $escortServices = $finderEscort->query("//*[contains(@class, 'twocolumn')]")->item(1);
                            foreach ($escortServices->childNodes as $service) {

                                if (preg_replace('/\s+/', '', $service->nodeValue) !== "") {
                                    $services_arr[] = $this->clarifyStringByRegex($service->nodeValue);
                                }
                            }

                            $single_escort['services'] = $services_arr;

                            $phoroWrapperUl = $finderEscort->query("//*[contains(@class, 'thumb_detail_image')]");
                            if ($phoroWrapperUl->length) {


                                if (!is_dir('./data/crwl-images-sex-bombua' . (Cubix_Application::getId() == APP_ED ? '' : '-' . Cubix_Application::getId()) . '/')) {
                                    mkdir('./data/crwl-images-sex-bombua' . (Cubix_Application::getId() == APP_ED ? '' : '-' . Cubix_Application::getId()) . '/');
                                }
                                if (is_dir('./data/crwl-images-sex-bombua' . (Cubix_Application::getId() == APP_ED ? '' : '-' . Cubix_Application::getId()) . '/' . $escortphone . '/')) {
                                    rmdir('./data/crwl-images-sex-bombua' . (Cubix_Application::getId() == APP_ED ? '' : '-' . Cubix_Application::getId()) . '/' . $escortphone . '/');
                                }
                                mkdir('./data/crwl-images-sex-bombua' . (Cubix_Application::getId() == APP_ED ? '' : '-' . Cubix_Application::getId()) . '/' . $escortphone . '/');
                                exec('find ./data/crwl-images-sex-bombua' . (Cubix_Application::getId() == APP_ED ? '' : '-' . Cubix_Application::getId()) . '/' . $escortphone . '/ -type f -exec chmod -R 0777 {} +');

                                $sources = [];
                                foreach ($phoroWrapperUl as $key => $photoWrapper) {
                                    $imgSrc = $photoWrapper->getAttribute('href');
                                    if (strpos($imgSrc, 'jpg')) {
                                        $srcset = 'https://sexbombua.com/' . $imgSrc;
                                        array_push($sources, $srcset);
                                    }
                                }

                                if (!empty($sources)) {
                                    foreach (array_unique($sources) as $key => $source) {
                                        $image = file_get_contents($source);

                                        $single_escort['images'][$key] = $escortphone . '-' . rand(10000, 99999);
                                        $single_escort['image_url'] = './data/crwl-images-sex-bombua' . (Cubix_Application::getId() == APP_ED ? '' : '-' . Cubix_Application::getId()) . '/' . $escortphone . '/' . $single_escort['images'][$key] . '.jpg';
                                        $single_escort['image_urls'][$key] = './data/crwl-images-sex-bombua' . (Cubix_Application::getId() == APP_ED ? '' : '-' . Cubix_Application::getId()) . '/' . $escortphone . '/' . $single_escort['images'][$key] . '.jpg';

                                        file_put_contents($single_escort['image_url'], $image);
                                    }
                                }
                            }

                            $escorts[] = $single_escort;

                            $saved = $model->save($single_escort);

                            if ($saved === 'Inserted successfull') {
                                $res['message'] = "Escort profile successfully imported.";
                            }

                            $imported_escorts[] = $url . '|';

                        } else {
                            $res['message'] = "Escort with phone number $escortphone already exist.";
                        }
                    }
                } else {
                    $res['message'] = 'Invalid data';
                }

                echo json_encode($res);
                die;

                echo json_encode($res);
                die;
            }
        }
    }

    public function sexNewsCrawlUrlImportAction()
    {
        if (Cubix_Application::getId() == APP_ED) {
            $db = Zend_Registry::get('db');
            $req = $this->_request;

            $res = [];
            $res['success'] = false;

            $url = $req->data['url'];
            $website = $req->data['web_site'];

            if (!is_null($url) && !is_null($website)) {

                $single_escort = array();
                $escort_profile_path = $url;

                $personal_content = file_get_contents($escort_profile_path);


                /* crawling one escort for testing */
//                            $personal_content = file_get_contents('https://www.sexnews.ch/de/glattbrugg-elegante-frau-fuer-raffinierte-maenner-original-foto-privatescort-0779770175/cat/escort/');

                $dom = new DOMDocument;
                $dom->loadHTML($personal_content);

                $opts = [
                    "http" => [
                        "method" => "GET",
                        "header" => "Accept-language: en\r\n" .
                            "x-requested-with: XMLHttpRequest\r\n"
                    ]
                ];

                $finderEscort = new DomXPath($dom);


                $escortphone = substr($finderEscort->query("//*[contains(@class, 'btn-telephone')]")->item(0)->getAttribute('href'), 5);

                if ($escortphone) {

                    $single_escort['phone'] = $escortphone;

                    $model = new Model_Importer();

                    if (!$model->existsByPhone($single_escort['phone']) && !$model->existsImportByPhone($single_escort['phone'])) {

                        $additionalInfo = $finderEscort->query("//*[contains(@class, 'bodytext')]")->item(0)->nodeValue;

                        $escortName = $finderEscort->query("//*[contains(@class, 'h1')]")->item(0)->nodeValue;
                        $single_escort['name'] = $this->clarifyStringByRegex($escortName);

                        $single_escort['gender'] = 1;

                        $single_escort['web_site'] = 'https://www.sexnews.ch/';

                        $single_escort['url'] = $escort_profile_path;

                        $single_escort['age'] = 26;
                        if (strpos($additionalInfo, 'Alter')) {
                            $single_escort['age'] = $this->clarifyStringByRegex((int)substr($additionalInfo, strpos($additionalInfo, 'Alter') + 7, 2));
                        }

                        $finder = new DomXPath($dom);
                        $services_provided = $finder->query("//*[contains(@class, 'service-true')]");

                        $services_arr = array();
                        foreach ($services_provided as $service) {
                            $services_arr[] = $service->textContent;
                        }
                        $single_escort['services'] = $services_arr;

                        $finder = new DomXPath($dom);
                        $servicesAndLanguages = $finder->query("//*[contains(@class, 'tablet-info-service')]");

                        foreach ($servicesAndLanguages as $serviceAndLanguage) {

                            $arr = explode('Sprache(n)', $serviceAndLanguage->textContent);

                            $services = $arr[0];
                            $languages = $arr[1];

                            $single_escort['services'] = explode(',', $this->clarifyStringByRegex(str_replace('Services', '', $services)));

                            $lang_array = explode(',', $languages);
                            if (!empty($lang_array)) {
                                $languages_array = array();
                                foreach ($lang_array as $key => $lang) {

                                    $langs['stars'] = 4;
                                    $langs['title'] = str_replace(' ', '', $this->clarifyStringByRegex($lang));
                                    $languages_array[] = $langs;
                                }
                                $single_escort['langs'] = $languages_array;
                            }
                        }

                        $single_escort['about_me'] = $this->clarifyStringByRegex($finder->query("//*[contains(@class, 'bodytext')]")->item(0)->textContent);

                        $single_escort['country'] = 'ch';

                        $single_escort['location'] = $this->clarifyStringByRegex($finder->query("//*[contains(@class, 'location')]")->item(0)->textContent);

                        $locations = [];
                        if ($single_escort['location']) {
                            $locations = explode(' ', $single_escort['location']);
                        }

                        if (!empty($locations[0])) {
                            $single_escort['postcode'] = $locations[0];
                        }


                        if (!empty($locations[1])) {
                            $single_escort['city'] = $locations[1];
                        }

                        $phoroWrapperUl = $finder->query("//*[contains(@class, 'carousel-thumbnail')]");

                        if (!empty($phoroWrapperUl->item(0)->getElementsByTagName('li'))) {

                            if (is_dir('./crwl-images-69/sexnews/' . $escortphone . '/')) {
                                rmdir('./crwl-images-69/sexnews/' . $escortphone . '/');
                            }
                            mkdir('./crwl-images-69/sexnews/' . $escortphone . '/');

                            foreach ($phoroWrapperUl as $key => $photoWrapper) {
                                $srcset = 'https://www.sexnews.ch/' . $photoWrapper->getElementsByTagName('img')->item(0)->getAttribute('src');

                                if (!empty($srcset)) {
                                    $image = file_get_contents($srcset);

                                    $single_escort['images'][$key] = $escortphone . '-' . rand(10000, 99999);
                                    $single_escort['image_url'] = './crwl-images-69/sexnews/' . $escortphone . '/' . $single_escort['images'][$key] . '.jpg';
                                    $single_escort['image_urls'][$key] = './crwl-images-69/sexnews/' . $escortphone . '/' . $single_escort['images'][$key] . '.jpg';

                                    file_put_contents($single_escort['image_url'], $image);
                                }
                            }
                        }

                        $escorts[] = $single_escort;

                        $saved = $model->save($single_escort);

                        if($saved === 'Inserted successfull') {
                            $res['message'] = "Escort profile successfully imported.";
                        }

                        $imported_escorts[] = $escort_profile_path . '|';
                    } else {
                        $res['message'] = "Escort with phone number $escortphone already exist.";
                    }
                }
            } else {
                $res['message'] = 'Invalid data';
            }

            echo json_encode($res);
            die;
        }
    }

    public function clarifyStringByRegex($string) {

        // Match Emoticons
        $regex_emoticons = '/[\x{1F600}-\x{1F64F}]/u';
        $clear_string = preg_replace($regex_emoticons, '', $string);

        // Match Miscellaneous Symbols and Pictographs
        $regex_symbols = '/[\x{1F300}-\x{1F5FF}]/u';
        $clear_string = preg_replace($regex_symbols, '', $clear_string);

        // Match Transport And Map Symbols
        $regex_transport = '/[\x{1F680}-\x{1F6FF}]/u';
        $clear_string = preg_replace($regex_transport, '', $clear_string);

        // Match Miscellaneous Symbols
        $regex_misc = '/[\x{2600}-\x{26FF}]/u';
        $clear_string = preg_replace($regex_misc, '', $clear_string);

        // Match Dingbats
        $regex_dingbats = "~[^a-zA-Z0-9_ !@#$%^&*();\\\/|<>\"'+.,:?=-]~";
        $clear_string = preg_replace($regex_dingbats, '', $clear_string);

        return $clear_string;
    }
}
