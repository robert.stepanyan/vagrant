<?php

class ReviewsReplyController extends Zend_Controller_Action
{
	public function init()
	{
		$this->model = new Model_Reviews();
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		if (!in_array($bu_user->type, array('superadmin', 'admin')) || (Cubix_Application::getId() != APP_6A && Cubix_Application::getId() != APP_EF))
			die('permission denied');
	}
	
	public function indexAction() 
	{
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('default_reviews_reply_data');
		$this->view->filter_params = $ses_filter->filter_params;
		/* FILTER PARAMS */
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'showname' => str_replace('_', '\_', $req->showname),
			'escort_id' => $req->escort_id,
			'status' => $req->status,
		);
				
		$data = $this->model->getAllReplies(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		$DEFINITIONS = Zend_Registry::get('defines');
		
		foreach ( $data as $i => $item ) {
			$data[$i]->escort_comment_status = $DEFINITIONS['review_reply_status'][$item->escort_comment_status];
			$data[$i]->clear_username = $item->username;
			
			if ($item->m_is_suspicious)
			{
				if (Cubix_Application::getId() == 1)
					$cl = 'red';
				else
					$cl = 'pink';
				$data[$i]->username = '<span class="' . $cl . ' bold">' . $item->username . '</span>';
				$data[$i]->c = 'c4';
			}
			elseif ($item->m_is_trusted)
			{
				if (Cubix_Application::getId() == 1)
					$cl = 'brown';
				else
					$cl = 'green';
				$data[$i]->username = '<span class="' . $cl . ' bold">' . $item->username . '</span>';
				$data[$i]->c = 'c5';
			}
			elseif ($item->new_member)
			{
				if (Cubix_Application::getId() == 1)
					$cl = 'green';
				else
					$cl = 'red';
				$data[$i]->username = '<span class="' . $cl . ' bold">' . $item->username . '</span>';
				$data[$i]->c = 'c1';
			}
			elseif ($this->model->fakeByPhone($item->user_id))
			{
				if (Cubix_Application::getId() == 1)
					$cl = 'blue';
				else
					$cl = 'red';
				$data[$i]->username = '<span class="' . $cl . ' bold">' . $item->username . '</span>';
				$data[$i]->c = 'c2';
			}
			elseif (($cities = $this->model->fakeByCity($item->user_id)) != false)
			{
				if (Cubix_Application::getId() == 1)
					$cl = 'pink';
				else
					$cl = 'red';
				$data[$i]->username = '<span class="' . $cl . ' bold">' . $item->username . '</span>';
				$data[$i]->c = 'c3';
				$data[$i]->cities = $cities;
			}
			
			$last = $this->model->getEscortLastReviewDate($item->escort_id);
			$data[$i]->escort_last_review_date = $last ? date('d F, Y', $last->date) : '';
			$data[$i]->escort_last_review_status = $last ? $DEFINITIONS['review_status_options'][$last->status] : '';
			$last_member = $this->model->getMemberLastReviewDate($item->user_id);
			$data[$i]->member_last_review_date = $last_member ? date('d F, Y', $last_member->date) : '';
			$data[$i]->member_last_review_status = $last_member ? $DEFINITIONS['review_status_options'][$last_member->status] : '';
			$data[$i]->date_registered = date('d F, Y', $item->date_registered);
		}
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
	
	public function editAction()
	{	
		if (!$this->_request->isPost())
		{
			$this->view->escort_id = $this->_request->escort_id;
			$this->view->review_id = $this->_request->id;
			$this->view->comment = $this->model->getEscortComment($this->_request->id);
		}
		else 
		{
			$data = new Cubix_Form_Data($this->_request);
			
			$data->setFields(array(
				'escort_id' => 'int',
				'review_id' => 'int',
				'comment' => ''
			));
			
			$data = $data->getData();
						
			$validator = new Cubix_Validator();
						
			switch ($this->_request->act)
			{
				case 'approve':
					$data['escort_comment_status'] = REVIEW_REPLY_APPROVED;
					//Cubix_SyncNotifier::notify($data['escort_id'], Cubix_SyncNotifier::EVENT_ESCORT_REVIEW_REPLY_APPROVED, array('escort_id' => $data['escort_id'], 'review_id' =>$data['id'] ));
					break;
				case 'need_mod':
					$data['escort_comment_status'] = REVIEW_REPLY_NEED_MODIFICATION;
					//Cubix_SyncNotifier::notify($data['escort_id'], Cubix_SyncNotifier::EVENT_ESCORT_REVIEW_REPLY_NEED_MODIFICATION, array('escort_id' => $data['escort_id'], 'review_id' =>$data['id'] ));
					break;
				case 'disable':
					$data['escort_comment_status'] = REVIEW_REPLY_DISABLED;
					//Cubix_SyncNotifier::notify($data['escort_id'], Cubix_SyncNotifier::EVENT_ESCORT_REVIEW_REPLY_DISABLED, array('escort_id' => $data['escort_id'], 'review_id' =>$data['id'] ));
					break;
			}

			if ($validator->isValid())
			{
				$this->model->saveEscortComment($data);
			}

			die(json_encode($validator->getStatus()));
		}
	}
	public function setStatusAction()
	{
		$status = $this->_request->status;
		$ids = $this->_request->id;
		foreach($ids as $id){
			$this->model->setEscortCommentStatus($id, $status);
		}
		die;
	}		
}
