<?php

class FeaturedGirlsController extends Zend_Controller_Action
{
    /**
     * @var Model_FeaturedGirls
     */
    protected $fgModel;

    /**
     * @var Model_EscortsV2
     */
    protected $eModel;

    /**
     * @return void
     */
    public function init()
    {
        $this->fgModel = new Model_FeaturedGirls();
        $this->eModel = new Model_EscortsV2();
    }

    /**
     * @return void
     */
    public function indexAction()
    {

    }

    /**
     * @return void
     */
    public function dataAction()
    {
        $req = $this->_request;
        $count = $this->_getParam('count', null);

        $filter = array(
            'escort_id' => $req->escort_id,
            'country' => $req->country,
            'city' => $req->city
        );

        $data = $this->fgModel->getAll(
            $this->_request->page,
            $this->_request->per_page,
            $filter,
            $this->_request->sort_field,
            $this->_request->sort_dir,
            $count
        );

        $DEFINITIONS = Zend_Registry::get('defines');

        foreach ($data as $d) {
            $stat = array();
            foreach ($DEFINITIONS['escort_status_options'] as $key => $e_status) {
                if ($this->eModel->hasStatusBit($d->id, $key)) {
                    if ($this->eModel->hasStatusBit($d->id, ESCORT_STATUS_INACTIVE)
                        && $key == ESCORT_STATUS_ACTIVE
                        && in_array(Cubix_Application::getId(), array(APP_EG_CO_UK, APP_ED))
                    ) {
                        continue;
                    }
                    $stat[] = $e_status;
                }
            }

            $d->status = implode(', ', $stat);//$DEFINITIONS['escort_status_options'][$d['status']];
        }

        echo json_encode(array('data' => $data, 'count' => $count));
        die;
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $escortIds = $this->_getParam('id', -1);

        $escorts = [];
        foreach($escortIds as $escortId) {
            $escorts[$escortId] = $this->eModel->get($escortId);

            $escorts[$escortId]->workingLocations = $workingLocations = $this->eModel->getWorkingLocations($escortId, array('country' => true));
            $escorts[$escortId]->featuredLocations = $featuredLocations = $this->fgModel->getLocationsByEscortId($escortId);
        }

        if (empty($escorts)) die("Escort was not found");
        $this->view->escorts = $escorts;

        if ($this->_request->isPost()) {

            $featuredCities = $this->_getParam('featured_cities', []);
            $featured_country = $this->_getParam('featured_country', []);

            foreach ($escortIds as $id) {
                $this->fgModel->removeFeaturedDataByEscortId($id);
            }

            foreach($featuredCities as $escort => $cities) {
                $this->fgModel->setFeaturedCities($escort, $cities);
            }

            foreach($featured_country as $escort => $countries) {
                $this->fgModel->setFeaturedCountry($escort, $countries);
            }

            die(json_encode(['status' => 'success']));
        }

    }

}
