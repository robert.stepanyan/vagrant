<?php

class CronEmailController extends Zend_Controller_Action
{
	/**
	 *
	 * @var Zend_Db_Adapter_Mysqli
	 */
	private $_db;

	private $cronjobs = array();

	private $count = 0;

	public function init()
	{
		$this->_db = Zend_Registry::get('db');
		
		$this->_helper->viewRenderer->setNoRender(true);
		
		$this->view->layout()->disableLayout();

		$this->cli = new Cubix_Cli();

		$this->start();

	}

	private function start(){
		$sql = "SELECT * FROM cron_emails cm 
					WHERE cm.last_sent_date  <= NOW() - INTERVAL cm.interval HOUR";

		$this->cronjobs = $this->_db->query($sql)->fetchAll();
	}


	public function emailManagerAction(){


	    if( ! $this->cronjobs) return $this->cli->out( "No scheduled emails to be send at the moment");

		foreach ($this->cronjobs as $email) {
			switch( $email->template_id ) {
				case 'member_city_alert' :
					 $this->memberCityAlertEmail();
					 break;

				case 'escort_profile_not_complete_reminder_24' :
					 $this->escortAccountNotComplete(1);
					break;

				case 'escort_profile_not_complete_reminder_20d' :
					 $this->escortAccountNotComplete(20);
					break;

				case 'escort_package_expiration_3_days':
                   $this->packageExpirationThreeDaysNoticeAction();
					$this->packageExpirationSMSThreeDaysNoticeAction();
					break;

                case 'escort_package_expiration_12_midday' :
                    $this->packageExpirationNotice();
                    $this->packageExpirationSMSNoticeAction();
                    break;

                case 'agency_profile_not_completed_24' :
                    $this->agencyAccountNotComplete(1);
                    break;

                case 'agency_profile_not_completed_30d' :
                    $this->agencyAccountNotComplete(30);
                    break;

                case 'agency_no_active_escorts_24' :
                    $this->agencyNoActiveEscorts(true);
                    break;

                case 'agency_no_active_escorts_14d' :
                    $this->agencyNoActiveEscorts();
                    break;

                case 'agency_not_enough_escorts' :
                    $this->agencyNotEnoughEscorts();
                    break;

                case 'agency_package_expiration_3d' :
                    $this->agencyPackageExpirationNotice(3,'agency_package_expiration');
                    break;

                case 'agency_package_expiration' :
                    $this->agencyPackageExpirationNotice(0, 'agency_package_expiration_b');
                    break;

                case "escort_agency_checkout_incomplete":
                    $this->escortAgencyCheckoutIncomplete();
                    break;

                case 'escort_being_inactive_reminder':
                    $this->updateLastSentDate($email->template_id);
                    $this->sendEmailToUsersByLastLoginAction('escort_being_inactive_reminder', USER_TYPE_SINGLE_GIRL); //EDIR-2494
                    break;

                case 'agency_being_inactive_reminder':
                    $this->updateLastSentDate($email->template_id);
                    $this->sendEmailToUsersByLastLoginAction('agency_being_inactive_reminder', USER_TYPE_AGENCY); //EDIR-2494
                    break;

                case 'halloween_event_2019':
                    $this->halloween_event_2019(); //EDIR-2540
                    break;
			}
			$this->updateLastSentDate($email->template_id);
			//$this->sendMailforDevNotification($email->template_id);
		}
    }

    private function halloween_event_2019()
    {
        set_time_limit(0);
        ini_set('memory_limit', '1024M');

        $tpl = 'halloween_event_2019';

        // Independent Escorts
        // ------------------------------------
        $sql = "SELECT
                e.id,
                el.lang_id,
                u.email,
                c.title_en 
            FROM
                escorts e
                INNER JOIN users u ON u.id = e.user_id
                INNER JOIN countries c ON e.country_id = c.id
                LEFT JOIN escort_langs el ON el.escort_id = e.id 
            WHERE
                e.type IN ( 1, 2, 3 ) 
                AND el.lang_id IN ( 'en', 'de', 'fr', 'it', 'ro', 'es' ) 
                AND e.country_id != 68 
                AND u.email != 'no@no.com'
                AND NOT e.status & 256
                AND NOT e.status & 16
            GROUP BY
                u.email
            ORDER BY
                e.id ASC";

        $escorts = $this->_db->query($sql)->fetchAll();
        $this->_db->closeConnection();

        foreach($escorts as $escort) {
            $escort = (array )$escort;
            $lng = isset($escort['lang_id']) && !empty($escort['lang_id']) ? $escort['lang_id'] : 'en';
            $this->cli->colorize('purple')->out('Sending email to. ' . $escort['id'] . '|' . $escort['email'] );
            $res = Cubix_Email::sendTemplate($tpl, $escort['email'], [], null, null, $lng);
            $this->cli->colorize('green')->out('Sending Complete | STATUS ' . ($res ? 'sent' : 'ERROR'));
        }

        $this->cli->colorize('yellow')->out('Sending Escorts - Done !')->reset();
        // ------------------------------------


        // Agencies
        // ------------------------------------
        $sql = "
            SELECT
                a.id,
                al.lang_id,
                u.email,
                c.title_en 
            FROM
                agencies a
                INNER JOIN users u ON u.id = a.user_id
                INNER JOIN countries c ON a.country_id = c.id
                LEFT JOIN agency_langs al ON al.agency_id = a.id 
            WHERE
                al.lang_id IN ( 'en', 'de', 'fr', 'it', 'ro', 'es' ) 
                AND a.country_id != 68 
                AND NOT a.STATUS & - 4 
            GROUP BY
                u.email
            ORDER BY
                a.id ASC";
        $agencies = $this->_db->query($sql)->fetchAll();
        $this->_db->closeConnection();

        foreach($agencies as $agency) {
            $agency = (array )$agency;
            $lng = isset($agency['lang_id']) && !empty($agency['lang_id']) ? $agency['lang_id'] : 'en';
            $this->cli->colorize('purple')->out('Sending email to. ' . $agency['id'] . '|' . $agency['email'] );
            $res = Cubix_Email::sendTemplate($tpl, $agency['email'], [], null, null, $lng);
            $this->cli->colorize('green')->out('Sending Complete | STATUS ' . ($res ? 'sent' : 'ERROR'));
        }
        // ------------------------------------

        return;
    }

    private function sendEmailToUsersByLastLoginAction($template = 'for_escorts_who_didnt_login', $userType = USER_TYPE_SINGLE_GIRL)     //EDIR-2494
    {
        set_time_limit(0);
        ini_set('memory_limit', '1024M');
        $this->cli->colorize('purple')->out('Fetching users to send emails.');
        $model = new Model_EscortsV2();

        if($userType == USER_TYPE_SINGLE_GIRL) {
            $users = $model->getActiveEscortsByLastLoginInterval();
        }else if ($userType == USER_TYPE_AGENCY) {
            $users = $model->getActiveAgenciesByLastLoginInterval();
        }

        $this->_db->closeConnection();

        $count = 0;
        $overall = count($users);
        if($overall){
            foreach ($users as $user) {
                $count++;

                if($userType == USER_TYPE_SINGLE_GIRL) {
                    $tpl_data = array(
                        'username' => $user->showname,
                        'lang' => $user->lang_id
                    );
                }else if ($userType == USER_TYPE_AGENCY) {
                    $tpl_data = array(
                        'username' => $user->name,
                        'lang' => 'en'
                    );
                }

                $tpl_data['backend_user'] = $user->backend_user;
                $tpl_data['user_id'] = $user->id;
                $tpl_data['is_cron'] = true;

                $res = Cubix_Email::sendTemplate($template, $user->email, $tpl_data, null, null, $tpl_data['lang']);
                $this->cli->colorize('green')->out("[$count / $overall] sent. Current Escort was " . $tpl_data['username'] . ' | STATUS ' . ($res ? 'sent' : 'ERROR') );
            }
        }
        $this->cli->colorize('yellow')->out('Done !')->reset();

        /*
        //testing
        $tpl_data = array(
            'username' => $overall
        );

        if(Cubix_Application::getId() == APP_ED) {

            foreach (['en',  'de','es',  'fr', 'ro', 'it'] as $lng) {
                Cubix_Email::sendTemplate($template, 'edhovhannisyan97@gmail.com', $tpl_data, null, null, $lng);
                Cubix_Email::sendTemplate($template, 'edhovhannisyan97@protonmail.com', $tpl_data, null, null, $lng);
                Cubix_Email::sendTemplate($template, 'edhovhannisyan97@yahoo.com', $tpl_data, null, null, $lng);

                Cubix_Email::sendTemplate($template, 'vvbilly@protonmail.com ', $tpl_data, null, null, $lng);
                Cubix_Email::sendTemplate($template, 'anca.lazar89@yahoo.com', $tpl_data, null, null, $lng);
                Cubix_Email::sendTemplate($template, 'liviu.bistriceanu@gmail.com', $tpl_data, null, null, $lng);
            }
            Cubix_Email::sendTemplate($template, 'knownasmax@gmail.com', $tpl_data);
            Cubix_Email::sendTemplate($template, 'mihranhayrapetyan@gmail.com', $tpl_data);
            Cubix_Email::sendTemplate($template, 'mihran1@bk.ru', $tpl_data);
            Cubix_Email::sendTemplate($template, 'sosete13@yahoo.com', $tpl_data);
            Cubix_Email::sendTemplate($template, 'sosete015@gmail.com', $tpl_data);

            Cubix_Email::sendTemplate($template, 'vvbilly@protonmail.com ', $tpl_data);
            Cubix_Email::sendTemplate($template, 'anca.lazar89@yahoo.com', $tpl_data);
        }*/

        return;
    }

    public function escortAgencyCheckoutIncomplete(){

        $sub_sql = 'SELECT DISTINCT user_id
                        FROM shopping_cart
                        WHERE `status` = 0 
                            AND TIMESTAMPDIFF( HOUR, creation_date, NOW( ) ) = 6 
                        ORDER BY TIMESTAMPDIFF( HOUR, creation_date, NOW( ) )';

        $sql = "SELECT sc.id, user_id, package_id, email, username, creation_date,u.backend_user as bu
                    FROM shopping_cart AS sc 
                    INNER JOIN users AS u ON u.id = sc.user_id 
                    WHERE sc.`status` = 0 
                        AND sc.user_id IN ( $sub_sql )	
                    GROUP BY sc.user_id";

        $app = Cubix_Application::getById();


        $result = $this->_db->query($sql)->fetchAll();

        if( !$result ) {
            $this->cli->out("No incomplete checkout found");
            return;
        }

        foreach($result as $item){
            $result = Cubix_Email::sendTemplate( 'escort_agency_checkout_incomplete', $item->email, array(
                'username' => $item->username,
                'user_id' => $item->user_id,
                'backend_user' => $item->bu,
                'link' => 'https://www.escortguide.co.uk/account/signin?referrer=private#billing/index/'
            ));

            $this->cli->out($item->user_id. ' - email sent' . " \n ");
        }


    }

	public function memberCityAlertEmail(){

        $app = Cubix_Application::getById();

        $sql = "SELECT DISTINCT ca.user_id, u.email, u.username,u.backend_user
					FROM city_alerts ca
					INNER JOIN users u ON u.id = ca.user_id
					GROUP BY ca.user_id order by ca.user_id";


            $result = $this->_db->query($sql)->fetchAll();


            if( !$result ) {
            	$this->cli->out( "City alerts doesn't exists");
            	return;
            }

            foreach ($result as $user) {

                $new_escorts_sql = "SELECT  ca.user_id, ca.city_id,  c.title_en, c.slug, GROUP_CONCAT( e.showname separator  '||' ) as escorts_showname, GROUP_CONCAT( e.id separator  '||' ) as escorts_ids
					FROM city_alerts ca
					INNER JOIN cities c ON c.id = ca.city_id
					INNER JOIN escorts e ON e.city_id = ca.city_id AND e.`status` & 32 AND DATE(e.date_registered) > CURDATE() - INTERVAL 1 DAY
					WHERE ca.user_id = ".$user->user_id."
					group by ca.city_id
					order by ca.city_id ";

                $new_escorts_in_cities = $this->_db->query($new_escorts_sql)->fetchAll();
                $buttons = '';

                if(!empty($new_escorts_in_cities)){
                    $this->count = count($new_escorts_in_cities);
	                foreach ($new_escorts_in_cities as  $new_escorts_in_city) {

	                    $buttons .= '<tr><td width="auto" align="center" valign="middle" height="30" style="background-color: #5cb85c;border-top-left-radius:4px;border-bottom-left-radius:4px;border-top-right-radius:4px;border-bottom-right-radius:4px;background-clip: padding-box;font-size:13px;font-family:Helvetica, arial, sans-serif;text-align:center;color:#ffffff;font-weight: 300;padding-left:18px;padding-right:18px;"><span style="color: #ffffff; font-weight: 300;">
	                                  		<a target="_blank" style="color: #ffffff; text-align:center;text-decoration: none;" 
	                                  		href="'.$app->url.'/escorts/nuove/city_gb_'.$new_escorts_in_city->slug.'/#newsletterPopupTurnOff">SEE PROFILES IN '.strtoupper($new_escorts_in_city->title_en).'</a></span>
	                                                                        </td><td width="auto" height="10"></td></tr><tr>
	                                    <td width="100%" height="10"></td>
	                                 </tr>';
	                }

	                $result = Cubix_Email::sendTemplate( 'member_city_alert', $user->email, array(
	                    'username' => $user->username,
	                    'buttons' => $buttons,
                        'user_id' => $user->user_id,
                        'backend_user' => $user->backend_user
	                ));

	               $this->cli->out($user->user_id. ' - email sent' . " \n ");

	            }
            }
    }

    public function escortAccountNotComplete($daily = false){

    	$where = '';

    	if($daily){
            $where  = ' AND (DATE(u.date_registered) = CURDATE() - INTERVAL '.$daily.' DAY) AND user_type = \'escort\'';
    	}

	    $sql = "
	     SELECT  u.id as id,u.email as email,u.username as username,u.backend_user as bu
	                    FROM escorts e
	                    INNER JOIN users u on u.id = e.user_id
	                    WHERE  (not e.status & 128 and not e.status & 32)
								AND u.status = 1 {$where} GROUP BY u.id";


            $result = $this->_db->query($sql)->fetchAll();

            $this->count = count($result);

            foreach ($result as $user) {
                Cubix_Email::sendTemplate('escort_profile_not_complete_reminder', $user->email, array(
                    'username' => $user->username,
                    'user_id' => $user->id,
                    'backend_user' => $user->bu
                ));
                $this->cli->out($user->id. ' - email sent' . " \n ");
            }
    }

    public function packageExpirationThreeDaysNoticeAction(){

        $result = $this->getExpiredOrdersByDaysCount(3);

        if(!$result){
        	$this->cli->out("Packages doesn't exists");
        	return ;
        }
        $this->count = count($result);
            foreach ($result as $user){
                 Cubix_Email::sendTemplate('escort_package_expiration_3_days', $user->email, array(
                     'username' => $user->username,
                     'user_id' => $user->id,
                     'backend_user' => $user->bu,
                     'package' => $user->package,
                     'date' => $user->expiration_date
                 ));

                $this->cli->out($user->id. ' - email was send for package expiration 3 days' . " \n ");
            }
    }

    public function packageExpirationSMSNotice($days, $template){

        set_time_limit(0);
        $originator = Cubix_Application::getPhoneNumber(Cubix_Application::getId());
        $model_sms = new Model_SMS_Outbox();
        $bu_user = Zend_Auth::getInstance()->getIdentity();
        $sms_outbox = new Model_SMS_EmailOutbox();

        $packages = $this->_db->fetchAll("
            SELECT
                op.id, op.escort_id, op.status, op.application_id, previous_order_package_id, op.package_id, ep.contact_phone_parsed, bu.username AS sales_username, bu.id AS sales_person_id,
                DATEDIFF(op.expiration_date, DATE(NOW()) ) AS days_left, e.showname, u.id as userid, u.email
            FROM order_packages op
            INNER JOIN escorts e ON e.id = op.escort_id
            INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
            INNER JOIN users u ON u.id = e.user_id
            INNER JOIN backend_users bu ON bu.id = u.sales_user_id
             WHERE DATEDIFF(op.expiration_date, DATE(NOW())) = ?
                AND op.status = ? 
                AND op.status <> ?
        ", array( $days, Model_Packages::STATUS_ACTIVE, Model_Billing_Packages::STATUS_SUSPENDED));

        foreach($packages as $package) {
            //Checking if has pending package.
            //We must send sms only if they don't have pending package

            $has_active_package = $this->_db->fetchOne('
                SELECT TRUE FROM order_packages WHERE escort_id = ? AND status = ?
            ', array($package->escort_id, Model_Billing_Packages::STATUS_PENDING));


            if ( ! $has_active_package ) {


                $message = $this->_db->fetchRow('
                    SELECT template 
                    FROM sms_templates WHERE title = ?
                ', $template);


                $data = array(
                    'escort_id' => $package->escort_id,
                    'phone_number' => $package->contact_phone_parsed,
                    'phone_from' => $originator,
                    'text' => $message->template,
                    'application_id' => Cubix_Application::getId(),
                    'sender_sales_person' => $package->sales_person_id
                );

                $model_sms->saveAndSend($data);
          
                $outbox_data = array(
                            'email' => $package->email,
                            'subject' => 'ad expiration '.$days.'days',
                            'text' => $message->template,
                            'backend_user_id' => $bu_user->id,
                            'date' => new Zend_Db_Expr('NOW()'),
                            'user_id' => $package->userid,
                            'application_id' => Cubix_Application::getId()
                        );

                $sms_outbox->add($outbox_data);
            }
            $this->cli->out(' SMS sent -'. $package->contact_phone_parsed . " \n ");
        }



    }

    public function packageExpirationSMSThreeDaysNoticeAction(){
        $this->packageExpirationSMSNotice( 3, 'ad expiration 3d' );
    }

    public function packageExpirationNotice()
    {

        $result = $this->getExpiredOrdersByDaysCount(0);

        if (!$result) {
            $this->cli->out("Package doesn't exists");
            return;
        }

        $this->count = count($result);
        foreach ( $result as $user ) {

            $res = Cubix_Email::sendTemplate( 'escort_package_expiration_12_midday', $user->email, array(
                'username' => $user->username,
                'user_id' => $user->id,
                'backend_user' => $user->bu,
                'package' => $user->package
            ) );

            $this->cli->out( $user->id . ' - email was send about package expiration' . " \n " );
        }
    }

    public function packageExpirationSMSNoticeAction(){
        $this->packageExpirationSMSNotice(1, 'ad expiration 24h');
    }

    public function agencyAccountNotComplete($daily = false)
    {

        $where = '';

        if ($daily) {
            $where = ' AND (DATE(u.date_registered) = CURDATE() - INTERVAL '.$daily.' DAY) AND user_type = \'agency\'';
        }

        $sql = "
	     SELECT  u.id as id,u.email as email,u.username as username, u.backend_user as bu
	                    FROM agencies ag
	                    INNER JOIN users u on u.id = ag.user_id
	                    WHERE  ag.last_modified IS NULL AND u.status = 1 {$where}";
        $result = $this->_db->query($sql)->fetchAll();
        var_dump($sql);

        if (!$result) {
            $this->cli->out("Agencies doesn't exists");
            return;
        }
        $this->count = count($result);
        foreach ($result as $user) {
            Cubix_Email::sendTemplate('agency_profile_not_completed', $user->email, array(
                'username' => $user->username,
                'user_id' => $user->id,
                'backend_user' => $user->bu,
            ));
            $this->cli->out($user->id . ' - email was send to agency ' . " \n ");
        }

    }


    public function agencyNoActiveEscorts($daily = false)
    {

        $where = '1';

        if ($daily) {
            $where = ' (DATE(u.date_registered) + INTERVAL 2 DAY) = CURDATE() ';
        }

        $statuses = array(USER_STATUS_DISABLED, USER_STATUS_DELETED);

        $sql = "SELECT  u.id as id,u.email as email,u.username as username,u.date_registered, u.backend_user as bu
	                    FROM agencies ag
	                            INNER JOIN users u on u.id = ag.user_id
								LEFT JOIN escorts e ON e.agency_id = ag.id
                                   WHERE {$where} and u.status NOT IN ('". implode("','", $statuses) ."')
                                    GROUP BY ag.id 
                                        HAVING COUNT(e.agency_id) = 0 
                                            ORDER BY u.id DESC";


        $result = $this->_db->query($sql)->fetchAll();

        if (!$result) {
            $this->cli->out("Agency with no active escorts doesn't exists");
            return;
        }
        $this->count = count($result);
        foreach ($result as $user) {
            Cubix_Email::sendTemplate('agency_no_active_escorts', $user->email, array(
                'username' => $user->username,
                'user_id' => $user->id,
                'backend_user' => $user->bu,
            ));
            $this->cli->out($user->id . ' - email was send to agency ' . " \n ");

        }

    }

    /*
 *  Not enough escorts (1-3 active escorts)-to send every 30 days if it’s the case
 * */

    public function agencyNotEnoughEscorts()
    {
        $where = '';
        if ( Cubix_Application::getId() == APP_EG_CO_UK )
            $where .= 'WHERE u.status NOT IN (-5, -4) ';

        $sql = "SELECT  u.id as id,u.email as email,u.username as username,u.date_registered, u.backend_user as bu, COUNT(e.agency_id) as count_escorts
	                    FROM agencies ag
	                            INNER JOIN users u on u.id = ag.user_id
								LEFT JOIN escorts e ON e.agency_id = ag.id
                                        ".$where."
                                    GROUP BY ag.id 
                                        HAVING COUNT(e.agency_id) > 0 AND COUNT(e.agency_id) < 3 
                                            ORDER BY u.id DESC;";

        $result = $this->_db->query($sql)->fetchAll();


        if (!$result) {
            $this->cli->out("Agency with no active escorts doesn't exists");
            return;
        }
        $this->count = count($result);
        foreach ($result as $user) {
            Cubix_Email::sendTemplate('agency_not_enough_escorts', $user->email, array(
                'username' => $user->username,
                'user_id' => $user->id,
                'backend_user' => $user->bu,
            ));
            $this->cli->out($user->id . ' - email was send to agency ' . " \n ");
        }

    }

    public function agencyPackageExpirationNotice($days_count,$template)
    {

        $result = $this->getExpiredOrdersByDaysCount($days_count, true);

        if (!$result) {
            $this->cli->out("No result !!!");
            return;
        }
        $this->count = count($result);
        foreach ($result as $user) {
            Cubix_Email::sendTemplate($template, $user->email, array(
                'username' => $user->username,
                'user_id' => $user->id,
                'backend_user' => $user->bu,
                'package' => $user->package,
                'showname' => $user->escort_showname,
                'date' => $user->expiration_date
            ));

            $this->cli->out($user->id . ' - email was send for package expiration day' . " \n ");
        }

    }

    /*
     * $days_count  | if 0 will return all expired packages on today
     * */

    private function getExpiredOrdersByDaysCount($days_count = 0,$agency = false){

        $external_join = " INNER JOIN users u ON u.id = e.user_id ";

        $external_select = " ";

        if($agency){
            $external_join = " INNER JOIN agencies a ON a.id = e.agency_id ";
            $external_join .= " INNER JOIN users u ON u.id = a.user_id ";
            $external_select .= " , e.showname as escort_showname";
        }

       $sql = "SELECT u.id,u.email,u.username,u.backend_user as bu, op.expiration_date,p.name AS package {$external_select} 
                                FROM order_packages op 
                                INNER JOIN escorts e ON e.id = op.escort_id
                                INNER JOIN packages p ON p.id = op.package_id
                                {$external_join}
                                
                                where (DATE(op.expiration_date) = CURDATE() + INTERVAL {$days_count} DAY) AND op.status = 2";

        return $this->_db->query($sql)->fetchAll();
    }

    private function updateLastSentDate($template_id){
    	try {
    		$date = new DateTime();
    		$this->_db->update('cron_emails',
    		   				array('last_sent_date' => $date->format('Y-m-d h:i:s')),
    		   					    $this->_db->quoteInto( 'template_id = ?', $template_id));
    	
    	} catch (Exception $e) {
    		var_dump($e->getMessage());
    	}	
    }

    private function sendMailforDevNotification($template){
        $emails = array('Zhora'=>'zhorabrainforce@gmail.com','Max'=>'mihranhayrapetyan@gmail.com');

        foreach ($emails as $name => $email){
            Cubix_Email::sendTemplate('cron_email_notification', $email, array(
                'username' => $name,
                'template' => $template,
                'count' => $this->count,
                'date' => date('Y-m-d h:i:s')
            ));
        }

        $this->count = 0;

    }
}
