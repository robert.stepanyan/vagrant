<?php

class MigrateController extends Zend_Controller_Action
{
	public function init()
	{
		/*$this->_db = Zend_Db::factory('mysqli', array(
			'host' => '172.16.0.91',
			'username' => 'root',
			'password' => 'Ghhy25p4',
			'dbname' => 'escortguide_com'
		));*/
		define('MAPP_ID', 1);

		/*$this->_db = Zend_Db::factory('mysqli', array(
			'host' => '127.0.0.1',
			'username' => 'root',
			'password' => '123456',
			'dbname' => 'ed_b'
		));
		$this->_db->setFetchMode(Zend_Db::FETCH_OBJ);
		$this->_db->query('SET NAMES `utf8`');*/
		/*$this->_db2 = Zend_Db::factory('mysqli', array(
			'host' => '192.168.0.1',
			'username' => 'root',
			'password' => '123456',
			'dbname' => 'escortforum_migrated'
		));*/
		$this->_db2 = Zend_Registry::get('db');
		// $this->_db2->query('SET NAMES `utf8`');
		
		//$this->_db->setFetchMode(Zend_Db::FETCH_OBJ);
		$this->_db2->setFetchMode(Zend_Db::FETCH_OBJ);
	}

	

	public function fixToursAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$cli = new Cubix_Cli();
		$pid_file = '/var/run/ed-migrate-escorts-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);

		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}

		set_time_limit(0);
		ini_set('memory_limit', '4048M');

		$order_packages = $this->_db2->fetchAll('SELECT id AS order_package_id FROM order_packages WHERE status = 2');

		foreach($order_packages as $i => $op) {

			$order_package_product = $this->_db2->fetchOne('SELECT TRUE FROM order_package_products WHERE order_package_id = ? AND product_id = 7', array($op->order_package_id));

			if ( ! $order_package_product ) {
				$this->_db2->insert('order_package_products',
				array(
					'order_package_id' => $op->order_package_id,
					'product_id' => 7,
					'is_optional' => 0,
					'price' => 0
				));
			}

			$cli->out($i . "/ Order Package ID:" . $op->order_package_id . "\n");
		}

		die('done');
	}

	public function fixSnapshotsAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$cli = new Cubix_Cli();
		$pid_file = '/var/run/ed-migrate-escorts-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);

		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}

		set_time_limit(0);
		ini_set('memory_limit', '4048M');

		$escorts = $this->_db2->fetchAll('SELECT id FROM escorts WHERE status & 32');

		foreach($escorts as $i => $escort) {
			$m_snapshot = new Model_Escort_SnapshotV2(array('id' => $escort->id));
			$m_snapshot->snapshotProfileV2();

			$cli->out($i . "/ Escort ID:" . $escort->id . " image is OKKK. Image ID:" . $escort->id . "\n");
		}

		die('done');
	}

	public function edMigrateEscortsAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$cli = new Cubix_Cli();
		$pid_file = '/var/run/ed-migrate-escorts-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);

		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}

		set_time_limit(0);
		ini_set('memory_limit', '4048M');

		$tmp_dir = sys_get_temp_dir();

		$old_escorts = $this->_db->fetchAll('
			SELECT
				l.id, l.name AS showname, l.url, l.description, l.email,
				l.image, lc.id AS city_id, lc.name AS city_name
			FROM esc_links l
			INNER JOIN esc_links_cat lc ON lc.id = l.cat_id
			WHERE l.approved = 1 AND l.active = 1
			GROUP BY l.email
			order by l.id asc
			/*limit 0, 3000*/
		');

		$count = 0;
		if ( count($old_escorts) ) {
			$i = 1;
			foreach ( $old_escorts as $old_escort ) {
				$img_src = @file_get_contents("http://www.escortdirectory.com/uploads/locations_original/" . $old_escort->image);
				if ( ! $img_src ) {
					$img_src = @file_get_contents("http://www.escortdirectory.com/uploads/locations/" . $old_escort->image);
				}

				if ( $img_src ) {

					$old_escort->showname = preg_replace('#Escort (.+) \- #', '', trim($old_escort->showname));

					$file = $tmp_dir . DIRECTORY_SEPARATOR . $old_escort->image;
					file_put_contents($file, $img_src);

					$img_info = @getimagesize($file);
					if ( $img_info[0] < 225 || $img_info[1] < 225 ) {
						$cli->out($i . "/ Escort ID:" . $old_escort->id . " image is too small. Image ID:" . $old_escort->image . "\n");
						//echo $i . ". Escort ID:" . $old_escort->id . " image is too small. Image ID:" . $old_escort->image . "<br/>";
						$i++;
						continue;
					}

					$cli->out($i . "/ Escort ID:" . $old_escort->id . " image is OKKK. Image ID:" . $old_escort->image . "\n");
					//echo $i . ". Escort ID:" . $old_escort->id . " image is OKKKK. Image ID:" . $old_escort->image . " - OK <br/>";
					$i++;

					$salt_hash = Cubix_Salt::generateSalt(trim($old_escort->email));
					$pass = Cubix_Salt::hashPassword(trim($old_escort->email), $salt_hash);

					$this->_db2->insert('users', array(
						'email' => trim($old_escort->email),
						'username' => preg_replace("#-#", '_', Cubix_Utils::makeSlug(trim($old_escort->showname))) . '_' . $old_escort->id,
						'password' => $pass,
						'salt_hash' => $salt_hash,
						'user_type' => 'escort',
						'status' => 1,
						'disabled' => 0,
						'sales_user_id' => 1,
						'application_id' => 69,
					));

					$new_user_id = $this->_db2->lastInsertId();

					$this->_db2->insert('escorts', array(
						'user_id' => $new_user_id,
						'showname' => trim($old_escort->showname),
						'gender' => 1,
						'country_id' => $this->_db2->fetchOne('SELECT country_id FROM cities WHERE id = ?', $old_escort->city_id),
						'city_id' => $old_escort->city_id,
						'status' => 32,
					));

					$new_escort_id = $this->_db2->lastInsertId();

					$this->_db2->insert('escort_cities', array(
						'escort_id' => $new_escort_id,
						'city_id' => $old_escort->city_id,
					));

					$this->_db2->insert('escort_profiles_v2', array(
						'escort_id' => $new_escort_id,
						'showname' => trim($old_escort->showname),
						'gender' => 1,
						'about_en' => $old_escort->description,
						'about_de' => $old_escort->description,
						'about_fr' => $old_escort->description,
						'about_it' => $old_escort->description,
						'about_pt' => $old_escort->description,
						'about_ro' => $old_escort->description,
						'email' => $old_escort->email,
						'website' => $old_escort->url,
					));

					$profile = array(
						'escort_id' => $new_escort_id,
						'showname' => trim($old_escort->showname),
						'gender' => 1,
						'country_id' => $this->_db2->fetchOne('SELECT country_id FROM cities WHERE id = ?', $old_escort->city_id),
						'city_id' => $old_escort->city_id,
						'about_en' => $old_escort->description,
						'about_de' => $old_escort->description,
						'about_fr' => $old_escort->description,
						'about_it' => $old_escort->description,
						'about_pt' => $old_escort->description,
						'about_ro' => $old_escort->description,
						'email' => $old_escort->email,
						'website' => $old_escort->url,
					);

					$this->_db2->insert('profile_updates_v2', array(
						'escort_id' => $new_escort_id,
						'revision' => 1,
						'date_updated' => new Zend_Db_Expr('NOW()'),
						'data' => serialize($profile),
						'status' => 2,
					));

					$m_snapshot = new Model_Escort_SnapshotV2(array('id' => $new_escort_id));
					$m_snapshot->snapshotProfileV2();

					try {
						$images = new Cubix_Images();
						$image = $images->save($file, $new_escort_id, 69, strtolower(@end(explode('.', $old_escort->image))));


						$image = new Cubix_Images_Entry($image);
						$image->setSize('sthumb');
						$image->setCatalogId($new_escort_id);
						$image_url = $images->getUrl($image);


						$photo_arr = array(
							'escort_id' => $new_escort_id,
							'hash' => $image->getHash(),
							'ext' => $image->getExt(),
							'type' => ESCORT_PHOTO_TYPE_HARD,
							'creation_date' => date('Y-m-d H:i:s', time())
						);
						$photo_arr['is_approved'] = 1;


						$photo = new Model_Escort_PhotoItem($photo_arr);

						$model = new Model_Escort_Photos();
						$photo = $model->save($photo);

					} catch(Exception $e) {
						echo $e->getMessage();
						continue;
					}


				} else {
					$cli->out($i . "/ Escort ID:" . $old_escort->id . " NO IMAGE. Image ID:" . $old_escort->image . "\n");
					//echo $i . ". Escort ID:" . $old_escort->id . " NO IMAGE. Image ID:" . $old_escort->image . " - NO IMAGE <br/>";
					$i++;
				}
			}
		}

		//var_dump($count);
		die;
	}

	public function edMigrateCountriesAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '2048M');

		$items = $this->_db->fetchAll('SELECT id, country FROM esc_links_cat WHERE parent_id <> 0 and country <> "" GROUP BY country');

		foreach( $items as $item ) {

			$this->_db2->insert('countries', array(
				//'id' => $item->id,
				'slug' => Cubix_Utils::makeSlug(Cubix_Model::translit($item->country)),
				'title_en' => $item->country,
				'title_pt' => $item->country,
				'title_es' => $item->country,
				'title_fr' => $item->country,
				'title_it' => $item->country,
				'title_ro' => $item->country,
				'title_de' => $item->country,
			));

		}

		die;
	}

	public function edGeoipCitiesAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '2048M');

		$items = $this->_db2->fetchAll('
			SELECT
				c.id, c.title_en AS city_title, cr.title_en AS country_title
			FROM cities c
			INNER JOIN countries cr ON cr.id = c.country_id
			WHERE c.latitude IS NULL AND c.longitude IS NULL
		');

		foreach( $items as $item ) {

			$address = array(
				$item->city_title,
				$item->country_title
			);

			$url = 'http://maps.googleapis.com/maps/api/geocode/json?address=' . implode(',', $address) .  '&sensor=false';
			$url = preg_replace('# #', '+', $url);

			$data = file_get_contents($url);

			$data = json_decode($data);

			$coord = array(
				'latitude' => $data->results[0]->geometry->location->lat,
				'longitude' => $data->results[0]->geometry->location->lng
			);

			print_r($coord);

			$this->_db2->update('cities', $coord, $this->_db2->quoteInto('id = ?', $item->id));

		}

		die;
	}

	public function edFixPhoneFreeAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '2048M');

		$items = $this->_db2->fetchAll('SELECT escort_id, phone_number FROM escort_profiles_v2');

		foreach( $items as $item ) {

			$this->_db2->update('escort_profiles_v2', array(
				'contact_phone_free' => preg_replace('/[^0-9]/', ' ', $item->phone_number),
			), $this->_db2->quoteInto('escort_id = ?', $item->escort_id));
		}

		die;
	}

	public function edMigratePhoneAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '2048M');

		$items = $this->_db2->fetchAll('SELECT id, title_en, phone_prefix FROM countries_copy');

		foreach( $items as $item ) {

			if ( $id = $this->_db2->fetchOne('SELECT id FROM countries WHERE title_en = ?', $item->title_en) ) {
				//var_dump($item->title_en);
				$this->_db2->update('countries', array('phone_prefix' => $item->phone_prefix), $this->_db2->quoteInto('id = ?', $id));
			}
		}

		die;
	}


	public function fixDiccAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '2048M');

		$dictionary = $this->_db2->fetchAll('SELECT * FROM dictionary');

		$langs = array('es', 'hu');

		foreach( $dictionary as $item ) {

			foreach ( $langs as $lang ) {
				if ( ! strlen($item->{'value_' . $lang}) ) {
					$this->_db2->update('dictionary', array('value_' . $lang => $item->value_en), $this->_db2->quoteInto('id = ?', $item->id));
				}
			}
		}




		die;
	}

	public function migrateGeoAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '2048M');

		$cities = $this->_db2->fetchAll('SELECT * FROM cities');

		$langs = array('es', 'hu');

		foreach( $cities as $city ) {

			foreach ( $langs as $lang ) {
				if ( ! strlen($city->{'title_' . $lang}) ) {
					$this->_db2->update('cities', array('title_' . $lang => $city->title_en), $this->_db2->quoteInto('id = ?', $city->id));
				}
			}
		}




		die;
	}

	public function migrateGeoFAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '2048M');

		$cities = $this->_db2->fetchAll('SELECT * FROM f_cities');

		$langs = array('es', 'hu');

		foreach( $cities as $city ) {

			foreach ( $langs as $lang ) {
				if ( ! strlen($city->{'title_' . $lang}) ) {
					$this->_db2->update('f_cities', array('title_' . $lang => $city->title_en), $this->_db2->quoteInto('id = ?', $city->id));
				}
			}
		}
		
		die;
	}

	public function migrateSeoAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '2048M');

		$seo_entities = $this->_db2->fetchAll('SELECT * FROM seo_entities');

		$langs = array('es', 'hu');

		foreach( $seo_entities as $seo_entitie ) {

			foreach ( $langs as $lang ) {
				if ( ! strlen($seo_entitie->{'title_' . $lang}) ) {
					$this->_db2->update('seo_entities', array('title_' . $lang => $seo_entitie->title_en), $this->_db2->quoteInto('id = ?', $seo_entitie->id));
				}
				if ( ! strlen($seo_entitie->{'heading_' . $lang}) ) {
					$this->_db2->update('seo_entities', array('heading_' . $lang => $seo_entitie->heading_en), $this->_db2->quoteInto('id = ?', $seo_entitie->id));
				}
				if ( ! strlen($seo_entitie->{'meta_description_' . $lang}) ) {
					$this->_db2->update('seo_entities', array('meta_description_' . $lang => $seo_entitie->meta_description_en), $this->_db2->quoteInto('id = ?', $seo_entitie->id));
				}
				if ( ! strlen($seo_entitie->{'meta_keywords_' . $lang}) ) {
					$this->_db2->update('seo_entities', array('meta_keywords_' . $lang => $seo_entitie->meta_keywords_en), $this->_db2->quoteInto('id = ?', $seo_entitie->id));
				}
				if ( ! strlen($seo_entitie->{'footer_text_' . $lang}) ) {
					$this->_db2->update('seo_entities', array('footer_text_' . $lang => $seo_entitie->footer_text_en), $this->_db2->quoteInto('id = ?', $seo_entitie->id));
				}
			}
		}

		die;
	}

	public function migrateSeoEntityAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '2048M');

		$seo_entities = $this->_db2->fetchAll('SELECT * FROM seo_entity_instances');

		$langs = array('es', 'hu');

		foreach( $seo_entities as $seo_entitie ) {

			foreach ( $langs as $lang ) {
				if ( ! strlen($seo_entitie->{'title_' . $lang}) ) {
					$this->_db2->update('seo_entity_instances', array('title_' . $lang => $seo_entitie->title_en), $this->_db2->quoteInto('id = ?', $seo_entitie->id));
				}
				if ( ! strlen($seo_entitie->{'heading_' . $lang}) ) {
					$this->_db2->update('seo_entity_instances', array('heading_' . $lang => $seo_entitie->heading_en), $this->_db2->quoteInto('id = ?', $seo_entitie->id));
				}
				if ( ! strlen($seo_entitie->{'meta_description_' . $lang}) ) {
					$this->_db2->update('seo_entity_instances', array('meta_description_' . $lang => $seo_entitie->meta_description_en), $this->_db2->quoteInto('id = ?', $seo_entitie->id));
				}
				if ( ! strlen($seo_entitie->{'meta_keywords_' . $lang}) ) {
					$this->_db2->update('seo_entity_instances', array('meta_keywords_' . $lang => $seo_entitie->meta_keywords_en), $this->_db2->quoteInto('id = ?', $seo_entitie->id));
				}
				if ( ! strlen($seo_entitie->{'footer_text_' . $lang}) ) {
					$this->_db2->update('seo_entity_instances', array('footer_text_' . $lang => $seo_entitie->footer_text_en), $this->_db2->quoteInto('id = ?', $seo_entitie->id));
				}
				if ( ! strlen($seo_entitie->{'heading_escort_' . $lang}) ) {
					$this->_db2->update('seo_entity_instances', array('heading_escort_' . $lang => $seo_entitie->heading_escort_en), $this->_db2->quoteInto('id = ?', $seo_entitie->id));
				}
				if ( ! strlen($seo_entitie->{'heading_trans_' . $lang}) ) {
					$this->_db2->update('seo_entity_instances', array('heading_trans_' . $lang => $seo_entitie->heading_trans_en), $this->_db2->quoteInto('id = ?', $seo_entitie->id));
				}
			}
		}

		die;
	}

	public function migrateCountriesPhoneCodeAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '2048M');

		$codes = $this->_db2->fetchAll('SELECT * FROM countries_phone_code');

		$langs = array('es', 'hu');

		foreach( $codes as $code ) {

			foreach ( $langs as $lang ) {
				if ( ! strlen($code->{'title_' . $lang}) ) {
					$this->_db2->update('countries_phone_code', array('title_' . $lang => $code->title_en), $this->_db2->quoteInto('id = ?', $code->id));
				}
			}
		}
		die;
	}

	public function migrateGeoZonesAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '2048M');

		$cities = $this->_db2->fetchAll('SELECT * FROM cityzones');

		$langs = array('es', 'hu');

		foreach( $cities as $city ) {

			foreach ( $langs as $lang ) {
				if ( ! strlen($city->{'title_' . $lang}) ) {
					$this->_db2->update('cityzones', array('title_' . $lang => $city->title_en), $this->_db2->quoteInto('id = ?', $city->id));
				}
			}
		}




		die;
	}

	public function migrateGeoRegionsAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '2048M');

		$cities = $this->_db2->fetchAll('SELECT * FROM regions');

		$langs = array('es', 'hu');

		foreach( $cities as $city ) {

			foreach ( $langs as $lang ) {
				if ( ! strlen($city->{'title_' . $lang}) ) {
					$this->_db2->update('regions', array('title_' . $lang => $city->title_en), $this->_db2->quoteInto('id = ?', $city->id));
				}
			}
		}




		die;
	}

	public function migrateGeoNatAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '2048M');

		$cities = $this->_db2->fetchAll('SELECT * FROM nationalities');

		$langs = array('es', 'hu');

		foreach( $cities as $city ) {

			foreach ( $langs as $lang ) {
				if ( ! strlen($city->{'title_' . $lang}) ) {
					$this->_db2->update('nationalities', array('title_' . $lang => $city->title_en), $this->_db2->quoteInto('id = ?', $city->id));
				}
			}
		}




		die;
	}

	public function migrateGeoCountriesAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '2048M');

		$countries = $this->_db2->fetchAll('SELECT * FROM countries');

		$langs = array('es', 'hu');

		foreach( $countries as $item ) {

			foreach ( $langs as $lang ) {
				if ( ! strlen($item->{'title_' . $lang}) ) {
					$this->_db2->update('countries', array('title_' . $lang => $item->title_en), $this->_db2->quoteInto('id = ?', $item->id));
				}
			}
		}




		die;
	}
	
	public function fixShitAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db_normal = $this->_db;
		$db_sucked = $this->_db2;
		
		$normal_orders = $db_normal->fetchAll('SELECT id, price FROM orders');
		
		foreach ( $normal_orders as $order ) {
			$db_sucked->update('orders', array('price' => $order['price']), $db_sucked->quoteInto('id = ?', $order['id']));
		}
		
		$normal_transfers = $db_normal->fetchAll('SELECT id, amount FROM transfers');
		
		foreach ( $normal_transfers as $transfer ) {
			$db_sucked->update('transfers', array('amount' => $transfer['amount']), $db_sucked->quoteInto('id = ?', $transfer['id']));
		}
		
		die('Done!');	
	}

	public function fixSmsEscortsAction()
	{
		set_time_limit(0);
		
		$smss = $this->_db2->fetchAll('SELECT id, escort_id, phone_from FROM sms_inbox');
		
		foreach ( $smss as $sms ) {
			$escort_id = $this->_db2->fetchOne('SELECT escort_id FROM escort_profiles_v2 WHERE contact_phone_parsed = ?', array($sms->phone_from));
			if ( $escort_id ) {
				$this->_db2->update('sms_inbox', array('escort_id' => $escort_id), $this->_db2->quoteInto('id = ?', $sms->id));
			}
		}
		
		die('Done');
	}
	
	
	protected function _addFields($table, $name, $length, $allow_null = false)
	{
		$langs = $this->_db2->query('
			SELECT id FROM langs ORDER BY FIELD(id, "en", "it", "de")
		')->fetchAll();
		
		echo "<h3>$table:</h3>" . "<br/>\r\n";
		
		$type = 'VARCHAR';
		if ( $length == 'TEXT' ) {
			$type = 'TEXT'; $length = '';
		}
		else {
			$length = '(' . $length . ')';
		}
		
		$exceptions = array();
		foreach ( $langs as $lang ) {
			try {
				$this->_db2->query('
					ALTER TABLE `' . $table . '`
					ADD COLUMN `' . $name . '_' . $lang->id . '`
					' . $type . ' ' . $length . '
					' . ($allow_null ? 'NOT' : 'DEFAULT') . ' NULL
				');
			} catch (Exception $e) {
				$exceptions[] = $e;
			}
		}
		
		foreach ( $exceptions as $e ) {
			echo $e->getMessage() . "<br/>\r\n";
		}
		
		echo "<br/>\r\n";
		echo "<strong>Done!</strong>";
		echo "<br/>\r\n";
	}
	
	public function l10nAction()
	{
		/*$this->_addFields('countries', 'title', '50', false);
		$this->_addFields('regions', 'title', '50', false);
		$this->_addFields('cities', 'title', '50', false);
		$this->_addFields('cityzones', 'title', '50', false);
		$this->_addFields('nationalities', 'title', '50', false);
		$this->_addFields('escort_profiles', 'about', 'TEXT', true);
		
		$this->_addFields('seo_entities', 'title', '255', true);
		$this->_addFields('seo_entities', 'heading', '255', true);
		$this->_addFields('seo_entities', 'meta_description', 'TEXT', true);
		$this->_addFields('seo_entities', 'meta_keywords', 'TEXT', true);
		
		$this->_addFields('seo_entity_instances', 'title', '255', true);
		$this->_addFields('seo_entity_instances', 'heading', '255', true);
		$this->_addFields('seo_entity_instances', 'meta_description', 'TEXT', true);
		$this->_addFields('seo_entity_instances', 'meta_keywords', 'TEXT', true);
		
		$this->_addFields('newsletter_templates', 'subject', '255', true);
		$this->_addFields('newsletter_templates', 'body_text', 'TEXT', true);
		$this->_addFields('newsletter_templates', 'body_html', 'TEXT', true);*/

		// $this->_addFields('escort_profiles', 'svc_additional', 'TEXT', true);

		die;
	}
	
	public function escortsAction()
	{
		set_time_limit(0);
		
		$APP_ID = 7;
		
		$defs = Zend_Registry::get('definitions');
		
		$db = $this->_db;
		$db2 = $this->_db2;
		
		echo 'Clearing `users` table' . "\r\n";
		$db2->query('TRUNCATE `users`');
		
		echo 'Clearing `escorts` table' . "\r\n";
		$db2->query('TRUNCATE `escorts`');
		
		echo 'Clearing `escort_profiles` table' . "\r\n";
		$db2->query('TRUNCATE `escort_profiles`');
		
		echo 'Porting escorts' . "\r\n";
		$db->query('
			INSERT INTO escortforum_v2.users (id, email, username, password, activation_hash, date_registered, date_last_refreshed, status, disabled, sales_user_id, application_id) 
			(
				SELECT u.id, u.email, u.login, u.password, u.verification, u.creation_date, e.new_refresh, u.status, 0, e.admin_user_id, u.application_id
				FROM esc_users u
				INNER JOIN esc_escorts e ON e.user_id = u.id
				WHERE u.user_type = 1 AND u.application_id = ' . $APP_ID . '
				GROUP BY u.id
			)
		');
		
		$db->query('
			INSERT INTO escortforum_v2.escorts (id, user_id, agency_id, showname, gender, country_id, city_id, cityzone_id, is_verified, date_last_modified)
			(
				SELECT e.id, e.user_id, NULL, e.showname, e.gender, 
					(SELECT id FROM escortforum_v2.countries WHERE iso = e.country_iso), 
					(
						SELECT v2c.id
						FROM escort_main_2.esc_escort_cities ec
						INNER JOIN escortforum_v2.cities v2c ON v2c.internal_id = ec.city_id
						WHERE ec.escort_id = e.id
						ORDER BY ec.is_default
						LIMIT 1
					),
					NULL, e.verified, e.last_modified
				FROM escort_main_2.esc_escorts e
				INNER JOIN escort_main_2.esc_users u ON u.id = e.user_id
				WHERE u.user_type = 1 AND u.application_id = ' . $APP_ID . '
				GROUP BY e.id
			)
		');
		
		$db->query('
			INSERT INTO escortforum_v2.escort_profiles (
				escort_id, height, weight, bust, waist, hip, shoe_size, breast_size, dress_size, is_smoker, hair_color, eye_color, birth_date, characteristics,
				about, contact_phone, phone_instructions, contact_email, contact_web, contact_zip, measure_units, availability, sex_availability, svc_kissing, svc_blowjob, 
				svc_cumshot, svc_69, svc_anal, svc_additional, ethnicity, nationality, admin_verified, vac_date_from, vac_date_to, _photos, _photos_private
			) (
				SELECT
					ed.id, ed.height, ed.weight, ed.bust, ed.waist, ed.hip, ed.shoe_size, ed.breast_size, ed.dress_size, ed.smoker, ed.hair_color, ed.eye_color, ed.birth_date,
					ed.characteristics, ed.about_me, ed.contact_phone, ed.phone_instructions, ed.contact_email, ed.contact_web, ed.contact_zip, ed.metric_system,
					ed.available_for, ed.sex_availability, ed.s_kissing, ed.s_blowjob, ed.s_cumshot, ed.s_69, ed.s_anal, ed.svc_additional, ed.ethnic, ed.nationality_iso, e.admin_verified,
					e.vacation_since, e.vacation_till, ed.photos, ed.photos_private
				FROM escort_main_2.esc_escort_details ed
				INNER JOIN escort_main_2.esc_escorts e ON e.id = ed.id
				INNER JOIN escort_main_2.esc_users u ON u.id = e.user_id
				WHERE u.user_type = 1 AND u.application_id = ' . $APP_ID . '
				GROUP BY ed.id
			)
		');
		
		echo 'Clearing `escort_cities` table' . "\r\n";
		$db2->query('TRUNCATE `escort_cities`');
		
		$db->query('
			INSERT INTO escortforum_v2.escort_cities (escort_id, city_id)
			(
				SELECT ec.escort_id, v2c.id AS city_id
				FROM esc_escort_cities ec
				INNER JOIN escortforum_v2.cities v2c ON v2c.internal_id = ec.city_id
				INNER JOIN esc_escorts e ON e.id = ec.escort_id
				INNER JOIN esc_users u ON u.id = e.user_id
				WHERE u.user_type = 1 AND u.application_id = ' . $APP_ID . '
			)
		');
		
		/*$default_city_id = $db->query('
			UPDATE escortforum_v2.escorts v2e
			LEFT JOIN esc_escort_cities ec ON ec.escort_id = v2e.id
			SET v2e.city_id = ec.city_id
			WHERE ec.is_default = 1
				')->fetch();*/
		
		echo 'Clearing `escort_langs` table' . "\r\n";
		$db2->query('TRUNCATE `escort_langs`');
		
		$db->query('
			INSERT INTO escortforum_v2.escort_langs (escort_id, level, lang_id)
			(
				SELECT el.escort_id, el.level, el.lng_iso 
				FROM esc_escort_languages el
				INNER JOIN esc_escorts e ON e.id = el.escort_id
				INNER JOIN esc_users u ON u.id = e.user_id
				WHERE u.user_type = 1 AND u.application_id = ' . $APP_ID . '
			)
		');
		
		echo 'Clearing `escort_rates` table' . "\r\n";
		$db2->query('TRUNCATE `escort_rates`');
		
		$db->query('
			INSERT INTO escortforum_v2.escort_rates (escort_id, availibility, time, time_unit, price, currency_id)
			(
				SELECT er.escort_id, er.available_for, er.time, er.time_unit, er.price, er.currency_id
				FROM esc_escort_rates er
				INNER JOIN esc_escorts e ON e.id = er.escort_id
				INNER JOIN esc_users u ON u.id = e.user_id
				WHERE u.user_type = 1 AND u.application_id = ' . $APP_ID . '
			)
		');
		
		echo 'Clearing `escort_working_times` table' . "\r\n";
		$db2->query('TRUNCATE `escort_working_times`');
		
		$db->query('
			INSERT INTO escortforum_v2.escort_working_times (escort_id, day_index, time_from, time_to)
			(
				SELECT ew.escort_id, ew.day, ew.time_from, ew.time_to
				FROM esc_escort_working_times ew
				INNER JOIN esc_escorts e ON e.id = ew.escort_id
				INNER JOIN esc_users u ON u.id = e.user_id
				WHERE u.user_type = 1 AND u.application_id = ' . $APP_ID . '
			)
		');
		
		echo 'Porting agencies' . "\r\n";
		
		echo 'Porting members' . "\r\n";
		
		die;
	}
	
	public function nationalititesAction()
	{
		$db = $this->_db2;
		
		require('../../library/Cubix/nationalities_it.php');
		
		foreach ( $_NATS as $iso => $title ) {
			$id = $this->_saveNat($iso, $title);
			
			/*$db->query('
				UPDATE escort_profiles SET nationality = ? WHERE nationality = ?
			', array(
				$id,
				$iso
			));*/
		}
		
		//$db->query('UPDATE escort_profiles SET nationality = 0 WHERE nationality IN ("", "_w") OR nationality IS NULL');
		
		die;
	}
	
	protected function _saveNat($iso, $title)
	{
		$db = $this->_db2;
		
		$id = $db->fetchOne('
			SELECT id FROM nationalities WHERE iso = ?
		', $iso);
		
		if ( ! $id ) {
			$db->insert('nationalities', array(
				'iso' => $iso,
				'slug' => Cubix_Utils::makeSlug($title),
				'title_de' => $title
			));
			
			$id = $db->lastInsertId();
		}
		else {
			$db->update('nationalities', array(
				'title_it' => $title
			), $db->quoteInto('id = ?', array($id)));
		}
		
		return $id;
	}
	
	public function geographyAction()
	{
		$db = $this->_db;
		$db2 = $this->_db2;
		
		
		/* COUNTRIES */
		
		/*$db2->query('TRUNCATE countries');
		
		require '../application/includes/countries.php';
		
		foreach ( $COUNTRIES as $iso ) {
			$db2->insert('countries', array(
				'iso' => $iso,
				'slug' => Cubix_Utils::makeSlug($LNG['country_' . $iso]),
				'title_en' => $LNG['country_' . $iso]
			));
		}*/
		
		
		/* REGIONS */
		
		/*$db2->query('TRUNCATE regions');
		
		// $db2->query('ALTER TABLE regions ADD internal_id INT(10) UNSIGNED DEFAULT NULL');
		
		$regions = $db->query('
			SELECT id, name, country_iso, short FROM esc_regions
			ORDER BY country_iso ASC
		')->fetchAll();
		
		require '../application/includes/regions.php';
		
		foreach ( $regions as $region ) {
			$country_id = $db2->fetchOne('
				SELECT id FROM countries WHERE iso = ?
			', $region->country_iso);
			
			$data = array(
				'internal_id' => $region->id
			);
			
			if ( ! is_null($region->short) ) {
				$data['title_en'] = isset($USA_STATES[$region->short]) ?
					$USA_STATES[$region->short] : $region->name;
				$data['state'] = $region->short;
			}
			else {
				$data['title_en'] = isset($LNG[$region->name]) ? 
					$LNG[$region->name] : $region->name;
			}
			
			$data['slug'] = Cubix_Utils::makeSlug($data['title_en']);
			
			if ( $country_id ) {
				$data['country_id'] = $country_id;
			}
			
			$db2->insert('regions', $data);
		}*/
		
		
		/* CITIES */

		/*$cities = $db2->query('SELECT * FROM cities')->fetchAll();
		print_r($cities);die;*/

		//$db2->query('DELETE FROM cities WHERE country_id = 216');
		
		//$db2->query('ALTER TABLE cities ADD internal_id INT(10) UNSIGNED DEFAULT NULL');
		
		$cities = $db->query('
			SELECT ec.id, ec.name, ec.country_iso, ec.state, ec.region_id FROM esc_cities ec
			WHERE country_iso = \'us\'
			ORDER BY country_iso
		')->fetchAll();
		
		require '../application/includes/cities.php';
		
		foreach ( $cities as $city ) {
			$data = array();
			
			$data['title_en'] = isset($LNG[$city['name']]) ? $LNG[$city['name']] : $city['name'];
			$data['slug'] = Cubix_Utils::makeSlug($data['title_en']);
			$data['internal_id'] = $city['id'];
			
			$country_id = $db2->fetchOne('
				SELECT id FROM countries WHERE iso = ?
			', $city['country_iso']);
			
			if ( $city->region_id ) {
				$region_id = $db2->fetchOne('
					SELECT id FROM regions WHERE internal_id = ?
				', $city['region_id']);
				
				if ( $region_id ) {
				}
				else {
					$region_id = $db2->fetchOne('
						SELECT id FROM regions 
						WHERE country_id = ?
					', $country_id);
				}
			}
			
			$data['country_id'] = $country_id;
			$data['region_id'] = $region_id;
			
			$db2->insert('cities', $data);
		}
		
		// $db2->query('ALTER TABLE cities DROP internal_id');
		// $db2->query('ALTER TABLE regions DROP internal_id');
		
		die;
	}

	public function geography2Action()
	{
		$db = $this->_db;
		$db2 = $this->_db2;

		/* CITIES */

		$cities = $db->query('
			SELECT ec.id, ec.name, ec.country_iso, ec.state, ec.region_id FROM esc_cities ec
			WHERE country_iso = \'fr\'
			ORDER BY country_iso
		')->fetchAll();

		$lng_id = 'en';

		require '../application/includes/cities_' . $lng_id . '.php';

		foreach ( $cities as $city ) {
			//print_r($city);
			$data = array();

			$data['title_' . $lng_id] = isset($LNG[$city['name']]) ? $LNG[$city['name']] : $city['name'];
			$data['slug'] = Cubix_Utils::makeSlug($data['title_' . $lng_id]);
			$data['internal_id'] = $city['id'];

			$country_id = 71;

			if ( $city->region_id ) {
				$region_id = $db2->fetchOne('
					SELECT id FROM regions WHERE internal_id = ?
				', $city['region_id']);

				if ( $region_id ) {
				}
				else {
					$region_id = $db2->fetchOne('
						SELECT id FROM regions
						WHERE country_id = ?
					', $country_id);
				}
			}

			$data['country_id'] = $country_id;
			$data['region_id'] = $region_id;

			//$db2->insert('cities', $data);
		}

		die;
	}

	public function fixStatesAction()
	{
		$pDB = $this->_db;
		$sDB = $this->_db2;

		$old_cities = $pDB->query("SELECT id, region_id, is_important FROM esc_cities WHERE country_iso = 'us'")->fetchAll();

		foreach ( $old_cities as $old_city ) {
			$new_region_id = $sDB->fetchOne('SELECT id FROM regions WHERE internal_id = ? AND country_id = 216', array($old_city['region_id']));

			echo "Old City ID : " . $old_city['id'] . " <br/>";
			$sDB->query('UPDATE cities SET region_id = ?, is_important = ? WHERE internal_id = ? AND country_id = 216', array($new_region_id, $old_city['is_important'], $old_city['id']));
		}

		die;
	}
	
	public function dictionaryAction()
	{
		/*Cubix_I18n::init();
		die;*/
		
		$lang_id = $this->_getParam('lang_id');
		$lang_file = '../application/includes/' . $lang_id . '_converted.php';
		
		if ( ! is_file($lang_file) ) {
			die('Lang file "' . $lang_file . '" does not exist');
		}
		
		$LNG = array();
		require($lang_file);
		
		$errors = array();
		
		foreach ( $LNG as $id => $value ) {
			try {
				$this->_transparentUpdate('dictionary', array('id' => $id, 'value_' . $lang_id => $value), $this->_db2->quoteInto('id = ?', $id));
			}
			catch ( Exception  $e ) {
				$errors[] = $e->getMessage() . '(' . $id . ')';
			}
		}
		
		echo implode("<br/>\r\n", $errors);
		
		die;
	}
	
	
	public function dictionary2Action()
	{		
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		
		$sDB = $this->_db2;
		
		$dic_it = $sDB->fetchAll('SELECT * FROM dic_it');
		
		$sDB->query('SET NAMES `utf8`');
		foreach ( $dic_it as $it ) {
			$sDB->update('dictionary', array('value_it' =>  $it->value_it), $sDB->quoteInto('id = ?', $it->id));
		}
		
		/*$sDB->query('SET NAMES `utf8`');
		foreach ( $rows as $row ) {
			if ( strlen($row[1]) ) {
				$sDB->update('dictionary', array('value_it' =>  $row[1]), $sDB->quoteInto('id = ?', $row[0]));
			}
		}*/
		
		die;
	}
	
	protected function _transparentUpdate($table, $data, $where)
	{
		$exists = $this->_db2->fetchOne('SELECT TRUE FROM dictionary WHERE id = ?', $data['id']);
		
		if ( ! $exists ) {
			$this->_db2->insert($table, $data);
		}
		else {
			$this->_db2->update($table, $data, $where);
		}
	}


	

	public function fixMainPhotosAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');

		$application_id = 9;

		$db = $this->_db2;

		$sql = "
			SELECT eps.data
			FROM escort_profiles_snapshots_v2 eps
			INNER JOIN escorts e ON e.id = eps.escort_id
			INNER JOIN users u ON u.id = e.user_id
			WHERE u.application_id = ?
		";

		$data = $db->query($sql, $application_id)->fetchAll();

		if ( count($data) ) {
			foreach( $data as $d ) {
				$item = unserialize($d->data);
				
				/*if ( $item->id == '162707' ) {
					print_r($item);die;
				}*/

				$sql = "
					SELECT ep.hash, ep.ext, ep.status
					FROM escort_photos ep
					WHERE escort_id = ? AND is_main = 1
				";

				$photo = $db->query($sql, $item->id)->fetch();

				if ( ! strlen($item->photo_hash) ) {

					$item->photo_hash = $photo->hash;
					$item->photo_ext = $photo->ext;
					$item->photo_status = $photo->status;
					//print_r($item);
					if ( isset($item->id) && $item->id ) {
						$db->update('escort_profiles_snapshots_v2', array('data' => serialize($item)), $db->quoteInto('escort_id = ?', $item->id));

						echo $item->id . " escort main photo fixed <br/>\n";
					}
				}
			}
		}

		die;
	}	

	public function fixDateAction()
	{
		set_time_limit(0);
		
		$db = $this->_db2;

		$sql = "
			SELECT escort_id, birth_date FROM  escort_profiles_v2 WHERE birth_date = '1970-01-01'
		";

		$data = $db->query($sql)->fetchAll();
		echo("Escorts count: " . count($data));
		foreach ( $data as $d ) {
			$escort_data = $db->query('SELECT escort_id, data FROM profile_updates_v2 WHERE escort_id = ?', $d->escort_id)->fetch();
			
			$escort_data = unserialize($escort_data->data);
			$escort_data['birth_date'] = null;
			
			$db->update('escort_profiles_v2', array('birth_date' => null), $db->quoteInto('escort_id = ?', $d->escort_id));
			$db->update('profile_updates_v2', array('data' => serialize($escort_data)), $db->quoteInto('escort_id = ?', $d->escort_id));
			
		}
		die;
	}

	public function fixSnapshotDateAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;

		$sql = "
			SELECT eps.escort_id, eps.data FROM escort_profiles_snapshots_v2 eps INNER JOIN escorts e ON e.id = eps.escort_id INNER JOIN users u ON u.id = e.user_id WHERE u.application_id = ?
		";

		$data = $db->query($sql, array(7))->fetchAll();
		echo(count($data));
		foreach ( $data as $d ) {
			$escort_data = unserialize($d->data);
			
			if ( $escort_data->age != 40 && is_numeric($escort_data->age) ) {
				$escort_data->birth_date = strtotime('- ' . $escort_data->age . ' years');
			}
			else {
				$escort_data->birth_date = null;
			}
			unset($escort_data->age);
			
			$db->update('escort_profiles_snapshots_v2', array('data' => serialize($escort_data)), $db->quoteInto('escort_id = ?', $d->escort_id));

		}
		die;
	}

	public function fixSnapshotAnd6NewLangAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '2048M');
		$db = $this->_db2;

		$sql = "
			SELECT eps.escort_id, eps.data FROM escort_profiles_snapshots_v2 eps INNER JOIN escorts e ON e.id = eps.escort_id INNER JOIN users u ON u.id = e.user_id WHERE u.application_id = ?
			ORDER BY e.id ASC
		";

		
		$data = $db->query($sql, array(16))->fetchAll();

		foreach ( $data as $d ) {
			$escort_data = unserialize($d->data);			
					
			$escort_data->city_title_es = $escort_data->city_title_es ? $escort_data->city_title_es : $scort_data->city_title_en;//Zürich City
			$escort_data->base_city_es = $escort_data->base_city_es ? $escort_data->base_city_es : $escort_data->base_city_en;//Zürich City
			$escort_data->region_title_es = $escort_data->region_title_es ? $escort_data->region_title_es : $escort_data->region_title_en;//Deutschschweiz
			$escort_data->nationality_title_es = $escort_data->nationality_title_es ? $escort_data->nationality_title_es : $escort_data->nationality_title_en;
			$escort_data->about_es = $escort_data->about_es ? $escort_data->about_es : "";
			$escort_data->additional_service_es = $escort_data->additional_service_es ? $escort_data->additional_service_es : "";

			$escort_data->city_title_hu = $escort_data->city_title_hu ? $escort_data->city_title_hu : $scort_data->city_title_en;//Zürich City
			$escort_data->base_city_hu = $escort_data->base_city_hu ? $escort_data->base_city_hu : $escort_data->base_city_en;//Zürich City
			$escort_data->region_title_hu = $escort_data->region_title_hu ? $escort_data->region_title_hu : $escort_data->region_title_en;//Deutschschweiz
			$escort_data->nationality_title_hu = $escort_data->nationality_title_hu ? $escort_data->nationality_title_hu : $escort_data->nationality_title_en;
			$escort_data->about_hu = $escort_data->about_hu ? $escort_data->about_hu : "";
			$escort_data->additional_service_hu = $escort_data->additional_service_hu ? $escort_data->additional_service_hu : "";

			/*$city_row = $db->fetchRow('SELECT title_ro FROM cities WHERE id = ?', array($escort_data->base_city_id));
			$escort_data->base_city_ro = $city_row->title_ro;
			//$escort_data->city_title_ro = $city_row->title_ro;*/
			
			$db->update('escort_profiles_snapshots_v2', array('data' => serialize($escort_data)), $db->quoteInto('escort_id = ?', $d->escort_id));

			echo $d->escort_id . "<br/>";

		}

		die('done');
	}

	public function fixRevisionDateAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;

		$sql = "
			SELECT pu.id, pu.data FROM profile_updates_v2 pu INNER JOIN escorts e ON e.id = pu.escort_id INNER JOIN users u ON u.id = e.user_id WHERE u.application_id = ?
		";

		$data = $db->query($sql, array(7))->fetchAll();
		echo(count($data));
		foreach ( $data as $d ) {
			$escort_data = unserialize($d->data);

			if ( $escort_data['birth_date'] == '0000-00-00' ) {
				$escort_data['birth_date'] = null;
			}
			else {
				$escort_data['birth_date'] = strtotime($escort_data['birth_date']);
			}
			

			$db->update('profile_updates_v2', array('data' => serialize($escort_data)), $db->quoteInto('id = ?', $d->id));
			//echo "Fix date for escort revision " . $d->id . "/r/n";
		}
		die;
	}

	public function fixAdminVerifiedAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;

		$sql = "SELECT escort_id, admin_verified FROM escort_profiles";

		$data = $db->query($sql)->fetchAll();


		foreach ( $data as $d ) {

			if ( $db->fetchOne('SELECT TRUE FROM escort_profiles_v2 WHERE escort_id = ?', array($d->escort_id)) ) {
				$db->update('escort_profiles_v2', array('admin_verified' => $d->admin_verified), $db->quoteInto('escort_id = ?', $d->escort_id));
			}
		}
		echo(count($data));
		die;
	}

	public function fixWorkingTimesAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;

		$sql = "SELECT escort_id, time_from, time_from_m, time_to, time_to_m FROM escort_working_times";

		$data = $db->query($sql)->fetchAll();

		foreach ( $data as $it ) {
			if ( ! strlen($it->time_from_m) || is_null($it->time_from_m) ) {
				$db->update('escort_working_times', array('time_from_m' => 0), $db->quoteInto('escort_id = ?', $it->escort_id));
			}

			if ( ! strlen($it->time_to_m) || is_null($it->time_to_m) ) {
				$db->update('escort_working_times', array('time_to_m' => 0), $db->quoteInto('escort_id = ?', $it->escort_id));
			}

			if ( $it->time_from == 24 ) {
				$db->update('escort_working_times', array('time_from' => 0, 'time_from_m' => 0), $db->quoteInto('escort_id = ?', $it->escort_id));
			}

			if ( $it->time_to == 24 ) {
				$db->update('escort_working_times', array('time_to' => 23, 'time_to_m' => 59), $db->quoteInto('escort_id = ?', $it->escort_id));
			}
		}
	}

	public function fixProfileUpdatesAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;
		//try {
		//$m = new Model_Escort_ProfileV2(array('id' => 155363/*2340*/));
		/*$max_rev = $m->getLatestRevision(Model_Escort_ProfileV2::REVISION_STATUS_APPROVED);
		$rev = $m->getRevision($max_rev);


		$sql = "SELECT day_index, time_from, time_from_m, time_to, time_to_m FROM escort_working_times WHERE escort_id = ?";
		$wt = $db->query($sql, array(155363))->fetchAll(Zend_Db::FETCH_ASSOC);

		$rev->data['times'] = $wt;

		$db->update('profile_updates_v2', array('data' => serialize($rev->data)), array( $db->quoteInto('escort_id', 155363), $db->quoteInto('revision', $max_rev) ) );
		}
		catch (Exception $e) {
			throw $e;
		}
		unset($m);
		die;*/
		

		$sql = "SELECT id FROM escorts";
		$escorts = $db->query($sql)->fetchAll();

		foreach ( $escorts as $escort ) {
			$data = null;
			
			$m = new Model_Escort_ProfileV2(array('id' => $escort->id));
			$max_rev = $m->getLatestRevision(Model_Escort_ProfileV2::REVISION_STATUS_APPROVED);
			$rev = $m->getRevision($max_rev);


			$sql = "SELECT day_index, time_from, time_from_m, time_to, time_to_m FROM escort_working_times WHERE escort_id = ?";
			$wt = $db->query($sql, array($escort->id))->fetchAll(Zend_Db::FETCH_ASSOC);

			$data = $rev->data['times'] = $wt;

			$db->update('profile_updates_v2', array('data' => serialize($data)), array( $db->quoteInto('escort_id', $escort->id), $db->quoteInto('revision', $max_rev) ) );
			unset($m);
		}
	}
	
	public function fixBaseCitiesAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;

		$sql = "
			select
				e.id, e.city_id
			from escorts e
			where not exists(select 1 from escort_cities ec where ec.escort_id = e.id and ec.city_id = e.city_id)
			and e.status & 256 = 0 and showname is not null AND e.city_id IS NOT NULL
		";

		$escorts = $db->query($sql)->fetchAll();

		foreach ( $escorts as $escort ) {
			
			$db->insert('escort_cities', array('escort_id' => $escort->id, 'city_id' => $escort->city_id));
			
			$m_profile = new Model_Escort_ProfileV2(array('id' => $escort->id));
			$latest_revision = $m_profile->getLatestRevision(Model_Escort_ProfileV2::REVISION_STATUS_APPROVED);
			
			$profile_revision = $m_profile->getRevision($latest_revision);
			
			$cities = $db->fetchAll('SELECT escort_id, city_id FROM escort_cities WHERE escort_id = ?', $escort->id, Zend_db::FETCH_ASSOC);
			
			
			$profile_revision->data['cities'] = $cities;
			
			
			$db->update('profile_updates_v2', array('data' => serialize($profile_revision->data)), array($db->quoteInto('escort_id = ?', $escort->id), $db->quoteInto('revision = ?', $latest_revision)));
			
			
		}
		die;
	}






	/*------------------------------------------------------------------*/

	private function _getRealProfileOld()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');

		$application_id = MAPP_ID;

		$db = $this->_db2;

		$sql = "
			SELECT ep.*, e.id, e.showname, e.gender, e.country_id, e.city_id, e.home_city_id FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN escort_profiles ep ON ep.escort_id = e.id WHERE u.application_id = ?
		";

		$data = $db->query($sql, $application_id)->fetchAll();

		$c = 0;
		$data_count = count($data);
		foreach ( $data as $d ) {

			if ( $db->fetchOne('SELECT TRUE FROM profile_updates_v2 WHERE escort_id = ?', $d->id) )	continue;

			$profile = array(
				'escort_id' => $d->escort_id,
				'showname' => $d->showname,
				'country_id' => $d->country_id,
				'city_id' => $d->city_id,
				'home_city_id' => $d->home_city_id,
				'gender' => $d->gender,
				'birth_date' => $d->birth_date,
				'gender' => $d->gender,
				'ethnicity' => ( intval($d->ethnicity) ) ? intval($d->ethnicity) : null,
				'nationality_id' => $d->nationality_id,
				'measure_units' => $d->measure_units,
				'hair_color' => ($d->hair_color) ? $d->hair_color : null,
				'eye_color' => ($d->eye_color) ? $d->eye_color: null,
				'height' => ($d->height) ? $d->height : null,
				'weight' => ($d->weight) ? $d->weight : null,
				'dress_size' => ($d->dress_size) ? $d->dress_size : null,
				'shoe_size' => ($d->shoe_size) ? $d->shoe_size : null,
				'bust' => ($d->bust) ? $d->bust : null,
				'waist' => ($d->waist) ? $d->waist : null,
				'hip' => ($d->hip) ? $d->hip : null,
				'cup_size' => ($d->breast_size) ? $d->breast_size : null,
				'pubic_hair' => null,
				'about_en' => $d->about_en,
				'about_it' => $d->about_it,
				'about_gr' => $d->about_gr,
				'about_fr' => $d->about_fr,
				'about_de' => $d->about_de,
				'is_smoking' => ($d->is_smoker) ? $d->is_smoker : null,
				'is_drinking' => null,
				'characteristics' => $d->characteristics,
				'zip' => $d->contact_zip,


				'incall_hotel_room' => null,
				'incall_other' => null,
				'outcall_other' => null,

				'sex_orientation' => null,
				'sex_availability' => $d->sex_availability,
				'additional_service_en' => $d->svc_additional,
				'additional_service_de' => $d->svc_additional,
				'additional_service_gr' => $d->svc_additional,
				'additional_service_fr' => $d->svc_additional,
				'additional_service_it' => $d->svc_additional,

				'available_24_7' => null,
				'phone_number' => $d->contact_phone,
				'contact_phone_parsed' => $this->parsePhone($d->contact_phone),
				'phone_number_alt' => null,
				'phone_instr' => null,
				'phone_instr_no_withheld' => null,
				'phone_instr_other' => $d->phone_instructions,

				'email' => $d->contact_email,
				'website' => $d->contact_web,
				'club_name' => null,
				'street' => null,
				'street_no' => null
			);


			$d->availability = intval($d->availability);
			switch ( $d->availability ) {
				case AVAILABLE_INCALL:
					$profile['incall_type'] = 4;
					break;
				case AVAILABLE_OUTCALL:
					$profile['outcall_type'] = 4;
					break;
				case AVAILABLE_INCALL_OUTCALL:
					$profile['incall_type'] = 4;
					$profile['outcall_type'] = 4;
					break;
			}

			$profile['langs'] = $db->fetchAll('SELECT lang_id, level FROM escort_langs WHERE escort_id = ?', $d->id, Zend_Db::FETCH_ASSOC);
			$profile['cities'] = $db->fetchAll('SELECT city_id FROM escort_cities WHERE escort_id = ?', $d->id, Zend_Db::FETCH_ASSOC);
			$profile['services'] = $db->fetchAll('SELECT service_id, price FROM escort_services WHERE escort_id = ?', $d->id, Zend_Db::FETCH_ASSOC);
			$profile['times'] = $db->fetchAll('
					SELECT
						day_index, time_from, time_from_m, time_to, time_to_m
					FROM
						escort_working_times
					WHERE escort_id = ?', $d->id, Zend_Db::FETCH_ASSOC
			);

			if ( count($profile['times']) > 0 ) {
				foreach( $profile['times'] as $i => $times ) {
					if ( ! strlen($times->time_from_m) ) {
						$profile['times'][$i]['time_from_m'] = "0";
					}

					if ( ! strlen($times->time_to_m) ) {
						$profile['times'][$i]['time_to_m'] = "0";
					}
				}
			}

			$profile['rates'] = $db->fetchAll('SELECT availability, type, time, time_unit, price, currency_id FROM escort_rates WHERE escort_id = ?', $d->id, Zend_Db::FETCH_ASSOC);

			try {

				$db_tmp = $profile['birth_date'];

				$profile['birth_date'] = strtotime($db_tmp);

				$db->insert('profile_updates_v2', array(
					'escort_id' => $d->id,
					'revision' => 1,
					'date_updated' => new Zend_Db_Expr("NOW()"),
					'data' => serialize($profile),
					'status' => Model_Escort_ProfileV2::REVISION_STATUS_NOT_APPROVED
				));

				$profile['birth_date'] = $db_tmp;

				$m_profile = new Model_Escort_ProfileV2(array('id' => $d->id));
				$m_profile->saveRevision();

				$c++;
				echo "[{$c}/{$data_count}] Escort #" . $profile['escort_id'] . ", Showname: \"" . $profile['showname'] . "\" -- migrated to V2\r\n";
				ob_flush();

				unset($m_profile);
				unset($profile);

			}
			catch ( Exception $e ) {
				throw $e;
				echo "ERROR: Escort ID " . $d->escort_id . "<br/>";
			}


		}


	}

	public function toV2Action()
	{
		set_time_limit(0);

		$this->_getRealProfileOld();

		die;
	}	

	public function fixServicesAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;

		$sql = "
			SELECT
				ep.escort_id AS id, ep.svc_kissing, ep.svc_blowjob, ep.svc_cumshot, ep.svc_69, ep.svc_anal, ep.svc_additional
			FROM escort_profiles ep
			INNER JOIN escorts e ON ep.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id
			WHERE u.application_id = " . MAPP_ID;
		$escorts = $db->query($sql)->fetchAll();

		foreach ( $escorts as $escort ) {

			if ( $escort->svc_kissing == KISSING ) {
				$db->insert('escort_services', array('escort_id' => $escort->id, 'service_id' => 8));
			}

			if ( $escort->svc_kissing == KISSING_WITH_TONGUE ) {
				$db->insert('escort_services', array('escort_id' => $escort->id, 'service_id' => 6));
			}

			if ( $escort->svc_blowjob == BLOWJOB_WITH_CONDOM ) {
				$db->insert('escort_services', array('escort_id' => $escort->id, 'service_id' => 9));
			}

			if ( $escort->svc_blowjob == BLOWJOB_WITHOUT_CONDOM ) {
				$db->insert('escort_services', array('escort_id' => $escort->id, 'service_id' => 10));
			}

			if ( $escort->svc_cumshot == CUMSHOT_IN_FACE ) {
				$db->insert('escort_services', array('escort_id' => $escort->id, 'service_id' => 4));
			}

			if ( $escort->svc_cumshot == CUMSHOT_IN_MOUTH_SPIT || $escort->svc_cumshot == CUMSHOT_IN_MOUTH_SWALLOW ) {
				$db->insert('escort_services', array('escort_id' => $escort->id, 'service_id' => 3));
			}

			if ( $escort->svc_69 == 1 ) {
				$db->insert('escort_services', array('escort_id' => $escort->id, 'service_id' => 1));
			}

			if ( $escort->svc_anal == 1 ) {
				$db->insert('escort_services', array('escort_id' => $escort->id, 'service_id' => 2));
			}

			if ( strlen($escort->svc_additional) ) {
				$db->update('escort_profiles_v2', array('additional_service_en' => $escort->svc_additional, 'additional_service_fr' => $escort->svc_additional), $db->quoteInto('escort_id = ?', $escort->id));
			}

		}
		die;
	}

	// Fix All packages Expiration date ( using package period )
	public function fixPackagesExpirationAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;

		$sql = "
			SELECT
				op.id, op.period, UNIX_TIMESTAMP(op.date_activated) AS date_activated
			FROM order_packages op
			WHERE op.application_id = ? AND op.date_activated IS NOT NULL AND op.period IS NOT NULL AND op.period <> 0
		";

		$packages = $db->query($sql, MAPP_ID)->fetchAll();

		foreach ( $packages as $package ) {
			$db->update('order_packages', array('expiration_date' => date("Y-m-d", strtotime('+ ' . $package->period . ' days', $package->date_activated))), $db->quoteInto('id = ?', $package->id));
		}
		die;
	}

	// Set package status top expired, if order_packages date_activated field is null
	public function fixPackageStatusesAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;

		$sql = "
			SELECT
				op.id
			FROM order_packages op
			WHERE op.application_id = ? AND op.date_activated IS NULL AND op.status = 2
		";

		$packages = $db->query($sql, MAPP_ID)->fetchAll();

		foreach ( $packages as $package ) {
			$db->update('order_packages', array('status' => 3), $db->quoteInto('id = ?', $package->id));
		}
		die;
	}

	// Set premium_escorts table city_id
	/*public function fixNewPremiumCityIdsAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;

		$sql = "
			select pe.order_package_id, c.id AS city_id from premium_escorts pe
			inner join order_packages ob on ob.id = pe.order_package_id
			inner join cities c on c.internal_id = pe.city_id
			WHERE ob.application_id = ?
		";

		$packages = $db->query($sql, MAPP_ID)->fetchAll();

		foreach ( $packages as $package ) {
			try {
				$db->update('premium_escorts', array('city_id' => $package->city_id), $db->quoteInto('order_package_id = ?', $package->order_package_id));
			}
			catch ( Exception $e ) {
				
			}
		}
		die;
	}*/

	// Fill Current Active Package information in "escorts" table
	public function fixPackagesAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;

		$sql = "
			SELECT
				op.escort_id, p.name, op.date_activated, op.expiration_date,
				op.package_id
			FROM order_packages op
			INNER JOIN packages p ON p.id = op.package_id
			WHERE op.status = 2 AND op.application_id = ?
			GROUP BY op.escort_id
		";

		$packages = $db->query($sql, MAPP_ID)->fetchAll();

		foreach ( $packages as $package ) {
			$data = array(
				'active_package_id' => $package->package_id,
				'active_package_name' => $package->name,
				'package_activation_date' => $package->date_activated,
				'package_expiration_date' => $package->expiration_date
			);

			$db->update('escorts', $data, $db->quoteInto('id = ?', $package->escort_id));
		}
		die;
	}

	private function parsePhone($phone)
	{
		if ( ! preg_match('/^\+/', $phone) &&  substr($phone, 0, 2) != '00' ) {
			$phone = '+' . $phone;
		}

		$phone = preg_replace('/^\+/', '00', $phone);
		$phone = preg_replace('/[^0-9]/', '', $phone);

		if ( $phone == '00' ) {
			return null;
		}

		return $phone;
	}

	private function clear_string($str)
	{
		$str = strtolower($str);

		$str = preg_replace("#[^0-9a-z]#", '-', $str);

		$str = preg_replace("#\s+#", '-', $str);

		$str = ___clean_str($str);

		return $str;
	}
	
	
	public function fixZonesAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;

		$sql = "
			SELECT
				id, city_id, name
			FROM __zones z
		";

		$old_zones = $db->fetchAll($sql);

		foreach ( $old_zones as $old_zone ) {
			
			$city_id = $db->fetchOne('SELECT id FROM cities WHERE internal_id = ?', $old_zone->city_id);
			$zone_slug = substr($old_zone->name, 8, strlen($old_zone->name));
			echo $zone_slug . "<br/>";
			$zone_slug = str_replace('_', '-', $zone_slug);
						
			$db->update('cityzones', array('city_id' => $city_id), $db->quoteInto('slug = ?', $zone_slug));
		}
		die;
	}
	
	public function fixGplAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;

		$sql = "
			SELECT
				id
			FROM order_packages WHERE package_id = 82 AND status = 2;
		";

		$gpl = $db->fetchAll($sql);

		foreach ( $gpl as $g ) {
			$arr = array(
				'order_package_id' => $g->id,
				'product_id' => 16,
				'is_optional' => 0,
				'price' => 0
			);
			try {
				$db->insert('order_package_products', $arr);
			}
			catch ( Exception $e ) {
				
			}
		}
		die;
	}
	
	public function fixDicAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;
		
		$lng = 'cn';

		$sql = "
			SELECT
				id, value_en
			FROM dictionary WHERE value_" . $lng . " IS NULL OR LENGTH(value_" . $lng . ") = 0;
		";

		$dic = $db->fetchAll($sql);

		foreach ( $dic as $d ) {
			try {
				
				$db->update('dictionary', array('value_' . $lng => $d->value_en), $db->quoteInto('id = ?', $d->id));
			}
			catch ( Exception $e ) {
				var_dump($e->getMessage());
			}
		}
		die('Done :)');
	}
	
	public function addTranslationsAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		
		$db = $this->_db2;	
		$xlsx = new Cubix_SimpleXLSX('/var/www/library/Cubix/cities.xlsx');
		$getWorksheetName = $xlsx->getWorksheetName();
		
		$rows = $xlsx->rowsEx(1);
		$db->query('SET NAMES `utf8`');
		foreach ( $rows as $row ) {
			if( strlen(trim($row[0]['value'])) && strlen($row[1]['value']) ) {

				$v = mysqli_escape_string(trim($row[1]['value']));
				$id = trim($row[0]['value']);
				$db->query("UPDATE cities SET title_ru = '{$v}' WHERE id = '" . $id . "'");
				//var_dump($row[0]['value'], $row[2]['value']);
			}
			else
			{
				var_dump($row[0]['value'], $row[2]['value']);
			}
		}		
		
		die('Done :)');
	}

	public function updateDictionaryAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		
		$db = $this->_db2;	
		$xlsx = new Cubix_SimpleXLSX('/var/www/library/dictionary_es.xlsx'); //TODO ONYX: remove /escorts
		$db->query('SET NAMES `utf8`');
		$rows = $xlsx->rowsEx();
		foreach ( $rows as $row ) {
			if( strlen(trim($row[0]['value'])) && strlen($row[3]['value']) ) {
				$v = trim($row[3]['value']);
				$id = trim($row[0]['value']);
				$db->query("UPDATE dictionary SET value_es = ? WHERE id = ?", array($v, $id));
			}
			else
			{
				var_dump($row[0]['value'], $row[3]['value']);
			}
		}		
		
		die('Done :)');
	}

	public function updateNationalitiesAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		
		$db = $this->_db2;	
		$xlsx = new Cubix_SimpleXLSX('/var/www/library/nat.xlsx');
		// $xlsx = new Cubix_SimpleXLSX('/var/www/escorts/library/nat.xlsx');
		$db->query('SET NAMES `utf8`');
		$rows = $xlsx->rowsEx();


		foreach ( $rows as $row ) {
			if( strlen(trim($row[1]['value'])) && strlen($row[2]['value']) && strlen($row[3]['value']) ) {
				$v = trim($row[2]['value']);
				$h = trim($row[3]['value']);
				$id = trim($row[1]['value']);

				$db->query("UPDATE nationalities SET title_es = ?, title_hu = ? WHERE title_pt = ?", array($v, $h, $id));
			}
			else
			{
				var_dump($row[1]['value'], $row[2]['value'], $row[3]['value']);
			}
		}		
		
		die('Done :)');
	}

	public function addCountriesAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');

		$db = $this->_db2;
		$xlsx = new Cubix_SimpleXLSX('/var/www/library/Cubix/countriesPT.xlsx');
		$getWorksheetName = $xlsx->getWorksheetName();

		$rows = $xlsx->rowsEx(1);
		$db->query('SET NAMES `utf8`');
		foreach ( $rows as $row ) {
			if( strlen(trim($row[0]['value'])) && strlen($row[3]['value']) ) {

				$v = mysql_escape_string(trim($row[3]['value']));
				$id = trim($row[0]['value']);
				$db->query("UPDATE countries SET title_pt = '{$v}' WHERE id = '" . $id . "'");
				var_dump($row[0]['value'], $row[3]['value']);
			}
			else
			{
				var_dump($row[0]['value'], $row[3]['value']);
			}
		}

		die('Done :)');
	}
	
	public function fixPhonesParsedAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		
		$db = $this->_db2;	
		
		$sql = "SELECT escort_id, phone_country_id, contact_phone_parsed FROM escort_profiles_v2 WHERE phone_number IS NOT NULL AND phone_number <> '' AND dd = 0";
		
		$phones = $db->fetchAll($sql);
		
		$sqlCC = "SELECT id, phone_prefix, ndd_prefix FROM countries_phone_code ORDER BY phone_prefix DESC";
		$country_codes = $db->fetchAll($sqlCC);
		
		foreach ( $phones as $phone ) {
			if ( strlen($phone->contact_phone_parsed) > 3 ) {
				if ( strpos($phone->contact_phone_parsed, '00') !== false ) {
					$phone_out_00 = substr($phone->contact_phone_parsed, 2);

					foreach ( $country_codes as $cc ) {
						//var_dump(preg_match("#" . $cc->phone_prefix . "#", $phone_out_00));
						if ( preg_match("#^" . $cc->phone_prefix . "#", $phone_out_00) ) {
							$phone_out_ndd = $phone_out_country_code = substr($phone_out_00, strlen($cc->phone_prefix));
							
							
							if ( preg_match("#^" . $cc->ndd_prefix . "#", $phone_out_country_code) ) {
								$phone_out_ndd = substr($phone_out_country_code, strlen($cc->ndd_prefix));
							}							
							
							$max_revision = $db->fetchOne('SELECT MAX(revision) FROM profile_updates_v2 WHERE escort_id = ?', $phone->escort_id);
							$revision = $db->fetchRow('SELECT id, data FROM profile_updates_v2 WHERE escort_id = ? AND revision = ?', array($phone->escort_id, $max_revision));
							
							$data = unserialize($revision->data);
							
							$data['phone_number'] = $phone_out_ndd;
							$data['phone_country_id'] = $cc->id;
							
							$db->update('profile_updates_v2', array('data' => serialize($data)), $db->quoteInto('id = ?', $revision->id));
							
							$snapshot = $db->fetchRow('SELECT data FROM escort_profiles_snapshots_v2 WHERE escort_id = ?', array($phone->escort_id));
							
							$snap = unserialize($snapshot->data);
							
							$snap->phone_number = $phone_out_ndd;
							$snap->phone_country_id = $cc->id;
							
							$db->update('escort_profiles_snapshots_v2', array('data' => serialize($snap)), $db->quoteInto('escort_id = ?', $phone->escort_id));
							
							// Update profile_updates_v2
							$db->update('escort_profiles_v2', array('phone_number' => $phone_out_ndd, 'phone_country_id' => $cc->id, 'dd' => 1), $db->quoteInto('escort_id = ?', $phone->escort_id));
							
							echo "Escort_id:" . $phone->escort_id . " -> " . $cc->phone_prefix . " - " . $phone_out_ndd . " - " . $phone->contact_phone_parsed . "<br/>";
							break;
						}
					}
				}
			}
		}
		
		die('Done :)');
	}
	
	public function insertSlugCitiesAction()
	{
		
		$cities = $this->_db2->fetchAll('SELECT id, title_' . Cubix_Application::getDefaultLang() . ' AS title FROM cities /*WHERE country_id = ?*/'/*, array(Cubix_Application::getById()->country_id)*/);
		
		foreach($cities as $city) {
			$data = array(				
				'slug' => Cubix_Utils::makeSlug($city->title)
			);
			$this->_db2->update('cities', $data, $this->_db2->quoteInto('id = ?', $city->id));
		}
		
		die('done!');
	}
	
	public function and6DicAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		
		$lng = 'fr';
		
		$dic = $this->_db->fetchAll('SELECT id, value_' . $lng . ' AS value FROM dictionary_' . $lng);
		
		foreach($dic as $d) {
			$this->_db->update('dictionary', array('value_' . $lng => $d['value']), $this->_db->quoteInto('id = ?', $d['id']));
		}
		
		die('done!');
	}
	
	public function fillCitiesOrderingAction()
	{
		$cities = $this->_db2->fetchAll('SELECT id, order_id, region_id FROM cities ORDER BY region_id ASC');
		$order = 1;
		$region_id = $cities[0]->region_id;
		
		foreach($cities as $city) {
			if ( $region_id != $city->region_id ) {
				$order = 1;
				$region_id = $city->region_id;
			}
			$this->_db2->update('cities', array('order_id' => $order), $this->_db2->quoteInto('id = ?', $city->id));
			$order++;
		}
		die('done');
	}
	
	/**
	 ** Merging "Zürich City" and "Zürich City Süd" in one city "Zürich City"
	 **/
	public function mergeZurichCitiesAction()
	{
		die('REMOVE ME :D.');
		// 1 step - change city_id in tables "escort_cities", "escorts"
		// 2 step - update tables "escort_profiles_snapshot_v2", "profile_updates_v2"
		$new_city_id = $this->_getParam('new_city_id');
		if ( ! $new_city_id ) {
			die('New city_id needed');
		}
		$new_city = $this->_db2->fetchRow('SELECT * FROM cities WHERE id = ? ', array($new_city_id));
		$old_cities = array(15, 16);
		
		//Getting list of escorts which has old_city
		$escorts = $this->_db2->fetchAll('SELECT id, city_id FROM escorts WHERE city_id IN (' . implode(',', $old_cities) . ')' );

		foreach($escorts as $escort) {
			$this->_db2->beginTransaction();
			try {

				$this->_db2->update('escorts', array('city_id' => $new_city->id), $this->_db2->quoteInto('id = ?', $escort->id ));
				
				$this->_db2->delete('escort_cities', $this->_db2->quoteInto('escort_id = ?', $escort->id ));
				$this->_db2->insert('escort_cities', array('city_id' => $new_city->id, 'escort_id' => $escort->id));


				//Snapshots fixing
				$snapshot_data = $this->_db2->fetchOne('SELECT data FROM escort_profiles_snapshots_v2 WHERE escort_id = ?', array($escort->id));
				if ( $snapshot_data ) {
					$snapshot_data = unserialize($snapshot_data);
					$snapshot_data->base_city_id = $new_city->id;
					$snapshot_data->city_slug = $new_city->slug;
					foreach(Cubix_I18n::getLangs() as $lang) {
						$snapshot_data->{'base_city_' . $lang } = $new_city->{'title_' . $lang};
						$snapshot_data->{'city_title_' . $lang } = $new_city->{'title_' . $lang};
					}

					$this->_db2->update('escort_profiles_snapshots_v2', array('data' => serialize($snapshot_data)), $this->_db2->quoteInto('escort_id = ?', $escort->id));
				}


				//Revisions fixing
				$revision_data = $this->_db2->fetchRow('SELECT data, revision FROM profile_updates_v2 WHERE escort_id = ? ORDER BY revision DESC LIMIT 1', array($escort->id));
				if ( $revision_data ) {
					$revision = $revision_data->revision;
					$revision_data = unserialize($revision_data->data);
					$revision_data['city_id'] = $new_city->id;
					$revision_data['cities'][0]['city_id'] = $new_city->id;

					$this->_db2->update('profile_updates_v2', array('data' => serialize($revision_data)), array($this->_db2->quoteInto('escort_id = ?', $escort->id), $this->_db2->quoteInto('revision = ?', $revision)));

				}

				$this->_db2->commit();
			} catch( Exception $ex ) {
				$this->_db2->rollBack();
				throw $ex;
			}
		}
		die('done');
	}
	
	/*public function fixSnapshotAgeAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;

		$sql = "SELECT eps.escort_id, eps.data FROM escort_profiles_snapshots_v2 eps INNER JOIN escorts e ON e.id = eps.escort_id INNER JOIN users u ON u.id = e.user_id WHERE u.application_id = ?";

		$data = $db->query($sql, array(Cubix_Application::getId()))->fetchAll();
		echo(count($data));
		
		foreach ( $data as $d ) {
			$escort_data = unserialize($d->data);
			
			$bd = $db->fetchRow('SELECT birth_date FROM escort_profiles_v2 WHERE escort_id = ?', array($d->escort_id));
			
			$escort_data->birth_date = ($bd->birth_date) ? strtotime($bd->birth_date) : null;
			
			$db->update('escort_profiles_snapshots_v2', array('data' => serialize($escort_data)), $db->quoteInto('escort_id = ?', $d->escort_id));

		}
		
		die('Done!');
	}*/
	
	/*public function activatePackageAction()
	{
		$order_package_id = $this->_request->order_package_id;
		
		if ( ! $order_package_id ) {
			die('Order package id required');
		}
		
		$cron = new Model_Cron();
		$cron->_ActivatePackage($order_package_id);
		die('done');
	}*/
	
	public function slogansAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;
		
		$query = "SELECT id, slogan, date_last_modified FROM escorts";
		$items = $db->query($query)->fetchAll();
		
		$i = 0;
		
		foreach ($items as $item)
			if (strlen(trim($item->slogan)) > 0)
			{
				/*$db->insert('escort_slogans', array(
					'escort_id' => $item->id,
					'text' => $item->slogan,
					'date' => $item->date_last_modified,
					'status' => 1
				));*/
				
				$i++;
			}
		
		echo 'Done!!! Count = ' . $i . ' slogans.';
		die;
	}
	
	public function fixAnnAgencySitesAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;
		
		$sql = '
			SELECT e.id, ep.website, eps.data
			FROM escort_profiles_v2 ep
			INNER JOIN escorts e ON e.id = ep.escort_id
			INNER JOIN escort_profiles_snapshots_v2 eps ON eps.escort_id = e.id
			WHERE e.agency_id IS NOT NULL AND ep.website <> ""
		';
		
		$agency_escorts = $db->fetchAll($sql);
		
		foreach ( $agency_escorts as $escort ) {
			//update escort_profiles_v2
			$db->update('escort_profiles_v2', array('website' => null), $db->quoteInto('escort_id = ?', $escort->id));
			
			//update snapshots
			$profile = unserialize($escort->data);
			$profile->website = null;
			$db->update('escort_profiles_snapshots_v2', array('data' => serialize($profile)), $db->quoteInto('escort_id = ?', $escort->id));
			
			$escort_revs = $db->fetchAll('SELECT revision, data FROM profile_updates_v2 WHERE escort_id = ?', array($escort->id));
			
			if ( count($escort_revs) ) {
				foreach( $escort_revs as $rev ) {
					$revision = unserialize($rev->data);
					
					$revision['website'] = null;
					
					$db->query('UPDATE profile_updates_v2 SET data = ? WHERE escort_id = ? AND revision = ?', array(serialize($revision), $escort->id, $rev->revision));
				}
			}
			echo "escort_id:" . $escort->id . "<br/>";
		}
		
		die('Done');
	}
	
	public function esAction()
	{
		/*$db = $this->_db2;
		
		$res = $db->query("SELECT x.*
			FROM (SELECT e.id, e.status 
 FROM escorts e 
INNER JOIN users u ON e.user_id = u.id 
INNER JOIN applications a ON a.id = u.application_id 
LEFT JOIN countries _c ON a.country_id = _c.id 
LEFT JOIN backend_users bu ON bu.id = u.sales_user_id 
LEFT JOIN order_packages ord_p ON ord_p.escort_id = e.id  AND  ord_p.application_id = 2 
LEFT JOIN escort_profiles_v2 ep ON ep.escort_id = e.id 
LEFT JOIN agencies ag ON ag.id = e.agency_id 
LEFT JOIN seo_entities si ON si.slug = 'escort' 
LEFT JOIN seo_entity_instances sei ON sei.primary_id = e.id AND sei.entity_id = si.id WHERE u.application_id = '2' AND e.country_id = '71' AND ord_p.package_id = 7 AND ord_p.status = 2  AND (e.status & 32)
 GROUP BY e.id
 ORDER BY e.id asc) x
			LEFT JOIN order_packages _op ON _op.escort_id = x.id AND _op.status = 2 AND _op.application_id = 2
			LEFT JOIN packages _p ON _p.id = _op.package_id
			GROUP BY x.id")->fetchAll();
		
		foreach ($res as $r)
		{
			$status = $r->status;
			echo 'Escort ID: ' . $r->id . '; Old status = ' . $status;
			$status = $status ^ 32;
			$status = $status | 16;
			echo ' - New status = ' . $status . '<br>';
			
			$db->update('escorts', array('status' => $status), $db->quoteInto('id = ?', $r->id));
		}
		
		die;*/
	}
	
	public function checkBlCommentsAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;
		$blackListModel = new Model_BlacklistedWords();
		
		$comments = $db->fetchAll('SELECT id, message from comments order by id');
		foreach($comments as $comment){
			echo '<br/>'.$comment->id;
			if($blackListModel->checkWords($comment->message)){
					echo ' has bl';
					$comment->has_bl = 1;
					$data = (array) $comment;
					$db->update('comments', $data, $db->quoteInto('id = ?', $data['id']));
				}
		}
		die('finish');
		
	}
	
	public function checkBlAboutAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;
		$blackListModel = new Model_BlacklistedWords();
		$langs = Cubix_I18n::getLangs(true);
				
		$escorts = $db->fetchAll('SELECT escort_id, ' .Cubix_I18n::getTblFields('about'). ' from  escort_profiles_v2 order by escort_id');
		
		foreach($escorts as $escort){
			echo '<br/>'.$escort->escort_id;
			foreach($langs as $lang){
				
				if($blackListModel->checkWords($escort->{'about_'.$lang}, 'about me') ){
					echo ' about_'.$lang.' has bl';
					$escort->about_has_bl = 1;
					$data = (array) $escort;
					$db->update('escort_profiles_v2', $data, $db->quoteInto('escort_id = ?', $data['escort_id']));
					BREAK;
				}
			}
			
			
		}
		die('finish');
	}
	
	/*public function reviewStatusAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;
		
		$items = $db->query('select id from reviews_old where creation_date < "2012-05-29" and status <> 3')->fetchAll();
		
		foreach ($items as $item)
		{
			$db->update('reviews', array('status' => 4), $db->quoteInto('id = ?', $item->id));
		}
		
		echo 'count = ' . count($items) . '<br>';
		die('finish');
	}*/
	
	/*public function fuckAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		
		$db = $this->_db2;
		
		$per_page = 1000;

		$j = 1;

		for ($i = 1; $i <= 34; $i++)
		{
			$start = ($i - 1) * $per_page;
			$escorts = $db->query('SELECT id, fuckometer, showname FROM escorts ORDER BY id LIMIT ?, ?', array($start, $per_page))->fetchAll();

			foreach ($escorts as $escort)
			{
				$new_f = 0;
				$str = '';
				$str .= $j . '. Escort ID: ' . $escort->id . ' - Showname: ' . $escort->showname . ' - Old fuckometer: ' . $escort->fuckometer . ' - ';
				$new_f = $this->updateEscortFuckometer($escort->id);
				$str .= 'New fuckometer: ' . $new_f;
				
				if (number_format($escort->fuckometer, 1) != number_format($new_f, 1))
					$str .= ' - NOT EQUALE!!!!!!!!!!!!!!!!!!!!!!!!';
				
				$str .=  "\n";
				
				file_put_contents('/var/log/fuck.log', $str, FILE_APPEND);
				$j ++;
			}
		}

		echo 'Finish' . "\n";
		die;
	}
	
	private function updateEscortFuckometer($escort_id)
	{
		$db = $this->_db2;
		
		$f = $this->calcEscortFuckometer($escort_id);

		$db->update('escorts', array(
			'fuckometer' => $f
		), array($db->quoteInto('id = ?', $escort_id)));
		
		return $f;
	}
	
	private function calcEscortFuckometer($escort_id)
	{
		$db = $this->_db2;

		$TOTAL_FUCKOMETER_USER_MIN_REVIEWS = 1;

		// list of trusted members user_ids
		$trusted = $this->getTrustedMembers();

		$result = $db->fetchAll('
			SELECT r.user_id,
				COUNT(r.user_id) AS count,
				SUM(r.fuckometer) AS sum,
				m.reviews_count
			FROM reviews r
			LEFT JOIN members m ON m.user_id = r.user_id
			WHERE r.escort_id = ? AND r.status IN (2, 4) AND m.reviews_count >= ?
			GROUP BY r.user_id
		', array($escort_id, $TOTAL_FUCKOMETER_USER_MIN_REVIEWS));

		// calculate fuckometer
		$count = $count_trusted = $sum = 0;
		foreach ( $result as $data ) {
			$trusted_k = in_array($data->user_id, $trusted) ? 2 : 1;

			$count += ($data->count * $trusted_k);
			$count_trusted += in_array($data->user_id, $trusted) ? $data->count : 0;
			$sum += ($data->sum * $trusted_k);
		}

		$fuckometer = 0;
		if ( $count != 0 ) {
			$fuckometer = $sum / $count;
		}

		// reviews count bonus
		switch ( true ) {
			case (5 < $count && 10 >= $count): $fuckometer += 0.5; break;
			case (10 < $count && 20 >= $count): $fuckometer += 1; break;
			case (20 < $count): $fuckometer += 2; break;
		}

		return $fuckometer;
	}
	
	private function getTrustedMembers()
	{
		$db = $this->_db2;

		$FUCKOMETER_TRUSED_USER_REVIEWS = 4;

		$members = $db->fetchAll('
			SELECT DISTINCT r.user_id
			FROM reviews r
			GROUP BY r.user_id
			HAVING COUNT(*) >= ?
		', $FUCKOMETER_TRUSED_USER_REVIEWS, Zend_Db::FETCH_NUM);
		
		$members = (array)$members;

		foreach ( $members as $i => $member ) {
			$members[$i] = $member[0];
		}

		return $members;
	}*/
	
	public function fixAgencyLocationsAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;
				
		$agencies = $db->fetchAll('
			SELECT 
				a.id,
				a.fake_city_id AS city_id, a.city_id AS area_id,
				fc.title_en AS city_title,
				c.title_en AS area_title,
				a.zip, a.address
			FROM agencies a
			INNER JOIN f_cities fc ON fc.id = a.fake_city_id 
			LEFT JOIN cities c ON c.id = a.city_id			
			INNER JOIN users u ON u.id = a.user_id AND u.application_id = ' . Cubix_Application::getId() . '
			WHERE a.latitude IS NULL
			GROUP BY a.id
		');
		
		foreach($agencies as $agency) {
			
			$address = array(
				$agency->address,
				$agency->city_title,
				'Switzerland'
			);
			
			$url = 'http://maps.googleapis.com/maps/api/geocode/json?address=' . implode(',', $address) .  '&sensor=false';
			$url = preg_replace('# #', '+', $url);
			
			$data = file_get_contents($url);
			
			$data = json_decode($data);
			
			$coord = array(
				'latitude' => $data->results[0]->geometry->location->lat,
				'longitude' => $data->results[0]->geometry->location->lng
			);
			$db->update('agencies', $coord, $db->quoteInto('id = ?', $agency->id));
			
			echo $url . "<br/>";
			echo 'Agency: ' . $agency->id . ' Address: ' . $data->results[0]->formatted_address . "<br/>";
		}
		
		die('finish');
	}
	

	public function fixFcitiesLocationsAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;
				
		$cities = $db->fetchAll('
			SELECT 
				id, title_en
			FROM f_cities
			WHERE latitude IS NULL
		');
		
		foreach($cities as $city) {
			
			$address = array(
				$city->title_en,
				'Switzerland'
			);
			
			$url = 'http://maps.googleapis.com/maps/api/geocode/json?address=' . implode(',', $address) .  '&sensor=false';
			$url = preg_replace('# #', '+', $url);
			
			$data = file_get_contents($url);
			
			$data = json_decode($data);
			
			$coord = array(
				'latitude' => $data->results[0]->geometry->location->lat,
				'longitude' => $data->results[0]->geometry->location->lng
			);
			$db->update('f_cities', $coord, $db->quoteInto('id = ?', $city->id));
			
			echo $url . "<br/>";
			echo 'City: ' . $city->title_en . "<br/>";
			//die;
		}
		
		die('finish');
	}
	
	public function fixAreasLocationsAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;
				
		$cities = $db->fetchAll('
			SELECT 
				id, title_en
			FROM cities
			WHERE latitude IS NULL
		');
		
		foreach($cities as $city) {
			
			$address = array(
				$city->title_en,
				'Switzerland'
			);
			
			$url = 'http://maps.googleapis.com/maps/api/geocode/json?address=' . implode(',', $address) .  '&sensor=false';
			$url = preg_replace('# #', '+', $url);
			
			$data = file_get_contents($url);
			
			$data = json_decode($data);
			
			$coord = array(
				'latitude' => $data->results[0]->geometry->location->lat,
				'longitude' => $data->results[0]->geometry->location->lng
			);
			$db->update('cities', $coord, $db->quoteInto('id = ?', $city->id));
			
			echo $url . "<br/>";
			echo 'City: ' . $city->title_en . "<br/>";
			//die;
		}
		
		die('finish');
	}
	

	public function moveBlAction()
	{
		/*set_time_limit(0);
		ini_set('memory_limit', '1024M');
		
		$db_from = $this->_db2;
		
		$db_to = $this->_db = Zend_Db::factory('mysqli', array(
			'host' => 'www1.ef.net',
			'username' => 'girlforum_ef',
			'password' => 'jas9Ed%lQuNeM',
			'dbname' => 'girlforum_escortforum_net'
		));
		
		$db_to->query('SET NAMES `utf8`');
		
		$items = $db_from->query('
			SELECT 
				c.title_en AS c_title, b.date, b.client_name, b.client_phone, b.comment
			FROM blacklisted_clients b 
			LEFT JOIN cities c ON b.city_id = c.id
			WHERE b.is_approved = 1 ORDER BY b.date
		')->fetchAll();
		
		$i = 0;
		
		foreach ($items as $item)
		{	
			$post = '';
						
			if ($item->client_name)
			{
				$post .= '<strong>Nome</strong><br/>';
				$post .= $item->client_name . '<br/>';
			}
			
			if ($item->c_title)
			{
				$post .= '<strong>Citta</strong><br/>';
				$post .= $item->c_title . '<br/>';
			}
			
			if ($item->client_phone)
			{
				$post .= '<strong>Numero di telefono</strong><br/>';
				$post .= $item->client_phone . '<br/>';
			}
			
			if ($item->comment)
			{
				$post .= '<strong>Commenta</strong><br/>';
				$post .= $item->comment;
			}
			
			$db_to->insert('posts', array(
				'author_id' => 2, //administrator
				'author_name' => 'administrator',
				'use_sig' => 1,
				'use_emo' => 1,
				'ip_address' => $_SERVER['REMOTE_ADDR'],
				'post_date' => strtotime($item->date),
				'post' => $post,
				'topic_id' => 19, // topic: client blacklist
				'new_topic' => 0
			));
			
			$i ++;
		}
		
		echo 'Count = ' . $i;
		
		$count = $db_to->fetchOne('SELECT COUNT(pid) FROM posts WHERE topic_id = 19');
		
		$db_to->update('topics', array(
			'posts' => $count,
			'last_post' => time()
		), $db_to->quoteInto('tid = ?', 19));
		
		$count = $db_to->fetchOne('SELECT COUNT(pid) FROM posts WHERE author_id = 2');
		
		$db_to->update('members', array(
			'posts' => $count
		), $db_to->quoteInto('member_id = ?', 2));
		
		$count = $db_to->fetchOne('SELECT COUNT(pid) FROM posts WHERE topic_id = 19');
		
		$db_to->update('forums', array(
			'posts' => $count
		), $db_to->quoteInto('id = ?', 9));
		*/
		die;
	}


	public function fixPhoneNumberAction(){
		
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;
		$sql = "SELECT escort_id, phone_number, contact_phone_parsed FROM escort_profiles_v2";
		$data = $db->fetchAll($sql);
		foreach ( $data as $d ) {
			if( preg_match("/^(\(?)11(\)?)/",$d->phone_number) AND strlen($d->contact_phone_parsed) < 15){
				$phone = trim($d->phone_number);
				$phone_parsed = $d->contact_phone_parsed;
				$phone = preg_replace("/^(\(?)11(\)?)/", '${1}119${2}', $phone);
				$phone_parsed = preg_replace("/^(0055)11/", '${1}119', $phone_parsed);
				$db->update('escort_profiles_v2', array('phone_number' => $phone, 'contact_phone_parsed' =>$phone_parsed), $db->quoteInto('escort_id = ?', $d->escort_id));
				
				$escort_data = $db->query('SELECT escort_id, data FROM profile_updates_v2 WHERE escort_id = ?', $d->escort_id)->fetch();


				$escort_data = unserialize($escort_data->data);
				$escort_data['phone_number'] = $phone;
				$escort_data['contact_phone_parsed'] = $phone_parsed;
								
				$db->update('profile_updates_v2', array('data' => serialize($escort_data)), $db->quoteInto('escort_id = ?', $d->escort_id));
				
				$m_snapshot = new Model_Escort_SnapshotV2(array('id' => $d->escort_id));
				$m_snapshot->snapshotProfileV2();
			}
		}
		die;
	}
	
	public function fixEscortCoordinatesAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;
		
		$sql = '
			SELECT 
				e.user_id, e.id AS escort_id, e.showname,
				e.fake_city_id AS city_id, e.city_id AS area_id, e.disabled_comments, e.comments,
				' . Cubix_I18n::getTblFields('fc.title', null, false, 'city_title') . ',
				' . Cubix_I18n::getTblFields('c.title', null, false, 'area_title') . ',
				e.fake_zip AS zip, CONCAT(ep.street, " ", ep.street_no) AS address, ep.email, ep.website AS web, ep.contact_phone_parsed AS phone, 
				eph.hash AS photo_hash, eph.ext AS photo_ext,
				e.latitude, e.longitude,
				c.slug AS area_slug, fc.slug AS city_slug,
				CONCAT(e.showname, " ", e.fake_zip, " ", ep.street, " ", ep.street_no, " ", ep.email, " ", ep.website, " ", ep.contact_phone_parsed, " ", fc.' . Cubix_I18n::getTblField('title') . ', " ", c.' . Cubix_I18n::getTblField('title') . ') AS search_string
			FROM escorts e
			INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
			INNER JOIN escort_photos eph ON eph.escort_id = e.id AND eph.is_main = 1
			INNER JOIN f_cities fc ON fc.id = e.fake_city_id 
			LEFT JOIN cities c ON c.id = e.city_id 
			INNER JOIN order_packages op ON op.escort_id = e.id AND op.order_id IS NOT NULL AND op.status = 2
			INNER JOIN users u ON u.id = e.user_id AND u.application_id = 16
			WHERE 
				u.status = 1 AND (u.user_type = \'escort\' OR u.user_type = \'agency\') AND e.status = 32
				AND ep.street IS NOT NULL AND ep.street <> \'\' AND e.latitude IS NULL
			GROUP BY e.id
		';
		
		$escorts = $db->fetchAll($sql);
		
		foreach ( $escorts as $escort ) {
			
			$fake_city_id = $db->fetchOne('SELECT fake_city_id FROM escorts WHERE id = ?', array($escort->escort_id));
			$city_title = $db->fetchOne('SELECT title_en FROM f_cities WHERE id = ?', array($fake_city_id));

			$address_data = array(
				$escort->address,
				$city_title,
				'Switzerland'
			);
			

			$url = 'http://maps.googleapis.com/maps/api/geocode/json?address=' . implode(',', $address_data) .  '&sensor=false';
			echo $escort->escort_id . "#----#" . $url . "<br/>";
			$url = preg_replace('# #', '+', $url);

			$c_data = file_get_contents($url);

			$c_data = json_decode($c_data);

			$coord = array(
				'latitude' => $c_data->results[0]->geometry->location->lat,
				'longitude' => $c_data->results[0]->geometry->location->lng
			);
			
			$db->update('escorts', $coord, $db->quoteInto('id = ?', $escort->escort_id));
		}
		
		echo count($escorts);die;
	}
	
	public function fixSelfCheckoutAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;
		
		$db->query('SET foreign_key_checks = 0');
		
		$t_sql = '
			SELECT 
				id, user_id
			FROM transfers
			WHERE backend_user_id = ?
		';
		
		$transfers = $db->fetchAll($t_sql, array(Model_Billing_OnlineBilling::$backend_users[$this->_request->application_id]['id']));
		
		if ( count($transfers) ) {
			foreach ( $transfers as $transfer ) {
				$sales_user_id = $db->fetchOne('SELECT sales_user_id FROM users WHERE id = ?', array($transfer->user_id));
				
				$db->update('transfers', array('backend_user_id' => $sales_user_id, 'is_self_checkout' => 1), $db->quoteInto('id = ?', $transfer->id));
			}
		}
		
		$o_sql = '
			SELECT 
				id, user_id
			FROM orders
			WHERE backend_user_id = ?
		';
		
		$orders = $db->fetchAll($o_sql, array(Model_Billing_OnlineBilling::$backend_users[$this->_request->application_id]['id']));
		//var_dump($orders);die;
		if ( count($orders) ) {
			foreach ( $orders as $order ) {
				$sales_user_id = $db->fetchOne('SELECT sales_user_id FROM users WHERE id = ?', array($order->user_id));
				
				$db->update('orders', array('backend_user_id' => $sales_user_id, 'is_self_checkout' => 1), $db->quoteInto('id = ?', $order->id));
			}
		}
		
		die('done');
	}
	
	public function phoneExistsAction(){
		
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;
		$sql = "SELECT escort_id, contact_phone_parsed FROM escort_profiles_v2";
		$data = $db->fetchAll($sql);
		foreach ( $data as $d ) {
			
				$db->update('escort_profiles_v2', array('phone_exists' => $d->contact_phone_parsed), $db->quoteInto('escort_id = ?', $d->escort_id));
				
							
		}
		die('Done');
	}
	
	public function fixCitySlugAction(){
		
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;
		$sql = "SELECT id, slug, title_pt AS title FROM cities";
		$data = $db->fetchAll($sql);
		foreach ( $data as $d ) {
				$db->update('cities', array('slug' => Cubix_Utils::makeSlug(Cubix_Model::translit($d->title))), $db->quoteInto('id = ?', $d->id));
		}
		die('Done');
	}
	
	/*public function dateAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;
		
		$items = $db->query('
			SELECT e.id, DATE(u.date_registered) AS date
			FROM escorts e
			INNER JOIN users u ON e.user_id = u.id
			GROUP BY e.id
		')->fetchAll();
		
		foreach ($items as $item)
		{
			$db->update('order_packages', array('date_activated' => $item->date), $db->quoteInto('escort_id = ?', $item->id));
		}
		
		die('done');
	}*/
	
	public function importAgenciesAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		
		$db = $this->_db2;	
		$xlsx = new Cubix_SimpleXLSX('/var/www/library/Cubix/agencies.xlsx');
		$getWorksheetName = $xlsx->getWorksheetName();
		
		$rows = $xlsx->rowsEx(1);
		unset($rows[0]);
		
		$model = new Model_Agencies();
		
		foreach ( $rows as $row ) {
			
			// 0 - agency_name
			// 1 - address
			// 2 - zip
			// 3 - fake_city (f_cities)
			// 4 - area (cities table)
			// 5 - web
			// 6 - phone
		
			$slug = $this->clear_string($row[0]['value']);
			$parse = parse_url($row[5]['value']);
			
			$email = (strlen($parse['host'])) ? 'info@' . preg_replace('#www.#', '', $parse['host']) : 'info@temp.com';
			
			$phone_parsed = (strlen($row[6]['value']) ) ? '0041' . substr(preg_replace('# #', '', $row[6]['value']), 1) : null;
			
			$save_data  = array(
				'application_id' 		=> 16,
				'sales_user_id'    		=> 72, // hauri
				'status' 	   			=> 1,
				'name'       			=> $row[0]['value'],
				'display_name'       	=> $row[0]['value'],
				'slug'       			=> $slug,
				'country_id' 			=> 1,
				
				'city_id'    			=> Model_Geography_FakeCities::getAreaIdByTitle($row[4]['value']),
				'fake_city_id'    		=> Model_Geography_FakeCities::getFakeCityIdByTitle($row[3]['value']),
				
				'zip'					=> $row[2]['value'],
				
				'phone_country_id'		=> ( strlen($row[6]['value']) ) ? 1 : null,
				'phone'		 			=> preg_replace('# #', '', $row[6]['value']),
				'contact_phone_parsed'	=> $phone_parsed,
				
				'email'      			=> $email,
				'u_email'      			=> $email,
				
				'web'		 			=> ( strlen($row[5]['value']) > 3 ) ? $row[5]['value'] : null,
				'phone_instructions' 	=> null,
				'username' 				=> preg_replace('#-#', '_', $slug),
				'password' 				=> '123456',
				'work_days'				=> null,
				'work_times_from'		=> null,
				'work_times_to'			=> null,
				'available_24_7'		=> null,
				'address'				=> $row[1]['value'],
				'latitude'				=> null,
				'longitude'				=> null,
				'is_anonymous'			=> 0,
				'is_club'				=> 1,
				'entrance'				=> 0,
				'wellness'				=> 0,
				'food'					=> 0,
				'outdoor'				=> 0,
				'filter_criteria'		=> null,
				'is_premium'			=> 0,
				'show_in_club_directory' => 1,
				'about' => array(),
				'is_imported' => 1
			);
			
			if ( ! $model->existsByUsername(preg_replace('#-#', '_', $slug)) ) {
				$model->save(new Model_AgencyItem($save_data));
				
				echo $row[0]['value'] . '<br/>';
			}			
			
			//echo $row[4]['value'] . ' - ' . Model_Geography_FakeCities::getAreaIdByTitle($row[4]['value']) . '<br/>';
		}
		
		die('Done :)');
	}
	
	public function fixCaAdsCityAction(){
		
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;
		
		$sql = "DELETE FROM classified_ads_cities";
		$db->query($sql);
		
		$sql2 = "SELECT id, city_id FROM classified_ads";
		$data = $db->fetchAll($sql2);
		
		foreach ( $data as $d ) {
				$db->insert('classified_ads_cities', array('ad_id' => $d->id, 'city_id' => $d->city_id));
		}
		
		die('Done');
	}
	
	public function fixAgenciesPAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;
		
		/*$sql = "select id, status from escorts where agency_id is not null and `status` & 256";
		$d_escorts = $db->fetchAll($sql);
		
		foreach ( $d_escorts as $escort ) {
			$status = new Cubix_EscortStatus($escort->id);
			
			$status->removeStatusBit(Cubix_EscortStatus::ESCORT_STATUS_DELETED);
			$status->save();
			echo "escort " . $escort->id . " status updated" . "<br/>";
		}*/
		
		$op_sql = "
			select opp.id from order_packages opp
			where status = 4 and expiration_date >= date(now()) and opp.status_comment like 'Agency Escort%'
		";
		$ops = $db->fetchAll($op_sql);
		
		foreach ( $ops as $o ) {
			$db->update('order_packages', array('status' => 2), $db->quoteInto('id = ?', $o->id));
			echo $o->id . "<br/>";
		}
		
		die('Done');
	}

	/*public function topEscortsAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;

		$escorts = $db->query('SELECT escort_id FROM reviews WHERE `status` = 2 GROUP BY escort_id HAVING COUNT(id) < 5 ')->fetchAll();

		foreach ($escorts as $escort)
		{
			$db->update('escorts', array('fuckometer' => 0), array($db->quoteInto('id = ?', $escort->escort_id)));
		}

		die('finish');
	}*/
	
	public function geotestAction()
	{
		$geoData = Model_Geoip::getClientLocation('134.212.240.112');
		
		//var_dump($geoData);
		$str = '';
		$ip = '134.212.240.112';
		$str .= $ip;
			
			if ($geoData['country'])
				$str .= ', ' . $geoData['country'];

			if ($geoData['city'])
				$str .= ', ' . $geoData['city'];

			if ($geoData['latitude'])
				$str .= ', lat: ' . $geoData['latitude'];

			if ($geoData['longitude'])
				$str .= ', lon: ' . $geoData['longitude'];
		
		echo $str;
		die;
	}
	
	public function fixVipAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;
		
		$packages = array(101, 102, 10, 80, 11, 79);
		
		$op_sql = "
			SELECT id AS order_package_id FROM order_packages WHERE package_id IN (" . implode(', ', $packages) . ") AND status = 2 and order_id is not null
		";
		
		$ops = $db->fetchAll($op_sql);
		
		foreach ( $ops as $o ) {
			echo $o->order_package_id . "<br/>";
			$db->insert('order_package_products', array(
				'order_package_id' => $o->order_package_id,
				'product_id' => 16,
				'is_optional' => 0,
				'price' => 0
			));
		}
		
		die('Done');
	}
	
	/*public function fixSystemDisabledAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;
		
		$items_d = $db->query("
			SELECT u.id, u.username, u.email, e.id AS escort_id 
			FROM users u
			LEFT JOIN escorts e ON u.id = e.user_id
			WHERE DATE(DATE_ADD(u.last_login_date, INTERVAL 30 DAY)) < DATE(NOW()) AND u.user_type IN ('escort')
		")->fetchAll();

		if ($items_d)
		{
			$k = 0;
			
			foreach ($items_d as $i)
			{	
				if ($i->escort_id)
				{
					$_status = new Cubix_EscortStatus($i->escort_id);
					$_status->setStatus(Cubix_EscortStatus::ESCORT_STATUS_OWNER_DISABLED);
					$_status->save();
				}

				// system disabled
				$mu = new Model_Users();
				$mu->setSystemDisabled($i->id, 1);
				
				$k++;
			}
		}
		
		echo 'count = ' . $k . '<br>';
		die('finish');
	}*/
	
	/*public function addNatsAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db;
		
		$file = file_get_contents('n.txt');
		
		$arr = explode("\n", $file);
				
		foreach ($arr as $a)
		{
			$a = trim($a);
			
			$nats = explode(' ', $a);
			
			//$nat = $nats[0];
			
			$iso = end($nats);
			$ni = count($nats) - 1;
			unset($nats[$ni]);
			$nat = implode(' ', $nats);
			
			$slug = Cubix_Utils::makeSlug($nat);
			
			echo $nat . '-' . $iso . '<br>';
			//echo $iso . '<br>';
			
			$db->insert('nationalities', array(
				'iso' => $iso,
				'slug' => $slug,
				'title_pt' => $nat,
				'title_es' => $nat,
				'title_fr' => $nat,
				'title_en' => $nat,
				'title_it' => $nat,
				'title_de' => $nat,
				'title_ro' => $nat
			));
		}
		
		die;
	}*/
	
	/*public function removeVipProductAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		//$db = Zend_Registry::get('db');
		$db = $this->_db2;
		
		$packages = array(11, 79);
		
		$op_sql = "
			SELECT id AS order_package_id FROM order_packages WHERE package_id IN (" . implode(', ', $packages) . ") AND status = 2 and order_id is not null
		";
		
		$ops = $db->query($op_sql)->fetchAll();
		$c = 0;
		foreach ( $ops as $o ) {
			echo $o->order_package_id . "<br/>";
			$db->query('DELETE FROM order_package_products WHERE order_package_id = ? AND product_id = 16', $o->order_package_id);
			$c++;
		}
		
		die('Count = ' . $c . '   ----   Done');
	}*/

	public function updateEscortsStatusAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		//$db = Zend_Registry::get('db');
		$db = $this->_db2;

		$sql = "
			select e.id from escorts e 
			inner join users u on u.id = e.user_id
			where e.status & 8 and u.application_id = 9
		";

		$escorts = $db->fetchAll($sql);

		foreach ($escorts as $escort) {
			$_status = new Cubix_EscortStatus($escort->id);
			$_status->setStatus(Cubix_EscortStatus::ESCORT_STATUS_ADMIN_DISABLED);
	  		$_status->save();
		}

		

  		die;
	}

	public function removeDefaultPackageAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		//$db = Zend_Registry::get('db');
		$db = $this->_db2;

		$sql = "
			select e.id as escort_id from escorts e
			inner join escort_profiles_v2 ep ON ep.escort_id = e.id
			inner join users u on u.id = e.user_id
			where e.status = 32 and ep.admin_verified = 1 and u.application_id = 9
			group by e.id
		";

		$escorts = $db->fetchAll($sql);		

		foreach ($escorts as $escort) {
			$db->update('order_packages', array('status' => 4), $db->quoteInto('escort_id = ?', $escort->escort_id));	
		}

		

  		die;
	}
	
	public function getNotWorkingSiteEscortsAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		
		$db = $this->_db2;

		$sql = "
			SELECT e.id, e.showname, u.id AS user_id, ep.website
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
			WHERE u.user_type = 'escort' AND ep.website IS NOT NULL AND ep.website <> '' AND u.status = 1 AND e.status & 32
		";

		$escorts = $db->fetchAll($sql);

		foreach ($escorts as $escort) {
			$db->query('UPDATE users SET sales_user_id = ? WHERE id = ?', array(83, $escort->user_id));
		}

		

  		die;
	}

	public function edSendSmsAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		
		$db = $this->_db2;	
		$xlsx = new Cubix_SimpleXLSX('/var/www/library/Cubix/ed_phones.xlsx');
		$getWorksheetName = $xlsx->getWorksheetName();

		$conf = Zend_Registry::get('system_config');
		$conf = $conf['sms'];
		$current_app_id = intval($this->_getParam('application_id'));
		$originator = Cubix_Application::getPhoneNumber($current_app_id);
		$model_sms = new Model_SMS_Outbox();
		
		$rows = $xlsx->rowsEx(1);
		$db->query('SET NAMES `utf8`');
		foreach ( $rows as $row ) {
			if( strlen(trim($row[0]['value'])) ) {

				
				$v = mysql_escape_string(trim($row[1]['value']));
				$phone = trim($row[0]['value']);
				$phone = preg_replace('/[^0-9]/', '', $phone);
				$phone = preg_replace('/^0/', '', $phone);
				$phone = "00" . $phone;
				
				
				$has_send = $db->fetchOne('SELECT TRUE FROM sms_outbox WHERE phone_to = ? AND text = ?', array($phone, "New! Register for FREE on www.escortdirectory.com or contact us at may@escortdirectory.com.Thank you"));

				var_dump($has_send);
				var_dump($phone);

				if ( ! $has_send ) {
					$data = array(
						'phone_number' => $phone,
						'phone_from' => $originator,
						'text' => "New! Register for FREE on www.escortdirectory.com or contact us at may@escortdirectory.com.Thank you",
						'application_id' => 69,
						'sender_sales_person' => 64
					);

					$sms = new Cubix_SMS($conf['userkey'], $conf['password']);
					$sms->setOriginator($originator);

					$id = $model_sms->save($data);
					var_dump($id);
					$sms->addRecipient($data['phone_number'], $id);

					$sms->setBufferedNotificationURL($conf['BufferedNotificationURL']);
					$sms->setDeliveryNotificationURL($conf['DeliveryNotificationURL']);
					$sms->setNonDeliveryNotificationURL($conf['NonDeliveryNotificationURL']);

					$sms->setContent($data['text']);

					if (1 != $result = $sms->sendSMS()) {}
				}


			} else {
				var_dump($row[0]['value'], $row[2]['value']);
			}
		}
		
		die('Done :)');
	}

	public function localizeImagesAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');

		$this->copyFiles('/var/www/escorts/and6.ch/public/img');

		die('DONE');
	}

	private function copyFiles($start_dir, $level = 0)
	{	
		$level++;

		$patterns = array(
			array('from' => "/_en./", 'to' => "_hu.", 'glob_pattern' => "*_en.*"),
			array('from' => "/-en-/", 'to' => "-hu-", 'glob_pattern' => "*-en-*"),
			array('from' => "/-en./", 'to' => "-hu.", 'glob_pattern' => "*-en.*"),
			array('from' => "/en_/", 'to' => "hu_", 'glob_pattern' => "en_*")
		);

		echo("$start_dir $level\n");
		chdir($start_dir);

		foreach($patterns as $pattern) {
			foreach(glob($pattern['glob_pattern']) as $filename) {
				$nfn = preg_replace($pattern['from'], $pattern['to'], $filename);
				if ( $nfn != $filename ) {
					copy($filename, $nfn);
					echo "$filename\n";
				}
			}
		}

		foreach(glob('*', GLOB_ONLYDIR) as $dir_name) {
			$this->copyFiles("$start_dir/$dir_name", $level);
		}
	}

	public function checkUsernamePasswordAction(){

		set_time_limit(0);
		ini_set('memory_limit', '1024M');

		
		$sql = "SELECT id, username, salt_hash, password, user_type FROM users WHERE id = 277346 AND  application_id = " . Cubix_Application::getId();
		
		$users = $this->_db2->fetchAll($sql);
		
		
		foreach ( $users as $user ) {

			$usernameHash = Cubix_Salt::hashPassword($user->username, $user->salt_hash);
			$passwordHash = $user->password;

			if($usernameHash == $passwordHash){
				$data['user_id'] = $user->id;
				$data['username'] = $user->username;

				if($user->user_type == 'escort'){
					$sql = "SELECT id as escort_id FROM escorts WHERE user_id = ".$user->id;
					$escort_id = $this->_db2->fetchOne($sql);
					$data['escort_id'] = $escort_id; 
				}

				$this->_db2->insert('username_equal_password',$data);

			}

			exit();
		}
		
		die();
	}

	function randomPassword() {
	    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
	    $pass = array(); //remember to declare $pass as an array
	    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
	    for ($i = 0; $i < 15; $i++) {
	        $n = rand(0, $alphaLength);
	        $pass[] = $alphabet[$n];
	    }
	    return implode($pass); //turn the array into a string
	}

	/*public function changeAllBackendPasswordsAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;	
		
		$excl_usernames = array('copy-api', 'gotd_manager', 'online_billing_manager', 'ivr', 'ivr-gotd', 'sync-manager', 'copy-api-uk');
		
		$where = " ";
		foreach( $excl_usernames as $excl ) {
			$where .= " AND username <> '" . $excl . "'";
		}

		$sql = "SELECT id, username FROM backend_users WHERE 1 " . $where . " AND application_id = " . Cubix_Application::getId();
		
		$users = $db->fetchAll($sql);
		
		foreach ( $users as $user ) {
			$password = $this->randomPassword();
			
			$db->update('backend_users', array(
				'password' => md5($password),
			), $db->quoteInto('id = ?', $user->id));

			echo $user->username . ' - ' . $password . "<br/>";
		}
		
		die();
	}*/

	/*public function calcRevComCountAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;

		$k = 0;
		$res = $db->query('select escort_id, count(id) as count from reviews GROUP BY escort_id')->fetchAll();

		if ($res)
		{
			foreach ($res as $r)
			{
				$db->update('escorts', array('reviews' => $r->count), $db->quoteInto('id = ?', $r->escort_id));
				$k++;
			}
		}

		echo 'Escort reviews count - ' . $k . "\n" . '<br>';
		$k = 0;

		$res = $db->query('select user_id, count(id) as count from reviews GROUP BY user_id')->fetchAll();

		if ($res)
		{
			foreach ($res as $r)
			{
				$db->update('members', array('reviews_count' => $r->count), $db->quoteInto('user_id = ?', $r->user_id));
				$k++;
			}
		}

		echo 'Member reviews count - ' . $k . "\n" . '<br>';
		$k = 0;

		$res = $db->query('select escort_id, count(id) as count from comments GROUP BY escort_id')->fetchAll();

		if ($res)
		{
			foreach ($res as $r)
			{
				$db->update('escorts', array('comments' => $r->count), $db->quoteInto('id = ?', $r->escort_id));
				$k++;
			}
		}

		echo 'Escort comments count - ' . $k . "\n" . '<br>';
		$k = 0;

		$res = $db->query('select user_id, count(id) as count from comments GROUP BY user_id')->fetchAll();

		if ($res)
		{
			foreach ($res as $r)
			{
				$db->update('members', array('comments_count' => $r->count), $db->quoteInto('user_id = ?', $r->user_id));
				$k++;
			}
		}

		echo 'Member comments count - ' . $k . "\n" . '<br>';

		die('done');
	}*/

	/*public function removeOldImagesA6Action()
	{
		die;
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$db = $this->_db2;

		$ftp_conf = array(
			'host' => 'pic.and6.ch',
			'login' => 'a6_photos',
			'password' => 'kse5Exn20a0mNEkA',
			'passive' => 1,
			'root_dir' => ''
		);

		$ftp = new Cubix_Images_Storage_Ftp($ftp_conf);

		$ftp->connect();

		$chunk = 2000;
		$part = 0;

		$cli = new Cubix_Cli();

		$size_map = array(
			'pp', 'pl',	'laction_thumb', 'xl_la_thumb', 'la_details', 'm480', 'backend_thumb', 'backend_agency_thumb',
			'backend_smallest', 'bubble_thumb', 'medium', 'new-main', 'gotm', 'gotd_m', 'gotm_xl', 'm_bubble',
			'new-thumb', 'thumb', 'xl_thumb', 'thumb_small', 'nlthumb', 'm-new-main', 'm-new-thumb', 'sthumb',
			'sthumb2', 'sthumb-agency', 'lvthumb', 'wthumb', 'agency_thumb', 'agency_photo_thumb', 'agency_p100',
			'orig', 'w68', 'vip_thumb', 't100p', 'm45', 'm95', 'm320', 'thumb_cropper', 'backend_thumb_cropper',
			'w514', 'm145', 'ob3_thumb', 'ob3_thumb_sm'
		);

		do {
			$data = $db->query('
				SELECT id, escort_id, hash, ext
				FROM escort_photo_history
				WHERE DATE(date) < DATE("2016-05-01")
				ORDER BY id LIMIT
			' . $part * $chunk . ', ' . $chunk)->fetchAll();
			$part += 1;
			$log = 'Chunk: ' . $part * $chunk . "\n\n";
			$cli->out($log);

			if (count($data))
			{
				foreach ($data as $photo)
				{
					$photo = (array)$photo;

					if ($photo['hash'] && $photo['ext'])
					{
						$escort_id = $photo['escort_id'];

						$parts = array();

						if (strlen($escort_id) > 2)
						{
							$parts[] = substr($escort_id, 0, 2);
							$parts[] = substr($escort_id, 2);
						}
						else
							$parts[] = $escort_id;

						$escort_id_path = implode('/', $parts);

						$full_path = '/and6.ch/' . $escort_id_path . '/' . $photo['hash'] . '.' . $photo['ext'];

						$real_size = $ftp->getFileSize($full_path);

						if ($real_size > 0)
						{
							$ftp->delete($full_path);
							$log = 'ID: ' . $photo['id'] . ', Escort ID: ' . $escort_id . ', path: ' . $full_path;
							$cli->out($log);
						}

						foreach ($size_map as $size)
						{
							$full_path = '/and6.ch/' . $escort_id_path . '/' . $photo['hash'] . '_' . $size . '.' . $photo['ext'];

							$real_size = $ftp->getFileSize($full_path);

							if ($real_size > 0)
							{
								$ftp->delete($full_path);
								$log = 'ID: ' . $photo['id'] . ', Escort ID: ' . $escort_id . ', path: ' . $full_path;
								$cli->out($log);
							}
						}
					}
				}
			}

		} while ( count($data) > 0 );

		$ftp->disconnect();

		die;
	}*/
}
