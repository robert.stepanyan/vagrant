<?php

class MembersController extends Zend_Controller_Action {
	public function init()
	{
		$this->model = new Model_Members();
	}
	
	public function indexAction() 
	{
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('default_members_data');
		$this->view->filter_params = $ses_filter->filter_params;
		/* FILTER PARAMS */
	}
	
	public function dataAction()
	{
		
		$req = $this->_request;
		
		$filter = array(
			'u.user_id' => $req->user_id,
			'u.username' => $req->username,
			'u.application_id' => $req->application_id,
			'u.email' => $req->email,
			'u.last_ip' => $req->last_ip,
			'u.status' => $req->status,
			'm.is_premium' => $req->only_premium,
			'partial' => $req->partial,
			'filter_by' => $req->filter_by,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to,
			'is_suspicious' => $req->is_suspicious,
			'has_member_comment' =>$req->has_member_comment,
			'is_system_member' =>$req->is_system_member,

			'm.rating' => $req->rating,
		);

		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);

		$DEFINITIONS = Zend_Registry::get('defines');
		$modelC = new Model_Comments();
		
		foreach ( $data as $i => $item ) {
			$data[$i]->status = $DEFINITIONS['user_status_options'][$item->status];
			
			if (!$item->city_title && $item->last_ip)
			{
				$geo = Cubix_Geoip::getClientLocation($item->last_ip);
				
				if (!is_null($geo))
				{
					$data[$i]->city_title = utf8_encode($geo['city']);
					$data[$i]->geo = 1;
					
				}
			}
			
			$data[$i]->reviews_count = intval($modelC->getUserReviewsCount($item->id));
			$data[$i]->comments_count = intval($modelC->getUserCommentsCount($item->id));
		}
				
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
		
	}
	
	public function createAction()
	{
		$can_set_premium = Model_Users::can('set-member-premium');
		
		if ( $this->_request->isPost() )
		{
			$data = new Cubix_Form_Data($this->_request);

			$params = array(
				'username' => '',
				'status' => '',
				'password' => '',
				'application_id' => 'int',
				//'sales_person_id' => 'int',
				'country_id' => 'int',
				'city_id' => 'int',
				'email' => '',
				'about_me' => '',
				'is_system_member' => 'int',
			);

			if ( $can_set_premium && Cubix_Application::getId() != APP_6A ){
				$params = array_merge($params, array('date_expires' => ''));
			}
			
			if ( Cubix_Application::getId() == APP_ED ){
				$params = array_merge($params, array('show_languages' => '','langs' => ''));
			}

			$data->setFields($params);
			
			$data = $data->getData();
						
			$validator = new Cubix_Validator();
				
			/*if ( ! $data['sales_person_id'] ) {
				$validator->setError('sales_person_id', 'Required');
			}*/

			if ( in_array(Cubix_Application::getId(), array(APP_ED)) ) {
				$DEFINITIONS = Zend_Registry::get('defines');

				$langs = $data['langs'];

				if ( ! is_array($langs) ) $langs = array();

				$invalid_lang = false;

				$langData = array();
				foreach ( $langs as $val => $lang_id ) {

					$langData[] = $lang_id;

					if ( ! array_key_exists($lang_id, $DEFINITIONS['language_options']) ) {
						$invalid_lang = true;
					}
				}

				if ( $invalid_lang ) {
					$validator->setError('language_error', Cubix_I18n::translate('sys_error_lang_is_invalid'));
				}


				if(count($data['langs'])){
					$data['langs'] = serialize($langData);
				}else{
					$data['langs'] = null;
				}
			}

			if ( $can_set_premium ) {
				if ( $data['date_expires'] ) {
					if ( $data['date_expires'] < time() ) {
						$validator->setError('date_expires', 'Wrong date');
					}
				}
				else {
					$data['date_expires'] = null;
				}
			}

			if ( ! $data['status'] ) {
				$validator->setError('status', 'Required');
			}
			
			if ( ! strlen($data['username']) ) {
				$validator->setError('username', 'Required');
			}
			elseif ( $this->model->existsByUsername($data['username']) ) {
				$validator->setError('username', 'Username Already exists');
			}
			elseif ( strlen($data['username']) < 6 ) {
				$validator->setError('username', 'Username must be at least 6 characters long');
			}
			
			if ( ! strlen($data['password']) ) {
				$validator->setError('password', 'Required');
			}
			elseif ($data['password'] == $data['username']){
				$validator->setError('password', "Can't use username as password");
			}
						
			/*???????????????????????*/
			if ( strlen($data['email']) && $this->model->existsByEmail($data['email']) ) {
				$validator->setError('email', 'Email Already exists');
			}
			
			if ( strlen($data['email']) && Cubix_Application::isDomainBlacklisted($data['email']) ) {
				$validator->setError('email', 'Domain is blacklisted');
			}
						
			if ( $validator->isValid() ) {
				$data_va = array(
					'application_id' 		=> $data['application_id'],
					//'sales_user_id'    		=> $data['sales_person_id'],
					'status' 	   			=> $data['status'],
					'country_id' 			=> $data['country_id'],
					'city_id'    			=> $data['city_id'],
					'email'      			=> $data['email'],
					'username' 				=> $data['username'],
					'password' 				=> $data['password'],
					'about_me' 				=> $data['about_me']
				);

				if ( Cubix_Application::getId() == APP_ED ){
					if (strlen($data['show_languages'])){
						$data_va['show_languages'] = 1;
					}else{
						$data_va['show_languages'] = 0;
					}

					$data_va['langs'] = $data['langs'];
				}
				
				if ( in_array(Cubix_Application::getId(), array(APP_ED, APP_EG_CO_UK, APP_EG, APP_6C)) ) {
					$data_va = array_merge($data_va, array('is_system_member' => $data['is_system_member']));
				}

				if ( $can_set_premium ) {
					if ( $data['date_expires'] ) {
						$data_va = array_merge($data_va, array('date_expires' => date("Y-m-d H:i:s", $data['date_expires'])));
					}
					else {
						$data_va = array_merge($data_va, array('date_expires' => null));
					}
				}

				$this->model->save(new Model_MemberItem($data_va));
			}
			
			die(json_encode($validator->getStatus()));
		}
		else {
			$this->view->can_set_premium = $can_set_premium;
		}
	}

	public function editAction()
	{
		$can_set_premium = Model_Users::can('set-member-premium');
		$bu_user = Zend_Auth::getInstance()->getIdentity();

        if( ! $this->_request->isPost() )
		{
			$data_st = $this->model->getById($this->_request->id);
			if ( Cubix_Application::getId() == APP_ED ){
				if(count($data_st['langs'])){
					$data_st['langs'] = unserialize($data_st['langs']);
				}
			}
			$this->view->member = $data_st;
					
			$this->view->can_set_premium = $can_set_premium;

			$l = Model_Users::getLastLogin($data_st->user_id);
			$this->view->last_login = $l->login_date;
			$this->view->last_ip = $l->ip;
			$this->view->last_login_count = Model_Users::getLastLoginsCount($data_st->user_id);

			$m_c = new Model_Comments();
			$this->view->comments_count = $m_c->getUserCommentsCount($data_st->user_id);
			$this->view->reviews_count = $m_c->getUserReviewsCount($data_st->user_id);
			$this->view->ip_info = Model_Users::getUniqueIps($data_st->user_id);
			$this->view->last_login_count_30 = Model_Users::getLastLogins($data_st->user_id, 30);
			$this->view->last_login_count_180 = Model_Users::getLastLogins($data_st->user_id, 180);
			$this->view->last_login_count_365 = Model_Users::getLastLogins($data_st->user_id, 365);
			$this->view->defines = Zend_Registry::get('defines');
            if (in_array(Cubix_Application::getId(), array(APP_ED)))
                $this->view->first_login_lang = $this->model->getFirstLoginLang($data_st->id);

		}
		else 
		{
			$data = new Cubix_Form_Data($this->_request);

			$params = array(
				'id' => 'int',
				'user_id' => 'int',
				'status' => '',
				'username' => '',
				'password' => '',
				'application_id' => 'int',
				//'sales_person_id' => 'int',
				'country_id' => 'int',
				'city_id' => 'int',
				'email' => '',
				'about_me' => '',
				'is_suspicious' => '',
				'suspicious_reason' => '',
				'is_trusted' => '',
				'trusted_reason' => '',
				'member_comment' => '',
				'pm_is_blocked' => '',
				'im_is_blocked' => '',				
				'newsletter' => 'int',
				'escorts_watched_type' => 'int',
				'is_system_member' => 'int',
				'rating' => ''
			);

			if ( $can_set_premium && Cubix_Application::getId() != APP_6A ){
				$params = array_merge($params, array('date_expires' => ''));
			}

			if ( Cubix_Application::getId() == APP_ED ){
				$params = array_merge($params, array('show_languages' => '','langs' => ''));
			}

			if ( Cubix_Application::getId() == APP_EF && $bu_user->type == 'superadmin' ){
				$params = array_merge($params, array('cam_trusted' => 'int'));
			}
			$data->setFields($params);
			
			$data = $data->getData();

			//print_r($data);

			$validator = new Cubix_Validator();
			
			/*if ( ! $data['sales_person_id'] ) {
				$validator->setError('sales_person_id', 'Required');
			}*/


			if ( in_array(Cubix_Application::getId(), array(APP_ED)) ) {
				$DEFINITIONS = Zend_Registry::get('defines');

				$langs = $data['langs'];

				if ( ! is_array($langs) ) $langs = array();

				$invalid_lang = false;

				$langData = array();
				foreach ( $langs as $val => $lang_id ) {

					$langData[] = $lang_id;

					if ( ! array_key_exists($lang_id, $DEFINITIONS['language_options']) ) {
						$invalid_lang = true;
					}
				}

				if ( $invalid_lang ) {
					$validator->setError('language_error', Cubix_I18n::translate('sys_error_lang_is_invalid'));
				}


				if(count($data['langs'])){
					$data['langs'] = serialize($langData);
				}else{
					$data['langs'] = null;
				}
			}

			if ( $can_set_premium ) {
				if ( $data['date_expires'] ) {
					if ( $data['date_expires'] < time() ) {
						$validator->setError('date_expires', 'Wrong date');
					}
				}
				else {
					$data['date_expires'] = null;
				}
			}
			
			if ( ! $data['status'] ) {
				$validator->setError('status', 'Required');
			}
			
			if ( ! strlen($data['username']) ) {
				$validator->setError('username', 'Required');
			}
			elseif ( $this->model->existsByUsername($data['username'], $data['user_id']) ) {
				$validator->setError('username', 'Username Already exists');
			}
			elseif ( strlen($data['username']) < 2 ) {//6
				$validator->setError('username', 'Username must be at least 6 characters long');
			}
				
			if (strlen($data['password']) && $data['password'] == $data['username']){
				$validator->setError('password', "Can't use username as password");
			}
			
			if ( strlen($data['email']) && Cubix_Application::isDomainBlacklisted($data['email']) ) {
				$validator->setError('email', 'Domain is blacklisted');
			}
			
			if ( strlen($data['email']) && $this->model->existsByEmail($data['email'], $data['user_id']) ) {
				$validator->setError('email', 'Already exists');
			}

			if ( strlen($data['is_suspicious']) && !strlen($data['suspicious_reason']) ) {
				$validator->setError('suspicious_reason', 'Required');
			}
			
			if ( strlen($data['is_trusted']) && !strlen($data['trusted_reason']) ) {
				$validator->setError('trusted_reason', 'Required');
			}
			
			if ( $validator->isValid() ) {

				$data_va = array(
					'id'					=> $data['id'],
					'user_id'				=> $data['user_id'],
					'application_id' 		=> $data['application_id'],
					//'sales_user_id'    		=> $data['sales_person_id'],
					'status' 	   			=> $data['status'],
					'country_id' 			=> $data['country_id'],
					'city_id'    			=> $data['city_id'],
					'email'      			=> $data['email'],
					'username' 				=> $data['username'],
					'password' 				=> $data['password'],
					'about_me' 				=> $data['about_me'],
					'pm_is_blocked'			=> $data['pm_is_blocked'],
					'im_is_blocked'			=> $data['im_is_blocked'],
					'newsletter'			=> $data['newsletter'],
					'escorts_watched_type'	=> $data['escorts_watched_type']
				);

				if ( Cubix_Application::getId() == APP_ED ){
					if (strlen($data['show_languages'])){
						$data_va['show_languages'] = 1;
					}else{
						$data_va['show_languages'] = 0;
					}

					$data_va['langs'] = $data['langs'];
				}

				if ( in_array(Cubix_Application::getId(), array(APP_6A, APP_EF)) ) {
					$data_va['rating'] = $data['rating'];
					$data_va['rating_changed_by_admin'] = 1;

					if ( ! $data['rating'] ) {
						$data_va['rating_changed_by_admin'] = 0;						
					}
				}

				if (strlen($data['is_suspicious']))
				{
					$data_va['is_suspicious'] = 1;
					$data_va['suspicious_reason'] = $data['suspicious_reason'];
				}
				else
				{
					$data_va['is_suspicious'] = 0;
					$data_va['suspicious_reason'] = '';
				}
				
				if (strlen($data['is_trusted']))
				{
					$data_va['is_trusted'] = 1;
					$data_va['trusted_reason'] = $data['trusted_reason'];
				}
				else
				{
					$data_va['is_trusted'] = 0;
					$data_va['trusted_reason'] = '';
				}
				if (strlen($data['member_comment']))
				{
					$data_va['member_comment'] = $data['member_comment'];
				}
				else
				{
					$data_va['member_comment'] = NULL;
				}
				if ( $can_set_premium ) {
					if ( $data['date_expires'] ) {
						$data_va = array_merge($data_va, array('date_expires' => date("Y-m-d H:i:s", $data['date_expires'])));
					}
					else {
						$data_va = array_merge($data_va, array('date_expires' => null));
					}
				}

				if ( in_array(Cubix_Application::getId(), array(APP_ED, APP_EG_CO_UK, APP_EG, APP_6C)) ) {
					$data_va = array_merge($data_va, array('is_system_member' => $data['is_system_member']));
				}

				if ( Cubix_Application::getId() == APP_EF && $bu_user->type == 'superadmin' ){
					$data_va = array_merge($data_va, array('cam_trusted' => $data['cam_trusted']));
				}
				$data_st = $this->model->getById($data_va['user_id']);
				
				$this->model->save(new Model_MemberItem($data_va));

				//newsletter email log
				$emails = array(
					'old' => $data_st->email,
					'new' => $data_va['email']
				);
				Cubix_Api::getInstance()->call('addNewlstterEmailLog', array($data_va['user_id'], 'member', 'edit', $emails));
				//

				/* Subscribe - Unsubscribe to newsman */
				if (Cubix_Application::getId() == APP_EF)
				{
					if ($data_st->newsletter != $data_va['newsletter'])
					{
						$ip = Cubix_Geoip::getIP();

						if ($data_va['newsletter'])
						{
							/* add */
							$m_n_i = new Cubix_Newsman_Ids();
							$n_ids = $m_n_i->get(Cubix_Application::getId());

							if (count($n_ids))
							{
								$list_id = reset(array_keys($n_ids));
								$conf = $m_n_i->getConf();

								try {
									$client = new Cubix_Newsman_Client($conf['user_id'], $conf['api_key']);
									$client->setCallType("rest");
									$client->setApiUrl("https://ssl.nzsrv.com/api");

									$subscriber_id = $client->subscriber->saveSubscribe($list_id, $data_va['email'], '', '', $ip, array());

									$segment_id = $n_ids[$list_id]['member'];

									if ($subscriber_id)
									{
										$client->subscriber->addToSegment(
											$subscriber_id, 
											$segment_id 
										);
									}
								}
								catch (Exception $e)
								{
									//var_dump($e);
								}
							}
							/**/
						}
						else
						{
							/* remove */
							$m_n_i = new Cubix_Newsman_Ids();
							$n_ids = $m_n_i->get(Cubix_Application::getId());

							if (count($n_ids))
							{
								$list_id = reset(array_keys($n_ids));
								$conf = $m_n_i->getConf();

								try {
									$client = new Cubix_Newsman_Client($conf['user_id'], $conf['api_key']);
									$client->setCallType("rest");
									$client->setApiUrl("https://ssl.nzsrv.com/api");
									
									$client->subscriber->saveUnsubscribe($list_id, $data_va['email'], $ip);
								}
								catch (Exception $e)
								{
									//var_dump($e);
								}
							}
							/**/
						}
					}
				}
				/**/
			}
			
			die(json_encode($validator->getStatus()));
		}
	}

	public function suspiciousAction()
	{
		if( ! $this->_request->isPost() )
		{
			$data_st = $this->model->getById($this->_request->id);
			$this->view->member = $data_st;
		}
		else
		{
			$data = new Cubix_Form_Data($this->_request);

			$params = array(
				'id' => 'int',
				'user_id' => 'int',
				'application_id' => 'int',
				'is_suspicious' => '',
				'suspicious_reason' => ''
			);

			$data->setFields($params);

			$data = $data->getData();

			$validator = new Cubix_Validator();

			if ( strlen($data['is_suspicious']) && !strlen($data['suspicious_reason']) ) {
				$validator->setError('suspicious_reason', 'Required');
			}

			if ( $validator->isValid() ) {

				$data_va = array(
					'id'					=> $data['id'],
					'user_id'				=> $data['user_id'],
					'application_id' 		=> $data['application_id']
				);

				if (strlen($data['is_suspicious']))
				{
					$data_va['is_suspicious'] = 1;
					$data_va['suspicious_reason'] = $data['suspicious_reason'];
				}
				else
				{
					$data_va['is_suspicious'] = 0;
					$data_va['suspicious_reason'] = '';
				}

				$this->model->saveSuspicious($data_va);
			}

			die(json_encode($validator->getStatus()));
		}
	}
	
	public function removeAction()
	{
		$ids = $this->_request->id;
		if(is_array($ids)){
			$user_ids = $ids;
		}
		else{
			$user_ids = array($ids);
		}
		foreach($user_ids as $user_id){
			$this->model->remove($user_id);
		}
		die;
	}
	public function toggleRecAction()
	{
		$member_id = intval($this->_request->member_id);
		if ( ! $member_id ) die;
		$this->model->toggleRec($member_id);
		die;
	}
	
	public function setStatusAction()
	{
		$user_ids = $this->_request->id;
		$status = intval($this->_request->status);
		$user_model = new Model_Users();
		foreach($user_ids as $user_id)
		{
			$user_model->setStatus($user_id,$status);
		}
		die;
	}

	public function markAsSystemMemberAction()
	{
		$user_ids = $this->_request->id;		
		$member_model = new Model_Members();
		foreach($user_ids as $user_id) {
			$member_model->markAsSystemMember($user_id);
		}
		die;
	}
	
	public function multiCommentAction()
	{
		$this->view->layout()->disableLayout();
		
		if( $this->_request->isPost() ){
			$data = new Cubix_Form_Data($this->_request);

			$params = array(
				'user_id' => 'arr-int',
				'is_suspicious' => '',
				'suspicious_reason' => ''
			);

			$data->setFields($params);

			$data = $data->getData();

			$validator = new Cubix_Validator();

			if ( strlen($data['is_suspicious']) && !strlen($data['suspicious_reason']) ) {
				$validator->setError('suspicious_reason', 'Required');
			}

			if ( $validator->isValid() ) {

				if (strlen($data['is_suspicious']))
				{
					$data_va['is_suspicious'] = 1;
					$data_va['suspicious_reason'] = $data['suspicious_reason'];
				}
				else
				{
					$data_va['is_suspicious'] = 0;
					$data_va['suspicious_reason'] = '';
				}
				foreach($data['user_id'] as $user_id)
				{
					$this->model->saveSuspiciousByUserId($data_va,$user_id);
				}
			}
			
			die(json_encode($validator->getStatus()));
				
		}
		else{
			$user_ids = $this->_request->id;
			$this->view->user_ids = is_array($user_ids) ? $user_ids : array($user_ids); 
			
		}
	}		
	
	public function ajaxGetMemberAction()
	{
		$user_id = trim($this->_getParam('user_id'));

		if ( ! strlen($user_id) ) {
			die(json_encode(array()));
		}
		
		$member = $this->model->getById($user_id);
		
		echo json_encode(array('id' => $member->user_id, 'username' => $member->username));
		die;
	}
	
	public function disableRevComAction()
	{
		$this->view->layout()->disableLayout();
		
		$this->view->act = $this->_request->act;
		$this->view->user_id = $this->_request->user_id;
		
		if ($this->_request->isPost())
		{
			$data = new Cubix_Form_Data($this->_request);

			$data->setFields(array(
				'user_id' => 'int',
				'act' => '',
				'disable_type' => 'int',
				'reason' => ''
			));

			$data = $data->getData();
			
			if (!$data['disable_type'])
				$data['disable_type'] = COMMENT_DISABLE_TYPE_INTERNAL;

			$validator = new Cubix_Validator();
			
			if ( ! $data['reason'] ) {
				$validator->setError('reason', 'Required');
			}
			
			if ( $validator->isValid() ) {
				switch ($data['act'])
				{
					case 'review':
						$this->model->disableReviews($data['user_id'], $data['disable_type'], $data['reason']);
						break;
					case 'comment':
						$this->model->disableComments($data['user_id'], $data['disable_type'], $data['reason']);
						break;
					case 'review_comment':
						$this->model->disableReviews($data['user_id'], $data['disable_type'], $data['reason']);
						$this->model->disableComments($data['user_id'], $data['disable_type'], $data['reason']);
						break;
				}
			}
			
			die(json_encode($validator->getStatus()));
		}
	}

	public function followListAction(){
		$this->view->memberId = $this->_request->id;

	}

	public function getFollowersAction(){
		$memberId = $this->_request->member_id;
		
		$data = $this->model->getFollowerList($memberId);
			
		die(json_encode($data));
	}

	public function unFollowAction(){
		$followId = $this->_request->id;

		$this->model->unFollow($followId);
	}
}
