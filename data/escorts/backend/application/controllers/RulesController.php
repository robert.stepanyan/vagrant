<?php

class RulesController extends Zend_Controller_Action
{
	protected $model;

	public function init()
	{
		if (Cubix_Application::getId() != APP_EF)
			die('permission denied');
		
		$this->model = new Model_System_Rules();
	}

	public function indexAction()
	{

	}

	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'title' => $req->title, 
			'question_en' => $req->question_en, 
			'answer_en' => $req->answer_en, 
			'question_it' => $req->question_it, 
			'answer_it' => $req->answer_it, 
			'application_id' => $req->application_id 
		);
		
		$data = $this->model->getAll(
			$req->page,
			$req->per_page, 
			$filter,
			$req->sort_field,
			$req->sort_dir,
			$count
		);
		
		foreach ($data as $k => $item)
		{
			if (strlen($item->answer_en) > 150)
			{
				$data[$k]->answer_en = mb_substr($item->answer_en, 0, 150) . ' ...';
			}
			
			if (strlen($item->answer_it) > 150)
			{
				$data[$k]->answer_it = mb_substr($item->answer_it, 0, 150) . ' ...';
			}
		}
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

	public function viewAction()
	{
		$this->view->item = $item = $this->model->get($this->_getParam('id'));
	}
}
