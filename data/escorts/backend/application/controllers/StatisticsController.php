<?php

/**
 * Created by SceonDev.
 * User: Zhora
 * Date: 08.08.2017
 * Time: 10:49
 */
class StatisticsController extends Zend_Controller_Action
{
    private $model;
    private $app_id;

    public function init()
    {
        $this->model = new Model_Statistics();
        $this->app_id = Cubix_Application::getId();
    }

    public function indexAction()
    {
        if (!in_array(Cubix_Application::getId(),array(APP_ED,APP_EF)))
        {
            return $this->_redirect($this->view->baseUrl());
        }

    }

    public function dataAction()
    {
        set_time_limit(0);
        $page = $this->_getParam('page');
        $per_page = $this->_getParam('per_page');
        $sort_field = $this->_getParam('sort_field');
        $sort_dir = $this->_getParam('sort_dir');


        $req = $this->_request;


        $filter = array(
            'escort_id' => $req->escort_id,
            'agency_id' => $req->agency_id,
            'date_from' => $req->date_from,
            'date_to' => $req->date_to,
            'user_type' => $req->user_type,
            'link' => $req->link
        );


        $data = $this->model->getAll($page, $per_page, $sort_field, $sort_dir, $filter, $count);

        echo json_encode(array('data' => $data, 'count' => $count));
        die;
    }

}