<?php

class IssuesController extends Zend_Controller_Action {

    /**
     * @var Model_Issues
     */
    public $model;

	public function init()
	{
		$this->model = new Model_Issues();
		$this->document_model = new Cubix_Documents();
		$this->user = Zend_Auth::getInstance()->getIdentity();
	}
	
	public function indexAction() 
	{
        $this->view->super_admin = false;

        if($this->user->type == 'superadmin'){
            $this->view->super_admin = true;
        }

		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('default_issues_data');
		$this->view->filter_params = $ses_filter->filter_params;
		/* FILTER PARAMS */
	}
	
	public function dataAction()
	{

		$req = $this->_request;

		$filter = array(
			'u.username' => $req->username,
			'st.id' => $req->ticket_number,
			'e.id' => $req->escort_id,
//			'u.application_id' => $req->application_id,
			/*'u.email' => $req->email,
			'u.last_ip' => $req->last_ip,*/
			'st.status' => $req->status,
			'st.application_id' => $req->app_id,
			'st.backend_user_id' =>$req->sales_user_id,
			'st.status_progress' => $req->status_progress,
			
			'st.disabled_in_pa' => $req->disabled_in_pa
		);

		if(Cubix_Application::getId() == APP_EF && $req->can_close_issue){
            $filter['can_close_issue'] = $req->can_close_issue;
        }

		// Follow up filter for escortdirectory
		if(Cubix_Application::getId() == APP_ED){
		    if($req->filter_follow_up == 1){
                $filter['st.to_follow_up'] = 1;
            }elseif($req->filter_follow_up == 2){
                $filter['st.to_follow_up'] = 0;
            }
        }

        $bu_user = Zend_Auth::getInstance()->getIdentity();

        if(Cubix_Application::getId() == APP_ED && $bu_user->type === "sales manager") {
            $filter['st.backend_user_id'] = $bu_user->id;
        }

        $data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);

        foreach ( $data as $i => $item ) {
            if ( $data[$i]->status == Model_Issues::STATUS_TICKET_CLOSED )
				$data[$i]->status = 'closed';
			else if ( $data[$i]->status == Model_Issues::STATUS_TICKET_OPENED )
				$data[$i]->status = 'opened';
			
			if ( isset($data[$i]->disabled_in_pa) && $data[$i]->disabled_in_pa ) {
				$data[$i]->status = 'disabled';
			}
			if($item->from_backend_user){
				$bu_model = new Model_BOUsers();
				$data[$i]->sent_by = $bu_model->get($item->from_backend_user)->first_name;
			}
			else{
				$data[$i]->sent_by = '';
			}
			
			$data[$i]->message = mb_substr($data[$i]->message, 0, 50, "UTF-8") . " ...";
			$data[$i]->subject = mb_substr($data[$i]->subject, 0, 20, "UTF-8") . " ...";
		}
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}


	public function setFollowUpAction(){
        $req = $this->_request;
        $id = intval($req->id);
        $follow_up =  intval($req->followup);
        $data = array('id' => $id, 'to_follow_up' => $follow_up);
        $this->model->setFollowUp($data);
        die;
    }

	public function setStatusAction()
	{
		$req = $this->_request;

		$id = intval($req->id);
		$user_id = intval($req->user_id);
		$status = intval($req->status);

		if( Cubix_Application::getId() == APP_EF && $this->user->type != 'superadmin'){
            $last_comment =  $this->model->getLastComments($req->id);
			if($last_comment->comment_date_diff < 2 || is_null($last_comment->bu_user_type)){
                die('nope');
            }
		}
		
		$data = array('id' => $id, 'status' => $status, 'resolve_date'=> new Zend_Db_Expr('NOW()'));

		$user = new Model_Users();
		$user = $user->get($user_id);

		if ( $status == Model_Issues::STATUS_TICKET_CLOSED ) {
			
			if ( strlen($user->email) ) {
				if(Cubix_Application::getId() == APP_ED){
					 Cubix_Email::sendTemplate('ticket_closed_v1', $user->email, array(
						'ticket_id' => $this->_request->id,
						'backend_user' => Zend_Auth::getInstance()->getIdentity()->username,
						'username' => $user->username,
						'message' => $this->_request->comment
					));  
				}elseif(Cubix_Application::getId() == APP_EG_CO_UK){
					Cubix_Email::sendTemplate('support_ticket_closed_v2', $user->email, array(
						'ticket_id' => $id,
						'username' => $user->username,
					));
				}else{
                    Cubix_Email::sendTemplate('support_ticket_comment_added', $user->email, array(
                        'ticket_id' => $this->_request->id,
                        'message' => $this->_request->comment,
                        'username' => $user->username,
                    ));
                }
			}
		}

		$this->model->setStatus($data);
		die;
	}
	
	public function setPaStatusAction()
	{
		$req = $this->_request;

		$id = intval($req->id);
		$user_id = intval($req->user_id);
		$status = intval($req->status);
		$pa_status = intval($req->ps_status);

		if( Cubix_Application::getId() == APP_EF && $this->user->type != 'superadmin'){
            $last_comment =  $this->model->getLastComments($req->id);
			if($last_comment->comment_date_diff < 2 || is_null($last_comment->bu_user_type)){
                die('nope');
            }
		}
		
		$data = array('id' => $id, 'status' => $status, 'disabled_in_pa' => $pa_status, 'resolve_date'=> new Zend_Db_Expr('NOW()'));

		if ( $status == Model_Issues::STATUS_TICKET_OPENED ) {
			$data['resolve_date'] = null;
		}
		
		$user = new Model_Users();
		$user = $user->get($user_id);

		if ( $status == Model_Issues::STATUS_TICKET_CLOSED ) {
			
			if ( strlen($user->email) ) {
				$ret = Cubix_Email::sendTemplate('support_ticket_closed', $user->email, array(
					'username' => $user->username,
					'ticket_id' => $id
				));
				
			}
		}

		$this->model->setStatus($data);
		die;
	}
	
	public function createTicketAction()
	{	
		
		/* get issue templates */
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		if (!in_array($bu_user->type, array('superadmin', 'admin')))
			$backend_user_id = $bu_user->id;
		else
			$backend_user_id = null;
		
		$modelIT = new Model_System_IssueTemplates();
		$this->view->issue_templates = $modelIT->getByBO($backend_user_id);
		if($this->_request->isPost() )
		{
			$data = new Cubix_Form_Data($this->_request);

			$fields = array(
                'user_type_id' => 'int',
                'user_type' => '',
                'subject' => '',
                'comment' => ''
            );

			if (Cubix_Application::getId() == APP_ED)
            {
                $fields['comment'] = 'arr';
            }

			$data->setFields($fields);
			
			$data = $data->getData();
			
			$validator = new Cubix_Validator();
			
			if ( ! $data['user_type_id'] ) {
				$validator->setError('user_type_id', 'Required');
			}

			if ( ! $data['user_type'] ) {
				$validator->setError('user_type', 'Required');
			}
			
			if ( ! strlen($data['subject']) ) {
				$validator->setError('subject', 'Required');
			}

            if (Cubix_Application::getId() == APP_ED)
            {
                $commValidatorArray = array();
                foreach ($data['comment'] as $item)
                {
                    if (strlen($item))
                    {
                        $commValidatorArray[] = true;
                        $comment = $item;
                    }else{
                        $commValidatorArray[] = false;
                    }

                }

                if ( !in_array(true,$commValidatorArray) ) {
                    $validator->setError('comment', 'Required');
                }

            }else{
                if ( ! strlen($data['comment']) ) {
                    $validator->setError('comment', 'Required');
                }
            }

			//$escort_model = new Model_EscortsV2();
			//$user_id = $escort_model->getUserId($data['escort_id']);
			$user_id = $data['user_type_id'];
			if ( $data['user_type'] != 'members' ) {
				$user_id = Model_Issues::getUserIdByUserTypeAndId($data['user_type'], $data['user_type_id']);
			}
			if ( ! $user_id ) {
				$validator->setError('user_type_id', 'Wrong ID');
			}

			$model_user = new Model_Users();
			$user = $model_user->get($user_id);

			if ( $validator->isValid() ) {				
				
				$data_ticket = array(
					'user_id' => $user_id,
					'subject' => $data['subject'],
					'message' => $comment? $comment : $data['comment'],
					'status' => Model_Issues::STATUS_TICKET_OPENED,
					'application_id' => Cubix_Application::getId(),
					'backend_user_id' => $user->backend_user,
					'status_progress' => Model_Issues::STATUS_ADMIN_SENT,
					'from_backend_user' => $bu_user->id
				);
				
				$this->model->insert($data_ticket);
			}
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function editAction()
	{	
		$issue = $this->model->get($this->_request->id);
		$this->view->issue = $issue['issue'];

        $this->view->can_close_issue = true;
		
        if( Cubix_Application::getId() == APP_EF && $this->user->type != 'superadmin'){
            $last_comment =  $this->model->getLastComments($this->_request->id);

            if($last_comment->comment_date_diff < 2 || is_null($last_comment->bu_user_type)){
                $this->view->can_close_issue = false;
            }
		}
		
        if(Cubix_Application::getId() == APP_ED) {
            $this->view->userIssuesHistory = $this->model->getUserIssuesHistory($issue['issue']['user_id']);
        }

		if($issue['attach']){
			foreach($issue['attach'] as &$attach){
				$url = $this->document_model->getUrl(new Cubix_Images_Entry(array(
						'application_id' => Cubix_Application::getId(),
						'catalog_id' => 'attached',
						'hash' => $attach->hash,
						'ext' => $attach->ext
					)));
				$attach->file_url = $url;
			}
		}
		$this->view->issue_attachment = $issue['attach'];
		$this->view->sale_person = is_null($issue->backend_user_id) ? $issue->sales_user_id : $issue->backend_user_id;
		$mod_type = array('sales clerk','sales manager','admin','superadmin');
		$moderators = new Model_Moderators();
		$this->view->sale_persons = $moderators->getByType($mod_type, $this->_request->application_id);

		$this->ajaxCommentsListAction();
		$this->view->layout()->enableLayout();
		
		/* get issue templates */
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		if (!in_array($bu_user->type, array('superadmin', 'admin')))
			$backend_user_id = $bu_user->id;
		else
			$backend_user_id = null;
		
		$modelIT = new Model_System_IssueTemplates();
		$this->view->issue_templates = $modelIT->getByBO($backend_user_id);
	}

	public function ajaxCommentsListAction()
	{
		$this->view->layout()->disableLayout();
		
		$comments = $this->model->getComments($this->_request->id);
		foreach($comments as &$comment){
			if($comment["attached_files"]){
				foreach($comment["attached_files"] as &$attach){
					$url = $this->document_model->getUrl(new Cubix_Images_Entry(array(
							'application_id' => Cubix_Application::getId(),
							'catalog_id' => 'attached',
							'hash' => $attach->hash,
							'ext' => $attach->ext
						)));
					$attach->file_url = $url;
				}
			}
		}
		$this->view->comments = $comments;
	}

	public function ajaxAddCommentAction()
	{
		$this->view->layout()->disableLayout();

		$validator = new Cubix_Validator();

		if ( ! $this->_request->comment ) {
            $validator->setError('comment', 'Required');
        }

        if (Cubix_Application::getId() == APP_ED && is_array($this->_request->comment))
        {
            foreach ($this->_request->comment as $item)
            {
                if (strlen($item))
                {
                    $comment =  $item;
                    break;
                }
            }
        }


        if ( $validator->isValid() ) {

			$data = array(
				'ticket_id' => $this->_request->id,
				'comment' => $comment ? $comment: $this->_request->comment,
				'backend_user_id' => Zend_Auth::getInstance()->getIdentity()->id,
				'backend_user_name' => Zend_Auth::getInstance()->getIdentity()->username
			);
			
			$comment_id = $this->model->addComment($data);
			$attaches = is_array($this->_request->attach) ? $this->_request->attach : array($this->_request->attach);
			
			if($attaches){
				$data = array(
					'ticket_comment_id'=> $comment_id,
					'ticket_id'=> $this->_request->id
				);
				foreach($attaches as $attach_id){
					
					$this->model->updateAttached($data, $attach_id);
				}
			}
			$progress_status = array('status_progress' => 3, 'is_read' => 0);
			if(in_array(Cubix_Application::getId(), array(APP_ED)))
            {
                $progress_status['last_updated_date'] = new Zend_Db_Expr('NOW()');
            }
			$this->model->update($this->_request->id,$progress_status);
			$user_id = intval($this->_request->user_id);
			$user = new Model_Users();
			$user = $user->get($user_id);

			if ( strlen($user->email) ) {
				if( Cubix_Application::getId() == APP_ED ){
					if( $user->user_type == 'member'){
						$newletter_template = 'ticket_answered_member_v1';
					}elseif($user->user_type == 'escort'){
						$newletter_template = 'ticket_answered_escort_v1';
					}else{
						$newletter_template = 'ticket_answered_agency_v1';
					}
					
					  Cubix_Email::sendTemplate($newletter_template, $user->email, array(
						'ticket_id' => $this->_request->id,
						'backend_user' => Zend_Auth::getInstance()->getIdentity()->username,
						'showname' => ($user->showname? $user->showname : $user->username),
						'username' => $user->username,
						'message' => $comment ? $comment: $this->_request->comment,
					));  
					
					 /* Cubix_Email::sendTemplate('support_ticket_comment_added', $user->email, array(
						'ticket_id' => $this->_request->id,
						'message' => $this->_request->comment
					));  */
				}elseif( in_array(Cubix_Application::getId(), array(APP_EG_CO_UK, APP_6B)) ){
					if( $user->user_type == 'member'){
						$newletter_template = 'member_admin_answered';
					}elseif($user->user_type == 'escort'){
						$newletter_template = 'escort_admin_reply';
					}else{
						$newletter_template = 'ticket_answered_agency_v1';
					}
					$lng = Cubix_Application::getId() == APP_6B ? 'pt': null;
					Cubix_Email::sendTemplate($newletter_template, $user->email, array(
						'ticket_id' => $this->_request->id,
						'backend_user' => Zend_Auth::getInstance()->getIdentity()->username,
						'showname' => ($user->showname? $user->showname : $user->username),
						'username' => $user->username,
						'message' => $this->_request->comment
					), null, null, $lng);
				}
				else{
					Cubix_Email::sendTemplate('support_ticket_comment_added', $user->email, array(
						'ticket_id' => $this->_request->id,
						'message' => $this->_request->comment
					));
				}
			}
		}

		die(json_encode($validator->getStatus()));
	}
	
	public function removeAction()
	{
		$id = intval($this->_request->id);

		$this->model->remove($id);
		die;
	}

	public function ajaxRemoveCommentAction()
	{
		$id = intval($this->_request->id);

		$this->model->removeComment($id);
		die;
	}

	public function ajaxChangeSalesPersonAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNorender(true);
		$id = intval($this->_request->id);
		$backend_user_id = intval($this->_request->backend_user_id);
		$this->model->changeBackendUserId($id, $backend_user_id);
        $logData = array(
            'event_type' => SUPPORT_TICKET_SALES_CHANGE,
            'ticket_id' => $id,
            'bu_executor_id' => $this->user->id,
            'comment' => 'Backend user '.$this->user->username.' assigned ticket: '.$id.' to another backend user with ID: '.$backend_user_id
        );
        $this->model->log($logData);
		$bu_model = new Model_BOUsers();
		$bu_email = $bu_model->getEmailById($backend_user_id);
		Cubix_Email::sendTemplate('issue_change_alert',$bu_email, array('issue_number' => $id ));
		die(json_encode(array('status' => 'success')));
	}
	
	public function setProgressStatusAction()
	{
		$req = $this->_request;

		$id = intval($req->id);
		$status = intval($req->status);

		$data = array('status_progress' => $status);

		$this->model->update($id,$data);
		die;
	}
	
	public function addAttachedFilesAction()
	{
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        try {
            $response = self::HTML5_upload();
            $docs = new Cubix_Documents();
            $docs->allowed_ext = array_merge($docs->allowed_ext, array('webm', 'avi', 'mp4')); 
            $error = false;
            $extension = end(explode(".", $response['name']));
            $extension = strtolower($extension);

            if (!in_array($extension, $docs->allowed_ext))
            {
                $error = 'Not allowed file extension.';
            }

            if ($error) {

                $return = array(
                    'status' => '0',
                    'error' => $error
                );

            } elseif ($response['error'] == 0 && $response['finish']) {

                // Save on remote storage
                $doc = $docs->save($response['tmp_name'], 'attached', $escort->application_id, $extension);

                $attach_arr = array(
                    'file_name' => $response['name'],
                    'hash' => $doc['hash'],
                    'ext' => $doc['ext'],

                );

                $attached_id = $this->model->addAttached($attach_arr);
                $return = array(
                    'status' => '1',
                    'id' => $attached_id
                );
            } else {
                $return = array(
                    'status' => '0',
                    'error' => 'Upload error .'
                );
            }

            echo json_encode($return);
        } catch (Exception $e) {
            echo json_encode(array(
                'status' => 0,
                'error' => $e->getMessage()
            ));
        }
	}

    public static function HTML5_upload()
    {
        $headers = array();
        $headers['Content-Length'] = $_SERVER['CONTENT_LENGTH'];
        $headers['X-File-Id'] = $_SERVER['HTTP_X_FILE_ID'];
        $headers['X-File-Name'] = $_SERVER['HTTP_X_FILE_NAME'];
        $headers['X-File-Resume'] = $_SERVER['HTTP_X_FILE_RESUME'];
        $headers['X-File-Size'] = $_SERVER['HTTP_X_FILE_SIZE'];

        $response = array();
        $response['id'] = $headers['X-File-Id'];
        $response['name'] = basename($headers['X-File-Name']); 	// Basename for security issues
        $response['size'] = $headers['Content-Length'];
        $response['error'] = UPLOAD_ERR_OK;
        $response['finish'] = FALSE;

        // Is resume?
        $flag = (bool)$headers['X-File-Resume'] ? FILE_APPEND : 0;
        $filename = $response['id'].'_'.$response['name'];
        $response['upload_name'] = $filename;

        // Write file
        $file = sys_get_temp_dir().DIRECTORY_SEPARATOR.$response['upload_name'];

        if (file_put_contents($file, file_get_contents('php://input'), $flag) === FALSE){
            $response['error'] = UPLOAD_ERR_CANT_WRITE;
        }
        else
        {
            $response['file_size'] = filesize($file);
            $response['X-File-Size'] = $headers['X-File-Size'];

            if (filesize($file) == $headers['X-File-Size'])
            {
                $response['tmp_name'] = $file;
                $response['finish'] = TRUE;
            }
        }

        return $response;
    }
}
