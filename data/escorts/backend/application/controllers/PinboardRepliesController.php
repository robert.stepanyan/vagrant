<?php

class PinboardRepliesController extends Zend_Controller_Action 
{
	public function init()
	{
		$this->model = new Model_Pinboard();
	}
	
	public function indexAction() 
	{
		
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'id' => $req->id,
			'post_id' => $req->post_id,
			'user_type' => $req->user_type,
			'application_id' => $req->application_id,
			'escort_id' => $req->escort_id,
			'showname' => $req->showname,
			'agency_name' => $req->agency_name,
			'username' => $req->username
		);
		
		$data = $this->model->getAllReplies(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
				
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
	
	public function removeAction()
	{
		$id = intval($this->_request->id);
		$this->model->removeReply($id);
		
		die;
	}
	
	public function editAction()
	{
		if( ! $this->_request->isPost() )
		{
			$this->view->data = $this->model->getReply($this->_request->id);
			$this->view->post = $this->model->getPostByReply($this->_request->id);
		}
		else 
		{
			$data = new Cubix_Form_Data($this->_request);
						
			$data->setFields(array(
				'id' => 'int',
				'reply' => ''
			));
			
			$data = $data->getData();
						
			$validator = new Cubix_Validator();
									
			if ( !strlen($data['reply']) ) {
				$validator->setError('reply', 'Required');
			}
			
			if ( $validator->isValid() ) {
				$this->model->saveReply($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
}
