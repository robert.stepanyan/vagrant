<?php

class EscortTemplatesController extends Zend_Controller_Action {
	
	public function init()
	{
		$this->_model = new Model_EscortTemplates();
	}
	
	public function indexAction()
	{
		
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array('application_id' => $req->application_id );
		
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		if (!in_array($bu_user->type, array('superadmin', 'admin')))
			$filter['sales_user_id'] = $bu_user->id;
		
		$data = $this->_model->getAll(
			$req->page,
			$this->_request->per_page, 
			$filter, 
			$req->sort_field,
			$req->sort_dir,
			$count
		);
		
		die(json_encode(array('data' => $data, 'count' => $count)));
	}
	
	public function createAction()
	{
		$this->view->layout()->disableLayout();
		if ( $this->_request->isPost() )
		{
			$title = trim($this->_request->getParam('title'));
			$id = preg_replace('/\s/','_',$title );
			$body = Cubix_I18n::getValues($this->_request->getParams(), 'body', '');
			$bu_user = Zend_Auth::getInstance()->getIdentity();
			$app_id = $this->_getParam('application_id');
			$validator = new Cubix_Validator();

			if ( ! strlen($title) ) {
				$validator->setError('title', 'This field is required');
			}
			else {
				$item = $this->_model->getById($id, $app_id);
				
				if ( $item && $item->id == $id ) {
					$validator->setError('title', 'Choose another title, this one already exists');
				}
			}

			if ( $validator->isValid() ) {
				
				$data = array();
				$langs = Cubix_I18n::getLangs(true);

				foreach ($langs as $lang)
				{
					$data['id'] = $id;
					$data['title'] = $title;
					$data['application_id'] = $app_id;
					$data['body'] = $body['body_' . $lang];
					$data['lang_id'] = $lang;
					$data['sales_user_id'] = $bu_user->id;
					
					$this->_model->insert($data);
				}
				
				
			}

			die(json_encode($validator->getStatus()));
		}
	}
	
	public function editAction()
	{
		$this->view->layout()->disableLayout();
		
		$id = $this->_request->id;
		$app_id = $this->_request->application_id;
		$template = $this->_model->get($id, $app_id);
		$langs = Cubix_I18n::getLangsByAppId($app_id);
		
		$data = array();
		foreach ($langs as $lang)
		{
			foreach ($template as $t)
			{
				if ($lang->lang_id == $t->lang_id)
				{
					$data[$lang->lang_id]['id'] = $t->id;
					$data[$lang->lang_id]['title'] = $t->title;
					$data[$lang->lang_id]['body'] = $t->body;
					$data[$lang->lang_id]['lang_id'] = $t->lang_id;
				}
			}
		}
		$this->view->template = $data;
		$this->view->id = $id;
		if ( $this->_request->isPost() )
		{
			$id = $this->_request->getParam('id');
			$body = Cubix_I18n::getValues($this->_request->getParams(), 'body', '');
			$app_id = $this->_getParam('application_id');

			$data = array();
			$langs = Cubix_I18n::getLangs(true);

			foreach ($langs as $lang)
			{
				$array = array('body' => $body['body_' . $lang], 'id'=> $id, 'application_id' => $app_id, 'lang_id' => $lang );
				$this->_model->update($data, $array);
			}

			$validator = new Cubix_Validator();
			die(json_encode($validator->getStatus()));
			
		}
	}
	
	public function deleteAction()
	{
		$id = $this->_request->id;
		if ( ! $id ) die;
		$this->_model->remove($id);

		die;
	}
}
