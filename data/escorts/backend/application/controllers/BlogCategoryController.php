<?php

class BlogCategoryController extends Zend_Controller_Action 
{
	public function init()
	{
		$this->model = new Model_BlogCategory();
	}
	
	public function indexAction() 
	{
		
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'id' => $req->id,
			'name' => $req->name
		);
		
		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		$DEFINITIONS = Zend_Registry::get('defines');


		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
	
	public function createAction()
	{				
		$req = $this->_request;

		if ( $req->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);

			$params = array(
				'layout' => 'int-nz',
			);

			$langs = Cubix_I18n::getLangs(true);

			foreach ($langs as $lng)
			{
				$params['title_' . $lng] = '';
			}


			$data->setFields($params);
			
			$data = $data->getData();
			
			$validator = new Cubix_Validator();
			
			if ( ! $data['title_en'] ) {
				$validator->setError('title_en', 'Required');
			}
			

			foreach ($langs as $lng)
			{
				if ($lng == 'en') continue;

				if ($data['title_' . $lng])
				{
					if (!$data['title_' . $lng])
					{
						$validator->setError('title_' . $lng, 'Required');
					}
				}
			}
			
			if ( $validator->isValid() ) {
				$data['slug'] = Cubix_Utils::makeSlug($data['title_en']);

				$this->model->save($data);				
			}
			
			die(json_encode($validator->getStatus()));
		}
	}

	public function editAction()
	{
		if ( !$this->_request->isPost() )
		{
			$this->view->data = $this->model->get($this->_request->id);
		}
		else
		{
			$data = new Cubix_Form_Data($this->_request);

			$params = array(
				'id' => 'int-nz',
				'layout' => 'int-nz',
				'status' => 'int',
			);

			$langs = Cubix_I18n::getLangs(true);

			foreach ($langs as $lng)
			{
				$params['title_' . $lng] = '';
			}


			$data->setFields($params);
			
			$data = $data->getData();
			
			$validator = new Cubix_Validator();
			
			if ( ! $data['title_en'] ) {
				$validator->setError('title_en', 'Required');
			}
			

			foreach ($langs as $lng)
			{
				if ($lng == 'en') continue;

				if ($data['title_' . $lng])
				{
					if (!$data['title_' . $lng])
					{
						$validator->setError('title_' . $lng, 'Required');
					}
				}
			}
			
			if ( $validator->isValid() ) {
				$data['slug'] = Cubix_Utils::makeSlug($data['title_en']);

				$this->model->save($data);				
			}
			
			die(json_encode($validator->getStatus()));
		}
	}

	public function reorderAction(){
		$req = $this->_request;

		$data = $this->model->getList();

		$this->view->data = $data;

		if ( $req->isPost() ) {
			$ids = $req->ids;
			$i = count($ids);
			foreach ($ids as $catId) {
				$this->model->updatePostion($catId,$i);
				$i--;
			}
			exit();
		}	


	}

	public function removeAction()
	{
		$id = intval($this->_request->id);
		$this->model->remove($id);
		
		die;
	}
	
	public function toggleAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->model->toggle($id);
		
		die;
	}
}
