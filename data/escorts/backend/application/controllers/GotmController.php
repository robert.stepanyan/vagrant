<?php

class GotmController extends Zend_Controller_Action {

	public function init()
	{
		$user = Zend_Auth::getInstance()->getIdentity();
		$allow = false;
		$apps = array(APP_EF, APP_6A, APP_6C,APP_EG, APP_EG_CO_UK, APP_BL);
		
		if ($user->type == 'superadmin' && in_array(Cubix_Application::getId(), $apps))
			$allow = true;
		
		if ($user->type == 'admin' && Cubix_Application::getId() == APP_EF) //69 - admin02
			$allow = true;
		elseif($user->username == 'tom' && Cubix_Application::getId() == APP_BL)
			$allow = true;
		if (!$allow)
			die('Permission denied');
		
		$this->model = new Model_Gotm();
	}
	
	public function indexAction() 
	{
		
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'showname' => $req->showname,
			'id' => $req->id,
			'application_id' => $req->application_id
		);
		
		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		$DEFINITIONS = Zend_Registry::get('defines');
				
		foreach ( $data as $i => $item ) 
		{
			if ($item->voted_members)
			{
				$arr = unserialize($item->voted_members);
				ksort($arr);
				$str = implode(',', $arr);
				
				if (strlen($str) > 0)
				{
					$users = $this->model->getUsersByIds($str);
					
					if ($users)
					{
						$arr_u = array();
						$c = 0;
						
						foreach ($users as $u)
						{
							if ($u->status == USER_STATUS_DISABLED)
								$arr_u[] = '<a href="#" rel="' . $u->id . '" class="p-member red">' . $u->username . '</a>';
							elseif($u->is_suspicious){
								$arr_u[] = '<a href="#" rel="' . $u->id . '" class="p-member pink">' . $u->username . '</a>';
							}
							else
							{
								$arr_u[] = '<a href="#" rel="' . $u->id . '" class="p-member">' . $u->username . '</a>';
								$c++;
							}
						}
						
						$data[$i]->voted_members = implode(', ', $arr_u);
						$data[$i]->real_count = $data[$i]->votes_count; // $c replaced for https://sceonteam.atlassian.net/browse/BEN-78
					}
				} 
			}
			
			$data[$i]->gender = $DEFINITIONS['gender_options'][$item->gender];
		}
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
	
	public function editAction()
	{
		if( ! $this->_request->isPost() )
		{
			$id = $this->_request->id;
						
			$this->view->data = $this->model->get($id);
		}
		else 
		{
			$data = new Cubix_Form_Data($this->_request);
						
			$data->setFields(array(
				'id' => 'int',
				'votes_count' => ''
			));
			
			$data = $data->getData();
						
			$validator = new Cubix_Validator();
			
			if (!is_numeric($data['votes_count']) ) {
				$validator->setError('votes_count', 'Must be number');
			}
			
			if ( $validator->isValid() ) {
				$data['votes_count'] = intval($data['votes_count']);
				$this->model->save($data);
			}
						
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function winnerAction()
	{
		if( ! $this->_request->isPost() )
		{
			$this->view->winner_id = $this->model->getWinnerId();
			
		}
		else 
		{
			
			$winner_id = trim($this->_request->winner_id);			
			$validator = new Cubix_Validator();
			
			if (!is_numeric($winner_id) && $winner_id !="" ) {
				$validator->setError('winner_id', 'Must be number');
			}
			
			if ( $validator->isValid() ) {
				
				$this->model->saveWinnerId($winner_id);
			}
						
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function getWinnerIdAction()
	{
		$this->view->winner_id = $this->model->getWinnerId();
	}
	
	public function historyAction()
	{
		
	}
	
	public function historyListAction()
	{
		$req = $this->_request;
		$filter = array(
			'application_id' => $req->application_id
		);
		
		$data = $this->model->getWinners(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		foreach ( $data as $i => $item ) 
		{
			if ($item->voted_members)
			{
				$arr = unserialize($item->voted_members);
				ksort($arr);
				$str = implode(',', $arr);
				
				if (strlen($str) > 0)
				{
					$users = $this->model->getUsersByIds($str);
					
					if ($users)
					{
						$arr_u = array();
						
						foreach ($users as $u)
						{
							if ($u->status == USER_STATUS_DISABLED)
								$arr_u[] = '<a href="#" rel="' . $u->id . '" class="p-member red">' . $u->username . '</a>';
							else
								$arr_u[] = '<a href="#" rel="' . $u->id . '" class="p-member">' . $u->username . '</a>';
						}
						
						$data[$i]->voted_members = implode(', ', $arr_u);
						
					}
				}
			}
		}
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
		
		die(json_encode($this->view->data));
	}		
}
