<?php

function replace___($m)
{
	$id = strtolower($m[1]);
	$id = preg_replace('#[^a-z0-9]#', ' ', $id);
	while ( strpos($id, '  ') !== false ) {
		$id = str_replace('  ', ' ', $id);
	}
	$id = 'service_' . trim(str_replace(' ', '_', $id), '_');

	return 'Cubix_I18n::translate(\'' . $id . '\')';
}

class EscortsV2Controller extends Zend_Controller_Action
{
	/**
	 * @var Model_EscortsV2
	 */
	protected $model;

	/**
	 * @var Model_UserItem
	 */
	protected $user;
	protected $importId;

	public function init()
	{

		/*error_reporting(E_ALL);
		ini_set('display_errors', '1');
		var_dump(geoip_record_by_name('81.253.17.88'));*/
		//		Cubix_SyncNotifier::markAsDone(array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26));
		//		die;

		// $db = Zend_Registry::get('db');

		// $defines = Zend_Registry::get('defines');
		// $services = $defines['services_flat'];

		// $src = file_get_contents('../../library/Cubix/services.php');
		// $src = preg_replace_callback("#Cubix_I18n::translate\('([^']+)'\)#", 'replace___', $src);
		// file_put_contents('../../library/Cubix/services.php', $src);

		$this->model = new Model_EscortsV2();
		$this->user = Zend_Auth::getInstance()->getIdentity();

		$this->view->super = $this->user->type=='superadmin'?1:0;

		$this->measure_units = $this->view->measure_units = Cubix_Application::getById($this->_getParam('application_id'))->measure_units;
	}
	
	public function indexAction()
	{
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('default_escorts-v2_data');
		$this->view->filter_params = $ses_filter->filter_params;
		/* FILTER PARAMS */
	}
	
	public function changeStatusAction()
	{
		
	}

	public function dataAction()
	{
		$req = $this->_request;

		$bu_user = Zend_Auth::getInstance()->getIdentity();

		// if ( in_array(Cubix_Application::getId(), array(APP_ED, APP_EG_CO_UK)) && $bu_user->type == 'call center' ) {
		// 	$req->setParam('sales_user_id', $bu_user->id);
		// }

		if ( $bu_user->type == 'sales manager' ) {
			//$req->setParam('sales_user_id', $bu_user->id);
		}
		else if ($bu_user->type == 'admin') {
			$req->setParam('application_id', $bu_user->application_id);
		}
		
		/*$phone = preg_replace('/^\+/', '00', $req->phone);*/
		$phone = preg_replace('/[^0-9]/', '', $req->phone);
		$phone = preg_replace('/^0/', '', $phone);

		if(in_array(Cubix_Application::getId(), array(APP_ED))){
           $filter_phone_prefix =  $req->filter_phone_prefix;
        }else{
           $filter_phone_prefix =  $req->phone_prefix;
        }

		$filter = array(
			// Personal
			'e.id = ?' => $req->escort_id,
			/*'u.application_id = ?' => (int) $req->application_id,*/
			'e.showname' => $req->showname,
			'u.username' => $req->username,
			'ep.email' => $req->email,
			'ep.twitter_name' => $req->twitter_name,
			'phone_number' => $phone,
			'ep.phone_country_id = ?' => $filter_phone_prefix,
			'ep.phone_instr' => $req->phone_instr,
			'ep.phone_instr_no_withheld' => $req->phone_instr_no_withheld,
			'has_website' => $req->has_web,
			//'website' => $req->website,
			'e.source_url' => $req->by_source_url,
			'no_phone_number' => $req->no_phone_number,
			'has_phone_number' => $req->has_phone_number,
			'phone_sms_verified' => $req->phone_sms_verified,
			/*'u.last_ip' => $req->last_ip,*/
			'ul.ip = ?' => trim($req->last_ip),
			'ul.country_id' => $req->geo_country,
            'ep.website' => $req->web,
			
			'u.sales_user_id = ?' => (int) $req->sales_user_id,
			
			'e.gender = ?' => $req->gender,
			'e.home_city_id = ?' => (int) $req->home_city_id,
			
			'e.country_id = ?' => $req->country,
			'rg.id = ?' => (int) $req->region,
            /*Change Grigor
             * 'city_id' => $req->city,
             */
			'city' => $req->city,

			'city_zone' => $req->cityzone,

			'ep.zip = ?' => $req->zip,
			'time_zone' => $req->time_zone_id,
            'ep.nationality_id = ?' => $req->nationality_id,

			'ep.admin_verified' => $req->admin_verified,

			// Status
			'e.status' => $req->status,
		
			'u.disabled = ?' => $req->disabled,
			/*'ep.admin_verified = ?' => $req->verified,*/
		
			'e.verified_status = ?' => $req->only_verified ? Model_EscortsV2::STATUS_VERIFIED : null,
            'no_id_card' => $req->no_id_card,
			'only_premium' => $req->only_premium,
			'only_city_tour' => $req->only_city_tour,
		    'skype_call_status' => $req->skype_call_status,
			// Date
			'filter_by' => $req->filter_by,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to,
			'had_running_package' => $req->had_running_package,
			'seo_data' => $req->seo_data,
			'about_text' => $req->about_text,
			'about_text_lang' => $req->about_text_lang,
			
			'bubble_message' => $req->bubble_message,
			'slogan' => $req->slogan,

			'excl_status' => Model_EscortsV2::ESCORT_STATUS_DELETED,

			//Check date FOR ED
			'check_date_from' => $req->check_date_from,
			'check_date_to' => $req->check_date_to,
			'next_check_date_from' => $req->next_check_date_from,
			'next_check_date_to' => $req->next_check_date_to,
			'check_comment_text' => $req->check_comment_text,
			'ec.check_status = ?' => $req->check_status,
			'check_by_backend_user' => $req->check_by_backend_user,
			'last_check_by_backend_user' => $req->last_check_by_backend_user,
			'overdue_checked' =>  $req->overdue_checked,
			'overdue_pending' =>  $req->overdue_pending,
			
            //Other
            'active_package_id' => $req->active_package_id,
			'ag.name' =>$req->agency_name,
			'ag.id' =>$req->agency_id,
            'u.email' => $req->reg_email,
			'ever_login' => $req->ever_login,
			'about_me' => $req->about_me,
            'e.is_suspicious' => $req->is_suspicious,
            'e.is_vip' => $req->is_vip,
            //auto approve revision
			'e.auto_approval' => (int) $req->auto_approve_rev,

            'is_vip_request' => $req->is_vip_request,
            'is_vip_package' => $req->is_vip_package,

			'e.check_website' => $req->check_website,
			'e.block_website' => $req->block_website,
			'e.site_not_working' => $req->site_not_working,
			
			'show_small_photos' => $req->show_small_photos,
			'photo_type' => $req->photo_disabled_type,
			'escort_comment' => $req->escort_comment,
			'escort_reviews' => $req->escort_reviews,
			'escort_comment_by' =>(int)  $req->escort_comment_by,
			'last_comment_by' => $req->last_comment_by,
			'has_active_reviews' => $req->has_active_reviews,
			'is_pornstar' => $req->is_pornstar,
			'bad_photo_history' => $req->bad_photo_history,
			'gallery_checked' => $req->gallery_checked,
			'about_less' => $req->about_me_characters,
			'about_me_informed' => $req->about_me_informed,
			'admin_comment_text' => $req->admin_comment_text,
			'e.need_age_verification' =>(int) $req->age_verification_status,
			'has_video' => $req->has_video,
			'text_done' => $req->text_done,

			'has_hand_verification_date' => $req->has_hand_verification_date,
			'hand_verification_date_from' => $req->hand_verification_date_from,
			'hand_verification_date_to' => $req->hand_verification_date_to,
			
			'has_private_photos' => $req->has_private_photos,
			'profile_type' => $req->profile_type,
			'photoshop' => $req->photoshop,
			'has_expo'	=> $req->has_expo,
			'e.skype_call_status' => $req->has_skype_video,
			'show_popunder' => $req->show_popunder,
			'large_popunder' => $req->large_popunder,
			'has_not_approved_pics' => $req->has_not_approved_pics,
            'map_activated' => $req->map_activated,

		);

		if  ($req->not_verified == 1){
            $filter['e.verified_status = ?'] = $req->not_verified ? Model_EscortsV2::STATUS_NOT_VERIFIED : null;
        }
		if(in_array(Cubix_Application::getId(), array(APP_BL))) {
            $filter['is_vaccinated'] = $req->is_vaccinated;
            $filter['gotd_date_from'] = $req->gotd_date_from;
            $filter['gotd_date_to'] = $req->gotd_date_to;
        }
		
		/* this section for ignoring all cases of source url redirecting filter value, IF 'Source url (website)' does not contains vivastreet */
		if(in_array(Cubix_Application::getId(), array(APP_EG_CO_UK)) ) {
            if (strpos($filter["e.source_url"], 'vivastreet') !== false &&  !empty($filter["e.source_url"])) {
                $filter['source_url_redirects'] = (int)$req->source_url_redirects;
            }
        }

        if(in_array(Cubix_Application::getId(), array(APP_ED,APP_EG_CO_UK))) {
            if ($req->sales_assigned_from) {
                $filter['sales_change_date_from'] = $req->sales_assigned_from;
            }
            if ($req->sales_assigned_to) {
                $filter['sales_change_date_to'] = $req->sales_assigned_to;
            }
        }

        if(in_array(Cubix_Application::getId(), array(APP_ED))) {
            if ($req->website) {
                $filter[] = 'ep.website LIKE "%'.$req->website.'%"';
            }
        }

		if ( $req->not_check_website == 1 ) {
			$filter[] = 'LENGTH(ep.website) > 0 AND e.check_website = 0 AND e.block_website <> 1';
		}
        if  ($req->orange_list == 1){
            $filter['orange_list'] = 1;
        }
        if  ($req->in_orange_list_from){
            $filter['in_orange_list_from'] = $req->in_orange_list_from;
        }
        if  ($req->in_orange_list_to){
            $filter['in_orange_list_to'] = $req->in_orange_list_to;
        }
		if($filter['e.id = ?']){
			if(strpos($req->escort_id, ',')){
				$exploded_eids = explode(',', $req->escort_id);
				if(is_array($exploded_eids)){
					$ids_string = '';
					foreach($exploded_eids as $eid){
						if((int)$eid>0){
							$ids .= (int)$eid.',';
						}
					}	

					$ids_string = rtrim($ids, ',');

					$filter[] = 'e.id IN ('.$ids_string.')';
					unset($filter['e.id = ?']);
					
				}else{
					unset($filter['e.type = ?']);
				}
			}
		}

		if( in_array(Cubix_Application::getId(), array(APP_ED)) ){
			if ( !empty($req->type) ) {
				$filter['e.type = ?'] = $req->type;
			}

            if ( !empty($req->data_entry_id) ) {
                $filter['e.data_entry_user = ?'] = $req->data_entry_id;
            }

            if (!empty($req->signed_in_once)){
                $filter['signed_in_once'] = $req->signed_in_once;
            }

            if (!empty($req->has_blacklisted_ips)){
                $filter['has_blacklisted_ips'] = $req->has_blacklisted_ips;
            }

            if (!empty($req->no_sync_escort)){
                $filter[] = 'u.sales_user_id <> 96';
            }

            if ( $req->has_viber == 1 ) {
                $filter[] = '(ep.viber = 1 OR ep.viber_1 = 1  OR ep.viber_2 = 1 )';
            }

            if ( $req->search_in_all_cities == 1 ) {
                $filter['search_in_all_cities'] = 1;
            }


            if ( $req->has_whatsapp == 1 ) {
                $filter[] = '(ep.whatsapp = 1 OR ep.whatsapp_1 = 1  OR ep.whatsapp_2 = 1 )';
            }
            if ( $req->has_telegram == 1 ) {
                $filter[] = 'ep.telegram = 1';
            }
            if ( $req->has_signal == 1 ) {
                $filter[] = 'ep.ssignal = 1';
            }
            if ( $req->has_wechat == 1 ) {
                $filter[] = 'ep.wechat = 1';
            }
		}

		if(Cubix_Application::getId() == APP_EF){
			if ( $req->zona_rossa == 1 ) {
	            $filter['zona_rossa'] = 1;
	        }

            if ( $req->f_valentine == 'yes' ) {
                $filter['is_valentines'] = 1;
            }

            if ( $req->f_valentine == 'no' ) {
                $filter['is_valentines'] = 0;
            }
	    }

		if(in_array(Cubix_Application::getId(), [APP_ED, APP_EG_CO_UK, APP_6B])) {
            if (isset($req->is_featured)){
                $filter['fg.is_featured = ?'] = $req->is_featured;
            }
        }
        if(in_array(Cubix_Application::getId(), [APP_EG_CO_UK])) {
            if (isset($req->phone_sms_verification_code)){
                $filter['e.phone_sms_verification_code = ?'] = $req->phone_sms_verification_code;
            }
            unset($req->phone_sms_verification_code);
        }
		if(in_array(Cubix_Application::getId(), [APP_A6])) {
            if (isset($req->premium_city) AND !empty($req->premium_city)){
                $filter['aa.area_id = ?'] = $req->premium_city;
            }
        }

		if( in_array(Cubix_Application::getId(), array(APP_6B, APP_AE, APP_BL, APP_EG_CO_UK, APP_ED)) ){
            if ( !empty($req->backend_user) ) {
                $filter['u.backend_user'] = $req->backend_user;
            }
        }

		if ( $req->has_email == 1 ) {
			$filter[] = 'ep.email IS NOT NULL';
		}
		
		if ( $req->is_vip == 1 ) {
			$filter[] = 'e.is_vip = 1';
		}		
		
		if ( !empty($req->cam_status) ) {
			$filter['e.cam_status = ?'] = $req->cam_status;
		}
		
		if ($req->has_rotating_feature == 1)
			$filter[] = '( e.photo_rotate_type = '.Model_Escort_Photos::PHOTO_ROTATE_ALL.' OR e.photo_rotate_type = '.Model_Escort_Photos::PHOTO_ROTATE_SELECTED.')';  
		// Independent and Agency girl filtering
		$escort_type_filter = $this->_getParam('escort_type');
		if ( $escort_type_filter == 'independent' ) {
			$filter[] = 'e.agency_id IS NULL';
		}
		elseif ( $escort_type_filter == 'agency' ) {
			$filter[] = 'e.agency_id IS NOT NULL';
		}
        if ( Cubix_Application::getId() == APP_ED && isset($req->more_than_one_city) )
        {
            $filter[] = "(SELECT
                                COUNT( city_id ) 
                            FROM
                                escort_cities 
                            WHERE
                                escort_id = e.id) > 1";
        }
		//APP_EG_CO_UK, APP_EG, APP_AE
		//
		if(!in_array(Cubix_Application::getId(), array(APP_ED,APP_EG_CO_UK,APP_EG,APP_AE))){
			unset($filter['ag.id']);
		}else{
			$sourceUrlNotNull = false;
			
			if(!empty($filter['ag.id']) || $escort_type_filter == 'agency'){
				if ( $req->has_source_url == 1) {
					$sourceUrlNotNull = true;
				}
				if ( $req->has_source_url == 2 ) {
					$filter[] = 'e.source_url IS NULL';
				}

				if(!empty($filter['ag.id'])){
					if ( $req->has_works_source_url == 1 ) {
						$filter['work_source_url'] = 1;
						$sourceUrlNotNull = true;
					}
					if ( $req->has_works_source_url == 2 ) {
						$filter['wrong_source_url'] = 1;
						$sourceUrlNotNull = true;
					}
				}

				if($sourceUrlNotNull){
					$filter[] = 'e.source_url IS NOT NULL';
				}
			}
		}
		if(in_array(Cubix_Application::getId(), array(APP_ED))){
			if ( $req->source_url_doesnt_have == 1 ) {
				$filter[] = 'e.source_url_doesnt_have IS NOT NULL';
			}
			if ( $req->source_url_doesnt_have == 2 ) {
				$filter[] = 'e.source_url_doesnt_have IS  NULL';
			}
		}

		// Revisions filter fix
		if ( $this->_getParam('sort_field') == 'revs_count' ) {
			$this->_setParam('sort_field', 'COUNT(DISTINCT(pu.revision))');
		}
		
		//FIXING PACKAGES LOGIC
		//IF PACKAGE EXPIRES 19 APRIL IT'S ACTUALLY EXPIRES 18 APRIL 23:59
		if ( $filter['filter_by'] == 'package_expiration_date' ) {
			$filter['date_from'] += $filter['date_from']? 60*60*24 : 0;
			$filter['date_to'] += $filter['date_to']? 60*60*24 : 0;
		}
       
		$model = new Model_EscortsV2();
        $page = $this->_request->page;
        $per_page = $this->_request->per_page;
        $sort_field = $this->_request->sort_field;
        $sort_dir = $this->_request->sort_dir;

        if ( $this->_request->filteredExport == 1 )
        {
            $page = 1;
            $per_page = 5000;
            $sort_field = 'id';
            $sort_dir = 'desc';
        }

        $data = $model->getAll(
            $page,
            $per_page,
            $filter,
            $sort_field,
            $sort_dir,
            $count
        );

        if ( $this->_request->filteredExport == 1 )
        {
            $escorts_ids = array();
            foreach ( $data as $escort )
            {
                $escorts_ids[] = $escort->id;
                if(in_array(Cubix_Application::getId(), array(APP_EG_CO_UK,APP_ED)))
                {
                    $model->updateLastExportDate($escort->id);
                }
            }
            if ( !empty($escorts_ids) ) {
                $this->exportIndependentAction( $escorts_ids );
            }
        }

//		var_dump($data);die;
		$DEFINITIONS = Zend_Registry::get('defines');
		//var_dump(Model_Escort_ListV2::ddd());
		foreach ($data as $d)
		{
			$stat = array();
			foreach( $DEFINITIONS['escort_status_options'] as $key => $e_status )
			{
				if ( $this->model->hasStatusBit($d['id'], $key) )
				{
				    if ($this->model->hasStatusBit($d['id'], ESCORT_STATUS_INACTIVE)
                        && $key == ESCORT_STATUS_ACTIVE
                        && in_array(Cubix_Application::getId(), array(APP_EG_CO_UK, APP_ED))
                    ){
				        continue;
                    }
					$stat[] = $e_status;
				}
			}

			$d['status'] = implode(', ', $stat);//$DEFINITIONS['escort_status_options'][$d['status']];
			$d['b_user_type'] = $this->user->type;
			if ( Cubix_Application::getId() == APP_BL){
				$d['b_username'] = $this->user->username;
			}
						
			// $d['main_photo'] = $d->getMainPhoto()->getUrl('agency_p100');
			
			
			//FIXING PACKAGES LOGIC
			//IF PACKAGE EXPIRES 19 APRIL IT'S ACTUALLY EXPIRES 18 APRIL 23:59
			if ( $d['expiration_date'] ) {
			    $format = "d M Y";
			    if (Cubix_Application::getId() == APP_BL){
			        $format = "d M Y H:i";
                }
				$d['expiration_date'] = date($format, strtotime('-1 day', strtotime($d['expiration_date']))); //-1 day
				//$d['expiration_date'] = date("d M Y", strtotime('-1 day', $d['expiration_date'])); 
			}
			if(Cubix_Application::getId() == APP_A6){
				if ($d['is_for_phone_billing']) {
					$d['expiration_date_phone'] = $model->getPhoneExpirationDate($d['order_package_id']);

				}
			}
			
			if ($req->gallery_view)
			{				
				$escort = $this->model->get($d['id']);
				
				$photos = array('public' => array(), 'private' => array(), 'disabled' => array() );
				$added = false;

				foreach ( $escort->getPhotos() as $photo ) {
					$added = true;
					if($photos->is_portrait==0 && !is_null($photo->args))
						$photo->rwidth = $this->GetPhotoWidth('200', $photo->width, $photo->height);
					 
					if ( $photo->type == ESCORT_PHOTO_TYPE_PRIVATE ) {
						$photos['private'][] = $photo;
					} elseif ( $photo->type == ESCORT_PHOTO_TYPE_DISABLED ) {
						$photos['disabled'][] = $photo;
					} else {
						$photos['public'][] = $photo;
					}
				}
				
				if ($added)
				{
					$this->view->data_photos = $photos;
					$html = $this->view->render('escorts-v2/data-photos.phtml');
					$d['photos_html'] = $html;
				}
				
			}
			if ($req->about_me_view)
			{
				$this->view->about_me_texts = $this->model->getAboutMeTexts($d['id']);
				$html = $this->view->render('escorts-v2/about-me-texts.phtml');
				$d['about_texts'] = $html;
			}		
			
			$c = $this->model->getLastComment($d['id']);
			
			if ($c)
				$d['comment'] = date('Y-m-d H:i:s', $c->date) . ' | Commented by: ' . $c->username . '<br/>' . $c->comment;
		}
		

		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
	public function profilesDueAction()
	{
		
	}


	private function GetPhotoWidth($height,$natural_width,$natural_height)
	{
		$ratio = $height /$natural_height; 
		return $natural_width * $ratio; 
	}
	
	public function addPenaltyAction()
	{	
		$this->view->layout()->disableLayout();
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		$req = $this->_request;;
		if($bu_user->type!='superadmin'||!isset($req->escort_id) || !isset($req->user_id))
		{
			die;
		}
		$this->view->penalty_types = Model_Settings::GetPenaltyTypes();
	}
	public function notVerifiedUsersAction()
	{

	}
	
	public function deleteDueDateAction()
	{
		if ( is_array($this->_request->id) ) {
			$escort_ids = $this->_request->id;
		}
		else {
			$escort_ids = array(intval($this->_request->id));
		}
		
		//if ( ! $this->user->hasAccessToEscort($id) ) die('Permission denied');

		if ( count($escort_ids) > 0 ) {
			foreach( $escort_ids as $escort_id ) {
				
				if ( ! $this->user->hasAccessToEscort($escort_id) )	continue;//die('Permission denied');
				
				$this->model->deleteDueDate($escort_id);
			}
		}
		
		die;
	}

	public function profilesDueDataAction()
	{
		$req = $this->_request;

		$bu_user = Zend_Auth::getInstance()->getIdentity();

		if ( $bu_user->type == 'sales manager' || $bu_user->type == 'sales clerk' ) {
			$req->setParam('sales_user_id', $bu_user->id);
		}
		else if ($bu_user->type == 'admin') {
			$req->setParam('application_id', $bu_user->application_id);
		}

		$filter = array(
			// Personal
			'e.id = ?' => $req->escort_id,
			'u.application_id = ?' => $req->application_id,

			'e.showname' => $req->showname,
			'u.username' => $req->username,
			'ep.email' => $req->email,
			'ep.phone_number' => $req->phone,
			'ep.website' => $req->web,
			'u.last_ip' => $req->last_ip,

			'u.sales_user_id = ?' => $req->sales_user_id,

			'e.gender = ?' => $req->gender,

			'e.country_id = ?' => $req->country,
			'c.region_id = ?' => $req->region,
			'ec.city_id = ?' => $req->city,
			'ecz.city_zone_id = ?' => $req->cityzone,

			'profile_due' => true,

			'ep.zip = ?' => $req->zip,

			'ep.admin_verified = ?' => $req->admin_verified,

			// Status
			'e.status' => $req->status,

			'u.disabled = ?' => $req->disabled,
			/*'ep.admin_verified = ?' => $req->verified,*/

			'e.verified_status = ?' => $req->only_verified ? Model_EscortsV2::STATUS_VERIFIED : null,
            'e.verified_status = ?' => $req->not_verified ? Model_EscortsV2::STATUS_NOT_VERIFIED : null,
            'only_city_tour' => $req->only_city_tour,

			// Date
			'filter_by' => $req->filter_by,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to,

			'seo_data' => $req->seo_data,
			'about_text' => $req->about_text,

			'excl_status' => Model_EscortsV2::ESCORT_STATUS_DELETED
		);

		// Independent and Agency girl filtering
		$escort_type_filter = $this->_getParam('escort_type');
		if ( $escort_type_filter == 'independent' ) {
			$filter[] = 'e.agency_id IS NULL';
		}
		elseif ( $escort_type_filter == 'agency' ) {
			$filter[] = 'e.agency_id IS NOT NULL';
		}

		// Revisions filter fix
		if ( $this->_getParam('sort_field') == 'revs_count' ) {
			$this->_setParam('sort_field', 'COUNT(DISTINCT(pu.revision))');
		}

		$model = new Model_EscortsV2();
		$data = $model->getAll(
			$this->_request->page,
			$this->_request->per_page,
			$filter,
			$this->_request->sort_field,
			$this->_request->sort_dir,
			$count
		);

		$DEFINITIONS = Zend_Registry::get('defines');

		foreach ($data as $d)
		{
			$stat = array();
			foreach( $DEFINITIONS['escort_status_options'] as $key => $e_status )
			{
				if ( $this->model->hasStatusBit($d['id'], $key) )
				{
					$stat[] = $e_status;
				}
			}

			$d['status'] = implode(', ', $stat);//$DEFINITIONS['escort_status_options'][$d['status']];
			$d['due_date'] = strtotime($d['due_date']);
			// $d['main_photo'] = $d->getMainPhoto()->getUrl('agency_p100');
		}
		//print_r($data); die;
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

	public function notVerifiedUsersDataAction()
	{
		$req = $this->_request;

		$bu_user = Zend_Auth::getInstance()->getIdentity();
		if ( $bu_user->type == 'sales manager' ) {
			$req->setParam('sales_user_id', $bu_user->id);
		}
		
		$filter = array(
			'username' => $req->username,
			'application_id' => $req->application_id,
			'email' => $req->email,
			'user_type' => $req->user_type,
			//'not_verified' => true,
			'status' => $req->status,
			'sales_user_id' => $req->sales_user_id 
		);

		$model = new Model_Users();
		$count = 0;
		$data = $model->getAll(
			$this->_request->page,
			$this->_request->per_page,
			$filter,
			$this->_request->sort_field,
			$this->_request->sort_dir,
			$count
		);
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

	public function activateUserAction()
	{
		
		if ( is_array($this->_request->id) ) {
			$user_ids = $this->_request->id;
		}
		else {
			$user_ids = array(intval($this->_request->id));
		}
		$status = $this->_request->status;
		$m_user = new Model_Users();
		if ($status == USER_STATUS_ACTIVE){
			foreach($user_ids as $user_id) {
				$m_user->activate($user_id);
				Model_Users::sendPostActivationEmail($m_user->get($user_id));
			}
		}
		else{
			foreach($user_ids as $user_id) {
				$m_user->setStatus($user_id, $status);
			}
		}
		die;
	}

	public function vacationAction()
	{
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('default_escorts_vacation-data');
		$this->view->filter_params = $ses_filter->filter_params;
		/* FILTER PARAMS */
	}
	
	public function removeVacationAction()
	{
		$model = new Model_EscortsV2();
		
		$model->removeVacation($this->_request->escort_id);
		
		die;
	}
	
	public function vacationDataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'e.showname' => $req->showname,
			'u.application_id' => $req->application_id
		);		
		
		$model = new Model_EscortsV2();
		$count = 0;
		$data = $model->getVacations(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		echo json_encode(array(
			'data' => $data,
			'count' => $count
		));
		die;
	}


	/* ------------>>> ESCORT PROFILE VALIDATION <<<---------- */
	
	protected function _validateLogin($edit_mode = false, $validator)
	{
		$escort = new stdClass();
        $blackListModel = new Model_BlacklistedWords();
		if ( $edit_mode ) {
			$id = intval($this->_getParam('id'));
			$escort = $this->model->get($id);
			
		}

		$fields = array(
			'showname' => '',
			'username' => '',
			'password' => '',
			'sales_user_id' => 'int-nz',
            'country_id' => 'int',
			'agency_id' => 'int-nz',
			'pseudo_escort' => 'int-nz',
			'admin_verified' => 'int-nz',
			'free_comment' => '',
			'due_date' => 'int-nz',
			'slogan' => '',
			'auto_approval' => 'int-nz',
            'block_website' => 'int',
			'check_website' => 'int',
            //'phone_number' => '',//Added Grigor
			'reg_email' => '',
			'im_is_blocked'	=> 'int-nz',
			'show_popunder' => 'int'
		);
		
		//Request from andy for a6. username, password, reg email can be changed
		//Only by admins and super-admins
		$can_change_username = true;

		if ( $edit_mode ) {
			$fields['id'] = 'int';
			$fields['status'] = 'int-nz';
			$fields['status_comment'] = '';
			
			$fields['is_suspicious'] = 'int-nz';
			$fields['susp_comment_show'] = 'int-nz';
			
			$fields['suspicious_comment'] = '';
			$fields['fake_picture_disabled'] = 'int';
			$fields['photo_auto_approval'] = 'int-nz';
			if ( Cubix_Application::getId() == APP_6A){
				$fields['about_me_informed'] = 'int';
			}

            if ( Cubix_Application::getId() == APP_ED && ($this->user->type == "superadmin" ||  $this->user->type == "admin") ){
                $fields['data_entry_id'] = 'int-nz';
            }
			
			if ( Cubix_Application::getId() == APP_A6 && $this->user->type != "superadmin" &&  $this->user->type != "admin" ) {
				$can_change_username = false;
			}
			
			if ( in_array(Cubix_Application::getId(), array(APP_BL)) ) { 
				$fields['show_chat_fake'] = 'int';
				$fields['chat_show_fake_date_from'] = 'int';
				$fields['chat_show_fake_date_to'] = 'int';
			}

		}

        if ( in_array(Cubix_Application::getId(), array(APP_ED,APP_EG_CO_UK)) ) {
            $fields['exp_date'] = 'int';
        }
		
		if ( in_array(Cubix_Application::getId(), array(APP_ED)) ) { 
			$fields['type'] = 'int';
			$fields['last_hand_verification_date'] = '';
		}

		$form = new Cubix_Form_Data($this->_request);

		$form->setFields($fields);
		$data = $form->getData();

		$data['application_id'] = intval($this->_getParam('application_id'));

		if ( $edit_mode ) {
			$data['application_id'] = $escort->application_id;
		
			if ( $data['status'] )
			{
				if ( $data['status'] == Model_EscortsV2::ESCORT_STATUS_ACTIVE ) {
					if ( $this->model->hasStatusBit($id, Model_EscortsV2::ESCORT_STATUS_NO_ENOUGH_PHOTOS) ) {
						$validator->setError('status', 'You can\'t set Active status.<br/>No enougth photos.');
					}
					elseif ( $this->model->hasStatusBit($id, Model_EscortsV2::ESCORT_STATUS_NO_PROFILE) ) {
						$validator->setError('status', 'You can\'t set Active status.<br/>Escort has no profile.');
					}
					elseif ( $this->model->hasStatusBit($id, Model_EscortsV2::ESCORT_STATUS_PROFILE_CHANGED) ) {
						$validator->setError('status', 'You can\'t set Active status.<br/>Escort\'s profile has not approved.');
					}elseif ( $this->model->hasStatusBit($id, Model_EscortsV2::ESCORT_STATUS_SUSPICIOUS_DELETED) && $data['is_suspicious'] ) {
						$validator->setError('status', 'You can\'t set Active status.<br/>Escort\'s profile is suspicious.');
					}
				}
			}
			/*else
			{
				//unset($data['status']);
				//$validator->setError('status', 'Required');
			}*/

			if ( ! $data['fake_picture_disabled'] ) {
				$data['fake_picture_disabled'] = 0;
			}

			$data['login']['fake_picture_disabled'] = $data['fake_picture_disabled'];
			unset($data['fake_picture_disabled']);
			
			/*if ( ! $data['photo_auto_approval'] ) {
				$data['photo_auto_approval'] = 0;
			}*/
			
			$data['login']['photo_auto_approval'] = $data['photo_auto_approval'];
			unset($data['photo_auto_approval']);     
		}
		
		if ( ! $edit_mode ) {
			if ( ! $data['photo_auto_approval'] ) {
				$data['photo_auto_approval'] = 0;
			}

			$data['login']['photo_auto_approval'] = $data['photo_auto_approval'];
			unset($data['photo_auto_approval']);     
		}
		
		
		if ( ! $can_change_username && $escort->email != $data['reg_email'] ) {
			$validator->setError('reg_email', 'No permissions');
		} elseif ( ! strlen($data['reg_email']) ) {
			$validator->setError('reg_email', 'Required');
		}
		elseif(!$validator->isValidEmail($data['reg_email'])){
			$validator->setError('reg_email', 'Invalid Email');
		}
		elseif ( strlen($data['email']) && Cubix_Application::isDomainBlacklisted($data['email']) ) {
			$validator->setError('email', 'Domain is blacklisted');
		}
		elseif(Cubix_Application::getId() == APP_ED &&  trim($data['reg_email']) != "no@no.com" ){
			if ( $edit_mode ) {
				$user_id = $this->model->getUserId($id);
			}
			elseif ( $data['agency_id'] ) {
				$user_id = Model_Agencies::getUserId($data['agency_id']);
			}
			else{
				$user_id = null;
			}
			if($this->model->existsByEmail($data['reg_email'], $user_id) && !$this->importId){
				$validator->setError('reg_email', 'Email exists');
			}
		}
		$data['login']['reg_email'] = $data['reg_email'];
		unset($data['reg_email']);

		if ( in_array(Cubix_Application::getId(), array(APP_ED)) ) { 
			if ( ! $data['type'] ) {
				$validator->setError('type', 'Required');
			}
		}

		if ( ! $data['admin_verified'] ) {
			$data['admin_verified'] = 1;
		}

		if ( ! $data['auto_approval'] ) {
			$data['auto_approval'] = 0;
		}
		
		if ( ! $data['im_is_blocked'] ) {
			$data['im_is_blocked'] = 0;
		}

		if ( ! $data['slogan'] ) {
			$data['slogan'] = null;
		}
		else if(mb_strlen($data['slogan'],'UTF-8') > 20 && Cubix_Application::getId() != APP_ED) {
			$validator->setError('slogan', 'Slogan text must be not <br/> longer then 20 chars.');
		}
		else if($blackListModel->checkWords($data['slogan'],Model_BlacklistedWords::BL_TYPE_SLOGAN )){
				$validator->setError('slogan', 'Has Bl. word '.$blackListModel->getWords() );
		}
		else{
			$data['slogan'] = $validator->removeEmoji($data['slogan']);
		}
		if ( ! $data['is_suspicious'] ) {
			$data['is_suspicious'] = 0;
			$data['suspicious_comment'] = null;
			$data['susp_comment_show'] = 0;
		}
		else if ( $data['is_suspicious'] && ! $data['suspicious_comment'] ) {
			$validator->setError('suspicious_comment', 'Required');
		}
		else
			$data['susp_comment_show'] = intval($data['susp_comment_show']);
		
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		if (!in_array($bu_user->type, array('superadmin', 'admin')) &&  $edit_mode){ 
			$susp_data = $this->model->getSuspiciousStatus($id);
			if($susp_data->is_suspicious != 0){
				$data['is_suspicious'] = $susp_data->is_suspicious;
				$data['suspicious_comment'] = $susp_data->suspicious_comment;
				$data['susp_comment_show'] = $susp_data->susp_comment_show;
			}
		}
		if ( ! $data['free_comment'] ) {
			$data['free_comment'] = null;
		}

		if ( ! $data['due_date'] ) {
			$data['due_date'] = null;
		}
		$escort_id = $this->_getParam('id');
		$model_packages = new Model_Packages();
		$bu_user = Zend_Auth::getInstance()->getIdentity();

		if ( ! strlen($data['showname']) ) {
			$validator->setError('showname', 'Required');
		}
		elseif ( mb_strlen($data['showname']) > 25  ) {
			$validator->setError('showname', 'Showname must not be <br/> longer then 25 chars.');
		}
		elseif ( ! preg_match('/^[-_a-z0-9\s]+$/i', $data['showname']) ) {
			$validator->setError('showname', 'Allowed only: -, _, a-z, 0-9');
		}
		if(Cubix_Application::getId() == APP_A6){
			if($model_packages->hasActivePackage($escort_id) && $this->model->getShownameByEscortId($escort_id) != $data['showname'] && $bu_user->type != 'superadmin' ){
				$validator->setError('showname', 'Has Active Package');
			}
		}
		
		/*elseif ( ($escort->showname != $data['showname']) && $this->model->existsByShowname($data['showname']) ) {
			$validator->setError('showname', 'Already exists');
		}*/

		if ( ! $data['agency_id'] ) {
			
			if ( ! $can_change_username && $escort->username != $data['username'] ) {
				$validator->setError('username', 'No permissions');
			} elseif ( ! strlen($data['username']) ) {
				$validator->setError('username', 'Required');
			}
			elseif ( $this->model->existsByUsername($data['username'], $id) ) {
				if ( $escort->agency_id ) {
					$validator->setError('username', 'Already exists');
				}
				else if ( $escort->username != $data['username'] ) {
					$validator->setError('username', 'Already exists');
				}
			}
			
			if ( ! $can_change_username && strlen($data['password']) ) {
				$validator->setError('password', 'No permissions');
			} elseif ( ! $edit_mode && ! strlen($data['password']) ) {
				$validator->setError('password', 'Required');
			}
			elseif (strlen($data['password']) && $data['password'] == $data['username']){
				$validator->setError('password', "Can't use username as password");
			}
		}
        $bc_user = Zend_Auth::getInstance()->getIdentity();
		if($bc_user->type != 'moderator' && $bc_user->type != 'moderator plus'){
			if ( ! $data['sales_user_id'] && ! $data['agency_id'] ) {
				$validator->setError('sales_user_id', 'Required');
			}
		}
		
		

		if ( ! $data['country_id'] ) {
			$validator->setError('country_id', 'Required');
		}

		if ( $edit_mode && $data['sales_user_id'] != $escort->sales_user_id && ! $data['agency_id'] ) {
			Cubix_SyncNotifier::notify($escort->id, Cubix_SyncNotifier::EVENT_ESCORT_PROFILE_TRANSFER, array('before_sales_id' => $escort->sales_user_id, 'after_sales_id' => $data['sales_user_id']));
		}
		
		
		if ( in_array(Cubix_Application::getId(), array(APP_BL)) ) {
			if ( $data['show_chat_fake'] ) {
				if ( ! $data['chat_show_fake_date_from'] || ! $data['chat_show_fake_date_to'] ) {
					$validator->setError('show_chat_fake', 'Select date range');
				}
				if ( $data['chat_show_fake_date_from'] > $data['chat_show_fake_date_to'] ) {
					$validator->setError('show_chat_fake', 'Date from should be greater.');
				}
			}
			
			if ( $edit_mode ) {
				$data['fake_chat'] = array(
					'show_chat_fake'	=> $data['show_chat_fake'],
					'date_from' => date('Y-m-d H:i', $data['chat_show_fake_date_from']),
					'date_to' => date('Y-m-d H:i', $data['chat_show_fake_date_to']),
				);
			}
			unset($data['chat_show_fake_date_from']);
			unset($data['chat_show_fake_date_to']);
			unset($data['show_chat_fake']);
		}
		
		if ( in_array(Cubix_Application::getId(), array(APP_ED)) ) { 
			$data['login']['type'] = $data['type'];
			unset($data['type']);
		}

		$data['login']['username'] = $data['username'];
		unset($data['username']);
		$data['login']['password'] = $data['password'];
		unset($data['password']);
		$data['login']['sales_user_id'] = $data['sales_user_id'];
		unset($data['sales_user_id']);
		$data['login']['data_entry_id'] = $data['data_entry_id'];
        unset($data['data_entry_id']);
        $data['login']['agency_id'] = $data['agency_id'];
		unset($data['agency_id']);
		$data['login']['pseudo_escort'] = $data['pseudo_escort'];
		unset($data['pseudo_escort']);
		$data['login']['application_id'] = $data['application_id'];
		unset($data['application_id']);
		$data['login']['id'] = $data['id'];
		unset($data['id']);
		$data['login']['is_suspicious'] = $data['is_suspicious'];
		unset($data['is_suspicious']);
		$data['login']['susp_comment_show'] = $data['susp_comment_show'];
		unset($data['susp_comment_show']);
		$data['login']['suspicious_comment'] = $data['suspicious_comment'];
		unset($data['suspicious_comment']);
		$data['login']['slogan'] = $data['slogan'];
		//unset($data['slogan']);
		$data['login']['auto_approval'] = $data['auto_approval'];
		unset($data['auto_approval']);
		
		$data['login']['im_is_blocked'] = $data['im_is_blocked'];
		unset($data['im_is_blocked']);
		
		return $data;
	}

	protected function _validateBiography($edit_mode = false, $validator)
	{
		$form = new Cubix_Form_Data($this->_request);
		
		$fields = array(
			'ethnicity' => 'int-nz',
			'gender' => 'int',
			'nationality_id' => 'int-nz',
			'birth_date' => 'int-nz', // timestamp

			'measure_units' => 'int-nz',
			'height' => '',
			'weight' => '',
			'bust' => 'int-nz',
			'waist' => 'int-nz',
			'hip' => 'int-nz',
			'hair_color' => 'int-nz',
			'hair_length' => 'int-nz',
			'pubic_hair' => 'int-nz',
			'tatoo' => 'int-nz',
			'piercing' => 'int-nz',
			'eye_color' => 'int-nz',
			'shoe_size' => 'int-nz',
			'cup_size' => '',
			'dress_size' => '',
			'breast_type' => 'int',
			'is_smoking' => 'int-nz',			
			'is_drinking' => 'int-nz',
			'block_countries' => 'arr-int',
			'blocked_members' => 'arr-int',
			'is_pornstar' => 'int',
			'stays_verified' => 'int',
			'bad_photo_history' => 'int',
			'characteristics' => 'notags|special',
			'langs' => ''
		);
		
		if ( Cubix_Application::getId() == APP_ED ) {
			$fields['sync_escort'] = 'int';
			$fields['mark_not_new'] = 'int';
		}
		if ( Cubix_Application::getId() == APP_BL ) {
			$fields['is_vaccinated'] = 'int';
		}

		if ( in_array(Cubix_Application::getId(), array(APP_ED, APP_EG_CO_UK)) ) {
			$fields['disable_when_tour_ends'] = 'int';
		}
		
		if ( Cubix_Application::getId() == 33 || Cubix_Application::getId() == 22 || Cubix_Application::getId() == APP_ED ) {
			$fields = array_merge($fields, array(
				'cuisine' => '',
				'drink' => '',
				'flower' => '',
				'gifts' => '',
				'perfume' => '',
				'designer' => '',
				'interests' => '',
				'sports' => '',
				'hobbies' => '',
				
				'min_book_time' => 'int-nz',
				'min_book_time_unit' => 'int-nz',
				
				'has_down_pay' => 'int-nz',
				'down_pay' => 'int-nz',
				'down_pay_cur' => 'int-nz',
				
				'reservation' => 'int-nz'
			));
		}
		$blackListModel = new Model_BlacklistedWords();
		if ( in_array(Cubix_Application::getId(), array(APP_6B, APP_6C, APP_EG, APP_EG_CO_UK, APP_ED)) ) {
			$fields = array_merge($fields, array(
				'at_exact_date' => 'int-nz',
				'activation_date' => '',
			));
		}
		
		$form->setFields($fields);
		$data = $form->getData();

		if ( Cubix_Application::getId() == 33 || Cubix_Application::getId() == 22 || Cubix_Application::getId() == APP_ED ) {
			if ( $data['has_down_pay'] != YES ) {
				$data['down_pay'] = null;
				$data['down_pay_cur'] = null;
			}						
		}
		
		if ( ! $data['gender'] ) {
			$validator->setError('gender', 'Required');
		}
		
		if(strlen($data['characteristics']) > 0){
			$data['characteristics'] = $validator->removeEmoji($data['characteristics']);
			if($blackListModel->checkWords($data['characteristics'] ,Model_BlacklistedWords::BL_TYPE_SPECIAL_CHAR )){
				$validator->setError('characteristics', 'Has Bl. word '.$blackListModel->getWords() );
			}
		}
		
		if ( in_array(Cubix_Application::getId(), array(APP_6B, APP_6C, APP_EG, APP_EG_CO_UK, APP_ED)) ) 
		{
			
			if ($data['activation_date'])
			{
				
				if ($data['activation_date'] < time())
					$validator->setError('activation_date', 'Date must be in future');
				else
				{
					$days_between = ceil(abs($data['activation_date'] - time()) / 86400);
					
					if ($days_between > 14)
						$validator->setError('activation_date', 'Date must be less than 14 days');
				}
			}
			elseif ($data['at_exact_date'] && !$data['activation_date']){
				$validator->setError('activation_date', 'Required');
			}
			
			if (!$data['at_exact_date'] || !$data['activation_date'])
			{
				unset($data['activation_date']);
			}
			
			unset($data['at_exact_date']);
		}
        if (!$data['birth_date']) {
            if (!in_array($this->user->type, ['admin', 'superadmin']) && array_key_exists('birth_date', $data))
                unset($data['birth_date']);
		}
		else {
		   if( is_numeric($data['birth_date']) ){
		       //birth day is a age of escort and we generate birthday, very stupid and old shit
               //$data['birth_date'] = $data['birth_date'] ? new Zend_Db_Expr('FROM_UNIXTIME(' . $data['birth_date'] . ')') : null;
               $data['birth_date'] = strtotime(date('Y-m-d', strtotime(date('Y') - $data['birth_date'] . '-01-01') ));
           }elseif (strlen($data['birth_date']) == 0)
           {
               unset($data['birth_date']);
           }
		}

		foreach ( $data['block_countries'] as $i => $zone_id ) {
			if ( $zone_id != '0' ) {
				$data['block_countries'][$i] = array('country_id' => $zone_id);
			}
			else {
				unset($data['block_countries'][$i]);
			}
		}
		
		/*if ( ! $data['measure_units'] && ($data['height'] || $data['weight']) ) {
			$validator->setError('measure_units', 'Required');
		}*/

		$data['measure_units'] = METRIC_SYSTEM;
		if ( $this->measure_units == ROYAL_SYSTEM ) {
			$data['measure_units'] = ROYAL_SYSTEM;
			if ( $data['height'] ) {
				$rx = '/^([0-9]+)(\s*\'\s*([0-9]+)\s*(\"|\'\')?\s*)?$/';
				if ( ! preg_match($rx, $data['height'], $a) ) {
					$validator->setError('height', 'invalid format');
				}
				else {
					$data['height'] = Cubix_UnitsConverter::convert($data['height'], 'ftin', 'cm');
				}
			}

			if ( $data['weight'] ) {
				$data['weight'] = Cubix_UnitsConverter::convert($data['weight'], 'lbs', 'kg');
			}
		}
		
		$data['height'] = intval($data['height']);
		if ( ! $data['height'] ) $data['height'] = null;
		if ( $this->measure_units != ROYAL_SYSTEM ) {
			$data['weight'] = intval($data['weight']);
		}
		if ( ! $data['weight'] ) $data['weight'] = null;

		$i18n_data = Cubix_I18n::getValues($this->_request->getParams(), 'about', '');
		$a_exits_data = array();
		$security = new Cubix_Security();
		foreach ( $i18n_data as $field => $value ) {
			if (strlen($value)) $a_exits_data[] = $field;

			if ( Cubix_Application::getId() != APP_A6 && Cubix_Application::getId() != APP_A6_AT ) {
				$value = $security->clean_html($value);
			}
			else {
				$value = strip_tags($value);
			}
						
			if($blackListModel->checkWords($value,Model_BlacklistedWords::BL_TYPE_ABOUT )){
				$validator->setError('about_types', 'Has Bl. word '.$blackListModel->getWords() );
			}
			if(Cubix_Application::getId() == APP_EF){

                $value = preg_replace('/[[:^print:]]/', '', $value);
                $wrong_letter['symbol'][] = $this->checkSymbols($value);

                $checkedData = trim($this->stripAsci(strip_tags($value)));

                for ($i = 0 ;$i < strlen($checkedData);$i++){
                    if(!(mb_detect_encoding($checkedData[$i],'UTF-8'))){
                        $prev_letter = ($i == 0) ? "":mb_substr($checkedData,strpos($checkedData,$checkedData[$i]) - 1,1,'utf-8');
                        $next_letter = ($i == strlen($checkedData))?"" : mb_substr($checkedData,strpos($checkedData,$checkedData[$i]) + 1,1,'utf-8');
                        $current_letter  = mb_substr($checkedData,strpos($checkedData,$checkedData[$i]),1,'utf-8');
                        $wrong_letter['letters'] = $prev_letter .''. $current_letter.''.$next_letter;
                        break;
                    }
                }


                $checkSymbol = implode($wrong_letter['symbol']);
                
                if($wrong_letter['letters']){
					
                    $validator->setError('about_types', Cubix_I18n::translate('error_language_letter',array('LENGTH'=>200)).' - '. utf8_encode($wrong_letter['letters']));
                }
                elseif(strlen($checkSymbol) > 1){
                    $validator->setError('about_types', Cubix_I18n::translate('error_symbol',array('LENGTH'=>200)) . ' - '.$checkSymbol);
                }

            }
			
			$value = $validator->removeEmoji($value);
			$i18n_data[$field] = $security->xss_clean($value);
			
		}

		if (in_array(Cubix_Application::getId(), array(APP_ED, APP_EG_CO_UK, APP_6B)))
		{
			
			if (!count($a_exits_data) && $data['sync_escort'] != 1)
				$validator->setError('about_types', 'About me required' );
		}

		$data = array_merge($data, $i18n_data);
		
		if (Cubix_Application::getId() == APP_A6)
		{
			$data['about_anibis'] = $this->_request->about_anibis;
		}
				
        if (Cubix_Application::getId() == APP_6A || Cubix_Application::getId() == APP_EF)
		{
			$i18n_data_soft = Cubix_I18n::getValues($this->_request->getParams(), 'about_soft', '');
			foreach ( $i18n_data_soft as $field => $value ) {
				$value = strip_tags($value);
				$i18n_data[$field] = $security->xss_clean($value);
			}
			$data = array_merge($data, $i18n_data_soft);
			
		}
		/*if ( strlen($datap['bust_waist_hip']) && ! preg_match('#^[0-9]+-[0-9]+-[0-9]+$#', $data['bust_waist_hip']) ) {
			$validator->setError('bust_waist_hip', 'Wrong format<br/> (90-60-90)');
		}*/

		$langs = array();
		if ( ! is_array($data['langs']) || ! count($data['langs']) ) {
			if ( Cubix_Application::getId() != APP_A6 && Cubix_Application::getId() != APP_A6_AT ) {
				$validator->setError('langs', 'At least one is required');
			}
		}
		else {
			foreach ( $data['langs'] as $i => $lang ) {
				$arr = explode(':', $lang);
				$langs[$i] = array('lang_id' => $arr[0], 'level' => $arr[1]);
			}
			unset($data['langs']);
			$data['langs'] = $langs;
		}

		return $data;
	}

	protected function _validateServices($edit_mode = false, $validator)
	{
		$form = new Cubix_Form_Data($this->_request);

		$form->setFields(array(
			'sex_orientation' => 'int-nz',
			'sex_availability' => '',
			'services' => 'arr-int',
			'keywords' => 'arr-int',
			'service_prices' => 'arr',
			'service_currencies' => 'arr'
			/*'additional_services' => 'notags|special'*/
		));
		$data = $form->getData();

		$DEFINITIONS = Zend_Registry::get('defines');

		if ( is_array($data['services']) ) {
			foreach ( $data['services'] as $i => $svc ) {
				$data['services'][$i] = array('service_id' => $svc);
				if ( $data['service_prices'][$svc] ) {
					$price = floatval($data['service_prices'][$svc]);
					$currency = (isset($data['service_currencies'][$svc])) ? $data['service_currencies'][$svc] : $this->view->currency('id');
					if ( $price && $price > 0 ) {
						$data['services'][$i] = array_merge($data['services'][$i], array('price' => $price, 'currency_id' => $currency));
					}
				}
			}
		}
		unset($data['service_prices']);
		unset($data['service_currencies']);

		if ( is_array($data['sex_availability']) ) {
			foreach ( $data['sex_availability'] as $i => $opt ) {
				if ( ! isset($DEFINITIONS['sex_availability_options'][$opt]) ) {
					unset($data['sex_availability'][$i]);
				}
			}
			$data['sex_availability'] = count($data['sex_availability']) ?
				implode(',', $data['sex_availability']) : null;
		}
		else {
			if ( Cubix_Application::getId() != APP_A6 && Cubix_Application::getId() != APP_A6_AT ) {
				$validator->setError('sex_availability', 'Required');
			}
		}

		$i18n_data = Cubix_I18n::getValues($this->_request->getParams(), 'additional_service', '');
		foreach ( $i18n_data as $field => $value ) {
			$i18n_data[$field] = htmlspecialchars(strip_tags(trim($value)));
		}
		$data = array_merge($data, $i18n_data);

		return $data;
	}

	protected function _validateWorkingTimes($edit_mode = false, $validator)
	{
		$form = new Cubix_Form_Data($this->_request);

		$form->setFields(array(
			'available_24_7' => 'int-nz',
			'night_escort' => 'int',
			'day_index' => 'arr-int',
			'time_from' => 'arr-int',
			'time_from_m' => 'arr-int',
			'time_to' => 'arr-int',
			'time_to_m' => 'arr-int',
			'night_escorts'=>'arr-int'
		));
		$data = $form->getData();
	
		$_data = array('available_24_7' => $data['available_24_7'],'night_escort' => $data['night_escort'], 'times' => array());

		for ( $i = 1; $i <= 7; $i++ ) {
			if ( isset($data['day_index'][$i]) && isset($data['time_from'][$i]) && isset($data['time_from_m']) && isset($data['time_to'][$i]) && isset($data['time_to_m'][$i]) ) {
			$night_escort =  in_array($i,$data['night_escorts'])?1:0;
			$_data['times'][] = array(
					'day_index' => $i,
					'time_from' => $data['time_from'][$i],
					'time_from_m' => $data['time_from_m'][$i],
					'time_to' => $data['time_to'][$i],
					'time_to_m' => $data['time_to_m'][$i],
					'night_escorts' =>$night_escort
				);
			}
		}

		return $_data;
	}

	protected function _validateWorking($edit_mode = false, $validator)
	{
		$form = new Cubix_Form_Data($this->_request);

		$form->setFields(array(
			'base_city_id' => 'int-nz',
			'home_city_id' => 'int-nz',
			'locations' => 'arr-int',
			'z-locations' => 'arr-int',
			'zip' => 'string',
			'incall' => 'int',
			'incall_type' => 'int-nz',
			'incall_hotel_room' => 'int-nz',
			'incall_other' => 'notags|special',
			'outcall' => 'int',
			'outcall_type' => 'int-nz',
			'outcall_other' => 'notags|special',
			'vac_date_from' => 'int-nz',
			'vac_date_to' => 'int-nz',
			'premium_cities' => 'arr-int'
		));
		$data = $form->getData();

		/*if ( ! is_array($data['cities']) ) $data['cities'] = array();

		foreach ( $data['cities'] as $i => $city_id ) {
			$data['cities'][$i] = array('city_id' => $city_id);
		}*/

		$app = Cubix_Application::getById($this->_request->application_id);

		if(in_array(Cubix_Application::getId(), array(APP_ED)) && in_array($data['base_city_id'], $data['locations'])) {
			if ( count($data['locations']) > $app->max_working_cities + 1 ) {
				$validator->setError('locations', 'You can add maximum ' . $app->max_working_cities . ' working locations');
			}
		} else {
			if ( count($data['locations']) > $app->max_working_cities ) {
				$validator->setError('locations', 'You can add maximum ' . $app->max_working_cities . ' working locations');
			}
		}

		if ( ! $data['locations'] ) {
			//if ( Cubix_Application::getId() != APP_A6 && Cubix_Application::getId() != APP_A6_AT ) {
				$validator->setError('locations', 'At least one is required');
			//}
		}
		elseif ( ! $data['base_city_id'] ) {
			//if ( Cubix_Application::getId() != APP_A6 && Cubix_Application::getId() != APP_A6_AT ) {
				$validator->setError('locations', 'Please select the base city below');
			//}
		}

		//		if ( ! $data['home_city_id'] ) {
		//			$validator->setError('home_city', 'Please select the home city below');
		//		}

		$cities = array();
		if ( $data['locations'] ) {
			foreach($data['locations'] as $i => $city) {
				$cities[$i] = array('city_id' => $city);
			}
		}
		$data['cities'] = $cities;
		unset($data['locations']);

		$data['city_id'] = $data['base_city_id'];
		unset($data['base_city_id']);

		if ( $data['incall_type'] == 2 && ! $data['incall_hotel_room'] ) {
			$validator->setError('err_incall', 'Please select hotel room');
		}

		$cityzones = array();
		if ( count($data['z-locations']) ) {
			foreach($data['z-locations'] as $i => $zone) {
				if ($zone == 0) {
					break;
				}
				$cityzones[$i] = array('city_zone_id' => $zone);
			}
		}
		$data['cityzones'] = $cityzones;
		unset($data['z-locations']);
		

		if ( ! $data['incall'] ) {
			$data['incall_type'] = null;
			$data['incall_hotel_room'] = null;
			$data['incall_other'] = null;
			$data['zip'] = null;
		}
		else{
			if ( !$data['incall_type'] ) {
			$validator->setError('err_incall', 'Please select Incall suboption');
			}
		}
		unset($data['incall']);

		if ( ! $data['outcall'] ) {
			$data['outcall_type'] = null;
			$data['outcall_other'] = null;
		}
		unset($data['outcall']);

		//		if ( ! $edit_mode ) {
		//			if ( ! is_null($data['incall_type']) && ! $data['zip'] ) {
		//				$validator->setError('zip', 'Invalid zip code');
		//			}
		//		}

		$vac_date_from = $data['vac_date_from'];
		$vac_date_to = $data['vac_date_to'];

		if ( ($vac_date_from && ! $vac_date_to) || ($vac_date_to && ! $vac_date_from) || ($vac_date_to < $vac_date_from) ) {
			$validator->setError('vac_date', 'Wrong interval');
		}

		$data['vacation'] = array(
			'vac_date_from' => $data['vac_date_from'],
			'vac_date_to' => $data['vac_date_to'],
		);

		/*-->Premium cities logic*/
		if ( Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT) {
			$escort_id = $this->_getParam('id');

			$modelPackages = new Model_Billing_Packages();
			$result = $modelPackages->checkIfHasAddAreas($escort_id);

			if ( $result ) {
				if ( ! count($data['premium_cities']) ) {
					$validator->setError('premium_cities', 'Please select at least one area!');
				}

				if ( count($data['premium_cities']) > $result->add_areas_count ) {
					$validator->setError('premium_cities', "You can select only " . $result->add_areas_count . " area!");
				}
			}
		/*<--Premium cities logic*/
		}
		
		unset($data['vac_date_from']);
		unset($data['vac_date_to']);

		//print_r($data);die;

		return $data;		
	}

	protected function _validateContacts($edit_mode = false, $validator)
	{
		$form = new Cubix_Form_Data($this->_request);

		$fields = array(
			'phone_number' => '',
			'phone_additional_1' => '',
			'phone_additional_2' => '',
			'phone_prefix'=>'',
			'phone_prefix_1'=>'',
			'phone_prefix_2'=>'',
			'disable_phone_prefix'=>'int',
			'disable_phone_prefix_1'=>'int',
			'disable_phone_prefix_2'=>'int',
			'agency_id' => 'int-nz',
			/*'phone_number_alt' => 'string',*/
			'phone_instr' => 'int-nz',
			'phone_instr_1' => 'int-nz',
			'phone_instr_2' => 'int-nz',
			'phone_instr_no_withheld' => 'int-nz',
			'phone_instr_no_withheld_1' => 'int-nz',
			'phone_instr_no_withheld_2' => 'int-nz',
			'phone_instr_other' => 'string',
			'phone_instr_other_1' => 'string',
			'phone_instr_other_2' => 'string',
			'phone_sms_verified' => 'int',
			'email' => 'string',
			'website' => 'string',
			'club_name' => 'string',
			'street' => 'string',
			'street_no' => 'string',
			'address_additional_info' => 'string',
			// 'block_website' => 'int',
			'fake_city_id' => '',
			'fake_zip' => '',
			'viber' => 'int',
			'whatsapp' => 'int',
			'viber_1' => 'int',
			'whatsapp_1' => 'int',
			'viber_2' => 'int',
			'whatsapp_2' => 'int',
			'prefered_contact_methods' => 'arr'
		);

        if ( Cubix_Application::getId() == APP_6B ) {
            $fields['twitter_name'] = '';
        }
                
		if ( Cubix_Application::getId() == APP_BL ) {
			$fields['snapchat'] = 'int';
			$fields['snapchat_username'] = 'string';
		}
		
		if ( Cubix_Application::getId() == APP_EF ) {
			$fields['escortbook_ad'] = 'int';
			$fields['no_renew_sms'] = 'int';
		}

		if ( Cubix_Application::getId() == APP_ED ) {
			$fields['wechat'] = 'int';
			$fields['telegram'] = 'int';
			$fields['ssignal'] = 'int';
		}

		if ( Cubix_Application::getId() == APP_A6 ) {
			$fields['show_phone'] = 'int';
			$fields['skype_id'] = 'string';
			$fields['video_link'] = '';
        }

        if (in_array(Cubix_Application::getId(), [ APP_EG_CO_UK, APP_A6, APP_BL, APP_6B, APP_AE, APP_EM, APP_EF, APP_EI ])) {
            $fields['telegram'] = 'int';
            if (in_array(Cubix_Application::getId(), array(APP_EG_CO_UK,APP_BL,APP_6B,APP_AE,APP_EM)))
            {
                $fields['ssignal'] = 'int';
            }
        }


		$form->setFields($fields);
		$data = $form->getData();

		if ( Cubix_Application::getId() == APP_A6 ) {
			$data['show_phone'] =  $data['show_phone'] == 0 ? 2 : 1;
		}
		
		$escort_id = intval($this->_getParam('id'));
		if ( strlen($data['email']) && Cubix_Application::isDomainBlacklisted($data['email']) ) {
			$validator->setError('email', 'Domain is blacklisted');
		}

		if ( strlen($data['website']) && Cubix_Application::isDomainBlacklisted($data['website']) ) {
			$validator->setError('website', 'Domain is blacklisted');
		}
		elseif (strlen($data['website']) && strtolower(substr($data['website'], 0, 7)) != 'http://' && strtolower(substr($data['website'], 0, 8)) != 'https://' && strtolower(substr($data['website'], 0, 4)) != 'www.')
		{
			$validator->setError('website', 'Website must start with http://, https:// or www.');
		}


		$blackListModel = new Model_BlacklistedWords();
		if($blackListModel->checkWords($data['phone_instr_other'] ,Model_BlacklistedWords::BL_TYPE_OTHER )){
			$validator->setError('phone_instr_other', 'Has Bl. word '.$blackListModel->getWords() );
		}
		if ( Cubix_Application::getId() != APP_A6 && Cubix_Application::getId() != APP_A6_AT ) {
			unset($data['fake_city_id']);
			unset($data['fake_zip']);
		}

		if ( Cubix_Application::getId() != APP_ED ) {
			unset($data['prefered_contact_methods']);
		}else{
			if(isset($data['prefered_contact_methods'])){
				foreach ($data['prefered_contact_methods'] as $k => $method) {
					if(!strlen($method)){
						unset($data['prefered_contact_methods'][$k]);
					}
				}
				foreach ($data['prefered_contact_methods'] as $method) {
				if($method == 6){
						$data['prefered_contact_methods'] = array('6');
						break;		
					}
				}
				$data['prefered_contact_methods'] = serialize(array_unique(array_values($data['prefered_contact_methods'])));
			}
		}
		
		/*if ( Cubix_Application::getId() == 1 ) {
			if ( ! $data['phone_number'] && ! $data['email'] ) {
				$validator->setError('email', 'Email or Phone Required');
			}
		}
		else {
			if ( /! $data['phone_number'] &&/ ! $data['email'] ) {
				$validator->setError('email', 'Email field is Required');
			}
		}*/

		$data['contact_phone_parsed'] = $data['phone_exists'] = $data['phone_country_id'] = null;
		if ( $data['phone_number'] ) {
			if ( ! preg_match("/^[0-9\s\+\-\(\)]+$/i", $data['phone_number']) ) {
				$validator->setError('email_phone_error', 'Invalid phone number');
			}
			elseif(preg_match("/^(\+|00)/", $data['phone_number']) ) {
				$validator->setError('email_phone_error', 'Please enter phone number without country calling code');
			}
			if ( ! ($data['phone_prefix']) ) {
				$validator->setError('email_phone_error', 'Country calling code Required');
			}
			list($country_id, $phone_prefix,$ndd_prefix) = explode('-',$data['phone_prefix']);

			if($data['phone_number']){
				$data['contact_phone_parsed'] = preg_replace('/[^0-9]/', '', $data['phone_number']);
				$data['contact_phone_parsed'] = preg_replace('/^'.$ndd_prefix.'/', '', $data['contact_phone_parsed']);
				$data['contact_phone_parsed'] = '00'.$phone_prefix.$data['contact_phone_parsed'];

				if ( Cubix_Application::getId() == APP_ED ) {
					$data['contact_phone_free'] = preg_replace('/[^0-9]/', ' ', $data['phone_number']);
					//$data['contact_phone_free'] = preg_replace('/^'.$ndd_prefix.'/', '', $data['contact_phone_free']);
					//$data['contact_phone_free'] = '00'.$phone_prefix.$data['contact_phone_free'];
				}
			}else{
				$data['contact_phone_parsed'] = null;
				
				if ( Cubix_Application::getId() == APP_ED ) {
					$data['contact_phone_free'] = null;
				}
			}
			$data['phone_country_id'] = $country_id;
			$data['phone_exists'] = $data['contact_phone_parsed'];
			if ( Cubix_Application::getId() != APP_A6 && Cubix_Application::getId() != APP_A6_AT ) {
				if ( in_array(Cubix_Application::getId(), array(APP_ED, APP_EG_CO_UK, APP_EI)) &&  ($this->user->type == "superadmin" || $this->user->type == "admin")  ) {
					
				} else {
					$results = $this->model->existsByPhone($data['contact_phone_parsed'],$escort_id, $data['agency_id']);
					$resCount = count($results);

					//if ( ! $data['agency_id'] ) {
					
						if($resCount > 0) {
						   $validator->setError('email_phone_error', 'Phone already exists');
						}
					//}
				}
			}
		}
		
		if ( $data['phone_additional_1'] ) {
			if ( ! preg_match("/^[0-9\s\+\-\(\)]+$/i", $data['phone_additional_1']) ) {
				$validator->setError('email_phone_error_1', 'Invalid phone number');
			}
			
			elseif(preg_match("/^(\+|00)/", $data['phone_additional_1']) ) {
				$validator->setError('email_phone_error_1', 'Please enter phone number without country calling code');
			}
			if ( ! ($data['phone_prefix_1']) ) {
				$validator->setError('email_phone_error_1', 'Country calling code Required');
			}
			
			list($country_id, $phone_prefix, $ndd_prefix) = explode('-',$data['phone_prefix_1']);
			$data['phone_country_id_1'] = $country_id;
		}
		
		if ( $data['phone_additional_2'] ) {
			if ( ! preg_match("/^[0-9\s\+\-\(\)]+$/i", $data['phone_additional_2']) ) {
				$validator->setError('email_phone_error_2', 'Invalid phone number');
			}
			
			elseif(preg_match("/^(\+|00)/", $data['phone_additional_2']) ) {
				$validator->setError('email_phone_error_2', 'Please enter phone number without country calling code');
			}
			if ( ! ($data['phone_prefix_2']) ) {
				$validator->setError('email_phone_error_2', 'Country calling code Required');
			}
			
			list($country_id, $phone_prefix, $ndd_prefix) = explode('-',$data['phone_prefix_2']);
			$data['phone_country_id_2'] = $country_id;
		}
				
		//        if(!$data['block_website']){
		//            $data['block_website'] = null;
		//        }



		return $data;
	}

    protected function _validateTags($edit_mode = false, $validator)
    {
        $escort_id = intval($this->_getParam('id'));
        $form = new Cubix_Form_Data($this->_request);

        $form->setFields(array(
            'escort_tags' => 'arr'
        ));

        $data = $form->getData();

        if ( ! isset($data['escort_tags']) || ! is_array($data['escort_tags']) ) $data['escort_tags'] = array();

        $data['tags_tmp'] = array();
        foreach ( $data['escort_tags'] as $i => $tag ) {
            list($tagId) = explode('-', $tag);
            $data['tags_tmp'][$i] = array('tag_id' => $tagId,'escort_id' => $escort_id);
        }

        $data['tags'] = $data['tags_tmp'];
        unset($data['tags_tmp']);
        unset($data['escort_tags']);

        return $data;
    }

	protected function _validateTours($edit_mode = false, $validator)
	{
		$form = new Cubix_Form_Data($this->_request);

		$form->setFields(array(
			'tours' => ''
		));
		$data = $form->getData();

		if ( ! isset($data['tours']) || ! is_array($data['tours']) ) $data['tours'] = array();

		$data['tours_tmp'] = array();
		foreach ( $data['tours'] as $i => $tour ) {
				list($country_id, $city_id, $date_from, $date_to, $phone) = explode(':', $tour);

			/*if ( strlen($tour['email']) && Cubix_Application::isDomainBlacklisted($data['tours']['email']) ) {
				$validator->setError('email', 'Domain is blacklisted');
			}*/
			//strtotime(date('d-m-Y', $date_from))
			//strtotime(date('d-m-Y', $date_to))
			$data['tours_tmp'][$i] = array($country_id, $city_id,$date_from ,$date_to , $phone);
		}

		$today = strtotime(date('d-m-Y', time()));

		foreach ( $data['tours_tmp'] as $i1 => $tour ) {
				list($country_id, $city_id, $date_from, $date_to, $phone) = $tour;
			
			if ( $date_to < $today ) {
				$validator->setError('tours', 'Some date intervals are invalid or overlapped');
				break;
			}

			if ( $date_from > $date_to ) {
				$validator->setError('tours', 'Some date intervals are invalid or overlapped');
				break;
			}

			$found = false;

			foreach ( $data['tours_tmp'] as $i2 => $t1 ) {
				if ( $i1 == $i2 ) continue;

				list($nil, $nil, $d_f, $d_t, $phone) = $t1;

				if ( ($d_f >= $date_from && $d_f < $date_to) || ($d_t > $date_from && $d_t < $date_to) ) {
					$found = true;

					$validator->setError('tours', 'Some date intervals are invalid or overlapped');
					break;
				}
			}

			if ( $found ) {
				break;
			}
		}
		//var_dump($data);die;
		$data['tours'] = $data['tours_tmp'];
		unset($data['tours_tmp']);

		return $data;
	}

	protected function _validateToursV2($edit_mode = false, $validator)
	{
		$form = new Cubix_Form_Data($this->_request);

		$form->setFields(array(
			'tours' => ''
		));
		$data = $form->getData();

		if ( ! isset($data['tours']) || ! is_array($data['tours']) ) $data['tours'] = array();

		$data['tours_tmp'] = array();
		foreach ( $data['tours'] as $i => $tour ) {
			list($country_id, $city_id, $date_from, $date_to, $phone, $minutes_from, $minutes_to) = explode(':', $tour);
			$date_from = strtotime(date('d-m-Y', $date_from));
			$date_to = strtotime(date('d-m-Y', $date_to)); 
			if ( $minutes_from == "" && $minutes_to == ""){
				$date_to = strtotime(date('d-m-Y', $date_to)) + 60 * 60 * 24 - 1; // 23:59:59 
			}
			$data['tours_tmp'][$i] = array($country_id, $city_id, $date_from, $date_to, $phone, $minutes_from, $minutes_to);
			
			/*if ( strlen($tour['email']) && Cubix_Application::isDomainBlacklisted($data['tours']['email']) ) {
				$validator->setError('email', 'Domain is blacklisted');
			}*/
			//strtotime(date('d-m-Y', $date_from))
			//strtotime(date('d-m-Y', $date_to))
			
		}

		$today = strtotime(date('d-m-Y', time()));

		foreach ( $data['tours_tmp'] as $i1 => $tour ) {
				list($country_id, $city_id, $date_from, $date_to, $phone, $minutes_from, $minutes_to) = $tour;
				$date_from += intval($minutes_from) * 60;
				$date_to += intval( $minutes_to) * 60; 
			
			if ( $date_to < $today ) {
				$validator->setError('tours', 'Some date intervals are invalid or overlapped');
				break;
			}

			if ( $date_from > $date_to ) {
				$validator->setError('tours', 'Some date intervals are invalid or overlapped');
				break;
			}

			$found = false;

			foreach ( $data['tours_tmp'] as $i2 => $t1 ) {
				if ( $i1 == $i2 ) continue;

				list($nil, $nil, $d_f, $d_t, $phone, $m_f, $m_t) = $t1;
				/*var_dump($d_f);
				var_dump($date_from);
				var_dump($date_to);*/
				if ( ( ($d_f + $m_f * 60) >= $date_from && ($d_f + $m_f * 60)  < $date_to) 
					|| ( ($d_t + $m_t * 60) > $date_from	&& ($d_t + $m_t * 60) < $date_to )) {
					$found = true;

					$validator->setError('tours', 'Some date intervals are invalid or overlapped ');
					break;
				}
			}

			if ( $found ) {
				break;
			}
		}
		//var_dump($data);die;
		$data['tours'] = $data['tours_tmp'];
		unset($data['tours_tmp']);
		return $data;
	}
	
	protected function _validatePrices($edit_mode = false, $validator)
	{
		$rates = $this->_getParam('rates');

		if ( ! is_array($rates) ) {
			$rates = array();
		}

		$data = array('rates' => array());

		$DEFINITIONS = Zend_Registry::get('defines');

		//$currencies = array_keys($DEFINITIONS['currencies']);
		$currencies = array_keys(Model_Currencies::getAllAssoc());
		
		$units = array_keys($DEFINITIONS['time_unit_options']);
		foreach ( $rates as $availability => $_rates ) {
			if ( $availability == 'incall' ) { $availability = 1; $types = array('additional-hour', 'overnight', 'dinner-date', 'weekend'); }
			elseif ( $availability == 'outcall' ) { $availability = 2; $types = array('additional-hour', 'overnight', 'dinner-date', 'weekend'); }
			elseif ( $availability == 'travel' ) { $availability = 0; $types = array('weekend'); }
			else continue;

			foreach ( $_rates as $rate ) {
				if ( get_magic_quotes_gpc() ) $rate = stripslashes($rate);
				$rate = @json_decode($rate);

				if ( is_object($rate) ) $rate = (array) $rate;
				if ( $rate === false || ! is_array($rate) ) {
					continue;
				}

				// Case when rate is standart and some data are not valid
				if ( ! isset($rate['type']) && ( ! isset($rate['time']) || ! isset($rate['unit']) || ! isset($rate['price']) || ! isset($rate['currency']) ) ) {
					continue;
				}
				// Case when rate is a type of custom and some data are not valid
				elseif ( isset($rate['type']) && ( ! isset($rate['price']) || ! isset($rate['currency']) || ! in_array($rate['type'], $types) ) ) {
					continue;
				}

				// If price is invalid
				$price = intval($rate['price']);
				if ( $price <= 0 ) continue;

				// If currency is invalid
				$currency = intval($rate['currency']);
				if ( ! in_array($currency, $currencies) ) {
					continue;
				}
				// If plus taxi exced
				$taxi = intval($rate['taxi']);
				if($taxi !=1 ){
					$taxi = null;
				}
				// If rate is custom validate data add only type, price and currency fields
				if ( ! isset($rate['type']) ) {
					$time = intval($rate['time']);
					if ( $time <= 0 ) { continue; }

					$unit = intval($rate['unit']);
					if ( ! in_array($unit, $units) ) { continue; };
					
					
					$data['rates'][] = array('availability' => $availability, 'time' => $time, 'time_unit' => $unit, 'price' => $price, 'currency_id' => $currency, 'plus_taxi'=> $taxi);
					
				}
				// Otherwize add also time and time unit
				else {
					$data['rates'][] = array('availability' => $availability, 'type' => $rate['type'], 'price' => $price, 'currency_id' => $currency, 'plus_taxi'=> $taxi, 'plus_taxi'=> $taxi);
				}
			}
		}
		$travel_pay_methods = $this->_getParam('travel_payment_method');
		$travel_other_method = $this->_getParam('travel_other_method');
		$special_rates = htmlspecialchars(strip_tags(trim($this->_getParam('special_rates'))));
		$data['travel_payment_methods'][] = $travel_pay_methods;
		$data['travel_other_method'][] = $travel_other_method;
		$data['special_rates'] = $special_rates;
		return $data;
	}

	protected function _validateHappyHour($edit_mode = false, $validator)
	{
		$escort_id = $this->_getParam('id');
		
		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'hh_from' => 'int',
			'hh_until' => 'int',
			'hh_weekday' => 'double',
			'hh_motto' => 'notags',
			'hh_force_save' => 'int'
		));
		$data = $form->getData();
		
		$hh_status = Cubix_Api::getInstance()->call('getHappyHourStatus', array($escort_id));
		
		if ( ($hh_status == Model_EscortV2Item::HH_STATUS_EXPIRED || $hh_status == Model_EscortV2Item::HH_STATUS_ACTIVE ) && $data['hh_force_save'] != 1  ) {
			return array();
		}
		
		$is_set_outcall_rates = false;
		$is_set_incall_rates = false;

		$hh_save = intval($this->_getParam('hh_save'));
		$hh_rates = $this->_getParam('hh_rates');
		$hh_old_rates = $this->_getParam('hh_old_rates');

		if ( ! is_null($hh_rates) ) {
			if ( isset($hh_rates['outcall']) ) {
				foreach( $hh_rates['outcall'] as $k => $v ) {
					if (intval($v)) {
						$is_set_outcall_rates = true;
						break;
					}
				}
			}

			if ( isset($hh_rates['incall']) ) {
				foreach( $hh_rates['incall'] as $k => $v ) {
					if (intval($v)) {
						$is_set_incall_rates = true;
						break;
					}
				}
			}
		}
		$_data = array(
			'hh_date_from'	=> null,
			'hh_date_to'	=> null,
			'hh_motto'		=> null,
			'hh_save'		=> 0,
			'has_hh'		=> 0
		);
		
		//SUPERADMIN CAN MAKE CHANGES EVEN IF HH IS ACTIVE.
		//IF CHECKED hh_force_save FLAG THAT MEANS ADMIN MAKES CHANGES AND VALIDATION MUST PROCEED
		
		$dates = $this->makeDate($data['hh_weekday'], $data['hh_from'], $data['hh_until']);
		if ( ($data['hh_from'] || $data['hh_until'] || /*strlen($data['hh_motto']) || */$is_set_outcall_rates || $is_set_incall_rates || $hh_save) ) {

			//VALIDATE DATE
			if ( ($hh_status != Model_EscortV2Item::HH_STATUS_EXPIRED && $hh_status != Model_EscortV2Item::HH_STATUS_ACTIVE && $hh_status != Model_EscortV2Item::HH_STATUS_ADMIN_DISABLED) || $data['hh_force_save'] ) { // DO NOT VALIDATE DATE IF EXPIRED OR ACTIVE. ONLY SUPERADMIN CAN MAKE CHANGES
				$diff_minutes = $this->dateDiff($dates['hh_date_from'], $dates['hh_date_to']);
				if ( $dates['hh_date_from'] < time() && ! $data['hh_force_save']  ) { // IF CHECKED hh_force_save DO NOT VALIDATE TIME TO BE IN FUTURE
					$validator->setError('err_hh_date', 'Date must be in the future');
				} elseif ( $dates['hh_date_to'] <= $dates['hh_date_from']  ) {
					$validator->setError('err_hh_date', 'Date until must be bigger then Date From');
				} elseif ( !is_null($this->escort->agency_id) && $diff_minutes > 300 ) {
					$validator->setError('err_hh_date', 'The duration of happy hour could not be grater then 5 hour !');
				}
				if ( is_null($this->escort->agency_id) ) {
					$diff_hour = $diff_minutes / 60;
					$used_time = Cubix_Api::getInstance()->call('getAgencyUsedHHHours', array($this->agency->id, $escort_id));

					if ( $used_time + $diff_hour > 10 )
						$validator->setError('err_hh_date', 'The duration of happy hour could not be grater then ' . (10 - $used_time) . ' hour !');
				}
			}
			//CHECK HH_PRICE TO BE SET AND BE SMALLER THEN OLD ONE, BOTH FOR OUTCALL AND INCALL
			if ( ! $is_set_incall_rates ) {
				if ( count($hh_rates['incall']) )
				$validator->setError('err_hh_incall_rates', 'At least one "Happy Hour Rate" is Required for Incall');
			} else {
				foreach( $hh_rates['incall'] as $k => $v ) {
					if ( trim($v) >= $hh_old_rates['incall'][$k] ) {
						$validator->setError('err_hh_incall_rates', 'please make sure to specify smaller price');
					}
				}
			}
			if ( ! $is_set_outcall_rates ) {
				if ( count($hh_rates['outcall']) )
				$validator->setError('err_hh_outcall_rates', 'At least one "Happy Hour Rate" is Required for Outcall');
			} else {
				foreach( $hh_rates['outcall'] as $k => $v ) {
					if ( trim($v) >= $hh_old_rates['outcall'][$k] ) {
						$validator->setError('err_hh_outcall_rates', 'please make sure to specify smaller price');
					}
				}
			}

			$_data = array(
				'hh_date_from'	=> date('Y-m-d H:i:s', $dates['hh_date_from']),
				'hh_date_to'	=> date('Y-m-d H:i:s', $dates['hh_date_to']),
				'hh_motto'		=> $data['hh_motto'],
				'hh_save'		=> $hh_save,
				'has_hh'		=> 1
			) + $_data;
		}
		$_data['hh_rates'] = ( isset($hh_rates['outcall']) ? $hh_rates['outcall'] : array() ) + (isset($hh_rates['incall']) ? $hh_rates['incall'] : array());

		return $_data;
	}
	
	protected function __validate($edit_mode = false, $validator)
	{
		$data = array();


		$login_data = $this->_validateLogin($edit_mode, $validator);

		$biography_data = $this->_validateBiography($edit_mode, $validator);
		$services_data = $this->_validateServices($edit_mode, $validator);
        if ( $edit_mode && in_array(Cubix_Application::getId(),array(APP_EG_CO_UK,APP_ED)))
            $tagData = $this->_validateTags($edit_mode, $validator);
		
		$working_times_data = $this->_validateWorkingTimes($edit_mode, $validator);
		$working_data = $this->_validateWorking($edit_mode, $validator);
		$contact_data = $this->_validateContacts($edit_mode, $validator);

		if( in_array(Cubix_Application::getId(), array(APP_EF, APP_6A, APP_BL, APP_AE, APP_6B, APP_6C, APP_EG, APP_EG_CO_UK))){
			$tours_data = $this->_validateToursV2($edit_mode, $validator);
		}
		else{
			$tours_data = $this->_validateTours($edit_mode, $validator);
		}
		$prices_data = $this->_validatePrices($edit_mode, $validator);
		if ( $edit_mode && Cubix_Application::getId() != APP_6A)
			$hh_data = $this->_validateHappyHour($edit_mode, $validator);

		$data = array_merge(
			$data,
			$login_data,
			$biography_data,
			$services_data,
			$working_times_data,
			$working_data,
			$contact_data,
			$prices_data,
			$tours_data
		);

		if ( $edit_mode && Cubix_Application::getId() != APP_6A) {
			$data = array_merge($data, $hh_data);
		}

        if ( $edit_mode && in_array(Cubix_Application::getId(),array(APP_EG_CO_UK,APP_ED))) {
            $data = array_merge($data, $tagData);
        }
        
		// in copy mode we can't move country and cities, because this applications
		// may not support that country
		if ( 'copy-listener' == $this->_getParam('request_mode') ) {
			$validator->unsetError('country_id');
			$validator->unsetError('locations');
		}
		
		return $data;
	}

	public function validateToursAction()
	{
		$this->view->layout()->disableLayout();

		$form = new Cubix_Form_Data($this->_request);

		$form->setFields(array(
			'tours' => ''
		));
		$data = $form->getData();

		if ( ! isset($data['tours']) || ! is_array($data['tours']) ) $data['tours'] = array();

		$data['tours_tmp'] = array();
		foreach ( $data['tours'] as $i => $tour ) {
			if( in_array(Cubix_Application::getId(), array(APP_EF, APP_6A, APP_BL, APP_AE, APP_6B, APP_6C, APP_EG, APP_EG_CO_UK))){
				list($country_id, $city_id, $date_from, $date_to, $phone, $minutes_from, $minutes_to) = explode(':', $tour);
				if ( $minutes_from != "" && $minutes_to != ""){
					$date_from = strtotime(date('d-m-Y', $date_from)) + intval($minutes_from) * 60;
					$date_to = strtotime(date('d-m-Y', $date_to)) + intval($minutes_to) * 60; 
				}
				else{
					$date_from = strtotime(date('d-m-Y', $date_from));
					$date_to = strtotime(date('d-m-Y', $date_to)) + 60 * 60 * 24 - 1; // 23:59:59 
				}
				$data['tours_tmp'][$i] = array($country_id, $city_id, $date_from, $date_to, $phone);
			}
			else{
				list($country_id, $city_id, $date_from, $date_to, $phone) = explode(':', $tour);
				$data['tours_tmp'][$i] = array($country_id, $city_id, strtotime(date('d-m-Y', $date_from)), strtotime(date('d-m-Y', $date_to)), $phone);
			}
			/*if ( strlen($tour['email']) && Cubix_Application::isDomainBlacklisted($data['tours']['email']) ) {
				$validator->setError('email', 'Domain is blacklisted');
			}*/

		}

		$today = strtotime(date('d-m-Y', time()));
		
		$is_error = false;
		
		foreach ( $data['tours_tmp'] as $i1 => $tour ) {
				list($country_id, $city_id, $date_from, $date_to, $phone) = $tour;

			if ( $date_to < $today ) {
				$is_error = true;
				//$validator->setError('tours', 'Some date intervals are invalid or overlapped');
				break;
			}
			
			if ( $date_from > $date_to ) {
				$is_error = true;
				//$validator->setError('tours', 'Some date intervals are invalid or overlapped');
				break;
			}
			
			$found = false;
			
			foreach ( $data['tours_tmp'] as $i2 => $t1 ) {
				if ( $i1 == $i2 ) continue;

				list($nil, $nil, $d_f, $d_t, $phone) = $t1;

				if ( ($d_f >= $date_from && $d_f < $date_to) || ($d_t > $date_from && $d_t <= $date_to) ) {
					$found = true;
					$is_error = true;
					//$validator->setError('tours', 'Some date intervals are invalid or overlapped');
					break;
				}
			}

			if ( $found ) {
				break;
			}
		}

		if ( $is_error ) {
			die('Some date intervals are invalid or overlapped');
		}
		else {
			die;
		}
		
		/*$data['tours'] = $data['tours_tmp'];
		unset($data['tours_tmp']);

		return $data;*/
	}

	public function toggleWebsiteStatusAction(){
		$this->view->layout()->disableLayout();

		if ( $this->_request->isPost()) {
			$escort_id = (int)$this->_request->escort_id;
			if(is_int($escort_id) && in_array($this->_request->status, array('pending', 'checked', 'blocked'))){
				$m_escort = new Model_EscortsV2();
				echo $m_escort->updateWebsiteStatus($escort_id, $this->_request->status, $this->_request->sales);
			}
		}
		die;
	}

	public function reconstructRates($data)
	{
		$result = array('incall' => array(), 'outcall' => array(), 'travel' => array());
		if($data){
			foreach ( $data as $rate ) {
				if ( $rate['availability'] == 1 ) {
					$key = 'incall';
				}
				elseif ( $rate['availability'] == 2 ) {
					$key = 'outcall';
				}
				elseif ( $rate['availability'] == 0 ) {
					$key = 'travel';
				}

				if ( $rate['type'] ) {
					$result[$key][] = array('type' => $rate['type'], 'price' => $rate['price'], 'hh_price' => $rate['hh_price'], 'currency' => $rate['currency_id'], 'rate_id' => $rate['id'], 'taxi' => $rate['plus_taxi']);
				}
				else {
					$result[$key][] = array('time' => $rate['time'], 'unit' => $rate['time_unit'], 'price' => $rate['price'], 'hh_price' => $rate['hh_price'], 'currency' => $rate['currency_id'], 'rate_id' => $rate['id'], 'taxi' => $rate['plus_taxi']);
				}
			}
		}

		return $result;
	}

	private function dateDiff($date_start, $date_end)
	{
	    $difference = $date_end - $date_start;
        $minutes = floor(($difference / 3600) * 60); // 3600 seconds in an hour

		return $minutes;
	}

	const HH_STATUS_ACTIVE  = 1;
	const HH_STATUS_PENDING = 2;
	const HH_STATUS_EXPIRED = 3;

	private function makeDate($week_day, $h_from, $h_to)
	{
		$month = date('m', $week_day);
		$day = date('d', $week_day);
		$year = date('Y', $week_day);

		$day_to = $day;
		if ( $h_to < $h_from ) {
			$day_to += 1;
		}
		return array('hh_date_from' => mktime($h_from, 0, 0, $month, $day, $year), 'hh_date_to' => mktime($h_to, 0, 0, $month, $day_to, $year));
	}

	/* ------------>>> END ESCORT PROFILE VALIDATION <<<---------- */
	
	public function simpleCreateAction()
	{
				
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		/*if (!in_array($bu_user->type, array('superadmin', 'admin'))){
			die('no permision');
		}*/
			
		if ( $this->_request->isPost() ) {
			$country_id = Model_Applications::getCountryId();
			$validator = new Cubix_Validator();

			$data = new Cubix_Form_Data($this->_request);
		
			$data->setFields(array(
				'username' => '',
				'password' => '',
				'email' => '',
				'sales_user_id' => 'int',
			));
		
			$data = $data->getData();
			$data['country_id'] = $country_id;						
						
			if ( ! strlen($data['username']) ) {
				$validator->setError('username', 'Required');
			}
			elseif ( $this->model->existsByUsername($data['username'], null) ) {
				$validator->setError('username', 'Already exists');
			}
						
			if (!strlen($data['password']) ) {
				$validator->setError('password', 'Required');
			}
			
			if (!$data['sales_user_id'] ) {
				$validator->setError('sales_user_id', 'Required');
			}
			
			if (!strlen($data['email']) ) {
				$validator->setError('email', 'Required');
			}
			elseif(!$validator->isValidEmail($data['email'])){
				$validator->setError('email', 'Invalid Email');
			}
			/*elseif($this->model->existsByEmail($data['email'], null)){
				$validator->setError('email', 'Email exists');
			}
			elseif ( strlen($data['email']) && Cubix_Application::isDomainBlacklisted($data['email']) ) {
				$validator->setError('email', 'Domain is blacklisted');
			}*/
			
			if ( $validator->isValid() ) {
				
				$m_saver = new Model_Escort_SaverV2();
				$escort_id = $m_saver->simpleSave($data);
			}
			
			$this->view->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender();
			
			echo json_encode($validator->getStatus() + array('escort_id' => $escort_id));
			return;
		}
	}
	
	public function createAction()
	{
		$m_agencies = new Model_Agencies();
		$country_model = new Model_Countries();
		$this->view->agency_list = $m_agencies->getList($this->_getParam('application_id'));
		$this->view->countries = $country_model->getPhoneCountries();
		$this->view->block_countries = in_array(Cubix_Application::getId(), array(APP_BL,APP_EI,APP_A6)) ? $country_model->getCountriesForBl() : $country_model->getCountries();

		$this->view->app_country = Model_Applications::getCountryId();
		$filter_templates = array('application_id' => intval(Cubix_Application::getId()) );
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		if (!in_array($bu_user->type, array('superadmin', 'admin')))
		$filter_templates['sales_user_id'] = $bu_user->id;

		$this->view->escort_templates = Model_EscortTemplates::getTemplates($filter_templates);
		if( Cubix_Application::getId() == 33 || Cubix_Application::getId() == 22 ) {
			//$this->view->travel_continents = Model_Countries::getContinentCountries();
		}
	
		if ( $this->_request->isPost() ) {
            $escortImportId = intval($this->_getParam('escortImportId'));
            if($escortImportId)
                $this->importId = $escortImportId;

			$validator = new Cubix_Validator();

			$data = $this->__validate(false, $validator);
		
			$escort_id = null;
			
			if ( $validator->isValid() ) {
				unset($data['status']);
				$m_saver = new Model_Escort_SaverV2();
			

				if ( Cubix_Application::getId() == APP_ED) {	
					$data['backend_escort'] = 1;
					$data['type'] = $data['login']['type'];
					
					if($data['sync_escort'] == 1 && !$data['last_hand_verification_date']){
						$data['last_hand_verification_date'] = new Zend_Db_Expr('NOW()');
						$data['hand_verification_sales_id'] = $data['login']['sales_user_id'];
					}elseif (isset($data['last_hand_verification_date']) && strlen($data['last_hand_verification_date']) == 0)
                    {
                        unset($data['last_hand_verification_date']);
                    }
				}

				if(in_array(Cubix_Application::getId(),array(APP_ED,APP_EG_CO_UK)))
                {
					$expirationDate = null;
					if( $data['exp_date'] )
					{
						$expirationDate = $data['exp_date'];
					}
					unset($data['exp_date']);
				}

				if(in_array(Cubix_Application::getId(), array(APP_ED,APP_EG_CO_UK,APP_EG,APP_AE))){ 
					if ( $data['agency_id'] ) {
						$source_url = $this->_request->source_url;	
						$source_url = str_replace(' ', '', $source_url);
						if(strlen($source_url) < 1){
							$source_url = null;
						}
						$data['source_url'] = $source_url;
					}elseif ($escortImportId)
                    {
                        $source_url = $this->_request->source_url;
                        $source_url = str_replace(' ', '', $source_url);
                        if(strlen($source_url) < 1){
                            $source_url = null;
                        }
                        $data['source_url'] = $source_url;
                    }
				}

         		$escort_id = $m_saver->save(new Model_EscortV2Item($data));

                if ($escortImportId && in_array(Cubix_Application::getId(), array(APP_ED,APP_A6)))
                {
                    $model_import = new Model_Importer();
                    $model_import->updateStatus($escortImportId,$escort_id);

                    $images = $this->_getParam('images');
                    if (count($images))
                    {
                        foreach ($images as $image)
                        {
                            $this->uploadHtml5FromDataAction($escort_id,$image);
                        }
                    }

                    $video = $this->_getParam('video');

                }


                /* getting bubble text value from form when we are saving imported escort */
                $forme = new Cubix_Form_Data($this->_request);
                $forme->setFields(array('bubble_text' => ''));
                $bubbleTextData = $forme->getData();

                if(!empty($bubbleTextData) && !empty($escort_id)) {

                    $bubble_text = $bubbleTextData["bubble_text"];

                    $bubble_text_model = new Model_Bubbles();
                    $bubble_text_model->insertNewBubbleText($escort_id, $bubble_text);
                }
                /* .......................... */

				$auto_approval = $data['login']['auto_approval'];
				if ( Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT ) {
					$data['phone_changed'] = 1;
					unset($data['fake_city_id']);
					unset($data['fake_zip']);
				}

				if(in_array(Cubix_Application::getId(), array(APP_ED,APP_EG_CO_UK,APP_EG,APP_AE))){	
					unset($data['source_url']);
				}	

				unset($data['login']);
				unset($data['vacation']);
				unset($data['tours']);
                unset($data['block_website']);
				unset($data['check_website']);
				unset($data['is_pornstar']);
				unset($data['bad_photo_history']);
				unset($data['stays_verified']);
				unset($data['premium_cities']);
				unset($data['mark_not_new']);
				unset($data['last_hand_verification_date']);
				unset($data['hand_verification_sales_id']);

				if(in_array(Cubix_Application::getId(), array(APP_BL))){	
					unset($data['is_vaccinated']);
				}

				$m_escorts = new Model_Escort_ProfileV2(array('id' => $escort_id));
								
				unset($data['phone_prefix']);
				
        		unset($data['hh_date_from']);
				unset($data['hh_date_to']);
				unset($data['hh_motto']);
				unset($data['hh_save']);
				unset($data['hh_rates']);
				unset($data['has_hh']);
				unset($data['no_renew_sms']);
				unset($data['show_popunder']);
				
				unset($data['at_exact_date']);
				unset($data['activation_date']);
				
				//Detecting if escort created through applications sync
				unset($data['sync_escort']);

				unset($data['backend_escort']);
				unset($data['disable_when_tour_ends']);
			
				$m_escorts->update($data);

				/*if ( Cubix_Application::getId() == APP_ED ) {
					unset($data['contact_phone_free']);
				}*/

				if ( ! $auto_approval ) {
					$m_escorts->saveRevision();
				}

				$_status = new Cubix_EscortStatus($escort_id);
				$_status->save();

				//$this->_forward('index', 'cron', 'billing');

				$mOrders = new Model_Billing_Orders();

				$user_id = $this->model->getUserId($escort_id);

                if(in_array(Cubix_Application::getId(),array(APP_ED,APP_EG_CO_UK)))
                {
                    if($expirationDate)
                    {
                        $mu = new Model_Users();
                        $mu->setExpirationDate( $user_id, $expirationDate ,'edit');
                    }
                }

				$mOrders->processUserOrders($user_id);
				
				if( Cubix_Application::getId() == APP_ED || Cubix_Application::getId() == 33 || Cubix_Application::getId() == 22 ) {
					$client = Cubix_Api_XmlRpc_Client::getInstance();
					//$results = $client->call('Escorts.updateTravelCountries', array($escort_id, $this->_request->travel_countries));
					$results = $client->call('Escorts.updateTravelPlace', array($escort_id, $this->_request->travel_place));
				}

				//Update profile type Single or DUO
				if ( in_array(Cubix_Application::getId(), array(APP_EF, APP_6A)) ) {
					$profile_type = (int) $this->_request->profile_type;				
					$this->model->updateField($escort_id, 'profile_type', $profile_type);
				}
			}
			
			$this->view->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender();

            if ($escortImportId && in_array(Cubix_Application::getId(), array(APP_ED,APP_A6)))
            {
                echo json_encode($validator->getStatus());
            }else{
                echo json_encode($validator->getStatus() + array('escort_id' => $escort_id));
            }
			return;
		}

        if($this->_request->getParam('isFast')){
            $this->renderScript('escorts-v2/fast-create.phtml');
        }
	}
	
	private function UserVideo($id)
	{
		$this->view->config = Zend_Registry::get('videos_config');
		$video_model = new Model_Video();
		$this->view->video	= $video_model->editVideo($id);
		$status = Model_Video::GetVideoStatusCount($id);
		$video = new Cubix_ParseVideo(null,$id,$this->view->config);
		$this->view->hide_video = $video->CheckVideoStatus($status) !== false ? 1 : 0;
	}
	
	public function sendActivationCodeAction(){
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		$this->view->layout()->disableLayout();
		
		if(Cubix_Application::getId() == APP_EF && in_array($bu_user->type, array('sales manager','superadmin','admin'))){
			$escort_id = intval($this->_getParam('escort_id'));
			$result = Model_EscortsV2::createActivationCode($escort_id);
			exit();
		}

	}
	public function editAction()
	{
		$id = intval($this->_getParam('id'));


		if ( ! $this->user->hasAccessToEscort($id) ) die('Permission denied');

		$modelProducts = new Model_Products();

		$m = new Model_Escorts();
		if(Cubix_Application::getId() == APP_A6){
			$m->logOpener($id);
		}

		$user_id = $this->model->getUserId($id);

		$this->view->last_ip = $m->getLastIP($id);
	
		$escort = $this->model->getProfile($id);

		if( Cubix_Application::getId() == APP_ED ){
            $this->view->package_country = $this->model->getPackageCountryId($id);
        }

        if( in_array(Cubix_Application::getId(), array(APP_EG_CO_UK,APP_ED)) )
        {
            $this->view->tags = Model_Tags::getAll();
            $this->view->escortTags = $this->model->getEscortTags($id);
        }

		$this->UserVideo($id);
		
		$add_data = $this->model->getAdditional($id, true);


		$this->view->ip_info = Model_Users::getUniqueIps($add_data->user_id);
		$escort = array_merge((array) $escort, (array) $add_data);


        $this->view->host =  Cubix_Application::getById(Cubix_Application::getId())->host;
		if( Cubix_Application::getId() == 33 || Cubix_Application::getId() == 22 ) {
			$this->view->travel_continents = Model_Countries::getContinentCountries();
		}

		if ( in_array(Cubix_Application::getId(), array(APP_BL)) ) {
			$this->view->chat_fake_schedule = $this->model->getChatFakeSchedule($id);
		}

        if ( in_array(Cubix_Application::getId(), array(APP_ED,APP_EG_CO_UK)) ) {
            $this->view->expiration_date = Model_Users::getExpirationDate($user_id);
        }

		if ( in_array(Cubix_Application::getId(), array(APP_ED,APP_EG_CO_UK,APP_EG,APP_AE)) && !empty($escort['agency_id']) ) {
			$this->view->is_agency_escort = true;
		}

		if ( Cubix_Application::getId() == APP_A6 ) {
			$client = Cubix_Api_XmlRpc_Client::getInstance();
			$this->view->send_online_now_sms = $client->call('Escorts.getSendOnlineNowSms', array($id));
		}

		if( Cubix_Application::getId() == APP_ED || Cubix_Application::getId() == APP_EM || Cubix_Application::getId() == APP_PC ) {
			$client = Cubix_Api_XmlRpc_Client::getInstance();
			//$this->view->selected_travel_countries = $client->call('Escorts.getTravelCountries', array($id));
			$this->view->travel_place = $client->call('Escorts.getTravelPlace', array($id));
		}

		if ( Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT) {
			$this->view->has_add_area = false;
			$modelPackages = new Model_Billing_Packages();
			$result = $modelPackages->checkIfHasAddAreas($id);


			if ( $result ) {
				$this->view->has_add_area = true;
				$this->view->add_areas_count = $result->add_areas_count;
				$active_order_package_id = $result->order_package_id;
				$this->view->add_areas = Cubix_Api::getInstance()->call('addareas_getAddAreas', array($id, $active_order_package_id));
			}
		}
		$model_packages = new Model_Packages();
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		if(Cubix_Application::getId() == APP_A6 && $model_packages->hasActivePackage($id) && $bu_user->type != 'superadmin'){
			$this->view->not_editable_showname = $model_packages->hasActivePackage($id);
		}
		else{
			$this->view->not_editable_showname = false;
		}

		if ( in_array(Cubix_Application::getId(), array(APP_EF)) && empty($escort['agency_id']) ) {
			$db = Zend_Registry::get('db');
			$checkPhoneConfirm = $db->fetchOne('SELECT true FROM confirm_phone_number WHERE escort_id = ? AND status = ?', array($id,1));
			if(!$checkPhoneConfirm){
				$this->view->phone_not_confirmed = true;
			}
		}

		if ( in_array(Cubix_Application::getId(), array(APP_EF, APP_6A)) ) {
			$this->view->profile_type = $this->model->getField($id, 'profile_type');
		}

		if ( in_array(Cubix_Application::getId(), array(APP_ED, APP_EG_CO_UK)) ) {
			$this->view->disable_when_tour_ends = $this->model->getDisableWhenTourEnds($id);
		}
        $back_user = Zend_Auth::getInstance()->getIdentity();

		if ( $this->_request->isPost() ) {

			$validator = new Cubix_Validator();

			$data = $this->__validate(true, $validator);

			if ( $validator->isValid() ) {

			    // in case if bace city changed but country doesn't
			    if (Cubix_Application::getId() == APP_ED && $back_user->type == 'superadmin'){
                    $m_cities = new Model_Geography_Cities();
                    $base_city = $m_cities->get($data['city_id']);
                    if ($base_city->country_id != $data['country_id']){
                        $data['country_id'] = $base_city->country_id;
                    }
                }

			    if ( in_array(Cubix_Application::getId(),array(APP_ED,APP_EG_CO_UK)) && $data['status'] & ESCORT_STATUS_ADMIN_DISABLED )
                {
                    $session_id = $this->model->getSessionId($user_id);
                    if( $session_id )
                    {
                        if ( Cubix_Application::getId() == APP_EG_CO_UK )
                        {
                            session_id($session_id);
                            session_start();
                            session_destroy();
                            session_commit();
                        }else{
                            $memcache_obj = new Memcache;
                            $memcache_obj->connect('memcached', 11214);
                            $memcache_obj->delete($session_id);
                        }
                    }
                }

			    //Update escort's tags
			    if( in_array(Cubix_Application::getId(), array(APP_EG_CO_UK,APP_ED)) && isset($data['tags']))
                {
                    if (!empty($data['tags']))
                    {
                        $tagsModel = new Model_Tags();
                        $tagsModel->saveEscortsTags($data['tags'],$id);
                    }
                    unset($data['tags']);
                }
                
                //Update profile type Single or DUO
				if ( in_array(Cubix_Application::getId(), array(APP_EF, APP_6A)) ) {
					$profile_type = (int) $this->_request->profile_type;
					$this->model->updateField($id, 'profile_type', $profile_type);
					Cubix_SyncNotifier::notify($id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED,array(
					   'profile_type'=>"*** profile type updated (".$profile_type.")"
                    ));
				}


				if ( Cubix_Application::getId() == APP_ED && ($bu_user->type == 'superadmin' || $bu_user->type == 'admin') ) {
				    $entry_user = $data['login']['data_entry_id'];
                    $this->model->updateField($id, 'data_entry_user', $entry_user);
                    unset($data['login']['data_entry_id']);
                }

				if(in_array(Cubix_Application::getId(),array(APP_ED,APP_EG_CO_UK)))
                {
                    if($data['exp_date'])
                    {
                        $mu = new Model_Users();
                        $mu->setExpirationDate( $user_id, $data['exp_date'] ,'edit');
                        Cubix_SyncNotifier::notify($id, Cubix_SyncNotifier::EVENT_ESCORT_EXPIRATION_UPDATED, array('Bo User' => $back_user->id, 'Expiration date' => 'Updated'));
                    }
                    unset($data['exp_date']);
                }

				if(Cubix_Application::getId() == APP_ED && $data['sync_escort'] == 1 && !$data['last_hand_verification_date']){
					$data['last_hand_verification_date'] = new Zend_Db_Expr('NOW()');
					$data['hand_verification_sales_id'] = $data['login']['sales_user_id'];
				}elseif (isset($data['last_hand_verification_date']) && strlen($data['last_hand_verification_date']) == 0)
                {
                    unset($data['last_hand_verification_date']);
                }
                /**
				 * @author GooGo
				 */
				$model_photos = new Model_Escort_Photos();
				$fake_picture_disabled = $data['login']['fake_picture_disabled'];
                $is_suspicious = $data['login']['is_suspicious']; // 0|1|2

                if ( Cubix_Application::getId() == APP_A6 ) {
                	$send_online_now_sms = (int) $this->_request->send_online_now_sms;
                	if ( ! $send_online_now_sms ) $send_online_now_sms = 0;
					$client = Cubix_Api_XmlRpc_Client::getInstance();
					$client->call('Escorts.setSendOnlineNowSms', array($id, $send_online_now_sms));
				}

				$old_susp = $this->model->getSuspiciousStatus($id);
				$tpl_data = array('ID' => $id);

				if ($is_suspicious == 1 && $old_susp != $is_suspicious && $bu_user->email)
				{
					//Cubix_Email::send($bu_user->email, 'Escort was marked suspicious', 'Escort ID ' . $id . ' was marked suspicious, she has to make verification');
					Cubix_Email::sendTemplate('marked_susp', $bu_user->email, $tpl_data);
				}
				elseif ($is_suspicious == 2 && $old_susp != $is_suspicious && $bu_user->email)
				{
					//Cubix_Email::send($bu_user->email, 'Escort was marked under investigation', 'Escort ID ' . $id . ' was marked under investigation, she has 48h to make verification');
					if(Cubix_Application::getId() == APP_ED){
						if($this->_request->agency_id){
							$agency = Cubix_Api::getInstance()->call('customSelect', array( 'name' , 'agencies', array('id' => $this->_request->agency_id) ));
							Cubix_Email::sendTemplate('marked_under_investigation_agency_v1', $this->_request->reg_email,array('agency'=>$agency['name'], 'escort_showname' => $this->_request->showname ));
						}else{
							Cubix_Email::sendTemplate('marked_under_investigation_v1', $this->_request->reg_email, array('showname'=> $this->_request->showname));
						}

						//Cubix_Email::sendTemplate('marked_under_invest', $bu_user->email, $tpl_data);
					}else{
						Cubix_Email::sendTemplate('marked_under_invest', $bu_user->email, $tpl_data);
					}
				}

				if ($is_suspicious) {
					$model_photos->setSuspicous($id, $is_suspicious);

					/* for 6a checking vip status */
					if (Cubix_Application::getId() == APP_6A)
					{
						$m_v_v = new Model_Verifications_Vip();

						$vip = $m_v_v->getLastRequestByEscort($id);

						if ($vip->status == 2) //verified
						{
							$m_v_v->setStatus($vip->id, 4, $bu_user->id);
							$m_v_v->updateComment(array('id' => $vip->id, 'comment' => 'System: escort has been set suspicious or under investigation'));
						}
					}
					/**/
				}
				elseif (Model_EscortsV2::isSuspicious($id) && !$is_suspicious){
					$model_photos->unsetSuspicous($id);
				}

				if( $escort['contact_phone_parsed'] != $data['contact_phone_parsed'] ){
					$data['phone_changed'] = 1;
					$data['old_phone'] = $escort['contact_phone_parsed'];
					//Cubix_SyncNotifier::notify($id, Cubix_SyncNotifier::EVENT_ESCORT_PHONE_CHANGED ,array( 'before'=> $escort['contact_phone_parsed'],'after' => $data['contact_phone_parsed']));
				}

				$status = $data['status'];
                // Send mail when status change offline
                if(in_array(Cubix_Application::getId(), [APP_EG_CO_UK, APP_ED])){
                    $is_offline = $this->model->hasStatusBit($id, Model_EscortsV2::ESCORT_STATUS_OFFLINE);

                    $text = 'Hello, it seems that something is wrong with your profile ' . $data['showname'] .
                        ' This usually means that the text or other information in your profile are breaking our guidelines.' .
                        ' In order to see exactly what is wrong and fix it, please contact us using the support' .
                        ' section or write us at info@escortguide.co.uk';

                    $agencyText = 'Hello, it seems that something is wrong with your profile ' . $data['showname'] .
                        ' This usually means that the text or other information in your profile'.
                        ' are breaking our guidelines. In order to see exactly what is wrong and' .
                        ' fix it, please contact us using the support section or write us at info@escortguide.co.uk';

                    $subject = 'Your profile was not approved!';

                    if (Cubix_Application::getId() == APP_ED) {
                        $avalableLangs = Cubix_I18n::getLangs();
                        $lng_ = 'en';
                        $escortLangs_ = $escort['langs'];
                        if(!empty($escortLangs_)) {
                            foreach($escortLangs_ as $escortLang) {
                                if(in_array($escortLang['lang_id'], $avalableLangs)) {
                                    $lng_ = $escortLang['lang_id'];
                                    break;
                                }
                            }
                        }

                        $text = $agencyText = Cubix_I18n::translateByLng($lng_, 'profile_not_approved_email_text', array('showname' => $data['showname']));
                        $subject = Cubix_I18n::translateByLng($lng_, 'your_profile_was_not_approved');
					}

                    if ( !$is_offline && $status == Model_EscortsV2::ESCORT_STATUS_OFFLINE) {
                        if (is_numeric($data['agency_id'])) {
                            $agency = Cubix_Api::getInstance()->call('customSelect', array('user_id', 'agencies', array('id' => $data['agency_id'])));
                            $agency_mail = Cubix_Api::getInstance()->call('customSelect', array('email', 'users', array('id' => $agency['user_id'])));
                            Cubix_Email::send($agency_mail['email'], $subject, $agencyText);
                        } elseif (!empty($data['email'])) {
                            Cubix_Email::send($data['email'], $subject, $text);
                        }elseif ( isset($data['login']['reg_email'])  ) {
                        	//&& !empty(isset($data['login']['reg_email']) )
                            Cubix_Email::send($data['login']['reg_email'], $subject, $text);
                        }
                    }



                    //EGUK-192,EDIR-704
                    if ($status & ESCORT_STATUS_DELETED || $status & ESCORT_STATUS_ADMIN_DISABLED)
                    {

                        try {
                            $m_n_i = new Cubix_Newsman_Ids();
                            $n_ids = $m_n_i->get(Cubix_Application::getId());

                            $list_id = reset(array_keys($n_ids));

                            $nm = new Cubix_Newsman($_SERVER["REMOTE_ADDR"]);
                            $nm->unsubscribe($list_id, $data['email']);
                        }
                        catch (Exception $e)
                        {
                            var_dump($e);
                        }
                    }

                }
                // Send mail when status change offline

				/*  if(Cubix_Application::getId() == APP_ED && ($status == ESCORT_STATUS_ADMIN_DISABLED || $status == ESCORT_STATUS_DELETED)){
					if(is_numeric($data['agency_id'])){
						$agency = Cubix_Api::getInstance()->call('customSelect', array( 'name' , 'agencies', array('id' =>$data['agency_id']) ));
						Cubix_Email::sendTemplate('admin_disabled_agency_v1', $data['email'],
						array(
							'agency'=> $agency['name'],
							'escort_showname'=> $data['showname']
						));
					}else{
						$date = Cubix_Api::getInstance()->call('customSelect', array( 'last_login_date' , 'users', array('email' =>$data['email']) ));
						if($date['last_login_date']){
							Cubix_Email::sendTemplate('admin_disabled_v1', $data['email'], array('showname'=> $data['showname']));
						}
					}
				}   */

				if ( $fake_picture_disabled ) {
					$status = Cubix_EscortStatus::ESCORT_STATUS_FAKE_PICTURE_DISABLED;
				}
				$blackListModel = new Model_BlacklistedWords();
                $data['about_has_bl'] = 0;
				foreach ( Cubix_I18n::getLangs(true) as $lng ) {
					if($blackListModel->checkWords($data['about_' . $lng],Model_BlacklistedWords::BL_TYPE_ABOUT )){
						$data['about_has_bl'] = 1; BREAK;
					}

				}
				unset($data['login']['fake_picture_disabled']);
				unset($data['status']);
				unset($data['sync_escort']);

                /* bubble text generates when we create escort from imported escorts, there we need to unset it
                because we edit already created escort */
                unset($data['bubble_text']);


				if(Cubix_Application::getId() == APP_A6 && $model_packages->hasActivePackage($id) && $bu_user->type != 'superadmin'){
					$changedLocationModel = new Model_ChangedLocation();
					$oldCityId = $changedLocationModel->checkChangedRegion($id,$data['city_id']);
					if($oldCityId){
						$changedData = array();
						$changedData['escort_id'] = $id;
						$changedData['sales_id'] = $bu_user->id;
						$changedData['new_city_id'] = $data['city_id'];
						$changedData['old_city_id'] = $oldCityId;
						$changedData['change_date'] = date('Y-m-d H:i:s');
						$changedData['active_package'] = $model_packages->getActivePackage($id);

						$changedLocationModel->add($changedData);
					}
				}

                $appId = intval(Cubix_Application::getId());
                $mustUpdateSourceUrl =  (bool)(in_array($appId, array(APP_EG, APP_AE)) && (isset($escort['agency_id']) && $escort['agency_id'] > 0) OR in_array($appId, [APP_ED, APP_EG_CO_UK]) );
                if ($mustUpdateSourceUrl) {
                    $source_url = $this->_request->source_url;
                    $source_url = str_replace(' ', '', $source_url);
                    if (strlen($source_url) < 1) {
                        $source_url = null;
                    }

                    $data['source_url'] = $source_url;
                    $data['source_url_doesnt_have'] = $this->_request->source_url_doesnt_have;
                }

				if((Cubix_Application::getId() == APP_EF)){
					if($data['login']['password'] != ''){
						$client = Cubix_Api_XmlRpc_Client::getInstance();
						$client->call('Users.passChangeNotRequired', array($data['login']['username']));
					}
				}

				$m_saver = new Model_Escort_SaverV2();

				// Saves escorts, tours and escort_slogans tables
				$escort_id = $m_saver->save(new Model_EscortV2Item($data));

                if (Cubix_Application::getId() == APP_EG_CO_UK)
                {
                    $data['old_city_id'] = $escort['city_id'];
                    $data['old_cities_ids'] = $escort['cities'];
                }

				if (in_array(Cubix_Application::getId(), array(APP_EG,APP_AE))){
					unset($data['source_url']);
					unset($data['source_url_doesnt_have']);
				}

				if ( in_array(Cubix_Application::getId(), array(APP_BL)) ) {
					$data['fake_chat']['user_id'] = $escort['user_id'];
					$this->model->updateChatFakeSchedule($data['fake_chat']);
					unset($data['fake_chat']);
				}

				if ( Cubix_Application::getId() == APP_A6 ) {
					unset($data['fake_city_id']);
					unset($data['fake_zip']);
				}

				unset($data['disable_when_tour_ends']);

                unset($data['photo_suspicious_date']);
				unset($data['login']['suspicious_comment']);
				unset($data['login']['reg_email']);

				unset($data['hh_date_from']);
				unset($data['hh_date_to']);
				unset($data['hh_motto']);
				unset($data['hh_rates']);
				unset($data['hh_save']);
				unset($data['has_hh']);
				unset($data['no_renew_sms']);
                unset($data['show_popunder']);
				unset($data['mark_not_new']);
				unset($data['last_hand_verification_date']);
				unset($data['hand_verification_sales_id']);
				
                $_status = new Cubix_EscortStatus($escort_id);

				$auto_approval = $data['login']['auto_approval'];

				if ( Cubix_Application::getId() == APP_ED ) {
					$data['type'] = $data['login']['type'];
                }

				unset($data['login']);

				unset($data['vacation']);
				unset($data['tours']);

                unset($data['block_website']);
				unset($data['check_website']);
				unset($data['is_pornstar']);
				unset($data['bad_photo_history']);
				unset($data['stays_verified']);
				unset($data['about_me_informed']);

				if ( Cubix_Application::getId() == APP_BL ) {
					unset($data['is_vaccinated']);
                }

				if ( $this->view->has_add_area && ( Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT) ) {
					Cubix_Api::getInstance()->call('addareas_updateEscortAreas', array($data['premium_cities'], $id, $active_order_package_id));
				}

				// Reset premium cities, if Country US Or France
				if(Cubix_Application::getId() == APP_ED){
                    $client = Cubix_Api_XmlRpc_Client::getInstance();
				    if($data['country_id'] == 68 || $data['country_id'] == 23){

                        $client->call('Escorts.resetPremiumCities', array($id));
                    }else{
                        $escort_old_city_data = $this->model->getProfile($id);

                        $escort_old_city_id = $escort_old_city_data['city_id'];

                        if($escort_old_city_id != $data['city_id']){
                            $client->call('Escorts.restorePremiumCities', array($id,$data['country_id'],$escort_old_city_id,$data['city_id']));
                        }
                    }
                }

				unset($data['premium_cities']);

				//unset($data['due_date']);

				$m_escorts = new Model_Escort_ProfileV2(array('id' => $id));

                unset($data['phone_prefix']);
                unset($data['phone_prefix_1']);
                unset($data['phone_prefix_2']);
				unset($data['latitude']);
                unset($data['longitude']);
				$m_escorts->update($data);
				/*if ( Cubix_Application::getId() == APP_ED ) {
					unset($data['contact_phone_free']);
				}*/

				if ( ! $auto_approval ) {
					$m_escorts->saveRevision();
				}
				else {
					$m_snapshot = new Model_Escort_SnapshotV2(array('id' => $id));
					$m_snapshot->snapshotProfileV2();
				}

				if( Cubix_Application::getId() == APP_ED || Cubix_Application::getId() == APP_EM || Cubix_Application::getId() == APP_PC ) {
					//$results = $client->call('Escorts.updateTravelCountries', array($id, $this->_request->travel_countries));
					$results = $client->call('Escorts.updateTravelPlace', array($id, $this->_request->travel_place, $bu_user->id));
				}

				/**
				 * @author GooGo
				 */

				if ( $status ) {
//					
					$_status->setStatus($status);

					$_status->save();

					// system disabled
					if ($add_data->user_type == 'escort' && in_array($status, array(Cubix_EscortStatus::ESCORT_STATUS_OWNER_DISABLED, Cubix_EscortStatus::ESCORT_STATUS_ACTIVE)))
					{
						$mu = new Model_Users();
						$mu->setSystemDisabled($add_data->user_id, 0);
					}
				}

				/**
				 * @author Tiko
				 */
				$allow_rev = $this->_request->allow_rev;

				if ($allow_rev)
				{
					if ($allow_rev == 'no')
						$v = 1;
					else
						$v = 0;
					if ($escort['disabled_reviews'] != $v){
					    $this->model->updateField($id, 'disabled_reviews', $v);
					    Cubix_SyncNotifier::notify($id, Cubix_SyncNotifier::EVENT_ESCORT_REVIEW_TOGGLED, array('escort_id' => $id, 'disabled_reviews'=>$v));
					}
				}

                if (in_array(Cubix_Application::getId(), array(APP_EG_CO_UK))) {
                    $sync_aprooved = intval($this->_request->sync_aprooved);
                    if (in_array($sync_aprooved, array(0, 1))) {
                        $this->model->updateField($id, 'sync_aprooved', $sync_aprooved);
                        Cubix_SyncNotifier::notify($id, Cubix_SyncNotifier::EVENT_ESCORT_REVIEW_TOGGLED, array('escort_id' => $id, 'sync_aprooved' => $sync_aprooved));
                    }
                }

				$allow_com = $this->_request->allow_com;

				if ($allow_com)
				{
					if ($allow_com == 'no')
						$v = 1;
					else
						$v = 0;
					if ($escort['disabled_comments'] != $v){
                        $this->model->updateField($id, 'disabled_comments', $v);
                        Cubix_SyncNotifier::notify($id, Cubix_SyncNotifier::EVENT_ESCORT_COMMENT_TOGGLED, array('escort_id' => $id, 'disabled_comments'=>$v));
                    }
				}

                /* changing sale agents for US and France when country changes and adding logs */
                if(!empty($data['country_id']) && in_array($data['country_id'], array(68, 23)) && in_array(Cubix_Application::getId(),array(APP_ED))){

                    $model_user = new Model_Users();
                    $user_obj = $model_user->get($user_id);
                    $user_type = $user_obj->user_type;
                    $current_sales_user_id = $user_obj->sales_user_id;

                    $model_user->changeSalesByCountry($user_id, $data['country_id'], $back_user->id, $current_sales_user_id, $user_type);
                }
			}

			$this->view->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender();

			echo json_encode($validator->getStatus());return;
		}
		else {
			$m_agencies = new Model_Agencies();
			$this->view->agency_list = $m_agencies->getList($this->_getParam('application_id'), false);

			$this->view->back_user_id = $back_user->id;
			$countyModel = new Model_Countries();
			$this->view->countries = $countyModel->getPhoneCountries();

			if(in_array(Cubix_Application::getId(), array(APP_BL,APP_EI,APP_A6))){
                $this->view->block_countries = $countyModel->getCountriesForBl();
            }else{
                $this->view->block_countries = $countyModel->getCountries();
            }


			$this->view->phone_prefix_id = $escort['phone_country_id'];
			$this->view->phone_prefix_id_1 = $escort['phone_country_id_1'];
			$this->view->phone_prefix_id_2 = $escort['phone_country_id_2'];
			//FOR Calling Code
			/*if($escort[phone_number]){
				if(preg_match('/^(\+|00)/',trim($escort[phone_number])))
				{
					$phone_prfixes = $countyModel->getPhonePrefixs();
					$phone_prefix_id = NULL;
					$phone_number = preg_replace('/^(\+|00)/', '',trim($escort[phone_number]));
					foreach($phone_prfixes as $prefix)
					{
						if(preg_match('/^('.$prefix->phone_prefix.')/',$phone_number))
						{
							$phone_prefix_id = $prefix->id;
							$escort[phone_number] = preg_replace('/^('.$prefix->phone_prefix.')/', '',$phone_number);
							BREAK;
						}

					}
					$this->view->phone_prefix_id = $phone_prefix_id;
				}
			}*/

			//
//            var_dump($escort);
			$escort = new Model_EscortV2Item($escort);
//
            if (in_array(Cubix_Application::getId(), array(APP_EG_CO_UK)))
            {
                $sync_aprooved = $escort->getSyncAproovment();
                if ($sync_aprooved == 1) {
                    $this->view->sync_aprooved = 'yes';
                }else{
                    $this->view->sync_aprooved = 'no';
                }
            }

            if (in_array(Cubix_Application::getId(), array(APP_ED)))
                $this->view->first_login_lang = $escort->getFirstLoginLang();

            $this->view->rates = $this->reconstructRates($this->model->getRates($id));
			$this->view->hh_status = Cubix_Api::getInstance()->call('getHappyHourStatus', array($id));

			$filter_templates = array('application_id' => intval(Cubix_Application::getId()) );
			$bu_user = Zend_Auth::getInstance()->getIdentity();
//			var_dump($filter_templates);die;
			if (!in_array($bu_user->type, array('superadmin', 'admin')))
			$filter_templates['sales_user_id'] = $bu_user->id;

			if (in_array(Cubix_Application::getId(),array(APP_ED, APP_EG_CO_UK, APP_EG, APP_6C, APP_6B)))
            {
                $this->view->verification_data = $this->model->getHandVerificationDate($escort->id);
            }

            $this->view->escort_templates = Model_EscortTemplates::getTemplates($filter_templates);

			$home_city_id = null;
			if ( isset($escort->home_city_id) ) {
				$home_city_id = $escort->home_city_id;
			}

			$home_city_country = '';
			if ( $home_city_id ) {
				$m_city = new Cubix_Geography_Cities();
				$home_city = $m_city->get($home_city_id);

				$m_country = new Cubix_Geography_Countries();
				$home_country = $m_country->get($home_city->country_id);

				$home_city_country = $home_city->{'title_' . $this->_request->lang_id} . ' (' . $home_country->{'title_' . $this->_request->lang_id} . ')';
			}
			$this->view->home_city_country = $home_city_country;

			//Getting blacklisted words and highlighting in text
			if ( $escort->about_has_bl == 1 ) {

				$langs = Cubix_I18n::getLangs();
				foreach($langs as $lang_id){
					$black_words_model = new Model_BlacklistedWords();
					$black_words = $black_words_model->getAllWordsByType(Model_BlacklistedWords::BL_TYPE_ABOUT);

					foreach($black_words as $word) {
						if ( ! strlen($word->name) ) continue;

						if($word->search_type == Model_BlacklistedWords::BL_SEARCH_TYPE_CONT){
							$pattern = '/' . preg_quote($word->name, '/') . '/';
							$escort->{'about_' . $lang_id} = preg_replace($pattern, '<abbr class = "black-listed" >' . $word->name . '</abbr>', $escort->{'about_' . $lang_id});
						}
						else{
							$pattern = '/(\W+)' . preg_quote($word->name, '/') . '(\W+)/';
							$escort->{'about_' . $lang_id} = preg_replace($pattern, '$1<abbr class = "black-listed" >' . $word->name . '</abbr>$2', $escort->{'about_' . $lang_id});
						}


					}
				}

			}


//			print_r($escort);die;
            if (in_array(Cubix_Application::getId(),array(APP_ED)))
            {
                $escort->keywords = Model_EscortsV2::getEscortKeywords($escort->id);
            }
			$this->view->escort = $escort;

			$this->view->b_active_package = $this->model->getActivePackage($id, false);

			/* LOAD ZONES */
			$param_cities = array();
			if ( count($escort->cities) ) {
				foreach ( $escort->cities as $city ) {
					$param_cities[] = $city['city_id'];
				}
			}

			$this->_request->setParam('ajax_cities', implode(',', $param_cities));

			$this->getCityzonesAction();
			$this->view->layout()->enableLayout();
			/* LOAD ZONES */

			$this->view->data = array('rates' => $this->reconstructRates($escort->rates));

			$this->view->data['block_countries'] = $countyModel->getBlockCountries( $id );

			$memberModel = new Model_Members();
			$this->view->data['blocked_members'] = $memberModel->getBlockedMembers($id);

			$photos = $escort->getPhotos();

			$private_photos = array();
			$disabled_photos = array();
			$archived_photos = array();
			foreach ($photos as $k => $photo)
			{
				if ( $photo->type == ESCORT_PHOTO_TYPE_PRIVATE )
				{
					$private_photos[] = $photo;
					unset($photos[$k]);
				} elseif ( $photo->type == ESCORT_PHOTO_TYPE_DISABLED ) {
					$disabled_photos[] = $photo;
					unset($photos[$k]);
				} elseif ( $photo->type == ESCORT_PHOTO_TYPE_ARCHIVED ) {
					$archived_photos[] = $photo;
					unset($photos[$k]);
				}
			}

			//$this->view->rates = $this->view->data['rates'];

			//print_r($escort);
			$this->view->photos = $photos;

			$this->view->private_photos = $private_photos;
			$this->view->disabled_photos = $disabled_photos;
			$this->view->archived_photos = $archived_photos;
			$this->view->history_photos = $escort->getPhotoHistory();
			$this->view->pg_data_pages_count = $escort->getPhotoHistoryCount();

			$this->view->tour_data = $this->model->getTours($id);
			$this->view->tour_history = $tour_history = $this->model->getToursHistory($id);

			$this->view->fake_picture_disabled = $this->model->hasStatusBit($id, Model_EscortsV2::ESCORT_STATUS_FAKE_PICTURE_DISABLED);
			$sort =Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() ==APP_A6_AT?0:1;
			$this->view->suspicious_history = $this->model->getSuspiciousHistory($id,$sort);

			// check no review and no comment
			if (!$escort->ag_disabled_reviews)
			{
				if ($escort->disabled_reviews == 0)
					$this->view->allow_rev = 'yes';
				else
					$this->view->allow_rev = 'no';
			}
			else
				$this->view->allow_rev = null;

			if (!$escort->ag_disabled_comments)
			{
				if ($escort->disabled_comments == 0)
					$this->view->allow_com = 'yes';
				else
					$this->view->allow_com = 'no';
			}
			else
				$this->view->allow_com = null;
			//
		}
	}

	public function editBubbleTextAction()
	{
		$escort_id = $this->view->escort_id = $this->_request->getParam('escort_id');

		$m_escort = new Model_EscortsV2();
		$this->view->bubble_text = $bubble_text = $m_escort->getBubbleText($escort_id);

		if ( $this->_request->isPost() ) {
			$bubble_text = trim($this->_request->getParam('bubble_text'));
			$bubble_text_id = $this->_request->getParam('bubble_text_id');

			$m_escort->updateBubbleText($bubble_text_id, $bubble_text, $escort_id);

			Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_BUBBLE_TEXT);
		}
	}

	public function getActivePackageAction()
	{
		$this->view->layout()->disableLayout();

		$id = intval($this->_getParam('id'));

		$m_escort = new Model_EscortsV2();
		$escort = $m_escort->get($id);

		$active_packages = $escort->getActivePackages();

		$m_o_packages = new Model_Billing_Packages();
		$act_packages = array();
		if ( count($active_packages) > 0 )
		{
			foreach ( $active_packages as $k => $active_package )
			{
				$package = $m_o_packages->get($active_package->id);

				if ( ! is_null($active_package->order_id) ) {
					$products = $package->getProducts();
					$opt_products = $package->getProducts(Model_Products::TYPE_OPTIONAL_PRODUCT);
				}
				else {
					$products = $package->getDefaultProducts();
					$opt_products = $package->getDefaultProducts(Model_Products::TYPE_OPTIONAL_PRODUCT);
				}


				if ( count($products) > 0 ) {
					foreach( $products as $k => $product )
					{
						$products[$k]->name = Cubix_I18n::translate($product->name);
					}
				}

				if ( count($opt_products) > 0 ) {
					foreach( $opt_products as $k => $product )
					{
						$opt_products[$k]->name = Cubix_I18n::translate($product->name);
					}
				}

				$active_package->package_products = $products;
				$active_package->package_opt_products = $opt_products;

				//FIXING PACKAGES LOGIC
				//IF PACKAGE EXPIRES 19 APRIL IT'S ACTUALLY EXPIRES 18 APRIL 23:59
				if ( isset($active_package->expiration_date) && $active_package->expiration_date ) {
					$active_package->expiration_date -= 60*60*24; //-1 day
				}

				$act_packages[] = $active_package;

			}
		}

		$data = array(
			'data' => $act_packages,
			'count' => 0
		);

		echo json_encode($data); die;
	}

	public function packagesHistoryAction()
	{
		$this->view->layout()->disableLayout();

		$id = intval($this->_getParam('id'));

		$m_escort = new Model_EscortsV2();
		$escort = $m_escort->get($id);

		$active_packages = $escort->getAllPackages();

		$m_o_packages = new Model_Billing_Packages();
		$act_packages = array();
		if ( count($active_packages) > 0 )
		{
			foreach ( $active_packages as $k => $active_package )
			{
				$package = $m_o_packages->get($active_package->id);

				if ( ! is_null($active_package->order_id) ) {
					$products = $package->getProducts();
					$opt_products = $package->getProducts(Model_Products::TYPE_OPTIONAL_PRODUCT);
				}
				else {
					$products = $package->getDefaultProducts();
					$opt_products = $package->getDefaultProducts(Model_Products::TYPE_OPTIONAL_PRODUCT);
				}


				if ( count($products) > 0 ) {
					foreach( $products as $k => $product )
					{
						$products[$k]->name = Cubix_I18n::translate($product->name);
					}
				}

				if ( count($opt_products) > 0 ) {
					foreach( $opt_products as $k => $product )
					{
						$opt_products[$k]->name = Cubix_I18n::translate($product->name);
					}
				}

				$active_package->package_products = $products;
				$active_package->package_opt_products = $opt_products;

				if ( isset($active_package->expiration_date) && $active_package->expiration_date ) {
					$active_package->expiration_date -= 60*60*24;
				}

				$act_packages[] = $active_package;
			}
		}

		$data = array(
			'data' => $act_packages,
			'count' => 0
		);

		echo json_encode($data); die;

		//$this->view->history_packages = array('data' => $act_packages);

	}

	public function getCityzonesAction()
	{
		$this->view->layout()->disableLayout();

		$cities = $this->_request->ajax_cities;
		$cities = trim($cities, ',');

		$cities = explode(',', $cities);

		$cz_model = new Cubix_Geography_Cityzones();

		$all_cityzones = array();
		if ( count($cities) ) {
			foreach( $cities as $city ) {
				$cityzones = $cz_model->ajaxGetAll($city);
				if ( count($cityzones) ) {
					$all_cityzones = array_merge($all_cityzones, $cityzones);
				}
			}
		}

		if ( $this->_request->ajax ) {
			die(json_encode($all_cityzones));
		}
		else {
			$this->view->all_cityzones = $all_cityzones;
		}
	}

	public function editRevisionAction()
	{

        $id = intval( $this->_getParam('id') );
        $revision = intval( $this->_getParam('revision' ) );
        $this->view->revision = $revision;
        $add_data = $this->model->getAdditional( $id, true );
        //if ( ! $this->user->hasAccessToEscort($id) ) die('Permission denied');
        $this->view->last_ip = $ip = ( new Model_Escorts() )->getLastIP( $id );
        $this->view->ip_info = $ips =  Model_Users::getUniqueIps( $add_data->user_id );
        $back_user = Zend_Auth::getInstance()->getIdentity();
        $this->view->back_user_id = $back_user->id;
        $countyModel = new Model_Countries();
		
		if ( $this->_request->isPost() ) {
			$validator = new Cubix_Validator();

			$data = array();
			$data = $this->__validate(true, $validator);

			if ( $validator->isValid() ) {
				/*$m_saver = new Model_Escort_SaverV2();
				$m_saver->save(new Model_EscortV2Item($data));*/

				unset($data['login']);
				unset($data['vacation']);
				unset($data['tours']);
				unset($data['status']);
				unset($data['premium_cities']);
				unset($data['bubble_text']);

				//print_r($data);die;

				$m_escorts = new Model_Escort_ProfileV2(array('id' => $id));
				$m_escorts->update($data, $revision);

				//$m_escorts->saveRevision();

				//$this->model->removeStatusBit($id, Model_EscortsV2::ESCORT_STATUS_PROFILE_CHANGED);
			}

			die(json_encode($validator->getStatus()));
		}
		else {
			$m_agencies = new Model_Agencies();
			$this->view->agency_list = $m_agencies->getList($this->_getParam('application_id'), false);

			$escort = $this->model->getProfile($id, $revision);

			$add_data = $this->model->getAdditional($id);

			$escort = array_merge((array) $escort, (array) $add_data);

			$escort = new Model_EscortV2Item($escort);
			
			$photos = $escort->getPhotos();
			
			$private_photos = array();
			$disabled_photos = array();
			foreach ($photos as $k => $photo)
			{
				if ( $photo->type == ESCORT_PHOTO_TYPE_PRIVATE )
				{
					$private_photos[] = $photo;
					unset($photos[$k]); 
				} elseif ( $photo->type == ESCORT_PHOTO_TYPE_DISABLED ) {
					$disabled_photos[] = $photo;
					unset($photos[$k]);
				}
			}
			
			$this->view->photos = $photos;
			$this->view->private_photos = $private_photos;
			$this->view->disabled_photos = $disabled_photos;
			$this->view->history_photos = $escort->getPhotoHistory();
			//print_r($escort); die;
			$this->view->escort = $escort;
			$this->view->countries = $countyModel->getPhoneCountries();
			$this->view->block_countries = $countyModel->getCountries();
			
			$memberModel = new Model_Members();
			$this->view->blocked_members = $memberModel->getBlockedMembers($id);
            $this->view->phone_prefix_id = $escort->phone_country_id;
            $this->view->phone_prefix_id_1 = $escort->phone_country_id_1;
            $this->view->phone_prefix_id_2 = $escort->phone_country_id_2;
			$this->view->data = array('rates' => $this->reconstructRates($escort->rates));

			$this->view->tour_data = $this->model->getTours($id);
		}
	}

	
	public function deleteAction()
	{
		if ( is_array($this->_request->id) ) {
			$escort_ids = $this->_request->id;
		}
		else {
			$escort_ids = array(intval($this->_request->id));
		}
		
		//if ( ! $this->user->hasAccessToEscort($id) ) die('Permission denied');
		$immed =  isset($this->_request->immed) ? true : false;
		if ( count($escort_ids) > 0 ) {
			foreach( $escort_ids as $escort_id ) {
				
				if ( ! $this->user->hasAccessToEscort($escort_id) )	continue;//die('Permission denied');

				$this->model->delete($escort_id, $immed);

				if (in_array(Cubix_Application::getId(),array(APP_EG_CO_UK,APP_ED)))
                {
                    $escort_email = $this->model->getEmailByEscortId($escort_id);

                    try {
                        $m_n_i = new Cubix_Newsman_Ids();
                        $n_ids = $m_n_i->get(Cubix_Application::getId());

                        $list_id = reset(array_keys($n_ids));

                        $nm = new Cubix_Newsman($_SERVER["REMOTE_ADDR"]);
                        $nm->unsubscribe($list_id, $escort_email);
                    }
                    catch (Exception $e)
                    {
                        var_dump($e);
                    }
                }

				 if(Cubix_Application::getId() == APP_ED ){
					
					$m_escort = new Model_EscortsV2();
					$esc  = $m_escort->get($escort_id);
				
					if(is_numeric($esc->agency_id)){
						$email_template = 'admin_disabled_agency_v1';
					}else{
						$email_template = 'admin_disabled_v1';
					}
					Cubix_Email::sendTemplate( $email_template, $esc->email, 
							array(
								'escort_showname' => $esc->showname,
								'showname' => $esc->showname,
								'agency' => $esc->agency_name,
								'url' => 'http://'.Cubix_Application::getById()->host.'/escort/'.$esc->showname.'-'.$esc->id,
								'subject' => $subject,
								'reason' => $text
							)
					); 
					
				} 
				
			}
		}
		
		die;
	}

	public function revisionPhotosAction()
	{

		$this->view->id = $id = intval($this->_getParam('id'));
		$this->view->is_auto_approve = intval($this->_getParam('auto_approve'));
		if ( ! $this->user->hasAccessToEscort($id) ) die('Permission denied');

		$escort = $this->model->get($id);
		$this->view->escort = $escort;

		$photos = array('public' => array(), 'private' => array(), 'disabled' => array());

		foreach ( $escort->getPhotos() as $photo ) {
				if($phots->is_portrait==0 && !is_null($photo->args))
						$photo->rwidth = $this->GetPhotoWidth('200', $photo->width, $photo->height);
			if ( $photo->type == ESCORT_PHOTO_TYPE_PRIVATE ) {
				$photos['private'][] = $photo;
			} elseif ( $photo->type == ESCORT_PHOTO_TYPE_DISABLED ) {
				$photos['disabled'][] = $photo;
			}
			else {
				$photos['public'][] = $photo;
			}
		}



		$request_m = new Model_Verifications_Requests();
		$request = $request_m->getLastRequest( $id );
		if ( $request ){
			$this->view->idcard_photos = $request->getPhotos();
		}

		$this->view->photos = $photos;
	}

	public function photosAction()
	{
		$this->view->id = $id = intval($this->_getParam('id'));
		$this->view->is_auto_approve = intval($this->_getParam('auto_approve'));
		if ( ! $this->user->hasAccessToEscort($id) ) die('Permission denied');
		
		
		/* GRAB PHOTOS FROM ORIGINAL SERVER */
		set_time_limit(0);
		
		/*$config = Zend_Registry::get('images_config');
		$url = trim($config['remote']['url'], '/');
		get_headers($url . '/migrate_photos.php?id=' . $id);*/
		// ------------------------
		
		
		/*$images = new Cubix_Images();
		
		
		
		try {
			$images->save('C:\Users\GuGo\Desktop\grabber\lia-19\6\4.jpg', 1);
		} catch (Exception $e) {
			
		}
		*/
		
		$escort = $this->model->get($id);
		$this->view->escort = $escort;

		$m_profile = new Model_Escort_ProfileV2(array('id' => $escort->id));
		$latest_revision = $m_profile->getRevision($m_profile->getLatestRevision());
		$revision_country_id = $latest_revision->data['country_id']; 
		if($escort->getCurrentTourCountry($escort->id) == 68 || $escort->country_id == 68 || $revision_country_id == 68){
			$escort->is_in_usa = 1;
		}

		$photos = array('public' => array(), 'private' => array(), 'disabled' => array() );
		
		foreach ( $escort->getPhotos() as $photo ) {
			if ( $photo->type == ESCORT_PHOTO_TYPE_PRIVATE ) {
				$photos['private'][] = $photo;
			} elseif ( $photo->type == ESCORT_PHOTO_TYPE_DISABLED ) {
				$photos['disabled'][] = $photo;
			} elseif ( $photo->type == ESCORT_PHOTO_TYPE_ARCHIVED ) {
				$photos['archived'][] = $photo;	
			} else {
				$photos['public'][] = $photo;
			}
		}
		$photos['photo_history'] = $escort->getPhotoHistory();
		$this->view->photos = $photos;
	}

	public function rotatePhotoAction()
	{
		$degree = intval($this->_getParam('degree'));
		$photo_id = intval($this->_getParam('photo_id'));
		$degree = $degree == 90 ? 90 : -90;
		$escort_id = $this->_getParam('escort_id');
		$hash = $this->_getParam('hash');
		$ext = $this->_getParam('ext');

        $conf = Zend_Registry::get('images_config');
		$__url = $conf['remote']['url'];

		try {
            if( Cubix_Application::getId() == APP_6B ) {
                // use curl if get_headers not working

                $this->__curlCall($__url . "/get_image.php?a=rotate&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash . "&ext=". $ext . "&d=". $degree);

                $data['a'] = 'clear_cache';
                $result = $this->__curlCall($__url . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash);
                //var_dump($result); die;
            } else {
                get_headers($__url . "/get_image.php?a=rotate&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash . "&ext=". $ext . "&d=". $degree);
                get_headers($__url . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash);
            }

			$result = array(
                'x' => 0,
                'y' => 0,
                'px' => 0,
                'py' => 0
            );
            $photo = new Model_Escort_PhotoItem(array(
                'id' => $photo_id
            ));
            $photo->setCropArgs($result);

            $catalog = $escort_id;
            $a = array();
            if ( is_numeric($catalog) ) {
                $parts = array();

                if ( strlen($catalog) > 2 ) {
                    $parts[] = substr($catalog, 0, 2);
                    $parts[] = substr($catalog, 2);
                }
                else {
                    $parts[] = '_';
                    $parts[] = $catalog;
                }
            }
            else if ( preg_match('#^([0-9]+)/(.+)$#', $catalog, $a) ) {
                array_shift($a);
                $catalog = $a[0];

                $parts = array();

                if ( strlen($catalog) > 2 ) {
                    $parts[] = substr($catalog, 0, 2);
                    $parts[] = substr($catalog, 2);
                }
                else {
                    $parts[] = '_';
                    $parts[] = $catalog;
                }

                $parts[] = $a[1];
            }

            $catalog = implode('/', $parts);

            if( Cubix_Application::getId() == APP_6B ) {
                $this->__curlCall($__url . "/" . Cubix_Application::getById()->host . "/" . $catalog . "/" . $hash . "_t100p.".$ext);
            } else {
                get_headers($__url . "/" . Cubix_Application::getById()->host . "/" . $catalog . "/" . $hash . "_t100p.".$ext);
            }

		}
		catch ( Exception $e ) {
			die(json_encode(array('error' => 'An error occured')));
		}

		die(json_encode(array('success' => true)));
	
	}

	public function __curlCall($url) {
        $url = str_replace(array('http://','https://'), '', $url);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        ob_start();

        curl_exec($ch);
        curl_close($ch);
        $response = ob_get_contents();

        ob_end_clean();

        return $response;
    }

	public function photoHistoryAction()
	{
		$this->view->layout()->disableLayout();
		$this->view->id = $id = intval($this->_getParam('id'));

        $this->view->pg_page = $page = $this->_getParam('page') ? intval($this->_getParam('page')) : 1;
        $this->view->pg_per_page = $pg_per_page = 100;

        if ( ! $this->user->hasAccessToEscort($id) ) die('Permission denied');


		/* GRAB PHOTOS FROM ORIGINAL SERVER */
		set_time_limit(0);

		$escort = $this->model->get($id);

		$this->view->history_photos = $escort->getPhotoHistory($page, $pg_per_page);
        $this->view->pg_data_pages_count = $escort->getPhotoHistoryCount();
    }

	public function photoHistoryRemovePermanentlyAction()
	{
		$this->view->layout()->disableLayout();

		$req = $this->_request;
		
		if( !isset($req->photos) && !is_array($req->photos) ) die;

		if ( $this->user->type == "superadmin" || $this->user->type == "admin") {
			
			$images = array();
			$model_photos = new Model_Escort_Photos();

			foreach ( $req->photos as $hash ) {
			
				$photo = $model_photos->gethistoryphoto($hash);

				$images[] = new Cubix_Images_Entry(array(
					'application_id' => Cubix_Application::getId(),
					'catalog_id' => $photo->escort_id,
					'hash' => $photo->hash,
					'ext' => $photo->ext
				));

				$model_photos->removehistoryphoto($hash);
			}
			
			$images_model = new Cubix_Images();
			$res = $images_model->remove($images);

		}
		echo 'Photo(s) Permanently Removed - '; 
		var_dump($res);

		die;
	}
	
	public function photoGalleriesAction()
	{
		$this->view->layout()->disableLayout();
		$id = intval($this->_getParam('id'));
		if ( ! $this->user->hasAccessToEscort($id) ) die('Permission denied');

		$escort = $this->model->get($id);
		$main_gallery = array( 0 => array('id' => 0, 'title' => "Main Gallery" ));
		$galleries = $escort->getPhotoGalleries();
		$galleries = is_null($galleries) ? array() : $galleries;
		$galleries = $main_gallery + $galleries;//array_merge($galleries2,$galleries);
		$this->view->galleries = $galleries;
		$this->view->photos = $escort->getPhotos('gallery_id', "ASC", false, true);
			
	}
	
	public function doPhotosAction()
	{
		$action = $this->_getParam('act');
		$model = new Model_Escort_Photos();
				
		$photos_min_count = Cubix_Application::getById($this->_getParam('application_id'))->min_photos;

		// if ( ! is_array($ids) ) die;

		// Try to fetch escort id of one of the reqested photos
		if( $action != 'adjust' && $action != 'getid'){
			$ids = $this->_getParam('photos');
			$db = Zend_Registry::get('db');
			$escort_id = $db->fetchOne('SELECT escort_id FROM escort_photos WHERE id = ?', reset($ids));
			if ( ! $escort_id ) die('Invalid photo id');
		}

		switch ($action)  {
			case 'public':
			case 'private':
			case 'archived':
			case 'disabled':
				foreach ( $ids as $id ) {
					$id = intval($id);
					if ( ! $id ) continue;
					
					$photo = $model->get($id);
					$photo->{'make' . ucfirst($action)}();
					Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED, array('id' => $id, 'action' => $action));
				}
				$status = new Cubix_EscortStatus($escort_id);
				$status->setStatusBit(32);
				$status->save();
				$model->reset($escort_id);
			break;
			case 'soft':
			case 'hard':
				foreach ( $ids as $id ) {
					$id = intval($id);
					if ( ! $id ) continue;
					
					$photo = $model->get($id);
					$photo->{'make' . ucfirst($action)}();
				}
				Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED, array('id' => $id, 'action' => $action));
			break;
			case 'verified':
				$escort_id = $model->get(end($ids))->escort_id;
				foreach ( $ids as $id ) {
					$id = intval($id);
					if ( ! $id ) continue;
					
					$photo = $model->get($id);
					$photo->toggleVerified();
					Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED, array('id' => $id, 'action' => 'verified toggled'));
				}
				$model->reset($escort_id);
			break;
            case 'valentines':
                $escort_id = $model->get(end($ids))->escort_id;
                foreach ( $ids as $id ) {
                    $id = intval($id);
                    if ( ! $id ) continue;

                    $photo = $model->get($id);
                    $photo->toggleValentines();
                    Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED, array('id' => $id, 'action' => 'verified toggled'));
                }
                $model->reset($escort_id);
                break;
			case 'retouch':
				foreach ( $ids as $id ) {
					$id = intval($id);
					if ( ! $id ) continue;
					
					$photo = $model->get($id);
					$photo->toggleRetouch();
					Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED, array('id' => $id, 'action' => 'retouch toggled'));
				}
				$status = new Cubix_EscortStatus($escort_id);
				$status->setStatusBit(32);
				$status->save();
			break;
			case 'edited':
				foreach ( $ids as $id ) {
					$id = intval($id);
					if ( ! $id ) continue;
					
					$photo = $model->get($id);
					$photo->toggleEdited();
					Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED, array('id' => $id, 'action' => 'edited toggled'));
				}
				break;
			case 'remove':
				
				$bu_user = Zend_Auth::getInstance()->getIdentity();
				if( Model_EscortsV2::isSuspicious($escort_id) && !in_array($bu_user->type, array('superadmin', 'admin'))){
						BREAK;
				}
				foreach ( $ids as $i => $id ) {
					$ids[$i] = intval($id);
					if ( ! $ids[$i] ) continue;
				}
				
				$images = array();
				
				foreach ( $ids as $id ) {
					$photo = $model->get($id);
					$images[] = new Cubix_Images_Entry(array(
						'application_id' => $photo->application_id,
						'catalog_id' => $photo->escort_id,
						'hash' => $photo->hash,
						'ext' => $photo->ext
					));
					Cubix_SyncNotifier::notify($photo->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_DELETED, array('id' => $id));
				}
				
				//$images_model = new Cubix_Images();
				//$images_model->remove($images);

				$escort_id = $model->get(end($ids))->escort_id;
				$escort = $this->model->get($escort_id);
				
				$model->remove($ids);

				$photos_count = $escort->getPhotosCount();

				$status = new Cubix_EscortStatus($escort_id);
				$status->setStatusBit(32);
				$status->save();
				
				$m_snapshot = new Model_Escort_SnapshotV2(array('id' => $escort_id));
				$m_snapshot->snapshotProfileV2();
				/*if ( $photos_count < $photos_min_count )
				{
					//if ( ! $this->model->hasStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_NO_ENOUGH_PHOTOS) ) {
						$this->model->setStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_NO_ENOUGH_PHOTOS);
					if ( $this->model->hasStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_ACTIVE) ) {
						$this->model->removeStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_ACTIVE);
					}
				}*/
			break;
			case 'sort':
				foreach ( $ids as $i => $id ) {
					$model->get($id)->reorder($i + 1);
				}
				Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED, array('action' => 'photos reordered'));
			break;
			case 'rotatable':
				$model->clearRotatable($escort_id);
				foreach ( $ids as $key => $id ) {
					$id = intval($id);
					if ( ! $id ) continue;
					if($key == 0){
						$model->get($id)->setMain();
					}
					$photo = $model->get($id);
					$id = $photo->setRotatable();
				}
				$this->model->updateRotateType($escort_id, Model_Escort_Photos::PHOTO_ROTATE_SELECTED);
				$m_snapshot = new Model_Escort_SnapshotV2(array('id' => $escort_id));
				$m_snapshot->snapshotProfileV2();
				Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED, array('id' => $id, 'action' => $action));
			break;
			case 'set-main':
				$id = end($ids);
				$model->get($id)->setMain();
				$model->clearRotatable($escort_id);
				$this->model->updateRotateType($escort_id, 0);
				
				$escort = $this->model->get($escort_id);
				$m_snapshot = new Model_Escort_SnapshotV2(array('id' => $escort_id));
				$m_snapshot->snapshotProfileV2();
				Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED, array('id' => $id, 'action' => 'made main'));
				//$escort->snapshotProfile($escort_id);
			break;
			case 'set-gotd':
				$id = end($ids);
				$model->get($id)->setGOTD();
				$escort_id = $model->get(end($ids))->escort_id;
				//$escort = $this->model->get($escort_id);
				//$m_snapshot = new Model_Escort_SnapshotV2(array('id' => $escort_id));
				//$m_snapshot->snapshotProfileV2();
				Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED, array('id' => $id, 'action' => 'made GOTD'));
				//$escort->snapshotProfile($escort_id);
			break;
		
			case 'set-profile-boost':
				$id = end($ids);
				$model->get($id)->setProfileBoost();
				$escort_id = $model->get(end($ids))->escort_id;
				Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED, array('id' => $id, 'action' => 'made Profile Boost'));
			break;
		
			case 'unset-large-popunder':
				
				foreach ( $ids as $id ) {
					$id = intval($id);
					if ( ! $id ) continue;
					var_dump($escort_id,$id);
					$model->unsetPopunder($escort_id,$id);
					Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED, array('id' => $id, 'action' => 'Unset popunder'));
				}
				break;
			break;
		
			case 'approve':
				
				$not_approved_ids = $model->getNotApprovedPhotos($escort_id);
						
				foreach ( $ids as $i => $id ) {
					$model->get($id)->approve();
					Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED, array('id' => $id, 'action' => 'approvement toggled'));
					
					if(Cubix_Application::getId() == APP_ED && in_array( $id, $not_approved_ids)){
						Cubix_FollowEvents::notify($escort_id, 'escort', FOLLOW_ESCORT_NEW_PICTURE , array('rel_id' => $id), true);
					}
					
					Cubix_LatestActions::addDirectActionDetail($escort_id, Cubix_LatestActions::ACTION_NEW_PHOTO, $id);
				}
				
				Cubix_LatestActions::addAction($escort_id, Cubix_LatestActions::ACTION_NEW_PHOTO);
				
				$escort_id = $model->get(end($ids))->escort_id;
				$escort = $this->model->get($escort_id);
				$photos_count = $escort->getPhotosCount();

				/*if ( $photos_count < $photos_min_count )
				{
					if ( ! $this->model->hasStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_NO_ENOUGH_PHOTOS) ) {
						$this->model->setStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_NO_ENOUGH_PHOTOS);
					}
					$this->model->removeStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_ACTIVE);
				}
				else {
					if ( $this->model->hasStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_NO_ENOUGH_PHOTOS) ) {
						$this->model->removeStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_NO_ENOUGH_PHOTOS);
					}

					if ( ! $this->model->hasStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_ADMIN_DISABLED) &&
						! $this->model->hasStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_OWNER_DISABLED) )
					{
						if (
							(! $this->model->hasStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_IS_NEW) &&
							! $this->model->hasStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_NO_PROFILE) &&
							! $this->model->hasStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_PROFILE_CHANGED))
						)
						{
							$this->model->setStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_ACTIVE);

							
						}
					}
				}*/
				$status = new Cubix_EscortStatus($escort_id);
				$status->setStatusBit(32);
				$status->save();

				if ( in_array(Cubix_Application::getId(), array(APP_EF))  ) {
					ignore_user_abort(true);
					ob_start();
					echo (json_encode(array('status' => 'success')));
					  
					ob_end_flush();
					ob_flush();
					flush();

					session_write_close();
					fastcgi_finish_request();
				}

				$m_snapshot = new Model_Escort_SnapshotV2(array('id' => $escort_id));
				$m_snapshot->snapshotProfileV2();
			break;
			case 'double_approve':
					$model->doubleApprove($ids);
					echo json_encode(array('success' => true));
										
			break;
			case 'adjust':
				$photo_id = intval($this->_getParam('photo_id'));

				$photo = $model->get($photo_id);

				$hash = $photo->getHash();
				$result = array(
					'x' => intval($this->_getParam('x')),
					'y' => intval($this->_getParam('y')),
					'px' => round(floatval($this->_getParam('px')), 4),
					'py' => round(floatval($this->_getParam('py')), 4),
					'rwidth'=>intval($this->_getParam('rwidth')),
					'rheight'=>intval($this->_getParam('rheight'))
				);
				$photo->setCropArgs($result);
                $ext = $photo->ext;

                // Crop all images
				$size_map = array(
					'backend_thumb' => array('width' => 150, 'height' => 205, 'use_crop_args' => true),
					'backend_agency_thumb' => array('width' => 100, 'height' => 78),
					'backend_smallest' => array('width' => 64, 'height' => 64, 'use_crop_args' => true),
					'medium' => array('width' => 225, 'height' => 267, 'use_crop_args' => true),
					'thumb' => array('width' => 150, 'height' => 200, 'use_crop_args' => true),
					'thumb_small' => array('width' => 105, 'height' => 140, 'use_crop_args' => true),
					'sthumb' => array('width' => 60, 'height' => 81, 'use_crop_args' => true),
					'lvthumb' => array('width' => 75, 'height' => 100, 'use_crop_args' => true),
					'wthumb' => array('width' => 141, 'height' => 88, 'use_crop_args' => true),
					'laction_thumb' => array('width' => 167, 'height' => 167, 'use_crop_args' => true),
					'xl_thumb' => array('width' => 302, 'height' => 455, 'use_crop_args' => true),
                    'xl_thumb_v2' => array('width' => 302, 'height' => 455, 'use_crop_args' => true),
					'w68' => array('width' => 68, 'height' => 91, 'use_crop_args' => true),
					'agency_p100' => array('width' => 90, 'height' => 120, 'use_crop_args' => true),
					'w514' => array(),
					'gotm_xl' => array('width' => 297, 'height' => 338, 'use_crop_args' => true),
					'list_v3' => array('width' => 265, 'height' => 354, 'use_crop_args' => true),
				);
				$conf = Zend_Registry::get('images_config');


                get_headers($conf['remote']['url'] . '/get_image.php?a=clear_cache&app=' . Cubix_Application::getById($this->_getParam('application_id'))->host . '&eid=' . $photo->escort_id . '&hash=' . $photo->hash);
				//echo $conf['remote']['url'] . '/get_image.php?a=clear_cache&app=' . Cubix_Application::getById($this->_getParam('application_id'))->host . '&eid=' . $photo->escort_id . '&hash=' . $photo->hash;

				$catalog = $photo->escort_id;
				
				$a = array();
				if ( is_numeric($catalog) ) {
					$parts = array();

					if ( strlen($catalog) > 2 ) {
						$parts[] = substr($catalog, 0, 2);
						$parts[] = substr($catalog, 2);
					}
					else {
						$parts[] = '_';
						$parts[] = $catalog;
					}
				}
				elseif ( preg_match('#^([0-9]+)/(.+)$#', $catalog, $a) ) {
					array_shift($a);
					$catalog = $a[0];

					$parts = array();

					if ( strlen($catalog) > 2 ) {
						$parts[] = substr($catalog, 0, 2);
						$parts[] = substr($catalog, 2);
					}
					else {
						$parts[] = '_';
						$parts[] = $catalog;
					}

					$parts[] = $a[1];
				}

                $catalog = implode('/', $parts);
                $resize = (int)$this->_getParam('resize');
                if ($photo->is_portrait == '0') {
                    $kwidth = floatval($photo->width / $resize);
                    $height = $photo->height;
                    $x = $result['x'] * $kwidth + 2;
                    $y = 0;
                    $width = $kwidth * $result['rwidth'];
                }
                else {
                    $kheight = floatval($photo->height / $resize);
                    $width = $photo->width;
                    $x = 0;
                    $y = $result['y'] * $kheight + 2;
                    $height = $kheight * $result['rheight'];
                }

				foreach($size_map as $size => $sm) {

					// echo $conf['remote']['url'] . '/' . Cubix_Application::getById($this->_getParam('application_id'))->host . '/' . $catalog . '/' . $hash . '_' . $size . '.jpg?args=' . $result['x'] . ':' . $result['y'] . '<br />';
                    get_headers($conf['remote']['url'] . '/' . Cubix_Application::getById($this->_getParam('application_id'))->host . '/' . $catalog . '/' . $hash . '_' . $size . '.'. $ext. '?args=' . $x . ':' . $y.'&cwidth='.$width.'&cheight='.$height);
				}
					
				Cubix_SyncNotifier::notify($photo->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED, array('id' => $photo->id, 'action' => 'adjustment changed'));

				echo json_encode(array('success' => true));
			break;
			/*case 'getid':
				$image_hash = $this->_getParam('image_hash');
				$db = Zend_Registry::get('db');
				$photo_id_from_hash = $db->fetchOne('SELECT id FROM escort_photos WHERE hash = ?', $image_hash);
				if (strlen($photo_id_from_hash) > 0)
					echo $photo_id_from_hash;
				else
					echo 'null';
			break;*/
		}
		
		if (!$model->hasMain($escort_id))
		{
			$model->setRandomMain($escort_id);
		}
		
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	}
	
	public function uploadHtml5Action()
	{
		$tmp_dir = sys_get_temp_dir();
		
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$escort_id = intval($this->_getParam('escort_id'));
		$type = intval($this->_getParam('type'));
		$type = $type == 0 ? 1 : $type;
		
		try {
			if ( ! $this->user->hasAccessToEscort($escort_id) ) {
				throw new Exception('Permission denied');
			}

			$escort = $this->model->get($escort_id);

			if ( ! $escort ) {
				throw new Exception('Wrong id supplied');
			}

			//$new_profile_apps = array(APP_A6, APP_EF, APP_BL, APP_6A, APP_6B);

			if ( $this->_getParam('application_id') == APP_A6 ) {
				$model_photos = new Model_Escort_Photos();
				$photo_count = $model_photos->getEscortPhotoCount($escort_id);
				if ( $photo_count >= Model_Escort_Photos::MAX_PHOTOS_COUNT ){
						throw new Exception("You've reached maximum amount of pictures ( 30 )", Cubix_Images::ERROR_IMAGE_COUNT_LIMIT_OVER);
				}
			}
			
			$response = array(
				'id'	=> $_SERVER['HTTP_X_FILE_ID'],
				'name'	=> $_SERVER['HTTP_X_FILE_NAME'],
				'size'	=> $_SERVER['HTTP_X_FILE_SIZE'],
				'error'	=> 0,
				'msg' => '',
				'finish'	=> FALSE
			);
			
			$file = $tmp_dir . DIRECTORY_SEPARATOR . $response['name'];
			file_put_contents($file, file_get_contents('php://input'));
			
			
			
			// Save on remote storage
			$images = new Cubix_Images();
			$image = $images->save($file, $escort->id, $escort->application_id, strtolower(@end(explode('.', $response['name']))));

			if ( ! isset($image['hash']) ) {
				throw new Exception("Photo upload failed. Please try again!");
			}
			
			$image = new Cubix_Images_Entry($image);
			$image->setSize('backend_thumb');
			$image->setCatalogId($escort_id);
			$image_url = $images->getUrl($image);

			
			
			$image_size = getimagesize($file);
			
			if ( ! filesize($file) || filesize($file) != $_SERVER['HTTP_X_FILE_SIZE'] ) {
				throw new Exception("Photo upload failed. Please try again!");
			}
			
			$is_portrait = 0;
			if ( $image_size ) {
				if ( $image_size[0] < $image_size[1] ) {
					$is_portrait = 1;
				}
			}
			
			$photo = new Model_Escort_PhotoItem(array(
				'escort_id' => $escort_id,
				'hash' => $image->getHash(),
				'ext' => $image->getExt(),
				'type' => $type,
				'is_approved' => 1,
				'is_portrait' => $is_portrait,
				'width' => $image_size[0],
				'height' => $image_size[1]
			));
			
			$model = new Model_Escort_Photos();
			$photo = $model->save($photo);
			
			$m_escorts = new Model_EscortsV2();

			$m_escorts->setEscortVerifiedStatus($escort_id);
			
			if ( Cubix_Application::getId() == APP_EF ) {
				$m_escorts->checkAgeCertification($escort_id);
			}
			$status = new Cubix_EscortStatus($escort_id);
			$status->setStatusBit(Cubix_EscortStatus::ESCORT_STATUS_ACTIVE);
			$status->save();

			Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_ADDED, array('id' => $photo->id, 'action' => 'photos uploaded'));
			Cubix_AlertEvents::notify($escort_id, ALERT_ME_NEW_PICTURES, array('id' => $photo->id));
			
			Cubix_LatestActions::addAction($escort_id, Cubix_LatestActions::ACTION_NEW_PHOTO);
			
			Cubix_LatestActions::addDirectActionDetail($escort_id, Cubix_LatestActions::ACTION_NEW_PHOTO, $photo->id);
			
			$m_snapshot = new Model_Escort_SnapshotV2(array('id' => $escort_id));
			$m_snapshot->snapshotProfileV2();

			$response['photo'] = $photo->toJSON('backend_thumb_cropper');
			$response['finish'] = TRUE;
			
		} catch (Exception $e) {
			$response['error']	= 1;
			$response['msg']	= $e->getMessage();
		}
		
		
		
		echo json_encode($response);
	}

    public function uploadHtml5FromDataAction($escort_id, $file, $type = 1)
    {


        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $model_escorts = new Model_EscortsV2();

        $type = intval($this->_getParam('type'));
        $type = $type == 0 ? 1 : $type;

        try {
            if ( ! $this->user->hasAccessToEscort($escort_id) ) {
                throw new Exception('Permission denied');
            }

            $escort = $model_escorts->get($escort_id);

            if ( ! $escort ) {
                throw new Exception('Wrong id supplied');
            }

            $model_photos = new Model_Escort_Photos();
            $photo_count = $model_photos->getEscortPhotoCount($escort_id);
            if ( $photo_count >= Model_Escort_Photos::MAX_PHOTOS_COUNT ){
                throw new Exception("You've reached maximum amount of pictures ( 30 )", Cubix_Images::ERROR_IMAGE_COUNT_LIMIT_OVER);
            }


            // Save on remote storage
            $images = new Cubix_Images();
            $image = $images->save($file, $escort->id, $escort->application_id);

            if ( ! isset($image['hash']) ) {
                throw new Exception("Photo upload failed. Please try again!");
            }

            $image = new Cubix_Images_Entry($image);
            $image->setSize('backend_thumb');
            $image->setCatalogId($escort_id);
            $image_url = $images->getUrl($image);



            $image_size = getimagesize($file);

            if ( ! filesize($file) ) {
                throw new Exception("Photo upload failed. Please try again!");
            }

            $is_portrait = 0;
            if ( $image_size ) {
                if ( $image_size[0] < $image_size[1] ) {
                    $is_portrait = 1;
                }
            }

            $photo = new Model_Escort_PhotoItem(array(
                'escort_id' => $escort_id,
                'hash' => $image->getHash(),
                'ext' => $image->getExt(),
                'type' => $type,
                'is_approved' => 1,
                'is_portrait' => $is_portrait,
                'width' => $image_size[0],
                'height' => $image_size[1]
            ));

            $model = new Model_Escort_Photos();
            $photo = $model->save($photo);

            $m_escorts = new Model_EscortsV2();

            $m_escorts->setEscortVerifiedStatus($escort_id);

            $status = new Cubix_EscortStatus($escort_id);
            $status->setStatusBit(Cubix_EscortStatus::ESCORT_STATUS_ACTIVE);
            $status->save();

            Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_ADDED, array('id' => $photo->id, 'action' => 'photos uploaded'));
            Cubix_AlertEvents::notify($escort_id, ALERT_ME_NEW_PICTURES, array('id' => $photo->id));

            Cubix_LatestActions::addAction($escort_id, Cubix_LatestActions::ACTION_NEW_PHOTO);

            Cubix_LatestActions::addDirectActionDetail($escort_id, Cubix_LatestActions::ACTION_NEW_PHOTO, $photo->id);

            $m_snapshot = new Model_Escort_SnapshotV2(array('id' => $escort_id));
            $m_snapshot->snapshotProfileV2();

            $response['photo'] = $photo->toJSON('backend_thumb_cropper');
            $response['finish'] = TRUE;

        } catch (Exception $e) {
            $response['error']	= 1;
            $response['msg']	= $e->getMessage();
        }




        return $response;
    }

	public function uploadPhotoAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$escort_id = intval($this->_getParam('escort_id'));
		$is_private = intval($this->_getParam('is_private'));
		
		$no_validation = $this->_getParam('no_validation', 1);
	
		$type = intval($this->_getParam('type'));
		
		$photos_min_count = Cubix_Application::getById($this->_getParam('application_id'))->min_photos;

		try {
			if ( ! $this->user->hasAccessToEscort($escort_id) ) {
				throw new Exception('Permission denied');
			}

			$escort = $this->model->get($escort_id);
			
			if ( ! $escort ) {
				throw new Exception('Wrong id supplied');
			}
			
			//$new_profile_apps = array(APP_A6, APP_EF, APP_BL, APP_6A, APP_6B);
		
			if ( $this->_getParam('application_id') == APP_A6 || $this->_getParam('application_id') == APP_A6_AT ) {
				$model_photos = new Model_Escort_Photos();
				$photo_count = $model_photos->getEscortPhotoCount($escort_id);
				if ( $photo_count >= Model_Escort_Photos::MAX_PHOTOS_COUNT ){
						throw new Exception("You've reached maximum amount of pictures ( 30 )", Cubix_Images::ERROR_IMAGE_COUNT_LIMIT_OVER);
				}
			}
			// Save on remote storage
			$images = new Cubix_Images();
			$image = $images->save($_FILES['Filedata']['tmp_name'], $escort->id, $escort->application_id, strtolower(@end(explode('.', $_FILES['Filedata']['name']))), null, null, $no_validation);

			$image = new Cubix_Images_Entry($image);
			$image->setSize('backend_thumb');
			$image->setCatalogId($escort_id);
			$image_url = $images->getUrl($image);

			$image_size = getimagesize($_FILES['Filedata']['tmp_name']);
			$is_portrait = 0;
			if ( $image_size ) {
				if ( $image_size[0] < $image_size[1] ) {
					$is_portrait = 1;
				}
			}
			
			$photo = new Model_Escort_PhotoItem(array(
				'escort_id' => $escort_id,
				'hash' => $image->getHash(),
				'ext' => $image->getExt(),
				'type' => $type,
				'is_approved' => 1,
				'is_portrait' => $is_portrait,
				'width' => $image_size[0],
				'height' => $image_size[1]
			));
			
			$model = new Model_Escort_Photos();
			$photo = $model->save($photo);

			$photos_count = $escort->getPhotosCount();

			$m_escorts = new Model_EscortsV2();

			$status = new Cubix_EscortStatus($escort_id);
			$status->setStatusBit(Cubix_EscortStatus::ESCORT_STATUS_ACTIVE);
			$status->save();

			Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_ADDED, array('id' => $photo->id, 'action' => 'photos uploaded'));
			Cubix_AlertEvents::notify($escort_id, ALERT_ME_NEW_PICTURES, array('id' => $photo->id));
			
			Cubix_LatestActions::addAction($escort_id, Cubix_LatestActions::ACTION_NEW_PHOTO);
			Cubix_LatestActions::addDirectActionDetail($escort_id, Cubix_LatestActions::ACTION_NEW_PHOTO, $photo->id);
			
			$m_snapshot = new Model_Escort_SnapshotV2(array('id' => $escort_id));
			$m_snapshot->snapshotProfileV2();

			$result = array(
				'status' => 'success',
				'photo' => $photo->toJSON('backend_thumb_cropper'),
				'id' => $photo->id
			);
		} catch (Exception $e) {
			$result = array('status' => 'error', 'msg' => 'asdasdasd');
			
			$result['msg'] = $e->getMessage();
		}
		@unlink($_FILES['Filedata']['tmp_name']);
		echo json_encode($result);
	}

	public function revisionsAction()
	{

		$this->view->layout()->disableLayout();
		$this->view->url = Cubix_Application::getById(Cubix_Application::getId())->url;
		
	
		$escort_id = intval($this->_getParam('escort_id'));
		if ( ! $this->user->hasAccessToEscort($escort_id) ) die('Permission denied');
		if ( ! $escort_id ) die;	
		$this->view->escort_id = $escort_id;

		// <editor-fold defaultstate="collapsed" desc="Fraud Warning Check">
		$warning = Model_SuspiciousWarnings::getWarningAboutEscortFormatted($escort_id);
		if ( $warning ) {
			$this->view->warning = $warning;
		}
		// </editor-fold>

		$m_escort = new Model_EscortsV2();
		$this->view->escort = $escort = $m_escort->get($escort_id);




		$this->view->active_package = $m_escort->getActivePackage($escort_id);
        $this->view->last_ip = $ip = !(empty($this->view->escort->agency_id)) ? (new Model_Agencies())->getLastIP($this->view->escort->agency_id) : (new Model_Escorts())->getLastIP($escort_id);
		
        // When user is still in profile steps but didnt finish all of them
        // -------------------------------
		if(Cubix_Application::getId() == APP_ED && $escort->status & Model_EscortsV2::ESCORT_STATUS_PARTIALLY_COMPLETE  ) {
            $this->view->is_profile_partialy = true;
        }else{
            $this->view->is_profile_partialy = false;
        }
		
		if(Cubix_Application::getId() == APP_ED && $escort->user_status == USER_STATUS_NOT_VERIFIED) {
            $this->view->not_ver_user = true;
        }else{
            $this->view->not_ver_user = false;
        }
		
		if($escort->status & Model_EscortsV2::ESCORT_STATUS_OFFLINE){
			$this->view->is_offline = $is_offline = true;
		}
		else{
			$this->view->is_offline = $is_offline = false;
		}
		
        // -------------------------------

        $m_profile = new Model_Escort_ProfileV2(array('id' => $escort_id));

		/* --> Process Actions */
		$a = $this->_getParam('a');
		$mode = $this->_getParam('m');
		if ( ! is_null($a) ) {
			
			$latest_revision = $m_profile->getLatestRevision(Model_Escort_ProfileV2::REVISION_STATUS_NOT_APPROVED);
			if(is_null($latest_revision)) die;

			if ( 'decline' == $a ) {
				
				$m_profile->updateRevision($latest_revision, Model_Escort_ProfileV2::REVISION_STATUS_DECLINED);

				/*$this->model->removeStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_PROFILE_CHANGED);
				$this->model->removeStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_IS_NEW);*/
				$status = new Cubix_EscortStatus($escort_id);
				$status->setStatusBit(Cubix_EscortStatus::ESCORT_STATUS_ACTIVE);
				$status->save();
			}
			elseif ( 'apply' == $a ) {
				
				$revision = $m_profile->getRevision($latest_revision);


                //IF Country is USA, sales person to USA_sales1
                if(Cubix_Application::getId() == APP_ED && !$escort->agency_id && $revision->data['country_id'] == 68){
                    $sales_person = 205; // USA_sales1
                    $m_escort->updateSalesPerson($sales_person, $escort->user_id);
                }elseif(Cubix_Application::getId() == APP_ED && $escort->country_id == 68 && !$escort->agency_id && $revision->data['country_id'] != 68){
                    $sales_person = 1; // admin
                    $m_escort->updateSalesPerson($sales_person, $escort->user_id);
                }

				if(Cubix_Application::getId() == APP_ED ){
                    unset($revision->data['last_step']);
                    unset($revision->data['apps_availble']);

					if(!isset($revision->data['type'])){
						$revision->data['type'] = $this->view->escort->type;
					}
				}


				$m_profile->applyRevision($revision->data,$latest_revision);
				$m_profile->updateRevision($latest_revision, Model_Escort_ProfileV2::REVISION_STATUS_APPROVED, 'dashboard');

				$status = new Cubix_EscortStatus($escort_id);
//				$status->removeStatusBit(Cubix_EscortStatus::ESCORT_STATUS_OWNER_DISABLED);
//				$status->setStatusBit(Cubix_EscortStatus::ESCORT_STATUS_ACTIVE);
				if ($mode == 'offline'){
                    $bu_user = Zend_Auth::getInstance()->getIdentity();
                    if(in_array(Cubix_Application::getId(), array(APP_EG_CO_UK, APP_ED, /*APP_EF, APP_AE, APP_BL, APP_6B*/))){
                        $status->setStatusBit(Cubix_EscortStatus::ESCORT_STATUS_OFFLINE);
                    }

                    if(in_array(Cubix_Application::getId(), [APP_EG_CO_UK, APP_ED])){

                        $text = 'Hello, it seems that something is wrong with your profile ' . $this->view->escort->showname .
                            ' This usually means that the text or other information in your profile are breaking our guidelines.' .
                            ' In order to see exactly what is wrong and fix it, please contact us using the support' .
                            ' section or write us at info@escortguide.co.uk';

                        $agencyText = 'Hello, it seems that something is wrong with your profile ' . $this->view->escort->showname .
                            ' This usually means that the text or other information in your profile'.
                            ' are breaking our guidelines. In order to see exactly what is wrong and' .
                            ' fix it, please contact us using the support section or write us at info@escortguide.co.uk';

                        $subject = 'Your profile was not approved!';

                        if (Cubix_Application::getId() == APP_ED) {
                            $avalableLangs = Cubix_I18n::getLangs();
                            $lng_ = 'en';
                            $escortLangs_ = $this->view->escort->getLangs();
                            if(!empty($escortLangs_)) {
                                foreach($escortLangs_ as $escortLang) {
                                    if(in_array($escortLang->id, $avalableLangs)) {
                                        $lng_ = $escortLang->id;
                                        break;
                                    }
                                }
                            }

                            $text = $agencyText = Cubix_I18n::translateByLng($lng_, 'profile_not_approved_email_text', array('showname' => $this->view->escort->showname));
                            $subject = Cubix_I18n::translateByLng($lng_, 'your_profile_was_not_approved');
                        }

                        $escort_email = $this->view->escort->email ? $this->view->escort->email : $this->view->escort->contact_email;

                        if ($this->view->escort->agency_id) {
                            $agency_model = new Model_Agencies();
                            $agency = $agency_model->getById($this->view->escort->agency_id);
                            $agency_email = $agency['agency_data']->email ? $agency['agency_data']->email : $agency['agency_data']->u_email;

                            Cubix_Email::send($agency_email, $subject, $agencyText);
                        } elseif ($escort_email) {
                            Cubix_Email::send($escort_email, $subject, $text);
                        }
                    }

//                    elseif (Cubix_Application::getId()== APP_EF && in_array($bu_user->type,array("superadmin", "admin", "sales manager"))){
//                        $status->setStatusBit(Cubix_EscortStatus::ESCORT_STATUS_OFFLINE);
//                    }
                }
				elseif($is_offline){
					$status->removeStatusBit(Cubix_EscortStatus::ESCORT_STATUS_OFFLINE);
					if(!$status->hasStatusBit(Cubix_EscortStatus::ESCORT_STATUS_OWNER_DISABLED )){
						$status->setStatusBit(Cubix_EscortStatus::ESCORT_STATUS_ACTIVE);
					}
				}
				$status->save();


				// PROFILE CHANGED
				if(in_array(Cubix_Application::getId(), array(APP_EG_CO_UK, APP_6B)) && $this->view->escort->status & 32){
					$escort_public_url = $this->view->url . '/escort/'.$this->view->escort->showname.'-'.$this->view->escort->id;
					$escort_email = $this->view->escort->email ? $this->view->escort->email : $this->view->escort->contact_email;

					if($this->view->escort->agency_id){
                        $agency_model = new Model_Agencies();
                        $agency = $agency_model->getById($this->view->escort->agency_id);
                        if($agency){
                        	$agency_email = $agency['agency_data']->email ? $agency['agency_data']->email : $agency['agency_data']->u_email;
                            Cubix_Email::sendTemplate('agency_escort_profile_changes_approved', $agency_email,
                                array(
                                    'showname' => $this->view->escort->showname,
                                    'agency' => $agency['agency_data']->name,
                                    'application_title' => 'Escortguide.co.uk',
                                    'url' => $escort_public_url
                                ));
                        }
                    }else{
                        Cubix_Email::sendTemplate('escort_profile_changes_approved', $escort_email,
                            array('showname' => $this->view->escort->showname,
                                'application_title' => 'Escortguide.co.uk',
                                'url' => $escort_public_url
                            ));
                    }
				}

				// PROFILE IS NEW
                if(in_array(Cubix_Application::getId(), array(APP_EG_CO_UK, APP_6B)) && $this->view->escort->status & 64){
                	$escort_public_url = $this->view->host.'/escort/'.$this->view->escort->showname.'-'.$this->view->escort->id;
                    $escort_email = $this->view->escort->email ? $this->view->escort->email : $this->view->escort->contact_email;

                    if($this->view->escort->agency_id){
                        $agency_model = new Model_Agencies();
                        $agency = $agency_model->getById($this->view->escort->agency_id);
                        if($agency){
                        	$agency_email = $agency['agency_data']->email ? $agency['agency_data']->email : $agency['agency_data']->u_email;
                            Cubix_Email::sendTemplate('admin_approved_account_agency', $agency_email,
                                array(
                                    'showname' => $this->view->escort->showname,
                                    'agency' => $agency['agency_data']->name,
                                    'application_title' => 'Escortguide.co.uk',
                                     'url' => $escort_public_url
                                ));
                            Cubix_Email::sendTemplate('agency_profile_published', $agency_email,
                                array(
                                    'showname' => $this->view->escort->showname,
                                    'agency' => $agency['agency_data']->name,
                                    'application_title' => 'Escortguide.co.uk',
                                    'url' => $escort_public_url
                                ));
                        }
                    }else{
                    	

                        Cubix_Email::sendTemplate('admin_approved_account', $escort_email,
                            array('showname' => $this->view->escort->showname,
                                'application_title' => 'Escortguide.co.uk'
                            ));
                       
                        Cubix_Email::sendTemplate('escort_approved ', $escort_email,
                            array('showname' => $this->view->escort->showname,
                                'application_title' => 'Escortguide.co.uk',
                                'url' => $escort_public_url
                            ));
                    }
                }

                if(Cubix_Application::getId() == APP_ED && $this->view->escort->status & 64){

                    if($this->view->escort->agency_id){
                        $agency_model = new Model_Agencies();
                        $agency = $agency_model->getById($this->view->escort->agency_id);

	                    $price_1 = $this->get_montly_price( 'agency' ,'vip', $agency['agency_data']->country_id, 'escorts', 30, 3 );
	                    $price_3 = $this->get_montly_price( 'agency' ,'vip', $agency['agency_data']->country_id, 'escorts', 30, 6 );

                        if($agency){
                            $template = 'admin_approved_account_agency_v1';
                            if ($revision->data['country_id'] == 68){
                                $template = 'admin_approved_account_agency_us';
                            }
                        	$agency_email = $agency['agency_data']->email ? $agency['agency_data']->email : $agency['agency_data']->u_email;
                            Cubix_Email::sendTemplate($template, $agency_email,
                                array(
                                    'showname' => $this->view->escort->showname,
                                    'agency' => $agency['agency_data']->name,
                                    'price_1' => $price_1,
                                    'price_3' => $price_3,
                                    'application_title' => 'Escortdirectory.com'
                                ));
                        }
                    }else{
                    	
                    	$vip_price = $this->get_montly_price( 'escort' ,'vip', $this->view->escort->country_id, 'escorts', 30  );
	                    $premium_price = $this->get_montly_price( 'escort' ,'premium', $this->view->escort->country_id, 'escorts',30);
	                    $escort_email = $this->view->escort->email ? $this->view->escort->email : $this->view->escort->contact_email;

                        $template = 'admin_approved_account_v1';

                        $config = Zend_Registry::get('system_config');

                        $payemnt_disabled_countries = explode(',',$config['paymentDisabledCountries']);
                        
	                    if(in_array($this->view->escort->country_id, $payemnt_disabled_countries) || in_array($revision->data['country_id'], $payemnt_disabled_countries)) {
	                        $template = 'admin_approved_account_v1_us';
                        }

                        Cubix_Email::sendTemplate($template, $escort_email,
                            array(
                            	'showname' => $this->view->escort->showname,
                            	'vip_price' => $vip_price,
                            	'premium_price' => $premium_price,
                                'application_title' => 'Escortdirectory.com'
                            ));

                    }
                }

                //WIZARD add notification about AC
                if(Cubix_Application::getId() == APP_EF && $this->view->escort->status & 64){

                	$db = Zend_Registry::get('db');

                	$needSend = $db->fetchRow('
						SELECT
							true
						FROM escorts
						WHERE id = ? AND wizard_notification_status_profile = 0
					', array($this->view->escort->id));

                	if($needSend){
	                 	$sql = '
							SELECT
								ac.status
							FROM age_verifications ac
							WHERE ac.status <> -1
							AND escort_id = ?
							ORDER BY id DESC';

						$status = $db->fetchRow($sql, array($this->view->escort->id));

						$triggerType = 3; //TRIGGER_APPROVED_PROFILE_MISSING_AC

						if($status){
							switch ($status->status) {
								case '2': // PENDING
									$triggerType = 2; //TRIGGER_APPROVED_PROFILE_PENNDING_AC
									break;
								case '3': // VERIFY
									$triggerType = 4; //TRIGGER_APPROVED_PROFILE_APPROVED_AC
									break;
								case '4': // REJECTED
									$triggerType = 5; //TRIGGER_APPROVED_PROFILE_REJECTED_AC
									break;		
							}	
						}else{
							//TRIGGER_COMPLETED_PROFILE_MISSING_AC
							$recurringTrigger = new Cubix_EmailTrigger(null, 10, $escort_id); //recurring
							$recurringTrigger->addToDeferred();
						}

						$et = new Cubix_EmailTrigger(null, $triggerType, $escort_id);
						$et->addToDeferred();

						$db->update('escorts', array('wizard_notification_status_profile' => 1), $db->quoteInto('id = ?', $this->view->escort->id));
					}
                }

				/*$this->model->removeStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_IS_NEW);
				$this->model->removeStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_PROFILE_CHANGED);
				
				$this->model->removeStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_NO_PROFILE);

				
				if ( ! $this->model->hasStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_ADMIN_DISABLED) &&
					! $this->model->hasStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_OWNER_DISABLED) )
				{
					if (
						(! $this->model->hasStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_IS_NEW) &&
						! $this->model->hasStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_NO_PROFILE) &&
						! $this->model->hasStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_PROFILE_CHANGED))
					)
					{
						$this->model->setStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_ACTIVE);
					}
				}

				$this->model->removeStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_IS_NEW);*/

				Zend_Registry::get('BillingHooker')->notify('escort_revision_applied', array($escort_id));

				/*if ( ! $this->model->hasStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_ACTIVE) ) {
					$this->model->setStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_ACTIVE);
				}*/
			}

			die;
		}
		/* <-- */
		
		/*
		if ( ! is_null($only_changes = $this->_getParam('only_changes')) ) {
			setcookie('escorts_revisions_only_changes', $only_changes, strtotime('+1 year'), '/');
		}
		else {
			$only_changes = intval($_COOKIE['escorts_revisions_only_changes']);
		}
		
		$this->view->only_changes = $only_changes;
		*/
		
		$this->view->revisions = $m_profile->getRevisions();
		
		$this->view->model = $this->model;
		
		$revision = intval($this->_getParam('revision'));
		
		if ( ! $revision ) {}
		
		$this->_helper->viewRenderer->setScriptAction('preview');
		
		if ( ! $revision ) {
			$revision = $m_profile->getLatestRevision();
		}
		
		$old_revision = $revision - 1;
		
		$new = $m_profile->getRevision($revision);
		$old = $m_profile->getRevision($old_revision);

		if ( ! $old ) {
			$old = clone $new;
			$old->date_updated = null;
			$old->revision = '-';
			foreach ( $old->data as $field => $value ) {
				if ( ! is_array($value) ) $value = '';
				else $value = array();
				
				$old->data[$field] = $value;
			}
		}
		
		if ( ! $new ) {
			$this->_setParam('revision', $old->revision);
			$this->_setParam('no_changes', true);
			return $this->_forward('revisions');
		}

		$langs = Cubix_I18n::getLangs();

		foreach($langs as $lang_id) {
			$new->data['about'][$lang_id] = $new->data['about_' . $lang_id];
			$old->data['about'][$lang_id] = $old->data['about_' . $lang_id];
			unset($new->data['about_' . $lang_id]);
			unset($old->data['about_' . $lang_id]);

			$new->data['svc_additional'][$lang_id] = $new->data['additional_service_' . $lang_id];
			$old->data['svc_additional'][$lang_id] = $old->data['additional_service_' . $lang_id];
			unset($new->data['additional_service_' . $lang_id]);
			unset($old->data['additional_service_' . $lang_id]);
		}

		$new->data['availability']['incall_type'] = $new->data['incall_type'];
		$new->data['availability']['incall_hotel_room'] = $new->data['incall_hotel_room'];
		$new->data['availability']['incall_other'] = $new->data['incall_other'];
		$new->data['availability']['outcall_type'] = $new->data['outcall_type'];
		$new->data['availability']['outcall_other'] = $new->data['outcall_other'];
		unset($new->data['incall_type']);
		unset($new->data['incall_hotel_room']);
		unset($new->data['incall_other']);
		unset($new->data['outcall_type']);
		unset($new->data['outcall_other']);
		$old->data['availability']['incall_type'] = $old->data['incall_type'];
		$old->data['availability']['incall_hotel_room'] = $old->data['incall_hotel_room'];
		$old->data['availability']['incall_other'] = $old->data['incall_other'];
		$old->data['availability']['outcall_type'] = $old->data['outcall_type'];
		$old->data['availability']['outcall_other'] = $old->data['outcall_other'];
		unset($old->data['incall_type']);
		unset($old->data['incall_hotel_room']);
		unset($old->data['incall_other']);
		unset($old->data['outcall_type']);
		unset($old->data['outcall_other']);

        unset($new->data['bubble_text']);

		//print_r($new); die;

		$this->view->old = $old;
		$this->view->new = $new;
		
		$this->view->diff = $m_profile->differProfile($old->data, $new->data);
		
		return;
	}
	
	public function previewAction()
	{
		
	}

	public function ordersDataAction()
	{
		$this->view->layout()->disableLayout();

		$m_orders = new Model_Billing_Orders();
		$req = $this->_request;

		$data = new Cubix_Form_Data($this->_request);
		
		$data->setFields(array(
			'page' => 'int',
			'per_page' => 'int',
			'sort_field' => '',
			'sort_dir' => '',			
			'user_id' => ''
		));
		
		$data = $data->getData();

		$filter = array();

		if ( strlen($data['user_id']) > 0 ) {
			$filter['u.id = ?'] = $data['user_id'];
		}

		$data = $m_orders->getAll(
			$filter,
			$req->page,
			$req->per_page,
			$req->sort_field,
			$req->sort_dir
		);

		die(json_encode($data));
	}

	public function salesWorkAction()
	{
		$bu_user = Zend_Auth::getInstance()->getIdentity();

		if ( 'data entry' == $bu_user->type  || 'data entry plus' == $bu_user->type) {
			return $this->_forward('permission-denied', 'error');
		}

		$this->view->layout()->disableLayout();
		
		$req = $this->_request;
		$this->view->user_id = $user_id = intval($req->user_id);
		$escort_id = intval($req->escort_id);

		if ( $escort_id ) {
			$escort = $this->model->get($escort_id);
		}
		else {
			$escort = $this->model->getByUserId($user_id);
		}
		

		$this->view->escort = $escort;
		
		$active_packages = $escort->getActivePackages();

		$this->view->total_orders_price = $escort->getTotalOrdersPrice();
		$this->view->total_orders_paid = $escort->getTotalOrdersPrice(true);
		

		if ( $escort->agency_id ) {
			$this->view->is_agency_escort = true;
		}

		$m_o_packages = new Model_Billing_Packages();
		$act_packages = array();		
		if ( count($active_packages) > 0 )
		{
			foreach ( $active_packages as $k => $active_package )
			{
				$package = $m_o_packages->get($active_package->id);

				if ( ! is_null($active_package->order_id) ) {
					$products = $package->getProducts();
					$opt_products = $package->getProducts(Model_Products::TYPE_OPTIONAL_PRODUCT);
				}
				else {
					$products = $package->getDefaultProducts();
					$opt_products = $package->getDefaultProducts(Model_Products::TYPE_OPTIONAL_PRODUCT);
				}


				if ( count($products) > 0 ) {
					foreach( $products as $k => $product )
					{
						$products[$k]->name = Cubix_I18n::translate($product->name);
					}
				}

				if ( count($opt_products) > 0 ) {
					foreach( $opt_products as $k => $product )
					{
						$opt_products[$k]->name = Cubix_I18n::translate($product->name);
					}
				}

				$active_package->package_products = $products;
				$active_package->package_opt_products = $opt_products;
				
				//FIXING PACKAGES LOGIC
				//IF PACKAGE EXPIRES 19 APRIL IT'S ACTUALLY EXPIRES 18 APRIL 23:59
				$active_package->expiration_date = strtotime('-1 day', $active_package->expiration_date); // -1 day
				
				$act_packages[] = $active_package;
			}
		}
		$this->view->active_packages = array('data' => $act_packages);
		//print_r($act_packages);
	}

	public function getOrdersTotalAction()
	{
		$this->view->layout()->disableLayout();

		$req = $this->_request;
		$escort_id = intval($req->escort_id);

		$m_escort = new Model_EscortsV2();

		$escort = $m_escort->get($escort_id);

		$data = array(
			'total_orders_price' => $escort->getTotalOrdersPrice(),
			'total_orders_paid' => $escort->getTotalOrdersPrice(true)
		);

		die(json_encode($data));
	}

	public function addSnapshotsAction()
	{
		$m_snapshot = new Model_Escort_SnapshotV2();
		$m_snapshot->getNoSnapshotEscorts();

		die;
	}

	public function get_montly_price($type, $advertise_type, $country, $category, $days = 30, $escorts_count = 3)
    {
    	if($type == 'escort'){
	    	if($advertise_type == 'vip'){
	    		 if(in_array($country, array(68, 10, 24 , 23, 33, 67 ))){
	    		 	// A COUNTRIES
	    		 	switch ($days) {
						case 15:
							return 27;
							break;
						case 30:
							return 49;
							break;	
						case 60:
							return 99;
							break;
						default:
							return 27;
							break;
					}
	    		 }else{
					// B COUNTRIES
					switch ($days) {
						case 15:
							return 20;
							break;
						case 30:
							return 34;
							break;	
						case 60:
							return 75;
							break;
						default:
							return 20;
							break;
					}
	    		 }
	    	}elseif($advertise_type == 'premium'){
	    		 if(in_array($country, array(68, 10, 24 , 23, 33, 67 ))){
	    		 	// A COUNTRIES
	    		 	switch ($days) {
						case 15:
							return 15;
							break;
						case 30:
							return 29;
							break;	
						case 60:
							return 75;
							break;
						default:
							return 15;
							break;
					}
	    		 }else{
					// B COUNTRIES
					switch ($days) {
						case 15:
							return 10;
							break;
						case 30:
							return 19;
							break;	
						case 60:
							return 51;
							break;
						default:
							return 10;
							break;
					}
	    		 }
	    	}
    	}elseif($type == 'agency'){
    		 if(in_array($country, array(68, 10, 24 , 23, 33, 67 ))){
				// A COUNTRIES
				if($escorts_count == 3){
					switch ($days) {
						case 15:
							return 80;
							break;
						case 30:
							return 140;
							break;	
						case 60:
							return 250;
							break;
						default:
							return 80;
							break;
					}
				}elseif($escorts_count == 6){
					switch ($days){
						case 15:
							return 159;
							break;
						case 30:
							return 275;
							break;	
						case 60:
							return 580;
							break;
						default:
							return 159;
							break;
					}
				}
    		 }else{
    		 	// B COUNTRIES
    		 	if($escorts_count == 3){
					switch ($days) {
						case 15:
							return 59;
							break;
						case 30:
							return 95;
							break;	
						case 60:
							return 200;
							break;
						default:
							return 59;
							break;
					}
				}elseif($escorts_count == 6){
					switch ($days) {
						case 15:
							return 117;
							break;
						case 30:
							return 185;
							break;	
						case 60:
							return 380;
							break;
						default:
							return 117;
							break;
					}
				}
    		 }
    	} 
    }

	public function calculateFuckometersAction()
	{
		set_time_limit(0);
		$db = Zend_Registry::get('db');
		$escorts = $db->fetchAll('SELECT id FROM escorts WHERE fuckometer = 0');
		foreach ( $escorts as $escort ) {
			$escort = new Model_EscortV2Item($escort);
			$escort->updateFuckometer();
		}

		die;
	}

	/**
	 * This func have to be executed every night
	 *
	 * At the beginning top20 and fuckometer ranks will be update
	 * Then if the month changed will save the winner of GOTM into top20 history table
	 */
	public function gotmCronAction()
	{	
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$db = Zend_Registry::get('db');

		$model = new Model_EscortsV2();
		/*$escort_id = $model->getIdByShowname('CeliaVip');
		$escort = new Model_EscortV2Item();
		$escort->setId($escort_id->id);*/

		Model_EscortsV2::updateTop20Positions();
		Model_EscortsV2::updateFuckometerRanks();
		
		// If it is first day of month, that means that winner needs to be annouced
		if ( 1 == intval(date('j')) ) {
			if (in_array(Cubix_Application::getId(), array(APP_BL, APP_AE))) // beneluxxx, asianescorts
			{
				$countries = $db->fetchAll('SELECT id FROM countries');
				$cids = array();
				
				foreach ($countries as $country)
					$cids[] = $country->id;
				
				$countries_str = implode(',', $cids);
				
				$escorts = $db->fetchAll('
					SELECT e.id FROM escorts e
					INNER JOIN order_packages op ON op.escort_id = e.id AND op.application_id = ? AND op.status = ' . Model_Billing_Packages::STATUS_ACTIVE . '
					WHERE e.votes_count >= 1 AND e.status & 32 AND e.gender = 1 AND e.country_id IN (' . $countries_str . ')
					GROUP BY e.id
					ORDER BY e.votes_count DESC
				', array(Cubix_Application::getId()));
			}
			else
			{
				if (in_array(Cubix_Application::getId(), array(APP_6A, APP_EF))) // 6a, ef, 
				{
					$escorts = $db->fetchAll('
						SELECT e.id FROM escorts e
						INNER JOIN order_packages op ON op.escort_id = e.id AND op.application_id = ? 
							AND op.expiration_date >= CURDATE() AND op.status = ' . Model_Billing_Packages::STATUS_ACTIVE . '
						INNER JOIN order_package_products opp ON opp.order_package_id = op.id
						INNER JOIN products p ON p.id = opp.product_id AND p.name = ?
						WHERE e.votes_count >= 1 AND e.status & 32 AND e.gender = 1 AND e.country_id = ?
						GROUP BY e.id
						ORDER BY e.votes_count DESC
					', array(Cubix_Application::getId(), 'girl_of_the_month', Cubix_Application::getById()->country_id));
				}
				else
				{
					$escorts = $db->fetchAll('
						SELECT e.id FROM escorts e
						INNER JOIN order_packages op ON op.escort_id = e.id AND op.application_id = ? AND op.status = ' . Model_Billing_Packages::STATUS_ACTIVE . '
						WHERE e.votes_count >= 1 AND e.status & 32 AND e.gender = 1 AND e.country_id = ?
						GROUP BY e.id
						ORDER BY e.votes_count DESC
					', array(Cubix_Application::getId(), Cubix_Application::getById()->country_id));
				}
			}
			
			$month = intval(date('n'));
			$year = intval(date('Y'));
						
			$month--;
			if ( 0 == $month ) {
				$month = 12;
				$year--;
			}
			
			if(Cubix_Application::getId() == APP_BL){
				$restriction_months = 12;
			}
			else{
				$restriction_months = 3;
			}
			$where_date_arr = array();
			for($i= 1; $i <= $restriction_months; $i++ ){
				//Get last 3 GOTM winner's year and month
				$p_month = $month - $i;
				$p_year = $year;
				if($p_month <= 0){
					$p_month = 12 + $p_month;
					$p_year--;
				}
				$where_date_arr[]= "( year = ".$p_year." AND month = ".$p_month.") ";
			}
			$where_date = implode(' OR ', $where_date_arr);

			foreach ( $escorts as $escort ) {
				// check if escort was GOTM of last 3 months, if so we need to take
				// another one as winner
				$was = $db->fetchOne('
					SELECT TRUE FROM girl_of_month
					WHERE ('.$where_date .') AND escort_id = ? AND application_id = ?
				', array($escort->id, Cubix_Application::getId()));
				
				if ( ! $was ) {
					break;
				}
			}
			
			if (in_array(Cubix_Application::getId(), array(APP_EF, APP_6A, APP_6C,APP_EG, APP_EG_CO_UK, APP_BL))){
				$gotm_model = new Model_Gotm();

				if ($winner_id = $gotm_model->getWinnerId()) {
					$escort->id = $winner_id;
					$db->query('UPDATE gotm_winner_id SET status = 0 WHERE status = 1' );
				}
			}
			
			// check if escort was GOTM before
			$was_gotm = $db->fetchOne('SELECT TRUE FROM girl_of_month WHERE escort_id = ?', array($escort->id));
			
			if ($was_gotm) {
				$db->query('UPDATE girl_of_month SET was_gotm = 1 WHERE escort_id = ?', $escort->id);
			}
			
			$gotm_votes = $db->fetchRow('SELECT votes_count, voted_members FROM escorts	WHERE id = ?', array($escort->id));
			
			$sales_user_id = $db->fetchOne('SELECT u.sales_user_id FROM users u LEFT JOIN escorts e ON e.user_id = u.id WHERE e.id = ?', $escort->id);
			
			$db->insert('girl_of_month', array(
				'escort_id' => $escort->id,
				'month' => $month,
				'year' => $year,
				'votes_count' => $gotm_votes->votes_count,
				'voted_members' => $gotm_votes->voted_members,
				'application_id' => Cubix_Application::getId(),
				'sales_user_id' => $sales_user_id
			));

			$db->query('
				UPDATE escorts e
				INNER JOIN users u ON e.user_id = u.id
				SET e.votes_count = 0, e.voted_members = NULL
				WHERE u.application_id = ?
			', Cubix_Application::getId());

			echo 'Winner Escort of ' . ($month . '/' . $year) . ' is #' . $escort->id;
		}

		echo "\n" . 'Done';
	}

	public function ajaxGetToursAction()
	{
		$escort_id = intval($this->_getParam('escort_id'));

		$m = new Model_EscortsV2();
		$tours = $m->getTours($escort_id);

		die(json_encode($tours));
	}

    /**
	 * This func have to be executed every night
	 *
	 * This will change status of escort
     * 
	 */

	public function suspiciousCronAction()
	{		
		$this->_helper->viewRenderer->setNoRender();
        $this->view->layout()->disableLayout();
		
		$do_nothing_apps = array(APP_6A, APP_EF);
		$has_under_invest_apps = array(APP_6A, APP_EF, APP_6B, APP_EG_CO_UK, APP_ED);
		
		$db = Zend_Registry::get('db');
		
		if ( in_array(Cubix_Application::getId(), $has_under_invest_apps) ) {						
			$invests = $db->query('SELECT id FROM escorts WHERE DATE_SUB(NOW(), INTERVAL +48 HOUR) > under_investigation_date AND is_suspicious = 2')->fetchAll();
			
			foreach ($invests as $i)
			{
				$db->update('escorts', array(
					'is_suspicious' => 1,
					'photo_suspicious_date' => new Zend_Db_Expr(('NOW()')),
					'under_investigation_date' => NULL
				), $db->quoteInto('id = ?', $i->id));
				
				$db->update('escort_photos', array(
					'is_suspicious' => 1
				), $db->quoteInto('escort_id = ?', $i->id));
			}
		}
		
		if ( in_array(Cubix_Application::getId(), $do_nothing_apps) )
			die;	
		
		$model = new Model_EscortsV2();
		
		$h = '24';
		
		if (Cubix_Application::getId() == APP_6B)
			$h = '168';

        $escortsBlock = $db->fetchAll("SELECT e.id,e.photo_suspicious_date as date
            FROM escorts e WHERE is_suspicious = 1
            AND DATE_SUB(now(), INTERVAL +" . $h . " HOUR) > photo_suspicious_date
            AND DATE_SUB(now(), INTERVAL +216 HOUR) < photo_suspicious_date AND photo_suspicious_date IS NOT NULL AND NOT (e.status & ?)", array(Model_EscortsV2::ESCORT_STATUS_SUSPICIOUS_DELETED) );

        $escortsDelete = $db->fetchAll("SELECT e.id,e.photo_suspicious_date as date
            FROM escorts e
            LEFT JOIN verify_requests vr ON vr.escort_id = e.id
            WHERE is_suspicious = 1
            AND DATE_SUB(now(), INTERVAL +216 HOUR) >= photo_suspicious_date AND (vr.status != 1 OR vr.status IS NULL) AND photo_suspicious_date IS NOT NULL AND NOT (e.status & ?)", array(Model_EscortsV2::ESCORT_STATUS_DELETED) );

        if(count($escortsBlock) > 0) {
            foreach ( $escortsBlock as $escort ) {
                $_status = new Cubix_EscortStatus($escort->id);
                $_status->setStatus(Cubix_EscortStatus::ESCORT_STATUS_SUSPICIOUS_DELETED);
                $_status->save();
            }
        }

        if(count($escortsDelete) > 0) {
            foreach ( $escortsDelete as $escort ) {
                $_status = new Cubix_EscortStatus($escort->id);
                $_status->setStatus(Cubix_EscortStatus::ESCORT_STATUS_DELETED);
                $_status->save();

                $model->delete($escort->id);
            }
        }

        echo count($escortsBlock).' Escorts Blocked AND '.count($escortsDelete).' Escorts Deleted';
		die;
    }

	public function editExpirationDateAction()
	{
		$escort_id = intval($this->_getParam('escort_id'));
		$m_packages = new Model_Billing_Packages();
		$package = $m_packages->getByEscortId($escort_id);
		//print_r($package);
		
		//FIXING PACKAGES LOGIC
		//IF PACKAGE EXPIRES 19 APRIL IT'S ACTUALLY EXPIRES 18 APRIL 23:59
		$package->expiration_date = date('d M Y', strtotime('-1 day', strtotime($package->expiration_date))); // -1 day sales
		$this->view->package = $package;
		if ( $this->_request->isPost() ) {

			$new_exp_date = $this->_request->new_expiration_date;
			$op_id = $this->_request->op_id;
			$escort_id = $this->_request->escort_id;

			try {
				$new_exp_date = strtotime('+1 day', $new_exp_date);
				$m_packages->updateExpirationDate($new_exp_date, $op_id, $escort_id);

				Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PACKAGE_EXPIRATION_UPDATED, array('id' => $op_id));
			}
			catch ( Exception $e ) {
				
			}
		}
	}

	public function assignSalesAction()
	{
		$req = $this->_request;

		$hash = $req->hash;
		$action = $req->act;

		$action_map = array('confirm', 'reject');

		if ( ! in_array($action, $action_map) ) {
			die('Wrong Action!');
		}

		$m_saver = new Model_Escort_SaverV2();
		$m_saver->assignSales($action, $hash);

		$this->view->act = $action;
	}
	
	public function toggleVipAction()
	{
		$escort_id = intval($this->_request->escort_id);
		if ( ! $escort_id ) die;
		$this->model->toggleVip($escort_id);
		
		Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_VIP_TOGGLED, array('escort_id' => $escort_id));
		
		die;
	}
	
	public function toggleReviewAction()
	{
		$escort_id = intval($this->_request->escort_id);
		if ( ! $escort_id ) die;
		$this->model->toggleReview($escort_id);
		
		Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_REVIEW_TOGGLED, array('escort_id' => $escort_id));
		
		die;
	}
	
	public function toggleCommentAction()
	{
		$escort_id = intval($this->_request->escort_id);
		if ( ! $escort_id ) die;
		$this->model->toggleComment($escort_id);
		
		Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_COMMENT_TOGGLED, array('escort_id' => $escort_id));
		
		die;
	}
	
	public function togglePornAction()
	{
		$escort_id = intval($this->_request->escort_id);
		if ( ! $escort_id ) die;
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		$this->model->togglePorn($escort_id, $bu_user->id);
				
		die;
	}
	
	public function assignToMeAction()
	{
		$user_id = intval($this->_request->user_id);
		$escort_id = $this->_request->escort_id;
		if ( ! $user_id ) die;
		
		$m_saver = new Model_Escort_SaverV2();
		$m_saver->assignSalesByUserId($user_id, $this->user->id, $escort_id);
		
		die;
	}

	public function ajaxAddCommentsAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$comment_data = $this->_request->getPost();
		$comment_data['remember_me'] = $comment_data['remember_me'] ?  date('Y-m-d ' , $comment_data['remember_me'] ) : NULL;
		$comment_data['date'] = date('Y-m-d H:i:s');
		$comment_data['email_sent'] = 0;
		Model_EscortsV2::insertEscortComments($comment_data);

		if ($comment_data['agency_id'] && $comment_data['update_agency'])
		{
			$m_a = new Model_Agencies();
			$m_e = new Model_EscortsV2();

			$escorts = $m_a->getActiveEscorts($comment_data['agency_id']);

			if ($escorts)
			{
				foreach ($escorts as $e)
				{
					$m_e->updateHandVerificationDate($e->id);
				}
			}
		}
	}

	public function ajaxAddResponseCommentAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$comment_data = $this->_request->getPost();
		$comment_data['date'] = date('Y-m-d H:i:s');
		
		Model_EscortsV2::insertEscortResponseComment($comment_data);
		if($comment_data['response'] == 'confirmed'){
			$m_e = new Model_EscortsV2();
			$m_e->updateHandVerificationDate( $comment_data['escort_id'] );
		}
	}
	
	public function ajaxAddCommentsCheckAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$comment_data = $this->_request->getPost();
		$comment_data['date'] = date('Y-m-d H:i:s');
		$comment_data['next_check_date'] = isset($comment_data['next_check_date']) ? date('Y-m-d',$comment_data['next_check_date'] ) : NULL;
		$comment_data['is_check_profile'] = 1;
		
		Model_EscortsV2::insertEscortComments($comment_data);

	}

	public function ajaxCommentsHistoryAction()
	{
		$this->view->layout()->disableLayout();
		$is_agency = false;
		$req = $this->_request;
		$id = isset($req->escort_id) ? $req->escort_id : NULL ;
		$page = isset($req->page) ? $req->page : 1;
		if( isset($req->agency_id) ){
			$id = $req->agency_id;
			$is_agency = true;
		}
		$per_page = 5;
		$this->view->page = $page;
		$this->view->per_page = $per_page;
		$this->view->definitions= Zend_Registry::get('defines');

		if(false && Cubix_Application::getId() == APP_ED && $is_agency){
			$db = Zend_Registry::get('db');
			$agencyModel = new Model_Agencies();
			$agencyModel->UpdateAgencyEscorts($id,array('date_last_modified' => new Zend_Db_Expr('NOW()')));
            $agencyModel->AddAgencyComment($id);

			$db->update('agencies', array('last_modified' =>  new Zend_Db_Expr('NOW()')), 'id = '.$id);
		}
		if(Cubix_Application::getId() == APP_ED && !$is_agency){
			$this->view->comments_history = Model_Escort_CheckProfile::getCommentsHistoryByEscortId($id, $page, $per_page, $count);
		}
		else{
			$this->view->comments_history = Model_EscortsV2::getCommentsHistoryByEscortId($id,$is_agency, $page, $per_page, $count);
		}
		$this->view->count = $count;
		
		$this->view->user_type = $this->user->type;
	}

	public function ajaxResponseCommentsHistoryAction()
	{
		$this->view->layout()->disableLayout();
		$is_agency = false;
		$req = $this->_request;
		$id = isset($req->escort_id) ? $req->escort_id : NULL ;
		$page = isset($req->page) ? $req->page : 1;
		if( isset($req->agency_id) ){
			$id = $req->agency_id;
			$is_agency = true;
		}
		$per_page = 5;
		$this->view->page = $page;
		$this->view->per_page = $per_page;
		$this->view->definitions= Zend_Registry::get('defines');
		
		if(Cubix_Application::getId() == APP_ED && !$is_agency){
			$this->view->comments_history = Model_Escort_CheckProfile::getResponseCommentsHistoryByEscortId($id, $page, $per_page, $count);
		}
		else{
			$this->view->comments_history = Model_EscortsV2::getCommentsHistoryByEscortId($id,$is_agency, $page, $per_page, $count);
		}
		$this->view->count = $count;
		
		$this->view->user_type = $this->user->type;
	}
	
	public function ajaxStatusHistoryAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		$id = isset($req->escort_id) ? $req->escort_id : NULL ;
		$page = isset($req->page) ? $req->page : 1;
		$period = isset($req->period) ? (int)$req->period : 0;
        $per_page = 5;
        $this->view->page = $page;
        $this->view->per_page = $per_page;
		if ( isset( $period ) AND $period > 0 ) {
            $date = new DateTime();
            $date->modify('+1 month');
            $tables = [ 'journal' ];
            while( $period-- > 0 )
            {
                $tables[] = 'journal_' . date("Y_m",strtotime('-'.$period." month")) . '_01_';
            }
        }
		$this->view->status_history = Model_EscortsV2::getStatusHystoryByEscortId(  $id, $page, $per_page, $count, $tables );
		$this->view->count = $count;
	}

	public function ajaxRemoveCommentAction()
	{
		$this->view->layout()->disableLayout();
		
		if ($this->user->type != 'superadmin')
			die;
		
		$id = intval($this->_request->id);
		
		if ($id)
		{
			$this->model->removeEscortComment($id);
		}
		
		die;
	}

	public function assignToAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$user_id = $this->view->user_id = $req->user_id;
		$escort_id = $this->view->escort_id = $this->_request->escort_id;
		$multiple = $this->view->multiple = isset($req->multiple) && $req->multiple == 1;
		if ( $req->isPost() ) {
			$sales_id = $req->sales_id;
			$validator = new Cubix_Validator();
			if ($multiple && in_array(Cubix_Application::getId(), array(APP_6B, APP_EG_CO_UK, APP_ED))){
                $escort_ids= explode(';', $escort_id);
			    foreach ($escort_ids as $id){
                    $m_saver = new Model_Escort_SaverV2();
                    $m_escorts = new Model_EscortsV2();
                    $user_id = $m_escorts->getUserId($id);
                    $m_saver->assignSalesByUserId($user_id, $sales_id, $escort_id);
                }
            }else{
                $m_saver = new Model_Escort_SaverV2();
                $m_saver->assignSalesByUserId($user_id, $sales_id, $escort_id);
            }
			die(json_encode($validator->getStatus()));
		}
		else {
		    if ($multiple){
                $this->view->escort_id =implode(';',$req->id);
            }
			$bu_model = new Model_BOUsers();
			$this->view->sales_persons = $bu_model->getAllSales();
		}
	}
	
	public function messageAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$id = $this->view->id = $req->id;
		
		if ( $req->isPost() ) {
			$message = $req->urgent_message;
			
			foreach ($id as $i)
				$this->model->setUrgentMessage($i, $message);
		}
		else 
		{
			if (count($id) == 1)
			{
				$this->view->urgent_message = $this->model->getUrgentMessage($id[0]);
			}
		}
	}
	
	public function addAdminCommentAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$id = $this->view->id = $req->id;
		
		if ( $req->isPost() ) {
			$admin_verified = intval($req->admin_verified);
			$due_date = $req->due_date;
			$free_comment = $req->free_comment;
			
			foreach ($id as $i)
				$this->model->addAdminComment($i, $admin_verified, $free_comment, $due_date, $this->user->id);
		}
	}
	
	public function ajaxGetTemplatesAction()
	{
		$req = $this->_request;
		$id = $req->id;
		if($id == '0'){
			die(json_encode('clear'));
		}
		$lang =  $req->lang;
		$app_id = Cubix_Application::getId();
		$model = new Model_EscortTemplates();
		$escort_templates = $model->get($id, $app_id);

		die(json_encode($escort_templates));
	}
	
	public function recoverAction()
	{
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		if ( $bu_user->type == "superadmin" || 
			( Cubix_Application::getId() == APP_BL && $bu_user->type == 'admin' && $bu_user->id == 73 ) || 
			( Cubix_Application::getId() == APP_EM && $bu_user->type == 'admin' && $bu_user->id == 75 )	||
			( Cubix_Application::getId() == APP_ED && $bu_user->type == 'admin' && $bu_user->id == 1 )
		){
			$escort_id = intval($this->_request->id);
			$status = new Cubix_EscortStatus($escort_id);

			$status->removeStatusBit( Model_EscortsV2::ESCORT_STATUS_TEMPRARY_DELETED );
			$status->removeStatusBit( Model_EscortsV2::ESCORT_STATUS_DELETED );
			$status->removeStatusBit( Model_EscortsV2::ESCORT_STATUS_SUSPICIOUS_DELETED );
			$status->save();
			$model_escorts = new Model_Escort_SaverV2();
			$model_escorts->removeDeletedDate($escort_id);
			$m_snapshot = new Model_Escort_SnapshotV2(array('id' => $escort_id));
			$m_snapshot->snapshotProfileV2();
			die();
		}
		else{
			die('No Permission');
		}
	}
	
	public function setStatusAction()
	{
		$bu_user = Zend_Auth::getInstance()->getIdentity();
        $admins = ['tom-' . APP_BL, 'Enzo-' . APP_EM];
        $currentAdminKey = $bu_user->first_name . '-' . Cubix_Application::getId();

		if ($bu_user->type == "superadmin" || $bu_user->id == 204 || in_array($currentAdminKey, $admins) || (Cubix_Application::getId() == APP_BL && $bu_user->id == 73)){
			$escort_ids = $this->_request->id;
			$status = intval($this->_request->status);

			foreach($escort_ids as $escort_id)
			{
                if ( in_array(Cubix_Application::getId(),array(APP_ED,APP_EG_CO_UK)) && $status & ESCORT_STATUS_ADMIN_DISABLED )
                {
                    $model_escorts = new Model_EscortsV2();
                    $user_id = Model_EscortsV2::getUserId($escort_id);
                    $session_id = $this->model->getSessionId($user_id);
                    if( $session_id )
                    {
                        if ( Cubix_Application::getId() == APP_EG_CO_UK )
                        {
                            session_id($session_id);
                            session_start();
                            session_destroy();
                            session_commit();
                        }else{
                            $memcache_obj = new Memcache;
                            $memcache_obj->connect('memcached', 11214);
                            $memcache_obj->delete($session_id);
                        }
                    }
                }
				$status_object = new Cubix_EscortStatus($escort_id);
				$status_object ->setStatus($status);
				$status_object ->save();
			}
		}
		else{
			die('No Permission');
		}
		die;
	}		

	public function setPopundersAction()
	{
		$this->view->layout()->disableLayout();
		
		$photo_id = (int) $this->_request->photo_id;
		$photo_model = new Model_Escort_Photos();
		$photo = $photo_model->get($photo_id);
		$this->view->photo = $photo;
		$this->view->photo_data = json_encode(array(
			'id' => $photo->id,
			'is_portrait' => 1,
			'args' => unserialize($photo->popunder_args),
			'type' => "popunder"
		));
		
		
	}
	
	public function cropPopunderAction()
	{
		$photo_id = intval($this->_getParam('photo_id'));
		$model = new Model_Escort_Photos();
		$photo = $model->get($photo_id);
        $ext = $photo->ext;
        $hash = $photo->getHash();
		$result = array(
			'x' => intval($this->_getParam('x')),
			'y' => intval($this->_getParam('y')),
			'px' => round(floatval($this->_getParam('px')), 4),
			'py' => round(floatval($this->_getParam('py')), 4),
			'rwidth'=>intval($this->_getParam('rwidth')),
			'rheight'=>intval($this->_getParam('rheight'))
		);
		$photo->setPopunderCropArgs($result);

		// Crop all images
		$size_map = array(
			'l_punder_thumb' => array('width' => 950, 'height' => 310, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
		);
		
		$conf = Zend_Registry::get('images_config');

		get_headers($conf['remote']['url'] . '/get_image.php?a=clear_cache&app=' . Cubix_Application::getById($this->_getParam('application_id'))->host . '&eid=' . $photo->escort_id . '&hash=' . $photo->hash);
		//echo $conf['remote']['url'] . '/get_image.php?a=clear_cache&app=' . Cubix_Application::getById($this->_getParam('application_id'))->host . '&eid=' . $photo->escort_id . '&hash=' . $photo->hash;
		
		$catalog = $photo->escort_id;

		$a = array();
		if ( is_numeric($catalog) ) {
			$parts = array();

			if ( strlen($catalog) > 2 ) {
				$parts[] = substr($catalog, 0, 2);
				$parts[] = substr($catalog, 2);
			}
			else {
				$parts[] = '_';
				$parts[] = $catalog;
			}
		}
		else if ( preg_match('#^([0-9]+)/(.+)$#', $catalog, $a) ) {
			array_shift($a);
			$catalog = $a[0];

			$parts = array();

			if ( strlen($catalog) > 2 ) {
				$parts[] = substr($catalog, 0, 2);
				$parts[] = substr($catalog, 2);
			}
			else {
				$parts[] = '_';
				$parts[] = $catalog;
			}

			$parts[] = $a[1];
		}

			$catalog = implode('/', $parts);
			$resize = (int)$this->_getParam('resize');
            $kheight = floatval($photo->height/$resize);
			$width =$photo->width;
			$x = 0;
			$y = $result['y']*$kheight+2;
			$height =$kheight*$result['rheight'];
		
		foreach($size_map as $size => $sm) {
			//echo $conf['remote']['url'] . '/' . Cubix_Application::getById($this->_getParam('application_id'))->host . '/' . $catalog . '/' . $hash . '_' . $size . '.jpg?args=' . $x . ':' . $y.'&cwidth='.$width.'&cheight='.$height; die;
			get_headers($conf['remote']['url'] . '/' . Cubix_Application::getById($this->_getParam('application_id'))->host . '/' . $catalog . '/' . $hash . '_' . $size . '.'. $ext .'args=' . $x . ':' . $y.'&cwidth='.$width.'&cheight='.$height);
		}
		Cubix_SyncNotifier::notify($photo->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED, array('id' => $photo->id, 'action' => 'popunder adjustment changed'));
		echo json_encode(array('success' => true));
		die;
			
	}
	
	public function savePopunderAction()
	{
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		if ($bu_user->type == "superadmin"){
			$escort_id = $this->_request->escort_id;
			$photo_id = $this->_request->photo_id;
			$this->model->setPopunderV2($escort_id,$photo_id);
			
		}
		else{
			die('No Permission');
		}
		die;
	}		
	public function setPopunderAction()
	{
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		if ($bu_user->type == "superadmin"){
			$escort_ids = $this->_request->id;
			
			foreach($escort_ids as $escort_id)
			{	
				$this->model->setPopunder($escort_id);
			}
		}
		else{
			die('No Permission');
		}
		die;
	}		
	
	// Escorts copy feature

	const RESULT_ESCORT_ALREADY_TAKEN = 1;
	const RESULT_ESCORT_TAKEN_SUCCESSFULLY = 2;
	const RESULT_ESCORT_VALIDATION_ERROR = 4;
	const RESULT_ESCORT_PHOTO_SUCCESS = 5;
	const RESULT_ESCORT_PHOTO_FAIL = 6;
	const RESULT_ESCORT_DOESNT_EXIST = 7;

	const RESULT_AGENCY_ALREADY_TAKEN = 91;
	const RESULT_AGENCY_TAKEN_SUCCESSFULLY = 92;
	const RESULT_AGENCY_ALREADY_EXISTS = 93;
	const RESULT_AGENCY_VALIDATION_ERROR = 94;
	const RESULT_AGENCY_LOGO_SUCCESS = 95;
	const RESULT_AGENCY_LOGO_FAIL = 96;
	const RESULT_AGENCY_DOESNT_EXIST = 97;

	public function copyAction()
	{
		$this->view->layout()->disableLayout();

		$escorts = json_decode(urldecode($_SERVER['QUERY_STRING']));
		
		$this->view->escorts = $escorts;
		
		$country_model = new Model_Countries();
		
		$results = array();

		if ( $this->_request->isPost() || null !== $this->_request->test_submit ) {
			$target_application_id = (int) $this->_getParam('target_application_id');

			$client = new Cubix_EscortCopier_Client($target_application_id);
			
			$total_progress = array();

			foreach ( $escorts as $_escort ) {
				$progress = array("Escort <strong>{$_escort->name}</strong>...");

				$escort = $this->model->getProfile($_escort->id);
				$add_data = $this->model->getAdditional($_escort->id);
				$working_locations = $this->model->getWorkingLocations($_escort->id);
				$tours = $this->model->getTours($_escort->id);
				$escort = array_merge((array) $escort, (array) $add_data);
				
				$escort['tours'] = $tours;
				$escort['working_locations'] = $working_locations;
				$escort['country_slug'] = $country_model->getSlugById($escort['country_id']);
				$escort['block_countries'] = $country_model->getBlockCountries($_escort->id);
				
				// if this is agency escort we need to copy agency at first {{{
				if ( $escort['agency_id'] ) {
					$orig_agency_id = $escort['agency_id'];
					$model = new Model_Agencies();
					$data = $model->getById($escort['agency_id']);

					$result = $client->call('CopyListener.takeCopyOfAgency', array($data));
				

					switch ( $result->result ) {
						case self::RESULT_AGENCY_ALREADY_TAKEN:
							$progress[] = "Agency {$data['agency_data']->name} has been already copied";
							break;
						case self::RESULT_AGENCY_TAKEN_SUCCESSFULLY:
							$progress[] = "Agency {$data['agency_data']->name} has been copied successfully, new id is {$result->id}";

							$db = Zend_Registry::get('db');
							list($logo_hash, $logo_ext) = $db->fetchRow('
								SELECT logo_hash, logo_ext
								FROM agencies
								WHERE id = ?
							', $orig_agency_id, Zend_Db::FETCH_NUM);
							if ( $logo_hash && $logo_ext ) {
								$image = new Cubix_Images_Entry();
								$image->hash = $logo_hash;
								$image->ext = $logo_ext;
								$image->catalog_id = 'agencies';
								$image->application_id = $data['agency_data']->application_id;

								$images = new Cubix_Images();
								$orig_logo_url = $images->getUrl($image);
								
								$binary = base64_encode($binary);
								$result = $client->call('CopyListener.takeCopyOfAgencyLogo', array($orig_agency_id, $data['agency_data']->application_id, $orig_logo_url));
								switch ( $result->result ) {
									case self::RESULT_AGENCY_LOGO_SUCCESS:
										$progress[] = "Agency {$data['agency_data']->name} logo successfully copied: " . $result->image_url;
										break;
									case self::RESULT_AGENCY_LOGO_FAIL:
										$progress[] = "Agency {$data['agency_data']->name} logo copying failed: " . $result->error;
										break;
									case self::RESULT_AGENCY_DOESNT_EXIST:
										$progress[] = "Agency {$data['agency_data']->name} doesn't exist in target application. Probably agency copy failed before.";
										break;
								}
							}
							break;
						case self::RESULT_AGENCY_ALREADY_EXISTS:
							$progress[] = "Agency {$data['agency_data']->name} already exists in target application";
							break;
						case self::RESULT_AGENCY_VALIDATION_ERROR:
							$progress[] = "Agency {$data['agency_data']->name} validation failed: " . print_r($result->errors, true);
							break;
					}
				}
				// }}}

				$result = $client->call('CopyListener.takeCopyOfEscort', array($escort));


				switch ( $result->result ) {
					case self::RESULT_AGENCY_DOESNT_EXIST:
						$progress[] = "Agency {$data['agency_data']->name} doesn't exist in target application. Probably agency copy failed before.";
						break;
					case self::RESULT_ESCORT_ALREADY_TAKEN:
						$progress[] = "Escort {$_escort->name} has already been copied";
						break;
					case self::RESULT_ESCORT_TAKEN_SUCCESSFULLY:
						$progress[] = "Escort {$_escort->name} was copied successfully, new id is {$result->id}";
						if ( count($result->warnings) ) {
							foreach ( $result->warnings as $warning ) {
								$progress[] = 'WARNING: ' . $warning;
							}
						}

						// copy escort photos {{{
						$escort = $this->model->get($_escort->id);
						
						foreach ( $escort->getPhotos() as $photo ) {
							// quick fix
							// TODO: NEW SERVER SETUP
							$result = $client->call('CopyListener.takeCopyOfEscortPhoto', array(
								$escort->id,
								(array) $photo,
								$escort->application_id,
								$photo->getUrl(),
								$photo->type == ESCORT_PHOTO_TYPE_PRIVATE
							));
//                            if($escort->id == 71309){
//                                var_dump($result);die;
//                            }
							//var_dump($result);die;
							switch ( $result->result ) {
								case self::RESULT_ESCORT_PHOTO_SUCCESS:
									$progress[] = 'Escort photo successfully copied: ' . $result->image_url;
									break;
								case self::RESULT_ESCORT_PHOTO_FAIL:
									$progress[] = 'Could not copy escort photo: ' . $result->error;
									break;
							}
						}
						// }}}
						break;
					
					default:
						if ( count($result->errors) ) {
							$progress[] = 'There were validation errors';
							foreach ( $result->errors as $errors ) {
								$progress[] = 'VALIDATION ERROR: ' . $errors;
							}
						}
						break;
				}

				$total_progress[$_escort->name] = $progress;
			}
			
			$this->view->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender();

			$html = '';
			foreach ( $total_progress as $escort => $messages ) {
				$html .= '<p style="margin-bottom: 10px">';
				foreach ( $messages as $message ) {
					$html .= $message . "<br/>\n";
				}
				$html .= '</p>';
			}

			echo json_encode(array('status' => 'success', 'result' => $html));
		}

		//die(json_encode(array('status' => 'success')));
		
	}
	
	public function ajaxAddSuspiciousCommentAction()
	{
		$this->view->layout()->disableLayout();
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		$comment_data = $this->_request->getPost();
		$this->view->comment = $comment_data['comment'];
		$this->view->date = $comment_data['date'] = date('Y-m-d');
		$this->view->username = $bu_user->username;
		$comment_data['bo_user_id'] = $bu_user->id;
	
		$this->view->id = Model_EscortsV2::insertSuspiciousComment($comment_data);

	}
	
	public function ajaxDeleteSuspiciousCommentAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$id = $this->_request->getParam('id');
		Model_EscortsV2::deleteSuspiciousComment($id);
	}	
	
	public function ajaxRemoveVerificationAction()
	{
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		if (!in_array($bu_user->type, array('superadmin', 'admin'))){
			die('Permission denied');
		}
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$escort_id = intval($this->_request->id);
		$model = new Model_Escort_Photos();
		$model->clearVerification($escort_id);
		$photos = $model->getPhotosIds($escort_id);
		foreach ( $photos as $photo ) {
			Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED, array('id' => $photo->id, 'action' => 'verified cleared'));
		}
		$model->reset($escort_id);

	}
	
	public function toggleCheckGalleryAction()
	{
		$escort_id = intval($this->_request->escort_id);
		if ( ! $escort_id ) die;
		$this->model->toggleCheckGallery($escort_id);
		die;
	}
	
	public function showGalleryAction()
	{
		$escort_ids = $this->_request->id;
		foreach($escort_ids as $escort_id)
		{	
			$this->model->toggleCheckGallery($escort_id);
		}
		die;
	}
	
	public function aboutMeInformAction()
	{
		$escort_ids = $this->_request->id;
		foreach($escort_ids as $escort_id)
		{	
			$this->model->toggleAboutMeInformed($escort_id);
		}
		die;
	}
	
	// for BL only
	public function ajaxDisplayAfterPackageExpireAction()
	{
		$this->view->layout()->disableLayout();
		
		$escort_id = intval($this->_request->id);
		$flag = intval($this->_request->flag);
		
		if ( ! $escort_id ) die;
		
		$this->model->changeDisplay($escort_id, $flag);
		
		die;
	}
	
	public function generatePassAction()
	{
		$this->view->layout()->disableLayout();
		$user_id = intval($this->_request->user_id);
		$password = $this->model->generatePassword($user_id);
		$user_model = new Model_Users();
		$user = $user_model->get($user_id);
		$email_tpl = 'generate_password';
		$tpl_data = array(
			'username' => $user->username,
			'password' => $password
		);
		Cubix_Email::sendTemplate($email_tpl, $user->email, $tpl_data);
		die;
	}
	
	public function generateMultiplePassAction()
	{
		$this->view->layout()->disableLayout();
		$ids = $this->_request->id;
		$user_model = new Model_Users();
		foreach($ids as $escort_id){
			$user = $user_model->getByEscortId($escort_id);
			$password = $this->model->generatePassword($user->id);
			$email_tpl = 'generate_password';
			$tpl_data = array(
				'username' => $user->username,
				'password' => $password
			);
			Cubix_Email::sendTemplate($email_tpl, $user->email, $tpl_data);
			
		}
		die;
	}
	
	public function ajaxAgeVerifyAction()
	{
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		if (!in_array($bu_user->type, array('superadmin', 'admin'))){
			die('Permission denied');
		}
		$this->view->layout()->disableLayout();
		$escort_id = $this->_request->escort_id;
		$type = $this->_request->type;
		$age_verfy_model = new Model_Verifications_AgeVerification();
		$age_verfy_model->updateEscortStatus($escort_id, Model_Verifications_AgeVerification::REQUEST_SENT, $type);
		if($type == Model_Verifications_AgeVerification::REQUEST_TYPE_OFFLINE){
			$status = new Cubix_EscortStatus($escort_id);
			$status->setStatusBit(Cubix_EscortStatus::ESCORT_STATUS_NEED_AGE_VERIFICATION);
			$status->save();
		}
		die;
	}

	public function ajaxPostponeAgeVerifyAction()
	{
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		if (!in_array($bu_user->type, array('superadmin', 'admin'))){
			die('Permission denied');
		}
		$this->view->layout()->disableLayout();
		$escort_id = $this->_request->escort_id;
		$age_verfy_model = new Model_Verifications_AgeVerification();
		$age_verfy_model->postpone24($escort_id);
		$status = new Cubix_EscortStatus($escort_id);
		$status->removeStatusBit(Cubix_EscortStatus::ESCORT_STATUS_NEED_AGE_VERIFICATION);
		$status->save();
		die;
	}

	public function ajaxLoadHandVerificationAction()
	{
		$this->view->layout()->disableLayout();
		$escort_id = (int)$this->_request->escort_id;

		$data = $this->model->getHandVerificationDate($escort_id);
		if($data->hand_verification_sales_id === -1){
			$data->sales_name = 'by system';
		}
		$this->view->data = $data;
		
	}


	public function ajaxLoadTextCopyrightAction()
	{
		$this->view->layout()->disableLayout();
		$escort_id = (int)$this->_request->escort_id;

		$this->view->data = $this->model->getTextCopyright($escort_id);

		
	}



	public function ajaxUpdateHandVerificationDateAction()
	{
        $bu_user = Zend_Auth::getInstance()->getIdentity();
		$this->view->layout()->disableLayout();
		$escort_id = (int)$this->_request->escort_id;
		$ids = $this->_request->id;
		$user_id = $this->model->getUserId($escort_id);

		if ( $escort_id ) {
			$this->model->updateHandVerificationDate($escort_id);
            Model_Users::setExpirationDate($user_id,null,'unset');

			Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('hand_verified_sales_id' => $bu_user->id ));
		} else {
			if ( count($ids) ) {
				foreach ($ids as $id) {
					$this->model->updateHandVerificationDate($id);
					Model_Users::setExpirationDate($user_id,null,'unset');

					Cubix_SyncNotifier::notify($id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('hand_verified_sales_id' => $bu_user->id ));
				}
			}
		}

		die;
	}


	public function ajaxUpdateTextCopyrightAction()
	{
		$this->view->layout()->disableLayout();
		$escort_id = (int)$this->_request->escort_id;
		// $ids = $this->_request->id;

		if ( $escort_id ) {
			$this->model->updateTextCopyright($escort_id);
		}

		die;
	}
	
	public function ajaxAgeCertifyAction()
	{
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		if (!in_array($bu_user->type, array('superadmin', 'admin'))){
			die('Permission denied');
		}
		$this->view->layout()->disableLayout();
		$escort_id = (int)$this->_request->escort_id;
		$model = new Model_Verifications_AgeVerification();
		
		
		$data = array(
			'need_age_verification' => Model_Verifications_AgeVerification::VERIFY,
			'age_verify_disabled' => 0, 
			'age_verify_admin_id' => $bu_user->id,
			'age_verify_date' => new Zend_Db_Expr('NOW()')
		);
		$model->updateEscort($escort_id, $data);
		$escort_status = new Cubix_EscortStatus($escort_id);
		if($escort_status->hasStatusBit(Cubix_EscortStatus::ESCORT_STATUS_NEED_AGE_VERIFICATION)){
			$escort_status->removeStatusBit(Cubix_EscortStatus::ESCORT_STATUS_NEED_AGE_VERIFICATION);
			$escort_status->save();
		}
				
		Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_STATUS_UPDATED, array('Age Certification' => "Age Certify"));
		die;
	}
	
	public function ajaxAgeCancelAction()
	{
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		if (!in_array($bu_user->type, array('superadmin', 'admin'))){
			die('Permission denied');
		}
		$this->view->layout()->disableLayout();
		$escort_id = (int)$this->_request->escort_id;
		$model = new Model_Verifications_AgeVerification();
		
		$data = array(
			'need_age_verification' => 0,
			'age_verify_disabled' => 0,
			'age_verify_type' => 0,
			'age_verify_admin_id' => $bu_user->id,
			'age_verify_date' => null
		);
		
		$model->updateEscort($escort_id, $data);
		$escort_status = new Cubix_EscortStatus($escort_id);
		if($escort_status->hasStatusBit(Cubix_EscortStatus::ESCORT_STATUS_NEED_AGE_VERIFICATION)){
			$escort_status->removeStatusBit(Cubix_EscortStatus::ESCORT_STATUS_NEED_AGE_VERIFICATION);
			$escort_status->save();
		}
		die;
	}		

	public function activateNowAction()
	{
		$this->view->layout()->disableLayout();
		$escort_id = (int)$this->_request->escort_id;

		$this->model->activateNowManual($escort_id);

		Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('Activation By Date' => 'Manually activated'));

		die;
	}

	public function loadRevisionAction()
	{
		$this->view->layout()->disableLayout();

		$escort_id = (int)$this->_request->escort_id;

		$ret = $this->model->loadRevision($escort_id);

		echo json_encode(array_merge($ret, array('escort_id' => $escort_id)));
		die;
	}

	public function editSourceUrlAction()
	{
		$escort_id = $this->view->escort_id = $this->_request->getParam('escort_id');

		$m_escort = new Model_EscortsV2();
		$this->view->source_url = $m_escort->getSourceUrl($escort_id);

		if ( $this->_request->isPost() ) {
			$source_url = trim($this->_request->getParam('source_url'));

			$m_escort->updateSourceUrl($source_url, $escort_id);
		}
	}

	public function happyHourRemoveAction()
	{
		$escort_id = (int)$this->_request->escort_id;

		if ($escort_id)
		{
			$model = new Model_EscortsV2();

			$model->removeHappyHour($escort_id);

			Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('Happy Hour' => 'Manually removed by admin'));
		}

		$success = array('status' => 'success');
        die(json_encode($success));
    }

	public function checkPhoneNumberAction(){
		$phone_number = preg_replace('/[^0-9]/', '', $this->_request->phone_number);
		$phone_prefix = $this->_request->phone_prefix;
		$agency_id = $this->_request->agency_id;
		$escort_id = $this->_request->escort_id;


		list($country_id, $phone_prefix,$ndd_prefix) = explode('-',$phone_prefix);
		
		if($phone_number){
			$contact_phone_parsed = preg_replace('/[^0-9]/', '', $phone_number);
			$contact_phone_parsed = preg_replace('/^'.$ndd_prefix.'/', '', $contact_phone_parsed);
			$contact_phone_parsed = '00'.$phone_prefix.$contact_phone_parsed;
		}

		$results = $this->model->existsByPhone($contact_phone_parsed,$escort_id, $agency_id);
		$resCount = count($results);

		if($resCount > 0) {
			$result = false;
		}else{
			$result = true;
		}

		echo json_encode(array('status' => $result));
		die;
	}

	public function phoneCommentAction(){
		$this->view->escort_id = $escort_id = $this->_request->escort_id;
		$this->view->phone_comment = $this->model->getPhoneComment($escort_id);
	}
	public function savePhoneCommentAction(){
		$escortId = $this->_request->id;
		$phone_comment = $this->_request->phone_comment;

		$this->model->savePhoneComment($escortId,$phone_comment);
		
		echo json_encode(array('status' => 'success'));
		die();
	}

	public function phoneConfirmAction(){
		$this->view->escort_id = $escort_id = $this->_request->escort_id;
		$this->view->phone = $phone = $this->_request->phone;
		$this->view->phone_number_parsed = $phone_number_parsed = $this->_request->phone_number_parsed;
		$this->view->phone_prefix = $phone_prefix = $this->_request->phone_prefix;
	}
	public function savePhoneConfirmAction(){
		$escortId = $this->_request->id;
		$phone_confirm_comment = $this->_request->phone_confirm_comment;
		$phone = $this->_request->phone;
		$phone_parsed = $this->_request->phone_number_parsed;
		$phone_prefix = $this->_request->phone_prefix;

		$this->model->savePhoneConfirm($escortId,$phone_confirm_comment,$phone,$phone_parsed,$phone_prefix);
		
		echo json_encode(array('status' => 'success'));
		die();
	}

	public function duplicateAction(){
		$escortId = $this->_request->escort_id;
		$db = Zend_Registry::get('db');
		
		$master_id = $db->fetchRow('SELECT master_id FROM escorts WHERE id = ?', array($escortId));

		$master_id = $master_id->master_id; 
		
		if(!$master_id){
			$sql = 'UPDATE escorts SET master_id = ? WHERE id = ?';
			$db->query($sql, array($escortId,$escortId));	
			$master_id = $escortId;
		}

		$username = $this->model->usernameByMasterId($master_id);
		$availableTypes = $this->model->getAvailableTypes($master_id);

		$user = $db->fetchRow('SELECT u.user_type, u.id
									  FROM escorts as e
									  LEFT JOIN users u ON u.id = e.user_id
									  WHERE e.id = ?', 
									  $master_id);
		
		$this->view->canEditUsername = false;

		if($user->user_type != 'agency'){		
			if($availableTypes){
				$setKey = array_values($availableTypes);
				$firstType = $setKey[0];
				$username .= strtolower($firstType);
			}
			$this->view->canEditUsername = true;
		}

		$this->view->master_id = $master_id;
		$this->view->availableTypes = $availableTypes;
		$this->view->username = $username;
	}

	public function saveDuplicateAction(){
		$bu_user = Zend_Auth::getInstance()->getIdentity();


		if(!((in_array($bu_user->type, array('superadmin', 'admin'))) && in_array(Cubix_Application::getId(), array(APP_ED)))){
			die('Not allow duplicate');
		}

		if ( $this->_request->isPost() ) {
			$db = Zend_Registry::get('db');

			$fields = array(
				'master_id' => 'int-nz',
				'type' => 'int-nz',
				'new_username' => '',
				'new_reg_email' => '',
			);

			$form = new Cubix_Form_Data($this->_request);
			$form->setFields($fields);
			$data = $form->getData();

			$validator = new Cubix_Validator();


			if(empty($data['type'])){
				$validator->setError('type', 'Required');
			}

			$availableTypes = $this->model->getAvailableTypes($data['master_id']);
			if(!isset($availableTypes[$data['type']])){
				$validator->setError('type', 'Already exist');
			}
			
			if(! strlen($data['master_id'])){
				$validator->setError('master_id', 'Required');
			}

			// Email Validation
			$user = $db->fetchRow('SELECT u.user_type, u.id
											  FROM escorts as e
											  LEFT JOIN users u ON u.id = e.user_id
											  WHERE e.id = ?', 
											  $data['master_id']);


			if($user->user_type == 'agency'){
				$user_id = $user->id;
			}else{
				$user_id = false;
				
				// Name Validation
				if(! strlen($data['new_username'])){
					$validator->setError('new_username', 'Required');
				}

				// if user not agency, we need create new user, for this we check username 
				if($this->model->existsByUsername($data['new_username'])){
					$validator->setError('new_username', 'Username exists');
				}	
			}


			if (!strlen($data['new_reg_email'])) {
				$validator->setError('new_reg_email', 'Required');
			}
			elseif(!$validator->isValidEmail($data['new_reg_email'])){
				$validator->setError('new_reg_email', 'Invalid Email');
			}
			elseif ( Cubix_Application::isDomainBlacklisted($data['new_reg_email']) ) {
				$validator->setError('email', 'Domain is blacklisted');
			}
			elseif($this->model->existsByEmail($data['new_reg_email'], $user_id) && !$this->importId){

				$validator->setError('new_reg_email', 'Email exists');
			}

			if ( $validator->isValid() ) {
				$newEscortId = false;
				$newEscortId = $this->model->duplicateEscort($data,$user_id);
				if(!$newEscortId){
					$validator->setError('type', 'Escort not created');
				}else{
					die(json_encode(array('status' => 'success', 'notClose' => true ,'newId' => $newEscortId )));
				}				
			}

			die(json_encode($validator->getStatus()));
		}
	}

	public function blockCamsAction()
	{
		$escort_id = intval($this->_request->escort_id);
		$status = intval($this->_request->status);
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		if(!in_array($bu_user->type, array('superadmin') )){
			die('Not allow duplicate');
		}
		
		$this->model->setBlockCamStatus($escort_id, $status);
		die;
	}
	
    public function checkSymbols($text){
        $matches = array();
        //allow italian letters
        $withoutSymbols =  array('&nbsp;','&ograve;','&agrave;','&egrave;',
            '&ugrave;','&igrave;','&ccedil;','&deg;','&deg;','&eacute;','&pound;','&amp;','&sect;',
            '&Ugrave;','&Egrave;','&Ograve;','&Eacute;','&Agrave;','&Igrave;','&rsquo;','&acute;',
            '&oacute;','&euro;','&mdash;','&ndash;','&raquo;','&laquo;','&rdquo;','&ldquo;','&hellip;','&aacute;','&gt;','&lt;',
            '&iacute;','&Iacute;','&Icirc;','&icirc;','&iuml;','&Iuml;','&ocirc;','&Ocirc;','&ucirc;','&Ucirc;','&acirc;','&Acirc;',
            '&ecirc;','&Ecirc;','&bull;'
        );
        $t = preg_match_all('/&(.*?){2,8}\;/is', $text, $matches);

        foreach ($withoutSymbols as $key => $value){
            foreach ($matches[0] as $k => $v){
                if($v == $value){
                    unset($matches[0][$k]);
                }
            }
        }

        return implode(' ',$matches[0]);
    }

    public function stripAsci($text){
        $text = str_replace("\xe2\x80\xa8", '<br>', $text);
        $text = str_replace("\xe2\x80\xa9", '<br>', $text);

        $data = preg_replace('#&(.*?){2,8};#si', '',$text );
        $data = str_replace('и','',$data);
        $data = str_replace('я','',$data);
        $data = str_split($data);

        foreach ($data as $key => $value ){
            if(in_array(ord($value),array(239,143,184))){
                unset($data[$key]);
            }
        }

        return implode('',$data);
    }

    public function secureTokenGenerateAction()
    {
    	$user_id = $this->_request->user_id;
    	if ( ! $user_id ) die;
    	
    	$secure_token = $this->model->generateSecureToken($user_id);

    	echo (json_encode(array('secure_token' => $secure_token, 'app_id' => Cubix_Application::getId())));
    	die;    	
    }


    public function exportIndependentAction( $escorts_ids = array() )
    {
        set_time_limit(0);
        ini_set('memory_limit', '2048M');



        $bu_user = Zend_Auth::getInstance()->getIdentity();

        $filter = array();

        if ( !empty($escorts_ids) )
        {
            $escorts_ids = implode(',',$escorts_ids);
            $filter["e.id IN ($escorts_ids)"] = true;
            $headers = array('escort_id', 'escort_type', 'showname', 'username', 'status', 'registration_email', 'contact_email', 'phone_number', 'country', 'city', 'sales_person', 'registration_date', 'last_login_date', 'first_sms', 'last_sms', 'first_email', 'last_email', 'comment', 'commented_by',
                'last_check_date', 'last_package', 'active_package_name', 'package_expiration_date', 'last_login_ip', 'sedcard_url');
        }else{
            $filter['e.agency_id IS NULL'] = true;
            $filter['e.status IN (8,16,32)'] = true;
            $filter['u.sales_user_id = ?'] = $bu_user->id;
            $headers = array('escort_id', 'country', 'username', 'status', 'registration_date', 'last_login_date', 'email',
                'phone_number', 'first_sms', 'last_sms', 'first_email', 'last_email', 'comment', 'commented_by',
                'last_check_date', 'last_package', 'active_package_name', 'package_expiration_date','sedcard_url');
        }

        ignore_user_abort(true);
        ob_start();

        echo (json_encode(array('status' => 'success')));

        ob_end_flush();
        ob_flush();
        flush();

        session_write_close();
        fastcgi_finish_request();

        if ( !empty($escorts_ids) )
        {
            $filename = 'escfiltered_' . hexdec(uniqid()) . '.csv';

        }else{
            $filename = 'escindependents_' . hexdec(uniqid()) . '.csv';
        }
        $handle = fopen('escindependents/' . $filename, 'w');



//        $handle = fopen('php://output', 'w');


        $data = $this->model->getDataForIndependentExport(
            $filter
        );


        fputcsv($handle, $headers);

        if ( !empty($escorts_ids) )
        {
            foreach ($data['data'] as $fld) {
                $arr = array(
                    $fld['escort_id'], $fld['escort_type'], $fld['showname'], $fld['username'], $fld['escort_status'], $fld['email'], $fld['contact_email'], $fld['phone_number'], $fld['country'] , $fld['city'], $fld['sales_person'], $fld['registration_date'], $fld['last_login_date'],
                    $fld['first_sms'], $fld['last_sms'], $fld['first_email'], $fld['last_email'], $fld['comment'], $fld['commented_by'],
                    $fld['last_check_date'], $fld['last_package'], $fld['active_package_name'], $fld['package_expiration_date'], $fld['last_ip'], $fld['sedcard_url']
                );


                fputcsv($handle, $arr);
            }
        }else{
            foreach ($data['data'] as $fld) {
                $arr = array(
                    $fld['escort_id'], $fld['country'], $fld['username'], $fld['escort_status'], $fld['registration_date'], $fld['last_login_date'], $fld['email'],
                    $fld['phone_number'], $fld['first_sms'], $fld['last_sms'], $fld['first_email'], $fld['last_email'], $fld['comment'], $fld['commented_by'],
                    $fld['last_check_date'], $fld['last_package'], $fld['active_package_name'], $fld['package_expiration_date'], $fld['sedcard_url']
                );


                fputcsv($handle, $arr);
            }
        }


        fclose($handle);
        
        $cache_key = 'escindep_data_download_' . $bu_user->id;
        $cache = Zend_Registry::get('cache');
        if (!$cache->load($cache_key)) {
            $cache->save($filename, $cache_key, array(), 3600);
        }
//            header('Content-Type: text/csv');
//            header('Content-Disposition: attachment; filename=transfers.csv');
        
        exit;






//        if( $data['data'] ) {
//
//        } else {
//            header('Content-Type: text/csv');
//            header('Content-Disposition: attachment; filename=transfers.csv');
//
//            exit;
//        }




    }

    public function checkCsvStatusAction(){
        $this->view->layout()->disableLayout();

        $bu_user = Zend_Auth::getInstance()->getIdentity();
        $cache_key = 'escindep_data_download_' . $bu_user->id;
        $cache = Zend_Registry::get('cache');

        if ($cache->load($cache_key))
        {
            echo (json_encode(array('status' => true)));
            die;
        }
        echo (json_encode(array('status' => false)));
        die;
    }

    public function downloadCsvAction() {
        $this->view->layout()->disableLayout();

        $bu_user = Zend_Auth::getInstance()->getIdentity();
        $cache_key = 'escindep_data_download_' . $bu_user->id;
        $cache = Zend_Registry::get('cache');
        if($filename = $cache->load($cache_key)) {
            $cache->remove($cache_key);

            if (file_exists('escindependents/' . $filename)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="'.basename('escindependents/' . $filename).'"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize('escindependents/' . $filename));
                readfile('escindependents/' . $filename);

                unlink('escindependents/' . $filename);
            }
        }

        exit;
    }

    public function antiFraudAction(){
       
    }

    public function antiFraudHistoryAction(){
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		$logins = Model_Users::getLogins($req->id);
		$user = Model_Users::get($req->id);
		
		$login_arr = array();
		foreach ($logins as $key => $login) {
			$geo = Model_Geoip::getClientLocation($login->ip);

			$login_arr[] = array_merge((array)$login, $geo);
		}

    	$this->view->logins = $login_arr;
    	$this->view->user = $user ;


    }

    public function antiFraudDataAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;

		$bu_user = Zend_Auth::getInstance()->getIdentity();
		if ( $bu_user->type == 'sales manager' ) {
			$req->setParam('sales_user_id', $bu_user->id);
		}
		
		$filter = array(
			'username' => $req->username,
			'application_id' => $req->application_id,
			'email' => $req->email,
			'user_type' => $req->user_type,
			//'not_verified' => true,
			'status' => $req->status,
			'sales_user_id' => $req->sales_user_id 
		);

		$model = new Model_Users();
		$count = 0;
		$data = $model->get_all_for_antifraud(
			$this->_request->page,
			$this->_request->per_page,
			$filter,
			$this->_request->sort_field,
			$this->_request->sort_dir,
			$count
		);
	
		foreach($data as $item){

			if($item->user_type == 'escort'){
				$item->combined_id = $item->escort_id;
			}elseif($item->user_type == 'member'){
				$item->combined_id = $item->id;
			}elseif($item->user_type == 'agency'){
				$item->combined_id = $item->agency_id;
			}

			if($item->registered_ip){
				$geo1 = Model_Geoip::getClientLocation($item->registered_ip);
				$item->reg_ip_country = $geo1['country'];
				$item->reg_ip_country_iso = $geo1['country_iso'];
				$item->reg_ip_region = $geo1['region'];
				$item->reg_ip_city = $geo1['city'];
			}

			if($item->last_ip){
				$geo2 = Model_Geoip::getClientLocation($item->last_ip);
				
				$item->last_ip_country = $geo2['country'];
				$item->last_ip_country_iso = $geo2['country_iso'];
				$item->last_ip_region = $geo2['region'];
				$item->last_ip_city = $geo2['city'];
				
			}
		
		}
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

	public function commentsHistoryAction(){
        $this->view->layout()->disableLayout();
        $is_agency = false;
        $req = $this->_request;
        $id = isset($req->escort_id) ? $req->escort_id : NULL ;
        $page = isset($req->page) ? $req->page : 1;
        if( isset($req->agency_id) ){
            $id = $req->agency_id;
            $is_agency = true;
        }
        $per_page = 5;
        $this->view->escort_id = $id;
        $this->view->page = $page;
        $this->view->per_page = $per_page;
        $this->view->definitions= Zend_Registry::get('defines');

        if(Cubix_Application::getId() == APP_ED && $is_agency){
            $db = Zend_Registry::get('db');
            $agencyModel = new Model_Agencies();
            $agencyModel->UpdateAgencyEscorts($id,array('date_last_modified' => new Zend_Db_Expr('NOW()')));

            $db->update('agencies', array('last_modified' =>  new Zend_Db_Expr('NOW()')), 'id = '.$id);
        }
        if(Cubix_Application::getId() == APP_ED && !$is_agency){
            $this->view->comments_history = Model_Escort_CheckProfile::getCommentsHistoryByEscortId($id, $page, $per_page, $count);
        }
        else{
            $this->view->comments_history = Model_EscortsV2::getCommentsHistoryByEscortId($id,$is_agency, $page, $per_page, $count);
        }
        $this->view->count = $count;
        $back_user = Zend_Auth::getInstance()->getIdentity();
        $this->view->back_user_id = $back_user->id;
        $this->view->user_type = $this->user->type;
    }

    public function callcenterCommentsAction(){
        $this->view->layout()->disableLayout();
        $model = new Model_EscortsV2();
        $is_agency = false;
        $req = $this->_request;
        $id = isset($req->escort_id) ? $req->escort_id : NULL ;
        $page = isset($req->page) ? $req->page : 1;
        if( isset($req->agency_id) ){
            $id = $req->agency_id;
            $is_agency = true;
        }
        $per_page = 5;

		$m_city = new Cubix_Geography_Cities();

        $this->view->escort = $model->get($id);
		$home_city = $m_city->get($this->view->escort->city_id);
        $this->view->city_title = $home_city->title_en;
        $this->view->escort_id = $id;
        $this->view->page = $page;
        $this->view->per_page = $per_page;
        $this->view->definitions= Zend_Registry::get('defines');

        if(Cubix_Application::getId() == APP_ED && $is_agency){
            $db = Zend_Registry::get('db');
            $agencyModel = new Model_Agencies();
            $agencyModel->UpdateAgencyEscorts($id,array('date_last_modified' => new Zend_Db_Expr('NOW()')));

            $db->update('agencies', array('last_modified' =>  new Zend_Db_Expr('NOW()')), 'id = '.$id);
        }
        
        $this->view->comments_history = Model_Escort_CheckProfile::getResponseCommentsHistoryByEscortId($id, $page, $per_page, $count);
        $this->view->admin_comments_history = Model_Escort_CheckProfile::getCommentsHistoryByEscortId($id, $page, $per_page, $count);
        $this->view->count = $count;
        $back_user = Zend_Auth::getInstance()->getIdentity();
        $this->view->back_user_id = $back_user->id;
        $this->view->user_type = $this->user->type;
    }

    public function personalUrlsAction(){
       
		$req = $this->_request;

		$bu_user = Zend_Auth::getInstance()->getIdentity();

		// if ( in_array(Cubix_Application::getId(), array(APP_ED, APP_EG_CO_UK)) && $bu_user->type == 'call center' ) {
		// 	$req->setParam('sales_user_id', $bu_user->id);
		// }

		if ( $bu_user->type == 'sales manager' ) {
			//$req->setParam('sales_user_id', $bu_user->id);
		}
		else if ($bu_user->type == 'admin') {
			$req->setParam('application_id', $bu_user->application_id);
		}
		
		/*$phone = preg_replace('/^\+/', '00', $req->phone);*/
		$phone = preg_replace('/[^0-9]/', '', $req->phone);
		$phone = preg_replace('/^0/', '', $phone);

		if(in_array(Cubix_Application::getId(), array(APP_ED))){
           $filter_phone_prefix =  $req->filter_phone_prefix;
        }else{
            $filter_phone_prefix =  $req->phone_prefix;
        }

		$filter = array(
			// Personal
			'e.id = ?' => (int) $req->escort_id,
			/*'u.application_id = ?' => (int) $req->application_id,*/
			'e.showname' => $req->showname,
			'u.username' => $req->username,
			'ep.email' => $req->email,
			'ep.twitter_name' => $req->twitter_name,
			'phone_number' => $phone,
			'ep.phone_country_id = ?' => $filter_phone_prefix,
			'ep.phone_instr' => $req->phone_instr,
			'ep.phone_instr_no_withheld' => $req->phone_instr_no_withheld,
			'has_website' => $req->has_web,
			'website' => $req->website,
			'no_phone_number' => $req->no_phone_number,
			'has_phone_number' => $req->has_phone_number,
			'phone_sms_verified' => $req->phone_sms_verified,
			/*'u.last_ip' => $req->last_ip,*/
			'ul.ip = ?' => trim($req->last_ip),
			'ul.country_id' => $req->geo_country,

			
			'u.sales_user_id = ?' => (int) $req->sales_user_id,
			
			'e.gender = ?' => $req->gender,
			'e.home_city_id = ?' => (int) $req->home_city_id,
			
			'e.country_id = ?' => $req->country,
			'rg.id = ?' => (int) $req->region,
            /*Change Grigor
             * 'city_id' => $req->city,
             */
			'city' => $req->city,

			'city_zone' => $req->cityzone,

			'ep.zip = ?' => $req->zip,
			'time_zone' => $req->time_zone_id,
            'ep.nationality_id = ?' => $req->nationality_id,

			'ep.admin_verified' => $req->admin_verified,

			// Status
			'e.status' => $req->status,
		
			'u.disabled = ?' => $req->disabled,
			/*'ep.admin_verified = ?' => $req->verified,*/
		
			'e.verified_status = ?' => $req->only_verified ? Model_EscortsV2::STATUS_VERIFIED : null,
            'no_id_card' => $req->no_id_card,
			'only_premium' => $req->only_premium,
			'only_city_tour' => $req->only_city_tour,
		    'skype_call_status' => $req->skype_call_status,
			// Date
			'filter_by' => $req->filter_by,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to,
			'had_running_package' => $req->had_running_package,
			'seo_data' => $req->seo_data,
			'about_text' => $req->about_text,
			'about_text_lang' => $req->about_text_lang,
			
			'bubble_message' => $req->bubble_message,
			'slogan' => $req->slogan,

			'excl_status' => Model_EscortsV2::ESCORT_STATUS_DELETED,

			//Check date FOR ED
			'check_date_from' => $req->check_date_from,
			'check_date_to' => $req->check_date_to,
			'next_check_date_from' => $req->next_check_date_from,
			'next_check_date_to' => $req->next_check_date_to,
			'check_comment_text' => $req->check_comment_text,
			'ec.check_status = ?' => $req->check_status,
			'check_by_backend_user' => $req->check_by_backend_user,
			'last_check_by_backend_user' => $req->last_check_by_backend_user,
			'overdue_checked' =>  $req->overdue_checked,
			'overdue_pending' =>  $req->overdue_pending,
			
            //Other
            'active_package_id' => $req->active_package_id,
			'ag.name' =>$req->agency_name,
			'ag.id' =>$req->agency_id,
            'u.email' => $req->reg_email,
			'ever_login' => $req->ever_login,
			'about_me' => $req->about_me,
            'e.is_suspicious' => $req->is_suspicious,
            'e.is_vip' => $req->is_vip,
            //auto approve revision
			'e.auto_approval' => (int) $req->auto_approve_rev,

            'is_vip_request' => $req->is_vip_request,
            'is_vip_package' => $req->is_vip_package,

			'e.check_website' => $req->check_website,
			'e.block_website' => $req->block_website,
			'e.site_not_working' => $req->site_not_working,
			
			'show_small_photos' => $req->show_small_photos,
			'photo_type' => $req->photo_disabled_type,
			'escort_comment' => $req->escort_comment,
			'escort_reviews' => $req->escort_reviews,
			'escort_comment_by' =>(int)  $req->escort_comment_by,
			'last_comment_by' => $req->last_comment_by,
			'has_active_reviews' => $req->has_active_reviews,
			'is_pornstar' => $req->is_pornstar,
			'bad_photo_history' => $req->bad_photo_history,
			'gallery_checked' => $req->gallery_checked,
			'about_less' => $req->about_me_characters,
			'about_me_informed' => $req->about_me_informed,
			'admin_comment_text' => $req->admin_comment_text,
			'e.need_age_verification' =>(int) $req->age_verification_status,
			'has_video' => $req->has_video,
			'text_done' => $req->text_done,

			'has_hand_verification_date' => $req->has_hand_verification_date,
			'hand_verification_date_from' => $req->hand_verification_date_from,
			'hand_verification_date_to' => $req->hand_verification_date_to,
			
			'has_private_photos' => $req->has_private_photos,
			'profile_type' => $req->profile_type,
			'photoshop' => $req->photoshop,
			'has_expo'	=> $req->has_expo,
			'e.skype_call_status' => $req->has_skype_video,
			'show_popunder' => $req->show_popunder,
			'large_popunder' => $req->large_popunder,
			'has_not_approved_pics' => $req->has_not_approved_pics,
            'map_activated' => $req->map_activated,

		);

        if  ($req->not_verified == 1){
            $filter['e.verified_status = ?'] = $req->not_verified ? Model_EscortsV2::STATUS_NOT_VERIFIED : null;
        }

		if ( $req->not_check_website == 1 ) {
			$filter[] = 'LENGTH(ep.website) > 0 AND e.check_website = 0 AND e.block_website <> 1';
		}

		if( in_array(Cubix_Application::getId(), array(APP_ED)) ){
			if ( !empty($req->type) ) {
				$filter['e.type = ?'] = $req->type;
			}

            if ( !empty($req->data_entry_id) ) {
                $filter['e.data_entry_user = ?'] = $req->data_entry_id;
            }

            if (!empty($req->signed_in_once)){
                $filter['signed_in_once'] = $req->signed_in_once;
            }

            if (!empty($req->has_blacklisted_ips)){
                $filter['has_blacklisted_ips'] = $req->has_blacklisted_ips;
            }

            if ( $req->has_viber == 1 ) {
                $filter[] = '(ep.viber = 1 OR ep.viber_1 = 1  OR ep.viber_2 = 1 )';
            }

            if ( $req->search_in_all_cities == 1 ) {
                $filter['search_in_all_cities'] = 1;
            }

            if ( $req->has_whatsapp == 1 ) {
                $filter[] = '(ep.whatsapp = 1 OR ep.whatsapp_1 = 1  OR ep.whatsapp_2 = 1 )';
            }
            if ( $req->has_telegram == 1 ) {
                $filter[] = 'ep.telegram = 1';
            }
            if ( $req->has_signal == 1 ) {
                $filter[] = 'ep.ssignal = 1';
            }
            if ( $req->has_wechat == 1 ) {
                $filter[] = 'ep.wechat = 1';
            }
		}

		if(in_array(Cubix_Application::getId(), [APP_ED, APP_EG_CO_UK, APP_6B])) {
            if (isset($req->is_featured)){
                $filter['fg.is_featured = ?'] = $req->is_featured;
            }
        }
		if(in_array(Cubix_Application::getId(), [APP_A6])) {
            if (isset($req->premium_city) AND !empty($req->premium_city)){
                $filter['aa.area_id = ?'] = $req->premium_city;
            }
        }

		if( in_array(Cubix_Application::getId(), array(APP_6B, APP_AE, APP_BL, APP_EG_CO_UK, APP_ED)) ){
            if ( !empty($req->backend_user) ) {
                $filter['u.backend_user'] = $req->backend_user;
            }
        }

		if ( $req->has_email == 1 ) {
			$filter[] = 'ep.email IS NOT NULL';
		}
		
		if ( $req->is_vip == 1 ) {
			$filter[] = 'e.is_vip = 1';
		}		

		if($req->website_status == 1){
			 $filter[] = 'e.check_website = 1';
		}elseif($req->website_status == 2){
			 $filter[] = 'e.block_website = 1';
		}elseif($req->website_status == 3){
            $filter[] = 'e.check_website = 0';
        }

		if ( !empty($req->cam_status) ) {
			$filter['e.cam_status = ?'] = $req->cam_status;
		}
		
		if ($req->has_rotating_feature == 1)
			$filter[] = '( e.photo_rotate_type = '.Model_Escort_Photos::PHOTO_ROTATE_ALL.' OR e.photo_rotate_type = '.Model_Escort_Photos::PHOTO_ROTATE_SELECTED.')';  
		// Independent and Agency girl filtering
		$escort_type_filter = $this->_getParam('escort_type');
		if ( $escort_type_filter == 'independent' ) {
			$filter[] = 'e.agency_id IS NULL';
		}
		elseif ( $escort_type_filter == 'agency' ) {
			$filter[] = 'e.agency_id IS NOT NULL';
		}
        if ( Cubix_Application::getId() == APP_ED && isset($req->more_than_one_city) )
        {
            $filter[] = "(SELECT
                                COUNT( city_id ) 
                            FROM
                                escort_cities 
                            WHERE
                                escort_id = e.id) > 1";
        }
		//APP_EG_CO_UK, APP_EG, APP_AE
		//
		if(!in_array(Cubix_Application::getId(), array(APP_ED,APP_EG_CO_UK,APP_EG,APP_AE))){
			unset($filter['ag.id']);
		}else{
			$sourceUrlNotNull = false;
			
			if(!empty($filter['ag.id']) || $escort_type_filter == 'agency'){
				if ( $req->has_source_url == 1) {
					$sourceUrlNotNull = true;
				}
				if ( $req->has_source_url == 2 ) {
					$filter[] = 'e.source_url IS NULL';
				}

				if(!empty($filter['ag.id'])){
					if ( $req->has_works_source_url == 1 ) {
						$filter['work_source_url'] = 1;
						$sourceUrlNotNull = true;
					}
					if ( $req->has_works_source_url == 2 ) {
						$filter['wrong_source_url'] = 1;
						$sourceUrlNotNull = true;
					}
				}

				if($sourceUrlNotNull){
					$filter[] = 'e.source_url IS NOT NULL';
				}
			}
		}
		if(in_array(Cubix_Application::getId(), array(APP_ED))){
			if ( $req->source_url_doesnt_have == 1 ) {
				$filter[] = 'e.source_url_doesnt_have IS NOT NULL';
			}
			if ( $req->source_url_doesnt_have == 2 ) {
				$filter[] = 'e.source_url_doesnt_have IS  NULL';
			}
		}

		// Revisions filter fix
		if ( $this->_getParam('sort_field') == 'revs_count' ) {
			$this->_setParam('sort_field', 'COUNT(DISTINCT(pu.revision))');
		}
		
		//FIXING PACKAGES LOGIC
		//IF PACKAGE EXPIRES 19 APRIL IT'S ACTUALLY EXPIRES 18 APRIL 23:59
		if ( $filter['filter_by'] == 'package_expiration_date' ) {
			$filter['date_from'] += 60*60*24; 
			$filter['date_to'] += 60*60*24; 
		}
       
		$model = new Model_EscortsV2();
        $page = $this->_request->page;
        $per_page = $this->_request->per_page;
        $sort_field = $this->_request->sort_field;
        $sort_dir = $this->_request->sort_dir;

        if ( $this->_request->filteredExport == 1 )
        {
            $page = 1;
            $per_page = 5000;
            $sort_field = 'id';
            $sort_dir = 'desc';
        }

        $data = $model->getAll(
            $page,
            $per_page,
            $filter,
            $sort_field,
            $sort_dir,
            $count
        );

        if ( $this->_request->filteredExport == 1 )
        {
            $escorts_ids = array();
            foreach ( $data as $escort )
            {
                $escorts_ids[] = $escort->id;
            }
            if ( !empty($escorts_ids) ) {
                $this->exportIndependentAction( $escorts_ids );
            }
        }

//		var_dump($data);die;
		$DEFINITIONS = Zend_Registry::get('defines');
		//var_dump(Model_Escort_ListV2::ddd());
		foreach ($data as $d)
		{
			$stat = array();
			foreach( $DEFINITIONS['escort_status_options'] as $key => $e_status )
			{
				if ( $this->model->hasStatusBit($d['id'], $key) )
				{
				    if ($this->model->hasStatusBit($d['id'], ESCORT_STATUS_INACTIVE)
                        && $key == ESCORT_STATUS_ACTIVE
                        && in_array(Cubix_Application::getId(), array(APP_EG_CO_UK, APP_ED))
                    ){
				        continue;
                    }
					$stat[] = $e_status;
				}
			}

			$d['status'] = implode(', ', $stat);//$DEFINITIONS['escort_status_options'][$d['status']];
			$d['b_user_type'] = $this->user->type;
			if ( Cubix_Application::getId() == APP_BL){
				$d['b_username'] = $this->user->username;
			}
						
			// $d['main_photo'] = $d->getMainPhoto()->getUrl('agency_p100');
			
			
			//FIXING PACKAGES LOGIC
			//IF PACKAGE EXPIRES 19 APRIL IT'S ACTUALLY EXPIRES 18 APRIL 23:59
			if ( $d['expiration_date'] ) {
			    $format = "d M Y";
			    if (Cubix_Application::getId() == APP_BL){
			        $format = "d M Y H:i";
                }
				$d['expiration_date'] = date($format, strtotime('-1 day', strtotime($d['expiration_date']))); //-1 day
				//$d['expiration_date'] = date("d M Y", strtotime('-1 day', $d['expiration_date'])); 
			}
			if(Cubix_Application::getId() == APP_A6){
				if ($d['is_for_phone_billing']) {
					$d['expiration_date_phone'] = $model->getPhoneExpirationDate($d['order_package_id']);

				}
			}
			
			if ($req->gallery_view)
			{				
				$escort = $this->model->get($d['id']);
				
				$photos = array('public' => array(), 'private' => array(), 'disabled' => array() );
				$added = false;

				foreach ( $escort->getPhotos() as $photo ) {
					$added = true;
					if($phots->is_portrait==0 && !is_null($photo->args))
						$photo->rwidth = $this->GetPhotoWidth('200', $photo->width, $photo->height);
					 
					if ( $photo->type == ESCORT_PHOTO_TYPE_PRIVATE ) {
						$photos['private'][] = $photo;
					} elseif ( $photo->type == ESCORT_PHOTO_TYPE_DISABLED ) {
						$photos['disabled'][] = $photo;
					} else {
						$photos['public'][] = $photo;
					}
				}
				
				if ($added)
				{
					$this->view->data_photos = $photos;
					$html = $this->view->render('escorts-v2/data-photos.phtml');
					$d['photos_html'] = $html;
				}
				
			}
			if ($req->about_me_view)
			{
				$this->view->about_me_texts = $this->model->getAboutMeTexts($d['id']);
				$html = $this->view->render('escorts-v2/about-me-texts.phtml');
				$d['about_texts'] = $html;
			}		
			
			$c = $this->model->getLastComment($d['id']);
			
			if ($c)
				$d['comment'] = date('Y-m-d H:i:s', $c->date) . ' | Commented by: ' . $c->username . '<br/>' . $c->comment;
		}
		

		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	
    }

  public function needAttentionEscortsAction(){
        $this->view->layout()->disableLayout();
        $model = new Model_EscortsV2();
        $is_agency = false;
        $req = $this->_request;
        $id = isset($req->escort_id) ? $req->escort_id : NULL ;
        $page = isset($req->page) ? $req->page : 1;
        if( isset($req->agency_id) ){
            $id = $req->agency_id;
            $is_agency = true;
        }
        $per_page = 5;

        $m_city = new Cubix_Geography_Cities();

        $this->view->escort = $model->get($id);
        $home_city = $m_city->get($this->view->escort->city_id);
        $this->view->city_title = $home_city->title_en;
        $this->view->escort_id = $id;
        $this->view->page = $page;
        $this->view->per_page = $per_page;
        $this->view->definitions= Zend_Registry::get('defines');

        if(Cubix_Application::getId() == APP_ED && $is_agency){
            $db = Zend_Registry::get('db');
            $agencyModel = new Model_Agencies();
            $agencyModel->UpdateAgencyEscorts($id,array('date_last_modified' => new Zend_Db_Expr('NOW()')));

            $db->update('agencies', array('last_modified' =>  new Zend_Db_Expr('NOW()')), 'id = '.$id);
        }


        $back_user = Zend_Auth::getInstance()->getIdentity();
        $this->view->back_user_id = $back_user->id;
        $this->view->user_type = $this->user->type;
    }


    public function escorts7DaysAction(){
    	

    }
    public function escortsNeedAttentionAction(){

    }


   	public function personalUrlAction(){
    	

    }

   	public function callCenterAction()
	{
		$req = $this->_request;

		$bu_user = Zend_Auth::getInstance()->getIdentity();

		if ($bu_user->type == 'admin') {
			$req->setParam('application_id', $bu_user->application_id);
		}
		
		/*$phone = preg_replace('/^\+/', '00', $req->phone);*/
		$phone = preg_replace('/[^0-9]/', '', $req->phone);
		$phone = preg_replace('/^0/', '', $phone);

		if(in_array(Cubix_Application::getId(), array(APP_ED))){
           $filter_phone_prefix =  $req->filter_phone_prefix;
        }else{
            $filter_phone_prefix =  $req->phone_prefix;
        }

		$filter = array(
			// Personal
			'e.id = ?' => (int) $req->escort_id,
			/*'u.application_id = ?' => (int) $req->application_id,*/
			'e.showname' => $req->showname,
			'u.username' => $req->username,
			'ep.email' => $req->email,
			'ep.twitter_name' => $req->twitter_name,
			'phone_number' => $phone,
			'ep.phone_country_id = ?' => $filter_phone_prefix,
			'ep.phone_instr' => $req->phone_instr,
			'ep.phone_instr_no_withheld' => $req->phone_instr_no_withheld,
			'has_website' => $req->has_web,
			'website' => $req->website,
			'no_phone_number' => $req->no_phone_number,
			'has_phone_number' => $req->has_phone_number,
			'phone_sms_verified' => $req->phone_sms_verified,
			/*'u.last_ip' => $req->last_ip,*/
			'ul.ip = ?' => trim($req->last_ip),
			'ul.country_id' => $req->geo_country,

			
			'u.sales_user_id = ?' => (int) $req->sales_user_id,
			
			'e.gender = ?' => $req->gender,
			'e.home_city_id = ?' => (int) $req->home_city_id,
			
			'e.country_id = ?' => $req->country,
			'rg.id = ?' => (int) $req->region,
            /*Change Grigor
             * 'city_id' => $req->city,
             */
			'city' => $req->city,

			'city_zone' => $req->cityzone,

			'ep.zip = ?' => $req->zip,
			'time_zone' => $req->time_zone_id,
            'ep.nationality_id = ?' => $req->nationality_id,

			'ep.admin_verified' => $req->admin_verified,

			// Status
			'e.status' => $req->status,
		
			'u.disabled = ?' => $req->disabled,
			/*'ep.admin_verified = ?' => $req->verified,*/
		
			'e.verified_status = ?' => $req->only_verified ? Model_EscortsV2::STATUS_VERIFIED : null,
            'no_id_card' => $req->no_id_card,
			'only_premium' => $req->only_premium,
			'only_city_tour' => $req->only_city_tour,
		    'skype_call_status' => $req->skype_call_status,
			// Date
			'filter_by' => $req->filter_by,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to,
			'had_running_package' => $req->had_running_package,
			'seo_data' => $req->seo_data,
			'about_text' => $req->about_text,
			'about_text_lang' => $req->about_text_lang,
			
			'bubble_message' => $req->bubble_message,
			'slogan' => $req->slogan,

			'excl_status' => Model_EscortsV2::ESCORT_STATUS_DELETED,

			//Check date FOR ED
			'check_date_from' => $req->check_date_from,
			'check_date_to' => $req->check_date_to,
			'next_check_date_from' => $req->next_check_date_from,
			'next_check_date_to' => $req->next_check_date_to,
			'check_comment_text' => $req->check_comment_text,
			'ec.check_status = ?' => $req->check_status,
			'check_by_backend_user' => $req->check_by_backend_user,
			'last_check_by_backend_user' => $req->last_check_by_backend_user,
			'overdue_checked' =>  $req->overdue_checked,
			'overdue_pending' =>  $req->overdue_pending,
			
            //Other
            'active_package_id' => $req->active_package_id,
			'ag.name' =>$req->agency_name,
			'ag.id' =>$req->agency_id,
            'u.email' => $req->reg_email,
			'ever_login' => $req->ever_login,
			'about_me' => $req->about_me,
            'e.is_suspicious' => $req->is_suspicious,
            'e.is_vip' => $req->is_vip,
            //auto approve revision
			'e.auto_approval' => (int) $req->auto_approve_rev,

            'is_vip_request' => $req->is_vip_request,
            'is_vip_package' => $req->is_vip_package,

			'e.check_website' => $req->check_website,
			'e.block_website' => $req->block_website,
			'e.site_not_working' => $req->site_not_working,
			
			'show_small_photos' => $req->show_small_photos,
			'photo_type' => $req->photo_disabled_type,
			'escort_comment' => $req->escort_comment,
			'escort_reviews' => $req->escort_reviews,
			'escort_comment_by' =>(int)  $req->escort_comment_by,
			'last_comment_by' => $req->last_comment_by,
			'has_active_reviews' => $req->has_active_reviews,
			'is_pornstar' => $req->is_pornstar,
			'bad_photo_history' => $req->bad_photo_history,
			'gallery_checked' => $req->gallery_checked,
			'about_less' => $req->about_me_characters,
			'about_me_informed' => $req->about_me_informed,
			'admin_comment_text' => $req->admin_comment_text,
			'e.need_age_verification' =>(int) $req->age_verification_status,
			'has_video' => $req->has_video,
			'text_done' => $req->text_done,

			'has_hand_verification_date' => $req->has_hand_verification_date,
			'hand_verification_date_from' => $req->hand_verification_date_from,
			'hand_verification_date_to' => $req->hand_verification_date_to,
			
			'has_private_photos' => $req->has_private_photos,
			'profile_type' => $req->profile_type,
			'photoshop' => $req->photoshop,
			'has_expo'	=> $req->has_expo,
			'e.skype_call_status' => $req->has_skype_video,
			'show_popunder' => $req->show_popunder,
			'large_popunder' => $req->large_popunder,
			'has_not_approved_pics' => $req->has_not_approved_pics,
            'map_activated' => $req->map_activated,

		);

        if  ($req->not_verified == 1){
            $filter['e.verified_status = ?'] = $req->not_verified ? Model_EscortsV2::STATUS_NOT_VERIFIED : null;
        }

        if ( $req->top_prio == 1 ) {
            $filter['top_prio'] = $req->top_prio;
        }

		if ( $req->not_check_website == 1 ) {
			$filter[] = 'LENGTH(ep.website) > 0 AND e.check_website = 0 AND e.block_website <> 1';
		}

		if( in_array(Cubix_Application::getId(), array(APP_ED)) ){
			if ( !empty($req->type) ) {
				$filter['e.type = ?'] = $req->type;
			}

            if ( !empty($req->data_entry_id) ) {
                $filter['e.data_entry_user = ?'] = $req->data_entry_id;
            }

            if (!empty($req->signed_in_once)){
                $filter['signed_in_once'] = $req->signed_in_once;
            }

            if (!empty($req->has_blacklisted_ips)){
                $filter['has_blacklisted_ips'] = $req->has_blacklisted_ips;
            }

            if ( $req->has_viber == 1 ) {
                $filter[] = '(ep.viber = 1 OR ep.viber_1 = 1  OR ep.viber_2 = 1 )';
            }
            if ( $req->has_whatsapp == 1 ) {
                $filter[] = '(ep.whatsapp = 1 OR ep.whatsapp_1 = 1  OR ep.whatsapp_2 = 1 )';
            }
            if ( $req->has_telegram == 1 ) {
                $filter[] = 'ep.telegram = 1';
            }
            if ( $req->has_signal == 1 ) {
                $filter[] = 'ep.ssignal = 1';
            }
            if ( $req->has_wechat == 1 ) {
                $filter[] = 'ep.wechat = 1';
            }
		}

		if(in_array(Cubix_Application::getId(), [APP_ED, APP_EG_CO_UK, APP_6B])) {
            if (isset($req->is_featured)){
                $filter['fg.is_featured = ?'] = $req->is_featured;
            }
        }
		if(in_array(Cubix_Application::getId(), [APP_A6])) {
            if (isset($req->premium_city) AND !empty($req->premium_city)){
                $filter['aa.area_id = ?'] = $req->premium_city;
            }
        }

		if( in_array(Cubix_Application::getId(), array(APP_6B, APP_AE, APP_BL, APP_EG_CO_UK, APP_ED)) ){
            if ( !empty($req->backend_user) ) {
                $filter['u.backend_user'] = $req->backend_user;
            }
        }

		if ( $req->has_email == 1 ) {
			$filter[] = 'ep.email IS NOT NULL';
		}
		
		if ( $req->is_vip == 1 ) {
			$filter[] = 'e.is_vip = 1';
		}		
		
		if ( !empty($req->cam_status) ) {
			$filter['e.cam_status = ?'] = $req->cam_status;
		}
		
		if ($req->has_rotating_feature == 1)
			$filter[] = '( e.photo_rotate_type = '.Model_Escort_Photos::PHOTO_ROTATE_ALL.' OR e.photo_rotate_type = '.Model_Escort_Photos::PHOTO_ROTATE_SELECTED.')';  
		// Independent and Agency girl filtering
		$escort_type_filter = $this->_getParam('escort_type');
		if ( $escort_type_filter == 'independent' ) {
			$filter[] = 'e.agency_id IS NULL';
		}
		elseif ( $escort_type_filter == 'agency' ) {
			$filter[] = 'e.agency_id IS NOT NULL';
		}
        if ( Cubix_Application::getId() == APP_ED && isset($req->more_than_one_city) )
        {
            $filter[] = "(SELECT
                                COUNT( city_id ) 
                            FROM
                                escort_cities 
                            WHERE
                                escort_id = e.id) > 1";
        }
		//APP_EG_CO_UK, APP_EG, APP_AE
		if(!in_array(Cubix_Application::getId(), array(APP_ED,APP_EG_CO_UK,APP_EG,APP_AE))){
			unset($filter['ag.id']);
		}else{
			$sourceUrlNotNull = false;
			
			if(!empty($filter['ag.id']) || $escort_type_filter == 'agency'){
				if ( $req->has_source_url == 1) {
					$sourceUrlNotNull = true;
				}
				if ( $req->has_source_url == 2 ) {
					$filter[] = 'e.source_url IS NULL';
				}

				if(!empty($filter['ag.id'])){
					if ( $req->has_works_source_url == 1 ) {
						$filter['work_source_url'] = 1;
						$sourceUrlNotNull = true;
					}
					if ( $req->has_works_source_url == 2 ) {
						$filter['wrong_source_url'] = 1;
						$sourceUrlNotNull = true;
					}
				}

				if($sourceUrlNotNull){
					$filter[] = 'e.source_url IS NOT NULL';
				}
			}
		}
		if(in_array(Cubix_Application::getId(), array(APP_ED))){
			if ( $req->source_url_doesnt_have == 1 ) {
				$filter[] = 'e.source_url_doesnt_have IS NOT NULL';
			}
			if ( $req->source_url_doesnt_have == 2 ) {
				$filter[] = 'e.source_url_doesnt_have IS  NULL';
			}
            if ( $req->received_5_sms == 1 ) {
                $filter[] = 'ec.comment LIKE "%auto sms and mail for confirmation%"';
            }
		}

		// Revisions filter fix
		if ( $this->_getParam('sort_field') == 'revs_count' ) {
			$this->_setParam('sort_field', 'COUNT(DISTINCT(pu.revision))');
		}
		
		//FIXING PACKAGES LOGIC
		//IF PACKAGE EXPIRES 19 APRIL IT'S ACTUALLY EXPIRES 18 APRIL 23:59
		if ( $filter['filter_by'] == 'last_contact_date' ) {
			$filter[] = 'er.date > DATE(FROM_UNIXTIME("'.$req->date_from.'"))'; 
			$filter[] = 'er.date < DATE(FROM_UNIXTIME("'.$req->date_to.'"))'; 
		}

        $having = null;
        $nCallTimes = intval($this->_getParam('call_count'));
        if ($nCallTimes > 5) {
            $having = 'call_count > 5';
        } else {
            $having = "call_count = $nCallTimes";
        }
       	
       	$filter[] = 'e.agency_id IS NULL'; 
       	$filter[] = 'e.last_hand_verification_date IS NULL'; 
       	$filter[] = 'e.status & 32';
       	$filter[] = "ep.phone_number != ''";
       	$filter[] = 'u.date_registered < DATE(NOW()) - INTERVAL 7 DAY'; 
		
		$model = new Model_EscortsV2();
        $page = $this->_request->page;
        $per_page = $this->_request->per_page;
        $sort_field = $this->_request->sort_field;
        $sort_dir = $this->_request->sort_dir;

        if ( $this->_request->filteredExport == 1 )
        {
            $page = 1;
            $per_page = 5000;
            $sort_field = 'id';
            $sort_dir = 'desc';
        }

        $data = $model->getCallCenter7days(
            $page,
            $per_page,
            $filter,
            $sort_field,
            $sort_dir,
            $count,
            $having
        );

        if ( $this->_request->filteredExport == 1 )
        {
            $escorts_ids = array();
            foreach ( $data as $escort )
            {
                $escorts_ids[] = $escort->id;
            }
            if ( !empty($escorts_ids) ) {
                $this->exportIndependentAction( $escorts_ids );
            }
        }

		$DEFINITIONS = Zend_Registry::get('defines');

		foreach ($data as $d)
		{
			$stat = array();
			foreach( $DEFINITIONS['escort_status_options'] as $key => $e_status )
			{
				if ( $this->model->hasStatusBit($d['id'], $key) )
				{
				    if ($this->model->hasStatusBit($d['id'], ESCORT_STATUS_INACTIVE)
                        && $key == ESCORT_STATUS_ACTIVE
                        && in_array(Cubix_Application::getId(), array(APP_EG_CO_UK, APP_ED))
                    ){
				        continue;
                    }
					$stat[] = $e_status;
				}
			}

			$d['status'] = implode(', ', $stat);//$DEFINITIONS['escort_status_options'][$d['status']];
			$d['b_user_type'] = $this->user->type;
			if ( Cubix_Application::getId() == APP_BL){
				$d['b_username'] = $this->user->username;
			}
						
			// $d['main_photo'] = $d->getMainPhoto()->getUrl('agency_p100');
			
			
			//FIXING PACKAGES LOGIC
			//IF PACKAGE EXPIRES 19 APRIL IT'S ACTUALLY EXPIRES 18 APRIL 23:59
			if ( $d['expiration_date'] ) {
			    $format = "d M Y";
			    if (Cubix_Application::getId() == APP_BL){
			        $format = "d M Y H:i";
                }
				$d['expiration_date'] = date($format, strtotime('-1 day', strtotime($d['expiration_date']))); //-1 day
				//$d['expiration_date'] = date("d M Y", strtotime('-1 day', $d['expiration_date'])); 
			}
			if(Cubix_Application::getId() == APP_A6){
				if ($d['is_for_phone_billing']) {
					$d['expiration_date_phone'] = $model->getPhoneExpirationDate($d['order_package_id']);

				}
			}
			
			if ($req->gallery_view)
			{				
				$escort = $this->model->get($d['id']);
				
				$photos = array('public' => array(), 'private' => array(), 'disabled' => array() );
				$added = false;

				foreach ( $escort->getPhotos() as $photo ) {
					$added = true;
					if($phots->is_portrait==0 && !is_null($photo->args))
						$photo->rwidth = $this->GetPhotoWidth('200', $photo->width, $photo->height);
					 
					if ( $photo->type == ESCORT_PHOTO_TYPE_PRIVATE ) {
						$photos['private'][] = $photo;
					} elseif ( $photo->type == ESCORT_PHOTO_TYPE_DISABLED ) {
						$photos['disabled'][] = $photo;
					} else {
						$photos['public'][] = $photo;
					}
				}
				
				if ($added)
				{
					$this->view->data_photos = $photos;
					$html = $this->view->render('escorts-v2/data-photos.phtml');
					$d['photos_html'] = $html;
				}
				
			}
			if ($req->about_me_view)
			{
				$this->view->about_me_texts = $this->model->getAboutMeTexts($d['id']);
				$html = $this->view->render('escorts-v2/about-me-texts.phtml');
				$d['about_texts'] = $html;
			}		
			
			$c = $this->model->getLastComment($d['id']);
			
			if ($c)
				$d['comment'] = date('Y-m-d H:i:s', $c->date) . ' | Commented by: ' . $c->username . '<br/>' . $c->comment;
		}
		

		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
	public function needAttentionAction()
    {
        $req = $this->_request;

        $bu_user = Zend_Auth::getInstance()->getIdentity();

        if ($bu_user->type == 'admin') {
            $req->setParam('application_id', $bu_user->application_id);
        }

        /*$phone = preg_replace('/^\+/', '00', $req->phone);*/
        $phone = preg_replace('/[^0-9]/', '', $req->phone);
        $phone = preg_replace('/^0/', '', $phone);

        if(in_array(Cubix_Application::getId(), array(APP_ED))){
            $filter_phone_prefix =  $req->filter_phone_prefix;
        }else{
            $filter_phone_prefix =  $req->phone_prefix;
        }

        $filter = array(
            // Personal
            'e.id = ?' => (int) $req->escort_id,
            /*'u.application_id = ?' => (int) $req->application_id,*/
            'e.showname' => $req->showname,
            'u.username' => $req->username,
            'ep.email' => $req->email,
            'ep.twitter_name' => $req->twitter_name,
            'phone_number' => $phone,
            'ep.phone_country_id = ?' => $filter_phone_prefix,
            'ep.phone_instr' => $req->phone_instr,
            'ep.phone_instr_no_withheld' => $req->phone_instr_no_withheld,
            'has_website' => $req->has_web,
            'website' => $req->website,
            'no_phone_number' => $req->no_phone_number,
            'has_phone_number' => $req->has_phone_number,
            'phone_sms_verified' => $req->phone_sms_verified,
            /*'u.last_ip' => $req->last_ip,*/
            'ul.ip = ?' => trim($req->last_ip),
            'ul.country_id' => $req->geo_country,


            'u.sales_user_id = ?' => (int) $req->sales_user_id,

            'e.gender = ?' => $req->gender,
            'e.home_city_id = ?' => (int) $req->home_city_id,

            'e.country_id = ?' => $req->country,
            'rg.id = ?' => (int) $req->region,
            /*Change Grigor
             * 'city_id' => $req->city,
             */
            'city' => $req->city,

            'city_zone' => $req->cityzone,

            'ep.zip = ?' => $req->zip,
            'time_zone' => $req->time_zone_id,
            'ep.nationality_id = ?' => $req->nationality_id,

            'ep.admin_verified' => $req->admin_verified,

            // Status
            'e.status' => $req->status,

            'u.disabled = ?' => $req->disabled,
            /*'ep.admin_verified = ?' => $req->verified,*/

            'e.verified_status = ?' => $req->only_verified ? Model_EscortsV2::STATUS_VERIFIED : null,
            'no_id_card' => $req->no_id_card,
            'only_premium' => $req->only_premium,
            'only_city_tour' => $req->only_city_tour,
            'skype_call_status' => $req->skype_call_status,
            // Date
            'filter_by' => $req->filter_by,
            'date_from' => $req->date_from,
            'date_to' => $req->date_to,
            'had_running_package' => $req->had_running_package,
            'seo_data' => $req->seo_data,
            'about_text' => $req->about_text,
            'about_text_lang' => $req->about_text_lang,

            'bubble_message' => $req->bubble_message,
            'slogan' => $req->slogan,

            'excl_status' => Model_EscortsV2::ESCORT_STATUS_DELETED,

            //Check date FOR ED
            'check_date_from' => $req->check_date_from,
            'check_date_to' => $req->check_date_to,
            'next_check_date_from' => $req->next_check_date_from,
            'next_check_date_to' => $req->next_check_date_to,
            'check_comment_text' => $req->check_comment_text,
            'ec.check_status = ?' => $req->check_status,
            'check_by_backend_user' => $req->check_by_backend_user,
            'last_check_by_backend_user' => $req->last_check_by_backend_user,
            'overdue_checked' =>  $req->overdue_checked,
            'overdue_pending' =>  $req->overdue_pending,

            //Other
            'active_package_id' => $req->active_package_id,
            'ag.name' =>$req->agency_name,
            'ag.id' =>$req->agency_id,
            'u.email' => $req->reg_email,
            'ever_login' => $req->ever_login,
            'about_me' => $req->about_me,
            'e.is_suspicious' => $req->is_suspicious,
            'e.is_vip' => $req->is_vip,
            //auto approve revision
            'e.auto_approval' => (int) $req->auto_approve_rev,

            'is_vip_request' => $req->is_vip_request,
            'is_vip_package' => $req->is_vip_package,

            'e.check_website' => $req->check_website,
            'e.block_website' => $req->block_website,
            'e.site_not_working' => $req->site_not_working,

            'show_small_photos' => $req->show_small_photos,
            'photo_type' => $req->photo_disabled_type,
            'escort_comment' => $req->escort_comment,
            'escort_reviews' => $req->escort_reviews,
            'escort_comment_by' =>(int)  $req->escort_comment_by,
            'last_comment_by' => $req->last_comment_by,
            'has_active_reviews' => $req->has_active_reviews,
            'is_pornstar' => $req->is_pornstar,
            'bad_photo_history' => $req->bad_photo_history,
            'gallery_checked' => $req->gallery_checked,
            'about_less' => $req->about_me_characters,
            'about_me_informed' => $req->about_me_informed,
            'admin_comment_text' => $req->admin_comment_text,
            'e.need_age_verification' =>(int) $req->age_verification_status,
            'has_video' => $req->has_video,
            'text_done' => $req->text_done,

            'has_hand_verification_date' => $req->has_hand_verification_date,
            'hand_verification_date_from' => $req->hand_verification_date_from,
            'hand_verification_date_to' => $req->hand_verification_date_to,

            'has_private_photos' => $req->has_private_photos,
            'profile_type' => $req->profile_type,
            'photoshop' => $req->photoshop,
            'has_expo'	=> $req->has_expo,
            'e.skype_call_status' => $req->has_skype_video,
            'show_popunder' => $req->show_popunder,
            'large_popunder' => $req->large_popunder,
            'has_not_approved_pics' => $req->has_not_approved_pics,
            'map_activated' => $req->map_activated,

        );

        if  ($req->not_verified == 1){
            $filter['e.verified_status = ?'] = $req->not_verified ? Model_EscortsV2::STATUS_NOT_VERIFIED : null;
        }

        if ( $req->not_check_website == 1 ) {
            $filter[] = 'LENGTH(ep.website) > 0 AND e.check_website = 0 AND e.block_website <> 1';
        }

        if( in_array(Cubix_Application::getId(), array(APP_ED)) ){
            if ( !empty($req->type) ) {
                $filter['e.type = ?'] = $req->type;
            }

            if ( !empty($req->data_entry_id) ) {
                $filter['e.data_entry_user = ?'] = $req->data_entry_id;
            }

            if (!empty($req->signed_in_once)){
                $filter['signed_in_once'] = $req->signed_in_once;
            }

            if (!empty($req->has_blacklisted_ips)){
                $filter['has_blacklisted_ips'] = $req->has_blacklisted_ips;
            }

            if ( $req->has_viber == 1 ) {
                $filter[] = '(ep.viber = 1 OR ep.viber_1 = 1  OR ep.viber_2 = 1 )';
            }
            if ( $req->has_whatsapp == 1 ) {
                $filter[] = '(ep.whatsapp = 1 OR ep.whatsapp_1 = 1  OR ep.whatsapp_2 = 1 )';
            }
            if ( $req->has_telegram == 1 ) {
                $filter[] = 'ep.telegram = 1';
            }
            if ( $req->has_signal == 1 ) {
                $filter[] = 'ep.ssignal = 1';
            }
            if ( $req->has_wechat == 1 ) {
                $filter[] = 'ep.wechat = 1';
            }
        }

        if(in_array(Cubix_Application::getId(), [APP_ED, APP_EG_CO_UK, APP_6B])) {
            if (isset($req->is_featured)){
                $filter['fg.is_featured = ?'] = $req->is_featured;
            }
        }
        if(in_array(Cubix_Application::getId(), [APP_A6])) {
            if (isset($req->premium_city) AND !empty($req->premium_city)){
                $filter['aa.area_id = ?'] = $req->premium_city;
            }
        }

        if( in_array(Cubix_Application::getId(), array(APP_6B, APP_AE, APP_BL, APP_EG_CO_UK, APP_ED)) ){
            if ( !empty($req->backend_user) ) {
                $filter['u.backend_user'] = $req->backend_user;
            }
        }

        if ( $req->has_email == 1 ) {
            $filter[] = 'ep.email IS NOT NULL';
        }

        if ( $req->is_vip == 1 ) {
            $filter[] = 'e.is_vip = 1';
        }

        if ( !empty($req->cam_status) ) {
            $filter['e.cam_status = ?'] = $req->cam_status;
        }

        if ($req->has_rotating_feature == 1)
            $filter[] = '( e.photo_rotate_type = '.Model_Escort_Photos::PHOTO_ROTATE_ALL.' OR e.photo_rotate_type = '.Model_Escort_Photos::PHOTO_ROTATE_SELECTED.')';
        // Independent and Agency girl filtering
        $escort_type_filter = $this->_getParam('escort_type');
        if ( $escort_type_filter == 'independent' ) {
            $filter[] = 'e.agency_id IS NULL';
        }
        elseif ( $escort_type_filter == 'agency' ) {
            $filter[] = 'e.agency_id IS NOT NULL';
        }
        if ( Cubix_Application::getId() == APP_ED && isset($req->more_than_one_city) )
        {
            $filter[] = "(SELECT
                                COUNT( city_id ) 
                            FROM
                                escort_cities 
                            WHERE
                                escort_id = e.id) > 1";
        }
        //APP_EG_CO_UK, APP_EG, APP_AE
        //
        if(!in_array(Cubix_Application::getId(), array(APP_ED,APP_EG_CO_UK,APP_EG,APP_AE))){
            unset($filter['ag.id']);
        }else{
            $sourceUrlNotNull = false;

            if(!empty($filter['ag.id']) || $escort_type_filter == 'agency'){
                if ( $req->has_source_url == 1) {
                    $sourceUrlNotNull = true;
                }
                if ( $req->has_source_url == 2 ) {
                    $filter[] = 'e.source_url IS NULL';
                }

                if(!empty($filter['ag.id'])){
                    if ( $req->has_works_source_url == 1 ) {
                        $filter['work_source_url'] = 1;
                        $sourceUrlNotNull = true;
                    }
                    if ( $req->has_works_source_url == 2 ) {
                        $filter['wrong_source_url'] = 1;
                        $sourceUrlNotNull = true;
                    }
                }

                if($sourceUrlNotNull){
                    $filter[] = 'e.source_url IS NOT NULL';
                }
            }
        }
        if(in_array(Cubix_Application::getId(), array(APP_ED))){
            if ( $req->source_url_doesnt_have == 1 ) {
                $filter[] = 'e.source_url_doesnt_have IS NOT NULL';
            }
            if ( $req->source_url_doesnt_have == 2 ) {
                $filter[] = 'e.source_url_doesnt_have IS  NULL';
            }
        }

        // Revisions filter fix
        if ( $this->_getParam('sort_field') == 'revs_count' ) {
            $this->_setParam('sort_field', 'COUNT(DISTINCT(pu.revision))');
        }


        $filter[] = 'e.status & 32';

        $model = new Model_EscortsV2();
        $page = $this->_request->page;
        $per_page = $this->_request->per_page;
        $sort_field = $this->_request->sort_field;
        $sort_dir = $this->_request->sort_dir;

        if ( $this->_request->filteredExport == 1 )
        {
            $page = 1;
            $per_page = 5000;
            $sort_field = 'id';
            $sort_dir = 'desc';
        }

        $data = $model->getNeedAttentionEscorts(
            $page,
            $per_page,
            $filter,
            $sort_field,
            $sort_dir,
            $count
        );

        if ( $this->_request->filteredExport == 1 )
        {
            $escorts_ids = array();
            foreach ( $data as $escort )
            {
                $escorts_ids[] = $escort->id;
            }
            if ( !empty($escorts_ids) ) {
                $this->exportIndependentAction( $escorts_ids );
            }
        }

        $DEFINITIONS = Zend_Registry::get('defines');

        foreach ($data as $d)
        {
            $stat = array();
            foreach( $DEFINITIONS['escort_status_options'] as $key => $e_status )
            {
                if ( $this->model->hasStatusBit($d['id'], $key) )
                {
                    if ($this->model->hasStatusBit($d['id'], ESCORT_STATUS_INACTIVE)
                        && $key == ESCORT_STATUS_ACTIVE
                        && in_array(Cubix_Application::getId(), array(APP_EG_CO_UK, APP_ED))
                    ){
                        continue;
                    }
                    $stat[] = $e_status;
                }
            }

            $d['status'] = implode(', ', $stat);//$DEFINITIONS['escort_status_options'][$d['status']];
            $d['b_user_type'] = $this->user->type;
            if ( Cubix_Application::getId() == APP_BL){
                $d['b_username'] = $this->user->username;
            }

            // $d['main_photo'] = $d->getMainPhoto()->getUrl('agency_p100');


            //FIXING PACKAGES LOGIC
            //IF PACKAGE EXPIRES 19 APRIL IT'S ACTUALLY EXPIRES 18 APRIL 23:59
            if ( $d['expiration_date'] ) {
                $format = "d M Y";
                if (Cubix_Application::getId() == APP_BL){
                    $format = "d M Y H:i";
                }
                $d['expiration_date'] = date($format, strtotime('-1 day', strtotime($d['expiration_date']))); //-1 day
                //$d['expiration_date'] = date("d M Y", strtotime('-1 day', $d['expiration_date']));
            }
            if(Cubix_Application::getId() == APP_A6){
                if ($d['is_for_phone_billing']) {
                    $d['expiration_date_phone'] = $model->getPhoneExpirationDate($d['order_package_id']);

                }
            }

            if ($req->gallery_view)
            {
                $escort = $this->model->get($d['id']);

                $photos = array('public' => array(), 'private' => array(), 'disabled' => array() );
                $added = false;

                foreach ( $escort->getPhotos() as $photo ) {
                    $added = true;
                    if($phots->is_portrait==0 && !is_null($photo->args))
                        $photo->rwidth = $this->GetPhotoWidth('200', $photo->width, $photo->height);

                    if ( $photo->type == ESCORT_PHOTO_TYPE_PRIVATE ) {
                        $photos['private'][] = $photo;
                    } elseif ( $photo->type == ESCORT_PHOTO_TYPE_DISABLED ) {
                        $photos['disabled'][] = $photo;
                    } else {
                        $photos['public'][] = $photo;
                    }
                }

                if ($added)
                {
                    $this->view->data_photos = $photos;
                    $html = $this->view->render('escorts-v2/data-photos.phtml');
                    $d['photos_html'] = $html;
                }

            }
            if ($req->about_me_view)
            {
                $this->view->about_me_texts = $this->model->getAboutMeTexts($d['id']);
                $html = $this->view->render('escorts-v2/about-me-texts.phtml');
                $d['about_texts'] = $html;
            }

            $c = $this->model->getLastComment($d['id']);

            if ($c)
                $d['comment'] = date('Y-m-d H:i:s', $c->date) . ' | Commented by: ' . $c->username . '<br/>' . $c->comment;
        }


        $this->view->data = array(
            'data' => $data,
            'count' => $count
        );
    }

    public function fullUpdateAction()
    {
        $this->view->layout()->disableLayout();
        $ids = $this->_request->id;
        if ( count($ids) ) {
            foreach ($ids as $id) {
                $this->model->updateHandVerificationDate($id);
                $this->model->updateLastModifiedDate($id);
                Cubix_SyncNotifier::notify($id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array());
            }
        }
        die;
    }

    public function getLastExportDateAction()
    {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        die(json_encode( $this->model->getLastExportDate()));
    }

    public function watermark_image($target, $wtrmrk_file, $newcopy) {
        $watermark = imagecreatefrompng($wtrmrk_file);
        imagealphablending($watermark, false);
        imagesavealpha($watermark, true);
        $img = imagecreatefromjpeg($target);
        $img_w = imagesx($img);
        $img_h = imagesy($img);
        $wtrmrk_w = imagesx($watermark);
        $wtrmrk_h = imagesy($watermark);
        $dst_x = ($img_w / 1.9) - ($wtrmrk_w / 1.9); // For centering the watermark on any image
        $dst_y = ($img_h / 1.5) - ($wtrmrk_h / 1.8); // For centering the watermark on any image
        imagecopy($img, $watermark, $dst_x, $dst_y, 0, 0, $wtrmrk_w, $wtrmrk_h);
        imagejpeg($img, $newcopy, 100);
        imagedestroy($img);
        imagedestroy($watermark);
    }

    public function syncAction()
    {
        $app_id = Cubix_Application::getId();

        $app = Cubix_Application::getById($app_id);
        $host = $app->host;
        $protocol = 'http';

        $username = 'babazeo';
        $password = 'GnaMer1a';

        if ( APPLICATION_ENV == 'development' ) {
            $protocol = 'http';
            $host .= '.test';
            $username = 'zuzuba';
            $password = 'iqibir+';
        }

        $url = $protocol.'://www.' . $host;
        $sync_url = $url . '/api/sync/manual-diff';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $sync_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        $result = curl_exec($ch);

        if (curl_errno($ch)) {
            $error_msg = curl_error($ch);
            die(json_encode(array('status' => 'error', 'message' => $error_msg)));
        }

        curl_close($ch);

        die(json_encode(array('status' => 'success', 'message' => 'Escorts successfully synced', 'result' => $result)));

//        $headers = array(
//            'Content-Type:application/json',
//            'Authorization: Basic '. base64_encode("user:password") // <---
//        );
//
//        $ch = curl_init($sync_url);
//        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/xml', $headers));
//        curl_setopt($ch, CURLOPT_HEADER, 1);
//        curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
//        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
//        curl_setopt($ch, CURLOPT_POST, 1);
////        curl_setopt($ch, CURLOPT_POSTFIELDS, $payloadName);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
//        $return = curl_exec($ch);
//        curl_close($ch);

//        $curl = curl_init($sync_url);
//        curl_setopt($curl, CURLOPT_URL, $url);
//        curl_setopt($curl, CURLOPT_POST, true);
//        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//
//        $headers = array(
//            "Content-Type: application/json",
//            "Authorization: Basic bG9naW46cGFzc3dvcmQ=",
//        );
//        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
//
//        $data = json_encode(['login' => $username, 'password' => $password]);
//
//        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
//
////for debug only!
//        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
//        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
//
//        $resp = curl_exec($curl);
//        curl_close($curl);
//        die(json_encode(['test' => $resp]));



        /*
        // try {
            $client = new Zend_Http_Client($sync_url, array(
                'maxredirects' => 3,
                'timeout' => 60,
                'adapter'   => 'Zend_Http_Client_Adapter_Test',

            ));

            $client->setAuth($username, $password, Zend_Http_Client::AUTH_BASIC);
           
            $response = $client->request(Zend_Http_Client::GET);


            if ( $response->isError() ) {
                die(json_encode(array('status' => 'error', 'message' => $response->getMessage())));
            }

            header('Cubix-message: ' . $url . ' has been successfully synchronized');
            die(json_encode(array('status' => 'success', 'message' => 'Escorts successfully synced', 'resp' => $response->getMessage(), 'sync_url' => $sync_url)));
        
        //  }catch (\Exception $exception) {
        //     die(json_encode(array('status' => 'error', 'message' => $exception->getMessage(), 'sync_url' => $sync_url)));
        // }*/
    }
}
