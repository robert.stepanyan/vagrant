<?php

class BlacklistedWordsController extends Zend_Controller_Action
{
	protected $model;

	public function init()
	{
		$this->model = new Model_BlacklistedWords();
	}

	public function indexAction()
	{

	}

	public function dataAction()
	{
		$req = $this->_request;

		$filter = array('name' => $req->name,'type' => $req->bl_type );
       
		$data = $this->model->getAll(
			$req->page,
			$req->per_page,
			$filter,
			$req->sort_field,
			$req->sort_dir,
			$count
		);

		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

	public function deleteAction()
	{
        if($this->_request->isPost()){
            $ids = $this->_request->getParam('id');
            if(count($ids) > 0 && $ids){
                foreach( $ids as $id ) {
                    $this->model->delete($id);
                }
            }
        }
        exit;
	}

    public function createAction(){
		
        $this->view->layout()->disableLayout();

        //$model = new Model_BlacklistedWords();
		$country_model = new Model_Countries();
		$this->view->countries = $country_model->getCountries();
		
        if($this->_request->isPost()){
            $data['name'] = trim($this->_request->getParam('name'));
			$data['search_type'] = $this->_request->getParam('search_type');
			$data['types'] = 0;

			$bl_types =  $this->_request->getParam('bl_types');
			$bl_countries =  $this->_request->getParam('bl_countries');
			

			$validator = new Cubix_Validator();
			

			if ( ! strlen($data['name']) ) {
				$validator->setError('name', 'Name is required');
			}
			elseif($this->model->exists($data['name'],$data['search_type'])){
				$validator->setError('name', 'Bl. word already exist');
			}
			
			if (!isset($bl_types)){
				$validator->setError('bl_type', 'Bl. type is required');
			}
			
			if ( $validator->isValid() ) {
				foreach($bl_types as $bl_type){
					$data['types'] += $bl_type;
				}

				$this->model->save($data,null, $bl_countries);

				if ( in_array(Cubix_Application::getId(), array(APP_A6, APP_EF))  ) {
					ignore_user_abort(true);
					ob_start();
					echo (json_encode(array('status' => 'success', 'a'=>'a' )));
					
					// usually it needs to be closed in this case it load forever in our server
					// header('Connection', 'close');
					// header('Content-Length: '.ob_get_length()); 
					  
					ob_end_flush();
					ob_flush();
					flush();

					session_write_close();
					fastcgi_finish_request();
				}

				$this->model->replaceOldBlWords( $data['name'], $data['search_type'], $bl_types, $bl_countries );
			}
			
            die(json_encode($validator->getStatus()));
        }        
    }

    public function editAction(){
		
        $this->view->layout()->disableLayout();
        $country_model = new Model_Countries();
		$this->view->countries = $country_model->getCountries();
		
        //$model = new Model_BlacklistedWords();

        $id = $this->_request->getParam('id');
        $this->view->blockedWork = $blockedWork = $this->model->get($id);
		$this->view->bl_types = $this->model->fromBitToArray($blockedWork->types);
		if(Cubix_Application::getId() == APP_ED) {
			$this->view->bl_countries = $this->model->get_bl_words_countries($id);
		}else{
			$this->view->bl_countries = '';
		}
		

		$bl_countries =  $this->_request->getParam('bl_countries');

		
        if($this->_request->isPost()){
            $data['name'] = trim($this->_request->getParam('name'));
			$data['types'] = 0;
			$bl_types =  $this->_request->getParam('bl_types');
			$data['search_type'] = $this->_request->getParam('search_type');
            
            $validator = new Cubix_Validator();
						
			if ( ! strlen($data['name']) ) {
				$validator->setError('name', 'Name is required');
			}
			elseif($this->model->exists( $data['name'], $data['search_type'],$id)){
				$validator->setError('name', 'Bl. word already exist');
			}
			if (!isset($bl_types)){
				$validator->setError('bl_type', 'Bl. type is required');
			}
			if ( $validator->isValid() ) {
				foreach($bl_types as $bl_type){
					$data['types'] += $bl_type;
				}
				$this->model->save($data, $id, $bl_countries);

				if ( in_array(Cubix_Application::getId(), array(APP_A6))  ) {
					ignore_user_abort(true);
					ob_start();
					echo (json_encode(array('status' => 'success', 'a'=>'a' )));
					
					// usually it needs to be closed in this case it load forever in our server
					// header('Connection', 'close');
					// header('Content-Length: '.ob_get_length()); 
					  
					ob_end_flush();
					ob_flush();
					flush();

					session_write_close();
					fastcgi_finish_request();
				}
				
				$this->model->replaceOldBlWords($data['name'],$data['search_type'],$bl_types, $bl_countries );
			}
			
			die(json_encode($validator->getStatus()));
        }

        $this->view->id = $id;
    }

    
}
