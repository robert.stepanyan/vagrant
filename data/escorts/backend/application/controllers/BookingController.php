<?php

class BookingController extends Zend_Controller_Action {

	public function init()
	{
		$this->model = new Model_Booking();
	}
	
	public function indexAction() 
	{
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('default_booking_data');
		$this->view->filter_params = $ses_filter->filter_params;
		/* FILTER PARAMS */
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'showname' => $req->showname
		);
		
		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		$DEFINITIONS = Zend_Registry::get('defines');
		
		foreach ( $data as $i => $item ) {
			$data[$i]->duration_unit = $DEFINITIONS['hours_days_options'][$item->duration_unit];
		}
		
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
	
	public function editAction()
	{
		if( ! $this->_request->isPost() )
		{
			$this->view->data = $data = $this->model->getById($this->_request->id);
			$un = @unserialize($data->contact);
			$un ? $contcat = $un : $contcat = array();
			$this->view->contact = $contcat;
			$un = @unserialize($data->preferred_time);
			$un ? $time = $un : $time = array();
			$this->view->time = $time;
		}
		else 
		{
			$data = new Cubix_Form_Data($this->_request);
						
			$data->setFields(array(
				'id' => 'int',
				'name' => '',
				'surname' => '',
				'address' => '',
				'zip' => '',
				'city' => '',
				'state' => '',
				'country_id' => 'int',
				'phone' => '',
				'email' => '',
				'duration' => 'int',
				'duration_unit' => '',
				'message' => ''
			));
			
			$data = $data->getData();
					
			$data['contact'] = serialize($this->_request->contact);
			$data['preferred_time'] = serialize($this->_request->time);
						
			$validator = new Cubix_Validator();
			
			if ( $validator->isValid() ) {
				$this->model->save($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function removeAction()
	{
		$id = intval($this->_request->id);
		$this->model->remove($id);
		
		die;
	}
}
