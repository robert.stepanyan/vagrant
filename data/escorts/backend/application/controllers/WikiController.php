<?php

class WikiController extends Zend_Controller_Action
{
	protected $model;

	public function init()
	{
		$this->model = new Model_Faq();
                
		$this->user = Zend_Auth::getInstance()->getIdentity();
		
		if (!in_array($this->user->type,array('superadmin','admin','sales manager'))){
			die('Permission denied');
		}
		$this->session = new Zend_Session_Namespace('add_wiki_piki_pic');
	}

	public function indexAction()
	{

	}

	public function dataAction()
	{
		$req = $this->_request;
                 
		$filter = array('title' => $req->title, 'question' => $req->question, 'answer' => $req->answer, 'type' => $req->type, 'application_id' => $req->application_id );
               
		$data = $this->model->getAll(
			$req->page,
			$req->per_page,
			$filter,
			$req->sort_field,
			$req->sort_dir,
			$count
		);
                
		$this->view->data = array(
			'data' => $data,
			'count' => $count
                        
		);
                
	}

	public function viewAction()
	{
            
		$this->view->item = $item = $this->model->get($this->_getParam('id'), $this->_getParam('application_id'));
                 if (Cubix_Application::getId() == APP_EF) {
                    $this->user = Zend_Auth::getInstance()->getIdentity();
                    $wiki_id = $this->view->id = $this->_getParam('id');
                    $photos = $this->model->getPhoto( $wiki_id );


                    foreach( $photos as $photo ){
                        $photo->url = $this->_getPhoto($photo, 'orig');
                        $photo->url_thumb = $this->_getPhoto($photo, 'backend_thumb');
                    }

                    $this->view->photos = $photos;
                 }
	}

    public function createAction()
    {
        if (in_array(Cubix_Application::getId(), array(APP_EF,APP_EG_CO_UK,APP_BL,APP_ED))) {
            $this->user = Zend_Auth::getInstance()->getIdentity();
            if ($this->_request->isPost() && ($this->user->type == 'superadmin' || $this->user->type == 'admin')) {
                $question = Cubix_I18n::getValues($this->_request->getParams(), 'question', '');
                $answer = Cubix_I18n::getValues($this->_request->getParams(), 'answer', '');
                $types = $this->_request->c_types;
                $title = trim($this->_request->title);
                $importance = $this->_request->importance;

                $validator = new Cubix_Validator();

                if (!$types)
                    $validator->setError('types', 'Select at least one type.');

                if (!$title)
                    $validator->setError('title', 'Required');

                if ($importance < 0)
                    $validator->setError('importance', "Importance can't have a negative value");

                if ($validator->isValid()) {
                    $data = array();
                    $langs = Cubix_I18n::getLangs(true);


                    $data['application_id'] = $this->_request->getParam('application_id');
                    $data['title'] = $title;
                    $data['importance'] = $importance;

                    foreach ($langs as $lang) {
                        $data['question_' . $lang] = $question['question_' . $lang];
                        $data['answer_' . $lang] = $answer['answer_' . $lang];
                    }

                    $data['type'] = implode(',', $types);

                    $model = $this->model;
                    $model->insert(new Model_System_FaqItem($data));

                    $adapter = $model::getAdapter();
                    $wiki_id = $adapter->lastInsertId();

                    $pic_data = $this->session->photos;

                    $data_pic = array();
                    foreach ($pic_data['wiki'] as $pic) {
                        $data_pic['wiki_id'] = $wiki_id;
                        $data_pic['hash'] = $pic['hash'];
                        $data_pic['ext'] = $pic['ext'];
                        $data_pic['status'] = 1;

                        $this->model->addPhotos($data_pic);
                    }

                    $this->session->unsetAll();
                }

                die(json_encode($validator->getStatus()));
            }
        }
    }
        
        
        public function uploadHtml5Action()
	{
		$tmp_dir = sys_get_temp_dir();
		$config = Zend_Registry::get('images_config');
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$req_id = intval($this->_getParam('req_id'));
		$wiki_id = intval($this->_getParam('wiki_id'));
		$ebat = intval($this->_getParam('ebat'));
		$is_replace = $this->_getParam('replace') ? true : false;
		
		try {
			
			$response = array(
				'id'	=> $_SERVER['HTTP_X_FILE_ID'],
				'name'	=> $_SERVER['HTTP_X_FILE_NAME'],
				'size'	=> $_SERVER['HTTP_X_FILE_SIZE'],
				'error'	=> 0,
				'msg' => '',
				'finish'	=> FALSE
			);
			
			$ext = strtolower(@end(explode('.', $response['name'])));
			$file = $tmp_dir . DIRECTORY_SEPARATOR . $response['name'];
			file_put_contents($file, file_get_contents('php://input'));
			
//			if ( !$escort_id ) {
//				throw new Exception("Please select escort id", Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
//			}
//			elseif($this->checkEscortExists($escort_id)){
//				throw new Exception('Escort id - '.$escort_id. ' exists', Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
//			}
			if (!in_array( $ext , $config['allowedExts'])){
				throw new Exception(Cubix_I18n::translate('sys_error_upload_img_allowed_extensions', array('extensions' => implode(', ', $config['allowedExts']))), Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
			}	
			
			$images = new Cubix_Images();
			$photo = $images->save($file, 'wiki', Cubix_Application::getId(), $ext);
			//$photo = array('hash' => "1111111111", 'ext'=> $ext);
			
			if ( ! isset($photo['hash']) ) {
				throw new Exception("Photo upload failed. Please try again!");
			}
                        
                        if( !isset( $ebat ) || !$this->session->photos['ebat'] || $ebat != $this->session->photos['ebat'] ){
                            $this->session->unsetAll();
                        }
                        
                        $this->session->photos['ebat'] = $ebat;
			
			if($is_replace){
//				$photo['update_date'] = new Zend_Db_Expr('NOW()');
                            $this->model->update($req_id, $photo);
			}
			else{
                            $this->session->photos['wiki'][] = $photo;
                            //echo $response['id']; print_r( $this->session->photos ); die;
			}
			
			
			$image = new Cubix_Images_Entry($photo);
			$image->setSize('t100p');
			$image->setCatalogId('wiki');
			$response['photo_url'] = $images->getUrl($image);
			$image->setSize(null);
			$response['photo_url_orig'] = $images->getUrl($image);
			$response['finish'] = TRUE;
		} catch (Exception $e) {
			$response['error']	= 1;
			$response['msg']	= $e->getMessage();
		}		
		
		echo json_encode($response);die;
	}
        
  

	public function editAction()
	{
		$this->view->item = $item = $this->model->get($this->_getParam('id'), $this->_getParam('application_id'));
		$langs = Cubix_I18n::getLangsByAppId($this->_getParam('application_id'));
		$this->user = Zend_Auth::getInstance()->getIdentity();
		$wiki_id = $this->view->id = $this->_getParam('id');
                $photos = $this->model->getPhoto( $wiki_id );

                
                foreach( $photos as $photo ){
                    $photo->url = $this->_getPhoto($photo, 'orig');
                    $photo->url_thumb = $this->_getPhoto($photo, 'backend_thumb');
                }
                
                $this->view->photos = $photos;
                
		if ( $this->_request->isPost() && in_array(Cubix_Application::getId(), array(APP_EF,APP_EG_CO_UK,APP_BL,APP_ED)) && ($this->user->type == 'superadmin' || $this->user->type == 'admin') )
		{			
			$id = $this->_request->getParam('id');
			$question = Cubix_I18n::getValues($this->_request->getParams(), 'question', '');
			$answer = Cubix_I18n::getValues($this->_request->getParams(), 'answer', '');
			$types = $this->_request->c_types;
			$title = trim($this->_request->title);
            $importance = $this->_request->importance;
                        
                      
                       

			$validator = new Cubix_Validator();

			if (!$types)
				$validator->setError('types', 'Select at least one type.');

			if (!$title)
				$validator->setError('title', 'Required');

			if ( $validator->isValid() ) {
				$data = array();

				$data['id'] = $id;
				$data['application_id'] = $this->_request->getParam('application_id');
				$data['title'] = $title;
                $data['importance'] = $importance;
                               
                               
                        
                                

                                $pic_data = $this->session->photos;

                                $data_pic = array();
                                foreach( $pic_data['wiki'] as $pic ){
                                    $data_pic['wiki_id'] = $wiki_id;
                                    $data_pic['hash'] = $pic['hash'];
                                    $data_pic['ext'] = $pic['ext'];
                                    $data_pic['status'] = 1;

                                    $this->model->addPhotos($data_pic);
                        }
                                $this->session->unsetAll();

				foreach ($langs as $lang)
				{
					$data['question_' . $lang->lang_id] = $question['question_' . $lang->lang_id];
					$data['answer_' . $lang->lang_id] = $answer['answer_' . $lang->lang_id];
				}

				$data['type'] = implode(',', $types);

				$this->model->update(new Model_System_FaqItem($data));
//				$this->model->updatePhoto(new Model_System_FaqItem($data_photos));
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
        
        function _getPhoto( $data, $thumb ){
            $conf = Zend_Registry::get('images_config');
            
            return $conf['remote']['url'] . "/" . Cubix_Application::getById()->host . "/wiki/" . $data->hash . "_" . $thumb  . "." . $data->ext;
        }

    public function toggleActiveAction()
    {
            $id = intval($this->_request->id);
            if ( ! $id ) die;
            $this->model->toggle($id);
    }

    public function removeAction()
    {
        if (in_array(Cubix_Application::getId(), array(APP_EF,APP_EG_CO_UK,APP_BL,APP_ED))) {
             $this->user = Zend_Auth::getInstance()->getIdentity();
             if ($this->user->type == 'superadmin' || $this->user->type == 'admin') {
                    $id = $this->_getParam('id');

                    $this->model->remove($id, $this->_getParam('application_id'));

                    die;
             }
        }
    }
    
    
    
   

    public function ajaxRemovePhotoAction()
    {
        $model= $this->model;
        $ids = json_decode( $this->_getParam('ids') );
        
        if( !count ( $ids ) ) die;
        //print_r( $ids ); die;
        
        $stat = $model->removePhotos( $ids );
        
        if( !$stat ){
            $status = array('status' => 'error');
        } else {
            $status = array('status' => 'success');
            
            if( isset( $this->session->photos['wiki'] ) ){
                foreach( $this->session->photos['wiki'] as $ind => $photo ){
                    if( in_array( $photo['id'], $ids ) ){
                        unset( $this->session->photos['wiki'][$ind] );
                    }
                }
            }
        };
        
        die( json_encode( $status ) );
    } 
}
