<?php


class ReciprocalLinksController extends Zend_Controller_Action {
	protected $_model;
	
	
	public function init()
	{
		$this->_model = new Model_ReciprocalLinks();
	}
	
	public function indexAction()
	{

	}
	
	public function dataAction()
	{
		$req = $this->_request;
        
		$filter = array(
			'website' => $req->website,
			'site_owner_type' => $req->site_owner_type,
			'status' => $req->status,
			'showname'	=> $req->showname,
			'agency_name'	=> $req->agency_name,
			'agency_id' => $req->agency_id,
			'escort_id' => $req->escort_id
		);
        
		$data = $this->_model->getAll(
			$req->page,
			$req->per_page,
			$filter,
			$req->sort_field,
			$req->sort_dir,
			$count
		);
		
		foreach ( $data as $i => $item ) 
		{
			$data[$i]->website = $item->website;
			$data[$i]->check_date = strtotime($item->check_date);
			$data[$i]->showname = $item->showname;
			$data[$i]->user_type = $item->user_type;
			$data[$i]->user_id = $item->user_id;
		}

		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
	
	public function removeAction()
	{
		$id = $this->_request->id;
		
		if ( ! is_array($id) )
			$id = array($id);
		
		$this->_model->delete($id);
		
		die;
	}
	
	public function runCrawlerAction()
	{
		set_time_limit(60 * 60 * 24);
		error_reporting(E_ALL ^ E_WARNING ^ E_NOTICE); //disable error reporting to avoid DOMDocument warnings
		
		$scriptStartTime = time();
		
		$websitesToCheck = $this->_model->getWebsitesToCheck();
		$total_count = count($websitesToCheck);
		foreach($websitesToCheck as $i => $website) {
			echo 'Fetching ' . $i . '/' . $total_count . ' ' . $website->url . ': ';
			$has_reciprocal_link = $this->_model->checkIfHasReciprocalLink($website->url);
			
			//Checking if we have this site in our database. 
			$is_exists = $this->_model->checkIfExists($website->url);
				
			if ( ! $has_reciprocal_link ) {
				echo 'Not found' . "\n";
				
				//Cubix_Email::sendTemplate('reciprocal_links', $website->email, array());
				
				if ( ! $is_exists ) { //Insert only if does not exist in our database
					$this->_model->insert(array(
						'website' => $website->url, 
						'check_date' => new Zend_Db_Expr('NOW()'),
						'user_id'	=> $website->user_id,
						'status' => RECIPROCAL_LINK_HAS_NOT_BANNER
					));
				}
				else
				{
					$this->_model->update(array('status' => RECIPROCAL_LINK_HAS_NOT_BANNER), $website->url);
				}
			} else { 
				echo 'Found.' . "\n";
				if ( $is_exists ) { //If exists and has reciprocal link remove it from our database
					//$this->_model->deleteByWebsite($website->url);
					$this->_model->update(array('status' => RECIPROCAL_LINK_HAS_BANNER), $website->url);
				}
				else
				{
					$this->_model->insert(array(
						'website' => $website->url, 
						'check_date' => new Zend_Db_Expr('NOW()'),
						'user_id'	=> $website->user_id,
						'status' => RECIPROCAL_LINK_HAS_BANNER
					));
				}
			}
			ob_flush();
		}
				
		$scriptEndTime = time();
		
		echo 'Fetching total time is ' . round( ($scriptEndTime - $scriptStartTime) / 60 ) . ' minutes' . "\n";
		die;
	}
	
	public function toggleAction()
	{
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		if ($bu_user->type == 'superadmin'){
			$id = intval($this->_request->id);
			if ( ! $id ) die;
			$this->_model->toggle($id);
		}
		die;
	}
}
