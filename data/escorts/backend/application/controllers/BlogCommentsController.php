<?php

class BlogCommentsController extends Zend_Controller_Action 
{
	public function init()
	{
		$this->model = new Model_Blog();
	}
	
	public function indexAction() 
	{
		
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'id' => $req->id,
			'post_id' => $req->post_id,
			'name' => $req->name,
			'email' => $req->email,
			'ip' => $req->ip,
			'comment' => $req->comment,
			'status' => $req->status,
			'is_reply' => $req->is_reply
		);
		
		$data = $this->model->getAllComments(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		$DEFINITIONS = Zend_Registry::get('defines');
		
		foreach ( $data as $i => $item ) {
			$data[$i]->status = $DEFINITIONS['blog_comment_statues'][$item->status];
		}
				
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
	
	public function removeAction()
	{
		$id = intval($this->_request->id);
		$this->model->removeComment($id);
		
		die;
	}
	
	public function editAction()
	{
		if ( ! $this->_request->isPost() )
		{
			$this->view->data = $this->model->getComment($this->_request->id);
			$this->view->post = $this->model->getPostByComment($this->_request->id);
			$this->view->admin_comments = $this->model->getAdminComments($this->_request->id);
		}
		else 
		{
			$data = new Cubix_Form_Data($this->_request);
						
			$data->setFields(array(
				'id' => 'int',
				'comment' => ''
			));
			
			$data = $data->getData();
			
			$validator = new Cubix_Validator();
									
			if ( !strlen($data['comment']) ) {
				$validator->setError('comment', 'Required');
			}
			
			switch ($this->_request->act)
			{
				case 'approve':
					$data['status'] = BLOG_COMMENT_STATUS_APPROVED;
					break;
				case 'disable':
					$data['status'] = BLOG_COMMENT_STATUS_DISABLED;
					break;
			}
			
			if ( $validator->isValid() ) {
				$this->model->saveComment($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}

	public function adminCommentAction()
	{
		$comment_id = $this->_request->comment_id;
		$post_id = $this->_request->post_id;
		$admin_comment = $this->_request->admin_comment;

		$data = array(
			'post_id' => $post_id,
			'comment' => $admin_comment,
			'is_reply' => $comment_id,
			'status' => 2,
			'by_admin' => 1
		);

		$this->model->addAdminComment($data);
	}

	public function getAdminCommentsAction()
	{
		$this->view->layout()->disableLayout();

		$this->view->admin_comments = $this->model->getAdminComments($this->_request->id);
	}
}
