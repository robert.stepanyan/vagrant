<?php

class AdsController extends Zend_Controller_Action
{
	public function init()
	{
		$this->model = new Model_Ads();
	}
	
	public function indexAction() 
	{
		/* FILTER PARAMS */
		/*$ses_filter = new Zend_Session_Namespace('default_comments_data');
		$this->view->filter_params = $ses_filter->filter_params;*/
		/* FILTER PARAMS */
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'application_id' => $req->application_id
		);
		
		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		/*$DEFINITIONS = Zend_Registry::get('defines');
		
		foreach ( $data as $i => $item ) {
			$data[$i]->status = $DEFINITIONS['comment_status_options'][$item->status];
			$data[$i]->message = mb_substr($item->message, 0, 50, 'UTF-8') . ' ...';
		}*/
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

	public function assignEscortAction()
	{
		$req = $this->_request;

		$id = $req->id;
		$this->view->app_host = Cubix_Application::getById($req->application_id)->host;
		$application_id = $req->application_id;

		$this->view->escorts = $this->model->getAssignedEscorts($id, $application_id);

		$this->view->id = $id;

		if ( $req->isPost() ) {
			$escort_ids = $req->escort_ids;

			$this->model->save($id, $escort_ids, $application_id);
		}
	}

	public function escortsListAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;

		$bu_user = Zend_Auth::getInstance()->getIdentity();

		if ( in_array($bu_user->type, array('sales manager', 'moderator', 'moderator plus')) ) {
			$req->setParam('sales_user_id', $bu_user->id);
		}
		else if ($bu_user->type == 'admin') {
			$req->setParam('application_id', $bu_user->application_id);
		}

		$result = array();

		$count = 0;

		/* --> Fetch Escorts */
		$model = new Model_Escorts();

		$filter = array(
			'e.showname' => $req->search,
			'u.user_type = ?' => 'escort',
			'u.sales_user_id = ?' => $req->sales_user_id,
			//'excl_status' => array(Model_Escorts::ESCORT_STATUS_DELETED)
			'e.status' => array(Model_Escorts::ESCORT_STATUS_ACTIVE),
			'u.application_id = ?' => $req->application_id
		);

		$data = $model->getForAutocompleter(
			1,
			10,
			$filter,
			'e.showname',
			'ASC',
			$count
		);

		if ( $count > 0 ) $result[] = array('title' => 'Escorts','type' => 'group');

		foreach($data as $row) {
			$result[] = array(
				'id' => $row->id,
				'title' => $row->showname,
				'username' => $row->username,
				//'balance' => $row->balance,
				'user_id' => $row->user_id,
				//'user_type' => USER_TYPE_SINGLE_GIRL,
				//'photo' => $row->getMainPhoto()->getUrl('backend_smallest')
			);
		}
		/* <-- */

		die(json_encode($result));
	}

	public function editAction()
	{
		if( ! $this->_request->isPost() )
		{
			$this->view->data = $data = $this->model->getById($this->_request->id);
		}
		else 
		{
			$data = new Cubix_Form_Data($this->_request);
						
			$data->setFields(array(
				'id' => 'int',
				'escort_id' => 'int',
				'status' => 'int',
				'comment' => ''
			));
			
			$data = $data->getData();
						
			$validator = new Cubix_Validator();
						
			if ( strlen($data['comment']) < self::MINIMUM_CHARS_COUNT ) {
				$validator->setError('comment', 'Minimum ' . self::MINIMUM_CHARS_COUNT . ' chars');
			}
			
			switch ($this->_request->act)
			{
				case 'approve':
					$data['status'] = COMMENT_ACTIVE;
					break;
				case 'reject':
					$data['status'] = COMMENT_DISABLED;
					break;
			}

			if ( $validator->isValid() ) {
				$this->model->save($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function removeAction()
	{
		$id = intval($this->_request->id);
		$this->model->remove($id);
		
		die;
	}
}
