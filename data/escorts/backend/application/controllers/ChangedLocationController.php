<?php

class ChangedLocationController extends Zend_Controller_Action
{
	protected $model;

	public function init()
	{
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		if($bu_user->type != 'superadmin'){
			die();
		}
		
		$this->model = new Model_ChangedLocation();
	}

	public function indexAction()
	{
		$bu_model = new Model_BOUsers();
		$this->view->sales_persons = $bu_model->getAllSales();
	}

	public function dataAction()
	{
		$req = $this->_request;

		$filter = array('sales_user_id' => $req->sales_user_id,
					    'date_from' => $req->date_from,
						'date_to' => $req->date_to );
       

		$data = $this->model->getAll(
			$req->page,
			$req->per_page,
			$filter,
			$req->sort_field,
			$req->sort_dir,
			$count
		);
	
		$citiesModel = new Cubix_Geography_Cities();
		$db = Zend_Registry::get('db');
		foreach ($data as $item) {
			$item->new_city = $citiesModel->getTitleById($item->new_city_id);
			$item->old_city = $citiesModel->getTitleById($item->old_city_id);

			$sql = "SELECT system_order_id FROM orders WHERE id = ?";
			$system_order_id = $db->fetchOne($sql, array($item->order_id));
			$item->system_order_id = $system_order_id;
		}

		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
}
