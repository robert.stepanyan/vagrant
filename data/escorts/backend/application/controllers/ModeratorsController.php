<?php

class ModeratorsController extends Zend_Controller_Action 
{
	public function init()
	{
		$this->model = new Model_Moderators();
	}
	
	public function indexAction() 
	{
		// TODO Auto-generated ModeratorsController::indexAction() default action
	}
	
	public function ajaxAction()
	{
		$application_id = intval($this->_request->application_id);
				
		echo json_encode(array('data' => $this->model->getByApplicationId($application_id)));
		die;
	}
}
