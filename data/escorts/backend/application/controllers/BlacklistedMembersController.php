<?php

class BlacklistedMembersController extends Zend_Controller_Action
{
	protected $model;
	protected $defines;

	public function init()
	{
		$this->view->defines = $this->defines = Zend_Registry::get('defines');
		$this->model = new Model_BlacklistedMembers();
	}

	public function indexAction()
	{

	}

	public function dataAction()
	{
		$req = $this->_request;
        
		$filter = array(
			'showname' => $req->showname, 
			'escort_id' => $req->escort_id, 
			'username' => $req->username, 
			'status' => $req->status, 
			'application_id' => $req->application_id 
		);
        
		$data = $this->model->getAll(
			$req->page,
			$req->per_page,
			$filter,
			$req->sort_field,
			$req->sort_dir,
			$count
		);
		
		foreach ( $data as $i => $item ) 
		{
			$data[$i]->status_name = $this->defines['bl_member_comments_status'][$item->status];
		}

		die(json_encode(array(
			'data' => $data,
			'count' => $count
		)));
	}

	public function viewAction()
	{
		$this->view->item = $item = $this->model->get($this->_getParam('id'));
		
		if ( $this->_request->isPost() )
		{
			$validator = new Cubix_Validator();
			$act = $this->_request->act;
			
			$data = array('id' => $item->id);
			
			if ($act == 'approve')
			{
				$data['status'] = BL_MEMBER_COMMENT_STATUS_APPROVED;

					$this->model->update($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function confirmAction()
	{
		set_time_limit(0);
		
		$ids = $this->_request->id;

		if ( is_array($ids) && count($ids) > 0)
		{
			foreach ($ids as $id)
			{
				$item = $this->model->get($id);

				if ($item->status != BL_MEMBER_COMMENT_STATUS_APPROVED)
				{
					$data = array('id' => $item->id);

					$data['status'] = BL_MEMBER_COMMENT_STATUS_APPROVED;

					$this->model->update($data);
				}
			}
		}
		
		die;
	}
}
