<?php

/**
 * Class AgenciesController
 *
 * @property  Model_Agencies $model
 */
class AgenciesController extends Zend_Controller_Action {

    public $model;
    public $user;

	public function init()
	{
		$this->model = new Model_Agencies();
        $this->user = Zend_Auth::getInstance()->getIdentity();
        $this->view->user = $this->user;
	}
	
	public function indexAction() 
	{
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('default_agencies_data');
		$this->view->filter_params = $ses_filter->filter_params;
		/* FILTER PARAMS */
	}

	public function fixAction()
	{
		$agencies = $this->model->fixAgencies();
		die;
	}
	
	public function dataAction()
	{
		$req = $this->_request;
        $bu_user = Zend_Auth::getInstance()->getIdentity();

		/*if ( in_array($bu_user->type, array('sales clerk','sales manager', 'moderator', 'moderator plus')) ) {
			$req->setParam('sales_user_id', $bu_user->id);
		}
		else if ($bu_user->type == 'admin') {
			$req->setParam('application_id', $bu_user->application_id);
		}*/
		
		$system = Zend_Registry::get('system_config');
		if ($system['allow_club'] == 1 && $system['allow_agency'] == 0)
			$req->setParam('is_club', 1);

		$filter = array(
			'a.name' => $req->agency_name,
			'u.username' => $req->username,
			'u.application_id = ?' => $req->application_id,
			'a.email' => $req->contact_email,
			'u.email' => $req->reg_email,
			'phone_number' => preg_replace("#[^0-9]#", "", $req->phone),
			'u.status = ?' => $req->status,
			'a.status' => $req->agency_status,
			'u.sales_user_id = ?' => $req->sales_user_id,
			'u.status <> ?' => USER_STATUS_DELETED,
			'a.is_club' => $req->is_club,
			'has_website' => $req->has_web,
			'a.id = ?' => $req->agency_id,
			'has_escorts' => $req->has_escorts,
			'has_package' => $req->has_package,
			'a.admin_verified = ?' => $req->admin_verified,
			'a.is_premium' => $req->is_premium,
			'a.show_in_club_directory' => $req->show_in_club_directory,
			'a.filter_criteria' => $req->filter_criteria,
			'ever_login' => $req->ever_login,
			'a.check_website' => $req->check_website,
			'a.block_website' => $req->block_website,
			'a.site_not_working' => $req->site_not_working,
			'a.zip = ?' => $req->zip,
			'admin_comment' => $req->admin_comment,
			'admin_comment_by' =>(int)  $req->admin_comment_by,
			'last_comment_by' => $req->last_comment_by,

			//'excl_status' => Model_Escorts::ESCORT_STATUS_DELETED
			'admin_comment_text' => $req->admin_comment_text,
			'updates_count' => $req->updates_count,
			'up_ag' => $req->up_ag,
			'd_from' => $req->d_from,
			'd_to' => $req->d_to
		);

		if (in_array(Cubix_Application::getId(),array(APP_A6)))
        {
            unset($filter['u.status <> ?']);
        }

		if($filter['a.id = ?']){
			if(strpos($req->agency_id, ',')){
				$exploded_eids = explode(',', $req->agency_id);
				if(is_array($exploded_eids)){
					$ids_string = '';
					foreach($exploded_eids as $eid){
						if((int)$eid>0){
							$ids .= (int)$eid.',';
						}
					}	

					$ids_string = rtrim($ids, ',');

					$filter[] = 'a.id IN ('.$ids_string.')';
					unset($filter['a.id = ?']);
					
				}
			}
		}


        if ($req->country)
            $filter['a.country_id = ?'] = $req->country;

        if (in_array(Cubix_Application::getId(),array(APP_EG_CO_UK,APP_ED)))
        {
            if ($req->updates_by == 1 || $req->updates_by == '0')
            {
                if ($req->updates_by == '0')
                {
                    $filter['a.updates_by < ?'] = 1;
                }else{
                    $filter['a.updates_by = ?'] = $req->updates_by;
                }
            }
        }

        if ($req->city)
            $filter['a.city_id = ?'] = $req->city;

        if ( Cubix_Application::getId() == APP_ED && !empty($req->data_entry_id) ) {
            $filter['a.data_entry_user = ?'] = $req->data_entry_id;
        }

		if ( $req->not_check_website == 1 ) {
			$filter[] = 'LENGTH(a.web) > 0 AND a.check_website = 0 AND a.block_website <> 1';
		}

		if ( strlen($req->web) ) {
			$filter = array_merge($filter, array('a.web' => '%' . $req->web));
		}

        if(in_array(Cubix_Application::getId(), array(APP_ED,APP_EG_CO_UK))){
            if ($req->sales_assigned_from )
            {
                $filter['sales_change_date_from'] = $req->sales_assigned_from;
            }
            if ( $req->sales_assigned_to )
            {
                $filter['sales_change_date_to'] = $req->sales_assigned_to;
            }

            if ($req->never_has_package)
                $filter['never_has_package'] = $req->never_has_package;

            if ($req->no_100_verified_escort)
                $filter['no_100_verified_escort'] = $req->no_100_verified_escort;
        }

		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page,
			$filter,
			$this->_request->sort_field,
			$this->_request->sort_dir,
			$count
		);
		
		$DEFINITIONS = Zend_Registry::get('defines');
		
		foreach ( $data as $i => $item ) {
			$data[$i]->logo_url = $item->getLogoUrl();
			$data[$i]->status = $DEFINITIONS['user_status_options'][$item->status];

			$c = $this->model->getLastComment($item->id);			
			if ($c) {
				$data[$i]->comment = date('Y-m-d H:i:s', $c->date) . ' | Commented by: ' . $c->username . '<br/>' . $c->comment;
			}

			$data[$i]->update_count = $this->model->getUpdateCount($item->id);
		}
		//print_r($data);die;
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
	
	public function createAction()
	{
		$country_model = new Model_Countries();
		$this->view->countries = $country_model->getPhoneCountries();
		$this->view->app_country = Model_Applications::getCountryId();
		if ( $this->_request->isPost() )
		{
			$data = new Cubix_Form_Data($this->_request);
						
			$data->setFields(array(
				'agency_name' => '',
				'display_name' => '',
				'agency_status'	=> '',
				'status' => '',
				'login' => '',
				'password' => '',
				'application_id' => 'int',
				'sales_person_id' => 'int',
				'country_id' => 'int',
				'region_id' => 'int',
				'city_id' => 'int',
				'work_days' => 'arr-int',
				'work_times_from' => 'arr-int',
				'work_times_to' => 'arr-int', 
				'email' => '',
				'u_email' => '',
				'web' => '',
				'phone_prefix' => '',
				'phone' => '',
				'phone_instructions' => '',
				'available_24_7' => 'int',
				'address' => '',
				'latitude' => 'double',
				'longitude' => 'double',
				'fake_city_id' => 'int',
				'zip' => '',
				'is_anonymous'	=> 'int',
				'is_club' => 'int',
				'entrance' => 'int',
				'wellness' => 'int',
				'wellness_add' => '',
				'food' => 'int',
				'food_add' => '',
				'outdoor' => 'int',
				'filter_criteria' => 'int',
				'is_premium' => 'int',
				'show_in_club_directory' => 'int',
				'is_international' => '',
				'viber' => 'int',
				'whatsapp' => 'int'
				));
			
			$data = $data->getData();
			
			$validator = new Cubix_Validator();

			if ( Cubix_Application::getId() != APP_ED ) {
				unset($data['is_international']);				
			}

			if ( Cubix_Application::getId() == APP_ED || Cubix_Application::getId() == APP_EG_CO_UK) {
				$data['wechat'] = 'int';				
				$data['telegram'] = 'int';				
				$data['ssignal'] = 'int';
			}

			if ( Cubix_Application::getId() == APP_ED ) {
				$premium_days = (int) $this->_request->premium_days;
				
				if ( $data['is_premium'] ) {
					if ( ! $premium_days ) {
						$validator->setError('is_premium', 'Premium Days required');
					} else {
						$data['premium_end_date'] = strtotime("+" . $premium_days . " day", time());
						$data['premium_days'] = $premium_days;
					}
				} else {
					$data['premium_end_date'] = null;
					$data['premium_days'] = null;
				}
			}

			if ( ! $data['available_24_7'] ) {
				$data['available_24_7'] = null;
			}
			
			if ($data['is_club'] == 1)
			{				
				///////////// wellness ////////////////
				if ($data['wellness'] == CLUB_WELLNESS_YES)
				{
					if (!$data['wellness_add'])
					{
						$validator->setError('err_wellness', 'Wellness Required (Free / With cost)');
					}
					else
					{
						$data['wellness'] = $data['wellness_add'];
						unset($data['wellness_add']);
					}
				}
				else
				{
					unset($data['wellness_add']);
				}
				///////////////////////////////////////
				
				///////////// food ////////////////////
				if ($data['food'] == CLUB_FOOD_YES)
				{
					if (!$data['food_add'])
					{
						$validator->setError('err_food', 'Food and drinks Required (Free / With cost)');
					}
					else
					{
						$data['food'] = $data['food_add'];
						unset($data['food_add']);
					}
				}
				else
				{
					unset($data['food_add']);
				}
				///////////////////////////////////////
			}
			else
			{
				$data['entrance'] = CLUB_ENTRANCE_NA;
				$data['wellness'] = CLUB_WELLNESS_NA;
				unset($data['wellness_add']);
				$data['food'] = CLUB_FOOD_NA;
				unset($data['food_add']);
				$data['outdoor'] = CLUB_OUTDOOR_NA;
			}
			
			if ( ! $data['sales_person_id'] ) {
				$validator->setError('sales_person_id', 'Required');
			}
			
			if ( ! $data['status'] ) {
				$validator->setError('status', 'Required');
			}
			
			if ( ! strlen($data['login']) ) {
				$validator->setError('login', 'Required');
			}
			elseif ( $this->model->existsByUsername($data['login']) ) {
				$validator->setError('login', 'Already exists');
			}
			
			if ( ! strlen($data['u_email']) ) {
				$validator->setError('u_email', 'Required');
			}
			elseif ( ! $validator->isValidEmail($data['u_email']) ) {
				$validator->setError('u_email', 'Invalid Email');
			}
			elseif (Cubix_Application::getId() == APP_ED && trim($data['u_email']) != "no@no.com" && $this->model->existsByEmail($data['u_email']) ) {
				$validator->setError('u_email', 'Email Already exists');
			}
			elseif ( strlen($data['u_email']) && Cubix_Application::isDomainBlacklisted($data['u_email']) ) {
				$validator->setError('u_email', 'Domain is blacklisted');
			}
			
			if ( ! strlen($data['password']) ) {
				$validator->setError('password', 'Required');
			}
			elseif ($data['password'] == $data['login']){
				$validator->setError('password', "Can't use username as password");
			}
			
			if ( ! strlen($data['agency_name']) ) {
				$validator->setError('agency_name', 'Required');
			}
			elseif ( ! preg_match('/^[-\.\s_a-z0-9]+$/i', $data['agency_name']) ) {
				$validator->setError('agency_name', 'Allowed only: -, _, a-z, 0-9');
			}

			if ( Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT ) {
				if ( ! $data['fake_city_id'] ) {
					$validator->setError('fake_city_id', 'Required');
				}
			}
						
			if ( ! $validator->isValidEmail($data['email']) ) {
				$validator->setError('email', 'Invalid Email');
			}
			elseif ( strlen($data['email']) && Cubix_Application::isDomainBlacklisted($data['email']) ) {
				$validator->setError('email', 'Domain is blacklisted');
			}
			
			if ( strlen($data['web']) && Cubix_Application::isDomainBlacklisted($data['web']) ) {
				$validator->setError('web', 'Domain is blacklisted');
			}
			
			$data['contact_phone_parsed'] = null;
			if ( $data['phone'] ) {
				if ( ! preg_match("/^[0-9\s\+\-\(\)]+$/i", $data['phone']) ) {
					$validator->setError('phone', 'Invalid phone number');
				}
				elseif(preg_match("/^(\+|00)/", $data['phone']) ) {
					$validator->setError('phone', 'Please enter phone number without country calling code');
				}
				if ( ! ($data['phone_prefix']) ) {
				$validator->setError('phone_prefix', 'Country calling code Required');
				}
				
				list($country_id,$phone_prefix,$ndd_prefix) = explode('-',$data['phone_prefix']);
				unset($data['phone_prefix']);
				$data['contact_phone_parsed'] = $data['phone'] = preg_replace('/[^0-9]/', '', $data['phone']);
				$data['contact_phone_parsed'] = preg_replace('/^'.$ndd_prefix.'/', '', $data['contact_phone_parsed']);
				$data['contact_phone_parsed'] = '00'.$phone_prefix.$data['contact_phone_parsed'];

				$data['phone_country_id'] = intval($country_id);
				
				if (!in_array(Cubix_Application::getId(), array(APP_A6 ,APP_A6_AT, APP_ED))) {
					$escort_model = new Model_EscortsV2();
					$results = $escort_model->existsByPhone($data['contact_phone_parsed'],null, $data['id']);

					$resCount = count($results);

					if($resCount > 0) {
						$validator->setError('phone', 'Phone already exists');
					}
				}
			}

			for ( $i = 1; $i <= 7; $i++ ) {
				if ( isset($data['work_days'][$i]) && ( ! @$data['work_times_from'][$i] || ! @$data['work_times_to'][$i] ) ) {
					$validator->setError('work_times_' . $i, '');
				}
			}
			
			$slug = '';
			$slug = $this->clear_string($data['agency_name']);

			$save_data  = array(
				'application_id' 		=> $data['application_id'],
				'sales_user_id'    		=> $data['sales_person_id'],
				'status' 	   			=> $data['status'],
				'name'       			=> $data['agency_name'],
				'display_name'       	=> strlen(trim($data['display_name'])) ? $data['display_name'] : $data['agency_name'],
				'slug'       			=> $slug,
				'country_id' 			=> $data['country_id'],
				'city_id'    			=> $data['city_id'],
				'email'      			=> $data['email'],
				'u_email'      			=> $data['u_email'],
				'web'		 			=> $data['web'],
				'phone_country_id'		=> $data['phone_country_id'],
				'phone'		 			=> $data['phone'],
				'contact_phone_parsed'	=> $data['contact_phone_parsed'],
				'phone_instructions' 	=> $data['phone_instructions'],
				'username' 				=> $data['login'],
				'password' 				=> $data['password'],
				'work_days'				=> $data['work_days'],
				'work_times_from'		=> $data['work_times_from'],
				'work_times_to'			=> $data['work_times_to'],
				'available_24_7'		=> $data['available_24_7'],
				'address'				=> $data['address'],
				'latitude'				=> $data['latitude'],
				'longitude'				=> $data['longitude'],
				'is_anonymous'			=> $data['is_anonymous'],
				'is_club'				=> $data['is_club'],
				'entrance'				=> $data['entrance'],
				'wellness'				=> $data['wellness'],
				'food'					=> $data['food'],
				'outdoor'				=> $data['outdoor'],
				'filter_criteria'		=> $data['filter_criteria'],
				'is_premium'			=> $data['is_premium'],
				'show_in_club_directory'=> $data['show_in_club_directory'],
				'viber'                 => $data['viber'],
				'whatsapp'              => $data['whatsapp']
			);

			if ( Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT ) {
			    if (Cubix_Application::getId() == APP_A6)
                    $save_data = array_merge($save_data,array('agency_status' => '1'));

                $save_data = array_merge($save_data, array('fake_city_id' => $data['fake_city_id'], 'zip' => $data['zip']));
			}

			if ( Cubix_Application::getId() == APP_ED || Cubix_Application::getId() == APP_EG_CO_UK) {
				$save_data['wechat'] = $data['wechat'];
				$save_data['telegram'] = $data['telegram'];
				$save_data['ssignal'] = $data['ssignal'];
				$is_international = ($data['is_international']) ? $data['is_international'] : 0;
				$save_data = array_merge($save_data, array('is_international' => $is_international, 'premium_end_date' => $data['premium_end_date'], 'premium_days' => $data['premium_days'], 'agency_status' => $data['agency_status']));
			}
			
			$i18n_data = Cubix_I18n::getValues($this->_request->getParams(), 'about', '');
				$security = new Cubix_Security();
			foreach ( $i18n_data as $field => $value ) {
				if ( Cubix_Application::getId() != APP_A6 && Cubix_Application::getId() != APP_A6_AT ) {
					$value = $security->clean_html($value);
				}
				else {
					$value = strip_tags($value);
				}
				$i18n_data[$field] = $security->xss_clean($value);
			}
			
			$save_data['about'] = $i18n_data;

			$result = $validator->getStatus();
			
			if ( $validator->isValid() ) {
				$new_id = $this->model->save(new Model_AgencyItem($save_data));
				
				Cubix_CityAlertEvents::notify($save_data['city_id'], NULL, $new_id);
			}
			
			$this->view->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender();
			echo json_encode(array_merge($validator->getStatus(), array('new_id' => $new_id)));
		}
	}

	private function clear_string($str)
	{
		$str = strtolower($str);

		$str = preg_replace("#[^0-9a-z]#", '-', $str);

		$str = preg_replace("#\s+#", '-', $str);

		$str = ___clean_str($str);

		return $str;
	}

	public function editAction()
	{
		$agency_id = intval($this->_request->id);
		if ( ! $this->user->hasAccessToAgency($agency_id) ) die('Permission denied');
		ini_set('memory_limit', '1024M');
		
		$b_user = Zend_Auth::getInstance()->getIdentity();
		$modelProducts = new Model_Products();
		$modelPackages = new Model_Packages();
		$back_user = Zend_Auth::getInstance()->getIdentity();
		$this->view->back_user_id = $back_user->id;
		if (in_array(Cubix_Application::getId(),array(APP_ED,APP_EG_CO_UK)))
        {
            $this->view->verification_data = $this->model->getHandVerificationDate($agency_id);
        }
		if( ! $this->_request->isPost() )
		{

			$data = $this->model->getById($agency_id);
			$this->view->last_ip = $this->model->getLastIP($agency_id);
			$this->view->last_login_date = $this->model->getLoginDate($agency_id);
			$this->view->ip_info = Model_Users::getUniqueIps($data['agency_data']->user_id);
			$this->view->agency_data = $ag = $data['agency_data'];
			$this->view->agency_langs  = $data['langs'];
			$this->view->working_hours = $data['working_hours'];
			$this->view->linked_agencies = $this->model->getLinkedAgencies($ag->id);
			$countyModel = new Model_Countries();
			$this->view->countries = $countyModel->getPhoneCountries();
			$this->view->phone_prefix_id = $ag->phone_country_id;
			$this->view->phone_prefix_id_1 = $ag->phone_country_id_1;
			$this->view->phone_prefix_id_2 = $ag->phone_country_id_2;
			$m_agencies = new Model_Agencies();
            if (in_array(Cubix_Application::getId(), array(APP_ED)))
                $this->view->first_login_lang = $this->model->getFirstLoginLang($agency_id);


			$this->view->agency_list = $m_agencies->getList($this->_getParam(Cubix_Application::getId()), true);
			$this->view->rates = $this->_reconstructRates($m_agencies->getRates($agency_id));
			$travel_payment_methods = $m_agencies->getTravelPaymentMethods($agency_id);
			$this->view->travel_payment_methods = $travel_payment_methods['payment_methods'];
			$this->view->other_payment_method = $travel_payment_methods['other_method'];
			
			//FOR Calling Code
			
			/*if($ag->phone){
				if(preg_match('/^(\+|00)/',trim($ag->phone)))
				{
					$phone_prfixes = $countyModel->getPhonePrefixs();
					$phone_prefix_id = NULL;
					$phone_number = preg_replace('/^(\+|00)/', '',trim($ag->phone));
					foreach($phone_prfixes as $prefix)
					{
						if(preg_match('/^('.$prefix->phone_prefix.')/',$phone_number))
						{
							$phone_prefix_id = $prefix->id;
							$ag->phone = preg_replace('/^('.$prefix->phone_prefix.')/', '',$phone_number);
							BREAK;
						}

					}
					$this->view->phone_prefix_id = $phone_prefix_id;
				}
			}*/

			//
			//reviews - comments
			if ($ag->disabled_reviews == 0)
				$this->view->allow_rev = 'yes';
			else
				$this->view->allow_rev = 'no';

			if ($ag->disabled_comments == 0)
				$this->view->allow_com = 'yes';
			else
				$this->view->allow_com = 'no';
			
			// auto_approval
			$this->view->auto_approval_status = $this->model->getIsAutoApproval($agency_id);			
		}
		else 
		{
            $m_agencies = new Model_Agencies();
			$data = new Cubix_Form_Data($this->_request);
			
			$fields = array(
				'id' => 'int',
				'user_id' => 'int',
				'agency_name' => '',
				'display_name' => '',
				'agency_status'	=> '',
				'status' => '',
				'login' => '',
				'password' => '',
				'application_id' => 'int',
				'sales_person_id' => 'int',
				'country_id' => 'int',
				'region_id' => 'int',
				'city_id' => 'int',
				'work_days' => 'arr-int',
				'work_times_from' => 'arr-double',
				'work_times_to' => 'arr-double',
				'email' => '',
				'u_email' => '',
				'web' => '',
				'phone_prefix' => '',
				'phone' => '',
				'phone_instructions' => '',
				'available_24_7' => 'int',
				'address' => '',
				'latitude' => 'double',
				'longitude' => 'double',
				'fake_city_id' => 'int',
				'zip' => '',
				'is_anonymous'	=> 'int',
				'linked_agencies' => 'arr-int',
				'block_website' => 'int',
				'check_website' => 'int',
				'is_club' => 'int',
				'entrance' => 'int',
				'wellness' => 'int',
				'wellness_add' => '',
				'food' => 'int',
				'food_add' => '',
				'outdoor' => 'int',
				'admin_verified' => 'int',
				'admin_verified_current' => 'int',
				'filter_criteria' => 'int',
				'is_premium' => 'int',
				'show_in_club_directory' => 'int',
				'phone_to_all_escorts' => 'int',
				'phone_sms_verified' => 'int',
				'is_international' => '',
				'email_to_all_escorts' => '',
				'website_to_all_escorts' => '',
				'phone_prefix_1' => '',
				'phone_prefix_2' => '',
				'phone_1' => '',
				'phone_2' => '',
				'phone_to_all_escorts_1' => 'int',
				'phone_to_all_escorts_2' => 'int',
				'phone_instructions_1' => '',
				'phone_instructions_2' => '',
				'is_anonymous_1' => 'int',
				'is_anonymous_2' => 'int',
				'phone_instr' => 'int',
				'phone_instr_1' => 'int',
				'phone_instr_2' => 'int',
				'viber' => 'int',
				'whatsapp' => 'int',
				'special_rates' => 'notags|special'
			);

			if (  Cubix_Application::getId() == APP_ED) {
				$fields['langs'] = '';
				$fields['data_entry_id'] = 'int';
				$fields['about_services'] = '';
				$fields['about_areas_covered'] = '';
				$fields['wechat'] = 'int';
				$fields['telegram'] = 'int';
				$fields['ssignal'] = 'int';
				$fields['viber_1'] = 'int';
				$fields['whatsapp_1'] = 'int';
				$fields['viber_2'] = 'int';
				$fields['whatsapp_2'] = 'int';
			}

			if ( Cubix_Application::getId() == APP_EG_CO_UK){
                $fields['wechat'] = 'int';
                $fields['telegram'] = 'int';
                $fields['ssignal'] = 'int';
            }

            if(in_array(Cubix_Application::getId(),array(APP_ED,APP_EG_CO_UK))) {
                $fields['updates_by'] = 'int';
            }

			if ( Cubix_Application::getId() == 33 || Cubix_Application::getId() == 22 || Cubix_Application::getId() == APP_ED ) {
				$fields['min_book_time'] = '';
				$fields['min_book_time_unit'] = 'int';
				$fields['has_down_pay'] = 'int';
				$fields['down_pay'] = '';
				$fields['down_pay_cur'] = 'int';
				$fields['reservation'] = 'int';
			}
			
			$data->setFields($fields);
			$data = $data->getData();
			if ( Cubix_Application::getId() == 33 || Cubix_Application::getId() == 22 || Cubix_Application::getId() == APP_ED ) {
				if ( $data['has_down_pay'] != YES ) {
					$data['down_pay'] = null;
					$data['down_pay_cur'] = null;
				}						
			}
			
			$validator = new Cubix_Validator();

			if ( Cubix_Application::getId() == APP_ED ) {
				$premium_days = (int) $this->_request->premium_days;
				
				if ( $data['is_premium'] ) {
					if ( ! $premium_days ) {
						$validator->setError('is_premium', 'Premium Days required');
					} else {
						$data['premium_end_date'] = strtotime("+" . $premium_days . " day", time());
						$data['premium_days'] = $premium_days;
					}
				} else {
					$data['premium_end_date'] = null;
					$data['premium_days'] = null;
				}
			}

			if ( Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT ) {
				if ( ! $data['fake_city_id'] ) {
					$validator->setError('fake_city_id', 'Required');
				}
			}

			if ( Cubix_Application::getId() != APP_ED ) {
				unset($data['is_international']);
			}
			
			if ( ! $data['available_24_7'] ) {
				$data['available_24_7'] = null;
			}
			
			if ($data['is_club'] == 1)
			{
				///////////// wellness ////////////////
				if ($data['wellness'] == CLUB_WELLNESS_YES)
				{
					if (!$data['wellness_add'])
					{
						$validator->setError('err_wellness', 'Wellness Required (Free / Price)');
					}
					else
					{
						$data['wellness'] = $data['wellness_add'];
						unset($data['wellness_add']);
					}
				}
				else
				{
					unset($data['wellness_add']);
				}
				///////////////////////////////////////
				
				///////////// food ////////////////////
				if ($data['food'] == CLUB_FOOD_YES)
				{
					if (!$data['food_add'])
					{
						$validator->setError('err_food', 'Food and drinks Required (Free / Price)');
					}
					else
					{
						$data['food'] = $data['food_add'];
						unset($data['food_add']);
					}
				}
				else
				{
					unset($data['food_add']);
				}
				///////////////////////////////////////
			}
			else
			{
				$data['entrance'] = CLUB_ENTRANCE_NA;
				$data['wellness'] = CLUB_WELLNESS_NA;
				unset($data['wellness_add']);
				$data['food'] = CLUB_FOOD_NA;
				unset($data['food_add']);
				$data['outdoor'] = CLUB_OUTDOOR_NA;
			}
			
			if ( ! $data['sales_person_id'] ) {
				$validator->setError('sales_person_id', 'Required');
			}
			
			if ( ! $data['status'] ) {
				$validator->setError('status', 'Required');
			}
			
			if ( ! strlen($data['login']) ) {
				$validator->setError('login', 'Required');
			}
			elseif ( $this->model->existsByUsername($data['login'], $data['user_id']) ) {
				$validator->setError('login', 'Already exists');
			}
			
			if (strlen($data['password']) && $data['password'] == $data['login']){
				$validator->setError('password', "Can't use username as password");
			}
			
			if ( ! strlen($data['u_email']) ) {
				$validator->setError('u_email', 'Required');
			}
			elseif ( ! $validator->isValidEmail($data['u_email']) ) {
				$validator->setError('u_email', 'Invalid Email');
			}
			elseif (Cubix_Application::getId() == APP_ED && trim($data['u_email']) != "no@no.com" && $this->model->existsByEmail($data['u_email'], $data['user_id'], $data['id']) ) {
				$validator->setError('u_email', 'Email Already exists');
			}
			elseif ( strlen($data['u_email']) && Cubix_Application::isDomainBlacklisted($data['u_email']) ) {
				$validator->setError('u_email', 'Domain is blacklisted');
			}
			
			/*if ( ! strlen($data['password']) ) {
				$validator->setError('password', 'Required');
			}*/
			if ( ! strlen($data['agency_name']) ) {
				$validator->setError('agency_name', 'Required');
			}
			elseif ( ! preg_match('/^[-\.\s_\'a-z0-9]+$/i', $data['agency_name']) ) {
				$validator->setError('agency_name', 'Allowed only: -, _, \', a-z, 0-9');
			}
			/*elseif ( $this->model->existsByName($data['agency_name'], $data['id']) ) {
				$validator->setError('agency_name', 'Already exists');
			}*/
			
			if ( strlen($data['email']) > 0 && ! $validator->isValidEmail($data['email']) ) {
				$validator->setError('email', 'Invalid Email');
			}
			elseif ( strlen($data['email']) && Cubix_Application::isDomainBlacklisted($data['email']) ) {
				$validator->setError('email', 'Domain is blacklisted');
			}
			
			if ( strlen($data['web']) && Cubix_Application::isDomainBlacklisted($data['web']) ) {
				$validator->setError('web', 'Domain is blacklisted');
			}

			$data['contact_phone_parsed'] = null;
			if ( $data['phone'] ) {
				if ( ! preg_match("/^[0-9\s\+\-\(\)]+$/i", $data['phone']) ) {
					$validator->setError('phone', 'Invalid phone number');
				}
				elseif(preg_match("/^(\+|00)/", $data['phone']) ) {
					$validator->setError('phone', 'Please enter phone number without country calling code');
				}

				if ( ! ($data['phone_prefix']) ) {
				$validator->setError('phone_prefix', 'Country calling code Required');
				}
				
				list($country_id,$phone_prefix,$ndd_prefix) = explode('-',$data['phone_prefix']);
				unset($data['phone_prefix']);
				$data['contact_phone_parsed'] = $data['phone'] = preg_replace('/[^0-9]/', '', $data['phone']);
				$data['contact_phone_parsed'] = preg_replace('/^'.$ndd_prefix.'/', '', $data['contact_phone_parsed']);
				$data['contact_phone_parsed'] = '00'.$phone_prefix.$data['contact_phone_parsed'];

				$data['phone_country_id'] = intval($country_id);
				
				if ( !in_array(Cubix_Application::getId(), array(APP_A6, APP_A6_AT))) {
                    if (!(Cubix_Application::getId() == APP_ED && $this->user->type == "superadmin")){
                        $escort_model = new Model_EscortsV2();
                        $results = $escort_model->existsByPhone($data['contact_phone_parsed'],null, $data['id']);
                        $resCount = count($results);
                        if($resCount > 0) {
                            $validator->setError('phone', 'Phone already exists');
                        }
                    }
				}
			}

			$data['contact_phone_parsed_1'] = null;
			if ( $data['phone_1'] ) {
				if ( ! preg_match("/^[0-9\s\+\-\(\)]+$/i", $data['phone_1']) ) {
					$validator->setError('phone_1', 'Invalid phone number');
				}
				elseif(preg_match("/^(\+|00)/", $data['phone_1']) ) {
					$validator->setError('phone_1', 'Please enter phone number without country calling code');
				}
				if ( ! ($data['phone_prefix_1']) ) {
					$validator->setError('phone_prefix_1', 'Country calling code Required');
				}

				list($country_id, $phone_prefix, $ndd_prefix) = explode('-', $data['phone_prefix_1']);
				unset($data['phone_prefix_1']);
				$data['contact_phone_parsed_1'] = $data['phone_1'] = preg_replace('/[^0-9]/', '', $data['phone_1']);
				$data['contact_phone_parsed_1'] = preg_replace('/^' . $ndd_prefix . '/', '', $data['contact_phone_parsed_1']);
				$data['contact_phone_parsed_1'] = '00' . $phone_prefix.$data['contact_phone_parsed_1'];

				$data['phone_country_id_1'] = intval($country_id);

				if (!in_array(Cubix_Application::getId(), array(APP_A6, APP_A6_AT, APP_ED))) {
                    if (!(Cubix_Application::getId() == APP_ED && $this->user->type == "superadmin")){
                        $escort_model = new Model_EscortsV2();
                        $results = $escort_model->existsByPhone($data['contact_phone_parsed_1'], null, $data['id']);
                        $resCount = count($results);
                        if($resCount > 0) {
                            $validator->setError('phone_1', 'Phone already exists');
                        }
                    }
				}
			}

			$data['contact_phone_parsed_2'] = null;
			if ( $data['phone_2'] ) {
				if ( ! preg_match("/^[0-9\s\+\-\(\)]+$/i", $data['phone_2']) ) {
					$validator->setError('phone_2', 'Invalid phone number');
				}
				elseif(preg_match("/^(\+|00)/", $data['phone_2']) ) {
					$validator->setError('phone_2', 'Please enter phone number without country calling code');
				}
				if ( ! ($data['phone_prefix_2']) ) {
					$validator->setError('phone_prefix_2', 'Country calling code Required');
				}

				list($country_id, $phone_prefix, $ndd_prefix) = explode('-', $data['phone_prefix_2']);
				unset($data['phone_prefix_2']);
				$data['contact_phone_parsed_2'] = $data['phone_2'] = preg_replace('/[^0-9]/', '', $data['phone_2']);
				$data['contact_phone_parsed_2'] = preg_replace('/^' . $ndd_prefix . '/', '', $data['contact_phone_parsed_2']);
				$data['contact_phone_parsed_2'] = '00' . $phone_prefix.$data['contact_phone_parsed_2'];

				$data['phone_country_id_2'] = intval($country_id);

				if (!in_array(Cubix_Application::getId(), array(APP_A6, APP_A6_AT))) {
				    if (!(Cubix_Application::getId() == APP_ED && $this->user->type == "superadmin")){
                        $escort_model = new Model_EscortsV2();
                        $results = $escort_model->existsByPhone($data['contact_phone_parsed_2'], null, $data['id']);
                        $resCount = count($results);
                        if($resCount > 0) {
                            $validator->setError('phone_2', 'Phone already exists');
                        }
                    }
				}
			}

			if( ( $data['admin_verified'] == Model_Agencies::STATUS_VERIFIED || $data['admin_verified'] == Model_Agencies::STATUS_NOT_VERIFIED)  && $data['admin_verified'] != $data['admin_verified_current']){
				$escort_ids = $this->model->getEscorts($agency_id);
				if ( count($escort_ids) ) {
					$escort_ids_string = "";
					foreach($escort_ids as $e_id){
						$escort_ids_string .= $e_id->id.",";
					}
					$escort_ids_string = substr($escort_ids_string, 0, -1);
					$this->model->verifyEscorts($escort_ids_string, $data['admin_verified']);
				}
			}
			unset($data['admin_verified_current']);
			
			for ( $i = 1; $i <= 7; $i++ ) {
				if ( isset($data['work_days'][$i]) && ( ! @$data['work_times_from'][$i] || ! @$data['work_times_to'][$i] ) ) {
					$validator->setError('work_times_' . $i, '');
				}
			}
			
			$slug = '';
			$slug = $this->clear_string($data['agency_name']);
			
			$this->model->insertPairs($data['id'], $data['linked_agencies']);
			
			
			//Prices Start 
			$rates = $this->_validatePrices();
			$this->model->insertRates($data['id'], $rates);
						
			//Prices END 
			
			$save_data = array(
				'id'					=> $data['id'],
				'user_id'				=> $data['user_id'],
				'application_id' 		=> $data['application_id'],
				'sales_user_id'    		=> $data['sales_person_id'],
				'status' 	   			=> $data['status'],
				'name'       			=> $data['agency_name'],
				'display_name'       	=> strlen(trim($data['display_name'])) ? $data['display_name'] : $data['agency_name'],
				'slug'       			=> $slug,
				'country_id' 			=> $data['country_id'],
				'city_id'    			=> $data['city_id'],
				'email'      			=> $data['email'],
				'u_email'      			=> $data['u_email'],
				'web'		 			=> $data['web'],
				'phone_country_id'		=> $data['phone_country_id'],
				'phone'		 			=> $data['phone'],
				'contact_phone_parsed'	=> $data['contact_phone_parsed'],
				'phone_instructions' 	=> $data['phone_instructions'],
				'username' 				=> $data['login'],
				'password' 				=> $data['password'],
				'work_days'				=> $data['work_days'],
				'work_times_from'		=> $data['work_times_from'],
				'work_times_to'			=> $data['work_times_to'],
				'available_24_7'		=> $data['available_24_7'],
				'address'				=> $data['address'],
				'latitude'				=> $data['latitude'],
				'longitude'				=> $data['longitude'],
				'is_anonymous'			=> $data['is_anonymous'],
				'block_website'			=> $data['block_website'],
				'check_website'			=> $data['check_website'],
				'is_club'				=> $data['is_club'],
				'entrance'				=> $data['entrance'],
				'wellness'				=> $data['wellness'],
				'food'					=> $data['food'],
				'outdoor'				=> $data['outdoor'],
				'admin_verified'        => $data['admin_verified'],
				'filter_criteria'       => $data['filter_criteria'],
				'is_premium'			=> $data['is_premium'],
				'show_in_club_directory'=> $data['show_in_club_directory'],
				'phone_country_id_1'	=> $data['phone_country_id_1'],
				'phone_country_id_2'	=> $data['phone_country_id_2'],
				'phone_1'	 			=> $data['phone_1'],
				'phone_2'	 			=> $data['phone_2'],
				'contact_phone_parsed_1'=> $data['contact_phone_parsed_1'],
				'contact_phone_parsed_2'=> $data['contact_phone_parsed_2'],
				'phone_instructions_1' 	=> $data['phone_instructions_1'],
				'phone_instructions_2' 	=> $data['phone_instructions_2'],
				'is_anonymous_1'		=> $data['is_anonymous_1'],
				'is_anonymous_2'		=> $data['is_anonymous_2'],
				'phone_instr'		    => $data['phone_instr'],
				'phone_instr_1'		    => $data['phone_instr_1'],
				'phone_instr_2'		    => $data['phone_instr_2'],
				'viber'                 => $data['viber'],
				'whatsapp'              => $data['whatsapp'],
				'special_rates'         => $data['special_rates']
			);
			
			if ( Cubix_Application::getId() == 33 || Cubix_Application::getId() == 22 || Cubix_Application::getId() == APP_ED ) {
				$save_data['min_book_time'] = $data['min_book_time'];
				$save_data['min_book_time_unit'] = $data['min_book_time_unit'];
				$save_data['has_down_pay'] = $data['has_down_pay'];
				$save_data['down_pay'] = $data['down_pay'];
				$save_data['down_pay_cur'] = $data['down_pay_cur'];
				$save_data['reservation'] = $data['reservation'];
				
			}
			
			if ( Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT ) {
				$save_data = array_merge($save_data, array('fake_city_id' => $data['fake_city_id'], 'zip' => $data['zip']));
			}
			
			if ( Cubix_Application::getId() == APP_ED) {
				$save_data['langs'] = $data['langs'];
				$save_data['data_entry_id'] = $data['data_entry_id'];
				$save_data['about_services'] = $data['about_services'];
				$save_data['about_areas_covered'] = $data['about_areas_covered'];
				$save_data['wechat'] = $data['wechat'];
				$save_data['telegram'] = $data['telegram'];
				$save_data['ssignal'] = $data['ssignal'];
                $save_data['viber_1'] = $data['viber_1'];
                $save_data['whatsapp_1'] = $data['whatsapp_1'];
                $save_data['viber_2'] = $data['viber_2'];
                $save_data['whatsapp_2'] = $data['whatsapp_2'];

				$is_international = ($data['is_international']) ? $data['is_international'] : 0;
				$save_data = array_merge($save_data, array('is_international' => $is_international, 'premium_end_date' => $data['premium_end_date'], 'premium_days' => $data['premium_days'], 'agency_status' => $data['agency_status']));

			}

            if ( Cubix_Application::getId() == APP_EG_CO_UK) {
                $save_data['wechat'] = $data['wechat'];
                $save_data['telegram'] = $data['telegram'];
                $save_data['ssignal'] = $data['ssignal'];
            }

            if ( Cubix_Application::getId() == APP_A6) {
                $save_data = array_merge($save_data, array('agency_status' => $data['agency_status']));
            }

            if(in_array(Cubix_Application::getId(),array(APP_ED,APP_EG_CO_UK))) {
                if ( $data['updates_by'] )
                {
                    $save_data['updates_by'] = $data['updates_by'];
                    $m_agencies->addAdminComment($agency_id,0,'Update by was changed to( Update By Data Entry)',null,Zend_Auth::getInstance()->getIdentity()->id);
                }else{
                    $save_data['updates_by'] = 0;
                    $m_agencies->addAdminComment($agency_id,0,'Update by was changed to( Update By Myself)',null,Zend_Auth::getInstance()->getIdentity()->id);
                }
            }

			$i18n_data = Cubix_I18n::getValues($this->_request->getParams(), 'about', '');
				$security = new Cubix_Security();
				foreach ( $i18n_data as $field => $value ) {
					if ( Cubix_Application::getId() != APP_A6 && Cubix_Application::getId() != APP_A6_AT ) {
						$value = $security->clean_html($value);
					}
					else {
						$value = strip_tags($value);
					}
					$i18n_data[$field] = $security->xss_clean($value);
				}
			$save_data['about'] = $i18n_data;



			if($save_data['check_website'] > 0 && !Model_Agencies::getWebsiteChecked($save_data['id'])){
				$escort_data = array();
				$escort_data['check_website'] = 1;
				$escort_data['block_website'] = 0;
				$save_data['check_website_date'] = $escort_data['check_website_date'] = new Zend_Db_Expr('NOW()');
				$save_data['check_website_person'] = $escort_data['check_website_person'] = $b_user->username;
				$this->model->UpdateAgencyEscorts($save_data['id'],$escort_data);
			}

			if($save_data['block_website'] > 0 && !Model_Agencies::getWebsiteblocked($save_data['id'])){
				$escort_data = array();
				$escort_data['block_website'] = 1;
				$escort_data['check_website'] = 0;
				$save_data['block_website_date'] = $escort_data['block_website_date'] = new Zend_Db_Expr('NOW()');
				$save_data['block_website_person'] = $escort_data['block_website_person'] = $b_user->username;
				$this->model->UpdateAgencyEscorts($save_data['id'],$escort_data);
			}

			if ( $validator->isValid() ) {
				
				if((Cubix_Application::getId() == APP_EF)){
					if($data['password'] != ''){
						$client = Cubix_Api_XmlRpc_Client::getInstance();
						$client->call('Users.passChangeNotRequiredById', array($data['user_id']));
					}
				}

				$old_city_id = $this->model->getCityId($data['id']);
				
				if ($old_city_id != $data['city_id'])
					Cubix_CityAlertEvents::notify ($data['city_id'], NULL, $data['id']);
				
				$this->model->save(new Model_AgencyItem($save_data));

				$agency_escorts = $this->model->getEscorts($data['id']);

				$send_online_now_sms = $this->_request->send_online_now_sms;

				$data_entry_id = $save_data['data_entry_id'];

				$m_escort_v2 = new Model_EscortsV2();
				if ( $send_online_now_sms == "send_all" ) {
					foreach($agency_escorts as $escort) {
						$m_escort_v2->updateField($escort->id, 'send_online_now_sms', 1);
					}
				} elseif ( $send_online_now_sms == "dont_send_all" ) {
					foreach($agency_escorts as $escort) {
						$m_escort_v2->updateField($escort->id, 'send_online_now_sms', 0);
					}
				}

				if($data_entry_id) {
                    foreach($agency_escorts as $escort) {
                        $m_escort_v2->updateField($escort->id, 'data_entry_user', $data_entry_id);
                    }
                }


				/* APPLYING TO ALL ESCORTS */
				if( $data['phone_to_all_escorts'] == 1 || $data['phone_sms_verified'] == 1 ||
					$data['phone_to_all_escorts_1'] == 1 || $data['phone_to_all_escorts_2'] == 1 ||
					$data['email_to_all_escorts'] == 1 || $data['website_to_all_escorts'] == 1)
				{
					// echo (json_encode(array('status' => 'success' )));
					if ( in_array(Cubix_Application::getId(), array(APP_EG_CO_UK, APP_EF, APP_ED))  ) {
						 
						ignore_user_abort(true);
						ob_start();
						echo (json_encode(array('status' => 'success', 'a'=>'a' )));
						
						// usually it needs to be closed in this case it load forever in our server
						// header('Connection', 'close');
						// header('Content-Length: '.ob_get_length()); 
						  
						ob_end_flush();
						ob_flush();
						flush();

						session_write_close();
						fastcgi_finish_request();
					
						
					}

					$escort_data = array();

					if($data['phone_to_all_escorts'] == 1){
						$escort_data['phone_country_id']     = $save_data['phone_country_id'];
						$escort_data['phone_number']         = $save_data['phone'];
						$escort_data['contact_phone_parsed'] = $save_data['contact_phone_parsed'];
						$escort_data['phone_exists']         = $save_data['contact_phone_parsed'];
						$escort_data['phone_instr_other']    = $save_data['phone_instructions'];
                        if ( in_array(Cubix_Application::getId(), array(APP_ED,APP_EG_CO_UK))  ) {
                            $escort_data['viber'] = $save_data['viber'];
                            $escort_data['whatsapp'] = $save_data['whatsapp'];
                            $escort_data['telegram'] = $save_data['telegram'];
                            $escort_data['ssignal'] = $save_data['ssignal'];
                            $escort_data['wechat'] = $save_data['wechat'];
                        }
					}

					if($data['phone_sms_verified'] == 1){
						$escort_data['phone_sms_verified']    = $data['phone_sms_verified'];
					}	
				
					if($data['phone_to_all_escorts_1'] == 1){
						$escort_data['phone_country_id_1'] = $save_data['phone_country_id_1'];
						$escort_data['phone_additional_1'] = $save_data['phone_1'];
						$escort_data['phone_instr_1'] 	   = $save_data['phone_instr_1'];
						$escort_data['phone_instr_other_1']  = $save_data['phone_instructions_1'];
                        if ( in_array(Cubix_Application::getId(), array(APP_ED))  ) {
                            $escort_data['viber_1'] = $save_data['viber_1'];
                            $escort_data['whatsapp_1'] = $save_data['whatsapp_1'];
                        }
					}

					if($data['phone_to_all_escorts_2'] == 1){
						$escort_data['phone_country_id_2'] = $save_data['phone_country_id_2'];
						$escort_data['phone_additional_2'] = $save_data['phone_2'];
						$escort_data['phone_instr_2'] 	   = $save_data['phone_instr_2'];
						$escort_data['phone_instr_other_2']  = $save_data['phone_instructions_2'];
                        if ( in_array(Cubix_Application::getId(), array(APP_ED))  ) {
                            $escort_data['viber_2'] = $save_data['viber_2'];
                            $escort_data['whatsapp_2'] = $save_data['whatsapp_2'];
                        }
					}

					if($data['email_to_all_escorts'] == 1){
						$escort_data['email']     = $save_data['email'];
					}

					if($data['website_to_all_escorts'] == 1){
						$escort_data['website']     = $save_data['web'];
					}

					foreach( $agency_escorts as $escort ) {
						$this->model->UpdateEscortProfile($escort->id, $escort_data);
						Cubix_SyncNotifier::notify($escort->id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED);
					}
				}
				/* APPLYING TO ALL ESCORTS END */

				$allow_rev = $this->_request->allow_rev;
				if ($allow_rev) {
					if ($allow_rev == 'no')
						$v = 1;
					else
						$v = 0;

					$this->model->updateField($data['id'], 'disabled_reviews', $v);
					
					$escs = $this->model->getEscorts($data['id']);

					foreach ($escs as $e)
						Cubix_SyncNotifier::notify($e->id, Cubix_SyncNotifier::EVENT_ESCORT_REVIEW_TOGGLED, array('escort_id' => $e->id));
				}

				$allow_com = $this->_request->allow_com;
				if ($allow_com) {
					if ($allow_com == 'no')
						$v = 1;
					else
						$v = 0;

					$this->model->updateField($data['id'], 'disabled_comments', $v);
					
					$escs = $this->model->getEscorts($data['id']);

					foreach ($escs as $e)
						Cubix_SyncNotifier::notify($e->id, Cubix_SyncNotifier::EVENT_ESCORT_COMMENT_TOGGLED, array('escort_id' => $e->id));
				}

				
			}
			$this->view->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender();					
			echo (json_encode($validator->getStatus()));
		}
	}

	public function approveAction()
	{
		$agency_id = intval($this->_request->id);
		if ( ! $this->user->hasAccessToAgency($agency_id) ) die('Permission denied');
		$back_user = Zend_Auth::getInstance()->getIdentity();
		$this->view->back_user_id = $back_user->id;

		if( ! $this->_request->isPost() )
		{
			$data = $this->model->getById($agency_id);
			
			$this->view->last_ip = $this->model->getLastIP($agency_id);
			$this->view->last_login_date = $this->model->getLoginDate($agency_id);
			$this->view->ip_info = Model_Users::getUniqueIps($data['agency_data']->user_id);
			$this->view->agency_data = $ag = $data['agency_data'];
			$this->view->agency_logo = $ag->getLogoUrl();
			$this->view->agency_logo_orig = $ag->getLogoUrl('orig');
			$this->view->agency_langs  = $data['langs'];
			$this->view->working_hours = $data['working_hours'];
			$countyModel = new Model_Countries();
			$this->view->countries = $countyModel->getPhoneCountries();
			$this->view->phone_prefix_id = $ag->phone_country_id;
			$this->view->phone_prefix_id_1 = $ag->phone_country_id_1;
			$this->view->phone_prefix_id_2 = $ag->phone_country_id_2;
		}
		else{
			$this->model->setStatus($agency_id, AGENCY_STATUS_ONLINE);
			$validator = new Cubix_Validator();
			die(json_encode($validator->getStatus()));
		}
	}
	
    public function uploadhtml5Action()
    {
        $this->view->layout()->disableLayout();
        $this->view->logo_url = '';
        $this->view->agency_id = $agency_id = intval($this->_getParam('agency_id'));
        if (in_array(Cubix_Application::getId(), array(APP_A6, APP_ED))) {

            $model_agency = new Model_Agencies();
            if($agency_id){
                $agency_data = $model_agency->getById($agency_id);
                $agency = $agency_data['agency_data'];
                $this->view->logo_url = $agency->getLogoUrl();
            }
        }

        $this->_helper->viewRenderer->setRender('upload-logo');

    }
	
	public function uploadLogoAction()
	{
		$agency_id = intval($this->_getParam('agency_id'));
		if ( ! $this->user->hasAccessToAgency($agency_id) ) die('Permission denied');
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $cnf= Zend_Registry::get('videos_config');
        $tmp_dir = $cnf['temp_dir_path'] ? $cnf['temp_dir_path'] : sys_get_temp_dir();
        $config =  Zend_Registry::get('images_config');
        $requestMode = $this->_getParam('request_mode');

        $agency = $this->model->getById($agency_id);
        $agency = $agency['agency_data'];



        try {

            $headers = array();
            $headers['Content-Length'] = $_SERVER['CONTENT_LENGTH'];
            $headers['X-File-Id'] = $_SERVER['HTTP_X_FILE_ID'];
            $headers['X-File-Name'] = $_SERVER['HTTP_X_FILE_NAME'];
            $headers['X-File-Resume'] = $_SERVER['HTTP_X_FILE_RESUME'];
            $headers['X-File-Size'] = $_SERVER['HTTP_X_FILE_SIZE'];

            $response = array();
            $response['id'] = $headers['X-File-Id'];
            $response['name'] = basename($headers['X-File-Name']); 	// Basename for security issues
            $response['size'] = $headers['Content-Length'];
            $response['error'] = UPLOAD_ERR_OK;
            $response['finish'] = FALSE;

            // Is resume?
            $flag = (bool)$headers['X-File-Resume'] ? FILE_APPEND : 0;
            $filename = $response['id'].'_'.$response['name'];
            $response['upload_name'] = $filename;
            $noValidation = false;
            // Write file
            $file = $tmp_dir.DIRECTORY_SEPARATOR.$response['upload_name'];
            if ($requestMode == 'copy-listener'){
                $noValidation = true;
                $tmpFile = $this->_getParam('file_name');
                if (empty($tmpFile) || !is_file($tmpFile) || filesize($tmpFile) < 1){
                    echo json_encode(array(
                        'finish' => 0,
                        'error' => 'File not found or empty'
                    ));
                    die;
                }
                $content = file_get_contents($tmpFile);
            }else{
                $content = file_get_contents('php://input');
            }

            if (file_put_contents($file, $content, $flag) === FALSE){
                $response['error'] = UPLOAD_ERR_CANT_WRITE;
            }
            else
            {

                $response['file_size'] = filesize($file);
                $response['X-File-Size'] = $headers['X-File-Size'];
                if (filesize($file) == $headers['X-File-Size'])
                {

                    $response['tmp_name'] = $file;
                    $response['finish'] = TRUE;
                }
            }
            if($response['error'] == 0 && $response['finish']){
                $error = false;
                $extension = end(explode(".", $response['name']));

                if (!in_array($extension, $config['allowedExts']  ) )
                {
                    $error = ' Not allowed file extension.';
                }

                if ($error) {
                    @unlink($file);
                    echo json_encode(array(
                        'finish' => 0,
                        'error' => $error
                    ));
                    die;
                } else {
                    // Save on remote storage

                    $images = new Cubix_Images();
                    $ext = strtolower(@end(explode('.', $response['name'])));
                    $image = $images->save($file, 'agencies', $agency->application_id, $ext, null, null, $noValidation);

                    if ( ! isset($image['hash']) ) {
                        @unlink($file);
                        echo json_encode(array(
                            'finish' => 0,
                            'error' => "Photo upload failed. Please try again!"
                        ));
                        die;

                    }
                    $agency->logo_hash = $image['hash'];
                    $agency->logo_ext = $image['ext'];


                    $this->model->setLogo($agency->id, $image);
                    $response['logo_url'] = $agency->getLogoUrl();

                    @unlink($file);
                    echo json_encode(array(
                        'finish' => 1,
                        'logo_url' => $agency->getLogoUrl(),
                        'error' => 0
                    ));
                    die;
                }
            }
            $this->model->showResponse($response);
            die;
        } catch ( Exception $e ) {
            echo json_encode(array(
                'finish' => 0,
                'error' => $e->getMessage()
            ));
            die;
        }


	}

	public function deleteLogoAction(){
        $this->view->layout()->disableLayout();
       $response = false;
        if ( $this->_request->agency_id ) {
            $agency_id = intval($this->_request->agency_id);
            $this->model->deleteLogo($agency_id);

        }
        echo $agency_id;die;
    }

//    public function uploadLogoAction()
//    {
//
//            $this->view->layout()->disableLayout();
//            $this->_helper->viewRenderer->setNoRender();
//
//            $agency_id = intval($this->_getParam('agency_id'));
////		var_dump(gettype($agency_id));die;
//
//            try {
//                $agency = $this->model->getById($agency_id);
//                $agency = $agency['agency_data'];
//
//                if ( ! $agency ) {
//                    throw new Exception('Wrong agency id was specified');
//                }
//
//                // Save on remote storage
//                $images = new Cubix_Images();
//                $image = $images->save($_FILES['Filedata']['tmp_name'], 'agencies', $agency->application_id, strtolower(@end(explode('.', $_FILES['Filedata']['name']))));
//
//                $agency->logo_hash = $image['hash'];
//                $agency->logo_ext = $image['ext'];
//
//                $this->model->setLogo($agency->id, $image);
//                $result = array(
//                    'status' => 'success',
//                    'logo_url' => $agency->getLogoUrl()
//                );
//            } catch (Exception $e) {
//                $result = array('status' => 'error', 'msg' => '');
//
//                $result['msg'] = $e->getMessage();
//            }
//
//            echo json_encode($result);
//
//    }

	public function deleteAction()
	{
		if ( $this->_request->agency_id ) {
			$agency_ids = array(intval($this->_request->agency_id));
		}
		else if ( $this->_request->id ) {
			$agency_ids = $this->_request->id;
		}

		if ( count($agency_ids) > 0 ) {
			foreach ( $agency_ids as $agency_id ) {
				//var_dump($agency_id);
				$this->model->delete($agency_id);
			}
		}	

		die;
	}
	
	public function escortListAction()
	{
		$this->view->layout()->disableLayout();
		$this->view->agency_id = $this->_request->agency_id; 
	}
	
	public function escortListDataAction()
	{
		$this->view->layout()->disableLayout();

		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		$req = $this->_request;
		
		$filter = array(
			// Personal
			'e.showname' => $req->showname,
			'ep.contact_phone_parsed' => $req->phone_parsed,
			'e.agency_id = ?' => $req->agency_id,
			// Status
			'e.status' => $req->status,
			'excl_status' => Model_Escorts::ESCORT_STATUS_DELETED
		);
		
		$model = new Model_EscortsV2();
		$data = $model->getForAgencies(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		$DEFINITIONS = Zend_Registry::get('defines');


		foreach ($data as $d)
		{
			
			$d['status'] = $DEFINITIONS['status_options'][$d['status']];
			$stat = array();
			foreach( $DEFINITIONS['escort_status_options'] as $key => $e_status )
			{
				if ( $model->hasStatusBit($d['id'], $key) )
				{
					$stat[] = $e_status;
				}
			}

			$d['status'] = implode(', ', $stat);
			
			
			$d['movable_package'] = true;


			if ( ! $d['premium_package'] || (Cubix_Application::getId() == APP_A6 && ! in_array($d['package_id'], array(12, 14, 19, 20, 11, 18, 22, 23, 24, 25))  ) || ($bu_user->type != 'superadmin' && Cubix_Application::getId() == APP_A6) ) {
				$d['movable_package'] = false;
			}
			
			//Default packages should be movable ob Bl. Katie
			if ( Cubix_Application::getId() == APP_BL && $d['package_id'] == 9 ) {
				$d['movable_package'] = true;
			}
			
			if ( $d['premium_package'] && Cubix_Application::getId() == APP_A6 && in_array($d['package_id'], array(12, 14, 23)) && $bu_user->type == 'sales manager' ) {
				$d['movable_package'] = true;
			}
			

			if(Cubix_Application::getId() == APP_ED && $d['package_id'] == 128){
				$d['movable_package'] = true;
			}

			if(Cubix_Application::getId() == APP_EG_CO_UK && $d['package_id'] == 106){
				$d['movable_package'] = true;
			}
			
			
			//$d->expiration_date -= 24*60*60;
			$d->expiration_date = $d->expiration_date ? date("d M Y", strtotime('-1 day', $d->expiration_date)) : null; //-1 day		
		}
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
		
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		die(json_encode($this->view->data));
		
	}

	public function ordersDataAction()
	{
		$this->view->layout()->disableLayout();

		$m_orders = new Model_Billing_Orders();
		$req = $this->_request;

		$data = new Cubix_Form_Data($this->_request);

		$data->setFields(array(
			'page' => 'int',
			'per_page' => 'int',
			'sort_field' => '',
			'sort_dir' => '',
			'user_id' => ''
		));

		$data = $data->getData();

		$filter = array();

		if ( strlen($data['user_id']) > 0 ) {
			$filter['u.id = ?'] = $data['user_id'];
		}

		$data = $m_orders->getAll(
			$filter,
			$req->page,
			$req->per_page,
			$req->sort_field,
			$req->sort_dir
		);

		die(json_encode($data));
	}

	public function salesWorkAction()
	{
		$this->view->layout()->disableLayout();

		$req = $this->_request;
		$this->view->user_id = $user_id = intval($req->user_id);

		$agency = $this->model->getByUserId($user_id);
		$this->view->agency = $agency;
		//$active_packages = $agency->getActivePackages();

		$this->view->total_orders_price = $agency->getTotalOrdersPrice();
		$this->view->total_orders_paid = $agency->getTotalOrdersPrice(true);


		$filter = array(
			'e.agency_id' => $agency->id
		);

		$count = '';
		$model = new Model_Escorts();
		$this->view->agency_escorts = $agency_escorts = $model->getAll(
			1,
			1000,
			$filter,
			'e.showname',
			'ASC',
			$count
		);

		if ( count($agency_escorts) > 0 ) {
			$this->view->first_escort_id = $agency_escorts[0]->id;
		}

		$this->view->active_packages = array('data' => array());
	}

	public function activePackagesDataAction()
	{
		$req = $this->_request;
		$escort_id = intval($req->escort_id);

		if ( ! $escort_id ) {
			die(json_encode(array('data' => array())));
		}

		$m_escort = new Model_Escorts();
		$escort = $m_escort->get($escort_id);
		$active_packages = $escort->getActivePackages();

		$m_o_packages = new Model_Billing_Packages();
		$act_packages = array();
		if ( count($active_packages) > 0 )
		{
			foreach ( $active_packages as $k => $active_package )
			{
				$package = $m_o_packages->get($active_package->id);

				$products = $package->getProducts();
				$opt_products = $package->getProducts(Model_Products::TYPE_OPTIONAL_PRODUCT);
				if ( count($products) > 0 ) {
					foreach( $products as $k => $product )
					{
						$products[$k]->name = Cubix_I18n::translate($product->name);
					}
				}

				if ( count($opt_products) > 0 ) {
					foreach( $opt_products as $k => $product )
					{
						$opt_products[$k]->name = Cubix_I18n::translate($product->name);
					}
				}

				$active_package->package_products = $products;
				$active_package->package_opt_products = $opt_products;

				$act_packages[] = $active_package;
			}
		}
		die(json_encode(array('data' => $act_packages)));
		$this->view->active_packages = array('data' => $act_packages);
	}

	public function getOrdersTotalAction()
	{
		$this->view->layout()->disableLayout();

		$req = $this->_request;
		$agency_id = intval($req->agency_id);

		$m_agency = new Model_Agencies();

		$agency = $m_agency->getById($agency_id);

		$data = array(
			'total_orders_price' => $agency['agency_data']->getTotalOrdersPrice(),
			'total_orders_paid' => $agency['agency_data']->getTotalOrdersPrice(true)
		);

		die(json_encode($data));
	}



    public function escortActivePackageAction()
	{
        $this->view->layout()->disableLayout();
        $escort_id  = $this->_request->getParam('id');

        $includeDefaultPackage = false;
        if (in_array(Cubix_Application::getId(), [APP_BL, APP_ED, APP_EG_CO_UK])) $includeDefaultPackage = true;

        $this->view->package = Cubix_Api::getInstance()->call('getEscortActivePackage', array($escort_id,Cubix_I18n::getLang(), $includeDefaultPackage));
	}

    public function ajaxEscortsAction()
	{

        $agency_id = intval($this->_getParam('agency_id'));

		$escorts = Cubix_Api::getInstance()->call('premium_getLinkedAgencyNonPremiumEscorts', array($agency_id));

		die(json_encode($escorts));
	}


	public function ajaxEscortCitiesAction()
	{


		$escort_id = intval($this->_getParam('escort_id'));


		$cities = Cubix_Api::getInstance()->call('premium_getEscortCities', array($escort_id,Cubix_I18n::getLang()));
		$limit = Cubix_Api::getInstance()->call('premium_getEscortCitiesLimit', array($escort_id));

		die(json_encode(array('cities' => $cities, 'limit' => $limit)));
	}


    public function ajaxPremiumSwitchAction()
	{


		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'from_escort_id' => 'int-nz',
			'to_escort_id' => 'int-nz',
			'city_ids' => 'arr-int'
		));
		$data = $form->getData();


		$client = Cubix_Api::getInstance();

        $methodToCall = 'premium_switchActivePackages';
        if (Cubix_Application::getId() == APP_ED) $methodToCall = 'premium_switchActivePackagesForED';

		$result = $client->call($methodToCall, array($data['from_escort_id'], $data['to_escort_id'], $data['city_ids']));
		die(json_encode(array('status' => 'success', 'resp'=>$result)));//'data' => $this->_getPremiumList(), 
	}

	public function toggleReviewAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		
		if ( ! $this->user->hasAccessToAgency($id) ) die('Permission denied');
		$this->model->toggleReview($id);
		
		$escs = $this->model->getEscorts($id);
		
		foreach ($escs as $e)
			Cubix_SyncNotifier::notify($e->id, Cubix_SyncNotifier::EVENT_ESCORT_REVIEW_TOGGLED, array('escort_id' => $e->id));
		
		die;
	}
	
	public function toggleCommentAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		
		if ( ! $this->user->hasAccessToAgency($id) ) die('Permission denied');
		$this->model->toggleComment($id);
		
		$escs = $this->model->getEscorts($id);
		
		foreach ($escs as $e)
			Cubix_SyncNotifier::notify($e->id, Cubix_SyncNotifier::EVENT_ESCORT_COMMENT_TOGGLED, array('escort_id' => $e->id));
		
		die;
	}

	public function disableAllEscortsAction()
	{
		$id = intval($this->_request->agency_id);
		if ( ! $id ) die;
		$this->model->disableAllEscorts($id);
		$escs = $this->model->getEscorts($id);

		foreach ($escs as $e)
			Cubix_SyncNotifier::notify($e->id, Cubix_SyncNotifier::EVENT_ESCORT_STATUS_UPDATED, array('escort_id' => $e->id));

		die;
	}

	public function disableAgenciesAllEscortsAction()
    {
        $ids = $this->_request->id;
        if ( count( $ids ) == 0 ) die;
        $result = $this->model->disableAgenciesAllEscorts($ids);

        foreach ($result as $item)
        {
            Cubix_SyncNotifier::notifyAgency($item->user_id, $item->agency_id, "Agency's all escorts disabled!");
        }

        die;
    }
	
	public function getAgencyContactDataAction()
	{
		$agency_id = (int) $this->_request->agency_id;
		
		if ( ! $agency_id ) return;
		
		$agency = new Model_Agencies();
		$contacts = $agency->getAgencyContactData($agency_id, Cubix_Application::getId());
		
		die(json_encode($contacts));
	}
	
	public function commentsAction()
	{
		$req = $this->_request;

		$filter = array(
			'id' => $req->agency_id
		);
		
		$data = $this->model->getComments(
			$this->_request->page,
			$this->_request->per_page,
			$filter,
			$this->_request->sort_field,
			$this->_request->sort_dir,
			$count
		);
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
	
	public function removeCommentAction()
	{
		$id = intval($this->_request->id);
		$this->model->removeComment($id);
		
		die;
	}
	
	public function toggleClubCommentAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->model->toggleClubComment($id);
		
		die;
	}
	
	public function uploadPhotosAction()
	{
		$agency_id = intval($this->_getParam('agency_id'));

        if ( ! $this->user->hasAccessToAgency($agency_id) ) die('Permission denied');
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $cnf= Zend_Registry::get('videos_config');
        $tmp_dir = $cnf['temp_dir_path'] ? $cnf['temp_dir_path'] : sys_get_temp_dir();
        $config =  Zend_Registry::get('images_config');


        try {
			$agency = $this->model->getById($agency_id);
			$agency = $agency['agency_data'];
			
			if ( ! $agency ) {
				throw new Exception('Wrong agency id was specified');
			}
			
			// Save on remote storage

            $headers = array();
            $headers['Content-Length'] = $_SERVER['CONTENT_LENGTH'];
            $headers['X-File-Id'] = $_SERVER['HTTP_X_FILE_ID'];
            $headers['X-File-Name'] = $_SERVER['HTTP_X_FILE_NAME'];
            $headers['X-File-Resume'] = $_SERVER['HTTP_X_FILE_RESUME'];
            $headers['X-File-Size'] = $_SERVER['HTTP_X_FILE_SIZE'];

            $response = array();
            $response['id'] = $headers['X-File-Id'];
            $response['name'] = basename($headers['X-File-Name']); 	// Basename for security issues
            $response['size'] = $headers['Content-Length'];
            $response['error'] = UPLOAD_ERR_OK;
            $response['finish'] = FALSE;

            // Is resume?
            $flag = (bool)$headers['X-File-Resume'] ? FILE_APPEND : 0;
            $filename = $response['id'].'_'.$response['name'];


            $response['upload_name'] = $filename;
            $noValidation = false;
            // Write file
            $file = $tmp_dir.DIRECTORY_SEPARATOR.$response['upload_name'];



            $content = file_get_contents('php://input');

            if (file_put_contents($file, $content, $flag) === FALSE){
                $response['error'] = UPLOAD_ERR_CANT_WRITE;
            }
            else
            {
                $response['file_size'] = filesize($file);
                $response['X-File-Size'] = $headers['X-File-Size'];
                if (filesize($file) == $headers['X-File-Size'])
                {
                    $response['tmp_name'] = $file;
                    $response['finish'] = TRUE;
                }
            }

            if($response['error'] == 0 && $response['finish']){
                $error = false;
                $extension = end(explode(".", $response['name']));

                if (!in_array($extension, $config['allowedExts']  ) )
                {
                    $error = ' Not allowed file extension.';
                }

                if ($error) {
                    @unlink($file);
                    $result = array('status' => 'error', 'msg' => 'ERrrrrrrrrrrrooorrrrrrrrrrrr');

                    $result['msg'] = 'error';
                    echo json_encode($result);
                    die;
                } else {
                    // Save on remote storage
                    $images = new Cubix_Images();
                    $ext = strtolower(@end(explode('.', $response['name'])));
                    $image = $images->save($file, 'agencies', $agency->application_id, $ext, $agency->id);
                    $this->model->addPhoto($agency->id, $image);
                    $image = new Cubix_Images_Entry($image);
                    $image->setCatalogId('agencies');
                    $image_url = $images->getAgencyUrl($image, $agency->id);

                    echo json_encode(array(
                        'status' => 'success',
                        'finish' => 1,
                        'error' => 0
                    ));
                    die;
                }
            }





		} catch (Exception $e) {
			$result = array('status' => 'error', 'msg' => 'ERrrrrrrrrrrrooorrrrrrrrrrrr');
			
			$result['msg'] = $e->getMessage();
		}
		
		echo json_encode($result);
	}
	
	public function getPhotosAction()
	{
		$this->view->layout()->disableLayout();
				
		$agency_id = $this->_request->agency_id;
		$photos = $this->model->getPhotos($agency_id);
		$images = new Cubix_Images();
		
		foreach ($photos as $photo)
		{
			$image = array('hash' => $photo->hash, 'ext' => $photo->ext);
			$image = new Cubix_Images_Entry($image);
			$image->setCatalogId('agencies');
			$image_url = $images->getAgencyUrl($image, $agency_id);
			$photo->image_url = $image_url;
		}
		
		$this->view->photos = $photos;
	}
	
	public function removePhotosAction()
	{
		$this->view->layout()->disableLayout();
		
		$ids = $this->_request->ids;
		
		$this->model->removePhotos($ids);
		die;
	}
	
	public function autoApprovalAction()
	{
		$this->view->layout()->disableLayout();
		
		$auto_approval = intval($this->_request->auto_approval);
		$id = intval($this->_request->id);
		
		$this->model->setAutoApproval($id, $auto_approval);
		
		die;
	}

	public function ajaxApplyEmailToAllEscortsAction()
	{
		$agency_id = (int) $this->_request->agency_id;
		$email = $this->_request->email;

		$validator = new Cubix_Validator();

		$agency_escorts = $this->model->getEscorts($agency_id);
		$escort_data = array();
		
		if ( ! strlen($email) ) {
			$validator->setError('email', 'Required');
		}
		elseif ( ! $validator->isValidEmail($email) ) {
			$validator->setError('email', 'Invalid Email');
		}
		
		if ( $validator->isValid() ) {
			$escort_data['email'] = $email;
			
			
			foreach($agency_escorts as $escort){
				$this->model->UpdateEscortProfile($escort->id, $escort_data);
				Cubix_SyncNotifier::notify($escort->id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED);
			}
		}

		die(json_encode($validator->getStatus()));
	}

	public function ajaxApplyWebsiteToAllEscortsAction()
	{
		$agency_id = (int) $this->_request->agency_id;
		$website = $this->_request->website;

		$validator = new Cubix_Validator();

		$agency_escorts = $this->model->getEscorts($agency_id);
		$escort_data = array();
		
		if ( ! strlen($website) ) {
			$validator->setError('website', 'Required');
		}		
		
		if ( $validator->isValid() ) {
			$escort_data['website'] = $website;
			
			
			foreach($agency_escorts as $escort){
				$this->model->UpdateEscortProfile($escort->id, $escort_data);
				Cubix_SyncNotifier::notify($escort->id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED);
			}
		}

		die(json_encode($validator->getStatus()));
	}
	
	private function _reconstructRates($data)
	{
		$result = array('incall' => array(), 'outcall' => array(), 'travel' => array());
		if($data){
			foreach ( $data as $rate ) {
				if ( $rate['availability'] == 1 ) {
					$key = 'incall';
				}
				elseif ( $rate['availability'] == 2 ) {
					$key = 'outcall';
				}
				elseif ( $rate['availability'] == 0 ) {
					$key = 'travel';
				}

				if ( $rate['type'] ) {
					$result[$key][] = array('type' => $rate['type'], 'price' => $rate['price'], 'hh_price' => $rate['hh_price'], 'currency' => $rate['currency_id'], 'rate_id' => $rate['id'], 'taxi' => $rate['plus_taxi']);
				}
				else {
					$result[$key][] = array('time' => $rate['time'], 'unit' => $rate['time_unit'], 'price' => $rate['price'], 'hh_price' => $rate['hh_price'], 'currency' => $rate['currency_id'], 'rate_id' => $rate['id'], 'taxi' => $rate['plus_taxi']);
				}
			}
		}

		return $result;
	}
	
	protected function _validatePrices()
	{
		$rates = $this->_getParam('rates');

		if ( ! is_array($rates) ) {
			$rates = array();
		}

		$data = array('rates' => array());

		$DEFINITIONS = Zend_Registry::get('defines');

		//$currencies = array_keys($DEFINITIONS['currencies']);
		$currencies = array_keys(Model_Currencies::getAllAssoc());
		
		$units = array_keys($DEFINITIONS['time_unit_options']);
		foreach ( $rates as $availability => $_rates ) {
			if ( $availability == 'incall' ) { $availability = 1; $types = array('additional-hour', 'overnight', 'dinner-date', 'weekend'); }
			elseif ( $availability == 'outcall' ) { $availability = 2; $types = array('additional-hour', 'overnight', 'dinner-date', 'weekend'); }
			elseif ( $availability == 'travel' ) { $availability = 0; $types = array('weekend'); }
			else continue;

			foreach ( $_rates as $rate ) {
				if ( get_magic_quotes_gpc() ) $rate = stripslashes($rate);
				$rate = @json_decode($rate);

				if ( is_object($rate) ) $rate = (array) $rate;
				if ( $rate === false || ! is_array($rate) ) {
					continue;
				}

				// Case when rate is standart and some data are not valid
				if ( ! isset($rate['type']) && ( ! isset($rate['time']) || ! isset($rate['unit']) || ! isset($rate['price']) || ! isset($rate['currency']) ) ) {
					continue;
				}
				// Case when rate is a type of custom and some data are not valid
				elseif ( isset($rate['type']) && ( ! isset($rate['price']) || ! isset($rate['currency']) || ! in_array($rate['type'], $types) ) ) {
					continue;
				}

				// If price is invalid
				$price = intval($rate['price']);
				if ( $price <= 0 ) continue;

				// If currency is invalid
				$currency = intval($rate['currency']);
				if ( ! in_array($currency, $currencies) ) {
					continue;
				}
				// If plus taxi exced
				$taxi = intval($rate['taxi']);
				if($taxi !=1 ){
					$taxi = null;
				}
				// If rate is custom validate data add only type, price and currency fields
				if ( ! isset($rate['type']) ) {
					$time = intval($rate['time']);
					if ( $time <= 0 ) { continue; }

					$unit = intval($rate['unit']);
					if ( ! in_array($unit, $units) ) { continue; };
					
					
					$data['rates'][] = array('availability' => $availability, 'time' => $time, 'time_unit' => $unit, 'price' => $price, 'currency_id' => $currency, 'plus_taxi'=> $taxi);
					
				}
				// Otherwize add also time and time unit
				else {
					$data['rates'][] = array('availability' => $availability, 'type' => $rate['type'], 'price' => $price, 'currency_id' => $currency, 'plus_taxi'=> $taxi, 'plus_taxi'=> $taxi);
				}
			}
		}
		$travel_pay_methods = $this->_getParam('travel_payment_method');
		$travel_other_method = $this->_getParam('travel_other_method');
		$data['travel_payment_methods'][] = $travel_pay_methods;
		$data['travel_other_method'][] = $travel_other_method;
		
		return $data;
	}
	
	public function ajaxGetRatesAction()
	{
		$agency_id = $this->_request->agency_id;
		$m_agencies = new Model_Agencies();
		$rates = $this->_reconstructRates($m_agencies->getRates($agency_id));
		$field_data = $m_agencies->getRatefields($agency_id);
		
		$rates['special_rates'] = $field_data->special_rates;
		if ( Cubix_Application::getId() == 33 || Cubix_Application::getId() == 22 || Cubix_Application::getId() == APP_ED ) {
			$rates['min_book_time'] = $field_data->min_book_time;
			$rates['min_book_time_unit'] = $field_data->min_book_time_unit;
			$rates['has_down_pay'] = $field_data->has_down_pay;
			$rates['down_pay'] = $field_data->down_pay;
			$rates['down_pay_cur'] = $field_data->down_pay_cur;
			$rates['reservation'] = $field_data->reservation;
			
			$rates['travel_payment_methods'] = $m_agencies->getTravelPaymentMethods($agency_id);
		}
		
		die(json_encode($rates));
	}


    public function exportAgenciesAction()
    {
        set_time_limit(0);
        ini_set('memory_limit', '2048M');



        $bu_user = Zend_Auth::getInstance()->getIdentity();

        $filter = array();
        $filter['u.status IN (-4,1)'] = true;
        $filter['u.sales_user_id = ?'] = $bu_user->id;
//        var_dump($this->_getParam('status'));die;
        $headers = array('agency_id', 'escort_id', 'country', 'username', 'status', 'registration_date', 'last_login_date', 'email',
            'phone_number', 'first_sms', 'last_sms', 'first_email', 'last_email', 'comment', 'commented_by', 'last_check_date',
             'last_package', 'active_package_name', 'package_expiration_date');



        ignore_user_abort(true);
        ob_start();

        echo (json_encode(array('status' => 'success')));

        ob_end_flush();
        ob_flush();
        flush();

        session_write_close();
        fastcgi_finish_request();

        $filename = 'agencies_' . hexdec(uniqid()) . '.csv';
        $handle = fopen('escindependents/' . $filename, 'w');

        $data = $this->model->getDataForAgencyExport(
            $filter
        );



        fputcsv($handle, $headers);



        foreach ($data['data'] as $fld) {
            $arr = array(
                $fld['agency_id'], $fld['escort_id'], $fld['country'], $fld['username'], $fld['agency_status'], $fld['registration_date'], $fld['last_login_date'], $fld['email'],
                $fld['phone_number'], $fld['first_sms'], $fld['last_sms'], $fld['first_email'], $fld['last_email'], $fld['comment'], $fld['commented_by'], $fld['last_check_date'],
                 $fld['last_package'], $fld['active_package_name'], $fld['package_expiration_date']
            );


            fputcsv($handle, $arr);
        }

        fclose($handle);
        $cache_key = 'escindep_data_download_' . $bu_user->id;
        $cache = Zend_Registry::get('cache');
        if ( ! $cache->load($cache_key) )
        {
            $cache->save($filename, $cache_key, array(), 3600);
        }

        exit;





    }

    public function checkCsvStatusAction(){
        $this->view->layout()->disableLayout();

        $bu_user = Zend_Auth::getInstance()->getIdentity();
        $cache_key = 'escindep_data_download_' . $bu_user->id;
        $cache = Zend_Registry::get('cache');

        if ($cache->load($cache_key))
        {
            echo (json_encode(array('status' => true)));
            die;
        }
        echo (json_encode(array('status' => false)));
        die;
    }

    public function downloadCsvAction() {
        $this->view->layout()->disableLayout();

        $bu_user = Zend_Auth::getInstance()->getIdentity();
        $cache_key = 'escindep_data_download_' . $bu_user->id;
        $cache = Zend_Registry::get('cache');
        if($filename = $cache->load($cache_key)) {
            $cache->remove($cache_key);

            if (file_exists('escindependents/' . $filename)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="'.basename('escindependents/' . $filename).'"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize('escindependents/' . $filename));
                readfile('escindependents/' . $filename);

                unlink('escindependents/' . $filename);
            }
        }

        exit;
    }

    public function inactiveAllEscortsAction()
    {
        $id = intval($this->_request->agency_id);
        if ( ! $id ) die;
        $this->model->inactiveAllEscorts($id);
        $escs = $this->model->getEscorts($id);

        foreach ($escs as $e)
            Cubix_SyncNotifier::notify($e->id, Cubix_SyncNotifier::EVENT_ESCORT_STATUS_UPDATED, array('escort_id' => $e->id));

        die;
	}
	
	public function disableAction()
	{
		$ids = $this->_request->id;
		if(empty($ids))	 die;
		
		$result = $this->model->disableAgencies($ids);
		foreach ($result as $item)
		{
            Cubix_SyncNotifier::notifyAgency($item->user_id, $item->agency_id, 'Agency disabled!');
		}

        die;
	}

    public function ajaxLoadHandVerificationAction()
    {
        $this->view->layout()->disableLayout();
        $agency_id = (int)$this->_request->agency_id;

        $data = $this->model->getHandVerificationDate($agency_id);
        if($data->hand_verification_sales_id === -1){
            $data->sales_name = 'by system';
        }
        $this->view->data = $data;

    }

    public function ajaxUpdateHandVerificationDateAction()
    {
        $this->view->layout()->disableLayout();
        $agency_id = (int)$this->_request->agency_id;
        $ids = $this->_request->id;

        if ( $agency_id ) {
            $this->model->updateHandVerificationDate($agency_id);


            Cubix_SyncNotifier::notify($agency_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array());
        } else {
            if ( count($ids) ) {
                foreach ($ids as $id) {
                    $this->model->updateHandVerificationDate($id);


                    Cubix_SyncNotifier::notify($id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array());
                }
            }
        }

        die;
    }

    public function assignToAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$user_id = $this->view->user_id = $req->user_id;
		$ids = $this->view->ids = $this->_request->ids;
		$multiple = $this->view->multiple = isset($req->multiple) && $req->multiple == 1;
		
		if ( $req->isPost() ) {
			$sales_id = $req->sales_id;
			$validator = new Cubix_Validator();
			if ($multiple){
                $agency_ids= explode(',', $ids);

			    foreach ($agency_ids as $agency_id){
			    	
			    	$this->model->updateAgencySales($sales_id,$agency_id);
                }
            }
			die(json_encode($validator->getStatus()));
		}
		else {
		    if ($multiple){
                $this->view->ids =implode(',',$req->id);
            }
			$bu_model = new Model_BOUsers();
			$this->view->sales_persons = $bu_model->getAllSales();
		}
	}


    public function addAdminCommentAction()
    {
        $this->view->layout()->disableLayout();
        $req = $this->_request;

        $id = $this->view->id = $req->id;

        if ( $req->isPost() ) {
            $admin_verified = intval($req->admin_verified);
            $due_date = $req->due_date;
            $free_comment = $req->free_comment;

            foreach ($id as $i)
                $this->model->addAdminComment($i, $admin_verified, $free_comment, $due_date, $this->user->id);
        }
    }

    public function updatesByToggleAction()
    {
        $id = intval($this->_request->id);
        if ( ! $id ) die;
        $m_agencies = new Model_Agencies();

        $res = $this->model->toggleUpdatesBy($id);
        $m_agencies->addAdminComment($id,0,'Update by was changed by toggle, From backend listing.',null,Zend_Auth::getInstance()->getIdentity()->id);

        echo json_encode(array(
            'data' => $res,
        ));

        exit;
    }
}
