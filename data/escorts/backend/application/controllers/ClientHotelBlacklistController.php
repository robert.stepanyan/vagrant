<?php

class ClientHotelBlacklistController extends Zend_Controller_Action {

	public function init()
	{
		$this->model = new Model_ClientHotelBlacklist();
	}
	
	public function indexAction() 
	{
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('default_client_blacklist_data');
		$this->view->filter_params = $ses_filter->filter_params;
		/* FILTER PARAMS */
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'username' => $req->username,
			'type' => $req->type,
			'name' => $req->name,
			'phone' => $req->phone,
			'comment' => $req->comment,
			'status' => $req->status
		);
		
		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		if ( count($data) ) {
			foreach ( $data as $i => $d ) {
				if($d->type != 'clients')
					continue;
	
				$p = 0;
				$images = $this->model->getImages($d->bl_id);
				if($images){
					foreach ($images as $image) {
						$img = new Cubix_ImagesCommonItem($image);
						$data[$i]->images[$p]['thumb'] = $img->getUrl('lvthumb');
						$data[$i]->images[$p]['original'] = $img->getUrl('orig');
						$p++;
					}

				}
			}
		}
		/*$DEFINITIONS = Zend_Registry::get('defines');
		
		foreach ( $data as $i => $item ) {
			$data[$i]->status = $DEFINITIONS['comment_status_options'][$item->status];
			$data[$i]->message = mb_substr($item->message, 0, 50, 'UTF-8') . ' ...';
		}*/
		
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

	public function createAction()
	{
		if( ! $this->_request->isPost() )
		{			
			$m_country = new Cubix_Geography_Countries();
			$this->view->countries = $m_country->ajaxGetAll(false);

			$m_city = new Cubix_Geography_Cities();
			$this->view->cities = $m_city->ajaxGetAll(null, $data->country_id);

			$this->view->types = array('clients', 'hotels');
		} else {
			$data = new Cubix_Form_Data($this->_request);
						
			$data->setFields(array(
				'type' => '',
				'date' => '',
				'country_id' => '',
				'city_id' => '',
				'name' => '',
				'phone' => '',
				'comment' => '',
				'attach' => 'arr-int'
			));
			
			$data = $data->getData();
						
			$validator = new Cubix_Validator();
						
			if ( ! strlen($data['comment']) ) {
				$validator->setError('comment', 'Required');
			}

			if ( ! strlen($data['name']) ) {
				$validator->setError('name', 'Required');
			}

			if ( ! strlen($data['date']) ) {
				$validator->setError('date', 'Required');
			} else {
				$data['date'] = new Zend_Db_Expr('FROM_UNIXTIME(' . $data['date'] . ')');
			}

			if ( ! strlen($data['country_id']) ) {
				$validator->setError('country_id', 'Required');
			}

			if ( ! strlen($data['type']) ) {
				$validator->setError('type', 'Required');
			}

			if ( ! strlen($data['phone']) ) {
				$validator->setError('phone', 'Required');
			}

			$data['application_id'] = Cubix_Application::getId();
			$data['is_approved'] = 1;
			
			
			switch ($this->_request->act)
			{
				case 'approve':
					$data['is_approved'] = 1;
				break;
				case 'save':
					unset($data['status']);
				break;
			}

			if($data['type'] != 'clients' && isset($data['attach'])){
				unset($data['attach']);
			}

			if ( $validator->isValid() ) {
				$this->model->insert($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}

	
	public function editAction()
	{
		if( ! $this->_request->isPost() )
		{
			$this->view->data = $data = $this->model->getById($this->_request->id);

			if ( $data) {
				$p = 0;
				$images = $this->model->getImages($data->bl_id);

				if($images){
					foreach ($images as $image) {
						$img = new Cubix_ImagesCommonItem($image);
						$data->images[$p]['id'] = $image->image_id;
						$data->images[$p]['thumb'] = $img->getUrl('lvthumb');
						$data->images[$p]['original'] = $img->getUrl('orig');
						$p++;
					}

				}
			}

			$this->view->data = $data;

			$m_country = new Cubix_Geography_Countries();
			$this->view->countries = $m_country->ajaxGetAll(false);

			$m_city = new Cubix_Geography_Cities();
			$this->view->cities = $m_city->ajaxGetAll(null, $data->country_id);

			$this->view->types = array('clients', 'hotels');
		}
		else 
		{
			$data = new Cubix_Form_Data($this->_request);
						
			$data->setFields(array(
				'id' => 'int',
				'type' => '',
				'country_id' => '',
				'city_id' => '',
				'name' => '',
				'phone' => '',
				'comment' => '',
				'attach' => 'arr-int'
			));
			
			$data = $data->getData();
						
			$validator = new Cubix_Validator();
						
			if ( ! strlen($data['comment']) ) {
				$validator->setError('comment', 'Required');
			}

			if ( ! strlen($data['name']) ) {
				$validator->setError('name', 'Required');
			}

			if ( ! strlen($data['country_id']) ) {
				$validator->setError('country_id', 'Required');
			}

			if ( ! strlen($data['type']) ) {
				$validator->setError('type', 'Required');
			}

			if ( ! strlen($data['phone']) ) {
				$validator->setError('phone', 'Required');
			}
			
			switch ($this->_request->act)
			{
				case 'approve':
					$data['is_approved'] = 1;
				break;
				case 'save':
					unset($data['status']);
				break;
			}

			if($data['type'] != 'clients' && isset($data['attach'])){
				unset($data['attach']);
			}

			if ( $validator->isValid() ) {
				$this->model->save($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}

	public function toggleActiveAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->model->toggle($id);
	}
	
	public function removeAction()
	{
		$id = intval($this->_request->id);
		$this->model->remove($id);
		
		die;
	}

	public function removeImageAction()
	{	
		$id = intval($this->_request->id);
		$this->model->removeImage($id);
		
		die;
	}
	
	public function attachedImageBlacklistAction()
	{
		$tmp_dir = sys_get_temp_dir();

		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$images = new Cubix_ImagesCommon();


		$file = $tmp_dir . DIRECTORY_SEPARATOR . $_SERVER['HTTP_X_FILE_NAME'];
		file_put_contents($file, file_get_contents('php://input'));
		
		if ( ! filesize($file) || filesize($file) != $_SERVER['HTTP_X_FILE_SIZE'] ) {
			throw new Exception("Photo upload failed. Please try again!");
		}

		$data['tmp_name'] = $file;
		$data['name'] = $_SERVER['HTTP_X_FILE_NAME'];

		$photo = $images->save($data);

		if (!$photo) {

			$return = array(
				'status' => '0',
				'error' => $error
			);

		} else {
			$return = array(
				'status' => '1',
				'id' => $photo['image_id'],
				'finish' => TRUE,
				'error' => false
			);
		}

		echo json_encode($return);
	}
}
