<?php

class CopyListenerController extends Zend_Controller_Action
{
	public function indexAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$server = new Cubix_JsonRpc_Server();
		$server->addModule(new Cubix_EscortCopier_JsonRpc_CopyListener());
		$response = $server->handle();
		ob_start();
		$response->send();
		$output = ob_get_clean();
		$this->_response->setHeader('Content-type', 'application/json');
		$this->_response->setBody($output);
	}
}

