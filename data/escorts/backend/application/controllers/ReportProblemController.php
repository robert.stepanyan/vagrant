<?php

class ReportProblemController extends Zend_Controller_Action {

    /**
     * @var Model_UserItem
     */
    protected $user;
	private $app=false;
	public function init()
    {
		$this->model = new Model_ReportProblem();
		$this->app = $this->view->app =in_array(Cubix_Application::getId(), array(APP_EF,APP_6A))?true:false;
	}

	public function indexAction() 
	{	

	}

	
	public function dataAction()
	{
        $this->user = Zend_Auth::getInstance()->getIdentity();
		$req = $this->_request;

		$problem = $req->problem==0?NULL:$req->problem;
        $this->view->super = $this->user->type=='superadmin'?1:0;
		if(in_array(Cubix_Application::getId(), array(APP_ED))){
            $filter = array(
                'application_id' => $req->application_id,
                'email' => $req->email,
                'name' => $req->name,
                'problem'=>$problem,
                'showname' => $req->showname,
                'report_type' => $req->report_type,

            );
        }else{
            $filter = array(
                'application_id' => $req->application_id,
                'email' => $req->email,
                'name' => $req->name,
                'problem'=>$problem,
                'showname' => $req->showname

            );
        }


		
		$session = Zend_Auth::getInstance()->getIdentity();
		if($this->app)
			$bu_user =$session->type=='superadmin'|| $session->type=='admin'?NULL:$session->id;
		else
			$bu_user = null;
		
		$data = $this->model->getAll(
			$bu_user,
			$req->page,
			$req->per_page, 
			$filter, 
			$req->sort_field, 
			$req->sort_dir, 
			$count
		);



		$problems = Zend_Registry::get('defines');
		$problems = $problems['report_problems'];
		$model_users = new Model_Users();
		
		foreach ( $data as $i => $item ) {
			$user = $model_users->getByEmail($data[$i]->email);

            if(in_array(Cubix_Application::getId(), array(APP_ED))){
                $data[$i]->report_type_col = '';
                if($data[$i]->report_type == 1){
                    $data[$i]->report_type_col = '<a class="sbtn icon-phone"></a>';
                }elseif($data[$i]->report_type == 2){
                    $data[$i]->report_type_col = '<a class="sbtn icon-photo"></a>';
                }

                if($data[$i]->photo_link != ''){
                    $data[$i]->photo_link = '<a href="'.$data[$i]->photo_link.'" target="_blank">'.$data[$i]->photo_link.'</a>';
                }
            }


			if($this->app):
				$data[$i]->problem = isset($data[$i]->problem) && isset($problems[$data[$i]->problem])?$problems[$data[$i]->problem][0]:'';
			 endif;
            $data[$i]->email_details = $data[$i]->email;
			 if($data[$i]->email != ''){
                 if ($user->user_type == 'member')
                     $data[$i]->email_details = $data[$i]->email . '<br/><strong>(Member ID: ' . $user->id . ')</strong>';
                 elseif ($user->user_type == 'escort')
                     $data[$i]->email_details = $data[$i]->email . '<br/><strong>(Escort ID: ' . $user->escort_id . ')</strong>';
                 elseif ($user->user_type == 'agency')
                     $data[$i]->email_details = $data[$i]->email . '<br/><strong>(Agency ID: ' . $user->agency_id . ')</strong>';
                 else
                     $data[$i]->email_details = $data[$i]->email;
             }


		}
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

	public function detailsAction()
	{	
		$this->view->layout()->disableLayout();
		$this->view->details = $this->model->get($this->_request->id);
		if($this->app):
			$problems = Zend_Registry::get('defines');
			$problems = $problems['report_problems'];
			$this->view->details->problem = isset($problems[$this->view->details->problem])?$problems[$this->view->details->problem][0]:'';
		endif;
		$this->view->replys = $this->model->getReplys($this->_request->id);
	}
	
	public function checkAction()
	{
		$id = intval($this->_request->id);
		
		$item = $this->model->get($id);
		
		if ($item->status != REPORT_PROBLEM_STATUS_REPLIED)
			$this->model->check($id);
		
		die;
	}
	
	public function replyAction()
	{
		if ($this->_request->isPost())
		{
			$data = new Cubix_Form_Data($this->_request);

			$data->setFields(array(
				'rp_id' => 'int',
				'reply' => ''
			));

			$data = $data->getData();
			
			$validator = new Cubix_Validator();

			if ( ! $data['reply'] ) {
				$validator->setError('reply', 'Required');
			}

			if ( $validator->isValid() ) {
				$back_user = Zend_Auth::getInstance()->getIdentity();
				$data['backend_user_id'] = $back_user->id;
				$data['date'] = new Zend_Db_Expr('NOW()');
				$this->model->reply($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
		else
			$this->view->details = $this->model->get($this->_request->id);
	}

	public function removeAction()
	{
		$id = intval($this->_request->id);

		$this->model->remove($id);
		die;
	}
	
}
