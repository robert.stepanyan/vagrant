<?php

class ImportedReviewsController extends Zend_Controller_Action
{
	public function init()
	{
		$this->model = new Model_ImportedReviews();
	}
	
	public function indexAction() 
	{
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('default_reviews_data');
		$ses_filter->filter_params['status'] = 1;
		if(isset($this->_request->escort_id)){
			$ses_filter->filter_params = array();
			$ses_filter->filter_params['escort_id_f'] = $this->_request->escort_id;
		}
		if(isset($this->_request->username)){
			$ses_filter->filter_params = array();
			$ses_filter->filter_params['username'] = $this->_request->username;
		}
		$this->view->filter_params = $ses_filter->filter_params;
		/* FILTER PARAMS */
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'showname' => str_replace('_', '\_', $req->showname),
			'escort_id' => $req->escort_id,
			'profileId' => $req->profileId,
			'profileType' => $req->profileType,
			'profilePhoneNumber' => $req->profilePhoneNumber,
			'profilePhoneNumber2' => $req->profilePhoneNumber2,
			'reviewId' => $req->reviewId,
			'reviewUsername' => $req->reviewUsername,
			'reviewdate' => $req->reviewdate,
			'reviewRanking' => $req->reviewRanking,
			'reviewText' => $req->review_texts,
			'profileUrl' => $req->profileUrl,
			'reviewUrl' => $req->reviewUrl,
			'profileWebsite1' => $req->profileWebsite1,
			'profileWebsite2' => $req->profileWebsite2,
			'status' => $req->status,
		);

		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);


		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

	public  function toggleActiveAction()
    {
        $id = intval($this->_getParam('id'));
        if ( ! $id ) die;
        $this->model->toggle($id);

        die;
    }

    public function syncAction(){
        $app_id = Cubix_Application::getId();
        $http = 'http';

        $app = Cubix_Application::getById($app_id);
        $url = $http.'://www.' . $app->host;
        $sync_url = $url . '/api/sync/imported-reviews';

        $client = new Zend_Http_Client($sync_url, array(
                'maxredirects' => 3,
                'timeout'      => 60)
        );
        $client->setAuth('babazeo', 'GnaMer1a', Zend_Http_Client::AUTH_BASIC);

        $response = $client->request(Zend_Http_Client::GET);

        if ( $response->isError() ) {
            die(json_encode(array('status' => 'error', 'message' => $response->getMessage())));
        }

        header('Cubix-message: ' . $url . ' has been successfully syncronized');
        die(json_encode(array('status' => 'success','message'=>'Reviews was successfully synchronized')));
    }

}
