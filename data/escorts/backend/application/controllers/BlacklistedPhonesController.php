<?php

class BlacklistedPhonesController extends Zend_Controller_Action
{
	protected $model;

	public function init()
	{
		if (Cubix_Application::getId() != APP_EF)
			die;
		
		$this->model = new Model_BlacklistedPhones();
	}

	public function indexAction()
	{

	}

	public function dataAction()
	{
		$req = $this->_request;

		$filter = array(
			'phone' => $req->phone,
			'application_id' => $req->application_id
		);
        
		$data = $this->model->getAll(
			$req->page,
			$req->per_page,
			$filter,
			$req->sort_field,
			$req->sort_dir,
			$count
		);

		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

	public function deleteAction()
	{
        if ($this->_request->isPost()) 
		{
            $ids = $this->_request->getParam('id');
            
			if (count($ids) > 0 && $ids)
			{
                foreach($ids as $id) 
				{
                    $this->model->delete($id);
                }
            }
        }
		
        die;
	}

    public function createAction()
	{
        $this->view->layout()->disableLayout();

        if ($this->_request->isPost())
		{
            $data = new Cubix_Form_Data($this->_request);

			$params = array(
				'phone' => ''
			);

			$data->setFields($params);
			
			$data = $data->getData();
						
			$validator = new Cubix_Validator();
			
			if ( !ctype_digit($data['phone']) || substr($data['phone'], 0, 2) != '00' || strlen($data['phone']) < 9 ) {
				$validator->setError('phone', 'Phone number is not valid');
			}
						
			if ( $validator->isValid() ) {
				$data['application_id'] = Cubix_Application::getId();
				
				$this->model->save($data);
			}
			
			die(json_encode($validator->getStatus()));
        }        
    }

    public function editAction()
	{
        $this->view->layout()->disableLayout();

        $id = $this->_request->getParam('id');
        $this->view->item = $this->model->get($id);
        
        if ($this->_request->isPost())
		{
            $data = new Cubix_Form_Data($this->_request);

			$params = array(
				'id' => 'int',
				'phone' => ''
			);

			$data->setFields($params);
			
			$data = $data->getData();
						
			$validator = new Cubix_Validator();
			
			if ( !ctype_digit($data['phone']) || substr($data['phone'], 0, 2) != '00' || strlen($data['phone']) < 9 ) {
				$validator->setError('phone', 'Phone number is not valid');
			}
						
			if ( $validator->isValid() ) {
				$this->model->save($data);
			}
			
			die(json_encode($validator->getStatus()));
        }
    }
}
