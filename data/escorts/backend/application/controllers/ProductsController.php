<?php

class ProductsController extends Zend_Controller_Action
{
	/**
	 * @var $products Model_Products
	 */
	public $products;

    /**
     * @var $model Model_Products
     */
	protected $model;
	
	public function init()
	{
		$this->model = new Model_Products();
	}
			
	public function indexAction()
	{
		
	}

	public function dataAction()
	{
		$req = $this->_request;

		$filter = array(
			/*'u.username' => $req->username,
			'u.application_id' => $req->application_id,
			'u.email' => $req->email,
			'u.last_ip' => $req->last_ip,
			'u.status' => $req->status*/
		);

		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page,
			$filter,
			$this->_request->sort_field,
			$this->_request->sort_dir,
			$count
		);

		$DEFINITIONS = Zend_Registry::get('defines');

		foreach ( $data as $i => $item ) {
			$data[$i]->available_for = $DEFINITIONS['prod_available'][$item->available_for];
			$data[$i]->name = Cubix_I18n::translate($item->name);
		}

		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

	public function createAction()
	{
		$req = $this->_request;

		$this->view->langs = Cubix_Application::getAll();

		if ( $req->isPost() )
		{
			$validator = new Cubix_Validator();

			if ( ! $req->name ) {
				$validator->setError('name', 'Required');
			}

			$name = $req->name;
			$available_for = $req->available_for;

			$prices = $req->prices;
			
			if ( $validator->isValid() ) {
				$this->model->save($name, $available_for, $prices);
			}

			die(json_encode($validator->getStatus()));
		}
	}

	public function editAction()
	{
		$req = $this->_request;
		$prod_id = $req->id;

		$this->view->langs = $langs = Cubix_Application::getAll();

		$this->view->product = $product = $this->model->get($prod_id);

		$prices = array();
		foreach ($langs as $lang)
		{
			 $prices[$lang->id] = $product->getPrices($lang->id);
		}
		$this->view->product_prices = $prices;

		if ( $req->isPost() )
		{
			$validator = new Cubix_Validator();

			$available_for = $req->available_for;
			$prcs = $req->prices;

			$this->model->update($available_for, $prod_id, $prcs);

			die(json_encode($validator->getStatus()));
		}
	}

	public function ajaxGetPricesAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$app_id = $req->app_id;
		$prod_id = $req->product_id;

		$this->view->product = $product = $this->model->get($prod_id);
		$this->view->product_prices = $product->getPrices($app_id);
	}

	public function ajaxGetByApplicationAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;

		$app_id = $req->app_id;
		die(json_encode($this->model->getAllNames($app_id)));
	}

	public function removeAction()
	{
		$req = $this->_request;

		$id = $req->id;

		$this->model->remove($id);

		die;
	}

	public function removePriceAction()
	{
		$req = $this->_request;

		$id = $req->id;
		$id = $req->app_id;

		$this->model->removePrice($id, $app_id);

		die;
	}
}