<?php

class BlogController extends Zend_Controller_Action 
{
	public function init()
	{
		$this->model = new Model_Blog();
		
		$this->_session = new Zend_Session_Namespace('blog-backend');
	}
	
	public function indexAction() 
	{
		$categoryModel = new Model_BlogCategory();
		$this->view->cats = $categoryModel->getList();

		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('default_blog_data');
		$this->view->filter_params = $ses_filter->filter_params;
		/* FILTER PARAMS */
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'id' => $req->id,
			'status' => $req->status,
			'backend_user_id' => $req->backend_user_id,
			'application_id' => $req->application_id,
			'title' => $req->title,
			'post' => $req->ppost,
			'missing_tr' => $req->missing_tr,
			'category_id' => $req->filter_category,
		);
			//'layout' => $req->layout,
		
		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
						
		foreach ( $data as $i => $item ) {
			$data[$i]->reply_link = '
				<a href="' . $this->view->baseUrl() . '/blog-comments?pid=' . $item->id . '" style="display: block; text-align: center;" target="_blank">
					<img src="/img/cubix-button/icons/msg_send.png" />
				</a>
			';
			$data[$i]->cover_image_id = ($item->cover_image_id) ? '<img style="display: block; margin: auto;" src="/img/famfamfam/tick.png" />' : '<img style="display: block; margin: auto;" src="/img/famfamfam/minus.png" />';
		}
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
	
	public function toggleAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->model->toggle($id);
		
		die;
	}
	
	public function specialAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->model->special($id);
		
		die;
	}
	public function sliderAction()
	{
		if(Cubix_Application::getId() == APP_ED){
			$id = intval($this->_request->id);
			if ( ! $id ) die;
			$this->model->slider($id);
			
			die;
		}
		die;
	}

	public function createAction()
	{				
		$req = $this->_request;
		
		$categoryModel = new Model_BlogCategory();
		$this->view->cats = $categoryModel->getList();

		if ( $req->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);

			$params = array('category_id' => 'int');

			$langs = Cubix_I18n::getLangs(true);

			foreach ($langs as $lng)
			{
				$params['title_' . $lng] = '';
				$params['short_post_' . $lng] = '';
				$params['post_' . $lng] = '';
			}

			$data->setFields($params);
			
			$data = $data->getData();
			
			$validator = new Cubix_Validator();
			
			if ( ! $data['title_en'] ) {
				$validator->setError('title_en', 'Required');
			}
			
			if ( ! $data['category_id'] ) {
				$validator->setError('category_id', 'Required');
			}

			if ( ! $data['short_post_en'] ) {
				$validator->setError('short_post_en', 'Required');
			}
			
			if ( ! $data['post_en'] ) {
				$validator->setError('post_en', 'Required');
			}

			foreach ($langs as $lng)
			{
				if ($lng == 'en') continue;

				if ($data['title_' . $lng] || $data['short_post_' . $lng] || $data['post_' . $lng])
				{
					if (!$data['title_' . $lng])
					{
						$validator->setError('title_' . $lng, 'Required');
					}

					if (!$data['short_post_' . $lng])
					{
						$validator->setError('short_post_' . $lng, 'Required');
					}

					if (!$data['post_' . $lng])
					{
						$validator->setError('post_' . $lng, 'Required');
					}
				}
			}
			
			if (!$this->_session->photos)
			{
				$validator->setError('ph', 'Photo Required');
			}

			if (!$this->_session->cover)
			{
				$validator->setError('phc', 'Cover Photo Required');
			}
			
			if ( $validator->isValid() ) {
				$bu = Zend_Auth::getInstance()->getIdentity();
				$data['slug'] = Cubix_Utils::makeSlug($data['title_en']);
				$data['backend_user_id'] = $bu->id;

				$this->model->save($data, $this->_session->photos, $this->_session->cover);
				
				$this->_session->unsetAll();
			}
			
			die(json_encode($validator->getStatus()));
		}
		else 
		{
			$this->_session->unsetAll();
		}
	}
	
	public function removeAction()
	{
		$id = intval($this->_request->id);
		$this->model->remove($id);
		
		die;
	}
	
	public function editAction()
	{
		if ( !$this->_request->isPost() )
		{
			$ret = $this->model->get($this->_request->id);
			$this->view->data = $ret['post'];
			$this->view->photos = $photos = $ret['photos'];
			$this->view->cover = $cover = $ret['cover'];

			$categoryModel = new Model_BlogCategory();
			$this->view->cats = $categoryModel->getList();

			$this->_session->unsetAll();

			if ($photos && count($photos))
			{
				foreach ($photos as $photo)
				{
					$photo = (array)$photo;

					$this->_session->photos[$photo['image_id']] = $photo;
				}
			}

			if ($cover)
			{
				$this->_session->cover[$cover['image_id']] = $cover;
			}
		}
		else
		{
			$data = new Cubix_Form_Data($this->_request);

			$langs = Cubix_I18n::getLangs(true);

			$params = array('id' => 'int','category_id' => 'int');

			foreach ($langs as $lng)
			{
				$params['title_' . $lng] = '';
				$params['short_post_' . $lng] = '';
				$params['post_' . $lng] = '';
			}

			$data->setFields($params);

			$data = $data->getData();

			$validator = new Cubix_Validator();

			if ( ! $data['title_en'] ) {
				$validator->setError('title_en', 'Required');
			}

			if ( ! $data['category_id'] ) {
				$validator->setError('category_id', 'Required');
			}

			if ( ! $data['short_post_en'] ) {
				$validator->setError('short_post_en', 'Required');
			}

			if ( ! $data['post_en'] ) {
				$validator->setError('post_en', 'Required');
			}

			foreach ($langs as $lng)
			{
				if ($lng == 'en') continue;

				if ($data['title_' . $lng] || $data['short_post_' . $lng] || $data['post_' . $lng])
				{
					if (!$data['title_' . $lng])
					{
						$validator->setError('title_' . $lng, 'Required');
					}

					if (!$data['short_post_' . $lng])
					{
						$validator->setError('short_post_' . $lng, 'Required');
					}

					if (!$data['post_' . $lng])
					{
						$validator->setError('post_' . $lng, 'Required');
					}
				}
			}

			if (!$this->_session->photos || !count($this->_session->photos))
			{
				$validator->setError('ph', 'Photo Required');
			}

			if (!$this->_session->cover || !count($this->_session->cover))
			{
				$validator->setError('phc', 'Cover Photo Required');
			}

			if ( $validator->isValid() ) {
				$bu = Zend_Auth::getInstance()->getIdentity();
				$data['slug'] = Cubix_Utils::makeSlug($data['title_en']);
				$data['backend_user_id'] = $bu->id;

				$this->model->save($data, $this->_session->photos, $this->_session->cover);

				$this->_session->unsetAll();
			}

			die(json_encode($validator->getStatus()));
		}
	}
	
	public function uploadHtml5Action()
	{
		$tmp_dir = sys_get_temp_dir();
		
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		
		try {						
			if ( count($this->_session->photos) >= 3 ) {
				throw new Exception("You've reached maximum amount of pictures ( 3 )", Cubix_Images::ERROR_IMAGE_COUNT_LIMIT_OVER);
			}
			
			$response = array(
				'id'	=> $_SERVER['HTTP_X_FILE_ID'],
				'name'	=> $_SERVER['HTTP_X_FILE_NAME'],
				'size'	=> $_SERVER['HTTP_X_FILE_SIZE'],
				'error'	=> 0,
				'msg' => '',
				'finish'	=> FALSE
			);
			
			$file = $tmp_dir . DIRECTORY_SEPARATOR . $response['name'];
			file_put_contents($file, file_get_contents('php://input'));

			$i = getimagesize($file);

			$file = array(
				'tmp_name' => $file,
				'name' => $response['name']
			);

			if ($i[0] < 360)
				throw new Exception("Image width must be larger than 359px.");

			$images = new Cubix_ImagesCommon();
			$photo = $images->save($file);
			
			$img = new Cubix_ImagesCommonItem($photo);
			
			$response['image_id'] = $img->getImageId();
			$response['photo'] = $img->getUrl('backend_thumb_cropper');
			$response['finish'] = TRUE;
			
			$this->_session->photos[$photo['image_id']] = $photo;
			
		} catch (Exception $e) {
			$response['error']	= 1;
			$response['msg']	= $e->getMessage();
		}		
		
		echo json_encode($response);die;
	}

	public function uploadCoverHtml5Action()
	{
		$tmp_dir = sys_get_temp_dir();

		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		try {
			if ( count($this->_session->cover) >= 1 ) {
				throw new Exception("You've reached maximum amount of pictures ( 1 )", Cubix_Images::ERROR_IMAGE_COUNT_LIMIT_OVER);
			}

			$response = array(
				'id'	=> $_SERVER['HTTP_X_FILE_ID'],
				'name'	=> $_SERVER['HTTP_X_FILE_NAME'],
				'size'	=> $_SERVER['HTTP_X_FILE_SIZE'],
				'error'	=> 0,
				'msg' => '',
				'finish'	=> FALSE
			);

			$file = $tmp_dir . DIRECTORY_SEPARATOR . $response['name'];
			file_put_contents($file, file_get_contents('php://input'));

			$i = getimagesize($file);

			$file = array(
				'tmp_name' => $file,
				'name' => $response['name']
			);

			if ($i[0] < 740)
				throw new Exception("Image width must be larger than 739px.");

			if ($i[1] < 400)
				throw new Exception("Image width must be larger than 399px.");

			$images = new Cubix_ImagesCommon();
			$photo = $images->save($file);

			$img = new Cubix_ImagesCommonItem($photo);

			$response['image_id'] = $img->getImageId();
			$response['photo'] = $img->getUrl('backend_thumb_cropper');
			$response['finish'] = TRUE;

			$this->_session->cover[$photo['image_id']] = $photo;

		} catch (Exception $e) {
			$response['error']	= 1;
			$response['msg']	= $e->getMessage();
		}

		echo json_encode($response);die;
	}
	
	public function ajaxRemovePhotoAction()
	{
		$image_id = (int) $this->_request->image_id;
		
		$status = array('status' => 'error');
		if ( isset($this->_session->photos[$image_id]) ) {
			unset($this->_session->photos[$image_id]);
			
			$this->model->removePhoto($image_id);
			
			$status = array('status' => 'success');
		}
		
		die(json_encode($status));
	}

	public function ajaxRemoveCoverAction()
	{
		$image_id = (int) $this->_request->image_id;

		$status = array('status' => 'error');

		if ( isset($this->_session->cover[$image_id]) ) {
			unset($this->_session->cover[$image_id]);

			$this->model->removePhoto($image_id);

			$status = array('status' => 'success');
		}

		die(json_encode($status));
	}

	/*public function migAction()
	{
		$db = Zend_Registry::get('db');

		$images = $db->query('SELECT id, image_id FROM blog_posts')->fetchAll();

		foreach ($images as $img)
		{
			if ($img->image_id)
			{
				$db->insert('blog_images', array('post_id' => $img->id, 'image_id' => $img->image_id));
			}
		}

		die;
	}*/
}
