<?php

class TopFavoritesController extends Zend_Controller_Action
{
	protected $model;
	protected $defines;

	public function init()
	{
		$this->model = new Model_TopFavorites();
	}

	public function indexAction()
	{

	}

	public function dataAction()
	{
		$req = $this->_request;
        
		$filter = array(
			'username' => $req->username, 
			'escort_id' => $req->escort_id, 
			'application_id' => $req->application_id 
		);
        
		$data = $this->model->getAll(
			$req->page,
			$req->per_page,
			$filter,
			$req->sort_field,
			$req->sort_dir,
			$count
		);
		
		foreach ( $data as $i => $item ) 
		{
			if ($item->status == USER_STATUS_DISABLED)
			{
				$data[$i]->username = '<a href="#" rel="' . $item->user_id . '" class="p-member red">' . $item->username . '</a>';
			}
			else
			{
				$data[$i]->username = '<a href="#" rel="' . $item->user_id . '" class="p-member">' . $item->username . '</a>';
			}
			
			$arr_esc = $this->model->getTopEscorts($item->user_id);
			$str = '';
			
			if ($arr_esc)
			{
				foreach ($arr_esc as $e)
				{
					$str .= '<div class="fleft">' . $e->rank . '. ' . $e->showname . ' (' . $e->escort_id . ')' . '</div>';
				}
			}
			
			$str .= '<div class="clear"></div>';
			$data[$i]->escorts = $str;
		}

		die(json_encode(array(
			'data' => $data,
			'count' => $count
		)));
	}

	public function viewAction()
	{
		$id = $this->_request->id;
		
		$this->view->item = $item = $this->model->get($id);
	}
	
	public function setStatusAction()
	{
		$id = $this->_request->id;
		$status = $this->_request->status;
		
		if (is_array($id))
		{
			foreach ($id as $i)
			{
				$this->model->update($i, $status);
			}
		}
		else
			$this->model->update($id, $status);
	
		die;
	}
}
