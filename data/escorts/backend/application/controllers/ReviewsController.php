<?php

class ReviewsController extends Zend_Controller_Action
{
	public function init()
	{
		$this->model = new Model_Reviews();
	}
	
	public function indexAction() 
	{
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('default_reviews_data');
		$ses_filter->filter_params['status'] = 1;
		if(isset($this->_request->escort_id)){
			$ses_filter->filter_params = array();
			$ses_filter->filter_params['escort_id_f'] = $this->_request->escort_id;
		}
		if(isset($this->_request->username)){
			$ses_filter->filter_params = array();
			$ses_filter->filter_params['username'] = $this->_request->username;
		}
		$this->view->filter_params = $ses_filter->filter_params;
		/* FILTER PARAMS */
	}
	
	public function dataAction()
	{
		// delete old deleted reviews
		$this->model->removeOlds(REVIEW_REMOVE_INTERVAL);
		//
		$req = $this->_request;
		
		$filter = array(
			'showname' => str_replace('_', '\_', $req->showname),
			'mode' => $req->mode,
			'escort_id' => $req->escort_id,
			'escort_id_f' => $req->escort_id_f,
			'username' => $req->username,
			'sms_unique' => $req->sms_unique,
			'application_id' => $req->application_id,
			'status' => $req->status,
			'is_fake_free' => $req->is_fake_free,
			'modified' => $req->modified,
			'answer' => $req->answer,
			'review_texts' => $req->review_texts,
			'is_suspicious' => $req->is_suspicious,
			'deleted' => $req->deleted ? 1 : 0,
			'country' => $req->country,
			'city' => $req->city,
			'ip' => $req->ip,
			'filter_by' => $req->filter_by,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to,
			'is_active_escort' => $req->is_active_escort,
			'has_active_package' => $req->has_active_package,
            'is_mobile' => $req->is_mobile,
            'added' => $req->added_f,
			'disable_type' => $req->disable_type
		);
				
		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		$DEFINITIONS = Zend_Registry::get('defines');
		
		foreach ( $data as $i => $item ) {
			$data[$i]->clear_username = $item->username;
			$data[$i]->sms_status = $DEFINITIONS['sms_status_options'][$item->sms_status];
			$data[$i]->status = $DEFINITIONS['review_status_options'][$item->status];
			//$data[$i]->is_suspicious = $DEFINITIONS['review_suspicious_options'][$item->is_suspicious];
			if ($item->is_suspicious == REVIEW_SUSPICIOUS_USER_IP_DUPLICITY)
				$d = '<a href="http://' . Cubix_Application::getById()->backend_url . '/dashboard/dublicities?member=' . $item->username . '" target="_blank">' . $DEFINITIONS['review_suspicious_options'][$item->is_suspicious] . '</a>';
			else
				$d = '<a href="http://' . Cubix_Application::getById()->backend_url . '/dashboard/cookie-dublicities?member=' . $item->username . '" target="_blank">' . $DEFINITIONS['review_suspicious_options'][$item->is_suspicious] . '</a>';
			$data[$i]->is_suspicious = $d;
			$item->is_fake_free == 1 ? $data[$i]->is_fake_free = 1 : $data[$i]->is_fake_free = 0;

			$data[$i]->c = '';
			$data[$i]->cities = '';

			if (Cubix_Application::getId() == APP_EF)
			{
				if (($item->escort_status & Cubix_EscortStatus::ESCORT_STATUS_DELETED)
					|| ($item->escort_status & Cubix_EscortStatus::ESCORT_STATUS_TEMPRARY_DELETED)
					|| ($item->escort_status & Cubix_EscortStatus::ESCORT_STATUS_SUSPICIOUS_DELETED))
				{
					$data[$i]->showname = '<span class="red bold">' . $item->showname . '</span>';
				}
				else if ($item->escort_status & Cubix_EscortStatus::ESCORT_STATUS_OWNER_DISABLED)
				{
					$data[$i]->showname = '<span class="brown bold">' . $item->showname . '</span>';
				}
				else if ($item->escort_status & Cubix_EscortStatus::ESCORT_STATUS_ADMIN_DISABLED)
				{
					$data[$i]->showname = '<span class="pink bold">' . $item->showname . '</span>';
				}
				else if ($item->has_package)
				{
					$data[$i]->showname = '<span class="green bold">' . $item->showname . '</span>';
				}
			}

			if ($item->m_is_suspicious)
			{
				if (Cubix_Application::getId() == 1)
					$cl = 'red';
				else
					$cl = 'pink';
				$data[$i]->username = '<span class="' . $cl . ' bold">' . $item->username . '</span>';
				$data[$i]->c = 'c4';
			}
			elseif ($item->m_is_trusted)
			{
				if (Cubix_Application::getId() == 1)
					$cl = 'brown';
				else
					$cl = 'green';
				$data[$i]->username = '<span class="' . $cl . ' bold">' . $item->username . '</span>';
				$data[$i]->c = 'c5';
			}
			elseif ($item->new_member)
			{
				if (Cubix_Application::getId() == 1)
					$cl = 'green';
				else
					$cl = 'red';
				$data[$i]->username = '<span class="' . $cl . ' bold">' . $item->username . '</span>';
				$data[$i]->c = 'c1';
			}
			elseif ($this->model->fakeByPhone($item->user_id))
			{
				if (Cubix_Application::getId() == 1)
					$cl = 'blue';
				else
					$cl = 'red';
				$data[$i]->username = '<span class="' . $cl . ' bold">' . $item->username . '</span>';
				$data[$i]->c = 'c2';
			}
			elseif (($cities = $this->model->fakeByCity($item->user_id)) != false)
			{
				if (Cubix_Application::getId() == 1)
					$cl = 'pink';
				else
					$cl = 'red';
				$data[$i]->username = '<span class="' . $cl . ' bold">' . $item->username . '</span>';
				$data[$i]->c = 'c3';
				$data[$i]->cities = $cities;
			}

			$data[$i]->sms_answer = $this->model->getSMSByUnique($item->sms_unique);

			$data[$i]->member_reviews_count = $this->model->getMemberReviewsCount($item->user_id);
			$data[$i]->member_reviews_count_approve = $this->model->getMemberReviewsCount($item->user_id, REVIEW_APPROVED);
			$data[$i]->member_reviews_count_disable = $this->model->getMemberReviewsCount($item->user_id, REVIEW_DISABLED);
			$data[$i]->escort_reviews_count = $this->model->getEscortReviewsCount($item->escort_id);
			$data[$i]->escort_reviews_count_approve = $this->model->getEscortReviewsCount($item->escort_id, REVIEW_APPROVED);
			$data[$i]->escort_reviews_count_disable = $this->model->getEscortReviewsCount($item->escort_id, REVIEW_DISABLED);
			$last = $this->model->getEscortLastReviewDate($item->escort_id);
			$data[$i]->escort_last_review_date = $last ? date('d F, Y', $last->date) : '';
			$data[$i]->escort_last_review_status = $last ? $DEFINITIONS['review_status_options'][$last->status] : '';
			$last_member = $this->model->getMemberLastReviewDate($item->user_id);
			$data[$i]->member_last_review_date = $last_member ? date('d F, Y', $last_member->date) : '';
			$data[$i]->member_last_review_status = $last_member ? $DEFINITIONS['review_status_options'][$last_member->status] : '';
			$data[$i]->date_registered = date('d F, Y', $item->date_registered);
			$data[$i]->is_mobile = ( $item->is_mobile ) ? 'M' : '-';

			$data[$i]->review = mb_convert_encoding($item->review, "UTF-8");
			
			if ($item->last_ip)
			{
				$geo = Cubix_Geoip::getClientLocation($item->last_ip);
				
					
				if (!is_null($geo))
				{
					$data[$i]->city_title = htmlspecialchars($geo['city'], ENT_QUOTES);
				}
			}
		}
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
	
	public function editAction()
	{
		if (!$this->_request->isPost())
		{
			$m = new Model_Escorts();
			$m_d = new Model_Dublicities();

			$this->view->data = $data = $this->model->getById($this->_request->id);
			$this->view->last_ip = $m->getLastIP($data->escort_id);
			$this->view->dublCount = $m_d->getForUserCount($data->user_id);
			$this->view->currencies = Model_Currencies::getAll();
		}
		else 
		{
			$data = new Cubix_Form_Data($this->_request);

			$fields = array(
                'id' => 'int',
                'status' => 'int',
                'esc_id' => 'int',
                'place' => 'int',
                'duration' => 'int',
                'duration_unit' => 'int',
                'price' => 'int',
                'currency' => 'int',
                'looks_rating' => '',
                'services_rating' => '',
                'sms_status' => 'int',
                'kissing' => 'int',
                'blowjob' => 'int',
                'cumshot' => 'int',
                's_69' => 'int',
                'anal' => 'int',
                'sex' => 'int',
                'attitude' => 'int',
                'conversation' => 'int',
                'breast' => 'int',
                'multiple_times_sex' => 'int',
                'availability' => 'int',
                'photos' => 'int',
                'services_comments' => '',
                'review' => '',
                'is_fake_free' => 'int',
                'is_deleted' => 'int',
                'escort_comment' => ''
            );

            if(Cubix_Application::getId() == APP_ED){
                $fields['quality'] = 'int';
                $fields['location_review'] = '';
                $fields['creation_date'] = '';
                $fields['creation_date_time'] = '';
                //$fields['country'] = $data['country_id'];
            }

			$data->setFields($fields);
			
			$data = $data->getData();
						
			$validator = new Cubix_Validator();
						
			/*if (!$data['duration']) {
				$validator->setError('duration', 'Required');
			}
			
			if ( ! $data['price'] ) {
				$validator->setError('price', 'Required');
			}
			elseif (!is_numeric($data['price']))
			{
				$validator->setError('price', 'Must be numeric');
			}*/

			switch ($this->_request->act)
			{
				case 'approve':
					$data['status'] = REVIEW_APPROVED;
					Cubix_SyncNotifier::notify($data['esc_id'], Cubix_SyncNotifier::EVENT_ESCORT_REVIEW_APPROVED, array('escort_id' => $data['esc_id'], 'review_id' =>$data['id'] ));
					Cubix_LatestActions::addAction($data['esc_id'], Cubix_LatestActions::ACTION_NEW_REVIEW);
					
					Cubix_LatestActions::addDirectActionDetail($data['esc_id'], Cubix_LatestActions::ACTION_NEW_REVIEW, $data['id']);
					
					if(Cubix_Application::getId() == APP_ED){
						$m = new Model_Escorts();
						
						$agency_id = $m->getAgencyId($data['esc_id']);
						$client = new Cubix_Api_XmlRpc_Client();
						$review  = $client->call('Reviews.getReviewbyID', array($data['id']));
						
						$shortcodes = array(
							'showname' => $review['showname'],
							'link_to_sedcard' => 'http://www.'.Cubix_Application::getById()->host.'/reviews/'.$review['showname'].'-'.$review['id'],
							'agency' => $review['agency_name'],
							'escort_showname' => $review['showname'],
							'username' => $review['author_username']
						);
						
						if($agency_id){
							Cubix_FollowEvents::notify($agency_id, 'agency', FOLLOW_AGENCY_NEW_REVIEW, array('rel_id' => $data['id'],'rel_id2' => $data['esc_id']), true);
							Cubix_Email::sendTemplate('review_published_agency_v1', $review['owner_email'], $shortcodes);  
						}else{
							Cubix_Email::sendTemplate('review_published_escort_v1', $review['owner_email'], $shortcodes);   
						}
						Cubix_Email::sendTemplate('review_published_member_v1', $review['author_email'], $shortcodes);   
						
						Cubix_FollowEvents::notify($data['esc_id'], 'escort', FOLLOW_ESCORT_NEW_REVIEW, array('rel_id' => $data['id']) , null, null, true);
					}
					
					// copy review to forum.beneluxxx.com
					if (Cubix_Application::getId() == APP_BL)
					{
						$m_e = new Model_EscortsV2();
						$e_data = $m_e->getDataForForum($data['esc_id']);

						if ($e_data)
						{
							$r_data = $this->model->getDataForForum($this->_request->id);

							$title = $e_data->showname . ' escort ' . $e_data->base_city;
							$title_seo = Cubix_Utils::makeSlug($title);
							$ip = Cubix_Geoip::getIP();
							$post = '<a class="bbc_url" rel="nofollow external" title="External link" href="';
							$post .= 'http://www.beneluxxx.com/escort/' . $e_data->showname . '-' . $data['esc_id'] . '">';
							$post .= 'http://www.beneluxxx.com/escort/' . $e_data->showname . '-' . $data['esc_id'];
							$post .= '</a>';
							$post .= '<br/>';
							$post .= 'Meeting Date: ' . date('d M, Y', $r_data->meeting_date);
							$post .= '<br/>';
							$post .= 'City: ' . $r_data->city;
							$post .= '<br/>';
							$post .= $data['review'];

							$api = new Cubix_Forum2Api();
							$ret = $api->addReviewAsTopicAndPost($title, $title_seo, $ip, $post, $this->_request->id);
						}
					}
					//
					break;
				case 'need_modification':
					$data['status'] = REVIEW_NEED_MODIFICATION;
					Cubix_SyncNotifier::notify($data['esc_id'], Cubix_SyncNotifier::EVENT_ESCORT_REVIEW_NEED_MODIFICATION, array('escort_id' => $data['esc_id'], 'review_id' =>$data['id'] ));
					break;
				case 'reject':
					$data['status'] = REVIEW_DISABLED;
					Cubix_SyncNotifier::notify($data['esc_id'], Cubix_SyncNotifier::EVENT_ESCORT_REVIEW_DISABLED, array('escort_id' => $data['esc_id'], 'review_id' =>$data['id'] ));
					break;
			}

			if ($validator->isValid())
			{
				if (!$data['is_deleted'])
				{
					$this->model->save($data);
				}
			}

			die(json_encode($validator->getStatus()));
		}
	}
	
	public function removeAction()
	{
		if (!$this->_request->isPost())
		{
			$this->view->data = $data = $this->model->getById($this->_request->id);
		}
		else 
		{
			$data = new Cubix_Form_Data($this->_request);

			$data->setFields(array(
				'id' => 'int',
				'reason' => ''
			));

			$data = $data->getData();

			$validator = new Cubix_Validator();

			if ( ! $data['reason'] ) {
				$validator->setError('reason', 'Required');
			}

			if ( $validator->isValid() ) {
				$this->model->remove($data);
			}
			$escort_id = $this->model->getEscortIdById($data['id']);
			Cubix_SyncNotifier::notify( $escort_id , Cubix_SyncNotifier::EVENT_ESCORT_REVIEW_DELETED, array( 'escort_id' => $escort_id, 'review_id' =>$data['id']));
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function disabledReasonAction()
	{
		if (!$this->_request->isPost())
		{
			$this->view->data = $data = $this->model->getById($this->_request->id);
		}
		else
		{
			$data = new Cubix_Form_Data($this->_request);

			$data->setFields(array(
				'id' => 'int',
				'disable_type' => 'int',
				'reason' => ''
			));

			$data = $data->getData();
			
			if (!$data['disable_type'])
				$data['disable_type'] = REVIEW_DISABLE_TYPE_INTERNAL;

			$validator = new Cubix_Validator();

			if ( ! $data['reason'] ) {
				$validator->setError('reason', 'Required');
			}

			if ( $validator->isValid() ) {
				$this->model->disabledReason($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function commentAction(){
		
		$this->view->layout()->disableLayout();
				
		if ( $this->_request->isPost() ) 
		{
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'id' => 'int',
				'comment' => 'special|notags'
			));
			$data = $data->getData();
			
			$validator = new Cubix_Validator();
			
			if ( $validator->isValid() ) 
			{	
				$this->model->updateCommentGrid($data);
			}

			die(json_encode($validator->getStatus()));
		}
		else
		{
			$id = intval($this->_request->id);
			$this->view->id = $id;
			$this->view->comment = $this->model->getCommentGrid($id);
		}
		
	}

    public function addAction()
    {
        $added = 1;
        if (!$this->_request->isPost())
        {
            $m = new Model_Escorts();
            $m_d = new Model_Dublicities();

            $this->view->data = $data = $this->model->getById($this->_request->id);
            $this->view->last_ip = $m->getLastIP($data->escort_id);
            $this->view->dublCount = $m_d->getForUserCount($data->user_id);
            $this->view->currencies = Model_Currencies::getAll();

        }
        else
        {
            $data = new Cubix_Form_Data($this->_request);

            $fields = array(
                'country' => 'int',
                'city' => 'int',
                'meeting_date' => '',
                'id' => 'int',
                'ip' => '',
                'status' => 'int',
                'escort_id' => 'int',
                'member_id' => 'int',
                'place' => 'int',
                'duration' => 'int',
                'duration_unit' => 'int',
                'price' => 'int',
                'currency' => 'int',
                'looks_rating' => '',
                'services_rating' => '',
                'sms_status' => 'int',
                'kissing' => 'int',
                'blowjob' => 'int',
                'cumshot' => 'int',
                '69' => 'int',
                'anal' => 'int',
                'sex' => 'int',
                'attitude' => 'int',
                'conversation' => 'int',
                'breast' => 'int',
                'multiple_times_sex' => 'int',
                'availability' => 'int',
                'photos' => 'int',
                'services_comments' => '',
                'review' => '',
                'is_fake_free' => 'int',
                'is_deleted' => 'int',
                'escort_comment' => '',
                'location_review' => ''
            );

            if(Cubix_Application::getId() == APP_ED){
                $fields['quality'] = 'int';
                $fields['location_review'] = '';
                $fields['creation_date'] = '';
                $fields['added'] = 'int';
                //$fields['country'] = $data['country_id'];
            }

            $data->setFields($fields);

            $data = $data->getData();

            $validator = new Cubix_Validator();
            $member_model = new Model_Members();
            $escort_model = new Model_EscortsV2();

                if (strlen($data['member_id']) && (!$member_model->memberExistsById($data['member_id']))) {
                    $validator->setError('member_id', 'Member does not exist');
                }
                if (strlen($data['escort_id']) && (!$escort_model->escortExistsById($data['escort_id']))) {
                    $validator->setError('escort_id', 'Escort does not exist');
                }

            if ($validator->isValid())
            {
                if (!$data['is_deleted'])
                {
                    $this->model->insert($data);
                }
            }

            die(json_encode($validator->getStatus()));
        }
    }
}
