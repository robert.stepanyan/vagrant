<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	public function run()
	{
		// Zend_Layout::startMvc();
		define('IN_BACKEND', 1);

		parent::run();
	}
	
	protected function _initRoutes()
	{
		require('../application/models/Plugin/Auth.php');
		require('../application/models/Plugin/Filter.php');
		$this->bootstrap('frontController');
		$front = $this->getResource('frontController');
		$router = $front->getRouter();
		
		$router->addRoute(
			'dashboard',
			new Zend_Controller_Router_Route(
				'',
				array(
					'module' => 'default',
					'controller' => 'dashboard',
					'action' => 'index'
				)
			)
		);
		
		$router->addRoute(
			'dublicities-view',
			new Zend_Controller_Router_Route(
				'dashboard/dublicities/view',
				array(
					'module' => 'default',
					'controller' => 'dashboard',
					'action' => 'dublicities-view'
				)
			)
		);
		
		$router->addRoute(
			'external-link',
			new Zend_Controller_Router_Route(
				'go',
				array(
					'module' => 'default',
					'controller' => 'index',
					'action' => 'go'
				)
			)
		);
	}
	
	protected function _initConfig()
	{
		$video_configs = $this->getOption('videos');
		if(HOSTNAME == 'escortforumit.xxx' && getenv('VIDEO_FTP_HOST')){
			$video_configs['remote']['ftp']['host'] = getenv('VIDEO_FTP_HOST');
			$video_configs['remote']['ftp']['port'] = getenv('VIDEO_FTP_PORT') ? getenv('FTP_PORT') : 21;
			$video_configs['remote']['ftp']['login'] = getenv('VIDEO_FTP_USER');
			$video_configs['remote']['ftp']['password'] = getenv('VIDEO_FTP_PASS');
			$video_configs['remote']['ftp']['passive'] = getenv('VIDEO_FTP_PASSIVE');

			if (getenv('VIDEO_TEMP_DIR_PATH')) {
				// this path has to be shared file system for chunked html5 upload to work
				$video_configs['temp_dir_path'] = getenv('VIDEO_TEMP_DIR_PATH');
			}
		}
		
		Zend_Registry::set('system_config', $this->getOption('system'));
		Zend_Registry::set('db_connection', $this->getOption('resources'));
		Zend_Registry::set('videos_config', $video_configs);
		Zend_Registry::set('analytics_config', $this->getOption('analytics'));
		Zend_Registry::set('escorts_config', $this->getOption('escorts'));
		Zend_Registry::set('feedback_config', $this->getOption('feedback'));
		Zend_Registry::set('newsletter_config', $this->getOption('newsletter'));
		Zend_Registry::set('statistics', $this->getOption('statistics'));
	}
	
	protected function _initAutoload()
	{
		$moduleLoader = new Zend_Application_Module_Autoloader(array(
			'namespace' => '',
			'basePath' => APPLICATION_PATH
		));

		$autoloader = Zend_Loader_Autoloader::getInstance();
		$autoloader->registerNamespace('Cubix_');

		return $moduleLoader;
	}
	
	protected function _initDatabase()
	{
		$this->bootstrap('db');
		if(HOSTNAME == 'escortforumit.xxx' && getenv('MYSQL_HOST')){
			$db_configs = array(
				'adapter' => 'MYSQLi',
				'host' => getenv('MYSQL_HOST'),
				'username' => getenv('MYSQL_USER'),
				'password' => getenv('MYSQL_PASS'),
				'port' => getenv('MYSQL_PORT') ? getenv('MYSQL_PORT') : 3306,
				'dbname' => getenv('MYSQL_DBNAME')
			);
			$db = new Cubix_Db_Adapter_Mysqli($db_configs);
		}
		else{
			$db = $this->getResource('db');
		}
		$db->setFetchMode(Zend_Db::FETCH_OBJ);

		Zend_Registry::set('db', $db);
		Cubix_Model::setAdapter($db);

		$db->query('SET NAMES `utf8`');
	}
	
	protected function _initViewHelpers()
	{
		$this->bootstrap('layout');
		$layout = $this->getResource('layout');
		$view = $layout->getView();

		$view->doctype('XHTML1_STRICT');

		$view->headMeta()
			->appendHttpEquiv('Content-Type', 'text/html; charset=utf-8')
			->appendHttpEquiv('Cache-Control', 'no-cache');
		$view->headTitle()->setSeparator(' - ');
		$view->headTitle('EscortForum Backend');
	}
	
	
	
	protected function _initStorage()
	{
		$images_configs = $this->getOption('images');
		
		if(HOSTNAME == 'escortforumit.xxx' && getenv('FTP_HOST')){
			$images_configs['remote']['ftp']['host'] = getenv('FTP_HOST');
			$images_configs['remote']['ftp']['port'] = getenv('FTP_PORT') ? getenv('FTP_PORT') : 21;
			$images_configs['remote']['ftp']['login'] = getenv('FTP_USER');
			$images_configs['remote']['ftp']['password'] = getenv('FTP_PASS');
			$images_configs['remote']['ftp']['passive'] = getenv('FTP_PASSIVE');

			if (getenv('IMAGE_TEMP_DIR_PATH')) {
				// this path has to be shared file system for chunked html5 upload to work
				$images_configs['temp_dir_path'] = getenv('IMAGE_TEMP_DIR_PATH');
			}
		}
		
		Zend_Registry::set('images_config', $images_configs);
	}

	protected function _initCache()
	{
		//
		$cache_configs = $this->getOption('cache');
		if(HOSTNAME == 'escortforumit.xxx' && getenv('MEMCACHE_HOST')){
			$cache_configs['host'] =  getenv('MEMCACHE_HOST');
			$cache_configs['port'] =  getenv('MEMCACHE_PORT');
		}
		
		$frontendOptions = array(
			'lifetime' => null,
			'automatic_serialization' => true
		);
		
		$backendOptions = array(
			'servers' => array(
				array(
					'host' => $cache_configs['host'],
					'port' => $cache_configs['port'],
					'persistent' =>  true
				)
			),
		);

		if (defined('APPLICATION_ENV') && (APPLICATION_ENV == 'staging' || APPLICATION_ENV == 'development') ) {
			$cache = Zend_Cache::factory('Core', 'Blackhole', array('automatic_serialization' => true));
		} else {
			//$cache = Zend_Cache::factory('Core', 'Blackhole', array('automatic_serialization' => true));
			$cache = Zend_Cache::factory(new Zend_Cache_Core($frontendOptions), new Zend_Cache_Backend_Memcached($backendOptions), $frontendOptions);
		}

		Zend_Registry::set('cache', $cache);
		
	}
	
	protected function _initDefines()
	{
		if ( ! is_callable('json_encode') ) {
			function json_encode($value) {
				return Zend_Json_Encoder::encode($value);
			}
			
			function json_decode($value)
			{
				return Zend_Json_Decoder::decode($value);
			}
		}
	}

	protected function _initApi()
	{
		$api_config = $this->getOption('api');
		if (getenv('API_SERVER')) {
			$api_config['server'] = getenv('API_SERVER');
		}
		Zend_Registry::set('api_config', $api_config);
		Cubix_Api_XmlRpc_Client::setServer($api_config['server']);
	}

	protected function _initHooks()
	{
		$hooker = new Cubix_Hooker();
		$hooker->addHook(new Model_Billing_Hook());
		
		Zend_Registry::set('BillingHooker', $hooker);
	}
}
