<?php
class Model_Journal extends Cubix_Model
{
	protected $_table = 'journal';
	protected $_itemClass = 'Model_JournalItem';

	public function getBackupTables()
	{
		return $this->getAdapter()->fetchAll('SHOW TABLE STATUS LIKE "journal%"');
	}

	public function get($id)
	{
		$sql = ' 
			SELECT
				st.id AS id, st.user_id, st.subject,
               (CASE u.user_type
                    WHEN "escort" THEN ES.id
                    WHEN "agency" THEN AG.id
                    WHEN "member" THEN MB.user_id END) as main_id,
				st.message, st.status, UNIX_TIMESTAMP(st.creation_date) AS creation_date,
				UNIX_TIMESTAMP(st.resolve_date) AS resolve_date, st.backend_user_id, u.username,u.user_type as usertype,u.id as user_id
			FROM support_tickets st
			INNER JOIN users u ON u.id = st.user_id
			LEFT JOIN applications ap ON ap.id = u.application_id
            LEFT OUTER JOIN escorts as ES ON ES.user_id = u.id
            LEFT OUTER JOIN agencies as AG ON AG.user_id = u.id
            LEFT OUTER JOIN members as MB ON MB.user_id = u.id
			WHERE 1 AND st.id = ?
		';
		
		$issue = parent::_fetchRow($sql, array($id));

		return $issue;
	}
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$table_name = 'journal';
		if ( isset($filter['backup_table']) && strlen($filter['backup_table']) ) {
			$table_name = $filter['backup_table'];
			unset($filter['backup_table']);
		}

		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				e.id AS escort_id, e.showname, j.event_type, j.comment, 
				UNIX_TIMESTAMP(j.date) AS event_date, j.data, bu.username AS bu_username
			FROM ' . $table_name . ' j
			INNER JOIN escorts e ON e.id = j.escort_id
			INNER JOIN users u ON u.id = e.user_id
			LEFT JOIN backend_users bu ON bu.id = j.sales_user_id
			WHERE 1
		';
		
				
		if ( strlen($filter['e.showname']) ) {
			$where .= self::quote(' AND e.showname LIKE ? ', $filter['e.showname'] . '%');
		}
		
		if ( strlen($filter['u.application_id']) ) {
			$where .= self::quote(' AND u.application_id = ? ', $filter['u.application_id']);
		}
		
		if ( strlen($filter['u.username']) ) {
			$where .= self::quote(' AND u.username LIKE ? ', $filter['u.username'] . '%');
		}
		
		if ( strlen($filter['e.escort_id']) ) {
			$where .= self::quote(' AND e.id = ? ', $filter['e.escort_id']);
		}		
		$user = Zend_Auth::getInstance()->getIdentity();
		if ( $user->type != 'superadmin' && $user->type != 'admin'){
			$where .= self::quote(' AND j.event_type != ? ', Cubix_SyncNotifier::EVENT_ESCORT_PHONE_CHANGED );
		}
		
		if ( strlen($filter['j.event_type']) ) {
			$where .= self::quote(' AND j.event_type = ? ', $filter['j.event_type']);
		}
		
		if ( strlen($filter['bu.sales_user_id']) ) {
			$where .= self::quote(' AND bu.id = ? ', $filter['bu.sales_user_id']);
		}

        /*if ( strlen($filter["date <= DATE_SUB(CONCAT(CURDATE(), ' 00:00:00'), ?)"]) ) {
            $interval = $filter["date <= DATE_SUB(CONCAT(CURDATE(), ' 00:00:00'), ?)"];
			$where .= " AND date <= DATE_SUB(CONCAT(CURDATE(), ' 00:00:00'), $interval)";
		}

        if ( strlen($filter["date >= DATE_SUB(CONCAT(CURDATE(), ' 00:00:00'), ?)"]) ) {
            $interval = $filter["date >= DATE_SUB(CONCAT(CURDATE(), ' 00:00:00'), ?)"];
            $where .= " AND date >= DATE_SUB(CONCAT(CURDATE(), ' 00:00:00'), $interval)";
		}


        if ( strlen($filter["date <= ?"]) ) {
            $where .= self::quote(' AND date <= ? ', $filter['date <= ?'] );
		}

        if ( strlen($filter["date >= ?"]) ) {
            $where .= self::quote(' AND date >= ? ', $filter['date >= ?'] );
		}*/

        if ( strlen($filter["event_type"]) ) {
            $where .= ' AND event_type IN (1,2,4,5,6,11,12,13,14,15,16,19,20) ';
		}
		
		/*if ( $filter['date_from'] && $filter['date_to'] ) {
			if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) ) {
				$where .= self::quote(' AND DATE(date) = DATE(FROM_UNIXTIME(?)) ', $filter['date_from']);
			}
		}*/

		if ( $filter['date_from'] ) {
			$where .= self::quote(' AND DATE(date) >= DATE(FROM_UNIXTIME(?)) ', $filter['date_from']);
		}

		if ( $filter['date_to'] ) {
			$where .= self::quote(' AND DATE(date) <= DATE(FROM_UNIXTIME(?)) ', $filter['date_to']);
		}
		
		if ( $filter['sales_id'] ) {
			$where .= self::quote(' AND u.sales_user_id = ? ', $filter['sales_id']);
		}
		
		$sql .= $where;
		
		$sql .= '
			/*GROUP BY st.id*/
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$data = parent::_fetchAll($sql);

		$countSql = "SELECT FOUND_ROWS()";
		$count = intval($this->getAdapter()->fetchOne($countSql));

		return $data;
	}
}
