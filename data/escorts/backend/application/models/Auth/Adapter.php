<?php
class Model_Auth_Adapter implements Zend_Auth_Adapter_Interface
{
	public function __construct($username, $password, $application_id, $use_light_version)
	{
		$this->username = $username;
		$this->password = $password;
		$this->application_id = $application_id;
		$this->use_light_version = $use_light_version;
	}

	public function authenticate()
	{
		$db = Zend_Registry::get('db');

		$user = $db->fetchRow('
			SELECT *
			FROM backend_users
			WHERE username = ? AND password = MD5(?) AND (application_id = ? OR application_id = 0) AND is_disabled = 0
		', array($this->username, $this->password, $this->application_id));

		if ( $user ) {
			$user->use_light_version = $this->use_light_version;
			return new Zend_Auth_Result(Zend_Auth_Result::SUCCESS, new Model_UserItem($user));
		}
		else {
			return new Zend_Auth_Result(Zend_Auth_Result::FAILURE, null);
		}
	}

	public function chatLoginAuth()
	{
		$db = Zend_Registry::get('db');

		$salt_hash = $this->getSaltByUsername($this->username);

		if ( ! $salt_hash ) {
			return false;
		}

		$password = Cubix_Salt::hashPassword($this->password, $salt_hash);

		$user_id = $db->fetchOne('
			SELECT id FROM users
			WHERE username = ? AND password = ? AND status = 1
		', array($this->username, $password));

		return $user_id;
		
		if ( ! $user_id ) {
			return false;
		}
		else {
			return true;
		}
	}

	protected function getSaltByUsername($username)
	{
		$db = Zend_Registry::get('db');

		$salt = $db->fetchOne('
			SELECT salt_hash
			FROM users
			WHERE username = ?
		', array($username));

		if ( ! $salt ) {
			return null;
		}

		return $salt;
	}
}
