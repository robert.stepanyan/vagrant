<?php

/**
 * Created by SceonDev.
 * User: Zhora
 * Date: 08.08.2017
 * Time: 10:52
 */
class Model_Statistics extends Cubix_Model
{
    private $db;

    public function __construct()
    {
        $this->db = Zend_Registry::get('db');
    }

    public function getAll($page, $per_page, $sort_field, $sort_dir, $filter, &$count)
    {

        $sql = '
                select ws.* ,COUNT(*) as count,MAX(date) as date 
                    from website_statistics ws
                        where 1
		';

        $countSql = '
			SELECT FOUND_ROWS()
		';

        $where = '';

        if ($filter['date_from']) {
            $where .= self::quote("AND DATE(ws.date) >= DATE(FROM_UNIXTIME(?)) ", $filter['date_from']);
        }

        if ($filter['date_to']) {
            $where .= self::quote(" AND DATE(ws.date) <= DATE(FROM_UNIXTIME(?)) ", $filter['date_to']);
        }

        if ($filter['user_type']) {
            $where .= self::quote(" AND user_type = ? ", $filter['user_type']);
        }

        if ($filter['link']) {
            $where .= self::quote(" AND link like ?", "%" . $filter['link'] . "%");
        }

        if ($filter['escort_id']) {
            $where .= self::quote(" AND escort_id = ? ", $filter['escort_id']);
        }

        if ($filter['agency_id']) {
            $where .= self::quote(" AND agency_id = ? ", $filter['agency_id']);
        }


        $sql .= $where;

        $sql .= '
			GROUP BY  ws.link
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . (($page - 1) * $per_page) . ', ' . $per_page . '
		';
        $data = parent::_fetchAll($sql);

        $count = intval($this->getAdapter()->fetchOne($countSql));

        return $data;
    }

}