<?php

class Model_PackageItem extends Cubix_Model_Item
{
	public function getPrices()
	{
		$filter = array($this->getId());		
		
		return $this->_adapter->query('
			SELECT
				pp.*
			FROM package_prices pp
			WHERE pp.package_id = ?'
		, $filter)->fetchAll();
	}

	public function getPackageProducts()
	{
		$tmp_products = parent::getAdapter()->query('SELECT pp.product_id, p.name FROM package_products pp INNER JOIN products p ON p.id = pp.product_id WHERE pp.is_optional = 0 AND pp.package_id = ?', $this->getId())->fetchAll();

		if ( count($tmp_products) == 0 )
			return array();

		$products = array();
		foreach($tmp_products as $k => $product)
		{
			$products[$k]['id'] = $product->product_id;
			$products[$k]['name'] = Cubix_I18n::translate($product->name);
		}

		return $products;
	}

	public function getOptionalProducts()
	{
		$addon_products = parent::getAdapter()->query('SELECT pp.product_id, p.name FROM package_products pp INNER JOIN products p ON p.id = pp.product_id WHERE pp.is_optional = 1 AND pp.package_id = ?', $this->getId())->fetchAll();

		$products = array();
		foreach($addon_products as $k => $product)
		{
			$products[$k]['id'] = $product->product_id;
			$products[$k]['name'] = Cubix_I18n::translate($product->name);
		}
		
		return $products;
	}
}
