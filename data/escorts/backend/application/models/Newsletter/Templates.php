<?php

class Model_Newsletter_Templates extends Cubix_Model
{
	protected $_table = 'newsletter_templates';
	protected $_itemClass = 'Model_Newsletter_TemplateItem';
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count = 0)
	{
		$sql = '
			SELECT
				id, title, from_email, subject AS subject
			FROM ' . $this->_table . '
			WHERE 1
		';
		
		$countSql = '
			SELECT
				COUNT(id)
			FROM ' . $this->_table . '
			WHERE 1
		';
		
		$where = '';
		
		if ( $filter['application_id'] ) {
			$where .= self::quote(' AND application_id = ?', $filter['application_id']);
		}

		if ( $filter['is_v2'] == 1 ) {
			$where .= ' AND is_v2 = 1';
		}
		
		// <-- Construct WHERE clause here
		
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$count = $this->getAdapter()->fetchOne($countSql);
		
		return parent::_fetchAll($sql);
	}
	
	public function getById($id)
	{
		return $this->_fetchRow('
			SELECT * FROM ' . $this->_table . ' WHERE id = ?
		', $id);
	}
	
	public function save($data)
	{
		if (isset($data['id']) && $data['id'] != '')
		{
			$id = $data['id'];

			parent::getAdapter()->update($this->_table, array(
				'title' => $data['title'],
				'from_email' => $data['from_email'],
				'subject' => $data['subject'],
				'body' => $data['body'],
				'body_plain' => $data['body_plain']
			), parent::quote('id = ?', $id));
		}
		else
		{
			$i_data = array(
				'title' => $data['title'],
				'from_email' => $data['from_email'],
				'subject' => $data['subject'],
				'body' => $data['body'],
				'body_plain' => $data['body_plain'],
				'application_id' => $data['application_id']
			);

			if ( in_array(Cubix_Application::getId(), array(APP_EF, APP_ED, APP_EI, APP_EG_CO_UK, APP_BL, APP_6B, APP_A6)) ) {
				$i_data['is_v2'] = $data['is_v2'];
			}

			parent::getAdapter()->insert($this->_table, $i_data);

			$id = parent::getAdapter()->lastInsertId();
		}

		return $id;
	}	
	
	public function remove($id)
	{
		parent::remove(parent::quote('id = ?', $id));
	}
	
	public function getList($data)
	{
		return parent::_fetchAll('
			SELECT
				id, title 
			FROM ' . $this->_table . '
			WHERE application_id = ' . $data['application_id'] . '
			ORDER BY id DESC
		');
	}

	public function checkTitle($title)
	{
		$count = $this->getAdapter()->fetchOne('SELECT COUNT(*) FROM ' . $this->_table . ' WHERE title = ?', $title);

		if ($count > 0)
			return true;
		else
			return false;
	}

	public function makeEscortList($escorts_data)
	{
		$m_photos = new Model_Escort_Photos();
		$m_escorts = new Model_Escorts();

		$escorts = array();

		foreach ( $escorts_data as $escort ) {
			list($escort_id, $photo_id) = explode(':', $escort);

			$photo = $m_photos->get($photo_id);
			$escort = $m_escorts->get($escort_id);
			$escort->photo = $photo;

			$escorts[] = $escort;
		}

		return $escorts;
	}

	public function saveNewsletter($request)
	{
		if ($request->send_time == 'now')
			$time = date('Y-m-d H:i:00', time() + 10 * 60);
		elseif ($request->send_time == 'custom')
			$time = date('Y-m-d H:i:00', $request->custom_time);

		if ( is_array($request->groups))
			$groups = implode(',', $request->groups);
		else
			$groups = null;
		
		$this->getAdapter()->insert('newsletters', array(
			'template_id' => $request->template_id,
			'from_email' => $request->from_email,
			'subject' => $request->subject,
			'body' => $request->email_body,
			'application_id' => $request->application_id,
			'groups' => $groups,
			'no_in_groups' => $request->no_group,
			'members' => $request->members,
			'creation_date' => new Zend_Db_Expr('NOW()'),
			'send_time' => $time
		));
	}
	
	public function getLayouts()
	{
		return $this->_fetchAll('SELECT * FROM newsletter_layouts');
	}
	
	public function getLayoutById($id)
	{
		return $this->_fetchRow('SELECT * FROM newsletter_layouts WHERE id = ?', array($id));
	}
}
