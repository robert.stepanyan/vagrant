<?php

class Model_Newsletter_NewslettersItem extends Cubix_Model_Item
{
	public function getVars()
	{
		$body = $this->getBody();
		$needle = '{EDITABLE_';
		$needle_end = '{/EDITABLE_';
				
		$vars = array();
		$data = array();

		preg_match_all("#" . $needle . "(.+?)}([\s\S]+?)" . $needle_end . "(.+?)}#", $body, $vars);

		if ( count($vars) > 0 && strlen($vars[0]) > 0 )
		{
			foreach($vars[1] as $i => $key)
			{
				$data[$key] = $vars[2][$i];
			}
		}

		unset($vars);

		return $data;
	}
	
	public function render($tpl_data, $lang_id = 'en')
	{
		$app = Cubix_Application::getById(Zend_Controller_Front::getInstance()->getRequest()->application_id);
		
		$tpl_data = array_merge(array(
			'app_title' => $app->title,
			'app_host' => $app->host,
			'app_url' => 'http://www.' . $app->host
		), $tpl_data);
		
		$html = $this->{'body_html_' . $lang_id};
		
		foreach ( $tpl_data as $var => $value ) {
			$html = str_replace('%' . $var . '%', $value, $html);
		}
		
		return $html;
	}
}
