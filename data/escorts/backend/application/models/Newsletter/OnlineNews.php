<?php

class Model_Newsletter_OnlineNews extends Cubix_Model
{
	protected $_table = 'online_news';
	
	const STATUS_ACTIVE   =  1;
	const STATUS_PENDING  =  2;
	const STATUS_EXPIRED  =  3;
	const STATUS_DISABLED =  4;
	
	
	const ACTIVATION_TYPE_IMMEDIATELY = 1;
	const ACTIVATION_TYPE_EXACT_DATE = 2;
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count = 0)
	{
		$sql = '
			SELECT
				id,title, '.Cubix_I18n::getTblFields('subject'). ', status, activation_type , UNIX_TIMESTAMP(DATE(start_date)) AS start_date , UNIX_TIMESTAMP(DATE(end_date)) AS end_date
			FROM ' . $this->_table . '
			WHERE 1
		';
		/*$sql = '
			SELECT
				id,title, subject, status, activation_type , start_date , end_date
			FROM ' . $this->_table . '
			WHERE 1
		';*/
		$countSql = '
			SELECT
				COUNT(id)
			FROM ' . $this->_table . '
			WHERE 1
		';
		
		$where = '';
		
		if ( $filter['application_id'] ) {
			$where .= self::quote(' AND application_id = ?', $filter['application_id']);
		}
		
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$count = $this->getAdapter()->fetchOne($countSql);
		
		return parent::_fetchAll($sql);
	}
	
	public function getById($id)
	{
		return $this->_fetchRow('
			SELECT * FROM ' . $this->_table . ' WHERE id = ?
		', $id);
	}
	
	public function save($data)
	{
		$app_id = Cubix_Application::getId();
		$data['start_date'] = $data['start_date'] ? new Zend_Db_Expr('FROM_UNIXTIME(' . $data['start_date'] . ')') : null;
		$data['end_date'] = $data['end_date'] ? new Zend_Db_Expr('FROM_UNIXTIME(' . $data['end_date'] . ')') : null;
		$data['application_id'] = $app_id;
		if (isset($data['id']) && $data['id'] != '')
		{
			$id = $data['id'];
			unset($data['id']);
			parent::getAdapter()->update($this->_table, $data, parent::quote('id = ?', $id));
		}
		else
		{
			parent::getAdapter()->insert($this->_table, $data);

			$id = parent::getAdapter()->lastInsertId();
		}

		return $id;
	}	
	
	public function remove($id)
	{
		parent::remove(parent::quote('id = ?', $id));
	}
	
	public function getAllDates(){
		
		$sql = '
			SELECT
				id, /*UNIX_TIMESTAMP(DATE(start_date)) AS*/ start_date , /*UNIX_TIMESTAMP(DATE(end_date)) AS*/ end_date
			FROM ' . $this->_table . '
			WHERE application_id = ?
		';
		return parent::_fetchAll($sql, array(Cubix_Application::getId()));
	}
	
	public function disableAllActive()
	{
		$this->getAdapter()->update($this->_table, array('status'=> self::STATUS_DISABLED ), parent::quote('status = ?', self::STATUS_ACTIVE));
	}
	
	public function setStatus($id,$status)
	{
		$this->getAdapter()->update($this->_table, array('status'=> $status ), parent::quote('id = ?', $id));
	}		
	
	public function getExpiredNews()
	{
		$sql = '
			SELECT id FROM '. $this->_table. '  
			WHERE end_date <= NOW() AND status = ? AND application_id = ?
		';
		return parent::_fetchAll($sql, array( self::STATUS_ACTIVE, Cubix_Application::getId()));
	}
	
	public function getPendingNews()
	{
		$sql = '
			SELECT id FROM online_news
			WHERE start_date <= NOW() AND end_date >= NOW() AND status = ? AND application_id = ?
				
		';
		return parent::_fetchAll($sql, array( self::STATUS_PENDING, Cubix_Application::getId()));
	}
}
