<?php

class Model_Newsletter_EmailCollecting extends Cubix_Model
{
	protected $_table = 'subscriber_log';

	public function getAll($page, $per_page, $sort_field, $sort_dir, &$count = 0, $filter)
	{
		$sql = '
			SELECT
				s.email, UNIX_TIMESTAMP(s.subscription_date) AS subscription_date
			FROM ' . $this->_table . ' s
		';

		$countSql = '
			SELECT
				COUNT(DISTINCT(s.subscription_date))
			FROM ' . $this->_table . ' s
		';

		$where = ' WHERE 1 ';

		if (trim($filter['email']) != '')
			$where .= ' AND (s.email LIKE \'%' . $filter['email'] . '%\')';
		if ($filter['date_from'] != '')
			$where .= ' AND s.subscription_date > \'' . date('Y-m-d H:i:s', $filter['date_from']) . '\'';
		if ($filter['date_to'] != '')
			$where .= ' AND s.subscription_date < \'' . date('Y-m-d H:i:s', $filter['date_to']) . '\'';

		$sql .= $where;
		$countSql .= $where;

		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$count = $this->getAdapter()->fetchOne($countSql);
		
		return parent::_fetchAll($sql);
	}

	public function addCollectingImages($escort_id, $ids){
		$db = parent::getAdapter();
		

		$data['escort_id'] = $escort_id;
		foreach( $ids as $id ) {
			$data['photo_id'] = $id;
			$db->insert('email_collecting_photos', $data);
		}
	}

	public function getAllPhoto(){
		$db = parent::getAdapter();

		$sql = '
			SELECT
				ecp.id, ecp.escort_id, ecp.photo_id
			FROM email_collecting_photos ecp
			INNER JOIN escort_photos ep ON ep.id = ecp.photo_id 
		';

		$where = ' WHERE 1 ';

		$photos = parent::_fetchAll($sql);
		

		foreach ($photos as $key => $p) {
			$photo = $db->query('
				SELECT * FROM escort_photos 
				WHERE id = ?
			', array($p->photo_id))->fetch();

			$photos[$key]->photo = new Model_Escort_PhotoItem($photo);
		}
		
		return $photos;
	}

	public function removeImage($id){
		parent::getAdapter()->query('DELETE FROM email_collecting_photos WHERE id = ?',$id);	
	}
}
