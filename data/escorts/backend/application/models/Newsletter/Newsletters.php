<?php

class Model_Newsletter_Newsletters extends Cubix_Model
{
	protected $_table = 'newsletters';
	protected $_itemClass = 'Model_Newsletter_NewslettersItem';
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count = 0)
	{
		$sql = '
			SELECT
				id, template_id, from_email, subject, body,
				application_id, groups, no_in_groups, members,
				UNIX_TIMESTAMP(send_time) AS send_time, status, emails_count, clicks,
				UNIX_TIMESTAMP(creation_date) AS creation_date
			FROM ' . $this->_table . '
			WHERE 1
		';
		
		$countSql = '
			SELECT
				COUNT(id)
			FROM ' . $this->_table . '
			WHERE 1
		';
		
		$where = '';
		
		if ( $filter['application_id'] ) {
			$where .= self::quote(' AND application_id = ?', $filter['application_id']);
		}

		if ( $filter['status'] ) {
			$where .= self::quote(' AND status = ?', $filter['status']);
		}

		if ($filter['date_from'] != '')
			$where .= ' AND creation_date >= \'' . date('Y-m-d', $filter['date_from']) . '\'';
		if ($filter['date_to'] != '')
			$where .= ' AND creation_date <= \'' . date('Y-m-d', $filter['date_to']) . '\'';
		
		// <-- Construct WHERE clause here
		
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$count = $this->getAdapter()->fetchOne($countSql);
		
		return parent::_fetchAll($sql);
	}

	public function updateStatus($id, $status)
	{
		$this->getAdapter()->update('newsletters', array(
			'status' => $status
		), parent::quote('id = ?', $id));
	}

	public function getById($id)
	{
		$item = $this->_fetchRow('
			SELECT * FROM ' . $this->_table . ' WHERE id = ?
		', $id);

		if ( ! $item )
			return null;

		return $item;
	}
	
	public function save($data)
	{
		if (isset($data['id']) && $data['id'] != '')
		{
			$id = $data['id'];

			parent::getAdapter()->update($this->_table, array(
				'title' => $data['title'],
				'from_email' => $data['from_email'],
				'subject' => $data['subject'],
				'body' => $data['body']
			), parent::quote('id = ?', $id));
		}
		else
		{
			parent::getAdapter()->insert($this->_table, array(
				'title' => $data['title'],
				'from_email' => $data['from_email'],
				'subject' => $data['subject'],
				'body' => $data['body'],
				'application_id' => $data['application_id']
			));

			$id = parent::getAdapter()->lastInsertId();
		}

		return $id;
	}	
	
	public function remove($id)
	{
		parent::remove(parent::quote('id = ?', $id));
	}
	
	public function getList()
	{
		return parent::_fetchAll('
			SELECT
				id, title 
			FROM ' . $this->_table . '
			ORDER BY id DESC
		');
	}

	public function checkTitle($title)
	{
		$count = $this->getAdapter()->fetchOne('SELECT COUNT(*) FROM ' . $this->_table . ' WHERE title = ?', $title);

		if ($count > 0)
			return true;
		else
			return false;
	}

	public function makeEscortList($escorts_data)
	{
		$m_photos = new Model_Escort_Photos();
		$m_escorts = new Model_Escorts();

		$escorts = array();

		foreach ( $escorts_data as $escort ) {
			list($escort_id, $photo_id) = explode(':', $escort);

			$photo = $m_photos->get($photo_id);
			$escort = $m_escorts->get($escort_id);
			$escort->photo = $photo;

			$escorts[] = $escort;
		}

		return $escorts;
	}

	public function updateNewsletter($id, $request)
	{
		if ($request->send_time == 'now')
			$time = date('Y-m-d H:i:00', time() + 10 * 60);
		elseif ($request->send_time == 'custom')
			$time = date('Y-m-d H:i:00', $request->custom_time);

		if ( is_array($request->groups))
			$groups = implode(',', $request->groups);
		else
			$groups = null;

		$this->getAdapter()->update('newsletters', array(
			'template_id' => $request->template_id,
			'from_email' => $request->from_email,
			'subject' => $request->subject,
			'body' => $request->body,
			'application_id' => $request->application_id,
			'groups' => $groups,
			'no_in_groups' => $request->no_group,
			'members' => $request->members,
			'creation_date' => new Zend_Db_Expr('NOW()'),
			'send_time' => $time
		), parent::quote('id = ?', $id));
	}
}
