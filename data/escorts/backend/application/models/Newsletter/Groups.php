<?php

class Model_Newsletter_Groups extends Cubix_Model
{
	protected $_table = 'newsletter_groups';
	protected $_itemClass = 'Model_Newsletter_TemplateItem';

	public function getAll($page, $per_page, $sort_field, $sort_dir, &$count = 0)
	{
		$sql = '
			SELECT
				id, group_title
			FROM ' . $this->_table . '
		';

		$countSql = '
			SELECT
				COUNT(id)
			FROM ' . $this->_table . '
		';

		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$count = $this->getAdapter()->fetchOne($countSql);

		return parent::_fetchAll($sql);
	}

	public function save(Model_Newsletter_TemplateItem $item)
	{
		parent::save($item);

		return parent::getAdapter()->lastInsertId();
	}

	public function getById($id)
	{
		return $this->_fetchRow('
			SELECT * FROM ' . $this->_table . ' WHERE id = ?
		', $id);
	}

	public function remove($id)
	{
		parent::remove(parent::quote('id = ?', $id));
		$this->removeGroups($id);
	}

	public function removeGroups($id)
	{
		$this->getAdapter()->delete('newsletter_subscribers_groups', parent::quote('group_id = ?', $id));
	}

	public function getList()
	{
		return parent::_fetchAll('
			SELECT
				id, group_title
			FROM ' . $this->_table . '
			ORDER BY id
		');
	}
}
