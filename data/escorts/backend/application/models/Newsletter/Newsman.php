<?php

class Model_Newsletter_Newsman extends Cubix_Model
{
	protected $_rpc_url = '';

	public function __construct()
	{
		$NEWSMAN_user_id = 61;
		$NEWSMAN_rpc_url = "https://ssl.newsman.ro/rpc";
		$API_key = "ec07223011108dc657660de62bdb9796";

		$this->_rpc_url = $NEWSMAN_rpc_url . "/" . $NEWSMAN_user_id . "/" . $API_key;
		$_SERVER["REMOTE_ADDR"] = "127.0.0.1";
		//echo $this->_rpc_url;die;
	}
	
	
	/**
		method: ab.saveSubscribe
		params:
			list_id - int
			email - string (lowercase trim pls)
			firstname - string // utf-8
			lastname - string // utf-8
			ip - string (ip of subscriber $_SERVER["REMOTE_ADDR"]
			props - structure ($abonat_props)

		return:
			int - newsman subscriber_id of newly subscribed user
	*/
	public function subscribe($LIST_ID, $email, $firstname, $lastname = "", $props = array())
	{		
		/*$abonat_props = array(
			"age" => 28,
			"sex" => "m", // m = male, f = female (can be anything)
			"city" => "Bucharest",
			"from" => "top_right_form",
			"registered_username" => "catalin",
			"country" => "Romania"
		);*/
		$client = new Zend_XmlRpc_Client($this->_rpc_url);
		
		try {
			
			$subscriber_id = $client->call("ab.saveSubscribe", array($LIST_ID, $email, $firstname, $lastname, $_SERVER["REMOTE_ADDR"], $props));
			
			return $subscriber_id;
		} catch (Zend_XmlRpc_Client_FaultException $e) { 
			// $e->getCode() returns 404
			var_dump($e->getMessage());
		} catch (Zend_Http_Client_Adapter_Exception $e) { 
			// $e->getCode() returns 404
			var_dump($e->getMessage());
		}
	}
	
	/**
	method: ab.saveUnsubscribe
	params:
		list_id - int
		email - string (lowercase trim pls)
		ip - string (ip of user unsubscribing $_SERVER["REMOTE_ADDR"]
	
	return:
		int - newsman subscriber_id of unsubscribed user
	*/
	public function unsubscribe($LIST_ID, $email)
	{
		$client = new Zend_XmlRpc_Client($this->_rpc_url);
		
		try {
			$subscriber_id = $client->call("ab.saveUnsubscribe", array($LIST_ID, $email, $_SERVER["REMOTE_ADDR"]));
			
			return $subscriber_id;
		} catch (Zend_XmlRpc_Client_FaultException $e) { 
			// $e->getCode() returns 404
			var_dump($e->getMessage());
		}
		catch (Zend_Http_Client_Adapter_Exception $e) { 
			// $e->getCode() returns 404
			var_dump($e->getMessage());
		}
		
		/*$request = xmlrpc_encode_request("ab.saveUnsubscribe", array($LIST_ID, $email, $_SERVER["REMOTE_ADDR"]), array("encoding" => "utf-8"));
		$context = stream_context_create(array("http" => array("method" => "POST", "header" => "Content-Type: text/xml", "content" => $request)));
		$raw_output = file_get_contents($this->_rpc_url, false, $context);
		$output = xmlrpc_decode($raw_output);
		
		if (@xmlrpc_is_fault($output)) {
			print("ERROR\n");
			var_dump($output);
		} 
		else {
			$subscriber_id = $output;
			return $subscriber_id;
		}*/
	}
	
	
	/**
		method: sg.subscribeAbonat
		params:
			subscriber_id - int
			segment_id - int

		return:
			string - OK for all ok
	*/
	public function bindToSegment($subscriber_id, $segment_id)
	{
		$client = new Zend_XmlRpc_Client($this->_rpc_url);
		
		try {
			$client->call("sg.subscribeAbonat", array($subscriber_id, $segment_id));
			
			return true;
		} catch (Zend_XmlRpc_Client_FaultException $e) { 
			// $e->getCode() returns 404
			var_dump($e->getMessage());
		}
		catch (Zend_Http_Client_Adapter_Exception $e) { 
			// $e->getCode() returns 404
			var_dump($e->getMessage());
		}
		
		/*$request = xmlrpc_encode_request("sg.subscribeAbonat", array($subscriber_id, $segment_id));
		$context = stream_context_create(array("http" => array("method" => "POST", "header" => "Content-Type: text/xml", "content" => $request)));
		$raw_output = file_get_contents($this->_rpc_url, false, $context);
		$output = xmlrpc_decode($raw_output);
		
		if (@xmlrpc_is_fault($output))
		{
			return false;
		} else
		{
			return true;
		}*/
	}
}
