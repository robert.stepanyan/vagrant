<?php

class Model_Newsletter_Subscribers extends Cubix_Model
{
	protected $_table = 'newsletter_subscribers';
	protected $_itemClass = 'Model_Newsletter_TemplateItem';

	public function getAll($page, $per_page, $sort_field, $sort_dir, &$count = 0, $filter)
	{
		$sql = '
			SELECT
				s.id, s.email, s.name, UNIX_TIMESTAMP(s.reg_date) AS reg_date, s.status
			FROM ' . $this->_table . ' s
			LEFT JOIN newsletter_subscribers_groups s_g ON s_g.subscriber_id = s.id
		';

		$countSql = '
			SELECT
				COUNT(DISTINCT(s.id))
			FROM ' . $this->_table . ' s
			LEFT JOIN newsletter_subscribers_groups s_g ON s_g.subscriber_id = s.id
		';

		$where = ' WHERE 1 ';

		if (trim($filter['email']) != '')
			$where .= ' AND (s.name LIKE \'%' . $filter['email'] . '%\' OR s.email LIKE \'%' . $filter['email'] . '%\')';
		if ($filter['status'] !== '')
			$where .= ' AND s.status = ' . $filter['status'];
		if ($filter['group'] != '')
			$where .= ' AND s_g.group_id = ' . $filter['group'];
		if ($filter['date_from'] != '')
			$where .= ' AND s.reg_date > \'' . date('Y-m-d H:i:s', $filter['date_from']) . '\'';
		if ($filter['date_to'] != '')
			$where .= ' AND s.reg_date < \'' . date('Y-m-d H:i:s', $filter['date_to']) . '\'';

		$sql .= $where;
		$countSql .= $where;

		$sql .= '
			GROUP BY s.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$count = $this->getAdapter()->fetchOne($countSql);
		
		return parent::_fetchAll($sql);
	}

	public function getGroupsById($id)
	{
		$sql = 'SELECT group_id FROM newsletter_subscribers_groups WHERE subscriber_id = ' . $id . ' ORDER BY group_id';
		return $this->getAdapter()->fetchAll($sql);
	}

	public function getGroups($subscriber_id)
	{
		$modelGroup = new Model_Newsletter_Groups();

		$cross = $this->getGroupsById($subscriber_id);
		$str = '';

		foreach ($cross as $c)
		{
			$str .= $modelGroup->getById($c->group_id)->group_title . ", ";
		}

		$groups = substr($str, 0, strlen($str) - 2);

		return $groups;
	}

	public function removeGroups($id)
	{
		$this->getAdapter()->delete('newsletter_subscribers_groups', parent::quote('subscriber_id = ?', $id));
	}

	public function save($data)
	{
		if (isset($data['id']) && $data['id'] != '')
		{
			$id = $data['id'];
			$this->removeGroups($id);
			parent::getAdapter()->update($this->_table, array(
				'email' => $data['email'],
				'name' => $data['name']
			), parent::quote('id = ?', $id));
		}
		else
		{
			parent::getAdapter()->insert($this->_table, array(
				'email' => $data['email'],
				'name' => $data['name'],
				'reg_date' => new Zend_Db_Expr('NOW()')
			));

			$id = parent::getAdapter()->lastInsertId();
		}

		foreach ($data['groups'] as $group)
		{
			parent::getAdapter()->insert('newsletter_subscribers_groups', array(
				'subscriber_id' => $id,
				'group_id' => $group
			));
		}

		return $id;
	}

	public function getById($id)
	{
		return $this->_fetchRow('
			SELECT * FROM ' . $this->_table . ' WHERE id = ?
		', $id);
	}

	public function remove($id)
	{
		parent::remove(parent::quote('id = ?', $id));
		$this->removeGroups($id);
	}

	public function getExportList()
	{
		$subscribers = $this->getAdapter()->fetchAll('SELECT * FROM newsletter_subscribers');

		foreach ($subscribers as $s)
		{
			$list .= $s->email . ', ';
		}

		return $list;
	}

	public function import($data)
	{
		$ses = new Zend_Session_Namespace('newsletter');
		$email_list = $data['email_list'];
		
		if ($email_list)
		{
			$this->_importValidEmails($email_list, $data['group']);
		}

		if (isset($ses->file_name))
		{
			$this->_importValidEmails(file_get_contents(APPLICATION_PATH . '/tmp_files/' . $ses->file_name), $data['group']);
			unlink(APPLICATION_PATH . '/tmp_files/' . $ses->file_name);
			$ses->unsetAll();
		}
	}

	protected function _importValidEmails($emails, $group)
	{
		if (preg_match_all('#\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b#is', $emails, $m))
		{
			foreach ($m[0] as $email)
			{
				$this->getAdapter()->insert($this->_table, array(
					'email' => $email,
					'reg_date' => new Zend_Db_Expr('NOW()')
				));

				$id = $this->getAdapter()->lastInsertId($this->_table);
				
				$this->getAdapter()->insert('newsletter_subscribers_groups', array(
					'subscriber_id' => $id,
					'group_id' => $group
				));
			}
		}
	}

	public function toggle($id)
	{
		$this->getAdapter()->update($this->_table, array(
			'status' => new Zend_Db_Expr('NOT ' . 'status')
		), $this->_db->quoteInto('id = ?', $id));
	}

	public function deleteChecked($ids)
	{
		foreach ($ids as $id)
		{
			$this->remove($id);
		}
	}

	public function noInGroups()
	{
		$sql = 'SELECT * FROM ' . $this->_table . ' s ';

		$sqlCount = 'SELECT COUNT(DISTINCT(s.id)) FROM ' . $this->_table . ' s ';

		$join = 'LEFT JOIN newsletter_subscribers_groups s_g ON s.id = s_g.subscriber_id ';
		$where = 'WHERE s_g.group_id IS NULL ';

		$sql = $sql . $join . $where;
		$sqlCount = $sqlCount . $join . $where;

		$sql .= 'GROUP BY s.id';

		$count = $this->getAdapter()->fetchOne($sqlCount);
		$items = $this->getAdapter()->fetchAll($sql);
		
		return array('count' => $count, 'items' => $items);
	}
}
