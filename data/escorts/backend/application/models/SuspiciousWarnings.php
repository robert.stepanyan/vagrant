<?php

class Model_SuspiciousWarnings extends Cubix_Model
{
	static public function getSuspiciousEscorts()
	{
		$sql = '
			SELECT e.id, e.showname, e.user_id, ep.contact_phone_parsed AS phone
			FROM escorts e
			INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
			WHERE e.agency_id IS NULL AND is_suspicious = 1
		';
		return self::db()->fetchAll($sql);
	}

	static public function getByPhone($escort)
	{
		$sql = '
			SELECT e.id, e.showname
			FROM escort_profiles_v2 ep
			INNER JOIN escorts e ON e.id = ep.escort_id AND e.agency_id IS NULL
			WHERE ep.contact_phone_parsed = ? AND ep.escort_id != ?
		';
		return self::db()->fetchAll($sql, array($escort->phone, $escort->id));
	}

	static public function getByMatchingIps($escort)
	{
		$sql = '
			SELECT e.id, e.showname, ull.ip
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN users_last_login ull ON ull.user_id = u.id
			WHERE e.agency_id IS NULL AND ull.ip IN (
				SELECT ull.ip
				FROM users_last_login ull
				WHERE ull.user_id = ?
			)
		';
		return self::db()->fetchAll($sql, $escort->user_id);
	}

	static public function getWarnings($page = 1, $limit = 10, &$count = 0)
	{
		$warnings = array();

		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				sw.id AS wid, e.id, e.showname, sw.date
			FROM suspicious_warning sw
			INNER JOIN escorts e ON e.user_id = sw.user_id
			WHERE e.agency_id IS NULL AND EXISTS(
				SELECT TRUE
				FROM suspicious_warning_user _swu
				INNER JOIN escorts _e ON _e.user_id = _swu.user_id
				WHERE _swu.warning_id = sw.id AND _e.status & 32
			) AND e.status & 32
			ORDER BY sw.date DESC
			LIMIT ' . (($page - 1) * $limit) . ', ' . $limit . '
		';
		$result = self::db()->fetchAll($sql);
		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');

		$match_sql = '
			SELECT
				e.id, e.showname, swu.type
			FROM suspicious_warning_user swu
			INNER JOIN escorts e ON e.user_id = swu.user_id
			WHERE swu.warning_id = ? AND e.agency_id IS NULL AND e.status & 32
		';

		foreach ( $result as $escort ) {
			$warning = (object) array('wid' => $escort->wid, 'id' => $escort->id, 'showname' => $escort->showname, 'date' => $escort->date);

			$matches = self::db()->fetchAll($match_sql, $escort->wid);
			$warning->match = (object) array('by_ip' => array(), 'by_client' => array());
			foreach ( $matches as $match ) {
				if ( $match->type == 1 ) { // by_ip
					$warning->match->by_ip[] = $match;
				}
				elseif ( $match-> type == 2 ) { // by_client
					$warning->match->by_client[] = $match;
				}
			}

			$warnings[] = $warning;
		}

		return $warnings;
	}

	static public function getWarningsFormatted($page = 1, $limit = 3, &$count = 0)
	{
		$result = array();
		
		$cache = Zend_Registry::get('cache');
		
		$cache_key = 'v2_statistics_' . Cubix_Application::getId() . $page . $limit;
		
		if ( ! $result = $cache->load($cache_key) )
		{
		
			$warnings = Model_SuspiciousWarnings::getWarnings($page, $limit, $count);
			$link_tpl = '<a class="escort-showname" href="http://www.' . Cubix_Application::getById()->host . '/escort/%s" rel="/escorts-v2/revisions?escort_id=%d" onclick="return Dashboard.ShowRevision(this)">%s</a>';

			foreach ( $warnings as $x => $w ) {
				$date = date('d M Y', strtotime($w->date));
				if ( ! isset($result[$date]) ) $result[$date] = array();

				$str = '';
				$str .= '[' . date('H:i:s', strtotime($w->date)) . "] Escort " . ($w->showname ? sprintf($link_tpl, $w->showname . '-' . $w->id, $w->id, $w->showname) : sprintf($link_tpl, $w->id, $w->id, '#' . $w->id)) . "";

				$_s = '';

				$by_ip = $w->match->by_ip;
				foreach ( $by_ip as $i => $b ) {
					if ( $b->showname ) {
						$by_ip[$i] = sprintf($link_tpl, $b->showname . '-' . $b->id, $b->id, $b->showname);
					}
					else {
						$by_ip[$i] = sprintf($link_tpl, $b->id, $b->id, '#' . $b->id);
					}
				}
				if ( count($by_ip) ) $_s .= 'IP: ' . implode(', ', $by_ip) . '';

				$by_client = $w->match->by_client;
				foreach ( $by_client as $i => $b ) {
					if ( $b->showname ) {
						$by_client[$i] = sprintf($link_tpl, $b->showname . '-' . $b->id, $b->id, $b->showname);
					}
					else {
						$by_client[$i] = sprintf($link_tpl, $b->id, $b->id, '#' . $b->id);
					}
				}
				if ( count($by_client) ) $_s .= (count($by_ip) ? '<br/>' : '') . 'CID: ' . implode(', ', $by_client) . '';


				if ( strlen($_s) ) {
					$str .= '<div class="list">' . $_s . '</div>';
				}
				else {
					$str .= $_s;
				}

				$result[$date][] = $str;
			}
			
			$cache->save($result, $cache_key, array(), 1200);
		}

		return $result;
	}

	static public function getWarningsList($page = 1, $limit = 3, &$count = 0)
	{
		$result = array();
		$warnings = Model_SuspiciousWarnings::getWarnings($page, $limit, $count);
		$link_tpl = '<a class="escort-showname" href="http://www.' . Cubix_Application::getById()->host . '/escort/%s" rel="/escorts-v2/revisions?escort_id=%d" target="_blank">%s</a>';

		foreach ( $warnings as $x => $w ) {
			$date = date('d M Y', strtotime($w->date));
			if ( ! isset($result[$date]) ) $result[$date] = array();

			$str = '';
			$str .= '[' . date('H:i:s', strtotime($w->date)) . "] Escort " . ($w->showname ? sprintf($link_tpl, $w->showname . '-' . $w->id, $w->id, $w->showname) : sprintf($link_tpl, $w->id, $w->id, '#' . $w->id)) . "";

			$_s = '';

			$by_ip = $w->match->by_ip;
			foreach ( $by_ip as $i => $b ) {
				if ( $b->showname ) {
					$by_ip[$i] = sprintf($link_tpl, $b->showname . '-' . $b->id, $b->id, $b->showname);
				}
				else {
					$by_ip[$i] = sprintf($link_tpl, $b->id, $b->id, '#' . $b->id);
				}
			}
			if ( count($by_ip) ) $_s .= 'IP: ' . implode(', ', $by_ip) . '';

			$by_client = $w->match->by_client;
			foreach ( $by_client as $i => $b ) {
				if ( $b->showname ) {
					$by_client[$i] = sprintf($link_tpl, $b->showname . '-' . $b->id, $b->id, $b->showname);
				}
				else {
					$by_client[$i] = sprintf($link_tpl, $b->id, $b->id, '#' . $b->id);
				}
			}
			if ( count($by_client) ) $_s .= (count($by_ip) ? '<br/>' : '') . 'CID: ' . implode(', ', $by_client) . '';


			if ( strlen($_s) ) {
				$str .= '<div class="list">' . $_s . '</div>';
			}
			else {
				$str .= $_s;
			}

			$result[$date][] = $str;
		}

		return $result;
	}

	static public function getWarningAboutEscort($escort_id)
	{
		$sql = '
			SELECT
				sw.id AS wid, e.id, e.showname, sw.date
			FROM suspicious_warning sw
			INNER JOIN escorts e ON e.user_id = sw.user_id
			WHERE e.agency_id IS NULL AND EXISTS(
				SELECT TRUE
				FROM suspicious_warning_user _swu
				INNER JOIN escorts _e ON _e.user_id = _swu.user_id
				WHERE _swu.warning_id = sw.id AND _e.status & 32
			) AND e.status & 32 AND e.id = ?
			ORDER BY sw.date DESC
		';
		$warning = self::db()->fetchRow($sql, $escort_id);
		if ( ! $warning ) return null;
		
		$match_sql = '
			SELECT
				e.id, e.showname, swu.type
			FROM suspicious_warning_user swu
			INNER JOIN escorts e ON e.user_id = swu.user_id
			WHERE swu.warning_id = ? AND e.agency_id IS NULL AND e.status & 32
		';

		$matches = self::db()->fetchAll($match_sql, $warning->wid);
		$warning->match = (object) array('by_ip' => array(), 'by_client' => array());
		foreach ( $matches as $match ) {
			if ( $match->type == 1 ) { // by_ip
				$warning->match->by_ip[] = $match;
			}
			elseif ( $match-> type == 2 ) { // by_client
				$warning->match->by_client[] = $match;
			}
		}

		return $warning;
	}

	static public function getWarningAboutEscortFormatted($escort_id)
	{
		$w = self::getWarningAboutEscort($escort_id);
		if ( ! $w ) return null;

		$link_tpl = '<a class="escort-showname" href="http://www.' . Cubix_Application::getById()->host . '/escort/%s" rel="/escorts-v2/revisions?escort_id=%d" target="_blank">%s</a>';
		$_s = '';

		$by_ip = $w->match->by_ip;
		foreach ( $by_ip as $i => $b ) {
			if ( $b->showname ) {
				$by_ip[$i] = sprintf($link_tpl, $b->showname . '-' . $b->id, $b->id, $b->showname);
			}
			else {
				$by_ip[$i] = sprintf($link_tpl, $b->id, $b->id, '#' . $b->id);
			}
		}
		if ( count($by_ip) ) $_s .= '<strong>Matches by IP:</strong> ' . implode(', ', $by_ip) . '';

		$by_client = $w->match->by_client;
		foreach ( $by_client as $i => $b ) {
			if ( $b->showname ) {
				$by_client[$i] = sprintf($link_tpl, $b->showname . '-' . $b->id, $b->id, $b->showname);
			}
			else {
				$by_client[$i] = sprintf($link_tpl, $b->id, $b->id, '#' . $b->id);
			}
		}
		if ( count($by_client) ) $_s .= (count($by_ip) ? '<br/>' : '') . '<strong>Matches by Cookie:</strong> ' . implode(', ', $by_client) . '';

		return $_s;
	}
}
