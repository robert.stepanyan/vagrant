<?php

class Model_CookieDublicities extends Cubix_Model
{	
	public static function getList($params = array(), $page = 1, $limit = 10, &$count = 0)
	{
		$db = self::db();
		
		$where = '';
		$join = '';
				
		if (strlen(trim($params['member'])) > 0)
		{
			$join .= " LEFT JOIN users u ON c.user_id = u.id";
			if(isset($params['partial'])){
				$where .= " AND u.username LIKE '" . $params['member'] . "%'";
			}
			else{
				$where .= " AND u.username = '" . $params['member'] . "'";
			}
			
		}
		
		if (strlen(trim($params['escort'])) > 0)
		{
			$join .= " LEFT JOIN escorts e ON c.user_id = e.user_id";
			if(isset($params['partial'])){
				$where .= " AND e.showname LIKE '" . $params['escort'] . "%'";
			}
			else{
				$where .= " AND e.showname = '" . $params['escort'] . "'";
			}
			
		}
		
		if (strlen(trim($params['agency'])) > 0)
		{
			$join .= " LEFT JOIN agencies a ON c.user_id = a.user_id";
			if(isset($params['partial'])){
				$where .= " AND a.name LIKE '" . $params['agency'] . "%'";
			}
			else{
				$where .= " AND a.name = '" . $params['agency'] . "'";
			}
			
		}
		
		$users = $db->query("
			SELECT SQL_CALC_FOUND_ROWS GROUP_CONCAT(c.client_id) AS client_ids, c.user_id, c.created
			FROM client_cookie c " . $join . " 
			WHERE 1 " . $where . "
			GROUP BY c.user_id
			ORDER BY c.created DESC
			LIMIT " . (($page - 1) * $limit) . ", " . $limit . "
		")->fetchAll();
		
		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');
		
		$arr = array();
		
		foreach ($users as $k => $user)
		{
			$parent_user = $db->query("
				SELECT u.user_type, if (u.user_type = 'escort', e.showname, if (u.user_type = 'agency', a.name, u.username)) as name, e.id as escort_id, a.id as agency_id, a.slug AS agency_slug, u.status
				FROM users u
				LEFT JOIN escorts e on u.id = e.user_id
				LEFT JOIN agencies a on u.id = a.user_id
				WHERE u.id = ?
			", $user->user_id)->fetch();
						
			$ar = explode(',', $user->client_ids);
			$ar2 = array();
			
			foreach ($ar as $a)
				$ar2[] = '"' . $a . '"';
			
			$str = implode(',', $ar2);
									
			$items = $db->query("
				SELECT 
					u.user_type, u.id AS user_id, u.status, e.id AS escort_id, m.is_suspicious, a.id AS agency_id,
					IF(u.user_type = 'escort', (SELECT showname FROM escorts WHERE user_id = u.id), IF(u.user_type = 'agency', (SELECT name FROM agencies WHERE user_id = u.id), u.username)) AS name
				FROM client_cookie c
				INNER JOIN users u ON u.id = c.user_id
				LEFT JOIN escorts e ON e.user_id = c.user_id
				LEFT JOIN members m ON m.user_id = c.user_id
				LEFT JOIN agencies a on c.user_id = a.user_id
				WHERE c.client_id IN (" . $str . ") AND c.user_id <> ?
			", $user->user_id)->fetchAll();
			
			if (count($items))			
			{
				$arr[$k]['parent_user_id'] = $user->user_id;
				$arr[$k]['parent_user_created'] = $user->created;
				$arr[$k]['parent_user_type'] = $parent_user->user_type;
				$arr[$k]['parent_user_status'] = $parent_user->status;
				$arr[$k]['parent_user_name'] = $parent_user->name;
				$arr[$k]['parent_user_escort_id'] = $parent_user->escort_id;
				$arr[$k]['parent_user_agency_id'] = $parent_user->agency_id;
				$arr[$k]['parent_user_agency_slug'] = $parent_user->agency_slug;
				$arr[$k]['items'] = $items;
			}
		}
			
		return $arr;
	}
}
