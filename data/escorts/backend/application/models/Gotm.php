<?php
class Model_Gotm extends Cubix_Model
{

	public function get($id)
	{
		return $this->db()->query('SELECT id, votes_count FROM escorts WHERE id = ?', $id)->fetch();
	}
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = ' 
			SELECT SQL_CALC_FOUND_ROWS e.id, e.showname, e.votes_count, e.voted_members, e.gender
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			WHERE 1 
		';
		
		$where = '';

		if ( strlen($filter['application_id']) ) {
			$where .= self::quote('AND u.application_id = ?', $filter['application_id']);
		}
				
		if ( strlen($filter['showname']) ) {
			$where .= self::quote(' AND e.showname LIKE ? ', $filter['showname'] . '%');
		}
				
		if ( $filter['id'] ) {
			$where .= self::quote('AND e.id = ?', $filter['id']);
		}
						
		$sql .= $where;
		
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$data = $this->db()->query($sql)->fetchAll();
		$count = $this->db()->fetchOne('SELECT FOUND_ROWS()');
		
		return $data;
	}	
	
	public function save($data)
	{
		$this->db()->update('escorts', array('votes_count' => $data['votes_count']), $this->db()->quoteInto('id = ?', $data['id']));
	}
	
	public function saveWinnerId($winner_id)
	{
		$user = Zend_Auth::getInstance()->getIdentity();
		$this->db()->delete('gotm_winner_id', array('status'=> 1) );
		if($winner_id != ""){
			$this->db()->insert('gotm_winner_id', array('winner_id' => $winner_id, 'status' => 1, 'admin_id' =>$user->id, 'date' => new Zend_Db_Expr('NOW()')));
		}
	}
	
	public function getWinnerId()
	{
		return $this->db()->fetchOne('SELECT winner_id FROM gotm_winner_id WHERE status = 1') ;
	}
	
	public function getUsersByIds($ids)
	{
		return $this->db()->query("SELECT u.id, u.username, u.status, m.is_suspicious FROM users u 
			LEFT join members m ON m.user_id = u.id
			WHERE u.id IN (" . $ids . ") 
			ORDER BY FIND_IN_SET (u.id, '" . $ids . "')")->fetchAll();
	}
	
	public function getWinners($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS e.showname, gotm.escort_id, gotm.month, gotm.year,gotm.votes_count,gotm.voted_members, bu.username AS sales
			FROM girl_of_month gotm
			INNER JOIN escorts e ON e.id = gotm.escort_id 
			LEFT JOIN backend_users bu ON bu.id = gotm.sales_user_id 
			WHERE 1 ' ;
		
		$where = '';

		if ( strlen($filter['application_id']) ) {
			$where .= self::quote('AND gotm.application_id = ?', $filter['application_id']);
		}
				
		$sql .= $where;
		
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$data = $this->db()->query($sql)->fetchAll();
		$count = $this->db()->fetchOne('SELECT FOUND_ROWS()');
		
		return $data;
	}		
}
