<?php
class Model_ClientBlacklist extends Cubix_Model
{
	protected $_table = 'blacklisted_clients';

	public function getById($id)
	{
		$sql = '
			SELECT
				u.username, bl.id AS bl_id, UNIX_TIMESTAMP(bl.date) AS date, bl.client_name, bl.client_phone, bl.comment, bl.is_approved
			FROM blacklisted_clients bl
			LEFT JOIN users u ON bl.user_id = u.id
			WHERE 1 AND bl.id = ?
		';

		return parent::_fetchRow($sql, array($id));
	}
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT 
				u.username, bl.id AS bl_id, UNIX_TIMESTAMP(bl.date) AS date, bl.client_name, bl.client_phone, bl.comment, bl.is_approved, u.email
			FROM blacklisted_clients bl
			LEFT JOIN users u ON bl.user_id = u.id
			WHERE bl.application_id = ' . Cubix_Application::getId() . '
		';
		
		$countSql = '
			SELECT COUNT(DISTINCT(bl.id))
			FROM blacklisted_clients bl
			LEFT JOIN users u ON bl.user_id = u.id
			WHERE bl.application_id = ' . Cubix_Application::getId() . '
		';
		
		$where = '';
				
		if ( strlen($filter['username']) ) {
			$where .= self::quote('AND u.username LIKE ?', $filter['username'] . '%');
		}

		if ( strlen($filter['client_name']) ) {
			$where .= self::quote('AND bl.client_name LIKE ?', '%' . $filter['client_name'] . '%');
		}

		if ( strlen($filter['client_phone']) ) {
			$where .= self::quote('AND bl.client_phone LIKE ?', '%' . $filter['client_phone'] . '%');
		}

		if ( strlen($filter['comment']) ) {
			$where .= self::quote('AND bl.comment LIKE ?', '%' . $filter['comment'] . '%');
		}
				
		if ( $filter['status'] ) {
			$status = 1;
			if ( $filter['status'] == 3 ) {
				$status = 0;
			}
			$where .= self::quote('AND bl.is_approved = ?', $status);
		}
				
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			GROUP BY bl.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$count = intval($this->getAdapter()->fetchOne($countSql));

		return parent::_fetchAll($sql);
	}	
	
	public function getImages($id)
	{
		$sql = '
				SELECT 
						image_id, hash, ext
					FROM blacklisted_clients_images as bli
					LEFT JOIN images i ON i.id = bli.image_id
					WHERE bli.bl_id = '.$id.'
				';

		return parent::_fetchAll($sql);		
	}

	public function save($data)
	{
		if (isset($data['id']) && $data['id'] != '') {
			$id = $data['id'];
			unset($data['id']);

			if(isset($data['attach'])){
				$photos = $data['attach'];
				unset($data['attach']);
			}

			parent::getAdapter()->update($this->_table, $data, parent::quote('id = ?', $id));

			if ( $id && count($photos) ) {
				foreach( $photos as $photo ) {
					parent::getAdapter()->insert('blacklisted_clients_images', array('bl_id' => $id, 'image_id' => $photo));
				}
			}
		}
	}

	public function insert($data)
	{
		if(isset($data['attach'])){
			$photos = $data['attach'];
			unset($data['attach']);
		}
		
		parent::getAdapter()->insert($this->_table, $data);
		$bl_id = parent::getAdapter()->lastInsertId();

		if ( $bl_id && count($photos) ) {
			foreach( $photos as $photo ) {
				parent::getAdapter()->insert('blacklisted_clients_images', array('bl_id' => $bl_id, 'image_id' => $photo));
			}
		}
	}

	public function toggle($id)
	{
		$field = 'is_approved';
		
		parent::getAdapter()->update($this->_table, array(
			$field => new Zend_Db_Expr('NOT ' . $field)
		), parent::quote('id = ?', $id));
	}
	
	
	public function remove($id)
	{
		parent::getAdapter()->delete($this->_table, parent::quote('id = ?', $id));

		$sql = '
				SELECT image_id
					FROM blacklisted_clients_images
					WHERE bl_id = '.$id.'
				';
		$bl_images = parent::_fetchAll($sql);	

		if ( count($bl_images) ) {
			foreach( $bl_images as $image ) {
				parent::getAdapter()->delete('images', parent::quote('id = ?', $image->image_id));
			}
		}

		parent::getAdapter()->delete("blacklisted_clients_images", parent::quote('bl_id = ?', $id));
	}

	public function removeImage($id)
	{
		parent::getAdapter()->delete("blacklisted_clients_images", parent::quote('image_id = ?', $id));
		parent::getAdapter()->delete("images", parent::quote('id = ?', $id));
	}
}
