<?php

/**
 * Class Model_Chatroom
 *
 * @author Eduard Hovhannisyan
 */
class Model_Chatroom extends Cubix_Model
{
    const PRIVACY_LEVEL_GENERAL = 1;
    const PRIVACY_LEVEL_PRIVATE_GROUP = 2;
    const PRIVACY_LEVEL_DIRECT = 3;

    /**
     * @param $page
     * @param $per_page
     * @param $filter
     * @param $sort_field
     * @param $sort_dir
     * @param null $count
     * @return array
     */
    public function getConversations($page, $per_page, $filter, $sort_field, $sort_dir, &$count = null)
    {
        $sql = '
			SELECT SQL_CALC_FOUND_ROWS 	
                   cut.thread_id, GROUP_CONCAT(cut.user_id) as user_ids, 
                   ct.title, ct.privacy_level, 
                   ct.date_created, count(cut.user_id) as participantsCount
			
			FROM chatroom_user_threads cut 
			LEFT JOIN chatroom_threads ct ON ct.id = cut.thread_id
			WHERE 1
			
		';

        if ($filter['query']) {
            $rows = parent::_fetchAll("SELECT thread_id from chatroom_messages WHERE text like '%{$filter['query']}%'");
            $threadIds = [];
            foreach ($rows as $row) $threadIds[] = $row->thread_id;

            if (empty($threadIds)) return [];

            $sql .= 'AND cut.thread_id in (' . implode(',', $threadIds) . ')';
        }

        if ($filter['username']) {
            $rows = parent::_fetchAll("
                SELECT cut.thread_id FROM chatroom_user_threads cut
                INNER JOIN users u ON cut.user_id = u.id
                WHERE u.username LIKE '%{$filter['username']}%'
            ");

            $threadIds = [];
            foreach ($rows as $row) $threadIds[] = $row->thread_id;

            if (empty($threadIds)) return [];

            $sql .= 'AND cut.thread_id in (' . implode(',', $threadIds) . ')';
        }


        if ($filter['date_from'] || $filter['date_to']) {
            $dateInterval = [];

            if ($filter['date_from']) {
                $dateInterval[] = 'DATE(cm.date_created) >= DATE(FROM_UNIXTIME(' . $filter['date_from'] . '))';
            }

            if ($filter['date_to']) {
                $dateInterval[] = 'DATE(cm.date_created) <= DATE(FROM_UNIXTIME(' . $filter['date_to'] . '))';
            }

            $rows = parent::_fetchAll('
                    SELECT cm.thread_id FROM chatroom_messages cm
                    WHERE ' . implode(' AND ', $dateInterval) . '
                ');

            $threadIds = [];
            foreach ($rows as $row) $threadIds[] = $row->thread_id;

            if (empty($threadIds)) return [];

            $sql .= ' AND cut . thread_id in(' . implode(', ', $threadIds) . ')';
        }

        $sql .= '
            GROUP BY cut . thread_id 
            HAVING participantsCount > 1
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

        $this->getAdapter()->query('SET NAMES `utf8`');
        $threads = parent::_fetchAll($sql);

        $count = $this->getAdapter()->fetchOne('SELECT FOUND_ROWS()');

        $userIds = [];
        foreach ($threads as $thread) {
            $userIds[] = $thread->user_ids;
        }
        $userIds = implode(',', $userIds);

        $users = [];
        if (empty($userIds)) return [];

        $usersRaw = parent::_fetchAll("SELECT id, username, user_type FROM users WHERE id in ($userIds)");
        foreach ($usersRaw as $user) {
            $users[$user->id] = $user;
        }

        foreach ($threads as $thread) {
            $thread->user_ids = explode(',', $thread->user_ids);
            $thread->users = [];

            foreach ($thread->user_ids as $id) {
                $thread->users[$id] = $users[$id];
            }

            switch ($thread->privacy_level) {
                case self::PRIVACY_LEVEL_DIRECT:
                    $thread->title = 'Direct Conversation';
                    break;
                case self::PRIVACY_LEVEL_PRIVATE_GROUP:
                    $thread->title .= ' (Group Conversation)';
                    break;
            }

            unset($thread->user_ids);
        }

        return $threads;
    }

    /**
     * @param $threadId
     * @param int $page
     * @param int $perPage
     * @return array
     */
    public function getMessagesByThreadId($threadId, $page = 1, $perPage = 10)
    {
        $sql = '
			SELECT SQL_CALC_FOUND_ROWS 	
                m . id, m . sender_profile, m . date_created as sentDate,
                m . text, m . sender_id, u . username, 
                at . file_url as attachment_url, at . file_type as attachment_file_type, at . original_name as file_original_name

                FROM chatroom_messages as m 
                LEFT JOIN chatroom_attachments as at ON at . message_id = m . id
                INNER JOIN users u ON u . id = m . sender_id
                WHERE m . thread_id = ' . $threadId . '       
		';

        $sql .= '
            ORDER BY m . date_created DESC
			LIMIT ' . ($page - 1) * $perPage . ', ' . $perPage . '
		';

        $this->getAdapter()->query('SET NAMES `utf8`');
        $messages = parent::_fetchAll($sql);

        $count = $this->getAdapter()->fetchOne('SELECT FOUND_ROWS()');

        return [
            'messages' => $messages,
            'hasMore' => $count > $page * $perPage,
            'totalCount' => $count
        ];
    }

    /**
     * @return string
     */
    public function getCurrentArchiveTable()
    {
        $sql = self::getArchiveTableSQL();
        $this->getAdapter()->query($sql);
        return self::getCurrentArchiveTableName();
    }

    /**
     * Combines old messages and attachments and moves to archive table
     * @throws Cubix_Cli_Exception
     */
    public function archiveOldMessages()
    {
        $cli = new Cubix_Cli();
        $cli->clear();

        $archiveTable = $this->getCurrentArchiveTable();

        $step = 10;
        $pivot = 0;
        $daysToArchive = 10;

        $baseSql = "SELECT 
            cm.id, cm.text, cm.thread_id, cm.sender_id, 
            cm.sender_profile, cm.date_created, cm.date_updated,
            ca.file_url, ca.file_type, ca.original_name, ca.id as attachment_id
        FROM chatroom_messages cm
        LEFT JOIN chatroom_attachments ca ON ca.message_id = cm.id
        WHERE cm.date_created <= subdate(current_date, $daysToArchive)";

        $overallMessagesToArchive = self::$_db->fetchOne("SELECT count(id) as cnt FROM chatroom_messages WHERE date_created <= subdate(current_date, $daysToArchive)");
        $messagesArchived = 0;

        do {
            parent::getAdapter()->beginTransaction();

            try {

                $sqlToRun = $baseSql . ' LIMIT ' . ($pivot * $step) . ',' . $step;
                $rows = parent::_fetchAll($sqlToRun);

                $messages = [];
                $messageIds = [];
                $attachmentIds = [];

                // Composing attachments into messages to use as a json
                // and avoid creation of another table for archived attachments
                foreach ($rows as $row) {

                    if (!isset($messages[$row->id])) {
                        $messageIds[] = $row->id;
                        $messages[$row->id] = [
                            'id' => $row->id,
                            'text' => !empty($row->text) ? $row->text : ' ',
                            'thread_id' => $row->thread_id,
                            'sender_id' => $row->sender_id,
                            'sender_profile' => $row->sender_profile,
                            'date_created' => $row->date_created,
                            'date_updated' => $row->date_updated,
                            'attachments' => [
                                'id' => $row->attachment_id,
                                'file_url' => $row->file_url,
                                'file_type' => $row->file_type,
                                'original_name' => $row->original_name,
                            ],
                        ];
                    }

                    $messages[$row->id]['attachments'][] = [
                        'id' => $row->attachment_id,
                        'file_url' => $row->file_url,
                        'file_type' => $row->file_type,
                        'original_name' => $row->original_name,
                    ];

                    $attachmentIds[] = $row->attachment_id;
                }

                // Remove messages from table since they are going to be inside archive
                if (!empty($messageIds))
                    $this->getAdapter()->query('DELETE FROM chatroom_messages WHERE id IN(' . implode($messageIds) . ')');

                // Remove attachments from table since they are going to be inside archive
                if (!empty($attachmentIds))
                    $this->getAdapter()->query('DELETE FROM chatroom_attachments WHERE id IN(' . implode($attachmentIds) . ')');

                // "Bulk insert" composed messages to archive
                $insertSql = "INSERT IGNORE INTO $archiveTable 
                    (`id`, `text`, `thread_id`, `sender_id`, `sender_profile`, `date_created`, `date_updated`, `attachments`) VALUES";

                $insertRows = [];
                foreach ($messages as $message) {

                    $message['text'] = self::$_db->quote($message['text']);
                    $message['date_created'] = self::$_db->quote($message['date_created']);
                    $message['date_updated'] = self::$_db->quote($message['date_updated']);
                    $message['sender_profile'] = self::$_db->quote($message['sender_profile']);
                    $message['attachments'] = self::$_db->quote(json_encode($message['attachments']));

                    $insertRows[] = "
                        (
                            {$message['id']},
                            {$message['text']},
                            {$message['thread_id']},
                            {$message['sender_id']},
                            {$message['sender_profile']},
                            {$message['date_created']},
                            {$message['date_updated']},
                            {$message['attachments']}
                        )
                    ";
                }

                if (!empty($insertRows)) {
                    $insertSql .= implode(' , ', $insertRows);
                    $this->getAdapter()->query($insertSql);
                    $messagesArchived += count($messages);
                    $cli->colorize('yellow')->out("[$messagesArchived / $overallMessagesToArchive] messages archived.")->reset();
                }


                parent::getAdapter()->commit();
                $pivot++;

            } catch (Exception $e) {
                parent::getAdapter()->rollBack();

                $cli->colorize('red')->out($e->getMessage())->reset();

                throw $e;
            }
        } while (count($messages) > 0);

        $cli->colorize('green')->out('Done !Enjoy your day :)')->reset();
        exit;
    }

    /**
     * This method simply returns the name of archived tables
     * depending on the current date
     * e.g. table_name_05_2020
     * @return string
     */
    private static function getCurrentArchiveTableName()
    {
        return 'chatroom_messages_archive_' . date('m') . '_' . date('Y');;
    }

    /**
     * Returns "messages archive"s table structure
     * @return string
     */
    private static function getArchiveTableSQL()
    {
        $tableName = self::getCurrentArchiveTableName();
        return " 
            CREATE TABLE IF NOT EXISTS `$tableName`  (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `text` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
              `attachments` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
              `thread_id` int(11) NULL DEFAULT NULL,
              `sender_id` int(10) UNSIGNED NULL DEFAULT NULL,
              `sender_profile` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
              `date_created` datetime NOT NULL,
              `date_updated` datetime NULL DEFAULT NULL,
              PRIMARY KEY (`id`, `date_created`) USING BTREE,
              INDEX `sender_id`(`sender_id`) USING BTREE,
              INDEX `chatroom_messages_ibfk_1`(`thread_id`) USING BTREE
            )
        ";
    }
}
