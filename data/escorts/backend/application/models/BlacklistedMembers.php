<?php

class Model_BlacklistedMembers extends Cubix_Model
{
    public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS 
				bmc.id, e.showname, u.username, bmc.comment, UNIX_TIMESTAMP(bmc.date) AS date, bmc.status
			FROM blacklisted_members_comments bmc
			INNER JOIN escorts e ON e.id = bmc.escort_id
			INNER JOIN users u ON u.id = bmc.user_id
			WHERE 1
		';

		$countSql = 'SELECT FOUND_ROWS()';
		
		if ( strlen($filter['showname']) ) {
			$sql .= self::quote('AND e.showname LIKE ?', '%' . $filter['showname'] . '%');
		}
		
		if ( strlen($filter['escort_id']) ) {
			$sql .= self::quote('AND escort_id = ?', $filter['escort_id']);
		}
		
		if ( strlen($filter['username']) ) {
			$sql .= self::quote('AND u.username LIKE ?', '%' . $filter['username'] . '%');
		}
		
        if ( strlen($filter['status']) ) {
			$sql .= self::quote('AND bmc.status = ?',  $filter['status']);
		}

		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$ret = parent::_fetchAll($sql);
		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return $ret;
	}
	
	public function get($id)
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS 
				bmc.id, e.showname, u.username, bmc.comment, UNIX_TIMESTAMP(bmc.date) AS date, bmc.status
			FROM blacklisted_members_comments bmc
			INNER JOIN escorts e ON e.id = bmc.escort_id
			INNER JOIN users u ON u.id = bmc.user_id
			WHERE bmc.id = ?
		';
		return self::_fetchRow($sql, array($id));
	}

	public function update($data)
	{
		parent::getAdapter()->update('blacklisted_members_comments', $data, parent::quote('id = ?', $data['id']));
	}
}
