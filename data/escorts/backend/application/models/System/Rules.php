<?php

class Model_System_Rules extends Cubix_Model
{	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{		
		$sql = '
			SELECT id, title, question_en, answer_en, question_it, answer_it 
			FROM rules
			WHERE 1
		';
		
		$countSql = '
			SELECT COUNT(DISTINCT(id))
			FROM rules
			WHERE 1
		';
		
		if ( strlen($filter['title']) ) {
			$sql .= $this->getAdapter()->quoteInto('AND title LIKE ?', '%' . $filter['title'] . '%');
			$countSql .= $this->getAdapter()->quoteInto('AND title LIKE ?', '%' . $filter['title'] . '%');
		}

		if ( strlen($filter['question_en']) ) {
			$sql .= $this->getAdapter()->quoteInto('AND question_en LIKE ?', '%' . $filter['question_en'] . '%');
			$countSql .= $this->getAdapter()->quoteInto('AND question_en LIKE ?', '%' . $filter['question_en'] . '%');
		}

		if ( strlen($filter['answer_en']) ) {
			$sql .= $this->getAdapter()->quoteInto('AND answer_en LIKE ?', '%' . $filter['answer_en'] . '%');
			$countSql .= $this->getAdapter()->quoteInto('AND answer_en LIKE ?', '%' . $filter['answer_en'] . '%');
		}
		
		if ( strlen($filter['question_it']) ) {
			$sql .= $this->getAdapter()->quoteInto('AND question_it LIKE ?', '%' . $filter['question_it'] . '%');
			$countSql .= $this->getAdapter()->quoteInto('AND question_it LIKE ?', '%' . $filter['question_it'] . '%');
		}

		if ( strlen($filter['answer_it']) ) {
			$sql .= $this->getAdapter()->quoteInto('AND answer_it LIKE ?', '%' . $filter['answer_it'] . '%');
			$countSql .= $this->getAdapter()->quoteInto('AND answer_it LIKE ?', '%' . $filter['answer_it'] . '%');
		}
		
		if ( strlen($filter['application_id']) ) {
			$sql .= $this->getAdapter()->quoteInto('AND application_id = ?', $filter['application_id']);
			$countSql .= $this->getAdapter()->quoteInto('AND application_id = ?', $filter['application_id']);
		}
		
		$sql .= '
			GROUP BY id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return $this->getAdapter()->query($sql)->fetchAll();
	}
	
	public function get($id)
	{
		return $this->getAdapter()->query('SELECT * FROM rules WHERE id = ?', array($id))->fetch();
	}
	
	public function insert($data)
	{
		$this->getAdapter()->insert('rules', $data);
	}
	
	public function update($data)
	{
		$id = $data['id'];
		unset($data['id']);
		
		$this->getAdapter()->update('rules', $data, $this->getAdapter()->quoteInto('id = ?', $id));
	}
			
	public function remove($id)
	{
		$this->getAdapter()->delete('rules', $this->getAdapter()->quoteInto('id = ?', $id));
	}
}