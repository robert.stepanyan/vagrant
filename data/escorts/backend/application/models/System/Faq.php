<?php

class Model_System_Faq extends Cubix_Model
{
	protected $_table = 'faq';
	protected $_itemClass = 'Model_System_FaqItem';

	const FAQ_TYPE_MEMBER = 'member';
	const FAQ_TYPE_ESCORT = 'escort';
	const FAQ_TYPE_AGENCY = 'agency';
	const FAQ_TYPE_ADMIN = 'admin';
	const FAQ_TYPE_SALES = 'sales';

	public function get($slug, $app_id)
	{
		return self::_fetchRow('
			SELECT * FROM faq WHERE id = ? AND application_id = ?
		', array($slug, $app_id));
	}
		
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$f = '';
		if (Cubix_Application::getId() == APP_EF) $f = ', question_it ';
		
		$sql = '
			SELECT id, title, question_en, answer_en, type, status' . $f . '
			FROM faq
			WHERE 1
		';
		
		$countSql = '
			SELECT COUNT(DISTINCT(id))
			FROM faq
			WHERE 1
		';
		
		if ( strlen($filter['title']) ) {
			$sql .= self::quote('AND title LIKE ?', '%' . $filter['title'] . '%');
			$countSql .= self::quote('AND title LIKE ?', '%' . $filter['title'] . '%');
		}

		if ( strlen($filter['question']) ) {
			$sql .= self::quote('AND question_en LIKE ?', '%' . $filter['question'] . '%');
			$countSql .= self::quote('AND question_en LIKE ?', '%' . $filter['question'] . '%');
		}

		if ( strlen($filter['answer']) ) {
			$sql .= self::quote('AND answer_en LIKE ?', '%' . $filter['answer'] . '%');
			$countSql .= self::quote('AND answer_en LIKE ?', '%' . $filter['answer'] . '%');
		}

		if ( strlen($filter['type']) ) {
			$sql .= self::quote('AND FIND_IN_SET(?, type)', $filter['type']);
			$countSql .= self::quote('AND FIND_IN_SET(?, type) = ?', $filter['type']);
		}
		
		if ( strlen($filter['application_id']) ) {
			$sql .= self::quote('AND application_id = ?', $filter['application_id']);
			$countSql .= self::quote('AND application_id = ?', $filter['application_id']);
		}
		
		$sql .= '
			GROUP BY id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$countSql .= '
			
		';
		
		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return parent::_fetchAll($sql);
	}
	
	public function insert(Model_System_FaqItem $item)
	{
		self::getAdapter()->insert($this->_table, (array)$item);	
	}
	
	public function update(Model_System_FaqItem $item)
	{
		self::getAdapter()->update($this->_table, (array)$item, self::getAdapter()->quoteInto('id = ?', $item['id']));
	}
		
	public function toggle($id)
	{
		parent::getAdapter()->update($this->_table, array(
			'status' => new Zend_Db_Expr('NOT status')
		), parent::quote('id = ?', $id));
	}
	
	public function remove($id, $app_id)
	{
		parent::remove(array(self::quote('id = ?', $id), self::quote('application_id = ?', $app_id)));
	}
}