<?php

class Model_System_IssueTemplates extends Cubix_Model
{
	public function getAll($p, $pp, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT i.*, b.username 
			FROM issue_templates i 
			LEFT JOIN backend_users b ON b.id = i.backend_user_id
			WHERE 1 
		';
		
		$countSql = 'SELECT COUNT(id) FROM issue_templates WHERE 1 ';
		
		$where = '';
		
		if ( isset($filter['backend_user_id']) ) {
			$where .= ' AND backend_user_id = ' . $filter['backend_user_id'];
		}
				
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
		';
		
		$count = $this->getAdapter()->fetchOne($countSql);
		
		return $this->getAdapter()->query($sql)->fetchAll();
	}
	
	public function save($data)
	{
		if (isset($data['id']) && $data['id'] != '')
		{
			$id = $data['id'];

			parent::getAdapter()->update('issue_templates', array(
				'title' => $data['title'],
				'comment' => $data['comment'],
				'signature' => $data['signature']
			), parent::quote('id = ?', $id));
		}
		else
		{
			parent::getAdapter()->insert('issue_templates', array(
				'title' => $data['title'],
				'comment' => $data['comment'],
				'signature' => $data['signature'],
				'backend_user_id' => $data['backend_user_id']
			));

			$id = parent::getAdapter()->lastInsertId();
		}

		return $id;
	}

    public function saveV2($data)
    {
        if (isset($data['id']) && $data['id'] != '')
        {
            $id = $data['id'];

            parent::getAdapter()->update('issue_templates', $data, parent::quote('id = ?', $id));
        }
        else
        {
            parent::getAdapter()->insert('issue_templates', $data);

            $id = parent::getAdapter()->lastInsertId();
        }

        return $id;
    }

	public function get($id)
	{
		$sql = 'SELECT * FROM issue_templates WHERE id = ?';
		
		return parent::getAdapter()->query($sql, $id)->fetch();
	}
	
	public function remove($id)
	{
		parent::getAdapter()->delete('issue_templates', parent::quote('id = ?', $id));
	}
	
	public function getByBO($backend_user_id)
	{
		$where = '';
		
		if ($backend_user_id)
		{
			$where = ' AND backend_user_id = ' . $backend_user_id;
		}

		$fields = in_array(Cubix_Application::getId(),array(APP_ED))? '*' : 'id, title';

		return parent::getAdapter()->query('SELECT ' . $fields . ' FROM issue_templates WHERE 1 ' . $where)->fetchAll();
	}
}
