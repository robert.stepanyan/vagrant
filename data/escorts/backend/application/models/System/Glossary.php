<?php

class Model_System_Glossary extends Cubix_Model
{
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT id, abbr, ' . Cubix_I18n::getTblFields('value') . '
			FROM glossary
			WHERE 1
		';

		$countSql = '
			SELECT COUNT(id)
			FROM glossary
			WHERE 1
		';

		if ( strlen($filter['abbr']) ) {
			$sql .= self::quote('AND abbr LIKE ?', $filter['abbr'] . '%');
			$countSql .= self::quote('AND abbr LIKE ?', $filter['abbr'] . '%');
		}

		if ( strlen($filter['value_en']) ) {
			$sql .= self::quote('AND value_en LIKE ?', '%' . $filter['value_en'] . '%');
			$countSql .= self::quote('AND value_en LIKE ?', '%' . $filter['value_en'] . '%');
		}

		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$countSql .= '

		';

		$count = intval($this->getAdapter()->fetchOne($countSql));

		return $this->getAdapter()->query($sql)->fetchAll();
	}

	public function getByAbbr($abbr)
	{
		return $this->getAdapter()->query('SELECT * FROM glossary WHERE abbr = ?', $abbr)->fetch();
	}

	public function getById($id)
	{
		return $this->getAdapter()->query('SELECT * FROM glossary WHERE id = ?', $id)->fetch();
	}

	public function checkAbbr($abbr, $id)
	{
		return $this->getAdapter()->fetchOne('SELECT TRUE FROM glossary WHERE abbr = ? AND id <> ?', array($abbr, $id));
	}

	public function save($data)
	{
		if ( isset($data['id']) && $data['id'] ) {
			$this->getAdapter()->update('glossary', $data, $this->getAdapter()->quoteInto('id = ?', $data['id']));
		}
		else {
			$this->getAdapter()->insert('glossary', $data);
		}
	}
	
	public function remove($id)
	{
		$this->getAdapter()->delete('glossary', $this->getAdapter()->quoteInto('id = ?', $id));
	}
}
