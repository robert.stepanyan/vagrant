<?php

class Model_System_Blackdomains extends Cubix_Model
{
	
protected $_table = 'blacklisted_domains';
	//protected $_itemClass = 'Model_SMS_SMSItem';
	
	public function get($id)
	{
		$sql = '
			SELECT bd.id, bd.domain, a.id AS application_id, a.title AS application_title
			FROM blacklisted_domains bd
			LEFT JOIN applications a ON a.id = bd.application_id
			WHERE bd.id = ?
		';
		
		return parent::_fetchRow($sql, $id);
	}
	
	public function isExists($domain, $app_id)
	{
		return (bool) self::getAdapter()->fetchOne('SELECT TRUE FROM blacklisted_domains WHERE domain = ? AND application_id = ?', array($domain, $app_id));
	}
	
	public function getAll($p, $pp, $sort_field, $sort_dir, $filter, &$count)
	{
		$sql = '
			SELECT bd.id, bd.domain, a.title AS application_title
			FROM blacklisted_domains bd
			LEFT JOIN applications a ON a.id = bd.application_id
			WHERE 1
		';
		
		$countSql = '
			SELECT
				COUNT(bd.id)
			FROM blacklisted_domains bd
			LEFT JOIN applications a ON a.id = bd.application_id
			WHERE 1
		';
		
		$where = '';

		if ( strlen($filter['a.id']) ) {
			$where .= self::quote('AND a.id = ? OR bd.application_id IS NULL', $filter['a.id']);
		}
		
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
		';
		
		$count = $this->getAdapter()->fetchOne($countSql);
		return parent::_fetchAll($sql);
	}
	
	public function save(Model_System_BlackdomainsItem $item)
	{
		parent::save($item);
	}
	
	public function remove($id)
	{
		parent::remove(self::quote('id = ?', $id));
	}
}
