<?php

class Model_System_EmailTemplates extends Cubix_Model
{
	protected $_table = 'email_templates';
	protected $_itemClass = 'Model_System_EmailTemplatesItem';

	public function get($id, $app_id)
	{
		return self::_fetchAll('
			SELECT * FROM email_templates WHERE id = ? AND application_id = ?
		', array($id, $app_id));
	}
	
	public function getById($id, $app_id)
	{
		return self::_fetchRow('
			SELECT * FROM email_templates WHERE id = ? AND application_id = ?
		', array($id, $app_id));
	}
	
	public function getByIdEn($id, $app_id, $lng)
	{
		return self::_fetchRow('
			SELECT * FROM email_templates WHERE id = ? AND application_id = ? AND lang_id = "en"
		', array($id, $app_id));
	}
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
        $field = '';
        if (Cubix_Application::getId() == APP_EG_CO_UK){
            $field .= 'not_used,';
        }
		$sql = '
			SELECT id,'.$field.' from_addr, ( IF(LENGTH(body) > 100, CONCAT(SUBSTRING(body, 1, 100), "..."), body ) ) AS body
			FROM email_templates
			WHERE 1
		';
		
		$countSql = '
			SELECT COUNT(DISTINCT(id))
			FROM email_templates
			WHERE 1
		';
		
		if ( strlen($filter['id']) ) {
			$sql .= self::quote('AND id LIKE ?', '%' . $filter['id'] . '%');
			$countSql .= self::quote('AND id LIKE ?', '%' . $filter['id'] . '%');
		}

		if (Cubix_Application::getId() == APP_EG_CO_UK) {
            if (strlen($filter['templates_for'] == 'escorts')) {
                $sql .= " AND id LIKE 'escort%' ";
            }
            if (strlen($filter['templates_for'] == 'agencies')) {
                $sql .= " AND id LIKE 'agency%' ";
            }
            if (strlen($filter['templates_for'] == 'members')) {
                $sql .= " AND id LIKE 'member%' ";
            }
        }

		if ( strlen($filter['application_id']) ) {
			$sql .= self::quote('AND application_id = ?', $filter['application_id']);
			$countSql .= self::quote('AND application_id = ?', $filter['application_id']);
		}
		
		$sql .= '
			GROUP BY id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$countSql .= '
			
		';
		
		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return parent::_fetchAll($sql);
	}
	
	public function insert(Model_System_EmailTemplatesItem $item)
	{
		foreach ($item as $it)
		{
			self::getAdapter()->insert($this->_table, $it);
		}
	}
	
	public function update(Model_System_EmailTemplatesItem $item)
	{
		foreach ($item as $it)
		{
			
			$is_exist = self::getAdapter()->fetchOne('SELECT TRUE FROM email_templates WHERE id = ? AND application_id = ? AND lang_id = ?', array($it['id'], $it['application_id'], $it['lang_id']));
			if ( $is_exist )
			{
				self::getAdapter()->query("
					UPDATE
						email_templates
					SET
						from_addr = ?,
						subject = ?,
						body_plain = ?,
						body = ?
					WHERE id = ? AND application_id = ? AND lang_id = ?
				", array($it['from_addr'], $it['subject'], $it['body_plain'], $it['body'], $it['id'], $it['application_id'], $it['lang_id']));
			}
			else
			{
				self::getAdapter()->insert($this->_table, $it);
			}			
		}
	}
		
	public function save(Model_System_EmailTemplatesItem $item)
	{
		if ( $item->getId() ) {
			self::getAdapter()->update($this->_table, $item->getData(), self::getAdapter()->quoteInto('id = ?', $item->getId()));
		}
		else {
			$data = $item->getData();
			if ( isset($data['entry_id']) ) {
				$data['id'] = $data['entry_id'];
				unset($data['entry_id']);
			}
			
			self::getAdapter()->insert($this->_table, $data);
		}
	}
	
	public function remove($id, $app_id)
	{
		parent::remove(array(self::quote('id = ?', $id), self::quote('application_id = ?', $app_id)));
	}
}