<?php
class Model_Importer extends Cubix_Model
{
	protected $_table = 'importer_data';
    const STATUS_NOT_VERIFIED = 1;
    const STATUS_VERIFIED = 2;
    const STATUS_VERIFY_RESET = 3;

    const NOT_VERIFIED = 1;
    const VERIFIED = 2;
    const VERIFY_RESET = 3;

    const ESCORT_STATUS_NO_PROFILE					= 1;		// 00000000000001
    const ESCORT_STATUS_NO_ENOUGH_PHOTOS			= 2;		// 00000000000010
    const ESCORT_STATUS_NOT_APPROVED				= 4;		// 00000000000100
    const ESCORT_STATUS_OWNER_DISABLED				= 8;		// 00000000001000
    const ESCORT_STATUS_ADMIN_DISABLED				= 16;		// 00000000010000
    const ESCORT_STATUS_ACTIVE						= 32;		// 00000000100000
    const ESCORT_STATUS_IS_NEW						= 64;		// 00000001000000
    const ESCORT_STATUS_PROFILE_CHANGED				= 128;		// 00000010000000
    const ESCORT_STATUS_DELETED						= 256;		// 0000000100000000
    const ESCORT_STATUS_FAKE_PICTURE_DISABLED		= 512;		// 0000001000000000
    const ESCORT_STATUS_TEMPRARY_DELETED            = 1024;		// 0000010000000000
    const ESCORT_STATUS_SUSPICIOUS_DELETED			= 2048;		// 0000100000000000
    const ESCORT_STATUS_NEED_AGE_VERIFICATION       = 4096;		// 0001000000000000
    const ESCORT_STATUS_OFFLINE                     = 8192;		// 0010000000000000
    const ESCORT_STATUS_INACTIVE                    = 16384;	// 0100000000000000
    const ESCORT_STATUS_PARTIALLY_COMPLETE          = 32768;	// 1000000000000000
    const ESCORT_STATUS_IP_BLOCKED          		= 65536;	// 10000000000000000

    const HH_STATUS_ACTIVE  = 1;
    const HH_STATUS_PENDING = 2;
    const HH_STATUS_EXPIRED = 3;
    const HH_STATUS_ADMIN_DISABLED = 4;

	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$where = '';
        $field = '';
		
		if ( strlen($filter['phone']) ) {
			$where .= self::quote(' AND phone LIKE "%?%" ', intval($filter['phone']));
		}

        if ( strlen($filter['status']) ) {
            $where .= self::quote(' AND `status` = ? ', intval($filter['status']));
        }

        if ( strlen($filter['website']) ) {
            $where .= self::quote(' AND `web_site` LIKE "%'.$filter['website'].'%"', null );
        }

        if ( strlen($filter['country']) && Cubix_Application::getId() == APP_ED) {
            $where .= self::quote(' AND `country` = ?', $filter['country'] );
        }

        if ( strlen($filter['date_from']) ) {
            $where .= self::quote(' AND UNIX_TIMESTAMP(`created_at`) > '.$filter['date_from'], null  );
        }

        if ( strlen($filter['date_to'])) {
            $where .= self::quote(' AND UNIX_TIMESTAMP(`created_at`) < '.$filter['date_to'], null );
        }

        if ( Cubix_Application::getId() == APP_ED )
        {
            $field .= ', online';
        }

		$sql = '
			SELECT
				id,phone,web_site,import_url,created_at,`data`,escort_id, `status` '.$field.'
			FROM '.$this->_table.'
			WHERE 1
		';

		$countSql = '
			SELECT COUNT(DISTINCT(id))
			FROM '.$this->_table.' 
			WHERE 1
		';
						
		$sql .= $where;
		$countSql .= $where;
		$sql .= '
			GROUP BY phone
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
//		echo $sql;die;

		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return parent::_fetchAll($sql);
	}	
	
	public function save($data)
	{

			$fields = array(
				'import_url' => $data['url'],
				'web_site' => $data['web_site'],
				'phone' => $data['phone'],
                'data' => serialize($data),
			);

			if ($data['web_site'] == 'https://www.eurogirlsescort.com')
            {
                $fields['country'] = $data['country_title'];
            }elseif ($data['web_site'] == 'https://www.slummi.com')
            {
                $fields['country'] = 'spain';
                $fields['online'] = $data['online'];
            }elseif ($data['web_site'] == 'https://www.ladies.de')
            {
                $fields['country'] = 'germany';
            }

            $isset = parent::getAdapter()->fetchOne('SELECT true FROM '.$this->_table.' WHERE phone = ? AND web_site = ?', array($data['phone'],$data['web_site']));
            if ($isset)
            {
                parent::getAdapter()->update($this->_table, $fields, parent::quote('id = ?', $data['id']));
                return 'Updated successfull';
            }else{
                parent::getAdapter()->insert($this->_table, $fields);
                return 'Inserted successfull';
            }
			

	}

    public function updateStatus($a6ImportId,$escort_id)
    {

        $fields = array(
            'escort_id' => $escort_id,
            'status' => 1
        );

        $isset = parent::getAdapter()->fetchOne('SELECT true FROM '.$this->_table.' WHERE id = ?', array($a6ImportId));
        if ($isset)
        {
            parent::getAdapter()->update($this->_table, $fields, parent::quote('id = ?', $a6ImportId));
            return 'Updated successfull';
        }
    }

	public function getById( $id )
    {
        $sql = '
			SELECT
				id,phone,web_site,import_url,created_at,`data`
			FROM '.$this->_table.'
			WHERE id = ?
		';

        return parent::_fetchRow( $sql, $id );
    }


    public function getUserId($id)
    {
        $db = Zend_Registry::get('db');

        return $db->fetchOne('SELECT user_id FROM escorts WHERE id = ?', $id);
    }

	public function remove($id)
	{
		return parent::getAdapter()->delete($this->_table, parent::quote('id = ?', $id));
	}

    public function existsByUsername($username)
    {
        return (bool) self::getAdapter()->fetchOne('SELECT TRUE FROM users u LEFT JOIN escorts e ON e.user_id = u.id  WHERE u.username = ? AND NOT ( IF(e.status IS NOT NULL ,e.status, 0) & ?) AND u.status <> -5 ' , array( $username, self::ESCORT_STATUS_DELETED));
    }

    public function existsByPhone($phone)
    {
        if($phone == ''){
            return array();
        }

        $sql = 'SELECT true 
                    FROM
                        escort_profiles_v2 
                    WHERE
                        phone_exists LIKE "%'.$phone.'%" 
                        OR contact_phone_parsed LIKE "%'.$phone.'%"';

        return self::getAdapter()->fetchOne($sql);
    }

    public function existsImportByPhone($phone)
    {
        if($phone == ''){
            return array();
        }

        $sql = 'SELECT true 
                    FROM
                        importer_data 
                    WHERE
                        phone LIKE "%'.$phone.'%"';

        return self::getAdapter()->fetchOne($sql);
    }

    public function addEscortCities( $city_id, $escort_id )
    {
        $fields = array(
            'city_id' => $city_id,
            'escort_id' => $escort_id
        );

        $isset = parent::getAdapter()->fetchOne('SELECT true FROM escort_cities WHERE escort_id = ? AND city_id = ?', array($escort_id,$city_id));
        if (!$isset)
        {
            return parent::getAdapter()->insert('escort_cities', $fields);
        }else{
            return false;
        }
    }

    public function getAvailableCountries()
    {
        $sql = 'SELECT
                    country 
                FROM
                    `importer_data` 
                WHERE
                    country IS NOT NULL 
                GROUP BY
                    country';

        return self::getAdapter()->fetchAll($sql);
    }

    public function updateJunk($import_id,$status)
    {
        return parent::getAdapter()->update($this->_table, array('status' => $status), parent::quote('id = ?', $import_id));
    }

    public function getCountryDataByTitle($country)
    {
        $sql = 'SELECT
                    iso,phone_prefix 
                FROM
                    `countries_phone_code` 
                WHERE
                    slug = "'.$country.'"';

        return self::getAdapter()->fetchRow($sql);
    }

    public function getPrefixDataByIso($iso)
    {
        $sql = 'SELECT
                    phone_prefix 
                FROM
                    `countries_phone_code` 
                WHERE
                    iso = "'.$iso.'"';

        return self::getAdapter()->fetchOne($sql);
    }
}
