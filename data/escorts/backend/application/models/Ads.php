<?php
class Model_Ads extends Cubix_Model
{
	protected $_table = 'banner_escorts';

	public function getAssignedEscorts($id, $application_id)
	{
		$sql = '
			SELECT
				e.showname, e.id
			FROM banner_escorts b
			INNER JOIN escorts e ON e.id = b.escort_id
			WHERE 1 AND b.application_id = ? AND b.template = ?
		';

		return parent::_fetchAll($sql, array($application_id, $id));
	}
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT
				b.id, b.name
			FROM banners b
			WHERE 1
		';
		
		$countSql = '
			SELECT COUNT(DISTINCT(b.id))
			FROM banners b
			WHERE 1
		';
		
		$where = '';
				
		if ( strlen($filter['application_id']) ) {
			$where .= self::quote('AND b.application_id = ?', $filter['application_id']);
		}

		/*if ( strlen($filter['username']) ) {
			$where .= self::quote('AND u.username LIKE ?', $filter['username'] . '%');
		}
				
		if ( $filter['status'] ) {
			$where .= self::quote('AND c.status = ?', $filter['status']);
		}*/
				
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			GROUP BY b.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$count = intval($this->getAdapter()->fetchOne($countSql));

		$items = parent::_fetchAll($sql);

		if ( count($items) ) {
			foreach ( $items as $k => $item ) {
				$items[$k]->count = $this->getAdapter()->query('SELECT COUNT(escort_id) AS count FROM banner_escorts WHERE template = ? AND application_id = ?', array($item->id, $filter['application_id']))->fetch()->count;
			}
		}
		
		return $items;
	}
	
	public function save($id, $escort_ids, $application_id)
	{
		parent::getAdapter()->delete($this->_table, array(parent::quote('template = ?', $id), parent::quote('application_id', $application_id)));

		if ( count($escort_ids) )
		{
			foreach($escort_ids as $escort_id) {
				parent::getAdapter()->insert($this->_table, array('escort_id' => $escort_id, 'template' => $id, 'application_id' => $application_id));
			}
		}
	}
	
	
	public function remove($id)
	{
		parent::getAdapter()->delete($this->_table, parent::quote('id = ?', $id));
	}
}
