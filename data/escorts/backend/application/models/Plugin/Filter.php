<?php

class Model_Plugin_Filter extends Zend_Controller_Plugin_Abstract
{	
	/**
	 * @param Zend_Controller_Request_Abstract $request
	 */
	public function preDispatch($request)
	{
		$app_id = Zend_Controller_Front::getInstance()->getRequest()->application_id;
		Cubix_Api_XmlRpc_Client::setApiKey(Cubix_Application::getById($app_id)->api_key);
		Cubix_Application::setId($app_id);
		
		$module = $request->getModuleName();
		$controller = $request->getControllerName();
		$action = $request->getActionName();

		if ( $controller == 'escorts' && $action == 'index' ) {
			header('Location: /escorts-v2');
			die;
		}
		/*else if ( ($request->application_id != 6 && $controller == 'escorts-v2') ) {
			header('Location: /escorts');
			die;
		}*/
		
        $bu_user = Zend_Auth::getInstance()->getIdentity();
        Model_BOUsersSessions::refresh($bu_user->id);

		$controllers_map = array(
			'escorts',
			'issues',
			'escorts-v2',
			'agencies',
			'members',
			'orders',
			'transfers',
			'packages',
			'reviews',
			'comments',
			'instances',
			'pinboard',
			'blog',
			'followers'
		);

		$actions_map = array(
			'data',
			'vacation-data'
		);

		$ignore_params_map = array(
			'application_id',
			'controller',
			'action',
			'module',
			'lang_id',
			'sort_field',
			'sort_dir',
			'page',
			'per_page'
		);

		if ( in_array($controller, $controllers_map) && in_array($action, $actions_map) )
		{
			$req_params = $request->getParams();
			$ses_filter = new Zend_Session_Namespace($module . '_' . $controller . '_' . $action);
			//echo $module . '_' . $controller . '_' . $action; die;
			$params = array();
			foreach ( $req_params as $k => $param ) {
				if ( ! in_array($k, $ignore_params_map) ) {
					if ( @strlen($param) == 0 ) {
						$params[$k] = '';
					}
					else {
						$params[$k] = trim($param);
					}
				}
			}

			$ses_filter->filter_params = $params;
		}
	}

	/**
	 * @param Zend_Controller_Request_Abstract $request
	 */
	public function  routeShutdown($request)
	{
		
	}
	
	public function dispatchLoopShutdown()
	{
		if ( Cubix_Application::getId() == APP_A6){
			$db = Zend_Registry::get('db');

			$profiler = $db->getProfiler();
			$url = $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
			$number = $profiler->getTotalNumQueries();
			$bu_user = Zend_Auth::getInstance()->getIdentity();

			foreach($profiler->getQueryProfiles() as $query){

				if($query->getElapsedSecs() > 0.5){
					$data = array(
						'url' => $url,
						'bu_id' => $bu_user->id,
						'query_number' => $number,
						'query' => $query->getQuery(),
						'time' =>  number_format($query->getElapsedSecs(), 8),
						'bu_id' => $bu_user->id
					);
					$db->insert('sql_benchmark', $data);
				}
			}
		
			/*$data = array(
				'url' => '--------------------',
				'query_number' => 0,
				'query' => '--------------------',
			);
			$db->insert('sql_benchmark', $data);*/
		}
		
	}		
}
