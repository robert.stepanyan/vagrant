<?php

class Model_Plugin_Auth extends Zend_Controller_Plugin_Abstract
{
	/**
	 * @var Zend_Auth
	 */
	protected $_auth;

	public function __construct()
	{
		$this->_auth = Zend_Auth::getInstance();
	}

	public function hasAccess($request, array $permissions = array())
	{
		$m = $request->getModuleName();
		$c = $request->getControllerName();
		$a = $request->getActionName();

		$allowed = isset($permissions['allowed']) ? $permissions['allowed'] : null;
		$disallowed = isset($permissions['disallowed']) ? $permissions['disallowed'] : null;

		if ( ! isset($allowed[$m]) ) {
			return false;
		}
		elseif ( $allowed[$m] == '*' ) {}
		elseif ( ! isset($allowed[$m][$c]) ) {
			return false;
		}
		elseif ( $allowed[$m][$c] == '*' ) {}
		elseif ( ! in_array($a, $allowed[$m][$c]) ) {
			return false;
		}

		if ( ! isset($disallowed[$m]) ) {}
		elseif ( ! isset($disallowed[$m][$c]) ) {}
		elseif ( in_array($a, $disallowed[$m][$c])) {
			return false;
		}

		// If passed through filter return true
		return true;
	}

	public function routeStartup($request)
	{
		if ( defined('IS_CLI') && IS_CLI ) return;

		$this->_auth = Zend_Auth::getInstance();
		
		if ( $this->_auth->hasIdentity() ) {
			
			$user = $this->_auth->getIdentity();
			
			$current_app_id = isset($_COOKIE['application_id']) ? $_COOKIE['application_id'] : null;
			
			if ( ! $current_app_id ) $current_app_id = $request->application_id;
			
			if ( ! $current_app_id ) {
				$current_app_id = $user->application_id;
			}
			else if ( $user->type !== 'superadmin' && $user->type !== 'seo manager' && $current_app_id != $user->application_id ) {
				$current_app_id = $user->application_id;
			}

			if ( ! $current_app_id ) {
				$current_app_id = Cubix_Application::getAll();
				$current_app_id = $current_app_id[0]->id;
			}
			
			setcookie('application_id', $current_app_id, time() + 2 * 7 * 24 * 60 * 60 /* two weeks */, '/');
			
			$request->setParam('application_id', $current_app_id);
			
		}
	}
	
	public function routeShutdown($request)
	{
		$request->setParam('lang_id', 'en');
		
		Cubix_I18n::init();
		
		require 'Cubix/defines.php';
//var_dump(file_exists('Cubix/DefinesByApp/defines.' . Cubix_Application::getAppHostname() . '.php'));
		//if ( file_exists('Cubix/DefinesByApp/defines-' . Cubix_Application::getAppHostname() . '.php') ) {*/
			//require 'Cubix/DefinesByApp/defines-' . Cubix_Application::getAppHostname() . '.php';
		//}
		
		Zend_Registry::set('definitions', $DEFINITIONS);

		if ( defined('IS_CLI') && IS_CLI ) return;
				
		if ( ! ($request->getModuleName() == 'billing' && $request->getControllerName() == 'cron' && $request->getActionName() == 'index')
				&& ! ($request->getModuleName() == 'sms' && $request->getControllerName() == 'index' && ($request->getActionName() == 'sms-callback' || $request->getActionName() == 'sms-inbox'))
					&& ! ($request->getModuleName() == 'ebsms' && $request->getControllerName() == 'index' && ($request->getActionName() == 'ebsms-callback' || $request->getActionName() == 'ebsms-inbox'))
				&& ! ($request->getModuleName() == 'default' && $request->getControllerName() == 'escorts' && $request->getActionName() == 'add-snapshots') 
				&& ! ($request->getModuleName() == 'default' && $request->getControllerName() == 'escorts-v2' && $request->getActionName() == 'assign-sales')
				&& ! ($request->getModuleName() == 'default' && $request->getControllerName() == 'index' && $request->getActionName() == 'login-chat')
				&& ! ($request->getModuleName() == 'default' && $request->getControllerName() == 'gateway')
				&& ! ($request->getModuleName() == 'billing' && $request->getControllerName() == 'online-billing' && $request->getActionName() == 'confirm-payment')
				&& ! ($request->getModuleName() == 'billing' && $request->getControllerName() == 'online-billing' && $request->getActionName() == 'powercash-callback')
				&& ! ($request->getModuleName() == 'billing' && $request->getControllerName() == 'online-billing' && $request->getActionName() == 'netbanx-callback-sc')
				&& ! ($request->getModuleName() == 'billing' && $request->getControllerName() == 'online-billing' && $request->getActionName() == 'netbanx-callback-gotd')
				&& ! ($request->getModuleName() == 'billing' && $request->getControllerName() == 'online-billing' && $request->getActionName() == 'netbanx-callback-boost-profile')
				&& ! ($request->getModuleName() == 'billing' && $request->getControllerName() == 'online-billing' && $request->getActionName() == 'netbanx-callback-expo')
				&& ! ($request->getModuleName() == 'billing' && $request->getControllerName() == 'online-billing' && $request->getActionName() == 'bitcoin-callback-sc')
				&& ! ($request->getModuleName() == 'billing' && $request->getControllerName() == 'online-billing' && $request->getActionName() == 'coinsome-callback')
				&& ! ($request->getModuleName() == 'billing' && $request->getControllerName() == 'online-billing' && $request->getActionName() == 'mmg-bill-callback')
				&& ! ($request->getModuleName() == 'billing' && $request->getControllerName() == 'online-billing' && $request->getActionName() == 'faxin-callback')
				&& ! ($request->getModuleName() == 'billing' && $request->getControllerName() == 'online-billing' && $request->getActionName() == 'twispay-callback')
				&& ! ($request->getModuleName() == 'billing' && $request->getControllerName() == 'online-billing' && $request->getActionName() == 'sc3-ivr-callback')
				&& ! ($request->getModuleName() == 'billing' && $request->getControllerName() == 'online-billing' && $request->getActionName() == 'postfinance-callback')
				//&& ! ($request->getModuleName() == 'default' && $request->getControllerName() == 'test' && $request->getActionName() == 'send-sms-crawler')

				&& ! ($request->getModuleName() == 'sms' && $request->getControllerName() == 'index' && $request->getActionName() == 'sms-callback6-b')
				&& ! ($request->getModuleName() == 'sms' && $request->getControllerName() == 'index' && $request->getActionName() == 'send-instantbook-sms')
				&& ! ($request->getModuleName() == 'sms' && $request->getControllerName() == 'index' && $request->getActionName() == 'sms-inbox6-b')
				//&& ! ($request->getModuleName() == 'default' && $request->getControllerName() == 'escorts-v2' && $request->getActionName() == 'do-photos')
				
				/*&& ! ($request->getModuleName() == 'default' && $request->getControllerName() == 'cron' && $request->getActionName() == 'reports-per-month')
				&& ! ($request->getModuleName() == 'default' && $request->getControllerName() == 'cron' && $request->getActionName() == 'reports-per-week')
						
				&& ! ($request->getModuleName() == 'billing' && $request->getControllerName() == 'orders' && $request->getActionName() == 'view')*/	) {
			
			if ( ! $this->_auth->hasIdentity() &&
					$request->getControllerName() != 'auth' ) {
			
				header('Cubix-redirect: /auth');
				header('Location: /auth');
				die('Access denied!');
			}
		}
		
		$user = $this->_auth->getIdentity();
//        var_dump($user->type); die;
		if ( 'data entry' == $user->type ) {
			$permissions = array(
				'allowed' => array(
					'default' => array(
						'escorts' => '*',
						'escorts-v2' => '*',
						'agencies' => '*',
						'comments' => '*',
						'reviews' => '*',
						'retouch-photos' => '*',
						'settings' => '*',
						'dashboard' => array('index', 'dataentry', 'tip-data'),
						'auth' => array('login', 'logout', 'index'),
						'index' => array('quick-email'),
						'blog' => '*',
						'blog-comments' => '*',
					),
					'geography' => '*',
					'seo' => '*',
					'system' => array(
						'issue-templates' => '*'
					)
				),
				'disallowed' => array(
					'default' => array(
						'escorts' => array('delete'),
						'agencies' => array('delete')
					)
				)
			);

			if ( ! $this->hasAccess($request, $permissions)) {
				die(header('Location: /dashboard'));
			}
		}
		elseif ( 'data entry plus' == $user->type ) {
			$permissions = array(
				'allowed' => array(
					'default' => array(
						'escorts' => '*',
						'escorts-v2' => '*',
						'agencies' => '*',
						'comments' => '*',
						'reviews' => '*',
						'settings' => '*',
						'retouch-photos' => '*',
						'classified-ads' => '*',
						'dashboard' => array('index', 'dataentry', 'tip-data'),
						'auth' => array('login', 'logout', 'index'),
						'index' => array('quick-email'),
					),
					'geography' => '*',
					'sms' => '*',
					'system' => array(
						'issue-templates' => '*',
						'comment-templates' => '*',
					)
				),
				'disallowed' => array(
					'default' => array(
						'escorts' => array('delete'),
						'agencies' => array('delete')
					)
				)
			);

			if ( ! $this->hasAccess($request, $permissions)) {
				die(header('Location: /dashboard'));
			}
		}
		elseif ( 'dash manager' == $user->type ) {
			$permissions = array(
				'allowed' => array(
					'default' => array(
						'members' => '*',
						'escorts-v2' => '*',
						'agencies' => '*',
						'bubbles' => '*',
						'slogans' => '*',
						'settings' => '*',
						'comments' => '*',
						'dashboard' => '*',
						'auth' => array('login', 'logout', 'index'),
						'index' => array('quick-email'),
					),
					'geography' => '*',
					'verifications' => '*',
					'billing' => array(
						'transfers' => array('confirm')
					),
					'system' => array(
						'issue-templates' => '*'
					)
				),
				'disallowed' => array(
					'default' => array(
						'escorts' => array('delete'),
						'agencies' => array('delete')
					)
				)
			);
			
			$current_app_id = isset($_COOKIE['application_id']) ? $_COOKIE['application_id'] : null;
			if ( $current_app_id == APP_A6 ) {
				$permissions['allowed']['billing'] = array(
					'orders'	=> array('index', 'data', 'close'),
					'transfers' => array('index', 'data', 'reject', 'confirm')
				);
			}

			if ( ! $this->hasAccess($request, $permissions)) {
				die('<h1>Access Denied</h1>');
			}
		}
		else if ( 'sales manager' == $user->type || 'sales clerk' == $user->type ) {
			$permissions = array(
				'allowed' => array(
					'default' => array(
						'test-client' => '*',
						'escorts' => '*',
						'escorts-v2' => '*',
						'agencies' => '*',
						'escort-templates' => '*',
						'comments' => '*',
						'reviews' => array('data'),
						'wiki' => '*',
						'rules' => '*',
						'issues' => '*',
						'settings' => '*',
						'backend-users' => array('edit'),
						//'members' => '*',
						//'dashboard' => array('index', 'dataentry', 'tip-data'),
						'dashboard' => '*',
						'auth' => array('login', 'logout', 'index'),
						'index' => '*',
						'journal'	=> '*',
						'report-problem' => '*',
						'video' => '*',
						'balance' => array('edit')
					),
					'geography' => '*',
					'verifications' => '*',
					'sms' => '*',
					//'newsletter' => '*',
					'billing' => '*',
					'system' => array(
						'issue-templates' => '*'
					)
				),
				'disallowed' => array(
					'default' => array(
						'escorts' => array('delete'),
						'agencies' => array('delete')
					)
				)
			);

			if ( ! $this->hasAccess($request, $permissions)) {
				die(header('Location: /dashboard'));
			}
		}
		
		else if ( 'sales' == $user->type ) {
			$permissions = array(
				'allowed' => array(
					'default' => array(
						'escorts' => '*',
						'escorts-v2' => '*',
						'agencies' => '*',
						'escort-templates' => '*',
						'comments' => '*',
						'wiki' => '*',
						'rules' => '*',
						'issues' => '*',
						'settings' => '*',
						'backend-users' => array('edit'),
						//'members' => '*',
						//'dashboard' => array('index', 'dataentry', 'tip-data'),
						'dashboard' => '*',
						'auth' => array('login', 'logout', 'index'),
						'index' => '*',
						'journal'	=> '*',
						'report-problem' => '*',
						'video' => '*',
						'balance' => array('edit')
					),
					'geography' => '*',
					'verifications' => '*',
					'sms' => '*',
					'billing' => '*',
					'system' => array(
						'issue-templates' => '*'
					)
				),
				'disallowed' => array(
					'default' => array(
						'escorts' => array('delete'),
						'agencies' => array('delete')
					)
				)
			);
			if ( ! $this->hasAccess($request, $permissions)) {
				die(header('Location: /dashboard'));
			}
		}
		
		else if ( 'moderator plus' == $user->type ) {
			$permissions = array(
				'allowed' => array(
					'default' => array(
						'escorts' => '*',
						'escorts-v2' => '*',
						'agencies' => '*',
						'escort-templates' => '*',
						'comments' => '*',
						'reviews' => '*',
						'settings' => '*',
						'client-blacklist' => '*',
						'bubbles' => '*',
						'slogans' => '*',
						'auto-approved-photos' => '*',
						'wiki' => '*',
						'rules' => '*',
						'issues' => '*',
						'backend-users' => array('edit'),
						'members' => '*',
						'payments' => '*',
						'turnover' => '*',
						'alertme' => '*',
						//'dashboard' => array('index', 'dataentry', 'tip-data'),
						'dashboard' => '*',
						'auth' => array('login', 'logout', 'index'),
						'index' => '*',
						'journal'	=> '*',
						'feedbacks' => '*',
						'blacklisted-words' => '*',
						'blacklisted-emails' => '*',
						'backend-user-sessions' => '*',
						'report-problem' => '*',
						'video' => '*',
						'balance' => array('edit')
					),
					'geography' => '*',
					'verifications' => '*',
					'sms' => '*',
					'seo' => '*',
					'newsletter' => '*',
					'system' => '*',
					
					
				),
				'disallowed' => array(
					'default' => array(
						'backend-users' => '*',
						'escorts-v2' => array('sales-work','delete', 'assign-to','assign-to-me'),
						'escorts' => array('delete'),
						'agencies' => array('delete')
					),
					'billing' => '*'
				)
			);

			if ( ! $this->hasAccess($request, $permissions)) {
				die(header('Location: /dashboard'));
			}
		}
		else if ( 'moderator' == $user->type ) {
			$permissions = array(
				'allowed' => array(
					'default' => array(
						'escorts' => '*',
						'escorts-v2' => '*',
						'agencies' => '*',
						'escort-templates' => '*',
						'comments' => '*',
						'reviews' => '*',
						'client-blacklist' => '*',
						'bubbles' => '*',
						'slogans' => '*',
						'settings' => '*',
						'auto-approved-photos' => '*',
						'wiki' => '*',
						'rules' => '*',
						'issues' => '*',
						'backend-users' => array('edit'),
						'members' => '*',
						'payments' => '*',
						'turnover' => '*',
						'alertme' => '*',
						//'dashboard' => array('index', 'dataentry', 'tip-data'),
						'dashboard' => '*',
						'auth' => array('login', 'logout', 'index'),
						'index' => '*',
						'journal'	=> '*',
						'feedbacks' => '*',
						'blacklisted-words' => '*',
						'blacklisted-emails' => '*',
						'backend-user-sessions' => '*',
						'report-problem' => '*',
						'video' => '*',
						'balance' => array('edit'),
						'chat' => '*',
						'classified-ads' => '*',
						'pinboard' => '*',
						'pinboard-replies' => '*',
					),
					'geography' => '*',
					'verifications' => '*',
					'sms' => '*',
					'seo' => '*',
					'newsletter' => '*',
					'system' => '*',
					
					
				),
				'disallowed' => array(
					'default' => array(
						'backend-users' => '*',
						'members' => '*',
						'payments' => '*',
						'turnover' => '*',
						'alertme' => '*',
						'escorts-v2' => array('sales-work','delete', 'assign-to','assign-to-me'),
						'escorts' => array('delete'),
						'agencies' => array('delete')
					),
					'billing' => '*'
				)
			);

			if ( ! $this->hasAccess($request, $permissions)) {
				die(header('Location: /dashboard'));
			}
		}
		elseif ( 'seo manager' == $user->type ) {
			$permissions = array(
				'allowed' => array(
					'default' => array(
						'dashboard' => array('index', 'dataentry', 'tip-data', 'switch-application'),
						'settings' => '*',
						'auth' => array('login', 'logout', 'index'),
						'escorts-v2' => array('index', 'data'/*, 'edit'*/)
					),
					'seo' => '*',
					'geography' => array(
						'countries' => '*',
						'regions' => array('index', 'data', 'ajax'),
						'cities' => '*',
						'cityzones' => array('index', 'data', 'ajax')
					),
					'system' => array(
						'static-content' => '*',
						'blacklisted-ips' => '*',
					 )
				),
				'disallowed' => array(
					'geography' => array(
						'cities'	=> array('delete')
					)
				)
			);

			if ( ! $this->hasAccess($request, $permissions)) {
				$request->setModuleName('default');
				$request->setControllerName('error');
				$request->setActionName('permission-denied');
				//die(header('Location: /dashboard'));
			}
		}
		elseif ( 'journal manager' == $user->type ) {
			$permissions = array(
				'allowed' => array(
					'default' => array(
						'dashboard' => array('tip-data', 'switch-application'),
						'settings' => '*',
						'journal' => '*',
						'auth' => array('login', 'logout', 'index')
					)
				),
				'disallowed' => array(
				)
			);

			if ( ! $this->hasAccess($request, $permissions)) {
				$request->setModuleName('default');
				$request->setControllerName('error');
				$request->setActionName('permission-denied');
				//die(header('Location: /dashboard'));
			}
		}
		elseif ( 'call center' == $user->type){

            $permissions = array(
                'allowed' => array(
                    'default' => array(
                       	'dashboard' => array('tip-data'),
                        'auth' => array('login', 'logout', 'index'),
                        'reviews' => array('data'),
                        'comments' => array('data'),
                        'blacklisted-words' => '*',
                        'blacklisted-emails' => '*',
                        'video' => '*',
                        'escorts-v2' => '*',
                        'agencies' => array('index', 'data'/*, 'edit'*/)
                    ),
                    'geography' => array(
                    	'cities' => '*',
                    	'regions' => array('ajax'),
                    	'cityzones' => array('ajax')
                    ),
                    'sms' => '*'
                ),
                'disallowed' => array(
                )
            );

            if ( ! $this->hasAccess($request, $permissions)) {
                $request->setModuleName('default');
                $request->setControllerName('error');
                $request->setActionName('permission-denied');
                //die(header('Location: /dashboard'));
            }
        }
		
		// Zend_Controller_Front::getInstance()->registerPlugin(new Model_Plugin_Acl());
	}
		
}
