<?php
class Model_Alertme extends Cubix_Model
{
	protected $_table = 'alert_users';
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count, &$dif_members_count)
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				UNIX_TIMESTAMP(a.date) AS date, u.username, e.showname, e.id AS escort_id, u.id AS user_id
			FROM '.$this->_table.' a
			INNER JOIN users u ON u.id = a.user_id
			INNER JOIN escorts e ON e.id = a.escort_id
			WHERE 1
		';
		
		$countSql = 'SELECT FOUND_ROWS()';
		$dif_members_countSql = '
			SELECT COUNT(a.user_id) 
			FROM '.$this->_table.' a
			INNER JOIN users u ON u.id = a.user_id
			INNER JOIN escorts e ON e.id = a.escort_id
			WHERE 1
		';
		
		if ( strlen($filter['username']) ) {
			$where .= self::quote(' AND u.username LIKE ?', $filter['username'] . '%');
		}
		
		if ( strlen($filter['showname']) ) {
			$where .= self::quote(' AND e.showname LIKE ?', $filter['showname'] . '%');
		}

		$sql .= $where;
		$dif_members_countSql .= $where;

		$sql .= '
			GROUP BY a.user_id, a.escort_id 
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$dif_members_countSql .= '
			GROUP BY a.user_id
		';
		
		$result =  parent::_fetchAll($sql);

		$count = intval($this->getAdapter()->fetchOne($countSql));
		$ret = $this->getAdapter()->query($dif_members_countSql)->fetchAll();
		$dif_members_count = count($ret);

		return $result;
	}
	
	public function getEvents($user_id, $escort_id)
	{
		$sql = 'SELECT event FROM '.$this->_table.' WHERE user_id = ? AND escort_id = ?';
		$raws = $this->getAdapter()->query($sql,array($user_id, $escort_id))->fetchAll();
		
		$arr = array();
		
		foreach ($raws as $r)
		{
			$arr[] = $r->event;
		}
		
		return $arr;
	}		
}
