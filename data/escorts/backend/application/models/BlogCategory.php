<?php
class Model_BlogCategory extends Cubix_Model
{
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT b.id, b.title_en AS title, b.status
			FROM blog_category b 
			WHERE b.removed = 0 
		';
		
		$countSql = '
			SELECT COUNT(DISTINCT(b.id))
			FROM blog_category b 
			WHERE b.removed = 0 
		';
		
		$where = '';
		
		if ( $filter['id'] ) {
			$where .= self::quote('AND b.id = ?', $filter['id']);
		}
				
		if ( $filter['name'] ) {
			$where .= self::quote('AND b.title_en LIKE ?', '%' . $filter['name'] . '%');
		}
		
		if ( strlen($filter['status']) ) {
			$where .= self::quote('AND b.status = ?', $filter['status']);
		}
		
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			GROUP BY b.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return $this->getAdapter()->query($sql)->fetchAll();
	}
		
	public function save($data, $photos, $cover)
	{
		if (isset($data['id']) && $data['id'] != '')
		{
			$id = $data['id'];
			unset($data['id']);
			
			parent::getAdapter()->update('blog_category', $data, parent::quote('id = ?', $id));
		}
		else
		{
			$max = parent::getAdapter()->fetchRow('SELECT MAX(position) AS max FROM blog_category');

			if(!$max->max)$max->max = 0;
			$data['position'] = $max->max +1;

			parent::getAdapter()->insert('blog_category', $data);
			$id = parent::getAdapter()->lastInsertId();
		}
	}
	
	public function get($id)
	{
		return parent::getAdapter()->query('SELECT * FROM blog_category WHERE removed != 1 AND  id = ?', $id)->fetch();
	}
		
	public function remove($id)
	{
		parent::getAdapter()->update('blog_category', array('removed'=>1), parent::quote('id = ?', $id));	
	}
		
	public function updatePostion($id,$position)
	{
		parent::getAdapter()->update('blog_category', array('position'=>$position), parent::quote('id = ?', $id));	
	}

	public function toggle($id)
	{
		$field = 'status';
		
		parent::getAdapter()->update('blog_category', array(
			$field => new Zend_Db_Expr('NOT ' . $field)
		), parent::quote('id = ?', $id));
	}
	
	public function getList()
	{
		return parent::_fetchAll('
			SELECT id, title_en  AS title, layout, position
			FROM blog_category
			WHERE removed = 0
			ORDER BY position DESC');
	}
}
