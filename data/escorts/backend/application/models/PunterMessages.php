<?php

class Model_PunterMessages  extends Cubix_Model {
	
	const PUNTER_STATUS_PENDING  = 1;
	const PUNTER_STATUS_APPROVED = 2;
	const PUNTER_STATUS_DECLINED = 3;
	const PUNTER_STATUS_EXPIRED  = 4;

	protected $_table = 'punter_messages';

	public function getPunters(&$count, $status = null, $id = null, $user_id = null, $page = 1, $per_page = 10){

		$db = Zend_Registry::get('db');
		$sql = "SELECT * from punter_messages where 1 ";
		$where = ' ';
		if(isset($status) && $status != ''){
			$where .= ' AND status = '. $status;
		}
		if(isset($user_id) && $user_id != ''){
			$where .= " AND user_id=".$user_id;
		}
		if(isset($id) && $id != ''){
			$where .= " AND id = ". $id;
		}
		$where .= ' AND parent = 0';
		$sql .= $where;
		$sql .= '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page;

		$result = $db->fetchAll($sql);
		$count = $db->fetchOne('SELECT FOUND_ROWS()');

		return $result;
	}


	public function getPunterById($id){
		$db = Zend_Registry::get('db');
		$sql = 'SELECT * FROM punter_messages where id='. $id;
		$result = $db->fetchAll($sql)[0];
		return $result;
	}

	public function updatePunter($id, $user_id, $message, $status){
		$db = Zend_Registry::get('db');
		$sql = 'UPDATE punter_messages SET user_id='. $user_id .', message="'. $message .'", status='. $status .' WHERE id='. $id;
		$result = $db->query($sql);
		return $result;

	}



}





?>