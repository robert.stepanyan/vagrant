<?php
class Model_AgencyComments extends Cubix_Model
{
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
        $acDateField = 'date';
        $acMessField = 'comment';
        if (Cubix_Application::getId() ==APP_ED){
            $acDateField = 'time';
            $acMessField = 'message';
        }
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				ac.id,
				u.username,
				a.name AS agency_name,
				a.id AS agency_id,
				UNIX_TIMESTAMP(ac.'.$acDateField.') AS date,
				ac.status,
				ac.'.$acMessField.' AS comment
			FROM agency_comments ac
			INNER JOIN agencies a ON a.id = ac.agency_id
			INNER JOIN users u ON ac.user_id = u.id
			WHERE 1
		';

		$countSql = 'SELECT FOUND_ROWS()';

		$where = '';

		if ( strlen($filter['application_id']) ) {
			$where .= self::quote('AND u.application_id = ?', $filter['application_id']);
		}

		if ( strlen($filter['agency_name']) ) {
			$where .= self::quote(' AND a.name LIKE ? ', $filter['agency_name'] . '%');
		}

		if ( strlen($filter['username']) ) {
			$where .= self::quote(' AND u.username LIKE ?', $filter['username'] . '%');
		}

		if ( $filter['agency_id'] ) {
			$where .= self::quote('AND a.id = ?', $filter['agency_id']);
		}

		if ( strlen($filter['status']) ) {
			$where .= self::quote('AND ac.status = ?', $filter['status']);
		}

		if ( isset($filter[$acMessField]) && $filter[$acMessField]  ) {
			$where .= ' AND ac.'.$acMessField.' LIKE "%' . $filter[$acMessField] . '%" ';
		}

		if ( $filter['date_from'] && $filter['date_to'] ) {
			if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) ) {
				$where .= self::quote( " AND DATE(ac.$acDateField) = DATE(FROM_UNIXTIME(?))" , $filter['date_from']);
			}
		}

		if ( $filter['date_from'] ) {
			$where .= self::quote(" AND DATE(ac.$acDateField) >= DATE(FROM_UNIXTIME(?)) ", $filter['date_from']);
		}

		if ( $filter['date_to'] ) {
			$where .= self::quote(" AND DATE(ac.$acDateField) <= DATE(FROM_UNIXTIME(?)) ", $filter['date_to']);
		}

		$sql .= $where;

		$sql .= '
			GROUP BY ac.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$data = parent::_fetchAll($sql);

		$count = intval($this->getAdapter()->fetchOne($countSql));

		return $data;
	}

	public function getById($id)
	{
        $acDateField = 'date';
        $acMessField = 'comment';
        if (Cubix_Application::getId() ==APP_ED){
            $acDateField = 'time';
            $acMessField = 'message';
        }
		$sql = '
			SELECT
				ac.id, u.username, u.id AS user_id, a.name AS agency_name, a.id AS agency_id, UNIX_TIMESTAMP(ac.'.$acDateField.') AS date,
				ac.status, ac.'.$acMessField.', m.agency_comments_count
			FROM agency_comments ac
			INNER JOIN users u ON ac.user_id = u.id
			INNER JOIN agencies a ON ac.agency_id = a.id
			LEFT JOIN members m ON m.user_id = u.id
			WHERE ac.id = ?
		';

		return parent::_fetchRow($sql, array($id));
	}
	
	public function save($data)
	{
		if (isset($data['id']) && $data['id'] != '')
		{
		    if (Cubix_Application::getId() ==APP_ED){
                $fields = array(
                    'message' => $data['message']
                );
            }else{
                $fields = array(
                    'comment' => $data['comment']
                );
            }


			if (isset($data['status']))
				$fields['status'] = $data['status'];

			parent::getAdapter()->update('agency_comments', $fields, parent::quote('id = ?', $data['id']));
		}
	}

	public function remove($id)
	{
		$user_id = parent::getAdapter()->fetchOne('SELECT user_id FROM agency_comments WHERE id = ?', $id);
		parent::getAdapter()->delete('agency_comments', parent::quote('id = ?', $id));
		parent::getAdapter()->query('UPDATE members SET agency_comments_count = agency_comments_count - 1 WHERE user_id = ?', $user_id);
	}
}
