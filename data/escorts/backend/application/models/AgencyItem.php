<?php

class Model_AgencyItem extends Cubix_Model_Item
{
	public function getTotalOrdersPrice($is_paid = false)
	{
		$bind = array($this->getId());
		if ( $is_paid ) {
			$bind = array(
				$this->getId(),
				Model_Billing_Orders::STATUS_PAID
			);
			$where = ' AND o.status = ? ';
		}

		$sql = "
			SELECT
				SUM(o.price) AS total_orders_price
			FROM orders o
			INNER JOIN users u ON u.id = o.user_id
			INNER JOIN agencies a ON u.id = a.user_id
			WHERE a.id = ? {$where}
		";

		return $this->_adapter->query($sql, $bind)->fetchColumn();
	}
	public function getLogoUrl($size = 'backend_agency_thumb')
	{
		if ( $this->logo_hash ) {
			$images_model = new Cubix_Images();
			
			return $images_model->getUrl(new Cubix_Images_Entry(array(
				'application_id' => $this->application_id,
				'catalog_id' => 'agencies',
				'size' => $size,
				'hash' => $this->logo_hash,
				'ext' => $this->logo_ext
			)));
		}
		
		return NULL;
	}

	public function getUser()
	{
		$user_id = $this->_adapter->fetchOne('SELECT user_id FROM agencies WHERE id = ?', $this->getId());

		$item = new Model_UserItem();
		$item->setId($user_id);

		return $item;
	}
}
