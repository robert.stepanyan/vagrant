<?php

class Model_EscortTemplates extends Cubix_Model
{
	
	protected $_table = 'escort_templates';
	
	public function get($id,$app_id)
	{
		return self::_fetchAll('
			SELECT * FROM '.$this->_table.' WHERE id = ? AND application_id = ?
		', array($id , $app_id));
	}

	public function getById($id, $app_id)
	{
		return self::_fetchRow('
			SELECT * FROM '.$this->_table.' WHERE id = ? AND application_id = ?
		', array($id, $app_id));
	}

	public function getByIdLang($id, $app_id,$lang)
	{
		return self::_fetchRow('
			SELECT * FROM '.$this->_table.' WHERE id = ? AND application_id = ? AND lang_id = ?
		', array($id, $app_id, $lang));
	}
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT et.id, et.title, et.body , b.username
			FROM '.$this->_table.' et
			LEFT JOIN backend_users b ON b.id = et.sales_user_id
			WHERE 1
		';

		$countSql = '
			SELECT COUNT(DISTINCT(id))
			FROM '.$this->_table.' et
			WHERE 1
		';

		if ( strlen($filter['id']) ) {
			$sql .= self::quote('AND et.id LIKE ?', '%' . $filter['id'] . '%');
			$countSql .= self::quote('AND et.id LIKE ?', '%' . $filter['id'] . '%');
		}

		if ( strlen($filter['application_id']) ) {
			$sql .= self::quote('AND et.application_id = ?', $filter['application_id']);
			$countSql .= self::quote('AND et.application_id = ?', $filter['application_id']);
		}
		
		if ( isset($filter['sales_user_id']) ) {
			$sql .= self::quote('AND et.sales_user_id = ?', $filter['sales_user_id']);
			$countSql .= self::quote('AND et.sales_user_id = ?', $filter['sales_user_id']);
		}

		$sql .= '
			GROUP BY id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$countSql .= '

		';

		$count = intval($this->getAdapter()->fetchOne($countSql));

		return parent::_fetchAll($sql);
	}

	public function insert($item)
	{
		parent::getAdapter()->insert($this->_table, $item);
	}

	public function update($item, $array)
	{
		self::getAdapter()->query("
						UPDATE " .$this->_table. "
						SET
							body = ?
						WHERE id = ? AND application_id = ? AND lang_id = ?
					", $array );
	}
	public function remove($id)
	{
		parent::getAdapter()->delete($this->_table, parent::quote('id = ?', $id));
	}

	public static function getTemplates($filter)
	{
		$sql = ' SELECT id, title FROM escort_templates WHERE 1 ';

		if ( strlen($filter['application_id']) ) {
			$sql .= self::quote(' AND application_id = ?', $filter['application_id']);
		}
		if ( isset($filter['sales_user_id']) ) {
			$sql .= self::quote(' AND sales_user_id = ?', $filter['sales_user_id']);
		}
		$sql .= ' GROUP BY id ORDER BY id ';
		
		return self::db()->fetchAll( $sql );
	}
}
