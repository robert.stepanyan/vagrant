<?php

class Model_BlockUser extends Cubix_Model
{

    public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT
				bi.user_id as user_id, u.username as username, u.user_type, e.id as escort_id, a.id as agency_id
			FROM block_ip bi			
			LEFT JOIN users u ON u.id = bi.user_id
			LEFT JOIN escorts e ON u.id = e.user_id
			LEFT JOIN agencies a ON u.id = a.user_id
			WHERE bi.exp_time > "'.time().'"
		';
		
		$countSql = '
			SELECT COUNT(u.id)
			FROM block_ip bi
			LEFT JOIN users u ON u.id = bi.user_id
			LEFT JOIN escorts e ON u.id = e.user_id
			LEFT JOIN agencies a ON u.id = a.user_id
			WHERE 1
		';

		$where = '';
		
		if ( strlen($filter['u.username']) ) {
			$where .= self::quote('AND u.username LIKE ?', $filter['u.username'] . '%');
		}
		
		if ( strlen($filter['e.id']) ) {
			$where .= self::quote('AND e.id = ?', $filter['e.id']);
		}
		
		if ( strlen($filter['a.id']) ) {
			$where .= self::quote('AND a.id = ?', $filter['a.id']);
		}

		if ( strlen($filter['u.id']) ) {
			$where .= self::quote('AND u.id = ?', $filter['u.id']);
		}

		$sql .= $where;
		$countSql .= $where;
		

		$sql .= '
			GROUP BY u.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';


		/*echo $sql;
		echo "<br/>";
		echo $countSql;*/
		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return parent::_fetchAll($sql);
	}


    public function remove($id = array()){
        self::getAdapter()->delete('block_ip', self::getAdapter()->quoteInto('user_id = ?', $id));

        self::getAdapter()->update('users', array(
			'fail_login_count' => 0,
			'fail_login_time' => null,
		), self::quote('id = ?', $id));
    }

}
