<?php
class Model_Booking extends Cubix_Model
{
	protected $_table = 'booking_requests';
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT 
				e.showname, c.title_en AS country_name, UNIX_TIMESTAMP(b.request_date) AS request_date, b.id, b.city, b.duration, b.duration_unit,
				UNIX_TIMESTAMP(b.creation_date) AS creation_date
			FROM booking_requests b
			INNER JOIN escorts e ON b.escort_id = e.id
			INNER JOIN countries c ON b.country_id = c.id
			WHERE b.application_id = ' . Cubix_Application::getId() . '
		';
		
		$countSql = '
			SELECT COUNT(DISTINCT(b.id))
			FROM booking_requests b
			INNER JOIN escorts e ON b.escort_id = e.id
			WHERE b.application_id = ' . Cubix_Application::getId() . '
		';
		
		$where = '';
				
		if ( strlen($filter['showname']) ) {
			$where .= self::quote('AND e.showname LIKE ?', $filter['showname'] . '%');
		}
				
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			GROUP BY b.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$count = intval($this->getAdapter()->fetchOne($countSql));

		return parent::_fetchAll($sql);
	}	
	
	public function getById($id)
	{
		$sql = '
			SELECT
				e.showname, b.country_id, UNIX_TIMESTAMP(b.request_date) AS request_date, b.id, b.city, b.name, b.surname, b.address, b.zip, b.state, b.phone, b.email, b.duration, 
				b.message, b.contact, b.preferred_time, b.duration_unit
			FROM booking_requests b 
			INNER JOIN escorts e ON b.escort_id = e.id
			WHERE b.id = ? AND b.application_id = ' . Cubix_Application::getId() . '
		';

		return parent::_fetchRow($sql, array($id));
	}
	
	public function save($data)
	{
		if (isset($data['id']) && $data['id'] != '') {
			$id = $data['id'];
			unset($data['id']);

			parent::getAdapter()->update($this->_table, $data, parent::quote('id = ?', $id));
		}
	}
	
	public function remove($id)
	{
		parent::getAdapter()->delete($this->_table, parent::quote('id = ?', $id));
	}
}
