<?php
class Model_Agencies extends Cubix_Model
{
	protected $_table = 'agencies';
	protected $_itemClass = 'Model_AgencyItem';
	
	const STATUS_NOT_VERIFIED = 1;
	const STATUS_VERIFIED = 2;
	
	public function getAllForSeo($value)
	{
		$sql = '
			SELECT
				id, name
 			FROM agencies
			WHERE 1 AND name LIKE "%' . $value . '%"
		';
		
		$sql .= '
			GROUP BY id
		';

		$items = parent::_fetchAll($sql);
		
		return $items;
	}

	public function getLastComment($id)
	{
		return parent::getAdapter()->query('
			SELECT c.comment, UNIX_TIMESTAMP(c.date) AS date, b.username 
			FROM escort_comments c 
			LEFT JOIN backend_users b ON b.id = c.sales_user_id
			WHERE c.agency_id = ? ORDER BY c.date DESC LIMIT 1', $id)->fetch();
	}
	
	public function fixAgencies()
	{
		$sql = "
			SELECT a.id, a.user_id, a.name FROM agencies a GROUP BY a.user_id
		";

		$agencies = parent::_fetchAll($sql);

		$sql = "
			SELECT a.id, a.user_id, a.name FROM agencies a LEFT JOIN escorts e ON e.agency_id = a.id WHERE a.user_id = ?
		";
		$countSql = "SELECT count(e.id) FROM agencies a LEFT JOIN escorts e ON e.agency_id = a.id WHERE a.id = ?";
		$ce = 0;
		foreach ( $agencies as $agency ) {
			$ags = parent::_fetchAll($sql, $agency->user_id);

			$count = count($ags);
			if ( count($ags) > 1 ) {
				$c = 1;
				foreach ( $ags as $ag ) {
					$esc_count = parent::getAdapter()->fetchOne($countSql, $ag->id);
					
					if ( $esc_count == 0 && $count != $c ) {
						parent::getAdapter()->query('DELETE FROM agencies WHERE id = ?', array($ag->id));
						/*echo $ag->id . " -- " . $ce . "<br />";*/
						$ce++;
						$c++;
					}
				}
			}
		}
		echo $ce . " agency are deleted.";
	}

	public function getById($id)
	{
		$sel = '';
		if ( Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT ) {
			$sel = ' , a.fake_city_id, a.zip, a.status AS agency_status ';
		}
		elseif (Cubix_Application::getId() == APP_BL)
		{
			$sel = ' , a.about_nl ';
		} elseif (Cubix_Application::getId() == APP_ED) {
			$sel = ' , a.is_international, premium_end_date, premium_days, a.about_services, a.about_areas_covered';
		}
		
		if ( Cubix_Application::getId() == 33 || Cubix_Application::getId() == 22 || Cubix_Application::getId() == APP_ED ) {
			$sel .= ",a.min_book_time, a.min_book_time_unit, a.has_down_pay, a.down_pay, a.down_pay_cur, a.reservation";
		}

		if(Cubix_Application::getId() == APP_ED) {
            $sel .= ', a.data_entry_user, a.wechat, a.telegram, a.ssignal , a.viber_1, a.viber_2, a.whatsapp_1, a.whatsapp_2 ';
        }
        if(Cubix_Application::getId() == APP_EG_CO_UK) {
            $sel .= ', a.telegram, a.ssignal, a.wechat';
        }
        if(in_array(Cubix_Application::getId(),array(APP_ED,APP_EG_CO_UK))) {
            $sel .= ', a.updates_by';
        }
		
		$sql = '
			SELECT 
				a.id,
				a.name, 
				a.display_name,
				c.id AS city_id, 
				ap.id AS application_id, 
				a.email, a.phone, 
				u.status, u.username,				
				a.user_id, a.web,
				a.phone_country_id,
				a.phone_instructions, 
				a.contact_phone_parsed,
				a.country_id, 
				u.sales_user_id,
				a.logo_hash,
				a.logo_ext,
				u.username,
				u.email AS u_email,
				u.id AS user_id,
				u.balance,
				u.registered_ip,
				a.available_24_7,
				a.disabled_reviews,
				a.disabled_comments,
				a.is_club,
				a.entrance,
				a.wellness,
				a.food,
				a.outdoor,
				a.filter_criteria,
				a.is_premium,
				a.show_in_club_directory,
				a.address,
				a.latitude,
				a.longitude,
				a.about_en,
				a.about_fr,
				a.about_it,
				a.about_pt,
				a.about_de,
				a.about_gr,
				a.is_anonymous,
				a.check_website,
				a.block_website,
				a.viber,
				a.whatsapp,
				a.admin_verified,
				a.phone_country_id_1,
				a.phone_country_id_2,
				a.phone_1,
				a.phone_2,
				a.phone_instructions_1,
				a.phone_instructions_2,
				a.phone_instr,
				a.phone_instr_1,
				a.phone_instr_2,
				a.is_anonymous_1,
				a.is_anonymous_2,
				a.special_rates
				' . $sel . '
			FROM users u
			LEFT JOIN agencies a ON a.user_id = u.id
			LEFT JOIN cities c ON c.id = a.city_id
			LEFT JOIN applications ap ON ap.id = u.application_id
			LEFT JOIN escorts e ON e.agency_id = a.id
			WHERE 1 AND a.id = ?
		';
		$data = array();
		$agency = parent::_fetchRow($sql, array($id));
		
		$data['agency_data'] = $agency;
		
		$sql = "SELECT * FROM agency_working_times WHERE agency_id = ?";
		$working_hours = parent::_fetchAll($sql, array($id));
		$whs = array();		
		foreach ( $working_hours as $wh )
		{
			$whs[$wh->day_index] = array('day_index' => $wh->day_index, 'time_from' => $wh->time_from, 'time_to' => $wh->time_to);
		}
		
		if(Cubix_Application::getId() == APP_ED){
			$sql = "SELECT * FROM agency_langs WHERE agency_id = ?";
			$languages = parent::_fetchAll($sql, array($id));
			$data['working_hours'] = $whs;
			$data['langs'] = $languages;
		}
		return $data;
	}

	public function getByUserId($id)
	{
		$sql = '
			SELECT
				a.id,
				a.name,
				c.id AS city_id,
				ap.id AS application_id,
				a.email, a.phone,
				a.phone_country_id,
				u.status, u.username,
				a.user_id, a.web,
				a.phone_instructions,
				a.country_id,
				u.sales_user_id,
				COUNT(e.id) AS escorts_count,
				a.logo_hash,
				a.logo_ext,
				u.balance
			FROM users u
			LEFT JOIN agencies a ON a.user_id = u.id
			LEFT JOIN cities c ON c.id = a.city_id
			LEFT JOIN applications ap ON ap.id = u.application_id
			LEFT JOIN escorts e ON e.agency_id = a.id
			WHERE 1 AND u.id = ?
			GROUP BY a.id
		';
		
		$agency = parent::_fetchRow($sql, array($id));

		return $agency;
	}

	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count = 0)
	{
		$sql = array(
			'fields' => array(
				
				'a.name', 'u.application_id', 'UNIX_TIMESTAMP(a.last_modified) AS last_modified',
				'UNIX_TIMESTAMP(u.date_registered) AS creation_date', 'c.' . Cubix_I18n::getTblField('title') . ' AS city_title',
				'ap.title AS application_title', 'COUNT(e.id) AS escorts_count', 'a.email', 'a.phone',
				'u.status', 'a.logo_hash', 'a.logo_ext', 'u.balance', 'u.username', 'u.id AS user_id',
                'bu.username as sales_person_username,cu.iso AS app_iso', 'a.disabled_reviews', 'a.disabled_comments',
				'a.web','a.check_website', 'a.check_website_date','a.site_not_working','a.check_website_person', 'a.block_website' , 'a.block_website_date','a.block_website_person', 'a.slug'
			),
			'joins' => array(
				
			),
			'where' => $filter,
			'order' => array($sort_field, $sort_dir),
			'page' => array($page, $per_page)
		);

		if (in_array(Cubix_Application::getId(),array(APP_ED,APP_EG_CO_UK)))
        {
            $sql['fields'][] = 'a.updates_by';
        }
		$data = $this->_getAll($sql);
		
		$count = $data['count'];
		return $data['result'];
	}

	protected function _getAll($sql)
	{
		$_sql = array(
			'tables' => array('agencies a'),
			'fields' => array(
				'SQL_CALC_FOUND_ROWS a.id'
			),
			'joins' => array(
				'INNER JOIN users u ON u.id = a.user_id',
				'INNER JOIN applications ap ON ap.id = u.application_id',
				'LEFT JOIN cities c ON c.id = a.city_id',
				'LEFT JOIN escorts e ON e.agency_id = a.id',
                'LEFT JOIN backend_users bu ON bu.id = u.sales_user_id',
                'LEFT JOIN countries cu ON cu.id = ap.country_id'//,
				//'LEFT JOIN orders o ON o.user_id = u.id'
			),
			'where' => array(

			),
			'group' => 'a.id'
		);

		/* --> START Custom Filter Logic */
		$filter = $sql['where'];

		$m_escort = new Model_Escorts();

		if ( $filter['e.status'] != Model_Escorts::ESCORT_STATUS_DELETED ) {
			if ( $filter['excl_status'] ) {
				$filter = array_merge($filter, $m_escort->_whereStatus2('e.status', null, $filter['excl_status']));
			}
		}
        
		unset($filter['excl_status']);

		$having = '';
		if ( isset($filter['has_escorts']) ) {
			switch ($filter['has_escorts']){
			case 1:   //YES
				$sql['having'] = ' COUNT(e.id) > 0 ';
				BREAK;
			case 2:  //NO
				$sql['having'] = ' COUNT(e.id) = 0 ';
				BREAK;
			case 3:  //NO
				$filter[] = ' e.status & 32 ';
				BREAK;
			}
			unset($filter['has_escorts']);
		}
		
		if ( isset($filter['ever_login']) ) {
			//$sql['joins'][] = "LEFT JOIN users_last_login ull ON ull.user_id = u.id";
			switch ( $filter['ever_login'] ) {
				case 1: // Did Login
					$filter[] = ' u.last_login_date IS NOT NULL ';
					break;
				case 2: // Didn't login
					$filter[] = ' u.last_login_date IS NULL ';
					break;
			}
			unset($filter['ever_login']);
		}

        if(Cubix_Application::getId() == APP_ED || Cubix_Application::getId() == APP_EG_CO_UK){
            if ( isset($filter['sales_change_date_from']) || isset($filter['sales_change_date_to']) ) {
                if (isset($filter['sales_change_date_from'])) {
                    $filter[] = '( SELECT MAX( `date` ) as sales_change_date FROM sales_change_history WHERE user_id = u.id ) > DATE(FROM_UNIXTIME("' . $filter['sales_change_date_from'] . '"))';
                    unset($filter['sales_change_date_from']);
                }
                if (isset($filter['sales_change_date_to'])) {
                    $filter[] = '( SELECT MAX( `date` ) as sales_change_date FROM sales_change_history WHERE user_id = u.id ) < DATE(FROM_UNIXTIME("' . $filter['sales_change_date_to'] . '"))';
                    unset($filter['sales_change_date_to']);
                }
            }
            if (isset($filter['never_has_package']))
            {
                $filter[] = '(SELECT COUNT(	DISTINCT ( opk.order_id )) FROM	order_packages opk	INNER JOIN escorts esc ON opk.escort_id = esc.id	INNER JOIN agencies ag ON esc.agency_id = ag.id WHERE esc.agency_id = a.id	AND opk.application_id = '.Cubix_Application::getId().') = 0';
                unset($filter['never_has_package']);
            }
            if (isset($filter['no_100_verified_escort']))
            {
                $filter[] = '(SELECT COUNT(DISTINCT(esc.id)) FROM escorts esc INNER JOIN users usr ON usr.id = esc.user_id WHERE usr.status = 1 AND esc.verified_status = 2 AND esc.status = 32 AND esc.agency_id = a.id AND u.application_id = '.Cubix_Application::getId().') = 0';
                unset($filter['no_100_verified_escort']);
            }
        }

		if ( isset($filter['has_website']) ) {
			switch ($filter['has_website']){
			case 1:   //has website
				$filter[] = '( a.web IS NOT NULL AND CHAR_LENGTH(TRIM(a.web)) > 0 )';
				BREAK;
			case 2:  //doesn't have website
				$filter[] = '( a.web IS NULL OR CHAR_LENGTH(TRIM(a.web)) = 0 )';
				BREAK;
			}
			unset($filter['has_website']);
		}
		
		if ( isset($filter['admin_comment_by']) && $filter['admin_comment_by'] )
		{	
			if ( !$filter['admin_comment']) {
				$sql['joins'][] = 'LEFT JOIN escort_comments ec ON ec.agency_id = a.id';
			}
			$filter[] = ' ec.sales_user_id  = '.$filter['admin_comment_by'];
			unset($filter['admin_comment_by']);
			
			if (isset($filter['last_comment_by']) && $filter['last_comment_by'])
			{
				$filter[] = ' ec.date = (SELECT MAX(date) FROM escort_comments WHERE agency_id = a.id)';
				unset($filter['last_comment_by']);
			}
		}
		elseif (isset($filter['last_comment_by']))
			unset($filter['last_comment_by']);
		
		if ( isset($filter['admin_comment']) && $filter['admin_comment'] ) {
			switch ( $filter['admin_comment'] ) {
				case 1: // Has admin comment
					$sql['joins'][] = 'INNER JOIN escort_comments ec ON ec.agency_id = a.id';
					break;
				case 2: // Doesn't have
					$sql['joins'][] = 'LEFT JOIN escort_comments ec ON ec.agency_id = a.id AND ec.escort_id IS NULL';
					$filter[] = ' ec.comment IS NULL ';
					break;
			}
			
			unset($filter['admin_comment']);
		}
		
		if ( isset($filter['admin_comment_text']) && $filter['admin_comment_text'] ) {
			if (!in_array('LEFT JOIN escort_comments ec ON ec.agency_id = a.id', $sql['joins']))
				$sql['joins'][] = 'INNER JOIN escort_comments ec ON ec.agency_id = a.id';
			$filter[] = ' ec.comment LIKE "%' . $filter['admin_comment_text'] . '%" ' ;
			unset($filter['admin_comment_text']);
		}

		if (isset($filter['up_ag']) && $filter['up_ag'])
		{
			$dates = '';

			if ($filter['d_from'] && $filter['d_to'])
			{
				$dates = ' AND DATE(date) >= "' . date('Y-m-d', $filter['d_from']) . '" AND DATE(date) <= "' . date('Y-m-d', $filter['d_to']) . '"';
			}
			elseif ($filter['d_from'])
			{
				$dates = ' AND DATE(date) >= "' . date('Y-m-d', $filter['d_from']) . '"';
			}
			elseif ($filter['d_to'])
			{
				$dates = ' AND DATE(date) <= "' . date('Y-m-d', $filter['d_to']) . '"';
			}

			if ($filter['up_ag'] == 'last_updated')
			{
				if (!in_array('INNER JOIN escort_comments ec ON ec.agency_id = a.id', $sql['joins']))
					$sql['joins'][] = 'INNER JOIN escort_comments ec ON ec.agency_id = a.id AND ec.update_agency = 1';

				//$filter[] = ' (SELECT TRUE FROM escort_comments WHERE agency_id = a.id AND update_agency = 1 ' . $dates . ' ORDER BY date DESC LIMIT 1)';
			}
			else
			{
				if ( isset($filter['updates_count']) && $filter['updates_count'] )
				{
					if (!in_array('INNER JOIN escort_comments ec ON ec.agency_id = a.id', $sql['joins']))
						$sql['joins'][] = 'INNER JOIN escort_comments ec ON ec.agency_id = a.id';

					$filter[] = ' (SELECT COUNT(update_agency) FROM escort_comments WHERE agency_id = a.id AND update_agency = 1 ' . $dates . ') = ' . $filter['updates_count'];

					unset($filter['updates_count']);
				}
			}

			unset($filter['up_ag']);
			unset($filter['d_from']);
			unset($filter['d_to']);
		}
		
		$unsetHaveing = false;
		if ( isset($filter['has_package']) ) {
			$unsetHaveing = true;
			$sql['having'] = ' order_id_sum > 0 ';
			$sql['fields'][] = 'SUM(op.order_id) as order_id_sum';
			
			$sql['joins'][] = "LEFT JOIN order_packages op ON op.escort_id = e.id AND op.status = 2 AND op.order_id IS NOT NULL AND op.expiration_date > NOW()";
//			$sql['joins'][] = "";
			unset($filter['has_package']);
		}
		
		if ( strlen($filter['a.status']) && !$filter['a.status'] ) {
			$filter[] = 'a.status = 0';
		} elseif ( strlen($filter['a.status']) && $filter['a.status'] ) {
			$filter[] = 'a.status = 1';
		} 

		unset($filter['a.status']);

		if(isset($filter['phone_number']) && $filter['phone_number']){
			$filter[] = 'a.contact_phone_parsed LIKE  "%' . $filter['phone_number'] . '%"';
			unset($filter['phone_number']);
		}

        if($filter['a.country_id = ?']){
            if($filter['a.country_id = ?'] == 'all_except_usa') {
                $filter['a.country_id <> ?'] = 68;
                unset($filter['a.country_id = ?']);
            }elseif($filter['a.country_id = ?'] == 'all_except_usa_ca'){
                $filter['a.country_id <> ? '] = 68;
                $filter['a.country_id <> ?'] = 10;
                unset($filter['a.country_id = ?']);
            }elseif($filter['a.country_id = ?'] == 'all_except_usa_fr'){
                $filter['a.country_id <> ? '] = 68;
                $filter['a.country_id <> ?'] = 23;
                unset($filter['a.country_id = ?']);
            }
        }

		/*if ( isset($filter['a.web']) && $filter['a.web'] ) {
			$filter = array_merge($filter, array('a.web = ' . $filter['a.web']));
		}
		unset($filter['a.web']);*/
        //print_r($filter);die;
		// Like Statements
		
		foreach ( $filter as $f => $v ) {
			if ( is_int($f) || strpos($f, '?') !== false ) continue;

			unset($filter[$f]);
			if ( ! $v ) continue;
			$filter[$f . ' LIKE ?'] = $v . '%';
		}
		
		if ( $a_status === 0 || $a_status == '0' ) {
			$filter[] = 'a.status = 0';
		} elseif ( $filter['a.status'] == 1 ) {
			$filter[] = 'a.status = 1';
		}

		$sql['where'] = $filter;
			
		$sql = array_merge_recursive($_sql, $sql);
		$dataSql = Cubix_Model::getSql($sql);


		//echo $dataSql;die;
//		$sql['group'] = false;
//		$sql['fields'] = array('COUNT(DISTINCT(a.id))');
//		$sql['page'] = false;

//		if ( $unsetHaveing ){
//			unset( $sql['having']);
//		}
		
//		$countSql = Cubix_Model::getSql($sql);
//var_dump($dataSql);die;
		$countSql = 'SELECT FOUND_ROWS()';
		return array(
			'result' => parent::_fetchAll($dataSql),
			'count' => parent::getAdapter()->fetchOne($countSql)
		);
	}

	/*public function __getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT 
				a.id, a.name, u.application_id,
				UNIX_TIMESTAMP(a.last_modified) AS last_modified, 
				UNIX_TIMESTAMP(u.date_registered) AS creation_date, 
				c.' . Cubix_I18n::getTblField('title') . ' AS city_title, 
				ap.title AS application_title, 
				COUNT(e.id) AS escorts_count,
				a.email, 
				a.phone, 
				u.status,
				a.logo_hash,
				a.logo_ext,
				u.balance,
				u.username,
				u.id AS user_id
			FROM agencies a
			INNER JOIN users u ON a.user_id = u.id
			LEFT JOIN cities c ON c.id = a.city_id
			INNER JOIN applications ap ON ap.id = u.application_id
			LEFT JOIN escorts e ON e.agency_id = a.id
			WHERE 1
		';
		
		$countSql = '
			SELECT COUNT(DISTINCT(a.id))
			FROM agencies a
			INNER JOIN users u ON a.user_id = u.id
			LEFT JOIN cities c ON c.id = a.city_id
			INNER JOIN applications ap ON ap.id = u.application_id
			LEFT JOIN escorts e ON e.agency_id = a.id
			WHERE 1
		';
		
		$where = '';

		$user = Zend_Auth::getInstance()->getIdentity();
		if ( 'sales manager' == $user->type ) {
			$where .= self::quote(' AND u.sales_user_id = ? ', $user->id);
		}
		
		if ( strlen($filter['u.application_id']) ) {
			$where .= self::quote('AND u.application_id = ?', $filter['u.application_id']);
		}
		
		if ( strlen($filter['a.name']) ) {
			$where .= self::quote('AND a.name LIKE ?', $filter['a.name'] . '%');
		}

		if ( ! strlen($filter['u.status']) ) {
			$where .= self::quote('AND u.status <> ?', USER_STATUS_DELETED);
		}

		if ( strlen($filter['u.username']) ) {
			$where .= self::quote('AND u.username LIKE ?', $filter['u.username'] . '%');
		}
		
		if ( strlen($filter['a.email']) ) {
			$where .= self::quote('AND a.email LIKE ?', $filter['a.email'] . '%');
		}
		
		if ( strlen($filter['a.phone']) ) {
			$where .= self::quote('AND a.phone LIKE ?', $filter['a.phone'] . '%');
		}

		if ( strlen($filter['u.status']) ) {
			$where .= self::quote(' AND u.status = ?', $filter['u.status']);
		}
		
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			GROUP BY a.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		
		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return parent::_fetchAll($sql);
	}*/
	
	public function save(Model_AgencyItem $item)
	{
		$users_model = new Model_Users();
		
		$user_data = $item->getData(array('u_email', 'username', 'password', 'status', 'sales_user_id', 'application_id'));
		$user_data['activation_hash'] = md5(microtime());
		$user_data['disabled'] = 1;
		$user_data['user_type'] = Model_Users::USER_TYPE_AGENCY;
		$user_data['email'] = $user_data['u_email'];
		unset($user_data['u_email']);
		$data_entry = isset($item->data_entry_id) ? $item->data_entry_id : false;
		$curr_user_id = isset($item->user_id) ? $item->user_id : false;
		if ( $curr_user_id ) {
			$user_data['id'] = $curr_user_id;

			$db = parent::getAdapter();
			$user = Zend_Auth::getInstance()->getIdentity();

			$old_sales = $db->fetchRow('
				SELECT u.sales_user_id, bu.username, bu.email
				FROM users u
				INNER JOIN backend_users bu ON bu.id = u.sales_user_id
				WHERE u.id = ?
			', $curr_user_id);

			$new_sales = $user_data['sales_user_id'];
			$new_sales_username = $db->fetchRow('SELECT username FROM backend_users WHERE id = ?', $new_sales);
			if ( $user->type != 'superadmin' && $old_sales->sales_user_id != $new_sales ) {
				
				if($user->type == 'admin' && in_array(Cubix_Application::getId(), array(APP_ED,APP_EG_CO_UK))){
					$message = 'changed from '. $old_sales->username .' to '. $new_sales_username->username;
					Cubix_SyncNotifier::notifyAgency($item->user_id, $item->id, $message);
				}
				else{
					unset($user_data['sales_user_id']);

					$new_sales_hash = md5(microtime() * microtime());
					$db->update('users', array('new_sales_user_id' => $new_sales, 'new_sales_hash' => $new_sales_hash), $db->quoteInto('id = ?', $curr_user_id));

					$email_data = array(
						'new_sales_hash' => $new_sales_hash,
						'salesname' => $new_sales_username->username,
						'agencyname' => $item->name,
						'username' => $old_sales->username
					);
					Cubix_Email::sendTemplate('agency_new_sales_verification', $old_sales->email, $email_data);
				}
			}

            if($data_entry) {
                $db->update('agencies', array('data_entry_user' => $data_entry), $db->quoteInto('id = ?', $item->id));
            }

            if (in_array(Cubix_Application::getId(),array(APP_ED,APP_EG_CO_UK)) && $old_sales->sales_user_id && $user_data['sales_user_id'] && $user_data['sales_user_id'] != $old_sales->sales_user_id )
            {
                $db->insert('sales_change_history', array('user_id' => $user_data['id'],'modifier_id' => $user->id, 'user_type' => $user_data['user_type'], 'from_sales_id' => $old_sales->sales_user_id,'to_sales_id' => $user_data['sales_user_id']));
            }
		}

		if ( strlen($user_data['password']) )
		{
			$salt_hash = Cubix_Salt::generateSalt($user_data['password']);
			$pass = Cubix_Salt::hashPassword($user_data['password'], $salt_hash);
			$user_data['password'] = $pass;
			$user_data['salt_hash'] = $salt_hash;
		}
		else 
		{
			unset($user_data['password']);
		}

		$user_id = $users_model->save(new Model_UserItem($user_data));
		
		if ($user_data['sales_user_id'] && $curr_user_id && Cubix_Application::getId() == APP_ED) {
			if($old_sales->sales_user_id != $new_sales) {
				$db->insert('escort_comments', array(
					'agency_id' => $item->id,
					'comment' => 'change of sale from ' . $old_sales->username . ' to ' . $new_sales_username->username,
					'sales_user_id' => $user->id, 
					'date' => new Zend_Db_Expr('NOW()')
				));
			}
		}

		$get_data_array = array(
            'name', 'display_name', 'slug', 'country_id', 'city_id', 'email', 'web', 'phone','contact_phone_parsed', 'phone_country_id', 'phone_instructions', 'phone_instr',
            'available_24_7', 'address', 'latitude', 'longitude', 'is_anonymous','block_website','block_website_person','block_website_date','check_website',
            'check_website_person','check_website_date', 'entrance', 'wellness', 'food', 'outdoor', 'admin_verified', 'filter_criteria', 'is_premium', 'show_in_club_directory',
            'phone_1','contact_phone_parsed_1', 'phone_country_id_1', 'phone_instructions_1', 'is_anonymous_1', 'phone_instr_1',
            'phone_2','contact_phone_parsed_2', 'phone_country_id_2', 'phone_instructions_2', 'is_anonymous_2', 'phone_instr_2',
            'viber', 'whatsapp', 'special_rates'
        );

		if (Cubix_Application::getId() == APP_ED)
        {
            $get_data_array[] = 'viber_1';
            $get_data_array[] = 'viber_2';
            $get_data_array[] = 'whatsapp_1';
            $get_data_array[] = 'whatsapp_2';
        }

		$agency_data = $item->getData($get_data_array);
		$about = $item->getData(array('about'));
		$agency_data = array_merge($agency_data,$about['about']);
		if (in_array(Cubix_Application::getId(),array(APP_ED,APP_EG_CO_UK)))
        {
            $agency_data['updates_by'] = $item->updates_by;
        }
		if ( Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT ) {
		    if (Cubix_Application::getId() == APP_A6)
                $agency_data['status'] = $item->agency_status;

			$agency_data = array_merge($agency_data,  $item->getData(array('fake_city_id', 'zip')));
		}

		if ( Cubix_Application::getId() == APP_ED) {
			$agency_data = array_merge($agency_data,  $item->getData(array('about_services', 'about_areas_covered')));
			$agency_data = array_merge($agency_data,  $item->getData(array('wechat', 'telegram', 'ssignal')));
			$agency_data = array_merge($agency_data,  $item->getData(array('is_international')));
			$agency_data = array_merge($agency_data, array('status' => $item->getAgencyStatus()));

			if ( $item->premium_end_date ) {
				$agency_data = array_merge($agency_data,  array('premium_days' => $item->premium_days, 'premium_end_date' => new Zend_Db_Expr('FROM_UNIXTIME(' . $item->premium_end_date . ')')));
			} else {
				$agency_data = array_merge($agency_data,  array('premium_end_date' => null, 'premium_days' => null));
			}
		}

		if ( Cubix_Application::getId() == APP_EG_CO_UK){
            $agency_data = array_merge($agency_data,  $item->getData(array('wechat', 'telegram', 'ssignal')));
        }
		
		if ( Cubix_Application::getId() == 33 || Cubix_Application::getId() == 22 || Cubix_Application::getId() == APP_ED ) {
			$agency_data = array_merge($agency_data,  $item->getData(array('min_book_time', 'min_book_time_unit', 'has_down_pay', 'down_pay', 'down_pay_cur', 'reservation')));
		}
		
		if ( $curr_user_id ) {
			$agency_data['user_id'] = $curr_user_id;
		}
		else {
			$agency_data['user_id'] = $user_id;
		}
		
		$curr_agency_id = $item->getId();
		
		if( $curr_agency_id ) {
			$agency_data['id'] = $curr_agency_id;
			$agency_data['last_modified'] = new Zend_Db_Expr('NOW()');
		}
		
		if ($item['is_club'])
			$agency_data['is_club'] = 1;
		else
			$agency_data['is_club'] = 0;
		
		parent::save(new Model_AgencyItem($agency_data));
		//Insert Agency Spoken Languages
		if ( Cubix_Application::getId() == APP_ED ) {
			$agency_id = $curr_agency_id;
			if( $item['langs'] == '' ){
				parent::getAdapter()->delete('agency_langs', parent::quote('agency_id = ?', $agency_id));
			}else{
				parent::getAdapter()->delete('agency_langs', parent::quote('agency_id = ?', $agency_id));
				 if(is_array($item['langs'])){
				 	foreach ($item['langs'] as $l_item) {
						$exploded_item = explode(':', $l_item);
						$lang_item['agency_id'] = $agency_id;
						$lang_item['level'] = $exploded_item[1];
						$lang_item['lang_id'] = $exploded_item[0];
						parent::getAdapter()->insert('agency_langs', $lang_item);
				 	}
				}
			}
		}
		
		//Insert Agency Working Hours
		if( $curr_agency_id )
		{
			$agency_id = $curr_agency_id;
			parent::getAdapter()->delete('agency_working_times', parent::quote('agency_id = ?', $agency_id));
		}
		else
			$agency_id = parent::getAdapter()->lastInsertId();
		$working_hours = $item->getData(array('work_days', 'work_times_from', 'work_times_to'));
		if ( ! $agency_data['available_24_7'] ) {
			for($i = 1; $i <= 7; $i++)
			{
				if(isset($working_hours['work_days'][$i]))
				{
					parent::getAdapter()->insert('agency_working_times', array('agency_id' => $agency_id, 'day_index' => $working_hours['work_days'][$i], 'time_from' => $working_hours['work_times_from'][$i], 'time_to' => $working_hours['work_times_to'][$i]));
				}
			}
		}
		else {
			parent::getAdapter()->query('DELETE FROM agency_working_times WHERE agency_id = ?', parent::quote('agency_id = ?', $agency_id));
		}
		
		Model_Activity::getInstance()->log( ($item->getId() ? 'edit' : 'create') . '_agency', array('agency id' => $agency_id, 'name' => $item->name, 'email' => $item->u_email ));

		return $agency_id;
	}
	
	public function existsByUsername($username, $id = null)
	{
		$params = array($username);
		$sql = "SELECT TRUE FROM users WHERE username = ? AND status <> -5";
		
		if($id)
		{
			$params[] = $id;
			$sql .= " AND id <> ?";
		}
		
		return (bool) self::getAdapter()->fetchOne($sql, $params);
	}
	
	public function existsByEmail($email, $id = null, $agency_id = null)
	{
		$params = array($email);
		$sql = "SELECT TRUE FROM users WHERE status <> -5 AND email = ?";
		if($id)
		{
			$params[] = $id;
			$sql .= " AND id <> ?";
			$linked_agencies = $this->getLinkedAgencies($agency_id);
			
			if(isset($linked_agencies)){
				foreach($linked_agencies as $link){
					$params[] = $link->user_id;
					$sql .= " AND id <> ?";
				}
			}
		}
		
		return (bool) self::getAdapter()->fetchOne($sql, $params);
	}
	
	public function existsByName($name, $id = null)
	{
		$params = array($name);
		$sql = "SELECT TRUE FROM agencies a
			 INNER JOIN users u ON a.user_id = u.id
			 WHERE u.status <> -5 AND a.name = ?";
		if($id)
		{
			$params[] = $id;
			$sql .= " AND a.id <> ?";
		}
			
		return (bool) self::getAdapter()->fetchOne($sql, $params);
	}
	
	public function setLogo($id, $image)
	{
		$item = new Model_AgencyItem(array(
			'id' => $id,
			'logo_hash' => $image['hash'],
			'logo_ext' => $image['ext']
		));
		
		parent::save($item);
	}
	
	public function getList($app_id = null, $only_active = true)
	{
		$sql = '
			SELECT 
				a.id, a.name
			FROM agencies a
			INNER JOIN users u ON a.user_id = u.id
			/*INNER JOIN applications ap ON ap.id = u.application_id*/
			WHERE CHAR_LENGTH(a.name) > 0 AND u.status <> ' . USER_STATUS_DELETED . '
		';

		if ( ! is_null($app_id) ) {
			$sql .= self::quote(' AND u.application_id = ?', $app_id );
		}

		if ( $only_active ) {
			$sql .= self::quote(' AND u.status = ?', USER_STATUS_ACTIVE );
		}

		$sql .= ' ORDER BY a.name ASC ';
		$result = parent::_fetchAll($sql);
		
		$agencies = array();
		foreach ( $result as $row ) {
			$agencies[$row->id] = $row->name;
		}
		
		return $agencies;
	}

	public function delete($agency_id)
	{
		$m_agency = new Model_Agencies();
		$m_escorts = new Model_EscortsV2();

		$agency = $m_agency->getById($agency_id);
		$agency = $agency['agency_data'];
		
		$filter = array(
			'e.agency_id = ?' => $agency_id
		);

		//$_old_escorts = new Model_Escorts();
		$escorts = $m_escorts->getForAgencies(
			1,
			10000,
			$filter,
			'e.showname',
			'ASC',
			$count
		);

	
		$m_transfers = new Model_Billing_Transfers();
		$m_packages = new Model_Billing_Packages();
		$m_orders = new Model_Billing_Orders();

		$transfers = $m_transfers->getAllByUsertId($agency->user_id);
//var_dump($transfers);die;
		self::getAdapter()->beginTransaction();
		
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		if( in_array($bu_user->type, array('superadmin', 'admin'))){
			$immed = true;
		}
		else{
			$immed = false;
		}
		
		try {
			foreach ( $escorts as $escort ) {
				//var_dump($escort->id);
				$m_escorts->delete($escort->id,$immed);

				Model_Activity::getInstance()->log('delete_escort', array('escort id' => $escort->id));
			}
			
			foreach ( $transfers as $k => $transfer ) {
				$transfers[$k] = $m_transfers->getDetails($transfer->id);
			}

			foreach( $transfers as $transfer ) {
				$transfer->reject('Escort #' . $escort->id . ' has been deleted', true);
			}

			$m_orders->closeByUserId($agency->user_id);
			
			self::getAdapter()->update('users', array('status' => USER_STATUS_DELETED), self::quote('id = ?', $agency->user_id));

			//newsletter email log
			$client = Cubix_Api::getInstance();
			$conf = Zend_Registry::get('api_config');

			Cubix_Api_XmlRpc_Client::setApiKey(Cubix_Application::getById(Zend_Controller_Front::getInstance()->getRequest()->application_id)->api_key);
			Cubix_Api_XmlRpc_Client::setServer($conf['server']);

			$email = self::getAdapter()->fetchOne('SELECT email FROM users WHERE id = ' . $agency->user_id);

			$emails = array(
				'old' => $email,
				'new' => null
			);

			$client->call('addNewlstterEmailLog', array($agency->user_id, 'agency', 'delete', $emails));
			//
			
			if ( Cubix_Application::getId() == APP_A6 ) {
                self::getAdapter()->delete('linked_agencies', parent::quote('agency2 = ?', $agency_id));
				Cubix_SyncNotifier::notifyAgency($agency->user_id, $agency_id, 'Agency deleted!');
			}

			self::getAdapter()->commit();
		}
		catch ( Exception $e ) {
			self::getAdapter()->rollBack();

			throw $e;
		}
	}

	public function deleteLogo($agency_id){
        $item = new Model_AgencyItem(array(
            'id' => $agency_id,
            'logo_hash' => '',
            'logo_ext' => ''
        ));

        parent::save($item);
    }


    public function getAgencyNonPremiumEscorts($agency_id,$app_id){
        

        $escorts = self::getAdapter()->fetchAll('
            SELECT e.id, e.showname
            FROM escorts e
            WHERE e.agency_id = ? AND e.id NOT IN (
                SELECT e.id
                FROM escorts e
                INNER JOIN order_packages op ON op.escort_id = e.id
                WHERE
                e.agency_id = ? AND
                op.status = ? AND
                op.application_id = ? AND
                op.order_id IS NOT NULL
            )
        ', array($agency_id, $agency_id, 2, $app_id));

        return $escorts;
    }

	/* for Newsletter Api */

	public function getAllForNewsletterApi()
	{
		$sql = "SELECT a.name AS name, u.email AS email FROM agencies a LEFT JOIN users u ON u.id = a.user_id WHERE u.user_type = 'agency'";
		return self::getAdapter()->fetchAll($sql);
	}
	
	public function toggleReview($id)
	{
		parent::getAdapter()->update($this->_table, array(
			'disabled_reviews' => new Zend_Db_Expr('NOT disabled_reviews')
		), parent::quote('id = ?', $id));
	}
	
	public function toggleComment($id)
	{
		parent::getAdapter()->update($this->_table, array(
			'disabled_comments' => new Zend_Db_Expr('NOT disabled_comments')
		), parent::quote('id = ?', $id));
	}
	
	public function updateField($id, $field, $value)
	{
		parent::getAdapter()->update($this->_table, array(
			$field => $value
		), parent::quote('id = ?', $id));
	}
	
	public function getEscorts($id)
	{
		return parent::getAdapter()->query('SELECT id FROM escorts WHERE agency_id = ?', $id)->fetchAll();
	}

	public function getActiveEscorts($id)
	{
		return parent::getAdapter()->query('SELECT id FROM escorts WHERE agency_id = ? AND status & 32', $id)->fetchAll();
	}
	
	public function getCityId($id)
	{
		return parent::getAdapter()->fetchOne('SELECT city_id FROM agencies WHERE id = ?', $id);
	}

	public function disableAllEscorts($id)
	{
		$res = parent::getAdapter()->update('escorts', array(
			'status' => Model_Escorts::ESCORT_STATUS_ADMIN_DISABLED	),
			parent::quote('agency_id = ?', $id));
	}

	public function disableAgenciesAllEscorts($ids)
    {
        $agencies = self::getAdapter()->fetchAll('
			SELECT u.id as user_id, a.id as agency_id
			FROM users u
			INNER JOIN agencies a ON a.user_id = u.id
			WHERE a.id IN ('.implode(',',$ids).')');

        $agencies_list = array();
        $users_list = array();
        foreach ($agencies as  $agency)
        {
            $agencies_list[] = $agency->agency_id;
            $users_list[] = $agency->user_id;
        }

        $res = parent::getAdapter()->update('escorts', array(
            'status' => Model_Escorts::ESCORT_STATUS_ADMIN_DISABLED	),
            ' agency_id IN ('.implode(',',$agencies_list).')');

        $res2 = parent::getAdapter()->update('users', array(
            'status' => -4),
            'id IN ('.implode(',',$users_list).')');

        return $agencies;
    }

    public function inactiveAllEscorts($id)
    {
        $res = parent::getAdapter()->update('escorts', array(
            'status' => Model_Escorts::ESCORT_STATUS_INACTIVE),
            parent::quote('agency_id = ?', $id));
    }

    public function disableAgencies($ids)
    {
        $agencies = self::getAdapter()->fetchAll('
			SELECT u.id as user_id, a.id as agency_id
			FROM users u
			INNER JOIN agencies a ON a.user_id = u.id
			WHERE a.id IN ('.implode(',',$ids).')');

        $agencies_list = array();
        foreach ($agencies as  $agency)
        {
            $agencies_list[] = $agency->user_id;
        }
        $res = parent::getAdapter()->update('users', array(
            'status' => -4),
            'id IN ('.implode(',',$agencies_list).')');
        return $agencies;
    }
	
	public function verifyEscorts($escort_ids, $status)
	{
		
		parent::getAdapter()->update('escort_profiles_v2', array(
			'admin_verified' => $status	),
			'escort_id IN ('.$escort_ids. ')');
	}
	
	public function getAgencyContactData($agency_id, $app_id) {
        

        $data = self::getAdapter()->fetchRow('
            SELECT 
				a.phone AS phone_number, a.phone_1 as phone_number_1, a.phone_2 as phone_number_2,
				a.email, a.phone_instructions AS phone_instr_other, a.web AS website,
				cp.id AS country_id, cp.phone_prefix, cp.ndd_prefix,
				cp1.id AS country_id_1, cp1.phone_prefix as phone_prefix_1, cp1.ndd_prefix as ndd_prefix_1,
				cp2.id AS country_id_2, cp2.phone_prefix as phone_prefix_2, cp2.ndd_prefix as ndd_prefix_2
            FROM agencies a
			INNER JOIN users u ON u.id = a.user_id
			LEFT JOIN countries_phone_code cp ON cp.id = a.phone_country_id
			LEFT JOIN countries_phone_code cp1 ON cp1.id = a.phone_country_id_1
			LEFT JOIN countries_phone_code cp2 ON cp2.id = a.phone_country_id_2
            WHERE a.id = ? AND u.application_id = ?
        ', array($agency_id, $app_id));

        return $data;
    }
	
	public function getLinkedAgencies($agency_id) {
		$result = self::getAdapter()->fetchAll('
			SELECT la.agency2, a.name AS title, a.user_id
			FROM linked_agencies la
			INNER JOIN agencies a ON a.id = la.agency2
			WHERE agency1 = ?
		', array($agency_id));
		
		return $result;
	}
	
	public function insertPairs($agency_id, $agency_ids)
	{
		if ( ! count($agency_ids) ) {
			self::getAdapter()->query('DELETE FROM linked_agencies WHERE agency1 = ' . $agency_id . ' OR agency2 = ' . $agency_id);
			return;
		}
		
		$result = self::getAdapter()->fetchAll('SELECT agency2 AS id FROM linked_agencies WHERE agency1 IN (' . implode(',', $agency_ids) . ') GROUP BY agency2');
		
		
		foreach($result as $r) {
			if ( ! in_array($r->id, $agency_ids) ) {
				$agency_ids[] = $r->id;
			}
		}
		if ( ! in_array($agency_id, $agency_ids) ) {
			$agency_ids[]= $agency_id;
		}
		foreach($agency_ids as $ag_id) {
			self::getAdapter()->query('DELETE FROM linked_agencies WHERE agency1 = ' . $ag_id . ' OR agency2 = ' . $ag_id);
		}
		foreach($agency_ids as $i) {
			foreach($agency_ids as $y) {
				if ( $i == $y ) CONTINUE;
				self::getAdapter()->query('INSERT INTO linked_agencies (agency1, agency2) VALUES(?, ?)', array($i, $y));
			}
		}
	}

	public static function getWebsiteChecked($id){
		return self::getAdapter()->fetchOne('SELECT check_website FROM agencies WHERE id = ?', $id);
	}

	public static function getWebsiteblocked($id){
		return self::getAdapter()->fetchOne('SELECT block_website FROM agencies WHERE id = ?', $id);
	}

	public static function UpdateAgencyEscorts($id, $data){
		return self::getAdapter()->update('escorts',$data, parent::quote('agency_id = ?', $id) );
	}

    public function AddAgencyComment($id)
    {
        $bu_user = Zend_Auth::getInstance()->getIdentity();

        parent::getAdapter()->insert('escort_comments', array(
            'agency_id' => $id,
            'comment' => 'Backend user ' . $bu_user->first_name . '(ID: ' . $bu_user->id. ')' . ' clicked on edit button.',
            'sales_user_id' => $bu_user->id,
            'date' => new Zend_Db_Expr('NOW()')
        ));
    }

	public function UpdateEscortProfile($escort_id, $data){
		
		$db = parent::getAdapter();
		
		try {
				$db->update('escort_profiles_v2',$data, parent::quote('escort_id = ?', $escort_id) );

				$m_snapshot = new Model_Escort_SnapshotV2(array('id' => $escort_id));
				$m_snapshot->snapshotProfileV2();
				
				//Revisions update
				$revision_data = $db->fetchRow('SELECT data, revision FROM profile_updates_v2 WHERE escort_id = ? ORDER BY revision DESC LIMIT 1', array($escort_id));
				if ( $revision_data ) {
					$revision = $revision_data->revision;
					$revision_data = unserialize($revision_data->data);
					if(is_array($revision_data)){
						$revision_data = array_merge($revision_data, $data);
						$db->update('profile_updates_v2', array('data' => serialize($revision_data)), array($db->quoteInto('escort_id = ?', $escort_id), $db->quoteInto('revision = ?', $revision)));
					}
				}

				
			} catch( Exception $ex ) {
				return $ex;
			}
		return;	
	}
	
	public function getComments($page, $per_page, $filter, $sort_field, $sort_dir, &$count = 0)
	{		
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS ac.id AS ac_id, u.username, ac.comment, UNIX_TIMESTAMP(ac.date) AS date, ac.status
			FROM agency_comments ac
			INNER JOIN agencies a ON a.id = ac.agency_id
			INNER JOIN users u ON u.id = ac.user_id
			WHERE ac.agency_id = ?
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$data = $this->getAdapter()->query($sql, $filter['id'])->fetchAll();
		$count = $this->getAdapter()->fetchOne('SELECT FOUND_ROWS()');
		
		return $data;
	}
	
	public function toggleClubComment($id)
	{
		$st = parent::getAdapter()->fetchOne('SELECT status FROM agency_comments WHERE id = ?', $id);

		if ($st != 1)
			$new_st = 1;
		else
			$new_st = 0;

		parent::getAdapter()->update('agency_comments', array(
			'status' => $new_st
		), parent::quote('id = ?', $id));
	}
	
	public function removeComment($id)
	{
		parent::getAdapter()->delete('agency_comments', parent::quote('id = ?', $id));
	}
	
	public function addPhoto($agency_id, $image)
	{
		parent::getAdapter()->insert('agency_photos', array(
			'agency_id' => $agency_id,
			'hash' => $image['hash'],
			'ext' => $image['ext'],
		));
	}
	
	public function getPhoto($id)
	{
		return parent::getAdapter()->query('SELECT * FROM agency_photos WHERE id = ?', $id)->fetch();
	}
	
	public function getPhotos($agency_id)
	{
		return parent::getAdapter()->query('SELECT * FROM agency_photos WHERE agency_id = ?', $agency_id)->fetchAll();
	}
	
	public function removePhotos($ids)
	{		
		parent::getAdapter()->query('DELETE FROM agency_photos WHERE id IN (' . $ids . ')');
	}
	
	public function getLastIP($id)
	{
		return self::getAdapter()->fetchOne('SELECT u.ip FROM users_last_login u INNER JOIN agencies a ON a.user_id = u.user_id WHERE a.id = ? ORDER BY login_date DESC LIMIT 1', $id);
	}

	public function getLoginDate($id)
	{
		return self::getAdapter()->fetchOne('SELECT u.login_date FROM users_last_login u INNER JOIN agencies a ON a.user_id = u.user_id WHERE a.id = ? ORDER BY login_date DESC LIMIT 1', $id);
	}
	
	public function getIsAutoApproval($agency_id)
	{
		$sql = 'SELECT auto_approval FROM escorts WHERE agency_id = ?';
		
		$escorts = self::getAdapter()->fetchAll($sql, array($agency_id));
		$c = count($escorts);
		$c_a = $c_na = 0;
		
		if ( $c ) 
		{
			foreach ($escorts as $e)
			{
				if ($e->auto_approval == 0)
					$c_na++;
				else
					$c_a++;
			}
			
			if ($c == $c_na)
				return 'denied';
			elseif ($c == $c_a)
				return 'allow';
			else
				return 'mixed';
		}
		
		return 'no_escorts';
	}
	
	public function setAutoApproval($agency_id, $auto_approval)
	{
		$this->getAdapter()->update('escorts', array('auto_approval' => $auto_approval), $this->getAdapter()->quoteInto('agency_id = ?', $agency_id));
	}
	
	public static function getUserId($id){
		return self::getAdapter()->fetchOne('SELECT user_id FROM agencies WHERE id = ?', $id);
	}
	
	public function insertRates($agency_id, $data)
	{
		$db = parent::getAdapter();
		$db->delete('agency_rates', $db->quoteInto('agency_id = ?', $agency_id));
		// Inserting agency rates
		$rates = $data['rates'];
		if ( count($rates) > 0 ) {
			foreach( $rates as $rate ) {
				$rate['agency_id'] = $agency_id;
				$db->insert('agency_rates', $rate);
			}
		}
		
		if ( Cubix_Application::getId() == 33 || Cubix_Application::getId() == 22 || Cubix_Application::getId() == APP_ED ) {
			$db->delete('agency_travel_payment_methods', $db->quoteInto('agency_id = ?', $agency_id));
			$travel_payment_methods = $data['travel_payment_methods'];
			$travel_other_method = $data['travel_other_method'];

			if ( count($travel_payment_methods[0]) ) {
				foreach ( $travel_payment_methods[0] as $method ) {
					$d = array('agency_id' => $agency_id, 'payment_method' => $method);
					if ( $method == PM_OTHER ) {
						$d['specify'] = $travel_other_method[0];
						
					}
					$db->insert('agency_travel_payment_methods', $d);
				}
			}
		}
	}
	
	public function getRates($agency_id)
	{
		$rates = parent::_fetchAll('
			SELECT * FROM
			agency_rates
			WHERE agency_id = ?
		', $agency_id);

		return $rates;
	}
	
	public function getRatefields($agency_id)
	{
		$fields = 'special_rates';
		
		if ( Cubix_Application::getId() == 33 || Cubix_Application::getId() == 22 || Cubix_Application::getId() == APP_ED ) {
			$fields .= ",min_book_time, min_book_time_unit, has_down_pay, down_pay, down_pay_cur, reservation";
		}
		
		return parent::_fetchRow('SELECT '.$fields.' FROM	agencies WHERE id = ?', $agency_id);
	}
	
	public function getTravelPaymentMethods($agency_id)
	{
		$payment_methods =  parent::_fetchAll('
				SELECT payment_method, specify FROM agency_travel_payment_methods
				WHERE agency_id = ?
			', $agency_id);
		
		$payment_methods_array = array();
		$other_method = '';
		foreach($payment_methods as $method){
			$payment_methods_array[] = $method->payment_method;
			if ( $method->payment_method == PM_OTHER ) {
				$other_method = $method->specify;
			}
		}
		
		return array('payment_methods' => $payment_methods_array, 'other_method' => $other_method) ;
	}

	public function getUpdateCount($id)
	{
		$sql = 'SELECT COUNT(id) FROM escort_comments WHERE agency_id = ? AND update_agency = 1';

		return parent::getAdapter()->fetchOne($sql, $id);
	}
	
	public function getNameById($id)
	{
		$sql = 'SELECT name FROM agencies WHERE id = ?';
		return parent::getAdapter()->fetchOne($sql, $id);
	}

	public function getNotApprovedAgencies()
	{
		$sql = 'SELECT a.id, a.name, u.sales_user_id, u.date_registered, bu.username as sales_name FROM agencies a
				INNER JOIN users u ON u.id = a.user_id
				INNER JOIN backend_users bu ON bu.id = u.sales_user_id 
				WHERE a.status = ?';
		return parent::getAdapter()->fetchAll($sql, AGENCY_STATUS_NOT_APPROVED);
	}
	
	public function setStatus($agency_id, $status)
	{
		$db = parent::getAdapter();
		$db->update('agencies', array('status' => $status), $db->quoteInto('id = ?', $agency_id));
	}		

    public function showResponse($response)
    {
        // If is HTML5
        if(isset($response['id'])){
            header('Cache-Control: no-cache, must-revalidate');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Content-type: application/json');
        }
        echo json_encode($response);
    }


    public function getDataForAgencyExport(array $filter = array(), $sort_field = 'agency_id', $sort_dir = 'DESC')
    {

        $fields = array(
            'DISTINCT a.id AS agency_id',
            'e.id AS escort_id',
            'so.phone_to AS sms_phone',
            'a.user_id AS usr_id',
            'c.title_en AS country',
            'u.username',
            'CASE u.status WHEN 1 THEN "active" ELSE "disabled" END as agency_status',
            'u.date_registered AS registration_date',
            'u.last_login_date',
            'u.email as email',
            'a.contact_phone_parsed as phone_number',
//            'MIN(so.date) AS first_sms',
//            'MAX(so.date) AS last_sms',
            '(SELECT comment FROM escort_comments WHERE agency_id = a.id ORDER BY date DESC LIMIT 1) AS comment',
            '(SELECT bu.username FROM escort_comments ec LEFT JOIN backend_users bu ON bu.id = ec.sales_user_id WHERE ec.agency_id = a.id ORDER BY ec.date DESC LIMIT 1) AS commented_by',
            'e.last_hand_verification_date AS last_check_date',
            '(SELECT pck.name FROM order_packages op LEFT JOIN packages pck ON pck.id = op.package_id  WHERE op.escort_id = e.id AND op.status <> 2 ORDER BY op.date_activated DESC LIMIT 1) AS last_package',
//            '(SELECT active_package_name FROM escorts esc WHERE esc.agency_id = a.id )',
            'e.active_package_name',
            'e.package_expiration_date'
        );


        $sql = array(
            'tables' => array('agencies a'),
            'fields' => $fields,
            'joins' => array(
                'INNER JOIN users u ON u.id = a.user_id',
                'LEFT JOIN countries c ON c.id =  a.country_id',
                'LEFT JOIN escorts e ON e.agency_id = a.id ',
                'LEFT JOIN sms_outbox so ON so.phone_to = a.contact_phone_parsed',
            ),
            'where' => array(
                ) + $filter,
//            'group' => 'escort_id',
            'order' => array($sort_field, $sort_dir),
//            'page' => array($page, $per_page)

        );


        $dataSql = Cubix_Model::getSql($sql);
//        var_dump($dataSql);die;
        $data = parent::_fetchAll($dataSql);

        if( $data ) {
            foreach ($data as $dt) {
                $email = parent::_fetchRow('SELECT MAX(date) AS last_email, MIN(date) AS first_email FROM email_outbox em LEFT JOIN agencies a ON em.user_id = a.user_id  WHERE em.user_id = ?', $dt['usr_id']);
                $sms = parent::_fetchRow('SELECT MAX(date) AS last_sms, MIN(date) AS first_sms FROM sms_outbox so LEFT JOIN agencies a ON so.phone_to = a.contact_phone_parsed WHERE so.phone_to = ?', $dt['sms_phone']);
                $dt['last_email'] = $email['last_email'];
                $dt['first_email'] = $email['first_email'];
                $dt['last_sms'] = $sms['last_sms'];
                $dt['first_sms'] = $sms['first_sms'];
            }
        }

//        echo '<pre>';
//        print_r($id);die;
//        die;
        return array(
            'data' => $data
        );
    }

    public function getHandVerificationDate($agency_id)
    {
        return self::getAdapter()->fetchRow('
			SELECT ag.last_hand_verification_date, ag.hand_verification_sales_id, bu.username AS sales_name
			FROM agencies ag
			LEFT JOIN backend_users bu ON bu.id = ag.hand_verification_sales_id
			WHERE ag.id = ?', $agency_id);

    }

    public function updateHandVerificationDate($agency_id)
    {
        $bc_user = Zend_Auth::getInstance()->getIdentity();

        parent::getAdapter()->update($this->_table, array(
            'last_hand_verification_date' => new Zend_Db_Expr('NOW()'),
            'hand_verification_sales_id' => $bc_user->id,
        ), parent::quote('id = ?', $agency_id));

    }

	public function updateAgencySales($new_sales_user_id, $agency_id )
    {
    	return parent::getAdapter()->query('UPDATE agencies a,  users u SET u.sales_user_id = ? WHERE 	a.user_id = u.id AND a.id = ? ', array($new_sales_user_id, $agency_id ));
    }

    public function getFirstLoginLang($id)
    {
        return self::getAdapter()->fetchOne('SELECT u.lang FROM users u INNER JOIN agencies a ON a.user_id = u.id WHERE a.id = ? ', $id);
    }

    public function getAgencySmsData($agency_id)
    {
        return parent::getAdapter()->query('SELECT a.id, a.name, a.display_name, a.contact_phone_parsed, a.contact_phone_parsed_1, a.contact_phone_parsed_2 FROM agencies a WHERE a.id = ?',  $agency_id)->fetch();
    }

    public function addAdminComment($agency_id, $admin_verified, $free_comment, $due_date, $bo_user_id)
    {
        if (!$due_date)
            $due_date = NULL;
        else
            $due_date = date('Y-m-d', $due_date);

        $this->getAdapter()->insert('escort_comments', array(
            'agency_id' => $agency_id,
            'comment' => $free_comment,
            'remember_me' => $due_date,
            'sales_user_id' => $bo_user_id,
            'date' => new Zend_Db_Expr('NOW()'),
            'admin_verified' => $admin_verified
        ));
    }

    public function toggleUpdatesBy($id){
        $field = 'updates_by';

        parent::getAdapter()->update('agencies', array(
            $field => new Zend_Db_Expr('NOT ' . $field)
        ), parent::quote('id = ?', $id));
    }
}
