<?php
class Model_Blog extends Cubix_Model
{
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$select = "SELECT p.id, UNIX_TIMESTAMP(p.date) AS date, p.title_en AS title, p.status, p.special, bu.username, p.view_count, p.cover_image_id";
		
		if(Cubix_Application::getId() == APP_ED) $select .=', p.slider';
		
		$sql = $select . '
			FROM blog_posts p 
			LEFT JOIN backend_users bu ON p.backend_user_id = bu.id 
			WHERE 1
		';
			// LEFT JOIN blog_category bc ON p.category_id = bc.id 

		$countSql = '
			SELECT COUNT(DISTINCT(p.id))
			FROM blog_posts p 
			WHERE 1
		';
		
		$where = '';
		
		if ( $filter['id'] ) {
			$where .= self::quote('AND p.id = ?', $filter['id']);
		}
		
		if ( isset($filter['category_id']) && $filter['category_id'] != '' ) {
			$where .= self::quote('AND p.category_id = ?', $filter['category_id']);
		}

		// if ( $filter['layout'] ) {
		// 	$where .= self::quote('AND bc.layout = ?', $filter['layout']);
		// }

		if ( $filter['title'] ) {
			$where .= self::quote('AND p.title_en LIKE ?', '%' . $filter['title'] . '%');
		}
		
		if ( $filter['post'] ) {
			$where .= self::quote('AND p.post_en LIKE ?', '%' . $filter['post'] . '%');
		}
		
		if ( $filter['backend_user_id'] ) {
			$where .= self::quote('AND p.backend_user_id = ?', $filter['backend_user_id']);
		}
		
		if ( strlen($filter['status']) ) {
			$where .= self::quote('AND p.status = ?', $filter['status']);
		}
		
		if ( strlen($filter['missing_tr']) ) {
			$lang = $filter['missing_tr'];
			$where .= self::quote('AND p.title_'.$lang.' = "" OR p.post_'.$lang.' = ""');
		}
		
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			GROUP BY p.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return $this->getAdapter()->query($sql)->fetchAll();
	}
	
	public function toggle($id)
	{
		$field = 'status';
		
		parent::getAdapter()->update('blog_posts', array(
			$field => new Zend_Db_Expr('NOT ' . $field)
		), parent::quote('id = ?', $id));
	}
	
	public function special($id)
	{
		$field = 'special';
		
		parent::getAdapter()->update('blog_posts', array(
			$field => new Zend_Db_Expr('NOT ' . $field)
		), parent::quote('id = ?', $id));
	}
	
	
	public function slider($id)
	{
		if(Cubix_Application::getId() == APP_ED){
			$field = 'slider';
			
			parent::getAdapter()->update('blog_posts', array(
				$field => new Zend_Db_Expr('NOT ' . $field)
			), parent::quote('id = ?', $id));
		}
	}
	
	
	public function save($data, $photos, $cover)
	{

		$cover_image_id = key($cover);
		$data['cover_image_id'] = $cover_image_id;

		if (isset($data['id']) && $data['id'] != '')
		{
			$id = $data['id'];
			unset($data['id']);
			
			parent::getAdapter()->update('blog_posts', $data, parent::quote('id = ?', $id));

			parent::getAdapter()->delete('blog_images', parent::getAdapter()->quoteInto('post_id = ?', $id));
		}
		else
		{
			$data['month_count'] = (intval(date('Y')) - 1970) * 12 + intval(date('m')) - 1;
			
			parent::getAdapter()->insert('blog_posts', $data);
			$id = parent::getAdapter()->lastInsertId();
		}

		foreach ($photos as $photo_id => $photo)
		{
			parent::getAdapter()->insert('blog_images', array('post_id' => $id, 'image_id' => $photo_id));
		}
	}
	
	public function get($id)
	{
		$post = parent::getAdapter()->query('SELECT * FROM blog_posts WHERE id = ?', $id)->fetch();
		
		$p_sql = '
			SELECT
				i.id AS image_id, i.hash, i.ext, ' . Cubix_Application::getId() . ' AS application_id
			FROM blog_images bi
			INNER JOIN images i ON i.id = bi.image_id
			WHERE bi.post_id = ?
		';
		$photos = parent::getAdapter()->query($p_sql, $post->id)->fetchAll();

		if ($photos)
			foreach ($photos as $k => $photo)
				$photos[$k] = new Cubix_ImagesCommonItem($photo);
		else
			$photos = array();

		$p_sql = '
			SELECT
				i.id AS image_id, i.hash, i.ext, ' . Cubix_Application::getId() . ' AS application_id
			FROM images i
			WHERE i.id = ?
		';
		$cover = parent::getAdapter()->query($p_sql, $post->cover_image_id)->fetch();

		if ($cover)
			$cover = new Cubix_ImagesCommonItem($cover);
		else
			$cover = null;

        return array('post' => $post, 'photos' => $photos, 'cover' => $cover);
	}
		
	public function remove($id)
	{
		$cover_image_id = parent::getAdapter()->fetchOne('SELECT cover_image_id FROM blog_posts WHERE id = ?', $id);
		$images = parent::getAdapter()->query('SELECT image_id FROM blog_images WHERE post_id = ?', $id)->fetchAll();
		
		parent::getAdapter()->delete('blog_posts', parent::quote('id = ?', $id));
		parent::getAdapter()->delete('blog_comments', parent::quote('post_id = ?', $id));
		parent::getAdapter()->delete('blog_images', parent::quote('post_id = ?', $id));

		foreach ($images as $img)
			$this->removePhoto($img);

		if ($cover_image_id)
			$this->removePhoto($cover_image_id);
	}
	
	public function getAllComments($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT id, post_id, UNIX_TIMESTAMP(date) AS date, name, email, is_reply, ip, status 
			FROM blog_comments
			WHERE by_admin = 0
		';
		
		$countSql = '
			SELECT COUNT(DISTINCT(id))
			FROM blog_comments 
			WHERE 1
		';
		
		$where = '';
		
		if ( $filter['id'] ) {
			$where .= self::quote('AND id = ?', $filter['id']);
		}
		
		if ( $filter['post_id'] ) {
			$where .= self::quote('AND post_id = ?', $filter['post_id']);
		}
				
		if ( $filter['name'] ) {
			$where .= self::quote('AND name LIKE ?', $filter['name'] . '%');
		}
		
		if ( $filter['email'] ) {
			$where .= self::quote('AND email LIKE ?', $filter['email'] . '%');
		}
		
		if ( $filter['ip'] ) {
			$where .= self::quote('AND ip LIKE ?', $filter['ip'] . '%');
		}
		
		if ( $filter['comment'] ) {
			$where .= self::quote('AND comment LIKE ?', $filter['comment'] . '%');
		}
		
		if ( $filter['status'] ) {
			$where .= self::quote('AND status = ?', $filter['status']);
		}
		
		if ( $filter['is_reply'] ) {
			$where .= self::quote('AND is_reply > ?', 0);
		}
							
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			GROUP BY id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return $this->getAdapter()->query($sql)->fetchAll();
	}
	
	public function removeComment($id)
	{
		parent::getAdapter()->delete('blog_comments', parent::quote('id = ?', $id));
	}
	
	public function getComment($id)
	{
		return parent::getAdapter()->query('SELECT id, comment, UNIX_TIMESTAMP(date) AS date, name, email, status, ip FROM blog_comments WHERE id = ? AND by_admin = 0', $id)->fetch();
	}

	public function getAdminComments($id)
	{
		return parent::getAdapter()->query('SELECT id, comment, UNIX_TIMESTAMP(date) AS date FROM blog_comments WHERE is_reply = ? AND by_admin = 1 ORDER BY date ASC', $id)->fetchAll();
	}
	
	public function getPostByComment($com_id)
	{
		$sql = '
			SELECT p.id, UNIX_TIMESTAMP(p.date) AS date, p.title_en AS title, p.post_en AS post, p.status
			FROM blog_posts p 
			INNER JOIN blog_comments c ON c.post_id = p.id 
			WHERE c.id = ?
		';

		$post = parent::getAdapter()->query($sql, $com_id)->fetch();
		
		$is_reply = parent::getAdapter()->fetchOne('SELECT is_reply FROM blog_comments WHERE id = ?', $com_id);
		
		$comment = null;
		
		if ($is_reply > 0)
		{
			$comment = $this->getComment($is_reply);
		}
		
		return array('post' => $post, 'parent_comment' => $comment);
	}
	
	public function saveComment($data)
	{
		if (isset($data['id']) && $data['id'] != '')
		{
			$id = $data['id'];
			unset($data['id']);
			
			parent::getAdapter()->update('blog_comments', $data, parent::quote('id = ?', $id));
		}
	}
	
	public function removePhoto($image_id)
	{
		parent::getAdapter()->delete('images', parent::quote('id = ?', $image_id));
	}

	public function getList($title)
	{
		$bind = array($title . '%');

		return parent::_fetchAll('
			SELECT id, title_en AS title
			FROM blog_posts
			WHERE title_en LIKE ?
			GROUP BY id
			ORDER BY title_en ASC
			LIMIT 10
		', $bind);
	}

	public function addAdminComment($data)
	{
		parent::getAdapter()->insert('blog_comments', $data);
	}

	public function getForBlogPicker($id, $size, $lang)
	{
		$title 	= 'title_'.$lang;
		$post 	= 'short_post_'.$lang;

		$post = parent::getAdapter()->query("SELECT id, slug, cover_image_id, date, {$post} as post, {$title} as title  FROM blog_posts WHERE id = ? AND status = 1 and {$post} <> '' and {$title} <> '' ", $id)->fetch();
		if($post){
			$date = date_create($post->date);
			$post->date = date_format($date,"M d, Y");
			$post->post = substr($post->post, 0, 100) . '...';
			$p_sql = '
				SELECT
					i.id AS image_id, i.hash, i.ext, ' . Cubix_Application::getId() . ' AS application_id
				FROM images i
				WHERE i.id = ?
			';
			$cover = parent::getAdapter()->query($p_sql, $post->cover_image_id)->fetch();

			if ($cover){
				$cover->hash = $cover->hash .$size; 
				$cover = new Cubix_ImagesCommonItem($cover);
				$cover = $cover->getUrl();
				$link = Cubix_Application::getById(Cubix_Application::getId())->url . '/blog/' . $post->slug . '/' . $post->id;
	        	return array('post' => $post, 'image' => $cover, 'link' => $link);
			}
		}
		return false;
	}
}
