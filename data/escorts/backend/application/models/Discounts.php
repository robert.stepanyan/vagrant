<?php

class Model_Discounts extends Cubix_Model
{
	protected $_request;
	
	protected $_table = 'discounts';


    public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
    {
        $sql = '
			SELECT 
				d.id, d.title, d.description, d.promo_code, d.amount, d.`percent`, bu.username as bo_user, UNIX_TIMESTAMP(d.`date`) as `date`, d.`status`, d.`type`, d.days
			FROM discounts d
			LEFT JOIN backend_users bu ON bu.id = d.bo_user
			WHERE 1 
		';

        $countSql = '
			SELECT COUNT(DISTINCT(d.id))
			FROM discounts d
			LEFT JOIN backend_users bu ON bu.id = d.bo_user
			WHERE 1 
		';

        $where = '';
        if ( strlen($filter['title']) ) {
            $where .= self::quote(' AND d.title LIKE ? ', $filter['title'] . '%');
        }

        if ( strlen($filter['description']) ) {
            $where .= self::quote(' AND d.description LIKE ? ', $filter['description'] . '%');
        }

        if ( $filter['type'] ) {
            $where .= self::quote(' AND d.`type` = ? ', $filter['type']);
        }

        if ( $filter['days'] ) {
            $where .= self::quote(' AND d.`days` = ? ', $filter['days']);
        }

        if ( strlen($filter['order_id']) ) {
            $where .= self::quote(' AND d.order_id = ? ', $filter['order_id']);
        }

        if ( strlen($filter['escort_id']) ) {
            $where .= self::quote(' AND d.escort_id = ? ', $filter['escort_id']);
        }

        if ( $filter['promo_code'] ) {
            $where .= self::quote(' AND d.promo_code LIKE ? ', $filter['promo_code'] . '%');
        }

        if ( $filter['percent'] ) {
            $where .= self::quote(' AND d.percent LIKE ? ', $filter['status'] . '%');
        }

        if ( $filter['date_from'] ) {
            $where .= self::quote(' AND UNIX_TIMESTAMP(date) > ? ', $filter['date_from']);
        }

        if ( $filter['date_to'] ) {
            $where .= self::quote(' AND UNIX_TIMESTAMP(date) < ? ', $filter['date_to']);
        }

        $sql .= $where;
        $countSql .= $where;

        $sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
        $count = intval($this->getAdapter()->fetchOne($countSql));

        return $this->getAdapter()->query($sql)->fetchAll();
    }

    public function get($id)
    {
        $sql = '
			SELECT 
				d.id,d.title, d.description, d.promo_code , d.amount ,d.`percent`, bu.username as bo_user , UNIX_TIMESTAMP(d.`date`) as date , d.`status`, d.escort_id, d.order_id, d.type
			FROM discounts d
			LEFT JOIN backend_users bu ON bu.id = d.bo_user
			WHERE d.id = ?
		';

        $discount = parent::_fetchRow($sql, array($id));

        return $discount;
    }

	public function save($data)
	{
        $db = self::getAdapter();
        $issetPromo = $db->fetchOne('SELECT true FROM discounts WHERE promo_code = ?', $data['promo_code']);

	    if (!$issetPromo)
        {
            parent::getAdapter()->insert($this->_table, $data);
        }
	}
	
	public function update($data,$id)
	{
        $db = self::getAdapter();
        $issetPromo = $db->fetchOne('SELECT true FROM discounts WHERE promo_code = ?', $data['promo_code']);
        
        if (!$issetPromo)
        {
            parent::getAdapter()->update($this->_table, $data, parent::quote('id = ?', $id));
        }
	}

	public function remove($id)
	{
		parent::getAdapter()->delete($this->_table, parent::quote('id = ?', $id));
	}

	public function toggle($id, $type)
	{
		$field = 'status';

		parent::getAdapter()->update($this->_table, array(
			$field => new Zend_Db_Expr('NOT ' . $field)
		), parent::quote('id = ?', $id));
	}
}
