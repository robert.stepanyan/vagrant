<?php

class Model_EBSMS_EmailOutbox extends Cubix_Model
{	
	public function getAll($p, $pp, $sort_field, $sort_dir, $filter, &$count)
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				eo.id, eo.email, eo.subject, eo.text, eo.user_id, UNIX_TIMESTAMP(eo.date) AS date, bu.username
			FROM email_outbox eo
			LEFT JOIN escorts e ON e.user_id = eo.user_id
			LEFT JOIN backend_users bu ON bu.id = eo.backend_user_id
			WHERE 1
		';
				
		$where = '';
		
		if ( $filter['escort_id'] ) {
			$where .= " AND e.id = " . $filter['escort_id'];
		}
		
		if ( $filter['status'] ) {
			$where .= " AND e.status & " . $filter['status'];
		}
		
		if ( strlen($filter['backend_user_id']) > 0 && $filter['backend_user_id'] != -1) {
			$where .= " AND eo.backend_user_id = " . $filter['backend_user_id'];
		}

		if ( $filter['backend_user_id'] == -1) {
			$where .= " AND eo.backend_user_id IS NULL";
		}
		
		if ( $filter['email'] ) {
			$where .= " AND eo.email LIKE '%" . $filter['email'] . "%'";
		}
		
		if ( $filter['template_id'] ) {
			$where .= self::quote(" AND eo.template_id = ? " , $filter['template_id'] );
			
		}
		
		if ( $filter['date_from'] && $filter['date_to'] ) {
				if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) ) {
					$where .= self::quote( " AND DATE(eo.date) = DATE(FROM_UNIXTIME(?))" , $filter['date_from']);
				}
			}
		if ( $filter['date_from'] ) {
				$where .= self::quote(" AND DATE(eo.date) >= DATE(FROM_UNIXTIME(?)) ", $filter['date_from']);
			}
		
		$sql .= $where;
		$countSql = 'SELECT FOUND_ROWS()';
		
		$sql .= '
			GROUP BY eo.id 
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
		';
		
		$items = parent::_fetchAll($sql);
		
		$count = $this->getAdapter()->fetchOne($countSql);
		return $items;
	}
	
	public function getById($id)
	{
		return $this->getAdapter()->fetchRow('SELECT eo.id, eo.subject, eo.email, eo.text, eo.date, u.username FROM email_outbox eo 
				                       LEFT JOIN users u ON u.id = eo.user_id 
									   WHERE eo.id = ?', array($id));
	}	
	
	public function getRowByIds($ids)
	{
		return $this->getAdapter()->fetchAll('SELECT
	            email_outbox.*,
	            users.email AS email
                FROM
	            email_outbox
	            LEFT JOIN users ON users.id = email_outbox.user_id 
                WHERE email_outbox.id IN ('. implode(',' ,$ids) . ')');
	}
	
	public function add($data)
	{
        $data['year'] = new Zend_Db_Expr('YEAR(NOW())');
		$this->getAdapter()->insert('email_outbox', $data);
	}
}
