<?php

class Model_EBSMS_SalesChanges extends Cubix_Model
{
	
	protected $_table = 'sales_agent_ebsms';


	public function getAll($p, $pp,$user_id, $sort_field, $sort_dir, $filter, &$count , $userType)
    {

        $sql = '
			SELECT DISTINCT
                `sa`.* ,
                `bu`.first_name as sales_to_name
            FROM
                sales_agent_ebsms sa
                LEFT JOIN backend_users bu ON bu.id = sa.sales_to_id 
            WHERE ';

        $countSql = 'SELECT COUNT(sa.id) FROM
                sales_agent_ebsms sa
                LEFT JOIN backend_users bu ON bu.id = sa.sales_to_id 
            WHERE ';

        $where = '';
        if ( $userType != 'superadmin' ) {
            $where .= self::quote('sa.sales_from_id = ? ', $user_id);
            if ($filter['escort_id']) {
                $where .= self::quote('AND sa.escort_id = ?', $filter['escort_id']);
            }
            if ($filter['agency_id']) {
                $where .= self::quote('AND sa.agency_id = ?', $filter['agency_id']);
            }
            $where .= self::quote('AND sa.`status` = ? ', SALES_CHANGE_PENDING);
            $where .= self::quote('AND sa.id IN (SELECT MAX( sa.id ) FROM sales_agent_ebsms WHERE `status` = ? ', SALES_CHANGE_PENDING);
            $where .= self::quote('OR `status` = ? ', SALES_CHANGE_APPROOVED);
            $where .= self::quote('OR `status` = ? GROUP BY escort_id,agency_id)', SALES_CHANGE_DECLINED);

            $where .= self::quote('OR sa.`status` = ? ', SALES_CHANGE_APPROOVED);
            if ($filter['escort_id']) {
                $where .= self::quote('AND sa.escort_id = ?', $filter['escort_id']);
            }
            if ($filter['agency_id']) {
                $where .= self::quote('AND sa.agency_id = ?', $filter['agency_id']);
            }
            $where .= self::quote('AND sa.id IN (SELECT MAX( sa.id ) FROM sales_agent_ebsms WHERE `status` = ? ', SALES_CHANGE_PENDING);
            $where .= self::quote('OR `status` = ? ', SALES_CHANGE_APPROOVED);
            $where .= self::quote('OR `status` = ? GROUP BY escort_id,agency_id)', SALES_CHANGE_DECLINED);

            $where .= self::quote('OR sa.`status` = ? ', SALES_CHANGE_DECLINED);
            if ($filter['escort_id']) {
                $where .= self::quote('AND sa.escort_id = ?', $filter['escort_id']);
            }
            if ($filter['agency_id']) {
                $where .= self::quote('AND sa.agency_id = ?', $filter['agency_id']);
            }
            $where .= self::quote('AND sa.id IN (SELECT MAX( sa.id ) FROM sales_agent_ebsms WHERE `status` = ? ', SALES_CHANGE_PENDING);
            $where .= self::quote('OR `status` = ? ', SALES_CHANGE_APPROOVED);
            $where .= self::quote('OR `status` = ? GROUP BY escort_id,agency_id)', SALES_CHANGE_DECLINED);
        }else{
            $where .= ' `status` > 1 ';
        }

            $sql .= $where;
            $countSql.= $where;

            $sql .= '
                ORDER BY ' . $sort_field . ' ' . $sort_dir . '
                LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
            ';
        $count = $this->getAdapter()->fetchOne( $countSql );
        return parent::_fetchAll( $sql );
    }


    public function acceptSalesChangingRequest($requestId)
    {
        try {
            $sql = "UPDATE sales_agent_ebsms 
                SET `status` = ?
                WHERE
                    id = ?";

            return parent::_fetchAll($sql, array(SALES_CHANGE_APPROOVED,$requestId));
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }


    public function declineSalesChangingRequest($requestId)
    {
        try {
            $sql = "UPDATE sales_agent_ebsms 
                SET `status` = ?
                WHERE
                    id = ?";

            return parent::_fetchAll($sql, array(SALES_CHANGE_DECLINED,$requestId));
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }


    public function changeSalesAgentInUsersTable( $user_id, $sales_id, $escort_id = null, $agency_id = null )
    {
        try {
            $db = self::getAdapter();

            $curr_user = Zend_Auth::getInstance()->getIdentity();

            $old_sales = $db->fetchRow('
			SELECT u.sales_user_id, bu.username, bu.email
			FROM users u
			INNER JOIN backend_users bu ON bu.id = u.sales_user_id
			WHERE u.id = ?
		    ', $user_id );
            $new_sales = $sales_id;
            $new_sales_username = $db->fetchRow('SELECT username, email FROM backend_users WHERE id = ?', (int)$new_sales );

            $db->update('users', array('sales_user_id' => $sales_id), $db->quoteInto('id = ?', $user_id) );

            if ( $old_sales->sales_user_id != $new_sales && $escort_id ) {
                $db->insert('escort_comments', array(
                    'escort_id' => $escort_id,
                    'comment' => 'change of sale from ' . $old_sales->username . ' to ' . $new_sales_username->username,
                    'sales_user_id' => $curr_user->id,
                    'date' => new Zend_Db_Expr('NOW()')
                ));
            }
            if ( $new_sales != $old_sales->sales_user_id ) {
                return Cubix_SyncNotifier::notify( $escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PROFILE_TRANSFER, array( 'before_sales_id' => $old_sales->sales_user_id, 'after_sales_id' => $new_sales ));
            }
        }catch ( Exception $e ){
            return $e;
        }
    }
	
}
