<?php

class Model_EBSMS_ChangePhone extends Cubix_Model
{
	
	protected $_table = 'confirm_phone_number';
		
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT
				cfn.*, UNIX_TIMESTAMP(cfn.send_date) AS date, bu.username as sales , e.showname
			FROM confirm_phone_number cfn
			LEFT JOIN escorts e ON e.id = cfn.escort_id 
			LEFT JOIN users u ON u.id = e.user_id
			LEFT JOIN backend_users bu ON bu.id = u.sales_user_id
			WHERE 1
		';
		
		$countSql = '
			SELECT COUNT(cfn.escort_id)
			FROM confirm_phone_number cfn
			LEFT JOIN escorts e ON e.id = cfn.escort_id 
			LEFT JOIN users u ON u.id = e.user_id
			WHERE 1
		';

		$where = '';
		
		if ( strlen($filter['escort_id']) ) {
			$where .= self::quote('AND cfn.escort_id LIKE ?', $filter['escort_id'] . '%');
		}

		if ( strlen($filter['changed_count']) ) {
			$where .= self::quote('AND cfn.confirm_count > ?', $filter['changed_count']);
		}
		
		if ( strlen($filter['showname']) ) {
			$where .= self::quote('AND e.showname LIKE ?', $filter['showname']);
		}
		
		if ( strlen($filter['old_phone']) ) {
			$where .= self::quote('AND cfn.old_phone LIKE ?', '%'.$filter['old_phone'] . '%');
		}
		
		if ( strlen($filter['new_phone']) ) {
			$where .= self::quote('AND cfn.new_phone_parsed LIKE ?', '%'.$filter['new_phone'] . '%');
		}
		
		if ( strlen($filter['status']) ) {
			$where .= self::quote('AND cfn.status = ?', $filter['status']);
		}
		
		if ( strlen($filter['sales_user_id']) ) {
			$where .= self::quote('AND u.sales_user_id = ?', $filter['sales_user_id'] );
		}
		
		if ( strlen($filter['showname']) ) {
			$where .= self::quote('AND e.showname LIKE ?', $filter['bu.username'] . '%');
		}
		
		if ( $filter['date_from'] && $filter['date_to'] ) {
				if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) ) {
					$where .= self::quote( " AND DATE(cfn.send_date) = DATE(FROM_UNIXTIME(?))" , $filter['date_from']);
				}
			}
		if ( $filter['date_from'] ) {
				$where .= self::quote(" AND DATE(cfn.send_date) >= DATE(FROM_UNIXTIME(?)) ", $filter['date_from']);
			}

		if ( $filter['date_to'] ) {
			$where .= self::quote(" AND DATE(cfn.send_date) <= DATE(FROM_UNIXTIME(?)) ", $filter['date_to']);
		}
		
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return parent::_fetchAll($sql);
	}
	
	public function GetCurrent($escort_id)
	{
		return self::$_db->fetchRow("SELECT * FROM $this->_table WHERE escort_id=?",$escort_id);
	}
	
	public function getEscortDataByPhone(&$row)
	{
		$escort_data = self::$_db->fetchRow("SELECT e.id, e.showname, bu.username as sales FROM escort_profiles_v2 ep
									INNER JOIN escorts e ON e.id = ep.escort_id 
									INNER JOIN users u ON u.id = e.user_id
									LEFT JOIN backend_users bu ON bu.id = u.sales_user_id
									WHERE ep.phone_exists = ?", $row->new_phone_parsed);
		//var_dump($escort_data);
		$row->escort_id =  $escort_data->id;
		$row->sales =  $escort_data->sales;
		$row->showname =  $escort_data->showname;
		return; 
	}
		
	
}
