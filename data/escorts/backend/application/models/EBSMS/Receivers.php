<?php

class Model_EBSMS_Receivers extends Cubix_Model
{
	public function getAll($p, $pp, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				r.*, b.username
			FROM receivers r
			LEFT JOIN backend_users b ON b.id = r.backend_user_id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
		';

		$count = $this->getAdapter()->fetchOne('SELECT FOUND_ROWS()');

		return parent::_fetchAll($sql);
	}

	public function get($id)
	{
		$sql = 'SELECT * FROM receivers	WHERE id = ?';
		
		return parent::_fetchRow($sql, $id);
	}

	public function save($data)
	{
		if (isset($data['id']) && $data['id'] != '')
		{
			$id = $data['id'];

			parent::getAdapter()->update('receivers', array(
				'number' => $data['number'],
				'gotd' => $data['gotd'],
				'daypass' => $data['daypass'],
				'other' => $data['other'],
				'backend_user_id' => $data['backend_user_id']
			), parent::quote('id = ?', $id));
		}
		else
		{
			parent::getAdapter()->insert('receivers', array(
				'number' => $data['number'],
				'gotd' => $data['gotd'],
				'daypass' => $data['daypass'],
				'other' => $data['other'],
				'backend_user_id' => $data['backend_user_id']
			));

			$id = parent::getAdapter()->lastInsertId();
		}

		return $id;
	}	
	
	public function remove($id)
	{
		parent::getAdapter()->delete('receivers', parent::quote('id = ?', $id));
	} 
}
