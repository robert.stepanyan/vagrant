<?php

class Model_EBSMS_Trash extends Cubix_Model
{
	
	protected $_table = 'ebsms_trash';
	//protected $_itemClass = 'Model_EBSMS_EBSMSItem';
	
	public function get($id)
	{
		$sql = '
			SELECT
				st.id, UNIX_TIMESTAMP(st.date) AS date, e.showname, ep.contact_phone_parsed, st.phone_from, u.status, bu.username AS sales_person,
				st.text, st.is_read
			FROM ebsms_trash st
			LEFT JOIN escort_profiles_v2 ep ON ep.escort_id = st.escort_id
			LEFT JOIN escorts e ON ep.escort_id = e.id			
			LEFT JOIN users u ON u.id = e.user_id
			LEFT JOIN backend_users bu ON bu.id = u.sales_user_id
			WHERE st.id = ?
		';
		
		return parent::_fetchRow($sql, $id);
	}
	
	public function getAll($p, $pp, $sort_field, $sort_dir, $filter, &$count)
	{
		$sql = '
			SELECT
				st.id, st.type, UNIX_TIMESTAMP(st.date) AS date, e.showname, ep.contact_phone_parsed, st.phone_from, u.status, bu.username AS sales_person,
				IF(LENGTH(st.text) > 30, CONCAT(SUBSTRING(st.text, 1, 30), \' ...\'), st.text) AS text, st.is_read, e.id AS escort_id
			FROM ebsms_trash st
			LEFT JOIN escort_profiles_v2 ep ON ep.escort_id = st.escort_id
			LEFT JOIN escorts e ON ep.escort_id = e.id			
			LEFT JOIN users u ON u.id = e.user_id
			LEFT JOIN backend_users bu ON bu.id = u.sales_user_id
			WHERE 1
		';
		
		$countSql = '
			SELECT
				COUNT(st.id)
			FROM ebsms_trash st
			LEFT JOIN escorts e ON st.escort_id = e.id
			LEFT JOIN users u ON u.id = e.user_id
			WHERE 1
		';
		
		$where = '';
		
		if ( strlen($filter['s.status']) ) {
			$where .= self::quote('AND s.status = ?', $filter['s.status']);
		}
		if ( $filter['date_from'] && $filter['date_to'] ) {
				if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) ) {
					$where .= self::quote( " AND DATE(st.date) = DATE(FROM_UNIXTIME(?))" , $filter['date_from']);
				}
			}
		if ( $filter['date_from'] ) {
				$where .= self::quote(" AND DATE(st.date) >= DATE(FROM_UNIXTIME(?)) ", $filter['date_from']);
			}

		if ( $filter['date_to'] ) {
			$where .= self::quote(" AND DATE(st.date) <= DATE(FROM_UNIXTIME(?)) ", $filter['date_to']);
		}
		
		if ( $filter['application_id'] ) {
			$where .= self::quote(" AND st.application_id = ?", $filter['application_id']);
		}
		
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
		';
		
		$count = $this->getAdapter()->fetchOne($countSql);
		return parent::_fetchAll($sql);
	}
	
	public function deleteForever($ids)
	{
		foreach ( $ids as $id )
		{
			parent::remove(self::quote('id = ?', $id));
		}
	} 
}
