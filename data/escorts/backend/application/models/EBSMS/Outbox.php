<?php

class Model_EBSMS_Outbox extends Cubix_Model
{
	
	protected $_table = 'ebsms_outbox';
	//protected $_itemClass = 'Model_EBSMS_EBSMSItem';
	
	public function get($id)
	{
		$fields = '';
		
		$sql = '
			SELECT
				so.id, UNIX_TIMESTAMP(so.date) AS date, e.showname, ep.contact_phone_parsed, so.phone_to, so.phone_from, u.status, bu.username AS sales_person,
				so.text, so.status AS ebsms_status, UNIX_TIMESTAMP(so.delivery_date) AS delivery_date, so.escort_ids' . $fields. '
			FROM ebsms_outbox so
			LEFT JOIN escort_profiles_v2 ep ON ep.escort_id = so.escort_id
			LEFT JOIN escorts e ON ep.escort_id = e.id			
			LEFT JOIN users u ON u.id = e.user_id
			LEFT JOIN backend_users bu ON bu.id = u.sales_user_id
			WHERE so.id = ?
			ORDER BY so.id DESC
		';

		return parent::_fetchRow($sql, $id);
	}
	
	public function getEscortsByPhone($phone, &$data)
	{
		$sql = '
			SELECT
				ep.escort_id, ep.showname, e.status, bu.username, e.user_id 
			FROM ( SELECT escort_id FROM escort_profiles_v2 WHERE contact_phone_parsed = ? LIMIT 100 ) epx
			INNER JOIN escort_profiles_v2 ep ON ep.escort_id = epx.escort_id
			INNER JOIN escorts e ON ep.escort_id = e.id	
			LEFT JOIN users u ON u.id = e.user_id
			LEFT JOIN backend_users bu ON bu.id = u.sales_user_id
		';
		
		$escorts = parent::_fetchAll($sql, $phone);
//		print_r($escorts);die;
		if ( $escorts ) {
			$DEFINITIONS = Zend_Registry::get('defines');
			$e_model = new Model_EscortsV2();
			
			if ( count($escorts) == 1 ) {
//			  print_r($escorts);die;
				$data->showname = "<span class='escort-popup'><a href='#".$escorts[0]->escort_id."'>".$escorts[0]->showname. " (" . $escorts[0]->escort_id . ")</a></span>";
				$data->sales_person = $escorts[0]->username;
//				print_r($data);die;
				$stat = array();
				foreach( $DEFINITIONS['escort_status_options'] as $key => $e_status )
				{
					if ( $e_model->hasStatusBit($escorts[0]->escort_id, $key) )
					{
						$stat[] = $e_status;
					}
				}
//                print_r($stat);
				$data->status = implode(', ', $stat);
//                print_r($data);
			}
			else {
				$uids = array();
				
				foreach ($escorts as $escort)
				{
					$uid = intval($escort->user_id);
					
					if (!in_array($uid, $uids))
						$uids[] = $uid;
				}
				
				$escorts_str = "";
				
				if (count($uids) == 1 && !in_array(0, $uids))
				{
					//agency
					
					$name = parent::getAdapter()->fetchOne('SELECT name FROM agencies WHERE user_id = ?', $uids[0]);
					
					$escorts_str = $name . ' <strong>(Agency)</strong>';
				}
				else
				{
					$escorts_str = "<span class='escort-popup'>";
					foreach ( $escorts as $k => $escort ) 
					{
						$escorts_str .= "<a href='#". $escort->escort_id."'>" . $escort->showname . "</a><br/>";

						if ( $k == 9 ) break;
					}
					$escorts_str .= "</span>";
				}
//				print_r($data);
				$data->showname = $escorts_str;
			}
		}
	}
	
	public function getAll($p, $pp, $sort_field, $sort_dir, $filter, &$count)
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				so.id, UNIX_TIMESTAMP(so.date) AS date, e.showname, ep.contact_phone_parsed, so.phone_to, e.status, e.status as escort_status, bu.username AS sales_person, bu2.username AS sender_sales_person, 
				IF(LENGTH(so.text) > 30, CONCAT(SUBSTRING(so.text, 1, 30), \' ...\'), so.text) AS text, so.status AS ebsms_status, UNIX_TIMESTAMP(so.delivery_date) AS delivery_date, e.id AS escort_id
			FROM ebsms_outbox so
			LEFT JOIN escort_profiles_v2 ep ON ep.escort_id = so.escort_id
			LEFT JOIN escorts e ON ep.escort_id = e.id			
			LEFT JOIN users u ON u.id = e.user_id
			LEFT JOIN backend_users bu ON bu.id = u.sales_user_id
			LEFT JOIN backend_users bu2 ON bu2.id = so.sender_sales_person
			WHERE 1
		';
		
		/*$countSql = '
			SELECT
				COUNT(so.id)
			FROM ebsms_outbox so
			LEFT JOIN escorts e ON so.escort_id = e.id
			LEFT JOIN users u ON u.id = e.user_id
			WHERE 1
		';*/
		
		$where = '';
		
		if ( strlen($filter['s.status']) ) {
			$where .= self::quote('AND s.status = ?', $filter['s.status']);
		}
		if ( $filter['date_from'] && $filter['date_to'] ) {
				if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) ) {
					$where .= self::quote( " AND DATE(so.date) = DATE(FROM_UNIXTIME(?))" , $filter['date_from']);
				}
			}
		if ( $filter['date_from'] ) {
				$where .= self::quote(" AND DATE(so.date) >= DATE(FROM_UNIXTIME(?)) ", $filter['date_from']);
			}

		if ( $filter['date_to'] ) {
			$where .= self::quote(" AND DATE(so.date) <= DATE(FROM_UNIXTIME(?)) ", $filter['date_to']);
		}
		
		if ( $filter['showname'] ) {
			$where .= " AND e.showname LIKE '" . $filter['showname'] . "%'";
		}
		
		if ( $filter['escort_id'] ) {
			//$filter['phone_to'] = Model_EscortsV2::getPhoneParsedById($filter['escort_id']);
			$where .= " AND so.escort_id = " . $filter['escort_id'];
		}
		
		if($filter['only_this_user']){ // ONLY FOR EF AND SALES USERS
			$where .= " AND (so.sender_sales_person = " . $filter['only_this_user']. " OR u.sales_user_id = " . $filter['only_this_user'] . ")";
		}else{
			if ( $filter['sender_sales_person'] ) {
				$where .= " AND so.sender_sales_person = " . $filter['sender_sales_person'];
			}
		}
		
		if ( $filter['e_status'] ) {
			$where .= " AND e.status = " . $filter['e_status'];
		}
		
		if ( strlen($filter['status']) > 0 ) {
			$where .= " AND so.status = " . $filter['status'];
		}
		
		if ( $filter['phone_to'] ) {
			$phone_to = str_replace(' ', '', $filter['phone_to']);
			$where .= " AND so.phone_to LIKE '%" . $phone_to . "%'";
		}
		
		if ( $filter['year'] ) {
			$where .= " AND so.year = " . $filter['year'];
		}
		
		if ( $filter['skype_video_call'] ) {
			$where .= " AND so.is_video_call = 1";
		}
		
		$sql .= $where;
		$countSql = 'SELECT FOUND_ROWS()';
		
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
		';
//		var_dump($sql);die;
//        var_dump($filter);die;
		$items = parent::_fetchAll($sql);
		
		$count = $this->getAdapter()->fetchOne($countSql);
		return $items;
	}
	
	public function save($data)
	{
		$escort_id = null;
		$escort_ids = null;
		if ( count($data['e_ids']) > 1 )
		{
			foreach($data['e_ids'] as $e_id)
			{
				$escort_ids[] = $e_id;	
			}
			$escort_ids = implode(',', $escort_ids);
		}		
		elseif ( count($data['e_ids']) == 1 )
			$escort_id = $data['e_ids'][0];
		
		if ( $data['escort_id'] ) {
			$escort_id = $data['escort_id'];
		}
		if (Cubix_Application::getId() != APP_6B){
            $sql = 'INSERT INTO ebsms_outbox (escort_id, escort_ids, phone_to, phone_from, text, application_id, sender_sales_person, is_spam, year ) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)';
        }else{
            $sql = 'INSERT INTO ebsms_outbox (escort_id, escort_ids, phone_to, phone_from, text, application_id, sender_sales_person, is_spam, year, identificationNumber) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
        }

		
		if ( is_array($data['phone_number']) ){
			$i = 0;
			foreach ($data['phone_number'] AS $phone)
			{
			    $bind = array($data['e_ids'][$i], $escort_ids, $phone, $data['phone_from'], $data['text'], $data['application_id'], $data['sender_sales_person'], 0 , date("Y"));
			    if (Cubix_Application::getId() == APP_6B){
			        array_push($bind, $data['identificationNumber']);
                }
				$this->getAdapter()->query($sql, $bind);
				$i++;
			}
		}
		else{
            $bind = array($escort_id, $escort_ids, $data['phone_number'], $data['phone_from'], $data['text'], $data['application_id'], $data['sender_sales_person'], $data['is_spam'] ? 1 : 0, date("Y"));
            if (Cubix_Application::getId() == APP_6B){
                array_push($bind, $data['identificationNumber']);
            }
			$this->getAdapter()->query($sql, $bind);
		}
		return $this->getAdapter()->lastInsertId();
	}

	public function savePostpone($data)
	{
		$escort_id = null;
		$escort_ids = null;
		if ( count($data['e_ids']) > 1 )
		{
			foreach($data['e_ids'] as $e_id)
			{
				$escort_ids[] = $e_id;	
			}
			$escort_ids = implode(',', $escort_ids);
		}		
		elseif ( count($data['e_ids']) == 1 )
			$escort_id = $data['e_ids'][0];
		
		$sql = 'INSERT INTO ebsms_outbox (escort_id, escort_ids, phone_to, phone_from, text, application_id, sender_sales_person, is_spam, status, exact_send_date) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, FROM_UNIXTIME(' . intval($data['exact_send_date']) . '))';
		
		if ( is_array($data['phone_number']) ){
			$i = 0;
			foreach ($data['phone_number'] AS $phone)
			{
				$this->getAdapter()->query($sql, array($data['e_ids'][$i], $escort_ids, $phone, $data['phone_from'], $data['text'], $data['application_id'], $data['sender_sales_person'], $data['is_spam'] ? 1 : 0, $data['status']));
				$i++;
			}
		}
		else{
			$this->getAdapter()->query($sql, array($escort_id, $escort_ids, $data['phone_number'], $data['phone_from'], $data['text'], $data['application_id'], $data['sender_sales_person'], $data['is_spam'] ? 1 : 0, $data['status']));
		}
		return $this->getAdapter()->lastInsertId();
	}
	
	public function delete($ids)
	{
		$sql = "
			INSERT INTO ebsms_trash (type, escort_id, phone_from, phone_to, `date`, text, is_read, application_id) 
				(SELECT " . Model_EBSMS_EBSMS::EBSMS_TYPE_OUTGOING . ", escort_id, phone_from, phone_to, `date`, text, 1, application_id  
			FROM ebsms_outbox
			WHERE id = ?)
		";
		
		foreach ( $ids as $id )
		{
			$this->getAdapter()->query($sql, $id);
		
			parent::remove(self::quote('id = ?', $id));
		}
	}
	
	public function update2EBSMSData($id, $msg_id = null, $status = 0)
	{
		$data = array();
		if ( $msg_id )
			$data = array_merge ($data, array('msg_id_2ebsms' => $msg_id));
		
		if ( $status !== false ) 
			$data = array_merge ($data, array('status_2ebsms' => $status));
		
		$this->getAdapter()->update('ebsms_outbox', $data, $this->getAdapter()->quoteInto('id = ?', $id));
	}
	
	public function getByIds($ids)
	{
		$sql = "SELECT escort_id, phone_to AS phone_number, phone_from, text FROM " .$this->_table. "  WHERE id IN (".implode(',', $ids).")";
		return $this->getAdapter()->fetchAll($sql);
	}
	
	public function saveAndSend($data)
	{
		$msg_id = $this->save($data);
		
		$config = Zend_Registry::get('system_config');
		$ebsms_config = $config['ebsms'];
		$EBSMS_USERKEY = $ebsms_config['userkey'];
		$EBSMS_PASSWORD = $ebsms_config['password'];
		$EBSMS_DeliveryNotificationURL = $ebsms_config['DeliveryNotificationURL'];
		$EBSMS_NonDeliveryNotificationURL = $ebsms_config['NonDeliveryNotificationURL'];
		$EBSMS_BufferedNotificationURL = $ebsms_config['BufferedNotificationURL'];
		
		$ebsms = new Cubix_EBSMS($EBSMS_USERKEY, $EBSMS_PASSWORD);

		$ebsms->setOriginator($data['phone_from']);
		$ebsms->addRecipient($data['phone_number'], $msg_id);
		$ebsms->setBufferedNotificationURL($EBSMS_BufferedNotificationURL);
		$ebsms->setDeliveryNotificationURL($EBSMS_DeliveryNotificationURL);
		$ebsms->setNonDeliveryNotificationURL($EBSMS_NonDeliveryNotificationURL);

		$ebsms->setContent($data['text']);

		return $ebsms->sendEBSMS();
	}		
}