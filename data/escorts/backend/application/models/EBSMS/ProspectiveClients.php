<?php

class Model_EBSMS_ProspectiveClients extends Cubix_Model
{
	protected $_table = 'perspective_clients';

	public function get($id)
	{
		$sql = '
			SELECT pc.* FROM perspective_clients pc
			WHERE pc.id = ?
		';
		
		return parent::_fetchRow($sql, $id);
	}
	
	public function getAll($p, $pp, $sort_field, $sort_dir, $filter, &$count)
	{
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS pc.*, bu.username
			FROM perspective_clients pc
		';
		
		$joins = array(
			// 'INNER JOIN ebsms_phone_number_groups png ON png.id = pn.group_id',
			// 'LEFT JOIN ebsms_outbox so ON so.phone_to = pn.number AND so.status = 1'
			   'INNER JOIN backend_users bu ON bu.id = pc.sales'
		);

		$where = ' WHERE 1 ';
		$having = '';

		if ( $filter['number'] ) {
			$where .= self::quote(' AND pc.parsed_number LIKE ?', $filter['number'] . '%');
		}
		
		if ( $filter['sales'] ) {
			$where .= self::quote(' AND pc.sales = ?', $filter['sales']);
		}

		if ( $filter['matches_ef'] ) {
			$where .= self::quote(' AND pc.matches_ef = ?', $filter['matches_ef']);
		}

		if ( $filter['contacts_count'] && !empty($filter['contacts_count']) ) {
		    $where .= self::quote(' AND pc.contacts_count = ?', $filter['contacts_count']);
        }
		
		if ( $filter['sales_user_id_real'] ) {
			if ( $bu_user->type == 'superadmin' || $bu_user->type == 'admin') {
				$where .= self::quote(' AND sales = ?', $filter['sales_user_id_real']);
			}
		}

		if(strlen($filter['date_from']) && strlen($filter['date_to'])) {
			$where .= self::quote(' AND date_added >= DATE(FROM_UNIXTIME(?))', $filter['date_from']);
			$where .= self::quote(' AND date_added <= DATE(FROM_UNIXTIME(?))', $filter['date_to']);
		}
		else if ( strlen($filter['date_from']) ) {
			$where .= self::quote(' AND date_added >= DATE(FROM_UNIXTIME(?))', $filter['date_from']);
		}
		else if ( strlen($filter['date_to']) ) {
			$where .= self::quote(' AND date_added <= DATE(FROM_UNIXTIME(?))', $filter['date_to']);
		}

		// if ( $filter['contact_state'] == 'contacted' ) {
		// 	$joins[1] = 'INNER JOIN ebsms_outbox so ON so.phone_to = pc.number AND so.status = 1';
		// } elseif ( $filter['contact_state'] == 'not_contacted' ) {
		// 	$joins[1] = 'LEFT JOIN ebsms_outbox so ON so.phone_to = pc.number';
		// 	$having .= ' HAVING contacted_times = 0';
		// } elseif ( $filter['contact_state'] == 'contact_failed' ) {
		// 	$joins[1] = 'INNER JOIN ebsms_outbox so ON so.phone_to = pc.number AND so.status = 2';
		// } elseif ( $filter['contact_state'] == 'contact_pending' ) {
		// 	$joins[1] = 'INNER JOIN ebsms_outbox so ON so.phone_to = pc.number AND so.status = 0';
		// }

		$sql .= implode(' ', $joins);
		$sql .= $where;
		
		$sql .= $having;
		
		$sql .= ' ORDER BY ' . $sort_field . ' ' . $sort_dir;
		
		if ( $p !== false && $pp !== false ) {
			$sql .= ' LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp;
		}
		
		$result = parent::_fetchAll($sql);
		$count = intval($this->getAdapter()->fetchOne('SELECT FOUND_ROWS()'));
		
		return $result;
	}
	
	public function save(Model_EBSMS_ProspectiveClientsItem $item)
	{
		parent::save($item);
	}
	
	public function remove($id)
	{
		parent::getAdapter()->delete($this->_table, parent::quote('id = ?', $id));
	}
	
	public function checkIfExists($number, $id = null)
	{
		$sql = 'SELECT TRUE FROM perspective_clients WHERE parsed_number = ?';
		$bind = array($number);
		if ( $id ) {
			$bind[] = $id;
			$sql .= ' AND id != ?';
		}
		
		return $this->getAdapter()->fetchOne($sql, $bind);
	}
	
	public function checkIfExistsInProject($number)
	{
		$result = array(
			'exists'	=> false
		);
		
		$escort_exists = $this->getAdapter()->fetchOne('SELECT TRUE FROM escort_profiles_v2 WHERE contact_phone_parsed LIKE "%' . $number . '%"');
		if ( $escort_exists ) {
			$result['exists'] = true;
			$result['reason'] = 'Escort with this phone exists.';
			return $result;
		}
		
		$have_in_contacts = $this->getAdapter()->fetchOne('SELECT TRUE FROM perspective_clients WHERE number = ?', $number);
		if ( $have_in_contacts ) {
			$result['exists'] = true;
			$result['reason'] = 'Already in contacts.';
			return $result;
		}
		
		return $result;
	}
	
	
	public function removeExisting()
	{
		 $existing_numbers = $this->getAdapter()->fetchAll('
			SELECT pn.id, pn.number, ep.showname, ep.escort_id
			FROM escort_profiles_v2 ep 
			INNER JOIN perspective_clients pn ON pn.number = ep.contact_phone_parsed 
		');
		 
		 $ids_to_remove = array();
		 foreach($existing_numbers as $en) {
			 $ids_to_remove[] = (string)$en->id;
		 }
		 
		 if ( count($ids_to_remove) ) {
			$this->getAdapter()->query('DELETE FROM perspective_clients WHERE id IN (' . implode(',', $ids_to_remove) . ')');
		 }
		 
		 return $existing_numbers;
	}
	
	public function blacklistNotDelivered()
	{
		$not_delivered = $this->getAdapter()->fetchAll('
			SELECT pn.id, pn.number, so.status
			FROM perspective_clients pn
			INNER JOIN ebsms_outbox so ON so.phone_to = pn.number AND status = 2 AND is_spam = 1
			WHERE pn.is_blacklisted = 0
		');
		
		 $ids_to_blacklist = array();
		 foreach($not_delivered as $nd) {
			 $ids_to_blacklist[] = (string)$nd->id;
		 }
		 
		 if ( count($ids_to_blacklist) ) {
			$this->getAdapter()->query('UPDATE perspective_clients SET is_blacklisted = 1 WHERE id IN (' . implode(',', $ids_to_blacklist) . ')');
		 }
		 
		 return $not_delivered;
	}
	public function get_country_codes(){
		return parent::_fetchAll("SELECT * FROM countries_phone_code ORDER BY title_en ");
	}
	public function get_last_24_added_numbers_count( $sales_id ){
		$sql = "SELECT count(id) as count FROM perspective_clients where date_added >= now() - INTERVAL 24 HOUR AND sales = ? ";
		return parent::_fetchRow( $sql, $sales_id );
	}
	public function get_country_iso_by_code( $iso ){
		$sql = 'SELECT cpc.iso FROM countries_phone_code cpc WHERE cpc.phone_prefix = ?';
		return parent::_fetchRow( $sql, $iso );
	}

	public static function getCommentsHistoryByClientId($client_id, $page = 1, $per_page = 5, &$count = 0)
	{
		$db = Zend_Registry::get('db');

		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				prc.id, prc.comment , prc.date, bu.username
			FROM perspective_clients_responses as prc
			LEFT JOIN backend_users bu ON bu.id = prc.sales_user_id
		
			WHERE client_id = ? ORDER BY prc.date DESC LIMIT ' . (($page - 1) * $per_page) . ', '. $per_page ;
	
		$results = $db->fetchAll($sql, $client_id);
		$count = $db->fetchOne('SELECT FOUND_ROWS()');
		return $results;
	}
	public static function getResponseCommentsHistoryByClientId($client_id, $page = 1, $per_page = 5, &$count = 0)
	{
		$db = Zend_Registry::get('db');
		
		$sql = 'SELECT SQL_CALC_FOUND_ROWS
			ec.id, ec.date, ec.comment ,ec.response , ec.date , bu.username FROM perspective_clients_responses as ec
			LEFT JOIN backend_users bu ON bu.id = ec.sales_user_id
			WHERE ec.escort_id = ? ORDER BY ec.date DESC LIMIT ' . (($page - 1) * $per_page) . ', '. $per_page ;
		$results = $db->fetchAll($sql, $id);

		$count = $db->fetchOne('SELECT FOUND_ROWS()');
		return $results;
	}

	public static function insertProspectiveClientsResponseComment($data)
	{
		$db = Zend_Registry::get('db');
		$db->insert('perspective_clients_responses', $data);

		parent::db()->query('UPDATE perspective_clients SET contacts_count = contacts_count + 1 WHERE id = ?', array($data['client_id']));

	}

	public function checkPhoneInEf($phone)
	{
	   return self::getAdapter()->fetchOne("SELECT true FROM	escort_profiles_v2 WHERE phone_exists = ?", $phone);
	}
}
