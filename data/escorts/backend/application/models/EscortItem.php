<?php

class Model_EscortItem extends Cubix_Model_Item
{
	public function cancelPackages($comment)
	{
		$this->_adapter->beginTransaction();

		try {
			$this->_adapter->query('
				UPDATE order_packages SET status = ?, status_comment = ? WHERE escort_id = ?
			', array(
				Model_Billing_Packages::STATUS_CANCELLED,
				$comment,
				$this->getId()
			));

			$this->_adapter->commit();
		}
		catch (Exception $e) {
			$this->_adapter->rollBack();

			throw $e;
		}
	}

	public function getLangs()
	{
		return $this->_adapter->query('
			SELECT lang_id AS id, level FROM escort_langs
			WHERE escort_id = ?
		', $this->getId())->fetchAll();
	}

	public function hasPendingPackage($app_id)
	{
		$sql = "SELECT TRUE FROM order_packages WHERE escort_id = ? AND application_id = ? AND status = ?";
		
		return $this->_adapter->fetchOne($sql, array($this->getId(), $app_id, Model_Packages::STATUS_PENDING));
	}

	public function vipPackagesCount()
	{
		$sql = "
			SELECT 
				COUNT(op.id) 
			FROM order_packages op 
			INNER JOIN escorts e ON e.id = op.escort_id
			WHERE op.status = ? AND op.package_id IN (21,22,23,24,25,26) AND e.status & 32";

		return $this->_adapter->fetchOne($sql, array(Model_Packages::STATUS_ACTIVE));
	}

	public function processDefaultPackages()
	{
		$applications = Cubix_Application::getAll();
		$active_packages = $this->hasActivePackages();

		foreach ( $applications as $app ) {
			if ( ! in_array($app->id, $active_packages) ) {
				$this->activateDefaultPackage($app->id);
			}
		}
	}

	public function hasActivePackages()
	{
		$applications = $this->_adapter->fetchAll('
			SELECT
				a.id
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN order_packages op ON op.escort_id = e.id
			INNER JOIN packages p ON op.package_id = p.id
			INNER JOIN applications a ON a.id = op.application_id
			/*INNER JOIN countries c ON c.id = a.country_id*/
			WHERE e.id = ? AND op.status = ?
		', array($this->getId(), Model_Billing_Packages::STATUS_ACTIVE));

		$result = array();
		foreach ( $applications as $app ) {
			$result[] = $app->id;
		}

		return $result;
	}

	/**
	 * Requires: id, agency_id, gender, country_id
	 *
	 * @param int $application_id
	 * @return
	 */
	public function activateDefaultPackage($application_id)
	{
		$db = $this->_adapter;
		//$is_international = $this->getIsInternational();
		//$is_super_old = $this->getIsSuperOld();

		try {
			$db->beginTransaction();

			$escort_id = $this->getId();

			if ( $application_id != 2 && $application_id != 1 ) {
				/* --> Get the default package for this application */
				if ( $this->country_id == Cubix_Application::getById($application_id)->country_id ) {
					$type = PROD_AVAILABLE_FOR_DOMESTIC;
				}
				else {
					$type = PROD_AVAILABLE_FOR_INTERNATIONAL;
				}
			}
			else {
				$country_id = $this->country_id;
				$home_city_id = ( isset($this->home_city_id) && $this->home_city_id ) ? $this->home_city_id : false;
				
				if ( $home_city_id ) {
					$m_city = new Cubix_Geography_Cities();
					$home_city = $m_city->get($this->home_city_id);
					$home_country_id = $home_city->country_id;
					$country_id = $home_country_id;
				}
				
				if ( /*! $is_international*/ $country_id == Cubix_Application::getById($application_id)->country_id ) {
					$type = PROD_AVAILABLE_FOR_DOMESTIC;
				}
				else {
					$type = PROD_AVAILABLE_FOR_INTERNATIONAL;
				}


				// Force to assign "Zero Package"
				$type = PROD_AVAILABLE_FOR_ALL;
			}


            $escort_type = null;
            if (Cubix_Application::getId() == APP_ED) {
                $escort_type = Model_EscortV2Item::getEscortTypeByID($escort_id);
            }
			
			if((Cubix_Application::getId() == APP_ED && $this->agency_id && $this->gender == GENDER_FEMALE && $this->country_id != 68) || (Cubix_Application::getId() == APP_EG_CO_UK && $this->agency_id && $this->gender == GENDER_FEMALE && $this->country_id != 216)) {
				if($this->countMonthlyPackageEscorts() < 3) {
					$package = $db->fetchRow('
						SELECT
							p.*
						FROM packages p
						WHERE
							p.application_id = ? AND
							p.available_for = ? AND
							p.is_active = 1 AND
							p.id = ?
					', array($application_id, PROD_AVAILABLE_FOR_ALL, Model_Packages::getMonthlyPackageId()));
				} else {
					$package = $db->fetchRow('
						SELECT
							p.*
						FROM packages p
						WHERE
							p.application_id = ? AND
							p.available_for = ? AND
							p.is_active = 1 AND
							p.id = ?
					', array($application_id, PROD_AVAILABLE_FOR_ALL, Model_Packages::getMinimumPackageId()));
				}
			} else {
				$package = $db->fetchRow('
					SELECT
						p.*
					FROM packages p
					WHERE
						p.application_id = ? AND
						p.available_for = ? AND
						p.is_default = 1 AND
						p.is_active = 1
				', array($application_id, $type));


				if ( ! $package ) {
					$package = $db->fetchRow('
						SELECT
							p.*
						FROM packages p
						WHERE
							p.application_id = ? AND
							p.available_for = ? AND
							p.is_default = 1 AND
							p.is_active = 1
					', array($application_id, PROD_AVAILABLE_FOR_ALL));
				}
			}		

	
			if ( ! $package ) {
				return;
			}
		
			$date_activated = new Zend_Db_Expr('NOW()');
			
			$expiration_date = null;
			if ( $application_id == APP_BL) {
				//Stupid rule by katie. Task: BEN-152
				return;
				/*$has_p_package = $db->fetchRow('
					SELECT * 
					FROM order_packages
					WHERE order_id IS NOT NULL AND escort_id = ?
				', array($escort_id));
				
				if ( $has_p_package ) return;*/
				
				//Setting expiration date for default package(1 year) only for beneluxxx
				$expiration_date = new Zend_Db_Expr('NOW() + INTERVAL 1 YEAR');
			}
			
			$db->insert('order_packages', array(
				'order_id' => null,
				'escort_id' => $escort_id,
				'package_id' => $package->id,
				'application_id' => $application_id,
				'base_period' => null,
				'period' => null,
				'date_activated' => $date_activated,
				'expiration_date'	=> $expiration_date,
				'status' => Model_Billing_Packages::STATUS_ACTIVE,
				'activation_type' => null,
				'base_price' => 0,
				'price' => 0
			));
			

			$order_package_id = $db->lastInsertId();

			$model = new Model_Packages();
			$products = $model->getProducts($package->id, $application_id, $this->agency_id, $this->gender, false, null, $escort_type);

			foreach ( $products as $product ) {
				$product_data = array(
					'order_package_id' => $order_package_id,
					'product_id' => $product->id,
					'is_optional' => 0,
					'price' => $product->price
				);

				$db->insert('order_package_products', $product_data);
			}

			$db->commit();

			Zend_Registry::get('BillingHooker')->notify('package_activated', array($order_package_id, $escort_id, $date_activated));
		}
		catch ( Exception $e ) {
			$db->rollBack();

			throw $e;
		}
	}

	public function countMonthlyPackageEscorts()
	{
		$result = $this->_adapter->fetchRow('
			SELECT
				COUNT(*) as count
			FROM escorts e
			INNER JOIN order_packages op ON op.escort_id = e.id
			INNER JOIN packages p ON op.package_id = p.id
			WHERE e.agency_id = ? AND op.status = ? AND p.id = ?
		', array($this->agency_id, Model_Packages::STATUS_ACTIVE, Model_Packages::getMonthlyPackageId()));


		return $result->count;
	}

	public function getTotalOrdersPrice($is_paid = false)
	{
		$bind = array($this->getId());
		if ( $is_paid ) {
			$bind = array(
				$this->getId(),
				Model_Billing_Orders::STATUS_PAID
			);
			$where = ' AND o.status = ? ';
		}

		$sql = "
			SELECT				
				SUM(o.price) AS total_orders_price
			FROM orders o
			INNER JOIN users u ON u.id = o.user_id
			INNER JOIN escorts e ON u.id = e.user_id
			WHERE e.id = ? {$where}
		";

		return $this->_adapter->query($sql, $bind)->fetchColumn();
	}

	public function getActivePackages()
	{
		$sql = '
			SELECT
				op.id, op.order_id, op.escort_id, op.package_id,
				op.period, UNIX_TIMESTAMP(op.date_activated) AS date_activated, UNIX_TIMESTAMP(op.expiration_date) AS expiration_date,
				op.price,
				p.name AS package_name,
				a.title AS application_title,
				c.iso AS app_iso
				/*bu.username AS sales_person*/
			FROM escorts e
			INNER JOIN order_packages op ON op.escort_id = e.id
			/*INNER JOIN orders o ON o.id = op.order_id*/
			INNER JOIN packages p ON p.id = op.package_id
			INNER JOIN applications a ON a.id = op.application_id
			/*INNER JOIN backend_users bu ON bu.id = o.backend_user_id*/
			INNER JOIN countries c ON c.id = a.country_id
			WHERE e.id = ? AND op.status = ?
		';

		return $this->_adapter->query($sql, array($this->getId(), Model_Packages::STATUS_ACTIVE))->fetchAll();
	}
	
	public function getCities()
	{
		return $this->_adapter->query('
			SELECT c.id, c.' . Cubix_I18n::getTblField('title') . ' AS title FROM cities c
			INNER JOIN escort_cities ec ON ec.city_id = c.id
			WHERE ec.escort_id = ?
		', $this->getId())->fetchAll();
	}
	
	public function getWorkTimes()
	{
		$data = $this->_adapter->query('
			SELECT day_index, time_from, time_to FROM escort_working_times
			WHERE escort_id = ?
		', $this->getId())->fetchAll();
		
		$result = array();
		foreach ( $data as $row ) {
			$result[$row->day_index] = array(
				'from' => $row->time_from,
				'to' => $row->time_to
			);
		}
		
		return $result;
	}
	
	public function getRates()
	{
		return $this->_adapter->query('
			SELECT availability, time, time_unit, price, currency_id FROM escort_rates
			WHERE escort_id = ?
		', $this->getId())->fetchAll();
	}
	
	public function getPhotos($sort_field = null, $sort_dir = null)
	{
		if ( ! $sort_field || ! $sort_dir )
		{
			$sort_field = "ep.ordering";
			$sort_dir = " ASC";
		}
		
		$sort = $sort_field . ' ' . $sort_dir;
				
		$photos = $this->_adapter->query('
			SELECT u.application_id, ep.* FROM escort_photos ep
			INNER JOIN escorts e ON e.id = ep.escort_id
			INNER JOIN users u ON u.id = e.user_id
			WHERE ep.escort_id = ? ORDER BY ' . $sort . '
		', $this->getId())->fetchAll();
		
		foreach ($photos as $i => $photo) {
			$photos[$i] = new Model_Escort_PhotoItem($photo);
		}
		
		return $photos;
	}

	public function getPhotosCount()
	{
		$countSql = "
			SELECT COUNT(ep.id) as count
			FROM escort_photos ep
			/*INNER JOIN escorts e ON e.id = ep.escort_id*/
			WHERE ep.escort_id = ? /*{$where}*/
		";

		$count = $this->_adapter->query($countSql, $this->getId())->fetch()->count;

		return $count;
	}
	
	public function getMainPhoto()
	{
		return new Model_Escort_PhotoItem(array(
			'application_id' => $this->application_id,
			'escort_id' => $this->id,
			'hash' => $this->photo_hash,
			'ext' => $this->photo_ext,
			'photo_status' => $this->photo_status
		));
	}
	
	public function getNewPhotoCount($type)
	{
		$photo = $this->_adapter->query("
			SELECT COUNT(ep.id) AS count
			FROM escort_photos ep
			INNER JOIN escorts e ON e.id = ep.escort_id
			INNER JOIN users u ON u.id = e.user_id
			WHERE ep.escort_id = ? AND ep.status = '" . Model_Escort_Photos::NOT_VERIFIED . "' AND ep.type IN (" . $type . ")
		", array($this->getId()/*, $type*/))->fetch();
		
		return $photo->count;
	}
	
	public function getPhoto($index)
	{
		$photos = $this->_adapter->query('
			SELECT * FROM escort_photos 
			WHERE escort_id = ? AND ordering = ?
		', array($this->getId(), $index))->fetch();
		
		return new Model_Escort_PhotoItem($photo);
	}

	public function getUser()
	{
		$user_id = $this->_adapter->fetchOne('SELECT user_id FROM escorts WHERE id = ?', $this->getId());
		
		$item = new Model_UserItem();
		$item->setId($user_id);

		return $item;
	}

	public function getReviewsCount()
	{
		return self::getAdapter()->fetchOne('SELECT reviews FROM escorts WHERE id = ?', $this->getId());
	}
}
