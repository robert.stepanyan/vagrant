<?php

class Model_TopComments extends Cubix_Model
{
    public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS 
				f.id, e.showname, u.username, f.comment, f.is_hidden, f.is_edited, 
				UNIX_TIMESTAMP(f.comment_date) AS comment_date, f.user_id, f.escort_id, 
				f.comment_status, f.type 
			FROM favorites f
			INNER JOIN escorts e ON e.id = f.escort_id
			INNER JOIN users u ON u.id = f.user_id
			WHERE LENGTH(f.comment) > 0 
		';

		$countSql = 'SELECT FOUND_ROWS()';
		
		if ( strlen($filter['showname']) ) {
			$sql .= self::quote('AND e.showname LIKE ?', '%' . $filter['showname'] . '%');
		}
		
		if ( strlen($filter['escort_id']) ) {
			$sql .= self::quote('AND f.escort_id = ?', $filter['escort_id']);
		}
		
		if ( strlen($filter['username']) ) {
			$sql .= self::quote('AND u.username LIKE ?', '%' . $filter['username'] . '%');
		}
		
        if ( strlen($filter['status']) ) {
			$sql .= self::quote('AND f.comment_status = ?',  $filter['status']);
		}
		
        if ( strlen($filter['type']) ) {
			$sql .= self::quote('AND f.type = ?',  $filter['type']);
		}

		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$ret = parent::_fetchAll($sql);
		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return $ret;
	}
	
	public function get($id)
	{
		$sql = '
			SELECT 
				f.id, e.showname, u.username, f.comment, f.is_hidden, f.is_edited, 
				UNIX_TIMESTAMP(f.comment_date) AS comment_date, f.user_id, f.escort_id, 
				f.type, f.comment_status
			FROM favorites f
			INNER JOIN escorts e ON e.id = f.escort_id
			INNER JOIN users u ON u.id = f.user_id
			WHERE f.id = ? 
		';
		
		return self::_fetchRow($sql, array($id));
	}

	public function update($id, $status)
	{
		parent::getAdapter()->update('favorites', array(
			'comment_status' => $status,
			'is_edited' => 0
		), parent::getAdapter()->quoteInto('id = ?', $id));
	}
}
