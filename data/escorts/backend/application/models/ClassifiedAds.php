<?php
class Model_ClassifiedAds extends Cubix_Model
{	
	const AD_STATUS_PENDING  = 1;
	const AD_STATUS_ACTIVE   = 2;
	const AD_STATUS_EXPIRED  = 3;
	
	const AD_TRANSFER_STATUS_PENDING    = 1;
	const AD_TRANSFER_STATUS_CONFIRMED  = 2;
	const AD_TRANSFER_STATUS_REJECTED   = 3;

	const AD_TRANSFER_GATEWAY_EPG = 1;
	const AD_TRANSFER_GATEWAY_POSTFINANCE = 2;
	const AD_TRANSFER_GATEWAY_IVR = 3;
	const AD_TRANSFER_GATEWAY_TWISPAY = 4;

	
	public function getAllForSeo($value)
	{
		$sql = '
			SELECT
				ca.id, ca.title
 			FROM classified_ads ca
			/*INNER JOIN cities c ON c.id = ca.city_id*/
			WHERE 1 AND ca.title LIKE "%' . $value . '%"
		';
		
		$sql .= '
			GROUP BY ca.id
		';

		$items = parent::_fetchAll($sql);
		
		return $items;
	}
	
	public function getPendingAds()
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				ca.id, ca.title 
 			FROM classified_ads ca
 			WHERE ca.status = 1 AND ca.is_disabled = 0
		';
		$items = $this->getAdapter()->fetchAll($sql);
		$count = $this->getAdapter()->fetchOne('SELECT FOUND_ROWS()');
		return array('items' => $items, 'count' => $count);
	}

	public function getAllAds($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
	    $fields = '';
		if(Cubix_Application::getId() == APP_A6){
			$joins = 'INNER JOIN cantons ct ON ct.id = c.city_id LEFT JOIN users u ON u.id = ca.user_id	LEFT JOIN escorts e ON e.user_id = u.id
			 LEFT JOIN agencies ag ON ag.user_id = u.id LEFT JOIN members m ON m.user_id = u.id ';
			$fields = ', ca.phone_escort_id ';
            $fields .= ', CASE
			WHEN u.user_type = "agency" THEN CONCAT(u.user_type,"-",ag.id)
			WHEN u.user_type = "escort" THEN IF(e.agency_id IS NOT NULL,CONCAT("agency","-",e.agency_id),CONCAT(u.user_type,"-",e.id))
			ELSE CONCAT(u.user_type,"-",m.id)
	        END AS user_data ';
		} else{
			$joins = 'INNER JOIN cities ct ON ct.id = c.city_id';
		}

		if(Cubix_Application::getId() == APP_ED){
			$fields = ', ca.is_ip_blocked ';
		} 

		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				ca.id, ca.status, ca.category_id, ca.phone, ca.email, ca.title, ca.text, ca.period,
				UNIX_TIMESTAMP(ca.creation_date) AS creation_date, UNIX_TIMESTAMP(ca.approvation_date) AS approvation_date,
				UNIX_TIMESTAMP(ca.expiration_date) AS expiration_date,
				ca.is_premium, UNIX_TIMESTAMP(ca.premium_activation_date) AS premium_activation_date, 
				UNIX_TIMESTAMP(ca.premium_expiration_date) AS premium_expiration_date, ca.is_disabled, ca.ip '.$fields.'
 			FROM classified_ads ca
			INNER JOIN classified_ads_cities c ON c.ad_id = ca.id '
			. $joins .
			' WHERE 1
		';
		/*$countSql = '
			SELECT COUNT(DISTINCT(ca.id))
			FROM classified_ads ca
			INNER JOIN classified_ads_cities c ON c.ad_id = ca.id
			WHERE 1
		';*/
		
		$where = '';
		
		if(Cubix_Application::getId() == APP_A6){
			unset($filter['ct.country_id']);
		}		
		if ( strlen($filter['application_id']) ) {
			$where .= self::quote(' AND ca.application_id = ? ', $filter['application_id']);
		}
		
		if ( strlen($filter['ca.id']) ) {
			$where .= self::quote(' AND ca.id = ? ', $filter['ca.id']);
		}

		if ( strlen($filter['ct.city_id']) ) {
			$where .= self::quote(' AND ct.id = ? ', $filter['ct.city_id']);
		}
		if ( strlen($filter['ct.country_id']) ) {
			$where .= self::quote(' AND ct.country_id = ? ', $filter['ct.country_id']);
		}
		
		if ( strlen($filter['ca.title']) ) {
			$where .= self::quote(' AND ca.title LIKE ?', $filter['ca.title'] . '%');
		}
		
		if ( strlen($filter['ca.text']) ) {
			$where .= self::quote(' AND ca.text LIKE ?', '%' . $filter['ca.text'] . '%');
		}
		if ( strlen($filter['ca.phone']) ) {
			if(Cubix_Application::getId() == APP_A6){
				$where .= self::quote(' AND ca.phone LIKE ?', '%'.$filter['ca.phone'] . '%');

			} else {
				$where .= self::quote(' AND ca.phone LIKE ?', $filter['ca.phone'] . '%');
			}
		}
		if ( strlen($filter['ca.email']) ) {
			$where .= self::quote(' AND ca.email LIKE ?', $filter['ca.email'] . '%');
		}
		
		if ( strlen($filter['ca.ip']) ) {
			$where .= self::quote(' AND ca.ip LIKE ?', $filter['ca.ip'] . '%');
		}
		
		if ( strlen($filter['ca.status']) ) {
			if($filter['ca.status'] == -99){
				$where .= self::quote(' AND is_disabled = ? ', 1);
			}
			elseif($filter['ca.status'] == -98){
				$where .= self::quote(' AND is_ip_blocked = ? ', 1);
			}
			else{
				$where .= self::quote(' AND ca.status = ? AND is_disabled <> 1 ', $filter['ca.status']);
			}
			
		}
		
		if ( strlen($filter['ca.category_id']) ) {
			$where .= self::quote(' AND ca.category_id = ? ', $filter['ca.category_id']);
		}
		
		if ( strlen($filter['ca.period']) ) {
			$where .= self::quote(' AND ca.period = ? ', $filter['ca.period']);
		}
		
		if ( strlen($filter['ca.city_id']) ) {
			$where .= self::quote(' AND c.city_id = ? ', $filter['ca.city_id']);
		}
		
		if ( strlen($filter['ca.is_premium']) ) {
			$where .= self::quote(' AND ca.is_premium = ? ', $filter['ca.is_premium']);
		}

		if(Cubix_Application::getId() == APP_A6){
			if ( strlen($filter['ca.whatsapp_available']) ) {
				$where .= self::quote(' AND ca.whatsapp_available = ? ', $filter['ca.whatsapp_available']);
			}
			if ( strlen($filter['ca.sms_available']) ) {
				$where .= self::quote(' AND ca.sms_available = ? ', $filter['ca.sms_available']);
			}
		}

		
		
		// <editor-fold defaultstate="collapsed" desc="Filtration by Date">
		if ( isset($filter['filter_by']) && $filter['filter_by'] ) {
			
			if ( $filter['filter_by'] == 'approvation_date' ) {
				$date_field = 'ca.approvation_date';
			}
			elseif ( $filter['filter_by'] == 'expiration_date' ) {
				$date_field = 'ca.expiration_date';
			}
			elseif ( $filter['filter_by'] == 'premium_activation_date' ) {
				$date_field = 'ca.premium_activation_date';
			}
			elseif ( $filter['filter_by'] == 'premium_expiration_date' ) {
				$date_field = 'ca.premium_expiration_date';
			}

			if ( $filter['date_from'] && $filter['date_to'] ) {
				if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) ) {
					$where .= self::quote(' AND DATE(' . $date_field . ') = DATE(FROM_UNIXTIME(?))', $filter['date_from']);
				}
			}

			if ( $filter['date_from'] ) {
				$where .= self::quote(' AND DATE(' . $date_field . ') >= DATE(FROM_UNIXTIME(?))', $filter['date_from']);
			}

			if ( $filter['date_to'] ) {
				$where .= self::quote(' AND DATE(' . $date_field . ') <= DATE(FROM_UNIXTIME(?))', $filter['date_to']);
			}
		}

		unset($filter['filter_by']);
		unset($filter['date_from']);
		unset($filter['date_to']);
		// </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Filtration by Phone Escort Id">
        if (Cubix_Application::getId() == APP_A6){
            if (!empty($filter['by_phone_escort_id']) && $filter['by_phone_escort_id'] == 'empty'){
                $where .= ' AND phone_escort_id IS NULL ';
            }elseif ($filter['by_phone_escort_id'] == 'Multiple' || is_numeric($filter['by_phone_escort_id'])){
                $where .= self::quote(' AND phone_escort_id = ? ', $filter['by_phone_escort_id']);
            }elseif (isset($filter['by_phone_escort_id']) && $filter['by_phone_escort_id'] == ''){
                $where .= ' AND  phone_escort_id = "" ';
            }
        }
        // </editor-fold>

        	$sql .= $where;
		//$countSql .= $where;
		
		$sql .= '
			GROUP BY ca.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		$this->getAdapter()->query('SET NAMES `utf8`');
		$items = $this->getAdapter()->fetchAll($sql);
		$count = $this->getAdapter()->fetchOne('SELECT FOUND_ROWS()');
		
		return $items;
	}
	
	public function saveAd($data, $photos, $id = null)
	{
		//print_r($photos);
		$edit = false;		
		if ( $id ) {
			$edit = true;
		}
		$cities = $data['cities'];
		unset($data['cities']);
		
		$data = array_merge($data, array('application_id' => Cubix_Application::getId()));
		
		if ( $edit ) {
			parent::getAdapter()->query('DELETE FROM classified_ads_images WHERE ad_id = ?', $id);
			parent::getAdapter()->query('DELETE FROM classified_ads_cities WHERE ad_id = ?', $id);
			
			parent::getAdapter()->update('classified_ads', $data, parent::quote('id = ?', $id));
		} else {
			parent::getAdapter()->insert('classified_ads', $data);
			$id = parent::getAdapter()->lastInsertId();
		}
		
		foreach ( $cities as $city ) {
			parent::getAdapter()->insert('classified_ads_cities', array('ad_id' => $id, 'city_id' => $city));
		}
		
		if ( count($photos) ) {
			$k = 1;
			foreach($photos as $photo) {
				if ( $k > 8 ) break;
				parent::getAdapter()->insert('classified_ads_images', array('ad_id' => $id, 'image_id' => $photo['image_id']));
				$k++;
			}
		}
		
		return $id;
	}
	
	public function getAdById($id)
	{
//			INNER JOIN escort_photos ep ON i.hash = ep.hash  ep.escort_id,
		$sql = '
			SELECT
				ca.*
			FROM classified_ads ca
			WHERE id = ?
		';
		$ad = parent::getAdapter()->fetchRow($sql, $id);
		
		$p_sql = '
			SELECT
				cai.args, i.id AS image_id, i.hash, i.ext, i.is_portrait, i.width, i.height, ' . Cubix_Application::getId() . ' AS application_id
			FROM classified_ads_images cai
			INNER JOIN images i ON i.id = cai.image_id
			WHERE cai.ad_id = ?
		';
		$photos = parent::getAdapter()->fetchALl($p_sql, $id);

        /*--------------- MINE Karo ---------------*/
        foreach ($photos as $i => $photo) {
            $photos[$i] = new Model_Escort_PhotoItem($photo);
        }
        /*--------------- /MINE Karo ---------------*/
		
		if(Cubix_Application::getId() == APP_A6){//APP_A6
			$c_sql = '
				SELECT
					c.ad_id, c.city_id, ct.title_en AS title
				FROM classified_ads_cities c
				INNER JOIN cantons ct ON ct.id = c.city_id
				WHERE c.ad_id = ?
			';
		}
		else{

			$c_sql = '
				SELECT
					c.ad_id, c.city_id, ct.title_en AS title
				FROM classified_ads_cities c
				INNER JOIN cities ct ON ct.id = c.city_id
				WHERE c.ad_id = ?
			';
		}
		$cities = parent::getAdapter()->fetchALl($c_sql, $id);
		
		return array('data' => $ad, 'photos' => $photos, 'cities' => $cities);
	}
	
	public function removeAd($id)
	{
		parent::getAdapter()->delete('classified_ads', parent::quote('id = ?', $id));
		parent::getAdapter()->delete('classified_ads_images', parent::quote('ad_id = ?', $id));
		parent::getAdapter()->delete('classified_ads_cities', parent::quote('ad_id = ?', $id));
	}
	
	public function removeAdImages($image_id)
	{
		parent::getAdapter()->delete('images', parent::quote('id = ?', $image_id));
		parent::getAdapter()->delete('classified_ads_images', parent::quote('image_id = ?', $image_id));
	}
	
	
	/* PACKAGE */
	public function removePackage($id)
	{
		parent::getAdapter()->delete('classified_ads_packages', parent::quote('id = ?', $id));
	}
	
	public function savePackage($data, $id = null)
	{
		$edit = false;		
		if ( $id ) {
			$edit = true;
		}
		
		$data = array_merge($data, array('application_id' => Cubix_Application::getId()));
		
		if ( $edit ) {
			parent::getAdapter()->update('classified_ads_packages', $data, parent::quote('id = ?', $id));
		} else {
			parent::getAdapter()->insert('classified_ads_packages', $data);
		}
	}
	
	public function getPackageById($id)
	{
		$sql = '
			SELECT
				p.*
			FROM classified_ads_packages p
			WHERE id = ?
		';
		
		return parent::getAdapter()->fetchRow($sql, $id);
	}
	
	public function getPackagesForSelectBox()
	{
		$sql = '
			SELECT
				p.*
			FROM classified_ads_packages p
		';
		
		return parent::getAdapter()->fetchAll($sql);
	}
	
	public function getAllPackages($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT
				p.*
			FROM classified_ads_packages p
			WHERE 1
		';
		
		$countSql = '
			SELECT COUNT(DISTINCT(p.id))
			FROM classified_ads_packages p
			WHERE 1
		';
		
		$where = '';
				
		if ( strlen($filter['application_id']) ) {
			$where .= self::quote('AND p.application_id = ?', $filter['application_id']);
		}

		/*if ( strlen($filter['username']) ) {
			$where .= self::quote('AND u.username LIKE ?', $filter['username'] . '%');
		}
				
		if ( $filter['status'] ) {
			$where .= self::quote('AND c.status = ?', $filter['status']);
		}*/
				
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			GROUP BY p.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$count = intval($this->getAdapter()->fetchOne($countSql));

		return parent::_fetchAll($sql);
	}
	/* PACKAGE */
	
	public function makePremium($ad_id, $period)
	{
		$data = array(
			'is_premium' => 1,
			'premium_activation_date' => new Zend_Db_Expr('NOW()'),
			'premium_expiration_date' => new Zend_Db_Expr('DATE_ADD(NOW(), INTERVAL ' . $period . ' DAY)'),
		);
		if (Cubix_Application::getId() == APP_A6) {
		    if($period > 7) {
                $data['expiration_date'] = new Zend_Db_Expr('DATE_ADD(NOW(), INTERVAL ' . $period . ' DAY)');
            }else{
                $data['expiration_date'] =  new Zend_Db_Expr('DATE_ADD(NOW(), INTERVAL 7 DAY)');
            }
		}
		parent::getAdapter()->update('classified_ads', $data, parent::quote('id = ?', $ad_id));
		if (Cubix_Application::getId() == APP_A6){
			$m_updates = new Model_ClassifiedAdsUpdates();
			$m_updates->addEvent($ad_id, Model_ClassifiedAdsUpdates::EVENT_TYPE_UPDATED);
		}
		return true;
	}
	
	public function addTransfer($data)
	{
		parent::getAdapter()->insert('classified_ads_transfers', $data);
		
		return true;
	}
	
	public function getAllTransfers($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT
				cat.id, cat.first_name, cat.last_name, cat.status, cat.transaction_id,
				cat.amount, cat.is_paid, bu.username AS sales_username, CONCAT(cat.first_name, " ", cat.last_name) AS payer_name,
				
				UNIX_TIMESTAMP(cat.date_transfered) AS date_transfered, UNIX_TIMESTAMP(cat.date_confirmed) AS date_confirmed,
				cat.package_name, cat.package_price, cat.package_period,
				
				ca.id AS ad_id
 			FROM classified_ads_transfers cat
			INNER JOIN classified_ads ca ON ca.id = cat.ad_id
			LEFT JOIN backend_users bu ON bu.id = cat.backend_user_id
			WHERE 1
		';
		
		$countSql = '
			SELECT COUNT(DISTINCT(cat.id))
			FROM classified_ads_transfers cat
			INNER JOIN classified_ads ca ON ca.id = cat.ad_id
			WHERE 1
		';
		
		$where = '';
				
		if ( strlen($filter['application_id']) ) {
			$where .= self::quote('AND cat.application_id = ?', $filter['application_id']);
		}
		
		if ( strlen($filter['cat.id']) ) {
			$where .= self::quote('AND cat.id = ?', $filter['cat.id']);
		}
		
		if ( strlen($filter['cat.ad_id']) ) {
			$where .= self::quote('AND cat.ad_id = ?', $filter['cat.ad_id']);
		}
		
		if ( strlen($filter['cat.first_name']) ) {
			$where .= self::quote(' AND cat.first_name LIKE ?', $filter['cat.first_name'] . '%');
		}
		
		if ( strlen($filter['cat.last_name']) ) {
			$where .= self::quote(' AND cat.last_name LIKE ?', $filter['cat.last_name'] . '%');
		}
		
		if ( strlen($filter['cat.transaction_id']) ) {
			$where .= self::quote('AND cat.transaction_id = ?', $filter['cat.transaction_id']);
		}
		
		if ( strlen($filter['cat.amount']) ) {
			$where .= self::quote('AND cat.amount = ?', $filter['cat.amount']);
		}
		
		if ( strlen($filter['cat.status']) ) {
			$where .= self::quote('AND cat.status = ?', $filter['cat.status']);
		}
		
		if ( strlen($filter['cat.is_paid']) ) {
			$where .= self::quote('AND cat.is_paid = ?', $filter['cat.is_paid']);
		}
		
		// <editor-fold defaultstate="collapsed" desc="Filtration by Date">
		if ( isset($filter['filter_by']) && $filter['filter_by'] ) {
			
			if ( $filter['filter_by'] == 'date_confirmed' ) {
				$date_field = 'cat.date_confirmed';
			}
			elseif ( $filter['filter_by'] == 'transfer_date' ) {
				$date_field = 'cat.transfer_date';
			}

			if ( $filter['date_from'] && $filter['date_to'] ) {
				if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) ) {
					$where .= self::quote(' AND DATE(' . $date_field . ') = DATE(FROM_UNIXTIME(?))', $filter['date_from']);
				}
			}

			if ( $filter['date_from'] ) {
				$where .= self::quote(' AND DATE(' . $date_field . ') >= DATE(FROM_UNIXTIME(?))', $filter['date_from']);
			}

			if ( $filter['date_to'] ) {
				$where .= self::quote(' AND DATE(' . $date_field . ') <= DATE(FROM_UNIXTIME(?))', $filter['date_to']);
			}
		}

		unset($filter['filter_by']);
		unset($filter['date_from']);
		unset($filter['date_to']);
		// </editor-fold>
				
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			GROUP BY cat.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$count = intval($this->getAdapter()->fetchOne($countSql));

		$items = parent::_fetchAll($sql);
		
		return $items;
	}
	
	public function rejectTransfer($id)
	{
		parent::getAdapter()->update('classified_ads_transfers', array('status' => self::AD_TRANSFER_STATUS_REJECTED), parent::quote('id = ?', $id));
		$ad_id = parent::getAdapter()->fetchOne('SELECT ad_id FROM classified_ads_transfers WHERE id = ?', $id);
		parent::getAdapter()->update('classified_ads', array('is_premium' => null, 'premium_activation_date' => null, 'premium_expiration_date' => null), parent::quote('id = ?', $ad_id));
	}
	
	public function getTransferById($id)
	{
		$sql = '
			SELECT
				cat.*
			FROM classified_ads_transfers cat
			WHERE cat.id = ?
		';
		
		return parent::getAdapter()->fetchRow($sql, $id);
	}
	
	public function editTransfer($data, $id)
	{
		parent::getAdapter()->update('classified_ads_transfers', $data, parent::quote('id = ?', $id));		
	}
	
	public function confirmTransfer($data, $id)
	{
		$data['status'] = self::AD_TRANSFER_STATUS_CONFIRMED;
		parent::getAdapter()->update('classified_ads_transfers', $data, parent::quote('id = ?', $id));
		
		$ad_data = parent::getAdapter()->fetchRow('SELECT ad_id, package_period FROM classified_ads_transfers WHERE id = ?', $id);
		
		$this->makePremium($ad_data->ad_id, $ad_data->package_period);
	}
	
	public function isTransactionIdExist($transaction_id, $tr_id = null)
	{
		if ( $tr_id ) {
			return parent::getAdapter()->fetchOne('SELECT TRUE FROM classified_ads_transfers WHERE transaction_id = ? AND id <> ?', array($transaction_id, $tr_id));
		} else {
			return parent::getAdapter()->fetchOne('SELECT TRUE FROM classified_ads_transfers WHERE transaction_id = ?', $transaction_id);
		}
				
	}

    public function getPhoto( $id ){

        return parent::getAdapter()->fetchRow('
			SELECT cai.*, i.ext, i.id, i.hash, i.width, i.is_portrait, i.height FROM classified_ads_images cai
			INNER JOIN images i ON i.id = cai.image_id
			WHERE cai.image_id = ?
		', $id);
    }

    public function setCropArgs( $args, $image_id ){
        $args = serialize($args);

        self::getAdapter()->query('UPDATE classified_ads_images SET args = ? WHERE image_id = ?', array( $args, $image_id ));

        return true;
    }

	public function getAllFeedbacks($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS id, name, email, to_addr, message, ca_id, UNIX_TIMESTAMP(date) as date, flag, status, ip, city
			FROM classified_ads_feedbacks
			WHERE 1
		';

		$countSql = 'SELECT FOUND_ROWS()';

		if ( strlen($filter['ca_id']) ) {
			$sql .= self::quote('AND ca_id = ?', $filter['ca_id']);
		}

		if ( strlen($filter['email']) ) {
			$sql .= self::quote('AND email LIKE ?', '%' . $filter['email'] . '%');
		}

		if ( strlen($filter['to_addr']) ) {
			$sql .= self::quote('AND to_addr LIKE ?', '%' . $filter['to_addr'] . '%');
		}

		if ( strlen($filter['flag']) ) {
			$sql .= self::quote('AND flag = ?',  $filter['flag']);
		}

		if ( strlen($filter['status']) ) {
			$sql .= self::quote('AND status = ?',  $filter['status']);
		}

		if ( strlen($filter['ip']) ) {
			$sql .= self::quote('AND ip LIKE ?', $filter['ip'] . '%');
		}

		if ( strlen($filter['city']) ) {
			$sql .= self::quote('AND city LIKE ?', '%' . $filter['city'] . '%');
		}

		$sql .= '
			GROUP BY id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$ret = parent::_fetchAll($sql);
		$count = intval($this->getAdapter()->fetchOne($countSql));

		return $ret;
	}

	public function getFeedback($id)
	{
		return self::_fetchRow('
			SELECT
			f.*, ca.title
			FROM classified_ads_feedbacks f
			LEFT JOIN classified_ads ca ON ca.id = f.ca_id
			WHERE f.id = ?
		', array($id));
	}

	public function updateFeedback($data)
	{
		parent::getAdapter()->update('classified_ads_feedbacks', $data, parent::quote('id = ?', $data['id']));
	}

}
