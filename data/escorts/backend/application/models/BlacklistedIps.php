<?php

class Model_BlacklistedIps extends Cubix_Model
{

    public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT *
			FROM blacklisted_ips
			WHERE 1
		';

		$countSql = '
			SELECT COUNT(DISTINCT(id))
			FROM blacklisted_ips
			WHERE 1
		';

		if ( strlen($filter['user_agent']) ) {
			$sql .= self::quote('AND user_agent LIKE ?', '%' . $filter['user_agent'] . '%');
			$countSql .= self::quote('AND user_agent LIKE ?', '%' . $filter['user_agent'] . '%');
		}


		$sql .= '
			GROUP BY id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$countSql .= '';

		$count = intval($this->getAdapter()->fetchOne($countSql));
		return parent::_fetchAll($sql);
	}

	public function get($id)
	{
		return self::_fetchRow('
			SELECT * FROM blacklisted_ips WHERE id = ?
		', array($id));
	}

    

    public function save($data,$id = null){
        if ( ! is_null($id) ) {
			self::getAdapter()->update('blacklisted_ips', $data, self::quote('id = ?', $id));
		}
		else {
			self::getAdapter()->insert('blacklisted_ips', $data);
		}
    }

    public function remove($id = array()){
        self::getAdapter()->delete('blacklisted_ips', self::getAdapter()->quoteInto('id = ?', $id));
    }


    public function checkWords($text){
        $sql = '
			SELECT id, user_agent
			FROM blacklisted_words
			WHERE 1';

        $wordList = parent::_fetchAll($sql);

        if( count($wordList) > 0 ){
            foreach ( $wordList as $word ){
                $pattern = '/^'.$word.'/';
                preg_match($pattern, $text, $matches, PREG_OFFSET_CAPTURE);
                var_dump($matches);
                exit;
            }
            
        }
        
    }

}
