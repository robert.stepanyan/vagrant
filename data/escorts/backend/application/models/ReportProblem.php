<?php
class Model_ReportProblem extends Cubix_Model
{
	protected $_table = 'report_problem';
	protected $_itemClass = 'Model_ReportProblem';

	public function get($id)
	{
		if(in_array(Cubix_Application::getId(), array(APP_ED) )){
			$sql = '
			SELECT
				rp.photo_link ,rp.report_type ,rp.id , rp.name , rp.email , rp.problem, rp.report , UNIX_TIMESTAMP(rp.date) as date, e.showname, rp.status
			FROM '.$this->_table.' rp 
			LEFT JOIN escorts e ON e.id = rp.escort_id	
			WHERE rp.id = ?
		';
		}else{
			$sql = '
			SELECT
				rp.id , rp.name , rp.email , rp.problem, rp.report , UNIX_TIMESTAMP(rp.date) as date, e.showname, rp.status
			FROM '.$this->_table.' rp 
			LEFT JOIN escorts e ON e.id = rp.escort_id	
			WHERE rp.id = ?
		';
		}
		
		
		$results = parent::getAdapter()->fetchRow($sql, array($id));

		return $results;
	}

	public function getAll($bu_user,$page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{

	    $edir_backend = false;
        if( in_array(Cubix_Application::getId(), array(APP_ED) ) ){
            $edir_backend = true;
        }
        if($edir_backend){
            $sql = '
			SELECT
				rp.photo_link ,rp.report_type ,rp.id , rp.name , rp.email , rp.report, rp.problem, rp.escort_id, UNIX_TIMESTAMP(rp.date) as date, e.showname, e.agency_id, e.id as escort_id, u.username,  u.user_type, rp.user_id, rp.status
			FROM '.$this->_table.' rp
			LEFT JOIN escorts e ON e.id = rp.escort_id	
			LEFT JOIN users u ON u.id = rp.user_id 
			WHERE 1
		';
        }else{
            $sql = '
			SELECT
				rp.id , rp.name , rp.email , rp.report, rp.problem, rp.escort_id, UNIX_TIMESTAMP(rp.date) as date, e.showname, u.username, rp.user_id, rp.status
			FROM '.$this->_table.' rp
			LEFT JOIN escorts e ON e.id = rp.escort_id	
			LEFT JOIN users u ON u.id = rp.user_id 
			WHERE 1
		';
        }

		
		$countSql = '
			SELECT COUNT(rp.id)
			FROM  '.$this->_table.' rp
			LEFT JOIN escorts e ON e.id = rp.escort_id		
			WHERE 1
		';
		
		if ( strlen($filter['application_id']) ) {
			$where .= self::quote('AND rp.application_id = ?', $filter['application_id']);
		}
		
		if ( strlen($filter['email']) ) {
			$where .= self::quote('AND rp.email = ?', $filter['email']);
		}

		if($edir_backend){
            if ( strlen($filter['report_type']) && $filter['report_type'] > 0) {
                $where .= self::quote('AND rp.report_type = ?', $filter['report_type']);
            }
        }


		
		if ( strlen($filter['name']) ) {
			$where .= self::quote('AND rp.name LIKE ?', $filter['name'].'%');
		}
		
		if ( strlen($filter['showname']) ) {
			$where .= self::quote('AND e.showname LIKE ?', $filter['showname'].'%');
		}
		if (strlen($filter['problem']) ) {
			$where .= self::quote('AND rp.problem = ?', $filter['problem']);
		}
		if(isset($bu_user) && is_numeric($bu_user))
		{
				$where .=" AND rp.escort_id IN(SELECT id FROM escorts WHERE user_id IN(SELECT id FROM users WHERE sales_user_id='$bu_user')) ";
		}
		
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		//echo $sql;die;

		$count = intval($this->getAdapter()->fetchOne($countSql));
		return parent::getAdapter()->fetchAll($sql);
	}

	
	public function remove($id)
	{
		parent::getAdapter()->delete($this->_table, parent::quote('id = ?', $id));
		parent::getAdapter()->delete('report_problem_replys', parent::quote('rp_id = ?', $id));
	}

	public function check($id)
	{
		parent::getAdapter()->update($this->_table, array('status' => REPORT_PROBLEM_STATUS_CHECKED), parent::quote('id = ?', $id));
	}
	
	public function reply($data)
	{
		parent::getAdapter()->insert('report_problem_replys', $data);
		parent::getAdapter()->update($this->_table, array('status' => REPORT_PROBLEM_STATUS_REPLIED), parent::quote('id = ?', $data['rp_id']));
	}
	
	public function getReplys($rp_id)
	{
		return parent::getAdapter()->query('SELECT r.*, b.username FROM report_problem_replys r LEFT JOIN backend_users b ON b.id = r.backend_user_id WHERE r.rp_id = ?', $rp_id)->fetchAll();
	}
}
