<?php
class Model_Suspicious extends Cubix_Model
{
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$fileds = '';
		if ( Cubix_Application::getId() == APP_ED ) {
			$fileds = ' , s.email, s.type ';
		}

		$sql = '
			SELECT 
				s.id, s.escort_id, s.user_id, s.link_1, s.link_2, s.link_3, s.comment, s.status, UNIX_TIMESTAMP(s.date) AS date, 
				e.showname, e.status AS escort_status, u.username '.$fileds.'
			FROM suspicious_photos_requests s
			INNER JOIN escorts e ON e.id = s.escort_id
			LEFT JOIN users u ON s.user_id = u.id
			WHERE 1 
		';
		
		$countSql = '
			SELECT COUNT(DISTINCT(s.id))
			FROM suspicious_photos_requests s
			INNER JOIN escorts e ON e.id = s.escort_id
			LEFT JOIN users u ON s.user_id = u.id
			WHERE 1 
		';
				
		$where = '';

		if ( strlen($filter['application_id']) ) {
			$where .= self::quote(' AND s.application_id = ?', $filter['application_id']);
		}
				
		if ( strlen($filter['showname']) ) {
			$where .= self::quote(' AND e.showname LIKE ? ', $filter['showname'] . '%');
		}
				
		if ( $filter['escort_id'] ) {
			$where .= self::quote(' AND e.id = ?', $filter['escort_id']);
		}
			
		if ( strlen($filter['status']) > 0 ) {
			$where .= self::quote(' AND s.status = ?', $filter['status']);
		}
		
		if ( Cubix_Application::getId() == APP_ED ) {
			if ( strlen($filter['type']) > 0 ) {
				$where .= self::quote(' AND s.type = ?', $filter['type']);
			}
		}
		$sql .= $where;
		$countSql .= $where;
				
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return $this->getAdapter()->query($sql)->fetchAll();
	}
	
	public function get($id)
	{
		$fileds = '';
		if ( Cubix_Application::getId() == APP_ED ) {
			$fileds = ' , s.email, s.type ';
		}

		$sql = '
			SELECT 
				s.id, s.escort_id, s.user_id, s.link_1, s.link_2, s.link_3, s.comment, s.status, UNIX_TIMESTAMP(s.date) AS date, 
				e.showname, e.status AS escort_status, u.username '.$fileds.'
			FROM suspicious_photos_requests s
			INNER JOIN escorts e ON e.id = s.escort_id
			LEFT JOIN users u ON s.user_id = u.id
			WHERE s.id = ? 
		';

		return $this->getAdapter()->query($sql, $id)->fetch();
	}
	
	public function setStatus($id, $status)
	{
		$this->getAdapter()->update('suspicious_photos_requests', array('status' => $status), $this->getAdapter()->quoteInto('id = ?', $id));
	}
	
	public function getEscortVerifiedStatus($escort_id)
	{
		return $this->getAdapter()->fetchOne('SELECT status FROM verify_requests WHERE escort_id = ? ORDER BY id DESC LIMIT 1', $escort_id);
	}
}
	
