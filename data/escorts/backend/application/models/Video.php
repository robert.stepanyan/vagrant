<?php


class Model_Video  extends Cubix_Model {
	protected $_table = 'video';
	protected $_itemClass = 'Model_VideoItem';
	
	const COVER_STATUS_PENDING = 1;
	const COVER_STATUS_VERIFIED = 2;
	const COVER_STATUS_REJECTED = 3;	
	
	public function __construct() 
	{

	}
	
	public function GetEscortVideo($id,$date_from,$date_to,$page=1,$per_page=10,$sort_field='date',$sort_dir='DESC',$status=null, $cover_status=null)
	{	
		$fields = '';
		if (Cubix_Application::getId() == APP_A6) {
			$fields .= ', v.vote_count';
		}

		$where = array();
			
		if(isset($id) && is_numeric($id))
		{	$id="$id";
			$id = self::$_db->quote($id);	
			$where[]=" e.id= $id ";
		}
		elseif(isset($id) && trim($id)!='' && !is_numeric($id))
		{
			return	array('data'=>array(),'count'=>0);
		}
		if(isset($status) && $status!=='0')
		{	$status = self::$_db->quote($status);	
			$where[] = " v.`status`=$status ";
		}
		if(isset($cover_status) && $cover_status!=='0' && Cubix_Application::getId() == APP_EF)
		{	$cover_status = self::$_db->quote($cover_status);	
			$where[] = " v.`cover_status`=$cover_status ";
		}
		if(isset($date_from) && $date_from!='')
		{		
			$date_from =date('Y-m-d',$date_from);
			$where[]=" DATE_FORMAT(v.`date`,'%Y-%m-%d')>='$date_from' ";
		}
		if(isset($date_to) && $date_to!='')
		{
			$date_to =date('Y-m-d',$date_to);
			$where[]=" DATE_FORMAT(v.`date`,'%Y-%m-%d')<='$date_to' ";
		}
		$where[] = "e.id = v.escort_id ";
		$where = implode(' AND ', $where);
	

		$page = !isset($page) || !is_numeric($page)?1:self::$_db->quote($page);
		$start = ($page-1)*$per_page;
        //Get video approve date and bo user for 6anuncio
        $sql_join_6b ='';
        $sql_fields_6b ='';
        if(Cubix_Application::getId() == APP_6B){
            $sql_fields_6b ='v.updated_at,b.first_name,';
            $sql_join_6b = 'LEFT JOIN backend_users b ON b.id =  v.approved_user_id';
        }

        $sql_fields_ed = '';
        $sql_fields_uk = '';
        $sql_join_ed ='';
        $sql_join_uk ='';
        if(Cubix_Application::getId() == APP_ED){
            $sql_fields_ed ='c.title_en as escort_country,';
            $sql_join_ed = 'LEFT JOIN countries c ON c.id =  e.country_id';
        }

        if(Cubix_Application::getId() == APP_EG_CO_UK){
            $sql_fields_uk =' bu.username as backend_user, v.updated_at,';
            $sql_join_uk = ' INNER JOIN users u ON u.id = e.user_id LEFT JOIN backend_users bu ON bu.id =  u.sales_user_id ';
        }


        $sql = "SELECT SQL_CALC_FOUND_ROWS COUNT(v.id) AS quantity, e.showname,e.gender, v.escort_id,v.date,".$sql_fields_6b.$sql_fields_ed.$sql_fields_uk." v.status, v.id, v.video, v.height" . $fields . "
			 FROM video v
			 ".$sql_join_6b."
			INNER JOIN escorts e
			ON  $where
			  ".$sql_join_ed."
             ".$sql_join_uk."
			 GROUP BY v.id
			ORDER BY v.`$sort_field` $sort_dir LIMIT $start,$per_page" ;
		$data = self::$_db->fetchAll($sql);

		$found = self::$_db->fetchRow('SELECT FOUND_ROWS() AS q');

		return	array('data'=>$data,'count'=>$found->q);
	}


	public function editVideo($id, $video_id = null)
	{
		if($video_id){
			$where = 'WHERE id = '. $video_id;
		}
		else{
			$where = 'WHERE escort_id = '. $id;
		}
				
		$sql_video = "SELECT * FROM video ". $where;
		$video = self::$_db->fetchAll($sql_video);
			
		if(!empty($video))
		{
			$sql_image = "SELECT i.hash, i.ext, i.width, i.height FROM video_image vi 
						  INNER JOIN images i ON vi.image_id = i.id 
						  WHERE vi.video_id = ? ";
			
			foreach ($video as &$v)
			{
				$photo_data = self::$_db->fetchRow($sql_image,$v->id );
				
				if($photo_data){
					$photo = new Cubix_ImagesCommonItem(array('hash' => $photo_data->hash,'escort_id' => $v->escort_id, 'width' => $photo_data->width, 'height' => $photo_data->height,'ext' => $photo_data->ext));
					$v->photo = array( 'url' => $photo->getUrl('m320'), 'width' => $photo->width, 'height' => $photo->height );
				}
				else{
					$v->photo = array( 'url' => '/img/video_back_logo.jpg', 'width' => 640, 'height' => 480 );
				}

				if(Cubix_Application::getId() == APP_EF) {
					if(!is_null($v->cover_hash)) {
						$cover = new Cubix_ImagesCommonItem(array('hash' => $v->cover_hash,'escort_id' => $v->escort_id, 'ext' => $v->cover_ext));
						$v->cover = array( 'url' => $cover->getUrl('m320'), 'orig_url' => $cover->getUrl('orig'));
					} else {
						$v->cover = array( 'url' => '/img/cover_back.png'); 
					}
				}
			}
		}
			
		return $video;
	}
	
	public function Remove($escort_id,$check)
	{
		$str_id ='';
		$count = count($check);
		for($i=0;$i<$count;$i++)
		{
			$check[$i] = self::$_db->quote($check[$i]);
			$str_id.=$check[$i].',';
		}
		$str_id=substr($str_id,0,strlen($str_id)-1);
		$sql = " DELETE FROM video WHERE id IN($str_id) AND escort_id=$escort_id ";
		try 
		{
			self::$_db->query($sql);
			Cubix_SyncNotifier::notify($escort_id,Cubix_SyncNotifier::EVENT_ESCORT_VIDEO_DELETED);
		}
		catch (Exception $e) {
			//throw new Exception($e->getMessage());
			var_dump($e->getMessage());
			
        }
	}
	
	public static function  GetVideoStatusCount($escort_id)
	{	
		$escort_id = self::$_db->quote($escort_id);
		return parent::_fetchAll("SELECT count(id) AS quantitiy,`status` FROM video WHERE escort_id=$escort_id GROUP BY escort_id,`status` ORDER by `status` ");
	}
	
	public function SaveVideo($data)
	{	
		try {
            self::$_db->insert('video', $data);
			$image_id = self::$_db->lastInsertId();
			Cubix_SyncNotifier::notify($data['escort_id'],Cubix_SyncNotifier::EVENT_ESCORT_VIDEO_ADDED);
        } catch (Exception $e) {
			var_dump($e->getMessage());
			//throw new Exception($e->getMessage());
        }
		return $image_id;
	}
	
	
	public function SaveVideoImage($escort_id,$data)
	{
		try {
			self::$_db->insert('video_image', $data);
			$image_id = self::$_db->lastInsertId();
			if(is_numeric($image_id))
			Cubix_SyncNotifier::notify($escort_id,Cubix_SyncNotifier::EVENT_ESCORT_VIDEO_ADDED);
			return $image_id;
		} 
		catch (Exception $e) {
			var_dump($e->getMessage());
			//throw new Exception($e->getMessage());
		}
	}


	public function setStatus($ids,$status,$escort_id,$max_video)
	{	
		if(!is_array($ids)){
			$ids = array($ids);
		}
		$str_id ='';
		$count = count($ids);
		$count = $count>$max_video?$max_video:$count;
		self::$_db = Zend_Registry::get('db');
		for($i=0;$i<$count;$i++)
		{	
			$ids[$i] = self::$_db->quote($ids[$i]);
			$str_id.=$ids[$i].',';
		}
		$str_id=substr($str_id,0,strlen($str_id)-1);
        $query_user_update = '';
        if(in_array(Cubix_Application::getId(),array(APP_6B,APP_EG_CO_UK))){

            $user = Zend_Auth::getInstance()->getIdentity();
            if($user){
                $approved_user_id = $user->id;
                $query_user_update = ", `updated_at`= NOW(), `approved_user_id`= '$approved_user_id'";
            }
        }
        $sql = " UPDATE video SET `status`='$status' $query_user_update WHERE id IN($str_id) AND escort_id=$escort_id  ";
		
		try {	
			
				$result = self::$_db->query($sql)->rowCount();
				if($result>0)
				{
					Cubix_SyncNotifier::notify($escort_id,Cubix_SyncNotifier::EVENT_ESCORT_VIDEO_UPDATED);
				}
				return $result;
		}
		catch (Exception $e) {
			   throw new Exception(Cubix_Api_Error::Exception2Array($e->getMessage()));
        }
	}

	public function setRejectReasons($ids,$reasons, $show_popup, $escort_id,$max_video)
	{	
		if(!is_array($ids)){
			$ids = array($ids);
		}
		$str_id ='';
		$count = count($ids);
		$count = $count>$max_video?$max_video:$count;
		self::$_db = Zend_Registry::get('db');
		for($i=0;$i<$count;$i++)
		{	
			$ids[$i] = self::$_db->quote($ids[$i]);
			$str_id.=$ids[$i].',';
		}
		$str_id=substr($str_id,0,strlen($str_id)-1);
		$sql = " UPDATE video SET `reject_reasons`='$reasons', `show_popup` = '$show_popup' WHERE id IN($str_id) AND escort_id=$escort_id  ";
		
		try {	
			
			$result = self::$_db->query($sql)->rowCount();
			return $result;
		}
		catch (Exception $e) {
			   throw new Exception(Cubix_Api_Error::Exception2Array($e->getMessage()));
        }
	}

	public function getPendingVideo($escort_id)
	{
		return self::$_db->fetchRow("SELECT v.id, v.video, v.height, v.width, i.hash as image_hash, i.ext as image_ext FROM video v
									INNER JOIN video_image vi ON vi.video_id = v.id
									INNER JOIN images i ON i.id = vi.image_id
									WHERE v.escort_id = ? ", array($escort_id));
	}
	
	public function getVideoById($id)
	{
		 return self::$_db->fetchOne(" SELECT video FROM video WHERE id = ? ", $id);
	}
	
	public function deleteById($video_id)
	{
		return self::$_db->delete($this->_table, parent::quote('id = ?', $video_id));
	}
	
	public function setDimensions($video_id)
	{
		$video_data = self::$_db->fetchRow("SELECT escort_id, video, height, width FROM video WHERE id = ? ", $video_id);
		$config = Zend_Registry::get('videos_config');
		$url = $config['remote']['url']."videos.php?a=set-dimensions&app=".Cubix_Application::getById()->host."&eid=".$video_data->escort_id."&hash=".$video_data->video."&ext=mp4&width=".$video_data->width."&height=".$video_data->height;
		echo $url;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);
		if (Cubix_Application::getId() == APP_A6){
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		}
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set curl to return the data instead of printing it to the browser.
		curl_setopt($ch, CURLOPT_URL, $url);
		$data = curl_exec($ch);
		if (FALSE === $data){
			echo $url;
			var_dump(curl_getinfo($ch), curl_errno($ch), curl_error($ch)) ;die;
		}
		curl_close($ch);
	}


	public function setCoverStatus($video_id, $escort_id, $cover_status)
	{	
		self::$_db = Zend_Registry::get('db');
		$sql = " UPDATE video SET `cover_status`='$cover_status' WHERE id=$video_id AND escort_id=$escort_id ";
		
		try {	
			$result = self::$_db->query($sql)->rowCount();
			if($result>0)
			{
				Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_VIDEO_UPDATED);
			}
			return $result;
		}
		catch (Exception $e) {
			throw new Exception(Cubix_Api_Error::Exception2Array($e->getMessage()));
        }
	}

	public function removeCover($video_id, $escort_id)
	{
		self::$_db = Zend_Registry::get('db');
		$sql = " UPDATE video SET `cover_status`=null,`cover_hash`=null,`cover_ext`=null WHERE id=$video_id AND escort_id=$escort_id ";

		try 
		{
			$result = self::$_db->query($sql)->rowCount();
			return $result;
		}
		catch (Exception $e) {
			throw new Exception(Cubix_Api_Error::Exception2Array($e->getMessage()));
        }
	}

	public function saveCover($data)
	{		
		$cover_hash = $data['cover_hash'];
		$cover_ext = $data['cover_ext'];
		$cover_status = $data['cover_status'];
		$escort_id = $data['escort_id'];
		$video_id = $data['video_id'];

		self::$_db = Zend_Registry::get('db');
		$sql = " UPDATE video SET `cover_status`='$cover_status',`cover_hash`='$cover_hash',`cover_ext`='$cover_ext' WHERE id=$video_id AND escort_id=$escort_id ";

		try 
		{
			$result = self::$_db->query($sql)->rowCount();
			return $result;
		}
		catch (Exception $e) {
			throw new Exception(Cubix_Api_Error::Exception2Array($e->getMessage()));
        }
	}

	public static function HTML5_upload_cover()
	{
        $headers = array();
        $headers['Content-Length'] = $_SERVER['CONTENT_LENGTH'];
        $headers['X-File-Id'] = $_SERVER['HTTP_X_FILE_ID'];
        $headers['X-File-Name'] = $_SERVER['HTTP_X_FILE_NAME'];
        $headers['X-File-Resume'] = $_SERVER['HTTP_X_FILE_RESUME'];
        $headers['X-File-Size'] = $_SERVER['HTTP_X_FILE_SIZE'];

        $response = array();
        $response['id'] = $headers['X-File-Id'];
        $response['name'] = basename($headers['X-File-Name']); 	// Basename for security issues
        $response['size'] = $headers['Content-Length'];
        $response['error'] = UPLOAD_ERR_OK;
        $response['finish'] = FALSE;

        // Is resume?
        $flag = (bool)$headers['X-File-Resume'] ? FILE_APPEND : 0;
        $filename = $response['id'].'_'.$response['name'];
        $response['upload_name'] = $filename;

        // Write file
        $file = sys_get_temp_dir().DIRECTORY_SEPARATOR.$response['upload_name'];

        if (file_put_contents($file, file_get_contents('php://input'), $flag) === FALSE){
            $response['error'] = UPLOAD_ERR_CANT_WRITE;
        }
        else
        {
            $response['file_size'] = filesize($file);
            $response['X-File-Size'] = $headers['X-File-Size'];
            if (filesize($file) == $headers['X-File-Size'])
            {
                $response['tmp_name'] = $file;
                $response['finish'] = TRUE;
            }
        }

        return $response;
	}	

	public function showResponse($response)
	{
		// If is HTML5
		if(isset($response['id'])){
			header('Cache-Control: no-cache, must-revalidate');
			header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
			header('Content-type: application/json');
		}
		echo json_encode($response);
	}		
}
