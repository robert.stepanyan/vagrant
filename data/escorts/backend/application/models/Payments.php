<?php
class Model_Payments extends Cubix_Model
{
	protected $_table = 'cgp_callback';
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				mpl.id,
				mpl.member_id,
				mpl.callback_id,
				u.username,
				m.is_recurring AS is_rec,
				UNIX_TIMESTAMP(mpl.date) AS date,
				UNIX_TIMESTAMP(m.date_expires) AS expiration_date,
				CONCAT(cc.status_desc," ",cc.status) AS status_desc,
				cc.status AS status,
				cc.amount

			FROM '.$this->_table.' cc
			INNER  JOIN members_premium_log mpl ON cc.id = mpl.callback_id
			LEFT  JOIN members m ON m.id = mpl.member_id
			LEFT  JOIN users u ON u.id = m.user_id
			WHERE 1
		';
		
		$countSql = 'SELECT FOUND_ROWS()';
		
		if ( strlen($filter['member_name']) ) {
			$where .= self::quote(' AND u.username LIKE ?', $filter['member_name'] . '%');
		}

		if ( strlen($filter['is_rec'])) {
			$where .= self::quote(' AND  m.is_recurring = ?', $filter['is_rec']);
		}

		if ( $filter['date_from'] ) {
				$where .= self::quote(" AND DATE(cc.date) >= DATE(FROM_UNIXTIME(?)) ", $filter['date_from']);
			}

		if ( $filter['date_to'] ) {
			$where .= self::quote(" AND DATE(cc.date) <= DATE(FROM_UNIXTIME(?)) ", $filter['date_to']);
		}

		if ( $filter['trans-success'] == 1) {
			$succes_statuses = implode(',' , Cubix_CGP_Callback::$success_statuses);
			$where .= ' AND  cc.status IN ('.$succes_statuses.')';
		}
		$sql .= $where;
		//$countSql .= $where;
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		$result =  parent::_fetchAll($sql);

		$count = intval($this->getAdapter()->fetchOne($countSql));

		return $result;
	}
	public function getRawByID($cgp_id)
	{
		$sql = 'SELECT raw FROM cgp_callback WHERE id = ?';
		$raw = $this->getAdapter()->fetchOne($sql,$cgp_id);
		return json_decode($raw);
	}

	public function getTurnover($filter)
	{
		$succes_statuses = implode(',' , Cubix_CGP_Callback::$success_statuses);
		$sql = 'SELECT SUM(cc.amount) as turnover FROM '.$this->_table.' cc
			 INNER  JOIN members_premium_log mpl ON cc.id = mpl.callback_id
			 LEFT  JOIN members m ON m.id = mpl.member_id
			 LEFT  JOIN users u ON u.id = m.user_id
			 WHERE cc.status IN ('.$succes_statuses .') ';

		if ( $filter['date_from'] ) {
				$where .= self::quote(" AND DATE(cc.date) >= DATE(FROM_UNIXTIME(?)) ", $filter['date_from']);
			}

		if ( $filter['date_to'] ) {
			$where .= self::quote(" AND DATE(cc.date) <= DATE(FROM_UNIXTIME(?)) ", $filter['date_to']);
		}

		if ( strlen($filter['member_name']) ) {
			$where .= self::quote(' AND u.username LIKE ?', $filter['member_name'] . '%');
		}
		$sql .= $where;
		return  $this->getAdapter()->fetchOne($sql);
	}
			
}
