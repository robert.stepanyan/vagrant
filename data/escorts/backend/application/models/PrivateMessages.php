<?php

class Model_PrivateMessages extends Cubix_Model
{

    public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS m.id, m.body AS message, UNIX_TIMESTAMP(m.date) as date, m.status, m.flag, u.username, e.showname, m.user_id, m.escort_id, m.thread_id
			FROM messages m
			LEFT JOIN escorts e ON e.id = m.escort_id
			LEFT JOIN users u ON u.id = m.user_id
			WHERE 1
		';

		$countSql = 'SELECT FOUND_ROWS()';
		
		if ( strlen($filter['showname']) ) {
			$sql .= self::quote('AND e.showname LIKE ?', '%' . $filter['showname'] . '%');
		}
		
		if ( strlen($filter['escort_id']) ) {
			$sql .= self::quote('AND e.id = ?', $filter['escort_id']);
		}
		
		if ( strlen($filter['username']) ) {
			$sql .= self::quote('AND u.username LIKE ?', '%' . $filter['username'] . '%');
		}
		if ( strlen($filter['status']) ) {
			$sql .= self::quote('AND m.status = ?', $filter['status']);
		}
		if ( strlen($filter['flag']) ) {
			$sql .= self::quote('AND m.flag = ?', $filter['flag']);
		}

		$sql .= '
			GROUP BY id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$ret = parent::_fetchAll($sql);
		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return $ret;
	}

	public function get($id)
	{
		return self::_fetchRow('
			SELECT m.id, m.body AS message, UNIX_TIMESTAMP(m.date) as date, m.status, u.username, e.showname, m.thread_id, m.user_id, e.id AS escort_id
			FROM messages m
			LEFT JOIN escorts e ON e.id = m.escort_id
			LEFT JOIN users u ON u.id = m.user_id
			WHERE m.id = ?
		', array($id));
	}

	public function update($data)
	{
		parent::getAdapter()->update('messages', $data, parent::quote('id = ?', $data['id']));
	}
	
	public function updateLastMessageData($item)
	{
		try {
			parent::getAdapter()->beginTransaction();
			//Getting last confirmed message of thread
			$last_message = self::_fetchRow('SELECT m.id FROM messages m WHERE thread_id = ? AND m.status = ? ORDER BY m.date DESC LIMIT 1', array($item->thread_id, 2));
			parent::getAdapter()->update('threads', array('last_message_id' => $last_message->id), parent::quote('id = ?', $item->thread_id));

			if ( ! is_null($item->escort_id) ) {
				$field = 'escort_id';
				$value = $item->escort_id;
			} else {
				$field = 'user_id';
				$value = $item->user_id;
			}

			//Updating flag in threads_participants
			$has_unread_message = parent::getAdapter()->fetchOne('SELECT TRUE FROM messages WHERE thread_id = ? AND read_date IS NULL', array($item->thread_id));
			parent::getAdapter()->query('UPDATE threads_participants SET has_unread_message = ? WHERE thread_id = ? AND (' . $field . ' <> ? OR ' . $field . ' IS NULL)', array($has_unread_message ? 1 : 0, $item->thread_id, $value));
			parent::getAdapter()->commit();
		} catch(Exception $ex) {
			parent::getAdapter()->rollBack();
			throw $ex;
		}
		
	}
	
	public function getReceiverData($thread_id,$escort_id, $user_id){
		
		if ( ! is_null($escort_id) ) {
				$field = 'tp.escort_id';
				$value = $escort_id;
			} else {
				$field = 'tp.user_id';
				$value = $user_id;
			}
			$fields_6a = "";
			if(Cubix_Application::getId() == APP_6A ){
				$fields_6a = " ,u.recieve_pm_email, u.recieve_pm_email_text";
			}
			return self::_fetchRow('
			SELECT e.showname , u.username, u.email '.$fields_6a.' 
			FROM threads_participants tp
			LEFT JOIN escorts e ON e.id = tp.escort_id
			LEFT JOIN users u ON u.id = tp.user_id OR u.id = e.user_id
			WHERE (' . $field . ' <> ? OR ' . $field . ' IS NULL) AND tp.thread_id = ?
		', array($value,$thread_id));
	}

    /**
     * Combines old messages/threads information and moves to archive table
     * @throws Cubix_Cli_Exception
     */
    public function archiveOldConversations()
    {
        define('CLI_LINE_BREAK', str_repeat('- ', 40));
        $cli = new Cubix_Cli();
        $cli->clear();

        $archiveTable = $this->getCurrentArchiveTable();

        $step = 200;
        $pivot = 0;
        $daysToArchive = 60; // 2 Months

        $baseSql = "SELECT
            tp.thread_id,	t.last_message_date 
        FROM threads_participants tp
	    INNER JOIN threads t ON t.id = tp.thread_id 
        WHERE t.last_message_date <= subdate( CURRENT_DATE, $daysToArchive ) 
        GROUP BY t.id 
        ORDER BY t.last_message_date";

        $threadsArchived = 0;
        $overallThreadsToArchive = self::$_db->fetchOne("SELECT count(*) 
            FROM (
                SELECT t.id 
                FROM threads_participants tp 
                INNER JOIN threads t ON t.id = tp.thread_id  
                WHERE t.last_message_date <= subdate( CURRENT_DATE, $daysToArchive ) 
                GROUP BY t.id
            ) _
        ");

        $messagesArchived = 0;

        do {
            parent::getAdapter()->beginTransaction();

            try {

                if($pivot % 10 == 0) {
                    $cli->colorize('yellow')->out(CLI_LINE_BREAK)->reset();
                }

                $cli->colorize('green')->out("[$threadsArchived/$overallThreadsToArchive] threads | [$messagesArchived] messages archived.")->reset();

                $sqlToRun = $baseSql . ' LIMIT ' . ($pivot * $step) . ',' . $step;
                $threads = parent::_fetchAll($sqlToRun);
                $threadIds = [];
                foreach ($threads as $thread) $threadIds[] = $thread->thread_id;

                if(!count($threadIds)) {
                    $cli->colorize('yellow')->out("No more threads found.")->reset();
                    break;
                };
                $messageRows = parent::_fetchAll('SELECT * FROM messages WHERE thread_id IN (' . implode(',', $threadIds) . ')');

                $messages = [];
                $messageIds = [];

                if(count($messageRows)) {
                    $cli->colorize('yellow')->out(count($messageRows) . " messages found to archive")->reset();
                }

                // Composing attachments into messages to use as a json
                // and avoid creation of another table for archived attachments
                foreach ($messageRows as $row) {

                    if (!isset($messages[$row->id])) {
                        $messageIds[] = $row->id;
                        $messages[$row->id] = [
                            'id' => $row->id,
                            'user_id' => !empty($row->user_id) ? $row->user_id : 0,
                            'escort_id' => !empty($row->escort_id) ? $row->escort_id : 0,
                            'agency_id' => !empty($row->agency_id) ? $row->agency_id : 0,
                            'thread_id' => $row->thread_id || 0,
                            'body' => $row->body || null,
                            'date' => $row->date || null,
                            'read_date' => $row->read_date || null,
                            'status' => !empty($row->status) ? $row->status : 0,
                            'flag' => !empty($row->flag) ? $row->flag : 0,
                        ];
                    }
                }

                // Remove messages from table since they are going to be inside archive
                if ( !empty($messageIds)) {
                    $this->getAdapter()->query('DELETE FROM messages WHERE id IN(' . implode($messageIds) . ')');
                }

                // Remove threads from table since they are going to be inside archive
                if (!empty($threadIds)) {
                    $threadsArchived += count($threadIds);

                    $this->getAdapter()->query('DELETE FROM threads WHERE id IN (' . implode(',', $threadIds) . ')');
                    $this->getAdapter()->query('DELETE FROM threads_participants WHERE thread_id IN (' . implode(',', $threadIds) . ')');
                }

                // "Bulk insert" composed messages to archive
                $insertSql = "INSERT IGNORE INTO $archiveTable 
                    (`id`, `user_id`, `escort_id`, `agency_id`, `thread_id`, `body`, `date`, `read_date`, `status`, `flag`) VALUES";

                $insertRows = [];
                foreach ($messages as $message) {

                    $message['body'] = self::$_db->quote($message['body']);
                    $message['date'] = self::$_db->quote($message['date']);
                    $message['read_date'] = self::$_db->quote($message['read_date']);

                    $insertRows[] = "
                        (
                            {$message['id']},
                            {$message['user_id']},
                            {$message['escort_id']},
                            {$message['agency_id']},
                            {$message['thread_id']},
                            {$message['body']},
                            {$message['date']},
                            {$message['read_date']},
                            {$message['status']},
                            {$message['flag']}
                        )
                    ";

                }

                if (!empty($insertRows)) {

                    $insertSql .= implode(' , ', $insertRows);

                    try {
                        $this->getAdapter()->query($insertSql);

                    } catch (Exception $e) {
                        $cli->colorize('blue')->out($e->getMessage())->reset();
                        die($insertSql);
                    }

                    $messagesArchived += count($messages);
                }

                parent::getAdapter()->commit();
                $pivot++;

            } catch (Exception $e) {
                parent::getAdapter()->rollBack();

                $cli->colorize('red')->out($e->getMessage())->reset();

                throw $e;
            }
        } while (count($threads) > 0);

        $cli->colorize('green')->out('Done !Enjoy your day :)')->reset();
        exit;
    }

    /**
     * @return string
     */
    public function getCurrentArchiveTable()
    {
        $sql = self::getArchiveTableSQL();
        $this->getAdapter()->query($sql);
        return self::getCurrentArchiveTableName();
    }

    /**
     * This method simply returns the name of archived tables
     * depending on the current date
     * e.g. table_name_05_2020
     * @return string
     */
    private static function getCurrentArchiveTableName()
    {
        return 'messages_archive_' . date('m') . '_' . date('Y');;
    }

    /**
     * Returns "messages archive"s table structure
     * @return string
     */
    private static function getArchiveTableSQL()
    {
        $tableName = self::getCurrentArchiveTableName();
        return " 
            CREATE TABLE IF NOT EXISTS `$tableName`  (
              `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
              `user_id` int(10) UNSIGNED NULL DEFAULT NULL,
              `escort_id` int(10) NULL DEFAULT NULL,
              `agency_id` int(10) NULL DEFAULT NULL,
              `thread_id` int(10) UNSIGNED NOT NULL,
              `body` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
              `date` datetime NOT NULL,
              `read_date` datetime NULL DEFAULT NULL,
              `status` tinyint(1) UNSIGNED ZEROFILL NULL DEFAULT 2,
              `flag` tinyint(1) NULL DEFAULT 0,
              PRIMARY KEY (`id`) USING BTREE
            )";
    }
}
