<?php

class Model_Escort_SnapshotV2 extends Cubix_Model_Item
{
	public function __construct($rowData = null)
	{
		if ( !is_null($rowData) ) {
			parent::__construct($rowData);
		}
		else {
			parent::__construct();
		}
	}

	public function getFullProfileV2()
	{
		//Zend_Controller_Front::getInstance()->getRequest()->setParam('lang_id', $lang_id);

		$sql = array(
			'tables' => array('escort_profiles_v2 ep'),
			'fields' => array(
				/* General */
				'e.id', 'e.agency_id', 'e.showname', 'e.gender', Cubix_I18n::getTblFields('ct.title', null, null, 'city_title'), 'eph.hash AS photo_hash',
				'eph.ext AS photo_ext', 'u.application_id', 'ep.email', 'u.username', 'eph.status AS photo_status','e.disabled_reviews', 'e.block_website',

				/* Contacts Section */
				Cubix_I18n::getTblFields('ct.title', null, null, 'base_city'), 'ct.id AS base_city_id', Cubix_I18n::getTblFields('c.title', null, null, 'country'),
				'c.slug AS country_slug', 'c.iso AS country_iso', 'r.slug AS region_slug', Cubix_I18n::getTblFields('r.title', null, null, 'region_title'), 'ct.slug AS city_slug',
				'IF(((DATE(e.vac_date_from) <= CURDATE() AND DATE(e.vac_date_to) > CURDATE()) OR DATE(e.vac_date_from) > CURDATE()), e.vac_date_to, 0) AS vac_date_to',
				'ep.phone_country_id','ep.phone_number','ep.phone_sms_verified', 'ep.phone_number_alt', 'ep.phone_instr', 'ep.phone_instr_no_withheld', 'ep.phone_instr_other','ep.disable_phone_prefix', 'ep.club_name', 'ep.website',
				'ep.email', 'ep.zip', 'ep.street', 'ep.street_no', 'ep.address_additional_info', 'ep.available_24_7', 'e.home_city_id',

				/* About Section */
				'ep.birth_date', 'n.iso AS nationality_iso', Cubix_I18n::getTblFields('n.title', null, null, 'nationality_title'),
				'ep.eye_color', 'ep.hair_color', 'ep.hair_length', 'ep.height', 'ep.weight', 'ep.bust', 'ep.waist', 'ep.hip', 'ep.shoe_size', 'ep.cup_size','ep.breast_type','ep.dress_size',
				'ep.is_smoking', 'ep.is_drinking', 'ep.measure_units', 'ep.sex_availability', 'ep.ethnicity', Cubix_I18n::getTblFields('ep.about', null, null, 'about'),
				'ep.characteristics', 'ep.pubic_hair', 'ep.tatoo', 'ep.piercing', 'ep.incall_type', 'ep.incall_hotel_room', 'ep.incall_other', 'ep.outcall_type', 'ep.outcall_other',
				'ep.sex_orientation','ep.special_rates',

				/* SetcardInfo Section */
				'e.date_last_modified', 'e.date_registered', 'u.last_login_date', '(SELECT SUM(count) FROM escort_hits_daily WHERE escort_id = e.id AND DATE_ADD(date, INTERVAL 90 DAY) >= DATE(NOW())) AS hit_count',

				/* Services Section */
				Cubix_I18n::getTblFields('ep.additional_service', null, null, 'additional_service')
			),
			'joins' => array(
				'INNER JOIN escorts e ON ep.escort_id = e.id',
				'INNER JOIN users u ON u.id = e.user_id',
				'INNER JOIN applications a ON a.id = u.application_id',

				'LEFT JOIN nationalities n ON n.id = ep.nationality_id',

				'LEFT JOIN escort_working_times ewt ON ewt.escort_id = e.id',
				'LEFT JOIN escort_langs el ON el.escort_id = e.id',
				'INNER JOIN escort_cities ec ON ec.city_id = e.city_id',

				'INNER JOIN countries c ON c.id = e.country_id',
				'INNER JOIN cities ct ON ct.id = ec.city_id',
				'LEFT JOIN regions r ON r.id = ct.region_id',
				'LEFT JOIN cityzones cz ON cz.id = e.cityzone_id',

				'LEFT JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1',

				/*'LEFT JOIN escort_hits_daily ehd ON ehd.escort_id = e.id AND DATE_ADD(ehd.date, INTERVAL 90 DAY) >= DATE(NOW())'*/
			),
			'where' => array(
				 'e.id = ?' => $this->getId()
			),
			'group' => 'e.id',
			'order' => false,
			'page' => false
		);

		if ( Cubix_Application::getId() == APP_ED || Cubix_Application::getId() == 33 || Cubix_Application::getId() == 22 ) {
			$sql['fields'] = array_merge($sql['fields'], array(
				'ep.cuisine',
				'ep.drink',
				'ep.flower',
				'ep.gifts',
				'ep.perfume',
				'ep.designer',
				'ep.interests',
				'ep.sports',
				'ep.hobbies',
				
				'ep.min_book_time',
				'ep.min_book_time_unit',
				
				'ep.has_down_pay',
				'ep.down_pay',
				'ep.down_pay_cur',
				
				'ep.reservation'
			));
		}

        if ( Cubix_Application::getId() == APP_BL ){
            $sql['fields'] = array_merge($sql['fields'], array(
                'e.is_pornstar'
            ));
        }
		
		if( Cubix_Application::getId() == APP_ED ){
			$sql['fields'] = array_merge($sql['fields'], array(
                'ep.travel_place',
                'ep.prefered_contact_methods',
				'e.type'
            ));
		}
		
		if ( Cubix_Application::getId() == APP_6A || Cubix_Application::getId() == APP_EF){
			$sql['fields'] = array_merge($sql['fields'], array( Cubix_I18n::getTblFields('ep.about_soft', null, null, 'about_soft')) );

			if(Cubix_Application::getId() == APP_EF){
				$sql['fields'] = array_merge($sql['fields'], array(
					'ep.escortbook_ad'
				));	
			}
		}
		
		if ( Cubix_Application::getId() == APP_A6 ) {
			$sql['fields'] = array_merge($sql['fields'], array(
				'ep.show_phone, ep.skype_id'
			));	
		}
		
		$dataSql = Cubix_Model::getSql($sql);

		$profile = $this->_adapter->fetchRow($dataSql);

		$db = $this->_adapter;

		$profile->langs = $db->fetchAll('
			SELECT el.lang_id AS id, el.level, l.title FROM escort_langs el
			LEFT JOIN langs l ON l.id = el.lang_id
			WHERE el.escort_id = ?
		', $this->getId());

		$profile->rates = $db->fetchAll('
			SELECT availability, time, time_unit, price, plus_taxi, currency_id, type FROM escort_rates
			WHERE escort_id = ?
		', $this->getId());
		
		if ( Cubix_Application::getId() == APP_ED || Cubix_Application::getId() == 33 || Cubix_Application::getId() == 22 ) {
			$profile->travel_payment_methods = $db->fetchAll('
				SELECT payment_method, specify FROM travel_payment_methods
				WHERE escort_id = ?
			', $this->getId());
		}

		$profile->tours = $db->fetchAll('
			SELECT t.id, t.date_from, t.date_to, t.phone, t.country_id,
				t.city_id, c.' . Cubix_I18n::getTblField('title') . ' AS country_title,
				ct.' . Cubix_I18n::getTblField('title') . ' AS city_title,
				c.slug AS country_slug, r.slug AS region_slug, ct.slug AS city_slug,
				c.iso AS country_iso,
				IF(DATE(t.date_from) <= DATE(NOW()) AND DATE(t.date_to) >= DATE(NOW()), 1, 0) AS is_in_tour,
				IF(DATE(t.date_from) > DATE(NOW()), 1, 0) AS is_upcoming_tour
			FROM tours t

			INNER JOIN countries c ON c.id = t.country_id
			INNER JOIN cities ct ON ct.id = t.city_id
			LEFT JOIN regions r ON r.id = ct.region_id
			WHERE t.escort_id = ? AND (DATE(t.date_from) >= DATE(NOW()) OR  DATE(t.date_from) <= DATE(NOW()) AND DATE(t.date_to) >= DATE(NOW()))
			ORDER BY t.date_from ASC
		', $this->getId());

		$profile->is_open = $db->fetchOne('
			SELECT
				IF ((
					ewt.day_index = ' . date('N') . '
					AND ewt.time_from <= ' . date('G') . '
					AND ewt.time_to >= ' . date('G') . '
				) OR (
					ewt.day_index = ' . date('N') . '
					AND ewt.time_from <= ' . date('G') . '
					AND ewt.time_from > ewt.time_to
				) OR (
					ewt.day_index = ' . ( 1 == date('N') ? 7 : date('N') - 1 ) . '
					AND ewt.time_to >= ' . date('G') . '
					AND ewt.time_from  > ewt.time_to
				), 1, 0)
			FROM escort_working_times ewt
			WHERE ewt.escort_id = ? AND ewt.day_index = ?
		', array($this->getId(), date('N')));

		$working_times = $db->query('
			SELECT ewt.day_index, ewt.time_from AS `from`, ewt.time_from_m AS `from_m`, ewt.time_to AS `to`, ewt.time_to_m AS `to_m`
			FROM escort_working_times ewt
			WHERE ewt.escort_id = ?
		', $this->getId())->fetchAll();

		$wts = array();
		foreach ( $working_times as $i => $wt ) {
			$wts[$wt->day_index] = $wt;
		}

		$profile->working_times = $wts;
		
		if ( $profile->birth_date ) {
			$profile->birth_date = strtotime($profile->birth_date);
		}
		
		return $profile;
	}

	public function snapshotProfileV2()
	{		
		set_time_limit(0);

		$has_old = $this->_adapter->fetchOne('SELECT TRUE FROM escort_profiles_snapshots_v2 WHERE escort_id = ?', array($this->getId()));
		
		//print_r($this->getFullProfileV2());die;
		$data = array('data' => serialize($this->getFullProfileV2()));

		if ( ! $has_old ) {
			$this->_adapter->insert('escort_profiles_snapshots_v2', $data + array('escort_id' => $this->getId()));
		}
		else {
			$this->_adapter->update('escort_profiles_snapshots_v2', $data, array($this->_adapter->quoteInto('escort_id = ?', $this->getId())));
		}
	}

	public function getNoSnapshotEscorts()
	{
		$escorts = $this->_adapter->query('
			SELECT e.id FROM escorts e
			INNER JOIN escort_profiles_v2 eprof ON eprof.escort_id = e.id
			LEFT JOIN escort_profiles_snapshots_v2 es ON es.escort_id = e.id
			WHERE es.escort_id IS NULL
		')->fetchAll();

		$c = 0;
		foreach ($escorts as $escort) {
			$this->id = $escort->id;
			$this->snapshotProfileV2();
			$c++;
		}

		echo $c . " escort snapshots added.";
	}
}