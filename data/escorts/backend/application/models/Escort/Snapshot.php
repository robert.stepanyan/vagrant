<?php

class Model_Escort_Snapshot extends Cubix_Model_Item
{
	public function __construct($rowData = null)
	{
		if ( !is_null($rowData) ) {
			parent::__construct($rowData);
		}
		else {
			parent::__construct();
		}
	}

	public function getFullProfile($lang_id = 'en')
	{
		Zend_Controller_Front::getInstance()->getRequest()->setParam('lang_id', $lang_id);

		$sql = array(
			'tables' => array('escort_profiles ep'),
			'fields' => array(
				/* General */
				'e.id', 'e.agency_id', 'e.showname', 'ct.' . Cubix_I18n::getTblField('title') . ' AS city_title', 'eph.hash AS photo_hash',
				'eph.ext AS photo_ext', 'u.application_id', 'ep.contact_email', 'u.username', 'eph.status AS photo_status','e.disabled_reviews',

				/* Contacts Section */
				'ct.' . Cubix_I18n::getTblField('title') . ' AS base_city', 'ct.id AS base_city_id', 'c.' . Cubix_I18n::getTblField('title') . ' AS country',
				'c.slug AS country_slug', 'c.iso AS country_iso', 'r.slug AS region_slug', 'r.' . Cubix_I18n::getTblField('title') . ' AS region_title', 'ct.slug AS city_slug',
				'IF(((DATE(ep.vac_date_from) <= CURDATE() AND DATE(ep.vac_date_to) > CURDATE()) OR DATE(ep.vac_date_from) > CURDATE()), ep.vac_date_to, 0) AS vac_date_to',
				'ep.contact_phone', 'ep.phone_instructions', 'ep.contact_web', 'ep.contact_email', 'ep.contact_zip',

				/* About Section */
				'FLOOR(DATEDIFF(CURDATE(), ep.birth_date) / 365.25) as age', 'n.iso AS nationality_iso', 'n.' . Cubix_I18n::getTblField('title') . ' AS nationality_title',
				'ep.eye_color', 'ep.hair_color', 'ep.hair_length', 'ep.height', 'ep.weight', 'ep.bust', 'ep.waist', 'ep.hip', 'ep.shoe_size', 'ep.breast_size', 'ep.dress_size',
				'ep.is_smoker', 'ep.measure_units', 'ep.availability', 'ep.sex_availability', 'ep.ethnicity', 'ep.' . Cubix_I18n::getTblField('about') . ' AS about',
				'ep.characteristics',

				/* SetcardInfo Section */
				'e.date_last_modified', 'e.date_registered', '(SELECT SUM(count) FROM escort_hits_daily WHERE escort_id = e.id AND DATE_ADD(date, INTERVAL 90 DAY) >= DATE(NOW())) AS hit_count',

				/* Services Section */
				'ep.svc_kissing', 'ep.svc_blowjob', 'ep.svc_cumshot', 'ep.svc_69', 'ep.svc_anal', 'ep.' . Cubix_I18n::getTblField('svc_additional') . ' AS svc_additional'
			),
			'joins' => array(
				'INNER JOIN escorts e ON ep.escort_id = e.id',
				'INNER JOIN users u ON u.id = e.user_id',
				'INNER JOIN applications a ON a.id = u.application_id',

				'LEFT JOIN nationalities n ON n.id = ep.nationality_id',

				'LEFT JOIN escort_working_times ewt ON ewt.escort_id = e.id',
				'LEFT JOIN escort_langs el ON el.escort_id = e.id',
				'INNER JOIN escort_cities ec ON ec.escort_id = e.id',

				'INNER JOIN countries c ON c.id = e.country_id',
				'INNER JOIN cities ct ON ct.id = ec.city_id',
				'LEFT JOIN regions r ON r.id = ct.region_id',
				'LEFT JOIN cityzones cz ON cz.id = e.cityzone_id',

				'INNER JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1',

				'LEFT JOIN escort_hits_daily ehd ON ehd.escort_id = e.id AND DATE_ADD(ehd.date, INTERVAL 90 DAY) >= DATE(NOW())'
			),
			'where' => array(
				 'e.id = ?' => $this->getId()
			),
			'group' => 'e.id',
			'order' => false,
			'page' => false
		);

		$dataSql = Cubix_Model::getSql($sql);

		$profile = $this->_adapter->fetchRow($dataSql);

		$db = $this->_adapter;
/*foreach ( $profiles as $profile ) {
	$this->setId($profile->id);*/
		$profile->langs = $db->fetchAll('
			SELECT el.lang_id AS id, el.level, l.title FROM escort_langs el
			LEFT JOIN langs l ON l.id = el.lang_id
			WHERE el.escort_id = ?
		', $this->getId());

		$profile->rates = $db->fetchAll('
			SELECT availability, time, time_unit, price, currency_id FROM escort_rates
			WHERE escort_id = ?
		', $this->getId());

		$profile->tours = $db->fetchAll('
			SELECT t.id, t.date_from, t.date_to, t.phone, t.country_id,
				t.city_id, c.' . Cubix_I18n::getTblField('title') . ' AS country_title,
				ct.' . Cubix_I18n::getTblField('title') . ' AS city_title,
				c.slug AS country_slug, r.slug AS region_slug, ct.slug AS city_slug,
				c.iso AS country_iso,
				IF(DATE(t.date_from) <= DATE(NOW()) AND DATE(t.date_to) >= DATE(NOW()), 1, 0) AS is_in_tour,
				IF(DATE(t.date_from) > DATE(NOW()), 1, 0) AS is_upcoming_tour
			FROM tours t

			INNER JOIN countries c ON c.id = t.country_id
			INNER JOIN cities ct ON ct.id = t.city_id
			INNER JOIN regions r ON r.id = ct.region_id
			WHERE t.escort_id = ? AND (DATE(t.date_from) >= DATE(NOW()) OR  DATE(t.date_from) <= DATE(NOW()) AND DATE(t.date_to) >= DATE(NOW()))
			ORDER BY t.date_from ASC
		', $this->getId());

		$profile->is_open = $db->fetchOne('
			SELECT
				IF ((
					ewt.day_index = ' . date('N') . '
					AND ewt.time_from <= ' . date('G') . '
					AND ewt.time_to >= ' . date('G') . '
				) OR (
					ewt.day_index = ' . date('N') . '
					AND ewt.time_from <= ' . date('G') . '
					AND ewt.time_from > ewt.time_to
				) OR (
					ewt.day_index = ' . ( 1 == date('N') ? 7 : date('N') - 1 ) . '
					AND ewt.time_to >= ' . date('G') . '
					AND ewt.time_from  > ewt.time_to
				), 1, 0)
			FROM escort_working_times ewt
			WHERE ewt.escort_id = ? AND ewt.day_index = ?
		', array($this->getId(), date('N')));

		$working_times = $db->query('
			SELECT ewt.day_index, ewt.time_from AS `from`, ewt.time_to AS `to`
			FROM escort_working_times ewt
			WHERE ewt.escort_id = ?
		', $this->getId())->fetchAll();

		$wts = array();
		foreach ( $working_times as $i => $wt ) {
			$wts[$wt->day_index] = $wt;
		}

		$profile->working_times = $wts;

		/*$db->insert('escort_profiles_snapshots', array('escort_id' => $profile->id, 'data' => serialize($profile), 'lang_id' => $lang_id));
}*/
		return $profile;
	}

	public function snapshotProfile()
	{
		//$this->setId($escort_id);
		set_time_limit(0);
		$app_id = $this->_adapter->fetchOne('SELECT u.application_id FROM users u INNER JOIN escorts e ON e.user_id = u.id WHERE e.id = ?', $this->getId());
		$langs = Cubix_I18n::getLangsByAppId($app_id);

		foreach ( $langs as $lang ) {
			$has_old = $this->_adapter->fetchOne('SELECT TRUE FROM escort_profiles_snapshots WHERE escort_id = ? AND lang_id = ?', array($this->getId(), $lang->lang_id));

			$data = array('data' => serialize($this->getFullProfile($lang->lang_id)));

			if ( ! $has_old ) {
				$this->_adapter->insert('escort_profiles_snapshots', $data + array('escort_id' => $this->getId(), 'lang_id' => $lang->lang_id));
			}
			else {
				$this->_adapter->update('escort_profiles_snapshots', $data, array($this->_adapter->quoteInto('escort_id = ?', $this->getId()), $this->_adapter->quoteInto('lang_id = ?', $lang->lang_id)));
			}
		}
	}

	public function getNoSnapshotEscorts()
	{
		$escorts = $this->_adapter->query('
			SELECT e.id FROM escorts e
			LEFT JOIN escort_profiles_snapshots es ON es.escort_id = e.id
			WHERE es.escort_id IS NULL
		')->fetchAll();

		$c = 0;
		foreach ($escorts as $escort) {
			$this->id = $escort->id;
			$this->snapshotProfile();
			$c++;
		}

		echo $c . " escort snapshots added.";
	}
}
