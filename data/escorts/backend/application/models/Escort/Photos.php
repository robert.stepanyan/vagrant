<?php

class Model_Escort_Photos extends Cubix_Model
{
	protected $_table = 'escort_photos';
	protected $_itemClass = 'Model_Escort_PhotoItem';
	
	const STATUS_VERIFIED = 1;
	const STATUS_NOT_VERIFIED = 2;
	const STATUS_NORMAL = 3;
	const STATUS_REMOVED = 4;
	
	const VERIFIED = 1;
	const NOT_VERIFIED = 2;
	const NORMAL = 3;
	const PHOTOSHOP = 8;
	
	const PHOTO_ROTATE_ALL = 1;
	const PHOTO_ROTATE_SELECTED = 2;
	const PHOTO_ROTATE_SELECTED_ONE = 3;
	
	const MAX_PHOTOS_COUNT = 30;

    /**
     * @param $id
     * @return array|null|Model_Escort_PhotoItem
     */
	public function get($id)
	{
		return $this->_fetchRow('
			SELECT u.application_id, ep.* FROM escort_photos ep
			INNER JOIN escorts e ON e.id = ep.escort_id
			INNER JOIN users u ON u.id = e.user_id
			WHERE ep.id = ?
		', $id);
	}

	public function gethistoryphoto($hash)
	{
		return $this->_fetchRow('
			SELECT * FROM escort_photo_history WHERE hash = ?
		', $hash);
	}
	public function removehistoryphoto($hash)
	{
		$result = self::getAdapter()->update('escort_photo_history', array('hash' => '', 'date' => new Zend_Db_Expr('NOW()')), parent::quote('hash = ?', $hash));
		return $result;
	}
	
	public function save(Model_Escort_PhotoItem $item)
	{
		$max = intval(self::getAdapter()->fetchOne('
			SELECT MAX(ordering) FROM escort_photos WHERE escort_id = ?' . 
			( $item->type == ESCORT_PHOTO_TYPE_PRIVATE ? ' AND type = ' . ESCORT_PHOTO_TYPE_PRIVATE : ' AND type <> ' . ESCORT_PHOTO_TYPE_PRIVATE ) . '
		', $item->escort_id));
		
		if ( ! $max ) $max = 0;
		$max++;
		
		$item->ordering = $max;
		$item->double_approved = 1;
		if ( 0 == self::getAdapter()->fetchOne('SELECT COUNT(id) FROM escort_photos WHERE escort_id = ? AND is_main = 1', $item->getEscortId()) ) {
			$item->is_main = 1;
		}
		
		parent::save($item);
		
		$id = self::getAdapter()->lastInsertId();
		
		return $this->get($id);
	}
	
	public function update($photo_id, $data)
	{
		self::getAdapter()->update('escort_photos', $data, parent::quote('id = ?', $photo_id));
		
		return $this->get($photo_id);
	}
	
	public function remove($ids)
	{
		$db = self::getAdapter();
		
		if ( ! is_array($ids) ) {
			$ids = array($ids);
		}
		
		$photos = $db->query('SELECT * FROM escort_photos WHERE id IN (' . implode(', ', $ids) . ')')->fetchAll();
		
		$escort_id = $db->query('SELECT escort_id FROM escort_photos WHERE id = ?', array($ids[0]))->fetch();
		
		$is_main = false;
		foreach ($photos as $photo) {
			if ($photo->is_main) {
				$is_main = true;
			}
			
			$db->insert('escort_photo_history', array(
				'escort_id' => $photo->escort_id,
				'hash' => $photo->hash,
				'ext' => $photo->ext,
				'date' => new Zend_Db_Expr('NOW()'),
				'from_back' => 1
			));
		}		
		
		foreach ( $ids as $id ) 
		{	
			parent::remove(self::quote('id = ?', $id));
			//parent::getAdapter()->update('escort_photos', array('status' => self::STATUS_REMOVED), parent::quote('id = ?', $id));
		}
		
		if ( $is_main ) {
            $db->query('UPDATE escort_photos SET is_main = 1 WHERE escort_id = ? AND type = 1 ORDER BY ordering ASC LIMIT 1', array($escort_id->escort_id));
        }
	}

	public function reset($escort_id)
	{
		$db = self::getAdapter();
		if ( $escort_id ) {
			$has_notVerified_public_img = $db->fetchOne('SELECT TRUE FROM escort_photos WHERE escort_id = ? AND ( type = '.ESCORT_PHOTO_TYPE_SOFT.' OR type = '.ESCORT_PHOTO_TYPE_HARD.' ) AND status <> 1', $escort_id);
			$has_verified_public =  $db->fetchOne('SELECT TRUE FROM escort_photos WHERE escort_id = ? AND ( type = '.ESCORT_PHOTO_TYPE_SOFT.' OR type = '.ESCORT_PHOTO_TYPE_HARD.' ) AND status = 1', $escort_id);
			if ( !$has_notVerified_public_img && $has_verified_public) {
				$db->update('escorts', array('verified_status' => Model_Verifications_Requests::VERIFY, 'is_suspicious' => 0), $db->quoteInto('id = ?', $escort_id));
			}
			else {
				$db->update('escorts', array('verified_status' => Model_Verifications_Requests::PENDING), $db->quoteInto('id = ?', $escort_id));
			}
		}
	}

	public function doubleApprove($ids)
	{
		$db = self::getAdapter();

		if ( ! is_array($ids) ) {
			$ids = array($ids);
		}

		$photos = $db->query('UPDATE escort_photos SET double_approved = 1 WHERE id IN (' . implode(', ', $ids) . ')');

	}
	
	public function getEscortPhotoCount($escort_id){
		
		$db = self::getAdapter();
		return $db->fetchOne('SELECT COUNT(id) FROM escort_photos WHERE escort_id = ?', array($escort_id));
		
	}
	
	public function getRotatablePicsByEscortId($escort_id)
	{
		$photos = array();
		$not_allowed_types = implode(',', array(ESCORT_PHOTO_TYPE_PRIVATE , ESCORT_PHOTO_TYPE_DISABLED, ESCORT_PHOTO_TYPE_ARCHIVED) );
		$results = self::getAdapter()->fetchAll('
			SELECT SQL_CALC_FOUND_ROWS id , is_main FROM escort_photos WHERE is_rotatable = 1 AND is_approved = 1 
			AND status <> ? AND type NOT IN ('. $not_allowed_types. ') AND retouch_status <> ? AND escort_id = ? ORDER BY id
		', array(self::STATUS_NOT_VERIFIED, Model_RetouchPhotos::NEEDS_RETOUCH, $escort_id));
		
		if ( ! $results ) return null;
		
		$count = parent::getAdapter()->fetchOne('SELECT FOUND_ROWS()');
		
		$photos['results'] = $results;
		$photos['count'] = $count;
		
		return $photos;
	}
	
	public function clearRotatable($escort_id)
	{
		self::getAdapter()->query('
			UPDATE escort_photos SET is_rotatable = 0 WHERE escort_id = ?
		', $escort_id);
	}
	
	public function clearVerification($escort_id)
	{
		self::getAdapter()->query('
			UPDATE escort_photos SET status = ? WHERE escort_id = ?
		', array( self::STATUS_NORMAL, $escort_id));
	}
	
	public function getPhotosIds($escort_id)
	{
		$db = self::getAdapter();
		return $db->fetchAll("SELECT id FROM escort_photos WHERE escort_id = ?",$escort_id); 
	}
	
	public function setSuspicous($escort_id, $susp)
	{
		self::getAdapter()->query('
			UPDATE escort_photos SET is_suspicious = ? WHERE escort_id = ?
		', array($susp, $escort_id));
	}
	
	public function unsetSuspicous($escort_id)
	{
		self::getAdapter()->query('
			UPDATE escort_photos SET is_suspicious = 0 WHERE escort_id = ?
		', array($escort_id));
	}		
	
	public function hasMain($escort_id)
	{
		return self::getAdapter()->fetchOne('SELECT TRUE FROM escort_photos WHERE (type = ? OR type = ?) AND escort_id = ? AND is_main = 1', array(ESCORT_PHOTO_TYPE_HARD, ESCORT_PHOTO_TYPE_SOFT, $escort_id));
	}
	
	public function setRandomMain($escort_id)
	{
		self::getAdapter()->query('
			UPDATE escort_photos SET is_main = 1 WHERE (type = ? OR type = ?) AND escort_id = ? ORDER BY ordering ASC LIMIT 1
		', array(ESCORT_PHOTO_TYPE_HARD, ESCORT_PHOTO_TYPE_SOFT, $escort_id));
	}
	
	public function unsetPopunder($escort_id, $photo_id)
	{
		$db = self::getAdapter();
		$db->update('escort_photos', array('is_popunder' => 0), $db->quoteInto('id = ?', $photo_id));
		$has_popunder = $db->fetchOne('SELECT TRUE FROM escort_photos WHERE escort_id = ? AND is_popunder = 1', array($escort_id));
		if(!$has_popunder){
			$db->update('escorts', array('has_large_popunder' => 0, 'is_suspicious' => 0), $db->quoteInto('id = ?', $escort_id));
		}
	}
	
	public function getNotApprovedPhotos($escort_id)
	{
		$db = self::getAdapter();
		$photos = $db->fetchAll(" SELECT id FROM escort_photos WHERE is_approved = 0 AND escort_id = ? ", array($escort_id));
		$array_ids = array();
		foreach($photos as $photo){
			$array_ids[] = $photo->id;
		}
		
		return $array_ids;
	}		
}
