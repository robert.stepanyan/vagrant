<?php

class Model_Escort_ProfileV2 extends Cubix_Model_Item
{
	protected $_idField = 'id';

	protected $_apiServer = "http://api.escortforum-v2.dev";
	//protected $_apiServer = "http://www2.6a.com";
	
	// Profile Revision System Block
	const REVISION_STATUS_NOT_APPROVED = 1;
	const REVISION_STATUS_APPROVED = 2;
	const REVISION_STATUS_DECLINED = 3;

	protected static $_revisions_statusMap = array(
		self::REVISION_STATUS_NOT_APPROVED => 'Not Approved',
		self::REVISION_STATUS_APPROVED => 'Approved',
		self::REVISION_STATUS_DECLINED => 'Declined',
	);

	public function __construct($rowData = null)
	{
		if ( ! is_null($rowData) ) {
			parent::__construct($rowData);
		}
		else {
			parent::__construct();
		}
	}

	public function getProfile()
	{
		$client = Cubix_Api::getInstance();

		//Cubix_Api_XmlRpc_Client::setApiKey(Cubix_Application::getById(Zend_Controller_Front::getInstance()->getRequest()->application_id)->api_key);
		//Cubix_Api_XmlRpc_Client::setServer($this->_apiServer);

		$params = array('id' => $this->getId());
		if ( ! is_null($this->getRevisionId()) ) {
			$params = array('id' => $this->getId(), 'revision_id' => $this->getRevisionId());
		}
		
		$data = $client->call('getEscortProfileData', $params);

		// var_dump($data);exit();
		if ( ROYAL_SYSTEM == Cubix_Application::getById()->measure_units ) {
			if ( isset($data['height']) && $data['height']) {
				$data['height'] = Cubix_UnitsConverter::convert($data['height'], 'cm', 'ftin');
			}

			if ( isset($data['weight']) && $data['weight']) {
				$data['weight'] = Cubix_UnitsConverter::convert($data['weight'], 'kg', 'lbs');
			}
		}

		/*foreach ( $data as $key => $value ) {
			$this->$key = $value;
		}*/

		return $data;
	}

	public function update($data, $revision_id = null)
	{
		$client = Cubix_Api::getInstance();
	
//		Cubix_Api_XmlRpc_Client::setApiKey(Cubix_Application::getById(Zend_Controller_Front::getInstance()->getRequest()->application_id)->api_key);
//		Cubix_Api_XmlRpc_Client::setServer($this->_apiServer);

		//$data['backend'] = true;
        if ( Zend_Auth::getInstance()->hasIdentity() ) {
            $bo_user = Zend_Auth::getInstance()->getIdentity();
            $data['backend_user_id'] = $bo_user->id;
        }
		$params = array($this->getId(), serialize($data));
		if ( ! is_null($revision_id) ) {
			$params = array($this->getId(), serialize($data), $revision_id);
		}
		
		$client->call('updateEscortProfileV2', $params);
		
	}

	public function getRevisions()
	{
		return $this->getAdapter()->query('
			SELECT revision, UNIX_TIMESTAMP(date_updated) AS date_updated, status
			FROM profile_updates_v2
			WHERE escort_id = ?
			ORDER BY revision DESC
		', $this->getId())->fetchAll();
	}

	public function getRevisionsStatus()
	{
		return $this->getAdapter()->query('
			SELECT status
			FROM profile_updates_v2
			WHERE escort_id = ?
			ORDER BY revision DESC
		', $this->getId())->fetchAll();
	}

	public function getRevision($revision)
	{
		$revision = $this->getAdapter()->fetchRow('
			SELECT revision, UNIX_TIMESTAMP(date_updated) AS date_updated, status, data
			FROM profile_updates_v2
			WHERE escort_id = ? AND revision = ?
			ORDER BY revision DESC
		', array($this->getId(), $revision));

		if ( ! $revision ) return null;

		$revision->data = unserialize($revision->data);

        if(Cubix_Application::getId() == APP_ED ){
            unset($revision->data['last_step']);
            unset($revision->data['apps_availble']);

            $cities = $revision->data['cities'];
            if (count($cities) > 0) {
                foreach ($cities as $key => $city) {
                    $city['escort_id'] = $this->getId();

                    if (Cubix_Application::getId() == APP_ED) {
                        if (is_numeric($city)) {
                            $city = array('city_id' => $city);
                            $cities[$key] = $city;
                        }
                    }
                }
            }
            $revision->data['cities'] = $cities;
        }

		return $revision;
	}

	public function getLatestRevision($status = null)
	{
		$bind = array($this->getId());
		
		$where = array();
		if ( ! is_null($status) ) {
			if ( ! is_array($status) ) {
				// $bind[] = $status;
				$where[] = 'status = ' . $status;
			}
			else {
				// $bind = array_merge($bind, $status);
				foreach ( $status as $s ) {
					$where[] = 'status = ' . $s;
				}
			}
		}
		
		return $this->getAdapter()->fetchOne('
			SELECT MAX(revision) FROM profile_updates_v2
			WHERE escort_id = ?' . ( ! is_null($status) ? ' AND (' . implode(' OR ', $where) . ') ' : '') . '
		', $bind);
	}

	public function saveRevision()
	{
		$latest_revision = $this->getLatestRevision(self::REVISION_STATUS_NOT_APPROVED);
		$id= $this->getId();
//var_dump($latest_revision);die;

		if ( $latest_revision ) {
			$profile_revision = $this->getRevision($latest_revision);

			$this->applyRevision($profile_revision, $latest_revision);

			$this->updateRevision($latest_revision, self::REVISION_STATUS_APPROVED);

			$s = new Cubix_EscortStatus($this->getId());
			$s->save();
		}
	}

	public static function getProfileStatusLabel($status)
	{
		return self::$_revisions_statusMap[$status];
	}

	public function getNotApprovedRevisions($app_id, $be_user)
	{
		$join = '';
		$where = '';
		if ( $be_user->type != 'superadmin' && $be_user->type != 'admin' && $be_user->type != 'dash manager' ) {
			$join .= 'INNER JOIN backend_users be ON be.id = u.sales_user_id';
			$where = " AND be.id = " . $be_user->id . " ";
		}
		else {
			$join .= 'LEFT JOIN backend_users be ON be.id = u.sales_user_id';
		}
		
		/*if ( $app_id == 16 ) {
			$where = " AND be.id <> " . Model_Dashboard::IVR_USER_ID . " ";
		}*/

		$sql = "
			SELECT 
				e.id, e.agency_id, e.showname, pu.id AS revision_id, pu.date_updated, be.username AS sales_name, e.country_id
			FROM escorts e
			INNER JOIN profile_updates_v2 pu ON pu.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id
			" . $join . "
			WHERE e.status IN(136,160,4232,4256,8320,8328,131072,131200,131232) 
			AND pu.status = ? AND u.application_id = ? ".$where."
			ORDER BY pu.date_updated DESC
		";
		
		//WHERE " . Model_Escort_ListV2::_whereStatus('e.status', Model_Escorts::ESCORT_STATUS_PROFILE_CHANGED, array(Model_Escorts::ESCORT_STATUS_IS_NEW, Model_Escorts::ESCORT_STATUS_DELETED)) . " AND pu.status = ? AND u.application_id = ? {$where} /*GROUP BY e.id*/ 
		
		$sqlCount = "
			SELECT COUNT(DISTINCT(pu.id)) AS rev_count FROM profile_updates_v2 pu
			INNER JOIN escorts e ON pu.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id
			" . $join . "
			WHERE e.status IN(136,160,4232,4256,8320,8328,131072,131200,131232) AND pu.status = ? AND u.application_id = ? ".$where."
		";

		//WHERE " . Model_Escort_ListV2::_whereStatus('e.status', Model_Escorts::ESCORT_STATUS_PROFILE_CHANGED, array(Model_Escorts::ESCORT_STATUS_IS_NEW, Model_Escorts::ESCORT_STATUS_DELETED)) . " AND pu.status = ? AND u.application_id = ? {$where}	
		$items = self::getAdapter()->fetchAll($sql, array(self::REVISION_STATUS_NOT_APPROVED, $app_id));
		$count = self::getAdapter()->fetchOne($sqlCount, array(self::REVISION_STATUS_NOT_APPROVED, $app_id));

		return array('count' => $count, 'data' => $items);
	}

	public function differProfile($old, $new)
	{
		$old = (array) $old; $new = (array) $new;

		$added = array(); $changed = array(); $removed = array();
		foreach ( $new as $field => $value ) {


            /*
             * SHOW Online Services in revision for And6, even if there are no changes
             * */
            if (in_array(Cubix_Application::getId(), array(APP_A6)) && $field == 'services')
            {
                $added[] = $field;
            }else{
                if ( ! $old[$field] && $value ) {
                    $added[] = $field;
                }
                elseif ( ! $value && $old[$field] ) {
                    $removed[] = $field;
                }
                elseif ( $old[$field] != $value ) {
                    $changed[] = $field;
                }
            }



		}

		return array('added' => $added, 'changed' => $changed, 'removed' => $removed);
	}

	public function updateRevision($revision, $status, $from = null)
	{
		if ( Model_Escort_ProfileV2::REVISION_STATUS_APPROVED == $status ) {
			Cubix_SyncNotifier::notify($this->getId(), Cubix_SyncNotifier::EVENT_ESCORT_REV_APPROVED, array('rev' => $revision));
			
			Cubix_LatestActions::addAction($this->getId(), Cubix_LatestActions::ACTION_PROFILE_UPDATE);
			Cubix_LatestActions::addActionDetail($this->getId(), Cubix_LatestActions::ACTION_PROFILE_UPDATE, $revision);
			
			if ($from == 'dashboard'){
				Cubix_AlertEvents::notify($this->getId(), ALERT_ME_MODIFICATION_ON_PROFILE, array('rev' => $revision));
				
				if(Cubix_Application::getId() == APP_ED ){
					Cubix_FollowEvents::notify($this->getId(), 'escort', FOLLOW_ESCORT_NEW_PROFILE , array('rel_id' => $this->getId()), true);
				}
			}
		}
		elseif ( Model_Escort_ProfileV2::REVISION_STATUS_DECLINED == $status ) {
			Cubix_SyncNotifier::notify($this->getId(), Cubix_SyncNotifier::EVENT_ESCORT_REV_DECLINED, array('rev' => $revision));
		}

		$this->getAdapter()->update('profile_updates_v2', array(
			'status' => $status
		), array(
			self::getAdapter()->quoteInto('escort_id = ?', $this->getId()),
			self::getAdapter()->quoteInto('revision = ?', $revision))
		);
	}

	public function applyRevision($data, $latest_revision)
	{


		if ( is_object($data) ) $data = (array) $data;
		if ( isset($data['data']) ) $data = $data['data'];

		if ( array_key_exists('slogan', $data) ) {
			unset($data['slogan']);
		}
		
		if ( array_key_exists('exp_date', $data) ) {
			unset($data['exp_date']);
		}

        if ( array_key_exists('bubble_text', $data) ) {
            unset($data['bubble_text']);
		}
		
		if(isset($data['backend_user_id'])){
		    unset($data['backend_user_id']);
        }

		if ( array_key_exists('is_pornstar', $data) ) {
			unset($data['is_pornstar']);
		}
		if ( array_key_exists('is_vaccinated', $data) ) {
			unset($data['is_vaccinated']);
		}
		
		if ( array_key_exists('bad_photo_history', $data) ) {
			unset($data['bad_photo_history']);
		}
		
		if ( array_key_exists('stays_verified', $data) ) {
			unset($data['stays_verified']);
		}
		
        if ( array_key_exists('photo_suspicious_date', $data) ) {
			unset($data['photo_suspicious_date']);
		}

		if ( array_key_exists('block_countries', $data) ){
			unset($data['block_countries']);
		}
		if ( array_key_exists('blocked_members', $data) ){
			unset($data['blocked_members']);
		}
		if ( array_key_exists('fake_city_id', $data) ){
			unset($data['fake_city_id']);
		}
		if ( array_key_exists('fake_zip', $data) ){
			unset($data['fake_zip']);
		}

		if ( array_key_exists('profile_type', $data) ){
			unset($data['profile_type']);
		}

		if ( array_key_exists('source_url', $data) ){
			unset($data['source_url']);
		}


		if ( array_key_exists('source_url_doesnt_have', $data) ){
			unset($data['source_url_doesnt_have']);
		}

		if ( Cubix_Application::getId() == APP_EF ) {
			unset($data['city_id']);
			unset($data['cities']);
		}

		/* get latitude and longitude */
		$country_title = self::getAdapter()->fetchOne('SELECT title_en FROM countries WHERE id = ?', array($data['country_id']));
		if ( Cubix_Application::getId() == APP_A6 ) {
			$fake_city_id = self::getAdapter()->fetchOne('SELECT fake_city_id FROM escorts WHERE id = ?', array($this->getId()));
			$city_title = self::getAdapter()->fetchOne('SELECT title_en FROM f_cities WHERE id = ?', array($fake_city_id));
			
			$address_data = array(
				$data['street_no'] . ' ' . $data['street'],
				$city_title,
				$country_title
			);
		} /*else {			
			$city_title = self::getAdapter()->fetchOne('SELECT title_en FROM cities WHERE id = ?', array($data['city_id']));
			
			$address_data = array(			
				$data['street_no'] . ' ' . $data['street'],
				$city_title,
				$country_title
			);
		}*/

        if (in_array(Cubix_Application::getId(), array(APP_A6, APP_EG_CO_UK))) {
    		$old_coords = self::getAdapter()->fetchRow('SELECT latitude, longitude FROM escorts WHERE id = ?', array($this->getId()));
        	if(isset($data['longitude']) && isset($data['latitude']) && strlen($data['longitude']) && strlen($data['latitude']) && $old_coords->latitude != $data['latitude'] && $old_coords->longitude != $data['longitude']){
        		$coord = array(
                    'latitude' => $data['latitude'],
                    'longitude' => $data['longitude']
                );
                self::getAdapter()->update('escorts', $coord, self::getAdapter()->quoteInto('id = ?', $this->getId()));
        	} else{

	            if (!empty($address_data)){
	                $location = str_replace (" ", "+", implode(',', $address_data));
	                $url = "http://open.mapquestapi.com/geocoding/v1/address?key=V33bo8GqPedNwMOUCrAvmnHzzyszaw83&location=".$location;
	                $url = preg_replace('# #', '+', $url);
	                $geo_data = file_get_contents($url);
	                if(!$geo_data){
	                	 // Cubix_Email::send('tigranyeng@gmail.com', 'mapquestapi expired V33bo8GqPedNwMOUCrAvmnHzzyszaw83', 'mapquestapi expired V33bo8GqPedNwMOUCrAvmnHzzyszaw83');
	                	$url = "http://open.mapquestapi.com/geocoding/v1/address?key=2IUAiWs5TEXGwOPGlGoPe9fnpaIniyFZ&location=" . $location;
		                $url = preg_replace('# #', '+', $url);
		                $geo_data = file_get_contents($url);
	                }
	                if($geo_data){
		                $geo_data = json_decode($geo_data);
		                if(strpos($geo_data->results[0]->locations[0]->latLng->lng, '-') !== false){
		                	$city_coords = self::getAdapter()->fetchRow('SELECT latitude, longitude FROM f_cities WHERE id = ?', array($fake_city_id));
		                	$coord = array(
			                    'latitude' => $city_coords->latitude,
			                    'longitude' => $city_coords->longitude
			                );
		                }else{
			                $coord = array(
			                    'latitude' => $geo_data->results[0]->locations[0]->latLng->lat,
			                    'longitude' => $geo_data->results[0]->locations[0]->latLng->lng
			                );
		                }
		                self::getAdapter()->update('escorts', $coord, self::getAdapter()->quoteInto('id = ?', $this->getId()));
	                }else{
	                	Cubix_Email::send('tigranyeng@gmail.com', 'backend mapquestapi 2 expired 2IUAiWs5TEXGwOPGlGoPe9fnpaIniyFZ', 'mapquestapi expired 2IUAiWs5TEXGwOPGlGoPe9fnpaIniyFZ ' . $url);
	                }
	            }
        	}
            unset($data['longitude']);
            unset($data['latitude']);
        }

        /* get latitude and longitude */


		$escort_data_array = array(
			'showname' => $data['showname'],
			'gender' => $data['gender'],
			'country_id' => $data['country_id'],
			'city_id' => $data['city_id'],
			'home_city_id' => $data['home_city_id'],
			'free_comment' => $data['free_comment'],
			'status_comment' => $data['status_comment'],
			'date_last_modified' => new Zend_Db_Expr('NOW()'),
			'due_date' => ($data['due_date']) ? new Zend_Db_Expr('FROM_UNIXTIME(' . $data['due_date'] . ')') : null
		);
		if ( Cubix_Application::getId() == APP_ED ) {
			$escort_data_array['type'] = $data['type'];
		}

		$escort_data = $escort_data_array ;

		if ( Cubix_Application::getId() == APP_EF ) {
			unset($escort_data['city_id']);
		}

		/*if ( isset($data['pseudo_escort']) && $data['pseudo_escort'] ) {
			$escort_data['pseudo_escort'] = $data['pseudo_escort'];
			unset($data['pseudo_escort']);
		}*/
		
		/*if ( $item->data['user_id'] ) {
			$data = array_merge($data, array('user_id' => $item->data['user_id']));
		}

		if ( $item->data['agency_id'] ) {
			$data = array_merge($data, array('agency_id' => $item->data['agency_id']));
		}*/
		$m_escort = new Model_EscortsV2();
		/*$showname_is_exists = $m_escort->existsByShownameId($data['showname'], $this->getId());

		if ( $showname_is_exists ) {
			$data['showname'] = '_' . $data['showname'];
			$escort_data['showname'] = $data['showname'];
		}*/
		
		$escort_data['showname'] = $data['showname'];
		$agency_id = $data['agency_id'];
		$old_rates = self::getAdapter()->fetchAll('SELECT availability, time, time_unit, price, currency_id, escort_id, type FROM escort_rates WHERE escort_id = ?', $this->getId(), Zend_Db::FETCH_ASSOC);

		foreach ( $data['rates'] as $k => $rate ) {
			if ( ! isset($rate['type']) ) {
				$data['rates'][$k]['type'] = null;
			}

			if ( ! isset($rate['time']) ) {
				$data['rates'][$k]['time'] = 0;
			}

			if ( ! isset($rate['time_unit']) ) {
				$data['rates'][$k]['time_unit'] = 2;
			}
		}

		$rate_changed = false;
		if ( count($old_rates) != count($data['rates']) ) {
			$rate_changed = true;
		}
		else {
			if ( count($data['rates']) > 0 ) {
				foreach ( $data['rates'] as $k => $new_rate ) {
					if ( $old_rates[$k] != $new_rate ) {
						$rate_changed = true;
					}
				}
			}
		}


		if ( $rate_changed ) {
			self::getAdapter()->delete('escort_rates', self::getAdapter()->quoteInto('escort_id = ?', $this->getId()));
			// Inserting escort rates
			$rates = $data['rates'];
			if ( count($rates) > 0 ) {
				foreach( $rates as $rate ) {
					$rate['escort_id'] = $this->getId();
					self::getAdapter()->insert('escort_rates', $rate);
				}
			}
			
			if(Cubix_Application::getId() == APP_ED ){
				Cubix_FollowEvents::notify($this->getId(), 'escort', FOLLOW_ESCORT_NEW_PRICE, array('rel_id' => $this->getId()) , true);
			}
			$this->resetHappyHour();
		}

		unset($data['rates']);

		if ( Cubix_Application::getId() == APP_ED || Cubix_Application::getId() == 33 || Cubix_Application::getId() == 22 ) {
			self::getAdapter()->delete('travel_payment_methods', self::getAdapter()->quoteInto('escort_id = ?', $this->getId()));
			
			$travel_payment_methods = $data['travel_payment_methods'];
			$travel_other_method = $data['travel_other_method'];
			
			if ( count($travel_payment_methods[0]) ) {
				foreach ( $travel_payment_methods[0] as $method ) {
					$d = array('escort_id' => $this->getId(), 'payment_method' => $method);
					if ( $method == PM_OTHER ) {
						$d['specify'] = $travel_other_method[0];
					}				
					self::getAdapter()->insert('travel_payment_methods', $d);
				}
			}
		}

		unset($data['travel_payment_methods']);
		unset($data['travel_other_method']);

		self::getAdapter()->update('escorts', $escort_data, self::getAdapter()->quoteInto('id = ?', $this->getId()));
		
		unset($data['free_comment']);
		unset($data['agency_id']);
		unset($data['user_id']);
		unset($data['status_comment']);

		self::getAdapter()->delete('escort_langs', self::getAdapter()->quoteInto('escort_id = ?', $this->getId()));
		// Inserting escort langs

		$langs = $data['langs'];
		if ( count($langs) > 0 ) {
			foreach( $langs as $lang ) {
				$lang['escort_id'] = $this->getId();
				self::getAdapter()->insert('escort_langs', $lang);
			}
		}
		unset($data['langs']);



		if ( Cubix_Application::getId() != APP_EF  ) {
			$cc = self::getAdapter()->fetchAll('SELECT city_id FROM escort_cities WHERE escort_id = ?', $this->getId());
			$old_cities = array();
			if ($cc)
				foreach ($cc as $c)
					$old_cities[] = $c->city_id;
			
			self::getAdapter()->delete('escort_cities', self::getAdapter()->quoteInto('escort_id = ?', $this->getId()));
			// Inserting escort cities
			$cities = $data['cities'];
			if ( count($cities) > 0 ) {
				foreach( $cities as $city ) {
					$city['escort_id'] = $this->getId();

                    if ( Cubix_Application::getId() == APP_ED  ) {
                        if (is_numeric($city)) {
                            $city = array('city_id' => $city);
                        }
                    }

                    self::getAdapter()->insert('escort_cities', $city);

					if (!in_array($city['city_id'], $old_cities))
					{
						Cubix_AlertEvents::notify($this->getId(), ALERT_ME_CITY, array('id' => $city['city_id'], 'type' => 'working'));
						Cubix_CityAlertEvents::notify($city['city_id'], $this->getId(), NULL);
						if(Cubix_Application::getId() == APP_ED ){
							if($agency_id){
								Cubix_FollowEvents::notify($agency_id, 'agency', FOLLOW_AGENCY_NEW_CITY, array('rel_id' => $city['city_id'], 'rel_id2' => FOLLOW_NEW_CITY_CITY, 'rel_id3' => $this->getId() ), true);
							}
							Cubix_FollowEvents::notify($this->getId(), 'escort', FOLLOW_ESCORT_NEW_CITY, array('rel_id' => $city['city_id'], 'rel_id2' => FOLLOW_NEW_CITY_CITY) , true);
						}
					}
				}
			}
		}


		unset($data['cities']);

		if ( Cubix_Application::getId() != APP_EF  ) {
			self::getAdapter()->delete('escort_cityzones', self::getAdapter()->quoteInto('escort_id = ?', $this->getId()));
			// Inserting escort cityzones
			$zones = $data['cityzones'];
			if ( count($zones) > 0 ) {
				foreach( $zones as $zone ) {
					$zone['escort_id'] = $this->getId();
					self::getAdapter()->insert('escort_cityzones', $zone);
				}
			}
		}
		unset($data['cityzones']);

		self::getAdapter()->delete('escort_services', self::getAdapter()->quoteInto('escort_id = ?', $this->getId()));
		// Inserting escort services
		$services = $data['services'];
		if ( count($services) > 0 ) {
			foreach( $services as $service ) {
				$service['escort_id'] = $this->getId();
				self::getAdapter()->insert('escort_services', $service);
			}
		}
		unset($data['services']);


		self::getAdapter()->delete('escort_keywords', self::getAdapter()->quoteInto('escort_id = ?', $this->getId()));
		// Inserting escort keywords
		$keywords = $data['keywords'];
		
		if ( count($keywords) > 0 ) {
			foreach( $keywords as $keyword ) {

			    if(Cubix_Application::getId() == APP_ED) {
			        if(isset($keyword['keyword_id'])) $keyword = $keyword['keyword_id'];
                }

				$k = array('escort_id' => $this->getId(), 'keyword_id' => $keyword);
				self::getAdapter()->insert('escort_keywords', $k);
			}
		}
		unset($data['keywords']);


		self::getAdapter()->delete('escort_working_times', self::getAdapter()->quoteInto('escort_id = ?', $this->getId()));
		// Inserting escort working times
		$times = $data['times'];
		if ( count($times) > 0 ) {
			foreach( $times as $time ) 
			{
				$time['escort_id'] = $this->getId();
				$time['time_from_m'] = intval($time['time_from_m']);
				$time['time_to_m'] = intval($time['time_to_m']);
				self::getAdapter()->insert('escort_working_times', $time);
			}
		}


		unset($data['times']);
		$data['escort_id'] = $this->getId();
		unset($data['id']);
		unset($data['night_escorts']);
		//$data['birth_date'] = strtotime('-' . $data['birth_date'] . ' year');

		$bd = is_numeric($data['birth_date']) ? date('Y-m-d', $data['birth_date']) : $data['birth_date'];

		$data['birth_date'] = $bd ? $bd/*new Zend_Db_Expr('FROM_UNIXTIME(' . $data['birth_date'] . ')')*/ : null;

		unset($data['phone']);
		unset($data['hh_motto']);

		unset($data['phone_prefix']);
		unset($data['phone_prefix_1']);
		unset($data['phone_prefix_2']);
		unset($data['country_id']);
		unset($data['city_id']);
		unset($data['home_city_id']);
		unset($data['service_currencies']);
		unset($data['due_date']);
		unset($data['vac_date_to']);
		unset($data['vac_date_from']);
		unset($data['type']);
		if ( Cubix_Application::getId() == APP_EG_CO_UK )
        {
            unset($data['old_city_id']);
            unset($data['old_cities_ids']);
        }

		// temporary solution 
		if ( Cubix_Application::getId() == APP_ED ) {
			unset($data['sync_escort']);
		}

		if ( self::getAdapter()->fetchOne('SELECT TRUE FROM escort_profiles_v2 WHERE escort_id = ?', $this->getId()) ) {
			$data['escort_id'] = $this->getId();
			//print_r($data); die;			
			self::getAdapter()->update('escort_profiles_v2', $data, self::getAdapter()->quoteInto('escort_id = ?', $this->getId()));
		}
		else {
			self::getAdapter()->insert('escort_profiles_v2', $data);
		}

		unset($data['admin_verified']);
		unset($data['about_has_bl']);
		$m_snapshot = new Model_Escort_SnapshotV2(array('id' => $this->getId()));
		$m_snapshot->snapshotProfileV2();

		$this->addSeoEntityInstance($this->getId(), $data);
		
		if(Cubix_Application::getId() == APP_ED && $agency_id && $latest_revision == 1 ){
			if(in_array($data['gender'],array(GENDER_MALE, GENDER_FEMALE, GENDER_TRANS))){
				switch ($data['gender'])
				{
					case GENDER_MALE:
						$event = FOLLOW_AGENCY_NEW_MALE;
						break;
					case GENDER_FEMALE:
						$event = FOLLOW_AGENCY_NEW_FEMALE;
						break;
					case GENDER_TRANS:
						$event = FOLLOW_AGENCY_NEW_TRANS;
						break;
				}
				Cubix_FollowEvents::notify($agency_id, 'agency', $event, array('rel_id' => $this->getId()), true);
			}
		}

	}
	
	public function addSeoEntityInstance($escort_id, $params)
	{
		$app_entity_ids = array(
			1  => 47,
			2  => 2,
			16 => 2,
			25 => 2,
			17 => 2,
			18 => 2,
			11 => 2,
			30 => 2,
			5  => 1,
			7  => 3,
			9  => 4,
			33 => 2,
			22 => 2,
			6  => 2,
		);

		$app_langs = array(
			1 => array('it', 'en'),
			2 => array('fr', 'en'),
			16 => array('de', 'fr', 'it', 'en'),
			25 => array('de', 'en'),
			17 => array('pt', 'en'),
			18 => array('fr', 'en'),
			11 => array('en', 'cn', 'my'),
			30 => array('fr', 'nl', 'en'),
			5 => array('en'),
			7 => array('en'),
			9 => array('it', 'en'),
			33 => array('en'),
			22 => array('en'),
			6 => array('de', 'fr', 'it', 'en'),
			69 => array('es', 'fr', 'it', 'en'),
		);

		//self::getAdapter()->query('DELETE FROM seo_entity_instances WHERE entity_id = ? AND primary_id = ?', array($app_entity_ids[Cubix_Application::getId()], $escort_id));
		
		$data = array(
			'entity_id' => $app_entity_ids[Cubix_Application::getId()],
			'primary_id' => $escort_id,
			'is_published' => 1,
			'primary_title' => $params['showname'] . '(' . $escort_id . ')'
		);

		$def_lang = $app_langs[Cubix_Application::getId()][0];
		$about_me = substr(strip_tags($params['about_' . $def_lang]), 0, 152);

		if ( Cubix_Application::getId() != APP_ED ) {
			if ( strlen($about_me) ) {

				foreach ( $app_langs[Cubix_Application::getId()] as $lang_id ) {
					$data['meta_description_' . $lang_id] = $about_me;
				}
				
				$seo_data = self::getAdapter()->fetchRow('SELECT * FROM seo_entity_instances 
					WHERE entity_id = ? AND primary_id = ?', 
					array($app_entity_ids[Cubix_Application::getId()], $escort_id));
				
				if($seo_data){
					$seo_entity_id = $seo_data->id;
					unset($seo_data->id);
					$seo_data = array_filter((array) $seo_data);
					$data = array_merge($seo_data,$data );
					self::getAdapter()->update('seo_entity_instances', $data, self::getAdapter()->quoteInto('id = ?',$seo_entity_id ));
				}
				else{
					self::getAdapter()->insert('seo_entity_instances', $data);
				}
				
			}
		}
	}

	public function resetHappyHour()
	{
		$has_hh = self::getAdapter()->fetchOne("SELECT has_hh FROM escorts WHERE id = ?", $this->getId());
		
		$escort_data = array(
			'hh_motto' => null,
			'hh_date_from' => null,
			'hh_date_to' => null,
			'hh_save' => 0,
			'hh_status' => Model_EscortsV2::HH_STATUS_PENDING,
			'has_hh' => ($has_hh) ? 1 : 0
		);
		self::getAdapter()->update("escorts", $escort_data, self::getAdapter()->quoteInto('id = ?', $this->getId()));
		self::getAdapter()->update("escort_rates", array('hh_price' => null), self::getAdapter()->quoteInto('escort_id = ?', $this->getId()));

		Cubix_SyncNotifier::notify($this->getId(), Cubix_SyncNotifier::EVENT_ESCORT_HAPPY_HOUR_DEACTIVATED, array('escort_id' => $this->getId(), 'action'=>'reset happy hour'));
	}
}