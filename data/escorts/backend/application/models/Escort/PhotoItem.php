<?php

class Model_Escort_PhotoItem extends Cubix_Model_Item
{
	/**
	 * The image utility library
	 *
	 * @var Cubix_Images
	 */
	protected static $_images;
	
	protected $_app_id = null;
	
	public function __construct($rowData = array())
	{
		parent::__construct($rowData);
		
		if ( empty(self::$_images) ) {
			self::$_images = new Cubix_Images();
		}
	}
	
	public function toJSON( $size_name, $is_ad_photos = null )
	{
		$data = array(
			'id' => $this->getId(),
			'escort_id' => $this->escort_id,
			'image_url' =>str_replace('.dev','',$this->getUrl($size_name)) ,
			'is_main' => $this->is_main,
			'type' => $this->type,
			'is_verified' => $this->status == Model_Escort_Photos::STATUS_VERIFIED,
			'status' => $this->status,
			'args' => ($args = unserialize($this->args)) ? $args : array('x' => 0, 'y' => 0),
			'is_approved' => $this->is_approved,
			'double_approved' => $this->double_approved,
			'orig_url' => $this->getUrl(),
			'gotd' => (isset($this->gotd)) ? $this->gotd : '0',
			'profile_boost' => (isset($this->profile_boost)) ? $this->profile_boost : '0',
			'retouch_status' => $this->retouch_status,
			'is_rotatable' => $this->is_rotatable,
			'is_portrait' => $this->is_portrait,
			'is_valentines' => $this->is_valentines,
			'is_suspicious' => $this->is_suspicious,
			'is_popunder' => (isset($this->is_popunder)) ? $this->is_popunder : 0,
			'width' =>  $this->width,
			'height' => $this->height,
			'hash' => $this->hash,
			'ext' => $this->ext,
		);

        if( !is_null( $is_ad_photos ) ){
            $img = new Cubix_ImagesCommonItem( $is_ad_photos );
            $data['image_url'] = $img->getUrl('backend_thumb_cropper').(($this->retouch_status ==2) ? '?'.time() : "");
            $data['orig_url'] = $img->getUrl();
            $data['id'] = $img->image_id;
            $data['args'] = ( $args = unserialize($is_ad_photos->args) ) ? $args : array('x' => 0, 'y' => 0);
        }
        if($this->retouch_status ==2){
            $data['image_url'] .=  '?'.time();
        }
        return $data;
	}
	
	public function getUrl($size_name = null)
	{
		if ( ! is_null($this->_app_id) ) {
			throw new Exception('Please set the application id of photo');
		}
		
		$data = array(
			'application_id' => $this->application_id,
			'catalog_id' => $this->escort_id,
			'hash' => $this->hash,
			'ext' => $this->ext
		);
		
		if ( $size_name )
		{
			$data['size'] = $size_name;
		}
		
		return self::$_images->getUrl(new Cubix_Images_Entry($data));
	}
	
	public function makePrivate()
	{
		// Fix ordering
		$this->_adapter->query('
			UPDATE escort_photos SET ordering = ordering - 1
			WHERE ordering > ? AND escort_id = ? AND type = ' . $this->type . '
		', array($this->ordering, $this->escort_id));
		
		$max = intval($this->_adapter->fetchOne('
			SELECT MAX(ordering) FROM escort_photos 
			WHERE escort_id = ? AND type = ' . ESCORT_PHOTO_TYPE_PRIVATE . '
		', $this->escort_id));
		
		if ( $max < 1 ) $max = 1;
		else $max++;
		
		$this->_adapter->query('
			UPDATE escort_photos SET type = ' . ESCORT_PHOTO_TYPE_PRIVATE . ', ordering = ' . $max . ', is_main = 0 WHERE id = ?
		', $this->getId());
		
		if ( $this->is_main ) {
			$this->_adapter->query('
				UPDATE escort_photos SET is_main = 1 WHERE type = ? AND escort_id = ? ORDER BY ordering ASC LIMIT 1
			', array(ESCORT_PHOTO_TYPE_HARD, $this->escort_id));
		}
	}
	
	public function makeArchived()
	{
		if ( $this->is_main ) {
			return;
			/*$this->_adapter->query('
				UPDATE escort_photos SET is_main = 1 WHERE type = ? AND escort_id = ? ORDER BY ordering ASC LIMIT 1
			', array(ESCORT_PHOTO_TYPE_HARD, $this->escort_id));*/
		}
		
		// Fix ordering
		$this->_adapter->query('
			UPDATE escort_photos SET ordering = ordering - 1
			WHERE ordering > ? AND escort_id = ? AND type = ' . $this->type . '
		', array($this->ordering, $this->escort_id));
		
		$max = intval($this->_adapter->fetchOne('
			SELECT MAX(ordering) FROM escort_photos 
			WHERE escort_id = ? AND type = ' . ESCORT_PHOTO_TYPE_ARCHIVED . '
		', $this->escort_id));
		
		if ( $max < 1 ) $max = 1;
		else $max++;
		
		$this->_adapter->query('
			UPDATE escort_photos SET type = ' . ESCORT_PHOTO_TYPE_ARCHIVED . ', ordering = ' . $max . ', is_main = 0 WHERE id = ?
		', $this->getId());
		
		if($this->is_suspicious){
			Cubix_SyncNotifier::notify($this->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED, array('id' => $this->id, 'action' => 'suspicious photo was archived'));
		}
		//if ( $this->is_main ) {
			/*$this->_adapter->query('
				UPDATE escort_photos SET is_main = 1 WHERE type = ? AND escort_id = ? ORDER BY ordering ASC LIMIT 1
			', array(ESCORT_PHOTO_TYPE_HARD, $this->escort_id));*/
		//}
	}
	
	public function makePublic()
	{
		$has_main = $this->_adapter->fetchOne('SELECT TRUE FROM escort_photos WHERE escort_id = ? AND is_main = 1 AND ( type = ? OR type = ?)', array($this->escort_id, ESCORT_PHOTO_TYPE_HARD, ESCORT_PHOTO_TYPE_SOFT));
		
		// Fix ordering
		$this->_adapter->query('
			UPDATE escort_photos SET ordering = ordering - 1
			WHERE ordering > ? AND escort_id = ? AND type = ' . $this->type . '
		', array($this->ordering, $this->escort_id));
		
		$max = $this->_adapter->fetchOne('
			SELECT MAX(ordering) FROM escort_photos 
			WHERE escort_id = ? AND type = ' . ESCORT_PHOTO_TYPE_HARD . ' OR type = '. ESCORT_PHOTO_TYPE_SOFT
		, $this->escort_id);
		
		if ( $max < 1 ) $max = 1;
		else $max++;
		
		$type = Cubix_Application::getId() == APP_EF ? ESCORT_PHOTO_TYPE_SOFT : ESCORT_PHOTO_TYPE_HARD ;
		if ( $has_main ) {
			$this->_adapter->query('
				UPDATE escort_photos SET type = ' . $type . ', ordering = ' . $max . ' WHERE id = ?
			', $this->getId());
		} else {
			$this->_adapter->query('
				UPDATE escort_photos SET type = ' . $type . ', ordering = ' . $max . ', is_main = 1 WHERE id = ?
			', $this->getId());
		}
	}
	
	public function makeDisabled()
	{
		// Fix ordering
		$this->_adapter->query('
			UPDATE escort_photos SET ordering = ordering - 1
			WHERE ordering > ? AND escort_id = ? AND type = ' . $this->type . '
		', array($this->ordering, $this->escort_id));
		
		$max = $this->_adapter->fetchOne('
			SELECT MAX(ordering) FROM escort_photos 
			WHERE escort_id = ? AND type = ' . ESCORT_PHOTO_TYPE_DISABLED . '
		', $this->escort_id);
		
		if ( $max < 1 ) $max = 1;
		else $max++;
		
		$approved = '';
		if(Cubix_Application::getId() != APP_A6) {
			$approved = ' is_approved = 1, ';
		}

		$this->_adapter->query('
			UPDATE escort_photos SET type = ' . ESCORT_PHOTO_TYPE_DISABLED . ', is_main = 0, '.$approved.' ordering = ' . $max . ' WHERE id = ?
		', $this->getId());
		
		if ( $this->is_main ) {
			$this->_adapter->query('
				UPDATE escort_photos SET is_main = 1 WHERE type = ? AND escort_id = ? ORDER BY ordering ASC LIMIT 1
			', array(ESCORT_PHOTO_TYPE_HARD, $this->escort_id));
		}
	}
	
	public function makeSoft()
	{
		$this->_adapter->query('
			UPDATE escort_photos SET type = ' . ESCORT_PHOTO_TYPE_SOFT . ' WHERE id = ?
		', $this->getId());
	}

	public function approve()
	{
		$this->_adapter->query('
			UPDATE escort_photos SET is_approved = IF (is_approved = 1, 0, 1) WHERE id = ?
		', $this->getId());
	}
	
	public function makeHard()
	{
		$this->_adapter->query('
			UPDATE escort_photos SET type = ' . ESCORT_PHOTO_TYPE_HARD . ' WHERE id = ?
		', $this->getId());
	}
	
	public function reorder($new_ordering)
	{
		$this->_adapter->query('
			UPDATE escort_photos SET ordering = ? WHERE id = ?
		', array($new_ordering, $this->getId()));
	}
	
	public function toggleVerified()
	{
		$this->_adapter->query('
			UPDATE escort_photos SET status = IF (status = 1, 3, 1) WHERE id = ?
		', $this->getId());
	}

    public function toggleValentines()
    {
        $this->_adapter->query('
			UPDATE escort_photos SET is_valentines = IF (is_valentines = 1, 3, 1) WHERE id = ?
		', $this->getId());

        $this->_adapter->query('
			UPDATE escort_photos SET is_valentines = 0 WHERE escort_id = ? AND id <> ? AND is_valentines < 3
		', array($this->escort_id,$this->getId()));
    }

	public function toggleRetouch()
	{
		try {
			$this->_adapter->beginTransaction();
			
			$new_status = Model_RetouchPhotos::NEEDS_RETOUCH;
			if ( $this->retouch_status == Model_RetouchPhotos::NEEDS_RETOUCH ) {
				$new_status = 0;
			}

			$sql = 'UPDATE escort_photos SET retouch_status = ?';
			if ( $new_status == Model_RetouchPhotos::NEEDS_RETOUCH ) {
				$sql .= ' ,is_approved = 1';
			}
			
			//Changing main photo
			if ( $this->is_main && $new_status == Model_RetouchPhotos::NEEDS_RETOUCH ) { 
				$this->_adapter->query('UPDATE escort_photos SET is_main = 1 WHERE  (type = ? OR type = ?) AND escort_id = ? AND retouch_status <> ? AND is_main <> 1 LIMIT 1', 
						array(ESCORT_PHOTO_TYPE_SOFT, ESCORT_PHOTO_TYPE_HARD, $this->escort_id, Model_RetouchPhotos::NEEDS_RETOUCH));//Make public first photo
				$sql .= ', is_tmp_main = 1, is_main = 0';
			} elseif ( $this->is_tmp_main && $new_status != Model_RetouchPhotos::NEEDS_RETOUCH ) {
				$this->_adapter->update('escort_photos', array('is_main' => 0), $this->_adapter->quoteInto('escort_id = ?', $this->escort_id));//Reseting all main pics
				$sql .= ', is_tmp_main = 0, is_main = 1';
			}
			
			$sql .= ' WHERE id = ?';
			
			$this->_adapter->query($sql, array($new_status, $this->getId()));
			
			
			
			$this->_adapter->commit();
		} catch (Exception $ex) {
			$this->_adapter->rollBack();
			throw $ex;
		}
	}
	
	public function setMain()
	{
		$this->_adapter->query('
			UPDATE escort_photos SET is_main = 0 WHERE escort_id = ?
		', $this->getEscortId());
		
		$this->_adapter->query('
			UPDATE escort_photos SET is_main = 1 WHERE id = ?
		', $this->getId());
	}
	
	public function setRotatable()
	{
		$this->_adapter->query('
			UPDATE escort_photos SET is_rotatable = 1 WHERE id = ?
		', $this->getId());
		return $this->getId();
	}
	
	public function setGOTD()
	{
		$this->_adapter->query('
			UPDATE escort_photos SET gotd = 0 WHERE escort_id = ?
		', $this->getEscortId());
		
		$this->_adapter->query('
			UPDATE escort_photos SET gotd = 1 WHERE id = ?
		', $this->getId());
	}
	
	public function setProfileBoost()
	{
		$this->_adapter->query('
			UPDATE escort_photos SET profile_boost = 0 WHERE escort_id = ?
		', $this->getEscortId());
		
		$this->_adapter->query('
			UPDATE escort_photos SET profile_boost = 1 WHERE id = ?
		', $this->getId());
	}
	
	public function setApplicationId($id)
	{
		$this->_app_id = $id;
	}
	
	public function getApplicationId()
	{
		return $this->_app_id;
	}

	public function setCropArgs($args)
	{
		$args = serialize($args);
		
		self::getAdapter()->query('UPDATE escort_photos SET args = ? WHERE id = ?', array($args, $this->getId()));

		return true;
	}
	
	public function setPopunderCropArgs($args)
	{
		$args = serialize($args);
		
		self::getAdapter()->query('UPDATE escort_photos SET popunder_args = ? WHERE id = ?', array($args, $this->getId()));

		return true;
	}
}
