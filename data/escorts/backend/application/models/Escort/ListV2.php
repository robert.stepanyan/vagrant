<?php

class Model_Escort_ListV2 extends Cubix_Model
{ 
	protected $_table = 'escorts';
	protected $_itemClass = 'Model_EscortV2Item';
	
	public function __construct($rowData = null)
	{
		if ( !is_null($rowData) ) {
			parent::__construct($rowData);
		}
		else {
			parent::__construct();
		}
	}

	public function getForAutocompleter($page, $per_page, $filter, $sort_field, $sort_dir)
	{
		$sql = array(
			'fields' => array(
				'u.id AS user_id', 'u.username', 'CONCAT(e.showname, \'-\', e.id) AS showname', /*'e.showname',*/
				'eph.hash AS photo_hash', 'eph.ext AS photo_ext', 'eph.status AS photo_status',
				'ag.name AS agency_name', 'ag.id AS agency_id',	'u.balance'
			),
			'joins' => array(
				'LEFT JOIN agencies ag ON ag.id = e.agency_id',
				'LEFT JOIN escort_profiles_v2 ep ON ep.escort_id = e.id',
				'LEFT JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1',
				'LEFT JOIN orders o ON o.user_id = u.id'
			),
			'where' => $filter,
			'order' => array($sort_field, $sort_dir),
			'page' => array($page, $per_page)
		);

		$data = $this->_getAll($sql);

		return $data;
	}

	public function getForAgencies($page, $per_page, $filter, $sort_field, $sort_dir)
	{
		$sql = array(
			'fields' => array(
				'u.username', 'e.showname','e.user_id', 'ep.phone_number AS contact_phone', 'ep.phone_number AS contact_phone_parsed', 'ep.email AS contact_email',
				'ag.name AS agency_name', 'ag.id AS agency_id',

				'_p.name AS package_name', '_p.id AS package_id', 'UNIX_TIMESTAMP(_op.expiration_date) AS expiration_date', '_op.id AS order_package_id', 'UNIX_TIMESTAMP(_op.date_activated) AS date_activated, _op.order_id as premium_package'
			),
			'joins' => array(
				'LEFT JOIN agencies ag ON ag.id = e.agency_id',
				'LEFT JOIN escort_profiles_v2 ep ON ep.escort_id = e.id',
				'LEFT JOIN order_packages _op ON _op.escort_id = e.id AND _op.status = 2 AND _op.application_id = ' . Cubix_Application::getId(),
				'LEFT JOIN packages _p ON _p.id = _op.package_id'
			),
			'where' => $filter,
			'order' => array($sort_field, $sort_dir),
			'page' => array($page, $per_page)
		);

		$data = $this->_getAll($sql);

		return $data;
	}
	
	public static function ddd()
	{
		return parent::db()->query("select expiration_date as expr from order_packages where escort_id = 4779")->fetch();
	}

	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir)
	{
		$checkSourceUrl = 0;

		$fields = '';
		$join = '';

		if(in_array(Cubix_Application::getId(), array(APP_ED,APP_EG_CO_UK,APP_EG,APP_AE))){
			$fields .= ', e.source_url ';
			
			if(isset($filter['work_source_url'])){
				$checkSourceUrl = 1;	
				unset($filter['work_source_url']);
			}
			if(isset($filter['wrong_source_url'])){
				$checkSourceUrl = 2;	
				unset($filter['wrong_source_url']);
			}

			if (in_array(Cubix_Application::getId(), array(APP_ED,APP_EG_CO_UK)))
            {
                $fields .= ', UNIX_TIMESTAMP(u.last_login_date) AS last_login_date, ep.viber, ep.whatsapp, ep.telegram, ep.ssignal';
            }
		}

		if(Cubix_Application::getId() == APP_A6){
			$fields .= ',_p.is_for_phone_billing ';
		}

		if(Cubix_Application::getId() == APP_EF){
			$fields .= ', e.need_age_verification, epc.comment as escort_phone_comment, e.block_cam, eph.is_valentines ';
		}

		if(Cubix_Application::getId() == APP_BL){
			$fields .= ', gotd_orders.activation_date ';
		}

		$for_sales_change = false;
		if( in_array(Cubix_Application::getId(), array(APP_ED, APP_EG_CO_UK))){
            if ( isset($filter['sales_change_date_from']) || isset($filter['sales_change_date_to']) ) {
                $for_sales_change = true;
                if (isset($filter['sales_change_date_from'])) {
                    $filter[] = 'sch.max_date > DATE(FROM_UNIXTIME("' . $filter['sales_change_date_from'] . '"))';
                    unset($filter['sales_change_date_from']);
                }
                if (isset($filter['sales_change_date_to'])) {
                    $filter[] = 'sch.max_date < DATE(FROM_UNIXTIME("' . $filter['sales_change_date_to'] . '"))';
                    unset($filter['sales_change_date_to']);
                }
            }
		}

		//Phone number search optimization 
		if (Cubix_Application::getId() == APP_A6 && $filter['phone_number']) {
			$esc_ids = parent::db()->fetchOne('SELECT GROUP_CONCAT(escort_id) FROM phone_grinder WHERE phone_part LIKE "'. $filter['phone_number'] .'%"');
			if($esc_ids){
				$filter[] = 'e.id IN ('. $esc_ids .')' ;
				unset($filter['phone_number']);
			}
			else{
				return array('result' => 0, 'count' => 0);
			}
		}

        $orangeList = false;
		if ($filter['orange_list'])
        {
            $filter[] = ' ~ e.STATUS & '.ESCORT_STATUS_DELETED.' AND ~ e.STATUS & '.ESCORT_STATUS_INACTIVE.' AND ( e.STATUS & '.ESCORT_STATUS_ACTIVE.' ) AND e.agency_id IS NULL AND ep.contact_phone_parsed IS NOT NULL AND (e.last_hand_verification_date IS NULL OR UNIX_TIMESTAMP( u.last_login_date ) < UNIX_TIMESTAMP(CURDATE() - INTERVAL 90 DAY)) ';
            if (in_array(Cubix_Application::getId(),array(APP_EG_CO_UK)))
            {
                $filter[] = '5 = (SELECT count(distinct id) from escort_comments WHERE escort_id = e.id AND `comment` = "auto sms and mail for confirmation")';
            }else{
                $filter[] = '_op.id IN (
                            SELECT
                                MIN(id) 
                            FROM
                                order_packages 
                            WHERE
                                escort_id = e.id 
                                AND date_activated IS NOT NULL AND application_id = ' . Cubix_Application::getId() . '
                            ) AND UNIX_TIMESTAMP(_op.date_activated) <  UNIX_TIMESTAMP(CURDATE() - INTERVAL 21 DAY) ';
            }

            $orangeList = true;
            unset($filter['orange_list']);
        }

        $inOrangeListDate = false;
        if ($filter['in_orange_list_from'] || $filter['in_orange_list_to'])
        {
            $inOrangeListDate = true;
            if (!$orangeList)
            {
                $filter[] = ' ~ e.STATUS & '.ESCORT_STATUS_DELETED.' AND ~ e.STATUS & '.ESCORT_STATUS_INACTIVE.' AND ( e.STATUS & '.ESCORT_STATUS_ACTIVE.' ) AND e.agency_id IS NULL AND ep.contact_phone_parsed IS NOT NULL AND (e.last_hand_verification_date IS NULL OR UNIX_TIMESTAMP( u.last_login_date ) < UNIX_TIMESTAMP(CURDATE() - INTERVAL 90 DAY)) ';

                if (in_array(Cubix_Application::getId(),array(APP_EG_CO_UK)))
                {
                    $filter[] = '5 = (SELECT count(distinct id) from escort_comments WHERE escort_id = e.id AND `comment` = "auto sms and mail for confirmation")';
                }else{
                    $filter[] = '_op.id IN (
                            SELECT
                                MIN(id) 
                            FROM
                                order_packages 
                            WHERE
                                escort_id = e.id 
                                AND date_activated IS NOT NULL AND application_id = ' . Cubix_Application::getId() . '
                            ) AND UNIX_TIMESTAMP(_op.date_activated) <  UNIX_TIMESTAMP(CURDATE() - INTERVAL 21 DAY) ';
                }
            }
            if ($filter["in_orange_list_from"])
            {
                $filter[] = ' UNIX_TIMESTAMP(ec.date) > '.$filter['in_orange_list_from'];
                unset($filter['in_orange_list_from']);
            }
            if ($filter['in_orange_list_to'])
            {
                $filter[] = ' UNIX_TIMESTAMP(ec.date) < '.$filter['in_orange_list_to'];
                unset($filter['in_orange_list_to']);
            }
        }


        if(Cubix_Application::getId() == APP_BL){
            if ($filter['gotd_date_from'] || $filter['gotd_date_to'])
            {
                if ($filter['gotd_date_from'] && $filter['gotd_date_to']) {
                    $filter[] = 'gotd_orders.activation_date > DATE(FROM_UNIXTIME("' . $filter['gotd_date_from'] . '")) AND gotd_orders.activation_date < DATE(FROM_UNIXTIME("' . $filter['gotd_date_to'] . '")) AND gotd_orders.`status` = 2';
                    unset($filter['gotd_date_from']);
                    unset($filter['gotd_date_to']);
                }
                elseif ($filter['gotd_date_from']) {
                    $filter[] = 'gotd_orders.activation_date > DATE(FROM_UNIXTIME("' . $filter['gotd_date_from'] . '")) AND gotd_orders.`status` = 2';
                    unset($filter['gotd_date_from']);
                }
                elseif ($filter['gotd_date_to']) {
                    $filter[] = 'gotd_orders.activation_date < DATE(FROM_UNIXTIME("' . $filter['gotd_date_to'] . '")) AND gotd_orders.`status` = 2';
                    unset($filter['gotd_date_to']);
                }
            }
        }

        if(Cubix_Application::getId() == APP_EG_CO_UK && !is_null($filter['source_url_redirects'])){
            $filter[] = 'e.souorce_url_redirects = '. $filter['source_url_redirects'];
                unset($filter['source_url_redirects']);
        }


		$sql = array(
			'fields' => array(
                'e.id AS e_id, UNIX_TIMESTAMP(e.date_last_modified) AS date_last_modified', 'e.status', 'UNIX_TIMESTAMP(e.date_registered) AS date_registered',
                'e.showname', 'e.due_date', 'e.deleted_date', 'e.check_website', 'e.check_website_date','e.check_website_person', 'e.block_website' , 'e.block_website_date','e.block_website_person', 'e.country_id',
                'u.id AS user_id', 'u.username', 'u.email AS reg_email', 'e.porn_notification', 'e.porn_notification_date', 'bu2.username AS porn_notification_person',
                'ep.phone_number','ep.phone_exists', 'ep.email', 'ep.contact_phone_parsed', 'ep.website', 'e.verified_status', 'e.gallery_is_checked',
                'ag.name AS agency_name', 'ag.id AS agency_id', 'e.is_suspicious', 'e.souorce_url_redirects',
								/**	 Added Grigor	 **/
								
                'UNIX_TIMESTAMP(photo_suspicious_date) AS date_suspicious', 'e.is_vip','e.disabled_reviews', 'e.disabled_comments', 'e.gender',
                '_p.name AS package_name', '_op.id AS order_package_id', '_op.expiration_date AS expiration_date', 'UNIX_TIMESTAMP(_op.date_activated) AS date_activated',
				'ag.disabled_reviews AS ag_disabled_reviews', 'ag.disabled_comments AS ag_disabled_comments, u.user_type as user_type'. $fields

			),
			'joins' => array(
				'LEFT JOIN escort_profiles_v2 ep ON ep.escort_id = e.id',
				'LEFT JOIN escort_photos eph ON eph.escort_id = e.id',
				'LEFT JOIN agencies ag ON ag.id = e.agency_id',
				'LEFT JOIN backend_users bu2 ON bu2.id = e.porn_notification_person',
				'LEFT JOIN order_packages _op ON _op.escort_id = e.id AND _op.status = 2 AND _op.application_id = ' . Cubix_Application::getId() . '',
				'LEFT JOIN packages _p ON _p.id = _op.package_id',
			),
			'where' => $filter,
			'order' => array($sort_field, $sort_dir),
			'page' => array($page, $per_page)
		);

        if ( $for_sales_change )
            $sql['joins'][] = 'LEFT JOIN ( SELECT user_id, MAX(date) max_date FROM sales_change_history GROUP BY id) sch ON sch.user_id = u.id';

        if ($orangeList)
            $sql['fields'][] = '1 as orange_list';

        if ($inOrangeListDate)
        {
            $sql['joins'][] = 'LEFT JOIN escort_comments ec ON ec.escort_id = e.id';
            $sql['fields'][] = '1 as orange_list';
        }

		if(Cubix_Application::getId() == APP_EF){
			$sql['joins'][] = 'LEFT JOIN escort_phone_comment epc ON epc.escort_id = e.id';
		}
        if(isset($filter['aa.area_id = ?'])){
            $sql['joins'][] = 'INNER JOIN additional_areas aa ON aa.order_package_id = _op.id';
        }
		if(in_array(Cubix_Application::getId(), array(APP_ED, APP_EG_CO_UK))){
		    if ( !$filter['search_in_all_cities'])
            {
                $sql['joins'][] = ' LEFT JOIN cities c ON c.id = e.city_id';
            }else{
                $sql['joins'][] = ' LEFT JOIN escort_cities ec ON ec.escort_id = e.id';
            }

			if(Cubix_Application::getId() == APP_ED && $filter['rg.id = ?'] ){
				$sql['joins'][] = ' LEFT JOIN regions rg ON rg.id = c.region_id';
			}

            if( Cubix_Application::getId() == APP_EG_CO_UK ){
                $sql['fields'][] = 'e.last_export_date';
            }

            /*$sql['joins'][] = ' LEFT JOIN countries co ON co.id = e.country_id';
            $sql['joins'][] = ' LEFT JOIN escort_rates er ON er.escort_id = e.id ';
            $sql['joins'][] = ' LEFT JOIN currencies cur ON cur.id = er.currency_id ';*/
		}

        if(in_array(Cubix_Application::getId(), array(APP_BL))){
            $sql['joins'][] = ' LEFT JOIN gotd_orders ON gotd_orders.escort_id = e.id';
        }

		$search_all_optimizitaion = true;

		if(Cubix_Application::getId() == APP_EF && $filter['zona_rossa'] == 1){
			  $sql['joins'][] = ' INNER JOIN cities c ON c.id = e.city_id AND c.zona_rossa = 1';
		}

		foreach($filter as $key => $f){

			if(!$f || in_array($key, array('e.country_id = ?', 'excl_status', 'about_text_lang'), true)) {
				if( $key != 'e.country_id = ?'){
					unset($filter[$key]);
				}
			}
			else{
				$search_all_optimizitaion = false;
				BREAK;
			}
		}

		if($search_all_optimizitaion){
			
			if($filter['e.country_id = ?']){
				if($filter['e.country_id = ?'] == 'all_except_usa') {
					$opt_where = 'WHERE country_id != 68 ';
				}elseif($filter['e.country_id = ?'] == 'all_except_usa_ca'){
					$opt_where = 'WHERE country_id != 68 and country_id != 10';
				}
                elseif($filter['e.country_id = ?'] == 'all_except_usa_fr'){
                    $opt_where = 'WHERE country_id != 68 and country_id != 23';
                }
				else {
					$opt_where = 'WHERE country_id = '. $filter['e.country_id = ?'];
				}
			}
			
			$sql['joins'][] = 'INNER JOIN (SELECT id FROM escorts '. $opt_where .'  ORDER BY id desc LIMIT '. ($page - 1) * $per_page . ','. $per_page .' ) as eop ON eop.id = e.id';
			//$sql['order'] = false;
			$sql['page'] = false;
		}

		$result = $this->_getAll($sql, true);

		$sql = $result['data_sql'];

		//print_r($sql); die;
		
		$escorts = parent::_fetchAll($sql);
		//echo $result['count_sql']; die;
		if($search_all_optimizitaion){
			$count = self::getAdapter()->fetchOne('SELECT count(*) FROM escorts '. $opt_where .'');
		}
		else{
			$count = self::getAdapter()->fetchOne('SELECT FOUND_ROWS()');
		}

		if ($checkSourceUrl && in_array(Cubix_Application::getId(), array(APP_ED,APP_EG_CO_UK,APP_EG,APP_AE))){
			$unsetEscortCount = 0;

			foreach ($escorts as $k => $escort) {

				// Check url work
				$result = $this->__checkUrlExist($escort->source_url);

				if($checkSourceUrl == 1){
					if(!$result){
						unset($escorts[$k]);
						$unsetEscortCount ++;
					}
				}

				if($checkSourceUrl == 2){
					if($result){
						unset($escorts[$k]);
						$unsetEscortCount ++;
					}
				}
				
			}
			
			$count = $count - $unsetEscortCount;
			if($unsetEscortCount > 0){
				$escorts = array_values($escorts);
			}
		}

		return array('result' => $escorts, 'count' => $count);
	}

	public function getCallCenter7days($page, $per_page, $filter, $sort_field, $sort_dir, $having = null )
	{
		$checkSourceUrl = 0;

		$fields = '';
		$join = '';

		if(in_array(Cubix_Application::getId(), array(APP_ED,APP_EG_CO_UK,APP_EG,APP_AE))){
			$fields .= ', e.source_url ';
			
			if(isset($filter['work_source_url'])){
				$checkSourceUrl = 1;	
				unset($filter['work_source_url']);
			}
			if(isset($filter['wrong_source_url'])){
				$checkSourceUrl = 2;	
				unset($filter['wrong_source_url']);
			}
		}

		if(Cubix_Application::getId() == APP_A6){
			$fields .= ',_p.is_for_phone_billing ';
		}

        if(Cubix_Application::getId() == APP_ED){
            $fields .= ',ec.comment as sms_comment, count(ec.escort_id) ';
        }

		if(Cubix_Application::getId() == APP_EF){
			$fields .= ', e.need_age_verification, epc.comment as escort_phone_comment, e.block_cam ';
		}
		if(Cubix_Application::getId() == APP_ED || Cubix_Application::getId() == APP_EG_CO_UK){
            $fields .= ', count( er.escort_id ) as call_count ';
            //$fields .= ', co.title_en as country,  c.title_en as city, cur.title as currency_title , min(er.price) as price, er.time, if(er.time_unit = 1, "m", if(er.time_unit = 2, "h", if(er.time_unit = 3, "h", "" )  )  ) as time_unit ';
		}

		//Phone number search optimization 
		if (Cubix_Application::getId() == APP_A6 && $filter['phone_number']) {
			$esc_ids = parent::db()->fetchOne('SELECT GROUP_CONCAT(escort_id) FROM phone_grinder WHERE phone_part LIKE "'. $filter['phone_number'] .'%"');
			if($esc_ids){
				$filter[] = 'e.id IN ('. $esc_ids .')' ;
				unset($filter['phone_number']);
			}
			else{
				return array('result' => 0, 'count' => 0);
			}
		}
		
		$sql = array(
			'fields' => array(
                'e.id AS e_id, UNIX_TIMESTAMP(e.date_last_modified) AS date_last_modified', 'e.status', 'UNIX_TIMESTAMP(e.date_registered) AS date_registered',
                'e.showname', 'e.due_date', 'e.deleted_date', 'e.check_website', 'e.check_website_date','e.check_website_person', 'e.block_website' , 'e.block_website_date','e.block_website_person',
                'u.id AS user_id', 'u.username', 'u.email AS reg_email', 'e.porn_notification', 'e.porn_notification_date', 'bu2.username AS porn_notification_person',
                'ep.phone_number','ep.phone_exists', 'ep.email', 'ep.contact_phone_parsed', 'ep.phone_country_id', 'ep.website', 'e.verified_status', 'e.gallery_is_checked',
                'ag.name AS agency_name', 'ag.id AS agency_id', 'e.is_suspicious, UNIX_TIMESTAMP(e.package_activation_date) AS package_activation_date ', 
                'UNIX_TIMESTAMP(photo_suspicious_date) AS date_suspicious', 'e.is_vip','e.disabled_reviews', 'e.disabled_comments', 'e.gender',
                'c.title_en as city_name',  'co.title_en as country_name', 'cp.phone_prefix',
                '_p.name AS package_name', '_op.id AS order_package_id', '_op.expiration_date AS expiration_date', 'UNIX_TIMESTAMP(_op.date_activated) AS date_activated',
				'ag.disabled_reviews AS ag_disabled_reviews', 'ag.disabled_comments AS ag_disabled_comments, u.user_type as user_type'. $fields

			),
			'joins' => array(
				'LEFT JOIN escort_profiles_v2 ep ON ep.escort_id = e.id',
				'LEFT JOIN agencies ag ON ag.id = e.agency_id',
				'LEFT JOIN backend_users bu2 ON bu2.id = e.porn_notification_person',
				'LEFT JOIN order_packages _op ON _op.escort_id = e.id AND _op.status = 2 AND _op.application_id = ' . Cubix_Application::getId() . '',
				'LEFT JOIN packages _p ON _p.id = _op.package_id',
                'LEFT JOIN escort_responses er ON er.escort_id = e.id',
			),
			'where' => $filter,
			'having' => $having,
			'order' => array($sort_field, $sort_dir),
			'page' => array($page, $per_page)
		);

		
        if(isset($filter['aa.area_id = ?'])){
            $sql['joins'][] = 'INNER JOIN additional_areas aa ON aa.order_package_id = _op.id';
        }
        if (Cubix_Application::getId() == APP_ED){
            $sql['joins'][] = 'LEFT JOIN escort_comments ec on ec.escort_id = e.id';
        }
		if(Cubix_Application::getId() == APP_ED || Cubix_Application::getId() == APP_EG_CO_UK){
			$sql['joins'][] = ' LEFT JOIN cities c ON c.id = e.city_id';
            $sql['joins'][] = ' LEFT JOIN countries co ON co.id = e.country_id';
            $sql['joins'][] = ' LEFT JOIN countries_phone_code cp ON cp.id = ep.phone_country_id';
           
		}

		$search_all_optimizitaion = true;

		foreach($filter as $key => $f){

			if(!$f || in_array($key, array('e.country_id = ?', 'excl_status', 'about_text_lang'), true)) {
				if( $key != 'e.country_id = ?'){
					unset($filter[$key]);
				}
			}
			else{
				$search_all_optimizitaion = false;
				BREAK;
			}
		}

		if($search_all_optimizitaion){
			
			if($filter['e.country_id = ?']){
				if($filter['e.country_id = ?'] == 'all_except_usa') {
					$opt_where = 'WHERE country_id != 68 ';
				}elseif($filter['e.country_id = ?'] == 'all_except_usa_ca'){
					$opt_where = 'WHERE country_id != 68 and country_id != 10';
				}
                elseif($filter['e.country_id = ?'] == 'all_except_usa_fr'){
                    $opt_where = 'WHERE country_id != 68 and country_id != 23';
                }
				else {
					$opt_where = 'WHERE country_id = '. $filter['e.country_id = ?'];
				}
			}
			
			$sql['joins'][] = 'INNER JOIN (SELECT id FROM escorts '. $opt_where .'  ORDER BY id desc LIMIT '. ($page - 1) * $per_page . ','. $per_page .' ) as eop ON eop.id = e.id';
			//$sql['order'] = false;
			$sql['page'] = false;
		}

		$result = $this->_getAll($sql, true);

		$sql = $result['data_sql'];

		//print_r($sql); die;
		
		$escorts = parent::_fetchAll($sql);
		//echo $result['count_sql']; die;
		if($search_all_optimizitaion){
			$count = self::getAdapter()->fetchOne('SELECT count(*) FROM escorts '. $opt_where .'');
		}
		else{
			$count = self::getAdapter()->fetchOne('SELECT FOUND_ROWS()');
		}

		if ($checkSourceUrl && in_array(Cubix_Application::getId(), array(APP_ED,APP_EG_CO_UK,APP_EG,APP_AE))){
			$unsetEscortCount = 0;

			foreach ($escorts as $k => $escort) {

				// Check url work
				$result = $this->__checkUrlExist($escort->source_url);

				if($checkSourceUrl == 1){
					if(!$result){
						unset($escorts[$k]);
						$unsetEscortCount ++;
					}
				}

				if($checkSourceUrl == 2){
					if($result){
						unset($escorts[$k]);
						$unsetEscortCount ++;
					}
				}
				
			}
			
			$count = $count - $unsetEscortCount;
			if($unsetEscortCount > 0){
				$escorts = array_values($escorts);
			}
		}

		return array('result' => $escorts, 'count' => $count);
	}

    public function getNeedAttentionEscorts($page, $per_page, $filter, $sort_field, $sort_dir)
    {
        $checkSourceUrl = 0;

        $fields = '';
        $join = '';

        if(in_array(Cubix_Application::getId(), array(APP_ED,APP_EG_CO_UK,APP_EG,APP_AE))){
            $fields .= ', e.source_url ';

            if(isset($filter['work_source_url'])){
                $checkSourceUrl = 1;
                unset($filter['work_source_url']);
            }
            if(isset($filter['wrong_source_url'])){
                $checkSourceUrl = 2;
                unset($filter['wrong_source_url']);
            }
        }

        if(Cubix_Application::getId() == APP_A6){
            $fields .= ',_p.is_for_phone_billing ';
        }

        if(Cubix_Application::getId() == APP_EF){
            $fields .= ', e.need_age_verification, epc.comment as escort_phone_comment, e.block_cam ';
        }
        if(Cubix_Application::getId() == APP_ED || Cubix_Application::getId() == APP_EG_CO_UK){
            //$fields .= ', co.title_en as country,  c.title_en as city, cur.title as currency_title , min(er.price) as price, er.time, if(er.time_unit = 1, "m", if(er.time_unit = 2, "h", if(er.time_unit = 3, "h", "" )  )  ) as time_unit ';
        }

        //Phone number search optimization
        if (Cubix_Application::getId() == APP_A6 && $filter['phone_number']) {
            $esc_ids = parent::db()->fetchOne('SELECT GROUP_CONCAT(escort_id) FROM phone_grinder WHERE phone_part LIKE "'. $filter['phone_number'] .'%"');
            if($esc_ids){
                $filter[] = 'e.id IN ('. $esc_ids .')' ;
                unset($filter['phone_number']);
            }
            else{
                return array('result' => 0, 'count' => 0);
            }
        }

        $sql = array(
            'fields' => array(
                'e.id AS e_id, UNIX_TIMESTAMP(e.date_last_modified) AS date_last_modified', 'e.status', 'UNIX_TIMESTAMP(e.date_registered) AS date_registered',
                'e.showname', 'e.due_date', 'e.deleted_date', 'e.check_website', 'e.check_website_date','e.check_website_person', 'e.block_website' , 'e.block_website_date','e.block_website_person',
                'u.id AS user_id', 'u.username', 'UNIX_TIMESTAMP(ull.login_date) AS last_login_date','u.email AS reg_email', 'e.porn_notification', 'e.porn_notification_date', 'bu2.username AS porn_notification_person',
                'ep.phone_number','ep.phone_exists', 'ep.email', 'ep.contact_phone_parsed', 'ep.phone_country_id', 'ep.website', 'e.verified_status', 'e.gallery_is_checked',
                'ag.name AS agency_name', 'ag.id AS agency_id', 'e.is_suspicious, UNIX_TIMESTAMP(e.package_activation_date) AS package_activation_date ',
                'UNIX_TIMESTAMP(photo_suspicious_date) AS date_suspicious', 'e.is_vip','e.disabled_reviews', 'e.disabled_comments', 'e.gender',
                'c.title_en as city_name',  'co.title_en as country_name', 'cp.phone_prefix',
                '_p.name AS package_name', '_op.id AS order_package_id', '_op.expiration_date AS expiration_date', 'UNIX_TIMESTAMP(_op.date_activated) AS date_activated',
                'ag.disabled_reviews AS ag_disabled_reviews', 'ag.disabled_comments AS ag_disabled_comments, u.user_type as user_type'. $fields

            ),
            'joins' => array(
                'LEFT JOIN escort_profiles_v2 ep ON ep.escort_id = e.id',
                'LEFT JOIN agencies ag ON ag.id = e.agency_id',
                'LEFT JOIN backend_users bu2 ON bu2.id = e.porn_notification_person',
                'LEFT JOIN order_packages _op ON _op.escort_id = e.id AND _op.status = 2 AND _op.application_id = ' . Cubix_Application::getId() . '',
                'LEFT JOIN packages _p ON _p.id = _op.package_id',
                'LEFT JOIN users_last_login ull ON ull.user_id = u.id',

            ),
            'where' => $filter,
            'order' => array($sort_field, $sort_dir),
            'page' => array($page, $per_page)
        );


        if(isset($filter['aa.area_id = ?'])){
            $sql['joins'][] = 'INNER JOIN additional_areas aa ON aa.order_package_id = _op.id';
        }


        if(Cubix_Application::getId() == APP_ED ){
            $sql['joins'][] = ' LEFT JOIN cities c ON c.id = e.city_id';
            $sql['joins'][] = ' LEFT JOIN countries co ON co.id = e.country_id';
            $sql['joins'][] = ' LEFT JOIN countries_phone_code cp ON cp.id = ep.phone_country_id';

        }

        $search_all_optimizitaion = true;

        foreach($filter as $key => $f){

            if(!$f || in_array($key, array('e.country_id = ?', 'excl_status', 'about_text_lang'), true)) {
                if( $key != 'e.country_id = ?'){
                    unset($filter[$key]);
                }
            }
            else{
                $search_all_optimizitaion = false;
                BREAK;
            }
        }

        if($search_all_optimizitaion){

            if($filter['e.country_id = ?']){
                if($filter['e.country_id = ?'] == 'all_except_usa') {
                    $opt_where = 'WHERE country_id != 68 ';
                }elseif($filter['e.country_id = ?'] == 'all_except_usa_ca'){
                    $opt_where = 'WHERE country_id != 68 and country_id != 10';
                }
                elseif($filter['e.country_id = ?'] == 'all_except_usa_fr'){
                    $opt_where = 'WHERE country_id != 68 and country_id != 23';
                }
                else {
                    $opt_where = 'WHERE country_id = '. $filter['e.country_id = ?'];
                }
            }

            $sql['joins'][] = 'INNER JOIN (SELECT id FROM escorts '. $opt_where .'  ORDER BY id desc LIMIT '. ($page - 1) * $per_page . ','. $per_page .' ) as eop ON eop.id = e.id';
            //$sql['order'] = false;
            $sql['page'] = false;
        }

        $result = $this->_getAll($sql, true);

        $sql = $result['data_sql'];
//        echo '<pre>';var_dump($sql);die;
        //print_r($sql); die;

        $escorts = parent::_fetchAll($sql);
        //echo $result['count_sql']; die;
        if($search_all_optimizitaion){
            $count = self::getAdapter()->fetchOne('SELECT count(*) FROM escorts '. $opt_where .'');
        }
        else{
            $count = self::getAdapter()->fetchOne('SELECT FOUND_ROWS()');
        }

        if ($checkSourceUrl && in_array(Cubix_Application::getId(), array(APP_ED,APP_EG_CO_UK,APP_EG,APP_AE))){
            $unsetEscortCount = 0;

            foreach ($escorts as $k => $escort) {

                // Check url work
                $result = $this->__checkUrlExist($escort->source_url);

                if($checkSourceUrl == 1){
                    if(!$result){
                        unset($escorts[$k]);
                        $unsetEscortCount ++;
                    }
                }

                if($checkSourceUrl == 2){
                    if($result){
                        unset($escorts[$k]);
                        $unsetEscortCount ++;
                    }
                }

            }

            $count = $count - $unsetEscortCount;
            if($unsetEscortCount > 0){
                $escorts = array_values($escorts);
            }
        }

        return array('result' => $escorts, 'count' => $count);
    }

	private function __checkUrlExist($url){

		if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
	        $url = "http://".$url;
	    }
		if (filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED)) {

			// if(Cubix_Application::getId() == APP_EG_CO_UK){
			// 	$agent = 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html';

			// 	$ch = curl_init();
			// 	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			// 	curl_setopt($ch, CURLOPT_VERBOSE, true);
			// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			// 	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			// 	curl_setopt($ch, CURLOPT_USERAGENT, $agent);
			// 	curl_setopt($ch, CURLOPT_URL,$url);
			// 	$result = curl_exec($ch);
			// 	var_dump($result);
			// 	exit();
		 //    }

		    try { 
			    $client = new Zend_Http_Client($url, array(
				    'maxredirects' => 0,
				    'strict'      => false,
				    'adapter'   => 'Zend_Http_Client_Adapter_Curl',
	        		'curloptions' => array(CURLOPT_FOLLOWLOCATION => true),
				    'timeout'      => 5));

				$resp = $client->request('GET');
			} catch (Zend_Http_Client_Adapter_Exception $e) {
			   return false;
			} catch (Zend_Some_other_Exception $e) {
			   return false;
			} catch (Exception $e) {
			    return false;
			}

			// $client = new Zend_Http_Client($url, array(
			//     'maxredirects' => 0,
			//     'strict'      => false,
			//     'adapter'   => 'Zend_Http_Client_Adapter_Curl',
   //      		'curloptions' => array(CURLOPT_FOLLOWLOCATION => true),
			//     'timeout'      => 15));

			// $resp = $client->request('GET');

			// if(Cubix_Application::getId() == APP_EG_CO_UK){
			// 	self::getAdapter()->query("UPDATE teeest SET teest = ? WHERE id = ?", array($resp, 1));
			// }
			
			// not need with maxredirects
			//$redirectCount  = $client->getRedirectionsCount();
			//&& $redirectCount == 0
			if($resp->getStatus() == 200 ){
				return true;
			}
		}
		return false;
	}

	/**
	 * @param $sql
	 * @param bool|false $return_sql
	 * @return array
     */
	private function _getAll($sql, $return_sql = false)
	{
      
		$_sql = array(
			'tables' => array('escorts e'),
			'fields' => array(
				'e.id', 'bu.username AS sales_person', 'bu.type AS sales_person_type', 'a.title AS signup_from', 'a.id AS application_id', '_c.iso AS app_iso'
			),
			'joins' => array(
				'INNER JOIN users u ON e.user_id = u.id',
				'INNER JOIN applications a ON a.id = u.application_id',
				/*'LEFT JOIN users_last_login ull ON ull.user_id = u.id',*/
				'LEFT JOIN countries _c ON a.country_id = _c.id',
				'LEFT JOIN backend_users bu ON bu.id = u.sales_user_id',
				/*'LEFT JOIN order_packages ord_p ON ord_p.escort_id = e.id AND ord_p.application_id = ' . Cubix_Application::getId()*/
			),
			'where' => array(

			),
			'group' => 'e.id',
		);

		/* --> START Custom Filter Logic */

		$filter = $sql['where'];

        if (isset($filter['fg.is_featured = ?'])) {

            $isFeatured = $filter['fg.is_featured = ?'];
            if ($isFeatured == 1) {
                $sql['joins'][] = ' INNER JOIN featured_girls fg ON fg.escort_id = e.id';
            }else if( $isFeatured == 0 ){
                $sql['joins'][] = ' LEFT JOIN featured_girls fg ON fg.escort_id = e.id';
                $filter[] = 'fg.escort_id IS NULL';
            }
            unset($filter['fg.is_featured = ?']);
        }

        if (isset($filter['top_prio'])) {
            $filter[] = "u.last_login_date IS NULL AND e.last_hand_verification_date IS NULL AND u.email = 'no@no.com' AND 4 < ( SELECT COUNT( id ) FROM escort_comments WHERE `comment` LIKE '%auto sms and mail for confirmation%' AND escort_id = e.id ) ";

            unset($filter['top_prio']);
        }

		// Get Only Tour Girls
		if ( isset($filter['only_city_tour']) && $filter['only_city_tour'] ) {
			$sql['joins'][] = 'INNER JOIN tours t ON t.escort_id = e.id';
			$filter[] = 'DATE(t.date_from) <= DATE(NOW())';
			$filter[] = 'DATE(t.date_to) >= DATE(NOW())';
		}

        if(isset($filter['ep.website']) && strlen($filter['ep.website'])) {
            $filter[] = "ep.website LIKE '%{$filter['ep.website']}%'";
            unset($filter['ep.website']);
        }

		if(isset($filter['map_activated'])) {
            $filter[] = "ep.location_lng IS NOT NULL AND ep.location_lng != '' ";
            $filter[] = "ep.location_lat IS NOT NULL AND ep.location_lat != '' ";

		    unset($filter['map_activated']);
        }


		if(isset($filter['e.country_id = ?']) && $filter['e.country_id = ?'] == 'all_except_usa') {
			unset($filter['e.country_id = ?']);
			$filter[] = 'e.country_id != 68';
		}

		if(isset($filter['e.country_id = ?']) && $filter['e.country_id = ?'] == 'all_except_usa_ca') {
			unset($filter['e.country_id = ?']);
			$filter[] = 'e.country_id != 68 and e.country_id != 10';
		}
        if(isset($filter['e.country_id = ?']) && $filter['e.country_id = ?'] == 'all_except_usa_fr') {
            unset($filter['e.country_id = ?']);
            $filter[] = 'e.country_id != 68 and e.country_id != 23';
        }


		if (isset($filter['has_hand_verification_date']) && $filter['has_hand_verification_date'])
		{			
			if ( $filter['has_hand_verification_date'] == 'yes' ) {
				$filter[] = 'e.last_hand_verification_date IS NOT NULL';
			} else if ( $filter['has_hand_verification_date'] == 'no' ) {
				$filter[] = 'e.last_hand_verification_date IS NULL';
			}
		}
		unset($filter['has_hand_verification_date']);

		if (isset($filter['hand_verification_date_from']) && $filter['hand_verification_date_from'])
		{			
			$filter["DATE(e.last_hand_verification_date) >= DATE(FROM_UNIXTIME(?))"] = $filter['hand_verification_date_from'];
		}
		unset($filter['hand_verification_date_from']);

		if (isset($filter['hand_verification_date_to']) && $filter['hand_verification_date_to'])
		{			
			$filter["DATE(e.last_hand_verification_date) <= DATE(FROM_UNIXTIME(?))"] = $filter['hand_verification_date_to'];
		}
		unset($filter['hand_verification_date_to']);


		unset($filter['only_city_tour']);

		if (isset($filter['e.is_suspicious']) && $filter['e.is_suspicious'])
		{
			$filter[] = '(e.is_suspicious = 1 OR e.is_suspicious = 2)';
		}
		unset($filter['e.is_suspicious']);
		
		if ( isset($filter['only_premium']) && $filter['only_premium'] ) {
			$sql['joins'][] = 'INNER JOIN packages p ON ord_p.package_id = p.id';
			$sql['joins'][] = 'INNER JOIN order_packages ord_p ON ord_p.escort_id = e.id';
			$filter[] = 'ord_p.status = 2 AND p.is_premium = 1';
		}
		
		unset($filter['only_premium']);
		
		if (isset($filter['is_vip_request']) && $filter['is_vip_request'])
		{
			$sql['joins'][] = 'INNER JOIN vip_requests vp ON vp.escort_id = e.id';
			$filter[] = 'vp.`status` IN (2, 4) AND vp.id = (SELECT MAX(id) FROM vip_requests WHERE escort_id = vp.escort_id)';
		}
		unset($filter['is_vip_request']);
		
		if (isset($filter['is_vip_package']) && $filter['is_vip_package'])
		{
			if (Cubix_Application::getId() == APP_BL) {
				$sql['joins'][] = 'INNER JOIN order_packages ord_p_is_vip ON ord_p_is_vip.escort_id = e.id';
				$filter[] = 'ord_p_is_vip.status = 2 AND ord_p_is_vip.package_id IN (21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31)';
			}
		}
		unset($filter['is_vip_package']);
		
		if ( isset($filter['show_small_photos']) ) {
			$filter[] = 'e.has_big_photos = 0';
		}
		unset($filter['show_small_photos']);

        // Get Last Login Info
        if ((isset($filter['ul.ip = ?']) && $filter['ul.ip = ?']) || (isset($filter['ul.country_id']) && $filter['ul.country_id'])) {
            $sql['joins'][] = 'INNER JOIN users_last_login ul ON ul.user_id = e.user_id';

            if ((isset($filter['ul.country_id']) && $filter['ul.country_id'])) {
                if (Cubix_Application::getId() == APP_ED) {
                    $filter['ul.country_id'];
                    $filter[] = 'ul.ip IS NOT NULL';

                }
            }
        }
		
		// Get Disabled Photos
		if ( (isset($filter['photo_type']) && $filter['photo_type'])  ) {
			$sql['joins'][] = 'INNER JOIN escort_photos epht ON epht.escort_id = e.id AND epht.type ='.$filter['photo_type'] ;
		}
		unset($filter['photo_type']);

		if ( isset($filter['profile_due']) ) {
			$filter[] = 'e.due_date IS NOT NULL';
		}
		unset($filter['profile_due']);

		if ( isset($filter['showname_is_not_null']) ) {
			$filter[] = 'e.showname IS NOT NULL';
		}
		unset($filter['showname_is_not_null']);

		if ( isset($filter['has_website']) ) {
			switch ($filter['has_website']){
			case 1: //has website
				$filter[] = '( ep.website IS NOT NULL AND CHAR_LENGTH(TRIM(ep.website)) > 0 )';
				BREAK;
			case 2:  //doesn't have website
				$filter[] = '( ep.website IS NULL OR CHAR_LENGTH(TRIM(ep.website)) = 0 )';
				BREAK;
			}
			unset($filter['has_website']);
		}
		
		if (isset($filter['website']) && $filter['website'])
		{
			$filter[] = 'ep.website LIKE "%' . $filter['website'] . '%"';
		}
		
		if ( isset($filter['phone_sms_verified']) ) {
			switch ($filter['phone_sms_verified']){
			case 1: //verified
				$filter[] = ' ep.phone_sms_verified = 1 ';
				BREAK;
			case 2:  //not verified
				$filter[] = ' ep.phone_sms_verified = 0 ';
				BREAK;
			}
			unset($filter['phone_sms_verified']);
		}
		
		if(isset($filter['no_phone_number'])){
			$filter[] = '( ep.phone_number IS NULL OR CHAR_LENGTH(TRIM(ep.phone_number)) = 0 )';
			unset($filter['no_phone_number']);
		}

		if(isset($filter['has_phone_number'])){
			$filter[] = '( ep.phone_number IS NOT NULL AND CHAR_LENGTH(TRIM(ep.phone_number)) > 0 )';
			unset($filter['has_phone_number']);
		}
		
		if(isset($filter['phone_number']) && $filter['phone_number']){
			$filter[] = '( ep.contact_phone_parsed LIKE  "%'. $filter['phone_number'] .'%"  OR ep.phone_exists LIKE "%' . $filter['phone_number'].'%" OR ep.phone_additional_1 LIKE  "%'. $filter['phone_number'] .'%" OR ep.phone_additional_2 LIKE  "%'. $filter['phone_number'] .'%" )';
			unset($filter['phone_number']);
		}
		
		if ( isset($filter['ep.admin_verified']) && $filter['ep.admin_verified'] ) {
			if( $filter['ep.admin_verified'] == 10){
				$filter[] = 'ep.admin_verified NOT IN(4,5,6)';
			}
			else{
				$filter[] = 'ep.admin_verified = ' .$filter['ep.admin_verified'];
			}
			unset($filter['ep.admin_verified']);
		}
		// Get Only Escorts With At Least One Photo
		if ( isset($filter['with_photo']) && $filter['with_photo'] ) {
			$sql['joins'][] = 'INNER JOIN escort_photos ephw ON ephw.escort_id = e.id';
		}
		
		if ( isset($filter['has_video']) && $filter['has_video'] ) {
			switch ( $filter['has_video'] ) {
				case 1: // Has video
					$sql['joins'][] = 'INNER JOIN video v ON v.escort_id = e.id';
					break;
				case 2: // Doesn't have video
					$sql['joins'][] = 'LEFT JOIN video v ON v.escort_id = e.id';
					$filter[] = ' v.escort_id IS NULL ';
					break;
			}
			unset($filter['has_video']);
		}
                
		//-------------------------------------------------------------------------------------------------
		if (isset($filter['text_done']) && $filter['text_done'])
		{	
			switch ($filter['text_done']){
			case 1: // HAS 
				$filter[] = ' e.text_copyrighter_id > 0 ';
				BREAK;
			case 2:  //HAS NOT 
				$filter[] = ' e.text_copyrighter_id IS NULL ';
				BREAK;
			}
			unset($filter['text_done']);
		}
//		-----------------------------------------------------------------------------------------------------------
		if ( isset($filter['escort_comment_by']) && $filter['escort_comment_by'] ) {
			if ( !$filter['escort_comment']) {
				$sql['joins'][] = 'LEFT JOIN escort_comments ec ON ec.escort_id = e.id';
			}
			$filter[] = ' ec.sales_user_id  = '.$filter['escort_comment_by'];
			unset($filter['escort_comment_by']);
			
			if (isset($filter['last_comment_by']) && $filter['last_comment_by'])
			{
				$filter[] = ' ec.date = (SELECT MAX(date) FROM escort_comments WHERE escort_id = e.id)';
				unset($filter['last_comment_by']);
			}
		}
		elseif (isset($filter['last_comment_by']))
			unset($filter['last_comment_by']);
		
		if ( isset($filter['escort_reviews']) && $filter['escort_reviews'] ) {
			switch ( $filter['escort_reviews'] ) {
				case 1: // Has escort review
					$filter[] = ' e.reviews > 0 ';
					break;
				case 2: // Doesn't have
					$filter[] = ' e.reviews = 0 ';
					break;
			}
			
			unset($filter['escort_reviews']);
		}
		
		if ( isset($filter['escort_comment']) && $filter['escort_comment'] ) {
			switch ( $filter['escort_comment'] ) {
				case 1: // Has escort comment
					$sql['joins'][] = 'INNER JOIN escort_comments ec ON ec.escort_id = e.id';
					break;
				case 2: // Doesn't have
					$sql['joins'][] = 'LEFT JOIN escort_comments ec ON ec.escort_id = e.id';
					$filter[] = ' ec.comment IS NULL ';
					break;
			}
			
			unset($filter['escort_comment']);
		}
		if ( isset($filter['time_zone']) && $filter['time_zone'] ) {
			$sql['joins'][] = 'INNER JOIN cities cts ON e.city_id = cts.id';
			$filter[] = ' cts.time_zone_id  = '.$filter['time_zone'];
			unset($filter['time_zone']);
		}
		
		if ( isset($filter['no_id_card']) && $filter['no_id_card'] ) {
			$sql['joins'][] = 'LEFT JOIN verify_requests vr_req ON vr_req.escort_id = e.id';
			$filter[] = ' vr_req.escort_id IS NULL ' ;
			unset($filter['no_id_card']);
		}
		
		if ( isset($filter['admin_comment_text']) && $filter['admin_comment_text'] ) {
			if (!in_array('LEFT JOIN escort_comments ec ON ec.escort_id = e.id', $sql['joins']))
				$sql['joins'][] = 'INNER JOIN escort_comments ec ON ec.escort_id = e.id';
			$filter[] = ' ec.comment LIKE "%' . $filter['admin_comment_text'] . '%" ' ;
			unset($filter['admin_comment_text']);
		}
		
		if ( isset($filter['has_active_reviews']) && $filter['has_active_reviews'] ) {
			$filter[] = 'e.reviews > 0 ' ;
			unset($filter['has_active_reviews']);
		}
		
		if ( isset($filter['is_pornstar']) && $filter['is_pornstar'] ) {
			$filter[] = 'e.is_pornstar = 1 ' ;
			unset($filter['is_pornstar']);
		}
		
		if ( isset($filter['bad_photo_history']) && $filter['bad_photo_history'] ) {
			$filter[] = 'e.bad_photo_history = 1 ' ;
			unset($filter['bad_photo_history']);
		}
		
		if ( isset($filter['gallery_checked']) ) {
			switch ($filter['gallery_checked']){
			case 1: //checked
				$filter[] = ' e.gallery_is_checked = 1 ';
				BREAK;
			case 2:  //not checked
				$filter[] = ' e.gallery_is_checked = 0 ';
				BREAK;
			}
			unset($filter['gallery_checked']);
		}
		if ( isset($filter['about_me_informed']) ) {
			switch ($filter['about_me_informed']){
			case 1: //informed
				$filter[] = ' e.about_me_informed = 1 ';
				BREAK;
			case 2:  //not informed
				$filter[] = ' e.about_me_informed = 0 ';
				BREAK;
			}
			unset($filter['about_me_informed']);
		}
		
		if (isset($filter['show_popunder']) && $filter['show_popunder'])
		{	
			switch ($filter['show_popunder']){
			case 1: // IS IN 
				$filter[] = ' e.show_popunder = 1 ';
				BREAK;
			case 2:  //NOT IN 
				$filter[] = ' e.show_popunder = 0 ';
				BREAK;
			}
			unset($filter['show_popunder']);
		}
		
		if (isset($filter['large_popunder']) && $filter['large_popunder'])
		{	
			switch ($filter['large_popunder']){
			case 1: // HAS 
				$filter[] = ' e.has_large_popunder = 1 ';
				BREAK;
			case 2:  //HAS NOT 
				$filter[] = ' e.has_large_popunder = 0 ';
				BREAK;
			}
			unset($filter['large_popunder']);
		}
		// <editor-fold defaultstate="collapsed" desc="Filtration by SEO Data">
		$join_seo_data = false;
		if ( isset($filter['seo_data']) && $filter['seo_data'] && $filter['seo_data'] == 1 ) {
			$filter[] = 'sei.id IS NOT NULL';
			$join_seo_data = true;
		}
		else if ( isset($filter['seo_data']) && $filter['seo_data'] && $filter['seo_data'] == 2 ) {			
			$filter[] = 'sei.id IS NULL';
			$join_seo_data = true;
		}
		else if ( isset($filter['seo_data']) && $filter['seo_data'] && $filter['seo_data'] == 3 ) {			
			$filter[] = 'CHAR_LENGTH(sei.footer_text_' . Cubix_Application::getDefaultLang(Zend_Controller_Front::getInstance()->getRequest()->application_id) . ') > 0';
			$join_seo_data = true;
		}
		else if ( isset($filter['seo_data']) && $filter['seo_data'] && $filter['seo_data'] == 4 ) {			
			$filter[] = 'CHAR_LENGTH(sei.footer_text_' . Cubix_Application::getDefaultLang(Zend_Controller_Front::getInstance()->getRequest()->application_id) . ') = 0';
			$join_seo_data = true;
		}
		//Join only when need
		if ( $join_seo_data ) {
			$sql['joins'][] = 'LEFT JOIN seo_entities si ON si.slug = \'escort\'';
			$sql['joins'][] = 'LEFT JOIN seo_entity_instances sei ON sei.primary_id = e.id AND sei.entity_id = si.id';
		}

		unset($filter['seo_data']);

		if (isset($filter['has_private_photos']) && $filter['has_private_photos'])
		{			
			$filter[] = 'eph.type = 3';
			$sql['joins'][] = 'INNER JOIN escort_photos eph ON eph.escort_id = e.id';
		}
		unset($filter['has_private_photos']);

		if (isset($filter['has_not_approved_pics']) && $filter['has_not_approved_pics'])
		{			
			$filter[] = 'ephapp.is_approved = 0';
			$sql['joins'][] = 'INNER JOIN escort_photos ephapp ON ephapp.escort_id = e.id';
		}
		unset($filter['has_not_approved_pics']);

		if (isset($filter['profile_type']) && $filter['profile_type'])
		{			
			$filter[] = 'e.profile_type = ' . $filter['profile_type'];
		}
		unset($filter['profile_type']);

		if (isset($filter['photoshop']) && $filter['photoshop'])
		{			
			$filter[] = 'e.verified_status = ' . Model_Verifications_Requests::PHOTOSHOP;
		}
		unset($filter['photoshop']);
			
		// </editor-fold>
		

		
		// <editor-fold defaultstate="collapsed" desc="Filtration by Bubble Messages">
		if ( isset($filter['bubble_message']) && $filter['bubble_message'] ) {
			switch ( $filter['bubble_message'] ) {
				case 1: // Has bubble messages
					$sql['joins'][] = 'INNER JOIN escort_bubbletexts eb ON eb.escort_id = e.id AND eb.status = 1';
					//$filter[] = 'CHAR_LENGTH(ep.about_' . Cubix_Application::getDefaultLang() . ') > 0';
					break;
				case 2: // Doesn't have
					$sql['joins'][] = 'LEFT JOIN escort_bubbletexts eb ON eb.escort_id = e.id';
					$filter[] = ' eb.text IS NULL ';
					break;
			}

			unset($filter['bubble_message']);
		}
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Filtration by Slogan">
		if ( isset($filter['slogan']) && $filter['slogan'] ) {
			switch ( $filter['slogan'] ) {
				case 1: // Has slogan
					$sql['joins'][] = 'INNER JOIN escort_slogans esl ON esl.escort_id = e.id AND esl.status = 1';
					break;
				case 2: // Doesn't have
					$sql['joins'][] = 'LEFT JOIN escort_slogans esl ON esl.escort_id = e.id';
					$filter[] = ' esl.text IS NULL ';
					break;
			}

			unset($filter['slogan']);
		}
		// </editor-fold>
		
		// <editor-fold defaultstate="collapsed" desc="Filtration by About Text">
		if ( isset($filter['about_text']) && $filter['about_text'] ) {
			switch ( $filter['about_text'] ) {
				case 1: // Has about text
					$filter[] = 'CHAR_LENGTH(ep.about_' . $filter["about_text_lang"] . ') > 0';
					break;
				case 2: // Doesn't have about text
					$filter[] = 'CHAR_LENGTH(ep.about_' . $filter["about_text_lang"] . ') = 0';
					break;
				case 3: // Has blacklisted words 
					$filter[] = 'ep.about_has_bl = 1';
					break;
				case 4: // Doesn't have about text in any of langs
					$langs = Cubix_I18n::getLangs(true);
					$w = '(';
					$wa = array();

					foreach ($langs as $i => $lang)
					{
						$wa[] =  'CHAR_LENGTH(ep.about_' . $lang . ') = 0';
					}

					$w .= implode(' AND ', $wa);
					$w .= ')';
					$filter[] = $w;
					break;
			}

			unset($filter['about_text']);
		}
		unset($filter["about_text_lang"]);
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Search in About me Text">
		if ( isset($filter['about_me']) && $filter['about_me'] ) {
			$langs = Cubix_Application::getLangs();
			$sql_string = ' (';
			foreach ($langs as $lang){
				$sql_string .= ' ep.about_'.$lang->id.' LIKE "%'. $filter['about_me'] .'%"  OR ';
			}
			$sql_string = substr_replace($sql_string ,")",-3);
			
			$filter[] = $sql_string;
			
			unset($filter['about_me']);
		}
		// </editor-fold>
		
		// <editor-fold defaultstate="collapsed" desc="Filtration by Login (ever)">
		if ( isset($filter['ever_login']) && $filter['ever_login'] ) {
			switch ( $filter['ever_login'] ) {
				case 1: // Did Login
					$filter[] = ' u.last_login_date IS NOT NULL ';
					break;
				case 2: // Didn't login
					$filter[] = ' u.last_login_date IS NULL ';
					break;
			}

			unset($filter['ever_login']);
		}
		// </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Filtration by Login (once)">
        if ( isset($filter['signed_in_once']) && $filter['signed_in_once'] ) {
            $once_signed_in_users = parent::getAdapter()->fetchCol("SELECT GROUP_CONCAT(DISTINCT ull.user_id) FROM users_last_login as ull GROUP BY ull.user_id HAVING count(ull.user_id) = 1");
            $filter["u.id in(?)"]= new Zend_Db_Expr(implode(',', $once_signed_in_users));
            unset($filter['signed_in_once']);
        }
        if ( isset($filter['has_blacklisted_ips']) && $filter['has_blacklisted_ips'] ) {
            $has_blacklisted_ips = parent::getAdapter()->fetchCol("SELECT GROUP_CONCAT(DISTINCT ull.user_id) FROM users_last_login as ull  WHERE ull.ip_blacklisted = 1 GROUP BY ull.user_id");
            $filter["u.id in(?)"]= new Zend_Db_Expr(implode(',', $has_blacklisted_ips));
            unset($filter['has_blacklisted_ips']);
        }
        // </editor-fold>
		
		// <editor-fold defaultstate="collapsed" desc="Filtration by Date">
		if ( isset($filter['filter_by']) && $filter['filter_by'] ) {
			if ( $filter['filter_by'] == 'registration_date' ) {
				$date_field = 'e.date_registered';
			}
			elseif ( $filter['filter_by'] == 'last_login_date' ) {
				$date_field = 'u.last_login_date';
			}
			elseif ( $filter['filter_by'] == 'last_modified_date' ) {
				$date_field = 'e.date_last_modified';
			}
			elseif ( $filter['filter_by'] == 'last_modified_date_backend' ) {
				$date_field = 'e.date_last_modified_backend';
			}
            elseif ( $filter['filter_by'] == 'package_expiration_date' ) {
                $date_field = 'op_exp.expiration_date';
                $_sql['joins'][] = 'INNER JOIN order_packages op_exp ON op_exp.escort_id = e.id AND op_exp.order_id IS NOT NULL AND op_exp.status IN (2,3)';
            }
			elseif ( $filter['filter_by'] == 'photo_suspicious_date' ) {
				$date_field = 'e.photo_suspicious_date';
			}
			elseif  ( $filter['filter_by'] == 'admin_disabled' ) {
				$date_field = 'es.date';
				$_sql['joins'][] = 'INNER JOIN escort_statuses es ON e.id = es.escort_id AND es.status & '.Cubix_EscortStatus::ESCORT_STATUS_ADMIN_DISABLED; 
			}
			elseif ($filter['filter_by'] == 'photo_changed' ){
				$date_field = 'ephc.creation_date';
				$sql['joins'][] = 'INNER JOIN escort_photos ephc ON ephc.escort_id = e.id';
			}
			elseif ($filter['filter_by'] == 'phone_changed' ){
				$date_field = 'e.phone_changed';
			}
			$both_date_exist = false;
			if ( $filter['date_from'] && $filter['date_to'] ) {
				$both_date_exist = true;
				if($filter['had_running_package'] ){
					$filter[] = "  (( DATE(op_r_pack.date_activated) >= DATE(FROM_UNIXTIME(".$filter['date_from'].")) AND DATE(op_r_pack.date_activated) <= DATE(FROM_UNIXTIME(".$filter['date_to']."))) 	OR (DATE(op_r_pack.expiration_date) >= DATE(FROM_UNIXTIME(".$filter['date_from'].")) AND DATE(op_r_pack.expiration_date) <= DATE(FROM_UNIXTIME(".$filter['date_to']."))))";
					$_sql['joins'][] = 'INNER JOIN order_packages op_r_pack ON op_r_pack.escort_id = e.id';
				}
				if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) ) {
					$filter["DATE($date_field) = DATE(FROM_UNIXTIME(?))"] = $filter['date_from'];
									
				}
			}

            if ( $filter['filter_by'] === 'did_login' )
            {
                $filter[] = 'u.last_login_date IS NOT NULL ';
            }else if( $filter['filter_by'] === 'did_not_login' )
            {
                $filter[] = 'u.last_login_date IS NULL ';
            }else if ( $filter['filter_by'] === 'one_login' )
            {
                $filter[] = '(SELECT COUNT(DISTINCT(`login_date`)) FROM users_last_login WHERE user_id = u.id) = 1 ';
            }

			if ( $filter['date_from'] ) {
				$filter["DATE({$date_field}) >= DATE(FROM_UNIXTIME(?))"] = $filter['date_from'];
				
				if($filter['had_running_package'] && !$both_date_exist){
					$filter["DATE(ord_p.date_activated) >= DATE(FROM_UNIXTIME(?))"] = $filter['date_from'];
				}
			}

			if ( $filter['date_to'] ) { 
				$filter["DATE({$date_field}) <= DATE(FROM_UNIXTIME(?))"] = $filter['date_to'];
				if($filter['had_running_package'] && !$both_date_exist){
					$filter["DATE(ord_p.expiration_date) <= DATE(FROM_UNIXTIME(?))"] = $filter['date_to'];
					
				}
			}
		}

		unset($filter['filter_by']);
		unset($filter['date_from']);
		unset($filter['date_to']);
		unset($filter['had_running_package']);
		// </editor-fold>
		
		// <editor-fold defaultstate="collapsed" desc="Check Fields for ED">
		
		if ( $filter['check_date_from'] || $filter['check_date_to'] ) {
			if (!in_array('INNER JOIN escort_comments ec ON ec.escort_id = e.id AND ec.is_check_profile = 1', $sql['joins']))
				$sql['joins'][] = 'INNER JOIN escort_comments ec ON ec.escort_id = e.id AND ec.is_check_profile = 1';
			
			if ( $filter['check_date_from'] && $filter['check_date_to'] ) {
				if ( date('d.m.Y', $filter['check_date_from']) == date('d.m.Y', $filter['check_date_to']) ) {
					$filter[" DATE(ec.date) = DATE(FROM_UNIXTIME(?))"] = $filter['check_date_from'];
				}
			}
			if ( $filter['check_date_from'] ) {
				$filter[" DATE(ec.date) >= DATE(FROM_UNIXTIME(?)) "] = $filter['check_date_from'];
			}

			if ( $filter['check_date_to'] ) {
				$filter[" DATE(ec.date) <= DATE(FROM_UNIXTIME(?)) "] = $filter['check_date_to'];
			}
			unset($filter['check_date_to']);
			unset($filter['check_date_from']);
		}
		
		if ( $filter['next_check_date_from'] || $filter['next_check_date_to'] ) {
			if (!in_array('INNER JOIN escort_comments ec ON ec.escort_id = e.id AND ec.is_check_profile = 1', $sql['joins']))
				$sql['joins'][] = 'INNER JOIN escort_comments ec ON ec.escort_id = e.id AND ec.is_check_profile = 1';
			
			if ( $filter['next_check_date_from'] && $filter['next_check_date_to'] ) {
				if ( date('d.m.Y', $filter['next_check_date_from']) == date('d.m.Y', $filter['next_check_date_to']) ) {
					$filter[" DATE(ec.next_check_date) = DATE(FROM_UNIXTIME(?))"] = $filter['next_check_date_from'];
				}
			}
			if ( $filter['next_check_date_from'] ) {
				$filter[" DATE(ec.next_check_date) >= DATE(FROM_UNIXTIME(?)) "] = $filter['next_check_date_from'];
			}

			if ( $filter['next_check_date_to'] ) {
				$filter[" DATE(ec.next_check_date) <= DATE(FROM_UNIXTIME(?)) "] = $filter['next_check_date_to'];
			}
			unset($filter['next_check_date_to']);
			unset($filter['next_check_date_from']);
		}
		
		if ( isset($filter['check_comment_text']) && $filter['check_comment_text'] ) {
			if (!in_array('INNER JOIN escort_comments ec ON ec.escort_id = e.id AND ec.is_check_profile = 1', $sql['joins']))
				$sql['joins'][] = 'INNER JOIN escort_comments ec ON ec.escort_id = e.id AND ec.is_check_profile = 1';
			$filter[] = ' ec.comment LIKE "%' . $filter['check_comment_text'] . '%" ' ;
			unset($filter['check_comment_text']);
		}
		
		if ( isset($filter['ec.check_status = ?']) && $filter['ec.check_status = ?']) {
			if (!in_array('INNER JOIN escort_comments ec ON ec.escort_id = e.id AND ec.is_check_profile = 1', $sql['joins']))
				$sql['joins'][] = 'INNER JOIN escort_comments ec ON ec.escort_id = e.id AND ec.is_check_profile = 1';
			
		}
		
		if ( isset($filter['check_by_backend_user']) && $filter['check_by_backend_user'] ) {
			if (!in_array('INNER JOIN escort_comments ec ON ec.escort_id = e.id AND ec.is_check_profile = 1', $sql['joins']))
				$sql['joins'][] = 'INNER JOIN escort_comments ec ON ec.escort_id = e.id AND ec.is_check_profile = 1';
			$filter[] = ' ec.sales_user_id  = '.$filter['check_by_backend_user'];
			unset($filter['check_by_backend_user']);
			
			if (isset($filter['last_check_by_backend_user']) && $filter['last_check_by_backend_user'])
			{
				$filter[] = ' ec.date = (SELECT MAX(date) FROM escort_comments WHERE escort_id = e.id AND is_check_profile = 1)';
				
			}
		}
		if ( isset($filter['last_check_by_backend_user']) && $filter['last_check_by_backend_user']) {
			unset($filter['last_check_by_backend_user']);
		}
		
		if ( isset($filter['overdue_pending']) && $filter['overdue_pending']) {
			if (!in_array('INNER JOIN escort_comments ec ON ec.escort_id = e.id AND ec.is_check_profile = 1', $sql['joins']))
				$sql['joins'][] = 'INNER JOIN escort_comments ec ON ec.escort_id = e.id AND ec.is_check_profile = 1';
				$filter[] = " DATE(ec.next_check_date) <= CURRENT_DATE ";
			
			unset($filter['overdue_pending']);
		}
		
		if ( isset($filter['overdue_checked']) && $filter['overdue_checked']) {
			if (!in_array('INNER JOIN escort_comments ec ON ec.escort_id = e.id AND ec.is_check_profile = 1', $sql['joins']))
				$sql['joins'][] = 'INNER JOIN escort_comments ec ON ec.escort_id = e.id AND ec.is_check_profile = 1';
				$filter[" DATE(ec.next_check_date) <= CURRENT_DATE AND ec.check_status = ? "] = Model_Escort_CheckProfile::STATUS_CHECKED_OK;
			
			unset($filter['overdue_checked']);
		}
		
		// </editor-fold>
		 
		
		// Filter by Status
		/*if ( $filter['e.status'] != Model_Escorts::ESCORT_STATUS_DELETED ) {
			if ( $filter['excl_status'] ) {
				$filter = array_merge($filter, $this->_whereStatus2('e.status', null, $filter['excl_status']));
			}
		}*/
		
		if ( isset($filter['u.sales_user_id = ?']) && $filter['u.sales_user_id = ?'] == -1 ) {
			$filter[] = '( u.sales_user_id IS NULL OR u.sales_user_id = 0)';
			unset($filter['u.sales_user_id = ?']);
		}

        if ( isset($filter['u.sales_user_id = ?']) && $filter['u.sales_user_id = ?'] == -96 ) {
            $filter[] = ' ( u.sales_user_id != 96 AND u.sales_user_id != 96 )';
            unset($filter['u.sales_user_id = ?']);
        }
		
		if ( isset($filter['active_package_expr_date']) && $filter['active_package_expr_date'] ) {
			$filter[] = 'e.package_expiration_date IS NOT NULL';
			unset($filter['active_package_expr_date']);
		}
		
		if ( isset($filter['active_package_id']) && $filter['active_package_id'] ) {
			
			$_sql['joins'][] = 'INNER JOIN order_packages op_apack ON op_apack.escort_id = e.id';

			if ( $filter['active_package_id'] == 'no_package' ) {
				$filter[] = 'e.active_package_id IS NULL';
			}
			else if ( $filter['active_package_id'] == 'with_package' ) {
				$filter[] = ' (op_apack.status = 2)';//'e.active_package_id IS NOT NULL';
			}
            else if ( $filter['active_package_id'] == 'with_paid_package' ) {
            	// this is complitely useless two have two options but same results but Katie confirmed and6 aminds want it so pls do not blame me
            	if(Cubix_Application::getId() == APP_A6 ){
            		$filter[] = ' op_apack.status = 2 AND op_apack.package_id != 97 ';
            	}
            	if (Cubix_Application::getId() == APP_ED){
                    $filter[] = ' _p.is_premium > 0 ';
                }
            	else{
                	$filter[] = ' (op_apack.order_id IS NOT NULL)';//'e.active_package_id IS NOT NULL';
            	}
            }
			else if ( $filter['active_package_id'] == 'any_package_exp_zero' ) {
				if(Cubix_Application::getId() == 1){
					$filter[] = ' op_apack.status = 2 AND op_apack.package_id != 100 AND op_apack.package_id != 97 ';
				}
				else{
					$filter[] = ' op_apack.status = 2 AND op_apack.package_id != 97 ';
				}
			}
			else if ( Cubix_Application::getId() == APP_A6 && ($filter['active_package_id'] == 'special_package') ) {
				$filter[] = 'e.active_package_id IN (9,11,12,13,18,22)' ;
			}
			else {				
				$filter[] = 'op_apack.package_id = ' . $filter['active_package_id'] .' AND op_apack.status = 2 ';
			}
			
			//print_r($sql['where']); die;
			unset($filter['active_package_id']);
		}
		

		unset($filter['excl_status']);
		
		
		if(isset($filter['active_package_id']) && $filter['e.need_age_verification']){
			if($filter['e.need_age_verification'] == -1){
				$filter[] = ' e.need_age_verification = 0 ';
			}
			else{
				$filter[' e.need_age_verification = ? '] = $filter['e.need_age_verification'];
			}
			
			unset($filter['e.need_age_verification']);
		}

		if(isset($filter['e.auto_approval']) && $filter['e.auto_approval'] ){
			if($filter['e.auto_approval'] == 1){
				$filter[] = 'e.auto_approval = 0 ';
			}elseif($filter['e.auto_approval'] == 2){
				$filter[] = 'e.auto_approval = 1 ';
			}
			unset($filter['e.auto_approval']);
		}
		
		if ( $filter['e.status'] ) {
			if( Cubix_Application::getId() == APP_EG_CO_UK || Cubix_Application::getId() == APP_EG ){
				if($filter['e.status'] == Model_EscortsV2::ESCORT_STATUS_ACTIVE){ 
					$filter[] = '~e.status & '.Model_EscortsV2::ESCORT_STATUS_DELETED;
				}
			}

			// no need to show inactive escorts when filtering status is "active"
			if( Cubix_Application::getId() == APP_EG_CO_UK || Cubix_Application::getId() == APP_ED ){
				if($filter['e.status'] == Model_EscortsV2::ESCORT_STATUS_ACTIVE){
					$filter[] = '~e.status & '. Model_EscortsV2::ESCORT_STATUS_INACTIVE;
				}
			}
			$filter = array_merge($filter, $this->_whereStatus2('e.status', $filter['e.status']));
		}

		unset($filter['e.status']);


		if ( isset($filter['city']) && $filter['city'] ) {
			//$sql['joins'][] = 'INNER JOIN escort_cities ec ON ec.escort_id = e.id';
            if ( !$filter['search_in_all_cities'])
            {
                $filter[] = 'e.city_id = ' . $filter['city'];
            }else{
                $filter[] = 'ec.city_id = ' . $filter['city'];
                unset($filter['search_in_all_cities']);
            }
			unset($filter['city']);
		}

        if ( isset($filter['search_in_all_cities']) )
        {
            unset($filter['search_in_all_cities']);
        }

		if ( isset($filter['city_zone']) && $filter['city_zone'] ) {
			$sql['joins'][] = 'INNER JOIN escort_cityzones ecz ON ecz.escort_id = e.id';
			$sql['where']['ecz.city_zone_id = ?'] = $filter['city_zone'];
			unset($filter['city_zone']);
		}
		
		if ( isset($filter['has_expo']) ) {
			$sql['joins'][] = 'INNER JOIN order_packages ord_p2 ON ord_p2.escort_id = e.id AND ord_p2.status = 2';
			$sql['joins'][] = 'INNER JOIN order_package_products ord_pp ON ord_pp.order_package_id = ord_p2.id AND ord_pp.product_id = ' . MILAN_EXPO_PRODUCT_ID;
			unset($filter['has_expo']);
		}

		// Like Statements
		foreach ( $filter as $f => $v ) {
			if ( is_int($f) || strpos($f, '?') !== false || strpos($f, 'in(') !== false) continue;

			unset($filter[$f]);
			if ( ! $v ) continue;

            
            
            if(strpos($v,'"') !== false){
                $v = trim($v,'"');
               $filter[$f . ' = ?'] =  $v ;
            }else{
                $v = str_replace('_', '\_', $v);
                $filter[$f . ' LIKE ?'] = '%' . $v . '%';
            }
		}
		
		$sql['where'] = $filter;
		
		/* <-- END Custom Filter Logic */
		 
		$sql = array_merge_recursive($_sql, $sql);
		
		$dataSql = Cubix_Model::getSql($sql, true);
        //echo($dataSql);die;
		$sql['group'] = false;
		$sql['fields'] = array('COUNT(DISTINCT(e.id))');
		$sql['page'] = false;
		$sql['order'] = false;
		$countSql = Cubix_Model::getSql($sql);

		if ( $return_sql ) {
			return array('data_sql' => $dataSql, 'count_sql' => $countSql);
		}
		return array(
			'result' => parent::_fetchAll($dataSql),
			'count' => parent::getAdapter()->fetchOne($countSql)
		);
	}

	public static function _whereStatus2($field, $status, $excl_status = array())
	{
		if ( ! is_null($status) ) { if ( ! is_array($status) ) $status = array($status); }
		else { $status = array(); }
		if ( ! is_array($excl_status) ) $excl_status = array($excl_status);

		$ret = array();

		foreach ( $status as $st ) {
			$ret[] = "({$field} & {$st})";
		}

		foreach ( $excl_status as $st ) {
			$ret[] = " NOT ({$field} & {$st})";
		}

		return $ret;
	}

	public static function _whereStatus($field, $status, $excl_status = array())
	{
		if ( ! is_null($status) ) if ( ! is_array($status) ) $status = array($status);
		if ( ! is_array($excl_status) ) $excl_status = array($excl_status);


		$ret = array();

		if ( is_array($status) ) {
			foreach ( $status as $st ) {
				$ret[] = "({$field} & {$st})";
			}

			$result = implode(' AND ', $ret);
		}

		if ( count($status) && count($excl_status) ) $result .= ' AND ';

		$ret = array();
		foreach ( $excl_status as $st ) {
			$ret[] = " NOT ({$field} & {$st})";
		}

		$result .= implode(' AND ', $ret);

		return $result;
	}

    /**
     * @param $website
     * @return mixed
     */
    public function getProfileByWebsite($website)
    {
        return $this->getAdapter()->query("
			SELECT * 
			FROM escorts
			WHERE source_url LIKE '%$website%'")->fetchAll();
    }

    /**
     * @param $id
     * @param bool $isRedirects
     */
    public function changeSourceUrlRedirection($id, $isRedirects = false)
    {
        $this->getAdapter()->query("UPDATE escorts SET souorce_url_redirects = " . (int) $isRedirects . " WHERE id = " . $id);
    }
}