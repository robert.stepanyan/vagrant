<?php

class Model_Escort_List extends Cubix_Model
{
	public function __construct($rowData = null)
	{
		if ( !is_null($rowData) ) {
			parent::__construct($rowData);
		}
		else {
			parent::__construct();
		}
	}

	public function getForAutocompleter($page, $per_page, $filter, $sort_field, $sort_dir, &$count = 0)
	{
		$sql = array(
			'fields' => array(
				'u.id AS user_id', 'u.username', 'e.showname',
				'eph.hash AS photo_hash', 'eph.ext AS photo_ext', 'eph.status AS photo_status',
				'ag.name AS agency_name', 'ag.id AS agency_id',	'u.balance'
			),
			'joins' => array(
				'LEFT JOIN agencies ag ON ag.id = e.agency_id',
				'LEFT JOIN escort_profiles ep ON ep.escort_id = e.id',
				'LEFT JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1'
			),
			'where' => $filter,
			'order' => array($sort_field, $sort_dir),
			'page' => array($page, $per_page)
		);

		$data = $this->_getAll($sql);

		$count = $data['count'];
		return $data['result'];
	}

	public function getForAgencies($page, $per_page, $filter, $sort_field, $sort_dir, &$count = 0)
	{
		$sql = array(
			'fields' => array(
				'u.username', 'e.showname', 'ep.contact_phone', 'ep.contact_phone_parsed', 'ep.contact_email'
			),
			'joins' => array(
				'LEFT JOIN escort_profiles ep ON ep.escort_id = e.id'
			),
			'where' => $filter,
			'order' => array($sort_field, $sort_dir),
			'page' => array($page, $per_page)
		);

		$data = $this->_getAll($sql);

		$count = $data['count'];
		return $data['result'];
	}

	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count = 0)
	{
		$sql = array(
			'fields' => array(
				'u.username', 'e.showname', 'UNIX_TIMESTAMP(e.date_last_modified) AS date_last_modified',
				'e.status', 'UNIX_TIMESTAMP(e.date_registered) AS date_registered', 'COUNT(DISTINCT(pu.revision)) AS revs_count',
				'COUNT(pu1.revision) AS is_updated', 'ag.name AS agency_name', 'ag.id AS agency_id', 'u.id AS user_id'
			),
			'joins' => array(
				'LEFT JOIN escort_profiles ep ON ep.escort_id = e.id',
				'LEFT JOIN profile_updates pu ON pu.escort_id = e.id',
				'LEFT JOIN profile_updates pu1 ON pu1.escort_id = e.id AND pu1.status = ' . Model_Escort_Profile::REVISION_STATUS_NOT_APPROVED,
				'LEFT JOIN escort_cities ec ON ec.escort_id = e.id',
				'LEFT JOIN escort_cityzones ecz ON ecz.escort_id = e.id',
				'LEFT JOIN cities c ON c.id = ec.city_id',
				'LEFT JOIN agencies ag ON ag.id = e.agency_id'
			),
			'where' => $filter,
			'order' => array($sort_field, $sort_dir),
			'page' => array($page, $per_page)
		);

		$data = $this->_getAll($sql);

		$count = $data['count'];
		return $data['result'];
	}

	private function _getAll($sql)
	{
		$_sql = array(
			'tables' => array('users u'),
			'fields' => array(
				'e.id', 'bu.username AS sales_person', 'a.title AS signup_from', 'u.application_id', '_c.iso AS app_iso'
			),
			'joins' => array(
				'INNER JOIN escorts e ON e.user_id = u.id',
				'INNER JOIN applications a ON a.id = u.application_id',
				'LEFT JOIN backend_users bu ON bu.id = u.sales_user_id',
				'LEFT JOIN countries _c ON a.country_id = _c.id'
			),
			'where' => array(

			),
			'group' => 'e.id'
		);

		/* --> START Custom Filter Logic */
		$filter = $sql['where'];
		// Get Only Tour Girls
		if ( isset($filter['only_city_tour']) && $filter['only_city_tour'] ) {
			$sql['joins'][] = 'INNER JOIN tours t ON t.escort_id = e.id';
			$filter[] = 'DATE(t.date_from) <= DATE(NOW())';
			$filter[] = 'DATE(t.date_to) >= DATE(NOW())';
		}

		unset($filter['only_city_tour']);

		// Get Only Escorts With At Least One Photo
		if ( isset($filter['with_photo']) && $filter['with_photo'] ) {
			$sql['joins'][] = 'INNER JOIN escort_photos eph ON eph.escort_id = e.id';
		}

		// Filter by Date
		if ( isset($filter['filter_by']) && $filter['filter_by'] ) {
			if ( $filter['filter_by'] == 'registration_date' ) {
				$date_field = 'e.date_registered';
			}
			elseif ( $filter['filter_by'] == 'last_modified_date' ) {
				$date_field = 'e.date_last_modified';
			}

			if ( $filter['date_from'] && $filter['date_to'] ) {
				if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) ) {
					$filter["DATE($date_field) = DATE(FROM_UNIXTIME(?))"] = $filter['date_from'];
				}
			}

			if ( $filter['date_from'] ) {
				$filter["DATE({$date_field}) >= DATE(FROM_UNIXTIME(?))"] = $filter['date_from'];
			}

			if ( $filter['date_to'] ) {
				$filter["DATE({$date_field}) <= DATE(FROM_UNIXTIME(?))"] = $filter['date_to'];
			}
		}

		unset($filter['filter_by']);
		unset($filter['date_from']);
		unset($filter['date_to']);

		// Filter by Status
		if ( $filter['e.status'] != Model_Escorts::ESCORT_STATUS_DELETED ) {
			if ( $filter['excl_status'] ) {
				$filter = array_merge($filter, $this->_whereStatus2('e.status', null, $filter['excl_status']));
			}
		}

		unset($filter['excl_status']);

		if ( $filter['e.status'] ) {
			$filter = array_merge($filter, $this->_whereStatus2('e.status', $filter['e.status']));
		}

		unset($filter['e.status']);

		// Like Statements
		foreach ( $filter as $f => $v ) {
			if ( is_int($f) || strpos($f, '?') !== false ) continue;

			unset($filter[$f]);
			if ( ! $v ) continue;
			$filter[$f . ' LIKE ?'] = $v . '%';
		}

		$sql['where'] = $filter;
		/* <-- END Custom Filter Logic */

		$sql = array_merge_recursive($_sql, $sql);
		$dataSql = Cubix_Model::getSql($sql);


		$sql['group'] = false;
		$sql['fields'] = array('COUNT(DISTINCT(e.id))');
		$sql['page'] = false;
		$countSql = Cubix_Model::getSql($sql);

		return array(
			'result' => parent::_fetchAll($dataSql),
			'count' => parent::getAdapter()->fetchOne($countSql)
		);
	}

	public function _whereStatus2($field, $status, $excl_status = array())
	{
		if ( ! is_null($status) ) { if ( ! is_array($status) ) $status = array($status); }
		else { $status = array(); }
		if ( ! is_array($excl_status) ) $excl_status = array($excl_status);

		$ret = array();

		foreach ( $status as $st ) {
			$ret[] = "({$field} & {$st})";
		}

		foreach ( $excl_status as $st ) {
			$ret[] = " NOT ({$field} & {$st})";
		}

		return $ret;
	}

	public function _whereStatus($field, $status, $excl_status = array())
	{
		if ( ! is_null($status) ) if ( ! is_array($status) ) $status = array($status);
		if ( ! is_array($excl_status) ) $excl_status = array($excl_status);


		$ret = array();

		if ( is_array($status) ) {
			foreach ( $status as $st ) {
				$ret[] = "({$field} & {$st})";
			}

			$result = implode(' AND ', $ret);
		}

		if ( count($status) && count($excl_status) ) $result .= ' AND ';

		$ret = array();
		foreach ( $excl_status as $st ) {
			$ret[] = " NOT ({$field} & {$st})";
		}

		$result .= implode(' AND ', $ret);

		return $result;
	}
}
