<?php

class Model_Escort_Saver extends Cubix_Model
{
	/**
	 *
	 * @var Model_Escorts
	 */
	protected $_escorts;

	protected $_table = 'escorts';
	protected $_itemClass = 'Model_EscortItem';

	public function __construct()
	{
		$this->_escorts = new Model_Escorts();
	}

	private function _prepare($item)
	{
		$user_data = $item->getData(array('email', 'username', 'password', 'status', 'sales_user_id', 'application_id'));

		$escort_data = $item->getData(array('agency_id', 'showname', 'gender', 'country_id', 'base_city_id', 'pseudo_escort'));
		$escort_data['city_id'] = $escort_data['base_city_id'];
		$escort_data['date_registered'] = new Zend_Db_Expr('CURRENT_TIMESTAMP');
		unset($escort_data['base_city_id']);
		if ( ! $escort_data['agency_id'] ) $escort_data['agency_id'] = null;

		$profile_data = $item->getData(array('height', 'weight', 'bust_waist_hip', 'shoe_size', 'breast_size', 'dress_size', 'is_smoker',
			'hair_color', 'hair_length', 'eye_color', 'birth_date', 'characteristics', 'contact_phone', 'phone_instructions', 'email', 'web_address', 'contact_zip', 'measure_units',
			'availability', 'sex_availability', 'svc_kissing', 'svc_blowjob', 'svc_cumshot', 'svc_69', 'svc_anal', 'ethnicity', 'nationality_id', 'admin_verified',
			'vac_date_from', 'vac_date_to'));
		foreach ( $profile_data['sex_availability'] as $key => $v) {
			unset($profile_data['sex_availability'][$key]);
			$profile_data['sex_availability'][] = $key;
		}
		$profile_data['sex_availability'] = implode(',', $profile_data['sex_availability']);
		$profile_data['contact_email'] = $profile_data['email'];
		unset($profile_data['email']);
		$profile_data['contact_web'] = $profile_data['web_address'];
		unset($profile_data['web_address']);
		$profile_data['birth_date'] = $profile_data['birth_date'] ? new Zend_Db_Expr('FROM_UNIXTIME(' . $profile_data['birth_date'] . ')') : NULL;
		$bwh = explode('-', $profile_data['bust_waist_hip']);
		unset($profile_data['bust_waist_hip']);
		@list($bust, $waist, $hip) = $bwh;
		$profile_data['bust'] = intval($bust);
		$profile_data['waist'] = intval($waist);
		$profile_data['hip'] = intval($hip);
		$profile_data['vac_date_from'] = $profile_data['vac_date_from'] ? new Zend_Db_Expr('FROM_UNIXTIME(' . $profile_data['vac_date_from'] . ')') : NULL;
		$profile_data['vac_date_to'] = $profile_data['vac_date_to'] ? new Zend_Db_Expr('FROM_UNIXTIME(' . $profile_data['vac_date_to'] . ')') : NULL;
		$profile_data = array_merge($profile_data, Cubix_I18n::getValues($item->getData(), 'about', ''));
		$profile_data = array_merge($profile_data, Cubix_I18n::getValues($item->getData(), 'svc_additional', ''));

		$langs = $item->langs;
		$escort_langs = array();
		foreach ( $langs as $lang ) {
			list($lang_id, $level) = explode(':', $lang);

			$escort_langs[] = array(
				'escort_id' => null,
				'lang_id' => $lang_id,
				'level' => $level
			);
		}

		// The following is the same as working locations
		$locations = $item->locations;
		$cities = array();
		foreach ( $locations as $city_id ) {
			$cities[] = array(
				'escort_id' => null,
				'city_id' => $city_id
			);
		}

		// TODO: Some questions about cityzones, what to do with them???

		$rates = $item->rates;
		if ( ! $rates ) $rates = array();

		$escort_rates = array();
		foreach ( $rates as $rate ) {
			list($time, $time_unit, $price, $currency_id, $availability) = explode(':', $rate);

			$escort_rates[] = array(
				'escort_id' => null,
				'time' => $time,
				'time_unit' => $time_unit,
				'price' => $price,
				'currency_id' => $currency_id,
				'availability' => $availability
			);
		}

		$work_days = $item->work_days;
		$work_times_from = $item->work_times_from;
		$work_times_to = $item->work_times_to;
		if ( ! $work_days ) $work_days = array();
		$work_times = array();
		foreach ( $work_days as $day ) {
			$work_times[] = array(
				'escort_id' => null,
				'day_index' => $day,
				'time_from' => $work_times_from[$day],
				'time_to' => $work_times_to[$day]
			);
		}

		$tours = $item->tours;
		if ( ! $tours ) $tours = array();
		$tour_data = array();
		foreach ( $tours as $tour ) {
			list($country_id, $city_id, $date_from, $date_to, $phone) = explode(':', $tour);

			$tour_data[] = array(
				'escort_id' => null,
				'date_from' => new Zend_Db_Expr('FROM_UNIXTIME(' . $date_from . ')'),
			 	'date_to' => new Zend_Db_Expr('FROM_UNIXTIME(' . $date_to . ')'),
				'phone' => $phone,
				'country_id' => $country_id,
				'city_id' => $city_id
			);
		}

		/*$tour_data = array();
		if($item->tour_countries)
		{
			for ($i = 0; $i < count($item->tour_countries); $i++)
			{
				$tour_data[] = array(
					'escort_id' => null,
					'date_from' => new Zend_Db_Expr('FROM_UNIXTIME(' . $item->tour_date_from[$i] . ')'),
				 	'date_to' => new Zend_Db_Expr('FROM_UNIXTIME(' . $item->tour_date_to[$i] . ')'),
					'phone' => $item->tour_phones[$i],
					'country_id' => $item->tour_countries[$i],
					'city_id' => $item->tour_cities[$i]
				);
			}
		}*/

		return array(
			'user_data' => $user_data,
			'escort_data' => $escort_data,
			'profile_data' => $profile_data,
			'langs' => $escort_langs,
			'cities' => $cities,
			'rates' => $escort_rates,
			'work_times' => $work_times,
			'tour_data' => $tour_data
		);
	}

	public function save(Model_EscortItem $item)
	{
		$db = self::getAdapter();
		
		$db->beginTransaction();
		try {
			$data = $this->_prepare($item);

			$data['escort_data']['date_last_modified'] = new Zend_Db_Expr('NOW()');
			$data['user_data']['user_type'] = Model_Users::USER_TYPE_ESCORT;
			$escort_status = $data['user_data']['status'];

			/* edit mode */
			if ( $escort_id = $item->getId() ) {
				$this->updateEscort($escort_id, $data);
			}
			/* end edit mode */
			else {
				$escort_id = $this->insertEscort($data);
			}

			// escort langs
			foreach ( $data['langs'] as $lang ) {
				$lang = array_merge($lang, array('escort_id' => $escort_id));
				$db->insert('escort_langs', $lang);
			}

			// escort cities
			foreach ( $data['cities'] as $city ) {
				$city = array_merge($city, array('escort_id' => $escort_id));
				$db->insert('escort_cities', $city);
			}

			// escort rates
			foreach ( $data['rates'] as $rate ) {
				$rate = array_merge($rate, array('escort_id' => $escort_id));
				$db->insert('escort_rates', $rate);
			}

			// tour
			foreach ( $data['tour_data'] as $tour ) {
				$tour = array_merge($tour, array('escort_id' => $escort_id));
				$db->insert('tours', $tour);
			}

			// working times
			foreach ( $data['work_times'] as $time ) {
				$time = array_merge($time, array('escort_id' => $escort_id));
				$db->insert('escort_working_times', $time);
			}

			$db->commit();

			$m_profile = new Model_Escort_Profile(array('id' => $escort_id));
			$m_profile->addProfileRevision($item);

			$m_snapshot = new Model_Escort_Snapshot(array('id' => $escort_id));
			$m_snapshot->snapshotProfile();

//			Model_Activity::getInstance()->log( ($item->getId() ? 'edit' : 'create') . '_escort', array('id' => $escort_id, 'showname' => $data['escort_data']['showname']));
		}
		catch ( Exception $e ) {
			$db->rollBack();
			throw $e;
		}

		return $escort_id;
	}

	private function insertEscort($data)
	{
		$db = self::getAdapter();
		$users_model = new Model_Users();
		
		if ( strlen($data['escort_data']['agency_id']) > 0 ) {
			$ag = $db->fetchRow('
				SELECT user_id FROM agencies WHERE id = ?
			', $data['escort_data']['agency_id']);

			$data['escort_data']['user_id'] = $ag->user_id;
		}
		else {
			$salt_hash = Cubix_Salt::generateSalt($data['user_data']['password']);
			$pass = Cubix_Salt::hashPassword($data['user_data']['password'], $salt_hash);
			$data['user_data']['password'] = $pass;
			$data['user_data']['salt_hash'] = $salt_hash;

			$user_status = STATUS_ACTIVE;

			$data['user_data']['status'] = $user_status;

			$user_id = $users_model->save(new Model_UserItem($data['user_data']));

			$data['escort_data']['user_id'] = $user_id;
		}

		// escort
		parent::save(new Model_EscortItem($data['escort_data']));
		$escort_id = $db->lastInsertId();

		$this->_escorts->setStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS);

		// escort profile
		$data['profile_data']['escort_id'] = $escort_id;
		$db->insert('escort_profiles', $data['profile_data']);

		return $escort_id;
	}

	private function updateEscort($escort_id, $data)
	{
		$db = self::getAdapter();
		
		$users_model = new Model_Users();
		$escort_status = $data['user_data']['status'];

		if ( $escort_status == Model_Escorts::ESCORT_STATUS_ACTIVE ) {
			$user_status = STATUS_ACTIVE;
			$this->_escorts->setStatusBit($escort_id, $escort_status);
			$this->_escorts->removeStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_ADMIN_DISABLED);
		}
		elseif ( $escort_status == Model_Escorts::ESCORT_STATUS_ADMIN_DISABLED ) {
			$user_status = STATUS_ADMIN_DISABLED;
			$this->_escorts->removeStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_ACTIVE);
			$this->_escorts->setStatusBit($escort_id, $escort_status);
		}

		//////////// USER ITEM ////////////////////////
		if ( ! strlen($data['user_data']['password'])) {
			unset($data['user_data']['password']);
		}
		else {
			$salt_hash = Cubix_Salt::generateSalt($data['user_data']['password']);
			$pass = Cubix_Salt::hashPassword($data['user_data']['password'], $salt_hash);
			$data['user_data']['password'] = $pass;
			$data['user_data']['salt_hash'] = $salt_hash;
		}

		$data['user_data']['status'] = $user_status;
		/////////////////////////////////////////////


		$this->updateUserRow($escort_id, $data);

		// escort
		$data['escort_data']['id'] = $escort_id;
		$data['escort_data']['date_registered'] = $esc->date_registered;

		parent::save(new Model_EscortItem($data['escort_data']));

		// escort profile
		$data['profile_data']['escort_id'] = $escort_id;
		$db->update('escort_profiles', $data['profile_data'], $db->quoteInto('escort_id = ?', $escort_id));

		$db->delete('escort_langs', $db->quoteInto('escort_id = ?', $escort_id));
		$db->delete('escort_cities', $db->quoteInto('escort_id = ?', $escort_id));
		$db->delete('escort_rates', $db->quoteInto('escort_id = ?', $escort_id));
		$db->delete('escort_working_times', $db->quoteInto('escort_id = ?', $escort_id));
		$db->delete('tours', $db->quoteInto('escort_id = ?', $escort_id));
	}

	private function updateUserRow($escort_id, &$data)
	{
		$db = self::getAdapter();
		$users_model = new Model_Users();
		
		$esc = $db->fetchRow('
			SELECT user_id, agency_id, date_registered FROM escorts WHERE id = ?
		', $escort_id);

		if ( $esc ) {
			$agencies_model = new Model_Agencies();

			/**
			 * If escort becomes agency escort, at first we need
			 * to delete corresponding row from users table and set
			 * the user id of agency
			 */
			if ( ! $esc->agency_id && $data['escort_data']['agency_id'] ) {
				$db->query('DELETE FROM users WHERE id = ?', $esc->user_id);
				$agency = $agencies_model->getById($data['escort_data']['agency_id']);
				$data['escort_data']['user_id'] = $agency['agency_data']->user_id;
			}
			/**
			 * If escort becomes an independent escort we need to create
			 * a row in users table and set the user_id to new inserted
			 * id of user row
			 */
			elseif ( $esc->agency_id && ! $data['escort_data']['agency_id'] ) {
				$users_model->save($user_item = new Model_UserItem($data['user_data']));
				$data['escort_data']['user_id'] = $user_item->getId();
			}
			/**
			 * If escort doesn't changed it's behaviour and he/she is
			 * independent simply update corresponding row in users table
			 */
			elseif ( ! $esc->agency_id ) {
				$data['user_data']['id'] = $esc->user_id;
				$users_model->save($user_item = new Model_UserItem($data['user_data']));
			}
			else {
				$agency = $agencies_model->getById($data['escort_data']['agency_id']);
				$data['escort_data']['user_id'] = $agency['agency_data']->user_id;
			}
		}
	}
}
