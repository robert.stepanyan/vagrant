<?php

class Model_Escort_Profile extends Cubix_Model_Item
{
	protected $_idField = 'id';
	protected $_apiServer = "http://api.escortforum-v2.dev";
	//protected $_apiServer = "http://server.escortforumit.xxx";

	// Profile Revision System Block
	const REVISION_STATUS_NOT_APPROVED = 1;
	const REVISION_STATUS_APPROVED = 2;
	const REVISION_STATUS_DECLINED = 3;

	protected static $_revisions_statusMap = array(
		self::REVISION_STATUS_NOT_APPROVED => 'Not Approved',
		self::REVISION_STATUS_APPROVED => 'Approved',
		self::REVISION_STATUS_DECLINED => 'Declined',
	);

	public function __construct($rowData = null)
	{
		if ( ! is_null($rowData) ) {
			parent::__construct($rowData);
		}
		else {
			parent::__construct();
		}
	}

	public function getProfile()
	{
		$client = Cubix_Api::getInstance();

		Cubix_Api_XmlRpc_Client::setApiKey(Cubix_Application::getById(Zend_Controller_Front::getInstance()->getRequest()->application_id)->api_key);
		Cubix_Api_XmlRpc_Client::setServer($this->_apiServer);

		$data = $client->call('getEscortProfileData', array($this->getId()));

		/*foreach ( $data as $key => $value ) {
			$this->$key = $value;
		}*/

		return $data;
	}

	public function update($data)
	{
		$client = Cubix_Api::getInstance();

		Cubix_Api_XmlRpc_Client::setApiKey(Cubix_Application::getById(Zend_Controller_Front::getInstance()->getRequest()->application_id)->api_key);
		Cubix_Api_XmlRpc_Client::setServer($this->_apiServer);

		$client->call('updateEscortProfile', array($this->getId(), serialize($data)));
	}

	public function addProfileRevision(Model_EscortItem $item)
	{
		$item->setThrowExceptions(true);

		$data = $item->getData(array('showname', 'base_city_id', 'ethnicity', 'nationality_id',
			'measure_units', 'gender', 'birth_date', 'height', 'weight', 'bust_waist_hip',
			'hair_color', 'hair_length', 'eye_color', 'shoe_size', 'breast_size', 'dress_size', 'is_smoker', 'availability',
			'contact_zip', 'characteristics', 'sex_availability', 'svc_kissing',
			'svc_blowjob', 'svc_cumshot', 'svc_69', 'svc_anal', 'work_days', 'work_times_from',
			'work_times_to', 'contact_phone', 'phone_instructions', 'email', 'web_address'
		));

		list($data['bust'], $data['waist'], $data['hip']) = explode('-', $data['bust_waist_hip']);
		unset($data['bust_waist_hip']);
		$data['contact_email'] = $data['email'];
		unset($data['email']);
		$data['contact_web'] = $data['web_address'];
		unset($data['web_address']);
		$data['sex_availability'] = implode(',', array_keys($data['sex_availability']));

		$data['working_times'] = array();
		if ( isset($data['work_days']) && is_array($data['work_days']) ) {
			foreach ( $data['work_days'] as $day ) {
				$data['working_times'][$day] = array('from' => $data['work_times_from'][$day], 'to' => $data['work_times_to'][$day]);
			}
		}

		unset($data['work_days']);
		unset($data['work_times_from']);
		unset($data['work_times_to']);


		$data['about'] = array();
		foreach ( Cubix_I18n::getLangs(true) as $lang_id ) {
			$data['about'][$lang_id] = $item->{'about_' . $lang_id};
		}

		$data['svc_additional'] = array();
		foreach ( Cubix_I18n::getLangs(true) as $lang_id ) {
			$data['svc_additional'][$lang_id] = $item->{'svc_additional_' . $lang_id};
		}

		$data['base_city_id'] = $item->getBaseCityId();

		$data['cities'] = array();

		$data['cities'] = isset($item->locations) && is_array($item->locations) ? $item->locations : array();
		foreach ( $data['cities'] as $i => $city_id ) {
			$data['cities'][$i] = array('id' => $city_id);
		}

		$langs = $item->langs;
		$data['langs'] = array();
		foreach ( $langs as $lang ) {
			list($lng, $lvl) = explode(':', $lang);
			$data['langs'][] = array('lng' => $lng, 'lvl' => $lvl);
		}

		$revision = $this->_adapter->fetchOne('SELECT MAX(revision) FROM profile_updates WHERE escort_id = ?', $this->getId());
		$revision = intval($revision);
		$revision++;

		$this->_adapter->insert('profile_updates', array(
			'escort_id' => $this->getId(),
			'status' => self::REVISION_STATUS_APPROVED,
			'revision' => $revision,
			'date_updated' => new Zend_Db_Expr('NOW()'),
			'data' => serialize($data),
			'backend_user_id' => Zend_Auth::getInstance()->getIdentity()->id
		));

		return $revision;
	}

	public function getProfileRevisions()
	{
		return $this->getAdapter()->query('
			SELECT revision, UNIX_TIMESTAMP(date_updated) AS date_updated, status
			FROM profile_updates
			WHERE escort_id = ?
			ORDER BY revision DESC
		', $this->getId())->fetchAll();
	}

	public function getProfileRevision($revision)
	{
		$revision = $this->getAdapter()->fetchRow('
			SELECT revision, UNIX_TIMESTAMP(date_updated) AS date_updated, status, data
			FROM profile_updates
			WHERE escort_id = ? AND revision = ?
			ORDER BY revision DESC
		', array($this->getId(), $revision));

		if ( ! $revision ) return null;

		$revision->data = unserialize($revision->data);

		return $revision;
	}

	public function getProfileLatestRevision($status = null)
	{
		$bind = array($this->getId());
		$where = array();
		if ( ! is_null($status) ) {
			if ( ! is_array($status) ) {
				// $bind[] = $status;
				$where[] = 'status = ' . $status;
			}
			else {
				// $bind = array_merge($bind, $status);
				foreach ( $status as $s ) {
					$where[] = 'status = ' . $s;
				}
			}
		}

		return $this->getAdapter()->fetchOne('
			SELECT MAX(revision) FROM profile_updates
			WHERE escort_id = ?' . ( ! is_null($status) ? ' AND (' . implode(' OR ', $where) . ') ' : '') . '
		', $bind);
	}

	public static function getProfileStatusLabel($status)
	{
		return self::$_revisions_statusMap[$status];
	}

	public function getNotApprovedRevisions($app_id, $be_user)
	{
		$join = '';
		$where = '';
		if ( $be_user->type != 'superadmin' && $be_user->type != 'admin' && $be_user->type != 'dash manager' ) {
			$join .= 'INNER JOIN backend_users be ON be.id = u.sales_user_id';
			$where = " AND be.id = " . $be_user->id . " ";
		}

		$sql = "
			SELECT ag.name AS agency_name, ag.id AS agency_id, e.id, e.showname, pu.id AS revision_id, pu.date_updated FROM escorts e
			INNER JOIN profile_updates pu ON pu.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id
			" . $join . "
			LEFT JOIN agencies ag ON ag.id = e.agency_id
			WHERE " . Model_Escorts::_whereStatus('e.status', Model_Escorts::ESCORT_STATUS_PROFILE_CHANGED, array(Model_Escorts::ESCORT_STATUS_DELETED)) . " AND pu.status = ? AND u.application_id = ? {$where} ORDER BY pu.date_updated DESC
		";

		$sqlCount = "
			SELECT COUNT(DISTINCT(pu.id)) AS rev_count FROM profile_updates pu
			INNER JOIN escorts e ON pu.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id
			" . $join . "
			WHERE " . Model_Escorts::_whereStatus('e.status', Model_Escorts::ESCORT_STATUS_PROFILE_CHANGED, array(Model_Escorts::ESCORT_STATUS_DELETED)) . " AND pu.status = ? AND u.application_id = ? {$where}
		";

		$items = self::getAdapter()->query($sql, array(self::REVISION_STATUS_NOT_APPROVED, $app_id))->fetchAll();
		$count = self::getAdapter()->query($sqlCount, array(self::REVISION_STATUS_NOT_APPROVED, $app_id))->fetch();

		return array('count' => $count, 'data' => $items);
	}

	public function differProfile($old, $new)
	{
		$old = (array) $old; $new = (array) $new;

		$added = array(); $changed = array(); $removed = array();
		foreach ( $new as $field => $value ) {
			if ( ! $old[$field] && $value ) {
				$added[] = $field;
			}
			elseif ( ! $value && $old[$field] ) {
				$removed[] = $field;
			}
			elseif ( $old[$field] != $value ) {
				$changed[] = $field;
			}
		}

		return array('added' => $added, 'changed' => $changed, 'removed' => $removed);
	}

	public function updateProfileRevision($revision, $status)
	{
		$this->getAdapter()->update('profile_updates', array(
			'status' => $status
		), array(
			self::getAdapter()->quoteInto('escort_id = ?', $this->getId()),
			self::getAdapter()->quoteInto('revision = ?', $revision))
		);
	}

	public function applyProfileRevision($data)
	{
		$item = new Model_EscortItem($data);
		$item->id = $this->getId();
		$item->setThrowExceptions(false);
		$data_array = array('showname',  'gender', 'country_id', 'base_city_id');
		if ( Cubix_Application::getId() == APP_ED ) {
			$data_array[] = 'type'; 
		}
		$data = $item->getData($data_array);
		$data['date_last_modified'] = new Zend_Db_Expr('NOW()');
		$data['city_id'] = $data['base_city_id'];
		unset($data['base_city_id']);
		
		self::getAdapter()->update('escorts', $data, self::getAdapter()->quoteInto('id = ?', $this->getId()));
		
		$data = $item->getData(array(
			'height', 'weight', 'bust', 'waist', 'hip', 'shoe_size', 'breast_size', 'dress_size', 'is_smoker',
			'hair_color', 'hair_length', 'eye_color', 'birth_date', 'characteristics', 'contact_phone', 'phone_instructions',
			'contact_email', 'contact_web', 'contact_zip', 'measure_units', 'availability', 'sex_availability',
			'svc_kissing', 'svc_blowjob', 'svc_cumshot', 'svc_69', 'svc_anal', 'ethnicity', 
			'nationality_id'
		));
		
		$data['nationality_id'] = intval($data['nationality_id']);
		
		$abouts = $item->getData(array('about'));
		$abouts = $abouts['about'];
		foreach ( Cubix_Application::getLangs(Zend_Controller_Front::getInstance()->getRequest()->application_id, true) as $lang_id ) {
			$data['about_' . $lang_id] = isset($abouts[$lang_id]) ? $abouts[$lang_id] : '';
		}

		$svc_additional = $item->getData(array('svc_additional'));
		$svc_additional = $svc_additional['svc_additional'];
		foreach ( Cubix_Application::getLangs(Zend_Controller_Front::getInstance()->getRequest()->application_id, true) as $lang_id ) {
			$data['svc_additional_' . $lang_id] = isset($svc_additional[$lang_id]) ? $svc_additional[$lang_id] : '';
		}

		$data['birth_date'] = $data['birth_date'] ? new Zend_Db_Expr('FROM_UNIXTIME(' . $data['birth_date'] . ')') : NULL;

		if ( !self::getAdapter()->query('SELECT TRUE FROM escort_profiles WHERE escort_id = ?', $this->getId())->fetch() )
		{
			self::getAdapter()->insert('escort_profiles', array('escort_id' => $this->getId()));
		}
		self::getAdapter()->update('escort_profiles', $data, self::getAdapter()->quoteInto('escort_id = ?', $this->getId()));
		
		$langs = $item->getData(array('langs'));
		$langs = $langs['langs'];
		if ( ! is_array($langs) ) $langs = array();
		
		self::getAdapter()->delete('escort_langs', array(self::getAdapter()->quoteInto('escort_id = ?', $this->getId())));
		
		foreach ( $langs as $lang ) {
			self::getAdapter()->insert('escort_langs', array(
				'escort_id' => $this->getId(),
				'level' => $lang['lvl'],
				'lang_id' => $lang['lng']
			));
		}
		/* Working Times */
		$working_times = $item->getData(array('working_times'));
		$working_times = $working_times['working_times'];
		if ( ! is_array($working_times) ) $working_times = array();

		self::getAdapter()->delete('escort_working_times', array(self::getAdapter()->quoteInto('escort_id = ?', $this->getId())));

		foreach ( $working_times as $day_index => $wt ) {
			self::getAdapter()->insert('escort_working_times', array(
				'escort_id' => $this->getId(),
				'day_index' => $day_index,
				'time_from' => $wt['from'],
				'time_to' => $wt['to']
			));
		}
		/* END Working Times */
		$cities = $item->getData(array('cities'));
		$cities = $cities['cities'];
		if ( ! is_array($cities) ) $cities = array();
		self::getAdapter()->delete('escort_cities', self::getAdapter()->quoteInto('escort_id = ?', $this->getId()));
		foreach ( $cities as $city ) {
			self::getAdapter()->insert('escort_cities', array(
				'escort_id' => $this->getId(),
				'city_id' => $city['id']
			));
		}

		$m_snapshot = new Model_Escort_Snapshot(array('id' => $this->getId()));
		$m_snapshot->snapshotProfile();
	}
}
