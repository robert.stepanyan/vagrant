<?php
class Model_Escort_NewHistory extends Cubix_Model
{
	protected $_table = 'escorts_new_history';
	

	public function __construct(){
		$this->_db = Zend_Registry::get('db');
	}

	public function save($data){
		
		if($this->checkNewEscort($data['escort_id'])){
			if(in_array($data['package_id'], array(19))){
				$type = 3;
				$period = 1;
			} else {
				$type = 2;
				$period = 5;
			}
			$sql = "INSERT into escorts_new_history (escort_id, expires_at, type) VAlUES (".$data['escort_id'].", NOW() + INTERVAL ".$period." DAY, ".$type.")";
			return $this->_db->query($sql);
		}

		if(in_array($data['package_id'], array(19)) && $this->checkEscortGets1DayNew($data['escort_id']) < 5 && !$this->checkEscortLast60Days($data['escort_id'])){
			$sql = "INSERT into escorts_new_history (escort_id, expires_at, type) VAlUES (".$data['escort_id'].", NOW() + INTERVAL 1 DAY, 3)";
			return $this->_db->query($sql);
			// return $client->call('Escorts.setNewHistory', array($data['escort_id'], 1, 3));

		} elseif($this->checkEscortLast60Days($data['escort_id']) && $this->checkEscortGets1DayNew($data['escort_id']) < 3){
			$sql = "INSERT into escorts_new_history (escort_id, expires_at, type) VALUES (".$data['escort_id'].", NOW() + INTERVAL 5 DAY, 2)";
			return $this->_db->query($sql);
			// return $client->call('Escorts.setNewHistory', array($data['escort_id'], 5, 2));

		}

	}

	public function checkEscortLast60Days($escort_id){
		$sql = "SELECT expires_at as expires_at from escorts_new_history where escort_id = '".$escort_id."' AND type='2' order by id DESC LIMIT 1 ";
		$last_p = $this->_db->query($sql)->fetch();
		
		$date = date ( 'Y-m-d' );
		$date = strtotime ( '-60 days' , strtotime($date));
		$date = date ( 'Y-m-d h:m:s' , $date );
		return $date > $last_p->expires_at;
	}

	public function checkEscortGets1DayNew($escort_id){
		$sql = "SELECT count(*) as `count` from escorts_new_history where escort_id = '".$escort_id."' AND expires_at > NOW() - INTERVAL 60 DAY";
		$count = $this->_db->query($sql)->fetch();

		return $count->count;
	}


	public function checkNewEscort($escort_id){
		$sql = "SELECT true as new_escort from escorts_new_history where escort_id='".$escort_id."'";
		$new = $this->_db->query($sql)->fetch();
		if(isset($new) && $new->new_escort){
			return false;
		} else {
			return true;
		}
	}
}
