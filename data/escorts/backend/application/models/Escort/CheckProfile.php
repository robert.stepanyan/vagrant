<?php

class Model_Escort_CheckProfile extends Cubix_Model
{ 
	const STATUS_CHECKED_OK = 1;
	const STATUS_CHECKED_POSTPONED = 2;
	const STATUS_CHECKED_DISABLED = 3;
	
	public static $status_array = array(
			self::STATUS_CHECKED_OK => 'Checked-ok',
			self::STATUS_CHECKED_POSTPONED => 'Postponed',
			self::STATUS_CHECKED_DISABLED => 'Checked-disabled'
	);
			
	public static function getCommentsHistoryByEscortId($id, $page = 1, $per_page = 5, &$count = 0)
	{
		$db = Zend_Registry::get('db');
		
		$sql = 'SELECT SQL_CALC_FOUND_ROWS
			ec.id, ec.comment ,ec.admin_verified, ec.date , bu.username, ec.check_status, ec.next_check_date, ec.is_check_profile FROM escort_comments as ec
			LEFT JOIN backend_users bu ON bu.id = ec.sales_user_id
			WHERE ec.escort_id = ? ORDER BY ec.date DESC LIMIT ' . (($page - 1) * $per_page) . ', '. $per_page ;
		$results = $db->fetchAll($sql, $id);

		$count = $db->fetchOne('SELECT FOUND_ROWS()');
		return $results;
	}

	public static function getResponseCommentsHistoryByEscortId($id, $page = 1, $per_page = 5, &$count = 0)
	{
		$db = Zend_Registry::get('db');
		
		$sql = 'SELECT SQL_CALC_FOUND_ROWS
			ec.id, ec.date, ec.comment ,ec.response , ec.date , bu.username FROM escort_responses as ec
			LEFT JOIN backend_users bu ON bu.id = ec.sales_user_id
			WHERE ec.escort_id = ? ORDER BY ec.date DESC LIMIT ' . (($page - 1) * $per_page) . ', '. $per_page ;
		$results = $db->fetchAll($sql, $id);

		$count = $db->fetchOne('SELECT FOUND_ROWS()');
		return $results;
	}
	
}