<?php

class Model_Escort_SaverV2 extends Cubix_Model
{
	/**
	 *
	 * @var Model_Escorts
	 */
	protected $_escorts;

	protected $_table = 'escorts';
	protected $_itemClass = 'Model_EscortV2Item';

	public function __construct()
	{
		$this->_escorts = new Model_EscortsV2();
	}

	public function save(Model_EscortV2Item $item)
	{
		$db = self::getAdapter();
		$users_model = new Model_Users();

		$db->beginTransaction();
		try {
			
			$data = $this->_prepare($item);

			/* edit mode */
			if ( $escort_id = $data['escort_data']['id'] ) {

				$this->updateEscort($escort_id, $data);
				
				Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array(
				    'escort_data'=>$data['escort_data'],
				    'user_row'=>'',
                ));
			}
			/* end edit mode */
			else {
				$escort_id = $this->insertEscort($data);

				// Set master ID
				if( in_array(Cubix_Application::getId(), array(APP_ED)) ){
					$db->update('escorts', array('master_id' => $escort_id), $db->quoteInto('id = ?', $escort_id));
				}

				Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_CREATED, $data);
			}

			// tours
			
			foreach ( $data['tour_data'] as $tour ) {
				$tour = array_merge($tour, array('escort_id' => $escort_id));
				$db->insert('tours', $tour);
				$city_id = $db->lastInsertId();
				Cubix_LatestActions::addActionDetail($escort_id, Cubix_LatestActions::ACTION_TOUR_UPDATE, $city_id);
				/*if(Cubix_Application::getId() == APP_ED ){
					Cubix_FollowEvents::notify($escort_id, 'escort', FOLLOW_ESCORT_NEW_CITY , $city_id, null, true);
				}*/
			}
            
			if ( count($data['tour_data']) ) {
				Cubix_LatestActions::addAction($escort_id, Cubix_LatestActions::ACTION_TOUR_UPDATE);
			}
			
			$db->commit();
			// slogan //
            $old_slogan = $db->fetchOne('SELECT `text` FROM escort_slogans WHERE escort_id = ?', $escort_id);
			if (strlen($data['escort_data']['slogan']) == 0)
			{
				if ($old_slogan){
                    $db->delete('escort_slogans', $db->quoteInto('escort_id = ?', $escort_id));

                    Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array(
                        'slogan' => '*** removed ***'
                    ));
                }
			}
			else
			{
				if ($old_slogan && $old_slogan != $data['escort_data']['slogan'])
				{
					$db->update('escort_slogans', array(
						'text' => $data['escort_data']['slogan']
					), $db->quoteInto('escort_id = ?', $escort_id));
					
					$log = '*** updated *** - ' . $data['escort_data']['slogan'];
				}
				elseif(!$old_slogan)
				{
					$db->insert('escort_slogans', array(
						'escort_id' => $escort_id,
						'text' => $data['escort_data']['slogan'],
						'status' => 1
					));
					
					$log = '*** inserted *** - ' . $data['escort_data']['slogan'];
				}
				if (!empty($log)){
                    Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array(
                        'slogan' => $log
                    ));
                }
			}
			//

			Model_Activity::getInstance()->log( ($data['escort_data']['id'] ? 'edit' : 'create') . '_escort', array('id' => $escort_id, 'showname' => $data['escort_data']['showname']));
		}
		catch ( Exception $e ) {
			$db->rollBack();
			throw $e;
		}

		return $escort_id;
	}

	private function _prepare($item)
	{
		$user_data = $this->getUserData($item);

		$data = $this->getEscortData($item);
		$escort_data = $data['escort_data'];

		if ( $data['user_id'] ) {
			$user_data['id'] = $data['user_id'];

		}

        if ( in_array(Cubix_Application::getId(), array(APP_6B, APP_AE, APP_BL, APP_EG_CO_UK, APP_ED)) ) {
            $bu_user = Zend_Auth::getInstance()->getIdentity();
            $user_data['backend_user'] = $bu_user->id;
        }

		$escort_data = array_merge($escort_data, $this->getVacation($item));

		$tour_data = $this->getTours($item);

		return array(
			'user_data' => $user_data,
			'escort_data' => $escort_data,
			'tour_data' => $tour_data
		);
	}

	private function getUserData($item)
	{
		$user_data = array(
			'email' => $item->login['reg_email'],
			'username' => $item->login['username'],
			'status' => USER_STATUS_ACTIVE,
			'sales_user_id' => $item->login['sales_user_id'],
			'application_id' => $item->login['application_id'],
			'im_is_blocked' => $item->login['im_is_blocked'],
			'user_type' => Model_Users::USER_TYPE_ESCORT,
//            'backend_user' => Zend_Auth::getInstance()->getIdentity()->id
		);

		/*if ( isset($item->login['reg_email']) && $item->login['reg_email'] ) {
			$user_data['reg_email'] = $item->login['reg_email'];
		}*/
		
		if ( strlen($item->login['password']) ) {
			$user_data['password'] = $item->login['password'];

			$salt_hash = Cubix_Salt::generateSalt($user_data['password']);
			$pass = Cubix_Salt::hashPassword($user_data['password'], $salt_hash);
			$user_data['password'] = $pass;
			$user_data['salt_hash'] = $salt_hash;
		}

		return $user_data;
	}

	private function getEscortData($item)
	{
		$b_user = Zend_Auth::getInstance()->getIdentity();
		$escort_data = array(
			'showname' => $item->showname,
			'gender' => $item->gender,
			'country_id' => $item->country_id,
			'city_id' => $item->city_id,
			
			'is_suspicious' => $item->login['is_suspicious'],
			'susp_comment_show' => $item->login['susp_comment_show'],
			
			'suspicious_comment' => $item->login['suspicious_comment'],
			'slogan' => $item->login['slogan'],
            'block_website' => $item->block_website,
			'check_website' => $item->check_website,
			'is_pornstar' => $item->is_pornstar,
			'bad_photo_history' => $item->bad_photo_history,
			'stays_verified' => $item->stays_verified,
			'auto_approval' => $item->login['auto_approval'],
			'photo_auto_approval' => $item->login['photo_auto_approval'],
			//'fake_picture_disabled' => $item->login['fake_picture_disabled'],
			'date_last_modified_backend' => new Zend_Db_Expr('NOW()'),
            'photo_suspicious_date' => new Zend_Db_Expr('NOW()'),
			'hh_rates'			=> is_array($item->hh_rates) ? $item->hh_rates : array(),
			'show_popunder' => $item->show_popunder,
		);
		
		if ( in_array(Cubix_Application::getId(), array(APP_6B, APP_6C, APP_EG, APP_EG_CO_UK, APP_ED)) ) {
			if ( isset($item->activation_date )) {
				$escort_data['activation_date'] = $item->activation_date ? new Zend_Db_Expr('FROM_UNIXTIME(' . $item->activation_date . ')') : NULL;
			}
		}
		if ( in_array(Cubix_Application::getId(), array(APP_BL)) ) {
			$escort_data['is_vaccinated'] = $item->is_vaccinated;
		}
		if ( in_array(Cubix_Application::getId(), array(APP_ED)) ) {
			$escort_data['sync_escort'] = $item->sync_escort;
			$escort_data['backend_escort'] = $item->backend_escort;
			$escort_data['type'] = $item->login['type'];
			$escort_data['mark_not_new'] = $item->mark_not_new;
			if(isset($item->last_hand_verification_date)){
				$escort_data['last_hand_verification_date'] = $item->last_hand_verification_date;
				$escort_data['hand_verification_sales_id'] = $item->hand_verification_sales_id;
			}
		}

		if ( in_array(Cubix_Application::getId(), array(APP_ED, APP_EG_CO_UK)) ) {
			$escort_data['disable_when_tour_ends'] = $item->disable_when_tour_ends;
		}

		if ( in_array(Cubix_Application::getId(), array(APP_EF)) ) {
			$escort_data['no_renew_sms'] = $item->no_renew_sms;
		}
		
		if ( isset($item->fake_city_id )) {
			$escort_data['fake_city_id'] = $item->fake_city_id;
		}
		if ( isset($item->fake_zip )) {
			$escort_data['fake_zip'] = $item->fake_zip;
		}

		$escort_data['hh_date_from'] = $item->hh_date_from;
		$escort_data['hh_date_to'] = $item->hh_date_to;
		
		if ( isset($item->login['is_suspicious']) && $item->login['is_suspicious'] ) {
			$escort_data['verified_status'] = Model_Verifications_Requests::PENDING;
		}
		if ( isset($item->hh_motto) && $item->hh_motto ) {
			$escort_data['hh_motto'] = $item->hh_motto;
		}
		if ( isset($item->hh_save)/* && $item->hh_save*/ ) {
			$escort_data['hh_save'] = (int) $item->hh_save;
		}
		if ( isset($item->has_hh) && $item->has_hh ) {
			$escort_data['has_hh'] = (int) $item->has_hh;
		}
		

		$user_id = null;

		//EDIT MODE
		if ( $item->login['id'] ) {
			$escort_data['id'] = $item->login['id'];

			$escort = self::getAdapter()->fetchRow('SELECT user_id FROM escorts WHERE id = ?', $item->login['id']);
			$escort_data['user_id'] = $escort->user_id;

			$user_id = $escort->user_id;
			
			if( in_array(Cubix_Application::getId(), array(APP_ED, APP_EG_CO_UK)) ){
				$escort_data['date_last_modified'] = new Zend_Db_Expr('NOW()');
			}
		}

		if ( $item->login['agency_id'] ) {
			$escort_data['agency_id'] = $item->login['agency_id'];

			$agency = self::getAdapter()->fetchRow('SELECT user_id FROM agencies WHERE id = ?', $item->login['agency_id']);
			$escort_data['user_id'] = $agency->user_id;
			$user_id = $agency->user_id;

			if ( in_array(Cubix_Application::getId(), array(APP_ED,APP_EG_CO_UK,APP_EG,APP_AE)) ) {

				if(strlen($item->source_url) < 1){
					$item->source_url = null;
				}
				$escort_data['source_url'] = $item->source_url;
			}
			if ( in_array(Cubix_Application::getId(), array(APP_ED)) ) {
				$escort_data['source_url_doesnt_have']  = $item->source_url_doesnt_have;
			}
		}
		else {
			$escort_data['agency_id'] = null;
		}

        if (in_array(Cubix_Application::getId(), array(APP_ED, APP_EG_CO_UK, APP_EG))) {
            if (strlen($item->source_url) < 1) {
                $item->source_url = null;
            }
            $escort_data['source_url'] = $item->source_url;
        }

        if ( $item->login['pseudo_escort'] ) {
			$escort_data['pseudo_escort'] = $item->login['pseudo_escort'];
		}

		if ( isset($item->status) ) {
			$escort_data['status'] = $item->status;
		}
		
		if ( isset($item->about_me_informed) ) {
			$escort_data['about_me_informed'] = $item->about_me_informed;
		}
		
		if($item->check_website && !Model_EscortsV2::getWebsiteChecked($item['login']['id'])){
			$escort_data['check_website_date'] = new Zend_Db_Expr('NOW()');
			$escort_data['check_website_person'] = $b_user->username;
		}

		if($item->block_website && !Model_EscortsV2::getWebsiteblocked($item['login']['id'])){
			$escort_data['block_website_date'] = new Zend_Db_Expr('NOW()');
			$escort_data['block_website_person'] = $b_user->username;
		}

		$escort_data['date_registered'] = new Zend_Db_Expr('CURRENT_TIMESTAMP');
		if ( isset($item->login['is_suspicious']) && $item->login['is_suspicious'] == 1 ) {
			$escort_data['photo_suspicious_date'] = new Zend_Db_Expr('NOW()');
		}
		elseif ( isset($item->login['is_suspicious']) && $item->login['is_suspicious'] == 2 ) {
			$escort_data['under_investigation_date'] = new Zend_Db_Expr('NOW()');
		}
		else{
			$escort_data['photo_suspicious_date'] = NULL;
			$escort_data['under_investigation_date'] = NULL;
		}
		
		
//        $escort_data['photo_suspicious_date'] = new Zend_Db_Expr('NOW()');

		return array('escort_data' => $escort_data, 'user_id' => $user_id);
	}

	private function getVacation($item)
	{
		$escort_data = array();
		$escort_data['vac_date_from'] = null;
		$escort_data['vac_date_to'] = null;

		if ( $item->vacation['vac_date_from'] && $item->vacation['vac_date_from'] ) {
			$escort_data['vac_date_from'] = new Zend_Db_Expr('FROM_UNIXTIME(' . $item->vacation['vac_date_from'] . ')');
			$escort_data['vac_date_to'] = new Zend_Db_Expr('FROM_UNIXTIME(' . $item->vacation['vac_date_to'] . ')');
		}

		return $escort_data;
	}

	private function getTours($item)
	{
		$tours = $item->tours;

		$m = new Model_EscortsV2();
		$tour_history = $m->getToursHistory($item->login['id']);

		$tour_data = array();
		if ( $tours ) {		
			foreach ( $tours as $i => $tour ) {
				if( in_array(Cubix_Application::getId(), array(APP_EF, APP_6A, APP_BL, APP_AE, APP_6B, APP_6C, APP_EG, APP_EG_CO_UK))){
					list($country_id, $city_id, $date_from, $date_to, $phone, $minutes_from, $minutes_to) = $tour;
					$tour_data[$i] = array(
						'escort_id' => null,
						'date_from' => new Zend_Db_Expr('FROM_UNIXTIME(' . $date_from . ')'),
						'date_to' => new Zend_Db_Expr('FROM_UNIXTIME(' . $date_to . ')'),
						'minutes_from' => $minutes_from != '' ? $minutes_from : null,
						'minutes_to' => $minutes_to != '' ? $minutes_to : null,
						'phone' => $phone,
						'country_id' => $country_id,
						'city_id' => $city_id
					);
				}
				else{
					list($country_id, $city_id, $date_from, $date_to, $phone) = $tour;

					$tour_data[$i] = array(
						'escort_id' => null,
						'date_from' => new Zend_Db_Expr('FROM_UNIXTIME(' . $date_from . ')'),
						'date_to' => new Zend_Db_Expr('FROM_UNIXTIME(' . $date_to . ')'),
						'phone' => $phone,
						'country_id' => $country_id,
						'city_id' => $city_id
					);
				}
					
			}
		}

		/*$tour_history_data = array();
		if ( $tour_history ) {
			foreach ( $tour_history as $i => $tour ) {
				$tour_history_data[$i] = array(
					'escort_id' => null,
					'date_from' => new Zend_Db_Expr('FROM_UNIXTIME(' . $tour->date_from . ')'),
					'date_to' => new Zend_Db_Expr('FROM_UNIXTIME(' . $tour->date_to . ')'),
					'phone' => $tour->phone,
					'country_id' => $tour->country_id,
					'city_id' => $tour->city_id
				);
			}
		}

		if( count($tour_history_data) ) {
			$tour_data = array_merge($tour_data, $tour_history_data);
		}*/
		
		return $tour_data;
	}

	private function insertEscort($data)
	{
		$db = self::getAdapter();

		unset($data['escort_data']['hh_rates']);
		
		if ( ! $data['escort_data']['agency_id'] ) {
			$data['escort_data']['user_id'] = $this->insertUser($data);
		}
		// Inserting escort	data
		parent::save(new Model_EscortV2Item($data['escort_data']));

		$escort_id = $db->lastInsertId();

		return $escort_id;
	}

	private function insertUser($data)
	{
		$users_model = new Model_Users();
		
		/*$salt_hash = Cubix_Salt::generateSalt($data['user_data']['password']);
		$pass = Cubix_Salt::hashPassword($data['user_data']['password'], $salt_hash);
		$data['user_data']['password'] = $pass;
		$data['user_data']['salt_hash'] = $salt_hash;*/

		// Inserting user data
		return $users_model->save(new Model_UserItem($data['user_data']));
	}

	private function updateEscort($escort_id, $data)
	{
		$db = self::getAdapter();
		
		unset($data['escort_data']['sync_escort']);
		unset($data['escort_data']['backend_escort']);		

		$this->updateUserRow($escort_id, $data);

		// escort
		$data['escort_data']['id'] = $escort_id;
		//$data['escort_data']['date_registered'] = $esc->date_registered;

		if ( ! isset($data['escort_data']['pseudo_escort']) && ! $data['escort_data']['pseudo_escort'] ) {
			$data['escort_data']['pseudo_escort'] = 0;
		}

		unset($data['escort_data']['date_registered']);
		$hh_rates = $data['escort_data']['hh_rates']; unset($data['escort_data']['hh_rates']);
		
		parent::save(new Model_EscortV2Item($data['escort_data']));
		Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED,array(
		    'escort_data'=>$data['escort_data'],
            'from'=>'SaverV2::updateEscort'
        ));
		if(isset($data['escort_data']['fake_city_id'])) {
			unset($data['escort_data']['fake_city_id']);
		}
		if(isset($data['escort_data']['fake_zip'])) {
			unset($data['escort_data']['fake_zip']);
		}
		
		// escort profile
		$esc_profile = $db->fetchOne('SELECT TRUE FROM escort_profiles_v2 WHERE escort_id = ?', array($escort_id));
		if ( ! $esc_profile ) {
			$db->insert('escort_profiles_v2', array('escort_id' => $escort_id));
            Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED,array(
                'escort_data'=>array('escort_id' => $escort_id),
                'action'=>'empty profile created',
                'from'=>'SaverV2::updateEscort'
            ));
		}

		if ( isset($data['escort_data']['is_suspicious']) && $data['escort_data']['is_suspicious'] ) {
			$this->setPhotosToNormal($escort_id);
            Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED,array(
                'escort_id'=>$escort_id,
                'action'=>'set photos to normal',
                'from'=>'SaverV2::updateEscort'
            ));
		}
		
		$db->delete('tours', $db->quoteInto('escort_id = ? AND DATE(date_to) >= CURDATE()', $escort_id));
		
		//saving happy hour rates
		foreach($hh_rates as $rate_id => $price) {
			$db->update(
				'escort_rates',
				array( 'hh_price' => $price ),
				$db->quoteInto('id = ?', $rate_id)
			);
		}
	}


	private function setPhotosToNormal($escort_id)
	{
		$db = self::getAdapter();
		
		if ( ! isset($escort_id) && ! $escort_id ) return;
		
		$db->update('escort_photos', array('status' => Model_Escort_Photos::STATUS_NORMAL), $db->quoteInto('escort_id = ?', $escort_id));
	}


	/**
	 * !!! NO need of this func anymore !!!
	 *
	 * @param int $escort_id
	 * @param string $data
	 * @deprecated
	 */
	private function setStatus($escort_id, &$data)
	{
		if ( isset($data['escort_data']['fake_picture_disabled']) && $data['escort_data']['fake_picture_disabled'] ) {
			$user_status = STATUS_ADMIN_DISABLED;
//			$this->_escorts->removeStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_ACTIVE);
//			$this->_escorts->setStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_ADMIN_DISABLED);
//			$this->_escorts->setStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_FAKE_PICTURE_DISABLED);
		}
		else {
			if ( $data['escort_data']['status'] ) {
				$escort_status = $data['escort_data']['status'];

				if ( $escort_status == Model_EscortsV2::ESCORT_STATUS_ACTIVE ) {
					$user_status = STATUS_ACTIVE;
//					$this->_escorts->setStatusBit($escort_id, $escort_status);
//					$this->_escorts->removeStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_ADMIN_DISABLED);
//					$this->_escorts->removeStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_OWNER_DISABLED);
				}
				elseif ( $escort_status == Model_EscortsV2::ESCORT_STATUS_ADMIN_DISABLED ) {
					$user_status = STATUS_ADMIN_DISABLED;
//					$this->_escorts->removeStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_ACTIVE);
//					$this->_escorts->setBit($escort_id, $escort_status);
				}
				elseif ( $escort_status == Model_EscortsV2::ESCORT_STATUS_OWNER_DISABLED ) {
					$user_status = STATUS_ACTIVE;
//					$this->_escorts->removeStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_ACTIVE);
//					$this->_escorts->removeStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_ADMIN_DISABLED);
//					$this->_escorts->setStatusBit($escort_id, $escort_status);
				}

//				$this->_escorts->removeStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_IS_NEW);
//				$this->_escorts->removeStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_NO_PROFILE);
//				$this->_escorts->removeStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_NOT_APPROVED);

				$data['user_data']['status'] = $user_status;
			}
			
//			$this->_escorts->removeStatusBit($escort_id, Model_EscortsV2::ESCORT_STATUS_FAKE_PICTURE_DISABLED);
		}
	}

	private function updateUserRow($escort_id, &$data)
	{
		$db = self::getAdapter();
		$users_model = new Model_Users();
		
		$esc = $db->fetchRow('
			SELECT user_id, agency_id, date_registered, is_suspicious FROM escorts WHERE id = ?
		', $escort_id);

		$user = Zend_Auth::getInstance()->getIdentity();

		$old_sales = $db->fetchRow('
			SELECT u.sales_user_id, bu.username, bu.email
			FROM users u
			INNER JOIN backend_users bu ON bu.id = u.sales_user_id
			WHERE u.id = ?
		', $data['user_data']['id']);

		$new_sales = $data['user_data']['sales_user_id'];
		$new_sales_username = $db->fetchRow('SELECT username FROM backend_users WHERE id = ?', $new_sales);
		if (Cubix_Application::getId() == APP_ED && $user->id == 204){
		    $sophie = true;
        }
		if ( !$sophie && $user->type != 'superadmin' && $user->type != 'dash manager' && $old_sales !== false && $old_sales->sales_user_id != $new_sales && ! strlen($data['escort_data']['agency_id'])) {
			unset($data['user_data']['sales_user_id']);
			$new_sales_exists = Model_Users::checkNewSalesPerson($data['user_data']['id'], $new_sales);
			if(!$new_sales_exists){
				$new_sales_hash = md5(microtime() * microtime());
				$db->update('users', array('new_sales_user_id' => $new_sales, 'new_sales_hash' => $new_sales_hash), $db->quoteInto('id = ?', $data['user_data']['id']));

				if ( $new_sales_username->username ) {
					$email_data = array(
						'new_sales_hash' => $new_sales_hash,
						'salesname' => $new_sales_username->username,
						'showname'  => $data['escort_data']['showname'],
						'username'  => $old_sales->username,
						'escort_id' => $data['escort_data']['id'] 
					);
					Cubix_Email::sendTemplate('escort_new_sales_verification', $old_sales->email, $email_data);
				}
			}
		}

		if ( $esc ) {
			$agencies_model = new Model_Agencies();
			
			

            if( ($esc->is_suspicious && $data['escort_data']['is_suspicious']) ) {
                unset($data['escort_data']['photo_suspicious_date']);
            } elseif ( ! $data['escort_data']['is_suspicious'] && $esc->is_suspicious) {
                $data['escort_data']['photo_suspicious_date'] = null;

				$this->saveSuspiciousJournal(array('escort_id' => $escort_id, 'event_type' => self::ESCORT_REMOVED_SUSPICIOUS));
				Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_SUSPICIOUS_UPDATED, array('escort_id' => $escort_id, 'action' => 'removed'));

            }elseif($data['escort_data']['is_suspicious']) {
            	
                if( $data['escort_data']['agency_id'] ) {
                    $agency = $agencies_model->getById($data['escort_data']['agency_id']);

                    $agency_user = $users_model->get($agency['agency_data']['user_id']);
                    
                    $email_data = array(
                        'agency' => $agency['agency_data']['name'],
                        'username' => $data['escort_data']['showname'] ? $data['escort_data']['showname'] : $data['escort_data']['username'],
                        'escort_showname' => $data['escort_data']['showname'],
                        'url' => 'https://www.'.Cubix_Application::getById()->host.'/account/signin?referrer=private/verify/index/'
                    );

                    $email = $agency_user['email'];
					if(Cubix_Application::getId() == APP_ED){
						Cubix_Email::sendTemplate('suspicious_photo_agency_v1', $email, $email_data);
					}elseif( in_array(Cubix_Application::getId(), array(APP_EG_CO_UK, APP_6B))){
						Cubix_Email::sendTemplate('agency_photos_marked_as_fake', $email, $email_data);
					}
                } else {
                    $email_data = array(
                         'url' => 'https://www.'.Cubix_Application::getById()->host.'/account/signin?referrer=private/verify/idcard/'
                    );

                    $email =  $data['user_data']['email'];
					
					if( Cubix_Application::getId() == APP_ED ){
						 Cubix_Email::sendTemplate('suspicious_photo_v1', $email, $email_data);
					}elseif( in_array(Cubix_Application::getId(), array(APP_EG_CO_UK, APP_6B))){
						Cubix_Email::sendTemplate('escort_photos_marked_as_fake', $email, $email_data);
					}
                }

				$this->saveSuspiciousJournal(array('escort_id' => $escort_id, 'event_type' => self::ESCORT_MARKED_SUSPICIOUS));
				Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_SUSPICIOUS_UPDATED, array('escort_id' => $escort_id, 'action' => 'marked'));
            }

			/**
			 * If escort becomes agency escort, at first we need
			 * to delete corresponding row from users table and set
			 * the user id of agency
			 */
           
			$reg_email = '';
			if ( isset($data['user_data']['email']) && $data['user_data']['email'] ) {
				$reg_email = $data['user_data']['email'];
				unset($data['user_data']['email']);
			}

			if ( ! $esc->agency_id && $data['escort_data']['agency_id'] ) {
				
				$db->query('SET foreign_key_checks = 0');
				
				$db->query('DELETE FROM users WHERE id = ? AND user_type = \'escort\'', $esc->user_id);
				$agency = $agencies_model->getById($data['escort_data']['agency_id']);
				$data['escort_data']['user_id'] = $agency['agency_data']->user_id;
				
				// Move all escorts orders and transfer to new agency Id 
				//$db->update('orders', array('user_id' => $data['escort_data']['user_id']), $db->quoteInto('user_id = ?', $esc->user_id));
				//$db->update('transfers', array('user_id' => $data['escort_data']['user_id']), $db->quoteInto('user_id = ?', $esc->user_id));
				
				$db->query('
					UPDATE orders o 
					INNER JOIN order_packages op ON o.id = op.order_id
					SET user_id = ?
					WHERE op.escort_id = ?
				', array($data['escort_data']['user_id'], $escort_id));
				
				$db->query('
					UPDATE transfers t 
					INNER JOIN transfer_orders tro ON tro.transfer_id = t.id
					INNER JOIN order_packages op ON op.order_id = tro.order_id
					set user_id = ?
					WHERE escort_id = ?
				', array($data['escort_data']['user_id'], $escort_id));
				
				$db->query('SET foreign_key_checks = 1');
				Cubix_SyncNotifier::notify($escort_id,Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array(
				    'escort_id'=>$escort_id,
                    'new_user_id'=>$data['escort_data']['user_id'],
                    'old_user_id'=>$esc->user_id,
                    'action'=>'escort assigned to agency : '.$data['escort_data']['agency_id'],
                    'coment'=>'updated orders and transfers',
                    'from'=>'SaverV2::updateUserRow'
                ));
			}
			/**
			 * If escort becomes an independent escort we need to create
			 * a row in users table and set the user_id to new inserted
			 * id of user row
			 */
			elseif ( $esc->agency_id && ! $data['escort_data']['agency_id'] ) {
				$old_user_id = $data['user_data']['id'];
				unset($data['user_data']['id']);
				
				$db->query('SET foreign_key_checks = 0');
				
				$user_id = $users_model->save(new Model_UserItem($data['user_data']));

				if($data['user_data']['sales_user_id'] && $old_sales->sales_user_id != $new_sales && Cubix_Application::getId() == APP_ED) {
					self::getAdapter()->insert('escort_comments', array(
						'escort_id' => $escort_id,
						'comment' => 'change of sale from ' . $old_sales->username . ' to ' . $new_sales_username->username,
						'sales_user_id' => $user->id, 
						'date' => new Zend_Db_Expr('NOW()')
					));
				}

				$data['escort_data']['user_id'] = $user_id;
				$data['escort_data']['agency_id'] = null;				
				
				// Move all escorts orders and transfer to new user Id 
				//$db->update('orders', array('user_id' => $user_id), $db->quoteInto('user_id = ?', $old_user_id));
				//$db->update('transfers', array('user_id' => $user_id), $db->quoteInto('user_id = ?', $old_user_id));
				
				$db->query('
					UPDATE orders o 
					INNER JOIN order_packages op ON o.id = op.order_id
					SET user_id = ?
					WHERE op.escort_id = ?
				', array($user_id, $escort_id));
				
				$db->query('
					UPDATE transfers t 
					INNER JOIN transfer_orders tro ON tro.transfer_id = t.id
					INNER JOIN order_packages op ON op.order_id = tro.order_id
					set user_id = ?
					WHERE escort_id = ?
				', array($user_id, $escort_id));
				
				
				$db->query('SET foreign_key_checks = 1');
                Cubix_SyncNotifier::notify($escort_id,Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array(
                    'escort_id'=>$escort_id,
                    'new_user_id'=>$user_id,
                    'old_user_id'=>$esc->user_id,
                    'action'=>'make new independent escort',
                    'coment'=>'updated orders and transfers',
                    'from'=>'SaverV2::updateUserRow'
                ));
			}
			/**
			 * If escort doesn't changed it's behaviour and he/she is
			 * independent simply update corresponding row in users table
			 */
			elseif ( ! $esc->agency_id ) {
				$data['user_data']['id'] = $esc->user_id;
				$users_model->save($user_item = new Model_UserItem($data['user_data']));
                if (in_array(Cubix_Application::getId(),array(APP_EG_CO_UK)) && $data['user_data']['sales_user_id'] && $data['user_data']['sales_user_id'] != $old_sales->sales_user_id ) {
                    $db->insert('sales_change_history', array('user_id' => $data['user_data']['id'],'modifier_id' => $user->id, 'user_type' => $data['user_data']['user_type'], 'from_sales_id' => $old_sales->sales_user_id, 'to_sales_id' => $data['user_data']['sales_user_id']));
                }

				if($data['user_data']['sales_user_id'] && $old_sales->sales_user_id != $new_sales && Cubix_Application::getId() == APP_ED) {
					self::getAdapter()->insert('escort_comments', array(
						'escort_id' => $escort_id,
						'comment' => 'change of sale from ' . $old_sales->username . ' to ' . $new_sales_username->username,
						'sales_user_id' => $user->id, 
						'date' => new Zend_Db_Expr('NOW()')
					));
				}

                Cubix_SyncNotifier::notify($escort_id,Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array(
                    'escort_id'=>$escort_id,
                    'user_id'=>$data['user_data']['id'],
                    'action'=>'update user row in users table',
                    'coment'=>'user type not changed',
                    'from'=>'SaverV2::updateUserRow'
                ));
			}
			else {
				$agency = $agencies_model->getById($data['escort_data']['agency_id']);
				$data['escort_data']['user_id'] = $agency['agency_data']->user_id;
			}
			
			if ( strlen($reg_email) ) {
				$db->update('users', array('email' => $reg_email), $db->quoteInto('id = ?', $data['escort_data']['user_id']));
			}
		}
	}

	const ESCORT_MARKED_SUSPICIOUS = 1;
	const ESCORT_REMOVED_SUSPICIOUS = 2;

	private function saveSuspiciousJournal($data)
	{
		$db = self::getAdapter();

		if ( ! is_array($data) ) $data = array();

		if ( Zend_Auth::getInstance()->hasIdentity() ) {
			$bo_user = Zend_Auth::getInstance()->getIdentity();
			$data['sales_user_id'] = $bo_user->id;
		}

		try {
			$db->insert('suspicious_journal', $data);
		}
		catch(Exception $e) {

		}
	}

	public function assignSales($action, $hash)
	{
		$db = self::getAdapter();
        $curr_user = Zend_Auth::getInstance()->getIdentity();
		if ( $action == 'confirm' ) {
			$new_sales = $db->fetchRow('SELECT id AS user_id, username, user_type, sales_user_id, new_sales_user_id, email FROM users WHERE new_sales_hash = ?', $hash);
			$db->update('users', array('sales_user_id' => $new_sales->new_sales_user_id, 'new_sales_user_id' => null, 'new_sales_hash' => null), $db->quoteInto('new_sales_hash = ?', $hash));
            if (in_array(Cubix_Application::getId(),array(APP_ED,APP_EG_CO_UK))) {
                $db->insert('sales_change_history', array('user_id' => $new_sales->user_id,'modifier_id' => $curr_user->id, 'user_type' => $new_sales->user_type, 'from_sales_id' => $new_sales->sales_user_id, 'to_sales_id' => $new_sales->new_sales_user_id));
            }
			$old_sales = $db->fetchRow('
				SELECT bu.id,bu.username, bu.email
				FROM backend_users bu
				WHERE bu.id = ?
			', $new_sales->sales_user_id);

			$new_sales_info = $db->fetchRow('
				SELECT bu.id,bu.username, bu.email
				FROM backend_users bu
				WHERE bu.id = ?
			', $new_sales->new_sales_user_id);

			if (in_array(Cubix_Application::getId(),array(APP_EF)))
            {
                $requested = $db->fetchOne('SELECT id FROM sales_agent_sms WHERE sales_from_id = ? AND sales_to_id = ? AND user_id = ?  AND status = ? ', array($old_sales->id,$new_sales_info->id,$new_sales->user_id,SALES_CHANGE_PENDING));
                if (isset($requested))
                {
                    $db->update('sales_agent_sms', array('status' => SALES_CHANGE_APPROOVED),$db->quoteInto('id = ?', $requested));
                }
            }

			if ( $new_sales->user_type == 'escort' ) {
				$u_id = $db->fetchOne('SELECT id FROM escorts where user_id = ?', $new_sales->user_id);
			}
			else if ( $new_sales->user_type == 'agency' ) {
				$u_id = $db->fetchOne('SELECT id FROM agencies where user_id = ?', $new_sales->user_id);
			}

            if (in_array(Cubix_Application::getId(),array(APP_EF))) {
                $email_data = array(
                    'salesname' => $old_sales->username,
                    'user_type' => $new_sales->user_type,
                    'showname' => $new_sales->username,
                    'id' => $u_id,
                    'new_sales' => $new_sales_info->username
                );
                $templateId = 'new_sales_change_notification';
                $to_email = $old_sales->email;
            }else{
                $email_data = array(
                    'username' => $new_sales_info->username,
                    'salesname' => $old_sales->username,
                    'user_type' => $new_sales->user_type,
                    'u_id' => $u_id
                );
                $templateId = 'escort_new_sales_verification_confirmed';
                $to_email = $new_sales_info->email;
            }


			if ( $new_sales->sales_user_id != $curr_user->id ) {
				Cubix_Email::sendTemplate($templateId, $to_email, $email_data);
			}

			if ( $new_sales->user_type == 'escort' ) {
				Cubix_SyncNotifier::notify($u_id, Cubix_SyncNotifier::EVENT_ESCORT_PROFILE_TRANSFER, array('before_sales_id' => $new_sales->sales_user_id, 'after_sales_id' => $new_sales->new_sales_user_id));
			}
			
			
		}
		else {
            if (in_array(Cubix_Application::getId(),array(APP_EF)))
            {
                $new_sales = $db->fetchRow('SELECT id AS user_id, user_type, sales_user_id, new_sales_user_id, email FROM users WHERE new_sales_hash = ?', $hash);
                $requested = $db->fetchOne('SELECT id FROM sales_agent_sms WHERE sales_from_id = ? AND sales_to_id = ? AND user_id = ?  AND status = ? ', array($new_sales->sales_user_id,$new_sales->new_sales_user_id,$new_sales->user_id,SALES_CHANGE_PENDING));
                if (isset($requested))
                {
                    $db->update('sales_agent_sms', array('status' => SALES_CHANGE_DECLINED),$db->quoteInto('id = ?', $requested));
                }
            }
			$db->update('users', array('new_sales_user_id' => null, 'new_sales_hash' => null), $db->quoteInto('new_sales_hash = ?', $hash));
		}
	}
	
	public function __assignSalesByUserId($user_id, $sales_id)
	{
		$db = self::getAdapter();

		$db->update('users', array('sales_user_id' => $sales_id), $db->quoteInto('id = ?', $user_id));
	}

	public function assignSalesByUserId($user_id, $sales_id, $escort_id)
	{
		$db = self::getAdapter();

		if ( ! $escort_id ) return false;
		
		$curr_user = Zend_Auth::getInstance()->getIdentity();
		
		$old_sales = $db->fetchRow('
			SELECT u.id as user_id, u.user_type, u.sales_user_id, bu.username, bu.email
			FROM users u
			INNER JOIN backend_users bu ON bu.id = u.sales_user_id
			WHERE u.id = ?
		', $user_id);
		
		$new_sales = $sales_id;
		$new_sales_username = $db->fetchRow('SELECT username, email FROM backend_users WHERE id = ?', $new_sales);
		
		$showname = $db->fetchOne('SELECT showname FROM escorts WHERE id = ?', $escort_id);
		
		if ( ! $showname && ( $curr_user->type == 'superadmin' ||  $curr_user->id == 204 /* sophie */ )){
			$db->update('users', array('sales_user_id' => $sales_id), $db->quoteInto('id = ?', $user_id));
            if (in_array(Cubix_Application::getId(),array(APP_ED,APP_EG_CO_UK)))
            {
                $db->insert('sales_change_history', array('user_id' => $old_sales->user_id, 'modifier_id' => $curr_user->id, 'user_type' => $old_sales->user_type, 'from_sales_id' => $old_sales->sales_user_id,'to_sales_id' => $new_sales));
            }
			if($old_sales->sales_user_id != $new_sales && in_array(Cubix_Application::getId(),array(APP_ED,APP_EF))) {
				$db->insert('escort_comments', array(
					'escort_id' => $escort_id,
					'comment' => 'change of sale from ' . $old_sales->username . ' to ' . $new_sales_username->username,
					'sales_user_id' => $curr_user->id, 
					'date' => new Zend_Db_Expr('NOW()')
				));
			}
		} 

		if ( ! $showname ) return false;
		
		if ($curr_user->type == 'superadmin' || $curr_user->type == 'dash manager' || $curr_user->id == 204 /* sophie */)
		{
			$db->update('users', array('sales_user_id' => $sales_id), $db->quoteInto('id = ?', $user_id));
            if (in_array(Cubix_Application::getId(),array(APP_ED,APP_EG_CO_UK)))
            {
                $db->insert('sales_change_history', array('user_id' => $old_sales->user_id,'modifier_id' => $curr_user->id, 'user_type' => $old_sales->user_type, 'from_sales_id' => $old_sales->sales_user_id,'to_sales_id' => $new_sales));
            }
			if($old_sales->sales_user_id != $new_sales && in_array(Cubix_Application::getId(),array(APP_ED,APP_EF))) {
				$db->insert('escort_comments', array(
					'escort_id' => $escort_id,
					'comment' => 'change of sale from ' . $old_sales->username . ' to ' . $new_sales_username->username,
					'sales_user_id' => $curr_user->id, 
					'date' => new Zend_Db_Expr('NOW()')
				));
			}

			$email_data = array(
				'salesname' => $new_sales_username->username,
				'showname' => $showname,
				'escort_id' => $escort_id,
                'username' => $old_sales->username
			);

			if (in_array(Cubix_Application::getId(),array(APP_ED,APP_EG_CO_UK)))
            {
                if ( ( Cubix_Application::getId() == APP_ED && !in_array( $new_sales, array( 197, 198, 196 ))) || ( Cubix_Application::getId() == APP_EG_CO_UK && !in_array( $new_sales, array( 191, 192 ))))
                {
                    Cubix_Email::sendTemplate('escort_new_sales_verification_superadmin', $old_sales->email, $email_data);
                    $email_data['username'] = $new_sales_username->username;
                    Cubix_Email::sendTemplate('escort_new_sales_verification_superadmin', $new_sales_username->email, $email_data);
                }
            }else{
                    Cubix_Email::sendTemplate('escort_new_sales_verification_superadmin', $old_sales->email, $email_data);
                    $email_data['username'] = $new_sales_username->username;
                    Cubix_Email::sendTemplate('escort_new_sales_verification_superadmin', $new_sales_username->email, $email_data);
            }

			if ( $new_sales != $old_sales->sales_user_id ) {
				Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PROFILE_TRANSFER, array('before_sales_id' => $old_sales->sales_user_id, 'after_sales_id' => $new_sales));
			}
		}
		else
		{
			if ( $old_sales->sales_user_id != $new_sales /*&& ! strlen($data['escort_data']['agency_id'])*/ ) {
				/*unset($data['user_data']['sales_user_id']);*/
				
				$new_sales_hash = md5(microtime() * microtime());
				$db->update('users', array('new_sales_user_id' => $new_sales, 'new_sales_hash' => $new_sales_hash), $db->quoteInto('id = ?', $user_id));

				if ( $old_sales->sales_user_id != $curr_user->id ) {
					$email_data = array(
						'new_sales_hash' => $new_sales_hash,
						'salesname' => $new_sales_username->username,
						'showname' => $showname,
						'username' => $old_sales->username,
						'escort_id' => $escort_id
					);

                    if (in_array(Cubix_Application::getId(),array(APP_ED,APP_EG_CO_UK)))
                    {
                        if ( ( Cubix_Application::getId() == APP_ED && !in_array( $new_sales, array( 197, 198, 196 ))) || ( Cubix_Application::getId() == APP_EG_CO_UK && !in_array( $new_sales, array( 191, 192 ))))
                        {
                            Cubix_Email::sendTemplate('escort_new_sales_verification', $old_sales->email, $email_data);
                        }
                    }else{
                        Cubix_Email::sendTemplate('escort_new_sales_verification', $old_sales->email, $email_data);
                    }
				}
				else {
					$email_data = array(
						'new_sales_hash' => $new_sales_hash,
						'salesname' => $old_sales->username,
						'showname' => $showname,
						'username' => $new_sales_username->username,
						'escort_id' => $escort_id
					);

                    if (in_array(Cubix_Application::getId(),array(APP_ED,APP_EG_CO_UK)))
                    {
                        if ( ( Cubix_Application::getId() == APP_ED && !in_array( $new_sales, array( 197, 198, 196 ))) || ( Cubix_Application::getId() == APP_EG_CO_UK && !in_array( $new_sales, array( 191, 192 ))))
                        {
                            Cubix_Email::sendTemplate('escort_new_sales_verification_m', /*$old_sales->email*/$new_sales_username->email, $email_data);
                        }
                    }else{
                        Cubix_Email::sendTemplate('escort_new_sales_verification_m', /*$old_sales->email*/$new_sales_username->email, $email_data);
                    }
				}
			}
		}
	}
	public function removeDeletedDate($escort_id)
	{
		$db = self::getAdapter();
		return $db->update('escorts', array('deleted_date' => null, 'date_last_modified' => new Zend_Db_Expr('NOW()') ), $db->quoteInto('id = ?', $escort_id));
	}
	
	public function simpleSave($data)
	{
		$db = self::getAdapter();
		$salt_hash = Cubix_Salt::generateSalt($data['password']);
		$pass = Cubix_Salt::hashPassword($data['password'] , $salt_hash);
	
		$user_data = array(
			'username' => $data['username'],
			'email' => $data['email'],
			'user_type' => 'escort',
			'sales_user_id' => $data['sales_user_id'],
			'salt_hash' => $salt_hash,
			'password' => $pass,
			'status' => USER_STATUS_ACTIVE,
			'application_id' => Cubix_Application::getId(),
			'date_registered' => new Zend_Db_Expr('CURRENT_TIMESTAMP')
		);
			
		$db->insert('users', $user_data);
		$user_id = $db->lastInsertId();
		
		$escort_data = array(
			'user_id' => $user_id,
			'country_id' => Cubix_Application::getById()->country_id
		);
		
		$db->insert('escorts',  $escort_data);
		$escort_id = $db->lastInsertId();
		$db->insert('escort_profiles_v2', array('escort_id' => $escort_id));
		$status = new Cubix_EscortStatus($escort_id);
		
		Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_CREATED);
		
		$statuses = array(
			Model_Escorts::ESCORT_STATUS_IS_NEW,
			Model_Escorts::ESCORT_STATUS_NO_PROFILE,
			Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS
		);
		$status->setStatusBit($statuses);
		$status->save();
		
		return $escort_id;
	}
}
