<?php

class Model_Geography_FakeCities extends Cubix_Model
{
	protected $_table = 'f_cities';
	
	public function getAll($region_id, $country_id, $p, $pp, $sort_field, $sort_dir, &$count)
	{
		if ( $region_id ) {
			$count = self::getAdapter()->fetchOne('
				SELECT COUNT(id) FROM '.$this->_table.'
				WHERE region_id = ? AND country_id = ? 
			', array($region_id, $country_id));

			return parent::_fetchAll('
				SELECT fc.id, fc.country_id, fc.region_id, fc.zip, fc.' . Cubix_I18n::getTblField('title') . ', fc.is_french, tz.title AS time_zone, tz.shift AS time_zone_shift  FROM  '.$this->_table.'  fc
				LEFT JOIN time_zones tz ON tz.id = 	fc.time_zone_id
				WHERE fc.region_id = ? AND fc.country_id = ?
				ORDER BY ' . $sort_field . ' ' . $sort_dir . '
				LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
			', array( $region_id, $country_id));
		}
		else {
			$count = self::getAdapter()->fetchOne('
				SELECT COUNT(id) FROM '.$this->_table.'
				WHERE country_id = ?
			', array($country_id));

			return parent::_fetchAll('
				SELECT fc.id, fc.country_id, fc.region_id, fc.zip,  fc.' . Cubix_I18n::getTblField('title') . ', fc.is_french, tz.title AS time_zone, tz.shift AS time_zone_shift FROM '.$this->_table.' fc
				LEFT JOIN time_zones tz ON tz.id = 	fc.time_zone_id
				WHERE fc.country_id = ?
				ORDER BY ' . $sort_field . ' ' . $sort_dir . '
				LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
			', array($country_id));
		}
	}
	
	public function ajaxGetAll($region_id, $country_id)
	{
		if ( $region_id )
		{
			return parent::_fetchAll('
				SELECT c.id, ' . Cubix_I18n::getTblField('c.title') . ' AS title, ' . Cubix_I18n::getTblField('r.title') . ' AS region_title, co.has_regions FROM cities c
				LEFT JOIN regions r ON r.id = c.region_id
				LEFT JOIN countries co ON co.id = c.country_id
				WHERE c.region_id = ?
				GROUP BY c.id
				ORDER BY title ASC
			', $region_id);
		}
		else
		{
			return parent::_fetchAll('
				SELECT c.id, ' . Cubix_I18n::getTblField('c.title') . ' AS title, ' . Cubix_I18n::getTblField('r.title') . ' AS region_title, co.has_regions FROM cities c
				LEFT JOIN regions r ON r.id = c.region_id
				LEFT JOIN countries co ON co.id = c.country_id
				WHERE c.country_id = ?
				GROUP BY c.id
				ORDER BY title ASC
			', $country_id);
		}
	}
	
	public function get($id)
	{
		return parent::_fetchRow('
			SELECT id, country_id, region_id, zip, ' . Cubix_I18n::getTblFields('title') . ', time_zone_id FROM '.$this->_table.'
			WHERE id = ?
		', $id);
	}
	
	public function getRegionId($city_id)
	{
		return parent::_fetchRow('
			SELECT region_id FROM '.$this->_table.'
			WHERE id = ?
		', $city_id);
	}
	
	public function save(Model_Geography_CityItem $item) {
		try {
				$slug = Cubix_Utils::makeSlug(Cubix_Model::translit($item->{'title_' . Cubix_Application::getDefaultLang()}));
				$item->setSlug($slug);
				$areas = $item->areas;
				unset($item->areas);
				parent::save($item);
				$f_city_id = isset($item->id) ? $item->id : parent::getAdapter()->lastInsertId();

				if(isset($item->id)){
					parent::getAdapter()->query('DELETE FROM f_cities_area WHERE f_city_id = ?', array($item->id));
				}
				if ( count($areas) > 0 ) {
						foreach ( $areas as $area ) {
							parent::getAdapter()->insert('f_cities_area', array('f_city_id' => $f_city_id, 'area_id' => $area ));
						}
				}
			}
		catch (Exception $e) {
			parent::getAdapter()->rollBack();

			throw $e;
		}
	}
	
	public function remove($id)
	{
		try {
			parent::remove(self::quote('id = ?' , $id));
			parent::getAdapter()->query('DELETE FROM f_cities_area WHERE f_city_id = ?', array($id));
		}
		catch (Exception $e) {
			parent::getAdapter()->rollBack();

			throw $e;
		}
	}
	
	public function cityAreas($f_city_id){
		return parent::_fetchAll('SELECT fca.area_id, ' . Cubix_I18n::getTblField('c.title') . ' AS title FROM f_cities_area fca
								  INNER JOIN cities c ON fca.area_id = c.id   WHERE f_city_id = ?',array($f_city_id));
	}
	
	public static function getFakeCityIdByTitle($title)
	{
		return parent::getAdapter()->fetchOne('
			SELECT id FROM f_cities
			WHERE title_en = ?
		', $title);
	}
	
	public static function getAreaIdByTitle($title)
	{
		return parent::getAdapter()->fetchOne('
			SELECT id FROM cities
			WHERE title_en = ?
		', $title);
	}	
}
