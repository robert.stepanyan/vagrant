<?php

class Model_Geography_Regions extends Cubix_Model
{
	protected $_table = 'regions';
	
	protected $_itemClass = 'Model_Geography_RegionItem';
	
	public function getAll($country_id, $p, $pp, $sort_field, $sort_dir, &$count, $title = null)
	{
		$where = '';
		if ( ! is_null($title) && strlen($title) ) {
			$where = ' AND title_en LIKE "%' . $title . '%"';
		}
		
		$count = self::getAdapter()->fetchOne('
			SELECT COUNT(id) FROM regions 
			WHERE country_id = ? ' . $where, $country_id);
		
		return parent::_fetchAll('
			SELECT id, ' . Cubix_I18n::getTblField('title') . ', state, is_french FROM regions
			WHERE country_id = ? ' . $where . ' 
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
		', $country_id);
	}
	
	public function ajaxGetAll($country_id)
	{
		return parent::_fetchAll('
			SELECT id, ' . Cubix_I18n::getTblField('title') . ' AS title FROM regions
			WHERE country_id = ?
			ORDER BY title ASC
		', $country_id);
	}

    public function ajaxGetAreas($region_id)
    {
        return parent::_fetchAll('
			SELECT id, ' . Cubix_I18n::getTblField('title') . ' AS title FROM f_cities
			WHERE region_id = ?
			ORDER BY title ASC
		', $region_id);
    }
    
	public function get($id)
	{
		return parent::_fetchRow('
			SELECT id, country_id, ' . Cubix_I18n::getTblFields('title') . ', state FROM regions
			WHERE id = ?
		', $id);
	}

    public function getAllRegions()
    {
        return parent::_fetchAll('
			SELECT * FROM regions
			WHERE true');
    }
		
	public function save(Model_Geography_RegionItem $item) {
		$slug = Cubix_Utils::makeSlug(Cubix_Model::translit($item->{'title_' . Cubix_I18n::getLang()}));
		$item->setSlug($slug);
		
		parent::save($item);
	}
	
	public function remove($id)
	{
		parent::remove(self::quote('id = ?' , $id));
	}

	public function getList($title)
	{
		return parent::_fetchAll('
			SELECT r.id, CONCAT(r.title_en, " (", c.title_en, ")") AS title FROM regions r
			INNER JOIN countries c ON c.id = r.country_id
			WHERE r.title_en LIKE ? AND c.id = ?
			ORDER BY title ASC
			LIMIT 10
		', array($title . '%', Cubix_Application::getById($this->_app_id)->country_id));
	}
}
