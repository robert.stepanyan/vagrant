<?php

class Model_Geography_Cities extends Cubix_Model
{
	protected $_table = 'cities';
	
	protected $_itemClass = 'Model_Geography_CityItem';
	
	public function getAll($region_id, $country_id, $p, $pp, $sort_field, $sort_dir, &$count, $title = null, $no_timezone = false, $zona_rossa = false)
	{
		$where = '';
		if ( ! is_null($title) && strlen($title) ) {
            $where = ' AND title_en LIKE "%' . $title . '%"';
		}
		if($no_timezone){
			$where = ' AND time_zone_id IS NULL ';
		}

		if($zona_rossa){
			$where = ' AND zona_rossa = '. $zona_rossa;
		}

		if ( $region_id ) {
			$count = self::getAdapter()->fetchOne('
				SELECT COUNT(id) FROM cities
				WHERE region_id = ? AND country_id = ? ' . $where
			, array($region_id, $country_id));
			
			$zona_rossa = '';
			if(Cubix_Application::getId() == APP_EF){
				$zona_rossa = ' zona_rossa,';
			}

			return parent::_fetchAll('
				SELECT '.$zona_rossa .'c.id, c.country_id, c.region_id, c.' . Cubix_I18n::getTblField('title') . ', c.is_french, tz.title AS time_zone, tz.shift AS time_zone_shift, c.title_geoip
				FROM cities c
				LEFT JOIN time_zones tz ON tz.id = 	c.time_zone_id
				WHERE c.region_id = ? AND c.country_id = ? ' . $where . ' 
				ORDER BY ' . $sort_field . ' ' . $sort_dir . '
				LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
			', array($region_id, $country_id));
		}
		else {

		    if(Cubix_Application::getId() == APP_ED && !$country_id){
                $count = self::getAdapter()->fetchOne('
				SELECT COUNT(id) FROM cities
				WHERE 1 ' . $where
                    , array());

                return parent::_fetchAll('
				SELECT c.id, c.country_id, c.region_id, c.' . Cubix_I18n::getTblField('title') . ', c.is_french, tz.title AS time_zone, tz.shift AS time_zone_shift, c.title_geoip 
				FROM cities c
				LEFT JOIN time_zones tz ON tz.id = 	c.time_zone_id
				WHERE 1 ' . $where . ' 
				ORDER BY ' . $sort_field . ' ' . $sort_dir . '
				LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
			', array());
            }elseif(Cubix_Application::getId() == APP_EF){
                $count = self::getAdapter()->fetchOne('
				SELECT COUNT(id) FROM cities
				WHERE country_id = ? ' . $where
                    , array($country_id));

                return parent::_fetchAll('
				SELECT c.id, c.country_id, c.region_id, c.' . Cubix_I18n::getTblField('title') . ', c.is_french, tz.title AS time_zone, tz.shift AS time_zone_shift, c.title_geoip , zona_rossa
				FROM cities c
				LEFT JOIN time_zones tz ON tz.id = 	c.time_zone_id
				WHERE c.country_id = ? ' . $where . ' 
				ORDER BY ' . $sort_field . ' ' . $sort_dir . '
				LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
			', array($country_id));
            }else{
                $count = self::getAdapter()->fetchOne('
				SELECT COUNT(id) FROM cities
				WHERE country_id = ? ' . $where
                    , array($country_id));

                return parent::_fetchAll('
				SELECT c.id, c.country_id, c.region_id, c.' . Cubix_I18n::getTblField('title') . ', c.is_french, tz.title AS time_zone, tz.shift AS time_zone_shift, c.title_geoip
				FROM cities c
				LEFT JOIN time_zones tz ON tz.id = 	c.time_zone_id
				WHERE c.country_id = ? ' . $where . ' 
				ORDER BY ' . $sort_field . ' ' . $sort_dir . '
				LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
			', array($country_id));
            }


		}
	}
	
	/*public function ajaxGetAll($region_id, $country_id)
	{
		return parent::_fetchAll('
			SELECT c.id, ' . Cubix_I18n::getTblField('c.title') . ' AS title, ' . Cubix_I18n::getTblField('r.title') . ' AS region_title, co.has_regions FROM cities c
			LEFT JOIN regions r ON c.region_id = r.id
			LEFT JOIN countries co ON co.id = r.country_id
			WHERE ' . ($region_id ? 'c.region_id = ?' : '(c.country_id = ? OR r.country_id = ?)') . '
			GROUP BY c.id
			ORDER BY title ASC
		', $region_id ? $region_id : array($country_id, $country_id));
	}*/
	
	public function ajaxGetAll($region_id, $country_id, $city_criteria = null)
	{
		if ( $region_id )
		{
			return parent::_fetchAll('
				SELECT c.id, ' . Cubix_I18n::getTblField('c.title') . ' AS title, ' . Cubix_I18n::getTblField('r.title') . ' AS region_title, co.has_regions FROM cities c
				LEFT JOIN regions r ON r.id = c.region_id
				LEFT JOIN countries co ON co.id = c.country_id
				WHERE c.region_id = ?
				GROUP BY c.id
				ORDER BY title ASC
			', $region_id);
		}
		else
		{
			if ( ! $city_criteria ) {
				return parent::_fetchAll('
					SELECT c.id, ' . Cubix_I18n::getTblField('c.title') . ' AS title, ' . Cubix_I18n::getTblField('r.title') . ' AS region_title, co.has_regions FROM cities c
					LEFT JOIN regions r ON r.id = c.region_id
					LEFT JOIN countries co ON co.id = c.country_id
					WHERE c.country_id = ?
					GROUP BY c.id
					ORDER BY title ASC
				', $country_id);
			} else {

				$where = " AND e.status & 32 ";
				if ( $city_criteria == "with_no_active_ads" ) {
					$where = " AND e.status ^ 32 ";
				}

				return parent::_fetchAll('
					SELECT c.id, ' . Cubix_I18n::getTblField('c.title') . ' AS title, ' . Cubix_I18n::getTblField('r.title') . ' AS region_title, co.has_regions FROM cities c
					LEFT JOIN regions r ON r.id = c.region_id
					LEFT JOIN countries co ON co.id = c.country_id
					INNER JOIN escort_cities ec ON ec.city_id = c.id
					INNER JOIN escorts e ON e.id = ec.escort_id
					WHERE c.country_id = ? ' . $where . '
					GROUP BY c.id
					ORDER BY title ASC
				', $country_id);
			}
		}
	}
	
	public function ajaxGetAllWoCountry()
	{
		return parent::_fetchAll('
				SELECT c.id, ' . Cubix_I18n::getTblField('c.title') . ' AS title, ' . Cubix_I18n::getTblField('r.title') . ' AS region_title, co.has_regions FROM cities c
				LEFT JOIN regions r ON r.id = c.region_id
				LEFT JOIN countries co ON co.id = c.country_id
				GROUP BY c.id
				ORDER BY title ASC
			');
	}
	
	public function ajaxGetAllCountryByAppId($app_id)
	{
		$where = ' AND co.id IN (' . Cubix_Application::getById()->country_id . ') ';
		if ($app_id == 30) {
			$c_arr = array(20, 119, 145);
			$where = ' AND co.id IN (' . implode(', ', $c_arr) . ') ';
		} else if ( $app_id == 11 ) {
			$c_arr = array(8, 2, 9, 6, 4, 3, 10, 7, 1, 5, 11);
			$where = ' AND co.id IN (' . implode(', ', $c_arr) . ') ';
		}
		
		return parent::_fetchAll('
				SELECT c.id, ' . Cubix_I18n::getTblField('c.title') . ' AS title, ' . Cubix_I18n::getTblField('r.title') . ' AS region_title, co.has_regions FROM cities c
				LEFT JOIN regions r ON r.id = c.region_id
				LEFT JOIN countries co ON co.id = c.country_id
				WHERE 1 ' . $where . '
				GROUP BY c.id
				ORDER BY title ASC
			');
	}
	
	public function get($id)
	{
		return parent::_fetchRow('
			SELECT id, country_id, region_id, ' . Cubix_I18n::getTblFields('title') . ', time_zone_id, url_slug, order_id, title_geoip 
			FROM cities
			WHERE id = ?
		', $id);
	}

	public function getCountry($city_id)
	{
		return parent::_fetchRow('
			SELECT co.id AS country_id, co.title_en AS country_title
			FROM cities c
			INNER JOIN countries co ON co.id = c.country_id
			WHERE c.id = ?
		', $city_id);
	}
	
	public function getRegionId($city_id)
	{
		return parent::_fetchRow('
			SELECT region_id FROM cities
			WHERE id = ?
		', $city_id);
	}
	
	public function save(Model_Geography_CityItem $item) {
		$slug = Cubix_Utils::makeSlug(Cubix_Model::translit($item->{'title_' . Cubix_Application::getDefaultLang()}));
		$item->setSlug($slug);
		
		$last_order_id = parent::db()->fetchOne('SELECT order_id FROM cities WHERE region_id = ? ORDER BY order_id DESC', array($item->region_id));
		$item->order_id = $last_order_id + 1;
		parent::save($item);
	}
	
	public function remove($id)
	{
		try {
			parent::db()->beginTransaction();
			//Fixing orders before removing
			$item = $this->get($id);
			$rows = parent::db()->fetchAll('SELECT * FROM cities WHERE region_id = ? AND order_id > ?', array($item->region_id, $item->order_id));

			foreach($rows as $row) {
				parent::db()->update('cities', array('order_id' => $row->order_id - 1), parent::db()->quoteInto('id = ?', $row->id));
			}
			
			parent::remove(self::quote('id = ?' , $id));
			parent::db()->commit();
		} catch (Exception $ex) {
			parent::db()->rollBack();
			throw $ex;
		}
	}
	
	public function getList($title, $for_def_country_only = true)
	{
		$bind = array($title . '%');
		if ( $for_def_country_only ) {
			$bind[] = Cubix_Application::getById($this->_app_id)->country_id;
		}

		return parent::_fetchAll('
			SELECT ct.id, CONCAT(ct.title_en, " (", c.title_en, ")") AS title FROM cities ct
			INNER JOIN countries c ON c.id = ct.country_id
			WHERE ct.title_en LIKE ?' . ($for_def_country_only ? ' AND c.id = ?' : '') . '
			GROUP BY ct.id
			ORDER BY title ASC
			LIMIT 10
		', $bind);
	}
	
	public function getFList($title, $for_def_country_only = true)
	{
		$bind = array($title . '%');
		if ( $for_def_country_only ) {
			$bind[] = Cubix_Application::getById($this->_app_id)->country_id;
		}

		return parent::_fetchAll('
			SELECT ct.id, CONCAT(ct.title_en, " (", c.title_en, ")") AS title FROM f_cities ct
			INNER JOIN countries c ON c.id = ct.country_id
			WHERE ct.title_en LIKE ?' . ($for_def_country_only ? ' AND c.id = ?' : '') . '
			GROUP BY ct.id
			ORDER BY title ASC
			LIMIT 10
		', $bind);
	}

	public function getFakeCities()
	{
		return parent::_fetchAll('
			SELECT id, title_de AS title, zip FROM f_cities
			ORDER BY title ASC
		');
	}

    public function getCities()
    {
        return parent::_fetchAll('
			SELECT id, title_en AS title FROM cities
			ORDER BY title ASC
		');
    }

    public function getPremiumCities()
    {
        return parent::db()->fetchAll('
			SELECT
	        c.title_en as title
            FROM
	        cities c
            INNER JOIN f_cities fc on fc.region_id = c.region_id
            GROUP BY title
		');
    }
	
	public function getAllCities($country_id)
	{
		return parent::_fetchAll('
			SELECT id, ' . Cubix_I18n::getTblField('title') . ' AS title FROM cities
			WHERE country_id = ?
			ORDER BY title ASC
		', $country_id);
	}

    public function getRegionCities($region_id)
    {
        return parent::_fetchAll('
			SELECT id, ' . Cubix_I18n::getTblField('title') . ' AS title FROM cities
			WHERE region_id = ?
			ORDER BY title ASC
		', $region_id);
    }

	public function changeOrder($id, $dir)
	{
		$row = parent::_fetchRow('SELECT id, order_id, region_id FROM cities WHERE id = ?', array($id));
		
		if ( $dir == 'up' ) {
			$index = $row->order_id - 1;
		} else {
			$index = $row->order_id + 1;
		}
		
		$s_row = parent::_fetchRow('SELECT id, order_id FROM cities WHERE order_id = ? AND region_id = ?', array($index, $row->region_id));
		
		if ( ! $s_row  ) {
			return;
		}
		
		parent::db()->beginTransaction();
		try {
			parent::db()->update($this->_table, array('order_id' => $s_row->order_id), parent::quote('id = ?', $row->id));
			parent::db()->update($this->_table, array('order_id' => $row->order_id), parent::quote('id = ?', $s_row->id));
			parent::db()->commit();
		} catch(Exception $ex) {
			parent::db()->rollBack();
			throw $ex;
		}
		
		return;
	}
	
	public function SetTimeZones($ids,$timezone)
	{
		$sql = ' UPDATE cities SET time_zone_id = '.$timezone.' WHERE id IN ('.implode(',', $ids).')';
		parent::getAdapter()->query($sql);
	}


	public function ajaxGetAllGroupedByCountry($country_id)
	{		
		return parent::_fetchAll('
			SELECT 
				c.id, ' . Cubix_I18n::getTblField('c.title') . ' AS title,
				co.id AS country_id, ' . Cubix_I18n::getTblField('co.title') . ' AS country_title
			FROM cities c
			INNER JOIN countries co ON co.id = c.country_id			
			GROUP BY c.id
			ORDER BY FIELD(co.id, "' . $country_id . '") DESC, co.title_en
		');
	}

    public function getAllExceptUsaFrance()
    {
        return parent::_fetchAll('
			SELECT 
				c.id, ' . Cubix_I18n::getTblField('c.title') . ' AS title,
				co.id AS country_id, ' . Cubix_I18n::getTblField('co.title') . ' AS country_title
			FROM cities c
			INNER JOIN countries co ON co.id = c.country_id
			WHERE country_id <> 23 AND country_id <> 68 OR region_id = 13			
			GROUP BY c.id
			ORDER BY country_title 
		');
    }
	
	public function getByIds($ids)
	{
		return parent::_fetchAll('SELECT id, title_en as title FROM cities WHERE id IN (' . $ids . ') ORDER BY title_en ASC');
	}

	public function getCitiesWithoutCoords()
	{
		return parent::_fetchAll('SELECT cc.id, cc.title_en, c.title_en as country FROM cities cc INNER JOIN countries c on c.id = cc.country_id
			WHERE cc.longitude IS NULL OR cc.latitude IS NULL ORDER BY	c.title_en ASC');
	}

	public function SetCoordinates($cityid, $longitude, $latitude)
	{
		$sql = "UPDATE cities SET longitude = '". $longitude . "', latitude = '" . $latitude . "' WHERE id = " . $cityid;
		parent::getAdapter()->query($sql);
	}

	public function getBySlug( $slug )
    {
        $sql = "SELECT
	                count(id) cnt 
                    FROM
                        `cities` 
                    WHERE
                        slug = '".$slug."'";
        return self::getAdapter()->fetchOne($sql);
    }

    public function getIdByTitleEn($title)
    {
        $sql = 'SELECT
                    c.id 
                FROM
                    f_cities_area fca
                    INNER JOIN cities c ON c.id = fca.area_id
                    INNER JOIN f_cities fc ON fc.id = fca.f_city_id 
                WHERE
                    fc.title_en LIKE "%'.$title.'%"';
        return self::getAdapter()->fetchOne($sql);
    }

    public function getIdByCitiesTitleEn($title)
    {
        $sql = 'SELECT
                    id 
                FROM
                    cities 
                WHERE
                    title_en = ?';
        return self::getAdapter()->fetchOne($sql,$title);
    }

    public function getIdByCitiesTitleDe($title)
    {
        $sql = 'SELECT
                    id 
                FROM
                    cities 
                WHERE
                    title_de = ?';
        return self::getAdapter()->fetchOne($sql,$title);
    }
}