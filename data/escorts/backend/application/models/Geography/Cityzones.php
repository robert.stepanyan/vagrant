<?php

class Model_Geography_Cityzones extends Cubix_Model
{
	protected $_table = 'cityzones';
	
	protected $_itemClass = 'Model_Geography_CityzoneItem';
	
	public function getAll($city_id, $p, $pp, $sort_field, $sort_dir, &$count, $title = null)
	{
		$where = '';
		if ( ! is_null($title) && strlen($title) ) {
			$where = ' AND title_en LIKE "%' . $title . '%"';
		}
		
		$count = self::getAdapter()->fetchOne('
			SELECT COUNT(id) FROM cityzones
			WHERE city_id = ? ' . $where
		, $city_id);
		
		return parent::_fetchAll('
			SELECT id,  ' . Cubix_I18n::getTblField('title') . ' FROM cityzones
			WHERE city_id = ? ' . $where . ' 
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
		', $city_id);
	}
	
	public function ajaxGetAll($city_id)
	{
		return parent::_fetchAll('
			SELECT c.id, ' . Cubix_I18n::getTblField('c.title') . ' AS title FROM cityzones c
			WHERE c.city_id = ?
			ORDER BY title ASC
		', array($city_id));
	}
	
	public function get($id)
	{
		return parent::_fetchRow('
			SELECT cz.id, cz.city_id, r.id AS region_id, ct.id AS country_id, ' . Cubix_I18n::getTblFields('cz.title') . ', ' . Cubix_I18n::getTblFields('c.title', null, null, 'city_title') . ' FROM cityzones cz
			LEFT JOIN cities c ON c.id = cz.city_id
			LEFT JOIN regions r ON r.id = c.region_id
			LEFT JOIN countries ct ON ct.id = c.country_id
			WHERE cz.id = ?
		', $id);
	}
	
	public function save(Model_Geography_CityzoneItem $item) {
		$m_cities = new Model_Geography_Cities();
		$city = $m_cities->get($item->city_id);
		
		$slug = Cubix_Utils::makeSlug(Cubix_Model::translit($city->title_en . '_' . $item->{'title_' . Cubix_I18n::getLang()}));
		$item->setSlug($slug);
		
		parent::save($item);
	}
	
	public function remove($id)
	{
		parent::remove(self::quote('id = ?' , $id));
	}

	public function getList($value, $for_def_country_only = true)
	{
		$bind = array($value . '%');
		if ( $for_def_country_only ) {
			$bind[] = Cubix_Application::getById($this->_app_id)->country_id;
		}

		return parent::_fetchAll('
			SELECT cz.id, CONCAT(cz.title_en, " (", ct.title_en, ", ", c.title_en, ")") AS title FROM cityzones cz
			INNER JOIN cities ct ON ct.id = cz.city_id
			INNER JOIN countries c ON ct.country_id = c.id
			WHERE cz.title_en LIKE ?' . ($for_def_country_only ? ' AND c.id = ?' : '') . '
			ORDER BY title ASC
			LIMIT 10
		', $bind);
	}
}
