<?php
class Model_Users extends Cubix_Model
{
	protected $_table = 'users';
	protected $_itemClass = 'Model_UserItem';
	
	const USER_TYPE_MEMBER = 'member';
	const USER_TYPE_AGENCY = 'agency';
	const USER_TYPE_ESCORT = 'escort';

	public function get($id)
	{
		return parent::_fetchRow('
			SELECT id, user_type, username, email, sales_user_id FROM users WHERE id = ?
		', array($id));
	}

	public function activate($user_id)
	{
		$this->_db->update('users', array(
			'activation_hash' => null,
			'status' => STATUS_ACTIVE
		), array(
			$this->_db->quoteInto('id = ?', $user_id)
		));

		return true;
	}
	
	public function setStatus($user_id,$status)
	{
		$this->_db->update('users', array(
			'status' => $status
		), array(
			$this->_db->quoteInto('id = ?', $user_id)
		));

		return true;
	}

	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT
				u.id,
				e.id as escort_id,
				u.username,
				u.email,
				u.user_type,
				u.application_id,
				UNIX_TIMESTAMP(u.date_registered) AS creation_date,
				c.iso AS app_iso,
				u.status,
				u.activation_hash
			FROM users u
			INNER JOIN applications a ON a.id = u.application_id
			LEFT JOIN countries c ON c.id = a.country_id
			LEFT JOIN escorts e on e.user_id = u.id
			WHERE 1
		';

		$countSql = '
			SELECT COUNT(DISTINCT(u.id))
			FROM users u
			INNER JOIN applications a ON a.id = u.application_id
			LEFT JOIN countries c ON c.id = a.country_id
			WHERE 1
		';

		$where = '';

		
		if ( strlen($filter['application_id']) ) {
			$where .= self::quote('AND u.application_id = ?', $filter['application_id']);
		}

		/*if ( strlen($filter['not_verified']) ) {
			$where .= ' AND u.activation_hash IS NOT NULL AND status = -1 ';
		}*/
		if ( strlen($filter['status']) ) {
			$where .= self::quote('AND u.activation_hash IS NOT NULL AND u.status = ?', $filter['status']);
		}
		if ( strlen($filter['username']) ) {
			$where .= self::quote('AND u.username LIKE ?', $filter['username'] . '%');
		}

		if ( strlen($filter['email']) ) {
			$where .= self::quote('AND u.email LIKE ?','%'. $filter['email'] . '%');
		}

		if ( strlen($filter['user_type']) ) {
			$where .= self::quote('AND u.user_type = ?', $filter['user_type']);
		}
		
		if ( strlen($filter['sales_user_id']) ) {
			$where .= self::quote('AND u.sales_user_id = ?', $filter['sales_user_id']);
		}

		$sql .= $where;
		$countSql .= $where;
		//echo $sql; die;

		$sql .= '
			GROUP BY u.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$count = intval($this->getAdapter()->fetchOne($countSql));

		return parent::_fetchAll($sql);
	}

	public function save(Model_UserItem $item)
	{
		unset($item['groups']);
		parent::save($item);

		// Set the new inserted id
		if ( ! $item->getId() ) {
			$item->setId(self::getAdapter()->lastInsertId());
		}

		return parent::getAdapter()->lastInsertId();
	}
	
	public function remove($id)
	{
		parent::remove(self::quote('id = ?', $id));
	}

	static public function can($action)
	{
		$auth = Zend_Auth::getInstance();
		$user = $auth->getIdentity();

		switch ( $action ) {
			case 'publish-seo-data':
				return in_array($user->type, array( 'moderator', 'moderator plus', 'seo manager', 'admin', 'superadmin'));
			case 'edit-escort-profile':
				//Andy's request to allow dash manager edit profile
				if ( in_array(Cubix_Application::getId(), array(APP_A6) ) ) {
					return in_array($user->type, array( 'moderator', 'moderator plus', 'sales clerk','sales manager', 'dash manager', 'sales', 'payments manager', 'data entry','data entry plus', 'admin', 'superadmin'));
				}
				return in_array($user->type, array( 'moderator', 'moderator plus', 'sales manager', 'sales', 'payments manager', 'data entry','data entry plus', 'admin', 'superadmin'));
			case 'edit-escort-seo-data':
				return in_array($user->type, array( 'moderator', 'moderator plus', 'seo manager', 'seo copywriter', 'admin', 'superadmin'));
			case 'set-member-premium':
				return in_array($user->type, array('superadmin'));
		}
	}

	public static function getLastLogin($id)
	{
		return parent::getAdapter()->query('SELECT login_date, ip FROM users_last_login WHERE user_id = ? ORDER BY login_date DESC LIMIT 1', $id)->fetch();
	}
	public static function getLogins($id)
	{
		return parent::getAdapter()->fetchAll('SELECT * FROM users_last_login WHERE user_id = ? ORDER BY login_date DESC', $id);
	}

	public static function getLastLoginsCount($id)
	{
		return parent::getAdapter()->fetchOne('SELECT COUNT(*) FROM users_last_login WHERE user_id = ?', $id);
	}

	public static function getLastLogins($id, $interval)
	{
		//return parent::getAdapter()->fetchOne('SELECT COUNT(*) FROM users_last_login WHERE user_id = ? AND DATE_ADD(login_date, INTERVAL -' . $interval . ' DAY)', $id);
		return parent::getAdapter()->fetchOne('SELECT COUNT(*) FROM users_last_login WHERE user_id = ? AND DATE_ADD(NOW(), INTERVAL -' . $interval . ' DAY) <= login_date', $id);
	}

	public static function sendPostActivationEmail($user) {
		Cubix_Email::sendTemplate('activate_success', $user['email'], array(
			'username' => $user['username']
		));
	}
	public static function getUniqueIps($user_id, $grouped = true)
	{
		$select = "";
        $groupBy = $grouped ? "GROUP BY ip" : '';

		if(in_array(Cubix_Application::getId(), array(APP_ED) )){
			$select .= ", MAX(ip_blacklisted) AS ip_blacklisted"; 
			return parent::getAdapter()->fetchAll("SELECT ip, MAX(login_date) AS last_date {$select} FROM users_last_login WHERE user_id = ? GROUP BY ip ORDER BY MAX(login_date) DESC", $user_id);
		}elseif(in_array(Cubix_Application::getId(), array( APP_EG_CO_UK, APP_EF, APP_BL, APP_EM))) {

		    $select = $grouped ? 'MAX(login_date) AS last_date' : 'login_date';
		    $order = $grouped ? 'MAX(login_date) DESC' : 'login_date desc';

			return parent::getAdapter()->fetchAll("SELECT ip, {$select} FROM users_last_login WHERE user_id = ? {$groupBy} ORDER BY {$order}", $user_id);
		}
	}
	
	public static function checkNewSalesPerson($user_id, $sales_person)
	{
		return parent::getAdapter()->fetchOne('SELECT TRUE FROM users WHERE id = ? AND new_sales_user_id = ?', array($user_id, $sales_person));
	}
	
	public function getByEmail($email)
	{
		return parent::getAdapter()->query('
			SELECT u.id, u.user_type, e.id AS escort_id, a.id AS agency_id
			FROM users u 
			LEFT JOIN escorts e ON u.id = e.user_id
			LEFT JOIN agencies a ON u.id = a.user_id
			WHERE u.status <> -5 AND u.email = ?
			GROUP BY u.email
		', $email)->fetch();
	}
	
	public function getByEscortId($escort_id)
	{
		return parent::getAdapter()->fetchRow('
			SELECT u.id, u.email, u.username, u.status
			FROM users u 
			INNER JOIN escorts e ON u.id = e.user_id
			WHERE e.id = ?
		', $escort_id);
	}

		public function getByAgencyId($agency_id)
	{
		return parent::getAdapter()->fetchRow('
			SELECT u.id, u.email, u.username, u.status
			FROM users u 
			INNER JOIN agencies a ON u.id = a.user_id
			WHERE a.id = ?
		', $agency_id);
	}

    public function getStatusById($user_id)
    {
        return parent::getAdapter()->fetchRow('
			SELECT status
			FROM users 
			WHERE id = ?
		', $user_id);
    }
	
	public function setSystemDisabled($id, $status)
	{
		$this->_db->update('users', array('system_disabled' => $status), $this->_db->quoteInto('id = ?', $id));
	}

	public function get_all_for_antifraud($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT
				u.id,
				e.id as escort_id,
				e.agency_id,
				u.username,
				u.email,
				u.user_type,
				u.application_id,
				UNIX_TIMESTAMP(u.date_registered) AS creation_date,
				c.title_en,
				u.status,
				u.activation_hash,
				u.registered_ip,
				u.last_ip,
				UNIX_TIMESTAMP(u.last_login_date) AS last_login_date
				
			FROM users u
			INNER JOIN applications a ON a.id = u.application_id
			LEFT JOIN escorts e on e.user_id = u.id
			LEFT JOIN countries c ON c.id = e.country_id
			LEFT JOIN members m on m.user_id = u.id
			WHERE 1
		';

		$countSql = '
			SELECT COUNT(DISTINCT(u.id))
			FROM users u
			INNER JOIN applications a ON a.id = u.application_id
			LEFT JOIN countries c ON c.id = a.country_id
			WHERE 1
		';

		$where = '';

		
		if ( strlen($filter['application_id']) ) {
			$where .= self::quote('AND u.application_id = ?', $filter['application_id']);
		}

		/*if ( strlen($filter['not_verified']) ) {
			$where .= ' AND u.activation_hash IS NOT NULL AND status = -1 ';
		}*/
		// if ( strlen($filter['status']) ) {
		// 	$where .= self::quote('AND u.activation_hash IS NOT NULL AND u.status = ?', $filter['status']);
		// }
		if ( strlen($filter['username']) ) {
			$where .= self::quote('AND u.username LIKE ?', $filter['username'] . '%');
		}

		if ( strlen($filter['email']) ) {
			$where .= self::quote('AND u.email LIKE ?','%'. $filter['email'] . '%');
		}

		if ( strlen($filter['user_type']) ) {
			$where .= self::quote('AND u.user_type = ?', $filter['user_type']);
		}
		
		

		$sql .= $where;
		$countSql .= $where;
		//echo $sql; die;

		$sql .= '
			GROUP BY u.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$count = intval($this->getAdapter()->fetchOne($countSql));

		return parent::_fetchAll($sql);
	}

    public static function getExpirationDate($user_id)
    {
        return parent::getAdapter()->fetchOne('
			SELECT UNIX_TIMESTAMP(expiration_date) FROM users WHERE id = ?
		', array($user_id));
    }

    public static function setExpirationDate($user_id, $date = null, $mode = 'create')
    {
        if ( $mode && $mode == 'unset' )
        {
            return parent::getAdapter()->update('users', array('expiration_date' => $date), parent::getAdapter()->quoteInto('id = ?', $user_id));
        }else{
            return parent::getAdapter()->update('users', array(
                'expiration_date' => (is_int($date))? date("Y-m-d H:i:s", $date ) : null
            ), array(
                parent::getAdapter()->quoteInto('id = ?', $user_id)
            ));
        }
    }

    public function changeSalesByCountry($userId, $countryId, $backend_user_id, $current_sales_user_id, $userType)
    {
        $salesId = 1; //For France - admin

        if ($countryId === 68) {
            $salesId = 205; //For USA - USA 1
        }

        try {
            $this->_db->update('users', array('sales_user_id' => $salesId), $this->_db->quoteInto('id = ?', $userId));

            //implementing query which creates a logs in 'sales_change_history' table when escort country changes
            if ($current_sales_user_id && $salesId && $salesId !=$current_sales_user_id )
            {
                $this->_db->insert('sales_change_history', array('user_id' => $userId, 'modifier_id' => $backend_user_id, 'user_type' => $userType, 'from_sales_id' => $current_sales_user_id, 'to_sales_id' => $salesId, 'date' => new Zend_Db_Expr('NOW()') ) );
            }
        }
        catch ( Exception $e ) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }
    }
}
