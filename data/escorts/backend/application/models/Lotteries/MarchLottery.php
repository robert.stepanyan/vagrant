<?php

class Model_Lotteries_MarchLottery extends Cubix_Model
{
	protected $_table = 'march8_win_tries';
	public function get($id)
	{
		return self::_fetchRow('
			SELECT  id, login_try , UNIX_TIMESTAMP(date) as date
			FROM march8_win_tries
			WHERE id = ? 
		', array($id));
	}
		
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
			
		$sql = "
			SELECT  id, login_try , UNIX_TIMESTAMP(date) as date
			FROM march8_win_tries
			WHERE 1
		";
		
		$countSql = '
			SELECT COUNT(id)
			FROM march8_win_tries
			WHERE 1
		';
		
		
		if ( $filter['date_from'] && $filter['date_to'] ) {
				if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) ) {
					$where_sql .= self::quote( " AND date = DATE(FROM_UNIXTIME(?))" , $filter['date_from']);
					
				}
			}
		if ( $filter['date_from'] ) {
				$where_sql .= self::quote(" AND date >= DATE(FROM_UNIXTIME(?)) ", $filter['date_from']);
				
			}

		if ( $filter['date_to'] ) {
			$where_sql .= self::quote(" AND date <= DATE(FROM_UNIXTIME(?)) ", $filter['date_to']);
		}
		
		$sql .= $where_sql;
		$countSql .= $where_sql;
		$sql .= '
			GROUP BY id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return parent::_fetchAll($sql);
	}
	
	public function save($item)
	{
		self::getAdapter()->insert($this->_table, $item);	
	}
	
	public function update($item)
	{
		self::getAdapter()->update($this->_table, $item, self::getAdapter()->quoteInto('id = ?', $item['id']));
	}
		
	public function remove($id)
	{
		parent::remove(array(self::quote('id = ?', $id)));
	}
	
	public function getSelected($page, $per_page, $sort_field, $sort_dir, &$count_info)
	{
				
		$sql = "
			SELECT SQL_CALC_FOUND_ROWS u.username, mw.user_id, UNIX_TIMESTAMP(mw.login_date) as login_date, u.user_type
			FROM march8_winners mw
			INNER JOIN users u ON u.id = mw.user_id
			ORDER BY " . $sort_field . " " . $sort_dir . "
			LIMIT " . ($page - 1) * $per_page . ", " . $per_page;
		
		$data = parent::_fetchAll($sql);
		$count_info = $this->getAdapter()->fetchOne('SELECT FOUND_ROWS()');
		
		return $data;
	}
	
	public function getEscortbyUserId($user_id)
	{
		return self::getAdapter()->fetchRow('
			SELECT id, showname FROM escorts WHERE user_id = ?
		', $user_id);
	}

	public function getAgencybyUserId($user_id)
	{
		return self::getAdapter()->fetchRow('
			SELECT id, name FROM agencies WHERE user_id = ?
		', $user_id);
	}
	
	public function clearCounts()
	{
		self::getAdapter()->update('march8_login_counts', array('count' => 0));
		self::getAdapter()->query('TRUNCATE march8_logged_users');
		
	}
			
}