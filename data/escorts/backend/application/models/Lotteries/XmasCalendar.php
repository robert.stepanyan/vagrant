<?php

class Model_Lotteries_XmasCalendar extends Cubix_Model
{
	protected $_table = 'xmas_calendar';
	
	public function get($id)
	{
		return self::_fetchRow('
			SELECT xc.id ,xc.win_id, xc.win_code,xc.checked,xc.price_text, UNIX_TIMESTAMP(xc.date) as date  
			FROM xmas_calendar xc
			WHERE id = ? 
		', array($id));
	}
		
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
			
		$sql = "
			SELECT xc.id ,xc.win_id, xc.win_code, xc.checked,xc.price_text, UNIX_TIMESTAMP(xc.date) as date, e.showname
			FROM xmas_calendar xc
			LEFT JOIN escorts e ON e.id = xc.win_id
			WHERE 1
		";
		
		$countSql = '
			SELECT COUNT(id)
			FROM xmas_calendar
			WHERE 1
		';
		
		if ( $filter['win_id'] ) {
			$where_sql = self::quote('AND win_id = ?', $filter['win_id']);
		}
		
		if ( strlen($filter['win_code']) ) {
			$where_sql .= self::quote('AND win_code = ?', $filter['win_code']);
		}
		
		if ( $filter['date_from'] && $filter['date_to'] ) {
				if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) ) {
					$where_sql .= self::quote( " AND date = DATE(FROM_UNIXTIME(?))" , $filter['date_from']);
					
				}
			}
		if ( $filter['date_from'] ) {
				$where_sql .= self::quote(" AND date >= DATE(FROM_UNIXTIME(?)) ", $filter['date_from']);
				
			}

		if ( $filter['date_to'] ) {
			$where_sql .= self::quote(" AND date <= DATE(FROM_UNIXTIME(?)) ", $filter['date_to']);
		}
		
		$sql .= $where_sql;
		$countSql .= $where_sql;
		$sql .= '
			GROUP BY id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return parent::_fetchAll($sql);
	}
	
	public function save($item)
	{
		self::getAdapter()->insert($this->_table, $item);	
	}
	
	public function update($item)
	{
		self::getAdapter()->update($this->_table, $item, self::getAdapter()->quoteInto('id = ?', $item['id']));
	}
		
	public function remove($id)
	{
		parent::remove(array(self::quote('id = ?', $id)));
	}
	
	public function checkLimitPerDay($date, $id = null)
	{   
		$where_id = isset($id) ? " AND id <> ". $id : "";
		$sql = 'SELECT count(id) FROM xmas_calendar WHERE date = ? '. $where_id;
		return self::getAdapter()->fetchOne($sql, array($date));
	}
	
	public function getWinners()
	{
		return self::_fetchAll('
			SELECT xc.win_id, xc.win_code, xc.price_text, xc.date, bu.username  
			FROM xmas_calendar xc
			INNER JOIN escorts e ON e.id = xc.win_id
			INNER JOIN users u  ON u.id = e.user_id
			LEFT JOIN backend_users bu ON bu.id = u.sales_user_id
			WHERE xc.checked = 1 
			ORDER BY xc.date DESC
		');
	}
	
	public function check($id)
	{
		self::getAdapter()->query('UPDATE xmas_calendar SET checked = if( checked = 1 , 2 , 1) WHERE id = ?',array($id));
	}
	
	public function getSelected($page, $per_page, $sort_field, $sort_dir, &$count_info)
	{
		$start_date = 7;
		$end_date = 24;
		
		$day = date('j');
		if($day > $end_date){
			$count = $end_date - $start_date + 1;
		}
		else{
			$count = $day - $start_date;
		}
		
		$sql = "
			SELECT SQL_CALC_FOUND_ROWS u.username, xt.user_id, u.user_type, count(xt.user_id)
			FROM xmas_tries xt
			INNER JOIN users u ON u.id = xt.user_id
			GROUP BY xt.user_id
			HAVING count(xt.user_id) >= ". $count ."
			ORDER BY " . $sort_field . " " . $sort_dir . "
			LIMIT " . ($page - 1) * $per_page . ", " . $per_page;
		
		$data = parent::_fetchAll($sql);
		$count_info = $this->getAdapter()->fetchOne('SELECT FOUND_ROWS()');
		
		return $data;
	}
	
	public function getEscortbyUserId($user_id)
	{
		return self::getAdapter()->fetchRow('
			SELECT id, showname FROM escorts WHERE user_id = ?
		', $user_id);
	}

	public function getAgencybyUserId($user_id)
	{
		return self::getAdapter()->fetchRow('
			SELECT id, name FROM agencies WHERE user_id = ?
		', $user_id);
	}
			
}