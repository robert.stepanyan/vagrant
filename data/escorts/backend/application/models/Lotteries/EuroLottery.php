<?php

class Model_Lotteries_EuroLottery extends Cubix_Model
{	
	protected $_table = 'euro_lottery';
	
	const STATUS_REMOVED = -1;
	
	public function getAll($p, $pp, $sort_field, $sort_dir, $filter, &$count)
	{
		$where = '';

		$bu_user = Zend_Auth::getInstance()->getIdentity();

		if ( $bu_user->type == 'sales manager' ) {
			$where .= self::quote(' AND u.sales_user_id = ?', $bu_user->id);
		}
		
		if ( strlen($filter['e.showname']) ) {
			$where .= self::quote(' AND e.showname LIKE ?', $filter['e.showname'] . '%');
		}
		
		if ( strlen($filter['agency_name']) ) {
			$where .= self::quote(' AND ag.name LIKE ?', $filter['agency_name'] . '%');
		}
		
		if ( strlen($filter['e.id']) ) {
			$where .= self::quote(' AND e.id = ?', $filter['e.id']);
		}
		
		if ( strlen($filter['status']) ) {
			$where .= self::quote(' AND el.status = ?', $filter['status']);
		}
		
		$sql = '
			SELECT 
				e.id AS escort_id, 
				e.showname,
				u.username,
				ag.name AS agency_name, 
				el.id, 
				UNIX_TIMESTAMP(el.creation_date) AS creation_date,
				el.status,
				UNIX_TIMESTAMP(el.update_date) AS update_date,
				bu.username as bu_user,
				u.id AS user_id,
			(SELECT count(m.euro_vote_for) FROM members m WHERE m.euro_vote_for = e.id GROUP BY m.euro_vote_for) as votes
			FROM euro_lottery el
			INNER JOIN escorts e ON el.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id
			LEFT JOIN agencies ag ON ag.id = e.agency_id
			LEFT JOIN backend_users bu ON bu.id = el.admin_id
			WHERE el.status <> ?
		';
		
		$countSql = '
			SELECT COUNT(el.id) FROM euro_lottery el
			INNER JOIN escorts e ON el.escort_id = e.id 
			INNER JOIN users u ON u.id = e.user_id
			WHERE el.status <> ?
		';
		
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
		';
		
		$count = $this->getAdapter()->fetchOne($countSql, self::STATUS_REMOVED);
		
		return parent::_fetchAll($sql, self::STATUS_REMOVED);
	}
	
	public function get($id)
	{
		$sql = "
			SELECT 
				e.id AS escort_id, 
				e.showname,
				el.id,
				el.hash,
				el.ext,
				UNIX_TIMESTAMP(el.creation_date) AS creation_date,
				el.status,
				UNIX_TIMESTAMP(el.update_date) AS update_date,
				el.admin_id,
				bu.username as bu_user,
				u.id AS user_id
			FROM euro_lottery el
			INNER JOIN escorts e ON el.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id
			LEFT JOIN backend_users bu ON bu.id = el.admin_id
			WHERE el.id = ?
		";
		
		$el_photo = $this->getAdapter()->fetchRow($sql, array($id));
		$el_photo->photo = new Model_Verifications_Request_PhotoItem($el_photo );
		return $el_photo;
	}
	
	public function add($data)
	{
		$this->_db->insert($this->_table, $data);
		Cubix_SyncNotifier::notify($data['escort_id'], Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('action' => 'EURO LOTTERY Photo Added  '));
	}
	
	public function remove($id, $escort_id)
	{
		$this->_db->update($this->_table, array('status' => self::STATUS_REMOVED ), $this->_db->quoteInto('id = ?', $id));
		Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('action' => 'EURO LOTTERY Photo Removed  '));
	}
	
	public function update($req_id, $data)
	{
		$this->_db->update($this->_table, $data, $this->_db->quoteInto('id = ?', $req_id));
	}
	
	public function checkEscortExists($escort_id)
	{
		return $this->_db->fetchOne('SELECT TRUE FROM euro_lottery WHERE escort_id = ? AND status <> ?', array( $escort_id, self::STATUS_REMOVED ));
	}		
	
}
