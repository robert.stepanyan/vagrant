<?php

class Model_SearchLog extends Cubix_Model
{
    public function fetchUsedSpots($countryId, $cityId = null)
    {
        $sql = 'SELECT id, frozen_position FROM ixs_banners WHERE frozen_country_id = ' . $countryId;

        if (!empty($cityId)) {
            $sql .= ' or frozen_city_id = ' . $cityId;
        }

        $result = parent::_fetchAll($sql);
        return $result;
    }

    public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count = null)
    {
        $sql = '
			SELECT SQL_CALC_FOUND_ROWS id, `query`, mobile_attempts, desktop_attempts, last_search_date, redirect_url, escorts_count
			FROM search_log sl
            WHERE 1
		';

        if (!empty($filter['query'])) {
            $sql .= ' AND sl.query like \'%' . $filter['query'] . '%\'';
        }

        if (!is_null($filter['has_redirect_url'])) {
            $sql .= ' AND sl.redirect_url IS NOT NULL';
        }

           if (!is_null($filter['no_redirect_url'])) {
            $sql .= ' AND sl.redirect_url IS  NULL';
        }

        if (!empty($filter['mobile_bigger_than'])) {
            $sql .= ' AND sl.mobile_attempts > ' . $filter['mobile_bigger_than'] ;
        }

        if (!empty($filter['desktop_bigger_than'])) {
            $sql .= ' AND sl.desktop_attempts > ' . $filter['desktop_bigger_than'] ;
        }

        $sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

        $this->getAdapter()->query('SET NAMES `utf8`');
        $rows = parent::_fetchAll($sql);

        $count = $this->getAdapter()->fetchOne('SELECT FOUND_ROWS()');

        return $rows;
    }

    public function update($ids, $data)
    {
        $sql = "UPDATE search_log SET ";

        if (isset($data['redirect_url'])) {
            $sql .= 'redirect_url = \'' . $data['redirect_url'] . '\'';
        }

        $sql .= 'WHERE id in (' . implode(',', $ids) . ')';
        return $this->db()->query($sql);

    }
}
