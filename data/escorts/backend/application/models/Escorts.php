<?php

class Model_Escorts extends Cubix_Model
{
	const STATUS_NOT_VERIFIED = 1;
	const STATUS_VERIFIED = 2;
	const STATUS_VERIFY_RESET = 3;
	
	const NOT_VERIFIED = 1;
	const VERIFIED = 2;
	const VERIFY_RESET = 3;
	
	protected $_table = 'escorts';
	protected $_itemClass = 'Model_EscortItem';

	public function getTours($escort_id)
	{
		$sql = '
			SELECT t.country_id, t.city_id, t.phone, UNIX_TIMESTAMP(DATE(t.date_from)) AS date_from, UNIX_TIMESTAMP(DATE(t.date_to)) AS date_to, c.title_en AS country_title, cit.title_en AS city_title
			FROM tours t
			LEFT JOIN countries c ON c.id = t.country_id
			LEFT JOIN cities cit ON cit.id = t.city_id
			WHERE escort_id = ? AND ((DATE(date_from) <= CURDATE() AND DATE(date_to) > CURDATE()) OR DATE(date_from) > CURDATE())
		';
		
		return self::getAdapter()->fetchAll($sql, $escort_id);
	}

	public function getForAutocompleter($page, $per_page, $filter, $sort_field, $sort_dir, &$count = 0)
	{
		$sql = array(
			'fields' => array(
				'u.id AS user_id', 'u.username', 'e.showname',
				'eph.hash AS photo_hash', 'eph.ext AS photo_ext', 'eph.status AS photo_status',
				'ag.name AS agency_name', 'ag.id AS agency_id',	'u.balance'
			),
			'joins' => array(
				'LEFT JOIN agencies ag ON ag.id = e.agency_id',
				'LEFT JOIN escort_profiles_v2 ep ON ep.escort_id = e.id',
				'LEFT JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1'
			),
			'where' => $filter,
			'order' => array($sort_field, $sort_dir),
			'page' => array($page, $per_page)
		);

		$data = $this->_getAll($sql);

		$count = $data['count'];
		return $data['result'];
	}

	public function getForAgencies($page, $per_page, $filter, $sort_field, $sort_dir, &$count = 0)
	{
		$sql = array(
			'fields' => array(
				'u.username', 'e.showname', 'ep.contact_phone', 'ep.contact_phone_parsed', 'ep.contact_email',
				'ag.name AS agency_name', 'ag.id AS agency_id'
			),
			'joins' => array(
				'LEFT JOIN agencies ag ON ag.id = e.agency_id',
				'LEFT JOIN escort_profiles_v2 ep ON ep.escort_id = e.id'
			),
			'where' => $filter,
			'order' => array($sort_field, $sort_dir),
			'page' => array($page, $per_page)
		);

		$data = $this->_getAll($sql);

		$count = $data['count'];
		return $data['result'];
	}

	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count = 0)
	{
		$sql = array(
			'fields' => array(
				'u.username','e.id AS escort_id', 'e.showname', 'UNIX_TIMESTAMP(e.date_last_modified) AS date_last_modified', 
				'e.status', 'UNIX_TIMESTAMP(e.date_registered) AS date_registered',
				'ag.name AS agency_name', 'ag.id AS agency_id', 'u.id AS user_id'
			),
			'joins' => array(
				'LEFT JOIN agencies ag ON ag.id = e.agency_id'
			),
			'where' => $filter,
			'order' => array($sort_field, $sort_dir),
			'page' => array($page, $per_page)
		);
		
		$data = $this->_getAll($sql);
		
		$count = $data['count'];
		return $data['result'];
	}

	private function _getAll($sql)
	{
		$_sql = array(
			'tables' => array('users u'),
			'fields' => array(
				'e.id', 'bu.username AS sales_person', 'a.title AS signup_from', 'u.application_id', '_c.iso AS app_iso'
			),
			'joins' => array(
				'INNER JOIN escorts e ON e.user_id = u.id',
				'INNER JOIN applications a ON a.id = u.application_id',
				'LEFT JOIN backend_users bu ON bu.id = u.sales_user_id',
				'LEFT JOIN countries _c ON a.country_id = _c.id'
			),
			'where' => array(
				
			),
			'group' => 'e.id'
		);

		/* --> START Custom Filter Logic */
		$filter = $sql['where'];
		// Get Only Tour Girls
		if ( isset($filter['only_city_tour']) && $filter['only_city_tour'] ) {
			$sql['joins'][] = 'INNER JOIN tours t ON t.escort_id = e.id';
			$filter[] = 'DATE(t.date_from) <= DATE(NOW())';
			$filter[] = 'DATE(t.date_to) >= DATE(NOW())';
		}

		unset($filter['only_city_tour']);

		// Get Only Escorts With At Least One Photo
		if ( isset($filter['with_photo']) && $filter['with_photo'] ) {
			$sql['joins'][] = 'INNER JOIN escort_photos eph ON eph.escort_id = e.id';
		}

		// Filter by Date
		if ( isset($filter['filter_by']) && $filter['filter_by'] ) {
			if ( $filter['filter_by'] == 'registration_date' ) {
				$date_field = 'e.date_registered';
			}
			elseif ( $filter['filter_by'] == 'last_modified_date' ) {
				$date_field = 'e.date_last_modified';
			}

			if ( $filter['date_from'] && $filter['date_to'] ) {
				if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) ) {
					$filter["DATE($date_field) = DATE(FROM_UNIXTIME(?))"] = $filter['date_from'];
				}
			}

			if ( $filter['date_from'] ) {
				$filter["DATE({$date_field}) >= DATE(FROM_UNIXTIME(?))"] = $filter['date_from'];
			}

			if ( $filter['date_to'] ) {
				$filter["DATE({$date_field}) <= DATE(FROM_UNIXTIME(?))"] = $filter['date_to'];
			}
		}

		unset($filter['filter_by']);
		unset($filter['date_from']);
		unset($filter['date_to']);

		// Filter by Status
		if ( $filter['e.status'] != Model_Escorts::ESCORT_STATUS_DELETED ) {
			if ( $filter['excl_status'] ) {
				$filter = array_merge($filter, $this->_whereStatus2('e.status', null, $filter['excl_status']));
			}
		}

		unset($filter['excl_status']);

		if ( $filter['e.status'] ) {
			$filter = array_merge($filter, $this->_whereStatus2('e.status', $filter['e.status']));
		}

		unset($filter['e.status']);
		if ($filter['u.application_id']){
            $filter['u.application_id = ?'] = $filter['u.application_id'];
        }
        unset($filter['u.application_id']);

		// Like Statements
		foreach ( $filter as $f => $v ) {
			if ( is_int($f) || strpos($f, '?') !== false ) continue;

			unset($filter[$f]);
			if ( ! $v ) continue;
			$filter[$f . ' LIKE ?'] = $v . '%';
		}

		$sql['where'] = $filter;
		/* <-- END Custom Filter Logic */

		$sql = array_merge_recursive($_sql, $sql);
		$dataSql = Cubix_Model::getSql($sql);
		//var_dump($dataSql); die;

		$sql['group'] = false;
		$sql['fields'] = array('COUNT(DISTINCT(e.id))');
		$sql['page'] = false;
		$countSql = Cubix_Model::getSql($sql);

		return array(
			'result' => parent::_fetchAll($dataSql),
			'count' => parent::getAdapter()->fetchOne($countSql)
		);
	}
	
	public function removeVacation($escort_id)
	{
		try{
			$sql = 'UPDATE escorts SET vac_date_from = NULL, vac_date_to = NULL WHERE id = ?';
			$this->getAdapter()->query($sql, $escort_id);
			Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED);
		}
		catch ( Exception $e ) {
			throw $e;
		}

	}

	/*For
	 EF escort skype data
	*/
	public function getSkypeData($page, $per_page, $filter, $sort_field, $sort_dir, &$count){
	    $sql = 'SELECT e.id, e.showname,es.escort_skype_username, es.paxum_email, es.status, es.price_30, es.price_60 FROM escort_skype es 
                INNER JOIN escorts e ON e.id = es.escort_id
                WHERE es.escort_id IS NOT NULL ';

        $countSql = '
			SELECT COUNT(DISTINCT(e.id)) FROM escort_skype es 
			INNER JOIN escorts e ON e.id = es.escort_id
			 WHERE es.escort_id IS NOT NULL ';

        $where = '';
        if ( $filter['e.showname'] ) {
            $where .= self::quote('AND e.showname LIKE ?', $filter['e.showname'] . '%');
        }
        if ( $filter['e.id'] ) {
            $where .= self::quote('AND e.id = ?', $filter['e.id']);
        }

        $sql .= $where;
        $countSql .= $where;

        $sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
        //echo $sql;die;
        $count = $this->getAdapter()->fetchOne($countSql);

        return parent::_fetchAll($sql);

    }


    /*
         EF escort skype history data
    */

    public function getSkypeHistoryData($escort_id){

        $where = self::quote('escort_id = ?', $escort_id);

        $sql = 'SELECT * FROM escort_skype_history WHERE '.$where;

        return parent::_fetchAll($sql);

    }
    public function getEscortSkypeData($escort_id){

        $where = self::quote('escort_id = ?', $escort_id);

        $sql = 'SELECT * FROM escort_skype WHERE '.$where;

        return parent::_fetchRow($sql);

    }

    /*
     * update escort skype data
     */

    public function updateEscortSkype($data){
        parent::getAdapter()->update('escort_skype', array('escort_skype_username' => $data['escort_skype_username'], 'paxum_email' => $data['paxum_email'], 'status' => $data['status'], 'price_30' => $data['price_30'],'price_60' => $data['price_60'] ), parent::quote('escort_id = ?', $data['escort_id']));
        parent::getAdapter()->update('escorts', array('skype_call_status' => $data['status']), parent::quote('id = ?', $data['escort_id']));
        $data['created_at'] = date("Y-m-d H:i:s");
        $this->_db->insert('escort_skype_history', $data);


    }



    public function getEscortShowname($escort_id){
        $escort = parent::_fetchRow('
			SELECT e.id,e.showname FROM escorts e
			WHERE e.id = ?
		', $escort_id);
        return $escort;
    }

	
	public function getVacations($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{		
		$sql = '
			SELECT e.id, e.showname, UNIX_TIMESTAMP(e.vac_date_from) AS vac_date_from, UNIX_TIMESTAMP(e.vac_date_to) AS vac_date_to FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			WHERE e.vac_date_from IS NOT NULL AND ((DATE(e.vac_date_from) <= CURDATE() AND DATE(e.vac_date_to) > CURDATE()) OR DATE(e.vac_date_from) > CURDATE())
		';
		
		$countSql = '
			SELECT COUNT(DISTINCT(e.id)) FROM escorts e 
			INNER JOIN users u ON u.id = e.user_id
			WHERE e.vac_date_from IS NOT NULL AND ((DATE(e.vac_date_from) <= CURDATE() AND DATE(e.vac_date_to) > CURDATE()) OR DATE(e.vac_date_from) > CURDATE())
		';
		
		$where = '';
		
		if ( $filter['e.showname'] ) {
			$where .= self::quote('AND e.showname LIKE ?', $filter['e.showname'] . '%');
		}
		
		if ( $filter['e.id'] ) {
			$where .= self::quote('AND e.id = ?', $filter['e.id']);
		}
		
		if ( $filter['u.application_id'] ) {
			$where .= self::quote('AND u.application_id = ?', $filter['u.application_id']);
		}
		
		$sql .= $where;
		$countSql .= $where;
		
		/*$sql .= '
			GROUP BY e.id
		';*/
		
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		//echo $sql;
		$count = $this->getAdapter()->fetchOne($countSql);
		
		return parent::_fetchAll($sql);
	}
	
	public function get($id)
	{
		$fields = " ";
		if ( in_array(Cubix_Application::getId(), array(APP_EF, APP_6A)) ) {
			$fields = " e.profile_type, ";
		}
		// edir-2303
		if (Cubix_Application::getId() == APP_ED){
		    $fields = " e.type, ";
        }
		$escort = parent::_fetchRow('
			SELECT e.id, e.user_id, e.agency_id, e.showname, e.gender, e.country_id, e.city_id,
				u.email, u.username, e.status, u.sales_user_id, u.application_id,
				eph.hash AS photo_hash,
				eph.ext AS photo_ext,
				eph.status AS photo_status,
				u.id AS user_id,
				' . $fields . '
				u.balance, e.pseudo_escort, a.name AS agency_name, u.user_type,
				be.username AS sales_name
			FROM escorts e
			LEFT JOIN users u ON u.id = e.user_id
			LEFT JOIN backend_users be ON be.id = u.sales_user_id
			LEFT JOIN agencies a ON a.id = e.agency_id
			LEFT JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1
			WHERE e.id = ?
		', $id);
		
		//$escort->sex_availability = explode(',', $escort->sex_availability);
		//$escort->work_times = $escort->getWorkTimes();
		
		return $escort;
	}

	public function getByUserId($user_id)
	{
		$escort = parent::_fetchRow('
			SELECT e.id, e.user_id, e.agency_id, e.showname, e.gender, e.country_id, e.city_id,
				u.email, u.username, e.status, u.sales_user_id, u.application_id,
				u.application_id,
				eph.hash AS photo_hash,
				eph.ext AS photo_ext,
				eph.status AS photo_status,
				u.balance, a.name AS agency_name,
				be.username AS sales_name
			FROM escorts e
			LEFT JOIN users u ON u.id = e.user_id
			LEFT JOIN backend_users be ON be.id = u.sales_user_id
			LEFT JOIN agencies a ON a.id = e.agency_id
			LEFT JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1
			WHERE u.id = ?
		', $user_id);

		//$escort->sex_availability = explode(',', $escort->sex_availability);
		//$escort->work_times = $escort->getWorkTimes();

		return $escort;
	}
	
	public function getUserId($id)
	{
		return parent::getAdapter()->fetchOne('SELECT user_id FROM escorts WHERE id = ?', $id);
	}
	
	public function getIdByShowname($showname)
	{
		$id = parent::_fetchRow('
			SELECT e.id
			FROM escorts e
			WHERE e.showname = ?
		', $showname);
		
		return $id;
	}

	public function existsByUsername($username)
	{
		return (bool) self::getAdapter()->fetchOne('SELECT TRUE FROM users WHERE username = ?', $username);
	}
	
	public function existsByShowname($showname)
	{
		return (bool) self::getAdapter()->fetchOne('SELECT TRUE FROM escorts WHERE showname = ?', $showname);
	}

	
	public function setStatus($escort_id, $status)
	{
		if ( $status == 'VERIFIED' )
		{
			$this->getAdapter()->query("UPDATE escorts SET verified_status = ? WHERE id = ?", array(Model_Escorts::VERIFIED, $escort_id));
			$this->getAdapter()->query("UPDATE escort_photos SET status = ? WHERE escort_id = ?", array(Model_Escort_Photos::VERIFIED, $escort_id));
			Model_Activity::getInstance()->log('verify_escort', array('escort id' => $escort_id));
		}
		
		if ( $status == 'REJECTED' )
		{
			$this->getAdapter()->query("UPDATE escorts SET verified_status = ? WHERE id = ?", array(Model_Escorts::NOT_VERIFIED, $escort_id));
			//$this->getAdapter()->query("UPDATE escort_photos SET status = ? WHERE escort_id = ? AND status = '" . Model_Escort_Photos::NOT_VERIFIED . "'", array(Model_Escort_Photos::NORMAL, $escort_id));
			Model_Activity::getInstance()->log('reject_verify_escort', array('escort id' => $escort_id));
		}
	}
	
	public function getList($value)
	{
		$application_id = Zend_Controller_Front::getInstance()->getRequest()->application_id;

		$sql = '
			SELECT e.id, u.username, e.showname, UNIX_TIMESTAMP(e.date_last_modified) AS date_last_modified, u.status, bu.username AS sales_person, a.title AS signup_from, u.application_id, UNIX_TIMESTAMP(u.date_registered) AS date_registered
			FROM users u
			INNER JOIN escorts e ON e.user_id = u.id
			LEFT JOIN cities c ON c.id = e.city_id
			LEFT JOIN applications a ON a.id = u.application_id
			LEFT JOIN backend_users bu ON bu.id = u.sales_user_id
			INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
			WHERE e.showname LIKE ? AND u.application_id = ?
		';
		
		$sql .= '
			GROUP BY e.id
		';
		
		$sql .= '
			ORDER BY e.showname ASC
			LIMIT 10
		';
		
		return parent::_fetchAll($sql, array($value . '%', $application_id));
	}

	public function getIdByPhone($phone_parsed)
	{
		$sql = '
			SELECT e.id FROM escorts e
			INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
			WHERE ep.phone_number = ?
		';
		
		return parent::_fetchAll($sql, $phone_parsed);
	}

	public function _whereStatus2($field, $status, $excl_status = array())
	{
		if ( ! is_null($status) ) { if ( ! is_array($status) ) $status = array($status); }
		else { $status = array(); }
		if ( ! is_array($excl_status) ) $excl_status = array($excl_status);

		$ret = array();

		foreach ( $status as $st ) {
			$ret[] = "({$field} & {$st})";
		}

		foreach ( $excl_status as $st ) {
			$ret[] = " NOT ({$field} & {$st})";
		}

		return $ret;
	}

	public function _whereStatus($field, $status, $excl_status = array())
	{
		if ( ! is_null($status) ) if ( ! is_array($status) ) $status = array($status);
		if ( ! is_array($excl_status) ) $excl_status = array($excl_status);
		
		
		$ret = array();

		if ( is_array($status) ) {
			foreach ( $status as $st ) {
				$ret[] = "({$field} & {$st})";
			}

			$result = implode(' AND ', $ret);
		}

		if ( count($status) && count($excl_status) ) $result .= ' AND ';
		
		$ret = array();
		foreach ( $excl_status as $st ) {
			$ret[] = " NOT ({$field} & {$st})";
		}
		
		$result .= implode(' AND ', $ret);
		
		return $result;
	}
	
	const ESCORT_STATUS_NO_PROFILE 			= 1;		// 000000001
	const ESCORT_STATUS_NO_ENOUGH_PHOTOS 	= 2;		// 000000010
	const ESCORT_STATUS_NOT_APPROVED 		= 4;		// 000000100
	const ESCORT_STATUS_OWNER_DISABLED 		= 8;		// 000001000
	const ESCORT_STATUS_ADMIN_DISABLED 		= 16;		// 000010000
	const ESCORT_STATUS_ACTIVE 				= 32;		// 000100000
	const ESCORT_STATUS_IS_NEW				= 64;		// 001000000
	const ESCORT_STATUS_PROFILE_CHANGED		= 128;		// 010000000
	const ESCORT_STATUS_DELETED				= 256;		// 100000000
    const ESCORT_STATUS_INACTIVE            = 16384;	// 0100000000000000
    const ESCORT_STATUS_AWAITING_PAYMENT 	= 131072;	// 100000000000000000


    public function setStatusBit($escort_id, $status)
	{		
		if ( ! is_array($status) ) $status = array($status);
		
		$old_status = (int) $this->_db->fetchOne('SELECT status FROM escorts WHERE id = ?', $escort_id);
		
		foreach ( $status as $bit ) {
			$old_status = $old_status | $bit;
		}
		
		$this->_db->update('escorts', array('status' => $old_status), $this->_db->quoteInto('id = ?', $escort_id));
	}
	
	public function removeStatusBit($escort_id, $status)
	{		
		if ( ! is_array($status) ) $status = array($status);
		
		$old_status = (int) $this->_db->fetchOne('SELECT status FROM escorts WHERE id = ?', $escort_id);
		foreach ( $status as $bit ) {
			if ( ! ($old_status & $bit) ) continue;
			$old_status = $old_status ^ $bit;
		}
		
		$this->_db->update('escorts', array('status' => $old_status), $this->_db->quoteInto('id = ?', $escort_id));
	}
	
	public function hasStatusBit($escort_id, $status)
	{
		if ( ! is_array($status) ) $status = array($status);
		
		$old_status = $this->_db->fetchOne('SELECT status FROM escorts WHERE id = ?', $escort_id);
		
		$result = true;
		foreach ( $status as $bit ) {
			$result = ($old_status & $bit) && $result;
		}
		
		return $result;
	}

	public function delete($escort_id)
	{
		file_put_contents('/var/log/and6-deprecated-functions.log', 'method Model_Escorts::delete() is deprecated', FILE_APPEND);
		return;
		$escort = $this->get($escort_id);
		
		$m_transfers = new Model_Billing_Transfers();
		$m_packages = new Model_Billing_Packages();
		$m_orders = new Model_Billing_Orders();

		$transfers = $m_transfers->getAllByUsertId($escort->user_id);

		self::getAdapter()->beginTransaction();

		try {
			$m_packages->cancelDefaultPackages($escort->id, 'Escort #' . $escort->id . ' profile has been deleted');

			if ( $escort->user_type == Model_Users::USER_TYPE_ESCORT ) {
				foreach ( $transfers as $k => $transfer ) {
					$transfers[$k] = $m_transfers->getDetails($transfer->id);
				}

				foreach( $transfers as $transfer ) {
					$transfer->reject('Escort #' . $escort->id . ' has been deleted', true);
				}

				$escort->cancelPackages('Escort #' . $escort->id . ' has been deleted');
				
				$m_orders->closeByUserId($escort->user_id);
			}
			else if ( $escort->user_type == Model_Users::USER_TYPE_AGENCY ) {
				$escort->cancelPackages('Agency Escort #' . $escort->id . ' has been deleted');
			}

			$this->removeStatusBit($escort->id, self::ESCORT_STATUS_ACTIVE);
			$this->setStatusBit($escort->id, self::ESCORT_STATUS_DELETED);

			if ( $escort->user_type == Model_Users::USER_TYPE_ESCORT ) {
				self::getAdapter()->update('users', array('status' => USER_STATUS_DELETED), self::quote('id = ?', $escort->user_id));
			}

			// Model_Activity::getInstance()->log('delete_escort', array('escort id' => $escort->id));

			self::getAdapter()->commit();
		}
		catch ( Exception $e ) {
			self::getAdapter()->rollBack();

			throw $e;
		}
	}

	public function getReviewsCount($escort_id)
	{
		return self::getAdapter()->fetchOne('SELECT reviews FROM escorts WHERE id = ?', $escort_id);
	}

	/* for Newsletter Api */

	public function getAllForNewsletterApi()
	{
		$sql = "SELECT e.showname AS name, u.email AS email FROM escorts e LEFT JOIN users u ON u.id = e.user_id WHERE u.user_type = 'escort'";
		return self::getAdapter()->fetchAll($sql);
	}

	public function getLastIP($id)
	{
		return self::getAdapter()->fetchOne('SELECT u.ip FROM users_last_login u INNER JOIN escorts e ON e.user_id = u.user_id WHERE e.id = ? ORDER BY login_date DESC LIMIT 1', $id);
	}

	public function getByAgencyId($agency_id)
	{
		return self::getAdapter()->fetchAll('SELECT e.id FROM escorts e INNER JOIN agencies a ON a.id = e.agency_id WHERE a.id = ?', $agency_id);
	}
	
	public function getAgencyId($escort_id)
	{
		return self::getAdapter()->fetchOne('SELECT e.agency_id FROM escorts e WHERE e.id = ?', $escort_id);
	}
	
	public function getsmallestRate($escort_id){
		return self::getAdapter()->fetchRow('SELECT * FROM escort_rates WHERE escort_id = ? AND time <> 0 ORDER BY price LIMIT 1', $escort_id);
	}
	
	public static function restore()
	{
		$original = Zend_Db::factory('mysqli', array(
			'host' => 'localhost',
			'username' => 'root',
			'password' => '',
			'dbname' => 'and6'
		));

		$backup = Zend_Db::factory('mysqli', array(
			'host' => 'localhost',
			'username' => 'root',
			'password' => '',
			'dbname' => 'and6_backup'
		));
		
		set_time_limit(0);
		ini_set('memory_limit', '2058M');
		
		$orders = $original->fetchAll('
			SELECT o.id, o.status, o.order_date, tor.transfer_id , t.date_transfered
			FROM orders o 
			INNER JOIN transfer_orders tor ON tor.order_id = o.id 
			INNER JOIN transfers t ON t.id = tor.transfer_id
			WHERE t.date_transfered >= "2012-02-11"
			GROUP BY tor.transfer_id
			
		');
		
		foreach($orders as $order) {
			$original->update('transfers', array('date_transfered' => $order['order_date']), $original->quoteInto('id = ?', $order['transfer_id']));
		}
		
		
	}
	
	public function logOpener($escort_id)
	{
		
		$user = Zend_Auth::getInstance()->getIdentity();
		$ip = Cubix_Geoip::getIP();
		
		$data = array(
			'escort_id' => $escort_id,
			'backend_user_id' => $user->id,
			'ip' => $ip
			
		);
		$this->_db->insert('profile_opener_log', $data);
	}		
	
}
