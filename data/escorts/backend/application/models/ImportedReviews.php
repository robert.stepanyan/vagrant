<?php
class Model_ImportedReviews extends Cubix_Model
{
	protected $_table = 'reviews_imported';

	public function getById($id)
	{
	    $select = '';
        if(Cubix_Application::getId() == APP_ED){
            $select .=',quality,location_review';
         }
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				ir.escort_id, ir.name, ir.profileId, ir.profileType, ir.profilePhoneNumber, ir.profilePhoneNumber2, ir.reviewId, ir.reviewUsername, UNIX_TIMESTAMP(ir.reviewdate) as review_date,
				ir.reviewRanking, ir.reviewText, ir.profileUrl, ir.reviewUrl, ir.profileWebsite1, ir.profileWebsite2
			FROM reviews_imported ir WHERE			
			ir.escort_id = ?
			ORDER BY ir.reviewdate DESC 
		';

		return parent::_fetchRow($sql, array($id));
	}
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$fields = '';
		$join = '';
		$where = '';

		
		if ( strlen($filter['showname']) ) {
			$where .= self::quote(' AND e.showname LIKE ? ', $filter['showname'] . '%');
		}
		
		if ( strlen($filter['escort_id']) ) {
			$where .= self::quote(' AND e.id = ? ', $filter['escort_id']);
		}

		if ( !is_null($filter['status']) && $filter['status'] != '' ) {
			$where .= self::quote(' AND ir.`status` = ? ', intval($filter['status']));
		}

		if ( isset($filter['reviewText']) && $filter['reviewText']  ) {
			$where .= ' AND ir.reviewText LIKE "%'.$filter['reviewText'].'%"';
		}

		$sql = '
		SELECT SQL_CALC_FOUND_ROWS
				ir.escort_id, ir.name as showname, ir.profileId, ir.profileType, ir.profilePhoneNumber, ir.profilePhoneNumber2, ir.reviewId, ir.reviewUsername, UNIX_TIMESTAMP(ir.reviewdate) as review_date,
				ir.reviewRanking, ir.reviewText, ir.profileUrl, ir.reviewUrl, ir.profileWebsite1, ir.profileWebsite2, ir.status
			FROM reviews_imported ir 
			LEFT JOIN escorts e ON e.id = ir.escort_id WHERE 1
		';

		$countSql = '
			SELECT COUNT(DISTINCT(ir.reviewId))
			FROM reviews_imported ir 
			LEFT JOIN escorts e ON e.id = ir.escort_id WHERE 1
		';
						
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			GROUP BY ir.reviewId
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return parent::_fetchAll($sql);
	}

	public function toggle($id)
    {
        parent::getAdapter()->update($this->_table, array(
            'status' => new Zend_Db_Expr('NOT status')
        ), parent::quote('reviewId = ?', $id));
    }
}
