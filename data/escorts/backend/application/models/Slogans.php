<?php
class Model_Slogans extends Cubix_Model
{

	public function get($id)
	{
		$sql = 'SELECT id, escort_id, text, status FROM escort_slogans WHERE id = ?';

		return parent::_fetchRow($sql, array($id));
	}
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = ' 
			SELECT SQL_CALC_FOUND_ROWS es.id, e.id AS escort_id, e.showname, es.text, UNIX_TIMESTAMP(es.date) AS date, es.status
			FROM escort_slogans es
			INNER JOIN escorts e ON e.id = es.escort_id
			INNER JOIN users u ON u.id = e.user_id
			WHERE 1 
		';
		
		$where = '';

		if ( strlen($filter['application_id']) ) {
			$where .= self::quote('AND u.application_id = ?', $filter['application_id']);
		}
				
		if ( strlen($filter['showname']) ) {
			if ($filter['mode'] == 'fixed')
				$where .= self::quote(' AND e.showname = ?', $filter['showname']);
			else
				$where .= self::quote(' AND e.showname LIKE ? ', $filter['showname'] . '%');
		}
				
		if ( $filter['escort_id'] ) {
			$where .= self::quote('AND e.id = ?', $filter['escort_id']);
		}
		
		if ( strlen($filter['status']) > 0 ) {
			$where .= self::quote('AND es.status = ?', $filter['status']);
		}

		if ( strlen($filter['country']) > 0 ) {
			if(is_numeric($filter['country']))
				$where .= self::quote('AND e.country_id in (?)', $filter['country']);
			else{
				$where .= self::quote('AND e.country_id in (?)', explode(',', $filter['country']));
			}
		}
				
		$sql .= $where;
		
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$data = parent::_fetchAll($sql);
		$count = $this->getAdapter()->fetchOne('SELECT FOUND_ROWS()');
		return $data;
	}	
	
	public function save($data)
	{
		$s = $this->get($data['id']);
		
		if (strlen($data['text']) == 0)
		{
			parent::getAdapter()->delete('escort_slogans', parent::quote('id = ?', $data['id']));
			$log = '*** removed ***';
		}
		else
		{
			parent::getAdapter()->update('escort_slogans', array('text' => $data['text']), parent::quote('id = ?', $data['id']));
			$log = '*** updated *** - ' . $data['text'];
		}
		
		Cubix_SyncNotifier::notify($s->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array(
			'slogan' => $log
		));
	}
		
	public function remove($id)
	{
		$s = $this->get($data['id']);
		
		parent::getAdapter()->delete('escort_slogans', parent::quote('id = ?', $id));
		
		Cubix_SyncNotifier::notify($s->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array(
			'slogan' => '*** removed ***'
		));
	}
	
	public function toggle($id)
	{
		$s = $this->get($id);
				
		$field = 'status';
		
		parent::getAdapter()->update('escort_slogans', array(
			$field => new Zend_Db_Expr('NOT ' . $field)
		), parent::quote('id = ?', $id));
		
		Cubix_SyncNotifier::notify($s->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array(
			'slogan' => '*** status changed from ' . intval($s->status) . ' to ' . intval(!$s->status) . ' ***'
		));
	}
	
	public function setStatus($id, $status)
	{
		$s = $this->get($id);
		parent::getAdapter()->update('escort_slogans', array('status' => $status), parent::getAdapter()->quoteInto('id = ?', $id));
		Cubix_SyncNotifier::notify($s->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array(
			'slogan' => '*** status changed from ' . intval($status) . ' to ' . intval(!$status) . ' ***'
		));
	}
}
