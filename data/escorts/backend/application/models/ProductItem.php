<?php

class Model_ProductItem extends Cubix_Model_Item
{
	public function getPrices($app_id = null)
	{
		$filter = array($this->getId());
		$where = '';
		if ( $app_id )
		{
			$filter = array_merge($filter, array($app_id));
			$where = ' AND pp.application_id = ? ';
		}
		
		return $this->_adapter->query('
			SELECT
				pp.*
			FROM product_prices pp			
			WHERE pp.product_id = ? ' . $where
		, $filter)->fetchAll();
	}
}
