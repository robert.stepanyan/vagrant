<?php
class Model_BOUsersSessions extends Cubix_Model
{
	protected $_table = 'backend_users_sessions';

    protected $_messages = array(
        "escort_added"            => "Escort ? added",
        "main_info_update"      => "Main info of escort ? was updated",
        "contact_info_update"   => "Contact info of escort ? was updated",
        "servicest_update"   => "Services of escort ? were updated",
        "vacations_update"   => "Vacations of escort ? were updated",
        "location_update"   => "Locations of escort ? were updated",
        
        "picture_added"   => "Picture ? for ? gallery of escort ? added",
        "picture_deleted"   => "Picture ? for ? gallery of escort ? was deleted",
        "main_picture_change"   => "Main pic of escort ? was changed to ?",
        
        "order_add"             => "Order ? was added",
        "order_status_change"   => "Status of order ? was changed to: ?",
        "order_status_modify"   => "Status of order ? was modified to : ?",

        "agency_update"         => "Agency ? was updated",
    );


    public static function setStart($sales_id){
        $sales_id = (int)$sales_id;
        $sql = "INSERT INTO backend_users_sessions(start_time,last_refresh_time,sales_id) VALUES(NOW(),NOW(),$sales_id)";
        self::getAdapter()->query($sql);
        if (in_array(Cubix_Application::getId(),array(APP_ED,APP_EG_CO_UK)))
            self::getAdapter()->query('UPDATE backend_users SET is_online = 1');
    }

    public static function refresh($sales_id){
        $sales_id = (int)$sales_id;

        $idSql = "SELECT id  FROM backend_users_sessions WHERE sales_id = $sales_id AND start_time = (SELECT MAX(start_time) FROM backend_users_sessions WHERE sales_id = $sales_id)";
        $row =  self::getAdapter()->fetchRow($idSql);
        if($row){
            $sql = "UPDATE backend_users_sessions SET last_refresh_time = NOW() WHERE id = $row->id";
            self::getAdapter()->query($sql);
        }
    }

    public function getTotal($filter){
        $sql = '
			SELECT bus.id,MIN(UNIX_TIMESTAMP(bus.start_time)) as start_time, MAX(UNIX_TIMESTAMP(bus.last_refresh_time)) as last_refresh_time, bu.id as sales_id, bu.username as bu_username,
                SUM(UNIX_TIMESTAMP(bus.last_refresh_time) - UNIX_TIMESTAMP(bus.start_time)) as total
FROM
                backend_users_sessions bus
				INNER JOIN backend_users bu ON bu.id = bus.sales_id
			WHERE 1
		';


        $date_field = 'bus.start_time';

        if ( $filter['date_from'] && $filter['date_to'] ) {
            if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) ) {
                $filter["DATE($date_field) = DATE(FROM_UNIXTIME(?))"] = $filter['date_from'];
            }
        }

        if ( $filter['date_from'] ) {
            $filter["DATE({$date_field}) >= DATE(FROM_UNIXTIME(?))"] = $filter['date_from'];
        }

        if ( $filter['date_to'] ) {
            $filter["DATE({$date_field}) <= DATE(FROM_UNIXTIME(?))"] = $filter['date_to'];
        }

        unset($filter['date_from']);
		unset($filter['date_to']);



        $where = Cubix_Model::getWhereClause($filter);
        if ( $where ){
            $sql .= ' AND '.$where;
        }


		$sql .= '
			GROUP BY bus.sales_id
		';

		return parent::_fetchAll($sql);

        
    }

	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT bus.id,UNIX_TIMESTAMP(bus.start_time) as start_time, UNIX_TIMESTAMP(bus.last_refresh_time) as last_refresh_time,bu.id as sales_id,bu.username as bu_username,
                (UNIX_TIMESTAMP(bus.last_refresh_time) - UNIX_TIMESTAMP(bus.start_time)) as total 
FROM
                backend_users_sessions bus
				INNER JOIN backend_users bu ON bu.id = bus.sales_id
			WHERE 1
		';

		$countSql = '
			SELECT COUNT(bus.id) FROM
                backend_users_sessions bus
				INNER JOIN backend_users bu ON bu.id = bus.sales_id
			WHERE 1
		';

        $date_field = 'bus.start_time';

        if ( $filter['date_from'] && $filter['date_to'] ) {
            if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) ) {
                $filter["DATE($date_field) = DATE(FROM_UNIXTIME(?))"] = $filter['date_from'];
            }
        }

        if ( $filter['date_from'] ) {
            $filter["DATE({$date_field}) >= DATE(FROM_UNIXTIME(?))"] = $filter['date_from'];
        }

        if ( $filter['date_to'] ) {
            $filter["DATE({$date_field}) <= DATE(FROM_UNIXTIME(?))"] = $filter['date_to'];
        }

        unset($filter['date_from']);
		unset($filter['date_to']);

       

        $where = Cubix_Model::getWhereClause($filter);
        if ( $where ){
            $sql .= ' AND '.$where;
            $countSql .= ' AND '.$where;
        }
		

		$sql .= '
			/*GROUP BY st.id*/
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$count = intval($this->getAdapter()->fetchOne($countSql));

		return parent::_fetchAll($sql);
	}

    public function get($id){
        $sql = '
			SELECT * FROM
                backend_users_sessions 	WHERE id = '.$id;

        return  parent::_fetchRow($sql);

    }
}
