<?php

class Model_Chat extends Cubix_Model
{
	public function getUsersList($user)
	{
		$sql = "
			SELECT u.id, u.user_type, e.showname, e.id AS escort_id, a.name AS agency_name, u.username 
			FROM users u
			LEFT JOIN escorts e ON e.user_id = u.id
			LEFT JOIN agencies a ON a.user_id = u.id
			WHERE IF (u.user_type = 'escort', e.showname LIKE '" . $user . "%', IF (u.user_type = 'agency', a.name LIKE '" . $user . "%', u.username LIKE '" . $user . "%'))
			GROUP BY u.id
			ORDER BY COALESCE(e.showname, a.name, u.username), e.id ASC 
			LIMIT 0, 100
		";
		
		return $this->getAdapter()->query($sql)->fetchAll();
	}

	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$db = $this->getAdapter();
		
		$where = '';
		
		if (strlen($filter['message'])) 
			$where .= $db->quoteInto(' AND cm.body LIKE ?', '%' . $filter['message'] . '%');
		
		if ( $filter['date_from'] && $filter['date_to'] && (date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']))) 
			$where .= $db->quoteInto( " AND cm.date = ? " , date('Y-m-d', $filter['date_from']));
		
		if ( $filter['date_from'] ) 
			$where .= $db->quoteInto(" AND cm.date >= ? ", date('Y-m-d', $filter['date_from']));

		if ( $filter['date_to'] ) 
			$where .= $db->quoteInto(" AND cm.date <= ? ", date('Y-m-d', $filter['date_to']));
		
		
		if (strlen($filter['f_user_id']) && strlen($filter['t_user_id'])) 
		{
			$where_both = ' ctp.user_id = ? AND cm.user_id = ? ';
			
			if ($filter['both'])
			{
				$where_both = ' ((ctp.user_id = ? AND cm.user_id = ?) OR (ctp.user_id = ' . $filter['f_user_id'] . ' AND cm.user_id = ' . $filter['t_user_id'] . ')) ';
			}
			
			$sql = '
				SELECT SQL_CALC_FOUND_ROWS
					cm.id, cm.body, cm.user_id, UNIX_TIMESTAMP(cm.date) AS date, UNIX_TIMESTAMP(cm.read_date) AS read_date, u.user_type, u.username, e.showname, e.id AS escort_id, 
					a.name AS agency_name, ctp.user_id AS user_id_to, a.id AS agency_id, m.is_suspicious, m.suspicious_reason 
				FROM chat_threads_participants ctp
				INNER JOIN chat_threads_participants ctp2 ON ctp2.thread_id = ctp.thread_id AND ctp2.user_id <> ctp.user_id
				INNER JOIN chat_messages cm ON cm.user_id = ctp2.user_id AND cm.thread_id = ctp2.thread_id
				INNER JOIN users u ON u.id = cm.user_id
				LEFT JOIN members m ON m.user_id = u.id
				LEFT JOIN escorts e ON e.user_id = u.id
				LEFT JOIN agencies a ON a.user_id = u.id
				WHERE ' . $where_both . $where . '
				GROUP BY cm.id 
				ORDER BY ' . $sort_field . ' ' . $sort_dir . '
				LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
			';
			
			$items = $db->query($sql, array($filter['t_user_id'], $filter['f_user_id']))->fetchAll();
		}
		elseif (!strlen($filter['f_user_id']) && strlen($filter['t_user_id']))
		{
			$sql = '
				SELECT SQL_CALC_FOUND_ROWS
					cm.id, cm.body, cm.user_id, UNIX_TIMESTAMP(cm.date) AS date, UNIX_TIMESTAMP(cm.read_date) AS read_date, u.user_type, u.username, e.showname, e.id AS escort_id, 
					a.name AS agency_name, ctp.user_id AS user_id_to, a.id AS agency_id, m.is_suspicious, m.suspicious_reason 
				FROM chat_threads_participants ctp
				INNER JOIN chat_threads_participants ctp2 ON ctp2.thread_id = ctp.thread_id AND ctp2.user_id <> ctp.user_id
				INNER JOIN chat_messages cm ON cm.user_id = ctp2.user_id AND cm.thread_id = ctp2.thread_id
				INNER JOIN users u ON u.id = cm.user_id
				LEFT JOIN members m ON m.user_id = u.id
				LEFT JOIN escorts e ON e.user_id = u.id
				LEFT JOIN agencies a ON a.user_id = u.id
				WHERE ctp.user_id = ? ' . $where . '
				GROUP BY cm.id 
				ORDER BY ' . $sort_field . ' ' . $sort_dir . '
				LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
			';
						
			$items = $db->query($sql, $filter['t_user_id'])->fetchAll();
		}
		elseif (strlen($filter['f_user_id']) && !strlen($filter['t_user_id']))
		{
			$sql = '
				SELECT SQL_CALC_FOUND_ROWS 
					cm.id, cm.body, cm.user_id, UNIX_TIMESTAMP(cm.date) AS date, UNIX_TIMESTAMP(cm.read_date) AS read_date, u.user_type, u.username, e.showname, e.id AS escort_id, 
					a.name AS agency_name, ctp.user_id AS user_id_to, a.id AS agency_id, m.is_suspicious, m.suspicious_reason 
				FROM chat_messages cm
				INNER JOIN chat_threads_participants ctp ON ctp.thread_id = cm.thread_id AND cm.user_id <> ctp.user_id
				INNER JOIN users u ON u.id = cm.user_id
				LEFT JOIN members m ON m.user_id = u.id
				LEFT JOIN escorts e ON e.user_id = u.id
				LEFT JOIN agencies a ON a.user_id = u.id
				WHERE cm.user_id = ? ' . $where . '
				GROUP BY cm.id 
				ORDER BY ' . $sort_field . ' ' . $sort_dir . '
				LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
			';
			
			$items = $db->query($sql, $filter['f_user_id'])->fetchAll();
		}
		else
		{
			$sql = '
				SELECT SQL_CALC_FOUND_ROWS 
					cm.id, cm.body, cm.user_id, UNIX_TIMESTAMP(cm.date) AS date, UNIX_TIMESTAMP(cm.read_date) AS read_date, u.user_type, u.username, e.showname, e.id AS escort_id, 
					a.name AS agency_name, ctp.user_id AS user_id_to, a.id AS agency_id, m.is_suspicious, m.suspicious_reason 
				FROM chat_messages cm
				INNER JOIN chat_threads_participants ctp ON ctp.thread_id = cm.thread_id AND cm.user_id <> ctp.user_id
				INNER JOIN users u ON u.id = cm.user_id
				LEFT JOIN members m ON m.user_id = u.id
				LEFT JOIN escorts e ON e.user_id = u.id
				LEFT JOIN agencies a ON a.user_id = u.id
				WHERE 1 ' . $where . '
				GROUP BY cm.id 
				ORDER BY ' . $sort_field . ' ' . $sort_dir . '
				LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
			';
			
			$items = $db->query($sql)->fetchAll();
		}
		
		$count = intval($db->fetchOne('SELECT FOUND_ROWS()'));
		
		if ($count > 50000)
			$count = 50000;
		
		return $items;
	}
	
	public function getUserInfo($user_id)
	{
		$sql = '
			SELECT u.user_type, u.username, e.showname, e.id AS escort_id, a.name AS agency_name, a.id AS agency_id
			FROM users u
			LEFT JOIN escorts e ON e.user_id = u.id
			LEFT JOIN agencies a ON a.user_id = u.id
			WHERE u.id = ?
		';
		
		return $this->getAdapter()->query($sql, $user_id)->fetch();
	}
	
	public function delete($ids)
	{
		$ids = implode(',', $ids);
		
		$this->getAdapter()->query('DELETE FROM chat_messages WHERE id IN (' . $ids . ')');
	}
}
