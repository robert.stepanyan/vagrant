<?php

class Model_FeaturedGirls extends Cubix_Model
{
    public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count = 0)
    {
        $sql = '
			SELECT SQL_CALC_FOUND_ROWS 
			    e.id AS id, UNIX_TIMESTAMP(e.date_last_modified) AS
			    date_last_modified, e.status, UNIX_TIMESTAMP(e.date_registered) AS date_registered,
                e.showname, e.due_date, e.deleted_date, e.check_website, e.check_website_date,e.check_website_person, e.block_website , e.block_website_date,e.block_website_person,
                u.id AS user_id, u.username, u.email AS reg_email, e.porn_notification, e.porn_notification_date,
                ep.phone_number,ep.phone_exists, ep.email, ep.contact_phone_parsed, ep.website, e.verified_status, e.gallery_is_checked
            FROM escorts e
            INNER JOIN users u ON u.id = e.user_id
            LEFT JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
            LEFT JOIN featured_girls fg ON fg.escort_id = e.id
                
			LEFT JOIN escort_cities ec ON ec.escort_id = e.id
			LEFT JOIN cities ct ON ec.city_id = ct.id
            WHERE e.agency_id IS NULL
		';

        if (!empty($filter['escort_id'])) {
            $sql .= ' AND e.id = ' . $filter['escort_id'];
        }

        if (!empty($filter['city'])) {
            $sql .= ' AND ( ec.city_id = ' . $filter['city'];
            $sql .= ' OR fg.city_id = ' . $filter['city'] . ')';
        } else if (!empty($filter['country'])) {
            $sql .= ' AND (ct.country_id = ' . $filter['country'];
            $sql .= ' OR fg.country_id = ' . $filter['country'] . ')';
        }

        $sql .= '
            GROUP BY e.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

        $this->getAdapter()->query('SET NAMES `utf8`');
        $rows = parent::_fetchAll($sql);

        $count = $this->getAdapter()->fetchOne('SELECT FOUND_ROWS()');

        return $rows;
    }

    public function removeFeaturedDataByEscortId($escortId)
    {
        $sql = "DELETE FROM featured_girls WHERE escort_id = ?";
        $params = array($escortId);

        parent::db()->query($sql, $params);
    }

    public function setFeaturedCountry($escortId, $countryIds)
    {
        foreach ($countryIds as $id) {
            $sql = "INSERT INTO featured_girls (escort_id, country_id) VALUES(?, ?) ";
            $params = array($escortId, $id);

            parent::db()->query($sql, $params);
        }
    }

    public function setFeaturedCities($escortId, $cityIds)
    {
        foreach ($cityIds as $id) {
            $sql = "INSERT INTO featured_girls (escort_id, city_id) VALUES(?, ?) ";
            $params = array($escortId, $id);

            parent::db()->query($sql, $params);
        }
    }

    public function getLocationsByEscortId($escortId)
    {
        $sql = "SELECT c.id as country_id, ct.id as city_id, fg.escort_id as escort_id, c.title_en as country_title, ct.title_en as city_title 
            FROM featured_girls fg 
            LEFT JOIN cities ct on fg.city_id = ct.id
            LEFT JOIN countries c on (fg.country_id = c.id OR ct.country_id = c.id)
            WHERE escort_id = ?";

        $params = array($escortId);

        return parent::_fetchAll($sql, $params);
    }

    public function isFeaturedByEscortId($escortId)
    {
        $sql = "SELECT TRUE 
                    FROM
                        escorts e
                        INNER JOIN featured_girls fg ON fg.escort_id = e.id 
                    WHERE
                        e.id = ?";

        $params = array($escortId);

        return parent::_fetchAll($sql, $params);
    }
}
