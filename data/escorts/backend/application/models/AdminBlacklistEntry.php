<?php
class Model_AdminBlacklistEntry extends Cubix_Model
{
	protected $_table = 'admin_blacklist_entry';

	public function getById($id)
	{
		$sql = '
			SELECT
				id, comment, is_approved
			FROM '.$this->_table.'
			WHERE 1 AND id = ?
		';

		return parent::_fetchRow($sql, array($id));
	}
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT 
				id, comment, is_approved
			FROM '.$this->_table.'
			WHERE 1 
		';
		
		$countSql = '
			SELECT COUNT(DISTINCT(id))
			FROM '.$this->_table.'
			WHERE 1 
		';
		
		$where = '';
				
		if ( strlen($filter['comment']) ) {
			$where .= self::quote('AND comment LIKE ?', '%' . $filter['comment'] . '%');
		}
				
		if ( $filter['status'] ) {
			$status = 1;
			if ( $filter['status'] == 3 ) {
				$status = 0;
			}
			$where .= self::quote('AND is_approved = ?', $status);
		}
				
		$sql .= $where;
		$countSql .= $where;

		$sql .= '
			GROUP BY id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$count = intval($this->getAdapter()->fetchOne($countSql));

		return parent::_fetchAll($sql);
	}	
	
	public function getImages($id)
	{
		$sql = '
				SELECT 
						image_id, hash, ext
					FROM admin_blacklist_entry_images as abe
					LEFT JOIN images i ON i.id = abe.image_id
					WHERE abe.ae_id = '.$id.'
				';

		return parent::_fetchAll($sql);		
	}

	public function save($data)
	{
		if (isset($data['id']) && $data['id'] != '') {
			$id = $data['id'];
			unset($data['id']);

			if(isset($data['attach'])){
				$photos = $data['attach'];
				unset($data['attach']);
			}

			parent::getAdapter()->update($this->_table, $data, parent::quote('id = ?', $id));

			if ( $id && count($photos) ) {
				foreach( $photos as $photo ) {
					parent::getAdapter()->insert('admin_blacklist_entry_images', array('ae_id' => $id, 'image_id' => $photo));
				}
			}
		}
	}

	public function insert($data)
	{
		if(isset($data['attach'])){
			$photos = $data['attach'];
			unset($data['attach']);
		}

		parent::getAdapter()->insert($this->_table, $data);	
		$ae_id = parent::getAdapter()->lastInsertId();

		if ( $ae_id && count($photos) ) {
			foreach( $photos as $photo ) {
				parent::getAdapter()->insert('admin_blacklist_entry_images', array('ae_id' => $ae_id, 'image_id' => $photo));
			}
		}
	}

	public function toggle($id)
	{
		$field = 'is_approved';
		
		parent::getAdapter()->update($this->_table, array(
			$field => new Zend_Db_Expr('NOT ' . $field)
		), parent::quote('id = ?', $id));
	}
	
	
	public function remove($id)
	{
		
		parent::getAdapter()->delete($this->_table, parent::quote('id = ?', $id));
		
		
		$sql = '
				SELECT image_id
					FROM admin_blacklist_entry_images
					WHERE ae_id = '.$id.'
				';
		$ae_images = parent::_fetchAll($sql);	

		if ( count($ae_images) ) {
			foreach( $ae_images as $image ) {
				parent::getAdapter()->delete('images', parent::quote('id = ?', $image->image_id));
			}
		}

		parent::getAdapter()->delete("admin_blacklist_entry_images", parent::quote('ae_id = ?', $id));
	}

	public function removeImage($id)
	{
		parent::getAdapter()->delete("admin_blacklist_entry_images", parent::quote('image_id = ?', $id));
		parent::getAdapter()->delete("images", parent::quote('id = ?', $id));
	}
}
