<?php
class Model_Reviews extends Cubix_Model
{
	protected $_table = 'reviews';

	public function getById($id)
	{
	    $select = '';
        $joins = '';

        if(Cubix_Application::getId() == APP_ED){
            $select .=',quality,location_review,r.country, ct.' . Cubix_I18n::getTblField('title') . ' AS country';
            $joins .= 'LEFT JOIN countries ct ON ct.id = r.country';
        }

		$sql = '
			SELECT
				r.id, u.username, e.showname, e.id AS escort_id, r.review, UNIX_TIMESTAMP(r.creation_date) AS creation_date, UNIX_TIMESTAMP(r.meeting_date) AS meeting_date,
				UNIX_TIMESTAMP(r.last_modified) AS last_modified, r.looks_rating, r.services_rating, r.sms_status, r.status, c.' . Cubix_I18n::getTblField('title') . ' AS city,
				r.city_is_in_escort_profile, r.fuckometer, r.meeting_place, r.duration, r.duration_unit, r.price, r.currency, r.s_kissing, r.s_blowjob, r.s_cumshot, r.s_69,
				r.s_anal, r.s_sex, r.s_attitude, r.s_conversation, r.s_breast, r.s_multiple_sex, r.s_availability, r.s_photos, r.services_comments, r.review , r.ip, r.is_fake_free,
				r.is_suspicious, r.t_user_info, r.t_meeting_date, r.t_meeting_place, r.t_comments, r.is_deleted, UNIX_TIMESTAMP(r.deleted_date) AS deleted_date, r.reason, r.disabled_reason,
				u.id AS user_id, r.sms_unique, m.suspicious_reason, m.trusted_reason, r.old_review, r.disable_type, r.escort_comment '. $select .'
			FROM reviews r
			INNER JOIN users u ON r.user_id = u.id
			INNER JOIN members m ON m.user_id = u.id
			INNER JOIN escorts e ON e.id = r.escort_id
			LEFT JOIN cities c ON c.id = r.city
			'. $joins .' 
			WHERE 1 AND r.id = ?
		';

		return parent::_fetchRow($sql, array($id));
	}
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$fields = '';
		$join = '';
		$where = '';

		if ( in_array(Cubix_Application::getId(), array(APP_6A, APP_EF)) ) {
			$fields = ', m.rating ';
		}
		
		if ( strlen($filter['application_id']) ) {
			$where .= self::quote(' AND r.application_id = ? ', $filter['application_id']);
		}
		
		if ( strlen($filter['showname']) ) {
			if ($filter['mode'] == 'fixed')
			{
				if ( $filter['escort_id'] ) 
					$where .= self::quote(' AND e.id = ? ', $filter['escort_id']);
				else
					$where .= self::quote(' AND e.showname = ?', $filter['showname']);
			}
			else
				$where .= self::quote(' AND e.showname LIKE ? ', $filter['showname'] . '%');
		}
		
		if ( strlen($filter['username']) ) {
			$where .= self::quote(' AND u.username LIKE ? ', $filter['username'] . '%');
		}
		
		if ( strlen($filter['escort_id_f']) ) {
			$where .= self::quote(' AND e.id = ? ', $filter['escort_id_f']);
		}

		if ( strlen($filter['sms_unique']) ) {
			$where .= self::quote(' AND r.sms_unique = ? ', $filter['sms_unique']);
		}
				
		if ( $filter['status'] ) {
			$where .= self::quote(' AND r.status = ? ', $filter['status']);
		}

		if ( $filter['is_fake_free'] ) {
			$where .= self::quote(' AND r.is_fake_free = ? ', $filter['is_fake_free']);
		}

        if ( $filter['added'] ) {
            $where .= self::quote(' AND r.added = ? ', $filter['added']);
        }
		
		if ( strlen($filter['modified']) ) {
			$where .= self::quote(' AND r.modified = ? ', $filter['modified']);
		}
		
		if ( strlen($filter['answer']) ) {
			if ($filter['answer'] == 'not_answer')
				$where .= ' AND (r.answer IS NULL  OR r.answer = 0) ';
			elseif ($filter['answer'] == 2)
				$where .= ' AND r.answer = 2 AND r.status = 1 ';
			else
				$where .= self::quote(' AND r.answer = ? ', 1);
		}

		if ( $filter['is_suspicious'] ) {
			$where .= self::quote(' AND r.is_suspicious = ? ', $filter['is_suspicious']);
		}

		if ( $filter['country'] ) {
			$join .= ' INNER JOIN cities c ON c.id = r.city ';
			$where .= self::quote(' AND c.country_id = ? ', $filter['country']);
		}

		if ( $filter['city'] ) {
			$where .= self::quote(' AND r.city = ? ', $filter['city']);
		}

		if ( strlen($filter['ip']) ) {
			$where .= self::quote(' AND r.ip LIKE ? ', $filter['ip'] . '%');
		}
		
		if ( $filter['is_active_escort'] ) {
			$where .= ' AND e.status = '.Model_Escorts::ESCORT_STATUS_ACTIVE;
		}
		
		if ( $filter['has_active_package'] ) {
			$join .= ' INNER JOIN order_packages ord_p ON ord_p.escort_id = e.id ';
			if(Cubix_Application::getId() == 1){
				$where .= ' AND ord_p.status = 2 AND ord_p.package_id != 100 ';
			}
			elseif(Cubix_Application::getId() == 30 || Cubix_Application::getId() == 11){
				$where .= ' AND ord_p.status = 2 AND ord_p.package_id != 9 ';
			}
			else{
				$where .= ' AND ord_p.status = 2 AND ord_p.package_id != 97 ';
			}
		}
		

		if ( isset($filter['filter_by']) && $filter['filter_by'] ) {
			if ( $filter['filter_by'] == 'registration_date' ) {
				$date_field = 'r.creation_date';
				$fields .= ', UNIX_TIMESTAMP( r.creation_date ) ';
			}
			elseif ( $filter['filter_by'] == 'last_modified_date' ) {
				$date_field = 'e.date_last_modified';
				$fields .= ', e.date_last_modified ';
			}


			if ( $filter['date_from'] && $filter['date_to'] ) {
				if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) ) {
					$where .= self::quote(' AND DATE(' . $date_field . ') = DATE(FROM_UNIXTIME(?)) ', $filter['date_from']);
				}
			}

			if ( $filter['date_from'] ) {
				$where .= self::quote(' AND DATE(' . $date_field . ') >= DATE(FROM_UNIXTIME(?)) ', $filter['date_from']);
			}

			if ( $filter['date_to'] ) {
				$where .= self::quote(' AND DATE(' . $date_field . ') <= DATE(FROM_UNIXTIME(?)) ', $filter['date_to']);
			}
		}
		
		if ( isset($filter['review_texts']) && $filter['review_texts']  ) {
			$where .= ' AND r.review LIKE "%'.$filter['review_texts'].'%"';
		}
		
		if ( isset($filter['disable_type']) && $filter['disable_type']  ) {
			$where .= ' AND r.status = ' . REVIEW_DISABLED . ' AND r.disable_type = ' . $filter['disable_type'];
		}
		
		$where .= self::quote(' AND r.is_deleted = ? ', $filter['deleted']);

		$fields_ef = '';

        //$_from_mobile = '';
		if (Cubix_Application::getId() == APP_EF)
		{
			$fields_ef = ' (SELECT TRUE FROM order_packages WHERE escort_id = e.id AND status = 2 AND order_id IS NOT NULL) AS has_package, r.is_mobile,';
			//$_from_mobile = 'r.is_mobile,';
		}

		$sql = '
			SELECT
				r.id,
				r.added,
				u.username,
				u.last_ip,
				u.id AS user_id,
				e.showname,
				e.id AS escort_id,
				r.review,
				UNIX_TIMESTAMP(r.creation_date) AS creation_date,
				UNIX_TIMESTAMP(r.meeting_date) AS meeting_date,
				r.looks_rating,
				r.services_rating,
				r.sms_unique,
				r.sms_status,
				r.status,
				r.is_fake_free,
				r.is_suspicious,
				IF(u.date_registered >= DATE_ADD(NOW(), INTERVAL -72 HOUR), 1, 0) AS new_member,
				m.is_suspicious AS m_is_suspicious,
				m.is_trusted AS m_is_trusted,
				UNIX_TIMESTAMP(u.date_registered) AS date_registered,
				m.suspicious_reason,
				m.trusted_reason,
				m.is_premium,
				m.member_comment,
				r.answer,
				r.comment_grid,
				e.status AS escort_status, ' . $fields_ef .  '
				u.email' . $fields . '
			FROM reviews r
			INNER JOIN users u ON r.user_id = u.id
			INNER JOIN members m ON m.user_id = u.id
			INNER JOIN escorts e ON e.id = r.escort_id
			' . $join . '
			WHERE 1
		';

		$countSql = '
			SELECT COUNT(DISTINCT(r.id))
			FROM reviews r
			INNER JOIN users u ON r.user_id = u.id
			INNER JOIN members m ON m.user_id = u.id
			INNER JOIN escorts e ON e.id = r.escort_id
			LEFT JOIN order_packages ord_p ON ord_p.escort_id = e.id  AND  ord_p.application_id = ' . Cubix_Application::getId().'
			' . $join . '
			WHERE 1
		';
						
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			GROUP BY r.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		//echo $sql;die;

		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return parent::_fetchAll($sql);
	}	
	
	public function save($data)
	{
		if (isset($data['id']) && $data['id'] != '')
		{
			$fields = array(
				'status' => $data['status'],
				'is_fake_free' => $data['is_fake_free'],
				'meeting_place' => $data['place'],
				'duration' => $data['duration'],
				'duration_unit' => $data['duration_unit'],
				'price' => $data['price'],
				'currency' => $data['currency'],
				'looks_rating' => $data['looks_rating'],
				'services_rating' => $data['services_rating'],
				'sms_status' => $data['sms_status'],
				's_kissing' => $data['kissing'],
				's_blowjob' => $data['blowjob'],
				's_cumshot' => $data['cumshot'],
				's_69' => $data['s_69'],
				's_anal' => $data['anal'],
				's_sex' => $data['sex'],
				's_attitude' => $data['attitude'],
				's_conversation' => $data['conversation'],
				's_breast' => $data['breast'],
				's_multiple_sex' => $data['multiple_times_sex'],
				's_availability' => $data['availability'],
				's_photos' => $data['photos'],
				'services_comments' => $data['services_comments'],
				'review' => $data['review'],
				'escort_comment' => $data['escort_comment'],
				'last_modified' => new Zend_Db_Expr('NOW()')
			);

			if(Cubix_Application::getId() == APP_ED){
                $fields['quality'] = $data['quality'];
                $fields['location_review'] = $data['location_review'];

                if (strpos($data['creation_date'], ',')) {
                    $date = date('Y-m-d', strtotime(str_replace(',','',$data['creation_date'])));
                }
                else {
                    $date = date('Y-m-d', $data['creation_date']);
                }

                $fields['creation_date'] =  $date . '  ' .$data['creation_date_time'];

            }

			$fields['fuckometer'] = $this->getFuckometer($data);

			

			parent::getAdapter()->update($this->_table, $fields, parent::quote('id = ?', $data['id']));
			
			$rev_date = parent::getAdapter()->fetchOne('SELECT creation_date FROM reviews WHERE id = ?', $data['id']);
			
			if ($data['status'] == REVIEW_APPROVED)
				Cubix_AlertEvents::notify($data['esc_id'], ALERT_ME_NEW_REVIEW, array('id' => $data['id'], 'set_date' => $rev_date));
		}
	}
	
	public function remove($data)
	{
		parent::getAdapter()->update($this->_table, array(
			'is_deleted' => 1,
			'deleted_date' =>  new Zend_Db_Expr('NOW()'),
			'reason' => $data['reason']
		), parent::quote('id = ?', $data['id']));
	}

	public function removeOlds($interval = 30)
	{
		$sql = 'DELETE FROM reviews WHERE is_deleted = 1 AND deleted_date < DATE_ADD(NOW(), INTERVAL -' . $interval . ' DAY)';
		parent::getAdapter()->query($sql);
	}

	public function getFuckometer($data)
	{
		$fuckometer = 71;

        $fuckometer += ($data['looks_rating']) * 0.45;
        $fuckometer += ($data['services_rating']) * 0.45;
        $fuckometer += (BLOWJOB_WITHOUT_CONDOM == $data['blowjob'])? 4 : 0;
        $fuckometer += (BLOWJOB_WITH_CONDOM == $data['blowjob'])? 2 : 0;
        $fuckometer += (KISSING_WITH_TONGUE == $data['kissing'])? 4 : 0;
        $fuckometer += (KISSING == $data['kissing'])? 2 : 0;
        $fuckometer += (CUMSHOT_IN_MOUTH_SPIT == $data['cumshot'])? 1 : 0;
        $fuckometer += (CUMSHOT_IN_MOUTH_SWALLOW == $data['cumshot'])? 1.5 : 0;
        $fuckometer += (YES == $data['anal'])? 1 : 0;
        $fuckometer += (YES == $data['s_69'])? 1 : 0;
        $fuckometer += (YES == $data['multiple_times_sex'])? 1 : 0;
        $fuckometer += (SEX_ENTHUSIASTIC == $data['sex'])? 1.5 : 0;
        $fuckometer += (SEX_PASSIVE == $data['sex'])? -2 : 0;
        $fuckometer += (SEX_ACTIVE == $data['sex'])? 0.5 : 0;
        $fuckometer += (ATTITUDE_FRIENDLY == $data['attitude'])? 1.5 : 0;
        $fuckometer += (ATTITUDE_UNFRIENDLY == $data['attitude'])? -2 : 0;
        $fuckometer += (ATTITUDE_POSHY == $data['attitude'])? -1 : 0;
        $fuckometer += (ATTITUDE_NORMAL == $data['attitude'])? 0.5 : 0;
        $fuckometer += (CONVERSATION_LANGUAGE_PROBLEMS == $data['conversation'])? -2 : 0;
        $fuckometer += (CONVERSATION_NORMAL == $data['conversation'])? 1 : 0;
        $fuckometer += (CONVERSATION_INTELLIGENT == $data['conversation'])? 1.5 : 0;
        $fuckometer += (CONVERSATION_VERY_SIMPLE == $data['conversation'])? -0.5 : 0;
        $fuckometer += (AVAILABILITY_EASY == $data['availability'])? 1 : 0;

        // reviews count bonus
        $escort = new Model_EscortsV2();

        $count = $escort->getReviewsCount($data['escort_id']);

		if (5 < $count && 10 >= $count)
		    $fuckometer += 0.5;
		else if (10 < $count && 20 >= $count)
		    $fuckometer += 1;
		else if (20 < $count)
		    $fuckometer += 2;

        $fuckometer = round($fuckometer, 1);

        return $fuckometer;
	}

	public function fakeByPhone($user_id)
	{
		$db = parent::getAdapter();

		$code_count = $db->fetchOne('SELECT COUNT(r.id) FROM reviews r INNER JOIN escort_profiles_v2 e_p ON r.escort_id = e_p.escort_id WHERE r.is_deleted = 0 AND MID(e_p.contact_phone_parsed, 1, 3) = ? AND r.user_id = ?', array('007', $user_id));
		$all_count = $db->fetchOne('SELECT COUNT(id) FROM reviews WHERE is_deleted = 0 AND user_id = ?', $user_id);

		if ($all_count > 1 && $code_count == $all_count)
			return true;
		else
			return false;
	}

	public function fakeByCity($user_id)
	{
		$db = parent::getAdapter();

		$all_count = $db->fetchOne('SELECT COUNT(DISTINCT(city)) FROM reviews WHERE is_deleted = 0 AND city > 0 AND user_id = ?', $user_id);

		if ($all_count > 1)
		{
			//return true;
			$cities = $db->query('SELECT c.title_en AS city_title, COUNT(r.city) AS count FROM reviews r INNER JOIN cities c ON c.id = r.city WHERE r.is_deleted = 0 AND r.city > 0 AND r.user_id = ? GROUP BY r.city', $user_id)->fetchAll();
			$cc = array();
			
			foreach ($cities as $c)
				$cc[] = $c->city_title . ' - ' . $c->count;

			$cts = implode(', ', $cc);
			
			return $cts;
		}
		else
			return false;
	}

	public function getSMSByUnique($unique)
	{
		return parent::getAdapter()->fetchOne('SELECT text FROM sms_inbox WHERE LOCATE(?, text) > 0', $unique);
	}

	public function getMemberReviewsCount($user_id, $status = null)
	{
		$where = '';
		
		if (!is_null($status))
			$where = ' AND status = ' . $status;
		
		return parent::getAdapter()->fetchOne('SELECT COUNT(id) FROM reviews WHERE user_id = ? AND is_deleted = 0' . $where . ' GROUP BY user_id', $user_id);
	}

	public function getEscortReviewsCount($escort_id, $status = null)
	{
		$where = '';
		
		if (!is_null($status))
			$where = ' AND status = ' . $status;
		
		return parent::getAdapter()->fetchOne('SELECT COUNT(id) FROM reviews WHERE escort_id = ? AND is_deleted = 0' . $where . ' GROUP BY escort_id', $escort_id);
	}

	public function getEscortLastReviewDate($escort_id)
	{
		return parent::getAdapter()->query('
			SELECT UNIX_TIMESTAMP(creation_date) AS date, status  
			FROM reviews 
			WHERE escort_id = ? AND is_deleted = 0 AND (status = ? OR status = ?) 
			ORDER BY id DESC 
			LIMIT 1
		', array($escort_id, REVIEW_APPROVED, REVIEW_DISABLED))->fetch();
	}
	
	public function getMemberLastReviewDate($user_id)
	{
		return parent::getAdapter()->query('
			SELECT UNIX_TIMESTAMP(creation_date) AS date, status  
			FROM reviews 
			WHERE user_id = ? AND is_deleted = 0 AND (status = ? OR status = ?) 
			ORDER BY id DESC 
			LIMIT 1
		', array($user_id, REVIEW_APPROVED, REVIEW_DISABLED))->fetch();
	}
	
	public function disabledReason($data)
	{
		parent::getAdapter()->update($this->_table, array(
			'disabled_reason' => $data['reason'],
			'disable_type' => $data['disable_type'],
			'status' => REVIEW_DISABLED
		), parent::quote('id = ?', $data['id']));
	}
	
	public function getEscortIdById($id)
	{
		return parent::getAdapter()->fetchOne('SELECT escort_id FROM reviews WHERE id = ?', $id);
    }
	
	public function getCommentGrid($id)
	{
		return parent::getAdapter()->fetchOne('SELECT comment_grid FROM reviews WHERE id = ?', $id);
	}
	
	public function updateCommentGrid($data)
	{
		return parent::db()->update($this->_table, array('comment_grid' => $data['comment']), parent::quote('id = ?', $data['id']));
		
	}
	
	public function getAllReplies($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT
				r.id, u.username, 
				r.escort_id,
				r.user_id AS user_id,
				e.showname,
				e.id AS escort_id,
				r.escort_comment,
				r.escort_comment_status,
				IF(u.date_registered >= DATE_ADD(NOW(), INTERVAL -72 HOUR), 1, 0) AS new_member,
				m.is_suspicious AS m_is_suspicious,
				m.is_trusted AS m_is_trusted,
				m.is_premium,
				m.member_comment,
				UNIX_TIMESTAMP(u.date_registered) AS date_registered
			FROM reviews r
			INNER JOIN escorts e ON e.id = r.escort_id
			INNER JOIN users u ON r.user_id = u.id
			INNER JOIN members m ON m.user_id = u.id
			WHERE r.escort_comment IS NOT NULL AND r.escort_comment <> ""
		';
		
		$countSql = '
			SELECT COUNT(DISTINCT(r.id))
			FROM reviews r
			INNER JOIN escorts e ON e.id = r.escort_id
			INNER JOIN users u ON r.user_id = u.id
			INNER JOIN members m ON m.user_id = u.id
			WHERE r.escort_comment IS NOT NULL AND r.escort_comment <> ""
		';
		
		$where = '';

		if ( $filter['escort_id'] ) {
			$where .= self::quote('AND r.escort_id = ?', $filter['escort_id']);
		}
		
		if ( $filter['status'] ) {
			$where .= self::quote('AND r.escort_comment_status = ?', $filter['status']);
		}
		
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			GROUP BY r.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$count = intval($this->getAdapter()->fetchOne($countSql));
		return parent::_fetchAll($sql);
	}
	
	public function getEscortComment($id)
	{
		return parent::getAdapter()->fetchOne('SELECT escort_comment FROM reviews WHERE id = ?', $id);
	}
	
	public function saveEscortComment($data)
	{
		$fields = array(
			'escort_comment' => $data['comment'],
			'escort_comment_status' => $data['escort_comment_status']
		);
		return parent::getAdapter()->update($this->_table, $fields, parent::quote('id = ?', $data['review_id']));
	}
	
	public function setEscortCommentStatus($review_id,$status)
	{
		return parent::getAdapter()->update($this->_table, array('escort_comment_status' => $status ), parent::quote('id = ?', $review_id));
	}

	public function getDataForForum($id)
	{
		return parent::getAdapter()->query('
			SELECT UNIX_TIMESTAMP(r.meeting_date) AS meeting_date, c.' . Cubix_I18n::getTblField('title') . ' AS city
			FROM reviews r
			LEFT JOIN cities c ON c.id = r.city
			WHERE r.id = ?
		', $id)->fetch();
	}

    public function insert($data)
    {
        $fields = array(
            'user_id' => $data['member_id'],
            'application_id' => Cubix_Application::getId(),
            'meeting_date' => $data['meeting_date'],
            'escort_id' => $data['escort_id'],
            'city' => $data['city'],
            'country' => $data['country'],
            'status' => 2,
            'is_fake_free' => $data['is_fake_free'],
            'meeting_place' => $data['place'],
            'duration' => $data['duration'],
            'duration_unit' => $data['duration_unit'],
            'price' => $data['price'],
            'ip' => Cubix_Geoip::getIP(),
            'currency' => $data['currency'],
            'looks_rating' => $data['looks_rating'],
            'services_rating' => $data['services_rating'],
            'sms_status' => $data['sms_status'],
            's_kissing' => $data['kissing'],
            's_blowjob' => $data['blowjob'],
            's_cumshot' => $data['cumshot'],
            's_69' => $data['69'],
            's_anal' => $data['anal'],
            's_sex' => $data['sex'],
            's_attitude' => $data['attitude'],
            's_conversation' => $data['conversation'],
            's_breast' => $data['breast'],
            's_multiple_sex' => $data['multiple_times_sex'],
            's_availability' => $data['availability'],
            's_photos' => $data['photos'],
            'services_comments' => $data['services_comments'],
            'review' => $data['review'],
            'escort_comment' => $data['escort_comment'],
            'location_review' => $data['location_review'],
            'last_modified' => new Zend_Db_Expr('NOW()'),
            'creation_date' => new Zend_Db_Expr('NOW()')

        );

        if(Cubix_Application::getId() == APP_ED) {
            $fields['creation_date'] = $data['creation_date'] . ' '. date('H:m:i');
            $fields['added'] = $data['added'];
        }

        parent::getAdapter()->insert('reviews', $fields);
    }
}
