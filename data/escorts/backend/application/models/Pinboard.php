<?php
class Model_Pinboard extends Cubix_Model
{
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT p.id, p.user_id, UNIX_TIMESTAMP(p.date) AS date, UNIX_TIMESTAMP(p.modification_date) AS modification_date, p.user_type, p.title, p.status, e.showname, e.id AS escort_id, u.username, u.email, 
				a.name AS agency_name, a.id AS agency_id 
			FROM pin_posts p 
			LEFT JOIN escorts e ON e.user_id = p.user_id 
			LEFT JOIN agencies a ON a.user_id = p.user_id 
			INNER JOIN users u ON p.user_id = u.id 
			WHERE 1
		';
		
		$countSql = '
			SELECT COUNT(DISTINCT(p.id))
			FROM pin_posts p 
			INNER JOIN users u ON p.user_id = u.id
			LEFT JOIN escorts e ON e.user_id = p.user_id 
			LEFT JOIN agencies a ON a.user_id = p.user_id 
			WHERE 1
		';
		
		$where = '';

		if ( strlen($filter['application_id']) ) {
			$where .= self::quote('AND p.application_id = ?', $filter['application_id']);
		}
		
		if ( $filter['id'] ) {
			$where .= self::quote('AND p.id = ?', $filter['id']);
		}
		
		if ( $filter['escort_id'] ) {
			$where .= self::quote('AND e.id = ?', $filter['escort_id']);
		}
		
		if ( $filter['showname'] ) {
			$where .= self::quote('AND e.showname LIKE ?', $filter['showname'] . '%');
		}
		
		if ( $filter['agency_name'] ) {
			$where .= self::quote('AND a.name LIKE ?', $filter['agency_name'] . '%');
		}
		
		if ( $filter['username'] ) {
			$where .= self::quote('AND u.username LIKE ?', $filter['username'] . '%');
		}
		
		if ( $filter['status'] ) {
			$where .= self::quote('AND p.status = ?', $filter['status']);
		}
		
		if ( $filter['user_type'] ) {
			if ($filter['user_type'] == 'member')
				$where .= self::quote('AND p.user_type = ?', $filter['user_type']);
			else
				$where .= ' AND p.user_type IN ("escort", "agency") ';
		}
					
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			GROUP BY p.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return $this->getAdapter()->query($sql)->fetchAll();
	}
	
	public function remove($id)
	{
		parent::getAdapter()->delete('pin_posts', parent::quote('id = ?', $id));
		parent::getAdapter()->delete('pin_replies', parent::quote('post_id = ?', $id));
	}
		
	public function get($id)
	{
		$sql = '
			SELECT 
				p.id, p.user_id, UNIX_TIMESTAMP(p.date) AS date, p.user_type, p.title, p.post, p.status, e.showname, e.id AS escort_id, u.username, 
				a.name AS agency_name, a.id AS agency_id 
			FROM pin_posts p 
			LEFT JOIN escorts e ON e.user_id = p.user_id 
			LEFT JOIN agencies a ON a.user_id = p.user_id 
			INNER JOIN users u ON p.user_id = u.id 
			WHERE p.id = ?
		';

		return parent::getAdapter()->query($sql, $id)->fetch();
	}
	
	public function getReplies($post_id)
	{
		return parent::getAdapter()->query('
			SELECT u.id AS user_id, u.username, u.user_type, e.id AS escort_id, e.showname, a.id AS agency_id, a.name AS agency_name, pr.reply, UNIX_TIMESTAMP(pr.date) AS date 
			FROM pin_replies pr 
			LEFT JOIN users u ON pr.user_id = u.id 
			LEFT JOIN escorts e ON e.user_id = u.id 
			LEFT JOIN agencies a ON a.user_id = u.id 
			WHERE pr.post_id = ? 
			GROUP BY pr.id 
			ORDER BY pr.date DESC
		', $post_id)->fetchAll();
	}
	
	public function save($data)
	{
		if (isset($data['id']) && $data['id'] != '')
		{
			$id = $data['id'];
			unset($data['id']);
			
			parent::getAdapter()->update('pin_posts', $data, parent::quote('id = ?', $id));			
		}
	}
	
	public function getAllReplies($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT pr.id, pr.post_id, pr.user_id, UNIX_TIMESTAMP(pr.date) AS date, u.user_type, e.showname, e.id AS escort_id, u.username, a.name AS agency_name, a.id AS agency_id 
			FROM pin_replies pr 
			INNER JOIN pin_posts p ON p.id = pr.post_id 
			INNER JOIN users u ON pr.user_id = u.id 
			LEFT JOIN escorts e ON e.user_id = pr.user_id 
			LEFT JOIN agencies a ON a.user_id = pr.user_id 
			WHERE 1
		';
		
		$countSql = '
			SELECT COUNT(DISTINCT(pr.id))
			FROM pin_replies pr 
			INNER JOIN pin_posts p ON p.id = pr.post_id 
			INNER JOIN users u ON pr.user_id = u.id
			LEFT JOIN escorts e ON e.user_id = pr.user_id 
			LEFT JOIN agencies a ON a.user_id = pr.user_id 
			WHERE 1
		';
		
		$where = '';

		if ( strlen($filter['application_id']) ) {
			$where .= self::quote('AND p.application_id = ?', $filter['application_id']);
		}
		
		if ( $filter['id'] ) {
			$where .= self::quote('AND pr.id = ?', $filter['id']);
		}
		
		if ( $filter['post_id'] ) {
			$where .= self::quote('AND pr.post_id = ?', $filter['post_id']);
		}
		
		if ( $filter['escort_id'] ) {
			$where .= self::quote('AND e.id = ?', $filter['escort_id']);
		}
		
		if ( $filter['showname'] ) {
			$where .= self::quote('AND e.showname LIKE ?', $filter['showname'] . '%');
		}
		
		if ( $filter['agency_name'] ) {
			$where .= self::quote('AND a.name LIKE ?', $filter['agency_name'] . '%');
		}
		
		if ( $filter['username'] ) {
			$where .= self::quote('AND u.username LIKE ?', $filter['username'] . '%');
		}
				
		if ( $filter['user_type'] ) {
			if ($filter['user_type'] == 'member')
				$where .= self::quote('AND u.user_type = ?', $filter['user_type']);
			else
				$where .= ' AND u.user_type IN ("escort", "agency") ';
		}
					
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			GROUP BY pr.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return $this->getAdapter()->query($sql)->fetchAll();
	}
	
	public function removeReply($id)
	{
		parent::getAdapter()->delete('pin_replies', parent::quote('id = ?', $id));
	}
	
	public function getReply($id)
	{
		$sql = '
			SELECT pr.id, pr.user_id, UNIX_TIMESTAMP(pr.date) AS date, u.user_type, pr.reply, e.showname, e.id AS escort_id, u.username, a.name AS agency_name, a.id AS agency_id 
			FROM pin_replies pr 
			LEFT JOIN escorts e ON e.user_id = pr.user_id 
			LEFT JOIN agencies a ON a.user_id = pr.user_id 
			INNER JOIN users u ON pr.user_id = u.id 
			WHERE pr.id = ?
		';

		return parent::getAdapter()->query($sql, $id)->fetch();
	}
	
	public function getPostByReply($reply_id)
	{
		$sql = '
			SELECT 
				p.id, p.user_id, UNIX_TIMESTAMP(p.date) AS date, p.user_type, p.title, p.post, p.status, e.showname, e.id AS escort_id, u.username, 
				a.name AS agency_name, a.id AS agency_id 
			FROM pin_posts p 
			INNER JOIN pin_replies pr ON pr.post_id = p.id 
			LEFT JOIN escorts e ON e.user_id = p.user_id 
			LEFT JOIN agencies a ON a.user_id = p.user_id 
			INNER JOIN users u ON p.user_id = u.id 
			WHERE pr.id = ?
		';

		return parent::getAdapter()->query($sql, $reply_id)->fetch();
	}
	
	public function saveReply($data)
	{
		if (isset($data['id']) && $data['id'] != '')
		{
			$id = $data['id'];
			unset($data['id']);
			
			parent::getAdapter()->update('pin_replies', $data, parent::quote('id = ?', $id));
		}
	}
}
