<?php


class Model_Analytics  extends Cubix_Model {
	protected $_table = 'analytics'; 
	public static $archive;
	private $db;
	public function __construct() 
	{
		if(self::$archive!=0)
		{	$config = Zend_Registry::get('analytics_config');
			$this->db =  Zend_Db::factory('mysqli', array(
				'host' => $config['host'],
				'username' => $config['username'],
				'password' => $config['password'],
				'dbname' => $config['dbname']
			));
			$this->_table = 'analytics_'.self::$archive;
		}
		else
		$this->db =  Zend_Registry::get('db');
	}
	
	public function GetFilterStatistic($where,$page,$per_page,$sort_field,$sort_dir)
	{	
		
		$page = !isset($page) || !is_numeric($page)?1:$this->db->quote($page);
		$start = ($page-1)*$per_page;
		$sql = "SELECT SQL_CALC_FOUND_ROWS COUNT(`value`) AS quantity,UNIX_TIMESTAMP(`date`) as `date`,year_date,`name`,`group`,text_value,user_id,`value`,id
					FROM $this->_table WHERE  $where GROUP BY `value`,`name`,`filter`,year_date
					ORDER BY `$sort_field` $sort_dir LIMIT $start,$per_page" ;
	
		$data =  $this->db->fetchAll($sql);
	
		$found =  $this->db->fetchRow('SELECT FOUND_ROWS() AS q');

		return	array('data'=>$data,'count'=>$found->q);
	}


	public function GetGroupItemStatistic($where,$page,$per_page,$sort_field,$sort_dir)
	{
		$page = !isset($page) || !is_numeric($page)?1:$this->db->quote($page);
		$start = ($page-1)*$per_page;
		$sql = "SELECT SQL_CALC_FOUND_ROWS COUNT(`value`) AS quantity,`name`, text_value,UNIX_TIMESTAMP(`date`) as `date`,year_date,`type`,`value`,`group`,id FROM $this->_table
				WHERE $where  GROUP BY `name`,`type`,text_value,year_date,`value`
				ORDER BY `$sort_field` $sort_dir LIMIT $start,$per_page";
		
		$data = $this->db->fetchAll($sql);
		$found =  $this->db->fetchRow('SELECT FOUND_ROWS() AS q');
		return array('data'=>$data,'count'=>$found->q);
	}
	public function GetItemStatistic($where,$page,$per_page,$sort_field,$sort_dir)
	{
		$page = !isset($page) || !is_numeric($page)?1:$this->db->quote($page);
		$start = ($page-1)*$per_page;
		$sql = "SELECT SQL_CALC_FOUND_ROWS COUNT(`value`) AS quantity,UNIX_TIMESTAMP(`date`) as `date`,year_date,`name`,`group`,`text_value`,`value`,id FROM $this->_table
				WHERE $where  GROUP BY `name`,`type`,year_date,`value`
				ORDER BY `$sort_field` $sort_dir LIMIT $start,$per_page";
		
		$data = $this->db->fetchAll($sql);
		$found =  $this->db->fetchRow('SELECT FOUND_ROWS() AS q');
		return array('data'=>$data,'count'=>$found->q);
	}
	public function GetGroups($filter,$app_id)
	{	
		$filter = $this->db->quote($filter);
		return $this->db->fetchAll("SELECT `group` FROM $this->_table WHERE `filter` LIKE $filter AND application_id='$app_id' GROUP BY `group`");
	}
	public function CreateWhere(array $filter)
	{	
		$where = array();
		foreach($filter as $k=>$v)
		{
			if(isset($v) && ($v=trim($v))!='')
			{
				if($k!='date_from' && $k!='date_to')
				{	
					if($k!='group' && $k!='filter' && $k!='year_date' && $k!='name' && $k!='application_id')
					{	
						$v.='%';
						
					}
					$v = $this->db->quote($v);
					
					if($k!='name')
						$where[] = " `$k` LIKE $v ";
					else
						$where[] = " `$k`=$v ";
				}
				elseif(is_numeric($v))
				{	$date =date('Y-m-d',$v);
					if($k=='date_from')
					{
						$where[]=" `year_date`>='$date' ";
					}
					else
					{
						$where[]=" `year_date`<='$date' ";
					}
				}
			}
		}
		$where = implode(' AND ', $where);
		return $where;
	}


	
	public function GetDetail($where,$page,$per_page,$sort_field,$sort_dir)
	{
		
		$page = !isset($page) || !is_numeric($page)?1:$this->db->quote($page);
		$start = ($page-1)*$per_page;
		$data = $this->db->fetchAll("SELECT SQL_CALC_FOUND_ROWS `country`,id,`value`,`name`,text_value,`type`,year_date,COUNT(id) AS `quantity` FROM $this->_table
				WHERE $where GROUP BY `country`,`value`
				ORDER BY `$sort_field` $sort_dir LIMIT $start,$per_page ");
		
		$found =  $this->db->fetchRow('SELECT FOUND_ROWS() AS q');
		return array('data'=>$data,'count'=>$found->q);
		
	}
	
	
}

