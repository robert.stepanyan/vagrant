<?php
class Model_VipSpots extends Cubix_Model
{
	protected $_table = 'vip_hardcodes';
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				UNIX_TIMESTAMP(vh.date_from) as date_from, UNIX_TIMESTAMP(vh.date_to) as date_to, vh.id, vh.escort_id, 
				vh.place, cities.title_en as city, countries.title_en as country
			FROM vip_hardcodes vh
			LEFT JOIN countries ON countries.id = vh.country_id
			LEFT JOIN cities ON cities.id = vh.city_id
			WHERE 1
		';
		
		$countSql = '
			SELECT FOUND_ROWS()
		';
		
		$where = '';

		if ( strlen($filter['escort_id']) ) {
			$where .= self::quote('AND vh.escort_id = ?', $filter['escort_id']);
		}

		if ( strlen($filter['country']) && strlen($filter['city']) ) {
			$where .= self::quote('AND vh.city_id = ?', $filter['city']);
		} elseif (strlen($filter['country'])) {
			$where .= self::quote('AND vh.country_id = ?', $filter['country']);
		}
			
		$sql .= $where;
		
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$data = parent::_fetchAll($sql);

		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return $data;
	}	

	public function remove($id)
	{
		parent::getAdapter()->delete($this->_table, parent::quote('id = ?', $id));
	}

	public function checkEscortId($escort_id)
	{
		$result = parent::getAdapter()->fetchOne('SELECT COUNT(*) FROM vip_hardcodes WHERE escort_id = ?', $escort_id);
		return $result;
	}

	public function checkPlaceAndCity($place, $city)
	{
		$result = parent::getAdapter()->fetchOne('SELECT COUNT(*) FROM vip_hardcodes WHERE place = ? AND city_id = ?', array($place, $city));
		return $result;
	}
	
	public function save($data)
	{
		$m = new Model_Escorts();
		$agency_id = $m->getAgencyId($data['escort_id']);

		$fields = array(
			'escort_id' => $data['escort_id'],
			'place' => $data['place'],
			'country_id' => $data['country'],
			'city_id' => $data['city'] ? $data['city'] : NULL,
			'date_from' => date('Y-m-d H:i:s', $data['date_from']),
			'date_to' => $data['date_to'] ? date('Y-m-d H:i:s', $data['date_to']) : NULL,
			'agency_id' => $agency_id
		);				

		parent::getAdapter()->insert($this->_table, $fields);				
	}
	
}
