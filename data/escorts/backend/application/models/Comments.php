<?php
class Model_Comments extends Cubix_Model
{
	protected $_table = 'comments';

	// comments status
	const COMMENT_ACTIVE = 1;
	const COMMENT_NOT_APPROVED = -3;
	const COMMENT_DISABLED = -4;

	const SITEADMIN_USERNAME = 'siteadmin';

	public function getById($id)
	{
		$sql = '
			SELECT
				c.id, u.username, u.id AS user_id, e.showname, e.id AS escort_id, UNIX_TIMESTAMP(c.time) AS time, c.is_reply_to, c.status, c.message, c.thumbup_count,
				c.thumbdown_count, m.suspicious_reason, c.is_suspicious, c.disable_type, c.disabled_reason
			FROM comments c
			INNER JOIN users u ON c.user_id = u.id
			LEFT JOIN members m ON m.user_id = u.id
			INNER JOIN escorts e ON e.id = c.escort_id
			WHERE 1 AND c.id = ?
		';

		return parent::_fetchRow($sql, array($id));
	}
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$fields = "";
		$join = "";

		if ( $filter['has_active_package'] ) {
			$join .= 'INNER JOIN order_packages ord_p ON ord_p.escort_id = e.id AND ord_p.status = 2 AND  ord_p.application_id = ' . Cubix_Application::getId();
		}

		if ( in_array(Cubix_Application::getId(), array(APP_6A, APP_EF)) ) {
			$fields = " m.rating, ";
		}
		
		if ( in_array(Cubix_Application::getId(), array(APP_A6)) ) {
			$fields .= " c.is_mobile, ";
		}

		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				c.id, u.username, 
				e.showname,
				e.id AS escort_id,
				UNIX_TIMESTAMP(c.time) AS time,
				c.is_reply_to,
				c.status,
				c.message,
				c.thumbup_count,
				c.thumbdown_count,
				m.is_suspicious AS is_m_suspicious, 
				c.user_type,
				m.suspicious_reason,
				IF(u.date_registered >= DATE_ADD(NOW(), INTERVAL -72 HOUR), 1, 0) AS new_member,
				u.id AS user_id,
				u.email,
				m.is_premium,
				m.member_comment,
				' . $fields . '
				c.is_suspicious
			FROM comments c
			INNER JOIN escorts e ON e.id = c.escort_id
			INNER JOIN users u ON c.user_id = u.id
			LEFT JOIN members m ON m.user_id = u.id
			' . $join . '
			WHERE 1
		';
		
		$countSql = '
			SELECT FOUND_ROWS()
			
			/*SELECT COUNT(DISTINCT(c.id))
			FROM comments c
			INNER JOIN users u ON c.user_id = u.id
			INNER JOIN escorts e ON e.id = c.escort_id
			LEFT JOIN order_packages ord_p ON ord_p.escort_id = e.id  AND  ord_p.application_id = ' . Cubix_Application::getId().'
			WHERE 1*/
		';
		
		$where = '';

		if ( strlen($filter['application_id']) ) {
			$where .= self::quote('AND u.application_id = ?', $filter['application_id']);
		}
				
		if ( strlen($filter['showname']) ) {
			if ($filter['mode'] == 'fixed')
				if ( $filter['escort_id'] ) 
					$where .= self::quote(' AND e.id = ? ', $filter['escort_id']);
				else
					$where .= self::quote(' AND e.showname = ?', $filter['showname']);
			else
				$where .= self::quote(' AND e.showname LIKE ? ', $filter['showname'] . '%');
		}

		if ( strlen($filter['username']) ) {
			$where .= self::quote(' AND u.username LIKE ?', $filter['username'] . '%');
		}
				
		if ( $filter['escort_id_f'] ) {
			$where .= self::quote('AND e.id = ?', $filter['escort_id_f']);
		}
		
		if ( $filter['status'] ) {
			if( $filter['status'] == -99 ){
				$where .= ' AND c.has_bl = 1 ';
			}
			else{
				$where .= self::quote('AND c.status = ?', $filter['status']);
			}
		}
		if ( $filter['is_active_escort'] ) {
			$where .= ' AND e.status = '.Model_Escorts::ESCORT_STATUS_ACTIVE;
		}

		if ( $filter['is_active_escort'] ) {
			$where .= ' AND e.status = '.Model_Escorts::ESCORT_STATUS_ACTIVE;
		}
	
		if ( $filter['has_active_package'] ) {
			if(Cubix_Application::getId() == 1){
					$where .= '  AND ord_p.package_id != 100 ';
			}
			elseif(Cubix_Application::getId() == 30 || Cubix_Application::getId() == 11){
				$where .= '  AND ord_p.package_id != 9 ';
			}
			else{
				$where .= '  AND ord_p.package_id != 97 ';
			}
		}
		
		if ( $filter['is_suspicious'] ) {
			$where .= self::quote(' AND c.is_suspicious = ? ', $filter['is_suspicious']);
		}

		if ( $filter['is_mobile'] ) {
			$where .= self::quote(' AND c.is_mobile = ? ', $filter['is_mobile']);
		}
		
		if ( isset($filter['comment']) && $filter['comment']  ) {
			$where .= ' AND  c.message LIKE "%'.$filter['comment'].'%" ';
		}
		
		if ( $filter['date_from'] && $filter['date_to'] ) {
				if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) ) {
					$where .= self::quote( " AND DATE(c.time) = DATE(FROM_UNIXTIME(?))" , $filter['date_from']);
				}
			}
		if ( $filter['date_from'] ) {
				$where .= self::quote(" AND DATE(c.time) >= DATE(FROM_UNIXTIME(?)) ", $filter['date_from']);
			}

		if ( $filter['date_to'] ) {
			$where .= self::quote(" AND DATE(c.time) <= DATE(FROM_UNIXTIME(?)) ", $filter['date_to']);
		}
		
		if ( isset($filter['disable_type']) && $filter['disable_type']  ) {
			$where .= ' AND c.status = ' . COMMENT_DISABLED . ' AND c.disable_type = ' . $filter['disable_type'];
		}
			
		$sql .= $where;
		//$countSql .= $where;
		
		$sql .= '
			GROUP BY c.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$data = parent::_fetchAll($sql);

		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return $data;
	}	
	
	public function save($data)
	{
		if (isset($data['id']) && $data['id'] != '')
		{
			$fields = array(
				'status' => $data['status'],
				'message' => $data['comment']
			);				

			parent::getAdapter()->update($this->_table, $fields, parent::quote('id = ?', $data['id']));
			
			$com_date = parent::getAdapter()->fetchOne('SELECT time FROM comments WHERE id = ?', $data['id']);
			
			if ($data['status'] == COMMENT_ACTIVE)
				Cubix_AlertEvents::notify($data['escort_id'], ALERT_ME_NEW_COMMENT, array('id' => $data['id'], 'set_date' => $com_date));
		}
	}
	public function reply($data)
	{ 
		$siteadmin_id = $this->getUserIdByUsername(self::SITEADMIN_USERNAME);
		$fields = array(
			'user_id' => $siteadmin_id,
			'escort_id' => $data['escort_id'],
			'time' => new Zend_Db_Expr('NOW()'),
			'is_reply_to' => $data['id'],
			'status' => self::COMMENT_ACTIVE,
			'message' => $data['reply']
		);
		parent::getAdapter()->insert($this->_table, $fields);
		
	}
	
	public function remove($id)
	{
		parent::getAdapter()->delete($this->_table, parent::quote('id = ?', $id));
	}

	public function getUserCommentsCount($user_id)
	{
		return parent::getAdapter()->fetchOne('SELECT COUNT(*) FROM comments WHERE user_id = ?', array($user_id));
	}

	public function getUserReviewsCount($user_id)
	{
		return parent::getAdapter()->fetchOne('SELECT COUNT(*) FROM reviews WHERE  is_deleted = 0 AND   user_id = ?', array($user_id));
	}
	public function getUserIdByUsername($username)
	{
		return parent::getAdapter()->fetchOne('SELECT id FROM users WHERE username LIKE ?',array($username));
	}

	public function fakeByPhone($user_id)
	{
		$db = parent::getAdapter();

		$code_count = $db->fetchOne('SELECT COUNT(c.id) FROM comments c INNER JOIN escort_profiles_v2 e_p ON c.escort_id = e_p.escort_id WHERE MID(e_p.contact_phone_parsed, 1, 3) = ? AND c.user_id = ?', array('007', $user_id));
		$all_count = $db->fetchOne('SELECT COUNT(id) FROM comments WHERE user_id = ?', $user_id);

		if ($all_count > 1 && $code_count == $all_count)
			return true;
		else
			return false;
	}
	
	public function disabledReason($data)
	{
		parent::getAdapter()->update($this->_table, array(
			'disabled_reason' => $data['reason'],
			'disable_type' => $data['disable_type'],
			'status' => COMMENT_DISABLED
		), parent::quote('id = ?', $data['id']));
	}
}
