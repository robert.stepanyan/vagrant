<?php

class Model_BlacklistedWords extends Cubix_Model
{
	const BL_TYPE_FEEDBACK = 1;			 	 //  00000000001
	const BL_TYPE_ABOUT = 2;		     	 //  00000000010
	const BL_TYPE_PRIVATE_MESSAGING = 4; 	 //  00000000100
	const BL_TYPE_INSTANT_MESSAGES = 8;	 	 //  00000001000
	const BL_TYPE_BUBBLE_TEXT = 16;      	 //  00000010000 
	const BL_TYPE_CLASSIFIED_ADS = 32;   	 //  00000100000
	const BL_TYPE_OTHER = 64;			 	 //  00001000000
	const BL_TYPE_SPECIAL_CHAR = 128;    	 //  00010000000
	const BL_TYPE_SLOGAN  = 256;         	 //  00100000000
	const BL_TYPE_ADDITIONAL_SERVICES = 512; //  01000000000
	const BL_TYPE_SHOWNAME = 1024; 			 //  10000000000
	
	const BL_SEARCH_TYPE_CONT = 1;
	const BL_SEARCH_TYPE_EXACT = 2;
	
	public $type = '';
	public $message = '';
	public function __construct( $type = 1)
	{
		$this->type = $type;
	}
	
    public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT id, name
			FROM blacklisted_words
			WHERE 1
		';

		$countSql = '
			SELECT COUNT(DISTINCT(id))
			FROM blacklisted_words
			WHERE 1
		';
		
		if ( strlen($filter['name']) > 0 ) {
			$sql .= self::quote('AND name LIKE ?', '%' . $filter['name'] . '%');
			$countSql .= self::quote('AND name LIKE ?', '%' . $filter['name'] . '%');
		}
		
		if ( strlen($filter['type']) ) {
			$sql .= self::quote('AND types & ? ', intval($filter['type']));
			$countSql .= self::quote('AND types & ? ', intval($filter['type']));
		}
		
		$sql .= '
			GROUP BY id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$count = intval($this->getAdapter()->fetchOne($countSql));
		return parent::_fetchAll($sql);
	}

	public function get($id)
	{
		return self::_fetchRow('
			SELECT * FROM blacklisted_words WHERE id = ?
		', array($id));
	}

    

    public function save( $data, $id = null, $bl_countries = array() ){
		
    	
        if ( ! is_null($id) ) {
			self::getAdapter()->update('blacklisted_words', $data, self::quote('id = ?', $id));
			$bl_id = $id;
		}
		else {
			self::getAdapter()->insert('blacklisted_words', $data);
			$bl_id  = self::getAdapter()->lastInsertId();
		}
		
		if(Cubix_Application::getId() == APP_ED) {
			$res = $this->get_bl_words_countries($bl_id);
			if( empty( $res ) ){
				$this->insert_bl_words_countries( $bl_id, $bl_countries );
			}else{
				$this->remove_bl_words_countries( $bl_id);
				$this->insert_bl_words_countries( $bl_id, $bl_countries );
			}
		}		
    }

    public function get_bl_words_countries( $bl_id ){
       return parent::_fetchAll('SELECT * FROM blacklisted_words_countries WHERE bl_word_id = ?',array($bl_id));
    }

    public function insert_bl_words_countries($bl_id, $bl_countries = array()){

        if ( ! empty( $bl_countries  ) ) {
			foreach ($bl_countries as $country) {
				$bl_coutnries_data['bl_word_id'] =  $bl_id;
				$bl_coutnries_data['bl_country'] =  $country;
				self::getAdapter()->insert('blacklisted_words_countries', $bl_coutnries_data);
			}
		}

    }

    public function remove_bl_words_countries( $bl_id ){
        return self::getAdapter()->delete('blacklisted_words_countries', self::getAdapter()->quoteInto('bl_word_id = ?', $bl_id));
    }

    public function delete($id = array()){
        self::getAdapter()->delete('blacklisted_words', self::getAdapter()->quoteInto('id = ?', $id));
        if(Cubix_Application::getId() == APP_ED) {
        	$this->remove_bl_words_countries( $id);
        }
    }


    public function checkWords($text, $type=self::BL_TYPE_FEEDBACK )
	{
        $sql = '
			SELECT id, name, search_type
			FROM blacklisted_words
			WHERE types & ?';
        $res = array();
		$symbols = array("\r\n", "\n", "\r");
        $wordList = parent::_fetchAll($sql, array($type));
		$text = strtolower(str_replace($symbols, "",$text));
        if( count($wordList) > 0 ){
            foreach ( $wordList as $word ){
                if(strlen(trim($word->name)) > 0)
				{
					
					$search_words = strtolower(str_replace( $symbols, "", $word->name));
					if($word->search_type == Model_BlacklistedWords::BL_SEARCH_TYPE_CONT){
						$result = stripos($text, $search_words);
					}
					else{
						
						$result = preg_match("/(\W+|^)".preg_quote($search_words, '/')."(\W+|$)/i", $text);
						$result = $result == 0 ? false : true; 
					}
					
					if ($result !== false) {

						$res[] = $word->name;

					}
				}
            }
			
            $this->message = implode(',', $res);
        }
        
        return $res;
    }
	
	public function getWords()
	{
        return $this->message;
    }
	
	public function replaceOldBlWords($word, $search_type, $types, $countries  = array())
	{

		if(!empty($countries)){
			foreach($countries as $country){
				$countryids .= $country.',';
			}

			$countryids = rtrim($countryids, ',');
			$countrysql = ' AND e.country_id in ('.$countryids.')';	
		}else{
			$countrysql = '';
		}

		foreach($types as $type){
			if($type == self::BL_TYPE_ABOUT){
				$this->_replaceAboutMe($word, $search_type, $countrysql);
			}
			if($type == self::BL_TYPE_OTHER){
				if(Cubix_Application::getId() == APP_A6) {
					$this->_replacePhoneInstrOther($word, $search_type); 	 
					$this->_replaceAddressAdditionalInfo($word, $search_type); 	 
					$this->_replaceIncallOther($word, $search_type); 	 
					$this->_replaceOutcallOther($word, $search_type); 	 
					$this->_replaceSpecialRates($word, $search_type); 	 
					$this->_replaceTourPhone($word, $search_type);  	 
					$this->_replaceHappyHourMotto($word, $search_type);  
				} else {
					$this->_replaceOther($word, $search_type, $countrysql); // phone_instr_other
				}
			}
			if($type == self::BL_TYPE_SPECIAL_CHAR){
				$this->_replaceSpecialChar($word, $search_type, $countrysql);
			}
			if($type == self::BL_TYPE_SLOGAN){
				$this->_replaceSlogan($word, $search_type, $countrysql);
			}	
			if($type == self::BL_TYPE_ADDITIONAL_SERVICES){
				$this->_replaceAdditionalServices($word, $search_type, $countrysql);
			}	
			if($type == self::BL_TYPE_SHOWNAME){
				$this->_replaceShowname($word, $search_type, $countrysql);
			}		
		}
		
	}
	
	private function _replaceAboutMe($word, $search_type, $countrysql){

		$db = $this->getAdapter();
		$langs = Cubix_I18n::getLangs();
		$replace_text = str_repeat("*", strlen($word));

		foreach($langs as $lang_id){
			$sql = '
				SELECT e.id, ep.about_'.$lang_id.' AS about_me
				FROM escort_profiles_v2 ep
				INNER JOIN escorts e ON e.id = ep.escort_id '.$countrysql.'		 	
				WHERE  NOT e.status & '.Model_EscortsV2::ESCORT_STATUS_DELETED ;

			if($search_type == Model_BlacklistedWords::BL_SEARCH_TYPE_CONT){
				$sql .= $db->quoteInto(' AND about_'.$lang_id.' LIKE ?', '%' . $word . '%');
			}
			else{
				$sql .= $db->quoteInto(' AND about_'.$lang_id.' RLIKE ?', '[[:<:]]' . $word . '[[:>:]]');
			}
			$bl_texts = $db->fetchAll($sql);
			//var_dump($bl_texts);die;
			foreach($bl_texts as $item){
				if($search_type == self::BL_SEARCH_TYPE_CONT){
					$pattern = '/' . preg_quote($word, '/') . '/i';
					$about_me_text = preg_replace($pattern, $replace_text, $item->about_me);
				}
				else{
					$pattern = "/(\W+|^)" . preg_quote($word, '/') . '(\W+|$)/i';
					$about_me_text = preg_replace($pattern, '$1'.$replace_text.'$2', $item->about_me);
				}
				$data = array('about_'.$lang_id => $about_me_text);
				
				$db->update('escort_profiles_v2',$data, self::quote('escort_id = ?', $item->id));
				$this->_updateRevision($item->id, $data);
			}
		}
		
	}
	
	private function _replaceOther($word, $search_type, $countrysql){
		$db = $this->getAdapter();
		$replace_text = str_repeat("*", strlen($word));
		$sql = '
			SELECT e.id, ep.phone_instr_other
			FROM escort_profiles_v2 ep
			INNER JOIN escorts e ON e.id = ep.escort_id '.$countrysql.'		 	
			WHERE  NOT e.status & '.Model_EscortsV2::ESCORT_STATUS_DELETED ;

		if($search_type == Model_BlacklistedWords::BL_SEARCH_TYPE_CONT){
			$sql .= $db->quoteInto(' AND ep.phone_instr_other LIKE ?', '%' . $word . '%');
		}
		else{
			$sql .= $db->quoteInto(' AND ep.phone_instr_other RLIKE ?', '[[:<:]]' . $word . '[[:>:]]');
		}
		$bl_texts = $db->fetchAll($sql);
		//var_dump($bl_texts);die;
		foreach($bl_texts as $item){
			if($search_type == self::BL_SEARCH_TYPE_CONT){
				$pattern = '/' . preg_quote($word, '/') . '/i';
				$new_text = preg_replace($pattern, $replace_text, $item->phone_instr_other);
			}
			else{
				$pattern = "/(\W+|^)" . preg_quote($word, '/') . '(\W+|$)/i';
				$new_text = preg_replace($pattern, '$1'.$replace_text.'$2', $item->phone_instr_other);
			}
			$data = array('phone_instr_other' => $new_text);
			$db->update('escort_profiles_v2',$data, self::quote('escort_id = ?', $item->id));
			$this->_updateRevision($item->id, $data);
		}
			
	}

	private function _replacePhoneInstrOther($word, $search_type){
		$db = $this->getAdapter();
		$replace_text = str_repeat("*", strlen($word));
		$sql = '
			SELECT e.id, ep.phone_instr_other
			FROM escort_profiles_v2 ep
			INNER JOIN escorts e ON e.id = ep.escort_id 		 	
			WHERE  NOT e.status & '.Model_EscortsV2::ESCORT_STATUS_DELETED;

		if($search_type == Model_BlacklistedWords::BL_SEARCH_TYPE_CONT){
			$sql .= $db->quoteInto(' AND ep.phone_instr_other LIKE ?', '%' . $word . '%');
		}
		else{
			$sql .= $db->quoteInto(' AND ep.phone_instr_other RLIKE ?', '[[:<:]]' . $word . '[[:>:]]');
		}
		$bl_texts = $db->fetchAll($sql);
		// var_dump($bl_texts);die;
		foreach($bl_texts as $item){
			if($search_type == self::BL_SEARCH_TYPE_CONT){
				$pattern = '/' . preg_quote($word, '/') . '/i';
				$new_text_phone_other = preg_replace($pattern, $replace_text, $item->phone_instr_other);
			}
			else{
				$pattern = "/(\W+|^)" . preg_quote($word, '/') . '(\W+|$)/i';
				$new_text_phone_other = preg_replace($pattern, '$1'.$replace_text.'$2', $item->phone_instr_other);
			}
			$data = array('phone_instr_other' => $new_text_phone_other);
			$db->update('escort_profiles_v2',$data, self::quote('escort_id = ?', $item->id));
			$this->_updateRevision($item->id, $data);
		}	
	}

	private function _replaceAddressAdditionalInfo($word, $search_type){
		$db = $this->getAdapter();
		$replace_text = str_repeat("*", strlen($word));
		$sql = '
			SELECT e.id, ep.address_additional_info
			FROM escort_profiles_v2 ep
			INNER JOIN escorts e ON e.id = ep.escort_id 		 	
			WHERE  NOT e.status & '.Model_EscortsV2::ESCORT_STATUS_DELETED;

		if($search_type == Model_BlacklistedWords::BL_SEARCH_TYPE_CONT){
			$sql .= $db->quoteInto(' AND ep.address_additional_info LIKE ?', '%' . $word . '%');
		}
		else{
			$sql .= $db->quoteInto(' AND ep.address_additional_info RLIKE ?', '[[:<:]]' . $word . '[[:>:]]');
		}
		$bl_texts = $db->fetchAll($sql);
		// var_dump($bl_texts);die;
		foreach($bl_texts as $item){
			if($search_type == self::BL_SEARCH_TYPE_CONT){
				$pattern = '/' . preg_quote($word, '/') . '/i';
				$new_text_address_other = preg_replace($pattern, $replace_text, $item->address_additional_info);
			}
			else{
				$pattern = "/(\W+|^)" . preg_quote($word, '/') . '(\W+|$)/i';
				$new_text_address_other = preg_replace($pattern, '$1'.$replace_text.'$2', $item->address_additional_info);
			}
			$data = array('address_additional_info' => $new_text_address_other);
			$db->update('escort_profiles_v2',$data, self::quote('escort_id = ?', $item->id));
			$this->_updateRevision($item->id, $data);
		}
			
	}

	private function _replaceIncallOther($word, $search_type){
		$db = $this->getAdapter();
		$replace_text = str_repeat("*", strlen($word));
		$sql = '
			SELECT e.id, ep.incall_other
			FROM escort_profiles_v2 ep
			INNER JOIN escorts e ON e.id = ep.escort_id 		 	
			WHERE  NOT e.status & '.Model_EscortsV2::ESCORT_STATUS_DELETED;

		if($search_type == Model_BlacklistedWords::BL_SEARCH_TYPE_CONT){
			$sql .= $db->quoteInto(' AND ep.incall_other LIKE ?', '%' . $word . '%');
		}
		else{
			$sql .= $db->quoteInto(' AND ep.incall_other RLIKE ?', '[[:<:]]' . $word . '[[:>:]]');
		}
		$bl_texts = $db->fetchAll($sql);
		// var_dump($bl_texts);die;
		foreach($bl_texts as $item){
			if($search_type == self::BL_SEARCH_TYPE_CONT){
				$pattern = '/' . preg_quote($word, '/') . '/i';
				$new_text_incall_other = preg_replace($pattern, $replace_text, $item->incall_other);
			}
			else{
				$pattern = "/(\W+|^)" . preg_quote($word, '/') . '(\W+|$)/i';
				$new_text_incall_other = preg_replace($pattern, '$1'.$replace_text.'$2', $item->incall_other);
			}
			$data = array('incall_other' => $new_text_incall_other);
			$db->update('escort_profiles_v2',$data, self::quote('escort_id = ?', $item->id));
			$this->_updateRevision($item->id, $data);
		}
			
	}

	private function _replaceOutcallOther($word, $search_type){
		$db = $this->getAdapter();
		$replace_text = str_repeat("*", strlen($word));
		$sql = '
			SELECT e.id, ep.outcall_other
			FROM escort_profiles_v2 ep
			INNER JOIN escorts e ON e.id = ep.escort_id 		 	
			WHERE  NOT e.status & '.Model_EscortsV2::ESCORT_STATUS_DELETED;

		if($search_type == Model_BlacklistedWords::BL_SEARCH_TYPE_CONT){
			$sql .= $db->quoteInto(' AND ep.outcall_other LIKE ?', '%' . $word . '%');
		}
		else{
			$sql .= $db->quoteInto(' AND ep.outcall_other RLIKE ?', '[[:<:]]' . $word . '[[:>:]]');
		}
		$bl_texts = $db->fetchAll($sql);
		// var_dump($bl_texts);die;
		foreach($bl_texts as $item){
			if($search_type == self::BL_SEARCH_TYPE_CONT){
				$pattern = '/' . preg_quote($word, '/') . '/i';
				$new_text_outcall_other = preg_replace($pattern, $replace_text, $item->outcall_other);
			}
			else{
				$pattern = "/(\W+|^)" . preg_quote($word, '/') . '(\W+|$)/i';
				$new_text_outcall_other = preg_replace($pattern, '$1'.$replace_text.'$2', $item->outcall_other);
			}
			$data = array('outcall_other' => $new_text_outcall_other);
			$db->update('escort_profiles_v2',$data, self::quote('escort_id = ?', $item->id));
			$this->_updateRevision($item->id, $data);
		}
			
	}

	private function _replaceSpecialRates($word, $search_type){
		$db = $this->getAdapter();
		$replace_text = str_repeat("*", strlen($word));
		$sql = '
			SELECT e.id, ep.special_rates
			FROM escort_profiles_v2 ep
			INNER JOIN escorts e ON e.id = ep.escort_id 		 	
			WHERE  NOT e.status & '.Model_EscortsV2::ESCORT_STATUS_DELETED;

		if($search_type == Model_BlacklistedWords::BL_SEARCH_TYPE_CONT){
			$sql .= $db->quoteInto(' AND ep.special_rates LIKE ?', '%' . $word . '%');
		}
		else{
			$sql .= $db->quoteInto(' AND ep.special_rates RLIKE ?', '[[:<:]]' . $word . '[[:>:]]');
		}
		$bl_texts = $db->fetchAll($sql);
		// var_dump($bl_texts);die;
		foreach($bl_texts as $item){
			if($search_type == self::BL_SEARCH_TYPE_CONT){
				$pattern = '/' . preg_quote($word, '/') . '/i';
				$new_text_special_rates = preg_replace($pattern, $replace_text, $item->special_rates);
			}
			else{
				$pattern = "/(\W+|^)" . preg_quote($word, '/') . '(\W+|$)/i';
				$new_text_special_rates = preg_replace($pattern, '$1'.$replace_text.'$2', $item->special_rates);
			}
			$data = array('special_rates' => $new_text_special_rates);
			$db->update('escort_profiles_v2',$data, self::quote('escort_id = ?', $item->id));
			$this->_updateRevision($item->id, $data);
		}
			
	}

	private function _replaceTourPhone($word, $search_type){
		$db = $this->getAdapter();
		$replace_text = str_repeat("*", strlen($word));
		$sql = '
			SELECT t.id, t.phone, t.escort_id
			FROM tours t
			INNER JOIN escorts e ON e.id = t.escort_id 		 	
			WHERE  NOT e.status & '.Model_EscortsV2::ESCORT_STATUS_DELETED ;

		if($search_type == Model_BlacklistedWords::BL_SEARCH_TYPE_CONT){
			$sql .= $db->quoteInto(' AND t.phone LIKE ?', '%' . $word . '%');
		}
		else{
			$sql .= $db->quoteInto(' AND t.phone RLIKE ?', '[[:<:]]' . $word . '[[:>:]]');
		}
		$bl_texts = $db->fetchAll($sql);
		//var_dump($bl_texts);die;
		foreach($bl_texts as $item){
			if($search_type == self::BL_SEARCH_TYPE_CONT){
				$pattern = '/' . preg_quote($word, '/') . '/i';
				$new_text = preg_replace($pattern, $replace_text, $item->phone);
			}
			else{
				$pattern = "/(\W+|^)" . preg_quote($word, '/') . '(\W+|$)/i';
				$new_text = preg_replace($pattern, '$1'.$replace_text.'$2', $item->phone);
			}
			$data = array('phone' => $new_text);
			$db->update('tours',$data, self::quote('id = ?', $item->id));
			$this->_updateRevision($item->escort_id, $data);
		}
			
	}

	private function _replaceHappyHourMotto($word, $search_type){
		$db = $this->getAdapter();
		$replace_text = str_repeat("*", strlen($word));
		$sql = '
			SELECT e.id, e.hh_motto
			FROM escorts e
			WHERE  NOT e.status & '.Model_EscortsV2::ESCORT_STATUS_DELETED ;

		if($search_type == Model_BlacklistedWords::BL_SEARCH_TYPE_CONT){
			$sql .= $db->quoteInto(' AND e.hh_motto LIKE ?', '%' . $word . '%');
		}
		else{
			$sql .= $db->quoteInto(' AND e.hh_motto RLIKE ?', '[[:<:]]' . $word . '[[:>:]]');
		}
		$bl_texts = $db->fetchAll($sql);
		//var_dump($bl_texts);die;
		foreach($bl_texts as $item){
			if($search_type == self::BL_SEARCH_TYPE_CONT){
				$pattern = '/' . preg_quote($word, '/') . '/i';
				$new_text = preg_replace($pattern, $replace_text, $item->hh_motto);
			}
			else{
				$pattern = "/(\W+|^)" . preg_quote($word, '/') . '(\W+|$)/i';
				$new_text = preg_replace($pattern, '$1'.$replace_text.'$2', $item->hh_motto);
			}
			$data = array('hh_motto' => $new_text);
			$db->update('escorts', $data , self::quote('id = ?', $item->id));
			$this->_updateRevision($item->id, $data);
		}
	}
	
	private function _replaceSpecialChar($word, $search_type, $countrysql){
		$db = $this->getAdapter();
		$replace_text = str_repeat("*", strlen($word));
		$sql = '
			SELECT e.id, ep.characteristics
			FROM escort_profiles_v2 ep
			INNER JOIN escorts e ON e.id = ep.escort_id '.$countrysql.'		 	
			WHERE  NOT e.status & '.Model_EscortsV2::ESCORT_STATUS_DELETED ;

		if($search_type == Model_BlacklistedWords::BL_SEARCH_TYPE_CONT){
			$sql .= $db->quoteInto(' AND ep.characteristics LIKE ?', '%' . $word . '%');
		}
		else{
			$sql .= $db->quoteInto(' AND ep.characteristics RLIKE ?', '[[:<:]]' . $word . '[[:>:]]');
		}
		$bl_texts = $db->fetchAll($sql);
		//var_dump($bl_texts);die;
		foreach($bl_texts as $item){
			if($search_type == self::BL_SEARCH_TYPE_CONT){
				$pattern = '/' . preg_quote($word, '/') . '/i';
				$new_text = preg_replace($pattern, $replace_text, $item->characteristics);
			}
			else{
				$pattern = "/(\W+|^)" . preg_quote($word, '/') . '(\W+|$)/i';
				$new_text = preg_replace($pattern, '$1'.$replace_text.'$2', $item->characteristics);
			}
			$data = array('characteristics' => $new_text);
			$db->update('escort_profiles_v2',$data, self::quote('escort_id = ?', $item->id));
			$this->_updateRevision($item->id, $data);
		}
			
	}
	
	private function _replaceSlogan($word, $search_type){
		$db = $this->getAdapter();
		$replace_text = str_repeat("*", strlen($word));
		$sql = '
			SELECT e.id, e.slogan
			FROM escorts e
			WHERE  NOT e.status & '.Model_EscortsV2::ESCORT_STATUS_DELETED ;

		if($search_type == Model_BlacklistedWords::BL_SEARCH_TYPE_CONT){
			$sql .= $db->quoteInto(' AND e.slogan LIKE ?', '%' . $word . '%');
		}
		else{
			$sql .= $db->quoteInto(' AND e.slogan RLIKE ?', '[[:<:]]' . $word . '[[:>:]]');
		}
		$bl_texts = $db->fetchAll($sql);
		//var_dump($bl_texts);die;
		foreach($bl_texts as $item){
			if($search_type == self::BL_SEARCH_TYPE_CONT){
				$pattern = '/' . preg_quote($word, '/') . '/i';
				$new_text = preg_replace($pattern, $replace_text, $item->slogan);
			}
			else{
				$pattern = "/(\W+|^)" . preg_quote($word, '/') . '(\W+|$)/i';
				$new_text = preg_replace($pattern, '$1'.$replace_text.'$2', $item->slogan);
			}
			$data = array('slogan' => $new_text);
			$db->update('escorts', $data , self::quote('id = ?', $item->id));
			$this->_updateRevision($item->id, $data);
			
		}
			
	}

	private function _replaceAdditionalServices($word, $search_type, $countrysql){
		$db = $this->getAdapter();
		$langs = Cubix_I18n::getLangs();

		$replace_text = str_repeat("*", strlen($word));
		foreach($langs as $lang_id){
			$sql = '
				SELECT e.id, ep.additional_service_'.$lang_id.' AS additional_service
				FROM escort_profiles_v2 ep
				INNER JOIN escorts e ON e.id = ep.escort_id '.$countrysql.'	 	
				WHERE  NOT e.status & '.Model_EscortsV2::ESCORT_STATUS_DELETED ;

			if($search_type == Model_BlacklistedWords::BL_SEARCH_TYPE_CONT){
				$sql .= $db->quoteInto(' AND additional_service_'.$lang_id.' LIKE ?', '%' . $word . '%');
			}
			else{
				$sql .= $db->quoteInto(' AND additional_service_'.$lang_id.' RLIKE ?', '[[:<:]]' . $word . '[[:>:]]');
			}
			$bl_texts = $db->fetchAll($sql);
			// var_dump($bl_texts);die;
			foreach($bl_texts as $item){
				if($search_type == self::BL_SEARCH_TYPE_CONT){
					$pattern = '/' . preg_quote($word, '/') . '/i';
					$additional_service_text = preg_replace($pattern, $replace_text, $item->additional_service);
				}
				else{
					$pattern = "/(\W+|^)" . preg_quote($word, '/') . '(\W+|$)/i';
					$additional_service_text = preg_replace($pattern, '$1'.$replace_text.'$2', $item->additional_service);
				}
				$data = array('additional_service_'.$lang_id => $additional_service_text);
				
				$db->update('escort_profiles_v2',$data, self::quote('escort_id = ?', $item->id));
				$this->_updateRevision($item->id, $data);
			}
		}
		
	}

	private function _replaceShowname($word, $search_type, $countrysql){
		$db = $this->getAdapter();
		$replace_text = str_repeat("*", strlen($word));
		$sql = '
			SELECT e.id, e.showname
			FROM escorts e
			WHERE  NOT e.status & '.Model_EscortsV2::ESCORT_STATUS_DELETED .$countrysql ;

		if($search_type == Model_BlacklistedWords::BL_SEARCH_TYPE_CONT){
			$sql .= $db->quoteInto(' AND e.showname LIKE ?', '%' . $word . '%');
		}
		else{
			$sql .= $db->quoteInto(' AND e.showname RLIKE ?', '[[:<:]]' . $word . '[[:>:]]');
		}
		$bl_texts = $db->fetchAll($sql);
		// var_dump($bl_texts);die;
		foreach($bl_texts as $item){
			if($search_type == self::BL_SEARCH_TYPE_CONT){
				$pattern = '/' . preg_quote($word, '/') . '/i';
				$new_text = preg_replace($pattern, $replace_text, $item->showname);
			}
			else{
				$pattern = "/(\W+|^)" . preg_quote($word, '/') . '(\W+|$)/i';
				$new_text = preg_replace($pattern, '$1'.$replace_text.'$2', $item->showname);
			}
			$data = array('showname' => $new_text);
			$db->update('escorts', $data , self::quote('id = ?', $item->id));
			$this->_updateRevision($item->id, $data);
			
		}
			
	}
	
	//Revisions update
	private function _updateRevision($escort_id, $data)
	{	
		$db = $this->getAdapter();
		$revision_data = $db->fetchRow('SELECT data, revision FROM profile_updates_v2 WHERE escort_id = ? ORDER BY revision DESC LIMIT 1', array($escort_id));

		if ( $revision_data ) {
			$revision = $revision_data->revision;
			$revision_data = unserialize($revision_data->data);
			$revision_data = array_merge($revision_data, $data);
			$db->update('profile_updates_v2', array('data' => serialize($revision_data)), array($db->quoteInto('escort_id = ?', $escort_id), $db->quoteInto('revision = ?', $revision)));

		}
		else {
			$revision += 1;
			$db->insert('profile_updates_v2', array(
				'escort_id' => $escort_id,
				'revision' => $revision,
				'date_updated' => new Zend_Db_Expr('NOW()'),
				'data' => serialize($data)
			));

		}
	}
	public function getAllWords()
	{
		 $sql = '
			SELECT id, name
			FROM blacklisted_words
			WHERE 1';

        $wordList = parent::_fetchAll($sql);
		
		return $wordList;
	}
	
	public function getAllWordsByType( $type = NULL  )
	{
		 $type = is_null($type) ? $this->type : $type;
		 $sql = '
			SELECT id, name, search_type
			FROM blacklisted_words
			WHERE types & ?' ;

        $wordList = parent::_fetchAll($sql,array($type));
		
		return $wordList;
	}
	
	public function fromBitToArray($bit)
	{
		$bl_types = array();
		if($bit & self::BL_TYPE_FEEDBACK){
			$bl_types[self::BL_TYPE_FEEDBACK] = 'FeedBack';
		}
		if($bit & self::BL_TYPE_ABOUT){
			$bl_types[self::BL_TYPE_ABOUT] = 'About Me';
		}
		if($bit & self::BL_TYPE_PRIVATE_MESSAGING){
			$bl_types[self::BL_TYPE_PRIVATE_MESSAGING] = 'Private Messaging';
		}
		if($bit & self::BL_TYPE_INSTANT_MESSAGES){
			$bl_types[self::BL_TYPE_INSTANT_MESSAGES] = 'Instant Messages';
		}
		if($bit & self::BL_TYPE_BUBBLE_TEXT){
			$bl_types[self::BL_TYPE_BUBBLE_TEXT] = 'Bubble Text';
		}
		if($bit & self::BL_TYPE_CLASSIFIED_ADS){
			$bl_types[self::BL_TYPE_CLASSIFIED_ADS] = 'Classifieds Ads';
		}
		if($bit & self::BL_TYPE_SPECIAL_CHAR){
			$bl_types[self::BL_TYPE_SPECIAL_CHAR] = 'Special Characteristics';
		}
		if($bit & self::BL_TYPE_SLOGAN){
			$bl_types[self::BL_TYPE_SLOGAN] = 'Slogan';
		}
		if($bit & self::BL_TYPE_ADDITIONAL_SERVICES){
			$bl_types[self::BL_TYPE_ADDITIONAL_SERVICES] = 'Additional Services';
		}
		if($bit & self::BL_TYPE_OTHER){
			$bl_types[self::BL_TYPE_OTHER] = 'Other Cases';
		}
		if($bit & self::BL_TYPE_SHOWNAME){
			$bl_types[self::BL_TYPE_SHOWNAME] = 'Showname';
		}
		return $bl_types;
	}
	
	public function exists($name, $search_type, $id = null)
	{
		$sql_edit = isset($id) ?  ' AND id <>'.$id : '';
		
		$sql = 'SELECT TRUE FROM blacklisted_words WHERE name = ? AND search_type = ?'.$sql_edit;
		return self::getAdapter()->fetchOne($sql, array($name , $search_type));
	}

}
