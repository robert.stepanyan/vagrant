<?php
class Model_Bubbles extends Cubix_Model
{
	protected $_table = 'escort_bubbletexts';

	// comments status
	const BUBBLE_ACTIVE = 1;
	const BUBBLE_REMOVED = 0;

	public function getById($id)
	{
		$sql = '
			SELECT
				eb.id, eb.text, UNIX_TIMESTAMP(eb.date) AS date, e.id AS escort_id, e.showname, eb.approvation
			FROM escort_bubbletexts eb
			INNER JOIN escorts e ON e.id = eb.escort_id
			WHERE eb.id = ? AND eb.status = ?
		';

		return parent::_fetchRow($sql, array($id, self::BUBBLE_ACTIVE));
	}
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT
				eb.id, eb.text, UNIX_TIMESTAMP(eb.date) AS date, e.id AS escort_id, e.showname, eb.approvation
			FROM escort_bubbletexts eb
			INNER JOIN escorts e ON e.id = eb.escort_id
			INNER JOIN users u ON e.user_id = u.id
			WHERE 1 AND eb.status = 1
		';
		
		$countSql = '
			SELECT COUNT(DISTINCT(e.id))
			FROM escort_bubbletexts eb
			INNER JOIN escorts e ON e.id = eb.escort_id
			INNER JOIN users u ON e.user_id = u.id
			WHERE 1 AND eb.status = 1
		';
		
		$where = '';

		if ( strlen($filter['application_id']) ) {
			$where .= self::quote('AND u.application_id = ?', $filter['application_id']);
		}

        if ( $filter['bubble_text'] && strlen($filter['bubble_text']) ) {
            $where .= self::quote('AND eb.text LIKE "%'.$filter['bubble_text'].'%"',null);
        }

		if ( strlen($filter['showname']) ) {
			if ($filter['mode'] == 'fixed')
				$where .= self::quote(' AND e.showname = ?', $filter['showname']);
			else
				$where .= self::quote(' AND e.showname LIKE ? ', $filter['showname'] . '%');
		}
				
		if ( $filter['escort_id'] ) {
			$where .= self::quote('AND e.id = ?', $filter['escort_id']);
		}
		
		if ( strlen($filter['status']) > 0 ) {
			$where .= self::quote('AND eb.approvation = ?', $filter['status']);
		}

		if ( strlen($filter['country']) > 0 ) {
			if(is_numeric($filter['country']))
				$where .= self::quote('AND e.country_id in (?)', $filter['country']);
			else{
				$where .= self::quote('AND e.country_id in (?)', explode(',', $filter['country']));
			}
		}
				
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			GROUP BY e.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$count = intval($this->getAdapter()->fetchOne($countSql));
		return parent::_fetchAll($sql);
	}	
	
	public function save($data)
	{
		if (isset($data['id']) && $data['id'] != '')
		{
			$fields = array(
				'text' => $data['text'],
				'date' => new Zend_Db_Expr('NOW()')	
			);

			parent::getAdapter()->update($this->_table, $fields, parent::quote('id = ?', $data['id']));
			
			Cubix_SyncNotifier::notify($data['escort_id'], Cubix_SyncNotifier::EVENT_ESCORT_BUBBLE_TEXT, array('id' => $data['id'], 'action' => 'updated'));
		}
	}
		
	public function remove($id)
	{
		$b = $this->getById($id);
		
		parent::getAdapter()->update($this->_table, array('status' => self::BUBBLE_REMOVED), parent::quote('id = ?', $id));
		
		Cubix_SyncNotifier::notify($b->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_BUBBLE_TEXT, array('id' => $id, 'action' => 'removed'));
	}
	
	public function toggle($id)
	{
		$b = $this->getById($id);
		
		$field = 'approvation';
		
		parent::getAdapter()->update('escort_bubbletexts', array(
			$field => new Zend_Db_Expr('NOT ' . $field)
		), parent::quote('id = ?', $id));
		
		Cubix_SyncNotifier::notify($b->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_BUBBLE_TEXT, array(
			'id' => $id, 
			'action' => '*** status changed from ' . intval($b->approvation) . ' to ' . intval(!$b->approvation) . ' ***')
		);
	}
	
	public function setStatus($id, $status)
	{
		parent::getAdapter()->update('escort_bubbletexts', array('approvation' => $status), parent::getAdapter()->quoteInto('id = ?', $id));
	}

    public function insertNewBubbleText($escortId, $bubbleText)
    {
        $sql = 'INSERT INTO escort_bubbletexts (`escort_id`, `date`, `text`, `status`, `approvation`)
                VALUES ('.$escortId.','. new Zend_Db_Expr("NOW()").', "'.$bubbleText.'", 1, 1 )';

        return parent::getAdapter()->query($sql);
    }
}
