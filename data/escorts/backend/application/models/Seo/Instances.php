<?php

class Model_Seo_Instances extends Cubix_Model
{
	protected $_table = 'seo_entity_instances';
	protected $_itemClass = 'Model_Seo_InstanceItem';
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count = 0)
	{
		$sql = '
			SELECT
				i.id, e.slug, i.primary_title, i.title_' . Cubix_Application::getDefaultLang($this->_app_id) . ' AS title
			FROM seo_entity_instances i
			INNER JOIN seo_entities e ON e.id = i.entity_id
			WHERE 1
		';
		
		$countSql = '
			SELECT
				COUNT(i.id)
			FROM seo_entity_instances i
			INNER JOIN seo_entities e ON e.id = i.entity_id
			WHERE 1
		';
		
		$where = '';
		
		if ( $filter['entity_id'] ) {
			$where .= self::quote(' AND e.id = ?', $filter['entity_id']);
		}

		if ( $filter['item_title'] ) {
			$where .= self::quote(' AND i.primary_title LIKE ?', $filter['item_title'] . '%');
		}

		if ( $filter['escort_id'] ) {
			$where .= self::quote(' AND i.primary_id = ?', $filter['escort_id']);
		}
		
		$where .= self::quote(' AND e.application_id = ?', $this->_app_id);
		
		// <-- Construct WHERE clause here
		
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$count = $this->getAdapter()->fetchOne($countSql);
		
		return parent::_fetchAll($sql);
	}
	
	public function getById($id)
	{
		return $this->_fetchRow('
			SELECT * FROM seo_entity_instances WHERE id = ?
		', array($id));
	}
	
	public function getByPrimaryId($entity, $primary_id, array $def_data = array())
	{
		$data = $this->_fetchRow('
			SELECT ei.* FROM seo_entity_instances ei
			INNER JOIN seo_entities e ON e.id = ei.entity_id
			WHERE e.slug = ? AND ei.primary_id = ? AND e.application_id = ?
		', array($entity, $primary_id, $this->_app_id));

		/*if ( ! $data && isset($def_data['primary_title']) && isset($def_data['entity_id']) ) {
			/*$data = $this->_fetchRow('
				SELECT
					' . Cubix_I18n::getTblFields('title') . ',
					' . Cubix_I18n::getTblFields('heading') . ',
					' . Cubix_I18n::getTblFields('meta_description') . ',
					' . Cubix_I18n::getTblFields('meta_keywords') . '
				FROM seo_entities
				WHERE slug = ?
			', $entity);*
			
			$data = array();
			
			foreach ( Cubix_I18n::getLangs(true) as $lang_id ) {
				$data['title_' . $lang_id] = '';
				$data['heading_' . $lang_id] = '';
				$data['meta_description_' . $lang_id] = '';
				$data['meta_keywords_' . $lang_id] = '';
			}
			
			$data = new Model_Seo_InstanceItem($data);
		
			$data->primary_id = $primary_id;
			
			if ( isset($def_data['primary_title']) ) $data->primary_title = $def_data['primary_title'];
			if ( isset($def_data['entity_id']) ) $data->entity_id = $def_data['entity_id'];
		}*/
		
		return $data;
	}
	
	public function save(Model_Seo_InstanceItem $item)
	{
		if ( ! $item->getId() ) {
			$title = parent::getAdapter()->fetchOne('SELECT primary_title FROM seo_entity_instances WHERE entity_id = ? AND primary_id = ?', array($item->entity_id, $item->primary_id));
			if ($title) {
				return array('save' => false, 'title' => $title);
			}
		}

		parent::save($item);
		
		return parent::getAdapter()->lastInsertId();
	}	
	
	public function remove($id)
	{
		parent::remove(self::quote('id = ?', $id));
	}
}
