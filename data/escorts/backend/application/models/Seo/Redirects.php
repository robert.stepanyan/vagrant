<?php

class Model_Seo_Redirects extends Cubix_Model
{
	protected $_table = 'redirect_pages';
	protected $_itemClass = 'Model_Seo_RedirectItem';


	public function getById($id)
	{
		return $this->_fetchRow('
			SELECT * FROM redirect_pages WHERE id = ?
		', $id);
	}
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count = 0)
	{
		$sql = '
			SELECT
				id, url, referer, redirect_page, date, hits_count
			FROM redirect_pages
			WHERE 1
		';

		$countSql = '
			SELECT
				COUNT(id)
			FROM redirect_pages
			WHERE 1
		';

		$where = '';

		if ( $filter['url'] ) {
			$where .= " AND url LIKE '%" .  $filter['url'] . "%'";
		}
		
		if ( $filter['application_id'] ) {
			$where .= self::quote(' AND application_id = ?', $filter['application_id']);
		}

		// <-- Construct WHERE clause here

		$sql .= $where;
		$countSql .= $where;

		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$count = $this->getAdapter()->fetchOne($countSql);
		
		if ($count > 50000)
			$count = 50000;

		return parent::_fetchAll($sql);
	}

	public function save(Model_Seo_RedirectItem $item)
	{
		parent::save($item);

		return parent::getAdapter()->lastInsertId();
	}

	public function remove($id)
	{
		parent::remove(self::quote('id = ?', $id));
	}
}
