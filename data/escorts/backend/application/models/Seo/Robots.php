<?php

class Model_Seo_Robots extends Cubix_Model
{
	protected $_table = 'seo_robots';
	protected $_itemClass = 'Model_Seo_RobotItem';
		
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count = 0)
	{
		$sql = '
			SELECT
				id, body, google_id
			FROM seo_robots
			WHERE 1
		';
		
		$countSql = '
			SELECT
				COUNT(id)
			FROM seo_robots
			WHERE 1
		';
		
		$where = '';
		
		if ( $filter['application_id'] ) {
			$where .= self::quote(' AND application_id = ?', $filter['application_id']);
		}
		
		// <-- Construct WHERE clause here
		
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$count = $this->getAdapter()->fetchOne($countSql);
		
		return parent::_fetchAll($sql);
	}
	
	public function getById($id)
	{
		return $this->_fetchRow('
			SELECT * FROM seo_robots WHERE id = ?
		', $id);
	}
	
	public function save(Model_Seo_EntityItem $item)
	{	
		parent::save($item);
		
		return parent::getAdapter()->lastInsertId();
	}
}
