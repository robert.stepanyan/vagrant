<?php

class Model_Seo_Entities extends Cubix_Model
{
	protected $_table = 'seo_entities';
	protected $_itemClass = 'Model_Seo_EntityItem';
	
	public function getList()
	{
		$application_id = Zend_Controller_Front::getInstance()->getRequest()->application_id;

		$sql = '
			SELECT
				id, slug, title 
			FROM seo_entities
			WHERE has_instances = TRUE AND application_id = ?
			ORDER BY slug ASC
		';
		
		return parent::_fetchAll($sql, $application_id);
	}
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count = 0)
	{
		$sql = '
			SELECT
				id, slug, title_' . Cubix_Application::getDefaultLang($this->_app_id) . ' AS title
			FROM seo_entities
			WHERE 1
		';
		
		$countSql = '
			SELECT
				COUNT(id)
			FROM seo_entities
			WHERE 1
		';
		
		$where = '';
		
		if ( $filter['application_id'] ) {
			$where .= self::quote(' AND application_id = ?', $filter['application_id']);
		}
		
		// <-- Construct WHERE clause here
		
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$count = $this->getAdapter()->fetchOne($countSql);
		
		return parent::_fetchAll($sql);
	}
	
	public function getById($id)
	{
		return $this->_fetchRow('
			SELECT * FROM seo_entities WHERE id = ?
		', $id);
	}
	
	public function getBySlug($slug)
	{
		return $this->_fetchRow('SELECT id FROM seo_entities WHERE slug = ? AND application_id = ?', array($slug, $this->_app_id));
	}
	
	public function existsBySlug($slug, $app_id = null)
	{
		$result = $this->getAdapter()->fetchOne('
			SELECT TRUE FROM seo_entities WHERE slug = ? AND application_id = ?
		', array($slug, is_null($app_id) ? $this->_app_id : $app_id));
		
		return $result;
	}
	
	public function save(Model_Seo_EntityItem $item)
	{	
		parent::save($item);
		
		return parent::getAdapter()->lastInsertId();
	}	
	
	public function remove($id)
	{
		parent::remove(self::quote('id = ?', $id));
	}
}
