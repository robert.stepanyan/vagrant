<?php

class Model_ixsBanners extends Cubix_Model
{
    //const HOST = 'https://adzz.io/';
    const HOST = 'https://ixspublic.com/';

    public function createTmpTableForIXSBanners()
    {
        $this->getAdapter()->query("DROP TABLE IF EXISTS `ixs_banners_tmp`");

        $sql = "
        CREATE TABLE `ixs_banners_tmp`  (
            `id` int(11) NOT NULL AUTO_INCREMENT,
          `type` enum('rotating','fixed') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'rotating',
          `frozen_position` int(3) NULL DEFAULT NULL,
          `frozen_country_id` int(11) NULL DEFAULT NULL,
          `frozen_city_id` int(11) NULL DEFAULT NULL,
          `frozen_region_id` int(11) NULL DEFAULT NULL,
          `ixs_id` int(11) NULL DEFAULT NULL,
          `zone_id` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
          `zone_rotate` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
          `zone_rotation` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
          `banner_id` int(11) NULL DEFAULT NULL,
          `filename` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
          `target` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
          `title` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
          `link` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
          `cmp_id` int(11) NULL DEFAULT NULL,
          `user_id` int(11) NULL DEFAULT NULL,
          `size` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
          `md5_hash` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
          `is_follow` int(11) NULL DEFAULT 0,
          `sort` int(11) NULL DEFAULT 0,
          `low_priority` int(11) NULL DEFAULT 0,
          PRIMARY KEY (`id`) USING BTREE,
          UNIQUE INDEX `unique_banner_id`(`banner_id`) USING BTREE
        ) ENGINE = InnoDB AUTO_INCREMENT = 2637 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;";

        return $this->getAdapter()->query($sql);
    }

    public function clearIXSBannersTable()
    {
        return $this->getAdapter()->query("TRUNCATE `ixs_banners`");
    }

    public function updateNewBannersSettings($bannerId)
    {
        $sql = 'SELECT `type`, `frozen_position`, `frozen_country_id`,`frozen_region_id`, `frozen_city_id`, `is_follow`, `low_priority` FROM `ixs_banners_tmp` WHERE `banner_id` = '.$bannerId;
        $result = parent::_fetchRow($sql);

        $type = 1; /* rotating */

        if($result->type == 'fixed') {
            $type = 2; /* fixed */
        }

        $this->getAdapter()->query("UPDATE `ixs_banners` SET `type`= $type, `frozen_position`= $result->frozen_position, `frozen_country_id`= $result->frozen_country_id,`frozen_region_id` = $result->frozen_region_id, `frozen_city_id` = $result->frozen_city_id, `is_follow` = $result->is_follow,  `low_priority` = $result->low_priority WHERE `banner_id` = ". $bannerId);
    }

    public function fillExistingBannersToTmpTable(Array $seeds)
    {
        $table = 'ixs_banners_tmp';
        $sql = "INSERT INTO $table ( `id`, `type`, `frozen_position`, `frozen_country_id`, `frozen_city_id`,
										 `ixs_id`, `zone_id`, `zone_rotate`,`zone_rotation` , `banner_id`, `filename`,
										 `target`, `title`, `link`, `cmp_id`, `user_id`, `size`, `md5_hash`, `is_follow`, `sort`, `low_priority`, `frozen_region_id`) VALUES ";
        foreach ($seeds as $seed) {
            $sql .= '(';
            foreach ($seed as $key => $value) {
                $sql .= '"' . $value . '",';
            }
            $sql = substr($sql, 0, -1); // Remove last comma
            $sql .= '),';
        }
        $sql = substr($sql, 0, -1); // Remove last comma

        try {
            return $this->getAdapter()->query($sql);
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }
    }

    public function copyImages($imgs)
    {
        $copycount = 0;
        foreach ($imgs as $img) {

            if (!file_exists('./images/ixsGovazd/' . $img['filename'])) {

                $sourceurl = self::HOST . 'uploads/' . $img['filename'];

                $result = @copy($sourceurl, './images/ixsGovazd/' . $img['filename']);
                if (!$result) {
                    return 'image copy error';
                }
            }else{

            }

            $copycount++;
        }
        return $copycount;
    }

    public function swapTempTable()
    {
        $this->getAdapter()->query("DROP TABLE IF EXISTS `ixs_banners_tmp`");
    }

    public function insertISXBanners($images, $zone)
    {

        $sql = "INSERT IGNORE INTO ixs_banners ( `id`, `ixs_id`, `zone_id`, `zone_rotate`, `zone_rotation`,
										 `banner_id`, `filename`, `target`,`title` , `link`, `cmp_id`,
										 `user_id`, `size`, `md5_hash`,`is_follow`, `sort`, `low_priority`)";
        $sql_value = 'VALUES ';


        foreach ($images as $image) {

            $image_arr = (array)$image;

            $image_arr['zone_id'] = $zone;

            $image_data[0] = (isset($image_arr['id']) ? $image_arr['id'] : '');
            $image_data[1] = (isset($image_arr['zone_id']) ? $image_arr['zone_id'] : '');
            $image_data[2] = (isset($image_arr['zone_rotate']) ? $image_arr['zone_rotate'] : '');
            $image_data[3] = (isset($image_arr['zone_rotation']) ? $image_arr['zone_rotation'] : '');
            $image_data[4] = (isset($image_arr['banner_id']) ? $image_arr['banner_id'] : '');
            $image_data[5] = (isset($image_arr['filename']) ? $image_arr['filename'] : '');
            $image_data[6] = (isset($image_arr['target']) ? $image_arr['target'] : '');
            $image_data[7] = (isset($image_arr['title']) ? $image_arr['title'] : 'notitle');
            $image_data[8] = (isset($image_arr['link']) ? $image_arr['link'] : '');
            $image_data[9] = (isset($image_arr['cmp_id']) ? $image_arr['cmp_id'] : '');
            $image_data[10] = (isset($image_arr['user_id']) ? $image_arr['user_id'] : '');
            $image_data[11] = (isset($image_arr['size']) ? $image_arr['size'] : '');
            $image_data[12] = (isset($image_arr['md5_hash']) ? $image_arr['md5_hash'] : '');
            $image_data[13] = (isset($image_arr['is_follow']) ? $image_arr['is_follow'] : '');
            $image_data[14] = (isset($image_arr['sort']) ? $image_arr['sort'] : '');
            $image_data[15] = (isset($image_arr['low_priority']) ? $image_arr['low_priority'] : '');

            $sql_value .= '(null,';
            //$last_key = end(array_keys($image));

            foreach ($image_data as $key => $value) {

                $sql_value .= '"' . $value . '"';
                if ($key < 15) {
                    $sql_value .= ',';
                }
            }
            $sql_value .= '),';

            unset($image_data[$key]);
        }

        $sql .= $sql_value;


        try {
            return $this->getAdapter()->query(rtrim($sql, ','));
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }
    }

    public function fetchUsedSpots($countryId, $cityId = null)
    {
        $sql = 'SELECT id, frozen_position FROM ixs_banners WHERE frozen_country_id = ' . $countryId;

        if (!empty($cityId)) {
            $sql .= ' or frozen_city_id = ' . $cityId;
        }

        $result = parent::_fetchAll($sql);
        return $result;
    }

    public function save($id, $data)
    {
        return self::getAdapter()->update('ixs_banners', $data, self::quote('id = ?', $id));
    }

    public function freezeBannerAt($bannerId, $position, $countryId, $cityId = null, $regionId = null)
    {
        $sql = "UPDATE ixs_banners SET frozen_position = $position, type = 'fixed', frozen_country_id = $countryId";
        if (!empty($cityId)) $sql .= ", frozen_city_id = $cityId";

        if (!empty($regionId)) {
            $sql .= ", frozen_region_id = $regionId";
        }
        else {
            $sql .= ", frozen_region_id = null";
        }

        $sql .= " WHERE id = $bannerId";

        return $this->getAdapter()->query($sql);
    }

    public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count = null)
    {
        $sql = '
			SELECT SQL_CALC_FOUND_ROWS ib.id, ib.zone_id, ib.banner_id, ib.link, ib.size, ib.filename,
			CASE
                WHEN NOT ct.title_en IS NULL THEN
                    CONCAT(c.title_en, \', \', ct.title_en, \' (Position - \', ib.frozen_position, \')\')
                WHEN NOT c.title_en IS NULL THEN
                    CONCAT(c.title_en, \', All cities\', \' (Position - \', ib.frozen_position, \')\')
            END AS frozen_location 
			FROM ixs_banners ib
			LEFT JOIN countries c 
			ON c.id = ib.frozen_country_id
			LEFT JOIN cities ct 
			ON ct.id = ib.frozen_city_id
			WHERE ib.zone_id in (\'x323139\', \'x36\') 
		';

        if (!empty($filter['id'])) {
            $sql .= ' AND ib.id = ' . $filter['id'];
        }

        if (!empty($filter['link'])) {
            $sql .= ' AND ib.link like \'%' . $filter['link'] . '%\'';
        }

        if (!empty($filter['type'])) {
            $sql .= ' AND ib.type = \'' . $filter['type'] . '\'';
        }

        if (!empty($filter['zone_id'])) {
            $sql .= ' AND (ib.zone_id = \'' . $filter['zone_id'] . '\' or ib.zone_id = \'x' . $filter['zone_id'] . '\' )';
        }

        if (!empty($filter['cmp_id'])) {
            $sql .= ' AND ib.cmp_id = ' . $filter['cmp_id'];
        }

        if (!empty($filter['banner_id'])) {
            $sql .= ' AND ib.banner_id = ' . $filter['banner_id'];
        }

        if (!empty($filter['frozen_country_id'])) {
            $sql .= ' AND ib.frozen_country_id = ' . $filter['frozen_country_id'];
        }

        $sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

        $this->getAdapter()->query('SET NAMES `utf8`');
        $rows = parent::_fetchAll($sql);

        $count = $this->getAdapter()->fetchOne('SELECT FOUND_ROWS()');

        return $rows;
    }

    public function getAllBannersForEGUK($page, $per_page, $filter, $sort_field, $sort_dir, &$count = null)
    {

        $sql = '
			SELECT SQL_CALC_FOUND_ROWS ib.id, ib.zone_id, ib.banner_id, ib.link, ib.size, ib.filename
			
			FROM ixs_banners ib
			LEFT JOIN countries c 
			ON c.id = ib.frozen_country_id
			LEFT JOIN cities ct 
			ON ct.id = ib.frozen_city_id
			WHERE 1
		';
//        WHERE ib.zone_id in ('x323537', 'x3330', 'x323238', 'x3436' , 'x323239')
        if (!empty($filter['id'])) {
            $sql .= ' AND ib.id = ' . $filter['id'];
        }

        if (!empty($filter['link'])) {
            $sql .= ' AND ib.link like \'%' . $filter['link'] . '%\'';
        }

        if (!empty($filter['type'])) {
            $sql .= ' AND ib.type = \'' . $filter['type'] . '\'';
        }

        if (!empty($filter['zone_id'])) {
            $sql .= ' AND (ib.zone_id = \'' . $filter['zone_id'] . '\' or ib.zone_id = \'x' . $filter['zone_id'] . '\' )';
        }

        if (!empty($filter['cmp_id'])) {
            $sql .= ' AND ib.cmp_id = ' . $filter['cmp_id'];
        }

        if (!empty($filter['banner_id'])) {
            $sql .= ' AND ib.banner_id = ' . $filter['banner_id'];
        }

        if (!empty($filter['frozen_country_id'])) {
            $sql .= ' AND ib.frozen_country_id = ' . $filter['frozen_country_id'];
        }

        $sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';


        $this->getAdapter()->query('SET NAMES `utf8`');
        $rows = parent::_fetchAll($sql);

        $count = $this->getAdapter()->fetchOne('SELECT FOUND_ROWS()');

        return $rows;
    }

    public function getById($id)
    {
        $sql = 'SELECT * FROM ixs_banners WHERE id = ' . $id;
        $result = parent::_fetchRow($sql);
        return $result;
    }

    static public function get($zone_id = null, $user_id = null, $size = null)
    {
        $where = array(
            'ixs.zone_id = ?' => $zone_id,
            'ixs.user_id = ?' => $user_id,
            'ixs.size = ?' => $size
        );

        $where = self::getWhereClause($where, true);
        $sql = '
			SELECT *
			FROM ixs_banners ixs
			' . (!is_null($where) ? 'WHERE ' . $where : '');

        $result = parent::_fetchAll($sql);
        return $result;
    }

    static public function getNewFetchedBanners()
    {
        $sql = 'SELECT * FROM ixs_banners ixs WHERE 1';

        $result = parent::_fetchAll($sql);
        return $result;
    }

    public static function get_right_sidebar_banners($zone_id = null, $rand_count = false)
    {

        if (isset($zone_id)) {
            $model_ixs = new Model_ixsBanners();
            $banners = $model_ixs->get($zone_id);

            if ($rand_count) {
                $random_banners = array();
                $random_els = array_rand($banners, $rand_count);
                if ($rand_count == 1) {
                    $random_banners[] = $banners[$random_els];
                    return $random_banners;
                } else {
                    foreach ($random_els as $random_el) {
                        $random_banners[] = $banners[$random_el];
                    }
                    $banners = $random_banners;
                }

            }

            return $banners;

        }

        return "";
    }

    public function truncate()
    {
        $sql = 'DELETE FROM ixs_banners WHERE frozen_position IS NULL';
        return $this->getAdapter()->query($sql);
    }

    public function delete($except = array())
    {
        $cmp_ids = implode(',', $except);
        $sql = 'DELETE FROM ixs_banners WHERE cmp_id NOT IN ('.$cmp_ids.')';
       
        return $this->getAdapter()->query($sql);
    }
    
    /**
     * @param $id
     * @return mixed
     */
    public function remove($id){
        return parent::getAdapter()->delete('ixs_banners', parent::quote('id = ?', $id));
    }
}
