<?php

class Model_UserItem extends Cubix_Model_Item
{
	public function __construct($rowData = array())
	{
		parent::__construct($rowData);
		
		$groups = array();
		
		foreach ( $this->getAdapter()->query('SELECT role_id AS id FROM acl_users_roles WHERE backend_user_id = ?', $this->getId())->fetchAll() as $role ) {
			$groups[] = 'group-' . $role->id;
		}
		
		$this->groups = $groups;
	}
	
	public function hasAccessToEscort($escort_id)
	{
		if ( 'sales manager' == $this->type || 'sales clerk' == $this->type ) {
			//return false;
			return $this->getId() == $this->getAdapter()->fetchOne('
				SELECT u.sales_user_id FROM escorts e INNER JOIN users u ON u.id = e.user_id WHERE e.id = ?
			', $escort_id);
		}
		else {
			return true;
		}
	}
	
	public function hasAccessToAgency($agency_id)
	{
		if ( 'sales manager' == $this->type || 'sales clerk' == $this->type ) {
			
			return $this->getId() == $this->getAdapter()->fetchOne('
				SELECT u.sales_user_id FROM agencies a INNER JOIN users u ON u.id = a.user_id WHERE a.id = ?
			', $agency_id);
		}
		else {
			return true;
		}
	}
	
	public function __wakeup()
	{
		$this->setAdapter(Zend_Registry::get('db'));
	}

	//
	public function getEscortId()
	{
		return $this->_adapter->fetchOne('
			SELECT id FROM escorts WHERE user_id = ?
		', $this->getId());
	}

	public function getAgencyId()
	{
		return $this->_adapter->fetchOne('
			SELECT id FROM agencies WHERE user_id = ?
		', $this->getId());
	}

	public function getBalance()
	{
		return $this->_adapter->fetchOne('SELECT balance FROM users WHERE id = ?', $this->getId());
	}
}
