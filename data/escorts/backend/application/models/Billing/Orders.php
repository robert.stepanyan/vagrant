<?php

class Model_Billing_Orders extends Cubix_Model
{
	protected $_table = 'orders';
	protected $_itemClass = 'Model_Billing_OrderItem';

	const STATUS_PENDING = 1;
	// const STATUS_CANCELLED = 2;
	const STATUS_PAID = 3;
	// const STATUS_CHARGEBACK = 4;
	const STATUS_PAYMENT_DETAILS_RECEIVED = 5;
	const STATUS_PAYMENT_REJECTED = 6;
	const STATUS_CLOSED = 7;
	const STATUS_PAYMENT_CHARGEBACK = 8;

	public static $STATUS_LABELS = array(
		self::STATUS_PENDING => 'pending',
		// self::STATUS_CANCELLED => 'cancelled',
		self::STATUS_PAID => 'paid',
		// self::STATUS_CHARGEBACK => 'chargeback',
		self::STATUS_PAYMENT_DETAILS_RECEIVED => 'payment details received',
		self::STATUS_PAYMENT_REJECTED => 'payment rejected',
		self::STATUS_CLOSED => 'closed',
		self::STATUS_PAYMENT_CHARGEBACK => 'payment chargeback'
	);

	const CONDITION_AFTER_RECEIVING_PAYMENT_DETAILS = 8;
	const CONDITION_AFTER_PAID = 5;
	const CONDITION_WITHOUT_PAYMENT = 10;

	public static $CONDITION_LABELS = array(
		self::CONDITION_AFTER_RECEIVING_PAYMENT_DETAILS => 'after receiving payment details',
		self::CONDITION_AFTER_PAID => 'after paid'/*,
		self::CONDITION_WITHOUT_PAYMENT => 'without payment'*/
	);

	public function getTotalOrdersPaid()
	{
		$sql = "SELECT SUM(price) FROM orders WHERE status = ?";

		return parent::getAdapter()->fetchOne($sql, array(Model_Billing_Orders::STATUS_PAID));
	}

	public function get($id)
	{
		$sql = "SELECT o.* FROM orders o WHERE id = ?";

		return parent::getAdapter()->fetchRow($sql, array($id));
	}

	public function getAllV2(array $data = array(), $page = 1, $per_page = 10, $sort_field = 'order_date', $sort_dir = 'DESC')
	{
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		$joins = array(
			10  => 'INNER JOIN order_packages op ON op.order_id = o.id',
			20  => 'INNER JOIN gotd_orders go ON op.id = go.order_package_id',
			30  => 'INNER JOIN escorts e ON e.user_id = u.id',
			40  => 'INNER JOIN agencies a ON a.user_id = u.id',
			
			999 => 'INNER JOIN escorts e ON e.user_id = o.user_id',
		);
		
		$active_joins = array();
		$group_by = " ";
		
		$where = array(
			'1'
		);
		
		if ( $data['discounted_orders'] ) {
			$where[] = '(op.discount <> 0 OR op.discount_fixed <> 0)';
			$active_joins[10] = $joins[10];
			$group_by = "GROUP BY o.system_order_id";
		}
		
		if ( $data['gotd_booking'] ) {
			$active_joins[10] = $joins[10];
			$active_joins[20] = $joins[20];
			
			$group_by = "GROUP BY o.system_order_id";
		}
		
		if ( $data['showname'] ) {
			$where[] = 'e.showname LIKE "' . $data['showname'] . '%"';
			
			$active_joins[30] = $joins[30];
		}
		
		if ( $data['escort_id'] ) {
			$where[] = 'e.id = ' . $data['escort_id'];
			
			$active_joins[30] = $joins[30];
		}
		
		if ( $data['agency_name'] ) {
			$where[] = 'a.name LIKE "' . $data['agency_name'] . '%"';
			
			$active_joins[40] = $joins[40];
		}
				
		if ( $bu_user->type == 'superadmin' ) {
			if ( $data['sales_user_id'] ) {
				$where[] = 'o.backend_user_id = ' . $data['sales_user_id'];
			}
		}
		else {
			if ( $bu_user->type == 'sales manager' || $bu_user->type == 'sales' || $bu_user->type == 'sales clerk' ) {
				$where[] = 'o.backend_user_id = ' . $bu_user->id;
			}
			else if ($bu_user->type == 'admin') {
				//$where[] = 'o.application_id = ' . $bu_user->application_id;
				if ( $data['sales_user_id'] ) {
					$where[] = 'o.backend_user_id = ' . $data['sales_user_id'];
				}
			}
		}
		
		if ( $data['sales_user_id_real'] ) {
			if ( $bu_user->type == 'superadmin' ) {
				$where[] = 'u.sales_user_id = ' . $data['sales_user_id_real'];
			}
			else if ($bu_user->type == 'admin') {
				$where[] = 'u.application_id = ' . $bu_user->application_id;
				$where[] = 'u.sales_user_id = ' . $data['sales_user_id_real'];
			}
		}
		
		if ( $data['status'] ) {
			if ( ! is_array($data['status']) ) {
				$where[] = 'o.status = ' . $data['status'];
			}
			else {
				$where[] = 'o.status IN (' . implode(', ', $data['status']) . ')';
			}
		}
		
		if ( $data['order_id'] ) {
			$where[] = 'o.system_order_id = "' . $data['order_id'] . '"';
		}
		
		if ( $data['email'] ) {
			$where[] = 'u.email LIKE "' . $data['email'] . '%"';
		}
		
		if ( $data['filter_by'] ) {
			if ( in_array($data['filter_by'], array('order_date', 'payment_date')) ) {
				if ( $data['date_from'] ) {
					$where[] = 'DATE(' . $data['filter_by'] . ') >= ' . new Zend_Db_Expr('DATE(FROM_UNIXTIME(' . $data['date_from'] . '))');
				}

				if ( $data['date_to'] ) {
					$where[] = 'DATE(' . $data['filter_by'] . ') <= ' . new Zend_Db_Expr('DATE(FROM_UNIXTIME(' . $data['date_to'] . '))');
				}
			}
		}
		
		if ( $data['showname'] || $data['escort_id'] ) {
			$active_joins[10] = $joins[10];
			$active_joins[30] = $joins[999];
			ksort($active_joins);
		}
		
		$base_sql = "
			SELECT
				SQL_CALC_FOUND_ROWS
				u.id AS user_id, u.user_type, u.username, o.id, UNIX_TIMESTAMP(order_date) AS order_date, 
				o.system_order_id, o.price, o.price_package, o.packages_count AS packages, o.taken_from_balance,
				o.status, UNIX_TIMESTAMP(o.payment_date) AS payment_date, o.activation_condition,
				o.is_self_checkout, o.taken_from_balance AS real_payment,
				o.backend_user_id, u.sales_user_id
			FROM orders o 
			INNER JOIN users u ON u.id = o.user_id
			" . implode(' ', $active_joins) . "
			WHERE " . implode(' AND ', $where) . "
			" . $group_by . "
			ORDER BY " . $sort_field . " " . $sort_dir . "			
			LIMIT " . ($page - 1) * $per_page . ", " . $per_page
		;
		
		//echo $base_sql;die;
		
		$data = parent::_fetchAll($base_sql);
		$count = $this->getAdapter()->fetchOne("SELECT FOUND_ROWS()");

		if ( count($data) ) {
			foreach ( $data as $i => $order ) {
				$data[$i]->status = self::$STATUS_LABELS[$order->status];
				$data[$i]->condition = self::$CONDITION_LABELS[$order->activation_condition];
				
				if ( $order->user_type == 'escort' ) {
					$data[$i]->showname = $this->getAdapter()->fetchOne("SELECT e.showname FROM escorts e WHERE e.user_id = ?", array($order->user_id));
					$data[$i]->escort_id = $this->getAdapter()->fetchOne("SELECT e.id FROM escorts e WHERE e.user_id = ?", array($order->user_id));
					if (Cubix_Application::getId() == APP_EF){
						$data[$i]->need_age_verification = $this->getAdapter()->fetchOne("SELECT e.need_age_verification FROM escorts e WHERE e.user_id = ?", array($order->user_id));
						$data[$i]->pseudo_escort = $this->getAdapter()->fetchOne("SELECT e.pseudo_escort FROM escorts e WHERE e.user_id = ?", array($order->user_id));
					}
				} else if ( $order->user_type == 'agency' ) {
					$data[$i]->agency_name = $this->getAdapter()->fetchOne("SELECT a.name FROM agencies a WHERE a.user_id = ?", array($order->user_id));
					$data[$i]->agency_id = $this->getAdapter()->fetchOne("SELECT a.id FROM agencies a WHERE a.user_id = ?", array($order->user_id));
				}
				
				$data[$i]->sales_username = $this->getAdapter()->fetchOne("SELECT bu.username FROM backend_users bu WHERE bu.id = ?", array($order->backend_user_id));
				$data[$i]->sales_username_real = $this->getAdapter()->fetchOne("SELECT bu.username FROM backend_users bu WHERE bu.id = ?", array($order->sales_user_id));
			}
		}
		
		return array(
			'data' => $data,
			'count' => $count,
			'total' => 0,
			'difference' => 0
		);
	}
	
	public function getAll(array $filter = array(), $page = 1, $per_page = 10, $sort_field = 'order_date', $sort_dir = 'DESC')
	{	
		$gotd_join = array();
		if ( isset($filter['gotd_booking']) ) {
			$gotd_join = array('joins' => 'INNER JOIN gotd_orders go ON op.id = go.order_package_id');
			
			unset($filter['gotd_booking']);
		}
		
		$sql = array(
			'tables' => array('orders o'),
			'fields' => array(
				'/* GET ALL */ o.id', 'o.status', 'u.username', 'o.user_id', 'system_order_id', 'UNIX_TIMESTAMP(order_date) AS order_date',
				'UNIX_TIMESTAMP(payment_date) AS payment_date', 'o.price', 'bu.username AS sales_username', 'bu2.username AS sales_username_real',
				'o.activation_condition', 'u.user_type', 'packages_count AS packages', 'a.name AS agency_name',
                /* Update Grigor */
                "CONCAT(t.first_name,' ',t.last_name) as pusername",
                /* Update Grigor */
				'e.showname', 'a.id AS agency_id', 'e.id AS escort_id', 'o.taken_from_balance', 'o.is_self_checkout'
			),
			'joins' => array(
				'INNER JOIN backend_users bu ON bu.id = o.backend_user_id',
				'INNER JOIN order_packages op ON op.order_id = o.id',
				'INNER JOIN users u ON u.id = o.user_id',
				'INNER JOIN backend_users bu2 ON bu2.id = u.sales_user_id',
				'LEFT JOIN escorts e ON e.user_id = o.user_id',
				'LEFT JOIN agencies a ON a.user_id = o.user_id AND u.user_type = \'agency\'',
				'LEFT JOIN transfer_orders ot ON ot.order_id = o.id',
				'LEFT JOIN transfers t ON t.id = ot.transfer_id',
			) + $gotd_join,
			'where' => array(
			) + $filter,
			'group' => 'o.system_order_id'/*'o.id'*/,
			'order' => array($sort_field, $sort_dir),
			'page' => array($page, $per_page)
		);		
		
		$dataSql = Cubix_Model::getSql($sql);
		
		//echo $dataSql;
      
		$sql['fields'] = array();
		$sql['group'] = 'o.id';
		$sql['page'] = false;
		$sql['order'] = false;


        $sql['fields'] = array('o.price, o.id');
		$aggrSql = Cubix_Model::getSql($sql);
		
		//echo "-------" . $aggrSql . "-------";

        $sqlTotal = array(
			'tables' => array('('.$aggrSql.') x'),
			'fields' => array(
				'COUNT(x.id) AS count, SUM(x.price) AS total'
			),
			'joins' => array(

			),
			'where' => array(
            
			)
        );
        
        $sqlTotals = Cubix_Model::getSql($sqlTotal);
		
		//echo "-------" . $sqlTotals . "-------";

		$aggr = $this->getAdapter()->fetchRow($sqlTotals);
		
		
		$sql['group'] = false;
		$sql['where'] = array_merge($sql['where'], array('((o.status = 3 AND o.activation_condition = 5) OR ((o.status = 5 OR o.status = 3) AND o.activation_condition = 8))'));
		$sql['fields'][] = '(op.base_price + ((op.base_price / op.base_period) * (op.period - op.base_period)) - op.price) AS result_price';
		
		$aggrSql = Cubix_Model::getSql($sql);
		
		$sqlDiff = array(
			'tables' => array('('.$aggrSql.') x'),
			'fields' => array(
				'SUM(x.result_price) AS difference'
			),
			'joins' => array(

			),
			'where' => array(
            
			)
        );
        
        $sqlDiffs = Cubix_Model::getSql($sqlDiff);
		
		//echo "-------" . $sqlDiffs . "-------"; die;
        
		$diff = $this->getAdapter()->fetchRow($sqlDiffs);

		$data = parent::_fetchAll($dataSql);

		foreach ( array_keys($data) as $i ) {
			$data[$i]->status = self::$STATUS_LABELS[$data[$i]->status];
			$data[$i]->condition = self::$CONDITION_LABELS[$data[$i]->activation_condition];
			$data[$i]->real_payment = $data[$i]->taken_from_balance;
		}

		return array(
			'data' => $data,
			'count' => $aggr->count,
			'total' => number_format($aggr->total, 2) . ' ' . Cubix_Application::getById()->currency_symbol,
			'difference' => number_format(round($diff->difference, 2), 2)
		);
	}
	
	public function getAllCount(array $filter = array(), $page = 1, $per_page = 10, $sort_field = 'order_date', $sort_dir = 'DESC')
	{	
		$gotd_join = array();
		if ( isset($filter['gotd_booking']) ) {
			$gotd_join = array('joins' => 'INNER JOIN gotd_orders go ON op.id = go.order_package_id');
			
			unset($filter['gotd_booking']);
		}
		
		$sql = array(
			'tables' => array('orders o'),
			'fields' => array(
				'/* TOTAL PAID */ o.id', 'o.status', 'u.username', 'o.user_id', 'system_order_id', 'UNIX_TIMESTAMP(order_date) AS order_date',
				'UNIX_TIMESTAMP(payment_date) AS payment_date', 'o.price', 'bu.username AS sales_username', 'bu2.username AS sales_username_real',
				'o.activation_condition', 'u.user_type', 'packages_count AS packages', 'a.name AS agency_name',
                /* Update Grigor */
                "CONCAT(t.first_name,' ',t.last_name) as pusername",
                /* Update Grigor */
				'e.showname', 'a.id AS agency_id', 'e.id AS escort_id', 'o.taken_from_balance', 'o.is_self_checkout'
			),
			'joins' => array(
				'INNER JOIN backend_users bu ON bu.id = o.backend_user_id',
				'INNER JOIN order_packages op ON op.order_id = o.id',
				'INNER JOIN users u ON u.id = o.user_id',
				'INNER JOIN backend_users bu2 ON bu2.id = u.sales_user_id',
				'LEFT JOIN escorts e ON e.user_id = o.user_id',
				'LEFT JOIN agencies a ON a.user_id = o.user_id AND u.user_type = \'agency\'',
				'LEFT JOIN transfer_orders ot ON ot.order_id = o.id',
				'LEFT JOIN transfers t ON t.id = ot.transfer_id',
			) + $gotd_join,
			'where' => array(
			) + $filter,
			'group' => 'o.system_order_id'/*'o.id'*/,
			'order' => array($sort_field, $sort_dir),
			'page' => array($page, $per_page)
		);		
		
		$dataSql = Cubix_Model::getSql($sql);
		
		//echo $dataSql;
      
		$sql['fields'] = array();
			$sql['group'] = 'o.id';
		$sql['page'] = false;
		$sql['order'] = false;


        $sql['fields'] = array('o.price, o.id');
			$aggrSql = Cubix_Model::getSql($sql);

		//echo "-------" . $aggrSql . "-------";

			$sqlTotal = array(
				'tables' => array('('.$aggrSql.') x'),
				'fields' => array(
					'COUNT(x.id) AS count, SUM(x.price) AS total'
				),
				'joins' => array(

				),
				'where' => array(

				)
			);

			$sqlTotals = Cubix_Model::getSql($sqlTotal);

		//echo "-------" . $sqlTotals . "-------<br><br>";
		
			$aggr = $this->getAdapter()->fetchRow($sqlTotals);
			
		
		$sql['group'] = false;
			$sql['where'] = array_merge($sql['where'], array('((o.status = 3 AND o.activation_condition = 5) OR ((o.status = 5 OR o.status = 3) AND o.activation_condition = 8))'));
		$sql['fields'][] = '(op.base_price + ((op.base_price / op.base_period) * (op.period - op.base_period)) - op.price) AS result_price';

			$aggrSql = Cubix_Model::getSql($sql);

			$sqlDiff = array(
				'tables' => array('('.$aggrSql.') x'),
				'fields' => array(
					'SUM(x.result_price) AS difference'
				),
				'joins' => array(

				),
				'where' => array(

				)
			);

			$sqlDiffs = Cubix_Model::getSql($sqlDiff);

		//echo "-------" . $sqlDiffs . "-------"; die;

			$diff = $this->getAdapter()->fetchRow($sqlDiffs);
			
		//$data = parent::_fetchAll($dataSql);
		$data = array();
	
		/*foreach ( array_keys($data) as $i ) {
			$data[$i]->status = self::$STATUS_LABELS[$data[$i]->status];
			$data[$i]->condition = self::$CONDITION_LABELS[$data[$i]->activation_condition];
			$data[$i]->real_payment = $data[$i]->taken_from_balance;
		}*/
	
		return array(
			'data' => $data,
			'count' => $aggr->count,
			'total' => number_format($aggr->total, 2) . ' ' . Cubix_Application::getById()->currency_symbol,
			'difference' => number_format(round($diff->difference, 2), 2)
		);
	}
	
	public function getAllCountV2(array $filter = array(), $type)
	{	
		$joins = array();
		if ( isset($filter['gotd_booking']) ) {
			$joins[11] = 'INNER JOIN gotd_orders go ON op.id = go.order_package_id';
			
			unset($filter['gotd_booking']);
		}
		
		if ( isset($filter['t.transfer_type_id = ?']) ) {
			$joins[12] = 'INNER JOIN transfer_orders ot ON ot.order_id = o.id';
			$joins[13] = 'INNER JOIN transfers t ON t.id = ot.transfer_id';	
		}
		
		if ( isset($filter['a.name LIKE ?']) ) {
			$joins[14] = 'INNER JOIN agencies a ON a.user_id = o.user_id AND u.user_type = \'agency\'';
		}
		
		if ( isset($filter['e.showname LIKE ?']) || isset($filter['e.id = ?']) ) {
			$joins[15] = 'INNER JOIN escorts e ON e.user_id = o.user_id';
		}
		
		 
		$sql = array(
			'tables' => array('orders o'),
			'fields' => array('o.price, o.id, o.price_package'),
			'joins' => array(
				/*'INNER JOIN backend_users bu ON bu.id = o.backend_user_id',*/
				'INNER JOIN order_packages op ON op.order_id = o.id',
				'INNER JOIN users u ON u.id = o.user_id'
				/*'INNER JOIN backend_users bu2 ON bu2.id = u.sales_user_id',*/
			) + $joins,
			'where' => array(
			) + $filter,
			
		);		
		
		if ($type == 'total'){	
			$sql['group'] = 'o.id';


			$aggrSql = Cubix_Model::getSql($sql);

			//echo "-------" . $aggrSql . "-------<br><br>";

			$sqlTotal = array(
				'tables' => array('('.$aggrSql.') x'),
				'fields' => array(
					'COUNT(x.id) AS count, SUM(x.price) AS total'
				),
				'joins' => array(

				),
				'where' => array(

				)
			);

			$sqlTotals = Cubix_Model::getSql($sqlTotal);

			//echo "-------" . $sqlTotals . "-------<br><br>";
		
			$aggr = $this->getAdapter()->fetchRow($sqlTotals);
			
			return array(
				'count' => $aggr->count,
				'total' => number_format($aggr->total, 2) . ' ' . Cubix_Application::getById()->currency_symbol,
			);
		
		}
		elseif($type == 'price-p'){
			$sql['group'] = 'o.id';


			$aggrSql = Cubix_Model::getSql($sql);

			//echo "-------" . $aggrSql . "-------<br><br>";

			$sqlTotal = array(
				'tables' => array('('.$aggrSql.') x'),
				'fields' => array(
					'COUNT(x.id) AS count, SUM(x.price_package) AS total'
				),
				'joins' => array(

				),
				'where' => array(

				)
			);

			$sqlTotals = Cubix_Model::getSql($sqlTotal);

			//echo "-------" . $sqlTotals . "-------<br><br>";
			
			$aggr = $this->getAdapter()->fetchRow($sqlTotals);
			
			return array(
				'count' => $aggr->count,
				'total' => number_format($aggr->total, 2) . ' ' . Cubix_Application::getById()->currency_symbol,
			);
		}
		else {
			$sql['where'] = array_merge($sql['where'], array('((o.status = 3 AND o.activation_condition = 5) OR ((o.status = 5 OR o.status = 3) AND o.activation_condition = 8))'));
			$sql['fields'] = array('(op.base_price + ((op.base_price / op.base_period) * (op.period - op.base_period)) - op.price) AS result_price');

			$aggrSql = Cubix_Model::getSql($sql);

			$sqlDiff = array(
				'tables' => array('('.$aggrSql.') x'),
				'fields' => array(
					'SUM(x.result_price) AS difference'
				),
				'joins' => array(

				),
				'where' => array(

				)
			);

			$sqlDiffs = Cubix_Model::getSql($sqlDiff);

			//echo "-------" . $sqlDiffs . "-------<br><br>";

			$diff = $this->getAdapter()->fetchRow($sqlDiffs);
			
			return array('difference' => number_format(round($diff->difference, 2), 2));
		}
	}
	
	public function getUsersForSalesPerson()
	{
		$backend_user_id = Zend_Auth::getInstance()->getIdentity()->id;

		$sql = array(
			'tables' => array('orders o'),
			'fields' => array(
				'u.id', 'CONCAT(e.showname, " <small>(", u.username, ")</small>") AS name', 'COUNT(o.id) AS orders_count'
			),
			'joins' => array(
				'INNER JOIN users u ON u.id = o.user_id',
				'INNER JOIN escorts e ON e.user_id = u.id'
			),
			'where' => array(
				'o.backend_user_id = ?' => $backend_user_id,
				'u.user_type = ?' => Model_Users::USER_TYPE_ESCORT,
				'o.status IN (?)' => new Zend_Db_Expr(Model_Billing_Orders::STATUS_PENDING . ', ' . Model_Billing_Orders::STATUS_PAYMENT_REJECTED)
			),
			'group' => 'u.id',
			'order' => array('u.username', 'ASC'),
			'page' => false
		);

		$dataSql = Cubix_Model::getSql($sql);

		$data = parent::getAdapter()->fetchAll($dataSql);
		
		foreach ( array_keys($data) as $i ) {
			
		}

		return $data;
	}

	public function getDetails($id)
	{
		/*$order = parent::_fetchRow('
			SELECT
				o.id, o.system_order_id, UNIX_TIMESTAMP(o.order_date) AS order_date,
				UNIX_TIMESTAMP(o.payment_date) AS payment_date, o.price, bu.username AS sales_username,
				u.username, u.user_type, o.activation_condition, o.status, u.id AS user_id,
				e.showname, a.name AS agency_name, bu.id AS sales_id, o.use_balance, tr.comment AS rejection_comment,				
				o.taken_from_balance
			FROM orders o
			INNER JOIN backend_users bu ON bu.id = o.backend_user_id
			
			INNER JOIN users u ON u.id = o.user_id
			LEFT JOIN escorts e ON e.user_id = o.user_id
			LEFT JOIN agencies a ON a.user_id = o.user_id
			LEFT JOIN transfer_orders tor ON tor.order_id = o.id
			LEFT JOIN transfer_rejections tr ON tr.transfer_id = tor.transfer_id
			WHERE o.id = ?
		', array($id));*/
		
		$field = '';
		$join = '';
		
		if (Cubix_Application::getId() == APP_A6)
		{
			$field = ', ci.title_de AS region ';
			$join = '
				LEFT JOIN escort_cities ec ON e.id = ec.escort_id
				LEFT JOIN cities ci ON ec.city_id = ci.id
			';
		}
		
		$order = parent::_fetchRow('
			SELECT
				o.id, o.system_order_id, UNIX_TIMESTAMP(o.order_date) AS order_date,
				UNIX_TIMESTAMP(o.payment_date) AS payment_date, o.price, bu.username AS sales_username, bu.email AS sales_email,
				u.username, u.user_type, o.activation_condition, o.status, u.id AS user_id,
				e.id AS escort_id, e.showname, a.name AS agency_name, bu.id AS sales_id, o.use_balance, tr.comment AS rejection_comment,
				o.taken_from_balance, t.transfer_type_id 
			FROM orders o
			INNER JOIN backend_users bu ON bu.id = o.backend_user_id
			INNER JOIN order_packages op ON op.order_id = o.id
			INNER JOIN users u ON u.id = o.user_id
			LEFT JOIN escorts e ON e.id = op.escort_id
			LEFT JOIN agencies a ON a.user_id = o.user_id
			LEFT JOIN transfer_orders tor ON tor.order_id = o.id
			LEFT JOIN transfers t ON tor.transfer_id = t.id
			LEFT JOIN transfer_rejections tr ON tr.transfer_id = tor.transfer_id 
			WHERE o.id = ?
			GROUP BY e.id
		', array($id));

		$order->packages = self::getAdapter()->fetchAll('
			SELECT
				op.id AS order_package_id, p.name, op.period, a.title AS application_title, c.iso AS application_iso, op.activation_type, op.status,
				op.discount, op.discount_fixed, op.discount_comment, op.surcharge, op.price, UNIX_TIMESTAMP(op2.expiration_date) AS previous_package_expiration_date, op2.id AS previous_order_package_id,
				e.showname, op.base_price, op.date_activated AS activation_date, op.expiration_date, e.id AS escort_id,
				UNIX_TIMESTAMP(op.activation_date) AS act_date ' . $field . '
			FROM order_packages op
			INNER JOIN packages p ON p.id = op.package_id
			INNER JOIN escorts e ON e.id = op.escort_id
			INNER JOIN applications a ON a.id = op.application_id
			LEFT JOIN countries c ON c.id = a.country_id ' . $join . '
			LEFT JOIN order_packages op2 ON op2.id = op.previous_order_package_id
			WHERE op.order_id = ?
		', array($order->id));

		$gotd = array();
		$votd = array();
		$opt_products = array();
		
		if ( count($order->packages) ) {
			foreach ( $order->packages as $package ) {
				
				$gotd[] = self::getAdapter()->fetchAll('
					SELECT
						e.showname, p.name, UNIX_TIMESTAMP(go.activation_date) AS activation_date, go.status,
						cc.' . Cubix_I18n::getTblField('title') . ' AS city_title,
						t.transaction_id, t.transfer_type_id
					FROM gotd_orders go
					INNER JOIN order_packages op ON op.id = go.order_package_id
					INNER JOIN packages p ON p.id = op.package_id
					INNER JOIN escorts e ON e.id = op.escort_id
					INNER JOIN applications a ON a.id = op.application_id
					LEFT JOIN countries c ON c.id = a.country_id
					INNER JOIN cities cc ON cc.id = go.city_id
					LEFT JOIN transfers t ON t.id = go.transfer_id
					WHERE op.id = ? AND go.status IN (2, 3)
					ORDER BY go.activation_date DESC
				', array($package->order_package_id));

				if (Cubix_Application::getId() == APP_A6){
					$votd[] = self::getAdapter()->fetchAll('
						SELECT
							e.showname, p.name, UNIX_TIMESTAMP(vo.activation_date) AS activation_date, vo.status,
							cc.' . Cubix_I18n::getTblField('title') . ' AS city_title,
							t.transaction_id, t.transfer_type_id
						FROM votd_orders vo
						INNER JOIN order_packages op ON op.id = vo.order_package_id
						INNER JOIN packages p ON p.id = op.package_id
						INNER JOIN escorts e ON e.id = op.escort_id
						INNER JOIN applications a ON a.id = op.application_id
						LEFT JOIN countries c ON c.id = a.country_id
						INNER JOIN cities cc ON cc.id = vo.city_id
						LEFT JOIN transfers t ON t.id = vo.transfer_id
						WHERE op.id = ? AND vo.status IN (2, 3)
						ORDER BY vo.activation_date DESC
					', array($package->order_package_id));
				}
				$opt_products[] = self::getAdapter()->fetchAll('
					SELECT
						p.id, p.name, opp.price
					FROM order_package_products opp
					INNER JOIN products p ON p.id = opp.product_id
					WHERE opp.order_package_id = ? AND opp.is_optional = 1
				', array($package->order_package_id));
				
			}
		}
		
		$order->gotd = $gotd;
		$order->votd = $votd;
		$order->opt_products = $opt_products;

		return $order;
	}
	
	public function getDetailsSimple($id)
	{
		$order = parent::_fetchRow('
			SELECT o.id, o.system_order_id, UNIX_TIMESTAMP(o.order_date) AS order_date, o.price, UNIX_TIMESTAMP(op.expiration_date) AS expiration_date, p.name AS package_name
			FROM orders o
			LEFT JOIN order_packages op ON o.id = op.order_id
			LEFT JOIN packages p ON p.id = op.package_id
			WHERE o.id = ?
		', array($id));
		
		if ( $order->expiration_date ) {
			$order->exp_date = strtotime('-1 day', $order->expiration_date);
			$order->expiration_date = date("d M Y", strtotime('-1 day', $order->expiration_date)); //-1 day
		}
		
		$packages = self::getAdapter()->fetchAll('
			SELECT
				op.id AS order_package_id, p.name, op.period, a.title AS application_title, c.iso AS application_iso, op.activation_type,
				op.activation_date, op.discount, op.discount_fixed, op.discount_comment, op.surcharge, op.price,
				e.showname, op.base_price
			FROM order_packages op
			INNER JOIN packages p ON p.id = op.package_id
			INNER JOIN escorts e ON e.id = op.escort_id
			INNER JOIN applications a ON a.id = op.application_id
			INNER JOIN countries c ON c.id = a.country_id
			WHERE op.order_id = ?
		', array($order->id));
		
		/*if ( count($packages) ) {
			foreach($packages as $i => $package) {
				$packages[$i]->exp_date = strtotime('-1 day', $package->expiration_date);
				$packages[$i]->expiration_date = date("d M Y", strtotime('-1 day', $package->expiration_date)); //-1 day				
			}
		}*/
		
		$order->packages = $packages;

		return $order;
	}

	/*public function delete($id)
	{
		parent::remove(self::quote('id = ?', $id));
	}*/

	public function isOrderIdExist($order_id)
	{
		$sql = "SELECT TRUE FROM orders WHERE system_order_id = ?";

		return parent::getAdapter()->fetchOne($sql, array($order_id));
	}

	public function getEscortIdByOrderId($order_id)
	{
		$sql = "SELECT escort_id FROM order_packages WHERE order_id = ?";

		return parent::getAdapter()->fetchOne($sql, array($order_id));
	}
	
	public function edit($data, $order_id)
	{
		$order_data = $data['order_data'];
		$add_data = $data['additional_data'];

		$m_packages = new Model_Packages();

		try {
			parent::getAdapter()->beginTransaction();

			parent::getAdapter()->update('orders', array(
				'activation_condition' => $order_data['activation_condition'],
				'backend_user_id' => $order_data['backend_user_id'],
				'price' => $order_data['price'],
				'price_package' => $order_data['price_package'],
				'use_balance' => $order_data['use_balance']
			), parent::quote('id = ?', $order_id));

			$order_package_id = parent::getAdapter()->query("SELECT id, escort_id FROM order_packages WHERE order_id = ?", array($order_id))->fetch();

			parent::getAdapter()->query('DELETE FROM order_packages WHERE order_id = ?', array($order_id));
            Cubix_SyncNotifier::notify($order_package_id->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PACKAGE_MODIFIED_UPDATED, array('id' => $order_package_id->id,'escort_id' => $order_package_id->escort_id, 'backend_user_id' => $order_data['backend_user_id'],'Deleted_Order_id' => $order_id));

			parent::getAdapter()->query('DELETE FROM premium_escorts WHERE order_package_id = ?', array($order_package_id->id));
			if ( Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT ) {
				parent::getAdapter()->query('DELETE FROM additional_areas WHERE order_package_id = ?', array($order_package_id->id));
			}
			if ( Cubix_Application::getId() == APP_BL ) {
				parent::getAdapter()->query('DELETE FROM package_extra_cities WHERE order_package_id = ?', array($order_package_id->id));
			}
			//parent::getAdapter()->query('DELETE FROM premium_tours WHERE order_package_id = ?', array($order_package_id->id));

			foreach ($add_data as $add)
			{
				$package_data = array(
					'order_id' => $order_id,
					'escort_id' => $add['pa_esc_id'],
					'package_id' => $add['package'],
					'application_id' => $add['app_id'],
					'base_period' => $add['package_data'][0]->period,
					'period' => $add['period'],
					'activation_type' => $add['activate_promo_package'],
					'discount' => $add['discount'],
					'discount_fixed' => $add['fix_discount'],
					'discount_comment' => $add['comment'],
					'surcharge' => $add['surcharge'],
					'base_price' => $add['package_data'][0]->price,
					'price' => $add['price'],
					'gotd_activation_date' => $add['gotd_activation_date'],
					'gotd_city_id' => $add['gotd_city_id'],
					'de_days' => $add['de_days']
				);
				
				if ( $add['gotd_activation_date'] ) {
					
					if ( date('d-m-Y', time()) < date('d-m-Y', $add['gotd_activation_date']) ) {
						$add['gotd_activation_date'] = mktime(0, 1, 0, date('m', $add['gotd_activation_date']), date('d', $add['gotd_activation_date']), date('Y', $add['gotd_activation_date']), 0);
						
					}
					
					$package_data['gotd_activation_date'] = new Zend_Db_Expr("FROM_UNIXTIME({$add['gotd_activation_date']})");
					$package_data['gotd_status'] = Model_Products::GOTD_STATUS_PENDING;
					
					$package_data['gotd_city_id'] = $add['gotd_city_id'];
				}
				else {
					$package_data['gotd_activation_date'] = null;
					$package_data['gotd_status'] = null;
					
					$package_data['gotd_city_id'] = null;
				}
				
				
				if ( $add['de_days'] ) {
					$package_data['de_days'] = $add['de_days'];
				}
				else {
					$package_data['de_days'] = 0;
				}

				/*if ( $add['activate_promo_package'] == Model_Packages::ACTIVATE_AT_EXACT_DATE ) {
					//$add['activation_date'] = strtotime($add['activation_date']);
					$add['activation_date'] = mktime(date("H", $add['activation_date']), '00', '00', date("m", $add['activation_date']), date("d", $add['activation_date']), date("Y", $add['activation_date']));
					$package_data['activation_date'] = new Zend_Db_Expr("FROM_UNIXTIME({$add['activation_date']})");
				}*/
				
				if ( $add['activate_promo_package'] == Model_Packages::ACTIVATE_AT_EXACT_DATE ) {
					$add['activation_date'] = mktime(date("H", $add['activation_date']), '00', '00', date("m", $add['activation_date']), date("d", $add['activation_date']), date("Y", $add['activation_date']));
					$package_data['activation_date'] = new Zend_Db_Expr("FROM_UNIXTIME({$add['activation_date']})");
				} elseif ( $add['activate_promo_package'] == Model_Packages::ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES )  {
					$pack = new Model_Packages();
					$active_package = $pack->getActivePackage($package_data['escort_id']);
					$package_data['previous_order_package_id'] = $active_package;
				}

				parent::getAdapter()->insert('order_packages', $package_data);
				$new_order_package_id = parent::getAdapter()->lastInsertId();

				if ( count($add['premium_city_spots']) > 0 ) {
					foreach ( $add['premium_city_spots'] as $city_spot ) {
						parent::getAdapter()->insert('premium_escorts', array('order_package_id' => $new_order_package_id, 'city_id' => $city_spot));
					}
				}
				
				if ( Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT ) {
					if ( count($add['add_areas']) > 0 ) {
						foreach ( $add['add_areas'] as $add_area ) {
							parent::getAdapter()->insert('additional_areas', array('order_package_id' => $new_order_package_id, 'escort_id' => $add['escort_data']['id'], 'area_id' => $add_area));
						}
						parent::getAdapter()->update('order_packages', array('add_areas_count' => count($add['add_areas'])), parent::getAdapter()->quoteInto('id = ?', $new_order_package_id));
					}
				}
				
				if ( Cubix_Application::getId() == APP_BL ) {
					if ( count($add['extra_country_cities']) == 2 && $add['extra_premium_city'] ) {
						foreach ( $add['extra_country_cities'] as $e_city ) {
							parent::getAdapter()->insert('package_extra_cities', array(
								'order_package_id' => $new_order_package_id, 
								'city_id' => $e_city,
								'is_premium' => $e_city == $add['extra_premium_city'] ? 1 : 0
							));
						}
					}
					
					if ( count($add['extra_vip_country'])) {
						foreach ( $add['extra_vip_country'] as $e_country ) {
							parent::getAdapter()->insert('package_vip_extra_countries', array(
								'order_package_id' => $new_order_package_id, 
								'country_id' => $e_country
							));
						}
					}
				}
				
				parent::getAdapter()->update('gotd_orders', array(
					'order_package_id' => $new_order_package_id
				), parent::getAdapter()->quoteInto('order_package_id = ?', $order_package_id->id));
				
				/*if ( count($add['premium_tour_spots']) > 0 ) {
					foreach ( $add['premium_tour_spots'] as $tour_spot ) {
						parent::getAdapter()->insert('premium_tours', array('order_package_id' => $new_order_package_id, 'tour_id' => $tour_spot));
					}
				}*/
                $escort_type = null;
                if (Cubix_Application::getId() == APP_ED){
                    $escort_type = Model_EscortV2Item::getEscortTypeByID($add['pa_esc_id']);
                }
				$products = $m_packages->getProducts($add['package'], $add['app_id'], $add['escort_data']->agency_id, $add['escort_data']->gender, false, null, $escort_type);

				if ( count($products) > 0 )
				{
					foreach( $products as $product )
					{
						$package_product_data = array(
							'order_package_id' => $new_order_package_id,
							'product_id' => $product->id,
							'is_optional' => 0,
							'price' => $product->price
						);
						parent::getAdapter()->insert('order_package_products', $package_product_data);
					}
				}

				if ( count($add['opt_products_data']) > 0 ) {
					foreach ( $add['opt_products_data'] as $opt_product ) {
						$package_product_data = array(
							'order_package_id' => $new_order_package_id,
							'product_id' => $opt_product->id,
							'is_optional' => 1,
							'price' => $opt_product->price
						);
						parent::getAdapter()->insert('order_package_products', $package_product_data);
					}
				}
			}

			parent::getAdapter()->commit();
			Zend_Registry::get('BillingHooker')->notify('order_edited', array($order_id));

			Model_Activity::getInstance()->log('create_order', array('order id' => $order_id));
		}
		catch (Exception $e) {
			parent::getAdapter()->rollBack();

			throw $e;
		}
		
	}

	public function save($data)
	{
		$order_data = $data['order_data'];
		$add_data = $data['additional_data'];

		$m_packages = new Model_Packages();

		try {
			parent::getAdapter()->beginTransaction();
			
			parent::getAdapter()->insert('orders', $order_data);
			$new_order_id = parent::getAdapter()->lastInsertId();

			foreach ($add_data as $add)
			{
				$package_data = array(
					'order_id' => $new_order_id,
					'escort_id' => $add['pa_esc_id'],
					'package_id' => $add['package'],
					'application_id' => $add['app_id'],
					'base_period' => $add['package_data'][0]->period,
					'period' => $add['period'],
					'activation_type' => $add['activate_promo_package'],
					'discount' => $add['discount'],
					'discount_fixed' => $add['fix_discount'],
					'discount_comment' => $add['comment'],
					'surcharge' => $add['surcharge'],
					'base_price' => $add['package_data'][0]->price,
					'price' => $add['price'],
					
					'gotd_activation_date' => $add['gotd_activation_date'],
					'gotd_city_id' => $add['gotd_city_id'],
					'de_days' => $add['de_days']
				);

				if ( $add['gotd_activation_date'] ) {
					if ( date('d-m-Y', time()) < date('d-m-Y', $add['gotd_activation_date']) ) {
						$add['gotd_activation_date'] = mktime('0', 1, 0, date('m', $add['gotd_activation_date']), date('d', $add['gotd_activation_date']), date('Y', $add['gotd_activation_date']), 0);
						
					}
					
					$package_data['gotd_activation_date'] = new Zend_Db_Expr("FROM_UNIXTIME({$add['gotd_activation_date']})");
					$package_data['gotd_status'] = Model_Products::GOTD_STATUS_PENDING;
					
					$package_data['gotd_city_id'] = $add['gotd_city_id'];
				}
				else {
					$package_data['gotd_activation_date'] = null;
					$package_data['gotd_status'] = null;
					$package_data['gotd_city_id'] = null;
				}
				
				if ( $add['de_days'] ) {
					$package_data['de_days'] = $add['de_days'];
				}
				else {
					$package_data['de_days'] = 0;
				}
				
				if ( $add['activate_promo_package'] == Model_Packages::ACTIVATE_AT_EXACT_DATE ) {
					$add['activation_date'] = mktime(date("H", $add['activation_date']), '00', '00', date("m", $add['activation_date']), date("d", $add['activation_date']), date("Y", $add['activation_date']));
					$package_data['activation_date'] = new Zend_Db_Expr("FROM_UNIXTIME({$add['activation_date']})");
				} elseif ( $add['activate_promo_package'] == Model_Packages::ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES )  {
					$pack = new Model_Packages();
					$active_package = $pack->getActivePackage($package_data['escort_id']);
					$package_data['previous_order_package_id'] = $active_package;
				}

				if(Cubix_Application::getId() == APP_ED){
                    $package_data['country_id'] = $add['country_id'];
                }
				
				parent::getAdapter()->insert('order_packages', $package_data);
				$new_order_package_id = parent::getAdapter()->lastInsertId();

				if ( count($add['premium_city_spots']) > 0 ) {
					foreach ( $add['premium_city_spots'] as $city_spot ) {
						parent::getAdapter()->insert('premium_escorts', array('order_package_id' => $new_order_package_id, 'city_id' => $city_spot));
					}
				}
				
				if ( Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT ) {
					if ( count($add['add_areas']) > 0 ) {
						foreach ( $add['add_areas'] as $add_area ) {
							parent::getAdapter()->insert('additional_areas', array('order_package_id' => $new_order_package_id, 'escort_id' => $add['escort_data']['id'], 'area_id' => $add_area));
						}
						parent::getAdapter()->update('order_packages', array('add_areas_count' => count($add['add_areas'])), parent::getAdapter()->quoteInto('id = ?', $new_order_package_id));
					}
				}
				
				if ( Cubix_Application::getId() == APP_BL ) {
					if ( count($add['extra_country_cities']) == 2 && $add['extra_premium_city'] ) {
						foreach ( $add['extra_country_cities'] as $e_city ) {
							parent::getAdapter()->insert('package_extra_cities', array(
								'order_package_id' => $new_order_package_id, 
								'city_id' => $e_city,
								'is_premium' => $e_city == $add['extra_premium_city'] ? 1 : 0
							));
						}
					}
					
					if ( count($add['extra_vip_country'])) {
						foreach ( $add['extra_vip_country'] as $e_country ) {
							parent::getAdapter()->insert('package_vip_extra_countries', array(
								'order_package_id' => $new_order_package_id, 
								'country_id' => $e_country
							));
						}
					}
				}
                $escort_type = null;
				if (Cubix_Application::getId() == APP_ED){
				    $escort_type = Model_EscortV2Item::getEscortTypeByID($add['pa_esc_id']);
                }
				$products = $m_packages->getProducts($add['package'], $add['app_id'], $add['escort_data']->agency_id, $add['escort_data']->gender, false, null, $escort_type);

				if ( count($products) > 0 )
				{
					foreach( $products as $product )
					{
						$package_product_data = array(
							'order_package_id' => $new_order_package_id,
							'product_id' => $product->id,
							'is_optional' => 0,
							'price' => $product->price
						);
						parent::getAdapter()->insert('order_package_products', $package_product_data);
					}
				}

				if ( count($add['opt_products_data']) > 0 ) {
					foreach ( $add['opt_products_data'] as $opt_product ) {
						$package_product_data = array(
							'order_package_id' => $new_order_package_id,
							'product_id' => $opt_product->id,
							'is_optional' => 1,
							'price' => $opt_product->price
						);
						parent::getAdapter()->insert('order_package_products', $package_product_data);
					}
				}
			}

			parent::getAdapter()->commit();
			Zend_Registry::get('BillingHooker')->notify('order_created', array($new_order_id));

			Model_Activity::getInstance()->log('create_order', array('order id' => $new_order_id));

			return $new_order_id;
		}
		catch (Exception $e) {
			parent::getAdapter()->rollBack();

			throw $e;
		}
	}

	public function pay($order_id, &$fake_balance = null)
	{
		$order = $this->getDetails($order_id);
		//var_dump("use_balance:" . $order->use_balance);
		if ( $order->use_balance ) {
			$balance = self::getAdapter()->fetchOne('
				SELECT balance FROM users WHERE id = ?
			', $order->user_id);
		}
		else {			

			$confirmed_transfers = self::getAdapter()->fetchAll('
				SELECT t.user_id, t.amount, tc.amount AS paid_amount
				FROM transfers t
				INNER JOIN transfer_confirmations tc ON tc.transfer_id = t.id
				INNER JOIN transfer_orders tor ON tor.transfer_id = t.id
				WHERE t.status = ? AND tor.order_id = ?
			', array(Model_Billing_Transfers::STATUS_CONFIRMED, $order_id));

			if ( $confirmed_transfers ) {
				foreach ($confirmed_transfers as $transfer) {
					$balance += $transfer->paid_amount;
				}
			} else {
				$balance = $fake_balance;
			}
		}
		
		//var_dump("balance:" . $balance);
		//var_dump("taken_from_balance:" . $order->taken_from_balance);

		if ( $order->use_balance && ! $order->taken_from_balance ) {
			$order->taken_from_balance = null;
			if ( $balance > $order->price ) {
				$order->taken_from_balance = $order->price;
			}
			else {
				$order->taken_from_balance = $balance;
			}
			self::getAdapter()->update('orders', array(
				'taken_from_balance' => $order->taken_from_balance
			), self::quote('id = ?', $order->id));
		}
		
		//var_dump("order_price:" . $order->price);
		//var_dump(doubleval($balance) < doubleval($order->price));

		if ( doubleval($balance) < doubleval($order->price) ) {
			return false;
		}

		self::getAdapter()->beginTransaction();
		try {
			if ( $order->use_balance ) {
				self::getAdapter()->query('UPDATE users SET balance = (balance - ?) WHERE id = ?', array($order->price, $order->user_id));
			}
			else {
				$fake_balance -= $order->price;
			}
			//var_dump($order->price);
			self::getAdapter()->update('orders', array(
				'status' => Model_Billing_Orders::STATUS_PAID,
				'payment_date'	=> new Zend_Db_Expr('NOW()')
			), self::quote('id = ?', $order->id));
			

			self::getAdapter()->commit();
			Zend_Registry::get('BillingHooker')->notify('order_paid', array($order_id));

			return false;
		}
		catch (Exception $e) {
			self::getAdapter()->rollBack();

			throw $e;
		}
	}

	public function getPrice($order_id)
	{
		return parent::getAdapter()->fetchOne('
			SELECT price FROM orders WHERE id = ?
		', $order_id);
	}

	/**
	 * This function is beeing called only when escort or agency is deleted
	 *
	 * @param int $user_id
	 */
	public function closeByUserId($user_id)
	{
		try {
			self::getAdapter()->beginTransaction();
			$orders = self::getAdapter()->fetchAll('SELECT id FROM orders WHERE user_id = ?', $user_id);
			foreach ( $orders as $order ) {
				$order = new Model_Billing_OrderItem(array('id' => $order->id));
				$order->close('User #' . $user_id . ' has been deleted');
			}

			self::getAdapter()->commit();
		}
		catch ( Exception $e ) {
			self::getAdapter()->rollBack();

			throw $e;
		}
		
	}


	public function removeOrder($order_id)
	{
		$transfer_id = self::getAdapter()->fetchOne('SELECT transfer_id FROM transfer_orders WHERE order_id = ?', $order_id);
		if ( $transfer_id ) {

			//Checking count of orders with this transfer
			$orders_count = self::getAdapter()->fetchOne('SELECT count(order_id) FROM transfer_orders WHERE transfer_id = ?', $transfer_id);

			//remove transfer when only 1 order paid with it
			if ( $orders_count == 1 ) {
				parent::getAdapter()->query('
					DELETE FROM transfers WHERE id = ?
				', $transfer_id);
			}
		}
		
		return parent::getAdapter()->query('
			DELETE FROM orders WHERE id = ?
		', $order_id);
	}

	public function closeOrder($order_id)
	{
		return parent::getAdapter()->query('
			UPDATE orders SET status = ? WHERE id = ?
		', array(Model_Billing_Orders::STATUS_CLOSED, $order_id));
	}

	public function updateSalesPerson($o_id, $sales_id)
	{
		$order_data = parent::getAdapter()->fetchRow('SELECT backend_user_id AS old_sales_id, system_order_id FROM orders WHERE id = ?', $o_id);
		
		parent::getAdapter()->update('orders', array('backend_user_id' => $sales_id), parent::getAdapter()->quoteInto('id = ?', $o_id));
		
		if ( $order_data->old_sales_id != $sales_id ) {
			
			$old_sales = parent::getAdapter()->fetchRow('SELECT * FROM backend_users WHERE id = ?', $order_data->old_sales_id);
			$new_sales = parent::getAdapter()->fetchRow('SELECT * FROM backend_users WHERE id = ?', $sales_id);
			
			if ( $new_sales->email ) {
				$email_data = array(
					'old_sales_name' => $old_sales->username,
					'system_order_id' => $order_data->system_order_id,
					'date_now' => date("d M Y", time())
				);
				Cubix_Email::sendTemplate('order_switch_sales_person', $new_sales->email, $email_data);
			}
		}
	}


	public function processUserOrders($user_id)
	{
		//
		do {
			$result = Model_Billing_ExactCron::run($user_id, array('pending-orders', 'expired-packages', 'asap-packages', 'exact-date-packages'));
		} while ( $result['count'] > 0 );
	}
	
	public function getPremiumCitiesByOpId($order_package_id)
	{
		$sql = "
			SELECT c.title_en FROM premium_escorts pe
			INNER JOIN cities c ON c.id = pe.city_id
			WHERE pe.order_package_id = ?
		";
				
		$cities = parent::getAdapter()->fetchAll($sql, array($order_package_id));
		
		
		if ( count($cities) > 0 ) {
			foreach ($cities as $k => $city) {
				$ct_str .= $city->title_en;
				if ( $k + 1 < count($cities) ) {
					$ct_str .= ", ";
				}
			}
		}
		else {
			$ct_str = "-";
		}
		
		return $ct_str;
	}
	
	/**
	 * Returns a data which needs to be exported as CSV, the task about ascii files
	 * requested by Davide
	 *
	 * @param array $filter
	 * @param type $page
	 * @param type $per_page
	 * @param type $sort_field
	 * @param type $sort_dir
	 * @return array
	 */
	public function getDataForExport(array $filter = array(), $page = 1, $per_page = 10, $sort_field = 'order_date', $sort_dir = 'DESC')
	{
		$sql = array(
			'tables' => array('orders o'),
			'fields' => array(
				'ee.showname','o.id', 'u.user_type', 'u.username', 'o.system_order_id AS order_id', 'o.status AS status',
				'GROUP_CONCAT(CONCAT(ee.showname, "(", op.escort_id, ")", " - ", TRIM(p.name)) SEPARATOR "\n") AS packages', 'o.order_date', 'o.payment_date', 'o.price',
				'bu.username AS sales_person', '(o.price - IF(o.taken_from_balance IS NULL, 0, o.taken_from_balance)) AS `real`'
			),
			'joins' => array(
				'INNER JOIN backend_users bu ON bu.id = o.backend_user_id',
				'INNER JOIN order_packages op ON op.order_id = o.id',
				'INNER JOIN packages p ON p.id = op.package_id',
				'INNER JOIN users u ON u.id = o.user_id',
				'LEFT JOIN escorts e ON e.user_id = o.user_id AND u.user_type = "escort"',
				'LEFT JOIN agencies a ON a.user_id = o.user_id AND u.user_type = "agency"',
				'INNER JOIN escorts ee ON ee.id = op.escort_id',
			),
			'where' => array(
			) + $filter,
			'group' => 'o.id',
			'order' => array($sort_field, $sort_dir)
		);
				
		$dataSql = Cubix_Model::getSql($sql);

		$data = parent::_fetchAll($dataSql);
		
		foreach ( array_keys($data) as $i ) {
			$data[$i]->status = self::$STATUS_LABELS[$data[$i]->status];
		}

		return array(
			'data' => $data
		);
	}
	
	public function extendOrder($data)
	{
		$db = parent::db();
		
		$has_product = $db->fetchOne('SELECT true FROM order_package_products WHERE product_id = ? AND order_package_id = ?', array($data['opt_product'], $data['order_package_id']));
		
		if ( ! $has_product ) {
			$db->update('orders', array('price' => $data['product_price'] + $data['order_price']), $db->quoteInto('id = ?', $data['order_id']));

			$exp_date = new Zend_Db_Expr('DATE_ADD(expiration_date, INTERVAL ' . $data['days'] . ' DAY)');
			$db->update('order_packages', array('price' => $data['product_price'] + $data['package_price'], 'expiration_date' => $exp_date), $db->quoteInto('id = ?', $data['order_package_id']));
		}
		
		if ( ! $has_product ) {
			$db->insert('order_package_products', array('order_package_id' => $data['order_package_id'], 'product_id' => $data['opt_product'], 'is_optional' => 1, 'price' => $data['product_price']));
		}
	}
	
	public function addGotd($data)
	{
		$db = parent::db();
		
		try {
			$db->beginTransaction();
			$db->update('orders', array('price' => $data['total_price'] + $data['order_price']), $db->quoteInto('id = ?', $data['order_id']));
			$db->update('order_packages', array('price' => $data['total_price'] + $data['package_price']), $db->quoteInto('id = ?', $data['order_package_id']));

			foreach($data['days'] as $day) {
				$db->insert('gotd_orders', array(
					'creation_date'		=> date('Y-m-d H:i:s', time()),
					'activation_date'	=> date('Y-m-d', $day),
					'escort_id'			=> $data['escort_id'],
					'city_id'			=> $data['city_id'],
					'status'			=> 2,
					'order_package_id'	=> $data['order_package_id'],
					'transfer_id'		=> $data['transfer_id']
				));
			}
			$db->commit();
		} catch(Exception $ex) {
			$db->rollBack();
			throw $ex;
		}
		
	}

	public function addVotd($data)
	{
		$db = parent::db();
		
		try {
			$db->beginTransaction();
			$db->update('orders', array('price' => $data['total_price'] + $data['order_price']), $db->quoteInto('id = ?', $data['order_id']));
			$db->update('order_packages', array('price' => $data['total_price'] + $data['package_price']), $db->quoteInto('id = ?', $data['order_package_id']));

			foreach($data['days'] as $day) {
				$db->insert('votd_orders', array(
					'creation_date'		=> date('Y-m-d H:i:s', time()),
					'activation_date'	=> date('Y-m-d', $day),
					'escort_id'			=> $data['escort_id'],
					'city_id'			=> $data['city_id'],
					'status'			=> 2,
					'order_package_id'	=> $data['order_package_id'],
					'transfer_id'		=> $data['transfer_id']
				));
			}
			$db->commit();
		} catch(Exception $ex) {
			$db->rollBack();
			throw $ex;
		}
		
	}
	
	public function addExtraCountry($data)
	{
		$db = parent::db();
		
		try {
			$db->beginTransaction();
			
			$db->update('orders', array('price' => $data['total_price'] + $data['order_price']), $db->quoteInto('id = ?', $data['order_id']));
			$db->update('order_packages', array('price' => $data['total_price'] + $data['package_price']), $db->quoteInto('id = ?', $data['order_package_id']));
			$db->insert('order_package_products', array(
				'order_package_id' => $data['order_package_id'],
				'product_id' => $data['opt_product'],
				'is_optional' => 1,
				'price' => $data['total_price']
			));
			foreach ($data['extra_country'] as $city)
			{
				$db->insert('package_extra_cities', array(
					'order_package_id' => $data['order_package_id'],
					'city_id' => $city,
					'is_premium' => $data['extra_premium'] == $city ? 1 : 0
				));
			}
			
			$db->commit();
		} catch(Exception $ex) {
			$db->rollBack();
			throw $ex;
		}
	}
	
	public function updateGotd($go_id, $escort_id, $data)
	{
		$db = parent::db();
		
		try {
			
			$sql = 'SELECT id, date_activated, expiration_date FROM order_packages WHERE escort_id = ? AND status IN (1, 2) ORDER BY status DESC';
			$order_packages = parent::getAdapter()->fetchAll($sql, array($escort_id));
			
			if ( count($order_packages) == 1 ) {
				//If has one package means has no pending packages 
				//And gotd is bought for this package
				$data['order_package_id'] = $order_packages[0]->id;
			} else {
				//If has more than one package means has also pending package.
				//Must check for which one gotd is bought

				$expiration_date = strtotime('-1 day', strtotime($order_packages[0]->expiration_date));//Fixing 1 day problem
				
				//First package is active one
				if ( strtotime($data['activation_date']) > $expiration_date ) { 
					//Means bought for the next(pending) package
					$data['order_package_id'] = $order_packages[1]->id;
				}
				else {
					
					//Means bought for the current(active) package
					$data['order_package_id'] = $order_packages[0]->id;
				}
			}
			
			//making active GOTDs pending again so sync can activate it properly
			$data['status'] = 2;

			$db->update('gotd_orders', $data, array($db->quoteInto('id = ?', $go_id)));
			
		} catch(Exception $ex) {
			throw $ex;
		}
		
	}

	public function updateVotd($vo_id, $escort_id, $data)
	{
		$db = parent::db();
		
		try {
			
			$sql = 'SELECT id, date_activated, expiration_date FROM order_packages WHERE escort_id = ? AND status IN (1, 2) ORDER BY id ASC';
			$order_packages = parent::getAdapter()->fetchAll($sql, array($escort_id));

			if ( count($order_packages) == 1 ) {
				//If has one package means has no pending packages 
				//And gotd is bought for this package
				$data['order_package_id'] = $order_packages[0]->id;
			} else {
				//If has more than one package means has also pending package.
				//Must check for which one gotd is bought

				$expiration_date = strtotime('-1 day', strtotime($order_packages[0]->expiration_date));//Fixing 1 day problem

				//First package is active one
				if ( strtotime($data['activation_date']) > $expiration_date ) {
					//Means bought for the next(pending) package
					$data['order_package_id'] = $order_packages[1]->id;
				} else {
					//Means bought for the current(active) package
					$data['order_package_id'] = $order_packages[0]->id;
				}
			}

			$db->update('votd_orders', $data, array($db->quoteInto('id = ?', $vo_id)));
			
		} catch(Exception $ex) {
			throw $ex;
		}
		
	}
	
	public function getOweners($orders_str, $current_backend_user_id)
	{
		return parent::db()->query("
			SELECT b.email, o.backend_user_id
			FROM orders o 
			INNER JOIN backend_users b ON o.backend_user_id = b.id 
			WHERE o.id IN (" . $orders_str . ") AND o.backend_user_id <> ?
			GROUP BY o.backend_user_id
		", $current_backend_user_id)->fetchAll();
	}
	
	public function getSystemIds($orders_str, $backend_user_id)
	{
		return parent::db()->query("SELECT system_order_id FROM orders WHERE id IN (" . $orders_str . ") AND backend_user_id = ?", $backend_user_id)->fetchAll();
	}
	
	public function changeCCOrderStatus($order_id)
	{
		parent::db()->update('orders', array('status' => Model_Billing_Orders::STATUS_PAYMENT_DETAILS_RECEIVED, 'activation_condition' => Model_Billing_Orders::CONDITION_AFTER_RECEIVING_PAYMENT_DETAILS), parent::db()->quoteInto('id = ?', $order_id));
	}
}
