<?php

class Model_Billing_OnlineBilling extends Cubix_Model {
	
	static $secret_prefixes = array(
		2 => 'A7W9DC',
		1 => 'ipnI3Inr'
	);
	
	static $site_ids = array(
		1 => 3076
	);
	
	static $backend_users = array(
		1	=> array('id' => 99, 'username' => 'online_billing_manager', 'password' => '5YHs47vuNhq+^Q>+'),
		16	=> array('id' => 113, 'username' => 'online_billing_manager', 'password' => '!UU=ub8WwMk!^Qqp'),
		9	=> array('id' => 185, 'username' => 'online_billing_manager', 'password' => 'dk9SG5$Wv398hGsj'),
		19	=> array('id' => 185, 'username' => 'online_billing_manager', 'password' => 'dk9SG5$Wv398hGsj'),
		69	=> array('id' => 195, 'username' => 'online_billing_manager', 'password' => ",D3.,Y^N'>WaFN<e"),
		5	=> array('id' => 190, 'username' => 'online_billing_manager', 'password' => '5qM32cbyeWtJrXcW'),
		17	=> array('id' => 89, 'username' => 'online_billing_manager', 'password' => 'rXcW55cFFDWtJrXcW'),
	);
	
	
	private $_shopping_cart;
	private $_callback;
	private $_is_from_cardgateplus;

	private $_notify_numbers = array(
		//2 => array('0040753095167'),
		//1 => array('0040753095167')
	);
	//private $_notify_numbers = array('0041765070055', '0040753095167', '0037491636334');

	private $_backend_user_id;
	private $_callback_log_id;
	
	public function __construct($callback, $is_from_cardgateplus = false, $check_sha1 = true) {

		$application_id = Cubix_Application::getId();
		
		if ( ! isset(self::$backend_users[$application_id]) ) {
			throw new Exception('No backend user for online billing manager in this application.');
		}
		$this->_backend_user_id = self::$backend_users[$application_id]['id'];
		$this->_is_from_cardgateplus = $is_from_cardgateplus;
		$this->_check_sha1 = $check_sha1;
		$host = preg_replace('/^(backend|backoffice)./', '', $_SERVER['HTTP_HOST']);
		
		$this->view->application = Cubix_Application::getByHost($host);
		
		$auth = Zend_Auth::getInstance();
		$adapter = new Model_Auth_Adapter(self::$backend_users[$application_id]['username'], self::$backend_users[$application_id]['password'], $application_id, false);
		$result = $auth->authenticate($adapter);
		
		
		if ( ! $result->isValid() ) {
			throw new Exception('Invalid callback user specified in configs');
		}
		
		$user_id = intval($callback['reference']);
		$hash = $callback['hash'];
		$fields = '';
		if (Cubix_Application::getId() == APP_ED){
		    $fields = ', e.type as escort_type';
        }
		$shopping_cart = parent::_fetchAll("
			SELECT sc.*, e.gender AS escort_gender, e.showname AS escort_showname, u.user_type AS user_type, pseudo_escort{$fields}
			FROM shopping_cart AS sc
			INNER JOIN escorts e ON e.id = sc.escort_id
			INNER JOIN users u ON u.id = e.user_id
			WHERE sc.user_id = ? AND sc.hash = ? AND sc.status = 0
		", array($user_id, $hash));

		$this->setCallback($callback);
		$this->setShoppingCart($shopping_cart);
	}
	
	public function getShoppingCart()
	{
		return $this->_shopping_cart;
	}
	
	public function setShoppingCart($shopping_cart)
	{
		$this->_shopping_cart = $shopping_cart;
	}
	
	public function getCallback()
	{
		return $this->_callback;
	}
	
	public function setCallback($callback)
	{
		$this->_callback = $callback;
	}
	
	public function updateLogData($data)
	{
		if ( !is_array($data) ) {
			$data = array($data);
		}
		
		parent::getAdapter()->update('online_billing_log', array('data' => serialize($data)), parent::getAdapter()->quoteInto('id = ?', $this->_callback_log_id));
	}
	
	public function addCallbackLog()
	{
		parent::getAdapter()->insert('online_billing_log', array_merge(array(
			'transaction_time'	=> $this->_callback['transaction_time'],
			'transaction_id'	=> $this->_callback['transaction_id'],
			'reference'			=> $this->_callback['reference'],
			'status_id'			=> $this->_callback['status_id'],
			'currency'			=> $this->_callback['currency'],
			'amount'			=> $this->_callback['amount'],
			'first_name'		=> $this->_callback['first_name'],
			'last_name'			=> $this->_callback['last_name'],
			'address'			=> $this->_callback['address'],
			'zipcode'			=> $this->_callback['zipcode'],
			'city'				=> $this->_callback['city'],
			'country_code'		=> $this->_callback['country_code'],
			'email'				=> $this->_callback['email'],
			'phone_number'		=> $this->_callback['phone_number'],
			'ip_address'		=> $this->_callback['ip_address'],
			'card_name'			=> $this->_callback['card_name'],
			'card_number'		=> $this->_callback['card_number'],
			'exp_date'			=> $this->_callback['exp_date'],
			'sha1'				=> $this->_callback['sha1'],
			'log_data'			=> $this->_callback['log_data']
		), array('date' => date('Y-m-d H:i:s'))));

		$this->_callback_log_id = parent::getAdapter()->lastInsertId();
	}
	
	public function isTransactionIdExists($transaction_id, $payment_system = null)
	{
		if($payment_system){
			return parent::getAdapter()->fetchOne('SELECT TRUE FROM transfers WHERE transaction_id = ? AND payment_system = ?', array($transaction_id, $payment_system ));
		}
		else{
			return parent::getAdapter()->fetchOne('SELECT TRUE FROM transfers WHERE transaction_id = ?', array($transaction_id));
		}
	}
	
	public function notifyThroughSms($text)
	{
		$application_id = Cubix_Application::getId();
		
		$originator = $phone_from = Cubix_Application::getPhoneNumber($application_id);

		$config = Zend_Registry::get('system_config');

		if (Cubix_Application::getId() != APP_6B)
		{
			$sms_config = $config['sms'];

			$SMS_USERKEY = $sms_config['userkey'];
			$SMS_PASSWORD = $sms_config['password'];

			$sms = new Cubix_SMS($SMS_USERKEY, $SMS_PASSWORD);
			$sms->setOriginator($originator);

			$notify_numbers = $this->_notify_numbers[$application_id];
			foreach ( $notify_numbers as $phone_to ) {
				$sms->setRecipient($phone_to, md5(microtime()));
				$sms->setContent($text);
				$sms->sendSMS();
			}
		}
		else
		{
//			$sms_config = $config['sms_6b'];
//
//			$SMS_USERKEY = $sms_config['user'];
//			$SMS_PASSWORD = $sms_config['password'];

			$sms = Cubix_6B_SMS::getInstance();

			$notify_numbers = $this->_notify_numbers[$application_id];
			$sms->send_sms($notify_numbers, $text,'', date('d/M/Y'));
//			foreach ( $notify_numbers as $phone_to ) {
//				$sms->setDestination($phone_to);
//				$sms->setContent($text);
//				$sms->send();
//			}
		}
	}
	
	public function pay($payment_type = null)
	{
		$application_id = Cubix_Application::getId();
		$user_id = $this->_callback['reference'];

		if ( ! $this->_shopping_cart ) {
			$this->updateLogData('User with id ' . $user_id . ' doesn\'t have shopping card');
			return false;
		}
		
		$transfer_type_id = 3;
		
		if($payment_type == '2000charge'){
			$transfer_type_id = 10; // Paysafe
		}
		
		if($payment_type == 'coinsome'){
			$transfer_type_id = 11; 
		}
		
		$sales_user_id = parent::db()->fetchOne('SELECT sales_user_id FROM users WHERE id = ?', array($user_id));
		
		parent::db()->beginTransaction();
		try {
			$system_order_id = $this->_getNewOrderId();
			
			// <editor-fold defaultstate="collapsed" desc="Inserting order row">
			$order_data = array(
				'user_id'	=> $user_id,
				'backend_user_id'	=> $sales_user_id, //$this->_backend_user_id,
				'status' => Model_Billing_Orders::STATUS_PENDING,
				'activation_condition' => Model_Billing_Orders::CONDITION_AFTER_PAID,
				'order_date' => new Zend_Db_Expr('NOW()'),
				'payment_date'	=> new Zend_Db_Expr('NOW()'),
				'system_order_id' => $system_order_id,
				'price' => 1, // Temporary price. Will be updated after foreach
				'price_package' => 1, //Temporary price. Will be updated after foreach
				'packages_count'	=> count($this->_shopping_cart),
				'application_id' => $application_id,
				'use_balance' => 0,
				'is_self_checkout' => 1
			);
			
			if($payment_type == '2000charge'){
				$order_data['activation_condition'] = Model_Billing_Orders::CONDITION_AFTER_RECEIVING_PAYMENT_DETAILS ; // Paysafe
			}

			parent::db()->insert('orders', $order_data);
			$order_id = parent::db()->lastInsertId();
			Model_Activity::getInstance()->log('create_order', array('order id' => $order_id));
			// </editor-fold>

			// <editor-fold defaultstate="collapsed" desc="Creating transfer">
			$exp_date = explode('/', $this->_callback['exp_date']);

			$transfer_array = array(
				'user_id' => $user_id,
				'transfer_type_id' => $transfer_type_id,
				'date_transfered' => new Zend_Db_Expr('NOW()'),
				'amount' => 1, // Temporary price. Will be updated after foreach
				'application_id' => $application_id,
				'status'	=> Model_Billing_Transfers::STATUS_PENDING,
				'backend_user_id'	=> $sales_user_id, //$this->_backend_user_id,
				'first_name'	=> $this->_callback['first_name'],
				'last_name'	=> $this->_callback['last_name'],
				'transaction_id' => $this->_callback['transaction_id'],
				'is_self_checkout' => 1,
				'cc_number'	=> $this->_callback['card_number'],
				'cc_address'	=> $this->_callback['address'],
				'cc_ip'			=> $this->_callback['ip_address'],
				'cc_holder'		=> $this->_callback['card_name'],
				'cc_exp_day'	=> (int)$exp_date[1],
				'cc_exp_month'	=> (int)$exp_date[0]
			);

			if ( $application_id == APP_A6 ) {
				$transfer_array['phone_billing_paid'] = 1;
			}
            if ($application_id == APP_EG_CO_UK) {
                // Inserting banner price to transfers  -  EGUK-299
                $parsed_data = unserialize($this->_shopping_cart[0]->data);
                if (isset($parsed_data['banner_price'])) {
                    $banner_price = $parsed_data['banner_price'];

                } else {
                    $banner_price = 0;
                }
                $transfer_array['banner_price'] = $banner_price;
            }

			if (in_array($application_id, array(APP_ED, APP_EG_CO_UK))) {
				$transfer_array['status'] = Model_Billing_Transfers::STATUS_CONFIRMED;
			}
			
			if ( in_array($application_id,array(APP_EF,APP_A6)) && isset($this->_shopping_cart[0])) {
				$transfer_array['is_mobile'] = $this->_shopping_cart[0]->is_mobile;
			}

            if ($application_id== APP_EF){
                $transfer_array['client_cookie_id'] = $this->_shopping_cart[0]->client_cookie_id;
                $transfer_array['ip'] = $this->_shopping_cart[0]->ip;
            }

			if(isset($payment_type)){
				$transfer_array['payment_system'] = $payment_type;
				
				/*if($payment_type == 'bitcoin'){
					$transfer_array['settled_amount'] = $this->_callback['settled_amount'];
				}*/
			}


			parent::db()->insert('transfers', $transfer_array);
			$transfer_id = parent::db()->lastInsertId();

			parent::db()->insert('transfer_orders', array(
				'transfer_id'	=> $transfer_id,
				'order_id'		=> $order_id
			));
			Model_Activity::getInstance()->log('add_transfer', array('transfer id' => $transfer_id));
			// </editor-fold>
			
			$m_package = new Model_Packages();
			$m_products = new Model_Products();
			
			$total_price = 0;
			$total_price_package = 0;

			foreach( $this->_shopping_cart as $order ) {
				$data = unserialize($order->data);
				
				if ( $order->user_type == 'agency' ) {
					$is_agency = true;
					$user_type = 2;
				} else {
					$user_type = null;
				}
				
				if ( $order->pseudo_escort == 1 ) {
					$user_type = 2;
				}
				
				$profile_type = null;
				if ( in_array(Cubix_Application::getId(), array(APP_EF, APP_6A)) ) {
					$profile_type = parent::db()->fetchOne('SELECT profile_type FROM escorts WHERE id = ?', $order->escort_id);
				}

				/*if ( $user_id == 467383 ) {
					parent::db()->rollBack();
					$this->updateLogData($order->escort_id. '-' .$application_id . '-' . $user_type . '-' .$order->escort_gender . '-' . $order->package_id);
					return false;
				}*/
				$escort_type = null;
				if ($application_id == APP_ED){
				    $escort_type = $order->escort_type;
                }
				$package = $m_package->getWithPrice($application_id, $user_type, $order->escort_gender, $order->package_id, false, null, $profile_type, $escort_type);
				
				/*if ( $order->escort_id == 82807 ) {
					parent::db()->rollBack();
					$this->updateLogData($user_type . '-' . var_export($package[0]->price));
					return false;
				}*/
				
				
				if ( $data['optional_products'] && count($data['optional_products']) ) {
					foreach($data['optional_products'] as $k => $v) {
						$opt_product = $m_products->getWithPrice($application_id, $user_type, $order->escort_gender, $v, false,$escort_type);
						if ( $opt_product ) {
							$data['optional_products'][] = array('id' => $opt_product->id, 'price' => $opt_product->price);
						}
						unset($data['optional_products'][$k]);
					}
				}
				
				$period = $package[0]->period;
				if ( $data['period'] ) {
					$period = $data['period'];
				}

				// <editor-fold defaultstate="collapsed" desc="Calculating package price">
				$price_data = array(
					'package_price' => $package[0]->price,
					'default_period' => $package[0]->period,
					'period' => $period,
					'discount' => $data['discount'],
					'opt_product_prices' => array()
				);
				
				if ( count($data['optional_products']) ) {
					$price_data['opt_product_prices'] = $data['optional_products'];
				}
				
				$package_price = $this->_calculatePrice($price_data);
				$package_price_without_gotd = $this->_calculatePrice($price_data, true);

				$total_price += $package_price;
				$total_price_package += $package_price_without_gotd;
				// </editor-fold>
				
				// <editor-fold defaultstate="collapsed" desc="Inserting order packages row">
				
				//Checking if has active package. IF has set activation type to after current pacakge expires
                //<promo-code-fold defaultstate="collapsed" desc="Calculating promo code preiod">
                if( in_array($application_id,array( APP_EG_CO_UK,APP_ED)) )
                {
                    if ($order->promo_code)
                    {
                        $promoData = parent::db()->fetchRow('SELECT * FROM discounts WHERE promo_code = ?', array($order->promo_code));
                        if ($promoData)
                        {
                            if ( $promoData->days )
                            {
                                $period += $promoData->days;
                            }
                            parent::db()->update('discounts', array('status' => PROMO_CODE_STATUS_SPENT), parent::db()->quoteInto('promo_code = ?', $promoData->promo_code));
                        }
                    }
                }

				$previous_order_package_id = parent::db()->fetchOne('SELECT id FROM order_packages WHERE order_id IS NOT NULL AND `status` = ? AND escort_id = ?', array(Model_Billing_Packages::STATUS_ACTIVE, $order->escort_id));
				$activation_type = Model_Billing_Packages::ACTIVATE_ASAP;
				
				if ( $previous_order_package_id ) {
					$activation_type = Model_Billing_Packages::ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES;
				} else {
					$previous_order_package_id = null;
				}

				
				$activation_date = null;
				if ( $data['activation_date'] ) {
					$previous_order_package_id = null;
					$activation_type = Model_Billing_Packages::ACTIVATE_AT_EXACT_DATE;
					$activation_date = $data['activation_date'];
				}
				
				$order_package_data = array(
					'order_id'			=> $order_id,
					'escort_id'			=> $order->escort_id,
					'package_id'		=> $order->package_id,
					'application_id'	=> $application_id,
					'base_period'		=> $package[0]->period,
					'period'			=> $period,
					'activation_type'	=> $activation_type,
					'base_price'		=> $package[0]->price,
					'price'				=> $package_price,
					'previous_order_package_id'	=> $previous_order_package_id,
					'activation_date'	=> $activation_date,
				);

				if ($data['discount']) {
					$order_package_data['discount'] = $data['discount'];
				}

                if ($application_id == APP_ED){
                    $order_package_data['country_id'] = $order->country_id;
                }
				
				parent::db()->insert('order_packages', $order_package_data);
				$order_package_id = parent::db()->lastInsertId();			
				

				// </editor-fold>
				
				// <editor-fold defaultstate="collapsed" desc="Inserting cities">
				if ( $data['premium_cities'] && count($data['premium_cities']) ) {
					foreach( $data['premium_cities'] as $prem_city ) {
						parent::db()->insert('premium_escorts', array('order_package_id' => $order_package_id, 'city_id' => $prem_city));
					}
				}

				// </editor-fold>

				if ( $data['additional_areas'] && count($data['additional_areas']) ) {
					foreach( $data['additional_areas'] as $area ) {
						parent::db()->insert('additional_areas', array('order_package_id' => $order_package_id, 'escort_id' => $order->escort_id, 'area_id' => $area));
					}
				}

				// <editor-fold defaultstate="collapsed" desc="Inserting products">
				$products = $m_package->getProducts($order->package_id, $application_id, $order->agency_id, $order->escort_gender, false, null, $escort_type);
				
				if ( count($products) > 0 )
				{
					foreach( $products as $product )
					{
						$package_product_data = array(
							'order_package_id' => $order_package_id,
							'product_id' => $product->id,
							'is_optional' => 0,
							'price' => $product->price
						);
						
						parent::db()->insert('order_package_products', $package_product_data);
					}
				}


				if ( $data['optional_products'] && count($data['optional_products']) > 0 ) {

					foreach ( $data['optional_products'] as $opt_product ) {
						$package_product_data = array(
							'order_package_id' => $order_package_id,
							'product_id' => $opt_product['id'],
							'is_optional' => 1,
							'price' => $opt_product['price']
						);
						parent::db()->insert('order_package_products', $package_product_data);
					}
				}
				// </editor-fold>
				
				// <editor-fold defaultstate="collapsed" desc="Insert row in order history">
				parent::db()->insert('online_billing_order_history', array(
					'user_id'	=> $user_id,
					'escort_id'	=> $order->escort_id,
					'agency_id'	=> $order->agency_id,
					'package_id'	=> $order->package_id,
					'order_id'		=> $order_id,
					'order_package_id'	=> $order_package_id,
					'transfer_id'		=> $transfer_id,
					'data'		=> $order->data,
					'showname'		=> $order->escort_showname,
					'package_title'	=> $package[0]->name,
					'period'		=> $period,
					'order_date'	=> new Zend_Db_Expr('NOW()'),
					'total_price'	=> $package_price
				));
				// </editor-fold>

				if ( $application_id == APP_A6){
					$enh_m = new Model_Escort_NewHistory();
					try{
						$enh_m->save(array('escort_id' => $order->escort_id, 'package_id' => $order->package_id, 'period' => $period));
					} catch(Exception $enhexp){
						@file_put_contents('../../../public/debug.log'," \n " .$enhexp->getMessage(), FILE_APPEND);
					}
				}

			}
			
			$secret_prefix = self::$secret_prefixes[$application_id];
			$site_id = self::$site_ids[$application_id];
			
			
			if ( ! $this->_check_sha1 ) {
				//Do not check sha1
			}else {
				//Comparing sha1
				$num_formated = number_format($total_price, 2, '.', '');
				$sha1_string = $secret_prefix . $num_formated . 'EUR' . 'SC-' . $user_id;
				if ( sha1($sha1_string) != $this->_callback['sha1'] ) {
                    Cubix_Email::send('vardanina@gmail.com', 'Sha1 issue', serialize(array('order_package_data' => $order_package_data,'error_message' => 'Failure. Sha1 doesn\'t match.----'. $num_formated.'----'.$this->_callback['amount'].'----','shopping_cart_data' => $this->_shopping_cart,'callback' => $this->_callback )));
                    Cubix_Email::send('mihranhayrapetyan@gmail.com', 'Sha1 issue', serialize(array('order_package_data' => $order_package_data,'error_message' => 'Failure. Sha1 doesn\'t match.----'. $num_formated.'----'.$this->_callback['amount'].'----','shopping_cart_data' => $this->_shopping_cart,'callback' => $this->_callback )));
                    Cubix_Email::send('sharps1@protonmail.com', 'Sha1 issue', serialize(array('order_package_data' => $order_package_data,'error_message' => 'Failure. Sha1 doesn\'t match.----'. $num_formated.'----'.$this->_callback['amount'].'----','shopping_cart_data' => $this->_shopping_cart,'callback' => $this->_callback )));
					parent::db()->rollBack();
					$this->updateLogData('Failure. Sha1 doesn\'t match.----'. $num_formated.'----'.$this->_callback['amount'].'----');
					return false;
				}
			}
			
			//Updating transfer amount and order price
			parent::db()->query('UPDATE transfers SET amount = ? WHERE id = ?', array($total_price, $transfer_id));
			parent::db()->query('UPDATE orders SET price = ?, price_package = ? WHERE id = ?', array($total_price, $total_price_package, $order_id));

			//Removing shopping cart NO MORE :)
			//parent::db()->query('DELETE FROM shopping_cart WHERE user_id = ?', array($user_id));
			parent::db()->update('shopping_cart', array('status' => 1), array( parent::db()->quoteInto('user_id  = ?', $user_id), parent::db()->quoteInto('hash = ?', $this->_callback['hash'])));
			if($application_id == APP_ED){
				$esc_m = new Model_Escorts();
				$esc = $esc_m->getByUserId($user_id);
				$esc_m->removeStatusBit($esc->id, $esc_m::ESCORT_STATUS_AWAITING_PAYMENT);
				$esc_m->setStatusBit($esc->id, $esc_m::ESCORT_STATUS_ACTIVE);
			}
			$this->updateLogData(array('transfer_id' => $transfer_id, 'msg' => 'Payment successfully recieved'));
			
			if (in_array($application_id, array(APP_ED, APP_EG_CO_UK))) {
				$auto_back_users = array(
					APP_ED => 84, // May
					APP_EG_CO_UK => 157 //brandy
				);
				$comment = 'Auto confirmed.';
				$model_transfers = new Model_Billing_Transfers();
				$transfer = $model_transfers->getDetails($transfer_id);
				$transfer->autoConfirm($comment, $total_price, $auto_back_users[$application_id]);
			}
			
			parent::db()->commit();
			
			
			// <editor-fold defaultstate="collapsed" desc="Saving cc info for future use">
			if ( $application_id == APP_EF ) {
				try {
					if ( $this->_callback['mmg_token'] ) {
						$gateway = 'mmgbill';
						$token = $this->_callback['mmg_token'];
					} elseif ( $this->_callback['netbanx_token'] ) {
						$gateway = 'netbanx';
						$token = $this->_callback['netbanx_token'];
					}
					if ( $token && strlen($this->_callback['card_number']) ) {
						$is_exists = parent::db()->fetchOne('
							SELECT TRUE FROM user_credit_cards WHERE user_id = ? AND number = ? AND gateway = ?
							', array($user_id, $this->_callback['card_number'], $gateway));
						
						preg_match('/master|visa|amex/', strtolower($this->_callback['card_name']), $matches);
						
						if ( ! $is_exists ) {
							parent::db()->insert('user_credit_cards', array(
								'user_id'	=> $user_id,
								'number'	=> $this->_callback['card_number'],
								'token' => $token,
								'gateway'	=> $gateway,
								'type'	=> $matches[0]
							));
						}
					}

					if ( $this->_callback['netbanx_profile_id'] && strlen($this->_callback['netbanx_profile_id']) ) {
						$is_exists = parent::db()->fetchOne('SELECT TRUE FROM netbanx_profile_ids WHERE user_id = ?', array($user_id));
						if ( ! $is_exists ) {
							parent::db()->insert('netbanx_profile_ids', array(
								'user_id'	=> $user_id,
								'profile_id'	=> $this->_callback['netbanx_profile_id']
							));
						}
					}

				} catch(Exception $ex) {
				}
			}
			// </editor-fold>

			
			
			/*Saving cc info for future use*/
			
			//Sending sms to sails
			/*$user = parent::_fetchRow('
				SELECT showname AS escort_showname, a.name AS agency_name FROM escorts e 
				LEFT JOIN agencies a ON a.id = e.agency_id
				WHERE e.user_id = ?
				GROUP BY e.user_id
			', array($user_id));*/
			
			$this->notifyThroughSms('you have new email');

			if ( Cubix_Application::getId() == APP_EF || Cubix_Application::getId() == APP_ED || Cubix_Application::getId() == APP_EG_CO_UK) {
				$user_data = parent::db()->fetchRow('SELECT username, user_type, sales_user_id FROM users WHERE id = ?', array($user_id));
				$sales_email = parent::db()->fetchRow('SELECT email, username FROM backend_users WHERE id = ?', array($user_data->sales_user_id));

				$escorts_info = '';

				foreach( $this->_shopping_cart as $escort_order ) {
					$escort_package = parent::db()->fetchRow('SELECT name, period FROM packages WHERE id = ?', array($escort_order->package_id));
					$escorts_info .= '<p>Escort: '.$escort_order->escort_showname.', '. $escort_order->id.', - '.$escort_package->name.', '.$escort_package->period.'days </p>';
				}

				$email_data = array(
					'username' => $user_data->username,
					'order_id' => $system_order_id,
					'escort_id' => $order->escort_id,
					'type' => $user_data->user_type,
					'amount' => $total_price,
					'sales' => $sales_email->username,
					'order_date'	=> date('Y-m-d H:i:s'),
					'transfer_id'	=> $transfer_id,
					'transfer_type_id' => $transfer_type_id,
					'payment_type' => $payment_type,
					'escorts' => $escorts_info,
					'package_title'	=> $package[0]->name,
					'period'		=> $package[0]->period,
					'transaction_id' => $this->_callback['transaction_id']
				);
				if (Cubix_Application::getId() == APP_EG_CO_UK && isset($banner_price)){
				    $email_data['banner'] = 'Banner price - '. $banner_price;
                }
				
				$this->sendEmailToSales($sales_email->email, $email_data);
			}

			return true;
		} catch(Exception $ex) {
			parent::db()->rollBack();
			$err_body = var_export($this->_shopping_cart, true);
			$err_body .= "\n\n";
			$err_body .= 'From application id '. $application_id;
			$err_body .= "\n\n";
			$err_body .= $ex->getMessage();
			Cubix_Email::send('badalyano@gmail.com', 'payment issue', $err_body);

			$this->updateLogData($ex->getMessage());
		}
	}
	
	public function sendEmailToSales($sales_email, $email_data)
	{
		$conf = Zend_Registry::get('api_config');
		Cubix_Api_XmlRpc_Client::setApiKey(Cubix_Application::getById(Zend_Controller_Front::getInstance()->getRequest()->application_id)->api_key);
		Cubix_Api_XmlRpc_Client::setServer($conf['server']);

		if(Cubix_Application::getId() == APP_ED || Cubix_Application::getId() == APP_EG_CO_UK){

			//$sales_email = 'mihranhayrapetyan@gmail.com';

			if( $email_data['type'] == 'escort' ){
				Cubix_Email::sendTemplate('self_checkout_escort', $sales_email, $email_data, null,null, null, false, 'New payment from escort '. $email_data['username']);
			}elseif( $email_data['type'] == 'agency' ){
				Cubix_Email::sendTemplate('self_checkout_agency', $sales_email, $email_data, null,null, null, false, 'New payment from agency '. $email_data['username']);
			}
		}else{
			Cubix_Email::sendTemplate('self_checkout', $sales_email, $email_data);
		}

	}

	public function payV3($payment_type = null, &$p_info = null)
	{
		$application_id = Cubix_Application::getId();
		$user_id = $this->_callback['reference'];
		
		if ( ! $this->_shopping_cart ) {
			$this->updateLogData('User with id ' . $user_id . ' doesn\'t have shopping card');
			return false;
		}
		
		$transfer_type_id = 3;
		
		if($payment_type == 'postfinance'){
			$transfer_type_id = 8; // Postcheck;
		}
		elseif($payment_type == '2000charge'){
			$transfer_type_id = 10; // Paysafe
		}elseif($payment_type == 'faxin'){
			$transfer_type_id = 11; 
		}
		
		//$sales_user_id = parent::db()->fetchOne('SELECT sales_user_id FROM users WHERE id = ?', array($user_id));
		$sales_user_id = $this->_backend_user_id;
		if ( $application_id == APP_A6 && $this->_callback['is_phone_billing'] ) {
			$sales_user_id = 100;//ivr-gotd
		}


		parent::db()->beginTransaction();
		try {
			$system_order_id = $this->_getNewOrderId();

			// <editor-fold defaultstate="collapsed" desc="Custom logic for gotd">

			//Deviding orders which contains only gotd, they have different logic
			//There is no need to create order etc..

				
			$orders_with_gotd_or_votd_only = array();
			
			foreach( $this->_shopping_cart as $i => $order ) {
				$data = unserialize($order->data);
				if ( ! $order->package_id && ( count($data['gotd']) > 0 || count($data['votd']) > 0 )) {
					unset($this->_shopping_cart[$i]);
					$orders_with_gotd_or_votd_only[] = $order;
				}
			}
			
			//if there is only gotd or votd orders without any package
			//add transfer, change status in gotd_orders and return

			if ( count($this->_shopping_cart) == 0){
				
				if (count($orders_with_gotd_or_votd_only) ) {
					$total_amount = 0;
					$exp_date = explode('/', $this->_callback['exp_date']);

					$transfer_array = array(
						'user_id' => $user_id,
						'transfer_type_id' => $transfer_type_id,
						'date_transfered' => new Zend_Db_Expr('NOW()'),
						'amount' => $this->_callback['amount'],
						'application_id' => $application_id,
						'status'	=> Model_Billing_Transfers::STATUS_PENDING,
						'backend_user_id'	=> $sales_user_id, //$this->_backend_user_id,
						'first_name'	=> $this->_callback['first_name'],
						'last_name'	=> $this->_callback['last_name'],
						'transaction_id' => $this->_callback['transaction_id'],
						'is_self_checkout' => 1,
						'cc_number'	=> $this->_callback['card_number'],
						'cc_address'	=> $this->_callback['address'],
						'cc_ip'			=> $this->_callback['ip_address'],
						'cc_holder'		=> $this->_callback['card_name'],
						'cc_exp_day'	=> (int)$exp_date[1],
						'cc_exp_month'	=> (int)$exp_date[0]
					);

					if (Cubix_Application::getId() == APP_A6) {
						$transfer_array['status'] = Model_Billing_Transfers::STATUS_CONFIRMED;
					}
					
					if(isset($payment_type)){
						$transfer_array['payment_system'] = $payment_type;
					}
					
					parent::db()->insert('transfers', $transfer_array);

					$transfer_id = parent::db()->lastInsertId();

					foreach($orders_with_gotd_or_votd_only as $order) {
						$data = unserialize($order->data);
						
						if($data['gotd'] && count($data['gotd']) > 0){
							$total_amount_gotd = $this->_activateGotds($data['gotd'], $user_id, $transfer_id, $order->escort_gender, $order->escort_gender);
							$this->_updateOrderPrice($transfer_id, $order->escort_id, $total_amount_gotd, 'gotd');
						}
						
						if($data['votd'] && count($data['votd']) > 0){
							$total_amount_votd = $this->_activateVotds($data['votd'], $user_id, $transfer_id, $order->escort_gender, $order->escort_gender);
							$this->_updateOrderPrice($transfer_id, $order->escort_id, $total_amount_votd, 'votd');
						}
					}

					if (Cubix_Application::getId() == APP_A6) {
						$comment = 'Auto confirmed, gotd or votd';
						$model_transfers = new Model_Billing_Transfers();
						$transfer = $model_transfers->getDetails($transfer_id);

						$transfer->autoConfirm($comment, $transfer_array['amount'], 89); //89 katie
					}
							
					//parent::db()->query('DELETE FROM shopping_cart WHERE user_id = ?', array($user_id));
					$this->updateLogData(array('transfer_id' => $transfer_id, 'msg' => 'Only GOTD or VOTD Payment successfully recieved'));
					parent::db()->update('shopping_cart', array('status' => 1), array( parent::db()->quoteInto('user_id  = ?', $user_id), parent::db()->quoteInto('hash = ?', $this->_callback['hash'])));
					if($application_id == APP_ED){
						$esc_m = new Model_Escorts();
						$esc = $esc_m->getByUserId($user_id);
						$esc_m->removeStatusBit($esc->id, $esc_m::ESCORT_STATUS_AWAITING_PAYMENT);
						$esc_m->setStatusBit($esc->id, $esc_m::ESCORT_STATUS_ACTIVE);
					}
					parent::db()->commit();
					$p_info = 1;
				}
		
				return true;
			}
			
			
			// </editor-fold>
			
			// <editor-fold defaultstate="collapsed" desc="Inserting order row">
			$order_data = array(
				'user_id'	=> $user_id,
				'backend_user_id'	=> $sales_user_id, //$this->_backend_user_id,
				'status' => Model_Billing_Orders::STATUS_PENDING,
				'activation_condition' => Model_Billing_Orders::CONDITION_AFTER_PAID,
				'order_date' => new Zend_Db_Expr('NOW()'),
				'payment_date'	=> new Zend_Db_Expr('NOW()'),
				'system_order_id' => $system_order_id,
				'price' => 1, // Temporary price. Will be updated after foreach
				'packages_count'	=> count($this->_shopping_cart),
				'application_id' => $application_id,
				'use_balance' => 0,
				'is_self_checkout' => 1
			);
			
			parent::db()->insert('orders', $order_data);
			$order_id = parent::db()->lastInsertId();

			
			Model_Activity::getInstance()->log('create_order', array('order id' => $order_id));
			// </editor-fold>

			// <editor-fold defaultstate="collapsed" desc="Creating transfer">
			$exp_date = explode('/', $this->_callback['exp_date']);

			$transfer_array = array(
				'user_id' => $user_id,
				'transfer_type_id' => $transfer_type_id,
				'date_transfered' => new Zend_Db_Expr('NOW()'),
				'amount' => 1, // Temporary price. Will be updated after foreach
				'application_id' => $application_id,
				'status'	=> Model_Billing_Transfers::STATUS_PENDING,
				'backend_user_id'	=> $sales_user_id, //$this->_backend_user_id,
				'first_name'	=> $this->_callback['first_name'],
				'last_name'	=> $this->_callback['last_name'],
				'transaction_id' => $this->_callback['transaction_id'],
				'is_self_checkout' => 1,
				'cc_number'	=> $this->_callback['card_number'],
				'cc_address'	=> $this->_callback['address'],
				'cc_ip'			=> $this->_callback['ip_address'],
				'cc_holder'		=> $this->_callback['card_name'],
				'cc_exp_day'	=> (int)$exp_date[1],
				'cc_exp_month'	=> (int)$exp_date[0]
			);

			if ( Cubix_Application::getId() == 16 && $this->_callback['is_phone_billing'] ) {
				$transfer_array['phone_billing_paid'] = 1;
				$transfer_array['is_self_checkout'] = 0;
				$transfer_array['transfer_type_id'] = 7;
			}


            if ( in_array(Cubix_Application::getId(),array(APP_EF,APP_A6)) && isset($this->_shopping_cart[0])) {
                $transfer_array['is_mobile'] = $this->_shopping_cart[0]->is_mobile;
            }

            if ($application_id== APP_EF){
                $transfer_array['client_cookie_id'] = $this->_shopping_cart[0]->client_cookie_id;
                $transfer_array['ip'] = $this->_shopping_cart[0]->ip;
            }

			if(isset($payment_type)){
				$transfer_array['payment_system'] = $payment_type;
			}
			
			parent::db()->insert('transfers', $transfer_array);

			$transfer_id = parent::db()->lastInsertId();

			parent::db()->insert('transfer_orders', array(
				'transfer_id'	=> $transfer_id,
				'order_id'		=> $order_id
			));
			Model_Activity::getInstance()->log('add_transfer', array('transfer id' => $transfer_id));
			// </editor-fold>
			
			$m_package = new Model_Packages();
			$m_products = new Model_Products();

			$total_price = 0;
			
			$is_agency = false;
			$has_daypass = false;

			foreach( $this->_shopping_cart as $order ) {
				$data = unserialize($order->data);
				
				if ( $order->user_type == 'agency' ) {
					$is_agency = true;
					$user_type = 2;
				} else {
					$user_type = null;
				}
				// edir-2303
				$escort_type = null;
				if ($application_id == APP_ED){
				    $escort_type = $order->escort_type;
                }
				$package = $m_package->getWithPrice($application_id, $user_type, $order->escort_gender, $order->package_id, false, null, null, $escort_type);
				
				if ( $application_id == APP_A6 ) {
					if (!$has_daypass)
					{
						if (in_array($order->package_id, array(19, 20, 23)))
							$has_daypass = true;
					}

					switch($order->package_id) {
						case 24:
							$package[0]->price = 350;
							break;
						case 25:
							$package[0]->price = 430;
							break;
						default:
							break;
					}
				}

				if ( $data['optional_products'] && count($data['optional_products']) ) {
					foreach($data['optional_products'] as $k => $v) {
						$opt_product = $m_products->getWithPrice($application_id, $user_type, $order->escort_gender, $v, false, $escort_type);
						if ( $opt_product ) {
							$data['optional_products'][] = array('id' => $opt_product->id, 'price' => $opt_product->price);
						}
						unset($data['optional_products'][$k]);
					}
				}
				// <editor-fold defaultstate="collapsed" desc="Calculating package price">
				$price_data = array(
					'package_price' => $package[0]->price,
					'default_period' => $package[0]->period,
					'period' => $package[0]->period,
					'opt_product_prices' => array(),
					'add_areas'	=> $data['additional_areas'],
					'gotds_count'	=> 0
				);
				if ( $application_id == APP_A6 ) {
					$price_data['votds_count'] = 0;
				}
				if ( count($data['optional_products']) ) {
					$price_data['opt_product_prices'] = $data['optional_products'];
				}
				
				
				if ( $data['gotd'] && count($data['gotd']) > 0 ) {
					$gotd_prod_id = 15;
					if ( $application_id == APP_BL ) {
						$gotd_prod_id = 16;
					}
					$gotd_prod = $m_products->getWithPrice($application_id, $user_type, $order->escort_gender, $gotd_prod_id);
					$price_data['gotd_price'] = $gotd_prod->price;
					foreach($data['gotd'] as $ref) {
						$count = count(explode(':', $ref));
						$price_data['gotds_count'] += $count;
						
					}
				}

				if ( $data['votd'] && count($data['votd']) > 0 ) {
					$votd_prod_id = 22;
					if ( $application_id == APP_BL ) {
						$votd_prod_id = 16;
					}
					$votd_prod = $m_products->getWithPrice($application_id, $user_type, $order->escort_gender, $votd_prod_id);
					$price_data['votd_price'] = $votd_prod->price;
					foreach($data['votd'] as $ref) {
						$count = count(explode(':', $ref));
						$price_data['votds_count'] += $count;
						
					}
				}

				$package_price = $this->_calculatePrice($price_data);
				$total_price += $package_price;
				
				// </editor-fold>
				
				// <editor-fold defaultstate="collapsed" desc="Inserting order packages row">
				
				//Checking if has active package. IF has set activation type to after current pacakge expires
				$previous_order_package_id = parent::db()->fetchOne('SELECT id FROM order_packages WHERE order_id IS NOT NULL AND `status` = ? AND escort_id = ?', array(Model_Billing_Packages::STATUS_ACTIVE, $order->escort_id));
				$activation_type = Model_Billing_Packages::ACTIVATE_ASAP;
				
				if ( $previous_order_package_id ) {
					$activation_type = Model_Billing_Packages::ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES;
				} else {
					$previous_order_package_id = null;
				}
				
				$order_package_data = array(
					'order_id'			=> $order_id,
					'escort_id'			=> $order->escort_id,
					'package_id'		=> $order->package_id,
					'application_id'	=> $application_id,
					'base_period'		=> $package[0]->period,
					'period'			=> $package[0]->period,
					'activation_type'	=> $activation_type,
					'base_price'		=> $package[0]->price,
					'price'				=> $package_price,
					'previous_order_package_id'	=> $previous_order_package_id,
				);
				
				if ( in_array($application_id, array(APP_A6)) && $data['additional_areas'] ) {
					$order_package_data['add_areas_count'] = count($data['additional_areas']);
				}

				if ( $data['activation_date'] ) {
					$order_package_data['activation_type'] = Model_Billing_Packages::ACTIVATE_AT_EXACT_DATE;
					$order_package_data['activation_date'] = date('Y-m-d H:i:00', $data['activation_date']);
				}
				
				parent::db()->insert('order_packages', $order_package_data);
				$order_package_id = parent::db()->lastInsertId();

				// </editor-fold>
				
				// <editor-fold defaultstate="collapsed" desc="Inserting cities">
				if ( $data['premium_cities'] && count($data['premium_cities']) ) {
					foreach( $data['premium_cities'] as $prem_city ) {
						parent::db()->insert('premium_escorts', array('order_package_id' => $order_package_id, 'city_id' => $prem_city));
					}
				}

				// </editor-fold>

				if ( $data['additional_areas'] && count($data['additional_areas']) ) {
					foreach( $data['additional_areas'] as $area ) {
						parent::db()->insert('additional_areas', array('order_package_id' => $order_package_id, 'escort_id' => $order->escort_id, 'area_id' => $area));
					}
				}

				// <editor-fold defaultstate="collapsed" desc="Inserting products">
				$products = $m_package->getProducts($order->package_id, $application_id, $order->agency_id, $order->escort_gender, false, null, $escort_type);

				if ( count($products) > 0 )
				{
					foreach( $products as $product )
					{
						$package_product_data = array(
							'order_package_id' => $order_package_id,
							'product_id' => $product->id,
							'is_optional' => 0,
							'price' => $product->price
						);
						parent::db()->insert('order_package_products', $package_product_data);
					}
				}

				if ( $data['optional_products'] && count($data['optional_products']) > 0 ) {

					foreach ( $data['optional_products'] as $opt_product ) {
						$package_product_data = array(
							'order_package_id' => $order_package_id,
							'product_id' => $opt_product['id'],
							'is_optional' => 1,
							'price' => $opt_product['price']
						);
						parent::db()->insert('order_package_products', $package_product_data);
					}
				}
				
				if ( $data['gotd'] && count($data['gotd']) > 0 ) {
					$this->_activateGotds($data['gotd'], $user_id, $transfer_id, $order->escort_gender, $order->escort_gender, $order_package_id);
					
				}

				if ( $data['votd'] && count($data['votd']) > 0 ) {
					$this->_activateVotds($data['votd'], $user_id, $transfer_id, $order->escort_gender, $order->escort_gender, $order_package_id);
					
				}
				
				// </editor-fold>
				
				// <editor-fold defaultstate="collapsed" desc="Insert row in order history">
				parent::db()->insert('online_billing_order_history', array(
					'user_id'	=> $user_id,
					'escort_id'	=> $order->escort_id,
					'agency_id'	=> $order->agency_id,
					'package_id'	=> $order->package_id,
					'order_id'		=> $order_id,
					'order_package_id'	=> $order_package_id,
					'transfer_id'		=> $transfer_id,
					'data'		=> $order->data,
					'showname'		=> $order->escort_showname,
					'package_title'	=> $package[0]->name,
					'period'		=> $package[0]->period,
					'order_date'	=> new Zend_Db_Expr('NOW()'),
					'transfer_id'	=> $transfer_id,
					'total_price'	=> $package_price
				));

				if ( $application_id == APP_A6){
					$enh_m = new Model_Escort_NewHistory();
					try{
						$enh_m->save(array('escort_id' => $order->escort_id, 'package_id' => $order->package_id, 'period' => $package[0]->period));
					} catch(Exception $enhexp){
						@file_put_contents('../../../public/debug.log'," \n " .$enhexp->getMessage(), FILE_APPEND);
					}
				}
				// </editor-fold>
			}

			if ($has_daypass)
				$p_info = 2;
			
			//Remember rows with only gotd or votd
			//after finishing all stuff with package we updating gotd_orders
			//Transfer is already created for package, just increase total_price
			$total_gotd_votd_left = 0;
			
			foreach($orders_with_gotd_or_votd_only as $order) {
				$data = unserialize($order->data);
				
				if($data['gotd'] && count($data['gotd']) > 0){
					
					$total_amount_gotd = $this->_activateGotds($data['gotd'], $user_id, $transfer_id, $order->escort_gender, $order->escort_gender);
					$total_gotd_votd_left += $total_amount_gotd;
					$this->_updateOrderPrice($transfer_id, $order->escort_id, $total_amount_gotd, 'gotd');
				}

				if($data['votd'] && count($data['votd']) > 0){
					
					$total_amount_votd += $this->_activateVotds($data['votd'], $user_id, $transfer_id, $order->escort_gender, $order->escort_gender);
					$total_gotd_votd_left += $total_amount_votd;
					$this->_updateOrderPrice($transfer_id, $order->escort_id, $total_amount_votd, 'votd');
				}
			}
			
			$secret_prefix = self::$secret_prefixes[$application_id];
			$site_id = self::$site_ids[$application_id];
			
			
			if ( ! $this->_check_sha1 ) {
				//Do not check sha1
			} elseif ( $this->_is_from_cardgateplus ) {
				//Comparing hash(mnd5)
				if ( $this->_callback['is_test'] == 1 ) {
					$hash = md5("TEST" . $this->_callback['transaction_id'] . 'EUR' . ($total_price * 100) . $this->_callback['original_reference'] . $this->_callback['status_id'] . $secret_prefix);
				} else {
					$hash = md5($this->_callback['transaction_id'] . 'EUR' . ($total_price * 100) . $this->_callback['original_reference'] . $this->_callback['status_id'] . $secret_prefix);
				}
				
				if ( $hash != $this->_callback['sha1'] ) {
					parent::db()->rollBack();
					$this->updateLogData('Failure. Hash doesn\'t match.');
					return false;
				}
				
			} else {
				//Comparing sha1
				if ( sha1($secret_prefix . number_format($total_price, 2, '.', '') . 'EUR' . 'SC3-' . $user_id) != $this->_callback['sha1'] ) {
					parent::db()->rollBack();
					$this->updateLogData('Failure. Sha1 doesn\'t match.');
					return false;
				}
			}
			
			//Updating transfer amount and order price
			parent::db()->query('UPDATE transfers SET amount = ? WHERE id = ?', array(( $total_price + $total_gotd_votd_left), $transfer_id));
			parent::db()->query('UPDATE orders SET price = ? WHERE id = ?', array($total_price, $order_id));

			// https://sceonteam.atlassian.net/browse/A6-64 covid-19
			
			if (Cubix_Application::getId() == APP_A6) {
				$comment = 'Auto confirmed.';
				$model_transfers = new Model_Billing_Transfers();
				$transfer = $model_transfers->getDetails($transfer_id);

				$transfer->autoConfirm($comment, $total_price, 89); //89 katie
			}
	
			//Removing shopping cart NO MORE :)
			//parent::db()->query('DELETE FROM shopping_cart WHERE user_id = ?', array($user_id));
			parent::db()->update('shopping_cart', array('status' => 1), array( parent::db()->quoteInto('user_id  = ?', $user_id), parent::db()->quoteInto('hash = ?', $this->_callback['hash'])));
			if($application_id == APP_ED){
				$esc_m = new Model_Escorts();
				$esc = $esc_m->getByUserId($user_id);
				$esc_m->removeStatusBit($esc->id, $esc_m::ESCORT_STATUS_AWAITING_PAYMENT);
				$esc_m->setStatusBit($esc->id, $esc_m::ESCORT_STATUS_ACTIVE);
			}
			
			$this->updateLogData(array('transfer_id' => $transfer_id, 'msg' => 'Payment successfully recieved'));
			parent::db()->commit();

			if(Cubix_Application::getId() == APP_A6){
                //Send Mail after order AND-461
                try{
                    $user = parent::db()->fetchRow('SELECT u.username, u.email,op.escort_id,op.package_id,p.name as package_name  FROM orders o 
                                INNER JOIN order_packages op ON op.order_id = o.id
                                INNER JOIN users u ON  u.id = o.user_id 
                                INNER JOIN  packages p ON p.id = op.package_id 
                                WHERE o.id = ?', array($order_id));

                    if($user){
                      if($user->email){
                        Cubix_Email::sendTemplate('sc_order_template', $user->email, array(
							'user_id' => $user_id,
                            'username' => $user->username,
                            'type'=>($this->_callback['is_phone_billing']) ? 'Phone' : 'Card',
                            'date' => date('Y-m-d'),
                            'amount' => ($total_price) ? $total_price . ' CHF':'none',
                            'transaction_id' => ($this->_callback['transaction_id']) ? $this->_callback['transaction_id'] : 'none',
                            'order_id' => $order_id,
                            'profile_id' => $user->escort_id,
                            'product_id' => $user->package_name
                        ));
                       }
				    }
					else{
						Cubix_Email::send('badalyano@gmail.com', 'AND6 PAYMENT EMAIL-'.date('Y-m-d', time()), 'user query is empty tr_id-'.$transfer_id , false, null);
					}
                }catch (Exception $e){
					Cubix_Email::send('badalyano@gmail.com', 'AND6 PAYMENT EMAIL-'.date('Y-m-d', time()), $e->getMessage() , false, null);
                    @file_put_contents('../../../public/debug.log'," \n " .$e->getMessage(), FILE_APPEND);
                }
            }
			
//			//Sending sms to sails
//			$user = parent::_fetchRow('
//				SELECT showname AS escort_showname, a.name AS agency_name FROM escorts e 
//				LEFT JOIN agencies a ON a.id = e.agency_id
//				WHERE e.user_id = ?
//				GROUP BY e.user_id
//			', array($user_id));
//			
//			$this->notifyThroughSms('you have new email');
//			
//			if ( Cubix_Application::getId() == APP_EF ) {
//				$user_data = parent::db()->fetchRow('SELECT username, user_type, sales_user_id FROM users WHERE id = ?', array($user_id));
//				$sales_email = parent::db()->fetchRow('SELECT email FROM backend_users WHERE id = ?', array($user_data->sales_user_id));
//				$email_data = array(
//					'username' => $user_data->username,
//					'order_id' => $system_order_id,
//					'escort_id' => $order->escort_id,
//					'type' => $user_data->user_type,
//					'amount' => number_format($package_price, 2)
//				);
//				$this->sendEmailToSales($sales_email->email, $email_data);
//			}
			
			return true;
		} catch(Exception $ex) {
			parent::db()->rollBack();
			$err_body = var_export($this->_shopping_cart, true);
			$err_body .= "\n\n";
			$err_body .= 'From application id '. $application_id;
			$err_body .= "\n\n";
			$err_body .= $ex->getMessage();
			Cubix_Email::send('badalyano@gmail.com', 'payment issue', $err_body);
			$this->updateLogData($ex->getMessage());
		}
	}
	
	
	
	
	private function _calculatePrice($price_data, $without_gotd = false)
	{
		$price = $price_data['package_price'];
		$default_period = $price_data['default_period'];
		$period = $price_data['period'];
		$discount = $price_data['discount'];
		$fix_discount = $price_data['fix_discount'];
		$surcharge = $price_data['surcharge'];
		$opt_products_price = 0;
		$gotds_count = $price_data['gotds_count'];
		$gotd_price = $price_data['gotd_price'];
		if(Cubix_Application::getId() == APP_A6){
			$votds_count = $price_data['votds_count'];
			$votd_price = $price_data['votd_price'];
		}
		$de_days = $price_data['de_days'];
		$add_areas_count = count($price_data['add_areas']);
		
		$add_area_price = 0;
		$dex_price = 0;
		if ( count($price_data['opt_product_prices']) > 0 ) {
			foreach ($price_data['opt_product_prices'] as $opt_prod_price) {
				
				if ( $opt_prod_price['id'] == Model_Products::ADDITIONAL_AREA ) {
					$add_area_price = $opt_prod_price['price'];
				}
				
				if ( $opt_prod_price['id'] == Model_Products::DAY_EXTEND ) {
					$dex_price = $opt_prod_price['price'] * $de_days;
				}
				else {
					if ( $default_period == $period ) {
						$opt_products_price += $opt_prod_price['price'];
						
					} else {
						$opt_products_price += (float) $opt_prod_price['price'] / $default_period;
					}
				}
				
			}
			if ( $default_period != $period ) {
				$opt_products_price = ceil((float) $opt_products_price * $period);
			}
		}
		$opt_products_price = $opt_products_price + $dex_price;
		
		$total_price = 0;

		if ( $default_period > 0 ) {
			$day_price = ceil($price / $default_period);
		}

		//$parts = $period / $default_period;
		$total_price = ceil(($price * ($period / $default_period) + $opt_products_price));

		if ( $period == $default_period ) {
			$total_price = $price + $opt_products_price;
		}

		// discount
		if ( $discount > 0 ) {
			$total_price -= $total_price * ($discount / 100);
		}

		//fixed_discount
		if ( $fix_discount > 0 ) {
			$total_price -= $fix_discount;
		}
		//surcharge
		if ( $surcharge > 0 ) {
			$total_price += $surcharge;
		}
		
		if ( $add_areas_count > 0 ) {
			$add_areas_count--;
			$total_price = $total_price + ($add_area_price * $add_areas_count);
		}

		if ( !$without_gotd && $gotds_count > 0 ) {
			$total_price = $total_price + ($gotds_count * $gotd_price);
		}

		if ( Cubix_Application::getId() == APP_A6 && $votds_count > 0 ) {
			$total_price = $total_price + ($votds_count * $votd_price);
		}

		return round($total_price, 2);
	}
	
	private function _generateOrderId()
	{
		$order_id = md5(microtime() * microtime() + microtime());
		$order_id = substr(strtoupper($order_id), 0, 8);

		return $order_id;
	}

	private function _getNewOrderId()
	{
		$m_orders = new Model_Billing_Orders();
		
		$order_id = $this->_generateOrderId();
		while( $m_orders->isOrderIdExist($order_id) ) {
			$order_id = $this->_generateOrderId();
		}

		return $order_id;
	}
	
	private function _activateGotds($gotds, $user_id, $transfer_id, $user_type, $escort_gender, $order_package_id = null)
	{
		$application_id = Cubix_Application::getId();
		$total_price = 0;
		$gotd_prod_id = 15;
		$model_products = new Model_Products();
				
		if ( $user_type == 'agency' ) {
			$product_user_type = 2;
		} else {
			$product_user_type = null;
		}
		
		$gotd_prod = $model_products->getWithPrice($application_id, $product_user_type, $escort_gender, $gotd_prod_id);
		$amount = $gotd_prod->price;
		
		foreach($gotds as $ref) {
			$ids = explode(':', $ref);
			$count = count($ids);
			$total_price += $count * $gotd_prod->price;
			
			$gotd_data = array(
				'status' => 2,
				'transfer_id' => $transfer_id
			);
			
			if($order_package_id){
				$gotd_data['order_package_id'] = $order_package_id;
			}
			
			parent::db()->update('gotd_orders', $gotd_data, array( 'id IN  ( ? )' => $ids) );
			
			if($application_id == APP_A6) {
				try{

					$gotd_orders = parent::_fetchAll('
						SELECT go.activation_date AS date, c.title_en AS city, u.email AS email, u.username, go.id,
								go.escort_id
						FROM gotd_orders AS go
						INNER JOIN cities c ON c.id = go.city_id
						INNER JOIN users u ON u.id = ?
						WHERE go.id IN (' . implode(',', $ids) . ') ', array($user_id));

					foreach ($gotd_orders as $i => $gotd_order) {
						if($gotd_order->email) {
							Cubix_Email::sendTemplate('gotd_purchase_confirm', $gotd_order->email, array(
								'user_id' => $user_id,
								'type'=>($this->_callback['is_phone_billing']) ? 'Phone' : 'Card',
								'date' => date('Y-m-d'),
								'transaction_id' => ($this->_callback['transaction_id']) ? $this->_callback['transaction_id'] : 'none',
								'username' => $gotd_order->username,
								'order_id' => $gotd_order->id,    
								'profile_id' => $gotd_order->escort_id, 
								'city' => $gotd_order->city,
								'activation_date' => $gotd_order->date,
								'amount' => ($amount) ? $amount . ' CHF':'none'
							), null, null, 'de');
						}
					}
				}catch (Exception $e){
					@file_put_contents('../../../public/debug.log'," \n " .$e->getMessage(), FILE_APPEND);
				}
			}
		}
		
		return $total_price;
	}
	
	private function _activateVotds($votds, $user_id, $transfer_id, $user_type, $escort_gender, $order_package_id = null)
	{
		$application_id = Cubix_Application::getId();
		$total_price = 0;
		$votd_prod_id = 22;
		$model_products = new Model_Products();
		
		if ( $user_type == 'agency' ) {
			$product_user_type = 2;
		} else {
			$product_user_type = null;
		}
		
		$votd_prod = $model_products->getWithPrice($application_id, $product_user_type, $escort_gender, $votd_prod_id);
		
		foreach($votds as $ref) {
			$ids = explode(':', $ref);
			$count = count($ids);
			$total_price += $count * $votd_prod->price;
			
			foreach($ids as $key =>  $id){
				$ids[$key] = ltrim ($id, 'VD_');
			}
			
			$votd_data = array(
				'status' => 2,
				'transfer_id' => $transfer_id
			);
			
			if($order_package_id){
				$votd_data['order_package_id'] = $order_package_id;
			}
			
			parent::db()->update('votd_orders', $votd_data, array( 'id IN  ( ? )' => $ids) );
			
			if($application_id == APP_A6) {
				try{
					$amount = $votd_prod->price;

					$votd_orders = parent::_fetchAll('
						SELECT vo.activation_date AS date, c.title_en AS city, u.email AS email, u.username, vo.id,
								vo.escort_id
						FROM votd_orders AS vo
						INNER JOIN cities c ON c.id = vo.city_id
						INNER JOIN users u ON u.id = ?
						WHERE vo.id IN (' . implode(',', $ids) . ') ', array($user_id));

					foreach ($votd_orders as $i => $votd_order) {
						if($votd_order->email) {
							Cubix_Email::sendTemplate('votd_purchase_confirm', $votd_order->email, array(
								'user_id' => $user_id,
								'type'=>($this->_callback['is_phone_billing']) ? 'Phone' : 'Card',
								'date' => date('Y-m-d'),
								'transaction_id' => ($this->_callback['transaction_id']) ? $this->_callback['transaction_id'] : 'none',
								'username' => $votd_order->username,
								'order_id' => $votd_order->id,    
								'profile_id' => $votd_order->escort_id, 
								'city' => $votd_order->city,
								'activation_date' => $votd_order->date,
								'amount' => ($amount) ? $amount . ' CHF':'none'
							), null, null, 'de');
						}
					}
				}catch (Exception $e){
					@file_put_contents('../../../public/debug.log'," \n " .$e->getMessage(), FILE_APPEND);
				}
			}
		}
		
		return $total_price;
	}
	
	private function _updateOrderPrice($transfer_id, $escort_id, $total_amount, $type)
	{
		if($type == 'gotd'){
			$order_package_id = self::db()->fetchOne('SELECT order_package_id FROM gotd_orders WHERE transfer_id = ? AND escort_id = ? ', array($transfer_id, $escort_id));
		}
		else if($type == 'votd'){
			$order_package_id = self::db()->fetchOne('SELECT order_package_id FROM votd_orders WHERE transfer_id = ? AND escort_id = ? ', array($transfer_id, $escort_id));
		}
		else{
			die('something went wrong');
		}
		
		$order_package = self::db()->fetchRow('
			SELECT id, order_id
			FROM order_packages 
			WHERE id = ?
		', array($order_package_id));
					
		//-->Increasing order price by price of GOTD OR VOTD product
		self::db()->query('
			UPDATE orders
			SET price = price + ?
			WHERE id = ?
		', array($total_amount, $order_package->order_id));

		self::db()->query('
			UPDATE order_packages
			SET price = price + ?
			WHERE id = ?
		', array($total_amount, $order_package->id));
		
		
		// very rare case for only agency accounts when multiple order_packages were bought with one order  
		$order_transfer_exists = self::db()->fetchOne('
			SELECT TRUE FROM transfer_orders
			WHERE order_id = ? AND transfer_id = ?
		', array( $order_package->order_id, $transfer_id) );
		
		if(!$order_transfer_exists){
			self::db()->insert('transfer_orders', array(
				'order_id'	=> $order_package->order_id,
				'transfer_id'	=> $transfer_id
			));
		}
	}		
	
	public function fixShit()
	{
		$bk_order = 'order_backup';
		$count = parent::db()->fetchOne('SELECT count(id) FROM orders_old');
		$chank_count = 1000;
		
		$chanks = ceil($count / $chank_count);
		
		for($i = 1; $i <= $chanks; $i++)
		{
			$rows = parent::db()->fetchAll('SELECT id, price FROM orders LIMIT ?, ?', array(($chank - 1) * $chank_count, $chank_count) );
			foreach($rows as $row) {
				parent::db()->update('order', array('price' => $row->price), parent::db()->quoteInto('id = ?', $row['id']));
			}
		}
	}
	
	public static function getIvrStoredCallbacks()
	{
		$tokens = parent::db()->fetchAll('SELECT * FROM ivr_callbacks WHERE status = 0');

		return $tokens;
	}
	
	public static function setIvrCallbackStatus($id, $status)
	{
		parent::db()->update('ivr_callbacks', array('status' => $status), parent::db()->quoteInto('id = ?', $id));

		return true;
	}
	
	public static function getPaymentGatewayTokens($type = 'epg')
	{
		$tokens = parent::db()->fetchAll('SELECT id, user_id, token, attempt FROM payment_gateway_tokens WHERE attempt < 30 AND type = ? ORDER BY id DESC', array($type));

		return $tokens;
	}

	public static function getPaymentFaxin($paymentid)
	{
		return parent::db()->fetchRow('SELECT * FROM payment_gateway_faxin WHERE paymentid = ?', array($paymentid));
	}
	
	public static function getCreditOrders()
	{
		$orders = parent::db()->fetchAll('SELECT * FROM credit_orders WHERE status = 0 ');

		return $orders;
	}
	
	public static function removePaymentGatewayToken($id)
	{
		parent::db()->delete('payment_gateway_tokens', parent::db()->quoteInto('id = ?', $id));
	}
	
	public static function updatePaymentGatewayTokenAttempt($id)
	{
		parent::db()->query('UPDATE payment_gateway_tokens SET attempt = attempt + 1 WHERE id = ?', array($id));
	}

	public static function updatePaymentGatewayFaxin($paymentid)
	{
		parent::db()->query('UPDATE payment_gateway_faxin SET status = 1, updated = now() WHERE paymentid = ?', array($paymentid));
	}
	
	public function confirmGotd()
	{
		
	}
	
	public static function getTransfer($transaction_id)
	{
		return parent::db()->fetchRow('SELECT * FROM transfers WHERE transaction_id = ?', array($transaction_id));
	}
	
	public static function insertCallbackLog($callback)
	{
		parent::db()->insert('online_billing_log', array_merge($callback, array('date' => date('Y-m-d H:i:s'))));
		
	}
	
	public static function checkTransactionIdExistsence($transaction_id, $payment_system = null)
	{
		if($payment_system){
			return parent::db()->fetchOne('SELECT TRUE FROM transfers WHERE transaction_id = ? AND payment_system = ?', array($transaction_id, $payment_system ));
		}
		else{
			return parent::db()->fetchOne('SELECT TRUE FROM transfers WHERE transaction_id = ?', array($transaction_id));
		}
	}
	
}

