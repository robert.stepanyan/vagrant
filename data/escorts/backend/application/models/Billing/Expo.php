<?php

class Model_Billing_Expo extends Cubix_Model
{
	protected $_table = 'expo_orders';
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				eo.id, UNIX_TIMESTAMP(eo.creation_date) AS creation_date,
				UNIX_TIMESTAMP(eo.start_date) AS start_date, 
				UNIX_TIMESTAMP(eo.end_date) AS end_date, 
				e.id AS escort_id, e.agency_id, e.status, e.showname, a.name AS agency_name,
				op.id AS order_package_id,
				op.application_id, t.transaction_id, o.system_order_id, o.id AS order_id
			FROM expo_orders eo
			INNER JOIN escorts e ON e.id = eo.escort_id
			INNER JOIN order_packages op ON op.id = eo.order_package_id
			INNER JOIN orders o ON o.id = op.order_id
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN backend_users bu ON bu.id = u.sales_user_id
			LEFT JOIN agencies a ON a.id = e.agency_id
			LEFT JOIN transfers t ON t.id = eo.transfer_id
			WHERE eo.status <> 1
		';
		
		$countSql = "SELECT FOUND_ROWS()";

		$where = '';
		
		if ( strlen($filter['e.showname']) ) {
			$where .= self::quote('AND e.showname LIKE ?', $filter['e.showname'] . '%');
		}

		if ( strlen($filter['e.id']) ) {
			$where .= self::quote('AND e.id = ?', $filter['e.id']);
		}
		
		if ( strlen($filter['sales_user_id']) ) {
			$where .= self::quote('AND bu.id = ?', $filter['sales_user_id']);
		}
		
		if ( strlen($filter['order_id']) ) {
			$where .= self::quote('AND o.system_order_id = ?', $filter['order_id']);
		}
		
		if ( strlen($filter['transaction_id']) ) {
			$where .= self::quote('AND t.transaction_id = ?', $filter['transaction_id']);
		}
		
		if ( $filter['date_from'] ) {
			$where .= self::quote('AND DATE(eo.start_date) >= DATE(FROM_UNIXTIME(?))', $filter['date_from']);
		}
		
		if ( $filter['date_to'] ) {
			$where .= self::quote('AND DATE(eo.end_date) <= DATE(FROM_UNIXTIME(?))', $filter['date_to']);
		}
		
		$sql .= $where;

		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$result = parent::_fetchAll($sql);
		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return $result;
	}
	
	public function get($id)
	{
		return parent::_fetchRow('
			SELECT eo.*, op.activation_type, op.date_activated, op.expiration_date, op.activation_date, op.period
			FROM expo_orders eo
			INNER JOIN order_packages op ON op.id = eo.order_package_id
			WHERE eo.id = ?
		', array($id));
	}
	
	public function add($data, $escort_id, $additional_data)
	{
		try {
			parent::db()->beginTransaction();
			parent::db()->query('UPDATE orders set price = price + ? WHERE id = ?', array($additional_data['price'], $additional_data['order_id']));
			parent::db()->query('UPDATE order_packages set price = price + ? WHERE id = ?', array($additional_data['price'], $additional_data['order_package_id']));
			
			parent::db()->insert('expo_orders', array(
				'creation_date'		=> date('Y-m-d H:i:s', time()),
				'start_date'	=> $data['start_date'],
				'end_date'	=> $data['end_date'],
				'escort_id'			=> $escort_id,
				'status'			=> 2,
				'order_package_id'	=> $additional_data['order_package_id'],
				'transfer_id'	=> $additional_data['transfer_id']
			));
			
			parent::db()->commit();
		} catch(Exception $ex) {
			parent::db()->rollBack();
			throw $ex;
		}
		
		return;
	}
	
	public function remove($id)
	{
		parent::db()->query('DELETE FROM expo_orders WHERE id = ?', array($id));
	}
	
	public function update($data)
	{
		parent::db()->update('expo_orders', $data, parent::quote('id = ?', $data['id']));
	}
}
