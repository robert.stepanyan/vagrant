<?php
class Model_Billing_PullPersons extends Cubix_Model
{
	protected $_table = 'moneygram_pull_persons';
	protected $_itemClass = 'Model_Billing_PullPersonItem';

	const PULL_PERSON_TYPE_MONEY_GRAM    = 1;
	const PULL_PERSON_TYPE_WESTERN_UNION = 2;
	
	public static $PULL_PERSON_TYPES = array(
		self::PULL_PERSON_TYPE_MONEY_GRAM => 'Moneygram',
		self::PULL_PERSON_TYPE_WESTERN_UNION => 'Western Union',
	);
	
	public function get($id)
	{
		$sql = '
			SELECT
				mpp.id, mpp.type_id, mpp.first_name, mpp.last_name
			FROM moneygram_pull_persons mpp
			LEFT JOIN applications ap ON ap.id = mpp.application_id
			WHERE 1 AND mpp.id = ?
		';
		
		return parent::_fetchRow($sql, array($id));
	}
	
	public function getByType($type_id, $is_active = false)
	{
		$active = $is_active ? "AND mpp.status = 1" : "";
		$sql = '
			SELECT
				mpp.id, mpp.type_id, CONCAT(mpp.first_name, " " , mpp.last_name) AS name
			FROM moneygram_pull_persons mpp
			LEFT JOIN applications ap ON ap.id = mpp.application_id
			WHERE 1 AND mpp.type_id = ? '.$active
		;
		
		return parent::_fetchAll($sql, array($type_id));
	}

	public function insert($data)
	{
		parent::getAdapter()->insert($this->_table, $data);

		return parent::getAdapter()->lastInsertId();
	}

	public function update($be_user_id, $data)
	{
		parent::getAdapter()->update($this->_table, $data, parent::getAdapter()->quoteInto('id = ?', $be_user_id));
	}

	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT
				mpp.id, mpp.type_id, mpp.first_name, mpp.last_name,
				mpp.status AS is_disabled
			FROM moneygram_pull_persons mpp
			LEFT JOIN applications ap ON ap.id = mpp.application_id
			WHERE 1
		';
		
		$countSql = '
			SELECT COUNT(mpp.id)
			FROM moneygram_pull_persons mpp
			LEFT JOIN applications ap ON ap.id = mpp.application_id
			WHERE 1
		';
		
		if ( strlen($filter['mpp.application_id']) ) {
			$where .= self::quote('AND mpp.application_id = ?', $filter['mpp.application_id']);
		}
		
		if ( strlen($filter['mpp.type_id']) ) {
			$where .= self::quote('AND mpp.type_id = ?', $filter['mpp.type_id']);
		}
		
		if ( strlen($filter['mpp.first_name']) ) {
			$where .= self::quote('AND mpp.first_name LIKE ?', $filter['mpp.first_name'] . '%');
		}
		
		if ( strlen($filter['mpp.last_name']) ) {
			$where .= self::quote('AND mpp.last_name LIKE ?', $filter['mpp.last_name'] . '%');
		}
		
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			GROUP BY mpp.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return parent::_fetchAll($sql);
	}



	/*public function remove($id)
	{
		parent::remove(self::quote('id = ?', $id));
		parent::getAdapter()->delete('ticket_comments', parent::quote('ticket_id = ?', $id));
	}*/

	public function toggle($id)
	{
		parent::getAdapter()->update($this->_table, array(
			'status' => new Zend_Db_Expr('NOT status')
		), parent::quote('id = ?', $id));
	}
}
