<?php

class Model_Billing_Hook extends Cubix_Hooker_Hook
{
	public function onEscortActivated($escort_id)
	{
		$model = new Model_Escorts();
		$model->get($escort_id)->processDefaultPackages();
	}

	public function onEscortRevisionApplied($escort_id)
	{
		$model = new Model_EscortsV2();
		$mOrders = new Model_Billing_Orders();
		$user_id = $model->getUserId($escort_id);
		$mOrders->processUserOrders($user_id);
	}

	public function onTransferCreated($transfer_id)
	{
		$orders = $this->_db->fetchAll('
			SELECT order_id AS id FROM transfer_orders WHERE transfer_id = ?
		', array($transfer_id));

		foreach ( $orders as $order ) {
			$this->_hooker->notify('order_payment_details_received', array($order->id, $transfer_id));
		}
		
		$this->_startCron('transfer', $transfer_id);
	}

	public function onTransferConfirmed($transfer_id)
	{
		$transfer = $this->_db->fetchRow('
			SELECT t.user_id, t.amount, tc.amount AS paid_amount
			FROM transfers t
			INNER JOIN transfer_confirmations tc ON tc.transfer_id = t.id
			WHERE t.status = ? AND t.id = ?
		', array(Model_Billing_Transfers::STATUS_CONFIRMED, $transfer_id));

		if ( ! $transfer ) return;
		
		$orders = $this->_db->fetchAll('
			SELECT order_id AS id FROM transfer_orders WHERE transfer_id = ?
		', array($transfer_id));

		$model = new Model_Billing_Orders();
		foreach ( $orders as $order ) {
			if ( true === $model->pay($order->id, $transfer->paid_amount) ) {
			}
		}
		
		/*$this->_db->update('users', array(
			'balance' => new Zend_Db_Expr('balance + ' . $transfer->paid_amount)
		), $this->_db->quoteInto('id = ?', $transfer->user_id));

		$this->_db->insert('balance_transfers', array(
			'user_id' => $transfer->user_id,
			'transfer_id' => $transfer_id,
			'amount' => $transfer->paid_amount
		));*/

		$this->_startCron('transfer', $transfer_id);
	}

	public function onTransferRejected($transfer_id, $email_data = array())
	{
		//Cubix_Email::sendTemplate('universal_message', 'dave.kasparov@gmail.com', array('message' => serialize($email_data)));
		Cubix_Email::sendTemplate('transfer_rejected', $email_data['email'], $email_data);
	}

	public function onOrderCreated($order_id)
	{
		//
		$this->_startCron('order', $order_id);
	}

	public function onOrderEdited($order_id)
	{
		$this->_startCron('order', $order_id);
	}
	
	/*public function curl_get_file_contents($URL)
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        curl_close($c);

        if ($contents) return $contents;
            else return FALSE;
    }*/

	public function onOrderPaid($order_id)
	{
		if ( Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT ) {
			/*$back_url = Cubix_Application::getById()->backend_url;

			$order_details = file_get_contents('http://' . $back_url . '/billing/orders/view?id=' . $order_id . '&for_email=1');
			
			
			Cubix_Email::sendTemplate('paid_order', 'dave.kasparov@gmail.com', array('info' => $order_details));*/
		}
		
		/*$order_packages = self::getAdapter()->fetchAll('
			SELECT id, price FROM order_packages WHERE order_id = ?
		', $order_id);

		foreach ( $order_packages as $package ) {
			self::getAdapter()->insert('balance_packages_activation', array(
				'order_package_id' => $package->id,
				'amount' => $package->price
			));
		}*/
	}

	public function onOrderClosed($order_id)
	{
		
	}

	public function onOrderPaymentDetailsReceived($order_id, $transfer_id)
	{
		$transfer = new Model_Billing_Transfers();
		$transfer = $transfer->getDetails($transfer_id);
		
		$this->_db->beginTransaction();
		try {
			
			$status = Model_Billing_Orders::STATUS_PAYMENT_DETAILS_RECEIVED;
			if ( $transfer->transfer_type_id == 6 ) {
				$status = Model_Billing_Orders::STATUS_PAID;
			}
			
			$this->_db->update('orders', array(
				'status' => $status
			), $this->_db->quoteInto('id = ?', $order_id));

			$this->_db->commit();
		}
		catch (Exception $e) {
			$this->_db->rollBack();

			throw $e;
		}
	}

	public function onAdminCancelledPackage($order_package_id)
	{
		$this->_startCron('package', $order_package_id);
	}

	public function onAdminUpgradedPackage($old_order_package_id, $new_order_package_id)
	{
		$this->_startCron('package', $new_order_package_id);
	}

	public function onPackageCancelled($order_package_id)
	{
		$escort_id = $this->_db->fetchOne('SELECT escort_id FROM order_packages WHERE id = ?', $order_package_id);
		$this->_db->update('escorts', array(
			'active_package_id' => null, 
			'active_package_name' => null, 
			'package_activation_date' => null, 
			'package_expiration_date' => null,
			),
			$this->_db->quoteInto('id = ?', $escort_id)
		);
		Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_PACKAGE_DEACTIVATED, array('order_package_id' => $order_package_id));
	}

	public function onDefaultPackageCancelled($order_package_id)
	{
		
	}

	public function onPackageUpgraded($order_package_id)
	{
		
	}

	public function onPackageExpired($order_package_id, $activated)
	{
		$escort_id = $this->_db->fetchOne('SELECT escort_id FROM order_packages WHERE id = ?', $order_package_id);
		if(is_array($activated) && count($activated) == 0){
			$this->_db->update('escorts', array(
				'active_package_id' => null, 
				'active_package_name' => null, 
				'package_activation_date' => null, 
				'package_expiration_date' => null,
				),
				$this->_db->quoteInto('id = ?', $escort_id)
			);
		}
		Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_PACKAGE_DEACTIVATED, array('order_package_id' => $order_package_id, 'activated_instead' => $activated));
	}

	public function onPackageActivated($order_package_id, $escort_id, $date_activated = null, $expiration_date = null)
	{
		$package = $this->_db->fetchRow('SELECT p.name, op.package_id FROM order_packages op INNER JOIN packages p ON p.id = op.package_id WHERE op.id = ?', $order_package_id);
		$this->_db->update('escorts', array(
			'active_package_id' => $package->package_id, 
			'active_package_name' => $package->name, 
			'package_activation_date' => $date_activated, 
			'package_expiration_date' => $expiration_date,
			'date_last_modified' => new Zend_Db_Expr('NOW()')
			),
			$this->_db->quoteInto('id = ?', $escort_id)
		);

		Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_PACKAGE_ACTIVATED, array('order_package_id' => $order_package_id ));
		$m_snapshot = new Model_Escort_SnapshotV2(array('id' => $escort_id));
		$m_snapshot->snapshotProfileV2();
		$escort = $this->_db->fetchRow('SELECT u.email, e.showname FROM escorts e INNER JOIN users u ON u.id = e.user_id WHERE e.id = ?', $escort_id);
		$package_products = $this->_db->fetchAll('SELECT product_id FROM package_products WHERE package_id = ?', $package->package_id, Zend_db::FETCH_ASSOC);

		$has_national_listing = false;
		$has_tour_ability = false;
		if ( count($package_products) > 0 ) {
			foreach ( $package_products as $product ) {
				if ( $product['product_id'] == 1 ) {
					$has_national_listing = true;
				}
				else if ( $product['product_id'] == 7 ) {
					$has_tour_ability = true;
				}
			}
		}

		// APP_ED Send "Profile is online" email only for front registered escorts (disables for backend created and synced escorts )
		if ( $package->name != "Zero Package" && Cubix_Application::getId() == APP_ED ) {
			$esc = $this->_db->fetchRow('SELECT sync_escort, backend_escort FROM escorts WHERE id = ?', $escort_id);

			if ( ! $esc->sync_escort && ! $esc->backend_escort ) {
				//Cubix_Email::sendTemplate('profile_will_be_visible_online', $escort->email, array('showname' => $escort->showname, 'escort_id' => $escort_id));
				//Cubix_Email::sendTemplate('profile_will_be_visible_online_v1', $escort->email, array('showname' => $escort->showname, 'escort_id' => $escort_id));
				
			}
		}
		
		if ( $package->name != "Zero Package" ) {
			if ( $has_national_listing || $has_tour_ability ) {
				
				if ( !in_array(Cubix_Application::getId(),array(APP_6C, APP_EG_CO_UK, APP_EG,APP_ED, APP_6B)) ) {
					Cubix_Email::sendTemplate('profile_will_be_visible_online', $escort->email, array('showname' => $escort->showname, 'escort_id' => $escort_id));
				}

				//send package activation alert to escort only for and6
				if ( Cubix_Application::getId() == APP_A6 ) {
					$model_sms = new Model_SMS_Outbox();
					$originator = Cubix_Application::getPhoneNumber(Cubix_Application::getId());

					$text = "Dein Profil ist jetzt online!
Beste Grüsse
Dein and6.com Team 
---
Your Profile is online now!
Best regards
Your and6.com Team";
					$phone_to = $this->_db->fetchOne('SELECT contact_phone_parsed FROM escort_profiles_v2 WHERE escort_id = ?', $escort_id);
					$send_online_now_sms = $this->_db->fetchOne('SELECT send_online_now_sms FROM escorts WHERE id = ?', $escort_id);

					if ( strlen($phone_to) && $send_online_now_sms ) {
						$data = array(
							'escort_id' => $escort_id,
							'phone_number' => $phone_to,
							'phone_from' => $originator,
							'text' => $text,
							'application_id' => Cubix_Application::getId(),
						);
						$model_sms->saveAndSend($data);
					}
				}
			}
		}

        if ( in_array(Cubix_Application::getId(),array(APP_EG_CO_UK, APP_6B)) ) {
            $package = $this->_db->fetchRow(
                'SELECT p.name, op.package_id,op.period,op.expiration_date FROM order_packages op INNER JOIN packages p ON p.id = op.package_id WHERE op.id = ?', $order_package_id);

            if(!(Cubix_Application::getId() == APP_6B && $package->name == 'Bronze Normal')) {
                if (Cubix_Application::getId() != APP_EG_CO_UK || (Cubix_Application::getId() == APP_EG_CO_UK && !in_array($package->package_id, [106, 105, 43]))) {
                    Cubix_Email::sendTemplate('escort_package_activated', $escort->email,
                        array(
                            'username' => $escort->showname,
                            'package'=>$package->name,
                            'escort_id' => $escort_id,
                            'count' => $package->period,
                            'date' => $package->expiration_date
                        )
                    );
                }
            }
        }
	}

	protected function _startCron($type, $id)
	{
		set_time_limit(0);

		$user_ids = array();

		switch ( $type ) {
			case 'transfer':
				$user_ids = $this->_db->fetchAll('
					SELECT o.user_id
					FROM transfer_orders `to`
					INNER JOIN orders o ON o.id = to.order_id
					WHERE to.transfer_id = ?
				', $id);

				foreach ( $user_ids as $i => $user ) {
					$user_ids[$i] = $user->user_id;
				}
				
				break;
			case 'order':
				$user_id = $this->_db->fetchOne('
					SELECT user_id FROM orders WHERE id = ?
				', $id);

				if ( $user_id ) {
					$user_ids = array($user_id);
				}

				break;
			case 'package':
				$user_id = $this->_db->fetchOne('
					SELECT o.user_id
					FROM order_packages op
					INNER JOIN orders o ON o.id = op.order_id
					WHERE op.id = ?
				', $id);

				if ( $user_id ) {
					$user_ids = array($user_id);
				}

				break;
		}


		$mOrders = new Model_Billing_Orders();

		foreach ( $user_ids as $user_id ) {
			$mOrders->processUserOrders($user_id);
		}
	}

	public function fixDefaultPackages(/*array $escorts*/)
	{
		/*foreach ( $escorts as $escort ) {
			$escort = $this->_db->fetchRow('SELECT id, agency_id, gender, country_id FROM escorts WHERE id = ?', $escort);
			if ( ! $escort ) continue;
			$escort = new Model_EscortItem($escort);
			$escort->processDefaultPackages();
		}*/

		$escorts = $this->_db->fetchAll('
			SELECT c.id, c.agency_id, c.gender, c.country_id FROM (
				SELECT
					e.id, e.agency_id, e.gender, e.country_id, COUNT(a.id) AS apps
				FROM applications a
				RIGHT JOIN order_packages op ON op.application_id = a.id AND op.status = 2
				RIGHT JOIN escorts e ON e.id = op.escort_id
				WHERE (e.status & ' . ESCORT_STATUS_ACTIVE  . ' AND NOT e.status & ' . ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_ADMIN_DISABLED . ')
				GROUP BY e.id
			) c WHERE c.apps = 0
		');

		foreach ( $escorts as $escort ) {
			$escort = new Model_EscortItem($escort);
			$escort->processDefaultPackages();
		}

		/*$escorts = $this->_db->fetchAll('
			SELECT
				e.id, e.agency_id, e.gender, e.country_id, COUNT(a.id) AS apps
			FROM escorts e
			INNER JOIN order_packages op ON op.escort_id <> e.id
			WHERE (e.status & ' . ESCORT_STATUS_ACTIVE  . ' AND NOT e.status & ' . ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_ADMIN_DISABLED . ')
			GROUP BY e.id
		');*/
	}
}
