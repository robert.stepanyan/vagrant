<?php

class Model_Billing_BubbleTransfers extends Cubix_Model
{
	protected $_table = 'bubble_phone_transfers';
	protected $_itemClass = 'Model_Billing_BubbleTransferItem';
	

	public function getAll($filter, $page, $per_page, $sort_field, $sort_dir)
	{
		$sql = "
			SELECT SQL_CALC_FOUND_ROWS
				t.id, e.showname, t.amount, t.payment_id, UNIX_TIMESTAMP(t.date) as date
			FROM bubble_phone_transfers t
			INNER JOIN escorts e ON e.id = t.escort_id
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN backend_users bu ON bu.id = u.sales_user_id
			
			WHERE 1
		";
		
		$sqlPaid = "
			SELECT SQL_CALC_FOUND_ROWS
				SUM(t.amount) AS paid
			FROM bubble_phone_transfers t
			INNER JOIN escorts e ON e.id = t.escort_id
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN backend_users bu ON bu.id = u.sales_user_id
			
			WHERE 1
		";

		$countSql = "SELECT FOUND_ROWS()";	

		if ( strlen($filter['e.showname']) ) {
			$where .= self::quote('AND e.showname LIKE ?', $filter['e.showname'] . '%');
		}
		
		if ( strlen($filter['t.payment_id']) ) {
			$where .= self::quote('AND t.payment_id = ?', $filter['t.payment_id']);
		}
		
		if ( strlen($filter['date_from']) ) {
			$where .= self::quote('AND DATE(t.date) >= DATE(FROM_UNIXTIME(?))', $filter['date_from']);
		}
		
		if ( strlen($filter['date_to']) ) {
			$where .= self::quote('AND DATE(t.date) <= DATE(FROM_UNIXTIME(?))', $filter['date_to']);
		}
		
		
		$sql .= $where;
		$sqlPaid .= $where;

		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$data = parent::_fetchAll($sql);
		$count = intval($this->getAdapter()->fetchOne($countSql));
		$paid = intval($this->getAdapter()->fetchOne($sqlPaid));
		
		return array(
			'data' => $data,
			'count' => $count,
			//'total' => number_format($total, 2) . ' ' . Cubix_Application::getById()->currency_symbol,
			'paid' => number_format($paid, 2) . ' ' . Cubix_Application::getById()->currency_symbol
		);		
	}
}
