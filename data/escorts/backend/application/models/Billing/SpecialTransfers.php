<?php

class Model_Billing_SpecialTransfers extends Cubix_Model
{
	protected $_table = 'special_transfers';
	protected $_itemClass = 'Model_Billing_SpecialTransferItem';

	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS 
				st.id, st.first_name, st.last_name, st.email, st.transaction_id, st.amount,
				st.cc_number, UNIX_TIMESTAMP(st.cc_exp_date) AS cc_exp_date, st.ip, st.comment, 
				UNIX_TIMESTAMP(st.date_transfered) AS date_transfered, 
				st.application_id, st.backend_user_id, bu.username AS backen_user_username,
				st.is_charged_back
			FROM special_transfers st
			INNER JOIN backend_users bu ON bu.id = st.backend_user_id
			WHERE 1
		';
		
		if ( strlen($filter['application_id']) ) {
			$sql .= self::quote('AND st.application_id = ?', $filter['application_id']);
		}

		if ( strlen($filter['first_name']) ) {
			$sql .= self::quote('AND st.first_name LIKE ?', '%' . $filter['first_name'] . '%');
		}
		if ( strlen($filter['last_name']) ) {
			$sql .= self::quote('AND st.last_name LIKE ?', '%' . $filter['last_name'] . '%');
		}
		if ( strlen($filter['transaction_id']) ) {
			$sql .= self::quote('AND st.transaction_id = ?', $filter['transaction_id']);
		}
		if ( strlen($filter['amount']) ) {
			$sql .= self::quote('AND st.amount = ?', $filter['amount']);
		}
		if ( strlen($filter['cc_number']) ) {
			$sql .= self::quote('AND st.cc_number = ?', $filter['cc_number']);
		}
		if ( strlen($filter['email']) ) {
			$sql .= self::quote('AND st.email LIKE ?', '%' . $filter['email'] . '%');
		}
		if ( strlen($filter['ip']) ) {
			$sql .= self::quote('AND st.ip LIKE ?', '%' . $filter['ip'] . '%');
		}
		if ( strlen($filter['comment']) ) {
			$sql .= self::quote('AND st.comment LIKE ?', '%' . $filter['comment'] . '%');
		}
		if ( strlen($filter['sales_user_id']) ) {
			$sql .= self::quote('AND bu.id = ?', $filter['sales_user_id']);
		}
		if ( strlen($filter['cc_exp_date']) ) {
			$sql .= self::quote('AND st.cc_exp_date = ?', date("Y-m-d", $filter['cc_exp_date']));
		}

		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		$data = parent::_fetchAll($sql);
		$count = intval($this->getAdapter()->fetchOne('SELECT FOUND_ROWS()'));
		
		return $data;
	}
	
	public function getById($id)
	{
		return self::_fetchRow('
			SELECT
				st.id, st.first_name, st.last_name, st.email, st.transaction_id, st.amount,
				st.cc_number, st.cc_exp_date AS cc_exp_date, st.ip, st.comment, 
				UNIX_TIMESTAMP(st.date_transfered) AS date_transfered, 
				st.application_id, st.backend_user_id, bu.username AS backen_user_username,
				st.is_charged_back
			FROM special_transfers st
			INNER JOIN backend_users bu ON bu.id = st.backend_user_id
			WHERE st.id = ?
		', $id);
	}
	
	public function save($data)
	{
		if ( isset($data['id']) && $data['id'] ) {
			self::getAdapter()->update($this->_table, $data, self::getAdapter()->quoteInto('id = ?', $data['id']));
		}
		else {
			self::getAdapter()->insert($this->_table, $data);
		}
	}
	
	public function delete($id)
	{
		$this->getAdapter()->query('DELETE FROM special_transfers WHERE id = ?', array($id));
	}
	
	public function isSpecialTransferExist($transaction_id)
	{
		return self::getAdapter()->fetchOne('SELECT TRUE FROM special_transfers WHERE transaction_id = ?', $transaction_id);
	}
	
	public function isSpecialTransferExistI($transaction_id, $id = null)
	{
		$sql = 'SELECT TRUE FROM special_transfers WHERE transaction_id = "' . $transaction_id . '"';
		
		if ( $id ) {
			$sql .= ' AND id <> ' . $id;
		}
		
		return self::getAdapter()->fetchOne($sql);
	}
}