<?php
class Model_Billing_Tokens extends Cubix_Model
{

	const STATUS_PENDING = 0;
	const STATUS_CAM_FAILED = 1;
	const STATUS_CONFIRMED = 2;
	const STATUS_REJECTED = 3;
	
	public static $STATUS_LABELS = array(
		self::STATUS_PENDING => 'Pending',
		self::STATUS_CAM_FAILED => 'CP Error',
		self::STATUS_CONFIRMED => 'Confirmed',
		self::STATUS_REJECTED => 'Rejected',
	);	
	
	public function addTransfer($tr_data)
	{
		self::getAdapter()->insert('token_transfers', $tr_data);
	}	

	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				tr.id, m.id as member_id, tr.tokens, tt.transaction_id, tr.amount,  tr.status, UNIX_TIMESTAMP(tt.date_transfered) AS date_transfered,
				tr.reject_reason, m.email
			FROM token_requests tr 
			INNER JOIN users u ON u.id = tr.user_id
			INNER JOIN members m ON m.user_id = tr.user_id
			LEFT JOIN token_transfers tt ON tt.request_id = tr.id
			WHERE 1
		';
		
		$countSql = "SELECT FOUND_ROWS()";

		$where = '';
		
		if ( strlen($filter['member_id']) ) {
			$where .= self::quote('AND m.id = ?', $filter['member_id']);
		}
		if ( strlen($filter['email']) ) {
			$where .= self::quote('AND m.email = ?', $filter['email']);
		}

		if ( strlen($filter['tokens']) ) {
			$where .= self::quote('AND tr.tokens = ?', $filter['tokens']);
		}

		if ( strlen($filter['amount']) ) {
			$where .= self::quote('AND tr.amount = ?', $filter['amount']);
		}

		if ( strlen($filter['status']) ) {
			$where .= self::quote('AND tr.status = ?', $filter['status']);
		}

		if ( strlen($filter['transaction_id']) ) {
			$where .= self::quote('AND tt.transaction_id = ?', $filter['transaction_id']);
		}
	
		
		if ( strlen($filter['date_from']) ) {
			$where .= self::quote('AND DATE(tt.date_transfered) >= DATE(FROM_UNIXTIME(?))', $filter['date_from']);
		}

		if ( strlen($filter['date_to']) ) {
			$where .= self::quote('AND DATE(tt.date_transfered) <= DATE(FROM_UNIXTIME(?))', $filter['date_to']);
		}

	
		$sql .= $where;
		
			$sql .= '
				ORDER BY ' . $sort_field . ' ' . $sort_dir . '
				LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
			';	
		
		
		$result = parent::_fetchAll($sql);
		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return $result;
	}

	public function getTotalAmount($filter)
	{

		$sql = '
			SELECT 
				sum(tr.amount)
			FROM token_requests tr 
			INNER JOIN users u ON u.id = tr.user_id
			INNER JOIN members m ON m.user_id = tr.user_id
			LEFT JOIN token_transfers tt ON tt.request_id = tr.id
			WHERE 1
		';
		

		$where = '';
		
		if ( strlen($filter['member_id']) ) {
			$where .= self::quote('AND m.id = ?', $filter['member_id']);
		}
		if ( strlen($filter['email']) ) {
			$where .= self::quote('AND m.email = ?', $filter['email']);
		}

		if ( strlen($filter['tokens']) ) {
			$where .= self::quote('AND tr.tokens = ?', $filter['tokens']);
		}

		if ( strlen($filter['amount']) ) {
			$where .= self::quote('AND tr.amount = ?', $filter['amount']);
		}

		if ( strlen($filter['status']) ) {
			$where .= self::quote('AND tr.status = ?', $filter['status']);
		}

		if ( strlen($filter['transaction_id']) ) {
			$where .= self::quote('AND tt.transaction_id = ?', $filter['transaction_id']);
		}
	
		
		if ( strlen($filter['date_from']) ) {
			$where .= self::quote('AND DATE(tt.date_transfered) >= DATE(FROM_UNIXTIME(?))', $filter['date_from']);
		}

		if ( strlen($filter['date_to']) ) {
			$where .= self::quote('AND DATE(tt.date_transfered) <= DATE(FROM_UNIXTIME(?))', $filter['date_to']);
		}

	
		$sql .= $where;
		
		return  $this->getAdapter()->fetchOne($sql);
	}

	public function getTransfer($request_id)
	{
		$sql = 'SELECT SQL_CALC_FOUND_ROWS
				tt.transaction_id,
				tt.payer_name,
				tr.amount,
				tr.tokens,
				tt.cc_number,
				tt.cc_holder,
				tt.cc_exp_month,
				tt.cc_exp_year,
				
				tt.ip,
				tr.status,
				tt.date_transfered,
				m.id as member_id,
				u.username,
				u.email
			FROM
				token_requests tr
			INNER JOIN users u ON u.id = tr.user_id
			INNER JOIN members m ON m.user_id = tr.user_id
			INNER JOIN token_transfers tt ON tt.request_id = tr.id
			WHERE
				tr.id = ?';

        return  parent::_fetchRow($sql,array($request_id));
	}

	public function getDetails($request_id)
	{
		$sql = 'SELECT
					* 
				FROM
					token_transfers tt
				WHERE
					tt.request_id = ?';

		return  parent::_fetchRow($sql,array($request_id));
	}
	
	public function reject($comment, $request_id)
	{
		$db = self::getAdapter();
		$bu_user = Zend_Auth::getInstance()->getIdentity();
			
		$data = array(
			'status' => self::STATUS_REJECTED,
			'reject_person' => $bu_user->id,
			'reject_reason' => $comment,
			'reject_date' => new Zend_Db_Expr('NOW()')
		);
		
		$db->update('token_requests', $data, $db->quoteInto('id = ?', $request_id));			
	}		
}
?>