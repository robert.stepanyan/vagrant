<?php
class Model_Billing_Credits extends Cubix_Model
{

	const STATUS_PENDING = 1;
	const STATUS_CONFIRMED = 2;
	const STATUS_REJECTED = 3;
	
	public static $STATUS_LABELS = array(
		self::STATUS_PENDING => 'Pending',
		self::STATUS_CONFIRMED => 'Confirmed',
		self::STATUS_REJECTED => 'Rejected',
	);	
	
	public function addTransfer($tr_data)
	{
		self::getAdapter()->insert('credit_transfers', $tr_data);
	}	
	
	public function getRequestDetails($request_id)
	{
		$sql = 'SELECT
					* 
				FROM
					credit_requests cr
				WHERE
					cr.id = ?';

		return  parent::_fetchRow($sql, array($request_id));
	}
	
	public function confirmRequestStatus($id)
	{
		$db = self::getAdapter();
		$db->update('credit_requests', ['status' => Model_Billing_Credits::STATUS_CONFIRMED ], $db->quoteInto('id = ?', $id));	
	}	

	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				ct.id, ct.credits, ct.transaction_id, ct.amount, UNIX_TIMESTAMP(ct.date_transfered) AS date_transfered,
				/*ct.reject_reason,*/ u.email, u.username, u.user_type
			FROM credit_transfers ct  
			INNER JOIN users u ON u.id = ct.user_id
			WHERE 1
		';
		
		$countSql = "SELECT FOUND_ROWS()";

		$where = '';
		
		if ( strlen($filter['username']) ) {
			$where .= self::quote('AND u.username = ?', $filter['username']);
		}
		if ( strlen($filter['email']) ) {
			$where .= self::quote('AND u.email = ?', $filter['email']);
		}
		
		if ( strlen($filter['status']) ) {
			$where .= self::quote('AND ct.status = ?', $filter['status']);
		}

		if ( strlen($filter['transaction_id']) ) {
			$where .= self::quote('AND ct.transaction_id = ?', $filter['transaction_id']);
		}
	
		
		if ( strlen($filter['date_from']) ) {
			$where .= self::quote('AND DATE(ct.date_transfered) >= DATE(FROM_UNIXTIME(?))', $filter['date_from']);
		}

		if ( strlen($filter['date_to']) ) {
			$where .= self::quote('AND DATE(ct.date_transfered) <= DATE(FROM_UNIXTIME(?))', $filter['date_to']);
		}

	
		$sql .= $where;
		
			$sql .= '
				ORDER BY ' . $sort_field . ' ' . $sort_dir . '
				LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
			';	
		
		
		$result = parent::_fetchAll($sql);
		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return $result;
	}

	
	
	public function getDetails($request_id)
	{
		$sql = 'SELECT
					ct.*, u.username, u.email, u.user_type 
				FROM
					credit_transfers ct
					INNER JOIN users u ON u.id = ct.user_id
				WHERE
					ct.id = ?';

		return  parent::_fetchRow($sql,array($request_id));
	}
	
	public function reject($comment, $request_id)
	{
		$db = self::getAdapter();
		$bu_user = Zend_Auth::getInstance()->getIdentity();
			
		$data = array(
			'status' => self::STATUS_REJECTED,
			'reject_person' => $bu_user->id,
			'reject_reason' => $comment,
			'reject_date' => new Zend_Db_Expr('NOW()')
		);
		
		$db->update('credit_requests', $data, $db->quoteInto('id = ?', $request_id));			
	}		
}
?>