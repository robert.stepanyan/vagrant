<?php

class Model_Billing_PaymentMethodes extends Cubix_Model
{
	protected $_table = 'payment_methodes';
	protected $_itemClass = 'Model_Billing_PaymentMethodes';

	public function getAll($p, $pp, $filter, $sort_field, $sort_dir, &$count){

        $sql = '
			SELECT pm.id,pm.title,pm.slug,pm.bu_user,pm.status,UNIX_TIMESTAMP(pm.last_update) last_update,ap.title AS application FROM payment_methodes as pm
			INNER JOIN applications ap ON ap.id = pm.application_id 
			WHERE pm.application_id = '.$filter['application_id'].'
			ORDER BY '.$sort_field.' '.$sort_dir.' 
		';

        $countSql = 'SELECT COUNT(id) FROM payment_methodes WHERE 1 ';

        $count = $this->getAdapter()->fetchOne($countSql);

        return $this->getAdapter()->fetchAll($sql);
    }

    public function get($id)
    {
        $sql = 'SELECT * FROM payment_methodes	WHERE id = ?';

        return $this->getAdapter()->fetchRow($sql, $id);
    }

    public function save($data)
    {
        if (isset($data['id']) && $data['id'] != '')
        {
            $id = $data['id'];

            parent::getAdapter()->update($this->_table, array(
                'title' => $data['title'],
                'slug' => $data['slug'],
                'bu_user' => $data['bu_user'],
                'application_id' => $data['application_id']
            ), parent::quote('id = ?', $id));
        }
        else
        {
            parent::getAdapter()->insert($this->_table, array(
                'title' => $data['title'],
                'slug' => $data['slug'],
                'bu_user' => $data['bu_user'],
                'application_id' => $data['application_id']
            ));

            $id = parent::getAdapter()->lastInsertId();
        }

        return $id;
    }


    public function remove($id)
    {
        parent::getAdapter()->delete('payment_methodes', parent::quote('id = ?', $id));
    }

    public function toggle($id)
    {
        $field = 'status';

        parent::getAdapter()->update($this->_table, array(
            $field => new Zend_Db_Expr('NOT ' . $field)
        ), parent::quote('id = ?', $id));
    }
}
