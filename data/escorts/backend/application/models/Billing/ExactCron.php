<?php

class Model_Billing_ExactCron
{
	/**
	 * @var Zend_Db_Adapter_Abstract
	 */
	protected $_db;

	/**
	 * @var Model_Cron
	 */
	protected static $_instance;

	/**
	 * @return Model_Billing_ExactCron
	 */
	public static function getInstance()
	{
		if ( empty(self::$_instance) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	public static function run($user_id, array $stages) {
		$instance = self::getInstance();
		
		$escort_ids = $instance->getEscortOrAgencyEscorts($user_id);
		// This measn if user_id is nor escort neigher agency
		if ( false === $escort_ids ) {
			return;
		}
		
		if ( is_numeric($escort_ids) ) {
			$escort_ids = array($escort_ids);
		}

		$result = array('count' => 0, 'escorts' => array());
		
		foreach ( $stages as $stage ) {
			$func = 'process ' . str_replace(array('_', '-'), array(' ', ' '), $stage);
			$func = str_replace(' ', '', ucwords($func));

			$args = array();

			// When we are processing pending orders we need to pass user id 'cause
			// order is associated with user_id, not agency or escort id
			if ( $stage == 'pending-orders' ) {
				$args[] = $user_id;
				list($count, $escorts) = call_user_func_array(array($instance, $func), $args);
				
				$result['count'] += $count;
				$result['escorts'] = array_merge($result['escorts'], $escorts);
			}
			// Otherwise we are processing packages and we need to pass escort id
			// 'cause in this case package is associated with escort id
			else {
				foreach ( $escort_ids as $escort_id ) {
					$args = array($escort_id);
					list($count, $escorts) = call_user_func_array(array($instance, $func), $args);
					
					$result['count'] += $count;
				}
			}
		}

		$result['escorts'] = array_unique($result['escorts']);

		// Finally proccess default packages
		foreach ( $escort_ids as $escort_id ) {
			$escort = Zend_Registry::get('db')->fetchRow('
				SELECT
					e.id, e.agency_id, e.gender, e.country_id, e.home_city_id
				FROM escorts e
				WHERE e.id = ?
			', $escort_id);

			$escort = new Model_EscortItem($escort);
			$escort->processDefaultPackages();
		}

		return $result;
	}

	public function getEscortOrAgencyEscorts($user_id)
	{
		$user_type = $this->_db->fetchOne('SELECT user_type FROM users WHERE id = ?', $user_id);

		if ( 'agency' == $user_type ) {
			$agency_id = $this->_db->fetchOne('SELECT id FROM agencies WHERE user_id = ?', $user_id);
			if ( ! $agency_id ) return false;

			$escorts = $this->_db->fetchAll('
				SELECT id FROM escorts
				WHERE agency_id = ? AND status & 32
			', array($agency_id));

			foreach ( $escorts as $i => $escort ) {
				$escorts[$i] = $escort->id;
			}

			return $escorts;
		}
		elseif ( 'escort' == $user_type ) {
			$escort_id = $this->_db->fetchOne('SELECT id FROM escorts WHERE user_id = ?', $user_id);
			if ( ! $escort_id ) return false;

			return $escort_id;
		}

		return false;
	}

	public function __construct()
	{
		$this->_db = Zend_Registry::get('db');
	}

	public function ProcessPendingOrders($user_id)
	{
		//
		$orders = $this->_db->fetchAll('
			SELECT
				o.id, u.id AS user_id, system_order_id, o.price
			FROM orders o
			INNER JOIN users u ON u.id = o.user_id
			INNER JOIN backend_users bu ON bu.id = o.backend_user_id
			INNER JOIN order_packages op ON op.order_id = o.id
			WHERE o.status IN (?, ?) AND o.user_id = ? AND o.use_balance = 1
			GROUP BY o.id
			ORDER BY u.id ASC, o.order_date ASC
		', array(Model_Billing_Orders::STATUS_PENDING, Model_Billing_Orders::STATUS_PAYMENT_DETAILS_RECEIVED, $user_id));
		
		$result = array(0, array());
		$model = new Model_Billing_Orders();
		foreach ( $orders as $order ) {
			if ( true === $model->pay($order->id) ) {
				$this->_log('Order paid', array('Order' => $order->id));
				$result[0]++;
			}
		}

		return $result;
	}

	public function ProcessExpiredPackages($escort_id)
	{
		$exclude = " ";
		//Exclude Phone Packages from expired packages list
		if ( Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT ) {
			$phone_packages = array(Model_Billing_Packages::PHONE_PACKAGE_1_DAY_PASS, Model_Billing_Packages::PHONE_PACKAGE_3_DAY_PASS, Model_Billing_Packages::PHONE_PACKAGE_5_DAY_PASS);
			$exclude .= " AND op.package_id NOT IN (" . implode(', ', $phone_packages) . ") ";
		}
		
		/* --> Get all expired packages */
		$expired_packages = $this->_db->fetchAll("
			SELECT
				op.id, op.escort_id, op.status, op.escort_id, op.application_id
			FROM order_packages op
			WHERE op.escort_id = ? AND DATE(op.expiration_date) <= DATE(NOW()) AND op.status = ? AND op.status <> ? {$exclude}
		", array($escort_id, Model_Packages::STATUS_ACTIVE, Model_Billing_Packages::STATUS_SUSPENDED));
		/* <-- */
		
		$result = array(0, array());
		foreach ( $expired_packages as $package ) {
			$result[1][] = $package->escort_id;
			/* --> Get packages that are needed to be activate after current's expiration */
			$packages = $this->_db->fetchAll('
				SELECT
					op.id, op.escort_id
				FROM order_packages op
				INNER JOIN orders o ON o.id = op.order_id
				WHERE
					op.activation_type = ? AND
					op.status = ? AND
					op.escort_id = ? AND
					op.application_id = ?
			', array(
				Model_Packages::ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES,
				Model_Packages::STATUS_PENDING,
				$package->escort_id,
				$package->application_id
			));
			/* <-- */

			$this->_db->beginTransaction();
			try {
				$activated = array();
				foreach ( $packages as $p ) {
					$this->_ActivatePackage($p->id, 'ProcessExpiredPackages');
					$result[0]++;
					$result[1][] = $p->escort_id;
				}

				$this->_db->update('order_packages', array(
					'status' => Model_Billing_Packages::STATUS_EXPIRED
				), $this->_db->quoteInto('id = ?', $package->id));

				$this->_db->commit();
				Zend_Registry::get('BillingHooker')->notify('package_expired', array($package->id, $activated));

				$result[0]++;
				$result[1][] = $package->escort_id;

				$this->_log('Package expired', array('Expired Package' => $package->id, 'Activated Instead' => $activated));
			}
			catch (Exception $e) {
				$this->_db->rollBack();

				$this->_log('Unable to expire the package', array('Package' => $package->id), 'ERROR');
			}
		}

		return $result;
	}

	public function ProcessAsapPackages($escort_id)
	{
		$packages = $this->_GetWaittingPackages($escort_id, Model_Packages::ACTIVATE_ASAP);

		$result = array(0, array());
		foreach ( $packages as $package ) {
			$this->_ActivatePackage($package->id, 'ProcessAsapPackages');
			$this->_log('Asap package activated', array('Package' => $package->id));
			$result[0]++;
			$result[1][] = $package->escort_id;
		}

		return $result;
	}

	public function ProcessExactDatePackages($escort_id)
	{
		$packages = $this->_GetWaittingPackages($escort_id, Model_Packages::ACTIVATE_AT_EXACT_DATE, 'DATE_FORMAT(op.activation_date, "%Y-%m-%d %H:%i:%s") <= DATE_FORMAT(NOW(), "%Y-%m-%d %H:%i:%s")');

		$result = array(0, array());
		foreach ( $packages as $package ) {
			$this->_ActivatePackage($package->id, 'ProcessExactDatePackages');
			$this->_log('Exact date package activated', array('Package' => $package->id));
			$result[0]++;
			$result[1][] = $package->escort_id;
		}

		return $result;
	}

	/**
	 * Returns all packages that are waitting for activation
	 *
	 * @param int $which Package activation type
	 * @return array
	 */
	protected function _GetWaittingPackages($escort_id, $which, $additional = '')
	{
		$packages = $this->_db->fetchAll('
			SELECT
				op.id, op.activation_date, op.escort_id, op.application_id
			FROM order_packages op
			INNER JOIN orders o ON o.id = op.order_id
			WHERE
				op.escort_id = ? AND op.status = ? AND (
					( o.activation_condition = ? AND o.status = ? ) OR
					( o.activation_condition = ? AND o.status IN (?, ?) ) OR
					( o.activation_condition = ? AND o.status IN (?, ?, ?) )
				) AND op.activation_type = ?' . (strlen($additional) ? ' AND ' . $additional : '') . '
			ORDER BY op.escort_id, o.order_date DESC
		', array(
			$escort_id,
			Model_Billing_Packages::STATUS_PENDING,

			/* --> Main Conditions-Statuses Logic (it describes which ones are "waitting for activation" packages) */
			Model_Billing_Orders::CONDITION_AFTER_PAID, Model_Billing_Orders::STATUS_PAID,
			Model_Billing_Orders::CONDITION_AFTER_RECEIVING_PAYMENT_DETAILS, Model_Billing_Orders::STATUS_PAID, Model_Billing_Orders::STATUS_PAYMENT_DETAILS_RECEIVED,
			Model_Billing_Orders::CONDITION_WITHOUT_PAYMENT, Model_Billing_Orders::STATUS_PAID, Model_Billing_Orders::STATUS_PAYMENT_DETAILS_RECEIVED, Model_Billing_Orders::STATUS_PENDING,
			/* <-- */

			$which
		));

		return $packages;
	}

	protected function _ActivatePackage($order_package_id, $func_name = null)
	{
		list($escort_id, $application_id, $period, $de_days, $package_id) = $this->_db->fetchRow('
			SELECT escort_id, application_id, period, de_days, package_id FROM order_packages WHERE id = ? AND order_id IS NOT NULL
		', $order_package_id, Zend_Db::FETCH_NUM);

		$m_escorts = new Model_Escorts();

		// Process if escort status is active
		if ( $m_escorts->hasStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_ACTIVE) ) {
			/* --> Cancel Current Active Package of Escort and Chargeback to Balance */
			$current_order_package_id = $this->_db->fetchOne('
				SELECT id FROM order_packages WHERE status = ? AND escort_id = ? AND application_id = ?
			', array(Model_Packages::STATUS_ACTIVE, $escort_id, $application_id));

			if ( $current_order_package_id ) {
				$comment = '#EXACT_CRON# Package #' . $current_order_package_id . ' cancelled by cron. Needed to activate package #' . $order_package_id;
				
				if ( $func_name ) {
					$comment .= ' : Function name: ' . $func_name;
				}
				
				$model = new Model_Billing_Packages();
				$model
					->get($current_order_package_id)
					->cancel($comment);
			}
			/* <-- */

			/* --> Activate Package Immediately */
			$date_activated = new Zend_Db_Expr('NOW()');
			$expiration_date = new Zend_Db_Expr('FROM_UNIXTIME(' . strtotime('+' . $period + $de_days . ' days', time()) . ')');
			
			$order = $this->_db->fetchRow('
				SELECT o.id, has_rejected_transfer 
				FROM order_packages op 
				INNER JOIN orders o ON o.id = op.order_id
				WHERE op.id = ?
			', array($order_package_id));
			
			
			$order_packages_update_data = array(
				'expiration_date' => $expiration_date,
				'status' => Model_Packages::STATUS_ACTIVE,
				'date_activated' => $date_activated
			
			);
			//IF PAYING FOR ORDER WITH STATUS transfer_rejected update only status
			//KEEP expiration and activation dates THE SAME
			if ( $order->has_rejected_transfer ) {
				unset($order_packages_update_data['expiration_date']);
				unset($order_packages_update_data['date_activated']);
			}
			//REMOVING has_rejected_transfer STATUS
			$this->_db->update('orders', array('has_rejected_transfer' => 0), $this->_db->quoteInto('id = ?', $order->id));
			
			$this->_db->update('order_packages', $order_packages_update_data, $this->_db->quoteInto('id = ?', $order_package_id));

			// Insert phone package expiration date with hours, minutes and seconds
			if ( (Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT) && in_array($package_id, array(Model_Billing_Packages::PHONE_PACKAGE_1_DAY_PASS, Model_Billing_Packages::PHONE_PACKAGE_3_DAY_PASS, Model_Billing_Packages::PHONE_PACKAGE_5_DAY_PASS)) ) {
				$this->_db->insert('phone_packages', array('order_package_id' => $order_package_id, 'expiration_date' => $expiration_date));
				//$this->_db->update('phone_packages', array('expiration_date' => $expiration_date), $this->_db->quoteInto('order_package_id = ?', $order_package_id));
			}
			
			Zend_Registry::get('BillingHooker')->notify('package_activated', array($order_package_id, $escort_id, $date_activated, $expiration_date));

			/*$this->_db->update('balance_packages_activation', array(
				'date' => new Zend_Db_Expr('NOW()')
			), $this->_db->quoteInto('order_package_id = ?', $order_package_id));*/
		}
	}

	protected $_log_file = 'billing-cron.log';

	public function _log($message, array $data = array(), $type = 'INFO')
	{
		foreach ( $data as $key => $value ) {
			if ( is_array($value) ) {
				$value = '(' . implode(', ', $value) . ')';
			}

			$data[$key] = $key . ': ' . $value;
		}

		$message = '[' . $type . ' | EXACT CRON ' . date('d.m.y H:i:s') . ']: ' . $message . ' (' . implode(', ', $data) . ')' . "\r\n";

		@file_put_contents($this->_log_file, $message, FILE_APPEND);
		//echo nl2br($message);
	}
}
