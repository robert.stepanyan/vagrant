<?php

class Model_Billing_PackageItem extends Cubix_Model_Item
{
	public function isSuspendable()
	{
		$sql = "
			SELECT
				product_id
			FROM order_package_products
			WHERE order_package_id = ?
		";

		$products = parent::getAdapter()->query($sql, array($this->getorderPackageId()))->fetchAll(Zend_Db::FETCH_ASSOC);
		
		$is_suspendable = 0;
		
		foreach ( $products as $product ) {			
			if ( Model_Billing_Packages::PRODUCT_SUSPENDABLE == $product['product_id'] ) {
				return 1;
			}
		}
		
		return $is_suspendable;
	}
	
	public function showExtendPackage()
	{
		$show_extend_package = false;
		
		if ( in_array($this->package_id, array(Model_Billing_Packages::PACKAGE_DIAMOND_LIGHT, Model_Billing_Packages::PACKAGE_DIAMOND_PREMIUM, Model_Billing_Packages::PACKAGE_GOLD_TOUR_PREMIUM)) ) {
			$sql = "
				SELECT
					product_id
				FROM order_package_products
				WHERE order_package_id = ?
			";

			$products = parent::getAdapter()->query($sql, array($this->getorderPackageId()))->fetchAll(Zend_Db::FETCH_ASSOC);
			
			$show_extend_package = true;
			
			foreach ( $products as $product ) {
				
				if ( in_array($product['product_id'], Model_Billing_Packages::$EXTEND_PACKAGE_PRODUCTS)) {
					$show_extend_package = false;
					break;
				}
			}
		}
		
		return $show_extend_package;
	}
	
	public function showAddGotd()
	{
		return parent::getAdapter()->fetchOne('SELECT true FROM package_products WHERE package_id = ? AND product_id = ?', array($this->package_id, GIRL_OF_THE_DAY));
	}

	public function showAddVotd($escort_id)
	{
		return parent::getAdapter()->fetchOne('SELECT true FROM video WHERE escort_id = ? AND status = ?', array($escort_id, 'approve'));
	}
	
	public function showAddProfileBoost()
	{
		return parent::getAdapter()->fetchOne('SELECT true FROM package_products WHERE package_id = ? AND product_id = ?', array($this->package_id, PRODUCT_BOOST_PROFILE));
	}
	
	public function showAddExtraCountry($order_package_id)
	{
		$has_product = parent::getAdapter()->fetchOne('SELECT true FROM package_products WHERE package_id = ? AND product_id = ?', array($this->package_id, Model_Products::EXTRA_COUNTRY));
		
		if ($has_product)
		{
			$has_extra_country = parent::getAdapter()->fetchOne('SELECT true FROM package_extra_cities WHERE order_package_id = ?', array($order_package_id));
			
			if (!$has_extra_country)
				return TRUE;
		}
		
		return FALSE;
	}
	
	public function showChangePremiumCities()
	{
		return parent::getAdapter()->fetchOne('SELECT true FROM package_products WHERE package_id = ? AND product_id = ?', array($this->package_id, Model_Products::CITY_PREMIUM_SPOT));
	}
	
	public function getEdListingProducts($order_package_id)
	{
		return parent::getAdapter()->fetchOne('
			SELECT GROUP_CONCAT(product_id)
			FROM order_package_products
			WHERE order_package_id = ? AND product_id IN (' . implode(',', array(PRODUCT_ED_LISTING, PRODUCT_NO_ED_LISTING)) . ')
			GROUP BY order_package_id
		', array($order_package_id));
		
	}
	
	public function getSuspendDate()
	{
		$sql = "
			SELECT
				UNIX_TIMESTAMP(suspend_date) AS suspend_date
			FROM suspend_resume_dates
			WHERE order_package_id = ? AND resume_date IS NULL
		";

		$date = parent::getAdapter()->query($sql, array($this->getorderPackageId()))->fetch();
			
		return $date;
	}
	
	public function getPremiumSpotCities()
	{
		$sql = "
			SELECT
				city_id
			FROM premium_escorts
			WHERE order_package_id = ?
		";

		return parent::getAdapter()->query($sql, array($this->getId()))->fetchAll();
	}
	
	public function getAddAreas()
	{
		$sql = "
			SELECT
				area_id AS id
			FROM additional_areas
			WHERE order_package_id = ?
		";

		return parent::getAdapter()->query($sql, array($this->getId()))->fetchAll();
	}
	
	public function getExtraCountryCities()
	{
		$sql = "
			SELECT
				city_id AS id, is_premium
			FROM package_extra_cities
			WHERE order_package_id = ?
		";

		return parent::getAdapter()->query($sql, array($this->getId()))->fetchAll();
	}
	
	public function getVipExtraCountries()
	{
		$sql = "
			SELECT
				country_id
			FROM package_vip_extra_countries
			WHERE order_package_id = ?
		";

		return parent::getAdapter()->query($sql, array($this->getId()))->fetchAll();
	}

	public function getPremiumSpotTours()
	{
		$sql = "
			SELECT
				tour_id
			FROM premium_tours
			WHERE order_package_id = ?
		";

		return parent::getAdapter()->query($sql, array($this->getId()))->fetchAll();
	}

	public function getProducts($product_type = Model_Products::TYPE_PRODUCT)
	{
		$sql = "
			SELECT
				opp.*,
				p.name
			FROM order_package_products opp
			INNER JOIN products p ON p.id = opp.product_id
			INNER JOIN order_packages op ON op.id = opp.order_package_id
			WHERE opp.order_package_id = ? AND opp.is_optional = ?
		";

		return parent::getAdapter()->query($sql, array($this->getOrderPackageId(), $product_type))->fetchAll();
	}

	public function getDefaultProducts($product_type = Model_Products::TYPE_PRODUCT)
	{
		$sql = "
			SELECT
				pp.*,
				p.name
			FROM package_products pp
			INNER JOIN products p ON p.id = pp.product_id
			
			WHERE pp.package_id = ? AND pp.is_optional = ?
		";

		return parent::getAdapter()->query($sql, array($this->getPackageId(), $product_type))->fetchAll();
	}

	public function cancel($comment)
	{
		parent::getAdapter()->beginTransaction();
		
		try {
			parent::getAdapter()->query('
				UPDATE order_packages SET status = ?, status_comment = ? WHERE id = ?
			', array(
				Model_Billing_Packages::STATUS_CANCELLED,
				$comment,
				$this->getId()
			));

//			$message = 'Order #' . $this->order_id . ' status is "' . Model_Billing_Orders::$STATUS_LABELS[$this->getOrderStatus()] . '"' . "\r\n";
//			$message .= 'Package #' . $this->id . ' status is "' . Model_Billing_Packages::$STATUS_LABELS[$this->getStatus()] . '"' . "\r\n";
			
			// If order has been really bought, chargeback remaining money to user's balance
			if ( $this->getOrderStatus() == Model_Billing_Orders::STATUS_PAID &&
					in_array($this->getStatus(), array(Model_Billing_Packages::STATUS_ACTIVE, Model_Billing_Packages::STATUS_PENDING)) ) {
				$chargeback_amount = $this->getChargebackAmount();
				$amount = $chargeback_amount;
				
				// Leave for now 
				/*parent::getAdapter()->query('
					UPDATE users SET balance = (balance + ?) WHERE id = ?
				', array($chargeback_amount, $this->user_id));*/
			}
			// If order has not been bought, but the package is active, form a debt and subtract from user's balance
			elseif ( $this->getOrderStatus() == Model_Billing_Orders::STATUS_PAYMENT_DETAILS_RECEIVED &&
					in_array($this->getStatus(), array(Model_Billing_Packages::STATUS_ACTIVE, Model_Billing_Packages::STATUS_EXPIRED)) ) {
				$debt = $this->getDebt();
				$amount = -1 * $debt;
				
				// Leave for now 
				/*parent::getAdapter()->query('
					UPDATE users SET balance = (balance - ?) WHERE id = ?
				', array($debt, $this->user_id));
				*/
			}
			/*elseif ( $this->getOrderStatus() == Model_Billing_Orders::STATUS_PAYMENT_REJECTED &&
					in_array($this->getStatus(), array(Model_Billing_Packages::STATUS_ACTIVE, Model_Billing_Packages::STATUS_EXPIRED)) ) {
				$debt = $this->getDebt();

				parent::getAdapter()->query('
					UPDATE users SET balance = (balance - ?) WHERE id = ?
				', array($debt, $this->user_id));
				$amount = -1 * $debt;
				var_dump($amount) . " -- 2";die;
			}*/
			// Otherwise simply cancel the package
			else {
				$amount = 0;
			}

//			$message .= "Days used: " . $this->getUsedDays() . ' (' . $this->getUsedDaysPrice() . ' EUR)' . "\r\n";
//			$message .= 'Cancellation amount: ' . number_format($amount, 2) . "\r\n";
//			echo $message . "\r\n";

			parent::getAdapter()->insert('balance_packages_cancellation', array(
				'order_package_id' => $this->getId(),
				'amount' => $amount,
				'comment' => $comment
			));
			
			parent::getAdapter()->commit();
			Zend_Registry::get('BillingHooker')->notify('package_cancelled', array($this->getId()));
		}
		catch (Exception $e) {
			parent::getAdapter()->rollBack();

			throw $e;
		}
	}

	public function getUsedDays()
	{
		//$now = time();
		$now = mktime(0, 0, 0, date("n"), date("j"), date("Y"));

		if ( $this->getStatus() == Model_Billing_Packages::STATUS_EXPIRED ) {
			$now = strtotime($this->expiration_date);
		}
		elseif ( $this->getStatus() != Model_Billing_Packages::STATUS_ACTIVE ) {
			return 0;
		}

		$time_left = $now - strtotime($this->date_activated);
		return round((($time_left / 24) / 60) / 60);
	}

	public function getUsedDaysPrice()
	{
		$days_used = $this->getUsedDays();
		// If this package is default, the period is null, avoid the division by zero
		$day_price = $this->period ? ceil($this->price / $this->period) : 0;

		return $days_used * $day_price;
	}

	public function getChargebackAmount()
	{
		$amount = $this->price - $this->getUsedDaysPrice();
		
		return $amount;
	}

	public function getDebt()
	{
		return $this->getUsedDaysPrice();
	}
	
	public function getBaseOptProducts()
	{
		$sql = "
			SELECT
				p.id, p.name, ppr.price
			FROM products p
			INNER JOIN package_products pp ON pp.product_id = p.id
			INNER JOIN product_prices ppr ON p.id = ppr.product_id

			WHERE pp.package_id = ? AND pp.is_optional = 1 AND ppr.application_id = ?
			GROUP BY p.id
		";

		return parent::getAdapter()->query($sql, array($this->getPackageId(), Cubix_Application::getId()))->fetchAll();
	}
	
	public function getExtendProducts()
	{
		$opt_products = $this->getBaseOptProducts();
		
		$ext_products = array();
		foreach ( $opt_products as $product ) {
			if ( in_array($product->id, Model_Billing_Packages::$EXTEND_PACKAGE_PRODUCTS) ) {				
				$ext_products[] = $product;
			}
		}
		
		return $ext_products;
	}
}
