<?php

class Model_Billing_ProfileBooster extends Cubix_Model
{
	const BOOST_PROFILE_ORDER_PENDING = 1;
	const BOOST_PROFILE_ORDER_PAID = 2;
	const BOOST_PROFILE_ORDER_EXPIRED = 3;
	
	protected $_table = 'profile_boost_orders';
	public $booster_sum;
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				pbo.id, UNIX_TIMESTAMP(pbo.creation_date) AS creation_date, 
				UNIX_TIMESTAMP(pbo.activation_date) AS activation_date, 
				pbo.hour_from, pbo.hour_to, e.id AS escort_id,
				e.agency_id,
				e.status,
				e.showname,
				a.name AS agency_name,
				op.id AS order_package_id,
				op.application_id,
				IF(pbo.city_id, c.title_en, "INDEX") AS boost_city,
				t.transaction_id,
				o.system_order_id,
				o.id AS order_id
			FROM profile_boost_orders pbo
			INNER JOIN escorts e ON e.id = pbo.escort_id
			INNER JOIN order_packages op ON op.id = pbo.order_package_id
			INNER JOIN orders o ON o.id = op.order_id
			INNER JOIN users u ON u.id = e.user_id
			LEFT JOIN cities c ON c.id = pbo.city_id
			LEFT JOIN agencies a ON a.id = e.agency_id
			LEFT JOIN transfers t ON t.id = pbo.transfer_id
			WHERE pbo.status = 2
		';
		
		$countSql = "SELECT FOUND_ROWS()";

		$sql_sum = '
			SELECT SUM(a) FROM (SELECT t.amount AS a
			FROM profile_boost_orders pbo
			INNER JOIN escorts e ON e.id = pbo.escort_id
			INNER JOIN order_packages op ON op.id = pbo.order_package_id
			INNER JOIN orders o ON o.id = op.order_id
			INNER JOIN users u ON u.id = e.user_id
			LEFT JOIN cities c ON c.id = pbo.city_id
			LEFT JOIN transfers t ON t.id = pbo.transfer_id
			WHERE pbo.status = 2
		';

		$where = '';
		
		if ( strlen($filter['e.showname']) ) {
			$where .= self::quote('AND e.showname LIKE ?', $filter['e.showname'] . '%');
		}

		if ( strlen($filter['e.id']) ) {
			$where .= self::quote('AND e.id = ?', $filter['e.id']);
		}
		
		$bu_user = Zend_Auth::getInstance()->getIdentity();

		if ( $bu_user->type == 'superadmin' ) {
			if ( $filter['sales_user_id'] ) {
				$where .= self::quote(' AND o.backend_user_id = ?', $filter['sales_user_id']);
			}
		}
		else {
			if ( $bu_user->type == 'sales manager' || $bu_user->type == 'sales' || $bu_user->type == 'sales clerk') {
				$where .= self::quote(' AND o.backend_user_id = ?', $bu_user->id);
			}
			else if ($bu_user->type == 'admin') {
				if ( $filter['sales_user_id'] ) {
					$where .= self::quote(' AND o.backend_user_id = ?', $filter['sales_user_id']);
				}
			}
		}

		if ( $filter['sales_user_id_real'] ) {
			if ( $bu_user->type == 'superadmin' ) {
				$where .= self::quote(' AND u.sales_user_id = ?', $filter['sales_user_id_real']);
			}
			else if ($bu_user->type == 'admin') {
				$where .= self::quote(' AND u.application_id = ?', $bu_user->application_id);
				$where .= self::quote(' AND u.sales_user_id = ?', $filter['sales_user_id_real']);
			}
		}
		
		if ( strlen($filter['boost_city']) ) {
			if ( strtolower($filter['boost_city']) == 'index' ) {
				$where .= self::quote('AND pbo.city_id IS NULL');
			} else {
				$where .= self::quote('AND c.title_en LIKE ?', $filter['boost_city'] . '%');
			}
		}
		
		if ( strlen($filter['order_id']) ) {
			$where .= self::quote('AND o.system_order_id = ?', $filter['order_id']);
		}
		
		if ( strlen($filter['transaction_id']) ) {
			$where .= self::quote('AND t.transaction_id = ?', $filter['transaction_id']);
		}
		
		/*if ( $filter['date_from'] ) {
			$where .= self::quote('AND DATE(pbo.activation_date) >= DATE(FROM_UNIXTIME(?))', $filter['date_from']);
		}
		
		if ( $filter['date_to'] ) {
			$where .= self::quote('AND DATE(pbo.activation_date) <= DATE(FROM_UNIXTIME(?))', $filter['date_to']);
		}
		
		if ( $filter['hour_from'] ) {
			$where .= self::quote('AND pbo.hour_from >= ?', $filter['hour_from']);
		}
		
		if ( $filter['hour_to'] ) {
			$where .= self::quote('AND pbo.hour_from <= ?', $filter['hour_to']);
		}*/
		// Date
		if ( strlen($filter['filter_by']) )
		{
			if ( $filter['filter_by'] == 'activation_date' ) {
				$date_field = 'pbo.activation_date';
			}
			else if ( $filter['filter_by'] == 'creation_date' )
				$date_field = 'pbo.creation_date';


			if (strlen($filter['date_from']) && strlen($filter['date_to']))
			{
				if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) )
				{
					$where .= self::quote(" AND DATE({$date_field}) = DATE(FROM_UNIXTIME(?))", $filter['date_from']);
				}
				else
				{
					$where .= self::quote(" AND DATE({$date_field}) >= DATE(FROM_UNIXTIME(?))", $filter['date_from']);
					$where .= self::quote(" AND DATE({$date_field}) <= DATE(FROM_UNIXTIME(?))", $filter['date_to']);
				}
			}
			else if ( strlen($filter['date_from']) ) {
				$where .= self::quote(" AND DATE({$date_field}) >= DATE(FROM_UNIXTIME(?))", $filter['date_from']);
			}
			else if ( strlen($filter['date_to']) ) {
				$where .= self::quote(" AND DATE({$date_field}) <= DATE(FROM_UNIXTIME(?))", $filter['date_to']);
			}
		}
		//---//

		$sql .= $where;
		$sql_sum .= $where;
		$sql_sum .= ' GROUP BY t.id) AS aa';
//var_dump($sql_sum);die;
		$sql .= '
			GROUP BY pbo.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$result = parent::_fetchAll($sql);
		$count = intval($this->getAdapter()->fetchOne($countSql));

		if ( Cubix_Application::getId() == APP_EF ) {
			$sum = $this->getAdapter()->fetchOne($sql_sum);
			$this->booster_sum = $sum;
		}
		
		return $result;
	}
	
	public function get($id)
	{
		return parent::_fetchRow('SELECT * FROM profile_boost_orders WHERE id = ?', array($id));
	}
	
	public function add($boosts, $escort_id, $additional_data)
	{
		try {
			parent::db()->beginTransaction();
			parent::db()->query('UPDATE orders set price = price + ? WHERE id = ?', array($additional_data['price'], $additional_data['order_id']));
			parent::db()->query('UPDATE order_packages set price = price + ? WHERE id = ?', array($additional_data['price'], $additional_data['order_package_id']));
			
			foreach($boosts as $boost) {
				parent::db()->insert('profile_boost_orders', array(
					'creation_date'		=> date('Y-m-d H:i:s', time()),
					'activation_date'	=> $boost['date'],
					'hour_from'			=> $boost['hour'],
					'hour_to'			=> $boost['hour'] + 1,
					'city_id'			=> $boost['city_id'],
					'escort_id'			=> $escort_id,
					'status'			=> self::BOOST_PROFILE_ORDER_PAID,
					'order_package_id'	=> $additional_data['order_package_id'],
					'transfer_id'	=> $additional_data['transfer_id']
				));
			}
			
			parent::db()->commit();
		} catch(Exception $ex) {
			parent::db()->rollBack();
			throw $ex;
		}
		
		return;
	}
	
	public function remove($id)
	{
		parent::db()->query('DELETE FROM profile_boost_orders WHERE id = ?', array($id));
	}
	
	public function update($data)
	{
		parent::db()->update('profile_boost_orders', $data, parent::quote('id = ?', $data['id']));
	}
}
