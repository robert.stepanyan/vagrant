<?php

class Model_Billing_VideoCall extends Cubix_Model
{
	protected $_table = 'video_chat_requests';
	
	const STATUS_PENDING = 0;
	const STATUS_PAID = 1;
	const STATUS_PAID_CONFIRMED = 2;
	const STATUS_CANCEL = 3;

	public static $STATUS_LABELS = array(
		self::STATUS_PENDING => 'Pending',
		self::STATUS_CANCEL => 'Canceled',
		self::STATUS_PAID => 'Paid',
		self::STATUS_PAID_CONFIRMED => 'Confirmed',
	);
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				vchr.id, DATE(vchr.creation_date) AS creation_date, vchr.comment, vchr.duration, vchr.status, vchr.amount, vchr.username as customer,
				e.id AS escort_id, e.agency_id, e.showname, u.username
			FROM video_chat_requests vchr 
			INNER JOIN escorts e ON e.id = vchr.escort_id
			LEFT JOIN users u on u.id = vchr.user_id
			WHERE 1
		';
		
		$countSql = "SELECT FOUND_ROWS()";

		$where = '';
		
		if ( strlen($filter['showname']) ) {
			$where .= self::quote('AND e.showname LIKE ?', $filter['showname'] . '%');
		}
		if ( strlen($filter['email']) ) {
			$where .= self::quote('AND u.email LIKE ?', $filter['email'] . '%');
		}

		if ( strlen($filter['escort_id']) ) {
			$where .= self::quote('AND e.id = ?', $filter['escort_id']);
		}
				
		if ( strlen($filter['status']) ) {
			$where .= self::quote('AND vchr.status = ?', $filter['status']);
		}
		
		if ( $filter['date_from'] ) {
			$where .= self::quote('AND DATE(vchr.creation_date) >= DATE(FROM_UNIXTIME(?))', $filter['date_from']);
		}
		
		if ( $filter['date_to'] ) {
			$where .= self::quote('AND DATE(vchr.creation_date) <= DATE(FROM_UNIXTIME(?))', $filter['date_to']);
		}
		
		$sql .= $where;

		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page ;

       
		$result = parent::_fetchAll($sql);
		$count = intval($this->getAdapter()->fetchOne($countSql));

		return $result;
	}
	
	public function get($id)
	{
		return parent::_fetchRow('
			SELECT eo.*, op.activation_type, op.date_activated, op.expiration_date, op.activation_date, op.period
			FROM expo_orders eo
			INNER JOIN order_packages op ON op.id = eo.order_package_id
			WHERE eo.id = ?
		', array($id));
	}

	public function getVideoCallRequestById($id){

	    return parent::_fetchRow('
			SELECT vchr.id, UNIX_TIMESTAMP(vchr.creation_date) AS creation_date, ep.contact_phone_parsed as phone,
				e.id AS escort_id, e.agency_id, e.showname, vchr.sms_text, vchr.comment, vchr.duration, vchr.status, vchr.amount
			FROM video_chat_requests vchr
			INNER JOIN escorts e ON e.id = vchr.escort_id
			INNER JOIN escort_profiles_v2 ep ON ep.escort_id = vchr.escort_id
			WHERE vchr.id = ?
		', array($id));
    }

    public function removeVideoCall($id){
        parent::getAdapter()->delete('video_chat_requests', parent::quote('id = ?', $id));
    }

    public function getEscortsIdByRequestId($ids){
        $explod_ids = explode(',', $ids);
        $ids_array = array();
        foreach ($explod_ids as $id){
            if(is_numeric($id) && $id > 0){
                $escort =  parent::_fetchRow('SELECT escort_id FROM video_chat_requests WHERE id = ? ', array($id));
                $ids_array[] = $escort->escort_id;
            }
        }
        return $ids_array;
    }

    public function getEscortsTotalAmount($ids, $date_from = null, $date_to = null){


	    $total_amount = 0;
	    if(!empty($ids)){
	        foreach ($ids as $id){
                $sql = "SELECT SUM(amount) as total_amount FROM video_chat_requests WHERE status = 2 AND escort_id = ? ";
                $where = '';
                if ( $date_from ) {

                    $date_from_format = date("Y-m-d H:i:s", strtotime($date_from));

                    $where .= self::quote('AND DATE(creation_date) >= ?', $date_from_format);
                }

                if ($date_to ) {
                    $date_to_format = date("Y-m-d H:i:s", strtotime($date_to));
                    $where .= self::quote(' AND DATE(creation_date) <= ? ', $date_to_format);
                }

                $sql .= $where;

                $row =  self::_fetchRow($sql , array($id));
                $total_amount += $row->total_amount;

            }
        }

	    return $total_amount;



    }

    /**
     * @param null $date_from
     * @param null $date_to
     * @return int
     */
    public function getTotalAmount($filter)
    {
        try {
            $sql = 'SELECT SUM(vchr.amount) as total_amount
			FROM video_chat_requests vchr 
			INNER JOIN escorts e ON e.id = vchr.escort_id
			LEFT JOIN users u on u.id = vchr.user_id
			WHERE 1
		';
            $where = '';

            if ( strlen($filter['showname']) ) {
                $where .= self::quote('AND e.showname LIKE ?', $filter['showname'] . '%');
            }
            if ( strlen($filter['email']) ) {
                $where .= self::quote('AND u.email LIKE ?', $filter['email'] . '%');
            }

            if ( strlen($filter['escort_id']) ) {
                $where .= self::quote('AND e.id = ?', $filter['escort_id']);
            }

            if ( strlen($filter['skype']) ) {
                $where .= self::quote('AND vchr.skype LIKE ?', $filter['skype'] . '%');
            }

            if ( strlen($filter['status']) ) {
                $where .= self::quote('AND vchr.status = ?', $filter['status']);
            }

            if ( $filter['date_from'] ) {
                $where .= self::quote('AND DATE(vchr.creation_date) >= DATE(FROM_UNIXTIME(?))', $filter['date_from']);
            }

            if ( $filter['date_to'] ) {
                $where .= self::quote('AND DATE(vchr.creation_date) <= DATE(FROM_UNIXTIME(?))', $filter['date_to']);
            }

            $sql .= $where;

            $total_amount = $this->getAdapter()->fetchOne($sql, array(self::STATUS_PAID_CONFIRMED));
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die;
        }

        return floatval($total_amount);
    }

    public function updateVideoCallRequest($id, $data){

        parent::getAdapter()->update('video_chat_requests', $data, parent::quote('id = ?', $id));


    }

    public function insert($data){
        return self::db()->insert('video_chat_requests', $data);
    }
	
	public function addTransfer($tr_data)
	{
		self::getAdapter()->insert('video_chat_transfers', $tr_data);
	}		

    public function getTransfer($request_id)
    {
        $sql = 'SELECT
                    transaction_id as `tid`,
                    cc_number as `cc`,
                    CONCAT(`cc_exp_month`, \'/\', `cc_exp_year`) as `exp`,
                    vct.payer_name as `pn`,
                    vct.amount as am,
                    vcr.user_agent as `di`,
                    vcr.sms_text as sms
                FROM
                    `video_chat_transfers` vct
                    INNER JOIN video_chat_requests vcr ON vct.request_id = vcr.id 
                WHERE
                    request_id = ?';
        return  parent::_fetchRow($sql,array($request_id));
    }
	
	public function getRequest($request_id)
    {
        $sql = 'SELECT
										vcr.*,
                    u.email as member_email,
                    u.username as member_username
                FROM
                    `video_chat_requests` vcr
                  
                    INNER JOIN users u on u.id = vcr.user_id
                WHERE
                    vcr.id = ?';
        return  parent::_fetchRow($sql,array($request_id));
    }

    public function getDataForExport(array $filter = array(), $page = 1, $per_page = 10, $sort_field = 'order_date', $sort_dir = 'DESC')
    {
        if (Cubix_Application::getId() == APP_A6)
        {
            $fields = array(
                'vct.date_transfered', ' vct.payer_name', 'vct.amount', ' vct.cc_holder', 'vct.cc_number', 'vct.cc_exp_year',
                'vct.cc_exp_month', 'vct.transaction_id', 'vct.amount AS paid_amount',' r.escort_id', 'r.status','date(r.creation_date) as creationDate',
                'e.showname', 'e.agency_id', '  a.`name` as AgencyName', 'pr.email as escortEmail', 'es.escort_skype_username as escortSkype', 'es.bank_wire_beneficiary_name',
                'es.bank_wire_iban','es.bank_wire_bic', 'es.bank_wire_city', 'es.paxum_email', 'r.email as memberEmail',  ' r.skype as client_skype', 'r.name as memberName'
            );
        }
        $sql = array(
            'tables' => array(' video_chat_transfers vct'),
            'fields' => $fields,
            'joins' => array(
                'INNER JOIN video_chat_requests r ON vct.request_id = r.id',
                'INNER JOIN escorts e ON e.id = r.escort_id',
                'INNER JOIN escort_profiles_v2 pr ON pr.escort_id = e.id',
                'LEFT JOIN escort_skype es ON es.escort_id = e.id',
                'LEFT JOIN agencies a ON a.id = e.agency_id'
            ),
            'where' => array(
                ) + $filter,
            'group' => 'vct.id',
            'order' => array($sort_field, $sort_dir)
        );


        if (Cubix_Application::getId() == APP_A6)
        {
            $sql['joins'][] = 'LEFT JOIN escort_cities ec ON e.id = ec.escort_id';
            $sql['joins'][] = 'LEFT JOIN cities ci ON ec.city_id = ci.id';
        }

        $dataSql = Cubix_Model::getSql($sql);
        $data = parent::_fetchAll($dataSql);

        foreach ( $data as $i => $row ) {
            $data[$i]->status = self::$STATUS_LABELS[$row->status];
        }

        return array(
            'data' => $data
        );
    }


}
