<?php

class Model_Billing_Discounts extends Cubix_Model {
  public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count = 0)
	{
		$sql = ' SELECT id, value_from, value_to, discount FROM agency_discounts';
		
		$countSql = ' SELECT COUNT(id) FROM agency_discounts';
		
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$count = $this->getAdapter()->fetchOne($countSql);
		
		return parent::_fetchAll($sql);
  }
  
  public function getById($id)
  {
    $sql = 'SELECT id, value_from, value_to, discount FROM agency_discounts WHERE id = ?';

    return $this->getAdapter()->fetchRow(self::quote($sql, $id));
  }
  
  public function add($data = array())
  {
    $this->getAdapter()->insert('agency_discounts', $data);
  }

  public function update($data = array())
  {
    $this->getAdapter()->update('agency_discounts', $data);
  }

  public function remove($id)
  {
    $this->getAdapter()->delete('agency_discounts', self::quote('id = ?', $id));
  }
}