<?php

class Model_Billing_Transfers extends Cubix_Model
{
	protected $_table = 'transfers';
	protected $_itemClass = 'Model_Billing_TransferItem';

	const STATUS_PENDING = 1;
	const STATUS_CONFIRMED = 2;
	const STATUS_REJECTED = 3;
	const STATUS_AUTO_REJECTED = 4;
	const STATUS_CHARGEBACK = 5;

	const WEBSITE_CARDGATE = 1;
	const WEBSITE_ADSGATE = 2;
	
	public static $STATUS_LABELS = array(
		self::STATUS_PENDING => 'Pending',
		self::STATUS_CONFIRMED => 'Confirmed',
		self::STATUS_REJECTED => 'Rejected',
		self::STATUS_AUTO_REJECTED => 'Auto Rejected',
		self::STATUS_CHARGEBACK => 'Chargeback'
	);

	public function getTotalTransfersPaid()
	{
		return parent::getAdapter()->fetchOne('SELECT SUM(tc.amount) FROM transfers t INNER JOIN transfer_confirmations tc ON tc.transfer_id = t.id');
	}

	public function getTotalTransfers()
	{
		return parent::getAdapter()->fetchOne('SELECT SUM(amount) FROM transfers WHERE status <> ?', self::STATUS_REJECTED);
	}

	public function getPullPersons()
	{
		return parent::getAdapter()->fetchAll('SELECT id, CONCAT(first_name, " ", last_name) AS name FROM moneygram_pull_persons WHERE status = 1 AND application_id = ?', Cubix_Application::getId());
	}

	/**
	 *
	 * @param int $id
	 * @return Model_Billing_TransferItem
	 */
	public function get($id)
	{
		return parent::_fetchRow('SELECT * FROM transfers WHERE id = ?', $id);
	}

	public function getAllV2(array $data = array(), $page = 1, $per_page = 10, $sort_field = 'date_transfered', $sort_dir = 'DESC')
	{
		$special_data = array();
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		$gotd_transfers = false;
		$votd_transfers = false;
		
		$joins = array(		
			10 => 'INNER JOIN escorts e ON e.user_id = u.id',
			20 => 'INNER JOIN agencies a ON a.user_id = u.id',
			30 => 'INNER JOIN transfer_orders too ON too.transfer_id = t.id',
			40 => 'INNER JOIN orders o ON o.id = too.order_id',
			50 => 'INNER JOIN order_packages op ON op.order_id = o.id',
			
			60 => 'INNER JOIN transfer_orders too2 ON too2.transfer_id = t.id',
			888 => 'INNER JOIN order_packages op2 ON op2.order_id = too2.order_id',
			999 => 'INNER JOIN escorts e ON e.id = op2.escort_id',
		);
		
		$active_joins = array();
		$group_by = " GROUP BY t.id ";
		
		$where = array(
			'1',
		);
		
		if ( $data['showname'] ) {
			$where[] = 'e.showname LIKE "' . $data['showname'] . '%"';
			
			//$active_joins[10] = $joins[10];
		}
		
		if ( $data['escort_id'] ) {
			$where[] = 'e.id = ' . $data['escort_id'];
			
			//$active_joins[10] = $joins[10];
		}
		
		if ( $data['agency_name'] ) {
			$where[] = 'a.name LIKE "' . $data['agency_name'] . '%"';
			
			$active_joins[20] = $joins[20];
		}
		
		if ( strlen($data['user_id']) > 0 ) {
			$where[] = 'u.id = '. $data['user_id'];
		}
		
		if ( $data['email'] ) {
			$where[] = 'u.email LIKE "' . $data['email'] . '%"';
		}
                
		if (Cubix_Application::getId() == APP_EF){
			if ($data['escort_type']){
				if ($data['escort_type'] == 'agency'){
					$where[] = 'e.agency_id IS NOT NULL';
					$active_joins[10] = $joins[10];
				} elseif ($data['escort_type'] == 'independent'){
					$where[] = 'e.agency_id IS NULL';
					$active_joins[10] = $joins[10];
				}
			}
		}
		
		$data['filter_by'] = 'date_transfered';
		if ( $data['filter_by'] ) {
			if ( in_array($data['filter_by'], array('date_transfered')) ) {
				if ( $data['date_from'] ) {
					$where[] = 'DATE(' . $data['filter_by'] . ') >= ' . new Zend_Db_Expr('DATE(FROM_UNIXTIME(' . $data['date_from'] . '))'); 
				}

				if ( $data['date_to'] ) {
					$where[] = 'DATE(' . $data['filter_by'] . ') <= ' . new Zend_Db_Expr('DATE(FROM_UNIXTIME(' . $data['date_to'] . '))');
				}
			}
		}
		
		if ( $data['sales_user_id_real'] ) {
			if ( $bu_user->type == 'superadmin' ) {
				$where[] = 'u.sales_user_id = '. $data['sales_user_id_real'];
			}
			
			else if ($bu_user->type == 'admin') {
				$where[] = 'u.sales_user_id = '. $data['sales_user_id_real'];
			}
		}
		
		if ( $bu_user->type == 'superadmin' ) {
			if ( $data['sales_user_id'] ) {
				$where[] = 't.backend_user_id = ' . $data['sales_user_id'];
			}
		}
		else {
			if ( $bu_user->type == 'sales manager' || $bu_user->type == 'sales' || $bu_user->type == 'sales clerk') {
				$where[] = 't.backend_user_id = ' . $bu_user->id;
			}
			else if ($bu_user->type == 'admin') {
				//$where[] = 't.application_id = ' . $bu_user->application_id;
				if ( $data['sales_user_id'] ) {
					$where[] = 't.backend_user_id = ' . $data['sales_user_id'];
				}
			}
		}
		
		if ( $data['status'] ) {
			$where[] = 't.status = ' . $data['status'];
		}
		
		if ( $data['sys_order_id'] ) {
			$where[] = 'o.system_order_id = "' . $data['sys_order_id'] . '"';
			$active_joins[30] = $joins[30];
			$active_joins[40] = $joins[40];
		}
		
		if ( $data['contains_disc_orders'] ) {
			$where[] = '(op.discount <> 0 OR op.discount_fixed <> 0)';
			$active_joins[30] = $joins[30];
			$active_joins[40] = $joins[40];
			$active_joins[50] = $joins[50];
		}
		
		if ( $data['has_comment'] ) {
			$where[] = 't.comment IS NOT NULL AND t.comment <> \'\'';
		}

        if (in_array(Cubix_Application::getId(), array(APP_EG_CO_UK))) {
            if ($data['banner_price']) {
                $where[] = 't.banner_price IS NOT NULL';
            }
        }

        if ( $data['is_slf_chckt'] ) {
            $where[] = 't.is_self_checkout = 1';
        }
		
		if (Cubix_Application::getId() == APP_6A)
		{
			if ($data['cc_info'])
			{
				if ($data['cc_info'] == 1)
					$where[] = 't.cc_number IS NOT NULL';
				elseif ($data['cc_info'] == 2)
					$where[] = 't.cc_number IS NULL';
			}
		}
		
		/* PAYMENT INFO FILTER */
		if ( $data['transfer_type_id'] ) {
			$where[] = 't.transfer_type_id = ' . $data['transfer_type_id'];
		}
				
		if ( $data['transaction_id'] ) {
			$where[] = 't.transaction_id = "' . $data['transaction_id'] . '"';
			$special_data['transaction_id'] = $data['transaction_id'];
		}
		
		if ( $data['is_self_checkout'] ) {
			$where[] = 't.is_self_checkout = ' . $data['is_self_checkout'] ;
		}
		
		if ( isset($data['phone_status']) && $data['phone_status'] != "") {
			$where[] = 't.phone_billing_paid = ' . $data['phone_status'] ;
		}
		
		if ( $data['var_symbol'] ) {
			$where[] = 't.var_symbol = ' . $data['var_symbol'];
		}
		
		if ( $data['mtcn'] ) {
			$where[] = 't.mtcn = ' . $data['mtcn'];
		}
		
		if ( $data['cc_number'] ) {
			$where[] = 't.cc_number LIKE "%' . $data['cc_number'] . '"';
			
			$special_data['special_cc_number'] = $data['cc_number'];
		}
		
		if ( $data['cc_email'] ) {
			$where[] = 't.cc_email LIKE "%' . $data['cc_email'] . '%"';
			
			$special_data['special_cc_email'] = $data['cc_email'];
		}
		
		if ( $data['first_name'] ) {
			$where[] = 't.first_name LIKE "%' . $data['first_name'] . '%"';
			
			$special_data['special_first_name'] = $data['first_name'];
		}
		
		if ( $data['last_name'] ) {
			$where[] = 't.last_name LIKE "%' . $data['last_name'] . '%"';
			
			$special_data['special_last_name'] = $data['last_name'];
		}
		
		if ( $data['pull_person_id'] ) {
			$where[] = 't.pull_person_id = ' . $data['pull_person_id'];
		}

        if (in_array(Cubix_Application::getId(),array(APP_EF,APP_A6))){
            if ( $data['is_mobile'] ) {
                $where[] = 't.is_mobile = ' . $data['is_mobile'];
            }
        }

		/* PAYMENT INFO FILTER */
		
		if ( $data['showname'] || $data['escort_id'] ) {
			
			if(Cubix_Application::getId() == APP_A6 && count(array_intersect_key($active_joins, array(10 => 1, 20 => 1, 30 => 1, 40 => 1,50 => 1))) == 0){
				$gotd_transfers = true;
				$votd_transfers = true;
			}
			//unset($active_joins[10]);
				$active_joins[60] = $joins[60];
				$active_joins[62] = $joins[888];
				$active_joins[63] = $joins[999];
			ksort($active_joins);
		}
		$aditionalFields = '';
		if (Cubix_Application::getId() == APP_EF){
		    $aditionalFields = ', ip, client_cookie_id';
		    if (isset($data['show_dupl_coockie']) && $data['show_dupl_coockie'] == 1){
                $DuplIdsByCookieSql = "
		        SELECT
                    GROUP_CONCAT(DISTINCT id),
                    GROUP_CONCAT(DISTINCT user_id) AS `gc`
                FROM
                    transfers
                WHERE client_cookie_id IS NOT NULL
                GROUP BY
                    client_cookie_id
                HAVING
                    COUNT(client_cookie_id) > 1 AND INSTR(gc, ',') > 0
		        ";
                $DuplIdsByCookie = $this->getAdapter()->fetchCol($DuplIdsByCookieSql);
                $where[] = 't.id IN('.implode(',', $DuplIdsByCookie).')';
                $where[] = 't.client_cookie_id is not null';
            }
        }
		$base_sql = "
			SELECT
				SQL_CALC_FOUND_ROWS
				
				u.id AS user_id, u.user_type, u.username, t.banner_price,
				t.id, t.status, t.comment_grid, t.backend_user_id, t.first_name, t.last_name,
				UNIX_TIMESTAMP(t.date_transfered) AS date_transfered, t.amount, t.transfer_type_id,
				tc.amount AS paid_amount, UNIX_TIMESTAMP(tc.date) AS payment_date, t.is_self_checkout $aditionalFields
			FROM transfers t
			INNER JOIN users u ON u.id = t.user_id
			LEFT JOIN transfer_confirmations tc ON tc.transfer_id = t.id
			" . implode(' ', $active_joins) . "
			WHERE " . implode(' AND ', $where) . "
			" . $group_by . "
			ORDER BY " . $sort_field . " " . $sort_dir . "			
			LIMIT " . ($page - 1) * $per_page . ", " . $per_page
		;

		//echo $base_sql;die; 

		$data = parent::_fetchAll($base_sql);
		$count = $this->getAdapter()->fetchOne("SELECT FOUND_ROWS()");
		$uniq_users = 0;
		// UNIQ USERS COUNT -----------------------------
		
		if (Cubix_Application::getId() == APP_EF){
			$uniq_users_sql = "
				SELECT COUNT(DISTINCT u.id)
				FROM transfers t 
				INNER JOIN users u ON u.id = t.user_id 
				LEFT JOIN transfer_confirmations tc ON tc.transfer_id = t.id 
				" . implode(' ', $active_joins) . " 
				WHERE " . implode(' AND ', $where) . " 
				"
			;

			$uniq_users = $this->getAdapter()->fetchOne($uniq_users_sql);
		}
		
		// ---------------------------------------------
		
		/* INCLUDE GOTD TRANSFERS FOR AND6 */
		
		if ($gotd_transfers) {
			$gotd_sql = "
				SELECT
				SQL_CALC_FOUND_ROWS
				u.id AS user_id, u.user_type, u.username,
				t.id, t.status, t.comment_grid, t.backend_user_id, t.first_name, t.last_name,
				UNIX_TIMESTAMP(t.date_transfered) AS date_transfered, t.amount, t.transfer_type_id,
				tc.amount AS paid_amount, UNIX_TIMESTAMP(tc.date) AS payment_date, t.is_self_checkout $aditionalFields
			FROM transfers t
			INNER JOIN users u ON u.id = t.user_id
			LEFT JOIN transfer_confirmations tc ON tc.transfer_id = t.id
			INNER JOIN gotd_orders go ON go.transfer_id = t.id
			INNER JOIN escorts e ON e.id = go.escort_id
			WHERE " . implode(' AND ', $where) . "
			";
		
		
			$gotd_data = parent::_fetchAll($gotd_sql);
			$data = array_merge($data, $gotd_data);
			//$data = $gotd_data;
		}
		
		/* INCLUDE VOTD TRANSFERS FOR AND6 */
		
		if ($votd_transfers) {
			$votd_sql = "
				SELECT
				SQL_CALC_FOUND_ROWS
				u.id AS user_id, u.user_type, u.username,
				t.id, t.status, t.comment_grid, t.backend_user_id, t.first_name, t.last_name,
				UNIX_TIMESTAMP(t.date_transfered) AS date_transfered, t.amount, t.transfer_type_id,
				tc.amount AS paid_amount, UNIX_TIMESTAMP(tc.date) AS payment_date, t.is_self_checkout $aditionalFields
			FROM transfers t
			INNER JOIN users u ON u.id = t.user_id
			LEFT JOIN transfer_confirmations tc ON tc.transfer_id = t.id
			INNER JOIN votd_orders vo ON vo.transfer_id = t.id
			INNER JOIN escorts e ON e.id = vo.escort_id
			WHERE " . implode(' AND ', $where) . "
			";
		
		
			$votd_data = parent::_fetchAll($votd_sql);
			$data = array_merge($data, $votd_data);
			//$data = $gotd_data;
		}
		//var_dump($gotd_data);die;
		
		if ( count($data) ) {
			foreach ( $data as $i => $transfer ) {
				$data[$i]->status = self::$STATUS_LABELS[$transfer->status];
				
				
				if ( $transfer->user_type == 'escort' ) {
					$escort_data = $this->getAdapter()->fetchRow("SELECT e.id, e.showname FROM escorts e WHERE e.user_id = ?", array($transfer->user_id));
					$data[$i]->showname = $escort_data->showname; 
					$data[$i]->escort_id = $escort_data->id; 
				} else if ( $transfer->user_type == 'agency' ) {
					$agency_data = $this->getAdapter()->fetchRow("SELECT a.id, a.name FROM agencies a WHERE a.user_id = ?", array($transfer->user_id));
					$data[$i]->agency_name = $agency_data->name;
					$data[$i]->agency_id = $agency_data->id;
				}
				
				$data[$i]->sales_username = $this->getAdapter()->fetchOne("SELECT bu.username FROM backend_users bu WHERE bu.id = ?", array($transfer->backend_user_id));
				
				$data[$i]->transfer_type = $this->getAdapter()->fetchOne("SELECT name FROM transfer_types WHERE id = ?", array($transfer->transfer_type_id));
				if (Cubix_Application::getId()==APP_EF){
				    $data[$i]->duplicity_cookie=$this->getDuplicityByCookie($transfer->user_id, $transfer->client_cookie_id);
				    $data[$i]->duplicity_ip=$this->getDuplicityByIp($transfer->user_id, $transfer->ip);
                }
				
				if (Cubix_Application::getId() == APP_ED){
					$cities_str = '';
					$premium_cities_sql = 'SELECT c.title_en as title FROM transfer_orders tro 
						INNER JOIN order_packages op ON op.order_id = tro.order_id
						INNER JOIN premium_escorts pe ON pe.order_package_id = op.id
						INNER JOIN cities c ON c.id = pe.city_id
						WHERE tro.transfer_id = ?
						GROUP BY c.id';
					$premium_cities = $this->getAdapter()->fetchAssoc($premium_cities_sql, array($transfer->id));
					$data[$i]->premium_cities = implode(' , ',array_keys($premium_cities));
				}
			}
		}

		/* INCLUDE SPECIAL TRANSFERS */
		if ( count($special_data) ) {
			$sp_sql = '
				SELECT 
					t.id, 2 AS status, CONCAT(t.first_name, " ", t.last_name) AS showname, "" AS username,
					"Credit Card" AS transfer_type, null AS comment_grid, UNIX_TIMESTAMP(t.date_transfered) AS date_transfered, 
					t.amount, bu.username AS sales_username, "escort" AS user_type, t.first_name, 
					t.last_name, UNIX_TIMESTAMP(t.date_transfered) AS payment_date, t.amount AS paid_amount, 
					0 AS is_self_checkout, 1 as is_special, is_charged_back
				FROM special_transfers t
				INNER JOIN backend_users bu ON bu.id = t.backend_user_id
				WHERE 1
			';
			
			if ( isset($special_data['transaction_id']) && $special_data['transaction_id'] ) {
				$sp_sql .= ' AND t.transaction_id="' . $special_data['transaction_id'] . '"';
			}
			
			if ( isset($special_data['special_first_name']) && $special_data['special_first_name'] ) {
				$sp_sql .= " AND t.first_name='" . $special_data['special_first_name'] . "'";
			}
			
			if ( isset($special_data['special_last_name']) && $special_data['special_last_name'] ) {
				$sp_sql .= " AND t.last_name='" . $special_data['special_last_name'] . "'";
			}
			
			if ( isset($special_data['special_cc_number']) && $special_data['special_cc_number'] ) {
				$sp_sql .= " AND t.cc_number LIKE '%" . $special_data['special_cc_number'] . "'";
			}
			
			if ( isset($special_data['special_cc_email']) && $special_data['special_cc_email'] ) {
				$sp_sql .= " AND t.email LIKE '%" . $special_data['special_cc_email'] . "%'";
			}

			$sp_data = parent::_fetchAll($sp_sql);
			
			foreach ( $sp_data as $i => $row ) {
				$sp_data[$i]->status = self::$STATUS_LABELS[$row->status];
			}
			
			$data = array_merge($data, $sp_data);
		}
		/* INCLUDE SPECIAL TRANSFERS */

		return array(
			'data' => $data,
			'count' => $count,
			'total' => 0,
			'paid' => 0,
			'dif_members_count' => $uniq_users
		);
	}
	
	public function getAll(array $filter = array(), $page = 1, $per_page = 10, $sort_field = 'date_transfered', $sort_dir = 'DESC', $special_data = array())
	{
		$joins = array();
		$show_special = false;
		$sp_tr_id = false;
		if ( isset($filter['show_special']) && $filter['show_special'] ) {
			$show_special = true;
			$sp_tr_id = $filter['show_special'];
			unset($filter['show_special']);
		}
		
		if ( isset($filter['contains_disc_orders']) && $filter['contains_disc_orders'] ) {
			$joins['joins'] = 'INNER JOIN order_packages op ON op.order_id = o.id';
			$filter['(op.discount <> 0 OR op.discount_fixed <> 0)'] = true;
			
			unset($filter['contains_disc_orders']);
		}
		
		$sql = array(
			'tables' => array('transfers t'),
			'fields' => array(
				't.id', 't.status', 'u.username', 't.user_id', 'tt.name AS transfer_type', 't.comment_grid',
				'UNIX_TIMESTAMP(date_transfered) AS date_transfered', 't.amount', 'bu.username AS sales_username', 'bu2.username AS sales_username_real',
				'u.user_type', 't.first_name', 't.last_name', 'UNIX_TIMESTAMP(tc.date) AS payment_date', 'tc.amount AS paid_amount', 'a.name AS agency_name',
				'e.showname', 'a.id AS agency_id', 'e.id AS escort_id', 't.is_self_checkout'
			),
			'joins' => array(
				'INNER JOIN users u ON u.id = t.user_id',
				'INNER JOIN backend_users bu ON bu.id = t.backend_user_id',
				'INNER JOIN backend_users bu2 ON bu2.id = u.sales_user_id',
				'INNER JOIN transfer_types tt ON tt.id = t.transfer_type_id',
				'LEFT JOIN transfer_confirmations tc ON tc.transfer_id = t.id',
				'LEFT JOIN escorts e ON e.user_id = t.user_id',
				'LEFT JOIN agencies a ON a.user_id = t.user_id',
				'LEFT JOIN transfer_orders too ON too.transfer_id = t.id',
				'LEFT JOIN orders o ON o.id = too.order_id'
			) + $joins,
			'where' => array(
                
			) + $filter,
			'group' => 't.id',
			'order' => array($sort_field, $sort_dir),
			'page' => array($page, $per_page)
		);

		if ( Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT ) {
			$sql['fields'][] = 't.phone_billing_paid';
		}	
		
		$dataSql = Cubix_Model::getSql($sql);
		//die($dataSql);
		
		$sql['order'] = false;
		$sql['page'] = false;
		$sql['group'] = false;	

//        $sql['fields'] = array('COUNT(DISTINCT(t.id))');
//		$countSql = Cubix_Model::getSql($sql);


        $sql['group'] =  't.id';
    
		$sql['fields'] = array('t.amount','tc.amount AS paid_amount,t.id');
		$aggrSql = Cubix_Model::getSql($sql);
		
        $sqlTotal = array(
			'tables' => array('('.$aggrSql.') x'),
			'fields' => array(
				'SUM(x.amount) as total',
                'SUM(x.paid_amount) AS paid_total',
                'COUNT(x.id) AS count'
			),
			'joins' => array(
                
			),
			'where' => array(

			));
        $sqlTotals = Cubix_Model::getSql($sqlTotal);
		
        $totals = $this->getAdapter()->fetchRow($sqlTotals);
      
        $total = @$totals->total;
        $paid = @$totals->paid_total;
        $count = @$totals->count;

//		$count = $this->getAdapter()->fetchOne($countSql);

		$data = parent::_fetchAll($dataSql);
     
		if ($show_special || count($special_data)) {
			$sp_sql = '
				SELECT 
					t.id, 2 AS status, CONCAT(t.first_name, " ", t.last_name) AS showname, "" AS username,
					"Credit Card" AS transfer_type, null AS comment_grid, UNIX_TIMESTAMP(t.date_transfered) AS date_transfered, 
					t.amount, bu.username AS sales_username, "escort" AS user_type, t.first_name, 
					t.last_name, UNIX_TIMESTAMP(t.date_transfered) AS payment_date, t.amount AS paid_amount, 
					0 AS is_self_checkout, 1 as is_special, is_charged_back
				FROM special_transfers t
				INNER JOIN backend_users bu ON bu.id = t.backend_user_id
				WHERE 1
			';
			
			if ( $show_special ) {
				$sp_sql .= " AND t.transaction_id =" . $sp_tr_id;
			}
			
			if ( isset($special_data['special_first_name']) && $special_data['special_first_name'] ) {
				$sp_sql .= " AND t.first_name='" . $special_data['special_first_name'] . "'";
			}
			
			if ( isset($special_data['special_last_name']) && $special_data['special_last_name'] ) {
				$sp_sql .= " AND t.last_name='" . $special_data['special_last_name'] . "'";
			}
			
			if ( isset($special_data['special_cc_number']) && $special_data['special_cc_number'] ) {
				$sp_sql .= " AND t.cc_number LIKE '%" . $special_data['special_cc_number'] . "'";
			}
			
			if ( isset($special_data['special_cc_email']) && $special_data['special_cc_email'] ) {
				$sp_sql .= " AND t.email LIKE '%" . $special_data['special_cc_email'] . "%'";
			}
			
			$sp_data = parent::_fetchAll($sp_sql);
			
			$data = array_merge($data, $sp_data);
		}

		foreach ( $data as $i => $row ) {
			$data[$i]->status = self::$STATUS_LABELS[$row->status];
		}

		return array(
			'data' => $data,
			'count' => $count,
			'total' => number_format($total, 2) . ' ' . Cubix_Application::getById()->currency_symbol,
			'paid' => number_format($paid, 2) . ' ' . Cubix_Application::getById()->currency_symbol
		);
	}
	
	public function getAllCount(array $filter = array(), $page = 1, $per_page = 10, $sort_field = 'date_transfered', $sort_dir = 'DESC', $special_data = array())
	{
		$joins = array();
		$show_special = false;
		$sp_tr_id = false;
		if ( isset($filter['show_special']) && $filter['show_special'] ) {
			$show_special = true;
			$sp_tr_id = $filter['show_special'];
			unset($filter['show_special']);
		}
		
		if ( isset($filter['contains_disc_orders']) && $filter['contains_disc_orders'] ) {
			$joins['joins'] = 'INNER JOIN order_packages op ON op.order_id = o.id';
			$filter['(op.discount <> 0 OR op.discount_fixed <> 0)'] = true;
			
			unset($filter['contains_disc_orders']);
		}
		
		$sql = array(
			'tables' => array('transfers t'),
			'fields' => array(
				't.id', 't.status', 'u.username', 't.user_id', 'tt.name AS transfer_type', 't.comment_grid',
				'UNIX_TIMESTAMP(date_transfered) AS date_transfered', 't.amount', 'bu.username AS sales_username', 'bu2.username AS sales_username_real',
				'u.user_type', 't.first_name', 't.last_name', 'UNIX_TIMESTAMP(tc.date) AS payment_date', 'tc.amount AS paid_amount', 'a.name AS agency_name',
				'e.showname', 'a.id AS agency_id', 'e.id AS escort_id', 't.is_self_checkout'
			),
			'joins' => array(
				'INNER JOIN users u ON u.id = t.user_id',
				'INNER JOIN backend_users bu ON bu.id = t.backend_user_id',
				'INNER JOIN backend_users bu2 ON bu2.id = u.sales_user_id',
				'INNER JOIN transfer_types tt ON tt.id = t.transfer_type_id',
				'LEFT JOIN transfer_confirmations tc ON tc.transfer_id = t.id',
				/*'LEFT JOIN escorts e ON e.user_id = t.user_id AND u.user_type = "escort"',*/
				'LEFT JOIN escorts e ON e.user_id = t.user_id',
				/*'LEFT JOIN agencies a ON a.user_id = t.user_id AND u.user_type = "agency"',*/
				'LEFT JOIN agencies a ON a.user_id = t.user_id',
				'LEFT JOIN transfer_orders too ON too.transfer_id = t.id',
				'LEFT JOIN orders o ON o.id = too.order_id'
			) + $joins,
			'where' => array(
                
			) + $filter,
			'group' => 't.id',
			'order' => array($sort_field, $sort_dir),
			'page' => array($page, $per_page)
		);

		if ( Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT ) {
			$sql['fields'][] = 't.phone_billing_paid';
		}	
		
		$dataSql = Cubix_Model::getSql($sql);
		//die($dataSql);
		
		$sql['order'] = false;
		$sql['page'] = false;
		$sql['group'] = false;	

//        $sql['fields'] = array('COUNT(DISTINCT(t.id))');
//		$countSql = Cubix_Model::getSql($sql);


        $sql['group'] =  't.id';
    
		$sql['fields'] = array('t.amount','tc.amount AS paid_amount,t.id');
		$aggrSql = Cubix_Model::getSql($sql);
		
        $sqlTotal = array(
			'tables' => array('('.$aggrSql.') x'),
			'fields' => array(
				'SUM(x.amount) as total',
                'SUM(x.paid_amount) AS paid_total',
                'COUNT(x.id) AS count'
			),
			'joins' => array(
                
			),
			'where' => array(

			));
        $sqlTotals = Cubix_Model::getSql($sqlTotal);
		//echo $sqlTotals;die;
        $totals = $this->getAdapter()->fetchRow($sqlTotals);
      
        $total = @$totals->total;
        $paid = @$totals->paid_total;
        $count = @$totals->count;

//		$count = $this->getAdapter()->fetchOne($countSql);

		$data = parent::_fetchAll($dataSql);
     
		if ($show_special || count($special_data)) {
			$sp_sql = '
				SELECT 
					t.id, 2 AS status, CONCAT(t.first_name, " ", t.last_name) AS showname, "" AS username,
					"Credit Card" AS transfer_type, null AS comment_grid, UNIX_TIMESTAMP(t.date_transfered) AS date_transfered, 
					t.amount, bu.username AS sales_username, "escort" AS user_type, t.first_name, 
					t.last_name, UNIX_TIMESTAMP(t.date_transfered) AS payment_date, t.amount AS paid_amount, 
					0 AS is_self_checkout, 1 as is_special, is_charged_back
				FROM special_transfers t
				INNER JOIN backend_users bu ON bu.id = t.backend_user_id
				WHERE 1
			';
			
			if ( $show_special ) {
				$sp_sql .= " AND t.transaction_id =" . $sp_tr_id;
			}
			
			if ( isset($special_data['special_first_name']) && $special_data['special_first_name'] ) {
				$sp_sql .= " AND t.first_name='" . $special_data['special_first_name'] . "'";
			}
			
			if ( isset($special_data['special_last_name']) && $special_data['special_last_name'] ) {
				$sp_sql .= " AND t.last_name='" . $special_data['special_last_name'] . "'";
			}
			
			if ( isset($special_data['special_cc_number']) && $special_data['special_cc_number'] ) {
				$sp_sql .= " AND t.cc_number LIKE '%" . $special_data['special_cc_number'] . "'";
			}
			
			if ( isset($special_data['special_cc_email']) && $special_data['special_cc_email'] ) {
				$sp_sql .= " AND t.email LIKE '%" . $special_data['special_cc_email'] . "%'";
			}
			
			$sp_data = parent::_fetchAll($sp_sql);
			
			$data = array_merge($data, $sp_data);
		}
//echo $dataSql;die;
		foreach ( $data as $i => $row ) {
			$data[$i]->status = self::$STATUS_LABELS[$row->status];
		}

		return array(
			'data' => $data,
			'count' => $count,
			'total' => number_format($total, 2) . ' ' . Cubix_Application::getById()->currency_symbol,
			'paid' => number_format($paid, 2) . ' ' . Cubix_Application::getById()->currency_symbol
		);
	}

	
	public function getAllCountV2(array $filter = array(), $type)
	{
//	    var_dump($filter);
//        var_dump($filter['DATE(date_transfered) >= ?']);
//        var_dump($filter['DATE(date_transfered) <= ?']);
//        die;
		$joins = array();
				
		if ( isset($filter['show_special']) && $filter['show_special'] ) {
			unset($filter['show_special']);
		}
		
		if ( isset($filter['contains_disc_orders']) && $filter['contains_disc_orders'] ) {
			$joins[11] = 'INNER JOIN order_packages op ON op.order_id = o.id';
			$filter['(op.discount <> 0 OR op.discount_fixed <> 0)'] = true;
			unset($filter['contains_disc_orders']);
		}
				
		if( isset($filter['o.system_order_id = ?']) ){
			$joins[12] = 'INNER JOIN transfer_orders too ON too.transfer_id = t.id';
			$joins[13] = 'INNER JOIN orders o ON o.id = too.order_id';	
		}
		
		if ( isset($filter['a.name LIKE ?']) ) {
			$joins[14] = 'INNER JOIN agencies a ON a.user_id = t.user_id AND u.user_type = \'agency\'';
		}
		
		if ( isset($filter['e.showname LIKE ?']) || isset($filter['e.id = ?']) || isset($filter['e.agency_id IS NULL']) || isset($filter['e.agency_id IS NOT NULL']) ) {
			$joins[15] = 'INNER JOIN escorts e ON e.user_id = t.user_id';
		}

        if( $filter['include_special_transfers'] ){
		    $is_special = $filter['include_special_transfers'];
		    unset( $filter['include_special_transfers'] );
        }
		
		$sql = array(
			'tables' => array('transfers t'),
			'fields' => array('t.amount','tc.amount AS paid_amount'),
			'joins' => array(
				'INNER JOIN users u ON u.id = t.user_id',
				/*'INNER JOIN backend_users bu ON bu.id = t.backend_user_id',
				'INNER JOIN backend_users bu2 ON bu2.id = u.sales_user_id',
				'INNER JOIN transfer_types tt ON tt.id = t.transfer_type_id',*/
				'LEFT JOIN transfer_confirmations tc ON tc.transfer_id = t.id',
			) + $joins,
			'where' => array(
                
			) + $filter,

		);
		
		$aggrSql = Cubix_Model::getSql($sql);
		
        $sqlTotal = array(
			'tables' => array('('.$aggrSql.') x'),
			'fields' => array(
				'SUM(x.amount) as total',
                'SUM(x.paid_amount) AS paid_total'
            ),
			'joins' => array(
                
			),
			'where' => array(

			));
        $sqlTotals = Cubix_Model::getSql($sqlTotal);
		//echo $sqlTotals;
        $totals = $this->getAdapter()->fetchRow($sqlTotals);
      
        $total = @$totals->total;

        $paid = @$totals->paid_total;


        if((isset($is_special)) && (Cubix_Application::getId() == APP_ED || Cubix_Application::getId() == APP_EG_CO_UK)){
            $filter_special = array();

            if($filter['DATE(date_transfered) >= ?']) {
                $filter_special['DATE(date_transfered) >= ?'] = $filter['DATE(date_transfered) >= ?'];
            }

            if( $filter['DATE(date_transfered) <= ?']) {
                $filter_special['DATE(date_transfered) <= ?'] = $filter['DATE(date_transfered) <= ?'];
            }
             $sql = array(
                'tables' => array('special_transfers'),
                'fields' => array(
                    'SUM(amount) as total'
                ),

                'where' => array(

                ) + $filter_special,
             );
            $sql_special = Cubix_Model::getSql($sql);

            //echo $sqlTotals;
            $special_amount = $this->getAdapter()->fetchOne($sql_special);
            $total = (float)$total + (float)$special_amount;

        }





		return array(
			'total' => number_format($total, 2) . ' ' . Cubix_Application::getById()->currency_symbol,
			'paid' => number_format($paid, 2) . ' ' . Cubix_Application::getById()->currency_symbol
		);
	}
	
	public function getDetails($id)
	{
		$fields = '';
		$ip = ' t.cc_ip as ip,';
		$and = '';
		$APP_ID = Cubix_Application::getId();
		if($APP_ID == APP_ED){
			// this is neccary when select agencie's escort
			$tables = '
						INNER JOIN transfer_orders tror ON tror.transfer_id = t.id
						INNER JOIN order_packages op ON op.order_id = tror.order_id';
			$and = 'AND e.id = op.escort_id';
		}
		if ($APP_ID == APP_EF){
		    $fields = 't.is_self_checkout, ';
            $ip = ' IF(t.cc_ip IS NULL OR t.cc_ip = "", t.ip, "") as `ip`,';
        }

		$transfer = parent::_fetchRow('
			SELECT
				t.banner_price, t.id, t.status, UNIX_TIMESTAMP(t.date_transfered) AS date_transfered,
				t.amount, bu.username AS sales_username, t.backend_user_id, t.transfer_type_id, t.cc_holder, t.cc_number, t.cc_exp_day, t.cc_exp_month, '.$ip.' t.cc_address, 
				t.cc_email,	tt.name AS transfer_type,u.id AS user_id, u.username, u.user_type, t.status,
				tc.amount AS paid_amount, UNIX_TIMESTAMP(tc.date) AS payment_date, UNIX_TIMESTAMP(tr.date) AS reject_date,
				c.' . Cubix_I18n::getTblField('title') . ' AS country_title,
				tr.comment AS reject_comment, tc.comment AS confirm_comment,
				t.first_name, t.last_name, t.country_id, t.city, t.var_symbol, t.transaction_id, t.pull_person_id, t.mtcn, t.comment, t.website_id,
				'.$fields.'
				e.showname, e.id AS escort_id, a.name AS agency_name, wat.account_type AS webmoney_account, CONCAT(mpp.first_name, " ", mpp.last_name) AS pull_person_name
			FROM transfers t
			INNER JOIN backend_users bu ON bu.id = t.backend_user_id
			INNER JOIN users u ON u.id = t.user_id
			INNER JOIN transfer_types tt ON tt.id = t.transfer_type_id 
			'.$tables.' 
			LEFT JOIN moneygram_pull_persons mpp ON mpp.id = t.pull_person_id
			LEFT JOIN countries c ON c.id = t.country_id
			LEFT JOIN transfer_confirmations tc ON tc.transfer_id = t.id
			LEFT JOIN transfer_rejections tr ON tr.transfer_id = t.id
			LEFT JOIN escorts e ON e.user_id = t.user_id '.$and.'
			LEFT JOIN agencies a ON a.user_id = t.user_id
			LEFT JOIN webmoney_account_types wat ON wat.id = t.webmoney_account
			WHERE t.id = ?
		', array($id));

		$orders = array();
		
		$model = new Model_Billing_Orders();
		
		foreach ( self::getAdapter()->fetchAll('SELECT ot.order_id FROM transfer_orders ot WHERE ot.transfer_id = ?', $transfer->id) as $order ) {
			$orders[] = $model->getDetailsSimple($order->order_id);
		}
		$active_package_orders = $orders;
		
		//Getting gotds paid with this transfer
		$gotds = self::getAdapter()->fetchAll('
			SELECT UNIX_TIMESTAMP(go.activation_date) AS activation_date, go.status, order_package_id,
				c.' . Cubix_I18n::getTblField('title') . ' AS city_title
			FROM gotd_orders go
			INNER JOIN transfers t ON t.id = go.transfer_id
			INNER JOIN cities c ON c.id = go.city_id
			WHERE go.transfer_id = ?
		', array($id));
		
		if(Cubix_Application::getId() == APP_A6){
			//Getting votds paid with this transfer
			$votds = self::getAdapter()->fetchAll('
				SELECT UNIX_TIMESTAMP(vo.activation_date) AS activation_date, vo.status, order_package_id,
					c.' . Cubix_I18n::getTblField('title') . ' AS city_title
				FROM votd_orders vo
				INNER JOIN transfers t ON t.id = vo.transfer_id
				INNER JOIN cities c ON c.id = vo.city_id
				WHERE vo.transfer_id = ?
			', array($id));
		}
		
		if(Cubix_Application::getId() == APP_A6 && count($active_package_orders) == 0 && isset($gotds)){
			$first_gotd = reset($gotds);
			$order_id = self::getAdapter()->fetchOne('SELECT order_id FROM order_packages WHERE id = ?', $first_gotd->order_package_id);
			if($order_id){
				$orders[] = $model->getDetailsSimple($order_id);
			}
		}

		if(Cubix_Application::getId() == APP_A6 && count($active_package_orders) == 0 && isset($votds)){
			$first_votd = reset($votds);
			$order_id = self::getAdapter()->fetchOne('SELECT order_id FROM order_packages WHERE id = ?', $first_votd->order_package_id);
			if($order_id){
				$orders[] = $model->getDetailsSimple($order_id);
			}
		}
		
		if(Cubix_Application::getId() == APP_A6){
			$transfer->votds = $votds;
		}


		if(Cubix_Application::getId() == APP_EF){
			//Getting votds paid with this transfer
			$bump = self::getAdapter()->fetchAll('
									SELECT
						UNIX_TIMESTAMP(pbump.activation_date) AS activation_date,
						pbump. STATUS,
						pbump.amount as paid_amount,
						order_package_id,
						c.' . Cubix_I18n::getTblField('title') . ' AS city_title
					FROM
						profile_bumps_orders pbump
					INNER JOIN transfers t ON t.id = pbump.transfer_id
					INNER JOIN cities c ON c.id = pbump.city_id
					WHERE
						pbump.transfer_id = ?
			', array($id));
			
			$transfer->bump = $bump;
		}

		

		$transfer->gotds = $gotds;
		$transfer->orders = $orders;

		return $transfer;
	}

	public function delete($id)
	{
		
	}

	public function getAllByUsertId($user_id)
	{
		$sql = "SELECT t.* FROM transfers t WHERE t.user_id = ?";
		
		return self::getAdapter()->query($sql, array($user_id))->fetchAll();
	}

	public function save(Model_Billing_TransferItem $item, $reurn_transfer_id = false)
	{
		$edit_mode = false;
		if ( $item->id ) $edit_mode = true;
		
		$orders = $item->orders;
		unset($item->orders);
		
		if ( ! $edit_mode ) {
			$item->date_transfered = new Zend_Db_Expr('FROM_UNIXTIME(' . $item->date_transfered . ')');
		}

		self::getAdapter()->beginTransaction();
		try {
			parent::save($item);
			$transfer_id = self::getAdapter()->lastInsertId();
			
			if ( ! $transfer_id ) {
				$transfer_id = $item->id;
			}
			//return $transfer_id;
			$item->setId($transfer_id);
			
			if ( $edit_mode ) {
				if ( is_array($orders) && count($orders) > 0 ) {
					foreach ( $orders as $order_id ) {
						if ( $order_id > 0 ) {
							$item->deleteTransferOrders($order_id);
						}
					}
				}
				//die;
			}

			if ( is_array($orders) && count($orders) > 0 ) {
				foreach ( $orders as $order_id ) {
					if ( $order_id > 0 ) {
						$item->addOrder($order_id);
					}
				}
			}
			$temp_docs = new Zend_Session_Namespace('temp_docs');
			if ( is_array($temp_docs->images) && count($temp_docs->images) > 0 ) {
				foreach ( $temp_docs->images as $temp_doc) {
					if ( !is_null($temp_doc)) {
						$temp_doc['transfer_id'] = $transfer_id;
						$this->setImg($temp_doc);
					}
				}
			}
			unset($temp_docs->images);
			self::getAdapter()->commit();
			
			if ( $item->transfer_type_id == 6 ) {
				$item->confirm('Sales Einkassiert', $item->amount);
			}
			
			if ( Cubix_Application::getId() == APP_A6 && $item->transfer_type_id == 1 ) { // Bank Transfer
				$item->confirm('And6 bank transfer', $item->amount);
			}
			
			Zend_Registry::get('BillingHooker')->notify('transfer_created', array($transfer_id));

			$result = Model_Activity::getInstance()->log('add_transfer', array('transfer id' => $transfer_id));
			
		}
		catch (Exception $e) {
			self::getAdapter()->rollBack();
			return $e->getMessage();
		}

		if ( $reurn_transfer_id ) {
			return $transfer_id;
		} else {
			return true;
		}
		
	}

	public function checkTransactionId($transaction_id, $transfer_id = null)
	{
		if ( $transfer_id ) {
			return ! ((bool) self::getAdapter()->fetchOne('SELECT TRUE FROM transfers WHERE transaction_id = ? AND id <> ?', array($transaction_id, $transfer_id)));
		} else {
			return ! ((bool) self::getAdapter()->fetchOne('SELECT TRUE FROM transfers WHERE transaction_id = ?', $transaction_id));
		}
		
	}

	public function getTransferTypes()
	{
		return self::getAdapter()->fetchAll('
			SELECT * FROM transfer_types
		');
	}

	public function getWebmoneyAccounts()
	{
		return self::getAdapter()->fetchAll('
			SELECT * FROM webmoney_account_types
		');
	}

	public function setImg($image)
	{
		self::getAdapter()->insert('transfer_imgs', array('transfer_id' => $image['transfer_id'], 'hash' => $image['hash'],'ext' => $image['ext']));
		return self::getAdapter()->lastInsertId();
		
	}

	public function getImgUrl($image, $size = false)
	{
		if ( $image['hash'] ) {
			$images_model = new Cubix_Images();

			$img_datas = array(
				'application_id' => Cubix_Application::getId(),
				'catalog_id' => 'transfers',
				'hash' => $image['hash'],
				'ext' => $image['ext']
			);
			if($size){
				$img_datas['size'] = $size;
			}
			return $images_model->getUrl(new Cubix_Images_Entry($img_datas));
		}

		return NULL;
	}

	public function getTransferImgsById($id)
	{
		$results = self::getAdapter()->fetchAll('SELECT * FROM transfer_imgs WHERE transfer_id = ?', $id);
		foreach($results as &$result){
			$result->img_url = $this->getImgUrl((array)$result);
			if($result->ext != 'pdf'){
				$result->img_url_small = $this->getImgUrl((array)$result,'agency_p100');
			}
		}
		return $results;
	}
	
	/**
	 * Returns a data which needs to be exported as CSV, the task about ascii files
	 * requested by Davide
	 *
	 * @param array $filter
	 * @param type $page
	 * @param type $per_page
	 * @param type $sort_field
	 * @param type $sort_dir
	 * @return array
	 */
	public function getDataForExport(array $filter = array(), $page = 1, $per_page = 10, $sort_field = 'order_date', $sort_dir = 'DESC')
	{
		if (Cubix_Application::getId() == APP_A6)
		{
			$fields = array(
				't.id', 'u.user_type', 'u.username', 'tt.name AS transfer_type', 't.status', 'CONCAT(t.first_name, " ", t.last_name) AS full_name',
				'GROUP_CONCAT(DISTINCT(o.system_order_id)) AS orders', 't.date_transfered', 't.amount AS paid_amount','t.amount AS paid_amount',
				'bu.username AS sales_person', 'o.id AS order_id', 'ci.title_de AS region'
			);
		}
		else
		{
			$fields = array(
				't.id', 'u.user_type', 'u.username', 'tt.name AS transfer_type', 't.status', 'CONCAT(t.first_name, " ", t.last_name) AS full_name',
				'GROUP_CONCAT(DISTINCT(o.system_order_id)) AS orders', 't.date_transfered', 'tc.date AS payment_date', 'tc.amount AS paid_amount',
				'bu.username AS sales_person', 'o.id AS order_id', 't.transaction_id', 't.amount as transfer_amount', 
			);
		}

		if (Cubix_Application::getId() == APP_ED){
			$fields[] .= ' GROUP_CONCAT( DISTINCT cname.title_en) as cities';
			$fields[] .= ' GROUP_CONCAT( DISTINCT countriesname.title_en) as countries';
		}
		
		$sql = array(
			'tables' => array('transfers t'),
			'fields' => $fields,
			'joins' => array(
				'INNER JOIN users u ON u.id = t.user_id',
				'INNER JOIN backend_users bu ON bu.id = t.backend_user_id',
				'INNER JOIN transfer_types tt ON tt.id = t.transfer_type_id',
				'LEFT JOIN transfer_confirmations tc ON tc.transfer_id = t.id',
				'LEFT JOIN transfer_orders too ON too.transfer_id = t.id',
				'LEFT JOIN orders o ON o.id = too.order_id',
				'LEFT JOIN escorts e ON e.user_id = t.user_id',
				'LEFT JOIN agencies a ON a.user_id = t.user_id'
			),
			'where' => array(
			) + $filter,
			'group' => 't.id',
			'order' => array($sort_field, $sort_dir)
		);
		
		if (Cubix_Application::getId() == APP_A6)
		{
			$sql['joins'][] = 'LEFT JOIN escort_cities ec ON e.id = ec.escort_id';
			$sql['joins'][] = 'LEFT JOIN cities ci ON ec.city_id = ci.id';
		}

		if (Cubix_Application::getId() == APP_ED){
			$sql['joins'][] = 'LEFT JOIN order_packages op ON op.order_id = o.id';
			$sql['joins'][] = 'LEFT JOIN premium_escorts pe on pe.order_package_id = op.id';
			$sql['joins'][] = 'LEFT JOIN cities cname on cname.id = pe.city_id';
			$sql['joins'][] = 'LEFT JOIN countries countriesname on countriesname.id = cname.country_id';
		}
		
		$dataSql = Cubix_Model::getSql($sql);
		$data = parent::_fetchAll($dataSql);
		
		foreach ( $data as $i => $row ) {
			$data[$i]->status = self::$STATUS_LABELS[$row->status];
		}

		return array(
			'data' => $data
		);
	}
	
	public function zeroBalance()
	{
		return parent::db()->query('
			UPDATE users 
			SET balance = 0
		');
	}
	
	public function getCommentGrid($id)
	{
		return parent::getAdapter()->fetchOne('SELECT comment_grid FROM transfers WHERE id = ?', $id);
	}
	
	public function updateCommentGrid($data)
	{
		return parent::db()->update($this->_table, array('comment_grid' => $data['comment']), parent::quote('id = ?', $data['id']));
		
	}

    public function getDuplicityByCookie($user_id, $client_cookie_id) {
        $sql = "SELECT t.id, t.status, u.user_type
                FROM transfers as t
                LEFT JOIN users as u ON t.user_id = u.id 
                WHERE u.id IS NOT NULl AND t.user_id <> ? AND t.client_cookie_id = ?";
       return self::getAdapter()->fetchAll($sql, array($user_id, $client_cookie_id));
	}

    public function getDuplicityByIp($user_id, $client_cookie_id) {
        $sql = "SELECT t.id, t.status, u.user_type
                FROM transfers as t
                LEFT JOIN users as u ON t.user_id = u.id 
                WHERE u.id IS NOT NULl AND t.user_id <> ? AND t.ip = ?";
       return self::getAdapter()->fetchAll($sql, array($user_id, $client_cookie_id));
    }
}
