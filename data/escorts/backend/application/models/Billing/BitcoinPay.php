<?php

class Model_Billing_BitcoinPay 
{
	private $Token = 'kkyd8Oyyp4hQ8NjGDTCbfdEf';
		
	public function getTransaction($payment_id)
	{
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "https://www.bitcoinpay.com/api/v1/transaction-history/".$payment_id);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);

		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		  "Content-Type: application/json",
		  "Authorization: Token " . $this->Token
		));
		
		$responseData = curl_exec($ch);
		
		if(curl_errno($ch)) {
			return curl_error($ch);
		}
		curl_close($ch);
		return $responseData;
	}
	
	public function saveCallbacks($payResponse)
	{
		$db = Zend_Registry::get('db');
		
		list($payment_type, $user_id) = explode('-', $payResponse->reference);
		
		$data = array(
			'user_id' => $user_id,
			'payment_type' => $payment_type,
			'price' => $payResponse->price,
			'status' => $payResponse->status,
			'data' => var_export($payResponse, true)
		);
		
		$db->insert('bitcoin_payment_log', $data);
	}		
			
}