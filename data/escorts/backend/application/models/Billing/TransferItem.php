<?php

class Model_Billing_TransferItem extends Cubix_Model_Item
{
	public function getOrders()
	{
		$sql = "
			SELECT o.*
			FROM orders o
			INNER JOIN transfer_orders tor ON tor.order_id = o.id
			WHERE tor.transfer_id = ?
		";

		$orders = self::getAdapter()->query($sql, array($this->getId()))->fetchAll();

		foreach ( $orders as $i => $order ) {
			$orders[$i] = new Model_Billing_OrderItem($order);
		}

		return $orders;
	}
	
	public function autoReject($comment, $cancel_packages = false)
	{
		self::getAdapter()->beginTransaction();

		$packages_model = new Model_Billing_Packages();

		try {
			self::getAdapter()->update('transfers', array(
				'status' => Model_Billing_Transfers::STATUS_REJECTED
			), array(
				self::getAdapter()->quoteInto('id = ?', $this->getId())
			));
			
			/*self::getAdapter()->insert('transfer_rejections', array(
				'transfer_id' => $this->getId(),
				'date' => new Zend_Db_Expr('NOW()'),
				'comment' => $comment,
				'backend_user_id' => Zend_Auth::getInstance()->getIdentity()->id
			));*/

			/* ->> Cancel all packages of orders associated with this transfer */

			// Get all orderss
			$orders = $this->getOrders();
			
			$email_data = array();
			
			// Cancel them
			foreach($orders as $order) {
				self::getAdapter()->update('orders', array(
					'status' => Model_Billing_Orders::STATUS_PAYMENT_REJECTED
					//'date_rejected' => new Zend_Db_Expr('NOW()')
				), array(
					self::getAdapter()->quoteInto('id = ?', $order->id)
				));

				/*self::getAdapter()->insert('order_status_log', array(
					'order_id' => $order->id,
					'backend_user_id' => Zend_Auth::getInstance()->getIdentity()->id,
					'initial_status' => $order->status,
					'new_status' => Model_Billing_Orders::STATUS_PAYMENT_REJECTED,
					'comment' => 'Order [' . $order->id . '] status changed from "' . Model_Billing_Orders::$STATUS_LABELS[$order->status] . '" to "' . Model_Billing_Orders::$STATUS_LABELS[Model_Billing_Orders::STATUS_PAYMENT_REJECTED] . '". Reason: Transfer [' . $this->getId() . '] Rejected'
				));*/

				$m_order = new Model_Billing_Orders();
				$o_data = $m_order->getDetails($order->id);
				
				$email_data = array(
					'username' => $o_data->username,
					'order_id' => $o_data->system_order_id,
					'escort_id' => $o_data->escort_id,
					'user_type' => $o_data->user_type,
					'price' => $o_data->price,
					'email' => $o_data->sales_email
				);
				
				if ( ! $cancel_packages ) {
					continue;
				}

				$order->close('Manual transfer rejection');
			}
			/* <-- */
			
			self::getAdapter()->commit();
			Zend_Registry::get('BillingHooker')->notify('transfer_rejected', array($this->getId(), $email_data));

			//Model_Activity::getInstance()->log('reject_transfer', array('transfer id' => $this->getId()));
		}
		catch (Exception $e) {
			self::getAdapter()->rollback();

			throw $e;
		}
	}

	public function reject($comment, $cancel_packages = false)
	{
		self::getAdapter()->beginTransaction();

		$packages_model = new Model_Billing_Packages();

		try {
			self::getAdapter()->update('transfers', array(
				'status' => Model_Billing_Transfers::STATUS_REJECTED
			), array(
				self::getAdapter()->quoteInto('id = ?', $this->getId())
			));
			
			self::getAdapter()->insert('transfer_rejections', array(
				'transfer_id' => $this->getId(),
				'date' => new Zend_Db_Expr('NOW()'),
				'comment' => $comment,
				'backend_user_id' => Zend_Auth::getInstance()->getIdentity()->id
			));

			/* ->> Cancel all packages of orders associated with this transfer */

			// Get all orderss
			$orders = $this->getOrders();
			
			$email_data = array();
			
			// Cancel them
			foreach($orders as $order) {
				self::getAdapter()->update('orders', array(
					'status' => Model_Billing_Orders::STATUS_PAYMENT_REJECTED,
					'has_rejected_transfer'	=> 1
					//'date_rejected' => new Zend_Db_Expr('NOW()')
				), array(
					self::getAdapter()->quoteInto('id = ?', $order->id)
				));

				self::getAdapter()->insert('order_status_log', array(
					'order_id' => $order->id,
					'backend_user_id' => Zend_Auth::getInstance()->getIdentity()->id,
					'initial_status' => $order->status,
					'new_status' => Model_Billing_Orders::STATUS_PAYMENT_REJECTED,
					'comment' => 'Order [' . $order->id . '] status changed from "' . Model_Billing_Orders::$STATUS_LABELS[$order->status] . '" to "' . Model_Billing_Orders::$STATUS_LABELS[Model_Billing_Orders::STATUS_PAYMENT_REJECTED] . '". Reason: Transfer [' . $this->getId() . '] Rejected'
				));
				
				$m_order = new Model_Billing_Orders();
				$o_data = $m_order->getDetails($order->id);
				
				$email_data = array(
					'username' => $o_data->username,
					'order_id' => $o_data->system_order_id,
					'escort_id' => $o_data->escort_id,
					'user_type' => $o_data->user_type,
					'price' => $o_data->price,
					'email' => $o_data->sales_email
				);

				if ( ! $cancel_packages ) {
					continue;
				}

				$order->close('Manual transfer rejection');
			}
			/* <-- */
			
			self::getAdapter()->commit();

			//EDIR-1328
			if(Cubix_Application::getId() != APP_ED){
                Zend_Registry::get('BillingHooker')->notify('transfer_rejected', array($this->getId(), $email_data));
            }

			Model_Activity::getInstance()->log('reject_transfer', array('transfer id' => $this->getId()));
		}
		catch (Exception $e) {
			self::getAdapter()->rollback();

			throw $e;
		}
	}
	
	public function chargeback($comment, $cancel_packages = false)
	{
		self::getAdapter()->beginTransaction();

		try {
			self::getAdapter()->update('transfers', array(
				'status' => Model_Billing_Transfers::STATUS_CHARGEBACK
			), array(
				self::getAdapter()->quoteInto('id = ?', $this->getId())
			));
			
			self::getAdapter()->insert('transfer_rejections', array(
				'transfer_id' => $this->getId(),
				'date' => new Zend_Db_Expr('NOW()'),
				'comment' => $comment,
				'backend_user_id' => Zend_Auth::getInstance()->getIdentity()->id
			));

			/* ->> Cancel all packages of orders associated with this transfer */

			// Get all orderss
			$orders = $this->getOrders();
			
			// Cancel them
			foreach($orders as $order) {
				self::getAdapter()->update('orders', array(
					'status' => Model_Billing_Orders::STATUS_PAYMENT_CHARGEBACK,
					'has_rejected_transfer'	=> 1
					//'date_rejected' => new Zend_Db_Expr('NOW()')
				), array(
					self::getAdapter()->quoteInto('id = ?', $order->id)
				));

				self::getAdapter()->insert('order_status_log', array(
					'order_id' => $order->id,
					'backend_user_id' => Zend_Auth::getInstance()->getIdentity()->id,
					'initial_status' => $order->status,
					'new_status' => Model_Billing_Orders::STATUS_PAYMENT_CHARGEBACK,
					'comment' => 'Order [' . $order->id . '] status changed from "' . Model_Billing_Orders::$STATUS_LABELS[$order->status] . '" to "' . Model_Billing_Orders::$STATUS_LABELS[Model_Billing_Orders::STATUS_PAYMENT_CHARGEBACK] . '". Reason: Transfer [' . $this->getId() . '] Chargeback'
				));

				if ( ! $cancel_packages ) {
					continue;
				}

				$order->close('Manual transfer rejection');
			}
			/* <-- */
			
			self::getAdapter()->commit();
			Zend_Registry::get('BillingHooker')->notify('transfer_rejected', array($this->getId()));

			Model_Activity::getInstance()->log('reject_transfer', array('transfer id' => $this->getId()));
		}
		catch (Exception $e) {
			self::getAdapter()->rollback();

			throw $e;
		}
	}

	public function getActivePackages()
	{
		$sql = "
			SELECT
				op.*
			FROM transfer_orders ot
			/*INNER JOIN transfers t ON t.id = ot.transfer_id*/
			INNER JOIN orders o ON o.id = ot.order_id
			INNER JOIN order_packages op ON op.order_id = o.id
			
			WHERE o.user_id = ? /*AND op.status = ?*/
			/*GROUP BY op.id*/
		";

		return self::getAdapter()->query($sql, array($this->getUserId()/*, Model_Packages::STATUS_ACTIVE*/))->fetchAll();
	}

	public function confirm($comment, $amount)
	{
		self::getAdapter()->beginTransaction();

		try {
			self::getAdapter()->insert('transfer_confirmations', array(
				'transfer_id' => $this->getId(),
				'backend_user_id' => Zend_Auth::getInstance()->getIdentity()->id,
				'comment' => $comment,
				'amount' => doubleval($amount),
				'date' => new Zend_Db_Expr('NOW()')
			));

			self::getAdapter()->update('transfers', array(
				'status' => Model_Billing_Transfers::STATUS_CONFIRMED
			), array(
				self::getAdapter()->quoteInto('id = ?', $this->getId())
			));

			self::getAdapter()->commit();
			Zend_Registry::get('BillingHooker')->notify('transfer_confirmed', array($this->getId()));

			Model_Activity::getInstance()->log('confirm_transfer', array('transfer id' => $this->getId()));
		}
		catch (Exception $e) {
			self::getAdapter()->rollback();

			throw $e;
		}
	}

	public function autoConfirm($comment, $amount, $auto_user_id)
	{
		self::getAdapter()->beginTransaction();

		try {
			self::getAdapter()->insert('transfer_confirmations', array(
				'transfer_id' => $this->getId(),
				'backend_user_id' => $auto_user_id,
				'comment' => $comment,
				'amount' => doubleval($amount),
				'date' => new Zend_Db_Expr('NOW()')
			));

			self::getAdapter()->update('transfers', array(
				'status' => Model_Billing_Transfers::STATUS_CONFIRMED
			), array(
				self::getAdapter()->quoteInto('id = ?', $this->getId())
			));

			self::getAdapter()->commit();
			Zend_Registry::get('BillingHooker')->notify('transfer_confirmed', array($this->getId()));

			Model_Activity::getInstance()->log('confirm_transfer', array('transfer id' => $this->getId()));
		}
		catch (Exception $e) {
			self::getAdapter()->rollback();

			throw $e;
		}
	}

	public function delete()
	{
		self::getAdapter()->delete('transfers', self::getAdapter()->quoteInto('id = ?', $this->getId()));
	}

	public function deleteTransferOrders($order_id)
	{
		self::getAdapter()->query('SET foreign_key_checks = 0');
		self::getAdapter()->delete('transfer_orders', array(self::getAdapter()->quoteInto('order_id = ?', $order_id), self::getAdapter()->quoteInto('transfer_id = ?', $this->getId())));
	}
	
	public function addOrder($order_id)
	{
		self::getAdapter()->insert('transfer_orders', array(
			'order_id' => $order_id,
			'transfer_id' => $this->getId()
		));
	}
}
