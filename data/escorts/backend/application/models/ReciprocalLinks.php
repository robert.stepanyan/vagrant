<?php

class Model_ReciprocalLinks extends Cubix_Model {
	
	private static $_selfUrl;//= 'www.escortforumit.xxx';

	public function __construct()
	{
		$application = Model_Applications::getById(Cubix_Application::getId());
		self::$_selfUrl = 'www.' . $application->host;
		//self::$_selfUrl = 'www.escortforumit.xxx';
	}
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$where = '';
		if ( strlen($filter['website']) ) {
			$where .= self::quote(' AND website LIKE ? ', '%' . $filter['website'] . '%');
		}

		if ( $filter['site_owner_type']) {
			$where .= self::quote(' AND u.user_type = ? ', $filter['site_owner_type']);
		}
		
		if ( strlen($filter['status'])) {
			$where .= self::quote(' AND wmrl.status = ? ', $filter['status']);
		}
		
		if ( strlen($filter['showname']) ) {
			$where .= self::quote(' AND e.showname LIKE ? ', $filter['showname'] . '%');
		}
		
		if ( strlen($filter['agency_name']) ) {
			$where .= self::quote(' AND a.name LIKE ? ', $filter['agency_name'] . '%');
		}
		
		if ( strlen($filter['agency_id']) ) {
			$where .= self::quote(' AND a.id = ? ', $filter['agency_id']);
		}
		
		if ( strlen($filter['escort_id']) ) {
			$where .= self::quote(' AND e.id = ? ', $filter['escort_id']);
		}
		
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS wmrl.id, wmrl.website, wmrl.check_date, wmrl.user_id,
				u.user_type, u.username, e.showname, e.id AS escort_id, a.id AS agency_id, a.name AS agency_name, wmrl.status
			FROM websites_missing_reciprocal_link wmrl
			INNER JOIN users u ON u.id = wmrl.user_id
			LEFT JOIN escorts e ON e.user_id = wmrl.user_id AND u.user_type = "escort"
			LEFT JOIN agencies a ON a.user_id = wmrl.user_id AND u.user_type = "agency"
			WHERE 1 ' . $where . '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$data = parent::_fetchAll($sql);
		$count = intval(self::db()->fetchOne('SELECT FOUND_ROWS()'));
		
		return $data;
	}
	
	public function delete($ids)
	{
		if ( ! count($ids) ) return;
		
		foreach($ids as $id) {
			self::db()->delete('websites_missing_reciprocal_link', self::quote('id = ?', $id));
		}
	}
	
	
	public function insert($row)
	{
		return self::db()->insert('websites_missing_reciprocal_link', $row);
	}
	
	public function update($row, $website)
	{
		return self::db()->update('websites_missing_reciprocal_link', $row, self::db()->quoteInto('website = ?', $website));
	}
	
	public function deleteByWebsite($url)
	{
		self::db()->delete('websites_missing_reciprocal_link', self::quote('website = ?', $url));
		
		return;
	}
	
	public function checkIfExists($url)
	{
		return self::db()->fetchOne('
			SELECT TRUE
			FROM websites_missing_reciprocal_link
			WHERE website = ?
		', array($url));
	}
	
	public function getWebsitesToCheck()
	{
		//GETTING websites of agencies and escorts with their user_id
		/*$sql = '
			(
				SELECT e.user_id AS user_id, ep.website AS url
				FROM escorts e
				INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
				WHERE e.status = ? AND ep.website IS NOT NULL AND CHAR_LENGTH(TRIM(ep.website)) > 0
				GROUP BY url
			)
			UNION ALL
			(
				SELECT a.user_id AS user_id, a.web AS url
				FROM agencies a
				WHERE a.web IS NOT NULL AND CHAR_LENGTH(TRIM(a.web)) > 0
				GROUP BY url
			)
			
		';*/
		$sql = '
			SELECT e.user_id AS user_id, ep.website AS url, u.email AS email
			FROM escorts e
			INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id
			WHERE e.status = ? AND u.user_type = ? AND ep.website IS NOT NULL AND CHAR_LENGTH(TRIM(ep.website)) > 0
		';
		
		return self::db()->fetchAll($sql, array(Model_EscortsV2::ESCORT_STATUS_ACTIVE, Model_Users::USER_TYPE_ESCORT));
	}
	
	public function checkIfHasReciprocalLink($url)
	{
		/*$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($curl, CURLOPT_CONNECTTIMEOUT, 10);
		
		$html = curl_exec($curl);
		curl_close($curl);*/
		$html = file_get_contents($url);
		
		$dom = new DOMDocument();
		$dom->loadHTML($html);
		
		$links = $dom->getElementsByTagName('a');
		foreach($links as $link) {
			if ( strpos($link->getAttribute('href'), self::$_selfUrl) ) {
				return true;
			}
		}
		
		return false;
	}
	
	public function toggle($id)
	{		
		$field = 'status';
		
		parent::getAdapter()->update('websites_missing_reciprocal_link', array(
			$field => new Zend_Db_Expr('NOT ' . $field)
		), parent::quote('id = ?', $id));
	}
}
