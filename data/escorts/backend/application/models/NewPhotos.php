<?php


class Model_NewPhotos  extends Cubix_Model {
	protected $_table = 'escort_photos';
	protected $_itemClass = 'Model_EscortItem';
	public function GetlastPhoto($id,$showname,$date_from,$date_to,$page=1,$per_page=10,$sort_field='quantity',$sort_dir='DESC')
	{	
		
			$db = Zend_Registry::get('db');
			$where=array();
			if(isset($id) && ($id=trim($id))!='')
			{	
				$id = $db->quote($id);	
				$where[]=" escorts.id=$id ";
			}
			if(isset($showname) && ($showname=trim($showname))!='')
			{	$showname = "$showname%";
				$showname = $db->quote($showname);	
				$where[]=" showname LIKE $showname ";
			}
			if(isset($date_from))
			{	
				$date_from =date('Y-m-d',$date_from);
				$where[]=" DATE_FORMAT(escort_photos.`creation_date`,'%Y-%m-%d')>='$date_from' ";
			}
			if(isset($date_to))
			{
				$date_to =date('Y-m-d',$date_to);
				$where[]=" DATE_FORMAT(escort_photos.`creation_date`,'%Y-%m-%d')<='$date_to' ";
			}
			
			$where = implode(' AND ', $where);
	
			$page = !isset($page) || !is_numeric($page)?1:$db->quote($page);
			$start = ($page-1)*$per_page;
			$sql = "SELECT SQL_CALC_FOUND_ROWS COUNT(escorts.id) AS quantity,showname,escort_photos.`creation_date`,gender,escort_id AS id
					 FROM escort_photos JOIN escorts 
					 ON $where AND escorts.id=escort_photos.escort_id AND (`type`='".ESCORT_PHOTO_TYPE_HARD."' OR `type`='".ESCORT_PHOTO_TYPE_PRIVATE."') GROUP BY escorts.id,DATE_FORMAT(creation_date,'%Y-%m-%d')
					 ORDER BY `$sort_field` $sort_dir LIMIT $start,$per_page" ;

			$data = $db->fetchAll($sql);
			$found = $db->fetchRow('SELECT FOUND_ROWS() AS q');
			return	array('data'=>$data,'count'=>$found->q);
	}
	
	public function get($id)
	{
		$db = Zend_Registry::get('db');
		$id = $db->quote($id);
		$sql = "SELECT  `creation_date`,id,`hash`,`type`,ext,escort_id FROM escort_photos WHERE  escort_id=$id AND (`type`='".ESCORT_PHOTO_TYPE_HARD."' OR `type`='".ESCORT_PHOTO_TYPE_PRIVATE."')  ORDER BY `creation_date` DESC";
		$data = $db->fetchAll($sql);
		return $data;
	}
	
	public function disable($ids)
	{	$str_id ='';
		$count = count($ids);
		for($i=0;$i<$count;$i++)
		{	
			$db = Zend_Registry::get('db');
			$ids[$i] = $db->quote($ids[$i]);
			$str_id.=$ids[$i].',';
		}
		$str_id=substr($str_id,0,strlen($str_id)-1);
		$sql = " UPDATE escort_photos SET `type`='".ESCORT_PHOTO_TYPE_DISABLED."' WHERE id IN($str_id)";
		$db->query($sql);
		
	}

	
	
}

?>
