<?php
class Model_Menu
{
	/**
	 * @var Model_Menu_Item
	 */
	protected $_rootNode;
	protected $_url;

	public function __construct(array $params)
	{
		$this->_url = Zend_Controller_Front::getInstance()->getRequest()->getPathInfo();
		$this->_url = trim($this->_url, '/');

		$this->_rootNode = new Model_Menu_Item('Root', array(
			'key' => '',
			'link' => '',
			'res_id' => '',
			'type' => '',
			'id' => ''
		));
		
		$this->_process($params, $this->_rootNode);

		//print_r($this->_rootNode);
	}

	protected function _process($params, Model_Menu_Item $parent)
	{
		foreach ($params as $node) {
			$item = new Model_Menu_Item($node['title'], array(
				'key' => @$node['key'],
				'link' => @$node['link'],
				'res_id' => @$node['res_id'],
				'class' => @$node['class'],
				'type' => @$node['type'],
				'id' => @$node['id']
			));

			if ( preg_match('#' . $item->key . '#', $this->_url) ) {
				$item->is_active = true;
			}
			
			$parent->addChild($item);

			if ( isset($node['children']) && is_array($node['children']) ) {
				$this->_process($node['children'], $item);
				$item->link = $item->children[0]->link;
			}
		}
	}
	
	public function getChildren()
	{
		return $this->_rootNode->children;
	}
}
