<?php

class Model_Applications extends Cubix_Model
{
	public function getAll()
	{

	}

	public function getById($id)
	{
		$db = Zend_Registry::get('db');
		
		return $db->query('SELECT * FROM applications WHERE id = ?', $id)->fetch();
	}
	
	public static function getApplicationByHost($host)
	{
		
		$db = Zend_Registry::get('db');

		$select = $db->query('SELECT * FROM applications WHERE host = ?', $host);

		return $select->fetch();
	}
	
	public static function getCountryId()
	{
		$db = Zend_Registry::get('db');
		$country_id = $db->query('SELECT country_id FROM applications a')->fetch();
		return $country_id->country_id;
	}
}
