<?php
class Model_BOUsers extends Cubix_Model
{
	protected $_table = 'backend_users';
	protected $_itemClass = 'Model_BOUserItem';

	public function get($id)
	{
	    $ed_fields = '';

        if ( Cubix_Application::getId() == APP_ED )
        {
            $ed_fields .= ', bu.phone_verification_allowed, bu.working_phone';
        }

		$sql = '
			SELECT
				bu.id, bu.first_name, bu.last_name, bu.username,
				bu.email, bu.skype, bu.type, bu.application_id,
				bu.is_disabled, bu.email_signature, bu.is_auto_approve_photos, bu.receive_emails'. $ed_fields .'
			FROM backend_users bu
			LEFT JOIN applications ap ON ap.id = bu.application_id
			WHERE 1 AND bu.id = ?
		';
		
		return parent::_fetchRow($sql, array($id));
	}

	public function userNameExists($username)
	{
		$sql = '
			SELECT TRUE	FROM backend_users bu
			LEFT JOIN applications ap ON ap.id = bu.application_id
			WHERE bu.username = ?
		';

		return parent::getAdapter()->fetchOne($sql, array($username));
	}

	public function insert($data)
	{
		parent::getAdapter()->insert($this->_table, $data);

		return parent::getAdapter()->lastInsertId();
	}

	public function update($be_user_id, $data)
	{
		parent::getAdapter()->update($this->_table, $data, parent::getAdapter()->quoteInto('id = ?', $be_user_id));
	}

	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
        $where = '';

		$sql = '
			SELECT
				bu.id, bu.first_name, bu.last_name, bu.username,
				bu.email, bu.skype, bu.type, bu.application_id,
				bu.is_disabled
			FROM backend_users bu			
			LEFT JOIN applications ap ON ap.id = bu.application_id
			WHERE 1
		';
		
		$countSql = '
			SELECT COUNT(bu.id)
			FROM backend_users bu
			LEFT JOIN applications ap ON ap.id = bu.application_id
			WHERE 1
		';
		
		if ( strlen($filter['bu.application_id']) ) {
			$where .= self::quote('AND bu.application_id = ?', $filter['bu.application_id']);
		}

        if ( strlen($filter['bu.phone_verification_allowed']) ) {
            $where .= self::quote('AND bu.phone_verification_allowed = ? ', $filter['bu.phone_verification_allowed']);
        }

		if ( strlen($filter['bu.username']) ) {
			$where .= self::quote('AND bu.username LIKE ?', $filter['bu.username'] . '%');
		}
		
		if ( strlen($filter['bu.id']) ) {
			$where .= self::quote('AND bu.id = ?', $filter['bu.id']);
		}
		
		/*if ( strlen($filter['u.last_ip']) ) {
			$where .= self::quote('AND u.last_ip LIKE ?', $filter['u.last_ip'] . '%');
		}
		*/
		if ( strlen($filter['bu.type']) ) {
			$where .= self::quote('AND bu.type = ?', $filter['bu.type']);
		}
		
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			GROUP BY bu.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		/*echo $sql;
		echo "<br/>";
		echo $countSql;*/
		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return parent::_fetchAll($sql);
	}



	public function remove($id)
	{
		parent::remove(self::quote('id = ?', $id));
		parent::getAdapter()->delete('ticket_comments', parent::quote('ticket_id = ?', $id));
	}

	public function toggle($id)
	{
		parent::getAdapter()->update($this->_table, array(
			'is_disabled' => new Zend_Db_Expr('NOT is_disabled')
		), parent::quote('id = ?', $id));
	}
	
	public function getAllSales()
	{
		$bu_type = " bu.type = ? ";
		$arr = array(Cubix_Application::getId(), 'sales manager');
        $bu_user = Zend_Auth::getInstance()->getIdentity();
		if ( Cubix_Application::getId() == 2 ) {
			$bu_type = " (bu.type = ? OR bu.type = ?) ";
			$arr = array(Cubix_Application::getId(), 'sales manager', 'admin');
		}elseif (in_array(Cubix_Application::getId(), array(APP_6B, APP_EG_CO_UK, APP_ED)) && ($bu_user->type == 'superadmin' ||  $bu_user->id == 204 /* sophie */)) {
            $bu_type = " (bu.type = ? OR bu.type = ? OR bu.type = ?) ";
            $arr = array(Cubix_Application::getId(), 'sales manager', 'admin', 'call center');
        }
		
		$sql = '
			SELECT
				bu.id, bu.first_name, bu.last_name, bu.username,
				bu.email, bu.skype, bu.type, bu.application_id
			FROM backend_users bu			
			LEFT JOIN applications ap ON ap.id = bu.application_id
			WHERE ap.id = ? AND ' . $bu_type . ' AND is_disabled <> 1
		';
		
		return parent::_fetchAll($sql, $arr);
	}
	
	public function getAllBoUsers()
	{
		$sql = '
			SELECT
				bu.id, bu.first_name, bu.last_name, bu.username,
				bu.email, bu.skype, bu.type, bu.application_id
			FROM backend_users bu			
			LEFT JOIN applications ap ON ap.id = bu.application_id
			WHERE ap.id = ? AND is_disabled <> 1
		';
		
		return parent::_fetchAll($sql, array(Cubix_Application::getId()));
	}
	
	public function getBoUsers()
	{
		$sql = '
			SELECT
				bu.id, bu.username
			FROM backend_users bu			
			LEFT JOIN applications ap ON ap.id = bu.application_id
			WHERE ap.id = ?
		';
		
		return parent::_fetchAll($sql, array(Cubix_Application::getId()));
	}
	
	public function getEmailById($id)
	{
		$sql = 'SELECT email FROM backend_users WHERE application_id = ? AND id = ?';
		return parent::getAdapter()->fetchOne($sql, array(Cubix_Application::getId(),$id));
	}

	public static function getCurrentStatus($id)
    {
        $sql = 'SELECT is_online FROM backend_users WHERE id = ?';
        return parent::getAdapter()->fetchOne($sql, array($id));
    }

    public function setStatus($id, $status)
    {
        return parent::getAdapter()->update($this->_table, array('is_online' => $status), parent::getAdapter()->quoteInto('id = ?', $id));
    }
}
