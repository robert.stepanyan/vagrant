<?php

class Model_MemberBlacklistedEmails extends Cubix_Model
{

    public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT id, name
			FROM member_blacklisted_emails
			WHERE 1
		';

		$countSql = '
			SELECT COUNT(DISTINCT(id))
			FROM member_blacklisted_emails
			WHERE 1
		';

		if ( strlen($filter['name']) ) {
			$sql .= self::quote('AND name LIKE ?', '%' . $filter['name'] . '%');
			$countSql .= self::quote('AND name LIKE ?', '%' . $filter['name'] . '%');
		}


		$sql .= '
			GROUP BY id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$countSql .= '';

		$count = intval($this->getAdapter()->fetchOne($countSql));
		return parent::_fetchAll($sql);
	}

	public function get($id)
	{
		return self::_fetchRow('
			SELECT * FROM member_blacklisted_emails WHERE id = ?
		', array($id));
	}

    

    public function save($data,$id = null){
        if ( ! is_null($id) ) {
			self::getAdapter()->update('member_blacklisted_emails', $data, self::quote('id = ?', $id));
		}
		else {
			self::getAdapter()->insert('member_blacklisted_emails', $data);
		}
    }

    public function delete($id = array()){
        self::getAdapter()->delete('member_blacklisted_emails', self::getAdapter()->quoteInto('id = ?', $id));
    }


    public function wordExists($text, $id = null){
        $sql = '
			SELECT id, name
			FROM member_blacklisted_emails
			WHERE 1';

        $bl_emails = parent::_fetchAll($sql);
		if( count($bl_emails) > 0 ){
            foreach ( $bl_emails as $email ){
                if ($email->name == $text && $id != $email->id)
				{
					return true;
					BREAK;
				}
				
            }
			return false;
            
        }
        
    }

}
