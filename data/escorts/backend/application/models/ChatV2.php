<?php

class Model_Chatv2 extends Cubix_Model
{
	public function getUsersList($user)
	{
		$sql = "
			SELECT u.id, u.user_type, e.showname, e.id AS escort_id, a.name AS agency_name, u.username 
			FROM users u
			LEFT JOIN escorts e ON e.user_id = u.id
			LEFT JOIN agencies a ON a.user_id = u.id
			WHERE IF (u.user_type = 'escort', e.showname LIKE '" . $user . "%', IF (u.user_type = 'agency', a.name LIKE '" . $user . "%', u.username LIKE '" . $user . "%'))
			GROUP BY u.id
			ORDER BY COALESCE(e.showname, a.name, u.username), e.id ASC 
			LIMIT 0, 100
		";
		
		return $this->getAdapter()->query($sql)->fetchAll();
	}

	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$db = $this->getAdapter();
		
		$where = '';
		
		if (strlen($filter['message'])) 
			$where .= $db->quoteInto(' AND cm.text LIKE ?', '%' . $filter['message'] . '%');
		
		if ( $filter['date_from'] && $filter['date_to'] && (date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']))) 
			$where .= $db->quoteInto( " AND cm.date_created = ? " , date('Y-m-d', $filter['date_from']));
		else{
			if ( $filter['date_from'] ) 
				$where .= $db->quoteInto(" AND cm.date_created >= ? ", date('Y-m-d', $filter['date_from']));

			if ( $filter['date_to'] ) 
				$where .= $db->quoteInto(" AND cm.date_created <= ? ", date('Y-m-d', $filter['date_to']));
		}
		
		
		if (strlen($filter['f_user_id']) && strlen($filter['t_user_id'])) 
		{
			$where_both = ' cm.sender_id = ? AND cut.user_id = ? ';
			
			if ($filter['both'])
			{
				$where_both = ' ((cm.sender_id = ? AND cut.user_id = ?) OR (cut.user_id = ' . $filter['f_user_id'] . ' AND cm.sender_id = ' . $filter['t_user_id'] . ')) ';
			}
			
			$sql = '
				SELECT SQL_CALC_FOUND_ROWS 
					cm.id, cm.text as body, cm.sender_id, UNIX_TIMESTAMP(cm.date_created) AS date, /*UNIX_TIMESTAMP(cm.read_date) AS read_date,*/ u.user_type, u.username, e.showname, e.id AS escort_id, 
					a.name AS agency_name, cut.user_id AS user_id_to, a.id AS agency_id, m.is_suspicious, m.suspicious_reason 
				FROM chatroom_messages cm
				INNER JOIN chatroom_user_threads cut ON cut.thread_id = cm.thread_id AND cut.user_id <> cm.sender_id
				INNER JOIN users u ON u.id = cm.sender_id
				LEFT JOIN members m ON m.user_id = u.id
				LEFT JOIN escorts e ON e.user_id = u.id
				LEFT JOIN agencies a ON a.user_id = u.id
				WHERE ' . $where_both . $where . '
				GROUP BY cm.id 
				ORDER BY ' . $sort_field . ' ' . $sort_dir . '
				LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
			';
						
			$items = $db->query($sql, array($filter['f_user_id'], $filter['t_user_id']))->fetchAll();
		}
		elseif (!strlen($filter['f_user_id']) && strlen($filter['t_user_id']))
		{
			$sql = '
				SELECT SQL_CALC_FOUND_ROWS 
					cm.id, cm.text as body, cm.sender_id, UNIX_TIMESTAMP(cm.date_created) AS date, /*UNIX_TIMESTAMP(cm.read_date) AS read_date,*/ u.user_type, u.username, e.showname, e.id AS escort_id, 
					a.name AS agency_name, cut.user_id AS user_id_to, a.id AS agency_id, m.is_suspicious, m.suspicious_reason 
				FROM chatroom_messages cm
				INNER JOIN chatroom_user_threads cut ON cut.thread_id = cm.thread_id AND cut.user_id <> cm.sender_id
				INNER JOIN users u ON u.id = cm.sender_id
				LEFT JOIN members m ON m.user_id = u.id
				LEFT JOIN escorts e ON e.user_id = u.id
				LEFT JOIN agencies a ON a.user_id = u.id
				WHERE cut.user_id = ? ' . $where . '
				GROUP BY cm.id 
				ORDER BY ' . $sort_field . ' ' . $sort_dir . '
				LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
			';
				
			$items = $db->query($sql, $filter['t_user_id'])->fetchAll();
		}
		elseif (strlen($filter['f_user_id']) && !strlen($filter['t_user_id']))
		{
			$sql = '
				SELECT SQL_CALC_FOUND_ROWS 
					cm.id, cm.text as body, cm.sender_id, UNIX_TIMESTAMP(cm.date_created) AS date, /*UNIX_TIMESTAMP(cm.read_date) AS read_date,*/ u.user_type, u.username, e.showname, e.id AS escort_id, 
					a.name AS agency_name, cut.user_id AS user_id_to, a.id AS agency_id, m.is_suspicious, m.suspicious_reason 
				FROM chatroom_messages cm
				INNER JOIN chatroom_user_threads cut ON cut.thread_id = cm.thread_id AND cut.user_id <> cm.sender_id
				INNER JOIN users u ON u.id = cm.sender_id
				LEFT JOIN members m ON m.user_id = u.id
				LEFT JOIN escorts e ON e.user_id = u.id
				LEFT JOIN agencies a ON a.user_id = u.id
				WHERE cm.sender_id = ? ' . $where . '
				GROUP BY cm.id 
				ORDER BY ' . $sort_field . ' ' . $sort_dir . '
				LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
			';
			
			$items = $db->query($sql, $filter['f_user_id'])->fetchAll();
		}
		else
		{
			$sql = '
				SELECT SQL_CALC_FOUND_ROWS 
					cm.id, cm.text as body, cm.sender_id, UNIX_TIMESTAMP(cm.date_created) AS date, /*UNIX_TIMESTAMP(cm.read_date) AS read_date,*/ u.user_type, u.username, e.showname, e.id AS escort_id, 
					a.name AS agency_name, cut.user_id AS user_id_to, a.id AS agency_id, m.is_suspicious, m.suspicious_reason 
				FROM chatroom_messages cm
				INNER JOIN chatroom_user_threads cut ON cut.thread_id = cm.thread_id
				INNER JOIN users u ON u.id = cm.sender_id
				LEFT JOIN members m ON m.user_id = u.id
				LEFT JOIN escorts e ON e.user_id = u.id
				LEFT JOIN agencies a ON a.user_id = u.id
				WHERE 1 ' . $where . '
				GROUP BY cm.id 
				ORDER BY ' . $sort_field . ' ' . $sort_dir . '
				LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
			';
			
			$items = $db->query($sql)->fetchAll();
		}
		
		$count = intval($db->fetchOne('SELECT FOUND_ROWS()'));
		
		return $items;
	}
	
	public function getUserInfo($user_id)
	{
		$sql = '
			SELECT u.user_type, u.username, e.showname, e.id AS escort_id, a.name AS agency_name, a.id AS agency_id
			FROM users u
			LEFT JOIN escorts e ON e.user_id = u.id
			LEFT JOIN agencies a ON a.user_id = u.id
			WHERE u.id = ?
		';
		
		return $this->getAdapter()->query($sql, $user_id)->fetch();
	}
	
	public function delete($ids)
	{
		$ids = implode(',', $ids);
		
		$this->getAdapter()->query('DELETE FROM chatroom_messages WHERE id IN (' . $ids . ')');
	}
}
