<?php
class Model_Faq extends Cubix_Model
{
    
         protected $_table = 'faq';
         protected $_table_photo = 'faq_photos';
	protected $_itemClass = 'Model_System_FaqItem';
        

	
	const FAQ_TYPE_ADMIN = 'admin';
	const FAQ_TYPE_SALES = 'sales';
        
        const STATUS_REMOVED = 0;
        
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
	    $additionalField = '';
		if ( in_array(Cubix_Application::getId(), array(APP_EF,APP_EG_CO_UK,APP_BL,APP_ED)) )
        {
            $additionalField = ', importance';
        }

        $sql = '
			SELECT id, title, question_en, type'. $additionalField .'  
			FROM faq
			WHERE (FIND_IN_SET(?, type) OR FIND_IN_SET(?, type)) AND status = 1
		';

		$countSql = '
			SELECT COUNT(DISTINCT(id))
			FROM faq
			WHERE (FIND_IN_SET(?, type) OR FIND_IN_SET(?, type)) AND status = 1
		';

		if ( strlen($filter['title']) ) {
			$sql .= self::quote('AND title LIKE ?', '%' . $filter['title'] . '%');
			$countSql .= self::quote('AND title LIKE ?', '%' . $filter['title'] . '%');
		}

		if ( strlen($filter['question']) ) {
			$sql .= self::quote('AND question_en LIKE ?', '%' . $filter['question'] . '%');
			$countSql .= self::quote('AND question_en LIKE ?', '%' . $filter['question'] . '%');
		}

		if ( strlen($filter['answer']) ) {
			$sql .= self::quote('AND answer_en LIKE ?', '%' . $filter['answer'] . '%');
			$countSql .= self::quote('AND answer_en LIKE ?', '%' . $filter['answer'] . '%');
		}

		if ( strlen($filter['type']) ) {
			$sql .= self::quote('AND FIND_IN_SET(?, type)', $filter['type']);
			$countSql .= self::quote('AND type = ?', $filter['type']);
		}

		if ( strlen($filter['application_id']) ) {
			$sql .= self::quote('AND application_id = ?', $filter['application_id']);
			$countSql .= self::quote('AND application_id = ?', $filter['application_id']);
		}

        $additionalOrder = '';
        if ( in_array(Cubix_Application::getId(), array(APP_EF,APP_EG_CO_UK,APP_BL,APP_ED)) )
        {
            $additionalOrder = 'importance desc, ';
        }

		$sql .= '
			GROUP BY id
			ORDER BY '.$additionalOrder . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$countSql .= '

		';

		$count = intval($this->getAdapter()->fetchOne($countSql, array(Model_System_Faq::FAQ_TYPE_ADMIN, Model_System_Faq::FAQ_TYPE_SALES)));

		return parent::_fetchAll($sql, array(Model_System_Faq::FAQ_TYPE_ADMIN, Model_System_Faq::FAQ_TYPE_SALES));
	}

	public function get($slug, $app_id)
	{
                
		return self::_fetchRow('
			SELECT * FROM faq WHERE id = ? AND application_id = ? AND (FIND_IN_SET(?, type) OR FIND_IN_SET(?, type))
		', array($slug, $app_id, Model_System_Faq::FAQ_TYPE_ADMIN, Model_System_Faq::FAQ_TYPE_SALES));
	}
        
        
        
        public function getPhoto($id)
	{
		$sql = "
			SELECT 
				*
				
			FROM faq_photos
			WHERE wiki_id = ?
                        AND status = 1
		";
		
		return $this->getAdapter()->fetchAll($sql, array($id));
	}
        
        
        
        public function countPhotos($id) {
            $sql = "
			SELECT 
				*
				
			FROM faq_photos
			WHERE id = ?
                        AND status = 1
		";
            return count($this->getAdapter()->fetchAll($sql, array($id)));
        }
        
        
        public function addPhotos($data)
	{
		$this->_db->insert($this->_table_photo, $data);
		//Cubix_SyncNotifier::notify($data['wiki_id'], Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('action' => 'EURO LOTTERY Photo Added  '));
	}
        
        
        public function insert(Model_System_FaqItem $item)
	{
            
		self::getAdapter()->insert($this->_table, (array)$item);	
	}
	
	public function update(Model_System_FaqItem $item)
	{
		self::getAdapter()->update($this->_table, (array)$item, self::getAdapter()->quoteInto('id = ?', $item['id']));
	}
        
        public function toggle($id)
	{
		parent::getAdapter()->update($this->_table, array(
			'status' => new Zend_Db_Expr('NOT status')
		), parent::quote('id = ?', $id));
	}
        
        public function remove($id, $app_id)
	{
		parent::remove(array(self::quote('id = ?', $id), self::quote('application_id = ?', $app_id)));
	}
        
        public function updatePhoto(Model_System_FaqItem $item)
	{
		self::getAdapter()->update($this->_table_photo, (array)$item);
	}
        
        public function removePhotos($ids)
	{
            
            return self::getAdapter()->query('UPDATE ' . $this->_table_photo . ' SET status = ' . self::STATUS_REMOVED . ' WHERE id IN (' . implode(',', $ids) . ')');
            
	}
}
