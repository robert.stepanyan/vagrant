<?php

class Model_Dublicities extends Cubix_Model
{	
	public static function getList($params = array(), $page = 1, $limit = 10, &$count = 0)
	{
		$db = self::db();
		
		$where = '';
		$join = '';
		
		if (strlen(trim($params['ip'])) > 0)
		{
			$join .= " LEFT JOIN users_last_login u ON s_w.user_id = u.user_id";
			$where .= " AND u.ip = '" . $params['ip'] . "'";
		}
		
		if (strlen(trim($params['member'])) > 0)
		{
			$join .= " LEFT JOIN users u ON s_w.user_id = u.id";
			$where .= " AND u.username = '" . $params['member'] . "'";
		}
		
		if (strlen(trim($params['escort'])) > 0)
		{
			$join .= " LEFT JOIN escorts e ON s_w.user_id = e.user_id";
			$where .= " AND e.showname = '" . $params['escort'] . "'";
		}
		
		if (strlen(trim($params['agency'])) > 0)
		{
			$join .= " LEFT JOIN agencies a ON s_w.user_id = a.user_id";
			$where .= " AND a.name = '" . $params['agency'] . "'";
		}
		
		//$join .= " INNER JOIN client_cookie cc ON s_w.user_id = cc.user_id";
		
		$users = $db->query("
			SELECT SQL_CALC_FOUND_ROWS s_w.user_id 
			FROM suspicious_warning s_w " . $join . " 
			WHERE s_w.date > DATE_ADD(NOW(), INTERVAL -30 DAY) " . $where . "
			GROUP BY s_w.user_id 
			ORDER BY s_w.date DESC
			LIMIT " . (($page - 1) * $limit) . ", " . $limit . "
		")->fetchAll();
		
		
		
		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');
		
		$arr = array();
		
		foreach ($users as $user)
		{
			$w = $db->query('
				SELECT s_w.id, s_w.date, u.username, u.user_type 
				FROM suspicious_warning s_w 
				INNER JOIN users u ON u.id = s_w.user_id 
				WHERE s_w.user_id = ? 
				ORDER BY s_w.date DESC
			', $user->user_id)->fetch();
					
			$items = $db->query('
				SELECT s.user_id, u.username, u.user_type, u.status 
				FROM suspicious_warning_user s 
				INNER JOIN users u ON u.id = s.user_id 
				WHERE s.type = 1 AND s.warning_id = ?
			', $w->id)->fetchAll();
			
			if ($items)
			{
				$ip = $db->fetchOne('SELECT ip FROM users_last_login WHERE user_id = ? ORDER BY login_date DESC', $user->user_id);

				switch ($w->user_type)
				{
					case 'escort':
						$e = $db->query('SELECT showname, id FROM escorts WHERE user_id = ?', $user->user_id)->fetch();
						$arr[$w->id]['name'] = $e->showname;
						$arr[$w->id]['escort_id'] = $e->id;
						break;
					case 'agency':
						$ag = $db->query('SELECT name, slug FROM agencies WHERE user_id = ?', $user->user_id)->fetch();
						$arr[$w->id]['name'] = array('name' => $ag->name, 'slug' => $ag->slug);
						break;
					default:
						$arr[$w->id]['name'] = null;
				}

				$arr[$w->id]['w_date'] = $w->date;
				$arr[$w->id]['ip'] = $ip;
				$arr[$w->id]['from_user_id'] = $user->user_id;
				$arr[$w->id]['from_user_username'] = $w->username;
				$arr[$w->id]['from_user_type'] = $w->user_type;
				$arr[$w->id]['users'] = $items;
			}
			else
				continue;
		}
		
		return $arr;
	}
	
	public function getForUserCount($user_id)
	{
		$db = self::db();
		
		$w = $db->query('SELECT s_w.id FROM suspicious_warning s_w WHERE s_w.user_id = ? ORDER BY s_w.date DESC', $user_id)->fetch();
		
		return $db->fetchOne('SELECT COUNT(s.user_id) FROM suspicious_warning_user s WHERE s.type = 1 AND s.warning_id = ?', $w->id);
	}
	
	public function getForUser($user_id)
	{
		$db = self::db();
		
		$w = $db->query('SELECT s_w.id FROM suspicious_warning s_w WHERE s_w.user_id = ? ORDER BY s_w.date DESC', $user_id)->fetch();
		
		$users = $db->query('SELECT s.user_id FROM suspicious_warning_user s WHERE s.type = 1 AND s.warning_id = ? GROUP BY s.user_id', $w->id)->fetchAll();
		$uids = array();
		
		foreach ($users as $user)
			$uids[] = $user->user_id;
				
		$uids_str = implode(',', $uids);
		
		return $db->query('
			SELECT e.id AS escort_id, e.showname, u.username, u.user_type, u.id AS user_id, a.id AS agency_id, a.name 
			FROM users u
			LEFT JOIN escorts e ON e.user_id = u.id
			LEFT JOIN agencies a ON a.user_id = u.id
			WHERE u.id IN (' . $uids_str . ')
			GROUP BY u.id 
			ORDER BY u.user_type
		')->fetchAll();
	}
}
