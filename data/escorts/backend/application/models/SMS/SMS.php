<?php

class Model_SMS_SMS extends Cubix_Model
{
	
	protected $_table = 'sms';
	protected $_itemClass = 'Model_SMS_SMSItem';
	
	// SMS
	const SMS_TYPE_INCOMING = 1;
	const SMS_TYPE_OUTGOING = 2;
	
	const SMS_IS_READ = 1;
	const SMS_IS_NOT_READ = 0;
	/*const SMS_ADMIN_USER_ID = -99;
	const SMS_MULTIPLE_USERS_FOUND = -1;
	const SMS_USER_NOT_FOUND = 0;*/
	
	const SMS_STATUS_UNKNOWN = 0;//The message is in the queue to be sent.
	const SMS_STATUS_SUCCESS = 1;//The message was sent and the carrier acknowledged the deliver.
	const SMS_STATUS_FAILURE = 2; //An error occurred during the comunication with the carrier and the message was not sent.
	const SMS_STATUS_BUFFERED = 3;

	// smsdigital new sms statuses
    const SMS_STATUS_SENT = 4; //The message was sent, but the carrier have yet to acknowledge the delivery.
    const SMS_STATUS_NOT_DELIVERED = 5; //The carrier could not send the message for techinical reasons, e.g.: number does not exist.
    const SMS_STATUS_BLACKLIST = 6; //The message was not sent because the recipient was blacklisted. Incurs no charges.
    const SMS_STATUS_INVALID = 7; //The message was not set because the recipient is either invalid, fixed telephone or nonexistent. Incurs no charges.
    const SMS_STATUS_QUOTA_EXCEEDED = 8; //Max weekly quota of 5 messages per recipient exceeded. Incurs no charge.
    const SMS_STATUS_NO_ACKNOWLEGDMENT = 9; //The message was sent to a carrier without delivery acknowledgment, e.g.: OI.
    const SMS_STATUS_SPAM = 10; //The message was considered spam. Incurs no charge.
    const SMS_STATUS_DUPLICATE = 11; //The same message was sent to the same recipient during a period of 12 hours. Incurs no charge.
	const SMS_STATUS_DELETED = 99;

	const SMS_STATUS_POSTPONED = 166;

    public static $STATUS_TEXTS = array(
        self::SMS_STATUS_UNKNOWN => "Pending",
        self::SMS_STATUS_SUCCESS => "Delivered",
        self::SMS_STATUS_FAILURE => "Delivery Fail",
        self::SMS_STATUS_SENT => "SENT",
        self::SMS_STATUS_NOT_DELIVERED => "Not DELIVERED",
        self::SMS_STATUS_INVALID => "Invalid Phone",
        self::SMS_STATUS_BLACKLIST => "BLACKLIST",
        self::SMS_STATUS_QUOTA_EXCEEDED => "QUOTA EXCEEDED",
        self::SMS_STATUS_NO_ACKNOWLEGDMENT => "NO ACKNOWLEGDMENT",
        self::SMS_STATUS_SPAM => "SPAM",
        self::SMS_STATUS_DUPLICATE => "DUPLICATE",

        self::SMS_STATUS_BUFFERED => "BUFFERED",
        self::SMS_STATUS_DELETED => "DELETED",
        self::SMS_STATUS_POSTPONED => "POSTPONED"
    );

	static function get($id) {
		//return self::getAdapter()->fetchOne('SELECT TRUE FROM sms_outbox WHERE msg_id_2sms = ?', $id);
		return self::getAdapter()->fetchOne('SELECT TRUE FROM sms_outbox WHERE id = ?', $id);
		//$result = mysql_query('SELECT 1 FROM sms_outbox WHERE id = ' . $id);
	}

    /**
     * @param $identificationNumber
     * @return string
     */
    public function getByIdentificationNumber($identificationNumber) {
        return self::getAdapter()->fetchOne('SELECT id FROM sms_outbox WHERE identificationNumber = ? ORDER BY id DESC LIMIT 1', $identificationNumber);
	}

	public function updateStatus($id, $status) 
	{
		self::getAdapter()->query('UPDATE sms_outbox SET status = ' . intval($status) . ' WHERE id = ' . $id);
	}
	public function updateSpam($id, $spam)
	{
		self::getAdapter()->query('UPDATE sms_outbox SET is_spam = ? WHERE id = ?', array(intval($spam), intval($id)));
	}

	public function updateDeliveryDate($id, $date) {
		//$this->getAdapter()->query('UPDATE sms_outbox SET delivery_date = FROM_UNIXTIME(' . intval($date) . ') WHERE id = ' . $id);
		//self::getAdapter()->update('sms_outbox', array('delivery_date' => new Zend_Db_Expr('FROM_UNIXTIME(' . intval($date) . ')') ), self::getAdapter()->quoteInto('msg_id_2sms = ?', $id));
		self::getAdapter()->update('sms_outbox', array('delivery_date' => new Zend_Db_Expr('FROM_UNIXTIME(' . intval($date) . ')') ), self::getAdapter()->quoteInto('id = ?', $id));
	}
	
	public function toggleIsRead($type, $set_as, $ids)
	{
		$table = '';
		switch( $type )
		{
			case 'inbox':
				$table = 'sms_inbox';
			break;
			case 'saved':
				$table = 'sms_saved';	
			break;
			case 'trash':
				$table = 'sms_trash';	
			break;
		}
		
		$sql = "UPDATE {$table} SET is_read = {$set_as} WHERE id = ?";
		
		foreach ( $ids as $id )
		{
			self::getAdapter()->query($sql, $id);
		}
	} 
	
	public function getTemplates($sales_user_id, $user_type = '')
	{
	    $where = '';

        if (in_array(Cubix_Application::getId(), array(APP_ED,APP_EG_CO_UK)) && strlen($user_type) )
            $where .= 'AND user_type LIKE "%'.$user_type.'%"';

		if (is_null($sales_user_id))
			return self::getAdapter()->query('SELECT * FROM sms_templates WHERE 1 '.$where.' ORDER BY title ASC')->fetchAll();
		else
			return self::getAdapter()->query('SELECT * FROM sms_templates WHERE sales_user_id = ? '.$where.' ORDER BY title ASC', $sales_user_id)->fetchAll();
	}
	
	public function getTemplate($template_id)
	{
		return self::getAdapter()->fetchOne('SELECT template FROM sms_templates WHERE id = ?', $template_id);
	}
	
	public static function encodeString($string)
	{
		$range = array(38, 60, 62);
		$range = array_merge($range, range(128, 255));

		$string = mb_convert_encoding($string, 'ISO-8859-1');
		$new_string = '';
//echo mb_internal_encoding();
//$new_string = str_replace('ü', '&#252;', $string);
		for ( $i = 0; $i < mb_strlen($string, 'ISO-8859-1'); ++$i ) 
		{
			$letter = mb_substr($string, $i, 1, 'ISO-8859-1');
			$ascii = (int) ord($letter);
//var_dump($letter);
			if ( in_array($ascii, $range) ) 
			{
//echo sprintf('%c', 252);
//var_dump($string, $letter, $ascii);die;
				$new_string .= '&#' . $ascii . ';';
			}
			else 
			{
				$new_string .= $letter;
			}
		}
//echo $new_string;die;
		return $new_string;
	}

    public static function getPhoneByIdentificationNumber($identificationNumber) {
        return self::getAdapter()->fetchOne('SELECT phone_to FROM sms_outbox WHERE identificationNumber = ? ', $identificationNumber);
	}
}
