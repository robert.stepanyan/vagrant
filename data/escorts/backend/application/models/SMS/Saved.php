<?php

class Model_SMS_Saved extends Cubix_Model
{
	
	protected $_table = 'sms_saved';
	//protected $_itemClass = 'Model_SMS_SMSItem';
	
	public function get($id)
	{
		$sql = '
			SELECT
				si.id, UNIX_TIMESTAMP(si.date) AS date, e.showname, ep.contact_phone_parsed, si.phone_from, u.status, bu.username AS sales_person,
				si.text, si.is_read
			FROM sms_saved si
			LEFT JOIN escort_profiles_v2 ep ON ep.escort_id = si.escort_id
			LEFT JOIN escorts e ON ep.escort_id = e.id			
			LEFT JOIN users u ON u.id = e.user_id
			LEFT JOIN backend_users bu ON bu.id = u.sales_user_id
			WHERE si.id = ?
		';
		
		return parent::_fetchRow($sql, $id);
	}
	
	public function getAll($p, $pp, $sort_field, $sort_dir, $filter, &$count)
	{
		$sql = '
			SELECT
				si.id, UNIX_TIMESTAMP(si.date) AS date, e.showname, ep.contact_phone_parsed, si.phone_from, u.status, bu.username AS sales_person,
				IF(LENGTH(si.text) > 30, CONCAT(SUBSTRING(si.text, 1, 30), \' ...\'), si.text) AS text, si.is_read, e.id AS escort_id
			FROM sms_saved si
			LEFT JOIN escort_profiles_v2 ep ON ep.escort_id = si.escort_id
			LEFT JOIN escorts e ON ep.escort_id = e.id			
			LEFT JOIN users u ON u.id = e.user_id
			LEFT JOIN backend_users bu ON bu.id = u.sales_user_id
			WHERE 1
		';
		
		$countSql = '
			SELECT
				COUNT(si.id)
			FROM sms_saved si
			LEFT JOIN escorts e ON si.escort_id = e.id
			LEFT JOIN users u ON u.id = e.user_id
			WHERE 1
		';
		
		$where = '';
		
		if ( strlen($filter['s.status']) ) {
			$where .= self::quote('AND s.status = ?', $filter['s.status']);
		}
		if ( $filter['date_from'] && $filter['date_to'] ) {
				if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) ) {
					$where .= self::quote( " AND DATE(si.date) = DATE(FROM_UNIXTIME(?))" , $filter['date_from']);
				}
			}
		if ( $filter['date_from'] ) {
				$where .= self::quote(" AND DATE(si.date) >= DATE(FROM_UNIXTIME(?)) ", $filter['date_from']);
			}

		if ( $filter['date_to'] ) {
			$where .= self::quote(" AND DATE(si.date) <= DATE(FROM_UNIXTIME(?)) ", $filter['date_to']);
		}
		
		if ( $filter['application_id'] ) {
			$where .= self::quote(" AND si.application_id = ?", $filter['application_id']);
		}
		
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
		';
		
		$count = $this->getAdapter()->fetchOne($countSql);
		return parent::_fetchAll($sql);
	}
	
	public function delete($ids)
	{
		$sql = "
			INSERT INTO sms_trash (type, escort_id, phone_from, phone_to, `date`, text, is_read, application_id) 
				(SELECT " . Model_SMS_SMS::SMS_TYPE_INCOMING . ", escort_id, phone_from, phone_to, `date`, text, is_read, application_id  
			FROM sms_saved 
			WHERE id = ?)
		";
		
		foreach ( $ids as $id )
		{
			$this->getAdapter()->query($sql, $id);
		
			parent::remove(self::quote('id = ?', $id));
		}
	}
}
