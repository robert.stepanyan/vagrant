<?php

class Model_SMS_Inbox extends Cubix_Model
{
	
	protected $_table = 'sms_inbox';
	//protected $_itemClass = 'Model_SMS_SMSItem';

	public function getThread($phone_number)
	{
		$sql = '
			SELECT x.id, x.date, x.showname, x.contact_phone_parsed, x.phone, x.sales_person, x.text, x.dest,x.is_delivered
			FROM ((
				SELECT
					si.id, UNIX_TIMESTAMP(si.date) AS date, e.showname, ep.contact_phone_parsed, si.phone_from AS phone, bu.username AS sales_person,
					si.text, "YOU" AS dest ,null as is_delivered
				FROM sms_inbox si
				LEFT JOIN escort_profiles_v2 ep ON ep.escort_id = si.escort_id
				LEFT JOIN escorts e ON ep.escort_id = e.id			
				LEFT JOIN users u ON u.id = e.user_id
				LEFT JOIN backend_users bu ON bu.id = u.sales_user_id
				WHERE si.phone_from = ?
			) UNION (
				SELECT
					so.id, UNIX_TIMESTAMP(so.date) AS date, e.showname, ep.contact_phone_parsed, so.phone_to AS phone, bu.username AS sales_person,
					so.text, "ME" AS dest,if(so.delivery_date,1,0) as is_delivered
				FROM sms_outbox so
				LEFT JOIN escort_profiles_v2 ep ON ep.escort_id = so.escort_id
				LEFT JOIN escorts e ON ep.escort_id = e.id			
				LEFT JOIN users u ON u.id = e.user_id
				LEFT JOIN backend_users bu ON bu.id = so.sender_sales_person
				WHERE so.phone_to = ?
			)) x
			ORDER BY x.date ASC
		';
		
		return parent::_fetchAll($sql, array($phone_number, $phone_number));
	}
	
	public function get($id)
	{
		$sql = '
			SELECT
				si.id, UNIX_TIMESTAMP(si.date) AS date, e.showname, ep.contact_phone_parsed, si.phone_from, u.status, bu.username AS sales_person,
				si.text, si.is_read
			FROM sms_inbox si
			LEFT JOIN escort_profiles_v2 ep ON ep.escort_id = si.escort_id
			LEFT JOIN escorts e ON ep.escort_id = e.id			
			LEFT JOIN users u ON u.id = e.user_id
			LEFT JOIN backend_users bu ON bu.id = u.sales_user_id
			WHERE si.id = ?
		';
		
		return parent::_fetchRow($sql, $id);
	}
	
	public function getEscortsByPhone($phone, &$data)
	{
		$sql = '
			SELECT
				ep.escort_id, ep.showname, e.status, bu.username
			FROM escort_profiles_v2 ep
			INNER JOIN escorts e ON ep.escort_id = e.id			
			LEFT JOIN users u ON u.id = e.user_id
			LEFT JOIN backend_users bu ON bu.id = u.sales_user_id
			WHERE ep.contact_phone_parsed = ?
		';
		
		$escorts = parent::_fetchAll($sql, $phone);
		
		if ( $escorts ) {
			$DEFINITIONS = Zend_Registry::get('defines');
			$e_model = new Model_EscortsV2();
			
			if ( count($escorts) == 1 ) {
				$data->showname = $escorts[0]->showname;
				$data->sales_person = $escorts[0]->username;
				$stat = array();
				foreach( $DEFINITIONS['escort_status_options'] as $key => $e_status )
				{
					if ( $e_model->hasStatusBit($escorts[0]->escort_id, $key) )
					{
						$stat[] = $e_status;
					}
				}

				$data->status = implode(', ', $stat);
			}
			else {
				$escorts_str = "";
				foreach ( $escorts as $k => $escort ) {
					
					$escorts_str .= $escort->showname . "<br/>";
					
					if ( $k == 9 ) break;
					
				}
				
				$data->showname = $escorts_str;
			}
		}
	}
	
	public function getAll($p, $pp, $sort_field, $sort_dir, $filter, &$count)
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				si.id, UNIX_TIMESTAMP(si.date) AS date, si.phone_from, si.for_review, si.is_sent,
				IF(LENGTH(si.text) > 30, CONCAT(SUBSTRING(si.text, 1, 30), \' ...\'), si.text) AS text, si.is_read, e.id AS escort_id,
				e.showname, e.status, bu.username AS sales_person
			FROM sms_inbox si
			/*LEFT JOIN escort_profiles_v2 ep ON ep.contact_phone_parsed = si.phone_from*/
			LEFT JOIN escorts e ON si.escort_id = e.id 
			LEFT JOIN users u ON u.id = e.user_id
			LEFT JOIN backend_users bu ON bu.id = u.sales_user_id
			WHERE si.date IN (
				SELECT MAX(si.date)
			    FROM sms_inbox si
			    GROUP BY si.phone_from
			)
		';
		
		/*$countSql = '
			SELECT
				COUNT(si.id)
			FROM sms_inbox si
			LEFT JOIN escort_profiles_v2 ep ON ep.contact_phone_parsed = si.phone_from
			LEFT JOIN escorts e ON si.escort_id = e.id
			LEFT JOIN users u ON u.id = e.user_id
			WHERE 1
		';*/
		
		$where = '';
		
		if ( strlen($filter['s.status']) ) {
			$where .= self::quote('AND s.status = ?', $filter['s.status']);
		}
		
		if ( strlen($filter['for_review']) ) {
			$where .= self::quote('AND si.for_review = ?', $filter['for_review']);
		}
		
		if ( strlen($filter['reply']) ) {
			$where .= self::quote(' AND si.is_sent = ?', $filter['reply']);
		}
		
		if ( $filter['date_from'] && $filter['date_to'] ) {
				if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) ) {
					$where .= self::quote( " AND DATE(si.date) = DATE(FROM_UNIXTIME(?))" , $filter['date_from']);
				}
			}
		if ( $filter['date_from'] ) {
				$where .= self::quote(" AND DATE(si.date) >= DATE(FROM_UNIXTIME(?)) ", $filter['date_from']);
			}

		if ( $filter['date_to'] ) {
			$where .= self::quote(" AND DATE(si.date) <= DATE(FROM_UNIXTIME(?)) ", $filter['date_to']);
		}
		
		if ( $filter['showname'] ) {
			$where .= " AND e.showname LIKE '" . $filter['showname'] . "%'";
		}
		
		if ( $filter['application_id'] ) {
			$where .= self::quote(" AND si.application_id = ?", $filter['application_id']);
		}
		
		if ( $filter['escort_id'] ) {
			$where .= " AND e.id = " . $filter['escort_id'];
		}
		
		if ( $filter['sales_user_id'] ) {
			$where .= " AND bu.id = " . $filter['sales_user_id'];
		}
		
		if ( $filter['phone_from'] ) {
			$where .= " AND si.phone_from LIKE '%" . $filter['phone_from'] . "%'";
		}
		
		$sql .= $where;
		
		$countSql = 'SELECT FOUND_ROWS()';
		
		$sql .= '
			GROUP BY si.phone_from
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
		';
		$items = parent::_fetchAll($sql);
		
		$count = $this->getAdapter()->fetchOne($countSql);
		
		return $items;
	}
	
	public function delete($ids)
	{
		$sql = "
			INSERT INTO sms_trash (type, escort_id, phone_from, phone_to, `date`, text, is_read, application_id) 
				(SELECT " . Model_SMS_SMS::SMS_TYPE_INCOMING . ", escort_id, phone_from, phone_to, `date`, text, is_read, application_id  
			FROM sms_inbox
			WHERE id = ?)
		";
		
		foreach ( $ids as $id )
		{
			$this->getAdapter()->query($sql, $id);
		
			parent::remove(self::quote('id = ?', $id));
		}
	}

	public function saveToInbox($data)
	{
		$sms_data = array(
			//'escort_id' => getEscortByPhone,
			'phone_from' => $data['orig'],
			'phone_to' => $data['recipient'],
			'date' => $data['date'],
			'text' => $data['text'],
			'application_id' => $data['application_id']
		);
		
		$c1 = preg_match('/^[0-9a-f]{5}(\s|\.|,|:|;)/i', $data['text']);
		$c2 = preg_match('/(\s|\.|,|:|;)[0-9a-f]{5}(\s|\.|,|:|;)/i', $data['text']);
		$c3 = preg_match('/(\s|\.|,|:|;)[0-9a-f]{5}$/i', $data['text']);
		
		$for_review = 0;
		
		if ($c1 || $c2 || $c3)
			$for_review = 1;
		
		$sms_data = array_merge($sms_data, array('for_review' => $for_review));
		
		$escort_id = $this->getAdapter()->fetchOne('
			SELECT p.escort_id 
			FROM escort_profiles_v2 p 
			INNER JOIN escorts e ON e.id = p.escort_id /*AND !(e.status & 256)*/
			WHERE p.contact_phone_parsed = ?
		', $data['orig']);
		if ( $escort_id ) {
			$sms_data = array_merge($sms_data, array('escort_id' => $escort_id));
		}
		
		$this->getAdapter()->insert('sms_inbox', $sms_data);
	}

	public function save($ids)
	{
		$sql = "
			INSERT INTO sms_saved (escort_id, phone_from, phone_to, `date`, text, is_read, application_id) 
				(SELECT escort_id, phone_from, phone_to, `date`, text, is_read, application_id  
			FROM sms_inbox
			WHERE id = ?)
		";
		
		foreach ( $ids as $id )
		{
			$this->getAdapter()->query($sql, $id);
		
			parent::remove(self::quote('id = ?', $id));
		}
	}
	
	public function toggle($id)
	{		
		$field = 'for_review';
		
		parent::getAdapter()->update('sms_inbox', array(
			$field => new Zend_Db_Expr('NOT ' . $field)
		), parent::quote('id = ?', $id));
	}
	
	public function sent($id)
	{		
		parent::getAdapter()->update($this->_table, array(
			'is_sent' => 1 
		), parent::quote('id = ?', $id));
	}
	
}
