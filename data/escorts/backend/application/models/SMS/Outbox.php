<?php

class Model_SMS_Outbox extends Cubix_Model
{
	
	protected $_table = 'sms_outbox';
	//protected $_itemClass = 'Model_SMS_SMSItem';
	
	public function get($id)
	{
		$fields = '';
		
		if (Cubix_Application::getId() == APP_EF){
			$fields = ', so.pass';
		}
		
		$sql = '
			SELECT
				so.id, UNIX_TIMESTAMP(so.date) AS date, e.showname, ep.contact_phone_parsed, so.phone_to, so.phone_from, u.status, bu.username AS sales_person,
				so.text, so.status AS sms_status, UNIX_TIMESTAMP(so.delivery_date) AS delivery_date, so.escort_ids' . $fields. '
			FROM sms_outbox so
			LEFT JOIN escort_profiles_v2 ep ON ep.escort_id = so.escort_id
			LEFT JOIN escorts e ON ep.escort_id = e.id			
			LEFT JOIN users u ON u.id = e.user_id
			LEFT JOIN backend_users bu ON bu.id = u.sales_user_id
			WHERE so.id = ?
		';

		return parent::_fetchRow($sql, $id);
	}
	
	public function getEscortsByPhoneOrId($phone, $escort_id, &$data)
	{
		if($escort_id){
			$sql = '
				SELECT
					ep.escort_id, ep.showname, e.status, bu.username, e.user_id, ep.contact_phone_parsed 
				FROM escorts e
				INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
				INNER JOIN users u ON u.id = e.user_id
				LEFT JOIN backend_users bu ON bu.id = u.sales_user_id
				WHERE e.id = ?
			';
			$escorts = parent::_fetchAll($sql, $escort_id);
		}
		else{
			$sql = '
				SELECT
					ep.escort_id, ep.showname, e.status, bu.username, e.user_id, ep.contact_phone_parsed  
				FROM  escort_profiles_v2 ep
				INNER JOIN escorts e ON ep.escort_id = e.id	
				INNER JOIN users u ON u.id = e.user_id
				LEFT JOIN backend_users bu ON bu.id = u.sales_user_id
				WHERE contact_phone_parsed = ?
			';
			$escorts = parent::_fetchAll($sql, $phone);
		}
		
		
		
		if ( $escorts ) {
			$DEFINITIONS = Zend_Registry::get('defines');
			
			$e_model = new Model_EscortsV2();
			
			if ( count($escorts) == 1 ) {
			  
				$data->showname = "<span class='escort-popup'><a href='#".$escorts[0]->escort_id."'>".$escorts[0]->showname. " (" . $escorts[0]->escort_id . ")</a></span>";
				$data->sales_person = $escorts[0]->username;

				$stat = array();
				foreach( $DEFINITIONS['escort_status_options'] as $key => $e_status )
				{
					if ( $e_model->hasStatusBit($escorts[0]->escort_id, $key) )
					{
						$stat[] = $e_status;
					}
				}
				
				if($data->phone_to != $escorts[0]->contact_phone_parsed){
					$data->phone_to = "<span class='red'>". $data->phone_to. "</span><br> (Current number) <br> <span class='green'>". $escorts[0]->contact_phone_parsed . "</span>";
				}
				
				$data->status = implode(', ', $stat);
			}
			else {
				$uids = array();
				
				foreach ($escorts as $escort)
				{
					$uid = intval($escort->user_id);
					
					if (!in_array($uid, $uids))
						$uids[] = $uid;
				}
				
				$escorts_str = "";
				
				if (count($uids) == 1 && !in_array(0, $uids))
				{
					//agency
					
					$name = parent::getAdapter()->fetchOne('SELECT name FROM agencies WHERE user_id = ?', $uids[0]);
					
					$escorts_str = $name . ' <strong>(Agency)</strong>';
				}
				else
				{
					$escorts_str = "<span class='escort-popup'>";
					foreach ( $escorts as $k => $escort ) 
					{
						$escorts_str .= "<a href='#". $escort->escort_id."'>" . $escort->showname . "</a><br/>";

						if ( $k == 9 ) break;
					}
					$escorts_str .= "</span>";
				}
//				print_r($data);
				$data->showname = $escorts_str;
			}
		}
	}
	
	public function getAll($p, $pp, $sort_field, $sort_dir, $filter, &$count)
	{
		$sql = array(
			'tables' => array('sms_outbox so'),
			'fields' => array(
                'so.id, UNIX_TIMESTAMP(so.date) AS date, so.phone_to, bu2.username AS sender_sales_person,
				IF(LENGTH(so.text) > 30, CONCAT(SUBSTRING(so.text, 1, 30), \' ...\'), so.text) AS text, so.status AS sms_status, 
				UNIX_TIMESTAMP(so.delivery_date) AS delivery_date, so.escort_id'
			),
			'joins' => array(
				'LEFT JOIN backend_users bu2 ON bu2.id = so.sender_sales_person'
				),
			'where' => array(),
			'order' => array($sort_field, $sort_dir),
			'page' => array($p, $pp)
		);
		
		$where = [];
		
		if ( $filter['date_from'] && $filter['date_to'] ) {
				if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) ) {
					$where[] = self::quote( " DATE(so.date) = DATE(FROM_UNIXTIME(?))" , $filter['date_from']);
				}
			}
		if ( $filter['date_from'] ) {
				$where[] = self::quote(" DATE(so.date) >= DATE(FROM_UNIXTIME(?)) ", $filter['date_from']);
			}

		if ( $filter['date_to'] ) {
			$where[] = self::quote(" DATE(so.date) <= DATE(FROM_UNIXTIME(?)) ", $filter['date_to']);
		}
		
		if ( $filter['showname'] ) {
			$where[] = " e.showname LIKE '" . $filter['showname'] . "%'";
		}
		
		if ( $filter['escort_id'] ) {
			//$filter['phone_to'] = Model_EscortsV2::getPhoneParsedById($filter['escort_id']);
			$where[] = " so.escort_id = " . $filter['escort_id'];
		}
		
		if($filter['only_this_user']){ // ONLY FOR EF AND SALES USERS
			$where[] = " (so.sender_sales_person = " . $filter['only_this_user']. " OR u.sales_user_id = " . $filter['only_this_user'] . ")";
		}
		elseif( $filter['sender_sales_person'] ){
			$where[] = " so.sender_sales_person = " . $filter['sender_sales_person'];
		}
		
		if ( $filter['e_status'] ) {
			$where[] = " e.status & " . $filter['e_status'];
		}
		
		if ( strlen($filter['status']) > 0 ) {
			$where[] = " so.status = " . $filter['status'];
		}
		
		if ( $filter['phone_to'] ) {
			$phone_to = str_replace(' ', '', $filter['phone_to']);
			$where[] = " so.phone_to LIKE '%" . $phone_to . "%'";
		}
		
		if ( $filter['is_spam'] ) {
			$where[] = " is_spam = 1";
		} else {
			$where[] = " is_spam = 0";
		}
		
		if ( $filter['year'] ) {
			$where[] = " so.year = " . $filter['year'];
		}
		
		if ( $filter['skype_video_call'] ) {
			$where[] = " so.is_video_call = 1";
		}
		
		$sql['where'] = $where;
		
		// JOIN TABLES ---------------------------------------
		
		if ( $filter['showname'] || $filter['e_status']) {
			$sql['joins'][] = 'INNER JOIN escorts e ON so.escort_id = e.id';
		}
		
		if($filter['only_this_user']){
			$sql['joins'][] = 'LEFT JOIN escorts e ON so.escort_id = e.id';
			$sql['joins'][] = 'LEFT JOIN users u ON u.id = e.user_id';
		}
		
		$dataSql = Cubix_Model::getSql($sql, true);
		
		$items = $this->getAdapter()->fetchAll($dataSql);
		
		$count = $this->getAdapter()->fetchOne('SELECT FOUND_ROWS()');
		
		return $items;
	}                                   
	
	public function save($data)
	{
		$escort_id = null;
		$escort_ids = null;
		if ( count($data['e_ids']) > 1 )
		{
			foreach($data['e_ids'] as $e_id)
			{
				$escort_ids[] = $e_id;	
			}
			$escort_ids = implode(',', $escort_ids);
		}		
		elseif ( count($data['e_ids']) == 1 )
			$escort_id = $data['e_ids'][0];
		
		if ( $data['escort_id'] ) {
			$escort_id = $data['escort_id'];
		}
		if (Cubix_Application::getId() != APP_6B){
            if (Cubix_Application::getId() == APP_ED){
                $sql = 'INSERT INTO sms_outbox (escort_id, escort_ids, phone_to, phone_from, text, application_id, sender_sales_person, is_spam, year , lang ) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
            }else{
                $sql = 'INSERT INTO sms_outbox (escort_id, escort_ids, phone_to, phone_from, text, application_id, sender_sales_person, is_spam, year ) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)';
            }
        }else{
            $sql = 'INSERT INTO sms_outbox (escort_id, escort_ids, phone_to, phone_from, text, application_id, sender_sales_person, is_spam, year, identificationNumber) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
        }

		
		if ( is_array($data['phone_number']) ){
			$i = 0;
			foreach ($data['phone_number'] AS $phone)
			{
			    $bind = array($data['e_ids'][$i], $escort_ids, $phone, $data['phone_from'], $data['text'], $data['application_id'], $data['sender_sales_person'], 0 , date("Y"));
			    if (Cubix_Application::getId() == APP_6B){
			        array_push($bind, $data['identificationNumber']);
                }elseif (Cubix_Application::getId() == APP_ED){
                    array_push($bind, $data['lang']);
                }
				$this->getAdapter()->query($sql, $bind);
				$i++;
			}
		}
		else{
            $bind = array($escort_id, $escort_ids, $data['phone_number'], $data['phone_from'], $data['text'], $data['application_id'], $data['sender_sales_person'], $data['is_spam'] ? 1 : 0, date("Y"));
            if (Cubix_Application::getId() == APP_6B){
                array_push($bind, $data['identificationNumber']);
            }elseif (Cubix_Application::getId() == APP_ED){
                array_push($bind, $data['lang']);
            }
			$this->getAdapter()->query($sql, $bind);
		}
		return $this->getAdapter()->lastInsertId();
	}

	public function savePostpone($data)
	{
		$escort_id = null;
		$escort_ids = null;
		if ( count($data['e_ids']) > 1 )
		{
			foreach($data['e_ids'] as $e_id)
			{
				$escort_ids[] = $e_id;	
			}
			$escort_ids = implode(',', $escort_ids);
		}		
		elseif ( count($data['e_ids']) == 1 )
			$escort_id = $data['e_ids'][0];
		
		$sql = 'INSERT INTO sms_outbox (escort_id, escort_ids, phone_to, phone_from, text, application_id, sender_sales_person, is_spam, status, exact_send_date) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, FROM_UNIXTIME(' . intval($data['exact_send_date']) . '))';
		
		if ( is_array($data['phone_number']) ){
			$i = 0;
			foreach ($data['phone_number'] AS $phone)
			{
				$this->getAdapter()->query($sql, array($data['e_ids'][$i], $escort_ids, $phone, $data['phone_from'], $data['text'], $data['application_id'], $data['sender_sales_person'], $data['is_spam'] ? 1 : 0, $data['status']));
				$i++;
			}
		}
		else{
			$this->getAdapter()->query($sql, array($escort_id, $escort_ids, $data['phone_number'], $data['phone_from'], $data['text'], $data['application_id'], $data['sender_sales_person'], $data['is_spam'] ? 1 : 0, $data['status']));
		}
		return $this->getAdapter()->lastInsertId();
	}
	
	public function delete($ids)
	{
		$sql = "
			INSERT INTO sms_trash (type, escort_id, phone_from, phone_to, `date`, text, is_read, application_id) 
				(SELECT " . Model_SMS_SMS::SMS_TYPE_OUTGOING . ", escort_id, phone_from, phone_to, `date`, text, 1, application_id  
			FROM sms_outbox
			WHERE id = ?)
		";
		
		foreach ( $ids as $id )
		{
			$this->getAdapter()->query($sql, $id);
		
			parent::remove(self::quote('id = ?', $id));
		}
	}
	
	public function update2SMSData($id, $msg_id = null, $status = 0)
	{
		$data = array();
		if ( $msg_id )
			$data = array_merge ($data, array('msg_id_2sms' => $msg_id));
		
		if ( $status !== false ) 
			$data = array_merge ($data, array('status_2sms' => $status));
		
		$this->getAdapter()->update('sms_outbox', $data, $this->getAdapter()->quoteInto('id = ?', $id));
	}
	
	public function getByIds($ids)
	{
		$sql = "SELECT escort_id, phone_to AS phone_number, phone_from, text FROM " .$this->_table. "  WHERE id IN (".implode(',', $ids).")";
		return $this->getAdapter()->fetchAll($sql);
	}
	
	public function saveAndSend($data)
	{
		$msg_id = $this->save($data);
		
		$config = Zend_Registry::get('system_config');
		$sms_config = $config['sms'];
		$SMS_USERKEY = $sms_config['userkey'];
		$SMS_PASSWORD = $sms_config['password'];
		$SMS_DeliveryNotificationURL = $sms_config['DeliveryNotificationURL'];
		$SMS_NonDeliveryNotificationURL = $sms_config['NonDeliveryNotificationURL'];
		$SMS_BufferedNotificationURL = $sms_config['BufferedNotificationURL'];
		
		$sms = new Cubix_SMS($SMS_USERKEY, $SMS_PASSWORD);

		$sms->setOriginator($data['phone_from']);
		$sms->addRecipient($data['phone_number'], $msg_id);
		$sms->setBufferedNotificationURL($SMS_BufferedNotificationURL);
		$sms->setDeliveryNotificationURL($SMS_DeliveryNotificationURL);
		$sms->setNonDeliveryNotificationURL($SMS_NonDeliveryNotificationURL);

		$sms->setContent($data['text']);

		return $sms->sendSMS();
	}

	public function getAutomaticSmsText($escort, $lng_ = 'en')
    {
        $hasSales = true;
        switch ($escort->bu_id) {
            case 196:
                $bu_email = 'jolyne@escortdirectory.com';
                $bu_phone = '+41767704542';
                break;
            case 197:
                $bu_email = 'carrie@escortdirectory.com';
                $bu_phone = '+41767431209';
                break;
            case 201:
                $bu_email = 'andrew@escortdirectory.com';
                $bu_phone = '+41798071084';
                break;
            case 213:
                $bu_email = 'barbara@escortdirectory.com';
                $bu_phone = '+41767704542';
                break;
            default:
                $hasSales = false;
                $bu_email = 'jolyne@escortdirectory.com';
                $bu_phone = '+41767704542 or +41767431209';
                break;
        }

        $sms_text1 = Cubix_I18n::translateByLng($lng_,'automatic_sms_text_default',array('user_type' => $escort->user_type, 'city' => ucfirst($escort->city)));
        $sms_text2 = Cubix_I18n::translateByLng($lng_,'automatic_sms_text_with_sales',array('user_type' => $escort->user_type,'city' => ucfirst($escort->city),'sales_email' => $bu_email,'phone' => $bu_phone));
        $sms_text3 = Cubix_I18n::translateByLng($lng_,'automatic_sms_text_expires_default',array('user_type' => $escort->user_type, 'city' => ucfirst($escort->city)));
        $sms_text4 = Cubix_I18n::translateByLng($lng_,'automatic_sms_text_expires_with_sales',array('user_type' => $escort->user_type,'city' => ucfirst($escort->city),'sales_email' => $bu_email,'phone' => $bu_phone));
        $sms_text5 = Cubix_I18n::translateByLng($lng_,'automatic_sms_text_with_email_default',array('user_type' => $escort->user_type, 'city' => ucfirst($escort->city)));

        $about11days = ($escort->date_activated <= strtotime("-11day") && $escort->date_activated >= strtotime("-12day"))? true : false;
        if (!$escort->email || $escort->email == 'no@no.com')
        {
            if ($about11days)
            {
                if ($hasSales)
                {
                    return $sms_text4;
                }else{
                    return $sms_text3;
                }
            }else{
                if ($hasSales)
                {
                    return $sms_text2;
                }else{
                    return $sms_text1;
                }
            }
        }else{
            if ($about11days)
            {
                if ($hasSales)
                {
                    return $sms_text4;
                }else{
                    return $sms_text3;
                }
            }else{
                if ($hasSales)
                {
                    return $sms_text2;
                }else{
                    return $sms_text5;
                }
            }
        }

    }

    public function getLastSmsLang($escort_id)
    {
        $sql = "SELECT lang FROM " .$this->_table. "  WHERE escort_id = ".$escort_id.' ORDER BY id DESC LIMIT 1';
        return $this->getAdapter()->fetchOne($sql);
    }
}