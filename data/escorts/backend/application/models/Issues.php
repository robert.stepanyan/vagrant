<?php
class Model_Issues extends Cubix_Model
{
	protected $_table = 'support_tickets';
	protected $_itemClass = 'Model_IssueItem';

	const STATUS_TICKET_OPENED = 1;
	const STATUS_TICKET_CLOSED = 2;	
	
	const STATUS_NEW_UNREAD = 1;
	const STATUS_NOT_REPLIED = 2;
	const STATUS_READ_REPLIED = 3;
	const STATUS_NEW_REPLY = 4;
	const STATUS_ADMIN_SENT = 5;

	const STATUS_TICKET_COMMENT_DEFAULT = 0;
	const STATUS_TICKET_COMMENT_DELETED = 1;

	public static function getUserIdByUserTypeAndId($user_type, $id)
	{
		if ( ! $user_type || ! $id ) return false;
		$db = Zend_Registry::get('db');

		$sql = 'SELECT user_id FROM ' . $user_type . ' WHERE id = ?';
		return $db->fetchOne($sql, $id);
	}

    /**
     * Getting data for the following cases:
     * 1. How many open thickets (at this moment) specified user has
     * 2. How many closed thickets specified user has
     *
     * @param $userId
     * @return mixed
     */
    public function getUserIssuesHistory($userId)
    {
        $result = [];

        // Step One, getting only open/closed tickets count
        // -------------------------------------------
        $_sql = array(
            'tables' => array('support_tickets st'),
            'fields' => array(
                'sum(case when st.status = ' . self::STATUS_TICKET_OPENED . ' then 1 else 0 end) AS openTicketsCount',
                'sum(case when st.status = ' . self::STATUS_TICKET_CLOSED . ' then 1 else 0 end) AS closedTicketsCount',
            ),
            'where' => array(
                'st.user_id = ?' => $userId,
            )
        );

        $sql = self::getSql($_sql);
        $rawData = (array) parent::_fetchRow($sql);

        $result['counts'] = [
            'open' => intval($rawData['openTicketsCount']),
            'closed' => intval($rawData['closedTicketsCount']),
        ];
        // -------------------------------------------


        // Step Two, select fist last ticket's dates
        // -------------------------------------------
        $first = "SELECT
                    creation_date as firstTicketDate,
	                null as lastTicketDate
                FROM
                    support_tickets st 
                WHERE
                    st.user_id = $userId AND st.status = " . self::STATUS_TICKET_CLOSED . " ORDER BY creation_date ASC LIMIT 1";
        $last = "SELECT
                    null as firstTicketDate,
	                creation_date as lastTicketDate
                FROM
                    support_tickets st 
                WHERE
                    st.user_id = $userId AND st.status = " . self::STATUS_TICKET_CLOSED . " ORDER BY creation_date DESC LIMIT 1";

        $rawData = parent::_fetchAll("($first) UNION ($last)");
        $result['dates'] = [
            'firstTicket' => (isset($rawData[0]) && isset($rawData[0]['firstTicketDate'])) ? $rawData[0]['firstTicketDate'] : null,
            'lastTicket' => (isset($rawData[1]) && isset($rawData[1]['lastTicketDate'])) ? $rawData[1]['lastTicketDate'] : null,
        ];
        // -------------------------------------------


        return $result;
    }

	public function get($id)
	{
        $follow_field = '';
	    if(Cubix_Application::getId() == APP_ED){
	        $follow_field = 'st.to_follow_up,';
        }
		$sql = '
			SELECT
				st.id AS id, st.user_id, st.subject,'.$follow_field.'
               (CASE u.user_type
                    WHEN "escort" THEN ES.id
                    WHEN "agency" THEN AG.id
                    WHEN "member" THEN MB.user_id END) as main_id,
				st.message, st.status, UNIX_TIMESTAMP(st.creation_date) AS creation_date,
				UNIX_TIMESTAMP(st.resolve_date) AS resolve_date, st.backend_user_id, u.username,u.user_type as usertype,u.id as user_id, u.sales_user_id
			FROM support_tickets st
			INNER JOIN users u ON u.id = st.user_id
			LEFT JOIN applications ap ON ap.id = u.application_id
            LEFT OUTER JOIN escorts as ES ON ES.user_id = u.id
            LEFT OUTER JOIN agencies as AG ON AG.user_id = u.id
            LEFT OUTER JOIN members as MB ON MB.user_id = u.id
			WHERE 1 AND st.id = ?
		';
		
		$issue = parent::_fetchRow($sql, array($id));
		
		$attached_files = parent::_fetchAll('SELECT * FROM ticket_attached_files WHERE ticket_comment_id IS NULL AND ticket_id = ?
		', $id);
		$data = array('issue'=> $issue , 'attach' => $attached_files  );
		
		return $data;
	}

	public function getComments($ticket_id)
	{
		$fields = '';
		if( Cubix_Application::getId() == APP_ED || Cubix_Application::getId() == APP_BL || Cubix_Application::getId() == APP_EG_CO_UK ){
			$fields = 'tc.read_date AS adm_comment_read_date, ';
		}
		$sql = '
			SELECT
				tc.id AS comment_id, '.$fields.' st.id AS ticket_id, tc.date AS comment_date, tc.comment, u.username, bu.username AS bu_username
			FROM ticket_comments tc
			INNER JOIN support_tickets st ON st.id = tc.ticket_id
			INNER JOIN users u ON u.id = st.user_id
			LEFT JOIN backend_users bu ON bu.id = tc.backend_user_id
			LEFT JOIN applications ap ON ap.id = u.application_id
			WHERE 1 AND st.id = ? 
		';

		
        $sql .= ' AND tc.status = ? ';
        $comments = parent::_fetchAll($sql, array($ticket_id, self::STATUS_TICKET_COMMENT_DEFAULT));

		
		foreach($comments as &$comment)
		{
			$comment['attached_files'] = $this->_db->fetchAll('SELECT * FROM ticket_attached_files WHERE ticket_comment_id = ?
										', $comment['comment_id']);
		}
		
		return $comments;
	}
	public function getLastComments($ticket_id)
	{

		$sql = '
			SELECT 
				DATEDIFF( NOW(),tc.date) as comment_date_diff, bu.type as bu_user_type, tc.id AS comment_id, st.id AS ticket_id, tc.date AS comment_date, tc.comment, u.username, bu.username AS bu_username
			FROM ticket_comments tc
			INNER JOIN support_tickets st ON st.id = tc.ticket_id
			INNER JOIN users u ON u.id = st.user_id
			LEFT JOIN backend_users bu ON bu.id = tc.backend_user_id
			LEFT JOIN applications ap ON ap.id = u.application_id
			WHERE 1 AND st.id = ?  ORDER BY tc.id DESC
		';

        $comment = parent::_fetchRow($sql, $ticket_id);



		return $comment;
	}

	public function addComment($data)
	{
		parent::getAdapter()->insert('ticket_comments', $data);
		return parent::getAdapter()->lastInsertId();
	}
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{

        $bu_user = Zend_Auth::getInstance()->getIdentity();


        $sql_part = '';
        $sql_select_part = '';
        $countSql_part = '';
        if(Cubix_Application::getId() == APP_EF  &&  $bu_user->id == 68){
            $sql_select_part = 'DATEDIFF(NOW(),tc.date) as comment_date_diff, bu.type as bu_user_type,';
            if(strlen($filter['can_close_issue'])){
                $sql_part = 'INNER JOIN ticket_comments tc ON st.id = tc.ticket_id';
                $countSql_part = 'INNER JOIN ticket_comments tc ON st.id = tc.ticket_id INNER JOIN backend_users bu ON bu.id = IFNULL(st.backend_user_id, u.sales_user_id)';
            }else{
                $sql_part = 'LEFT JOIN ticket_comments tc ON st.id = tc.ticket_id';
            }

        }

        if(in_array(Cubix_Application::getId(),array(APP_ED)))
        {
            $updated_date = 'UNIX_TIMESTAMP(st.last_updated_date) AS resolve_date';
        }else{
            $updated_date = 'UNIX_TIMESTAMP(st.resolve_date) AS resolve_date';
        }

        $sql = '
			SELECT '.$sql_select_part.'
				st.id AS id, st.user_id, st.subject,
				st.message, st.status, st.status_progress , UNIX_TIMESTAMP(st.creation_date) AS creation_date,
				'.$updated_date.', st.backend_user_id, u.sales_user_id, u.username, u.user_type,
				ap.title AS app_title, c.iso AS app_iso, e.agency_id, e.id AS escort_id, e.showname, ag.name AS agency_name, bu.first_name,
				st.from_backend_user, st.disabled_in_pa
			FROM support_tickets st
			INNER JOIN users u ON u.id = st.user_id
			'.$sql_part.'
			LEFT JOIN applications ap ON ap.id = st.application_id
			LEFT JOIN countries c ON c.id = ap.country_id
			LEFT JOIN escorts e ON e.user_id = u.id
			LEFT JOIN agencies ag ON ag.id = e.agency_id
			LEFT JOIN backend_users bu ON bu.id = IFNULL(st.backend_user_id, u.sales_user_id)
			WHERE 1
		';

		$countSql = '
			SELECT COUNT(DISTINCT st.id)
			FROM support_tickets st
			INNER JOIN users u ON u.id = st.user_id
			'.$countSql_part.'
			LEFT JOIN escorts e ON e.user_id = u.id
			
			WHERE 1
		';


		
		if ( strlen($filter['st.application_id']) ) {
			$where .= self::quote('AND st.application_id = ?', $filter['st.application_id']);
		}

		if ( strlen($filter['st.disabled_in_pa']) ) {
			$where .= ' AND st.disabled_in_pa = 1 ';
		}
		
		if ( strlen($filter['u.username']) ) {
			$where .= self::quote('AND u.username LIKE ?', $filter['u.username'] . '%');
		}
		
		if ( strlen($filter['st.id']) ) {
			$where .= self::quote('AND st.id = ?', $filter['st.id']);
		}
		
		if ( strlen($filter['e.id']) ) {
			$where .= self::quote('AND e.id = ?', $filter['e.id'] );
		}
		/*if ( strlen($filter['u.last_ip']) ) {
			$where .= self::quote('AND u.last_ip LIKE ?', $filter['u.last_ip'] . '%');
		}
		*/
		if ( strlen($filter['st.status']) ) {
			$where .= self::quote('AND st.status = ?', $filter['st.status']);
		}
		
		if ( strlen($filter['st.backend_user_id']) ) {
			$where .= self::quote('AND st.backend_user_id = ?', $filter['st.backend_user_id']);
		}
		
		if ( strlen($filter['st.status_progress']) ) {
			$where .= self::quote('AND st.status_progress = ?', $filter['st.status_progress']);
		}

		//get Follow up filter for escortdirectory
        if(Cubix_Application::getId() == APP_ED){
            if ( strlen($filter['st.to_follow_up']) ) {
                $where .= self::quote('AND st.to_follow_up = ?', $filter['st.to_follow_up']);
            }
        }
        if(Cubix_Application::getId() == APP_EF && strlen($filter['can_close_issue']) ){
            $where .= ' AND datediff( NOW(),tc.date) >= 3 ';
            $where .= ' AND (bu.type = "admin" OR bu.type = "superadmin")';
        }

		$sql .= $where;
		$countSql .= $where;

		$sql .= '
			GROUP BY st.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		/*echo $sql;
		echo "<br/>";
		echo $countSql;die;*/
		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return parent::_fetchAll($sql);
	}
	
	public function insert($data)
	{
		parent::getAdapter()->insert('support_tickets', $data);
		return self::db()->lastInsertId();
		
	}
	
	public function remove($id)
	{
		parent::remove(self::quote('id = ?', $id));
		parent::getAdapter()->delete('ticket_comments', parent::quote('ticket_id = ?', $id));
	}

	public function removeComment($id)
	{
		Model_Activity::getInstance()->log('delete_ticket_comment', array('comment id' => $id));

		parent::getAdapter()->update('ticket_comments', array('status' => self::STATUS_TICKET_COMMENT_DELETED), parent::quote('id = ?', $id));
	}

	public function setFollowUp($data){
        $up_data = array('to_follow_up' => $data['to_follow_up']);
        parent::getAdapter()->update('support_tickets', $up_data, parent::getAdapter()->quoteInto('id = ?', $data['id']));
    }

	public function setStatus($data)
	{
		$up_data = array('status' => $data['status'], 'resolve_date'=> $data['resolve_date']);
        if(in_array(Cubix_Application::getId(), array(APP_ED)))
        {
            $up_data['last_updated_date'] = new Zend_Db_Expr('NOW()');
        }
		if ( isset($data['disabled_in_pa']) ) {
			$up_data = array_merge($up_data, array('disabled_in_pa' => $data['disabled_in_pa'], 'is_read' => 1, 'status_progress' => self::STATUS_READ_REPLIED));
		}
		elseif ($data['status'] == self::STATUS_TICKET_CLOSED){
			$up_data = array_merge($up_data, array('is_read' => 1));
		}
		
		Model_Activity::getInstance()->log('ticket_status_changes', array('ticket_id' => $data['id'], 'status' => $data['status'] ));
		parent::getAdapter()->update('support_tickets', $up_data, parent::getAdapter()->quoteInto('id = ?', $data['id']));
		
	}

	public function getOpenedCount($application_id = null)
	{
		$_sql = array(
			'tables' => array('support_tickets st'),
			'fields' => array(
				'COUNT(st.id)'
			),
			'joins' => array(
				'INNER JOIN users u ON u.id = st.user_id'
			),
			'where' => array(
				'st.status = ?' => self::STATUS_TICKET_OPENED
			)
		);

		if ( ! is_null($application_id) ) {
			$_sql['where']['st.application_id = ?'] = $application_id;
		}

		$sql = self::getSql($_sql);

		return parent::getAdapter()->fetchOne($sql);
	}

	public function changeBackendUserId($id, $backend_user_id)
	{
		parent::getAdapter()->update($this->_table, array('backend_user_id' => $backend_user_id), parent::quote('id = ?', $id));
	}
	
	public function update($id, $data)
	{
		parent::getAdapter()->update($this->_table, $data, parent::getAdapter()->quoteInto('id = ?', $id));
	}
	
	public function addAttached($data)
	{
		parent::getAdapter()->insert('ticket_attached_files', $data);
		return parent::getAdapter()->lastInsertId();
	}
	
	public function updateAttached($data, $id){
		parent::getAdapter()->update('ticket_attached_files', $data, parent::getAdapter()->quoteInto('id = ?', $id));
	}

    public function log($logData){
        parent::getAdapter()->insert('support_tickets_log', $logData);
    }
}
