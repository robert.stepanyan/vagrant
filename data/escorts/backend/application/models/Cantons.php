<?php

class Model_Cantons extends Cubix_Model 
{
	protected $_table = 'cantons';


	public function getById($id)
	{
		return parent::_fetchRow('
			SELECT 
				c.id, c.' . Cubix_I18n::getTblField('title') . ' as title,
				
			FROM cantons c
			WHERE c.id = ?', array($id)
		);
	}

	public function getAll()
	{
		return parent::_fetchAll('SELECT id, ' . Cubix_I18n::getTblField('title') . ' as title FROM cantons');
	}
	
}
