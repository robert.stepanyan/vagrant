<?php

class Model_Verifications_RequestItem extends Cubix_Model_Item
{
	public function getPhotos()
	{
		$photos = $this->_adapter->query('
			SELECT u.application_id, vf.* FROM verify_files vf
			INNER JOIN verify_requests vr ON vr.id = vf.request_id
			INNER JOIN escorts e ON e.id = vr.escort_id
			INNER JOIN users u ON u.id = e.user_id
			WHERE vr.id = ?
		', $this->getId())->fetchAll();
		
		
		foreach ($photos as $i => $photo) {
			$photo->escort_id = $this->escort_id;
			$photos[$i] = new Model_Verifications_Request_PhotoItem($photo);
		}
		
		return $photos;
	}
}
