<?php

class Model_Verifications_Vip extends Cubix_Model
{	
	const PENDING = 1;
	const VERIFY = 2;
	const REJECTED = 3;
	const SUSPENDED = 4;
	
	const REQUEST_OWNER_ADMIN = 1;
	const REQUEST_OWNER_ESCORT = 2;
	const REQUEST_OWNER_AGENCY = 3;
	
	public function getAll($p, $pp, $sort_field, $sort_dir, $filter, &$count)
	{
		$join = '';
		$where = '';
		
		if ( strlen($filter['e.showname']) ) {
			$where .= self::quote(' AND e.showname LIKE ?', $filter['e.showname'] . '%');
		}
		
		if ( strlen($filter['e.id']) ) {
			$where .= self::quote(' AND e.id = ?', $filter['e.id']);
		}
		
		if ( strlen($filter['u.application_id']) ) {
			$where .= self::quote(' AND u.application_id = ?', $filter['u.application_id']);
		}
		
		if ( strlen($filter['v.status']) ) {
			$where .= self::quote(' AND v.status = ?', $filter['v.status']);
		}
		
		if ( strlen($filter['with_comment']) ) {
			$where .= ' AND LENGTH(v.comment) > 0 ';
		}
				
		if (strlen($filter['date_from']) && strlen($filter['date_to'])) {
			$where .= self::quote(' AND v.creation_date >= DATE(FROM_UNIXTIME(?))', $filter['date_from']);
			$where .= self::quote(' AND v.creation_date <= DATE(FROM_UNIXTIME(?))', $filter['date_to']);
		}
		else if ( strlen($filter['date_from']) ) {
			$where .= self::quote(' AND v.creation_date >= DATE(FROM_UNIXTIME(?))', $filter['date_from']);
		}
		else if ( strlen($filter['date_to']) ) {
			$where .= self::quote(' AND v.creation_date <= DATE(FROM_UNIXTIME(?))', $filter['date_to']);
		}
		
		if (strlen($filter['active_package_id'])) {
			$join .= " LEFT JOIN order_packages o_p ON o_p.escort_id = e.id ";
			$where .= self::quote(' AND o_p.status = 2 AND o_p.package_id = ?', $filter['active_package_id']);
		}
		
		if ( strlen($filter['e.verified_status']) ) {
			$where .= self::quote(' AND e.verified_status = ?', $filter['e.verified_status']);
		}
		
		$sql = '
			SELECT 
				e.id AS escort_id, 
				e.showname, 
				v.id, 
				UNIX_TIMESTAMP(v.creation_date) AS creation_date,
				v.status,
				UNIX_TIMESTAMP(v.action_date) AS action_date,
				v.request_owner,
				v.req_own_backend_user_id,
				bu.username as bu_user,
				v.reject_reason,
				u.application_id,
				u.id AS user_id,
				v.comment
			FROM vip_requests v
			INNER JOIN escorts e ON v.escort_id = e.id 
			LEFT JOIN backend_users bu ON bu.id = v.backend_user_id
			INNER JOIN users u ON u.id = e.user_id' . $join . '
			WHERE 1
		';
		
		$countSql = '
			SELECT COUNT(v.id) FROM vip_requests v
			INNER JOIN escorts e ON v.escort_id = e.id 
			INNER JOIN users u ON u.id = e.user_id' . $join . '
			WHERE 1
		';
		
		$sql .= $where;
		$countSql .= $where;
				
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
		';
		
		$count = $this->getAdapter()->fetchOne($countSql);
		
		return parent::_fetchAll($sql);
	}
	
	public function getEscortsList($escort)
	{
		$sql = "SELECT showname, id	FROM escorts WHERE showname LIKE '" . $escort . "%'	LIMIT 0, 10";
		
		return $this->getAdapter()->query($sql)->fetchAll();
	}
	
	public function add($data)
	{
		$escort_id = $data['e_id'];
		$bu_id = $data['bu_id'];
		
		$this->getAdapter()->insert('vip_requests', array(
			'escort_id' => $escort_id,
			'creation_date' => new Zend_Db_Expr('NOW()'),
			'action_date' => new Zend_Db_Expr('NOW()'),
			'request_owner' => self::REQUEST_OWNER_ADMIN,
			'req_own_backend_user_id' => $bu_id,
			'backend_user_id' => $bu_id,
			'status' => self::VERIFY
		));
		
		Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('action' => 'vip_request_verified'));
	}
	
	public function getLastRequestByEscort($escort_id)
	{
		return $this->getAdapter()->query('SELECT * FROM vip_requests WHERE escort_id = ? ORDER BY creation_date DESC LIMIT 1', $escort_id)->fetch();
	}
	
	public function get($id)
	{
		$sql = "
			SELECT 
				e.id AS escort_id, 
				e.showname, 
				v.id, 
				UNIX_TIMESTAMP(v.creation_date) AS creation_date,
				v.status,
				UNIX_TIMESTAMP(v.action_date) AS action_date,
				v.request_owner,
				v.req_own_backend_user_id,
				bu.username as bu_user,
				v.reject_reason,
				u.id AS user_id
			FROM vip_requests v
			INNER JOIN escorts e ON v.escort_id = e.id 
			LEFT JOIN backend_users bu ON bu.id = v.backend_user_id
			INNER JOIN users u ON u.id = e.user_id
			WHERE v.id = ?
		";
		
		return $this->getAdapter()->query($sql, $id)->fetch();
	}
	
	public function getHistory($id, $escort_id)
	{
		$sql = "
			SELECT 
				e.id AS escort_id, 
				e.showname, 
				v.id, 
				UNIX_TIMESTAMP(v.creation_date) AS creation_date,
				v.status,
				UNIX_TIMESTAMP(v.action_date) AS action_date,
				v.request_owner,
				v.req_own_backend_user_id,
				bu.username as bu_user,
				v.reject_reason,
				u.id AS user_id
			FROM vip_requests v
			INNER JOIN escorts e ON v.escort_id = e.id 
			LEFT JOIN backend_users bu ON bu.id = v.backend_user_id
			INNER JOIN users u ON u.id = e.user_id
			WHERE v.id <> ? AND escort_id = ?
			ORDER BY creation_date DESC
		";
		
		return $this->getAdapter()->query($sql, array($id, $escort_id))->fetchAll();
	}
	
	public function remove($id)
	{
		$this->getAdapter()->delete('vip_requests', $this->getAdapter()->quoteInto('id = ?', $id));
	}

	public function setStatus($id, $status, $bu_id, $reason = null)
	{
		$this->getAdapter()->update('vip_requests', array(
			'action_date' => new Zend_Db_Expr('NOW()'),
			'backend_user_id' => $bu_id,
			'reject_reason' => $reason,
			'status' => $status
		), $this->getAdapter()->quoteInto('id = ?', $id));
		
		if ($status == self::VERIFY)
		{
			$escort_id = $this->getAdapter()->fetchOne('SELECT escort_id FROM vip_requests WHERE id = ?', $id);
			Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('action' => 'vip_request_verified'));
		}
	}
	
	public function getComment($id)
	{
		return $this->getAdapter()->fetchOne('SELECT comment FROM vip_requests WHERE id = ?', $id);
	}
	
	public function updateComment($data)
	{
		$this->getAdapter()->update('vip_requests', array('comment' => $data['comment']), $this->getAdapter()->quoteInto('id = ?', $data['id']));
	}
}
