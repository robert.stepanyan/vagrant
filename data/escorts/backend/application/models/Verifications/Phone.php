<?php

class Model_Verifications_Phone extends Cubix_Model
{	
	const STATUS_PENDING = 0;
	const STATUS_CONFIRMED = 1;
		
	public function getAll($p, $pp, $filter, $sort_field, $sort_dir, &$count)
	{
		$where = '';
		
		$bu_user = Zend_Auth::getInstance()->getIdentity();

		if ( $bu_user->type == 'sales manager' ) {
			$where .= self::quote(' AND u.sales_user_id = ?', $bu_user->id);
		}
		
		if ( strlen($filter['showname']) ) {
			$where .= self::quote(' AND e.showname LIKE ?', $filter['showname'] . '%');
		}
		
		if ( strlen($filter['escort_id']) ) {
			$where .= self::quote(' AND e.id = ?', intval($filter['escort_id']));
		}
		
		if ( strlen($filter['phone']) ) {
			$where .= self::quote(' AND epv.phone LIKE ?', '%'. $filter['phone'] . '%');
		}
		
		if ( strlen($filter['status']) ) {
			$where .= self::quote(' AND epv.status = ?', $filter['status']);
		}
		
		if ( strlen($filter['method']) ) {
			$where .= self::quote(' AND epv.type = ?', $filter['method']);
		}
		
		if ( $filter['date_from'] && $filter['date_to'] ) {
				if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) ) {
					$where .= self::quote( " AND DATE(epv.date) = DATE(FROM_UNIXTIME(?))" , $filter['date_from']);
				}
			}
		if ( $filter['date_from'] ) {
				$where .= self::quote(" AND DATE(epv.date) >= DATE(FROM_UNIXTIME(?)) ", $filter['date_from']);
			}

		if ( $filter['date_to'] ) {
			$where .= self::quote(" AND DATE(epv.date) <= DATE(FROM_UNIXTIME(?)) ", $filter['date_to']);
		}
		
		$sql = '
			SELECT 
				e.id AS escort_id, 
				e.showname,
				UNIX_TIMESTAMP(epv.date) AS date,
				epv.status,
				epv.type,
				epv.code,
				epv.tries,
				epv.phone,
				c.title_en as country
			FROM escorts_phone_verification epv
			INNER JOIN escorts e ON epv.escort_id = e.id
			INNER JOIN countries c ON e.country_id = c.id
			WHERE 1 
		';
		
		$countSql = '
			SELECT COUNT(*) FROM escorts_phone_verification epv
			INNER JOIN escorts e ON epv.escort_id = e.id 
			WHERE 1 
		';
		
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
		';
		
		$count = $this->getAdapter()->fetchOne($countSql);
		
		return parent::_fetchAll($sql);
	}
	
	
	
}
