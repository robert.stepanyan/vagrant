<?php

class Model_Verifications_AgeVerification extends Cubix_Model
{	
	const REQUEST_SENT = 1;
	const PENDING = 2;
	const VERIFY = 3;
	const REJECTED = 4;
	const REMOVED = 5;
	
	const REQUEST_TYPE_ONLINE = 1;
	const REQUEST_TYPE_OFFLINE = 2;
	
	public function getAll($p, $pp, $sort_field, $sort_dir, $filter, &$count)
	{
		$where = '';

		$bu_user = Zend_Auth::getInstance()->getIdentity();

		if ( $bu_user->type == 'sales manager' ) {
			$where .= self::quote(' AND u.sales_user_id = ?', $bu_user->id);
		}
		
		if ( strlen($filter['e.showname']) ) {
			$where .= self::quote(' AND e.showname LIKE ?', $filter['e.showname'] . '%');
		}
		
		if ( strlen($filter['agency_name']) ) {
			$where .= self::quote(' AND ag.name LIKE ?', $filter['agency_name'] . '%');
		}
		
		if ( strlen($filter['name']) ) {
			$where .= self::quote(' AND av.name LIKE ?', $filter['name'] . '%');
		}
		
		if ( strlen($filter['last_name']) ) {
			$where .= self::quote(' AND av.last_name LIKE ?', $filter['last_name'] . '%');
		}
		
		if ( strlen($filter['both']) ) {
			$where .= self::quote(' AND ( av.name LIKE ? ', $filter['both']. '%');
			$where .= self::quote(' OR av.last_name LIKE ? )  ', $filter['both']. '%');
		}
		
		if ( strlen($filter['e.id']) ) {
			$where .= self::quote(' AND e.id = ?', $filter['e.id']);
		}
		
		if ( strlen($filter['u.application_id']) ) {
			$where .= self::quote(' AND u.application_id = ?', $filter['u.application_id']);
		}
		
		if ( strlen($filter['av.status']) ) {
			$where .= self::quote(' AND av.status = ?', $filter['av.status']);
		}
		
		if ( $filter['special_case'] ) {
			$where .= self::quote(' AND av.special_case = ?', $filter['special_case']);
		}
		
		if ( strlen($filter['day']) ) {
			$where .= self::quote(' AND DAY(av.born_date) = ?', $filter['day']);
		}
		
		if ( strlen($filter['month']) ) {
			$where .= self::quote(' AND MONTH(av.born_date) = ?', $filter['month']);
		}

		if ( strlen($filter['year']) ) {
			$where .= self::quote(' AND YEAR(av.born_date) = ?', $filter['year']);
		}
		if (strlen($filter['date_from']) && strlen($filter['date_to'])) {
			if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) ) {
				$where .= 	self::quote(" AND DATE(av.creation_date) = DATE(FROM_UNIXTIME(?))" , $filter['date_from']);
			}
			else{	
				$where .= self::quote(' AND av.creation_date >= DATE(FROM_UNIXTIME(?))', $filter['date_from']);
				$where .= self::quote(' AND av.creation_date <= DATE(FROM_UNIXTIME(?))', $filter['date_to']);
			}
		}
		else if ( strlen($filter['date_from']) ) {
			$where .= self::quote(' AND av.creation_date >= DATE(FROM_UNIXTIME(?))', $filter['date_from']);
		}
		else if ( strlen($filter['date_to']) ) {
			$where .= self::quote(' AND av.creation_date <= DATE(FROM_UNIXTIME(?))', $filter['date_to']);
		}

        $order_joins = "";
        $order_where = "";
        if( Cubix_Application::getId() == APP_EF &&  strlen($filter['active_package']) ){
            $order_joins = ' INNER JOIN order_packages op ON op.escort_id = e.id ';
            $order_where = ' AND op.status = 2 AND op.order_id IS NOT NULL ';
        }

		$isNewSql = '';
		if(Cubix_Application::getId()  == APP_EF){
			if ( strlen($filter['e.mark_not_new']) ) {
				$where .= self::quote(' AND e.mark_not_new = ?', $filter['e.mark_not_new']);
			}
			$isNewSql = ' e.mark_not_new, ';
			//$isNewSql = ' IF ((e.date_registered < (CURDATE() - INTERVAL 5 DAY)) AND (e.mark_not_new = 0), 0, 1) AS mark_not_new, e.mark_not_new AS removed_new, ';
		}

		$sql = '
			SELECT
				e.id AS escort_id, 
				e.showname,
				'.$isNewSql.'
				u.username,
				ag.name AS agency_name, 
				IF(e.age_verify_date IS NOT NULL , e.age_verify_date, "") as age_verify_date,
				av.id, 
				UNIX_TIMESTAMP(av.creation_date) AS creation_date,
				av.status,
				av.name,
				av.last_name,
				av.born_date,
				UNIX_TIMESTAMP(av.update_date) AS update_date,
				bu.username as bu_user,
				IF(bu2.username IS NOT NULL , bu2.username , "") as sent_by,
				av.reject_reason,
				av.special_case,
				u.application_id,
				u.id AS user_id,
				c.attempt_count
			FROM age_verifications av
			INNER JOIN escorts e ON av.escort_id = e.id 
			INNER JOIN (SELECT av.escort_id, count(av.escort_id) as attempt_count
						FROM age_verifications av
						WHERE av.status >= 0
						GROUP BY av.escort_id) c ON e.id = c.escort_id
			LEFT JOIN agencies ag ON ag.id = e.agency_id
			LEFT JOIN backend_users bu ON bu.id = av.admin_id
			LEFT JOIN backend_users bu2 ON bu2.id = e.age_verify_admin_id
			INNER JOIN users u ON u.id = e.user_id
			'.$order_joins.'
			WHERE av.status <> -1  
		';
		
		$countSql = '

			SELECT COUNT(av.id) FROM age_verifications av
			INNER JOIN escorts e ON av.escort_id = e.id 
			INNER JOIN users u ON u.id = e.user_id
			LEFT JOIN agencies ag ON ag.id = e.agency_id
			'.$order_joins.'
			WHERE av.status <> -1
		';
		$where .= $order_where;
		$sql .= $where;
		$countSql .= $where;
		

		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
		';

		$count = $this->getAdapter()->fetchOne($countSql);
		
		return parent::_fetchAll($sql);
	}
	
	public function getPhotosByRequestId($id)
	{
		$photos = $this->getAdapter()->query('
			SELECT e.id as escort_id, u.application_id, avf.* FROM age_verification_files avf
			INNER JOIN age_verifications av ON av.id = avf.request_id
			INNER JOIN escorts e ON e.id = av.escort_id
			INNER JOIN users u ON u.id = e.user_id
			WHERE av.id = ?
		', $id)->fetchAll();
		
		
		foreach ($photos as $i => $photo) {
			$photos[$i] = new Model_Verifications_Request_PhotoItem($photo);
		}
		
		return $photos;
	}
		
	public function get($id)
	{
		$sql = "
			SELECT 
				e.id AS escort_id, 
				e.showname,
				av.id,
				av.name,
				av.last_name,
				av.born_date,
				UNIX_TIMESTAMP(av.creation_date) AS creation_date,
				av.status,
				UNIX_TIMESTAMP(av.update_date) AS update_date,
				av.admin_id,
				bu.username as bu_user,
				av.reject_reason,
				u.id AS user_id,
				av.comment
			FROM age_verifications av
			INNER JOIN escorts e ON av.escort_id = e.id 
			LEFT JOIN backend_users bu ON bu.id = av.admin_id
			INNER JOIN users u ON u.id = e.user_id
			WHERE av.id = ?
		";
		
		return $this->getAdapter()->query($sql, $id)->fetch();
	}
	
	public function getRejectReasons($escort_id)
	{
		$sql = "
			SELECT 
				av.update_date AS reject_date,
				bu.username as bu_user,
				av.reject_reason
			FROM age_verifications av
			INNER JOIN backend_users bu ON bu.id = av.admin_id
			WHERE av.status = ? AND av.escort_id = ?
		";
		
		return $this->getAdapter()->FetchAll($sql, array(self::REJECTED,$escort_id));
	}
	
	public function setStatus($request_id, $status, $reason = null)
	{
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		$this->getAdapter()->update('age_verifications', array(
			'update_date' => new Zend_Db_Expr('NOW()'),
			'admin_id' => $bu_user->id,
			'reject_reason' => $reason,
			'status' => $status
		), $this->getAdapter()->quoteInto('id = ?', $request_id));
		
	}
	
	public function updateEscortStatus($escort_id , $status, $verify_type = null, $need_video = 0)
	{
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		if($status == self::REQUEST_SENT){
			
			$data = array(
				'age_verify_date' => new Zend_Db_Expr('NOW()'),
				'need_age_verification' => $status,
				'age_verify_type' => $verify_type,
				'age_verify_admin_id' => $bu_user->id,
				'need_age_verify_video' => $need_video
			);
			
			if($verify_type == self::REQUEST_TYPE_OFFLINE){
				$data['age_verify_disabled'] = 1;
			}
			
			$this->getAdapter()->update('escorts', $data , $this->getAdapter()->quoteInto('id = ?', $escort_id));
		}
		else{
			
			$data = array(
				'need_age_verification' => $status,
				'need_age_verify_video' => $need_video
			);
			
			if($status == self::VERIFY){
				$data['age_verify_disabled'] = 0;
			}
			$this->getAdapter()->update('escorts', $data , $this->getAdapter()->quoteInto('id = ?', $escort_id));
		}
		$escort = $this->getEscortData($escort_id);
		$langs = Cubix_I18n::getLangs(true);
		$messages = array();
		
		if($status == self::REQUEST_SENT){
			$dic_id = 'age_verification_suspect';
		}
		else if($status == self::VERIFY){
			$dic_id = 'age_verification_verify';
		}
		else if($status == self::REJECTED){
			$dic_id = 'age_verification_reject';
		}
		
		foreach ($langs AS $lang){
			$messages[$lang] = Cubix_I18n::translateByLng($lang, $dic_id, array('showname'=> $escort->showname ));
		}
				
		/* send email */
		$subject = " Age Certification";
		if($status != self::REJECTED){
			$email_data = array(
				'subject' => $subject ,
				'message' => implode("<br/><br/>", $messages)
			);
			Cubix_Email::sendTemplate('universal_message', $escort->email , $email_data);
		}
		
		
		Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('action' => 'age cerification -'.$status));
		return true;
		
	}
	
	public function getEscortData($escort_id)
	{
		$sql = " SELECT e.showname, u.email, ep.contact_phone_parsed FROM escorts e
				INNER JOIN users u ON u.id = e.user_id
				INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
				WHERE e.id = ? ";
		
		return $this->getAdapter()->fetchRow($sql, $escort_id);
	}
	
	public function postpone24($escort_id)
	{
		$data = array(
				'age_verify_date' => new Zend_Db_Expr('NOW()'),
				'age_verify_type' => self::REQUEST_TYPE_ONLINE,
				'age_verify_disabled' => 0
			);
		
		$this->getAdapter()->update('escorts', $data, $this->getAdapter()->quoteInto('id = ?', $escort_id));
		
	}
	
	public function getDuplicity($escort_id, $name, $last_name, $born_date)
	{
		$sql = " SELECT MAX(av.id) as req_id, av.escort_id, e.need_age_verification FROM age_verifications av
				 INNER JOIN escorts e ON av.escort_id = e.id
				 WHERE av.escort_id <> ? AND av.born_date = ? AND ((av.name = ? AND av.last_name = ?) OR (av.name = ? AND av.last_name = ?)) 
				 GROUP BY av.escort_id";
		
		$duplicities = $this->getAdapter()->fetchAll($sql, array($escort_id, $born_date, $name,$last_name, $last_name, $name));
		$duplicities_array = array();
		if($duplicities){
			foreach($duplicities as $key =>$dupl){
				$duplicities_array[$key]['req_id'] =  $dupl->req_id;
				$duplicities_array[$key]['escort_id'] =  $dupl->escort_id;
				$duplicities_array[$key]['ver_type'] = $dupl->need_age_verification;
			}
		}
		
		return $duplicities_array;
	}
	
	public function get100Verified($escort_id){
		return $this->getAdapter()->fetchOne('SELECT id FROM age_verifications WHERE escort_id = ? AND status = -1 ORDER BY id DESC', $escort_id);
	}

    /**
     * @param $escort_id integer
     * @param $request_id integer ID_Card verification id
     * @throws Exception
     * @throws Zend_Db_Adapter_Exception
     *
     * used APP_EF only
     */
	public function addAgeVerificationFromIdcard($escort_id,$request_id)
	{		

		$age_verification = $this->getByIdcardId($request_id);
		if (empty($age_verification)){
            $age_verification = array(
                'escort_id' => $escort_id,
                'idcard_id' => $request_id,
                'creation_date' => new Zend_Db_Expr('NOW()'),
                'status' => self::VERIFY
            );
            $this->getAdapter()->insert('age_verifications', $age_verification);
            $last_id = $this->_db->lastInsertId();
        }else{
            $last_id = $age_verification->id;
            // delete old files in  age_verification_files table
            $this->_db->delete('age_verification_files',array($this->_db->quoteInto('request_id = ?', $last_id)));
            // set new update date
            $this->_db->update('age_verifications', array('update_date' => new Zend_Db_Expr('NOW()'),), array( $this->_db->quoteInto('id = ?', $last_id)));
        }
		
		$idcard_model = new Model_Verifications_Requests();
		/* @var $idcard_model_item Model_Verifications_RequestItem */
		$idcard_model_item = $idcard_model->get($request_id);
		$photos = $idcard_model_item->getPhotos();
		$images = new Cubix_Images();
		
		foreach($photos as $photo){
			if ($photo->is_video){
                $video_ftp = new Cubix_VideosCommon();
                $localFile=$video_ftp->retrive($photo, $escort_id);
                $video_hash = uniqid() . '_age_certify';
                $video_ext = 'mp4';
                $name = $video_hash . '.' . $video_ext;
                if ($video_ftp->_storeToPicVideo($localFile, $escort_id, $name)) {
                    $age_verify_file = array('hash' => $video_hash, 'ext' => $video_ext, 'is_video' => 1);
                }
                unlink($localFile);
            }else{
                $catalog_id = $escort_id . '/verify';
                $localFile = $images->retreive(new Cubix_Images_Entry(array(
                    'ext' => $photo->ext,
                    'hash' => $photo->hash,
                    'catalog_id' => $catalog_id,
                    'application_id' => Cubix_Application::getId()
                )));
                $age_verify_file =  $images->save($localFile, $escort_id . '/age-verification', Cubix_Application::getId(), $photo->ext,null,$photo->hash, true);
                unlink($localFile);
            }
			if (!empty($age_verify_file)){
                $age_verify_file['request_id'] = $last_id;
                $this->_db->insert('age_verification_files', $age_verify_file);
            }
		}
	
		$this->_db->update('escorts', array('need_age_verification' => self::VERIFY, 'age_verify_disabled' => 0 ), array( $this->_db->quoteInto('id = ?', $escort_id)));
		Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('action' => 'Age certification from idcard'));
	}
	
	public function updateIdcardId($request_id,$idcard_id)
	{
		$this->_db->update('age_verifications', array('idcard_id' => $idcard_id), array( $this->_db->quoteInto('id = ?', $request_id)));
	}
	
	public function saveInfo($request_id, $name, $last_name, $date, $comment)
	{
		if ($request_id)
		{
			$this->_db->update('age_verifications', array(
				'name' => $name,
				'last_name' => $last_name,
				'born_date' => $date,
				'comment' => $comment
			), array( $this->_db->quoteInto('id = ?', $request_id)));
		}
	}
	
	public function add($data, $photos = null)
	{
		$this->getAdapter()->insert('age_verifications', $data);
		$last_id = $this->_db->lastInsertId();
		
		if($photos){
			foreach($photos as $photo){

				$photo['request_id'] = $last_id;
				$this->_db->insert('age_verification_files', $photo);
			}
		}
	}
	
	public function updateEscort($escort_id, $data)
	{
		$this->_db->update('escorts', $data , array( $this->_db->quoteInto('id = ?', $escort_id)));
	}
	
	public function getByIdcardId($idcard_id)
	{
		return $this->getAdapter()->fetchRow( "SELECT id, name, last_name, born_date FROM age_verifications WHERE idcard_id = ?", $idcard_id);
	}

	public function getComment($id)
	{
		return $this->getAdapter()->fetchOne('SELECT comment FROM age_verifications WHERE id = ?', $id);
	}
	
	public function updateComment($data)
	{
		$this->getAdapter()->update('age_verifications', array('comment' => $data['comment']), $this->getAdapter()->quoteInto('id = ?', $data['id']));
	}
	
	public function markSpecial($id)
	{
		$this->getAdapter()->update('age_verifications', array('special_case' => new Zend_Db_Expr('NOT special_case')), $this->getAdapter()->quoteInto('id = ?', $id));
	}
	public function removeNew($id){
		$this->getAdapter()->update('escorts', array('mark_not_new' => 1), $this->getAdapter()->quoteInto('id = ?', $id));
		Cubix_SyncNotifier::notify($id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('action' => 'Remove New'));
	}
	public function restoreNew($id){
		$this->getAdapter()->update('escorts', array('mark_not_new' => 0), $this->getAdapter()->quoteInto('id = ?', $id));
		Cubix_SyncNotifier::notify($id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('action' => 'Restore New'));
	}
}
