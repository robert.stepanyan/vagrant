<?php

class Model_Verifications_Requests extends Cubix_Model
{
	const TYPE_WEBCAM = 1;
	const TYPE_IDCARD = 2;
	
	const INFORM_METHOD_EMAIL = 1;
	const INFORM_METHOD_SMS = 2;
	
	const PENDING = 1;
	const VERIFY = 2;
	const CONFIRMED = 3;
	const REJECTED = 4;
	const REMOVED = 5;
	const PHOTOSHOP = 6;
	const BLUR_VERIFY = 7;
	
	protected $_table = 'verify_requests';
	protected $_itemClass = 'Model_Verifications_RequestItem';



	public function getLastRequest( $escort_id )
	{
		$sql = "
			SELECT
				e.id AS escort_id,
				e.showname,
				vr.id,
				vr.inform_method,
				UNIX_TIMESTAMP(vr.date) AS date,
				vr.status,
				UNIX_TIMESTAMP(vr.date_1) AS date_1,
				vr.date_1_is_confirmed,
				UNIX_TIMESTAMP(vr.date_2) AS date_2,
				vr.date_2_is_confirmed,
				UNIX_TIMESTAMP(vr.date_3) AS date_3,
				vr.date_3_is_confirmed,
				vr.comment,
				vr.soft,
				vr.soft_username,
				vr.phone,
				vr.email,
				vr.type,
				u.application_id

			FROM verify_requests vr
			INNER JOIN escorts e ON vr.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id
			WHERE vr.escort_id = ?
			ORDER BY vr.id DESC
		";

		return  parent::_fetchRow($sql, $escort_id);
	}



	public function get($request_id)
	{
		$sql = "
			SELECT 
			
				e.id AS escort_id, 
				e.showname, 
				vr.id, 
				vr.inform_method, 
				UNIX_TIMESTAMP(vr.date) AS date,
				vr.status,
				UNIX_TIMESTAMP(vr.date_1) AS date_1,
				vr.date_1_is_confirmed,
				UNIX_TIMESTAMP(vr.date_2) AS date_2,
				vr.date_2_is_confirmed,
				UNIX_TIMESTAMP(vr.date_3) AS date_3,
				vr.date_3_is_confirmed,
				vr.comment,
				vr.soft,
				vr.soft_username,
				vr.phone,
				vr.email,
				vr.type,
				u.application_id
			
			FROM verify_requests vr
			INNER JOIN escorts e ON vr.escort_id = e.id 
			INNER JOIN users u ON u.id = e.user_id
			WHERE vr.id = ?
		";
		
		return  parent::_fetchRow($sql, $request_id);
	}

	public function getAll($p, $pp, $sort_field, $sort_dir, $filter, &$count, $type = null)
	{
		$be = '';
		/*if ( strlen($filter['bu.id']) ) {
			$be = ' INNER JOIN backend_users bu ON bu.id = u.sales_user_id ';
		}*/
		$fields = "";
		$joins = "";
		$isNewSql = "";
        $order_joins = "";
        $order_where = "";
		if( Cubix_Application::getId() == APP_EF ){
			$joins .= " LEFT JOIN age_verifications av ON av.idcard_id = vr.id ";
			$fields .= " ,e.need_age_verification, av.name , av.last_name, av.born_date,av.status as age_status ";
			$isNewSql = ' IF ((e.date_registered > (CURDATE() - INTERVAL 5 DAY)) AND (e.mark_not_new = 0), 0, 1) AS mark_not_new, e.mark_not_new AS removed_new, ';
            if ( strlen($filter['active_package']) ) {
                $joins .= $order_joins = ' INNER JOIN order_packages op ON op.escort_id = e.id ';
                $order_where = ' AND op.status = 2 AND op.order_id IS NOT NULL ';
            }
		}
		$sql = '
			SELECT
				ag.name AS agency_name, ag.id AS agency_id,
				e.id AS escort_id, 
				e.showname,
				'.$isNewSql.' 
				vr.id, 
				vr.inform_method, 
				UNIX_TIMESTAMP(vr.date) AS date,
				vr.status,
				UNIX_TIMESTAMP(vr.date_1) AS date_1,
				vr.date_1_is_confirmed,
				UNIX_TIMESTAMP(vr.date_2) AS date_2,
				vr.date_2_is_confirmed,
				UNIX_TIMESTAMP(vr.date_3) AS date_3,
				vr.date_3_is_confirmed,
				vr.comment,
				vr.soft,
				vr.soft_username,
				vr.phone,
				vr.email,
				vr.type,
				u.application_id,
				u.username,
				bu.username as bu_user,
				vr.reason_ids,
				vr.reason_text,
				vr.is_read,
				c.attempt_count,
				UNIX_TIMESTAMP(vr.rejected_date) AS rejected_date '.
				$fields. '
			FROM verify_requests vr
			INNER JOIN escorts e ON vr.escort_id = e.id
			INNER JOIN (SELECT vr.escort_id, count(vr.escort_id) as attempt_count
						FROM verify_requests vr
						WHERE vr.status >= 0
						GROUP BY vr.escort_id) c ON e.id = c.escort_id
			LEFT JOIN backend_users bu ON bu.id = vr.verifier_person
			LEFT JOIN agencies ag ON ag.id = e.agency_id
			INNER JOIN users u ON u.id = e.user_id'.
			$joins.'
			WHERE 1
		';
		//' . $be . '
		$countSql = "
			SELECT COUNT(vr.id) FROM verify_requests vr
			INNER JOIN escorts e ON vr.escort_id = e.id 
			INNER JOIN users u ON u.id = e.user_id
			
			LEFT JOIN backend_users bu ON bu.id = vr.verifier_person
			$order_joins
			WHERE 1
		";
		//' . $be . '
		
		if ( ! is_null($type) ) {
			$sql .= self::quote(' AND vr.type = ?', $type);
			$countSql .= self::quote(' AND vr.type = ?', $type);
		}
		
		$where = '';
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		if ( $bu_user->type == 'sales manager' ) {
			$where .= self::quote(' AND u.sales_user_id = ?', $bu_user->id);
		}
		
		if ( strlen($filter['bu.id']) ) {
			$where .= self::quote(' AND bu.id = ?', $filter['bu.id']);
		}

		if ( strlen($filter['e.showname']) ) {
			$where .= self::quote(' AND e.showname LIKE ?', $filter['e.showname'] . '%');
		}
		
		if ( strlen($filter['e.id']) ) {
			$where .= self::quote(' AND e.id = ?', $filter['e.id']);
		}
		
		if ( strlen($filter['u.application_id']) ) {
			$where .= self::quote(' AND u.application_id = ?', $filter['u.application_id']);
		}
		
		if ( strlen($filter['vr.status']) ) {
			$where .= self::quote(' AND vr.status = ?', $filter['vr.status']);
		}
		
		if ( strlen($filter['vr.inform_method']) ) {
			$where .= self::quote(' AND vr.inform_method = ?', $filter['vr.inform_method']);
		}
		
		if(strlen($filter['date_from']) && strlen($filter['date_to'])) {
			$where .= self::quote(' AND vr.date >= DATE(FROM_UNIXTIME(?))', $filter['date_from']);
			$where .= self::quote(' AND vr.date <= DATE(FROM_UNIXTIME(?))', $filter['date_to']);
		}
		else if ( strlen($filter['date_from']) ) {
			$where .= self::quote(' AND vr.date >= DATE(FROM_UNIXTIME(?))', $filter['date_from']);
		}
		else if ( strlen($filter['date_to']) ) {
			$where .= self::quote(' AND vr.date <= DATE(FROM_UNIXTIME(?))', $filter['date_to']);
		}

		$where .= $order_where;
		//echo 'dd';
		$sql .= $where;
		$countSql .= $where;

		$sql .= '
			GROUP BY vr.id	
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			
			LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
		';
		
		//echo $sql; die;

		$count = $this->getAdapter()->fetchOne($countSql);

		return parent::_fetchAll($sql);
	}

    public function getVerifyRequestsForDash($sort_field, $sort_dir, $filter, &$count, $type) {

	    $joins = '';
	    if (!empty($filter['bu.id'])){
	        $joins = 'LEFT JOIN backend_users bu ON bu.id = vr.verifier_person';
        }
        $sql = "
        SELECT
				ag.`name` AS agency_name,
			    ag.id AS agency_id,
				e.id AS escort_id, 
				e.showname,
				vr.id, 
				UNIX_TIMESTAMP(vr.date) AS `date`,
				vr.`status`,
				vr.type,
				u.username,
               e.country_id
			FROM verify_requests vr
			INNER JOIN escorts e ON vr.escort_id = e.id
			LEFT JOIN agencies ag ON ag.id = e.agency_id
			INNER JOIN users u ON u.id = e.user_id
			$joins 
			WHERE 1
        ";

        $countSql = "
            SELECT COUNT(vr.id) 
            FROM verify_requests vr
			INNER JOIN escorts e ON vr.escort_id = e.id 
			INNER JOIN users u ON u.id = e.user_id
			$joins
			WHERE 1
        ";

        $where = '';

        if (!empty($type)) {
            $where .= self::quote(' AND vr.type = ?', $type);
        }

        $bu_user = Zend_Auth::getInstance()->getIdentity();
        if ( $bu_user->type == 'sales manager' ) {
            $where .= self::quote(' AND u.sales_user_id = ?', $bu_user->id);
        }

        if ( !empty($filter['bu.id']) ) {
            $where .= self::quote(' AND bu.id = ?', $filter['bu.id']);
        }

        if ( !empty($filter['vr.status']) ) {
            $where .= self::quote(' AND vr.`status` = ?', $filter['vr.status']);
        }

        if ( !empty($filter['u.application_id']) ) {
            $where .= self::quote(' AND u.application_id = ?', $filter['u.application_id']);
        }

        $sql.= $where;
        $sql .= '
			GROUP BY vr.id	
			ORDER BY ' . $sort_field . ' ' . $sort_dir;

        $countSql .= $where;
        $count = $this->getAdapter()->fetchOne($countSql);

        return parent::_fetchAll($sql);
	}
	
	public function setStatus($escort_id, $request_id, $status, $confirm_date = null, $reason_ids = null, $free_text = null, $need_video = 0, $blured_ids = null)
	{
		if ( $confirm_date ) {
			$confirm_date = ", date_" . $confirm_date . "_is_confirmed = 1 ";
		}
		
		if ($status == self::REMOVED)
		{
			$action = 'remove_verify_request';
			
			$this->getAdapter()->delete('verify_requests', $this->getAdapter()->quoteInto('id = ?', $request_id));
			$this->getAdapter()->delete('verify_files', $this->getAdapter()->quoteInto('request_id = ?', $request_id));
						
			Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('action' => $action));
			Model_Activity::getInstance()->log($action, array('escort id' => $escort_id, 'request id' => $request_id));
		}
		else
		{
			if ( $status == self::VERIFY ) {
				$action = 'verify_escort';
				/* Edit Grigor.Unset Suspicious Criteria */
				$this->getAdapter()->query("UPDATE escorts SET verified_status = ?, is_suspicious = 0, photo_suspicious_date = null WHERE id = ?", array(Model_Escorts::VERIFIED, $escort_id));
				/* Edit Grigor */
				$this->getAdapter()->query("UPDATE escort_photos SET status = '" . Model_Escort_Photos::VERIFIED . "' WHERE escort_id = ?", array($escort_id));
				$this->getAdapter()->update('verify_requests', array(
					'reason_ids' => '',
					'reason_text' => '',
					'is_read' => 0,
					'rejected_date' => NULL
				), $this->getAdapter()->quoteInto('id = ?', $request_id));
			}
			elseif ( $status == self::PHOTOSHOP ) {
				$action = 'verify_escort';
				/* Edit Grigor.Unset Suspicious Criteria */
				$this->getAdapter()->query("UPDATE escorts SET verified_status = ?, is_suspicious = 0, photo_suspicious_date = null WHERE id = ?", array(Model_Verifications_Requests::PHOTOSHOP, $escort_id));
				/* Edit Grigor */
				$this->getAdapter()->query("UPDATE escort_photos SET status = '" . Model_Escort_Photos::PHOTOSHOP . "' WHERE escort_id = ?", array($escort_id));
				$this->getAdapter()->update('verify_requests', array(
					'reason_ids' => '',
					'reason_text' => '',
					'rejected_date' => NULL
				), $this->getAdapter()->quoteInto('id = ?', $request_id));
			}
			elseif ( $status == Model_Verifications_Requests::REJECTED ) {
				$action = 'reject_verify_escort';
				
				$this->getAdapter()->query("UPDATE escort_photos SET status = ? WHERE escort_id = ? ", array(Model_Escort_Photos::NORMAL, $escort_id));
				$this->getAdapter()->query("UPDATE escorts SET verified_status = ? , need_idcard_video = ?  WHERE id = ?", array(Model_Escorts::STATUS_NOT_VERIFIED, $need_video, $escort_id));
				$this->getAdapter()->update('verify_requests', array(
					'reason_ids' => $reason_ids,
					'reason_text' => $free_text,
					'is_read' => 0,
					'rejected_date' => new Zend_Db_Expr('NOW()')
				), $this->getAdapter()->quoteInto('id = ?', $request_id));
			}
			elseif ( $status == Model_Verifications_Requests::BLUR_VERIFY ) {
				$action = 'blur_verify_escort';
				
				$this->getAdapter()->query("UPDATE escort_photos SET status = ? WHERE escort_id = ? ", array(Model_Escort_Photos::NORMAL, $escort_id));
				$this->getAdapter()->query("UPDATE escorts SET verified_status = ? , need_idcard_video = ?  WHERE id = ?", array(Model_Escorts::STATUS_NOT_VERIFIED, $need_video, $escort_id));
				$this->getAdapter()->update('verify_requests', array(
					'reason_ids' => '2',
					'reason_text' => '',
					'is_read' => 0,
					'rejected_date' => new Zend_Db_Expr('NOW()'),
					'blur_verify_ids' => serialize($blured_ids)
				), $this->getAdapter()->quoteInto('id = ?', $request_id));
			}
			
			if ( $status == self::CONFIRMED ) {
				$action = 'confirm_verify_request';
			}
			
			Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('action' => $action));

			$this->getAdapter()->query("UPDATE verify_requests SET status = ? {$confirm_date} WHERE id = ?", array($status, $request_id));

			if ( isset($action) ) {
				Model_Activity::getInstance()->log($action, array('escort id' => $escort_id, 'request id' => $request_id));
			}
		}
		
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		$this->setVerifierPerson($request_id, $bu_user->id);
	}
	
	public function setVerifierPerson($id, $bu_id)
	{
		$this->getAdapter()->update('verify_requests', array('verifier_person'=> $bu_id), $this->getAdapter()->quoteInto('id = ?', $id));
	}
	
	public function setRejectedReason($id, $text)
	{
		$this->getAdapter()->update('verify_requests', array('rejected_reason'=> $text), $this->getAdapter()->quoteInto('id = ?', $id));
	}
	
	public function addRequest($request)
	{
		try {
			$this->_db->insert('verify_requests', $request);
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		
		return $this->_db->lastInsertId();
	}
	public function addPhoto($data)
	{
		try {
			$this->getAdapter()->insert('verify_files', $data);
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
	public function getLastVerifiedId($escort_id)
	{
		 return $this->getAdapter()->fetchOne("SELECT id FROM verify_requests 
			WHERE status = ? AND escort_id = ? ORDER BY id DESC LIMIT 1 ", 
				array(self::VERIFY,$escort_id)); 
	}
}
