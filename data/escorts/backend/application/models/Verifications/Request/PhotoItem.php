<?php

class Model_Verifications_Request_PhotoItem extends Cubix_Model_Item
{
	/**
	 * The image utility library
	 *
	 * @var Cubix_Images
	 */
	protected static $_images;
	
	protected $_app_id = null;
	
	public function __construct($rowData = array())
	{
		parent::__construct($rowData);
		
		if ( empty(self::$_images) ) {
			self::$_images = new Cubix_Images();
		}
	}
	
	public function getUrl($size_name = null, $catalog = 'verify')
	{
		if ( ! is_null($this->_app_id) ) {
			throw new Exception('Please set the application id of photo');
		}
		
		$data = array(
			'application_id' => $this->application_id,
			'catalog_id' => $this->escort_id . '/'. $catalog,
			'hash' => $this->hash,
			'ext' => $this->ext
		);
		
		if ( $size_name ) {
			$data['size'] = $size_name;
		}
		
		return self::$_images->getUrl(new Cubix_Images_Entry($data));
	}
	
	public function getServerUrl($size_name = null, $catalog = 'verify')
	{
		if ( ! is_null($this->_app_id) ) {
			throw new Exception('Please set the application id of photo');
		}
		
		$data = array(
			'application_id' => $this->application_id,
			'catalog_id' => $this->escort_id . '/'. $catalog,
			'hash' => $this->hash,
			'ext' => $this->ext
		);
		
		if ( $size_name ) {
			$data['size'] = $size_name;
		}
		
		return self::$_images->getServerUrl(new Cubix_Images_Entry($data));
	}
}
