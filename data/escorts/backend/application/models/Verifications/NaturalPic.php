<?php

class Model_Verifications_NaturalPic extends Cubix_Model
{	
	const STATUS_PENDING = 1;
	const STATUS_VERIFIED = 2;
	const STATUS_REMOVED = 3;
	const STATUS_REJECTED = 4;
	
	public function getAll($p, $pp, $sort_field, $sort_dir, $filter, &$count)
	{
		$where = '';

		$bu_user = Zend_Auth::getInstance()->getIdentity();

		if ( $bu_user->type == 'sales manager' ) {
			$where .= self::quote(' AND u.sales_user_id = ?', $bu_user->id);
		}
		
		if ( strlen($filter['e.showname']) ) {
			$where .= self::quote(' AND e.showname LIKE ?', $filter['e.showname'] . '%');
		}
		
		if ( strlen($filter['agency_name']) ) {
			$where .= self::quote(' AND ag.name LIKE ?', $filter['agency_name'] . '%');
		}
		
		if ( strlen($filter['e.id']) ) {
			$where .= self::quote(' AND e.id = ?', $filter['e.id']);
		}
		
		if ( strlen($filter['status']) ) {
			$where .= self::quote(' AND np.status = ?', $filter['status']);
		}
		
		$sql = '
			SELECT 
				e.id AS escort_id, 
				e.showname,
				u.username,
				ag.name AS agency_name, 
				np.id, 
				UNIX_TIMESTAMP(np.creation_date) AS creation_date,
				np.status,
				UNIX_TIMESTAMP(np.update_date) AS update_date,
				bu.username as bu_user,
				np.reject_reason,
				u.id AS user_id
			FROM natural_pics np
			INNER JOIN escorts e ON np.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id
			LEFT JOIN agencies ag ON ag.id = e.agency_id
			LEFT JOIN backend_users bu ON bu.id = np.admin_id
			WHERE np.status <> 3
		';
		
		$countSql = '
			SELECT COUNT(np.id) FROM natural_pics np
			INNER JOIN escorts e ON np.escort_id = e.id 
			INNER JOIN users u ON u.id = e.user_id
			WHERE np.status <> 3
		';
		
		$sql .= $where;
		$countSql .= $where;
			
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
		';
		
		$count = $this->getAdapter()->fetchOne($countSql);
		
		return parent::_fetchAll($sql);
	}
	
	public function getPhotosByRequestId($id)
	{
		$photos = $this->getAdapter()->query('
			SELECT e.id as escort_id, u.application_id, avf.* FROM age_verification_files avf
			INNER JOIN age_verifications av ON av.id = avf.request_id
			INNER JOIN escorts e ON e.id = av.escort_id
			INNER JOIN users u ON u.id = e.user_id
			WHERE av.id = ?
		', $id)->fetchAll();
		
		
		foreach ($photos as $i => $photo) {
			$photos[$i] = new Model_Verifications_Request_PhotoItem($photo);
		}
		
		return $photos;
	}
		
	public function get($id)
	{
		$sql = "
			SELECT 
				e.id AS escort_id, 
				e.showname,
				np.id,
				np.hash,
				np.ext,
				UNIX_TIMESTAMP(np.creation_date) AS creation_date,
				np.status,
				UNIX_TIMESTAMP(np.update_date) AS update_date,
				np.admin_id,
				bu.username as bu_user,
				np.reject_reason,
				u.id AS user_id
			FROM natural_pics np
			INNER JOIN escorts e ON np.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id
			LEFT JOIN backend_users bu ON bu.id = np.admin_id
			WHERE np.id = ?
		";
		
		$nat_photo = $this->getAdapter()->fetchRow($sql, array($id));
		$nat_photo->photo = new Model_Verifications_Request_PhotoItem($nat_photo );
		return $nat_photo;
	}
	
	public function getRejectReasons($escort_id)
	{
		$sql = "
			SELECT 
				np.update_date AS reject_date,
				bu.username as bu_user,
				np.reject_reason
			FROM natural_pics np
			INNER JOIN backend_users bu ON bu.id = bp.admin_id
			WHERE np.status = ? AND np.escort_id = ?
		";
		
		return $this->getAdapter()->FetchAll($sql, array(self::REJECTED,$escort_id));
	}
	
	public function setStatus($escort_id, $request_id, $status, $reason = null)
	{
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		$app_id = Cubix_Application::getId();
		$m = new Model_Escorts();
	 	$esc = $m->get($escort_id); 
		
		$this->getAdapter()->update('natural_pics', array(
			'update_date' => new Zend_Db_Expr('NOW()'),
			'admin_id' => $bu_user->id,
			'reject_reason' => $reason,
			'status' => $status //status 2 is approved 4 is rejected
		), $this->getAdapter()->quoteInto( 'id = ?', $request_id));
		Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('action' => 'Natural Pic Action - '.$status));
		
		if($app_id == APP_ED && $status == 2){
			
			if(is_numeric($esc->agency_id)){
				$email_template = 'natural_photo_approved_agency_v1';
			}else{
				$email_template = 'natural_photo_approved_v1';
			}
			Cubix_Email::sendTemplate( $email_template, $esc->email, 
				array(
					'showname' => $esc->showname,
					'escort_showname' => $esc->showname,
					'agency' => $esc->agency_name,
					'url' => 'http://www.'.Cubix_Application::getById()->host.'/escort/'.$esc->showname.'-'.$esc->id,
				)
			);
			
		} elseif($app_id == APP_ED && $status == 4){
			if(is_numeric($esc->agency_id)){
				$email_template = 'natural_photo_rejected_agency_v1';
			}else{
				$email_template = 'natural_photo_rejected_v1';
			}
			Cubix_Email::sendTemplate( $email_template, $esc->email, 
				array(
					'showname' => $esc->showname,
					'escort_showname' => $esc->showname,
					'reason' => $reason,
					'agency' => $esc->agency_name,
					'url' => 'http://www.'.Cubix_Application::getById()->host.'/escort/'.$esc->showname.'-'.$esc->id,
				)
			);
		}  
	}
	
	public function getEscortData($escort_id)
	{
		$sql = " SELECT e.showname, u.email, ep.contact_phone_parsed FROM escorts e
				INNER JOIN users u ON u.id = e.user_id
				INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
				WHERE e.id = ? ";
		
		return $this->getAdapter()->fetchRow($sql, $escort_id);
	}
	
		
	public function updateEscort($escort_id, $data)
	{
		$this->_db->update('escorts', $data , array( $this->_db->quoteInto('id = ?', $escort_id)));
	}
	
	public function add($data)
	{
		$this->_db->insert('natural_pics', $data);
		Cubix_SyncNotifier::notify($data['escort_id'], Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('action' => 'Natural Pic Action Added  '));
	}
	
}
