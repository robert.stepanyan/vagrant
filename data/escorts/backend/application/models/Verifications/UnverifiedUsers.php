<?php

class Model_Verifications_UnverifiedUsers extends Cubix_Model
{	
	const STATUS_PENDING = 1;
	const STATUS_VERIFIED = 2;
	const STATUS_REMOVED = 3;
	const STATUS_REJECTED = 4;
	
	public function getAll($p, $pp, $sort_field, $sort_dir, $filter)
	{
		$where = "";

		$bu_user = Zend_Auth::getInstance()->getIdentity();

		if ( strlen($filter['u.email']) ) {
			$where .= " AND u.email != 'no@no.ch' AND u.email LIKE '" .  $filter['u.email']."'";
			
		}

		if ( strlen($filter['u.user_type']) && $filter['u.user_type'] != 'all' ) {
			$where .= " AND u.user_type LIKE '" .  $filter['u.user_type']."'";
		}

		if ( strlen($filter['uu.status']) && $filter['uu.status'] != 'all' ) {
			if ($filter['uu.status'] == 'not_notified') {
				$where .= " AND uu.status IS NULL";
			}elseif($filter['uu.status'] == 'email_sent'){
				$where .= " AND uu.status = 0";
			}elseif ($filter['uu.status'] == 'returned_user') {
				$where .= " AND uu.status = 1";
			}
		}
		
		if ( strlen($filter['u.date_registered']) ) {
			$date =  gmdate("Y-m-d", $filter['u.date_registered']);
			$where .= " AND u.date_registered BETWEEN '".$date ." 00:00:00' AND '".$date ." 23:59:59'";
		}
		
		$sql = "SELECT u.id AS u_id,
				IF(user_type='escort' , e.id, 	
				IF(user_type='member' , u.id,
				IF(user_type='agency' ,a.id, NULL ) ) ) as id,
				u.username as u_username,
				u.activation_hash as u_activation_hash,
				u.email as u_email,
				u.user_type as u_type,
				DATE_FORMAT(u.date_registered,'%d-%m-%Y')  AS reg_date,
				IF(	uu.`status` = 0 , 'Email Sent', 	
				IF(	uu.`status` = 1  , 'Returned User', 'Not notified' ) ) as status
			FROM users u
			LEFT join escorts e ON e.user_id = u.id
			LEFT join agencies a ON a.user_id = u.id
			LEFT JOIN users_unverified uu ON uu.user_id = u.id
			WHERE ( u.status LIKE -1 or uu.status = 1 or uu.status = 0 )
		";
		
		$countSql = 'SELECT COUNT(u.id) FROM users u LEFT JOIN users_unverified uu ON uu.user_id = u.id
		 WHERE ( u.status LIKE -1 or uu.status = 1 or uu.status = 0 )';
		
		$sql .= $where;
		$countSql .= $where;
			
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
		';

		$count = $this->getAdapter()->fetchOne($countSql);

		$data = parent::_fetchAll($sql);
		
		return array('data' => $data, 'count' => $count) ;
		
	}
	public function send_mail($username, $email, $reg_type, $activation_hash, $user_id, $reg_date){
			
		$user_already_get_email_2nd_time = $this->getAdapter()->fetchOne("SELECT user_id FROM users_unverified WHERE user_id = '".$user_id."'");
		

		if(!is_int($user_already_get_email_2nd_time)){

			$reg_datetime = strtotime($reg_date);
			$lastyear = strtotime("-1 year", strtotime(date('d-m-Y')));
			
			if($reg_datetime < $lastyear){
				$smtp2 = true;
				$template_id = 'signup_success_old_users_and6-mail.net';

			}else{
				$smtp2 = false;
				$template_id = 'signup_success_old_users';
			}

			if ( in_array(Cubix_Application::getId(), array( APP_A6 )) ) {
				$language = 'de';
				
			}else{
				$language = 'en';
				$template_id = 'signup_success';
			}

			$status = Cubix_Email::sendTemplate($template_id, $email, array(
				'activation_hash' => $activation_hash,
				'email' => $email,
				'username' => $username,
				'reg_type' => $reg_type
			), null , null , $language, $smtp2);
			
			if($status == true){
				$user_arr['user_id'] = $user_id;
				$user_arr['verification_notify_date'] =  date('Y-m-d h:i:s', time());
				$user_arr['status'] = 0;
				$this->insert_unverified_users($user_arr);
				return array('status'=>'success', 'message'=>'Email Sent successfully');
			}else{
				return array('status'=>'error', 'message'=>'Unexpected error');
			}
			
		}else
		{
			return array('status'=>'error', 'message'=>'This user already received notified second time');
		}
	}
	
	public function insert_unverified_users($user_arr){
		// only user(s) who get email for verificaiton second time are will be inserted in DB table users_unverified
		parent::getAdapter()->insert('users_unverified', $user_arr);
	}
	
	public function sync(){
		$status = $this->getAdapter()->query('update users_unverified uu INNER JOIN users u on u.id = uu.user_id and u.status != -1 set uu.status = 1');
		return array('status'=>'success', 'message'=> $status);
	}
}
