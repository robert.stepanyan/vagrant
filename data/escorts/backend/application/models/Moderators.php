<?php
class Model_Moderators extends Cubix_Model
{
	protected $_table = 'backend_users';
	protected $_itemClass = 'Model_ModeratorItem';
	
	public function getByApplicationId( $application_id )
	{
		$sql = "SELECT * FROM backend_users WHERE is_disabled = 0 AND application_id = ? ORDER BY username";
		
		return parent::_fetchAll($sql, array($application_id));
	}

	public function getByType($type, $app_id)
	{
		if ( ! is_array($type) ) {
			$type = array($type);
		}

		if ( is_array($type) ) {
			foreach ( $type as $i => $t ) {
				$type[$i] = parent::getAdapter()->quote($t);
			}
			$type = implode(',', $type);
		}

		$sql = "SELECT * FROM backend_users WHERE is_disabled = 0 AND type IN (" . $type . ") AND application_id = ? ORDER BY username";

		return parent::_fetchAll($sql, array($app_id));
	}
}
