<?php

class Model_TopFavorites extends Cubix_Model
{
    public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS 
				f.id, f.user_id, u.username, u.status 
			FROM favorites f
			INNER JOIN users u ON u.id = f.user_id
			WHERE f.rank > 0 
		';

		$countSql = 'SELECT FOUND_ROWS()';
				
		if ( strlen($filter['username']) ) {
			$sql .= self::quote('AND u.username LIKE ?', '%' . $filter['username'] . '%');
		}
		
		if ( strlen($filter['escort_id']) ) {
			$sql .= self::quote('AND f.escort_id = ?', $filter['escort_id']);
		}
		
		$sql .= '
			GROUP BY f.user_id 
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$ret = parent::_fetchAll($sql);
		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return $ret;
	}
	
	public function getTopEscorts($user_id)
	{
		return $this->getAdapter()->query('
			SELECT f.escort_id, f.rank, e.showname 
			FROM favorites f 
			INNER JOIN escorts e ON f.escort_id = e.id 
			WHERE f.user_id = ? AND f.rank > 0
			ORDER BY f.rank ASC
		', $user_id)->fetchAll();
	}
}
