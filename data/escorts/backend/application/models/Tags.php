<?


class Model_Tags  extends Cubix_Model {
	protected $_table = 'tags';

	public function getAll()
    {
        return parent::_fetchAll('SELECT * FROM tags WHERE application_id = '.Cubix_Application::getId());
    }

    public function saveEscortsTags($tags,$escort_id)
    {
        if (count($tags))
        {
            parent::getAdapter()->delete('escort_tags',parent::quote('escort_id = ?', $escort_id));
            foreach ( $tags as $tag )
            {
                parent::getAdapter()->insert('escort_tags', $tag);
            }
        }
    }
}
