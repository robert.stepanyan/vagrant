<?php

class Model_Packages extends Cubix_Model
{
	protected $_request;
	
	protected $_table = 'packages';
	protected $_itemClass = 'Model_PackageItem';

	const ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES = 6;
	const ACTIVATE_AT_EXACT_DATE = 7;
	const ACTIVATE_ASAP = 9;

	public static $ACTIVATE_LABELS = array(
		self::ACTIVATE_ASAP => 'ASAP',
		self::ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES => 'after current package expires',
		self::ACTIVATE_AT_EXACT_DATE => 'at exact date'
	);
	
	public static $ACTIVATE_LABELS_AP = array(
		self::ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES => 'after current package expires',
		self::ACTIVATE_ASAP => 'ASAP',		
		self::ACTIVATE_AT_EXACT_DATE => 'at exact date'
	);
	
	const STATUS_PENDING  = 1;
	const STATUS_ACTIVE   = 2;
	const STATUS_EXPIRED  = 3;
	const STATUS_CANCELLED = 4;
	const STATUS_UPGRADED = 5;
	const STATUS_SUSPENDED = 6;


	public static $STATUS_LABELS = array(
		self::STATUS_PENDING  => 'pending',
		self::STATUS_ACTIVE   => 'active',
		self::STATUS_EXPIRED  => 'expired',
		self::STATUS_CANCELLED => 'cancelled',
		self::STATUS_UPGRADED => 'upgraded',
		self::STATUS_SUSPENDED => 'suspended'
	);

	public function get($id)
	{
		$sql = '
			SELECT
				p.*
			FROM packages p
			WHERE 1 AND p.id = ?
		';
			
		return parent::_fetchRow($sql, array($id));
	}

	public function getWithPrice($app_id, $user_type, $gender, $package_id = null, $pseudo_escort = false, $base_price = null, $profile_type = null, $escort_type = null)
	{
		
		if ( $user_type ) {
			$user_type = USER_TYPE_AGENCY;
		}
		else {
			$user_type = USER_TYPE_SINGLE_GIRL;
		}

		if ( $pseudo_escort ) {
			$user_type = USER_TYPE_AGENCY;
		}

		$where = "";
		if ( $package_id ) {
			$where .= " AND p.id = {$package_id} ";
		}

		if ( $base_price ) {
			$where .= " AND pp.price > {$base_price} ";
		}

		if ($escort_type){
		    $where .= " AND pp.category = {$escort_type} ";
        }

		if ( Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT ) {
			/*$bu_user = Zend_Auth::getInstance()->getIdentity();

			if ( $bu_user->username != "david" ) {
				$where .= " AND p.is_for_phone_billing = 0 ";
			}*/
		}

		if ( $profile_type == PROFILE_TYPE_DUO ) {
			if ( $gender == GENDER_FEMALE ) {
				$gender = GENDER_DUO_FEMALE;
			} elseif ( $gender == GENDER_MALE ) {
				$gender = GENDER_DUO_MALE;
			} elseif ( $gender == GENDER_TRANS ) {
				$gender = GENDER_DUO_TRANS;
			} elseif ( $gender == GENDER_DOMINA ) {
				$gender = GENDER_DUO_DOMINA;
			} elseif ( $gender == GENDER_BDSM ) {
				$gender = GENDER_DUO_BDSM;
			}
		}
		
		$sql = "
			SELECT 
				p.id,
				p.name,
				p.available_for,
				pp.price,
				p.period,
				p.is_default
            FROM packages p
			INNER JOIN package_prices pp ON pp.package_id = p.id
            WHERE
				p.application_id = ?
                AND pp.user_type = ?
                AND pp.gender = ?
                AND p.is_active = 1
				{$where}			
		";

		return parent::_fetchAll($sql, array($app_id, $user_type, $gender));
	}

    public function getActivePackages($app_id){
        
        $result = parent::_fetchAll('
			SELECT id, name AS title
			FROM packages
            WHERE is_active = 1 AND application_id = ?
		',array($app_id));

		$new = array();
		foreach ( $result as $i => $row ) {
			$new[$row->id] = $row->title;
		}

		return $new;
    }
	
	public function getPackages($app_id){
        
        $result = parent::_fetchAll('
			SELECT id, name AS title
			FROM packages
            WHERE application_id = ?
		',array($app_id));

		$new = array();
		foreach ( $result as $i => $row ) {
			$new[$row->id] = $row->title;
		}

		return $new;
    }	

	public function getOptProducts($package_id, $app_id, $user_type, $gender, $pseudo_escort = false, $show_hiddens = true, $profile_type = null, $escort_type = null)
	{
		if ( $user_type ) {
			$user_type = USER_TYPE_AGENCY;
		}
		else {
			$user_type = USER_TYPE_SINGLE_GIRL;
		}

		if ( $pseudo_escort ) {
			$user_type = USER_TYPE_AGENCY;
		}

		if ( $profile_type == PROFILE_TYPE_DUO ) {
			if ( $gender == GENDER_FEMALE ) {
				$gender = GENDER_DUO_FEMALE;
			} elseif ( $gender == GENDER_MALE ) {
				$gender = GENDER_DUO_MALE;
			} elseif ( $gender == GENDER_TRANS ) {
				$gender = GENDER_DUO_TRANS;
			} elseif ( $gender == GENDER_DOMINA ) {
				$gender = GENDER_DUO_DOMINA;
			} elseif ( $gender == GENDER_BDSM ) {
				$gender = GENDER_DUO_BDSM;
			}
		}

		$where = "";
		if ($app_id == APP_ED && $escort_type){
		    $where = " AND pr.escort_type = {$escort_type} ";
        }
		$sql = "
		SELECT
				pp.product_id, p.name, pr.price
			FROM package_products pp
			INNER JOIN products p ON p.id = pp.product_id
			INNER JOIN product_prices pr ON pr.product_id = p.id
			WHERE pp.is_optional = 1 AND pp.package_id = ? AND pr.application_id = ? AND pr.user_type = ? AND pr.gender = ? {$where}
		". ( ! $show_hiddens ? 'AND p.is_hidden = 0' : '' );

		$addon_products = parent::getAdapter()->query($sql, array($package_id, $app_id, $user_type, $gender))->fetchAll();

		$products = array();
		foreach($addon_products as $k => $product)
		{
			$products[$k]['id'] = $product->product_id;
			$products[$k]['name'] = Cubix_I18n::translate($product->name);
			$products[$k]['price'] = $product->price;
		}

		return $products;
	}

	public function getProducts($package_id, $app_id, $user_type, $gender, $pseudo_escort = false, $profile_type = null, $escort_type = null)
	{
		if ( $user_type ) {
			$user_type = USER_TYPE_AGENCY;
		}
		else {
			$user_type = USER_TYPE_SINGLE_GIRL;
		}

		if ( $pseudo_escort ) {
			$user_type = USER_TYPE_AGENCY;
		}

		if ( $profile_type == PROFILE_TYPE_DUO ) {
			if ( $gender == GENDER_FEMALE ) {
				$gender = GENDER_DUO_FEMALE;
			} elseif ( $gender == GENDER_MALE ) {
				$gender = GENDER_DUO_MALE;
			} elseif ( $gender == GENDER_TRANS ) {
				$gender = GENDER_DUO_TRANS;
			} elseif ( $gender == GENDER_DOMINA ) {
				$gender = GENDER_DUO_DOMINA;
			} elseif ( $gender == GENDER_BDSM ) {
				$gender = GENDER_DUO_BDSM;
			}
		}

		if ($app_id == APP_ED && $escort_type){
		    $where = " AND pr.escort_type = {$escort_type} ";
        }
		$sql = "
		SELECT
				pp.product_id, p.name, pr.price
			FROM package_products pp
			INNER JOIN products p ON p.id = pp.product_id
			INNER JOIN product_prices pr ON pr.product_id = p.id
			WHERE pp.is_optional = 0 AND pp.package_id = ? AND pr.application_id = ? AND pr.user_type = ? AND pr.gender = ? {$where}
		";

		$productss = parent::getAdapter()->query($sql, array($package_id, $app_id, $user_type, $gender))->fetchAll();


		$products = array();
		foreach($productss as $k => $product) {
			$products[$k]->id = $product->product_id;
			$products[$k]->name = Cubix_I18n::translate($product->name);
			$products[$k]->price = $product->price;
		}

		return $products;
	}

	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count = 0)
	{
		/*$items = parent::getAdapter()->query('SELECT id, products__, addon_products__ FROM packages')->fetchAll();

		foreach($items as $item)
		{
			if ( $item->products__ )
			{
				foreach(explode(',', $item->products__) as $prod)
				{
					parent::getAdapter()->insert('package_products', array('package_id' => $item->id, 'product_id' => $prod));
				}
			}
			
			if ( $item->addon_products__ )
			{
				foreach(explode(',', $item->addon_products__) as $prod)
				{
					parent::getAdapter()->insert('package_products', array('package_id' => $item->id, 'product_id' => $prod, 'is_optional' => 1));
				}
			}
		}
		die;*/
		
		$sql = '
			SELECT
				p.*, a.title AS application
			FROM packages p
			INNER JOIN applications a ON a.id = p.application_id
			WHERE 1
		';

		$countSql = '
			SELECT COUNT(DISTINCT(p.id))
			FROM packages p
			WHERE 1
		';

		$where = '';
		if ( strlen($filter['p.application_id']) ) {
			$where .= self::quote('AND p.application_id = ?', $filter['p.application_id']);
		}
		
		$sql .= $where;
		$countSql .= $where;

		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$count = intval($this->getAdapter()->fetchOne($countSql));

		return parent::_fetchAll($sql);
	}

	public function save($data)
	{
		parent::getAdapter()->insert($this->_table, $data['package_data']);

		$package_id = parent::getAdapter()->lastInsertId();

		if ( count($data['prices']) > 0 )
		{
			$new_price_data = array();
			foreach($data['prices'] as $price)
			{
				$price_data = explode(':', $price);

				$new_price_data['package_id'] = $package_id;
				$new_price_data['user_type'] = $price_data[0];
				$new_price_data['gender'] = $price_data[1];
				$new_price_data['price'] = $price_data[2];
				$new_price_data['crypto_price'] = $price_data[3];
                if (Cubix_Application::getId() == APP_ED){
                    $new_price_data['category'] = $price_data[4];
                }

				parent::getAdapter()->insert('package_prices', $new_price_data);
			}
		}

		if ( count($data['products']) > 0 )
		{
			//$products = implode(',', $data['products']);
			foreach($data['products'] as $product)
			{
				parent::getAdapter()->insert('package_products', array('package_id' => $package_id, 'product_id' => $product));
			}
		}

		if ( count($data['opt_products']) > 0 )
		{
			//$products = implode(',', $data['opt_products']);
			foreach($data['opt_products'] as $product)
			{
				parent::getAdapter()->insert('package_products', array('package_id' => $package_id, 'product_id' => $product, 'is_optional' => 1));
			}
		}
	}
	
	public function update($data)
	{
		$package_id = $data['package_data']['id'];
		unset($data['package_data']['id']);
		
		parent::getAdapter()->update($this->_table, $data['package_data'], parent::quote('id = ?', $package_id));

		parent::getAdapter()->query('DELETE FROM package_prices WHERE package_id = ?', array($package_id));
		if ( count($data['prices']) > 0 )
		{
			$new_price_data = array();
			foreach($data['prices'] as $price)
			{
				$price_data = explode(':', $price);

				$new_price_data['package_id'] = $package_id;
				$new_price_data['user_type'] = $price_data[0];
				$new_price_data['gender'] = $price_data[1];
				$new_price_data['price'] = $price_data[2];
				$new_price_data['crypto_price'] = $price_data[3];
                if (Cubix_Application::getId() == APP_ED){
                    $new_price_data['category'] = $price_data[4];
                }
				
				parent::getAdapter()->insert('package_prices', $new_price_data);
			}
		}

		if ( count($data['products']) > 0 )
		{
			try
			{
				parent::getAdapter()->delete('package_products', array(
					parent::quote('package_id = ?', $package_id,
					parent::quote('is_optional = ?', 0)
				)));

				foreach($data['products'] as $product)
				{
					parent::getAdapter()->insert('package_products', array('package_id' => $package_id, 'product_id' => $product));
				}
			}
			catch ( Exception $e ) {
				throw $e;
			}
		}

		parent::getAdapter()->delete('package_products', array(
			parent::quote('package_id = ?', $package_id),
			parent::quote('is_optional = ?', 1),
		));
		if ( count($data['opt_products']) > 0 )
		{
			foreach($data['opt_products'] as $product)
			{
				parent::getAdapter()->insert('package_products', array('package_id' => $package_id, 'product_id' => $product, 'is_optional' => 1));
			}
		}
	}

	public function remove($id)
	{
		parent::getAdapter()->delete($this->_table, parent::quote('id = ?', $id));
		parent::getAdapter()->delete('package_prices', parent::quote('package_id = ?', $id));
		parent::getAdapter()->delete('package_products', parent::quote('package_id = ?', $id));
		//parent::getAdapter()->delete('package_addon_products', parent::quote('package_id = ?', $id));
	}

	public function toggle($id, $type)
	{
		$field = 'is_default';
		if ( $type == 'active' ) {
			$field = 'is_active';
		}
		parent::getAdapter()->update($this->_table, array(
			$field => new Zend_Db_Expr('NOT ' . $field)
		), parent::quote('id = ?', $id));
	}

	public function hasActivePackage($escort_id)
	{
		return parent::getAdapter()->fetchOne('
			SELECT COUNT(*) 
			FROM order_packages o_p
			WHERE o_p.escort_id = ? AND o_p.status = ?',
			array($escort_id, Model_Packages::STATUS_ACTIVE)
		);
	}
	
	public function getActivePackage($escort_id)
	{
		return parent::getAdapter()->fetchOne('
			SELECT id
			FROM order_packages o_p
			WHERE o_p.escort_id = ? AND o_p.status = ?',
			array($escort_id, Model_Packages::STATUS_ACTIVE)
		);
	}
	
	//Temporary function to set previous_order_package_id field
	public static function fixPrevioussPackages()
	{
		//Getting pending packages with activation type after current package expires
		$packages = parent::getAdapter()->fetchAll('
			SELECT op.id, op.escort_id, op1.id AS previous_package_id
			FROM order_packages op
			INNER JOIN order_packages op1 ON op1.escort_id = op.escort_id AND op1.status = ?
			WHERE op.status = ? AND op.activation_type = ?
		',array(self::STATUS_ACTIVE, self::STATUS_PENDING, self::ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES));
		
		foreach($packages as $package) {
			parent::getAdapter()->update('order_packages', array('previous_order_package_id' => $package->previous_package_id), parent::quote('id = ?', $package->id));
		}
	}
	
	public static function getZeroPackageId()
	{
		return parent::getAdapter()->fetchOne('
			SELECT id
			FROM packages 
			WHERE name = "Zero Package" AND is_active= 1'
			
		);
	}

	public static function getMinimumPackageId()
	{
		return parent::getAdapter()->fetchOne('
			SELECT id
			FROM packages 
			WHERE name = "Minimum" AND is_active= 1'
			
		);
	}

	public static function getMonthlyPackageId()
	{
		// monthly_packages IDs
		$monthly_package = [ 
			APP_ED => 128, 
			APP_EG_CO_UK => 106 
		];
		
		return $monthly_package[Cubix_Application::getId()];
	}
}
