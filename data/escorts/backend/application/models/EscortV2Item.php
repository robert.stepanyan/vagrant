<?php

class Model_EscortV2Item extends Cubix_Model_Item
{
	const HH_STATUS_ACTIVE  = 1;
	const HH_STATUS_PENDING = 2;
	const HH_STATUS_EXPIRED = 3;
	const HH_STATUS_ADMIN_DISABLED = 4;
	
	public function cancelPackages($comment)
	{
		$this->_adapter->beginTransaction();

		try {
			$this->_adapter->query('
				UPDATE order_packages SET status = ?, status_comment = ? WHERE escort_id = ?
			', array(
				Model_Billing_Packages::STATUS_CANCELLED,
				$comment,
				$this->getId()
			));

			$this->_adapter->commit();
		}
		catch (Exception $e) {
			$this->_adapter->rollBack();

			throw $e;
		}
	}

	public function getLangs()
	{
		return $this->_adapter->query('
			SELECT lang_id AS id, level FROM escort_langs
			WHERE escort_id = ?
		', $this->getId())->fetchAll();
	}

	public function hasPendingPackage($app_id)
	{
		$sql = "SELECT TRUE FROM order_packages WHERE escort_id = ? AND application_id = ? AND status = ?";
		
		return $this->_adapter->fetchOne($sql, array($this->getId(), $app_id, Model_Packages::STATUS_PENDING));
	}

	public function processDefaultPackages()
	{
		$applications = Cubix_Application::getAll();
		$active_packages = $this->hasActivePackages();

		foreach ( $applications as $app ) {
			if ( ! in_array($app->id, $active_packages) ) {
				$this->activateDefaultPackage($app->id);
			}
		}
	}

	public function hasActivePackages()
	{
		$applications = $this->_adapter->fetchAll('
			SELECT
				a.id
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN order_packages op ON op.escort_id = e.id
			INNER JOIN packages p ON op.package_id = p.id
			INNER JOIN applications a ON a.id = op.application_id
			INNER JOIN countries c ON c.id = a.country_id
			WHERE e.id = ? AND op.status = ?
		', array($this->getId(), Model_Billing_Packages::STATUS_ACTIVE));

		$result = array();
		foreach ( $applications as $app ) {
			$result[] = $app->id;
		}

		return $result;
	}

	/**
	 * Requires: id, agency_id, gender, country_id
	 *
	 * @param int $application_id
	 * @return
	 */
	public function activateDefaultPackage($application_id)
	{
		$db = $this->_adapter;

		try {
			$db->beginTransaction();

			$escort_id = $this->getId();

			/* --> Get the default package for this application */
			if ( $this->country_id == Cubix_Application::getById($application_id)->country_id ) {
				$type = PROD_AVAILABLE_FOR_DOMESTIC;
			}
			else {
				$type = PROD_AVAILABLE_FOR_INTERNATIONAL;
			}

			/* --> Workaround for disabling international default packages */
			if ( $type == PROD_AVAILABLE_FOR_INTERNATIONAL ) {
				return;
			}
			/* <-- */

			$package = $db->fetchRow('
				SELECT
					p.*
				FROM packages p
				WHERE
					p.application_id = ? AND
					p.available_for = ? AND
					p.is_default = 1 AND
					p.is_active = 1
			', array($application_id, $type));

			if ( ! $package ) {
				$package = $db->fetchRow('
					SELECT
						p.*
					FROM packages p
					WHERE
						p.application_id = ? AND
						p.available_for = ? AND
						p.is_default = 1 AND
						p.is_active = 1
				', array($application_id, PROD_AVAILABLE_FOR_ALL));
			}

			if ( ! $package ) {
				return;
			}
			/* <-- */

			$db->insert('order_packages', array(
				'order_id' => null,
				'escort_id' => $escort_id,
				'package_id' => $package->id,
				'application_id' => $application_id,
				'base_period' => null,
				'period' => null,
				'date_activated' => new Zend_Db_Expr('NOW()'),
				'status' => Model_Billing_Packages::STATUS_ACTIVE,
				'activation_type' => null,
				'base_price' => 0,
				'price' => 0
			));

			$order_package_id = $db->lastInsertId();
            $escort_type = null;
            if (Cubix_Application::getId() == APP_ED) {
                $escort_type = Model_EscortV2Item::getEscortTypeByID($escort_id);
            }
			$model = new Model_Packages();
			$products = $model->getProducts($package_id, $application_id, $this->agency_id, $this->gender, false, null, $escort_type);

			foreach ( $products as $product ) {
				$product_data = array(
					'order_package_id' => $order_package_id,
					'product_id' => $product->id,
					'is_optional' => 0,
					'price' => $product->price
				);

				$db->insert('order_package_products', $product_data);
			}

			$db->commit();
		}
		catch ( Exception $e ) {
			$db->rollBack();

			throw $e;
		}
	}

	public function getTotalOrdersPrice($is_paid = false)
	{
		$bind = array($this->getId());
		if ( $is_paid ) {
			$bind = array(
				$this->getId(),
				Model_Billing_Orders::STATUS_PAID
			);
			$where = ' AND o.status = ? ';
		}

		$sql = "
			SELECT				
				SUM(o.price) AS total_orders_price
			FROM orders o
			INNER JOIN users u ON u.id = o.user_id
			INNER JOIN escorts e ON u.id = e.user_id
			WHERE e.id = ? {$where}
		";

		return $this->_adapter->query($sql, $bind)->fetchColumn();
	}

	public function getActivePackages()
	{
        $join = " INNER JOIN countries c ON c.id = a.country_id ";
        if (Cubix_Application::getId() == APP_ED){
            $join = " LEFT JOIN countries c ON c.id = a.country_id ";
        }
		$sql = '
			SELECT
				op.id, op.order_id, op.escort_id, op.package_id,
				op.period, UNIX_TIMESTAMP(op.date_activated) AS date_activated, UNIX_TIMESTAMP(op.expiration_date) AS expiration_date,
				op.price,
				p.name AS package_name,
				a.title AS application_title,
				c.iso AS app_iso,
				bu.username AS sales_person
			FROM escorts e
			INNER JOIN order_packages op ON op.escort_id = e.id
			LEFT JOIN orders o ON o.id = op.order_id
			INNER JOIN packages p ON p.id = op.package_id
			INNER JOIN applications a ON a.id = op.application_id
			LEFT JOIN backend_users bu ON bu.id = o.backend_user_id
			'.$join.'
			WHERE e.id = ? AND op.status = ?
		';

		return $this->_adapter->query($sql, array($this->getId(), Model_Packages::STATUS_ACTIVE))->fetchAll();
	}

	/*public function getAllPackages()
	{
		$sql = '
			SELECT
				op.id, op.order_id, op.escort_id, op.package_id,
				op.period, UNIX_TIMESTAMP(op.date_activated) AS date_activated, UNIX_TIMESTAMP(op.expiration_date) AS expiration_date,
				op.price,
				p.name AS package_name,
				a.title AS application_title,
				c.iso AS app_iso,
				bu.username AS sales_person
			FROM escorts e
			INNER JOIN order_packages op ON op.escort_id = e.id
			LEFT JOIN orders o ON o.id = op.order_id
			INNER JOIN packages p ON p.id = op.package_id
			INNER JOIN applications a ON a.id = op.application_id
			LEFT JOIN backend_users bu ON bu.id = o.backend_user_id
			INNER JOIN countries c ON c.id = a.country_id
			WHERE e.id = ? AND op.status <> ?
		';

		return $this->_adapter->query($sql, array($this->getId(), Model_Packages::STATUS_ACTIVE))->fetchAll();
	}*/
	
	public function getAllPackages()
	{
		$packs = array(Model_Packages::STATUS_CANCELLED, Model_Packages::STATUS_EXPIRED, Model_Packages::STATUS_UPGRADED);
		$packs = implode(',', $packs);
		$join = " INNER JOIN countries c ON c.id = a.country_id ";
		if (Cubix_Application::getId() == APP_ED){
		    $join = " LEFT JOIN countries c ON c.id = a.country_id ";
        }
		$sql = '
			SELECT
				op.id, op.order_id, op.escort_id, op.package_id,
				op.period, UNIX_TIMESTAMP(op.date_activated) AS date_activated, UNIX_TIMESTAMP(op.expiration_date) AS expiration_date,
				op.price,
				p.name AS package_name,
				a.title AS application_title,
				c.iso AS app_iso,
				bu.username AS sales_person
			FROM escorts e
			INNER JOIN order_packages op ON op.escort_id = e.id
			LEFT JOIN orders o ON o.id = op.order_id
			INNER JOIN packages p ON p.id = op.package_id
			INNER JOIN applications a ON a.id = op.application_id
			LEFT JOIN backend_users bu ON bu.id = o.backend_user_id
			'.$join.'
			WHERE e.id = ? AND op.status IN (' . $packs . ')
		';

		return $this->_adapter->query($sql, $this->getId())->fetchAll();
	}
	
	public function getCities()
	{
		return $this->_adapter->query('
			SELECT c.id, c.' . Cubix_I18n::getTblField('title') . ' AS title FROM cities c
			INNER JOIN escort_cities ec ON ec.city_id = c.id
			WHERE ec.escort_id = ?
		', $this->getId())->fetchAll();
	}
	
	public function getWorkTimes()
	{
		$data = $this->_adapter->query('
			SELECT day_index, time_from, time_to FROM escort_working_times
			WHERE escort_id = ?
		', $this->getId())->fetchAll();
		
		$result = array();
		foreach ( $data as $row ) {
			$result[$row->day_index] = array(
				'from' => $row->time_from,
				'to' => $row->time_to
			);
		}
		
		return $result;
	}
	
	public function getRates()
	{
		return $this->_adapter->query('
			SELECT availability, time, time_unit, price, currency_id FROM escort_rates
			WHERE escort_id = ?
		', $this->getId())->fetchAll();
	}
	
	public function getPhotos($sort_field = null, $sort_dir = null, $no_retouch = false, $no_archive = false)
	{
		if ( ! $sort_field || ! $sort_dir )
		{
			$sort_field = "ep.is_approved ASC, ep.ordering";
			$sort_dir = " ASC";
		}
		$where = '';
		if($no_retouch){
			$where .= " AND retouch_status <> ".Model_RetouchPhotos::NEEDS_RETOUCH;  
		}
		if($no_archive){
			$where .= " AND ep.type <> ".ESCORT_PHOTO_TYPE_ARCHIVED;  
		}
		$sort = $sort_field . ' ' . $sort_dir;
		if (in_array(Cubix_Application::getId(),array(APP_EF)) ){
		    $sort = " ep.is_approved ASC, ep.is_main DESC, ep.ordering ASC ";
        }elseif (Cubix_Application::getId() == APP_A6){
            $sort = " ep.ordering ASC, ep.id ASC ";
        }

		$photos = $this->_adapter->query('
			SELECT u.application_id, ep.* FROM escort_photos ep
			INNER JOIN escorts e ON e.id = ep.escort_id
			INNER JOIN users u ON u.id = e.user_id
			WHERE ep.escort_id = ? '.$where.' ORDER BY ' . $sort . '
		', $this->getId())->fetchAll();
		
		foreach ($photos as $i => $photo) {
			$photos[$i] = new Model_Escort_PhotoItem($photo);
		}
		
		return $photos;
	}

	public function getPhotosForEDSync()
	{
	    $limit = 10;
	    if (Cubix_Application::getId() == APP_EF){
	        $limit = 50;
        }
		$photos = $this->_adapter->fetchAll('
			SELECT u.application_id, ep.* FROM escort_photos ep
			INNER JOIN escorts e ON e.id = ep.escort_id
			INNER JOIN users u ON u.id = e.user_id
			WHERE ep.escort_id = ? AND ep.is_approved = 1 AND ep.type <> ? AND ep.type <> ? 
			ORDER BY ep.is_main DESC
			LIMIT ?
		', array($this->getId(),ESCORT_PHOTO_TYPE_PRIVATE, ESCORT_PHOTO_TYPE_ARCHIVED, $limit));
		
		foreach ($photos as $i => $photo) {
			$photos[$i] = new Model_Escort_PhotoItem($photo);
		}
		
		return $photos;
	}
	
	public function getPhotosCount()
	{
		$countSql = "
			SELECT COUNT(ep.id) as count
			FROM escort_photos ep
			/*INNER JOIN escorts e ON e.id = ep.escort_id*/
			WHERE ep.escort_id = ? AND ep.is_approved = 1 /*{$where}*/
		";

		$count = $this->_adapter->query($countSql, $this->getId())->fetch()->count;

		return $count;
	}
	
	public function getMainPhoto()
	{
		return new Model_Escort_PhotoItem(array(
			'application_id' => $this->application_id,
			'escort_id' => $this->id,
			'hash' => $this->photo_hash,
			'ext' => $this->photo_ext,
			'photo_status' => $this->photo_status
		));
	}
	
	public function getNewPhotoCount($type)
	{
		$photo = $this->_adapter->query("
			SELECT COUNT(ep.id) AS count
			FROM escort_photos ep
			INNER JOIN escorts e ON e.id = ep.escort_id
			INNER JOIN users u ON u.id = e.user_id
			WHERE ep.escort_id = ? AND ep.status = '" . Model_Escort_Photos::NOT_VERIFIED . "' AND ep.type IN (" . $type . ")
		", array($this->getId()/*, $type*/))->fetch();
		
		return $photo->count;
	}
	
	public function getPhoto($index)
	{
		$photos = $this->_adapter->query('
			SELECT * FROM escort_photos 
			WHERE escort_id = ? AND ordering = ?
		', array($this->getId(), $index))->fetch();
		
		return new Model_Escort_PhotoItem($photo);
	}

	public function getUser()
	{
		$user_id = $this->_adapter->fetchOne('SELECT user_id FROM escorts WHERE id = ?', $this->getId());
		
		$item = new Model_UserItem();
		$item->setId($user_id);
		return $item;
	}
	
	public function getUserData()
	{
		$user_data = $this->_adapter->fetchRow('
			SELECT u.email 
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id 
			WHERE e.id = ?
		', $this->getId());
		
		return $user_data;
	}

    public function getFirstLoginLang()
    {
        $user_lang = $this->_adapter->fetchRow('
			SELECT u.lang
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id 
			WHERE e.id = ?
		', $this->getId());

        return $user_lang;
    }

    public function getSyncAproovment()
    {
        $sync_aprooved = $this->_adapter->fetchOne('
			SELECT e.sync_aprooved
			FROM escorts e
			WHERE e.id = ?
		', $this->getId());

        return $sync_aprooved;
    }

	public function getReviewsCount()
	{
		return $this->_adapter->fetchOne('SELECT reviews FROM escorts WHERE id = ?', $this->getId());
	}







	public function updateFuckometer()
	{
		if ( ! $this->id ) {
			throw new Exception('Id is required property');
		}

		self::getAdapter()->update('escorts', array(
			'fuckometer' => $f = $this->calcFuckometer()
		), array(self::getAdapter()->quoteInto('id = ?', $this->id)));
	}

	public function calcFuckometer()
	{
		if ( ! $this->id ) {
			throw new Exception('Id is required property');
		}

		// list of trusted members user_ids
		$trusted = Model_Members::getTrusted();

		$result = self::getAdapter()->fetchAll('
			SELECT r.user_id,
				COUNT(r.user_id) AS count,
				SUM(r.fuckometer) AS sum,
				m.reviews_count
			FROM reviews r
			LEFT JOIN members m ON m.user_id = r.user_id
			WHERE r.escort_id = ? AND r.status = 2 AND m.reviews_count >= ?
			GROUP BY r.user_id
		', array($this->id, TOTAL_FUCKOMETER_USER_MIN_REVIEWS));

		// calculate fuckometer
		$count = $count_trusted = $sum = 0;
		foreach ( $result as $data ) {
			$trusted_k = in_array($data->user_id, $trusted) ? 2 : 1;

			$count += ($data->count * $trusted_k);
			$count_trusted += in_array($data->user_id, $trusted) ? $data->count : 0;
			$sum += ($data->sum * $trusted_k);
		}

		$fuckometer = 0;
		if ( $count != 0 ) {
			$fuckometer = $sum / $count;
		}

		// reviews count bonus
		switch ( true ) {
			case (5 < $count && 10 >= $count): $fuckometer += 0.5; break;
			case (10 < $count && 20 >= $count): $fuckometer += 1; break;
			case (20 < $count): $fuckometer += 2; break;
		}

		return $fuckometer;
	}

	public function resetStatusBits()
	{
		$m = new Model_EscortsV2();
		$m->resetStatusBits($this->getId());
	}
	
	public function getPhotoHistory($page = 1, $per_page = 100)
	{
	    $page = $page == 0 ? 1 : $page;
	    $per_page = $per_page == 0 ? 100 : $per_page;

	    $sql = 'SELECT eph.escort_id ,eph.hash, eph.ext ,UNIX_TIMESTAMP(eph.date) AS date, eph.from_back 
			FROM escort_photo_history eph
			WHERE eph.escort_id = ? 
			ORDER BY eph.date DESC
			LIMIT '. ($page - 1) * $per_page . ','. $per_page;

		$photos = $this->_adapter->query($sql, $this->getId())->fetchAll();
		
		foreach ($photos as $i => $photo) {
			$photos[$i] = new Model_Escort_PhotoItem($photo);
		}
		
		return $photos;
	}

	public function getPhotoHistoryCount()
    {
        $photos = $this->_adapter->query("
			SELECT COUNT(id) AS count
			FROM escort_photo_history
			WHERE escort_id = ?
		", array($this->getId()))->fetch();

        return $photos->count;
    }
	
	public function getPhotoGalleries()
	{
		$galleries = $this->_adapter->fetchAssoc("
			SELECT eg.id, eg.title FROM escort_galleries eg
			WHERE eg.escort_id = ? ORDER BY eg.id 
		", $this->getId());
		
		return $galleries;
	}		

	public function getCurrentTourCountry($id)
	{
		return $this->_adapter->fetchOne('SELECT country_id FROM tours WHERE escort_id = ? AND DATE(date_from) <= DATE(NOW()) AND DATE(date_to) >= DATE(NOW())', $id);
	}

    public function hasStatusBit($status)
    {
        if ( ! is_array($status) ) $status = array($status);
        $result = true;
        foreach ( $status as $bit ) {
            $result = ($this->status & $bit) && $result;
        }

        return $result;
    }


    public static function isFromUSA($escort_id, $country_id) {
        $escortItemObj = new static($escort_id);
        $m_profile = new Model_Escort_ProfileV2(array('id' => $escort_id));
        $latest_revision = $m_profile->getRevision($m_profile->getLatestRevision());
        $revision_country_id = $latest_revision->data['country_id'];
        if($escortItemObj->getCurrentTourCountry($escort_id) == 68 || $country_id == 68 || $revision_country_id == 68){
           return true;
        }

        return false;
    }

    public static function getEscortTypeByID($Id) {
        return self::getAdapter()->fetchOne("SELECT type from escorts where id = ?", array($Id));
    }

}