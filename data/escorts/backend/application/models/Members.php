<?php
class Model_Members extends Cubix_Model
{
	protected $_table = 'users';
	protected $_itemClass = 'Model_MemberItem';

	public function getById($id)
	{
		$fields = " ";
		if ( in_array(Cubix_Application::getId(), array(APP_ED, APP_EG_CO_UK, APP_EG, APP_6C)) ) {
			$fields = " m.is_system_member, ";
		}

		if ( in_array(Cubix_Application::getId(), array(APP_6A, APP_EF)) ) {
			$fields .= " m.rating, ";
		}

		if ( in_array(Cubix_Application::getId(), array(APP_EG_CO_UK, APP_ED, APP_EF)) ) {
			$fields .= " m.escorts_watched_type, ";
		}

		if ( in_array(Cubix_Application::getId(), array(APP_ED)) ) {
			$fields .= " m.show_languages, m.langs, ";
		}
		
		
		if ( in_array(Cubix_Application::getId(), array(APP_EF)) ) {
			$fields .= " m.cam_trusted, ";
		}
		
		$sql = '
			SELECT
				m.id,
				u.id AS user_id,
				u.username,
				m.about_me,
				c.id AS city_id,
				ap.id AS application_id, 
				m.email,
				u.status,
				m.country_id,
				u.sales_user_id,
				IF (m.date_expires, UNIX_TIMESTAMP(m.date_expires), NULL) AS date_expires,
				u.last_login_date,
				u.last_ip,
				m.reviews_count,
				m.agency_comments_count,
				u.date_registered,
				m.is_suspicious,
				m.suspicious_reason,
				m.is_trusted,
				m.trusted_reason,
				m.member_comment,
				u.pm_is_blocked,
				u.im_is_blocked,
				' . $fields . '
				m.newsletter
			FROM members m
			INNER JOIN users u ON u.id = m.user_id
			LEFT JOIN cities c ON c.id = m.city_id
			LEFT JOIN applications ap ON ap.id = u.application_id
			WHERE 1 AND u.id = ?
		';
		
		$member = parent::_fetchRow($sql, array($id));

		return $member;
	}
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$fields = " ";

		if ( in_array(Cubix_Application::getId(), array(APP_6A, APP_EF)) ) {
			$fields .= " m.rating, ";
		}


		$join = '';
		
		if ( in_array(Cubix_Application::getId(), array(APP_BL, APP_EF)) ) {
			$fields .= " COUNT(f.id) as follow_count, ";
			$join .= " LEFT JOIN follow f ON f.followed_id = m.user_id ";
		}

		if ( strlen($filter['u.last_ip']) ) {
			$join .= ' LEFT JOIN users_last_login ull ON m.user_id = ull.user_id ';
		}

		$sql = '
			SELECT
				u.id,
				u.username,
				m.email,
				u.application_id,
				u.last_ip,
				UNIX_TIMESTAMP(u.date_registered) AS creation_date, 
				c.' . Cubix_I18n::getTblField('title') . ' AS city_title, 
				ap.title AS application_title, 
				u.status,
				m.id as member_id,
				m.is_premium,
				m.is_suspicious,
				m.is_trusted,
				' . $fields . '
				m.is_recurring
			FROM members m
			INNER JOIN users u ON u.id = m.user_id
			LEFT JOIN cities c ON c.id = m.city_id
			INNER JOIN applications ap ON ap.id = u.application_id ' . $join . '
			WHERE 1
		';
		
		$countSql = '
			SELECT COUNT(DISTINCT(u.id))
			FROM members m
			INNER JOIN users u ON u.id = m.user_id
			LEFT JOIN cities c ON c.id = u.city_id
			INNER JOIN applications ap ON ap.id = u.application_id ' . $join . '
			WHERE 1
		';
		
		$where = ' AND u.user_type = \'' . Model_Users::USER_TYPE_MEMBER . '\' ';
		
		if ( strlen($filter['u.application_id']) ) {
			$where .= ' AND u.application_id = ' . $filter['u.application_id'];
		}
		if ( strlen($filter['u.user_id']) ) {
			$where .= ' AND u.id = ' . $filter['u.user_id'];
		}
					
		if ( strlen($filter['u.username']) ) {
			if (strlen($filter['partial']))
				$where .= self::quote(' AND u.username LIKE ?', '%' . $filter['u.username'] . '%');
			else
				$where .= self::quote(' AND u.username LIKE ?', $filter['u.username'] . '%');
		}
		
		if ( strlen($filter['u.email']) ) {
			$where .= self::quote(' AND u.email LIKE ?', '%' . $filter['u.email'] . '%');
		}

		if ( strlen($filter['is_system_member']) ) {
			if ( $filter['is_system_member'] == 'show_system_members' ) {
				$where .= ' AND m.is_system_member = 1 ';
			} elseif ( $filter['is_system_member'] == 'show_normal_members' ) {
				$where .= ' AND m.is_system_member = 0 ';
			}
		}
		
		if ( strlen($filter['u.last_ip']) ) {
			//$where .= self::quote(' AND u.last_ip LIKE ?', $filter['u.last_ip'] . '%');
			$where .= self::quote(' AND ull.ip LIKE ?', $filter['u.last_ip'] . '%');
		}

		if ( strlen($filter['u.status']) ) {
			$where .= self::quote(' AND u.status = ?', $filter['u.status']);
		}

		if ( strlen($filter['m.rating']) ) {
			$where .= self::quote(' AND m.rating = ?', $filter['m.rating']);
		}

		if ( strlen($filter['m.is_premium']) ) {
			$where .= self::quote(' AND m.is_premium = ?', $filter['m.is_premium']);
		}
		
		if ( strlen($filter['is_suspicious']) ) {
			$where .= self::quote(' AND m.is_suspicious = ?', $filter['is_suspicious']);
		}
		
		if ( strlen($filter['has_member_comment']) ) {
			$where .= ' AND (m.member_comment != "" AND m.member_comment IS NOT NULL) ';
		}
		
		if ( isset($filter['filter_by']) && $filter['filter_by'] ) {
			if ( $filter['filter_by'] == 'registration_date' ) {
				$date_field = 'u.date_registered';
			}
			elseif ( $filter['filter_by'] == 'last_login_date' ) {
				$date_field = 'u.last_login_date';
			}
			
			if ( $filter['date_from'] && $filter['date_to'] ) {
				if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) ) {
					$where .= self::quote( " AND DATE( $date_field ) = DATE(FROM_UNIXTIME(?))" , $filter['date_from']);
				}
			}
			if ( $filter['date_from'] ) {
				$where .= self::quote(" AND DATE($date_field) >= DATE(FROM_UNIXTIME(?)) ", $filter['date_from']);
			}
			if ( $filter['date_to'] ) {
				$where .= self::quote(" AND DATE($date_field) <= DATE(FROM_UNIXTIME(?)) ", $filter['date_to']);
			}
		}
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			GROUP BY u.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		//echo $sql;
		//echo $countSql;die;
		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return parent::_fetchAll($sql);
	}	
	
	public function save(Model_MemberItem $item)
	{
		$users_model = new Model_Users();
		
		$user_data = $item->getData(array('email', 'username', 'password', 'status'/*, 'sales_user_id'*/, 'application_id'));
		$user_data['activation_hash'] = md5(microtime());
		//$user_data['disabled'] = 1;
		$user_data['user_type'] = Model_Users::USER_TYPE_MEMBER;
		$user_data['pm_is_blocked'] = $item->pm_is_blocked ? 1 : 0;
		$user_data['im_is_blocked'] = $item->im_is_blocked ? 1 : 0;
		
		$curr_user_id = isset($item->user_id) ? $item->user_id : false;
		if ( $curr_user_id ) {
			$user_data['id'] = $curr_user_id;
		}
		
		if ( strlen($user_data['password']) )
		{
			$salt_hash = Cubix_Salt::generateSalt($user_data['password']);
			$pass = Cubix_Salt::hashPassword($user_data['password'], $salt_hash);
			$user_data['password'] = $pass;
			$user_data['salt_hash'] = $salt_hash;
		}
		else 
		{
			unset($user_data['password']);
		}
		

		$user_id = $users_model->save(new Model_UserItem($user_data));

		$can_set_premium = Model_Users::can('set-member-premium');

		$member_fields = array('about_me', 'country_id', 'city_id', 'email');

		if ( in_array(Cubix_Application::getId(), array(APP_6A, APP_EF)) ) {
			$member_fields[] = 'rating';
			$member_fields[] = 'rating_changed_by_admin';
		}

		if ( in_array(Cubix_Application::getId(), array(APP_EF, APP_EG_CO_UK, APP_ED)) ) {
			$member_fields[] = 'escorts_watched_type';
		}

		if ( $can_set_premium ) {
			$member_fields[] = 'date_expires';
		}

		if ( in_array(Cubix_Application::getId(), array(APP_ED, APP_EG_CO_UK, APP_EG, APP_6C)) ) {
			$member_fields[] = 'is_system_member';
		}

		$member_data = $item->getData($member_fields);


		if ( $can_set_premium ) {
			if ( isset($member_data['date_expires']) && $member_data['date_expires'] ) {
				$member_data['is_premium'] = 1;
			}
			else {
				$member_data['is_premium'] = 0;
			}
		}

		if ( $curr_user_id ) {
			$member_data['user_id'] = $curr_user_id;
		}
		else {
			$member_data['user_id'] = $user_id;
		}
		
		$curr_member_id = $item->getId();
		
		if ( in_array(Cubix_Application::getId(), array(APP_ED)) ) {
			$member_data['langs'] = $item['langs'];
			$member_data['show_languages'] = $item['show_languages'];
		}

		if( $curr_member_id )
		{
			// if ( in_array(Cubix_Application::getId(), array(APP_ED)) ) {
			// 	$langs = $this->_getParam('langs', array());
			// 	var_dump($langs);
			// 	exit();
			// 	if ( ! is_array($langs) ) $langs = array();

			// 	foreach ( $langs as $lang_id => $val ) {
			// 		if ( array_key_exists($lang_id, $defines['language_options']) ) {
			// 			$data['langs'][] = $lang_id;
			// 		}
			// 	}

			// }
			//$member_data['id'] = $curr_member_id;
			$member_data['is_suspicious'] = $item['is_suspicious'];
			$member_data['suspicious_reason'] = $item['suspicious_reason'];
			$member_data['is_trusted'] = $item['is_trusted'];
			$member_data['trusted_reason'] = $item['trusted_reason'];
			$member_data['member_comment'] = $item['member_comment'];
			$member_data['newsletter'] = $item['newsletter'];
			$bu_user = Zend_Auth::getInstance()->getIdentity();
			if ( Cubix_Application::getId() == APP_EF && $bu_user->type == 'superadmin' ){
				$member_data['cam_trusted'] = $item['cam_trusted'];
			}
			
			parent::getAdapter()->update('members', $member_data, parent::quote('id = ?', $curr_member_id));
			$id = $curr_member_id;
		}
		else
		{
			parent::getAdapter()->insert('members', $member_data);
			$id = parent::getAdapter()->lastInsertId();
		}	
		
		if ($member_data['is_premium'] == 1)
		{
			//self::setPremium($id, strtotime($member_data['date_expires']), 0);
		}
		//parent::save(new Model_MemberItem($member_data));
		//Model_Activity::getInstance()->log( ($item->getId() ? 'edit' : 'create') . '_member', array('agency id' => $agency_id, 'name' => $item->name));
	}
	
	public function existsByUsername($username, $id = null)
	{
		$params = array($username);
		$sql = "SELECT TRUE FROM users WHERE status <> -5 AND status <> -1 AND username = ?";
		if($id)
		{
			$params[] = $id;
			$sql .= " AND id <> ?";
		}
			
		return (bool) self::getAdapter()->fetchOne($sql, $params);
	}
	
	public function existsByEmail($email, $id = null)
	{
		$params = array($email);
		$sql = "SELECT TRUE FROM users WHERE status <> -5 AND status <> -1 AND email = ?";
		if($id)
		{
			$params[] = $id;
			$sql .= " AND id <> ?";
		}
			
		return (bool) self::getAdapter()->fetchOne($sql, $params);
	}

    public function memberExistsById($id)
    {
        $params = array($id);
        $sql = "SELECT TRUE FROM users WHERE user_type = 'member' AND status <> -5 AND status <> -1 AND id = ?";
        return (bool) self::getAdapter()->fetchOne($sql, $params);
    }

    public function escortExistsById($id)
    {
        $params = array($id);
        $sql = "SELECT TRUE FROM users WHERE user_type = 'escort' AND  status <> -5 AND status <> -1 AND id = ?";
        return (bool) self::getAdapter()->fetchOne($sql, $params);
    }


	public function remove($id)
	{
		$email = parent::getAdapter()->fetchOne('SELECT email FROM users WHERE id = ?', $id);
		
		parent::remove(self::quote('id = ?', $id));
		parent::getAdapter()->delete('members', parent::quote('user_id = ?', $id));

		//newsletter email log
		$emails = array(
			'old' => $email,
			'new' => null
		);
		Cubix_Api::getInstance()->call('addNewlstterEmailLog', array($id, 'member', 'delete', $emails));
		//
	}



	
	static public function getTrusted()
	{
		$db = Zend_Registry::get('db');

		$members = $db->fetchAll('
			SELECT DISTINCT r.user_id
			FROM reviews r
			GROUP BY r.user_id
			HAVING COUNT(*) >= ?
		', FUCKOMETER_TRUSED_USER_REVIEWS, Zend_Db::FETCH_NUM);

		foreach ( $members as $i => $member ) {
			$members[$i] = $member[0];
		}

		return $members;
	}

	/* for Newsletter Api */

	public function getAllForNewsletterApi()
	{
		$sql = "SELECT u.username AS name, u.email AS email FROM users u WHERE u.user_type = 'member'";
		return self::getAdapter()->fetchAll($sql);
	}

	public function saveSuspicious($data)
	{
		self::getAdapter()->update('members', array(
			'is_suspicious' => $data['is_suspicious'],
			'suspicious_reason' => $data['suspicious_reason']
		), self::getAdapter()->quoteInto('id = ?', $data['id']));
	}

	public function saveSuspiciousByUserId($data,$user_id)
	{
		self::getAdapter()->update('members', array(
			'is_suspicious' => $data['is_suspicious'],
			'suspicious_reason' => $data['suspicious_reason']
		), self::getAdapter()->quoteInto('user_id = ?', $user_id));
	}


	static public function getExpiredRecurringMembers()
	{
		$sql = '
			SELECT * FROM (
				SELECT
					m.id, u.username, cc.transaction_id, cc.amount, m.payment_ref AS ref, m.is_recurring
				FROM members m
				INNER JOIN users u ON u.id = m.user_id

				INNER JOIN members_premium_log mpl ON mpl.member_id = m.id AND mpl.status = 1
				INNER JOIN cgp_callback cc ON cc.id = mpl.callback_id

				WHERE m.date_expires <= NOW() AND m.is_recurring = 1 AND m.failed_recurring_count < 3
				ORDER BY m.id, cc.date ASC
			) AS x GROUP BY x.id
		';
		return self::db()->fetchAll($sql);
	}

	static public function getExpiredMembers()
	{
		$sql = '
			SELECT * FROM (
				SELECT
					m.id, u.username, cc.transaction_id, cc.amount, m.payment_ref AS ref, m.is_recurring
				FROM members m
				INNER JOIN users u ON u.id = m.user_id

				INNER JOIN members_premium_log mpl ON mpl.member_id = m.id AND mpl.status = 1
				INNER JOIN cgp_callback cc ON cc.id = mpl.callback_id

				WHERE m.date_expires <= NOW() AND m.is_premium = 1
				ORDER BY m.id, cc.date ASC
			) AS x GROUP BY x.id
		';
		return self::db()->fetchAll($sql);
	}

	
	static public function setPremium($member_id, $expires, $is_recurring)
	{
		$updated = self::db()->update('members', array(
			'is_premium' => 1,
			'date_expires' => new Zend_Db_Expr('FROM_UNIXTIME(' . $expires . ')'),
			'is_recurring' => (bool) $is_recurring,
			'failed_recurring_count' => 0
		), self::db()->quoteInto('id = ?', $member_id));
		
		if ( $updated ) {
			self::_logPremium(null, $member_id, self::STATUS_PREMIUM_UPDATED, 'Expiration set to ' . date('d M Y, H:i:s', $expires));

			// in escortforumit.xxx we have a forum
			if ( 1 == Cubix_Application::getId() ) {
				$forum = new Cubix_ForumApi();
				if ( false == $forum->SetPremium(1, self::getUserIdById($member_id)) ) {
					self::_logPremium(null, $member_id, self::STATUS_FORUM_PREMIUM, 'Could not set member as premium in FORUM!');
				}
			}
		}

		return $updated;
	}

	static public function setPremiumByRef($ref, $expires, $is_recurring)
	{
		return self::setPremium(self::getIdByRef($ref), $expires, $is_recurring);
	}

	static public function setPremiumExpired($member_id)
	{
		$updated = self::db()->update('members', array(
			'is_premium' => 0
		), self::db()->quoteInto('id = ?', $member_id));

		if ( $updated ) {
			self::_logPremium(null, $member_id, self::STATUS_PREMIUM_EXPIRED, 'Membership has expired!');

			// in escortforumit.xxx we have a forum
			if ( 1 == Cubix_Application::getId() ) {
				$forum = new Cubix_ForumApi();
				if ( false == $forum->SetPremium(0, self::getUserIdById($member_id)) ) {
					self::_logPremium(null, $member_id, self::STATUS_FORUM_PREMIUM, 'Could not set member expired in FORUM!');
				}
			}
		}

		return $updated;
	}

	static public function getIdByRef($ref)
	{
		if ( ! ($id = self::db()->fetchOne('SELECT id FROM members WHERE payment_ref = ?', $ref)) ) {
			self::_logPremium(null, null, self::STATUS_MEMBER_NOT_FOUND, 'Member with ref ' . $ref . ' not found');
		}

		return $id;
	}

	const STATUS_TRANSACTION_SUCCESS = 1;
	const STATUS_TRANSACTION_FAIL = 2;
	const STATUS_TRANSACTION_RECURRING_ERROR = 6;
	const STATUS_TRANSACTION_RECURRING_SUCCESS = 7;
	const STATUS_PREMIUM_UPDATED = 3;
	const STATUS_PREMIUM_EXPIRED = 4;
	const STATUS_MEMBER_NOT_FOUND = 5;
	const STATUS_FORUM_PREMIUM = 8;
	const STATUS_PREMIUM_EXPIRATION_FAILED = 9;

	static public function logFail($ref, $callback_id, $desc, $status = self::STATUS_TRANSACTION_FAIL)
	{
		self::_logPremium($callback_id, self::getIdByRef($ref), $status, $desc);
	}

	static public function logSuccess($ref, $callback_id, $desc, $status = self::STATUS_TRANSACTION_SUCCESS)
	{
		self::_logPremium($callback_id, self::getIdByRef($ref), $status, $desc);
	}

	static private function _logPremium($callback_id, $member_id, $status, $desc = null)
	{
		self::db()->insert('members_premium_log', array(
			'callback_id' => $callback_id,
			'member_id' => $member_id,
			'date' => new Zend_Db_Expr('NOW()'),
			'status' => $status,
			'desc' => $desc
		));
	}

	static public function getUserIdById($id)
	{
		return self::db()->fetchOne('SELECT user_id FROM members WHERE id = ?', $id);
	}

	public function toggleRec($member_id)
	{
		parent::getAdapter()->update('members', array(
			'is_recurring' => new Zend_Db_Expr('NOT is_recurring')
		), parent::quote('id = ?', $member_id));
	}
	
	static public function incrementFailedRecurring($ref)
	{	
		parent::getAdapter()->update('members', array(
			'failed_recurring_count' => new Zend_Db_Expr('failed_recurring_count + 1')
		), parent::quote('id = ?', self::getIdByRef($ref)));
	}
	
	public function getMembersAutocomplete($username)
	{
		return parent::_fetchAll('
			SELECT u.id, u.username
			FROM users u
			WHERE u.username LIKE ? AND user_type="member"
			ORDER BY u.username ASC
		', array($username . '%'));
	}
	public function getBlockedMembers($escort_id)
	{
		return parent::_fetchAll('
			SELECT u.id AS user_id, u.username
			FROM escort_blocked_users ebu
			INNER JOIN users u ON u.id = ebu.user_id
			WHERE ebu.escort_id = ?
		', array($escort_id));
	}
	
	public function disableReviews($user_id, $disable_type, $reason)
	{
		parent::getAdapter()->update('reviews', array(
			'status' => REVIEW_DISABLED,
			'disable_type' => $disable_type,
			'disabled_reason' => $reason
		), parent::getAdapter()->quoteInto('user_id = ?', $user_id));
	}
	
	public function disableComments($user_id, $disable_type, $reason)
	{
		parent::getAdapter()->update('comments', array(
			'status' => COMMENT_DISABLED,
			'disable_type' => $disable_type,
			'disabled_reason' => $reason
		), parent::getAdapter()->quoteInto('user_id = ?', $user_id));
	}

	public function markAsSystemMember($user_id)
	{
		parent::getAdapter()->update('members', array(
			'is_system_member' => 1
		), parent::getAdapter()->quoteInto('user_id = ?', $user_id));
	}

	public function getFollowerList($followedId){
		$db = Zend_Registry::get('db');

		$sql = 'SELECT SQL_CALC_FOUND_ROWS 
				e.showname, u.username, f.id, u.user_type, f.status, u.id as user_id, e.id as escort_id
				FROM follow as f
				LEFT JOIN users u ON u.id = f.follower_id
				LEFT JOIN escorts e ON e.user_id = u.id
				WHERE f.followed_id = ?';

		$data = $db->fetchAll($sql, $followedId);

		foreach ($data as $value) {
			if($value->status == 0){
				$value->status = 'Not approved';	
			}else{
				$value->status = 'approved';	
			}

			if(!empty($value->escort_id)){
				$value->user_id = $value->escort_id;
			}
		}

		$count = $db->fetchOne('SELECT FOUND_ROWS()');

		$data = array(
			'data' => $data,
			'count' => $count
		);

		return $data;
	}

	public function unFollow($followId){
		parent::getAdapter()->delete('follow', parent::quote('id = ?', $followId));	
	}

    public function getFirstLoginLang($id)
    {
        return self::getAdapter()->fetchOne('SELECT u.lang FROM users u INNER JOIN members m ON m.user_id = u.id WHERE m.id = ? ', $id);
    }
}
