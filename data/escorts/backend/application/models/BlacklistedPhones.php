<?php

class Model_BlacklistedPhones extends Cubix_Model
{

    public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT id, phone, UNIX_TIMESTAMP(date) AS date
			FROM blacklisted_phones
			WHERE 1
		';

		$countSql = '
			SELECT COUNT(DISTINCT(id))
			FROM blacklisted_phones
			WHERE 1
		';
		
		if ( strlen($filter['application_id']) ) {
			$sql .= self::quote('AND application_id = ?', $filter['application_id']);
			$countSql .= self::quote('AND application_id = ?', $filter['application_id']);
		}
		
		if ( strlen($filter['phone']) ) {
			$sql .= self::quote('AND phone LIKE ?', '%' . $filter['phone'] . '%');
			$countSql .= self::quote('AND phone LIKE ?', '%' . $filter['phone'] . '%');
		}


		$sql .= '
			GROUP BY id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$countSql .= '';

		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return $this->getAdapter()->query($sql)->fetchAll();
	}
	
	public function save($data)
	{
        if (isset($data['id']) && $data['id']) {
			self::getAdapter()->update('blacklisted_phones', $data, self::quote('id = ?', $data['id']));
		}
		else {
			self::getAdapter()->insert('blacklisted_phones', $data);
		}
    }
	
	public function get($id)
	{
		return self::_fetchRow('
			SELECT * FROM blacklisted_phones WHERE id = ?
		', array($id));
	}

    public function delete($id)
	{
        self::getAdapter()->delete('blacklisted_phones', self::getAdapter()->quoteInto('id = ?', $id));
    }
}
