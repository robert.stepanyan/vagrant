<?php

class Model_BlockedUsername extends Cubix_Model
{	
	
    public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT id, word
			FROM blocked_username
			WHERE 1
		';

		$countSql = '
			SELECT COUNT(DISTINCT(id))
			FROM blocked_username
			WHERE 1
		';
		
		if ( strlen($filter['word']) > 0 ) {
			$sql .= self::quote('AND word LIKE ?', '%' . $filter['word'] . '%');
			$countSql .= self::quote('AND word LIKE ?', '%' . $filter['word'] . '%');
		}

		$sql .= '
			GROUP BY id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$count = intval($this->getAdapter()->fetchOne($countSql));
		return parent::_fetchAll($sql);
	}

	public function get($id)
	{
		return self::_fetchRow('
			SELECT * FROM blocked_username WHERE id = ?
		', array($id));
	}

    

    public function save($data,$id = null){
        if ( ! is_null($id) ) {
			self::getAdapter()->update('blocked_username', $data, self::quote('id = ?', $id));
		}
		else {
			self::getAdapter()->insert('blocked_username', $data);
		}
    }

    public function delete($id = array()){
        self::getAdapter()->delete('blocked_username', self::getAdapter()->quoteInto('id = ?', $id));
    }


	public function getAllWords()
	{
		 $sql = '
			SELECT id, word
			FROM blocked_username
			WHERE 1';

        $wordList = parent::_fetchAll($sql);
		
		return $wordList;
	}
	

	
	public function exists($name, $id = null)
	{
		$sql_edit = isset($id) ?  ' AND id <>'.$id : '';
		
		$sql = 'SELECT TRUE FROM blocked_username WHERE word = ? '.$sql_edit;
		return self::getAdapter()->fetchOne($sql, array($name));
	}

}
