<?php
class Model_ClassifiedAdsUpdates extends Cubix_Model
{	
	const EVENT_TYPE_UPDATED  = 1;
	const EVENT_TYPE_REMOVED  = 2;

	public function addEvent($ad_id, $event_type)
	{
		$sql = '
			SELECT
				TRUE
			FROM classified_ads_updates
			WHERE ad_id = ? AND is_sync_done = 0
		';

		$exists = parent::getAdapter()->fetchOne($sql,array($ad_id));
		if(!$exists){
			parent::getAdapter()->insert('classified_ads_updates', array('ad_id' => $ad_id, 'event_type' => $event_type));
		} else{
			self::getAdapter()->query('UPDATE classified_ads_updates SET event_type = ? WHERE ad_id = ? AND is_sync_done = 0', array( $event_type, $ad_id ));
		}
	}
}
