<?php

class Model_VerificationTypes extends Cubix_Model
{
	public function getVerificationTypes()
    {
        $db = Zend_Registry::get('db');
        return $db->fetchAll('SELECT id,verification_methodes,`status` FROM verification_types ');
    }

    public function toggleVerificationTypes($id){
        $field = 'status';

        parent::getAdapter()->update('verification_types', array(
            $field => new Zend_Db_Expr('NOT ' . $field)
        ), parent::quote('id = ?', $id));
    }
}
