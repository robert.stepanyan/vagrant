<?php

class Model_ResetedPassword extends Cubix_Model
{	
	
    public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT e.showname, e.id as escort_id, rp.id, rp.user_id, rp.activation as activation, UNIX_TIMESTAMP(rp.created_date) as created_date, UNIX_TIMESTAMP(rp.activation_date) as activation_date, rp.sales_id, bu.first_name as sales_person
			FROM reseted_password as rp
			LEFT JOIN backend_users bu ON bu.id = rp.sales_id  
			LEFT JOIN escorts e ON e.user_id = rp.user_id  
			WHERE 1
		';

		$countSql = '
			SELECT COUNT(DISTINCT(rp.id))
			FROM reseted_password  as rp
			LEFT JOIN backend_users bu ON bu.id = rp.sales_id  
			LEFT JOIN escorts e ON e.user_id = rp.user_id  
			WHERE 1
		';
		
		// if ( strlen($filter['word']) > 0 ) {
		// 	$sql .= self::quote('AND word LIKE ?', '%' . $filter['word'] . '%');
		// 	$countSql .= self::quote('AND word LIKE ?', '%' . $filter['word'] . '%');
		// }

		if ( strlen($filter['e.showname']) ) {
			$sql .= self::quote('AND e.showname LIKE ?','%'. $filter['e.showname'] . '%');
			$countSql .= self::quote('AND e.showname LIKE ?','%'. $filter['e.showname'] . '%');
		}


		if ( strlen($filter['e.id']) ) {
			$sql.= self::quote('AND e.id = ?',$filter['e.id']);
			$countSql.= self::quote('AND e.id = ?',$filter['e.id']);
		}

		if ( !empty($filter['sales_user_id']) ) {
			$sql .= self::quote(' AND rp.sales_id  = ?', $filter['sales_user_id']);
			$countSql .= self::quote(' AND rp.sales_id  = ?', $filter['sales_user_id']);
		}

		$sql .= '
			GROUP BY id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$count = intval($this->getAdapter()->fetchOne($countSql));
		return parent::_fetchAll($sql);
	}    

    public function save($data,$id = null){
		self::getAdapter()->insert('reseted_password', $data);
    }

    public function delete($id = array()){
        self::getAdapter()->delete('reseted_password', self::getAdapter()->quoteInto('id = ?', $id));
    }
}
