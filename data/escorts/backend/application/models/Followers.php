<?php
class Model_Followers extends Cubix_Model
{
		
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$join = '';
		$where = '';
		if ($filter['user_id']){
			$where .= ' AND u.id = ' . $filter['user_id'] ;
		}
		
		if ($filter['escort_id']){
			$where .= ' AND fu.type_id = ' . $filter['escort_id']. ' AND fu.type = "escort"';
		}
		
		if ($filter['agency_id']){
			$where .= ' AND fu.type_id = ' . $filter['agency_id']. ' AND fu.type = "agency"';
		}
				
		if (isset($filter['username']) && strlen($filter['username'])){
			$where .= self::quote(' AND u.username LIKE ?', '%' . $filter['username'] . '%');
		}
		
		if ($filter['type']){
			switch($filter['type']){
				case 1:
					$where .= ' AND fu.email IS NULL ' ;
					break;
				case 2:
					$where .= ' AND fu.user_id IS NULL ' ;
					break;
			}
		}
		
		if (isset($filter['email']) && strlen($filter['email'])){
			$where .=  ' AND (' . self::quote(' u.email LIKE ?', '%' . $filter['email'] . '%') . ' OR ' .self::quote('fu.email LIKE ?', '%' . $filter['email'] . '%'). ') ';
		}
		
		$sql = "SELECT SQL_CALC_FOUND_ROWS u.id as member_id, fu.id, fu.user_id, fu.email, fu.type, fu.type_id, UNIX_TIMESTAMP(fu.date) as follow_date,
				fu.status,  fu.short_comment AS member_comment, u.username
				FROM follow_users fu
				LEFT JOIN users u ON u.id = fu.user_id
				".$join."
				WHERE 1 ";
		
				
		$sql .= $where;
		
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$data = parent::_fetchAll($sql);
		$countSql = 'SELECT FOUND_ROWS()';
		$count =  intval($this->getAdapter()->fetchOne($countSql)); 
		
		return $data;
	}	
	
	public function getEvents($follow_user_id)
	{
		return $this->db()->fetchAssoc('
			SELECT event, extra FROM follow_users_events
			WHERE follow_user_id = ? 
		', $follow_user_id);
	}
	
	public function getFollowData($follow_user_id)
	{
		return $this->db()->fetchRow('
			SELECT * FROM follow_users
			WHERE id = ? 
		', $follow_user_id);
	}		
	
	public function editFollowData($follow_user_id, $data, $events, $cities)
	{
		try{
			$this->db()->update('follow_users', $data, $this->db()->quoteInto('id = ?', $follow_user_id));
			$this->db()->delete('follow_users_events', $this->db()->quoteInto('follow_user_id = ?', $follow_user_id));
			self::addEvents($follow_user_id, $events, $cities);
			if(Cubix_Application::getId() == APP_ED){
				$follow_user_data = $this->db()->fetchRow('
					SELECT * FROM follow_users
					WHERE id = ? 
				', $follow_user_id);

				if($follow_user_data->user_id){
					$this->db()->update('follow_users', array('lang' => $data['lang']), $this->db()->quoteInto('user_id = ?', $follow_user_data->user_id));
				}else{
					$this->db()->update('follow_users', array('lang' => $data['lang']), $this->db()->quoteInto('email = ?', $follow_user_data->email));

				}
			}
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
	
	private function addEvents($id,$events,$extra)
	{
		foreach($events as $event){
			$data = array(
				'follow_user_id' => $id, 
				'event' => $event
			);
			if($event == FOLLOW_ESCORT_NEW_CITY || $event == FOLLOW_AGENCY_NEW_CITY){
				$data['extra'] = $extra;
			}
			self::db()->insert('follow_users_events', $data);
		}
	}
	
	public function encode($array)
	{
		if(count($array) > 0){
			return "|". implode('||', $array). "|";
		}
		else{
			return '';
		}
	}
	
	public function decode($string)
    {
        $string1 = trim($string, '|');
        return explode('||', $string1);
    }

    public function updateFollow($follow_id, $data){
        return $this->db()->update('follow_users', $data, $this->db()->quoteInto('id = ?', $follow_id));
    }
}
