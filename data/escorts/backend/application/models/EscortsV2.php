<?php
class Model_EscortsV2 extends Cubix_Model
{
	const STATUS_NOT_VERIFIED = 1;
	const STATUS_VERIFIED = 2;
	const STATUS_VERIFY_RESET = 3;
	
	const NOT_VERIFIED = 1;
	const VERIFIED = 2;
	const VERIFY_RESET = 3;

	const ESCORT_STATUS_NO_PROFILE					= 1;		// 00000000000001
	const ESCORT_STATUS_NO_ENOUGH_PHOTOS			= 2;		// 00000000000010
	const ESCORT_STATUS_NOT_APPROVED				= 4;		// 00000000000100
	const ESCORT_STATUS_OWNER_DISABLED				= 8;		// 00000000001000
	const ESCORT_STATUS_ADMIN_DISABLED				= 16;		// 00000000010000
	const ESCORT_STATUS_ACTIVE						= 32;		// 00000000100000
	const ESCORT_STATUS_IS_NEW						= 64;		// 00000001000000
	const ESCORT_STATUS_PROFILE_CHANGED				= 128;		// 00000010000000
	const ESCORT_STATUS_DELETED						= 256;		// 0000000100000000
	const ESCORT_STATUS_FAKE_PICTURE_DISABLED		= 512;		// 0000001000000000
	const ESCORT_STATUS_TEMPRARY_DELETED            = 1024;		// 0000010000000000
	const ESCORT_STATUS_SUSPICIOUS_DELETED			= 2048;		// 0000100000000000
    const ESCORT_STATUS_NEED_AGE_VERIFICATION       = 4096;		// 0001000000000000
    const ESCORT_STATUS_OFFLINE                     = 8192;		// 0010000000000000
    const ESCORT_STATUS_INACTIVE                    = 16384;	// 0100000000000000
    const ESCORT_STATUS_PARTIALLY_COMPLETE          = 32768;	// 1000000000000000
	const ESCORT_STATUS_IP_BLOCKED          		= 65536;	// 10000000000000000
	const ESCORT_STATUS_AWAITING_PAYMENT			= 131072;	// 100000000000000000

	const HH_STATUS_ACTIVE  = 1;
	const HH_STATUS_PENDING = 2;
	const HH_STATUS_EXPIRED = 3;
	const HH_STATUS_ADMIN_DISABLED = 4;

	const SMS_ACTIVATION_TIME_MIN = 1440; // one day
	
	protected $_table = 'escorts';
	protected $_itemClass = 'Model_EscortV2Item';

	public function getTours($escort_id)
	{
		$sql = '
			SELECT t.id, t.country_id, t.city_id, t.phone, UNIX_TIMESTAMP(t.date_from) AS date_from, UNIX_TIMESTAMP(t.date_to) AS date_to, minutes_from, minutes_to, c.title_en AS country_title, c.slug AS country_slug, cit.title_en AS city_title
			FROM tours t
			LEFT JOIN countries c ON c.id = t.country_id
			LEFT JOIN cities cit ON cit.id = t.city_id
			WHERE escort_id = ? AND ((DATE(date_from) <= CURDATE() AND DATE(date_to) >= CURDATE()) OR DATE(date_from) > CURDATE())
			ORDER BY t.date_from,t.minutes_from ASC
		';
		
		return self::getAdapter()->fetchAll($sql, $escort_id);
	}

	public function getToursHistory($escort_id)
	{
		$sql = '
			SELECT t.id, t.country_id, t.city_id, t.phone, UNIX_TIMESTAMP(DATE(t.date_from)) AS date_from, UNIX_TIMESTAMP(DATE(t.date_to)) AS date_to, c.title_en AS country_title, cit.title_en AS city_title
			FROM tours t
			LEFT JOIN countries c ON c.id = t.country_id
			LEFT JOIN cities cit ON cit.id = t.city_id
			WHERE escort_id = ? AND DATE(t.date_to) < CURDATE()
			ORDER BY t.date_from DESC
		';

		return self::getAdapter()->fetchAll($sql, $escort_id);
	}

	public function getVideo($id)
	{
		
		$sql_video = "SELECT * FROM video WHERE escort_id=?";
		$video = self::getAdapter()->fetchAll($sql_video,$id);		
		$data = array();
		if(!empty($video))
		{
			$v_id = '';
			foreach ($video as $v)
			{
				$v_id.="'".$v->id."',";
			}
			$v_id = substr($v_id,0,strlen($v_id)-1);
			
			$sql_image = "SELECT `hash`, ext, width, height, 2 AS application_id,video_id,image_id  FROM images JOIN video_image ON image_id=images.id AND video_id IN($v_id) ORDER BY video_image.image_id ASC";
			$data = self::getAdapter()->fetchAll($sql_image);
			
		}
		return array($data,$video);
	}
	
	public function getTour($tour_id)
	{
		$sql = '
			SELECT t.id, t.country_id, t.city_id, t.phone, UNIX_TIMESTAMP(DATE(t.date_from)) AS date_from, UNIX_TIMESTAMP(DATE(t.date_to)) AS date_to, c.title_en AS country_title, cit.title_en AS city_title
			FROM tours t
			LEFT JOIN countries c ON c.id = t.country_id
			LEFT JOIN cities cit ON cit.id = t.city_id
			WHERE t.id = ?
		';

		return self::getAdapter()->fetchRow($sql, $tour_id);
	}

	public function getForAutocompleter($page, $per_page, $filter, $sort_field, $sort_dir, &$count = 0)
	{
		$m_list = new Model_Escort_ListV2();
		$list = $m_list->getForAutocompleter($page, $per_page, $filter, $sort_field, $sort_dir);
		
		$count = $list['count'];
		return $list['result'];
	}

	public function getForAgencies($page, $per_page, $filter, $sort_field, $sort_dir, &$count = 0)
	{
		$m_list = new Model_Escort_ListV2();
		$list = $m_list->getForAgencies($page, $per_page, $filter, $sort_field, $sort_dir);

		$count = $list['count'];
		return $list['result'];
	}

	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count = 0)
	{
		$m_list = new Model_Escort_ListV2();
		$list = $m_list->getAll($page, $per_page, $filter, $sort_field, $sort_dir);

		$count = $list['count'];
		return $list['result'];
	}

	public function getCallCenter7days($page, $per_page, $filter, $sort_field, $sort_dir, &$count = 0, $having = null)
	{
		$m_list = new Model_Escort_ListV2();
		$list = $m_list->getCallCenter7days($page, $per_page, $filter, $sort_field, $sort_dir, $having);

		$count = $list['count'];
		return $list['result'];
	}
	
	public function getNeedAttentionEscorts($page, $per_page, $filter, $sort_field, $sort_dir, &$count = 0)
    {
        $m_list = new Model_Escort_ListV2();
        $list = $m_list->getNeedAttentionEscorts($page, $per_page, $filter, $sort_field, $sort_dir);

        $count = $list['count'];
        return $list['result'];
    }

	public function removeVacation($escort_id)
	{
		$sql = 'UPDATE escort_profiles_v2 SET vac_date_from = NULL, vac_date_to = NULL WHERE escort_id = ?';
		$this->getAdapter()->query($sql, $escort_id);
	}
	
	public function getVacations($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{		
		$sql = '
			SELECT e.id, e.showname, UNIX_TIMESTAMP(ep.vac_date_from) AS vac_date_from, UNIX_TIMESTAMP(ep.vac_date_to) AS vac_date_to FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
			WHERE ep.vac_date_from IS NOT NULL AND ((DATE(ep.vac_date_from) <= CURDATE() AND DATE(ep.vac_date_to) > CURDATE()) OR DATE(ep.vac_date_from) > CURDATE())
		';
		
		$countSql = '
			SELECT COUNT(DISTINCT(e.id)) FROM escorts e 
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
			WHERE ep.vac_date_from IS NOT NULL AND ((DATE(ep.vac_date_from) <= CURDATE() AND DATE(ep.vac_date_to) > CURDATE()) OR DATE(ep.vac_date_from) > CURDATE())
		';
		
		$where = '';
		
		if ( $filter['e.showname'] ) {
			$where .= self::quote('AND e.showname LIKE ?', $filter['e.showname'] . '%');
		}
		
		if ( $filter['u.application_id'] ) {
			$where .= self::quote('AND u.application_id = ?', $filter['u.application_id']);
		}
		
		$sql .= $where;
		$countSql .= $where;
		
		/*$sql .= '
			GROUP BY e.id
		';*/
		
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		//echo $sql;
		$count = $this->getAdapter()->fetchOne($countSql);
		
		return parent::_fetchAll($sql);
	}

	/**
     * @return  null|array|Model_EscortV2Item
	 */
	public function get($id)
	{
		$escort = parent::_fetchRow('
			SELECT e.id, e.type, e.user_id, e.agency_id, e.showname, e.gender, e.country_id, e.city_id, e.is_suspicious,
				u.email, u.username, e.status, u.sales_user_id, u.application_id, u.registered_ip, u.status as user_status,
				
				ep.height, ep.weight, ep.bust, ep.waist, ep.hip, ep.shoe_size, 
				ep.cup_size, ep.dress_size, /*ep.is_smoker, ep.hair_color, ep.hair_length, ep.eye_color,
				IF (ep.birth_date, UNIX_TIMESTAMP(ep.birth_date), NULL) AS birth_date, 
				IF (ep.vac_date_from, UNIX_TIMESTAMP(ep.vac_date_from), NULL) AS vac_date_from, 
				IF (ep.vac_date_to, UNIX_TIMESTAMP(ep.vac_date_to), NULL) AS vac_date_to, 
				ep.characteristics, ' . Cubix_I18n::getTblFields('ep.about') . ', ep.contact_phone, 
				ep.phone_instructions, ep.contact_email, ep.contact_web, ep.contact_zip, ep.measure_units, 
				ep.availability, ep.sex_availability, ep.svc_kissing, ep.svc_blowjob, ep.svc_cumshot, ep.svc_69, 
				ep.svc_anal, ' . Cubix_I18n::getTblFields('ep.svc_additional') . ', ep.ethnicity, ep.nationality_id, 
				ep.admin_verified,*/

                ep.contact_phone_parsed as contact_phone_parsed,
                ep.email as contact_email,
				
				eph.hash AS photo_hash,
				eph.ext AS photo_ext,
				eph.status AS photo_status,
				u.id AS user_id,
				u.balance, e.pseudo_escort, a.name AS agency_name, u.user_type, u.date_registered
			FROM escorts e
			LEFT JOIN users u ON u.id = e.user_id
			LEFT JOIN escort_profiles_v2 ep ON e.id = ep.escort_id
			LEFT JOIN agencies a ON a.id = e.agency_id
			LEFT JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1
			WHERE e.id = ?
		', $id);
		
		//$escort->sex_availability = explode(',', $escort->sex_availability);
		//$escort->work_times = $escort->getWorkTimes();
		
		return $escort;
	}

	public function getProfile($id, $revision = null)
	{
		$profile = new Model_Escort_ProfileV2(array('id' => $id, 'revision_id' => $revision));
		
		return $profile->getProfile();
	}

	public function getAdditional($id, $a6 = false)
	{
		$f = "";
		if ( (Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT) && $a6 == true ) {
			$f .= " , e.fake_city_id, e.fake_zip ";
		}
        if ( Cubix_Application::getId() == APP_ED) {
            $f .= " , e.data_entry_user";
        }

		if (Cubix_Application::getId() == APP_6A || Cubix_Application::getId() == APP_EF){
			//$f .= " , ".Cubix_I18n::getTblFields('ep.about_soft');
		}
		if (in_array(Cubix_Application::getId(), array(APP_6B, APP_6C, APP_EG, APP_EG_CO_UK, APP_ED))) {
			$f .= " ,e.type , e.date_registered, e.activation_date ";
		}
		if (Cubix_Application::getId() == APP_6A){
			$f .= ", e.about_me_informed ";
		}
		if (Cubix_Application::getId() == APP_BL){
			$f .= ", ep.phone_sms_verified, ep.snapchat, ep.snapchat_username ";
			$f .= ", e.is_vaccinated ";
		}
		if (Cubix_Application::getId() == APP_EF){
			$f .= ", e.need_age_verification, e.age_verify_date, e.age_verify_type, e.verified_status, e.age_verify_disabled, ep.escortbook_ad, e.no_renew_sms ";
		}
		if(in_array(Cubix_Application::getId(), array(APP_ED,APP_EG_CO_UK,APP_EG,APP_AE))){
			$f .= " , e.source_url";
		}
		if(in_array(Cubix_Application::getId(), array(APP_ED,APP_EG_CO_UK,APP_EG,APP_AE))){
			$f .= " , e.source_url_doesnt_have";
		} 
		if(in_array(Cubix_Application::getId(), array(APP_ED))){
			// e.type causes  error in function editRevisionAction when marging escorts array and additional data array
			$f .= " , ep.prefered_contact_methods , e.mark_not_new, ep.wechat ,ep.telegram, ep.ssignal, e.block_website";
		}
        if(in_array(Cubix_Application::getId(), array(APP_EG_CO_UK))){
            $f .= " , e.last_hand_verification_date";
        }
        if (in_array(Cubix_Application::getId(), [APP_A6, APP_EG_CO_UK, APP_BL, APP_6B, APP_AE, APP_EM, APP_EF, APP_EI])) {
            $f .= " , ep.telegram ";
            if (in_array(Cubix_Application::getId(), array(APP_EG_CO_UK,APP_BL,APP_6B,APP_AE,APP_EM)))
            {
                $f .= " , ep.ssignal ";
            }
        }
		
		$escort = parent::_fetchRow('
			SELECT e.id, e.agency_id, u.balance, e.pseudo_escort, a.name AS agency_name, u.user_type, u.username, e.status, u.system_disabled, u.im_is_blocked, e.is_pornstar, e.bad_photo_history, e.stays_verified,
				u.application_id, u.sales_user_id,u.registered_ip, e.vac_date_from, e.vac_date_to, ep.admin_verified,ep.about_has_bl, e.free_comment, e.due_date, e.is_suspicious,
				e.slogan, e.auto_approval,e.block_website, e.check_website, e.suspicious_comment,u.id as user_id, u.email AS reg_email, e.hh_date_from, e.hh_date_to, e.hh_motto, e.hh_save,
				e.disabled_reviews, e.disabled_comments,e.show_popunder, a.disabled_reviews AS ag_disabled_reviews, a.disabled_comments AS ag_disabled_comments, e.photo_auto_approval, e.status_comment, 
				e.susp_comment_show, e.display_after_package_expire, ep.viber, ep.whatsapp,
				ep.phone_additional_1, ep.phone_additional_2, ep.phone_additional_3 ' . $f .'
			FROM escorts e
			LEFT JOIN users u ON u.id = e.user_id
			LEFT JOIN escort_profiles_v2 ep ON e.id = ep.escort_id
			LEFT JOIN agencies a ON a.id = e.agency_id
			LEFT JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1
			WHERE e.id = ?
		', $id);

		return $escort;
	}

	public function getByUserId($user_id)
	{
		$escort = parent::_fetchRow('
			SELECT e.id, e.user_id, e.agency_id, e.showname, e.gender, e.country_id, e.city_id,
				u.email, u.username, e.status, u.sales_user_id, u.application_id,
				ep.height, ep.weight, ep.bust, ep.waist, ep.hip, ep.shoe_size, ep.breast_size, ep.dress_size, ep.is_smoker, ep.hair_color,
				ep.hair_length, ep.eye_color,
				IF (ep.birth_date, UNIX_TIMESTAMP(ep.birth_date), NULL) AS birth_date,
				IF (ep.vac_date_from, UNIX_TIMESTAMP(ep.vac_date_from), NULL) AS vac_date_from,
				IF (ep.vac_date_to, UNIX_TIMESTAMP(ep.vac_date_to), NULL) AS vac_date_to,
				ep.characteristics, ' . Cubix_I18n::getTblFields('ep.about') . ', ep.contact_phone, ep.phone_instructions, ep.contact_email, ep.contact_web, ep.contact_zip, ep.measure_units, ep.availability,
				ep.sex_availability, ep.svc_kissing, ep.svc_blowjob, ep.svc_cumshot, ep.svc_69, ep.svc_anal, ' . Cubix_I18n::getTblFields('ep.svc_additional') . ', ep.ethnicity, ep.nationality_id, ep.admin_verified,
				u.application_id,
				eph.hash AS photo_hash,
				eph.ext AS photo_ext,
				eph.status AS photo_status,
				u.balance, a.name AS agency_name
			FROM escorts e
			LEFT JOIN users u ON u.id = e.user_id
			LEFT JOIN escort_profiles_v2 ep ON e.id = ep.escort_id
			LEFT JOIN agencies a ON a.id = e.agency_id
			LEFT JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1
			WHERE u.id = ?
		', $user_id);

		//$escort->sex_availability = explode(',', $escort->sex_availability);
		//$escort->work_times = $escort->getWorkTimes();

		return $escort;
	}

    /**
     * Change Escort Sales Person
     */

    public function updateSalesPerson($sales_person, $user_id){
        $this->getAdapter()->query("UPDATE users SET sales_user_id = ? WHERE id = ?", array($sales_person, $user_id));
    }


	public function getIdByShowname($showname)
	{
		$id = parent::_fetchRow('
			SELECT e.id
			FROM escorts e
			WHERE e.showname = ?
		', $showname);
		
		return $id;
	}

	public function existsByUsername($username, $escort_id)
	{
		$where = '';
		if($escort_id){
			$where .= " AND e.id <> ". $escort_id;
		}
		return (bool) self::getAdapter()->fetchOne('SELECT TRUE FROM users u LEFT JOIN escorts e ON e.user_id = u.id  WHERE u.username = ? AND NOT ( IF(e.status IS NOT NULL ,e.status, 0) & ?) AND u.status <> -5 '. $where , array( $username, self::ESCORT_STATUS_DELETED));
	}

	public function getDisableWhenTourEnds($id)
	{
		return self::getAdapter()->fetchOne('SELECT disable_when_tour_ends FROM escorts WHERE id = ?', array($id));
	}

    public function getUserIdByEscortIdOrId($id, $type)
    {
        $sql = 'SELECT `user_id` FROM escorts WHERE';

        if ($type == 'agency') {
            $sql .= ' agency_id = ?';
        }
        if ($type == 'escort') {
            $sql .= ' id = ?';
        }
        return self::getAdapter()->fetchOne($sql, array($id));
    }

    public function existsByPhone($phone, $escort_id, $agency_id)
	{
	    if($phone == ''){
            return array();
        }
	   $params = array($phone, self::ESCORT_STATUS_DELETED,self::ESCORT_STATUS_DELETED);
	   $where = "";

	   if ($escort_id){
		   $where .= " AND ep.escort_id <> ". $escort_id;
	   }
	   
	   if ($agency_id){
		   $agency_ids = array($agency_id);
		   $model_agency = new Model_Agencies();
		   $linked_agencies = $model_agency->getLinkedAgencies($agency_id);
		   if($linked_agencies){
				foreach($linked_agencies as $agency){
					$agency_ids[] = $agency->agency2;
				}
		   }
		   $where .= " AND ( e.agency_id NOT IN ( ". implode( ',' , $agency_ids) .  ") OR e.agency_id IS NULL )";
		   
	   }else {
		   $where .= " OR a.contact_phone_parsed = $phone";
	   }

	   $sql = "SELECT agency_id
			FROM escort_profiles_v2 ep
            INNER JOIN escorts as e ON e.id = ep.escort_id
            INNER JOIN users u ON u.id = e.user_id
            LEFT JOIN agencies a ON a.user_id = u.id
			WHERE phone_exists = ? AND phone_exists != \"\" AND u.application_id = " . Cubix_Application::getId() . "
			AND ( NOT e.status & ? OR ( e.status & ? AND DATEDIFF( now(), e.deleted_date) < 60 )) {$where}	
			GROUP BY e.agency_id";

		return self::getAdapter()->fetchAll($sql, $params);
	}
	
	public function getShownameByEscortId($escort_id)
	{
		return self::getAdapter()->fetchOne('SELECT showname FROM escorts WHERE id =  ? ', array($escort_id));
	}

    public function getEmailByEscortId($escort_id)
    {
        return self::getAdapter()->fetchOne('SELECT u.email FROM escorts e INNER JOIN users u ON e.user_id = u.id WHERE e.id =  ? ', array($escort_id));
    }

	public function existsByShowname($showname)
	{
		return (bool) self::getAdapter()->fetchOne('SELECT TRUE FROM escorts WHERE showname = ? AND NOT (e.status & ?) ', array( $username, self::ESCORT_STATUS_DELETED));
	}

    public function escortExistsById($id)
    {
        $params = array($id);
        $sql = "SELECT TRUE FROM escorts WHERE  id = ?";
        return (bool) self::getAdapter()->fetchOne($sql, $params);
    }

	public function existsByShownameId($showname, $escort_id)
	{
		return (bool) self::getAdapter()->fetchOne('SELECT TRUE FROM escorts WHERE showname = ? AND id <> ?  AND NOT (e.status & ?) ', array($showname, $escort_id, self::ESCORT_STATUS_DELETED));
	}
	
	public function existsByEmail($email,$user_id){
		$params = array($email);
		$where = '';
		if ($user_id){
		   $params[] = $user_id;
		   $where .= " AND id <> ? ";
	    }
		
		return self::getAdapter()->fetchOne('SELECT TRUE FROM users WHERE email = ? AND status <> -5 '.$where, $params);
	}
	
	public function setStatus($escort_id, $status)
	{
		if ( $status == 'VERIFIED' )
		{
			$this->getAdapter()->query("UPDATE escorts SET verified_status = ? WHERE id = ?", array(Model_Escorts::VERIFIED, $escort_id));
			$this->getAdapter()->query("UPDATE escort_photos SET status = ? WHERE escort_id = ?", array(Model_Escort_Photos::VERIFIED, $escort_id));
			Model_Activity::getInstance()->log('verify_escort', array('escort id' => $escort_id));
		}
		
		if ( $status == 'REJECTED' )
		{
			$this->getAdapter()->query("UPDATE escorts SET verified_status = ? WHERE id = ?", array(Model_Escorts::NOT_VERIFIED, $escort_id));
			$this->getAdapter()->query("UPDATE escort_photos SET status = ? WHERE escort_id = ? AND status = '" . Model_Escort_Photos::NOT_VERIFIED . "'", array(Model_Escort_Photos::NORMAL, $escort_id));
			Model_Activity::getInstance()->log('reject_verify_escort', array('escort id' => $escort_id));
		}
	}
	
	public function getList($value)
	{
		$application_id = Zend_Controller_Front::getInstance()->getRequest()->application_id;

		$sql = '
			SELECT e.id, u.username, e.showname, UNIX_TIMESTAMP(e.date_last_modified) AS date_last_modified, u.status, bu.username AS sales_person, a.title AS signup_from, u.application_id, UNIX_TIMESTAMP(u.date_registered) AS date_registered
			FROM users u
			INNER JOIN escorts e ON e.user_id = u.id
			LEFT JOIN cities c ON c.id = e.city_id
			LEFT JOIN applications a ON a.id = u.application_id
			LEFT JOIN backend_users bu ON bu.id = u.sales_user_id
			INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
			WHERE e.showname LIKE ? AND u.application_id = ?
		';
		
		$sql .= '
			GROUP BY e.id
		';
		
		$sql .= '
			ORDER BY e.showname ASC
			LIMIT 10
		';
		
		return parent::_fetchAll($sql, array($value . '%', $application_id));
	}

	public function getIdByPhone($phone_parsed)
	{
		$sql = '
			SELECT e.id FROM escorts e
			INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
			WHERE ep.phone_number = ?
		';
		
		return parent::_fetchRow($sql, $phone_parsed);
	}
	
	public function getIdByPhoneLike($phone_parsed)
	{
		$sql = '
			SELECT e.id FROM escorts e
			INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
			WHERE ep.contact_phone_parsed LIKE ?
            AND ep.contact_phone_parsed IS NOT NULL
		';
		
		return parent::_fetchAll($sql, "%".$phone_parsed."%");
	}

	public function getPhoneByEscortId($id)
	{
		$sql = '
			SELECT ep.contact_phone_parsed FROM escort_profiles_v2 ep
			WHERE ep.escort_id = ?
		';
		
		return $this->_db->fetchOne($sql, $id);
	}
	
	public function getAboutMeTexts($escort_id)
	{
		$sql = '
			SELECT ep.escort_id, '. Cubix_I18n::getTblFields('ep.about'). ' FROM escort_profiles_v2 ep
			WHERE ep.escort_id = ?
		';
		
		return parent::_fetchRow($sql, $escort_id);
	}

	public function resetStatusBits($escort_id)
	{
		
	}
	
	public function setStatusBit($escort_id, $status)
	{		
		if ( ! is_array($status) ) $status = array($status);
		
		$old_status = (int) $this->_db->fetchOne('SELECT status FROM escorts WHERE id = ?', $escort_id);
		
		foreach ( $status as $bit ) {
			$old_status = $old_status | $bit;
		}
		
		$this->_db->update('escorts', array('status' => $old_status), $this->_db->quoteInto('id = ?', $escort_id));
	}
	
	public function removeStatusBit($escort_id, $status)
	{		
		if ( ! is_array($status) ) $status = array($status);
		
		$old_status = (int) $this->_db->fetchOne('SELECT status FROM escorts WHERE id = ?', $escort_id);
		foreach ( $status as $bit ) {
			if ( ! ($old_status & $bit) ) continue;
			$old_status = $old_status ^ $bit;
		}
		
		$this->_db->update('escorts', array('status' => $old_status), $this->_db->quoteInto('id = ?', $escort_id));
	}
	
	public function hasStatusBit($escort_id, $status)
	{
		if ( ! is_array($status) ) $status = array($status);
		
		$old_status = $this->_db->fetchOne('SELECT status FROM escorts WHERE id = ?', $escort_id);
		
		$result = true;
		foreach ( $status as $bit ) {
			$result = ($old_status & $bit) && $result;
		}
		
		return $result;
	}

	
	public function getPhoneExpirationDate($id)
	{
		return parent::db()->fetchOne('SELECT UNIX_TIMESTAMP(expiration_date) AS expiration_date_phone FROM phone_packages WHERE order_package_id = ' . $id);
	}
	
	public function deleteDueDate($escort_id)
	{
		self::getAdapter()->update('escorts', array('due_date' => null), self::getAdapter()->quoteInto('id = ?', $escort_id));
	}

	public function delete($escort_id, $immed = false)
	{
		$escort = $this->get($escort_id);
        
		$m_transfers = new Model_Billing_Transfers();
		$m_packages = new Model_Billing_Packages();
		$m_orders = new Model_Billing_Orders();

		$transfers = $m_transfers->getAllByUsertId($escort->user_id);

		self::getAdapter()->beginTransaction();

		try {
			$m_packages->cancelDefaultPackages($escort->id, 'Escort #' . $escort->id . ' profile has been deleted');

			if ( $escort->user_type == Model_Users::USER_TYPE_ESCORT ) {
				/*foreach ( $transfers as $k => $transfer ) {
					$transfers[$k] = $m_transfers->getDetails($transfer->id);
				}

				foreach( $transfers as $transfer ) {
					$transfer->reject('Escort #' . $escort->id . ' has been deleted', true);
				}*/

				$escort->cancelPackages('Escort #' . $escort->id . ' has been deleted');
				
				$m_orders->closeByUserId($escort->user_id);
			}
			else if ( $escort->user_type == Model_Users::USER_TYPE_AGENCY ) {
				$escort->cancelPackages('Agency Escort #' . $escort->id . ' has been deleted');
			}

			// User newer version of status management
			$status = new Cubix_EscortStatus($escort->id);
			// Will disable user row also
			$status->setStatus(Cubix_EscortStatus::ESCORT_STATUS_DELETED);
			$status->save();
			
			// Deletion Date For phone numbers existence
			$bu_user = Zend_Auth::getInstance()->getIdentity();
			if( in_array($bu_user->type, array('superadmin', 'admin')) && $immed){
				self::getAdapter()->update('escorts', array('deleted_date' => null), self::getAdapter()->quoteInto('id = ?', $escort_id));
			}
			else{
				self::getAdapter()->update('escorts', array('deleted_date' => new Zend_Db_Expr('NOW()') ), self::getAdapter()->quoteInto('id = ?', $escort_id));
			}
//			$this->removeStatusBit($escort->id, self::ESCORT_STATUS_ACTIVE);
//			$this->setStatusBit($escort->id, self::ESCORT_STATUS_DELETED);
			if ( $escort->user_type == Model_Users::USER_TYPE_ESCORT ) {
				self::getAdapter()->update('users', array('status' => USER_STATUS_DELETED), self::quote('id = ?', $escort->user_id));
			}

			Model_Activity::getInstance()->log('delete_escort', array('escort id' => $escort->id));

			//newsletter email log
			$client = Cubix_Api::getInstance();
			$conf = Zend_Registry::get('api_config');

			Cubix_Api_XmlRpc_Client::setApiKey(Cubix_Application::getById(Zend_Controller_Front::getInstance()->getRequest()->application_id)->api_key);
			Cubix_Api_XmlRpc_Client::setServer($conf['server']);

			$emails = array(
				'old' => $escort->email,
				'new' => null
			);

			$client->call('addNewlstterEmailLog', array($escort->user_id, $escort->user_type, 'delete', $emails));
			//

			self::getAdapter()->commit();
		}
		catch ( Exception $e ) {
			self::getAdapter()->rollBack();

			throw $e;
		}
	}

	public function getReviewsCount($escort_id)
	{
		return self::getAdapter()->fetchOne('SELECT reviews FROM escorts WHERE id = ?', $escort_id);
	}

	static public function updateFuckometerRanks()
	{
		set_time_limit(0);
		$db = Zend_Registry::get('db');
		
        $result = $db->fetchAll('
			SELECT e.id, e.fuckometer_rank AS rank
			FROM escorts e
			LEFT JOIN order_packages op ON op.escort_id = e.id AND op.application_id = ' . Cubix_Application::getId() . '
			WHERE e.status & 32 AND op.expiration_date >= CURDATE()
			ORDER BY e.fuckometer DESC
		');

		foreach ( $result as $index => $escort ) {
			/*$escort = new Model_EscortV2Item($escort);
			$escort->updateFuckometer();
			continue;*/


			if ( ! strlen($escort->rank) ) $escort->rank = trim(str_repeat('0|', 7), '|');
            $rank = substr($escort->rank, strpos($escort->rank, '|') + 1) . '|' . ($index + 1);
//			echo $escort->id . ': ' . $rank . "\n";
			$db->update('escorts', array(
				'fuckometer_rank' => $rank
			), $db->quoteInto('id = ?', $escort->id));
		}
	}

	static public function updateTop20Positions()
	{
		set_time_limit(0);
		$db = Zend_Registry::get('db');

		$result = $db->fetchAll('
			SELECT
				e.id, e.top20, ROUND(e.votes_sum / e.votes_count, 2) as vote
			FROM escorts e
			INNER JOIN order_packages op ON op.escort_id = e.id AND op.application_id = ? AND op.expiration_date >= CURDATE()
			WHERE
				e.votes_count >= 1 AND e.status & 32 AND e.gender = 1 AND e.country_id = ?
			ORDER BY vote DESC
		', array(Cubix_Application::getId(), Cubix_Application::getById()->country_id));
        
		foreach ($result as $index => $row) {
			if ( ! $row->top20 ) $row->top20 = trim(str_repeat('0|', 7), '|');
            $top20 = substr($row->top20, strpos($row->top20, '|') + 1) . '|' . ($index + 1);
			$db->update('escorts', array('top20' => $top20), array($db->quoteInto('id = ?', $row->id)));
        }
	}

	public function getUserId($id)
	{
		$db = Zend_Registry::get('db');

		return $db->fetchOne('SELECT user_id FROM escorts WHERE id = ?', $id);
	}

	public function getActivePackage($escort_id, $is_default = true)
	{
		$db = Zend_Registry::get('db');

		$where = "";
		if ( $is_default ) {
			$where = " AND p.is_default = 0 ";
		}

		$sql = "
			SELECT op.period, op.date_activated, op.expiration_date, p.name
			FROM order_packages op
			INNER JOIN escorts e ON e.id = op.escort_id
			INNER JOIN packages p ON p.id = op.package_id
			WHERE e.id = ? {$where} AND op.status = ?
			ORDER BY op.date_activated DESC
		";

		$package = $db->query($sql, array($escort_id, Model_Billing_Packages::STATUS_ACTIVE))->fetch();

		return $package;
	}

	public function getBubbleText($escort_id)
	{
		$db = Zend_Registry::get('db');
		return $db->query('
			SELECT
			bt.id, bt.date, bt.text, bt.status
			FROM escort_bubbletexts bt
			INNER JOIN escorts e ON e.id = bt.escort_id
			WHERE bt.escort_id = ? /*AND bt.status = 1*/
			ORDER BY bt.date DESC
		', $escort_id)->fetch();
    }

	public function updateBubbleText($bubble_text_id, $text, $escort_id)
	{
		$db = Zend_Registry::get('db');

		$data = array('text' => $text);

		$db->update('escort_bubbletexts', array('status' => 0), $db->quoteInto('escort_id = ?', $escort_id));
		
		if ( ! strlen($text) ) {
			$data['status'] = 0;
			$data['text'] = null;
		}
		else {
			$data['status'] = 1;
		}

		if ( strlen($bubble_text_id) ) {
			$db->update('escort_bubbletexts', $data, $db->quoteInto('id = ?', $bubble_text_id));
		}
		else {
			$data['escort_id'] = $escort_id;
			$db->insert('escort_bubbletexts', $data);
		}
    }

	public function getSuspiciousData($escort_id)
	{
		$data = array();

		$susp_comment = parent::_fetchRow('
			SELECT suspicious_comment
			FROM escorts
			WHERE is_suspicious = 1 AND id = ?
		', $escort_id);

		if ( isset($susp_comment->suspicious_comment) && $susp_comment->suspicious_comment ) {
			$sales_data = parent::_fetchRow('
				SELECT UNIX_TIMESTAMP(sj.date) AS date, bu.username
				FROM suspicious_journal sj
				INNER JOIN backend_users bu ON bu.id = sj.sales_user_id
				WHERE sj.event_type = \'1\' AND sj.escort_id = ?
				ORDER BY sj.date DESC
				LIMIT 1
			', $escort_id);

			$data['comment'] = $susp_comment->suspicious_comment;
			$data['date'] = $sales_data->date;
			$data['username'] = $sales_data->username;
		}

		return $data;
	}
	
	public function getSuspiciousHistory($escort_id,$sort=1)
	{	
		$sql = '
			SELECT sh.id, sh.comment, sh.date, bu.username
			FROM suspicious_history sh
			LEFT JOIN  backend_users bu ON bu.id = sh.bo_user_id
			WHERE sh.escort_id = ?
			ORDER BY sh.date
		';
		if($sort==1)
		$sql.=',sh.id';
		
		$sql.=' DESC ';
		return self::getAdapter()->fetchAll($sql, $escort_id);
	}
	
	public function toggleVip($escort_id)
	{
		parent::getAdapter()->update($this->_table, array(
			'is_vip' => new Zend_Db_Expr('NOT is_vip')
		), parent::quote('id = ?', $escort_id));
	}
	
	public function toggleReview($escort_id)
	{
		parent::getAdapter()->update($this->_table, array(
			'disabled_reviews' => new Zend_Db_Expr('NOT disabled_reviews')
		), parent::quote('id = ?', $escort_id));
	}
	
	public function toggleComment($escort_id)
	{
		parent::getAdapter()->update($this->_table, array(
			'disabled_comments' => new Zend_Db_Expr('NOT disabled_comments')
		), parent::quote('id = ?', $escort_id));
	}
	
	public function togglePorn($escort_id, $bu_user)
	{
		parent::getAdapter()->update($this->_table, array(
			'porn_notification' => new Zend_Db_Expr('NOT porn_notification'),
			'porn_notification_date' => new Zend_Db_Expr('NOW()'),
			'porn_notification_person' => $bu_user
		), parent::quote('id = ?', $escort_id));
	}
	
	public function updateField($escort_id, $field, $value)
	{
		parent::getAdapter()->update($this->_table, array(
			$field => $value
		), parent::quote('id = ?', $escort_id));
	}

	public function getField($escort_id, $field)
	{
		return parent::getAdapter()->fetchOne('SELECT ' . $field . ' FROM ' . $this->_table . ' WHERE id = ?', $escort_id);
	}

	public function getRates($escort_id)
	{
		$rates = parent::_fetchAll('
			SELECT * FROM
			escort_rates
			WHERE escort_id = ?
		', $escort_id);

		return $rates;
	}

	public static function insertEscortComments($data)
	{
		$db = Zend_Registry::get('db');
		$db->insert('escort_comments', $data);
	}

	public static function insertEscortResponseComment($data)
	{
		$db = Zend_Registry::get('db');
		$db->insert('escort_responses', $data);
	}

	public static function getCommentsHistoryByEscortId($id,$is_agency, $page = 1, $per_page = 5, &$count = 0)
	{
		$db = Zend_Registry::get('db');

		if($is_agency){
			$id_type = 'agency_id';
		}
		else{
			$id_type = 'escort_id';
		}

		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				ec.id, ec.comment , ec.date,ec.admin_verified , bu.username, ec.update_agency
			FROM escort_comments as ec
			LEFT JOIN backend_users bu ON bu.id = ec.sales_user_id
			WHERE '.$id_type.' = ? ORDER BY ec.date DESC LIMIT ' . (($page - 1) * $per_page) . ', '. $per_page ;
		$results = $db->fetchAll($sql, $id);

		$count = $db->fetchOne('SELECT FOUND_ROWS()');
		return $results;
	}


	public static function getStatusHystoryByEscortId($id, $page = 1, $per_page = 5, &$count = 0, $tables = null)
	{
        $db = Zend_Registry::get('db');

        if (isset($tables) && !empty($tables))
        {
            $isset_tables = array();
            foreach ($tables as $key => $table)
            {
                $isset_tables[] = $db->fetchOne( "SELECT `table_name` FROM `information_schema`.`tables` WHERE  `table_name` LIKE '%".$table."%'" );
            }
            foreach ($isset_tables as $isset_tble)
            {
                if ($isset_tble)
                {
                    $sqls[] = '( SELECT data , sales_user_id, date
                    FROM ' . $isset_tble. ' 
                    WHERE escort_id = '. $id .' AND event_type= '. Cubix_SyncNotifier::EVENT_ESCORT_STATUS_UPDATED .' ORDER BY date DESC )';
                }
            }

            $main_sql = ' SELECT SQL_CALC_FOUND_ROWS j.*, bu.username FROM (' . implode( ' UNION ALL ', $sqls ) . ') as j INNER JOIN backend_users bu ON bu.id = j.sales_user_id ORDER BY j.date DESC, j.sales_user_id DESC  LIMIT ' . (($page - 1) * $per_page) . ', '. $per_page;
            $results = $db->fetchAll($main_sql , array($id , Cubix_SyncNotifier::EVENT_ESCORT_STATUS_UPDATED ) );
		}else{
        $sql = 'SELECT SQL_CALC_FOUND_ROWS	journal.`data`,username,journal.`date`
            FROM  backend_users JOIN journal 
            ON backend_users.id = journal.sales_user_id AND
            escort_id = ? AND event_type=? ORDER BY journal.date DESC, journal.id DESC  LIMIT ' . (($page - 1) * $per_page) . ', '. $per_page ;
            $results = $db->fetchAll( $sql, array($id,Cubix_SyncNotifier::EVENT_ESCORT_STATUS_UPDATED) );
        }
		$count = $db->fetchOne('SELECT FOUND_ROWS()');
		return $results;
	}

	public function removeEscortComment($id)
	{
		$this->getAdapter()->delete('escort_comments', $this->getAdapter()->quoteInto('id = ?', $id));
	}

	public static function changeHappyHourStatus($escort_id, $status)
	{
		if ( ! $escort_id || ! $status ) return false;
		self::getAdapter()->update('escorts', array('hh_status' => $status), self::getAdapter()->quoteInto('id = ?', $escort_id));
		
		return true;
	}

	public static function getWebsiteChecked($id){
		return self::getAdapter()->fetchOne('SELECT check_website FROM escorts WHERE id = ?', $id);
	}

	public static function getWebsiteblocked($id){
		return self::getAdapter()->fetchOne('SELECT block_website FROM escorts WHERE id = ?', $id);
	}
	
	public static function insertSuspiciousComment($data)
	{
		$db = Zend_Registry::get('db');
		$db->insert('suspicious_history', $data);
		return  $db->lastInsertId();
	}
	
	public static function deleteSuspiciousComment($id)
	{
		$db = Zend_Registry::get('db');
		$db->delete('suspicious_history', parent::quote('id = ?', $id));
	}

	public static function getWorkingLocations($escort_id, $with = array()){

	    $joins = array();
	    $fields = array('c.slug');
        if (isset($with['country']) && $with['country'] == TRUE) {
            $joins[] = 'LEFT JOIN countries ctr ON ctr.id = c.country_id';
            $fields[] = 'ctr.id as country_id, ctr.title_en as country_title';
        }

		$cities = self::getAdapter()->fetchAll('
			SELECT c.id, c.title_en AS title, 
            '.implode(',', $fields ).'
			FROM 
				escort_cities ec
			INNER JOIN cities c ON c.id = ec.city_id
			'.implode(' ', $joins ).'
			WHERE ec.escort_id = ?
		', $escort_id);
		
		$main_city = self::getAdapter()->fetchRow('
			SELECT c.id, c.title_en AS title
			FROM 
				escorts e
			INNER JOIN cities c ON c.id = e.city_id
			WHERE e.id = ?
		', $escort_id);
		
		foreach ($cities as $i => $city) {
			if ( $city->id == $main_city->id ) {
				$cities[$i]->is_main = true;
			} else {
				$cities[$i]->is_main = false;
			}
		}
		
		return $cities;
	}
	
	public function getUrgentMessage($id)
	{
		return parent::getAdapter()->fetchOne('SELECT urgent_message FROM escorts WHERE id = ?', $id);
	}
	
	public function setUrgentMessage($id, $message)
	{
		parent::getAdapter()->update('escorts', array('urgent_message' => $message), parent::getAdapter()->quoteInto('id = ?', $id));
	}

	public function setEscortVerifiedStatus($escort_id)
	{
		 $escort_status = parent::getAdapter()->fetchOne('SELECT verified_status FROM escorts WHERE id = ?', $escort_id);

		if (self::STATUS_VERIFIED == $escort_status || self::STATUS_VERIFY_RESET == $escort_status) {

			if (self::STATUS_VERIFY_RESET != $escort_status) {
				parent::getAdapter()->update('escorts', array('verified_status' => self::STATUS_VERIFY_RESET), parent::getAdapter()->quoteInto('id = ?', $escort_id));
			}
		}
		
		//parent::getAdapter()->update('escorts', array('verified_status' => $status), parent::getAdapter()->quoteInto('id = ?', $id));
	}

	public function checkAgeCertification($escort_id)
	{
		$sql = " SELECT e.showname, u.email FROM escorts e
				INNER JOIN users u ON u.id = e.user_id
				WHERE e.id = ? AND e.need_age_verification = 0 ";
		
		$escort_data = parent::db()->fetchRow($sql, $escort_id);
		
		if($escort_data){
			
			$update_data = array(
				'need_age_verification' => Model_Verifications_AgeVerification::REQUEST_SENT,
				'age_verify_type' =>  Model_Verifications_AgeVerification::REQUEST_TYPE_ONLINE,
				'age_verify_date' => new Zend_Db_Expr('NOW()'),
				'age_verify_disabled' => 1
			);
			
			parent::db()->update('escorts', $update_data , parent::db()->quoteInto('id = ?', $escort_id));
			$status = new Cubix_EscortStatus($escort_id);
			$status->setStatusBit(Cubix_EscortStatus::ESCORT_STATUS_NEED_AGE_VERIFICATION);
			$status->save();
			
			Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('action' => 'age cerification automatic '));
			return $escort_data;
			
		}
		else{
			return false;
		}
		
	}
	
	public function toggleCheckGallery($escort_id)
	{
		parent::getAdapter()->update($this->_table, array(
			'gallery_is_checked' => new Zend_Db_Expr('NOT gallery_is_checked'),
		), parent::quote('id = ?', $escort_id));
		Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_SHOW_GALLERY_TOGGLED, array('escort_id' => $escort_id));
	}
	
	public function toggleAboutMeInformed($escort_id)
	{
		parent::getAdapter()->update($this->_table, array(
			'about_me_informed' => new Zend_Db_Expr('NOT about_me_informed'),
		), parent::quote('id = ?', $escort_id));
		
	}
	
	public function setPopunder($escort_id)
	{
		$this->getAdapter()->update('escorts',array('show_popunder' => 1), $this->getAdapter()->quoteInto('id = ?', $escort_id));
		Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('action' => 'set popunder'));
	}
	
	public function setPopunderV2($escort_id, $photo_id)
	{
		$this->getAdapter()->update('escort_photos',array('is_popunder' => 1), $this->getAdapter()->quoteInto('id = ?', $photo_id));
		$this->getAdapter()->update('escorts',array('has_large_popunder' => 1), $this->getAdapter()->quoteInto('id = ?', $escort_id));
		Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED, array('id' => $photo_id, 'action' => 'set popunder'));
	}
	
	public function getSuspiciousStatus($id){
		return parent::getAdapter()->fetchRow('SELECT is_suspicious, suspicious_comment, susp_comment_show FROM escorts WHERE id = ?', $id);
	}
	
	public function changeDisplay($escort_id, $flag)
	{
		parent::getAdapter()->update($this->_table, array(
			'display_after_package_expire' => $flag,
		), parent::quote('id = ?', $escort_id));
	}
	
	public function setBlockCamStatus($escort_id, $status)
	{
		parent::getAdapter()->update($this->_table, array(
			'block_cam' => $status,
		), parent::quote('id = ?', $escort_id));
		
	}
	
	public function hadEverPaidPackage($escort_id)
	{
		$count = parent::getAdapter()->fetchOne('
			SELECT COUNT(*)
			FROM order_packages 
			WHERE escort_id = ? AND order_id IS NOT NULL
		', $escort_id);
		
		if ($count > 0)
			return true;
		else
			return false;
	}
	
	public function getLastComment($id)
	{
		return parent::getAdapter()->query('
			SELECT c.comment, UNIX_TIMESTAMP(c.date) AS date, b.username 
			FROM escort_comments c 
			LEFT JOIN backend_users b ON b.id = c.sales_user_id
			WHERE c.escort_id = ? ORDER BY c.date DESC LIMIT 1', $id)->fetch();
	}
	
	public static function isSuspicious($id)
	{
		return parent::getAdapter()->fetchone('
			SELECT is_suspicious 
			FROM escorts 			
			WHERE id = ?',$id) ;
	}
	
	public static function getPhoneParsedById($id)
	{
		return parent::getAdapter()->fetchone('
			SELECT contact_phone_parsed
			FROM escort_profiles_v2 			
			WHERE escort_id = ?',$id) ;
	
	}		
	
	public function addAdminComment($escort_id, $admin_verified, $free_comment, $due_date, $bo_user_id)
	{
		if (!$due_date)
			$due_date = NULL;
		else
			$due_date = date('Y-m-d', $due_date);
		
		$this->getAdapter()->insert('escort_comments', array(
			'escort_id' => $escort_id,
			'comment' => $free_comment,
			'remember_me' => $due_date,
			'sales_user_id' => $bo_user_id,
			'date' => new Zend_Db_Expr('NOW()'),
			'admin_verified' => $admin_verified
		));
	}
	
	public function generatePassword($user_id )
	{
		$random_pass = md5(microtime() * rand());
		$random_pass = substr($random_pass, 0, 10);
		$salt_hash = Cubix_Salt::generateSalt($random_pass);
		$pass = Cubix_Salt::hashPassword($random_pass, $salt_hash);
		
		try {
			self::getAdapter()->update('users', array('password' => $pass, 'salt_hash' => $salt_hash), self::getAdapter()->quoteInto('id = ?', $user_id));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		return $random_pass;
	}

	public function getHandVerificationDate($escort_id)
	{
		return self::getAdapter()->fetchRow('
			SELECT e.last_hand_verification_date, e.hand_verification_sales_id, bu.username AS sales_name
			FROM escorts e
			LEFT JOIN backend_users bu ON bu.id = e.hand_verification_sales_id
			WHERE e.id = ?', $escort_id);
	
	}	

	public function getTextCopyright($escort_id)
	{
		return self::getAdapter()->fetchRow('
			SELECT e.text_copyright_date, e.text_copyrighter_id, bu.username AS sales_name
			FROM escorts e
			LEFT JOIN backend_users bu ON bu.id = e.text_copyrighter_id
			WHERE e.id = ?', $escort_id);
	
	}	

	

	public function updateHandVerificationDate($escort_id)
	{
		$bc_user = Zend_Auth::getInstance()->getIdentity();

		if (in_array(Cubix_Application::getId(),array(APP_EG_CO_UK,APP_ED))) {
            $phone_same_as_agency = self::getAdapter()->fetchRow('SELECT
                                                                    ag.id as agency_id,ag.contact_phone_parsed,ag.user_id
                                                                
                                                                FROM
                                                                    agencies ag
                                                                    INNER JOIN escorts e ON e.agency_id = ag.id
                                                                    INNER JOIN escort_profiles_v2 epv ON e.id = epv.escort_id 
                                                                    AND epv.phone_exists = ag.contact_phone_parsed 
                                                                WHERE
                                                                    e.id = ?
                                                                    ', $escort_id);

            if (!is_null($phone_same_as_agency->agency_id)) {
                $is_verified = self::getAdapter()->fetchRow('SELECT
                                                                    apv.id 
                                                                FROM
                                                                    agencies_phone_verification apv
                                                                    INNER JOIN escorts e ON e.agency_id = apv.agency_id 
                                                                WHERE
                                                                    e.id = ?
                                                                    AND apv.status = 1', $escort_id);
                if (!$is_verified->id) {
                    $data = array(
                        'agency_id' => $phone_same_as_agency->agency_id,
                        'user_id' => $phone_same_as_agency->user_id,
                        'type' => 'hand',
                        'phone' => $phone_same_as_agency->contact_phone_parsed,
                        'status' => 1,
                        'date' => new Zend_Db_Expr('NOW()')
                    );
                    parent::getAdapter()->insert('agencies_phone_verification', $data);
                }
            }
        }
		parent::getAdapter()->update($this->_table, array(
			'last_hand_verification_date' => new Zend_Db_Expr('NOW()'),
			'hand_verification_sales_id' => $bc_user->id,
		), parent::quote('id = ?', $escort_id));
		
	}

	public function updateWebsiteStatus($escort_id, $status, $sales)
	{
		try {
			if( $status == 'blocked' ){
				parent::getAdapter()->update($this->_table, array(
				'block_website_date' => null,
				'block_website' => 0,
				'check_website' =>  1,
				'check_website_date' =>  new Zend_Db_Expr('NOW()'),
				'block_website_person' =>  addslashes($sales),
				), parent::quote('id = ?', $escort_id));
			
			}elseif( $status == 'checked' ){
				parent::getAdapter()->update($this->_table, array(
				'block_website_date' => new Zend_Db_Expr('NOW()'),
				'block_website' => 1,
				'check_website' =>  0,
				'check_website_date' => null,
				'block_website_person' =>  addslashes($sales),
				), parent::quote('id = ?', $escort_id));
			}else{
				parent::getAdapter()->update($this->_table, array(
				'block_website_date' => null,
				'block_website' => 0,
				'check_website' =>  1,
				'check_website_date' =>  new Zend_Db_Expr('NOW()'),
				'block_website_person' =>  addslashes($sales),
				), parent::quote('id = ?', $escort_id));
			}
			return true;

		} catch ( Exception $e ) {
			throw $e;
		}
	}

	public function updateTextCopyright($escort_id)
	{
		$bc_user = Zend_Auth::getInstance()->getIdentity();
		

		parent::getAdapter()->update($this->_table, array(
			'text_copyright_date' => new Zend_Db_Expr('NOW()'),
			'text_copyrighter_id' => $bc_user->id,
		), parent::quote('id = ?', $escort_id));
		
	}

	

	public function activateNowManual($escort_id)
	{
		parent::getAdapter()->update('escorts', array(
			'activation_date' => null,
		), parent::quote('id = ?', $escort_id));
	}
	
	public function getChatFakeSchedule($escort_id)
	{
		return parent::getAdapter()->fetchRow('
			SELECT UNIX_TIMESTAMP(cfus.date_from) AS date_from, UNIX_TIMESTAMP(cfus.date_to) AS date_to
			FROM chat_fake_users_schedule cfus
			INNER JOIN escorts e ON e.user_id = cfus.user_id
			WHERE e.id = ? AND is_removed = 0
		', array($escort_id));
	}
	
	public function updateChatFakeSchedule($data)
	{
		$exist = parent::getAdapter()->fetchOne('SELECT TRUE FROM chat_fake_users_schedule WHERE user_id = ? ', $data['user_id']);
		
		if ( $data['show_chat_fake'] && !$exist ) {
			unset($data['show_chat_fake']);
			parent::getAdapter()->insert('chat_fake_users_schedule', $data);
		}
		elseif($data['show_chat_fake'] && $exist){
			unset($data['show_chat_fake']);
			$data['is_removed'] = 0;
			parent::getAdapter()->update('chat_fake_users_schedule', $data, parent::quote('user_id = ?', $data['user_id']));
		}
		elseif(!$data['show_chat_fake'] && $exist){
			parent::getAdapter()->update('chat_fake_users_schedule', array('is_removed' => 1), parent::quote('user_id = ?', $data['user_id']));
		}
		
		
	}

	public function loadRevision($escort_id)
	{
		$rev_count = parent::getAdapter()->fetchOne('SELECT COUNT(*) FROM profile_updates_v2 WHERE escort_id = ?', $escort_id);
		$is_updated = parent::getAdapter()->fetchOne('SELECT 1 FROM profile_updates_v2 WHERE escort_id = ? AND status = 1', $escort_id);

		return array('count' => $rev_count, 'is_updated' => $is_updated);
	}

	public function updateRotateType($escort_id, $type)
	{
		parent::getAdapter()->update($this->_table, array(
			'photo_rotate_type' => $type,
		), parent::quote('id = ?', $escort_id));
		
	}

	public function getDataForForum($id)
	{
		return parent::getAdapter()->query('
			SELECT e.showname, c.' . Cubix_I18n::getTblField('title') . ' AS base_city
			FROM escorts e
			INNER JOIN cities c ON c.id = e.city_id
			WHERE e.id = ?
		', $id)->fetch();
	}

	public function createActivationCode($escort_id){

		$bc_user = Zend_Auth::getInstance()->getIdentity();

		$user = self::getAdapter()->fetchRow('SELECT e.user_id, u.username, u.user_type, u.email, u.salt_hash, ep.contact_phone_parsed as phone
											  FROM escorts as e
											  LEFT JOIN users u ON u.id = e.user_id
											  LEFT JOIN escort_profiles_v2 ep ON e.id = ep.escort_id 
											  WHERE e.id = ?', 
											  $escort_id);

		if($user->user_type == 'escort' ){
			
			// Generate activation code
			$code = substr(mt_rand(100000,999999), 0, 5);
			
			// Activation code available still
			$activationAvailableTime = time() + (60 * Model_EscortsV2::SMS_ACTIVATION_TIME_MIN);
			
			// Save activation code
			$sql = 'UPDATE users SET activation_key = '.$code.', activation_available = '.$activationAvailableTime.' WHERE id = ?';
			self::getAdapter()->query($sql, $user->user_id);

			// Get and set Escort lang
			$escort_lng = Cubix_Api::getInstance()->call('getEscortSmsLng', array($escort_id));


			// Send Sms
			if($user->phone){

				$conf = Zend_Registry::get('system_config');
				$conf = $conf['sms'];

				// Get from number
				$originator = $phone_from = Cubix_Application::getPhoneNumber(Cubix_Application::getId());
				

				$activationSms = Cubix_I18n::translateByLng($escort_lng, 'activation_sms', array('CODE' => $code, 'USERNAME' => $user->username));
				$text = $activationSms;

				$sms = new Cubix_SMS($conf['userkey'], $conf['password']);
				$sms->setOriginator($originator);
				$sms->setRecipient($user->phone, md5(microtime()));
				$sms->setContent($text);
				$sms->sendSMS();


				// Create reset history
				$reset_history_model = new Model_ResetedPassword();

				$bu_user = Zend_Auth::getInstance()->getIdentity();
				
				$data = array();
				$data['user_id'] = $user->user_id;
				$data['sales_id'] = $bu_user->id;
				$data['created_date'] = date('Y-m-d H:i:s');
				$data['activation'] = 0;
				$data['code'] = $code;

				$reset_history_model->save($data);
			}

			// Send email
			 if($user->email){

			 	$email_tpl = 'reset_password';
			 	$newPassword = Cubix_Salt::generateRandomString(8);
			 	$passwordHash = Cubix_Salt::hashPassword($newPassword,$user->salt_hash);

			 	$tpl_data = array(
			 		'username' => $user->username,
			 		'pass' => $newPassword
			 	);
				
			 	Cubix_Email::sendTemplate($email_tpl, $user->email, $tpl_data);
//                 if ($success) {
                     $sql = "UPDATE `users` SET `password` = '{$passwordHash}' WHERE id = ?";
                     self::getAdapter()->query($sql, $user->user_id);
//                 }
			 }
		}
	}
	
	public function hadPhotoModification($escort_id, $last_sync_date)
	{
		return parent::getAdapter()->fetchOne('
			SELECT TRUE FROM journal j
			WHERE
				j.escort_id = ? AND
				j.event_type IN (' . implode(',', array(
				Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_ADDED,
				Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED,
				Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_DELETED,
			)) . ')  AND j.date > ?
		', array($escort_id, $last_sync_date));
	}

	/*public function hadPhotoModification($escort_id, $last_sync_date)
	{
		return parent::getAdapter()->fetchAll('
			SELECT event_type, data FROM journal j
			WHERE
				j.escort_id = ? AND
				j.event_type IN (' . implode(',', array(
				Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_ADDED,
				Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED,
				Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_DELETED,
			)) . ')  AND j.date > ?
		', array($escort_id, $last_sync_date));
	}*/
	public function getSourceUrl($escort_id){
		$sql = '
			SELECT source_url
			FROM escorts
			WHERE id = ?
		';

		$data = self::getAdapter()->fetchRow($sql, $escort_id);
		if($data){
			return $data->source_url;
		}else{
			return false;
		}
	}

	public function updateSourceUrl($source_url, $escort_id){

		$source_url = str_replace(' ', '', $source_url);
		if(strlen($source_url) < 1){
			$source_url = null;
		}


		$sql = 'UPDATE escorts SET source_url = ? WHERE id = ?';
		$this->getAdapter()->query($sql, array($source_url, $escort_id));
	}

	public function removeHappyHour($escort_id)
	{
		self::getAdapter()->update('escorts', array(
			'has_hh' => 0,
			'hh_status' => 2,
			'hh_save' => 0,
			'hh_date_from' => null,
			'hh_date_to' => null,
			'hh_motto' => null
		), self::getAdapter()->quoteInto('id = ?', $escort_id));
	}

	public function getPhoneComment($escort_id){
		$sql = '
			SELECT comment
			FROM escort_phone_comment
			WHERE escort_id = ?
		';

		$data = self::getAdapter()->fetchRow($sql, $escort_id);
		if($data){
			return $data->comment;
		}else{
			return false;
		}	
	}

	public function savePhoneComment($escort_id, $phone_comment){
		$commentExist = $this->getPhoneComment($escort_id);
		if($commentExist){
			if(strlen($phone_comment) > 1){
				self::getAdapter()->query("UPDATE escort_phone_comment SET comment = ? WHERE escort_id = ?", array($phone_comment, $escort_id));	
			}else{//remove
				self::getAdapter()->delete('escort_phone_comment', self::getAdapter()->quoteInto('escort_id = ?', $escort_id));
			}	
		}else{
				if(strlen($phone_comment) > 1){ 
					self::getAdapter()->insert('escort_phone_comment', array('escort_id' =>  $escort_id, 'comment' => $phone_comment ));	
				}
		}
	}

	public function savePhoneConfirm($escort_id, $phone_confirm_comment, $phone, $phone_parsed, $phone_prefix){

		$bu_user = Zend_Auth::getInstance()->getIdentity();
		// var_dump($bu_user->first_name);

		$data = array();
		$data['new_phone'] = $phone;
		$data['country_code'] = $phone_prefix;
		$data['new_phone_parsed'] = $phone_parsed; 
		$data['status'] = 1;
		$data['confirm_date'] = date('Y-m-d H:i:s');
		$data['confirm_by_sales'] = 1;
		$data['escort_id'] = $escort_id;
		
		$sql = '
			SELECT *
			FROM confirm_phone_number
			WHERE escort_id = ?
		';

		$confirmPhoneNumber = self::getAdapter()->fetchAll($sql, $escort_id);

		if($confirmPhoneNumber){
			unset($data['escort_id']);
			self::getAdapter()->update('confirm_phone_number',$data,$this->_db->quoteInto('escort_id = ?', $escort_id));
		}else{
			self::getAdapter()->insert('confirm_phone_number',$data);
		}

		$text = '<b>Confirm phone number: '.$phone_parsed.'</b><br/>';
		$text .= $phone_confirm_comment;

		$comment_data['comment'] = $text;
		$comment_data['date'] = date('Y-m-d H:i:s');
		$comment_data['email_sent'] = 0;
		$comment_data['sales_user_id'] = $bu_user->id;
		$comment_data['escort_id'] = $escort_id;

		$this->insertEscortComments($comment_data);
	}

	public function getAvailableTypes($master_id){
		$sql = '
			SELECT type
			FROM escorts
			WHERE master_id = ?
		';

		$existingTypes = self::getAdapter()->fetchAll($sql, $master_id);
		$DEFINITIONS = Zend_Registry::get('defines');
		$availableTypes = $DEFINITIONS['ESCORT_TYPES'];
		
		foreach ($availableTypes as $key => $availableType) {
			foreach ($existingTypes as $existingType) {
				if($existingType->type == $key){
					unset($availableTypes[$key]);
				}
			}
		}
		return $availableTypes;
	}
	public function usernameByMasterId($master_id){
		$sql = '
			SELECT u.username
			FROM escorts as e
			LEFT JOIN users u ON u.id = e.user_id
			WHERE e.master_id = ?
		';

		$username = self::getAdapter()->fetchRow($sql, $master_id);
		return $username->username;
	}

	public function duplicateEscort($data,$is_agency){
		$db = self::getAdapter();
		$images = new Cubix_Images();

		try {
			$error = 0;
			$db->beginTransaction();


			// Maybe after we need duplicate not from master ID, duplicate by selected escort ID
			$oldEscortId = $data['master_id']; 
			$currentDate = new Zend_Db_Expr('NOW()');
			// Duplicate user
			if($is_agency){ // It is agency escort, and we not need create new user.
				$userId = $is_agency;
			}else{ 
				$user = $db->fetchRow('SELECT u.*
									  FROM escorts as e
									  LEFT JOIN users u ON u.id = e.user_id
									  WHERE e.id = ?', 
									  $oldEscortId,Zend_Db::FETCH_ASSOC);
				unset($user['id']);
				
				$user['email'] = $data['new_reg_email'];
				$user['username'] = $data['new_username'];
				$user['date_registered'] = $currentDate;
				$user['date_last_refreshed'] = null;
				$user['last_login_date'] = null;
				
				$db->insert('users', $user);
				$userId = $db->lastInsertId();
			}
			
			//Duplicate escort                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             // Duplicate escort
			$escort = $db->fetchRow('SELECT * FROM escorts WHERE id = ?', $oldEscortId,Zend_Db::FETCH_ASSOC);
			unset($escort['id']);
			$escort['photo_suspicious_date'] = null;
			$escort['date_last_modified'] = null; 
			$escort['date_last_modified_backend'] = $currentDate;
			$escort['date_registered'] = $currentDate;
			$escort['due_date'] = null;
			$escort['package_activation_date'] = null;
			$escort['package_expiration_date'] = null;
			$escort['hh_date_from'] = null;
			$escort['hh_date_to'] = null;
			$escort['activation_date'] = $currentDate;

			if(!$is_agency)$escort['user_id'] = $userId;
			
			$escort['type'] = $data['type'];

			$db->insert('escorts', $escort);
			$newEscortId = $db->lastInsertId();
			
			// Duplicate escort_blocked_countries
			$escort_blocked_countries = $db->fetchAll('SELECT * FROM escort_blocked_countries WHERE escort_id = ?', $oldEscortId,Zend_Db::FETCH_ASSOC);
			if($escort_blocked_countries){
				foreach ($escort_blocked_countries as $item) {
					$item['escort_id'] = $newEscortId;	
					$db->insert('escort_blocked_countries', $item);
				}
			}

			// Duplicate escort_blocked_users
			$escort_blocked_users = $db->fetchAll('SELECT * FROM escort_blocked_users WHERE escort_id = ?', $oldEscortId,Zend_Db::FETCH_ASSOC);
			if($escort_blocked_users){
				foreach ($escort_blocked_users as $item) {
					$item['escort_id'] = $newEscortId;	
					$db->insert('escort_blocked_users', $item);
				}
			}

			// Duplicate escort_bubbletexts
			$escort_bubbletext = $db->fetchRow('SELECT * FROM escort_bubbletexts WHERE escort_id = ? ORDER BY id DESC LIMIT 1', $oldEscortId,Zend_Db::FETCH_ASSOC);
			if($escort_bubbletext){
			 unset($escort_bubbletext['id']);
				$escort_bubbletext['escort_id'] = $newEscortId;	
				$db->insert('escort_bubbletexts', $escort_bubbletext);
			}

			// Duplicate escort_cities
			$escort_cities = $db->fetchAll('SELECT * FROM escort_cities WHERE escort_id = ?', $oldEscortId,Zend_Db::FETCH_ASSOC);
			if($escort_cities){
				foreach ($escort_cities as $item) {
					$item['escort_id'] = $newEscortId;	
					$db->insert('escort_cities', $item);
				}
			}

			// Duplicate escort_cityzones
			$escort_cityzones = $db->fetchAll('SELECT * FROM escort_cityzones WHERE escort_id = ?', $oldEscortId,Zend_Db::FETCH_ASSOC);
			if($escort_cityzones){
				foreach ($escort_cityzones as $item) {
					$item['escort_id'] = $newEscortId;	
					$db->insert('escort_cityzones', $item);
				}
			}

			//Duplicate escort_keywords
			$escort_keywords = $db->fetchAll('SELECT * FROM escort_keywords WHERE escort_id = ?', $oldEscortId,Zend_Db::FETCH_ASSOC);
			if($escort_keywords){
				foreach ($escort_keywords as $item) {
					$item['escort_id'] = $newEscortId;	
					$db->insert('escort_keywords', $item);
				}
			}

			//Duplicate escort_langs
			$escort_langs = $db->fetchAll('SELECT * FROM escort_langs WHERE escort_id = ?', $oldEscortId,Zend_Db::FETCH_ASSOC);
			if($escort_langs){
				foreach ($escort_langs as $item) {
					$item['escort_id'] = $newEscortId;	
					$db->insert('escort_langs', $item);
				}
			}

			// Duplicate escort_profiles_v2
			$escort_profiles_v2 = $db->fetchRow('SELECT * FROM escort_profiles_v2 WHERE escort_id = ?', $oldEscortId,Zend_Db::FETCH_ASSOC);
			$escort_profiles_v2['escort_id'] = $newEscortId;
			$db->insert('escort_profiles_v2', $escort_profiles_v2);

			//Duplicate escort_rates
			$escort_rates = $db->fetchAll('SELECT * FROM escort_rates WHERE escort_id = ?', $oldEscortId,Zend_Db::FETCH_ASSOC);
			if($escort_rates){
				foreach ($escort_rates as $item) {
				    unset($item['id']);
					$item['escort_id'] = $newEscortId;	
					$db->insert('escort_rates', $item);
				}
			}

			// //Duplicate escort_services
			$escort_services = $db->fetchAll('SELECT * FROM escort_services WHERE escort_id = ?', $oldEscortId,Zend_Db::FETCH_ASSOC);
			if($escort_services){
				foreach ($escort_services as $item) {
					$item['escort_id'] = $newEscortId;	
					$db->insert('escort_services', $item);
				}
			}

			//Duplicate escort_slogans
			$escort_slogans = $db->fetchRow('SELECT * FROM escort_slogans WHERE escort_id = ? ORDER BY id DESC LIMIT 1', $oldEscortId,Zend_Db::FETCH_ASSOC);
			unset($escort_slogans['id']);
			$escort_slogans['escort_id'] = $newEscortId;
			$escort_slogans['date'] = $currentDate;
			$db->insert('escort_slogans', $escort_slogans);

			//Duplicate escort_travel_continents
			$escort_travel_continents = $db->fetchRow('SELECT * FROM escort_travel_continents WHERE escort_id = ?', $oldEscortId,Zend_Db::FETCH_ASSOC);
			$escort_travel_continents['escort_id'] = $newEscortId;
			$db->insert('escort_travel_continents', $escort_travel_continents);
			
			//Duplicate escort_services
			$escort_working_times = $db->fetchAll('SELECT * FROM escort_working_times WHERE escort_id = ?', $oldEscortId,Zend_Db::FETCH_ASSOC);
			if($escort_working_times){
				foreach ($escort_working_times as $item) {
					$item['escort_id'] = $newEscortId;	
					$db->insert('escort_working_times', $item);
				}
			}


			//Duplicate natural_pics
			$natural_pics = $db->fetchAll('SELECT * FROM natural_pics WHERE escort_id = ?', $oldEscortId,Zend_Db::FETCH_ASSOC);
			if($natural_pics){
				foreach ($natural_pics as $item) {
					//Copy image
					$images->copy($item['hash'],$item['ext'],$item['escort_id'] . '/natural-pics',$newEscortId . '/natural-pics',null,$is_agency,null);
					unset($item['id']);
					$item['escort_id'] = $newEscortId;
					$db->insert('natural_pics', $item);
				}
			}

			// escort_galleries
			$escort_galleries = $db->fetchAll('SELECT * FROM escort_galleries WHERE escort_id = ?', $oldEscortId,Zend_Db::FETCH_ASSOC);
			if($escort_galleries){
				$galleryRelation = array();
				foreach ($escort_galleries as $item) {
					$oldGalleryId  = $item['id'];
					unset($item['id']);

					$item['escort_id'] = $newEscortId;	
					
					$db->insert('escort_galleries', $item);
					$newGalleryId = $db->lastInsertId();

					$galleryRelation[] = array('oldGalleryId'=>$oldGalleryId,'newGalleryId'=>$newGalleryId);
				}
			}

			//Duplicate escort_photos
			$escort_photos = $db->fetchAll('SELECT * FROM escort_photos WHERE escort_id = ?', $oldEscortId,Zend_Db::FETCH_ASSOC);
			if($escort_photos){
				foreach ($escort_photos as $item) {
					unset($item['id']);
					$images->copy($item['hash'],$item['ext'],$item['escort_id'],$newEscortId,null,$is_agency,null);
					$item['escort_id'] = $newEscortId;
					$item['creation_date'] = date('Y-m-d H:i:s', time());
					if($item['gallery_id'] > 0){
						foreach ($galleryRelation as $galleryRel) {
							if($item['gallery_id'] == $galleryRel['oldGalleryId']){
								$item['gallery_id'] = $galleryRel['newGalleryId'];
								break;
							}
						}
					}
					$db->insert('escort_photos', $item);
				}
			}


			// DB is coped, but fizical file not coping
			// Video need to move
			// Duplicate video
            $videos = new Cubix_VideosCommon();
			 $video = $db->fetchAll('SELECT * FROM video WHERE escort_id = ?', $oldEscortId,Zend_Db::FETCH_ASSOC);
			 if($video){
			 	foreach ($video as $item) {
			 		$videoId = $item['id'];
			 		$videoObj  = (object) array(
			 		    'hash'=> $item['video']."_{$item['height']}p",
                        'ext' => 'mp4'
                    );
                    $localFile =  $videos->retrive($videoObj,$item['escort_id']);
                    if ($localFile){
                        $videos->_storeToPicVideo($localFile, $newEscortId, "{$videoObj->hash}.{$videoObj->ext}");
                        unset($item['id']);
                        $item['escort_id'] = $newEscortId;
                        $item['date'] =  date('Y-m-d H:i:s', time());
                        $db->insert('video', $item);
                        $newVideoId = $db->lastInsertId();

                        $videoImageRel = $db->fetchAll('SELECT * FROM video_image WHERE video_id = ?', $videoId,Zend_Db::FETCH_ASSOC);
                        if($videoImageRel){
                            foreach ($videoImageRel as $videoImageItem) {
                                $videoImage = $db->fetchRow('SELECT * FROM images WHERE id = ?', $videoImageItem['image_id'],Zend_Db::FETCH_ASSOC);
                                // Need generate new name, and move image;
                                if($videoImage){
                                    unset($videoImage['id']);
                                    $db->insert('images', $videoImage);
                                    $newVideoImageId = $db->lastInsertId();

                                    $videoImageItem['video_id'] = $newVideoId;
                                    $videoImageItem['image_id'] = $newVideoImageId;
                                    $db->insert('video_image', $videoImageItem);

                                }
                            }
                        }
                    }
			 	}
			 }

			if($db->commit()){
				return $newEscortId;
			}
			return false;
		}
		catch ( Exception $e ) {
			$db->rollBack();

			throw $e;
		}	
	}
	
	public function getEscortPicker($filter){
		
		$sql = '
			SELECT e.id, e.showname, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.gender, e.is_suspicious, min(er.price) AS price, 
			cur.title AS currency_title, co.title_en AS country,	c.title_en AS city, er.time,
			IF (er.time_unit = 1,"m", IF (er.time_unit = 2, "h", IF (er.time_unit = 3, "h", "")	) ) AS time_unit
			FROM escorts e
			LEFT JOIN cities c ON c.id = e.city_id
			LEFT JOIN countries co ON co.id = e.country_id
			LEFT JOIN escort_rates er ON er.escort_id = e.id
			LEFT JOIN currencies cur ON cur.id = er.currency_id
			WHERE e.id = ? AND e.status & ?
		';
		
		return parent::_fetchAll($sql, array($filter['id'], $filter['status']));
	}

	public function generateSecureToken($user_id)
	{
		$secure_token = bin2hex(openssl_random_pseudo_bytes(16));

		$sql = 'UPDATE users SET secure_token = ?, expiration_date = (now() + INTERVAL 10 MINUTE) WHERE id = ?';
		$this->getAdapter()->query($sql, array($secure_token, $user_id));
		
		return $secure_token;
	}

    public function getDataForIndependentExport(array $filter = array(), $page = 1, $per_page = 5000, $sort_field = 'escort_id', $sort_dir = 'DESC')
    {

        $fields = array(
            'e.id AS escort_id',
            'CASE e.type WHEN 1 THEN "Escort" WHEN 2 THEN "BDSM" ELSE "Massage" END AS escort_type',
            'e.showname',
            'so.phone_to AS sms_phone',
            'e.user_id AS usr_id',
            'c.title_en AS country',
            'cty.title_en AS city',
            'ull.ip AS last_ip',
            'bu.username AS sales_person',
            'u.username',
            'CASE e.status WHEN 32 THEN "active" ELSE "disabled" END as escort_status',
            'e.date_registered AS registration_date',
            'u.last_login_date',
            'u.email as email',
            'ep.email AS contact_email',
            'ep.contact_phone_parsed as phone_number',
//            'MIN(so.date) AS first_sms',
//            'MAX(so.date) AS last_sms',
            '(SELECT comment FROM escort_comments WHERE escort_id = e.id ORDER BY date DESC LIMIT 1) AS comment',
            '(SELECT bu.username FROM escort_comments ec LEFT JOIN backend_users bu ON bu.id = ec.sales_user_id WHERE ec.escort_id = e.id ORDER BY ec.date DESC LIMIT 1) AS commented_by',
            'e.last_hand_verification_date AS last_check_date',
            '(SELECT pck.name FROM order_packages op LEFT JOIN packages pck ON pck.id = op.package_id  WHERE op.escort_id = e.id AND op.status <> 2 ORDER BY op.date_activated DESC LIMIT 1) AS last_package',
            'e.active_package_name',
            'e.package_expiration_date',
            'CONCAT(app.url, "/escort/", e.showname, "-", e.id,"?f=b") as sedcard_url'
        );


        $sql = array(
            'tables' => array('escorts e'),
            'fields' => $fields,
            'joins' => array(
                'INNER JOIN users u ON u.id = e.user_id',
                'LEFT JOIN countries c ON c.id =  e.country_id',
                'LEFT JOIN cities cty ON cty.id = e.city_id',
                'LEFT JOIN backend_users bu ON bu.id = u.sales_user_id',
                'LEFT JOIN users_last_login ull ON ull.user_id = u.id',
                'LEFT JOIN escort_profiles_v2 ep ON ep.escort_id = e.id',
                'LEFT JOIN sms_outbox so ON so.escort_id = e.id',
                'LEFT JOIN applications app ON app.id = '.Cubix_Application::getId()
            ),
            'where' => array(
                ) + $filter,
            'group' => 'escort_id',
            'order' => array($sort_field, $sort_dir),
            'page' => array($page, $per_page)

        );


        $dataSql = Cubix_Model::getSql($sql);

        $data = parent::_fetchAll($dataSql);

        if( $data ) {
            foreach ($data as $dt) {
                $email = parent::_fetchRow('SELECT MAX(date) AS last_email, MIN(date) AS first_email FROM email_outbox em LEFT JOIN escorts e ON e.user_id = em.user_id WHERE em.user_id = ?', $dt['usr_id']);
                $sms = parent::_fetchRow('SELECT MAX(date) AS last_sms, MIN(date) AS first_sms FROM sms_outbox so LEFT JOIN escorts e ON so.escort_id = e.id WHERE so.phone_to = ?', $dt['sms_phone']);
                $dt['last_email'] = $email['last_email'];
                $dt['first_email'] = $email['first_email'];
                $dt['last_sms'] = $sms['last_sms'];
                $dt['first_sms'] = $sms['first_sms'];
            }
        }

//        echo '<pre>';
//        print_r($id);die;
//        die;
        return array(
            'data' => $data
        );
    }


    public function getActiveAgenciesByLastLoginInterval($interval = 31)
    {
        $sql = '
    	    SELECT
    	        u.id,
                u.email,
                u.backend_user,
                a.name,
                DATEDIFF( NOW( ), u.last_login_date ) AS datediff 
            FROM
                agencies AS a
                LEFT JOIN users u ON a.id = a.user_id 
            WHERE
                a.STATUS = '.USER_STATUS_ACTIVE.'
                AND (
                    u.last_login_date < DATE( DATE_ADD( NOW( ), INTERVAL - 31 DAY ) ) 
                    OR u.last_login_date IS NULL
                    )
                AND u.user_type = "agency" 
            GROUP BY
                u.email 
            HAVING
                MOD ( datediff, 31 ) = 0
    	';

        return  parent::_fetchAll($sql);
    }

    public function getActiveEscortsByLastLoginInterval()
    {
        $join = '';
        $lang_field = 'el.lang_id';
        if (in_array(Cubix_Application::getId(),array(APP_ED)))
        {
            $lang_field = 'CASE
                                WHEN ell.lang_id IS NOT NULL THEN ell.lang_id
                                WHEN u.lang IS NOT NULL THEN u.lang  
                                ELSE "en" 
                            END AS lang_id';
            $join .= 'LEFT JOIN (SELECT MAX(`level`),escort_id,lang_id from escort_langs GROUP BY escort_id ) ell ON ell.escort_id = e.id';
        }

    	$sql = '
    	    SELECT
    	        u.id,
                u.email,
                u.backend_user,
                e.showname,
                DATEDIFF( NOW( ), u.last_login_date ) AS datediff,
                '.$lang_field.' 
            FROM
                escorts AS e
                LEFT JOIN users u ON u.id = e.user_id 
                LEFT JOIN escort_langs el ON el.escort_id = e.id
                '.$join.'
            WHERE
                e.STATUS & 32 
                AND u.last_login_date < DATE( DATE_ADD( NOW( ), INTERVAL - 31 DAY ) ) 
                AND u.user_type = "escort" 
                
                GROUP BY u.email
            HAVING
                MOD ( datediff, 31 ) = 0
    	';

    	return  parent::_fetchAll($sql);
	  	
    }
    public function getPackageCountryId($escort_id){
        $countries_a = array(10,24,33,67);
        $package =  parent::_fetchRow('
		SELECT op.id AS opid,op.country_id
		FROM order_packages op
		INNER JOIN packages p ON p.id = op.package_id
		WHERE
			op.escort_id = ? AND
			op.status = ? AND
			op.application_id = ?
	', array($escort_id, 2, Cubix_Application::getId()));
        $responce = array();
        if($package){
            if($package->country_id > 0){
                $responce['country_id'] = $package->country_id;
                if(in_array($package->country_id, $countries_a)){
                    // Country Type A
                    $responce['country_type'] = 1;
                }else{
                    // Country Type B
                    $responce['country_type'] = 2;
                }
            }



        }
        return $responce;
    }

    public function updateLastModifiedDate($id)
    {
        $sql = 'UPDATE escorts SET date_last_modified = '.new Zend_Db_Expr('NOW()').' WHERE id =? ';
        $this->getAdapter()->query($sql,$id);
    }

    public function updateLastExportDate($id)
    {
        $sql = 'UPDATE escorts SET last_export_date = '.new Zend_Db_Expr('NOW()').' WHERE id =? ';
        $this->getAdapter()->query($sql,$id);
    }

    public function getLastExportDate()
    {
        $sql = 'SELECT MAX( last_export_date ) as last_export_date FROM escorts';
        return  parent::_fetchRow($sql);
    }

    public function getEscortKeywords($escort_id)
    {
        $db = self::getAdapter();
        $key_words = $db->fetchAll('SELECT keyword_id FROM escort_keywords WHERE escort_id = ?', $escort_id,Zend_Db::FETCH_ASSOC);
        $keyWords = array();
        if ($key_words)
        {
            foreach ($key_words as $keyword)
            {
                $keyWords[] = $keyword['keyword_id'];
            }
        }
        return $keyWords;
    }

    public function getEscortMessengers($escort_id,$orig_app_id)
    {
        $db = self::getAdapter();
        $field = '';
        if ($orig_app_id != APP_EF)
            $field .= ', ssignal';

        return $db->fetchRow('SELECT birth_date,viber,whatsapp,telegram'.$field.' FROM escort_profiles_v2 WHERE escort_id = ?', $escort_id,Zend_Db::FETCH_ASSOC);
    }

    public function getEscortLastCheckDate($escort_id)
    {
        return self::getAdapter()->fetchRow('SELECT last_hand_verification_date,hand_verification_sales_id FROM escorts WHERE id = ?', $escort_id,Zend_Db::FETCH_ASSOC);
    }

    public function getSessionId($user_id){
        return self::getAdapter()->fetchOne('SELECT
                                                        session_id 
                                                    FROM
                                                        users_last_login 
                                                    WHERE
                                                        user_id = ? 
                                                    ORDER BY
                                                        login_date DESC', $user_id);
    }

    public function getEscortTags($escortId) {
        $tags = self::getAdapter()->fetchAll("SELECT tag_id FROM escort_tags where escort_id = ?", array($escortId));

        $tagsArray = array();
        if (count($tags))
        {
            foreach ($tags as $tag)
            {
                $tagsArray[] = $tag->tag_id;
            }
        }

        return $tagsArray;
    }
}
