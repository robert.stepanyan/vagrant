<?php

class Model_Cron
{
	/**
	 * @var Zend_Db_Adapter_Abstract
	 */
	protected $_db;

	/**
	 * @var Model_Cron
	 */
	protected static $_instance;

	/**
	 * @return Model_Cron
	 */
	public static function getInstance()
	{
		if ( empty(self::$_instance) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	public static function run(array $stages) {
		$result = array('count' => 0, 'escorts' => array(), 'stages' => array(), 'counts' => array());

		$instance = self::getInstance();

		foreach ( $stages as $stage ) {
			$stage = 'process ' . str_replace(array('_', '-'), array(' ', ' '), $stage);
			$stage = str_replace(' ', '', ucwords($stage));
			if ( ! isset($result['stages'][$stage]) ) $result['stages'][$stage] = array();

			list($count, $escorts) = $instance->$stage();
			$result['count'] += $count;
			$result['counts'][$stage] = $count;
			if (is_array($escorts) ) {
				$result['escorts'] = array_merge($result['escorts'], $escorts);
			}
			$result['stages'][$stage][] = $escorts;
		}

		if (is_array($result['escorts']) ) {
			$result['escorts'] = array_unique($result['escorts']);
		}
		return $result;
	}

	public function __construct()
	{
		$this->_db = Zend_Registry::get('db');
	}

	public function ProcessPendingTransfers()
	{
		$result = array(0, array());
		return $result;
		//if ( ! isset(Cubix_Application::getId()) || ! Cubix_Application::getId() ) return $result;
		if ( Cubix_Application::getId() != APP_A6 && Cubix_Application::getId() != APP_A6_AT ) return $result;
		
		$transfers = $this->_db->fetchAll('
			SELECT
				t.id, t.date_transfered, t.status
			FROM transfers t
			INNER JOIN users u ON u.id = t.user_id
			INNER JOIN backend_users bu ON bu.id = t.backend_user_id			
			WHERE t.status = ? /*AND transfer_type_id = 1*/ AND DATE_SUB(CURDATE(),INTERVAL 4 DAY) >= DATE(t.date_transfered) AND transfer_type_id <> 8
			GROUP BY t.id
		', array(Model_Billing_Transfers::STATUS_PENDING));
		
		
		
		$model = new Model_Billing_Transfers();
		
		foreach ( $transfers as $tr ) {
			$transfer = $model->getDetails($tr->id);
			
			if ( Model_Billing_Transfers::STATUS_PENDING == $transfer->status || Model_Billing_Transfers::STATUS_CONFIRMED == $transfer->status ) {
				$transfer->autoReject("Transfer is auto rejected.", false);
			}
		}

		return $result;
	}
	
	public function ProcessPendingBankTransfers()
	{
		$bank_transfers = $this->_db->fetchAll('
			SELECT
				t.id, t.date_transfered, t.status
			FROM transfers t
			INNER JOIN users u ON u.id = t.user_id
			INNER JOIN backend_users bu ON bu.id = t.backend_user_id			
			WHERE t.status = ? AND transfer_type_id = 1 AND DATE_SUB(CURDATE(),INTERVAL 10 DAY) >= DATE(t.date_transfered)
			GROUP BY t.id
		', array(Model_Billing_Transfers::STATUS_PENDING));
		
		$result = array(0, array());
		
		$model = new Model_Billing_Transfers();
		
		foreach ( $bank_transfers as $tr ) {
			$transfer = $model->getDetails($tr->id);
			
			if ( Model_Billing_Transfers::STATUS_PENDING == $transfer->status || Model_Billing_Transfers::STATUS_CONFIRMED == $transfer->status ) {
				$transfer->autoReject("Bank Transfer is auto rejected.", false);
			}
		}

		return $result;
	}
	
	public function ProcessPaymentRejectedOrders()
	{
		$orders = $this->_db->fetchAll('
			SELECT
				o.id, u.id AS user_id, system_order_id, o.price, op.id AS order_package_id
			FROM orders o
			INNER JOIN users u ON u.id = o.user_id
			INNER JOIN backend_users bu ON bu.id = o.backend_user_id
			INNER JOIN order_packages op ON op.order_id = o.id
			WHERE o.status = ? AND op.status = ?
			GROUP BY o.id
			ORDER BY u.id ASC, o.order_date ASC
		', array(Model_Billing_Orders::STATUS_PAYMENT_REJECTED, Model_Billing_Packages::STATUS_ACTIVE));
		
		$result = array(0, array());
		$model = new Model_Billing_Orders();
		foreach ( $orders as $order ) {
			$this->_db->update('order_packages', array(
					'status' => Model_Billing_Packages::STATUS_PENDING
			), $this->_db->quoteInto('id = ?', $order->order_package_id));

			Zend_Registry::get('BillingHooker')->notify('order_created', array($order->order_package_id, $activated));
		}

		return $result;
	}
	
	public function ProcessPendingOrders()
	{
		$orders = $this->_db->fetchAll('
			SELECT
				o.id, u.id AS user_id, system_order_id, o.price
			FROM orders o
			INNER JOIN users u ON u.id = o.user_id
			INNER JOIN backend_users bu ON bu.id = o.backend_user_id
			INNER JOIN order_packages op ON op.order_id = o.id
			WHERE o.status IN (?, ?) AND o.use_balance = 1
			GROUP BY o.id
			ORDER BY u.id ASC, o.order_date ASC
		', array(Model_Billing_Orders::STATUS_PENDING, Model_Billing_Orders::STATUS_PAYMENT_DETAILS_RECEIVED));
		
		$result = array(0, array());
		$model = new Model_Billing_Orders();
		foreach ( $orders as $order ) {
			if ( true === $model->pay($order->id) ) {
				$this->_log('Order paid', array('Order' => $order->id));
				$result[0]++;
			}
		}

		return $result;
	}

	public function ProcessExpiredPackages()
	{
		$exclude = " ";
		$dateCondition = "DATE(op.expiration_date) <= DATE(NOW())";
		//Exclude Phone Packages from expired packages list
		if ( Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT ) {
			$phone_packages = array(Model_Billing_Packages::PHONE_PACKAGE_1_DAY_PASS, Model_Billing_Packages::PHONE_PACKAGE_3_DAY_PASS, Model_Billing_Packages::PHONE_PACKAGE_5_DAY_PASS);
			$exclude .= " AND op.package_id NOT IN (" . implode(', ', $phone_packages) . ") ";
		}

		if (Cubix_Application::getId() == APP_BL){
		    $dateCondition = "op.expiration_date <= NOW()";
        }

		/* --> Get all expired packages */
		$expired_packages = $this->_db->fetchAll("
			SELECT
				op.id, op.escort_id, op.status, op.application_id, previous_order_package_id, op.package_id, u.email AS user_email, u.sales_user_id as sales_id, bu.email AS sales_email, e.showname as user_name
			FROM order_packages op
			INNER JOIN escorts e ON e.id = op.escort_id
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN backend_users bu ON bu.id = u.sales_user_id
			WHERE $dateCondition AND op.status = ? AND op.status <> ? {$exclude}
		", array(Model_Packages::STATUS_ACTIVE, Model_Billing_Packages::STATUS_SUSPENDED));
		/* <-- */

		$result = array(0, array());
		foreach ( $expired_packages as $package ) {
			$result[1][] = $package->escort_id;
			/* --> Get packages that are needed to be activate after current's expiration */
			$packages = $this->_db->fetchAll('
				SELECT
					op.id, op.escort_id, o.status, o.activation_condition
				FROM order_packages op
				INNER JOIN orders o ON o.id = op.order_id
				WHERE
					op.activation_type = ? AND
					op.status = ? AND
					op.previous_order_package_id = ? AND
					op.application_id = ? AND 
					(o.status = ? OR o.status = ? )
			', array(
				Model_Packages::ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES,
				Model_Packages::STATUS_PENDING,
				$package->id,
				$package->application_id,
				Model_Billing_Orders::CONDITION_AFTER_PAID,
				Model_Billing_Orders::CONDITION_AFTER_RECEIVING_PAYMENT_DETAILS
			));
			/* <-- */

			$this->_db->beginTransaction();
			try {
				$activated = array();
				foreach ( $packages as $p ) {
					$this->_ActivatePackage($p->id, 'ProcessExpiredPackages');
					$activated[] = $p->id;
					$result[0]++;
					$result[1][] = $p->escort_id;
				}
				
				$this->_db->update('order_packages', array(
					'status' => Model_Billing_Packages::STATUS_EXPIRED
				), $this->_db->quoteInto('id = ?', $package->id));

				Zend_Registry::get('BillingHooker')->notify('package_expired', array($package->id, $activated));

				$this->_db->commit();

				$result[0]++;
				$result[1][] = $package->escort_id;
				
				// Special logic for beneluxxx special packages ))
				if ( Cubix_Application::getId() == APP_BL ) {
					
					if ( $package->package_id == Model_Billing_Packages::BL_BELGIQUE_SPECIAL || $package->package_id == Model_Billing_Packages::BL_HOLLAND_SPECIAL ) {
						$status = new Cubix_EscortStatus($package->escort_id);
						$status->setStatus(Cubix_EscortStatus::ESCORT_STATUS_OWNER_DISABLED);
						$status->save();
					}
					
					if ( $package->package_id != Model_Billing_Packages::BL_BELGIQUE_SPECIAL && $package->package_id != Model_Billing_Packages::BL_HOLLAND_SPECIAL ) {
						Cubix_Email::sendTemplate('expired_packages_notification', $package->user_email, array('sales_email' => $package->sales_email));
					}
					
					//var_dump( $package->package_id);die;
				}

				if( Cubix_Application::getId() == APP_ED){
					if(empty($activated)){
						if($package->sales_id == 201){

							$ex_status = new Cubix_EscortStatus($package->escort_id);
							$ex_status->removeStatusBit(Cubix_EscortStatus::ESCORT_STATUS_ACTIVE);
							$ex_status->setStatusBit(Cubix_EscortStatus::ESCORT_STATUS_AWAITING_PAYMENT);
							$ex_status->save();

							Cubix_Email::sendTemplate('package_expired_sales', $package->sales_email, array('salesname' => 'Andrew', 'escort_id' => $package->escort_id, 'escort_name' => $package->user_name));
							Cubix_Email::sendTemplate('package_expired_sales', 'tigmar1998@gmail.com', array('salesname' => 'Andrew', 'escort_id' => $package->escort_id, 'escort_name' => $package->user_name));
						}
					}
				}

				$this->_log('Package expired', array('Expired Package' => $package->id, 'Activated Instead' => $activated));
			}
			catch (Exception $e) {
				$this->_db->rollBack();

				$this->_log('Unable to expire the package', array('Package' => $package->id), 'ERROR');
			}
		}

		return $result;
	}
	
	public function ProcessExpiredPhonePackages()
	{		
		/* --> Get all expired packages */
		$expired_packages = $this->_db->fetchAll("
			SELECT
				op.id, op.escort_id, op.status, op.application_id, previous_order_package_id
			FROM order_packages op
			INNER JOIN phone_packages pp ON pp.order_package_id = op.id
			WHERE pp.expiration_date <= NOW() AND op.status = ? AND op.status <> ? AND op.package_id IN (" . Model_Billing_Packages::PHONE_PACKAGE_1_DAY_PASS . ", " . Model_Billing_Packages::PHONE_PACKAGE_3_DAY_PASS . ", " . Model_Billing_Packages::PHONE_PACKAGE_5_DAY_PASS . ")
		", array(Model_Packages::STATUS_ACTIVE, Model_Billing_Packages::STATUS_SUSPENDED));
		/* <-- */
//print_r($expired_packages);
		$result = array(0, array());
		foreach ( $expired_packages as $package ) {
			$result[1][] = $package->escort_id;
			/* --> Get packages that are needed to be activate after current's expiration */
			$packages = $this->_db->fetchAll('
				SELECT
					op.id, op.escort_id
				FROM order_packages op
				INNER JOIN orders o ON o.id = op.order_id
				WHERE
					op.activation_type = ? AND
					op.status = ? AND
					op.previous_order_package_id = ? AND
					op.application_id = ?
			', array(
				Model_Packages::ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES,
				Model_Packages::STATUS_PENDING,
				$package->id,
				$package->application_id
			));
			/* <-- */

			$this->_db->beginTransaction();
			try {
				$activated = array();
				foreach ( $packages as $p ) {
					$this->_ActivatePackage($p->id, 'ProcessExpiredPhonePackages');
					$activated[] = $p->id;
					$result[0]++;
					$result[1][] = $p->escort_id;
				}

				$this->_db->update('order_packages', array(
					'status' => Model_Billing_Packages::STATUS_EXPIRED
				), $this->_db->quoteInto('id = ?', $package->id));

				Zend_Registry::get('BillingHooker')->notify('package_expired', array($package->id, $activated));

				$this->_db->commit();

				$result[0]++;
				$result[1][] = $package->escort_id;

				$this->_log('Package expired', array('Expired Package' => $package->id, 'Activated Instead' => $activated));
			}
			catch (Exception $e) {
				$this->_db->rollBack();

				$this->_log('Unable to expire the package', array('Package' => $package->id), 'ERROR');
			}
		}

		return $result;
	}

	public function ProcessExpiredPostcheckPackages()
	{
		$result = array(0, array());
		$expired_transfers = $this->_db->fetchAll("
			SELECT
				t.id, t.date_transfered
			FROM transfers t			
			WHERE t.transfer_type_id = 8 AND t.status = 1 AND t.date_transfered < DATE_SUB(NOW(), INTERVAL ? DAY)
		", array(3));

		if ( count($expired_transfers) ) {
			foreach( $expired_transfers as $transfer ) {
				$transfer = new Model_Billing_TransferItem($transfer);

				$transfer->autoReject('72 Postcheck Auto Rejection!', true);
				
				$result[0]++;
			}
		}
		
		return $result;
	}

	public function ProcessAsapPackages()
	{
		$packages = $this->_GetWaittingPackages(Model_Packages::ACTIVATE_ASAP);

		$result = array(0, array());
		foreach ( $packages as $package ) {
			$this->_ActivatePackage($package->id, 'ProcessAsapPackages');
			$this->_log('Asap package activated', array('Package' => $package->id));
			$result[0]++;
			$result[1][] = $package->escort_id;
		}

		return $result;
	}

	public function ProcessExactDatePackages()
	{
		$packages = $this->_GetWaittingPackages(Model_Packages::ACTIVATE_AT_EXACT_DATE, 'DATE_FORMAT(op.activation_date, "%Y-%m-%d %H:%i:%s") <= DATE_FORMAT(NOW(), "%Y-%m-%d %H:%i:%s")');

		$result = array(0, array());
		foreach ( $packages as $package ) {
			$this->_ActivatePackage($package->id, 'ProcessExactDatePackages');
			$this->_log('Exact date package activated', array('Package' => $package->id));
			$result[0]++;
			$result[1][] = $package->escort_id;
		}

		return $result;
	}
	
	protected function ProcessFixAfterCurrentPackageExpiresPackages()
	{
		$packages = $this->_GetWaittingPackages(Model_Packages::ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES);
		$model_package = new Model_Billing_Packages();
		
		$result = array(0, array());
		foreach ( $packages as $package ) {
			$prev_order_package = $model_package->get($package->previous_order_package_id);
			if ( $prev_order_package->status != Model_Billing_Packages::STATUS_ACTIVE &&  $prev_order_package->status != Model_Billing_Packages::STATUS_SUSPENDED ) 
			{
				$this->_ActivatePackage($package->id, 'ProcessFixAfterCurrentPackageExpiresPackages');
				$this->_log('Fixing After Current Package Expires Package activation', array('Package' => $package->id));
				$result[0]++;
				$result[1][] = $package->escort_id;
			}
		}

		return $result;
	}

	/**
	 * Returns all packages that are waitting for activation
	 *
	 * @param int $which Package activation type
	 * @return array
	 */
	protected function _GetWaittingPackages($which, $additional = '')
	{
		$packages = $this->_db->fetchAll('
			SELECT
				op.id, op.activation_date, op.escort_id, op.application_id, op.previous_order_package_id
			FROM order_packages op
			INNER JOIN orders o ON o.id = op.order_id
			INNER JOIN escorts e ON e.id = op.escort_id
			WHERE
				op.status = ? AND (
					( o.activation_condition = ? AND o.status = ? ) OR
					( o.activation_condition = ? AND o.status IN (?, ?) ) OR
					( o.activation_condition = ? AND o.status IN (?, ?, ?) )
				) AND op.activation_type = ?' . (strlen($additional) ? ' AND ' . $additional : '') . '
				AND e.status & 32
			ORDER BY op.escort_id, o.order_date DESC
		', array(
			Model_Billing_Packages::STATUS_PENDING,

			/* --> Main Conditions-Statuses Logic (it describes which ones are "waitting for activation" packages) */
			Model_Billing_Orders::CONDITION_AFTER_PAID, Model_Billing_Orders::STATUS_PAID,
			Model_Billing_Orders::CONDITION_AFTER_RECEIVING_PAYMENT_DETAILS, Model_Billing_Orders::STATUS_PAID, Model_Billing_Orders::STATUS_PAYMENT_DETAILS_RECEIVED,
			Model_Billing_Orders::CONDITION_WITHOUT_PAYMENT, Model_Billing_Orders::STATUS_PAID, Model_Billing_Orders::STATUS_PAYMENT_DETAILS_RECEIVED, Model_Billing_Orders::STATUS_PENDING,
			/* <-- */

			$which
		));

		return $packages;
	}
	

	protected function _ActivatePackage($order_package_id, $func_name = null)
	{
		list($escort_id, $application_id, $period, $de_days, $package_id) = $this->_db->fetchRow('
			SELECT escort_id, application_id, period, de_days, package_id FROM order_packages WHERE id = ? AND order_id IS NOT NULL
		', $order_package_id, Zend_Db::FETCH_NUM);

		$m_escorts = new Model_Escorts();

		// Process if escort status is active
		if ( $m_escorts->hasStatusBit($escort_id, Model_Escorts::ESCORT_STATUS_ACTIVE) ) {
			/* --> Cancel Current Active Package of Escort and Chargeback to Balance */
			$current_order_package_id = $this->_db->fetchOne('
				SELECT id FROM order_packages WHERE status = ? AND escort_id = ? AND application_id = ?
			', array(Model_Packages::STATUS_ACTIVE, $escort_id, $application_id));

			if ( $current_order_package_id ) {
				$comment = 'Package #' . $current_order_package_id . ' cancelled by cron. Needed to activate package #' . $order_package_id;
				
				if ( $func_name ) {
					$comment .= ' : Function name: ' . $func_name;
				}
				
				$model = new Model_Billing_Packages();
				$model
					->get($current_order_package_id)
					->cancel($comment);
			}
			/* <-- */

			/* --> Activate Package Immediately */
			$date_activated = new Zend_Db_Expr('NOW()');
			$expiration_date = new Zend_Db_Expr('FROM_UNIXTIME(' . strtotime('+' . $period + $de_days . ' days', time()) . ')');
			
			
			$order = $this->_db->fetchRow('
				SELECT o.id, has_rejected_transfer 
				FROM order_packages op 
				INNER JOIN orders o ON o.id = op.order_id
				WHERE op.id = ?
			', array($order_package_id));
			
			
			$order_packages_update_data = array(
				'expiration_date' => $expiration_date,
				'status' => Model_Packages::STATUS_ACTIVE,
				'date_activated' => $date_activated
			
			);
			//IF PAYING FOR ORDER WITH STATUS transfer_rejected update only status
			//KEEP expiration and activation dates THE SAME
			if ( $order->has_rejected_transfer ) {
				unset($order_packages_update_data['expiration_date']);
				unset($order_packages_update_data['date_activated']);
			}
			//REMOVING has_rejected_transfer STATUS
			$this->_db->update('orders', array('has_rejected_transfer' => 0), $this->_db->quoteInto('id = ?', $order->id));
			
			$this->_db->update('order_packages', $order_packages_update_data, $this->_db->quoteInto('id = ?', $order_package_id));
			/* <-- */
			
			// Insert phone package expiration date with hours, minutes and seconds
			if ( (Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT) && in_array($package_id, array(Model_Billing_Packages::PHONE_PACKAGE_1_DAY_PASS, Model_Billing_Packages::PHONE_PACKAGE_3_DAY_PASS, Model_Billing_Packages::PHONE_PACKAGE_5_DAY_PASS)) ) {
				$this->_db->insert('phone_packages', array('order_package_id' => $order_package_id, 'expiration_date' => $expiration_date));
				//$this->_db->update('phone_packages', array('expiration_date' => $expiration_date), $this->_db->quoteInto('order_package_id = ?', $order_package_id));
			}

			Zend_Registry::get('BillingHooker')->notify('package_activated', array($order_package_id, $escort_id, $date_activated, $expiration_date));

		}
	}

	public function ProcessGracePeriod()
	{
		$config = Zend_Registry::get('system_config');
		$config = $config['billing'];
		$first_grace_period = $config['firstGracePeriod'];
		$second_grace_period = $config['secondGracePeriod'];

		$result = array(0, array());
		/* --> First Grace Period */
		$transfers = $this->_db->fetchAll('
			SELECT
				t.id
			FROM transfers t
			INNER JOIN transfer_orders `to` ON to.transfer_id = t.id
			WHERE t.status = ? AND t.date_transfered < DATE_SUB(NOW(), INTERVAL ? DAY)
		', array(Model_Billing_Transfers::STATUS_PENDING, $first_grace_period));

		$errors = array();
		foreach ( $transfers as $transfer ) {
			try {
				$transfer = new Model_Billing_TransferItem($transfer);
				$transfer->reject('First Grace Period Expired', false);
			}
			catch ( Exception $e ) {
				$errors[] = $transfer->id;
			}
		}

		if ( count($errors) ) {
			$this->_log('Could not reject following transfers in first grace period', array('Transfers' => $errors), 'ERROR');
		}
		/* <-- */
		
		/* --> Second Grace Period */
		$transfers = $this->_db->fetchAll('
			SELECT
				t.id
			FROM transfers t
			INNER JOIN transfer_rejections tr ON tr.transfer_id = t.id
			WHERE t.status = ? AND tr.date < DATE_SUB(NOW(), INTERVAL ? DAY)
		', array(Model_Billing_Transfers::STATUS_REJECTED, $second_grace_period));
		
		$errors = array();
		foreach ( $transfers as $transfer ) {
			try {
				$transfer = new Model_Billing_TransferItem($transfer);
				foreach ( $transfer->getOrders() as $order ) {
					if ( $order->getStatus() == Model_Billing_Orders::STATUS_PAYMENT_REJECTED ) {
						// This function returns escort ids for which packages have been cancelled
						$escorts = $order->close('Automatic close grace period expired');
						$result[1] = array_merge($result[1], $escorts);
					}
				}
			}
			catch ( Exception $e ) {
				$errors[] = $transfer->id;
			}

			$this->_db->update('transfers', array(
				'status' => Model_Billing_Transfers::STATUS_AUTO_REJECTED
			), $this->_db->quoteInto('id = ?', $transfer->id));

			Zend_Registry::get('BillingHooker')->notify('transfer_rejected', array($transfer->id));
		}

		if ( count($errors) ) {
			$this->_log('Could not close all orders for transfers in second grace period', array('Transfers' => $errors), 'ERROR');
		}
		/* <-- */

		return $result;
	}

	protected $_log_file = 'billing-cron.log';

	public function _log($message, array $data = array(), $type = 'INFO')
	{
		foreach ( $data as $key => $value ) {
			if ( is_array($value) ) {
				$value = '(' . implode(', ', $value) . ')';
			}
			
			$data[$key] = $key . ': ' . $value;
		}

		$message = '[' . $type . ' ' . date('d.m.y H:i:s') . ']: ' . $message . ' (' . implode(', ', $data) . ')' . "\r\n";

		// echo nl2br($message);

		@file_put_contents($this->_log_file, $message, FILE_APPEND);
	}

	const HH_STATUS_ACTIVE = 1;
	const HH_STATUS_PENDING = 2;
	const HH_STATUS_EXPIRED = 3;
	const HH_STATUS_ADMIN_DISABLED = 4;
	
	private function dateDiff($date_start, $date_end)
	{
	    $difference = $date_end - $date_start;
        $minutes = floor(($difference / 3600) * 60); // 3600 seconds in an hour

		return $minutes;
	}

	public function __ProcessHappyHour()
	{
		$hhs = $this->_db->fetchAll('
			SELECT
				e.id AS escort_id, e.agency_id, e.showname, e.has_hh, e.hh_status, e.hh_save,
				UNIX_TIMESTAMP(e.hh_date_from) AS hh_date_from, UNIX_TIMESTAMP(e.hh_date_to) AS hh_date_to, hh_motto
			FROM escorts e			
			WHERE e.has_hh = 1 AND e.status & ?
		', array(Cubix_EscortStatus::ESCORT_STATUS_ACTIVE));
		
	
		if ( count($hhs) > 0 ) {
			foreach( $hhs as $hh ) {
				// Activate happy hour
				if ( $hh->hh_date_from < time() && $hh->hh_date_to > time() ) {
					if ( $hh->hh_status != self::HH_STATUS_ACTIVE ) {
						echo "Activate " . $hh->showname . " Happy Hour\n";
						$this->_db->update('escorts', array('hh_status' => self::HH_STATUS_ACTIVE), $this->_db->quoteInto('id = ?', $hh->escort_id));
						Cubix_SyncNotifier::notify($hh->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_HAPPY_HOUR_ACTIVATED, array('escort_id' => $hh->escort_id));
					}
				}
				// Expire happy hour
				else if ( $hh->hh_date_to < time() ) {
					if ( $hh->hh_status != self::HH_STATUS_EXPIRED ) {
						echo "Expire " . $hh->showname . " Happy Hour\n";
						$this->_db->update('escorts', array('hh_status' => self::HH_STATUS_EXPIRED), $this->_db->quoteInto('id = ?', $hh->escort_id));
						Cubix_SyncNotifier::notify($hh->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_HAPPY_HOUR_DEACTIVATED, array('escort_id' => $hh->escort_id));
					}
				}
				else if ( $this->dateDiff($hh->hh_date_from, time()) > 7 * 24 * 60 ) {
					echo "Set to Pending " . $hh->showname . " Happy Hour\n";
					$this->_db->update('escorts', array('hh_status' => self::HH_STATUS_PENDING), $this->_db->quoteInto('id = ?', $hh->escort_id));
					Cubix_SyncNotifier::notify($hh->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_HAPPY_HOUR_DEACTIVATED, array('escort_id' => $hh->escort_id));

					if ( $hh->hh_save ) {
						$data = array(
							'hh_date_from' => strtotime('+7 days', $hh->hh_date_from),
							'hh_date_to' => strtotime('+7 days', $hh->hh_date_to)
						);
						echo "Update " . $hh->showname . " Happy Hour date\n";
						$this->_db->update('escorts', $data, $this->_db->quoteInto('id = ?', $hh->escort_id));

						if ( $hh->hh_date_from < time() && $hh->hh_date_to > time() ) {
							echo "Reactivate " . $hh->showname . " Happy Hour\n";
							$this->_db->update('escorts', array('hh_status' => self::HH_STATUS_ACTIVE), $this->_db->quoteInto('id = ?', $hh->escort_id));
							Cubix_SyncNotifier::notify($hh->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_HAPPY_HOUR_ACTIVATED, array('escort_id' => $hh->escort_id));
						}						
					}
				}
			}
		}	
	}

	public function ProcessHappyHour()
	{
		$hhs = $this->_db->fetchAll('
			SELECT
				e.id AS escort_id, e.agency_id, e.showname, e.has_hh, e.hh_status, e.hh_save,
				UNIX_TIMESTAMP(e.hh_date_from) AS hh_date_from, UNIX_TIMESTAMP(e.hh_date_to) AS hh_date_to, hh_motto
			FROM escorts e
			WHERE e.has_hh = 1 /*AND e.status & ?*/
		'/*, array(Cubix_EscortStatus::ESCORT_STATUS_ACTIVE)*/);


		if ( count($hhs) > 0 ) {
			foreach( $hhs as $hh ) {
				$activate_period = 7;
				if ( ! $hh->agency_id ) {
					$activate_period = 3;
				}
				// Activate happy hour
				if ( $hh->hh_date_from < time() && $hh->hh_date_to > time() ) {
					if ( $hh->hh_status != self::HH_STATUS_ACTIVE && $hh->hh_status != self::HH_STATUS_ADMIN_DISABLED ) {
						echo "Activate " . $hh->showname . " Happy Hour\n";
						$this->_db->update('escorts', array('hh_status' => self::HH_STATUS_ACTIVE), $this->_db->quoteInto('id = ?', $hh->escort_id));
						Cubix_SyncNotifier::notify($hh->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_HAPPY_HOUR_ACTIVATED, array('escort_id' => $hh->escort_id));
					}
				}
				// Expire happy hour
				else if ( $hh->hh_date_to < time() && $hh->hh_status != self::HH_STATUS_EXPIRED && $this->dateDiff($hh->hh_date_from, time()) < $activate_period * 24 * 60 ) {
						echo "Expire " . $hh->showname . " Happy Hour\n";
						$this->_db->update('escorts', array('hh_status' => self::HH_STATUS_EXPIRED), $this->_db->quoteInto('id = ?', $hh->escort_id));
						Cubix_SyncNotifier::notify($hh->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_HAPPY_HOUR_DEACTIVATED, array('escort_id' => $hh->escort_id));
				}
				else if ( $this->dateDiff($hh->hh_date_from, time()) > $activate_period * 24 * 60 ) {
					if ( $hh->hh_status != self::HH_STATUS_PENDING ) {
						echo "Set to Pending " . $hh->showname . " Happy Hour\n";
						$this->_db->update('escorts', array('hh_status' => self::HH_STATUS_PENDING), $this->_db->quoteInto('id = ?', $hh->escort_id));
						Cubix_SyncNotifier::notify($hh->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_HAPPY_HOUR_DEACTIVATED, array('escort_id' => $hh->escort_id));

						if ( $hh->hh_save ) {
							$data = array(
								'hh_date_from' => new Zend_Db_Expr('FROM_UNIXTIME(' . strtotime('+ ' . $activate_period . ' days', $hh->hh_date_from) . ')'),
								'hh_date_to' => new Zend_Db_Expr('FROM_UNIXTIME(' . strtotime('+ ' . $activate_period . ' days', $hh->hh_date_to) . ')')
							);
							
							echo "Update " . $hh->showname . " Happy Hour date\n";
							$this->_db->update('escorts', $data, $this->_db->quoteInto('id = ?', $hh->escort_id));

							if ( $hh->hh_date_from < time() && $hh->hh_date_to > time() ) {
								echo "Reactivate " . $hh->showname . " Happy Hour\n";
								$this->_db->update('escorts', array('hh_status' => self::HH_STATUS_ACTIVE), $this->_db->quoteInto('id = ?', $hh->escort_id));
								Cubix_SyncNotifier::notify($hh->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_HAPPY_HOUR_ACTIVATED, array('escort_id' => $hh->escort_id));
							}
						}
					}
				}
			}
		}


	}

	public function ProcessTrialPackageExpiresEmail()
	{
		$trial_escorts = $this->_db->fetchAll('
			SELECT
				e.id AS escort_id, e.showname, /*UNIX_TIMESTAMP(op.expiration_date) AS exp_date,*/ u.email, op.id AS order_package_id
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN order_packages op ON op.escort_id = e.id
			WHERE op.package_id = 98 AND op.status = ? AND u.user_type = \'escort\' AND e.home_city_id IS NULL AND op.trial_sent = 0 AND (op.expiration_date = DATE_ADD(CURDATE(), INTERVAL +1 DAY) OR op.expiration_date = CURDATE())
		', array(Model_Packages::STATUS_ACTIVE));

		if ( count($trial_escorts) ) {
			Cubix_Api_XmlRpc_Client::setApiKey(Cubix_Application::getById()->api_key);

			foreach( $trial_escorts as $escort ) {

				echo "Sending \"Trial Package Expires\" email to escort " . $escort->showname . "\r\n";
				Cubix_Email::sendTemplate('free_trial_package_expires', $escort->email, array('showname' => $escort->showname));

				$this->_db->update('order_packages', array('trial_sent' => 1), $this->_db->quoteInto('id = ?', $escort->order_package_id));
			}
		}
	}
	
	public function ProcessGotdEscorts()
	{		
		$expired_gotd_escorts = $this->_db->fetchAll('
			SELECT
				op.id, op.gotd_activation_date, op.escort_id, e.showname
			FROM order_packages op
			INNER JOIN escorts e ON e.id = op.escort_id
			WHERE op.status = ? AND op.gotd_status = ? AND DATE(op.gotd_activation_date) < DATE(NOW())
		', array(Model_Packages::STATUS_ACTIVE, Model_Products::GOTD_STATUS_ACTIVE));

		if ( count($expired_gotd_escorts) > 0 ) {
			echo "Found some expired gotds\n";
			foreach ( $expired_gotd_escorts as $g ) {
				echo "Escort {$g->showname}: ";
				$this->_db->update('order_packages', array('gotd_status' => Model_Products::GOTD_STATUS_EXPIRED), $this->_db->quoteInto('id = ?', $g->id));
				//$this->_db->query('DELETE FROM order_package_products WHERE order_package_id = ? AND product_id = ?', array($g->id, GIRL_OF_THE_DAY));
				Cubix_SyncNotifier::notify($g->escort_id, Cubix_SyncNotifier::EVENT_GOTD_EXPIRED, array('escort_id' => $g->escort_id));
				echo "marked as expired\n";
			}
			echo "\n";
		}
		else {
			echo "No expired gotds found\n\n";
		}
		
		$active_gotd_escorts = $this->_db->fetchAll('
			SELECT
				op.id, op.gotd_activation_date, op.escort_id, e.showname
			FROM order_packages op
			INNER JOIN escorts e ON e.id = op.escort_id
			WHERE op.status = ? /*AND op.gotd_status = ?*/ AND DATE(op.gotd_activation_date) = DATE(NOW())
		', array(Model_Packages::STATUS_ACTIVE/*, Model_Products::GOTD_STATUS_PENDING*/));
		
		if ( count($active_gotd_escorts) > 0 ) {
			echo "Found some gotds to activate\n";
			foreach ( $active_gotd_escorts as $g ) {
				echo "Escort {$g->showname}: ";
				$this->_db->update('order_packages', array('gotd_status' => Model_Products::GOTD_STATUS_ACTIVE), $this->_db->quoteInto('id = ?', $g->id));
				
				Cubix_SyncNotifier::notify($g->escort_id, Cubix_SyncNotifier::EVENT_GOTD_ACTIVATED, array('escort_id' => $g->escort_id));
				//break;
				//$this->_db->query('DELETE FROM order_package_products WHERE order_package_id = ? AND product_id = ?', array($g->id, GIRL_OF_THE_DAY));
				echo "marked as active\n";
			}
			echo "\n";
		}
		else {
			echo "No gotds to activate\n\n";
		}
	}

	public function ProcessVotd()
	{	
		try {
			$this->_db->beginTransaction;
		
			$this->_db->query('DELETE FROM votd_orders WHERE status = 1  AND TIME_TO_SEC(TIMEDIFF(NOW(), creation_date)) > ?', array(60*30));

			$expired_votd_orders = $this->_db->fetchAll('SELECT * FROM votd_orders WHERE status = 3 AND DATE(activation_date) < DATE(NOW()) AND is_notified = 0');

			$votd_orders_to_activate = $this->_db->fetchAll('SELECT * FROM votd_orders WHERE status = 2 AND DATE(activation_date) = DATE(NOW())');

			if(count($expired_votd_orders)){
				foreach($expired_votd_orders as $expired_votd){
					$this->_db->query("UPDATE votd_orders SET is_notified = ? WHERE id = ?", array(1, $expired_votd->id));
					$this->_db->query('UPDATE video SET is_votd = 0, votd_city_id = NULL WHERE escort_id = ?', array($expired_votd->escort_id));
					Cubix_SyncNotifier::notify($expired_votd->escort_id, Cubix_SyncNotifier::EVENT_VOTD_EXPIRED, array('escort_id' => $expired_votd->escort_id));
				}
			}
			if(count($votd_orders_to_activate)){
				foreach($votd_orders_to_activate as $votd_to_activate){
					$this->_db->query("UPDATE votd_orders SET status = ? WHERE id = ?", array(Model_Products::VOTD_STATUS_EXPIRED, $votd_to_activate->id));
					$this->_db->query('UPDATE video SET is_votd = 1, votd_city_id = ? WHERE escort_id = ? AND status = ?', array($votd_to_activate->city_id, $votd_to_activate->escort_id, 'approve'));
					Cubix_SyncNotifier::notify($votd_to_activate->escort_id, Cubix_SyncNotifier::EVENT_VOTD_ACTIVATED, array('escort_id' => $votd_to_activate->escort_id));
				}
			}



			//////// when votd video was delete and uploaded another one

			$votd_activated_orders = $this->_db->fetchAll('SELECT * FROM votd_orders WHERE status = 3 AND is_notified = 0 AND DATE(activation_date) = DATE(NOW())');

			if(count($votd_activated_orders)){
				foreach($votd_activated_orders as $votd_activated){
					$video = $this->_db->fetchRow('SELECT * FROM video WHERE escort_id = ? AND status = ? AND is_votd = ? ', array($votd_activated->escort_id, 'approve', 0));


					if(!empty($video)) {
						$this->_db->query('UPDATE video SET is_votd = 1, votd_city_id = ? WHERE escort_id = ? AND status = ? AND is_votd = ? ', array($votd_activated->city_id, $votd_activated->escort_id, 'approve', 0));
						
						Cubix_SyncNotifier::notify($votd_activated->escort_id, Cubix_SyncNotifier::EVENT_VOTD_REACTIVATED, array('escort_id' => $votd_activated->escort_id));
					}
				}
			}

			////////

			$this->_db->commit();
		} catch (Exception $e) {
			$this->_db->rollBack();
			var_dump($e);
		}
		
		
		die('done');
	}
	
	public function ProcessClassifiedAds()
	{
		$expired_premium_ads = $this->_db->fetchAll('
			SELECT 
				id
			FROM classified_ads
			WHERE is_premium = 1 AND premium_expiration_date <= NOW() AND status = ?
		', array(Model_ClassifiedAds::AD_STATUS_ACTIVE));
		
		if ( count($expired_premium_ads) ) {
			foreach ( $expired_premium_ads as $premium_ad ) {
				$this->_db->update('classified_ads', array('is_premium' => 0, 'premium_activation_date' => null, 'premium_expiration_date' => null), $this->_db->quoteInto('id = ?', $premium_ad->id));
				if (Cubix_Application::getId() == APP_A6){
					$m_updates = new Model_ClassifiedAdsUpdates();
					$m_updates->addEvent($premium_ad->id, Model_ClassifiedAdsUpdates::EVENT_TYPE_UPDATED);
				}
				echo "Ad #" . $premium_ad->id . " premium package is expired!\n";
			}
		} else {
			echo "No expired premium ads found!\n";
		}
		
		
		
		$expired_ads = $this->_db->fetchAll('
			SELECT 
				id
			FROM classified_ads
			WHERE expiration_date <= NOW() AND status = ?
		', array(Model_ClassifiedAds::AD_STATUS_ACTIVE));
		
		if ( count($expired_ads) ) {
			foreach ( $expired_ads as $ad ) {
				$this->_db->update('classified_ads', array('status' => Model_ClassifiedAds::AD_STATUS_EXPIRED), $this->_db->quoteInto('id = ?', $ad->id));
				if (Cubix_Application::getId() == APP_A6){
					$m_updates = new Model_ClassifiedAdsUpdates();
					$m_updates->addEvent($ad->id, Model_ClassifiedAdsUpdates::EVENT_TYPE_UPDATED);
				}
				echo "Ad #" . $ad->id . " is expired!\n";
			}
		} else {
			echo "No expired ads found!\n";
		}
	}
	
	public function ProcessActivePackagesWithoutExpDate()
	{	
		$result = array(0, array());
		$order_packages = $this->_db->fetchAll('
			SELECT id, order_id FROM order_packages WHERE status = ? and order_id IS NOT NULL AND expiration_date IS NULL
		', array(Model_Billing_Packages::STATUS_ACTIVE));
		
		
		foreach ( $order_packages as $package ) {
			$this->_db->update('order_packages', array('status' => Model_Billing_Packages::STATUS_EXPIRED, 'status_comment' => 'Package expired because of no expiration and activation dates defined.'), $this->_db->quoteInto('id = ?', $package->id));
			echo "Package #" . $package->id . " is expired. \n";
			$result[0]++;
		}
		
		return $result;
	}
	
	public function ProcessActivateOwnerDisabledWithPackages()
	{
		$packages = $this->_db->fetchAll('
			SELECT
				op.id, op.activation_date, op.escort_id, op.application_id, op.previous_order_package_id
			FROM order_packages op
			INNER JOIN orders o ON o.id = op.order_id
			INNER JOIN escorts e ON e.id = op.escort_id
			WHERE
				op.status = ? AND (
					( o.activation_condition = ? AND o.status = ? ) OR
					( o.activation_condition = ? AND o.status IN (?, ?) ) OR
					( o.activation_condition = ? AND o.status IN (?, ?, ?) )
				) AND 
				(
					op.activation_type = ? OR
					(op.activation_type = ? AND EXISTS( SELECT TRUE FROM order_packages WHERE id = op.previous_order_package_id AND status NOT IN (?, ?))) OR
					(op.activation_type = ? AND DATE_FORMAT(op.activation_date, "%Y-%m-%d %H:%i:%s") <= DATE_FORMAT(NOW(), "%Y-%m-%d %H:%i:%s"))
				)
				AND e.status & ? AND o.order_date > "2020-08-10"
			GROUP BY op.escort_id	
		', array(
			Model_Billing_Packages::STATUS_PENDING,
			/* --> Main Conditions-Statuses Logic (it describes which ones are "waitting for activation" packages) */
			Model_Billing_Orders::CONDITION_AFTER_PAID, Model_Billing_Orders::STATUS_PAID,
			Model_Billing_Orders::CONDITION_AFTER_RECEIVING_PAYMENT_DETAILS, Model_Billing_Orders::STATUS_PAID, Model_Billing_Orders::STATUS_PAYMENT_DETAILS_RECEIVED,
			Model_Billing_Orders::CONDITION_WITHOUT_PAYMENT, Model_Billing_Orders::STATUS_PAID, Model_Billing_Orders::STATUS_PAYMENT_DETAILS_RECEIVED, Model_Billing_Orders::STATUS_PENDING,
			/* <-- */
			Model_Packages::ACTIVATE_ASAP,
			Model_Packages::ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES, Model_Billing_Packages::STATUS_ACTIVE, Model_Billing_Packages::STATUS_SUSPENDED,
			Model_Packages::ACTIVATE_AT_EXACT_DATE,
			Cubix_EscortStatus::ESCORT_STATUS_OWNER_DISABLED	
			
		));
		
		$result = array(0, array());
		foreach ( $packages as $package ) {
			$status = new Cubix_EscortStatus($package->escort_id);
			$status->setStatus(Cubix_EscortStatus::ESCORT_STATUS_ACTIVE);
			$status->save();
			
			$result[0]++;
			$result[1][] = $package->escort_id;
			Cubix_SyncNotifier::notify($package->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_STATUS_UPDATED, array('action' => 'ProcessActivateOwnerDisabledWithPackages'));	
		}
		
		return $result;
	}		
}
