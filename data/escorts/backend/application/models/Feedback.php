<?php

class Model_Feedback extends Cubix_Model
{

    public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS id, name, email, to_addr, message, escort_showname, UNIX_TIMESTAMP(date) as date, flag, status, ip, city  
			FROM escort_feedbacks
			WHERE 1
		';

		$countSql = 'SELECT FOUND_ROWS()';
		
		if (in_array(Cubix_Application::getId(), array(APP_EG_CO_UK, APP_SI, APP_EI)))
		{
			$sql .= self::quote('AND application_id = ?', Cubix_Application::getId());
			//$countSql .= self::quote('AND application_id = ?', Cubix_Application::getId());
		}

		if ( strlen($filter['escort_showname']) ) {
			$sql .= self::quote('AND escort_showname LIKE ?', '%' . $filter['escort_showname'] . '%');
			//$countSql .= self::quote('AND escort_showname LIKE ?', '%' . $filter['escort_showname'] . '%');
		}
		
		if ( strlen($filter['escort_id']) ) {
			$sql .= self::quote('AND escort_id = ?', $filter['escort_id']);
			//$countSql .= self::quote('AND escort_id = ?', $filter['escort_id']);
		}

		if ( strlen($filter['email']) ) {
			$sql .= self::quote('AND email LIKE ?', '%' . $filter['email'] . '%');
			//$countSql .= self::quote('AND email LIKE ?', '%' . $filter['email'] . '%');
		}

        if ( strlen($filter['to_addr']) ) {
			$sql .= self::quote('AND to_addr LIKE ?', '%' . $filter['to_addr'] . '%');
			//$countSql .= self::quote('AND to_addr LIKE ?', '%' . $filter['to_addr'] . '%');
		}

        if ( strlen($filter['flag']) ) {
			$sql .= self::quote('AND flag = ?',  $filter['flag']);
			//$countSql .= self::quote('AND flag = ?', $filter['flag'] );
		}
		
        if ( strlen($filter['status']) ) {
			$sql .= self::quote('AND status = ?',  $filter['status']);
			//$countSql .= self::quote('AND status = ?', $filter['status'] );
		}
		
		if ( strlen($filter['ip']) ) {
			$sql .= self::quote('AND ip LIKE ?', $filter['ip'] . '%');
		}
		
		if ( strlen($filter['city']) ) {
			$sql .= self::quote('AND city LIKE ?', '%' . $filter['city'] . '%');
		}

		$sql .= '
			GROUP BY id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$ret = parent::_fetchAll($sql);
		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return $ret;
	}

	public function get($id)
	{
		return self::_fetchRow('
			SELECT * FROM escort_feedbacks WHERE id = ?
		', array($id));
	}

	public function update($data)
	{
		parent::getAdapter()->update('escort_feedbacks', $data, parent::quote('id = ?', $data['id']));
	}
}
