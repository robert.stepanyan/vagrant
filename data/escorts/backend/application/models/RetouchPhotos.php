<?php
class Model_RetouchPhotos extends Cubix_Model
{
	const NEEDS_RETOUCH   = 1;	
	const RETOUCHED		  = 2;
	const NO_NEED_RETOUCH = 3;
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = ' 
			SELECT SQL_CALC_FOUND_ROWS 
				e.showname , e.id AS id , e.id AS escort_id, UNIX_TIMESTAMP(e.date_last_modified) AS date_last_modified , 
				e.status, UNIX_TIMESTAMP(e.date_registered) AS date_registered,
				ag.name AS agency_name, ag.id AS agency_id
			FROM escort_photos ep
			INNER JOIN escorts e ON e.id = ep.escort_id
			LEFT JOIN agencies ag ON ag.id = e.agency_id
			WHERE NOT (e.status & 256)
		';
		
		$where = '';

		if ( strlen($filter['showname']) ) {
			if ($filter['mode'] == 'fixed')
				$where .= self::quote(' AND e.showname = ?', $filter['showname']);
			else
				$where .= self::quote(' AND e.showname LIKE ? ', $filter['showname'] . '%');
		}
				
		if ( $filter['escort_id'] ) {
			$where .= self::quote('AND e.id = ?', $filter['escort_id']);
		}
		
		$where .= self::quote(' AND ep.retouch_status = ? ', self::NEEDS_RETOUCH);
				
		$sql .= $where;
		
		$sql .= '
			GROUP BY ep.escort_id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$datas = parent::_fetchAll($sql);
		$count = $this->getAdapter()->fetchOne('SELECT FOUND_ROWS()');
		return $datas;
	}
	
	public function getRetouchPhotos($escort_id)
	{
		$photos = parent::_fetchAll('
			SELECT 
				u.application_id, ep.* 
			FROM escort_photos ep
			INNER JOIN escorts e ON e.id = ep.escort_id
			INNER JOIN users u ON u.id = e.user_id
			WHERE ep.escort_id = ? AND ep.retouch_status <> 0
			ORDER BY ep.retouch_status ASC
		', array($escort_id));
		
		foreach ($photos as $i => $photo) {
			$photos[$i] = new Model_Escort_PhotoItem($photo);
		}
		
		return $photos;
	}
	
	public function getRetouchPhoto($photo_id)
	{
		$photo = parent::_fetchRow('
			SELECT 
				u.application_id, ep.* 
			FROM escort_photos ep
			INNER JOIN escorts e ON e.id = ep.escort_id
			INNER JOIN users u ON u.id = e.user_id
			WHERE ep.id = ? AND ep.retouch_status <> 0
			ORDER BY ep.retouch_status ASC
		', array($photo_id));
		
		$photo = new Model_Escort_PhotoItem($photo);
		
		return $photo;
	}
	
	public function resetMainPhotos($escort_id)
	{
		parent::db()->query('UPDATE escort_photos SET is_main = 0 WHERE escort_id = ?', array($escort_id));
		
		return true;
	}
	
	public function Delete($escort_id,$photo_id)
	{
				
		$result = parent::db()->query('DELETE FROM escort_photos WHERE escort_id = ? AND id=?', array($escort_id,$photo_id))->rowCount();
		if($result>0)
			Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_DELETED, array('id' =>$photo_id, 'action' => 'retouch photo deleted'));
		return $result;
	}
	public function RemoveMark($escort_id,$photo_id)
	{
			
		$result = parent::db()->query("UPDATE escort_photos SET retouch_status='".self::NO_NEED_RETOUCH."',is_approved='1' WHERE escort_id =? AND id=?",array($escort_id,$photo_id))->rowCount();
		if($result>0)
			Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED, array('id' =>$photo_id, 'action' => 'retouch photo updated: '.NO_NEED_RETOUCH));
		return $result;
	}
}
