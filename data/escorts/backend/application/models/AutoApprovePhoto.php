<?php
class Model_AutoApprovePhoto extends Cubix_Model
{

	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = ' 
			SELECT SQL_CALC_FOUND_ROWS e.showname , e.id AS id ,
			UNIX_TIMESTAMP(e.date_last_modified) AS date_last_modified , e.status, UNIX_TIMESTAMP(e.date_registered) AS date_registered,
            ag.name AS agency_name, ag.id AS agency_id
			FROM escort_photos ep
			INNER JOIN escorts e ON e.id = ep.escort_id
			LEFT JOIN agencies ag ON ag.id = e.agency_id
			WHERE e.photo_auto_approval = 1 AND ep.double_approved = 0 AND  NOT (e.status & 256)
			
		';
		
		$where = '';

		if ( strlen($filter['showname']) ) {
			if ($filter['mode'] == 'fixed')
				$where .= self::quote(' AND e.showname = ?', $filter['showname']);
			else
				$where .= self::quote(' AND e.showname LIKE ? ', $filter['showname'] . '%');
		}
				
		if ( $filter['escort_id'] ) {
			$where .= self::quote('AND e.id = ?', $filter['escort_id']);
		}
				
		$sql .= $where;
		
		$sql .= '
			GROUP BY ep.escort_id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$datas = parent::_fetchAll($sql);
		$count = $this->getAdapter()->fetchOne('SELECT FOUND_ROWS()');
		return $datas;
	}	

}
