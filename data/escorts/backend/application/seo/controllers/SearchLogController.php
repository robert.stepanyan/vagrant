<?php

class Seo_SearchLogController extends Zend_Controller_Action
{
    /**
     * @var Model_SearchLog
     */
    protected $model;

    public function init()
    {
        $this->model = new Model_SearchLog();
    }

    public function indexAction()
    {

    }

    public function dataAction()
    {
        $req = $this->_request;
        $filter = array(
            'id' => $req->id,
            'query' => $req->query,
            'has_redirect_url' => $req->has_redirect_url,
            'no_redirect_url' => $req->no_redirect_url,
            'mobile_bigger_than' => $req->mobile_bigger_than,
            'desktop_bigger_than' => $req->desktop_bigger_than,
        );

        $count = 0;
        $data = $this->model->getAll(
            $req->page,
            $req->per_page,
            $filter,
            $req->sort_field,
            $req->sort_dir,
            $count
        );

        echo json_encode(array('data' => $data, 'count' => $count));
        die;
    }

    public function editAction()
    {
        $id = intval($this->_getParam('id'));
        $this->view->banner = $this->model->getById($id);

        if ($this->_request->isPost()) {
            return $this->save();
        }
    }

    public function setRedirectionAction()
    {
        $ids = $this->_getParam('id');

        if(is_string($ids) AND !empty($ids)) {
            $this->view->ids = $ids = explode(',', $ids);
        }

        if ($this->_request->isPost()) {
            $data['redirect_url'] = $this->_getParam('redirect_url');

            $response = ['status' => 'fail', 'use_callback' => true];
            if($this->model->update($ids, $data)) {
                $response['status'] = 'success';
            }

            echo json_encode($response);
            die;
        }

    }

}
