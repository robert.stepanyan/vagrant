<?php

class Seo_InstancesController extends Zend_Controller_Action 
{
	/**
	 * @var Model_Seo_Instances
	 */
	protected $_model;
	
	/**
	 * @var array
	 */
	protected $_entity_map = array();
	
	public function init()
	{
		if (Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT)
			$f_name = 'getFList';
		else
			$f_name = 'getList';
		
		$this->_entity_map = array(
			'region' => array(
				'callback' => array('Model_Geography_Regions', 'getList'),
				'fields' => array('id' => 'id', 'title' => 'title')
			),
			'blog-post' => array(
				'callback' => array('Model_Blog', $f_name),
				'fields' => array('id' => 'id', 'title' => 'title')
			),'city' => array(
				'callback' => array('Model_Geography_Cities', $f_name),
				'fields' => array('id' => 'id', 'title' => 'title')
			),
			'city-bdsm' => array(
				'callback' => array('Model_Geography_Cities', $f_name),
				'fields' => array('id' => 'id', 'title' => 'title')
			),
			'city-agencies' => array(
				'callback' => array('Model_Geography_Cities', $f_name),
				'fields' => array('id' => 'id', 'title' => 'title')
			),
			'city-agency' => array(
				'callback' => array('Model_Geography_Cities', $f_name),
				'fields' => array('id' => 'id', 'title' => 'title')
			),
			'city-agency-escorts' => array(
				'callback' => array('Model_Geography_Cities', $f_name),
				'fields' => array('id' => 'id', 'title' => 'title')
			),
			'city-boys-trans' => array(
				'callback' => array('Model_Geography_Cities', $f_name),
				'fields' => array('id' => 'id', 'title' => 'title')
			),
			'city-citytours' => array(
				'callback' => array('Model_Geography_Cities', $f_name),
				'fields' => array('id' => 'id', 'title' => 'title')
			),
			'city-escorts' => array(
				'callback' => array('Model_Geography_Cities', $f_name),
				'fields' => array('id' => 'id', 'title' => 'title')
			),
			'city-escorts-only' => array(
				'callback' => array('Model_Geography_Cities', $f_name),
				'fields' => array('id' => 'id', 'title' => 'title')
			),
			'city-independent-escorts' => array(
				'callback' => array('Model_Geography_Cities', $f_name),
				'fields' => array('id' => 'id', 'title' => 'title')
			),
			'city-upcomingtours' => array(
				'callback' => array('Model_Geography_Cities', $f_name),
				'fields' => array('id' => 'id', 'title' => 'title')
			),

			'country-bdsm' => array(
				'callback' => array('Model_Geography_Countries', $f_name),
				'fields' => array('id' => 'id', 'title' => 'title')
			),
			'country-agencies' => array(
				'callback' => array('Model_Geography_Countries', $f_name),
				'fields' => array('id' => 'id', 'title' => 'title')
			),
			'country-agency' => array(
				'callback' => array('Model_Geography_Countries', $f_name),
				'fields' => array('id' => 'id', 'title' => 'title')
			),
			'country-agency-escorts' => array(
				'callback' => array('Model_Geography_Countries', $f_name),
				'fields' => array('id' => 'id', 'title' => 'title')
			),
			'country-boys-trans' => array(
				'callback' => array('Model_Geography_Countries', $f_name),
				'fields' => array('id' => 'id', 'title' => 'title')
			),
			'country-citytours' => array(
				'callback' => array('Model_Geography_Countries', $f_name),
				'fields' => array('id' => 'id', 'title' => 'title')
			),
			'country-escorts' => array(
				'callback' => array('Model_Geography_Countries', $f_name),
				'fields' => array('id' => 'id', 'title' => 'title')
			),
			'country-escorts-only' => array(
				'callback' => array('Model_Geography_Countries', $f_name),
				'fields' => array('id' => 'id', 'title' => 'title')
			),
			'country-independent-escorts' => array(
				'callback' => array('Model_Geography_Countries', $f_name),
				'fields' => array('id' => 'id', 'title' => 'title')
			),
			'country-upcomingtours' => array(
				'callback' => array('Model_Geography_Countries', $f_name),
				'fields' => array('id' => 'id', 'title' => 'title')
			),

			'home-page-independantes' => array(
				'callback' => array('Model_Geography_Cities', 'getList'),
				'fields' => array('id' => 'id', 'title' => 'title')
			),
			'home-page-independent' => array(
				'callback' => array('Model_Geography_Cities', 'getList'),
				'fields' => array('id' => 'id', 'title' => 'title')
			),
			'cityzone' => array(
				'callback' => array('Model_Geography_Cityzones', 'getList'),
				'fields' => array('id' => 'id', 'title' => 'title')
			),
			'escort' => array(
				'callback' => array('Model_Escorts', 'getList'),
				'fields' => array('id' => 'id', 'title' => 'showname')
			),
			'classified-ads' => array(
				'callback' => array('Model_ClassifiedAds', 'getAllForSeo'),
				'fields' => array('id' => 'id', 'title' => 'title')
			),
			'agency' => array(
				'callback' => array('Model_Agencies', 'getAllForSeo'),
				'fields' => array('id' => 'id', 'title' => 'name')
			),
			'agency-profile' => array(
				'callback' => array('Model_Agencies', 'getAllForSeo'),
				'fields' => array('id' => 'id', 'title' => 'name')
			)
		);
		
		$this->_model = new Model_Seo_Instances();
	}

	public function syncAction()
	{
		$app_id = $this->_getParam('application_id');
		$http = 'http';
		$app = Cubix_Application::getById($app_id);

		
		if($app_id == APP_ED || $app_id == APP_A6 || $app_id == APP_EF){
			$http .= 's';
		}

		$url = $http.'://www.' . $app->host;
		$sync_url = $url . '/api/dictionary/seo-plain';

		$client = new Zend_Http_Client($sync_url, array(
			'maxredirects' => 5,
            'timeout'      => 90)
		);

		$client->setAuth('babazeo', 'GnaMer1a', Zend_Http_Client::AUTH_BASIC);

		$response = $client->request(Zend_Http_Client::GET);
		
		//$result = @file_get_contents($sync_url);

		if ( /*$result === false*/$response->isError() ) {
			die(json_encode(array('status' => 'error')));
		}

        if ( in_array(Cubix_Application::getId(), array(APP_EG_CO_UK))  ) {
            ignore_user_abort(true);
            ob_start();
            echo (json_encode(array('status' => 'success', 'a'=>'a' )));

            // usually it needs to be closed in this case it load forever in our server
            // header('Connection', 'close');
            // header('Content-Length: '.ob_get_length());

            ob_end_flush();
            ob_flush();
            flush();

            session_write_close();
            fastcgi_finish_request();
        }

		header('Cubix-message: ' . $url . ' has been successfully syncronized');
		die(json_encode(array('status' => 'success', 'result' => $url, 'response' => /*$result*/$response->getBody())));
	}

	public function indexAction()
	{
		$ses_filter = new Zend_Session_Namespace('seo_instances_data');
		$this->view->filter_params = $ses_filter->filter_params;
	}
	
	public function itemsAction()
	{
		$entity_id = intval($this->_getParam('entity_id'));
		
		if ( ! $entity_id ) die;
		
		$model = new Model_Seo_Entities();
		
		$entity = $model->getById($entity_id);
		
		if ( ! $entity ) die;
		
		$slug = $entity->slug;
		
		if ( ! isset($this->_entity_map[$slug]) ) die;
		
		$map = $this->_entity_map[$slug];
		
		$class = $map['callback'][0];
		
		$obj = new $class();
		
		$for_def_country_only = true;
		if ( Cubix_Application::getId() == APP_ED || Cubix_Application::getId() == 33 || Cubix_Application::getId() == 11 || Cubix_Application::getId() == 30 || Cubix_Application::getId() == 22 ) {
			$for_def_country_only = false;
		}
		
		$result = call_user_func(array($obj, $map['callback'][1]), $this->_getParam('value'), $for_def_country_only);
		
		$data = array();
		
		foreach ( $result as $row ) {
			$title = $row->{$map['fields']['title']};
			
			if ($slug == 'escort')
				$title .= ' (' . $row->id . ')';
			
			$data[] = array('id' => $row->id, 'title' => $title);
		}
		
		die(json_encode($data));
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'entity_id' => $this->_getParam('entity_id'),
			'item_title' => $this->_getParam('item_title'),
			'escort_id' => $this->_getParam('escort_id')
		);
		
		$data = $this->_model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter,
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		die(json_encode(array('data' => $data, 'count' => $count)));
	}
	
	public function createAction()
	{
		// TODO: Validation
		
		$this->view->layout()->disableLayout();
		
		$model = new Model_Seo_Entities();
		
		$this->view->entities = $model->getList();
		
		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'entity_id' => 'int',
				'primary_id' => 'int',
				'primary_title' => ''
			));
			$data = $data->getData();

			$data['primary_title'] = $this->_getParam('item');
			
			$data = array_merge($data, Cubix_I18n::getValues($this->_request->getParams(), 'title', ''));
			$data = array_merge($data, Cubix_I18n::getValues($this->_request->getParams(), 'heading', ''));
			$data = array_merge($data, Cubix_I18n::getValues($this->_request->getParams(), 'heading_escort', ''));
			$data = array_merge($data, Cubix_I18n::getValues($this->_request->getParams(), 'heading_trans', ''));
			$data = array_merge($data, Cubix_I18n::getValues($this->_request->getParams(), 'meta_description', ''));
			$data = array_merge($data, Cubix_I18n::getValues($this->_request->getParams(), 'meta_keywords', ''));
			$data = array_merge($data, Cubix_I18n::getValues($this->_request->getParams(), 'footer_text', ''));
			
			$validator = new Cubix_Validator();
			
			if ( ! $data['entity_id'] ) {
				$validator->setError('entity_id', 'Entity is required');
			}
			elseif ( ! $data['primary_id'] ) {
				$validator->setError('primary_id', 'Select the item');
			}
			
			if ( $validator->isValid() ) {
				$return = $this->_model->save(new Model_Seo_InstanceItem($data));
				if(is_array($return)){
					$validator->setError('primary_id', 'item exists check '.$return['title'] );
				}
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function editAction()
	{
		$this->view->layout()->disableLayout();
		
		$id = intval($this->_request->id);
		
		$model = new Model_Seo_Entities();
		
		$this->view->instance = $instance = $this->_model->getById($id);
		$this->view->entities = $model->getList();
		
		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'id' => 'int',
				/*'entity_id' => 'int',
				'primary_id' => 'int',
				'primary_title' => ''*/
			));
			$data = $data->getData();

			// $data['primary_title'] = $this->_getParam('item');
			
			$data = array_merge($data, Cubix_I18n::getValues($this->_request->getParams(), 'title', ''));
			$data = array_merge($data, Cubix_I18n::getValues($this->_request->getParams(), 'heading', ''));
			$data = array_merge($data, Cubix_I18n::getValues($this->_request->getParams(), 'heading_escort', ''));
			$data = array_merge($data, Cubix_I18n::getValues($this->_request->getParams(), 'heading_trans', ''));
			$data = array_merge($data, Cubix_I18n::getValues($this->_request->getParams(), 'meta_description', ''));
			$data = array_merge($data, Cubix_I18n::getValues($this->_request->getParams(), 'meta_keywords', ''));
			$data = array_merge($data, Cubix_I18n::getValues($this->_request->getParams(), 'footer_text', ''));

			$validator = new Cubix_Validator();
			
			/*if ( ! $data['entity_id'] ) {
				$validator->setError('entity_id', 'Entity is required');
			}
			elseif ( ! $data['primary_id'] ) {
				$validator->setError('primary_id', 'Select the item');
			}*/

			
			if ( $validator->isValid() ) {
				
				$this->_model->save(new Model_Seo_InstanceItem($data));
				}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function deleteAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->_model->remove($id);
	}

	public function compactAction()
	{
		$model_entities = new Model_Seo_Entities();
		
		$primary_id = $this->_getParam('primary_id');
		$primary_title = $this->_getParam('primary_title');
		$entity_slug = $this->_getParam('entity_slug');
		
		
		$entity = $model_entities->getBySlug($entity_slug);
		
		if ( ! $entity ) die;
		
		$instance = $this->_model->getByPrimaryId($entity_slug, $primary_id, array('primary_title' => $primary_title, 'entity_id' => $entity->id));
		$this->view->instance = $instance;

		if ( ! $instance ) {
			// if there is no instance with the parameters
			// specified, construct and pass an item to view
			
			$data = array(
				'entity_id' => $entity->id,
				'primary_id' => $primary_id,
				'primary_title' => $primary_title
			);
			
			$data = array_merge($data, Cubix_I18n::getValues(array(), 'title', ''));
			$data = array_merge($data, Cubix_I18n::getValues(array(), 'heading', ''));
			$data = array_merge($data, Cubix_I18n::getValues(array(), 'meta_description', ''));
			$data = array_merge($data, Cubix_I18n::getValues(array(), 'meta_keywords', ''));
			$data = array_merge($data, Cubix_I18n::getValues(array(), 'footer_text', ''));
			
			$instance = new Model_Seo_InstanceItem($data);
		}
		
		$this->view->instance = $instance;
		
		if ( $this->_request->isPost() ) {
			$data = array();
			
			$data = array_merge($data, Cubix_I18n::getValues($this->_request->getParams(), 'title', ''));
			$data = array_merge($data, Cubix_I18n::getValues($this->_request->getParams(), 'heading', ''));
			$data = array_merge($data, Cubix_I18n::getValues($this->_request->getParams(), 'meta_description', ''));
			$data = array_merge($data, Cubix_I18n::getValues($this->_request->getParams(), 'meta_keywords', ''));
			$data = array_merge($data, Cubix_I18n::getValues($this->_request->getParams(), 'footer_text', ''));

			$data['is_published'] = intval($this->_getParam('is_published'));

			foreach ( $data as $key => $value ) {
				$instance->{$key} = $value;
			}
			
			$this->_model->save($instance);
			
			die;
		}



		if ( $entity_slug == 'escort' )
		{
			$escort_id = $primary_id;
			$this->view->escort_id = $primary_id;

			$m_profile = new Model_Escort_ProfileV2(array('id' => $escort_id));

			$this->view->revisions = $m_profile->getRevisions();

			$this->view->model = $this->model;

			$revision = $m_profile->getLatestRevision();
			
			$old_revision = $revision - 1;

			$new = $m_profile->getRevision($revision);
			$old = $m_profile->getRevision($old_revision);

			if ( ! $old ) {
				$old = clone $new;
				$old->date_updated = null;
				$old->revision = '-';
				foreach ( $old->data as $field => $value ) {
					if ( ! is_array($value) ) $value = '';
					else $value = array();

					$old->data[$field] = $value;
				}
			}

			if ( ! $new ) {
				$this->_setParam('revision', $old->revision);
				$this->_setParam('no_changes', true);
				return $this->_forward('revisions');
			}

			$langs = Cubix_I18n::getLangs();

			foreach($langs as $lang_id) {
				$new->data['about'][$lang_id] = $new->data['about_' . $lang_id];
				$old->data['about'][$lang_id] = $old->data['about_' . $lang_id];
				unset($new->data['about_' . $lang_id]);
				unset($old->data['about_' . $lang_id]);

				$new->data['svc_additional'][$lang_id] = $new->data['additional_service_' . $lang_id];
				$old->data['svc_additional'][$lang_id] = $old->data['additional_service_' . $lang_id];
				unset($new->data['additional_service_' . $lang_id]);
				unset($old->data['additional_service_' . $lang_id]);
			}

			$new->data['availability']['incall_type'] = $new->data['incall_type'];
			$new->data['availability']['incall_hotel_room'] = $new->data['incall_hotel_room'];
			$new->data['availability']['incall_other'] = $new->data['incall_other'];
			$new->data['availability']['outcall_type'] = $new->data['outcall_type'];
			$new->data['availability']['outcall_other'] = $new->data['outcall_other'];
			unset($new->data['incall_type']);
			unset($new->data['incall_hotel_room']);
			unset($new->data['incall_other']);
			unset($new->data['outcall_type']);
			unset($new->data['outcall_other']);
			$old->data['availability']['incall_type'] = $old->data['incall_type'];
			$old->data['availability']['incall_hotel_room'] = $old->data['incall_hotel_room'];
			$old->data['availability']['incall_other'] = $old->data['incall_other'];
			$old->data['availability']['outcall_type'] = $old->data['outcall_type'];
			$old->data['availability']['outcall_other'] = $old->data['outcall_other'];
			unset($old->data['incall_type']);
			unset($old->data['incall_hotel_room']);
			unset($old->data['incall_other']);
			unset($old->data['outcall_type']);
			unset($old->data['outcall_other']);

			//print_r($new); die;

			$this->view->old = $old;
			$this->view->new = $new;

			$this->view->diff = $m_profile->differProfile($old->data, $new->data);
		}
	}
}
