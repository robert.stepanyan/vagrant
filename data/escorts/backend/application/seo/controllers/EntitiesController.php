<?php

class Seo_EntitiesController extends Zend_Controller_Action 
{
	/**
	 * @var Model_Seo_Entities
	 */
	protected $_model;
	
	public function init()
	{
		$this->_model = new Model_Seo_Entities();
	}
	
	public function indexAction()
	{
		
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'application_id' => $this->_request->application_id
		);
		
		$data = $this->_model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		die(json_encode(array('data' => $data, 'count' => $count)));
	}
	
	public function createAction()
	{
		$this->view->layout()->disableLayout();
		
		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'slug' => '',
				// 'application_id' => 'int',
				'has_instances' => 'int'
			));
			$data = $data->getData();

			$data = array_merge($data, Cubix_I18n::getValues($this->_request->getParams(), 'title', ''));
			$data = array_merge($data, Cubix_I18n::getValues($this->_request->getParams(), 'heading', ''));
			$data = array_merge($data, Cubix_I18n::getValues($this->_request->getParams(), 'meta_description', ''));
			$data = array_merge($data, Cubix_I18n::getValues($this->_request->getParams(), 'meta_keywords', ''));
			$data = array_merge($data, Cubix_I18n::getValues($this->_request->getParams(), 'footer_text', ''));
			
			$validator = new Cubix_Validator();
			
			if ( ! strlen($data['slug']) ) {
				$validator->setError('slug', 'Required');
			}
			/*else if ( $this->_model->existsBySlug($data['slug']) ) {
				$validator->setError('slug', 'Entity with the same slug exists');
			}*/
			
			if ( $validator->isValid() ) {
				$applications = array($this->_getParam('application_id'));
				if ( $this->_getParam('all_apps') ) {
					$applications = array();
					foreach ( Cubix_Application::getAll() as $app ) {
						$applications[] = $app->id;
					}
				}

				foreach ( $applications as $app_id ) {
					if ( $this->_model->existsBySlug($data['slug'], $app_id) ) {
						continue;
					}

					$data['application_id'] = $app_id;
					$this->_model->save(new Model_Seo_EntityItem($data));
				}
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function editAction()
	{
		$this->view->layout()->disableLayout();
		
		$id = intval($this->_request->id);
		
		$this->view->entity = $entity = $this->_model->getById($id);
		
		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'id' => 'int',
				'application_id' => 'int',
				'has_instances' => 'int'
			));
			$data = $data->getData();
			
			$data = array_merge($data, Cubix_I18n::getValues($this->_request->getParams(), 'title', ''));
			$data = array_merge($data, Cubix_I18n::getValues($this->_request->getParams(), 'heading', ''));
			$data = array_merge($data, Cubix_I18n::getValues($this->_request->getParams(), 'meta_description', ''));
			$data = array_merge($data, Cubix_I18n::getValues($this->_request->getParams(), 'meta_keywords', ''));
			$data = array_merge($data, Cubix_I18n::getValues($this->_request->getParams(), 'footer_text', ''));
			
			$validator = new Cubix_Validator();
			
			if ( $validator->isValid() ) {
				$this->_model->save(new Model_Seo_EntityItem($data));
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function deleteAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->_model->remove($id);
	}
}
