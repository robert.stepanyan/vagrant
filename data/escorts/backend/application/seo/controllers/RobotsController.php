<?php

class Seo_RobotsController extends Zend_Controller_Action
{
	protected $_model;
	
	public function init()
	{
		$this->_model = new Model_Seo_Robots();
	}
	
	public function indexAction()
	{
		
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'application_id' => $this->_request->application_id
		);
		
		$data = $this->_model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		die(json_encode(array('data' => $data, 'count' => $count)));
	}
		
	public function editAction()
	{
		$this->view->layout()->disableLayout();
		
		$id = intval($this->_request->id);
		
		$this->view->item = $entity = $this->_model->getById($id);
		
		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'id' => 'int',
				'application_id' => 'int',
				'body' => '',
				'm_body' => '',
				'google_id' => '',
			));
			$data = $data->getData();
			
			$validator = new Cubix_Validator();
			
			if ( $validator->isValid() ) {
				$this->_model->save(new Model_Seo_EntityItem($data));
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
}
