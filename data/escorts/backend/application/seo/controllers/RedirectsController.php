<?php

class Seo_RedirectsController extends Zend_Controller_Action
{
	protected $_model;

	public function init()
	{
		$this->_model = new Model_Seo_Redirects();
	}

	public function indexAction()
	{

	}

	public function dataAction()
	{
		$req = $this->_request;

		$filter = array(
			'application_id' => $this->_request->application_id,
			'url' => $this->_getParam('url')
		);

		$data = $this->_model->getAll(
			$this->_request->page,
			$this->_request->per_page,
			$filter,
			$this->_request->sort_field,
			$this->_request->sort_dir,
			$count
		);

		die(json_encode(array('data' => $data, 'count' => $count)));
	}

	public function editAction()
	{
		$this->view->layout()->disableLayout();

		$id = intval($this->_request->id);

		$this->view->redirect = $redirect = $this->_model->getById($id);

		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'id' => 'int',
				'application_id' => 'int',
				'url'	=> 'string',
				'redirect_page'	=> 'string'
			));
			$data = $data->getData();
			
			$validator = new Cubix_Validator();

			if ( $validator->isValid() ) {
				$this->_model->save(new Model_Seo_RedirectItem($data));
			}

			die(json_encode($validator->getStatus()));
		}
	}

	public function createAction()
	{
		$this->view->layout()->disableLayout();

		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'application_id' => 'int',
				'url'	=> 'string',
				'redirect_page'	=> 'string'
			));
			$data = $data->getData();

			$validator = new Cubix_Validator();
			
			if ( ! strlen($data['url']) ) {
				$validator->setError('url', 'Required');
			}
			if ( ! strlen($data['redirect_page']) ) {
				$validator->setError('redirect_page', 'Required');
			}

			if ( $validator->isValid() ) {
				/*$applications = array($this->_getParam('application_id'));
				if ( $this->_getParam('all_apps') ) {
					$applications = array();
					foreach ( Cubix_Application::getAll() as $app ) {
						$applications[] = $app->id;
					}
				}
				foreach ( $applications as $app_id ) {
					if ( $this->_model->existsBySlug($data['slug'], $app_id) ) {
						continue;
					}

					$data['application_id'] = $app_id;
					$this->_model->save(new Model_Seo_RedirectItem($data));
				}*/
				$this->_model->save(new Model_Seo_RedirectItem($data));
			}

			die(json_encode($validator->getStatus()));
		}
	}

	public function deleteAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->_model->remove($id);
	}
}