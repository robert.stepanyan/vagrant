<?php

class Sms_TemplatesController extends Zend_Controller_Action {
	
	public function init()
	{
		$this->_model = new Model_SMS_Templates();
	}
	
	public function indexAction()
	{
		
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'application_id' => $this->_request->application_id
		);
		
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		if (!in_array($bu_user->type, array('superadmin', 'admin')))
			$filter['sales_user_id'] = $bu_user->id;
		
		$data = $this->_model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		die(json_encode(array('data' => $data, 'count' => $count)));
	}
	
	public function createAction()
	{
		$this->view->layout()->disableLayout();
		
		if ( $this->_request->isPost() ) {
			$bu_user = Zend_Auth::getInstance()->getIdentity();
			
			$data = new Cubix_Form_Data($this->_request);
			$fields = array(
                'title' => '',
                'template' => '',
                'application_id' => 'int',
                'sales_user_id' => 'int'
            );

            if(in_array(Cubix_Application::getId(), array(APP_ED,APP_EG_CO_UK))){
                $fields['user_type'] = '';
            }

			$data->setFields($fields);
			$data = $data->getData();
            if(in_array(Cubix_Application::getId(), array(APP_ED,APP_EG_CO_UK))){
                if (count($data['user_type']) > 0)
                {
                    $user_tupe = implode(',',$data['user_type']);
                    $data['user_type'] = $user_tupe;
                }else{
                    unset($data['user_type']);
                }
            }
			$data['sales_user_id'] = $bu_user->id;
			
			$validator = new Cubix_Validator();
			
			if ( ! strlen($data['title']) ) {
				$validator->setError('title', 'Required');
			}
						
			if ( ! strlen($data['template']) ) {
				$validator->setError('template', 'Required');
			}
			
			if ( $validator->isValid() ) {
				$this->_model->save($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function editAction()
	{
		$this->view->layout()->disableLayout();
        $bu_user = Zend_Auth::getInstance()->getIdentity();
		$id = intval($this->_request->id);
		
		$this->view->item = $template = $this->_model->get($id);
		
		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$fields = array(
                'id' => 'int',
                'title' => '',
                'template' => '',
                'application_id' => 'int'
            );

            if(in_array(Cubix_Application::getId(), array(APP_ED,APP_EG_CO_UK))){
                $fields['user_type'] = '';
            }

			$data->setFields($fields);
			$data = $data->getData();

            if(in_array(Cubix_Application::getId(), array(APP_ED,APP_EG_CO_UK))){
                if (count($data['user_type']) > 0)
                {
                    $user_tupe = implode(',',$data['user_type']);
                    $data['user_type'] = $user_tupe;
                }else{
                    unset($data['user_type']);
                }
                $data['utype_change_user_id'] = $bu_user->id;
                $data['utype_change_date'] = new Zend_Db_Expr('NOW()');
            }

			$validator = new Cubix_Validator();
			
			if ( ! strlen($data['title']) ) {
				$validator->setError('title', 'Required');
			}
						
			if ( ! strlen($data['template']) ) {
				$validator->setError('template', 'Required');
			}
			
			if ( $validator->isValid() ) {
				$this->_model->save($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function deleteAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->_model->remove($id);

		die;
	}
}
