<?php

class Sms_EmailoutboxController extends Zend_Controller_Action {
	
	public function init()
	{
		$this->model = new Model_SMS_EmailOutbox();
	}
	
	public function indexAction() 
	{
		
	}
		
	public function dataAction()
	{
		$page = $this->_getParam('page');
		$per_page = $this->_getParam('per_page');
		$sort_field = $this->_getParam('sort_field');
		$sort_dir = $this->_getParam('sort_dir');

		$req = $this->_request;
			
		$filter = array(
			'backend_user_id' => $req->backend_user_id,
			'email' => $req->email,
			'escort_id' => $req->escort_id,
			'status' => $req->status,
			'template_id' => $req->template_id,
			'year' => $req->year,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to
		);

        if(in_array(Cubix_Application::getId(), array(APP_ED,APP_EG_CO_UK))){
            $filter['user_type'] = $req->user_type;
            $filter['from_type'] = $req->from_type;
        }

		$count = 0;
		$data = $this->model->getAll($page, $per_page, $sort_field, $sort_dir, $filter, $count);
		
		foreach ($data as $i => $item)
		{
		    $user = Model_Users::get($item->user_id);
			$data[$i]->text = strip_tags($item->text);
            $data[$i]->user_name = $user->username;
            $data[$i]->user_type = $user->user_type;
		}

		echo json_encode(array('data' => $data, 'count' => $count));
		die;
	}
	
	public function detailsAction()
	{
		$this->view->layout()->disableLayout();
		$id = intval($this->_getParam('id'));
		$this->view->data = $this->model->getById($id);
	}
	
	public function resendAction()
	{
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		$ids = $this->_getParam('id');
		if ( !is_array($ids) )
			$ids = array($ids);
		
		$data = $this->model->getRowByIds($ids);
		foreach($data as $row){
			unset($row->id);		
			$row->date = new Zend_Db_Expr('NOW()');
			$row->backend_user_id = $bu_user->id;
			$this->model->add((array)$row);
            Cubix_Email::send($row->email, $row->subject, $row->text, true);
		}
		echo '<pre>';var_dump($row, $data);die;
		die('OK');
	}		
}
