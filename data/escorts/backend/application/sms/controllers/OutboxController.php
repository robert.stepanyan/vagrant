<?php

class Sms_OutboxController extends Zend_Controller_Action {
	
	public function init()
	{
		$this->model = new Model_SMS_Outbox();
	}
	
	public function indexAction() 
	{
		$bu_user = Zend_Auth::getInstance()->getIdentity();

		$this->view->can_change_sender = true;

		if (Cubix_Application::getId() == APP_EF && ($bu_user->type != 'admin' && $bu_user->type != 'superadmin')) {
			$this->view->can_change_sender = false;	
		}	
	}
		
	public function dataAction()
	{
			
		$page = $this->_getParam('page');
		$per_page = $this->_getParam('per_page');
		$sort_field = $this->_getParam('sort_field');
		$sort_dir = $this->_getParam('sort_dir');
		

		$req = $this->_request;
			
					
		$filter = array(
			'showname' => $req->showname,
			'status' => $req->status,
			'escort_id' => $req->escort_id,
			'sender_sales_person' => $req->sender_sales_person,
			'e_status' => $req->e_status,
			'phone_to' => $req->phone_to,
			'year' => $req->year,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to,
			'application_id' => $req->application_id,
			'is_spam'	=> $req->is_spam,
			'skype_video_call' => $req->skype_video_call	
		);

//		var_dump($filter);die;
		$count = 0;

		$bu_user = Zend_Auth::getInstance()->getIdentity();

		if (Cubix_Application::getId() == APP_EF && ($bu_user->type != 'admin' && $bu_user->type != 'superadmin')){
			$filter['only_this_user'] = $bu_user->id;
		}

		$data = $this->model->getAll($page, $per_page, $sort_field, $sort_dir, $filter, $count);
				
		foreach ($data as $d)
		{
			//$phone = $d->contact_phone_parsed ? $d->contact_phone_parsed : $d->phone_to;
			$this->model->getEscortsByPhoneOrId($d->phone_to, $d->escort_id, $d);
			$d->sms_status_text = Model_SMS_SMS::$STATUS_TEXTS[$d->sms_status];
//			print_r($d);
			//$d->showname = $d->showname . ' (' . $d->escort_id . ')';
		}

//		print_r($data);
		echo json_encode(array('data' => $data, 'count' => $count));
		die;
	}
	
	public function deleteAction()
	{
		$id = $this->_request->id;
		
		if ( !is_array($id) )
			$id = array($id);
		
		$this->model->delete($id);
		
		die;
	}
	
	public function resendAction() 
	{
		$conf = Zend_Registry::get('system_config');

		$ids = $this->_request->id;
		
		if ( !is_array($ids) )
			$ids = array($ids);
		$resend_sms = $this->model->getByIds($ids);
		
		$model_sms = new Model_SMS_Outbox();

		$current_app_id = intval($this->_getParam('application_id'));
		$originator = Cubix_Application::getPhoneNumber($current_app_id);

		if (Cubix_Application::getId() != APP_6B)
		{
			$conf = $conf['sms'];
			$sms = new Cubix_SMS($conf['userkey'], $conf['password']);

			$sms->setOriginator($originator);

			$b_user = Zend_Auth::getInstance()->getIdentity();

			foreach( $resend_sms as $r_sms )
			{
				$r_sms = (array) $r_sms;
				$r_sms['sender_sales_person'] = $b_user->id;

				$id = $model_sms->save( $r_sms );

				$sms->setRecipient($r_sms['phone_number'], $id);

				$sms->setBufferedNotificationURL($conf['BufferedNotificationURL']);
				$sms->setDeliveryNotificationURL($conf['DeliveryNotificationURL']);
				$sms->setNonDeliveryNotificationURL($conf['NonDeliveryNotificationURL']);

				$sms->setContent($r_sms['text']);

				if (1 != $result = $sms->sendSMS()) {

				}
			}
		}
		else
		{

//			$conf = $conf['sms_6b'];
//			$sms = new Cubix_SMS6B($conf['user'], $conf['password']);
            $sms = Cubix_6B_SMS::getInstance();
			$b_user = Zend_Auth::getInstance()->getIdentity();

			foreach( $resend_sms as $r_sms )
			{
				$r_sms = (array) $r_sms;
				$r_sms['sender_sales_person'] = $b_user->id;

                $response = $sms->send_sms($r_sms['phone_number'], $r_sms['text'], '', date('d/M/Y'));
                $Recipients = $response->getRecipients();
                $r_sms['identificationNumber'] = $Recipients[0]['identificationNumber'];
                $id = $model_sms->save( $r_sms );
//				$sms->setDestination($r_sms['phone_number'], $id);
//				$sms->setContent($r_sms['text']);
//				$sms->send();
			}
		}

		die;
	}		
}
