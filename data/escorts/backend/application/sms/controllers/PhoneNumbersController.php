<?php

class SMS_PhoneNumbersController extends Zend_Controller_Action
{
	private static $jobsPath;
	
	public function init()
	{	
		$config = Zend_Registry::get('images_config');
		
       	if(isset($config['temp_dir_path'])){
			self::$jobsPath = $config['temp_dir_path'] . DIRECTORY_SEPARATOR .  'sms_jobs' . DIRECTORY_SEPARATOR;
		}
		else{
			self::$jobsPath = APPLICATION_PATH . '/sms_jobs/';
		}
		
		$this->model = new Model_SMS_PhoneNumbers();
	}
	
	public function indexAction()
	{
		$this->view->groups = Model_SMS_PhoneGroups::getForSelect();
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		$page = $this->_getParam('page');
		$per_page = $this->_getParam('per_page');
		$sort_field = $this->_getParam('sort_field');
		$sort_dir = $this->_getParam('sort_dir');
		$count = 0;
		
		$filter = array(
			'number' => preg_replace('/\s+/', '', $req->number),
			'group_id'	=> $req->group_id,
			'contact_state'	=> $req->contact_state,
			'blacklisted'	=> $req->blacklisted,
		);

        if ( Cubix_Application::getId() == APP_ED || Cubix_Application::getId() == APP_EG_CO_UK  ) {
            $filter['contact_times'] = $req->contact_times;
        }



        $data = $this->model->getAll( $page, $per_page, $sort_field, $sort_dir, $filter, $count);
        //print_r( $data ); die;
        if ( Cubix_Application::getId() == APP_ED ) {
            foreach ($data as $key => $item) {
               if($data[$key]->group_id == 15 && substr($data[$key]->number, 0, 5) == '00330') {
                    $data[$key]->number = substr($data[$key]->number, 0, 4).substr($data[$key]->number, 5);
                }
            }
        } elseif (Cubix_Application::getId() == APP_EG_CO_UK){
            foreach ($data as $key => $item) {
                if($data[$key]->group_id == 1 && substr($data[$key]->number, 0, 5) == '00440') {
                    $data[$key]->number = substr($data[$key]->number, 0, 4).substr($data[$key]->number, 5);
                }
            }
        }
		
		echo json_encode(array('data' => $data, 'count' => $count));
		die;
	}
	
	public function createAction()
	{
		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'number' => '',
				'group_id' => 'int'
			));
			$data = $data->getData();
			
			$validator = new Cubix_Validator();
				
			if ( ! strlen($data['number']) ) {
				$validator->setError('number', 'Required');
			} elseif ( ! is_numeric($data['number']) ) {
				$validator->setError('number', 'Only numbers');
			} elseif ( $this->model->checkIfExists($data['number']) ) {
				$validator->setError('number', 'Alraedy exists in db.');
			}
			
			if ( ! $data['group_id'] ) {
				$validator->setError('group_id', 'Required');
			}
			
			if ( $validator->isValid() ) {
				$this->model->save(new Model_SMS_PhoneNumbersItem($data));
			}
			
			die(json_encode($validator->getStatus()));
		} else {
			$this->view->groups = Model_SMS_PhoneGroups::getForSelect();
		}
	}
	
	public function editAction()
	{
		$id = intval($this->_getParam('id'));
		
		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'id'	=> 'int',
				'number' => '',
				'group_id' => 'int'
			));
			$data = $data->getData();
			
			$validator = new Cubix_Validator();
				
			if ( ! strlen($data['number']) ) {
				$validator->setError('number', 'Required');
			} elseif ( ! is_numeric($data['number']) ) {
				$validator->setError('number', 'Only numbers');
			} elseif ( $this->model->checkIfExists($data['number'], $data['id']) ) {
				$validator->setError('number', 'Alraedy exists in db.');
			}
			
			if ( ! $data['group_id'] ) {
				$validator->setError('group_id', 'Required');
			}
			
			if ( $validator->isValid() ) {
				$this->model->save(new Model_SMS_PhoneNumbersItem($data));
			}
			
			die(json_encode($validator->getStatus()));
		}
		else {
			$this->view->phone = $this->model->get($id);
			$this->view->groups = Model_SMS_PhoneGroups::getForSelect();
		}
	}
	
	public function removeAction()
	{
		$this->model->remove($this->_getParam('id'));
	}
	
	public function sendSmsAction()
	{
		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);

			$data->setFields(array(
				'body'	=> '',
				'notify_email' => '',
				'group_id' => '',
				'contact_state'	=> '',
				'count'	=> '',
                'contact_times' => ''
			));



			$data = $data->getData();


			
			$validator = new Cubix_Validator();
			
			if ( ! $data['group_id'] ) {
				$validator->setError('group_id', 'Required');
			}
			
			if ( ! $data['body'] ) {
				$validator->setError('body', 'Required');
			}
			
			if ( $data['notify_email'] && ! $validator->isValidEmail($data['notify_email']) ) {
				$validator->setError('notify_email', 'Invalid email');
			}
			
			if ( $data['count'] && strlen($data['count']) ) {
				if ( ! is_numeric($data['count']) ) {
					$validator->setError('count', 'Numeric');
				}
			}
			
			if ( $validator->isValid() ) {
				
				/*Getting phone numbers*/
				$p = $pp = false;
				if ( $data['count'] ) {
					$p = 1; 
					$pp = $data['count'];
				}
				
				$phone_numbers = array();
				$ph = $this->model->getAll($p, $pp, 'pn.id', 'ASC', array(
					'group_id'	=> $data['group_id'],
					'contact_state'	=> $data['contact_state'],
					'blacklisted'	=> 2,
                    'contact_times' => $data['contact_times']
				), $count);
				
				foreach($ph as $d) {
					$phone_numbers[] = $d->number;
				}
				/*Getting phone numbers*/
				
				if ( count($phone_numbers) ) {
					$bu_user = Zend_Auth::getInstance()->getIdentity();
					$job = array(
						'body'	=> $data['body'],
						'notify_email'	=> $data['notify_email'],
						'phone_numbers'	=> $phone_numbers,
						'admin_id'	=> $bu_user->id
					);
					if(Cubix_Application::getId() == APP_EF || Cubix_Application::getId() == APP_ED){
						$this->model->storeSmsJob(['body' => json_encode($job)]);
					}
					else{
						file_put_contents(self::$jobsPath . 'job' . time() . '.json', json_encode($job));
					}
				}
			}
			
			die(json_encode($validator->getStatus()));
		} else {
			$this->view->groups = Model_SMS_PhoneGroups::getForSelect();
		}
	}
	
	public function runSmsJobsAction()
	{
		if ( ! defined('IS_CLI') || ! IS_CLI ) {
			die('Please run from command line');
		}

		$cli = new Cubix_Cli();
		$pid_file = '/var/run/run_sms_job_' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		if ( $cli->isRunning() ) {
			die('Cron is already running, exitting...' . PHP_EOL);
		}

		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		
		
		$conf = Zend_Registry::get('system_config');
		$conf = $conf['sms'];
		
		$phone_from = $conf['number'];
		$SMS_USERKEY = $conf['userkey'];
		$SMS_PASSWORD = $conf['password'];

		$model_sms = new Model_SMS_Outbox();
		$sms = new Cubix_SMS($SMS_USERKEY, $SMS_PASSWORD);
		$sms->setOriginator($phone_from);
		
		$sms->setBufferedNotificationURL($conf['BufferedNotificationURL']);
		$sms->setDeliveryNotificationURL($conf['DeliveryNotificationURL']);
		$sms->setNonDeliveryNotificationURL($conf['NonDeliveryNotificationURL']);
		
		$files = scandir(self::$jobsPath);

		foreach($files as $file) {
			if ($file != "." && $file != "..") {
				$email_body = '';
				$job = json_decode(file_get_contents(self::$jobsPath . $file));
				$sended = array();
				try {
					foreach($job->phone_numbers AS $i => $phone_number) {
						echo($phone_number . "\n");

						$data = array(
							'phone_number' => $phone_number,
							'phone_from' => $phone_from,
							'text' => $job->body,
							'application_id' => Cubix_Application::getId(),
							'sender_sales_person' => $job->admin_id,
							'is_spam'	=> 1
						);
						
						$sms_id = $model_sms->save($data);

						ob_flush();
						$sms->setRecipient($phone_number, $sms_id);
						$sms->setContent($job->body);
						$sms->sendSMS();
						$sended[] = $phone_number;
					}
					echo('Job done' . "\n");
					$email_body = $job->body;
				} catch(Exception $ex) {
					echo('ERROR' . "\n");
					ob_flush();
					$email_body = 'Error occurred during sms job' . "\n";
					$email_body .= $ex->getMessage() . "\n";
					$email_body .= 'Sended numbers below' . "\n";
					$email_body .= implode("\n", $sended);
				}
				if ( $job->notify_email ) {
					Cubix_Email::send($job->notify_email, 'Sms job report', $email_body);
				}
				
				unlink(self::$jobsPath . $file);
				
				break; //Only per one job
			}
		}
		die;
	}
	
	public function runSmsJobsEfAction()
	{ 
		if ( ! defined('IS_CLI') || ! IS_CLI ) {
			die('Please run from command line');
		}

		$cli = new Cubix_Cli();
		$pid_file = '/var/run/run_sms_job_' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		if ( $cli->isRunning() ) {
			die('Cron is already running, exitting...' . PHP_EOL);
		}

		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		
		
		$conf = Zend_Registry::get('system_config');
		$conf = $conf['sms'];
		
		$phone_from = $conf['number'];
		$SMS_USERKEY = $conf['userkey'];
		$SMS_PASSWORD = $conf['password'];

		$model_sms = new Model_SMS_Outbox();
		$sms = new Cubix_SMS($SMS_USERKEY, $SMS_PASSWORD);
		$sms->setOriginator($phone_from);
		
		$sms->setBufferedNotificationURL($conf['BufferedNotificationURL']);
		$sms->setDeliveryNotificationURL($conf['DeliveryNotificationURL']);
		$sms->setNonDeliveryNotificationURL($conf['NonDeliveryNotificationURL']);
		
		$job_data = $this->model->getSmsJobs();
		
		$email_body = '';
		$job = json_decode($job_data->body);
		$sended = array();
		try {
			foreach($job->phone_numbers AS $i => $phone_number) {
				echo($phone_number . "\n");

				$data = array(
					'phone_number' => $phone_number,
					'phone_from' => $phone_from,
					'text' => $job->body,
					'application_id' => Cubix_Application::getId(),
					'sender_sales_person' => $job->admin_id,
					'is_spam'	=> 1
				);

				$sms_id = $model_sms->save($data);

				ob_flush();
				$sms->setRecipient($phone_number, $sms_id);
				$sms->setContent($job->body);
				$sms->sendSMS();
				$sended[] = $phone_number;
			}
			echo('Job done' . "\n");
			$email_body = $job->body;
		} catch(Exception $ex) {
			echo('ERROR' . "\n");
			ob_flush();
			$email_body = 'Error occurred during sms job' . "\n";
			$email_body .= $ex->getMessage() . "\n";
			$email_body .= 'Sended numbers below' . "\n";
			$email_body .= implode("\n", $sended);
		}
		
		if ( $job->notify_email ) {
			Cubix_Email::send($job->notify_email, 'Sms job report', $email_body);
		}
		
		$this->model->removeSmsJob($job_data->id);
		die;
	}
	
	/*public function importAction()
	{
		$group_id = 9;
		
		if ( ! $group_id ) die('Group ID required');
		
		$phone_numbers = file_get_contents(APPLICATION_PATH . '/phone_numbers.txt');
		$phone_numbers = explode("\n", $phone_numbers);
		$phone_numbers = array_unique($phone_numbers);
		
		$excluded_numbers = 0;
		foreach($phone_numbers as $number) {
			$number = preg_replace("/[^0-9]+/", "", $number);

			if ( ! strlen($number) ) continue;
			
			//$number = '0044' . $number;
			$number = '001' . $number;

			//$number = '0033' . substr_replace($number, '', 0, 1);
			
			echo $number . ' - ';
			$result = $this->model->checkIfExistsInProject($number);
			if ( $result['exists'] ) {
				$excluded_numbers++;
				echo $result['reason'];
			} else {
				$this->model->save(new Model_SMS_PhoneNumbersItem(array(
					'number'	=> $number,
					'group_id'	=> $group_id
				)));
				echo 'imported.';
			}
			echo "\n";
			ob_flush();
		}
		
		echo('Excluded ' . $excluded_numbers . ' from ' . count($phone_numbers) . "\n" );
		ob_flush();
		
		die('Done');
	}*/
	public function importAction()
	{
		if ( $this->_request->isPost() ) {

			$validator = new Cubix_Validator();

			if ( ! $this->_request->group_id )	{
				$validator->setError('group_id', 'Select group');
			}

			$country_code = $this->_request->getParam('country_code', '');
			$country_code = trim($country_code);
			$country_code = preg_replace("/[^0-9]+/", "", $country_code);

			$phone_numbers = $this->_request->phone_numbers;			
			if ( strlen($phone_numbers) ) {
				$phone_numbers = explode("\n", $phone_numbers);
				$phone_numbers = array_unique($phone_numbers);
			} else {
				$phone_numbers = array();
			}

			if ( ! count($phone_numbers) ) {
				$validator->setError('phone_numbers', 'Required');
			} elseif ( count($phone_numbers) > 3000 ) {
				$validator->setError('phone_numbers', 'No more than 3000.');
			}
				
			if ( ! $validator->isValid() ) {
			} else {
				$excluded_numbers = 0;
				foreach($phone_numbers as $number) {
					$number = preg_replace("/[^0-9]+/", "", $number);

					if ( ! strlen($number) ) continue;
					
					$number = $country_code . $number;

					$result = $this->model->checkIfExistsInProject($number);
					if ( $result['exists'] ) {
						$excluded_numbers++;
					} else {
						$this->model->save(new Model_SMS_PhoneNumbersItem(array(
							'number'	=> $number,
							'group_id'	=> $this->_request->group_id
						)));
					}
				}
				

				//echo('Excluded ' . $excluded_numbers . ' from ' . count($phone_numbers) . "\n" );
				
			}
			die(json_encode($validator->getStatus()));
		} else {
			$this->view->groups = Model_SMS_PhoneGroups::getForSelect();
		}
	}
	
	//Removing escorts which registered after their contact is imported to db
	//Maybe our campaign was successful :))
	public function removeExistingAction()
	{
		if ( ! defined('IS_CLI') || ! IS_CLI ) {
			die('Please run from command line');
		}

		$cli = new Cubix_Cli();
		$pid_file = '/var/run/remove_existing_' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		if ( $cli->isRunning() ) {
			die('Cron is already running, exitting...' . PHP_EOL);
		}

		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		
		$existing = $this->model->removeExisting();
		
		
		echo 'Removed ' . count($existing) . ' numbers';
		echo("\n");
		
		//only for 6anuncio yet
		if ( Cubix_Application::getId() == APP_6B ) {
			$not_delivered = $this->model->blacklistNotDelivered();
			echo 'Blacklisted ' . count($not_delivered) . ' numbers';
		}
		
		
		die;
	}
}

