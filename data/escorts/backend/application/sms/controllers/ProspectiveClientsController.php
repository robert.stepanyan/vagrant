<?php

class SMS_ProspectiveClientsController extends Zend_Controller_Action
{
	private static $jobsPath;
	
	public function init()
	{	
		self::$jobsPath = APPLICATION_PATH . '/sms_jobs/';
		$this->model = new Model_SMS_ProspectiveClients();
        $this->defs = Zend_Registry::get('definitions');
    }
	
	public function indexAction()
	{
	    $this->view->group_names = $this->defs['PROSPECTIVE_CLIENT_GROUPS'];
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		$page = $this->_getParam('page');
		$per_page = $this->_getParam('per_page');
		$sort_field = $this->_getParam('sort_field');
		$sort_dir = $this->_getParam('sort_dir');
		$count = 0;

		$filter = array(
			'number' => preg_replace('/\s+/', '', $req->number),
			'sales'	=> $req->sales,
			'country_code'	=> $req->country_code,
			'number'	=> $req->number,
			'matches_ef'	=> $req->matches_ef,
			'contacts_count'	=> $req->contacts_count,
			'sales_user_id_real' => $req->sales_user_id_real,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to,
			'group_name' => intval($req->group_name),
		);

        $data = $this->model->getAll( $page, $per_page, $sort_field, $sort_dir, $filter, $count);
       
		echo json_encode(array('data' => $data, 'count' => $count));
		die;
	}
	
	public function createAction()
	{

		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'number' => 'int',
				'country_code' => 'int'
			));

			$data = $data->getData();
			$validator = new Cubix_Validator();
			$bu_user = Zend_Auth::getInstance()->getIdentity();

			if ( ! strlen($data['number']) ) {
				$validator->setError('number', 'Required');
			} elseif ( $this->model->checkIfExists($data['country_code'].$data['number']) ) {
				$validator->setError('number', 'Alraedy exists in db.');
			}
			
			if ( ! $data['country_code'] ) {
				$validator->setError('country_code', 'Required');
			}
			
			//phone format check
			$country_iso = Model_SMS_ProspectiveClients::get_country_iso_by_code($data['country_code']);
			$result = Cubix_PhoneNumber::isValid( $data['country_code'].$data['number'], $country_iso->iso, null );
			
			//24 hours limit
			$last_24_hours_numbers_count = Model_SMS_ProspectiveClients::get_last_24_added_numbers_count($bu_user->id);

			if($last_24_hours_numbers_count->count > 3){
				$validator->setError('number', 'only 50 numbers allowed per 24 hour');
			}


			if ( $result === true ) {
				$data['parsed_number'] = $data['country_code'].$data['number'];
			}else{
				$validator->setError('number', $result);
			}

			if ( $validator->isValid() ) {
				
				$data['sales'] = $bu_user->id;
				$data['contacts_count']  = 0;
				$data['date_added']  = new Zend_Db_Expr('NOW()');
				//EF DB check 
				$res = Model_SMS_ProspectiveClients::checkPhoneInEf('00'.$data['country_code'].$data['number']);
				
				if($res){
					$data['matches_ef'] = 1;
				}else{
					$data['matches_ef'] = 0;
				}
				

				$this->model->save( new Model_SMS_ProspectiveClientsItem($data) );
			}
			
			die(json_encode($validator->getStatus()));
		} else {
			$this->view->country_codes = Model_SMS_ProspectiveClients::get_country_codes();
		}
	}
	
	public function editAction()
	{
		$id = intval($this->_getParam('id'));
		
		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'id'	=> 'int',
				'number' => '',
				'group_id' => 'int'
			));
			$data = $data->getData();
			
			$validator = new Cubix_Validator();
				
			if ( ! strlen($data['number']) ) {
				$validator->setError('number', 'Required');
			} elseif ( ! is_numeric($data['number']) ) {
				$validator->setError('number', 'Only numbers');
			} elseif ( $this->model->checkIfExists($data['number'], $data['id']) ) {
				$validator->setError('number', 'Alraedy exists in db.');
			}
			
			if ( ! $data['group_id'] ) {
				$validator->setError('group_id', 'Required');
			}
			
			if ( $validator->isValid() ) {
				$this->model->save(new Model_SMS_ProspectiveClientsItem($data));
			}
			
			die(json_encode($validator->getStatus()));
		}
		else {
			$this->view->phone = $this->model->get($id);
			$this->view->groups = Model_SMS_ProspectiveClients::getForSelect();
		}
	}
	
	public function removeAction()
	{
		$this->model->remove($this->_getParam('id'));die;
	}
	
	public function sendSmsAction()
	{
		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);

			$data->setFields(array(
				'body'	=> '',
				'notify_email' => '',
				'sms_sent'	=> '',
				'count'	=> '',
			));



			$data = $data->getData();



			$validator = new Cubix_Validator();
			
			if ( ! $data['body'] ) {
				$validator->setError('body', 'Required');
			}
			
			if ( $data['notify_email'] && ! $validator->isValidEmail($data['notify_email']) ) {
				$validator->setError('notify_email', 'Invalid email');
			}
			
			if ( $data['count'] && strlen($data['count']) ) {
				if ( ! is_numeric($data['count']) ) {
					$validator->setError('count', 'Numeric');
				}
			}
			
			if ( $validator->isValid() ) {
				
				/*Getting phone numbers*/
                $page = 1;
				$perPage = 200;
				$sms_sent = $data['sms_sent'];
				if ( $data['count'] ) {
                    $perPage = $data['count'];
				}

				$phone_numbers = array();
				$ph = $this->model->getForSms($page , $perPage, $sms_sent);

				foreach($ph as $d) {
					$phone_numbers[] = $d->parsed_number;
                    $this->model->updateSmsSentStatus($d->parsed_number);
				}
				/*Getting phone numbers*/
				
				if ( count($phone_numbers) ) {
					$bu_user = Zend_Auth::getInstance()->getIdentity();
					$job = array(
						'body'	=> $data['body'],
						'notify_email'	=> $data['notify_email'],
						'phone_numbers'	=> $phone_numbers,
						'admin_id'	=> $bu_user->id
					);

					file_put_contents(self::$jobsPath . 'job' . time() . '.json', json_encode($job));
				}
			}
			
			die(json_encode($validator->getStatus()));
		} else {
            $this->view->groups = Model_SMS_PhoneGroups::getForSelect();
		}
	}

    public function sendExactSmsAction()
    {
        if ( $this->_request->isPost() ) {
            $data = new Cubix_Form_Data($this->_request);

            $data->setFields(array(
                'body'	=> '',
                'notify_email' => '',
            ));



            $data = $data->getData();



            $validator = new Cubix_Validator();

            if ( ! $data['body'] ) {
                $validator->setError('body', 'Required');
            }

            if ( $data['notify_email'] && ! $validator->isValidEmail($data['notify_email']) ) {
                $validator->setError('notify_email', 'Invalid email');
            }

            if ( $validator->isValid() ) {

                $conf = Zend_Registry::get('system_config');
                $conf = $conf['sms'];

                $phone_from = $conf['number'];
                $SMS_USERKEY = $conf['userkey'];
                $SMS_PASSWORD = $conf['password'];

                $model_sms = new Model_SMS_Outbox();
                $sms = new Cubix_SMS($SMS_USERKEY, $SMS_PASSWORD);
                $sms->setOriginator($phone_from);

                $sms->setBufferedNotificationURL($conf['BufferedNotificationURL']);
                $sms->setDeliveryNotificationURL($conf['DeliveryNotificationURL']);
                $sms->setNonDeliveryNotificationURL($conf['NonDeliveryNotificationURL']);

                /*Getting phone numbers*/
                $_ids = $this->_request->id;

                $ids = '';
                if (count($_ids))
                {
                    $numItems = count($_ids);
                    $i = 0;
                    foreach ($_ids as $id)
                    {
                        if(++$i === $numItems)
                        {
                            $ids .= $id;
                        }else{
                            $ids .= $id . ',';
                        }
                    }
                }else{
                    $validator->setError('phone', 'Required');
                }

                $phone_numbers = $this->model->getPhoneNumbersByIds($ids);

                /*Getting phone numbers*/

                if (count($phone_numbers)) {

                    $bu_user = Zend_Auth::getInstance()->getIdentity();
                    $sended = array();
                    try {
                        foreach ($phone_numbers as $phone_number) {
                            Model_SMS_ProspectiveClients::updateProspectiveClientsContactTime($phone_number);

                            if (!preg_match('/^00/',$phone_number) && !preg_match('/^+/',$phone_number))
                            {
                                $phone_number = '00' . $phone_number ;
                            }


                            $sms_data = array(
                                'phone_number' => $phone_number,
                                'phone_from' => $phone_from,
                                'text' => $data['body'],
                                'application_id' => Cubix_Application::getId(),
                                'sender_sales_person' => $bu_user->id,
                                'is_spam' => 1
                            );

                            $sms_id = $model_sms->save($sms_data);

                            ob_flush();
                            $sms->setRecipient($phone_number, $sms_id);
                            $sms->setContent($sms_data['text']);
                            $sms->sendSMS();
                            $sended[] = $phone_number;
                        }
                        $email_body = $data['body'];
                    } catch (Exception $ex) {
                        echo('ERROR' . "\n");
                        ob_flush();
                        $email_body = 'Error occurred during sms job' . "\n";
                        $email_body .= $ex->getMessage() . "\n";
                        $email_body .= 'Sended numbers below' . "\n";
                        $email_body .= implode("\n", $sended);
                    }

                    if ($data['notify_email']) {
                        Cubix_Email::send($data['notify_email'], 'Sms job report', $email_body);
                    }

                }
            }

            die(json_encode($validator->getStatus()));
        } else {
            $this->view->ids = $this->_request->id;
        }
    }

	public function runSmsJobsAction()
	{
		if ( ! defined('IS_CLI') || ! IS_CLI ) {
			die('Please run from command line');
		}

		$cli = new Cubix_Cli();
		$pid_file = '/var/run/run_sms_job_' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		if ( $cli->isRunning() ) {
			die('Cron is already running, exitting...' . PHP_EOL);
		}

		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		
		
		$conf = Zend_Registry::get('system_config');
		$conf = $conf['sms'];
		
		$phone_from = $conf['number'];
		$SMS_USERKEY = $conf['userkey'];
		$SMS_PASSWORD = $conf['password'];

		$model_sms = new Model_SMS_Outbox();
		$sms = new Cubix_SMS($SMS_USERKEY, $SMS_PASSWORD);
		$sms->setOriginator($phone_from);
		
		$sms->setBufferedNotificationURL($conf['BufferedNotificationURL']);
		$sms->setDeliveryNotificationURL($conf['DeliveryNotificationURL']);
		$sms->setNonDeliveryNotificationURL($conf['NonDeliveryNotificationURL']);
		
		$files = scandir(self::$jobsPath);

		foreach($files as $file) {
			if ($file != "." && $file != "..") {
				$email_body = '';
				$job = json_decode(file_get_contents(self::$jobsPath . $file));
				$sended = array();
				try {
					foreach($job->phone_numbers AS $i => $phone_number) {
						echo($phone_number . "\n");

						$data = array(
							'phone_number' => $phone_number,
							'phone_from' => $phone_from,
							'text' => $job->body,
							'application_id' => Cubix_Application::getId(),
							'sender_sales_person' => $job->admin_id,
							'is_spam'	=> 1
						);
						
						$sms_id = $model_sms->save($data);

						ob_flush();
						$sms->setRecipient($phone_number, $sms_id);
						$sms->setContent($job->body);
						$sms->sendSMS();
						$sended[] = $phone_number;
					}
					echo('Job done' . "\n");
					$email_body = $job->body;
				} catch(Exception $ex) {
					echo('ERROR' . "\n");
					ob_flush();
					$email_body = 'Error occurred during sms job' . "\n";
					$email_body .= $ex->getMessage() . "\n";
					$email_body .= 'Sended numbers below' . "\n";
					$email_body .= implode("\n", $sended);
				}
				if ( $job->notify_email ) {
					Cubix_Email::send($job->notify_email, 'Sms job report', $email_body);
				}
				
				unlink(self::$jobsPath . $file);
				
				break; //Only per one job
			}
		}
		die;
	}
	
	/*public function importAction()
	{
		$group_id = 9;
		
		if ( ! $group_id ) die('Group ID required');
		
		$phone_numbers = file_get_contents(APPLICATION_PATH . '/phone_numbers.txt');
		$phone_numbers = explode("\n", $phone_numbers);
		$phone_numbers = array_unique($phone_numbers);
		
		$excluded_numbers = 0;
		foreach($phone_numbers as $number) {
			$number = preg_replace("/[^0-9]+/", "", $number);

			if ( ! strlen($number) ) continue;
			
			//$number = '0044' . $number;
			$number = '001' . $number;

			//$number = '0033' . substr_replace($number, '', 0, 1);
			
			echo $number . ' - ';
			$result = $this->model->checkIfExistsInProject($number);
			if ( $result['exists'] ) {
				$excluded_numbers++;
				echo $result['reason'];
			} else {
				$this->model->save(new Model_SMS_ProspectiveClientsItem(array(
					'number'	=> $number,
					'group_id'	=> $group_id
				)));
				echo 'imported.';
			}
			echo "\n";
			ob_flush();
		}
		
		echo('Excluded ' . $excluded_numbers . ' from ' . count($phone_numbers) . "\n" );
		ob_flush();
		
		die('Done');
	}*/
	public function importAction()
	{
		if ( $this->_request->isPost() ) {

			$validator = new Cubix_Validator();

			if ( ! $this->_request->group_id )	{
				$validator->setError('group_id', 'Select group');
			}

			$country_code = $this->_request->getParam('country_code', '');
			$country_code = trim($country_code);
			$country_code = preg_replace("/[^0-9]+/", "", $country_code);

			$phone_numbers = $this->_request->phone_numbers;			
			if ( strlen($phone_numbers) ) {
				$phone_numbers = explode("\n", $phone_numbers);
				$phone_numbers = array_unique($phone_numbers);
			} else {
				$phone_numbers = array();
			}

			if ( ! count($phone_numbers) ) {
				$validator->setError('phone_numbers', 'Required');
			} elseif ( count($phone_numbers) > 3000 ) {
				$validator->setError('phone_numbers', 'No more than 3000.');
			}
				
			if ( ! $validator->isValid() ) {
			} else {
				$excluded_numbers = 0;
				foreach($phone_numbers as $number) {
					$number = preg_replace("/[^0-9]+/", "", $number);

					if ( ! strlen($number) ) continue;
					
					$number = $country_code . $number;

					$result = $this->model->checkIfExistsInProject($number);
					if ( $result['exists'] ) {
						$excluded_numbers++;
					} else {
						$this->model->save(new Model_SMS_ProspectiveClientsItem(array(
							'number'	=> $number,
							'group_id'	=> $this->_request->group_id
						)));
					}
				}
				

				//echo('Excluded ' . $excluded_numbers . ' from ' . count($phone_numbers) . "\n" );
				
			}
			die(json_encode($validator->getStatus()));
		} else {
			$this->view->groups = Model_SMS_ProspectiveClients::getForSelect();
		}
	}
	
	//Removing escorts which registered after their contact is imported to db
	//Maybe our campaign was successful :))
	public function removeExistingAction()
	{
		if ( ! defined('IS_CLI') || ! IS_CLI ) {
			die('Please run from command line');
		}

		$cli = new Cubix_Cli();
		$pid_file = '/var/run/remove_existing_' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		if ( $cli->isRunning() ) {
			die('Cron is already running, exitting...' . PHP_EOL);
		}

		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		
		$existing = $this->model->removeExisting();
		
		
		echo 'Removed ' . count($existing) . ' numbers';
		echo("\n");
		
		//only for 6anuncio yet
		if ( Cubix_Application::getId() == APP_6B ) {
			$not_delivered = $this->model->blacklistNotDelivered();
			echo 'Blacklisted ' . count($not_delivered) . ' numbers';
		}
		
		
		die;
	}

	 public function commentsAction(){
        $this->view->layout()->disableLayout();
        $req = $this->_request;
        $model = new Model_EscortsV2();
        
        $client_id = isset($req->client_id) ? $req->client_id : NULL ;
        $page = isset($req->page) ? $req->page : 1;
      
        $per_page = 5;

		//$m_city = new Cubix_Geography_Cities();
        //$this->view->escort = $model->get($eid);
		//$home_city = $m_city->get($this->view->escort->city_id);
       // $this->view->city_title = $home_city->title_en;
        $this->view->client_id = $client_id;
        $this->view->page = $page;
        $this->view->per_page = $per_page;
        $this->view->definitions= Zend_Registry::get('defines');

        // if( $is_agency){
        //     $db = Zend_Registry::get('db');
        //     $agencyModel = new Model_Agencies();
        //     $agencyModel->UpdateAgencyEscorts($id,array('date_last_modified' => new Zend_Db_Expr('NOW()')));

        //     $db->update('agencies', array('last_modified' =>  new Zend_Db_Expr('NOW()')), 'id = '.$id);
        // }
      
        $this->view->comments_history = Model_SMS_ProspectiveClients::getCommentsHistoryByClientId($client_id, $page, $per_page, $count);
       	$this->view->count = $count;
        $back_user = Zend_Auth::getInstance()->getIdentity();
        $this->view->back_user_id = $back_user->id;
        $this->view->user_type = $this->user->type;
    }

    public function ajaxAddResponseCommentAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$comment_data = $this->_request->getPost();
		$comment_data['date'] = date('Y-m-d H:i:s');
		
		Model_SMS_ProspectiveClients::insertProspectiveClientsResponseComment($comment_data);
		
		//if($comment_data['response'] == 'confirmed'){
		//	$m_e = new Model_EscortsV2();
		//	$m_e->updateHandVerificationDate( $comment_data['escort_id'] );
		//}
	}

	public function ajaxResponseCommentsHistoryAction()
	{
		$this->view->layout()->disableLayout();
		$is_agency = false;
		$req = $this->_request;
		$client_id = isset($req->client_id) ? $req->client_id : NULL ;
		$page = isset($req->page) ? $req->page : 1;
		

		$per_page = 5;
		$this->view->page = $page;
		$this->view->per_page = $per_page;
		$this->view->definitions= Zend_Registry::get('defines');
		
		$this->view->comments_history = Model_SMS_ProspectiveClients::getCommentsHistoryByClientId($client_id, $page, $per_page, $count);
		$this->view->count = $count;
		$this->view->client_id = $client_id;
		
		$this->view->user_type = $this->user->type;
	}

    public function assignToAction()
    {
        $this->view->layout()->disableLayout();
        $req = $this->_request;

        $user_id = $this->view->user_id = $req->user_id;
        $ids = $this->view->ids = $this->_request->ids;
        $multiple = $this->view->multiple = isset($req->multiple) && $req->multiple == 1;

        if ( $req->isPost() ) {
            $sales_id = $req->sales_id;
            $validator = new Cubix_Validator();
            if ($multiple){
                $prosp_ids= explode(',', $ids);

                foreach ($prosp_ids as $prosp_id){

                    $this->model->updateSales($sales_id,$prosp_id);
                }
            }
            die(json_encode($validator->getStatus()));
        }
        else {
            if ($multiple){
                $this->view->ids =implode(',',$req->id);
            }
            $bu_model = new Model_BOUsers();
            $this->view->sales_persons = $bu_model->getAllSales();
        }
    }

    public function syncAction(){
    	
    	$this->model->syncClients();
    	var_dump(array('message' => 'Perspective clients synched'));die;
    }
}

