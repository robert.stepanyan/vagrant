<?php

class Sms_ReceiversController extends Zend_Controller_Action {
	
	public function init()
	{
		if (Cubix_Application::getId() != APP_A6)
			die;

		$this->_model = new Model_SMS_Receivers();

		$bu_user = Zend_Auth::getInstance()->getIdentity();

		if (!in_array($bu_user->type, array('superadmin', 'admin')))
			die('You have not permission');
	}
	
	public function indexAction()
	{
		
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'application_id' => $this->_request->application_id
		);

		$data = $this->_model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);

		foreach ($data as $i => $item)
		{
			if ($item->gotd)
				$data[$i]->gotd = '<img src="/img/famfamfam/accept.png" />';
			else
				$data[$i]->gotd = '<img src="/img/famfamfam/accept_passive.png" />';

			if ($item->daypass)
				$data[$i]->daypass = '<img src="/img/famfamfam/accept.png" />';
			else
				$data[$i]->daypass = '<img src="/img/famfamfam/accept_passive.png" />';

			if ($item->other)
				$data[$i]->other = '<img src="/img/famfamfam/accept.png" />';
			else
				$data[$i]->other = '<img src="/img/famfamfam/accept_passive.png" />';
		}
		
		die(json_encode(array('data' => $data, 'count' => $count)));
	}
	
	public function createAction()
	{
		$this->view->layout()->disableLayout();
		
		if ( $this->_request->isPost() )
		{
			$bu_user = Zend_Auth::getInstance()->getIdentity();
			
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'number' => '',
				'gotd' => 'int',
				'daypass' => 'int',
				'other' => 'int'
			));
			$data = $data->getData();
			$data['backend_user_id'] = $bu_user->id;
			
			$validator = new Cubix_Validator();
			
			if ( ! strlen($data['number']) )
			{
				$validator->setError('number', 'Required');
			}
			elseif (!ctype_digit($data['number']) || substr($data['number'], 0, 2) != '00')
			{
				$validator->setError('number', 'Number format is invalid');
			}

			if ( $validator->isValid() ) {
				$this->_model->save($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function editAction()
	{
		$this->view->layout()->disableLayout();
		
		$id = intval($this->_request->id);
		
		$this->view->item = $template = $this->_model->get($id);
		
		if ( $this->_request->isPost() )
		{
			$bu_user = Zend_Auth::getInstance()->getIdentity();

			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'id' => 'int',
				'number' => '',
				'gotd' => 'int',
				'daypass' => 'int',
				'other' => 'int'
			));
			$data = $data->getData();
			$data['backend_user_id'] = $bu_user->id;
			
			$validator = new Cubix_Validator();
			
			if ( ! strlen($data['number']) )
			{
				$validator->setError('number', 'Required');
			}
			elseif (!ctype_digit($data['number']) || substr($data['number'], 0, 2) != '00')
			{
				$validator->setError('number', 'Number format is invalid');
			}
			
			if ( $validator->isValid() ) {
				$this->_model->save($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function deleteAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->_model->remove($id);

		die;
	}
}
