<?php

class System_StaticContentController extends Zend_Controller_Action
{
	/**
	 * @var Model_System_Dictionary
	 */
	protected $model;
	
	public function init()
	{
		$this->model = new Model_System_StaticContent();
	}
	
	public function indexAction() 
	{
		
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array('title' => $req->id, 'application_id' => $req->application_id );
		
		$data = $this->model->getAll(
			$req->page,
			$req->per_page, 
			$filter,
			$req->sort_field,
			$req->sort_dir,
			$count
		);
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
	
	public function createAction()
	{
		if ( $this->_request->isPost() )
		{					
			$slug = $this->_request->getParam('slug');
			$title = Cubix_I18n::getValues($this->_request->getParams(), 'title', '');
			$body = Cubix_I18n::getValues($this->_request->getParams(), 'body', '');
			
			$validator = new Cubix_Validator();
			
						
			if ( ! strlen($slug) ) {
				$validator->setError('slug', 'This field is required');
			}
			else {
				$item = $this->model->getById($slug, $this->_getParam('application_id'));

				if ( $item && $item->getId() == $entry_id ) {
					$validator->setError('slug', 'Choose another slug, this one already exists');
				}
			}
			
			if ( $validator->isValid() ) {
				
				$data = array();				
				$langs = Cubix_I18n::getLangs(true);
				
				foreach ($langs as $lang)
				{
					$data[$lang]['slug'] = $slug; 
					$data[$lang]['application_id'] = $this->_request->getParam('application_id');
					
					
					$data[$lang]['title'] = $title['title_' . $lang];
					
					$data[$lang]['body'] = $body['body_' . $lang];
					$data[$lang]['lang_id'] = $lang;
				}
				
				$this->model->insert(new Model_System_StaticContentItem($data));
			}
			
			die(json_encode($validator->getStatus()));
		}
	}

	public function editAction()
	{
		$template = $this->model->get($this->_getParam('slug'), $this->_getParam('application_id'));
		$this->view->slug = $this->_getParam('slug');
		
		$langs = Cubix_I18n::getLangsByAppId($this->_getParam('application_id'));
		
		$data = array();
		foreach ($langs as $lang)
		{
			foreach ($template as $t)
			{
				if ($lang->lang_id == $t['lang_id'])
				{
					$data[$lang->lang_id]['slug'] = $t['slug'];
					$data[$lang->lang_id]['application_id'] = $t['application_id'];
					
					
					$data[$lang->lang_id]['title'] = $t['title'];
					
					$data[$lang->lang_id]['body'] = $t['body'];
					$data[$lang->lang_id]['lang_id'] = $t['lang_id'];
				}
			}
		}
		
		
		$this->view->page = $data;
		
		if ( $this->_request->isPost() )
		{			
			$slug = $this->_request->getParam('slug');
			
			$title = Cubix_I18n::getValues($this->_request->getParams(), 'title', '');
			
			$body = Cubix_I18n::getValues($this->_request->getParams(), 'body', '');
			
			$data = array();
			foreach ($langs as $lang)
			{
				$data[$lang->lang_id]['slug'] = $this->view->slug; 
				$data[$lang->lang_id]['application_id'] = $this->_request->getParam('application_id');
				
				
				$data[$lang->lang_id]['title'] = $title['title_' . $lang->lang_id];
				
				$data[$lang->lang_id]['body'] = $body['body_' . $lang->lang_id];
				$data[$lang->lang_id]['lang_id'] = $lang->lang_id;
			}
			
			$this->model->update(new Model_System_StaticContentItem($data));
			
			$validator = new Cubix_Validator();
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function removeAction()
	{
		$slug = $this->_getParam('slug');
		
		$this->model->remove($slug, $this->_getParam('application_id'));
		
		die;
	}

    public function syncAction(){
        $app_id = Cubix_Application::getId();
        $http = 'http';
        if($app_id == APP_ED || $app_id == APP_A6){
            $http .= 's';
        }
        $app = Cubix_Application::getById($app_id);
        $url = $http.'://www.' . $app->host;
        $sync_url = $url . '/api/dictionary/static-content';

        $client = new Zend_Http_Client($sync_url, array(
                'maxredirects' => 0,
                'timeout'      => 60)
        );
        $client->setAuth('babazeo', 'GnaMer1a', Zend_Http_Client::AUTH_BASIC);

        $response = $client->request(Zend_Http_Client::GET);

        if ( $response->isError() ) {
            die(json_encode(array('status' => 'error', 'message' => $response->getMessage())));
        }

        header('Cubix-message: ' . $url . ' has been successfully syncronized');
        die(json_encode(array('status' => 'success','message'=>'Static Content was successfully synchronized')));
    }
}
