<?php

class System_GlossaryController extends Zend_Controller_Action
{
	protected $model;
	
	public function init()
	{
		$this->model = new Model_System_Glossary();
	}
	
	public function indexAction() 
	{
		
	}
	
	public function dataAction()
	{
		$req = $this->_request;

		$filter = array('abbr' => $req->abbr, 'value_en' => $req->value_en);
		
		$data = $this->model->getAll(
			$req->page,
			$req->per_page, 
			$filter, 
			$req->sort_field, 
			$req->sort_dir, 
			$count
		);

		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
	
	public function createAction()
	{
		if ( $this->_request->isPost() )
		{
			$data = new Cubix_Form_Data($this->_request);
			$fields = array(
				'abbr' => ''
			);
			$data->setFields($fields);
			$data = $data->getData();
			
			$values = Cubix_I18n::getValues($this->_request->getParams(), 'value', '');
			
			$validator = new Cubix_Validator();
			
			if ( ! strlen($data['abbr']) ) {
				$validator->setError('abbr', 'This field is required');
			}
			else {
				$item = $this->model->getByAbbr($data['abbr']);

				if ( $item ) {
					$validator->setError('abbr', 'This one already exists');
				}
			}

			if ( $validator->isValid() ) {
				$this->model->save(array_merge($data, $values));
			}
			
			die(json_encode($validator->getStatus()));
		}
	}

	public function editAction()
	{
		$this->view->entry = $entry = $this->model->getById($this->_getParam('id'));
		
		if ( $this->_request->isPost() )
		{
			$data = Cubix_I18n::getValues($this->_request->getParams(), 'value', '');

			$validator = new Cubix_Validator();

			$exists = $this->model->checkAbbr($this->_getParam('abbr'), $this->_getParam('id'));

			if ( $exists ) {
				$validator->setError('abbr', 'This one already exists');
			}

			if ( $validator->isValid() ) {
				$this->model->save(array_merge(
					array('id' => $this->_getParam('id'), 'abbr' => $this->_getParam('abbr')),
					$data
				));
			}

			die(json_encode($validator->getStatus()));
		}
	}

	public function removeAction()
	{
		$id = $this->_getParam('id');
		
		$this->model->remove($id);
		
		die;
	}

	public function syncAction()
	{
		$app_id = $this->_getParam('application_id');

		$app = Cubix_Application::getById($app_id);
		$url = 'http://www.' . $app->host;
		$sync_url = $url . '/api/dictionary/glossary';

		$client = new Zend_Http_Client($sync_url, array(
			'maxredirects' => 0,
			'timeout'      => 30)
		);
		$client->setAuth('babazeo', 'GnaMer1a', Zend_Http_Client::AUTH_BASIC);

		$response = $client->request(Zend_Http_Client::GET);

		//$result = @file_get_contents($sync_url);

		if ( /*$result === false*/$response->isError() ) {
			die(json_encode(array('status' => 'error')));
		}

		header('Cubix-message: ' . $url . ' has been successfully syncronized');
		die(json_encode(array('status' => 'success', 'result' => $url, 'response' => /*$result*/$response->getBody())));
	}
}
