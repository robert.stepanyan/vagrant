<?php

class System_IxsBannersController extends Zend_Controller_Action
{
    /**
     * @var Model_ixsBanners
     */
    protected $model;

    /**
     * @var Cubix_Cli
     */
    private $cli;

    public function init()
    {
        $this->model = new Model_ixsBanners();
    }

    public function indexAction()
    {

    }

    public function dataAction()
    {
        $req = $this->_request;
        $filter = array(
            'id' => $req->id,
            'link' => $req->link,
            'zone_id' => $req->zone_id,
            'type' => $req->type,
            'frozen_country_id' => $req->frozen_country_id,
            'cmp_id' => $req->cmp_id,
            'banner_id' => $req->banner_id,
        );

        $count = 0;

        $current_app_id = intval($this->_getParam('application_id'));

        if (Cubix_Application::getId() == APP_EG_CO_UK) {
            $data = $this->model->getAllBannersForEGUK(
                $req->page,
                $req->per_page,
                $filter,
                $req->sort_field,
                $req->sort_dir,
                $count
            );
        }
        else {
            $data = $this->model->getAll(
                $req->page,
                $req->per_page,
                $filter,
                $req->sort_field,
                $req->sort_dir,
                $count
            );
        }



        echo json_encode(array('data' => $data, 'count' => $count));
        die;
    }

    public function editAction()
{

    $id = intval($this->_getParam('id'));
    $this->view->banner = $this->model->getById($id);
    $this->view->app_id = Cubix_Application::getId();

    /* getting regions for edit block */
    $regionModel = new Model_Geography_Regions();
    $regions = $regionModel->getAllRegions();

    /*get cities in regions make collection*/
    $regionsAndCities = [];
    $citiesModel = new Model_Geography_Cities();
    foreach ($regions as $region) {
        $citiesOfThisRegion = $citiesModel->getRegionCities($region->id);

        $regionsAndCities[$region->title_en.','.$region->id] = $citiesOfThisRegion;
    }

    $this->view->regionsAndCities = $regionsAndCities;


    if ($this->_request->isPost()) {
        return $this->save();
    }
}

    public function removeAction()
    {
        $id = intval($this->_getParam('id'));
        $this->view->banner = $this->model->remove($id);

    }

    public function chooseSpotAction()
    {
        $countryId = intval($this->_getParam('countryId'));
        $cityId = intval($this->_getParam('cityId'));
        $currentBannerId = intval($this->_getParam('currentBanner'));

        if(!empty($currentBannerId)) {
            $this->view->banner = $this->model->getById($currentBannerId);
        }

        $usedSpots = $this->model->fetchUsedSpots($countryId, $cityId);
        $this->view->usedSpots = [];

        foreach ($usedSpots as $id => $spot) {
            $this->view->usedSpots[$spot->frozen_position] = $spot;
        }
    }

    public function syncAction()
    {
        $ixs_id = 3;

        if (Cubix_Application::getId() == APP_EG_CO_UK) {
            $ixs_id = 10;
        }
        $this->view->layout()->disableLayout();

        $url = Model_ixsBanners::HOST . "deliver.php?websiteID=$ixs_id";

        $ixspublic = file_get_contents($url);

        preg_match_all('|zonesJsonBuffer = ({[^;]+);|is', $ixspublic, $matches);
        $ixs = json_decode($matches[1][0]);

        $cmp_ids = array();
        foreach ($ixs as $zone => $imgs) {
           foreach($imgs as $img){
                $cmp_ids[] = (int) $img->cmp_id;
           }
        }
        
        $this->model->delete($cmp_ids);

        $leftInTable = 0;
        $this->model->getAll(1, 1000, [], 'id', 'asc', $leftInTable);

        $response = ['success' => -1 * $leftInTable, 'fail' => 0];

        if (in_array(Cubix_Application::getId(), array(APP_ED, APP_EG_CO_UK))) {
            $this->cli = new Cubix_Cli();
            $this->cli->clear();
            $this->cli->colorize('yellow')->out('Creating temp. table. ')->reset();
            $this->model->createTmpTableForIXSBanners();

            $existingIxsBanners = $this->model->get();
            $this->cli->colorize('green')->out('Fill existing banners to tmp. table '. count($existingIxsBanners))->reset();
            $this->model->fillExistingBannersToTmpTable($existingIxsBanners);

            $this->cli->colorize('blue')->out('Clearing Banners from table.')->reset();
            $this->model->clearIXSBannersTable();

            $this->cli->colorize('green')->out('Inserting Banners fetched from IXS.')->reset();
            foreach ($ixs as $zone => $images) {
                $mysql_insert_result = $this->model->insertISXBanners($ixs->$zone, $zone);

                if ($mysql_insert_result) {
                    $response['success'] += count($images);
                } else {
                    $response['fail'] += count($images);
                }
            }

            $this->cli->colorize('green')->out('Updating Banners Settings.')->reset();
            $newBanners = $this->model->getNewFetchedBanners();

            foreach ($newBanners as $banner) {
                if ($banner->banner_id) {
                    $this->model->updateNewBannersSettings($banner->banner_id);
                }
            }

            $this->cli->colorize('red')->out('Deleting Temp. Table.')->reset();
            $this->model->swapTempTable();
            $this->cli->colorize('red')->out('SUCCESS')->reset();
        }
        else {
            foreach ($ixs as $zone => $images) {
                $mysql_insert_result = $this->model->insert($ixs->$zone, $zone);

                if ($mysql_insert_result) {
                    $response['success'] += count($images);
                } else {
                    $response['fail'] += count($images);
                }
            }
        }

        die(json_encode($response));
    }

    public function getUsedSpotsAction()
    {
        if ($this->_request->isPost()) {
            $countryId = $this->_getParam('countryId');
            $cityId = $this->_getParam('cityId');

            $spots = $this->model->fetchUsedSpots($countryId, $cityId);

            echo json_encode([
                'spots' => $spots
            ]);

            exit;
        }

        return false;
    }

    public function save()
    {
        $relation = $this->_getParam('relation');
        $priority = $this->_getParam('priority');
        $position = $this->_getParam('position');
        $type = $this->_getParam('type');
        $id = $this->_getParam('id');

        if (!empty($position) && $type == 'fixed') {
            $data = [
                'frozen_country_id' => $this->_getParam('countryId'),
                'frozen_city_id' => $this->_getParam('city'),
                'frozen_position' => $position,
                'type' => 'fixed'
            ];
        }else{
            $data = [
                'frozen_country_id' => 0,
                'frozen_city_id' => 0,
                'frozen_position' => 0,
                'type' => 'rotating'
            ];
        }

        if ($relation == 'dofollow') {
            $data['is_follow'] = '1';
         }
        else {
            $data['is_follow'] = '0';
        }

        $data['low_priority'] = $priority;

        $response = ['status' => 'fail', 'use_callback' => true];
        if($this->model->save($id, $data)) {
            $response['status'] = 'success';
        }

        echo json_encode($response);
        die;
    }

    public function updateFrozenPositionAction()
    {
        $response = ['status' => 'fail'];

        if ($this->_request->isPost()) {
            $countryId = $this->_getParam('countryId');
            $cityId = $this->_getParam('cityId');
            $bannerId = $this->_getParam('bannerId');
            $position = $this->_getParam('position');

            $regionId = null;
            if (!empty($this->_getParam('regionId'))) {
                $regionId = $this->_getParam('regionId');
            }

            /* EDIR-1018, creating constant 9999, for all countries except US, if the banner is 9999 it
                meens that position is not priority
            */
            if ($countryId == 9999 && in_array(Cubix_Application::getId(), array(APP_ED))) {
                $position = 0;
            }

            $isUpdated = $this->model->freezeBannerAt($bannerId, $position, $countryId, $cityId, $regionId);

            if ($isUpdated) {
                $response['status'] = 'success';
            }

            echo(json_encode([
                'countryId' => $countryId,
                'cityId' => $cityId,
                'bannerId' => $bannerId,
                'position' => $position,
                'status' => $response['status']
            ]));

            die;
        }

        return false;
    }
}
