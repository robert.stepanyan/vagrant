<?php

class System_FaqController extends Zend_Controller_Action
{
	protected $model;
	
	public function init()
	{
		$this->model = new Model_System_Faq();
	}
	
	public function indexAction() 
	{
		
	}
	
	public function dataAction()
	{		
		$req = $this->_request;
		
		$filter = array('title' => $req->title, 'question' => $req->question, 'answer' => $req->answer, 'type' => $req->type, 'application_id' => $req->application_id );
		
		$data = $this->model->getAll(
			$req->page,
			$req->per_page, 
			$filter,
			$req->sort_field,
			$req->sort_dir,
			$count
		);
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
	
	public function createAction()
	{
		if ( $this->_request->isPost() )
		{					
			$question = Cubix_I18n::getValues($this->_request->getParams(), 'question', '');
			$answer = Cubix_I18n::getValues($this->_request->getParams(), 'answer', '');
			$types = $this->_request->c_types;
			$title = trim($this->_request->title);
			
			$validator = new Cubix_Validator();

			if (!$types)
				$validator->setError('types', 'Select at least one type.');

			if (!$title)
				$validator->setError('title', 'Required');
						
			if ( $validator->isValid() ) {
				
				$data = array();				
				$langs = Cubix_I18n::getLangs(true);

				$data['application_id'] = $this->_request->getParam('application_id');
				$data['title'] = $title;

				foreach ($langs as $lang)
				{
					$data['question_' . $lang] = $question['question_' . $lang];
					$data['answer_' . $lang] = $answer['answer_' . $lang];
				}

				$data['type'] = implode(',', $types);
								
				$this->model->insert(new Model_System_FaqItem($data));
			}
			
			die(json_encode($validator->getStatus()));
		}
	}

	public function editAction()
	{
		$this->view->item = $item = $this->model->get($this->_getParam('id'), $this->_getParam('application_id'));
		$langs = Cubix_I18n::getLangsByAppId($this->_getParam('application_id'));
		
		if ( $this->_request->isPost() )
		{			
			$id = $this->_request->getParam('id');
			$question = Cubix_I18n::getValues($this->_request->getParams(), 'question', '');
			$answer = Cubix_I18n::getValues($this->_request->getParams(), 'answer', '');
			$types = $this->_request->c_types;
			$title = trim($this->_request->title);

			$validator = new Cubix_Validator();

			if (!$types)
				$validator->setError('types', 'Select at least one type.');

			if (!$title)
				$validator->setError('title', 'Required');

			if ( $validator->isValid() ) {
				$data = array();

				$data['id'] = $id;
				$data['application_id'] = $this->_request->getParam('application_id');
				$data['title'] = $title;

				foreach ($langs as $lang)
				{
					$data['question_' . $lang->lang_id] = $question['question_' . $lang->lang_id];
					$data['answer_' . $lang->lang_id] = $answer['answer_' . $lang->lang_id];
				}

				$data['type'] = implode(',', $types);

				$this->model->update(new Model_System_FaqItem($data));
			}
			
			die(json_encode($validator->getStatus()));
		}
	}

	public function toggleActiveAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->model->toggle($id);
	}
	
	public function removeAction()
	{
		$id = $this->_getParam('id');
		
		$this->model->remove($id, $this->_getParam('application_id'));
		
		die;
	}
}
