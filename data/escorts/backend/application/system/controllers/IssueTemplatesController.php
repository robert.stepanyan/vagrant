<?php

class System_IssueTemplatesController extends Zend_Controller_Action {
	
	public function init()
	{
		$this->_model = new Model_System_IssueTemplates();
	}
	
	public function indexAction()
	{
		
	}
	
	public function dataAction()
	{		
		$filter = array(
			
		);
		
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		if (!in_array($bu_user->type, array('superadmin', 'admin')))
			$filter['backend_user_id'] = $bu_user->id;
		
		$data = $this->_model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		die(json_encode(array('data' => $data, 'count' => $count)));
	}
	
	public function createAction()
	{
		$this->view->layout()->disableLayout();
		
		if ( $this->_request->isPost() ) {
			$bu_user = Zend_Auth::getInstance()->getIdentity();
			
			$data = new Cubix_Form_Data($this->_request);

			if (!in_array(Cubix_Application::getId(),array(APP_ED)))
            {
                $data->setFields(array(
                    'title' => '',
                    'comment' => '',
                    'signature' => ''
                ));
                $data = $data->getData();
                $data['backend_user_id'] = $bu_user->id;

                $validator = new Cubix_Validator();

                if ( ! strlen($data['title']) ) {
                    $validator->setError('title', 'Required');
                }

                if ( ! strlen($data['comment']) && ! strlen($data['signature']) ) {
                    $validator->setError('comment', 'Comment or Signature Required');
                }

                if ( $validator->isValid() ) {
                    $this->_model->save($data);
                }
            }else{
                $langs = Cubix_I18n::getLangs(true);
                $fieldsArray = array();
                foreach ($langs as $lang)
                {
                    $fieldsArray['title_' . $lang] =  '';
                    $fieldsArray['comment_' . $lang] =  '';
                    $fieldsArray['signature_' . $lang] =  '';
                }

                $data->setFields($fieldsArray);
                $data = $data->getData();
                $data['backend_user_id'] = $bu_user->id;

                $has_any_title_array = array();
                $has_comment_or_signature_array = array();
                foreach ($langs as $lang)
                {
                    if ( strlen($data['title_' . $lang]) ) {
                        $has_any_title_array[] = true;
                    }else{
                        $has_any_title_array[] = false;
                    }

                    if ( strlen($data['comment_' . $lang]) && strlen($data['signature_' . $lang]) ) {
                        $has_comment_or_signature_array[] = true;
                    }else{
                        $has_comment_or_signature_array[] = false;
                    }
                }

                $validator = new Cubix_Validator();

                if (!in_array(true,$has_any_title_array))
                {
                    $validator->setError('title', 'Required');
                }

                if (!in_array(true,$has_comment_or_signature_array))
                {
                    $validator->setError('comment', 'Comment or Signature Required');
                }

                if ( $validator->isValid() ) {
                    $this->_model->saveV2($data);
                }
            }

			die(json_encode($validator->getStatus()));
		}
	}
	
	public function editAction()
	{
		$this->view->layout()->disableLayout();
		
		$id = intval($this->_request->id);
		
		$this->view->item = $template = $this->_model->get($id);
		
		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			if (!in_array(Cubix_Application::getId(),array(APP_ED)))
            {
                $data->setFields(array(
                    'id' => 'int',
                    'title' => '',
                    'comment' => '',
                    'signature' => ''
                ));
                $data = $data->getData();

                $validator = new Cubix_Validator();

                if ( ! strlen($data['title']) ) {
                    $validator->setError('title', 'Required');
                }

                if ( ! strlen($data['comment']) && ! strlen($data['signature']) ) {
                    $validator->setError('comment', 'Comment or Signature Required');
                }

                if ( $validator->isValid() ) {
                    $this->_model->save($data);
                }
            }else{
                $langs = Cubix_I18n::getLangs(true);
                $fieldsArray = array('id' => 'int');
                foreach ($langs as $lang)
                {
                    $fieldsArray['title_' . $lang] =  '';
                    $fieldsArray['comment_' . $lang] =  '';
                    $fieldsArray['signature_' . $lang] =  '';
                }

                $data->setFields($fieldsArray);
                $data = $data->getData();

                $has_any_title_array = array();
                $has_comment_or_signature_array = array();
                foreach ($langs as $lang)
                {
                    if ( strlen($data['title_' . $lang]) ) {
                        $has_any_title_array[] = true;
                    }else{
                        $has_any_title_array[] = false;
                    }

                    if ( strlen($data['comment_' . $lang]) && strlen($data['signature_' . $lang]) ) {
                        $has_comment_or_signature_array[] = true;
                    }else{
                        $has_comment_or_signature_array[] = false;
                    }
                }

                $validator = new Cubix_Validator();

                if (!in_array(true,$has_any_title_array))
                {
                    $validator->setError('title', 'Required');
                }

                if (!in_array(true,$has_comment_or_signature_array))
                {
                    $validator->setError('comment', 'Comment or Signature Required');
                }

                if ( $validator->isValid() ) {
                    $this->_model->saveV2($data);
                }
            }

			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function deleteAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->_model->remove($id);

		die;
	}
	
	public function ajaxGetAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		
		$temp = $this->_model->get($id);
		$temp->comment = nl2br($temp->comment);
		die(json_encode($temp));
	}
}
