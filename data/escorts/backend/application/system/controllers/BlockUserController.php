<?php

class System_BlockUserController extends Zend_Controller_Action
{
	protected $model;

	public function init()
	{
		if (!in_array(Cubix_Application::getId() , array(APP_EF, APP_A6) )) die;
		
		$this->model = new Model_BlockUser();
	}

	public function indexAction()
	{
		
	}

	public function dataAction()
	{
		$req = $this->_request;

		$filter = array(
			'u.username' => $req->username,
			'e.id' => $req->escort_id,
			'a.id' => $req->agency_id,
			'u.id' => $req->member_id,
		);
        

		$data = $this->model->getAll(
			$req->page,
			$req->per_page,
			$filter,
			$req->sort_field,
			$req->sort_dir,
			$count
		);

		foreach ($data as $value) {
			if($value->user_type == 'agency'){
				$value->member_id = $value->agency_id;
			}
			if($value->user_type == 'escort'){
				$value->member_id = $value->escort_id;
			}
			if($value->user_type == 'escort'){
				$value->member_id = $value->user_id;
			}
		}


		echo json_encode( array(
			'data' => $data,
			'count' => $count
		) );

		exit;
	}

	public function removeAction()
	{
		$id = intval($this->_getParam('id'));

		$this->model->remove($id);

		die;
	}

    
}
