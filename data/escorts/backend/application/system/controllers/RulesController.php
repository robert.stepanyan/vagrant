<?php

class System_RulesController extends Zend_Controller_Action
{
	protected $model;
	
	public function init()
	{
		if (Cubix_Application::getId() != APP_EF)
			die('permission denied');
		
		$this->model = new Model_System_Rules();
	}
	
	public function indexAction() 
	{
		
	}
	
	public function dataAction()
	{		
		$req = $this->_request;
		
		$filter = array(
			'title' => $req->title, 
			'question_en' => $req->question_en, 
			'answer_en' => $req->answer_en, 
			'question_it' => $req->question_it, 
			'answer_it' => $req->answer_it, 
			'application_id' => $req->application_id 
		);
		
		$data = $this->model->getAll(
			$req->page,
			$req->per_page, 
			$filter,
			$req->sort_field,
			$req->sort_dir,
			$count
		);
		
		foreach ($data as $k => $item)
		{
			if (strlen($item->answer_en) > 150)
			{
				$data[$k]->answer_en = mb_substr($item->answer_en, 0, 150) . ' ...';
			}
			
			if (strlen($item->answer_it) > 150)
			{
				$data[$k]->answer_it = mb_substr($item->answer_it, 0, 150) . ' ...';
			}
		}
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
	
	public function createAction()
	{
		if ( $this->_request->isPost() )
		{					
			$question = Cubix_I18n::getValues($this->_request->getParams(), 'question', '');
			$answer = Cubix_I18n::getValues($this->_request->getParams(), 'answer', '');
			$title = trim($this->_request->title);
			
			$validator = new Cubix_Validator();

			if (!$title)
				$validator->setError('title', 'Required');
						
			if ( $validator->isValid() ) {
				
				$data = array();				
				$langs = Cubix_I18n::getLangs(true);

				$data['application_id'] = $this->_request->getParam('application_id');
				$data['title'] = $title;

				foreach ($langs as $lang)
				{
					$data['question_' . $lang] = $question['question_' . $lang];
					$data['answer_' . $lang] = $answer['answer_' . $lang];
				}
								
				$this->model->insert($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}

	public function editAction()
	{
		$this->view->item = $item = $this->model->get($this->_getParam('id'));
		$langs = Cubix_I18n::getLangsByAppId($this->_getParam('application_id'));
		
		if ( $this->_request->isPost() )
		{			
			$id = $this->_request->getParam('id');
			$question = Cubix_I18n::getValues($this->_request->getParams(), 'question', '');
			$answer = Cubix_I18n::getValues($this->_request->getParams(), 'answer', '');
			$title = trim($this->_request->title);

			$validator = new Cubix_Validator();

			if (!$title)
				$validator->setError('title', 'Required');

			if ( $validator->isValid() ) {
				$data = array();

				$data['id'] = $id;
				$data['application_id'] = $this->_request->getParam('application_id');
				$data['title'] = $title;

				foreach ($langs as $lang)
				{
					$data['question_' . $lang->lang_id] = $question['question_' . $lang->lang_id];
					$data['answer_' . $lang->lang_id] = $answer['answer_' . $lang->lang_id];
				}

				$this->model->update($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function removeAction()
	{
		$id = $this->_getParam('id');
		
		$this->model->remove($id);
		
		die;
	}
}
