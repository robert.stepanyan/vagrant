<?php

class System_BlacklistedIpsController extends Zend_Controller_Action
{
	protected $model;

	public function init()
	{
		$this->model = new Model_BlacklistedIps();
	}

	public function indexAction()
	{
		
	}

	public function dataAction()
	{
		$req = $this->_request;

		$filter = array('name' => $req->name );
        
		$data = $this->model->getAll(
			$req->page,
			$req->per_page,
			$filter,
			$req->sort_field,
			$req->sort_dir,
			$count
		);
//		$this->view->data = array(
//			'data' => $data,
//			'count' => $count
//		);


		echo json_encode( array(
			'data' => $data,
			'count' => $count
		) );

		exit;
	}

	public function deleteAction()
	{
       
        if($this->_request->isPost()){
            $ids = $this->_request->getParam('id');
            if(count($ids) > 0 && $ids){
                foreach( $ids as $id ) {
                    $this->model->delete($id);
                }
            }
        }
        exit;
	}

    public function createAction(){
        $this->view->layout()->disableLayout();

        if($this->_request->isPost()){
            $data['ip'] = $this->_request->getParam('ip');
			$data['user_agent'] = $this->_request->getParam('user_agent');
			$data['block'] = $this->_request->getParam('block');
			$validator = new Cubix_Validator();
			if(Cubix_Application::getId() == APP_ED){

				$data['comment'] = $this->_request->getParam('comment');

				if ( ! strlen($data['comment']) ) {
					$validator->setError('comment', 'Required');
				}
			}
			if ( $validator->isValid() ) {
            	$this->model->save($data);
			}

			die(json_encode($validator->getStatus()));
			exit;
//            $this->_redirect('blacklisted-words');
        }        
    }

    public function editAction(){
        $this->view->layout()->disableLayout();

        $id = $this->_request->getParam('id');
        $this->view->item = $this->model->get($id);
        $validator = new Cubix_Validator();

        if($this->_request->isPost()){
            $data['ip'] = $this->_request->getParam('ip');
			$data['block'] = $this->_request->getParam('block');
			$data['user_agent'] = $this->_request->getParam('user_agent');

			$validator = new Cubix_Validator();
			if(Cubix_Application::getId() == APP_ED){

				$data['comment'] = $this->_request->getParam('comment');

				if ( ! strlen($data['comment']) ) {
					$validator->setError('comment', 'Required');
				}
			}
			if ( $validator->isValid() ) {
            	$this->model->save($data,$id);
			}

			die(json_encode($validator->getStatus()));
        }

        $this->view->id = $id;
    }


	public function removeAction()
	{
		$id = intval($this->_getParam('id'));

		$this->model->remove($id);

		die;
	}

    
}
