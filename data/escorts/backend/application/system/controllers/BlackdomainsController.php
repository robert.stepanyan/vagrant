<?php

class System_BlackdomainsController extends Zend_Controller_Action
{
	/**
	 * @var Model_System_Dictionary
	 */
	protected $model;
	
	public function init()
	{
		$this->model = new Model_System_Blackdomains();
	}
	
	public function indexAction()
	{
		
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$current_app_id = intval($this->_getParam('application_id'));
		
		$filter = array();
		if ( $current_app_id != 0 )
		{
			$filter['a.id'] = $current_app_id;
		}
		
		$data = $this->model->getAll(
			$req->page,
			$req->per_page,  
			$req->sort_field, 
			$req->sort_dir,
			$filter,
			$count
		);
		
		echo json_encode(array('data' => $data, 'count' => $count));
		die;
	}
	
	public function createAction()
	{
		$global_app_id = $this->_getParam('application_id');
		$this->view->global_app_id = $global_app_id;
		
		if ( $this->_request->isPost() ) 
		{			
			$domain = $this->_getParam('domain');
			$black_in_all = intval($this->_getParam('black_in_all'));
			$application_id = intval($this->_getParam('app_id'));
			
			$validator = new Cubix_Validator();
						
			if ( ! $application_id && !$black_in_all ) {
				$validator->setError('app_id', 'Application is required');
			}

			if ( strlen($domain) && $this->model->isExists($domain, $application_id) ) {
				$validator->setError('domain', 'Domain already exists');
			}
			
			if ( ! strlen($domain) ) {
				$validator->setError('domain', 'Domain is required');
			}			
			
			if ( $validator->isValid() ) {
				$this->model->save(new Model_System_BlackdomainsItem(array(
					'domain'			=> $domain,
					'application_id'	=> $application_id
				)));
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function editAction()
	{		
		$id = intval($this->_getParam('id'));
		$global_app_id = $this->_getParam('application_id');
		$this->view->global_app_id = $global_app_id;
		
		if ( $this->_request->isPost() ) 
		{			
			$domain = $this->_getParam('domain');
			$old_domain = $this->_getParam('old_domain');
			$black_in_all = intval($this->_getParam('black_in_all'));
			$application_id = intval($this->_getParam('app_id'));
			
			$validator = new Cubix_Validator();
						
			if ( ! $application_id && !$black_in_all ) {
				$validator->setError('app_id', 'Application is required');
			}

			if ( $domain != $old_domain )
			{
				if ( strlen($domain) && $this->model->isExists($domain, $application_id) ) {
					$validator->setError('domain', 'Domain already exists');
				}
			}
			
			if ( ! strlen($domain) ) {
				$validator->setError('domain', 'Domain is required');
			}
			
			if ( $black_in_all )
				$application_id = null;
			
			if ( $validator->isValid() ) {
				$this->model->save(new Model_System_BlackdomainsItem(array(
					'id'				=> $id,
					'domain'			=> $domain,
					'application_id'	=> $application_id
				)));
			}
			
			die(json_encode($validator->getStatus()));
		}
		else {
			$this->view->blackdomains = $this->model->get($id);
		}
	}
	
	public function removeAction()
	{
		$id = intval($this->_getParam('id'));
		
		$this->model->remove($id);
		
		die;
	}
}
