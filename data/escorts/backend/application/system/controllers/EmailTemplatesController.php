<?php

class System_EmailTemplatesController extends Zend_Controller_Action
{
	/**
	 * @var Model_System_Dictionary
	 */
	protected $model;
	
	public function init()
	{
		$this->model = new Model_System_EmailTemplates();
	}
	
	public function indexAction() 
	{
		
	}
	
	public function dataAction()
	{
		$req = $this->_request;
        $config = Zend_Registry::get('images_config');
        $tempDir = !empty($config['temp_dir_path']) ? $config['temp_dir_path'] : sys_get_temp_dir();
        $pattern = $tempDir.DIRECTORY_SEPARATOR . Cubix_Application::getId().'_284964_*';
        $FileList = glob($pattern);


		$filter = array(
		    'id' => $req->id, 'application_id' => $req->application_id,
            'templates_for' => $req->templates_for,
        );
		$data = $this->model->getAll(
			$req->page,
			$req->per_page, 
			$filter, 
			$req->sort_field, 
			$req->sort_dir, 
			$count
		);
		
		foreach($data as $k => $d)
		{
			$data[$k]->body = html_entity_decode($d->body);
		}

		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
	
	public function createAction()
	{
		if ( $this->_request->isPost() )
		{
					
			$entry_id = $this->_request->getParam('entry_id');
			$from_address = $this->_request->getParam('from_address');
			$subject = Cubix_I18n::getValues($this->_request->getParams(), 'subject', '');
			$body_plain = Cubix_I18n::getValues($this->_request->getParams(), 'body_plain', '');
			$body = Cubix_I18n::getValues($this->_request->getParams(), 'body', '');
			
			$validator = new Cubix_Validator();
			
			if ( ! strlen($from_address) ) {
				$validator->setError('from_address', 'This field is required');
			}
			
			if ( ! strlen($entry_id) ) {
				$validator->setError('entry_id', 'This field is required');
			}
			else {
				$item = $this->model->getById($entry_id, $this->_getParam('application_id'));

				if ( $item && $item->getId() == $entry_id ) {
					$validator->setError('entry_id', 'Choose another id, this one already exists');
				}
			}
			
			if ( $validator->isValid() ) {
				
				$data = array();				
				$langs = Cubix_I18n::getLangs(true);
				
				foreach ($langs as $lang)
				{
					$data[$lang]['id'] = $entry_id; 
					$data[$lang]['application_id'] = $this->_request->getParam('application_id');
					$data[$lang]['from_addr'] = $from_address;
					$data[$lang]['subject'] = $subject['subject_' . $lang];
					$data[$lang]['body_plain'] = $body_plain['body_plain_' . $lang];
					$data[$lang]['body'] = $body['body_' . $lang];
					$data[$lang]['lang_id'] = $lang;
				}
				
				$this->model->insert(new Model_System_EmailTemplatesItem($data));
			}
			
			die(json_encode($validator->getStatus()));
		}
	}

	public function editAction()
	{
		$template = $this->model->get($this->_getParam('id'), $this->_getParam('application_id'));
		$this->view->id = $this->_getParam('id');
		
		$langs = Cubix_I18n::getLangsByAppId($this->_getParam('application_id'));
		
		
		
		$data = array();
		foreach ($langs as $lang)
		{
			foreach ($template as $t)
			{
				if ($lang->lang_id == $t['lang_id'])
				{
					$data[$lang->lang_id]['id'] = $t['id'];
					$data[$lang->lang_id]['application_id'] = $t['application_id'];
					
					$data[$lang->lang_id]['from_addr'] = $t['from_addr'];
					$data[$lang->lang_id]['subject'] = $t['subject'];
					$data[$lang->lang_id]['body_plain'] = $t['body_plain'];
					$data[$lang->lang_id]['body'] = $t['body'];
					$data[$lang->lang_id]['lang_id'] = $t['lang_id'];
				}
			}
		}
		$this->view->template = $data;		
		
		if ( $this->_request->isPost() )
		{			
			$entry_id = $this->_request->getParam('entry_id');
			$from_address = $this->_request->getParam('from_address');
			$subject = Cubix_I18n::getValues($this->_request->getParams(), 'subject', '');
			$body_plain = Cubix_I18n::getValues($this->_request->getParams(), 'body_plain', '');
			$body = Cubix_I18n::getValues($this->_request->getParams(), 'body', '');
			
			$data = array();
			foreach ($langs as $lang)
			{
				$data[$lang->lang_id]['id'] = $this->view->id; 
				$data[$lang->lang_id]['application_id'] = $this->_request->getParam('application_id');
				$data[$lang->lang_id]['from_addr'] = $from_address;
				
				$data[$lang->lang_id]['subject'] = $subject['subject_' . $lang->lang_id];
				$data[$lang->lang_id]['body_plain'] = $body_plain['body_plain_' . $lang->lang_id];
				$data[$lang->lang_id]['body'] = $body['body_' . $lang->lang_id];
				$data[$lang->lang_id]['lang_id'] = $lang->lang_id;
				
			}
			
			$this->model->update(new Model_System_EmailTemplatesItem($data));
			
			$validator = new Cubix_Validator();
			die(json_encode($validator->getStatus()));
		}
		
	}
	
	public function removeAction()
	{
		$id = $this->_getParam('id');
		
		$this->model->remove($id, $this->_request->getParam('application_id'));
		
		die;
	}
}
