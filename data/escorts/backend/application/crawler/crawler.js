var SimpleCrawler = require("simplecrawler"),
	MongoClient = require('mongodb').MongoClient,
	nodemailer = require("nodemailer"),
	fs = require('fs'),
	async = require('async'),
	collectionName = 'crawler_results',
	mongoClient,
	parsedPagesCount = 0,
	parsedPhoneNumbersCount = 0,
	parsedEmailsCount = 0;

var Crawler = function() {
	self = this;
	
	this.init = function(callback)
	{
		MongoClient.connect('mongodb://127.0.0.1:27017/ed_crawler', function(err, db) {
			if ( err ) throw err;
			mongoClient = db;
			
			
			callback();
		});
	}
	this.start = function(link, callback)
	{
		var crw = SimpleCrawler.crawl(link);

		crw.on('fetchcomplete', function(queueItem, responseBuffer, response){
			if ( queueItem.stateData.contentType && queueItem.stateData.contentType == 'text/html' ) {
				var page = responseBuffer.toString('utf-8');
				self.parseInfo(page, queueItem.host, queueItem.path, function(err) {
					if ( err ) {
						console.log(err);
					} else {
						console.log(queueItem.url + ' parsed.');
					}
				});
			}
		});
		
		crw.on('fetcherror', function(queueItem, response) {
			console.log(response);
		});
		
		crw.on('fetchtimeout', function(queueItem, crawlerTimeoutValue ) {
			return callback();
		});
		
		crw.on('discoverycomplete', function(queueItem, resources) {
		});
		
		crw.addFetchCondition(function(parsedURL) {
			return ! (
				parsedURL.path.match(/\.pdf$/i)	||
				parsedURL.path.match(/\.jpg$/i)	||
				parsedURL.path.match(/\.png$/i)	||
				parsedURL.path.match(/\.gif$/i)	||
				parsedURL.path.match(/\.css$/i)	||
				parsedURL.path.match(/\.js$/i)
			);
		});
		
		crw.maxConcurrency = 10;
		crw.userAgent = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36';
		crw.timeout = 20000;
		
		crw.on('complete', function(){
			return callback();
		});
	}
	
	this.parseInfo = function(page, host, path, callback) {
		var phoneNumbers = self.getPhoneNumbers(page),
			emails = self.getEmails(page);

			mongoClient.collection(collectionName).insert({host: host, path: path}, {w:1}, function(err, obj) {
				if ( err ) {
					return callback(err.message);
				}

				var parsed = 0;
				if ( phoneNumbers && phoneNumbers.length ) {
					self.insertPhoneNumbers(host, path, phoneNumbers, function(err) {
						if ( err ) {
							return callback(err.message);
						}
						if ( ++parsed == 2 ) {
							return callback();
						}
					});
				} else {
					if ( ++parsed == 2 ) {
						return callback();
					}
				}

				if ( emails && emails.length ) {
					self.insertEmails(host, path, emails, function(err) {
						if ( err ) {
							return callback(err.message);
						}

						if ( ++parsed == 2 ) {
							return callback();
						}
					});
				} else {
					if ( ++parsed == 2 ) {
						return callback();
					}
				}
			});
	}
	
	this.getPhoneNumbers = function(html)
	{
		var phoneNumbers = html.match(/(\+|0{2})[0-9 \(\)\-]{9,}/gi);
		if ( ! phoneNumbers ) return [];
		for ( i = 0; i < phoneNumbers.length; i++ ) {
			phoneNumbers[i] = phoneNumbers[i].replace(/[^0-9\+]+/g, "").replace('+', '00');
		}
		
		return phoneNumbers
	}
	
	this.getEmails = function(html)
	{
		var emails = html.match(/\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+/gi);
		if ( ! emails ) return [];
		
		for ( i = 0; i < emails.length; i++ ) {
			emails[i] = emails[i].replace(/\s/g, "").toLowerCase();
		}
		
		return emails;
	}
	
	this.insertPhoneNumbers = function(host, path, phoneNumbers, callback)
	{
		var ph = phoneNumbers.slice(0);
		(function insertOne() {
			var phoneNumber = ph.splice(0, 1)[0];
			
			mongoClient.collection(collectionName).find({phoneNumbers: {$in: [phoneNumber]}}).count(function(err, count) {
				if ( err ) {
					return callback(err);
				}

				if ( ! count ) {
					mongoClient.collection(collectionName).update({host: host, path: path}, {$push: {phoneNumbers: phoneNumber}}, {upsert: true}, function(err, res) {
						if (err) {
							return callback(err);
						}
						
						parsedPhoneNumbersCount++;
						if ( ph.length == 0 ) {
							return callback();
						} else {
							insertOne();
						}
						
					});
				} else {
					if ( ph.length == 0 ) {
						return callback();
					} else {
						insertOne();
					}
				}
				
				return callback();

			});
			
		})();
	}
	
	this.insertEmails = function(host, path, emails, callback)
	{
		var em = emails.slice(0);
		(function insertOne() {
			var email = em.splice(0, 1)[0];
			
			mongoClient.collection(collectionName).find({emails: {$in: [email]}}).count(function(err, count) {
				if ( err ) {
					return callback(err);
				}

				if ( ! count ) {
					mongoClient.collection(collectionName).update({host: host, path: path}, {$push: {emails: email}}, {upsert: true}, function(err, res) {
						if (err) {
							return callback(err);
						}
						
						parsedEmailsCount++;
						if ( em.length == 0 ) {
							return callback();
						} else {
							insertOne();
						}
						
					});
				} else {
					if ( em.length == 0 ) {
						return callback();
					}{
						insertOne();
					}
				}
				return callback();
				
			});
			
		})();
	}
	
	this.sendEmails = function()
	{
		cursor = mongoClient.collection(collectionName).find({'$where': 'this.emails && this.emails.length > 0'}, ['emails']);
		cursor.each(function(err, doc) {
			if ( err ) {
				console.log(err);
			}
			
			if ( doc && doc.emails ) {
				for(i in doc.emails) {
					console.log(doc.emails[i]);
				}
			}
		});
		
		
		/*cursor.each(function(err, obj) {
			console.log(obj.emails);
		});*/
		
		/*var smtpTransport = nodemailer.createTransport("SMTP", {
			service: 'ESCORTDIRECTORY',
			host: "smtp.edir-mail.com",
			port: 25,
			auth: {
				user: "info@escortdirectory.com",
				pass: "ED1-ppq#W"
			}
		});
		
		var mailOptions = {
			from: "eva@escortdirectory.com",
			to: "kvahagn@gmail.com, sharp17s@hushmail.com",
			subject: "escortdirectory.com",
			html: "<p>New! Add your escort profile for FREE on <a href='http://www.escortdirectory.com'>http://www.escortdirectory.com</a> or contact us at: eva@escortdirectory.com.</p><p>See you on the site!</p>"
		}
		
		smtpTransport.sendMail(mailOptions, function(error, response){
			if(error){
				console.log(error);
			}else{
				console.log("Message sent: " + response.message);
			}
			smtpTransport.close();
		});*/
		
	}
	this.exportPhoneNumbers = function()
	{
		cursor = mongoClient.collection(collectionName).find({phoneNumbers: {$exists: true}, $where: 'this.phoneNumbers.length > 1'}, ['phoneNumbers']);
		
		var arr = [];
		cursor.toArray(function(err, docs) {
			for( j in docs ) {
				if ( docs[j].phoneNumbers ) {
					for( i in docs[j].phoneNumbers) {
						if ( docs[j].phoneNumbers[i].length > 9 ) {
							arr.push(docs[j].phoneNumbers[i]);
						}
					}
				}
			}
			
			fs.writeFile('phone_numbers.txt', arr.join(','), function(err) {
				if ( err ) {
					console.log(err);
				} else {
					console.log('Phone numbers done');
				}
				
				mongoClient.close();
			})
		});
	}
}

var crawler = new Crawler();
crawler.init(function() {
	
	crawler.exportPhoneNumbers();
//	async.eachLimit(['http://classifieds.myredbook.com/','https://www.slixa.com/','http://sipsap.com/'], 1, crawler.start, function(err) {
//		if ( err ) {
//			console.log(err);
//		}
//		
//		mongoClient.close();
//	});
});



