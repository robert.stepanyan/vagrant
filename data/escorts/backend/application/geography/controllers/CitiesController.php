<?php

class Geography_CitiesController extends Zend_Controller_Action
{
	public function init()
	{
		$this->model = new Model_Geography_Cities();
	}
	
	public function indexAction()
	{
	
	}
	
	public function dataAction()
	{
		$country_id = intval($this->_getParam('country_id'));
		$title = trim($this->_getParam('title'));
		$no_timezone = $this->_getParam('no_timezone');
		$zona_rossa = $this->_getParam('zona_rossa');
		
		if ( ! $country_id ) {
			$country_id = null;
		}
		
		$region_id = intval($this->_getParam('region_id'));
		$page = $this->_getParam('page');
		$per_page = $this->_getParam('per_page');
		$sort_field = $this->_getParam('sort_field');
		$sort_dir = $this->_getParam('sort_dir');
		$count = 0;
		$sort_field = 'order_id'; $sort_dir = 'ASC';
		$data = $this->model->getAll($region_id, $country_id, $page, $per_page, $sort_field, $sort_dir, $count, $title,$no_timezone,$zona_rossa);
		echo json_encode(array('data' => $data, 'count' => $count));
		die;
	}

	public function ajaxHomeCitiesSearchAction()
	{
		$name = trim($this->_getParam('city_search_name'));
		$lng = trim($this->_getParam('lng'));

		if ( ! strlen($name) ) {
			die(json_encode(array()));
		}
		$model = new Cubix_Geography_Cities();

		$cities = $model->ajaxGetAllCities($name, $lng);

		$tmp_cities = array();
		if ( count($cities) ) {
			foreach ( $cities as $i => $city ) {
				$tmp_cities[$i] = array('id' => $city->id, 'name' => $city->title . ' (' . $city->country_title . ')');
			}
		}

		echo json_encode($tmp_cities);
		die;
	}

	public function ajaxGetEscortCitiesAction()
	{
		$escort_id = intval($this->_getParam('escort_id'));

		$m_escorts = new Model_Escorts();
		$escort = $m_escorts->get($escort_id);

		$escort_cities = $escort->getCities();

		foreach( $escort_cities as $k => $esc_city ) {
			$escort_cities[$k]->is_base = 0;
			if ( $esc_city->id == $escort->city_id ) {
				$escort_cities[$k]->is_base = 1;
			}
		}
		
		die(json_encode($escort_cities));
	}
	
	public function ajaxAction()
	{
		$country_id = intval($this->_getParam('country_id'));
		$region_id = intval($this->_getParam('region_id'));
		$city_criteria = $this->_getParam('city_criteria');
		
		echo json_encode(array(
			'data' => $this->model->ajaxGetAll($region_id, $country_id, $city_criteria)
		));
		die;
	}
	
	public function createAction()
	{

		$country_id = intval($this->_getParam('country_id'));
		
		if ( ! $country_id ) {
			$country_id = null;
		}
		
		$this->view->country_id = $country_id;
		$this->view->region_id = $region_id = intval($this->_getParam('region_id'));
		$url_slug = $this->_getParam('url_slug');
		$title_geoip = $this->_getParam('title_geoip');

		$m_timezones = new Cubix_Geography_TimeZones();
		$this->view->time_zones = $m_timezones->getAll();
		
		if ( $this->_request->isPost() ) {

			$titles = Cubix_I18n::getValues($this->_request->getParams(), 'title', '');
			$validator = new Cubix_Validator();

			$time_zone_id = intval($this->_getParam('time_zone_id'));

			if ( ! strlen($titles['title_en']) ) {
				$validator->setError('title_en', 'English title is required');
			}
			
			if ( $country_id && ! $region_id ) {
				$countries_model = new Model_Geography_Countries();
				if ( $countries_model->get($country_id)->has_regions ) {
					$validator->setError('region_id', 'Required');
				}
			}
			elseif ( ! $country_id ) {
				$validator->setError('country_id', 'Required');
			}
            
			$citiesModel = new Model_Geography_Cities();

			$issetSlug = $citiesModel->getBySlug($url_slug);

			 if ($issetSlug > 0)
             {
                 $validator->setError('url_slug', 'Url slug exists');
             }
			
			if ( Cubix_Application::getId() != APP_ED ) {
				if ( ! strlen($url_slug) ) {
					$validator->setError('url_slug', 'Url slug is required');
				}
				elseif ( preg_match('/[^a-zA-Z0-9-]+/', $url_slug) ) {
					$validator->setError('url_slug', 'Only alphanumeric symbols');
				}
			}

			if ( $validator->isValid() ) {

				$save_data = array(
					'region_id' => $region_id,
					'country_id' => $country_id,
					'time_zone_id' => $time_zone_id,
					'url_slug'	=> $url_slug,
					'title_geoip'	=> $title_geoip
				);

				if ( Cubix_Application::getId() == APP_ED || Cubix_Application::getId() == APP_6B ) {
					$m_countries = new Model_Geography_Countries();
					$geo_country = $m_countries->get($country_id);

					$address = array(
						$titles['title_en'],
						$geo_country->title_en
					);
                    $location = str_replace (" ", "+", implode(',', $address));
                    $url = "http://open.mapquestapi.com/geocoding/v1/address?key=2IUAiWs5TEXGwOPGlGoPe9fnpaIniyFZ&location=".$location;
					$url = preg_replace('# #', '+', $url);

					$geo_data = file_get_contents($url);
					$geo_data = json_decode($geo_data);

                    $coord = array(
                        'latitude' => $geo_data->results[0]->locations[0]->latLng->lat,
                        'longitude' => $geo_data->results[0]->locations[0]->latLng->lng
                    );

					$save_data = array_merge($save_data, $coord);
				}

				$this->model->save(new Model_Geography_CityItem(array_merge($save_data, $titles)));
			}

			die(json_encode($validator->getStatus()));
		}
	}
	public function locationsupdateAction()
	{
		echo 'no permission';die;
		//if(Cubix_Application::getId() == APP_6B){
//		if(Cubix_Application::getId() == APP_ED){
		if(Cubix_Application::getId() == APP_EG_CO_UK){
			$cities = $this->model->getCitiesWithoutCoords();

			foreach ($cities AS $city) {
				echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
				
				$address = $city->country.', '. $city->title_en;
				$cityclean = str_replace (" ", "+", $address);
				$details_url = "http://open.mapquestapi.com/geocoding/v1/address?key=2IUAiWs5TEXGwOPGlGoPe9fnpaIniyFZ&location=".$cityclean;
				
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $details_url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				$geoloc = json_decode(curl_exec($ch), true);
				
				
				$coord = array(
					'longitude' => $geoloc['results'][0]['locations'][0]['latLng']['lng'],
					'latitude' => $geoloc['results'][0]['locations'][0]['latLng']['lat']
				);

				$this->model->SetCoordinates($city->id, $coord['longitude'], $coord['latitude']);

			}
			echo 'done';die;
		}
		
	}
	public function editAction()
	{
		$id = intval($this->_getParam('id'));

		$m_timezones = new Cubix_Geography_TimeZones();
		$this->view->time_zones = $m_timezones->getAll();

		if ( $this->_request->isPost() ) {
			$country_id = intval($this->_getParam('country_id'));
			$region_id = intval($this->_getParam('region_id'));
			$time_zone_id = intval($this->_getParam('time_zone_id'));
			$titles = Cubix_I18n::getValues($this->_request->getParams(), 'title', '');
			$url_slug = $this->_getParam('url_slug');
			$title_geoip = $this->_getParam('title_geoip');
			
			$validator = new Cubix_Validator();
			
			if ( ! strlen($titles['title_en']) ) {
				$validator->setError('title_en', 'English title is required');
			}

			if ( ! $time_zone_id ) {
				$time_zone_id = null;
			}
			
			if ( $country_id && ! $region_id ) {
				$countries_model = new Model_Geography_Countries();
				if ( $countries_model->get($country_id)->has_regions ) {
					$validator->setError('region_id', 'Required');
				}
			}
			elseif ( ! $country_id ) {
				$validator->setError('country_id', 'Required');
			}

			if ( Cubix_Application::getId() != APP_ED ) {
				if ( ! strlen($url_slug) ) {
					$validator->setError('url_slug', 'Url slug is required');
				}
				elseif ( preg_match('/[^a-zA-Z0-9-]+/', $url_slug) ) {
					$validator->setError('url_slug', 'Only alphanumeric symbols');
				}
			}
			
			if ( $validator->isValid() ) {

				$save_data = array(
					'id' => $id,
					'region_id' => $region_id,
					'country_id' => $country_id,
					'time_zone_id' => $time_zone_id,
					'url_slug'		=> $url_slug,
					'title_geoip'	=> $title_geoip
				);

				if ( Cubix_Application::getId() == APP_ED || Cubix_Application::getId() == APP_6B  ) {
					$m_countries = new Model_Geography_Countries();
					$geo_country = $m_countries->get($country_id);

					$address = array(
						$titles['title_en'],
						$geo_country->title_en
					);

					$url = "http://open.mapquestapi.com/geocoding/v1/address?key=2IUAiWs5TEXGwOPGlGoPe9fnpaIniyFZ&location=" . implode(',', $address);
					$url = preg_replace('# #', '+', $url);
					
					
					$geo_data = file_get_contents($url);
					$geo_data = json_decode($geo_data);
					
					if(!is_null($geo_data)){
                        $coord = array(
                            'latitude' => $geo_data->results[0]->locations[0]->latLng->lat,
                            'longitude' => $geo_data->results[0]->locations[0]->latLng->lng
                        );
                        $save_data = array_merge($save_data, $coord);
					}else{
						$cityclean = str_replace (" ", "+", implode(',', $address));
						$details_url = "http://open.mapquestapi.com/geocoding/v1/address?key=2IUAiWs5TEXGwOPGlGoPe9fnpaIniyFZ&location=" . $cityclean;

						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, $details_url);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						$geoloc = json_decode(curl_exec($ch), true);

						$coord = array(
							'latitude' => $geoloc['results'][0]['locations'][0]['latLng']['lat'],
							'longitude' => $geoloc['results'][0]['locations'][0]['latLng']['lng']
						);
						$save_data = array_merge($save_data, $coord);
					}
				}

				$this->model->save(new Model_Geography_CityItem(array_merge($save_data, $titles)));
			}
			
			die(json_encode($validator->getStatus()));
		}
		else {
			$this->view->city = $this->model->get($id);
		}
	}
	
	public function ajaxGetCitiesGroupedAction()
	{
		$country_id = intval($this->_getParam('country_id'));
		$region_id = intval($this->_getParam('region_id'));
		
		//$model = new Cubix_Geography_Cities();
		
		$cities = $this->model->ajaxGetAll($region_id, $country_id);
		
		$regions = array();
		foreach ( $cities as $city ) {
			if ( ! isset($regions[$city->region_title]) ) {
				$regions[$city->region_title] = array();
			}
			$has_regions = $city->has_regions;
			$regions[$city->region_title][] = $city;
		}
		
		//ksort($regions);
		
		foreach ( $regions as $k => $region ) {
			usort($regions[$k], create_function('$a1, $a2', 'return ($a1["title"] == $a2["title"]) ? 1 : ($a1["title"] > $a2["title"] ? 1 : 0);'));
		}
		
		$is_json = ! is_null($this->_getParam('json'));
		if ( $is_json ) {
			die(json_encode($regions));
		}
		
		$html = '';

		if ( $has_regions )
		{
			foreach ( $regions as $title => $cities )
			{

				$html .= $has_regions ? '<optgroup label="' . $title . '">' : '';
				foreach ( $cities as $city )
				{
					$html .= '<option value="' . $city->id .'">' . $city->title . '</option>';
				}
				$html .= $has_regions ? '</optgroup>' : '';
			}
		}
		else
		{
			foreach ( $cities as $city )
			{
				$html .= '<option value="' . $city->id .'">' . $city->title . '</option>';
			}
		}
		
		echo $html;
		
		die;
	}
	
	public function removeAction()
	{
        $source_city_id = intval($this->_getParam('id'));
        $destination_city_id = intval($this->_getParam('destination_city'));
        $bu_user = Zend_Auth::getInstance()->getIdentity();
        $client = Cubix_Api_XmlRpc_Client::getInstance();

        // check if deleting city is used even one escort
        $escorts = $client->call('Escorts.getEscortsByCityOrHomeCityId', array($source_city_id)); // from escorts
        $escort_cities = $client->call('Escorts.getEscortCitiesByCityId', array($source_city_id)); // from escort_cities
        $escort_ids = $client->call('Escorts.getEscortIdsByCityId', array($source_city_id)); // form escorts and escort_cities table together
        $escorts_has_source_city = $escorts || $escort_cities || $escort_ids;

        if ( $this->_request->isPost() ) {
            if (in_array(Cubix_Application::getId(), array(APP_ED)) && ($bu_user->type == 'admin' || $bu_user->type == 'superadmin')) {
                if ($escorts_has_source_city) {
                    $db = Zend_Registry::get('db');
                    $cities = $db->fetchAll("SELECT id FROM cities");
                    $errors = $city_ids = array();
                    $destination_city = $this->model->get($destination_city_id);

                    foreach ($cities as $city) {
                        $city_ids[] = $city->id;
                    }

                    if (!$source_city_id) {
                        $errors['source_city_id'] = 'Selected source city ID is required';
                    } elseif (!$destination_city_id) {
                        $errors['destination_city'] = 'Please select city';
                    } elseif (!$destination_city) { // check if destination city exist in cities table
                        $errors['destination_city'] = 'Selected city ID is not valid';
                    }

                    if (!empty($errors)) {
                        die(json_encode(array('status' => 'error', 'msgs' => $errors)));
                    }

                    try {
                        $db->beginTransaction();

                        if (!is_null($escort_ids)) {

                            // change city, home_city and cities in profile_updates_v2 table
                            $profile_last_updates = $client->call('Escorts.getLastUpdatedProfilesByEscortIds', array($escort_ids));

                            foreach ($profile_last_updates as $profile_update) {
                                $new_profile = (array)$profile_update;
                                $data = unserialize($profile_update['data']);
                                if ($data['city_id'] == $source_city_id) {
                                    $data['city_id'] = $destination_city_id;
                                }
                                if ($data['home_city_id'] == $source_city_id) {
                                    $data['home_city_id'] = $destination_city_id;
                                }

                                if (isset($data['cities']) && is_array($data['cities'])) {
                                    $profile_cities = array();
                                    foreach ($data['cities'] as $data_city) {
                                        if ($data_city['city_id'] != $source_city_id) {
                                            $profile_cities[] = $data_city;
                                        }
                                    }
                                    $data['cities'] = $profile_cities;
                                }

                                unset($new_profile['id']);
                                $new_profile['data'] = serialize($data);

                                if ($profile_update['status'] == 2) {
                                    $new_profile['date_updated'] = new Zend_Db_Expr('NOW()');
                                    $new_profile['backend_user_id'] = $bu_user->id;
                                    $new_profile['revision'] = intval($profile_update['revision']) + 1;
                                    $db->insert('profile_updates_v2', $new_profile);
                                } else {
                                    $db->update('profile_updates_v2', $new_profile, $db->quoteInto('id = ?', $profile_update['id']));
                                }
                            }

                            // change city, home_city, base_city and cities in escort_profiles_snapshots_v2 table
                            $escort_profiles_snapshots_v2_table_exist = $client->call('Application.checkTableExist', array('escort_profiles_snapshots_v2'));
                            if ($escort_profiles_snapshots_v2_table_exist) {
                                $profile_snapshots = $client->call('Escorts.getProfileSnapshotsByEscortIds', array($escort_ids));
                                foreach ($profile_snapshots as $profile_snapshot) {
                                    $snapshot_data = (array)unserialize($profile_snapshot['data']);
                                    if (isset($snapshot_data['city_id']) && $snapshot_data['city_id'] == $source_city_id) {
                                        $snapshot_data['city_id'] = $destination_city_id;
                                    }
                                    if (isset($snapshot_data['home_city_id']) && $snapshot_data['home_city_id'] == $source_city_id) {
                                        $snapshot_data['home_city_id'] = $destination_city_id;
                                    }
                                    if (isset($snapshot_data['base_city_id']) && $snapshot_data['base_city_id'] == $source_city_id) {
                                        $snapshot_data['base_city_id'] = $destination_city_id;
                                    }

                                    if (isset($snapshot_data['cities']) && is_array($snapshot_data['cities'])) {
                                        $snapshot_cities = array();
                                        foreach ($snapshot_data['cities'] as $snapshot_data_city) {
                                            if (isset($snapshot_data_city['city_id']) && $snapshot_data_city['city_id'] != $source_city_id) {
                                                $snapshot_cities[] = $snapshot_data_city;
                                            }
                                        }
                                        $snapshot_data['cities'] = $snapshot_cities;
                                    }
                                    $db->update('escort_profiles_snapshots_v2', array('data' => serialize($snapshot_data)), $db->quoteInto('escort_id = ?', $profile_snapshot['escort_id']));
                                }
                            }
                        }

                        // update city and home_city in escorts table
                        foreach ($escorts as $escort) {
                            $escort_data = [
                                'city_id' => $escort['city_id'] == $source_city_id ? $destination_city_id : $escort['city_id'],
                                'home_city_id' => $escort['home_city_id'] == $source_city_id ? $destination_city_id : $escort['home_city_id'],
                                'country_id' => $destination_city->country_id
                            ];

                            $db->update('escorts', $escort_data, $db->quoteInto('id = ?', $escort['id']));
                        }

                        // delete source cities from escort_cities table
                        $db->delete('escort_cities', $db->quoteInto('city_id = ?', $source_city_id));

                        // update escort cities
                        foreach ($escort_cities as $escort_city) {
                            // check if escort already has an destination city (to avoid duplications)
                            $escort_city_exist = $db->fetchOne('SELECT TRUE FROM escort_cities WHERE escort_id = ? AND city_id = ?', array($escort_city['escort_id'], $destination_city_id));
                            if (!$escort_city_exist) {
                                $db->insert('escort_cities', array('escort_id' => $escort_city['escort_id'], 'city_id' => $destination_city_id));
                            }
                        }

                        // change city ids in a few tables
                        $db->update('gotd_orders', array('city_id' => $destination_city_id), $db->quoteInto('city_id = ?', $source_city_id));
                        $db->update('premium_escorts', array('city_id' => $destination_city_id), $db->quoteInto('city_id = ?', $source_city_id));
                        $db->update('order_packages', array('gotd_city_id' => $destination_city_id), $db->quoteInto('gotd_city_id = ?', $source_city_id));
                        $db->update('members', array('city_id' => $destination_city_id), $db->quoteInto('city_id = ?', $source_city_id));
                        $db->update('agencies', array('city_id' => $destination_city_id), $db->quoteInto('city_id = ?', $source_city_id));

                        $user_city_alerts_table_exist = $client->call('Application.checkTableExist', array('user_city_alert'));
                        if ($user_city_alerts_table_exist) {
                            $db->update('user_city_alert', array('city_id' => $destination_city_id), $db->quoteInto('city_id = ?', $source_city_id));
                        }

                        $blacklisted_clients_6a_table_exist = $client->call('Application.checkTableExist', array('blacklisted_clients_6a'));
                        if ($blacklisted_clients_6a_table_exist) {
                            $db->update('blacklisted_clients_6a', array('city_id' => $destination_city_id), $db->quoteInto('city_id = ?', $source_city_id));
                        }

                        $blacklisted_clients_hotels_table_exist = $client->call('Application.checkTableExist', array('blacklisted_clients_6a'));
                        if ($blacklisted_clients_hotels_table_exist) {
                            $db->update('blacklisted_clients_hotels', array('city_id' => $destination_city_id), $db->quoteInto('city_id = ?', $source_city_id));
                        }

                        // save converting(deleting) city info in activity log table
                        Model_Activity::getInstance()->log('delete_city', array('source_city_id' => $source_city_id, 'destination_city_id' => $destination_city_id));

                        // if everything is ok apply all queries
                        $db->commit();
                    } catch (Exception $ex) {
                        $db->rollBack();
                        die(json_encode(array('status' => 'error', 'msgs' => $ex->getMessage())));
                    }
                }

                // delete city
                $this->model->remove($source_city_id);
            }

            die(json_encode(array('status' => 'success')));
        }

        $this->view->bu_user = $bu_user;
        $this->view->escorts_has_source_city = $escorts_has_source_city;
        $this->view->city = $this->model->get($source_city_id);
        $this->view->country = Model_Geography_Cities::getCountry($source_city_id);
    }

	public function toggleFrenchAction()
	{
		$id = intval($this->_getParam('id'));
		$db = Zend_Registry::get('db');
		$db->update('cities', array('is_french' => new Zend_Db_Expr('NOT is_french')), array($db->quoteInto('id = ?', $id)));
	}

	public function toggleZonaRossaAction()
	{
		$id = intval($this->_getParam('id'));
		$db = Zend_Registry::get('db');
		$db->update('cities', array('zona_rossa' => new Zend_Db_Expr('NOT zona_rossa')), array($db->quoteInto('id = ?', $id)));
	}
	
	public function syncAction()
	{
		$app_id = $this->_getParam('application_id');
        $action_name = ($this->_getParam('action_name')) ? $this->_getParam('action_name') : 'Cities';
		$app = Cubix_Application::getById($app_id);

		$http = 'http';


        $url = $http.'://www.' . $app->host;
		$sync_url = $url . '/api/dictionary/geography-plain';
		
		$client = new Zend_Http_Client($sync_url, array(
				'timeout'      => 60)
		);

		$client->setAuth('babazeo', 'GnaMer1a', Zend_Http_Client::AUTH_BASIC);

		$response = $client->request(Zend_Http_Client::GET);
	
		if ( $response->isError() ) {
			die(json_encode(array('status' => 'error', 'message' => $response->getMessage())));
		}

		header('Cubix-message: ' . $url . ' has been successfully syncronized');
		die(json_encode(array('status' => 'success','message'=>"{$action_name} was successfully synchronized", 'result' => $url, 'response' => $response->getBody())));
	}
	
	public function orderingAction()
	{
		$id = $this->_getParam('id');
		$dir = $this->_getParam('dir');
		
		if ( ! $id || ! $dir ) die;
		
		$this->model->changeOrder($id, $dir);
		
		die('Success');
	}
	
	public function setTimezoneAction()
	{
		if ( $this->_request->isPost() ) {
			$ids = unserialize($this->_getParam('ids'));
			$time_zone_id = intval($this->_getParam('time_zone_id'));
			$validator = new Cubix_Validator();
			if ( ! $time_zone_id ) {
				$validator->setError('time_zone_id', 'Required');
			}
			$this->model->setTimeZones($ids,$time_zone_id);
			die(json_encode($validator->getStatus()));
		}
		else{
			$ids = $this->_getParam('id');
			$this->view->ids = htmlentities(serialize($ids));
			$m_timezones = new Cubix_Geography_TimeZones();
			$this->view->time_zones = $m_timezones->getAll();
			
		}
	}


	public function ajaxGetAllCitiesGroupedByCountryAction()
	{
		$country_id = intval($this->_getParam('country_id'));		
		
		$cities = $this->model->ajaxGetAllGroupedByCountry($country_id);
		
		$countries = array();
		foreach ( $cities as $city ) {
			if ( ! isset($countries[$city->country_title]) ) {
				$countries[$city->country_title] = array();
			}			
			$countries[$city->country_title][] = $city;
		}	
		
		foreach ( $countries as $k => $country ) {
			usort($countries[$k], create_function('$a1, $a2', 'return ($a1["title"] == $a2["title"]) ? 1 : ($a1["title"] > $a2["title"] ? 1 : 0);'));
		}		
		
		$html = '';
		
		foreach ( $countries as $title => $cities )
		{
			$html .= '<optgroup label="' . $title . '">';
			foreach ( $cities as $city )
			{
				$html .= '<option value="' . $city->id .'">' . $city->title . '</option>';
			}
			$html .= '</optgroup>';
		}
		
		
		die($html);
	}
}