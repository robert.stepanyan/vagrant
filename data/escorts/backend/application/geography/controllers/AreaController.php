<?php

class Geography_AreaController extends Zend_Controller_Action
{
    protected $model;
    protected $model_cities;

	public function init()
	{
		$this->model = new Model_Geography_Regions();
		$this->model_cities = new Model_Geography_Cities();

	}
	
	public function indexAction()
	{
		
	}
	

	
	public function ajaxAction()
	{
		$region_id = intval($this->_getParam('region_id'));

		echo json_encode(array(
			'data' => $this->model_cities->getRegionCities($region_id)
		));
		die;
	}


	public function toggleFrenchAction()
	{
		$id = intval($this->_getParam('id'));
		$db = Zend_Registry::get('db');
		$db->update('regions', array('is_french' => new Zend_Db_Expr('NOT is_french')), array($db->quoteInto('id = ?', $id)));
	}
}
