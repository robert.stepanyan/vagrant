<?php

class Geography_UniversalController extends Zend_Controller_Action
{
	public function init()
	{
		$this->model = new Model_Geography_Abstract();
		$this->model->setEntity($this->_getParam('entity'));
		
		$this->view->parents = $this->model->getParents();
		
		$this->view->entity = $this->_getParam('entity');
		$this->view->singular = preg_replace('/ies$/', 'y', $this->view->entity);
		$this->view->singular = preg_replace('/es$/', 'e', $this->view->singular);
		$this->view->singular = preg_replace('/s$/', '', $this->view->singular);
	}
	
	public function indexAction ()
	{
		
	}

	public function dataAction()
	{
		$count = 0;
		
		$p = intval($this->_getParam('page'));
		$pp = intval($this->_getParam('per_page'));
		
		$sort_field = $this->_getParam('sort_field');
		$sort_dir = $this->_getParam('sort_dir');
		
		$data = $this->model->getAll($p, $pp, $sort_field, $sort_dir, $count);
		echo json_encode(array('data' => $data, 'count' => $count));
		die;
	}
	
	public function ajaxAction()
	{
		$parent_id = intval($this->_getParam('parent_id'));
		
		$filter = array();
		
		if ( $parent_id ) {
			$filter['parent_id = ?'] = $parent_id;
		}
		
		$data = $this->model->getFiltered($filter);
		echo json_encode(array('data' => $data));
		die;
	}
	
	public function createAction()
	{
		if ( $this->_request->isPost() ) {
			$this->model->save($this->_getItem());
		}
		else {
		}
	}
	
	public function editAction()
	{
		if ( $this->_request->isPost() ) {
			/*
				TODO validation
			*/
			
			$this->model->save($this->_getItem());
			die;
		}
		else {
			$id = intval($this->_getParam('id'));
			
			$this->view->item = $this->model->get($id);
		}
	}
	
	public function removeAction()
	{
		$id = intval($this->_getParam('id'));
		
		$this->model->remove($id);
		die;
	}
	
	protected function _getItem() {
		$title = trim($this->_getParam('title'));
		
		$data = array(
			'title' => $title
		);
		
		$id = intval($this->_getParam('id'));
		
		if ( $id ) {
			$data['id'] = $id;
		}
		
		$parent_id = intval($this->_getParam('parent_id'));
		
		if ( $parent_id ) {
			$data['parent_id'] = $parent_id;
		}
		
		$item = new Model_Geography_AbstractItem($data);
		
		return $item;
	}
}
