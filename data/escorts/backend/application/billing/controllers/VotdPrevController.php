<?php

class Billing_VotdPrevController extends Zend_Controller_Action
{
	/**
	 * @var Model_Billing_Packages
	 */
	protected $_model;

	public function init()
	{
		$this->_model = new Model_Billing_Packages();
	}

	public function indexAction()
	{
		// var_dump('expression');die;
	}

	public function dataAction()
	{
		$req = $this->_request;

		$filter = array(
			'ag.name' => $req->agency_name,
			'e.showname' => $req->showname,
			'e.id' => $req->escort_id,
			// Date
			'filter_by' => $req->filter_by,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to,
			'sales_user_id' => $req->sales_user_id,
			'sales_user_id_real' => $req->sales_user_id_real,
			
			'votd_city' => $req->votd_city,
			'votd_status' => $req->votd_status,

			//'op.status' => Model_Billing_Packages::STATUS_ACTIVE,
			/*'op.application_id' => $req->app_id,*/
			'o.sys_order_id' => $req->sys_order_id
		);

		$data = $this->_model->getAllV2(
			$this->_request->page,
			$this->_request->per_page,
			$filter,
			$this->_request->sort_field,
			$this->_request->sort_dir,
			$count,
			false,
			true
		);
		
		$votd_statuses = Model_Products::$VOTD_STATUS_TITLES;
		foreach ( $data as $k => $package ) {
			$data[$k]->package_status = Model_Packages::$STATUS_LABELS[$package->package_status];
			$data[$k]->votd_status_text = $votd_statuses[$package->votd_status];
			
			if ( Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT )
				$data[$k]->is_suspendable = $package->isSuspendable();
			
			//FIXING PACKAGES LOGIC
			//IF PACKAGE EXPIRES 19 APRIL IT'S ACTUALLY EXPIRES 18 APRIL 23:59
			if ( $data[$k]->expiration_date ) {
				$data[$k]->expiration_date -= 60*60*24; // -1 day
			}
		}
		
		die(json_encode(array('data' => $data, 'count' => $count, 'sum' => 'TOTAL PAID: ' . number_format($this->_model->votd_sum, 2))));
	}
	
	public function cancelPendingVotdAction()
	{
		$votd_id = (int) $this->_request->votd_id;
		
		if (! $votd_id ) {
			die(')');
		}
		
		$this->_model->cancelPendingVotd($votd_id);
		
		die();
	}
}
