<?php

class Billing_ProfileBoosterController extends Zend_Controller_Action
{
	/**
	 * @var Model_Billing_Packages
	 */
	protected $_model;

	public function init()
	{
		$this->_model = new Model_Billing_ProfileBooster();
	}

	public function indexAction()
	{
		
	}

	public function dataAction()
	{
		$req = $this->_request;

		$filter = array(
			'e.showname' => $req->showname,
			'e.id' => $req->escort_id,
			/*'date_from' => $req->date_from,
			'date_to' => $req->date_to,
			'hour_from' => $req->hour_from,
			'hour_to' => $req->hour_to,*/
			'filter_by' => $req->filter_by,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to,
			'boost_city' => $req->boost_city,
			'order_id'	=> $req->order_id,
			'transaction_id'	=> $req->transaction_id,
			'sales_user_id' => $req->sales_user_id,
			'sales_user_id_real' => $req->sales_user_id_real,
		);

		$data = $this->_model->getAll(
			$this->_request->page,
			$this->_request->per_page,
			$filter,
			$this->_request->sort_field,
			$this->_request->sort_dir,
			$count,
			true
		);
		
		foreach($data as $i => $d) {
			$data[$i]->activation_date += $d->hour_from * 60 * 60;
			$data[$i]->editable = true;
			if ( $data[$i]->activation_date <  time() ) {
				$data[$i]->editable = false;
			}
		}
		
		die(json_encode(array('data' => $data, 'count' => $count, 'sum' => 'TOTAL PAID: ' . number_format($this->_model->booster_sum, 2))));
	}
	
	public function addAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		$m_package = new Model_Billing_Packages();
		$m_products = new Model_Products();
		$client = Cubix_Api_XmlRpc_Client::getInstance();
				
		$op_id = $this->view->op_id = $req->order_package_id;
		$package = $m_package->get($op_id);

		$escort_id = $package->escort_id;
		
		$application_id = Cubix_Application::getId();
		
		
		if ( $this->_request->isPost() ) { 
			$booked_hours = $client->call('OnlineBillingV2.getBookedProfileBoosts', array(null, $escort_id));
			
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'user_id' => 'int',
				'orders' => 'arr-int',
				'transfer_type_id' => 'int',
				'date_transfered' => 'int',
				'amount' => 'double',
				'first_name' => '',
				'last_name' => '',
				'country_id' => 'int',
				'city' => '',
				'var_symbol' => '',
				'transaction_id' => '',
				'pull_person_id' => 'int',
				'webmoney_account' => 'int',
				'website_id' => 'int',
				'mtcn' => '',
				'comment' => 'notags|special',
				'application_id' => $this->_request->application_id,
				'discount'	=> '',
				'd_comment'	=> '',
				'boosts'	=> ''
			));
			$data = $data->getData();			
			
			$validator = new Cubix_Validator();
			$boosts = array();
			
			if ( ! $data['boosts'] || ! count($data['boosts']) ) {
				$validator->setError('boost_err', 'Select at least one booster.');
			} else {
				foreach($data['boosts'] as $boost) {
					list($city_id, $date, $hour) = explode(":", $boost);
					$date = date('Y-m-d', strtotime($date));
					$boosts[] = array('city_id' => is_numeric($city_id) ? $city_id : null, 'date' => $date, 'hour' => $hour);
					
					if ( strtotime($date) + $hour * 60 * 60 < time() ) {
						$validator->setError('boost_err', 'Date should be in the future.');
					}
					
					if ( $booked_hours['all'][$date] ) {
						foreach($booked_hours['all'][$date] as $b) {
							if ( $b['hour_from'] == $hour ) {
								$validator->setError('boost_err', 'This date/hour already booked.');
								break;
							}
						}
					}

					//Checking if escort has booked boost ffor the same date-hour
					//Escort can have 1 boost on index and 1 in city for the same hour
					foreach($booked_hours['escort'] as $b) {
						if ( $b['date'] == $date && $b['hour_from'] == $hour && 
							(($city_id != 'index' && $b['city_id'] != 'index' ) || ($city_id == 'index' && $b['city_id'] == 'index' ) )
						) {
							$validator->setError('boost_err', 'Already have boost for that date/hour.');
						}
					}
				}
			}
			
			$data['user_id'] = $package->user_id;
			$data['orders'] = array($package->order_id);
			
			
			if ( ! $data['transfer_type_id'] ) {
				$validator->setError('transfer_type_id', 'Required');
			}

			$data['date_transfered'] = time();
			
			if ( ! $data['amount'] && ! $data['discount'] ) {
				$validator->setError('days', 'Amount Required');
			}

			if ( $data['transfer_type_id'] == 5 ) {
				if ( ! $data['webmoney_account'] ) {
					$validator->setError('webmoney_account', 'Required');
				}
				
				if ( ! $data['transaction_id'] ) {
					$validator->setError('transaction_id', 'Required');
				}
			}

			if ( $data['transfer_type_id'] == 3 ) {
				if ( ! $data['transaction_id'] ) {
					$validator->setError('transaction_id', 'Required');
				}
			}
			
			if ( $data['transfer_type_id'] != 5 ) {
				if ( $data['transfer_type_id'] && ! $data['first_name'] ) {
					$validator->setError('first_name', 'Required');
				}

				if ( $data['transfer_type_id'] && ! $data['last_name'] ) {
					$validator->setError('last_name', 'Required');
				}
			}
			
			$d_apps = array(APP_EF, APP_6A);
			if ( in_array(Cubix_Application::getId(), $d_apps) ) {
				if ( $data['discount'] > 0 && ! strlen($data['d_comment']) ) {
					$validator->setError('d_comment', 'Required');
				}
				
				$data['comment'] = $data['d_comment'];
			}
			unset($data['d_comment']);
			unset($data['discount']);

			$data['backend_user_id'] = Zend_Auth::getInstance()->getIdentity()->id;
			$data['status'] = Model_Billing_Transfers::STATUS_CONFIRMED;//Set status as confirmed. no need to confirm manually
			
			if ( $data['transaction_id'] ) {
				$m_transfer = new Model_Billing_Transfers();
				if ( false === $m_transfer->checkTransactionId($data['transaction_id']) ) {
					$validator->setError('transaction_id', 'Already in database');
				}
			}
			
			if ( $validator->isValid() ) {
				unset($data['boosts']);
				$_model = new Model_Billing_Transfers();
				
				if ( $transfer_id = $_model->save($transfer = new Model_Billing_TransferItem($data), true) ) {
					//$validator->setError('global_error', $result);					
				}

				
				//ATTENTION !!! confirm only when transfer types is not bank transfer/sales einkassiert.
				//transfer is confirming in save function above
				if ( $data['transfer_type_id'] != 1 && $data['transfer_type_id'] != 6 ) {
					$tr = new Model_Billing_TransferItem(array('id' => $transfer_id));
					$tr->confirm('Profile Booster Transfer', $data['amount']);
				}
				
				
				$this->_model->add($boosts, $escort_id, array('transfer_id' => $transfer_id, 'order_package_id' => $op_id, 'price' =>$data['amount'], 'order_id' => $package->order_id));
				Cubix_SyncNotifier::notify($package->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PACKAGE_EXPIRATION_UPDATED, array('id' => $op_id));
			}

			die(json_encode($validator->getStatus()));
		}
	}
	
	public function editAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$this->view->boost = $this->_model->get($req->id);
		
		
		if ( $this->_request->isPost() ) { 
			$booked_hours = $client->call('OnlineBillingV2.getBookedProfileBoosts', array(null, $escort_id));
			
			$data = new Cubix_Form_Data($this->_request);
			
			$data->setFields(array(
				'id'	=> '',
				'city_id'	=> '',
				'activation_date'	=> '',
				'hour_from'	=> ''
			));
			$data = $data->getData();			
			
			$validator = new Cubix_Validator();
			
			$data['activation_date'] = date('Y-m-d', $data['activation_date']);
			
			if ( ! $data['city_id'] ) {
				$validator->setError('boost_err', 'City required');
			}
			
			if ( ! $data['hour_from'] ) {
				$validator->setError('boost_err', 'Hour required');
			}
			if ( strtotime($data['activation_date']) + $data['hour_from'] * 60 * 60 < time() ) {
				$validator->setError('boost_err', 'Date should be in the future.');
			}

			if ( $booked_hours['all'][$data['activation_date']] ) {
				foreach($booked_hours['all'][$data['activation_date']] as $b) {
					if ( $b['hour_from'] == $data['hour_from'] ) {
						$validator->setError('boost_err', 'This date/hour already booked.');
						break;
					}
				}
			}

			//Checking if escort has booked boost ffor the same date-hour
			//Escort can have 1 boost on index and 1 in city for the same hour
			foreach($booked_hours['escort'] as $b) {
				if ( $b['date'] == $data['activation_date'] && $b['hour_from'] == $data['hour_from'] && 
					(($data['city_id'] != 'index' && $b['city_id'] != 'index' ) || ($data['city_id'] == 'index' && $b['city_id'] == 'index' ) )
				) {
					$validator->setError('boost_err', 'Already have boost for that date/hour.');
				}
			}
			
			if ( $validator->isValid() ) {
				if ( ! is_numeric($data['city_id']) ) $data['city_id'] = null;
				$this->_model->update($data);
			}

			die(json_encode($validator->getStatus()));
		}
	}
	
	
	public function removeAction()
	{
		$id = (int) $this->_request->id;
		
		if (! $id ) {
			die(')');
		}
		
		$this->_model->remove($id);
		
		die();
	}
}
