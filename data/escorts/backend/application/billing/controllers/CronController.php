<?php

class Billing_CronController extends Zend_Controller_Action
{
	/**
	 *
	 * @var Zend_Db_Adapter_Abstract
	 */
	protected $_db;

	public function init()
	{
		$this->_db = Zend_Registry::get('db');
	}

	public function indexAction()
	{
		if ( ! defined('IS_CLI') || ! IS_CLI ) {
			die('Please run from command line');
		}

		$cli = new Cubix_Cli();
		$pid_file = '/var/run/v2_billing_' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		if ( $cli->isRunning() ) {
			die('Cron is already running, exitting...' . PHP_EOL);
		}
		$cli->clear();
		$cli->out();
		set_time_limit(0);
		
		if ( ! is_null($this->_getParam('cron')) && (! defined('IS_CLI') || ! IS_CLI) ) {
			$result = Model_Cron::run(array('grace-period'));
		}

		do {			
			$steps = array(
				'pending-transfers', 'pending-bank-transfers', 
				'pending-orders', 'expired-packages', 
				'asap-packages', 'exact-date-packages', 
				'payment-rejected-orders', 
				'fix-after-current-package-expires-packages',
				'active-packages-without-exp-date'
			);
			
			if ( Cubix_Application::getId() == APP_A6 ) {
				$steps = array_merge(array('expired-phone-packages', 'expired-postcheck-packages', 'activate-owner-disabled-with-packages'), $steps);
			}
			
			$result = Model_Cron::run($steps);
			
			foreach ( $result['counts'] as $stage => $count ) {
				$cli->colorize('purple')->out($stage, false);
				$cli->column(60)->colorize('green')->out($count);
				$cli->out();
			}
			
			$cli->out('------------------------------------------------------------');
			$cli->colorize('purple')->out('Total: ', false);
			$cli->column(60)->colorize('green')->out($result['count']);
			$cli->out('------------------------------------------------------------');
				
		} while ( $result['count'] > 0 );
		
		$apps_without_default_package = array(APP_A6, APP_A6_AT);
		
		if ( ! in_array(Cubix_Application::getId(), $apps_without_default_package) ) {
			$this->fixDefaultPackages();
		}
		$cli->reset();
		die;
	}

	public function fixDefaultPackages(/*array $escorts*/)
	{		
		
		/*foreach ( $escorts as $escort ) {
			$escort = $this->_db->fetchRow('SELECT id, agency_id, gender, country_id FROM escorts WHERE id = ?', $escort);
			if ( ! $escort ) continue;
			$escort = new Model_EscortItem($escort);
			$escort->processDefaultPackages();
		}*/

		
		if ( Cubix_Application::getId() == APP_ED ) {//Do not activate default package for escorts synced from other projects
			$config = Zend_Registry::get('system_config');
			$escorts = $this->_db->fetchAll('
				SELECT c.id, c.agency_id, c.gender, c.country_id, c.home_city_id FROM (
					SELECT
						e.id, e.agency_id, e.gender, e.country_id, COUNT(a.id) AS apps, e.home_city_id/*, e.is_super_old*/
					FROM applications a
					RIGHT JOIN order_packages op ON op.application_id = a.id AND op.status = 2 AND a.id = ' . Cubix_Application::getId() . '
					RIGHT JOIN escorts e ON e.id = op.escort_id
					RIGHT JOIN users u ON u.id = e.user_id
					WHERE (e.status & ' . ESCORT_STATUS_ACTIVE  . ' AND NOT e.status & ' . ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_ADMIN_DISABLED . ') AND
						u.sales_user_id <> ' . $config['syncManagerUserId'] . '
					GROUP BY e.id
				) c WHERE c.apps = 0
			');
		} else {
			$escorts = $this->_db->fetchAll('
				SELECT c.id, c.agency_id, c.gender, c.country_id, c.home_city_id FROM (
					SELECT
						e.id, e.agency_id, e.gender, e.country_id, COUNT(a.id) AS apps, e.home_city_id/*, e.is_super_old*/
					FROM applications a
					RIGHT JOIN order_packages op ON op.application_id = a.id AND op.status = 2 AND a.id = ' . Cubix_Application::getId() . '
					RIGHT JOIN escorts e ON e.id = op.escort_id
					WHERE (e.status & ' . ESCORT_STATUS_ACTIVE  . ' AND NOT e.status & ' . ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_ADMIN_DISABLED . ')
					GROUP BY e.id
				) c WHERE c.apps = 0
			');
		}
		
		foreach ( $escorts as $escort ) {
			echo "Escort id " . $escort->id . "\n\r";
			$escort = new Model_EscortItem($escort);
			$escort->processDefaultPackages();
		}
	}

	public function changePackageLogicAction()
	{
		// Monthly to Minimum
		$query1 = $this->_db->query('
			UPDATE order_packages as op 
			INNER JOIN escorts as e ON e.id = op.escort_id
			SET op.package_id = 125
			WHERE op.package_id = 102 and e.agency_id is not null and e.gender = 1 and e.country_id <> 68
		');

		$query2 = $this->_db->query('
			UPDATE escorts as e
			INNER JOIN order_packages as op ON e.id = op.escort_id
			SET e.active_package_id = 125, e.active_package_name = "Minimum"
			WHERE op.package_id = 125 and op.status = 2 and e.agency_id is not null and e.gender = 1
		');

		$escorts = $this->_db->fetchAll('
			SELECT e.id, e.agency_id
			FROM escorts e
			INNER JOIN order_packages op ON e.id = op.escort_id 
			WHERE (e.status & ' . ESCORT_STATUS_ACTIVE  . ' AND NOT e.status & ' . ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_ADMIN_DISABLED . ')  AND op.package_id = 125 AND op.status = 2 and e.agency_id is not null and e.gender = 1 and e.country_id <> 68
		');

		// Minimum to Monthly Package 
		foreach ($escorts as $escort) {
			if($this->countMonthlyPackageEscorts($escort->agency_id) < 3) {
				$query3 = $this->_db->query('
					UPDATE order_packages
					SET package_id = ' . Model_Packages::getMonthlyPackageId() . '
					WHERE escort_id = ' . $escort->id . ' AND package_id = 125 AND status = 2
				');
			} 
		}

		$query4 = $this->_db->query('
			UPDATE escorts as e
			INNER JOIN order_packages as op ON e.id = op.escort_id
			SET e.active_package_id = ' . Model_Packages::getMonthlyPackageId() . ', e.active_package_name = "Monthly Package"
			WHERE op.package_id = ' . Model_Packages::getMonthlyPackageId() . ' and op.status = 2 and e.agency_id is not null and e.gender = 1
		');

		$query5 = $this->_db->query('
			INSERT INTO order_package_products (order_package_id, product_id, is_optional, price)
			SELECT id,21,0,0 
			FROM order_packages 
			WHERE package_id <> 125');

	}

	public function changePackageLogicForEgukAction()
	{
		// Default packages(Bronze Normal & Promo International) to Minimum
		$query1 = $this->_db->query('
			UPDATE order_packages as op 
			INNER JOIN escorts as e ON e.id = op.escort_id
			SET op.package_id = 105
			WHERE (op.package_id = 43 or op.package_id = 44) and e.agency_id is not null and e.gender = 1 and e.country_id <> 216
		');

		$query2 = $this->_db->query('
			UPDATE escorts as e
			INNER JOIN order_packages as op ON e.id = op.escort_id
			SET e.active_package_id = 105, e.active_package_name = "Minimum"
			WHERE op.package_id = 105 and op.status = 2 and e.agency_id is not null and e.gender = 1
		');

		$escorts = $this->_db->fetchAll('
			SELECT e.id, e.agency_id
			FROM escorts e
			INNER JOIN order_packages op ON e.id = op.escort_id 
			WHERE (e.status & ' . ESCORT_STATUS_ACTIVE  . ' AND NOT e.status & ' . ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_ADMIN_DISABLED . ')  AND op.package_id = 105 AND op.status = 2 and e.agency_id is not null and e.gender = 1 and e.country_id <> 216
		');

		// Minimum to Monthly Package 
		foreach ($escorts as $escort) {
			if($this->countMonthlyPackageEscorts($escort->agency_id) < 3) {
				$query3 = $this->_db->query('
					UPDATE order_packages
					SET package_id = ' . Model_Packages::getMonthlyPackageId() . '
					WHERE escort_id = ' . $escort->id . ' AND package_id = 105 AND status = 2
				');
			} 
		}

		$query4 = $this->_db->query('
			UPDATE escorts as e
			INNER JOIN order_packages as op ON e.id = op.escort_id
			SET e.active_package_id = ' . Model_Packages::getMonthlyPackageId() . ', e.active_package_name = "Monthly Package"
			WHERE op.package_id = ' . Model_Packages::getMonthlyPackageId() . ' and op.status = 2 and e.agency_id is not null and e.gender = 1
		');

		$query5 = $this->_db->query('
			INSERT INTO order_package_products (order_package_id, product_id, is_optional, price)
			SELECT id,16,0,0 
			FROM order_packages 
			WHERE package_id <> 105');

	}

	public function countMonthlyPackageEscorts($agency_id)
	{
		return  $this->_db->fetchOne('
			SELECT
				COUNT(*) as count
			FROM escorts e
			INNER JOIN order_packages op ON op.escort_id = e.id
			INNER JOIN packages p ON op.package_id = p.id
			WHERE e.agency_id = ? AND op.status = ? AND p.id = ?
		', array($agency_id, Model_Packages::STATUS_ACTIVE, Model_Packages::getMonthlyPackageId()));

	}
	
	public function removeUnusedIvrTransfersAction()
	{
		$unused_orders = $this->_db->fetchAll('SELECT o.id, t.transfer_id FROM orders o INNER JOIN transfer_orders t ON t.order_id = o.id WHERE o.backend_user_id = ? AND o.status = ?  AND TIME_TO_SEC(TIMEDIFF(NOW(), o.order_date)) > ?', array(100, Model_Billing_Orders::STATUS_PAYMENT_DETAILS_RECEIVED, 60*30));
		try {
			$this->_db->beginTransaction();
			foreach($unused_orders as $order) {
				echo 'Deleting order_id -> ' . $order->id . "\n";
				$this->_db->beginTransaction();
				$this->_db->delete('orders', $this->_db->quoteInto('id = ?', $order->id));
				$this->_db->delete('transfers', $this->_db->quoteInto('id = ?', $order->transfer_id));
			}
			$this->_db->commit();
		} catch(Exception $e) {
			$this->_db->rollBack();
			throw $e;
		}
		
		die('Done');
		
	}
	
	public function zeroBalanceAction()
	{
		if ( ! defined('IS_CLI') || ! IS_CLI ) {
			die('Please run from command line');
		}
		$model = new Model_Billing_Transfers();
		echo('Setting balance to zero. Please wait...') . "\n";
		$model->zeroBalance();
		die('done');
	}
	
	public function removeNotPaidGotdAction()
	{
		$this->_db->query('
			DELETE FROM gotd_orders
			WHERE status = 1  AND TIME_TO_SEC(TIMEDIFF(NOW(), creation_date)) > ?', array(60*30));
		
		die('Done');
		
	}

    public function removeNotPaidGotdEgAction()
    {
        $this->_db->query('
			DELETE FROM gotd_orders
			WHERE status = 1  AND TIME_TO_SEC(TIMEDIFF(NOW(), creation_date)) > ?', array(60*2));

        die('Done');

    }
	
	public function activateGotdPhoneBillingAction()
	{
		$gotd_orders_to_activate = $this->_db->fetchAll('SELECT * FROM gotd_orders WHERE status = 2 AND DATE(activation_date) = DATE(NOW())');
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		
		$product = $client->call('Billing.getProduct', array(GIRL_OF_THE_DAY));

		
		$this->_db->beginTransaction;
		foreach($gotd_orders_to_activate as $gotd) {
			try {
				$this->_db->update('order_packages', array(
					'gotd_status'	=> 1,
					'gotd_activation_date' => $gotd->activation_date,
					'gotd_city_id'	=> $gotd->city_id
				), $this->_db->quoteInto('id = ?', $gotd->order_package_id));


				$is_alread_exists = $this->_db->fetchOne('SELECT TRUE FROM order_package_products WHERE order_package_id = ? AND product_id = ?', array($gotd->order_package_id, GIRL_OF_THE_DAY));
				if ( ! $is_alread_exists ) {
                    if ( in_array(Cubix_Application::getId(),array(APP_EG_CO_UK,APP_ED)) )
                    {
                       $escortHasPaidPackage = $this->_db->fetchOne('SELECT true FROM order_packages WHERE order_id IS NOT NULL AND id = ?', array($gotd->order_package_id));
                       
					   $product['price'] = ($escortHasPaidPackage) ? $product['price']  : GIRL_OF_THE_DAY_WITHOUT_PACKAGE_PRICE;
                    }
					$this->_db->insert('order_package_products', array(
						'order_package_id'	=> $gotd->order_package_id,
						'product_id'		=> GIRL_OF_THE_DAY,
						'is_optional'		=> 1,
						'price'				=> $product['price']
					));
				}


				$this->_db->update('gotd_orders', array('status' => 3), $this->_db->quoteInto('id = ?', $gotd->id));

				$this->_db->commit();
				echo 'Escort:' . $gotd->escort_id . ' GOTD moved to order_package_products' . "/n";
			} catch (Exception $e) {
				$this->_db->rollBack();
				var_dump($e);
			}
		}
		
		die('done');
	}
	
	public function removeNotPaidBoostProfilesAction()
	{
		$this->_db->query('
			DELETE FROM profile_boost_orders
			WHERE status = 1 AND TIME_TO_SEC(TIMEDIFF(NOW(), creation_date)) > ?', array(60*30));
		
		die('Done');
	}
	
	public function sendNotificationAboutExpiringPackagesAction()
	{
		set_time_limit(0);
		
		$packages = $this->_db->fetchAll("
			SELECT
				op.id, op.escort_id, op.status, op.application_id, previous_order_package_id, op.package_id, u.email AS user_email, bu.email AS sales_email,
				DATEDIFF(op.expiration_date, DATE(NOW()) ) AS days_left, e.showname
			FROM order_packages op
			INNER JOIN escorts e ON e.id = op.escort_id
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN backend_users bu ON bu.id = u.sales_user_id
			WHERE 
				(DATEDIFF(op.expiration_date, DATE(NOW()) ) = ? || DATEDIFF(op.expiration_date, DATE(NOW()) ) = ?) 
				AND op.status = ? 
				AND op.status <> ? 
		", array(5, 1, Model_Packages::STATUS_ACTIVE, Model_Billing_Packages::STATUS_SUSPENDED));

		foreach($packages as $package) {
			//Checking if has pending package. 
			//We must send email only if they don't have pending package

			$has_active_package = $this->_db->fetchOne('
				SELECT TRUE FROM order_packages WHERE escort_id = ? AND status = ?
			', array($package->escort_id, Model_Billing_Packages::STATUS_PENDING));

			if ( ! $has_active_package ) {
				Cubix_Email::sendTemplate('package_expiring_notification', $package->user_email, array('days_left' => $package->days_left, 'showname' => $package->showname));
			}
		}
		die('done');
	}

	public function sendPackageExpirationMsgAction()
	{
		set_time_limit(0);		

		$originator = Cubix_Application::getPhoneNumber(Cubix_Application::getId());
		$model_sms = new Model_SMS_Outbox();
		
		$packages = $this->_db->fetchAll("
			SELECT
				op.id, op.escort_id, op.status, op.application_id, previous_order_package_id, op.package_id, ep.contact_phone_parsed, bu.username AS sales_username, bu.id AS sales_person_id,
				DATEDIFF(op.expiration_date, DATE(NOW()) ) AS days_left, e.showname
			FROM order_packages op
			INNER JOIN escorts e ON e.id = op.escort_id
			INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN backend_users bu ON bu.id = u.sales_user_id
			WHERE 
				(DATEDIFF(op.expiration_date, DATE(NOW())) = ? || 
				DATEDIFF(op.expiration_date, DATE(NOW())) = ? ||
				DATEDIFF(op.expiration_date, DATE(NOW())) = ?)
				AND op.status = ? 
				AND op.status <> ?
				AND e.no_renew_sms = 0
		", array(5, 3, 1, Model_Packages::STATUS_ACTIVE, Model_Billing_Packages::STATUS_SUSPENDED));
		
		foreach($packages as $package) {
			//Checking if has pending package. 
			//We must send sms only if they don't have pending package

			$has_active_package = $this->_db->fetchOne('
				SELECT TRUE FROM order_packages WHERE escort_id = ? AND status = ?
			', array($package->escort_id, Model_Billing_Packages::STATUS_PENDING));

			if ( ! $has_active_package && $package->contact_phone_parsed ) {
				
				$template_title = $package->sales_username . '_' . $package->days_left;
				$message = $this->_db->fetchRow('
					SELECT template 
					FROM sms_templates WHERE title = ?
				', $template_title);

				$data = array(
					'escort_id' => $package->escort_id,
					'phone_number' => $package->contact_phone_parsed,
					'phone_from' => $originator,
					'text' => $message->template,
					'application_id' => Cubix_Application::getId(),
					'sender_sales_person' => $package->sales_person_id,
					'is_spam' => false
				);

				$model_sms->saveAndSend($data);
			}
		}

		die('done');
	}
	
	public function sendSmsAboutExpiringPackagesAction()
	{
		set_time_limit(0);
		$originator = Cubix_Application::getPhoneNumber(Cubix_Application::getId());
				
		$packages = $this->_db->fetchAll("
			SELECT
				op.id, op.escort_id, op.package_id, ep.contact_phone_parsed,
				TIMESTAMPDIFF(HOUR,NOW() ,CONVERT(op.expiration_date, DATETIME)) AS hours_left
			FROM order_packages op
			INNER JOIN escorts e ON e.id = op.escort_id
			INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id 
			WHERE 
				op.package_id IN (13,18,22,24,25) AND
				TIMESTAMPDIFF(HOUR,NOW() ,CONVERT(op.expiration_date, DATETIME)) = 12 
				AND op.status = ? 
				AND op.status <> ?
			UNION
			SELECT
				op.id, op.escort_id, op.package_id, ep.contact_phone_parsed,
				TIMESTAMPDIFF(HOUR,NOW() ,pp.expiration_date) AS hours_left
			FROM order_packages op
			INNER JOIN escorts e ON e.id = op.escort_id
			INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
			INNER JOIN phone_packages pp ON pp.order_package_id = op.id
			WHERE 
				op.package_id IN (19,20,23)
				AND	TIMESTAMPDIFF(HOUR,NOW(), pp.expiration_date) = 5 
				AND op.status = ? 
				AND op.status <> ?
		", array(Model_Packages::STATUS_ACTIVE, Model_Billing_Packages::STATUS_SUSPENDED,Model_Packages::STATUS_ACTIVE, Model_Billing_Packages::STATUS_SUSPENDED));
		
		foreach($packages as $package) {
			//Checking if has pending package. 
			//We must send sms only if they don't have pending package
			
			$has_active_package = $this->_db->fetchOne('
				SELECT TRUE FROM order_packages WHERE escort_id = ? AND status = ?
			', array($package->escort_id, Model_Billing_Packages::STATUS_PENDING));

			if ( ! $has_active_package ) {
				/* send sms */
				$message = Cubix_I18n::translateByLng('de', "sms_package_expiration", array('HOUR'=> $package->hours_left ));

				$model_sms = new Model_SMS_Outbox();
				$data = array(
					'escort_id' => $package->escort_id,
					'phone_number' => $package->contact_phone_parsed,
					'phone_from' => $originator,
					'text' => $message,
					'application_id' => Cubix_Application::getId(),
					'is_spam' => false
				);
				
				$model_sms->saveAndSend($data);
			}
		}
		die('done');
	}
	
	
	
	
	//Sending email to katie every day for unused transfers
	public function notUsedEpgTransfersAction()
	{
		$epg_payment = new Model_Billing_EpgGateway();
		
		$result = $epg_payment->getTransactions(date('Y-m-d', strtotime('-7 days')), date('Y-m-d', strtotime('-6 days')));
		$result = str_replace("Success\n", '', $result['ResultMessage']);
		$csvData = $result;
		$lines = explode("\n", $csvData);
		unset($lines[0]);
		$array = array();
		foreach ($lines as $line) {
			$array[] = str_getcsv($line);
		}
		$transactions = $array;
		
		$m_transfers = new Model_Billing_Transfers();
		$not_exists = array();
		
		
		ob_start();
		$df = fopen("php://output", 'w');
		
		foreach($transactions as $transaction) {
			$transaction_id = $transaction[5];
			$not_exist = $m_transfers->checkTransactionId($transaction_id);
			if ( $not_exist ) {
				$not_exists[] = $transaction_id;
				fputcsv($df, array($transaction_id));
			}
			
		}
		fclose($df);
		$csv = ob_get_clean();
		
		
		if ( count($not_exists) ) {
			$attachment = array(
				'item' => $csv,
				'type'	=> 'text/csv',
				'name'	=> 'not_used_epg_transfers.csv'
			);
			$body = 'From ' . date('Y-m-d', strtotime('-7 days')) . ' to ' . date('Y-m-d', strtotime('-6 days'));
			Cubix_Email::send('sharp17s@hushmail.com', 'Not used transfers for last 7 days', $body, false, null, null, array($attachment));
		}
		die('DONE');
	}
	
}
