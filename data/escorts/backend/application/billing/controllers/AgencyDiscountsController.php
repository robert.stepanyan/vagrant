<?php

class Billing_AgencyDiscountsController extends Zend_Controller_Action {
  protected $_model;

  public function init()
  {
    $this->_model = new Model_Billing_Discounts();
  }

  public function indexAction()
  {

  }

  public function dataAction()
  {
    $req = $this->_request;
    $filter = array();
    
		$data = $this->_model->getAll(
			$req->page,
			$req->per_page, 
			$filter, 
			$req->sort_field, 
			$req->sort_dir, 
			$count
		);
		
		die(json_encode(array('data' => $data, 'count' => $count)));
  }

  public function editDiscountAction()
  {
    $this->view->layout()->disableLayout();
    $req = $this->_request;
    $id = $req->id;

    $this->view->agency_discount = $this->_model->getById($id);

    if ($req->isPost()) {
      $value_from = $req->value_from;
      $value_to = $req->value_to;
      $discount = $req->discount;

      $validator = new Cubix_Validator();
			
			if (!$value_from || !is_numeric($value_from)) {
				$validator->setError('value_from', 'This field is required and must be number');
			}
			
			if ($value_to && !is_numeric($value_to)) {
				$validator->setError('value_to', 'This field is required and must be number');
      }

      if (!$value_from || !is_numeric($value_from)) {
				$validator->setError('discount', 'This field is required and must be number');
			}
      
      if ( $validator->isValid() ) {

        $this->_model->update(array(
          'value_from' => $value_from,
          'value_to' => $value_to,
          'discount' => $discount
        ));
      }

      die(json_encode($validator->getStatus()));
    }
  }

  public function addDiscountAction()
  {
    $this->view->layout()->disableLayout();
    $req = $this->_request;

    if ($req->isPost()) {
      $value_from = $req->value_from;
      $value_to = $req->value_to;
      $discount = $req->discount;

      $validator = new Cubix_Validator();
			
			if (!$value_from || !is_numeric($value_from)) {
				$validator->setError('value_from', 'This field is required and must be number');
			}
			
			if ($value_to && !is_numeric($value_to)) {
				$validator->setError('value_to', 'This field is required and must be number');
      }

      if (!$value_from || !is_numeric($value_from)) {
				$validator->setError('discount', 'This field is required and must be number');
			}
      
      if ( $validator->isValid() ) {

        $this->_model->add(array(
          'value_from' => $value_from,
          'value_to' => $value_to,
          'discount' => $discount
        ));
      }

      die(json_encode($validator->getStatus()));
    }
  }

  public function removeDiscountAction()
  {
    $id = $this->_request->id;
    $this->_model->remove($id);
    die();
  }
}