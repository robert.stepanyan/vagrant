<?php

class Billing_CronSmsController extends Zend_Controller_Action
{
	/**
	 *
	 * @var Zend_Db_Adapter_Abstract
	 */

	protected $_db;

	public function init()
	{
		$this->_db = Zend_Registry::get('db');
	}

	public function indexAction()
	{

		if ( ! defined('IS_CLI') || ! IS_CLI ) {
			die('Please run from command line');
		}

		set_time_limit(0);

		$cli = new Cubix_Cli();
		$pid_file = '/var/run/cron_sms_' . Cubix_Application::getId() . '.pid';
		
		$cli->setPidFile($pid_file);
		
		if ( $cli->isRunning() ) {
			die('Cron is already running, exitting...' . PHP_EOL);
		}else{
			$this->sendPackageExpirationMsgAction();
		}

		die;
	}

	public function sendPackageExpirationMsgAction()
	{
		set_time_limit(0);		

		$originator = Cubix_Application::getPhoneNumber(Cubix_Application::getId());
		
		$model_sms = new Model_SMS_Outbox();
		$bu_user = Zend_Auth::getInstance()->getIdentity();
        $sms_outbox = new Model_SMS_EmailOutbox();

		$packages = $this->_db->fetchAll("
			SELECT
				op.id, op.escort_id, op.status, op.application_id, previous_order_package_id, op.package_id, ep.contact_phone_parsed, bu.username AS sales_username, bu.id AS sales_person_id,
				DATEDIFF(op.expiration_date, DATE(NOW()) ) AS days_left, e.showname, u.id as userid, u.email
			FROM order_packages op
			INNER JOIN escorts e ON e.id = op.escort_id
			INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN backend_users bu ON bu.id = u.sales_user_id
			WHERE 
				(DATEDIFF(op.expiration_date, DATE(NOW())) = ? || 
				DATEDIFF(op.expiration_date, DATE(NOW())) = ?)
				AND op.status = ? 
				AND op.status <> ?
		", array(3, 1,  Model_Packages::STATUS_ACTIVE, Model_Billing_Packages::STATUS_SUSPENDED));
		

		foreach($packages as $package) {
			//Checking if has pending package. 
			//We must send sms only if they don't have pending package

			$has_active_package = $this->_db->fetchOne('
				SELECT TRUE FROM order_packages WHERE escort_id = ? AND status = ?
			', array($package->escort_id, Model_Billing_Packages::STATUS_PENDING));

			if ( ! $has_active_package ) {
				

				if( $package->days_left == 1 ){
					$template_title = 'ad expiration 24h';
				}elseif( $package->days_left == 3 ){
					$template_title = 'ad expiration 3d';
				}

				
				$message = $this->_db->fetchRow('
					SELECT template 
					FROM sms_templates WHERE title = ?
				', $template_title);

				$data = array(
					'escort_id' => $package->escort_id,
					'phone_number' => $package->contact_phone_parsed,
					'phone_from' => $originator,
					'timeleft' => $package->days_left,
					'text' => $message->template,
					'application_id' => Cubix_Application::getId(),
					'sender_sales_person' => $package->sales_person_id,
					'is_spam' => false
				);
				
				$model_sms->saveAndSend($data);

				$outbox_data = array(
                            'email' => $package->email,
                            'subject' => 'ad expiration '.$package->days_left.'days',
                            'text' => $message->template,
                            'backend_user_id' => $bu_user->id,
                            'date' => new Zend_Db_Expr('NOW()'),
                            'user_id' => $package->userid,
                            'application_id' => Cubix_Application::getId()
                );
                
                $sms_outbox->add($outbox_data);

	            //Cubix_Email::sendTemplate('cron_sms_notification', 'mihranhayrapetyan@gmail.com', $data);
		        
			}
		}

		die('done');
	}

}
