<?php

class Billing_VideoCallController extends Zend_Controller_Action
{
	/**
	 * @var Model_Billing_VideoCall
	 */
	protected $_model;
		
	public function init()
	{
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		if ( $bu_user->type != 'superadmin' && $bu_user->type != 'admin' ) {
			die('no permission');
		}
		$this->_model = new Model_Billing_VideoCall();
	}
	
	public function indexAction() 
	{
		
	}	
	
	public function dataAction()
	{
		
		$req = $this->_request;
		
		$filter = array(
			'name' => trim($req->name),
			'status' => $req->status,
			'escort_id' => trim($req->escort_id),
            'date_from'=> intval($req->date_from),
            'date_to'=> intval($req->date_to),
			'showname' => trim($req->showname),
            'more_info' => trim($req->id)
		);
        if ( Cubix_Application::getId() == APP_A6 ) {
            $_SESSION['csv_filters'] = serialize($filter);
        }
		$data = $this->_model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);

		if ( $count ) {
			foreach ( $data as $i => $item ) {
				$data[$i]->status = Model_Billing_VideoCall::$STATUS_LABELS[$item->status];
				if(!$data[$i]->username){
					$data[$i]->username = $data[$i]->customer;
				} 
//				$data[$i]->more_info = Model_Billing_VideoCall::getTransfer($item->id);
				/*if ( $data[$i]->user_agent){
					$user_agent = Cubix_UserAgentParser::parse_user_agent($data[$i]->user_agent);
					$data[$i]->user_agent = $user_agent['platform'] .' '. $user_agent['browser'].' v' . $user_agent['version'];
				}*/
			}
		}

		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}


	public function totalAmountAction(){
        $this->view->layout()->disableLayout();
        $ids = $this->_getParam('ids');
        $this->view->ids = $ids;
    }

    public function calcTotalAmountAction(){
        $req = $this->_request;
        $filter = array(
            'name' => trim($req->name),
            'transaction_id' => trim($req->transaction_id),
            'skype' => trim($req->skype),
            'email' => trim($req->email),
            'status' => $req->status,
            'escort_id' => trim($req->escort_id),
            'date_from'=> intval($req->date_from),
            'date_to'=> intval($req->date_to),
            'showname' => trim($req->showname),
            'more_info' => trim($req->id)
        );
        $this->view->layout()->disableLayout();

        $total_amount = $this->_model->getTotalAmount($filter);
        $this->view->total_amount = $total_amount;
    }

    public function sendSmsAction(){

        $request_id = $this->_getParam('id');
        if (in_array(Cubix_Application::getId(), array(APP_EF))){

            $model = new Model_Billing_VideoCall();
            $data =  $model->getVideoCallRequestById($request_id);


            $duration = $data->duration;
            $phone = $data->phone;
            $name = $data->name;
            $skype = $data->skype;
            $escort_id = $data->escort_id;

            if($data->sms_text != ''){
                $text = $data->sms_text;
            }else{
                $text = "Nuova richiesta video skype: $name skype id $skype ha pagato per te una chiamata di $duration minuti. Accetta la sua richiesta e organizza la tua chiamata! Dettagli nel tuo account su escortforumit.xxx. info@escortforumit.xxx";

            }
			
            // Get from number
            $originator = $phone_from = Cubix_Application::getPhoneNumber(Cubix_Application::getId());

            $sms_data = array();
            $model_sms = new Model_SMS_Outbox();
            $sms_data['phone_from'] = $originator;
            $sms_data['application_id'] = Cubix_Application::getId();
            $sms_data['phone_number'] = $phone;
            $sms_data['escort_id'] = $escort_id;
            $sms_data['text'] = $text;
            $sms_data['is_video_call'] = 1;

            $model_sms->saveAndSend($sms_data);

            $model_pm = new Cubix_PrivateMessaging('member', 525597); // Skype Video Booking;
            $model_pm->sendMessage($text, 'escort', $escort_id, false, false);

            echo json_encode(array('status' => 'success'));
            die;



        }else{
            echo json_encode(array('status' => 'error'));
            die;
        }


    }
    public function sendEmailCustomerAction(){

        $request_id = $this->_getParam('id');
        if (in_array(Cubix_Application::getId(), array(APP_EF))){

            $model = new Model_Billing_VideoCall();
            $data =  $model->getVideoCallRequestById($request_id);


            $duration = $data->duration;
            $skype = $data->skype;
            $showname = $data->showname;
            $customer_email = $data->email;



            //SENDING EMAIL TO CUSTOMER
            $email_content = Cubix_I18n::translateByLng('it', 'video_call_cutomer_email_text', array(
                'skype' => $skype,
                'duration' => $duration,
                'escort' => $showname
            ));

            Cubix_Email::sendTemplate('universal_message', $customer_email , array(
                'subject' => 'a tua videochiamata Skype con '. $showname,
                'message' => $email_content
            ));

            echo json_encode(array('status' => 'success'));
            die;

        }else{
            echo json_encode(array('status' => 'error'));
            die;
        }


    }


	public function toggleActiveAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->_model->toggle($id);
	}

	public function createAction()
	{
        $this->view->layout()->disableLayout();
		$req = $this->_request;

		if ( $req->isPost() ) {

			$validator = new Cubix_Validator();

			if ( ! $req->escort_id ) {
				$validator->setError('escort_id', 'Required');
			}
			
			if ( ! $req->duration ) {
				$validator->setError('duration', 'Required');
			}
			if ( ! $req->amount ) {
				$validator->setError('amount', 'Required');
			}

			if ( $validator->isValid() )
			{
				$status = (int) $req->status;

				if( (int) $req->status == 1){
                    $status = 0;
                }

				$data = array(
					'escort_id' => $req->escort_id,
					'duration' => $req->duration,
                    'amount' => $req->amount,
					'sms_text' => $req->sms_text,
					'comment' => $req->comment,
					'status' => $status,
				);

				$this->_model->insert($data);
			}

			die(json_encode($validator->getStatus()));
		}


	}

	public function editAction()
	{
        if (in_array(Cubix_Application::getId(), array(APP_EF, APP_A6))){
            $this->view->layout()->disableLayout();
            $request_id = $this->_getParam('id' );
            $model = new Model_Billing_VideoCall();

            if ( $this->_request->isPost() ){
                $req = $this->_request;

                $validator = new Cubix_Validator();

                if ( ! $req->escort_id ) {
                    $validator->setError('escort_id', 'Required');
                }
                if ( ! $req->duration ) {
                    $validator->setError('duration', 'Required');
                }
                if ( ! $req->amount ) {
                    $validator->setError('amount', 'Required');
                }

                if ( $validator->isValid() ) {
                    $status = (int)$req->status;

                    if ((int)$req->status == 1) {
                        $status = 0;
                    }

                    $update_data = array(
                        'escort_id' => $req->escort_id,
                        'duration' => $req->duration,
                        'amount' => $req->amount,
                        'sms_text' => $req->sms_text,
                        'status' => $status,
                        'comment' => $req->comment,
                    );
					
                    $model->updateVideoCallRequest($request_id, $update_data);
                    echo json_encode(array('status' => 'success'));
                    die;
                }
				else{
					die(json_encode($validator->getStatus()));
				}
            }else{
                $data =  $model->getVideoCallRequestById($request_id);
                $this->view->data = $data;
            }
        }else{
            $this->_redirect($this->view->baseUrl() . '/billing/video-call');
        }
	}
	
	public function removeAction()
	{
        $this->view->layout()->disableLayout();
        $request_id = $this->_getParam('id' );
        $model = new Model_Billing_VideoCall();
        $model->removeVideoCall($request_id);
        echo "deleted";
		die;
	}

	public function detailsAction()
    {
        $id = $this->_getParam('id' );
        $data = Model_Billing_VideoCall::getTransfer( $id );

        $requestData = Model_Billing_VideoCall::getRequest( $id );
       
        $this->view->member_email = $requestData->member_email;
        $this->view->member_username = $requestData->member_username;
        
        $this->view->layout()->disableLayout();
        $user_agent =  Cubix_UserAgentParser::parse_user_agent( $data->di );
        $this->view->device_info = $user_agent['platform'] .' '. $user_agent['browser'].' v' . $user_agent['version'];
        $this->view->data = $data;

      

    }

    public function exportAction()
    {
        set_time_limit(0);
        ini_set('memory_limit', '2048M');

        $req = $this->_request;
        if ( Cubix_Application::getId() == APP_A6 ) {
            $sessionFilter = unserialize( $_SESSION['csv_filters'] );
        }
        $data = new Cubix_Form_Data($this->_request);

        $data->setFields(array(
            'transfer_type_id' => 'int',
            'transaction_id' => '',
            'mtcn' => '',
            'var_symbol' => '',
            'first_name' => '',
            'last_name' => '',
            'pull_person_id' => '',
            'app_id' => 'int',
            'sales_user_id' => 'int',
            'sales_user_id_real' => 'int',
            'status' => 'int',
            'filter_by' => '',
            'date_from' => 'int',
            'date_to' => 'int',
            'user_id' => '',
            'showname' => '',
            'skype' => '',
            'name' => '',
            'escort_id' => 'int',
            'email' => '',
            'agency_name' => '',
            'sys_order_id' => ''
        ));
        $data = $data->getData();

        $bu_user = Zend_Auth::getInstance()->getIdentity();

        // <editor-fold defaultstate="collapsed" desc="Filter">
        $filter = array();

        if ( $sessionFilter['date_from']) {
            $filter[' DATE(r.creation_date) >= DATE(FROM_UNIXTIME(?))'] = $sessionFilter['date_from'];
        }
        if ( $sessionFilter['date_to']) {
            $filter[' DATE(r.creation_date) <= DATE(FROM_UNIXTIME(?))'] = $sessionFilter['date_to'];
        }

        if ( $sessionFilter['showname'] ) {
            $filter['e.showname LIKE ?'] = $sessionFilter['showname'] . '%';
        }

        if ( $sessionFilter['escort_id'] ) {
            $filter['e.id = ?'] = $sessionFilter['escort_id'];
        }

        if ( $sessionFilter['email'] ) {
            $filter['r.email LIKE ?'] = $sessionFilter['email'] . '%';
        }

        if ( $sessionFilter['transaction_id'] ) {
            $filter['vct.transaction_id = ?'] = $sessionFilter['transaction_id'];
        }

        if ( $sessionFilter['name'] ) {
            $filter['r.name = ?'] = $sessionFilter['name'];
        }

        if ( $sessionFilter['skype'] ) {
            $filter['r.skype = ?'] = $sessionFilter['skype'];
        }

        if ( $sessionFilter['status'] ) {
            $filter['r.status = ?'] = $sessionFilter['status'];
        }

        $page = $req->page ? (int) $req->page : null;
        $per_page = $req->per_page ? (int) $req->per_page : null;
        $sort = $req->sort_field ? $req->sort_field : 'vct.date_transfered';
        $dir = $req->dir ? $req->dir : 'DESC';

        $data = $this->_model->getDataForExport(
            $filter,
            $page,
            $per_page,
            $sort,
            $dir
        );


        if ( in_array(Cubix_Application::getId(), array(APP_A6))  ) {
            $filename = 'video-calls_' . hexdec(uniqid()) . '.csv';
            $handle = fopen('video-calls/' . $filename, 'w');
        } else {
            header('Content-Type: text/csv');
            header('Content-Disposition: attachment; filename=video-calls.csv');

            $handle = fopen('php://output', 'w');
        }

        $headers = ["Showname", "Date Transfered", "Payer name", "Amount","CC holder", "CC number", "CC exp month", "TransactionId","Paid amount",
            "EscortId","Escort Email","Member Email","Client Skype","Member Name"];
        fputcsv($handle, $headers);

        foreach ( $data['data'] as $order ) {
            $order = (array) $order;

            $arr = array(
                $order['showname'], $order['date_transfered'], $order['payer_name'], $order['amount'], $order['cc_holder'],
                $order['cc_number'], $order['cc_exp_month'], $order['transaction_id'], $order['paid_amount'], $order['escort_id'],
                $order['escortEmail'], $order['memberEmail'], $order['client_skype'], $order['memberName']
            );

           fputcsv($handle, $arr);

        }
        fclose($handle);

        if (file_exists('video-calls/' . $filename)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename('video-calls/' . $filename).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize('video-calls/' . $filename));
            readfile('video-calls/' . $filename);

            unlink('video-calls/' . $filename);
        }
        exit;
    }
}
