<?php

class Billing_OnlineBillingController extends Zend_Controller_Action {
	
	public function init()
	{	
		
		//file_put_contents('/tmp/online-billing-request.log', var_export($req->transaction_id, true), FILE_APPEND);
		
		if ( ! defined('IS_CLI') || ! IS_CLI ) {
			$host = str_replace('backend.', '', $_SERVER['HTTP_HOST']);
			$app_data = Cubix_Application::getByHost($host);

			Cubix_Application::setId($app_data->id);
		}
	}
	
	//<editor-fold defaultstate="collapsed" desc="EPG PAYMENT">
	
	public function runPaymentTokenizerAction()
	{
		
		$tokens = Model_Billing_OnlineBilling::getPaymentGatewayTokens('epg');
		if ( ! count($tokens) ) {
			echo 'There are no tokens to check.' . "\n";
		}
		
		$epg_payment = new Model_Billing_EpgGateway();
				
		if ( $_GET['is_test'] == 1 ) {
			$callback = array(
				'transaction_time' => '2012-05-10 13:48:35',
				'transaction_id' => 'TEST' . rand(10000, 15000),
				'reference'   => 48322,
				'hash'		 => 'zzzzzz',
				'status_id'   => '200',
				'currency'   => 'EUR',
				'amount'   => '90.00',
				'first_name'   => 'Bjorn Zwaaneveld',
				'last_name'   => 'Bjorn Zwaaneveld',
				'address'   => 'Bitovska 12',
				'zipcode'   => 14000,
				'city'    => 'Praha 4',
				'country_code'  => 'CZ',
				'email'    => 'blabla@bla.bla',
				'phone_number'  => '465465431',
				'ip_address'  => '192.168.0.1',
				'card_name'   => 'Bjorn Zwaaneveld',
				'card_number'  => '552885******6252',
				'exp_date'   => '12/12',
				'sha1'    => 'afsdfkajsdkfjksdfjsdjflkj'
			);

			$onlineBilling = new Model_Billing_OnlineBilling($callback, false, false);

			$onlineBilling->addCallbackLog();

			if ( $onlineBilling->isTransactionIdExists($callback['transaction_id']) ) {
				$onlineBilling->updateLogData('Failure. transaction_id:' . $callback['transaction_id'] . ' already exists.');
			}
			
			$onlineBilling->payV3();
			die;
			
		}
		
		
		
		
		foreach($tokens as $token) {
			$result = $epg_payment->getTransactionStatus($token->token);
			
			echo $result['ResultMessage'] . "\n";
			if ( $result['ResultStatus'] == "OK") {
								
				list($transaction_type, $ref, $hash) = explode('-', $result['TransactionReference']);
								
				switch($transaction_type) {
					case 'SC' :
						$callback = array (
							'transaction_time'	=> $result['TransactionDate'],
							'transaction_id'	=> $result['TransactionId'],
							'reference'			=> $ref,
							'hash'				=> $hash,
							'status'			=> "200",
							'currency'			=> $result['Currency'],
							'amount'			=> $result['Amount'],
							'first_name'		=> $result['FirstName'],
							'last_name'			=> $result['LastName'],
							'address'			=> $result['Address'],
							'zipcode'			=> $result['Zip'],
							'city'				=> $result['City'],
							'country_code'		=> $result['Country'],
							'email'				=> $result['Email'],
							'phone_number'		=> $result['Phone'],
							'ip_address'		=> '',
							'card_name'			=> '',
							'card_number'		=> '',
							'exp_date'			=> '',
							'sha1'				=> sha1(Model_Billing_OnlineBilling::$secret_prefixes[Cubix_Application::getId()] . $result['Amount'] . 'EUR' . 'SC-' . $ref)
						);

						$onlineBilling = new Model_Billing_OnlineBilling($callback, false, false);

						$onlineBilling->addCallbackLog();

						if ( $onlineBilling->isTransactionIdExists($callback['transaction_id']) ) {
							$onlineBilling->updateLogData('Failure. transaction_id:' . $callback['transaction_id'] . ' already exists.');
							continue;
						}
											
						$onlineBilling->pay();
						
						
						try {
							$config = Zend_Registry::get('system_config');
							if ( $config['smsNotificationNumbers'] &&  strlen($config['smsNotificationNumbers']) ) {
								$to_numbers = explode(',', $config['smsNotificationNumbers']);
								$text = $result['FirstName'] . ' ' . $result['LastName'] . ' just paid for ad.';
								$this->_sendSMSToNumbers($to_numbers, $text);
							}
						} catch(Exception $ex) {
							echo('error in sending sms');
						}
						
						break;
						case 'SC3' :
						$callback = array (
							'transaction_time'	=> $result['TransactionDate'],
							'transaction_id'	=> $result['TransactionId'],
							'reference'			=> $ref,
							'status'			=> "200",
							'hash'				=> $hash,
							'currency'			=> $result['Currency'],
							'amount'			=> $result['Amount'],
							'first_name'		=> $result['FirstName'],
							'last_name'			=> $result['LastName'],
							'address'			=> $result['Address'],
							'zipcode'			=> $result['Zip'],
							'city'				=> $result['City'],
							'country_code'		=> $result['Country'],
							'email'				=> $result['Email'],
							'phone_number'		=> $result['Phone'],
							'ip_address'		=> '',
							'card_name'			=> '',
							'card_number'		=> '',
							'exp_date'			=> '',
							'sha1'				=> sha1(Model_Billing_OnlineBilling::$secret_prefixes[Cubix_Application::getId()] . $result['Amount'] . 'EUR' . 'SC3-' . $ref)
						);

						$onlineBilling = new Model_Billing_OnlineBilling($callback);

						$onlineBilling->addCallbackLog();

						if ( $onlineBilling->isTransactionIdExists($callback['transaction_id']) ) {
							$onlineBilling->updateLogData('Failure. transaction_id:' . $callback['transaction_id'] . ' already exists.');
							continue;
						}
											
						$onlineBilling->payV3();
						
						if(Cubix_Application::getId() != APP_A6) {
							try {
								$config = Zend_Registry::get('system_config');
								if ( $config['smsNotificationNumbers'] &&  strlen($config['smsNotificationNumbers']) ) {
									$to_numbers = explode(',', $config['smsNotificationNumbers']);
									$text = $result['FirstName'] . ' ' . $result['LastName'] . ' just paid for ad.';
									$this->_sendSMSToNumbers($to_numbers, $text);
								}
							} catch(Exception $ex) {
								echo('error in sending sms');
							}
						}
						
						
						break;
					case 'gotd' :
						$additional_info = array (
							'transaction_id'	=> $result['TransactionId'],
							'payment_system'    => 'epg'
						);
						$application_id = Cubix_Application::getId();
						$client = new Cubix_Api_XmlRpc_Client();
						$response = $client->call('Billing.confirmGotd', array($ref, $application_id, $additional_info));
						
						if ( $response ) {
							$activation_date = implode("\n", explode(',', $response['activation_date']));
							$body = 'Escort "' . $response['showname'] . '" ID:' . $response['id'] . ' has just payed for GOTD IN ' . $response['city'] . "\n" . $activation_date;
						} else {
							$body = 'Error occured in confirmation. gotd_order_id: ' . $id;
						}

						if ( $response['sales_email'] && strlen($response['sales_email']) ) {
							Cubix_Email::send($response['sales_email'], 'Gotd Paid', $body);
						}

						if($application_id == APP_A6) {
							if ( $response['email'] && strlen($response['email']) ) {
								$cities = explode(',', $response['group_city']);
								$dates = explode(',', $response['activation_date']);
								foreach ($dates as $i => $date) {
									Cubix_Email::sendTemplate('gotd_purchase_confirm', $response['email'], array(
										'city' => $cities[$i],
										'date' => $date
									));
								}
							}
						}
						
						break;
					case 'CA':
						$t_model = new Model_ClassifiedAds();
						
						$tmp = explode('_', $ref);
						$ad_id = $tmp[0];
						$package_id = $tmp[1];
						
						$package = $t_model->getPackageById($package_id);
						
						$tr_data = array(
							'ad_id' => $ad_id,
							'status' => Model_ClassifiedAds::AD_TRANSFER_STATUS_CONFIRMED,
							'first_name' => $result['FirstName'],
							'last_name' => $result['LastName'],
							'transaction_id' => $result['TransactionId'],
							'amount' => $result['Amount'],
							'application_id' => Cubix_Application::getId(),
							//'backend_user_id' => $be_user->id,
							'date_confirmed' => new Zend_Db_Expr('NOW()'),
							'package_name' => $package->name,
							'package_price' => $package->price,
							'package_period' => $package->period,
							'is_paid' => 1
						);

						if(Cubix_Application::getId() == APP_A6) {
							$tr_data['gateway'] = Model_ClassifiedAds::AD_TRANSFER_GATEWAY_EPG;
						}
						$t_model->makePremium($ad_id, $package->period);
						
						$t_model->addTransfer($tr_data);					
						
						break;
					case 'EP':
						$req = $this->_request;
						
						list($user_id, $extension, $opid) = explode('|', $ref);
						$req->setParam('user_id', $user_id);
						$req->setParam('extension', $extension);
						$req->setParam('opid', $opid);						
						
		
						/*$callback = array(
							'transaction_time' => '2012-05-10 13:48:35',
							'transaction_id' => 938931,
							'reference'   => 247324,
							'status_id'   => '200',
							'currency'   => 'EUR',
							'amount'   => '310.00',
							'first_name'   => 'Bjorn Zwaaneveld',
							'last_name'   => 'Bjorn Zwaaneveld',
							'address'   => 'Bitovska 12',
							'zipcode'   => 14000,
							'city'    => 'Praha 4',
							'country_code'  => 'CZ',
							'email'    => 'blabla@bla.bla',
							'phone_number'  => '465465431',
							'ip_address'  => '192.168.0.1',
							'card_name'   => 'Bjorn Zwaaneveld',
							'card_number'  => '552885******6252',
							'exp_date'   => '12/12',
							'sha1'    => 'c0e8222ca0e281f00d5dde4d25ba189c043767fc'
						);*/

						$callback = array(							
							'first_name'		=> $result['FirstName'],
							'last_name'			=> $result['LastName']
						);

						/*$onlineBilling = new Model_Billing_OnlineBilling($callback);

						$onlineBilling->addCallbackLog();*/

						/*if (sha1(Model_Billing_OnlineBilling::SECRET_PREFIX . number_format($req->amount, 2) . 'EUR' . 'EP-' . $req->cleaned_reference) != $req->sha1 ) {
							$onlineBilling->updateLogData('Failure. Sha1 doesn\'t match.');
							die;
						}

						if ( $callback['status_id'] != 200 ) {
							die('STATUS 300');
						}*/

						$m_package = new Model_Billing_Packages();

						$op_id = $req->opid;
						$package = $m_package->get($op_id);

						$extend_products = $package->getExtendProducts();

						$days = 3;

						if ( $this->_request->extension == Model_Billing_Packages::DL_PRODUCT_PLUS_6_DAYS || $this->_request->extension == Model_Billing_Packages::DP_PRODUCT_PLUS_6_DAYS || $this->_request->extension == Model_Billing_Packages::GT_PRODUCT_PLUS_6_DAYS ) {
							$days = 6;
						} else if ( $this->_request->extension == Model_Billing_Packages::DL_PRODUCT_PLUS_9_DAYS || $this->_request->extension == Model_Billing_Packages::DP_PRODUCT_PLUS_9_DAYS ) {
							$days = 9;
						}

						$ext_data = array(
							'order_id' => $package->order_id,
							'order_package_id' => $op_id,
							'days' => $days,
							'opt_product' => $this->_request->extension,
							'product_price' => $result['Amount'],
							'order_price' => $package->order_price,
							'package_price' => $package->price
						);

						$data = array(
							'user_id' => $req->user_id, 
							'orders' => array($package->order_id),
							'transfer_type_id' => 3,
							'date_transfered' => time(),
							'amount' => $result['Amount'],
							'backend_user_id' => Model_Billing_OnlineBilling::$backend_users[Cubix_Application::getId()]['id'],
							'transaction_id' => $result['TransactionId'],
							'application_id' => Cubix_Application::getId(),
							'first_name' => $result['FirstName'],
							'last_name' => $result['LastName'],
							'status' => Model_Billing_Transfers::STATUS_PENDING,
							'is_self_checkout' => 1
						);
						

						$_model = new Model_Billing_Transfers();
						$o_model = new Model_Billing_Orders();

						$o_model->extendOrder($ext_data);
						$_model->save($transfer = new Model_Billing_TransferItem($data));

						Cubix_SyncNotifier::notify($package->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PACKAGE_EXPIRATION_UPDATED, array('id' => $op_id));
						
						break;
					case 'vc' :
												
						$body = 'Skype video call has just been payed request_id: ' . $ref;
						
						$callback = array (
							'transaction_id'	=> $result['TransactionId'],
							'reference'			=> $ref,
							'currency'			=> $result['Currency'],
							'amount'			=> $result['Amount'],
							'first_name'		=> $result['FirstName'],
							'last_name'			=> $result['LastName'],
							'address'			=> $result['Address'],
							'zipcode'			=> $result['Zip'],
							'city'				=> $result['City'],
							'country_code'		=> $result['Country'],
							'email'				=> $result['Email'],
							'phone_number'		=> $result['Phone'],
							'data'              => $body,
							'log_data'          => json_encode($result) 	
						);
						
						$model_video_call = new Model_Billing_VideoCall();
						
						$tr_data = array(
							'request_id' => $ref,
							'payer_name' => $result['FirstName']. '-'. $result['LastName'],
							'transaction_id' => $result['TransactionId'],
							'amount' => $result['Amount'],
							'payment_system'    => 'epg',
							'date_transfered' => new Zend_Db_Expr('NOW()'),
							'cc_holder'			=> $result['CreditcardType'],
							'cc_number'		=> $result['CreditCardBIN'] . 'XXXXXXXX' . $result['CreditcardLast4'],
							'cc_exp_month'		=> $result['CreditcardExpiryMonth'],
							'cc_exp_year'		=> $result['CreditcardExpiryYear'],
							
						);
						Model_Billing_OnlineBilling::insertCallbackLog($callback);
						$model_video_call->addTransfer($tr_data);
						break;
					case 'cam':
						
						$body = 'TOKEN for CAMS has just been payed request_id: ' . $ref;
						
						$callback = array (
							'transaction_id'	=> $result['TransactionId'],
							'reference'			=> $ref,
							'currency'			=> $result['Currency'],
							'amount'			=> $result['Amount'],
							'first_name'		=> $result['FirstName'],
							'last_name'			=> $result['LastName'],
							'address'			=> $result['Address'],
							'zipcode'			=> $result['Zip'],
							'city'				=> $result['City'],
							'country_code'		=> $result['Country'],
							'email'				=> $result['Email'],
							'phone_number'		=> $result['Phone'],
							'data'              => $body,
							'log_data'          => json_encode($result) 	
						);
						
						$model_tokens = new Model_Billing_Tokens();
												
						$tr_data = array(
							'request_id' => $ref,
							'payer_name' => $result['FirstName']. '-'. $result['LastName'],
							'transaction_id' => $result['TransactionId'],
							'amount' => $result['Amount'],
							'payment_system'    => 'epg',
							'date_transfered' => new Zend_Db_Expr('NOW()'),
							'cc_holder'			=> $result['CreditcardType'],
							'cc_number'		=> $result['CreditCardBIN'] . 'XXXXXXXX' . $result['CreditcardLast4'],
							'cc_exp_month'		=> $result['CreditcardExpiryMonth'],
							'cc_exp_year'		=> $result['CreditcardExpiryYear'],
							
						);
						Model_Billing_OnlineBilling::insertCallbackLog($callback);
						$model_tokens->addTransfer($tr_data);
						break;
					default;
					
				}
				
				
				Model_Billing_OnlineBilling::removePaymentGatewayToken($token->id);
				
				echo $callback['first_name'] . ' ' . $callback['last_name'] . ' payment confirmed' . "\n";
			} else {
				if ( $token->attempt >= 30 )  {
					//Model_Billing_OnlineBilling::removePaymentGatewayToken($token->id);
				} else {
					Model_Billing_OnlineBilling::updatePaymentGatewayTokenAttempt($token->id);
				}
			}
			
		}
		
		
		die;
	}
	
	// </editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="ECARDON PAYMENT">
	
	public function runEcardonTokenizerAction()
	{
		if (defined('IS_CLI') && IS_CLI) {
			$cli = new Cubix_Cli();
			$pid_file = '/var/run/ecardon-tokenizer' . Cubix_Application::getId() . '.pid';
			$cli->setPidFile($pid_file);
		
			if ( $cli->isRunning() ) {
				$cli->out('Action is already running... exitting...');
				exit(1);
			}
		}
		
		
		$tokens = Model_Billing_OnlineBilling::getPaymentGatewayTokens('ecardon');
		if ( ! count($tokens) ) {
			echo 'There are no tokens to check.' . "\n";
		}
		
		$app_id = Cubix_Application::getId();
		/*if($app_id == APP_EF){
			$start_time = microtime(true);
			$que_name = 'ha.payment_callbacks';
			$uniq_id = uniqid();
			$connection = new Cubix_PhpAmqpLib_Connection_AMQPStreamConnection(
					'bear.rmq.cloudamqp.com',
					5672,
					'rcgaoxjv', 
					'NPszYKCFWRyKFSqa-Z8jR-ckXYBNd97B', 
					'rcgaoxjv'
				);

			$channel = $connection->channel();
		}*/
			
		foreach($tokens as $token) {
				
			//<editor-fold defaultstate="collapsed" desc="FOR TESTING PURPOSES">
			if ( $token->attempt == -99 ) {
				$callback = array(
					'transaction_time' => '2012-05-10 13:48:35',
					'transaction_id' => 'TEST-161616161616',
					'reference'   => $token->user_id,
					'hash'		=> 'p15c4q',
					'status_id'   => '200',
					'currency'   => 'EUR',
					'amount'   => '100',
					'first_name'   => 'Bjorn Zwaaneveld',
					'last_name'   => 'Bjorn Zwaaneveld',
					'address'   => 'Bitovska 12',
					'zipcode'   => 14000,
					'city'    => 'Praha 4',
					'country_code'  => 'CZ',
					'email'    => 'blabla@bla.bla',
					'phone_number'  => '465465431',
					'ip_address'  => '192.168.0.1',
					'card_name'   => 'Bjorn Zwaaneveld',
					'card_number'  => '552885******6252',
					'exp_date'   => '12/12',
					'sha1'    => 'afsdfkajsdkfjksdfjsdjflkj',
					'log_data' => 'no log data just test'
				);
										
				$onlineBilling = new Model_Billing_OnlineBilling($callback, false, false);

				$onlineBilling->addCallbackLog();
				
				if ( $onlineBilling->isTransactionIdExists($callback['transaction_id']) ) {
					$onlineBilling->updateLogData('Failure. transaction_id:' . $callback['transaction_id'] . ' already exists.');
				}

				$onlineBilling->pay('ecardon');
				Model_Billing_OnlineBilling::updatePaymentGatewayTokenAttempt($token->id);
				echo ' TEST Token activated ' . $token->token . "\n";
				CONTINUE;
			}
			// </editor-fold>
			
			$alt_payment = new Model_Billing_EcardonGateway();
			$result = $alt_payment->getStatus($token->token);
			$result_dec = json_decode($result);
			echo ' Checking Token ' . $token->token . "\n";
			
			if(isset($result_dec->result) && ($result_dec->result->code == '000.000.000' || $result_dec->result->code == '000.100.110'))
			{
				/*if($app_id == APP_EF){
					$end_time = microtime(true);
					$time = round($end_time - $start_time, 4);

					$rabbit_msg = array(
						'uid' => $uniq_id, 
						'host' => $_SERVER['HTTP_HOST'],
						'ip' => $_SERVER['LOCAL_ADDR'],
						'type' => 'ecardon',
						'user_id' => $token->user_id,
						'token' => $token->token,
						'exec_time' => $time,
						'date' => date("Y-m-d H:i:s")
					);
				}
				
				$msg = new Cubix_PhpAmqpLib_Message_AMQPMessage(json_encode($rabbit_msg));
				$channel->basic_publish($msg, '', $que_name);*/
				
				list($transaction_type, $ref, $hash) = explode('-', $result_dec->merchantTransactionId);
				$transaction_date = date('Y-m-d H:i:s', strtotime($result_dec->timestamp));   

				switch($transaction_type) {
					case 'SC' :
						$callback = array (
							'transaction_time'	=> $transaction_date,
							'transaction_id'	=> $result_dec->id,
							'reference'			=> $ref,
							'hash'				=> $hash,
							'status'			=> "200",
							'currency'			=> $result_dec->currency,
							'amount'			=> $result_dec->amount,
							'first_name'		=> $result_dec->card->holder,
							'last_name'			=> "",
							'address'			=> 'no address',
							'zipcode'			=> '',
							'city'				=> '',
							'country_code'		=> '',
							'email'				=> '',
							'phone_number'		=> '',
							'ip_address'		=> '',
							'card_name'			=> $result_dec->paymentBrand,
							'card_number'		=> $result_dec->card->bin . 'XXXXXXXX' . $result_dec->card->last4Digits,
							'exp_date'			=> $result_dec->card->expiryMonth .'/'.$result_dec->card->expiryYear,
							'sha1'				=> sha1(Model_Billing_OnlineBilling::$secret_prefixes[Cubix_Application::getId()] . $result_dec->amount . 'EUR' . 'SC-' . $ref),
							'log_data'          => $result
						);

						$onlineBilling = new Model_Billing_OnlineBilling($callback);

						$onlineBilling->addCallbackLog();

						if ( $onlineBilling->isTransactionIdExists($callback['transaction_id'], 'ecardon') ) {
							$onlineBilling->updateLogData('Failure. transaction_id:' . $callback['transaction_id'] . ' already exists.');
							continue;
						}
						$onlineBilling->pay('ecardon');

						try {
							$config = Zend_Registry::get('system_config');

							if ( $config['smsNotificationNumbers'] &&  strlen($config['smsNotificationNumbers']) ) {
								$to_numbers = explode(',', $config['smsNotificationNumbers']);
								$text = 'Ecardon SC for user_id--' . $ref . ' just paid for ad.';
								$this->_sendSMSToNumbers($to_numbers, $text);
							}
						} catch(Exception $ex) {
							echo('error in sending sms');
						}

						break;
					case 'gotd' :
						$additional_info = array (
							'transaction_id'	=> $result_dec->id,
							'payment_system'    => 'ecardon',
							'first_name'		=>	$result_dec->card->holder
						);
						$application_id = Cubix_Application::getId();
						$client = new Cubix_Api_XmlRpc_Client();
						$response = $client->call('Billing.confirmGotd', array($ref, $application_id, $additional_info));

						if ( $response && is_array($response) ) {
							$activation_date = implode("\n", explode(',', $response['activation_date']));
							$body = $ref.' Escort "' . $response['showname'] . '" ID:' . $response['id'] . ' has just payed for GOTD IN ' . $response['city'] . "\n" . $activation_date;
						} else {
							$body = 'Error occured in GOTD confirmation: ' . $response;
						}

						$callback = array (
							'transaction_time'	=> $transaction_date,
							'transaction_id'	=> $result_dec->id,
							'reference'			=> $ref,
							'currency'			=> $result_dec->currency,
							'amount'			=> $result_dec->amount,
							'first_name'		=> $result_dec->card->holder,
							'last_name'			=> "",
							'log_data'          => $result,
							'data'               => $body
						);


						Model_Billing_OnlineBilling::insertCallbackLog($callback);
						
						if ( $response['sales_email'] && strlen($response['sales_email']) ) {
							Cubix_Email::send($response['sales_email'], 'Gotd Paid', $body);
						}

						if($application_id == APP_A6) {
							if ( $response['email'] && strlen($response['email']) ) {
								$cities = explode(',', $response['group_city']);
								$dates = explode(',', $response['activation_date']);
								foreach ($dates as $i => $date) {
									Cubix_Email::sendTemplate('gotd_purchase_confirm', $response['email'], array(
										'city' => $cities[$i],
										'date' => $date
									));
								}
							}
						}

						break;
					case 'pb' :
						$additional_info = array (
							'transaction_id'	=> $result_dec->id,
							'payment_system'    => 'ecardon',
							'first_name'		=>	$result_dec->card->holder
						);
						
						$application_id = Cubix_Application::getId();
						$client = new Cubix_Api_XmlRpc_Client();

						$response = $client->call('OnlineBillingV2.confirmProfileBoost', array($ref, $application_id, $additional_info));

						if ( $response ) {
							$activation_date = implode("\n", explode(',', $response['activation_date']));
							$body = 'Escort "' . $response['showname'] . '" ID:' . $response['id'] . ' has just payed for BOOST IN ' . $response['city'] . "\n" . $activation_date;
						} else {
							$body = 'Error occured in confirmation. profile_boost_order_id: ' . $response['id'];
						}

						$callback = array (
							'transaction_time'	=> $transaction_date,
							'transaction_id'	=> $result_dec->id,
							'reference'			=> $ref,
							'currency'			=> $result_dec->currency,
							'amount'			=> $result_dec->amount,
							'first_name'		=> $result_dec->card->holder,
							'last_name'			=> "",
							'log_data'          => $result,
							'data'               => $body
						);		

						Model_Billing_OnlineBilling::insertCallbackLog($callback);

						if ( $response['sales_email'] && strlen($response['sales_email']) ) {
							Cubix_Email::send($response['sales_email'], 'Profile Boost Paid', $body);
						}

						break;
					case 'vc' :
												
						$body = 'Skype video call has just been payed request_id: ' . $ref;
						
						$callback = array (
							'transaction_time'	=> $transaction_date,
							'transaction_id'	=> $result_dec->id,
							'reference'			=> $ref,
							'currency'			=> $result_dec->currency,
							'amount'			=> $result_dec->amount,
							'first_name'		=> $result_dec->card->holder,
							'last_name'			=> "",
							'log_data'          => $result,
							'data'               => $body
						);		
						
						$model_video_call = new Model_Billing_VideoCall();
												
						$tr_data = array(
							'request_id' => $ref,
							'date_transfered'	=> $transaction_date,
							'transaction_id'	=> $result_dec->id,
							'amount'			=> $result_dec->amount,
							'payment_system'    => 'ecardon',
							'payer_name'		=> $result_dec->card->holder,
							'cc_holder'			=> $result_dec->paymentBrand,
							'cc_number'		    => $result_dec->card->bin . 'XXXXXXXX' . $result_dec->card->last4Digits,
							'cc_exp_month'		=> $result_dec->card->expiryMonth,
							'cc_exp_year'		=> $result_dec->card->expiryYear,
						);
						
						Model_Billing_OnlineBilling::insertCallbackLog($callback);
						$model_video_call->addTransfer($tr_data);
						
						break;
					case 'cam':
						$body = 'TOKEN for EF-CAMS has just been payed request_id: ' . $ref;
						
						$callback = array (
							'transaction_time'	=> $transaction_date,
							'transaction_id'	=> $result_dec->id,
							'reference'			=> $ref,
							'currency'			=> $result_dec->currency,
							'amount'			=> $result_dec->amount,
							'first_name'		=> $result_dec->card->holder,
							'last_name'			=> "",
							'log_data'          => $result,
							'data'               => $body
						);		
						
						$model_tokens = new Model_Billing_Tokens();
												
						$tr_data = array(
							'request_id' => $ref,
							'date_transfered'	=> $transaction_date,
							'transaction_id'	=> $result_dec->id,
							'amount'			=> $result_dec->amount,
							'payment_system'    => 'ecardon',
							'payer_name'		=> $result_dec->card->holder,
							'cc_holder'			=> $result_dec->paymentBrand,
							'cc_number'		    => $result_dec->card->bin . 'XXXXXXXX' . $result_dec->card->last4Digits,
							'cc_exp_month'		=> $result_dec->card->expiryMonth,
							'cc_exp_year'		=> $result_dec->card->expiryYear,
						);
						
						Model_Billing_OnlineBilling::insertCallbackLog($callback);
						$model_tokens->addTransfer($tr_data);
						
						break;
					default;
				}

				echo  'Ecardon '.$transaction_type.' for user_id --' . $ref . ' payment confirmed' . "\n";	
				$return = true;
			}
			else{
				$return = false;
			}
			
			if($return){	
				Model_Billing_OnlineBilling::removePaymentGatewayToken($token->id);
			} 
			else {
				if ( $token->attempt >= 30 )  {
					//Model_Billing_OnlineBilling::removePaymentGatewayToken($token->id);
				} else {
					Model_Billing_OnlineBilling::updatePaymentGatewayTokenAttempt($token->id);
				}
			}
		}
		/*if($app_id == APP_EF){
			$channel->close();
			$connection->close();
		}*/
		die;
	}
	
	// </editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="2000 Charge PAYMENT">
	public function run2000ChargeTokenizerAction()
	{
		
		$tokens = Model_Billing_OnlineBilling::getPaymentGatewayTokens('2000charge');
		if ( ! count($tokens) ) {
			echo 'There are no tokens to check.' . "\n";
		}
		
		$charge_payment = new Cubix_2000charge_Transaction();		
		foreach($tokens as $token) {
			
			try{
				echo $token->token .' --- ';
				$result = $charge_payment->get($token->token);
				$status = $result->status;
				echo  $result->status . "\n";
			}catch(Exception $ex) {
				echo $ex->getMessage() . "\n";
				$status = 'Not Found';
			}
			
			$app_id = Cubix_Application::getId();
			/*if($app_id == APP_EF){
				$start_time = microtime(true);
				$que_name = 'ha.payment_callbacks';
				$uniq_id = uniqid();
				$connection = new Cubix_PhpAmqpLib_Connection_AMQPStreamConnection(
						'bear.rmq.cloudamqp.com',
						5672,
						'rcgaoxjv', 
						'NPszYKCFWRyKFSqa-Z8jR-ckXYBNd97B', 
						'rcgaoxjv'
					);

				$channel = $connection->channel();

				$end_time = microtime(true);
				$time = round($end_time - $start_time, 4);

				$rabbit_msg = array(
					'uid' => $uniq_id, 
					'host' => $status,
					'ip' => $_SERVER['LOCAL_ADDR'],
					'type' => '2000charge',
					'user_id' => $token->user_id,
					'token' => $token->token,
					'exec_time' => $time,
					'date' => date("Y-m-d H:i:s")
				);

				$msg = new Cubix_PhpAmqpLib_Message_AMQPMessage(json_encode($rabbit_msg));
				$channel->basic_publish($msg, '', $que_name);
				$channel->close();
				$connection->close();
			}*/
			
			if ( $status == "Funded") {
								
				list($transaction_type, $ref, $hash) = explode('-', $result->merchantTransactionId);
				$transaction_date = date('Y-m-d H:i:s', strtotime($result->payment->created));   				
				switch($transaction_type) {
					case 'SC' :
						$callback = array (
							'transaction_time'	=> $transaction_date,
							'transaction_id'	=> $result->id,
							'reference'			=> $ref,
							'hash'				=> $hash,
							'status'			=> "200",
							'currency'			=> $result->currency,
							'amount'			=> $result->amount / 100,
							'first_name'		=> $result->payment->holder,
							'last_name'			=> '',
							'email'				=> $result->customer->email,
							'phone_number'		=> '',
							'ip_address'		=> $result->payment->ipAddress,
							'card_name'			=> '',
							'card_number'		=> '',
							'exp_date'			=> '',
							'sha1'				=> sha1(Model_Billing_OnlineBilling::$secret_prefixes[Cubix_Application::getId()] . number_format($result->amount / 100, 2, '.', '') . 'EUR' . 'SC-' . $ref),
							'log_data'          => json_encode($result)
						);
						
						$onlineBilling = new Model_Billing_OnlineBilling($callback, false, false);

						$onlineBilling->addCallbackLog();

						if ( $onlineBilling->isTransactionIdExists($callback['transaction_id'], '2000charge') ) {
							$onlineBilling->updateLogData('Failure. transaction_id:' . $callback['transaction_id'] . ' already exists.');
							continue;
						}
											
						$onlineBilling->pay('2000charge');
						
						
						try {
							$config = Zend_Registry::get('system_config');
							if ( $config['smsNotificationNumbers'] &&  strlen($config['smsNotificationNumbers']) ) {
								$to_numbers = explode(',', $config['smsNotificationNumbers']);
								$text = $result['FirstName'] . ' ' . $result['LastName'] . ' just paid for ad.';
								$this->_sendSMSToNumbers($to_numbers, $text);
							}
						} catch(Exception $ex) {
							echo('error in sending sms');
						}
						
						break;
					case 'SC3' :
						
						$callback = array (
							'transaction_time'	=> $transaction_date,
							'transaction_id'	=> $result->id,
							'reference'			=> $ref,
							'hash'				=> $hash,
							'status'			=> "200",
							'currency'			=> $result->currency,
							'amount'			=> $result->amount / 100,
							'first_name'		=> $result->payment->holder,
							'last_name'			=> '',
							'email'				=> $result->customer->email,
							'phone_number'		=> '',
							'ip_address'		=> $result->payment->ipAddress,
							'card_name'			=> '',
							'card_number'		=> '',
							'exp_date'			=> '',
							'sha1'				=> sha1(Model_Billing_OnlineBilling::$secret_prefixes[Cubix_Application::getId()] . number_format($result->amount / 100, 2, '.', '') . 'EUR' . 'SC3-' . $ref),
							'log_data'          => json_encode($result)
						);
						
						$onlineBilling = new Model_Billing_OnlineBilling($callback);

						$onlineBilling->addCallbackLog();

						if ( $onlineBilling->isTransactionIdExists($callback['transaction_id'], '2000charge') ) {
							$onlineBilling->updateLogData('Failure. transaction_id:' . $callback['transaction_id'] . ' already exists.');
							continue;
						}
						
						$onlineBilling->payV3('2000charge');
						break;
					case 'gotd' :
						$additional_info = array (
							'transaction_id'	=> $result->id,
							'payment_system'    => '2000charge'
						);
						$application_id = Cubix_Application::getId();
						$client = new Cubix_Api_XmlRpc_Client();
						$response = $client->call('Billing.confirmGotd', array($ref, $application_id, $additional_info));
						
						if ( $response ) {
							$activation_date = implode("\n", explode(',', $response['activation_date']));
							$body = 'Escort "' . $response['showname'] . '" ID:' . $response['id'] . ' has just payed for GOTD IN ' . $response['city'] . "\n" . $activation_date;
						} else {
							$body = 'Error occured in confirmation. gotd_order_id: ' . $id;
						}

						$callback = array (
							'transaction_time'	=> $transaction_date,
							'transaction_id'	=> $result->id,
							'reference'			=> $ref,
							'currency'			=> $result->currency,
							'amount'			=> $result->amount / 100,
							'first_name'		=> $result->payment->holder,
							'last_name'			=> "",
							'log_data'          => json_encode($result),
							'data'               => $body
						);

						Model_Billing_OnlineBilling::insertCallbackLog($callback);
						
						
						if ( $response['sales_email'] && strlen($response['sales_email']) ) {
							Cubix_Email::send($response['sales_email'], 'Gotd Paid', $body);
						}

						if($application_id == APP_A6) {
							if ( $response['email'] && strlen($response['email']) ) {
								$cities = explode(',', $response['group_city']);
								$dates = explode(',', $response['activation_date']);
								foreach ($dates as $i => $date) {
									Cubix_Email::sendTemplate('gotd_purchase_confirm', $response['email'], array(
										'city' => $cities[$i],
										'date' => $date
									));
								}
							}
						}
						
						break;
					case 'pb' :
						$additional_info = array (
							'transaction_id'	=> $result->id,
							'payment_system'    => '2000charge',
							'first_name'		=> $result->payment->holder
							
						);
						
						$application_id = Cubix_Application::getId();
						$client = new Cubix_Api_XmlRpc_Client();

						$response = $client->call('OnlineBillingV2.confirmProfileBoost', array($ref, $application_id, $additional_info));

						if ( $response ) {
							$activation_date = implode("\n", explode(',', $response['activation_date']));
							$body = 'Escort "' . $response['showname'] . '" ID:' . $response['id'] . ' has just payed for BOOST IN ' . $response['city'] . "\n" . $activation_date;
						} else {
							$body = 'Error occured in confirmation. profile_boost_order_id: ' . $response['id'];
						}

						$callback = array (
							'transaction_time'	=> $transaction_date,
							'transaction_id'	=> $result->id,
							'reference'			=> $ref,
							'currency'			=> $result->currency,
							'amount'			=> $result->amount / 100,
							'first_name'		=> $result->payment->holder,
							'last_name'			=> "",
							'log_data'          => json_encode($result),
							'data'               => $body
						);

						Model_Billing_OnlineBilling::insertCallbackLog($callback);

						if ( $response['sales_email'] && strlen($response['sales_email']) ) {
							Cubix_Email::send($response['sales_email'], 'Profile Boost Paid', $body);
						}

						break;
					
					case 'CA':
						$t_model = new Model_ClassifiedAds();
						
						$tmp = explode('_', $ref);
						$ad_id = $tmp[0];
						$package_id = $tmp[1];
						
						$package = $t_model->getPackageById($package_id);
						
						$tr_data = array(
							'ad_id' => $ad_id,
							'status' => Model_ClassifiedAds::AD_TRANSFER_STATUS_CONFIRMED,
							'first_name' => $result['FirstName'],
							'last_name' => $result['LastName'],
							'transaction_id' => $result['TransactionId'],
							'amount' => $result['Amount'],
							'application_id' => Cubix_Application::getId(),
							//'backend_user_id' => $be_user->id,
							'date_confirmed' => new Zend_Db_Expr('NOW()'),
							'package_name' => $package->name,
							'package_price' => $package->price,
							'package_period' => $package->period,
							'is_paid' => 1
						);

						if(Cubix_Application::getId() == APP_A6) {
							$tr_data['gateway'] = Model_ClassifiedAds::AD_TRANSFER_GATEWAY_EPG;
						}
						$t_model->makePremium($ad_id, $package->period);
						
						$t_model->addTransfer($tr_data);					
						
						break;
					
					default;
					
				}
				
				
				Model_Billing_OnlineBilling::removePaymentGatewayToken($token->id);
				
				echo $callback['first_name'] . ' ' . $callback['last_name'] . ' payment confirmed' . "\n";
			} else {
				if ( $token->attempt >= 30 )  {
					//Model_Billing_OnlineBilling::removePaymentGatewayToken($token->id);
				} else {
					Model_Billing_OnlineBilling::updatePaymentGatewayTokenAttempt($token->id);
				}
			}
			
		}
		
		
		die;
	}
	// </editor-fold>
	 
	//<editor-fold defaultstate="collapsed" desc="NETBANX PAYMENT">
	public function netbanxCallbackScAction()
	{
		$req = json_decode(file_get_contents("php://input"), true);
		//file_put_contents('/tmp/online-billing-request.log', var_export($req, true), FILE_APPEND);
		$ref = $req['transaction.merchantRefNum'];
		list($method, $ref) = explode('-', $ref);

		$callback = array(
			'transaction_time'	=> time(),
			'transaction_id'	=> $req['transaction.confirmationNumber'],
			'reference'			=> $ref,
			'currency'			=> $req['transaction.currencyCode'],
			'amount'			=> $req['transaction.amount'] / 100,
			'first_name'		=> 'not provided',
			'last_name'			=> 'not provided',
			'address'			=> 'not provided',
			'zipcode'			=> 'not provided',
			'city'				=> 'not provided',
			'country_code'		=> 'not provided',
			'email'				=> 'not provided',
			'phone_number'		=> 'not provided',
			'ip_address'		=> 'not provided',
			'card_name'			=> $req['transaction.card.brand'],
			'card_number'		=> $req['transaction.card.lastDigits'],
			'exp_date'			=> $req['transaction.card.expiry'],
			'netbanx_token'		=> $req['profile.paymentToken'],
			'netbanx_profile_id'		=> $req['profile.id']
		);

		$onlineBilling = new Model_Billing_OnlineBilling($callback, false, false);

		$onlineBilling->addCallbackLog();

		if ( $onlineBilling->isTransactionIdExists($callback['transaction_id']) ) {
			$onlineBilling->updateLogData('Failure. transaction_id:' . $callback['transaction_id'] . ' already exists.');
			die;
		}

		if ( $req['transaction.status'] != 'success' ) {
			die;
		}

		$onlineBilling->pay();

		header("HTTP/1.1 200 OK");
		die('200 ok');
	}


	public function netbanxCallbackGotdAction()
	{
		$req = json_decode(file_get_contents("php://input"), true);
		
		$ref = $req['transaction.merchantRefNum'];
		list($method, $ref) = explode('-', $ref);
		
		$additional_info = array (
			'transaction_id'	=> $req['transaction.confirmationNumber'],
			'gateway'	=> 'netbanx',
			'card_number'		=> $req['transaction.card.lastDigits'],
			'card_name'			=> $req['transaction.card.brand'],
			'token'				=> $req['profile.paymentToken'],
			'netbanx_profile_id'		=> $req['profile.id']
		);
		$application_id = Cubix_Application::getId();
		Cubix_Api_XmlRpc_Client::setApiKey(Cubix_Application::getById()->api_key);
		$client = new Cubix_Api_XmlRpc_Client();
		$response = $client->call('Billing.confirmGotd', array($ref, $application_id, $additional_info));
		
		if ( $response ) {
			$activation_date = implode("\n", explode(',', $response['activation_date']));
			$body = 'Escort "' . $response['showname'] . '" ID:' . $response['id'] . ' has just payed for GOTD IN ' . $response['city'] . "\n" . $activation_date;
		} else {
			$body = 'Error occured in confirmation. gotd_order_id: ' . $id;
		}
		
		if ( $response['sales_email'] && strlen($response['sales_email']) ) {
			Cubix_Email::send($response['sales_email'], 'Gotd Paid', $body);
		}

		if($application_id == APP_A6) {
			if ( $response['email'] && strlen($response['email']) ) {
				$cities = explode(',', $response['group_city']);
				$dates = explode(',', $response['activation_date']);
				foreach ($dates as $i => $date) {
					Cubix_Email::sendTemplate('gotd_purchase_confirm', $response['email'], array(
						'city' => $cities[$i],
						'date' => $date
					));

				}
			}
		}

		header("HTTP/1.1 200 OK");
		die('200 ok');
	}
	
	
	public function netbanxCallbackBoostProfileAction()
	{
		$req = json_decode(file_get_contents("php://input"), true);
		
		$ref = $req['transaction.merchantRefNum'];
		list($method, $ref) = explode('-', $ref);
		
		$additional_info = array (
			'transaction_id'	=> $req['transaction.confirmationNumber'],
			'gateway'	=> 'netbanx',
			'card_number'		=> $req['transaction.card.lastDigits'],
			'card_name'			=> $req['transaction.card.brand'],
			'token'				=> $req['profile.paymentToken'],
			'netbanx_profile_id'		=> $req['profile.id']
		);
		
		$application_id = Cubix_Application::getId();
		Cubix_Api_XmlRpc_Client::setApiKey(Cubix_Application::getById()->api_key);
		$client = new Cubix_Api_XmlRpc_Client();
		$response = $client->call('OnlineBillingV2.confirmProfileBoost', array($ref, $application_id, $additional_info));
		
		if ( $response ) {
			$activation_date = implode("\n", explode(',', $response['activation_date']));
			$body = 'Escort "' . $response['showname'] . '" ID:' . $response['id'] . ' has just payed for BOOST IN ' . $response['city'] . "\n" . $activation_date;
		} else {
			$body = 'Error occured in boost profile confirmation. profile_boost_order_id: ' . $id;
		}

		if ( $response['sales_email'] && strlen($response['sales_email']) ) {
			Cubix_Email::send($response['sales_email'], 'Profile Boost Paid', $body);
		}

		header("HTTP/1.1 200 OK");
		die('200 ok');
	}
	
	
	public function netbanxCallbackExpoAction()
	{
		$req = json_decode(file_get_contents("php://input"), true);
		
		$ref = $req['transaction.merchantRefNum'];
		list($method, $ref) = explode('-', $ref);
		
		$additional_info = array (
			'transaction_id'	=> $req['transaction.confirmationNumber'],
			'card_number'		=> $req['transaction.card.lastDigits'],
			'card_name'			=> $req['transaction.card.brand']
		);
		
		Cubix_Api_XmlRpc_Client::setApiKey(Cubix_Application::getById()->api_key);
		$client = new Cubix_Api_XmlRpc_Client();
		$response = $client->call('OnlineBillingV2.confirmExpoOrder', array($ref, $additional_info));
		
		if ( $response ) {
			$body = 'Escort "' . $response['showname'] . '" ID:' . $response['id'] . ' has just payed for Milan Expo.
			Start Date: ' . $response['start_date'] . ', End Date: ' . $response['end_date'];
		} else {
			$body = 'Error occured in milan expo confirmation. expo_order_id: ' . $ref;
		}
		
		if ( $response['sales_email'] && strlen($response['sales_email']) ) {
			Cubix_Email::send($response['sales_email'], 'Milan Expo Paid', $body);
		}
		
		header("HTTP/1.1 200 OK");
		die('200 ok');
	}

	// </editor-fold>
	 
	//<editor-fold defaultstate="collapsed" desc="BITCOIN PAYMENT">
	
	public function bitcoinCallbackScAction()
	{
		$req_json = file_get_contents('php://input');
		$payResponse = json_decode($req_json);

		$bitcoin_payment = new Model_Billing_BitcoinPay();
		$bitcoin_payment->saveCallbacks($payResponse);

		if($payResponse->status == 'confirmed'){

			$transaction_data = $bitcoin_payment->getTransaction($payResponse->payment_id);
			$tr_decoded = json_decode($transaction_data);
			list($transaction_type, $user_id, $hash) = explode('-', $tr_decoded->data->reference);

			switch($transaction_type) {
				case 'SC' :

					$callback = array(
						'transaction_time'	=> date('Y-m-d H:i:s'),
						'transaction_id'	=> $tr_decoded->data->payment_id,
						'reference'			=> $user_id,
						'hash'				=> $hash,
						'currency'			=> $tr_decoded->data->currency,
						'amount'			=> $tr_decoded->data->price,
						'settled_amount'    => $tr_decoded->data->settled_amount,
						'first_name'		=> 'not provided',
						'last_name'			=> 'not provided',
						'address'			=> 'not provided',
						'zipcode'			=> 'not provided',
						'city'				=> 'not provided',
						'country_code'		=> 'not provided',
						'email'				=> 'not provided',
						'phone_number'		=> 'not provided',
						'ip_address'		=> 'not provided',
						'sha1'				=> sha1(Model_Billing_OnlineBilling::$secret_prefixes[Cubix_Application::getId()] . $tr_decoded->data->price . 'EUR' . 'SC-' . $user_id),
						'log_data'          => var_export($tr_decoded->data, true)
					);

					$onlineBilling = new Model_Billing_OnlineBilling($callback, false, true);

					$onlineBilling->addCallbackLog();

					if ( $onlineBilling->isTransactionIdExists($callback['transaction_id'], 'bitcoin') ) {
						$onlineBilling->updateLogData('Failure. transaction_id:' . $callback['transaction_id'] . ' already exists.');
						die;
					}

					$onlineBilling->pay('bitcoin');
					break;
				case 'gotd' :

					$additional_info = array (
						'transaction_id'	=> $tr_decoded->data->payment_id,
						'payment_system'    => 'bitcoin',
						'first_name'		=> 'BTC',
					);

					$application_id = Cubix_Application::getId();
					Cubix_Api_XmlRpc_Client::setApiKey(Cubix_Application::getById()->api_key);
					$client = new Cubix_Api_XmlRpc_Client();
					$response = $client->call('Billing.confirmGotd', array($user_id, $application_id, $additional_info));

					if ( $response ) {
						$activation_date = implode("\n", explode(',', $response['activation_date']));
						$body = 'Escort "' . $response['showname'] . '" ID:' . $response['id'] . ' has just payed for GOTD IN ' . $response['city'] . "\n" . $activation_date;
					} else {
						$body = 'Error occured in confirmation. gotd_order_id: ' . $response['id'];
					}

					$callback = array (
						'transaction_time'	=> date('Y-m-d H:i:s'),
						'transaction_id'	=> $tr_decoded->data->payment_id,
						'reference'			=> $user_id,
						'currency'			=> $tr_decoded->data->currency,
						'amount'			=> $tr_decoded->data->price,
						'settled_amount'    => $tr_decoded->data->settled_amount,
						'log_data'          => var_export($tr_decoded->data, true)
					);

					Model_Billing_OnlineBilling::insertCallbackLog($callback);

					if ( $response['sales_email'] && strlen($response['sales_email']) ) {
						Cubix_Email::send($response['sales_email'], 'Gotd Paid', $body);
					}

					if($application_id == APP_A6) {
						if ( $response['email'] && strlen($response['email']) ) {
							$cities = explode(',', $response['group_city']);
							$dates = explode(',', $response['activation_date']);
							foreach ($dates as $i => $date) {
								Cubix_Email::sendTemplate('gotd_purchase_confirm', $response['email'], array(
									'city' => $cities[$i],
									'date' => $date
								));
							}
						}
					}

					break;
				case 'pb' :

					$additional_info = array (
						'transaction_id'	=> $tr_decoded->data->payment_id,
						'payment_system'    => 'bitcoin',
						'first_name'		=> 'BTC',
					);

					$application_id = Cubix_Application::getId();
					Cubix_Api_XmlRpc_Client::setApiKey(Cubix_Application::getById()->api_key);
					$client = new Cubix_Api_XmlRpc_Client();
					$response = $client->call('OnlineBillingV2.confirmProfileBoost', array($user_id, $application_id, $additional_info));

					if ( $response ) {
						$activation_date = implode("\n", explode(',', $response['activation_date']));
						$body = 'Escort "' . $response['showname'] . '" ID:' . $response['id'] . ' has just payed for BOOST IN ' . $response['city'] . "\n" . $activation_date;
					} else {
						$body = 'Error occured in confirmation. profile_boost_order_id: ' . $response['id'];
					}

					$callback = array (
						'transaction_time'	=> date('Y-m-d H:i:s'),
						'transaction_id'	=> $tr_decoded->data->payment_id,
						'reference'			=> $user_id,
						'currency'			=> $tr_decoded->data->currency,
						'amount'			=> $tr_decoded->data->price,
						'settled_amount'    => $tr_decoded->data->settled_amount,
						'log_data'          => var_export($tr_decoded->data, true)
					);

					Model_Billing_OnlineBilling::insertCallbackLog($callback);

					if ( $response['sales_email'] && strlen($response['sales_email']) ) {
						Cubix_Email::send($response['sales_email'], 'Profile Boost Paid', $body);
					}
					break;
			}
		}
	}
	// </editor-fold>
	 
	//<editor-fold defaultstate="collapsed" desc="Coinsome PAYMENT">
	
	public function coinsomeCallbackAction()
	{
		$application_id = Cubix_Application::getId();
		Cubix_Api_XmlRpc_Client::setApiKey(Cubix_Application::getById()->api_key);
		
		try{
			$order_id = intval($this->_request->id);
			
			if($order_id == 0 ){
				header("HTTP/1.1 200 OK");
				die('ok');
			} 
			
			$coinsome_model = new Cubix_Coinsome();
			$transaction_json = $coinsome_model->getTransaction($order_id);
			$transaction = json_decode($transaction_json);
			$coinsome_model->saveLog($order_id, $transaction->status, $transaction_json);
			$coinsome_model->updateOrder($order_id, $transaction->status, $transaction_json);
			
			if($transaction->status == Cubix_Coinsome::ORDER_PAID){

				$order = $coinsome_model->getOrder($order_id);
				
				switch($order->payment_type) {
					case 'SC' :

						$callback = array(
							'transaction_time'	=> $transaction->created_at,
							'transaction_id'	=> $order_id,
							'reference'			=> $order->user_id,
							'hash'				=> $order->hash,
							'currency'			=> 'BTC',
							'amount'			=> $transaction->amountUsd,
							'first_name'		=> 'not provided',
							'last_name'			=> 'not provided',
							'address'			=> $transaction->address,
							'zipcode'			=> 'not provided',
							'city'				=> 'not provided',
							'country_code'		=> 'not provided',
							'email'				=> 'not provided',
							'phone_number'		=> 'not provided',
							'ip_address'		=> 'not provided',
							'log_data'          => $transaction_json
						);

						$onlineBilling = new Model_Billing_OnlineBilling($callback, false, false);

						$onlineBilling->addCallbackLog();

						if ( $onlineBilling->isTransactionIdExists($callback['transaction_id'], 'coinsome') ) {
							$onlineBilling->updateLogData('Failure. transaction_id:' . $callback['transaction_id'] . ' already exists.');
							die;
						}

						$onlineBilling->pay('coinsome');
						break;
					case 'gotd' :

						$additional_info = array (
							'transaction_id'	=> $order_id,
							'payment_system'    => 'coinsome',
							'first_name'		=> 'BTC',
						);

						$client = new Cubix_Api_XmlRpc_Client();
						$response = $client->call('Billing.confirmGotd', array($order->gotd_orders_ids, $application_id, $additional_info));

						if ( $response ) {
							$activation_date = implode("\n", explode(',', $response['activation_date']));
							$body = 'Escort "' . $response['showname'] . '" ID:' . $response['id'] . ' has just paid for GOTD IN ' . $response['city'] . "\n" . $activation_date;
						} else {
							$body = 'Error occured in confirmation. gotd_order_id: ' . $response['id'];
						}

						$callback = array (
							'transaction_time'	=> $transaction->created_at,
							'transaction_id'	=> $order_id,
							'reference'			=> $order->user_id,
							'currency'			=> 'BTC',
							'amount'			=> $transaction->amountUsd,
							'log_data'          => $transaction_json
						);

						Model_Billing_OnlineBilling::insertCallbackLog($callback);

						if ( $response['sales_email'] && strlen($response['sales_email']) ) {
							Cubix_Email::send($response['sales_email'], 'Gotd Paid', $body);
						}

						if($application_id == APP_A6) {
							if ( $response['email'] && strlen($response['email']) ) {
								$cities = explode(',', $response['group_city']);
								$dates = explode(',', $response['activation_date']);
								foreach ($dates as $i => $date) {
									Cubix_Email::sendTemplate('gotd_purchase_confirm', $response['email'], array(
										'city' => $cities[$i],
										'date' => $date
									));
								}
							}
						}

						break;
					case 'pbump' :
						
						$additionalInfo = array(
                            'transaction_id'	=> $order_id,
							'payment_system'    => 'coinsome',
							'first_name'		=> 'BTC',
                        );

                        $applicationId = Cubix_Application::getId();
                        $client = new Cubix_Api_XmlRpc_Client();

                        $response = $client->call('OnlineBillingV2.confirmProfileBump', array($order->user_id, $applicationId, $additionalInfo));
                       
                        if ( $response ) {
                            $body = 'Escort "' . $response['showname'] . '" ID:' . $response['id'] . ' has just payed for BUMP IN ' . $response['city'] . "\n" ;
                        } else {
                            $body = 'Error occured in confirmation. profile bump order id: ' . $response['id'];
                        }

                        $callback = array (
                            'transaction_time'	=> $transaction->created_at,
                            'transaction_id'	=> $order_id,
                            'reference'			=> $order->user_id,
                            'currency'			=> 'BTC',
                            'amount'			=> $transaction['data']['amount'],
                            'first_name'		=> 'BTC',
                            'last_name'			=> "",
                            'log_data'          => json_encode($transaction),
                            'data'               => $body
                        );

                        Model_Billing_OnlineBilling::insertCallbackLog($callback);

                        switch ($response['frequency']) {
                        	case 'bump-option-1':
                        		$frequency_text = __('bump_per_hour', array('hour'=> 4));
                        		break;
                        	case 'bump-option-2':
                        		$frequency_text = __('bump_per_hour', array('hour'=> 8));
                        		break;
                        	case 'bump-option-3':
                        		$frequency_text = __('bump_per_hour', array('hour'=> 12));
                        		break;
                        	case 'bump-option-4':
                        		$frequency_text = __('bump_2_per_day');
                        		break;
                        	case 'bump-option-5':
                        		$frequency_text = __('bump_1_per_day');
                        		break;
                        	case 'bump-option-6':
                        		$frequency_text = __('not_avaiable');
                        		break;
                        	default:
                        		$frequency_text = '';
                        		break;
                        }
                        
                        $email_model = new Model_SMS_EmailOutbox();
	
                        $log_data = array(
							'email' =>  $response['user_email'],
							'subject' => 'Bump Acquistato',
							'text' => 'Cara '.addslashes($response['username']).', Hai acquistato l\'opzione bump per il tuo profilo su escortforumit.xxx. Numero bump: '.addslashes($response['bumps_count']).', frequenza '.addslashes($frequency_text).'
							 Ad ogni bump, il tuo annuncio viene spinto in cima alla categoria del tuo pacchetto, fino a quando un nuovo annuncio verrà inserito nella stessa categoria.
							Per qualsiasi domanda, si prega di contattare info@escortforumit.xxx o il proprio agente di vendita',
							'backend_user_id' => 1,
							'date' => new Zend_Db_Expr('NOW()'),
							'user_id' => $response['user_id'],
							'application_id' => $applicationId
						);

						$email_model->add($log_data);

                        Cubix_Email::sendTemplate('bump_is_purchased', $response['user_email'], array(
                            'username' => $response['username'],
                            'bumps_count' =>  $response['bumps_count'],
                            'frequency' =>  $frequency_text,
                        ));

                     	if ( $response['sales_email'] && strlen($response['sales_email']) ) {
                            Cubix_Email::sendTemplate('bump_notification_for_sales', $response['sales_email'] , array(
	                            'id' => $response['id'],
	                            'city' => $response['city'],
	                            'count' => $response['bumps_count'],
	                            'frequency' => $frequency_text,
                        	));
                        }
					break;
				}
			}
		}
		catch(Exception $ex) {
			Cubix_Email::send('badalyano@gmail.com', 'Coinsome Error callback', $ex->getMessage());
			header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
			die;
		}
		
		header("HTTP/1.1 200 OK");
		die('ok');
	}
	
	// </editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="TWISPAY PAYMENT">

    public function runTwispayTokenizerAction($token = null)
    {
        $isManual = empty($token) === FALSE; // This indicates, whether the action is running by cron or for a single transaction

        #ini_set('display_errors', 1);
        #ini_set('display_startup_errors', 1);
        #error_reporting(E_ALL);
        $cli = new Cubix_Cli();

        // Clear console
        $cli->clear();

        // Prevent from running multiple
        // times same action at the same time
        $pid_file = '/var/run/twispay-tokenizer' . Cubix_Application::getId() . '.pid';
        $cli->setPidFile($pid_file);

        if ($cli->isRunning()) {
            $cli->out('Action is already running... exitting...');
            exit(1);
        };

        $tokens = Model_Billing_OnlineBilling::getPaymentGatewayTokens('twispay');

        // This is the case, when current action was ran
        if ($isManual) {
            $tokens = [$token];
        } else if (!count($tokens)) {
            echo 'There are no tokens to check.' . "\n";
        }

        $twispayConfigs = Zend_Registry::get('system_config')['twispay'];
        Cubix_Twispay_TwispayApi::setConfig($twispayConfigs);

		
        foreach ($tokens as $row) {

            try {
                $cli->colorize('green')->out('Working on token: ' . $row->token);
            	

                $token = json_decode($row->token, true);

                $transactionId = $token['transactionId'];
                $orderId = $token['orderId'];
                $transaction = $order = null;

                if ($token['externalOrderId'] AND (!$transactionId || !$orderId)) {

                    // Perform a search operation with externalOrderID to get the order complete info
                    $orderMeta = Cubix_Twispay_TwispayApi::search(Cubix_Twispay_TwispayApi::TYPE_ORDER, ['externalOrderId' => $token['externalOrderId']]);

                    if (isset($orderMeta['data']) AND isset($orderMeta['data'][0])) $order = [
                        'data' => $orderMeta['data'][0]
                    ];

                    if($order) {
                        $transactionMeta = Cubix_Twispay_TwispayApi::search(Cubix_Twispay_TwispayApi::TYPE_TRANSACTION, ['orderId' => $order['data']['id']]);
                        if(isset($transactionMeta['data']) AND isset($transactionMeta['data'][0])) $transaction = [
                            'data' => $transactionMeta['data'][0]
                        ];
                    }
                }

                // Getting transaction data if it was not already fetched above
                if(!$transaction) $transaction = Cubix_Twispay_TwispayApi::get(Cubix_Twispay_TwispayApi::TYPE_TRANSACTION, $token['transactionId']);

                // In manual state, we don't know order ID
                // But this first call above, provides that info also
                if($isManual) $token['orderId'] = $transaction['data']['orderId'];

                // Getting transaction data if it was not already fetched above
                if(!$order) $order = Cubix_Twispay_TwispayApi::get(Cubix_Twispay_TwispayApi::TYPE_ORDER, $token['orderId']);

                $cli->colorize('green')->out('Status: ' . $transaction['data']['transactionStatus']);

                if($isManual) {
                    echo "<pre>";
                    echo '<h1>Transaction</h1>';
                    print_r($transaction);
                    echo '<h1>Order</h1>';
                    print_r($order);
                    echo '</pre>';
                }
				

				
                
                if ($transaction['data']['transactionStatus'] !== 'complete-ok' && $transaction['data']['transactionStatus'] !== 'in-progress') {
                    Model_Billing_OnlineBilling::updatePaymentGatewayTokenAttempt($row->id);
                    $cli->colorize('yellow')->out('Failure. transaction_id:' . $transaction['data']['id'] . ' trasaction failed.');
                    continue;
                }

                list($method, $ref, $hash) = explode('-', $order['data']['externalOrderId']);

                switch ($method) {
                    case 'SC3' :

                        $callback = array (
                            'transaction_time'	=> $transaction['data']['creationDate'],
                            'transaction_id'	=> $transaction['data']['id'],
                            'reference'			=> $ref,
                            'hash'				=> $hash,
                            'status'			=> "200",
                            'currency'			=> $transaction['data']['currency'],
                            'amount'			=> $transaction['data']['amount'],
                            'first_name'		=> $transaction['data']['cardHolderName'],
                            'last_name'			=> '',
                            'email'				=> '',
                            'phone_number'		=> '',
                            'ip_address'		=> $transaction['data']['ip'],
                            'card_name'			=> $transaction['data']['cardProvider'],
                            'card_number'		=> $transaction['data']['cardNumber'],
                            'exp_date'			=> $transaction['data']['cardExpiryDate'],
                            'sha1'				=> sha1(Model_Billing_OnlineBilling::$secret_prefixes[Cubix_Application::getId()] . number_format($transaction['data']['amount'], 2, '.', '') . 'EUR' . 'SC3-' . $ref),
                            'log_data'          => json_encode($transaction)
                        );

                        $onlineBilling = new Model_Billing_OnlineBilling($callback, false, false);

                        $onlineBilling->addCallbackLog();

                        if ( $onlineBilling->isTransactionIdExists($callback['transaction_id'], 'twispay') ) {
                            $onlineBilling->updateLogData('Failure. transaction_id:' . $callback['transaction_id'] . ' already exists.');

                            if($isManual) {
                                echo 'Failure. transaction_id:' . $callback['transaction_id'] . ' already exists.';
                            }

                            continue;
                        }

                        $payed = $onlineBilling->payV3('twispay');

                        if($payed) {
                            Model_Billing_OnlineBilling::removePaymentGatewayToken($row->id);
                            $cli->colorize('green')->out($callback['first_name'] . ' ' . $callback['last_name'] . ' payment confirmed (SC3)')->reset();
                        }

                        break;
                    case 'SC' :
                        $callback = array (
                            'transaction_time'	=> $transaction['data']['creationDate'],
                            'transaction_id'	=> $transaction['data']['id'],
                            'reference'			=> $ref,
                            'hash'				=> $hash,
                            'status'			=> "200",
                            'currency'			=> $transaction['data']['currency'],
                            'amount'			=> $transaction['data']['amount'],
                            'first_name'		=> $transaction['data']['cardHolderName'],
                            'last_name'			=> "",
                            'address'			=> 'no address',
                            'zipcode'			=> '',
                            'city'				=> '',
                            'country_code'		=> '',
                            'email'				=> '',
                            'phone_number'		=> '',
                            'ip_address'		=> $transaction['data']['ip'],
                            'card_name'			=> $transaction['data']['cardProvider'],
                            'card_number'		=> $transaction['data']['cardNumber'],
                            'exp_date'			=> $transaction['data']['cardExpiryDate'],
                            'sha1'				=> sha1(Model_Billing_OnlineBilling::$secret_prefixes[Cubix_Application::getId()] . $transaction['data']['amount'] . 'EUR' . 'SC-' . $ref),
                            'log_data'          => json_encode($transaction)
                        );

                        $onlineBilling = new Model_Billing_OnlineBilling($callback);

                        $onlineBilling->addCallbackLog();

                        if ( $onlineBilling->isTransactionIdExists($callback['transaction_id'], 'twispay') ) {
                            $onlineBilling->updateLogData('Failure. transaction_id:' . $callback['transaction_id'] . ' already exists.');
                            $cli->colorize('red')->out('Transaction with ID: ' . $transaction['data']['id'] . ' is duplicated, skipping ...');
                            continue;
                        }
                        $payed = $onlineBilling->pay('twispay');

                        if ($payed) {
                            Model_Billing_OnlineBilling::removePaymentGatewayToken($row->id);
                            $cli->colorize('green')->out($callback['first_name'] . ' ' . $callback['last_name'] . ' payment confirmed (SC)');
                        } else {
                            $cli->colorize('red')->out($callback['first_name'] . ' ' . $callback['last_name'] . ' paymend rejected, checkout online_billing_log (SC)');
                        }

                        try {
                            $config = Zend_Registry::get('system_config');

                            if ( $config['smsNotificationNumbers'] &&  strlen($config['smsNotificationNumbers']) ) {
                                $to_numbers = explode(',', $config['smsNotificationNumbers']);
                                $text = 'Twispay SC for user_id--' . $ref . ' just paid for ad.';
                                $this->_sendSMSToNumbers($to_numbers, $text);
                            }
                        } catch(Exception $ex) {
                            echo('error in sending sms');
                        }

                        break;
					case 'pb':
                        $additionalInfo = array(
                            'transaction_id'    => $transaction['data']['id'],
                            'payment_system'    => 'twispay',
                            'first_name'        => $transaction['data']['cardHolderName']
                        );

                        $applicationId = Cubix_Application::getId();
                        $client = new Cubix_Api_XmlRpc_Client();

                        $response = $client->call('OnlineBillingV2.confirmProfileBoost', array($ref, $applicationId, $additionalInfo));

                        if ( $response ) {
                            $activationDate = implode("\n", explode(',', $response['activation_date']));
                            $body = 'Escort "' . $response['showname'] . '" ID:' . $response['id'] . ' has just payed for BOOST IN ' . $response['city'] . "\n" . $activationDate;
                        } else {
                            $body = 'Error occured in confirmation. profile_boost_order_id: ' . $response['id'];
                        }

                        $callback = array (
                            'transaction_time'	=> $transaction['data']['creationDate'],
                            'transaction_id'	=> $transaction['data']['id'],
                            'reference'			=> $ref,
                            'currency'			=> $transaction['data']['currency'],
                            'amount'			=> $transaction['data']['amount'],
                            'first_name'		=> $transaction['data']['cardHolderName'],
                            'last_name'			=> "",
                            'log_data'          => json_encode($transaction),
                            'data'               => $body
                        );

                        Model_Billing_OnlineBilling::insertCallbackLog($callback);

                        if ( $response['sales_email'] && strlen($response['sales_email']) ) {
                            Cubix_Email::send($response['sales_email'], 'Profile Boost Paid', $body);
                        }

                        Model_Billing_OnlineBilling::removePaymentGatewayToken($row->id);
                        $cli->colorize('green')->out($callback['first_name'] . ' payment confirmed (PROFILE BOOST)');

                        break;
                    case 'pbump' :
						
						$additionalInfo = array(
                            'transaction_id'    => $transaction['data']['id'],
                            'payment_system'    => 'twispay',
                            'first_name'        => $transaction['data']['cardHolderName']
                        );

                        $applicationId = Cubix_Application::getId();
                        $client = new Cubix_Api_XmlRpc_Client();

                        $response = $client->call('OnlineBillingV2.confirmProfileBump', array($ref, $applicationId, $additionalInfo));
                       
                        if ( $response ) {
                            $body = 'Escort "' . $response['showname'] . '" ID:' . $response['id'] . ' has just payed for BUMP IN ' . $response['city'] . "\n" ;
                        } else {
                            $body = 'Error occured in confirmation. profile bump order id: ' . $response['id'];
                        }

                        $callback = array (
                            'transaction_time'	=> $transaction['data']['creationDate'],
                            'transaction_id'	=> $transaction['data']['id'],
                            'reference'			=> $ref,
                            'currency'			=> $transaction['data']['currency'],
                            'amount'			=> $transaction['data']['amount'],
                            'first_name'		=> $transaction['data']['cardHolderName'],
                            'last_name'			=> "",
                            'log_data'          => json_encode($transaction),
                            'data'               => $body
                        );

                        Model_Billing_OnlineBilling::insertCallbackLog($callback);

                        switch ($response['frequency']) {
                        	case 'bump-option-1':
                        		$frequency_text = __('bump_per_hour', array('hour'=> 4));
                        		break;
                        	case 'bump-option-2':
                        		$frequency_text = __('bump_per_hour', array('hour'=> 8));
                        		break;
                        	case 'bump-option-3':
                        		$frequency_text = __('bump_per_hour', array('hour'=> 12));
                        		break;
                        	case 'bump-option-4':
                        		$frequency_text = __('bump_2_per_day');
                        		break;
                        	case 'bump-option-5':
                        		$frequency_text = __('bump_1_per_day');
                        		break;
                        	case 'bump-option-6':
                        		$frequency_text = __('not_avaiable');
                        		break;
                        	default:
                        		$frequency_text = '';
                        		break;
                        }
                        
                        $email_model = new Model_SMS_EmailOutbox();
	
                        $log_data = array(
							'email' =>  $response['user_email'],
							'subject' => 'Bump Acquistato',
							'text' => 'Cara '.addslashes($response['username']).', Hai acquistato l\'opzione bump per il tuo profilo su escortforumit.xxx. Numero bump: '.addslashes($response['bumps_count']).', frequenza '.addslashes($frequency_text).'
							 Ad ogni bump, il tuo annuncio viene spinto in cima alla categoria del tuo pacchetto, fino a quando un nuovo annuncio verrà inserito nella stessa categoria.
							Per qualsiasi domanda, si prega di contattare info@escortforumit.xxx o il proprio agente di vendita',
							'backend_user_id' => 1,
							'date' => new Zend_Db_Expr('NOW()'),
							'user_id' => $response['user_id'],
							'application_id' => $applicationId
						);

						$email_model->add($log_data);

                        Cubix_Email::sendTemplate('bump_is_purchased', $response['user_email'], array(
                            'username' => $response['username'],
                            'bumps_count' =>  $response['bumps_count'],
                            'frequency' =>  $frequency_text,
                        ));

                     	//test
                        // Cubix_Email::sendTemplate('bump_notification_for_sales', 'mihranhayrapetyan@gmail.com', array(
                        //     'id' => $response['id'],
                        //     'city' => $response['city'],
                        //     'count' => $response['bumps_count'],
                        //     'frequency' => $frequency_text,
                        // ));

                        if ( $response['sales_email'] && strlen($response['sales_email']) ) {
                            Cubix_Email::sendTemplate('bump_notification_for_sales', $response['sales_email'] , array(
	                            'id' => $response['id'],
	                            'city' => $response['city'],
	                            'count' => $response['bumps_count'],
	                            'frequency' => $frequency_text,
                        	));
                        }

                        Model_Billing_OnlineBilling::removePaymentGatewayToken($row->id);
                        $cli->colorize('green')->out($callback['first_name'] . ' payment confirmed (PROFILE BUMP)');

                       	break;
                    case 'gotd' :
                        $ref = $order['data']['description'];

                        $additional_info = array (
                            'transaction_id'	=> $transaction['data']['id'],
                            'payment_system'    => 'twispay'
                        );
						
						$callback = array (
                            'transaction_time'	=> $transaction['data']['creationDate'],
                            'transaction_id'	=> $transaction['data']['id'],
                            'reference'			=> $ref,
                            'currency'			=> $transaction['data']['currency'],
                            'amount'			=> $transaction['data']['amount'],
                            'first_name'		=> $transaction['data']['cardHolderName'],
                            'last_name'			=> "",
                            'log_data'          => json_encode($transaction),
                        );
						
						if ( Model_Billing_OnlineBilling::checkTransactionIdExistsence( $transaction['data']['id'], 'twispay') ) {
							$callback['data'] =  'Failure. transaction_id:' . $callback['transaction_id'] . ' already exists.';
							Model_Billing_OnlineBilling::insertCallbackLog($callback);
							continue;
						}
						
                        $application_id = Cubix_Application::getId();
                        $client = new Cubix_Api_XmlRpc_Client();
                        $response = $client->call('Billing.confirmGotd', array($ref, $application_id, $additional_info));

                        if ( $response ) {
                            $activation_date = implode("\n", explode(',', $response['activation_date']));
                            $body =  'Escort "' . $response['showname'] . '" ID:' . $response['id'] . ' has just payed for GOTD IN ' . $response['city'] . "\n" . $activation_date;
                        } else {
                            $body = 'Error occured in confirmation. gotd_order_id: ' . $response['id'];
                        }              
						$callback['data'] = $body;
                        Model_Billing_OnlineBilling::insertCallbackLog($callback);
                       
                        if ( $response['sales_email'] && strlen($response['sales_email']) ) {
                            Cubix_Email::send($response['sales_email'], 'Gotd Paid', $body);
                        }

                        if($application_id == APP_A6) {
                            if ( $response['email'] && strlen($response['email']) ) {
                                $cities = explode(',', $response['group_city']);
                                $dates = explode(',', $response['activation_date']);
                                foreach ($dates as $i => $date) {
                                    Cubix_Email::sendTemplate('gotd_purchase_confirm', $response['email'], array(
                                        'city' => $cities[$i],
                                        'date' => $date
                                    ));
								}
                            }
                        }

                        Model_Billing_OnlineBilling::removePaymentGatewayToken($row->id);
                        $cli->colorize('green')->out($callback['first_name'] . ' payment confirmed (GOTD)');

                        break;
					case 'vc' :
												
						$body = 'Cam Booking has just been payed request_id: ' . $ref;
						
						$callback = array(
                            'transaction_time' => $transaction['data']['creationDate'],
                            'transaction_id' => $transaction['data']['id'],
                            'reference' => $ref,
                            'currency' => $transaction['data']['currency'],
                            'amount' => $transaction['data']['amount'],
                            'first_name' => $transaction['data']['cardHolderName'],
                            'last_name' => "",
                            'log_data' => json_encode($transaction),
                            'data' => $body
                        );
						
						$model_video_call = new Model_Billing_VideoCall();
												
						 $tr_data = array(
                            'request_id' => $ref,
                            'date_transfered' => date('Y-m-d H:i:s'),
                            'transaction_id' => $transaction['data']['id'],
                            'amount' => $transaction['data']['amount'],
                            'payment_system' => 'twispay',
                            'payer_name' => $transaction['data']['cardHolderName'],
                            'cc_holder' => strtoupper($transaction['data']['cardType']),
                            'cc_number' => str_replace('*', 'X', $transaction['data']['cardNumber']),
                            'cc_exp_month' => $transaction['data']['card']['expiryMonth'],
                            'cc_exp_year' => $transaction['data']['card']['expiryYear'],
                        );
						
						Model_Billing_OnlineBilling::insertCallbackLog($callback);
						$model_video_call->addTransfer($tr_data);
						
						Model_Billing_OnlineBilling::removePaymentGatewayToken($row->id);
                        $cli->colorize('green')->out($callback['first_name'] . ' payment confirmed (Video Chat)');
						
						break;
					case 'CA':
						$t_model = new Model_ClassifiedAds();
						
						$package = $t_model->getPackageById($hash);
						
						$card_holder_name = ( is_null($transaction['cardHolderName']) ? 'not avaiable' : $transaction['cardHolderName'] );

						$tr_data = array(
							'ad_id' => $ref,
							'status' => Model_ClassifiedAds::AD_TRANSFER_STATUS_CONFIRMED,
							'first_name' => $card_holder_name ,
							'last_name' => '',
							'transaction_id' => $transaction['data']['id'],
							'amount' => $transaction['data']['amount'],
							'application_id' => Cubix_Application::getId(),
							//'backend_user_id' => $be_user->id,
							'date_confirmed' => new Zend_Db_Expr('NOW()'),
							'package_name' => $package->name,
							'package_price' => $package->price,
							'package_period' => $package->period,
							'is_paid' => 1
						);

						if(Cubix_Application::getId() == APP_A6) {
							$tr_data['gateway'] = Model_ClassifiedAds::AD_TRANSFER_GATEWAY_EPG;
						}
						

						$t_model->makePremium($ref, $package->period);
						
						$t_model->addTransfer($tr_data);
						
						
						$body = 'CA paid: ' . $ref;
						$callback = array(
                            'transaction_time' => date('Y-m-d H:i:s'),
                            'transaction_id' => $transaction['data']['id'],
                            'reference' => $ref,
                            'currency' => $transaction['data']['currency'],
                            'amount' => $transaction['data']['amount'],
                            'first_name' => $transaction['data']['cardHolderName'],
                            'last_name' => "",
                            'log_data' => json_encode($transaction),
                            'data' => $body
                        );

						Model_Billing_OnlineBilling::insertCallbackLog($callback);

						Model_Billing_OnlineBilling::removePaymentGatewayToken($row->id);
                        $cli->colorize('green')->out($callback['first_name'] . 'Twispay payment confirmed for classfied ad');					
						
						break;
                    case 'cam':
                        $body = 'TOKEN for EF-CAMS has just been payed request_id: ' . $ref;
                        $model_tokens = new Model_Billing_Tokens();

                        $callback = array(
                            'transaction_time' => $transaction['data']['creationDate'],
                            'transaction_id' => $transaction['data']['id'],
                            'reference' => $ref,
                            'currency' => $transaction['data']['currency'],
                            'amount' => $transaction['data']['amount'],
                            'first_name' => $transaction['data']['cardHolderName'],
                            'last_name' => "",
                            'log_data' => json_encode($transaction),
                            'data' => $body
                        );

                        $tr_data = array(
                            'request_id' => $ref,
                            'date_transfered' => date('Y-m-d H:i:s'),
                            'transaction_id' => $transaction['data']['id'],
                            'amount' => $transaction['data']['amount'],
                            'payment_system' => 'twispay',
                            'payer_name' => $transaction['data']['cardHolderName'],
                            'cc_holder' => strtoupper($transaction['data']['cardType']),
                            'cc_number' => str_replace('*', 'X', $transaction['data']['cardNumber']),
                            'cc_exp_month' => $transaction['data']['card']['expiryMonth'],
                            'cc_exp_year' => $transaction['data']['card']['expiryYear'],
                        );

                        Model_Billing_OnlineBilling::insertCallbackLog($callback);
                        $model_tokens->addTransfer($tr_data);

                        Model_Billing_OnlineBilling::removePaymentGatewayToken($row->id);
                        $cli->colorize('green')->out($callback['first_name'] . ' payment confirmed (CAM TOKENS)');

                        break;
					case 'CR':
                        $body = 'Credits has just been paid for request -' . $ref;
						
                        $model_tokens = new Model_Billing_Credits();

                        $callback = array(
                            'transaction_time' => $transaction['data']['creationDate'],
                            'transaction_id' => $transaction['data']['id'],
                            'reference' => $ref,
                            'currency' => $transaction['data']['currency'],
                            'amount' => $transaction['data']['amount'],
                            'first_name' => $transaction['data']['cardHolderName'],
                            'last_name' => "",
                            'log_data' => json_encode($transaction),
                            'data' => $body
                        );
						
						$request_details = $model_tokens->getRequestDetails($ref);
						
						if($transaction['data']['amount'] == $request_details->amount){
							$tr_data = array(
								'user_id' => $request_details->user_id,
								'request_id' => $ref,
								'date_transfered' => date('Y-m-d H:i:s'),
								'transaction_id' => $transaction['data']['id'],
								'amount' => $transaction['data']['amount'],
								'credits' => $request_details->credits,
								'payment_system' => 'twispay',
								'payer_name' => $transaction['data']['cardHolderName'],
								'cc_holder' => strtoupper($transaction['data']['cardType']),
								'cc_number' => str_replace('*', 'X', $transaction['data']['cardNumber']),
								'cc_exp_month' => $transaction['data']['card']['expiryMonth'],
								'cc_exp_year' => $transaction['data']['card']['expiryYear'],
							);
							
							$model_tokens->addTransfer($tr_data);
							$model_tokens->ConfirmRequestStatus($ref);

							Model_Billing_OnlineBilling::removePaymentGatewayToken($row->id);
							$cli->colorize('green')->out($callback['first_name'] . ' Credits confirmed ');
						}
						else{
							$callback['data'] = 'Credit Amounts dosnt match --- '.$transaction['data']['amount']. ' ---- '. $request_details->amount;  
							$cli->colorize('red')->out($callback['first_name'] . ' Credit Amounts dosnt match ');
						}
						Model_Billing_OnlineBilling::insertCallbackLog($callback);

					break;
				
                    default;
                }

            } catch (Exception $ex) {
                $cli->colorize('red')->out($ex->getMessage());
            }

            if ( $row->attempt >= 30 )  {
                //Model_Billing_OnlineBilling::removePaymentGatewayToken($token->id);
            } else {
                Model_Billing_OnlineBilling::updatePaymentGatewayTokenAttempt($row->id);
            }
        }

        $cli->colorize('green')->out('Done! Have a nice day');
        die;
    }


	public function postfinanceCallbackAction()
	{
		// ini_set('display_errors', 1);
		// ini_set('display_startup_errors', 1);
		// error_reporting(E_ALL);
		$req = $this->_request;

		$data = array();
		$data['ACCEPTANCE'] = $req->ACCEPTANCE;
		$data['AMOUNT'] = $req->amount;
		$data['BRAND'] = $req->BRAND;
		$data['PAYIDSUB'] = $req->PAYIDSUB;
		$data['CURRENCY'] = $req->currency;
		$data['NCERROR'] = $req->NCERROR;
		$data['ORDERID'] = $req->orderID;
		$data['PAYID'] = $req->PAYID;
		$data['PM'] = $req->PM;
		$data['TRXDATE'] = $req->TRXDATE;
		$data['IP'] = $req->IP;
		$data['STATUS'] = $req->STATUS;
		//$data['ECI'] = $req->ECI;
		// $data['CARDNO'] = $req->CARDNO;
		// $data['CN'] = $req->CN;
		// $data['ED'] = $req->ED;
		// $data['SHASIGN'] = $req->SHASIGN;
		
		file_put_contents('/var/log/online-billing-postfinance-tests.log', '--------------------------'.PHP_EOL.serialize($data).PHP_EOL, FILE_APPEND);
		file_put_contents('/var/log/online-billing-postfinance-tests.log', serialize($req->SHASIGN).PHP_EOL, FILE_APPEND);

		$SHASIGN = $req->SHASIGN;

		ksort($data, SORT_STRING);

		$dataString = '';
		foreach($data as $key=>$value)
		{
			$dataString .= $key.'='.$value.'Hrzlp-/436)89wWr';
		}

		$checksum = hash('sha256', $dataString);

		if(strtoupper($checksum) != $SHASIGN){
			die;
		}

		file_put_contents('/var/log/online-billing-postfinance-tests.log', serialize($data).PHP_EOL, FILE_APPEND);

		list($method, $ref, $hash) = explode('-', $data['ORDERID']);

		switch($method) {
			case 'SC3': 
				$callback = array(
					'transaction_time'	=> date('Y-m-d H:i:s'),
					'transaction_id'	=> $data['PAYID'],
					'reference'			=> $ref,
					'currency'			=> $data['CURRENCY'],
					'hash'				=> $hash,
					'amount'			=> $data['AMOUNT'],
					'first_name'		=> 'not',
					'last_name'			=> 'provided',
					'address'			=> 'not provided',
					'zipcode'			=> 'not provided',
					'city'				=> 'not provided',
					'country_code'		=> 'not provided',
					'email'				=> 'not provided',
					'phone_number'		=> 'not provided',
					'ip_address'		=> $data['IP'],
					'card_name'			=> 'not provided',
					'card_number'		=> 'not provided',
					'exp_date'			=> 'not provided',
					'gateway'			=> 'postfinance',
					'sha1'				=> sha1(Model_Billing_OnlineBilling::$secret_prefixes[Cubix_Application::getId()] . $data['AMOUNT'] . 'CHF' . 'SC3-' . $ref)
				);
				

				$onlineBilling = new Model_Billing_OnlineBilling($callback, false, false);
				
				$onlineBilling->addCallbackLog();
				if ( $onlineBilling->isTransactionIdExists($callback['transaction_id']) ) {
					$onlineBilling->updateLogData('Failure. transaction_id:' . $callback['transaction_id'] . ' already exists.');
					die('<html><head></head><body>OK</body></html>');
				}

				file_put_contents('/var/log/online-billing-postfinance-tests.log', $data['STATUS'].PHP_EOL, FILE_APPEND);
				if ($data['STATUS'] !== '9' && $data['STATUS'] !== '91') {
					$onlineBilling->updateLogData('Failure. transaction_id:' . $callback['transaction_id'] . ' trasaction failed, Status not 9.');
					die('<html><head></head><body>OK</body></html>');
				}
				
				file_put_contents('/var/log/online-billing-postfinance-tests.log', 'Befor submit pay'.PHP_EOL, FILE_APPEND);
				$onlineBilling->payV3('postfinance');
				file_put_contents('/var/log/online-billing-postfinance-tests.log', 'Pay submited'.PHP_EOL, FILE_APPEND);
				break;
			case 'CA':
				if ($data['STATUS'] === '9' || $data['STATUS'] === '91') {
					$t_model = new Model_ClassifiedAds();
							
					$ad_id = $ref;
					$package_id = $hash;
					
					$package = $t_model->getPackageById($package_id);
					
					$tr_data = array(
						'ad_id' => $ad_id,
						'status' => Model_ClassifiedAds::AD_TRANSFER_STATUS_CONFIRMED,
						'first_name' => 'not',
						'last_name' => 'provided',
						'transaction_id' => $data['PAYID'],
						'amount' => $data['AMOUNT'],
						'application_id' => Cubix_Application::getId(),
						//'backend_user_id' => $be_user->id,
						'date_confirmed' => new Zend_Db_Expr('NOW()'),
						'package_name' => $package->name,
						'package_price' => $package->price,
						'package_period' => $package->period,
						'is_paid' => 1
					);
					if(Cubix_Application::getId() == APP_A6) {
						$tr_data['gateway'] = Model_ClassifiedAds::AD_TRANSFER_GATEWAY_POSTFINANCE;
					}
					$t_model->makePremium($ad_id, $package->period);
					
					$t_model->addTransfer($tr_data);
				} else{
					//failure
				}
				break;
			default;
		}

//        Cubix_Api_XmlRpc_Client::setApiKey(Cubix_Application::getById()->api_key);
//        Cubix_Email::send('zhorabrainforce@gmail.com', 'ch-data', var_export($ch_data, true));
//        Cubix_Email::send('zhorabrainforce@gmail.com', 'decrypted_data', var_export($decrypted_data, true));

		header("HTTP/1.1 200 OK");
		die('<html><head></head><body>OK</body></html>');
	}

	// </editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="MMGBILL PAYMENT">
	
	public function mmgBillCallbackAction()
	{
		$req = $this->_request;
		//file_put_contents('/tmp/online-billing-request.log', var_export(array($req->txn_status, $req->txn_type, $req->c1), true), FILE_APPEND);
		$application_id = Cubix_Application::getId();
		Cubix_Api_XmlRpc_Client::setApiKey(Cubix_Application::getById()->api_key);

		$is_escortbook = false;
		if ( strpos($req->c1, 'escortbook') !== FALSE ) {
			$is_escortbook = true;
			$c1 = str_replace('escortbook', '', $req->c1);
			$req->setParam('c1', $c1);
		}

		if ( ( $req->txn_status != 'APPROVED' && ! $is_escortbook ) || strpos($req->c1, 'adspay789') === 0 || strpos($req->c1, 'paygate456') === 0 ) {
			header("HTTP/1.1 200 OK");
			die('<html><head></head><body>OK</body></html>');
		}

		// <editor-fold defaultstate="collapsed" desc="If c2 is set means request is for another project">
		// mmgbill doesn't have dynamic postback ursl. so constructing request and calling project mentioned in c2 variable">
		
		if ( $req->c2 || $is_escortbook ) {
			try {
				if ( $is_escortbook ) {
					$client = new Zend_Http_Client('http://www.escortbook.com/_payments/mmgbill/postback.php');			
				} else {
					$client = new Zend_Http_Client('https://backend.' . $req->c2 . '/billing/online-billing/mmg-bill-callback');	
				}
				
				$client->setParameterPost(array(
					'ti_mer'	=> $req->ti_mer,
					'txn_status'	=> $req->txn_status,
					'ti_mmg'	=>	$req->ti_mmg,
					'cu'	=> $req->cu,
					'ia'	=> $req->amc,
					'name'	=> $req->name,
					'address'	=> $req->address,
					'email'		=> $req->email,
					'customer_ip'	=> $req->customer_ip,
					'card_brand'	=> $req->card_brand,
					'ccnum'			=> $req->ccnum,
					'txn_type'		=> $req->txn_type,
					'product_code'	=> $req->product_code,
					'mmg_errno'	=> $req->mmg_errno,
					'c1'		=> $req->c1,
					'mid'		=> $req->mid,
					'ti_ini'	=> $req->ti_ini
				));
				
				$response = $client->request('POST');
				
				// file_put_contents('/var/log/online-billing-request.log', $params, FILE_APPEND);

				if ( $response ) {
					header("HTTP/1.1 200 OK");
					die('<html><head></head><body>OK</body></html>');
				}
			} catch(Exception $e) {
				file_put_contents('/var/log/online-billing-request.log', var_export($e, true), FILE_APPEND);
			}
		}
		// </editor-fold>
				
		list($method, $ref, $hash) = explode('Z', $req->ti_mer);
		// file_put_contents('/var/log/online-billing-request.log', $params, FILE_APPEND);

		try{
			switch($method) {
				case 'SC': 
				case 'S': 						
					$callback = array(
						'transaction_time'	=> date('Y-m-d H:i:s'),
						'transaction_id'	=> $req->ti_mmg,
						'reference'			=> $ref,
						'hash'				=> $hash,
						'currency'			=> $req->cu,
						'amount'			=> $req->amc / 100,
						'first_name'		=> $req->name,
						'last_name'			=> '',
						'zipcode'			=> 'not provided',
						'city'				=> 'not provided',
						'country_code'		=> 'not provided',
						'phone_number'		=> 'not provided',
						'ip_address'		=> $req->customer_ip,
						'card_name'			=> $req->card_brand,
						'card_number'		=> $req->ccnumm,
						'exp_date'			=> 'not provided'
					);
					
					// if($application_id == APP_EI) {
					// 	Cubix_Email::send('mihranhayrapetyan@gmail.com', 'MMG BILLING  method callback', var_export($callback, true));
					// }

					//If set to save card info pass mmg_token 
					//to model which will save it
					if ( $req->c1 == 1 ) {
						$callback['mmg_token'] = $req->ti_mmg;
					}

					//if is_oneclick is set, calling it from frontent
					//cause mmg doesn't postback for oneclick api
					//getting previous transfer with mmg_token which is previous transaction_id
					//and set info to callback
					if ( $req->is_oneclick == 1 && $req->prev_ti_mmg ) {
						$transfer = Model_Billing_OnlineBilling::getTransfer($req->prev_ti_mmg);
						$callback['first_name'] = $transfer->first_name;
						$callback['last_name'] = $transfer->last_name;
						$callback['address'] = $transfer->cc_address;
						$callback['card_name'] = $transfer->cc_holder;
						$callback['card_number'] = $transfer->cc_number;
					}

					$onlineBilling = new Model_Billing_OnlineBilling($callback, false, false);
					

					$onlineBilling->addCallbackLog();

					if ( $onlineBilling->isTransactionIdExists($callback['transaction_id']) ) {
						$onlineBilling->updateLogData('Failure. transaction_id:' . $callback['transaction_id'] . ' already exists.');
						header("HTTP/1.1 200 OK");
						die('<html><head></head><body>OK</body></html>');
					}

					$onlineBilling->pay('mmgbill');
					break;
				case 'G':
					$additional_info = array (
						'transaction_id'	=> $req->ti_mmg,
						'gateway'	=> 'mmgbill',
						'card_number'	=> $req->ccnumm,
						'card_name'	=> $req->card_brand
					);

					if ( $req->is_oneclick == 1 && $req->prev_ti_mmg ) {
						$transfer = Model_Billing_OnlineBilling::getTransfer($req->prev_ti_mmg);
						$additional_info['card_number'] = $transfer->cc_number;
					}

					if ( $req->c1 == 1 ) {
						$additional_info['token'] = $req->ti_mmg;
					}


					$client = new Cubix_Api_XmlRpc_Client();
					$ref = str_replace('x', ':', $ref);
					$response = $client->call('Billing.confirmGotd', array($ref, $application_id, $additional_info));
					
					if ( $response ) {
						$activation_date = implode("\n", explode(',', $response['activation_date']));
						$body = 'Escort "' . $response['showname'] . '" ID:' . $response['id'] . ' has just payed for GOTD IN ' . $response['city'] . "\n" . $activation_date;
					} else {
						$body = 'Error occured in confirmation. gotd_order_id: ' . $ref;
					}

					$callback = array(
						'transaction_time'	=> date('Y-m-d H:i:s'),
						'transaction_id'	=> $req->ti_mmg,
						'reference'			=> $ref,
						'currency'			=> $req->cu,
						'amount'			=> $req->amc / 100,
						'first_name'		=> $req->name,
						'last_name'			=> 'MMG GOTD',
						'zipcode'			=> 'not provided',
						'city'				=> 'not provided',
						'country_code'		=> 'not provided',
						'phone_number'		=> 'not provided',
						'ip_address'		=> $req->customer_ip,
						'card_name'			=> $req->card_brand,
						'card_number'		=> $req->ccnumm,
						'data'               => $body
					);

					Model_Billing_OnlineBilling::insertCallbackLog($callback);
					
					if ( $response['sales_email'] && strlen($response['sales_email']) ) {
						Cubix_Email::send($response['sales_email'], 'Gotd Paid', $body);
					}

					if($application_id == APP_A6) {
						if ( $response['email'] && strlen($response['email']) ) {
							$cities = explode(',', $response['group_city']);
							$dates = explode(',', $response['activation_date']);
							foreach ($dates as $i => $date) {
								Cubix_Email::sendTemplate('gotd_purchase_confirm', $response['email'], array(
									'city' => $cities[$i],
									'date' => $date
								));
							}
						}
					}
				
					break;
				case 'PB':
					$additional_info = array (
						'transaction_id'	=> $req->ti_mmg,
						'gateway'	=> 'mmgbill',
						'card_number'	=> $req->ccnumm,
						'card_name'	=> $req->card_brand
					);

					if ( $req->is_oneclick == 1 && $req->prev_ti_mmg ) {
						$transfer = Model_Billing_OnlineBilling::getTransfer($req->prev_ti_mmg);
						$additional_info['card_number'] = $transfer->cc_number;
					}

					if ( $req->c1 == 1 ) {
						$additional_info['token'] = $req->ti_mmg;
					}

					$client = new Cubix_Api_XmlRpc_Client();
					$ref = str_replace('x', ':', $ref);


					$response = $client->call('OnlineBillingV2.confirmProfileBoost', array($ref, $application_id, $additional_info));

					if ( $response ) {
						$activation_date = implode("\n", explode(',', $response['activation_date']));
						$body = 'Escort "' . $response['showname'] . '" ID:' . $response['id'] . ' has just payed for BOOST IN ' . $response['city'] . "\n" . $activation_date;
					} else {
						$body = 'Error occured in confirmation. profile_boost_order_id: ' . $ref;
					}

					$callback = array(
						'transaction_time'	=> date('Y-m-d H:i:s'),
						'transaction_id'	=> $req->ti_mmg,
						'reference'			=> $ref,
						'currency'			=> $req->cu,
						'amount'			=> $req->amc / 100,
						'first_name'		=> $req->name,
						'last_name'			=> 'MMG PB',
						'zipcode'			=> 'not provided',
						'city'				=> 'not provided',
						'country_code'		=> 'not provided',
						'phone_number'		=> 'not provided',
						'ip_address'		=> $req->customer_ip,
						'card_name'			=> $req->card_brand,
						'card_number'		=> $req->ccnumm,
						'data'               => $body
					);
					Model_Billing_OnlineBilling::insertCallbackLog($callback);

					if ( $response['sales_email'] && strlen($response['sales_email']) ) {
						Cubix_Email::send($response['sales_email'], 'Profile Boost Paid', $body);
					}

					header("HTTP/1.1 200 OK");
					die('<html><head></head><body>OK</body></html>');

					break;
				case 'EX':
					$additional_info = array (
						'transaction_id'	=> $req->ti_mmg,
						'card_number'	=> $req->ccnumm,
						'card_name'	=> $req->card_brand
					);

					$client = new Cubix_Api_XmlRpc_Client();

					$response = $client->call('OnlineBillingV2.confirmExpoOrder', array($ref, $additional_info));

					if ( $response ) {
						$body = 'Escort "' . $response['showname'] . '" ID:' . $response['id'] . ' has just payed for Milan Expo.
						Start Date: ' . $response['start_date'] . ', End Date: ' . $response['end_date'];
					} else {
						$body = 'Error occured in milan expo confirmation. expo_order_id: ' . $ref;
					}
				
					if ( $response['sales_email'] && strlen($response['sales_email']) ) {
						Cubix_Email::send($response['sales_email'], 'Milan Expo Paid', $body);
					}


					header("HTTP/1.1 200 OK");
					die('<html><head></head><body>OK</body></html>');

					break;
				default;
			}
		} catch(Exception $e) {
			$err_body = var_export($_POST, true);
			$err_body .= "\n\n";
			$err_body .= 'From application id '. $application_id;
			$err_body .= "\n\n";
			$err_body .= $e->getMessage();
			Cubix_Email::send('badalyano@gmail.com', 'MMG issue', $err_body);
		}	
		header("HTTP/1.1 200 OK");
		die('<html><head></head><body>OK</body></html>');
	}

	public function faxinCallbackAction(){
	
		$application_id = Cubix_Application::getId();
		Cubix_Api_XmlRpc_Client::setApiKey(Cubix_Application::getById()->api_key);

		$json = file_get_contents('php://input');
		$data = json_decode($json);

		if (!is_numeric($data->amount) || !isset($data->paymentid)){
			header("HTTP/1.0 404 Not Found");
			exit('Invalid Transaction, paymentid does not exists or amount is wrong');
		} 
		
		$payment = Model_Billing_OnlineBilling::getPaymentFaxin($data->paymentid);
		
		if ( !count($payment) ) {
			header("HTTP/1.0 404 Not Found");
			exit('Invalid Transaction, paymentid not found');	
		}

		list($method, $ref, $hash) = explode('-', $payment->reference);
	
        switch ($method) {
            case 'SC3' :
                $callback = array (
                    'transaction_time'	=> $payment->created,
                    'transaction_id'	=> $data->transactionid,
                    'reference'			=> $ref,
                    'hash'				=> $hash,
                    'status'			=> "1",
                    'currency'			=> 'CHF',
                    'amount'			=> $data->amount / 100,
                    'first_name'		=> 'Faxin',
                    'last_name'			=> 'Faxinian',
                    'email'				=> '',
                    'phone_number'		=> '',
                    'ip_address'		=> '',
                    'card_name'			=> '',
                    'card_number'		=> '',
                    'exp_date'			=> '',
                    'sha1'				=> '',
                    'log_data'          => json_encode($data)
                );
	
                $onlineBilling = new Model_Billing_OnlineBilling($callback, false, false);

                $onlineBilling->addCallbackLog();

                if ( $onlineBilling->isTransactionIdExists($callback['transaction_id'], 'faxin') ) {
                    $onlineBilling->updateLogData('Failure. transaction_id:' . $callback['transaction_id'] . ' already exists.');

                    header("HTTP/1.1 200 OK");
					die('<html><head></head><body>ok</body></html>');
                }

                $paid = $onlineBilling->payV3('faxin');

                if($paid) {
                    Model_Billing_OnlineBilling::updatePaymentGatewayFaxin($data->paymentid);
                   // Cubix_Email::send('mihranhayrapetyan@gmail.com', 'faxin payment success '.$data->paymentid, var_export($payment, true));
                }else{
                	Cubix_Email::send('mihranhayrapetyan@gmail.com', 'faxin '.$method.' payment failed '.$data->paymentid, var_export($payment, true));
                	header("HTTP/1.1 200 OK");
					die('<html><head></head><body>ok</body></html>');
                }

                break;
                case 'CA':
					$t_model = new Model_ClassifiedAds();
					
					$package = $t_model->getPackageById($hash);
						
					$tr_data = array(
						'ad_id' => $ref,
						'status' => Model_ClassifiedAds::AD_TRANSFER_STATUS_CONFIRMED,
						'first_name' => 'faxin' ,
						'last_name' => '',
						'transaction_id' => $payment->transactionid,
						'amount' => $data->amount  / 100,
						'application_id' => Cubix_Application::getId(),
						//'backend_user_id' => $be_user->id,
						'date_confirmed' => new Zend_Db_Expr('NOW()'),
						'package_name' => $package->name,
						'package_price' => $package->price,
						'package_period' => $package->period,
						'is_paid' => 1
					);

					$t_model->makePremium($ref, $package->period);
					
					$t_model->addTransfer($tr_data);
					
					
					$body = 'CA paid: ' . $ref;
					$callback = array(
                        'transaction_time' => date('Y-m-d H:i:s'),
                        'transaction_id' => $payment->transactionid,
                        'reference' => $ref,
                        'currency' => 'CHF',
                        'amount' => $data->amount  / 100,
                        'first_name' => "",
                        'last_name' => "",
                        'log_data' => json_encode($data),
                        'data' => $body
                    );

					Model_Billing_OnlineBilling::insertCallbackLog($callback);

					Model_Billing_OnlineBilling::updatePaymentGatewayFaxin($data->paymentid);
                   	
                   	header("HTTP/1.1 200 OK");
					die('<html><head></head><body>ok</body></html>');
					break;
                case 'BA':
					
					Cubix_Email::send('mihranhayrapetyan@gmail.com', 'faxin payment BA '.$data->paymentid, var_export($payment, true));
					Model_Billing_OnlineBilling::updatePaymentGatewayFaxin($data->paymentid);
		     		
		     		header("HTTP/1.1 200 OK");
					die('<html><head></head><body>ok</body></html>');
					break;
            default;
        }

		header("HTTP/1.1 200 OK");
		die('<html><head></head><body>ok</body></html>');
	}
	
	public function testMmgAction()
	{
		$method = 'SC';
		try{
			
			$callback = array(
				'transaction_time'	=> date('Y-m-d H:i:s'),
				'transaction_id'	=> 'xxxxxx-'.rand(1,10000),
				'reference'			=> '9199',
				'hash'				=> 'zzzzzz',
				'currency'			=> 'EUR',
				'amount'			=>  10,
				'first_name'		=> 'test',
				'last_name'			=> 'test',
				'zipcode'			=> 'not provided',
				'city'				=> 'not provided',
				'country_code'		=> 'not provided',
				'phone_number'		=> 'not provided',
				'ip_address'		=> '8.8.8.8',
				'card_name'			=> '',
				'card_number'		=> '',
				'email'				=> '',
				'exp_date'			=> 'not provided'
			);
			
			$onlineBilling = new Model_Billing_OnlineBilling($callback, false);
			$onlineBilling->addCallbackLog();
			
			//check if transfer already exists
			if ( $onlineBilling->isTransactionIdExists($callback['transaction_id']) ) {
				$onlineBilling->updateLogData('Failure. transaction_id:' . $callback['transaction_id'] . ' already exists.');
				header("HTTP/1.1 200 OK");
				die('<html><head></head><body>OK</body></html>');
			}
			
			switch($method) {
				case 'SC': 
					$onlineBilling->pay('mmgbill');
					break;
				
				default;
			}
		} catch(Exception $e) {
			$err_body = var_export($_POST, true);
			$err_body .= "\n\n";
			$err_body .= 'From application id HUMP';
			$err_body .= "\n\n";
			$err_body .= $e->getMessage();
			var_dump($err_body);die;
		}	
		header("HTTP/1.1 200 OK");
		die('<html><head></head><body>OK</body></html>');
	}
	// </editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="Powecash PAYMENT">
	public function powercashCallbackAction()
	{
		
		$req = $this->_request;
		$application_id = Cubix_Application::getId();
		Cubix_Api_XmlRpc_Client::setApiKey(Cubix_Application::getById()->api_key);
		//Cubix_Email::send('badalyano@gmail.com', 'Powercash callback', var_export($_REQUEST, true));
		
		if($req->errorcode != 0 && $req->errorcode != 2000){
			header("HTTP/1.1 200 OK");
			die('<html><head></head><body>OK</body></html>');
		}
		
		list($method, $ref, $hash) = explode('-', $req->orderid);
		$payment_model = new Model_Billing_EcardonGateway();
		$result = $payment_model->getStatus($req->transactionid);
		$origin_data = unserialize($result);
		
		try{
			switch($method) {
				case 'SC': 
										
					$callback = array(
						'transaction_time'	=> date('Y-m-d H:i:s'),
						'transaction_id'	=> $req->transactionid,
						'reference'			=> $ref,
						'hash'				=> $hash,
						'currency'			=> $origin_data['currency'],
						'amount'			=> $origin_data['amount'],
						'first_name'		=> 'not provided',
						'last_name'			=> 'not provided',
						'card_name'			=> $req->card_type,
						'card_number'		=> $req->bin . 'XXXXXXXX' . $req->ccn_four,
						'exp_date'			=> $req->expiry_month .'/'. $req->expiry_year,
						'log_data'          => json_encode($_REQUEST)
					);
					
					$onlineBilling = new Model_Billing_OnlineBilling($callback, false, false);
					$onlineBilling->addCallbackLog();

					if ( $onlineBilling->isTransactionIdExists($callback['transaction_id']) ) {
						$onlineBilling->updateLogData('Failure. transaction_id:' . $callback['transaction_id'] . ' already exists.');
						header("HTTP/1.1 200 OK");
						die('<html><head></head><body>OK</body></html>');
					}

					$onlineBilling->pay('powercash');
					break;
				case 'gotd':
					$additional_info = array (
						'transaction_id'	=> $req->transactionid,
						'payment_system'	=> 'powercash',
					);
					
					$client = new Cubix_Api_XmlRpc_Client();
					$response = $client->call('Billing.confirmGotd', array($ref, $application_id, $additional_info));

					if ( $response ) {
						$activation_date = implode("\n", explode(',', $response['activation_date']));
						$body = 'Escort "' . $response['showname'] . '" ID:' . $response['id'] . ' has just payed for GOTD IN ' . $response['city'] . "\n" . $activation_date;
					} else {
						$body = 'Error occured in confirmation. gotd_order_id: ' . $ref;
					}

					$callback = array(
						'transaction_time'	=> date('Y-m-d H:i:s'),
						'transaction_id'	=> $req->transactionid,
						'reference'			=> $ref,
						'currency'			=> $origin_data['currency'],
						'amount'			=> $origin_data['amount'],
						'first_name'		=> $req->name,
						'last_name'			=> 'Powercash GOTD',
						'zipcode'			=> 'not provided',
						'city'				=> 'not provided',
						'country_code'		=> 'not provided',
						'phone_number'		=> 'not provided',
						'card_name'			=> $req->card_type,
						'card_number'		=> $req->bin . 'XXXXXXXX' . $req->ccn_four,
						'exp_date'			=> $req->expiry_month .'/'. $req->expiry_year,
						'data'               => $body,
						'log_data'          => json_encode($_REQUEST)
					);

					Model_Billing_OnlineBilling::insertCallbackLog($callback);
					
					if ( $response['sales_email'] && strlen($response['sales_email']) ) {
						Cubix_Email::send($response['sales_email'], 'Gotd Paid', $body);
					}

					if($application_id == APP_A6) {
						if ( $response['email'] && strlen($response['email']) ) {
							$cities = explode(',', $response['group_city']);
							$dates = explode(',', $response['activation_date']);
							foreach ($dates as $i => $date) {
								Cubix_Email::sendTemplate('gotd_purchase_confirm', $response['email'], array(
									'city' => $cities[$i],
									'date' => $date
								));
							}
						}
					}
				
					break;
				case 'pbump' :
						$additional_info = array (
							'transaction_id'	=> $req->transactionid,
							'payment_system'	=> 'powercash',
						);
						/*$additionalInfo = array(
                            'transaction_id'    => $transaction['data']['id'],
                            'payment_system'    => 'powercash',
                            'first_name'        => $transaction['data']['cardHolderName']
                        );*/

                        $applicationId = Cubix_Application::getId();
                        $client = new Cubix_Api_XmlRpc_Client();

                        $response = $client->call('OnlineBillingV2.confirmProfileBump', array($ref, $applicationId, $additional_info));
                       
                        if ( $response ) {
                            $body = 'Escort "' . $response['showname'] . '" ID:' . $response['id'] . ' has just payed for BUMP IN ' . $response['city'] . "\n" ;
                        } else {
                            $body = 'Error occured in confirmation. profile bump order id: ' . $response['id'];
                        }

						$callback = array(
							'transaction_time'	=> date('Y-m-d H:i:s'),
							'transaction_id'	=> $req->transactionid,
							'reference'			=> $ref,
							'currency'			=> $origin_data['currency'],
							'amount'			=> $origin_data['amount'],
							'first_name'		=> $req->name,
							'last_name'			=> 'Powercash GOTD',
							'zipcode'			=> 'not provided',
							'city'				=> 'not provided',
							'country_code'		=> 'not provided',
							'phone_number'		=> 'not provided',
							'card_name'			=> $req->card_type,
							'card_number'		=> $req->bin . 'XXXXXXXX' . $req->ccn_four,
							'exp_date'			=> $req->expiry_month .'/'. $req->expiry_year,
							'data'               => $body,
							'log_data'          => json_encode($_REQUEST)
						);
						
                        Model_Billing_OnlineBilling::insertCallbackLog($callback);

                        switch ($response['frequency']) {
                        	case 'bump-option-1':
                        		$frequency_text = __('bump_per_hour', array('hour'=> 4));
                        		break;
                        	case 'bump-option-2':
                        		$frequency_text = __('bump_per_hour', array('hour'=> 8));
                        		break;
                        	case 'bump-option-3':
                        		$frequency_text = __('bump_per_hour', array('hour'=> 12));
                        		break;
                        	case 'bump-option-4':
                        		$frequency_text = __('bump_2_per_day');
                        		break;
                        	case 'bump-option-5':
                        		$frequency_text = __('bump_1_per_day');
                        		break;
                        	case 'bump-option-6':
                        		$frequency_text = __('not_avaiable');
                        		break;
                        	default:
                        		$frequency_text = '';
                        		break;
                        }
                        
                        $email_model = new Model_SMS_EmailOutbox();
	
                        $log_data = array(
							'email' =>  $response['user_email'],
							'subject' => 'Bump Acquistato',
							'text' => 'Cara '.addslashes($response['username']).', Hai acquistato l\'opzione bump per il tuo profilo su escortforumit.xxx. Numero bump: '.addslashes($response['bumps_count']).', frequenza '.addslashes($frequency_text).'
							 Ad ogni bump, il tuo annuncio viene spinto in cima alla categoria del tuo pacchetto, fino a quando un nuovo annuncio verrà inserito nella stessa categoria.
							Per qualsiasi domanda, si prega di contattare info@escortforumit.xxx o il proprio agente di vendita',
							'backend_user_id' => 1,
							'date' => new Zend_Db_Expr('NOW()'),
							'user_id' => $response['user_id'],
							'application_id' => $applicationId
						);

						$email_model->add($log_data);

                        Cubix_Email::sendTemplate('bump_is_purchased', $response['user_email'], array(
                            'username' => $response['username'],
                            'bumps_count' =>  $response['bumps_count'],
                            'frequency' =>  $frequency_text,
                        ));				
				default;
			}
		} catch(Exception $e) {
			$err_body = var_export($_POST, true);
			$err_body .= "\n\n";
			$err_body .= 'From application id '. $application_id;
			$err_body .= "\n\n";
			$err_body .= $e->getMessage();
			Cubix_Email::send('badalyano@gmail.com', 'Powercash issue', $err_body);
		}	
		header("HTTP/1.1 200 OK");
		die('<html><head></head><body>OK</body></html>');
	}
	// </editor-fold>
	
	public function sc3IvrCallbackAction()
	{
		//This callback is emulated from front
		//index controller, sc3IvrCallbackAction
		$ref = $this->_request->ref;
		$hash = $this->_request->hash;
		$pid = $this->_request->pid;
		
		$ivrpays = Model_Billing_OnlineBilling::getIvrStoredCallbacks();
		if ( ! count($ivrpays) ) {
			echo 'There are no saved IVR callbacks to check.' . "\n";
		}
		
		foreach($ivrpays as $ivr) {
					
			$callback = array (
				'transaction_time'	=> '',
				'transaction_id'	=> $ivr->pid,
				'reference'			=> $ivr->user_id,
				'hash'				=> $ivr->hash,
				'status'			=> "200",
				'currency'			=> 'EUR',
				'amount'			=> $ivr->amount,
				'first_name'		=> 'IVR',
				'last_name'			=> 'IVR',
				'address'			=> '',
				'zipcode'			=> '',
				'city'				=> '',
				'country_code'		=> '',
				'email'				=> '',
				'phone_number'		=> '',
				'ip_address'		=> '',
				'card_name'			=> '',
				'card_number'		=> '',
				'exp_date'			=> '',
				'is_phone_billing'	=> 1,
				'sha1'				=> ''/*sha1(Model_Billing_OnlineBilling::$secret_prefixes[Cubix_Application::getId()] . $result['Amount'] . 'EUR' . 'SC3-' . $ref)*/
			);

			$onlineBilling = new Model_Billing_OnlineBilling($callback, false, false);

			try {
				$conf = Zend_Registry::get('api_config');
				Cubix_Api_XmlRpc_Client::setApiKey(Cubix_Application::getById(Zend_Controller_Front::getInstance()->getRequest()->application_id)->api_key);
				Cubix_Api_XmlRpc_Client::setServer($conf['server']);
			}
			catch (Exception $ex) {

			}

			$onlineBilling->addCallbackLog();

			if ( $onlineBilling->isTransactionIdExists($callback['transaction_id'], 'ivr') ) {
				$onlineBilling->updateLogData('Failure. transaction_id:' . $callback['transaction_id'] . ' already exists.');
				Model_Billing_OnlineBilling::setIvrCallbackStatus($ivr->id, 2);
				die('transaction_id already exists');
			}	

			$paymentStatus = $onlineBilling->payV3('ivr', $p_info);

			if(!$paymentStatus){
				Model_Billing_OnlineBilling::setIvrCallbackStatus($ivr->id, 2);
				die('payv3 issue');
			}
			
			if(Cubix_Application::getId() != APP_A6) {
				try {
					$config = Zend_Registry::get('system_config');

					if (!in_array($p_info, array(1, 2))) $p_info = null;

					$client = new Cubix_Api_XmlRpc_Client();
					$to_numbers = $client->call('Billing.getSmsReceivers', array($p_info));

					if ( /*$config['smsNotificationNumbers'] &&  strlen($config['smsNotificationNumbers'])*/count($to_numbers) ) {
						//$to_numbers = explode(',', $config['smsNotificationNumbers']);
						$text = 'just paid for ad.';
						$this->_sendSMSToNumbers($to_numbers, $text);
					}
				} catch(Exception $ex) {
					echo('error in sending sms');
				}
			}

			
			Model_Billing_OnlineBilling::setIvrCallbackStatus($ivr->id, 1);
		}
		
		die('sta=OK');
	}
	
	public function checkTokenAction()
	{
		$token = $this->_request->token;
		
		if ( ! $token ) {
			die('provide token');
		}
		
		$epg_payment = new Model_Billing_EpgGateway();
		$result = $epg_payment->getTransactionStatus($token);
		
		var_dump($result);die;
	}

    /**
     * This action, will created transfers/orders etc. for a single transaction
     * that has been made on Twispay.com
     * Just pass transactionID from twispay as a GET parameter.
     *
     * @author Eduard Hovhannisyan
     * @return void
     */
    public function tokenizeTwispaySingleAction()
    {
        $transactionId = $this->_request->transactionId;

        if (!is_numeric($transactionId)) exit('Invalid Transaction ID');

        $fakeToken = (object)array('id' => 0, 'token' => json_encode(['transactionId' => $transactionId]));

        $this->runTwispayTokenizerAction($fakeToken);
        exit;
    }
		
	public function testSmsAction()
	{
		$text = 'You got me ';
		$to_numbers = ['0037499555538', '0037499555538'];
		$this->_sendSMSToNumbers($to_numbers, $text);
		die('done');
	}
	
	private function _sendSMSToNumbers($to_numbers, $text)
	{
		$config = Zend_Registry::get('system_config');
		$originator = $phone_from = Cubix_Application::getPhoneNumber(Cubix_Application::getId());
		$sms_config = $config['sms'];

		$SMS_USERKEY = $sms_config['userkey'];
		$SMS_PASSWORD = $sms_config['password'];

		foreach ( $to_numbers as $phone_to ) {
			//sms info
			echo($phone_to . ';');
			$sms = new Cubix_SMS($SMS_USERKEY, $SMS_PASSWORD);
			$sms->setOriginator($originator);
			$sms->setRecipient($phone_to, md5(microtime()));
			$sms->setContent($text);
			$sms->sendSMS();
		}
	}

}
