<?php

class Billing_PaymentMethodesController extends Zend_Controller_Action
{
    /**
     * @var Model_Billing_PaymentMethodes
     */
    protected $_model;

    public function init()
    {
        $this->_model = new Model_Billing_PaymentMethodes();
    }

    public function indexAction()
    {
        $model = new Model_Escorts();

        // $escort = $model->get(10988/*131374*/)->processDefaultPackages();


        /* FILTER PARAMS */
        $ses_filter = new Zend_Session_Namespace('billing_packages_data');
        $this->view->filter_params = $ses_filter->filter_params;
        /* FILTER PARAMS */
    }

    public function dataAction()
    {
        $filter = array(
            'application_id' => $this->_request->application_id
        );

        $data = $this->_model->getAll(
            $this->_request->page,
            $this->_request->per_page,
            $filter,
            $this->_request->sort_field,
            $this->_request->sort_dir,
            $count
        );

        die(json_encode(array('data' => $data, 'count' => $count)));
    }

    public function toggleActiveAction()
    {
        $id = intval($this->_request->id);
        if ( ! $id ) die;
        $this->_model->toggle($id);
    }
}
