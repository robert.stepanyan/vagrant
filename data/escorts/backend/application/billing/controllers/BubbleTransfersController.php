<?php

class Billing_BubbleTransfersController extends Zend_Controller_Action
{
	/**
	 * @var Model_Billing_Transfers
	 */
	protected $_model;

	public function init()
	{
		$this->view->model = $this->_model = new Model_Billing_BubbleTransfers();
	}

	public function indexAction()
	{
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('billing_bubble_transfers_data');
		$this->view->filter_params = $ses_filter->filter_params;
		/* FILTER PARAMS */

		/*$this->view->transfer_data = array(
			'total' => number_format($this->_model->getTotalTransfers(), 2),
			'paid' => number_format($this->_model->getTotalTransfersPaid(), 2)
		);*/
	}

	public function dataAction()
	{
		$req = $this->_request;

		$data = new Cubix_Form_Data($this->_request);
		$data->setFields(array(
			'filter_by' => '',
			'date_from' => 'int',
			'date_to' => 'int',
			'showname' => '',
			'payment_id' => '',
		));
		$data = $data->getData();
		
		
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		$filter = array();
	
		if ( $data['showname'] ) {
			$filter['e.showname'] = $data['showname'];
		}
		if ( $data['payment_id'] ) {
			$filter['t.payment_id'] = $data['payment_id'];
		}
		
		if ( $data['date_from'] ) {
			$filter['date_from'] = $data['date_from'];
		}
		if ( $data['date_to'] ) {
			$filter['date_to'] = $data['date_to'];
		}

		$data = $this->_model->getAll(
			$filter,
			$req->page,
			$req->per_page,
			$req->sort_field,
			$req->sort_dir
		);

		die(json_encode($data));
	}
}
