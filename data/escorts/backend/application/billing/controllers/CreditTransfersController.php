<?php

class Billing_CreditTransfersController extends Zend_Controller_Action
{
	/**
	 * @var Model_Billing_Orders
	 */
	protected $_model;
		
	public function init()
	{
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		if ( $bu_user->type != 'superadmin' && $bu_user->type != 'admin' ) {
			die('no permission');
		}
		$this->_model = new Model_Billing_Credits();

	}
	
	public function indexAction() 
	{
		
	}	
	
	public function dataAction()
	{
		
		$req = $this->_request;
		
		$filter = array(
			'transaction_id' => trim($req->transaction_id),
			'status' => $req->status,
			'username' => trim($req->username),
			'email' => trim($req->email),
			'date_from' => $req->date_from,
            'date_to' => $req->date_to,
		);
		
		$data = $this->_model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field,
			$this->_request->sort_dir, 
			$count
		);

		if ( $count ) {
			foreach ( $data as $i => $item ) {
				$data[$i]->status = Model_Billing_Tokens::$STATUS_LABELS[$item->status];
			}
		}

		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

	public function detailsAction()
    {
        $this->view->layout()->disableLayout();
       $this->view->data = $this->_model->getDetails( $this->_getParam('request_id' ) );
        
    }
	
	public function rejectAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$request_id = intval($req->id);
		if ( ! $request_id ) die;
		
		$this->view->transfer = $transfer = $this->_model->getDetails($request_id);
		
		if ( ! $transfer ) die;
	
		if ( $req->isPost() ) {
			$comment = $this->_getParam('reject_reason');
			$validator = new Cubix_Validator();

			/*if ( ! strlen($comment) ) {
				$validator->setError('reject_reason', 'You need to leave Reason');
			}*/
			
			if ( $validator->isValid() ) {
				$this->_model->reject($comment, $request_id);
				
			}

			die(json_encode($validator->getStatus()));
		}
	}
}
