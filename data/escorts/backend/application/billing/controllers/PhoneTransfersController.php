<?php

class Billing_PhoneTransfersController extends Zend_Controller_Action
{
	/**
	 * @var Model_Billing_Transfers
	 */
	protected $_model;
	
	const IVR_SALES_ID = 100;
	const IVR_GOTD_SALES_ID = 102;
	const TRANSFER_TYPE_PHONE_BILLING = 7;

	public function init()
	{
		$this->view->model = $this->_model = new Model_Billing_Transfers();
		$this->temp_docs = new Zend_Session_Namespace('temp_docs');
	}

	public function indexAction()
	{
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('billing_transfers_data');
		$this->view->filter_params = $ses_filter->filter_params;
		/* FILTER PARAMS */

		/*$this->view->transfer_data = array(
			'total' => number_format($this->_model->getTotalTransfers(), 2),
			'paid' => number_format($this->_model->getTotalTransfersPaid(), 2)
		);*/
	}

	public function dataAction()
	{
		$req = $this->_request;

		$data = new Cubix_Form_Data($this->_request);
		$data->setFields(array(
			'app_id' => 'int',
			'sales_user_id' => 'int',
			'status' => 'int',
			'filter_by' => '',
			'date_from' => 'int',
			'date_to' => 'int',
			'user_id' => '',
			'status' => 'int',
			
			'phone_status' => '',

			'showname' => '',
			'email' => '',
			'agency_name' => '',

			'sys_order_id' => ''
		));
		
		$filter = $data->getData();
		$filter['transfer_type_id'] = self::TRANSFER_TYPE_PHONE_BILLING;
				
		$data = $this->_model->getAllV2(
			$filter,
			$req->page,
			$req->per_page,
			$req->sort_field,
			$req->sort_dir
		);

		die(json_encode($data));
	}

	public function createAction()
	{
		$this->view->layout()->disableLayout();

		$order_id = intval($this->_request->order_id);

		if ( $order_id ) {
			$model = new Model_Billing_Orders();
			$order = $model->getDetails($order_id);
			if ( $order ) {
				$this->view->order = $order;
			}
		}

		if ( $this->_request->isPost() ) { 
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'user_id' => 'int',
				'orders' => 'arr-int',
				'transfer_type_id' => 'int',
				'date_transfered' => 'int',
				'amount' => 'double',
				'first_name' => '',
				'last_name' => '',
				'country_id' => 'int',
				'city' => '',
				'var_symbol' => '',
				'transaction_id' => '',
				'pull_person_id' => 'int',
				'webmoney_account' => 'int',
				'mtcn' => '',
				'comment' => 'notags|special',
				'application_id' => $this->_request->application_id
			));
			$data = $data->getData();

			$validator = new Cubix_Validator();

			if ( ! $data['user_id'] ) {
				$validator->setError('global_error', 'Please select an agency or escort');
			}

			if ( ! $data['transfer_type_id'] ) {
				$validator->setError('transfer_type_id', 'Required');
			}

			if ( ! $data['date_transfered'] ) {
				$validator->setError('date_transfered', 'Required');
			}

			if ( ! $data['amount'] ) {
				$validator->setError('amount', 'Required');
			}

			if ( $data['transfer_type_id'] == 5 ) {
				if ( ! $data['webmoney_account'] ) {
					$validator->setError('webmoney_account', 'Required');
				}
				
				if ( ! $data['transaction_id'] ) {
					$validator->setError('transaction_id', 'Required');
				}
			}

			if ( $data['transfer_type_id'] != 5 ) {
				if ( $data['transfer_type_id'] && ! $data['first_name'] ) {
					$validator->setError('first_name', 'Required');
				}

				if ( $data['transfer_type_id'] && ! $data['last_name'] ) {
					$validator->setError('last_name', 'Required');
				}
			}

			$data['backend_user_id'] = Zend_Auth::getInstance()->getIdentity()->id;

			if ( $data['transaction_id'] ) {
				if ( false === $this->_model->checkTransactionId($data['transaction_id']) ) {
					$validator->setError('transaction_id', 'Already in database');
				}
			}

			if ( $validator->isValid() ) {
				if ( is_array($data['orders']) ) {
					$orders_model = new Model_Billing_Orders();
					$subtotal = 0;
					foreach ( $data['orders'] as $order_id ) {
						$price = $orders_model->getPrice($order_id);
						if ( is_null($price) ) continue;
						$subtotal += doubleval($price);
					}
					
					$user = new Model_UserItem(array('id' => $data['user_id']));
					if ( $subtotal > doubleval($data['amount']) + $user->getBalance()/* && $subtotal > 0*/ ) {
						$validator->setError('limited', 'Subtotal is more than balance');
					}

					/*if ( $subtotal == 0 ) {
						$validator->unsetError('amount');
					}*/
				}
			}

			if ( $validator->isValid() ) {
				if ( true !== ($result = $this->_model->save($transfer = new Model_Billing_TransferItem($data))) ) {
					$validator->setError('global_error', $result);
				}
				else {
					header('Cubix-Message: ' . 'Transfer Saved!');
				}
			}

			die(json_encode($validator->getStatus()));
		}
		else{
			if(isset($this->temp_docs->images)){

			/*	foreach ( $this->temp_docs->images as $photo ) {
					$images[] = new Cubix_Images_Entry(array(
						'application_id' => Cubix_Application::getId(),
						'catalog_id' => 'transfers',
						'hash' => $photo->hash,
						'ext' => $photo->ext
					));
			}
			$images_model = new Cubix_Images();
			$images_model->remove($images);*/
			unset($this->temp_docs->images);
				
			}
		}
	}

	public function detailsAction()
	{
		$this->view->layout()->disableLayout();

		$id = intval($this->_getParam('id'));

		$transfer = $this->_model->getDetails($id);
		$this->view->transfer = $transfer;
		$this->view->transfer_imgs = $this->_model->getTransferImgsById($id);
		
	}

	public function editAction()
	{
		$this->view->layout()->disableLayout();

		$id = intval($this->_request->id);

		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
			));
			$data = $data->getData();
			$data['application_id'] = $this->_request->application_id;

			$validator = new Cubix_Validator();

			if ( $validator->isValid() ) {
				$this->_model->save(new Model_Billing_TransferItem());
			}

			die(json_encode($validator->getStatus()));
		}
	}

	public function deleteAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;

		$transfer = $this->_model->get($id);
		if ( ! $transfer ) die;

		if ( Model_Billing_Transfers::STATUS_REJECTED == $transfer->status ) {
			$transfer->delete();
		}

		die;
	}

	public function rejectAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$transfer_id = intval($req->id);
		if ( ! $transfer_id ) die;
		
		$this->view->transfer = $transfer = $this->_model->getDetails($transfer_id);
		if ( ! $transfer ) die;
	
//print_r($transfer->getOrders());die;
		//print_r($transfer->getActivePackages());

		$comment = $this->_getParam('reject_reason');

		if ( $req->isPost() ) {

			$validator = new Cubix_Validator();

			/*if ( strlen($req->cancel_packages) && count($req->selected) == 0 ) {
				$validator->setError('select_to_reject', 'You need to select orders to reject');
			}*/

			//$selected_orders = $req->selected;

			if ( $validator->isValid() ) {
				if ( Model_Billing_Transfers::STATUS_PENDING == $transfer->status || Model_Billing_Transfers::STATUS_CONFIRMED == $transfer->status ) {
					$transfer->reject($comment, $this->_request->cancel_packages);
					header('Cubix-Message: Transfer successfully rejected!');
				}
			}

			die(json_encode($validator->getStatus()));
		}
	}

	public function confirmAction()
	{
		$this->view->layout()->disableLayout();
		
		$id = intval($this->_request->id);
		if ( ! $id ) die;

		$this->view->transfer = $transfer = $this->_model->getDetails($id);
		if ( ! $transfer ) die;
		$this->view->transfer_imgs = $this->_model->getTransferImgsById($id);
		$comment = $this->_getParam('comment');
		$amount = doubleval(doubleval(str_replace(',', '', $this->_getParam('amount'))));

		if ( $this->_request->isPost() ) {
			$validator = new Cubix_Validator();

			if ( ! $amount ) {
				$validator->setError('amount', 'Required');
			}

			if ( $validator->isValid() ) {
				if ( (Model_Billing_Transfers::STATUS_PENDING == $transfer->status) || (Model_Billing_Transfers::STATUS_REJECTED == $transfer->status) ) {
					$transfer->confirm($comment, $amount);
					header('Cubix-Message: Transfer successfully confirmed!');
				}
			}

			die(json_encode($validator->getStatus()));
		}
	}

	public function uploadImgAction()
	{
		try {
			// Save on remote storage
			$images = new Cubix_Images();
			$image = $images->save($_FILES['Filedata']['tmp_name'], 'transfers', Cubix_Application::getId(), strtolower(@end(explode('.', $_FILES['Filedata']['name']))));
			
			//$transfer_img_id = $this->_model->setImg($image);
			$transfer_img_id = count($this->temp_docs->images);
			$this->temp_docs->images[] = $image;
			$result = array(
				'status' => 'success',
				'transfer_img_id' => $transfer_img_id,
				'img_url' => $image['ext'] == 'pdf' ? '/img/cubix-uploader/pdf-icon.jpg': $this->_model->getImgUrl($image,'agency_p100')
			);
		} catch (Exception $e) {
			$result = array('status' => 'error', 'msg' => '');

			$result['msg'] = $e->getMessage();
		}

		die(json_encode($result));
	}

	public function removeImgAction()
	{
		$ids = $this->_getParam('photos');

		foreach ( $ids as $i => $id ) {
					$ids[$i] = intval($id);
					if ( ! $ids[$i] ) continue;
				}
		try {
		$images = array();

		foreach ( $ids as $id ) {
			$photo = $this->temp_docs->images[$id];
			$images[] = new Cubix_Images_Entry(array(
				'application_id' => Cubix_Application::getId(),
				'catalog_id' => 'transfers',
				'hash' => $photo['hash'],
				'ext' => $photo['ext']
			));			
			$this->temp_docs->images[$id] = null;
		}
		
		$images_model = new Cubix_Images();
		$images_model->remove($images);
		$result = array('success' => true);

		}catch (Exception $e) {
			$result = array('status' => 'error', 'msg' => '');
			$result['msg'] = $e->getMessage();
		}
		die(json_encode($result));

	}
	
	public function exportAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		
		$req = $this->_request;

		$data = new Cubix_Form_Data($this->_request);
		$data->setFields(array(
			'transfer_type_id' => 'int',
			'transaction_id' => '',
			'mtcn' => '',
			'var_symbol' => '',
			'first_name' => '',
			'last_name' => '',
			'pull_person_id' => '',


			'app_id' => 'int',
			'sales_user_id' => 'int',
			'status' => 'int',
			'filter_by' => '',
			'date_from' => 'int',
			'date_to' => 'int',
			'user_id' => '',
			'status' => 'int',

			'showname' => '',
			'email' => '',
			'agency_name' => '',

			'sys_order_id' => ''
		));
		$data = $data->getData();

		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		// <editor-fold defaultstate="collapsed" desc="Filter">
		$filter = array();

		$filter['t.application_id'] = $this->_request->application_id;

		if ( $data['sys_order_id'] ) {
			$filter['o.system_order_id = ?'] = $data['sys_order_id'];
		}

		if ( $data['showname'] ) {
			$filter['e.showname LIKE ?'] = $data['showname'] . '%';
		}

		if ( $data['email'] ) {
			$filter['u.email LIKE ?'] = $data['email'] . '%';
		}

		if ( $data['agency_name'] ) {
			$filter['a.name LIKE ?'] = $data['agency_name'] . '%';
		}
		if ( $data['transfer_type_id'] ) {
			$filter['t.transfer_type_id = ?'] = $data['transfer_type_id'];
		}
		if ( $data['transaction_id'] ) {
			$filter['t.transaction_id = ?'] = $data['transaction_id'];
		}
		if ( $data['var_symbol'] ) {
			$filter['t.var_symbol = ?'] = $data['var_symbol'];
		}
		if ( $data['mtcn'] ) {
			$filter['t.mtcn = ?'] = $data['mtcn'];
		}
		if ( $data['first_name'] ) {
			$filter['t.first_name = ?'] = $data['first_name'];
		}
		if ( $data['last_name'] ) {
			$filter['t.last_name = ?'] = $data['last_name'];
		}
		if ( $data['pull_person_id'] ) {
			$filter['t.pull_person_id = ?'] = $data['pull_person_id'];
		}


		if ( $bu_user->type == 'superadmin' ) {
			if ( $data['sales_user_id'] ) {
				$filter['t.backend_user_id = ?'] = $data['sales_user_id'];
			}
		}
		else {
			if ( $bu_user->type == 'sales manager' || $bu_user->type == 'sales clerk' ) {
				$filter['t.backend_user_id = ?'] = $bu_user->id;
				//$filter['t.backend_user_id = ?'] = $data['sales_user_id'];
			}
			else if ($bu_user->type == 'admin') {
				$filter['bu.application_id = ?'] = $bu_user->application_id;
				$filter['t.backend_user_id = ?'] = $data['sales_user_id'];
			}
		}

		if ( $data['status'] ) {
			$filter['t.status = ?'] = $data['status'];
		}

		$data['filter_by'] = 'date_transfered';
		if ( $data['filter_by'] ) {
			if ( in_array($data['filter_by'], array('date_transfered')) ) {
				if ( $data['date_from'] ) {
					$filter['DATE(' . $data['filter_by'] . ') >= ?'] = new Zend_Db_Expr('DATE(FROM_UNIXTIME(' . $data['date_from'] . '))');
				}

				if ( $data['date_to'] ) {
					$filter['DATE(' . $data['filter_by'] . ') <= ?'] = new Zend_Db_Expr('DATE(FROM_UNIXTIME(' . $data['date_to'] . '))');
				}
			}
		}

		if ( strlen($data['user_id']) > 0 ) {
			$filter['u.id = ?'] = $data['user_id'];
		}
		// </editor-fold>
		
		$page = $req->page ? (int) $req->page : null;
		$per_page = $req->per_page ? (int) $req->per_page : null;
		$sort = $req->sort_field ? $req->sort_field : 't.date_transfered';
		$dir = $req->dir ? $req->dir : 'DESC';
		
		$data = $this->_model->getDataForExport(
			$filter,
			$page,
			$per_page,
			$sort,
			$dir
		);
		
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment; filename=transfers.csv');
		
		$handle = fopen('php://output', 'w');
		foreach ( $data['data'] as $order ) {
			fputcsv($handle, array(
				$order['user_type'], $order['username'], $order['transfer_type'], $order['status'], $order['full_name'],
				$order['orders'], $order['date_transfered'], $order['payment_date'], $order['paid_amount'],
				$order['sales_person']
			));
		}
		fclose($handle);
		
		exit;
	}
}
