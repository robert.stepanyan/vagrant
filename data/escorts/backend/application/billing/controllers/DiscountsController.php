<?php

class Billing_DiscountsController extends Zend_Controller_Action
{
	/**
	 * @var Model_Billing_Discounts
	 */
	protected $_model;

    public function init()
    {
        $this->model = new Model_Discounts();
    }

    public function indexAction()
    {

    }

    public function dataAction()
    {
        $req = $this->_request;
        $type = $req->type?$req->type:null;

        $filter = array(
            'title' => $req->title,
            'description' => $req->description,
            'promo_code' => $req->promo_code,
            'percent' => $req->percent,
            'amount' => $req->amount,
            'date_from' => $req->c_date_from,
            'date_to' => $req->c_date_to,
            'status' => $req->status,
            'escort_id' => $req->escort_id,
            'order_id' => $req->order_id,
            'type' => $type,
            'days' => $req->days,
        );

        $data = $this->model->getAll(
            $this->_request->page,
            $this->_request->per_page,
            $filter,
            $this->_request->sort_field,
            $this->_request->sort_dir,
            $count
        );



        $this->view->data = array(
            'data' => $data,
            'count' => $count
        );
    }

    public function createAction()
    {
        $req = $this->_request;

        if ( $req->isPost() )
        {
            $validator = new Cubix_Validator();
            $bu_user = Zend_Auth::getInstance()->getIdentity();

            $title = $req->getParam('title');
            $description = $req->getParam('description');
            $promo_code = $req->getParam('promo_code_create');
            $percent = $req->getParam('percent');
            $amount = $req->getParam('amount');
            $type = $req->getParam('create_type');
            $days = $req->getParam('days');

            if ( ! $title ) {
                $validator->setError('title', 'Plaese add a title.');
            }

            if ( ! $description ) {
                $validator->setError('description', 'Required');
            }

            if ( !$percent && !$amount && !$days ) {
                $validator->setError('type', 'Please add a percent or amount or days of discount');
            }

            if ( ! $promo_code ) {
                $validator->setError('promo_code', 'Required');
            }

            if ( !$type )
                $type = DISCOUNT_FOR_EVERYONE;

            $data = array(
                'title' => $title,
                'description' => $description,
                'promo_code' => $promo_code,
                'percent' => $percent,
                'amount' => $amount,
                'bo_user' => $bu_user->id,
                'type' => $type,
                'days' => $days
            );

            if ( $validator->isValid() ) {
                $this->model->save($data);
            }

            die(json_encode($validator->getStatus()));
        }
    }

    public function editAction()
    {
        $req = $this->_request;
        $discount = $this->model->get($this->_request->id);
        $this->view->discount = $discount;

        if ( $req->isPost() )
        {
            $validator = new Cubix_Validator();
            $bu_user = Zend_Auth::getInstance()->getIdentity();

            $id = $req->id;
            $title = $req->title;
            $description = $req->description;
            $promo_code = $req->promo_code_edit;
            $percent = $req->percent;
            $amount = $req->amount;
            $type = $req->type;
            $days = $req->days;

            if ( ! $title ) {
                $validator->setError('title', 'Plaese add a title.');
            }

            if ( ! $description ) {
                $validator->setError('description', 'Required');
            }

            if ( !$percent && !$amount && !$days) {
                $validator->setError('type', 'Please add a percent or amount or days of discount');
            }

            if ( ! $promo_code ) {
                $validator->setError('promo_code', 'Required');
            }

            if ( !$type )
                $type = DISCOUNT_FOR_EVERYONE;

            $data = array(
                'title' => $title,
                'description' => $description,
                'promo_code' => $promo_code,
                'percent' => $percent,
                'amount' => $amount,
                'bo_user' => $bu_user->id,
                'type' => $type,
                'days' => $days
            );

            if ( $validator->isValid() ) {
                $this->model->update($data,$id);
            }

            die(json_encode($validator->getStatus()));
        }
    }

    public function removeAction()
    {
        $req = $this->_request;

        $id = $req->id;

        $this->model->remove($id);
    }

    public function toggleActiveAction()
    {
        $id = intval($this->_request->id);
        if ( ! $id ) die;
        $this->model->toggle($id, 'active');
    }

}
