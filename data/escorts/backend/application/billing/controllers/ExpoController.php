<?php

class Billing_ExpoController extends Zend_Controller_Action
{
	/**
	 * @var Model_Billing_Packages
	 */
	protected $_model;

	public function init()
	{
		$this->_model = new Model_Billing_Expo();
	}

	public function indexAction()
	{
		
	}

	public function dataAction()
	{
		$req = $this->_request;

		$filter = array(
			'e.showname' => $req->showname,
			'e.id' => $req->escort_id,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to,
			'order_id'	=> $req->order_id,
			'transaction_id'	=> $req->transaction_id,
			'sales_user_id'	=> $req->sales_user_id
		);

		$data = $this->_model->getAll(
			$this->_request->page,
			$this->_request->per_page,
			$filter,
			$this->_request->sort_field,
			$this->_request->sort_dir,
			$count,
			true
		);
		
		foreach($data as $i => $d) {
			$data[$i]->editable = true;
			if ( $data[$i]->start_date <  time() ) {
				$data[$i]->editable = false;
			}
		}
		
		die(json_encode(array('data' => $data, 'count' => $count)));
	}
	
	public function addAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		$m_package = new Model_Billing_Packages();
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		
		$op_id = $this->view->op_id = $req->order_package_id;
		$package = $m_package->get($op_id);

		$escort_id = $package->escort_id;
		
		$application_id = Cubix_Application::getId();
		
		$working_locations = Model_EscortsV2::getWorkingLocations($escort_id);
		
		$has_milan = false;
		foreach($working_locations as $wl) {
			if ( $wl->id == 758 ) {
				$has_milan = true;
				break;
			}
		}
		
		if ( ! $has_milan ) {
			die('Doesn\'t have Milan in working locations');
		}
		
		$this->view->price = $price = $client->call('OnlineBillingV2.getExpoProductPrice', array($package->package_id));
		
		if ( $this->_request->isPost() ) { 
			$booked_expo_orders = $client->call('OnlineBillingV2.getEscortExpoOrders', array($escort_id));
			
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'user_id' => 'int',
				'orders' => 'arr-int',
				'transfer_type_id' => 'int',
				'date_transfered' => 'int',
				'first_name' => '',
				'last_name' => '',
				'country_id' => 'int',
				'city' => '',
				'var_symbol' => '',
				'transaction_id' => '',
				'pull_person_id' => 'int',
				'webmoney_account' => 'int',
				'website_id' => 'int',
				'mtcn' => '',
				'comment' => 'notags|special',
				'application_id' => $this->_request->application_id,
				'discount'	=> '',
				'd_comment'	=> '',
				'start_date'	=> '',
				'end_date'	=> '',
				'amount' => 'double'
			));
			$data = $data->getData();			
			
			
			//Removing hours from timestamp
			if ( $data['start_date'] ) {
				$data['start_date'] = strtotime(date("Y-m-d", $data['start_date'])) + 4*60*60;
			}
			
			if ( $data['end_date'] ) {
				$data['end_date'] = strtotime(date("Y-m-d", $data['end_date'])) + 4*60*60;
			}
			
			$validator = new Cubix_Validator();
			
			$package_activation_date = $package->date_activated;
			$package_expiration_date = $package->expiration_date;

			if ( $package->activation_type == 7 ) {
				$package_activation_date = date('Y-m-d', strtotime($package->activation_date));
				$package_expiration_date = date('Y-m-d', strtotime($package->activation_date . ' + ' . $package->period . ' days'));
			} elseif ( $package->activation_type == 6 && $package->previous_order_package_id ) {
				
				$prev_package = $m_package->get($package->previous_order_package_id);
				$package_activation_date = date('Y-m-d', strtotime($prev_package->expiration_date));
				$package_expiration_date = date('Y-m-d', strtotime($prev_package->expiration_date . ' + ' . $package->period . ' days'));
			}
			
			
			if ( ! $data['start_date'] ) {
				$validator->setError('start_date', 'Required');
			} elseif ( ! $data['end_date'] ) {
				$validator->setError('end_date', 'Required');
			} elseif ( $data['start_date'] >= $data['end_date'] ) {
				$validator->setError('start_date', 'Start date must be greater');
			} elseif ( $data['start_date'] > strtotime('2015-10-31') || $data['end_date'] < strtotime('2015-05-01') ) {
				$validator->setError('start_date', 'You can select only in May');
			} elseif ( $data['start_date'] < strtotime($package_activation_date) || $data['end_date'] > strtotime($package_expiration_date) ) {
				$validator->setError('start_date', 'Select date within package range.');
			} else {
				$days_count = ($data['end_date'] - $data['start_date']) / (60*60*24);
				if ( $days_count < 2 ) {
					$validator->setError('start_date', 'At least 3 days');
				}
			}
			
			foreach($booked_expo_orders as $bo) {
				if ( $data['start_date'] <= strtotime($bo['end_date']) && $data['end_date'] >= strtotime($bo['start_date']) ) {
					$validator->setError('start_date', 'Already booked ' . $bo['start_date'] . ' - ' . $bo['end_date']);
					break;
				}
			}
			
			
			$data['user_id'] = $package->user_id;
			$data['orders'] = array($package->order_id);
			
			
			if ( ! $data['transfer_type_id'] ) {
				$validator->setError('transfer_type_id', 'Required');
			}

			$data['date_transfered'] = time();
			
			if ( $data['transfer_type_id'] == 5 ) {
				if ( ! $data['webmoney_account'] ) {
					$validator->setError('webmoney_account', 'Required');
				}
				
				if ( ! $data['transaction_id'] ) {
					$validator->setError('transaction_id', 'Required');
				}
			}

			if ( $data['transfer_type_id'] == 3 ) {
				if ( ! $data['transaction_id'] ) {
					$validator->setError('transaction_id', 'Required');
				}
			}
			
			if ( $data['transfer_type_id'] != 5 ) {
				if ( $data['transfer_type_id'] && ! $data['first_name'] ) {
					$validator->setError('first_name', 'Required');
				}

				if ( $data['transfer_type_id'] && ! $data['last_name'] ) {
					$validator->setError('last_name', 'Required');
				}
			}
			
			$d_apps = array(APP_EF, APP_6A);
			if ( in_array(Cubix_Application::getId(), $d_apps) ) {
				if ( $data['discount'] > 0 && ! strlen($data['d_comment']) ) {
					$validator->setError('d_comment', 'Required');
				}
				
				$data['comment'] = $data['d_comment'];
			}
			unset($data['d_comment']);
			unset($data['discount']);

			$data['backend_user_id'] = Zend_Auth::getInstance()->getIdentity()->id;
			$data['status'] = Model_Billing_Transfers::STATUS_CONFIRMED;//Set status as confirmed. no need to confirm manually
			
			if ( $data['transaction_id'] ) {
				$m_transfer = new Model_Billing_Transfers();
				if ( false === $m_transfer->checkTransactionId($data['transaction_id']) ) {
					$validator->setError('transaction_id', 'Already in database');
				}
			}
			
			if ( $validator->isValid() ) {
				
				$expo_data = array(
					'start_date'	=> date('Y-m-d', $data['start_date']),
					'end_date'		=> date('Y-m-d', $data['end_date'])
				);
				unset($data['start_date']);
				unset($data['end_date']);
				
				$_model = new Model_Billing_Transfers();
				
				if ( $transfer_id = $_model->save($transfer = new Model_Billing_TransferItem($data), true) ) {
					//$validator->setError('global_error', $result);					
				}
				
				
				//ATTENTION !!! confirm only when transfer types is not bank transfer/sales einkassiert.
				//transfer is confirming in save function above
				if ( $data['transfer_type_id'] != 1 && $data['transfer_type_id'] != 6 ) {
					$tr = new Model_Billing_TransferItem(array('id' => $transfer_id));
					$tr->confirm('Milan Expo Transfer', $data['amount']);
				}
				
				$this->_model->add($expo_data, $escort_id, array('transfer_id' => $transfer_id, 'order_package_id' => $op_id, 'price' => $data['amount'], 'order_id' => $package->order_id));
				//Cubix_SyncNotifier::notify($package->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PACKAGE_EXPIRATION_UPDATED, array('id' => $op_id));
			}

			die(json_encode($validator->getStatus()));
		}
	}
	
	public function editAction()
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		$this->view->expo = $expo = $this->_model->get($req->id);
		
		
		if ( $this->_request->isPost() ) {
			$booked_expo_orders = $client->call('OnlineBillingV2.getEscortExpoOrders', array($expo->escort_id));
			
			$data = new Cubix_Form_Data($this->_request);
			
			$data->setFields(array(
				'id'	=> 'int',
				'start_date'	=> '',
				'end_date'	=> ''
			));
			$data = $data->getData();			
			
			$validator = new Cubix_Validator();
			
			
			$package_activation_date = $expo->date_activated;
			$package_expiration_date = $expo->expiration_date;

			if ( $expo->activation_type == 7 ) {
				$package_activation_date = date('Y-m-d', strtotime($expo->activation_date));
				$package_expiration_date = date('Y-m-d', strtotime($expo->activation_date . ' + ' . $expo->period . ' days'));
			}
			
			$paid_days_count = (strtotime($expo->end_date) - strtotime($expo->start_date)) / (60*60*24);
			
			if ( ! $data['start_date'] ) {
				$validator->setError('start_date', 'Required');
			} elseif ( ! $data['end_date'] ) {
				$validator->setError('end_date', 'Required');
			} elseif ( $data['start_date'] >= $data['end_date'] ) {
				$validator->setError('start_date', 'Start date must be greater');
			} elseif ( $data['start_date'] > strtotime('2015-05-31') || $data['end_date'] < strtotime('2015-05-01') ) {
				$validator->setError('start_date', 'You can select only in May');
			} elseif ( $data['start_date'] < strtotime($package_activation_date) || $data['end_date'] > strtotime($package_expiration_date) ) {
				$validator->setError('start_date', 'Select date within package range.');
			} else {
				$days_count = ($data['end_date'] - $data['start_date']) / (60*60*24);
				if ( $days_count != $paid_days_count ) {
					$validator->setError('start_date', 'Paid days count is ' . ($paid_days_count + 1));
				}
			}
			
			foreach($booked_expo_orders as $bo) {
				if ( $data['start_date'] <= strtotime($bo['end_date']) && $data['end_date'] >= strtotime($bo['start_date']) ) {
					$validator->setError('start_date', 'Already booked ' . $bo['start_date'] . ' - ' . $bo['end_date']);
					break;
				}
			}
			
			
			if ( $validator->isValid() ) {
				$data['start_date']	= date('Y-m-d', $data['start_date']);
				$data['end_date']	= date('Y-m-d', $data['end_date']);
					
				$this->_model->update($data);
			}

			die(json_encode($validator->getStatus()));
		}
	}
	
	
	public function removeAction()
	{
		$id = (int) $this->_request->id;
		
		if (! $id ) {
			die(')');
		}
		
		$this->_model->remove($id);
		
		die();
	}
	
	public function cronAction()
	{
		$db = Zend_Registry::get('db');
		
		set_time_limit(0);
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$cli = new Cubix_Cli();
		$pid_file = '/var/run/expo-cron-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}
		
		
		/*--->Activating*/
		$to_activate = $db->fetchAll('
			SELECT *
			FROM expo_orders
			WHERE start_date = DATE(NOW()) AND status = 2
		');
		
		foreach($to_activate as $expo) {
			$db->beginTransaction();
			try {
				$db->insert('order_package_products', array(
					'order_package_id'	=> $expo->order_package_id,
					'product_id'	=> MILAN_EXPO_PRODUCT_ID,
					'price'	=> 0
				));
				$db->update('expo_orders', array('status' => 3), $db->quoteInto('id = ?', $expo->id));
				
				Cubix_SyncNotifier::notify($expo->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('escort_id' => $expo->escort_id));
				$db->commit();
				$cli->out('Escort ' . $expo->escort_id . ' expo activated.');
			} catch(Exception $e) {
				$db->rollBack();
				$cli->out('Error expoOrderId: ' . $expo->id . '. ' . $e->getMessage());
			}
		}
		/*<---Activating*/
		
		
		/*--->Deactivating*/
		$to_deactivate = $db->fetchAll('
			SELECT *
			FROM expo_orders
			WHERE end_date < DATE(NOW()) AND status = 3
		');
		
		foreach($to_deactivate as $expo) {
			$db->beginTransaction();
			try {
				$db->query('DELETE FROM order_package_products WHERE order_package_id = ? AND product_id = ?', array($expo->order_package_id, MILAN_EXPO_PRODUCT_ID));
				$db->update('expo_orders', array('status' => 4), $db->quoteInto('id = ?', $expo->id));
				
				Cubix_SyncNotifier::notify($expo->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('escort_id' => $expo->escort_id));
				$db->commit();
				$cli->out('Escort ' . $expo->escort_id . ' expo deactivated.');
			} catch(Exception $e) {
				$db->rollBack();
				$cli->out('Error expoOrderId: ' . $expo->id . '. ' . $e->getMessage());
			}
		}
		/*<---Deactivating*/
		
		
		
		die('DONE');
	}
	
	public function removeNotPaidExposAction()
	{
		$db = Zend_Registry::get('db');
		
		$db->query('
			DELETE FROM expo_orders
			WHERE status = 1  AND TIME_TO_SEC(TIMEDIFF(NOW(), creation_date)) > ?', array(60*30));
		
		die('Done');
	}
}
