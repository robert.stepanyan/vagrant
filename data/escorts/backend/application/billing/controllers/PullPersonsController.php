<?php

class Billing_PullPersonsController extends Zend_Controller_Action
{
	/**
	 * @var Model_Billing_Orders
	 */
	protected $_model;
		
	public function init()
	{
		$this->_model = new Model_Billing_PullPersons();
	}
	
	public function indexAction() 
	{
		
	}	
	
	public function dataAction()
	{
		
		$req = $this->_request;
		
		$filter = array(
			'mpp.first_name' => $req->first_name,
			'mpp.last_name' => $req->last_name,
			'mpp.application_id' => $req->application_id,
			'mpp.type_id' => $req->type_id
		);
		
		$data = $this->_model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		if ( count($data) ) {
			foreach ( $data as $k => $value ) {
				$data[$k]->type_name = Model_Billing_PullPersons::$PULL_PERSON_TYPES[$value->type_id];
			}
		}
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

	public function toggleActiveAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->_model->toggle($id);
	}

	public function createAction()
	{
		$req = $this->_request;

		if ( $req->isPost() ) {

			$validator = new Cubix_Validator();

			if ( ! $req->first_name ) {
				$validator->setError('first_name', 'Required');
			}
			if ( ! $req->last_name ) {
				$validator->setError('last_name', 'Required');
			}

			if ( $validator->isValid() )
			{
				$type_id = (int) $req->type_id;
				
				$data = array(
					'type_id' => $type_id,
					'first_name' => $req->first_name,
					'last_name' => $req->last_name,
					'application_id' => $req->application_id,
					'status' => 1
				);

				$this->_model->insert($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}

	public function editAction()
	{
		$req = $this->_request;

		$user_id = $req->id;
		$user = $this->_model->get($user_id);
		$this->view->pull_person = $user;

		if ( $req->isPost() ) {
			$validator = new Cubix_Validator();

			if ( ! $req->first_name ) {
				$validator->setError('first_name', 'Required');
			}
			if ( ! $req->last_name ) {
				$validator->setError('last_name', 'Required');
			}

			if ( $validator->isValid() )
			{
				$type_id = (int) $req->type_id;
				
				$data = array(
					'type_id' => $type_id,
					'first_name' => $req->first_name,
					'last_name' => $req->last_name,
					'application_id' => $req->application_id,
					'status' => 1
				);

				$this->_model->update($user_id, $data);
			}

			die(json_encode($validator->getStatus()));
		}
	}
	
	public function removeAction()
	{
		/*$id = intval($this->_request->id);

		$this->model->remove($id);*/
		die;
	}
}
