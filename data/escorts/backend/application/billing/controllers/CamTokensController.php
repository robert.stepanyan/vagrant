<?php

class Billing_CamTokensController extends Zend_Controller_Action
{
	/**
	 * @var Model_Billing_Orders
	 */
	protected $_model;
		
	public function init()
	{
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		if ( $bu_user->type != 'superadmin' && $bu_user->type != 'admin' ) {
			die('no permission');
		}
		$this->_model = new Model_Billing_Tokens();

	}
	
	public function indexAction() 
	{
		
	}	
	
	public function dataAction()
	{
		
		$req = $this->_request;
		
		$filter = array(
			'transaction_id' => trim($req->transaction_id),
			'status' => $req->status,
			'member_id' => trim($req->member_id),
			'email' => trim($req->email),
			'date_from' => $req->date_from,
            'date_to' => $req->date_to,
		);
		
		$data = $this->_model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field,
			$this->_request->sort_dir, 
			$count
		);

		if ( $count ) {
			foreach ( $data as $i => $item ) {
				$data[$i]->status = Model_Billing_Tokens::$STATUS_LABELS[$item->status];
//				$data[$i]->more_info = Model_Billing_Tokens::getTransfer($item->id);
				if ( $data[$i]->user_agent){
					$user_agent = Cubix_UserAgentParser::parse_user_agent($data[$i]->user_agent);
					$data[$i]->user_agent = $user_agent['platform'] .' '. $user_agent['browser'].' v' . $user_agent['version'];
				}
			}
		}

		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

	public function dataTotalAction()
	{
		 $this->view->layout()->disableLayout();
		$req = $this->_request;
			
		$filter = array(
			'transaction_id' => trim($req->transaction_id),
			'status' => $req->status,
			'member_id' => trim($req->member_id),
			'email' => trim($req->email),
			'date_from' => $req->date_from,
            'date_to' => $req->date_to,
		);
		
		$this->view->data = $this->_model->getTotalAmount($filter);
	}


	public function totalAmountAction(){
        $this->view->layout()->disableLayout();
        $ids = $this->_getParam('ids');
        $this->view->ids = $ids;
    }

    public function calcTotalAmountAction(){
        $this->view->layout()->disableLayout();
        $ids = $this->_getParam('ids');
        $date_from = $this->_getParam('date_from');
        $date_to = $this->_getParam('date_to');

        $escort_ids = $this->_model->getEscortsIdByRequestId($ids);

        $total_amount = $this->_model->getEscortsTotalAmount($escort_ids, $date_from, $date_to);
        $this->view->total_amount = $total_amount;
    }

    
    public function sendEmailCustomerAction(){

        $request_id = $this->_getParam('id');
        if (in_array(Cubix_Application::getId(), array(APP_EF))){

            $model = new Model_Billing_Tokens();
            $data =  $model->getVideoCallRequestById($request_id);


            $duration = $data->duration;
            $skype = $data->skype;
            $showname = $data->showname;
            $customer_email = $data->email;



            //SENDING EMAIL TO CUSTOMER
            $email_content = Cubix_I18n::translateByLng('it', 'video_call_cutomer_email_text', array(
                'skype' => $skype,
                'duration' => $duration,
                'escort' => $showname
            ));

            Cubix_Email::sendTemplate('universal_message', $customer_email , array(
                'subject' => 'a tua videochiamata Skype con '. $showname,
                'message' => $email_content
            ));

            echo json_encode(array('status' => 'success'));
            die;

        }else{
            echo json_encode(array('status' => 'error'));
            die;
        }


    }


	public function toggleActiveAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->_model->toggle($id);
	}

	public function createAction()
	{
        $this->view->layout()->disableLayout();
		$req = $this->_request;

		if ( $req->isPost() ) {

			$validator = new Cubix_Validator();

			if ( ! $req->escort_id ) {
				$validator->setError('escort_id', 'Required');
			}
			if ( ! $req->name ) {
				$validator->setError('name', 'Required');
			}
			if ( ! $req->email ) {
				$validator->setError('email', 'Required');
			}
			if ( ! $req->duration ) {
				$validator->setError('duration', 'Required');
			}
			if ( ! $req->amount ) {
				$validator->setError('amount', 'Required');
			}

			if ( $validator->isValid() )
			{
				$status = (int) $req->status;

				if( (int) $req->status == 1){
                    $status = 0;
                }

				$data = array(
					'escort_id' => $req->escort_id,
					'name' => $req->name,
					'duration' => $req->duration,
                    'session_id' => session_id(),
					'amount' => $req->amount,
					'skype' => $req->skype,
					'email' => $req->email,
					'sms_text' => $req->sms_text,
					'status' => $status,
				);

				$this->_model->insert($data);
			}

			die(json_encode($validator->getStatus()));
		}


	}

	public function editAction()
	{

        if (in_array(Cubix_Application::getId(), array(APP_EF))){
            $this->view->layout()->disableLayout();
            $request_id = $this->_getParam('id' );
            $model = new Model_Billing_Tokens();

            if ( $this->_request->isPost() ){
                $req = $this->_request;

                $validator = new Cubix_Validator();

                if ( ! $req->escort_id ) {
                    $validator->setError('escort_id', 'Required');
                }
                if ( ! $req->name ) {
                    $validator->setError('name', 'Required');
                }
                if ( ! $req->email ) {
                    $validator->setError('email', 'Required');
                }
                if ( ! $req->duration ) {
                    $validator->setError('duration', 'Required');
                }
                if ( ! $req->amount ) {
                    $validator->setError('amount', 'Required');
                }

                if ( $validator->isValid() ) {
                    $status = (int)$req->status;

                    if ((int)$req->status == 1) {
                        $status = 0;
                    }

                    $update_data = array(
                        'escort_id' => $req->escort_id,
                        'name' => $req->name,
                        'duration' => $req->duration,
                        'session_id' => session_id(),
                        'amount' => $req->amount,
                        'skype' => $req->skype,
                        'email' => $req->email,
                        'sms_text' => $req->sms_text,
                        'status' => $status,
                        'request_id' => $request_id,
                    );


                    $model->updateVideoCallRequest($update_data);
                    echo json_encode(array('status' => 'success'));
                    die;
                }
            }else{
                $data =  $model->getTokenReqById($request_id);
                $this->view->data = $data;
            }
        }else{
            $this->_redirect($this->view->baseUrl() . '/billing/video-call');
        }
	}
	
	public function removeAction()
	{
		/*$id = intval($this->_request->id);

		$this->model->remove($id);*/
		die;
	}

	public function detailsAction()
    {
        $this->view->layout()->disableLayout();
       $this->view->data = Model_Billing_Tokens::getTransfer( $this->_getParam('request_id' ) );
        
    }
	
	public function rejectAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$request_id = intval($req->id);
		if ( ! $request_id ) die;
		
		$this->view->transfer = $transfer = $this->_model->getDetails($request_id);
		
		if ( ! $transfer ) die;
	
		if ( $req->isPost() ) {
			$comment = $this->_getParam('reject_reason');
			$validator = new Cubix_Validator();

			/*if ( ! strlen($comment) ) {
				$validator->setError('reject_reason', 'You need to leave Reason');
			}*/
			
			if ( $validator->isValid() ) {
				$this->_model->reject($comment, $request_id);
				
			}

			die(json_encode($validator->getStatus()));
		}
	}
}
