<?php

class Billing_TransfersController extends Zend_Controller_Action
{
	/**
	 * @var Model_Billing_Transfers
	 */
	protected $_model;

	public function init()
	{
		$this->view->model = $this->_model = new Model_Billing_Transfers();
		$this->temp_docs = new Zend_Session_Namespace('temp_docs');
	}

	public function indexAction()
	{
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('billing_transfers_data');
		$this->view->filter_params = $ses_filter->filter_params;
		/* FILTER PARAMS */

		/*$this->view->transfer_data = array(
			'total' => number_format($this->_model->getTotalTransfers(), 2),
			'paid' => number_format($this->_model->getTotalTransfersPaid(), 2)
		);*/
	}

	public function dataAction()
	{
		$req = $this->_request;

		$data = new Cubix_Form_Data($this->_request);
		$fields = array(
            'application_id' => 'int',

            'transfer_type_id' => 'int',
            'transaction_id' => '',
            'mtcn' => '',
            'cc_number' => '',
            'cc_email' => '',
            'var_symbol' => '',
            'first_name' => '',
            'last_name' => '',
            'pull_person_id' => '',
            'cc_info' => '',


            'app_id' => 'int',
            'sales_user_id' => 'int',
            'sales_user_id_real' => 'int',
            'status' => 'int',
            'filter_by' => '',
            'date_from' => 'int',
            'date_to' => 'int',
            'user_id' => '',

            'showname' => '',
            'escort_id' => 'int',
            'email' => '',
            'agency_name' => '',
            'escort_type' => '',
            'sys_order_id' => '',

            'has_comment' => '',
            'banner_price' => '',
            'is_mobile' => 'int',

            'contains_disc_orders' => '',

        );
		if (Cubix_Application::getId() == APP_EF){
		    $fields['show_dupl_coockie'] = 'int';
        }
        if (Cubix_Application::getId() == APP_ED){
            $fields['is_slf_chckt'] = 'int';
        }
		$data->setFields($fields);

		$filter = $data->getData();

		$transfers = $this->_model->getAllV2(
			$filter,
			$req->page,
			$req->per_page,
			$req->sort_field,
			$req->sort_dir
		);

		die(json_encode($transfers));
	}
	
	public function dataCountAction()
	{
		$this->view->layout()->disableLayout();
		
		$req = $this->_request;

		$data = new Cubix_Form_Data($this->_request);
		$type = $this->_request->getParam('type');
		$data->setFields(array(
			'transfer_type_id' => 'int',
			'transaction_id' => '',
			'mtcn' => '',
			'cc_number' => '',
			'cc_email' => '',
			'var_symbol' => '',
			'first_name' => '',
			'last_name' => '',
			'pull_person_id' => '',
			'cc_info' => '',


			'app_id' => 'int',
			'sales_user_id' => 'int',
			'sales_user_id_real' => 'int',
			'status' => 'int',
			'filter_by' => '',
			'date_from' => 'int',
			'date_to' => 'int',
			'user_id' => '',

			'showname' => '',
			'escort_id' => 'int',
			'email' => '',
			'agency_name' => '',
			'escort_type' => '',
			'sys_order_id' => '',
			
			'has_comment' => '',
			'banner_price' => '',
			'is_slf_chckt' => 'int',
			'contains_disc_orders' => '',
            'include_special_transfers' => '',
		));
		$data = $data->getData();

		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		$filter = array();

		$special_data = array();

		if ( $data['sys_order_id'] ) {
			$filter['o.system_order_id = ?'] = $data['sys_order_id'];
		}
		
		if ( $data['contains_disc_orders'] ) {
			$filter['contains_disc_orders'] = true;
		}

		if ( $data['include_special_transfers'] ) {
			$filter['include_special_transfers'] = true;
		}


                
		if (Cubix_Application::getId() == APP_EF){
			$escort_type_filter = $data['escort_type'];

			if ( $escort_type_filter == 'independent' ) {
				$filter['e.agency_id IS NULL'] = true;
			} elseif ( $escort_type_filter == 'agency' ) {
				$filter['e.agency_id IS NOT NULL'] = true;
			}
		}

		if ( $data['showname'] ) {
			$filter['e.showname LIKE ?'] = $data['showname'] . '%';
		}
		
		if ( $data['escort_id'] ) {
			$filter['e.id = ?'] = $data['escort_id'];
		}

		if ( $data['email'] ) {
			$filter['u.email LIKE ?'] = $data['email'] . '%';
		}

		if ( $data['agency_name'] ) {
			$filter['a.name LIKE ?'] = $data['agency_name'] . '%';
		}
		if ( $data['transfer_type_id'] ) {
			$filter['t.transfer_type_id = ?'] = $data['transfer_type_id'];
		}
		
		if ( $data['transaction_id'] ) {
			$filter['t.transaction_id = ?'] = $data['transaction_id'];
			$filter['show_special'] = $data['transaction_id'];
		}
		
		if ( $data['var_symbol'] ) {
			$filter['t.var_symbol = ?'] = $data['var_symbol'];
		}
		if ( $data['mtcn'] ) {
			$filter['t.mtcn = ?'] = $data['mtcn'];
		}
		if ( $data['cc_number'] ) {
			$filter['t.cc_number LIKE ?'] = '%' . $data['cc_number'];
			
			$special_data['special_cc_number'] = $data['cc_number'];
		}
		if ( $data['cc_email'] ) {
			$filter['t.cc_email LIKE ?'] = '%' . $data['cc_email'] . '%';
			
			$special_data['special_cc_email'] = $data['cc_email'];
		}
		if ( $data['first_name'] ) {
			$filter['t.first_name = ?'] = $data['first_name'];
			
			$special_data['special_first_name'] = $data['first_name'];
		}
		if ( $data['last_name'] ) {
			$filter['t.last_name = ?'] = $data['last_name'];
			
			$special_data['special_last_name'] = $data['last_name'];
		}
		if ( $data['pull_person_id'] ) {
			$filter['t.pull_person_id = ?'] = $data['pull_person_id'];
		}
		
		if ( $data['has_comment'] ) {
			$filter['t.comment IS NOT NULL AND t.comment <> \'\''] = true;
		}

        if ($data['banner_price']) {
            $filter['t.banner_price IS NOT NULL'] = true;
        }

        if ( $data['is_slf_chckt'] ) {
            $filter['t.is_self_checkout = 1'] = true;
        }

        if ( $data['sales_user_id_real'] ) {
            if ( $bu_user->type == 'superadmin' ) {
                $filter[] = 'u.sales_user_id = '. $data['sales_user_id_real'];
            }

            else if ($bu_user->type == 'admin') {
                $filter[] = 'u.sales_user_id = '. $data['sales_user_id_real'];
            }
        }

		if ( $bu_user->type == 'superadmin' ) {
			if ( $data['sales_user_id'] ) {
				$filter['t.backend_user_id = ?'] = $data['sales_user_id'];
			}
		}
		else {
			if ( $bu_user->type == 'sales manager' || $bu_user->type == 'sales clerk' ) {
				$filter['t.backend_user_id = ?'] = $bu_user->id;
				//$filter['t.backend_user_id = ?'] = $data['sales_user_id'];
			}
			else if ($bu_user->type == 'admin') {
				//$filter['bu.application_id = ?'] = $bu_user->application_id;
				if (Cubix_Application::getId() != APP_BL && $bu_user->username != "tom" ) {
					$filter['t.backend_user_id = ?'] = $data['sales_user_id'];
				}
			}
		}

		if ( $data['status'] ) {
			$filter['t.status = ?'] = $data['status'];
		}

		$data['filter_by'] = 'date_transfered';
		if ( $data['filter_by'] ) {
			if ( in_array($data['filter_by'], array('date_transfered')) ) {
				if ( $data['date_from'] ) {
					$filter['DATE(' . $data['filter_by'] . ') >= ?'] = new Zend_Db_Expr('DATE(FROM_UNIXTIME(' . $data['date_from'] . '))');
				}

				if ( $data['date_to'] ) {
					$filter['DATE(' . $data['filter_by'] . ') <= ?'] = new Zend_Db_Expr('DATE(FROM_UNIXTIME(' . $data['date_to'] . '))');
				}
			}
		}

		if ( strlen($data['user_id']) > 0 ) {
			$filter['u.id = ?'] = $data['user_id'];
		}
		
		$s = implode('_', $filter);
         
		$cache_key =  'transfers_' . preg_replace('#[^a-zA-Z0-9_]#', '_', $s) . $type.'_'.$data['escort_type'];
		
		$cache = Zend_Registry::get('cache');
//        var_dump($filter);die;
		if ( ! $data = $cache->load($cache_key) )
		{
			$data = $this->_model->getAllCountV2(
				$filter,
				1,
				10,
				'id',
				'asc'
			);
			
			$cache->save($data, $cache_key, array(), 3600); //1 hours
		}

		$this->view->data = $data;
//		var_dump($data);die;
		$this->view->type = $req->type;
	}

	public function createAction()
	{
		$this->view->layout()->disableLayout();

		$order_id = intval($this->_request->order_id);

		if ( $order_id ) {
			$model = new Model_Billing_Orders();
			$order = $model->getDetails($order_id);
			if ( $order ) {
				$this->view->order = $order;
			}
		}
		
		$this->view->bu_user = $bu_user = Zend_Auth::getInstance()->getIdentity();

		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			
			$params = array(
				'user_id' => 'int',
				'orders' => 'arr-int',
				'transfer_type_id' => 'int',
				'date_transfered' => 'int',
				'amount' => 'double',
				'first_name' => '',
				'last_name' => '',
				'country_id' => 'int',
				'city' => '',
				'var_symbol' => '',
				'transaction_id' => '',
				'pull_person_id' => 'int',
				'webmoney_account' => 'int',
				'website_id' => 'int',
				'mtcn' => '',
				'comment' => 'notags|special',
				'application_id' => $this->_request->application_id
			);
			if ( $bu_user->type == 'superadmin' && Cubix_Application::getId() == APP_6A ) {
				$params = array_merge($params, array(
					'cc_holder' => '',
					'cc_number' => '',
					'cc_exp_month' => '',
					'cc_exp_day'	=> '',
					'cc_ip'	=> '',
					'cc_address'	=> ''
				));
			}
			
			$data->setFields($params);
			
			$data = $data->getData();

			if ( Cubix_Application::getId() == APP_A6 && $bu_user->type != 'superadmin' ) {
				$data['date_transfered'] = time();
			}
			
			$validator = new Cubix_Validator();

			if ( ! $data['user_id'] ) {
				$validator->setError('global_error', 'Please select an agency or escort');
			}

			if ( ! $data['transfer_type_id'] ) {
				$validator->setError('transfer_type_id', 'Required');
			}

			if ( ! $data['date_transfered'] ) {
				$validator->setError('date_transfered', 'Required');
			}

			if ( ! $data['amount'] ) {
				$validator->setError('amount', 'Required');
			}

			if ( $data['transfer_type_id'] == 5 ) {
				if ( ! $data['webmoney_account'] ) {
					$validator->setError('webmoney_account', 'Required');
				}
				
				if ( ! $data['transaction_id'] ) {
					$validator->setError('transaction_id', 'Required');
				}
			}

			if ( $data['transfer_type_id'] == 3 ) {
				if ( ! $data['transaction_id'] ) {
					$validator->setError('transaction_id', 'Required');
				}
			}
			
			if ( $data['transfer_type_id'] != 5 ) {
				if ( $data['transfer_type_id'] && ! $data['first_name'] ) {
					$validator->setError('first_name', 'Required');
				}
				
				if ( $data['transfer_type_id'] && ! $data['last_name'] ) {
					$validator->setError('last_name', 'Required');
				}
			}
			
			if ( $data['transfer_type_id'] == 7 ||  $data['transfer_type_id'] == 2) {
				if ( ! $data['mtcn'] ) {
					$validator->setError('mtcn', 'Required');
				}
			}

			$data['backend_user_id'] = Zend_Auth::getInstance()->getIdentity()->id;

			if ( $data['transaction_id'] ) {
				if ( false === $this->_model->checkTransactionId($data['transaction_id']) ) {
					$validator->setError('transaction_id', 'Already in database');
				}
			
				if ( Cubix_Application::getId() != APP_A6 ) {
					$st_model = new Model_Billing_SpecialTransfers();

					if ( $st_model->isSpecialTransferExist($data['transaction_id']) ) {
						$validator->setError('transaction_id', 'Already in special transfers database');
					}
				}
			}

			if ( $validator->isValid() ) {
				if ( is_array($data['orders']) ) {
					$orders_model = new Model_Billing_Orders();
					$subtotal = 0;
					foreach ( $data['orders'] as $order_id ) {
						$price = $orders_model->getPrice($order_id);
						if ( is_null($price) ) continue;
						$subtotal += doubleval($price);
					}
					
					$user = new Model_UserItem(array('id' => $data['user_id']));
					if ( $subtotal > doubleval($data['amount']) + $user->getBalance()/* && $subtotal > 0*/ ) {
						$validator->setError('limited', 'Subtotal is more than balance');
					}

					/*if ( $subtotal == 0 ) {
						$validator->unsetError('amount');
					}*/
					if(Cubix_Application::getId() == APP_ED){
						$ord_m = new Model_Billing_Orders();
						foreach($data['orders'] as $k => $ord){
							$ord_d = $ord_m->get($ord);	

							$esc_m = new Model_Escorts();
							$esc = $esc_m->getByUserId($ord_d->user_id);
							$status = new Cubix_EscortStatus($esc->id);
				            $status->removeStatusBit(Cubix_EscortStatus::ESCORT_STATUS_AWAITING_PAYMENT);
				            $status->save();
						}
					}
				}

			}
			
			if ( $validator->isValid() ) {
				if ( true !== ($result = $this->_model->save($transfer = new Model_Billing_TransferItem($data))) ) {
					$validator->setError('global_error', $result);
				}
				else {
					// email to orders oweners
					if (is_array($data['orders']))
					{
						
						$orders_str = implode(',', $data['orders']);
						if ( strlen($orders_str) ) {
							$orders_oweners = $orders_model->getOweners($orders_str, $data['backend_user_id']);
						}
						
						if ($orders_oweners)
						{
							$orders_oweners_emails = array();
							$orders_oweners_ids = array();
							
							foreach ($orders_oweners as $owener)
							{
								$orders_oweners_emails[$owener->backend_user_id] = $owener->email;
								$orders_oweners_ids[] = $owener->backend_user_id;
							}
							
							if (count($orders_oweners_emails) > 0)
							{
								foreach ($orders_oweners_ids as $b_id)
								{
									$system_ids = $orders_model->getSystemIds($orders_str, $b_id);
									$sys_ids = array();

									foreach ($system_ids as $sys_id)
										$sys_ids[] = $sys_id->system_order_id;

									if (count($sys_ids) > 0)
									{
										$sys_ids_str = implode(',', $sys_ids);
										
										$email_data = array(
											'current_user_name' => Zend_Auth::getInstance()->getIdentity()->username,
											'sys_ids' => $sys_ids_str
										);

										Cubix_Email::sendTemplate('transfer_for_other_sales', $orders_oweners_emails[$b_id], $email_data);
									}
								}
							}
						}					
					}
					//////////////////////////
					header('Cubix-Message: ' . 'Transfer Saved!');
				}
			}

			die(json_encode($validator->getStatus()));
		}
		else{
			if(isset($this->temp_docs->images)){

			/*	foreach ( $this->temp_docs->images as $photo ) {
					$images[] = new Cubix_Images_Entry(array(
						'application_id' => Cubix_Application::getId(),
						'catalog_id' => 'transfers',
						'hash' => $photo->hash,
						'ext' => $photo->ext
					));
			}
			$images_model = new Cubix_Images();
			$images_model->remove($images);*/
			unset($this->temp_docs->images);
				
			}
		}
	}

	public function detailsAction()
	{
		$this->view->layout()->disableLayout();

		$id = intval($this->_getParam('id'));

		$transfer = $this->_model->getDetails($id);
        $geoLocation = array();
        if (Cubix_Application::getId() == APP_EF){
            if ($transfer->ip && $transfer->is_self_checkout){
                $ip2Location =Cubix_IP2Location::getInstance();
                $lookupData = $ip2Location->lookup($transfer->ip);
                $geoLocation['countryName'] = strpos($lookupData['countryName'], 'This parameter is unavailable') !== false  ? '-' : $lookupData['countryName'];
                $geoLocation['regionName'] = strpos($lookupData['regionName'], 'This parameter is unavailable') !== false  ? '-' : $lookupData['regionName'];
                $geoLocation['cityName'] = strpos($lookupData['cityName'], 'This parameter is unavailable') !== false  ? '-' : $lookupData['cityName'];
            }
            $transfer->geoLocation =  $geoLocation;


            $bin = substr($transfer->cc_number, 0, 6);
            if(strlen($bin) == 6){

	            try {
		            $ch = curl_init('https://lookup.binlist.net/' . $bin); 
					$headers = array();
					$headers[] = "Accept-Version: 3";
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
					curl_setopt($curl, CURLOPT_HTTPHEADER, $headers); 
					$output = curl_exec($ch); 
					curl_close($ch);     
					$data = json_decode($output);
					$transfer->card_country = $data->country->name;
	            } catch (Exception $e) {
	            	
	            }
            }
        }

		$this->view->transfer = $transfer;
		
		$this->view->transfer_imgs = $this->_model->getTransferImgsById($id);
		
	}

	public function editAction()
	{
		$this->view->layout()->disableLayout();

		$id = $this->view->id = intval($this->_request->id);

		$this->view->transfer = $this->_model->getDetails($id);
		
		$orders = array();
		if( count($this->view->transfer->orders) ) {
			$model = new Model_Billing_Orders();
			foreach ( $this->view->transfer->orders as $o ) {
				$order = $model->getDetails($o->id);
				
				if ( $order ) {
					$orders[] = $order;
				}
			}			
		}
		//var_dump(json_encode($orders));die;
		$this->view->orders = $orders;
		
		if ( $this->_request->isPost() ) {
			
			$data = new Cubix_Form_Data($this->_request);
			$bu_user = Zend_Auth::getInstance()->getIdentity();
			$params = array(
				'user_id' => 'int',
				'orders' => 'arr-int',
				'transfer_type_id' => 'int',
				//'date_transfered' => 'int',
				'amount' => 'double',
				'first_name' => '',
				'last_name' => '',
				'country_id' => 'int',
				'city' => '',
				'var_symbol' => '',
				'transaction_id' => '',
				'pull_person_id' => 'int',
				'webmoney_account' => 'int',
				'website_id' => 'int',
				'mtcn' => '',
				'comment' => 'notags|special',
				'application_id' => $this->_request->application_id
			);
			
			if ( $bu_user->type == 'superadmin' && Cubix_Application::getId() == APP_6A ) {
				$params = array_merge($params, array(
					'cc_holder' => '',
					'cc_number' => '',
					'cc_exp_month' => '',
					'cc_exp_day'	=> '',
					'cc_ip'	=> '',
					'cc_address'	=> ''
				));
			}
			$data->setFields($params);
			
			$data = $data->getData();
			
			//var_dump($data); die;
			$validator = new Cubix_Validator();

			if ( ! $data['user_id'] ) {
				$validator->setError('global_error', 'Please select an agency or escort');
			}

			if ( ! $data['transfer_type_id'] ) {
				$validator->setError('transfer_type_id', 'Required');
			}

			/*if ( ! $data['date_transfered'] ) {
				$validator->setError('date_transfered', 'Required');
			}*/

			if ( ! $data['amount'] ) {
				$validator->setError('amount', 'Required');
			}

			if ( $data['transfer_type_id'] == 5 ) {
				if ( ! $data['webmoney_account'] ) {
					$validator->setError('webmoney_account', 'Required');
				}
				
				if ( ! $data['transaction_id'] ) {
					$validator->setError('transaction_id', 'Required');
				}
			}

			if ( $data['transfer_type_id'] == 3 ) {
				if ( ! $data['transaction_id'] ) {
					$validator->setError('transaction_id', 'Required');
				}
			}
			
			if ( $data['transfer_type_id'] != 5 ) {
				if ( $data['transfer_type_id'] && ! $data['first_name'] ) {
					$validator->setError('first_name', 'Required');
				}

				if ( $data['transfer_type_id'] && ! $data['last_name'] ) {
					$validator->setError('last_name', 'Required');
				}
			}
			
			if ( $data['transfer_type_id'] == 7 ||  $data['transfer_type_id'] == 2) {
				if ( ! $data['mtcn'] ) {
					$validator->setError('mtcn', 'Required');
				}
			}
			//$data['backend_user_id'] = Zend_Auth::getInstance()->getIdentity()->id;

			if ( $data['transaction_id'] ) {
				if ( false === $this->_model->checkTransactionId($data['transaction_id'], $id) ) {
					$validator->setError('transaction_id', 'Already in database');
				}
			}
			
			if ( $validator->isValid() ) {
				$data['id'] = $id;
				//unset($data['date_transfered']);
				$ret = $this->_model->save(new Model_Billing_TransferItem($data));
				//var_dump($ret);die;
			}

			die(json_encode($validator->getStatus()));
		}
	}

	public function deleteAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;

		$transfer = $this->_model->get($id);
		if ( ! $transfer ) die;

		if ( Model_Billing_Transfers::STATUS_REJECTED == $transfer->status ) {
			$transfer->delete();
		}

		die;
	}

	public function rejectAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$transfer_id = intval($req->id);
		if ( ! $transfer_id ) die;
		
		$this->view->transfer = $transfer = $this->_model->getDetails($transfer_id);
		if ( ! $transfer ) die;
	
		//print_r($transfer->getActivePackages());

		$comment = $this->_getParam('reject_reason');

		if ( $req->isPost() ) {

			$validator = new Cubix_Validator();

			/*if ( strlen($req->cancel_packages) && count($req->selected) == 0 ) {
				$validator->setError('select_to_reject', 'You need to select orders to reject');
			}*/

			//$selected_orders = $req->selected;
			
			if ( ! strlen($comment) ) {
				$validator->setError('reject_reason', 'You need to leave Reason');
			}
			
			if ( $validator->isValid() ) {
				if ( Model_Billing_Transfers::STATUS_PENDING == $transfer->status || Model_Billing_Transfers::STATUS_CONFIRMED == $transfer->status ) {
					$transfer->reject($comment, $this->_request->cancel_packages);
					
					$bu_user = Zend_Auth::getInstance()->getIdentity();
					
					if (in_array($bu_user->type, array('admin', 'superadmin')) && $bu_user->id != $transfer->backend_user_id)
					{
						$o = $transfer->orders;
						
						$body = 'Order ID: ' . $o[0]->system_order_id . ' was rejected in date ' . date('d M Y');
						
						$m = new Model_BOUsers();
						$email = $m->get($transfer->backend_user_id)->email;
						
						if ($email)
							Cubix_Email::send($email, 'Transfer rejected', $body);
					}
					
					header('Cubix-Message: Transfer successfully rejected!');
				}
			}

			die(json_encode($validator->getStatus()));
		}
	}
	
	public function chargebackAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$transfer_id = intval($req->id);
		if ( ! $transfer_id ) die;
		
		$this->view->transfer = $transfer = $this->_model->getDetails($transfer_id);
		if ( ! $transfer ) die;
	
		$comment = $this->_getParam('chargeback_reason');

		if ( $req->isPost() ) {
			$validator = new Cubix_Validator();
			if ( $validator->isValid() ) {
				if ( Model_Billing_Transfers::STATUS_PENDING == $transfer->status || Model_Billing_Transfers::STATUS_CONFIRMED == $transfer->status ) {
					$transfer->chargeback($comment, $this->_request->cancel_packages);
				}
			}

			die(json_encode($validator->getStatus()));
		}
	}

	public function confirmAction()
	{
		$this->view->layout()->disableLayout();
		
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		
		$this->view->transfer = $transfer = $this->_model->getDetails($id);
		
		if ( ! $transfer ) die;
		$this->view->transfer_imgs = $this->_model->getTransferImgsById($id);
		$comment = $this->_getParam('comment');
		$amount = doubleval(doubleval(str_replace(',', '', $this->_getParam('amount'))));

		if ( $this->_request->isPost() ) {
			$validator = new Cubix_Validator();
			
			if (Cubix_Application::getId() == APP_EG_CO_UK){
				if($transfer->user_type == 'escort'){
					$escort_model = new Model_EscortsV2;
					$escort = $escort_model->get($transfer->escort_id);
					$escort_status = $escort->status;

					if($escort_status & ESCORT_STATUS_ACTIVE != 0){
						$validator->setError('not_active_escort', 'In order to confirm transfer please activate this account');
					}
				}elseif($transfer->user_type == 'agency'){
					$agency_model = new Model_Agencies;
					$agency = $agency_model->getByUserId($transfer->user_id);

					if($agency->status != 1){
						$validator->setError('not_active_escort', 'In order to confirm transfer please activate this agency account');
					}
				}
				
			}
			
			if ( ! $amount ) {
				$validator->setError('amount', 'Required');
			}

			if($validator->isValid()){
				if ( (Model_Billing_Transfers::STATUS_PENDING == $transfer->status) || (Model_Billing_Transfers::STATUS_REJECTED == $transfer->status) ) {
					$transfer->confirm($comment, $amount);
					header('Cubix-Message: Transfer successfully confirmed!');
				}
			}
			
			die(json_encode($validator->getStatus()));
		}
	}

	public function uploadImgAction()
	{
		try {
			// Save on remote storage
			$images = new Cubix_Images();
			$image = $images->save($_FILES['Filedata']['tmp_name'], 'transfers', Cubix_Application::getId(), strtolower(@end(explode('.', $_FILES['Filedata']['name']))));
			
			//$transfer_img_id = $this->_model->setImg($image);
			$transfer_img_id = count($this->temp_docs->images);
			$this->temp_docs->images[] = $image;
			$result = array(
				'status' => 'success',
				'transfer_img_id' => $transfer_img_id,
				'img_url' => $image['ext'] == 'pdf' ? '/img/cubix-uploader/pdf-icon.jpg': $this->_model->getImgUrl($image,'agency_p100')
			);
		} catch (Exception $e) {
			$result = array('status' => 'error', 'msg' => '');

			$result['msg'] = $e->getMessage();
		}

		die(json_encode($result));
	}

	public function removeImgAction()
	{
		$ids = $this->_getParam('photos');

		foreach ( $ids as $i => $id ) {
					$ids[$i] = intval($id);
					if ( ! $ids[$i] ) continue;
				}
		try {
		$images = array();

		foreach ( $ids as $id ) {
			$photo = $this->temp_docs->images[$id];
			$images[] = new Cubix_Images_Entry(array(
				'application_id' => Cubix_Application::getId(),
				'catalog_id' => 'transfers',
				'hash' => $photo['hash'],
				'ext' => $photo['ext']
			));			
			$this->temp_docs->images[$id] = null;
		}
		
		$images_model = new Cubix_Images();
		$images_model->remove($images);
		$result = array('success' => true);

		}catch (Exception $e) {
			$result = array('status' => 'error', 'msg' => '');
			$result['msg'] = $e->getMessage();
		}
		die(json_encode($result));

	}
	
	public function exportAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '2048M');
		
		$req = $this->_request;

		$data = new Cubix_Form_Data($this->_request);

		$data->setFields(array(
			'transfer_type_id' => 'int',
			'transaction_id' => '',
			'mtcn' => '',
			'var_symbol' => '',
			'first_name' => '',
			'last_name' => '',
			'pull_person_id' => '',


			'app_id' => 'int',
			'sales_user_id' => 'int',
			'sales_user_id_real' => 'int',
			'status' => 'int',
			'filter_by' => '',
			'date_from' => 'int',
			'date_to' => 'int',
			'user_id' => '',
			'status' => 'int',
                        
			'showname' => '',
			'escort_id' => 'int',
			'email' => '',
			'agency_name' => '',

			'sys_order_id' => ''
		));
		$data = $data->getData();

		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		// <editor-fold defaultstate="collapsed" desc="Filter">
		$filter = array();

		$filter['t.application_id'] = $this->_request->application_id;



		if ( $data['sys_order_id'] ) {
			$filter['o.system_order_id = ?'] = $data['sys_order_id'];
		}

		if ( $data['showname'] ) {
			$filter['e.showname LIKE ?'] = $data['showname'] . '%';
		}

		if ( $data['escort_id'] ) {
			$filter['e.id = ?'] = $data['escort_id'];
		}

		if ( $data['email'] ) {
			$filter['u.email LIKE ?'] = $data['email'] . '%';
		}

		if ( $data['agency_name'] ) {
			$filter['a.name LIKE ?'] = $data['agency_name'] . '%';
		}
		if ( $data['transfer_type_id'] ) {
			$filter['t.transfer_type_id = ?'] = $data['transfer_type_id'];
		}
		if ( $data['transaction_id'] ) {
			$filter['t.transaction_id = ?'] = $data['transaction_id'];
		}
		if ( $data['var_symbol'] ) {
			$filter['t.var_symbol = ?'] = $data['var_symbol'];
		}
		if ( $data['mtcn'] ) {
			$filter['t.mtcn = ?'] = $data['mtcn'];
		}
		if ( $data['first_name'] ) {
			$filter['t.first_name = ?'] = $data['first_name'];
		}
		if ( $data['last_name'] ) {
			$filter['t.last_name = ?'] = $data['last_name'];
		}
		if ( $data['pull_person_id'] ) {
			$filter['t.pull_person_id = ?'] = $data['pull_person_id'];
		}


		if ( $bu_user->type == 'superadmin' ) {
			if ( $data['sales_user_id'] ) {
				$filter['t.backend_user_id = ?'] = $data['sales_user_id'];
			}
		}
		else {
			if ( $bu_user->type == 'sales manager' || $bu_user->type == 'sales clerk' ) {
				$filter['t.backend_user_id = ?'] = $bu_user->id;
				//$filter['t.backend_user_id = ?'] = $data['sales_user_id'];
			}
			else if ($bu_user->type == 'admin') {
				$filter['bu.application_id = ?'] = $bu_user->application_id;
				$filter['t.backend_user_id = ?'] = $data['sales_user_id'];
			}
		}

        if ( $data['sales_user_id_real'] ) {
                $filter['u.sales_user_id = ?'] = $data['sales_user_id_real'];
        }

		if ( $data['status'] ) {
			$filter['t.status = ?'] = $data['status'];
		}

		$data['filter_by'] = 'date_transfered';
		if ( $data['filter_by'] ) {
			if ( in_array($data['filter_by'], array('date_transfered')) ) {
				if ( $data['date_from'] ) {
					$filter['DATE(' . $data['filter_by'] . ') >= ?'] = new Zend_Db_Expr('DATE(FROM_UNIXTIME(' . $data['date_from'] . '))');
				}

				if ( $data['date_to'] ) {
					$filter['DATE(' . $data['filter_by'] . ') <= ?'] = new Zend_Db_Expr('DATE(FROM_UNIXTIME(' . $data['date_to'] . '))');
				}
			}
		}

		if ( strlen($data['user_id']) > 0 ) {
			$filter['u.id = ?'] = $data['user_id'];
		}
		// </editor-fold>

		// TEMPORARY TILL LEVON WILL FIX SLAVE 
		/*if ( in_array(Cubix_Application::getId(), array(APP_A6))  ) {
			ignore_user_abort(true);
			ob_start();

			echo (json_encode(array('status' => 'success')));
			  
			ob_end_flush();
			ob_flush();
			flush();

			session_write_close();
			fastcgi_finish_request();

			$db_configs = array(
				'host' => '127.0.0.1',
				'username' => 'a6_cubix',
				'password' => 'AEserrGfg^^%5GF94',
				'port' => '13306',
				'dbname' => 'and6'
			);

			$db = new Cubix_Db_Adapter_Mysqli($db_configs);

			$db->setFetchMode(Zend_Db::FETCH_OBJ);

			Zend_Registry::set('db', $db);
			Cubix_Model::setAdapter($db);

			$db->query('SET NAMES `utf8`');
		}*/


		$page = $req->page ? (int) $req->page : null;
		$per_page = $req->per_page ? (int) $req->per_page : null;
		$sort = $req->sort_field ? $req->sort_field : 't.date_transfered';
		$dir = $req->dir ? $req->dir : 'DESC';

		$data = $this->_model->getDataForExport(
			$filter,
			$page,
			$per_page,
			$sort,
			$dir
		);

	
		if ( in_array(Cubix_Application::getId(), array(APP_A6))  ) {
			$filename = 'transfers_' . hexdec(uniqid()) . '.csv';
			$handle = fopen('transfers/' . $filename, 'w');
		} else {
			header('Content-Type: text/csv');
			header('Content-Disposition: attachment; filename=transfers.csv');
					
			$handle = fopen('php://output', 'w');
		}

		foreach ( $data['data'] as $order ) {
			$arr = array(
				$order['user_type'], $order['username'], $order['transfer_type'], $order['status'], $order['full_name'], $order['orders']
			);
			
			if (Cubix_Application::getId() == APP_A6)
			{
				$arr[] = $order['region'];
			}
			
			$arr[] = $order['date_transfered'];
			$arr[] = $order['payment_date']; 
			$arr[] = $order['paid_amount'];
			$arr[] = $order['sales_person'];
			$arr[] = $order['order_id'];
			$arr[] = $order['transaction_id'];
			$arr[] = $order['transfer_amount'];
			

			if (Cubix_Application::getId() == APP_ED)
			{
				$arr[] = $order['cities'];
				$arr[] = $order['countries'];
			}
			
			fputcsv($handle, $arr);
		}
		fclose($handle);

		if ( in_array(Cubix_Application::getId(), array(APP_A6))  ) {
			
			$cache_key = 'transfer_data_download_' . $bu_user->id;
			$cache = Zend_Registry::get('cache');

			if ( ! $cache->load($cache_key) )
			{	
				$cache->save($filename, $cache_key, array(), 3600); 
			}
		}

		exit;
	}

	public function checkCsvStatusAction(){
		$this->view->layout()->disableLayout();

		$bu_user = Zend_Auth::getInstance()->getIdentity();
		$cache_key = 'transfer_data_download_' . $bu_user->id;
		$cache = Zend_Registry::get('cache');
		if ($cache->load($cache_key))
		{	
			echo (json_encode(array('status' => true)));
			die;
		}
		echo (json_encode(array('status' => false)));
		die;
	}

	public function downloadCsvAction() {
		$this->view->layout()->disableLayout();

		$bu_user = Zend_Auth::getInstance()->getIdentity();
		$cache_key = 'transfer_data_download_' . $bu_user->id;
		$cache = Zend_Registry::get('cache');
		if($filename = $cache->load($cache_key)) {
			$cache->remove($cache_key);

			if (file_exists('transfers/' . $filename)) {
			    header('Content-Description: File Transfer');
			    header('Content-Type: application/octet-stream');
			    header('Content-Disposition: attachment; filename="'.basename('transfers/' . $filename).'"');
			    header('Expires: 0');
			    header('Cache-Control: must-revalidate');
			    header('Pragma: public');
			    header('Content-Length: ' . filesize('transfers/' . $filename));
			    readfile('transfers/' . $filename);

			    unlink('transfers/' . $filename);
			}
		}

		exit;
	}
	
	public function commentAction(){
		
		$this->view->layout()->disableLayout();
				
		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'id' => 'int',
				'comment' => 'special|notags'
			));
			$data = $data->getData();
			
			$validator = new Cubix_Validator();
			/*if(strlen(trim($data['comment'])) == 0){
				$validator->setError('comment', 'Comment Required');
			}*/
			
			
			if ( $validator->isValid() ) {
				
				$this->_model->updateCommentGrid($data);
			}

			die(json_encode($validator->getStatus()));
		}
		else
		{
			$id = intval($this->_request->id);
			$this->view->id = $id;
			$this->view->comment = $this->_model->getCommentGrid($id);
		}
		
	}
	
	public function editCreditCardInfoAction()
	{
		$this->view->layout()->disableLayout();
		
		$id = $this->view->id = intval($this->_request->id);
		$this->view->transfer = $this->_model->getDetails($id);
		
		if ( $this->_request->isPost() ) {
			$validator = new Cubix_Validator();
			$data = new Cubix_Form_Data($this->_request);
			$params = array(
				'cc_holder' => '',
				'cc_email' => '',
				'cc_number' => '',
				'cc_exp_month' => '',
				'cc_exp_day'	=> '',
				'cc_ip'	=> '',
				'cc_address'	=> ''
			);
			
			$data->setFields($params);
			$data = $data->getData();
			
			if ( $validator->isValid() ) {
				$data['id'] = $id;
				$ret = $this->_model->save(new Model_Billing_TransferItem($data));

				die(json_encode($validator->getStatus()));
			}
		}
	}
	
	public function findTransactionIdAction()
	{
		$this->view->layout()->disableLayout();
		
		$this->view->result = 'Nothing found.';
		
		$bu = Zend_Auth::getInstance()->getIdentity();
		if ( $bu->type != 'superadmin' ) {
			$this->view->result = 'You don\'t have such privileges.';
			return;
		}
		$transaction_id = $this->_request->transaction_id;
		
		if ( ! $transaction_id ) {
			$this->view->result = 'No transaction id specified';
			return;
		}
		
		
		$api_keys = array(
			'escortforumit.xxx' => array('api_key' => 'n2n6fp5t571b7c8qkdn17tliff5707ie', 'server' => 'http://server.escortforumit.xxx'),
			//'6annnonce.com' => array('api_key' => '188e18301e3c4d071cdc2c1aed9515eb', 'server' => 'http://server.6annonce.com'),
			'and6.ch' => array('api_key' => 'vva724hglxfryk4avfp2jbee67rbsp7u', 'server' => 'http://server.and6.ch'),
			'and6.com' => array('api_key' => 'vva724hglxfryk4avfp2jbee67rbsp7u', 'server' => 'http://server.and6.com'),
			'beneluxxx.com' => array('api_key' => '1fgzkv28mly8e034c496iq0ptttetp90', 'server' => 'http://server.beneluxxx.com'),
			'escortdirectory.com' => array('api_key' => '9p30yrtt4j1xbyzwscg5rkecz78vsitr', 'server' => 'http://server.escortdirectory.com'),
			'escortdguide.co.uk' => array('api_key' => 's4ovt76cgw09vzin88y2ohyxavgzofvm', 'server' => 'http://server.escortguide.co.uk'),
			'escortmeetings.com' => array('api_key' => 'cb4pbyx6wnae77dgi4xb32kk10cmvlkd', 'server' => 'http://server.escortmeetings.com')
			
		);
		
		foreach( $api_keys as $app => $sett ) {
			Cubix_Api_XmlRpc_Client::setApiKey($sett['api_key']);
			Cubix_Api_XmlRpc_Client::setServer($sett['server']);
			
			try {
				$client = new Cubix_Api_XmlRpc_Client();
				$result = $client->call('Billing.findTransactionId', array($transaction_id));

				if ( $result ) {
					$this->view->result = 'This transfer is used in <b>' . $app . '</b>';
					break;
				}
			} catch(Exception $e) {
				die('Error occured.');
			}
		}
	}
	
}
