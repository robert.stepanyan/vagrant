<?php

class Billing_GotdPrevController extends Zend_Controller_Action
{
	/**
	 * @var Model_Billing_Packages
	 */
	protected $_model;

	public function init()
	{
		$this->_model = new Model_Billing_Packages();
	}

	public function indexAction()
	{
		
	}

	public function dataAction()
	{
		$req = $this->_request;

		$filter = array(
			'ag.name' => $req->agency_name,
			'e.showname' => $req->showname,
			'e.id' => $req->escort_id,
			// Date
			'filter_by' => $req->filter_by,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to,
			'sales_user_id' => $req->sales_user_id,
			'sales_user_id_real' => $req->sales_user_id_real,
			
			'gotd_city' => $req->gotd_city,
			'gotd_status' => $req->gotd_status,

			//'op.status' => Model_Billing_Packages::STATUS_ACTIVE,
			/*'op.application_id' => $req->app_id,*/
			'o.sys_order_id' => $req->sys_order_id
		);

		$data = $this->_model->getAllV2(
			$this->_request->page,
			$this->_request->per_page,
			$filter,
			$this->_request->sort_field,
			$this->_request->sort_dir,
			$count,
			true
		);
		
		$gotd_statuses = Model_Products::$GOTD_STATUS_TITLES;
		foreach ( $data as $k => $package ) {
			$data[$k]->package_status = Model_Packages::$STATUS_LABELS[$package->package_status];
			$data[$k]->gotd_status_text = $gotd_statuses[$package->gotd_status];
			
			if ( Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT )
				$data[$k]->is_suspendable = $package->isSuspendable();
			
			//FIXING PACKAGES LOGIC
			//IF PACKAGE EXPIRES 19 APRIL IT'S ACTUALLY EXPIRES 18 APRIL 23:59
			if ( $data[$k]->expiration_date ) {
				$data[$k]->expiration_date -= 60*60*24; // -1 day
			}
			if ( $data[$k]->gotd_activation_date ) {
				if(Cubix_Application::getId() == APP_EG_CO_UK ){
					$data[$k]->gotd_activation_date += 60*60*2; // server time difference 
				}
				$data[$k]->gotd_activation_date = date("j M Y",$data[$k]->gotd_activation_date);
			}
			
		}
		
		die(json_encode(array('data' => $data, 'count' => $count, 'sum' => 'TOTAL PAID: ' . number_format($this->_model->gotd_sum, 2))));
	}
	
	public function cancelPendingGotdAction()
	{
		$gotd_id = (int) $this->_request->gotd_id;
		
		if (! $gotd_id ) {
			die(')');
		}
		Model_Activity::getInstance()->log('delete_gotd', array('gotd_id' => $gotd_id, 'status' => 'pending' ));
		$this->_model->cancelPendingGotd($gotd_id);
		
		die();
	}

	public function cancelActiveGotdAction()
	{
		$gotd_id = (int) $this->_request->gotd_id;

		if (! $gotd_id ) {
			die(')');
		}

		$buUser = Zend_Auth::getInstance()->getIdentity();

		if ($buUser->type == 'superadmin' && in_array(Cubix_Application::getId(),array(APP_EF,APP_BL,APP_EG_CO_UK))){
			Model_Activity::getInstance()->log('delete_gotd', array('gotd_id' => $gotd_id, 'status' => 'active' ));
            $this->_model->cancelActiveGotd($gotd_id);
        }

		die();
	}
}
