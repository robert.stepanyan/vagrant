<?php

class Billing_SpecialTransfersController extends Zend_Controller_Action
{
	/**
	 * @var Model_Billing_SpecialTransfers
	 */
	protected $_model;
	

	public function init()
	{
		$this->view->model = $this->_model = new Model_Billing_SpecialTransfers();
	}

	public function indexAction() 
	{
		
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'application_id' => $req->application_id,
			
			'first_name' => $req->first_name, 
			'last_name' => $req->last_name,			
			'transaction_id' => $req->transaction_id,			
			'amount' => $req->amount,			
			'cc_number' => $req->cc_number,			
			'email' => $req->email,			
			'ip' => $req->ip,			
			'comment' => $req->comment,			
			'sales_user_id' => $req->sales_user_id,			
			'cc_exp_date' => $req->cc_exp_date
		);
		
		$data = $this->_model->getAll(
			$req->page,
			$req->per_page, 
			$filter, 
			$req->sort_field, 
			$req->sort_dir, 
			$count
		);
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
	
	public function createAction()
	{
		$this->view->layout()->disableLayout();
		
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		
		if ( $bu_user->type != 'superadmin' && Cubix_Application::getId() != APP_ED && Cubix_Application::getId() != APP_EG_CO_UK ) {
			if ( Cubix_Application::getId() == APP_BL && $bu_user->username == 'tom' ) {

			} else {
				$this->_request->setModuleName('billing');
				$this->_request->setControllerName('special-transfers');
				$this->_request->setActionName('denied');
				exit;
			}
		}


		
		if ( $this->_request->isPost() )
		{
			$data = new Cubix_Form_Data($this->_request);
			
			$fields = array(
				'first_name' => '',
				'last_name' => '',
				'amount' => '',
				'transaction_id' => '',
				'cc_number' => '',
				'cc_exp_date_month' => 'int',
				'cc_exp_date_year' => 'int',
				'ip' => '',
				'email' => '',
				'comment' => '',
				'is_charged_back' => '',
			);
			$data->setFields($fields);
			$data = $data->getData();
			
			$validator = new Cubix_Validator();
			
			if ( ! strlen($data['first_name']) ) {
				$validator->setError('first_name', 'This field is required');
			}
			
			if ( ! strlen($data['last_name']) ) {
				$validator->setError('last_name', 'This field is required');
			}
			
			if ( ! strlen($data['amount']) ) {
				$validator->setError('amount', 'This field is required');
			}
			if ( ! strlen($data['transaction_id']) ) {
				$validator->setError('transaction_id', 'This field is required');
			} else if ( $this->_model->isSpecialTransferExistI($data['transaction_id']) ) {
				$validator->setError('transaction_id', 'Transaction ID is already exist');
			}
			
			
			if ( ! strlen($data['cc_number']) ) {
				$validator->setError('cc_number', 'This field is required');
			}
			if ( ! strlen($data['comment']) ) {
				$validator->setError('comment', 'This field is required');
			}
			
			if ( $validator->isValid() ) {
				
				$data['backend_user_id'] = $bu_user->id;
				$data['application_id'] = $this->_request->application_id;
				//$data['cc_exp_date'] = date('Y-m-d', $data['cc_exp_date']);
				$data['cc_exp_date'] = $data['cc_exp_date_year']."-".$data['cc_exp_date_month']."-01";
				unset($data['cc_exp_date_year']);
				unset($data['cc_exp_date_month']);
				$this->_model->save($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}

	public function editAction()
	{
		$this->view->layout()->disableLayout();
		
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		if ( $bu_user->type != 'superadmin' ) {
			if ( Cubix_Application::getId() == APP_BL && $bu_user->username == 'tom' ) {

			} else {
				$this->_request->setModuleName('billing');
				$this->_request->setControllerName('special-transfers');
				$this->_request->setActionName('denied');
				exit;
			}
		}

		$id = intval($this->_request->id);
		
		$this->view->item = $this->_model->getById($id);
		
		if ( $this->_request->isPost() )
		{
			$data = new Cubix_Form_Data($this->_request);
			
			$fields = array(
				'id' => '',
				'first_name' => '',
				'last_name' => '',
				'amount' => '',
				'transaction_id' => '',
				'cc_number' => '',
				'cc_exp_date_month' => 'int',
				'cc_exp_date_year' => 'int',
				'ip' => '',
				'email' => '',
				'comment' => '',
				'is_charged_back' => '',
			);
			$data->setFields($fields);
			$data = $data->getData();
			
			$validator = new Cubix_Validator();
			
			if ( ! strlen($data['first_name']) ) {
				$validator->setError('first_name', 'This field is required');
			}
			
			if ( ! strlen($data['last_name']) ) {
				$validator->setError('last_name', 'This field is required');
			}
			
			if ( ! strlen($data['amount']) ) {
				$validator->setError('amount', 'This field is required');
			}
			if ( ! strlen($data['transaction_id']) ) {
				$validator->setError('transaction_id', 'This field is required');
			} else if ( $this->_model->isSpecialTransferExistI($data['transaction_id'], $data['id']) ) {
				$validator->setError('transaction_id', 'Transaction ID is already exist');
			}
			
			if ( ! strlen($data['cc_number']) ) {
				$validator->setError('cc_number', 'This field is required');
			}
			if ( ! strlen($data['comment']) ) {
				$validator->setError('comment', 'This field is required');
			}
			
			if ( $validator->isValid() ) {
				$bu_user = Zend_Auth::getInstance()->getIdentity();
				$data['backend_user_id'] = $bu_user->id;
				$data['application_id'] = $this->_request->application_id;
				//$data['cc_exp_date'] = date('Y-m-d', $data['cc_exp_date']);
				$data['cc_exp_date'] = $data['cc_exp_date_year']."-".$data['cc_exp_date_month']."-01";
				unset($data['cc_exp_date_year']);
				unset($data['cc_exp_date_month']);
				$this->_model->save($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}

	public function deleteAction()
	{
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		if ( $bu_user->type != 'superadmin' ) {
			if ( Cubix_Application::getId() == APP_BL && $bu_user->username == 'tom' ) {

			} else {
				$this->_request->setModuleName('billing');
				$this->_request->setControllerName('special-transfers');
				$this->_request->setActionName('denied');
				exit;
			}
		}
		
		$id = intval($this->_request->id);
		if ( ! $id ) die;

		$this->_model->delete($id);
		
		die;
	}
	
	public function deniedAction()
	{
		$this->view->layout()->setLayout('error');
		if ( isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) ) {
			$this->view->layout()->disableLayout();
		}
	}
}