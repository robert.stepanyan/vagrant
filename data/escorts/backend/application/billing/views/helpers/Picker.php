<?php

class Zend_View_Helper_Picker extends Zend_View_Helper_Abstract
{
	public function picker(array $params = array(), $type = 'escort')
	{
		$user_type = null;
		$name = '';
		$image = '/img/no-photo.png';
		$balance = '';
		$username = '';
		$user_id = '';
		
		if ( isset($params['user_id']) ) {
			$model = new Model_Users();
			$user = $model->get($params['user_id']);

			$user_type = $user->getUserType();

			if ( $user_type == 'escort' ) {
				$escort_id = $user->getEscortId();
			}
			elseif ( $user_type == 'agency' ) {
				$agency_id = $user->getAgencyId();
			}
		}
		elseif ( isset($params['escort_id']) ) {
			$user_type = 'escort';
			$escort_id = $params['escort_id'];
		}
		elseif ( isset($params['agency_id']) ) {
			$user_type = 'agency';
			$agency_id = $params['agency_id'];
		}

		if ( $user_type == 'escort' ) {
			$model = new Model_Escorts();
			$escort = $model->get($escort_id);

			$name = $escort->showname;
			$image = $escort->getMainPhoto()->getUrl('backend_smallest');
			if ( ! $image ) $image = '/img/no-photo.png';
			$balance = $escort->getUser()->getBalance();
			$username = $escort->username;
			$user_id = $escort->user_id;
		}
		elseif ( $user_type == 'agency' ) {
			$model = new Model_Agencies();
			$agency = $model->getById($agency_id);

			$name = $agency['agency_data']->name;
			$image = $agency['agency_data']->getLogoUrl('backend_smallest');
			if ( ! $image ) $image = '/img/no-photo.png';
			$balance = $agency['agency_data']->getUser()->getBalance();
			$username = $agency['agency_data']->username;
			$user_id = $agency['agency_data']->user_id;
		}

		$el_id = md5(microtime());
		if ( isset($params['id']) ) $el_id = $params['id'];

		ob_start();
		?>
<div id="<?= $el_id ?>" class="cx-picker">
	<div class="thumb fleft">
		<img src="<?= $image ?>" width="64" height="64" alt="<?= $name ?>" title="<?= $name ?>" />
	</div>
	<div class="info fleft">
		<div class="name">
			<div class="text <?= $user_type ?>"><?= $name ?></div>
			<? if ( ! isset($params['read_only']) || false === $params['read_only'] ): ?>
			<a href="#"></a>
			<? endif ?>
			<div class="clear"></div>
		</div>
		<div class="username">
			Username: <?= $username ?>
		</div>
		<div class="balance">
			Balance: <span class="amount"><?= str_replace(',', '', number_format($balance, 2)) ?></span> <?= $this->view->currency() ?><br/>
		</div>
		<div class="clear"></div>
	</div>
    <div class="searchbox fleft hide">
		<div class="autocomplete fleft">
			<label>Showname</label>
			<input type="text" class="autocomplete" value="<?= $name ?>" />
			<img class="loader" src="/img/tiny-loader.gif" alt="" />
		 </div>
		 <div class="clear"></div>
		 <div class="search-fields fleft">
			 <div class="inner">
				 <label for="id">Escort Id</label>
				 <input type="text" class="txt w6 blur" name="escort_id">
			 </div>
			 <div class="inner">
				 <label for="id">Order Id</label>
				 <input type="text" class="txt w6 blur" name="order_id">
			 </div>
			 <div class="inner">
				 <a  href="#"></a>
			 </div>
		 </div>
	</div>
	<div class="clear"></div>
	<input type="hidden" name="<?= $params['field_name'] ?>" value="<?= $user_id ?>" />
	<input type="hidden" name="<?= $params['field_type'] ?>" value="<?= $user_type ?>" />
</div>
<div class="clear"></div>
<script type="text/javascript">
	function init_<?= $el_id ?> () {
		var el = $('<?= $el_id ?>');
		if ( ! el ) return;
		
		el.readOnly = <?= isset($params['read_only']) && true === $params['read_only'] ? 'true' : 'false' ?>;
		var isPrimaryId = <?= isset($params['primary_id']) ? 1 : 0 ?>;
		var overlay = new Cubix.Overlay( el , { showLoader: true });
		var els = {
			main: el,
			autocomplete: el.getElement('.autocomplete'),
			searchbox:el.getElement('.searchbox'),
			info: el.getElement('.info'),
			btn: el.getElement('.info .name a'),
			searchBtn: el.getElement('.search-fields a'),
			input: el.getElement('.autocomplete input'),
			loader: el.getElement('.autocomplete .loader'),
		};
		
		var info = {
			balance: els.info.getElement('.balance span'),
			name: els.info.getElement('.name .text'),
			img: el.getElement('.thumb img'),
			username: el.getElement('.username'),
			user_id: el.getElement('input[name=<?= $params['field_name'] ?>]'),
			user_type:el.getElement('input[name=<?= $params['field_type'] ?>]')
		}
		
		/*els.input.addEvent('blur', function () {
			if ( info.user_id.get('value').length ) {
				setMode('view');
			}
		});*/

		function setMode(mode) {
			function modeEdit() {
				els.info.addClass('hide');
				els.searchbox.removeClass('hide');
				els.input.focus();
			};

			function modeView() {
				els.info.removeClass('hide');
				els.searchbox.addClass('hide');
			};

			if ( mode == 'edit' ) {
				modeEdit();
			}
			else if ( mode == 'view' ) {
				modeView();
			}
		};

		function modeAction(data){
				var title = data.title;
				if ( title.length > 24 ) {
					title = title.slice(0, 21) + '...';
					info.name.set('title', data.title);
				}

				info.balance.set('html', data.balance);
				info.name.set('html', title);

				if ( data.photo ) {
					info.img.set('src', data.photo);
				}
				else {
					info.img.set('src', '/img/no-photo.png');
				}

				info.img.set('alt', title);
				info.img.set('title', title);
				info.username.set('html', 'Username: ' + data.username);
				info.name.removeClass('escort');
				info.name.removeClass('agency');
				info.user_id.set('value', data.user_id);
				info.user_type.set('value', data.user_type);

				if ( data.user_type == <?= USER_TYPE_SINGLE_GIRL ?> ) {
					info.name.addClass('escort');

				}
				else if ( data.user_type == <?= USER_TYPE_AGENCY ?> ) {
					info.name.addClass('agency');
				}
				if(isPrimaryId){
					$(el).getParent().getElement('input[name=primary_id]').set('value',data.id);
				}
		}

		var input = els.input;
		var loader = els.loader;
		loader.setStyle('display', 'none');

		if ( ! els.input.get('value').length ) {
			setMode('edit');
		}

		input.autocompleter = new Autocompleter.Request.JSON(input, '', {
			indicator: loader
		}).addEvent('onSelection', function () {
			//modeAction(this.selected.token);
			//els.input.fireEvent('blur');*/
		});

		<? if ( $user_id ): ?>
		input.autocompleter.selected = { token: { user_id: <?= $user_id ?>, balance: <?= $balance ?>, username: '<?= $username ?>', user_type: '<?= $user_type ?>' } };
		<? endif ?>

		input.autocompleter.request.options.url = '/billing/orders/items';

		el.disable = function () {
			this.set('opacity', 0.5);
			this.disabled = true;
		}.bind(el);

		el.enable = function () {
			this.set('opacity', null);
			this.disabled = false;
		}.bind(el);

		if ( els.btn ) {
			els.btn.addEvents({
				'mousedown': function () {
					if ( el.disabled ) return;

					this.addClass('down');
					document.addEvent('mouseup', this.mouseup = function () { this.removeClass('down'); document.removeEvent('mouseup', this.mouseup) }.bind(this));
				},
				'mouseenter': function () {
					if ( el.disabled ) return;

					this.addClass('hover');
				},
				'mouseleave': function () {
					if ( el.disabled ) return;

					this.removeClass('hover');
				},
				click: function (e) {
					if ( el.disabled ) return;

					e.stop();

					setMode('edit');
				}
			});
		}
		if ( els.searchBtn ) {
			els.searchBtn.addEvents({
				'mousedown': function () {
					if ( el.disabled ) return;

					this.addClass('down');
					document.addEvent('mouseup', this.mouseup = function () { this.removeClass('down'); document.removeEvent('mouseup', this.mouseup) }.bind(this));
				},
				'mouseenter': function () {
					if ( el.disabled ) return;

					this.addClass('hover');
				},
				'mouseleave': function () {
					if ( el.disabled ) return;

					this.removeClass('hover');
				},
				click: function (e) {
					if ( el.disabled ) return;
					e.stop();
					overlay.disable();
					var reg = /-[0-9]+$/;
					var autocompValue = els.input.get('value').replace(reg,'');

					var post_data = {
						escort_id: els.searchbox.getElement('input[name=escort_id]').get('value'),
						order_id: els.searchbox.getElement('input[name=order_id]').get('value'),
						value:autocompValue
					}
					new Request.JSON({
						url: '/billing/orders/items',
						method: 'post',
						data: post_data,
						onSuccess: function (resp) {
							if (resp.length){
								modeAction(resp[1]);

							}
							else{
								modeAction({
									title:'Nothing found',
									balance:'----',
									user_type:0,
									user_id:0
								})
							}
							overlay.enable();

						}
					}).send();
					setMode('view');
					
				}
			});
		}
				
	}
	
	init_<?= $el_id ?>();
</script>
		<?

		return ob_get_clean();
	}
}
