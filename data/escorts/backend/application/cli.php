<?php
define('IS_CLI', 1);
define('IN_BACKEND', 1);

if ( preg_match('#\.(([^.]+?)\.(co\.uk|com|xxx|ch|net|))((\.test))?$#', $_SERVER['PSEUDO_SERVER_NAME'], $a) ) {
        array_shift($a);
        $hostname = reset($a);
        $config_file = $hostname . '.ini';
        define('HOSTNAME', $hostname);
}

chdir(dirname(__FILE__) . '/../public');


defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../../library'),
    get_include_path(),
)));

try {
	require_once 'Zend/Console/Getopt.php';
    $opts = new Zend_Console_Getopt(array(
		'help|h'		=> 'Displays usage information.',
		'action|a=s'	=> 'Action to perform in format of module.controller.action',
		'host'			=> 'Hostname to immitate',
		'application|p-i' => 'Application id.',
		'debug|d'		=> 'Debug mode',
		'staging|s'		=> 'Stating mode',
		'video_id|v-i'=> 'video_id for converting',
	));

    $opts->parse();
}
catch ( Zend_Console_Getopt_Exception $e ) {
    echo($e->getMessage() ."\n\n". $e->getUsageMessage());
	exit;
}

if ( isset($opts->h) ) {
    echo $opts->getUsageMessage();
    exit;
}


if ( isset($opts->a) ) {
	if ( isset($opts->d) ) {
		define('APPLICATION_ENV', 'development');
	}
	elseif ( isset($opts->s) ) {
		define('APPLICATION_ENV', 'staging');
	}
	
	defined('APPLICATION_ENV')
		|| define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

	require_once 'Zend/Application.php';

	$config_file = 'application.ini';
	
	if ( 1 == $opts->p ) {
		$config_file = 'escortforumit.xxx.ini';
	}
	else if ( 2 == $opts->p ) {
		$config_file = '6annonce.com.ini';
	}
	else if ( 5 == $opts->p ) {
		$config_file = 'escortguide.co.uk.ini';
	}
	else if ( 6 == $opts->p ) {
		$config_file = 'sexindex.ch.ini';
	}
	else if ( 7 == $opts->p ) {
		$config_file = 'escortguide.com.ini';
	}
	else if ( 9 == $opts->p ) {
		$config_file = 'escortitalia.com.ini';
	}
	else if ( 11 == $opts->p ) {
		$config_file = 'asianescorts.com.ini';
	}
	else if ( 16 == $opts->p ) {
		$config_file = 'and6.com.ini';
	}
	else if ( 25 == $opts->p ) {
		$config_file = 'and6.at.ini';
	}
	else if ( 17 == $opts->p ) {
		$config_file = '6anuncio.com.ini';
	}
	else if ( 18 == $opts->p ) {
		$config_file = '6classifieds.com.ini';
	}
	else if ( 33 == $opts->p ) {
		$config_file = 'escortmeetings.com.ini';
	}
	else if ( 22 == $opts->p ) {
		$config_file = 'pinkconnection.com.ini';
	}
	else if ( 30 == $opts->p ) {
		$config_file = 'beneluxxx.com.ini';
	}
	else if ( 69 == $opts->p ) {
		$config_file = 'escortdirectory.com.ini';
	}
	
	$application = new Zend_Application(
		APPLICATION_ENV,
		APPLICATION_PATH . '/configs/' . $config_file
	);

	
	$_SERVER['HTTP_HOST'] = $opts->host;
	$_SERVER['REQUEST_URI'] = '';

	$application->bootstrap();

	$reqRoute = array_reverse(explode('.', $opts->a));
    @list($action, $controller, $module) = $reqRoute;

    $request = new Cubix_Controller_Request_Cli($action, $controller, $module, array(
		'lang_id' => 'en',
		'application_id' => $opts->p,
		'video_id'=>$opts->v,
	));

	$front = $application->getBootstrap()->getResource('frontController');

	$front->setRequest($request);
	$front->setRouter(new Cubix_Controller_Router_Cli());
    $front->setResponse(new Zend_Controller_Response_Cli());
    $front->throwExceptions(true);
	
	$application->run();
}

