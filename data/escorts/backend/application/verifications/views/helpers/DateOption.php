<?php

class Zend_View_Helper_DateOption
{
	public function dateOption($date)
	{
		$today = time();
		//$tomorrow = strtotime('+1 day', $today);
		$tomorrow = mktime (0, 0, 0, date("m"), (date("d") + 1), date("Y"));
		
		$is_expired = false;
		//Expired
		if ( $date - $today < 0 ) {
			$is_expired = true;
			$bg_color = 'background-color: #FFBFC8';
		}
		//Today
		else if ( $date - $today < 3600 * 24 ) {
			
			$bg_color = 'background-color: #BFFFC2';
		}
		//Tomorrow
		if ( ($date - $tomorrow) > 0 && ($date - $tomorrow) < 3600 * 24 ) {
			$bg_color = 'background-color: #BFC4FF';
			
		}
		
		return  array('is_expired' => $is_expired, 'bg_color' => $bg_color);
	}
}
