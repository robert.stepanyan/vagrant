<?php

class Verifications_VerificationTypesController extends Zend_Controller_Action
{

    const PACKAGE_STATUS_ACTIVE = 2;

    public function init()
    {
        $this->model = new Model_VerificationTypes();
    }

    public function indexAction()
    {

    }

    public function dataAction()
    {
        $data = $this->model->getVerificationTypes();

        echo json_encode(array(
            'data' => $data,
        ));

        exit;
    }

    public function toggleAction()
    {
        $id = intval($this->_request->id);
        if ( ! $id ) die;

        $res = $this->model->toggleVerificationTypes($id);

        echo json_encode(array(
            'data' => $res,
        ));

        exit;
    }

}