<?php

class Verifications_NotificationsController extends Zend_Controller_Action {
	
	public function init()
	{
		$this->model = new Model_Verifications_Requests();
	}
	
	public function indexAction() 
	{
		
	}
	
	public function dataAction()
	{
		$page = $this->_getParam('page');
		$per_page = $this->_getParam('per_page');
		$sort_field = $this->_getParam('sort_field');
		$sort_dir = $this->_getParam('sort_dir');
		
		$req = $this->_request;			
		
		$filter = array(			
			'e.showname' => $req->showname,
			'e.verified_status' => Model_Escorts::VERIFY_RESET,
			'u.application_id' => $req->application_id,
			/*'vr.status' => $status,
			'vr.inform_method' => $req->inform_method,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to*/
		);
		
		$m_escorts = new Model_Escorts();
		$count = 0;
		$data = $m_escorts->getAll($page, $per_page, $filter, $sort_field, $sort_dir, $count);
		
		foreach ( $data as $k => $escort )
		{			
			if ( $private_count = $escort->getNewPhotoCount(ESCORT_PHOTO_TYPE_PRIVATE) )
			{
				$text = "and {$private_count} unverified private photos";
			}
			$data[$k]->info = "has {$escort->getNewPhotoCount(ESCORT_PHOTO_TYPE_HARD)} unverified photos" . $text;
		}
		
		echo json_encode(array('data' => $data, 'count' => $count));
		die;
	}
	
	public function setStatusAction()
	{
		$status = $this->_getParam('status');
		$escort_id = $this->_getParam('escort_id');
		$reason_ids = $this->_getParam('reason_ids');
		$model = new Model_Escorts();
				
		if ($status == 'REJECTED')
		{
			$validator = new Cubix_Validator();

			if ( ! $reason_ids ) {
				$validator->setError('reason_1', 'Required');
			}

			if ( $validator->isValid() ) {
				
				$request_m = new Model_Verifications_Requests();
				$free_text = $this->_getParam('free_text');
				$request_id = $request_m->getLastVerifiedId($escort_id );
				$model->setStatus($escort_id, $status);
				$request_m->setStatus($escort_id, $request_id, Model_Verifications_Requests::REJECTED, null, $reason_ids, $free_text);
				/* send email */
				$esc = $model->get($escort_id);
				$showname = $esc->showname;
				$subject = '100% verification process';
				$ids = explode(',', $reason_ids);

				$this->view->showname = $showname;
				$this->view->ids = $ids;
				$this->view->free_text = $this->_getParam('free_text');

				if ( Cubix_Application::getId() == 1 ) {
					$body = $this->view->render('email-templates/verification-ef.phtml');
				}

				elseif ( Cubix_Application::getId() == 2 )
				{
					$body = $this->view->render('email-templates/verification-6a.phtml');
				}

				elseif ( Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT )
				{
					$body = $this->view->render('email-templates/verification-and6.phtml');
				}

                if ($body)
                    Cubix_Email::send($esc->email, $subject, $body, true);

                if(in_array(Cubix_Application::getId() , array(APP_EG_CO_UK, APP_6B))){


                    $email_template = 'escort_verification_100_percent_reject';
                    $shortcodes['showname'] = $esc->showname? $esc->showname : $esc->username;
                    Cubix_Email::sendTemplate( $email_template, $esc->email, $shortcodes );
                }

				

				
			}

			Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_STATUS_UPDATED, array('100_verification' => $status));
			
			die(json_encode($validator->getStatus()));
		}
		else{
			$model->setStatus($escort_id, $status);
			
			Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_STATUS_UPDATED, array('100_verification' => $status));
			
			die;
		}
	}
	
	public function photosAction()
	{
		$escort_id = $this->_getParam('escort_id');
		
		$m_escorts = new Model_Escorts();
		$this->view->escort = $escort = $m_escorts->get($escort_id);
		$this->view->photos = $escort->getPhotos('ep.status', 'DESC');
	}


	public function revisionPhotosAction()
	{
	    $this->view->b_user = Zend_Auth::getInstance()->getIdentity()->id;
		$escort_id = $this->_getParam('escort_id');

//        $escorts_model = new Model_EscortsV2();
//        $escortv2 = $escorts_model->getProfile($escort_id);
//        $escortv2 = new Model_EscortV2Item($escortv2);
//        $escortsv2 = $escorts->get($escort_id);


		$m_escorts = new Model_Escorts();
		$this->view->escort = $escort = $escortsv2 = $m_escorts->get($escort_id);
		$this->view->photos = $escort->getPhotos('ep.status', 'DESC');
        $escortsv2 = new Model_EscortV2Item($escortsv2);
        $this->view->history_photos = $escortsv2->getPhotoHistory();
//        var_dump($history_photos);die;

		$request_m = new Model_Verifications_Requests();
		$request = $request_m->getLastRequest( $escort_id );
		if ( $request ){
			$this->view->idcard_photos = $request->getPhotos();
		}
		
		if(Cubix_Application::getId() == APP_EF){
			$conf = Zend_Registry::get('images_config');
			$this->view->sid = urlencode(Cubix_Utils::crypter( $_COOKIE['PHPSESSID'], $conf['secret_key'], $conf['secret_iv'], 'e'));
		}

	}


	public function reasonAction()
	{

	}
}
