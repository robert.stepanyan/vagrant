<?php

class Verifications_WebcamController extends Zend_Controller_Action {
	
	public function init()
	{
		$this->model = new Model_Verifications_Requests();
	}
	
	public function indexAction() 
	{
		
	}
	
	public function dataAction()
	{
		$page = $this->_getParam('page');
		$per_page = $this->_getParam('per_page');
		$sort_field = $this->_getParam('sort_field');
		$sort_dir = $this->_getParam('sort_dir');
		
		
		$req = $this->_request;
		
		$status = Model_Verifications_Requests::PENDING;
		if ( $req->status )
			$status = $req->status;
		else 
			$status = '';
			
					
		$filter = array(			
			'e.showname' => $req->showname,
			'u.application_id' => $req->application_id,
			'vr.status' => $status,
			'vr.inform_method' => $req->inform_method,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to
		);
		
		$count = 0;
		$data = $this->model->getAll($page, $per_page, $sort_field, $sort_dir, $filter, $count, Model_Verifications_Requests::TYPE_WEBCAM);
		echo json_encode(array('data' => $data, 'count' => $count));
		die;
	}
	
	public function setStatusAction()
	{
		$status = $this->_getParam('status');
		$escort_id = $this->_getParam('escort_id');
		$request_id = $this->_getParam('request_id');
		$confirm_date = $this->_getParam('confirm_date');
		
		$model = new Model_Verifications_Requests();
		$model->setStatus($escort_id, $request_id, $status, $confirm_date);
		
		die;
	}
	
	public function upcomingAction()
	{
		
	}
	
	public function showAction()
	{
		$this->view->layout()->disableLayout();
		$req_id = $this->_request->request_id;
		
		$model = new Model_Verifications_Requests();
		$this->view->request = $model->get($req_id);
		
	}
}
