<?php

class Verifications_AgeVerificationController extends Zend_Controller_Action {
	
	const PACKAGE_STATUS_ACTIVE   = 2;

	public function init()
	{
		$this->model = new Model_Verifications_AgeVerification;
		$this->session = new Zend_Session_Namespace('add_verification_photos');
		$this->user = Zend_Auth::getInstance()->getIdentity();
		$this->view->config = Zend_Registry::get('videos_config');
		$this->view->host =  Cubix_Application::getById(Cubix_Application::getId())->host;
			
	}
	
	public function indexAction() 
	{
		
	}
		
	public function dataAction()
	{
		$page = $this->_getParam('page');
		$per_page = $this->_getParam('per_page');
		$sort_field = $this->_getParam('sort_field');
		$sort_dir = $this->_getParam('sort_dir');
		

		$req = $this->_request;
		
		/*$status = Model_Verifications_AgeVerification::PENDING;
		if ( $req->status )
			$status = $req->status;
		else 
			$status = '';*/
		
		$filter = array(			
			'e.showname' => $req->showname,
			'agency_name' => $req->agency_name,
			'e.id' => $req->escort_id,
			'u.application_id' => $req->application_id,
			'name' => $req->name,
			'last_name' => $req->last_name,
			'both' => $req->both,
			'day' => $req->date_day,
			'month' => $req->date_month,
			'year' => $req->date_year,
			'av.status' => $req->status,
			'special_case' => intval($req->special_case),
			'date_from' => $req->date_from,
			'date_to' => $req->date_to,
			'e.mark_not_new' => $req->not_new,
			'active_package' => $req->active_package
		);
		
		$count = 0;
		$data = $this->model->getAll($page, $per_page, $sort_field, $sort_dir, $filter, $count);

		$m_d = new Model_System_Dictionary();

		foreach ( $data as $i => $item ) {
			/*switch ($item->status)
			{
				case Model_Verifications_AgeVerification::PENDING:
					$data[$i]->status = 'PENDING';
					break;
				case Model_Verifications_AgeVerification::VERIFY:
					$data[$i]->status = 'VERIFIED';
					break;
				case Model_Verifications_AgeVerification::REJECTED:
					$data[$i]->status = 'REJECTED';
					break;
			}*/


			if ($req->application_id == APP_EF)
			{
				$db = Zend_Registry::get('db');
				$is_new_by_package = $db->fetchRow('
					SELECT
						IF ((op.date_activated > (CURDATE() - INTERVAL 5 DAY)) AND (e.mark_not_new = 0), 0, 1) AS mark_not_new
					FROM order_packages op
					INNER JOIN escorts e ON e.id = op.escort_id
					WHERE op.escort_id = ? AND op.application_id = ? AND op.status = ?
				', array($item->escort_id, $req->application_id, self::PACKAGE_STATUS_ACTIVE));

				if($is_new_by_package && $is_new_by_package->mark_not_new != 1){
					$data[$i]->mark_not_new = $is_new_by_package->mark_not_new;
				}

			}

			if ($item->status == Model_Verifications_AgeVerification::REJECTED && $item->reason_ids)
			{
				$r_ids = explode(',', $item->reason_ids);
				$reasons = array();
				
				foreach ($r_ids as $r_id)
				{
					if (in_array(Cubix_Application::getId(), array(APP_A6, APP_A6_AT)))
						$l = 'de';
					else
						$l = 'en';
					
					if ($r_id == 4)
						$reasons[$r_id] = $item->reason_text;
					else
						$reasons[$r_id] = $m_d->getById('verify_reject_reason_' . $r_id)->{'value_' . $l};
				}
				
				$data[$i]->reasons = json_encode($reasons);
				$data[$i]->rejected_date = date('d M Y H:i:s', $item->rejected_date);
				$data[$i]->is_read = $item->is_read ? 'Read' : 'Unread';
			}
			$duplicity = $this->model->getDuplicity($data[$i]->escort_id,$data[$i]->name,$data[$i]->last_name, $data[$i]->born_date);
			
			//$data[$i]->duplicity =  implode( ',', $duplicity);
			$data[$i]->duplicity =  json_encode($duplicity);
			$data[$i]->born_date = $data[$i]->born_date ? date("j M Y",strtotime($data[$i]->born_date )) : "---";
			
			if ($req->gallery_view)
			{	
				$escort_model = new Model_EscortsV2();
				$escort = $escort_model->get($item->escort_id);
				
				$photos = array('public' => array(), 'private' => array(), 'disabled' => array() );
				$added = false;

				foreach ( $escort->getPhotos() as $photo ) {
					$added = true;
					if($phots->is_portrait == 0 && !is_null($photo->args))
						$photo->rwidth = $this->GetPhotoWidth('200', $photo->width, $photo->height);
					 
					if ( $photo->type == ESCORT_PHOTO_TYPE_PRIVATE ) {
						$photos['private'][] = $photo;
					} elseif ( $photo->type == ESCORT_PHOTO_TYPE_DISABLED ) {
						$photos['disabled'][] = $photo;
					} else {
						$photos['public'][] = $photo;
					}
				}
				
				if ($added)
				{
					$this->view->data_photos = $photos;
					$this->view->addScriptPath(APPLICATION_PATH . '/views/scripts/');
					$html = $this->view->render('escorts-v2/data-photos.phtml');
					$data[$i]->photos_html = $html;
				}
			}
		}
		echo json_encode(array('data' => $data, 'count' => $count));
		die;
	}
	
	
	public function setStatusAction()
	{
		$status = $this->_getParam('status');
		$escort_id = $this->_getParam('escort_id');
		$request_id = $this->_getParam('request_id');
		$reason_ids = $this->_getParam('reason_ids');
		$free_text = $this->_getParam('free_text');
		$escort_status = new Cubix_EscortStatus($escort_id);

		$req = $this->_request;

		// Wizard send notification about change AC status for EF only
		if ($req->application_id == APP_EF)
		{
			$db = Zend_Registry::get('db');

			$needSend = $db->fetchRow('
				SELECT
					true
				FROM escorts
				WHERE id = ? AND wizard_notification_status = 0
			', array($escort_id));

			if($needSend){

				$removeReminder = new Cubix_EmailTrigger(null, 10, $escort_id);
				$removeReminder->removeFromDeferred();

				$triggerType = false;

				if($status == 4){//Reject
					$triggerType = 9;
				}elseif ($status == 3) { //Verify
					$revisionsStatus = $db->fetchRow('
						SELECT status
						FROM profile_updates_v2
						WHERE escort_id = ?
						ORDER BY revision DESC
					', $escort_id, Zend_Db::FETCH_OBJ);

					if($revisionsStatus && $revisionsStatus->status == 2){
						$triggerType = 7;	
					}

					if($revisionsStatus && $revisionsStatus->status == 1){
						$triggerType = 8;	
					}

				}
				
				if($triggerType){
					$db->update('escorts', array('wizard_notification_status' => 1), $db->quoteInto('id = ?', $escort_id));

					$et = new Cubix_EmailTrigger(null, $triggerType, $escort_id);
					$et->addToDeferred();
				}
			}
		}
		
		
		if ($status == Model_Verifications_AgeVerification::VERIFY ) //|| $status == Model_Verifications_Requests::REMOVED
		{
			$this->model->setStatus($request_id, $status);
					
			/*if ($status == Model_Verifications_AgeVerification::VERIFY)
			{*/
			$this->model->updateEscortStatus($escort_id, Model_Verifications_AgeVerification::VERIFY);
			$escort_status->removeStatusBit(Cubix_EscortStatus::ESCORT_STATUS_NEED_AGE_VERIFICATION);
			$escort_status->save();
			//}
		}
		else
		{
			$validator = new Cubix_Validator();
			if ( ! $reason_ids ) {
				$validator->setError('reason_1', 'Required');
				$validator->setError('reason_2', 'Required');
				$validator->setError('reason_3', 'Required');
				$validator->setError('reason_4', 'Required');
				$validator->setError('reason_5', 'Required');
				$validator->setError('reason_6', 'Required');
				$validator->setError('reason_7', 'Required');
				$validator->setError('reason_8', 'Required');
				
			}
			else if(strpos($reason_ids, '20') !== false && strlen($free_text) == 0){
				$validator->setError('reason_20', 'Required');
			}	
				

			if ( $validator->isValid() ) {
				$reasons = array();
				$need_video = 0;
				foreach(explode(',',$reason_ids) as $reason_id){
					if($reason_id == 20){
						$reasons[] = $free_text;
					}
					else{
						$reasons[] = Cubix_I18n::translateByLng('en','certify_reject_reason_'.$reason_id); 
					}
					if($reason_id == 6 || $reason_id == 8){
						$need_video = 1;
					}
				}
				
				$this->model->setStatus($request_id, $status, implode('<br/><br/>',$reasons));
				$this->model->updateEscortStatus($escort_id, Model_Verifications_AgeVerification::REJECTED, null, $need_video);
				$escort_status->setStatusBit(Cubix_EscortStatus::ESCORT_STATUS_NEED_AGE_VERIFICATION);
				$escort_status->save();
				
				$ids = explode(',', $reason_ids);
				$escort_data = $this->model->getEscortData($escort_id);
				$this->view->showname = $escort_data->showname;
				$this->view->ids = $ids;
				$this->view->free_text = $free_text;
				$subject = " Age Certification";
				
				$body = $this->view->render('email-templates/age-certification-ef.phtml');
				if ($body){
					Cubix_Email::send($escort_data->email, $subject, $body, true);
				}
			}
			
			die(json_encode($validator->getStatus()));
		}
		die;
	}
	
	public function photosAction()
	{
		$request_id = $this->_getParam('request_id');
		
		$request = $this->model->get($request_id);
		$this->view->age_verification_photos = $this->model->getPhotosByRequestId($request_id);
		
		$m_escorts = new Model_Escorts();
		$this->view->escort = $escort = $m_escorts->get($request->escort_id);
		$this->view->photos = $escort->getPhotos();
		$this->view->name = $request->name;
		$this->view->last_name = $request->last_name;
		$this->view->comment = $request->comment;
		$this->view->born_date = isset($request->born_date) ? date("j M Y", strtotime($request->born_date)) : null;
		$this->view->day = isset($request->born_date) ? date("j", strtotime($request->born_date)) : null;
		$this->view->month = isset($request->born_date) ? date("n", strtotime($request->born_date)) : null;
		$this->view->year = isset($request->born_date) ? date("Y", strtotime($request->born_date)) : null;
		$m_escort = new Model_EscortsV2();
		$this->view->susp_data = $m_escort->getSuspiciousData($request->escort_id);
		
		$this->view->user_type = $this->user->type;
		$conf = Zend_Registry::get('images_config');
		$this->view->sid = urlencode(Cubix_Utils::crypter( $_COOKIE['PHPSESSID'], $conf['secret_key'], $conf['secret_iv'], 'e'));
		
	}
	
	public function saveInfoAction()
	{
		$request_id = intval($this->_getParam('request_id'));
		$name = $this->_getParam('name');
		$last_name = $this->_getParam('last_name');
		$day = intval($this->_getParam('day'));
		$month = intval($this->_getParam('month'));
		$year = intval($this->_getParam('year'));
		$comment = $this->_getParam('comment');
		
		if ($this->_request->isPost())
		{
			$validator = new Cubix_Validator();
			
			if (strlen(trim($name)) == 0) {
				$validator->setError('name', 'Required');
			}
			
			if (strlen(trim($last_name)) == 0) {
				$validator->setError('last_name', 'Required');
			}
			
			if (!$day) {
				$validator->setError('day', 'Required');
			}
			
			if (!$month) {
				$validator->setError('month', 'Required');
			}
			
			if (!$year) {
				$validator->setError('year', 'Required');
			}
			
			if ( $validator->isValid() ) {
				$date = $year . '-' . $month . '-' . $day;
				
				$this->model->saveInfo($request_id, $name, $last_name, $date, $comment);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}

	public function reasonAction()
	{
		
	}
	
	public function certifyFromIdcardAction()
	{
		$escort_id = $this->_getParam('escort_id');
		$idcard_id = $this->_getParam('request_id');
		
		if ( ! $this->user->hasAccessToEscort($escort_id) ) {
			die('Permission denied');
		}
		$status = Model_Verifications_AgeVerification::VERIFY;
		$request_id = $this->model->get100Verified($escort_id);
		
		if($request_id){
			$this->model->setStatus($request_id, $status);
			$this->model->updateEscortStatus($escort_id, $status);
		}
		else{
			$this->model->addAgeVerificationFromIdcard($escort_id,$idcard_id);
		}
		$escort_status = new Cubix_EscortStatus($escort_id);
		if($escort_status->hasStatusBit(Cubix_EscortStatus::ESCORT_STATUS_NEED_AGE_VERIFICATION)){
			$escort_status->removeStatusBit(Cubix_EscortStatus::ESCORT_STATUS_NEED_AGE_VERIFICATION);
			$escort_status->save();
		}
		die;
	}
	
	public function certifyFromEscortsAction()
	{
		$this->view->layout()->disableLayout();
		$ids = $this->_request->id;
		
		foreach($ids as $escort_id){
			
			$status = Model_Verifications_AgeVerification::VERIFY;
			$request_id = $this->model->get100Verified($escort_id);

			if($request_id){
				$this->model->setStatus($request_id, $status);
				$this->model->updateEscortStatus($escort_id, $status);
			}
			else{
				$model_request = new Model_Verifications_Requests();
				$idcard_id = $model_request->getLastVerifiedId($escort_id);
				if($idcard_id){
					$this->model->addAgeVerificationFromIdcard($escort_id,$idcard_id);
				}
				else{
					$age_verification = array(
						'escort_id' => $escort_id,
						'creation_date' => new Zend_Db_Expr('NOW()'),
						'status' => Model_Verifications_AgeVerification::VERIFY
					);
					$this->model->add($age_verification);
					$escort_data  = array(
						'need_age_verification' => Model_Verifications_AgeVerification::VERIFY,
						'age_verify_disabled' => 0 
					);
					$this->model->updateEscort($escort_id, $escort_data );
				}
			}
			$escort_status = new Cubix_EscortStatus($escort_id);
			if($escort_status->hasStatusBit(Cubix_EscortStatus::ESCORT_STATUS_NEED_AGE_VERIFICATION)){
				$escort_status->removeStatusBit(Cubix_EscortStatus::ESCORT_STATUS_NEED_AGE_VERIFICATION);
				$escort_status->save();
			}
		}
		die;
	}
	
	public function addAction()
	{
		$this->view->layout()->disableLayout();
		
		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);

			$params = array(
				'escort_id' => 'int',
				'name' => '',
				'last_name' => '',
				'day' => 'int',
				'month' => 'int',
				'year' => 'int',
			);

			$data->setFields($params);
			$data = $data->getData();
						
			$validator = new Cubix_Validator();
			
			if ( ! $data['escort_id'] ) {
				$validator->setError('escort_id', 'Required');
			}
			
			if ( ! strlen($data['name']) ) {
				$validator->setError('name', 'Required');
			}
			
			if ( ! strlen($data['last_name']) ) {
				$validator->setError('last_name', 'Required');
			}
			
			if ( !$data['day'] || !$data['month'] || !$data['year']) {
				$validator->setError('day', 'Required');
			}elseif (!checkdate($data['month'],$data['day'], $data['year'])){
                $validator->setError('day', 'Invalid');
            }
			
			if ( $validator->isValid() ) {
				$bu_user = Zend_Auth::getInstance()->getIdentity();
				$data['creation_date'] = new Zend_Db_Expr('NOW()');
				$data['status'] = Model_Verifications_AgeVerification::VERIFY;
				$data['born_date'] = $data['year'] . '-' . $data['month'] . '-' . $data['day'];
				$data['admin_id'] = $bu_user->id;
				unset($data['year']);
				unset($data['month']);
				unset($data['day']);
				$Files = $this->session->photos;
				if (Cubix_Application::getId() == APP_EF){
                    ignore_user_abort(true);
                    ob_start();
                    echo json_encode($validator->getStatus());
                    ob_end_flush();
                    ob_flush();
                    flush();
                    session_write_close();
                    fastcgi_finish_request();
				    $Files = $this->getFileList($data['escort_id']);
				    $Files = $this->saveFiles($Files, $data['escort_id']);
				    $this->deleteFiles($data['escort_id']);
                }
				$this->model->add($data, $Files);
				$this->model->updateEscortStatus($data['escort_id'], Model_Verifications_AgeVerification::VERIFY);
				$escort_status = new Cubix_EscortStatus($data['escort_id']);
				if($escort_status->hasStatusBit(Cubix_EscortStatus::ESCORT_STATUS_NEED_AGE_VERIFICATION)){
					$escort_status->removeStatusBit(Cubix_EscortStatus::ESCORT_STATUS_NEED_AGE_VERIFICATION);
					$escort_status->save();
				}
				$this->session->unsetAll();
                if (Cubix_Application::getId() == APP_EF){
                    die;
                }
			}
			
			die(json_encode($validator->getStatus()));
		} else {
			$this->session->unsetAll();
		}
	}

    public function uploadHtml5EfAction() {

        $config = Zend_Registry::get('images_config');
        $videoConfig = Zend_Registry::get('videos_config');
        $tmp_dir = (!empty($videoConfig['temp_dir_path']) && is_dir($videoConfig['temp_dir_path'])) ? $videoConfig['temp_dir_path'] : sys_get_temp_dir();
        $tmp_dir = DIRECTORY_SEPARATOR.trim($tmp_dir,"/ \t\n\r\0\x0B");
        $allowed_video_formats = array('flv', 'wmv', '3gp', 'mkv', 'mp4', 'avi', 'mov');
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $escort_id = intval($this->_getParam('escort_id'));
        /* @var $request Zend_Controller_Request_Http */
        $request = $this->getRequest();
        try {

            $response = array(
                'id'	=> $request->getHeader('X_FILE_ID'),
                'name'	=> $request->getHeader('X_FILE_NAME'),
                'size'	=> $request->getHeader('X_FILE_SIZE'),
                'error'	=> 0,
                'msg' => '',
                'finish'	=> false
            );

            $ext = strtolower(@end(explode('.', $response['name'])));
            if ( !$escort_id ) {
                throw new Exception("Please select escort id", Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
            }

            if (!in_array( $ext , $config['allowedExts'])) {
                if (in_array($ext, $allowed_video_formats)) {
                    $maxSize = floatval($videoConfig['VideoMaxSize']);
                    $size =  number_format(($response['size'] / pow(1024, 2)));
                    $size =  floatval($size);
                    if ($size > $maxSize) {
                        throw new Exception(Cubix_I18n::translate('sys_error_upload_video_allowed_max_size', array('size' => $videoConfig['VideoMaxSize'])), Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
                    }

                    $file = $tmp_dir . DIRECTORY_SEPARATOR . Cubix_Application::getId().'_'. $escort_id.'_age_vrf_vid_'.$response['name'];
                    $file = preg_replace('/\s+/', '', $file);
                    file_put_contents($file, file_get_contents('php://input'));
                    $response['is_video'] = 1;
                    $response['finish'] = true;
                }else{
                    throw new Exception(Cubix_I18n::translate('sys_error_upload_img_allowed_extensions', array('extensions' => implode(', ', array_merge($config['allowedExts'],$allowed_video_formats)))), Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
                }
            }else{
                $file = $tmp_dir . DIRECTORY_SEPARATOR . Cubix_Application::getId().'_'.$escort_id.'_age_vrf_img_'.$response['name'];
                $file = preg_replace('/\s+/', '', $file);
                file_put_contents($file, file_get_contents('php://input'));
                // Make sure that the image is valid and get it's width, height and type
                $img_info = @getimagesize($file);

                list($width, $height) = $img_info;

                if ( false === $img_info ) {
                    unlink($file);
                    throw new Exception(Cubix_I18n::translate('sys_error_upload_img_only_images'), Cubix_Images::ERROR_IMAGE_INVALID);
                }

                $min_sidesize = $config['minImageSize'];

                $new_profile_apps = array(APP_A6, APP_EF, APP_BL, APP_6A, APP_6B, APP_6C, APP_AE, APP_A6_AT, APP_EG_CO_UK, APP_EG, APP_EM);

                if ( in_array(Cubix_Application::getId(), $new_profile_apps) ) {
                    $p_min_width = 400;
                    $p_min_height = 600;
                    $l_min_width = 500;
                    $l_min_height = 375;

                    if ( $width < $height ) {
                        if ( $width < $p_min_width || $height < $p_min_height ) {
                            unlink($file);
                            throw new Exception(Cubix_I18n::translate('sys_error_upload_img_size_min_a6', array('min_width' => $p_min_width, 'min_height' => $p_min_height, 'ratio' => Cubix_I18n::translate('portrait'))), Cubix_Images::ERROR_IMAGE_TOO_SMALL);
                        }
                    } else {
                        if ( $width < $l_min_width || $height < $l_min_height ) {
                            unlink($file);
                            throw new Exception(Cubix_I18n::translate('sys_error_upload_img_size_min_a6', array('min_width' => $l_min_width, 'min_height' => $l_min_height, 'ratio' => Cubix_I18n::translate('landscape'))), Cubix_Images::ERROR_IMAGE_TOO_SMALL);
                        }
                    }
                } else {
                    if ( $width < $min_sidesize || $height < $min_sidesize ) {
                        unlink($file);
                        throw new Exception(Cubix_I18n::translate('sys_error_upload_img_size_min', array('min_sidesize' => $min_sidesize)), Cubix_Images::ERROR_IMAGE_TOO_SMALL);
                    }
                }

                $response['finish'] = true;
            }
        } catch (Exception $e) {
            $response['error']	= 1;
            $response['msg']	= $e->getMessage();
        }

        echo json_encode($response);die;
    }
	
	public function uploadHtml5Action()
	{
		$config = Zend_Registry::get('images_config');
		$videoConfig = Zend_Registry::get('videos_config');
		$tmp_dir = (!empty($videoConfig['temp_dir_path']) && is_dir($videoConfig['temp_dir_path'])) ? $videoConfig['temp_dir_path'] : sys_get_temp_dir();
		$tmp_dir = DIRECTORY_SEPARATOR.trim($tmp_dir,"/ \t\n\r\0\x0B");
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$escort_id = intval($this->_getParam('escort_id'));
		/* @var $request Zend_Controller_Request_Http */
        $request = $this->getRequest();
		try {
			
			$response = array(
				'id'	=> $request->getHeader('X_FILE_ID'),
				'name'	=> $request->getHeader('X_FILE_NAME'),
				'size'	=> $request->getHeader('X_FILE_SIZE'),
				'error'	=> 0,
				'msg' => '',
				'finish'	=> false
			);
			
			$ext = strtolower(@end(explode('.', $response['name'])));
			$file = $tmp_dir . DIRECTORY_SEPARATOR . $response['name'];
			$file = preg_replace('/\s+/', '', $file);
			file_put_contents($file, file_get_contents('php://input'));
			
			if ( !$escort_id ) {
				throw new Exception("Please select escort id", Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
			}

            if (!in_array( $ext , $config['allowedExts'])) {
			    throw new Exception(Cubix_I18n::translate('sys_error_upload_img_allowed_extensions', array('extensions' => implode(', ', $config['allowedExts']))), Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
            }
            $images = new Cubix_Images();
            $photo = $images->save($file, $escort_id . '/age-verification', Cubix_Application::getId(), $ext);
            //$photo = array('hash' => "1111111111", 'ext'=> $ext);

            if (!isset($photo['hash'])) {
                throw new Exception("Photo upload failed. Please try again!");
            }
            unlink($file);
            $this->session->photos[$response['id']] = $photo;

            $image = new Cubix_Images_Entry($photo);
            $image->setSize('t100p');
            $image->setCatalogId($escort_id . '/age-verification');
            $response['photo_url'] = $images->getUrl($image);
            //$response['file_orig'] = str_replace('_t100p','',$response['file_url']);
            $response['finish'] = true;

		} catch (Exception $e) {
			$response['error']	= 1;
			$response['msg']	= $e->getMessage();
		}		
		
		echo json_encode($response);die;
	}
	
	public function ajaxRemovePhotoAction()
	{
		$image_id = $this->_request->image_id;
				
		$status = array('status' => 'error');
		if ( isset($this->session->photos[$image_id]) ) {
			unset($this->session->photos[$image_id]);
			$status = array('status' => 'success');
		}
		
		die(json_encode($status));
	}

	public function ajaxRemovePhotoEfAction()
	{
        $status = array('status' => 'error');
		$escort_id = $this->_request->getParam('escort_id');
		if (!$escort_id){
            die(json_encode($status));
        }
        $this->deleteFiles($escort_id);
        $status['status'] ='success';
		die(json_encode($status));
	}

    /**
     * @param $escortID
     * @throws Zend_Exception
     */
    private function deleteFiles($escortID){
        $videoConfig = Zend_Registry::get('videos_config');
        $tmp_dir = (!empty($videoConfig['temp_dir_path']) && is_dir($videoConfig['temp_dir_path'])) ? $videoConfig['temp_dir_path'] : sys_get_temp_dir();
        $pattern = $tmp_dir.DIRECTORY_SEPARATOR . Cubix_Application::getId().'_'. $escortID.'_nat_vrf_*';
        $FileList = glob($pattern);
        foreach ($FileList as $tmpFile){
            if(is_file($tmpFile)){
                unlink($tmpFile);
            }
        }
    }

    /**
     * @param $escortId
     * @return array
     * @throws Zend_Exception
     */
    private function getFileList($escortId){
        $config = Zend_Registry::get('images_config');
        $tempDir = !empty($config['temp_dir_path']) ? $config['temp_dir_path'] : sys_get_temp_dir();
        $pattern = $tempDir.DIRECTORY_SEPARATOR . Cubix_Application::getId().'_'. $escortId.'_*';
        $FileList = glob($pattern);
        $Files = array();
        foreach ($FileList as $k => $file){
            $pathInfo = pathinfo($file);
            if(in_array($pathInfo['extension'],$config['allowedExts'])){
                if(strpos($file,'age_vrf_img') > 0){
                    $Files['image'][] = array(
                        'tmp_name' => $file,
                        'name' => $pathInfo['basename'],
                        'size' => filesize($file),
                        'ext' => strtolower($pathInfo['extension'])
                    );
                }elseif(strpos($file,'nat_vrf_img') > 0){
                    $Files['pic'][] = array(
                        'tmp_name' => $file,
                        'name' => $pathInfo['basename'],
                        'size' => filesize($file),
                        'ext' => strtolower($pathInfo['extension'])
                    );
                }
            }else{
                if(strpos($file,'age_vrf_vid') > 0){
                    $Files['video'] = array(
                        'tmp_name' => $file,
                        'name' => $pathInfo['basename'],
                        'size' => filesize($file),
                        'ext' => strtolower($pathInfo['extension'])
                    );
                }elseif(strpos($file,'nat_vrf_vid') > 0){
                    $Files['natural-video'] = array(
                        'tmp_name' => $file,
                        'name' => $pathInfo['basename'],
                        'size' => filesize($file),
                        'ext' => strtolower($pathInfo['extension'])
                    );
                }
            }
        }

        return $Files;
    }

    /**
     * @param $data
     * @param $Files
     * @param $escort_id
     * @return array
     * @throws Zend_Exception
     */
    private function saveFiles($Files, $escort_id) {
        $images = new Cubix_Images();
        $video_config =  Zend_Registry::get('videos_config');
        $files = $verify_photos = array();
        foreach ($Files['image'] as $key => $image){
            if (!isset($image['tmp_name']) || $image['tmp_name'] == '' ) continue;
            $files[] = $images->save($image['tmp_name'], $escort_id . '/age-verification', Cubix_Application::getId(), $image['ext']);
        }

        if($Files['video']['tmp_name'] != '') {
            $video = $Files['video']['tmp_name'];
            $video_ftp = new Cubix_VideosCommon();
            $video_hash = uniqid() . '_age_certify';
            $video_model = new Cubix_ParseVideo($video, $escort_id, $video_config);
            $video = $video_model->ConvertToMP4();
            $video_ext = 'mp4';
            $name = $video_hash . '.' . $video_ext;
            if ($video_ftp->_storeToPicVideo($video, $escort_id, $name)) {
                $files[] = array('hash' => $video_hash, 'ext' => $video_ext, 'is_video' => 1);
            }
        }
        return $files;
    }

	public function commentAction()
	{	
		$this->view->layout()->disableLayout();
				
		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'id' => 'int',
				'comment' => 'special|notags'
			));
			$data = $data->getData();
			
			$validator = new Cubix_Validator();
						
			if ( $validator->isValid() ) 
			{	
				$this->model->updateComment($data);
			}

			die(json_encode($validator->getStatus()));
		}
		else
		{
			$id = intval($this->_request->id);
			$this->view->id = $id;
			$this->view->comment = $this->model->getComment($id);
		}
	}
	
	public function rotateAction()
	{
		$degree = intval($this->_getParam('degree'));
		$degree = $degree == 90 ? 90 : -90;
		$escort_id = $this->_getParam('escort_id');
		$hash = $this->_getParam('hash');
		$ext = $this->_getParam('ext');

		try {
			$conf = Zend_Registry::get('images_config');
			
			get_headers($conf['remote']['url'] . "/get_image.php?a=rotate&sub_dir=age-verification&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash . "&ext=". $ext . "&d=". $degree);
			get_headers($conf['remote']['url'] . "/get_image.php?a=clear_cache&sub_dir=age-verification&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash);
			
			$catalog = $escort_id;
			
			$parts = array();

			if ( strlen($catalog) > 2 ) {
				$parts[] = substr($catalog, 0, 2);
				$parts[] = substr($catalog, 2);
			}
			else {
				$parts[] = '_';
				$parts[] = $catalog;
			}
			
			$parts[] = "age-verification";
			$catalog = implode('/', $parts);
			
			get_headers($conf['remote']['url'] . "/" . Cubix_Application::getById()->host . "/" . $catalog . "/" . $hash . "_t100p.".$ext);

		}
		catch ( Exception $e ) {
			die(json_encode(array('error' => 'An error occured')));
		}

		die(json_encode(array('success' => true)));
	
	}
	
	private function GetPhotoWidth($height,$natural_width,$natural_height)
	{
		$ratio = $height /$natural_height; 
		return $natural_width * $ratio; 
	}
	
	public function markSpecialAction()
	{
		if (!in_array($this->user->type, array('superadmin', 'admin')))
			die('permission denied');
		
		$id = intval($this->_getParam('id'));
		$this->model->markSpecial($id);
		die(json_encode(array('success' => true)));
	}
	public function removeNewAction(){
		$req = $this->_request;
		$this->model->removeNew($req->escort_id);
		die(json_encode(array('success' => true)));
	}
	public function restoreNewAction(){
		$req = $this->_request;
		$this->model->restoreNew($req->escort_id);
		die(json_encode(array('success' => true)));
	}
}
