<?php

class Verifications_VipController extends Zend_Controller_Action {
	
	public function init()
	{
		if (Cubix_Application::getId() != APP_6A)
			die('permission denied');
		
		$user = Zend_Auth::getInstance()->getIdentity();
		
		if ($user->type != 'superadmin')
			die('permission denied');
		
		$this->model = new Model_Verifications_Vip();
	}
	
	public function indexAction() 
	{
		
	}
			
	public function dataAction()
	{
		$page = $this->_getParam('page');
		$per_page = $this->_getParam('per_page');
		$sort_field = $this->_getParam('sort_field');
		$sort_dir = $this->_getParam('sort_dir');
		
		$req = $this->_request;
		
		$status = Model_Verifications_Vip::PENDING;
		if ( $req->status )
			$status = $req->status;
		else 
			$status = '';
		
		$filter = array(			
			'e.showname' => $req->showname,
			'e.id' => $req->escort_id,
			'u.application_id' => $req->application_id,
			'v.status' => $status,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to,
			'with_comment' => $req->with_comment,
			'active_package_id' => $req->active_package_id,
			'e.verified_status' => $req->verified == 'verified' ? Model_EscortsV2::STATUS_VERIFIED : ($req->verified == 'not_verified' ? Model_EscortsV2::STATUS_NOT_VERIFIED : null)
		);
		
		$count = 0;
		$data = $this->model->getAll($page, $per_page, $sort_field, $sort_dir, $filter, $count);
		
		$m_bu = new Model_BOUsers();
		$m_a = new Model_Agencies();
		
		foreach ( $data as $i => $item ) {
			if ($item->request_owner == Model_Verifications_Vip::REQUEST_OWNER_ESCORT)
				$data[$i]->request_owner = '<strong><i>Escort:</i></strong> ' . $item->showname;
			elseif ($item->request_owner == Model_Verifications_Vip::REQUEST_OWNER_AGENCY)
				$data[$i]->request_owner = '<strong><i>Agency:</i></strong> ' . $m_a->getByUserId($item->user_id)->name;
			elseif ($item->request_owner == Model_Verifications_Vip::REQUEST_OWNER_ADMIN && $item->req_own_backend_user_id)
				$data[$i]->request_owner = '<strong><i>BU:</i></strong> ' . $m_bu->get($item->req_own_backend_user_id)->username;
		}
		
		echo json_encode(array('data' => $data, 'count' => $count));
		die;
	}
	
	public function createAction() 
	{
		if ( $this->_request->isPost() )
		{
			$data = new Cubix_Form_Data($this->_request);

			$params = array(
				'escort_f_h' => 'int',
				'escort_id' => 'int'
			);

			$data->setFields($params);
			
			$data = $data->getData();
						
			$validator = new Cubix_Validator();
						
			if ( ! $data['escort_f_h'] && ! $data['escort_id'] ) {
				$validator->setError('escort_f', 'Please Select Escort OR write Escort ID');
			}
			else
			{
				$data['escort_id'] ? $e_id = $data['escort_id'] : $e_id = $data['escort_f_h'];
				$last_req = $this->model->getLastRequestByEscort($e_id);
				
				if ($last_req && $last_req->status == Model_Verifications_Vip::VERIFY)
					$validator->setError('escort_f', 'This escort already has Verified VIP status');
				elseif ($last_req && $last_req->status == Model_Verifications_Vip::PENDING)
					$validator->setError('escort_f', 'This escort already has Pending VIP status');
			}
									
			if ( $validator->isValid() ) {
				
				$user = Zend_Auth::getInstance()->getIdentity();
				
				$data['bu_id'] = $user->id;
				$data['e_id'] = $e_id;
				
				$this->model->add($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function listAction()
	{
		$escort = trim($this->_getParam('escort'));
		
		if ( ! strlen($escort) ) {
			die(json_encode(array()));
		}

		$list = $this->model->getEscortsList($escort);

		$tmp = array();
		
		if ( count($list) ) 
		{
			foreach ( $list as $i => $l ) 
			{
				$name = $l->showname . ' (ID: ' . $l->id . ')';
								
				$tmp[$i] = array('id' => $l->id, 'name' => $name);
			}
		}

		echo json_encode($tmp);
		die;
	}
	
	public function editAction()
	{
		$this->view->layout()->disableLayout();
		
		$r = $this->model->get($this->_request->id);
				
		$m_bu = new Model_BOUsers();
		$m_a = new Model_Agencies();
		
		if ($r->request_owner == Model_Verifications_Vip::REQUEST_OWNER_ESCORT)
			$r->request_owner = '<strong><i>Escort</i></strong>';
		elseif ($r->request_owner == Model_Verifications_Vip::REQUEST_OWNER_AGENCY)
			$r->request_owner = '<strong><i>Agency:</i></strong> ' . $m_a->getByUserId($r->user_id)->name;
		elseif ($r->request_owner == Model_Verifications_Vip::REQUEST_OWNER_ADMIN && $r->req_own_backend_user_id)
			$r->request_owner = '<strong><i>BU:</i></strong> ' . $m_bu->get($r->req_own_backend_user_id)->username;
		
		$this->view->r = $r;
		
		$history = $this->model->getHistory($this->_request->id, $r->escort_id);
		
		if ($history)
		{
			foreach ($history as $i => $h)
			{
				if ($h->request_owner == Model_Verifications_Vip::REQUEST_OWNER_ESCORT)
					$history[$i]->request_owner = '<strong><i>Escort</i></strong>';
				elseif ($h->request_owner == Model_Verifications_Vip::REQUEST_OWNER_AGENCY)
					$history[$i]->request_owner = '<strong><i>Agency:</i></strong> ' . $m_a->getByUserId($h->user_id)->name;
				elseif ($h->request_owner == Model_Verifications_Vip::REQUEST_OWNER_ADMIN && $h->req_own_backend_user_id)
					$history[$i]->request_owner = '<strong><i>BU:</i></strong> ' . $m_bu->get($h->req_own_backend_user_id)->username;
			}
		}
		
		$this->view->history = $history;
	}
	
	public function removeAction()
	{
		$id = $this->_request->id;
		
		if (!$id) die;
		
		$this->model->remove($id);
		
		die;
	}

	public function setStatusAction()
	{
		$status = $this->_getParam('status');
		$id = $this->_getParam('id');
		$reason = $this->_getParam('reason');
		
		if (!$status) die;
		if (!$id) die;
		
		$user = Zend_Auth::getInstance()->getIdentity();
		
		$validator = new Cubix_Validator();
		
		if (in_array($status, array(Model_Verifications_Vip::VERIFY, Model_Verifications_Vip::SUSPENDED)))
		{
			$this->model->setStatus($id, $status, $user->id);
			
		}
		elseif ($status == Model_Verifications_Vip::REJECTED)
		{
			if (strlen($reason) == 0) {
				$validator->setError('reject_reason', 'Required');
			}
			
			if ($validator->isValid())
			{
				$this->model->setStatus($id, $status, $user->id, $reason);
			}
		}
		
		die(json_encode($validator->getStatus()));
	}
	
	public function reasonAction()
	{
		
	}
	
	public function commentAction()
	{	
		$this->view->layout()->disableLayout();
				
		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'id' => 'int',
				'comment' => 'special|notags'
			));
			$data = $data->getData();
			
			$validator = new Cubix_Validator();
						
			if ( $validator->isValid() ) 
			{	
				$this->model->updateComment($data);
			}

			die(json_encode($validator->getStatus()));
		}
		else
		{
			$id = intval($this->_request->id);
			$this->view->id = $id;
			$this->view->comment = $this->model->getComment($id);
		}
	}
}
