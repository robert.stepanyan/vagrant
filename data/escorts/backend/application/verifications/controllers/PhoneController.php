<?php

class Verifications_PhoneController extends Zend_Controller_Action {
	
	public function init()
	{
		$this->model = new Model_Verifications_Phone();
		$this->user = Zend_Auth::getInstance()->getIdentity();
        $admins = ['Carrie', 'jolyne', 'super-admin', 'may'];
        $currentAdminKey = $this->user->first_name;
		if (Cubix_Application::getId() == APP_ED && !in_array($currentAdminKey, $admins) )
		{
			die('Permission denied');
		}	
	}
	
	public function indexAction() 
	{	
		
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'escort_id' => $req->escort_id,
			'showname' => $req->showname,
			'phone' => $req->phone,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to,
			'status' => $req->status,
			'method' => $req->method
		);
		
		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		if(count($data) > 0 ){
			foreach($data as $row){
				$row->escort_id = "<span class='escort-popup'><a href='#". $row->escort_id."'> ".$row->escort_id." </a></span>";
			}
		}
		
		$data = array(
			'data' => $data,
			'count' => $count
		);
		die(json_encode($data));
		
	}		
	
	public function editAction()
	{
		$this->view->layout()->disableLayout();
		$this->view->data =  $this->model->GetCurrent($this->_request->escort_id);
		$pr = new Model_Countries();
		$this->view->phone_countries = $pr->getPhonePrefixs();
	}
	
	public function saveAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		if(isset($this->_request->phone) && is_numeric($this->_request->phone))
		{

			$this->model->SavePhone($this->_request->escort_id,$this->_request->phone,$this->_request->country_code);
		}
	}
}
