<?php

class Verifications_IdcardController extends Zend_Controller_Action {
	
	const PACKAGE_STATUS_ACTIVE   = 2;

	public function init()
	{
		$this->model = new Model_Verifications_Requests();
		$this->session = new Zend_Session_Namespace('add_verification_photos');
		$this->view->config = Zend_Registry::get('videos_config');
		$this->view->host =  Cubix_Application::getById(Cubix_Application::getId())->host;
		$this->user = Zend_Auth::getInstance()->getIdentity();
		if (Cubix_Application::getId() == APP_A6 && $this->user->type != 'superadmin' && $this->user->username != "backoffice" )
		{
			die('Permission denied');
		}	
	}
	
	public function indexAction() 
	{
		
	}
		
	public function dataAction()
	{
		$page = $this->_getParam('page');
		$per_page = $this->_getParam('per_page');
		$sort_field = $this->_getParam('sort_field');
		$sort_dir = $this->_getParam('sort_dir');
		$age_model = new Model_Verifications_AgeVerification();
		
		$req = $this->_request;
		
		$status = Model_Verifications_Requests::PENDING;
		if ( $req->status )
			$status = $req->status;
		else 
			$status = '';
			
					
		$filter = array(			
			'e.showname' => $req->showname,
			'e.id' => $req->escort_id,
			'u.application_id' => $req->application_id,
			'vr.status' => $status,
			'vr.inform_method' => $req->inform_method,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to,
            'active_package' => $req->active_package
		);
		
		$count = 0;
		$data = $this->model->getAll($page, $per_page, $sort_field, $sort_dir, $filter, $count, Model_Verifications_Requests::TYPE_IDCARD);

		$m_d = new Model_System_Dictionary();
		
		foreach ( $data as $i => $item ) {
			switch ($item->status)
			{
				case Model_Verifications_Requests::PENDING:
					$data[$i]->status = 'PENDING';
					break;
				case Model_Verifications_Requests::VERIFY:
					$data[$i]->status = 'VERIFY';
					break;
				case Model_Verifications_Requests::REJECTED:
					$data[$i]->status = 'REJECTED';
					break;
				case Model_Verifications_Requests::BLUR_VERIFY:
					$data[$i]->status = 'BLUR';	
					break;
			}
			
			
			
			
			
			
			if ($req->application_id == APP_EF)
			{
				$db = Zend_Registry::get('db');
				$is_new_by_package = $db->fetchRow('
					SELECT
						IF ((op.date_activated > (CURDATE() - INTERVAL 5 DAY)) AND (e.mark_not_new = 0), 0, 1) AS mark_not_new
					FROM order_packages op
					INNER JOIN escorts e ON e.id = op.escort_id
					WHERE op.escort_id = ? AND op.application_id = ? AND op.status = ?
				', array($item->escort_id, $req->application_id, self::PACKAGE_STATUS_ACTIVE));

				if($is_new_by_package && $is_new_by_package->mark_not_new != 1){
					$data[$i]->mark_not_new = $is_new_by_package->mark_not_new;
				}

			}

			if ($item->status == 'REJECTED' && $item->reason_ids)
			{
				$r_ids = explode(',', $item->reason_ids);
				$reasons = array();
				
				foreach ($r_ids as $r_id)
				{
					if (in_array(Cubix_Application::getId(), array(APP_A6, APP_A6_AT)))
						$l = 'de';
					else
						$l = 'en';
					
					if ($r_id == 4)
						$reasons[$r_id] = $item->reason_text;
					else
						$reasons[$r_id] = $m_d->getById('verify_reject_reason_' . $r_id)->{'value_' . $l};
					
					
					
				}
				
				$data[$i]->reasons = json_encode($reasons);
				$data[$i]->rejected_date = date('d M Y H:i:s', $item->rejected_date);
				$data[$i]->is_read = $item->is_read ? 'Read' : 'Unread';
			}

		
			
			if (Cubix_Application::getId() == APP_EF ){
				$duplicity = $age_model->getDuplicity($data[$i]->escort_id,$data[$i]->name,$data[$i]->last_name, $data[$i]->born_date);
				$data[$i]->duplicity =  json_encode($duplicity);
			}
		}
		
		
		
		
		
			
					
		echo json_encode(array('data' => $data, 'count' => $count));
		die;
	}
	
	public function addAction()
	{
		if ($this->user->type != 'superadmin' && $this->user->type != 'admin'){
			die('Permission denied');
		}
		$escort_id = intval($this->_getParam('escort_id'));
		$request = $this->model->getLastRequest($escort_id);
		$idcard_photos = array();
		if($request){
			 $idcard_photos = $request->getPhotos();
		}
		$m_escorts = new Model_Escorts();
		$this->view->escort = $escort = $m_escorts->get($escort_id);
		$this->view->photos = $escort->getPhotos();
		$this->view->idcard_photos = $idcard_photos;
		$this->view->escort_id = $escort_id;
		if ( $this->_request->isPost() ) {
			$validator = new Cubix_Validator();
            $Files = $this->session->verify_files;
            if (Cubix_Application::getId() == APP_EF){
                $Files = $this->getFileList($escort_id);
			}
			if ( count($Files) < 1) {
				$validator->setError('error_message', 'Upload Verification Photos');
			}
			if ( $validator->isValid() ) {

			    if (Cubix_Application::getId() == APP_EF){
                    ignore_user_abort(true);
                    ob_start();
                    echo json_encode($validator->getStatus());
                    ob_end_flush();
                    ob_flush();
                    flush();
                    session_write_close();
                    fastcgi_finish_request();
                    $Files = $this->getFileList($escort_id);
					$Files = $this->saveFiles($Files, $escort_id);
                    $this->deleteFiles($escort_id);
				}
				try{
					$verify_request = array(
						'type' => Model_Verifications_Requests::TYPE_IDCARD,
						'escort_id'=> $escort_id,
						'inform_method' => 0,
						'from_back' => 1
					);
					$request_id = $this->model->addRequest($verify_request);

					foreach($Files as $pic){
						$pic['request_id'] = $request_id;
						$this->model->addPhoto($pic);
					}
					unset($this->session->verify_files);
                    if (Cubix_Application::getId() == APP_EF){
                        die;
                    }
				}catch (Exception $e) {
					die(json_encode(array('error' => $e->getMessage())));
				}
			}
			die(json_encode($validator->getStatus()));
			
		}
		else{
			$this->session->verify_files = array();
		}
		
	}
	
	public function addVerPhotosAction(){
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$config = Zend_Registry::get('images_config');
        $videoConfig = Zend_Registry::get('videos_config');
        $tmp_dir = (!empty($videoConfig['temp_dir_path']) && is_dir($videoConfig['temp_dir_path'])) ? $videoConfig['temp_dir_path'] : sys_get_temp_dir();
        $tmp_dir = DIRECTORY_SEPARATOR.trim($tmp_dir,"/ \t\n\r\0\x0B");
        $allowed_video_formats = array('flv', 'wmv', '3gp', 'mkv', 'mp4', 'avi', 'mov');
		$escort_id = intval($this->_getParam('escort_id'));
        /* @var $request Zend_Controller_Request_Http */
        $request = $this->getRequest();
		try {
            if (!$this->user->hasAccessToEscort($escort_id)) {
                throw new Exception('Permission denied');
            }
            $escorts_v2_model = new Model_EscortsV2();
            $escort = $escorts_v2_model->get($escort_id);

            if (!$escort) {
                throw new Exception('Wrong id supplied');
            }

            $upload_pic_name = $request->getHeader('X_FILE_NAME');
            $upload_pic_id = $request->getHeader('X_FILE_ID');
            $upload_pic_size = $request->getHeader('X_FILE_SIZE');
            //$new_profile_apps = array(APP_A6, APP_EF, APP_BL, APP_6A, APP_6B);
            $ext = strtolower(@end(explode('.', $upload_pic_name)));
            $response = array(
                'id' => $upload_pic_id,
                'name' => $upload_pic_name,
                'size' => $upload_pic_size,
                'file_url' => '',
                'error' => 0,
                'msg' => '',
                'finish' => FALSE
            );
                $file = $tmp_dir . DIRECTORY_SEPARATOR . $upload_pic_name;
            file_put_contents($file, file_get_contents('php://input'));

            if (!in_array($ext, $config['allowedExts'])) {
                throw new Exception(Cubix_I18n::translate('sys_error_upload_img_allowed_extensions', array('extensions' => implode(', ', array_merge($config['allowedExts'], $allowed_video_formats)))), Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
            }
            // Save on remote storage
            $images = new Cubix_Images();
            $image = $images->save($file, $escort_id . '/verify', $escort->application_id, $ext);

            if (!isset($image['hash'])) {
                throw new Exception("Photo upload failed. Please try again!");
            }

            $verify_files = array(
                'hash' => $image['hash'],
                'ext' => $ext
            );

            $this->session->verify_files[$upload_pic_id] = $verify_files;
            $image = new Cubix_Images_Entry($image);
            $image->setSize('t100p');
            $image->setCatalogId($escort_id . '/verify');
            $response['file_url'] = $images->getUrl($image);
            $response['file_orig'] = str_replace('_t100p', '', $response['file_url']);
            $response['finish'] = true;
            $response['session'] = $this->session->verify_files;

		} catch (Exception $e) {
			$response['error']	= 1;
			$response['msg']	= $e->getMessage();
		}

		echo json_encode($response);
	}

	public function addVerPhotosEfAction(){
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$config = Zend_Registry::get('images_config');
        $videoConfig = Zend_Registry::get('videos_config');
		
        $tmp_dir = (!empty($videoConfig['temp_dir_path']) && is_dir($videoConfig['temp_dir_path'])) ? $videoConfig['temp_dir_path'] : sys_get_temp_dir();
        $tmp_dir = DIRECTORY_SEPARATOR.trim($tmp_dir,"/ \t\n\r\0\x0B");
        $allowed_video_formats = array('flv', 'wmv', '3gp', 'mkv', 'mp4', 'avi', 'mov');
		$escort_id = intval($this->_getParam('escort_id'));
        /* @var $request Zend_Controller_Request_Http */
        $request = $this->getRequest();
		try {
			if ( ! $this->user->hasAccessToEscort($escort_id) ) {
				throw new Exception('Permission denied');
			}
			$escorts_v2_model = new Model_EscortsV2();
			$escort = $escorts_v2_model->get($escort_id);

			if ( ! $escort ) {
				throw new Exception('Wrong id supplied');
			}

			$upload_pic_name = $request->getHeader('X_FILE_NAME');
			$upload_pic_id	 = $request->getHeader('X_FILE_ID');
			$upload_pic_size = $request->getHeader('X_FILE_SIZE');
			//$new_profile_apps = array(APP_A6, APP_EF, APP_BL, APP_6A, APP_6B);
			$ext = strtolower(@end(explode('.', $upload_pic_name)));
				$response = array(
				'id'	=> $upload_pic_id,
				'name'	=> $upload_pic_name,
				'size'  => $upload_pic_size,
				'file_url'	=> '',
				'error'	=> 0,
				'msg' => '',
				'finish'	=> FALSE
			);

			if (!in_array( $ext , $config['allowedExts'])){
                    if (in_array($ext, $allowed_video_formats)) {
                        $maxSize = floatval($videoConfig['VideoMaxSize']);
                        $size =  number_format(($response['size'] / pow(1024, 2)));
                        $size =  floatval($size);
                        if ($size > $maxSize) {
                            throw new Exception(Cubix_I18n::translate('sys_error_upload_video_allowed_max_size', array('size' => $videoConfig['VideoMaxSize'])), Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
                        }

                        $file = $tmp_dir . DIRECTORY_SEPARATOR . Cubix_Application::getId().'_'.$escort_id.'_nat_vrf_vid_'.$upload_pic_name;
                        $file =  preg_replace('/\s+/', '', $file);
                        file_put_contents($file, file_get_contents('php://input'));
                    }else{
                        throw new Exception(Cubix_I18n::translate('sys_error_upload_img_allowed_extensions', array('extensions' => implode(', ', array_merge($config['allowedExts'],$allowed_video_formats)))), Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
                    }
			}else{
                $file = $tmp_dir . DIRECTORY_SEPARATOR . Cubix_Application::getId().'_'.$escort_id.'_nat_vrf_img_'.$upload_pic_name;
                $file =  preg_replace('/\s+/', '', $file);
				
				
                file_put_contents($file, file_get_contents('php://input'));
				//var_dump($file);
                // Make sure that the image is valid and get it's width, height and type
                $img_info = @getimagesize($file);
				
                list($width, $height) = $img_info;
				//var_dump($img_info);
                if ( false === $img_info ) {
					//unlink($file);
					die('-----------');
                    //throw new Exception(Cubix_I18n::translate('sys_error_upload_img_only_images'), Cubix_Images::ERROR_IMAGE_INVALID);
                }

                $min_sidesize = $config['minImageSize'];

                $new_profile_apps = array(APP_A6, APP_EF, APP_BL, APP_6A, APP_6B, APP_6C, APP_AE, APP_A6_AT, APP_EG_CO_UK, APP_EG, APP_EM);

                if ( in_array(Cubix_Application::getId(), $new_profile_apps) ) {
                    $p_min_width = 400;
                    $p_min_height = 600;
                    $l_min_width = 500;
                    $l_min_height = 375;

                    if ( $width < $height ) {
                        if ( $width < $p_min_width || $height < $p_min_height ) {
                            unlink($file);
                            throw new Exception(Cubix_I18n::translate('sys_error_upload_img_size_min_a6', array('min_width' => $p_min_width, 'min_height' => $p_min_height, 'ratio' => Cubix_I18n::translate('portrait'))), Cubix_Images::ERROR_IMAGE_TOO_SMALL);
                        }
                    } else {
                        if ( $width < $l_min_width || $height < $l_min_height ) {
                            unlink($file);
                            throw new Exception(Cubix_I18n::translate('sys_error_upload_img_size_min_a6', array('min_width' => $l_min_width, 'min_height' => $l_min_height, 'ratio' => Cubix_I18n::translate('landscape'))), Cubix_Images::ERROR_IMAGE_TOO_SMALL);
                        }
                    }
                } else {
                    if ( $width < $min_sidesize || $height < $min_sidesize ) {
                        unlink($file);
                        throw new Exception(Cubix_I18n::translate('sys_error_upload_img_size_min', array('min_sidesize' => $min_sidesize)), Cubix_Images::ERROR_IMAGE_TOO_SMALL);
                    }
                }
                $response['finish'] = true;
            }

		} catch (Exception $e) {
			$response['error']	= 1;
			$response['msg']	= $e->getMessage();
		}

		echo json_encode($response);
	}

    public function ajaxRemovePhotoEfAction()
    {
        $status = array('status' => 'error');
        $escort_id = $this->_request->getParam('escort_id');
        if (!$escort_id){
            die(json_encode($status));
        }
        $this->deleteFiles($escort_id);
        $status['status'] ='success';
        die(json_encode($status));
    }

    /**
     * @param $escortID
     * @throws Zend_Exception
     */
    private function deleteFiles($escortID){
        $videoConfig = Zend_Registry::get('videos_config');
        $tmp_dir = (!empty($videoConfig['temp_dir_path']) && is_dir($videoConfig['temp_dir_path'])) ? $videoConfig['temp_dir_path'] : sys_get_temp_dir();
        $pattern = $tmp_dir.DIRECTORY_SEPARATOR . Cubix_Application::getId().'_'. $escortID.'_nat_vrf_*';
        $FileList = glob($pattern);
        foreach ($FileList as $tmpFile){
            if(is_file($tmpFile)){
                unlink($tmpFile);
            }
        }
    }

    /**
     * @param $escortId
     * @return array
     * @throws Zend_Exception
     */
    private function getFileList($escortId){
        $config = Zend_Registry::get('images_config');
        $allowed_image_Exts= array('jpg', 'jpeg', 'gif', 'png');
        $allowed_video_formats = array('flv','wmv','3gp','mkv','mp4', 'avi','mov');
        $tempDir = !empty($config['temp_dir_path']) ? $config['temp_dir_path'] : sys_get_temp_dir();
        $pattern = $tempDir.DIRECTORY_SEPARATOR . Cubix_Application::getId().'_'. $escortId.'_*';
        $FileList = glob($pattern);
        $Files = array();
        foreach ($FileList as $k => $file){
            $pathInfo = pathinfo($file);
            if(in_array(strtolower($pathInfo['extension']),$allowed_image_Exts)){
                $Files['pic'][] = array(
                    'tmp_name' => $file,
                    'name' => $pathInfo['basename'],
                    'size' => filesize($file),
                    'ext' => $pathInfo['extension']
                );
            }elseif(in_array(strtolower($pathInfo['extension']),$allowed_video_formats)){
                $Files['video'] = array(
                    'tmp_name' => $file,
                    'name' => $pathInfo['basename'],
                    'size' => filesize($file),
                    'ext' => $pathInfo['extension']
                );
            }
        }

        return $Files;
    }

    /**
     * @param $data
     * @param $Files
     * @param $escort_id
     * @return array
     * @throws Zend_Exception
     */
    private function saveFiles($Files, $escort_id) {
        $images = new Cubix_Images();
        $video_config =  Zend_Registry::get('videos_config');
        $files = $verify_photos = array();
        foreach ($Files['pic'] as $key => $image){
            if (!isset($image['tmp_name']) || $image['tmp_name'] == '' ) continue;
            $files[] = $images->save($image['tmp_name'], $escort_id . '/verify', Cubix_Application::getId(), $image['ext']);
        }

        if($Files['natural-video']['tmp_name'] != '') {
            $video = $Files['natural-video']['tmp_name'];
            $video_ftp = new Cubix_VideosCommon();
            $video_hash = uniqid() . '_idcard';
            $video_model = new Cubix_ParseVideo($video, $escort_id, $video_config);
            $video = $video_model->ConvertToMP4();
            $video_ext = 'mp4';
            $name = $video_hash . '.' . $video_ext;
            if ($video_ftp->_storeToPicVideo($video, $escort_id, $name)) {
                $files[] = array('hash' => $video_hash, 'ext' => $video_ext, 'is_video' => 1);
            }
        }
        return $files;
    }

	public function setStatusAction()
	{
		$status = $this->_getParam('status');
		$escort_id = $this->_getParam('escort_id');
		$request_id = $this->_getParam('request_id');
		$reason_ids = $this->_getParam('reason_ids');
		$free_text = $this->_getParam('free_text');
		$ids = $this->_getParam('id');
		
		if ( ! $this->user->hasAccessToEscort($escort_id) ) {
			die('Permission denied');
		}
		$model = new Model_Verifications_Requests();
		
		$m = new Model_Escorts();
		$m_escort = new Model_EscortsV2();
		$esc = $m->get($escort_id);
		$showname = $esc->showname;

		$subject = '100% verification process';

		if ( $status == Model_Verifications_Requests::PHOTOSHOP || $status == Model_Verifications_Requests::VERIFY || $status == Model_Verifications_Requests::REMOVED)
		{
			$model->setStatus($escort_id, $request_id, $status);
			
			if ($status == Model_Verifications_Requests::VERIFY || $status == Model_Verifications_Requests::PHOTOSHOP)
			{
				if ( Cubix_Application::getId() == APP_6A ) {
					$body = $this->view->render('email-templates/verification-success-6a.phtml');
				}
				elseif ( Cubix_Application::getId() == APP_EF ) 
				{
					$body = $this->view->render('email-templates/verification-success-ef.phtml');
				}
				elseif ( Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT )
				{
					$body = $this->view->render('email-templates/verification-success-and6.phtml');
				}
				
				if ($body)
					Cubix_Email::send($esc->email, $subject, $body, true);
				
				//Sending email notification				
				if(Cubix_Application::getId() == APP_ED){
					if(is_numeric($esc->agency_id)){
						$email_template = '100_verification_approved_agency_v1';
						$shortcodes['escort_showname'] = $esc->showname;
						$shortcodes['agency'] = $esc->agency_name;
					} else{
						$email_template = '100_verification_approved_v1';
						$shortcodes['showname'] = $esc->showname;
					}
					$shortcodes['url'] = 'https://www.'.Cubix_Application::getById()->host.'/escort/'.$esc->showname.'-'.$esc->id;
					Cubix_Email::sendTemplate( $email_template, $esc->email, $shortcodes );

				}elseif(in_array(Cubix_Application::getId() , array(APP_EG_CO_UK, APP_6B))){
					if(is_numeric($esc->agency_id)){
						$email_template = 'agency_photo_verification_approved';
						$shortcodes['showname'] = $esc->showname? $esc->showname : $esc->username;
						$shortcodes['agency'] = $esc->agency_name;
					} else{
						$email_template = 'escort_photo_verification_approved';
						$shortcodes['showname'] = $esc->showname? $esc->showname : $esc->username;
					}
					
					$shortcodes['url'] ='https://www.'.Cubix_Application::getById()->host.'/escort/'.$esc->showname.'-'.$esc->id.'#newsletterPopupTurnOff';
					Cubix_Email::sendTemplate( $email_template, $esc->email, $shortcodes );
				}
				////Sending email notification	END
			}
			
			die;
		}
		elseif($status == Model_Verifications_Requests::BLUR_VERIFY){
			$model->setStatus($escort_id, $request_id, $status, null, null, null, 0 , $ids);
				
			$this->view->showname = $showname;
			$this->view->ids = array(2);
			/* send email */				
			if ( Cubix_Application::getId() == 1 ) {
				$body = $this->view->render('email-templates/verification-ef.phtml');
			}
			if ($body)
				Cubix_Email::send($esc->email, $subject, $body, true);
			die;
		}
		else
		{
			$validator = new Cubix_Validator();
			if ( ! $reason_ids ) {
				$validator->setError('reason_1', 'Required');
				$validator->setError('reason_2', 'Required');
				$validator->setError('reason_3', 'Required');
				$validator->setError('reason_4', 'Required');
				
				if (Cubix_Application::getId() == APP_EF) 
				{
					$validator->setError('reason_6', 'Required');
					$validator->setError('reason_7', 'Required');
					$validator->setError('reason_9', 'Required');
				}
				
				if (Cubix_Application::getId() == APP_6A) 
				{
					$validator->setError('reason_8', 'Required');
				}
			}
			
			else if(strpos($reason_ids, '4') !== false && strlen($free_text) == 0){
				$validator->setError('reason_4', 'Required');
			}
			
			if ( $validator->isValid() ) {
				$need_video = 0;
				if(strpos($reason_ids, '10') !== false){
					$need_video = 1;
				}
				$model->setStatus($escort_id, $request_id, $status, null, $reason_ids, $free_text, $need_video);
				
				/* send email */
				$ids = explode(',', $reason_ids);

				$this->view->showname = $showname;
				$this->view->ids = $ids;
				$this->view->free_text = $this->_getParam('free_text');
				
				if ( Cubix_Application::getId() == 1 ) {
					$body = $this->view->render('email-templates/verification-ef.phtml');
				}
				elseif ( Cubix_Application::getId() == 2 ) 
				{
					$body = $this->view->render('email-templates/verification-6a.phtml');
				}
				elseif ( Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT )
				{
					$body = $this->view->render('email-templates/verification-and6.phtml');
				}
				
				if ($body)
					Cubix_Email::send($esc->email, $subject, $body, true);
				
				
				############################ dev mail template before migration into current domain
                if( Cubix_Application::getId() == APP_ED ){
					$m_d = new Model_System_Dictionary();
					if ($reason_ids)
					{
						$r_ids = explode(',', $reason_ids);
						$reasons_text = '';
						
						foreach ($r_ids as $r_id){
							$reasons_text .= $m_d->getById( 'verify_reject_reason_' . $r_id )->{'value_en'}.'<br>';
						}
						$reasons_text .= $reasons_text . '<br>'. $free_text;
					}
					if(is_numeric($esc->agency_id)){
						$email_template = '100_verification_rejected_agency_v1';
						$shortcodes['escort_showname'] = $esc->showname;
						$shortcodes['agency'] = $esc->agency_name;
					} else{
						$email_template = '100_verification_rejected_v1';
						$shortcodes['showname'] = $esc->showname;
					}
					$shortcodes['url'] = 'https://www.'.Cubix_Application::getById()->host.'/escort/'.$esc->showname.'-'.$esc->id;
					$shortcodes['reason'] = $reasons_text;
					Cubix_Email::sendTemplate( $email_template, $esc->email, $shortcodes );

				}elseif( in_array(Cubix_Application::getId(),array(APP_EG_CO_UK, APP_6B))){
					$m_d = new Model_System_Dictionary();

					if ($reason_ids)
					{

						$r_ids = explode(',', $reason_ids);
						$reasons_text = '';
						
						foreach ($r_ids as $r_id){
							if($r_id == 4){
								$reasons_text .= $reasons_text . '<br>'. $free_text;
							}else{
								$reasons_text .= $m_d->getById( 'verify_reject_reason_' . $r_id )->{'value_en'}.'<br>';
							}	
						}
					}

					if(is_numeric($esc->agency_id)){
						$email_template = 'agency_photo_verification_rejected';
						$shortcodes['escort_showname'] = $esc->showname;
						$shortcodes['agency'] = $esc->agency_name;
					} else{
						$email_template = 'escort_photo_verification_rejected';
						$shortcodes['username'] = ($esc->showname ? $esc->showname : $esc->username);

					}

					$shortcodes['url'] = 'https://www.'.Cubix_Application::getById()->host.'/account/signin?referrer=private/verify/idcard/';
					$shortcodes['reason'] = $reasons_text;

					Cubix_Email::sendTemplate( $email_template, $esc->email, $shortcodes );

				}
			}

			die(json_encode($validator->getStatus()));
		}
	}
	
	public function photosAction()
	{
		$request_id = $this->_getParam('request_id');
		
		$request = $this->model->get($request_id);
		if ( ! $this->user->hasAccessToEscort($request->escort_id) ) {
			die('Permission denied');
		}
		$this->view->idcard_photos = $request->getPhotos();
		
		$m_escorts = new Model_Escorts();
		$this->view->escort = $escort = $m_escorts->get($request->escort_id);
		$this->view->photos = $escort->getPhotos();

        $m_escortsV2 = new Model_EscortsV2();
        $escortV2 = $m_escortsV2->get($request->escort_id);
        $this->view->photo_history = $escortV2->getPhotoHistory();

		$m_escort = new Model_EscortsV2();
		$this->view->susp_data = $m_escort->getSuspiciousData($request->escort_id);
		
		if(Cubix_Application::getId() == APP_EF){
			$bu_user = Zend_Auth::getInstance()->getIdentity();
			$age_model = new Model_Verifications_AgeVerification();
			$this->view->user_type = $bu_user->type;
			$age_info = $age_model->getByIdcardId($request_id);
			if($age_info){
				$age_info->born_date = isset($age_info->born_date) ? date("j M Y", strtotime($age_info->born_date)) : null;
				$age_info->day = isset($age_info->born_date) ? date("j", strtotime($age_info->born_date)) : null;
				$age_info->month = isset($age_info->born_date) ? date("n", strtotime($age_info->born_date)) : null;
				$age_info->year = isset($age_info->born_date) ? date("Y", strtotime($age_info->born_date)) : null;
			}
			$this->view->age_info = $age_info;
			$conf = Zend_Registry::get('images_config');
			$this->view->sid = urlencode(Cubix_Utils::crypter( $_COOKIE['PHPSESSID'], $conf['secret_key'], $conf['secret_iv'], 'e'));
			
		}
	}

	public function reasonAction()
	{
		
	}
	
	public function rotateAction()
	{
		$degree = intval($this->_getParam('degree'));
		$degree = $degree == 90 ? 90 : -90;
		$escort_id = $this->_getParam('escort_id');
		$hash = $this->_getParam('hash');
		$ext = $this->_getParam('ext');

		try {
			$conf = Zend_Registry::get('images_config');
			
			get_headers($conf['remote']['url'] . "/get_image.php?a=rotate&sub_dir=verify&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash . "&ext=". $ext . "&d=". $degree);
			get_headers($conf['remote']['url'] . "/get_image.php?a=clear_cache&sub_dir=verify&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash);
			
			$catalog = $escort_id;
			
			$parts = array();

			if ( strlen($catalog) > 2 ) {
				$parts[] = substr($catalog, 0, 2);
				$parts[] = substr($catalog, 2);
			}
			else {
				$parts[] = '_';
				$parts[] = $catalog;
			}
			
			$parts[] = "verify";
			$catalog = implode('/', $parts);
			
			get_headers($conf['remote']['url'] . "/" . Cubix_Application::getById()->host . "/" . $catalog . "/" . $hash . "_t100p.".$ext);

		}
		catch ( Exception $e ) {
			die(json_encode(array('error' => 'An error occured')));
		}

		die(json_encode(array('success' => true)));
	
	}
	
	public function rejectReasonsAction()
	{
		$escort_id = $this->_getParam('escort_id');
		if ( ! $this->user->hasAccessToEscort($escort_id) ) {
			die('Permission denied');
		}
		$m_escorts = new Model_Escorts();
		$age_ver_model = new Model_Verifications_AgeVerification();
		
		$this->view->escort = $escort = $m_escorts->get($escort_id);
		$this->view->reasons = $age_ver_model->getRejectReasons($escort_id);
	}		
}
