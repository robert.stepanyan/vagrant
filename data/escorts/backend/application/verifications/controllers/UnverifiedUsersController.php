<?php

class Verifications_UnverifiedUsersController extends Zend_Controller_Action {
	
	const PACKAGE_STATUS_ACTIVE   = 2;

	public function init()
	{
		$this->model = new Model_Verifications_UnverifiedUsers();
		$this->view->config = Zend_Registry::get('videos_config');
		$this->view->host =  Cubix_Application::getById(Cubix_Application::getId())->host;
		$this->user = Zend_Auth::getInstance()->getIdentity();
	}
	
	public function indexAction() 
	{
		
	}
		
	public function dataAction()
	{
		$page = $this->_getParam('page');
		$per_page = $this->_getParam('per_page');
		$sort_field = $this->_getParam('sort_field');
		$sort_dir = $this->_getParam('sort_dir');

		$req = $this->_request;
		
		$filter = array(			
			'u.id' => $req->u_id,
			'u.email' => $req->u_email,
			'u.user_type' => $req->u_type,
			'u.date_registered' => $req->reg_date,
			'u.user_type' => $req->user_type,
			'uu.status' => $req->mail_status	
		);

		echo json_encode($this->model->getAll($page, $per_page, $sort_field, $sort_dir, $filter));
		die;
	}
		
	public function resendAction() 
	{	
		$action_type = $this->_getParam('allusers');
		$users_count = $this->_getParam('users-count', 250);

		if($action_type == 1){
			
			$filter['uu.status'] = 'not_notified';
			$users = $this->model->getAll(1, $users_count, 'id', 'desc',  $filter);
			
			foreach ($users['data']as $user) {
				$reg_type = $this->get_reg_type_user_type($user->u_type);
				$status = $this->model->send_mail($user->u_username, $user->u_email, $reg_type, $user->u_activation_hash, $user->u_id, $user->reg_date );
			}
		}else{
			
			$user_type = $this->_getParam('type');
			$u_id  = $this->_getParam('u_id'); // USER ID
			$id  = $this->_getParam('id');   // ESCORT ID, MEMBER ID AGENCY ID
			$email = $this->_getParam('email');
			$username = $this->_getParam('username');
			$activation_hash = $this->_getParam('activation_hash');
			$reg_type = $this->get_reg_type_user_type($user_type);
			$reg_date = $this->_getParam('reg_date');
			
			$status = $this->model->send_mail($username, $email, $reg_type, $activation_hash, $u_id, $reg_date);
		}

		echo json_encode($status);
		die;
	}

	public function syncAction() 
	{	
		// this functon will update db table users_unverified 
		// it will update status of users to 1 in that table if they verified their account
		// works only after clicking on sync button from backend
		echo json_encode($this->model->sync());
		die;
	}
	// 
	public function get_reg_type_user_type($user_type){
		//this is necessary for EGUK CO UK project
		switch ($user_type) {
			case 'member':
				$reg_type = 'signup-freemember';
				break;
			case 'escort':
				$reg_type = 'signup-independent';
				break;
			case 'agency':
				$reg_type = 'signup-agency';
				break;
		}
		return $reg_type;
	}
}
