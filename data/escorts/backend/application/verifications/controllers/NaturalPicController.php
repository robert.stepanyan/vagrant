<?php

class Verifications_NaturalPicController extends Zend_Controller_Action {
	
	public function init()
	{
		$this->model = new Model_Verifications_NaturalPic();
		$this->user = Zend_Auth::getInstance()->getIdentity();
		$this->session = new Zend_Session_Namespace('add_natural_pic');
	}
	
	public function indexAction() 
	{
		
	}
		
	public function dataAction()
	{
		$page = $this->_getParam('page');
		$per_page = $this->_getParam('per_page');
		$sort_field = $this->_getParam('sort_field');
		$sort_dir = $this->_getParam('sort_dir');
		
		$req = $this->_request;
		
		$status = Model_Verifications_NaturalPic::STATUS_PENDING;
		if ( $req->status )
			$status = $req->status;
		else 
			$status = '';
			
					
		$filter = array(			
			'e.showname' => $req->showname,
			'e.id' => $req->escort_id,
			'status' => $status,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to
		);
		
		$count = 0;
		$data = $this->model->getAll($page, $per_page, $sort_field, $sort_dir, $filter, $count);
		
		$m_d = new Model_System_Dictionary();
		
		foreach ( $data as $i => $item ) {			
			switch ($item->status)
			{
				case Model_Verifications_NaturalPic::STATUS_PENDING:
					$data[$i]->status_text = 'PENDING';
					break;
				case Model_Verifications_NaturalPic::STATUS_VERIFIED:
					$data[$i]->status_text = 'VERIFIED';
					break;
				case Model_Verifications_NaturalPic::STATUS_REJECTED:
					$data[$i]->status_text = 'REJECTED';
					break;
			}
			
		}
		
		echo json_encode(array('data' => $data, 'count' => $count));
		die;
	}
	
	public function setStatusAction()
	{
		$status = $this->_getParam('status');
		$escort_id = $this->_getParam('escort_id');
		$request_id = $this->_getParam('request_id');
		$reason_ids = $this->_getParam('reason_ids');
		$free_text = $this->_getParam('free_text');
		
		if ( ! $this->user->hasAccessToEscort($escort_id) ) {
			die('Permission denied');
		}
				
		$m = new Model_EscortsV2();
		$esc = $m->get($escort_id);
		$showname = $esc->showname;
		
		if ( $status == Model_Verifications_NaturalPic::STATUS_VERIFIED || $status == Model_Verifications_NaturalPic::STATUS_REMOVED)
		{
			$this->model->setStatus($escort_id,$request_id, $status);
			
			if ($status == Model_Verifications_NaturalPic::STATUS_VERIFIED )
			{
				Cubix_Email::sendTemplate('natural_pic_approve', $esc->email, array('showname' => $showname));
			}
			else
			{
				Cubix_Email::sendTemplate('natural_pic_removed', $esc->email, array('showname' => $showname));
			}
			die;
		}
		else
		{
			$validator = new Cubix_Validator();
			if ( ! $reason_ids ) {
				$validator->setError('reason_1', 'Required');
				$validator->setError('reason_2', 'Required');
				$validator->setError('reason_3', 'Required');
				$validator->setError('reason_4', 'Required');
				
			}
			
			else if(strpos($reason_ids, '20') !== false && strlen($free_text) == 0){
				$validator->setError('reason_20', 'Required');
			}
			
			if ( $validator->isValid() ) {
				foreach(explode(',',$reason_ids) as $reason_id){
					if($reason_id == 20){
						$reasons[] = $free_text;
					}
					else{
						$reasons[] = Cubix_I18n::translateByLng('en','natural_reject_reason_'.$reason_id); 
					}
					
				}				
				$this->model->setStatus($escort_id,$request_id, $status, implode('<br/><br/>',$reasons));
								
				/* send email */
				$ids = explode(',', $reason_ids);

				$this->view->showname = $showname;
				$this->view->ids = $ids;
				$this->view->free_text = $this->_getParam('free_text');
				
				if ( Cubix_Application::getId() == APP_BL ) {
					$body = $this->view->render('email-templates/natural-pic.phtml');
				}
				elseif ( Cubix_Application::getId() == APP_EF ) 
				{
					$body = $this->view->render('email-templates/natural-pic_ef.phtml');
				}/*
				elseif ( Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT )
				{
					$body = $this->view->render('email-templates/verification-and6.phtml');
				}*/
				$subject = "Natural Pic has been REJECTED";
				
				if ($body)
					Cubix_Email::send($esc->email, $subject, $body, true);
			}

			die(json_encode($validator->getStatus()));
		}
	}
	
	public function photosAction()
	{
		$request_id = $this->_getParam('request_id');
		
		$request = $this->model->get($request_id);
		if ( ! $this->user->hasAccessToEscort($request->escort_id) ) {
			die('Permission denied');
		}
		
		$this->view->np_photo = $request->photo;
		
		$m_escorts = new Model_EscortsV2();
		$this->view->escort = $escort = $m_escorts->get($request->escort_id);
		$this->view->photos = $escort->getPhotos();
		
		
		$m_escort = new Model_EscortsV2();
		$this->view->susp_data = $m_escort->getSuspiciousData($request->escort_id);
		
	}

	public function reasonAction()
	{
		
	}
	
	public function rotateAction()
	{
		$degree = intval($this->_getParam('degree'));
		$degree = $degree == 90 ? 90 : -90;
		$escort_id = $this->_getParam('escort_id');
		$hash = $this->_getParam('hash');
		$ext = $this->_getParam('ext');

		try {
			$conf = Zend_Registry::get('images_config');
			
			get_headers($conf['remote']['url'] . "/get_image.php?a=rotate&sub_dir=verify&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash . "&ext=". $ext . "&d=". $degree);
			get_headers($conf['remote']['url'] . "/get_image.php?a=clear_cache&sub_dir=verify&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash);
			
			$catalog = $escort_id;
			
			$parts = array();

			if ( strlen($catalog) > 2 ) {
				$parts[] = substr($catalog, 0, 2);
				$parts[] = substr($catalog, 2);
			}
			else {
				$parts[] = '_';
				$parts[] = $catalog;
			}
			
			$parts[] = "verify";
			$catalog = implode('/', $parts);
			
			get_headers($conf['remote']['url'] . "/" . Cubix_Application::getById()->host . "/" . $catalog . "/" . $hash . "_t100p.".$ext);

		}
		catch ( Exception $e ) {
			die(json_encode(array('error' => 'An error occured')));
		}

		die(json_encode(array('success' => true)));
	
	}
	
	public function addAction()
	{
		$this->view->layout()->disableLayout();
		
		if ( $this->_request->isPost() ) {
			
			$escort_id = intval($this->_getParam('escort_id'));			
			$validator = new Cubix_Validator();
			
			if ( ! $escort_id) {
				$validator->setError('escort_id', 'Required');
			}
			elseif(!$this->session->photos){
				$validator->setError('escort_id', 'Upload Natural Pic');
			}			
			if ( $validator->isValid() ) {
				$bu_user = Zend_Auth::getInstance()->getIdentity();
				$pic_data = reset($this->session->photos);
				$data = array();
				$data['escort_id'] = $escort_id;
				$data['creation_date'] = new Zend_Db_Expr('NOW()');
				$data['status'] = Model_Verifications_NaturalPic::STATUS_VERIFIED ;
				$data['admin_id'] = $bu_user->id;
				$data['hash'] = $pic_data['hash'];
				$data['ext'] = $pic_data['ext'];
				
				$this->model->add($data);
				$this->session->unsetAll();
			}
				die(json_encode($validator->getStatus()));
		} else {
			$this->session->unsetAll();
		}
	}
	
	public function uploadHtml5Action()
	{
		$tmp_dir = sys_get_temp_dir();
		$config = Zend_Registry::get('images_config');
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$escort_id = intval($this->_getParam('escort_id'));
		
		try {
			
			$response = array(
				'id'	=> $_SERVER['HTTP_X_FILE_ID'],
				'name'	=> $_SERVER['HTTP_X_FILE_NAME'],
				'size'	=> $_SERVER['HTTP_X_FILE_SIZE'],
				'error'	=> 0,
				'msg' => '',
				'finish'	=> FALSE
			);
			
			$ext = strtolower(@end(explode('.', $response['name'])));
			$file = $tmp_dir . DIRECTORY_SEPARATOR . $response['name'];
			file_put_contents($file, file_get_contents('php://input'));
			
			if ( !$escort_id ) {
				throw new Exception("Please select escort id", Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
			}
			
			if (!in_array( $ext , $config['allowedExts'])){
				throw new Exception(Cubix_I18n::translate('sys_error_upload_img_allowed_extensions', array('extensions' => implode(', ', $config['allowedExts']))), Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
			}	
			
			$images = new Cubix_Images();
			$photo = $images->save($file, $escort_id . '/natural-pics', Cubix_Application::getId(), $ext);
			//$photo = array('hash' => "1111111111", 'ext'=> $ext);
			
			if ( ! isset($photo['hash']) ) {
				throw new Exception("Photo upload failed. Please try again!");
			}
			
			$this->session->photos[$response['id']] = $photo;
			
			$image = new Cubix_Images_Entry($photo);
			$image->setSize('t100p');
			$image->setCatalogId($escort_id . '/natural-pics');
			$response['photo_url'] = $images->getUrl($image);
			$response['finish'] = TRUE;
		} catch (Exception $e) {
			$response['error']	= 1;
			$response['msg']	= $e->getMessage();
		}		
		
		echo json_encode($response);die;
	}
	
	public function ajaxRemovePhotoAction()
	{
		$image_id = $this->_request->image_id;
				
		$status = array('status' => 'error');
		if ( isset($this->session->photos[$image_id]) ) {
			unset($this->session->photos[$image_id]);
			$status = array('status' => 'success');
		}
		
		die(json_encode($status));
	}
}
