<?php 
$LNG['ladies'] = "Ladies";
$LNG['latest_join'] = "Latest Join";

$LNG['city_zones_in'] = "Zone di"; 
$LNG['latin_only'] = "Solo latina"; 
$LNG['width_comments'] = "Width Comments"; 
$LNG['100_fake_free_review'] = "100% reale"; 
$LNG['100_fake_free_review_desc'] = "Una recensione è verificata per 100%  se l'agenzia/ragazza conferma via sms/e-mail che l'incontro è avvenuto, così tutte e due le persone sono d'accordo sull'incontro e la recensione è priva di truffa per 100% !"; 
$LNG['24_7'] = "24/7"; 
$LNG['PROD_additional_city'] = "Additional city"; 
$LNG['PROD_city_premium_spot'] = "City premium spot"; 
$LNG['PROD_girl_of_the_month'] = "Girl of the month"; 
$LNG['PROD_international_directory'] = "International directory"; 
$LNG['PROD_main_premium_spot'] = "Main premium spot"; 
$LNG['PROD_national_listing'] = "National listing"; 
$LNG['PROD_no_reviews'] = "No reviews"; 
$LNG['PROD_search'] = "Search"; 
$LNG['PROD_tour_ability'] = "Tour ability"; 
$LNG['PROD_tour_premium_spot'] = "&quot;On tour&quot; premium spot"; 
$LNG['_all'] = "Tutte"; 
$LNG['about_me'] = "Su di me"; 
$LNG['about_meeting'] = "Sull’incontro"; 
$LNG['account_activated'] = "I tuoi dati sono stati attivati, entra adesso e aggiungi il tuo profilo/ escorts. <a href='".NGE_URL."/login.php$QUERY_STRING'>Click here</a> to login."; 
$LNG['account_blocked_desc'] = "<p>Suo account e stato sospeso per uno dei motivi seguenti:</p> 
<ul> 
<li>insulto di un utente/escort</li> 
<li>discriminazione di un utente/escort</li> 
<li>aver fatto una recensione o un post falso</li> 
<li>istigazione degli utenti contro qualcuno o qualcosa</li> 
<li>promozione ovvia per una agenzia o escort</li> 
</ul> 
<p>Aspettiamo i Suoi commenti o declarazione al: <a href='mailto:".NGE_CONTACT_EMAIL."'>".NGE_CONTACT_EMAIL."</a></p> 
<p>Nota: Per piacere non si registra una altra volta: tutti i Suoi post e recensioni verrano cancellati e blocchiamo il Suo IP.</p>"; 
$LNG['account_not_verified'] = "Il cliente non e stato verificato."; 
$LNG['account_type'] = "Tipo di cliente"; 
$LNG['active'] = "Attiva"; 
$LNG['ad_reg_confirm'] = "Grazie per la registrazione !<br /> Ti abbiamo mandato una mail con le istruzioni come puoi accedere al tuo account che hai appena creato. Per favore controlla la tua casella di posta elettronica. Se hai qualsiasi problema di confermare il tuo acount, contattaci al <a 
href='mailto:".NGE_CONTACT_EMAIL."'>".NGE_CONTACT_EMAIL."</a>. 
<br/><br/>".NGE_SITE_TITLE." team."; 
$LNG['add'] = "Aggiungi"; 
$LNG['add_escort'] = "Aggiungi profilo escort"; 
$LNG['add_escort_profile'] = "Aggiungi il profilo dell’escort"; 
$LNG['add_profile_now'] = "Aggiungi il tuo profile con foto."; 
$LNG['add_review'] = "Aggiungi una recensione"; 
$LNG['add_third_party_girl'] = "Aggiungere una ragazza di un altro sito alla nostra database"; 
$LNG['add_watch'] = "Aggiungere alla lista dei preferiti"; 
$LNG['add_watch_success'] = "<b>%NAME%</b> è stato/a aggiunto/a alla tua lista dei preferiti con successo. <a href='javascript:history.back()'>Clicca qui per tornare indietro.</a>"; 
$LNG['additional'] = "Supplementare"; 
$LNG['address'] = "Via"; 
$LNG['age'] = "Età"; 
$LNG['age_between'] = "età tra"; 
$LNG['agencies_match'] = "Agenzie corrispondenti alle criterie"; 
$LNG['agency'] = "Agenzia"; 
$LNG['agency_city_tour_not_allowed'] = "Le ragazze in periodo di prova non sono in lista perchè non possono avere la funzione city tour. Acquista adesso un Premium spot per abilitare questa funzione. Clicca <a href='mailto:".NGE_CONTACT_EMAIL."'>qui per ordinare</a>."; 
$LNG['agency_data'] = "Dati dell’agenzia"; 
$LNG['agency_denied_review'] = "rifutato"; 
$LNG['agency_denied_review_desc'] = "L'agenzia ha comunicato che non c'è stato cliente con i dati forniti, la recensione può essere falsa, non ti devi fidare. L'agenzia/ragazza non sa se la recensione sia positiva/ negativa quando la conferma!"; 
$LNG['agency_name'] = "Nome dell’agenzia"; 
$LNG['agency_profile'] = "Profilo dell’agenzia"; 
$LNG['agency_without_name'] = "Agenzia senza nome"; 
$LNG['ajaccio'] = "Ajaccio"; 
$LNG['all'] = "Tutti..."; 
$LNG['all_cities'] = "Tutte le città..."; 
$LNG['all_countries'] = "Tutti i paesi ..."; 
$LNG['all_escorts'] = "Tutte le escort"; 
$LNG['all_origins'] = "Tutte le origini..."; 
$LNG['all_reviews'] = "Tutte le recensioni"; 
$LNG['already_registered_on_another_site'] = "Login già esistente. Se hai un account con lo stesso nome su %SITE%, non hai bisogno di registrarti di nuovo, per favore clicca qui e accedi al sito con la stessa login e password che usi su %SITE%."; 
$LNG['already_reviewed'] = "Hai già recensito questa ragazza! Solo una recensione è consentita per una ragazza! Per favore aggiorna la tua recensione <a href='%LINK%'>current review</a> se vuoi!"; 
$LNG['already_voted'] = "Hai gia votato questa ragazza."; 
$LNG['also_provided_services'] = "Ha anche fornito questi servizi"; 
$LNG['anal'] = "Anale"; 
$LNG['are_you_escort'] = "Sei una escort indipendente, agenzia o un Club Privè ?"; 
$LNG['at_her_apartment'] = "A casa sua"; 
$LNG['at_her_hotel'] = "Nel suo albergo"; 
$LNG['at_my_flat_or_house'] = "A casa mia/nel mio appartamento"; 
$LNG['at_my_hotel'] = "Nel mio albergo"; 
$LNG['attention'] = "Attenzione!"; 
$LNG['attitude'] = "Attitudine"; 
$LNG['availability'] = "Disponibilità"; 
$LNG['available_for'] = "Disponibile per"; 
$LNG['back_to_actual_tours'] = 'Back to actual tours'; 
$LNG['basic'] = "Base"; 
$LNG['become_premium_member_and_view_private_photos'] = "<a 
href='%LINK%'>Diventa un utente Premium ADESSO </a> e potrai vedere le foto 
speciali e non censurati di questa ragazza!"; 
$LNG['bio'] = "Bio"; 
$LNG['birth_date'] = "Data di nascita"; 
$LNG['black'] = "Nero"; 
$LNG['blond'] = "Bionda"; 
$LNG['blowjob'] = "Pompino"; 
$LNG['blowjob_with_condom'] = "Pompino coperto"; 
$LNG['blowjob_without_condom'] = "Pompino scoperto"; 
$LNG['blue'] = "Blu"; 
$LNG['bombshellf'] = "Sconvolgente"; 
$LNG['book_email_to_escort'] = "<pre> 
Dear %ESCORT%, 
   a Premium Member of ".NGE_SITE_TITLE." is wishing to meet you on %DATE%. 
This user is claiming the discount of %DISCOUNT%%, which you are offering to our trusted users. 
Details: 
Username: %USERNAME% 
Full name: %FULLNAME% 
Desired date: %DATE% 
Duration: %DURATION% 
Phone: %PHONE% 
Email: %EMAIL% 
Comments: %COMMENTS% 
Security code: %CODE% 
This code is used to verification of the user. The user is required to give you this code upon request, 
if you'd like to verify his discount claiming rights. 
Best Regards, 
<a href=\"".NGE_URL."\">".NGE_SITE_TITLE."</a> 
</pre>"; 
$LNG['book_this_girl'] = "Prenota questa ragazzal"; 
$LNG['booking_desc'] = "Grazie per il tuo interessamento per prenotare  %NAME%. Come utente Premium ricevi uno sconto di %DISCOUNT%% nel prenotare questa ragazza. Per favore usa il modulo di sotto per prenotare la ragazza e chiedere il tuo sconto!"; 
$LNG['booking_mail_subject'] = "%SITE% prenotazione"; 
$LNG['booking_mail_text'] = "<pre> 
Gentile %NAME%, 
hai prenotato %ESCORT_NAME% con successo per il giorno %DATE%. 
La ragazza ti contatterà fra un po’ per mettersi d’accordo sui dettagli della tua data. 
Come Utente Premium dell’ %SITE%, hai diritto di chiedere il tuo sconto  di %DISCOUNT%% 
dal prezzo della ragazza. 
Nel caso di avere qualsiasi complicazione, per favore memorizza questa ID di sicurezza: %VERIFICATION_ID% 
Questa ID può essere usato per verificare la tua iscrizione Premium e ridarti lo sconto. 
Ti auguriamo un fantastico divertimento con la ragazza che hai scelta! 
%SITE% Team 
</pre> 
"; 
$LNG['boys'] = "Uomini"; 
$LNG['boys_trans'] = "Boys/Trans"; 
$LNG['breast'] = "Tette"; 
$LNG['breast_size'] = "Tagliadel seno"; 
$LNG['brown'] = "Bruna"; 
$LNG['bruntal'] = "Bruntál"; 
$LNG['bust'] = "Seno"; 
$LNG['by'] = "di"; 
$LNG['calabria'] = "Calabria"; 
$LNG['can_have_max_cities'] = "Puoi scegliere solo %NUMBER% città"; 
$LNG['cancel'] = "Cancella"; 
$LNG['cant_be_empty'] = "Questo campo non può essere vuoto"; 
$LNG['cant_go_ontour_to_homecity'] = "Le escort non possono fare city tour nella propria città"; 
$LNG['castelfranco'] = "Castelfranco"; 
$LNG['chalons-en-champagne'] = "Chalons-en-Champagne"; 
$LNG['change_all_escorts'] = "Applicare le modifice su tutti i profili escort"; 
$LNG['change_passwd'] = "Cambia password"; 
$LNG['char_desc'] = "tatoo etc."; 
$LNG['characteristics'] = "Caratteristiche"; 
$LNG['chiavari'] = "Chiavari"; 
$LNG['choose'] = "Scegli..."; 
$LNG['choose_availability'] = "Choose availability..."; 
$LNG['choose_another_city'] = "Seleziona un'altra città"; 
$LNG['choose_another_region'] = "Choose another region"; 
$LNG['choose_city'] = "Scegli la città ..."; 
$LNG['choose_country'] = "Scegli il paese..."; 
$LNG['choose_currency'] = "Scegli la valuta ..."; 
$LNG['choose_escort'] = "Scegli escort ..."; 
$LNG['choose_escort_to_promote'] = "Seleziona una escort da promuovere"; 
$LNG['choose_language'] = "Scegli la lingua ..."; 
$LNG['choose_level'] = "Scegli il livello ..."; 
$LNG['choose_ethnic'] = "Choose ethnic..."; 
$LNG['choose_nationality'] = "Seleziona la nazionalità..."; 
$LNG['choose_state'] = "Scegli la regione ..."; 
$LNG['choose_this_user'] = "Seleziona questo utente"; 
$LNG['choose_time'] = "Scegli l’ora ..."; 
$LNG['choose_your_city'] = "Scegli la tua città"; 
$LNG['choose_your_country'] = "Scegli il tuo paese"; 
$LNG['choosen_escort'] = "Scegli la ragazza: %NAME%"; 
$LNG['cities'] = "Città"; 
$LNG['city'] = "Città"; 
$LNG['city_escort_notify'] = "Venire informato via e-mail se un’escort nuova viene aggiunta nella tua citta"; 
$LNG['city_of_meeting'] = "Città dell’incontro"; 
$LNG['claim_your_discount'] = "Chiedi il tuo sconto ORA e prenota questa ragazza"; 
$LNG['comments'] = "Commenti"; 
$LNG['comments_to_her_services'] = "Commenti sul servizio"; 
$LNG['comments_to_her_services_desc'] = "Descrivila da punto di vista sessuale, per esempio servizi speciali (S/M, giochi, etc)."; 
$LNG['company_name'] = "Nome dell’agenzia"; 
$LNG['conf_password'] = "Conferma password"; 
$LNG['confirmation_email_1'] = "Caro %USER%, grazie per la tua registrazione. Il tuo login è il seguente:"; 
$LNG['confirmation_email_2'] = "Tieni queste informazioni in un posto sicuro."; 
$LNG['confirmation_email_subject'] = NGE_SITE_TITLE." registration"; 
$LNG['contact'] = "Contatto"; 
$LNG['contact_her'] = "Contatta direttamente questa ragazza"; 
$LNG['contact_her_desc'] = "Stai contattando  %NAME%. Questa mail arriverà dal dominio  %SITE% e sarà certificata per esprimere la tua attendibilità come Utente Premium dell’ %SITE%"; 
$LNG['contact_her_mail_header'] = "<pre> 
************************************************************************************* 
Questa è una mail certificata dall’utente Premium del  %SITE% 
Questo utente è un’utente fidato della nostra communità e 
paga regolarmente la tassa dell’iscrizione. 
*************************************************************************************</pre>"; 
$LNG['contact_her_mail_subject'] = "Messaggio da %SITE% Utente Premium"; 
$LNG['contact_info'] = "Informazioni per contattarci"; 
$LNG['contact_text'] = " 
Escortforum pubblicità<BR /><BR /> 
Noi come la communità di escort più grande in ".$LNG['country_'.MAIN_COUNTRY]." Vi offriamo spazio di pubblicità sul nostro sito. 
Limitiamo il numero dei siti pubblicizzati a 10, che significa un ottima frequenza di click per ogni banner.<BR /> 
<BR /> 
Alcune statistiche:<BR /> 
<BR /> 
Hits: 3 millioni in media, e in continuo incremento <BR /> 
Visitatori: in media piu di 25.000 al giorno <BR /> 
Membri registrati: 15000 attualmente e in continua crescita<BR /> 
<BR /> 
<BR /> 
Se volete provare e discutere sugli dettagli ora, contattateci. Vi rispondiamo immediatamente.<BR /> 
<BR /> 
<a href='mailto:".NGE_CONTACT_EMAIL."'>".NGE_CONTACT_EMAIL."</a> 
Tel.: 0036 1203 1808 (10:00 - 20:00) 
"; 
$LNG['contact_us'] = "Contattaci / Ads"; 
$LNG['continue'] = "Continua"; 
$LNG['conversation'] = "Convesazione"; 
$LNG['countries'] = "Paesi"; 
$LNG['country'] = "Paese"; 
$LNG['country_tw'] = "Taiwan"; 
$LNG['couples'] = "Coppie"; 
$LNG['created'] = "Creato"; 
$LNG['cumshot'] = "Sborrata in bocca"; 
$LNG['cumshot_on_body'] = "Sborrata sul corpo"; 
$LNG['cumshot_on_face'] = "Sborrata sulla facia"; 
$LNG['curr_password'] = "Password attuale"; 
$LNG['curr_password_empty_err'] = "Devi fornire la tua password"; 
$LNG['currency'] = "Valuta"; 
$LNG['currently_listed_sites'] = "Attualmente abbiamo questa lista dei siti. Per favore seleziona un sito."; 
$LNG['date'] = "Data"; 
$LNG['day'] = "giorno"; 
$LNG['day_of_meeting'] = "Data dell’incontro"; 
$LNG['day_phone'] = "Telefono"; 
$LNG['days'] = "giorni"; 
$LNG['default_city'] = "La citta selezionata appare dopo il log-in."; 
$LNG['delete_rates'] = "Cancella tutti i prezzi"; 
$LNG['delete_sel_pcis'] = "Cancella le immagini selezionate"; 
$LNG['delete_selected_reviews'] = "Cancella le recensioni selezionate"; 
$LNG['delete_selected_tour'] = "Cancella tour selezionato"; 
$LNG['desired_date'] = "Data scelta"; 
$LNG['discount'] = "Sconto"; 
$LNG['discount_code'] = "Codice sconto"; 
$LNG['discount_code_txt'] = "Per richiedere lo sconto del 5%, digita il codice sconto se l'hai ricevuto dai nostri partner."; 
$LNG['discount_desc'] = "In questa pagina ti presentiamo tutte le ragazze che offrono uno  <b>sconto</b> per gli Utenti Premium."; 
$LNG['discounted_girls'] = "Ragazze con Sconto"; 
$LNG['dont_forget_variable_symbol'] = " 
    <p>Per favore NON DIMENTICARE di includere il codice identificativo: <big>%ESCORT_ID%</big>.<br /> 
    Senza questo non possiamo rintracciare il vostro pagamento.<br /> 
    Grazie mille.</p> 
"; 
$LNG['dont_know'] = "Non lo so"; 
$LNG['dress_size'] = "Taglia del vestito"; 
$LNG['duration'] = "Durata"; 
$LNG['easy_to_get_appointment'] = "Facile fissare un appuntamento"; 
$LNG['edit'] = "Modifica"; 
$LNG['edit_escort'] = "Cambia il profilo escort"; 
$LNG['edit_gallery'] = "Cambia la galleria"; 
$LNG['edit_languages'] = "Cambia le lingue"; 
$LNG['edit_locations'] = "Cambia le città dove lavori"; 
$LNG['edit_private_photos'] = "Modificare le foto private"; 
$LNG['edit_private_photos_desc'] = "Per favore carica qui le foto senza censura o glamour (foto speciali che non vorresti far vedere ad un vasto pubblico), che solo gli utenti paganti potranno visualizzare. In questo modo puoi essere sicura che le tue foto più personali saranno viste solo da &quot;clienti di classe&quot;, che hanno espresso il loro interessamento pagando la quota dell’iscrizione Premium."; 
$LNG['edit_profile'] = "Aggiorna il tuo profilo"; 
$LNG['edit_rates'] = "Cambia i prezzi"; 
$LNG['edit_review'] = "Aggiorni una recensione"; 
$LNG['edit_settings'] = "Aggiorna le opzioni"; 
$LNG['edit_units_in'] = "Modifica units in"; 
$LNG['email'] = "E-mail"; 
$LNG['email_domain_blocked_err'] = "Spiacenti, %DOMAIN% al momento non accetta e-mail da noi. Vi invitiamo ad usare un indirizzo e-mail differente oppure a contattare %DOMAIN% per poter accettare la nostra corrispondenza."; 
$LNG['email_lng'] = "La lingua delle mail"; 
$LNG['email_not_registered'] = "Questo indirizzo e-mail non é registrato al nostro sito."; 
$LNG['email_sig'] = NGE_SITE_TITLE." team."; 
$LNG['empty_login_err'] = "Il login non può essere vuoto"; 
$LNG['empty_passwd_err'] = "La password non può essere vuota"; 
$LNG['empty_watchlist'] = "Non hai nessun nome nella tua lista dei preferiti."; 
$LNG['english'] = "English"; 
$LNG['enthusiastic'] = "Enthusiastica"; 
$LNG['error'] = "Error"; 
$LNG['error_adding_watch'] = "E’ successo un errore mentre aggiungevi/rimuovevi un articolo alla/dalla tua lista. Si prega di contattare l’amministatore del sito."; 
$LNG['esc_mb_chng_subject'] = "Variazione dati".NGE_SITE_TITLE; 
$LNG['esc_mb_chng_to_free_email'] = "<pre> Caro/a %LOGIN%, 
l’iscrizione della tua escort con il nome %SHOWNAME% è stata cambiata in \"gratuita\". 
Hai perso i vantaggi dei membri iscritti a pagamento. 
Fai l’upgrade adesso e potrai: 
- partecipazione alla rotazione delle ragazze nella main page, significa che non rimarrai mai fermo nelle ultime pagine del sito dove nessun utente può vederti. Tutti i profili a pagamento, vengono ruotati ogni ora, quindi tutti hanno la stessa possibilità di apparire nella prima pagina di una particolare città o nella main page; 
- potrai partecipare al concorso \�?Ragazza del mese\�? 
- avrai la possibilità di partecipare alla lista delle ragazze TOP10 nella sezione recensioni. 
<!-- Fai l’UPGRADE ADESSO e prendi la speciale offerta: 
- un annuncio il prossimo mese nella rivista Redmag magazine in ".$LNG['country_'.MAIN_COUNTRY].".--> 
Per fare l’upgrade puoi rispondere a questa mail, oppure accedere al sito di ".NGE_SITE_TITLE." con il tuo account e cliccare \" FARE L’UPGRADE ADESSO!\". 
".NGE_SITE_TITLE." Team 
</pre> 
"; 
$LNG['esc_mb_chng_to_normal_email'] = "<pre>Caro/a %LOGIN%, 
l’iscrizione della tua escort con il nome %SHOWNAME% è stata upgradata a \"normale\". 
La nuova data di scadenza è: %EXPIRE% 
Da adesso il tuo profilo ha questi vantaggi: 
- partecipazione alla rotazione delle ragazze nella main page, significa che non rimarrai mai fermo nelle ultime pagine del sito dove nessun utente può vederti. Tutti i profili a pagamento, vengono ruotati ogni ora, quindi tutti hanno la stessa possibilità di apparire nella prima pagina di una particolare città o nella main page; 
- potrai partecipare al concorso \�?Ragazza del mese\�? 
- avrai la possibilità di partecipare alla lista delle ragazze TOP10 nella sezione recensioni. 
<!—In più, il tuo profilo verrà pubblicato nella rivista mensile <a target='_blank' href='http://www.redmag.it'>Redmag</a> magazine in ".$LNG['country_'.MAIN_COUNTRY].".--> 
".NGE_SITE_TITLE." Team 
</pre> 
"; 
$LNG['esc_mb_chng_to_premium_email'] = "<pre>Caro/a %LOGIN%, 
l’iscrizione della tua escort con il nome %SHOWNAME% è stata upgradata a \"premium\". 
La nuova data di scadenza è: %EXPIRE% 
Da adesso il tuo profilo ha questi vantaggi: 
- UNDER CONSTRUCTION 
<!—In più, il tuo profilo verrà pubblicato nella rivista mensile <a target='_blank' href='http://www.redmag.it'>Redmag</a> magazine in ".$LNG['country_'.MAIN_COUNTRY].".--> 
".NGE_SITE_TITLE." Team 
</pre> 
"; 
$LNG['esc_mb_chng_to_trial_email'] = "<pre> Caro/a %LOGIN%, 
l’iscrizione della tua escort con il nome %SHOWNAME% è stata upgradata a \"prova\". 
La nuova data di scadenza è: %EXPIRE% 
Il tuo stato di utenza è stato approvato in account di ".NGE_SITE_TITLE.". 
In questo periodo di prova avrai gli stessi privilegi degli utenti iscritti a pagamento: 
- partecipazione alla rotazione delle ragazze nella main page, significa che non rimarrai mai fermo nelle ultime pagine del sito dove nessun utente può vederti. Tutti i profili a pagamento, vengono ruotati ogni ora, quindi tutti hanno la stessa possibilità di apparire nella prima pagina di una particolare città o nella main page; 
- potrai partecipare al concorso \�?Ragazza del mese\�? 
- avrai la possibilità di partecipare alla lista delle ragazze TOP10 nella sezione recensioni. 
Dopo il periodo di prova, dovrai fare l’upgrade ad una iscrizione a pagamento, altrimenti perderai i privilegi e il tuo profilo apparirà nell’ultima pagina. 
<!—Fai l’UPGRADE ADESSO e prendi la speciale offerta: 
- un annuncio il prossimo mese nella rivista Redmag magazine in ".$LNG['country_'.MAIN_COUNTRY].".--> 
Per fare l’upgrade puoi rispondere a questa mail, oppure accedere al sito di ".NGE_SITE_TITLE." con il tuo account e cliccare \" FARE L’UPGRADE ADESSO!\". 
".NGE_SITE_TITLE." Team 
</pre> 
"; 
$LNG['esc_mb_chng_to_vip_email'] = "<pre> Caro/a %LOGIN%, 
l’iscrizione della tua escort con il nome %SHOWNAME% è stata upgradata a \"VIP\". 
Questo profilo non scadrà mai e avrà gli stessi vantaggi degli iscritti a pagamento. 
".NGE_SITE_TITLE." Team 
</pre> 
"; 
$LNG['esc_mb_memb_expired_email'] = "<pre> Caro/a %LOGIN%, 
l’iscrizione della tua escort con il nome %SHOWNAME% è scaduta. 
Hai perso tutti i vantaggi e il tuo profilo è stato spostato nella turnazione gratuita. 
Se vuoi rinnovare il tuo account, contattaci all’indirizzo e-mail ".NGE_CONTACT_EMAIL.". 
".NGE_SITE_TITLE." Team 
</pre> 
"; 
$LNG['esc_mb_memb_expired_email_subj'] = "Iscrizione scaduta"; 
$LNG['esc_mb_memb_expires_soon_email'] = "<pre> Caro/a %LOGIN%, 
l’iscrizione della tua escort con il nome %SHOWNAME%  scadrà tra %DAYS% giorni, 
non dimenticare di rinnovarlo. Per farlo contattaci all’indirizzo e-mail ".NGE_CONTACT_EMAIL.". 
".NGE_SITE_TITLE." Team 
</pre> 
"; 
$LNG['esc_mb_memb_expires_soon_email_subj'] = "L’iscrizione sta per scadere"; 
$LNG['esc_to_upg'] = "Il profilo escort da upgradare"; 
$LNG['esc_upg_promo'] = " 
<ul> 
<li>parteciperai alla rotazione delle ragazze nella home page, 
quindi non rimarrai mai nelle ultime pagine dove gli utenti non potranno vederti. Tutti i profili a pagamento ruotano ogni ora, così tutti coloro che sono iscritti hanno la stessa possibilità di apparire nella prima pagina di una particolare città o anche nella home page di ".NGE_SITE_TITLE.".</li> 
<li>Potrai partecipare al concorso \"Ragazza del mese,\"</li> 
<li>e avrai la possibilità di essere inclusa nella TOP10 della recensione ragazze.</li> 
</ul>"; 
$LNG['esc_upg_promo_txt'] = "<ahref='/private/upgrade_escort.php$QUERY_STRING'>Fai l’upgrade ADESSO</a><!—e prendi la speciale offerta: 
<p style='color:red'>Un annuncio nella prossima edizione del mensile <a target='_blank' href='http://www.redmag.it'>Redmag</a> magazine in Italia</p>-->"; 
$LNG['esc_upgrade_payment_details'] = " 
    Grazie per esserti interessato all’acquisto su ".NGE_SITE_TITLE.".<br /> 
    Ti preghiamo di versare %AMOUNT% EUR nel nostro conto e il tuo profilo verrà upgradato alla ricezione della somma.<br /> 
    <p><strong>Dettagli bancari:</strong><br /> 
    SMD MEDIA:<br /> 
    Conto corrente n. 6152293433/25<br /> 
    IBAN: IT42 P030 6910 9106 1522 9343 325<br /> 
    SWIFT: BCITIT333P0<br /> 
    Banca Intesa - filiale 2390<br /> 
    Via G. Rubini n. 6, 22100 Como</p> 
Se ci dovessero essere problemi o per eventuali domande, non esitare a contattarci al nostro indirizzo e-mail <a href='mailto:".NGE_CONTACT_EMAIL."'>".NGE_CONTACT_EMAIL."</a>. 
"; 
$LNG['escort'] = "Escort"; 
$LNG['escort_active'] = "Il tuo profilo escort è attivo. <a href='%LINK%'>Clicca qui per disattivarlo</a>."; 
$LNG['escort_added_continue_review'] = "Escort aggiunta con successo. <a href='%LINK%'>Clicca qui</a> per passare a recensire."; 
$LNG['escort_city_tour_not_allowed'] = "Non puoi aggiungere un Tour durante il periodo di prova. Devi acquistare un Premium Spot per poter utilizzare questa funzione. Clicca <a href='mailto:".NGE_CONTACT_EMAIL."'>qui per ordinare</a>."; 
$LNG['escort_data_form_text'] = "Riempi solamente con le informazioni che vuoi  publicare sul sito."; 
$LNG['escort_inactive'] = "Questo profilo attualmente non è attivo!"; 
$LNG['escort_modification_disabled'] = "At this moment we are performing scheduled maintenance. No escort modifications are accepted. Please come back later. We are sorry for the inconvenience."; 
$LNG['escort_name_active'] = "Il profilo escort %ESCORT% è attivo. <a href='%LINK%'>Clicca qui per disattivarlo</a>."; 
$LNG['escort_name_not_active'] = "Il profilo escort %ESCORT% non è attivo. <a href='%LINK%'>Clicca qui per attivarlo</a>."; 
$LNG['escort_name_waiting_for_approval'] = "L’escort %ESCORT% sta aspettando l’approvazione dall’amministratore del sito internet"; 
$LNG['escort_not_active'] = "Il tuo profilo escort non è attivo. <a href='%LINK%'> Clicca qui per attivarlo</a>."; 
$LNG['escort_not_found_use_search'] = "Escort %NAME% non è trovata, per favore usa “Cerca�? per specificare una ragazza."; 
$LNG['escort_not_in_db_add_her'] = "Se la ragazza non è nella nostra database, per favore <a href='%LINK%'>aggiungila adesso</a>."; 
$LNG['escort_profile_added'] = "Il profilo escort è stato aggiunto con successo al nostro database!<BR /> Per favore 
<a href='/private/edit_gallery.php$QUERY_STRING'>Carica le foto per il tuo profilo.</a>"; 
$LNG['escort_profile_updated'] = "Il profilo è stato aggiornato con successo! <BR /> 
<a href='/private/index.php$QUERY_STRING'>Torna indietro</a>"; 
$LNG['escort_waiting_for_approval'] = "Il tuo profilo escort sta aspettando l’approvazione dall’amministratore del sito internet."; 
$LNG['escorts'] = "Accompagnatrici"; 
$LNG['escorts_in_city'] = "Accompagnatrici a %CITY%"; 
$LNG['escorts_match'] = "Escort corrispondenti alle criterie"; 
$LNG['evening_phone'] = "Telefono alt."; 
$LNG['expires'] = "Scadenza"; 
$LNG['eye_color'] = "Colore degli occhi"; 
$LNG['eyes'] = "Occhi"; 
$LNG['fake_free_review_confirm_mail_01'] = " 
    <p>Ciao %SHOWNAME%,<br /> 
un untente di   ".NGE_SITE_TITLE." ha aggiunto una recensione su di te. 
Per evitare le recensioni false, ti chiediamo di verificare se hai incontrato questo cliente e confermare o rifiutare le informazioni sottoscritte. Rispondi con un semplice “Yes�? per confermare la recensione,  “No�? per  rifiutarla. </p> 
    <p>I dettagli del cliente:</p> 
"; 
$LNG['fake_free_review_confirm_mail_02'] = " 
    <p>Adesso ti chiediamo di rispondere con Yes o No (non aggiungere altro o modificare le risposte, altrimenti il sistema non riconoscerà la tua risposta). p> 
    <p>Il Tuo ".NGE_SITE_TITLE." team</p> 
"; 
$LNG['fake_free_review_confirm_mail_subj'] = NGE_SITE_TITLE." Conferma per recensione Fake Free"; 
$LNG['fake_photo'] = "Foto false!"; 
$LNG['fax'] = "Numero di Fax"; 
$LNG['female'] = "Donna"; 
$LNG['ff_guests'] = "<div class='big center'>Mancano solo %DAYS% giorni all'estrazione!</div><a href='%LINK%' rel='nofollow'>Iscriviti ADESSO </a> come Utente Premium - e vinci la possibilità di scopare <a href='%LINK%' rel='nofollow'>la tua ragazza preferita GRATUITAMENTE!</a> *"; 
$LNG['ff_guests_today'] = "<div class='big center'>OGGI</div>annunceremo il vincitore del mese della ´Free Fuck Lottery´! Forse sei tu? Prendi l'occasione e <a href='%LINK%' rel='nofollow'>iscriviti ORA</a> come Utente Premium - per partecipare all'estrazione! *"; 
$LNG['ff_nonpaid'] = "<div class='big center'>Mancano solo %DAYS% giorni all'estrazione!</div><a href='%LINK%' rel='nofollow'>Passa al Premium membership ADESSO</a> - e vinci la possibilità di scopare <a href='%LINK%' rel='nofollow'>la tua ragazza preferita GRATUITAMENTE!</a> *"; 
$LNG['ff_nonpaid_today'] = "<div class='big center'>OGGI</div>annunceremo il vincitore della ´Free Fuck Lottery´ del mese! Forse sei tu? Prendi l'occasione e <a href='%LINK%' rel='nofollow'>passa al Premium membership ORA</a> per essere nell'estrazione! *"; 
$LNG['ff_paid'] = " <div class='big center'>Mancano solo %DAYS% giorni all'estrazione!</div>"; 
$LNG['ff_paid_today'] = " <div class='big center'>OGGI</div>annunceremo il vincitore della ´Free Fuck Lottery´ del mese! Forse sei tu?"; 
$LNG['file_uploaded'] = "La foto è stata caricata."; 
$LNG['finish_reg'] = "Completa la registrazione"; 
$LNG['first_name'] = "Nome"; 
$LNG['fluent'] = "Fluente"; 
$LNG['forgot_email_1'] = "Caro %USER%, hai richiesto di inviare la tua password con il form �?dimenticato password’ sul %SITE%."; 
$LNG['forgot_email_2'] = "Il tuo login e:"; 
$LNG['forgot_email_subject'] = NGE_SITE_TITLE." richiesta del codice d’accesso"; 
$LNG['forgot_passwd'] = "Hai dimenticato la tua password?"; 
$LNG['forgot_password'] = "Hai dimenticato la tua password?"; 
$LNG['free'] = "Gratuita"; 
$LNG['free_fuck_desc'] = "Alla fine di ogni mese, estrarremo un utente Premium fortunato, che vincerà una Scopata Gratutia con qualsiasi ragazza da lui selezionata su ".NGE_SITE_TITLE.".  Noi pagheremo tutto il costo per lui! <BR /> <BR /> Congratulazioni a tutti i vincitori fortunati!  Speriamo che ti sia divertito con la tua ragazza preferita... !"; 
$LNG['free_fuck_lotto'] = "Free Fuck Lottery"; 
$LNG['free_fuck_note'] = "Nota: Questa proposta è valida fino a 300 Euro / prenotazione per un’ora, quindi se scegli una ragazza per 250 Euro all'ora, gli altri 50 euro non ti verranno pagati. La vincita non può essere cambiata in contanti o in qualsiasi altra cosa. Il vincitore riceverà tutti i dettagli per riscuotere la sua vincita."; 
$LNG['free_mb_desc'] = "Il tuo profilo è attualmente in visione in basso, alla fine dell’ultima pagina, perciò solo pochi utenti possono vederlo. Fai l’upgrade adesso e:"; 
$LNG['free_mb_desc_ag'] = "%COUNT% escorts - <a href='/private/upgrade_escort.php$QUERY_STRING'>Fare l’upgrade adesso </a> par farti conoscere dagli utenti!"; 
$LNG['friday'] = "Venerdì"; 
$LNG['friendly'] = "Amichevole"; 
$LNG['from'] = "da"; 
$LNG['fuckometer'] = "Fuckometer"; 
$LNG['fuckometer_desc'] = "The Fuckometer is a rating indicating the girl's performance based on the review indications. It is used to signalize the best girls in action."; 
$LNG['fuckometer_range'] = "Fuckometer"; 
$LNG['fuckometer_rating'] = "Fuckometer"; 
$LNG['fuckometer_short'] = "FM"; 
$LNG['fuckometer_toplist'] = "Fuckometer toplist"; 
$LNG['full_name'] = "Nome e Cognome"; 
$LNG['gays'] = "Gays"; 
$LNG['gender'] = "Sesso"; 
$LNG['genuine_photo'] = "Foto originali per 100%"; 
$LNG['get_trusted_review'] = "Diventare “recensore attendibile�?"; 
$LNG['get_trusted_review_desc'] = "Diventa una recensione attendibile 100%. Per favore compila i campi di sotto!<br /> 
    Come funziona? <a href='%LINK%'>clicca qui </a>!"; 
$LNG['girl'] = "Ragazza"; 
$LNG['girl_booked'] = "Hai prenotato  %NAME% per %DATE%."; 
$LNG['girl_no_longer_listed'] = "Questa ragazza non è più presente sul %SITE%"; 
$LNG['girl_of'] = "Ragazza del mese di"; 
$LNG['girl_of_the_month_history'] = "L'archivio della ragazza del mese"; 
$LNG['girl_on_tour_1'] = "Ragazza e in tour a %CITY% - %COUNTRY%"; 
$LNG['girl_on_tour_2'] = "dal %FROM_DATE% al %TO_DATE%"; 
$LNG['girl_on_tour_3'] = $LNG['tour_phone'].": %PHONE%"; 
$LNG['girl_on_tour_4'] = $LNG['tour_email'].": %EMAIL%"; 
$LNG['girl_origin'] = "Origine della ragazza"; 
$LNG['girls'] = "Ragazze"; 
$LNG['girls_in_italy'] = "Tutte le ragazze di questa lista sono ORA in ".$LNG['country_'.MAIN_COUNTRY]."! Prenotale SUBITO!"; 
$LNG['girls_international'] = "Ragazze disponibili worldwide, nota per favore che se le vuoi incontrare in ".$LNG['country_'.MAIN_COUNTRY].", nella gran parte dei casi è necessario prenotarle per almeno 12 ore"; 
$LNG['go_to_escort_profile'] = "Andare al profilo dell'escort"; 
$LNG['go_to_reviews_overview'] = "Visione generale"; 
$LNG['gray'] = "Grigio"; 
$LNG['green'] = "Verde"; 
$LNG['hair'] = "Capelli"; 
$LNG['hair_color'] = "Colore dei capelli"; 
$LNG['hard_to_believe_its_her'] = "Difficile credere che le foto siano le sue"; 
$LNG['hard_to_book'] = "Difficile da prenotare"; 
$LNG['height'] = "Altezza"; 
$LNG['help_system'] = "Aiuto"; 
$LNG['help_watch_list'] = "Puoi aggiungere un utente/escort o agenzia alla tua lista dei preferiti. Questa lista serve per ricevere una e-mail di avviso quando succede un evento speciale: 
Se una  <b>escort</b> è presente nella tua lista, riceverai la nostra e-mail nei seguenti casi: 
<ul><li>La escort ha ricevuto una nuova recensione</li><li>La escort è partita per city tour </li><li>La escort è andata in/ tornata dalla vacanze.</li></ul> 
Se aggiungi <b>un’agenzia</b> alla tua lista, riceverai le seguenti notifiche: 
<ul><li>l’agenzia ha aggiunto una nuova escort</li></ul> 
Se invece aggiungi un <b>utente iscritto</b> alla tua lista, riceverai queste notifiche: <ul><li>L’utente ha aggiunto una nuova recensione</li></ul> 
Puoi mettere tutti i nomi che vuoi sulla lista dei preferiti. 
Per modificare la tua lista dei preferiti, clicca su \"Lista dei preferiti\" nel menu dopo aver fatto login."; 
$LNG['hip'] = "Anca"; 
$LNG['home'] = "Home Page"; 
$LNG['home_page'] = "Home page"; 
$LNG['hour'] = "ora"; 
$LNG['hours'] = "ore"; 
$LNG['howto_set_main'] = "Regolare un immagine come quella principale, clicca qui."; 
$LNG['i_agree_with_terms'] = "Ho letto e accetto i <a href='%LINK%'>termini generali e le condizioni</a> descritte"; 
$LNG['in'] = "A"; 
$LNG['in_face'] = "In faccia"; 
$LNG['in_mouth_spit'] = "In bocca con sputo "; 
$LNG['in_mouth_swallow'] = "In bocca con ingoio"; 
$LNG['incall'] = "ricevo"; 
$LNG['independent'] = "Indipendente"; 
$LNG['independent_escorts_from'] = "Escort indipendente proveniente da"; 
$LNG['info'] = "Informazioni"; 
$LNG['intelligent'] = "Intelligente"; 
$LNG['international'] = "Internazionale"; 
$LNG['international_directory'] = "Indice internazionale"; 
$LNG['invalid_birth_date'] = "La data di nascita non è valida"; 
$LNG['invalid_curr_password'] = "La password non è corretta"; 
$LNG['invalid_desired_date'] = "La data non è valida "; 
$LNG['invalid_discount_code'] = "Codice sconto non valido."; 
$LNG['invalid_email'] = "L’email non è valida"; 
$LNG['invalid_email_info'] = "Il nostro sistema ha identificato che la sua mail non e valida. Questo non ci permette di inviarle le nostre e-mail. Il suo account e temporaneamente sospeso. Per favore provveda inserendo una nuova e-mail valida, noi le invieremo nuovamente una mail di verifica con un link per la conferma. Appena lei riceve questa e-mail faccia un click sul link e il suo account sara nuovamente attivo.<br /><br /> Grazie per la comprensione."; 
$LNG['invalid_file_type'] = "Il tipo del file non è valido. Sono concessi solo i files del tipo Jpeg."; 
$LNG['invalid_meeting_date'] = "Data d’incontro non valido"; 
$LNG['invalid_showname'] = "Nome visualizzato non valido, vengono accettati solo caratteri alfanumerici."; 
$LNG['invalid_user_type'] = "Username non valido"; 
$LNG['invitation'] = "Benvenuto sul servizio escort! Scegli una città dove cercare una ragazza."; 
$LNG['invitation_1'] = "Nelle seguente pagine troverai le migliore ragazze escort, Club privè e Agenzie escort nella tua zona- ".$LNG['country_'.MAIN_COUNTRY]." e internazionali. Puoi vedere le informazioni di contatto e foto di ciascuna ragazza."; 
$LNG['invitation_2'] = "Ti auguriamo una piacevole navigazione sul nostro sito. Per domande e prenotazioni non esitare a contattarci: <a href='mailto:".NGE_CONTACT_EMAIL."'>".NGE_CONTACT_EMAIL."</a>"; 
$LNG['kiss'] = "Bacio"; 
$LNG['kiss_with_tongue'] = "Bacio con la lingua"; 
$LNG['kissing'] = "Bacio"; 
$LNG['kissing_with_tongue'] = "Bacio con la lingua"; 
$LNG['language'] = "Lingua"; 
$LNG['language_problems'] = "Problemi di lingua"; 
$LNG['languages'] = "Lingue"; 
$LNG['languages_changed'] = "Le lingue sono state aggiornate"; 
$LNG['last'] = "Ultimo"; 
$LNG['last_name'] = "Cognome"; 
$LNG['latest_10_reviews'] = "Le ultime 10 recensioni sulle ragazze"; 
$LNG['latest_10_third_party_reviews'] = "Le ultime 10 recensioni sulle ragazze degli altri siti."; 
$LNG['level'] = "Livello"; 
$LNG['limitrofi'] = "Limitrofi"; 
$LNG['links'] = "Links"; 
$LNG['loano'] = "Loano"; 
$LNG['locations_changed'] = "Le città sono state aggiornate"; 
$LNG['logged_in'] = "Benvenuto"; 
$LNG['login'] = "Login"; 
$LNG['enter'] = "Login"; 
$LNG['login_and_password_alphanum'] = "Usename e password possono contenere solo caratteri alfanumerici Es:(0-9, a-z, A-Z, _)."; 
$LNG['login_and_signup_disabled'] = "At this moment we are performing scheduled maintenance. No new registrations and site logins are accepted. Please come back later. We are sorry for the inconvenience."; 
$LNG['login_exists'] = "Username già esistente. Per favore scegli un altro username"; 
$LNG['login_problems_contact'] = "Se continui ad avere problemi, non esitare a contattarci: <a href='mailto:".NGE_CONTACT_EMAIL."'>".NGE_CONTACT_EMAIL."</a>"; 
$LNG['logout'] = "Esci"; 
$LNG['looking_for_girls_in_your_city'] = "Cerchi accompagnatrici nella tua città ?"; 
$LNG['looking_for_girls_in_your_region'] = "Looking for girls in your region?"; 
$LNG['looks'] = "Bellezza"; 
$LNG['looks_and_services'] = "Bellezza e servizio"; 
$LNG['looks_range'] = "Voto per la bellezza"; 
$LNG['looks_rating'] = "Voto per la bellezza"; 
$LNG['lower_date'] = "La data finale non può essere antecedente a quella iniziale"; 
$LNG['lower_fuckometer'] = "Fuckomoter non è corretto"; 
$LNG['lower_looks_rating'] = "Voto per la bellezza non è corretto"; 
$LNG['lower_services_rating'] = "Voto per i servizi non è corretto"; 
$LNG['male'] = "Uomo"; 
$LNG['masturbation'] = "Masturbazione"; 
$LNG['meeting_costs'] = "Prezzo dell’incontro"; 
$LNG['meeting_date'] = "Data d’incontro"; 
$LNG['meeting_date_range'] = "Data d’incontro"; 
$LNG['meeting_dress_comments'] = "Posto d’incontro, codice d’abbigliamento, commenti"; 
$LNG['meeting_length'] = "Durata dell’incontro"; 
$LNG['member'] = "Utente"; 
$LNG['member_name'] = "Nome del membro"; 
$LNG['member_profit_01'] = "Scrivere nel forum e inserire i tuoi commenti ed esperienze"; 
$LNG['member_profit_02'] = "Raccontare le tue impressioni personali sulle le ragazze che hai conosciuto"; 
$LNG['member_profit_03'] = "Essere sempre informato sulle nuove ragazze di %SITE%"; 
$LNG['member_profits'] = "Come utente registrato potrai:"; 
$LNG['membership_change_subject'] = "Cambiamento di Profilo Utente su".NGE_SITE_TITLE; 
$LNG['membership_change_to_free_mail'] = " 
    <p>Gentile %LOGIN%,<br /> 
il Suo abbonamento come *Premium Member* su ".NGE_SITE_TITLE." è scaduto, quindi il Suo profilo utente è stato cambiato in Iscrizione Gratuita</p> 
    <p>Se vuole rinnovare il Suo abbonamento, vada sul nostro sito o ci scriva una mail..</p> 
    <p>Clicchi qui per rinnovare ed avrà:</p> 
    <ul> 
        <li> Ragazze con sconto di prenotazione, clicca <a 
href='%LINK%'>qui</a></li> 
        <li> Partecipazione automatica all’estrazione della &quot;Free Fuck 
Lottery&quot; dove puoi vincere un’incontro gratuito con la Sua escort preferita </li> 
        <li>Accesso al Premium Forum</li> 
        <li>Messaggi privati</li> 
        <li>Lista preferiti per: ragazze, utenti e agenzie</li> 
        <li>Possibilità di ricerca avanzata</li> 
        <li>Nessuna pubblicità</li> 
    </ul> 
    <p> Per ulteriori domande o per qualsiasi problema ci contatti via e-mail. 
Le risponderemo il prima possibile.</p> 
    <p>Distinti Saluti<br /> 
    ".NGE_SITE_TITLE."- Team</p> 
"; 
$LNG['membership_change_to_premium_mail'] = " 
    <p>Gentile %LOGIN%,<br /> 
la Sua iscrizione come  *Premium Member* presso il sito ".NGE_SITE_TITLE." è stata effettuata con successo. La ringraziamo per avere scelto i nostri servizi.</p> 
    <p>I Suoi dati d’accesso sono i seguenti:<br /> 
    Login: %LOGIN%</p> 
    <p>(Protegga questi dati in un posto sicuro!)</p> 
    <p>I vantaggi più importanti di essere un *Premium Member* sono</p> 
    <ul> 
        <li>Ragazze con sconto di prenotazione, clicca <a 
href='%LINK%'>qui</a></li> 
        <li>Partecipazione automatica all’estrazione della &quot;Free Fuck 
Lottery&quot; dove puoi vincere un’incontro gratuito con la Sua escort preferita</li> 
        <li>Accesso al Premium Forum</li> 
        <li>Messaggi privati</li> 
        <li>Lista preferiti per: ragazze, utenti e agenzie</li> 
        <li>Possibilità di ricerca avanzata</li> 
        <li>Nessuna pubblicità</li> 
    </ul> 
    <p>Per ulteriori domande o per qualsiasi problema ci contatti via e-mail. 
Le risponderemo il prima possibile.</p> 
    <p>Distinti Saluti<br /> 
    ".NGE_SITE_TITLE."- Team</p> 
"; 
$LNG['membership_type'] = " Tipi d’iscrizione "; 
$LNG['membership_types'] = "Tipi d’iscrizione"; 
$LNG['men'] = "Uomini"; 
$LNG['menu'] = "Menu"; 
$LNG['message'] = "Messaggio"; 
$LNG['message_for'] = "Messaggio a %NAME%"; 
$LNG['message_not_sent'] = "Il messaggio non è stato inviato."; 
$LNG['message_sent'] = "Il tuo messaggio è stato inviato a %NAME%."; 
$LNG['metric'] = "Sistema metrico"; 
$LNG['metric_desc'] = "centimetri, chilogrammi,..."; 
$LNG['minutes'] = "minuti"; 
$LNG['modified'] = "Modificato"; 
$LNG['modify_escort'] = "Modifica escort"; 
$LNG['monday'] = "Lunedì"; 
$LNG['month'] = "Mese"; 
$LNG['month_1'] = "Gennaio"; 
$LNG['month_10'] = "Ottobre"; 
$LNG['month_11'] = "Novembre"; 
$LNG['month_12'] = "Dicembre"; 
$LNG['month_2'] = "Febbraio"; 
$LNG['month_3'] = "Marzo"; 
$LNG['month_4'] = "Aprile"; 
$LNG['month_5'] = "Maggio"; 
$LNG['month_6'] = "Giugno"; 
$LNG['month_7'] = "Luglio"; 
$LNG['month_8'] = "Agosto"; 
$LNG['month_9'] = "Settembre"; 
$LNG['months'] = "Mesi"; 
$LNG['more_info'] = "Ulteriori informazioni..."; 
$LNG['more_new_girls'] = "Altre nuove ragazze"; 
$LNG['more_news'] = "Altre novità..."; 
$LNG['multiple_times_sex'] = "Venire più volte per lo stesso rate"; 
$LNG['my_escort_profile'] = "Il mio profilo escort"; 
$LNG['my_escorts'] = "Le mie escort"; 
$LNG['my_languages'] = "La mia lingua"; 
$LNG['my_locations'] = "Dove lavoro"; 
$LNG['my_new_review_notify_agency'] = "Venire informato/a via e-mail se una delle vostre escort riceve una nuova recensione"; 
$LNG['my_new_review_notify_single'] = "Venire avvisata tramite e-mail se ricevi una nuova recensione."; 
$LNG['my_photos'] = "Le mie foto"; 
$LNG['my_rates'] = "I miei prezzi"; 
$LNG['my_real_profile'] = "I miei dati reali"; 
$LNG['my_reviews'] = "Le mie recensioni"; 
$LNG['name'] = "Nome"; 
$LNG['name_of_the_lady'] = "Nome della ragazza"; 
$LNG['name_of_the_site'] = "Nome del sito"; 
$LNG['ethnic'] = "Etnia"; 
$LNG['nationality'] = "Nazionalità"; 
$LNG['need_help'] = "Serve aiuto ?"; 
$LNG['need_to_be_registered_to_review'] = "Devi essere registrato per poter fare una recensione su di una ragazza, <a href='%LINK%'>Registrati ora gratuitamente!</a>"; 
$LNG['never'] = "mai"; 
$LNG['new'] = "Nuovo"; 
$LNG['new_arrivals'] = "Nuovi annunci"; 
$LNG['new_email'] = "Nuova e-mail"; 
$LNG['new_entries'] = "Nuova arrivata"; 
$LNG['new_escort_email_by_agency_body'] = "<pre> L’agenzia \"%AGENCY%\" ha aggiunto una nuova escort: %ESCORT% ! 
Ricevi questa e-mail perchè hai aggiunto quest’agenzia alla lista dei tuoi preferiti. 
Clicca sul link 
<a href=\"%LINK%\">%LINK%</a> 
per vedere la scheda della ragazza. 
Il tuo ".NGE_SITE_TITLE." Team 
</pre>"; 
$LNG['new_escort_email_by_agency_subject'] = "%AGENCY%ha aggiunto una nuova escort!"; 
$LNG['new_password'] = "Nuova Password"; 
$LNG['new_password_empty_err'] = "La nuova password non può essere vuota"; 
$LNG['new_review_email_agency_body'] = "<pre>User %USER% ha postato una nuova recensione su %ESCORT% ! 
Clicca su questo link 
<a href=\"%LINK%\">%LINK%</a> 
per leggere la recensione. 
Il tuo ".NGE_SITE_TITLE." Team 
</pre>"; 
$LNG['new_review_email_agency_subject'] = "La tua escort ha ricevuto una nuova recensione!"; 
$LNG['new_review_email_by_escort_body'] = "<pre> %ESCORT% ha ricevuto una nuova recensione da %USER% ! 
Ricevi questa e-mail perchè hai aggiunto questa escort alla tua lista dei preferiti. 
Clicca qui 
<a href=\"%LINK%\">%LINK%</a> 
per leggere la recensione. 
Il tuo ".NGE_SITE_TITLE." Team 
</pre>"; 
$LNG['new_review_email_by_escort_subject'] = "%ESCORT% ha ricevuto una nuova recensione!"; 
$LNG['new_review_email_by_user_body'] = "<pre> Utente %USER% ha postato una nuova recensione su %ESCORT% ! 
Ricevi questa e-mail perchè hai aggiunto questo utente alla tua lista dei preferiti. 
Clicca su questo link 
<a href=\"%LINK%\">%LINK%</a> 
per leggere la recensione. 
Il tuo ".NGE_SITE_TITLE." Team 
</pre>"; 
$LNG['new_review_email_by_user_subject'] = "%USER% ha postato una nuova recensione !"; 
$LNG['new_review_email_escort_body'] = "<pre>User %USER% Ha postato una nuova recensione su di te! 
Clicca su questo link 
<a href=\"%LINK%\">%LINK%</a> 
per leggere la recensione. 
Il tuo ".NGE_SITE_TITLE." Team 
</pre>"; 
$LNG['new_review_email_escort_subject'] = "Hai una nuova recensione!"; 
$LNG['news'] = "Novità"; 
$LNG['newsletter'] = "Newsletter"; 
$LNG['next'] = "Seguente"; 
$LNG['next_step'] = "Continua"; 
$LNG['no'] = "No"; 
$LNG['no_blowjob'] = "No Pompino"; 
$LNG['no_cumshot'] = "Non è possibile venire"; 
$LNG['no_esc_sel_err'] = "Devi selezionare una escort"; 
$LNG['no_escort_languages'] = "Non hai settato nessuna lingua. Clicca qui per sceglierne una."; 
$LNG['no_escort_main_pic'] = "Non ha una foto. <a href='%LINK%'>Clicca qui</a> per sceglierne una."; 
$LNG['no_escort_name_languages'] = "Escort %ESCORT% non ha settato nessuna lingua.Clicca qui per sceglierne una."; 
$LNG['no_escort_name_main_pic'] = "Escort %ESCORT% non ha una foto. <a href='%LINK%'>Clicca qui</a> per sceglierne una."; 
$LNG['no_escort_name_photos'] = "Escort %ESCORT% non hai caricato delle foto valide. <a href='%LINK%'>Clicca qui per aggiornare la galleria</a>."; 
$LNG['no_escort_photos'] = "Non hai caricato delle foto valide per il tuo profilo. <a href='%LINK%'>Clicca qui per aggiornare la tua galleria</a>."; 
$LNG['no_escort_profile'] = "Non hai un profilo escort attivo. <a href='%LINK%'>Clicca qui per aggiungerne uno</a>."; 
$LNG['no_escorts_found'] = "Nessuna ragazza trovata"; 
$LNG['no_escorts_profile'] = "Non hai definito un profilo escort. <a href='%LINK%'>Clicca qui per aggiungerne uno</a>."; 
$LNG['no_kiss'] = "Niente baci"; 
$LNG['no_kissing'] = "No Bacio"; 
$LNG['no_only_once'] = "No, solo una volta"; 
$LNG['no_premium_escorts'] = "Nessuna premium escort."; 
$LNG['no_reviews_found'] = "Nessuna recensione trovata"; 
$LNG['no_sex_avail_chosen'] = "Devi selezionare almeno un categoria di clienti"; 
$LNG['no_users_found'] = "Nessun utente trovato"; 
$LNG['no_votes'] = "Senza voto"; 
$LNG['norm_memb'] = "1 mese di iscrizione NORMALE per 50 EUR"; 
$LNG['normal'] = "Normale"; 
$LNG['normal_mb_desc_ag'] = "%COUNT% escorts (1st expiring in %DAYS% days)"; 
$LNG['normal_review'] = "normale"; 
$LNG['normal_review_desc'] = "Recensione normale significa che l'agenzia/ragazza non ha risposto al sms/e-mail, quindi la recensione non è stata ne confermata nè smentita! Questo vuol dire che la recensine può essere considerata veritiera!"; 
$LNG['not_approved'] = "In attesa di approvazione"; 
$LNG['not_available'] = "Not available"; 
$LNG['not_logged'] = "Not logged in"; 
$LNG['not_logged_desc'] = "You have to be logged in to use this feature."; 
$LNG['not_verified_msg'] = " 
  <p>Il tuo account non è stato confermato.<br /> 
  Per diventare un membro del ".NGE_SITE_TITLE." devi verificare il tuo indirizzo e-mail. Se non hai ancora ricevuto la mail, per favore <a href=\"%LINK%\"> clicca qui e ti rimandiamo la mail di conferma</a>. 
  </p><p> 
  Nota: Per favore controlla anche la cartella spam, perchè purtroppo alcuni provider mettono le nostre mail nella cartella spam. Grazie per la comprensione. 
  </p>"; 
$LNG['not_yet_member'] = "Non sei ancora REGISTRATO? Registrati adesso gratis!"; 
$LNG['not_yet_premium_signup_now'] = "Non sei ancora un Utente Premium? <a href='%LINK%'> Registrati ORA</a> e risparmia tanti soldi!"; 
$LNG['note'] = "Nota"; 
$LNG['notify_comon'] = "<pre>Ricevi questa mail perche sei registrato/a sull’ ".NGE_SITE_TITLE." e hai scelto di ricevere questi avvisi. 
Per cambiare i tuoi settaggi, entra nel tuo account e clicca su \"Watching list\" nel menu.</pre>"; 
$LNG['now'] = "Adesso"; 
$LNG['number'] = "Numero"; 
$LNG['number_of_reviews'] = "Numero di recensioni"; 
$LNG['offers_discounts'] = "Offre sconto"; 
$LNG['on_tour_in'] = "in tour a"; 
$LNG['only'] = "solamente"; 
$LNG['only_fake_free_reviews'] = "Solo recensioni reali per 100%"; 
$LNG['only_for_premium_members'] = "Solo per utenti Premium"; 
$LNG['only_girls_with_reviews'] = "Solo ragazze  con recensione"; 
$LNG['only_reviews_from_user'] = "Solo recensioni scritte da un certo utente"; 
$LNG['only_reviews_not_older_then'] = "Solo recensioni non più vecchie di"; 
$LNG['optional'] = "opzionale"; 
$LNG['oral_without_condom'] = "Orale senza preservativo"; 
$LNG['order_premium_spot_now'] = "Ordina un Premium spot subito!"; 
$LNG['origin_escortforum'] = "escortforum"; 
$LNG['origin_non_escortforum'] = "non escortforum"; 
$LNG['other'] = "Altro"; 
$LNG['other_boys_from'] = "Altri accompagnatori di"; 
$LNG['other_escorts_from'] = "Altre accompagnatrici di"; 
$LNG['boys_from'] = "Accompagnatori di"; 
$LNG['escorts_from'] = "Accompagnatrici di"; 
$LNG['outcall'] = "outcall"; 
$LNG['overall_rating'] = "Valutazione completa"; 
$LNG['partners'] = "Partners"; 
$LNG['passive'] = "Passiva"; 
$LNG['passwd_has_been_sent'] = "La password è stata inviata all’indirizzo %EMAIL%"; 
$LNG['passwd_succesfuly_sent'] = "La Vostra password é stata inviata al Vostro indirizzo di posta elettronica."; 
$LNG['password'] = "Password"; 
$LNG['password_changed'] = "La password è stata cambiata con successo"; 
$LNG['password_invalid'] = "La password non è valida. Deve contenere minimo 6 caratteri"; 
$LNG['password_missmatch'] = "Password"; 
$LNG['percent_discount'] = "%DISCOUNT%% sconto"; 
$LNG['phone'] = "Telefono"; 
$LNG['phone_instructions'] = "Istruzione"; 
$LNG['photo_correct'] = "Foto corretta"; 
$LNG['photos'] = "Le foto"; 
$LNG['pic_delete_failed'] = "La cancellazione dell’immagine non è riuscita"; 
$LNG['pic_deleted'] = "Immagine cancellata"; 
$LNG['place'] = "Luogo incontro"; 
$LNG['please_choose_services'] = "Per favore seleziona i servizi che sono stati offerti dalla ragazza, questo influenza il suo fuckometer!"; 
$LNG['please_fill_form'] = "Compila per favore questo form"; 
$LNG['please_provide_email'] = "Inserisci l'indirizzo e-mail che hai utilizzato per effettuare la registrazione su %SITE%"; 
$LNG['please_select'] = "Seleziona per favore..."; 
$LNG['plus2'] = "2+"; 
$LNG['poshy'] = "Raffinata"; 
$LNG['posted_reviews'] = "Fino ad adesso hai scritto %NUMBER% recensioni"; 
$LNG['posted_to_forum'] = "Fino ad adesso hai inserito %NUMBER% messaggi sul forum."; 
$LNG['premium'] = "Premium"; 
$LNG['premium_escorts'] = "Premium escorts"; 
$LNG['premium_mb_desc_ag'] = "%COUNT% escorts (1st expiring in %DAYS% days)"; 
$LNG['premium_user'] = "Utente Premium"; 
$LNG['previous'] = "Precedente"; 
$LNG['price'] = "Prezzo"; 
$LNG['priv_apart'] = "Appartamenti privati"; 
$LNG['private_menu'] = "Menu privato"; 
$LNG['problem_report'] = "Segnala un problema"; 
$LNG['problem_report_back'] = "Clicca qui per tornare al profilo della ragazza"; 
$LNG['problem_report_no_accept'] = "Grazie, la segnalazione del problema e stata inviata."; 
$LNG['problem_report_no_name'] = "Per favore, aggiungi il tuo nome"; 
$LNG['problem_report_no_report'] = "Scrivi la tua segnalazione per favore"; 
$LNG['prof_city_expl'] = "Citta d’origine della ragazza, non necessariamente la sua città di lavoro. La locazione di lavoro puo essere definito nei prossimi punti."; 
$LNG['prof_country_expl'] = "Paese d’origine della ragazza, non necessariamente la sua città di lavoro. La locazione di lavoro può essere definita nei prossimi punti."; 
$LNG['profile'] = "Profilo"; 
$LNG['profile_changed'] = "Il profilo è stato aggiornato"; 
$LNG['promote_this_girl'] = "Promuovere questo/a ragazzo/a"; 
$LNG['promote_yourself'] = "Promuoviti da solo/a"; 
$LNG['provided_services'] = "Servizi forniti"; 
$LNG['publish_date'] = "Data della pubblicazione"; 
$LNG['rank'] = "Classifica"; 
$LNG['rate'] = "Prezzo"; 
$LNG['rate_looks'] = "Valuta la sua bellezza"; 
$LNG['rate_services'] = "Valuta il servizio"; 
$LNG['rates'] = "Prezzi"; 
$LNG['rates_changed'] = "I prezzi sono stati aggiornati"; 
$LNG['rates_deleted'] = "Prezzi cancellatti con successo"; 
$LNG['rating'] = "Rating"; 
$LNG['re_verify_sent'] = "Ti abbiamo rinviato la mail di conferma. Per favore controlla la tua casella postale e segue le istruzioni della mail."; 
$LNG['read_this_review'] = "Leggi questa recensione"; 
$LNG['read_whole_article'] = "Leggi tutto l'articolo"; 
$LNG['real'] = "Naturali"; 
$LNG['real_data_form_text'] = "Ti chiediamo di fornire informazioni reali. Ricordiamo, inoltre, che queste informazioni personali non verranno mai pubblicate sul sito, verranno trattate in modo confidenziale per ragioni interne."; 
$LNG['real_name'] = "Nome vero"; 
$LNG['recalibrate_now'] = "Verificare ADESSO"; 
$LNG['receive_newsletter'] = "Ricevere ".NGE_SITE_TITLE." newsletter"; 
$LNG['red'] = "Rosso"; 
$LNG['reference'] = "Come ci hai conosciuto?"; 
$LNG['regards'] = "Saluti, %SITE% team"; 
$LNG['region'] = "Regione"; 
$LNG['register'] = "Registrati"; 
$LNG['registration'] = "Registrazione"; 
$LNG['rem_watch'] = "Rimuovere dalla lista dei preferiti"; 
$LNG['rem_watch_success'] = "<b>%NAME%</b> è stato/a rimosso/a dalla lista dei preferiti con successo. <a href='javascript:history.back()'>Clicca qui per tornare indietro.</a>"; 
$LNG['remove'] = "Cancella"; 
$LNG['renew_now'] = "Rinnova adesso!"; 
$LNG['required_fields'] = "I campi obbligatori sono segnati con *"; 
$LNG['review'] = "Recensioni"; 
$LNG['review_changed'] = "Recensione è stata aggiornata"; 
$LNG['review_guide_and_rules'] = " 
    <strong>Regolamento:</strong><br /> 
    Per favore rispetta il regolamento, e non scrivere recensioni false! Ogni recensione aiuta gli altri utenti a ricevere un servizio migliore! <ul> 
        <li>per favore aggiungi tutti i servizi che sono offerti dalla ragazza</li> 
        <li>aggiungere nuove ragazze alla notra database: se non trovi la ragazza che vuoi recensire, per favore aggiungila alla nostra database</li> 
        <li>per favore descrivi le tue esperienze con la ragazza, le recensioni troppo corte, p.es. “Lei è ok!�? vengono cancellate. </li> 
        <li>per favore non dare voti più alti di 8.0 se la ragazza non ha offerto almeno questi servizi: 
        <ul> 
            <li>oral senza preservativo</li> 
            <li>attitudine naturale o enthusiastica nel sesso</li> 
            <li>bacio con la lingua</li> 
        </ul> 
    </ul>"; 
$LNG['review_language'] = "Lingua di recensioni"; 
$LNG['review_sms'] = "Sei stata/o recensita/o sull ".NGE_SITE_TITLE."! Semplicemente rispondi via sms: Si, se e vero e No se non e vero! Nel secondo sms riceverai i dettagli del cliente."; 
$LNG['review_status'] = "La posizione della recensione"; 
$LNG['review_this_girl'] = "Recensire questa ragazza"; 
$LNG['reviews'] = "Recensioni"; 
$LNG['reviews_to_recalibrate'] = " 
   Gentile %NAME%,<br /> 
    hai %REVIEWS% recensioni da verificare. Ti chiediamo di aiutarci a verificare tutte le tue recensioni dando informazioni più dettagliate sulla ragazza che hai incontrata."; 
$LNG['rouen'] = "Rouen"; 
$LNG['royal'] = "Sistema reale"; 
$LNG['royal_desc'] = "pollici, piedi,..."; 
$LNG['rss_channel_desc'] = "Le ultimissime escort"; 
$LNG['salon'] = "Night Club"; 
$LNG['sardegna'] = "Sardegna"; 
$LNG['saturday'] = "Sabato"; 
$LNG['sauna'] = "Locali notturni"; 
$LNG['save_changes'] = "Salva modifiche"; 
$LNG['score'] = "Score"; 
$LNG['search'] = "Cerca"; 
$LNG['search_desc'] = "Benvenuto nel motore di ricerca semplice del ".NGE_SITE_TITLE.".  Per piu opzioni di ricerca clicca su <a href='%LINK%'>  ricerca avanzata </a>."; 
$LNG['search_desc2'] = "Benvenuto nel motore di ricerca avanzata del ".NGE_SITE_TITLE.". Per opzioni piu semplici di ricerca clicca su <a href='%LINK%'> ricerca semplice </a>."; 
$LNG['search_escorts'] = "Cerca"; 
$LNG['search_name'] = "Nome di ragazza/agenzia"; 
$LNG['search_reviews'] = "Cerca recensione"; 
$LNG['search_reviews_back'] = "Torna a cercare recensioni"; 
$LNG['search_reviews_no_params'] = "Indica per piacere almeno un criterio di ricerca "; 
$LNG['search_reviews_no_result'] = "Spiacenti, nessuna recensione trovata, riprovalo per favore."; 
$LNG['search_users'] = "Cerca utente"; 
$LNG['see_escorts_soon_on_tour'] = "Clicca qui per vedere tutte le ragazze che arriveranno a breve in ".$LNG['country_'.MAIN_COUNTRY].""; 
$LNG['see_independent_preview'] = "guarda le preview di tutte le escort indipendenti"; 
$LNG['select_as_main'] = "Vuoi regolare l’immagine selezionata come quella principale ?"; 
$LNG['select_your_service_requirements'] = "Seleziona i requisiti per il servizio"; 
$LNG['send_message'] = "Invia messaggio"; 
$LNG['service_comments'] = "Commenti sul servizio"; 
$LNG['services'] = "Servizi"; 
$LNG['services_range'] = "Voto per i servizi"; 
$LNG['services_rating'] = "Voto per i servizi"; 
$LNG['services_she_was_willing'] = "Servizi che voleva fare"; 
$LNG['settings'] = "Opzioni"; 
$LNG['settings_update_error'] = "Errore durante l’aggiornamento dei settaggi"; 
$LNG['settings_updated'] = "Settaggi aggiornati con successo."; 
$LNG['sex'] = "Sesso"; 
$LNG['sex_available_to'] = "Offre servizi per"; 
$LNG['shoe_size'] = "Numero di scarpe"; 
$LNG['show_all_reviews'] = "Visualizza tutte le recensioni"; 
$LNG['show_email'] = "Vedere l’email sul sito?"; 
$LNG['show_name'] = "Nome"; 
$LNG['show_only_girls_on_tour_in'] = "Mostrami solo le ragazze in tour nella città di"; 
$LNG['show_site_reviews'] = "Visualizza le recensioni del %SITE%"; 
$LNG['show_third_party_reviews'] = "Visualizza le recensioni sulle ragazze degli altri siti"; 
$LNG['showname_exists'] = "Il nome è gia esistente, scegli un altro per favore"; 
$LNG['sign_up'] = "Registrati"; 
$LNG['sign_up_for_free'] = "Iscriviti ora gratis!"; 
$LNG['signup_activated_01'] = "Il tuoi dati sono stati attivati !"; 
$LNG['signup_activated_02'] = " 
        Congratulazioni!<br /> 
        Ora sei un utenti registrato di %SITE%.<br /> 
        I dettagli del tuo login sono stati trasmessi alla tua email."; 
$LNG['signup_activated_03'] = "Clicca qui per entrare."; 
$LNG['signup_and_win'] = "Signup today as a Premium Member and WIN A FREE FUCK OF THE VALUE OF 300 EURO!"; 
$LNG['signup_confirmation_email'] = "<pre> 
Caro %USER%, 
Grazie per la registrazione %SITE%. 
Ora sei un utente registrato completo di %SITE%. 
Sotto puoi trovare le informazioni del tuo login. Conservale in un posto sicuro per dei futuri riferimenti: 
Username: %USER% 
Sito: %SITE% 
Buona navigazione! 
A presto, con le tue inserzioni e le tue recensioni ! 
Il tuo %SITE% Team</pre>"; 
$LNG['signup_confirmation_email_subject'] = "Registrazione sul %SITE%"; 
$LNG['signup_error_email_missing'] = "Errore: Non hai fornito una email valida, inserisci per favore una email valida."; 
$LNG['signup_error_email_used'] = "Errore: Questo email e gia esistente per %SITE%., scegli un altro."; 
$LNG['signup_error_login_missing'] = "Errore: Non hai compilato il login."; 
$LNG['signup_error_login_used'] = "Errore: Il login è già esistente. Scegli un altro."; 
$LNG['signup_error_password_invalid'] = "Errore: Il la password non è valido. La pssword deve contenere minimo 6 caratteri."; 
$LNG['signup_error_password_missing'] = "Errore: Non hai inserito la tua password."; 
$LNG['signup_error_password_wrong'] = "Errore: le password devono essere uguali."; 
$LNG['signup_error_terms_missing'] = "Devi accettare i termini e le condizioni in ordine per poter essere membro del nostro sito."; 
$LNG['signup_successful_01'] = "La registrazione su %SITE% è riuscita."; 
$LNG['signup_successful_02'] = "Grazie per la registrazione !"; 
$LNG['signup_successful_03'] = "Per confermare la registrazione controlla la tua posta elettronica."; 
$LNG['signup_successful_04'] = "<b>Attenzione: e possibile che la nostra email di conferma ci metta qualche minuto. Controlla bene la tua posta.</b>"; 
$LNG['signup_verification_email'] = "<pre> 
Caro %USER%, 
grazie per la registrazione %SITE%. 
Per completare la tua registrazione, devi per prima cliccare sul  link sotto, per attivare i tuoi dati. 
<a href=\"%LINK%\">%LINK%</a> 
Website: %SITE% 
Your %SITE% Team.</pre>"; 
$LNG['signup_verification_email_subject'] = "Registration at %SITE%"; 
$LNG['silicon'] = "Silicone"; 
$LNG['single_girl'] = "Ragazze single"; 
$LNG['sitemap'] = "Sitemap"; 
$LNG['smoker'] = "Fumatore"; 
$LNG['somewhere_else'] = "Altrove"; 
$LNG['soon'] = "Fra poco"; 
$LNG['state'] = "Regione"; 
$LNG['step_1'] = "Punto 1"; 
$LNG['step_2'] = "Punto 2"; 
$LNG['submit'] = "Invia"; 
$LNG['subscribe'] = "Abbonarsi"; 
$LNG['successfuly_logged_in'] = "Ora sei entrato."; 
$LNG['sucks'] = "Sucks"; 
$LNG['sunday'] = "Domenica"; 
$LNG['swallow'] = "Swallow"; 
$LNG['system_error'] = "Errore del sistema, contatta il servizio clienti."; 
$LNG['terms_and_conditions'] = "Condizioni generali"; 
$LNG['terms_text'] = " 
    <h1>Condizioni generali di contratto (CGC)</h1> 
    <h2>&sect; 1 Rapporto d’affari</h2> 
    <p>Le CGC regolano i rapporti tra gli utenti (di seguito „utenti“), i clienti (di seguito „clienti“) e la DA Products GmbH (di seguito „DA“), il gestore del sito Internet „".NGE_SITE_TITLE."“ (di seguito „sito web“) e valgono per tutti i servizi e i prodotti offerti nel sito web, a seconda dei prodotti e dei servizi di seguito vengono indicate ulteriori condizioni contrattuali.</p> 
    <h2>&sect; 2 Direttive generali d’uso</h2> 
    <p>Il sito web è destinato a utenti che abbiano più di 18 anni.</p> 
    <p>L’età e altre informazioni personali contenute nel profilo dell’utente o cliente, in annunci, chat, forum o recensioni ecc. sono controllati dalla DA solo a campione; questo controllo non ha validità giuridica e non è vincolante. Ogni utente ha la responsabilità di controllare che i suoi partner abbiano compiuto la maggiore età in base alla legislazione in vigore nel proprio paese.</p> 
    <p>La DA non è responsabile per l’utilizzo delle informazioni personali rese accessibili nel sito web.</p> 
    <h2>&sect; 3 Dati utente / cliente, link di terzi, cookies</h2> 
    <p>All’atto dell’utilizzo e in particolare all’atto della registrazione e inserzione nel sito web la DA provvede a mettere in memoria alcune informazioni. Il cliente/utente può controllare e modificare i propri dati contenuti nel profilo e nelle inserzioni ogni qualvolta lo desideri. La DA inoltre mette in memoria in forma anonima i dati utente e cliente per i contatti, per la sicurezza tecnica del sistema, per motivi di statistica, di innovazione e per il miglioramento del prodotto. I dati non vengono messi a disposizione di terzi se non per la lotta agli abusi e in caso di contenuti vietati per legge. Con la registrazione e il conseguente utilizzo del sito web l’utente/cliente da il proprio <strong>consenso</strong> affinché i dati messi a disposizione possano essere <strong>elaborati e memorizzati</strong> per uso interno come consentito dalla legge sulla privacy. In caso di partecipazione a concorsi si provvederà ad indicare caso per caso se i dati vengono trasmessi a terzi per permettere lo svolgimento del concorso.</p> 
    <p>I contenuti di dati trasmessi dall’utente/cliente via Internet come e-mail („messaggi privati“), SMS o simili, non vengono sottoposti al controllo della DA. La DA non è responsabile per il contenuti di tali dati. La DA non utilizza indirizzi mail consultabili per l’invio di spam (le newsletter vengono spedite esclusivamente a utenti/clienti che richiedono espressamente questo servizio v. “Settings”) e non vende indirizzi mail a chi spedisce spam. Non è da escludersi che gli indirizzi mail consultabili pubblicamente vengano utilizzati in modo improprio da terzi, la DA tuttavia non si assume alcuna responsabilità in merito.</p> 
    <p>Il sito web contiene dei <strong>link</strong> per offerte di terzi. La DA non ha alcuna influenza sul trattamento che gli offerenti terzi fanno dei dati degli utenti raccolti sui propri siti web, l’utilizzo di questi siti è a rischio proprio dell’utente/cliente, la Da non si assume alcuna responsabilità per un utilizzo improprio o la diffusione dei dati.</p> 
    <p>Per poter adattare il sito web nel miglior modo possibile alle esigenze degli utenti/clienti, in certi casi vengono inseriti dei <strong>cookies</strong> (piccoli file memorizzati nel computer dell’utente/cliente per facilitare il ritrovamento del sito e il suo aggiornamento). I cookies non contengono dati personali. Configurando il browser in un certo modo è possibile rinunciare all’installazione dei cookies. Rinunciando ai cookies si può tuttavia provocare un peggioramento o l’impossibilità di accedere ai servizi e alle funzioni offerti dal sito web; la DA non si assume alcuna responsabilità in merito.</p> 
    <h2>&sect; 4 Forum e recensioni</h2> 
    <p>Con la registrazione al sito web e la partecipazione a forum e recensioni l’utente dichiara che i diritti d’autore sono suoi e che alla DA vengono conferiti i <strong>diritti d’uso</strong> per gli articoli e le recensioni. Non è possibile garantire la riservatezza dei forum e la pubblicazione degli articoli spediti. La DA non si assume alcuna responsabilità per la correttezza dei contenuti degli articoli e non tiene alcuna corrispondenza in merito.</p> 
    <p>Il contenuto della registrazione e degli articoli in forum e recensioni non deve contravvenire alle leggi svizzere né a quelle dei paesi dove articoli/recensioni vengono immessi nella rete. Contenuti con riferimenti a pornografia infantile, sesso con animali o escrementi e tutti i dati memorizzati dei relativi utenti e clienti vengono trasmessi subito e senza preavviso alle autorità penali competenti. L’utente assicura espressamente che i suoi articoli/recensioni non contravvengono a diritti d’autore, alla legislazione sulla tutela dei dati o al diritto della concorrenza e si impegna a mantenere la DA <strong>libera e indenne</strong> da rivendicazioni da parte di terzi che si basano su atti illegali dell’utente o su errori nel contenuto dei suoi articoli e delle informazioni ivi messe a disposizione e in generale su infrazioni alla legge, comprese le spese giudiziarie e di difesa.</p> 
    <p>L’utente si impegna a <strong>mantenere segreti</strong> i dati di login e le password, per evitare che persone non autorizzate (in particolare minorenni) abbiano accesso al sito web.</p> 
    <p>La DA ha il diritto di cancellare senza indicare i motivi e senza preavviso qualsiasi articolo. Nel caso in cui ci siano dei terzi che rivendicano in modo attendibile i diritti per contenuti di articoli di cui è responsabile l’utente, la DA può bloccare questi articoli e i relativi dati (anche per salvare le prove).</p> 
    <p>La <strong>cancellazione</strong> di utenti registrati avviene solo su richiesta scritta e può essere richiesto un controllo dell’identità. Da canto suo la DA può cancellare in qualsiasi momento e senza giustificazione registrazioni indebite.</p> 
    <p>La DA si riserva il diritto, ma non è suo dovere, di controllare il sito web e i profili, i contenuti e le offerte ivi pubblicate. Il blocco di tali dati è esclusivamente a discrezione della DA, l’utilizzo responsabile da parte dell’utente/cliente rimane espressamente valida.</p> 
    <h2>&sect; 5 Inserzioni e pubblicità su banner nel sito web</h2> 
    <p>L’inserzionista, in qualità di cliente, conferisce a DA l’incarico di mettergli a disposizione sul sito web, per un certo periodo, un numero desiderato di spazi per inserzioni commerciali o non commerciali (con o senza fotografia) o banner (attualmente le rubriche sono Escort, Boys, Trans, Discounted Girls, Tours, Directories interne, queste possono essere in qualsiasi momento ampliate o ridotte) in cambio del versamento del costo corrente dell’annuncio. La DA si riserva il diritto di modificare in qualsiasi momento il tipo, il volume, le condizioni e i costi delle inserzioni per le scadenze future.</p> 
    <p>Le inserzioni e i banner vengono concordati per iscritto o online sulla base di accordi separati sulle inserzioni, inoltre i regolamenti contenuti in questo paragrafo valgono come parte integrante degli accordi individuali. L’attivazione di incarichi viene confermata al cliente per e-mail (se questa è stata fornita) dopo l’avvenuto pagamento della tariffa. La DA si riserva il diritto, dopo aver controllato i dati del cliente o in caso di non avvenuto pagamento della tariffa dovuta per le inserzioni, di rifiutare l’incarico o di sospenderlo o stornarlo senza preavviso; ciò non solleva il cliente dal pagamento delle somme contrattualmente dovute per le prestazioni in scadenza. <em>Il pagamento può essere effettuato in franchi svizzeri o euro in contanti alla stipula del contratto, mediante bonifico anticipato o carta di credito.</em></p> 
    <p>I clienti si impegnano a fornire solo dati conformi a verità per quanto riguarda la loro persona e i servizi da loro offerti, essi sono responsabili dei dati pubblicati e si impegnano a indennizzare la DA in caso di rivalsa diretta (comprese eventuali spese di difesa e penali). Se vengono riscontrate violazioni le inserzioni possono essere cancellate senza diffida fino alla loro correzione o in casi gravi, in modo permanente senza diritto di rimborso delle somme già pagate. Contenuti con riferimenti a pornografia infantile, sesso con animali o escrementi e i dati memorizzati dei relativi utenti e clienti vengono trasmessi subito e senza preavviso alle autorità penali competenti.</p> 
    <p>Per quanto concerne le inserzioni viene applicato il <strong>diritto svizzero sulle commesse, luogo d’adempimento e foro competente è Zurigo.</strong></p> 
    <h2>&sect; 6 Diritti d’autore, modifica del sito web</h2> 
    <p>Tutti i file di testo, le immagini, gli elementi web e sonori, i loghi e la grafica di questo sito e di tutte le sue sottocartelle sono di proprietà intellettuale della DA e sono protetti da diritti d’autore. E’ vietato scaricare, copiare o utilizzare file aggirando o non rispettando le regole d’uso descritte nel presente contratto. Nel caso questa clausola non venga rispettata, l’utente/cliente incorre in un procedimento civile e penale con richiesta di risarcimento danni. La concessione di licenza deve essere autorizzata espressamente e per iscritto dalla DA. La DA può modificare il sito web in qualsiasi momento senza preavviso.</p> 
    <h2>&sect; 7 Responsabilità, hyperlinks</h2> 
    <p>La DA non è tenuta in alcun momento a garantire o a ripristinare l’accesso al sito, il sito web può essere disattivato in qualsiasi momento senza preavviso, anche a tempo indeterminato. Non si assume <strong>alcuna responsabilità</strong> per danni causati da hardware o software difettosi o da azioni di terzi (anche in caso di negligenza) o per danni di qualsiasi tipo conseguenti all’accesso (o all’impossibilità di accedere). Rimane salvo il rimborso di parte dei costi delle inserzioni in caso di impossibilità di accesso per un periodo prolungato per sola responsabilità della DA (appositamente o per colpa grave); l’impossibilità di accesso dovuta a manutenzione del sito non da diritto a rimborsi. Salvo ai clienti inserzionisti la DA non fornisce al alcun supporto utente.</p> 
    <p>Il sito web contiene link a siti internet esterni (cosiddetti „hyperlinks“). Inserendo questi link la DA non si appropria né dei siti internet, né dei loro contenuti. Per i contenuti dei siti nei link sono responsabili solo ed esclusivamente i rispettivi offerenti. Per contenuti sconosciuti non si fornisce alcuna garanzia, neanche per danni conseguenti e per la disponibilità dei siti linkati.</p> 
    <h2>&sect; 8 Scelta giuridica, foro competente</h2> 
    <p>Queste CGC e i relativi rapporti giuridici sottostanno al diritto svizzero. Il foro competente è Zurigo.</p> 
    <p><em>June 2006</em>, ci si riserva il diritto di apportare modifiche in ogni momento.</p> 
    <p>DA Products GmbH<br /> 
    Forchstrasse 113a<br /> 
    8127 Forch<br /> 
    Svizzera</p> 
    <p><a href='mailto:info@daproducts.ch'>info@daproducts.ch</a></p> 
"; 
$LNG['terni'] = "Terni"; 
$LNG['text_of_message'] = "Il testo del messaggio"; 
$LNG['thank_you'] = "Grazie"; 
$LNG['thanks_for_registration'] = "Grazie per la registrazione! Per finire, controlla la tua posta elettronica e segui le indicazioni descritte."; 
$LNG['third_party_escort_reviews'] = "Recensioni su ragazze di altri siti"; 
$LNG['thursday'] = "Giovedì"; 
$LNG['time'] = "Ora"; 
$LNG['to'] = "a"; 
$LNG['to_change_use_menu'] = "Per cambiare i tuoi dati e le varie opzioni, usa il menu privato sulla parte superiore."; 
$LNG['today_new'] = "Today new"; 
$LNG['top20'] = "La ragazza del mese"; 
$LNG['top20_desc'] = "Vota per la tua ragazza preferita, e aiutala ad essere la Ragazza del Mese! Ogni fine del mese la ragazza con il maggior numero di voti diventa la nuova Ragazza del Mese! 
<br /><br /> 
Vota ORA! I voti sono da 1 (peggiore) a 10 (migliore) ! 
"; 
$LNG['top_10_ladies'] = "Le ragazze top 10"; 
$LNG['top_10_reviewers'] = "I top 10 delle recensioni"; 
$LNG['tour_add'] = "Aggiungi tour"; 
$LNG['tour_already'] = "Hai già un tour in questo periodo."; 
$LNG['tour_contact'] = "Contatto tour"; 
$LNG['tour_duration'] = "Durata del tour"; 
$LNG['tour_email'] = "E-mail tour"; 
$LNG['tour_empty_country'] = "Il campo paese non puo essere vuoto"; 
$LNG['tour_empty_from_date'] = "Il campo \"da\" non puo essere vuoto"; 
$LNG['tour_empty_to_date'] = "Il campo \"a\" non puo essere vuoto"; 
$LNG['tour_girl'] = "Ragazza in tour"; 
$LNG['tour_location'] = "Luogo del tour"; 
$LNG['tour_lower_date'] = "La data del ritorno non puo essere antecedente alla partenza. To Date can't be lower than From date"; 
$LNG['tour_no_escorts'] = "Prima aggiungi profilo escort "; 
$LNG['tour_phone'] = "Telefono tour"; 
$LNG['tour_saved'] = "La data del tour è stata salvata"; 
$LNG['tour_update'] = "Aggiorna tour"; 
$LNG['tour_updated'] = "La data del tour è stata aggiornata"; 
$LNG['tours'] = "City tours"; 
$LNG['trans'] = "Transessuali"; 
$LNG['trial'] = "Prova"; 
$LNG['trial_mb_desc'] = " 
<p>Ti è stato regalato un periodo di prova gratuito su ".NGE_SITE_TITLE.". In questo periodo potrai utilizzare le seguenti funzioni:</p> 
<ul> 
    <li>profilo sulla pagina principale e sulla pagina di una città da te scelta</li> 
    <li>gli utenti potranno aggiungere recensioni su di te</li> 
    <li>partecipazione nella classifica Top10 delle recensioni e nel concorso Ragazza del Mese</li> 
    <li>partecipazione nella rotazione dei profile, così il tuo profile non sarà mai nelle ultime pagine</li> 
</ul> 
<p>e Più !</p> 
"; 
$LNG['trial_mb_desc2'] = " 
<p> 
Se vuoi rimanere inserzionista, dopo il periodo di prova sarà necessario comprare un Premium Spot che a parte dei vantaggi sovrascritti, ti consenterà di usare una funzione in più! 
<ul> 
    <li>Potrai usare la funzione City Tour e aprire numerose strumenti pubblicitari</li> 
    <li><strong style='color: red;'>Free printed advertisment in the Redmag 
magazine in Italy</strong> (<a 
href='http://www.redmag.it/'>www.redmag.it</a>)</li> 
</ul> 
<p><a href='mailto:".NGE_CONTACT_EMAIL."'>Clicca qui per ordinare!</a></p> 
"; 
$LNG['trial_mb_desc_ag'] = "%COUNT% escorts (1st expiring in %DAYS% days)"; 
$LNG['trustability'] = "Attendibilità"; 
$LNG['trusted_review_popup'] = " 
    <p>Se tu ci fornisci le informazioni dettagliati sull’incontro, noi le inoltriamo direttamente alla ragazza. La ragazza non può vedere la recensione, quindi lei non sa se la tua recensione è positiva o negativa. Dopo che lei avrà confermato che ti ha incontrato, la recensione apparerà come veritiera per 100 %.</p> 
    <p>Per favore dacci qualche informazione su di te e sul posto dove hai incontrato la ragazza! Nota che solo la ragazza in questione riceverà questa informazione per sms o e-mail. Tutti questi dati non vengono memorizzati sul nostro sito. Per esempio se hai prenotato con il tuo nome o con nome falso, scrivi sempre quello e la data /ora esatta, se hai prenotato via e-mail scrivi il tuo indirizzo e-mail, etc.</p> 
    <p>Esempi.:</p> 
    <ol> 
        <li>gianni escortforum@hotmail.com, Lunedì 17 Genn. 2005 20:00</li> 
        <li>pinky 333 333 454, Mercoledì 16. Feb. 05 verso le 08:00 hotel Ibis a Milano</li> 
    </ol>"; 
$LNG['trusted_user_mail_subject'] = " Membro fidato dell' %SITE% "; 
$LNG['trusted_user_mail_text'] = "<pre> 
Gentile %NAME%, 
Grazie per aver postato sul %SITE% ! 
Ormai sei diventato un utente \"fidato\" e non è più necessaria l'approvazione dell'admin per i tuoi post. 
Benvenuto nella nostra communità e ti auguriamo buon divertimento! 
%SITE% Team. 
</pre>"; 
$LNG['tuesday'] = "Martedì"; 
$LNG['tuscany'] = "Tuscany"; 
$LNG['type'] = "Type"; 
$LNG['ugly'] = "Brutta"; 
$LNG['unfriendly'] = "Poco amichevole"; 
$LNG['unsubscribe'] = "Disiscrizione"; 
$LNG['upcoming_tours'] = "Prossimi Tours"; 
$LNG['update_languages'] = "Aggiorna le lingue"; 
$LNG['update_locations'] = "Aggiorna le città"; 
$LNG['update_rates'] = "Aggiorna i prezzi"; 
$LNG['upg_esc_memb'] = "Fare l’upgrade dell’iscrizione"; 
$LNG['upg_esc_memb_desc'] = "Grazie per aver deciso di passare all’iscrizione su ".NGE_SITE_TITLE."  !<BR /> 
Per favore scegli il tipo e la durata di iscrizione che vorresti comprare e clicca sul tasto \"<B> FARE L’UPGRADE ADESSO</B>\"  per essere re-indirizzato al provider di pagamento:"; 
$LNG['upgrade_membership'] = "Passa al Premium membership"; 
$LNG['upgrade_now'] = "FARE L’UPGRADE ADESSO!"; 
$LNG['upload_failed'] = "Il caricamento della foto non è riuscito."; 
$LNG['upload_picture'] = "Carica la foto"; 
$LNG['upload_private_photo'] = "Caricare foto private"; 
$LNG['url'] = "URL"; 
$LNG['user'] = "Utente"; 
$LNG['username'] = "Username"; 
$LNG['users_match'] = "Utenti trovati:"; 
$LNG['vacation_add'] = "Aggiungi vacanza"; 
$LNG['vacation_end'] = "La vacanza finisce"; 
$LNG['vacation_no_escorts'] = "Per favore, agguingi prima il profilo della ragazza "; 
$LNG['vacation_start'] = "La vacanza inizia"; 
$LNG['vacations'] = "Vacanze"; 
$LNG['vacations_active'] = "%NAME% e in vacanza <strong>dal %FROM_DATE% al %TO_DATE%</strong>"; 
$LNG['vacations_active_soon'] = "%NAME% e in vacanza, torna <strong>fra poco</strong>"; 
$LNG['vacations_currently_date'] = "La ragazza e attualmente in vacanza, torna il %DATE%."; 
$LNG['vacations_currently_soon'] = "La ragazza e attualmente in vacanza, torna <strong>fra poco</strong>."; 
$LNG['vacations_empty_to_date'] = "'La vacanza finisce' - questo campo non puo essere vuoto"; 
$LNG['invalid_vacation_dates'] = "Vacation dates are not valid"; 
$LNG['vacations_lower_date'] = "La data del ritorno non puo essere antecedente della data dell'inizio della vacanza"; 
$LNG['vacations_return'] = "Tornata dalle ferie"; 
$LNG['verification_email'] = "Caro %USER%, grazie per la registrazione. Per confermare la tua registrazione clicca su questo link:"; 
$LNG['verification_email_subject'] = NGE_SITE_TITLE." verifica del cliente"; 
$LNG['very_simple'] = "Molto semplice"; 
$LNG['view_gallery'] = "Vedi la galleria"; 
$LNG['view_my_private_photos'] = "Visualizza le mie foto private"; 
$LNG['view_my_profile'] = "Vedi il mio profilo"; 
$LNG['view_my_public_photos'] = "Visualizza le mie foto pubbliche"; 
$LNG['view_profile'] = "Vedi il profilo"; 
$LNG['view_reviews'] = "Aggiungi/Vedi le recensioni"; 
$LNG['view_site_escort_reviews'] = "Visulizza le recensioni sulle ragazze del %SITE% "; 
$LNG['view_third_party_escort_reviews'] = "Visualizza le recensioni delle ragazze di altri siti"; 
$LNG['vip'] = "VIP"; 
$LNG['vip_mb_desc_ag'] = "%COUNT% escorts"; 
$LNG['vote'] = "Voto"; 
$LNG['vote_her_looks'] = "Votala"; 
$LNG['votes'] = "Voti"; 
$LNG['waist'] = "Vita"; 
$LNG['watch_list'] = "Lista dei preferiti"; 
$LNG['web'] = "Sito internet"; 
$LNG['wednesday'] = "Mercoledì"; 
$LNG['week'] = "Settimana"; 
$LNG['weeks'] = "Settimane"; 
$LNG['weight'] = "Peso"; 
$LNG['welcome'] = "Benvenuto ".NGE_SITE_TITLE; 
$LNG['whats_new'] = "Quali sono le novità ?"; 
$LNG['when_you_met_her'] = "Quando l’hai incontrata"; 
$LNG['when_you_met_her_desc'] = "data e ora"; 
$LNG['when_you_met_lady'] = "Quando hai incontrato la ragazza"; 
$LNG['where_did_you_meet_her'] = "Dove l’hai incontrata?"; 
$LNG['where_you_met_her'] = "Dove l’hai incontrata"; 
$LNG['where_you_met_her_desc'] = "hotel, casa sua, casa tua"; 
$LNG['white'] = "Bianco"; 
$LNG['whole'] = "Tutta l'"; 
$LNG['wlc_city'] = "Dove lavori (città)"; 
$LNG['wlc_country'] = "Dove lavori (Città)"; 
$LNG['women'] = "Donne"; 
$LNG['working_locations'] = "Città di lavoro"; 
$LNG['working_time'] = "Orario di lovoro"; 
$LNG['write_here_name_of_site'] = "Per favore scrivi qui il nome del sito se non è il sito proprio della ragazza! Attenzione: scrivi solo siti di pubblicità, grazie! Cioè www.saraescort.com NO!"; 
$LNG['wrong_login'] = "Login  non corretto"; 
$LNG['wrong_login_or_password'] = "Username o password non sono corretti"; 
$LNG['wrong_time_interval'] = "Wrong time interval for working time on %DAY%."; 
$LNG['year'] = "Anno"; 
$LNG['years'] = "Anni"; 
$LNG['yellow'] = "Giallo"; 
$LNG['yes'] = "Si"; 
$LNG['yes_more_times'] = "Si, più volte"; 
$LNG['you_are_premium'] = "Sei un Membro Premium"; 
$LNG['you_have_log'] = "Devi esser loggato per usare questa funzione!. <a href='%LINK%'>Clicca qui per fare login</a> or <a href='%LINK2%'>Registrati ora.</a>"; 
$LNG['you_have_voted_for'] = "Hai votato per %ESCORT%! Grazie"; 
$LNG['you_r_watching_agencies'] = "Queste <b>agenzie</b> sono nella tua lista dei preferiti:"; 
$LNG['you_r_watching_escorts'] = "Queste <b>escort</b> sono nella tua lista dei preferiti:"; 
$LNG['you_r_watching_users'] = "Questi <b>utenti</b> sono nella tua lista dei preferiti:"; 
$LNG['your_booking_text'] = "Il testo della prenotazione"; 
$LNG['your_data'] = "I tuoi dati"; 
$LNG['your_discount'] = "Il tuo sconto: %DISCOUNT%%"; 
$LNG['your_full_name'] = "Il tuo nome e cognome"; 
$LNG['your_info'] = "Le tue informazioni"; 
$LNG['your_info_desc'] = "e-mail, numero di telefono, nome"; 
$LNG['your_login_name'] = "Username"; 
$LNG['your_name'] = "Il tuo nome"; 
$LNG['your_report'] = "Il tuo report"; 
$LNG['zip'] = "Codice postale"; 
$LNG['69'] = "69"; 
// 2006/12/11 
$LNG['tours_in_other_countries'] = "Tours in altre nazioni"; 
$LNG['brunette'] = "Brunette"; 
$LNG['hazel'] = "Hazel"; 
$LNG['profile_or_main_page_links_only'] = "Add links only to your profile/ main webpage of  your agency or to your personal website."; 
$LNG['competitors_links_will_be_deleted'] = "All third-party advertising/ competitors links will be deleted by administrator!"; 
$LNG['domain_blacklisted'] = "Domain blacklisted"; 
$LNG['phone_blocked'] = "Phone number is blocked"; 
$LNG['independent_girls'] = "Ragazze indipendenti"; 
$LNG['agency_girls'] = "Agenzie ragazze"; 
$LNG['girls'] = "Ragazze"; 
$LNG['inform_about_changes'] = "Grazie mille. Ti informeremo su tutti i cambiamenti per l'accesso ad " . NGE_SITE_TITLE; 
$LNG['escort_photos_over_18'] = "Tutte le accompagnatrici avevano piů di 18 anni al momento dello scatto fotografico."; 
$LNG['captcha_please_enter'] = "Per favore inserire il codice"; 
$LNG['captcha_here'] = "in questo riquadro"; 
$LNG['captcha_verification_failed'] = "Verificazione test fallita."; 
$LNG['my_awards'] = "My Awards"; 
$LNG['escort_girls'] = "Escort Girls"; 
$LNG['locally_available_in_countries'] = "Siamo anche disponibili in queste altre nazioni"; 
$LNG['domina'] = "Domina"; 
$LNG['dominas'] = "Dominas"; 
$LNG['club'] = "Club"; 
$LNG['club_name'] = "Nome dell'club"; 
$LNG['club_profile'] = "Profilo dell’club"; 
$LNG['studio'] = "Studio"; 
$LNG['studio_girls'] = "Studio Girls"; 
$LNG['all_regions'] = "All regions..."; 
$LNG['see_all_escorts_of_this_agency'] = "See all escorts of this agency"; 
$LNG['see_all_escorts_of_this_club'] = "See all escorts of this club"; 
$LNG['invalid_height'] = "Altezza non è valida"; 
$LNG['invalid_weight'] = "Peso non è valida"; 
$LNG['subscribe_to_newsletter'] = "Subscribe to our newsletter!"; 
$LNG['inform_every_week'] = "We will inform you every week about escorts on " . NGE_SITE_TITLE; 
$LNG['clubs_match'] = "Clubs matching criteria"; 
$LNG['advertise_with_us'] = "Pubblicizza con noi"; 
switch (APPLICATION_ID) 
{ 
    case 1: // escortforumit.com 
        $LNG['index_bottom_text'] = "Da noi trovi le piu belle accompagnatrici, escort, girl, annunci, di tutta Italia, inoltre abbiamo annunci di escort, girl, accompagnatrici, nelle maggiori città d'Italia Roma, Milano, Torino, Bologna, Napoli, Firenze! Cosa aspetti contatta le accompagnatrici piu belle che non hai mai trovatoa Roma, Bologna, Firenze, Napoli, Milano, Torino, e in tutte le altre città d'Italia. Troverai sempre gli annunci, escort, girl e le accompagnatrici piu belle ditutta Italia sempre aggiornate."; 
        break; 
    case 2: // escort-annonce.com 
        $LNG['index_bottom_text'] = "Bienvenue dans Escort annonce, le plus grand annuaire d'annonces de rencontres d'Escorts girl /boys/ trans en France et en Europe. Vous trouverez des Escort à Paris Toulouse Lyon Marseille et toutes les grandes villes de France où vous pourrez faire une rencontre sexe entre adultes consentants. Vous pourrez aussi prendre connaissance des appréciations sur les Escort girl rencontrées par les autres membres, et aussi le tchat pour voir en direct via la webcam l'Escort girl de votre choix avant de la rencontrer. Pour une rencontre furtive d'une Escort girl après le bureau, Escort annonce est le site où vous pourrez revenir plusieurs fois par jour pour voir les nouvelles girls inscrites près de chez vous pour des rencontres les plus chaudes. Si vous rencontrez ou voyez une annonce d'escort qui ne correspond pas à la réalité, n'hésitez pas nous contacter, nous ferons notre possible pour n'avoir que des vraies annonces pour faire de vraies rencontres coquines."; 
        break; 
    case 3: // hellasescorts.com 
        $LNG['index_bottom_text'] = "Στο " . NGE_SITE_TITLE . " θα βρείτε callgirl και τις πιο καυτές και σέξυ συνοδούς & από την Αθήνα, συνοδούς από �?εσσαλονίκη, συνοδούς από Πάτρα και όλες τις μεγάλες πόλεις της Ελλάδας."; 
        break; 
         
    case 4: // escortchicas.com 
        $LNG['welcome_description'] = "We are the largest Network for erotic contacts in Europe! Find today your escapade, your companionship, your sexual adventure – in Spain and Europe! We update our site daily. All girls with pictures and contact details! You can register for FREE and benefit from many advantages! If you have any question or uncertainity, please contact us directly: <a href='mailto:" . NGE_CONTACT_EMAIL . "'>" . NGE_CONTACT_EMAIL . "</a>"; 
        $LNG['index_bottom_text'] = "Estàs buscando relax en barcelona o un relax en madrid? En nuestro portal tenemos contactos escorts en todas las ciudades de Espana, relax en valencia, relax en sevilla o en las islas tambien tenemos anuncios de relax en mallora, ibizao las islas canarias. Diferentes anuncios escorts de agencias, escorts independenties y travestis. Si no buscas los que quieres encontrar en otros portales aqui lo encuentraras seguramente. Mira los anuncios y encuentra en tu ciudad las mas calientes mujeres."; 
        break; 
    case 6: // sexindex.ch 
        $LNG['welcome_description'] = "dem grössten Netzwerk für erotische Kontakte, Sex Clubs und Escort in Europa! Auf der Suche nach einem Bordell, einer Ferienbegleitung, einem Seitensprung, einem Escort Girl oder guten Sex Clubs? Bei uns werden Sie fündig - täglich aktuelle Kontaktanzeigen (Setcards) mit Fotos und Bewertungen. Melden Sie sich GRATIS an und profitieren Sie von vielen Vorteilen und Vergünstigungen! Bei Fragen oder Problemen wenden Sie sich bitte direkt an: <a href='mailto:" . NGE_CONTACT_EMAIL . "'>" . NGE_CONTACT_EMAIL . "</a>"; 
        $LNG['advertise_now'] = "<a href='" . NGE_URL . "/advertising.php' rel='nofollow'>Werben Sie gratis auf Sexindex.ch!</a> Auskunft unter <a href='mailto:" . NGE_CONTACT_EMAIL . "'>" . NGE_CONTACT_EMAIL . "</a> oder Telefon 078 899 14 89"; 
        $LNG['autumn_action'] = " 
        <div style='overflow: hidden; width: 755px; padding: 5px 10px; font-size: 10px; line-height: 1.1; background: #f5f5f5; border: 1px solid #ccc;'> 
            <div style='float: left; width: 360px;'> 
                <p><big>Sexindex.ch Herbst Aktion<br /> 
                <span style='color: #DA1E05;'>Werbung die sich auszahlt – über 120,000 Besucher pro Monat</span></big></p> 
                <p>Profitieren Sie von unserer Aktion und schalten Sie noch heute Ihre Premium Sexanzeige<br /> 
                (Premium = Topplatzierung, immer an oberster Stelle gelistet).</p> 
                <p style='color: #DA1E05;'>15 TAGE PREMIUM INSERAT CHF 100.00<br /> 
                30 TAGE PREMIUM INSERAT CHF 200.00</p> 
            </div> 
            <div style='margin: 0 0 0 385px;'> 
                <p>Es geht ganz einfach. Senden Sie uns folgende Angaben per <a href='mailto:" . NGE_CONTACT_EMAIL . "'>Email</a> zu und Ihre Sedcard wird umgehend aufgeschaltet:</p> 
                <ul> 
                    <li>Dauer des Inserates</li> 
                    <li>Ihre Fotos und Kontaktdaten (Telefon und/oder E-Mail)</li> 
                    <li>Gewünschter Name und Inseratetext</li> 
                    <li>Ihr Arbeitsort / Stadt</li> 
                    <li>Ihre Webseite (URL) (optional)</li> 
                </ul> 
                <p>Für mehr Informationen oder Fragen zu diesem Angebot oder zur normalen Gratiswerbung auf Sexindex.ch kontaktieren Sie uns wie gewohnt per <a href='mailto:" . NGE_CONTACT_EMAIL . "'>Email</a> oder Telefon 078 899 14 89.<br /> 
                Das Inkasso erfolgt nachträglich (bar oder Banküberweisung).</p> 
            </div> 
        </div> 
        "; 
        $LNG['escort_girls_description'] = "All diese attraktiven Ladies besuchen Sie zu Hause, im Büro oder im Hotel. Oder sind Sie auf der Suche nach einer Ferienbegleitung? Swingerclubbegleitung? Hier werden Sie sicher fündig! Täglich aktuell."; 
        $LNG['independent_girls_description'] = "Auf der Suche nach einem ganz diskreten Abenteuer? Hier inserieren Girls die Sie in absolut privater Atmosphäre empfangen – entweder in einer Privatwohnung oder einem kleinen Salon ohne Bar/Empfang. Diskret und privat!"; 
        $LNG['studio_girls_description'] = "Täglich aktuell finden Sie hier die heissesten Girls aus den besten Etablissements der ganzen Schweiz."; 
        $LNG['index_bottom_text'] = "Gemäss Statistik sind Begriffe die etwas mit Sex und Erotik zu tun haben die meist gesuchten im Internet. 98 Prozent der Männer waren gemäss Umfragen schon mindestens einmal auf Pornoseiten. Beliebt sind auch Suchbegriffe wie Kontaktanzeigen, Bordell, Sex Clubs, Escort, ficken, Transen etc. In der Schweiz gibt es eine Vielzahl an Sex Seiten und Sexführern. Und manchmal ist es schwierig den Ueberblick zu behalten. Denn wirklich gute Sex-Angebote, wo man auch das findet was man sucht, sind rar. Oder ist es Ihnen nicht auch schon so ergangen? Sie tippen bei einer Suchmaschine den Begriff „Bordell“ ein und landen, statt auf einem Sexführer, auf einer kostenpflichtigen Seite mit Kontaktanzeigen? Oder Sie haben Lust auf schnellen Sex und suchen ein Bordell in Ihrer Nähe wo Sie richtig geil ficken können? Statt dessen werden Sie auf eine Seite geführt wo Sie nur ein kleines Sexangebot haben und wenige Escort Girls / Sex Clubs aufgeführt sind. Wie gesagt – es ist ziemlich schwierig. Einige Seiten haben sich etabliert und decken gewisse Sparten ab. Für Transen Kontakte zum Beispiel eignet sich das Portal happysex hervorragend. Denn happysex hat in den meisten Regionen der Schweiz Kontaktanzeigen bzw. Sexanzeigen von Transen. Auch Bordelle, Escort Girls und Sex Clubs werden aufgeführt. Jedoch nimmt happysex im direkten Vergleich zu anderen Sex Seiten vom Design und Aufbau her einer der hinteren Plätze ein. Bei uns auf Sexindex legen wir wert auf Aktualität der Sex Kontakt Anzeigen und Sedcards der Escort Girls. Wir nehmen regelmässig neue Bordelle, Sexstudios, private Frauen, Escorts und Salons in unsere Datenbank auf. Alle Sedcards sind mit Fotos und Details und sind einheitlich präsentiert. Auch die Werbung versuchen wir dezent zu halten – jedoch brauchen wir die Banner um für Sie weiterhin ein gratis Sexführer zu bleiben. In diesem Sinne – ficken ist die schönste Nebensache der Welt – und ob Sie auf sexindex oder happysex fündig werden spielt schlussendlich keine Rolle. Happy hunting!"; 
        break; 
} 
$LNG['escort_girls_in_city'] = "Escort Girls in %CITY%"; 
$LNG['independent_girls_in_city'] = "Private Girls in %CITY%"; 
$LNG['studio_girls_in_city'] = "Studio Girls in %CITY%"; 
$LNG['new_arrivals_in_city'] = "New Girls in %CITY%"; 
$LNG['all_girls_in_city'] = "All girls in %CITY%"; 
$LNG['see_all_girls_from_agency'] = "click to see all girls from this agency"; 
$LNG['see_all_independent_girls'] = "click to see all independent girls"; 
$LNG['last_modified'] = "Ultima modifica"; 
$LNG['advertise_with_us'] = "Pubblicizza con noi"; 
$LNG['nearest_cities_to_you'] = "Le citta piu vicine a Te"; 
$LNG['nearest_cities_to_city'] = "Le citta piu vicine a %CITY%"; 
$LNG['cities_in_region'] = "Cities in %REGION%"; 
$LNG['submit_form_to_signup'] = "Submit this form to signup and get Promoted from largest escort directory!"; 
$LNG['signup_now_to_largest_escort_directory'] = "Iscriviti adesso <span class='pink'>GRATIS</span> nel più grande archivio escort del mondo. Abbiamo attualmente oltre <span class='pink'>3 milioni di clienti</span> mensili all'interno del nostro network. Inizia ad usare adesso la nostra popolaritŕ per promuoverti <span class='pink'>GRATIS!</span>"; 
$LNG['signing_on_all_local_sites'] = "Subito dopo l'iscrizione, il tuo profilo verrà automaticamente attivato su tutti i nostri siti. I tuoi dati di accesso, saranno uguali su tutti i siti del nostro network, dovrai solo fare accesso al sito che ti interessa."; 
$LNG['please_choose_your_business'] = "Per favore seleziona la tua categoria"; 
$LNG['independent_privat'] = "Escort indipendente"; 
$LNG['escort_agency'] = "Agenzia Escort"; 
$LNG['club_bordell_strip'] = "Club / Casa chiusa / Sauna club / Stripclub, Studio di massaggi"; 
$LNG['please_fill_form'] = "Per favore compila il modulo qui di seguito"; 
$LNG['please_write_numbers_from_field'] = "Per favore scrivi qui accanto esattamente i numeri che vedi nel riquadro"; 
$LNG['just_moment_away_from_escort_directory'] = "You are just moment away from having access to largest escort directory!"; 
$LNG['local'] = "Local"; 
$LNG['local_directory'] = "Club/Escort listing"; 
$LNG['exchange_banners'] = "If do you want interchange banners and/or links with us, email us to <a href='mailto:" . NGE_CONTACT_EMAIL . "'>" . NGE_CONTACT_EMAIL . "</a>"; 
$LNG['our_network_in_europe'] = "Our network in Europe"; 
$LNG['agencies_and_independent_in_spain'] = "Agencies and independent girls in Spain"; 
$LNG['directories_and_guide_in_spain'] = "Directories, forum and erotic guide in Spain"; 
$LNG['friends'] = "Friends"; 
$LNG['escort_service'] = "Servizio Escort"; 
$LNG['studio_and_private_girls'] = "Studio ed escort Private"; 
$LNG['advertising'] = "Pubblicità"; 
$LNG['premium_girls'] = "Premium girls"; 
$LNG['submit_form_to_signup'] = "Submit this form to signup and get Promoted from largest escort directory!"; 
$LNG['signup_now_to_largest_escort_directory'] = "Signup now for <span class='pink'>FREE</span> to the largest escort directory worldwide. We currently have over <span class='pink'>3 million clients</span> monthly on our network. Start using our popularity to promote yourself now <span class='pink'>FOR FREE!</span>"; 
$LNG['signing_on_all_local_sites'] = "By signing up, your profile will automatically be displayed on all these local country sites. Your login data will work on all the sites - you just have to signup once."; 
$LNG['please_choose_your_business'] = "Please choose your business"; 
$LNG['independent_privat'] = "Independent Escort / Privat Girl"; 
$LNG['escort_agency'] = "Escort Agency"; 
$LNG['club_bordell_strip'] = "Club / Bordell / Sauna Club / Stripclub (Tabledance), Massage Studio"; 
$LNG['please_fill_form'] = "Per favore compila il segeunte modulo"; 
$LNG['please_write_numbers_from_field'] = "Please write the numbers like in the field"; 
$LNG['just_moment_away_from_escort_directory'] = "You are just moment away from having access to largest escort directory!"; 
$LNG['choose_your_region'] = "Choose your region"; 
$LNG['setcards_online'] = "%COUNT% setcards online"; 
$LNG['only_escorts'] = "only escorts"; 
$LNG['only_private_girls'] = "only private girls"; 
$LNG['only_studio_girls'] = "only studio girls"; 
$LNG['most_popular_cities'] = "La città più viste"; 
$LNG['renew_your_profile'] = "Il suo annuncio è scaduto e quindi non può più effettuare accesso al suo profilo per effettuare delle modifiche. Se lei vuole può contattare il suo Sales di riferimento per effettuare il rinnovo o scrivere una mail ad <a href='mailto:" . NGE_CONTACT_EMAIL . "'>" . NGE_CONTACT_EMAIL . "</a>"; 
$LNG['relax'] = "Relax"; 
$LNG['email_unsubscribed'] = "E-mail %EMAIL% is removed from " . NGE_SITE_TITLE . " newsletter."; 
$LNG['vip_girls'] = "Ragazze VIP"; 
$LNG['crop_photo'] = "Crop photo"; 
$LNG['show_thumbnails_on'] = "Show thumbnails on"; 
$LNG['photo_was_cropped'] = "Photo was cropped."; 
$LNG['crop_feature_teaser'] = " 
    <p><strong style='color: red;'>ATTENZIONE:</strong><br /> 
    Abbiamo appena rilasciato una nuova importante funzionalità. Tutte le foto che troverete ora in Home Page e nella directory internazionale, hanno la stessa dimensione. Per questo però ti chiediamo di controllare se la tua foto viene visualizzata correttamente. Entra quindi per favore nel tuo account, seleziona &quot;modifica foto&quot; e segui le istruzioni.</p> 
"; 
$LNG['crop_feature_description'] = " 
    <p><strong style='color: red;'>ATTENZIONE:</strong><br /> 
    tutte le foto PRINCIPALI che mostriamo sulla home page del sito devono avere le stesse dimensioni. Questo per offrire a tutti coloro che navigano sul sito un prodotto professionale e piacevole da guardare.</p> 
    <p>Cosa devi fare:</p> 
    <ol> 
        <li>Controlla la tua foto principale, quella che mostriamo in home page. Se ti sembra che tutto vada bene, non andare oltre, altrimenti guarda il punto 2).</li> 
        <li>Clicca su &quot;Crop Photo&quot; (ti permetterà di adattare la tua foto alle nostre specifiche)</li> 
        <li>Ora dovresti vedere un riquadro che si sovrappone alla tua foto. Puoi muoverlo e quello che sta all’interno verrà poi pubblicato. Seleziona quindi la parte della tua foto che vuoi pubblicare e clicca su &quot;Crop Image&quot; 
        <li>A questo punto hai finito. Potrai controllare l’immagine sempre dal tuo account e modificarla quante volte vuoi fino a quando non ti andrà bene.</li> 
    </ol> 
"; 
$LNG['temp_announce'] = 'Il nostro nuovo dominio è escortforumit.xxx';
$LNG['we_only_sell_advertisement'] = NGE_SITE_TITLE . " è un sistema di ricerca di informazioni e di annunci pubblicitari e non ha nessuna connessione o nessuna responsabilità con qualsiasi sito o informazioni individuali menzionati qui. Noi vendiamo SOLAMENTE spazi pubblicitari, non siamo nè una agenzia di escort, nè siamo in alcun modo coinvolti in affari con le accompagnatrici e la prostituzione. Non ci assumiamo nessuna responsabilità per le azioni svolte da siti terzi o individuali, ai quali si ha accesso tramite links, e-mail o telefono presi all'interno di questo portale."; 
$LNG['user_messages_review_waiting_for_approval'] = "Your review about %MESSAGE% is waiting for verification. Please be patient"; 
$LNG['user_messages_favorite_girl_review'] = "Your favorite girl <strong>%MESSAGE%</strong> has new review. <a href='%LINK%'>click here to see</a>"; 
$LNG['user_messages_review_approval'] = "Your review <strong>%MESSAGE%</strong> was approved by administrator."; 
$LNG['user_messages_review_deleted'] = "Your review <strong>%MESSAGE%</strong>  was deleted by administrator. Reason given: %LINK%"; 
$LNG['user_messages_new_discount'] = "%MESSAGE% is offering you a special EF discount. <a href='%LINK%'>Check her out!</a>"; 
$LNG['user_messages_deleted_discount'] = "%MESSAGE% is not offering you a special EF discount anymore."; 
$LNG['user_messages_favorite_disabled'] = "Your favorite girl <strong>%MESSAGE%</strong> is no longer with us! We are sorry! Maybe time to find a new favorite?"; 
$LNG['user_messages_favorite_activated'] = "Your favorite girl <strong>%MESSAGE%</strong> joined us again!"; 
$LNG['user_messages_girl_of_month'] = "Girl of the month %MONTH%/%YEAR% is: <strong>%MESSAGE%</strong>!! Congratulation %MESSAGE%!!!"; 
$LNG['user_messages_free_fuck_winner'] = "<strong>You are the WINNER of the freefuck lottery contact us.</strong>"; 
$LNG['user_messages_free_fuck_others'] = "The winner of the free fuck lottery of month %MONTH%/%YEAR% is: <strong>%MESSAGE%</strong>!! Congratulation %MESSAGE%!!!"; 
$LNG['user_messages_favorite_region_girls'] = "New girls from your favourite region joined EF since your last visit. %LINK%"; 
$LNG['user_messages_favorite_region_vip'] = "There is new VIP %MESSAGE% from your favourite region since your last visit. %LINK%"; 
$LNG['user_email_review_approved'] = "Hello %TARGET_0%\nYour review %TARGET_1% was approved by administrator.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['escort_email_review_approved'] = "Hello %TARGET_0%\nReview %TARGET_1% about you was approved by administrator.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_favorites_review_approved'] = "Hello %TARGET_0%\nYour favorite girl  %TARGET_1% has new review.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['agency_email_review_approved'] = "Hello %TARGET_0%\nReview about %TARGET_1% was approved by administrator.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_review_approval_favorites'] = NGE_SITE_TITLE . " new review"; 
$LNG['user_email_subject_review_approval'] = NGE_SITE_TITLE . " review approved"; 
$LNG['escort_email_subject_review_approval'] = NGE_SITE_TITLE . " review approved"; 
$LNG['agency_email_subject_review_approval'] = NGE_SITE_TITLE . " review approved"; 
$LNG['escort_email_review_new'] = "Hello %TARGET_0%\nNew review %TARGET_1% about you is waiting for approval.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['agency_email_review_new'] = "Hello %TARGET_0%\nYour escort %TARGET_1% has new review waiting for approval.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_review_new'] = "Hello %TARGET_0%\nYour review about %TARGET_1% is waiting for verification. Please be patient.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_review_new'] = NGE_SITE_TITLE . " review waiting for approval"; 
$LNG['escort_email_subject_review_new'] = NGE_SITE_TITLE . " new review waiting for approval"; 
$LNG['agency_email_subject_review_new'] = NGE_SITE_TITLE . " new review waiting for approval"; 
$LNG['user_email_review_deleted'] = "Hello %TARGET_0%\nYour review <strong>%TARGET_1%</strong>  was deleted by administrator. Reason given: %TARGET_2%\n" . NGE_SITE_TITLE . " Team"; 
$LNG['escort_email_review_deleted']	= "Hello %TARGET_0%\nThe review <strong>%TARGET_1%</strong> about you was deleted by administrator. Reason given: %TARGET_2%\n" . NGE_SITE_TITLE . " Team"; 
$LNG['agency_email_review_deleted']	= "Hello %TARGET_0%\nThe review about <strong>%TARGET_1%</strong> was deleted by administrator. Reason given: %TARGET_2%\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_review_delete'] = NGE_SITE_TITLE . " review deleted"; 
$LNG['escort_email_subject_review_delete'] = NGE_SITE_TITLE . " review deleted"; 
$LNG['agency_email_subject_review_delete'] = NGE_SITE_TITLE . " review deleted"; 
$LNG['user_email_discount_new'] = "Hello %TARGET_0%\n%TARGET_1% is offering you a special EF discount.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['escort_email_discount_new'] = "Hello %TARGET_0%\nNew discount was assigned to you.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['agency_email_discount_new'] = "Hello %TARGET_0%\n%TARGET_1% is now offering a special EF discount.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_discount_new'] = NGE_SITE_TITLE . " new discount"; 
$LNG['escort_email_subject_discount_new'] = NGE_SITE_TITLE . " new discount"; 
$LNG['agency_email_subject_discount_new'] = NGE_SITE_TITLE . " new discount"; 
$LNG['user_email_discount_deleted']	= "Hello %TARGET_0%\nYour escort %TARGET_1% is not offering a special EF discount anymore.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_discount_deleted'] = NGE_SITE_TITLE . " discount expired"; 
$LNG['user_email_escort_deactivated'] = "Hello %TARGET_0%\nYour favorite girl <strong>%TARGET_1%</strong> is no longer with us! We are sorry! Maybe time to find a new favorite?\n" . NGE_SITE_TITLE . " Team"; 
$LNG['escort_email_escort_deactivated'] = "Hello %TARGET_0%\nYour account has been deactivated.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['agency_email_escort_deactivated'] = "Hello %TARGET_0%\nAccount of %TARGET_1% has been deactivated.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_favorites_deactivated'] = NGE_SITE_TITLE . " escort deactivated"; 
$LNG['escort_email_subject_favorites_deactivated'] = NGE_SITE_TITLE . " account deactivated"; 
$LNG['agency_email_subject_favorites_deactivated'] = NGE_SITE_TITLE . " account deactivated"; 
$LNG['user_email_escort_activated'] = "Hello %TARGET_0%\nYour favorite girl <strong>%TARGET_1%</strong> joined us again!\n" . NGE_SITE_TITLE . " Team"; 
$LNG['escort_email_escort_activated'] = "Hello %TARGET_0%\nYour account has been activated.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['agency_email_escort_activated'] = "Hello %TARGET_0%\nAccount of %TARGET_1% has been activated.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['agency_email_subject_favorites_activated'] = NGE_SITE_TITLE . " escort activated"; 
$LNG['agency_email_subject_favorites_activated'] = NGE_SITE_TITLE . " account activated"; 
$LNG['agency_email_subject_favorites_activated'] = NGE_SITE_TITLE . " account activated"; 
$LNG['user_email_girl_of_the_month'] = "Hello %TARGET_0%\nGirl of the month %TARGET_1% is: <strong>%TARGET_2%</strong>!! Congratulation %TARGET_2%!!!\n" . NGE_SITE_TITLE . " Team"; 
$LNG['escort_email_girl_of_the_month'] = "Hello %TARGET_0%\nGirl of the month %TARGET_1% is: <strong>%TARGET_2%</strong>!! Congratulation %TARGET_2%!!!\n" . NGE_SITE_TITLE . " Team"; 
$LNG['escort_email_girl_of_the_month_winner'] = "Hello %TARGET_0%\nYou are new girl of the month %TARGET_1%! Congratulation %TARGET_0%!!!\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_girl_of_the_month'] = NGE_SITE_TITLE . " girl of the month"; 
$LNG['escort_email_subject_girl_of_the_month'] = NGE_SITE_TITLE . " girl of the month"; 
$LNG['user_email_free_fuck_winner'] = "Hello %TARGET_0%\n<strong>You are the WINNER of the freefuck lottery contact us.</strong>\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_free_fuck_others'] = "Hello %TARGET_0%\nThe winner of the free fuck lottery of month %TARGET_1% is: <strong>%TARGET_2%</strong>!! Congratulation %TARGET_2%!!!\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_free_fuck_winner'] = NGE_SITE_TITLE . " freefuck lottery winner"; 
$LNG['user_email_subject_free_fuck_others'] = NGE_SITE_TITLE . " freefuck lottery"; 
$LNG['user_email_new_girls_in_region'] = "Hello %TARGET_0%\nNew girls from your favourite region joined EF.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_new_girls_in_region'] = NGE_SITE_TITLE . " new girls"; 
$LNG['user_email_new_vip_girls_in_region'] = "Hello %TARGET_0%\nThere is new VIP %TARGET_1% from your favourite region since your last visit. %TARGET_2%\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_new_vip_girls_in_region'] = NGE_SITE_TITLE . " new VIP girls"; 
$LNG['loft'] = 'Loft'; 
$LNG['loft_escorts_photo_limit_reached'] = "Loft escorts can have max %LIMIT% photos in gallery."; 
$LNG['loft_escorts_cant_go_on_tour'] = "Loft escorts can't go on tour."; 
//CUBIX Added By David 
$LNG['email_exists'] = "Email exists"; 
/* design2 texts */ 
$LNG['members_login'] = "Login utenti"; 
$LNG['signup_now'] = "Iscriviti adesso"; 
$LNG['live_now'] = "Live now"; 
$LNG['girl_has_video'] = "Girl has video"; 
$LNG['girl_has_reviews'] = "Girl has reviews"; 
$LNG['100%_real_photos'] = "100% real photos"; 
$LNG['happy_hour'] = "Happy hour"; 
$LNG['icons_explanation'] = "Icons explanation"; 
$LNG['offer_incall'] = "Offerte ricevo"; 
$LNG['offer_outcall'] = "Offerte in esterna"; 
$LNG['ebony_only'] = "Solo mulatte"; 
$LNG['asian_only'] = "Solo asiatiche"; 
$LNG['blondes_only'] = "Solo bionde"; 
$LNG['add_to_my_favourites'] = "Add to my favourites"; 
$LNG['remove_from_favourites'] = "Remove from favourites"; 
$LNG['add_view_reviews'] = "Add/View reviews"; 
$LNG['about_escort'] = "Su di %SHOWNAME%"; 
$LNG['contact_me'] = "Contact me"; 
$LNG['now_open'] = "Aperti adesso"; 
$LNG['quick_links'] = "Links rapidi"; 
$LNG['closed'] = "Closed"; 
$LNG['viewed'] = "Visto"; 
$LNG['free_signup'] = "Free signup"; 
$LNG['signup_profit'] = " 
<ul> 
    <li>aggiungi <strong>commenti</strong></li> 
    <li class='fsa-odd'>gestisci la lista <strong>favoriti</strong></li> 
    <li>scrivi <strong>recensioni</strong></li> 
    <li class='fsa-odd'>accedi alla <strong>ricerca avanzata</strong></li> 
    <li>list dei più <strong>visti</strong> </li> 
</ul> 
"; 
$LNG['sort_by'] = "In ordine"; 
$LNG['random'] = "Random"; 
$LNG['alphabetically'] = "Alfabetico"; 
$LNG['most_viewed'] = "I più visti"; 
$LNG['newest_first'] = "Ultimi arrivati"; 
$LNG['narrow_your_search'] = "Ricerca per"; 
$LNG['with_reviews'] = "With reviews"; 
$LNG['search_by_name_agency'] = "Ricerca per Nome/Agenzia"; 
$LNG['comments_responses'] = "Comments &amp; Responses"; 
$LNG['post_comment'] = "Post a text comment"; 
$LNG['back'] = "Back"; 
$LNG['reply'] = "Reply"; 
$LNG['spam'] = "Spam"; 
$LNG['only_registered_users_can_add_comments']	= "Only registered users can add comments. Please <a href='%VALUE%'>login here</a>."; 
$LNG['your_comment'] = "Your comment"; 
$LNG['close'] = "Close"; 
$LNG['message_waiting_for_approval'] = "Thank you. Your message is waiting for approval now."; 
$LNG['ago'] = 'ago'; 
$LNG['time_ago'] = '%TIME% ago'; 
$LNG['yesterday'] = "Yesterday"; 
$LNG['minute'] = "minute"; 
$LNG['spanking'] = "Spanking"; 
$LNG['domination'] = "Domination"; 
$LNG['fisting'] = "Fisting"; 
$LNG['massage'] = "Massage"; 
$LNG['role_play'] = "Role play"; 
$LNG['bdsm'] = "BDSM"; 
$LNG['hardsports'] = "Hardsports"; 
$LNG['hummilation'] = "Hummilation"; 
$LNG['rimming'] = "Rimming"; 
$LNG['less_than'] = "meno di %NUMBER%"; 
$LNG['more_than'] = "pi? di %NUMBER%";
$LNG['free_signup_advantages'] = "L'iscrizione su <span class='red'>" . NGE_SITE_TITLE . "</span> è assolutamente <span class='red-big'>GRATUITA </span> e porta molti vantaggi."; 
$LNG['reset'] = "Reset"; 
$LNG['private_area'] = "Private area"; 
$LNG['choose_your_choice'] = "Choose your choice"; 
$LNG['base_city'] = "Città"; 
$LNG['additional_cities'] = "Additional cities"; 
$LNG['zone'] = "Zone"; 
$LNG['actual_tour_data'] = "NOW in %CITY% (%COUNTRY%)"; 
$LNG['upcoming_tour_data'] = "in %DAYS% in %CITY% (%COUNTRY%), from %FROM_DATE% - %TO_DATE%"; 
$LNG['beta_warning'] = NGE_SITE_TITLE . " is in Beta phase. We are constantly working on the site and make regular updates and improvements."; 
$LNG['ethnic_white'] = "Caucasica"; 
$LNG['ethnic_black'] = "Nera"; 
$LNG['ethnic_asian'] = "Asiatica"; 
$LNG['ethnic_latin'] = "Latina"; 
$LNG['ethnic_african'] = "Africana"; 
$LNG['ethnic_indian'] = "Indiana"; 
$LNG['members_signup'] = "Iscrizione Utenti"; 
$LNG['click_here_your_signup'] = "Click here for your singup."; 
$LNG['your_email'] = "Your e-mail"; 
$LNG['missing_message'] = "Missing message"; 
$LNG['your_message_sent'] = "Your message was sent."; 
$LNG['signup'] = "Signup"; 
$LNG['signup_to_largest_escort_directory'] = "Signup now for <span class='red-big'>FREE</span> to the largest escort directory worldwide. We currently have over <span class='red-big'>3 million clients</span> monthly on our network. Start using our popularity to promote yourself now <span class='red-big'>FOR FREE</span>!"; 
$LNG['one_signup_all_sites'] = "By signing up, your profile will be automatically displayed on all these local country sites. Your login data will work on all the sites - you just have to signup once."; 
$LNG['email_verification_subject'] = NGE_SITE_TITLE . " account verification"; 
$LNG['email_verification_text'] = "<b>Dear %USER%,</b><br /><br /> 
<b>Thank you for registering at " . NGE_SITE_TITLE . "</b>.<br /><br /> 
You are 1 step away from activating your account, simply click on the link below or copy and paste it into your browser.<br /><br /> 
<b><a href='%LINK%' style='color: #dd0000;'>%LINK%</a></b><br /><br /> 
NOTE: <span style='font-size: 11px;'>if you experience any trouble, please contact us at <b><a href='mailto:" . NGE_CONTACT_EMAIL . "' style='color: #dd0000;'>" . NGE_CONTACT_EMAIL . "</a></b>.</span><br /><br /> 
<b>Yours faithfully</b><br /> 
<b>" . NGE_SITE_TITLE .  " Team</b>"; 
$LNG['email_confirmation_subject'] = NGE_SITE_TITLE . " registration"; 
$LNG['email_confirmation_text'] = "Dear %USER%,</b><br /><br /> 
Thank you for registering at " . NGE_SITE_TITLE . ".<br /> 
Your registration is now complete and you are an active member of " . NGE_SITE_TITLE . ".<br /><br /> 
<b>Following is your login information. Please keep this information on a safe place.<br /> 
<span style='display: block; float: left; width: 75px;'>Login:</span> <span style='color: #dd0000;'>%USER%</span><br /> 
<span style='display: block; float: left; width: 75px;'>Web:</span> <a href='" . NGE_URL . "' style='color: #dd0000;'>" . NGE_URL . "</a></b><br /><br /> 
Have fun on the site!<br /><br /> 
<b>Yours faithfully</b><br />  
<b>" . NGE_SITE_TITLE . " Team</b>"; 
//CUBIX Added By David 
$LNG['contact_us_text1'] = "In case if you wish to contact us regarding general enquiries, feedback or advertising, choose from the following options"; 
$LNG['phone_international'] = "Phone International"; 
$LNG['contact_us_text2'] = "Alternatively please use our contact form below"; 
$LNG['your_message'] = "Your Message"; 
$LNG['link_exschange'] = "Link Exchange"; 
$LNG['contact_us_text3'] = "If you would like to exchange links, please email the webmaster (e-mail).<br />Please copy our banner and place it on your server before contacting us regarding link exchange."; 
// 
//Cubix Added By Tiko 
$LNG['delete_profile'] = "Delete Profile"; 
$LNG['confirm_delete_profile'] = "Are you sure you want to delete the escort profile ?"; 
$LNG['delete_escort'] = "Delete escort profile"; 
$LNG['escort_profile_deleted'] = "Escort profile has been successfuly deleted"; 
$LNG['escort_profile_deleted_error'] = "Error: Escort profile has not been deleted"; 
$LNG['same_city'] = 'Working city and tour city can not be same.'; 
$LNG['overnight'] = 'overnight'; 
$LNG['directory_for_female_escorts'] = NGE_SITE_TITLE . ' is a UK escort directory for female escorts, escort agencies, independent escorts, trans and couples. 
On escortguide.co.uk you can find the perfect escort in London, Manchester, Leeds and all other areas in the United Kingdom. 
We update our escort directory on a daily basis - you will find new female escorts in london and from your area every day. 
If you are an escort agency or a female independent escort based in London or the UK, simply click on the 
signup link to register and upload your escort profile to escortguide.co.uk. '; 
//new search 
$LNG['kissing_no'] = "No Kissing"; 
$LNG['blowjob_no'] = "No Blowjob"; 
$LNG['cumshot_no'] = "No Cumshot"; 
$LNG['cumshot_in_mouth_spit'] = "Cumshot In Mouth Spit"; 
$LNG['cumshot_in_mouth_swallow'] = "Cumshot In Mouth Swallow"; 
$LNG['cumshot_in_face'] = "Cumshot In Face"; 
$LNG['with_comments'] = "with comments"; 
$LNG['nickname'] = "Nickname"; 
$LNG['without_reviews'] = "Without reviews"; 
$LNG['not_older_then'] = "Not older then"; 
$LNG['order_results'] = "Order results"; 
$LNG['select_your_girl_agency_requirements'] = "Select your girl/agency requirements"; 
$LNG['girls_details_bios'] = "Girls details/bio�s"; 
$LNG['cm'] = "cm"; 
$LNG['ft'] = "ft"; 
$LNG['kg'] = "kg"; 
$LNG['lb'] = "lb"; 
$LNG['girls_origin'] = "Girls origin"; 
$LNG['ethnicity'] = "Ethnicity"; 
$LNG['measurements'] = "Measurements"; 
$LNG['open_hours'] = "Open hours"; 
$LNG['my_favorites'] = "My Favorites";
$LNG['learn_more_webcam'] = "Se hai una webcam più un account di Skype o MSN, questo è il modo semplice per prendere una vostra foto e verificarla. Devi solo cliccare nel link „Richiesta sessione Webcam“ scegli una data ed un orario e attendete la nostra conferma. All’orario stabilito avrai un mini appuntamento (video chat) con uno dei nostri amministratori per verificare le foto. Successivamente questa sessione riceverete il 100% verificata.";
$LNG['learn_more_passport'] = "Se scegli questa opzione devi scannerizzare  il tuo passaporto oppure un documento ufficiale valido come ad esempio la tua carta d’identità o la tua patente di guida, caricarla nel nostro sistema più 1 o 2 foto private.";

$LNG['verify_welcome'] = "Benvenuti!";
$LNG['verify_welcome_text'] = "Benvenuti al processo verificia foto 100% reali.  Utilizza questa opzione e ricevi il 100% foto verificate e genuine. In solo pochissime operazioni incrementerai la tua credibilità del tuo profilo e riceverai molte più visite da parte degli utenti. Riceverai anche il logo all’interno del tuo profilo dove mostrerai a tutti gli utenti che le tue foto sono reali al 100%. In più il suo profilo verrà inserito come extra nell’apposito menù.";
$LNG['verify_enroll'] = "Richiedilo adesso!";
$LNG['verify_enroll_text'] = "Noi abbiamo al momento <strong>2 opzioni</strong> per verificare che le tue foto sono 100% verificate e reali: ";
$LNG['verify_enroll_1'] = "In webcam con il nostro admin";
$LNG['verify_enroll_2'] = "Copia del passaporto";
$LNG['verify_learn_more'] = "Learn more...";
$LNG['verify_start_now'] = "<span>Fai partire adesso </span>il processo di verifica";
$LNG['verify_steps'] = "PRIMO PASSO:";
$LNG['verify_select_2_options'] = "Per favore seleziona una delle 2 opzioni in lista:";
$LNG['verify_select_1'] = "Sessione in webcam con il nostro admin";
$LNG['verify_select_1_note'] = "<strong>note:</strong> devi avere una webcam ed un account Skype o MSN";
$LNG['verify_select_2'] = "Copia del passaporto – devi avere un passaporto o una carta d’identità validi";
$LNG['verify_select_2_note'] = "<strong>note:</strong> per velocizzare il tutto vi consigliamo di caricare anche 1 o 2 foto private in aggiunta della copia del passaporto";
$LNG['verify_important'] = "IMPORTANTE – PER FAVORE LEGGERE ATTENTAMENTE: Ogni tentativo di frode o di cercare di barare risulterà in un ban a vita dal network di EF. Il vostro annuncio verrà disabilitato e perderete la somma investita nel vostro annuncio.";

$LNG['verify_sms_message'] = "Videochat confirmation for the photo verification process: 
Date: #date#, Time: #time#. You will be contacted by our administrator. #title#";
$LNG['verify_email_subject'] = "Online appointment confirmation";
$LNG['verify_email_message'] = "Hereby we confirm your online appointment for the photo verification process at:<br /><br />
Date: #date#<br />
Time: #time#<br /><br />
You will be contacted by one of our administrators. Kind regards.<br />#title#";

$LNG['reject_sms_message'] = "Please choose 3 new dates for the videochat session (photo verification). We could not confirm your requested dates. Just login and choose again. #title#";
$LNG['reject_email_subject'] = "100% photo verification process / please enter new dates";
$LNG['reject_email_message'] = "Dear #showname#<br /><br />
Unfortunately all suggested dates for the video chat session to verify your pictures could not been confirmed. Please login again to your private area and choose three new
dates. We would like to appologize for any inconvenience caused.<br /><br />
Kind regards<br />
#title#";
$LNG['reg_has_been_succ'] = " La registrazione è avvenuta con successo !";

$LNG['escort_is_premium'] = 'This escort has premium package. <br/> If you want to delete the profile of this escort, <br/> please assign the package to another escort and then delete the profile.';
$LNG['100_verify'] = '100 Verify';
$LNG['verify_global_title'] = "Processo 100% foto verificate";

$LNG['verify_webcam_session'] = "Sessione Webcam";
$LNG['verify_webcam_request'] = "Richiedi una sessione webcam con un admin, per favore scegliere 2 appuntamenti. In caso di cancellazione dello stesso, avvisare l’admin.";
$LNG['verify_appointment'] = "Appuntamenti richiesti nr.";
$LNG['verify_preffered'] = "Orario preferito";
$LNG['verify_comment_field'] = "Aggiungi un commento";
$LNG['verify_webcam_software'] = "Software webcam:<br /><span>(selezionare MSN o Skype)</span>";
$LNG['verify_type_username'] = "Tipo username";
$LNG['verify_select_form'] = "Per favore scegli come ricevere la notifica circa la conferma dell�?appuntamento";
$LNG['verify_recommended'] = "raccomandato";
$LNG['verify_over_sms'] = "Tramite sms";
$LNG['verify_congratulations'] = "Congratulazioni!";
$LNG['verify_congratulations_text'] = "<p>La tua richiesta di incotro tramite webcam è stata inviata con successo. Verrai informata via e-mail o nel tuo profilo personale se l’incontro è confermato e riceverai tutti i dettagli dello stesso.</p><p class='colored verpic-note verpic-olli2'>Per favore prendi nota che dal lunedì al venerdì la tua richiesta verra evasa in 24 ore circa, durante il fine settimana verrà posticipata al lunedì successivo. Le festività nazionali verranno considerate allo stesso modo dei fine settimana.</p>";
$LNG['verify_already_pending'] = "Avete già richiesta in sospeso. Si prega di attendere che i nostri amministratori a contatto con voi.";
$LNG['verify_rejected'] = "Respinto";
$LNG['verify_passport_upload'] = "Caricamento del passaporto e carta d�?identità";
$LNG['verify_passport_upload_text'] = "Devi avere un passaporto oppure una carta d’identità valida per far validare il processo. Per favore prendere nota, NON possiamo accettare copie via e-mail di passaporti o di carte d’identità. Devi caricare la copia del passaporto tramite il pannello qui presente. In caso di problemi contattate il vostro agente: ";
$LNG['verify_upload_now'] = "Carica adesso il tuo documento qui";
$LNG['verify_provide_pics'] = "Consiglio: Carica anche 1 o 2 foto private, questo incrementerà la possibilità di avere il logo 100% verificata e velocizzerà il processo.";
$LNG['verify_select_notify'] = "Per favore scegli come ricevere la notifica circa il risultato della verifica:";
$LNG['verify_image_no'] = "Immagine n. ";
$LNG['verify_uploaded'] = "caricato";
$LNG['verify_uploaded_pictures'] = "Caricato le foto";
$LNG['verify_uploaded_pictures_text'] = "Hai caricato le foto con successo, per favore aggiungi un commento nel riquadro apposito per specificare che tipo di documento hai caricato.";
$LNG['verify_uploaded_comment_field'] = "Riquadro commenti";
$LNG['verify_congratulations_text2'] = "<p class='colored'><strong>Hai completato con successo la tua richiesta di verifica foto reali 100% tramite il caricamento dei file.</strong></p><p class='verpic-note'>Per favore prendi nota che dal lunedì al venerdì la tua richiesta verra evasa in 24 ore circa, durante il fine settimana verrà posticipata al lunedì successivo. Le festività nazionali verranno considerate allo stesso modo dei fine settimana.</p>";
$LNG['verify_attention'] = "ATTENZIONE: SOLO IN CASO DI PROBLEMI NEL CARICARE LE IMMAGINI, CONTATTATE IL VOSTRO SALES, SENZA INVIARE NIENTE A LUI, PER FAR RISOLVERE IL PROBLEMA SUL VOSTRO PROFILO. SUCCESSIVAMENTE RISOLTO IL PROBLEMA SOLO TRAMITE QUESTO PANNELLO PROVATE A RICARICARE LE IMMAGINI . ";
$LNG['verify_genuine'] = "100% foto verificate !<br/>
Tutte le foto sono genuine - approvate e confermate dallo staff di EF.";
$LNG['verify_welcome_agency'] = "Benvenuti al processo di verifica foto reali 100%.Questa opzione incrementerà la credibilità dei profili caricatie porterà molti più cilenti. Inizia la procedura adesso e scopri molto di più selezionando la escort ce vorresti sia verificata.";
$LNG['verify_choose_profile'] = "SELEZIONA LA ESCORT PER LA VERIFICA";
$LNG['verify_unverified'] = "You have not unverified escorts.";