<?php 
define('APPLICATION_ID', 6);

$LNG['ladies'] = "Ladies";
$LNG['latest_join'] = "Latest Join";

$LNG['width_comments'] = "Width Comments"; 
$LNG['100_fake_free_review'] = "100% echt"; 
$LNG['100_fake_free_review_desc'] = "Eine 100% echte Bewertung100% bedeutet, dass die Agentur bzw. das Girl per SMS bestätigt hat, dass das Treffen in der Form stattgefunden hat und die Angaben der Wahrheit entsprechen!"; 
$LNG['24_7'] = "24/7"; 
$LNG['PROD_additional_city'] = "Zusätzliche Stadt"; 
$LNG['PROD_city_premium_spot'] = "City Premium Spot"; 
$LNG['PROD_girl_of_the_month'] = "Girl des Monats"; 
$LNG['PROD_international_directory'] = "Internationales Verzeichnis"; 
$LNG['PROD_main_premium_spot'] = "Haupt Premium Spot"; 
$LNG['PROD_national_listing'] = "National listing"; 
$LNG['PROD_no_reviews'] = "Keine Berichte"; 
$LNG['PROD_search'] = "Suche"; 
$LNG['PROD_tour_ability'] = "Tour Verfügbarkeit"; 
$LNG['PROD_tour_premium_spot'] = "&quot;Auf Tour&quot;Premium Spot"; 
$LNG['_all'] = "Alle"; 
$LNG['about_me'] = "Ueber mich"; 
$LNG['about_meeting'] = "Ueber das Treffen"; 
$LNG['account_activated'] = "Ihr Konto wurde erfolgreich aktiviert, bitte loggen Sie sich ein und machen Sie die weiteren Schritte. <a href='" . NGE_URL . "/login.php".$QUERY_STRING."'>Hier klicken</a> zum anmelden."; 
$LNG['account_blocked_desc'] = "<p>Ihr Konto wurde aus einem der folgenden Gründe suspendiert:</p> 
<ul> 
<li>Beleidigung eines Users oder Escorts</li> 
<li>Diskriminierung eines Users oder Escorts</li> 
<li>Unwahre oder falsche Einträge oder Berichte</li> 
<li>Aufhetzung anderer User gegen andere</li> 
<li>Offensichtliche (Eigen)Werbung für Agenturen oder Escorts</li> 
</ul> 
<p>Wenn Du uns Deinen Kommentar oder Erklärung mitteilen möchtest, dann kontaktiere uns bitte unter: <a href='mailto:".NGE_CONTACT_EMAIL."'>".NGE_CONTACT_EMAIL."</a></p> 
<p>NOTE: Bitte nicht noch einmal registrieren und Beiträge schreiben. Wir werden alle Beiträge löschen und Deine IP Adresse blockieren</p>"; 
$LNG['account_not_verified'] = "Dein Konto wurde nicht verifiziert."; 
$LNG['account_type'] = "Kontotyp"; 
$LNG['active'] = "Aktiv"; 
$LNG['add'] = "Hinzufügen"; 
$LNG['add_escort'] = "Escort Profil hinzufügen"; 
$LNG['add_escort_profile'] = "Escort Profil hinzufügen"; 
$LNG['add_profile_now'] = "Eröffne Dein Profil mit Foto JETZT"; 
$LNG['add_review'] = "Bericht hinzufügen"; 
$LNG['add_third_party_girl'] = "Füge ein Drittpartei-Girl zu unserer Datenbank hinzu"; 
$LNG['add_watch'] = "Zur Favoritenliste hinzufügen"; 
$LNG['add_watch_success'] = "<b>%NAME%</b> wurde erfolgreich zu Deiner Favoritenliste hinzugefügt. <a href='javascript:history.back()'>Hier geht’szurück .</a>"; 
$LNG['additional'] = "Zusätzlich"; 
$LNG['address'] = "Strasse"; 
$LNG['age'] = "Alter"; 
$LNG['age_between'] = "Alter zwischen"; 
$LNG['agencies'] = "Agenturen"; 
$LNG['agencies_match'] = "Agentur Suchkriterium"; 
$LNG['agency'] = "Agentur"; 
$LNG['agency_city_tour_not_allowed'] = "Nur Girls welche KEINE Probemitgliedschaft haben werden gezeigt. City Touren sind für Girls in der Probemitgliedschaft nicht erlaubt. Bitte kaufe einen Premiium Spot um dieses Feature für sie freizuschalten. Klicke <a href='mailto:".NGE_CONTACT_EMAIL."'>hier um zu bestellen</a>."; 
$LNG['agency_data'] = "Agentur Daten"; 
$LNG['agency_denied_review'] = "abgewiesen"; 
$LNG['agency_denied_review_desc'] = "Die Agentur hat mitgeteilt, dass es zum besagten Zeitpunkt kein Treffen gegeben hat. Höchstwahrscheinlich handelt es sich um einen Fake Bericht da die Agentur/Girl die Angaben nicht bestätigt hat!"; 
$LNG['agency_girls'] = "Agentur Girls"; 
$LNG['agency_name'] = "Agentur Name"; 
$LNG['agency_profile'] = "Agentur Profil"; 
$LNG['agency_without_name'] = "Agentur ohne Namen"; 
$LNG['ajaccio'] = "Ajaccio"; 
$LNG['all'] = "Alle..."; 
$LNG['all_cities'] = "Alle Städte..."; 
$LNG['all_countries'] = "Alle Länder..."; 
$LNG['all_escorts'] = "Alle Escorts"; 
$LNG['all_origins'] = "Alle Herkünfte..."; 
$LNG['all_reviews'] = "Alle Berichte"; 
$LNG['already_registered_on_another_site'] = "Wenn Du bereits auf %SITE% registriert bist, braucht es keine Neuanmeldung. Einfach mit Deinem %SITE% Login und Passwort anmelden."; 
$LNG['already_reviewed'] = "Du hast dieses Girl schon bewertet! Nur eine Bewertung per Girl ist möglich, bitte Deine derzeitige Bewertung <a href='%LINK%'>anpassen</a> wenn Du möchtest!"; 
$LNG['already_voted'] = "Du hast bereits für dieses Girl gestimmt."; 
$LNG['also_provided_services'] = "Sie hat auch diese Services angeboten"; 
$LNG['anal'] = "Anal"; 
$LNG['are_you_escort'] = "Bist Du ein independent Escort Girl, eine Agentur oder ein Privatclub?"; 
$LNG['at_her_apartment'] = "Im Studio / Wohnung"; 
$LNG['at_her_hotel'] = "In ihrem Hotel"; 
$LNG['at_my_flat_or_house'] = "In meinem Haus/Wohnung"; 
$LNG['at_my_hotel'] = "In meinem Hotel"; 
$LNG['attention'] = "Achtung !"; 
$LNG['attitude'] = "Verhalten"; 
$LNG['availability'] = "Verfügbarkeit"; 
$LNG['available_for'] = "Verfügbar für"; 
$LNG['basic'] = "Grundsätzlich"; 
$LNG['back_to_actual_tours'] = 'Zurück zu den aktuellen Tours'; 
$LNG['back_to_reviews'] = "<< Zurück zu den Bewertungen"; 
$LNG['beauty'] = "Schönheit"; 
$LNG['become_premium_member_and_view_private_photos'] = "<a href='%LINK%'>Werde JETZT Premium Mitglied</a> und sehe unzensurierte und spezielle Fotos dieses Girls!"; 
$LNG['bio'] = "Bio"; 
$LNG['birth_date'] = "Geburtsdatum"; 
$LNG['black'] = "Schwarz"; 
$LNG['blond'] = "Blond"; 
$LNG['blowjob'] = "Blowjob"; 
$LNG['blowjob_with_condom'] = "Blowjob mit Gummi"; 
$LNG['blowjob_without_condom'] = "Blowjob ohne Gummi"; 
$LNG['blue'] = "Blau"; 
$LNG['bombshellf'] = "Sexbombe"; 
$LNG['book_email_to_escort'] = "<pre> 
Liebe %ESCORT%, 
   Ein Premium Mitglied von ".NGE_SITE_TITLE." möchte Dich am %DATE% treffen. 
Der Interessent beansprucht den Rabatt von %DISCOUNT%%, welchen Du unseren Premium Mitgliedern offerierst. 
Details: 
Username: %USERNAME% 
Ganzer Name: %FULLNAME% 
Gewünschter Termin: %DATE% 
Dauer: %DURATION% 
Telefon: %PHONE% 
Email: %EMAIL% 
Kommentare: %COMMENTS% 
Sicherheits Code: %CODE% 
Dieser Code dient der Verifikation des Interessenten. Der Interessent ist dazu verpflichtet, Dir auf Anfrage den Code mitzuteilen, 
damit Du verifizieren kannst ob er tatsächlich Anspruch auf den Rabatt hat. 
Viele Grüsse, 
<a href=\"".NGE_URL."\">".NGE_SITE_TITLE."</a> 
</pre>"; 
$LNG['book_this_girl'] = "Dieses Girl buchen"; 
$LNG['booking_desc'] = "Danke für Dein Interesse %NAME% zu buchen. Als Premium Mitglied erhältst Du einen Rabatt von %DISCOUNT%% auf Buchungen dieses Girls. Bitte fülle untenstehende Felder aus um das Girl zu buchen und vom Rabatt zu profitieren!"; 
$LNG['booking_mail_subject'] = "%SITE% Buchung"; 
$LNG['booking_mail_text'] = "<pre> 
Lieber %NAME%, 
Die Buchung von %ESCORT_NAME% für am %DATE% war erfolgreich. 
Das Girl wird in Kürze mit Dir Kontakt aufnehmen um die letzten Details für euer Treffen zu klären. 
Als Premium Mitglied von %SITE%, hast Du Anspruch auf eine Preisreduktion von %DISCOUNT%% auf den Normalpreis des Girls. 
Bitte bewahre folgende Sicherheits Nummer sorgfältig auf: %VERIFICATION_ID% 
Diese Nummer weist Dich als Premium Mitglied aus und garantiert Dir den Preisnachlass. 
Wir wünschen Dir eine fantastische Zeit mit dem Girl Deiner Wahl ! 
%SITE% Team</pre>"; 
$LNG['boys'] = "Boys"; 
$LNG['boys_trans'] = "Boys/Trans"; 
$LNG['breast'] = "Busen"; 
$LNG['breast_size'] = "Brustumfang"; 
$LNG['brown'] = "Braun"; 
$LNG['bruntal'] = "Bruntál"; 
$LNG['bust'] = "Busen"; 
$LNG['by'] = "von"; 
$LNG['calabria'] = "Calabria"; 
$LNG['can_have_max_cities'] = "Du kannst maximal %NUMBER% Städte auswählen"; 
$LNG['cancel'] = "Abbrechen"; 
$LNG['cant_be_empty'] = "kann nicht leer sein"; 
$LNG['cant_go_ontour_to_homecity'] = "Escort kann nicht in Deine Stadt kommen."; 
$LNG['castelfranco'] = "Castelfranco"; 
$LNG['chalons-en-champagne'] = "Chalons-en-Champagne"; 
$LNG['change_all_escorts'] = "Aenderungen bei allen Escort Profilen anwenden"; 
$LNG['change_passwd'] = "Passwort ändern"; 
$LNG['char_desc'] = "Tatoo etc."; 
$LNG['characteristics'] = "Eigenschaften"; 
$LNG['chiavari'] = "Chiavari"; 
$LNG['choose'] = "Wählen..."; 
$LNG['choose_availability'] = "Choose availability..."; 
$LNG['choose_another_city'] = "Wähle eine andere Stadt"; 
$LNG['choose_another_region'] = "Wähle eine andere Region"; 
$LNG['choose_city'] = "Stadt wählen..."; 
$LNG['choose_country'] = "Land wählen..."; 
$LNG['choose_currency'] = "Währung wählen..."; 
$LNG['choose_escort'] = "Escort wählen..."; 
$LNG['choose_escort_to_promote'] = "Wähle Escort zum bewerben"; 
$LNG['choose_language'] = "Sprache wählen..."; 
$LNG['choose_level'] = "Ebene wählen..."; 
$LNG['choose_ethnic'] = "Choose ethnic..."; 
$LNG['choose_nationality'] = "Nationalität wählen..."; 
$LNG['choose_state'] = "Region wählen..."; 
$LNG['choose_this_user'] = "Diesen User wählen"; 
$LNG['choose_time'] = "Zeit wählen..."; 
$LNG['choose_your_city'] = "Wähle Deine Stadt"; 
$LNG['choose_your_country'] = "Wähle Dein Land"; 
$LNG['choosen_escort'] = "Gewähltes Escort: %NAME%"; 
$LNG['cities'] = "Städte"; 
$LNG['city'] = "Stadt"; 
$LNG['city_escort_notify'] = "Werde von uns per Email informiert sobald ein neues Escort in Deiner Stadt verfügbar ist."; 
$LNG['city_of_meeting'] = "Stadt für das Treffen"; 
$LNG['claim_your_discount'] = "Profitiere JETZT vom Rabatt & buche dieses Girl"; 
$LNG['comment_review'] = "Kommentiere den Bericht"; 
$LNG['comment'] = "Kommentar"; 
$LNG['comments'] = "Kommentare"; 
$LNG['comments_to_her_services'] = "Kommentare zu ihren Services"; 
$LNG['comments_to_her_services_desc'] = "Beschreibe ihre sexuelle Rolle, zum Beispiel (S/M, Toy, etc)."; 
$LNG['company_name'] = "Agentur Name"; 
$LNG['conf_password'] = "Passwort bestätigen"; 
$LNG['confirmation_email_1'] = "Lieber %USER%, vielen Dank für die Registrierung auf %SITE%. Im folgenden findest Du Deine Login Informationen:"; 
$LNG['confirmation_email_2'] = "Bitte bewahre diese Daten gut auf."; 
$LNG['confirmation_email_subject'] = NGE_SITE_TITLE." Regsitration"; 
$LNG['contact'] = "Kontakt"; 
$LNG['contact_her'] = "Dieses Girl direkt kontaktieren"; 
$LNG['contact_her_desc'] = "Du kontaktierst %NAME%. Das Email wird von %SITE% Domain kommen und von uns zertifiziert, um Deinen Status als Premium Mitglied von %SITE% zu bestätigen"; 
$LNG['contact_her_mail_header'] = "<pre> 
************************************************************************************* 
Dies ist ein zertifiziertes Email eines %SITE% Premium Mitglieds. 
Dieser User ist ein bestätigtes Mitglied unserer Community und 
bezahlt eine regelmässige Mitgliedschafts Gebühr. 
*************************************************************************************</pre>"; 
$LNG['contact_her_mail_subject'] = "Mitteilung eines %SITE% Premium Mitglieds"; 
$LNG['contact_info'] = "Kontakt Info"; 
$LNG['contact_text'] = " 
Als grösste Escort Community in ".$LNG['country_'.MAIN_COUNTRY]." offerieren wir nun Werbeplätze auf unserer Seite. Wir haben die verfügbaren Plätze 
auf die Anzahl von 10 beschränkt – was eine sehr gute Klickrate Ihres Banners verspricht.<BR /> 
<BR /> 
Einige Fakten:<BR /> 
<BR /> 
Hits -> durchschnittlich 3 Millionen täglich <BR /> 
Unique visitors -> 25,000 und mehr durchschnittlich pro Tag<BR /> 
Members -> fast 15,000, und jeden Tag mehr<BR /> 
<BR /> 
Kontaktieren Sie uns um die Details zu besprechen. Wir werden umgehend mit Ihnen Rücksprache nehmen<BR /> 
<BR /> 
<a href='mailto:".NGE_CONTACT_EMAIL."'>".NGE_CONTACT_EMAIL."</a> 
Tel.: 0036 1203 1808 (10:00 - 20:00) 
</pre> 
"; 
$LNG['contact_us'] = "Kontakt / Werbung"; 
$LNG['continue'] = "Weiter"; 
$LNG['conversation'] = "Konversation"; 
$LNG['countries'] = "Länder"; 
$LNG['country'] = "Land"; 
$LNG['country_tw'] = "Taiwan"; 
$LNG['couples'] = "Paare"; 
$LNG['created'] = "Erstellt"; 
$LNG['cumshot'] = "Cumshot"; 
$LNG['cumshot_on_body'] = "Cumshot on Body"; 
$LNG['cumshot_on_face'] = "Cumshot in Face"; 
$LNG['curr_password'] = "Aktuelles Passwort"; 
$LNG['curr_password_empty_err'] = "Bitte gib Dein aktuelles Passwort ein"; 
$LNG['currency'] = "Währung"; 
$LNG['currently_listed_sites'] = "Hier sind die aktuellen Seiten aufgelistet. Bitte wähle die Seite."; 
$LNG['date'] = "Datum"; 
$LNG['day'] = "Tag"; 
$LNG['day_of_meeting'] = "Datum des Treffens"; 
$LNG['day_phone'] = "Telefon"; 
$LNG['days'] = "Tagen"; 
$LNG['default_city'] = "Standard Stadt für nach dem Login"; 
$LNG['delete_rates'] = "Alle Preise entfernen"; 
$LNG['delete_sel_pcis'] = "Selektierte Bilder löschen"; 
$LNG['delete_selected_reviews'] = "Selektierte Berichte löschen"; 
$LNG['delete_selected_tour'] = "Selektierte Tour löschen"; 
$LNG['desired_date'] = "Gewünschtes Datum"; 
$LNG['discount'] = "Rabatt"; 
$LNG['discount_code'] = "Rabatt Code"; 
$LNG['discount_code_txt'] = "Um vom 5% Rabatt zu profitieren, gib bitte den Rabatt Code ein, den Du von einem unserer Partner erhalten hast."; 
$LNG['discount_desc'] = "Auf dieser Seite siehst Du alle Girls die unseren Premium Mitgliedern einen <b>Rabatt</b> anbieten."; 
$LNG['discounted_girls'] = "Girls mit Rabatt"; 
$LNG['dont_forget_variable_symbol'] = " 
    <p>NICHT VERGESSEN Dein variables Symbol anzugeben: <big>%ESCORT_ID%</big>.<br /> 
   Ohne können wir Deine Zahlung nicht identifizieren.<br /> 
    Vielen Dank.</p> 
"; 
$LNG['dont_know'] = "Weiss nicht"; 
$LNG['dress_size'] = "Kleidergrösse"; 
$LNG['duration'] = "Dauer"; 
$LNG['easy_to_get_appointment'] = "Einfach ein Treffen zu machen"; 
$LNG['edit'] = "Bearbeiten"; 
$LNG['edit_escort'] = "Escort Profil bearbeiten"; 
$LNG['edit_gallery'] = "Gallerie bearbeiten"; 
$LNG['edit_languages'] = "Sprachen bearbeiten"; 
$LNG['edit_locations'] = "Orte bearbeiten"; 
$LNG['edit_private_photos'] = "Private Fotos bearbeiten"; 
$LNG['edit_private_photos_desc'] = "Please upload non-censored (without hiding any part of the picture) or glamour (special photos, which you do not want to offer to the wide public) photos here, which only our Premium paying users will be able to view. This way we ensure, that your sensitive photos, are only being seen by high-class customers, which expressed already their interest by paying the membership fee."; 
$LNG['edit_profile'] = "Profil aktualisieren"; 
$LNG['edit_rates'] = "Preise bearbeiten"; 
$LNG['edit_review'] = "Bericht aktualisieren"; 
$LNG['edit_settings'] = "Einstellungen aktualisieren"; 
$LNG['edit_units_in'] = "Anlagen bearbeiten in"; 
$LNG['email'] = "Email"; 
$LNG['email_domain_blocked_err'] = "Es tut uns leid %DOMAIN% akzeptiert momentan keine Emails von uns. Bitte eine andere Email Adresse wählen oder %DOMAIN% kontaktieren um unsere Korrespondenz zu akzeptieren."; 
$LNG['email_lng'] = "Sprache für den Emailverkehr"; 
$LNG['email_not_registered'] = "Dieses Email ist auf unserer Seite nicht registriert."; 
$LNG['empty_login_err'] = "Login kann nicht leer sein."; 
$LNG['empty_passwd_err'] = "Passwort kann nicht leer sein."; 
$LNG['empty_watchlist'] = "Es sind keine Einträge auf Deiner Favoritenliste"; 
$LNG['english'] = "Englisch"; 
$LNG['enthusiastic'] = "Enthusiastisch"; 
$LNG['error'] = "Fehler"; 
$LNG['error_adding_watch'] = "Fehler bei der Anpassung Deiner Favoritenliste. Bitte kontaktiere den Seitenadministrator."; 
$LNG['esc_mb_chng_subject'] = "Mitgliedschaft ändern auf ".NGE_SITE_TITLE; 
$LNG['esc_mb_chng_to_free_email'] = "<pre>Lieber %LOGIN%, 
  Die Mitgliedschaft Deines Escortprofils mit dem Namen %SHOWNAME% wurde auf \"free\" geändert. 
Du hast die Vorteile einer bezahlten Mitgliedschaft leider verloren. 
Verbessere jetzt Deine Mitgliedschaft und erhalte: 
- Girl/Bild Rotation auf der Hauptseite, was bedeutet, 
dass Du nicht auf der letzten Seite angezeigt wirst 
wo Du nur von wenigen Usern gesehen wirst. Alle bezahlten Profile rotieren stündlich. 
Somit hast Du die Chance zuoberst auf der Hauptseite oder der Stadtseite gezeigt zu werden. 
Das Zufallsprinzip gilt – jedes bezahlte Profil hat die gleichen Chancen. 
- Du kannst beim Wettbewerb \"Girl des Monats\" teilnehmen 
   
#N/A 
  reviewd girls list 
Um Deine Mitgliedschaft zu verbessern, antworte auf diese Mail oder 
logge Dich auf der Webseite ein und klicke auf \"Upgrade now\". 
".NGE_SITE_TITLE." Team 
</pre> 
"; 
$LNG['esc_mb_chng_to_normal_email'] = "<pre>Lieber %LOGIN%, 
   Die Mitgliedschaft des Escort Profils mit dem Namen %SHOWNAME% wurde auf  \"normal\" gestuft. 
Verfällt neu am: %EXPIRE% 
Von jetzt an profitierst Du von diesen Vorteilen: 
- Girl/Bild Rotation auf der Hauptseite, was bedeutet, 
dass Du nicht auf der letzten Seite angezeigt wirst 
wo Du nur von wenigen Usern gesehen wirst. Alle bezahlten Profile rotieren stündlich. 
Somit hast Du die Chance zuoberst auf der Hauptseite oder der Stadtseite gezeigt zu werden. 
Das Zufallsprinzip gilt – jedes bezahlte Profil hat die gleichen Chancen. 
- Du kannst beim Wettbewerb \"Girl des Monats\" teilnehmen 
   
#N/A 
  reviewd girls list 
".NGE_SITE_TITLE." Team 
</pre> 
"; 
$LNG['esc_mb_chng_to_premium_email'] = "<pre>Lieber %LOGIN%, 
  Die Mitgliedschaft des Escort Profils mit dem Namen %SHOWNAME% wurde auf \"premium\" gestuft. 
Verfällt neu am: %EXPIRE% 
Von jetzt an profitierst Du von diesen Vorteilen: 
#N/A 
".NGE_SITE_TITLE." Team 
</pre> 
"; 
$LNG['esc_mb_chng_to_trial_email'] = "<pre>Lieber %LOGIN%, 
  Die Mitgliedschaft des Escort Profils mit dem Namen %SHOWNAME% wurde auf \"trial\" gestuft. 
Verfällt neu am: %EXPIRE% 
Du hast eine Probemitgliedschaft auf 
 ".NGE_SITE_TITLE." gelöst. In dieser Periode erhältst Du alle 
Vorteile und Privilegien einer bezahlten Mitgliedschaft, 
die wären: 
- Girl/Bild Rotation auf der Hauptseite, was bedeutet, 
dass Du nicht auf der letzten Seite angezeigt wirst 
wo Du nur von wenigen Usern gesehen wirst. Alle bezahlten Profile rotieren stündlich. 
Somit hast Du die Chance zuoberst auf der Hauptseite oder der Stadtseite gezeigt zu werden. 
Das Zufallsprinzip gilt – jedes bezahlte Profil hat die gleichen Chancen. 
- Du kannst beim Wettbewerb \"Girl des Monats\" teilnehmen 
   
#N/A 
  reviewd girls list 
Nach der Probezeit musst Du Deine Mitgliedschaft in eine 
bezahlte Mitgliedschaft umwandeln, sonst verlierst Du alle Funktionen und Dein Profil 
erscheint nur noch auf den letzten Seiten. 
Um Deine Mitgliedschaft zu verbessern, antworte auf diese Mail oder 
logge Dich auf der Webseite ein und klicke auf \"Upgrade now\". 
".NGE_SITE_TITLE." Team 
</pre> 
"; 
$LNG['esc_mb_chng_to_vip_email'] = "<pre>Lieber %LOGIN%, 
   Die Mitgliedschaft des Escort Profils mit dem Namen %SHOWNAME% wurde auf \"VIP\" gestuft. 
Das Profil verfällt nicht und geniesst die selben Vorteile wie die einer bezahlten Mitgliedschaft. 
".NGE_SITE_TITLE." Team 
</pre> 
"; 
$LNG['esc_mb_memb_expired_email'] = "<pre>Lieber %LOGIN%, 
  Die Mitgliedschaft des Escort Profils mit dem Namen %SHOWNAME% ist verfallen. 
Du hast alle Vorteile verloren, und dein Konto wurde deaktiviert. 
Wenn Du die Mitgliedschaft erneuern möchtest, dann kontaktiere uns unter ".NGE_CONTACT_EMAIL.". 
".NGE_SITE_TITLE." Team 
</pre> 
"; 
$LNG['esc_mb_memb_expired_email_subj'] = "Membership expired"; 
$LNG['esc_mb_memb_expires_soon_email'] = "<pre>Lieber %LOGIN%, 
   Die Mitgliedschaft des Escort Profils mit dem Namen %SHOWNAME% verfällt in %DAYS% Tagen. 
Nicht vergessen die Mitgliedschaft zu verlängern. Kontaktiere uns hierzu unter ".NGE_CONTACT_EMAIL.". 
".NGE_SITE_TITLE." Team 
</pre> 
"; 
$LNG['esc_mb_memb_expires_soon_email_subj'] = "Mitgliedschaft verfällt bald"; 
$LNG['esc_to_upg'] = "Escort Profil zum upgraden"; 
$LNG['esc_upg_promo'] = " 
<ul> 
<li>Teilnahme an der Girl Rotation auf der Hauptseite. 
Dies bedeutet, dass Dein Profil nicht mehr auf den 
hinteren/letzten Seiten angezeigt wird. Alle bezahlten Profile rotieren stündlich. 
Jedes Profil hat die selbe Chance, ganz zuforderst angezeigt zu werden. 
Dies auf der Hauptseite und der betreffenden Städteseite.</li> 
<li>Teilnahme am \"Girl des Monats\" 
  Wettbewerb</li> 
<li>Teilnahme an TOP10 Girls (Girls mit den 
  meisten Berichten)</li> 
</ul>"; 
$LNG['esc_upg_promo_txt'] = "<a href='/private/upgrade_escort.php$QUERY_STRING'>JETZT Upgraden</a>"; 
$LNG['esc_upgrade_payment_details'] = " 
    Danke für Dein Interesse am Kauf einer Mitgliedschaft auf ".NGE_SITE_TITLE.".<br /> 
    Bitte überweise den Betrag von %AMOUNT% EUR auf unser Konto und Dein Profil wird umgehend nach dem Erhalt des Geldes feigeschaltet/aktualisiert.<br /> 
    <p><strong>Bank details:</strong><br /> 
    SMD MEDIA:<br /> 
    Conto corrente n. 6152293433/25<br /> 
    IBAN: IT42 P030 6910 9106 1522 9343 325<br /> 
    SWIFT: BCITIT333P0<br /> 
    Banca Intesa - filiale 2390<br /> 
    Via G. Rubini n. 6, 22100 Como</p> 
    Bei Fragen oder Problemen, zögere nicht, uns zu kontaktieren <a href='mailto:".NGE_CONTACT_EMAIL."'>".NGE_CONTACT_EMAIL."</a>. 
"; 
$LNG['escort'] = "Escort"; 
$LNG['escort_active'] = "Dein Escort Profil ist aktiv. <a href='%LINK%'>Hier klicken</a> zum Deaktivieren."; 
$LNG['escort_added_continue_review'] = "Escort erfolgreich hinzugefügt. <a href='%LINK%'>Hier klicken</a> um weiter zu schauen."; 
$LNG['escort_city_tour_not_allowed'] = "Während der Trial Periode ist es nicht erlaubt auf City Tour zu gehen. Du musst einen Premium Spot kaufen um dieses Feature zu aktivieren. Zum Bestellen <a href='mailto:".NGE_CONTACT_EMAIL."'>hier klicken</a>."; 
$LNG['escort_data_form_text'] = "Nur die Informationen ausfüllen die auf der Seite gezeigt werden sollen."; 
$LNG['escort_inactive'] = "Dieses Escort ist momentan nicht aktiv !"; 
$LNG['escort_modification_disabled'] = "Im Moment sind wir mit Wartungsarbeiten beschäftigt. Es können keine Anpassungen vorgenommen werden. Bitte probiere es später erneut. Wir entschuldigen uns für die Unannehmlichkeiten."; 
$LNG['escort_name_active'] = "Escort %ESCORT%'s Profil ist aktiv. <a href='%LINK%'>Klicke hier</a> zum deaktivieren."; 
$LNG['escort_name_not_active'] = "Escort %ESCORT%'s Profil ist inaktiv. <a href='%LINK%'>Klicke hier</a> zum aktivieren."; 
$LNG['escort_name_waiting_for_approval'] = "Escort %ESCORT%'s Profil wartet auf die Freigabe vom Seitenadmin."; 
$LNG['escort_not_active'] = "Dein Escort Profil ist nicht aktiv. <a href='%LINK%'>Klicke hier</a> zum aktivieren."; 
$LNG['escort_not_found_use_search'] = "Escort %NAME% nicht definiert, bitte benutze Suche um eins auszuwählen."; 
$LNG['escort_not_in_db_add_her'] = "Falls das Escort nicht in unserer Datenbank ist, bitte hier klicken um es <a href='%LINK%'>hinzuzufügen</a>."; 
$LNG['escort_profile_added'] = "Escort Profil wurde erfolgreich zur Datenbank hinzugefügt !<BR /> Bitte 
<a href='/private/edit_gallery.php$QUERY_STRING'>lade Deine Fotos für Dein Profil hinauf.</a>"; 
$LNG['escort_profile_updated'] = "Escort Profil wurde erfolgreich aktualisiert ! <BR /> 
<a href='/private/index.php$QUERY_STRING'>Zurück.</a>"; 
$LNG['escort_waiting_for_approval'] = "Dein Escort Profil muss vom Seitenadmin freigegeben werden."; 
$LNG['escorts'] = "Escorts"; 
$LNG['escorts_in_city'] = "Escorts in %CITY%"; 
$LNG['escorts_match'] = "Escorts Suchkriterium"; 
$LNG['evening_phone'] = "Alt. Tel."; 
$LNG['expires'] = "Verfällt"; 
$LNG['eye_color'] = "Augenfarbe"; 
$LNG['eyes'] = "Augen"; 
$LNG['fake_free_review_confirm_mail_01'] = " 
    <p>Hi %SHOWNAME%,<br /> 
   Ein Member von ".NGE_SITE_TITLE." hat über Dich auf unserer Seite berichtet. Um Falschangaben vorzubeugen bitten wir Dich die Informationen zu bestätigen oder abzulehnen ! Bitte einfach auf diese Email mit „Ja“ oder „Nein“ antworten (wobei „Ja“ akzeptieren bedeutet)!</p> 
    <p>Die Details des Kunden:</p> 
"; 
$LNG['fake_free_review_confirm_mail_02'] = " 
    <p>Bitte antworte mit: Ja für richtig, und nein für nicht richtig.</p> 
    <p>Dein ".NGE_SITE_TITLE." Team</p> 
"; 
$LNG['fake_free_review_confirm_mail_subj'] = NGE_SITE_TITLE." Fake Free Review Bestätigung"; 
$LNG['fake_photo'] = "Fake Foto!"; 
$LNG['fax'] = "Fax"; 
$LNG['female'] = "Weiblich"; 
$LNG['ff_guests'] = "<div class='big center'>nur noch %DAYS% Tage bis zur Auslosung!</div><a href='%LINK%' rel='nofollow'>JETZT anmelden</a> als Premium Mitglied - und erhalte die Chance auf ein GRATIS Rendez-Vous mit <a href='%LINK%' rel='nofollow'>Deinem Lieblingsgirl!</a> *"; 
$LNG['ff_guests_today'] = "<div class='big center'>HEUTE</div>geben wir den Gewinner der diesmonatigen „Free Fuck Lotterie“ bekannt ! Vielleicht bist es DU ? Ergreife die Chance und melde Dich <a href='%LINK%' rel='nofollow'>JETZT</a> als Premium Mitglied an – und schon bist Du in der Auslosung dabei! *"; 
$LNG['ff_nonpaid'] = "<div class='big center'>nur noch %DAYS% Tage bis zur Ziehung !</div><a href='%LINK%' rel='nofollow'>Werde JETZT Premium Mitglied</a> - und erhalte die Chance auf ein Sextreffen mit Deinem <a href='%LINK%' rel='nofollow'>Lieblingsgirl GRATIS!</a> *"; 
$LNG['ff_nonpaid_today'] = "<div class='big center'>HEUTE</div> geben wir den Gewinner der diesmonatigen „Free Fuck Lotterie“ bekannt ! Vielleicht bist es DU ? Ergreife die Chance und <a href='%LINK%' rel='nofollow'>werde JETZT Premium Member</a> um in der Auslosung dabei zu sein! *"; 
$LNG['ff_paid'] = " <div class='big center'>nur noch %DAYS% Tage bis zur Ziehung !</div>"; 
$LNG['ff_paid_today'] = " <div class='big center'>HEUTE</div>geben wir den Gewinner der diesmonatigen „Free Fuck Lotterie“ bekannt ! Vielleicht bist es DU ?"; 
$LNG['file_uploaded'] = "Foto erfolgreich hochgeladen."; 
$LNG['finish_reg'] = "Registration abschliessen"; 
$LNG['first_name'] = "Vorname"; 
$LNG['fluent'] = "Fliessend"; 
$LNG['forgot_email_1'] = "Lieber %USER%, Du hast Deine Login Daten auf der Seite %SITE% angefordert."; 
$LNG['forgot_email_2'] = "Deine Login Details sind:"; 
$LNG['forgot_email_subject'] = NGE_SITE_TITLE . " Passwort Anfrage"; 
$LNG['forgot_passwd'] = "Passwort vergessen?"; 
$LNG['forgot_password'] = "Passwort vergessen"; 
$LNG['free'] = "Gratis"; 
$LNG['free_fuck_desc'] = "An jedem Monatsende losen wir einen glücklichen Premium Member aus. Dieser gewinnt ein GRATIS Treffen mit dem Girl seiner Wahl auf ".NGE_SITE_TITLE.". Wir bezahlen alle Unkosten für das Treffen !<BR /> <BR /> Gratulation allen glücklichen Gewinnern ! Wir hoffen Du hattest eine geile Zeit mit dem Traumgirl Deiner Wahl... !"; 
$LNG['free_fuck_lotto'] = "Free Fuck Lotterie"; 
$LNG['free_fuck_note'] = "Wichtig: der Wert entspricht einem Betrag von maximum EUR 300 oder einer einstündigen Buchung. Wenn das Treffen nur EUR 250 kostet werden die restlichen 50 Euro nicht ausbezahlt. Der Preis kann nicht in Cash oder einer anderen Form ausbezahlt werden. Details zum genauen Ablauf werden dem Gewinner per Email gesendet."; 
$LNG['free_mb_desc'] = "Dein Profil ist aktuell auf den hinteren Seiten 
aufgeführt, was zu einer gerningen Anzahl von Klicks führt. 
Mach den Upgrade deiner Mitgliedschaft und geniesse folgende Vorteile::"; 
$LNG['free_mb_desc_ag'] = "%COUNT% Escorts - <a href='/private/upgrade_escort.php$QUERY_STRING'>Jetzt upgraden </a>für mehr Aufmerksamkeit bei den Kunden !"; 
$LNG['friday'] = "Freitag"; 
$LNG['friendly'] = "Freundlich"; 
$LNG['from'] = "Von"; 
$LNG['fuckometer'] = "Fuckometer"; 
$LNG['fuckometer_desc'] = "Der Fuckometer zeigt die Qualität des jeweiligen Girls an aufgrund der Berichte/Bewertungen der User. Er zeigt die Girls mit dem höchsten Wiederholungsfaktor/Suchtwert."; 
$LNG['fuckometer_range'] = "Fuckometer Spanne"; 
$LNG['fuckometer_rating'] = "Fuckometer Bemessung"; 
$LNG['fuckometer_short'] = "FM"; 
$LNG['fuckometer_toplist'] = "Fuckometer Topliste"; 
$LNG['full_name'] = "Vollständiger Name"; 
$LNG['gays'] = "Gays"; 
$LNG['gender'] = "Geschlecht"; 
$LNG['genuine_photo'] = "100% Originalfoto"; 
$LNG['get_trusted_review'] = "Bekomme einen echten Bericht"; 
$LNG['get_trusted_review_desc'] = " 
    Bekomme einen 100% echten Bericht! Fülle dazu die nachfolgenden Felder aus!<br /> 
   Wie funktioniert das <a href='%LINK%'>klicke hier</a>!"; 
$LNG['girl'] = "Girl"; 
$LNG['girl_booked'] = "Du hast %NAME% am %DATE% gebucht."; 
$LNG['girl_no_longer_listed'] = "Dieses Girl ist nicht mehr auf %SITE% eingetragen"; 
$LNG['girl_of'] = "Girl von"; 
$LNG['girl_of_the_month_history'] = "Girl des Monats Rückblick"; 
$LNG['girl_on_tour_1'] = "Girl ist auf Tour in %CITY% - %COUNTRY%"; 
$LNG['girl_on_tour_2'] = "Von %FROM_DATE% bis %TO_DATE%"; 
$LNG['girl_on_tour_3'] = $LNG['tour_phone'].": %PHONE%"; 
$LNG['girl_on_tour_4'] = $LNG['tour_email'].": %EMAIL%"; 
$LNG['girl_origin'] = "Girl Herkunft"; 
$LNG['girls_in_italy'] = "Alle gezeigten Girls sind JETZT in ".$LNG['country_'.MAIN_COUNTRY]."! Buche sie JETZT!"; 
$LNG['girls_international'] = "Möchtest Du ein internationales Girl buchen in  ".$LNG['country_'.MAIN_COUNTRY]." muss das Treffen in den meisten Fällen minimum 12 Stunden dauern"; 
$LNG['go_to_escort_profile'] = "Gehe zum Escort Profil"; 
$LNG['go_to_reviews_overview'] = "Gehe zur Bewertungsübersicht"; 
$LNG['gray'] = "Grau"; 
$LNG['green'] = "Grün"; 
$LNG['hair'] = "Haare"; 
$LNG['hair_color'] = "Haarfarbe"; 
$LNG['hard_to_believe_its_her'] = "Schwer zu glauben dass es sie ist"; 
$LNG['hard_to_book'] = "Schwierig zu buchen"; 
$LNG['height'] = "Grösse"; 
$LNG['help_system'] = "Hilfe"; 
$LNG['help_watch_list'] = "Du kannst jeden beliebigen User, Escort oder Agentur zu Deiner Liste hinzufügen. 
Wenn etwas wichtiges passiert, wird diese Liste für Email Benachrichtigungen gebraucht. 
Wenn ein <b>Escort Girl</b>  in Deiner Liste eingetragen ist, bekommst Du folgende Benachrichtigungen: 
<ul><li>Escort Girl hat eine neue Bewertung</li><li>Escort Girl ist auf Tour</li><li>Escort Girl ist in den Ferien oder von den Ferien zurück</li></ul> 
Wenn Du eine <b>agency</b> zur Liste hinzufügst, bekommst Du diese Emailbenachrichtigungen: 
<ul><li>Agentur hat ein neues Girl aufgeschaltet</li></ul> 
Wenn Du einen <b>anderen Member</b> zur Liste hinzufügst, erhältst Du folgende Emailbenachrichtigungen: 
<ul><li>Member hat einen neuen Bericht geschrieben</li></ul> 
Du kannst in der Liste so viele Einträge haben wie du möchtest. 
Für die einfache Handhabung der Liste logge Dich ein und klicke auf \"Favorite list\" im Menu. 
"; 
$LNG['hip'] = "Hip"; 
$LNG['home'] = "Home"; 
$LNG['home_page'] = "Home page"; 
$LNG['hour'] = "Stunde"; 
$LNG['hours'] = "Stunden"; 
$LNG['howto_set_main'] = "Auf das Bild klicken welches Du als Hauptbild verwenden möchtest."; 
$LNG['i_agree_with_terms'] = "Ich habe <a href='%LINK%'>die allgemeinen Geschäftsbedingungen</a> gelesen & akzeptiert"; 
$LNG['in'] = "In"; 
$LNG['in_face'] = "Ins Gesicht"; 
$LNG['in_mouth_spit'] = "In den Mund – spucken"; 
$LNG['in_mouth_swallow'] = "In den Mund - schlucken"; 
$LNG['incall'] = "Incall"; 
$LNG['independent'] = "Independent"; 
$LNG['independent_escorts_from'] = "Independent Escorts von"; 
$LNG['independent_girls'] = "Private Girls"; 
$LNG['info'] = "Info"; 
$LNG['intelligent'] = "Intelligent"; 
$LNG['international'] = "International"; 
$LNG['international_directory'] = "International"; 
$LNG['invalid_birth_date'] = "Ungültiges Geburtsdatum"; 
$LNG['invalid_curr_password'] = "Ungültiges Passwort"; 
$LNG['invalid_desired_date'] = "Ungültiges gewünschtes Datum"; 
$LNG['invalid_discount_code'] = "Ungültiger Rabatt Code."; 
$LNG['invalid_email'] = "Ungültige Email"; 
$LNG['invalid_email_info'] = "Deine Email Adresse wurde von unserem System nicht anerkannt. Wir können Dir leider keine Nachrichten senden. Dein Konto wurde vorübergehend gesperrt. Bitte gib eine andere, gültige Emailadresse an, damit wir Dir das Bestätigunsemail mit dem Link zustellen können. Nach dem Erhalt des Emails und des Bestätigunslinks funktioniert Dein Konto wieder einwandfrei.<br /><br /> 
Vielen Dank für Dein Verständnis."; 
$LNG['invalid_file_type'] = "Ungültiges Format. Es sind nur Jpeg Bilder möglich."; 
$LNG['invalid_meeting_date'] = "Ungültiges Datum des Treffens"; 
$LNG['invalid_showname'] = "Ungültiger Showname – bitte verwende ausschliesslich alphanumerische Zeichen."; 
$LNG['invalid_user_type'] = "Invalid User Typ"; 
$LNG['invitation'] = "Willkommen beim Escort Service! Bitte wähle die Stadt wo Du eine Begleitung suchst."; 
$LNG['invitation_1'] = "Auf den folgenden Seiten findest Du die besten Escort Girls, Privatclubs und Escortagenturen in Deiner Umgebung. - ".$LNG['country_'.MAIN_COUNTRY]." und International. Du siehst Fotos und Kontaktdetails von jedem Girl."; 
$LNG['invitation_2'] = "Wir wünschen Dir nun viel Spass auf unserer Seite.Bei Fragen oder Unklarheiten kontaktiere uns unter: <a href='mailto:".NGE_CONTACT_EMAIL."'>".NGE_CONTACT_EMAIL."</a>"; 
$LNG['joined'] = "Angemeldet"; 
$LNG['kiss'] = "Küssen"; 
$LNG['kiss_with_tongue'] = "Küssen mit Zunge"; 
$LNG['kissing'] = "Küssen"; 
$LNG['kissing_with_tongue'] = "Küssen mit Zunge"; 
$LNG['language'] = "Sprache"; 
$LNG['language_problems'] = "Sprachprobleme"; 
$LNG['languages'] = "Sprachen"; 
$LNG['languages_changed'] = "Sprachen erfolgreich aktualisiert"; 
$LNG['last'] = "Letzte"; 
$LNG['last_name'] = "Nachname"; 
$LNG['latest_10_reviews'] = "Die letzten 10 Berichte der Girls"; 
$LNG['latest_10_third_party_reviews'] = "Die letzten 10 Drittparteien Berichte"; 
$LNG['level'] = "Level"; 
$LNG['limitrofi'] = "Limitrofi"; 
$LNG['links'] = "Links"; 
$LNG['loano'] = "Loano"; 
$LNG['locations_changed'] = "Orte erfolgreich aktualisiert"; 
$LNG['logged_in'] = "Du bist eingeloggt als"; 
$LNG['login'] = "Login"; 
$LNG['enter'] = "Login"; 
$LNG['login_and_password_alphanum'] = "Login und Passwort können nur alphanumerische Zeichen enthalten (0-9, a-z, A-Z, _)."; 
$LNG['login_and_signup_disabled'] = "Im moment sind wir mit Wartungsarbeiten beschäftigt. Es können deshalb keine Anmeldungen/Logins erfolgen. Komm bitte später wieder. Bitte entschuldige die Unannehmlichkeiten."; 
$LNG['login_exists'] = "Login existiert bereits. Bitte wähle ein anderes..."; 
$LNG['login_problems_contact'] = "Sollten immer noch Probleme bestehen, dann kontaktiere uns unter: <a href='mailto:".NGE_CONTACT_EMAIL."'>".NGE_CONTACT_EMAIL."</a>"; 
$LNG['logout'] = "Logout"; 
$LNG['looking_for_girls_in_your_city'] = "Suchst Du Girls in Deiner Stadt?"; 
$LNG['looking_for_girls_in_your_region'] = "Suchst Du Girls in Deiner Region?"; 
$LNG['looks'] = "Aussehen"; 
$LNG['looks_and_services'] = "Aussehen und Service"; 
$LNG['looks_range'] = "Aussehen Spanne"; 
$LNG['looks_rating'] = "Aussehen Bewertung"; 
$LNG['lower_date'] = "Bis Datum kann nicht kleiner sein als von Datum"; 
$LNG['lower_fuckometer'] = "Fuckomoter Spanne ist nicht korrekt"; 
$LNG['lower_looks_rating'] = "Aussehen Bewertungsspanne ist nicht korrekt"; 
$LNG['lower_services_rating'] = "Service Bewertungsspanne ist nicht korrekt"; 
$LNG['male'] = "Männlich"; 
$LNG['meeting_costs'] = "Kosten für's Treffen"; 
$LNG['meeting_date'] = "Datum des Treffens"; 
$LNG['meeting_date_range'] = "Treffdatum Spanne"; 
$LNG['meeting_dress_comments'] = "Treffpunkt, Dress Code, Kommentare"; 
$LNG['meeting_length'] = "Dauer des Treffens"; 
$LNG['member'] = "Member"; 
$LNG['member_name'] = "Member Name"; 
$LNG['member_profit_01'] = "werde Vollmitglied des Forums und teile Deine Erfahrungen und Erlebnisse"; 
$LNG['member_profit_02'] = "Berichte über die Girls schreiben"; 
$LNG['member_profit_03'] = "werde über neue Girls auf %SITE% informiert"; 
$LNG['member_profits'] = "Als Member profitierst Du von"; 
$LNG['membership_change_subject'] = "Membership Aenderung auf ".NGE_SITE_TITLE; 
$LNG['membership_change_to_free_mail'] = " 
    <p>Lieber %LOGIN%,<br /> 
    Dein Status als Premium Member von ".NGE_SITE_TITLE." ist verfallen und wurde auf die normale Mitgliedschaft zurückgestuft.</p> 
    <p>Wenn Du Deine Premium Mitgliedschaft verlängern möchtest, schreib uns eine Email oder tu dies direkt auf der Webseite.</p> 
    <p>JETZT upgraden und profitieren:</p> 
    <ul> 
        <li>Rabatt auf alle Buchungen mit den Girls <a href='%LINK%'>hier</a></li> 
        <li>Automatische Teilnahme an der monatlichen &quot;Free Fuck Lotterie&quot; wo Du ein gratis Treffen mit Deinem Lieblingsgirl gewinnen kannst</li> 
        <li>Zutritt zum Premium Forum</li> 
        <li>Private Nachrichten schreiben</li> 
        <li>Favoritenliste anlegen mit Girls, Usern, Agenturen</li> 
        <li>Detailsuche</li> 
        <li>Keine Werbung</li> 
    </ul> 
    <p>Für weitere Fragen oder bei Problemen kontaktiere uns ganz einfach per Email. Wir werden uns umgehend mit Dir in Verbindung setzen.</p> 
    <p>Herzliche Grüsse<br /> 
    ".NGE_SITE_TITLE."- Team</p> 
"; 
$LNG['membership_change_to_premium_mail'] = " 
    <p>Lieber %LOGIN%,<br /> 
    Deine Anmeldung als Premium Member von ".NGE_SITE_TITLE." war erfolgreich. Danke für Dein Vertrauen.</p> 
    <p>Deine Login Daten sind wie folgt:<br /> 
    Login: %LOGIN%</p> 
    <p>(Diese Informationen bitte sorgfältig aufbewahren)</p> 
    <p>Hier noch einmal im Ueberblick was nun Deine Vorteile als Premium Member sind:</p> 
    <ul> 
        <li>Rabatt auf alle Buchungen mit den Girls <a href='%LINK%'>hier</a></li> 
        <li>Automatische Teilnahme an der monatlichen &quot;Free Fuck Lotterie&quot; wo Du ein gratis Treffen mit Deinem Lieblingsgirl gewinnen kannst</li> 
        <li>Zutritt zum Premium Forum</li> 
        <li>Private Nachrichten schreiben</li> 
        <li>Favoritenliste anlegen mit Girls, Usern, Agenturen</li> 
        <li>Detailsuche</li> 
        <li>Keine Werbung</li> 
    </ul> 
    <p>Für weitere Fragen oder bei Problemen kontaktiere uns ganz einfach per Email. Wir werden uns umgehend mit Dir in Verbindung setzen.</p> 
    <p>Herzliche Grüsse<br /> 
    ".NGE_SITE_TITLE."- Team</p> 
"; 
$LNG['membership_type'] = "Membership type"; 
$LNG['membership_types'] = "Mitgliedschafts Typen"; 
$LNG['men'] = "Männer"; 
$LNG['menu'] = "Menu"; 
$LNG['message'] = "Nachricht"; 
$LNG['message_for'] = "Nachricht für %NAME%"; 
$LNG['message_not_sent'] = "Nachricht senden fehlgeschlagen."; 
$LNG['message_sent'] = "Deine Mitteilung wurde an %NAME% gesendet."; 
$LNG['metric'] = "Metrisches System"; 
$LNG['metric_desc'] = "Zentimeter, Kilo, ..."; 
$LNG['middle_name'] = "Zweiter Name"; 
$LNG['minutes'] = "Minuten"; 
$LNG['modified'] = "Modifiziert"; 
$LNG['modify_escort'] = "Escort ändern"; 
$LNG['monday'] = "Montag"; 
$LNG['month'] = "Monat"; 
$LNG['month_1'] = "Januar"; 
$LNG['month_10'] = "Oktober"; 
$LNG['month_11'] = "November"; 
$LNG['month_12'] = "Dezember"; 
$LNG['month_2'] = "Februar"; 
$LNG['month_3'] = "März"; 
$LNG['month_4'] = "April"; 
$LNG['month_5'] = "Mai"; 
$LNG['month_6'] = "Juni"; 
$LNG['month_7'] = "Juli"; 
$LNG['month_8'] = "August"; 
$LNG['month_9'] = "September"; 
$LNG['months'] = "Monate"; 
$LNG['more_info'] = "Mehr Info..."; 
$LNG['more_new_girls'] = "Mehr neue Girls"; 
$LNG['more_news'] = "Mehr News..."; 
$LNG['multiple_times_sex'] = "Mehrmals Sex"; 
$LNG['my_escort_profile'] = "Mein Escort Profil"; 
$LNG['my_escorts'] = "Meine Escorts"; 
$LNG['my_languages'] = "Meine Sprachen"; 
$LNG['my_locations'] = "Meine Arbeitsorte"; 
$LNG['my_new_review_notify_agency'] = "Werde per Email informiert sobald eines Deiner Escorts eine neue Bewertung erhält."; 
$LNG['my_new_review_notify_single'] = "Werde per Email informiert sobald Du eine neue Bewertung erhältst."; 
$LNG['my_photos'] = "Meine Fotos"; 
$LNG['my_rates'] = "Meine Preise"; 
$LNG['my_real_profile'] = "Meine richtigen Angaben"; 
$LNG['my_reviews'] = "Meine Bewertungen"; 
$LNG['name'] = "Name"; 
$LNG['name_of_the_lady'] = "Name des Girls"; 
$LNG['name_of_the_site'] = "Name der Seite"; 
$LNG['ethnic'] = "Ethnic"; 
$LNG['nationality'] = "Nationalität"; 
$LNG['need_help'] = "Hilfe ?"; 
$LNG['need_to_be_registered_to_review'] = "Du musst auf %SITE% registriert sein um einen Bericht zu schreiben. <a href='%LINK%'>GRATIS Anmeldung hier!</a>"; 
$LNG['never'] = "nie"; 
$LNG['new'] = "Neu"; 
$LNG['new_arrivals'] = "Neue Girls"; 
$LNG['new_email'] = "Neue Email"; 
$LNG['new_entries'] = "Neue Einträge"; 
$LNG['new_escort_email_by_agency_body'] = "<pre>Agentur \"%AGENCY%\" hat ein neues Escort hinzugefügt: %ESCORT% ! 
Du bekommst diese Benachrichtigung, weil Du die betreffende Agentur in Deiner Favoritenliste hast. 
Klicke auf diesen Link 
<a href=\"%LINK%\">%LINK%</a> 
um die Sedcard des Girls zu sehen. 
Dein ".NGE_SITE_TITLE." Team 
</pre>"; 
$LNG['new_escort_email_by_agency_subject'] = "%AGENCY% hat ein neues Escort hinzugefügt !"; 
$LNG['new_password'] = "Neues Passwort"; 
$LNG['new_password_empty_err'] = "Neues Passwort kann nicht leer sein"; 
$LNG['new_review_email_agency_body'] = "<pre>User %USER% hat einen Bericht über %ESCORT% geschrieben ! 
Klick auf diesen Link 
<a href=\"%LINK%\">%LINK%</a> 
um den Bericht zu lesen. 
Dein ".NGE_SITE_TITLE." Team 
</pre>"; 
$LNG['new_review_email_agency_subject'] = "Dein Escort hat eine neue Bewertung !"; 
$LNG['new_review_email_by_escort_body'] = "<pre>Escort %ESCORT% hat eine neue Bewertung von %USER% erhalten! 
Du erhältst diese Mitteilung weil das betreffende Escort in Deiner Favoritenliste ist. 
Klick auf diesen Link 
<a href=\"%LINK%\">%LINK%</a> 
um den Bericht zu lesen. 
Dein ".NGE_SITE_TITLE." Team 
</pre>"; 
$LNG['new_review_email_by_escort_subject'] = "%ESCORT% wurde neu bewertet !"; 
$LNG['new_review_email_by_user_body'] = "<pre>User %USER% hat einen Bericht über %ESCORT% geschrieben ! 
Du erhältst diese Mitteilung weil Du den User in Deiner Favoritenliste hast. 
Klick auf diesen Link 
<a href=\"%LINK%\">%LINK%</a> 
um den Bericht zu lesen. 
Dein ".NGE_SITE_TITLE." Team 
</pre>"; 
$LNG['new_review_email_by_user_subject'] = "%USER% hat einen neuen Bericht geschrieben !"; 
$LNG['new_review_email_escort_body'] = "<pre>User %USER% hat einen neuen Bericht über Dich geschrieben ! 
Klick auf diesen Link 
<a href=\"%LINK%\">%LINK%</a> 
um den Bericht zu lesen. 
Dein ".NGE_SITE_TITLE." Team 
</pre>"; 
$LNG['new_review_email_escort_subject'] = "Du hast eine neue Bewertung !"; 
$LNG['news'] = "News"; 
$LNG['newsletter'] = "Newsletter"; 
$LNG['next'] = "Nächste"; 
$LNG['next_step'] = "Nächster Schritt"; 
$LNG['no'] = "Nein"; 
$LNG['no_blowjob'] = "Kein Blowjob"; 
$LNG['no_cumshot'] = "Kein Abspritzen"; 
$LNG['no_esc_sel_err'] = "Du musst ein Escort auswählen"; 
$LNG['no_escort_languages'] = "Du hast keine Sprachen ausgewählt. <a href='%LINK%'>Klicke hier</a> für die Auswahl."; 
$LNG['no_escort_main_pic'] = "Du hast kein Hauptbild selektiert. <a href='%LINK%'>Klicke hier</a> um eins auszuwählen."; 
$LNG['no_escort_name_languages'] = "Escort %ESCORT% hat keine Sprachen selektiert. <a href='%LINK%'>Klicke hier</a> für die Auswahl."; 
$LNG['no_escort_name_main_pic'] = "Escort %ESCORT% hat kein Hauptbild. <a href='%LINK%'>Klicke hier</a>um eins auszuwählen."; 
$LNG['no_escort_name_photos'] = "Escort %ESCORT% hat keine gültigen Fotos. <a href='%LINK%'>Klicke hier</a> um die Gallerie zu aktualisieren."; 
$LNG['no_escort_photos'] = "Du hast keine gültigen Fotos in Deinem Profil. <a href='%LINK%'>Klick hier</a> um die Gallerie zu aktualisieren."; 
$LNG['no_escort_profile'] = "Du hast kein aktives Escort Profil. <a href='%LINK%'>Klicke hier</a>um eins hinzuzufügen."; 
$LNG['no_escorts_found'] = "Keine Escorts gefunden"; 
$LNG['no_escorts_profile'] = "Du hast keine Escort Profile definiert. Bitte <a href='%LINK%'>hier klicken</a> um eins hinzuzufügen."; 
$LNG['no_kiss'] = "Kein Küssen"; 
$LNG['no_kissing'] = "Kein Küssen"; 
$LNG['no_only_once'] = "Nein, nur einmal"; 
$LNG['no_premium_escorts'] = "Keine Premium Escorts."; 
$LNG['no_reviews_found'] = "Keine Bewertungen gefunden"; 
$LNG['no_sex_avail_chosen'] = "Du musst minimum eine sexuelle Verfügbarkeit auswählen"; 
$LNG['no_users_found'] = "Keine Benutzer gefunden"; 
$LNG['no_votes'] = "Nicht bewertet"; 
$LNG['norm_memb'] = "1 monatige normale Mitgliedschaft für 50 Euro"; 
$LNG['normal'] = "Normal"; 
$LNG['normal_mb_desc_ag'] = "%COUNT% Escorts (1ste verfällt in %DAYS% Tagen)"; 
$LNG['normal_review'] = "normal"; 
$LNG['normal_review_desc'] = "Ein normaler Bericht bedeutet, dass die Agentur/Girl den Bericht weder bestätigt noch dementiert hat! Das heisst, der Bericht kann allemals als verlässlich gewertet werden!"; 
$LNG['not_approved'] = "Wartet auf Freigabe"; 
$LNG['not_available'] = "Nicht verfügbar"; 
$LNG['not_logged'] = "Nicht angemeldet"; 
$LNG['not_logged_desc'] = "Du musst eingeloggt sein um dieses Feature zu nutzen."; 
$LNG['not_verified_msg'] = " 
<p>Dein Konto wurde noch nicht verifiziert.<br /> 
Um die Registration auf ".NGE_SITE_TITLE." abzuschliessen, muss Deine Email Adresse bestätigt werden. Falls Du noch kein Email mit dem Bestätigungslink erhalten hast, <a href=\"%LINK%\">klicke hier um es noch einmal zu erhalten</a>. 
</p><p> 
Wichtig: überprüfe auch Deinen Spam Ordner. Einige Provider taxieren Mails von uns als Spam. Vielen Dank für Dein Verständnis. 
</p>"; 
$LNG['not_yet_member'] = "Noch kein MEMBER? Melde dich jetzt GRATIS an!"; 
$LNG['not_yet_premium_signup_now'] = "Noch kein Premium Mitglied? <a href='%LINK%'>JETZT anmelden</a> und viel Geld sparen!"; 
$LNG['note'] = "Note"; 
$LNG['notify_comon'] = "<pre>Du bekommst diese Email weil Du auf ".NGE_SITE_TITLE." registriert bist und diese Benachrichtigungen bestellt hast. 
Um die Einstellungen zu ändern, logge Dich ein und benutze das Menu \"Settings\".</pre>"; 
$LNG['now'] = "Jetzt"; 
$LNG['number'] = "Nr."; 
$LNG['number_of_reviews'] = "Anzahl Bewertungen"; 
$LNG['offers_discounts'] = "Offerieren Rabatte"; 
$LNG['on_tour_in'] = "auf Tour in"; 
$LNG['only'] = "nur"; 
$LNG['only_fake_free_reviews'] = "Nur 100% fake free Bewertungen"; 
$LNG['only_for_premium_members'] = "nur für Premium Mitglieder"; 
$LNG['only_girls_with_reviews'] = "Nur Girls mit Bewertungen"; 
$LNG['only_reviews_from_user'] = "Bewertungen nur von Benutzern"; 
$LNG['only_reviews_not_older_then'] = "Bewertungen nicht älter als"; 
$LNG['optional'] = "optional"; 
$LNG['oral_without_condom'] = "Blasen ohne Gummi"; 
$LNG['order_premium_spot_now'] = "Premium Spot jetzt bestellen!"; 
$LNG['origin'] = "Herkunft"; 
$LNG['origin_escortforum'] = "Escort Forum"; 
$LNG['origin_non_escortforum'] = "nichtg Escort Forum"; 
$LNG['other'] = "Andere"; 
$LNG['other_boys_from'] = "Andere Escorts von"; 
$LNG['other_escorts_from'] = "Andere Escorts von"; 
$LNG['boys_from'] = "Escorts von"; 
$LNG['escorts_from'] = "Escorts von"; 
$LNG['outcall'] = "Outcall"; 
$LNG['overall_rating'] = "Gesamtbewertung"; 
$LNG['partners'] = "Partner"; 
$LNG['passive'] = "Passiv"; 
$LNG['passwd_has_been_sent'] = "Ihr Passwort wurde an %EMAIL% versandt"; 
$LNG['passwd_succesfuly_sent'] = "Ihr Passwort wurde erfolgreich an Ihre Email Adresse gesandt."; 
$LNG['password'] = "Passwort"; 
$LNG['password_changed'] = "Passwort erfolgreich geändert"; 
$LNG['password_invalid'] = "Das Passwort ist ungültig. Es muss mindestens 6 Zeichen lang sein."; 
$LNG['password_missmatch'] = "Passwort stimmt nicht überein"; 
$LNG['percent_discount'] = "%DISCOUNT%% Rabatt"; 
$LNG['phone'] = "Telefon"; 
$LNG['phone_instructions'] = "Telefon Instruktionen"; 
$LNG['photo_correct'] = "Foto korrekt"; 
$LNG['photos'] = "Fotos"; 
$LNG['pic_delete_failed'] = "Bild löschen gescheitert"; 
$LNG['pic_deleted'] = "Bild gelöscht"; 
$LNG['place'] = "Platz"; 
$LNG['please_choose_services'] = "Bitte wähle die Services aus, die das Girl gemacht hat; es beeinflusst den Fuckometer! Danke für Dein Verständnis!"; 
$LNG['please_fill_form'] = "bitte fülle das Formular vollständig aus"; 
$LNG['please_provide_email'] = "Bitte gib die Email Adresse an mit der Du Dich auf %SITE% registriert hast"; 
$LNG['please_select'] = "Bitte wählen..."; 
$LNG['plus2'] = "2+"; 
$LNG['poshy'] = "Schick"; 
$LNG['posted_reviews'] = "Du hast bisher %NUMBER% Berichte geschrieben."; 
$LNG['posted_to_forum'] = "Du hast bisher %NUMBER% Forum Beiträge geschrieben."; 
$LNG['premium'] = "Premium"; 
$LNG['premium_escorts'] = "Premium Escorts"; 
$LNG['premium_mb_desc_ag'] = "%COUNT% Escorts (1ste verfällt in %DAYS% Tagen)"; 
$LNG['premium_user'] = "Premium Benutzer"; 
$LNG['previous'] = "Vorher"; 
$LNG['price'] = "Preis"; 
$LNG['priv_apart'] = "Privates Appartment"; 
$LNG['private_menu'] = "Privates Menu"; 
$LNG['problem_report'] = "Ein Problem melden"; 
$LNG['problem_report_back'] = "Hier klicken um zum Escort Profil zurückzukehren"; 
$LNG['problem_report_no_accept'] = "Vielen Dank, Dein Problem Report wurde akzeptiert"; 
$LNG['problem_report_no_name'] = "Bitte um Angabe Deines Namens"; 
$LNG['problem_report_no_report'] = "Bitte Bericht erstellen"; 
$LNG['prof_city_expl'] = "Herkunfts Stadt des Girls, nicht unbedingt ihr Arbeitsort. Die Arbeitsorte können in den nächsten Schritten definiert werden."; 
$LNG['prof_country_expl'] = "Herkunftsland des Girls, nicht unbedingt ihr Arbeitsort. Die Arbeitsorte können in den nächsten Schritten definiert werden."; 
$LNG['profile'] = "Profil"; 
$LNG['profile_changed'] = "Profil erfolgreich aktualisiert"; 
$LNG['promote_this_girl'] = "Für dieses Girl werben"; 
$LNG['promote_yourself'] = "Für Dich selbst werben"; 
$LNG['provided_services'] = "Angebotene Services"; 
$LNG['publish_date'] = "Datum publizieren"; 
$LNG['rank'] = "Rang"; 
$LNG['rate'] = "Bewerten"; 
$LNG['rate_looks'] = "Bewerte ihr Aussehen"; 
$LNG['rate_services'] = "Bewerte ihre Services"; 
$LNG['rates'] = "Preise"; 
$LNG['rates_changed'] = "Preise erfolgreich aktualisiert"; 
$LNG['rates_deleted'] = "Preise erfolgreich gelöscht"; 
$LNG['rating'] = "Bewertung"; 
$LNG['re_verify_sent'] = "Wir haben Dir das Aktivierungsemail erneut gesendet. Bitte prüfe Deine Inbox und folge den im Mail erhaltenen Instruktionen."; 
$LNG['read_this_review'] = "Diesen Bericht lesen"; 
$LNG['read_whole_article'] = "Den ganzen Artikel lesen"; 
$LNG['real'] = "Echt"; 
$LNG['real_data_form_text'] = "Bitte gib Deine wirklichen Daten an. Denk daran, dass diese Informationen nicht im Profil veröffentlicht werden noch in irgend einer anderen Form zugänglich sind. Die Daten sind ausschliesslich für uns intern gedacht."; 
$LNG['real_name'] = "Richtiger Name"; 
$LNG['recalibrate_now'] = "JETZT neu einstellen"; 
$LNG['receive_newsletter'] = "Erhalte ".NGE_SITE_TITLE." Newsletter"; 
$LNG['red'] = "Rot"; 
$LNG['reference'] = "Wie hast Du von uns gehört?"; 
$LNG['regards'] = "Viele Grüsse, %SITE% Team."; 
$LNG['region'] = "Region"; 
$LNG['register'] = "Registrieren"; 
$LNG['registration'] = "Registration"; 
$LNG['rem_watch'] = "Von Favoritenliste entfernen"; 
$LNG['rem_watch_success'] = "<b>%NAME%</b> wurde erfolgreich von Deiner Favoritenliste gelöscht. <a href='javascript:history.back()'>Schritt zurück klick hier.</a>"; 
$LNG['remove'] = "Entfernen"; 
$LNG['renew_now'] = "Jetzt erneuern !"; 
$LNG['required_fields'] = "Notwendige Felder sind mit * gekennzeichnet"; 
$LNG['rev_resume_title'] = "Der letzte Bericht über dieses Escort"; 
$LNG['review'] = "Bewertung"; 
$LNG['review_changed'] = "Bewertung erfolgreich aktualisiert"; 
$LNG['review_guide_and_rules'] = " 
    <strong>Leitfaden und Regeln:</strong><br /> 
    Wir bitten Dich, diese Regeln zu befolgen und keine falschen Berichte zu verfassen. Jeder Bericht hilft Dir und den anderen Usern zukünftig bessere Erfahrungen zu machen. 
    <ul> 
        <li>Bitte nenne alle Services die das Girl offeriert</li> 
        <li>Füge neue Girls unserer Datenbank hinzu. Falls Du das Girl nicht findest, füge es unserer Datenbank hinzu.</li> 
        <li>Bitte beschreibe Dein Erlebnis mit ihr. Kurzberichte wie &quot;Sie war geil&quot; werden gelöscht.</li> 
        <li>Bitte bewerte das Girl nicht höher als 8.0, wenn sie nicht mindestens diese Services angeboten hat: 
        <ul> 
            <li>Französisch ohne</li> 
            <li>Neutrales oder enthusiastisches Sexverhalten</li> 
            <li>Küssen mit Zunge</li> 
        </ul> 
    </ul>"; 
$LNG['review_language'] = "Bericht Sprache"; 
$LNG['review_sms'] = "Du wurdest auf ".NGE_SITE_TITLE." bewertet! Antworte ganz einfach per SMS: ja, für richtig und nein für nicht richtig! In einem zweiten SMS erhältst Du die Angaben Deines Kunden!"; 
$LNG['review_status'] = "Bewertungsstatus"; 
$LNG['review_this_girl'] = "Bewerte dieses Girl"; 
$LNG['reviews'] = "Bewertungen"; 
$LNG['reviews_to_recalibrate'] = " 
    Lieber %NAME%,<br /> 
    Du hast %REVIEWS% Berichte zum überarbeiten. Bitte hilf uns die Berichte zu verbessern, indem Du detailliertere Informationen über das Treffen/Girl schreibst."; 
$LNG['rouen'] = "Rouen"; 
$LNG['royal'] = "Royal System"; 
$LNG['royal_desc'] = "Inches, Fuss, ..."; 
$LNG['rss_channel_desc'] = "Neueste Escorts"; 
$LNG['salon'] = "Night Club"; 
$LNG['sardegna'] = "Sardinien"; 
$LNG['saturday'] = "Samstag"; 
$LNG['sauna'] = "Sauna"; 
$LNG['save_changes'] = "Aenderungen speichern"; 
$LNG['score'] = "Punktzahl"; 
$LNG['search'] = "Suchen"; 
$LNG['search_desc'] = "Willkommen bei der einfachen Suche von ".NGE_SITE_TITLE.". Für eine detailliertere Suche klicke auf <a href='%LINK%'>Detailsuche</a>."; 
$LNG['search_desc2'] = "Willkommen bei der Detailsuche von ".NGE_SITE_TITLE.". Für die einfache Suchmaske klicke auf <a href='%LINK%'>einfache Suche</a>."; 
$LNG['search_escorts'] = "Escorts suchen"; 
$LNG['search_girl'] = "Girl suchen"; 
$LNG['search_agency'] = "Agentur suchen"; 
$LNG['search_name'] = "Girl-/Agentur Name"; 
$LNG['search_reviews'] = "Bewertungen suchen"; 
$LNG['search_reviews_back'] = "Zurück zur Suche"; 
$LNG['search_reviews_no_params'] = "Bitte minimum ein Suchkriterium eingeben"; 
$LNG['search_reviews_no_result'] = "Sorry, wir konnten keine Resultate finden. Bitte versuche es erneut"; 
$LNG['search_users'] = "Benutzer suchen"; 
$LNG['see_escorts_soon_on_tour'] = "Hier klicken um die Girls zu sehen die bald in ".$LNG['country_'.MAIN_COUNTRY]." ankommen"; 
$LNG['see_independent_preview'] = "Vorschau aller Independent Girls ansehen"; 
$LNG['select_as_main'] = "Willst Du das gewählte Bild als Dein Hauptbild verwenden?"; 
$LNG['select_your_service_requirements'] = "Wähle Deine Service Anforderungen"; 
$LNG['send_message'] = "Nachricht senden"; 
$LNG['service_comments'] = "Service Kommentare"; 
$LNG['services'] = "Services"; 
$LNG['services_range'] = "Service Angebot"; 
$LNG['services_rating'] = "Service Bewertung"; 
$LNG['services_she_was_willing'] = "Services die sie gemacht hat"; 
$LNG['settings'] = "Einstellungen"; 
$LNG['settings_update_error'] = "Fehler beim Aktualisieren der Einstellungen"; 
$LNG['settings_updated'] = "Einstellungen erfolgreich aktualisiert."; 
$LNG['sex'] = "Sex"; 
$LNG['sex_available_to'] = "Sexuell verfügbar für"; 
$LNG['shoe_size'] = "Schuhgrösse"; 
$LNG['show_all_reviews'] = "Alle Bewertungen zeigen"; 
$LNG['show_email'] = "Email auf der Seite anzeigen?"; 
$LNG['showname'] = "Showname/Angezeigter Name"; 
$LNG['show_name'] = "Name anzeigen"; 
$LNG['show_only_girls_on_tour_in'] = "Nur Girls zeigen auf Tour in"; 
$LNG['show_site_reviews'] = "Zeige %SITE% Berichte"; 
$LNG['show_third_party_reviews'] = "Bewertungen von Drittparteien zeigen"; 
$LNG['showname_exists'] = "Dieses Alias existiert bereits, bitte wähle ein anderes"; 
$LNG['sign_up'] = "Anmelden"; 
$LNG['sign_up_for_free'] = "Gratis auf " . NGE_SITE_TITLE . " anmelden"; 
$LNG['signup_activated_01'] = "Dein Konto wurde erfolgreich aktiviert!"; 
$LNG['signup_activated_02'] = " 
X	        Gratulation!<br /> 
        Jetzt bist Du ein Mitglied von %SITE%.<br /> 
        Deine Login Information wurde Dir per Email zugesandt."; 
$LNG['signup_activated_03'] = "Klick hier um Dich einzuloggen."; 
$LNG['signup_and_win'] = "Heute als Premium Mitglied anmelden und die Chance erhalten <strong>EINEN GRATIS SEXTREFF IM WERT VON EUR 300 ZU GEWINNEN!</strong>"; 
$LNG['signup_confirmation_email'] = "<pre> 
Lieber %USER%, 
Danke für die Registrierung auf %SITE%. 
Du bist jetzt ein geprüftes Mitglied von %SITE%. 
Im folgenden findest Du Deine Login Informationen. Bitte bewahre diese gut auf und verwende sie für zukünftige Anfragen als Referenz: 
Login: %USER% 
Website: %SITE% 
Geniesse die Seite! 
Wir freuen uns auf Deine Berichte und Beiträge im Forum! 
Your %SITE% Team</pre>"; 
$LNG['signup_confirmation_email_subject'] = "Registration auf %SITE%"; 
$LNG['signup_error_email_missing'] = "Fehler: Die angegebene Email Adresse ist ungültig, bitte eine andere Email eingeben."; 
$LNG['signup_error_email_used'] = "Fehler: Diese Email wird bereits für %SITE% verwendet. Bitte wähle eine andere."; 
$LNG['signup_error_login_missing'] = "Fehler: Du hast keinen Login Namen angegeben, bitte ausfüllen."; 
$LNG['signup_error_login_used'] = "Fehler: Dieses Login existiert bereits. Bitte wähle ein anderes."; 
$LNG['signup_error_password_invalid'] = "Fehler: Passwort ist ungültig. Das Passwort muss minimum 6 Zeichen lang sein."; 
$LNG['signup_error_password_missing'] = "Fehler: Du hast kein Passwort gewählt, bitte ausfüllen."; 
$LNG['signup_error_password_wrong'] = "Fehler: Erste und zweite Passworteingabe stimmen nicht überein. Die Passwörter müssen identisch sein."; 
$LNG['signup_error_terms_missing'] = "Du musst die Allgemeinen Geschäftsbedingungen akzeptieren um Mitglied unserer Seite zu werden."; 
$LNG['signup_successful_01'] = "Registration als Mitglied von %SITE% erfolgreich."; 
$LNG['signup_successful_02'] = "Vielen Dank für die Anmeldung !"; 
$LNG['signup_successful_03'] = "Um die Anmeldung abzuschliessen, überprüfe Deine Mailbox."; 
$LNG['signup_successful_04'] = "<b>ACHTUNG: Bitte überprüfe auch den Spam Folder wenn Du kein Mail erhältst. In wenigen Fällen kann es mehrere Stunden dauern bis zum Erhalt.</b>"; 
$LNG['signup_verification_email'] = "<pre> 
Lieber %USER%, 
Danke für Deine Anmeldung auf %SITE%. 
Um die Anmeldung abzuschliessen, klicke bitte den untenstehenden Bestätigungslink. 
<a href=\"%LINK%\">%LINK%</a> 
Website: %SITE% 
Dein %SITE% Team.</pre>"; 
$LNG['signup_verification_email_subject'] = "Registration auf %SITE%"; 
$LNG['silicon'] = "Silikon"; 
$LNG['single_girl'] = "Single Girl"; 
$LNG['sitemap'] = "Sitemap"; 
$LNG['smoker'] = "Rauchen"; 
$LNG['somewhere_else'] = "Anderswo"; 
$LNG['soon'] = "Bald"; 
$LNG['state'] = "Region"; 
$LNG['step_1'] = "Schritt 1"; 
$LNG['step_2'] = "Schritt 2"; 
$LNG['submit'] = "Abschicken"; 
$LNG['subscribe'] = "Anmelden"; 
$LNG['successfuly_logged_in'] = "Du bist nun eingeloggt."; 
$LNG['sucks'] = "Sucks"; 
$LNG['sunday'] = "Sonntag"; 
$LNG['swallow'] = "Schlucken"; 
$LNG['system_error'] = "System Fehler, bitte kontaktiere den Kundendienst."; 
$LNG['terms_and_conditions'] = "Allgemeine Geschäftsbedingungen"; 
$LNG['terms_text'] = " 
    <h1>Condizioni generali di contratto (CGC)</h1> 
    <h2>&sect; 1 Rapporto d’affari</h2> 
    <p>Le CGC regolano i rapporti tra gli utenti (di seguito &quot;utenti&quot;), i clienti (di seguito &quot;clienti&quot;) e la DA Products GmbH (di seguito „DA“), il gestore del sito Internet &quot;".NGE_SITE_TITLE."&quot; (di seguito „sito web“) e valgono per tutti i servizi e i prodotti offerti nel sito web, a seconda dei prodotti e dei servizi di seguito vengono indicate ulteriori condizioni contrattuali.</p> 
    <h2>&sect; 2 Direttive generali d’uso</h2> 
    <p>Il sito web è destinato a utenti che abbiano più di 18 anni.</p> 
    <p>L’età e altre informazioni personali contenute nel profilo dell’utente o cliente, in annunci, chat, forum o recensioni ecc. sono controllati dalla DA solo a campione; questo controllo non ha validità giuridica e non è vincolante. Ogni utente ha la responsabilità di controllare che i suoi partner abbiano compiuto la maggiore età in base alla legislazione in vigore nel proprio paese.</p> 
    <p>La DA non è responsabile per l’utilizzo delle informazioni personali rese accessibili nel sito web.</p> 
    <h2>&sect; 3 Dati utente / cliente, link di terzi, cookies</h2> 
    <p>All’atto dell’utilizzo e in particolare all’atto della registrazione e inserzione nel sito web la DA provvede a mettere in memoria alcune informazioni. Il cliente/utente può controllare e modificare i propri dati contenuti nel profilo e nelle inserzioni ogni qualvolta lo desideri. La DA inoltre mette in memoria in forma anonima i dati utente e cliente per i contatti, per la sicurezza tecnica del sistema, per motivi di statistica, di innovazione e per il miglioramento del prodotto. I dati non vengono messi a disposizione di terzi se non per la lotta agli abusi e in caso di contenuti vietati per legge. Con la registrazione e il conseguente utilizzo del sito web l’utente/cliente da il proprio <strong>consenso</strong> affinché i dati messi a disposizione possano essere <strong>elaborati e memorizzati</strong> per uso interno come consentito dalla legge sulla privacy. In caso di partecipazione a concorsi si provvederà ad indicare caso per caso se i dati vengono trasmessi a terzi per permettere lo svolgimento del concorso.</p> 
    <p>I contenuti di dati trasmessi dall’utente/cliente via Internet come e-mail („messaggi privati“), SMS o simili, non vengono sottoposti al controllo della DA. La DA non è responsabile per il contenuti di tali dati. La DA non utilizza indirizzi mail consultabili per l’invio di spam (le newsletter vengono spedite esclusivamente a utenti/clienti che richiedono espressamente questo servizio v. “Settings”) e non vende indirizzi mail a chi spedisce spam. Non è da escludersi che gli indirizzi mail consultabili pubblicamente vengano utilizzati in modo improprio da terzi, la DA tuttavia non si assume alcuna responsabilità in merito.</p> 
    <p>Il sito web contiene dei <strong>link</strong> per offerte di terzi. La DA non ha alcuna influenza sul trattamento che gli offerenti terzi fanno dei dati degli utenti raccolti sui propri siti web, l’utilizzo di questi siti è a rischio proprio dell’utente/cliente, la Da non si assume alcuna responsabilità per un utilizzo improprio o la diffusione dei dati.</p> 
    <p>Per poter adattare il sito web nel miglior modo possibile alle esigenze degli utenti/clienti, in certi casi vengono inseriti dei <strong>cookies</strong> (piccoli file memorizzati nel computer dell’utente/cliente per facilitare il ritrovamento del sito e il suo aggiornamento). I cookies non contengono dati personali. Configurando il browser in un certo modo è possibile rinunciare all’installazione dei cookies. Rinunciando ai cookies si può tuttavia provocare un peggioramento o l’impossibilità di accedere ai servizi e alle funzioni offerti dal sito web; la DA non si assume alcuna responsabilità in merito.</p> 
    <h2>&sect; 4 Forum e recensioni</h2> 
    <p>Con la registrazione al sito web e la partecipazione a forum e recensioni l’utente dichiara che i diritti d’autore sono suoi e che alla DA vengono conferiti i <strong>diritti d’uso</strong> per gli articoli e le recensioni. Non è possibile garantire la riservatezza dei forum e la pubblicazione degli articoli spediti. La DA non si assume alcuna responsabilità per la correttezza dei contenuti degli articoli e non tiene alcuna corrispondenza in merito.</p> 
    <p>Il contenuto della registrazione e degli articoli in forum e recensioni non deve contravvenire alle leggi svizzere né a quelle dei paesi dove articoli/recensioni vengono immessi nella rete. Contenuti con riferimenti a pornografia infantile, sesso con animali o escrementi e tutti i dati memorizzati dei relativi utenti e clienti vengono trasmessi subito e senza preavviso alle autorità penali competenti. L’utente assicura espressamente che i suoi articoli/recensioni non contravvengono a diritti d’autore, alla legislazione sulla tutela dei dati o al diritto della concorrenza e si impegna a mantenere la DA <strong>libera e indenne</strong> da rivendicazioni da parte di terzi che si basano su atti illegali dell’utente o su errori nel contenuto dei suoi articoli e delle informazioni ivi messe a disposizione e in generale su infrazioni alla legge, comprese le spese giudiziarie e di difesa.</p> 
    <p>L’utente si impegna a <strong>mantenere segreti</strong> i dati di login e le password, per evitare che persone non autorizzate (in particolare minorenni) abbiano accesso al sito web.</p> 
    <p>La DA ha il diritto di cancellare senza indicare i motivi e senza preavviso qualsiasi articolo. Nel caso in cui ci siano dei terzi che rivendicano in modo attendibile i diritti per contenuti di articoli di cui è responsabile l’utente, la DA può bloccare questi articoli e i relativi dati (anche per salvare le prove).</p> 
    <p>La <strong>cancellazione</strong> di utenti registrati avviene solo su richiesta scritta e può essere richiesto un controllo dell’identità. Da canto suo la DA può cancellare in qualsiasi momento e senza giustificazione registrazioni indebite.</p> 
    <p>La DA si riserva il diritto, ma non è suo dovere, di controllare il sito web e i profili, i contenuti e le offerte ivi pubblicate. Il blocco di tali dati è esclusivamente a discrezione della DA, l’utilizzo responsabile da parte dell’utente/cliente rimane espressamente valida.</p> 
    <h2>&sect; 5 Inserzioni e pubblicità su banner nel sito web</h2> 
    <p>L’inserzionista, in qualità di cliente, conferisce a DA l’incarico di mettergli a disposizione sul sito web, per un certo periodo, un numero desiderato di spazi per inserzioni commerciali o non commerciali (con o senza fotografia) o banner (attualmente le rubriche sono Escort, Boys, Trans, Discounted Girls, Tours, Directories interne, queste possono essere in qualsiasi momento ampliate o ridotte) in cambio del versamento del costo corrente dell’annuncio. La DA si riserva il diritto di modificare in qualsiasi momento il tipo, il volume, le condizioni e i costi delle inserzioni per le scadenze future.</p> 
    <p>Le inserzioni e i banner vengono concordati per iscritto o online sulla base di accordi separati sulle inserzioni, inoltre i regolamenti contenuti in questo paragrafo valgono come parte integrante degli accordi individuali. L’attivazione di incarichi viene confermata al cliente per e-mail (se questa è stata fornita) dopo l’avvenuto pagamento della tariffa. La DA si riserva il diritto, dopo aver controllato i dati del cliente o in caso di non avvenuto pagamento della tariffa dovuta per le inserzioni, di rifiutare l’incarico o di sospenderlo o stornarlo senza preavviso; ciò non solleva il cliente dal pagamento delle somme contrattualmente dovute per le prestazioni in scadenza. <em>Il pagamento può essere effettuato in franchi svizzeri o euro in contanti alla stipula del contratto, mediante bonifico anticipato o carta di credito.</em></p> 
    <p>I clienti si impegnano a fornire solo dati conformi a verità per quanto riguarda la loro persona e i servizi da loro offerti, essi sono responsabili dei dati pubblicati e si impegnano a indennizzare la DA in caso di rivalsa diretta (comprese eventuali spese di difesa e penali). Se vengono riscontrate violazioni le inserzioni possono essere cancellate senza diffida fino alla loro correzione o in casi gravi, in modo permanente senza diritto di rimborso delle somme già pagate. Contenuti con riferimenti a pornografia infantile, sesso con animali o escrementi e i dati memorizzati dei relativi utenti e clienti vengono trasmessi subito e senza preavviso alle autorità penali competenti.</p> 
    <p>Per quanto concerne le inserzioni viene applicato il <strong>diritto svizzero sulle commesse, luogo d’adempimento e foro competente è Zurigo.</strong></p> 
    <h2>&sect; 6 Diritti d’autore, modifica del sito web</h2> 
    <p>Tutti i file di testo, le immagini, gli elementi web e sonori, i loghi e la grafica di questo sito e di tutte le sue sottocartelle sono di proprietà intellettuale della DA e sono protetti da diritti d’autore. E’ vietato scaricare, copiare o utilizzare file aggirando o non rispettando le regole d’uso descritte nel presente contratto. Nel caso questa clausola non venga rispettata, l’utente/cliente incorre in un procedimento civile e penale con richiesta di risarcimento danni. La concessione di licenza deve essere autorizzata espressamente e per iscritto dalla DA. La DA può modificare il sito web in qualsiasi momento senza preavviso.</p> 
    <h2>&sect; 7 Responsabilità, hyperlinks</h2> 
    <p>La DA non è tenuta in alcun momento a garantire o a ripristinare l’accesso al sito, il sito web può essere disattivato in qualsiasi momento senza preavviso, anche a tempo indeterminato. Non si assume <strong>alcuna responsabilità</strong> per danni causati da hardware o software difettosi o da azioni di terzi (anche in caso di negligenza) o per danni di qualsiasi tipo conseguenti all’accesso (o all’impossibilità di accedere). Rimane salvo il rimborso di parte dei costi delle inserzioni in caso di impossibilità di accesso per un periodo prolungato per sola responsabilità della DA (appositamente o per colpa grave); l’impossibilità di accesso dovuta a manutenzione del sito non da diritto a rimborsi. Salvo ai clienti inserzionisti la DA non fornisce al alcun supporto utente.</p> 
    <p>Il sito web contiene link a siti internet esterni (cosiddetti „hyperlinks“). Inserendo questi link la DA non si appropria né dei siti internet, né dei loro contenuti. Per i contenuti dei siti nei link sono responsabili solo ed esclusivamente i rispettivi offerenti. Per contenuti sconosciuti non si fornisce alcuna garanzia, neanche per danni conseguenti e per la disponibilità dei siti linkati.</p> 
    <h2>&sect; 8 Scelta giuridica, foro competente</h2> 
    <p>Queste CGC e i relativi rapporti giuridici sottostanno al diritto svizzero. Il foro competente è Zurigo.</p> 
    <p><em>June 2006</em>, ci si riserva il diritto di apportare modifiche in ogni momento.</p> 
    <p>DA Products GmbH<br /> 
    Forchstrasse 113a<br /> 
    8127 Forch<br /> 
    Svizzera</p> 
    <p><a href='mailto:ricco171717@hotmail.com'>ricco171717@hotmail.com</a></p> 
"; 
$LNG['terni'] = "Terni"; 
$LNG['text_of_message'] = "Text der Nachricht"; 
$LNG['thank_you'] = "Vielen Dank"; 
$LNG['thanks_for_registration'] = "Vielen Dank für die Registration! Um die Registration abzuschliessen, überprüfe Deine Mailbox und folge den Anweisungen im Mail."; 
$LNG['this_escort_has'] = "Dieses Escort hat <strong>%RESULT_COUNT%</strong> Bewertung(en).<br />"; 
$LNG['third_party_escort_reviews'] = "Drittparteien Escort Bewertungen"; 
$LNG['thursday'] = "Donnerstag"; 
$LNG['time'] = "Zeit"; 
$LNG['title'] = "Titel"; 
$LNG['to'] = "Zu"; 
$LNG['to_change_use_menu'] = "Um Deine Daten / Einstellungen zu ändern benutze bitte Dein privates Menu oben (Icons)."; 
$LNG['today_new'] = "Heute neu"; 
$LNG['top20'] = "Girl des Monats"; 
$LNG['top20_desc'] = "Stimme für Dein Lieblingsgirl und mach es zum Girl des Monats! Das Girl mit den meisten Stimmen gewinnt jeweils den Titel Girl des Monats! 
<br /></br /> JETZT abstimmen! Die Bewertung reicht von 1 (schlechteste) zu 10 (beste) !"; 
$LNG['top_10_ladies'] = "Die Top 10 Girls"; 
$LNG['top_10_reviewers'] = "Die Top 10 Bewerter"; 
$LNG['top_30_ladies'] = "Die Top 30 Girls"; 
$LNG['top_30_reviewers'] = "Die Top 30 Bewerter"; 
$LNG['tour_add'] = "Tour hinzufügen"; 
$LNG['tour_already'] = "Du hast in dieser Zeit bereits eine Tour"; 
$LNG['tour_contact'] = "Tour Kontakt"; 
$LNG['tour_duration'] = "Tour Dauer"; 
$LNG['tour_email'] = "Tour Email"; 
$LNG['tour_empty_country'] = "Land kann nicht leer sein"; 
$LNG['tour_empty_from_date'] = "Von Datum kann nicht leer sein"; 
$LNG['tour_empty_to_date'] = "Bis Datum kann nicht leer sein"; 
$LNG['tour_girl'] = "Tour Girl"; 
$LNG['tour_location'] = "Tour Ort"; 
$LNG['tour_lower_date'] = "Bis Datum kann nicht kleiner sein als von Datum"; 
$LNG['tour_no_escorts'] = "Bitte zuerst Escort Profil eingeben "; 
$LNG['tour_phone'] = "Tour Telefon"; 
$LNG['tour_saved'] = "Tour Angaben gespeichert"; 
$LNG['tour_update'] = "Tour aktualisieren"; 
$LNG['tour_updated'] = "Tour Angaben aktualisiert"; 
$LNG['tours'] = "City Touren"; 
$LNG['trans'] = "Trans"; 
$LNG['trial'] = "Probe"; 
$LNG['trial_mb_desc'] = " 
<p>Du hast nun eine Trial/Probe Mitgliedschaft auf  ".NGE_SITE_TITLE.". In dieser Probezeit profitierst Du von diesen Features:</p> 
<ul> 
    <li>Profil auch auf der Hauptseite und nicht nur auf der Städteseite die Du gewählt hast</li> 
    <li>Users können Dich bewerten und Berichte über Dich schreiben</li> 
    <li>Teilnahme an den Top10 Reviews und bei Girl des Monats</li> 
    <li>Rotation Deines Profils. Das bedeutet keine Stagnation auf den hinteren Plätzen</li> 
</ul> 
<p>und vieles mehr !</p> 
"; 
$LNG['trial_mb_desc2'] = " 
<p>Nach der Trial/Probe Zeit kannst Du einen Premium Spot kaufen um weiterhin von allen Vorteilen zu profitieren. Plus profitierst Du von einem <strong style='color: red;'>Spezialangebot</strong> ! 
<ul> 
    <li>Du hast Zugriff auf die City Tour Features welche unzählige Promotionmöglichkeiten enthalten!</li> 
    </ul> 
<p><a href='mailto:".NGE_CONTACT_EMAIL."'>Zum bestellen hier klicken !</a></p> 
"; 
$LNG['trial_mb_desc_ag'] = "%COUNT% Escorts (1ste verfällt in %DAYS% Tagen)"; 
$LNG['trustability'] = "Vertrauenswürdigkeit"; 
$LNG['trusted_review_popup'] = " 
    <p>Du schickst uns detaillierte Informationen über das Treffen. Die Angaben leiten wir zum betreffenden Girl weiter. Das Girl sieht nicht ob Deine Bewertung positiv oder negativ ist solange sie das Treffen nicht bestätigt hat. Nachdem sie bestätigt hat, dass das Treffen stattgefunden hat, wird Dein Bericht als 100% Fake Free angezeigt.</p> 
    <p>Bitte gib und einige Informationen über Dich, das Girl und den Ort des Treffens. Diese Info ist nur für das Girl bestimmt und wird nirgends anders veröffentlicht. Das Girl wird diese Info per SMS oder Email erhalten. Zum Beispiel: Du hast per Email gebucht, dann brauchen wir die Email Adresse. Du hast per Telefon mit Deinem Namen (echt oder Fantasie) gebucht, brauchen wir die Zeit und den Namen etc. Du kannst es sehr kurz machen - hier zwei Beispiele:</p> 
    <p>Beispiele:</p> 
    <ol> 
        <li>gianni escortforum@hotmail.com, Montag 17.Jan 05 20:00 Uhr</li> 
        <li>pinky 333 333 454, Mittwoch 16. Feb. 05 um 08:00pm beim Hotel Ibis, Oerlikon</li> 
    </ol>"; 
$LNG['trusted_user_mail_subject'] = "%SITE% Trusted User"; 
$LNG['trusted_user_mail_text'] = "<pre> 
Lieber %NAME%, 
Danke für Deinen Beitrag auf %SITE% ! 
Du wurdest jetzt als Trusted User eingestuft und Deine Berichte müssen vom Seitenadministrator nicht länger überprüft / freigegeben werden. 
Willkommen in unserer Community - und viel Spass ! 
%SITE% Team.</pre>"; 
$LNG['tuesday'] = "Dienstag"; 
$LNG['tuscany'] = "Toscana"; 
$LNG['type'] = "Typ"; 
$LNG['ugly'] = "hässlich"; 
$LNG['unfriendly'] = "Unfreundlich"; 
$LNG['unsubscribe'] = "Abmelden"; 
$LNG['upcoming_tours'] = "Zukünftige Touren"; 
$LNG['update_languages'] = "Sprachen aktualisieren"; 
$LNG['update_locations'] = "Orte aktualisieren"; 
$LNG['update_rates'] = "Preise aktualisieren"; 
$LNG['upg_esc_memb'] = "Premium Member werden"; 
$LNG['upg_esc_memb_desc'] = "Danke für Deine Entscheidung, Premium Member auf ".NGE_SITE_TITLE." zu werden !<BR /> 
Bitte wähle die Art und die Dauer Deiner Mitgliedschaft die du erwerben möchtest und klicke dann auf den \"<B>UPGRADE NOW</B>\" Button  
um zur Zahlungsseite weitergeleitet zu werden:"; 
$LNG['upgrade_membership'] = "Upgrade der Mitgliedschaft"; 
$LNG['upgrade_now'] = "JETZT UPGRADEN!"; 
$LNG['upload_failed'] = "Fehler beim Hochladen des Fotos."; 
$LNG['upload_picture'] = "Foto hochladen"; 
$LNG['upload_private_photo'] = "Privatfoto hochladen"; 
$LNG['url'] = "URL"; 
$LNG['user'] = "Benutzer"; 
$LNG['username'] = "Benutzername"; 
$LNG['users_match'] = "User Matching Kriterium"; 
$LNG['vacation_add'] = "Ferien eintragen"; 
$LNG['vacation_end'] = "Zurück aus den Ferien"; 
$LNG['vacation_no_escorts'] = "Bitte zuerst Escort Profil eintragen "; 
$LNG['vacation_start'] = "Ferienbeginn"; 
$LNG['vacations'] = "Ferien"; 
$LNG['vacations_active'] = "%NAME% ist in den Ferien <strong>von %FROM_DATE% bis %TO_DATE%</strong>"; 
$LNG['vacations_active_soon'] = "%NAME% ist in den Ferien, wir zurück sein am <strong>bald</strong>"; 
$LNG['vacations_currently_date'] = "Das Girl ist momentan im Urlaub und wird am %DATE% zurück sein"; 
$LNG['vacations_currently_soon'] = "Dieses Girl ist momentan im Urlaub und wir zurück sein am <strong>bald</strong>."; 
$LNG['vacations_empty_to_date'] = "Das Rückkehrdatum der Ferien kann nicht leer sein"; 
$LNG['invalid_vacation_dates'] = "Vacation dates are not valid"; 
$LNG['vacations_lower_date'] = "Das Startdatum kann nicht früher sein als das Enddatum"; 
$LNG['vacations_return'] = "Zurück aus den Ferien"; 
$LNG['verification_email'] = "Lieber %USER%,vielen Dank für die Registration auf %SITE%.Um die Registration abzuschliessen bitte auf diesen Link klicken:"; 
$LNG['verification_email_subject'] = NGE_SITE_TITLE." Konto Verifizierung"; 
$LNG['very_simple'] = "Sehr einfach"; 
$LNG['view_gallery'] = "Gallerie anschauen"; 
$LNG['view_my_private_photos'] = "Meine privaten Fotos anschauen"; 
$LNG['view_my_profile'] = "Mein Profil anschauen"; 
$LNG['view_my_public_photos'] = "Meine öffentlichen Fotos anschauen"; 
$LNG['view_profile'] = "Profil anschauen"; 
$LNG['view_reviews'] = "Berichte anschauen/hinzufügen"; 
$LNG['view_site_escort_reviews'] = "Escort Berichte %SITE% anschauen"; 
$LNG['view_third_party_escort_reviews'] = "Drittpartei Escort Berichte anschauen"; 
$LNG['vip'] = "VIP"; 
$LNG['vip_mb_desc_ag'] = "%COUNT% Escorts"; 
$LNG['vote'] = "Abstimmen"; 
$LNG['vote_her_looks'] = "Ihr Aussehen bewerten"; 
$LNG['votes'] = "Stimmen"; 
$LNG['waist'] = "Taille"; 
$LNG['watch_list'] = "Favoritenliste"; 
$LNG['web'] = "Web"; 
$LNG['wednesday'] = "Mittwoch"; 
$LNG['week'] = "Woche"; 
$LNG['weeks'] = "Wochen"; 
$LNG['weight'] = "Gewicht"; 
$LNG['welcome'] = "Willkommen auf ".NGE_SITE_TITLE; 
$LNG['whats_new'] = "Was ist neu ?"; 
$LNG['when_you_met_her'] = "Wann hast Du sie getroffen"; 
$LNG['when_you_met_her_desc'] = "Datum und Zeit"; 
$LNG['when_you_met_lady'] = "Wann hast Du das Girl getroffen"; 
$LNG['where_did_you_meet_her'] = "Wo hast Du sie getroffen"; 
$LNG['where_you_met_her'] = "Wo habt ihr euch getroffen"; 
$LNG['where_you_met_her_desc'] = "Hotel, Ihre Wohnung, Deine Wohnung"; 
$LNG['white'] = "Weiss"; 
$LNG['whole'] = "Ganze"; 
$LNG['wlc_city'] = "Arbeitsort (Stadt)"; 
$LNG['wlc_country'] = "Arbeitsort (Land)"; 
$LNG['women'] = "Frauen"; 
$LNG['working_locations'] = "Arbeitsorte"; 
$LNG['working_time'] = "Arbeitszeit"; 
$LNG['write_here_name_of_site'] = "Bitte in den untenstehenden Feldern den Namen und die URL der Seite wo das Girl gelistet ist eintragen."; 
$LNG['wrong_login'] = "Falsches Login"; 
$LNG['wrong_login_or_password'] = "Falsches Login oder Passwort"; 
$LNG['wrong_time_interval'] = "Falscher Zeitintervall bei der Arbeitszeit am %DAY%."; 
$LNG['year'] = "Jahr"; 
$LNG['years'] = "Jahre"; 
$LNG['yellow'] = "Gelb"; 
$LNG['yes'] = "Ja"; 
$LNG['yes_more_times'] = "Ja, mehrere Male"; 
$LNG['you_are_premium'] = "Du bist ein Premium Mitglied"; 
$LNG['you_have_log'] = "Du musst eingeloggt sein um dieses Feature zu benutzen. <a href='%LINK%'>Zum einloggen hier klicken</a> oder <a href='%LINK2%'>Hier neu anmelden.</a>"; 
$LNG['your_comment_added'] = "Dein Kommentar wurde erfolgreich hinzugefügt."; 
$LNG['you_have_voted_for'] = "Du hast für %ESCORT% abgestimmt! Vielen Dank."; 
$LNG['you_r_watching_agencies'] = "Diese <b>Agenturen</b> sind auf Deiner Favoritenliste:"; 
$LNG['you_r_watching_escorts'] = "Diese <b>escorts</b> sind auf Deiner Favoritenliste:"; 
$LNG['you_r_watching_users'] = "Diese <b>users</b> sind auf Deiner Favoritenliste:"; 
$LNG['your_booking_text'] = "Dein Buchungstext"; 
$LNG['your_data'] = "Deine Angaben"; 
$LNG['your_discount'] = "Dein Rabatt: %DISCOUNT%%"; 
$LNG['your_full_name'] = "Dein vollständiger Name"; 
$LNG['your_info'] = "Deine Info"; 
$LNG['your_info_desc'] = "Email, Telefon, Name"; 
$LNG['your_login_name'] = "Nickname"; 
$LNG['your_name'] = "Dein Name"; 
$LNG['your_report'] = "Dein Bericht"; 
$LNG['zip'] = "Postleitzahl"; 
$LNG['69'] = "69"; 
// 2006/12/11 
$LNG['tours_in_other_countries'] = "Tours in anderen Ländern"; 
$LNG['brunette'] = "Brunett"; 
$LNG['hazel'] = "Nussbraun"; 
$LNG['profile_or_main_page_links_only'] = "Links nur zu Deinem Profil / Hauptseite Deiner Agentur oder Deiner persönlichen Seite."; 
$LNG['competitors_links_will_be_deleted'] = "Jegliche Werbung oder Verlinkung zu Konkurrenzseiten werden vom Administrator gelöscht!"; 
$LNG['domain_blacklisted'] = "Domain auf der Blacklist"; 
$LNG['phone_blocked'] = "Telefonnummer ist blockiert"; 
$LNG['girls'] = "Girls"; 
$LNG['inform_about_changes'] = "Vielen Dank für die Newsletter Anmeldung. Sie werden zukünftig über Neuerungen und Aktionen auf " . NGE_SITE_TITLE . " informiert."; 
$LNG['escort_photos_over_18'] = "All escorts were 18 or older at the time of depiction."; 
$LNG['captcha_please_enter'] = "Please enter"; 
$LNG['captcha_here'] = "here"; 
$LNG['captcha_verification_failed'] = "Text verification failed."; 
$LNG['my_awards'] = "Meine Auszeichnungen"; 
$LNG['escort_girls'] = "Escort Girls"; 
$LNG['locally_available_in_countries'] = "Wir sind auch lokal in folgenden Ländern präsent"; 
$LNG['domina'] = "Domina"; 
$LNG['dominas'] = "Dominas"; 
$LNG['club'] = "Club"; 
$LNG['club_name'] = "Club Name"; 
$LNG['club_profile'] = "Club Profil"; 
$LNG['studio'] = "Studio"; 
$LNG['studio_girls'] = "Studio Girls"; 
$LNG['all_regions'] = "All regions..."; 
$LNG['see_all_escorts_of_this_agency'] = "Alle girls dieser Agentur anzeigen"; 
$LNG['see_all_escorts_of_this_club'] = "Alle girls dieses Studio anzeigen"; 
$LNG['invalid_height'] = "Ungültiges Grösse"; 
$LNG['invalid_weight'] = "Ungültiges Gewicht"; 
$LNG['subscribe_to_newsletter'] = "Newsletter abonnieren!"; 
$LNG['inform_every_week'] = "Erhalten Sie regelmässige Infos zu neuen Girls, Aktionen und Neuerungen auf " . NGE_SITE_TITLE; 
$LNG['clubs_match'] = "Studio Suchkriterium"; 
switch (APPLICATION_ID) 
{ 
    case 1: // escortforumit.com 
        $LNG['index_bottom_text'] = "Da noi trovi le piu belle accompagnatrici, escort, girl, annunci, di tutta Italia, inoltre abbiamo annunci di escort, girl, accompagnatrici, nelle maggiori città d'Italia Roma, Milano, Torino, Bologna, Napoli, Firenze! Cosa aspetti contatta le accompagnatrici piu belle che non hai mai trovatoa Roma, Bologna, Firenze, Napoli, Milano, Torino, e in tutte le altre città d'Italia. Troverai sempre gli annunci, escort, girl e le accompagnatrici piu belle ditutta Italia sempre aggiornate."; 
        break; 
    case 2: // escort-annonce.com 
        $LNG['index_bottom_text'] = "Bienvenue dans Escort annonce, le plus grand annuaire d'annonces de rencontres d'Escorts girl /boys/ trans en France et en Europe. Vous trouverez des Escort à Paris Toulouse Lyon Marseille et toutes les grandes villes de France où vous pourrez faire une rencontre sexe entre adultes consentants. Vous pourrez aussi prendre connaissance des appréciations sur les Escort girl rencontrées par les autres membres, et aussi le tchat pour voir en direct via la webcam l'Escort girl de votre choix avant de la rencontrer. Pour une rencontre furtive d'une Escort girl après le bureau, Escort annonce est le site où vous pourrez revenir plusieurs fois par jour pour voir les nouvelles girls inscrites près de chez vous pour des rencontres les plus chaudes. Si vous rencontrez ou voyez une annonce d'escort qui ne correspond pas à la réalité, n'hésitez pas nous contacter, nous ferons notre possible pour n'avoir que des vraies annonces pour faire de vraies rencontres coquines."; 
        break; 
    case 3: // hellasescorts.com 
        $LNG['index_bottom_text'] = "Στο " . NGE_SITE_TITLE . " θα βρείτε callgirl και τις πιο καυτές και σέξυ συνοδούς & από την Αθήνα, συνοδούς από �?εσσαλονίκη, συνοδούς από Πάτρα και όλες τις μεγάλες πόλεις της Ελλάδας."; 
        break; 
         
    case 4: // escortchicas.com 
        $LNG['welcome_description'] = "We are the largest Network for erotic contacts in Europe! Find today your escapade, your companionship, your sexual adventure – in Spain and Europe! We update our site daily. All girls with pictures and contact details! You can register for FREE and benefit from many advantages! If you have any question or uncertainity, please contact us directly: <a href='mailto:" . NGE_CONTACT_EMAIL . "'>" . NGE_CONTACT_EMAIL . "</a>"; 
        $LNG['index_bottom_text'] = "Estàs buscando relax en barcelona o un relax en madrid? En nuestro portal tenemos contactos escorts en todas las ciudades de Espana, relax en valencia, relax en sevilla o en las islas tambien tenemos anuncios de relax en mallora, ibizao las islas canarias. Diferentes anuncios escorts de agencias, escorts independenties y travestis. Si no buscas los que quieres encontrar en otros portales aqui lo encuentraras seguramente. Mira los anuncios y encuentra en tu ciudad las mas calientes mujeres."; 
        break; 
    case 6: // sexindex.ch 
        $LNG['welcome_description'] = "dem grössten Netzwerk für erotische Kontakte, Sex Clubs und Escort in Europa! Auf der Suche nach einem Bordell, einer Ferienbegleitung, einem Seitensprung, einem Escort Girl oder guten Sex Clubs? Bei uns werden Sie fündig - täglich aktuelle Kontaktanzeigen (Setcards) mit Fotos und Bewertungen. Melden Sie sich GRATIS an und profitieren Sie von vielen Vorteilen und Vergünstigungen! Bei Fragen oder Problemen wenden Sie sich bitte direkt an: <a href='mailto:" . NGE_CONTACT_EMAIL . "'>" . NGE_CONTACT_EMAIL . "</a>"; 
        $LNG['advertise_now'] = "<a href='" . NGE_URL . "/advertising.php' rel='nofollow'>Werben Sie gratis auf Sexindex.ch!</a> Auskunft unter <a href='mailto:" . NGE_CONTACT_EMAIL . "'>" . NGE_CONTACT_EMAIL . "</a> oder Telefon 078 899 14 89"; 
        $LNG['autumn_action'] = " 
        <div style='overflow: hidden; width: 755px; padding: 5px 10px; font-size: 10px; line-height: 1.1; background: #f5f5f5; border: 1px solid #ccc;'> 
            <div style='float: left; width: 360px;'> 
                <p><big>Sexindex.ch Herbst Aktion<br /> 
                <span style='color: #DA1E05;'>Werbung die sich auszahlt – über 120,000 Besucher pro Monat</span></big></p> 
                <p>Profitieren Sie von unserer Aktion und schalten Sie noch heute Ihre Premium Sexanzeige<br /> 
                (Premium = Topplatzierung, immer an oberster Stelle gelistet).</p> 
                <p style='color: #DA1E05;'>15 TAGE PREMIUM INSERAT CHF 100.00<br /> 
                30 TAGE PREMIUM INSERAT CHF 200.00</p> 
            </div> 
            <div style='margin: 0 0 0 385px;'> 
                <p>Es geht ganz einfach. Senden Sie uns folgende Angaben per <a href='mailto:" . NGE_CONTACT_EMAIL . "'>Email</a> zu und Ihre Sedcard wird umgehend aufgeschaltet:</p> 
                <ul> 
                    <li>Dauer des Inserates</li> 
                    <li>Ihre Fotos und Kontaktdaten (Telefon und/oder E-Mail)</li> 
                    <li>Gewünschter Name und Inseratetext</li> 
                    <li>Ihr Arbeitsort / Stadt</li> 
                    <li>Ihre Webseite (URL) (optional)</li> 
                </ul> 
                <p>Für mehr Informationen oder Fragen zu diesem Angebot oder zur normalen Gratiswerbung auf Sexindex.ch kontaktieren Sie uns wie gewohnt per <a href='mailto:" . NGE_CONTACT_EMAIL . "'>Email</a> oder Telefon 078 899 14 89.<br /> 
                Das Inkasso erfolgt nachträglich (bar oder Banküberweisung).</p> 
            </div> 
        </div> 
        "; 
        $LNG['escort_girls_description'] = "All diese attraktiven Ladies besuchen Sie zu Hause, im Büro oder im Hotel. Oder sind Sie auf der Suche nach einer Ferienbegleitung? Swingerclubbegleitung? Hier werden Sie sicher fündig! Täglich aktuell."; 
        $LNG['independent_girls_description'] = "Auf der Suche nach einem ganz diskreten Abenteuer? Hier inserieren Girls die Sie in absolut privater Atmosphäre empfangen – entweder in einer Privatwohnung oder einem kleinen Salon ohne Bar/Empfang. Diskret und privat!"; 
        $LNG['studio_girls_description'] = "Täglich aktuell finden Sie hier die heissesten Girls aus den besten Etablissements der ganzen Schweiz."; 
         
        $LNG['index_bottom_text'] = "Gemäss Statistik sind Begriffe die etwas mit Sex und Erotik zu tun haben die meist gesuchten im Internet. 98 Prozent der Männer waren gemäss Umfragen schon mindestens einmal auf Pornoseiten. Beliebt sind auch Suchbegriffe wie Kontaktanzeigen, Bordell, Sex Clubs, Escort, ficken, Transen etc. In der Schweiz gibt es eine Vielzahl an Sex Seiten und Sexführern. Und manchmal ist es schwierig den Ueberblick zu behalten. Denn wirklich gute Sex-Angebote, wo man auch das findet was man sucht, sind rar. Oder ist es Ihnen nicht auch schon so ergangen? Sie tippen bei einer Suchmaschine den Begriff „Bordell“ ein und landen, statt auf einem Sexführer, auf einer kostenpflichtigen Seite mit Kontaktanzeigen? Oder Sie haben Lust auf schnellen Sex und suchen ein Bordell in Ihrer Nähe wo Sie richtig geil ficken können? Statt dessen werden Sie auf eine Seite geführt wo Sie nur ein kleines Sexangebot haben und wenige Escort Girls / Sex Clubs aufgeführt sind. Wie gesagt – es ist ziemlich schwierig. Einige Seiten haben sich etabliert und decken gewisse Sparten ab. Für Transen Kontakte zum Beispiel eignet sich das Portal happysex hervorragend. Denn happysex hat in den meisten Regionen der Schweiz Kontaktanzeigen bzw. Sexanzeigen von Transen. Auch Bordelle, Escort Girls und Sex Clubs werden aufgeführt. Jedoch nimmt happysex im direkten Vergleich zu anderen Sex Seiten vom Design und Aufbau her einer der hinteren Plätze ein. Bei uns auf Sexindex legen wir wert auf Aktualität der Sex Kontakt Anzeigen und Sedcards der Escort Girls. Wir nehmen regelmässig neue Bordelle, Sexstudios, private Frauen, Escorts und Salons in unsere Datenbank auf. Alle Sedcards sind mit Fotos und Details und sind einheitlich präsentiert. Auch die Werbung versuchen wir dezent zu halten – jedoch brauchen wir die Banner um für Sie weiterhin ein gratis Sexführer zu bleiben. In diesem Sinne – ficken ist die schönste Nebensache der Welt – und ob Sie auf sexindex oder happysex fündig werden spielt schlussendlich keine Rolle. Happy hunting!"; 
        break; 
} 
$LNG['escort_girls_in_city'] = "Escort Girls in %CITY%"; 
$LNG['independent_girls_in_city'] = "Private Girls in %CITY%"; 
$LNG['studio_girls_in_city'] = "Studio Girls in %CITY%"; 
$LNG['new_arrivals_in_city'] = "Neue Girls in %CITY%"; 
$LNG['all_girls_in_city'] = "All girls in %CITY%"; 
$LNG['see_all_girls_from_agency'] = "click to see all girls from this agency"; 
$LNG['see_all_independent_girls'] = "click to see all independent girls"; 
$LNG['last_modified'] = "zuletzt geändert"; 
$LNG['advertise_with_us'] = "Werben Sie <span class='free'>GRATIS</span>"; 
$LNG['nearest_cities_to_you'] = "Nearest cities to you"; 
$LNG['nearest_cities_to_city'] = "Nearest cities to %CITY%"; 
$LNG['cities_in_region'] = "Cities in %REGION%"; 
$LNG['local'] = "Local"; 
$LNG['local_directory'] = "Club/Escort Verzeichnis"; 
$LNG['exchange_banners'] = "If do you want interchange banners and/or links with us, email us to <a href='mailto:" . NGE_CONTACT_EMAIL . "'>" . NGE_CONTACT_EMAIL . "</a>"; 
$LNG['our_network_in_europe'] = "Our network in Europe"; 
$LNG['agencies_and_independent_in_spain'] = "Agencies and independent girls in Spain"; 
$LNG['directories_and_guide_in_spain'] = "Directories, forum and erotic guide in Spain"; 
$LNG['friends'] = "Friends"; 
$LNG['escort_service'] = "Escort Service"; 
$LNG['studio_and_private_girls'] = "Studio und private Girls"; 
$LNG['advertising'] = "Werbung"; 
$LNG['premium_girls'] = "Premium Girls"; 
$LNG['submit_form_to_signup'] = "Submit this form to signup and get Promoted from largest escort directory!"; 
$LNG['signup_now_to_largest_escort_directory'] = "Signup now for <span class='pink'>FREE</span> to the largest escort directory worldwide. We currently have over <span class='pink'>3 million clients</span> monthly on our network. Start using our popularity to promote yourself now <span class='pink'>FOR FREE!</span>"; 
$LNG['signing_on_all_local_sites'] = "By signing up, your profile will automatically be displayed on all these local country sites. Your login data will work on all the sites - you just have to signup once."; 
$LNG['please_choose_your_business'] = "Bitte wählen Sie Ihr Geschäftsbereich"; 
$LNG['independent_privat'] = "Privat Girl / independent Escort"; 
$LNG['escort_agency'] = "Escort Agentur"; 
$LNG['club_bordell_strip'] = "Club / Bordell / Sauna Club / Stripclub (Tabledance), Massage Studio"; 
$LNG['please_fill_form'] = "Bitte füllen Sie folgendes Formular aus"; 
$LNG['please_write_numbers_from_field'] = "Please write the numbers like in the field"; 
$LNG['just_moment_away_from_escort_directory'] = "You are just moment away from having access to largest escort directory!"; 
$LNG['choose_your_region'] = "Wähle deine Region"; 
$LNG['setcards_online'] = "%COUNT% Setcards online"; 
$LNG['only_escorts'] = "nur Escorts"; 
$LNG['only_private_girls'] = "nur Private Girls"; 
$LNG['only_studio_girls'] = "nur Studio Girls"; 
$LNG['most_popular_cities'] = "Most popular cities"; 
$LNG['renew_your_profile'] = "Your profile is expired. If you would like to renew it, contact your Sales person or write us at <a href='mailto:" . NGE_CONTACT_EMAIL . "'>" . NGE_CONTACT_EMAIL . "</a>"; 
$LNG['relax'] = "Relax"; 
$LNG['email_unsubscribed'] = "E-mail %EMAIL% is removed from " . NGE_SITE_TITLE . " newsletter."; 
$LNG['vip_girls'] = "VIP girls"; 
$LNG['crop_photo'] = "Crop photo"; 
$LNG['show_thumbnails_on'] = "Show thumbnails on"; 
$LNG['photo_was_cropped'] = "Photo was cropped."; 
$LNG['crop_feature_teaser'] = " 
    <p><strong style='color: red;'>ACHTUNG:</strong><br /> 
    Wir haben ein neues Feature eingeführt. Alle Hauptfotos werden von nun an in der selben Grösse angezeigt. Darum bitten wir Dich, Dein Hauptbild zu überprüfen. Bitte folge den Instruktionen unter dem Menupunkt &quot;Gallerie bearbeiten&quot;.</p> 
"; 
$LNG['crop_feature_description'] = " 
    <p><strong style='color: red;'>ACHTUNG:</strong><br /> 
    Mit dem neuen Feature erscheinen alle Hauptfotos auf Sexindex.ch in der selben Grösse. Dies ergibt ein einheitliches Bild und dient dem Look der Seite.</p> 
    <p>Wie es funktioniert:</p> 
    <ol> 
        <li>Überprüfe jetzt Dein Hauptfoto (das Foto, welches auf " . NGE_SITE_TITLE . " als erstes angezeigt wird). Wenn alles gut aussieht musst Du nicht weiterlesen. Falls das Foto verschoben ist lese bitte weiter.</li> 
        <li>Klicke auf &quot;Crop Photo&quot;</li> 
        <li>In der Mitte des Fotos erscheint ein Rahmen welcher zeigt, wie das Foto dargestellt wird. Du kannst nun den Rahmen mit der Maus so verschieben, bis du mit der Darstellung zufrieden bist. Dann klicke auf &quot;Crop the Image&quot;.</li> 
        <li>Fertig! Der ausgewählte Bereich wird nun als Hauptfoto angezeigt. Du kannst zur Gallerie zurückkehren.</li> 
    </ol> 
"; 
$LNG['we_only_sell_advertisement'] = NGE_SITE_TITLE . " ist eine Werbe- und Informationsplattform und hat als solche sonst keine Verbindung oder Verantwortung zu/für die auf der Seite aufgeführten Personen / Agenturen. Wir agieren lediglich als Verkäufer von Werbeflächen, sind selbst weder eine Escort Agentur noch ein Club oder sonstwie ins Escort- bzw. Prostitutionsgeschäft involviert. Für die Inhalte externer Links sowie für die auf unserer Seite aufgeführten Kontaktangaben (Email, Telefon) lehnen wir jegliche Verantwortung ab."; 
$LNG['user_messages_review_waiting_for_approval'] = "Your review about %MESSAGE% is waiting for verification. Please be patient"; 
$LNG['user_messages_favorite_girl_review'] = "Your favorite girl <strong>%MESSAGE%</strong> has new review. <a href='%LINK%'>click here to see</a>"; 
$LNG['user_messages_review_approval'] = "Your review <strong>%MESSAGE%</strong> was approved by administrator."; 
$LNG['user_messages_review_deleted'] = "Your review <strong>%MESSAGE%</strong>  was deleted by administrator. Reason given: %LINK%"; 
$LNG['user_messages_new_discount'] = "%MESSAGE% is offering you a special EF discount. <a href='%LINK%'>Check her out!</a>"; 
$LNG['user_messages_deleted_discount'] = "%MESSAGE% is not offering you a special EF discount anymore."; 
$LNG['user_messages_favorite_disabled'] = "Your favorite girl <strong>%MESSAGE%</strong> is no longer with us! We are sorry! Maybe time to find a new favorite?"; 
$LNG['user_messages_favorite_activated'] = "Your favorite girl <strong>%MESSAGE%</strong> joined us again!"; 
$LNG['user_messages_girl_of_month'] = "Girl of the month %MONTH%/%YEAR% is: <strong>%MESSAGE%</strong>!! Congratulation %MESSAGE%!!!"; 
$LNG['user_messages_free_fuck_winner'] = "<strong>You are the WINNER of the freefuck lottery contact us.</strong>"; 
$LNG['user_messages_free_fuck_others'] = "The winner of the free fuck lottery of month %MONTH%/%YEAR% is: <strong>%MESSAGE%</strong>!! Congratulation %MESSAGE%!!!"; 
$LNG['user_messages_favorite_region_girls'] = "New girls from your favourite region joined EF since your last visit. %LINK%"; 
$LNG['user_messages_favorite_region_vip'] = "There is new VIP %MESSAGE% from your favourite region since your last visit. %LINK%"; 
$LNG['user_email_review_approved'] = "Hello %TARGET_0%\nYour review %TARGET_1% was approved by administrator.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['escort_email_review_approved'] = "Hello %TARGET_0%\nReview %TARGET_1% about you was approved by administrator.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_favorites_review_approved'] = "Hello %TARGET_0%\nYour favorite girl  %TARGET_1% has new review.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['agency_email_review_approved'] = "Hello %TARGET_0%\nReview about %TARGET_1% was approved by administrator.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_review_approval_favorites'] = NGE_SITE_TITLE . " new review"; 
$LNG['user_email_subject_review_approval'] = NGE_SITE_TITLE . " review approved"; 
$LNG['escort_email_subject_review_approval'] = NGE_SITE_TITLE . " review approved"; 
$LNG['agency_email_subject_review_approval'] = NGE_SITE_TITLE . " review approved"; 
$LNG['escort_email_review_new'] = "Hello %TARGET_0%\nNew review %TARGET_1% about you is waiting for approval.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['agency_email_review_new'] = "Hello %TARGET_0%\nYour escort %TARGET_1% has new review waiting for approval.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_review_new'] = "Hello %TARGET_0%\nYour review about %TARGET_1% is waiting for verification. Please be patient.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_review_new'] = NGE_SITE_TITLE . " review waiting for approval"; 
$LNG['escort_email_subject_review_new'] = NGE_SITE_TITLE . " new review waiting for approval"; 
$LNG['agency_email_subject_review_new'] = NGE_SITE_TITLE . " new review waiting for approval"; 
$LNG['user_email_review_deleted'] = "Hello %TARGET_0%\nYour review <strong>%TARGET_1%</strong>  was deleted by administrator. Reason given: %TARGET_2%\n" . NGE_SITE_TITLE . " Team"; 
$LNG['escort_email_review_deleted']	= "Hello %TARGET_0%\nThe review <strong>%TARGET_1%</strong> about you was deleted by administrator. Reason given: %TARGET_2%\n" . NGE_SITE_TITLE . " Team"; 
$LNG['agency_email_review_deleted']	= "Hello %TARGET_0%\nThe review about <strong>%TARGET_1%</strong> was deleted by administrator. Reason given: %TARGET_2%\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_review_delete'] = NGE_SITE_TITLE . " review deleted"; 
$LNG['escort_email_subject_review_delete'] = NGE_SITE_TITLE . " review deleted"; 
$LNG['agency_email_subject_review_delete'] = NGE_SITE_TITLE . " review deleted"; 
$LNG['user_email_discount_new'] = "Hello %TARGET_0%\n%TARGET_1% is offering you a special EF discount.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['escort_email_discount_new'] = "Hello %TARGET_0%\nNew discount was assigned to you.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['agency_email_discount_new'] = "Hello %TARGET_0%\n%TARGET_1% is now offering a special EF discount.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_discount_new'] = NGE_SITE_TITLE . " new discount"; 
$LNG['escort_email_subject_discount_new'] = NGE_SITE_TITLE . " new discount"; 
$LNG['agency_email_subject_discount_new'] = NGE_SITE_TITLE . " new discount"; 
$LNG['user_email_discount_deleted']	= "Hello %TARGET_0%\nYour escort %TARGET_1% is not offering a special EF discount anymore.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_discount_deleted'] = NGE_SITE_TITLE . " discount expired"; 
$LNG['user_email_escort_deactivated'] = "Hello %TARGET_0%\nYour favorite girl <strong>%TARGET_1%</strong> is no longer with us! We are sorry! Maybe time to find a new favorite?\n" . NGE_SITE_TITLE . " Team"; 
$LNG['escort_email_escort_deactivated'] = "Hello %TARGET_0%\nYour account has been deactivated.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['agency_email_escort_deactivated'] = "Hello %TARGET_0%\nAccount of %TARGET_1% has been deactivated.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_favorites_deactivated'] = NGE_SITE_TITLE . " escort deactivated"; 
$LNG['escort_email_subject_favorites_deactivated'] = NGE_SITE_TITLE . " account deactivated"; 
$LNG['agency_email_subject_favorites_deactivated'] = NGE_SITE_TITLE . " account deactivated"; 
$LNG['user_email_escort_activated'] = "Hello %TARGET_0%\nYour favorite girl <strong>%TARGET_1%</strong> joined us again!\n" . NGE_SITE_TITLE . " Team"; 
$LNG['escort_email_escort_activated'] = "Hello %TARGET_0%\nYour account has been activated.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['agency_email_escort_activated'] = "Hello %TARGET_0%\nAccount of %TARGET_1% has been activated.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['agency_email_subject_favorites_activated'] = NGE_SITE_TITLE . " escort activated"; 
$LNG['agency_email_subject_favorites_activated'] = NGE_SITE_TITLE . " account activated"; 
$LNG['agency_email_subject_favorites_activated'] = NGE_SITE_TITLE . " account activated"; 
$LNG['user_email_girl_of_the_month'] = "Hello %TARGET_0%\nGirl of the month %TARGET_1% is: <strong>%TARGET_2%</strong>!! Congratulation %TARGET_2%!!!\n" . NGE_SITE_TITLE . " Team"; 
$LNG['escort_email_girl_of_the_month'] = "Hello %TARGET_0%\nGirl of the month %TARGET_1% is: <strong>%TARGET_2%</strong>!! Congratulation %TARGET_2%!!!\n" . NGE_SITE_TITLE . " Team"; 
$LNG['escort_email_girl_of_the_month_winner'] = "Hello %TARGET_0%\nYou are new girl of the month %TARGET_1%! Congratulation %TARGET_0%!!!\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_girl_of_the_month'] = NGE_SITE_TITLE . " girl of the month"; 
$LNG['escort_email_subject_girl_of_the_month'] = NGE_SITE_TITLE . " girl of the month"; 
$LNG['user_email_free_fuck_winner'] = "Hello %TARGET_0%\n<strong>You are the WINNER of the freefuck lottery contact us.</strong>\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_free_fuck_others'] = "Hello %TARGET_0%\nThe winner of the free fuck lottery of month %TARGET_1% is: <strong>%TARGET_2%</strong>!! Congratulation %TARGET_2%!!!\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_free_fuck_winner'] = NGE_SITE_TITLE . " freefuck lottery winner"; 
$LNG['user_email_subject_free_fuck_others'] = NGE_SITE_TITLE . " freefuck lottery"; 
$LNG['user_email_new_girls_in_region'] = "Hello %TARGET_0%\nNew girls from your favourite region joined EF.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_new_girls_in_region'] = NGE_SITE_TITLE . " new girls"; 
$LNG['user_email_new_vip_girls_in_region'] = "Hello %TARGET_0%\nThere is new VIP %TARGET_1% from your favourite region since your last visit. %TARGET_2%\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_new_vip_girls_in_region'] = NGE_SITE_TITLE . " new VIP girls"; 
$LNG['loft'] = 'Loft'; 
$LNG['loft_escorts_photo_limit_reached'] = "Loft escorts can have max %LIMIT% photos in gallery."; 
$LNG['loft_escorts_cant_go_on_tour'] = "Loft escorts can't go on tour."; 
//CUBIX Added By David 
$LNG['email_exists'] = "Email exists"; 
/* design2 texts */ 
$LNG['members_login'] = "Members Login"; 
$LNG['signup_now'] = "Jetzt anmelden"; 
$LNG['live_now'] = "Jetzt live"; 
$LNG['girl_has_video'] = "Girl mit Video"; 
$LNG['girl_has_reviews'] = "Girl wurde bewertet"; 
$LNG['100%_real_photos'] = "100% echte Fotos"; 
$LNG['happy_hour'] = "Happyhour"; 
$LNG['icons_explanation'] = "Symbol Erklärung"; 
$LNG['offer_incall'] = "Incall"; 
$LNG['offer_outcall'] = "Outcall"; 
$LNG['ebony_only'] = "Ebony only"; 
$LNG['asian_only'] = "Asian only"; 
$LNG['blondes_only'] = "Blondes only"; 
$LNG['add_to_my_favourites'] = "Zu Favoriten hinzufügen"; 
$LNG['remove_from_favourites'] = "Von Favoriten entfernen"; 
$LNG['add_view_reviews'] = "Bewertungen ansehen/hinzufügen"; 
$LNG['about_escort'] = "Über %SHOWNAME%"; 
$LNG['contact_me'] = "Kontaktiere mich"; 
$LNG['now_open'] = "Jetzt geöffnet"; 
$LNG['closed'] = "Closed"; 
$LNG['viewed'] = "Viewed"; 
$LNG['quick_links'] = "Quick links"; 
$LNG['free_signup'] = "Gratis Anmeldung"; 
$LNG['signup_profit'] = " 
<ul> 
    <li><strong>Kommentare</strong> schreiben</li> 
    <li class='fsa-odd'><strong>Favoritenliste</strong> list</li> 
    <li><strong>Bewertungen</strong> schreiben</li> 
    <li class='fsa-odd'><strong>Detailsuche</strong></li> 
    <li><strong>Watchlist</strong></li> 
</ul> 
"; 
$LNG['sort_by'] = "Sortiert nach"; 
$LNG['random'] = "zufällig"; 
$LNG['alphabetically'] = "alphabetisch"; 
$LNG['most_viewed'] = "am meisten angesehen"; 
$LNG['newest_first'] = "neueste zuerst"; 
$LNG['narrow_your_search'] = "Suche verfeinern"; 
$LNG['with_reviews'] = "mit Bewertungen"; 
$LNG['search_by_name_agency'] = "Girl/Club Suche"; 
$LNG['comments_responses'] = "Kommentare &amp; Antworten"; 
$LNG['post_comment'] = "Kommentar schreiben"; 
$LNG['back'] = "Zurück"; 
$LNG['reply'] = "Antworten"; 
$LNG['spam'] = "Spam"; 
$LNG['only_registered_users_can_add_comments'] = "Nur registrierte Mitglieder können Kommentare schreiben. Bitte <a href='%VALUE%'>hier einloggen</a>."; 
$LNG['your_comment'] = "Dein Kommentar"; 
$LNG['close'] = "Schliessen"; 
$LNG['message_waiting_for_approval'] = "Vielen Dank. Deine Mitteilung wartet auf die Freigabe vom Seitenadmin."; 
$LNG['ago'] = 'vor'; 
$LNG['time_ago'] = 'vor %TIME%'; 
$LNG['yesterday'] = "Gestern"; 
$LNG['minute'] = "Minute"; 
$LNG['spanking'] = "Spanking"; 
$LNG['domination'] = "Domination"; 
$LNG['fisting'] = "Fisting"; 
$LNG['massage'] = "Massage"; 
$LNG['role_play'] = "Rollenspiele"; 
$LNG['bdsm'] = "BDSM"; 
$LNG['hardsports'] = "Hardsports"; 
$LNG['hummilation'] = "Humiliation"; 
$LNG['rimming'] = "Rimming"; 
$LNG['less_than'] = "weniger als %NUMBER%"; 
$LNG['more_than'] = "mehr als %NUMBER%"; 
$LNG['free_signup_advantages'] = "Die Anmeldung auf  <span class='red'>" . NGE_SITE_TITLE . "</span> ist absolut <span class='red-big'>GRATIS</span> und bietet viele Vorteile."; 
$LNG['reset'] = "Zurücksetzen"; 
$LNG['private_area'] = "Memberbereich"; 
$LNG['choose_your_choice'] = "Auswählen"; 
$LNG['base_city'] = "Stadt"; 
$LNG['additional_cities'] = "Zusätzliche Städte"; 
$LNG['zone'] = "Zone"; 
$LNG['actual_tour_data'] = "JETZT in %CITY% (%COUNTRY%)"; 
$LNG['upcoming_tour_data'] = "in %DAYS% in %CITY% (%COUNTRY%), von %FROM_DATE% - %TO_DATE%"; 
$LNG['beta_warning'] = NGE_SITE_TITLE . " befindet sich noch in der BETA-Phase. Das heisst, wir arbeiten ständig an der Seite und machen regelmässige Updates und Verbesserungen."; 
$LNG['ethnic_white'] = "Caucasian (white)"; 
$LNG['ethnic_black'] = "Black"; 
$LNG['ethnic_asian'] = "Asian"; 
$LNG['ethnic_latin'] = "Latin"; 
$LNG['ethnic_african'] = "African"; 
$LNG['ethnic_indian'] = "Indian"; 
$LNG['members_signup'] = "Members signup"; 
$LNG['click_here_your_signup'] = "Click here for your singup."; 
$LNG['your_email'] = "Your e-mail"; 
$LNG['missing_message'] = "Missing message"; 
$LNG['your_message_sent'] = "Your message was sent."; 
$LNG['signup'] = "Signup"; 
$LNG['signup_to_largest_escort_directory'] = "Melden Sie sich jetzt <span class='red-big'>GRATIS</span> an und profitieren von unserem internationalen Netzwerk mit über <span class='red-big'>3 Millionen Usern</span> pro Monat."; 
$LNG['one_signup_all_sites'] = "Bei einer Anmeldung wird Ihr Profil automatisch auch auf all den obgenannten lokalen Länder-Seiten ersichtlich sein. Ihre Login Daten funktionieren auf allen Seiten – d.h. Sie brauchen sich nur einmal zu registrierern!"; 
$LNG['email_verification_subject'] = NGE_SITE_TITLE . " account verification"; 
$LNG['email_verification_text'] = "<b>Dear %USER%,</b><br /><br /> 
<b>Thank you for registering at " . NGE_SITE_TITLE . "</b>.<br /><br /> 
You are 1 step away from activating your account, simply click on the link below or copy and paste it into your browser.<br /><br /> 
<b><a href='%LINK%' style='color: #dd0000;'>%LINK%</a></b><br /><br /> 
NOTE: <span style='font-size: 11px;'>if you experience any trouble, please contact us at <b><a href='mailto:" . NGE_CONTACT_EMAIL . "' style='color: #dd0000;'>" . NGE_CONTACT_EMAIL . "</a></b>.</span><br /><br /> 
<b>Yours faithfully</b><br /> 
<b>" . NGE_SITE_TITLE .  " Team</b>"; 
$LNG['email_confirmation_subject'] = NGE_SITE_TITLE . " registration"; 
$LNG['email_confirmation_text'] = "Dear %USER%,</b><br /><br /> 
Thank you for registering at " . NGE_SITE_TITLE . ".<br /> 
Your registration is now complete and you are an active member of " . NGE_SITE_TITLE . ".<br /><br /> 
<b>Following is your login information. Please keep this information on a safe place.<br /> 
<span style='display: block; float: left; width: 75px;'>Login:</span> <span style='color: #dd0000;'>%USER%</span><br /> 
<span style='display: block; float: left; width: 75px;'>Web:</span> <a href='" . NGE_URL . "' style='color: #dd0000;'>" . NGE_URL . "</a></b><br /><br /> 
Have fun on the site!<br /><br /> 
<b>Yours faithfully</b><br />  
<b>" . NGE_SITE_TITLE . " Team</b>"; 
//CUBIX Added By David 
$LNG['contact_us_text1'] = "In case if you wish to contact us regarding general enquiries, feedback or advertising, choose from the following options"; 
$LNG['phone_international'] = "Phone International"; 
$LNG['contact_us_text2'] = "Alternatively please use our contact form below"; 
$LNG['your_message'] = "Your Message"; 
$LNG['link_exschange'] = "Link Exchange"; 
$LNG['contact_us_text3'] = "If you would like to exchange links, please email the webmaster (e-mail).<br />Please copy our banner and place it on your server before contacting us regarding link exchange."; 
// 
//Cubix Added By Tiko 
$LNG['delete_profile'] = "Delete Profile"; 
$LNG['confirm_delete_profile'] = "Are you sure you want to delete the escort profile ?"; 
$LNG['delete_escort'] = "Delete escort profile"; 
$LNG['escort_profile_deleted'] = "Escort profile has been successfuly deleted"; 
$LNG['escort_profile_deleted_error'] = "Error: Escort profile has not been deleted"; 
$LNG['same_city'] = 'Working city and tour city can not be same.'; 
$LNG['overnight'] = 'overnight'; 
$LNG['directory_for_female_escorts'] = NGE_SITE_TITLE . ' is a UK escort directory for female escorts, escort agencies, independent escorts, trans and couples. 
On escortguide.co.uk you can find the perfect escort in London, Manchester, Leeds and all other areas in the United Kingdom. 
We update our escort directory on a daily basis - you will find new female escorts in london and from your area every day. 
If you are an escort agency or a female independent escort based in London or the UK, simply click on the 
signup link to register and upload your escort profile to escortguide.co.uk. '; 
//new search 
//new search 
$LNG['all_origins'] = "alle ..."; 
$LNG['popularity_views'] = "Anzahl / Klicks";  
$LNG['last_modified_date'] = "Änderungsdatum"; 
$LNG['registered_date'] = "Registrierungsdatum"; 
$LNG['word'] = "wort"; 
$LNG['phrase'] = "ganzer Satz"; 
$LNG['colour'] = "farbe ..."; 
$LNG['doesnt_matter'] = "Doesn't matter"; 
$LNG['specific_words'] = "Stichwörter"; 
$LNG['kissing_no'] = "No Kissing"; 
$LNG['blowjob_no'] = "No Blowjob"; 
$LNG['cumshot_no'] = "No Cumshot"; 
$LNG['cumshot_in_mouth_spit'] = "Cumshot In Mouth Spit"; 
$LNG['cumshot_in_mouth_swallow'] = "Cumshot In Mouth Swallow"; 
$LNG['cumshot_in_face'] = "Cumshot In Face"; 
$LNG['with_comments'] = "mit Kommentaren"; 
$LNG['nickname'] = "Nickname"; 
$LNG['without_reviews'] = "ohne Bewertung"; 
$LNG['not_older_then'] = "nicht alter als"; 
$LNG['order_results'] = "Sortieren nach"; 
$LNG['select_your_girl_agency_requirements'] = "Wählen Sie aus folgenden Optionen"; 
$LNG['girls_details_bios'] = "Girls details/bio's"; 
$LNG['cm'] = "cm"; 
$LNG['ft'] = "ft"; 
$LNG['kg'] = "kg"; 
$LNG['lb'] = "lb"; 
$LNG['girls_origin'] = "Nationalität"; 
$LNG['ethnicity'] = "Ethnizität"; 
$LNG['measurements'] = "Masse"; 
$LNG['open_hours'] = "Open hours"; 
$LNG['my_favorites'] = "My Favorites";
$LNG['learn_more_webcam'] = "If you have a webcam plus an account at
Skype or MSN, this is the easiest way to
get your photos verified.
Just click on the 'Webcam session request' link,
choose a date and a time and wait for our
confirmation. At the requested time you will
then have an short online appointment (video chat)
with one of our adminstrators to approve that it's
you on the photos. After this session your
photos will be 100% verified. ";
$LNG['learn_more_passport'] = "If you choose this option you need to scan your
passport or any other official document such as
identity card or driving license and upload it
to our system along with 1-2 additional private photos.";

$LNG['verify_welcome'] = "Welcome!";
$LNG['verify_welcome_text'] = "Welcome to the 100% verified picture process.
Use the chance and get your profile pictures to be
100% verified genuine. In just a few simple
steps you will increase the credibility of your profile
and get many more users. You will get also a sticker
to your profile which will show to the users that your photos
are 100% genuine. Additionally your profile will be
listed in an extra sort by menu. ";
$LNG['verify_enroll'] = "Enroll now!";
$LNG['verify_enroll_text'] = "We provide currently the following <strong>2 options</strong> to get your pictures 100% verified:";
$LNG['verify_enroll_1'] = "Webcam session request with our admin";
$LNG['verify_enroll_2'] = "Passport copy";
$LNG['verify_learn_more'] = "Learn more...";
$LNG['verify_start_now'] = "<span>START NOW </span> the process";
$LNG['verify_steps'] = "STEP GUIDLINE:";
$LNG['verify_select_2_options'] = "Please select one of the following 2 options for the process:";
$LNG['verify_select_1'] = "Webcam session request with our admin";
$LNG['verify_select_1_note'] = "<strong>note:</strong> you need to have a webcam and an account at Skype or MSN ";
$LNG['verify_select_2'] = "Passport copy - you need a valid passport or identity card";
$LNG['verify_select_2_note'] = "<strong>note:</strong> to speed up the process we recommend to upload 1-2 private photos in addition to the passport copy. ";
$LNG['verify_important'] = "IMPORTANT - PLEASE READ CAREFULLY: Any attempt of fraud or tricking the process will result in a lifelong
banning from the EF network. All your paid advertisements will be disabled and no refund given. ";

$LNG['verify_sms_message'] = "Videochat confirmation for the photo verification process: 
Date: #date#, Time: #time#. You will be contacted by our administrator. #title#";
$LNG['verify_email_subject'] = "Online appointment confirmation";
$LNG['verify_email_message'] = "Hereby we confirm your online appointment for the photo verification process at:<br /><br />
Date: #date#<br />
Time: #time#<br /><br />
You will be contacted by one of our administrators. Kind regards.<br />#title#";

$LNG['reject_sms_message'] = "Please choose 3 new dates for the videochat session (photo verification). We could not confirm your requested dates. Just login and choose again. #title#";
$LNG['reject_email_subject'] = "100% photo verification process / please enter new dates";
$LNG['reject_email_message'] = "Dear #showname#<br /><br />
Unfortunately all suggested dates for the video chat session to verify your pictures could not been confirmed. Please login again to your private area and choose three new
dates. We would like to appologize for any inconvenience caused.<br /><br />
Kind regards<br />
#title#";
$LNG['reg_has_been_succ'] = "Ihre Registrierung war erfolgreich !";
$LNG['verify_attention'] = "Important note: For uploading the photos please use solely the form below. Do not send the photos by e-mail. Only send an e-mail to the sales representative in case you experience any problems uploading the pictures. ";