<?php 
include(NGE_LNG_DIR . "/cities_es.php"); 
include(NGE_LNG_DIR . "/city_zones.php"); 
include(NGE_LNG_DIR . "/countries_es.php"); 
include(NGE_LNG_DIR . "/languages_es.php"); 
include(NGE_LNG_DIR . "/nationalities_es.php"); 
include(NGE_LNG_DIR . "/regions_es.php"); 
$LNG['width_comments'] = "Width Comments"; 
$LNG['100_fake_free_review'] = "100% libre de fraude"; 
$LNG['100_fake_free_review_desc'] = "Experiencia 100% confiable es cuando la agencia o la chica confirmaron por sms que el encuentro tuvo lugar como se describe y está completamente libre de fraude!"; 
$LNG['24_7'] = "24/7"; 
$LNG['PROD_additional_city'] = "Ciudad adicional"; 
$LNG['PROD_city_premium_spot'] = "Espacio premium de ciudad"; 
$LNG['PROD_girl_of_the_month'] = "Chica del Mes"; 
$LNG['PROD_international_directory'] = "Directorio internacional"; 
$LNG['PROD_main_premium_spot'] = "Espacio Premium principal"; 
$LNG['PROD_national_listing'] = "Lista nacional"; 
$LNG['PROD_no_reviews'] = "Sin experiencias"; 
$LNG['PROD_search'] = "Buscar"; 
$LNG['PROD_tour_ability'] = "Disponibilidad de tour"; 
$LNG['PROD_tour_premium_spot'] = "&quot;En tour&quot; espacio premium"; 
$LNG['_all'] = "Todas"; 
$LNG['about_me'] = "Sobre mí"; 
$LNG['about_meeting'] = "Sobre este encuentro"; 
$LNG['account_activated'] = "Su cuenta ha sido exitosamente activada, por favor iNicie sesión ahora y agregue su perfil. <a href='".NGE_URL."/login.php$QUERY_STRING'>Haga click aquí</a> para hacer login."; 
$LNG['account_blocked_desc'] = "<p>Su membresía ha sido suspendida por una de las siquientes razones:</p> 
<ul> 
<li>insultar usuarios o escorts</li> 
<li>discriminacion de usuarios o escorts</li> 
<li>escribir falsas experiencias o temas</li> 
<li>instigacion de usuarios en contra de alguien o algo</li> 
<li>hacer promoción obvia de agencias o escorts</li> 
</ul> 
<p>Si lo desea, puede enviarnos sus comentarios o declaración al respecto a: <a href='mailto:".NGE_CONTACT_EMAIL."'>".NGE_CONTACT_EMAIL."</a></p> 
<p>NOTA: por favor no se registre otra vez y escriba en nuestro sitio web, pues eliminaremos todos sus temas y experiencias y bloquearemos su IP</p>"; 
$LNG['account_not_verified'] = "Esta cuenta no ha sido verificada aún ."; 
$LNG['account_type'] = "Tipo de cuenta"; 
$LNG['active'] = "Activa"; 
$LNG['add'] = "Agregue"; 
$LNG['add_escort'] = "Agregue perfil de escort"; 
$LNG['add_escort_profile'] = "Agregar perfil de escort"; 
$LNG['add_profile_now'] = "Añade tu anuncio ahora"; 
$LNG['add_review'] = "Agregar experiencia"; 
$LNG['add_third_party_girl'] = "Agregue una chica en tercera persona a la base de datos"; 
$LNG['add_watch'] = "Agregar a lista de favoritos"; 
$LNG['add_watch_success'] = "<b>%NAME%</b> fue exitosamente agregado a su lista de favoritos. <a href='javascript:history.back()'>Haga click aquí para regresar.</a>"; 
$LNG['additional'] = "Adicional"; 
$LNG['address'] = "Calle y número"; 
$LNG['age'] = "Edad"; 
$LNG['age_between'] = "Edades entre"; 
$LNG['agencies_match'] = "Agencias que cumplen estos criterios"; 
$LNG['agency'] = "Agencia"; 
$LNG['agency_city_tour_not_allowed'] = "Solamente las chicas que NO están en periodo de prueba aparecen en esta lista. El Tour de Ciudad no se permite para chicas en periodo de prueba. Por favor obtenga un espacio Premium para que las chicas tengan acceso a esta herramienta. Haga click <a href='mailto:".NGE_CONTACT_EMAIL."'>aquí para ordenar</a>."; 
$LNG['agency_data'] = "Datos de la agencia"; 
$LNG['agency_denied_review'] = "denegada"; 
$LNG['agency_denied_review_desc'] = "La agencia dijo que no han tenido un cliente que concuerde con los datos provistos, por lo general esta es una revision falsa y no debe confiar en esta, ya que la agencia o la chica no saben si la experiencia sería positiva o negativa antes de confirmarla!"; 
$LNG['agency_girls'] = "Agencias de escorts"; 
$LNG['agency_name'] = "Nombre de la agencia"; 
$LNG['agency_profile'] = "Perfil de agencia"; 
$LNG['agency_without_name'] = "Agencia sin nombre"; 
$LNG['ajaccio'] = "Ajaccio"; 
$LNG['all'] = "Todos..."; 
$LNG['all_cities'] = "Todas las ciudades..."; 
$LNG['all_countries'] = "Todos los países..."; 
$LNG['all_escorts'] = "Todas las escorts"; 
$LNG['all_origins'] = "Todos los orígenes..."; 
$LNG['all_reviews'] = "Todas las experiencias"; 
$LNG['already_registered_on_another_site'] = "Si usted ya está registrado con %SITE%, no necesita registrarse otra vez. Simplemente inicise su sesión en %SITE% usando su nombre de usuario y contraseńa."; 
$LNG['already_reviewed'] = "Usted ya ha escrito su experiencia sobre esta chica! Solamente es posible escribir una experiencia sobre una chica, pero puede actualizar su <a href='%LINK%'>experiencia existente</a> si así lo desea!"; 
$LNG['already_voted'] = "Usted ya ha votado por esta chica."; 
$LNG['also_provided_services'] = "Ella también dio estos servicios"; 
$LNG['anal'] = "Anal"; 
$LNG['are_you_escort'] = "Eres una escort independiente, tienes una agencia o un Club privado?"; 
$LNG['at_her_apartment'] = "En su apartamento"; 
$LNG['at_her_hotel'] = "En su hotel"; 
$LNG['at_my_flat_or_house'] = "En mi casa/apartamento"; 
$LNG['at_my_hotel'] = "En mi hotel"; 
$LNG['attention'] = "Atención !"; 
$LNG['attitude'] = "Actitud"; 
$LNG['availability'] = "Disponibilidad"; 
$LNG['available_for'] = "Disponible para"; 
$LNG['basic'] = "Básico"; 
$LNG['back_to_actual_tours'] = "Regresar al tour actual"; 
$LNG['become_premium_member_and_view_private_photos'] = "<a href='%LINK%'>Obtenga su membresía Premium AHORA</a> y vea fotos especiales y sin censura de esta chica!"; 
$LNG['bio'] = "Biografia"; 
$LNG['birth_date'] = "Fecha de nacimiento"; 
$LNG['black'] = "Negro"; 
$LNG['blond'] = "Rubio"; 
$LNG['blowjob'] = "Sexo oral"; 
$LNG['blowjob_with_condom'] = "Sexo oral con condón"; 
$LNG['blowjob_without_condom'] = "Sexo oral sin condón"; 
$LNG['blue'] = "Azul"; 
$LNG['bombshellf'] = "preciosa"; 
$LNG['book_email_to_escort'] = "<pre> 
Estimada %ESCORT%, 
   un Miembro Premium de ".NGE_SITE_TITLE." desea conocerte en %DATE%. 
Este usuario está reclamando su descuento de %DISCOUNT%%, el cual le ofreces a nuestros usuarios de confianza. 
Detalles: 
Usuario: %USERNAME% 
Nombre completo: %FULLNAME% 
Fecha deseada: %DATE% 
Duración: %DURATION% 
Teléfono: %PHONE% 
Email: %EMAIL% 
Comentarios: %COMMENTS% 
Código de seguridad: %CODE% 
Este código es usado para verificar al usuario. El usuario debe darle a usted este código si usted lo pide, 
para verificar que tiene derecho a reclamar un descuento. 
Deseándole lo mejor, 
<a href=\"".NGE_URL."\">".NGE_SITE_TITLE."</a> 
</pre>"; 
$LNG['book_this_girl'] = "Contrate a esta chica"; 
$LNG['booking_desc'] = "Gracias por su interés en contratar a %NAME%. Como un Miembro Premium usted obtiene un descuento de %DISCOUNT%% por contratarla. Por favor use la forma abajo para contratar a esta chica y reclamar su descuento!"; 
$LNG['booking_mail_subject'] = "%SITE% contratación"; 
$LNG['booking_mail_text'] = "<pre> 
Estimado %NAME%, 
usted ha contratado exitosamente a %ESCORT_NAME% para una cita en %DATE%. 
Esta chica se pondrá en contacto con usted próximament para últimar los detalles de vuestra cita. 
Como un Miembro Premium de %SITE%, usted tiene el derecho a reclamar su descuento de %DISCOUNT%% del precio de esta chica. 
En caso de problemas, por favor guarde este número de identificación: %VERIFICATION_ID% 
Este número puede ser usado para verficar que usted es un Miembro Premium y obtener su descuento. 
Le deseamos un rato fantástico con la chica de su elección! 
El equipo de %SITE%</pre>"; 
$LNG['boys'] = "Chicos"; 
$LNG['boys_trans'] = "Chicos/Travestis"; 
$LNG['breast'] = "Senos"; 
$LNG['breast_size'] = "Talla de brasiere"; 
$LNG['brown'] = "Castano"; 
$LNG['bruntal'] = "BruntĂˇl"; 
$LNG['bust'] = "Busto"; 
$LNG['by'] = "por"; 
$LNG['calabria'] = "Calabria"; 
$LNG['can_have_max_cities'] = "Usted puede tener un máximo de %NUMBER% ciudades definidas"; 
$LNG['cancel'] = "Cancelar"; 
$LNG['cant_be_empty'] = "no puede estár vacío"; 
$LNG['cant_go_ontour_to_homecity'] = "Escorts no pueden ir de tour a su ciudad de base."; 
$LNG['castelfranco'] = "Castelfranco"; 
$LNG['chalons-en-champagne'] = "Chalons-en-Champagne"; 
$LNG['change_all_escorts'] = "Cambiar todos los perfiles de escort"; 
$LNG['change_passwd'] = "Cambie contraseńa"; 
$LNG['char_desc'] = "tatuajes etc."; 
$LNG['characteristics'] = "Caraterísticas"; 
$LNG['chiavari'] = "Chiavari"; 
$LNG['choose'] = "Elija..."; 
$LNG['choose_availability'] = "Choose availability..."; 
$LNG['choose_another_city'] = "Elija otra ciudad"; 
$LNG['choose_another_region'] = "Choose another region"; 
$LNG['choose_city'] = "Elija una ciudad..."; 
$LNG['choose_country'] = "Elija un país..."; 
$LNG['choose_currency'] = "Elija tipo de moneda..."; 
$LNG['choose_escort'] = "Elija escort..."; 
$LNG['choose_escort_to_promote'] = "Elija un escort a promover"; 
$LNG['choose_language'] = "Elija un idioma..."; 
$LNG['choose_level'] = "Elija nivel..."; 
$LNG['choose_ethnic'] = "Choose ethnic..."; 
$LNG['choose_nationality'] = "Elija nacionalidad..."; 
$LNG['choose_state'] = "Elija provincia..."; 
$LNG['choose_this_user'] = "Elija este usuario"; 
$LNG['choose_time'] = "Elija hora..."; 
$LNG['choose_your_city'] = "Elija su ciudad"; 
$LNG['choose_your_country'] = "Elija su país"; 
$LNG['choosen_escort'] = "Escort elegida: %NAME%"; 
$LNG['cities'] = "Ciudades"; 
$LNG['city'] = "Ciudad"; 
$LNG['city_escort_notify'] = "Infórmese por email si una escort de su ciudad es agregada"; 
$LNG['city_of_meeting'] = "Ciudad del encuentro"; 
$LNG['claim_your_discount'] = "Reclame su descuento AHORA y contrate a esta chica"; 
$LNG['comments'] = "Comentarios"; 
$LNG['comments_to_her_services'] = "Commentarios sobre sus servicios"; 
$LNG['comments_to_her_services_desc'] = "Describa su parte sexual, for ejemplo servicios especiales (S/M, dildos, etc)."; 
$LNG['company_name'] = "Nombre de compańia"; 
$LNG['conf_password'] = "Confirme su contraseńa"; 
$LNG['confirmation_email_1'] = "Estimado %USER%, gracias por registrarse con %SITE%. A continuación hallará su información de Inicio de sesión:"; 
$LNG['confirmation_email_2'] = "Por favor, guarde esta información en un lugar seguro."; 
$LNG['confirmation_email_subject'] = NGE_SITE_TITLE." registración"; 
$LNG['contact'] = "Contacto"; 
$LNG['contact_her'] = "Contacte a esta chica directamente"; 
$LNG['contact_her_desc'] = "Está contactando a %NAME%. Este email vendrá del dominio de %SITE% y será certificado, para expresar vuestra confiabilidad como un Miembro Premium de %SITE%"; 
$LNG['contact_her_mail_header'] = "<pre> 
************************************************************************************* 
Este es un email certificado de un Miembro Premium de %SITE%. 
Este usuario es un miembro confiable de nuestra comunidad y paga su membresía regularmente. 
*************************************************************************************</pre>"; 
$LNG['contact_her_mail_subject'] = "Mensaje de un Miembro Premium de %SITE%"; 
$LNG['contact_info'] = "Información de contacto"; 
$LNG['contact_text'] = " 
La comunidad de escorts más grande de ".$LNG['country_'.MAIN_COUNTRY]." ahora ofrece espacios para anunciarse en nuestro sitio web. Hemos limitado el número de 
sitios que se pueden anunciar a 10 - lo cual significa que estos banners recibirán un alto número de clicks.<BR /> 
<BR /> 
Algunas estadísticas:<BR /> 
<BR /> 
Hits -> 3 millones o más al día como promedio<BR /> 
Visitantes unicos -> 25,000 o más al día como promedio<BR /> 
Members -> Casi 15,000, creciendo diariamente<BR /> 
<BR /> 
Si le gustaría aprovechar esta oportunidad y discutir los detalles, simplemente contáctenos. Le responderemos inmediatamente.<BR /> 
<BR /> 
<a href='mailto:".NGE_CONTACT_EMAIL."'>".NGE_CONTACT_EMAIL."</a> 
Tel.: 0036 1203 1808 (10:00 - 20:00) 
</pre> 
"; 
$LNG['contact_us'] = "Contacto / anuncios"; 
$LNG['continue'] = "Continuar"; 
$LNG['conversation'] = "Conversación"; 
$LNG['countries'] = "Países"; 
$LNG['country'] = "País"; 
$LNG['country_tw'] = "Taiwan"; 
$LNG['couples'] = "Parejas"; 
$LNG['created'] = "Creado"; 
$LNG['cumshot'] = "Semen sobre ella"; 
$LNG['cumshot_on_body'] = "Semen sobre el cuerpo"; 
$LNG['cumshot_on_face'] = "Semen sobre la cara"; 
$LNG['curr_password'] = "Contraseńa actual"; 
$LNG['curr_password_empty_err'] = "Usted debe ingresar su contraseńa actual"; 
$LNG['currency'] = "Tipo de moneda"; 
$LNG['currently_listed_sites'] = "En estos momentos tenemos aquí la lista de los sitios web de donde escoger. Por favor seleccione uno."; 
$LNG['date'] = "Fecha"; 
$LNG['day'] = "día"; 
$LNG['day_of_meeting'] = "Fecha del encuentro"; 
$LNG['day_phone'] = "Teléfono"; 
$LNG['days'] = "días"; 
$LNG['default_city'] = "Ciudad por defecto mostrada después de Iniciar la sesión"; 
$LNG['delete_rates'] = "Eliminar todos los precios"; 
$LNG['delete_sel_pcis'] = "Eliminar las fotos seleccionadas"; 
$LNG['delete_selected_reviews'] = "Eliminar las experiencias seleccionadas"; 
$LNG['delete_selected_tour'] = "Eliminar el tour seleccionado"; 
$LNG['desired_date'] = "Fecha deseada"; 
$LNG['discount'] = "Descuento"; 
$LNG['discount_code'] = "Código de descuento"; 
$LNG['discount_code_txt'] = "Para reclamar su descuento del 5%, por favor entre su código de descuento, si ha recibido uno de nuestros asociados."; 
$LNG['discount_desc'] = "En está página le presentamos a todas las chicas que están ofreciendo un <b>descuento</b> a nuestros Miembros Premium."; 
$LNG['discounted_girls'] = "Chicas con descuento"; 
$LNG['dont_forget_variable_symbol'] = " 
    <p>Por favor, NO OLVIDE incluir la variable: <big>%ESCORT_ID%</big>.<br /> 
Sin él, no podemos identificar su pago.<br /> 
Gracias.</p> 
"; 
$LNG['dont_know'] = "No sé"; 
$LNG['dress_size'] = "Talla de vestido"; 
$LNG['duration'] = "Duración"; 
$LNG['easy_to_get_appointment'] = "Fácil de hacer una cita"; 
$LNG['edit'] = "Cambiar"; 
$LNG['edit_escort'] = "Modifique perfil de escort"; 
$LNG['edit_gallery'] = "Modificar galería"; 
$LNG['edit_languages'] = "Cambiar idiomas"; 
$LNG['edit_locations'] = "Cambiar localidades"; 
$LNG['edit_private_photos'] = "Cambiar fotos privadas"; 
$LNG['edit_private_photos_desc'] = "Por favor suba aquí fotos sin censura (que no escondan ninguna parte la foto) o de glamour (fotos expeciales, que no quiere ofrecerle al público en general), las cuales solamente nuestros miembros Premium con membresía pagada podrán ver. Al hacer esto, tus fotografias especiales solamente las podrán ver clientes de clase alta, los cuales ya han expresado su interes, al pagar su membresía."; 
$LNG['edit_profile'] = "Actualizar perfil"; 
$LNG['edit_rates'] = "Cambiar precios"; 
$LNG['edit_review'] = "Actualizar experiencia"; 
$LNG['edit_settings'] = "Actualizar preferencias"; 
$LNG['edit_units_in'] = "Modificar las unidades en"; 
$LNG['email'] = "E-mail"; 
$LNG['email_domain_blocked_err'] = "Lo sentimos, %DOMAIN% no está aceptando emails nuestros en estos momentos. Por favor elija una dirección de email diferente o contacte a %DOMAIN% para que acepte nuestra correspondencia."; 
$LNG['email_lng'] = "Idioma de los emails"; 
$LNG['email_not_registered'] = "Este email no está registrado en nuestro sitio web."; 
$LNG['empty_login_err'] = "Login no puede estár vacío."; 
$LNG['empty_passwd_err'] = "Contraseńa no puede estár vacía."; 
$LNG['empty_watchlist'] = "No hay ningún articulo en su lista de favoritos"; 
$LNG['english'] = "Inglés"; 
$LNG['enthusiastic'] = "Entusiasta"; 
$LNG['error'] = "Error"; 
$LNG['error_adding_watch'] = "Un error ocurrió cuando intentó agregar/remover un articulo de su lista de favoritos. Por favor contacte al administrador de este sitio web."; 
$LNG['esc_mb_chng_subject'] = "Cambio de membresía en ".NGE_SITE_TITLE; 
$LNG['esc_mb_chng_to_free_email'] = "<pre>Estimado %LOGIN%, 
la membresía de su perfil de escort con nombre %SHOWNAME% ha sido cambiada a \"gratis\". 
Usted ha perdido acceso a las ventajas de una membresía pagada. 
Mejore su membresía ahora y obtenga: 
- participación en la rotación de chicas en la página principal, 
lo cual significa que usted nunca se quedará en la última página, donde 
los usuarios no la verian. Todos los perfiles pagados son rotados cada hora, 
por lo que todos los perfiles tienen la misma oportunidad de aparecer en la primera página de una 
ciudad en particular o en la página principal de un sitio web 
- usted podrá participar en la competencia de la \"Chica del Mes\" 
- usted tendrá la oportunidad de ser incluída en la lista TOP10 de chicas con experiencias 
Para mejorar su membresía usted puede responder a este email o Iniciar sesión en nuestro 
sitio web y hacer click en  \"Mejore su membresía Ahora\". 
El equipo de ".NGE_SITE_TITLE." 
</pre> 
"; 
$LNG['esc_mb_chng_to_normal_email'] = "<pre>Estimado %LOGIN%, 
la membresía de su perfil de escort bajo el nombre %SHOWNAME% ha sido mejorada a  \"normal\". 
Nueva fecha de expiracion: %EXPIRE% 
A partir de ahora usted se beneficiara de las siguientes ventajas: 
- participación en la rotación de chicas en la página principal, 
lo cual significa que usted nunca se quedará en la última página, donde 
los usuarios no la verian. Todos los perfiles pagados son rotados cada hora, 
por lo que todos los perfiles tienen la misma oportunidad de aparecer en la primera página de una 
ciudad en particular o en la página principal de un sitio web 
- usted podrá participar en la competencia de la \"Chica del Mes\" 
- usted tendrá la oportunidad de ser incluída en la lista TOP10 de chicas con experiencias 
El equipo de ".NGE_SITE_TITLE." 
</pre> 
"; 
$LNG['esc_mb_chng_to_premium_email'] = "<pre>Estimado %LOGIN%, 
la membresía de su perfil de escort con nombre %SHOWNAME% ha sido mejorada a \"premium\". 
Nueva fecha de expiracion: %EXPIRE% 
A partir de ahora usted se beneficiara de las siguientes ventajas: 
- BAJO CONSTRUCCION 
El equipo de ".NGE_SITE_TITLE." 
</pre> 
"; 
$LNG['esc_mb_chng_to_trial_email'] = "<pre>Estimado %LOGIN%, 
la membresía de su perfil de escort con nombre %SHOWNAME% ha sido cambiada a \"trial\". 
Nueva fecha de expiracion: %EXPIRE% 
Le hemos dado una membresía de prueba en ".NGE_SITE_TITLE.". Durante este periodo, usted 
recibira todos los beneficios de una membresía pagada, la cual incluye: 
- participación en la rotación de chicas en la página principal, 
lo cual significa que usted nunca se quedará en la última página, donde 
los usuarios no la verian. Todos los perfiles pagados son rotados cada hora, 
por lo que todos los perfiles tienen la misma oportunidad de aparecer en la primera página de una 
ciudad en particular o en la página principal de un sitio web 
- usted podrá participar en la competencia de la \"Chica del Mes\" 
- usted tendrá la oportunidad de ser incluída en la lista TOP10 de chicas con experiencias 
Al final de la membresía de prueba, usted debera mejorar a membresía pagada, de lo contrario perderia acceso 
a las herramientas, y su perfil aparecerá en las últimas páginas. 
Para mejorar su membresía usted puede responder a este email o Iniciar sesión en nuestro 
sitio web y hacer click en  \"Mejore su membresía Ahora\". 
El equipo de ".NGE_SITE_TITLE." 
</pre> 
"; 
$LNG['esc_mb_chng_to_vip_email'] = "<pre>Estimado %LOGIN%, 
la membresía de su perfil de escort con nombre %SHOWNAME% ha sido cambiada a  \"VIP\". 
Este perfil no expira, y tiene todos los beneficios de una membresía normal pagada. 
El equipo de ".NGE_SITE_TITLE." 
</pre> 
"; 
$LNG['esc_mb_memb_expired_email'] = "<pre>Estimado %LOGIN%, 
la membresía de su perfil de escort con nombre %SHOWNAME% ha expirado. 
Usted ha perdido los beneficios y su cuenta ha sido desactivada. 
Si desea renovar su membresía, por favor contáctenos a traves de  ".NGE_CONTACT_EMAIL.". 
El equipo de ".NGE_SITE_TITLE." 
</pre> 
"; 
$LNG['esc_mb_memb_expired_email_subj'] = "Membresia expirada"; 
$LNG['esc_mb_memb_expires_soon_email'] = "<pre>Estimado %LOGIN%, 
la membresía de su perfil de escort con nombre %SHOWNAME% expira en %DAYS% días, 
por favor no olvide renovarla. Para hacerlo, por favor contáctenos a traves de ".NGE_CONTACT_EMAIL.". 
El equipo de ".NGE_SITE_TITLE." 
</pre> 
"; 
$LNG['esc_mb_memb_expires_soon_email_subj'] = "Membresia expira pronto"; 
$LNG['esc_to_upg'] = "Mejorar perfil de escort"; 
$LNG['esc_upg_promo'] = " 
<ul> 
<li>participación en la rotación de chicas en la página principal, 
lo cual significa que usted nunca se quedará en la última página, donde 
los usuarios no la verian. Todos los perfiles pagados son rotados cada hora, 
por lo que todos los perfiles tienen la misma oportunidad de aparecer en la primera página de una 
ciudad en particular o en la página principal de un sitio web</li> 
<li>usted podrá participar en la competencia de la \"Chica del Mes\"</li> 
<li>usted tendrá la oportunidad de ser incluída en la lista TOP10 de chicas con experiencias</li> 
</ul>"; 
$LNG['esc_upg_promo_txt'] = "<a href='/private/upgrade_escort.php$QUERY_STRING'>Mejore su membresía AHORA</a>"; 
$LNG['esc_upgrade_payment_details'] = " 
Gracias por su interes en comprar una membresía en ".NGE_SITE_TITLE.".<br /> 
Por favor transfiera  %AMOUNT% EUR a nuestra cuenta y su perfil será actualizado inmediatamente después que el dinero este en nuestra cuenta.<br /> 
    <p><strong>Detalles bancarios:</strong><br /> 
    SMD MEDIA:<br /> 
    Conto corrente n. 6152293433/25<br /> 
    IBAN: IT42 P030 6910 9106 1522 9343 325<br /> 
    SWIFT: BCITIT333P0<br /> 
    Banca Intesa - filiale 2390<br /> 
    Via G. Rubini n. 6, 22100 Como</p> 
	Si tiene problemas o preguntas, por favor no dude en contactarnos a  <a href='mailto:".NGE_CONTACT_EMAIL."'>".NGE_CONTACT_EMAIL."</a>. 
"; 
$LNG['escort'] = "Escort"; 
$LNG['escort_active'] = "Su perfil de escort está activo. <a href='%LINK%'>Haga click aquí</a> para desactivarlo."; 
$LNG['escort_added_continue_review'] = "Escort agregada exitosamente. <a href='%LINK%'>Haga click aquí</a> para continuar y agregar su experiencia."; 
$LNG['escort_city_tour_not_allowed'] = "Usted no puede ir en Tour de Ciudad durante su periodo de prueba. Por favor obtenga un espacio Premium para activar esta herramienta. Haga click <a href='mailto:".NGE_CONTACT_EMAIL."'>aquí para ordenar</a>."; 
$LNG['escort_data_form_text'] = "Solamente complete la información que desee sea publicada en el sitio web."; 
$LNG['escort_inactive'] = "Esta escort no está activa en estos momentos !"; 
$LNG['escort_modification_disabled'] = "En estos momentos estámos llevando a cabo mantenimiento en el sitio web. No se aceptan modificaciones a escorts. Por favor, regrese más tarde. Le pedimos disculpas por esta inconveniencia."; 
$LNG['escort_name_active'] = "El perfil del escort %ESCORT% está activo. <a href='%LINK%'>Haga click aquí</a> para desactivarlo."; 
$LNG['escort_name_not_active'] = "El perfil del escort %ESCORT% no está activo. <a href='%LINK%'>Haga click aquí</a> para activarlo."; 
$LNG['escort_name_waiting_for_approval'] = "El perfil del escort %ESCORT% está en espera de la aprobación del administrador de este sitio web."; 
$LNG['escort_not_active'] = "Su perfil de escort no está activo. <a href='%LINK%'>Haga click aquí</a> para activarlo."; 
$LNG['escort_not_found_use_search'] = "Escort %NAME% no identificada, por favor use la búsqueda para especificar una chica."; 
$LNG['escort_not_in_db_add_her'] = "Si la escort no está en nuestra base de datos, por favor <a href='%LINK%'>agréguela ahora</a>."; 
$LNG['escort_profile_added'] = "Perfil de escort exitosamente agregado a la base de datos!<BR /> Por favor 
<a href='/private/edit_gallery.php$QUERY_STRING'>agregue fotos a su perfil.</a>"; 
$LNG['escort_profile_updated'] = "Perfil de escort actualizado exitosamente! <BR /> 
<a href='/private/index.php$QUERY_STRING'>Regrese.</a>"; 
$LNG['escort_waiting_for_approval'] = "Su perfil de escort está en espera de la aprobación del administrador de este sitio web."; 
$LNG['escorts'] = "Escorts"; 
$LNG['escorts_in_city'] = "Escorts en %CITY%"; 
$LNG['escorts_match'] = "Escorts que cumplen estos criterios"; 
$LNG['evening_phone'] = "Teléfono alterno"; 
$LNG['expires'] = "Expira"; 
$LNG['eye_color'] = "Color de los ojos"; 
$LNG['eyes'] = "Ojos"; 
$LNG['fake_free_review_confirm_mail_01'] = " 
    <p>Hola %SHOWNAME%,<br /> 
un miembro de ".NGE_SITE_TITLE." ha escrito una experiencia sobre ti en nuestro sitio web. Para evitar experiencias falsas te pedimos que verifiques si está persona ha sido tu cliente y que confirmes o niegues la información en este email! Por favor, simplemente respondenos con SI o NO, SI significa que este es tu cliente!</p> 
    <p>Los detalles del cliente:</p> 
"; 
$LNG['fake_free_review_confirm_mail_02'] = " 
    <p>Responde ahora con: SI por verdadero o NO por falso.</p> 
    <p>Su equipo de ".NGE_SITE_TITLE."</p> 
"; 
$LNG['fake_free_review_confirm_mail_subj'] = NGE_SITE_TITLE." confirmación de experiencia libre de fraude"; 
$LNG['fake_photo'] = "Foto falsa!"; 
$LNG['fax'] = "Fax"; 
$LNG['female'] = "Femenino"; 
$LNG['ff_guests'] = "<div class='big center'>solamente %DAYS% días hasta el sorteo!</div><a href='%LINK%' rel='nofollow'>Regístrese AHORA</a> como Miembro Premium - y obtenga su oportunidad de tirarse a su <a href='%LINK%' rel='nofollow'>chica favorita GRATIS!</a> *"; 
$LNG['ff_guests_today'] = "<div class='big center'>HOY</div> anunciaremos al afortunado ganador de la 'Lotería de Tirar Gratis' de este mes! Podria ser USTED? Obtenga su oportunidad y  <a href='%LINK%' rel='nofollow'>Regístrese AHORA</a> como Miembro Premium - para tomar parte en el sorteo! *"; 
$LNG['ff_nonpaid'] = "<div class='big center'>solamente %DAYS% días hasta el sorteo!</div><a href='%LINK%' rel='nofollow'>Mejore su membresía AHORA</a> - y obtenga su oportunidad de tirarse a su <a href='%LINK%' rel='nofollow'>chica favorita GRATIS!</a> *"; 
$LNG['ff_nonpaid_today'] = "<div class='big center'>HOY</div> anunciaremos al afortunado ganador de la 'Lotería de Tirar Gratis' de este mes! Podria ser USTED? Obtenga su oportunidad y  <a href='%LINK%' rel='nofollow'>Mejore su membresía AHORA</a> para tomar parte en el sorteo! *"; 
$LNG['ff_paid'] = " <div class='big center'>solamente %DAYS% días hasta el sorteo!</div>"; 
$LNG['ff_paid_today'] = " <div class='big center'>HOY</div> anunciaremos al afortunado ganador de la 'Lotería de Tirar Gratis' de este mes! Podria ser USTED?"; 
$LNG['file_uploaded'] = "Su foto ha sido subida exitosamente."; 
$LNG['finish_reg'] = "Completar registración"; 
$LNG['first_name'] = "Nombre"; 
$LNG['fluent'] = "Fluente"; 
$LNG['forgot_email_1'] = "Estimado %USER%, usted ha solicitado que su contraseńa le sea enviada mediante la forma 'Olvidé contraseńa' en %SITE%."; 
$LNG['forgot_email_2'] = "Su información de login es:"; 
$LNG['forgot_email_subject'] = NGE_SITE_TITLE." solicitar contraseńa"; 
$LNG['forgot_passwd'] = "Olvidó su contraseńa?"; 
$LNG['forgot_password'] = "Olvidé mi contraseńa"; 
$LNG['free'] = "Gratis"; 
$LNG['free_fuck_desc'] = "Al final de cada mes, elegimos un afortunado Miembro Premium, que ganará una Tirada Gratis con la chica de su elección.".NGE_SITE_TITLE.". Nosotros pagamos todos los costos del escort por usted !<BR /> <BR /> Felicidades a todos los afortunados ganadores! Esperamos que la pase super con su chica favorita... !"; 
$LNG['free_fuck_lotto'] = "Lotería de Tirar Gratis"; 
$LNG['free_fuck_note'] = "Nota: este valor es de hasta 300 euro o 1 hora de servicio, si usted elige una chica que cuesta 250 euro, no habrá pago de los 50 restantes; este premio no puede ser cambiado en efectivo ni otras alternativas. Detalles de este sorteo serán enviados al ganador."; 
$LNG['free_mb_desc'] = "Su perfil se muestra al pie de las últimas páginas, lo cual resulta en pocos números de usuarios 
viéndolo. Mejore su membresía ahora y obtenga:"; 
$LNG['free_mb_desc_ag'] = "%COUNT% escorts - <a href='/private/upgrade_escort.php$QUERY_STRING'>Mejore ahora</a> para informar a sus clientes sobre ellas!"; 
$LNG['friday'] = "Viernes"; 
$LNG['friendly'] = "Amistosa"; 
$LNG['from'] = "Desde"; 
$LNG['fuckometer'] = "Fuckometer"; 
$LNG['fuckometer_desc'] = "El Fuckometer es un rating que indica el desenvolvimiento sexual de la chica basado en las experiencias recibidas. Se usa para seńalar a las mejores chicas en acción."; 
$LNG['fuckometer_range'] = "Rango del Fuckometer"; 
$LNG['fuckometer_rating'] = "Calificación de su Fuckometer"; 
$LNG['fuckometer_short'] = "FM"; 
$LNG['fuckometer_toplist'] = "Los Fuckometer más altos"; 
$LNG['full_name'] = "Nombre completo"; 
$LNG['gays'] = "Gays"; 
$LNG['gender'] = "Sexo"; 
$LNG['genuine_photo'] = "Foto 100% genuina"; 
$LNG['get_trusted_review'] = "Obtener experiencia confiable"; 
$LNG['get_trusted_review_desc'] = " 
Obtenga una experiencia 100% confiable! Por favor llene todos los campos a continuación!<br /> 
Cómo funciona esto? <a href='%LINK%'>Haga click aquí</a>!"; 
$LNG['girl'] = "Chica"; 
$LNG['girl_booked'] = "Usted ha contratado a %NAME% en %DATE%."; 
$LNG['girl_no_longer_listed'] = "Esta chica ya no está en la lista de %SITE%"; 
$LNG['girl_of'] = "Chica de"; 
$LNG['girl_of_the_month_history'] = "Historia de las Chicas del Mes"; 
$LNG['girl_on_tour_1'] = "Esta chica está en tour en %CITY% - %COUNTRY%"; 
$LNG['girl_on_tour_2'] = "Desde %FROM_DATE% hasta %TO_DATE%"; 
$LNG['girl_on_tour_3'] = $LNG['tour_phone'].": %PHONE%"; 
$LNG['girl_on_tour_4'] = $LNG['tour_email'].": %EMAIL%"; 
$LNG['girl_origin'] = "Origen de la chica"; 
$LNG['girls_in_italy'] = "Todas las chicas en esta lista están en estos momentos en ".$LNG['country_'.MAIN_COUNTRY]."! Contrátelas AHORA!"; 
$LNG['girls_international'] = "Chicas disponibles internacionalmente, por favor note que si desea tenerlas en ".$LNG['country_'.MAIN_COUNTRY]." necesita contratarlas por al menos 12 horas en la mayoría de los casos!"; 
$LNG['go_to_escort_profile'] = "Ir al perfil de escort"; 
$LNG['go_to_reviews_overview'] = "Ir a página de experiencias"; 
$LNG['gray'] = "Gris"; 
$LNG['green'] = "Verde"; 
$LNG['hair'] = "Cabello"; 
$LNG['hair_color'] = "Color del cabello"; 
$LNG['hard_to_believe_its_her'] = "Me cuesta creer que sea ella"; 
$LNG['hard_to_book'] = "Difícil de hacer una cita"; 
$LNG['height'] = "Altura"; 
$LNG['help_system'] = "Ayuda"; 
$LNG['help_watch_list'] = "Usted puede agregar cualquier usuario, chica o agencia a su lista de favoritos. 
Esta lista se usa para notificarle por email, en caso que algun evento especial tenga lugar. 
Si una <b>escort</b> está en su lista, usted va a recibir estas alertas por email: 
<ul><li>Escort recibió una nueva experiencia</li><li>Escort se ha ido de tour</li><li>Escort se fue / retornó de vacaciones</li></ul> 
Si usted agrega una <b>agencia</b> a su lista, usted va a recibir estas alertas por email: 
<ul><li>Agencia agregó una nueva escort</li></ul> 
Si usted agrega un <b>miembro regular</b> a su lista, usted va a recibir estas alertas por email: 
<ul><li>Miembro submitió una nueva experiencia</li></ul> 
Usted puede tener tantos artículos en su lista como desee. 
Para manejar esta lista facilmente, haga click en \"Lista de favoritos\" en el menú después de haber iniciado la sesión. 
"; 
$LNG['hip'] = "Caderas"; 
$LNG['home'] = "Casa"; 
$LNG['home_page'] = "Página principal"; 
$LNG['hour'] = "hora"; 
$LNG['hours'] = "horas"; 
$LNG['howto_set_main'] = "Para seleccionar una foto como principal, haga click en ella."; 
$LNG['i_agree_with_terms'] = "He leido y estoy de acuerdo con los <a href='%LINK%'>terminos y condiciones</a> generales"; 
$LNG['in'] = "En"; 
$LNG['in_face'] = "En la cara"; 
$LNG['in_mouth_spit'] = "Escupirle en la boca"; 
$LNG['in_mouth_swallow'] = "Traga semen en la boca"; 
$LNG['incall'] = "Mi lugar"; 
$LNG['independent'] = "Independiente"; 
$LNG['independent_escorts_from'] = "Escorts independientes de"; 
$LNG['independent_girls'] = "Acompañantes"; 
$LNG['info'] = "Información"; 
$LNG['intelligent'] = "Inteligente"; 
$LNG['international'] = "Internacional"; 
$LNG['international_directory'] = "Directorio internacional"; 
$LNG['invalid_birth_date'] = "Fecha de nacimiento inválida"; 
$LNG['invalid_curr_password'] = "Contraseńa actual incorrecta"; 
$LNG['invalid_desired_date'] = "Fecha deseada es inválida"; 
$LNG['invalid_discount_code'] = "Código de descuento inválido."; 
$LNG['invalid_email'] = "E-mail inválido"; 
$LNG['invalid_email_info'] = "Nuestro systema ha identificado su dirección de email como invalida. No eserá posible enviarle a Usted un e-mail. Su cuenta ha sido temporalmente bloqueada. Por favor proveanos una direccion de e-mail valida, nosotros le enviaremos un mail de verificación con un link de confirmación. Despues de que usted reciba el mail y de click en el link su cuenta volvera a funcionar otra vez.<br /><br /> 
Gracias po su comprehensión."; 
$LNG['invalid_file_type'] = "Tipo de archivo inválido. Solamente se permiten archivos Jpeg."; 
$LNG['invalid_meeting_date'] = "Fecha de encuentro inválida"; 
$LNG['invalid_showname'] = "Nombre ficticio inválido, por favor use solamente caracteres alfanuméricos."; 
$LNG['invalid_user_type'] = "Tipo de usuario inválido"; 
$LNG['invitation'] = "Bienvenido a Servicio de Escorts. Por favor elija la ciudad donde está buscando un escort."; 
$LNG['invitation_1'] = "En las siguientes páginas usted encontrara las mejores chicas escorts, clubs privados y agencias de escorts en su area - ".$LNG['country_'.MAIN_COUNTRY]." e Internacional. Usted podrá ver información de contacto y fotos de cada chica."; 
$LNG['invitation_2'] = "Le deseamos una feliz estancia en nuestro sitio web. Si tiene preguntas o dudas, no dude  en contactarnos: <a href='mailto:".NGE_CONTACT_EMAIL."'>".NGE_CONTACT_EMAIL."</a>"; 
$LNG['kiss'] = "Beso"; 
$LNG['kiss_with_tongue'] = "Beso con lengua"; 
$LNG['kissing'] = "Besos"; 
$LNG['kissing_with_tongue'] = "Besos con lengua"; 
$LNG['language'] = "Idioma"; 
$LNG['language_problems'] = "Problemas con el idioma"; 
$LNG['languages'] = "Idiomas"; 
$LNG['languages_changed'] = "Idiomas exitosamente actualizados"; 
$LNG['last'] = "Ultima"; 
$LNG['last_name'] = "Apellido"; 
$LNG['latest_10_reviews'] = "Las ultimas 10 experiencias de las chicas"; 
$LNG['latest_10_third_party_reviews'] = "Las 10 experiencias más últimas de las terceras personas"; 
$LNG['level'] = "Nivel"; 
$LNG['limitrofi'] = "Limitrofi"; 
$LNG['links'] = "Enlaces"; 
$LNG['loano'] = "Loano"; 
$LNG['locations_changed'] = "Localidades exitosamente actualizadas"; 
$LNG['logged_in'] = "Usted está logged-in como"; 
$LNG['login'] = "Login"; 
$LNG['enter'] = "Login"; 
$LNG['login_and_password_alphanum'] = "Login y contraseńa pueden contener solamente caracteres alfanuméricos (0-9, a-z, A-Z, _)."; 
$LNG['login_and_signup_disabled'] = "En estos momentos estamos llevando a cabo mantenimiento en el sitio web, y no se aceptan nuevas registraciones o logins. Por favor, regrese más tarde. Le pedimos disculpas por esta inconveniencia."; 
$LNG['login_exists'] = "Ese usuario ya existe. Por favor elija otro nombre de usuario..."; 
$LNG['login_problems_contact'] = "Si aún tiene problemas, no dude en contactarnos: <a href='mailto:".NGE_CONTACT_EMAIL."'>".NGE_CONTACT_EMAIL."</a>"; 
$LNG['logout'] = "Logout"; 
$LNG['looking_for_girls_in_your_city'] = "Buscando chicas en vuestra ciudad?"; 
$LNG['looking_for_girls_in_your_region'] = "Looking for girls in your region?"; 
$LNG['looks'] = "Apariencia física"; 
$LNG['looks_and_services'] = "Apariencia y servicios"; 
$LNG['looks_range'] = "Calificación de apariencia"; 
$LNG['looks_rating'] = "Calificación de su apariencia"; 
$LNG['lower_date'] = "La fecha no puede ser menor que Desde la fecha en el rango de fecha"; 
$LNG['lower_fuckometer'] = "El rango de Fuckomoter no es correcto"; 
$LNG['lower_looks_rating'] = "Calificación de apariencia no es correcto"; 
$LNG['lower_services_rating'] = "Calificación de servicios no es correcto"; 
$LNG['male'] = "Masculino"; 
$LNG['masturbation'] = "Masturbación"; 
$LNG['meeting_costs'] = "Costo del encuentro"; 
$LNG['meeting_date'] = "Fecha del encuentro"; 
$LNG['meeting_date_range'] = "Rango de la fecha de encuentro"; 
$LNG['meeting_dress_comments'] = "lugar de encuentro, tipo de vestuario, comentarios"; 
$LNG['meeting_length'] = "Duración del encuentro"; 
$LNG['member'] = "Miembro"; 
$LNG['member_name'] = "Nombre del miembro"; 
$LNG['member_profit_01'] = "ser un miembro con total acceso al forum y podrá agregar sus comentarios y experiencias"; 
$LNG['member_profit_02'] = "escribir su opinión sobre las chicas"; 
$LNG['member_profit_03'] = "informarse sobres las nuevas chicas en %SITE%"; 
$LNG['member_profits'] = "Al ser miembro, se puede beneficiar de"; 
$LNG['membership_change_subject'] = "Cambio de membresía en ".NGE_SITE_TITLE; 
$LNG['membership_change_to_free_mail'] = " 
    <p>Estimado %LOGIN%,<br /> 
   Su subscripción como *PremiumMember* de ".NGE_SITE_TITLE." ha expirado y su membresía ha sido cambiada a Gratis.</p> 
    <p>Si le gustaria extender su membresía, por favor hágalo en nuestro sitio web o envíenos un email.</p> 
    <p>Mejore su membresía AHORA y obtenga:</p> 
    <ul> 
        <li>Precios descontados por reservacíones con todas las chicas que aparecen <a href='%LINK%'>aquí</a></li> 
        <li>Usted automáticamente entrará en nuestra &quot;Lotería de Tirar Gratis&quot; mensualmente, donde puede ganar una tirada gratis con su chica favorita</li> 
        <li>Acceso al foro premium</li> 
        <li>Mensajes privados</li> 
        <li>Lista de favoritos para chicas, usuarios y agencias</li> 
        <li>Acceso a nuestra búsqueda avanzada</li> 
        <li>Libre de anuncios</li> 
    </ul> 
    <p>Si tiene preguntas, o si encuentra algun problema, por favor contáctenos por email. Le responderemos rápidamente.</p> 
    <p>Sinceramente<br /> 
    ".NGE_SITE_TITLE."- Team</p> 
"; 
$LNG['membership_change_to_premium_mail'] = " 
    <p>Estimado %LOGIN%,<br /> 
    Su subscripción como *PremiumMember* de ".NGE_SITE_TITLE." fue exitosa. Gracias por su confianza.</p> 
    <p>Su información de login es:<br /> 
    Login: %LOGIN%</p> 
    <p>(Por favor guarde está información en un lugar seguro)</p> 
    <p>Una vez más, estos son los beneficios de un *Premium Member* :</p> 
    <ul> 
        <li>Precios descontados por reservacíones con todas las chicas que aparecen <a href='%LINK%'>aquí</a></li> 
        <li>Usted automáticamente entrará en nuestra &quot;Lotería de Tirar Gratis&quot; mensualmente, donde puede ganar una tirada gratis con su chica favorita</li> 
        <li>Acceso al foro premium</li> 
        <li>Mensajes privados</li> 
        <li>Lista de favoritos para chicas, usuarios y agencias</li> 
        <li>Acceso a nuestra búsqueda avanzada</li> 
        <li>Libre de anuncios</li> 
    </ul> 
    <p>Si tiene preguntas, o si encuentra algun problema, por favor contáctenos por email. Le responderemos rápidamente.</p> 
    <p>Sinceramente<br /> 
   El equipo de ".NGE_SITE_TITLE."</p> 
"; 
$LNG['membership_type'] = "Tipo de membresía"; 
$LNG['membership_types'] = "Tipos de membresía"; 
$LNG['men'] = "Hombres"; 
$LNG['menu'] = "Menú"; 
$LNG['message'] = "Mensaje"; 
$LNG['message_for'] = "Mensaje para %NAME%"; 
$LNG['message_not_sent'] = "Su mensaje no pudo ser enviado."; 
$LNG['message_sent'] = "Su mensaje fué enviado a %NAME%."; 
$LNG['metric'] = "Sistema métrico"; 
$LNG['metric_desc'] = "centímetros, kilogramos, ..."; 
$LNG['middle_name'] = "Segundo nombre"; 
$LNG['minutes'] = "minutos"; 
$LNG['modified'] = "Modificado"; 
$LNG['modify_escort'] = "Modificar escort"; 
$LNG['monday'] = "Lunes"; 
$LNG['month'] = "Mes"; 
$LNG['month_1'] = "Enero"; 
$LNG['month_10'] = "Octubre"; 
$LNG['month_11'] = "Noviembre"; 
$LNG['month_12'] = "Diciembre"; 
$LNG['month_2'] = "Febrero"; 
$LNG['month_3'] = "Marzo"; 
$LNG['month_4'] = "Abril"; 
$LNG['month_5'] = "Mayo"; 
$LNG['month_6'] = "Junio"; 
$LNG['month_7'] = "Julio"; 
$LNG['month_8'] = "Agosto"; 
$LNG['month_9'] = "Septiembre"; 
$LNG['months'] = "Meses"; 
$LNG['more_info'] = "Más información..."; 
$LNG['more_new_girls'] = "Más chicas nuevas"; 
$LNG['more_news'] = "Más noticias..."; 
$LNG['multiple_times_sex'] = "Sexo varias veces"; 
$LNG['my_escort_profile'] = "Mi perfil de escort"; 
$LNG['my_escorts'] = "Mis escorts"; 
$LNG['my_languages'] = "Mis idiomas"; 
$LNG['my_locations'] = "Mis localidades"; 
$LNG['my_new_review_notify_agency'] = "Infórmese por email si una de sus escorts recibe una nueva experiencia."; 
$LNG['my_new_review_notify_single'] = "Infórmese por email si usted recibe una nueva experiencia."; 
$LNG['my_photos'] = "Mis fotos"; 
$LNG['my_rates'] = "Mis precios"; 
$LNG['my_real_profile'] = "Mis datos reales"; 
$LNG['my_reviews'] = "Mis experiencias"; 
$LNG['name'] = "Nombre"; 
$LNG['name_of_the_lady'] = "Nombre de la chica"; 
$LNG['name_of_the_site'] = "Nombre del sitio web"; 
$LNG['ethnic'] = "Ethnic"; 
$LNG['nationality'] = "Nacionalidad"; 
$LNG['need_help'] = "Necesita ayuda?"; 
$LNG['need_to_be_registered_to_review'] = "Usted debe estar registrado en %SITE% para compartir sus experiencias con una chica, <a href='%LINK%'>regístrese ahora GRATIS!</a>"; 
$LNG['never'] = "nunca"; 
$LNG['new'] = "Nueva"; 
$LNG['new_arrivals'] = "Las más nuevas"; 
$LNG['new_email'] = "Nuevo e-mail"; 
$LNG['new_entries'] = "Nuevas entradas"; 
$LNG['new_escort_email_by_agency_body'] = "<pre>La agencia \"%AGENCY%\" agrego una nueva escort: %ESCORT% ! 
Usted ha recibido esta notificación porque esta agencia está en su lista de favoritos. 
Haga click en este vínculo 
<a href=\"%LINK%\">%LINK%</a> 
para leer la experiencia. 
El equipo de ".NGE_SITE_TITLE." 
</pre>"; 
$LNG['new_escort_email_by_agency_subject'] = "%AGENCY% agrego una nueva escort !"; 
$LNG['new_password'] = "Nueva contraseńa"; 
$LNG['new_password_empty_err'] = "La nueva contraseńa no puede estár vacía"; 
$LNG['new_review_email_agency_body'] = "<pre>Usuario %USER% ha escrito una nueva experiencia sobre vuestra escort %ESCORT% ! 
Haga click en este vínculo 
<a href=\"%LINK%\">%LINK%</a> 
para leer la experiencia. 
El equipo de ".NGE_SITE_TITLE." 
</pre>"; 
$LNG['new_review_email_agency_subject'] = "Vuestra escort tiene una nueva experiencia!"; 
$LNG['new_review_email_by_escort_body'] = "<pre>Escort %ESCORT% una nueva experiencia del usuario %USER% ! 
Usted ha recibido esta notificación porque esta escort está en su lista de favoritos. 
Haga click en este vínculo 
<a href=\"%LINK%\">%LINK%</a> 
para leer la experiencia. 
El equipo de ".NGE_SITE_TITLE." 
</pre>"; 
$LNG['new_review_email_by_escort_subject'] = "%ESCORT% ha recibido una nueva experiencia !"; 
$LNG['new_review_email_by_user_body'] = "<pre>Usuario %USER% ha escrito una nueva experiencia sobre la escort %ESCORT% ! 
Usted ha recibido esta notificación porque este usuario está en su lista de favoritos. 
Haga click en este vínculo 
<a href=\"%LINK%\">%LINK%</a> 
para leer la experiencia. 
El equipo de  ".NGE_SITE_TITLE." 
</pre>"; 
$LNG['new_review_email_by_user_subject'] = "%USER% ha escrito una nueva experiencia!"; 
$LNG['new_review_email_escort_body'] = "<pre>Usuario %USER% ha escrito una nueva experiencia sobre usted! 
Haga click en este vínculo 
<a href=\"%LINK%\">%LINK%</a> 
para leer la experiencia. 
El equipo de ".NGE_SITE_TITLE." 
</pre>"; 
$LNG['new_review_email_escort_subject'] = "Usted tiene una nueva experiencia!"; 
$LNG['news'] = "Noticias"; 
$LNG['newsletter'] = "Newsletter"; 
$LNG['next'] = "Próximo"; 
$LNG['next_step'] = "Próximo paso"; 
$LNG['no'] = "No"; 
$LNG['no_blowjob'] = "No sexo oral"; 
$LNG['no_cumshot'] = "No semen en el cuerpo o cara"; 
$LNG['no_esc_sel_err'] = "Usted ha seleccionado un escort"; 
$LNG['no_escort_languages'] = "Usted aún no ha elegido sus idiomas. <a href='%LINK%'>Haga click aquí</a> para elegir uno."; 
$LNG['no_escort_main_pic'] = "Usted aún no ha elegido una foto principal. <a href='%LINK%'>Haga click aquí</a> para elegir una."; 
$LNG['no_escort_name_languages'] = "Escort %ESCORT% no tiene un idioma principal aún. <a href='%LINK%'>Haga click aquí</a> para elegir uno."; 
$LNG['no_escort_name_main_pic'] = "Escort %ESCORT% no tiene una foto principal aún . <a href='%LINK%'>Haga click aquí</a> para elegir una."; 
$LNG['no_escort_name_photos'] = "Escort %ESCORT% no tiene fotos válidas en su perfil. <a href='%LINK%'>Haga click aquí</a> para actualizar la galería."; 
$LNG['no_escort_photos'] = "Usted no tiene fotos válidas en su perfil. <a href='%LINK%'>Haga click aquí</a> para actualizar su galería."; 
$LNG['no_escort_profile'] = "Usted no tiene un perfil de escort activo. <a href='%LINK%'>Haga click aquí</a> para agregar uno."; 
$LNG['no_escorts_found'] = "No se encontró ninguna escort"; 
$LNG['no_escorts_profile'] = "Usted no tiene ningún perfil de escort definido. Por favor haga <a href='%LINK%'>click aquí</a> para agregar uno."; 
$LNG['no_kiss'] = "No beso"; 
$LNG['no_kissing'] = "No besos"; 
$LNG['no_only_once'] = "No, solo una vez"; 
$LNG['no_premium_escorts'] = "No escorts premium."; 
$LNG['no_reviews_found'] = "No se encontraron experiencias"; 
$LNG['no_sex_avail_chosen'] = "Usted debe seleccionar al menos 1 actividad sexual"; 
$LNG['no_users_found'] = "No se encontró usuarios"; 
$LNG['no_votes'] = "Sin votos"; 
$LNG['norm_memb'] = "1 mes de membresía normal por 50 EUR"; 
$LNG['normal'] = "Normal"; 
$LNG['normal_mb_desc_ag'] = "%COUNT% escorts (la 1ra expira en %DAYS% días)"; 
$LNG['normal_review'] = "normal"; 
$LNG['normal_review_desc'] = "Experiencia normal significa que la agencia o la chica no respondieron al sms/e-mail, asi que la experiencia no ha sido confirmada, pero tampoco denegada! Eso significa que la experiencia puede ser catalogada como confiable!"; 
$LNG['not_approved'] = "En espera de aprobación"; 
$LNG['not_available'] = "No disponible"; 
$LNG['not_logged'] = "No está logged-in"; 
$LNG['not_logged_desc'] = "Usted debe iniciar sesión antes de usar esta herramienta."; 
$LNG['not_verified_msg'] = " 
<p>Su cuenta no está aún verificada.<br /> 
Para ser un miembro completo de  ".NGE_SITE_TITLE." usted necesita verificar su dirección de e-mail. Si aún no ha recibido el email, por favor <a href=\"%LINK%\">haga click aquí para re-enviar el email de activación</a>. 
</p><p> 
Nota: por favor revise también su carpeta de email chatarra, desgraciadamente en algunos casos, nuestros emails están iendo a esta carpeta. Gracias por su comprensión. 
</p>"; 
$LNG['not_yet_member'] = "Aun no es un MIEMBRO? Regístrese ahora gratis!"; 
$LNG['not_yet_premium_signup_now'] = "No es aún un Miembro Premium? <a href='%LINK%'>Regístrese AHORA</a> y ahorre un montón de dinero!"; 
$LNG['note'] = "Nota"; 
$LNG['notify_comon'] = "<pre>Usted está recibiendo este email porque está registrado en ".NGE_SITE_TITLE." y ha elegido recibir estas alertas. 
Para cambiar estás preferencias, por favor Inicie sesión en su cuenta y haga click en \"Preferencias\" en el menú.</pre>"; 
$LNG['now'] = "Ahora"; 
$LNG['number'] = "Nro."; 
$LNG['number_of_reviews'] = "Número de experiencias"; 
$LNG['offers_discounts'] = "Ofrece descuentos"; 
$LNG['on_tour_in'] = "en tour en"; 
$LNG['only'] = "solo"; 
$LNG['only_fake_free_reviews'] = "Solo experiencias 100% libre de fraude"; 
$LNG['only_for_premium_members'] = "solo para miembros Premium"; 
$LNG['only_girls_with_reviews'] = "Solo chicas con experiencias"; 
$LNG['only_reviews_from_user'] = "Solo experiencias del usuario"; 
$LNG['only_reviews_not_older_then'] = "Solo experiencias más nuevas que"; 
$LNG['optional'] = "opcional"; 
$LNG['oral_without_condom'] = "Sexo oral sin condón"; 
$LNG['order_premium_spot_now'] = "Ordene un espacio premium ahora!"; 
$LNG['origin_escortforum'] = "foro de escorts"; 
$LNG['origin_non_escortforum'] = "foro de no escorts"; 
$LNG['other'] = "Otro"; 
$LNG['other_boys_from'] = "Otros escorts de"; 
$LNG['other_escorts_from'] = "Otras escorts de"; 
$LNG['boys_from'] = "Relax"; 
$LNG['escorts_from'] = "Relax"; 
$LNG['outcall'] = "Tu lugar"; 
$LNG['overall_rating'] = "Calificación general"; 
$LNG['partners'] = "Partners"; 
$LNG['passive'] = "Pasiva"; 
$LNG['passwd_has_been_sent'] = "Su contraseńa a sido enviada a %EMAIL%"; 
$LNG['passwd_succesfuly_sent'] = "Su contraseńa ha sido exitosamente enviada a su dirección de email."; 
$LNG['password'] = "Contraseńa"; 
$LNG['password_changed'] = "Contasena cambiada exitosamente"; 
$LNG['password_invalid'] = "La contraseńa es inválida. Debe tener al menos 6 caracteres."; 
$LNG['password_missmatch'] = "Las contraseńas no concuerdan"; 
$LNG['percent_discount'] = "%DISCOUNT%% descuento"; 
$LNG['phone'] = "Teléfono"; 
$LNG['phone_instructions'] = "Instrucciones por teléfono"; 
$LNG['photo_correct'] = "Foto correcta"; 
$LNG['photos'] = "Fotos"; 
$LNG['pic_delete_failed'] = "Eliminacion de foto ha fallado"; 
$LNG['pic_deleted'] = "Foto eliminada"; 
$LNG['place'] = "Lugar"; 
$LNG['please_choose_services'] = "Por favor elija los servicios recibidos de la chica; esto influenciará su fuckometer! Gracias por su comprensión!"; 
$LNG['please_fill_form'] = "Por favor complete este questionario"; 
$LNG['please_provide_email'] = "Por favor indique el email que usó cuando se registró en %SITE%"; 
$LNG['please_select'] = "Por favor elija..."; 
$LNG['plus2'] = "2+"; 
$LNG['poshy'] = "Poshy"; 
$LNG['posted_reviews'] = "Hasta el momento usted ha escrito %NUMBER% experiencias."; 
$LNG['posted_to_forum'] = "Hasta el momento usted ha escrito %NUMBER% temas en este foro."; 
$LNG['premium'] = "Premium"; 
$LNG['premium_escorts'] = "Escorts Premium"; 
$LNG['premium_mb_desc_ag'] = "%COUNT% escorts (la 1ra expira en %DAYS% días)"; 
$LNG['premium_user'] = "Usuario Premium"; 
$LNG['previous'] = "Anterior"; 
$LNG['price'] = "Precio"; 
$LNG['priv_apart'] = "Apartamento privado"; 
$LNG['private_menu'] = "Menú Privado"; 
$LNG['problem_report'] = "Reporte un problema"; 
$LNG['problem_report_back'] = "Haga click aquí para regresar al perfil de esta escort"; 
$LNG['problem_report_no_accept'] = "Gracias, su reporte de este problema ha sido aceptado"; 
$LNG['problem_report_no_name'] = "Por favor provéanos con su nombre"; 
$LNG['problem_report_no_report'] = "Por favor provéanos con su reporte"; 
$LNG['prof_city_expl'] = "Ciudad de origen de la chica, no necesariamente su localidad de trabajo. Las localidades de trabajo pueden ser definidas más adelante."; 
$LNG['prof_country_expl'] = "País de origen de la chica, no necesariamente su localidad de trabajo. Las localidades de trabajo pueden ser definidas más adelante."; 
$LNG['profile'] = "Perfil"; 
$LNG['profile_changed'] = "Perfil actualizado exitosamente"; 
$LNG['promote_this_girl'] = "Promueva esta chica"; 
$LNG['promote_yourself'] = "Promuévase usted misma"; 
$LNG['provided_services'] = "Servicios provistos"; 
$LNG['publish_date'] = "Fecha de publicación"; 
$LNG['rank'] = "Nivel"; 
$LNG['rate'] = "Precio"; 
$LNG['rate_looks'] = "Calificación de su apariencia"; 
$LNG['rate_services'] = "Calificación de sus servicios"; 
$LNG['rates'] = "Precios"; 
$LNG['rates_changed'] = "Precios exitosamente actualizados"; 
$LNG['rates_deleted'] = "Precios eliminados exitosamente"; 
$LNG['rating'] = "Rating"; 
$LNG['re_verify_sent'] = "Le hemos enviado su email de activación una vez más. Por favor revise su cuenta de emails y siga las instrucciones incluídas en ella."; 
$LNG['read_this_review'] = "Lea esta experiencia"; 
$LNG['read_whole_article'] = "Lea el artículo completo"; 
$LNG['real'] = "Reales"; 
$LNG['real_data_form_text'] = "Por favor provéanos con su información verdadera. Esta información no será parte de su perfil, ni será mostrada en ningún lugar de nuestro sitio web, y será completamente confidencial, y para uso interno solamente."; 
$LNG['real_name'] = "Nombre real"; 
$LNG['recalibrate_now'] = "Recalibrar AHORA"; 
$LNG['receive_newsletter'] = "Recibir ".NGE_SITE_TITLE." newsletter"; 
$LNG['red'] = "Rojo"; 
$LNG['reference'] = "Cómo supo de nosotros?"; 
$LNG['regards'] = "Gracias, el equipo de %SITE%."; 
$LNG['region'] = "Región"; 
$LNG['register'] = "Regístrese"; 
$LNG['registration'] = "Registración"; 
$LNG['rem_watch'] = "Eliminar de lista de favoritos"; 
$LNG['rem_watch_success'] = "<b>%NAME%</b> fue exitosamente eliminado de su lista de favoritos. <a href='javascript:history.back()'>Haga click aquí para regresar.</a>"; 
$LNG['remove'] = "Elimine"; 
$LNG['renew_now'] = "Renueve ahora!"; 
$LNG['required_fields'] = "Los campos requeridos están marcados con *"; 
$LNG['review'] = "Experiencia"; 
$LNG['review_changed'] = "Experiencia exitosamente modificada"; 
$LNG['review_guide_and_rules'] = " 
    <strong>Reglas y regulaciones:</strong><br /> 
Le pedimos que por favor respete estas reglas, y que no escriba experiencias falsas! Cada experiencia le ayuda a usted y a nuestros otros miembros a obtener mejores servicios! 
    <ul> 
        <li>por favor especifique los servicios que esta chica ofrece</li> 
        <li>agregar nuevas chicas a la base de datos, si no encuentra una chica en específico, por favor agréguela a nuestra base de datos</li> 
        <li>por favor describa su experiencia con ella; experiencias cortas como: es magnífica, serán canceladas</li> 
        <li>por favor no le de a las chicas un rating de mas de 8.0 si no ofrecen al menos: 
        <ul> 
            <li>sexo oral sin condón</li> 
            <li>actitud entusiasta o neutral hacia el sexo</li> 
            <li>besos con lengua</li> 
        </ul> 
    </ul>"; 
$LNG['review_language'] = "Revisar idioma"; 
$LNG['review_sms'] = "Usted ha recibido una experiencia en ".NGE_SITE_TITLE."! Simplemente respóndanos por sms con: Sí, en caso de ser verdadera, o No en caso de ser falsa! En un 2do sms usted obtendrá los detalles de su cliente!"; 
$LNG['review_status'] = "Estado de la experiencia"; 
$LNG['review_this_girl'] = "Escriba su experiencia sobre esta chica"; 
$LNG['reviews'] = "Experiencias"; 
$LNG['reviews_to_recalibrate'] = " 
    Estimado %NAME%,<br /> 
    Usted tiene %REVIEWS% experiencias que recalibrar. Por favor, ayudenos a recalibrar todas sus experiencias dándonos información más detallada sobre la chica que usted conoció."; 
$LNG['rouen'] = "Rouen"; 
$LNG['royal'] = "Sistema real"; 
$LNG['royal_desc'] = "pulgadas, pies, ..."; 
$LNG['rss_channel_desc'] = "Escorts más recientes"; 
$LNG['salon'] = "Club nocturno"; 
$LNG['sardegna'] = "Sardegna"; 
$LNG['saturday'] = "Sábado"; 
$LNG['sauna'] = "Sauna"; 
$LNG['save_changes'] = "Guardar cambios"; 
$LNG['score'] = "Calificación"; 
$LNG['search'] = "Buscar"; 
$LNG['search_desc'] = "Bienvenidos a la herramienta de búsqueda rapida de ".NGE_SITE_TITLE.". Para más opciones de búsqueda, haga click en <a href='%LINK%'>búsqueda avanzada</a>."; 
$LNG['search_desc2'] = "Bienvenidos a la herramienta de búsqueda avanzada de ".NGE_SITE_TITLE.". Para búsqueda más sencilla, haga click en <a href='%LINK%'>búsqueda rapida</a>."; 
$LNG['search_escorts'] = "Buscar escorts"; 
$LNG['search_name'] = "Chica-/Agencia nombre"; 
$LNG['search_reviews'] = "Buscar Experiencias"; 
$LNG['search_reviews_back'] = "Regresar a la búsqueda"; 
$LNG['search_reviews_no_params'] = "Por favor provea al menos un parámetro"; 
$LNG['search_reviews_no_result'] = "Lo sentimos, no se halló ningún resultado, por favor intente de nuevo"; 
$LNG['search_users'] = "Buscar usuarios"; 
$LNG['see_escorts_soon_on_tour'] = "Haga click aquí para ver todas las chicas que llegarán a ".$LNG['country_'.MAIN_COUNTRY]." pronto"; 
$LNG['see_independent_preview'] = "vea presentación de todas las independientes"; 
$LNG['select_as_main'] = "Desea seleccionar una foto como la principal?"; 
$LNG['select_your_service_requirements'] = "Seleccione sus requerimientos de servicio"; 
$LNG['send_message'] = "Enviar mensaje"; 
$LNG['service_comments'] = "Comentarios sobre sus servicios"; 
$LNG['services'] = "Servicios"; 
$LNG['services_range'] = "Calificación de servicios"; 
$LNG['services_rating'] = "Calificación de sus servicios"; 
$LNG['services_she_was_willing'] = "Servicios que estuvo dispuesta a hacer"; 
$LNG['settings'] = "Preferencias"; 
$LNG['settings_update_error'] = "Error actualizando preferencias"; 
$LNG['settings_updated'] = "Preferencias actualizadas exitosamente."; 
$LNG['sex'] = "Sexo"; 
$LNG['sex_available_to'] = "Sexualmente disponible para"; 
$LNG['shoe_size'] = "Talla de calzado"; 
$LNG['show_all_reviews'] = "Mostrar todas las experiencias"; 
$LNG['show_email'] = "Mostrar dirección de email en el sitio web?"; 
$LNG['show_name'] = "Nombre ficticio"; 
$LNG['show_only_girls_on_tour_in'] = "Muéstrame chicas en tour en"; 
$LNG['show_site_reviews'] = "Mostrar las experiencias de %SITE%"; 
$LNG['show_third_party_reviews'] = "Mostrar experiencias de terceras personas"; 
$LNG['showname_exists'] = "Ese nombre ficticio ya existe, por favor elija otro"; 
$LNG['sign_up'] = "Regístrese"; 
$LNG['sign_up_for_free'] = "Regístrese GRATIS ahora!"; 
$LNG['signup_activated_01'] = "Su membresía ha sido exitosamente activada!"; 
$LNG['signup_activated_02'] = " 
        Felicidades!<br /> 
        Ahora usted es un miembro completo de %SITE%.<br /> 
        Sus detalles de su cuenta de usuario han sido enviados a su email."; 
$LNG['signup_activated_03'] = "Haga click aquí para Iniciar sesión."; 
$LNG['signup_and_win'] = "Regístrese hoy como Miembro Premium y <strong>GANE UNA TIRADA GRATIS CON VALOR DE 300 EURO!</strong>"; 
$LNG['signup_confirmation_email'] = "<pre> 
Estimado %USER%, 
gracias por registrarse con %SITE%. 
Ahora usted es un miembro completo de %SITE%. 
A continuación encontrará su información de su cuenta de usuario para que pueda Iniciar sesión. Por favor guárdela en un lugar seguro para futuras referencias: 
Login: %USER% 
Sitio web: %SITE% 
Disfrute nuestro sitio web! 
Esperamos con entusiasmo sus comentarios y opiniones! 
Su equipo de %SITE%</pre>"; 
$LNG['signup_confirmation_email_subject'] = "Registración en %SITE%"; 
$LNG['signup_error_email_missing'] = "Error: Usted no proveyo una dirección de email válida, por favor, provea una dirección de email válida."; 
$LNG['signup_error_email_used'] = "Error: Este email ya está en uso en %SITE%. Por favor, elija otro."; 
$LNG['signup_error_login_missing'] = "Error: Usted no proveyó un nombre de login, por favor provéalo."; 
$LNG['signup_error_login_used'] = "Error: Este login ya existe. Por favor, elija uno diferente."; 
$LNG['signup_error_password_invalid'] = "Error: Contraseńa es inválida. La contraseńa debe tener al menos 6 caracteres."; 
$LNG['signup_error_password_missing'] = "Error: Usted no proveyó una contraseńa, por favor provéala."; 
$LNG['signup_error_password_wrong'] = "Error: Contraseńa y la contraseńa repetida no concuerdan. Las contraseńas deben ser exactamente iguales en ambos campos."; 
$LNG['signup_error_terms_missing'] = "Usted debe estár de acuerdo con los terminos y condiciones para ser miembro de nuestro sitio web."; 
$LNG['signup_successful_01'] = "Registración como Miembro de %SITE% ha sido exitosa."; 
$LNG['signup_successful_02'] = "Gracias por registrarse!"; 
$LNG['signup_successful_03'] = "Para completar su registración, por favor revise su email."; 
$LNG['signup_successful_04'] = "<b>ATENCION: Por favor, revise su carpeta de email chatarra si no recibe un email de nosotros, en algunos casos puede tomar varias horas para recibirlo.</b>"; 
$LNG['signup_verification_email'] = "<pre> 
Estimado %USER%, 
gracias por registrarse con %SITE%. 
Para convertirse en miembro completo, primero debe visitar el vínculo a continuación, para activar su membresía. 
<a href=\"%LINK%\">%LINK%</a> 
Sitio web: %SITE% 
Su equipo de %SITE%.</pre>"; 
$LNG['signup_verification_email_subject'] = "Registración en %SITE%"; 
$LNG['silicon'] = "Silicon"; 
$LNG['single_girl'] = "Escort independiente"; 
$LNG['sitemap'] = "Mapa del Sitio"; 
$LNG['smoker'] = "Fumador"; 
$LNG['somewhere_else'] = "Otro lugar"; 
$LNG['soon'] = "Pronto"; 
$LNG['state'] = "Provincia"; 
$LNG['step_1'] = "Paso 1"; 
$LNG['step_2'] = "Paso 2"; 
$LNG['submit'] = "Enviar"; 
$LNG['subscribe'] = "Subscribirse"; 
$LNG['successfuly_logged_in'] = "Usted ha hecho login exitosamente."; 
$LNG['sucks'] = "Terrible"; 
$LNG['sunday'] = "Domingo"; 
$LNG['swallow'] = "Tragar semen"; 
$LNG['system_error'] = "Error del sistema, por favor contacte a Servicio al Cliente."; 
$LNG['terms_and_conditions'] = "Terminos y condiciones"; 
$LNG['terms_text'] = " 
    <h1>Condiciones generales de contrato (CGC)</h2> 
      
    <h2>&sect;1 Informe de negocios</h2> 
    <p>Las CGC regulan los informes entre los usuarios ( de seguido &quot;usuarios&quot;), los clientes (de seguido &quot;clientes&quot;) y la DA Products GmbH (de seguido &quot;DA&quot;), el gestor del sitio Internet &quot;EscortChicas.com&quot; (de seguido &quot;sitio web&quot;) y valen para todos los servicios y los productos ofrecidos en el sitio web, en segundo de los productos y de los servicios de seguido vienen indicados ulteriores condiciones contratantes.</p> 
     
    <h2>&sect;2 Directivos generales de uso</h2> 
    <p>El sitio web está destinado a usuarios que tengan más de 18 años de edad.</p> 
    <p>La edad y otras informaciones personales contenidos en el perfil del usuario o del cliente, en anuncios, chat, forum o reseñas ecc. están controlados de DA solo en muestra; este control no tiene validez jurídico y no es obligatorio. Cada usuario tiene la responsabilidad de controlar que sus partner hayan cumplido la mayor edad en base a la legislación en vigor en el propio país. La DA no es responsable para la utilización de las informaciones personales accesible en el sitio web.</p> 
     
    <h2>&sect;3 Datos usuarios / cliente, link de terceros, cookies</h2> 
    <p>En el acto de la utilización y en particular en el acto de la registración e inserción en el sitio web la DA puede poner en memoria algunas informaciones. El cliente/usuario puede controlar y modificar los propios datos contenidos en el perfil y en las inserciones cada vez que se desee. La DA también recuerda en forma anónimo los datos usuarios y cliente para los contactos, para el seguro técnico del sistema, para motivos estadísticos, de inovación y para el mejoramiento del producto.  Los datos no se disponen para terceros si no para la lucha contra los abusos y en caso de contenidos prohibidos por ley. Con la registración y la consecuente utilización del sitio web el cliente/usuario da su propio consentimiento a fin de que los datos puestos a disposición puedan ser elaborados y memorizados para uso interno consentido por la ley sobre privacy.</p> 
    <p>En caso de participación y concursos se proveerá a indicar asunto por asunto si los datos van transmetidos a terceros para poner el desarrollo del concurso.</p> 
    <p>El contenido de los datos trasmetidos por el usuario/cliente por Internet como e-mail (&quot;mensajes privados&quot;, SMS ó similares, no estan sometidos al contról de la DA. La DA no es responsable para los contenidos de dichos datos. La DA no utiliza direcciones mail consultivos para el envío de spam (los newsletter se envían exclusivo a los usuarios/clientes que preguntan expreso este servicio v. “Settings”) y no vende direcciones mail a quién envía spam. No se puede excluir que las direcciones mail consultivos en público sean utilizados de modo impropio de terceros, la DA igualmente no asume alguna responsabilidad en mérito.</p> 
    <p>El sitio web contiene link para ofertas de terceros. La DA no tiene ninguna influencia al tratamiento que hagan los interesados terceros con los datos de los usuarios recojidos en sus propios sitio web, la utilización de estos sitios tiene propio riesgo al usuario/cliente, la DA no toma ningúna responsabilidad para el utilizo propio o la difusión de los datos.</p> 
    <p>Para  adaptar el sitio web en la mejor manera posible a las exigencias de los usuarios/clientes en ciertos casos se insertan cookies (pequeños file memorizados en la computadora del usuario/cliente para facilitar el encuentro del sitio y su aplazamiento). Los cookies no contienen datos personales. Configurando el browser en un cierto modo es posible renunciar a la instalación de los cookies. Renunciando a los cookies puede provocar un empeoramiento ó la imposibilidad de acceder a los servicios y a las funciones ofrecidos por el sitio web; la DA no asume alguna responsabilidad en mérito.</p> 
     
    <h2>&sect;4 Forum y reseñas</h2> 
    <p>Con la registración al sitio web y la participación al forum y reseña el usuario declara que los derechos del autor son suyos y que a la DA vienen concedidos los derechos del uso para los artículos y las reseñas.</p> 
    <p>No es posible garantizar la intimidad de los forum y la publicidad de los artículos envíados. La DA no asume ninguna responsabilidad para la correción de los artículos contenidos y no tiene ninguna  correspondencia en mérito.</p> 
    <p>El contenido de la registración y de los artículos en forum y reseña no contravienen a las leyes suizas tampoco aquellos países dónde los artículos/reseñas son introducidos en la red. Contenidos referente a pornografía infantil, sexo con animales o excrementos y todos los datos memorizados de relativos usuarios y clientes serán transmetidos sín préaviso a la autoridad penales competentes. El usuario asegura que sus artículos/reseñas no contravienen a los derechos del autor, a la legislación sobre la tutela de los datos ó a los derechos de la concurrencia y se empeña a mantener la DA libre e indemne de petición por parte de terceros que se basan en actos ilegales del usuario ó en los errores del contenido de sus artículos y de las informaciones puestos a disposición y en general sobre las infracciones a la ley incluyendo los gastos jurídicos y defensas.</p> 
    <p>El usuario se compromete mantener en secreto los datos de login y los password, para evitar que las personas no autorizadas (en particular menores de edad) hayan accedido al sitio web.</p> 
    <p>La DA tiene derecho de cancelar sín indicar los motivos y sín préaviso de cualquier artículo. En caso de terceros que reclaman en modo digno los derechos para contenidos de artículos de cuales es responsable el usuario, la DA puede bloquear estos artículos y los datos relativos (también para salvar las pruebas).</p> 
    <p>La cancelación de los usuarios registrados solo se realiza con la solicitud por escrito y puede ser solicitado un control de la identidad. A su lado la DA puede cancelar en cualquier momento y sín justificaciones y registraciones injustas.</p> 
    <p>La DA reserva sus derechos pero no es su obligación controlar el sitio web y los perfiles, los contenidos y las ofertas publicadas. El bloque de dichos datos es exclusivo en discreción de la DA, la responsable utilización de parte del usuario/cliente queda expreso válido.</p> 
     
    <h2>&sect;5 inserciones y publicidad sobre banner en el sitio web</h2> 
    <p>El insercionista, en calidad del cliente, confiere a DA el encargo de poner a disposición en el sitio web para un cierto período un número deseado de espacios para inserciones comerciales ó no comerciales (con ó sín fotografía) o banner (las rubricas actuales son Escort, Boys, Travestis, Discounted Girls, Tours Directories interno, estas pueden ser ampliadas o reducidas en cualquier momento) en cambio del rembolso del cargo corriente del anuncio. La DA se reserva el derecho de modificar en cualquier momento el tipo, el volumen, las condiciones y los gastos de las inserciones para la decadencia en el futuro. La actividad de encargados se confirman al cliente por e-mail (si esta ha sido realizada) después del acontenido pago de la tarifa. La DA tiene derecho después de haber controdado los datos del cliente ó en caso del no acontenido pago de la tarifa debido por las inserciones, de renunciar el encargo ó de suspender ó anular sín préaviso; quiere decir que no alivia al cliente del pago de las sumas según debido por las prestaciones en decadencia. El pago puede ser efectuado en francos suizos ó euro en efectivo según acuerdo del contrato, por medio de bonífico anticipo ó carta de crédito.</p> 
    <p>Los clientes se obligan a facilitar solo datos conformes a la verdad según su persona y los servicios de ellos  ofrecidos, son responsables de los datos publicados y estan obligados a indemnizar la DA en caso de envío contra rembolso directo (icluído eventuales cargos de defensa y penables). Si se encuentran violaciones las inserciones pueden ser canceladas sín advertencia hasta su correción ó en casos graves de modo permanente sín derecho de rembolso de las sumas ya pagadas. Contenidos referente a pornografía infantil, sexo con animales o excrementos y todos los datos memorizados de relativos usuarios y clientes serán transmetidos sín préaviso a la autoridad penales competentes.</p> 
    <p>A cuanto se refieren a las inserciones se aplican el derecho suizo. El lugar de realización  y foro competente es en Zurich.</p> 
     
    <h2>&sect;6 Derechos del autor, modificación del sitio web</h2> 
    <p>Todos los files de prueba, las imágenes, los elementos web y sonidos, los logos y la gráfica de este sitio  y de todos los subcarteles son de propiedad intelectual de la DA y estan protejidos de los derechos del autor. Está prohibido descargar, copiar ó utilizar file no respetando las reglas del uso descrito en el contrato presente. En caso esta cláusula no será respetada el usuario/clliente cae en un procedimento civil y penable con demanda de compensación  dañales. La concesión de licenzia tiene que ser autorizada expresivo y por escreto de la DA. La DA puede modificar el sitio web en cualquier momento sin préaviso.</p> 
     
    <h2>&sect;7 Responsabilidad, hyperlinks</h2> 
    <p>La DA no está obligada en ningún momento garantizar ó poner en marcha el acceso al sitio. El sitio web puede ser desactivado en cualquier momento sin préaviso, también en tiempo indeterminado. No se asume ningúna responsabilidad por causa de daños causados de hardware ó software defectos ó de acciones de terceros (también en caso de negligencia) ó por daños de cualquier tipo consecuentes al acceso (ó a la imposibilidad de acceder). Queda a salvo el rembolso de parte de los gastos de las inserciones en caso de imposibilidad de acceso para un período prolongado por única responsabilidad de la DA (por culpa grave); la imposibilidad de acceso creada por el entretenimiento del sitio no da el derecho al rembolso. Salvo a los clientes insercionistas la DA no concede algún suporte usuario.</p> 
    <p>El sitio web contiene link a lugares internet externos (“hyperlinks”). Inseriendo estos link la DA no se aprovecha ni de los sitios internet tampóco de sus contenidos. Para los contenidos de los sitios en link son ellos mismos responsables y exclusivo los oferentes respetados. Para contenidos desconocidos no se dan ninguna garantía tampóco para daños consecuentes y para la dsiponibilidad de los sitios linkati.</p> 
     
    <h2>&sect;8 Elección jurídica, foro competente</h2> 
    <p>Estos CGC i los raportes relativos jurídicos están firmados por el derecho suizo. El foro competente es en Zurich.</p> 
    <p>Junio 2006, se reserva el derecho de aportar modificaciones en cada momento.</p> 
     
    <p>DA Products GmbH<br /> 
    Forchstrasse 113a<br /> 
    8127 Forch<br /> 
    Suiza</p> 
     
    <p><a href='mailto:ricco171717@hotmail.com'>ricco171717@hotmail.com</a></p> 
"; 
$LNG['terni'] = "Terni"; 
$LNG['text_of_message'] = "Texto del mensaje"; 
$LNG['thank_you'] = "Gracias"; 
$LNG['thanks_for_registration'] = "Gracias por registrarse! Para completar esta registración, por favor revise su email y siga las instrucciones incluídas en nuestro mensaje."; 
$LNG['third_party_escort_reviews'] = "Experiencias de escorts en tercera persona"; 
$LNG['thursday'] = "Jueves"; 
$LNG['time'] = "Hora"; 
$LNG['to'] = "Hasta"; 
$LNG['to_change_use_menu'] = "Para cambiar o modificar sus datos y preferencias utilice por favor el menú privado en la tapa."; 
$LNG['today_new'] = "Nuevo hoy"; 
$LNG['top20'] = "Chica del Mes"; 
$LNG['top20_desc'] = "Vote por su favorita personal y hágala Chica del Mes! Al final de cada mes, la chica con la calificación más alta será la nueva Chica del Mes! 
<br /></br /> Vote AHORA! La calificación es de 1 (peor) a 10 (mejor)!"; 
$LNG['top_10_ladies'] = "Las 10 mejores chicas"; 
$LNG['top_10_reviewers'] = "Los 10 mejores escritores de experiencias"; 
$LNG['tour_add'] = "Agregar tour"; 
$LNG['tour_already'] = "Usted ya tiene un tour en estas fechas"; 
$LNG['tour_contact'] = "Contacto del tour"; 
$LNG['tour_duration'] = "Duración del tour"; 
$LNG['tour_email'] = "Email del tour"; 
$LNG['tour_empty_country'] = "País no puede estar vacío"; 
$LNG['tour_empty_from_date'] = "Desde la fecha no puede estar vacía"; 
$LNG['tour_empty_to_date'] = "Hasta la fecha no puede estar vacía"; 
$LNG['tour_girl'] = "Chica de tour"; 
$LNG['tour_location'] = "Locación del tour"; 
$LNG['tour_lower_date'] = "Hasta la fecha no puede ser menor que Desde la fecha"; 
$LNG['tour_no_escorts'] = "Por favor primero agrege un perfil de escort"; 
$LNG['tour_phone'] = "Teléfono del tour"; 
$LNG['tour_saved'] = "Datos del tour guardados"; 
$LNG['tour_update'] = "Actualizar tour"; 
$LNG['tour_updated'] = "Datos del tour actualizados"; 
$LNG['tours'] = "Tours"; 
$LNG['trans'] = "Travestis"; 
$LNG['trial'] = "Prueba"; 
$LNG['trial_mb_desc'] = " 
<p>Se le ha otorgado una membresía de prueba en ".NGE_SITE_TITLE.". Durante este periodo, usted tendrá acceso a estos beneficios:</p> 
<ul> 
    <li>perfil en la página principal y en la página de la ciudad que usted elija</li> 
    <li>usuario puede escribir experiencias sobre usted</li> 
    <li>participación el la lista Top10 y en la promoción Chica del Mes</li> 
    <li>participación en la rotación de perfiles, para que su perfil nunca se quede en la última página</li> 
</ul> 
<p>y Más!</p> 
"; 
$LNG['trial_mb_desc2'] = " 
<p>Despues del periodo de prueba, usted necesitará comprar un Espacio Premium para mantener estos beneficios, y ademas, obtendrá una <strong style='color: red;'>Oferta Especial</strong> ! 
<ul> 
    <li>usted podrá usar la herramienta Tour de Ciudad, lo cual le dara acceso a muchas otras herramientas de promoción</li> 
    </ul> 
<p><a href='mailto:".NGE_CONTACT_EMAIL."'>Haga click aquí para ordernar!</a></p> 
"; 
$LNG['trial_mb_desc_ag'] = "%COUNT% escorts (1ra expira en %DAYS% días)"; 
$LNG['trustability'] = "Confiabilidad"; 
$LNG['trusted_review_popup'] = " 
    <p>Usted nos provee información sobre su encuentro, nosotros le proveemos esta información directamente a la chica. La chica no puede ver su experiencia, y no sabe si vuestra experiencia es buena o mala hasta que ella confirme si le conoció a usted o no. La experiencia será mostrada como 100% confiable</p> 
    <p>Por favor provéanos información sobre usted y el lugar donde conoció a la chica, note que esta información solamente la chica obtendrá via email o sms. Toda su información en nuestro sitio web no será guardada. Por ejemplo, usted la contrató con su nombre o con un nombre ficticio, use este nombre y la hora exacta, is la contrató via email, use su email, etc.</p> 
    <p>Ejemplos:</p> 
    <ol> 
        <li>gianni escortforum@hotmail.com, lunes 17.enero 05 20:00pm</li> 
        <li>pinky 333 333 454, miercoles 16. Feb. 05 alrededor de 08:00pm en hotel ibis milano</li> 
    </ol>"; 
$LNG['trusted_user_mail_subject'] = "Usuario de confianza de %SITE%"; 
$LNG['trusted_user_mail_text'] = "<pre> 
Estimado %NAME%, 
gracias por escribir un tema en %SITE% ! 
Usted se ha convertido en un usuario de confianza, y ya no requerirá que la administración verifique sus temas. 
Le damos la bienvenida a nuestra comunidad y le deseamos un monton de diversión! 
El equipo de %SITE%.</pre>"; 
$LNG['tuesday'] = "Martes"; 
$LNG['tuscany'] = "Tuscany"; 
$LNG['type'] = "Tipo"; 
$LNG['ugly'] = "fea"; 
$LNG['unfriendly'] = "No amistosa"; 
$LNG['unsubscribe'] = "Eliminar subscripción"; 
$LNG['upcoming_tours'] = "Tours futuros"; 
$LNG['update_languages'] = "Actualice idiomas"; 
$LNG['update_locations'] = "Actualice localidades"; 
$LNG['update_rates'] = "Actualizar precios"; 
$LNG['upg_esc_memb'] = "Mejore su membresía"; 
$LNG['upg_esc_memb_desc'] = "Gracias por su decisión de mejorar su membresía en ".NGE_SITE_TITLE." !<BR /> 
Por favor elija el tipo y duración de la membresía que desear comprar, y haga click en el boton \"<B>MEJORE SU MEMBRESIA AHORA</B>\" 
para ir a la página de nuestro procesador de pagos:"; 
$LNG['upgrade_membership'] = "Mejore su membresía"; 
$LNG['upgrade_now'] = "MEJORE SU MEMBRESIA AHORA!"; 
$LNG['upload_failed'] = "Subir la foto ha fallado."; 
$LNG['upload_picture'] = "Subir photo"; 
$LNG['upload_private_photo'] = "Subir fotos privadas"; 
$LNG['url'] = "Web"; 
$LNG['user'] = "Usuario"; 
$LNG['username'] = "Usuario"; 
$LNG['users_match'] = "Usuarios que concuerdan con su criterio de búsqueda"; 
$LNG['vacation_add'] = "Agregar vacaciones"; 
$LNG['vacation_end'] = "Regreso de vacaciones"; 
$LNG['vacation_no_escorts'] = "Por favor, primero agregue un perfil de escort "; 
$LNG['vacation_start'] = "Comienzo de vacaciones"; 
$LNG['vacations'] = "Vacaciones"; 
$LNG['vacations_active'] = "%NAME% está de vacaciones <strong>desde %FROM_DATE% hasta %TO_DATE%</strong>"; 
$LNG['vacations_active_soon'] = "%NAME% está de vacaciones, y regresará <strong>pronto</strong>"; 
$LNG['vacations_currently_date'] = "Esta chica está de vacaciones en este momento, y regresará en %DATE%."; 
$LNG['vacations_currently_soon'] = "Esta chica está de vacaciones en este momento, y regresará <strong>pronto</strong>."; 
$LNG['vacations_empty_to_date'] = "La fecha de regreso de vacaciones no puede estár vacía"; 
$LNG['invalid_vacation_dates'] = "Vacation dates are not valid"; 
$LNG['vacations_lower_date'] = "La fecha de inicio no puede ser menor que la fecha final"; 
$LNG['vacations_return'] = "Regresa de vacaciones"; 
$LNG['verification_email'] = "Estimado %USER%, gracias por registrarse con %SITE%. Para confirmar su registración, por favor haga click en este vínculo:"; 
$LNG['verification_email_subject'] = NGE_SITE_TITLE." verificación de cuenta"; 
$LNG['very_simple'] = "Muy simple"; 
$LNG['view_gallery'] = "Ver galería"; 
$LNG['view_my_private_photos'] = "Ver mis fotos privadas"; 
$LNG['view_my_profile'] = "Ver mi perfil"; 
$LNG['view_my_public_photos'] = "Ver mis fotos publicas"; 
$LNG['view_profile'] = "Ver perfil"; 
$LNG['view_reviews'] = "Agregar/Ver experiencias"; 
$LNG['view_site_escort_reviews'] = "Ver experiencias de escorts de %SITE%"; 
$LNG['view_third_party_escort_reviews'] = "Ver experiencias de escorts de terceras personas"; 
$LNG['vip'] = "VIP"; 
$LNG['vip_mb_desc_ag'] = "%COUNT% escorts"; 
$LNG['vote'] = "Votar"; 
$LNG['vote_her_looks'] = "Votar por su apariencia física"; 
$LNG['votes'] = "Votos"; 
$LNG['waist'] = "Cintura"; 
$LNG['watch_list'] = "Lista de favoritos"; 
$LNG['web'] = "Sitio web"; 
$LNG['wednesday'] = "Miércoles"; 
$LNG['week'] = "Semana"; 
$LNG['weeks'] = "Semanas"; 
$LNG['weight'] = "Peso"; 
$LNG['welcome'] = "Bienvenido en ".NGE_SITE_TITLE; 
$LNG['whats_new'] = "Qué es lo nuevo ?"; 
$LNG['when_you_met_her'] = "Cuándo la conoció"; 
$LNG['when_you_met_her_desc'] = "fecha y hora"; 
$LNG['when_you_met_lady'] = "Cuando conoció a la chica..."; 
$LNG['where_did_you_meet_her'] = "Donde la conoció"; 
$LNG['where_you_met_her'] = "Donde la conoció"; 
$LNG['where_you_met_her_desc'] = "hotel, su lugar, vuestro apartamento"; 
$LNG['white'] = "Blanco"; 
$LNG['whole'] = "Completo"; 
$LNG['wlc_city'] = "Localidad de trabajo (ciudad)"; 
$LNG['wlc_country'] = "País donde trabaja"; 
$LNG['women'] = "Mujeres"; 
$LNG['working_locations'] = "Localidades de trabajo"; 
$LNG['working_time'] = "Horas de trabajo"; 
$LNG['write_here_name_of_site'] = "Por favor, agregue en los campos a continuación el nombre y dirección web del sitio donde la chica aparece."; 
$LNG['wrong_login'] = "Login incorrecto"; 
$LNG['wrong_login_or_password'] = "Login o contraseńa incorrectos"; 
$LNG['wrong_time_interval'] = "Intervalo de tiempo erróneo el dia %DAY%."; 
$LNG['year'] = "Ańo"; 
$LNG['years'] = "Años"; 
$LNG['yellow'] = "Amarillo"; 
$LNG['yes'] = "Yes"; 
$LNG['yes_more_times'] = "Si, más veces"; 
$LNG['you_are_premium'] = "Usted es un Miembro Premium"; 
$LNG['you_have_log'] = "Usted debe estár logged-in para usar esta herramienta. <a href='%LINK%'>Haga click aquí para Iniciar sesión</a> o <a href='%LINK2%'>Regístrese ahora.</a>"; 
$LNG['you_have_voted_for'] = "Usted ha votado por la escort %ESCORT%! Gracias."; 
$LNG['you_r_watching_agencies'] = "Estas <b>agencias</b> están en su lista de favoritos:"; 
$LNG['you_r_watching_escorts'] = "Estas <b>escorts</b> están en su lista de favoritos:"; 
$LNG['you_r_watching_users'] = "Estos <b>usuarios</b> están en su lista de favoritos:"; 
$LNG['your_booking_text'] = "Tu texto personalizado respecto a esta contratación"; 
$LNG['your_data'] = "Sus datos"; 
$LNG['your_discount'] = "Su descuento: %DISCOUNT%%"; 
$LNG['your_full_name'] = "Su nombre completo"; 
$LNG['your_info'] = "Su información"; 
$LNG['your_info_desc'] = "e-mail, teléfono, nombre"; 
$LNG['your_login_name'] = "Su nombre de usuarios"; 
$LNG['your_name'] = "Su nombre"; 
$LNG['your_report'] = "Su reporte"; 
$LNG['zip'] = "Código postal"; 
$LNG['69'] = "69"; 
// 2006/12/11 
$LNG['tours_in_other_countries'] = "Tours en otros países"; 
$LNG['brunette'] = "Brunette"; 
$LNG['hazel'] = "Hazel"; 
$LNG['profile_or_main_page_links_only'] = "Add links only to your profile/ main webpage of  your agency or to your personal website."; 
$LNG['competitors_links_will_be_deleted'] = "All third-party advertising/ competitors links will be deleted by administrator!"; 
$LNG['domain_blacklisted'] = "Domain blacklisted"; 
$LNG['phone_blocked'] = "Phone number is blocked"; 
$LNG['girls'] = "Chicas"; 
$LNG['inform_about_changes'] = "Thank you. We will inform you about changes on " . NGE_SITE_TITLE; 
$LNG['escort_photos_over_18'] = "Todos las escorts tenia mas de 18 años en el momento de la publicacion de su anuncio."; 
$LNG['captcha_please_enter'] = "Por favor escribes"; 
$LNG['captcha_here'] = "aquí"; 
$LNG['captcha_verification_failed'] = "Verificación del texto falló."; 
$LNG['my_awards'] = "My Awards"; 
$LNG['escort_girls'] = "Escort Girls"; 
$LNG['locally_available_in_countries'] = "Nosotros estamos tambien en estos Naciones"; 
$LNG['domina'] = "Domina"; 
$LNG['dominas'] = "Dominas"; 
$LNG['club'] = "Club"; 
$LNG['club_name'] = "Nombre de club"; 
$LNG['club_profile'] = "Perfil de club"; 
$LNG['studio'] = "Studio"; 
$LNG['studio_girls'] = "Studio Girls"; 
$LNG['all_regions'] = "All regions..."; 
$LNG['see_all_escorts_of_this_agency'] = "See all escorts of this agency"; 
$LNG['see_all_escorts_of_this_club'] = "See all escorts of this club"; 
$LNG['invalid_height'] = "Altura inválida"; 
$LNG['invalid_weight'] = "Peso inválida"; 
$LNG['subscribe_to_newsletter'] = "Subscribete a nuestra newsletter ya!!"; 
$LNG['inform_every_week'] = "Te informaremos cada semana de nuestras ultimas chicas Calientes en " . NGE_SITE_TITLE; 
$LNG['clubs_match'] = "Clubs matching criteria"; 
switch (APPLICATION_ID) 
{ 
    case 1: // escortforumit.com 
        $LNG['index_bottom_text'] = "Da noi trovi le piu belle accompagnatrici, escort, girl, annunci, di tutta Italia, inoltre abbiamo annunci di escort, girl, accompagnatrici, nelle maggiori città d'Italia Roma, Milano, Torino, Bologna, Napoli, Firenze! Cosa aspetti contatta le accompagnatrici piu belle che non hai mai trovatoa Roma, Bologna, Firenze, Napoli, Milano, Torino, e in tutte le altre città d'Italia. Troverai sempre gli annunci, escort, girl e le accompagnatrici piu belle ditutta Italia sempre aggiornate."; 
        break; 
    case 2: // escort-annonce.com 
        $LNG['index_bottom_text'] = "Bienvenue dans Escort annonce, le plus grand annuaire d'annonces de rencontres d'Escorts girl /boys/ trans en France et en Europe. Vous trouverez des Escort à Paris Toulouse Lyon Marseille et toutes les grandes villes de France où vous pourrez faire une rencontre sexe entre adultes consentants. Vous pourrez aussi prendre connaissance des appréciations sur les Escort girl rencontrées par les autres membres, et aussi le tchat pour voir en direct via la webcam l'Escort girl de votre choix avant de la rencontrer. Pour une rencontre furtive d'une Escort girl après le bureau, Escort annonce est le site où vous pourrez revenir plusieurs fois par jour pour voir les nouvelles girls inscrites près de chez vous pour des rencontres les plus chaudes. Si vous rencontrez ou voyez une annonce d'escort qui ne correspond pas à la réalité, n'hésitez pas nous contacter, nous ferons notre possible pour n'avoir que des vraies annonces pour faire de vraies rencontres coquines."; 
        break; 
    case 3: // hellasescorts.com 
        $LNG['index_bottom_text'] = "Στο " . NGE_SITE_TITLE . " θα βρείτε callgirl και τις πιο καυτές και σέξυ συνοδούς & από την Αθήνα, συνοδούς από Θεσσαλονίκη, συνοδούς από Πάτρα και όλες τις μεγάλες πόλεις της Ελλάδας."; 
        break; 
         
    case 4: // escortchicas.com 
        $LNG['welcome_description'] = "Tu portal relax en Toda España. Putas independientes y Club relax en Barcelona, Madrid, Mallorca, Valencia, Sevilla y toda España. Encuentra ahora una nueva Amante, una compañera de viajes o simplemente una aventura erótica, en España o en cualquier parte de Europa. Anuncios eroticos con telefono directo de todas las mujeres mas caliente en España. Actualizamos diariamente nuestras chicas con Fotos y Calificaciones. Inscribete gratis y aprovecha de nuestros servicios. Si tienes alguna inquietud porfavor contactonos por e-mail: <a href='mailto:" . NGE_CONTACT_EMAIL . "'>" . NGE_CONTACT_EMAIL . "</a>"; 
        $LNG['index_bottom_text'] = "Estàs buscando relax en barcelona o un relax en madrid? En nuestro portal tenemos contactos escorts en todas las ciudades de España, relax en valencia, relax en sevilla o en las islas tambien tenemos anuncios de relax en mallora, ibizao las islas canarias. Diferentes anuncios escorts de agencias, escorts independenties y travestis. Si no buscas los que quieres encontrar en otros portales aqui lo encuentraras seguramente. Mira los anuncios y encuentra en tu ciudad las mas calientes mujeres."; 
        break; 
    case 6: // sexindex.ch 
        $LNG['welcome_description'] = "dem grössten Netzwerk für erotische Kontakte, Sex Clubs und Escort in Europa! Auf der Suche nach einem Bordell, einer Ferienbegleitung, einem Seitensprung, einem Escort Girl oder guten Sex Clubs? Bei uns werden Sie fündig - täglich aktuelle Kontaktanzeigen (Setcards) mit Fotos und Bewertungen. Melden Sie sich GRATIS an und profitieren Sie von vielen Vorteilen und Vergünstigungen! Bei Fragen oder Problemen wenden Sie sich bitte direkt an: <a href='mailto:" . NGE_CONTACT_EMAIL . "'>" . NGE_CONTACT_EMAIL . "</a>"; 
        $LNG['advertise_now'] = "<a href='" . NGE_URL . "/advertising.php' rel='nofollow'>Werben Sie gratis auf Sexindex.ch!</a> Auskunft unter <a href='mailto:" . NGE_CONTACT_EMAIL . "'>" . NGE_CONTACT_EMAIL . "</a> oder Telefon 078 899 14 89"; 
        $LNG['autumn_action'] = " 
        <div style='overflow: hidden; width: 755px; padding: 5px 10px; font-size: 10px; line-height: 1.1; background: #f5f5f5; border: 1px solid #ccc;'> 
            <div style='float: left; width: 360px;'> 
                <p><big>Sexindex.ch Herbst Aktion<br /> 
                <span style='color: #DA1E05;'>Werbung die sich auszahlt – über 120,000 Besucher pro Monat</span></big></p> 
                <p>Profitieren Sie von unserer Aktion und schalten Sie noch heute Ihre Premium Sexanzeige<br /> 
                (Premium = Topplatzierung, immer an oberster Stelle gelistet).</p> 
                <p style='color: #DA1E05;'>15 TAGE PREMIUM INSERAT CHF 100.00<br /> 
                30 TAGE PREMIUM INSERAT CHF 200.00</p> 
            </div> 
            <div style='margin: 0 0 0 385px;'> 
                <p>Es geht ganz einfach. Senden Sie uns folgende Angaben per <a href='mailto:" . NGE_CONTACT_EMAIL . "'>Email</a> zu und Ihre Sedcard wird umgehend aufgeschaltet:</p> 
                <ul> 
                    <li>Dauer des Inserates</li> 
                    <li>Ihre Fotos und Kontaktdaten (Telefon und/oder E-Mail)</li> 
                    <li>Gewünschter Name und Inseratetext</li> 
                    <li>Ihr Arbeitsort / Stadt</li> 
                    <li>Ihre Webseite (URL) (optional)</li> 
                </ul> 
                <p>Für mehr Informationen oder Fragen zu diesem Angebot oder zur normalen Gratiswerbung auf Sexindex.ch kontaktieren Sie uns wie gewohnt per <a href='mailto:" . NGE_CONTACT_EMAIL . "'>Email</a> oder Telefon 078 899 14 89.<br /> 
                Das Inkasso erfolgt nachträglich (bar oder Banküberweisung).</p> 
            </div> 
        </div> 
        "; 
        $LNG['escort_girls_description'] = "All diese attraktiven Ladies besuchen Sie zu Hause, im Büro oder im Hotel. Oder sind Sie auf der Suche nach einer Ferienbegleitung? Swingerclubbegleitung? Hier werden Sie sicher fündig! Täglich aktuell."; 
        $LNG['independent_girls_description'] = "Auf der Suche nach einem ganz diskreten Abenteuer? Hier inserieren Girls die Sie in absolut privater Atmosphäre empfangen – entweder in einer Privatwohnung oder einem kleinen Salon ohne Bar/Empfang. Diskret und privat!"; 
        $LNG['studio_girls_description'] = "Täglich aktuell finden Sie hier die heissesten Girls aus den besten Etablissements der ganzen Schweiz."; 
        $LNG['index_bottom_text'] = "Gemäss Statistik sind Begriffe die etwas mit Sex und Erotik zu tun haben die meist gesuchten im Internet. 98 Prozent der Männer waren gemäss Umfragen schon mindestens einmal auf Pornoseiten. Beliebt sind auch Suchbegriffe wie Kontaktanzeigen, Bordell, Sex Clubs, Escort, ficken, Transen etc. In der Schweiz gibt es eine Vielzahl an Sex Seiten und Sexführern. Und manchmal ist es schwierig den Ueberblick zu behalten. Denn wirklich gute Sex-Angebote, wo man auch das findet was man sucht, sind rar. Oder ist es Ihnen nicht auch schon so ergangen? Sie tippen bei einer Suchmaschine den Begriff „Bordell“ ein und landen, statt auf einem Sexführer, auf einer kostenpflichtigen Seite mit Kontaktanzeigen? Oder Sie haben Lust auf schnellen Sex und suchen ein Bordell in Ihrer Nähe wo Sie richtig geil ficken können? Statt dessen werden Sie auf eine Seite geführt wo Sie nur ein kleines Sexangebot haben und wenige Escort Girls / Sex Clubs aufgeführt sind. Wie gesagt – es ist ziemlich schwierig. Einige Seiten haben sich etabliert und decken gewisse Sparten ab. Für Transen Kontakte zum Beispiel eignet sich das Portal happysex hervorragend. Denn happysex hat in den meisten Regionen der Schweiz Kontaktanzeigen bzw. Sexanzeigen von Transen. Auch Bordelle, Escort Girls und Sex Clubs werden aufgeführt. Jedoch nimmt happysex im direkten Vergleich zu anderen Sex Seiten vom Design und Aufbau her einer der hinteren Plätze ein. Bei uns auf Sexindex legen wir wert auf Aktualität der Sex Kontakt Anzeigen und Sedcards der Escort Girls. Wir nehmen regelmässig neue Bordelle, Sexstudios, private Frauen, Escorts und Salons in unsere Datenbank auf. Alle Sedcards sind mit Fotos und Details und sind einheitlich präsentiert. Auch die Werbung versuchen wir dezent zu halten – jedoch brauchen wir die Banner um für Sie weiterhin ein gratis Sexführer zu bleiben. In diesem Sinne – ficken ist die schönste Nebensache der Welt – und ob Sie auf sexindex oder happysex fündig werden spielt schlussendlich keine Rolle. Happy hunting!"; 
        break; 
} 
$LNG['escort_girls_in_city'] = "Escort Girls in %CITY%"; 
$LNG['independent_girls_in_city'] = "Private Girls in %CITY%"; 
$LNG['studio_girls_in_city'] = "Studio Girls in %CITY%"; 
$LNG['new_arrivals_in_city'] = "New Girls in %CITY%"; 
$LNG['all_girls_in_city'] = "All girls in %CITY%"; 
$LNG['see_all_girls_from_agency'] = "click to see all girls from this agency"; 
$LNG['see_all_independent_girls'] = "click to see all independent girls"; 
$LNG['last_modified'] = "Last modified"; 
$LNG['advertise_with_us'] = "Anunciar con nosotros"; 
$LNG['nearest_cities_to_you'] = "Nearest cities to you"; 
$LNG['nearest_cities_to_city'] = "Nearest cities to %CITY%"; 
$LNG['cities_in_region'] = "Cities in %REGION%"; 
$LNG['local'] = "Local"; 
$LNG['local_directory'] = "Club/Escort listing"; 
$LNG['exchange_banners'] = "Intercambio de enlaces. Si quieres intercambiar banners y/o enlaces con nosotros, escríbenos a <a href='mailto:" . NGE_CONTACT_EMAIL . "'>" . NGE_CONTACT_EMAIL . "</a>"; 
$LNG['our_network_in_europe'] = "Nuestra red de portales en Europa"; 
$LNG['agencies_and_independent_in_spain'] = "Agencias y chicas independientes en España"; 
$LNG['directories_and_guide_in_spain'] = "Directorios, foros y guias eroticas en España"; 
$LNG['friends'] = "Amigos"; 
$LNG['escort_service'] = "Servicios escorts"; 
$LNG['studio_and_private_girls'] = "Agencia relax y chicas independientes"; 
$LNG['advertising'] = "Advertising"; 
$LNG['premium_girls'] = "Premium girls"; 
$LNG['submit_form_to_signup'] = "Submit this form to signup and get Promoted from largest escort directory!"; 
$LNG['signup_now_to_largest_escort_directory'] = "Signup now for <span class='pink'>FREE</span> to the largest escort directory worldwide. We currently have over <span class='pink'>3 million clients</span> monthly on our network. Start using our popularity to promote yourself now <span class='pink'>FOR FREE!</span>"; 
$LNG['signing_on_all_local_sites'] = "By signing up, your profile will automatically be displayed on all these local country sites. Your login data will work on all the sites - you just have to signup once."; 
$LNG['please_choose_your_business'] = "Please choose your business"; 
$LNG['independent_privat'] = "Independent Escort / Privat Girl"; 
$LNG['escort_agency'] = "Escort Agency"; 
$LNG['club_bordell_strip'] = "Club / Bordell / Sauna Club / Stripclub (Tabledance), Massage Studio"; 
$LNG['please_fill_form'] = "Please fill out the following form"; 
$LNG['please_write_numbers_from_field'] = "Please write the numbers like in the field"; 
$LNG['just_moment_away_from_escort_directory'] = "You are just moment away from having access to largest escort directory!"; 
$LNG['choose_your_region'] = "Choose your region"; 
$LNG['setcards_online'] = "%COUNT% setcards online"; 
$LNG['only_escorts'] = "only escorts"; 
$LNG['only_private_girls'] = "only private girls"; 
$LNG['only_studio_girls'] = "only studio girls"; 
$LNG['most_popular_cities'] = "Most popular cities"; 
$LNG['renew_your_profile'] = "Your profile is expired. If you would like to renew it, contact your Sales person or write us at <a href='mailto:" . NGE_CONTACT_EMAIL . "'>" . NGE_CONTACT_EMAIL . "</a>"; 
$LNG['relax'] = "Relax"; 
$LNG['email_unsubscribed'] = "E-mail %EMAIL% is removed from " . NGE_SITE_TITLE . " newsletter."; 
$LNG['vip_girls'] = "VIP girls"; 
$LNG['crop_photo'] = "Crop photo"; 
$LNG['show_thumbnails_on'] = "Show thumbnails on"; 
$LNG['photo_was_cropped'] = "Photo was cropped."; 
$LNG['crop_feature_teaser'] = " 
    <p><strong style='color: red;'>ATTENTION:</strong><br /> 
    We have launched a new feature. All photos on the main page and in the international directory have now the same size. Therefore we would like to ask you to verify your selected main picture. Follow the instructions in escort &quot;edit gallery&quot; section.</p> 
"; 
$LNG['crop_feature_description'] = " 
    <p><strong style='color: red;'>ATTENTION:</strong><br /> 
    With our new feature, all MAIN pictures appear in the same size on our site. This gives the site a much more professional look.</p> 
    <p>How it works:</p> 
    <ol> 
        <li>Check now your main photo. If everything looks fine with the picture, you don't have to proceed to step 2. If anything looks wrong (i.e. head cut away), follow the instructions in step 2.</li> 
        <li>Click on &quot;crop photo&quot;</li> 
        <li>In the middle of the photo you now see a frame which is a preview of how the picture will look like on the main page. You can move the frame around until you have chosen the part of the photo you want. Click on &quot;crop the image&quot; button.</li> 
        <li>You are already done. You can review the photo on the edit gallery main page. If it looks alright, you are set.</li> 
    </ol> 
"; 
$LNG['we_only_sell_advertisement'] = NGE_SITE_TITLE . " es un portal de publicidad, y de información, y pues tal no tiene ninguna conexión o responsabilidad con cualesquiera  de los sitios o de los individuos mencionados aquí. Vendemos SOLAMENTE espacio de publicidad, no somos una agencia de acompañamiento, ni estamos de cualquier manera implicada en negocio de escort o de prostitución. No tomamos ninguna responsabilidad del contenido o de las acciones de los Web site o de los individuos  que usted puede tener acceso a los links al email o a contactos de teléfono de este portal."; 
$LNG['user_messages_review_waiting_for_approval'] = "Your review about %MESSAGE% is waiting for verification. Please be patient"; 
$LNG['user_messages_favorite_girl_review'] = "Your favorite girl <strong>%MESSAGE%</strong> has new review. <a href='%LINK%'>click here to see</a>"; 
$LNG['user_messages_review_approval'] = "Your review <strong>%MESSAGE%</strong> was approved by administrator."; 
$LNG['user_messages_review_deleted'] = "Your review <strong>%MESSAGE%</strong>  was deleted by administrator. Reason given: %LINK%"; 
$LNG['user_messages_new_discount'] = "%MESSAGE% is offering you a special EF discount. <a href='%LINK%'>Check her out!</a>"; 
$LNG['user_messages_deleted_discount'] = "%MESSAGE% is not offering you a special EF discount anymore."; 
$LNG['user_messages_favorite_disabled'] = "Your favorite girl <strong>%MESSAGE%</strong> is no longer with us! We are sorry! Maybe time to find a new favorite?"; 
$LNG['user_messages_favorite_activated'] = "Your favorite girl <strong>%MESSAGE%</strong> joined us again!"; 
$LNG['user_messages_girl_of_month'] = "Girl of the month %MONTH%/%YEAR% is: <strong>%MESSAGE%</strong>!! Congratulation %MESSAGE%!!!"; 
$LNG['user_messages_free_fuck_winner'] = "<strong>You are the WINNER of the freefuck lottery contact us.</strong>"; 
$LNG['user_messages_free_fuck_others'] = "The winner of the free fuck lottery of month %MONTH%/%YEAR% is: <strong>%MESSAGE%</strong>!! Congratulation %MESSAGE%!!!"; 
$LNG['user_messages_favorite_region_girls'] = "New girls from your favourite region joined EF since your last visit. %LINK%"; 
$LNG['user_messages_favorite_region_vip'] = "There is new VIP %MESSAGE% from your favourite region since your last visit. %LINK%"; 
$LNG['user_email_review_approved'] = "Hello %TARGET_0%\nYour review %TARGET_1% was approved by administrator.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['escort_email_review_approved'] = "Hello %TARGET_0%\nReview %TARGET_1% about you was approved by administrator.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_favorites_review_approved'] = "Hello %TARGET_0%\nYour favorite girl  %TARGET_1% has new review.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['agency_email_review_approved'] = "Hello %TARGET_0%\nReview about %TARGET_1% was approved by administrator.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_review_approval_favorites'] = NGE_SITE_TITLE . " new review"; 
$LNG['user_email_subject_review_approval'] = NGE_SITE_TITLE . " review approved"; 
$LNG['escort_email_subject_review_approval'] = NGE_SITE_TITLE . " review approved"; 
$LNG['agency_email_subject_review_approval'] = NGE_SITE_TITLE . " review approved"; 
$LNG['escort_email_review_new'] = "Hello %TARGET_0%\nNew review %TARGET_1% about you is waiting for approval.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['agency_email_review_new'] = "Hello %TARGET_0%\nYour escort %TARGET_1% has new review waiting for approval.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_review_new'] = "Hello %TARGET_0%\nYour review about %TARGET_1% is waiting for verification. Please be patient.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_review_new'] = NGE_SITE_TITLE . " review waiting for approval"; 
$LNG['escort_email_subject_review_new'] = NGE_SITE_TITLE . " new review waiting for approval"; 
$LNG['agency_email_subject_review_new'] = NGE_SITE_TITLE . " new review waiting for approval"; 
$LNG['user_email_review_deleted'] = "Hello %TARGET_0%\nYour review <strong>%TARGET_1%</strong>  was deleted by administrator. Reason given: %TARGET_2%\n" . NGE_SITE_TITLE . " Team"; 
$LNG['escort_email_review_deleted']	= "Hello %TARGET_0%\nThe review <strong>%TARGET_1%</strong> about you was deleted by administrator. Reason given: %TARGET_2%\n" . NGE_SITE_TITLE . " Team"; 
$LNG['agency_email_review_deleted']	= "Hello %TARGET_0%\nThe review about <strong>%TARGET_1%</strong> was deleted by administrator. Reason given: %TARGET_2%\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_review_delete'] = NGE_SITE_TITLE . " review deleted"; 
$LNG['escort_email_subject_review_delete'] = NGE_SITE_TITLE . " review deleted"; 
$LNG['agency_email_subject_review_delete'] = NGE_SITE_TITLE . " review deleted"; 
$LNG['user_email_discount_new'] = "Hello %TARGET_0%\n%TARGET_1% is offering you a special EF discount.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['escort_email_discount_new'] = "Hello %TARGET_0%\nNew discount was assigned to you.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['agency_email_discount_new'] = "Hello %TARGET_0%\n%TARGET_1% is now offering a special EF discount.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_discount_new'] = NGE_SITE_TITLE . " new discount"; 
$LNG['escort_email_subject_discount_new'] = NGE_SITE_TITLE . " new discount"; 
$LNG['agency_email_subject_discount_new'] = NGE_SITE_TITLE . " new discount"; 
$LNG['user_email_discount_deleted']	= "Hello %TARGET_0%\nYour escort %TARGET_1% is not offering a special EF discount anymore.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_discount_deleted'] = NGE_SITE_TITLE . " discount expired"; 
$LNG['user_email_escort_deactivated'] = "Hello %TARGET_0%\nYour favorite girl <strong>%TARGET_1%</strong> is no longer with us! We are sorry! Maybe time to find a new favorite?\n" . NGE_SITE_TITLE . " Team"; 
$LNG['escort_email_escort_deactivated'] = "Hello %TARGET_0%\nYour account has been deactivated.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['agency_email_escort_deactivated'] = "Hello %TARGET_0%\nAccount of %TARGET_1% has been deactivated.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_favorites_deactivated'] = NGE_SITE_TITLE . " escort deactivated"; 
$LNG['escort_email_subject_favorites_deactivated'] = NGE_SITE_TITLE . " account deactivated"; 
$LNG['agency_email_subject_favorites_deactivated'] = NGE_SITE_TITLE . " account deactivated"; 
$LNG['user_email_escort_activated'] = "Hello %TARGET_0%\nYour favorite girl <strong>%TARGET_1%</strong> joined us again!\n" . NGE_SITE_TITLE . " Team"; 
$LNG['escort_email_escort_activated'] = "Hello %TARGET_0%\nYour account has been activated.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['agency_email_escort_activated'] = "Hello %TARGET_0%\nAccount of %TARGET_1% has been activated.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['agency_email_subject_favorites_activated'] = NGE_SITE_TITLE . " escort activated"; 
$LNG['agency_email_subject_favorites_activated'] = NGE_SITE_TITLE . " account activated"; 
$LNG['agency_email_subject_favorites_activated'] = NGE_SITE_TITLE . " account activated"; 
$LNG['user_email_girl_of_the_month'] = "Hello %TARGET_0%\nGirl of the month %TARGET_1% is: <strong>%TARGET_2%</strong>!! Congratulation %TARGET_2%!!!\n" . NGE_SITE_TITLE . " Team"; 
$LNG['escort_email_girl_of_the_month'] = "Hello %TARGET_0%\nGirl of the month %TARGET_1% is: <strong>%TARGET_2%</strong>!! Congratulation %TARGET_2%!!!\n" . NGE_SITE_TITLE . " Team"; 
$LNG['escort_email_girl_of_the_month_winner'] = "Hello %TARGET_0%\nYou are new girl of the month %TARGET_1%! Congratulation %TARGET_0%!!!\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_girl_of_the_month'] = NGE_SITE_TITLE . " girl of the month"; 
$LNG['escort_email_subject_girl_of_the_month'] = NGE_SITE_TITLE . " girl of the month"; 
$LNG['user_email_free_fuck_winner'] = "Hello %TARGET_0%\n<strong>You are the WINNER of the freefuck lottery contact us.</strong>\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_free_fuck_others'] = "Hello %TARGET_0%\nThe winner of the free fuck lottery of month %TARGET_1% is: <strong>%TARGET_2%</strong>!! Congratulation %TARGET_2%!!!\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_free_fuck_winner'] = NGE_SITE_TITLE . " freefuck lottery winner"; 
$LNG['user_email_subject_free_fuck_others'] = NGE_SITE_TITLE . " freefuck lottery"; 
$LNG['user_email_new_girls_in_region'] = "Hello %TARGET_0%\nNew girls from your favourite region joined EF.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_new_girls_in_region'] = NGE_SITE_TITLE . " new girls"; 
$LNG['user_email_new_vip_girls_in_region'] = "Hello %TARGET_0%\nThere is new VIP %TARGET_1% from your favourite region since your last visit. %TARGET_2%\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_new_vip_girls_in_region'] = NGE_SITE_TITLE . " new VIP girls"; 
$LNG['loft'] = 'Loft'; 
$LNG['loft_escorts_photo_limit_reached'] = "Loft escorts can have max %LIMIT% photos in gallery."; 
$LNG['loft_escorts_cant_go_on_tour'] = "Loft escorts can't go on tour."; 
//CUBIX Added By David 
$LNG['email_exists'] = "Email exists"; 
/* design2 texts */ 
$LNG['members_login'] = "Members Login"; 
$LNG['signup_now'] = "Signup now"; 
$LNG['live_now'] = "Live now"; 
$LNG['girl_has_video'] = "Girl has video"; 
$LNG['girl_has_reviews'] = "Girl has reviews"; 
$LNG['100%_real_photos'] = "100% real photos"; 
$LNG['happy_hour'] = "Happy hour"; 
$LNG['icons_explanation'] = "Icons explanation"; 
$LNG['offer_incall'] = "Offer incall"; 
$LNG['offer_outcall'] = "Offer outcall"; 
$LNG['ebony_only'] = "Ebony only"; 
$LNG['asian_only'] = "Asian only"; 
$LNG['blondes_only'] = "Blondes only"; 
$LNG['add_to_my_favourites'] = "Add to my favourites"; 
$LNG['remove_from_favourites'] = "Remove from favourites"; 
$LNG['add_view_reviews'] = "Add/View reviews"; 
$LNG['about_escort'] = "About %SHOWNAME%"; 
$LNG['contact_me'] = "Contact me"; 
$LNG['now_open'] = "Now open"; 
$LNG['closed'] = "Closed"; 
$LNG['viewed'] = "viewed"; 
$LNG['quick_links'] = "Quick links"; 
$LNG['free_signup'] = "Free signup"; 
$LNG['signup_profit'] = " 
<ul> 
    <li>add <strong>comments</strong></li> 
    <li class='fsa-odd'>manage <strong>favorite</strong> list</li> 
    <li>write <strong>reviews</strong></li> 
    <li class='fsa-odd'>access to <strong>advanced search</strong></li> 
    <li><strong>watch</strong> list</li> 
</ul> 
"; 
$LNG['sort_by'] = "Sort by"; 
$LNG['random'] = "Random"; 
$LNG['alphabetically'] = "Alphabetically"; 
$LNG['most_viewed'] = "Most viewed"; 
$LNG['newest_first'] = "Newest first"; 
$LNG['narrow_your_search'] = "Narrow your search"; 
$LNG['with_reviews'] = "With reviews"; 
$LNG['search_by_name_agency'] = "Search by Name/Agency"; 
$LNG['comments_responses'] = "Comments &amp; Responses"; 
$LNG['post_comment'] = "Post a text comment"; 
$LNG['back'] = "Back"; 
$LNG['reply'] = "Reply"; 
$LNG['spam'] = "Spam"; 
$LNG['only_registered_users_can_add_comments']	= "Only registered users can add comments. Please <a href='%VALUE%'>login here</a>."; 
$LNG['your_comment'] = "Your comment"; 
$LNG['close'] = "Close"; 
$LNG['message_waiting_for_approval'] = "Thank you. Your message is waiting for approval now."; 
$LNG['ago'] = 'ago'; 
$LNG['time_ago'] = '%TIME% ago'; 
$LNG['yesterday'] = "Yesterday"; 
$LNG['minute'] = "minute"; 
$LNG['spanking'] = "Spanking"; 
$LNG['domination'] = "Domination"; 
$LNG['fisting'] = "Fisting"; 
$LNG['massage'] = "Massage"; 
$LNG['role_play'] = "Role play"; 
$LNG['bdsm'] = "BDSM"; 
$LNG['hardsports'] = "Hardsports"; 
$LNG['hummilation'] = "Hummilation"; 
$LNG['rimming'] = "Rimming"; 
$LNG['less_than'] = "less than %NUMBER%"; 
$LNG['more_than'] = "more than %NUMBER%"; 
$LNG['free_signup_advantages'] = "The signup on <span class='red'>" . NGE_SITE_TITLE . "</span> is absolutely <span class='red-big'>FREE</span> and has many advantages."; 
$LNG['reset'] = "Reset"; 
$LNG['private_area'] = "Private area"; 
$LNG['choose_your_choice'] = "Choose your choice"; 
$LNG['base_city'] = "Base city"; 
$LNG['additional_cities'] = "Additional cities"; 
$LNG['zone'] = "Zone"; 
$LNG['actual_tour_data'] = "NOW in %CITY% (%COUNTRY%)"; 
$LNG['upcoming_tour_data'] = "in %DAYS% in %CITY% (%COUNTRY%), from %FROM_DATE% - %TO_DATE%"; 
$LNG['beta_warning'] = NGE_SITE_TITLE . " is in Beta phase. We are constantly working on the site and make regular updates and improvements."; 
$LNG['ethnic_white'] = "Caucasian (white)"; 
$LNG['ethnic_black'] = "Black"; 
$LNG['ethnic_asian'] = "Asian"; 
$LNG['ethnic_latin'] = "Latin"; 
$LNG['ethnic_african'] = "African"; 
$LNG['ethnic_indian'] = "Indian"; 
$LNG['members_signup'] = "Members signup"; 
$LNG['click_here_your_signup'] = "Click here for your singup."; 
$LNG['your_email'] = "Your e-mail"; 
$LNG['missing_message'] = "Missing message"; 
$LNG['your_message_sent'] = "Your message was sent."; 
$LNG['signup'] = "Signup"; 
$LNG['signup_to_largest_escort_directory'] = "Signup now for <span class='red-big'>FREE</span> to the largest escort directory worldwide. We currently have over <span class='red-big'>3 million clients</span> monthly on our network. Start using our popularity to promote yourself now <span class='red-big'>FOR FREE</span>!"; 
$LNG['one_signup_all_sites'] = "By signing up, your profile will be automatically displayed on all these local country sites. Your login data will work on all the sites - you just have to signup once."; 
$LNG['email_verification_subject'] = NGE_SITE_TITLE . " account verification"; 
$LNG['email_verification_text'] = "<b>Dear %USER%,</b><br /><br /> 
<b>Thank you for registering at " . NGE_SITE_TITLE . "</b>.<br /><br /> 
You are 1 step away from activating your account, simply click on the link below or copy and paste it into your browser.<br /><br /> 
<b><a href='%LINK%' style='color: #dd0000;'>%LINK%</a></b><br /><br /> 
NOTE: <span style='font-size: 11px;'>if you experience any trouble, please contact us at <b><a href='mailto:" . NGE_CONTACT_EMAIL . "' style='color: #dd0000;'>" . NGE_CONTACT_EMAIL . "</a></b>.</span><br /><br /> 
<b>Yours faithfully</b><br /> 
<b>" . NGE_SITE_TITLE .  " Team</b>"; 
$LNG['email_confirmation_subject'] = NGE_SITE_TITLE . " registration"; 
$LNG['email_confirmation_text'] = "Dear %USER%,</b><br /><br /> 
Thank you for registering at " . NGE_SITE_TITLE . ".<br /> 
Your registration is now complete and you are an active member of " . NGE_SITE_TITLE . ".<br /><br /> 
<b>Following is your login information. Please keep this information on a safe place.<br /> 
<span style='display: block; float: left; width: 75px;'>Login:</span> <span style='color: #dd0000;'>%USER%</span><br /> 
<span style='display: block; float: left; width: 75px;'>Web:</span> <a href='" . NGE_URL . "' style='color: #dd0000;'>" . NGE_URL . "</a></b><br /><br /> 
Have fun on the site!<br /><br /> 
<b>Yours faithfully</b><br />  
<b>" . NGE_SITE_TITLE . " Team</b>"; 
//CUBIX Added By David 
$LNG['contact_us_text1'] = "In case if you wish to contact us regarding general enquiries, feedback or advertising, choose from the following options"; 
$LNG['phone_international'] = "Phone International"; 
$LNG['contact_us_text2'] = "Alternatively please use our contact form below"; 
$LNG['your_message'] = "Your Message"; 
$LNG['link_exschange'] = "Link Exchange"; 
$LNG['contact_us_text3'] = "If you would like to exchange links, please email the webmaster (e-mail).<br />Please copy our banner and place it on your server before contacting us regarding link exchange."; 
// 
//Cubix Added By Tiko 
$LNG['delete_profile'] = "Delete Profile"; 
$LNG['confirm_delete_profile'] = "Are you sure you want to delete the escort profile ?"; 
$LNG['delete_escort'] = "Delete escort profile"; 
$LNG['escort_profile_deleted'] = "Escort profile has been successfuly deleted"; 
$LNG['escort_profile_deleted_error'] = "Error: Escort profile has not been deleted"; 
$LNG['same_city'] = 'Working city and tour city can not be same.'; 
$LNG['overnight'] = 'overnight'; 
$LNG['directory_for_female_escorts'] = NGE_SITE_TITLE . ' is a UK escort directory for female escorts, escort agencies, independent escorts, trans and couples. 
On escortguide.co.uk you can find the perfect escort in London, Manchester, Leeds and all other areas in the United Kingdom. 
We update our escort directory on a daily basis - you will find new female escorts in london and from your area every day. 
If you are an escort agency or a female independent escort based in London or the UK, simply click on the 
signup link to register and upload your escort profile to escortguide.co.uk. '; 
//new search 
$LNG['kissing_no'] = "No Kissing"; 
$LNG['blowjob_no'] = "No Blowjob"; 
$LNG['cumshot_no'] = "No Cumshot"; 
$LNG['cumshot_in_mouth_spit'] = "Cumshot In Mouth Spit"; 
$LNG['cumshot_in_mouth_swallow'] = "Cumshot In Mouth Swallow"; 
$LNG['cumshot_in_face'] = "Cumshot In Face"; 
$LNG['with_comments'] = "with comments"; 
$LNG['nickname'] = "Nickname"; 
$LNG['without_reviews'] = "Without reviews"; 
$LNG['not_older_then'] = "Not older then"; 
$LNG['order_results'] = "Order results"; 
$LNG['select_your_girl_agency_requirements'] = "Select your girl/agency requirements"; 
$LNG['girls_details_bios'] = "Girls details/bio’s"; 
$LNG['cm'] = "cm"; 
$LNG['ft'] = "ft"; 
$LNG['kg'] = "kg"; 
$LNG['lb'] = "lb"; 
$LNG['girls_origin'] = "Girls origin"; 
$LNG['ethnicity'] = "Ethnicity"; 
$LNG['measurements'] = "Measurements"; 
$LNG['open_hours'] = "Open hours"; 
$LNG['my_favorites'] = "My Favorites";
$LNG['learn_more_webcam'] = "If you have a webcam plus an account at
Skype or MSN, this is the easiest way to
get your photos verified.
Just click on the 'Webcam session request' link,
choose a date and a time and wait for our
confirmation. At the requested time you will
then have an short online appointment (video chat)
with one of our adminstrators to approve that it's
you on the photos. After this session your
photos will be 100% verified. ";
$LNG['learn_more_passport'] = "If you choose this option you need to scan your
passport or any other official document such as
identity card or driving license and upload it
to our system along with 1-2 additional private photos.";

$LNG['verify_welcome'] = "Welcome!";
$LNG['verify_welcome_text'] = "Welcome to the 100% verified picture process.
Use the chance and get your profile pictures to be
100% verified genuine. In just a few simple
steps you will increase the credibility of your profile
and get many more users. You will get also a sticker
to your profile which will show to the users that your photos
are 100% genuine. Additionally your profile will be
listed in an extra sort by menu. ";
$LNG['verify_enroll'] = "Enroll now!";
$LNG['verify_enroll_text'] = "We provide currently the following <strong>2 options</strong> to get your pictures 100% verified:";
$LNG['verify_enroll_1'] = "Webcam session request with our admin";
$LNG['verify_enroll_2'] = "Passport copy";
$LNG['verify_learn_more'] = "Learn more...";
$LNG['verify_start_now'] = "<span>START NOW </span> the process";
$LNG['verify_steps'] = "STEP GUIDLINE:";
$LNG['verify_select_2_options'] = "Please select one of the following 2 options for the process:";
$LNG['verify_select_1'] = "Webcam session request with our admin";
$LNG['verify_select_1_note'] = "<strong>note:</strong> you need to have a webcam and an account at Skype or MSN ";
$LNG['verify_select_2'] = "Passport copy - you need a valid passport or identity card";
$LNG['verify_select_2_note'] = "<strong>note:</strong> to speed up the process we recommend to upload 1-2 private photos in addition to the passport copy. ";
$LNG['verify_important'] = "IMPORTANT - PLEASE READ CAREFULLY: Any attempt of fraud or tricking the process will result in a lifelong
banning from the EF network. All your paid advertisements will be disabled and no refund given. ";

$LNG['verify_sms_message'] = "Videochat confirmation for the photo verification process: 
Date: #date#, Time: #time#. You will be contacted by our administrator. #title#";
$LNG['verify_email_subject'] = "Online appointment confirmation";
$LNG['verify_email_message'] = "Hereby we confirm your online appointment for the photo verification process at:<br /><br />
Date: #date#<br />
Time: #time#<br /><br />
You will be contacted by one of our administrators. Kind regards.<br />#title#";

$LNG['reject_sms_message'] = "Please choose 3 new dates for the videochat session (photo verification). We could not confirm your requested dates. Just login and choose again. #title#";
$LNG['reject_email_subject'] = "100% photo verification process / please enter new dates";
$LNG['reject_email_message'] = "Dear #showname#<br /><br />
Unfortunately all suggested dates for the video chat session to verify your pictures could not been confirmed. Please login again to your private area and choose three new
dates. We would like to appologize for any inconvenience caused.<br /><br />
Kind regards<br />
#title#";
$LNG['reg_has_been_succ'] = "Registration has been successful !";
$LNG['verify_attention'] = "Important note: For uploading the photos please use solely the form below. Do not send the photos by e-mail. Only send an e-mail to the sales representative in case you experience any problems uploading the pictures. ";