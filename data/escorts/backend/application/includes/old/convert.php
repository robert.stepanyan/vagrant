<?php
$LANGS = array('en', 'de', 'fr', 'it', 'gr');

function convert($string)
{
	// all NGE_ constants in the middle of string
	$a = array();
	$constants_regex = '#"\s*\.\s*NGE_(.+?)\s*\.\s*"#';
	preg_match_all($constants_regex, $string, $a);
	$constants = array_unique($a[1]);
	
	foreach ( $constants as $constant ) {
		$string = preg_replace('#"\s*\.\s*NGE_' . $constant . '\s*\.\s*"#', '%sys_' . strtolower($constant) . '%', $string);
	}
	
	// all NGE_ constants in the start of string
	$a = array();
	$constants_regex = '#NGE_(.+?)\s*\.\s*"#';
	preg_match_all($constants_regex, $string, $a);
	$constants = array_unique($a[1]);
	
	foreach ( $constants as $constant ) {
		$string = preg_replace('#NGE_' . $constant . '\s*\.\s*"#', '"%sys_' . strtolower($constant) . '%', $string);
	}
	
	// all NGE_ constants in the end of string
	$a = array();
	$constants_regex = '#"\s*\.\s*NGE_(.+?)\s*;#';
	preg_match_all($constants_regex, $string, $a);
	$constants = array_unique($a[1]);
	
	foreach ( $constants as $constant ) {
		$string = preg_replace('#"\s*\.\s*NGE_' . $constant . '\s*;#', '%sys_' . strtolower($constant) . '%";', $string);
	}
	
	// $QUERY_STRING
	$regex = '#"\s*\.\s*\$QUERY_STRING\s*\.\s*"#';
	$string = preg_replace($regex, '', $string);
	
	// $QUERY_STRING embeded
	$regex = '#\$QUERY_STRING#';
	$string = preg_replace($regex, '', $string);
	
	// current country title
	$regex = '#"\.\$LNG\[\'country_\'.MAIN_COUNTRY\]\."#';
	$string = preg_replace($regex, '%sys_app_country_title%', $string);
	
	
	return $string;
}

foreach ( $LANGS as $lang ) {
	$contents = file_get_contents($lang . '.php');
	file_put_contents($lang . '_converted.php', convert($contents));
}
