<?php 
include(NGE_LNG_DIR . "/cities_gr.php"); 
include(NGE_LNG_DIR . "/city_zones.php"); 
include(NGE_LNG_DIR . "/countries_gr.php"); 
include(NGE_LNG_DIR . "/languages_gr.php"); 
include(NGE_LNG_DIR . "/nationalities_gr.php"); 
include(NGE_LNG_DIR . "/regions_gr.php"); 
//CUBIX Added By David 
$LNG['email_exists'] = "Email exists"; 
$LNG['width_comments'] = "Width Comments"; 
$LNG['100_fake_free_review'] = "100% έγκυρες κριτικές"; 
$LNG['100_fake_free_review_desc'] = "100% έγκυρη κριτική είναι εάν το πρακτορείο/κοπέλα επιβεβαιωθεί με sms ότι η συνάντηση ισχύει, και οτι οι δύο πλευρές έχουν συμφωνήσει 100%!"; 
$LNG['24_7'] = "24/7"; 
$LNG['PROD_additional_city'] = "Επιπρόσθετη πόλη"; 
$LNG['PROD_city_premium_spot'] = "Πόλη premium χώρος"; 
$LNG['PROD_girl_of_the_month'] = "Κορίτσι του μήνα"; 
$LNG['PROD_international_directory'] = "Παγκόσμιος κατάλογος"; 
$LNG['PROD_main_premium_spot'] = "Κύριος premium χώρος"; 
$LNG['PROD_national_listing'] = "Εθνική λίστα"; 
$LNG['PROD_no_reviews'] = "Όχι κριτικές"; 
$LNG['PROD_search'] = "Αναζήτηση"; 
$LNG['PROD_tour_ability'] = "Ικανότητα τούρ"; 
$LNG['PROD_tour_premium_spot'] = "&quot;On tour&quot; premium spot"; 
$LNG['_all'] = "Όλα"; 
$LNG['about_me'] = "Λίγα λόγια για εμένα"; 
$LNG['about_meeting'] = "Για την συνάντηση"; 
$LNG['account_activated'] = "Ο λογαριασμός ενεργοποιήθηκε επιτυχώς, παρακαλώ συνδεθείτε τώρα και προσθέστε το προφίλ σας/συνοδούς. <a href='".NGE_URL."/login.php$QUERY_STRING'>Κάντε κλίκ εδώ</a> να συνδεθείτε."; 
$LNG['account_blocked_desc'] = "<p>Ο λογαριασμό σας είναι σε αναστολή για τους παρακάτω λόγους:</p> 
<ul> 
<li>προσβλητικός χρήστης ή συνοδός</li> 
<li>διακρίσεις απο χρήστες ή συνοδούς</li> 
<li>δημιουργία ψεύτικων αξιολογίσεων και θεμάτων</li> 
<li>παρακίνηση απο χρήστες εναντίον άλλων χρηστών ή κάτι άλλου</li> 
<li>δημιουργεία καταφανής διαφήμισης άλλου πρακτορείου ή συνοδού</li> 
</ul> 
<p>Εάν θέλετε να στείλετε τα σχόλια σας ή την δήλωση σας παρακαλούμε στείλτε e-mail στο: <a href='mailto:".NGE_CONTACT_EMAIL."'>".NGE_CONTACT_EMAIL."</a></p> 
<p>ΣΗΜΕΙΩΣΗ: παρακαλούμε μην εγγραφείτε ξανά και γράψετε, θα σβήσουμε τα σχόλια και τις αξιολογίσεις σας και θα μπλοκάρουμε το IP σας.</p>"; 
$LNG['account_not_verified'] = "Ο λογαριασμός δεν έχει εξακριβωθεί."; 
$LNG['account_type'] = "Είδος λογαριασμού"; 
$LNG['active'] = "Ενεργός"; 
$LNG['add'] = "Προσθήκη"; 
$LNG['add_escort'] = "Προσθέσετε προφίλ συνοδού"; 
$LNG['add_escort_profile'] = "Πρόσθεσε προφίλ συνοδού"; 
$LNG['add_profile_now'] = "Πρόσθεσε το προφίλ σου με φωτογραφία ΤΩΡΑ"; 
$LNG['add_review'] = "Πρόσθεσε κριτική"; 
$LNG['add_third_party_girl'] = "Προσθέστε ανεξάρτητη κοπέλα στα δεδομένα"; 
$LNG['add_watch'] = "Πρόσθεση στα αγαπημένα"; 
$LNG['add_watch_success'] = "<b>%NAME%</b> προστέθηκε επιτυχώς στα αγαπημένα. <a href='javascript:history.back()'>Κάντε κλίκ εδώ να πάτε πίσω.</a>"; 
$LNG['additional'] = "Επιπλέον"; 
$LNG['address'] = "Οδός"; 
$LNG['age'] = "Ηλικία"; 
$LNG['age_between'] = "Ηλικία ανάμεσα"; 
$LNG['agencies_match'] = "Κριτήρια που ταιριάζουν με το πρακτορείο"; 
$LNG['agency'] = "Πρακτορείο"; 
$LNG['agency_city_tour_not_allowed'] = "Μόνο κορίτσια ΕΚΤΟΣ δοκιμαστικής περίοδου είναι στην λίστα. Ταξίδια δεν επιτρέπονται για τα κορίτσια που είναι σε δοκιμαστική περίοδο. Παρακαλούμε αγοράστε ένα Premium χώρο, να ενεργοποιήσετε τη λειτουργία για αυτές. Click <a href='mailto:".NGE_CONTACT_EMAIL."'>εδώ για παραγγελία</a>."; 
$LNG['agency_data'] = "Στοιχεία πρακτορείου"; 
$LNG['agency_denied_review'] = "άρνηθηκε"; 
$LNG['agency_denied_review_desc'] = "Το πρακτορείο είπε οτι δεν υπάρχει στο site πελάτης με αυτά τα δεδομένα, αυτή είναι μια ψεύτικη αξιολόγιση και δεν θα πρέπει να εμπισεύεσαι την αξιολόγιση γιατί το πρακτορείο/κοπέλα δεν γνωρίζει έαν η αξιολόγιση είναι καλή ή άσχημη με την επιβεβαιώση!"; 
$LNG['agency_girls'] = "Κορίτσια πρακτορείου"; 
$LNG['agency_name'] = "Πρακτορείο εταιρείας"; 
$LNG['agency_profile'] = "Προφίλ πρακτορείου"; 
$LNG['agency_without_name'] = "Πρακτορείο χωρίς όνομα"; 
$LNG['ajaccio'] = "Ajaccio"; 
$LNG['all'] = "Όλα..."; 
$LNG['all_cities'] = "Όλες οι πόλεις..."; 
$LNG['all_countries'] = "Όλες οι χώρες..."; 
$LNG['all_escorts'] = "Όλοι οι συνοδοί"; 
$LNG['all_origins'] = "Όλες οι προελεύσεις..."; 
$LNG['all_reviews'] = "Όλες οι κριτικές"; 
$LNG['already_registered_on_another_site'] = "Εάν είστε ήδη μέλος στο %SITE%, δεν χρειάζεται να ξανακάνετε εγγραφή. Απλώς συνδεθείτε στο %SITE% με το όνομα χρήστη και κωδικό."; 
$LNG['already_reviewed'] = "Έχετε ήδη αξιολογίσει την κοπέλα! Μόνο 1 αξιολόγιση για κάθε κοπέλα είναι δυνατόν, παρακαλούμε ανανεώστε <a href='%LINK%'>την υπάρχον αξιολόγιση</a> εάν θέλετε!"; 
$LNG['already_voted'] = "Έχεις ήδη ψηφήσει το κορίτσι αυτό."; 
$LNG['also_provided_services'] = "Επίσης προσφέρει τις εξής υπηρεσίες"; 
$LNG['anal'] = "Πρωκτικό"; 
$LNG['are_you_escort'] = "Είστε ανεξάρτητη συνοδός, πρακτορείο ή ιδιωτική εταιρεία?"; 
$LNG['at_her_apartment'] = "Στον χώρο της"; 
$LNG['at_her_hotel'] = "Στο ξενοδοχείο της"; 
$LNG['back_to_actual_tours'] = "Πίσω στα ενεργά tours"; 
$LNG['at_my_flat_or_house'] = "Στον χώρο μου"; 
$LNG['at_my_hotel'] = "Στο ξενοδοχείο μου"; 
$LNG['attention'] = "Προσοχή !"; 
$LNG['attitude'] = "Συμπεριφορά"; 
$LNG['availability'] = "Διαθεσιμότητα"; 
$LNG['available_for'] = "Διαθέσιμο για"; 
$LNG['basic'] = "Βασικό"; 
$LNG['become_premium_member_and_view_private_photos'] = "<a href='%LINK%'>Γίνε premium μέλος ΤΩΡΑ</a> και δες σπέσιαλ φωτογραφίες χωρίς λογοκρισία των κοριτσιών!"; 
$LNG['bio'] = "Βιογραφικό"; 
$LNG['birth_date'] = "Ημερομηνία γέννησης"; 
$LNG['black'] = "Μαύρα"; 
$LNG['blond'] = "Ξανθά"; 
$LNG['blowjob'] = "Πεολειχία"; 
$LNG['blowjob_with_condom'] = " Πεολειχία με"; 
$LNG['blowjob_without_condom'] = " Πεολειχία χωρίς"; 
$LNG['blue'] = "Μπλέ"; 
$LNG['bombshellf'] = "υπέροχο"; 
$LNG['book_email_to_escort'] = "<pre> 
Κύριε %ESCORT%, 
   ένα Premium μέλος απο ".NGE_SITE_TITLE." θα ήθελε να σας συναντήσει %DATE%. 
Αυτός ο χρήστης κατοχύρωσε την έκπτωση του %DISCOUNT%%, που προσφέρουμε στα έμπιστα μέλη μας. 
Λεπτομέριες: 
Όνομα χρήστη: %USERNAME% 
Ολόκληρο όνομα: %FULLNAME% 
Επιθυμητή ημερομηνία: %DATE% 
Διάρκεια: %DURATION% 
Τηλέφωνο: %PHONE% 
Email: %EMAIL% 
Σχόλια: %COMMENTS% 
Security code: %CODE% 
Ο κωδικός αυτός είναι για την εξακρίβωση του χρήστη. Ο χρήστης απαιτείτε να δώσει αυτό τον κωδικό όταν ζητηθεί, 
εάν χρειάζεται να εξακριβώσεις το δικαίωμα του για έκπτωση. 
Best Regards, 
<a href=\"".NGE_URL."\">".NGE_SITE_TITLE."</a> 
</pre>"; 
$LNG['book_this_girl'] = "Κάντε book αυτό το κορίτσι"; 
$LNG['booking_desc'] = "Σας ευχαριστούμε για το ενδιαφέρον σας να κάνετε book %NAME%. Σαν Premium μέλος έχετε έκπτωση %DISCOUNT%% για το book σε αυτό το κορίτσι. Παρακαλούμε χρησιμοποιείστε την παρακάτω φόρμα για να κάνετε book αυτό το κορίτσι και να κερδίσετε την έκπτωση!"; 
$LNG['booking_mail_subject'] = "%SITE% booking"; 
$LNG['booking_mail_text'] = "<pre> 
Κύριε %NAME%, 
κάνατε επιτυχημένο book %ESCORT_NAME% για την ημερομηνία %DATE%. 
Το κορίτσι σύντομα θα έρθει σε επαφή μαζί σας για τις λεπτομέριες του ραντεβού. 
Ώς Premium μέλος στο %SITE%, έχετε το δικαίωμα να κατοχυρώσετε %DISCOUNT%% την έκπτωση στην τιμή της κοπέλας. 
Για οτιδήποτε απορίες, παρακαλούμε αποθηκεύστε αυτό το ασφαλές id: %VERIFICATION_ID% 
Αυτό το ID μπορεί να χρησιμοποιηθεί για να εξακριβωθεί η συνδρομή σας ώς Premium μέλος και να πάρετε την έκπτωση. 
Σας ευχόμαστε καλή διασκέδαση με το κορίτσι της επιλογής σας! 
%SITE% Ομάδα</pre>"; 
$LNG['boys'] = "Αγόρια"; 
$LNG['boys_trans'] = "Αγόρι/τράνς"; 
$LNG['breast'] = "Στήθος"; 
$LNG['breast_size'] = "Νούμερο στήθους"; 
$LNG['brown'] = "Καστανά"; 
$LNG['bruntal'] = "BruntΓ΅l"; 
$LNG['bust'] = "Στήθος"; 
$LNG['by'] = "από"; 
$LNG['calabria'] = "Calabria"; 
$LNG['can_have_max_cities'] = "Μπορείτε μέχρι %NUMBER% πόλεις να ορίσετε"; 
$LNG['cancel'] = "Άκυρο"; 
$LNG['cant_be_empty'] = "Δεν πρέπει να είναι κενό"; 
$LNG['cant_go_ontour_to_homecity'] = "Escort`s can`t go on city your to their home city."; 
$LNG['castelfranco'] = "Castelfranco"; 
$LNG['chalons-en-champagne'] = "Chalons-en-Champagne"; 
$LNG['change_all_escorts'] = "Αίτηση αλλαγής σε όλα τα προφίλ συνοδών"; 
$LNG['change_passwd'] = "Αλλαγή κωδικού"; 
$LNG['char_desc'] = "tatoo κτλ."; 
$LNG['characteristics'] = "Χαρακτηριστικά"; 
$LNG['chiavari'] = "Chiavari"; 
$LNG['choose'] = "Επιλογή..."; 
$LNG['choose_availability'] = "Choose availability..."; 
$LNG['choose_another_city'] = "Διάλεξε άλλη πόλη"; 
$LNG['choose_another_region'] = "Choose another region"; 
$LNG['choose_city'] = "Διάλεξε πόλη..."; 
$LNG['choose_country'] = "Επιλέξτε χώρα..."; 
$LNG['choose_currency'] = "Επέλεξε νόμισμα..."; 
$LNG['choose_escort'] = "Επέλεξε συνοδό..."; 
$LNG['choose_escort_to_promote'] = "Δίαλεξε συνοδό να διαφημίσεις"; 
$LNG['choose_language'] = "Διάλεξε γλώσσα..."; 
$LNG['choose_level'] = "Επέλεξε επίπεδο..."; 
$LNG['choose_ethnic'] = "Choose ethnic..."; 
$LNG['choose_nationality'] = "Επέλεξε εθνικότητα..."; 
$LNG['choose_state'] = "Επέλεξε περιοχή..."; 
$LNG['choose_this_user'] = "Επέλεξε αυτόν τον χρήστη"; 
$LNG['choose_time'] = "Επέλεξε ώρα..."; 
$LNG['choose_your_city'] = "Επιλέξτε την πόλη σας"; 
$LNG['choose_your_country'] = "Επέλεξε την χώρα σου"; 
$LNG['choosen_escort'] = "Συνοδός επιλογής: %NAME%"; 
$LNG['cities'] = "Πόλεις"; 
$LNG['city'] = "Πόλη"; 
$LNG['city_escort_notify'] = "Πληροφορίες στο e-mail εάν συνοδός απο την πόλη μου προστέθηκε"; 
$LNG['city_of_meeting'] = "Πόλη συνάντησης"; 
$LNG['claim_your_discount'] = "Αξίωσε την έκπτωση ΤΩΡΑ & κάνε book σε αυτό το κορίτσι"; 
$LNG['comments'] = "Σχόλια"; 
$LNG['comments_to_her_services'] = "Σχόλια για τις υπηρεσίες της"; 
$LNG['comments_to_her_services_desc'] = "Περιγράψτε τις υπηρεσίες της, παράδειγμα ειδικές υπηρεσίες (S/M, παιχνίδια, etc)."; 
$LNG['company_name'] = "Πρακτορείο εταιρείας"; 
$LNG['conf_password'] = "Επιβεβαίωση κωδικού"; 
$LNG['confirmation_email_1'] = "Κύριε %USER%, ευχαριστούμε για την εγγραφή σας στο %SITE%. Παρακάτω είναι οι πληροφορίες για την σύνδεση σας:"; 
$LNG['confirmation_email_2'] = "Παρακαλούμε κρατήστε τις πληροφορίες αυτές σε ασφαλές μέρος."; 
$LNG['confirmation_email_subject'] = NGE_SITE_TITLE." εγγραφή"; 
$LNG['contact'] = "Επικοινωνία"; 
$LNG['contact_her'] = "Επικοινωνήστε με αυτό το κορίτσι απευθείας"; 
$LNG['contact_her_desc'] = "Επικοινωνείτε %NAME%. Αυτό το email θα έρθει απο %SITE% και θα εξακριβωθεί, να εκφράσουμε την εμπιστοσύνη σαν Premium μέλος του %SITE%"; 
$LNG['contact_her_mail_header'] = "<pre> 
************************************************************************************* 
Αυτό είναι ένα εξακριβωμένο email από %SITE% Premium χρήστη. 
Αυτός ο χρήστης είναι έμπιστο μέλος της κοινότητας μας και πληρώνει τακτικά τη συνδρομή μέλους. 
*************************************************************************************</pre>"; 
$LNG['contact_her_mail_subject'] = "Μήνυμα από %SITE% Premium χρήστη"; 
$LNG['contact_info'] = "Πληροφορίες επικοινωνίας"; 
$LNG['contact_text'] = " 
Ώς η μεγαλύτερη κοινότητα συνοδών στην ".$LNG['country_'.MAIN_COUNTRY]." προσφέρουμε χώρο για διαφήμιση στο site μας. Όριο στις διαφημίσεις μας έχουμε τα 10 sites - που αυτό σημαίνει πολύ καλό click rate για κάθε banner.<BR /> 
<BR /> 
Μερικά στατιστικά:<BR /> 
<BR /> 
Hits -> 3 εκατομμύρια και πλέον σε καθημερινή βάση<BR /> 
Unique visitors -> 25,000 και πλέον καθημερινά<BR /> 
Members -> κοντά στα 15,000, με καθημερινή προσθήκη<BR /> 
<BR /> 
Εάν θέλετε να αρπάξετε την ευκαιρία τώρα και να συζητήσουμε τις λεπτομέριες, απλώς επικοινωνείστε μαζί μας. Θα σας απαντήσουμε το συντομότερο<BR /> 
<BR /> 
<a href='mailto:".NGE_CONTACT_EMAIL."'>".NGE_CONTACT_EMAIL."</a> 
Τήλ.: 0036 1203 1808 (10:00 - 20:00) 
</pre> 
"; 
$LNG['contact_us'] = "Επικοινωνία / Διαφήμιση"; 
$LNG['continue'] = "Συνέχεια"; 
$LNG['conversation'] = "Συζήτηση"; 
$LNG['countries'] = "Χώρες"; 
$LNG['country'] = "Χώρα"; 
$LNG['country_tw'] = "Taiwan"; 
$LNG['couples'] = "Ζευγάρια"; 
$LNG['created'] = "Δημιουργήθηκε"; 
$LNG['cumshot'] = "Χύσιμο"; 
$LNG['cumshot_on_body'] = "Τελείωμα στο σώμα"; 
$LNG['cumshot_on_face'] = "Τελείωμα στο πρόσωπο"; 
$LNG['curr_password'] = "Υπάρχον κωδικός"; 
$LNG['curr_password_empty_err'] = "Θα πρέπει να βάλετε τον ήδη υπάρχον κωδικό"; 
$LNG['currency'] = "Νόμισμα"; 
$LNG['currently_listed_sites'] = "Ήδη έχουμε στην λίστα μας sites για να διαλέξετε. Παρακαλώ διαλέξτε site."; 
$LNG['date'] = "Ημερομηνία"; 
$LNG['day'] = "μέρα"; 
$LNG['day_of_meeting'] = "Ημερομηνία συνάντησης"; 
$LNG['day_phone'] = "Τηλέφωνο"; 
$LNG['days'] = "μέρες"; 
$LNG['default_city'] = "Να φαίνετε επιλεγμένη πόλη με τη σύνδεση"; 
$LNG['delete_rates'] = "Διαγράψτε όλες τις εκτιμήσεις"; 
$LNG['delete_sel_pcis'] = "Σβήσε επιλεγμένες φωτογραφίες"; 
$LNG['delete_selected_reviews'] = "Διαγράψτε επιλεγμένες αξιολογίσεις"; 
$LNG['delete_selected_tour'] = "Διαγραφή επιλεγμένου τούρ"; 
$LNG['desired_date'] = "Επιθυμιτή ημερομηνία"; 
$LNG['discount'] = "Έκπτωση"; 
$LNG['discount_code'] = "Κωδικός έκπτωσης"; 
$LNG['discount_code_txt'] = "Για να κατοχυρόσετε το 5% έκπτωσης, παρακαλούμε συμπληρώστε τον κωδικό έκπτωσης, εάν έχετε απο τους συνεργάτες μας."; 
$LNG['discount_desc'] = "Σε αυτή τη σελίδα θα δείτε όλα τα κορίτσια που σας προσφέρουμε <b>έκπτωση</b> στα Premium μέλη μας."; 
$LNG['discounted_girls'] = "Προσφορές"; 
$LNG['dont_forget_variable_symbol'] = " 
    <p>Παρακαλούμε ΜΗΝ ΞΕΧΑΣΕΤΕ να συμπεριλάβετε το μεταβλητό σας σύμβολο: <big>%ESCORT_ID%</big>.<br /> 
    Χωρίς αυτό δεν μπορούμε να εξακριβώσουμε την πληρωμή σας.<br /> 
    Σας ευχαριστούμε.</p> 
"; 
$LNG['dont_know'] = "Δεν γνωρίζω"; 
$LNG['dress_size'] = "Νούμερο ενδύματος"; 
$LNG['duration'] = "Διάρκεια"; 
$LNG['easy_to_get_appointment'] = "Εύκολο ραντεβού"; 
$LNG['edit'] = "Επεξεργασία"; 
$LNG['edit_escort'] = "Επεξεργαστείτε προφίλ συνοδού"; 
$LNG['edit_gallery'] = "Επεξεργασία γκαλερί"; 
$LNG['edit_languages'] = "Επεξεργασία γλώσσας"; 
$LNG['edit_locations'] = "Επεξεργασία περιοχής"; 
$LNG['edit_private_photos'] = "Επεξεργασία προσωπικών φωτογραφιών"; 
$LNG['edit_private_photos_desc'] = "Παρακαλώ ανεβάστε φωτογραφίες που δεν κρύβουν κάποιο σημείο του σώματος ή γκλάμουρ (σπέσιαλ φωτογραφίες, που δεν θα ήθελες να είναι εμφνανείς σε όλους) φωτογραφίες, που μόνο οι Premium χρήστες θα μπορούν να δούν. Με αυτό τον τρόπο εξασφαλίζουμε, οτι τις πολύ προσωπικές φωτογραφίες, θα μπορουν να τις δούν μόνο οι high class πελάτες μας, που έχουν εκφράσει το ενδιαφέρον τους πληρώνοντας τη συνδρομή μέλους."; 
$LNG['edit_profile'] = "Ανανέωση προφίλ"; 
$LNG['edit_rates'] = "Επεξεργασία εκτίμησης"; 
$LNG['edit_review'] = "Ανανέωσε αξιολόγιση"; 
$LNG['edit_settings'] = "Ανανέωση ρυθμίσεων"; 
$LNG['edit_units_in'] = "Επεξεργασία μονάδων"; 
$LNG['email'] = "E-mail"; 
$LNG['email_domain_blocked_err'] = "Λυπούμαστε %DOMAIN% δεν δέχεται emails απο εμάς. Παρακαλούμε επιλέξτε κάποια άλλη διεύθυνση email ή επικοινωνίστε %DOMAIN% να δεχτεί την απάντηση μας."; 
$LNG['email_lng'] = "Γλώσσα που χρησιμοποιείτε στα emails"; 
$LNG['email_not_registered'] = "Αυτό το email δεν έχει εγγραφτεί στο site μας."; 
$LNG['empty_login_err'] = "Άδειο πεδίο σύνδεσης."; 
$LNG['empty_passwd_err'] = "Άδειο πεδίο κωδικού."; 
$LNG['empty_watchlist'] = "Η λίστα με τα αγαπημένα είναι κενή"; 
$LNG['english'] = "Αγγλικά"; 
$LNG['enthusiastic'] = "Ενθουσιαστικό"; 
$LNG['error'] = "Λάθος"; 
$LNG['error_adding_watch'] = "Υπήρξε πρόβλημα κατά την προσθήκη/διαγραφή αντικειμένου απο την λίστα των αγαπημένων σας. Παρακαλούμε επικοινωνίστε με τον διαχειριστή του site."; 
$LNG['esc_mb_chng_subject'] = "Συνδρομή άλλαξε σε".NGE_SITE_TITLE; 
$LNG['esc_mb_chng_to_free_email'] = "<pre>Κύριε %LOGIN%, 
   η συνδρομή στο site μας με όνομα προφίλ %SHOWNAME% έχει αλλάξει σαν \"δωρεάν\". 
Έχετε χάσει τα προνόμοια απο πληρωμένη συνδρομή. 
Αναβάθμισε την συνδρομή σου τώρα και κέρδισε τα προνόμοια: 
- να πάρετε μέρος στις εναλλαγές του προφίλ σας στην κεντρική σελίδα, 
αυτό σημαίνει οτι ποτέ δεν θα είσαι στην τελευταία σελίδα 
που σημαίνει οτι κανένας δεν θα σε δεί. Όλα τα πληρωμένα προφίλ αλλάζουν κάθε μία ώρα, 
έτσι κάθε προφίλ έχει τις ίδιες ευκαιρίες να εμφανιστεί στην πρώτη σελίδα 
της πόλης ή της κεντρικής σελίδας 
- θα μπορείς να πάρεις μέρος στο \"Κορίτσι του μήνα\" 
  διαγωνισμό 
- θα έχεις την ευκαιρία να είσαι μέσα στο TOP10 
  των αξιλογιμένων κοριτσιών 
Για να αναβαθμίσετε την συνδρομή μπορείτε να απαντήσετε σε αυτό το email ή 
να συνδεθείτε στο site και να κάνετε κλίκ στο \"Αναβάθμιση τώρα\". 
".NGE_SITE_TITLE." Ομάδα 
</pre> 
"; 
$LNG['esc_mb_chng_to_normal_email'] = "<pre>Κύριε %LOGIN%, 
   η συνδρομή στο site μας με όνομα προφίλ %SHOWNAME% αναβαθμίστηκε σε \"normal\". 
Ημερομηνία λήξης: %EXPIRE% 
Απο τώρα μπορείτε να απολαμβάνετε τα παρακάτω προνόμοια: 
- να πάρετε μέρος στις εναλλαγές του προφίλ σας στην κεντρική σελίδα, 
αυτό σημαίνει οτι ποτέ δεν θα είσαι στην τελευταία σελίδα 
που σημαίνει οτι κανένας δεν θα σε δεί. Όλα τα πληρωμένα προφίλ αλλάζουν κάθε μία ώρα, 
έτσι κάθε προφίλ έχει τις ίδιες ευκαιρίες να εμφανιστεί στην πρώτη σελίδα 
της πόλης ή της κεντρικής σελίδας 
- θα μπορείς να πάρεις μέρος στο \"Κορίτσι του μήνα\" 
  διαγωνισμό 
- θα έχεις την ευκαιρία να είσαι μέσα στο TOP10 
  των αξιλογιμένων κοριτσιών 
".NGE_SITE_TITLE." Ομάδα 
</pre> 
"; 
$LNG['esc_mb_chng_to_premium_email'] = "<pre>Κύριε %LOGIN%, 
   η συνδρομή στο site μας με όνομα προφίλ %SHOWNAME% έχει αναβαθμιστεί σε \"premium\". 
Νέα ημερομηνία λήξης: %EXPIRE% 
Απο τώρα έχετε τα παρακάτω προνόμοια: 
- ΥΠΟ ΚΑΤΑΣΚΕΥΗ 
".NGE_SITE_TITLE." Ομάδα 
</pre> 
"; 
$LNG['esc_mb_chng_to_trial_email'] = "<pre>Κύριε %LOGIN%, 
   η συνδρομή στο site μας με όνομα προφίλ %SHOWNAME% έχει αλλάξει σε \"δοκιμαστική\". 
Νέα ημερομηνία λήξης: %EXPIRE% 
Είστε συνδρομητής στην δοκιμαστική περίοδο 
στο ".NGE_SITE_TITLE.". Σε αυτή την περίοδο, θα 
λάβετε όλα τα προνόμοια ώς πληρωμένη συνδρομή, 
που περιλαμβάνει: 
- να πάρετε μέρος στις εναλλαγές του προφίλ σας στην κεντρική σελίδα, 
αυτό σημαίνει οτι ποτέ δεν θα είσαι στην τελευταία σελίδα 
που σημαίνει οτι κανένας δεν θα σε δεί. Όλα τα πληρωμένα προφίλ αλλάζουν κάθε μία ώρα, 
έτσι κάθε προφίλ έχει τις ίδιες ευκαιρίες να εμφανιστεί στην πρώτη σελίδα 
της πόλης ή της κεντρικής σελίδας 
- θα μπορείς να πάρεις μέρος στο \"Κορίτσι του μήνα\" 
  διαγωνισμό 
- θα έχεις την ευκαιρία να είσαι μέσα στο TOP10 
  των αξιλογιμένων κοριτσιών 
Μετά την δοκιμαστική συνδρομή, θα πρέπει να αναβαθμίσετε 
σε πληρωμένη συνδρομή, αλλιώς θα χάσετε όλα τα χαρακτηριστικά και 
το προφίλ σου θα εμφανιστεί στην τελευταία σελίδα. 
Για να αναβαθμήσεις την συνδρομή θα πρέπει να απαντήσεις σε αυτό το email ή 
να συνδεθείς στο site και να κάνεις κλίκ στο \"Αναβάθμιση τώρα\". 
".NGE_SITE_TITLE." Ομάδα 
</pre> 
"; 
$LNG['esc_mb_chng_to_vip_email'] = "<pre>Κύριε %LOGIN%, 
   η συνδρομή στο site μας με όνομα προφίλ %SHOWNAME% έχει αλλάξει σαν \"VIP\". 
Αυτό το προφίλ δεν λήγει και μοιράζεται τα ίδια προνόμοια με πληρωμένη συνδρομή. 
".NGE_SITE_TITLE." Ομάδα 
</pre> 
"; 
$LNG['esc_mb_memb_expired_email'] = "<pre>Κύριε %LOGIN%, 
   η συνδρομή στο site μας με όνομα προφίλ %SHOWNAME% έληξε. 
Έχετε χάσει τα προνόμοια, και ο λογαριασμός σας έχει απενεργοποιηθεί. 
Εάν επιθυμείτε να ανανεώσετε τη συνδρομή σας, παρακαλούμε επικοινωνίστε με εμάς στο ".NGE_CONTACT_EMAIL.". 
".NGE_SITE_TITLE." Ομάδα 
</pre> 
"; 
$LNG['esc_mb_memb_expired_email_subj'] = "Η συνδρομή έληξε"; 
$LNG['esc_mb_memb_expires_soon_email'] = "<pre>Κύριε %LOGIN%, 
   η συνδρομή στο site μας με όνομα προφίλ %SHOWNAME% λήγει σέ %DAYS% μέρες, 
παρακαλούμε μην ξεχάσετε να το ανανεώσετε. Για να το κάνετε επικοινωνίστε με εμάς στο ".NGE_CONTACT_EMAIL.". 
".NGE_SITE_TITLE." Ομάδα 
</pre> 
"; 
$LNG['esc_mb_memb_expires_soon_email_subj'] = "Η συνδρομή λήγει σύντομα"; 
$LNG['esc_to_upg'] = "Προφίλ συνοδού να αναβαθμιστεί"; 
$LNG['esc_upg_promo'] = " 
<ul> 
<li>εναλλαγή με την προβολή σου στην κεντρική σελίδα, 
που σημαίνει δεν θα είσαι στην τελευταία σελίδα που την βλέπουν λιγότεροι χρήστες. Όλα τα πληρωμένα προφίλ αλλάζουν ανά ώρα, 
έτσι το κάθε προφίλ έχει τις ίδιες ευκαιρίες για εμφάνιση στην πρώτη σελίδα την πόλης ή στην κεντρική σελίδα του site</li> 
<li>θα μπορείς να πάρεις μέρος στο \"Κορίτσι του μήνα\" 
  διαγωνισμό</li> 
<li>θα έχεις ευκαιρία να συμπεριληφθείς στο TOP10 
  των αξιολογούμενων κοριτσιών</li> 
</ul>"; 
$LNG['esc_upg_promo_txt'] = "<a href='/private/upgrade_escort.php$QUERY_STRING'>Αναβάθμισε ΤΩΡΑ</a>"; 
$LNG['esc_upgrade_payment_details'] = " 
    Ευχαριστούμε για το ενδιαφέρον να γίνεται συνδρομητής στο ".NGE_SITE_TITLE.".<br /> 
    Παρακαλούμε μεταφέρετε %AMOUNT% EUR στον λογαριασμό μας και το προφίλ σας θα ανανεωθεί σύντομα μετά την πληρωμή.<br /> 
    <p><strong>Στοιχεία τράπεζας:</strong><br /> 
    SMD MEDIA:<br /> 
    Conto corrente n. 6152293433/25<br /> 
    IBAN: IT42 P030 6910 9106 1522 9343 325<br /> 
    SWIFT: BCITIT333P0<br /> 
    Banca Intesa - filiale 2390<br /> 
    Via G. Rubini n. 6, 22100 Como</p> 
    Για προβλήματα ή απορίες, παρακαλούμε μην διστάσετε να επικοινωνίσετε με εμάς στο <a href='mailto:".NGE_CONTACT_EMAIL."'>".NGE_CONTACT_EMAIL."</a>. 
"; 
$LNG['escort'] = "Συνοδός"; 
$LNG['escort_active'] = "Το προφίλ σας είναι ενεργό. <a href='%LINK%'>Κάντε κλίκ εδώ</a> για απενεργοποίηση."; 
$LNG['escort_added_continue_review'] = "Συνοδός προστέθηκε επιτυχώς. <a href='%LINK%'>Κάντε κλίκ εδώ</a> να συνεχίσετε τις αξιολογίσεις."; 
$LNG['escort_city_tour_not_allowed'] = "Δεν επιτρέπεται να κάνετε ταξίδι την δοκιμαστική περίοδο. Παρακαλούμε αγοράστε ένα Premium χώρο, να ενεργοποιήσετε τη λειτουργία αυτή. Κάντε κλίκ <a href='mailto:".NGE_CONTACT_EMAIL."'>εδώ για παραγγελία</a>."; 
$LNG['escort_data_form_text'] = "Συμπληρώστε με πληροφορίες που θα φαίνονται στο site."; 
$LNG['escort_inactive'] = "Συνοδός δεν είναι ενεργός!"; 
$LNG['escort_modification_disabled'] = "Αυτή τη στιγμή κάνουμε προγραματισμένη συντήρηση. Δεν είναι δυνατόν αλλαγές στους συνοδούς. Παρακαλούμε ελάτε αργότερα. Σας ζητάμε συγνώμη για την αναστάτωση."; 
$LNG['escort_name_active'] = "Συνοδός %ESCORT%'s προφίλ είναι ενεργό. <a href='%LINK%'>Κάντε κλίκ εδώ</a> για απενεργοποίηση."; 
$LNG['escort_name_not_active'] = "Συνοδός %ESCORT%'s προφίλ δεν είναι ενεργό. <a href='%LINK%'>Κάντε κλίκ εδώ</a> να ενεργοποιηθεί."; 
$LNG['escort_name_waiting_for_approval'] = "Συνοδός %ESCORT%'s προφίλ περιμένει έκγριση απο τον διαχειριστή του site."; 
$LNG['escort_not_active'] = "Το προφίλ σας δεν είναι ενεργό. <a href='%LINK%'>Κάντε κλίκ εδώ</a> να ενεργοποιηθεί."; 
$LNG['escort_not_found_use_search'] = "Συνοδός %NAME% δεν αναγνωρίστικε, παρακαλώ χρησιμοποιήστε την αναζήτηση να ορίσετε μία."; 
$LNG['escort_not_in_db_add_her'] = "Εάν συνοδός δεν είναι στα δεδομένα μας, παρακαλώ <a href='%LINK%'>προσθέστε μία τώρα</a>."; 
$LNG['escort_profile_added'] = "Προφίλ συνοδού έχει προστεθεί στα δεδομένα επιτυχώς !<BR /> Παρακαλώ 
<a href='/private/edit_gallery.php$QUERY_STRING'>ανεβάστε φωτογραφίες για το προφίλ σας.</a>"; 
$LNG['escort_profile_updated'] = "Προφίλ συνοδού έχει προστεθεί στα δεδομένα επιτυχώς ! <BR /> 
<a href='/private/index.php$QUERY_STRING'>Πήγαινε πίσω.</a>"; 
$LNG['escort_waiting_for_approval'] = "Περιμένετε να εγκριθεί το προφίλ σας απο τον διαχειριστή του site."; 
$LNG['escorts'] = "Συνοδοί"; 
$LNG['escorts_in_city'] = "Συνοδοί στη %CITY%"; 
$LNG['escorts_match'] = "Κριτήρια που ταιριάζουν με τη συνοδό"; 
$LNG['evening_phone'] = "Άλλο τηλέφωνο"; 
$LNG['expires'] = "Λήγει"; 
$LNG['eye_color'] = "Χρώμα ματιών"; 
$LNG['eyes'] = "Μάτια"; 
$LNG['fake_free_review_confirm_mail_01'] = " 
    <p>Hi %SHOWNAME%,<br /> 
    μέλος στο ".NGE_SITE_TITLE." αξιολογίθηκε στο site σου. Για να αποφύγουμε ψεύτικες κριτικές θα ζητηθεί να εξακριβωθεί εάν έχετε τον πελάτη αυτό και να επιβεβαιωθεί ή απορριφθούν οι πληροφορίες σε αυτό το email! Παρακαλούμε απλώς απαντήσε μάς με: ναί ή όχι, το ναί σημαίνει ότι ο πελάτης είναι απο εσάς!</p> 
    <p>Λεπτομέριες του πελάτη:</p> 
"; 
$LNG['fake_free_review_confirm_mail_02'] = " 
    <p>Απαντήστε παρακαλώ με: Ναί για αλήθεια, και Όχι για μη αλήθεια.</p> 
    <p>Δικοί σας ".NGE_SITE_TITLE." ομάδα</p> 
"; 
$LNG['fake_free_review_confirm_mail_subj'] = NGE_SITE_TITLE." βεβαίωση αληθινής αξιολόγισης"; 
$LNG['fake_photo'] = "Ψεύτικη φωτογραφία!"; 
$LNG['fax'] = "Fax"; 
$LNG['female'] = "Γυναίκα"; 
$LNG['ff_guests'] = "<div class='big center'>μόνο %DAYS% μέρες έμειναν μέχρι την κλήρωση !</div><a href='%LINK%' rel='nofollow'>Γράψου ΤΩΡΑ</a> ως Premium Μέλος - και κέρδισε την ευκαιρία για σέξ <a href='%LINK%' rel='nofollow'>με το αγαπημένο σου κορίτσι ΔΩΡΕΑΝ!</a> *"; 
$LNG['ff_guests_today'] = "<div class='big center'>ΣΗΜΕΡΑ</div>θα ανακοινώσουμε το νικητή της 'Δωρεάν Fuck Λοτταρίας'! Μήπως είσαι ΕΣΥ? Πάρε την ευκαιρία και <a href='%LINK%' rel='nofollow'>Γράψου ΤΩΡΑ</a> ώς Premium Μέλος - για να μπείς στην κλήρωση! *"; 
$LNG['ff_nonpaid'] = "<div class='big center'>μόνο %DAYS% μέρες έμειναν μέχρι την κλήρωση !</div><a href='%LINK%' rel='nofollow'>Αναβάθμισε τη συνδρομή σου ΤΩΡΑ</a> - και κέρδισε την ευκαιρία για σέξ <a href='%LINK%' rel='nofollow'>με το αγαπημένο σου κορίτσι ΔΩΡΕΑΝ!</a> *"; 
$LNG['ff_nonpaid_today'] = "<div class='big center'>ΣΗΜΕΡΑ</div>θα ανακοινώσουμε το νικητή της 'Δωρεάν Fuck Λοτταρίας'! Μήπως είσαι ΕΣΥ? Πάρε την ευκαιρία και <a href='%LINK%' rel='nofollow'>Αναβάθμισε τη συνδρομή σου ΤΩΡΑ</a> για να μπείς στην κλήρωση! *"; 
$LNG['ff_paid'] = " <div class='big center'>μόνο %DAYS% μέρες έμειναν μέχρι την κλήρωση !</div>"; 
$LNG['ff_paid_today'] = " <div class='big center'>ΣΗΜΕΡΑ</div>θα ανακοινώσουμε το νικητή της 'Δωρεάν Fuck Λοτταρίας'! Μήπως είσαι ΕΣΥ? "; 
$LNG['file_uploaded'] = "Η φωτογραφία ανέβηκε επιτυχώς."; 
$LNG['finish_reg'] = "Τέλος εγγραφής"; 
$LNG['first_name'] = "Όνομα"; 
$LNG['fluent'] = "Άνετο"; 
$LNG['forgot_email_1'] = "Κύριε %USER%, ζητήσατε τον κωδικό σας να σταλεί με το 'Ξέχασα κωδικό' φόρμα στο %SITE%."; 
$LNG['forgot_email_2'] = "Οι πληροφορίες σας σύνδεσης είναι:"; 
$LNG['forgot_email_subject'] = NGE_SITE_TITLE." αίτηση κωδικού"; 
$LNG['forgot_passwd'] = "Ξέχασες τον κωδικό σου?"; 
$LNG['forgot_password'] = "Ξέχασα κωδικό"; 
$LNG['free'] = "Δωρεάν"; 
$LNG['free_fuck_desc'] = "Στο τέλος κάθε μήνα, θα κληρώσουμε ένα τυχερό Premium μέλος, που θα κερδίσει δωρεάν sex με οποιαδήποτε κοπέλα επιλέξει".NGE_SITE_TITLE.". Θα πληρώσουμε τα έξοδα συνοδού εμείς για το νικητή!<BR /> <BR /> Συνγχαρητήρια σε όλους τους τυχερούς νικητές! Σας ευχόμαστε νά έχετε φανταστική διασκέδαση με το κορίτσι της επιλογής σας... !"; 
$LNG['free_fuck_lotto'] = "Δωρεάν Fuck Λοτταρία"; 
$LNG['free_fuck_note'] = "Σημείωση: ή αξία του είναι 300 ευρώ ή 1 ώρα book, εάν επιλέξετε κοπέλα που κοστίζει 250 ευρώ, δεν θα πλήρωθείτε τα υπόλοιπα 50 ευρώ, το δώρο αυτό δεν μπορεί να αλλαχτεί με χρήματα ή οτιδήποτε άλλο. Λεπτομέριες για την παραλαβή του δώρου θα σταλούν στο νικητή."; 
$LNG['free_mb_desc'] = "Το προφίλ σου εμφανίζεται κάτω στην τελαυταία σελίδα, με αποτέλεσμα μικρός αριθμός χρηστών 
να το βλέπει μόνο. Αναβάθμισε την συνδρομή σου τώρα και κέρδισε:"; 
$LNG['free_mb_desc_ag'] = "%COUNT% συνοδοί - <a href='/private/upgrade_escort.php$QUERY_STRING'>Αναβάθμιση τώρα</a>να ξέρουν οι πελάτες για αυτές!"; 
$LNG['friday'] = "Παρασκευή"; 
$LNG['friendly'] = "Φιλική"; 
$LNG['from'] = "Από"; 
$LNG['fuckometer'] = "Fuck-όμετρο"; 
$LNG['fuckometer_desc'] = "Το Fuck-όμετρο βαθμολογεί και δείχνει την συμπεριφορά της κοπέλας βάση των κριτικών. Υπάρχει για να δείχνει την απόδοση της κοπέλας στην πραγματικότητα."; 
$LNG['fuckometer_range'] = "Fuck-όμετρο"; 
$LNG['fuckometer_rating'] = "Βαθμολόγιση Fuck-όμετρου"; 
$LNG['fuckometer_short'] = "FM"; 
$LNG['fuckometer_toplist'] = "Fuck-όμετρο"; 
$LNG['full_name'] = "Ολόκληρο όνομα"; 
$LNG['gays'] = "Γκέυ"; 
$LNG['gender'] = "Φύλο"; 
$LNG['genuine_photo'] = "100% αυθεντική φωτογραφία"; 
$LNG['get_trusted_review'] = "Έμπιστη αξιολόγιση"; 
$LNG['get_trusted_review_desc'] = " 
    Δείτε 100% έμπιστη αξιολόγιση! Παρακαλώ συμπληρώστε τα κενά απο κάτω!<br /> 
    Πώς δουλεύει αυτό<a href='%LINK%'>κάντε κλίκ εδώ</a>!"; 
$LNG['girl'] = "Κορίτσι"; 
$LNG['girl_booked'] = "Έχετε κάνει book %NAME% για %DATE%."; 
$LNG['girl_no_longer_listed'] = "Το κορίτσι αυτό δεν είναι στην λίστα μας %SITE%"; 
$LNG['girl_of'] = "Κορίτσι του"; 
$LNG['girl_of_the_month_history'] = "Ιστορικό απο κορίτσια του μήνα"; 
$LNG['girl_on_tour_1'] = "Η κοπέλα είναι σε τούρ στην %CITY% - %COUNTRY%"; 
$LNG['girl_on_tour_2'] = "Από %FROM_DATE% έως %TO_DATE%"; 
$LNG['girl_on_tour_3'] = $LNG['tour_phone'].": %PHONE%"; 
$LNG['girl_on_tour_4'] = $LNG['tour_email'].": %EMAIL%"; 
$LNG['girl_origin'] = "Προέλευση κοπέλας"; 
$LNG['girls_in_italy'] = "Όλα τα κορίτσια που είναι στην λίστα είναι ΤΩΡΑ στην ".$LNG['country_'.MAIN_COUNTRY]."! Κάντε κράτηση ΤΩΡΑ!"; 
$LNG['girls_international'] = "Διαθέσιμα κορίτσια για ολόκληρο τον κόσμο, παρακαλούμε σημειώστε έαν θέλετε να τις έχετε στην ".$LNG['country_'.MAIN_COUNTRY]." πρέπει το λιγότερο να έχετε κάνει κράτηση 12ώρες!"; 
$LNG['go_to_escort_profile'] = "Πήγαινε στα προφίλ συνοδών"; 
$LNG['go_to_reviews_overview'] = "Πήγαινε σε όλες τις κριτικές"; 
$LNG['gray'] = "Γκρί"; 
$LNG['green'] = "Πράσινα"; 
$LNG['hair'] = "Μαλλιά"; 
$LNG['hair_color'] = "Χρώμα μαλλιών"; 
$LNG['hard_to_believe_its_her'] = "Δύσκολο να το πιστέψω οτι είναι αυτή"; 
$LNG['hard_to_book'] = "Δύσκολο να κάνεις κράτηση"; 
$LNG['height'] = "Ύψος"; 
$LNG['help_system'] = "Βοήθεια"; 
$LNG['help_watch_list'] = "Μπορείς να προσθέσεις χρήστη, συνοδό ή πρακτορείο στην λίστα με τα αγαπημένα. 
Η λίστα αυτή χρησιμοποιείτε για ενημερώσεις email, σε περίπτωση κάτι νεότερου γεγονότος. 
Εάν μία <b>συνοδός </b> είναι στην λίστα, θα λάβεις αυτό το email: 
<ul><li>Συνοδός έλαβε νέα αξιολόγιση</li><li>Συνοδός βρίσκετε σε τούρ</li><li>Συνοδός αναχώρησε απο / γυρίζει απο εκδρομή</li></ul> 
Εάν ένα <b>πρακτορείο</b> είναι στήν λίστα, θα λάβεις αυτό το email: 
<ul><li>Agency added a new escort</li></ul> 
Εάν ένα<b>μέλος</b> είναι στην λίστα, θα λάβεις αυτό το email: 
<ul><li>Το μέλος έβαλε μια νέα αξιολόγιση</li></ul> 
Μπορείς να έχεις όσα αντικείμενα θέλεις στην λίστα σου. 
Για εύκολη διαχείρηση της λίστας, κάνε κλίκ στο \"Αγαπημένη λίστα\" στο μενού μετά την σύνδεση σου. 
"; 
$LNG['hip'] = "Γοφοί"; 
$LNG['home'] = "Αρχική"; 
$LNG['home_page'] = "Αρχική σελίδα"; 
$LNG['hour'] = "ώρα"; 
$LNG['hours'] = "ώρες"; 
$LNG['howto_set_main'] = "Για να μπεί κύρια φωτογραφία, κάνε κλίκ πάνω."; 
$LNG['i_agree_with_terms'] = "Έχω διαβάσει και συμφωνώ με τους γενικούς <a href='%LINK%'>terms & conditions</a>"; 
$LNG['in'] = "Μέσα"; 
$LNG['in_face'] = "Στο πρόσωπο"; 
$LNG['in_mouth_spit'] = "Στο στόμα το έφτυσε"; 
$LNG['in_mouth_swallow'] = "Στο στόμα το κατάπιε"; 
$LNG['incall'] = "Incall"; //Εντός 
$LNG['independent'] = "Ανεξάρτητο"; 
$LNG['independent_escorts_from'] = "Ανεξάρτητοι συνοδοί από"; 
$LNG['independent_girls'] = "Ανεξάρτητα κορίτσια"; 
$LNG['info'] = "Πληροφορίες"; 
$LNG['intelligent'] = "Έξυπνη"; 
$LNG['international'] = "Διεθνώς"; //Παγκόσμιο 
$LNG['international_directory'] = "Παγκόσμιος κατάλογος"; 
$LNG['invalid_birth_date'] = "Λανθασμένη ημερομηνία γέννησης"; 
$LNG['invalid_curr_password'] = "Λανθασμένος υπάρχον κωδικός"; 
$LNG['invalid_desired_date'] = "Λανθασμένη επιθυμιτη ημερομηνία"; 
$LNG['invalid_discount_code'] = "Λανθασμένος κωδικός έκπτωσης."; 
$LNG['invalid_email'] = "Λανθασμένο e-mail"; 
$LNG['invalid_email_info'] = "Our system has identified your e-mail address as invalid. It is not possible to send an email to you. Your account has been temporary blocked. Please provide us with a valid email address, we'll send you a verification email with a confirmation link. After you receive the email and click on the link your account will be again working.<br /><br /> 
Thank you for understanding."; 
$LNG['invalid_file_type'] = "Λανθασμένος τύπος αρχείου. Μόνο Jpeg αρχεία είναι έγκυρα."; 
$LNG['invalid_meeting_date'] = "Λανθασμένη ημερομηνία συνάντησης"; 
$LNG['invalid_showname'] = "Λανθασμένο όνομα, παρακαλούμε χρησιμοποιείστε μόνο γράμματα και αριθμούς."; 
$LNG['invalid_user_type'] = "Λανθασμένος είδος χρήστη"; 
$LNG['invitation'] = "Καλώς ήρθατε στις υπηρεσίες συνοδών! Παρακαλούμε επιλέξτε πόλη που ψάχνετε για συνοδό."; 
$LNG['invitation_1'] = "Στις παρακάτω σελίδες θα βρείτε τα καλύτερα κορίτσια συνοδούς, private clubs και γραφεία συνοδών στην περιοχή σου - ".$LNG['country_'.MAIN_COUNTRY]." και σε ολόκληρο τον κόσμο. Μπορείτε να δείτε λεπτομέριες επικοινωνίας και φωτογραφίες για κάθε κοπέλα."; 
$LNG['invitation_2'] = "Σας ευχόμαστε μια ευχάριστη περιήγηση στο site μάς. Για ερωτήσεις και απορίες μην διστάσετε να επικοινωνίσετε με εμάς: <a href='mailto:".NGE_CONTACT_EMAIL."'>".NGE_CONTACT_EMAIL."</a>"; 
$LNG['kiss'] = "Φιλί"; 
$LNG['kiss_with_tongue'] = "Φιλί με γλώσσα"; 
$LNG['kissing'] = "Φιλί"; 
$LNG['kissing_with_tongue'] = "Φιλί με γλώσσα"; 
$LNG['language'] = "Γλώσσα"; 
$LNG['language_problems'] = "Πρόβλημα γλώσσας"; 
$LNG['languages'] = "Γλώσσες"; 
$LNG['languages_changed'] = "Οι γλώσσες ανανεώθηκαν επιτυχώς"; 
$LNG['last'] = "Τελευταίο"; 
$LNG['last_name'] = "Επώνυμο"; 
$LNG['latest_10_reviews'] = "Οι τελευταίες 10 κριτικές"; 
$LNG['latest_10_third_party_reviews'] = "Οι τελευταίες 10 κριτικές άλλων"; 
$LNG['level'] = "Επίπεδο"; 
$LNG['limitrofi'] = "Limitrofi"; 
$LNG['links'] = "Σύνδεσμοι"; 
$LNG['loano'] = "Loano"; 
$LNG['locations_changed'] = "Οι περιοχές ανανεώθηκαν επιτυχώς"; 
$LNG['logged_in'] = "Είστε συνδεδεμένος"; 
$LNG['login'] = "Σύνδεση"; 
$LNG['enter'] = "Είσοδος"; 
$LNG['login_and_password_alphanum'] = "Το όνομα χρήστη και ο κωδικός πρέπει να περιέχουν μόνο γράμματα και νούμερα(0-9, a-z, A-Z, _)."; 
$LNG['login_and_signup_disabled'] = "Αυτή τη στιγμή κάνουμε προγραματισμένη συντήρηση. Δεν μπορείτε να κάνετε εγγραφή ή σύνδεση αυτή τη στιγμή. Παρακαλούμε ελάτε αργότερα. Σας ζητάμε συγνώμη για την αναστάτωση."; 
$LNG['login_exists'] = "Ο κωδικός χρήστη υπάρχει ήδη. Παρακαλώ επιλέξτε άλλον..."; 
$LNG['login_problems_contact'] = "Εάν έχετε προβλήματα μην διστάσετε να επικοινωνίσετε με εμάς: <a href='mailto:".NGE_CONTACT_EMAIL."'>".NGE_CONTACT_EMAIL."</a>"; 
$LNG['logout'] = "Αποσύνδεση"; 
$LNG['looking_for_girls_in_your_city'] = "Ψάχνεις για κορίτσια στην πόλη σου?"; 
$LNG['looking_for_girls_in_your_region'] = "Looking for girls in your region?"; 
$LNG['looks'] = "Looks"; 
$LNG['looks_and_services'] = "Εμφάνιση και υπηρεσίες"; 
$LNG['looks_range'] = "Εμφάνιση σειράς"; 
$LNG['looks_rating'] = "Βαθμολόγιση εμφάνισης"; 
$LNG['lower_date'] = "Η επιλεγμένη ημερομηνία δεν πρέπει να είναι πιο πρίν απο την υπάρχον"; 
$LNG['lower_fuckometer'] = "Fuck-όμετρο δεν είναι σωστό"; 
$LNG['lower_looks_rating'] = "Η εμφάνιση σειράς βαθμολόγισης δεν είναι σωστή"; 
$LNG['lower_services_rating'] = "Η σειρά βαθμολόγισης υπηρεσιών δεν είναι σωστή"; 
$LNG['male'] = "Άντρας"; 
$LNG['masturbation'] = "Αυνανισμός"; 
$LNG['meeting_costs'] = "Έξοδα συνάντησης"; 
$LNG['meeting_date'] = "Ημερομηνία συνάντησης"; 
$LNG['meeting_date_range'] = "Σειρά ημερομηνίας συναντήσεων"; 
$LNG['meeting_dress_comments'] = "μέρος συνάντησης, dress code, σχόλια"; 
$LNG['meeting_length'] = "Διάρκεια συνάντησης"; 
$LNG['member'] = "Μέλος"; 
$LNG['member_name'] = "Όνομα μέλους"; 
$LNG['member_profit_01'] = "γίνε μέλος στο φόρουμ και γράψε τα σχόλια σου και εμπειρίες σου"; 
$LNG['member_profit_02'] = "γράψε αξιολογίσεις για τα κορίτσια"; 
$LNG['member_profit_03'] = "πληροφορήσου για τα νέα κορίτσια στο %SITE%"; 
$LNG['member_profits'] = "Ώς συνδρομητής έχετε τα οφέλη"; 
$LNG['membership_change_subject'] = "Συνδρομή άλλαξε σε".NGE_SITE_TITLE; 
$LNG['membership_change_to_free_mail'] = " 
    <p>Κύριε %LOGIN%,<br /> 
    Η συνδρομή σας ώς *PremiumMember* στο ".NGE_SITE_TITLE." έχει λήξει και η συνδρομή σας είναι ή δωρεάν.</p> 
    <p>Εάν θέλετε να επεκτείνετε την συνδρομή σας, παρακαλούμε κάντε το απο το site ή στείλτε μας email.</p> 
    <p>Αναβαθμίστε ΤΩΡΑ και κερδίστε:</p> 
    <ul> 
        <li>Ποσοστό έκπτωσεις για τις κοπέλες που παρουσιάζουμε<a href='%LINK%'>εδώ</a></li> 
        <li>Αυτόματα παρακολουθείτε τη μηνιαία &quot;Δωρεάν Fuck Λοτταρία&quot; που μπορείτε να νικήσετε ένα δωρεάν ραντεβού με την αγαπημένη σας κοπέλα</li> 
        <li>Πρόσβαση στο premium φόρουμ</li> 
        <li>Ιδιωτικά μηνύματα</li> 
        <li>Αγαπημένες λίστες με κοπέλες, χρήστες, πρακτορεία</li> 
        <li>Δυνατότητα προχωρημένων αναζητήσεων</li> 
        <li>Όχι διαφημίσεις</li> 
    </ul> 
    <p>Για παραπάνω ερωτήσεις ή αντιμετωπίζεται κανένα πρόβλημα, απλώς επικοινωνίστε με email. Θα σας απαντήσουμε σύντομα.</p> 
    <p>Πραγματικά δικοί σας<br /> 
    ".NGE_SITE_TITLE."- Ομάδα</p> 
"; 
$LNG['membership_change_to_premium_mail'] = " 
    <p>Κύριε %LOGIN%,<br /> 
    Η συνδρομή σας ώς *PremiumMember* στο ".NGE_SITE_TITLE." ήταν επιτυχής. Σας ευχαριστούμε για την εμπιστοσύνη.</p> 
    <p>Πληροφορίες σύνδεσης είναι οι εξής:<br /> 
    Σύνδεση: %LOGIN%</p> 
    <p>(Παρακαλούμε κρατήστε τις πληροφορίες αυτές ασφαλής)</p> 
    <p>Πάλι εδώ, ένα σύνολο για τα προνόμοια ώς *Premium Member* είναι:</p> 
    <ul> 
        <li>Έκπτωση σε όλα τα bookings με τις κοπέλες που εμφανίζοντε <a href='%LINK%'>εδώ</a></li> 
        <li>Αυτόματα πέρνετε μέρος στη&quot;Δωρεάν Fuck Λοτταρία&quot; που μπορείς να κερδίσεις μία ώρα δωρεάν με την συνοδό της αρεσκείας σου</li> 
        <li>Πρόσβαση στο premium φόρουμ</li> 
        <li>Ιδιωτικά μηνύματα</li> 
        <li>Αγαπημένες λίστες με κοπέλες, χρήστες, πρακτορεία</li> 
        <li>Προχωρημένες αναζητήσεις</li> 
        <li>Όχι διαφημίσεις</li> 
    </ul> 
    <p>Για περισσότερες ερωτήσεις ή εάν αντιμετωπίζετε κάποιο πρόβλημα, απλώς επικοινωνείστε με εμάς με email. Θα απαντήσουμε σύντομα.</p> 
    <p>Ειλικρινά δικοί σας<br /> 
    ".NGE_SITE_TITLE."- Ομάδα</p> 
"; 
$LNG['membership_type'] = "Τύπος συνδρομής"; 
$LNG['membership_types'] = "Τύποι συνδρομών"; 
$LNG['men'] = "Άντρες"; 
$LNG['menu'] = "Μενού"; 
$LNG['message'] = "Μήνυμα"; 
$LNG['message_for'] = "Μήνυμα για %NAME%"; 
$LNG['message_not_sent'] = "Αποστολή μηνύματος απέτυχε."; 
$LNG['message_sent'] = "Το μήνυμα στάλθηκε στο %NAME%."; 
$LNG['metric'] = "Σύστημα μέτρησης"; 
$LNG['metric_desc'] = "cm, klg, ..."; 
$LNG['middle_name'] = "Μέσον όνομα"; 
$LNG['minutes'] = "minutes"; 
$LNG['modified'] = "Επεξεργάστηκε"; 
$LNG['modify_escort'] = "Επεξεργασία συνοδού"; 
$LNG['monday'] = "Δευτέρα"; 
$LNG['month'] = "Μήνας"; 
$LNG['month_1'] = "Ιανουάριος"; 
$LNG['month_10'] = "Οκτώμβριος"; 
$LNG['month_11'] = "Νοέμβριος"; 
$LNG['month_12'] = "Δεκέμβριος"; 
$LNG['month_2'] = "Φεβρουάριος"; 
$LNG['month_3'] = "Μάρτιος"; 
$LNG['month_4'] = "Απρίλιος"; 
$LNG['month_5'] = "Μάιος"; 
$LNG['month_6'] = "Ιούνιος"; 
$LNG['month_7'] = "Ιούλιος"; 
$LNG['month_8'] = "Αύγουστος"; 
$LNG['month_9'] = "Σεπτέμβριος"; 
$LNG['months'] = "Μήνες"; 
$LNG['more_info'] = "Περισσότερες πληροφορίες..."; 
$LNG['more_new_girls'] = "Περισσότερα νέα κορίτσια"; 
$LNG['more_news'] = "Περισσότερα νέα..."; 
$LNG['multiple_times_sex'] = "Πολλές φορές σέξ"; 
$LNG['my_escort_profile'] = "Το προφίλ μου"; 
$LNG['my_escorts'] = "Οι συνοδοί μου"; 
$LNG['my_languages'] = "Η γλώσσα μου"; 
$LNG['my_locations'] = "Η περιοχή μου"; 
$LNG['my_new_review_notify_agency'] = "Πληροφορίες στο email εάν κάποιος συνοδός έλαβε νέα αξιολόγιση."; 
$LNG['my_new_review_notify_single'] = "Πληροφορίες στο email εάν έλαβα νέα αξιολόγιση."; 
$LNG['my_photos'] = "Οι φωτογραφίες μου"; 
$LNG['my_rates'] = "Οι βαθμολογίες μου"; 
$LNG['my_real_profile'] = "Τα πραγματικά μου στοιχεία"; 
$LNG['my_reviews'] = "Οι κριτικές μου"; 
$LNG['name'] = "Όνομα"; 
$LNG['name_of_the_lady'] = "Όνομα της γυναίκας"; 
$LNG['name_of_the_site'] = "Όνομα του site"; 
$LNG['ethnic'] = "Ethnic"; 
$LNG['nationality'] = "Εθνικότητα"; 
$LNG['need_help'] = "Χρειάζεσαι βοήθεια ?"; 
$LNG['need_to_be_registered_to_review'] = "Πρέπει να είστε εγγεγραμένος στο %SITE% για να δείτε αξιολογίσεις για τις γυναίκες, <a href='%LINK%'>Εγγραφείτε δωρεάν ΤΩΡΑ!</a>"; 
$LNG['never'] = "ποτέ"; 
$LNG['new'] = "Νέο"; 
$LNG['new_arrivals'] = "Νέες αφίξεις"; 
$LNG['new_email'] = "Νέα email"; 
$LNG['new_entries'] = "Νέες αφίξεις"; 
$LNG['new_escort_email_by_agency_body'] = "<pre>Το πρακτορείο \"%AGENCY%\" πρόσθεσε νέο συνοδό: %ESCORT% ! 
Έλαβες αυτή την ενημέρωση γιατί το πρακτορείο είναι στην λίστα σου με τα αγαπημένα. 
Κάνε κλίκ στον σύνδεσμο αυτό 
<a href=\"%LINK%\">%LINK%</a> 
να δείς του συνοδού την κάρτα. 
Your ".NGE_SITE_TITLE." Team 
</pre>"; 
$LNG['new_escort_email_by_agency_subject'] = "%AGENCY% προστέθικε νέα συνοδός!"; 
$LNG['new_password'] = "Νέος κωδικός"; 
$LNG['new_password_empty_err'] = "Ο νεός κωδικός δεν πρέπει να είναι κενός"; 
$LNG['new_review_email_agency_body'] = "<pre>Ο χρήστης %USER% δημοσίευσε μια νέα αξιολόγιση για την συνοδό σου %ESCORT% ! 
Κάνε κλίκ στον σύνδεσμο αυτό 
<a href=\"%LINK%\">%LINK%</a> 
να διαβάσεις την κριτική. 
Your ".NGE_SITE_TITLE." Team 
</pre>"; 
$LNG['new_review_email_agency_subject'] = "Συνοδός σου έχει μια νέα κριτική!"; 
$LNG['new_review_email_by_escort_body'] = "<pre>Συνοδός %ESCORT% έλαβε μια νέα κριτική απο τον χρήστη %USER% ! 
Έλαβες αυτή την ενημέρωση γιατί ο συνοδός είναι στην λίστα σου με τα αγαπημένα. 
Κάνε κλίκ στον σύνδεσμο αυτό 
<a href=\"%LINK%\">%LINK%</a> 
να διαβάσεις την κριτική. 
Your ".NGE_SITE_TITLE." Team 
</pre>"; 
$LNG['new_review_email_by_escort_subject'] = "%ESCORT% έλαβε μια νέα αξιολόγιση!"; 
$LNG['new_review_email_by_user_body'] = "<pre>Ο χρήστης %USER% δημοσίευσε νεά κριτική για συνοδό %ESCORT% ! 
Έλαβες αυτή την ενημέρωση γιατί ο χρήστης είναι στην λίστα σου με τα αγαπημένα. 
Κάνε κλίκ στον σύνδεσμο αυτό 
<a href=\"%LINK%\">%LINK%</a> 
να διαβάσεις την κριτική. 
Your ".NGE_SITE_TITLE." Team 
</pre>"; 
$LNG['new_review_email_by_user_subject'] = "%USER% δημοσίευσε νέα αξιολόγιση!"; 
$LNG['new_review_email_escort_body'] = "<pre>Ο χρήστης %USER% δημοσίευσε μια νέα κριτική για εσένα! 
Κάνε κλίκ στον σύνδεσμο αυτό 
<a href=\"%LINK%\">%LINK%</a> 
να διαβάσεις την κριτική. 
Your ".NGE_SITE_TITLE." Team 
</pre>"; 
$LNG['new_review_email_escort_subject'] = "Έχεις μια νέα κριτική!"; 
$LNG['news'] = "Νέα"; 
$LNG['newsletter'] = "Ενημερωτικό δελτίο"; 
$LNG['next'] = "Επόμενο"; 
$LNG['next_step'] = "Επόμενο βήμα"; 
$LNG['no'] = "Όχι"; 
$LNG['no_blowjob'] = "Όχι πεολειχία"; 
$LNG['no_cumshot'] = "Όχι χύσιμο"; 
$LNG['no_esc_sel_err'] = "Πρέπει να επιλέξετε συνοδό"; 
$LNG['no_escort_languages'] = "Δεν έχετε επιλέξει γλώσσα. <a href='%LINK%'>Κάντε κλίκ εδώ</a> να επιλέξετε μία."; 
$LNG['no_escort_main_pic'] = "Δεν έχετε επιλέξει κεντρική φωτογραφία. <a href='%LINK%'>Κάντε κλίκ εδώ</a> to choose one."; 
$LNG['no_escort_name_languages'] = "Συνοδός %ESCORT% δεν έχει επιλέξει γλώσσα. <a href='%LINK%'>Κάντε κλίκ εδώ</a> να επιλέξετε μία."; 
$LNG['no_escort_name_main_pic'] = "Συνοδός %ESCORT% δεν έχει επιλέξει κεντρική φωτογραφία. <a href='%LINK%'>Κάντε κλίκ εδώ</a> να επιλέξετε μία."; 
$LNG['no_escort_name_photos'] = "Συνοδός %ESCORT% δεν έχει έγκυρες φωτογραφίες στο προφίλ του. <a href='%LINK%'>Κάντε κλίκ εδώ</a> να ανανεώσετε την γκαλερί."; 
$LNG['no_escort_photos'] = "Δεν έχετε έγκυρες φωτογραφίες στο προφίλ σας. <a href='%LINK%'>Κάντε κλίκ εδώ</a> να ανανεώσετε την γκαλερί σας."; 
$LNG['no_escort_profile'] = "Δεν έχετε ενεργό κανένα προφίλ συνοδού. <a href='%LINK%'>Κάντε κλίκ εδώ</a> να προσθέσετε ένα."; 
$LNG['no_escorts_found'] = "Δεν βρέθηκαν συνοδοί"; 
$LNG['no_escorts_profile'] = "Δεν έχετε ορίσει κανένα προφίλ συνοδού. Παρακαλώ <a href='%LINK%'>Κάντε κλίκ εδώ</a> να προσθέσετε ένα."; 
$LNG['no_kiss'] = "Όχι φιλί"; 
$LNG['no_kissing'] = "Όχι φιλί"; 
$LNG['no_only_once'] = "Όχι, μόνο μία"; 
$LNG['no_premium_escorts'] = "Όχι premium συνοδοί."; 
$LNG['no_reviews_found'] = "Δεν βρέθηκαν κριτικές"; 
$LNG['no_sex_avail_chosen'] = "Έχεις να επιλέξεις τουλάχιστον μία σεξουαλική διαθεσιμότητα"; 
$LNG['no_users_found'] = "Δεν βρέθηκαν χρήστες"; 
$LNG['no_votes'] = "Χωρίς ψήφους"; 
$LNG['norm_memb'] = "1 μήνας νορμάλ συνδρομή για 50 ευρώ"; 
$LNG['normal'] = "Νορμάλ"; 
$LNG['normal_mb_desc_ag'] = "%COUNT% συνοδοί (1st expiring in %DAYS% days)"; 
$LNG['normal_review'] = "νορμάλ"; 
$LNG['normal_review_desc'] = "Νορμάλ αξιολόγιση σημαίνει, το πρακτορείο/κοπέλα δεν απαντάει στο sms/e-mail έτσι η κριτική δεν είναι επιβεβαιωμένη αλλά επίσης δεν διαψεύδεται! Αυτό σημαίνει ότι η κριτική μπορεί να θεωρηθεί ώς έγκυρη!"; 
$LNG['not_approved'] = "Περιμένετε για αποδοχή"; 
$LNG['not_available'] = "Not available"; 
$LNG['not_logged'] = "Δεν είστε συνδεδεμένος"; 
$LNG['not_logged_desc'] = "Πρέπει να συνδεθείτε για να χρησιμοποιήσετε αυτό το χαρακτηριστικό."; 
$LNG['not_verified_msg'] = " 
<p>Ο λογαριασμός σας δεν έχει εξακριβωθεί ακόμα.<br /> 
Για να γίνεται κανονικό μέλος στο ".NGE_SITE_TITLE." πρέπει να επαληθεύσετε το e-mail σας. Εάν ακόμα δεν έχετε λάβει το email μας<a href=\"%LINK%\">κάντε κλίκ εδώ να σας ξαναστείλουμε το email για ενεργοποίηση</a>. 
</p><p> 
Σημείωση: παρακαλώ κοιτάξτε και τον φάκελο με spam, μερικές φόρες κάποιοι provider στέλνουν τα mails στον φάκελο spam. Ευχαριστούμε για την κατανόηση. 
</p>"; 
$LNG['not_yet_member'] = "Δεν είσαι ΜΕΛΟΣ?<br />Δωρεάν εγγραφή τώρα!"; 
$LNG['not_yet_premium_signup_now'] = "Δεν είστε  premium μέλος? <a href='%LINK%'>Γραφτείτε ΤΩΡΑ</a> και γλιτώστε χρήματα!"; 
$LNG['note'] = "Σημείωση"; 
$LNG['notify_comon'] = "<pre>Έλαβες αυτό το email γιατί είσαι μέλος στο ".NGE_SITE_TITLE." και έχεις επιλέξει να σου στέλνουμε αυτά τα emails. 
Για να αλλάξεις ρυθμίσεις, παρακαλώ συνδεθείτε στον λογαριασμό σας και κάντε κλίκ στο \"Ρυθμίσεις\" στο μενού.</pre>"; 
$LNG['now'] = "Τώρα"; 
$LNG['number'] = "Νο."; 
$LNG['number_of_reviews'] = "Νούμερο κριτικών"; 
$LNG['offers_discounts'] = "Προσφορές εκτπώσεων"; 
$LNG['on_tour_in'] = "σε ταξίδι στην"; 
$LNG['only'] = "μόνο"; 
$LNG['only_fake_free_reviews'] = "Μόνο 100% αληθινές κριτικές"; 
$LNG['only_for_premium_members'] = "μόνο για premium μέλη"; 
$LNG['only_girls_with_reviews'] = "Μόνο κορίτσια με κριτικές"; 
$LNG['only_reviews_from_user'] = "Μόνο κριτικές απο χρήστη"; 
$LNG['only_reviews_not_older_then'] = "Κριτικές όχι παλαιότερες από"; 
$LNG['optional'] = "προαιρετικό"; 
$LNG['oral_without_condom'] = "Στωματικό χωρίς"; 
$LNG['order_premium_spot_now'] = "Παράγγειλε Premium χώρο!"; 
$LNG['origin_escortforum'] = "forum συνοδών"; 
$LNG['origin_non_escortforum'] = "forum μη συνοδών"; 
$LNG['other'] = "Άλλο"; 
$LNG['other_boys_from'] = "Άλλοι συνοδοί από"; 
$LNG['other_escorts_from'] = "Άλλοι συνοδοί από"; 
$LNG['boys_from'] = "Συνοδοί από"; 
$LNG['escorts_from'] = "Συνοδοί από"; 
$LNG['outcall'] = "Outcall"; //Εκτός 
$LNG['overall_rating'] = "Γενική αξιολόγιση"; 
$LNG['partners'] = "Συνεργάτες"; 
$LNG['passive'] = "Παθητικό"; 
$LNG['passwd_has_been_sent'] = "Ο κωδικός σου θα σταλεί στο %EMAIL%"; 
$LNG['passwd_succesfuly_sent'] = "Ο κωδικός σας στάλθηκε με επιτυχία στο email που μας δώσατε."; 
$LNG['password'] = "Κωδικός"; 
$LNG['password_changed'] = "Ο κωδικός άλλαξε επιτυχώς"; 
$LNG['password_invalid'] = "Ο κωδικός είναι άκυρος. Πρέπει να περιέχει το λιγότερο 6 χαρακτήρες."; 
$LNG['password_missmatch'] = "Ο κωδικός δεν ταιριάζει"; 
$LNG['percent_discount'] = "%DISCOUNT%% έκπτωση"; 
$LNG['phone'] = "Τηλέφωνο"; 
$LNG['phone_instructions'] = "Phone instructions"; 
$LNG['photo_correct'] = "Διόρθωση φωτογραφίας"; 
$LNG['photos'] = "Φωτογραφίες"; 
$LNG['pic_delete_failed'] = "Διαγραφή φωτογραφίας απέτυχε"; 
$LNG['pic_deleted'] = "Φωτογραφία διαγράφηκε"; 
$LNG['place'] = "Μέρος"; 
$LNG['please_choose_services'] = "Παρακαλώ διαλέξτε τις υπηρεσίες που πρόσφερε η κοπέλα; αυτό επιδρά στο fuck-όμετρο! Ευχαριστούμε για την κατανόηση!"; 
$LNG['please_fill_form'] = "Παρακαλώ συμπληρώστε την παρακάτω φόρμα"; 
$LNG['please_provide_email'] = "Παρακαλούμε θέστε το email που χρησιμοποιήσατε κατά την εγγραφή%SITE%"; 
$LNG['please_select'] = "Παρακαλώ επιλέξτε..."; 
$LNG['plus2'] = "2+"; 
$LNG['poshy'] = "Poshy"; 
$LNG['posted_reviews'] = "Έχετε γράψει %NUMBER% κριτικές μέχρι τώρα."; 
$LNG['posted_to_forum'] = "Έχετε γράψει %NUMBER% καταχωρήσεις μέχρι τώρα."; 
$LNG['premium'] = "Premium"; 
$LNG['premium_escorts'] = "Premium συνοδοί"; 
$LNG['premium_mb_desc_ag'] = "%COUNT% συνοδοί (1st expiring in %DAYS% days)"; 
$LNG['premium_user'] = "Premium χρήστης"; 
$LNG['previous'] = "Προηγούμενο"; 
$LNG['price'] = "Τιμή"; 
$LNG['priv_apart'] = "Ιδιωτικός χώρος"; 
$LNG['private_menu'] = "Προσωπικό μενού"; 
$LNG['problem_report'] = "Αναφορά προβλήματος"; 
$LNG['problem_report_back'] = "Κάντε κλίκ εδώ να μεταβείτε στο προφίλ συνοδού"; 
$LNG['problem_report_no_accept'] = "Ευχαριστούμε, η αναφορά για το πρόβλημα ελήφθη"; 
$LNG['problem_report_no_name'] = "Παρακαλώ, δώστε το όνομα σας"; 
$LNG['problem_report_no_report'] = "Παρακαλώ, δώστε αναφορά"; 
$LNG['prof_city_expl'] = "Πόλη προέλευσης κοπέλας, όχι υποχρεωτικά η περιοχή εργασίας. Περιοχές εργασίας μπορούν να οριστούν στα επόμενα βήματα."; 
$LNG['prof_country_expl'] = "Χώρα προέλευσης κοπέλας, όχι υποχρεωτικά η περιοχή εργασίας. Περιοχές εργασίας μπορούν να οριστούν στα επόμενα βήματα."; 
$LNG['profile'] = "Προφίλ"; 
$LNG['profile_changed'] = "Το προφίλ ανανεώθηκε επιτυχώς"; 
$LNG['promote_this_girl'] = "Προώθησε αυτή την κοπέλα"; 
$LNG['promote_yourself'] = "Προώθησε τον εαυτό σου"; 
$LNG['provided_services'] = "Υπηρεσίες που παρέχουμε"; 
$LNG['publish_date'] = "Δημοσίευση ραντεβού"; 
$LNG['rank'] = "θέση"; 
$LNG['rate'] = "Εκτίμηση"; 
$LNG['rate_looks'] = "Εκτίμησε την εμφάνιση της"; 
$LNG['rate_services'] = "Εκτίμησε τις υπηρεσίες της"; 
$LNG['rates'] = "Εκτιμήσεις"; 
$LNG['rates_changed'] = "Οι εκτιμήσεις ανανεώθηκαν επιτυχώς"; 
$LNG['rates_deleted'] = "Εκτιμήσεις διαγράφηκαν επιτυχώς"; 
$LNG['rating'] = "Βαθμολόγιση"; 
$LNG['re_verify_sent'] = "Το εmail ενεργοποίησης εστάλη ξανά. Παρακαλούμε τσεκάρετε το email inbox και ακολουθείστε τις οδηγίες."; 
$LNG['read_this_review'] = "Διάβασε αυτή την κριτικές"; 
$LNG['read_whole_article'] = "Διαβάστε ολόκληρο το άρθρο"; 
$LNG['real'] = "Πραγματικό"; 
$LNG['real_data_form_text'] = "Παρακαλούμε γράψτε πραγματικές πληροφορίες. Να ξέρετε, πως αυτό δεν είναι κομμάτι για το προφίλ σας, οι πληροφορίες σας είναι ασφαλής για δική μας χρήση και δεν θα δοθούν σε τρίτους."; 
$LNG['real_name'] = "Πραγματικό όνομα"; 
$LNG['recalibrate_now'] = "Ξαναδιορθώστε ΤΩΡΑ"; 
$LNG['receive_newsletter'] = "Αποδέχομαι ".NGE_SITE_TITLE." newsletter"; 
$LNG['red'] = "Κόκκινα"; 
$LNG['reference'] = "Πώς μας βρήκατε?"; 
$LNG['regards'] = "Με εκτίμηση, %SITE% ομάδα."; 
$LNG['region'] = "Περιφέρεια"; 
$LNG['register'] = "Εγγραφή"; 
$LNG['registration'] = "Εγγραφή"; 
$LNG['rem_watch'] = "Διαγραφή απο τα αγαπημένα"; 
$LNG['rem_watch_success'] = "<b>%NAME%</b> διαγράφηκε επιτυχώς στα αγαπημένα. <a href='javascript:history.back()'>Κάντε κλίκ εδώ να πάτε πίσω.</a>"; 
$LNG['remove'] = "Αφαίρεση"; 
$LNG['renew_now'] = "Ανανέωσε τώρα!"; 
$LNG['required_fields'] = "Υποχρεωτική συμπλήρωση τα κενά που έχουν*"; 
$LNG['review'] = "Αξιολόγιση"; 
$LNG['review_changed'] = "Η κριτική ανανεώθηκε επιτυχώς"; 
$LNG['review_guide_and_rules'] = " 
    <strong>Οδηγός και κανόνες:</strong><br /> 
    Παρακαλούμε σεβαστείτε τους κανόνες, μην γράφετε ψεύτικες κριτικές! Κάθε κριτική βοηθάει εσάς και τα υπόλοιπα μέλη για καλύτερες υπηρεσίες! 
    <ul> 
        <li>παρακαλώ καθορίστε όλες τις υπηρεσίες που προσφέρει η κοπέλα</li> 
        <li>πρόσεθεσε νέα κοπέλα στην βάση δεδομένων, εάν δεν βρίσκεται την κοπέλα παρακαλούμε να την προσθέσετε στην βάση δεδομένων μας</li> 
        <li>παρακαλούμε περιγράψτε την εμπειρία μαζί της, αξιολογίσεις με μικρές προτάσεις: είναι τέλεια, θα ακυρωθεί</li> 
        <li>παρακαλούμε μην βαθμολογείτε τις κοπέλες παραπάνω απο 8.0 εάν δεν προσφέρουν το λιγότερο: 
        <ul> 
            <li>στοματικό χωρίς προφυλακτικό</li> 
            <li>φυρική συμπεριφορά ή ενθουσιώδης</li> 
            <li>φιλί με γλώσσα</li> 
        </ul> 
    </ul>"; 
$LNG['review_language'] = "Γλώσσα κριτικής"; 
$LNG['review_sms'] = "Έχετε μια κριτική στο ".NGE_SITE_TITLE."! Απλώς απαντήστε με sms εμάς με: ναι, για αλήθεια και όχι, για μή αλήθεια! Στο 2ο sms θα λάβεις πληροφορίες για τον πελάτη!"; 
$LNG['review_status'] = "Επίπεδο κριτικής"; 
$LNG['review_this_girl'] = "Αξιολογίστε την κοπέλα"; 
$LNG['reviews'] = "Κριτικές"; 
$LNG['reviews_to_recalibrate'] = " 
    Κύριε %NAME%,<br /> 
    Έχετε %REVIEWS% Κριτικές να ξανακαθορίσετε. Παρακαλούμε βοηθήστε μας να ξανακαθορίσουμε τις αξιολογίσεις σας και να δώσουμε λεπτομερείς πληροφορίες για την κοπέλα που συναντήσατε."; 
$LNG['rouen'] = "Rouen"; 
$LNG['royal'] = "Βασιλικό σύστημα"; 
$LNG['royal_desc'] = "ίντσες, πόδια, ..."; 
$LNG['rss_channel_desc'] = "Νέοι συνοδοί"; 
$LNG['salon'] = "Night Club"; 
$LNG['sardegna'] = "Sardegna"; 
$LNG['saturday'] = "Σάββατο"; 
$LNG['sauna'] = "Σάουνα"; 
$LNG['save_changes'] = "Αλλαγές αποθηκεύτικαν"; 
$LNG['score'] = "Σκόρ"; 
$LNG['search'] = "Αναζήτηση"; 
$LNG['search_desc'] = "Καλώς ήρθατε στο απλό εργαλείο αναζήτησης του ".NGE_SITE_TITLE.". Για πιο πολλές επιλογές κάντε κλίκ <a href='%LINK%'>προχωρημένη αναζήτηση</a>."; 
$LNG['search_desc2'] = "Καλώς ήρθατε στο προχωρημένη αναζήτηση του ".NGE_SITE_TITLE.". Για απλές αναζητήσεις κάνε κλίκ <a href='%LINK%'>απλή αναζήτηση</a>."; 
$LNG['search_escorts'] = "Αναζήτηση συνοδών"; 
$LNG['search_name'] = "Κορίτσι-/όνομα πρακτορείου"; 
$LNG['search_reviews'] = "Αναζήτηση κριτικών"; 
$LNG['search_reviews_back'] = "Πίσω στην φόρμα αναζήτησης"; 
$LNG['search_reviews_no_params'] = "Παρακαλώ, διαλέξτε το λιγότερο μια παράμετρο αναζήτησης"; 
$LNG['search_reviews_no_result'] = "Συγνώμη, κανένα αποτέλεσμα δεν βρέθηκε, παρακαλώ προσπαθήστε ξανά"; 
$LNG['search_users'] = "Αναζήτηση χρήστη"; 
$LNG['see_escorts_soon_on_tour'] = "Κάντε κλίκ εδώ να δείτε τις κοπέλες που σύντομα θα είναι στην ".$LNG['country_'.MAIN_COUNTRY].""; 
$LNG['see_independent_preview'] = "δείτε παρουσιάσεις απο ανεξάρτητους συνοδούς"; 
$LNG['select_as_main'] = "Θέλεις να βάλεις την επιλεγμένη φωτογραφία ώς κύρια;"; 
$LNG['select_your_service_requirements'] = "Διάλεξε τις προυποθέσεις για τις υπηρεσίες"; 
$LNG['send_message'] = "Αποστολή μηνύματος"; 
$LNG['service_comments'] = "Σχόλια υπηρεσιών"; 
$LNG['services'] = "Υπηρεσίες"; 
$LNG['services_range'] = "Σειρά υπηρεσιών"; 
$LNG['services_rating'] = "Βαθμολόγιση υπηρεσιών"; 
$LNG['services_she_was_willing'] = "Υπηρεσίες που προσφέρει"; 
$LNG['settings'] = "Ρυθμίσεις"; 
$LNG['settings_update_error'] = "Λανθασμένη ανανέωση ρυθμίσεων"; 
$LNG['settings_updated'] = "Οι ρυθμίσεις ανανεώθηκαν επιτυχώς."; 
$LNG['sex'] = "Sex"; 
$LNG['sex_available_to'] = "Σεξουαλική διαθεσιμότητα"; 
$LNG['shoe_size'] = "Νούμερο παπούτσι"; 
$LNG['show_all_reviews'] = "Δείξε όλες τις Κριτικές"; 
$LNG['show_email'] = "Να είναι εμφανές το e-mail στο site?"; 
$LNG['show_name'] = "Εμφάνιση ονόματος"; 
$LNG['show_only_girls_on_tour_in'] = "Δείξε μου κοπέλες που είναι σε τούρ στις"; 
$LNG['show_site_reviews'] = "Δείξε %SITE% κριτικές"; 
$LNG['show_third_party_reviews'] = "Δείξε τις κριτικές άλλων"; 
$LNG['showname_exists'] = "Το όνομα αυτό υπάρχει, παρακαλώ επιλέξτε άλλο"; 
$LNG['sign_up'] = "Εγγραφή"; 
$LNG['sign_up_for_free'] = "Δωρεάν εγγραφή!"; 
$LNG['signup_activated_01'] = "Ο λογαριασμό σας ενεργοποιήθηκε επιτυχώς!"; 
$LNG['signup_activated_02'] = " 
        Συνχαρητήρια!<br /> 
        Είστε full μέλος στο %SITE% τώρα.<br /> 
        Οι πληροφορίες του λογαριασμού σας έχουν σταλεί στο e-mail σας."; 
$LNG['signup_activated_03'] = "Κάντε κλίκ να συνδεθείτε."; 
$LNG['signup_and_win'] = "Γραφτείτε σαν Premium Μέλος και <strong>ΚΕΡΔΙΣΤΕ ΜΙΑ ΔΩΡΕΑΝ ΣΥΝΑΝΤΗΣΗ ΑΞΙΑΣ 300 ΕΥΡΩ!</strong>"; 
$LNG['signup_confirmation_email'] = "<pre> 
Κύριε %USER%, 
σας ευχαριστούμε για την εγγραφή σας στο %SITE%. 
Είστε full μέλος στο %SITE%. 
Παρακάτω θα βρείτε τις πληροφορίες σύνδεσης σας. Παρακαλούμε κρατήστε τις πληροφορίες σε ασφαλές μέρος για μελλοντική χρήση: 
Σύνδεση: %USER% 
Website: %SITE% 
Απολαύστε το site! Περιμένουμε τις δημοσιεύσεις και τις κριτικές σας ! 
Your %SITE% Team</pre>"; 
$LNG['signup_confirmation_email_subject'] = "Εγγραφή στο %SITE%"; 
$LNG['signup_error_email_missing'] = "Λάθος: Δεν δώσατε έγκυρο e-mail, παρακαλώ βάλτε ένα έγκυρο e-mail."; 
$LNG['signup_error_email_used'] = "Λάθος: Αυτό το e-mail ήδη χρησιμοποιείτε στο %SITE%. Παρακαλώ επιλέξτε άλλο."; 
$LNG['signup_error_login_missing'] = "Λάθος: Δεν βάλατε όνομα σύνδεσης, παρακαλώ συμπληρώστε το."; 
$LNG['signup_error_login_used'] = "Λάθος: Το όνομα σύνδεσης υπάρχει ήδη. Παρακαλώ επιλέξτε διαφορετικό."; 
$LNG['signup_error_password_invalid'] = "Λάθος: Ο κωδικός είναι λανθασμένος. Ο κωδικός πρέπει να περιέχει το λιγότερο 6 χαρακτήρες."; 
$LNG['signup_error_password_missing'] = "Λάθος: Δεν βάλατε κωδικό, παρακαλώ συμπληρώστε το."; 
$LNG['signup_error_password_wrong'] = "Λάθος: Ο κωδικός και ο ξαναγραμμένος κωδικός δεν ταιριάζουν. Οι κωδικοί πρέπει να είναι ίδιοι και στα δύο κενά."; 
$LNG['signup_error_terms_missing'] = "Έχετε συμφωνήσει με τους όρους και προυποθέσεις για να εγγραφείτε στο site μας."; 
$LNG['signup_successful_01'] = "Η εγγραφή σας ως μέλος στο %SITE% ήταν επιτυχής."; 
$LNG['signup_successful_02'] = "Σας ευχαριστούμε για την εγγραφή σας !"; 
$LNG['signup_successful_03'] = "Για να ολοκληρωθεί η εγγραφή σας παρακαλώ ελένξτε το e-mail σας."; 
$LNG['signup_successful_04'] = "<b>ΠΡΟΣΟΧΗ: Παρακαλώ ελέγξτε τον φάκελο junk στο mailbox σας εάν δεν έχετε λάβει ακόμα το e-mail, μερικές φορές μπορεί να θέλει κάποιες ώρες για να το λάβετε.</b>"; 
$LNG['signup_verification_email'] = "<pre> 
Κύριε %USER%, 
σας ευχαριστούμε για την εγγραφή σας στο %SITE%. 
Για να γίνεται full μέλος πρώτα πρέπει να επισκεφτείτε τον σύνδεσμο παρακάτω, να ενεργοποιήσετε τον λογαριασμό σας. 
<a href=\"%LINK%\">%LINK%</a> 
Website: %SITE% 
Your %SITE% Team.</pre>"; 
$LNG['signup_verification_email_subject'] = "Εγγραφή στο %SITE%"; 
$LNG['silicon'] = "Σιλικόνη"; 
$LNG['single_girl'] = "Μόνο κορίτσι"; 
$LNG['sitemap'] = "Sitemap"; 
$LNG['smoker'] = "Καπνιστής"; 
$LNG['somewhere_else'] = "Κάπου αλλού"; 
$LNG['soon'] = "Σύντομα"; 
$LNG['state'] = "Περιοχή"; 
$LNG['step_1'] = "Βήμα 1"; 
$LNG['step_2'] = "Βήμα 2"; 
$LNG['submit'] = "Υποβολή"; 
$LNG['subscribe'] = "Εγγραφή"; 
$LNG['successfuly_logged_in'] = "Είστε επιτυχώς συνδεδεμένος."; 
$LNG['sucks'] = "Άσχημο"; 
$LNG['sunday'] = "Κυριακή"; 
$LNG['swallow'] = "Κατάποση"; 
$LNG['system_error'] = "Λάθος συστήματος, παρακαλώ επικοινωνίστε με την εξυπηρέτηση πελατών."; 
$LNG['terms_and_conditions'] = "Όροι και προυποθέσεις"; 
$LNG['terms_text'] = " 
    <h1>Condizioni generali di contratto (CGC)</h1> 
    <h2>&sect; 1 Rapporto dβ€™affari</h2> 
    <p>Le CGC regolano i rapporti tra gli utenti (di seguito β€utentiβ€), i clienti (di seguito β€clientiβ€) e la DA Products GmbH (di seguito β€DAβ€), il gestore del sito Internet β€".NGE_SITE_TITLE."β€ (di seguito β€sito webβ€) e valgono per tutti i servizi e i prodotti offerti nel sito web, a seconda dei prodotti e dei servizi di seguito vengono indicate ulteriori condizioni contrattuali.</p> 
    <h2>&sect; 2 Direttive generali dβ€™uso</h2> 
    <p>Il sito web Γ¨ destinato a utenti che abbiano piΓΉ di 18 anni.</p> 
    <p>Lβ€™etΓ  e altre informazioni personali contenute nel profilo dellβ€™utente o cliente, in annunci, chat, forum o recensioni ecc. sono controllati dalla DA solo a campione; questo controllo non ha validitΓ  giuridica e non Γ¨ vincolante. Ogni utente ha la responsabilitΓ  di controllare che i suoi partner abbiano compiuto la maggiore etΓ  in base alla legislazione in vigore nel proprio paese.</p> 
    <p>La DA non Γ¨ responsabile per lβ€™utilizzo delle informazioni personali rese accessibili nel sito web.</p> 
    <h2>&sect; 3 Dati utente / cliente, link di terzi, cookies</h2> 
    <p>Allβ€™atto dellβ€™utilizzo e in particolare allβ€™atto della registrazione e inserzione nel sito web la DA provvede a mettere in memoria alcune informazioni. Il cliente/utente puΓ² controllare e modificare i propri dati contenuti nel profilo e nelle inserzioni ogni qualvolta lo desideri. La DA inoltre mette in memoria in forma anonima i dati utente e cliente per i contatti, per la sicurezza tecnica del sistema, per motivi di statistica, di innovazione e per il miglioramento del prodotto. I dati non vengono messi a disposizione di terzi se non per la lotta agli abusi e in caso di contenuti vietati per legge. Con la registrazione e il conseguente utilizzo del sito web lβ€™utente/cliente da il proprio <strong>consenso</strong> affinchΓ© i dati messi a disposizione possano essere <strong>elaborati e memorizzati</strong> per uso interno come consentito dalla legge sulla privacy. In caso di partecipazione a concorsi si provvederΓ  ad indicare caso per caso se i dati vengono trasmessi a terzi per permettere lo svolgimento del concorso.</p> 
    <p>I contenuti di dati trasmessi dallβ€™utente/cliente via Internet come e-mail (β€messaggi privatiβ€), SMS o simili, non vengono sottoposti al controllo della DA. La DA non Γ¨ responsabile per il contenuti di tali dati. La DA non utilizza indirizzi mail consultabili per lβ€™invio di spam (le newsletter vengono spedite esclusivamente a utenti/clienti che richiedono espressamente questo servizio v. β€Settingsβ€) e non vende indirizzi mail a chi spedisce spam. Non Γ¨ da escludersi che gli indirizzi mail consultabili pubblicamente vengano utilizzati in modo improprio da terzi, la DA tuttavia non si assume alcuna responsabilitΓ  in merito.</p> 
    <p>Il sito web contiene dei <strong>link</strong> per offerte di terzi. La DA non ha alcuna influenza sul trattamento che gli offerenti terzi fanno dei dati degli utenti raccolti sui propri siti web, lβ€™utilizzo di questi siti Γ¨ a rischio proprio dellβ€™utente/cliente, la Da non si assume alcuna responsabilitΓ  per un utilizzo improprio o la diffusione dei dati.</p> 
    <p>Per poter adattare il sito web nel miglior modo possibile alle esigenze degli utenti/clienti, in certi casi vengono inseriti dei <strong>cookies</strong> (piccoli file memorizzati nel computer dellβ€™utente/cliente per facilitare il ritrovamento del sito e il suo aggiornamento). I cookies non contengono dati personali. Configurando il browser in un certo modo Γ¨ possibile rinunciare allβ€™installazione dei cookies. Rinunciando ai cookies si puΓ² tuttavia provocare un peggioramento o lβ€™impossibilitΓ  di accedere ai servizi e alle funzioni offerti dal sito web; la DA non si assume alcuna responsabilitΓ  in merito.</p> 
    <h2>&sect; 4 Forum e recensioni</h2> 
    <p>Con la registrazione al sito web e la partecipazione a forum e recensioni lβ€™utente dichiara che i diritti dβ€™autore sono suoi e che alla DA vengono conferiti i <strong>diritti dβ€™uso</strong> per gli articoli e le recensioni. Non Γ¨ possibile garantire la riservatezza dei forum e la pubblicazione degli articoli spediti. La DA non si assume alcuna responsabilitΓ  per la correttezza dei contenuti degli articoli e non tiene alcuna corrispondenza in merito.</p> 
    <p>Il contenuto della registrazione e degli articoli in forum e recensioni non deve contravvenire alle leggi svizzere nΓ© a quelle dei paesi dove articoli/recensioni vengono immessi nella rete. Contenuti con riferimenti a pornografia infantile, sesso con animali o escrementi e tutti i dati memorizzati dei relativi utenti e clienti vengono trasmessi subito e senza preavviso alle autoritΓ  penali competenti. Lβ€™utente assicura espressamente che i suoi articoli/recensioni non contravvengono a diritti dβ€™autore, alla legislazione sulla tutela dei dati o al diritto della concorrenza e si impegna a mantenere la DA <strong>libera e indenne</strong> da rivendicazioni da parte di terzi che si basano su atti illegali dellβ€™utente o su errori nel contenuto dei suoi articoli e delle informazioni ivi messe a disposizione e in generale su infrazioni alla legge, comprese le spese giudiziarie e di difesa.</p> 
    <p>Lβ€™utente si impegna a <strong>mantenere segreti</strong> i dati di login e le password, per evitare che persone non autorizzate (in particolare minorenni) abbiano accesso al sito web.</p> 
    <p>La DA ha il diritto di cancellare senza indicare i motivi e senza preavviso qualsiasi articolo. Nel caso in cui ci siano dei terzi che rivendicano in modo attendibile i diritti per contenuti di articoli di cui Γ¨ responsabile lβ€™utente, la DA puΓ² bloccare questi articoli e i relativi dati (anche per salvare le prove).</p> 
    <p>La <strong>cancellazione</strong> di utenti registrati avviene solo su richiesta scritta e puΓ² essere richiesto un controllo dellβ€™identitΓ . Da canto suo la DA puΓ² cancellare in qualsiasi momento e senza giustificazione registrazioni indebite.</p> 
    <p>La DA si riserva il diritto, ma non Γ¨ suo dovere, di controllare il sito web e i profili, i contenuti e le offerte ivi pubblicate. Il blocco di tali dati Γ¨ esclusivamente a discrezione della DA, lβ€™utilizzo responsabile da parte dellβ€™utente/cliente rimane espressamente valida.</p> 
    <h2>&sect; 5 Inserzioni e pubblicitΓ  su banner nel sito web</h2> 
    <p>Lβ€™inserzionista, in qualitΓ  di cliente, conferisce a DA lβ€™incarico di mettergli a disposizione sul sito web, per un certo periodo, un numero desiderato di spazi per inserzioni commerciali o non commerciali (con o senza fotografia) o banner (attualmente le rubriche sono Escort, Boys, Trans, Discounted Girls, Tours, Directories interne, queste possono essere in qualsiasi momento ampliate o ridotte) in cambio del versamento del costo corrente dellβ€™annuncio. La DA si riserva il diritto di modificare in qualsiasi momento il tipo, il volume, le condizioni e i costi delle inserzioni per le scadenze future.</p> 
    <p>Le inserzioni e i banner vengono concordati per iscritto o online sulla base di accordi separati sulle inserzioni, inoltre i regolamenti contenuti in questo paragrafo valgono come parte integrante degli accordi individuali. Lβ€™attivazione di incarichi viene confermata al cliente per e-mail (se questa Γ¨ stata fornita) dopo lβ€™avvenuto pagamento della tariffa. La DA si riserva il diritto, dopo aver controllato i dati del cliente o in caso di non avvenuto pagamento della tariffa dovuta per le inserzioni, di rifiutare lβ€™incarico o di sospenderlo o stornarlo senza preavviso; ciΓ² non solleva il cliente dal pagamento delle somme contrattualmente dovute per le prestazioni in scadenza. <em>Il pagamento puΓ² essere effettuato in franchi svizzeri o euro in contanti alla stipula del contratto, mediante bonifico anticipato o carta di credito.</em></p> 
    <p>I clienti si impegnano a fornire solo dati conformi a veritΓ  per quanto riguarda la loro persona e i servizi da loro offerti, essi sono responsabili dei dati pubblicati e si impegnano a indennizzare la DA in caso di rivalsa diretta (comprese eventuali spese di difesa e penali). Se vengono riscontrate violazioni le inserzioni possono essere cancellate senza diffida fino alla loro correzione o in casi gravi, in modo permanente senza diritto di rimborso delle somme giΓ  pagate. Contenuti con riferimenti a pornografia infantile, sesso con animali o escrementi e i dati memorizzati dei relativi utenti e clienti vengono trasmessi subito e senza preavviso alle autoritΓ  penali competenti.</p> 
    <p>Per quanto concerne le inserzioni viene applicato il <strong>diritto svizzero sulle commesse, luogo dβ€™adempimento e foro competente Γ¨ Zurigo.</strong></p> 
    <h2>&sect; 6 Diritti dβ€™autore, modifica del sito web</h2> 
    <p>Tutti i file di testo, le immagini, gli elementi web e sonori, i loghi e la grafica di questo sito e di tutte le sue sottocartelle sono di proprietΓ  intellettuale della DA e sono protetti da diritti dβ€™autore. Eβ€™ vietato scaricare, copiare o utilizzare file aggirando o non rispettando le regole dβ€™uso descritte nel presente contratto. Nel caso questa clausola non venga rispettata, lβ€™utente/cliente incorre in un procedimento civile e penale con richiesta di risarcimento danni. La concessione di licenza deve essere autorizzata espressamente e per iscritto dalla DA. La DA puΓ² modificare il sito web in qualsiasi momento senza preavviso.</p> 
    <h2>&sect; 7 ResponsabilitΓ , hyperlinks</h2> 
    <p>La DA non Γ¨ tenuta in alcun momento a garantire o a ripristinare lβ€™accesso al sito, il sito web puΓ² essere disattivato in qualsiasi momento senza preavviso, anche a tempo indeterminato. Non si assume <strong>alcuna responsabilitΓ </strong> per danni causati da hardware o software difettosi o da azioni di terzi (anche in caso di negligenza) o per danni di qualsiasi tipo conseguenti allβ€™accesso (o allβ€™impossibilitΓ  di accedere). Rimane salvo il rimborso di parte dei costi delle inserzioni in caso di impossibilitΓ  di accesso per un periodo prolungato per sola responsabilitΓ  della DA (appositamente o per colpa grave); lβ€™impossibilitΓ  di accesso dovuta a manutenzione del sito non da diritto a rimborsi. Salvo ai clienti inserzionisti la DA non fornisce al alcun supporto utente.</p> 
    <p>Il sito web contiene link a siti internet esterni (cosiddetti β€hyperlinksβ€). Inserendo questi link la DA non si appropria nΓ© dei siti internet, nΓ© dei loro contenuti. Per i contenuti dei siti nei link sono responsabili solo ed esclusivamente i rispettivi offerenti. Per contenuti sconosciuti non si fornisce alcuna garanzia, neanche per danni conseguenti e per la disponibilitΓ  dei siti linkati.</p> 
    <h2>&sect; 8 Scelta giuridica, foro competente</h2> 
    <p>Queste CGC e i relativi rapporti giuridici sottostanno al diritto svizzero. Il foro competente Γ¨ Zurigo.</p> 
    <p><em>June 2006</em>, ci si riserva il diritto di apportare modifiche in ogni momento.</p> 
    <p>DA Products GmbH<br /> 
    Forchstrasse 113a<br /> 
    8127 Forch<br /> 
    Svizzera</p> 
    <p><a href='mailto:ricco171717@hotmail.com'>ricco171717@hotmail.com</a></p> 
"; 
$LNG['terni'] = "Terni"; 
$LNG['text_of_message'] = "Κείμενο μηνύματος"; 
$LNG['thank_you'] = "Ευχαριστούμε"; 
$LNG['thanks_for_registration'] = "Ευχαριστούμε για την εγγραφή! για να ολοκληρωθεί, παρακαλούμε ελένγξτε το email inbox σας και ακολουθήστε τις οδηγίες που σας στείλα με."; 
$LNG['third_party_escort_reviews'] = "Αξιολογίσεις για συνοδούς απο άλλους"; 
$LNG['thursday'] = "Πέμπτη"; 
$LNG['time'] = "Ώρα"; 
$LNG['to'] = "Σέ"; 
$LNG['to_change_use_menu'] = "Για να αλλάξετε / επεξεργαστείτε τα δεδομένα και ρυθμίσεις σας παρακαλώ χρησιμοποιείτε το προσωπικό μενού στην κορυφή."; 
$LNG['today_new'] = "Σημερινό νέο"; 
$LNG['top20'] = "Κορίτσι του μήνα"; 
$LNG['top20_desc'] = "Ψήφισε την αγαπημένη σου συνοδό και κάντην κορίτσι του μήνα! Πάντα κάθε φορά το κορίτσι με την υψηλότερη βαθμολογία θα είναι το κορίτσι του μήνα! 
<br /></br /> Ψήφισε ΤΩΡΑ! Η κλίμακα βαθμολόγισης είναι απο 1 (χειρότερο) έως 10 (καλύτερο) !"; 
$LNG['top_10_ladies'] = "Οι καλύτερες 10 γυναίκες"; 
$LNG['top_10_reviewers'] = "Κορυφαίοι 10 αξιολογιτές"; 
$LNG['tour_add'] = "Πρόσθεσε τούρ"; 
$LNG['tour_already'] = "Έχεις ήδη τούρ στην επιλεγμένη περίοδο"; 
$LNG['tour_contact'] = "Επικοινωνία τούρ"; 
$LNG['tour_duration'] = "Διάρκεια τούρ"; 
$LNG['tour_email'] = "Τούρ email"; 
$LNG['tour_empty_country'] = "Η χώρα δεν πρέπει να είναι κενή"; 
$LNG['tour_empty_from_date'] = "Απο ημερομηνία δεν πρέπει να είναι κενή"; 
$LNG['tour_empty_to_date'] = "Η ημερομηνία δεν πρέπει να είναι κενή"; 
$LNG['tour_girl'] = "Κορίτσι τούρ"; 
$LNG['tour_location'] = "Περιοχές τούρ"; 
$LNG['tour_lower_date'] = "Η επιλεγμένη ημερομηνία δεν πρέπει να είναι πιο πρίν απο την υπάρχον"; 
$LNG['tour_no_escorts'] = "Παρακαλώ, προσθέστε προφίλ συνοδού πρώτα "; 
$LNG['tour_phone'] = "Τηλέφωνο τούρ"; 
$LNG['tour_saved'] = "Δεδομένα τούρ αποθηκέυτικαν"; 
$LNG['tour_update'] = "Ανανέωση τούρ"; 
$LNG['tour_updated'] = "Δεδομένα τούρ ανανεώθηκαν"; 
$LNG['tours'] = "City tours"; //Πόλεις τούρς 
$LNG['trans'] = "Τράνς"; 
$LNG['trial'] = "Δοκιμαστικό"; 
$LNG['trial_mb_desc'] = " 
<p>Έχετε πρόσβαση στη δοκιμαστική περίοδο στο ".NGE_SITE_TITLE.". Σε αυτή την περίοδο, μπορείτε να έχετε πρόσβαση στις παρακάτω λειτουργίες:</p> 
<ul> 
    <li>προφίλ στην κύρια σελίδα, και στην σελίδα πόλης που έχει επιλεχθεί</li> 
    <li>κάθε χρήστης μπορέι να γράψει αξιολογίσεις για εσένα</li> 
    <li>συμμετοχή στο Top10 των αξιολογίσεων και Κορίτσι του Μήνα</li> 
    <li>συμμετοχή στις εναλλαγές προφίλ, έτσι το προφίλ σου δεν θα είναι στο τέλος της λίστας</li> 
</ul> 
<p>και Άλλα !</p> 
"; 
$LNG['trial_mb_desc2'] = " 
<p>Μετά τη δοκιμαστική περίοδο χρειάζεται να αγοράσετε Premium Χώρο να κρατήσετε τα διαθέσιμα χαρακτηριστικά, επίσης να πάρετε <strong style='color: red;'>Ειδικές προσφορές</strong> ! 
<ul> 
    <li>μπορείτε να χρησιμοποιήσετε τις επιλογές του της Πόλης Τούρ, ανοίγοντας πολλά νέα εργαλεία διαφήμισης</li> 
    </ul> 
<p><a href='mailto:".NGE_CONTACT_EMAIL."'>Κάντε κλίκ εδώ για παραγγελία!</a></p> 
"; 
$LNG['trial_mb_desc_ag'] = "%COUNT% συνοδοί (1η λήγει σε %DAYS% μέρες)"; 
$LNG['trustability'] = "Εμπιστευτικότητα"; 
$LNG['trusted_review_popup'] = " 
    <p>Παρέχετε λεπτομερείς πληροφορίες για το ραντεβού σε εμάς, παρέχεουμε μετέπειτα τις πληροφορίες αυτές απευθειάς στην κοπέλα. Η κοπέλα δεν μπορεί να δεί την αξιολόγιση σας ούτε μέτα μπορεί να μάθει έαν ήταν θετική ή αρνητική η αξιολόγιση σας, η αξιολόγιση θα φανεί σαν 100% ψεύτικη.</p> 
    <p>Παρακαλούμε δώστε μας πληροφορίες για εσάς και για το μέρος που θα συναντήσετε την κοπέλα, σημειώστε ότι αυτές τις πληροφορίες θα τις λάβει μόνο η κοπέλα με sms ή mail. Τα δεδομένα σας στο site δε θα κρατηθούν. Παράδειγμα κάνατε book με αλήθινό ή ψεύτικο όνομα παρακαλούμε χρησιμοποιήστε τό ίδιο την ίδια στιγμή, εάν κάνατε το book με e-mail, χρησιμοποιήστε το email κτλ.</p> 
    <p>Παραδείγματα:</p> 
    <ol> 
        <li>Γιάννης escortforum@hotmail.com, Δευτέρα 17.Ιανουαρίου 06 20:00μμ</li> 
        <li>pinky 333 333 454, Τετάρτη 16. Φεβρουαρίου. 05 γύρω στις 08:00πμ στο ξενοδοχείο ibis milano</li> 
    </ol>"; 
$LNG['trusted_user_mail_subject'] = "%SITE% έμπιστος χρήστης"; 
$LNG['trusted_user_mail_text'] = "<pre> 
Κύριε %NAME%, 
σας ευχαριστούμε που γράψατε στο %SITE% ! 
Είστε έμπιστος χρήστης πιά, και η επαλήθευση απο τον διαχειριστή για τις δημοσιεύσεις σας δεν χρειάζεται. 
Σας καλωσορίζουμε στην κοινότητα μας και σας ευχόμαστε να έχετε ευχάριστη ώρα εδώ! 
%SITE% Team.</pre>"; 
$LNG['tuesday'] = "Τρίτη"; 
$LNG['tuscany'] = "Tuscany"; 
$LNG['type'] = "Είδος"; 
$LNG['ugly'] = "άσχημο"; 
$LNG['unfriendly'] = "Μή φιλική"; 
$LNG['unsubscribe'] = "Διαγραφή"; 
$LNG['upcoming_tours'] = "Επόμενα τούρς"; 
$LNG['update_languages'] = "Ανανέωση γλώσσας"; 
$LNG['update_locations'] = "Ανανέωση περιοχών"; 
$LNG['update_rates'] = "Ανανέωση εκτιμήσεων"; 
$LNG['upg_esc_memb'] = "Αναβάθμιση συνδρομής"; 
$LNG['upg_esc_memb_desc'] = "Ευχαριστούμε για την απόφαση σας να αναβαθμίσετε την συνδρομή σας στο".NGE_SITE_TITLE." !<BR /> 
Παρακαλούμε επιλέξτε το είδος και την διάρκεια της συνδρομής που θέλετε να αγοράσετε, και μετά κάντε κλίκ στο \"<B>ΑΝΑΒΑΘΜΙΣΗ ΤΩΡΑ</B>\" κουμπί 
να μεταφερθείτε στην σελίδα πληρωμών:"; 
$LNG['upgrade_membership'] = "Αναβάθμηση συνδρομής"; 
$LNG['upgrade_now'] = "ΑΝΑΒΑΘΜΙΣΗ ΤΩΡΑ!"; 
$LNG['upload_failed'] = "Αποτυχημένο ανέβασμα φωτογγραφίας."; 
$LNG['upload_picture'] = "Ανεβάστε φωτογραφία"; 
$LNG['upload_private_photo'] = "Ανέβασε πρωσοπικές φωτογραφίες"; 
$LNG['url'] = "URL"; 
$LNG['user'] = "Χρήστης"; 
$LNG['username'] = "Όνομα χρήστη"; 
$LNG['users_match'] = "Χρήστες που ταιριάζουν με τα κριτήρια"; 
$LNG['vacation_add'] = "Πρόσθεσε ταξίδι"; 
$LNG['vacation_end'] = "Επιστροφή απο ταξίδι"; 
$LNG['vacation_no_escorts'] = "Παρακαλώ, προσθέστε το προφίλ συνοδού πρώτα"; 
$LNG['vacation_start'] = "Έναρξη ταξιδιού"; 
$LNG['vacations'] = "Ταξίδια"; 
$LNG['vacations_active'] = "%NAME% είναι σε ταξίδι <strong>από %FROM_DATE% έως %TO_DATE%</strong>"; 
$LNG['vacations_active_soon'] = "%NAME% είναι σε ταξίδι, θα επιστρέψει <strong>σύντομα</strong>"; 
$LNG['vacations_currently_date'] = "Η κοπέλα βρίσκεται αυτή τη στιγμή σε ταξίδι, θα επιστρέψει στις %DATE%."; 
$LNG['vacations_currently_soon'] = "Η κοπέλα βρίσκεται αυτή τη στιγμή σε ταξίδι, θα επιστρέψει <strong>σύντομα</strong>."; 
$LNG['vacations_empty_to_date'] = "Η ημερομηνία επιστροφής απο το ταξίδι δεν πρέπει να είναι κενή"; 
$LNG['invalid_vacation_dates'] = "Vacation dates are not valid"; 
$LNG['vacations_lower_date'] = "Η ημερομηνία έναρξης δεν πρέπει να είναι μικρότερη απο την υπάρχον"; 
$LNG['vacations_return'] = "Επιστροφή απο ταξίδι"; 
$LNG['verification_email'] = "Κύριε %USER%, σας ευχαριστούμε για την εγγραφή σας στο %SITE%. Για να επιβεβαιώσετε την εγγραφή σας παρακαλώ κάντε κλίκ στον παρκάτο σύνδεσμο:"; 
$LNG['verification_email_subject'] = NGE_SITE_TITLE." εξακρίβωση λογαριασμού"; 
$LNG['very_simple'] = "Πολύ απλά"; 
$LNG['view_gallery'] = "Δές γκαλερί"; 
$LNG['view_my_private_photos'] = "Δές τις ιδιωτικές φωτογραφίες μου"; 
$LNG['view_my_profile'] = "Δές το προφίλ μου"; 
$LNG['view_my_public_photos'] = "Δές τις δημόσιες φωτογραφίες μου"; 
$LNG['view_profile'] = "Δες το προφίλ"; 
$LNG['view_reviews'] = "Πρόσθεσε/δές αξιολογίσεις"; 
$LNG['view_site_escort_reviews'] = "Δείτε %SITE% αξιολογίσεις συνοδών"; 
$LNG['view_third_party_escort_reviews'] = "Δείτε αξιολογίσεις συνοδών απο άλλους"; 
$LNG['vip'] = "VIP"; 
$LNG['vip_mb_desc_ag'] = "%COUNT% συνοδοί"; 
$LNG['vote'] = "Ψήφος"; 
$LNG['vote_her_looks'] = "Ψήφισε την κοπέλα"; 
$LNG['votes'] = "Ψήφοι"; 
$LNG['waist'] = "Μέση"; 
$LNG['watch_list'] = "Λίστα με αγαπημένα"; 
$LNG['web'] = "Web"; 
$LNG['wednesday'] = "Τετάρτη"; 
$LNG['week'] = "Εβδομάδα"; 
$LNG['weeks'] = "Εβδομάδες"; 
$LNG['weight'] = "Κιλά"; 
$LNG['welcome'] = "Καλώς ήρθατε στο ".NGE_SITE_TITLE; 
$LNG['whats_new'] = "Τι καινούριο ;"; 
$LNG['when_you_met_her'] = "Πότε την συνάντησες"; 
$LNG['when_you_met_her_desc'] = "μέρα και ώρα"; 
$LNG['when_you_met_lady'] = "Όταν συναντήσεις την γυναίκα"; 
$LNG['where_did_you_meet_her'] = "Πού την συνάντησες"; 
$LNG['where_you_met_her'] = "Πού την συνάντησες"; 
$LNG['where_you_met_her_desc'] = "ξενοδοχείο, στον χώρο της, στον χώρο μου"; 
$LNG['white'] = "Άσπρα"; 
$LNG['whole'] = "Ολόκληρο"; 
$LNG['wlc_city'] = "Περιοχή εργασίας (πόλη)"; 
$LNG['wlc_country'] = "Περιοχή(Χώρα)"; 
$LNG['women'] = "Γυναίκες"; 
$LNG['working_locations'] = "Περιοχές"; 
$LNG['working_time'] = "Ώρες εργασίας"; 
$LNG['write_here_name_of_site'] = "Παρακαλώ προσθέστε στα παρακάτω κενά το όνομα και την διεύθυνση του site που η κοπέλα είναι στην λίστα του."; 
$LNG['wrong_login'] = "Σφάλμα σύνδεσης"; 
$LNG['wrong_login_or_password'] = "Λάθος όνομα χρήστη ή κωδικός"; 
$LNG['wrong_time_interval'] = "Λάθος ώρα μεσολαβεί για ώρα εργασίας την %DAY%."; 
$LNG['year'] = "Χρόνος"; 
$LNG['years'] = "Έτη"; 
$LNG['yellow'] = "Κίτρινα"; 
$LNG['yes'] = "Ναί"; 
$LNG['yes_more_times'] = "Ναί, πολλές φορές"; 
$LNG['you_are_premium'] = "Είστε Premium μέλος"; 
$LNG['you_have_log'] = "Πρέπει να είστε συνδεδεμένος για να χρησιμοποιήσετε αυτή την λειτουργεία. <a href='%LINK%'>Κάντε κλίκ εδώ για σύνδεση</a> ή <a href='%LINK2%'>Εγγραφείτε εδώ.</a>"; 
$LNG['you_have_voted_for'] = "Έχετε ψηφήσει για την %ESCORT%! Ευχαριστώ."; 
$LNG['you_r_watching_agencies'] = "Αυτά τα <b>πρακτορεία</b> είναι στην λίστα με τα αγαπημένα:"; 
$LNG['you_r_watching_escorts'] = "Αυτοί οι <b>συνοδοί</b> είναι στην λίστα με τα αγαπημένα:"; 
$LNG['you_r_watching_users'] = "Αυτοί οι <b>χρήστες</b> είναι στην λίστα με τα αγαπημένα:"; 
$LNG['your_booking_text'] = "Κείμενο book"; 
$LNG['your_data'] = "Τα δεδομένα σου"; 
$LNG['your_discount'] = "Η έκπτωση σας: %DISCOUNT%%"; 
$LNG['your_full_name'] = "Ολόκληρο το όνομα"; 
$LNG['your_info'] = "Οι πληροφορίες σου"; 
$LNG['your_info_desc'] = "e-mail, τηλέφωνο, όνομα"; 
$LNG['your_login_name'] = "Όνομα σύνδεσης"; 
$LNG['your_name'] = "Το όνομα σας"; 
$LNG['your_report'] = "Η αναφορά σας"; 
$LNG['zip'] = "Ταχυδρομικός κώδικας"; 
$LNG['69'] = "69"; 
// 2006/12/11 
$LNG['tours_in_other_countries'] = "Tours σε άλλες χώρες"; 
$LNG['brunette'] = "Καστανά σκούρα"; 
$LNG['hazel'] = "Καστανά ανοιχτά"; 
$LNG['profile_or_main_page_links_only'] = "Βάλτε links μόνο στο προφίλ σας η στη κεντρική σας σελίδα."; 
$LNG['competitors_links_will_be_deleted'] = "Έμμεσες διαφημίσεις και link θα διαγράφονται!"; 
$LNG['domain_blacklisted'] = "Ιστοσελίδα μπλοκαρισμένη"; 
$LNG['phone_blocked'] = "Νούμερο μπλοκαρισμένο"; 
$LNG['girls'] = "Κορίτσια"; 
$LNG['inform_about_changes'] = "Thank you. We will inform you about changes on " . NGE_SITE_TITLE; 
$LNG['escort_photos_over_18'] = "All escorts were 18 or older at the time of depiction."; 
$LNG['captcha_please_enter'] = "Please enter"; 
$LNG['captcha_here'] = "here"; 
$LNG['captcha_verification_failed'] = "Text verification failed."; 
$LNG['my_awards'] = "My Awards"; 
$LNG['escort_girls'] = "Escort Girls"; 
$LNG['locally_available_in_countries'] = "We are also locally available in these countries"; 
$LNG['domina'] = "Domina"; 
$LNG['dominas'] = "Dominas"; 
$LNG['club'] = "εταιρεία"; 
$LNG['club_name'] = "Όνομα εταιρείας"; 
$LNG['club_profile'] = "Προφίλ εταιρείας"; 
$LNG['studio'] = "Studio"; 
$LNG['studio_girls'] = "Studio Girls"; 
$LNG['all_regions'] = "All regions..."; 
$LNG['see_all_escorts_of_this_agency'] = "See all escorts of this agency"; 
$LNG['see_all_escorts_of_this_club'] = "See all escorts of this club"; 
$LNG['invalid_height'] = "Invalid height"; 
$LNG['invalid_weight'] = "Invalid weight"; 
$LNG['subscribe_to_newsletter'] = "Subscribe to our newsletter!"; 
$LNG['inform_every_week'] = "We will inform you every week about escorts on " . NGE_SITE_TITLE; 
$LNG['clubs_match'] = "Clubs matching criteria"; 
switch (APPLICATION_ID) 
{ 
    case 1: // escortforumit.com 
        $LNG['index_bottom_text'] = "Da noi trovi le piu belle accompagnatrici, escort, girl, annunci, di tutta Italia, inoltre abbiamo annunci di escort, girl, accompagnatrici, nelle maggiori città d'Italia Roma, Milano, Torino, Bologna, Napoli, Firenze! Cosa aspetti contatta le accompagnatrici piu belle che non hai mai trovatoa Roma, Bologna, Firenze, Napoli, Milano, Torino, e in tutte le altre città d'Italia. Troverai sempre gli annunci, escort, girl e le accompagnatrici piu belle ditutta Italia sempre aggiornate."; 
        break; 
    case 2: // escort-annonce.com 
        $LNG['index_bottom_text'] = "Bienvenue dans Escort annonce, le plus grand annuaire d'annonces de rencontres d'Escorts girl /boys/ trans en France et en Europe. Vous trouverez des Escort à Paris Toulouse Lyon Marseille et toutes les grandes villes de France où vous pourrez faire une rencontre sexe entre adultes consentants. Vous pourrez aussi prendre connaissance des appréciations sur les Escort girl rencontrées par les autres membres, et aussi le tchat pour voir en direct via la webcam l'Escort girl de votre choix avant de la rencontrer. Pour une rencontre furtive d'une Escort girl après le bureau, Escort annonce est le site où vous pourrez revenir plusieurs fois par jour pour voir les nouvelles girls inscrites près de chez vous pour des rencontres les plus chaudes. Si vous rencontrez ou voyez une annonce d'escort qui ne correspond pas à la réalité, n'hésitez pas nous contacter, nous ferons notre possible pour n'avoir que des vraies annonces pour faire de vraies rencontres coquines."; 
        break; 
    case 3: // hellasescorts.com 
        $LNG['index_bottom_text'] = "Στο " . NGE_SITE_TITLE . " θα βρείτε callgirl και τις πιο καυτές και σέξυ συνοδούς & από την Αθήνα, συνοδούς από Θεσσαλονίκη, συνοδούς από Πάτρα και όλες τις μεγάλες πόλεις της Ελλάδας."; 
        break; 
         
    case 4: // escortchicas.com 
        $LNG['welcome_description'] = "We are the largest Network for erotic contacts in Europe! Find today your escapade, your companionship, your sexual adventure – in Spain and Europe! We update our site daily. All girls with pictures and contact details! You can register for FREE and benefit from many advantages! If you have any question or uncertainity, please contact us directly: <a href='mailto:" . NGE_CONTACT_EMAIL . "'>" . NGE_CONTACT_EMAIL . "</a>"; 
        $LNG['index_bottom_text'] = "Estàs buscando relax en barcelona o un relax en madrid? En nuestro portal tenemos contactos escorts en todas las ciudades de Espana, relax en valencia, relax en sevilla o en las islas tambien tenemos anuncios de relax en mallora, ibizao las islas canarias. Diferentes anuncios escorts de agencias, escorts independenties y travestis. Si no buscas los que quieres encontrar en otros portales aqui lo encuentraras seguramente. Mira los anuncios y encuentra en tu ciudad las mas calientes mujeres."; 
        break; 
    case 6: // sexindex.ch 
        $LNG['welcome_description'] = "dem grössten Netzwerk für erotische Kontakte, Sex Clubs und Escort in Europa! Auf der Suche nach einem Bordell, einer Ferienbegleitung, einem Seitensprung, einem Escort Girl oder guten Sex Clubs? Bei uns werden Sie fündig - täglich aktuelle Kontaktanzeigen (Setcards) mit Fotos und Bewertungen. Melden Sie sich GRATIS an und profitieren Sie von vielen Vorteilen und Vergünstigungen! Bei Fragen oder Problemen wenden Sie sich bitte direkt an: <a href='mailto:" . NGE_CONTACT_EMAIL . "'>" . NGE_CONTACT_EMAIL . "</a>"; 
        $LNG['advertise_now'] = "<a href='" . NGE_URL . "/advertising.php' rel='nofollow'>Werben Sie gratis auf Sexindex.ch!</a> Auskunft unter <a href='mailto:" . NGE_CONTACT_EMAIL . "'>" . NGE_CONTACT_EMAIL . "</a> oder Telefon 078 899 14 89"; 
        $LNG['autumn_action'] = " 
        <div style='overflow: hidden; width: 755px; padding: 5px 10px; font-size: 10px; line-height: 1.1; background: #f5f5f5; border: 1px solid #ccc;'> 
            <div style='float: left; width: 360px;'> 
                <p><big>Sexindex.ch Herbst Aktion<br /> 
                <span style='color: #DA1E05;'>Werbung die sich auszahlt – über 120,000 Besucher pro Monat</span></big></p> 
                <p>Profitieren Sie von unserer Aktion und schalten Sie noch heute Ihre Premium Sexanzeige<br /> 
                (Premium = Topplatzierung, immer an oberster Stelle gelistet).</p> 
                <p style='color: #DA1E05;'>15 TAGE PREMIUM INSERAT CHF 100.00<br /> 
                30 TAGE PREMIUM INSERAT CHF 200.00</p> 
            </div> 
            <div style='margin: 0 0 0 385px;'> 
                <p>Es geht ganz einfach. Senden Sie uns folgende Angaben per <a href='mailto:" . NGE_CONTACT_EMAIL . "'>Email</a> zu und Ihre Sedcard wird umgehend aufgeschaltet:</p> 
                <ul> 
                    <li>Dauer des Inserates</li> 
                    <li>Ihre Fotos und Kontaktdaten (Telefon und/oder E-Mail)</li> 
                    <li>Gewünschter Name und Inseratetext</li> 
                    <li>Ihr Arbeitsort / Stadt</li> 
                    <li>Ihre Webseite (URL) (optional)</li> 
                </ul> 
                <p>Für mehr Informationen oder Fragen zu diesem Angebot oder zur normalen Gratiswerbung auf Sexindex.ch kontaktieren Sie uns wie gewohnt per <a href='mailto:" . NGE_CONTACT_EMAIL . "'>Email</a> oder Telefon 078 899 14 89.<br /> 
                Das Inkasso erfolgt nachträglich (bar oder Banküberweisung).</p> 
            </div> 
        </div> 
        "; 
        $LNG['escort_girls_description'] = "All diese attraktiven Ladies besuchen Sie zu Hause, im Büro oder im Hotel. Oder sind Sie auf der Suche nach einer Ferienbegleitung? Swingerclubbegleitung? Hier werden Sie sicher fündig! Täglich aktuell."; 
        $LNG['independent_girls_description'] = "Auf der Suche nach einem ganz diskreten Abenteuer? Hier inserieren Girls die Sie in absolut privater Atmosphäre empfangen – entweder in einer Privatwohnung oder einem kleinen Salon ohne Bar/Empfang. Diskret und privat!"; 
        $LNG['studio_girls_description'] = "Täglich aktuell finden Sie hier die heissesten Girls aus den besten Etablissements der ganzen Schweiz."; 
        $LNG['index_bottom_text'] = "Gemäss Statistik sind Begriffe die etwas mit Sex und Erotik zu tun haben die meist gesuchten im Internet. 98 Prozent der Männer waren gemäss Umfragen schon mindestens einmal auf Pornoseiten. Beliebt sind auch Suchbegriffe wie Kontaktanzeigen, Bordell, Sex Clubs, Escort, ficken, Transen etc. In der Schweiz gibt es eine Vielzahl an Sex Seiten und Sexführern. Und manchmal ist es schwierig den Ueberblick zu behalten. Denn wirklich gute Sex-Angebote, wo man auch das findet was man sucht, sind rar. Oder ist es Ihnen nicht auch schon so ergangen? Sie tippen bei einer Suchmaschine den Begriff „Bordell“ ein und landen, statt auf einem Sexführer, auf einer kostenpflichtigen Seite mit Kontaktanzeigen? Oder Sie haben Lust auf schnellen Sex und suchen ein Bordell in Ihrer Nähe wo Sie richtig geil ficken können? Statt dessen werden Sie auf eine Seite geführt wo Sie nur ein kleines Sexangebot haben und wenige Escort Girls / Sex Clubs aufgeführt sind. Wie gesagt – es ist ziemlich schwierig. Einige Seiten haben sich etabliert und decken gewisse Sparten ab. Für Transen Kontakte zum Beispiel eignet sich das Portal happysex hervorragend. Denn happysex hat in den meisten Regionen der Schweiz Kontaktanzeigen bzw. Sexanzeigen von Transen. Auch Bordelle, Escort Girls und Sex Clubs werden aufgeführt. Jedoch nimmt happysex im direkten Vergleich zu anderen Sex Seiten vom Design und Aufbau her einer der hinteren Plätze ein. Bei uns auf Sexindex legen wir wert auf Aktualität der Sex Kontakt Anzeigen und Sedcards der Escort Girls. Wir nehmen regelmässig neue Bordelle, Sexstudios, private Frauen, Escorts und Salons in unsere Datenbank auf. Alle Sedcards sind mit Fotos und Details und sind einheitlich präsentiert. Auch die Werbung versuchen wir dezent zu halten – jedoch brauchen wir die Banner um für Sie weiterhin ein gratis Sexführer zu bleiben. In diesem Sinne – ficken ist die schönste Nebensache der Welt – und ob Sie auf sexindex oder happysex fündig werden spielt schlussendlich keine Rolle. Happy hunting!"; 
        break; 
} 
$LNG['escort_girls_in_city'] = "Escort Girls in %CITY%"; 
$LNG['independent_girls_in_city'] = "Private Girls in %CITY%"; 
$LNG['studio_girls_in_city'] = "Studio Girls in %CITY%"; 
$LNG['new_arrivals_in_city'] = "New Girls in %CITY%"; 
$LNG['all_girls_in_city'] = "All girls in %CITY%"; 
$LNG['see_all_girls_from_agency'] = "click to see all girls from this agency"; 
$LNG['see_all_independent_girls'] = "click to see all independent girls"; 
$LNG['last_modified'] = "Last modified"; 
$LNG['advertise_with_us'] = "Advertise with us"; 
$LNG['nearest_cities_to_you'] = "Nearest cities to you"; 
$LNG['nearest_cities_to_city'] = "Nearest cities to %CITY%"; 
$LNG['cities_in_region'] = "Cities in %REGION%"; 
$LNG['local'] = "Local"; 
$LNG['local_directory'] = "Club/Escort listing"; 
$LNG['exchange_banners'] = "If do you want interchange banners and/or links with us, email us to <a href='mailto:" . NGE_CONTACT_EMAIL . "'>" . NGE_CONTACT_EMAIL . "</a>"; 
$LNG['our_network_in_europe'] = "Our network in Europe"; 
$LNG['agencies_and_independent_in_spain'] = "Agencies and independent girls in Spain"; 
$LNG['directories_and_guide_in_spain'] = "Directories, forum and erotic guide in Spain"; 
$LNG['friends'] = "Friends"; 
$LNG['escort_service'] = "Escort service"; 
$LNG['studio_and_private_girls'] = "Studio and private girls"; 
$LNG['advertising'] = "Advertising"; 
$LNG['premium_girls'] = "Premium girls"; 
$LNG['submit_form_to_signup'] = "Submit this form to signup and get Promoted from largest escort directory!"; 
$LNG['signup_now_to_largest_escort_directory'] = "Signup now for <span class='pink'>FREE</span> to the largest escort directory worldwide. We currently have over <span class='pink'>3 million clients</span> monthly on our network. Start using our popularity to promote yourself now <span class='pink'>FOR FREE!</span>"; 
$LNG['signing_on_all_local_sites'] = "By signing up, your profile will automatically be displayed on all these local country sites. Your login data will work on all the sites - you just have to signup once."; 
$LNG['please_choose_your_business'] = "Please choose your business"; 
$LNG['independent_privat'] = "Independent Escort / Privat Girl"; 
$LNG['escort_agency'] = "Escort Agency"; 
$LNG['club_bordell_strip'] = "Club / Bordell / Sauna Club / Stripclub (Tabledance), Massage Studio"; 
$LNG['please_fill_form'] = "Please fill out the following form"; 
$LNG['please_write_numbers_from_field'] = "Please write the numbers like in the field"; 
$LNG['just_moment_away_from_escort_directory'] = "You are just moment away from having access to largest escort directory!"; 
$LNG['choose_your_region'] = "Choose your region"; 
$LNG['setcards_online'] = "%COUNT% setcards online"; 
$LNG['only_escorts'] = "only escorts"; 
$LNG['only_private_girls'] = "only private girls"; 
$LNG['only_studio_girls'] = "only studio girls"; 
$LNG['most_popular_cities'] = "Most popular cities"; 
$LNG['renew_your_profile'] = "Your profile is expired. If you would like to renew it, contact your Sales person or write us at <a href='mailto:" . NGE_CONTACT_EMAIL . "'>" . NGE_CONTACT_EMAIL . "</a>"; 
$LNG['relax'] = "Relax"; 
$LNG['email_unsubscribed'] = "E-mail %EMAIL% is removed from " . NGE_SITE_TITLE . " newsletter."; 
$LNG['vip_girls'] = "VIP girls"; 
$LNG['crop_photo'] = "Crop photo"; 
$LNG['show_thumbnails_on'] = "Show thumbnails on"; 
$LNG['photo_was_cropped'] = "Photo was cropped."; 
$LNG['crop_feature_teaser'] = " 
    <p><strong style='color: red;'>ATTENTION:</strong><br /> 
    We have launched a new feature. All photos on the main page and in the international directory have now the same size. Therefore we would like to ask you to verify your selected main picture. Follow the instructions in escort &quot;edit gallery&quot; section.</p> 
"; 
$LNG['crop_feature_description'] = " 
    <p><strong style='color: red;'>ATTENTION:</strong><br /> 
    With our new feature, all MAIN pictures appear in the same size on our site. This gives the site a much more professional look.</p> 
    <p>How it works:</p> 
    <ol> 
        <li>Check now your main photo. If everything looks fine with the picture, you don't have to proceed to step 2. If anything looks wrong (i.e. head cut away), follow the instructions in step 2.</li> 
        <li>Click on &quot;crop photo&quot;</li> 
        <li>In the middle of the photo you now see a frame which is a preview of how the picture will look like on the main page. You can move the frame around until you have chosen the part of the photo you want. Click on &quot;crop the image&quot; button.</li> 
        <li>You are already done. You can review the photo on the edit gallery main page. If it looks alright, you are set.</li> 
    </ol> 
"; 
$LNG['we_only_sell_advertisement'] = NGE_SITE_TITLE . " is an advertising and information resource, and as such has no connection or liability with any of the sites or individuals mentioned here. We ONLY sell advertisment space, we are not an escort agency, nor we are in any way involved in escorting or prostitution business. We take no responsibility for the content or actions of third party websites or individuals that you may access following links, email or phone contacts from this portal."; 
$LNG['user_messages_review_waiting_for_approval'] = "Your review about %MESSAGE% is waiting for verification. Please be patient"; 
$LNG['user_messages_favorite_girl_review'] = "Your favorite girl <strong>%MESSAGE%</strong> has new review. <a href='%LINK%'>click here to see</a>"; 
$LNG['user_messages_review_approval'] = "Your review <strong>%MESSAGE%</strong> was approved by administrator."; 
$LNG['user_messages_review_deleted'] = "Your review <strong>%MESSAGE%</strong>  was deleted by administrator. Reason given: %LINK%"; 
$LNG['user_messages_new_discount'] = "%MESSAGE% is offering you a special EF discount. <a href='%LINK%'>Check her out!</a>"; 
$LNG['user_messages_deleted_discount'] = "%MESSAGE% is not offering you a special EF discount anymore."; 
$LNG['user_messages_favorite_disabled'] = "Your favorite girl <strong>%MESSAGE%</strong> is no longer with us! We are sorry! Maybe time to find a new favorite?"; 
$LNG['user_messages_favorite_activated'] = "Your favorite girl <strong>%MESSAGE%</strong> joined us again!"; 
$LNG['user_messages_girl_of_month'] = "Girl of the month %MONTH%/%YEAR% is: <strong>%MESSAGE%</strong>!! Congratulation %MESSAGE%!!!"; 
$LNG['user_messages_free_fuck_winner'] = "<strong>You are the WINNER of the freefuck lottery contact us.</strong>"; 
$LNG['user_messages_free_fuck_others'] = "The winner of the free fuck lottery of month %MONTH%/%YEAR% is: <strong>%MESSAGE%</strong>!! Congratulation %MESSAGE%!!!"; 
$LNG['user_messages_favorite_region_girls'] = "New girls from your favourite region joined EF since your last visit. %LINK%"; 
$LNG['user_messages_favorite_region_vip'] = "There is new VIP %MESSAGE% from your favourite region since your last visit. %LINK%"; 
$LNG['user_email_review_approved'] = "Hello %TARGET_0%\nYour review %TARGET_1% was approved by administrator.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['escort_email_review_approved'] = "Hello %TARGET_0%\nReview %TARGET_1% about you was approved by administrator.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_favorites_review_approved'] = "Hello %TARGET_0%\nYour favorite girl  %TARGET_1% has new review.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['agency_email_review_approved'] = "Hello %TARGET_0%\nReview about %TARGET_1% was approved by administrator.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_review_approval_favorites'] = NGE_SITE_TITLE . " new review"; 
$LNG['user_email_subject_review_approval'] = NGE_SITE_TITLE . " review approved"; 
$LNG['escort_email_subject_review_approval'] = NGE_SITE_TITLE . " review approved"; 
$LNG['agency_email_subject_review_approval'] = NGE_SITE_TITLE . " review approved"; 
$LNG['escort_email_review_new'] = "Hello %TARGET_0%\nNew review %TARGET_1% about you is waiting for approval.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['agency_email_review_new'] = "Hello %TARGET_0%\nYour escort %TARGET_1% has new review waiting for approval.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_review_new'] = "Hello %TARGET_0%\nYour review about %TARGET_1% is waiting for verification. Please be patient.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_review_new'] = NGE_SITE_TITLE . " review waiting for approval"; 
$LNG['escort_email_subject_review_new'] = NGE_SITE_TITLE . " new review waiting for approval"; 
$LNG['agency_email_subject_review_new'] = NGE_SITE_TITLE . " new review waiting for approval"; 
$LNG['user_email_review_deleted'] = "Hello %TARGET_0%\nYour review <strong>%TARGET_1%</strong>  was deleted by administrator. Reason given: %TARGET_2%\n" . NGE_SITE_TITLE . " Team"; 
$LNG['escort_email_review_deleted']	= "Hello %TARGET_0%\nThe review <strong>%TARGET_1%</strong> about you was deleted by administrator. Reason given: %TARGET_2%\n" . NGE_SITE_TITLE . " Team"; 
$LNG['agency_email_review_deleted']	= "Hello %TARGET_0%\nThe review about <strong>%TARGET_1%</strong> was deleted by administrator. Reason given: %TARGET_2%\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_review_delete'] = NGE_SITE_TITLE . " review deleted"; 
$LNG['escort_email_subject_review_delete'] = NGE_SITE_TITLE . " review deleted"; 
$LNG['agency_email_subject_review_delete'] = NGE_SITE_TITLE . " review deleted"; 
$LNG['user_email_discount_new'] = "Hello %TARGET_0%\n%TARGET_1% is offering you a special EF discount.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['escort_email_discount_new'] = "Hello %TARGET_0%\nNew discount was assigned to you.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['agency_email_discount_new'] = "Hello %TARGET_0%\n%TARGET_1% is now offering a special EF discount.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_discount_new'] = NGE_SITE_TITLE . " new discount"; 
$LNG['escort_email_subject_discount_new'] = NGE_SITE_TITLE . " new discount"; 
$LNG['agency_email_subject_discount_new'] = NGE_SITE_TITLE . " new discount"; 
$LNG['user_email_discount_deleted']	= "Hello %TARGET_0%\nYour escort %TARGET_1% is not offering a special EF discount anymore.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_discount_deleted'] = NGE_SITE_TITLE . " discount expired"; 
$LNG['user_email_escort_deactivated'] = "Hello %TARGET_0%\nYour favorite girl <strong>%TARGET_1%</strong> is no longer with us! We are sorry! Maybe time to find a new favorite?\n" . NGE_SITE_TITLE . " Team"; 
$LNG['escort_email_escort_deactivated'] = "Hello %TARGET_0%\nYour account has been deactivated.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['agency_email_escort_deactivated'] = "Hello %TARGET_0%\nAccount of %TARGET_1% has been deactivated.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_favorites_deactivated'] = NGE_SITE_TITLE . " escort deactivated"; 
$LNG['escort_email_subject_favorites_deactivated'] = NGE_SITE_TITLE . " account deactivated"; 
$LNG['agency_email_subject_favorites_deactivated'] = NGE_SITE_TITLE . " account deactivated"; 
$LNG['user_email_escort_activated'] = "Hello %TARGET_0%\nYour favorite girl <strong>%TARGET_1%</strong> joined us again!\n" . NGE_SITE_TITLE . " Team"; 
$LNG['escort_email_escort_activated'] = "Hello %TARGET_0%\nYour account has been activated.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['agency_email_escort_activated'] = "Hello %TARGET_0%\nAccount of %TARGET_1% has been activated.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['agency_email_subject_favorites_activated'] = NGE_SITE_TITLE . " escort activated"; 
$LNG['agency_email_subject_favorites_activated'] = NGE_SITE_TITLE . " account activated"; 
$LNG['agency_email_subject_favorites_activated'] = NGE_SITE_TITLE . " account activated"; 
$LNG['user_email_girl_of_the_month'] = "Hello %TARGET_0%\nGirl of the month %TARGET_1% is: <strong>%TARGET_2%</strong>!! Congratulation %TARGET_2%!!!\n" . NGE_SITE_TITLE . " Team"; 
$LNG['escort_email_girl_of_the_month'] = "Hello %TARGET_0%\nGirl of the month %TARGET_1% is: <strong>%TARGET_2%</strong>!! Congratulation %TARGET_2%!!!\n" . NGE_SITE_TITLE . " Team"; 
$LNG['escort_email_girl_of_the_month_winner'] = "Hello %TARGET_0%\nYou are new girl of the month %TARGET_1%! Congratulation %TARGET_0%!!!\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_girl_of_the_month'] = NGE_SITE_TITLE . " girl of the month"; 
$LNG['escort_email_subject_girl_of_the_month'] = NGE_SITE_TITLE . " girl of the month"; 
$LNG['user_email_free_fuck_winner'] = "Hello %TARGET_0%\n<strong>You are the WINNER of the freefuck lottery contact us.</strong>\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_free_fuck_others'] = "Hello %TARGET_0%\nThe winner of the free fuck lottery of month %TARGET_1% is: <strong>%TARGET_2%</strong>!! Congratulation %TARGET_2%!!!\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_free_fuck_winner'] = NGE_SITE_TITLE . " freefuck lottery winner"; 
$LNG['user_email_subject_free_fuck_others'] = NGE_SITE_TITLE . " freefuck lottery"; 
$LNG['user_email_new_girls_in_region'] = "Hello %TARGET_0%\nNew girls from your favourite region joined EF.\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_new_girls_in_region'] = NGE_SITE_TITLE . " new girls"; 
$LNG['user_email_new_vip_girls_in_region'] = "Hello %TARGET_0%\nThere is new VIP %TARGET_1% from your favourite region since your last visit. %TARGET_2%\n" . NGE_SITE_TITLE . " Team"; 
$LNG['user_email_subject_new_vip_girls_in_region'] = NGE_SITE_TITLE . " new VIP girls"; 
$LNG['loft'] = 'Loft'; 
$LNG['loft_escorts_photo_limit_reached'] = "Loft escorts can have max %LIMIT% photos in gallery."; 
$LNG['loft_escorts_cant_go_on_tour'] = "Loft escorts can't go on tour."; 
/* design2 texts */ 
$LNG['members_login'] = "Members Login"; 
$LNG['signup_now'] = "Signup now"; 
$LNG['live_now'] = "Live now"; 
$LNG['girl_has_video'] = "Girl has video"; 
$LNG['girl_has_reviews'] = "Girl has reviews"; 
$LNG['100%_real_photos'] = "100% real photos"; 
$LNG['happy_hour'] = "Happy hour"; 
$LNG['icons_explanation'] = "Icons explanation"; 
$LNG['offer_incall'] = "Offer incall"; 
$LNG['offer_outcall'] = "Offer outcall"; 
$LNG['ebony_only'] = "Ebony only"; 
$LNG['asian_only'] = "Asian only"; 
$LNG['blondes_only'] = "Blondes only"; 
$LNG['add_to_my_favourites'] = "Add to my favourites"; 
$LNG['remove_from_favourites'] = "Remove from favourites"; 
$LNG['add_view_reviews'] = "Add/View reviews"; 
$LNG['about_escort'] = "About %SHOWNAME%"; 
$LNG['contact_me'] = "Contact me"; 
$LNG['now_open'] = "Now open"; 
$LNG['closed'] = "Closed"; 
$LNG['viewed'] = "viewed"; 
$LNG['quick_links'] = "Quick links"; 
$LNG['free_signup'] = "Free signup"; 
$LNG['signup_profit'] = " 
<ul> 
    <li>add <strong>comments</strong></li> 
    <li class='fsa-odd'>manage <strong>favorite</strong> list</li> 
    <li>write <strong>reviews</strong></li> 
    <li class='fsa-odd'>access to <strong>advanced search</strong></li> 
    <li><strong>watch</strong> list</li> 
</ul> 
"; 
$LNG['sort_by'] = "Sort by"; 
$LNG['random'] = "Random"; 
$LNG['alphabetically'] = "Alphabetically"; 
$LNG['most_viewed'] = "Most viewed"; 
$LNG['newest_first'] = "Newest first"; 
$LNG['narrow_your_search'] = "Narrow your search"; 
$LNG['with_reviews'] = "With reviews"; 
$LNG['search_by_name_agency'] = "Search by Name/Agency"; 
$LNG['comments_responses'] = "Comments &amp; Responses"; 
$LNG['post_comment'] = "Post a text comment"; 
$LNG['back'] = "Back"; 
$LNG['reply'] = "Reply"; 
$LNG['spam'] = "Spam"; 
$LNG['only_registered_users_can_add_comments']	= "Only registered users can add comments. Please <a href='%VALUE%'>login here</a>."; 
$LNG['your_comment'] = "Your comment"; 
$LNG['close'] = "Close"; 
$LNG['message_waiting_for_approval'] = "Thank you. Your message is waiting for approval now."; 
$LNG['ago'] = 'ago'; 
$LNG['time_ago'] = '%TIME% ago'; 
$LNG['yesterday'] = "Yesterday"; 
$LNG['minute'] = "minute"; 
$LNG['spanking'] = "Spanking"; 
$LNG['domination'] = "Domination"; 
$LNG['fisting'] = "Fisting"; 
$LNG['massage'] = "Massage"; 
$LNG['role_play'] = "Role play"; 
$LNG['bdsm'] = "BDSM"; 
$LNG['hardsports'] = "Hardsports"; 
$LNG['hummilation'] = "Hummilation"; 
$LNG['rimming'] = "Rimming"; 
$LNG['less_than'] = "less than %NUMBER%"; 
$LNG['more_than'] = "more than %NUMBER%"; 
$LNG['free_signup_advantages'] = "The signup on <span class='red'>" . NGE_SITE_TITLE . "</span> is absolutely <span class='red-big'>FREE</span> and has many advantages."; 
$LNG['reset'] = "Reset"; 
$LNG['private_area'] = "Private area"; 
$LNG['choose_your_choice'] = "Choose your choice"; 
$LNG['base_city'] = "Base city"; 
$LNG['additional_cities'] = "Additional cities"; 
$LNG['zone'] = "Zone"; 
$LNG['actual_tour_data'] = "NOW in %CITY% (%COUNTRY%)"; 
$LNG['upcoming_tour_data'] = "in %DAYS% in %CITY% (%COUNTRY%), from %FROM_DATE% - %TO_DATE%"; 
$LNG['beta_warning'] = NGE_SITE_TITLE . " is in Beta phase. We are constantly working on the site and make regular updates and improvements."; 
$LNG['ethnic_white'] = "Caucasian (white)"; 
$LNG['ethnic_black'] = "Black"; 
$LNG['ethnic_asian'] = "Asian"; 
$LNG['ethnic_latin'] = "Latin"; 
$LNG['ethnic_african'] = "African"; 
$LNG['ethnic_indian'] = "Indian"; 
$LNG['members_signup'] = "Members signup"; 
$LNG['click_here_your_signup'] = "Click here for your singup."; 
$LNG['your_email'] = "Your e-mail"; 
$LNG['missing_message'] = "Missing message"; 
$LNG['your_message_sent'] = "Your message was sent."; 
$LNG['signup'] = "Signup"; 
$LNG['signup_to_largest_escort_directory'] = "Signup now for <span class='red-big'>FREE</span> to the largest escort directory worldwide. We currently have over <span class='red-big'>3 million clients</span> monthly on our network. Start using our popularity to promote yourself now <span class='red-big'>FOR FREE</span>!"; 
$LNG['one_signup_all_sites'] = "By signing up, your profile will be automatically displayed on all these local country sites. Your login data will work on all the sites - you just have to signup once."; 
$LNG['email_verification_subject'] = NGE_SITE_TITLE . " account verification"; 
$LNG['email_verification_text'] = "<b>Dear %USER%,</b><br /><br /> 
<b>Thank you for registering at " . NGE_SITE_TITLE . "</b>.<br /><br /> 
You are 1 step away from activating your account, simply click on the link below or copy and paste it into your browser.<br /><br /> 
<b><a href='%LINK%' style='color: #dd0000;'>%LINK%</a></b><br /><br /> 
NOTE: <span style='font-size: 11px;'>if you experience any trouble, please contact us at <b><a href='mailto:" . NGE_CONTACT_EMAIL . "' style='color: #dd0000;'>" . NGE_CONTACT_EMAIL . "</a></b>.</span><br /><br /> 
<b>Yours faithfully</b><br /> 
<b>" . NGE_SITE_TITLE .  " Team</b>"; 
$LNG['email_confirmation_subject'] = NGE_SITE_TITLE . " registration"; 
$LNG['email_confirmation_text'] = "Dear %USER%,</b><br /><br /> 
Thank you for registering at " . NGE_SITE_TITLE . ".<br /> 
Your registration is now complete and you are an active member of " . NGE_SITE_TITLE . ".<br /><br /> 
<b>Following is your login information. Please keep this information on a safe place.<br /> 
<span style='display: block; float: left; width: 75px;'>Login:</span> <span style='color: #dd0000;'>%USER%</span><br /> 
<span style='display: block; float: left; width: 75px;'>Web:</span> <a href='" . NGE_URL . "' style='color: #dd0000;'>" . NGE_URL . "</a></b><br /><br /> 
Have fun on the site!<br /><br /> 
<b>Yours faithfully</b><br />  
<b>" . NGE_SITE_TITLE . " Team</b>"; 
//CUBIX Added By David 
$LNG['contact_us_text1'] = "In case if you wish to contact us regarding general enquiries, feedback or advertising, choose from the following options"; 
$LNG['phone_international'] = "Phone International"; 
$LNG['contact_us_text2'] = "Alternatively please use our contact form below"; 
$LNG['your_message'] = "Your Message"; 
$LNG['link_exschange'] = "Link Exchange"; 
$LNG['contact_us_text3'] = "If you would like to exchange links, please email the webmaster (e-mail).<br />Please copy our banner and place it on your server before contacting us regarding link exchange."; 
// 
//Cubix Added By Tiko 
$LNG['delete_profile'] = "Delete Profile"; 
$LNG['confirm_delete_profile'] = "Are you sure you want to delete the escort profile ?"; 
$LNG['delete_escort'] = "Delete escort profile"; 
$LNG['escort_profile_deleted'] = "Escort profile has been successfuly deleted"; 
$LNG['escort_profile_deleted_error'] = "Error: Escort profile has not been deleted"; 
$LNG['same_city'] = 'Working city and tour city can not be same.'; 
$LNG['overnight'] = 'overnight'; 
$LNG['directory_for_female_escorts'] = NGE_SITE_TITLE . ' is a UK escort directory for female escorts, escort agencies, independent escorts, trans and couples. 
On escortguide.co.uk you can find the perfect escort in London, Manchester, Leeds and all other areas in the United Kingdom. 
We update our escort directory on a daily basis - you will find new female escorts in london and from your area every day. 
If you are an escort agency or a female independent escort based in London or the UK, simply click on the 
signup link to register and upload your escort profile to escortguide.co.uk. '; 
//new search 
$LNG['kissing_no'] = "No Kissing"; 
$LNG['blowjob_no'] = "No Blowjob"; 
$LNG['cumshot_no'] = "No Cumshot"; 
$LNG['cumshot_in_mouth_spit'] = "Cumshot In Mouth Spit"; 
$LNG['cumshot_in_mouth_swallow'] = "Cumshot In Mouth Swallow"; 
$LNG['cumshot_in_face'] = "Cumshot In Face"; 
$LNG['with_comments'] = "with comments"; 
$LNG['nickname'] = "Nickname"; 
$LNG['without_reviews'] = "Without reviews"; 
$LNG['not_older_then'] = "Not older then"; 
$LNG['order_results'] = "Order results"; 
$LNG['select_your_girl_agency_requirements'] = "Select your girl/agency requirements"; 
$LNG['girls_details_bios'] = "Girls details/bio’s"; 
$LNG['cm'] = "cm"; 
$LNG['ft'] = "ft"; 
$LNG['kg'] = "kg"; 
$LNG['lb'] = "lb"; 
$LNG['girls_origin'] = "Girls origin"; 
$LNG['ethnicity'] = "Ethnicity"; 
$LNG['measurements'] = "Measurements"; 
$LNG['open_hours'] = "Open hours"; 
$LNG['my_favorites'] = "My Favorites";
$LNG['learn_more_webcam'] = "If you have a webcam plus an account at
Skype or MSN, this is the easiest way to
get your photos verified.
Just click on the 'Webcam session request' link,
choose a date and a time and wait for our
confirmation. At the requested time you will
then have an short online appointment (video chat)
with one of our adminstrators to approve that it's
you on the photos. After this session your
photos will be 100% verified. ";
$LNG['learn_more_passport'] = "If you choose this option you need to scan your
passport or any other official document such as
identity card or driving license and upload it
to our system along with 1-2 additional private photos.";

$LNG['verify_welcome'] = "Welcome!";
$LNG['verify_welcome_text'] = "Welcome to the 100% verified picture process.
Use the chance and get your profile pictures to be
100% verified genuine. In just a few simple
steps you will increase the credibility of your profile
and get many more users. You will get also a sticker
to your profile which will show to the users that your photos
are 100% genuine. Additionally your profile will be
listed in an extra sort by menu. ";
$LNG['verify_enroll'] = "Enroll now!";
$LNG['verify_enroll_text'] = "We provide currently the following <strong>2 options</strong> to get your pictures 100% verified:";
$LNG['verify_enroll_1'] = "Webcam session request with our admin";
$LNG['verify_enroll_2'] = "Passport copy";
$LNG['verify_learn_more'] = "Learn more...";
$LNG['verify_start_now'] = "<span>START NOW </span> the process";
$LNG['verify_steps'] = "STEP GUIDLINE:";
$LNG['verify_select_2_options'] = "Please select one of the following 2 options for the process:";
$LNG['verify_select_1'] = "Webcam session request with our admin";
$LNG['verify_select_1_note'] = "<strong>note:</strong> you need to have a webcam and an account at Skype or MSN ";
$LNG['verify_select_2'] = "Passport copy - you need a valid passport or identity card";
$LNG['verify_select_2_note'] = "<strong>note:</strong> to speed up the process we recommend to upload 1-2 private photos in addition to the passport copy. ";
$LNG['verify_important'] = "IMPORTANT - PLEASE READ CAREFULLY: Any attempt of fraud or tricking the process will result in a lifelong
banning from the EF network. All your paid advertisements will be disabled and no refund given. ";

$LNG['verify_sms_message'] = "Videochat confirmation for the photo verification process: 
Date: #date#, Time: #time#. You will be contacted by our administrator. #title#";
$LNG['verify_email_subject'] = "Online appointment confirmation";
$LNG['verify_email_message'] = "Hereby we confirm your online appointment for the photo verification process at:<br /><br />
Date: #date#<br />
Time: #time#<br /><br />
You will be contacted by one of our administrators. Kind regards.<br />#title#";

$LNG['reject_sms_message'] = "Please choose 3 new dates for the videochat session (photo verification). We could not confirm your requested dates. Just login and choose again. #title#";
$LNG['reject_email_subject'] = "100% photo verification process / please enter new dates";
$LNG['reject_email_message'] = "Dear #showname#<br /><br />
Unfortunately all suggested dates for the video chat session to verify your pictures could not been confirmed. Please login again to your private area and choose three new
dates. We would like to appologize for any inconvenience caused.<br /><br />
Kind regards<br />
#title#";
$LNG['reg_has_been_succ'] = "Registration has been successful !";
$LNG['100_verify'] = '100 Verify';
$LNG['verify_global_title'] = "100% VERIFIED PICTURE PROCESS";

$LNG['escort_is_premium'] = 'This escort has premium package. <br/> If you want to delete the profile of this escort, <br/> please assign the package to another escort and then delete the profile.';
$LNG['100_verify'] = '100 Verify';
$LNG['verify_global_title'] = "100% VERIFIED PICTURE PROCESS";

$LNG['verify_webcam_session'] = "Webcam session";
$LNG['verify_webcam_request'] = "Request a webcam session with our admin, please provide at least 2 appointments. In case of cancellation pls inform the admin.";
$LNG['verify_appointment'] = "Appointment request nr.";
$LNG['verify_preffered'] = "Preffered time";
$LNG['verify_comment_field'] = "Comment field";
$LNG['verify_webcam_software'] = "Webcam software:<br /><span>(select either MSN or skype)</span>";
$LNG['verify_type_username'] = "Type username";
$LNG['verify_select_form'] = "Pls selected the form you would like to get confirmed your meeting request";
$LNG['verify_recommended'] = "recommended";
$LNG['verify_over_sms'] = "Over phone SMS";
$LNG['verify_congratulations'] = "Congratulations!";
$LNG['verify_congratulations_text'] = "<p>You succesfully requested a meeting for <strong>webcam verification process</strong>. You will be informed <strong>by mail</strong> or in your <strong>personal account panel</strong> if your meeting has been confirmed and will be provided with all details.</p><p class='colored verpic-note verpic-olli2'>Please note that from monday-friday your request will be answered within 24h, on weekends just on the following monday. National holidays are treated the same way like weekend day's.</p>";
$LNG['verify_already_pending'] = "You already have pending request. Please wait for our administrator to contact you.";
$LNG['verify_rejected'] = "Rejected";
$LNG['verify_passport_upload'] = "Passport, identity card upload";
$LNG['verify_passport_upload_text'] = "You have choosen to provide a passport or identity card for the verification process. Pls note, we cannot accept e-mails with passport copy or identity card. You must provide them over the upload pannel here. In case of difficulties pls contact your sales agent: ";
$LNG['verify_upload_now'] = "Upload now your documents here";
$LNG['verify_provide_pics'] = "TIP: Provide 1-2 privat picture, this way you increase your chance for the 100% verification process and speed up the process.";
$LNG['verify_select_notify'] = "Please select how you wanna get notified about your verification results:";
$LNG['verify_image_no'] = "Image no. ";
$LNG['verify_uploaded'] = "uploaded";
$LNG['verify_uploaded_pictures'] = "Uploaded pictures";
$LNG['verify_uploaded_pictures_text'] = "You have uploaded following pictures, please put in the comment field, what kind of documents you provided.";
$LNG['verify_uploaded_comment_field'] = "Comment field";
$LNG['verify_congratulations_text2'] = "<p class='colored'><strong>You succssefully completed the 100% verification process by upload of documents!</strong></p><p class='verpic-note'>Please note that from monday-friday your request will be answered within 24h, on weekends just on the following monday.  National holidays are treated the same way like weekend day's.</p>";
$LNG['verify_attention'] = "Important note: For uploading the photos please use solely the form below. Do not send the photos by e-mail. Only send an e-mail to the sales representative in case you experience any problems uploading the pictures. ";