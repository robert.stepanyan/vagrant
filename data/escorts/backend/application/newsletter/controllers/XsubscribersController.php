<?php

class Newsletter_XsubscribersController extends Zend_Controller_Action
{
	/**
	 * @var Model_Seo_Entities
	 */
	protected $_model;
		
	public function init()
	{
	}
	
	public function indexAction()
	{
		
	}

	public function dataAction()
	{
		/*$req = $this->_request;

		$data = $this->_model->getAll(
			$req->page,
			$req->per_page,
			$req->sort_field,
			$req->sort_dir,
			$count,
			array('email' => $req->email, 'status' => $req->status, 'group' => $req->group, 'date_from' => $req->date_from, 'date_to' => $req->date_to)
		);

		foreach ($data as $k => $d)
		{
			$data[$k]['group_title'] = $this->_model->getGroups($d[id]);
		}*/

		$data = ''; $count = '';

		die(json_encode(array('data' => $data, 'count' => $count)));
	}
	
	public function createAction()
	{
		/*$this->view->layout()->disableLayout();

		$modelGroups = new Model_Newsletter_Groups();
		$this->view->groups = $modelGroups->getList();
		
		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'email' => '',
				'name' => '',
				'reg_date' => '',
				'status' => '',
				'groups' => ''
			));
			$data = $data->getData();
			
			$validator = new Cubix_Validator();
			
			if ( ! strlen($data['email']) ) {
				$validator->setError('email', 'Required');
			}
			elseif ( ! $validator->isValidEmail($data['email']) ) {
				$validator->setError('email', 'Invalid Email');
			}

			if ( ! $data['groups'] ) {
				$validator->setError('groups_list', 'Required');
			}
			
			if ( $validator->isValid() ) {
				$this->_model->save($data);
			}
			
			die(json_encode($validator->getStatus()));
		}*/
	}
	
	public function editAction()
	{
		/*$this->view->layout()->disableLayout();
		$modelGroups = new Model_Newsletter_Groups();

		$this->view->groups = $modelGroups->getList();
		$id = intval($this->_request->id);
		$this->view->item = $this->_model->getById($id);
		$cross = $this->_model->getGroupsById($id);
		$ids = array();

		foreach ($cross as $c)
			$ids[] = $c->group_id;

		$this->view->cross = $ids;

		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'id' => 'int',
				'email' => '',
				'name' => '',
				'reg_date' => '',
				'status' => '',
				'groups' => ''
			));
			$data = $data->getData();
					
			$validator = new Cubix_Validator();
			
			if ( ! strlen($data['email']) ) {
				$validator->setError('email', 'Required');
			}
			elseif ( ! $validator->isValidEmail($data['email']) ) {
				$validator->setError('email', 'Invalid Email');
			}

			if ( ! $data['groups'] ) {
				$validator->setError('groups_list', 'Required');
			}
			
			if ( $validator->isValid() ) {
				$this->_model->save($data);
			}
			
			die(json_encode($validator->getStatus()));
		}*/
	}
	
	public function deleteAction()
	{
		/*$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->_model->remove($id);*/

		die;
	}

	public function exportAction()
	{
		/*$this->view->layout()->disableLayout();

		$this->view->list = $this->_model->getExportList();*/
	}

	public function importAction()
	{
		/*$this->view->layout()->disableLayout();
		$modelGroup = new Model_Newsletter_Groups();

		$this->view->groups = $modelGroup->getList();

		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'email_list' => '',
				'group' => ''
			));
			$data = $data->getData();

			$validator = new Cubix_Validator();
		
			if ( ! $data['group'] ) {
				$validator->setError('groups_list', 'Required');
			}

			if ( $validator->isValid() ) {
				$this->_model->import($data);
			}

			die(json_encode($validator->getStatus()));
		}*/
	}

	public function fileAction()
	{
		/*$this->view->layout()->disableLayout();

		$config = Zend_Registry::get('newsletter_config');
		$size = $config['file']['size'];
		$size_byte = $size * 1024 * 1024;

		if ($this->_request->isPost() && isset($_FILES['file']) && @is_uploaded_file($_FILES['file']['tmp_name']))
		{
			$ses = new Zend_Session_Namespace('newsletter');

			$ext = strtolower(@end(explode('.', $_FILES['file']['name'])));
			$name = md5(microtime()) . '.' . $ext;
			
			if ($ext == 'txt')
			{
				if ($_FILES['file']['size'] <= $size_byte)
				{
					move_uploaded_file($_FILES['file']['tmp_name'], APPLICATION_PATH . '/tmp_files/' . $name);
					$ses->file_name = $name;
					$this->view->ok = 'File uploaded!';
				}
				else
				{
					$this->view->error = 'Maximum size of video can be ' . $size . ' Mb.';
				}
			}
			else
			{
				$this->view->error = 'Wrong file.';
			}
		}*/
	}

	public function toggleAction()
	{
		/*$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->_model->toggle($id);*/
	}

	public function deleteCheckedAction()
	{
		/*$id = $this->_request->id;

		if ( !is_array($id) )
			$id = array($id);

		$this->_model->deleteChecked($id);*/

		die;
	}

	public function syncAction()
	{
		$this->view->layout()->disableLayout();
		$newsletter_api = new Cubix_NewsletterApi;

		$params['requesttype'] = 'user';
		$params['requestmethod'] = 'GetLists';

		$ret = $newsletter_api->process($params);

		$arr = array();

		$status = $ret['status'];
		$data = (array)$ret['data'];
		$this->view->groups = $data['item'];

		if ( $this->_request->isPost() )
		{
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'type' => '',
				'group' => ''
			));
			$data = $data->getData();

			$validator = new Cubix_Validator();

			if ( ! strlen($data['type']) ) {
				$validator->setError('type', 'Required');
			}
			
			if ( ! $data['group'] ) {
				$validator->setError('group', 'Required');
			}

			if ( $validator->isValid() )
			{
				$start = time();

				switch ($data['type'])
				{
					case 'escorts':
						$m = new Model_Escorts();
						$items = $m->getAllForNewsletterApi();
						break;
					case 'members':
						$m = new Model_Members();
						$items = $m->getAllForNewsletterApi();
						break;
					case 'agencies':
						$m = new Model_Agencies();
						$items = $m->getAllForNewsletterApi();
						break;
					default :
						die;
				}

				$list_id = $data['group'];
				
				$params['requesttype'] = 'subscribers';
				$params['requestmethod'] = 'AddSubscriberToList';

				$count = 0;

				foreach ($items as $item)
				{
					$email = $item->email;
					$name = $item->name;
					
					$params['details'] = array(
						'emailaddress' => $email,
						'mailinglist' => $list_id,
						'format' => 'h',
						'confirmed' => 1,
						'customfields' => array(
							'fieldid' => 2,
							'value' => $name
						)
					);
					
					$ret = $newsletter_api->process($params);

					$count ++;
				}

				$end = time();

				$time = $end - $start; // in seconds
				$min = (int)($time / 60);
				$sec = $time % 60;

				//newsletter.log
				$f = fopen('newsletter.log', 'a');
				fwrite($f, 'Add Subscribers from ' . $data['type'] . ', count = ' . $count . ', time = ' . $time . ' seconds - ' . $min . ' min ' . $sec . ' sec' . "\r\n");
				fclose($f);

			}

			die(json_encode($validator->getStatus()));
		}
	}
}
