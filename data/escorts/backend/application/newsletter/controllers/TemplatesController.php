<?php

class Newsletter_TemplatesController extends Zend_Controller_Action 
{
	protected $_model;
	protected $_interval;
	
	//protected $_tpl_vars = array();
	
	public function init()
	{
		$this->_model = new Model_Newsletter_Templates();
		//$this->_interval = '-7 days';
		$this->_interval = '-1 year';
	}
	
	public function indexAction()
	{
		
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'application_id' => $this->_request->application_id
		);
		
		$data = $this->_model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		die(json_encode(array('data' => $data, 'count' => $count)));
	}
	
	public function createAction()
	{
		$this->view->layout()->disableLayout();
		
		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'title' => '',
				'subject' => '',
				'from_email' => '',
				'body' => '',
				'application_id' => 'int'
			));
			$data = $data->getData();
			
			$validator = new Cubix_Validator();
			
			if ( ! strlen($data['title']) ) {
				$validator->setError('title', 'Required');
			}
			elseif ($this->_model->checkTitle($data['title']))
			{
				$validator->setError('title', 'Title exists, please choose another one');
			}
			
			if ( ! strlen($data['from_email']) ) {
				$validator->setError('from_email', 'Required');
			}

			if ( ! strlen($data['body']) ) {
				$validator->setError('body', 'Required');
			}
			
			if ( $validator->isValid() ) {
				$this->_model->save($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function editAction()
	{
		$this->view->layout()->disableLayout();
		
		$id = intval($this->_request->id);
		
		$this->view->item = $template = $this->_model->getById($id);
		
		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'id' => 'int',
				'title' => '',
				'subject' => '',
				'from_email' => '',
				'body' => '',
				'application_id' => 'int'
			));
			$data = $data->getData();
			//echo $template->title;die;
			$validator = new Cubix_Validator();
			
			if ( ! strlen($data['title']) ) {
				$validator->setError('title', 'Required');
			}
			elseif ($this->_model->checkTitle($data['title']) && ($template->title != $data['title']))
			{
				$validator->setError('title', 'Title exists, please choose another one');
			}

			if ( ! strlen($data['from_email']) ) {
				$validator->setError('from_email', 'Required');
			}

			if ( ! strlen($data['body']) ) {
				$validator->setError('body', 'Required');
			}
			
			if ( $validator->isValid() ) {
				$this->_model->save($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function deleteAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->_model->remove($id);

		die;
	}
	
	/* ****************************************************** */
	public function sendAction()
	{
		$this->view->templates = $this->_model->getList();

		if ($this->_request->send_newsletter == 1)
		{
			//echo 'send';
			$application_id = intval($this->_request->application_id);
			$template_id = intval($this->_request->template_id);
			$members = $this->_request->members;
			$groups = $this->_request->groups;
			$no_group = $this->_request->no_group;
			$from_email = trim($this->_request->from_email);
			$subject = trim($this->_request->subject);
			$vars = $this->_request->vars;
			$send_time = $this->_request->send_time;
			$escorts_data = $this->_request->escorts;

			$validator = new Cubix_Validator();

			if ( $send_time == 'custom' && ! strlen($this->_request->custom_time) ) {
				$validator->setError('custom_time', 'Custom Time Required');
			}

			if ( ! strlen($from_email) ) {
				$validator->setError('from_email', 'Required');
			}

			if ( ! strlen($subject) ) {
				$validator->setError('subject', 'Required');
			}

			if ( ! isset($groups) && !isset($members) && !isset($no_group) ) {
				$validator->setError('group_list', 'Required');
			}

			if ( $validator->isValid() ) {
				$tpl = $this->_model->getById($template_id);

				//Replace html with template variables
				//Escort List
				if ($escorts_data)
				{
					$escorts = $this->_model->makeEscortList($escorts_data);
					$this->view->escorts = $escorts;
					$this->view->url = Cubix_Application::getById($application_id)->url;
					$this->view->backend_url = Cubix_Application::getById($application_id)->backend_url;
					$html = $this->view->render('templates/tpl-escorts.phtml');
					$tpl->body = str_replace('{ESCORT_LIST}', $html, $tpl->body);
				}

				//Template Vars
				if ($vars)
				{
					foreach ($vars as $k => $var)
					{
						//$tpl->body = preg_replace("#{EDITABLE_$k}([\s\S]+?){/EDITABLE_$k}#", $var, $tpl->body);
						$tpl->body = preg_replace("#({EDITABLE_$k})([\s\S]+?)({/EDITABLE_$k})#", '$1' . $var . '$3', $tpl->body);
					}
				}

				$this->_request->setParam('email_body', $tpl->body);

				$this->_model->saveNewsletter($this->_request);
			}

			die(json_encode($validator->getStatus()));
		}
	}
	
	public function loadAction()
	{
		$tpl_id = intval($this->_getParam('template_id'));
		if ( ! $tpl_id ) die;
		
		$tpl = $this->_model->getById($tpl_id);
		$this->view->vars = $tpl->getVars();
		$this->view->tpl = $tpl;
		
		$m_groups = new Model_Newsletter_Groups();
		$this->view->groups = $m_groups->getList();

		$m_subscribers = new Model_Newsletter_Subscribers();
		$res = $m_subscribers->noInGroups();

		$this->view->no_group_count = intval($res['count']);
	}
	
	public function escortPickerAction()
	{
		$m = new Model_Escorts();

		$count = 0;
		$escorts = $m->getAll(1, 1000, array(
			'filter_by' => 'registration_date',
			'date_from' => strtotime('-1 year'),
			//'date_from' => strtotime('-7 days'),
			'with_photo' => true,
			'u.application_id' => $this->_getParam('application_id')
		), 'u.date_registered', 'DESC', $count);
		$this->view->count = $count;
		$this->view->escorts = $escorts;
	}
	
	public function previewAction()
	{
		$this->view->layout()->disableLayout();
		
		$template_id = intval($this->_getParam('template_id'));
		
		if ( ! $template_id ) {
			return;
		}

		$escorts_data = (array) $this->_getParam('escorts', array());
		$escorts = $this->_model->makeEscortList($escorts_data);
		
		$this->view->escorts = $escorts;
		$html = $this->view->render('templates/tpl-escorts.phtml');
		
		$tpl = $this->_model->getById($template_id);
		$tpl->body = str_replace('{ESCORT_LIST}', $html, $tpl->body);

		$vars = $this->_getParam('vars');
		//Template Vars
		if ($vars)
		{
			foreach ($vars as $k => $var)
			{
				$tpl->body = preg_replace("#{EDITABLE_$k}([\s\S]+?){/EDITABLE_$k}#", $var, $tpl->body);
			}
		}
				
		die($tpl->body);
	}
}
