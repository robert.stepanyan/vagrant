<?php

class Newsletter_XgroupsController extends Zend_Controller_Action
{
	/**
	 * @var Model_Seo_Entities
	 */
	protected $_model;
		
	public function init()
	{
		
	}
	
	public function indexAction()
	{
		
	}

	public function dataAction()
	{
		$newsletter_api = new Cubix_NewsletterApi;

		$params['requesttype'] = 'user';
		$params['requestmethod'] = 'GetLists';

		$ret = $newsletter_api->process($params);
		
		$arr = array();

		$status = $ret['status'];
		$data = (array)$ret['data'];
		$data = $data['item'];
		$count = 0;

		if ($status[0] == 'SUCCESS')
		{
			foreach ($data as $item)
			{
				$item = (array)$item;

				$id = $item['listid'];
				$name = $item['name'];
				$subscribecount = $item['subscribecount'];

				$arr[] = array('id' => $id, 'group_title' => $name, 'subscribecount' => $subscribecount);
				$count ++;
			}
		}
		else
		{
			print_r($data);
			//error
		}

		die(json_encode(array('data' => $arr, 'count' => $count)));
	}
}
