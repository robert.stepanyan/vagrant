<?php

class Newsletter_NewslettersController extends Zend_Controller_Action
{
	protected $_model;
	
	//protected $_tpl_vars = array();
	
	public function init()
	{
		$this->_model = new Model_Newsletter_Newsletters();
		
		/*$this->_tpl_vars = array(
			'app_title' => '',
			'app_host' => '',
			'member_username' => '',
			'escorts_list' => ''
		);*/
	}
	
	public function indexAction()
	{
		
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'application_id' => $this->_request->application_id,
			'status' => $this->_request->status,
			'date_from' => $this->_request->date_from,
			'date_to' => $this->_request->date_to
		);
		
		$data = $this->_model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);

		$defines = Zend_Registry::get('defines');

		foreach ($data as $d)
		{
			$d->status_text = $defines['newsletter_status_options'][$d->status];
			if ( $d->emails_count > 0 ) {
				$d->ctr = round($d->clicks / $d->emails_count * 100, 2) . '%';
			}
			else {
				$d->ctr = '-';
			}
		}

		die(json_encode(array('data' => $data, 'count' => $count)));
	}

	public function cancelSendingAction()
	{
		$id = intval($this->_request->id);

		$this->_model->updateStatus($id, NEWSLETTER_CANCELED);

		die;
	}

	public function editAction()
	{
		$id = intval($this->_request->id);

		if ( $this->_request->isPost() )
		{
			$application_id = intval($this->_request->application_id);
			$id = intval($this->_request->id);
			$members = $this->_request->members;
			$groups = $this->_request->groups;
			$no_group = $this->_request->no_group;
			$from_email = trim($this->_request->from_email);
			$subject = trim($this->_request->subject);
			$vars = $this->_request->vars;
			$send_time = $this->_request->send_time;
			$escorts_data = $this->_request->escorts;

			$validator = new Cubix_Validator();

			if ( $send_time == 'custom' && ! strlen($this->_request->custom_time) ) {
				$validator->setError('custom_time', 'Custom Time Required');
			}

			if ( ! strlen($from_email) ) {
				$validator->setError('from_email', 'Required');
			}

			if ( ! strlen($subject) ) {
				$validator->setError('subject', 'Required');
			}

			if ( ! isset($groups) && !isset($members) && !isset($no_group) ) {
				$validator->setError('group_list', 'Required');
			}

			if ( $validator->isValid() ) {
				$tpl = $this->_model->getById($id);

				//Replace html with template variables
				//Escort List
				/*if ($escorts_data)
				{
					$escorts = $this->_model->makeEscortList($escorts_data);
					$this->view->escorts = $escorts;
					$html = $this->view->render('templates/tpl-escorts.phtml');
					$tpl->body = str_replace('{ESCORT_LIST}', $html, $tpl->body);
				}*/

				//Template Vars
				if ($vars)
				{
					foreach ($vars as $k => $var)
					{
						//$tpl->body = preg_replace("#{EDITABLE_$k}([\s\S]+?){/EDITABLE_$k}#", $var, $tpl->body);
						$tpl->body = preg_replace("#({EDITABLE_$k})([\s\S]+?)({/EDITABLE_$k})#", '$1' . $var . '$3', $tpl->body);
					}
				}

				$this->_request->setParam('body', $tpl->body);

				$this->_model->updateNewsletter($id, $this->_request);
			}
			die(json_encode($validator->getStatus()));
		}
	}

	public function loadAction()
	{
		$tpl_id = intval($this->_getParam('id'));
		if ( ! $tpl_id ) die;

		$tpl = $this->_model->getById($tpl_id);
		$this->view->vars = $tpl->getVars();
		$this->view->tpl = $tpl;

		$m_groups = new Model_Newsletter_Groups();
		$this->view->groups = $m_groups->getList();

		$m_subscribers = new Model_Newsletter_Subscribers();
		$res = $m_subscribers->noInGroups();

		$this->view->no_group_count = intval($res['count']);
	}
}
