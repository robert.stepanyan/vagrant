<?php

class Newsletter_GroupsController extends Zend_Controller_Action
{
	/**
	 * @var Model_Seo_Entities
	 */
	protected $_model;

	protected $_tpl_vars = array();
		
	public function init()
	{
		$this->_model = new Model_Newsletter_Groups();
	}
	
	public function indexAction()
	{
		
	}

	public function dataAction()
	{
		$req = $this->_request;

		$data = $this->_model->getAll(
			$req->page,
			$req->per_page,
			$req->sort_field,
			$req->sort_dir,
			$count
		);

		die(json_encode(array('data' => $data, 'count' => $count)));
	}
	
	public function createAction()
	{
		$this->view->layout()->disableLayout();
		
		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'group_title' => ''
			));
			$data = $data->getData();
			
			$validator = new Cubix_Validator();
			
			if ( ! strlen($data['group_title']) ) {
				$validator->setError('group_title', 'Required');
			}
			
			if ( $validator->isValid() ) {
				$this->_model->save(new Model_Newsletter_TemplateItem($data));
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function editAction()
	{
		$this->view->layout()->disableLayout();
		
		$id = intval($this->_request->id);
		
		$this->view->group = $this->_model->getById($id);
		
		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'id' => 'int',
				'group_title' => ''
			));
			$data = $data->getData();
					
			$validator = new Cubix_Validator();
			
			if ( ! strlen($data['group_title']) ) {
				$validator->setError('group_title', 'Required');
			}
			
			if ( $validator->isValid() ) {
				$this->_model->save(new Model_Newsletter_TemplateItem($data));
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function deleteAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->_model->remove($id);
	}
}
