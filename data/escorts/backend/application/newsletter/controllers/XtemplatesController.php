<?php

class Newsletter_XtemplatesController extends Zend_Controller_Action
{
	protected $_model;
	protected $_interval;
	
	/*const MAIL_SERVER_HOST = "m7.desrv.com";
	const MAIL_SERVER_PORT = 465;
	const MAIL_SERVER_AUTH = true;
	const MAIL_SERVER_USER = "info@escortforumit.xxx";
	const MAIL_SERVER_PASS = "tr#nzk99";
	const MAIL_FROM = "info@escortguide.com";
	const MAIL_FROM_NAME = "6annonce";*/
	
	/*const AUTH_MAIL_SERVER_HOST = "m1.czsrv.com";
	const AUTH_MAIL_SERVER_PORT = 465;
	const AUTH_MAIL_SERVER_AUTH = true;
	const AUTH_MAIL_SERVER_USER = "info@6anuncio.com";
	const AUTH_MAIL_SERVER_PASS = "bristhe#zzk";
	const AUTH_MAIL_FROM = "info@asianescorts.com";
	const AUTH_MAIL_FROM_NAME = "asianescorts.com";*/
	
	const AUTH_MAIL_SERVER_HOST = "zsmtp.newsmanapp.com";
	const AUTH_MAIL_SERVER_PORT = 587;
	const AUTH_MAIL_SERVER_AUTH = true;
	const AUTH_MAIL_SERVER_USER = "info@asianescorts.com";
	const AUTH_MAIL_SERVER_PASS = "tr#nzk912";
	const AUTH_MAIL_FROM = "info@asianescorts.com";
	const AUTH_MAIL_FROM_NAME = "asianescorts.com";
	
	const MAIL_SERVER_HOST = "localhost";
	const MAIL_SERVER_PORT = 25;
	const MAIL_SERVER_AUTH = false;
	const MAIL_SERVER_USER = "";
	const MAIL_SERVER_PASS = "";
	const MAIL_FROM = "info@escortguide.co.uk"; //info@escortguide.com old
	const MAIL_FROM_NAME = "6annonce";
	
	//protected $_tpl_vars = array();
	
	public function init()
	{
		$this->_model = new Model_Newsletter_Templates();
		$this->_interval = '-14 days';
		//$this->_interval = '-1 year';
		//$this->_interval = '-1 month';
	}
	
	public function indexAction()
	{
		
	}
	
	public function dataAction()
	{
		$req = $this->_request;

		$filter = array(
			'application_id' => $this->_request->application_id
		);

		$data = $this->_model->getAll(
			$this->_request->page,
			$this->_request->per_page,
			$filter,
			$this->_request->sort_field,
			$this->_request->sort_dir,
			$count
		);

		die(json_encode(array('data' => $data, 'count' => $count)));
	}
	
	public function createAction()
	{
		$this->view->layout()->disableLayout();
		
		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'title' => '',
				'subject' => '',
				'from_email' => '',
				'body' => '',
				'body_plain' => '',
				'application_id' => 'int'
			));
			$data = $data->getData();
			
			$validator = new Cubix_Validator();
			
			if ( ! strlen($data['title']) ) {
				$validator->setError('title', 'Required');
			}
			elseif ($this->_model->checkTitle($data['title']))
			{
				$validator->setError('title', 'Title exists, please choose another one');
			}
			
			if ( ! strlen($data['from_email']) ) {
				$validator->setError('from_email', 'Required');
			}

			if ( ! strlen($data['body']) ) {
				$validator->setError('body', 'Required');
			}
			
			if ( $validator->isValid() ) 
			{
				$this->_model->save($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function editAction()
	{
		$this->view->layout()->disableLayout();
		
		$id = intval($this->_request->id);
		
		$this->view->item = $template = $this->_model->getById($id);
				
		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'id' => 'int',
				'title' => '',
				'subject' => '',
				'from_email' => '',
				'body' => '',
				'body_plain' => '',
				'application_id' => 'int'
			));
			$data = $data->getData();
			//echo $template->title;die;
			$validator = new Cubix_Validator();
			
			if ( ! strlen($data['title']) ) {
				$validator->setError('title', 'Required');
			}
			elseif ($this->_model->checkTitle($data['title']) && ($template->title != $data['title']))
			{
				$validator->setError('title', 'Title exists, please choose another one');
			}

			if ( ! strlen($data['from_email']) ) {
				$validator->setError('from_email', 'Required');
			}

			if ( ! strlen($data['body']) ) {
				$validator->setError('body', 'Required');
			}
			
			if ( $validator->isValid() )
			{
				$data['body'] = $validator->removeNewsletterEmoji($data['body']);
				$this->_model->save($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function deleteAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->_model->remove($id);

		die;
	}
	
	/* ****************************************************** */
	public function sendAction()
	{
		$this->view->templates = $this->_model->getList(array('application_id' => $this->_request->application_id));
		
		if ($this->_request->send_newsletter == 1)
		{
			//echo 'send';
			$application_id = intval($this->_request->application_id);
			$template_id = intval($this->_request->template_id);
			$members = $this->_request->members;
			$groups = $this->_request->groups;
			$no_group = $this->_request->no_group;
			$from_email = trim($this->_request->from_email);
			$from_name = trim($this->_request->from_name);
			$subject = trim($this->_request->subject);
			$vars = $this->_request->vars;
			$send_time = $this->_request->send_time;
			$escorts_data = $this->_request->escorts;
			$send_by = $this->_request->send_by;
			$assign_plain = $this->_request->assign_plain;

			$validator = new Cubix_Validator();

			if ( $send_time == 'custom' && ! strlen($this->_request->custom_time) ) {
				$validator->setError('custom_time', 'Custom Time Required');
			}

			if ( ! strlen($from_name) ) {
				$validator->setError('from_name', 'Required');
			}

			if ( ! strlen($from_email) ) {
				$validator->setError('from_email', 'Required');
			}

			if ( ! strlen($subject) ) {
				$validator->setError('subject', 'Required');
			}

			if ( ! isset($groups) && !isset($members) && !isset($no_group) ) {
				$validator->setError('group_list', 'Required');
			}

			if ( $validator->isValid() ) {
				$tpl = $this->_model->getById($template_id);
				
				$application_id = Cubix_Application::getId();
				//Replace html with template variables
				//Escort List
				if ($escorts_data)
				{
					$escorts = $this->_model->makeEscortList($escorts_data);
					$this->view->escorts = $escorts;
					$this->view->url = Cubix_Application::getById($application_id)->url;
					$this->view->backend_url = Cubix_Application::getById($application_id)->backend_url;
					$html = $this->view->render('xtemplates/tpl-escorts.phtml');
					$tpl->body = str_replace('{ESCORT_LIST}', $html, $tpl->body);
					$tpl->body = str_replace('{TOUR_ESCORT_LIST}', $html, $tpl->body);
					
					if ( $assign_plain ) {
						$escorts = $this->_model->makeEscortList($escorts_data);						
						$url = Cubix_Application::getById($application_id)->url;
						
						$escort_urls = "";
						$esc = "escort";
						if ( $application_id == 1 ) {
							$esc = "accompagnatrici";
						}
						else if ( $application_id == 2 ) {
							$esc = "escort";
						}
						
						foreach ( $escorts as $escort ) {
							$escort_urls .= $url . '/' . $esc . '/' . $escort->showname . '-' . $escort->id . "\n\r";
						}
						//$html = $this->view->render('xtemplates/tpl-escorts.phtml');
						$tpl->body_plain = str_replace('{ESCORT_LIST}', $escort_urls, $tpl->body_plain);
						$tpl->body_plain = str_replace('{TOUR_ESCORT_LIST}', $escort_urls, $tpl->body_plain);
					}
				}

				//Template Vars
				if ($vars)
				{
					foreach ($vars as $k => $var)
					{
						//$tpl->body = preg_replace("#({EDITABLE_$k})([\s\S]+?)({/EDITABLE_$k})#", '$1' . $var . '$3', $tpl->body);
						$tpl->body = preg_replace("#({EDITABLE_$k})([\s\S]+?)({/EDITABLE_$k})#", $var, $tpl->body);
						$tpl->body_plain = preg_replace("#({EDITABLE_$k})([\s\S]+?)({/EDITABLE_$k})#", strip_tags($var), $tpl->body_plain);
					}
				}

				if ( $send_by == 'newsman' ) {
					
					$auth_apps = array(APP_BL, APP_EM, APP_6B, APP_EG_CO_UK, APP_EG, APP_EF);
					
					$config = array();
					if ( in_array(Cubix_Application::getId(), $auth_apps) ) {
						
						$config = array(
							'port' => self::AUTH_MAIL_SERVER_PORT
						);
						
						if ( self::AUTH_MAIL_SERVER_AUTH )
						{
							$config = array_merge(array(
								'auth' => 'login',
								'ssl' => 'tls',
								'username' => self::AUTH_MAIL_SERVER_USER,
								'password' => self::AUTH_MAIL_SERVER_PASS
							), $config);
						}
						$mail_server_host = self::AUTH_MAIL_SERVER_HOST;
						
					} else {
						if ( self::MAIL_SERVER_PORT == 25 ) {
							$config = array(
								'port' => self::MAIL_SERVER_PORT
							);
						}
						if ( self::MAIL_SERVER_AUTH )
						{
							$config = array_merge(array(
								'auth' => 'login',
								'ssl' => 'tls',
								'username' => self::MAIL_SERVER_USER,
								'password' => self::MAIL_SERVER_PASS
							), $config);
						}
						$mail_server_host = self::MAIL_SERVER_HOST;
					}
					
					if(Cubix_Application::getId() == APP_EF){
						$config = array(
								'auth' => 'login',
								'ssl' => 'tls',
								//'port' => 465,
								'username' => "info@escortforum.net",
								'password' => "tr#nzk99",
						);
						$mail_server_host = "smtp.newsmanapp.com";
					}

					if(Cubix_Application::getId() == APP_6B){
						$mail_server_host = "smtp.newsmanapp.com";
					}
							
					$tr = new Zend_Mail_Transport_Smtp($mail_server_host, $config);
					Zend_Mail::setDefaultTransport($tr);
					$mail = new Zend_Mail('UTF-8');

					if ( $assign_plain ) {
						$mail->setBodyText($tpl->body_plain);
					}
					
					$mail->setBodyHtml($tpl->body);

					//EF
					/*if ( $application_id == 1 ) {
						$list_id = '78';
					}
					//6A
					else */if ( $application_id == 2 ) {
						$list_id = '79';
					}
					//AE
					else if ( $application_id == APP_AE ) {
						$list_id = '97';
					}
					//EM
					else if ( $application_id == 33 ) {
						$list_id = '98';
					}
					//PC
					else if ( $application_id == 22 ) {
						$list_id = '98';
					}
					//BL
					/*else if ( $application_id == 30 ) {
						$list_id = '107';
					}*/
					//EG.CO.UK
					else if ( $application_id == 5 ) {
						$list_id = '109';
					}
					else if ( $application_id == 17 ) {
						$list_id = '147';
					}
					else if ( $application_id == APP_A6 ) {
						$list_id = '251';
					}
					else if ( $application_id == APP_A6_AT ) {
						$list_id = '251';
					}
					else if ( $application_id == APP_EG ) {
						$list_id = '315';
					} else if ( $application_id == APP_EI ) {
						$list_id = '341';
					}
					
					//$mail->addTo('dave.kasparov@gmail.com');
					if ( $application_id == APP_6B ) {
						/*$email_map = array(
							536 => 'ninit-61-43056c45f35097ec709be4c897a1b4d5@anuncio-m.com',
							537 => 'ninit-61-c2b760bd5328a876d1d10a8408c05f5f@anuncio-m.com',
							1136 => 'ninit-61-e31d0c2a05ebd56bc96db4a8a674a253@anuncio-m.com',
							8786 => 'ninit-61-8abc8195e5e14c6869d98b00d7d61572@anuncio-m.com',
							1548 => 'ninit-61-065c22c518457eab1e9e8d58a4b955fb@anuncio-m.com',
							5277 => 'ninit-61-008093bf53c2e32d9eb921679880dcfb@anuncio-m.com',
							47765 => 'ninit-61-ecccbe7f48d4e9d72485efae3d71bcb3@anuncio-m.com',
						);*/

						$email_map = array(
							48718 => 'ninit-61-29ee41e302d00f34a6ae799ea2d21e09@nln.6anuncio.com',
							48719 => 'ninit-61-34561e3aea7a03e166df0d71844cc820@nln.6anuncio.com',
							48717 => 'ninit-61-29507b7a86850abe21da389bb0a2dd61@nln.6anuncio.com',
						);
						
						foreach ($groups as $k => $group) {
							if ( $k == 0 ) {
								$mail->addTo($email_map[$group]);
							}
							else {
								$mail->addCc($email_map[$group]);
							}
						}
					}
					elseif ( $application_id == APP_ED ) {
						$email_map = array(
							47597 => 'ninit-61-a2b22fdaef497b0a49ef7fb15b926cef@ed-mime.com', 
							47598 => 'ninit-61-6ffc9d54818a3129b709ab70efec3a88@ed-mime.com', 
							22118 => 'ninit-61-0290b783569719695c9fbc35ea6102bd@ed-mime.com', 
							47592 => 'ninit-61-7f6b24693a8efc350ed7c5e6a5d96df2@ed-mime.com', 
							47594 => 'ninit-61-6e8013e784898cb6f0cd4d979bd32436@ed-mime.com',
							47593 => 'ninit-61-828876c86ce307a88a96f101b6298c51@ed-mime.com', 
							47595 => 'ninit-61-6e69d321bb8f20d4e289215ce3ca3762@ed-mime.com', 
							
							47602 => 'ninit-61-43254b91bc0f40630a190db8f9aa4701@ed-mime.com', 
							47604 => 'ninit-61-8c8712941c90c6fd79f7947edd1db519@ed-mime.com',
							47603 => 'ninit-61-bdf17e6e69231f4000b7184ad3e4da72@ed-mime.com', 
							47608 => 'ninit-61-41cc5398ba96fdacfef2c3d17f09fc06@ed-mime.com',
							49890 => 'ninit-61-6bc5edc7e8ad63e84ed503d461ca065e@ed-mime.com',

							50024 => 'ninit-61-c323f578706fad27fb6204bb8839a3cf@ed-mime.com',

							51429 => 'ninit-61-e29bd0766924c4661d0b7b707f9e768f@ed-mime.com',
							53675 => 'ninit-61-32a6f09de93c3ef65235fb45d68b2aea@ed-mime.com',
							53773 => 'ninit-61-eecc925bd1a43299f9a48958a1e32ed0@ed-mime.com',
							57183 => 'ninit-61-36be36334f5567e6376d5df41f75b02a@ed-mime.com',
							58975 => 'ninit-61-0084bfa3e04719996d899e5be08a794b@ed-mime.com',
							58976 => 'ninit-61-b738a472856a454e1faca63d42b33e3e@ed-mime.com',
							59294 => 'ninit-61-a08c472d8cf681afe18b1ec27d5b5e9d@ed-mime.com',
							59295 => 'ninit-61-5e986006536e366ca2948a4163d26e52@ed-mime.com',
							29295 => 'ninit-61-46ccfe35af59c611d406fbb1db56e980@ed-mime.com',
							29296 => 'ninit-61-004dfb96ed8df7e4eeaf79bd2366e635@ed-mime.com',
							29297 => 'ninit-61-378bf22668cab43983e7cb932224ece1@ed-mime.com',
							29298 => 'ninit-61-96a8c303d0571f459d23660bd430a1f1@ed-mime.com',
							29299 => 'ninit-61-0efbec61ac2b17e53ae6b1d851d4ebcc@ed-mime.com',
							29300 => 'ninit-61-9465b09dd872939f7b88c75fc2dcac06@ed-mime.com',

							29301 => 'ninit-61-affe10d64141ba372cd44141ff2b9db5@ed-mime.com',
							29302 => 'ninit-61-5493d67dfd7e8eb596a2796fbba7baf9@ed-mime.com',
							29303 => 'ninit-61-dfdd614cc08e978156eb57119a549cc3@ed-mime.com',
							29304 => 'ninit-61-f32c6d43a059b7000722496c49f8457d@ed-mime.com',

							29305 => 'ninit-61-faa9c53bb79650af6c8f558baaa016df@ed-mime.com',
							29306 => 'ninit-61-cda1589e0a3699bc7c2b2719cc4ab7b2@ed-mime.com',
							29307 => 'ninit-61-59b3fde44281074a01a8f8d190ce2f62@ed-mime.com',
							29308 => 'ninit-61-cf92d6f211d045f310d605e9769fde0e@ed-mime.com',

							29309 => 'ninit-61-3ae60f74c8a55b6035b2a94be57c6c2f@ed-mime.com',
							29310 => 'ninit-61-a7ae2b9ca1b17c172c529e2bd9849713@ed-mime.com',
							29311 => 'ninit-61-0d545682655959cbd37ba8244fad63f4@ed-mime.com',
							29312 => 'ninit-61-87e8fadbaa898b53b650949751f13fa9@ed-mime.com',

							29313 => 'ninit-61-eb3e98589460e2f5bba142bb5f6731a9@ed-mime.com',
							29314 => 'ninit-61-67892c72c19e7e1f4f4f6e02039314d5@ed-mime.com',
							29315 => 'ninit-61-736f24342539c1a26849d8dd8937ff72@ed-mime.com',
							29316 => 'ninit-61-2fd61045f22eae5fd17cb88ba0a38a41@ed-mime.com',

							29317 => 'ninit-61-f12db7c8a7c8d561017997f764c535c2@ed-mime.com',
							29318 => 'ninit-61-b8da067f4b2ea639a1b52386b9ca2883@ed-mime.com',
							29319 => 'ninit-61-4d302b5ab2267b36ad0134c6b7fa7b40@ed-mime.com',
							29320 => 'ninit-61-148bb4f66689396e351d90ab52d3a725@ed-mime.com',

							29321 => 'ninit-61-69b4a47218132b50023496122cfaa2a2@ed-mime.com',
							29322 => 'ninit-61-1ad107bb4b586366ca2a2471b7224e15@ed-mime.com',
							29323 => 'ninit-61-5cedfabff41df2312a91d2f36dbf1d9c@ed-mime.com',
							29324 => 'ninit-61-565bf155cc94bf30a256b290d4b2f923@ed-mime.com',

							29326 => 'ninit-61-40d32eccadd89aa0a4e0ebc74398d1a3@ed-mime.com',
							29327 => 'ninit-61-3c5d6a3450e155aa37db57f2a0ee0b31@ed-mime.com',
							29328 => 'ninit-61-160477af0386de39ffc1a460359201ba@ed-mime.com',

						);
						
						foreach ($groups as $k => $group) {
							if ( $k == 0 ) {
								$mail->addTo($email_map[$group]);
							}
							else {
								$mail->addCc($email_map[$group]);
							}
						}
					}
					elseif ( $application_id == APP_EF ) {
						$email_map = array(
							116 => 'ninit-61-9a0526673311a26cafab02156e2d7e43@escortforum-news1.com',
							117 => 'ninit-61-2525862b926c861393f1d14755b3cf43@escortforum-news1.com',
							118 => 'ninit-61-6479fb957d6569b05ec8119cea4be1ad@escortforum-news1.com',
							119 => 'ninit-61-05a81048d555a37334c6d8afb49e911a@escortforum-news1.com',
							1372 => 'ninit-61-9c62e1e5f1f2f316c028f52807831a57@escortforum-news1.com',
							1376 => 'ninit-61-9978c660f0a5c9096239ffc4342a29da@escortforum-news1.com',
							1377 => 'ninit-61-61679f4145d6351959bea6bfac28c623@escortforum-news1.com',
							1378 => 'ninit-61-422fda203dc7458a2efe8d595412388e@escortforum-news1.com',
							1379 => 'ninit-61-6b4a846459b245a25f0cb9fdc83f479e@escortforum-news1.com',
							1380 => 'ninit-61-4fd98c050d38d9104add2b9d3c624943@escortforum-news1.com',
							1381 => 'ninit-61-31c3d37d22bb927b85726fb71c89cb1a@escortforum-news1.com',
							1382 => 'ninit-61-4ed8ddaf4814011882edc2a27eb337b4@escortforum-news1.com',
							1383 => 'ninit-61-77f2a22e2f197cc3180b0f186da12e25@escortforum-news1.com',
						);
						
						foreach ($groups as $k => $group) {
							if ( $k == 0 ) {
								$mail->addTo($email_map[$group]);
							}
							else {
								$mail->addCc($email_map[$group]);
							}
						}
					}
					elseif ( $application_id == APP_A6 ) {
						$email_map = array(
							819 => 'ninit-61-4fcd5b7ae49e13459218809f6eba37f8@and6-mail.info',
							821 => 'ninit-61-5a6cb2070be33e83b5eae3c5cb82c363@and6-mail.info',
							829 => 'ninit-61-54bb7e907d1759e408afc2bcca098cdc@and6-mail.info'
						);
						
						foreach ($groups as $k => $group) {
							if ( $k == 0 ) {
								$mail->addTo($email_map[$group]);
							}
							else {
								$mail->addCc($email_map[$group]);
							}
						}
					}
					elseif ( $application_id == APP_EG ) {
						$email_map = array(
							1129 => 'ninit-61-91ff2a0042cc5be2d1bd1f9ddf6c1f9b@escortguide-news.com',
							1128 => 'ninit-61-3d122ae4f87827e102f4803dde4375b7@escortguide-news.com',
							1130 => 'ninit-61-6f5c8a5a97bcf9d42182e560d754ea3d@escortguide-news.com',
							1131 => 'ninit-61-966fa5b368a4d388e913d6e7b4082ab9@escortguide-news.com'
							//1128 => 'badalyano@gmail.com'
						);
						
						foreach ($groups as $k => $group) {
							if ( $k == 0 ) {
								$mail->addTo($email_map[$group]);
							}
							else {
								$mail->addCc($email_map[$group]);
							}
						}
					}
					elseif ( $application_id == APP_BL ) {
						$email_map = array(
							269 => 'ninit-61-179f771439b6688fde08993b9423f8e1@nln.beneluxxx.com',
							270 => 'ninit-61-7c4de1c8c5ca5f78322d50159da97c1d@nln.beneluxxx.com',
							271 => 'ninit-61-1b32429be15e6c6543c9c91b4a84f105@nln.beneluxxx.com',
							264 => 'ninit-61-a177070170006b40a66d176d244a509c@nln.beneluxxx.com',
							276 => 'ninit-61-ee67a31ea8ae815e9a4c02163f213869@nln.beneluxxx.com',
							289 => 'ninit-61-da96c72529b8d774fb261d0f33fbf686@nln.beneluxxx.com',
							239 => 'ninit-61-427d9c7438099515d109913d88a92c68@nln.beneluxxx.com',
							1010 => 'ninit-61-b1b6a8024b70bebb5cd9ec94e005a5a6@nln.beneluxxx.com',
							49605 => 'ninit-61-cf91be2698a7247b0fe5fe6ecb22ae78@nln.beneluxxx.com',
						);
						
						foreach ($groups as $k => $group) {
							if ( $k == 0 ) {
								$mail->addTo($email_map[$group]);
							}
							else {
								$mail->addCc($email_map[$group]);
							}
						}
					}
					
					// EG.CO.UK
					elseif ( $application_id == APP_EG_CO_UK ) {
						$email_map = array(
							260 => 'ninit-61-cc439d242ab859df73d39b7d0725917f@escortguide-news.com',
							261 => 'ninit-61-612e93dcbbe0afb817cbf9b74ca5806f@escortguide-news.com',
							262 => 'ninit-61-01c24b5be2a82abda993b76d3ff5f52d@escortguide-news.com',
							277 => 'ninit-61-585060fcc8fa281f268f476a3ba4adba@escortguide-news.com',
							263 => 'ninit-61-65e37f0e76c9676154815beaa21be75a@escortguide-news.com',
							53775 => 'ninit-61-92a53dfd96f5f4bbbc573900cffe7ee5@escortguide-news.com',
							264 => 'ninit-61-73790c694be527cc83fb86c912cd10d8@escortguide-news.com',
							265 => 'ninit-61-ff5503cdf999b1d0abc359353173a896@escortguide-news.com'
						);
						
						foreach ($groups as $k => $group) {
							if ( $k == 0 ) {
								$mail->addTo($email_map[$group]);
							}
							else {
								$mail->addCc($email_map[$group]);
							}
						}
					}
					
					elseif ( $application_id == APP_A6_AT ) {
						
					}
					/*elseif ( $application_id == APP_AE ) {
						$email_map = array(
							//222 => 'dave.kasparov@gmail.com'
							222 => 'ninit-61-b85d5747e7144f0f5c36989b3dac194b@asianescorts-newsl.com'
						);
						
						foreach ($groups as $k => $group) {
							if ( $k == 0 ) {
								$mail->addTo($email_map[$group]);
							}
							else {
								$mail->addCc($email_map[$group]);
							}
						}
					}*/
					elseif ( $application_id == APP_EM ) {
						//$mail->addTo('ninit-61-bd7db0391e877dbe7385ef52683a6612@escomeet.com');
						//$mail->addTo('ninit-61-99c07e40e7b124b1646a59b9dc62a0fa@escomeet.com'); // test
						$email_map = array(
							241 => 'ninit-61-6405aa848d57d2a413c14493335188d6@escmeet.com',							
							265 => 'ninit-61-3d172d676afe4cc945e294031ea3a17c@escmeet.com',							
							1142 => 'ninit-61-bd7db0391e877dbe7385ef52683a6612@escmeet.com',
							272 => 'ninit-61-87f08302f1a1b9ee59f9726e455ad2a7@escmeet.com',
							36310 => 'ninit-61-4831fad1ab42f9c15e9067fbaf6db40d@escmeet.com',
							36329 => 'ninit-61-4a44dcd92ca6b38dafc0d3a21e276803@escmeet.com',
							275 => 'ninit-61-0922845bc5bfcea1003d73bc088dc6e6@escmeet.com',
							274 => 'ninit-61-d50ea8416ac9532dbf861ea658a87395@escmeet.com',
							268 => 'ninit-61-576bf1d3da3b7dee8fa57e67f1499181@escmeet.com',
							266 => 'ninit-61-858591ba6dd7a1af88f94da49cdd4585@escmeet.com',
							273 => 'ninit-61-9d3f5fe3225ae9abbd3a28331de868b3@escmeet.com',
							283 => 'ninit-61-99c07e40e7b124b1646a59b9dc62a0fa@escmeet.com',
							37694 => 'ninit-61-fd2ca29560addc3e7b28d1ab1cce36a8@escmeet.com',
							36337 => 'ninit-61-136ff1ea7d8417d190e126abefe61b86@escmeet.com',
							267 => 'ninit-61-94b9521e841d66d2f1e312c6fba4393b@escmeet.com',
						);
						
						foreach ($groups as $k => $group) {
							if ( $k == 0 ) {
								$mail->addTo($email_map[$group]);
							}
							else {
								$mail->addCc($email_map[$group]);
							}
						}
					}
					elseif ( $application_id == APP_EI ) {
						$email_map = array(
							56121 => 'ninit-61-893a5410d0316056d13caf6c9ee1e11c@nzsrv.com',
							56158 => 'ninit-61-a56da82d8a02172ae3652f61a1826171@escoit.com',
							56160 => 'ninit-61-f59fba52753e645e3b767f112c6f4301@escoit.com',
							56161 => 'ninit-61-7c73c989f9e400ee15f5b2e7c126d305@escoit.com'
						);
						
						foreach ($groups as $k => $group) {
							if ( $k == 0 ) {
								$mail->addTo($email_map[$group]);
							}
							else {
								$mail->addCc($email_map[$group]);
							}
						}
					}
					else {
						if ( $application_id == APP_AE ) {
							$mail->addTo('ninit-' . $list_id . '-' . $groups[0] . '@asianescorts-news.com');
							//var_dump('ninit-' . $list_id . '-' . $groups[0] . '@newsman.ro'); die;
						}
						else {
							$mail->addTo('ninit-' . $list_id . '-' . $groups[0] . '@nzsrv.com');
							//$mail->addTo('dave.kasparov@gmail.com');
							//var_dump('ninit-' . $list_id . '-' . $groups[0] . '@newsman.ro'); die;
						}

						if ( count($groups) > 1 ) {
							unset($groups[0]);
							foreach ($groups as $group) {
								if ( $application_id == 11 ) {
									$mail->addCc('ninit-' . $list_id . '-' . $group . '@asianescorts-news.com');
								}
								else {
									$mail->addCc('ninit-' . $list_id . '-' . $group . '@nzsrv.com');
								}
							}
						}
					}
					
					$mail->setSubject($subject);

					if ( ! is_null($application_id) ) {
						$mail_from_name = Cubix_Application::getById()->title;
						/*if ( $application_id == 1 ) {
							$mail_from_name = 'escortforum';
						}
						else if ( $application_id == 2 ) {
							$mail_from_name = '6annonce';
						}
						else if ( $application_id == 11 ) {
							$mail_from_name = 'asianescorts';
						}
						else if ( $application_id == 5 ) {
							$mail_from_name = 'escortguide.co.uk';
						}*/
					}

					$mail->setFrom($from_email, $from_name);

					$mail->send();
				}
				else if ( $send_by == 'marketer' ) {
					///////////////////////////////////////////
					$newsletter_api = new Cubix_NewsletterApi;


					$params['requesttype'] = 'subscribers';
					$params['requestmethod'] = 'CreateNewsletter';

					$params['details'] = array(
						'n_name' => $tpl->title,
						'n_subject' => /*$tpl->subject*/$subject,
						'n_textbody' => 'Text body',
						'n_htmlbody' => base64_encode($tpl->body)
					);

					$ret = $newsletter_api->process($params);

					$status = $ret['status'];
					$data = (array)$ret['data'];

					if ($status[0] == 'SUCCESS')
					{
						$t_id = $data[0];
					}
					else
					{
						$t_id = null;
						print_r($data);
					}
					//////////////////////////////////////////

					if (is_null($t_id))
					{
						die;
					}

					$request = $this->_request;

					if ($request->send_time == 'now')
						$time = date('Y-m-d H:i:00', time() + 10 * 60);
					elseif ($request->send_time == 'custom')
						$time = date('Y-m-d H:i:00', $request->custom_time);
					///////////////////////////////////////////
					$params3['requesttype'] = 'subscribers';
					$params3['requestmethod'] = 'send';

					$params3['details'] = array(
						's_mailinglists' => implode(',', $request->groups),
						's_newsletter' => $t_id,
						's_sendstarttime' => $time,
						's_sendfromname' => $request->from_name,
						's_sendfromemail' => $request->from_email
					);

					$ret = $newsletter_api->process($params3);

					$status = $ret['status'];
					$data = (array)$ret['data'];

					if ($status[0] == 'SUCCESS')
					{

					}
					else
					{
						print_r($data);
						die;
					}
					//////////////////////////////////////////
				}
			}

			die(json_encode($validator->getStatus()));
		}
	}
	
	public function loadAction()
	{
		$tpl_id = intval($this->_getParam('template_id'));
		if ( ! $tpl_id ) die;
		
		$tpl = $this->_model->getById($tpl_id);
		$tpl->from_name = '';
		//$tpl = $obj;

		$this->view->vars = $tpl->getVars();
		$this->view->tpl = $tpl;
		
		$m_groups = new Model_Newsletter_Groups();
		
		///////////////////////////////////////////
		$newsletter_api = new Cubix_NewsletterApi;

		$params2['requesttype'] = 'user';
		$params2['requestmethod'] = 'GetLists';

		$ret = $newsletter_api->process($params2);

		$arr = array();

		$status = $ret['status'];
		$data = (array)$ret['data'];
		$data = $data['item'];
		
		if ($status[0] == 'SUCCESS')
		{
			foreach ($data as $item)
			{
				$item = (array)$item;
				
				$id = $item['listid'];
				$name = $item['name'];

				$obj = new Model_Newsletter_TemplateItem();

				$obj->id = $id;
				$obj->group_title = $name;

				$arr[] = $obj;
			}
		}
		else
		{
			//error
		}
		
		////////////////////////////////////////////////////////
		//$this->view->groups = $m_groups->getList();
		$this->view->groups = $arr;

		//$m_subscribers = new Model_Newsletter_Subscribers();
		//$res = $m_subscribers->noInGroups();

		//$this->view->no_group_count = intval($res['count']);
	}
	
	public function escortPickerAction()
	{
		$m = new Model_EscortsV2();

		$filter =  array(
			'filter_by' => 'registration_date',
			'e.status' => 32,
			'active_package_expr_date' => true,
			'active_package_id' => 'with_package',
			'date_from' => strtotime($this->_interval),
			'u.application_id' => $this->_getParam('application_id'),
			'e.gender' => 1
		);

		if ( Cubix_Application::getId() == APP_BL ) {
			unset($filter['e.gender']);
		}
		
		if ( Cubix_Application::getId() != 1 && Cubix_Application::getId() != 2 ) {
			unset($filter['active_package_expr_date']);
			unset($filter['active_package_id']);
		}
		
		$count = 0;
		$escorts = $m->getAll(1, 1000, $filter, 'date_registered', 'DESC', $count);
		$this->view->count = $count;
		$this->view->escorts = $escorts;
	}

	public function tourEscortPickerAction()
	{
		$m = new Model_EscortsV2();

		$filter =  array(
			'filter_by' => 'registration_date',
			'e.status' => 32,
			'only_city_tour' => true,
			/*'active_package_expr_date' => true,
			'active_package_id' => 'with_package',*/			
			'u.application_id' => $this->_getParam('application_id'),
			//'e.gender' => 1,
		);
		
		if ( Cubix_Application::getId() != 1 && Cubix_Application::getId() != 2 ) {
			unset($filter['active_package_expr_date']);
			unset($filter['active_package_id']);
		}
		
		$count = 0;
		$escorts = $m->getAll(1, 1000, $filter, 'date_registered', 'DESC', $count);
		$this->view->count = $count;
		$this->view->escorts = $escorts;
	}
	
	public function previewAction()
	{
		$this->view->layout()->disableLayout();
		
		$template_id = intval($this->_getParam('template_id'));
		
		if ( ! $template_id ) {
			return;
		}

		$this->view->url = Cubix_Application::getById($application_id)->url;

		$escorts_data = (array) $this->_getParam('escorts', array());
		$escorts = $this->_model->makeEscortList($escorts_data);
		
		$this->view->escorts = $escorts;
		$html = $this->view->render('xtemplates/tpl-escorts.phtml');
		
		$tpl = $this->_model->getById($template_id);
		
		$tpl->body = str_replace('{ESCORT_LIST}', $html, $tpl->body);
		$tpl->body = str_replace('{TOUR_ESCORT_LIST}', $html, $tpl->body);

		$vars = $this->_getParam('vars');
		//Template Vars
		if ($vars)
		{
			foreach ($vars as $k => $var)
			{
				$tpl->body = preg_replace("#{EDITABLE_$k}([\s\S]+?){/EDITABLE_$k}#", $var, $tpl->body);
			}
		}
				
		die($tpl->body);
	}
}
