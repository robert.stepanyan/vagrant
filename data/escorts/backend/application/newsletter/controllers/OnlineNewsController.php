<?php

class Newsletter_OnlineNewsController extends Zend_Controller_Action
{
	
	
	//protected $_tpl_vars = array();
	
	public function init()
	{
		$bc_user = Zend_Auth::getInstance()->getIdentity();
		if($bc_user->type != 'superadmin' ){
			die;
		}
		$this->_model = new Model_Newsletter_OnlineNews();
	
	}
	
	public function indexAction()
	{ 
		
	}
	
	public function dataAction()
	{
		$req = $this->_request;

		$filter = array(
			'application_id' => $this->_request->application_id
		);

		$data = $this->_model->getAll(
			$this->_request->page,
			$this->_request->per_page,
			$filter,
			$this->_request->sort_field,
			$this->_request->sort_dir,
			$count
		);

		die(json_encode(array('data' => $data, 'count' => $count)));
	}
	
	public function createAction()
	{
		$this->view->layout()->disableLayout();
		
		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'title' => '',
				'activation_type' => '',
				'start_date' => '',
				'end_date' => '',
				
			));
			$subjects = Cubix_I18n::getValues($this->_request->getParams(), 'subject', '');
			$data = $data->getData();
			$data =array_merge($data,$subjects);
			
			$validator = new Cubix_Validator();
			
			if ( ! strlen($data['title']) ) {
				$validator->setError('title', 'Required');
			}
			if ( ! strlen($data['subject_en']) ) {
				$validator->setError('subject', 'Required');
			}
			
			if ( ! strlen($data['activation_type']) ) {
				$validator->setError('activation_type', 'Required');
			}
			
			if($data['start_date'] > $data['end_date']){
				$validator->setError('date_error_message', 'Wrong Date');
			}
			
			if($data['activation_type'] == Model_Newsletter_OnlineNews::ACTIVATION_TYPE_IMMEDIATELY){
				$this->_model->disableAllActive();
				$data['start_date'] = strtotime(date("Y-m-d"));
				$data['status'] = Model_Newsletter_OnlineNews::STATUS_ACTIVE;
				
			}
			elseif($data['activation_type'] == Model_Newsletter_OnlineNews::ACTIVATION_TYPE_EXACT_DATE){
				
				if($data['start_date'] > $data['end_date']){
					$validator->setError('date_error_message', 'Wrong Date Interval');
				}
				else{
					$online_news_dates = $this->_model->getAllDates();

					foreach ( $online_news_dates as $date ) {
						if ( $date->id == $id ) continue;

						if ( ( strtotime($date->start_date) >= $data['start_date'] && strtotime($date->start_date) <= $data['end_devdate']) || (strtotime($date->end_date) >= $data['start_date'] && strtotime($date->end_date) <= $data['end_date']) ) {
							$validator->setError('date_error_message', 'date intervals are overlapped with Online News ID '.$date->id);

							break;
						}
					}
				}
				$data['status'] = Model_Newsletter_OnlineNews::STATUS_PENDING;
			}
			
			$data['application_id'] = Cubix_Application::getId();
			
			if ( $validator->isValid() ) 
			{
				$this->_model->save($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function editAction()
	{
		$this->view->layout()->disableLayout();
		
		$id = intval($this->_request->id);
		
		$this->view->item = $this->_model->getById($id);
		
		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'id' => 'int',
				'title' => '',
				'activation_type' => '',
				'start_date' => '',
				'end_date' => '',
				
			));
			$subjects = Cubix_I18n::getValues($this->_request->getParams(), 'subject', '');
			$data = $data->getData();
			$data =array_merge($data,$subjects);
			
			$validator = new Cubix_Validator();
			
			if ( ! strlen($data['title']) ) {
				$validator->setError('title', 'Required');
			}
						
			if ( ! strlen($data['subject_en']) ) {
				$validator->setError('subject_en', 'Required');
			}
			
			
			
			if ( ! strlen($data['activation_type']) ) {
				$validator->setError('activation_type', 'Required');
			}
			
			if($data['activation_type'] == Model_Newsletter_OnlineNews::ACTIVATION_TYPE_IMMEDIATELY){
				$this->_model->disableAllActive();
				$data['start_date'] = strtotime(date("Y-m-d"));
				$data['status'] = Model_Newsletter_OnlineNews::STATUS_ACTIVE;
				
			}
			elseif($data['activation_type'] == Model_Newsletter_OnlineNews::ACTIVATION_TYPE_EXACT_DATE){
				
				if($data['start_date'] > $data['end_date']){
					$validator->setError('date_error_message', 'Wrong Date Interval');
				}
				else{
					$online_news_dates = $this->_model->getAllDates();

					foreach ( $online_news_dates as $date ) {
						if ( $date->id == $id ) continue;

						if ( ( strtotime($date->start_date) >= $data['start_date'] && strtotime($date->start_date) <= $data['end_devdate']) || (strtotime($date->end_date) >= $data['start_date'] && strtotime($date->end_date) <= $data['end_date']) ) {
							$validator->setError('date_error_message', 'date intervals are overlapped with Online News ID '.$date->id);

							break;
						}
					}
				}
				$data['status'] = Model_Newsletter_OnlineNews::STATUS_PENDING;
			}
			
			$data['application_id'] = Cubix_Application::getId();
			
			if ( $validator->isValid() )
			{
				$this->_model->save($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function deleteAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->_model->remove($id);

		die;
	}
	
	public function toggleActiveAction()
	{
		$id = intval($this->_request->id);
		$status = intval($this->_request->status);
		
		if($status == Model_Newsletter_OnlineNews::STATUS_ACTIVE){
			$this->_model->disableAllActive();
		}
		$this->_model->setStatus($id, $status);
		
	}		
	
}
