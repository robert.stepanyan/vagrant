<?php

set_time_limit(0);

class Newsletter_XnewsletterController extends Zend_Controller_Action
{
	// Lists
	// 6annone.com-agencies
	// 6annone.com-escorts
	// 6annone.com-members

	protected $_prefix = '6annone.com-';

	public function emailSyncCronAction()
	{
		if (Cubix_Application::getId() == 1)
			$this->_prefix = '';
		
		$m = new Cubix_NewsletterApi();
		
		$lists = $this->_getLists();

		if ($lists)
		{
			$items = $m->getNewsletterLog();

			if ($items)
			{
				$start = time();
				$ids = array();
				$addSucCount = $addErrCount = $editSucCount = $editErrCount = $deleteSucCount = $deleteErrCount = 0;

				foreach ($items as $item)
				{
					$item = (array)$item;
					$ids[] = $item['id'];
					$user_id = $item['user_id'];

					switch ($item['user_type'])
					{
						case 'escort':
							$list_id = $lists[$this->_prefix . 'escorts'];
							break;
						case 'agency':
							$list_id = $lists[$this->_prefix . 'agencies'];
							break;
						case 'member':
							$list_id = $lists[$this->_prefix . 'members'];
							break;
						default:
							continue;
					}

					switch ($item['action'])
					{
						case 'add':
							if ($this->_isInList($list_id, $item['new_email']) == 'no')
							{
								$name = $this->_getUsername($user_id);
								
								if (is_null($name))
									$name = '';

								if ($this->_addToList($list_id, $item['new_email'], $name))
									$addSucCount ++;
								else
									$addErrCount ++;
							}
							else
								$addErrCount ++;
							break;
						case 'edit':
							if ($this->_isInList($list_id, $item['old_email']) == 'yes')
							{
								if (!$this->_deleteFromList($list_id, $item['old_email']))
								{
									$editErrCount ++;
									continue;
								}

								if ($this->_isInList($list_id, $item['new_email']) == 'no')
								{
									$name = $this->_getUsername($user_id);

									if (is_null($name))
										$name = '';

									if (!$this->_addToList($list_id, $item['new_email'], $name))
									{
										$editErrCount ++;
										continue;
									}
								}
								else
								{
									$editErrCount ++;
									continue;
								}

								$editSucCount ++;
							}
							else
								$editErrCount ++;

							break;
						case 'delete':
							if ($this->_isInList($list_id, $item['old_email']) == 'yes')
							{
								if ($this->_deleteFromList($list_id, $item['old_email']))
									$deleteSucCount ++;
								else
									$deleteErrCount ++;
							}
							else
								$deleteErrCount ++;

							break;
						default:
							continue;
					}
				}

				$m->changeNewslettersLogStatus(implode(',', $ids));

				$end = time();
				$time = $end - $start; // in seconds
				$min = (int)($time / 60);
				$sec = $time % 60;

				echo '
					Done!!! <br/>
					Add: Suc = ' . $addSucCount . ', Err = ' . $addErrCount . '<br/>
					Edit: Suc = ' . $editSucCount . ', Err = ' . $editErrCount . '<br/>
					Delete: Suc = ' . $deleteSucCount . ', Err = ' . $deleteErrCount . '<br/><br/>
					Total Count = ' . ($addSucCount + $addErrCount + $editSucCount + $editErrCount + $deleteSucCount + $deleteErrCount) . '<br/><br/>
					Time = ' . $time . ' seconds: ' . $min . ' min, ' . $sec . ' sec
				';


			}
			else
			{
				echo 'No any emails at this moment.';
			}
		}
		else
		{
			echo 'Error: not response from getLists';
		}

		die;
	}

	protected function _getLists()
	{
		$newsletter_api = new Cubix_NewsletterApi();
		
		$params['requesttype'] = 'user';
		$params['requestmethod'] = 'GetLists';

		$ret = $newsletter_api->process($params);

		$arr = array();

		$status = $ret['status'];
		$data = (array)$ret['data'];
		$data = $data['item'];
		$count = 0;

		if ($status[0] == 'SUCCESS')
		{
			foreach ($data as $item)
			{
				$item = (array)$item;

				$id = $item['listid'];
				$name = $item['name'];

				$arr[$name] = $id;
			}
		}
		else
			return false;

		return $arr;
	}

	protected function _getUsername($user_id)
	{
		$m = new Model_Users();
		$user = $m->get($user_id);

		if ($user)
		{
			return $user->username;
		}

		return null;
	}

	protected function _addToList($list_id, $email, $name = null)
	{
		$newsletter_api = new Cubix_NewsletterApi;

		$params['requesttype'] = 'subscribers';
		$params['requestmethod'] = 'AddSubscriberToList';

		$params['details'] = array(
			'emailaddress' => $email,
			'mailinglist' => $list_id,
			'format' => 'h',
			'confirmed' => 1,
			'customfields' => array(
				'fieldid' => 2,
				'value' => $name
			)
		);

		$ret = $newsletter_api->process($params);

		$status = $ret['status'];

		if ($status[0] == 'SUCCESS')
			return true;
		else
			return false;
	}

	protected function _isInList($list_id, $email)
	{
		$newsletter_api = new Cubix_NewsletterApi;

		$params['requesttype'] = 'subscribers';
		$params['requestmethod'] = 'IsSubscriberOnList';

		$params['details'] = array(
			'Email' => $email,
			'List' => $list_id
		);

		$ret = $newsletter_api->process($params);

		$status = $ret['status'];
		$data = (array)$ret['data'];

		if ($status[0] == 'SUCCESS')
		{
			if (is_array($data) && count($data) > 0)
				return 'yes';
			else
				return 'no';
		}
		else
			return 'error';
	}

	protected function _deleteFromList($list_id, $email)
	{
		$newsletter_api = new Cubix_NewsletterApi;

		$params['requesttype'] = 'subscribers';
		$params['requestmethod'] = 'DeleteSubscriber';

		$params['details'] = array(
			'emailaddress' => $email,
			'list' => $list_id
		);

		$ret = $newsletter_api->process($params);

		$status = $ret['status'];

		if ($status[0] == 'SUCCESS')
			return true;
		else
			return false;
	}
}