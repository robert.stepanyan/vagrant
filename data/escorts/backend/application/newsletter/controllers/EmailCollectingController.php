<?php

class Newsletter_EmailCollectingController extends Zend_Controller_Action
{
	/**
	 * @var Model_Seo_Entities
	 */
	protected $_model;
		
	public function init()
	{
		$this->_model = new Model_Newsletter_EmailCollecting();
	}
	
	public function indexAction()
	{
		
	}

	public function dataAction()
	{
		$req = $this->_request;

		$data = $this->_model->getAll(
			$req->page,
			$req->per_page,
			$req->sort_field,
			$req->sort_dir,
			$count,
			array('email' => $req->email, 'date_from' => $req->date_from, 'date_to' => $req->date_to)
		);

		die(json_encode(array('data' => $data, 'count' => $count)));
	}

	public function addCollectingImagesAction(){
		$req = $this->_request;

		$idsString = $req->ids;
		$escort_id = $req->escort_id;
		
		$ids = explode(",", $idsString);
		if(count($ids) > 0){
			$this->_model->addCollectingImages($escort_id, $ids);
		}

		return true;
	}

	public function imagesAction(){
		$this->view->layout()->disableLayout();

		$photos = $this->_model->getAllPhoto();

		$this->view->photos = $photos;
	}

	public function removeImageAction()
	{	
		$id = intval($this->_request->id);
		$this->_model->removeImage($id);
		
		die;
	}
}
