<?php

set_time_limit(0);

class Newsletter_NewsmanCronController extends Zend_Controller_Action
{
	public function emailSyncCronAction()
	{
		ini_set('memory_limit', '1024M');
		set_time_limit(0);
		
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$cli = new Cubix_Cli();
		$pid_file = '/var/run/newsman-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}
		
		$cli->clear();
		
		$db = Zend_Registry::get('db');
		
		$app_id = Cubix_Application::getId();
		
		// EF
		if ( $app_id == 1 ) {
			$list_id = 78;
			$segments = array(
				'agency' => 117,
				'member' => 116,
				'escort' => 118
			);
		}
		//6A
		elseif ( $app_id == 2 ) {
			$list_id = 79;
			$segments = array(
				'agency' => 120,
				'member' => 122,
				'escort' => 121
			);
		}
		// AE
		elseif ( $app_id == 11 ) {
			$list_id = 97;
			$segments = array(
				'agency' => 221,
				'member' => 219,
				'escort' => 220
			);
		}
		//EM
		elseif ( $app_id == 33 ) {
			$list_id = 98;
			$segments = array(
				'agency' => 267,
				'member' => 265,
				'escort' => 266
			);
		}
		//PC
		elseif ( $app_id == 22 ) {
			$list_id = 98;
			$segments = array(
				'agency' => 267,
				'member' => 265,
				'escort' => 266
			);
		}
		//BL
		elseif ( $app_id == 30 ) {
			$list_id = 107;
			$segments = array(
				'agency' => 271,
				'member' => 269,
				'escort' => 270
			);
		}
		// EG.CO.UK
		elseif ( $app_id == 5 ) {
			$list_id = 109;
			$segments = array(
				'agency' => 260,
				'member' => 261,
				'escort' => 262
			);
		}
		
		$groups = array('agency', 'escort', 'member');
		
		$sql = "SELECT id, email, username FROM users WHERE user_type = ? AND application_id = ? AND newsman_added = 0 AND email IS NOT NULL AND email <> ''";
		
		
		$nm = new Model_Newsletter_Newsman();
				
		//var_dump($m->unsubscribe(78, 'review@6annonce.com'));
		
		foreach ( $groups as $group ) {
			$result = $db->fetchAll($sql, array($group, $app_id));
			
			$count = count($result);			
			if ( $count > 0 ) {
				foreach ( $result as $k => $res ) {
					/*echo $list_id;
					echo $res->email;
					echo $res->username;die;*/
					$subscriber_id = $nm->subscribe($list_id, $res->email, $res->username);
					$is_added = $nm->bindToSegment($subscriber_id, $segments[$group]);
					if ( $is_added ) {
						$cli->colorize('green')->out($k . "/" . $count . " Added - Subscriber " . $res->email . " - " . $res->username . " added to NEWSMAN database ! newsman_id = " . $subscriber_id . " type = " . $group)->reset()->out();
						$db->update('users', array('newsman_added' => 1), $db->quoteInto('id = ?', $res->id));
					}
					else {
						$cli->colorize('red')->out($k . "/" . $count . " Error - Subscriber " . $res->email . " - " . $res->username . " not added to NEWSMAN database ! type = " . $group)->reset()->out();
						$db->update('users', array('newsman_added' => 1), $db->quoteInto('id = ?', $res->id));
					}
				}
			}
		}
		
		die;
	}
}