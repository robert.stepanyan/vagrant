<?php

class Newsletter_TemplatesV2Controller extends Zend_Controller_Action 
{
	protected $_model;

	
	const AUTH_MAIL_SERVER_HOST = "m1.czsrv.com";
	const AUTH_MAIL_SERVER_PORT = 465;
	const AUTH_MAIL_SERVER_AUTH = true;
	const AUTH_MAIL_SERVER_USER = "info@6anuncio.com";
	const AUTH_MAIL_SERVER_PASS = "bristhe#zzk";
	const AUTH_MAIL_FROM = "info@asianescorts.com";
	const AUTH_MAIL_FROM_NAME = "asianescorts.com";
	
	const MAIL_SERVER_HOST = "localhost";
	const MAIL_SERVER_PORT = 25;
	const MAIL_SERVER_AUTH = false;
	const MAIL_SERVER_USER = "";
	const MAIL_SERVER_PASS = "";
	const MAIL_FROM = "info@escortguide.com";
	const MAIL_FROM_NAME = "6annonce";
	
	//protected $_tpl_vars = array();
	
	public function init()
	{
		$this->_model = new Model_Newsletter_Templates();
	}
	
	public function indexAction()
	{
		
	}
	
	public function dataAction()
	{
		$req = $this->_request;

		$filter = array(
			'application_id' => $this->_request->application_id,
			'is_v2'	=> 1
		);

		$data = $this->_model->getAll(
			$this->_request->page,
			$this->_request->per_page,
			$filter,
			$this->_request->sort_field,
			$this->_request->sort_dir,
			$count
		);

		die(json_encode(array('data' => $data, 'count' => $count)));
	}
	
	public function createAction()
	{
		$this->view->layout()->disableLayout();
		$this->view->layouts = $this->_model->getLayouts();
		
		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'title' => '',
				'subject' => '',
				'from_email' => '',
				'body' => '',
				'application_id' => 'int'
			));
			$data = $data->getData();
			
			$validator = new Cubix_Validator();
			
			if ( ! strlen($data['title']) ) {
				$validator->setError('title', 'Required');
			}
			elseif ($this->_model->checkTitle($data['title']))
			{
				$validator->setError('title', 'Title exists, please choose another one');
			}
			
			if ( ! strlen($data['from_email']) ) {
				$validator->setError('from_email', 'Required');
			}

			if ( ! strlen($data['body']) ) {
				$validator->setError('body', 'Required');
			}
			
			if ( $validator->isValid() ) 
			{
				$data['is_v2'] = 1;
				$this->_model->save($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	
	public function editAction()
	{
		$this->view->layout()->disableLayout();
		
		$id = intval($this->_request->id);
		
		$this->view->item = $template = $this->_model->getById($id);
				
		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'id' => 'int',
				'title' => '',
				'subject' => '',
				'from_email' => '',
				'body' => '',
				'application_id' => 'int'
			));
			$data = $data->getData();
			//echo $template->title;die;
			$validator = new Cubix_Validator();
			
			if ( ! strlen($data['title']) ) {
				$validator->setError('title', 'Required');
			}
			elseif ($this->_model->checkTitle($data['title']) && ($template->title != $data['title']))
			{
				$validator->setError('title', 'Title exists, please choose another one');
			}

			if ( ! strlen($data['from_email']) ) {
				$validator->setError('from_email', 'Required');
			}

			if ( ! strlen($data['body']) ) {
				$validator->setError('body', 'Required');
			}
			
			if ( $validator->isValid() )
			{
				//$data['body'] = $validator->removeEmoji($data['body']);
				$this->_model->save($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function deleteAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->_model->remove($id);

		die;
	}
	
	public function getLayoutAction()
	{
		$id = $this->_request->id;
		$layout = ($this->_model->getLayoutById($id));
		die(json_encode($layout));
	}
	
	public function constructAction()
	{
		$this->view->layout()->disableLayout();
	}
	
	public function uploadAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		try {
			$response = array(
				'error'	=> 0,
				'msg' => '',
				'finish'	=> FALSE
			);
			
			
			$file = $_FILES['Filedata']['tmp_name'];
			$file = array(
				'tmp_name' => $file,
				'name' => $_FILES['Filedata']['name']
			);
			
			$images = new Cubix_ImagesCommon();
			$photo = $images->save($file);
			$img = new Cubix_ImagesCommonItem($photo);
			
			
			$response['image_id'] = $img->getImageId();
			$response['photo'] = $img->getUrl($this->_request->size);
			$response['finish'] = TRUE;
			
		} catch (Exception $e) {
			$response['error']	= 1;
			$response['msg']	= $e->getMessage();
		}
		
		echo json_encode($response);die;
	}
	
	public function escortPickerAction()
	{
		$m = new Model_EscortsV2();
		
		$filter =  array(
			'id' => (int) $this->_request->escort_id,
			'status' => 32
		);
		
		$this->view->nsl_img_size = $this->_request->nsl_img_size;

		$escorts = $m->getEscortPicker($filter);
		$this->view->escorts = $escorts;
	}

	public function blogPickerAction()
	{
		$m = new Model_Blog();
		$id = (int) $this->_request->post_id;
		$size = $this->_request->nsl_img_size;
		$lang = $this->_request->lang;
		$post = $m->getForBlogPicker($id,$size,$lang);
		die(json_encode($post));
	}
}
