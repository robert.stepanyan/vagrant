<?php

class EBSMS_PhonesController extends Zend_Controller_Action
{
	public function init()
	{
		$this->model = new Model_EBSMS_Phones();
	}
	
	public function indexAction()
	{
		
	}
	
	public function dataAction()
	{
		$page = $this->_getParam('page');
		$per_page = $this->_getParam('per_page');
		$sort_field = $this->_getParam('sort_field');
		$sort_dir = $this->_getParam('sort_dir');
		$count = 0;
		
		$current_app_id = intval($this->_getParam('application_id'));
		
		$filter = array();
		if ( $current_app_id != 0 )
		{
			$filter['a.id'] = $current_app_id;
		}
		
		$data = $this->model->getAll( $page, $per_page, $sort_field, $sort_dir, $filter, $count);
		
		echo json_encode(array('data' => $data, 'count' => $count));
		die;
	}
	
	public function editAction()
	{
		$id = intval($this->_getParam('id'));
		
		if ( $this->_request->isPost() ) {
			$phone_number = $this->_getParam('phone_number');
			
			$validator = new Cubix_Validator();
						
			if ( ! strlen($phone_number) ) {
				$validator->setError('phone_number', 'Phone number is required');
			}
			
			if ( $validator->isValid() ) {
				$this->model->save(new Model_EBSMS_PhonesItem(array(
					'id'             => $id,					
					'phone_number'   => $phone_number
				)));
			}
			
			die(json_encode($validator->getStatus()));
		}
		else {
			$this->view->phone_num = $this->model->get($id);
		}
	}
}

