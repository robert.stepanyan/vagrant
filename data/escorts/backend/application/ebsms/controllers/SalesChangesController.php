<?php

class Ebsms_SalesChangesController extends Zend_Controller_Action {

    public function init()
    {
        $this->model = new Model_EBSMS_SalesChanges();
    }

    public function indexAction()
    {
        $bu_user = Zend_Auth::getInstance()->getIdentity();
    }

    public function dataAction()
    {
        set_time_limit(0 );
        $buser = Zend_Auth::getInstance()->getIdentity();
        $buserType = $buser['type'];
        $page = $this->_getParam('page' );
        $per_page = $this->_getParam('per_page' );
        $sort_field = $this->_getParam('sort_field' );
        $sort_dir = $this->_getParam('sort_dir' );

        $req = $this->_request;

        $filter = array(
            'escort_id' => $req->escort_id,
            'agency_id' => $req->agency_id,
        );

        $count = 0;
        $data = $this->model->getAll( $page, $per_page , $buser->id , $sort_field , $sort_dir , $filter , $count ,$buserType);
        echo json_encode(array( 'data' => $data , 'count' => $count ) );
        die;
    }


    public function cancelAction()
    {
        $req = $this->_request;
        $result = $this->model->declineSalesChangingRequest( $req->getParam('id') );
        if ( empty( $result ) )
        {
            die ( json_encode( array( 'compleate' => true) ) );
        }else{
            die ( json_encode( array( 'error' => 'Something went wrong' ) ) );
        }
    }

    public function acceptAction()
    {
        $id = $this->_request->getParam('id' );
        $escort_id = $this->_request->getParam('escort_id' );
        $agency_id = $this->_request->getParam('agency_id' );
        $user_id = $this->_request->getParam('user_id' );
        $salesToId = $this->_request->getParam('sales_to_id' );

        $this->model->acceptSalesChangingRequest( $id );

        $result = $this->model->changeSalesAgentInUsersTable( $user_id , $salesToId , $escort_id , $agency_id );

        if ( empty( $result ) )
        {
            die ( json_encode( array( 'error' => $result ) ) );
        }else{
            die ( json_encode( array( 'error' => $result ) ) );
        }
    }


}
