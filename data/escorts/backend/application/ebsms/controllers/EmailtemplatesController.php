<?php

class Ebsms_EmailtemplatesController extends Zend_Controller_Action {
	
	public function init()
	{
		$this->_model = new Model_EBSMS_EmailTemplates();
	}
	
	public function indexAction()
	{
		
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'application_id' => $this->_request->application_id
		);
		
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		if (!in_array($bu_user->type, array('superadmin', 'admin')))
			$filter['backend_user_id'] = $bu_user->id;
		
		$data = $this->_model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		die(json_encode(array('data' => $data, 'count' => $count)));
	}
	
	public function createAction()
	{
		$this->view->layout()->disableLayout();
		
		if ( $this->_request->isPost() ) {
			$bu_user = Zend_Auth::getInstance()->getIdentity();
			
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'subject' => '',
				'from' => '',
				'from_name' => '',
				'body' => '',
				'signature' => '',
				'application_id' => 'int',
				'backend_user_id' => 'int'
			));
			$data = $data->getData();
			$data['backend_user_id'] = $bu_user->id;
			
			$validator = new Cubix_Validator();
			
			if ( strlen($data['from']) && !$validator->isValidEmail($data['from']) ) {
				$validator->setError('from', 'Email is not valid');
			}
			
			if ( ! strlen($data['subject']) ) {
				$validator->setError('subject', 'Required');
			}
						
			if ( ! strlen($data['body']) ) {
				$validator->setError('body', 'Required');
			}
			
			if ( $validator->isValid() ) {
				$this->_model->save($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function editAction()
	{
		$this->view->layout()->disableLayout();
		
		$id = intval($this->_request->id);
		
		$this->view->item = $template = $this->_model->get($id);
		
		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'id' => 'int',
				'subject' => '',
				'from' => '',
				'from_name' => '',
				'body' => '',
				'signature' => '',
				'application_id' => 'int'
			));
			$data = $data->getData();
			
			$validator = new Cubix_Validator();
			
			if ( strlen($data['from']) && !$validator->isValidEmail($data['from']) ) {
				$validator->setError('from', 'Email is not valid');
			}
			
			if ( ! strlen($data['subject']) ) {
				$validator->setError('subject', 'Required');
			}
						
			if ( ! strlen($data['body']) ) {
				$validator->setError('body', 'Required');
			}
			
			if ( $validator->isValid() ) {
				$this->_model->save($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function deleteAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->_model->remove($id);

		die;
	}
	
	public function getAction()
	{
		$this->view->layout()->disableLayout();
		$id = intval($this->_request->id);
		$item = $this->_model->get($id);
		echo json_encode($item);
		die;
	}
}
