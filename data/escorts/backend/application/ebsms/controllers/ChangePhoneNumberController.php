<?php

class Ebsms_ChangePhoneNumberController extends Zend_Controller_Action {
	
	public function init()
	{
		$this->model = new Model_EBSMS_ChangePhone();
		$this->session = Zend_Auth::getInstance()->getIdentity();
	}
	
	public function indexAction() 
	{	
		$this->super = $this->view->super =$this->session->type=='superadmin' || $this->session->type=='admin'?1:0;
		$pr = new Model_Countries();
		$this->view->phone_countries = $pr->getPhonePrefixs();
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'escort_id' => $req->escort_id,
			'showname' => $req->showname,
			'old_phone' => $req->old_phone,
			'new_phone' => $req->new_phone,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to,
			'status' => $req->status,
			'sales_user_id' => $req->sales_user_id,
			'changed_count' => $req->changed_count
		);
		
		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		if(count($data) > 0 ){
			foreach($data as $row){
				if($row->escort_id == 0){
					$this->model->getEscortDataByPhone($row);
				}
				$row->escort_id = "<span class='escort-popup'><a href='#".$row->escort_id."'> ".$row->escort_id." </a></span>";
			}
		}
		
		$data = array(
			'data' => $data,
			'count' => $count
		);
		die(json_encode($data));
		
	}		
	
	public function editAction()
	{
		$this->view->layout()->disableLayout();
		$this->view->data =  $this->model->GetCurrent($this->_request->escort_id);
		$pr = new Model_Countries();
		$this->view->phone_countries = $pr->getPhonePrefixs();
	}
	
	public function saveAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		if(isset($this->_request->phone) && is_numeric($this->_request->phone))
		{

			$this->model->SavePhone($this->_request->escort_id,$this->_request->phone,$this->_request->country_code);
		}
	}
}
