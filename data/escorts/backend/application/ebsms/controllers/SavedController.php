<?php

class Ebsms_SavedController extends Zend_Controller_Action {
	
	public function init()
	{
		$this->model = new Model_EBSMS_Saved();
	}
	
	public function indexAction() 
	{
		
	}
		
	public function dataAction()
	{
		$page = $this->_getParam('page');
		$per_page = $this->_getParam('per_page');
		$sort_field = $this->_getParam('sort_field');
		$sort_dir = $this->_getParam('sort_dir');
		

		$req = $this->_request;
			
					
		$filter = array(
			/*'s.status' => Model_EBSMS_EBSMS::EBSMS_STATUS_DELETED
			'e.showname' => $req->showname,
			'vr.status' => $status,
			'vr.inform_method' => $req->inform_method,*/
			'date_from' => $req->date_from,
			'date_to' => $req->date_to,
			'application_id' => $req->application_id
		);
		
		$count = 0;
		$data = $this->model->getAll($page, $per_page, $sort_field, $sort_dir, $filter, $count);
		
		$DEFINITIONS = Zend_Registry::get('defines');
		
		foreach ($data as $d)
		{
			$d->status = $DEFINITIONS['status_options'][$d->status];
		}
		
		echo json_encode(array('data' => $data, 'count' => $count));
		die;
	}
	
	public function toggleIsReadAction()
	{
		$request = $this->_request;

		$model = new Model_EBSMS_EBSMS();
		$model->toggleIsRead($request->type, $request->set_as, $request->id);
		die;
	}
	
	public function deleteAction()
	{
		$id = $this->_request->id;
		
		if ( !is_array($id) )
			$id = array($id);
		
		$this->model->delete($id);
		
		die;
	}
}
