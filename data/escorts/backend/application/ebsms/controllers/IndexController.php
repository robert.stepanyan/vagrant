<?php

class Ebsms_IndexController extends Zend_Controller_Action {
	
	public function init()
	{
		$this->model = new Model_EBSMS_EBSMS();
	}
	
	public function ebsmsCallbackAction()
	{
		$this->view->layout()->disableLayout();

		$req = $this->_request;
		$m_ebsms = new Model_EBSMS_EBSMS();

        $id = intval($req->getParam('id'));
        $status = intval($req->getParam('status'));

//        if($id == 99999999 || $id == 'id' || $req->getParam('id') == 'id'){
//            $host = str_replace('backend.', '', $_SERVER['HTTP_HOST']);
//            $app_data = Cubix_Application::getByHost($host);
//            Cubix_Application::setId($app_data->id);
//            Cubix_Api_XmlRpc_Client::setApiKey(Cubix_Application::getById()->api_key);
//            $body = 'POST ----- '. var_export($_POST, true). '-------- GET ------- ' . var_export($_GET, true);
//            Cubix_Email::send('narek.amirkhanyan92@gmail.com', 'EBSMS CALLBACK', $body);
//        }
        $ebsms_exists = $m_ebsms->get($id);

        if (!$ebsms_exists) {
            die('---');
        }

        $m_ebsms->updateStatus($id, $status);

        if ($status == 1) {
            $m_ebsms->updateDeliveryDate($id, time());
        }
        elseif ($status == 2) {

        }
        elseif ($status == 3) {

        }

		die;
	}

	public function ebsmsCallback3Action()
	{
		$this->view->layout()->disableLayout();
		
		$req = $this->_request;
		$m_ebsms = new Model_EBSMS_EBSMS();

		$id = $req->msg_id;
		$status = intval($req->status);

		$ebsms_exists = $m_ebsms->get($id);
		
		//file_put_contents('/tmp/ebsms-callback.log', var_export($ebsms_exists, true) );
		
		if ( ! $ebsms_exists ) {
			die;
		}
		
		$m_ebsms->updateStatus($id, $status);

		if ( $status == 2 ) {
			$m_ebsms->updateDeliveryDate($id, time());
		}
		/*elseif ( $status == 2 ) {
			
		}
		elseif ( $status == 3 ) {
			
		}*/

		die;
	}
	
	public function ebsmsCallback2Action()
	{
		$this->view->layout()->disableLayout();

		$req = $this->_request;
		$m_ebsms = new Model_EBSMS_EBSMS();

		$id = intval($req->cliMsgId);
		$status = intval($req->status);

		$ebsms_exists = $m_ebsms->get($id);
		
		if ( ! $ebsms_exists ) {
			die;
		}

		$m_ebsms->updateStatus($id, $status);

		if ( $status == 1 ) {
			$m_ebsms->updateDeliveryDate($id, time());
		}
		elseif ( $status == 2 ) {
			
		}
		elseif ( $status == 3 ) {
			
		}

		die;
	}
	
	public function hex_decode_string($source) 
	{
		$string = '';
		
		for ( $i = 0; $i < strlen($source) - 1; $i += 4 ) {
			$num = (($i + 4) / 4);
			$hex = substr($source, $i, 4);
			list($hex1, $hex2) = array(substr($hex, 0, 2), substr($hex, 2));

			$dec = base_convert($hex, 16, 10);
			$char = html_entity_decode('&#' . $dec . ';', ENT_NOQUOTES, 'UTF-8');

			$string .= $char;
		 }

		 return $string;
	}

	public function ebsmsInboxAction()
	{
		$this->view->layout()->disableLayout();
		//mb_internal_encoding('UTF-8');
		$req = $this->_request;
		
		$m_inbox = new Model_EBSMS_Inbox();

		$orig = $req->ORIG;
		$recipient = $req->RCPNT;
		$text = $req->MSG;
		$msghex = $req->MSGHex;
		/*$text = mb_convert_encoding($text, "UTF-8", "ASCII");
		
		if ( $msghex ) {
			list($msghex) = explode("\n", $msghex);
			list($nil, $msghex) = explode('=', $msghex);
			
			$text = $this->hex_decode_string($msghex);
		}*/
		
		
		/*if($text == dechex(hexdec($text))) {
		  
		} else {
		  $text = $this->hextostr($text);
		}*/
		$text = utf8_encode($text);
		
		file_put_contents('/tmp/ebsms-data.log', var_export($_REQUEST, true) . ':' . $text . ":" . $msghex . ':' . $req->MSGHex . "\n----------------------------\n\n", FILE_APPEND);
		
		if ( ! $orig || ! $recipient ) die;
		
		if ( ! strlen($text) || $text == '<MessageData>' || strtolower($text) == 'bee' ) die;
		
		$h = $_SERVER['HTTP_HOST'];
		$hh = explode('.', $h);
		unset($hh[0]);
		$host = implode('.', $hh);
		
		$data = array(
			//'escort_id' => getEscortByPhone,
			'orig' => $orig,
			'recipient' => $recipient,
			'date' => new Zend_Db_Expr('NOW()'),
			'text' => $text,
			'application_id' => Cubix_Application::getByHost($host)->id
		);

		$m_inbox->saveToInbox($data);

		die("end");
	}
	
	public function indexAction() 
	{
		
	}
	
	public function testAction()
	{
		$conf = Zend_Registry::get('system_config');
		$conf = $conf['ebsms'];
		
		$data = new Cubix_Form_Data($this->_request);
			
		$data->setFields(array(
			'from' => '',
			'to' => '',
			'ebsms' => ''
		));
		
		$data = $data->getData();
		
		$validator = new Cubix_Validator();

		if ( ! $data['from'] ) {
			$validator->setError('from', 'From Number is required');
		}
		
		if ( ! $data['to'] ) {
			$validator->setError('to', 'To number is required');
		}
			
		if ( ! $data['ebsms'] ) {
			$validator->setError('ebsms', 'EBSMS is required');
		}
		
		if ( $validator->isValid() ) {
			if ( $this->_getParam('send') && $this->_request->isPost() ) {
				$ebsms = new Cubix_SMS($conf['userkey'], $conf['password']);
				
				$ebsms->setOriginator($data['from']);
				$ebsms->addRecipient($data['to']);
	        	
	        	// $ebsms->setBufferedNotificationURL($conf['BufferedNotificationURL']);
	        	// $ebsms->setDeliveryNotificationURL($conf['DeliveryNotificationURL']);
	        	// $ebsms->setNonDeliveryNotificationURL($conf['NonDeliveryNotificationURL']);
	
	        	$ebsms->setContent($data['ebsms']);
	        	
	        	if ( 1 != $ebsms->sendEBSMS() ) {
	        		$this->view->error = $ebsms->getErrorDescription();
	        	}
	        	else {
	        		$this->view->success = true;
	        	}
	        	
	        	$this->view->credits = $ebsms->showCredits();
			}
			else {
				$this->view->data = $data;
			}
		}
		else {
			$status = $validator->getStatus();
			print_r($status);
			die;
		}
		
		// die;
	}
	
	public function sendAction()
	{

		$conf = Zend_Registry::get('system_config');
		$b_user = Zend_Auth::getInstance()->getIdentity();
		if ( $this->_request->isPost() )
		{
			$data = new Cubix_Form_Data($this->_request);
			
			$data->setFields(array(
				'ebsms_inbox_id' => '',
				'e_ids' => '',
				'phone_number' => '',
				'phone_number_original' => '',
				'text' => '',

				'exact_send_date' => '',
				'send_time' => '',
				'escort_numbers' => ''

			));

			$data = $data->getData();

			$validator = new Cubix_Validator();

			if($esc_num = $data['escort_numbers']) {
			   $esc_num = unserialize($esc_num);


            } else {

                if (!$data['phone_number']) {
                    $validator->setError('phone_number', 'Required');
                }
            }

          	
          	$data['e_ids'] = array();
            foreach( $esc_num as $item => $i ) {
               $data['e_ids'][] = $i['id'];
            }
           
			if ( ! $data['text'] ) {
				$validator->setError('text', 'Required');
			}

			if ( isset($data['send_time']) && $data['send_time'] == "later" ) {

				$sec_diff = $data['exact_send_date'] - time();

				$days = floor($sec_diff/3600/24);

				if ( isset($data['exact_send_date']) && ! $data['exact_send_date'] ) {
					$validator->setError('exact_send_date', 'Required');
				} elseif ( $days >= 3 ) {
					$validator->setError('exact_send_date', 'Needs 3 days interval');
				} elseif ( $days < 0 ) {
					$validator->setError('exact_send_date', 'Needs date in future');
				}
			}
		}
		
		if ( $validator->isValid() ) {
			
			if(isset($data['ebsms_inbox_id']) AND $data['phone_number'] == $data['phone_number_original']){
				$modelebsms_inbox = new Model_EBSMS_Inbox();
				$modelebsms_inbox->sent($data['ebsms_inbox_id']);
			}
			$model_escorts = new Model_EscortsV2();
			$model_ebsms = new Model_EBSMS_Outbox();
			
			$current_app_id = intval($this->_getParam('application_id'));


            $configurator = $conf['ebsms'];
            $originator = $configurator['number'];

			
			$data['phone_from'] = $originator;

			if ( isset($data['send_time']) && $data['send_time'] == "later" ) {
				$data['status'] = Model_EBSMS_EBSMS::EBSMS_STATUS_POSTPONED;
			}

			$conf = $conf['ebsms'];
			$ebsms = new Cubix_SMS($conf['userkey'], $conf['password']);

			$ebsms->setOriginator($originator);
			$phone_number = $data['phone_number'];

			$data['application_id'] = Cubix_Application::getId();

			$data['sender_sales_person'] = $b_user->id;


			if ( is_array($esc_num) )
			{
				foreach( $esc_num as $item => $i )
				{
					$data['phone_number'] = $i['phone_number'];
					$data['escort_id']	=  $i['id'];
					if ( $data['status'] != Model_EBSMS_EBSMS::EBSMS_STATUS_POSTPONED ) {
						$id = $model_ebsms->save($data);
						$ebsms->setRecipient($i['phone_number'], $id);

						$ebsms->setBufferedNotificationURL($conf['BufferedNotificationURL']);
						$ebsms->setDeliveryNotificationURL($conf['DeliveryNotificationURL']);
						$ebsms->setNonDeliveryNotificationURL($conf['NonDeliveryNotificationURL']);

						$ebsms->setContent($data['text']);

						if (1 != $result = $ebsms->sendEBSMS()) {

						}
					} else {
						$id = $model_ebsms->savePostpone($data);
					}
				}
			}
			else
			{

				if ( $data['status'] != Model_EBSMS_EBSMS::EBSMS_STATUS_POSTPONED ) {
					$id = $model_ebsms->save($data);
						
					$ebsms->addRecipient($data['phone_number'], $id);

					$ebsms->setBufferedNotificationURL($conf['BufferedNotificationURL']);
					$ebsms->setDeliveryNotificationURL($conf['DeliveryNotificationURL']);
					$ebsms->setNonDeliveryNotificationURL($conf['NonDeliveryNotificationURL']);
					
					$ebsms->setContent($data['text']);

					if (1 != $result = $ebsms->sendSMS()) {

					}else{
					    $this->model->updateDelivery($id,Model_EBSMS_EBSMS::EBSMS_STATUS_SUCCESS);
                    }
				} else {
					$id = $model_ebsms->savePostpone($data);
				}
			}
		}
			
		die(json_encode($validator->getStatus()));
	}
	
	public function sendInstantbookEbsmsAction()
	{
		$conf = Zend_Registry::get('system_config');

		if ( $this->_request->isPost() )
		{
			$data = $this->_request->getParams();
			
			$validator = new Cubix_Validator();
	
			if ( ! $data['phone_number'] ) {
				$validator->setError('phone_number', 'Required');
			}
				
			if ( ! $data['text'] ) {
				$validator->setError('text', 'Required');
			}

			if ( isset($data['send_time']) && $data['send_time'] == "later" ) {

				$sec_diff = $data['exact_send_date'] - time();

				$days = floor($sec_diff/3600/24);

				if ( isset($data['exact_send_date']) && ! $data['exact_send_date'] ) {
					$validator->setError('exact_send_date', 'Required');
				} elseif ( $days >= 3 ) {
					$validator->setError('exact_send_date', 'Needs 3 days interval');
				} elseif ( $days < 0 ) {
					$validator->setError('exact_send_date', 'Needs date in future');
				}
			}
		}
		
		if ( $validator->isValid() ) {
			
			if(isset($data['ebsms_inbox_id']) AND $data['phone_number'] == $data['phone_number_original']){
				$modelebsms_inbox = new Model_EBSMS_Inbox();
				$modelebsms_inbox->sent($data['ebsms_inbox_id']);
			}
			$model_escorts = new Model_EscortsV2();
			$model_ebsms = new Model_EBSMS_Outbox();
			
			$current_app_id = intval($this->_getParam('application_id'));
			$originator = Cubix_Application::getPhoneNumber($current_app_id);
			
			$data['phone_from'] = $originator;

			if ( isset($data['send_time']) && $data['send_time'] == "later" ) {
				$data['status'] = Model_EBSMS_EBSMS::EBSMS_STATUS_POSTPONED;
			}

			if (Cubix_Application::getId() != APP_6B)
			{
				$conf = $conf['ebsms'];
				$ebsms = new Cubix_SMS($conf['userkey'], $conf['password']);

				$ebsms->setOriginator($originator);
				$phone_number = $data['phone_number'];

				$data['application_id'] = Cubix_Application::getId();

				$b_user = Zend_Auth::getInstance()->getIdentity();
				$data['sender_sales_person'] = $b_user->id;



				if ( is_array($phone_number) )
				{
					foreach( $phone_number as $i => $phone )
					{
						$data['phone_number'] = $phone;						
						$data['escort_id']	=  $data['e_ids'][$i];
						if ( $data['status'] != Model_EBSMS_EBSMS::EBSMS_STATUS_POSTPONED ) {
							$id = $model_ebsms->save($data);
							$ebsms->setRecipient($phone, $id);

							$ebsms->setBufferedNotificationURL($conf['BufferedNotificationURL']);
							$ebsms->setDeliveryNotificationURL($conf['DeliveryNotificationURL']);
							$ebsms->setNonDeliveryNotificationURL($conf['NonDeliveryNotificationURL']);

							$ebsms->setContent($data['text']);

							if (1 != $result = $ebsms->sendEBSMS()) {

							}
						} else {
							$id = $model_ebsms->savePostpone($data);
						}
					}
				}
				else
				{

					if ( $data['status'] != Model_EBSMS_EBSMS::EBSMS_STATUS_POSTPONED ) {
						$id = $model_ebsms->save($data);

						$ebsms->addRecipient($data['phone_number'], $id);

						$ebsms->setBufferedNotificationURL($conf['BufferedNotificationURL']);
						$ebsms->setDeliveryNotificationURL($conf['DeliveryNotificationURL']);
						$ebsms->setNonDeliveryNotificationURL($conf['NonDeliveryNotificationURL']);

						$ebsms->setContent($data['text']);

						if (1 != $result = $ebsms->sendEBSMS()) {

						}
					} else {
						$id = $model_ebsms->savePostpone($data);
					}
				}
			}
			else
			{
//				$conf = $conf['ebsms_6b'];
//				$ebsms = new Cubix_SMS6B($conf['user'], $conf['password']);
                $ebsms = Cubix_6B_SMS::getInstance();
				$phone_number = $data['phone_number'];

				$data['application_id'] = Cubix_Application::getId();

				$b_user = Zend_Auth::getInstance()->getIdentity();
				$data['sender_sales_person'] = $b_user->id;

				if ( is_array($phone_number) )
				{
					foreach( $phone_number as $phone )
					{
						$data['phone_number'] = $phone;

                        $response = $ebsms->send_ebsms($phone,$data['text'], '',date("d/M/Y"));
                        $Recipients = $response->getRecipients();
                        $data['identificationNumber'] = $Recipients[0]['identificationNumber'];
                        $id = $model_ebsms->save($data);
//						$ebsms->setDestination($phone, $id);
//						$ebsms->setContent($data['text']);
//						$ebsms->send();
					}
				}
				else
				{
                    $response =$ebsms->send_ebsms($data['phone_number'],$data['text'], '',date("d/M/Y"));
                    $Recipients = $response->getRecipients();
                    $data['identificationNumber'] = $Recipients[0]['identificationNumber'];
                    $id = $model_ebsms->save($data);
//					$ebsms->setDestination($data['phone_number'], $id);
//					$ebsms->setContent($data['text']);
//					$ebsms->send();
				}
			}
		}
			
		die(json_encode($validator->getStatus()));
	}
	
	public function statusAction()
	{
		
	}

	
	public function composeAction() 
	{
		$this->view->layout()->disableLayout();
		$ids = $this->_request->id;
		if(isset($this->_request->ebsms_id)){
			$this->view->ebsms_inbox_id = $this->_request->ebsms_id;
		}
		$model_escorts = new Model_EscortsV2();


		if ( count($ids) ) {
			$this->view->ids = $ids;
            $escorts = array();
            $escort_numbers = array();
			foreach( $ids as $id ) {
				$esc = $model_escorts->getProfile($id);

                if($esc['contact_phone_parsed']){
                    $escort_info = array(
                        'id' => $id,
                        'phone_number' => $esc['contact_phone_parsed']
                    );
                    $escort_numbers[] = $escort_info;
                    $escorts[] = $esc;
                }
			}
            $this->view->escorts = $escorts;
            $this->view->escort_numbers = serialize($escort_numbers);
		}

        if(isset($this->_request->escort_id)){
			$this->view->phone_from = $model_escorts->getPhoneByEscortId($this->_request->escort_id);
		}
		else{
			$this->view->phone_from = urldecode($this->_request->phone_from);
		}
		$this->view->no_btn = intval($this->_request->no_btn);
        
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		if (!in_array($bu_user->type, array('superadmin', 'admin','data entry plus')))
			$sales_user_id = $bu_user->id;
		else
			$sales_user_id = null;
		
		$modelEbsms = new Model_EBSMS_EBSMS();
		$this->view->temlates = $modelEbsms->getTemplates($sales_user_id);
	}
	
	public function templateAction()
	{
		$this->view->layout()->disableLayout();
		$modelEbsms = new Model_EBSMS_EBSMS();
		
		$id = $this->_request->id;
		$template = '';
		
		if (intval($id) > 0)
			$template = $modelEbsms->getTemplate($id);
		
		echo $template;
		die;
	}


	public function readAction()
	{
		$this->view->layout()->disableLayout();
		
		$id = $this->view->id = $this->_request->id;
		$type = $this->view->type = $this->_request->type;

		if ( $type != 'outbox' )
			$this->model->toggleIsRead($type, Model_EBSMS_EBSMS::EBSMS_IS_READ, array($id));
		
		switch($type)
		{
			case 'inbox':
				$model = new Model_EBSMS_Inbox();
				$this->view->ebsms = $model->get($id);
			break;
			case 'outbox':
				$model = new Model_EBSMS_Outbox();
				$ebsms = $model->get($id);
				$bu_user = Zend_Auth::getInstance()->getIdentity();
				if ($bu_user->type == 'superadmin' && $ebsms->pass){
					$ebsms->text = str_replace('******', $ebsms->pass, $ebsms->text);
				}
					
				$this->view->ebsms = $ebsms;	
			break;
			case 'saved':
				$model = new Model_EBSMS_Saved();
				$this->view->ebsms = $model->get($id);
			break;
			case 'trash':
				$model = new Model_EBSMS_Trash();
				$this->view->ebsms = $model->get($id);
			break;
		}	
	}
	
	public function escortListAction()
	{
		$this->view->layout()->disableLayout();
	}
	
	public function escortListDataAction()
	{
		$this->view->layout()->disableLayout();
		
		$req = $this->_request;
		
		$filter = array(
			// Personal
			'e.showname' => $req->showname,
			'ep.contact_phone_parsed' => $req->phone_parsed,
			'u.application_id = ?' => $req->application_id,
			'showname_is_not_null' => true,
			// Status
			'u.status' => $req->status,
			'excl_status' => Model_EscortsV2::ESCORT_STATUS_DELETED
		);
		
		$model = new Model_EscortsV2();
		$data = $model->getAll(
			$this->_request->page,
			$this->_request->per_page,
			$filter,
			$this->_request->sort_field,
			$this->_request->sort_dir,
			$count
		);
		
		$DEFINITIONS = Zend_Registry::get('defines');
		
		foreach ($data as $d)
		{
			$d['status'] = $DEFINITIONS['status_options'][$d['status']];
		}
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
		
		die(json_encode($this->view->data));
		
	}

	public function readThreadAction()
	{
		$this->view->layout()->disableLayout();
		
		$id = $this->view->id = $this->_request->id;
		$phone_number = $this->view->phone_number = $this->_request->phone_number;
		$this->view->is_ajax = $this->_request->is_ajax;

		$model = new Model_EBSMS_Inbox();
		$this->view->thread = $model->getThread($phone_number);

		$this->model->toggleIsRead('inbox', Model_EBSMS_EBSMS::EBSMS_IS_READ, array($id));

		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		if (!in_array($bu_user->type, array('superadmin', 'admin','data entry plus')))
			$sales_user_id = $bu_user->id;
		else
			$sales_user_id = null;
		
		$modelEbsms = new Model_EBSMS_EBSMS();
		$this->view->temlates = $modelEbsms->getTemplates($sales_user_id);
	}

	public function ebsmsCallback6BAction()
	{
		$this->view->layout()->disableLayout();
        $m_ebsms = new Model_EBSMS_EBSMS();
        if ($this->_getParam('Sequencia')){
            $Responce = new Cubix_6B_EBSMSResponseWebHook();
            $Params = $this->_getAllParams();

            $Responce->setResponse($Params);

            if ($Responce->isReplay()){
                $Params['reply'] = 1;
                $Params['ebsms_id'] = null;
                $m_inbox = new Model_EBSMS_Inbox();
                $orig = Model_EBSMS_EBSMS::getPhoneByIdentificationNumber($Responce->getIdentificationNumber());
                $data = array(
                    'orig' => $orig,
                    'recipient' => '27368',
                    'date' => $Responce->getReplyDate(),
                    'text' => $Responce->getMessage(),
                    'application_id' => APP_6B
                );
                $req_string = var_export($Params , true);
                $m_inbox->saveToInbox($data);
            }else{
                $ebsms_id = $m_ebsms->getByIdentificationNumber($Responce->getIdentificationNumber());
                $Params['ebsms_id'] = $ebsms_id;
                $Params['reply'] = 0;
                $req_string = var_export($Params , true);
                if (!$ebsms_id){
                    file_put_contents("/var/log/ebsms.log", "date - " . date('d M Y, H:i') . ", request ------- " . $req_string . "-----------\r\n", FILE_APPEND);
                    die("---");
                }
                $status =$Responce->getStatusForEbsmsOutBox();
                $m_ebsms->updateStatus($ebsms_id, $status);
                if ($status == Model_EBSMS_EBSMS::EBSMS_STATUS_SUCCESS){
                    $m_ebsms->updateDeliveryDate($ebsms_id, time());
                }elseif ($status == Model_EBSMS_EBSMS::EBSMS_STATUS_SPAM){
                    $m_ebsms->updateSpam($ebsms_id, 1);
                }
            }
            file_put_contents("/var/log/ebsms.log", "date - " . date('d M Y, H:i') . ", request ------- " . $req_string . "-----------\r\n", FILE_APPEND);
            die("Ok");
        }else{
            $phone = $this->_request->phone;
            $message_id = intval($this->_request->message_id);
            $ebsms_id_facilita = intval($this->_request->ebsms_id_facilita);
            $status_mensagem = $this->_request->status_mensagem;

            file_put_contents('/var/log/ebsms.log', 'date - ' . date('d M Y, H:i') . ', ebsms_id_facilita - ' . $ebsms_id_facilita . ', status_mensagem - ' . $status_mensagem . ', phone - ' . $phone . ', message_id - ' . $message_id . "\n", FILE_APPEND);

            $status_mensagem = intval(substr($status_mensagem, 0, 1));
            $ebsms_exists = $m_ebsms->get($message_id);

            if ( ! $ebsms_exists ) {
                die;
            }

            if ( $status_mensagem == 9 ) {
                $m_ebsms->updateStatus($message_id, 1);
                $m_ebsms->updateDeliveryDate($message_id, time());
            }
        }


		die;
	}

	public function ebsmsInbox6BAction()
	{
		$this->view->layout()->disableLayout();
		//mb_internal_encoding('UTF-8');
		$req = $this->_request;
		$req_string = var_export($_REQUEST , true);
		file_put_contents("/var/log/ebsms.log", "date - " . date('d M Y, H:i') . ", request ------- " . $req_string . "-----------", FILE_APPEND);
		$m_inbox = new Model_EBSMS_Inbox();

		$orig = $req->telefone;
		$recipient = '27368';
		$text = $req->mensagem;

		$text = utf8_encode($text);

		if ( ! $orig ) die('---');

		$orig = '0055' . $orig;

		$h = $_SERVER['HTTP_HOST'];
		$hh = explode('.', $h);
		unset($hh[0]);
		$host = implode('.', $hh);

		$data = array(
			'orig' => $orig,
			'recipient' => $recipient,
			'date' => new Zend_Db_Expr('NOW()'),
			'text' => $text,
			'application_id' => Cubix_Application::getByHost($host)->id
		);

		$m_inbox->saveToInbox($data);

		die;
	}
}
