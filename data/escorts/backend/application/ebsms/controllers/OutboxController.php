<?php

class Ebsms_OutboxController extends Zend_Controller_Action {
	
	public function init()
	{
		$this->model = new Model_EBSMS_Outbox();
	}
	
	public function indexAction() 
	{
		$bu_user = Zend_Auth::getInstance()->getIdentity();

		$this->view->can_change_sender = true;

		if (Cubix_Application::getId() == APP_EF && ($bu_user->type != 'admin' && $bu_user->type != 'superadmin')) {
			$this->view->can_change_sender = false;	
		}	
	}
		
	public function dataAction()
	{
			
		$page = $this->_getParam('page');
		$per_page = $this->_getParam('per_page');
		$sort_field = $this->_getParam('sort_field');
		$sort_dir = $this->_getParam('sort_dir');
		

		$req = $this->_request;
			
					
		$filter = array(
			/*'s.status' => Model_EBSMS_EBSMS::EBSMS_STATUS_DELETED
			'e.showname' => $req->showname,
			'vr.status' => $status,
			'vr.inform_method' => $req->inform_method,*/
			'showname' => $req->showname,
			'status' => $req->status,
			'escort_id' => $req->escort_id,
			'sender_sales_person' => $req->sender_sales_person,
			'e_status' => $req->e_status,
			'phone_to' => $req->phone_to,
			'year' => $req->year,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to,
			'application_id' => $req->application_id,
			'is_spam'	=> $req->is_spam,
			'skype_video_call' => $req->skype_video_call	
		);

//		var_dump($filter);die;
		$count = 0;

		$bu_user = Zend_Auth::getInstance()->getIdentity();

		if (Cubix_Application::getId() == APP_EF && ($bu_user->type != 'admin' && $bu_user->type != 'superadmin')){
			$filter['only_this_user'] = $bu_user->id;
		}

		$data = $this->model->getAll($page, $per_page, $sort_field, $sort_dir, $filter, $count);
				
		$DEFINITIONS = Zend_Registry::get('defines');
		
		foreach ($data as $d)
		{
			$d->status = $DEFINITIONS['escort_status_options'][$d->status];
			$phone = $d->contact_phone_parsed ? $d->contact_phone_parsed : $d->phone_to;
			$this->model->getEscortsByPhone($phone, $d);
			$d->ebsms_status_text = Model_EBSMS_EBSMS::$STATUS_TEXTS[$d->ebsms_status];
//			print_r($d);
			//$d->showname = $d->showname . ' (' . $d->escort_id . ')';
		}

//		print_r($data);
		echo json_encode(array('data' => $data, 'count' => $count));
		die;
	}
	
	public function deleteAction()
	{
		$id = $this->_request->id;
		
		if ( !is_array($id) )
			$id = array($id);
		
		$this->model->delete($id);
		
		die;
	}
	
	public function resendAction() 
	{
		$conf = Zend_Registry::get('system_config');

		$ids = $this->_request->id;
		
		if ( !is_array($ids) )
			$ids = array($ids);
		$resend_ebsms = $this->model->getByIds($ids);
		
		$model_ebsms = new Model_EBSMS_Outbox();

		$current_app_id = intval($this->_getParam('application_id'));

        $config = $conf['ebsms'];
        $originator = $config['number'];

		if (Cubix_Application::getId() != APP_6B)
		{
			$conf = $conf['ebsms'];
			$ebsms = new Cubix_SMS($conf['userkey'], $conf['password']);

			$ebsms->setOriginator($originator);

			$b_user = Zend_Auth::getInstance()->getIdentity();

			foreach( $resend_ebsms as $r_ebsms )
			{
				$r_ebsms = (array) $r_ebsms;
				$r_ebsms['sender_sales_person'] = $b_user->id;

				$id = $model_ebsms->save( $r_ebsms );

				$ebsms->setRecipient($r_ebsms['phone_number'], $id);

				$ebsms->setBufferedNotificationURL($conf['BufferedNotificationURL']);
				$ebsms->setDeliveryNotificationURL($conf['DeliveryNotificationURL']);
				$ebsms->setNonDeliveryNotificationURL($conf['NonDeliveryNotificationURL']);

				$ebsms->setContent($r_ebsms['text']);

				if (1 != $result = $ebsms->sendEBSMS()) {

				}
			}
		}
		else
		{

//			$conf = $conf['ebsms_6b'];
//			$ebsms = new Cubix_EBSMS6B($conf['user'], $conf['password']);
            $ebsms = Cubix_6B_EBSMS::getInstance();
			$b_user = Zend_Auth::getInstance()->getIdentity();

			foreach( $resend_ebsms as $r_ebsms )
			{
				$r_ebsms = (array) $r_ebsms;
				$r_ebsms['sender_sales_person'] = $b_user->id;

                $response = $ebsms->send_ebsms($r_ebsms['phone_number'], $r_ebsms['text'], '', date('d/M/Y'));
                $Recipients = $response->getRecipients();
                $r_ebsms['identificationNumber'] = $Recipients[0]['identificationNumber'];
                $id = $model_ebsms->save( $r_ebsms );
//				$ebsms->setDestination($r_ebsms['phone_number'], $id);
//				$ebsms->setContent($r_ebsms['text']);
//				$ebsms->send();
			}
		}

		die;
	}		
}
