<?php

class Ebsms_InboxController extends Zend_Controller_Action {
	
	public function init()
	{
		$this->model = new Model_EBSMS_Inbox();
	}
	
	public function indexAction() 
	{
		
	}
		
	public function dataAction()
	{
		set_time_limit(0);
		$page = $this->_getParam('page');
		$per_page = $this->_getParam('per_page');
		$sort_field = $this->_getParam('sort_field');
		$sort_dir = $this->_getParam('sort_dir');
		

		$req = $this->_request;
			
					
		$filter = array(
			/*'s.status' => Model_EBSMS_EBSMS::EBSMS_STATUS_DELETED
			'vr.status' => $status,
			'vr.inform_method' => $req->inform_method,*/
			'showname' => $req->showname,
			'escort_id' => $req->escort_id,
			'phone_from' => $req->phone_from,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to,
			'sales_user_id' => $req->sales_user_id,
			'for_review' => $req->for_review,
			'reply' => $req->reply,
			'application_id' => $req->application_id
		);
		
		$count = 0;
		$data = $this->model->getAll($page, $per_page, $sort_field, $sort_dir, $filter, $count);
		
		$DEFINITIONS = Zend_Registry::get('defines');
		
		foreach ($data as $d)
		{
			$d->status = $DEFINITIONS['escort_status_options'][$d->status];
			//$this->model->getEscortsByPhone($d->phone_from, $d);
		}
		
		echo json_encode(array('data' => $data, 'count' => $count));
		die;
	}
	
	
	public function toggleIsReadAction()
	{
		$request = $this->_request;

		$model = new Model_EBSMS_EBSMS();
		$model->toggleIsRead($request->type, $request->set_as, $request->id);
		die;
	}
	
	public function deleteAction()
	{
		$id = $this->_request->id;
		
		if ( !is_array($id) )
			$id = array($id);
		
		$this->model->delete($id);
		
		die;
	}
	
	public function saveAction()
	{
		$id = $this->_request->id;
		
		if ( !is_array($id) )
			$id = array($id);
		
		$this->model->save($id);
		
		die;
	}
	
	public function toggleAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->model->toggle($id);
		
		die;
	}
}
