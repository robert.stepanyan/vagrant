<?php

class EBSMS_PhoneGroupsController extends Zend_Controller_Action
{
	public function init()
	{
		$this->model = new Model_EBSMS_PhoneGroups();
	}
	
	public function indexAction()
	{
		
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		$page = $this->_getParam('page');
		$per_page = $this->_getParam('per_page');
		$sort_field = $this->_getParam('sort_field');
		$sort_dir = $this->_getParam('sort_dir');
		$count = 0;
		
		$filter = array(
			'name' => $req->name
		);
		
		$data = $this->model->getAll( $page, $per_page, $sort_field, $sort_dir, $filter, $count);
		
		echo json_encode(array('data' => $data, 'count' => $count));
		die;
	}
	
	public function createAction()
	{
		if ( $this->_request->isPost() ) {
			$name = $this->_getParam('name');
			
			$validator = new Cubix_Validator();
						
			if ( ! strlen($name) ) {
				$validator->setError('name', 'Name is required');
			}
			
			if ( $validator->isValid() ) {
				$this->model->save(new Model_EBSMS_PhoneGroupsItem(array(
					'name'   => $name
				)));
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function editAction()
	{
		$id = intval($this->_getParam('id'));
		
		if ( $this->_request->isPost() ) {
			$name = $this->_getParam('name');
			
			$validator = new Cubix_Validator();
						
			if ( ! strlen($name) ) {
				$validator->setError('name', 'Name is required');
			}
			
			if ( $validator->isValid() ) {
				$this->model->save(new Model_EBSMS_PhoneGroupsItem(array(
					'id'             => $id,					
					'name'   => $name
				)));
			}
			
			die(json_encode($validator->getStatus()));
		}
		else {
			$this->view->group = $this->model->get($id);
		}
	}
	
	public function removeAction()
	{
		$this->model->remove($this->_getParam('id'));
	}
}

