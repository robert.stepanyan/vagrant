<?php

class Lotteries_XmasLotteryController extends Zend_Controller_Action
{
	protected $model;
	
	public function init()
	{
		$user = Zend_Auth::getInstance()->getIdentity();
		if ($user->type != 'superadmin' && $user->type != 'admin'){
			die('Permission denied');
		}
		$this->model = new Model_Lotteries_XmasLottery();
	}
	
	public function indexAction() 
	{
		
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'date_from' => $req->date_from,
			'date_to' => $req->date_to,
            'is_diamond' => $req->is_diamond
		);
		
		$data = $this->model->getAll(
			$req->page,
			$req->per_page, 
			$filter,
			$req->sort_field,
			$req->sort_dir,
			$count
		);
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
	
	public function addAction()
	{
		if ( $this->_request->isPost() )
		{					
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'login_try' => '',
				'date' => '',
                'is_diamond' => 'int',
			));
			
			$data = $data->getData();
			
			$langs = Cubix_I18n::getLangs(true);
			foreach($langs as $id){
				$key = 'text_'.$id;
				$data[$key] = $this->_request->{$key};
			}
			
			$validator = new Cubix_Validator();
			
			if(strlen($data['login_try']) == 0){
				$validator->setError('login_try', 'Required');
			}
			elseif (!is_numeric($data['login_try']) ) {
				$validator->setError('login_try', 'Must be integer');
			}
			
			if(strlen($data['date']) == 0){
				$validator->setError('date', 'Required');
			}
			else{
				$data['date'] = date("Y-m-d", $data['date']);
				/*if($this->model->checkLimitPerDay($data['date']) > 1 ){
					$validator->setError('date', '2 win IDs per day' );
				}*/
			}
			
			if(strlen($data['date']) == 0){
				$validator->setError('date', 'Required');
			}
			
			if ( $validator->isValid() ) {
					$this->model->save($data);
			}
			die(json_encode($validator->getStatus()));
		}
	}

	public function editAction()
	{
		
		if ( $this->_request->isPost() )
		{	
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'id' => 'int',
				'login_try' => '',
				'date' => '',
                'is_diamond' => 'int',
			));
			
			$data = $data->getData();

			$langs = Cubix_I18n::getLangs(true);
			foreach($langs as $id){
				$key = 'text_'.$id;
				$data[$key] = $this->_request->{$key};
			}
			$validator = new Cubix_Validator();
			
			if(strlen($data['login_try']) == 0){
				$validator->setError('login_try', 'Required');
			}
			elseif (!is_numeric($data['login_try']) ) {
				$validator->setError('login_try', 'Must be integer');
			}
			
			if(strlen($data['date']) == 0){
				$validator->setError('date', 'Required');
			}
			else{
				$data['date'] = date("Y-m-d", $data['date']);
				/*if($this->model->checkLimitPerDay($data['date'], $data['id'] ) > 1 ){
					$validator->setError('date', 'limitation 2 win IDs per day' );
				}*/
			}
					
			if ( $validator->isValid() ) {
					$this->model->update($data);
			}
			die(json_encode($validator->getStatus()));
		}
		else{
			$winner = $this->model->get($this->_request->id);
			$winner->date = $winner->date + 6 * 60 * 60; // Timezone add 6 hour  
			$this->view->winner = $winner;
		}
	}

	public function removeAction()
	{
		$id = $this->_getParam('id');
		$this->model->remove($id);
		die;
	}
	
	public function winnersAction()
	{
		
	}		
	
	public function winnerListAction()
	{
		$req = $this->_request;
				
		$data = $this->model->getSelected(
			$this->_request->page,
			$this->_request->per_page, 
			$this->_request->sort_field, 
			$this->_request->sort_dir,
			$count	
		);
		
		$user_model = new Model_Users();
		
		foreach ( $data as $i => $item ) 
		{
			
			if ($item->user_type == 'escort')
			{
				$escort_data = $this->model->getEscortbyUserId($item->user_id);
				$data[$i]->ea_id = $escort_data->id;
				$data[$i]->ea_name = $escort_data->showname;
			}
			else{
				$agency_data = $this->model->getAgencybyUserId($item->user_id);
				$data[$i]->ea_id = $agency_data->id;
				$data[$i]->ea_name = $agency_data->name;
			}
		}
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
		
		die(json_encode($this->view->data));
	}		
	
}
