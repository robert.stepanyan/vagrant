<?php
session_start();
unset($_SESSION['error']);
if(isset($_POST) && count($_POST) > 0){
    $data = array(
        'name' => '',
        'email' => '',
        'message' => '',
        'captcha' => ''
        );
    foreach ($_POST as $key => $requestParam){
        if(key_exists($key, $data)){
            $data[$key] = addslashes(strip_tags(trim($requestParam)));
        }else{
            $_SESSION['error'] = "Invalid Key:".$key;
        }
    }
    
    if(isset($data['captcha']) && !empty($data['captcha']) && (strtolower($data['captcha']) === $_SESSION['captcha'])){
        $to = "someone@example.com";
        $subject = "Feedback";
        $message = @$data['message'];
        $from = @$data['email'];
        $headers = "From: ".$data['name'];
        @mail($to,$subject,$message,$headers);
        $_SESSION['error']['none'] = true;

    }else{
         $_SESSION['error']['captcha'] = "Captcha Error";
    }



}


header('Location: about.php');



?>
