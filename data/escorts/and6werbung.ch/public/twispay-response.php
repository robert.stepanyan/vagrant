<?php

if(!isset($_POST['opensslResult'])){
    header('location:https://www.and6werbung.ch');
}

include 'inc/common.php';
include_once 'library/Cube/Twispay.php';

$twispay = new Cube_Twispay();

$result = $twispay->decrypt($_POST['result']);

?>

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <?php require_once 'head.php'; ?>
        <style>
            label.error {display:none !important}
        </style>
    </head>

<body>
<div class="shadow">
    <div class="container">
        <div class="header">
            <a href="index.html"><img src="img/logo<?= $postfix ?>.png" alt="" /></a>
        </div>

        <div class="content">

            <?php require_once 'menu.php'; ?>

            <div class="contentBody">

                <?php if ( $result['status'] == 'in-progress' || $result['status'] == 'complete-ok'):?>
<!--                On success-->
                    <h4>Transaction Successful !</h4>
                    <p> Please copy and send us the transaction id. </p>
                    <p>Transaction ID - <?= $result['transactionId']?></p>

                    <p>Amount - <?= $result['amount'] .' '.$result['currency'];?></p>
                    <p>Date - <?= date('Y-m-d h:i:s',$result['timestamp'])?></p>

                <?php else: ?>
<!--                On Fail-->
                    <h4>Transaction Failed !</h4>
                    <p>Your payment was not successful! Please double check your data and try again. If you continue to have problems, contact your sales representative.</p>

                <?php endif;?>

            </div>

        </div>

    </div>

<?php include('footer.php'); ?>