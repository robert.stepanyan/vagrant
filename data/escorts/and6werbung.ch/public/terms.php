<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <?php
        require_once 'head.php';
      ?>
  </head>
  <body>
      <div class="shadow">
          <div class="container">
              <div class="header">
                  <a href="index.html"><img src="img/logo<?= $postfix ?>.png" alt="" /></a>
              </div>
              <div class="content">
                  <?php
                    $page = 'terms';
                    require_once 'menu.php';
                  ?>

                  <div class="contentBody">

                        <p class="f22 blue title"><b>Advertising Terms</b></p>
                        <div class="pageInner">
                            <p class="f16 blue"><b>Please read carefully our advertising terms. By ordering any package from our site you agree with our conditions.</b></p>
                            <ul class="termsUl">
                                <li>When you submit your order, you will get redirected to our external payment provider.</li>
                                <li>After successful payment you may be contacted by our staff if we need more details.</li>
								<? if ( $siteName == 'gate456' ): ?>
                                <li>A refund is possible only in exceptional cases in the next 24 hours.</li>
								<? else: ?>
								<li>All orders done through ads<?= $siteName ?>.com are non-refundable.</li>
								<? endif; ?>
                                <li>All data are kept confidentially, pls read our <a href="privacy.php">privacy policy</a>.</li>
                                <li>All requests will be answered within 24-48 hours.</li>
                            </ul>
                        </div>
						
						<p class="f16 title" style="margin-top: 15px;"><b>Disclaimer of liability</b></p>
						<div class="pageInner" style="padding-right: 20px;">
                            <p class="f14">
								All material on the Site is for information only and does not address the circumstances of any particular individual or entity. The information on the Site is not intended to be comprehensive or complete and may not be up-to-date, and should not be relied on as such. We do not assume or accept any responsibility or liability in relation to either the availability of the Site or the information on it.<br/><br/>
 
All information is provided "as is", without any warranties of any kind and at your sole risk. We make no representations and disclaim all express and implied warranties and conditions of any kind in relation to information on the Site.<br/><br/>
 
Whether or not advised of the possibility, neither we nor any other party involved in creating, producing, maintaining or delivering the Site shall be liable to you for any kind of loss or damage that may be suffered or claimed by you (or any person claiming under or through you) or to any third party, which arises in connection with the use of, inability to use or the results of use of the Site and any websites linked to the Site or the material on the Site or such other websites, whether such losses or damages are suffered directly or indirectly or are immediate or consequential and whether arising from negligence, breach of contract or otherwise.<br/><br/>
 
Nothing in this disclaimer excludes or limits our liability for fraud, for negligence causing death or personal injury, or for any liability to the extent that it cannot be excluded or limited under the applicable law or regulatory system.</p>   
                        </div>
						
						<p class="f16 title" style="margin-top: 15px;"><b>Governing law</b></p>
						<div class="pageInner" style="padding-right: 20px;">
                            <p class="f14">								
This Site and all disputes or other matters arising out of it shall be governed by the laws of
the Province of Nicosia, Cyprus and dealt with by a court of competent jurisdiction in Nicosia,
Cyprus or such other location as the parties may agree. Since you should have no reason to
have a grievance with us, as a disincentive to unwarranted litigation, you agree that if you sue
us and don't win, you will pay all our costs, including reasonable fees for in-house and outside counsel. 
							</p>   
                        </div>
						
						<div class="pageInner" style="padding-right: 20px;">
                            <p class="f14">
								<br>
								This website is owned, regulated and operated by MARTXOACO LIMITED, from the Cyprus offices.<br>
MARTXOACO LIMITED / HE-338394 <br>
Chrysanthou Mylona, 16 <br>
1st floor, Flat/Office 101 <br>
Akropoli, 2014, Nicosia, Cyprus
							</p>   
                        </div>
                  </div>

              </div>

          </div>
          <?php
		  include('footer.php');
		  ?>
