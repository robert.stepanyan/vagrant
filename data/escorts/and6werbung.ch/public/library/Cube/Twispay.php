<?php

/**
 * @author: Zhora
 * Date: 18.06.2018
 * Time: 16:43
 */

require_once 'interfaces/iPaymentConfig.php';


class Cube_Twispay implements iPaymentConfig
{
    private $site_id;
    private $api_key;
    private $url;
    private $currency;
    private $config;

    public function __construct()
    {
        $this->initConfigs();

        $this->site_id = $this->config['site_id'];
        $this->api_key = $this->config['api_key'];
        $this->url = $this->config['url'];
        $this->currency = $this->config['currency'];
    }

    public function initConfigs()
    {
        if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1' || $_SERVER['REMOTE_ADDR'] == '10.0.2.2') {
            //Development Configs
            $this->config = array(
                'site_id'  => 1531,
                'api_key'  => '2b743029e3b9761afa2181eb978eaf18',
                'url'      => 'https://secure-stage.twispay.com',
                'currency' => 'CHF'
            );
        } else {
            //Production Configs
            $this->config = array(
                'site_id'  => 137,
                'api_key'  => '215866f4f0f32e06189f35f1e6b4a9f3',
                'url'      => 'https://secure.twispay.com',
                'currency' => 'CHF'
            );
        }
    }

    public function prepareRequest($amount)
    {

        $data = array(
            'siteId' => intval($this->site_id),
            'identifier' => 'and6werb-' . rand(1, 10000),
            'amount' => $amount,
            'currency' => $this->currency,
            'description' => 'Self Checkout And6.com',
            'orderType' => 'purchase',
            'orderId' => 'SC3-and6werb-' . rand(1, 10000),
            'cardTransactionMode' => 'authAndCapture',
            'backUrl' => ''
        );

        $data['checksum'] = $this->generateChecksum($data, $this->api_key);


        try {
            $payment_from = '<form accept-charset="UTF-8" id="twispay-payment-form" action="' . $this->url . '" method="post">';

            foreach ($data as $k => $v) {
                $payment_from .= '<input type="hidden" name="' . $k . '" value="' . $v . '">';
            }
            $payment_from .= '<input type="submit" value="submit">';
            $payment_from .= '</form>';

        } catch (Exception $e) {
            $message = $e->getMessage();
            var_dump($message);
            die();
        }

        return $payment_from;
    }

    public function generateChecksum($data, $key)
    {
        // - taking the list of the submitted fields, order this list ascending by the field names;
        // - if the value of a field itself is a list of values, recursively order the list of values by their key names;
        ksort($data, SORT_STRING);

        // - with the ordered list of fields and field values, build a URL encoded query string, according to RFC 1738;
        // $qs = http_build_query($data);

        // - with the result of the previous steps, generate a HMAC (hash-based message authentication code) using the SHA-512 hashing algoritm and your secret key;
        // $hh = hash_hmac('sha512', $qs, $key , true);

        // - taking the generated HMAC in binary format, encode it in base64.
        // $checksum = base64_encode($hh);
        $checksum = base64_encode(hash_hmac('sha512', http_build_query($data), $key, true));
        return $checksum;
    }

    function decrypt($data)
    {
        // - the value of the `result` parameter need to be BASE64 decoded;
        $data = base64_decode($data);

        // - with the result, take the first 32 characters, that's the IV (initialization vector);
        $iv = substr($data, 0, 32);

        // - take the rest of the result and the IV, using RIJNDAEL 256 cypher, mode CBC, decrypt the value;
        $crypted = substr($data, 32);

        // - the resulted decrypted value is the data, in JSON format.

        $decrypted_data = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->api_key, $crypted, MCRYPT_MODE_CBC, $iv);

        $decoded_data = json_decode(preg_replace('/[\x00-\x1F\x7F]/u', '', $decrypted_data),true);

        return $decoded_data;
    }


}