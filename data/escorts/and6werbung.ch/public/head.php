<link href="/css/main.css?v=1" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/js/jquery-1.5.min.js" ></script>
<script type="text/javascript" src="/js/jquery.validate.js"></script>
<script type="text/javascript">
    $(document).ready(function(){


        $("#feedbackForm").validate({
        rules: {
                email: {
                  required: true,
                  email: true
                }
              }
        });

    });
</script>

<?php

include('content.php');

$postfix = "_and6";
$siteName = "werbung";
$country = "Switzerland";
$website = "and6.ch";
$key = "and6werbung";
if ($_GET['lng'] == 'fr')
	$key = "and6werbung_fr";
elseif ($_GET['lng'] == 'de')
	$key = "and6werbung_de";

?>

