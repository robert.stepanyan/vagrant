<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <?php
        require_once 'head.php';
      ?>
  </head>
  <body>
      <div class="shadow">
          <div class="container">
              <div class="header">
                  <a href="index.html"><img src="img/logo.png" alt="" /></a>
              </div>
              <div class="content">
                  <?php
                    $page = 'sites';
                    require_once 'menu.php';
                  ?>

                  <div class="contentBody">
                        <div class="pageInner">
                            <p class="paddingtop10">
                              <b>
                                  We run many internationally known advertising sites worldwide. adsgate123.com offers only advertising for France. You still can contact us if you are interested in advertising solutions for different countries.
                              </b>

                            </p>
                            <p class="f16 blue"><b>We are locally available in:</b></p>
                            <p>
                                United Kingdom<br />
                                Switzerland<br />
                                Italy<br />
                                United States<br />
                                Spain<br />
                                Greece<br />
                                Germany<br />

                            </p>

                            <div class="siteItem">
                                <span class="blue f16"><b>6annonce.com </b></span> -  Leading advertising site in France (Alexa Traffic Rank: 49,120)
                                <div class="siteImage">
                                    <img src="img/tmp_image.jpg" alt="" />
                                    <a href="http://6annonce.com" target="_blank">Click the image to view site</a>
                                </div>
                            </div>

                        </div>
                  </div>

              </div>

          </div>
          <div class="footer">
              copyright 2011 <span class="blue">ads</span>gate123.com
          </div>
       </div>
  </body>
</html>
