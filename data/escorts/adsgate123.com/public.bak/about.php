<?php
session_start();
$success = false;
$error = false;
$errorMail = false;
$data = array(
        'name' => '',
        'email' => '',
        'message' => '',
        'captcha' => ''
        );
if(isset($_POST) && count($_POST) > 0){
    
    foreach ($_POST as $key => $requestParam){
        if(key_exists($key, $data)){
            $data[$key] = addslashes(strip_tags(trim($requestParam)));
        }
    }

    if(isset($data['captcha']) && !empty($data['captcha']) && (strtolower($data['captcha']) === $_SESSION['captcha'])){
        $to = "someone@example.com";
        $subject = "Feedback";
        $message = @$data['message'];
        $from = @$data['email'];
        $headers = "From: ".$data['name'];
        if(@mail($to,$subject,$message,$headers)){
            $success = true;
        }else{
            $errorMail = true;
        }
        

    }else{
         $error = true;
    }
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <?php
        require_once 'head.php';
      ?>
  </head>
  <body>
      <div class="shadow">
          <div class="container">
              <div class="header">
                  <a href="index.html"><img src="img/logo.png" alt="" /></a>
              </div>
              <div class="content">
                  <?php
                    $page = 'about';
                    require_once 'menu.php';
                  ?>

                  <div class="contentBody">
                  
                        <p class="f22 blue title"><b>About Us</b></p>
                        <div class="pageInner">
                            <p class="f16 blue"><b>Imprint</b></p>
							
							<p>
                                <img src="img/contacts.png" alt="" />
<!--                                PTP Media Ltd.<br />
							56 Seymour Street<br />
							London, W1H 7JJ<br />-->
							</p>
                            <p class="f16 blue"><b>Contact</b></p><br />
                            <div class="feedback">
                                <div>
                                    <?php
                                    if($success){
                                        ?>
                                    <div class="green">
                                        The Message Have Successfully Sent</div>
                                    <?php
                                    }elseif($errorMail){
                                    ?>
                                    <div class="require">Try Again</div>
                                    <?php }?>
                                </div>
                                <form action="" id="feedbackForm" method="post">
                                    <div class="feedbackRow">
                                        <div class="fl w125 paddingbottom10">
                                            <label for="name" class="f14">Name:</label>
                                        </div>
                                        <div class="fl w215">
                                            <input type="text" class="w215 required" name="name" value="<?php echo $data['name']; ?>" id="name" />
                                        </div>
                                        <div class="clear"></div>
                                    </div>

                                    <div class="feedbackRow">
                                        <div class="fl w125">
                                            <label for="email"  class="f14">Your e-mail:</label>
                                        </div>
                                        <div class="fl w215">
                                            <input type="text" class="w215 required" name="email" value="<?php echo $data['email']; ?>" id="email" />
                                        </div>
                                        <div class="clear"></div>
                                    </div>

                                    <div class="feedbackRow">
                                        <div class="fl w125">
                                            <label for="message"  class="f14">Message:</label>
                                        </div>
                                        <div class="fl w215 ">
                                            <textarea rows="0" class="w215 required" name="message" id="message" cols="0"><?php echo $data['message']; ?></textarea>
                                        </div>
                                         <div class="clear"></div>
                                    </div>

                                    <div class="feedbackRow">
                                        <div class="fl w125">
                                           <img src="captcha.php" width="90px" height="30px" alt="" />
                                        </div>
                                        <div class="fl w125">

                                            <input type="text" class="w215 required" name="captcha" id="captcha" />
                                            <?php
                                            if($error){
                                            ?>
                                            <label for="captcha" generated="true" class="error">
                                               Invalid Captcha Code.
                                            </label>
                                            <?php } ?>
                                            
                                        </div>
                                         <div class="clear"></div>
                                    </div>

                                    <div class="feedbackRow">
                                        <div class="fl w125">
                                            &nbsp;
                                        </div>
                                        <div class="fl w125">
                                            <input type="submit" class="submit" value="" />
                                        </div>
                                         <div class="clear"></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                  </div>

              </div>

          </div>
          <div class="footer">
              copyright 2011 <span class="blue">ads</span>gate123.com
          </div>
       </div>
  </body>
</html>
