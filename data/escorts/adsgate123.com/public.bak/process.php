<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

include 'inc/common.php';

// validating data
$errors = array();


// site
/*
if(empty($_POST['site']))
{
	$errors['site'] = 'required';
	$site = '';
} else {

	$site = stripslashes($_POST['site']);
}

if($site != '6annonce.com') {
	
	$errors['site'] = 'invalid site';
}
*/



// amount
if(empty($_POST['amount'])){

	$errors['amount'] = 'required';
	$amount = '';
} else {

	$amount = stripslashes($_POST['amount']);
}

if(!is_numeric($amount) || $amount <= 0) {

	$errors['amount'] = 'invalid amount';
}



// lang
$lang = (!empty($_POST['lang'])) ? $_POST['lang'] : '';


// Showing form again
if(count($errors) > 0) {

	include 'views/payment.phtml';
	die;
}



$gate = new Cube_Epgateway();
$ref = md5(rand(100000, 1000000000));

try 
{
    // Success URL
    $SuccessURL = 'http://'.$_SERVER['HTTP_HOST'].'/success.php';

    // Checking if Recurring
    if(isset($_POST['recurring']) && ($_POST['recurring'] == 1))
    {
        // ($reference, $amount, $duration , $redirect, $mode = 'monthly', $trial_amount = 0, $trial_duration = 0 )
        $token = $gate->getTokenForRecurring($ref, $amount, 1, $SuccessURL);
    }
    elseif(isset($_POST['auth']) && ($_POST['auth'] == 1))
    {
       $token = $gate->getTokenForAuth($ref, $amount, $SuccessURL);
    }
    else {
        $token = $gate->getTokenForSale($ref, $amount, $SuccessURL);
    }


    // Redirecting to Payment Gateqway URL
    header('Location:'.$gate->getPaymentGatewayUrl($token, $lang));
}
catch(Exception $e)
{
	print_r($e);
	$errors['exception'] = 'Unexpected error, please try again or contact Administrator if you see the error again';
	include 'views/payment.phtml';
}



// $client = new Zend_Http_Client('http://www.a1plus.am');
// $response = $client->request();
// echo $response;



?>
