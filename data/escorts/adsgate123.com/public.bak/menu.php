<?php
$prefixs = array(
    'home' => '',
    'about' => '',
    'terms' => '',
    'sites' => '',
    'privacy' => ''
);

$prefixs[$page] = 'Active';
?>
<div class="menu">
    <a href="index.php" class="menuItemMain<?php echo $prefixs['home']?>">&nbsp;</a>
    <img src="img/menu/separator.gif" class="fl" />
    <a href="about.php" class="menuItemAbout<?php echo $prefixs['about']?>">&nbsp;</a>
    <img src="img/menu/separator.gif" class="fl" />
    <a href="terms.php" class="menuItemTerms<?php echo $prefixs['terms']?>">&nbsp;</a>
    <img src="img/menu/separator.gif" class="fl" />
    <a href="sites.php" class="menuItemSites<?php echo $prefixs['sites']?>">&nbsp;</a>
    <img src="img/menu/separator.gif" class="fl" />
    <a href="privacy.php" class="menuItemPrivacy<?php echo $prefixs['privacy']?>">&nbsp;</a>
    <div class="clear"></div>
</div>
