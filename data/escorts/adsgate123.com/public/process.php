<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include 'inc/common.php';

// validating data
$errors = array();

// site
/*
if(empty($_POST['site']))
{
	$errors['site'] = 'required';
	$site = '';
} else {

	$site = stripslashes($_POST['site']);
}

if($site != '6annonce.com') {
	
	$errors['site'] = 'invalid site';
}
*/




// amount
if(empty($_POST['amount'])){

	$errors['amount'] = 'required';
	$amount = '';
} else {

	$amount = stripslashes($_POST['amount']);
}

if(!is_numeric($amount) || $amount <= 0) {

	$errors['amount'] = 'invalid amount';
}



// lang
$lang = (!empty($_POST['lang'])) ? $_POST['lang'] : '';


// Showing form again
if(count($errors) > 0) {

	include 'views/payment.phtml';
	die;
}


if ( preg_match('#adsgate456#', $_SERVER['HTTP_HOST']) > 0 ) {
	$dName = "adsgate456.com";
        //$siteId = '1000270';
        //$guid = 'CD237BC9-63CD-6EA9-845B-0CCFF4155D1F';
		$siteId = '1001447';
        $guid = '95EA2890-D436-CF99-C0B6-40C575E60FB7';
}
else {
	$dName = "adsgate123.com";
        //$siteId = '1000316';
        $siteId = '1000339';
        //$guid = 'D32C493B-2FB3-CFFC-09F0-30FDC69C698C';
        $guid = 'E6ED91FF-7089-1F74-0B45-29343BCFF82E';
}

$MerchantInfo = 'http://www.'.$dName.';info@'.$dName;
$TransactionUrl = 'www.'.$dName;


$gate = new Cube_Epgateway($MerchantInfo, $TransactionUrl, $siteId, $guid);

if ( preg_match('#adsgate123#', $_SERVER['HTTP_HOST']) > 0 ) {
	$gate->setTokenizerUrl('https://apps.epg-services.com/V2/pp/tokenizer/get');
	$gate->setPaymentGatewayUrl('https://apps.epg-services.com/V2/pp/paymentpage?Token=');
	$gate->setTransactionStatusUrl('https://apps.epg-services.com/V2/pp/tokenizer/get_result');
}

$ref = md5(rand(100000, 1000000000));

try 
{
    // Success URL
	$hostUrl = $_SERVER['HTTP_HOST'];
	$findme   = 'www.';
	$pos = strpos($hostUrl, $findme);

	if ( $pos === false ){
		$hostUrl = "www.".$hostUrl;
	}

    $SuccessURL = 'http://'.$hostUrl.'/success.php';
	

    // Checking if Recurring
    if(isset($_POST['recurring']) && ($_POST['recurring'] == 1))
    {
        // ($reference, $amount, $duration , $redirect, $mode = 'monthly', $trial_amount = 0, $trial_duration = 0 )
        $token = $gate->getTokenForRecurring($ref, $amount, 1, $SuccessURL);
    }
    elseif(isset($_POST['auth']) && ($_POST['auth'] == 1))
    {
       $token = $gate->getTokenForAuth($ref, $amount, $SuccessURL);
    }
    else {
        $token = $gate->getTokenForSale($ref, $amount, $SuccessURL);
    }
	
	
	
    // Redirecting to Payment Gateqway URL
    header('Location:'.$gate->getPaymentGatewayUrl($token, $lang));
}
catch(Exception $e)
{
	//print_r($e);
	die('Unexpected error, please try again or contact Administrator if you see the error again');
	exit;
	$errors['exception'] = 'Unexpected error, please try again or contact Administrator if you see the error again';
	include 'views/payment.phtml';
}





// $client = new Zend_Http_Client('http://www.a1plus.am');
// $response = $client->request();
// echo $response;



?>
