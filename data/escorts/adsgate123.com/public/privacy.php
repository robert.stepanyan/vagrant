<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head> 
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <?php
        require_once 'head.php';
      ?>
  </head>
  <body>
      <div class="shadow">
          <div class="container">
              <div class="header">
                  <a href="index.html"><img src="img/logo<?= $postfix ?>.png" alt="" /></a>
              </div>
              <div class="content">
                  <?php
                    $page = 'privacy';
                    require_once 'menu.php';
                  ?>

                  <div class="contentBody">

                        <p class="f22 blue title"><b>Privacy Policy</b></p>
                        <div class="pageInner">
                            <div class="privacyItem">
                                <p class="f16 blue"><b>Our Commitment to Privacy</b></p>
                                <p><b>Your Privacy is important to us.
    To better protect your privacy we provide this notice explaining our online information practices and the choices you can make about the way your information is collected and used. To make this notice easy to find, we make it available an our homepage and at every point where personally identifiable information may be requested.</b></p>
                            </div>

                            <div class="privacyItem">
                                <p class="f16 blue"><b>The Information We Collect</b></p>
                                <p><b>This notice applies to all information collected or submitted on this website. On some pages, you can order products/subscriptions, make requests, and register to receive materials. The types 			of personal information collected at these pages are: Fullname, Username, Email address.
								</p>
                            </div>

                            <div class="privacyItem">
                                <p class="f16 blue"><b>The Way We Use Information</b></p>
                                <p><b>We use collected information for protection of access to our system (username, password) and for user verification (email).</b></p>
                            </div>

                            <div class="privacyItem">
                                <p class="f16 blue"><b>Our Commitment To Data Security</b></p>
                                <p><b>To Prevent unauthorized access, maintain data accuracy, and ensure the correct use of information, we have put in place appropriate physical, electronic, and managerial procedures to safeguard and secure the information we collect online.</b></p>
                            </div>

                            <div class="privacyItem">
                                <p class="f16 blue"><b>How You Can Access Or Correct Your Information</b></p>
                                <p><b>You can access all your personally identifiable information that we collect online and maintain by SQL database. We use this procedure to better safeguard your information.</b></p>
                                <p class="padding10"><b>You can access all your personally identifiable information that we collect online and maintain by SQL database. We use this procedure to better safeguard your information.</b></p>
                                <p><b>To protect your privacy and security, we will also take reasonalble steps to verify your identity before granting access or making corrections.<br />
How to Contact us<br />
Should you have other questions or concerns about these privacy policies, send us an message</b></p>
                            </div>
                        </div>
                  </div>

              </div>

          </div>
          <?php
		  include('footer.php');
		  ?>
