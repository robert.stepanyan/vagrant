<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <?php
       require_once 'head.php';
      ?>
  </head>
  <body>
      <div class="shadow">
          <div class="container">
              <div class="header">
                  <a href="index.html"><img src="img/logo<?= $postfix ?>.png" alt="" /></a>
              </div>
              <div class="content">
                  <?php
                    $page = 'sites';
                    require_once 'menu.php';
                  ?>

                  <div class="contentBody">
                        <div class="pageInner">
                            <p class="paddingtop10">
                              <b>
                                  We run many internationally known advertising sites worldwide. ads<?= $siteName ?>.com offers only advertising for <?php echo $country ?>. You still can contact us if you are interested in advertising solutions for different countries.
                              </b>

                            </p>
                            <p class="f16 blue"><b>We are locally available in:</b></p>
                            <p>
                                <?php echo $content[$key]['sites']; ?>

                            </p>

                            <div class="siteItem">
                                <span class="blue f16"><b><?php echo $website ?> </b></span> <?php echo $content[$key]['sites_langing']; ?>
                                <div class="siteImage">
                                    <img src="img/<?php echo $website ?>.jpg" alt="" />
                                    <a href="http://<?php echo $website ?>" target="_blank">Click the image to view site</a>
                                </div>
                            </div>

                        </div>
                  </div>

              </div>

          </div>
          <?php
		  include('footer.php');
		  ?>