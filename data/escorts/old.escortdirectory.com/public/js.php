<?php
$CACHE_OFFSET = 7 * 24 * 60 * 60;
$JS_LOCATION  = "js/combined/";
$CSS_LOCATION = "css/combined/";

function getMaxModifTime( $js_sources ){
	$max_modif_time = null;
	foreach( $js_sources as $source ){
		$m_time = @filemtime($source);
		if( is_null($max_modif_time) ){
			$max_modif_time = $m_time;
		}
		if( $m_time > $max_modif_time ){
			$max_modif_time = $m_time;
		}
	}
	return $max_modif_time;
}


function writeToFile( $js_sources, $file_path ){
	//ATTENTION !!! Minify class is changed by Vahag. CLIENT CACHE VALIDATION IS DISABLED.
	$output = Minify::serve('Files', array(
        'files'  => $js_sources
        ,'quiet' => true
        //,'lastModifiedTime' => $lastModified
        ,'encodeOutput' => false
    ));
   
	$js = $output['content'];
	//echo $js;

	$f = @fopen($file_path, 'w');

	if (!$f) {
		echo 'Problem while writing to file.' . $file_path ; die;
	} else {
		$bytes = fwrite($f, $js);
		fclose($f);
	}
}


require dirname(__FILE__) . '/min/config.php';

set_include_path($min_libPath . PATH_SEPARATOR . get_include_path());

require_once 'Minify.php';

	//JS FILES
	$js_sources = array(
		'js/start.js',
		'js/mootools.js',
		//'js/Autosize.js',
		'js/dg-filter.js',
		'js/datepicker.js',
		'js/uniform.js',
		'js/overlay.js',
		'js/moopopup.js',
		'js/popups.js',
		'js/slideGallery.js',
		'js/bubble.js',
		'js/global.js',
		'js/slimbox.js',
		'js/scrollspy.js',
		'js/roar.js',
		'js/scrollable.js',
		//'js/jwplayer.js',
		'js/video-modal.js',
		'js/video-escort.js',
		'js/responsive.js',
		'js/email-collecting.js'
	);

	$other_sources = array(
		'listing' => array(
			'js/r-popup.js',
			'js/photo-roller.js',
			'js/scrollbars.js',
			'js/comments.js',
			'js/escorts.js',
			'js/hash.js',
			'js/Autocompleter.js',
			'js/Autocompleter.Local.js',
		),
		'blog' => array(
			'js/blog.js',
		),
		'reviews' => array(

		),
		'ca' => array(

		),
		'la' => array(
			'js/latest_actions.js',
		),
		'pf' => array(
			'js/photo_feed.js',
		),
		'payment' => array(
			
		),
		'private' => array(
			'js/fancy/Swiff.Uploader.js',
			'js/fancy/Fx.ProgressBar.js',
			'js/fancy/FancyUpload2.js',
			'js/fancy/FancyUpload3.Attach.js',
			'js/MooUpload.js',
			//'js/tiny_mce_3_3_5/tiny_mce.js',


			'js/FloatingTips.js',
			'js/private-rates.js',
			'js/private.js',
		),
		'payment' => array(
		)
	);



	//CSS FILES
	$css_sources = array(
		'css/reset.css',
		'css/slimbox.css',
		'css/datepicker.css',
		'css/uniform.css',
		'css/moopopup.css',
		'css/style.css',
		'css/roar.css',
		'css/slider.css',
		'css/scrollable.css',
		'css/profile.css',
		'css/video-modal.css',
		'css/jw_skins/ed-seven.css',
	);

	$css_other = array(
		'listing' => array(
			'css/scroll.css',
			'css/overwrite.css',
        	'css/responsive/overwrite.css'
		),
		'private' => array(
			'css/private.css',
			'css/overwrite.css',
        	'css/responsive/overwrite.css'
		)
	);

	$load_key = $_GET['load_key'];
	$type = $_GET['type'];
	
		
	/* Validate all get params */
	$types = array('css', 'js');

	if ( ! in_array($type, $types) ) {
		die('Invalid type was specified');
	}

	if ( $type == 'css' ) {
		if ( ! array_key_exists($load_key, $css_other) ) {
			die('Invalid load_key was specified');		
		} 
	} elseif ( $type == 'js' ) {
		if ( ! array_key_exists($load_key, $other_sources) ) {
			die('Invalid load_key was specified');		
		} 
	}
	/* Validate all get params */

	
	$SOURCE_LOCATION = ( $type == 'css' ) ? $CSS_LOCATION : $JS_LOCATION;
	if ( strlen($load_key) ) {
		$SOURCE_LOCATION .= '_' . $load_key . '.' . $type;
	} else {
		$SOURCE_LOCATION .= $type == 'js' ? '_scripts.js' : '_styles.css';
	}
	
	if ( $type == 'js' ) {
		$_sources = array_merge($js_sources, ( strlen($load_key) &&  isset( $other_sources[$load_key]) ? $other_sources[$load_key] : array()) );
	} elseif ( $type == 'css' ) {
		$_sources = array_merge($css_sources, ( strlen($load_key) &&  isset($css_other[$load_key]) ? $css_other[$load_key] : array()) );
	}

	if( file_exists($SOURCE_LOCATION)  ){
		$file_modif_time = @filemtime( $SOURCE_LOCATION );
		if( getMaxModifTime($_sources) > $file_modif_time ){
			writeToFile($_sources, $SOURCE_LOCATION);
		}else{
			// no need to write again			
		}		
	}else{
		writeToFile($_sources, $SOURCE_LOCATION);
	}

	$js = @file_get_contents( $SOURCE_LOCATION );
	
	if ( $type == 'js' ) {
		header ('content-type: text/javascript; charset: UTF-8');
	} elseif ( $type == 'css' ) {
		header ('content-type: text/css;');
	}
	//USER CACHE VALIDATION
	if( getMaxModifTime($_sources) < $file_modif_time && $_SERVER['HTTP_CACHE_CONTROL'] == 'max-age=0' && array_key_exists("HTTP_IF_MODIFIED_SINCE",$_SERVER) ){
		header("HTTP/1.1 304 Not Modified");
	} else {
		header ('last-modified: ' . gmdate ('D, d M Y H:i:s', getMaxModifTime($_sources) ) . ' GMT');
		header ('cache-control: max-age=0');
	}
	header ('expires: ' . gmdate ('D, d M Y H:i:s', time() + $CACHE_OFFSET ) . ' GMT');
	header ('date: ' . gmdate ('D, d M Y H:i:s', time() ) . ' GMT');

	
	ob_start ('ob_gzhandler');
	echo $js;
	ob_flush();
