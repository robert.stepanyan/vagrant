Sceon.EmailCollectingPopup = {};

Sceon.EmailCollectingPopup.inputPlaceHolder = 'Enter your email';
Sceon.EmailCollectingPopup.inProcess = false;
Sceon.EmailCollectingPopup.url = '';
Sceon.EmailCollectingPopup.domain = '.escortdirectory.com';

Sceon.EmailCollectingPopup.Show = function () {
	
	if ( Sceon.EmailCollectingPopup.inProcess ) return false;

	var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000' });
	page_overlay.disable();

	var y_offset = 130;

	Sceon.EmailCollectingPopup.inProcess = true;
	
	var city_slug = "";
	var r_url = "";
	
	new Request({
		url: Sceon.EmailCollectingPopup.url + r_url,
		method: 'get',
		onSuccess: function (resp) {
			var resp = new Element('div', {
				'id' : 'email-collecting-popup',
				'html': resp
			});

			new Element('div', {'class': 'close'}).inject(resp, 'top');

			Sceon.EmailCollectingPopup.inProcess = false;
			resp.inject(document.body);

			Sceon.EmailCollectingPopup.inputPlaceHolder = $$('#email-collecting-popup .email-input').get('value')[0];

			$('email-collecting-popup').setStyles({
				left: window.getWidth() / 2 - $('email-collecting-popup').getWidth() / 2,
				top: window.getHeight() / 2 + window.getScroll().y - $('email-collecting-popup').getHeight() / 2
			})

			$$('#email-collecting-popup .close').addEvent('click', function() {
				Cookie.write('ec_session_closed', true, {domain: Sceon.EmailCollectingPopup.domain});
				$('email-collecting-popup').destroy();
				page_overlay.enable();
			});

			$$('#email-collecting-popup .email-input').addEvent('focus', function() {
				if ( this.get('value') == Sceon.EmailCollectingPopup.inputPlaceHolder ) {
					this.set('value', '');	
				}
			}).addEvent('blur', function() {
				if ( this.get('value') == '' ) {
					this.set('value', Sceon.EmailCollectingPopup.inputPlaceHolder);
				}
			});

			var forms = $$('#email-collecting-popup form');
			forms.addEvent('submit', Sceon.EmailCollectingPopup.Send);
			forms.each(function (form) { form.set('action', form.get('action') + (form.get('action').indexOf('?') != -1 ? '&' : '?') + 'ajax'); });
		}
	}).send();
		
	
	return false;
}

Sceon.EmailCollectingPopup.Send = function (e) {
	e.stop();
	var overlay = new Cubix.Overlay($('email-collecting-popup'), { loader: _st('loader-small.gif'), position: '50%', no_relative: true });
	

	this.set('send', {
		onSuccess: function (resp) {
			overlay.disable();
			resp = JSON.decode(resp);

			overlay.enable();
			$$('.err').set('html', '');
			if ( resp.status == 'error' ) {
				$$('.err').set('html', resp.msg);
			}
			else if ( resp.status == 'success' ) {
				$$('#email-collecting-popup .ec-body').set('html', resp.html);
				
				Cookie.write('email_collecting', 'done', {domain: Sceon.EmailCollectingPopup.domain, duration: 365});
			}
		}.bind(this)
	});

	this.send();
}

Sceon.EmailCollectingPopup.ShowConfirmed = function(status) {

	if ( ! status ) status = 1;

	var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000' });
	

	var y_offset = 130;

	new Request({
		url: Sceon.EmailCollectingPopup.url + '?ec_status=' + status,
		method: 'get',
		onSuccess: function (resp) {
			page_overlay.disable();
			var resp = new Element('div', {
				'id' : 'email-collecting-popup',
				'class' : 'confirmed',
				'html': resp
			});

			new Element('div', {'class': 'close'}).inject(resp, 'top');
			resp.inject(document.body);

			$('email-collecting-popup').setStyles({
				left: window.getWidth() / 2 - $('email-collecting-popup').getWidth() / 2,
				top: window.getHeight() / 2 - $('email-collecting-popup').getHeight() / 2 + window.getScroll().y - y_offset
			});

			$$('#email-collecting-popup .close').addEvent('click', function() {
				Cookie.write('ec_session_closed', true, {domain: Sceon.EmailCollectingPopup.domain});
				$('email-collecting-popup').destroy();
				page_overlay.enable();
			});
		}
	}).send();
}