Cubix.Pl = {};

var PlannerPicker = new Class({
	Extends: DatePicker,
	appointmentDays: [],
	holidayDays: [],
	otherDays: [],
	selectAllCheckboxes: [],
	/*monthInterval: 3,
	mainContainerId: 'planner-container',*/
	hideOtherMonthDays: true,
	editable: false,
	setEventDays: function(appointmentDays, holidayDays, otherDays) {
		if (appointmentDays) {
			this.appointmentDays = appointmentDays;
		}

		if (holidayDays) {
			this.holidayDays = holidayDays;
		}
		
		if (otherDays) {
			this.otherDays = otherDays;
		}
	},
	initialize: function(attachTo, options, appointmentDays, holidayDays, otherDays, monthInterval, container) {
		this.setEventDays(appointmentDays, holidayDays, otherDays);
		
		this.attachTo = attachTo;
		this.mainContainerId = container ? container : 'planner-container';
		this.monthInterval = monthInterval ? monthInterval : 1;
		this.setOptions(options).attach();
		if (this.options.timePickerOnly) {
			this.options.timePicker = true;
			this.options.startView = 'time';
		}

		this.formatMinMaxDates();
		document.addEvent('mousedown', this.close.bind(this));
	},
	attach: function() {
		// toggle the datepicker through a separate element?
		if ($chk(this.options.toggleElements)) {
			var togglers = $$(this.options.toggleElements);
			document.addEvents({
				'keydown': function(e) {
					if (e.key == "tab") {
						this.close(null, true);
					}
				}.bind(this)
			});
		}
		;

		// attach functionality to the inputs
		$$(this.attachTo).each(function(item, index) {
			// never double attach
			/*if (item.retrieve('datepicker'))
				return;*/

			// determine starting value(s)
			if ($chk(item.get('value'))) {
				var init_clone_val = this.format(new Date(this.unformat(item.get('value'), this.options.inputOutputFormat)), this.options.format);
			} else if (!this.options.allowEmpty) {
				var init_clone_val = this.format(new Date(), this.options.format);
			} else {
				var init_clone_val = '';
			}

			// create clone
			var display = item.getStyle('display');
			var clone = item
					.setStyle('display', this.options.debug ? display : 'none')
					.store('datepicker', true) // to prevent double attachment...
					.clone()
					.store('datepicker', true) // ...even for the clone (!)
					.removeProperty('name')    // secure clean (form)submission
					.setStyle('display', display)
					.set('value', init_clone_val)
					.inject(item, 'after');
			this.onFocus(item, clone);

			// events
			if ($chk(this.options.toggleElements)) {
				togglers[index]
						.setStyle('cursor', 'pointer')
						.addEvents({
							'click': function(e) {
								this.onFocus(item, clone);
							}.bind(this)
						});
				clone.addEvents({
					'blur': function() {
						item.set('value', clone.get('value'));
					}
				});
			} else {
				clone.addEvents({
					'keydown': function(e) {
						if (this.options.allowEmpty && (e.key == "delete" || e.key == "backspace")) {
							item.set('value', '');
							e.target.set('value', '');
							this.close(null, true);
						} else if (e.key == "tab") {
							this.close(null, true);
						} else {
							e.stop();
						}
					}.bind(this),
					'focus': function(e) {
						this.onFocus(item, clone);
					}.bind(this)
				});
			}
		}.bind(this));
	},
	dayclick: function(obj, e) {
		var selected_date = (this.sel_d.month + 1) + '/' + parseInt(this.sel_d.day) + '/' + this.sel_d.year;
		
		if (!e.target.hasClass('day')) return false;
		
		if (obj.hasClass('appointment') == true) 
		{
			this.loadPopup(selected_date, obj);
		}
		else if (obj.hasClass('holiday') == true)
		{
			this.loadPopup(selected_date, obj);
		}
		else if (obj.hasClass('other') == true)
		{
			this.loadPopup(selected_date, obj);
		}
		
	},
	loadPopup: function(date, obj) {
		closeAll();
		
		getViewGlobal(date, obj);
	},
	renderMonth: function(interator) {
		var month = this.d.getMonth();

		month += interator;

		if (month > 11) {
			month = month - 12;
			this.d.setYear(this.d.getFullYear() + 1);
		}

		this.d.setDate(1);
		this.d.setMonth(month);

		var selectAll = new Date();
		selectAll.setDate(1);
		selectAll.setMonth(month);
		selectAll.setYear(this.d.getFullYear());

		this.picker.getElement('.titleText').set('text', this.options.months[month] + ' ' + this.d.getFullYear());
		
		var r = new Element('input', {
			type: 'radio',
			'class': 'r-m',
			name: 'r_m',
			value: ((month + 1).toString() + '-' + this.d.getFullYear().toString()),
			id: ((month + 1).toString() + '-' + this.d.getFullYear().toString())
		});
		
		r.inject(this.picker.getElement('.titleText'), 'top');

		while (this.d.getDay() != this.options.startDay) {
			this.d.setDate(this.d.getDate() - 1);
		}

		var container = new Element('div', {
			'class': 'days'
		}).inject(this.newContents);
		var titles = new Element('div', {
			'class': 'titles'
		}).inject(container);
		var d, i, classes, e, weekcontainer;

		for (d = this.options.startDay; d < (this.options.startDay + 7); d++) {
			new Element('div', {
				'class': 'title day day' + (d % 7)
			}).set('text', this.options.days[(d % 7)].substring(0, this.options.dayShort)).inject(titles);
		}

		var available = false;
		var t = this.today.toDateString();

		var currentChoice = this.dateFromObject(this.choice).toDateString();

		for (i = 0; i < 42; i++) {
			classes = [];
			classes.push('day');
			classes.push('day' + this.d.getDay());
			if (this.d.toDateString() == t)
				classes.push('today');
			if (this.d.toDateString() == currentChoice)
				classes.push('selected');
			if (this.d.getMonth() != month)
				classes.push('otherMonth');

			var dataPicker = this;

			/* appointmentDays Select */
			Array.each(this.appointmentDays, function(day, index) {
				var sd = new Date(Date.parse(day));

				var tmp_a = dataPicker.dateFromObject(dataPicker.dateToObject(sd)).toDateString();

				if (dataPicker.d.toDateString() == tmp_a) {
					classes.push('appointment');
				};
			});

			/* holidayDays Select */
			Array.each(this.holidayDays, function(day, index) {
				var sd = new Date(Date.parse(day));

				var tmp_a = dataPicker.dateFromObject(dataPicker.dateToObject(sd)).toDateString();

				if (dataPicker.d.toDateString() == tmp_a) {
					classes.push('holiday');
				};
			});
			
			/* otherDays Select */
			Array.each(this.otherDays, function(day, index) {
				var sd = new Date(Date.parse(day));

				var tmp_a = dataPicker.dateFromObject(dataPicker.dateToObject(sd)).toDateString();

				if (dataPicker.d.toDateString() == tmp_a) {
					classes.push('other');
				};
			});

			if (i % 7 == 0) {
				weekcontainer = new Element('div', {
					'class': 'week week' + (Math.floor(i / 7))
				}).inject(container);
			}

			e = new Element('div', {
				'class': classes.join(' ')
			}).set('text', this.d.getDate()).inject(weekcontainer);

			if (e.hasClass('otherMonth') && this.hideOtherMonthDays) {
				e.set('text', '');
			}
			
			e.addEvent('click', function(e, d) {
				this.sel_d = d;
			}.bindWithEvent(this, {
				day: this.d.getDate(),
				month: this.d.getMonth(),
				year: this.d.getFullYear()
			}));

			//e.addEvent('click', this.dayclick.bind(this, e));
			var self = this;
			e.addEvent('click', function(ev) {
				self.dayclick(this, ev);
			});

			this.d.setDate(this.d.getDate() + 1);
		}
	},
	render: function(fx) {
		for (var i = 0; i < this.monthInterval; i++) {
			this.constructPicker();

			// remember current working date
			var startDate = new Date(this.d.getTime());

			// intially assume both left and right are allowed
			this.limit = {
				right: false,
				left: false
			};

			// render! booty!
			if (this.mode == 'decades') {
				this.renderDecades();
			} else if (this.mode == 'year') {
				this.renderYear();
			} else if (this.mode == 'time') {
				this.renderTime();
				this.limit = {
					right: true,
					left: true
				}; // no left/right in timeview
			} else {
				this.renderMonth(i);
			}

			this.picker.getElement('.previous').setStyle('visibility', this.limit.left ? 'hidden' : 'visible');
			this.picker.getElement('.next').setStyle('visibility', this.limit.right ? 'hidden' : 'visible');
			
			// restore working date
			this.d = startDate;

			// if ever the opacity is set to '0' it was only to have us fade it in here
			// refer to the constructPicker() function, which instantiates the picker at opacity 0 when fading is desired
			if (this.picker.getStyle('opacity') == 0) {
				this.picker.tween('opacity', 0, 1);
			}
		}

		// animate
		if ($chk(fx))
			this.fx(fx);
	},
	constructPicker: function() {
		this.picker = new Element('div', {
			'class': this.options.pickerClass
		}).inject($(this.mainContainerId));
		if (this.options.useFadeInOut) {
			this.picker.setStyle('opacity', 0).set('tween', {
				duration: this.options.animationDuration
			});
		}

		var h = new Element('div', {
			'class': 'header'
		}).inject(this.picker);
		var titlecontainer = new Element('div', {
			'class': 'title'
		}).inject(h);
		new Element('div', {
			'class': 'previous'
		}).addEvent('click', this.previous.bind(this)).set('text', '').inject(h);
		new Element('div', {
			'class': 'next'
		}).addEvent('click', this.next.bind(this)).set('text', '').inject(h);
		new Element('div', {
			'class': 'closeButton'
		}).addEvent('click', this.close.bindWithEvent(this, true)).set('text', '').inject(h);
		new Element('span', {
			'class': 'titleText'
		}).inject(titlecontainer);

		var b = new Element('div', {
			'class': 'body'
		}).inject(this.picker);
		this.bodysize = b.getSize();
		this.slider = new Element('div', {
			styles: {
				position: 'absolute',
				top: 0,
				left: 0,
				width: 2 * this.bodysize.x,
				height: this.bodysize.y
			}
		}).set('tween', {
			duration: this.options.animationDuration,
			transition: Fx.Transitions.Quad.easeInOut
		}).inject(b);

		this.oldContents = new Element('div', {
			styles: {
				position: 'absolute',
				top: 0,
				left: this.bodysize.x,
				width: this.bodysize.x,
				height: this.bodysize.y
			}
		}).inject(this.slider);
		
		this.newContents = new Element('div', {
			styles: {
				position: 'absolute',
				top: 0,
				left: 0,
				width: this.bodysize.x,
				height: this.bodysize.y
			}
		}).inject(this.slider);
	},
	checkAll: function(day, ischecked) {
		var checkboxField = day.getMonth() + '-' + day.getFullYear();

		if (ischecked == 0) {
			this.selectAllCheckboxes.erase(checkboxField);
		} else {
			this.selectAllCheckboxes.combine([checkboxField]);
		}

		var daysIn = daysInMonth(day.getMonth() + 1, day.getFullYear());

		var values = [];
		for (var i = 1; i <= daysIn; i++) {
			//day.setDate(i);
			var selected = new Object();
			selected.day = i;
			selected.month = (day.getMonth());
			selected.year = day.getFullYear();

			var event_day = '01';
			if (i < 10) {
				event_day = '0' + i;
			} else {
				event_day = i;
			}
			if (ischecked == 0) {
				this.unselect(selected, 1);
				this.availableDays.erase((day.getMonth() + 1) + "/" + event_day + "/" + day.getFullYear());

			} else {
				values.combine([(day.getMonth() + 1) + "/" + event_day + "/" + day.getFullYear()]);
				this.select(selected, 1);
			}
		}

		this.availableDays.combine(values);

		$$('.' + this.options.pickerClass).destroy();

		this.render();
	},
	close: function(e, force) {
		return;
	},
	previous: function() {
		$$('.' + this.options.pickerClass).destroy();

		if (this.mode == 'decades') {
			this.d.setFullYear(this.d.getFullYear() - this.options.yearsPerPage);
		} else if (this.mode == 'year') {
			this.d.setFullYear(this.d.getFullYear() - 1);
		} else if (this.mode == 'month') {
			this.d.setDate(1);
			this.d.setMonth(this.d.getMonth() - 1);
		}
		this.render();
		changeMonth();
	},
	next: function() {
		$$('.' + this.options.pickerClass).destroy();

		if (this.mode == 'decades') {
			this.d.setFullYear(this.d.getFullYear() + this.options.yearsPerPage);
		} else if (this.mode == 'year') {
			this.d.setFullYear(this.d.getFullYear() + 1);
		} else if (this.mode == 'month') {
			this.d.setDate(1);
			this.d.setMonth(this.d.getMonth() + 1);
		}
		this.render();
		changeMonth();
	},
	select: function(values, skipIfExists) {
		if (!this.editable) {
			return;
		}
		this.choice = $merge(this.choice, values);
		var d = this.dateFromObject(this.choice);
		var key = this.choice.year + '-' + (this.choice.month + 1) + '-' + this.choice.day;

		var splatted = d.toDateString();
		this.input.set('value', this.format(d, this.options.inputOutputFormat));

		//1 available
		//2 non available
		//3 non
		var date = splatted;

		if (!date)
			return;

		var found = false;
		$$('#sh-events input[type=hidden]').each(function(el) {
			var _date = el.get('value');
			if (date == _date) {
				found = true;
				el.set('value', date);

				var sub_div = el.getParent('div').getChildren('.typeClass');

				if (sub_div.get('value') == 0) {
					sub_div.set('value', 1);
				} else {
					if (!skipIfExists) {
						if (sub_div.get('value') == 1) {
							sub_div.set('value', 2);
						} else {
							if (sub_div.get('value') == 2) {
								sub_div.set('value', 0);
							}
						}
					}

				}
			}
		});

		var selected_country_count = $$('#sh-events input[type=hidden]').length + 1;
		
		if (!found) {
			var eventday = new Element('div', {
				'class': 'eventday'
			}).inject($('sh-events'));

			new Element('input', {
				type: 'hidden',
				name: 'date_names[]',
				value: date
			}).inject(eventday);

			new Element('input', {
				type: 'hidden',
				name: 'selected_days[' + key + '][type]',
				value: 1,
				'class': 'typeClass'
			}).inject(eventday);

		}
	},
	unselect: function(values) {
		if (!this.editable) {
			return;
		}
		this.choice = $merge(this.choice, values);
		var d = this.dateFromObject(this.choice);
		var key = this.choice.year + '-' + (this.choice.month + 1) + '-' + this.choice.day;

		var splatted = d.toDateString();
		this.input.set('value', this.format(d, this.options.inputOutputFormat));

		var date = splatted;

		if (!date)
			return;

		var found = false;
		$$('#sh-events input[type=hidden]').each(function(el) {
			var _date = el.get('value');
			if (date == _date) {
				found = true;

				el.set('value', date);

				var sub_div = el.getParent('div').getChildren('.typeClass');

				if (sub_div.get('value') == 1) {
					sub_div.set('value', 0);
				}
			}
		});

		if (!found) {
			var eventday = new Element('div', {
				'class': 'eventday'
			}).inject($('sh-events'));

			new Element('input', {
				type: 'hidden',
				name: 'date_names[]',
				value: date
			}).inject(eventday);

			new Element('input', {
				type: 'hidden',
				name: 'selected_days[' + key + '][type]',
				value: 0,
				'class': 'typeClass'
			}).inject(eventday);

		}
	},
});

function daysInMonth(month, year) {
	var dd = new Date(year, month, 0);
	return dd.getDate();
}

function selectEscort() {
	if ($('escort'))
		$('escort').addEvent('change', function() {
			var overlay = new Cubix.Overlay($('planner'), {
				loader: _st('loader-small.gif'),
				position: '50%',
				offset: {
					left: 0,
					top: -1
				}
			});

			overlay.disable();

			var es = this.get('value');
			
			$('escort_id').set('value', es);
			
			get(overlay);
		});
}

function renderPlanner() {
	new PlannerPicker('.calendar1', {
		pickerClass: 'schedule',
		months: Cubix.Pl.months,
		days: Cubix.Pl.days
	}, Cubix.Pl.a, Cubix.Pl.h, Cubix.Pl.o, 3, 'planner-container');
	
	$(Cubix.Pl.r_m).set('checked', 'checked');
}

function alertDetails() {
	var alert_ch = $$('input[name=alert]:checked').get('value');
				
	if (alert_ch == "2")
	{
		if (!$('alert-detals').hasClass('none'))
			$('alert-detals').addClass('none');
	}
	else
	{
		if ($('alert-detals').hasClass('none'))
			$('alert-detals').removeClass('none');
	}
}

function add() {
	$$('.add').addEvent('click', function(e) {
		if (Cubix.Pl.isAgency && !$('escort_id').get('value'))
		{
			$$('.sel-err').removeClass('none');
			$('escort').addClass('err');
			
			return false;
		}
		
		closeAll();
		
		var overlay = new Cubix.Overlay($('planner'), {
			has_loader: false,
			position: '50%',
			offset: {
				left: 0,
				top: -1
			},
			color: '#000',
			opacity: .38
		});

		overlay.disable();
		
		new Request({
			url: lang_id + '/planner/ajax-add',
			method: 'get',
			onSuccess: function (resp) {			
				$('add1').set('html', resp);
				
				new DatePicker($('date'), {
					pickerClass: 'datepicker',
					positionOffset: { x: -10, y: 0 },
					allowEmpty: true,
					format: 'j M Y',
					toggleElements: '.cal',
					months: Cubix.Pl.months,
					days: Cubix.Pl.days
				});
				
				close(overlay);
				
				alertDetails();
				
				$$('input[name=alert]').addEvent('click', function () {
					alertDetails();
				});
				
				save(overlay);
			}
		}).send();

		return false;
	});
}

function save(overlay, id) {
	$$('.save').addEvent('click', function(e) {
		e.stop();
		
		if (id)
			var pid = 'p' + id;
		else
			var pid = 'padd';
		
		var ov = new Cubix.Overlay($(pid), {
			loader: _st('loader-small.gif'),
			position: '50%',
			offset: {
				left: 0,
				top: -1
			}
		});

		ov.disable();

		var alert_type = new Array();

		$$('input[name=alert_type]:checked').each(function(e) {
			alert_type.push(e.get('value'));
		});
		
		var ac = 'add';
		var data = {
			type: $$('input[name=type]:checked')[0].get('value'),
			date: $('date').get('value'),
			hour: $('hour').get('value'),
			minute: $('minute').get('value'),
			free_text: $('free-text').get('value'),
			alert: $$('input[name=alert]:checked')[0].get('value'),
			alert_type: alert_type,
			alert_time: $('alert-time').get('value'),
			alert_time_unit: $('alert-time-unit').get('value')
		};
		
		if (id)
		{
			ac = 'edit';
			data.id = id;
		}
		
		if (Cubix.Pl.isAgency)
			data.escort_id = $('escort_id').get('value');

		new Request({
			url: lang_id + '/planner/ajax-' + ac,
			method: 'post',
			data: data,
			onSuccess: function (resp) {
				resp = JSON.decode(resp);
				var status = resp.status;

				if (status == 'success')
				{
					get(overlay);

				}
				else if (status == 'error')
				{
					ov.enable();
					
					var msgs = resp.msgs;

					$$('.in-d').removeClass('err');
					$('free-text').removeClass('err');
					$('chb').removeClass('err');
					$('alert-time').removeClass('err');

					if (msgs.date)
					{
						$$('.in-d').addClass('err');
					}

					if (msgs.free_text)
					{
						$('free-text').addClass('err');
					}

					if (msgs.alert_type)
					{
						$('chb').addClass('err');
					}

					if (msgs.alert_time)
					{
						$('alert-time').addClass('err');
					}
				}
			}
		}).send();
	});
}

function get(overlay) {
	if ($('escort_id'))
		var escort_id = $('escort_id').get('value');
	
	var esc = '';
	
	if (escort_id)
		esc = '&escort_id=' + escort_id;
	
	var r_m = '';
	
	if ($$('input[name=r_m]:checked').get('value').length)
		r_m = $$('input[name=r_m]:checked').get('value').toString();
	else
	{
		var dt = new Date();
		r_m = (dt.getMonth() + 1).toString() + '-' + dt.getFullYear();
	}
		
	new Request({
		url: lang_id + '/planner/ajax-get?r_m=' + r_m + esc,
		method: 'get',
		evalScripts: true,
		onSuccess: function (resp) {
			$('ajax-content').set('html', resp);

			renderPlanner();
			add();
			getView();
			selectEscort();
			changeMonth();

			$$('.popup').destroy();
			
			if (overlay)
				overlay.enable();
		}
	}).send();
}

function close(overlay) {
	$$('.x').addEvent('click', function() {
		$$('.popup').destroy();
		
		if (overlay)
			overlay.enable();
		
		return false;
	});
}

function closeAll() {
	$$('.x').fireEvent('click');
}

function getView() {
	$$('.cur-view').addEvent('click', function (e) {
		if (!e.target.hasClass('cur-view')) return false;
		
		closeAll();
		
		var self = this;
		var date = self.get('rel');
		
		getViewGlobal(date, self);
		
		return false;
	});
}

function getViewGlobal(date ,obj) {
	if ($('escort_id'))
		var escort_id = $('escort_id').get('value');
	
	var esc = '';
	
	if (escort_id)
		esc = '&escort_id=' + escort_id;
	
	new Request({
		url: lang_id + '/planner/ajax-get-event?date=' + date + esc,
		method: 'get',
		onSuccess: function (resp) {
			var pop  = new Element('div', {html: resp});
			pop.inject(obj);
			
			close();

			$$('.remove').addEvent('click', function() {
				var id = this.get('rel');
				var pid = 'p' + date.split('/').join('');
				
				var ov = new Cubix.Overlay($(pid), {
					loader: _st('loader-small.gif'),
					position: '50%',
					offset: {
						left: 0,
						top: -1
					}
				});

				ov.disable();
								
				new Request({
					url: lang_id + '/planner/ajax-remove',
					method: 'post',
					data: {
						id: id
					},
					onSuccess: function (resp) {
						get();
					}
				}).send();
				
				return false;
			});
			
			$$('.edit').addEvent('click', function() {
				var id = this.get('rel');
				var pid = 'p' + date.split('/').join('');
				
				var ov = new Cubix.Overlay($(pid), {
					loader: _st('loader-small.gif'),
					position: '50%',
					offset: {
						left: 0,
						top: -1
					}
				});

				ov.disable();
												
				new Request({
					url: lang_id + '/planner/ajax-edit',
					method: 'get',
					data: {
						id: id
					},
					onSuccess: function (resp) {
						ov.enable();
						closeAll();
						
						var overlay = new Cubix.Overlay($('planner'), {
							has_loader: false,
							position: '50%',
							offset: {
								left: 0,
								top: -1
							},
							color: '#000',
							opacity: .38
						});

						overlay.disable();
						
						var myScroll = new Fx.Scroll(window);
						myScroll.toElement($('planner'));
						
						$('add1').set('html', resp);
				
						new DatePicker($('date'), {
							pickerClass: 'datepicker',
							positionOffset: { x: -10, y: 0 },
							allowEmpty: true,
							format: 'j M Y',
							toggleElements: '.cal',
							months: Cubix.Pl.months,
							days: Cubix.Pl.days
						});

						close(overlay);

						alertDetails();

						$$('input[name=alert]').addEvent('click', function () {
							alertDetails();
						});

						save(overlay, id);
					}
				}).send();
				
				return false;
			});
		}
	}).send();
}

function changeMonth() {
	$$('.r-m').addEvent('click', function() {
		get();
	});
}

Cubix.Pl.load = function() {	
	var overlay = new Cubix.Overlay($('dashboard'), {color: '#fff', opacity: .6, loader: _st('loader-small.gif'), position: '50%', offset: {bottom: 0, top: 0}});
	
	overlay.disable();
	
	get(overlay);
	
	return false;
}
