Sceon.ShowTerms = function (link) {
	window.open(link, 'showTerms', 'menubar=no, resizable=yes, status=no, scrollbars=yes, toolbar=no, width=640, height=480');
	return false;
}

var _log = function (data) {
	if ( console && console.log != undefined ) {
		console.log(data);
	}
}

var _st = function (object) {
	var parts = object.split('.');
	if ( parts.length < 2 ) {
		return _log('invalid parameter, must contain filename with extension');
	}

	var ext = parts[parts.length - 1];

	var base_url = 'http://st.escortdirectory.com',
		prefix = '';

	switch ( ext ) {
		case 'jpg':
		case 'gif':
		case 'png':
			prefix = '/img/';
			break;
		case 'css':
			prefix = '/css/';
			break;
		case 'js':
			prefix = '/js/';
			break;
		default:
			return _log('invalid extension "' + ext + '"');
	}

	url = base_url + prefix + object;

	return url;
}

Sceon.initLangPicker = function() {

	var selected_cont = $$('.flags .selected')[0];
	var lng_iso = selected_cont.getChildren('.lng-iso')[0];
	var flag = selected_cont.getChildren('.flag')[0];
	var select = selected_cont.getNext('div.select');

	selected_cont.addEvent('click', function(e){
		e.stop();
		this.getNext('div.select').toggleClass('none');
	});

	$$('/*.flags .select .s-lng-iso, */.flags .select .flag').addEvent('click', function(e){
		e.stop();

		var curr_lng_iso = lng_iso.get('rel');
		var curr_lng = lng_iso.get('lng');

		lng_iso.set('html', this.get('rel')).set('rel', this.get('rel')).set('lng', this.get('lng'));
		flag.set('class', '').set('class', 'flag ' + this.get('lng')).set('rel', this.get('rel')).set('lng', this.get('lng'));

		select.addClass('none');

		var url = this.get('href');
		window.location = url;

		var e_overlay = new Cubix.Overlay($('container'), {color: '#fff', opacity: .6, loader: _st('loader-tiny.gif'), position: '90px', offset: {bottom: 0, top: 0}});
		e_overlay.disable();
	});
};

Sceon.initFooterLangPicker = function() {

	$$('#footer .f-flags a').addEvent('click', function(e){
		e.stop();

		var url = this.get('href');
		window.location = url;

		var e_overlay = new Cubix.Overlay($('container'), {color: '#fff', opacity: .6, loader: _st('loader-tiny.gif'), position: '90px', offset: {bottom: 0, top: 0}});
		e_overlay.disable();
	});
};

Sceon.iniSignInBox = function() {

	if ( $$('#signin-wrapper .btn-signin').length ) {
		$$('#signin-wrapper .btn-signin').addEvent('click', function(e){
			e.stop();

			$$('#signin-wrapper .login-box')[0].toggleClass('none');
		});

		$$('#signin-wrapper .btn-cancel').addEvent('click', function(e){
			e.stop();

			$$('#signin-wrapper .login-box')[0].addClass('none');
		});
	}

	var username = $$('.login-box input[name=username]')[0];
	var password = $$('.login-box input[name=password]')[0];

	if ( username && password ) {

		username.addEvent('focus', function(){
			if ( this.get('value') == headerVars.tUsername ) {
				this.set('value', '');
			}
		});
		username.addEvent('blur', function(){
			if ( ! this.get('value').length ) {
				this.set('value', headerVars.tUsername);
			}
		});

		var initPassword = function(pass) {
			pass.addEvent('focus', function() {
				this.destroy();
				injectNewPassword();
			});
		};

		var injectNewPassword = function() {
			var pass = new Element('input', {
				name: 'password',
				type: 'password',
				'class': 'si-txt pass',
				value: ''
			}).inject(username, 'after');

			pass.addEvent('blur', function() {
				if ( ! this.get('value').length ) {
					this.destroy();
					var pass = new Element('input', {
						name: 'password',
						type: 'text',
						'class': 'si-txt pass',
						value: headerVars.tPassword
					});
					pass.inject(username, 'after');
					initPassword(pass);
				}
			});

			pass.focus();
			pass.focus();
			pass.focus();
		};

		initPassword(password);
	}
};

Sceon.initContactUsLinks = function() {

	if( ! $$('a.contact').length ) return;

	$$('a.contact').removeEvents('click');
	$$('a.contact').addEvent('click', function(e){
		e.stop();

		Popup.ContactUs.display();
	});
};

Sceon.initAdvertiseLinks = function() {
	return;
	if( ! $$('a.advertise').length ) return;

	$$('a.advertise').removeEvents('click');
	$$('a.advertise').addEvent('click', function(e){
		e.stop();

		Popup.Advertise.display();
	});
};

Sceon.initSideMenuSimple = function() {
	var top_btn = $$('#side-bar .block .head a.top-btn');

	var menu_lis = $$('#side-bar .block a.closed, #side-bar .block li.li-country');

	var subs = $$('#side-bar .block .sub');

	var root = $$('#side-bar ul.root');
	var root_all_countries = $$('#side-bar ul.root.all-countries')[0];

	subs.each(function(el){
		var height = el.getCoordinates().height;
		el.store('sub-height', height);
		//el.addClass('none');
	});

	root.removeClass('visibility-hidden');

	var filter = new DG.Filter({
	  filterField : $('countrySearchField'),
	  filterEl : $$('.all-countries-filter')[0],
	  xPathFilterElements : '.country',
	  onMatchShowChildren : true
	});

	if ( $('citySearchField') ) {
		var filterCity = new DG.Filter({
		  filterField : $('citySearchField'),
		  filterEl : $$('.filter-cities')[0],
		  xPathFilterElements : '.city',
		  onMatchShowChildren : true,
		  listeners : {
		    beforefilter : function() { 

		    },
		    afterfilter : function() { 
		    	var sub = $$('li.sub')[0];
		    	var scrollbar = $$('.mscrollbar');

		    	var sub_lis = sub.getElements('ul li');
		    	
		    	var count = 0;
				sub_lis.each(function(el){
					if ( el.getStyle('display') != 'none' ) {
						count++;
					}
				});

				if ( count <= 12 ) {
					scrollbar.setStyle('display', 'none');
				} else {
					scrollbar.setStyle('display', '');
				}				

				window.fireEvent('resize');
		    }
		  }
		});
	}

	$$('.mscrollbar').destroy();
	var sc_root = new Scrollable(root_all_countries);
	Sceon.all_scroll = sc_root;

	if ( root_all_countries.getElements('li.li-country').length < 24 ) {
		Sceon.all_scroll.hideContainer(true);
	}

	//block open/close btn init
	top_btn.addEvent('click', function(e){
		e.stop();

		var self = this;
		var root = this.getParent('div.head').getNext('ul.root');

		root.setStyle('overflow', 'hidden');

		if ( this.hasClass('closed') ) {
			new Fx.Tween(root, {
				duration: 300,
				transition: 'quart:out',
				property: 'height',
				onStart: function() {
					root.setStyle('height', 0);
					root.removeClass('none');
					self.removeClass('closed').addClass('opened');
				},
				onComplete: function() {
					root.setStyle('height', 'auto');

					Sceon.Escorts.ss.options.min = window.getScrollSize().y - window.getSize().y - 1000;
				}
			}).start(0, this.retrieve('root-height'));

		} else {

			var height = root.getCoordinates().height;
			this.store('root-height', height);

			new Fx.Tween(root, {
				duration: 300,
				transition: 'quart:out',
				property: 'height',
				onStart: function() {
					self.removeClass('opened').addClass('closed');
				},
				onComplete: function() {
					root.addClass('none');

					Sceon.Escorts.ss.options.min = window.getScrollSize().y - window.getSize().y - 1000;
				}
			}).start(height, 0);
		}
	});
};

Sceon.checkForNewPms = function() {
	new Request.JSON({
		url: lang_id + '/private-messaging/check-for-new-messages',
		method: 'GET',
		onSuccess: function (resp) {
			if ( resp.result ) {
				if ( ! Cookie.read('pm_new_messages') || resp.count != Cookie.read('pm_new_messages') ) {
					if ( resp.count > Cookie.read('pm_new_messages') ) {
					
						if ( $$('#private-messaging table').length && typeof PrivateMessaging != 'undefined' ) {
							PrivateMessaging.LoadThreads(1);
						} else {
							var growl = new mooGrowl({
								position: "right",
								containerClass: 'new-pm-message',
								notificationDelay: 10000
							});

							var message = headerVars.pmNewMessageText.replace('#count#', resp.count);
							message = message.replace('#link#', '"/dashboard"')
							growl.notify({
								text: message,
								delay: 0
							});
						}
						
					}
					
					if ( typeof PrivateMessaging != 'undefined' && PrivateMessaging.updateUnreadThreadsCount ) {
						PrivateMessaging.updateUnreadThreadsCount(resp.count);
					}
					Cookie.write('pm_new_messages', resp.count, {duration: 1/192})
				}
				
			}
		}
	}).send();
	return;
}

var Geography = {
	loadCities: function (city, country, city_id, hidden) {

		if ( ! $defined(hidden) ) {
			hidden = false;
		}

		var selected;
		if ( hidden ) {
			selected = country.get('value');
		}
		else {
			selected = country.getSelected();
		}

		if ( ! selected.length ) return;
		if ( ! hidden ) {
			var country_id = selected[0].get('value');
		}
		else {
			var country_id = selected;
		}

		if ( ! country_id ) {
			city.empty();
			new Element('option', { value: '', html: '- city -' }).inject(city);
			return;
		}

		$$(country, city).set('disabled', 'disabled');

		new Request({
			url: lang_id + '/geography/ajax-get-cities',
			data: { country_id: country_id },
			method: 'get',
			onSuccess: function (resp) {
				resp = JSON.decode(resp, true);
				if ( ! resp ) return;
				city.empty();
				new Element('option', { value: '', html: '- city -' }).inject(city);

				resp.data.each(function (c) {
					new Element('option', {
						value: c.id,
						html: c.title + '&nbsp;&nbsp;',
						selected: city_id == c.id ? 'selected' : null
					}).inject(city);
				});
				$$(country, city).set('disabled', null);

                $$(country, city).fireEvent('pakistan');
			}
		}).send();
	},
	loadCitiesClassifiedAds: function (city, country, city_id, hidden) {

		if ( ! $defined(hidden) ) {
			hidden = false;
		}

		var selected;
		if ( hidden ) {
			selected = country.get('value');
		}
		else {
			selected = country.getSelected();
		}

		if ( ! selected.length ) return;
		if ( ! hidden ) {
			var country_id = selected[0].get('value');
		}
		else {
			var country_id = selected;
		}

		if ( ! country_id ) {
			city.empty();
			new Element('option', { value: '', html: '- city -' }).inject(city);
			return;
		}

		$$(country, city).set('disabled', 'disabled');

		new Request({
			url: lang_id + '/geography/ajax-get-cities-classified-ads',
			data: { country_id: country_id },
			method: 'get',
			onSuccess: function (resp) {
				resp = JSON.decode(resp, true);
				if ( ! resp ) return;
				city.empty();
				new Element('option', { value: '', html: '- city -' }).inject(city);

				resp.data.each(function (c) {
					new Element('option', {
						value: c.id,
						html: c.title + '&nbsp;&nbsp;',
						selected: city_id == c.id ? 'selected' : null
					}).inject(city);
				});
				$$(country, city).set('disabled', null);

				$$(country, city).fireEvent('pakistan');
			}
		}).send();
	}
}

Sceon.initRecaptcha = function() {
	if ( ! $$('a.recaptcha-refresh').length ) return false;

	$$('a.recaptcha-refresh').addEvent('click', function(e){
		e.stop();

		var image = this.getParent('div.recaptcha-box').getElements('img.recaptcha-image')[0];

		image.set('src', image.get('src') + '&' + Number.random(1,100000));
	});
}

window.addEvent('domready', function() {

	Cubix.Bubble.Load();

	Sceon.iniSignInBox();
	Sceon.initRecaptcha();

	Sceon.initContactUsLinks();
	Sceon.initAdvertiseLinks();

	if ( $('go-top') ) {

		window.addEvent('resize', function() {
			$('go-top').set('styles', {						
				left: $('container').getCoordinates().right + 1,
			});
		
		});

		new ScrollSpy({
			min: 1,
			max: 100000,
			onEnter: function(position,state,enters) {
				$('go-top').set('styles', {
					display: 'block',
					position: 'fixed',
					bottom: '40px',
					'z-index': '100',
					left: $('container').getCoordinates().right + 1,
					//'z-index': '10'
				});
			},
			onLeave: function(position,state,leaves) {
				$('go-top').set('style', '');
			},
			container: window
		});
		

		$('go-top').addEvent('click', function(e){
			e.stop();
			var myFx = new Fx.Scroll(window,{
				offset: {
					'x': 0,
					'y': -5
				}
			});
			myFx.toElement('top-menu');
		});
	}
	
	/* MARCH 8 POPUP */
	/*if(Cookie.read('disable_march8_popup') != 1 && $('march8-popup')){
		var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.65, color: '#000' });
		page_overlay.disable();

		$('march8-popup').setStyles({
			left: window.getWidth() / 2 - 335,
			top: window.getHeight() / 2 - 247
		});
		$('march8-popup').fade('in');

		$$('#march8-popup .close').addEvent('click', function() {
			$('march8-popup').fade('out');
			page_overlay.enable();
			Cookie.write('disable_march8_popup', 1);
		});
		
		$$('#march8-popup .button').addEvent('click', function(e) {
			e.stop();
			var options = {};
			if($('dont_show_8popup').get('checked')){
				options.duration = 15;
			}
			$('march8-popup').fade('out');
			page_overlay.enable();
			Cookie.write('disable_march8_popup', 1, options);
		});
		
	}*/
	
	
	/*if ( $('signup-234') ) {

		var page_dim = $('container').getCoordinates();
		var ads_dim = $('ads').getCoordinates();

		new ScrollSpy({
			min: ads_dim.height,
			max: 100000,
			onEnter: function(position,state,enters) {
				$('signup-234').set('styles', {
					display: 'block',
					position: 'fixed',
					top: '0px',
					left: page_dim.right + 2,
					'z-index': '100'
				});
			},
			onLeave: function(position,state,leaves) {
				$('signup-234').set('style', '');
			},
			container: window
		});
	}*/

	if ( Sceon.EmailCollectingPopup && Cookie.read('email_collecting') != 'done' && ! Cookie.read('ec_session_closed') && ! headerVars.signedIn && Cookie.read('ec_count') == 3 && ! $('splash') ) {
		Sceon.EmailCollectingPopup.url = '/index/email-collecting-popup'
		Sceon.EmailCollectingPopup.Show();
	}

	if ( Cookie.read('ecr') ) {
		Sceon.EmailCollectingPopup.url = '/index/email-collecting-popup'
		Sceon.EmailCollectingPopup.ShowConfirmed(Cookie.read('ecr'));
	}

	Sceon.initLangPicker();
	Sceon.initFooterLangPicker();
	
	if ( headerVars.currentUser ) {
		setInterval(Sceon.checkForNewPms, 10000);
	}
});