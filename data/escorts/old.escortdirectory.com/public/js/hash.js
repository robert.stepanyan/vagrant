Sceon.Filter = {};

Sceon.Filter.Set = function (filter) {

	/*if ( ! Sceon.Filter.filter ) {
		return;
	}*/

	Sceon.LocationHash.Set(Sceon.LocationHash.Make(filter));

	return false;
}

/* --> LocationHash */
Sceon.LocationHash = {};

Sceon.LocationHash.Set = function (hash) {
	if ( hash.length ) {
		document.location.hash = '#' + hash;
	}
	else {
		document.location.hash = '';
	}
	
	return false;
}

Sceon.LocationHash.AppendEscortPosition = function (hash_string) {
	var hash = document.location.hash.substring(1);

	if ( hash.test(/(segmented\=(.+)\;)/) ) {
		if ( ! hash.test(/(p_id\=(.+)\;)/) ) {
			hash += hash_string;
		} else {
			hash = hash.replace(/(p_id\=(.+)\;)/g, hash_string);
		}

		document.location.hash = hash;
	}

	return false;
}

Sceon.LocationHash.Parse = function () {
	var hash = document.location.hash.substring(1);
	
	if ( ! hash.length ) {
		return {};
	}
	
	var params = hash.split(';');
	
	var filter = '';
	
	var not_array_params = ['p_id', 'currency', 'segmented', 'top_search_input', 'ts_slug', 
	'ts_type', 'page', 'agency_sort', 'sort', 'list_type', 'list_type_a', 'reg', 'name', 'showname', 'agency', 
	'agency_slug', 'city', 'top_name', 'city_slug', 'phone', 'age_from', 'age_to', 'height_from', 'height_to',
	'weight_from', 'weight_to', 'price_from', 'price_to', 'tatoo', 'piercing', 's_currency', 'incall_other', 
	'outcall_other', 's_real_pics', 's_verified_contact', 's_with_video', 's_pornstar', 's_is_online_now', 'sahe'];
	
	params.each(function (param) {
		var key_value = param.split('=');
		
		var key = key_value[0];
		var val = key_value[1];
		
		if ( val === undefined ) return;
		
		val = val.split(',');
				
		val.each(function(it) {
			filter += key + ( ( ! not_array_params.contains(key) ) ? '[]=' : '=' ) + it + '&';
		});
	});
	
	return filter;
}

Sceon.LocationHash.Make = function (params) {
	
	var hash = '';
	var sep = ';';
	var value_glue = ',';

	//var popups = Sceon.Filter.filter.getElements('div.filter-popup');
	var search_box_wrapper = $('search-box-wrapper');

	var search_box = search_box_wrapper.getElement('#search-box');
	if ( search_box_wrapper.getElement('#search-box').hasClass('none') ) {
		var search_box = search_box_wrapper.getElement('#advanced-search-box-v2');
	}

	var sort_box = $$('.sorting-box')[0];
	var agency_sort_box = $$('.agency-sorting-box')[0];
	var list_type = $$('.view-type:not(.view-type.agency)')[0];
	var list_type_a = $$('.view-type.agency')[0];

	var show_all_hidden_escorts = $('show_all_hidden_escorts');

	/*var sorting_select = sort_box.getElements('select[name=sort]')[0];
	var by_showname = sort_box.getElement('#search');*/

	var map = {};
	/*popups.each(function(popup) {
		var inputs = popup.getElements('input:checked');
		inputs.append(popup.getElements('input[type=hidden]'));
		inputs.each(function(input) {
			var index = input.get('name').replace('[]', '');
			if ( map[index] === undefined ) map[index] = new Array();
			map[index].append([input.get('value')]);
		});
	});*/

	if ( search_box ) {

		var hidden_inputs = search_box.getElements('input[type=hidden]');
		hidden_inputs.each(function(input) {
			if ( input.get('name') && input.get('value').length ) {
				var index = input.get('name').replace('[]', '');
				if ( map[index] === undefined ) map[index] = new Array();
				map[index].append([input.get('value')]);
			}
		});

		var inputs = search_box.getElements('input:checked');
		inputs.each(function(input) {
			if ( input.get('value').length > 0 ) {
				var index = input.get('name').replace('[]', '');
				if ( map[index] === undefined ) map[index] = new Array();
				map[index].append([input.get('value')]);
			}
		});

		var input_texts = search_box.getElements('input[type=text]');
		input_texts.each(function(input) {
			if ( input.get('value').length > 0 && input.get('value') != searchVars.search_by_showname ) {
				var index = input.get('name').replace('[]', '');
				if ( map[index] === undefined ) map[index] = new Array();
				map[index].append([input.get('value')]);
			}
		});

		var selects = search_box.getElements('select');

		selects.each(function(input) {
			if ( input.getSelected().get("value")[0] ) {

				var index = input.get('name').replace('[]', '');
				if ( map[index] === undefined ) map[index] = new Array();
				map[index].append([input.getSelected().get("value")[0]]);
			}
		});
	}

	if ( sort_box ) {
		map = Object.merge(map, {sort: [sort_box.getElements('select[name=sort]')[0].getSelected()[0].get('value')]});
		map = Object.merge(map, {currency: [sort_box.getElements('select[name=currency]')[0].getSelected()[0].get('value')]});
	}

	if ( agency_sort_box ) {
		map = Object.merge(map, {agency_sort: [agency_sort_box.getElements('select[name=agency_sort]')[0].getSelected()[0].get('value')]});
		map = Object.merge(map, {currency: [agency_sort_box.getElements('select[name=currency]')[0].getSelected()[0].get('value')]});
	}

	if ( list_type ) {
		map = Object.merge(map, {list_type: [list_type.getElements('input[name=list_type]')[0].get('value')]});
	}

	if ( list_type_a ) {
		map = Object.merge(map, {list_type: [list_type_a.getElements('input[name=list_type_a]')[0].get('value')]});
	}

	if ( show_all_hidden_escorts ) {
		map = Object.merge(map, {sahe: [show_all_hidden_escorts.get('value')]});
	}



	/*if ( by_showname.get('value') != Sceon.Filter.search_input_text ) {
		map = $merge(map, {name: [by_showname.get('value')]});
	}*/



	map = Object.merge(map, params);


	for (var i in map) {
		hash += i + '=' + map[i].join(value_glue) + sep;
	}
	
	return hash;
}
/* <-- */

/* --> HashController */
Sceon.HashController = {
	_current: '',
	noListenOn: ['/links', '/escort/'],
	
	init: function () {	
		setInterval('Sceon.HashController.check()', 100);
	},
	
	check: function () {
		var found = false;
		this.noListenOn.each(function(controller) {
			if ( controller.test(window.location.pathname) && window.location.pathname != "/" ) {
				found = true;
			}
		});
		
		if ( found ) return;
	
		var hash = document.location.hash.substring(1);

		hash = hash.replace(/segmented=(.+)/, "");
		hash = hash.replace(/e_id=(.+)/, "");
		hash = hash.replace(/p_id=(.+)/, "");
		this._current = this._current.replace(/segmented=(.+)/, "");
		this._current = this._current.replace(/e_id=(.+)/, "");
		this._current = this._current.replace(/p_id=(.+)/, "");

		if (hash != this._current) {
			this._current = hash;
			
					
			var data = Sceon.LocationHash.Parse();
			//Sceon.Filter.getForm(data);
			if ( this._current ) {
				Sceon.Escorts.Load(hash, data/*, Sceon.HashController.Callback*/);
			}
		}
	}
};

Sceon.HashController.Callback = function () {
	Sceon.Filter.Change(Sceon.LocationHash.Parse());
	Sceon.Filter.Set(Sceon.LocationHash.Parse(), true);
}

window.addEvent('domready', function(){
	Sceon.HashController.init();
});