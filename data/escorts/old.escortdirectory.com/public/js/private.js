Controls = {};

Controls.SelectBox = new Class({
	Implements: [Options, Events],

	options: {
		disabled: false,
		name: '',
		values: [
			{value: 1, label: 'EUR'},
			{value: 2, label: 'USD'},
			{value: 3, label: 'GBP'},
			{value: 4, label: 'CHF'}
		],
		selected: 1
	},

	element: {},
	list: {},
	input: {},

	selected: {},

	disable: function () {
		this.element.addClass('cx-selectbox-disabled');
	},

	enable: function () {
		this.element.removeClass('cx-selectbox-disabled');
	},

	getValue: function (value) {
		var found = false;
		this.options.values.each(function (i) {
			if ( found ) return;
			if ( value == i.value ) {
				found = i;
			}
		});

		return found;
	},

	initialize: function (o) {
		this.setOptions(o || {});

		this.render();

		this.bind = {
			mouseenter: this.onMouseEnter.bindWithEvent(this),
			mouseleave: this.onMouseLeave.bindWithEvent(this),
			click: this.onClick.bindWithEvent(this),
			select: this.onSelect.bindWithEvent(this),
			hide: this.onHide.bindWithEvent(this)
		};

		this.attach();

		if ( this.options.disabled ) {
			this.disable();
		}
	},

	render: function () {
		this.element = new Element('span', {
			'class': 'cx-selectbox'
		});

		this.current = new Element('em', {
			html: this.getValue(this.options.selected).label
		}).inject(this.element);

		this.list = new Element('div', {
			'class': 'cx-selectbox-list hide'
		}).inject(this.element);

		this.options.values.each(function (i, j) {
			var el = new Element('div', {
				html: i.label
			}).inject(this.list);
			el.store('value', i);
			if ( j == 0 ) {
				el.addClass('selected');
			}
		}.bind(this));

		this.input = new Element('input', {
			type: 'hidden',
			name: this.options.name,
			value: this.getValue(this.options.selected).value
		}).inject(this.element);
	},

	onMouseEnter: function (e) {

	},

	onMouseLeave: function (e) {

	},

	onClick: function (e) {
		if ( this.element.hasClass('cx-selectbox-disabled') ) return;
		e.stop();
		if ( this.list.hasClass('hide') ) {
			this.element.addClass('cx-selectbox-pushed');
			this.list.removeClass('hide');
			$(document.body).addEvent('click', this.bind.hide);
		}
		else {
			this.onHide({});
		}
	},

	onSelect: function (e) {
		this.list.getElements('div').removeClass('selected');
		this.select(e.target);
	},

	onHide: function (e) {
		this.element.removeClass('cx-selectbox-pushed');
		this.list.addClass('hide');
		$(document.body).removeEvent('click', this.bind.hide);
	},

	attach: function () {
		this.element.addEvents({
			mouseenter: this.bind.mouseenter,
			mouseleave: this.bind.mouseleave,
			click: this.bind.click
		});

		this.list.getElements('div').addEvents({
			click: this.bind.select
		});
	},

	select: function (el) {
		var value = el.retrieve('value');
		this.input.set('value', value.value);
		this.current.set('html', value.label);
		this.selected = value;
		el.addClass('selected');
	},

	getElement: function () {
		return this.element;
	},

	privateArea: function () {
		window.location.href = '.';
	}
});

Controls.LngPicker = new Class({
	Implements: [Options, Events],

	options: {
		defaultLang: 'en'
	},

	el: null,
	langs: [],
	defaultLng: null,

	initialize: function (el, options) {
		this.setOptions(options || {});

		this.el = el;
		this.langs = this.el.getElements('span');		

		this.bind = {
			click: this.click.bindWithEvent(this)
		};

		this.attach();

		this.defaultLng = this.langs[0];
		this.langs.each(function(lng){
			if ( this.options.defaultLang == lng.get('cx:lng') ) {
				this.defaultLng = lng;
			}
		}.bind(this));

		this.click({}, this.defaultLng);
	},

	attach: function () {
		this.langs.each(function (lng) {
			lng.addEvent('click', this.bind.click.bindWithEvent(this, [lng]));
		}.bind(this));
	},

	click: function (e, el) {
		this.langs.removeClass('active');
		el.addClass('active');

		this.fireEvent('change', [el]);
	}
});

Popup.editEscortPopup = {};
Popup.editEscortPopup.inProcess = false;
Popup.editEscortPopup.url = '';

Popup.editEscortPopup.Show = function (url) {

	var box_height = 575;
	var box_width = 990;

	if ( Popup.editEscortPopup.inProcess ) return false;

	var page_overlay = new Cubix.Overlay(document.body, {has_loader: false, opacity: 0.5, color: '#000', z_index: 2, overlay_class: 'edit-escort-overlay'});
	page_overlay.disable();

	page_overlay.overlay.addClass('overlay-loader');

	var y_offset = 80;

	var container = new Element('div', {'class': 'escort-popup-wrapper'}).setStyles({
		left: window.getWidth() / 2 - box_width / 2,
		//top:  window.getHeight() / 2 - box_height / 2 + window.getScroll().y - y_offset,
		top:  y_offset,
		opacity: 0
	}).inject(document.body);

	Popup.editEscortPopup.inProcess = true;

	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {

			Popup.editEscortPopup.inProcess = false;
			container.set('html', resp);

			Popup.editEscortPopup.initElements(container);
			Popup.editEscortPopup.initTabs(container, page_overlay);

			var mode = container.getElements('input[name=mode]')[0].get('value');

			var close_btn = new Element('div', {
				html: '',
				'class': 'escort-popup-close-btn-v2'
			}).inject(container);

			close_btn.addEvent('click', function() {
				$$('.escort-popup-wrapper').destroy();
				page_overlay.enable();

				$$('.swiff-uploader-box').destroy();

				//Sceon.reloadAgencyProfileBlock(container.getElements('input[name=escort]').get('value'));
				//Sceon.agencyEscortAction();
			});

			container.getElements('a.cancel').addEvent('click', function(e){
				e.stop();
				$$('.escort-popup-wrapper').destroy();
				page_overlay.enable();

				$$('.swiff-uploader-box').destroy();

				//Sceon.reloadAgencyProfileBlock(container.getElements('input[name=escort]').get('value'));
				//Sceon.agencyEscortAction();
			});

			if ( mode == 'create' ) {
				if ( container.getElements('.btn-back').length ) {
					container.getElements('.btn-back').addEvent('click', function(){
						container.getElements('input[name=then]').set('value', ':back');
					});
				}


				if ( container.getElements('.save-and-next').length ) {
					container.getElements('.save-and-next').addEvent('click', function(){
						//console.log('NEXT FIRST');
						container.getElements('input[name=then]').set('value', ':next');
					});
				}
			}

			if ( container.getElements('.finish').length ) {
				container.getElements('.finish').addEvent('click', function(e){
					e.stop();
					$$('.escort-popup-wrapper').destroy();
					page_overlay.enable();

					$$('.swiff-uploader-box').destroy();

					Sceon.reloadAgencyProfileBlock(container.getElements('input[name=escort]').get('value'));
					Sceon.agencyEscortAction(null, null, null, true);
				});
			}

			container.tween('opacity', '1');
			page_overlay.overlay.removeClass('overlay-loader');

			var forms = container.getElements('form');
			forms.addEvent('submit', Popup.editEscortPopup.Send);
			forms.each(function (form) {form.set('action', form.get('action') + (form.get('action').indexOf('?') != -1 ? '&' : '?') + 'ajax');});
		}
	}).send();


	return false;
}

Popup.editEscortPopup.Send = function (e) {
	e.stop();

	var overlay = new Cubix.Overlay($$('.escort-popup-wrapper')[0], {loader: _st('loader-small.gif'), position: '50%', no_relative: true});
	overlay.disable();

	var container = $$('.escort-popup-wrapper')[0];

	this.get('send').removeEvents('success');
	this.set('send', {
		onSuccess: function (resp) {

			//var resp = JSON.decode(resp);

			if ( resp ) {

				container.set('html', resp);

				var mode = container.getElements('input[name=mode]')[0].get('value');

				if ( mode == 'update' ) {
					if ( container.getElements('input[name=error-status]').length ) {
						var error_status = container.getElements('input[name=error-status]')[0].get('value');

						if ( error_status == 'success' ) {
							var growl = new mooGrowl({
								position: "right"
							});

							growl.notify({
								//title: "this is a critical growl",
								text: "Escort Profile data saved successfully!",
								delay: 0
							});
						}
					}
				}

				Popup.editEscortPopup.initTabs(container);
				Popup.editEscortPopup.initElements(container);

				var close_btn = new Element('div', {
					html: '',
					'class': 'escort-popup-close-btn-v2'
				}).inject(container);

				close_btn.addEvent('click', function() {
					$$('.escort-popup-wrapper').destroy();
					$$('.edit-escort-overlay').destroy();

					$$('.swiff-uploader-box').destroy();

					//Sceon.reloadAgencyProfileBlock(container.getElements('input[name=escort]').get('value'));
					//Sceon.agencyEscortAction();
				});

				if ( mode == 'create' ) {
					if ( container.getElements('.btn-back').length ) {
						container.getElements('.btn-back').addEvent('click', function(){
							container.getElements('input[name=then]').set('value', ':back');
						});
					}


					if ( container.getElements('.save-and-next').length ) {
						container.getElements('.save-and-next').addEvent('click', function(){
							//console.log('NEXT');
							container.getElements('input[name=then]').set('value', ':next');
						});
					}


				}

				container.getElements('a.cancel').addEvent('click', function(e){
					e.stop();
					$$('.escort-popup-wrapper').destroy();
					$$('.edit-escort-overlay').destroy();

					$$('.swiff-uploader-box').destroy();

					//Sceon.reloadAgencyProfileBlock(container.getElements('input[name=escort]').get('value'));
					//Sceon.agencyEscortAction();
				});

				if ( container.getElements('.finish').length ) {
					container.getElements('.finish').addEvent('click', function(e){
						e.stop();
						$$('.escort-popup-wrapper').destroy();
						$$('.edit-escort-overlay').destroy();

						$$('.swiff-uploader-box').destroy();

						Sceon.reloadAgencyProfileBlock(container.getElements('input[name=escort]').get('value'));
						Sceon.agencyEscortAction(null, null, null, true);
					});
				}

				if ( mode == 'update' ) {
					if ( container.getElements('input[name=error-status]').length ) {
						var error_status = container.getElements('input[name=error-status]')[0].get('value');

						if ( error_status == 'success' ) {
							Sceon.reloadAgencyProfileBlock(container.getElements('input[name=escort]').get('value'));
							Sceon.agencyEscortAction(null, null, null, false);
						}
					}
				}

				var forms = container.getElements('form');
				forms.addEvent('submit', Popup.editEscortPopup.Send);
				
				forms.each(function (form) {form.set('action', form.get('action') + (form.get('action').indexOf('?') != -1 ? '&' : '?') + 'ajax');});
				
			} else {
				//console.log(resp);
			}

			overlay.enable();

		}.bind(this)
	});
	
	this.send();
	
}

Popup.editEscortPopup.initialFormData = '';

Popup.editEscortPopup.initTabs = function(container) {
	if ( container.getElements('.escort-tabs')[0].hasClass('edit') ) {

		var cont_overlay = new Cubix.Overlay(container, {loader: _st('loader-small.gif'), position: '50%', no_relative: true});

		container.getElements('.escort-tabs a, .gallery-tabs a, #gallery-tabs .tab').addEvent('click', function(e){
			e.stop();

			if ( Popup.editEscortPopup.initialFormData != container.getElement('form').toQueryString() ) {
				if ( ! confirm('If you continue, all the changes you\'ve made will be lost!\n\nClick "Cancel" to stay on this page and click "SAVE" to save current tab') ) {
					return false;
				}
			}

			cont_overlay.disable();

			Popup.editEscortPopup.inProcess = true;

			new Request({
				url: this.get('href'),
				method: 'get',
				onSuccess: function (resp) {

					Popup.editEscortPopup.inProcess = false;
					container.set('html', resp);

					Popup.editEscortPopup.initTabs(container);
					Popup.editEscortPopup.initElements(container);

					var close_btn = new Element('div', {
						html: '',
						'class': 'escort-popup-close-btn-v2'
					}).inject(container);

					close_btn.addEvent('click', function() {
						$$('.escort-popup-wrapper').destroy();
						$$('.edit-escort-overlay').destroy();

						//Sceon.reloadAgencyProfileBlock(container.getElements('input[name=escort]').get('value'));
						//Sceon.agencyEscortAction();
					});

					container.getElements('a.cancel').addEvent('click', function(e){
						e.stop();
						$$('.escort-popup-wrapper').destroy();
						$$('.edit-escort-overlay').destroy();

						//Sceon.reloadAgencyProfileBlock(container.getElements('input[name=escort]').get('value'));
						//Sceon.agencyEscortAction();
					});

					if ( container.getElements('.finish').length ) {
						container.getElements('.finish').addEvent('click', function(e){
							e.stop();
							$$('.escort-popup-wrapper').destroy();
							$$('.edit-escort-overlay').destroy();

							$$('.swiff-uploader-box').destroy();

							Sceon.reloadAgencyProfileBlock(container.getElements('input[name=escort]').get('value'));
							Sceon.agencyEscortAction(null, null, null, true);
						});
					}

					//container.tween('opacity', '1');

					var forms = container.getElements('form');
					forms.addEvent('submit', Popup.editEscortPopup.Send);
					forms.each(function (form) {form.set('action', form.get('action') + (form.get('action').indexOf('?') != -1 ? '&' : '?') + 'ajax');});
				}
			}).send();
		});
	} else {
		container.getElements('.escort-tabs a').addEvent('click', function(e){
			e.stop();
		});
	}
}

Popup.editEscortPopup.initElements = function(container) {
	
	var elements = container.getElements('select, input[type=radio], input[type=checkbox]');

	var uni_els = new Mooniform(elements);

	if ( container.getElements('.update-mode a.switch-to-create-mode.standart').length ) {
		var switch_to_edit_mode_standart_els = container.getElements('.update-mode a.switch-to-create-mode.standart');

		switch_to_edit_mode_standart_els.addEvent('click', function(e){
			e.stop();

			var update_mode = this.getParent('div.update-mode').addClass('none');
			var create_mode = update_mode.getPrevious('div.create-mode').removeClass('none');

			uni_els.reInit(elements);
		});
	}
	
	// INIT about me text lang picker
	if ( container.getElements('.about-me .lng-picker').length ) {
		var lngPicker = new Controls.LngPicker(container.getElements('.lng-picker')[0], {
			defaultLang:'en',
			onChange: function (el) {
				var lng = el.get('cx:lng');
				container.getElements('.container-about-field').setStyle('display', 'none');
				container.getElement('#container-about-' + lng).setStyle('display', 'block').focus();
			}
		});
	}

	Popup.initEscortLanguages(container);

	Popup.initEscortServices(container);

	if ( container.getElements('a.ico-error').length ) {
		new FloatingTips(container.getElements('a.ico-error'),{
			center: true,
			position: 'top',
			arrowSize: 9
		});
	}
	
	Popup.initEscortWorkingTimes(container, uni_els);

	Popup.initEscortRates(container);

	Popup.initEscortWorkingCities(container, uni_els);

	Popup.editEscortPopup.initialFormData = container.getElement('form').toQueryString();

	Popup.initEscortPhotos(container);

	Popup.initEscortVideos(container);
	
	Popup.initEscortNaturalPic(container);
	Popup.initBio(container, uni_els);
};

Popup.initBio = function(container, uni_els) {
	if ( ! container.getElements('.bio-tab').length ) return;

	var initLocations = function () {
		var remove = function () {
			if ( this.getParent('.location') )
			{
				this.getParent('.location').destroy();
			}
		};

		container.getElements('#countries .icon-remove').addEvent('click', remove);

		var city_id = container.getElement('#block_country').get('value'),
			city_title = container.getElement('#block_country').getSelected().get('html');

		if ( ! city_id ) return;

		var found = false;
		container.getElements('#countries input[type=hidden]').each(function (el) {
			var _city_id = el.get('value');
			if ( city_id == _city_id ) {
				found = true;
				el.set('value', city_id);
				el.getPrevious('label').getElement('span').set('html', city_title);
			}
		});

		var selected_country_count = container.getElements('#countries div.location').length + 1;

		if ( ! found ) {
			var location = new Element('div', {'class': 'location mb5'}).inject(container.getElement('#countries'));

			new Element('a', {
				'class': 'sbtn icon-remove'
			}).inject(new Element('div', {
					'class': 'fleft m0'
			}).inject(location)).addEvent('click', remove);

			var label = new Element('label', {'class': 'fleft'}).inject(location);

			var span = new Element('span', {
				html: city_title
			}).inject(label);

			new Element('input', {
				type: 'hidden',
				name: 'block_countries[]',
				value: city_id
			}).inject(location);

			new Element('div', {'class': 'clear'}).inject(location);
		}

	};

	initLocations();

	container.getElement('#btn-location-add').addEvent('click', function() {
		initLocations();
	});

	var mode = container.getElements('input[name=mode]')[0].get('value');
	var gender_male = 2;
	var elements_to_disable_for_male = ['select[name=dress_size]', 'select[name=cup_size]', 'select[name=breast_type]', 'input[name=bust]', 'input[name=waist]', 'input[name=hip]'];
	container.getElements('input[name=gender]').addEvent('change', function(){
		if ( this.get('value') == gender_male ) {
			elements_to_disable_for_male.each(function(el){
				var el = container.getElements(el);
				el.set('disabled', 'disabled');
				el.set('value', '');
				uni_els.reInit(el);
			});
		} else {
			elements_to_disable_for_male.each(function(el){
				var el = container.getElements(el);
				el.set('disabled', '');
				uni_els.reInit(el);
			});
		}
	});

	var checked_gender = container.getElements('input[name=gender]:checked');
	if ( checked_gender.get('value') == gender_male ) {
		checked_gender.fireEvent('change');
	}
};

Popup.initPhotoSortables = function(container) {

	var sortables = new Sortables($$('.photos ul.public-photos-list'), {
		clone: function (event, element, list) {
			if ( event.event.button ) {this.reset();return new Element('div');}

			var pos = element.getPosition(element.getOffsetParent());
			pos.x += 5;
			pos.y += 5;

			return element.getElement('img').clone(true).setStyles({
				'margin': '0px',
				'position': 'absolute',
				'z-index': 100,
				'visibility': 'hidden',
				'opacity': .7,
				'width': element.getElement('img').getStyle('width')
			}).inject(this.list).position(pos);
		},

		opacity: 1,
		revert: true,
		handle: '.move',

		onStart: function (el) {
			var section = el.getParent('.photos');
			this.mode = section.hasClass('public') ? 'public' :
				section.hasClass('private') ? 'private' : '';
			this.drag.addEvent('enter', function () {
				this.started = true;
			}.bind(this));
		},

		onComplete: function (el) {
			if ( ! this.started ) return;
			else this.started = false;

			save_order(this.mode);
		}
	});

	save_order = function (mode) {
		var ids = [];

		$$('.' + mode + ' li input[type=checkbox]').each(function (el) {
			ids.include(el.get('value'));
		});

		new Request({
			url: '/private-v2/photos',
			method: 'get',
			data: {a: 'sort', photo_id: ids, escort: $('escort_id').get('value')},
			onSuccess: function (resp) {

			}.bind(this)
		}).send();
	};
};

Popup.editEscortPopup.targetTab = '';
Popup.editEscortPopup.tempUrl = '';
Popup.initEscortPhotos = function(container) {
	
	if ( ! container.getElements('.photo-gallery-tab').length ) return;

	$$('.swiff-uploader-box').destroy();

	
	var initRenameGallery = function(){
		var renamePopup = $$('.rename-gallery-popup')[0];
		
		var contentOverlay = new Cubix.Overlay(container, {has_loader: false, opacity: 0.5, color: '#fff', z_index: 201, overlay_class: 'cont-overlay', no_relative: true});
		
		// rename gallery popup
		container.getElements('.rename-gallery').addEvent('click', function(event) {
			event.stop();
			Popup.editEscortPopup.targetTab = event.target;
			Popup.editEscortPopup.tempUrl  = Popup.editEscortPopup.targetTab.get('href');
			var galleryName = event.target.getPrevious('a').get('text');
			$('gallery-name-f').set('value',galleryName);
			contentOverlay.disable();
			renamePopup.removeClass('none');
			new Fx.Tween(renamePopup, {
					property: 'opacity',
					duration: 600
				}).start(0,1);
			
		}.bind(this));
		
		//rename popup Cancel
		renamePopup.getElement('.cancel').addEvent('click', function(){
			new Fx.Tween(renamePopup, {
					property: 'opacity',
					duration: 600,
					onComplete: function() {
						renamePopup.addClass('none');
						contentOverlay.enable();
					}.bind(this)
				}).start(1,0);
				
				
		}.bind(this));
		
		//rename popup OK 
		renamePopup.getElement('.ok').addEvent('click', function(){
			
			var galleryName = $('gallery-name-f').get('value');
						
			new Request({
				url: Popup.editEscortPopup.tempUrl ,
				method: 'get',
				data: {'gallery_name' : galleryName},
				onSuccess: function () {
					
				}
			}).send();
			Popup.editEscortPopup.targetTab.getPrevious('a').set('text',galleryName);
			renamePopup.getElement('.cancel').fireEvent('click');
			
		}.bind(this));
	}
	
	initRenameGallery();
	
	var Cropper = new Class({
		Implements: [Events],

		el: null,
		els: {},
		bind: {},

		disabled: false,

		max: {x: 0, y: 0},
		mouse: {start: {x: 0, y: 0}, now: {x: 0, y: 0}, diff: {x: 0, y: 0}},

		moved: false,

		initialize: function (el) {
			this.el = $(el);
			this.els.img = this.el.getElement('img');
			this.els.img.ondragstart = function () {return false;};

			var initial = JSON.decode(el.get('cubix:initial'));

			this.els.img.onload = function () {
				this.bind = {
					start: this.handleStart.bindWithEvent(this),
					move: this.handleMove.bindWithEvent(this),
					end: this.handleEnd.bindWithEvent(this)
				};

				this.max = {
					x: this.els.img.getWidth() - this.el.getWidth(),
					y: this.els.img.getHeight() - this.el.getHeight()
				};

				this.init();

				if ( initial ) {
					this.setInitial(initial.x, initial.y);
				}
			}.bind(this);
		},

		init: function () {
			this.els.img.addEvent('mousedown', this.bind.start);
		},

		handleStart: function (e) {
			e.stop();

			if ( this.disabled ) return;

			document.addEvent('mousemove', this.bind.move);
			document.addEvent('mouseup', this.bind.end)

			this.mouse.start = e.page;

			this.mouse.start.x -= this.mouse.diff.x;
			this.mouse.start.y -= this.mouse.diff.y;
		},

		handleMove: function (e) {
			this.moved = true;

			this.mouse.now = e.page;

			var x = this.mouse.now.x - this.mouse.start.x,
				y = this.mouse.now.y - this.mouse.start.y;

			this.mouse.diff = {x: x, y: y};

			if ( this.mouse.diff.x > 0 ) this.mouse.diff.x = 0;
			if ( this.mouse.diff.y > 0 ) this.mouse.diff.y = 0;

			if ( -1 * this.mouse.diff.x >= this.max.x ) {
				this.mouse.diff.x = -1 * this.max.x;
			}

			if ( -1 * this.mouse.diff.y >= this.max.y ) {
				this.mouse.diff.y = -1 * this.max.y;
			}

			this.update();
		},

		handleEnd: function (e) {
			document.removeEvent('mousemove', this.bind.move);
			document.removeEvent('mouseup', this.bind.end);

			if ( this.moved ) {
				this.fireEvent('complete', [this.mouse.diff]);
			}
		},

		update: function () {
			this.els.img.setStyles({
				left: this.mouse.diff.x,
				top: this.mouse.diff.y
			});
		},

		enable: function () {
			this.els.img.set('opacity', 1);
			this.disabled = false;
		},

		disable: function () {
			this.els.img.set('opacity', 0.5);
			this.disabled = true;
		},

		revert: function () {
			this.mouse.diff = this.initial;
			this.update();
		},

		initial: {},

		setInitial: function (x, y) {
			this.mouse.diff = {x: x, y: y};
			this.initial = {x: x, y: y}

			this.update();
		}
	});

	var saveAdjustment = function (args) {
		this.disable();

		args = $extend({
			px: args.x / 150.0,
			py: args.y / 200.0
		}, args);

		new Request({
			url: '/private-v2/photos',
			method: 'get',
			data: $extend({a: 'set-adj', photo_id: this.el.get('cubix:photo-id'), escort: $('escort_id').get('value')}, args),
			onSuccess: function (resp) {
				resp = JSON.decode(resp);

				if ( resp.error ) {
					alert('An error occured');
					this.revert();
				}
				else {
					this.setInitial(args.x, args.y);
				}

				this.enable();
			}.bind(this)
		}).send();
	}

	$$('.wrapper').each(function (el) {
		new Cropper(el).addEvent('complete', saveAdjustment);
	});

	Popup.initPhotoSortables(container);

	var initSetMain = function() {
		$$('a.set-main').addEvent('click', function(e){
			e.stop();

			var photo_ids = $$('input[name=photo_id]:checked');

			if ( ! photo_ids.length ) {
				alert('Please select one approved photo to set it main!');
				return false;
			}

			if ( photo_ids.length > 1 ) {
				alert('Please select one approved photo to set it main!');
				return false;
			}

			var page_overlay = new Cubix.Overlay($$('.photos ')[0], {loader: _st('loader-small.gif'), has_loader: true, opacity: 0.5, color: '#fff', z_index: 2});
			page_overlay.disable();
			new Request({
				url: '/private-v2/photos',
				method: 'get',
				data: {a: 'set-main', 'photo_id[]': photo_ids[0].get('value'), escort: $('escort_id').get('value')},
				onSuccess: function (resp) {
					resp = JSON.decode(resp);

					$$('li.main').removeClass('main');
					photo_ids[0].getParent('li').addClass('main');

					page_overlay.enable();

				}.bind(this)
			}).send();
		});
	};

	initSetMain();

	var initRemovePhotos = function() {
		$$('a.remove').addEvent('click', function(e){
			e.stop();

			var photo_ids = $$('input[name=photo_id]:checked');

			if ( ! photo_ids.length ) {
				alert('Please select at least one photo to remove!');
				return false;
			}

			var page_overlay = new Cubix.Overlay($$('.photos ')[0], {loader: _st('loader-small.gif'), has_loader: true, opacity: 0.5, color: '#fff', z_index: 2});
			page_overlay.disable();
			new Request({
				url: '/private-v2/photos',
				method: 'get',
				data: {a: 'delete', 'photo_id': photo_ids.get('value'), escort: $('escort_id').get('value')},
				onSuccess: function (resp) {
					resp = JSON.decode(resp);


					photo_ids.each(function(it){
						it.getParent('li').destroy();
					});

					if ( resp.main_photo_id ) {
						$$('input[name=photo_id]').each(function(it){
							if ( it.get('value') == resp.main_photo_id ) {
								it.getParent('li').addClass('main');
							}
						});
					}

					page_overlay.enable();

				}.bind(this)
			}).send();
		});
	};

	initRemovePhotos();

	var initUpload = function(form, is_private) {

		var myUpload = new MooUpload('filecontrol', {
			action: form.get('action') + '&gallery_id=' + $('gallery-id').get('value'),
			accept: 'image/*',
			method: 'auto',				// Automatic upload method (Choose the best)
			
			onFileUpload: function(fileindex, response){ 
				
				if(response.error == 0 && response.finish){
					
					var public_photos_list = $$('.public-photos-list')[0];
					
					var item = $('filecontrol_file_' + (fileindex));
					itemBox = item.getParent('li');
					itemBox.set('tween', { onComplete: function() {this.element.destroy()}});
					itemBox.tween('opacity', 0);
					
					if ( $$('.no-photos').length ) {
						$$('.no-photos').destroy();
					}

					var container = public_photos_list;
					/*if ( response.photo_type == 3 ) {
						var container = private_photos_list;
					}*/
					
					var li = new Element('li', {'class': (response.is_main) ? 'main' : ''}).inject(container, 'top');

					li.setStyles({opacity: 0, display: 'block'});
					li.tween('opacity', 1);

					var wrapper = new Element('div', {
						'class': 'wrapper',
						'cubix:photo-id': response.photo_id,
						'cubix:initial': (response.args) ? response.args : ''
					}).inject(li, 'top');
					var img = new Element('img', {'src': response.photo_url}).inject(wrapper, 'top');

					var p_actions = new Element('div', {'class': 'p-actions'}).inject(wrapper, 'bottom');
					var move = new Element('span', {'class':'move'}).inject(p_actions, 'top');
					var check = new Element('span', {'class':'check'}).inject(p_actions, 'bottom');
					var input_check = new Element('input', {'type':'checkbox', 'name':'photo_id', 'value': response.photo_id}).inject(check, 'top');

					new Mooniform([input_check]);

					if ( response.is_approved == 0 ) {
						var not_approved = new Element('div', {'class': 'not_approved', 'html': 'not approved'}).inject(wrapper, 'bottom');
					}

					Popup.initPhotoSortables(container);

					$$('.wrapper').each(function (el) {
						new Cropper(el).addEvent('complete', saveAdjustment);
					});
					
					//this.reposition();

				}
			},
			onFileUploadError: function(filenum, response){
				$('filecontrol_file_' + filenum ).set('html',response.error);
			},
			onFinishUpload: function() {			
				
			}
		});
		
		/*var up = new FancyUpload2(form.getElement('.status'), form.getElement('.list'), {
			verbose: false,
			url: form.action,
			path: '/js/fancy/Swiff.Uploader.swf',
			data: "a=upload&is_private=" + is_private + "&PHPSESSID=" + $('sess_name').get('value') + '&gallery_id=' + $('gallery-id').get('value'),
			typeFilter: {
				'Images (*.jpg, *.jpeg, *.gif, *.png)': '*.jpg; *.jpeg; *.gif; *.png'
			},
			appendCookieData: true,
			target: form.getElement('.browse'),
			container: $$('.gallery-tab-inner')[0],
			allowDuplicates: true,
			onLoad: function() {
				form.getElement('.status').removeClass('hide');
				this.target.addEvents({
					click: function() {
						return false;
					},
					mouseenter: function() {
						this.addClass('hover');
					},
					mouseleave: function() {
						this.removeClass('hover');
						this.blur();
					},
					mousedown: function() {
						this.focus();
					}
				});


				form.getElement('.clear-list').addEvent('click', function() {
					up.remove(); // remove all files
					return false;
				});
				form.getElement('.upload').addEvent('click', function() {
					up.start(); // start upload
					return false;
				});
			},

			onSelectFail: function(files) {
				files.each(function(file) {
					new Element('li', {
						'class': 'validation-error',
						html: file.validationErrorMessage || file.validationError,
						title: MooTools.lang.get('FancyUpload', 'removeTitle'),
						events: {
							click: function() {
								this.destroy();
							}
						}
					}).inject(this.list, 'top');
				}, this);
			},

			onFileSuccess: function(file, response) {
				var json = JSON.decode(response, true) || {};

				var public_photos_list = $$('.public-photos-list')[0];
				//var private_photos_list = $$('.private-photos-list')[0];

				if (json.status == '1') {
					count = file.element.getParent('ul').getElements('li.file').length;
					file.info.destroy();
					file.element.destroy();

					if ( $$('.no-photos').length ) {
						$$('.no-photos').destroy();
					}

					var container = public_photos_list;
					if ( json.photo_type == 3 ) {
						var container = private_photos_list;
					}

					var li = new Element('li', {'class': (json.is_main) ? 'main' : ''}).inject(container, 'top');

					li.setStyles({opacity: 0, display: 'block'});
					li.tween('opacity', 1);

					var wrapper = new Element('div', {
						'class': 'wrapper',
						'cubix:photo-id': json.photo_id,
						'cubix:initial': (json.args) ? json.args : '',
					}).inject(li, 'top');
					var img = new Element('img', {'src': json.photo_url}).inject(wrapper, 'top');

					var p_actions = new Element('div', {'class': 'p-actions'}).inject(wrapper, 'bottom');
					var move = new Element('span', {'class':'move'}).inject(p_actions, 'top');
					var check = new Element('span', {'class':'check'}).inject(p_actions, 'bottom');
					var input_check = new Element('input', {'type':'checkbox', 'name':'photo_id', 'value': json.photo_id}).inject(check, 'top');

					new Mooniform([input_check]);

					if ( json.is_approved == 0 ) {
						var not_approved = new Element('div', {'class': 'not_approved', 'html': 'not approved'}).inject(wrapper, 'bottom');
					}

					Popup.initPhotoSortables(container);

					$$('.wrapper').each(function (el) {
						new Cropper(el).addEvent('complete', saveAdjustment);
					});

					this.reposition();

				} else {
					file.element.addClass('file-failed');
					file.element.removeClass('file');
					file.info.set('html', (json.error ? (json.error) : response));

					count = file.element.getParent('ul').getElements('li.file').count;

					if ( ! count ) {
						//window.location = window.location;
					}

					if ( count == 0 ) {
						//window.location = window.location;
					}
				}


			},
			onFail: function(error) {
				if ( error == "flash" ) {
					$$('.swiff-uploader-box').addClass('none');

					new Element('p', {
						'class' : 'flash-error an',
						'html' : 'Please Install <a target="_blank" href="http://get.adobe.com/flashplayer/">Adobe Flash Player</a>'
					}).inject($$('.photo-block-title')[0], 'after');
					
				}
			}
		});*/
	};

		initUpload($('form-public'), 0);
	//initUpload($('form-private'), 1);
};

var ajaxCities = function (container, uni_els) {
	var c = container.getElement('#country').getSelected()[0], ct = container.getElement('#cities');

	var elements = container.getElements('select, input[type=radio], input[type=checkbox]');

	ct.empty();
	container.getElement('#locations').empty();

	//c.set('disabled', 'disabled');
	//ct.set('disabled', 'disabled');
	new Request({
		url: lang_id + '/geography/ajax-get-cities-grouped',
		data: {country_id: c.get('value')},
		onSuccess: function (html) {
			ct.set('html', html);

			new Element('option', {value:'', html:'- - -'}).inject(ct, 'top');
			//c.set('disabled', null);
			//ct.set('disabled', null);

			uni_els.reInit([ct]);
		}
	}).send();
}




var ajaxCitiesZones = function (container,uni_els) {
	var cz = container.getElement('#city-zones');
	var cities = $('locations').getElements('input[type=hidden]');
	var ajax_cities = '';
	if ( cities.length ) {
		cities.each(function(it){
			ajax_cities += ',' + it.get('value');
		});
	}

	new Request({
		url: '/private-v2/profile/get-cityzones?ajax=1&ajax_cities=' + ajax_cities,
		onSuccess: function (data) {
			data = JSON.decode(data);
			if ( ! data.length ) {

             

            	container.getElement('#city-zones').empty();
				new Element('option', {value:'', html:'- - -'}).inject(cz, 'top');
				uni_els.reInit([cz]);
				$('cityzones_wrp').hide();
			}else{
				$('city-zones').empty();
				data.each(function(it){
					new Element('option', {
						value: it.id,
						html: '[' + it.city_title + ']' + ' ' + it.title
					}).inject($('city-zones'));
				});
				if($('incall').get('checked')){
					$('cityzones_wrp').show();
				}
			}
			

			// var selected_zones = $('selected-hidden-z').getElements('input');
			var selected_zones = $('city-zone-data').getElements('input[type=hidden]');
			selected_zones.each(function(it){
				var found = false;

				data.each(function(all_z){
					if ( all_z.id == it.get('value') ) {
						found = true;
					}
				});

				if ( ! found ) {
					it.getParent('.city-zone-item').destroy();
				}
			});

          

		}
	}).send();
	

}

Popup.initEscortVideos = function(container) {
	
	if ( ! container.getElements('.video-gallery-tab').length ) return;
	
	var initVideoUpload = function(form) {

		var myUpload = new MooUpload('filecontrol', {
			action: form.get('action'),
			accept: 'video/*',
			method: 'auto', // Automatic upload method (Choose the best)
			multiple: false,
			onAddFiles: function(){
				var items = $$('#filecontrol_listView .item');
				if(items.length > 1){
					var close = items[0].getElement('.delete');
					close.fireEvent('click', {
						stop: function(){}
					});
				}
			},
			onFileUpload: function(fileindex, response){ 
				
				if(response.error == 0){
					
					var item = $$('#filecontrol_listView .mooupload_readonly')[0];
					item.destroy();
					
					if ( $$('.no-video').length ) {
						$$('.no-video').destroy();
					}
					$$('.video-actions').removeClass('none');
					var container = $('escort_videos');			
					var ul = new Element('ul', {'class': 'main'}).inject(container, 'top');
					var input_hidden = new Element('input', {'type':'hidden', 'id':'video-config', 'value': response.vod}).inject(ul, 'top');
					
					ul.setStyles({opacity: 0, display: 'block'});
					ul.tween('opacity', 1);
					
					var li = new Element('li').inject(ul, 'top');
					var strong = new Element('strong', {'html': 'Pending' }).inject(li, 'top');
					var subUl =  new Element('ul', {'class': 'video'}).inject(li, 'bottom');
					var subLi =   new Element('li' , { 'id': 'video_' + response.video_id, 'class': 'current'}).inject(subUl, 'top');
					
					var img = new Element('img', {'src': response.photo_url, 'oheight' : response.video_height, 'owidth' : response.video_width, 'rel' : response.video_name }).inject(subLi, 'top');
					new Element('span', {'class': 'video_play_btn'}).inject(subLi, 'bottom');
					
					clickVideo();
					initRemoveVideo();
				}
			},
			onFileUploadError: function(filenum, response){
				$$('.mooupload_error').appendText(response.error);
			},
			onFinishUpload: function() {			
				
			}
		});
		
		/*var up = new FancyUpload2(form.getElement('.status'), form.getElement('.list'), {
			verbose: false,
			url: form.action,
			path: '/js/fancy/Swiff.Uploader.swf',
			data: "a=upload&PHPSESSID=" + $('sess_name').get('value'),
			typeFilter: {
				'Videos (*.flv, *.wmv)': '*.flv; *.wmv'
			},
			appendCookieData: true,
			target: form.getElement('.browse'),
			container: $$('.gallery-tab-inner')[0],
			multiple: false,
			fileListMax: 1,
			onLoad: function() {
				form.getElement('.status').removeClass('hide');
				this.target.addEvents({
					click: function() {
						return false;
					},
					mouseenter: function() {
						this.addClass('hover');
					},
					mouseleave: function() {
						this.removeClass('hover');
						this.blur();
					},
					mousedown: function() {
						this.focus();
					}
				});


				form.getElement('.clear-list').addEvent('click', function() {
					up.remove(); // remove all files
					return false;
				});
				form.getElement('.upload').addEvent('click', function() {
					up.start(); // start upload
					return false;
				});
			},

			onSelectFail: function(files) {
				files.each(function(file) {
					new Element('li', {
						'class': 'validation-error',
						html: file.validationErrorMessage || file.validationError,
						title: MooTools.lang.get('FancyUpload', 'removeTitle'),
						events: {
							click: function() {
								this.destroy();
							}
						}
					}).inject(this.list, 'top');
				}, this);
			},

			onFileSuccess: function(file, response) {
				var json = JSON.decode(response, true) || {};

				var container = $('escort_videos');
				
				if (json.status == '1') {
					count = file.element.getParent('ul').getElements('li.file').length;
					file.info.destroy();
					file.element.destroy();

					if ( $$('.no-video').length ) {
						$$('.no-video').destroy();
					}
					$$('.video-actions').removeClass('none');
										
					var ul = new Element('ul', {'class': 'main'}).inject(container, 'top');
					var input_hidden = new Element('input', {'type':'hidden', 'id':'video-config', 'value': json.vod}).inject(ul, 'top');
					
					ul.setStyles({opacity: 0, display: 'block'});
					ul.tween('opacity', 1);
					
					var li = new Element('li').inject(ul, 'top');
					var strong = new Element('strong', {'html': 'Pending' }).inject(li, 'top');
					var subUl =  new Element('ul', {'class': 'video'}).inject(li, 'bottom');
					var subLi =   new Element('li' , { 'id': 'video_' + json.video_id, 'class': 'current'}).inject(subUl, 'top');
					
					var img = new Element('img', {'src': json.photo_url, 'oheight' : json.video_height, 'owidth' : json.video_width, 'rel' : json.video_name }).inject(subLi, 'top');
					new Element('span', {'class': 'video_play_btn'}).inject(subLi, 'bottom');
					
					clickVideo();
					initRemoveVideo();

				} else {
					file.element.addClass('file-failed');
					file.element.removeClass('file');
					file.info.set('html', (json.error ? (json.error) : response));

					count = file.element.getParent('ul').getElements('li.file').count;

					if ( ! count ) {
						//window.location = window.location;
					}

					if ( count == 0 ) {
						//window.location = window.location;
					}
				}


			},
			onFail: function(error) {
				console.log(error);
				if ( error == "flash" ) {
					$$('.swiff-uploader-box').addClass('none');

					new Element('p', {
						'class' : 'flash-error an',
						'html' : 'Please Install <a target="_blank" href="http://get.adobe.com/flashplayer/">Adobe Flash Player</a>'
					}).inject($$('.photo-block-title')[0], 'after');
					
				}
			}
		});*/
	};
	
	initVideoUpload($('form-video'));
	
	var clickVideo = function()
	{
		$$('.video>li').addEvent('click',function(){
			var img = $(this).getChildren('img');
			var width = img.get('owidth')[0];
			var height = img.get('oheight')[0];
			var video = img.get('rel');
			var image = img.get('src')[0].replace('m320','orig');
			video = $('video-config').get('value') + video + '_' + height + 'p.mp4';
			Play(video,image,width,height,true);
		});
	};
	clickVideo();
	
	var Play = function(video,image,width,height,auto)
	{
		var vm = new VideoModal({	
			"hideFooter":true,
			'offsetTop':'20%',
			'width':width,
			'height':height,
			//'closeButton': false,
			'onAppend':function()	
			{	
			jwplayer('video-modal').setup({
				flashplayer: '/jwplayer.flash.swf',
				height:height,
				width:width,
				image:image,
				autostart: auto,
				logo: {
					file: "/img/video/logo.png"
				},
				skin: {
					name: "ed"
				},
				file: video
				/*provider: 'rtmp',
				file:'rtmp://'+video,
				streamer:'rtmp://'+video*/
				});
			}
			});
		vm.addButton("Cancel", "btn");
		vm.show({ 
			"model":"modal", 
			"title":"",
			'contents':'<div id="video-modal"></div>'
		}); 
	}
	
	var initRemoveVideo = function() {
		$$('.video-actions a.remove').addEvent('click', function(e){
			e.stop();

			
			var page_overlay = new Cubix.Overlay($$('.video ')[0], {loader: _st('loader-small.gif'), has_loader: true, opacity: 0.5, color: '#fff', z_index: 2});
			page_overlay.disable();
			new Request({
				url: '/private-v2/video',
				method: 'get',
				data: {a: 'delete', video_id: $$('.video .current')[0].get('id').replace('video_' , ''), escort: $('escort_id').get('value')},
				onSuccess: function (resp) {
					$('escort_videos').getElement('ul.main').destroy();
					$$('.video-actions').addClass('none');
					page_overlay.enable();

				}.bind(this)
			}).send();
		});
	};

	initRemoveVideo();
}


Popup.initEscortNaturalPic = function(container) {
	
	if ( ! container.getElements('.natural-pic-tab').length ) return;
	Popup.NaturalPicOverlay = new Cubix.Overlay($$('.natural-box-wrapper')[0], {loader: _st('loader-small.gif'), has_loader: true, opacity: 0.5, color: '#fff', z_index: 2});
	var initNatUpload = function(form){
		
		var up = new FancyUpload2(form.getElement('.uploader'), form.getElement('.list'), {
			verbose: false,
			url: form.action,
			path: '/js/fancy/Swiff.Uploader.swf',
			data: "a=upload&PHPSESSID=" + $('sess_name').get('value'),
			timeLimit: 120,
			typeFilter: {
				'Images (*.jpg, *.jpeg, *.gif, *.png)': '*.jpg; *.jpeg; *.gif; *.png'
			},
			appendCookieData: true,
			target: form.getElement('.browse'),
			multiple: false,
			fileSizeMax: 15 * 1024 * 1024,
			fileListMax: 1,
			onLoad: function() {
				this.target.addEvents({
					click: function() {
						return false;
					},
					mouseenter: function() {
						this.addClass('hover');
					},
					mouseleave: function() {
						this.removeClass('hover');
						this.blur();
					},
					mousedown: function() {
						this.focus();
					}
				});

				form.getElement('.upload').addEvent('click', function() {
					up.start(); // start upload
					return false;
				});
			},

			onSelectFail: function(files) {
				files.each(function(file) {
					new Element('li', {
						'class': 'validation-error',
						html: file.validationErrorMessage || file.validationError,
						title: MooTools.lang.get('FancyUpload', 'removeTitle'),
						events: {
							click: function() {
								this.destroy();
							}
						}
					}).inject(this.list, 'top');
				}, this);
			},
			onFileStart: function(files){
				/*form.getElement('.current-progress-bar').fade('in');*/
				Popup.NaturalPicOverlay.disable();
			},
			onFileSuccess: function(file, response) {
				var json = JSON.decode(response, true) || {};

				var container = $$('.natural-box')[0];
				
				if (json.status == '1') {
					container.set('html','');					
					var div = new Element('div', {'id': 'nat_' + json.pic_id, 'class': 'wrapper'}).inject(container, 'top');
					var strong = new Element('strong', {'html': 'Pending' }).inject(div, 'top');
					var img = new Element('img', {'src': json.photo_url}).inject(div, 'top');
									
					if(form.getElement('.delete').hasClass('none')){
						initRemovePic();
					}
				} else {
					file.element.addClass('file-failed');
					file.element.removeClass('file');
					file.info.set('html', (json.error ? (json.error) : response));
					count = file.element.getParent('ul').getElements('li.file').count;
				}
			},
			onComplete: function(files) {
				Popup.NaturalPicOverlay.enable();
				setTimeout(function() {
					up.remove();
					/*form.getElement('.current-progress-bar').fade('out');*/
				}, 3000)
				
			},
			onFail: function(error) {
				if ( error == "flash" ) {
					$$('.swiff-uploader-box').addClass('none');

					new Element('p', {
						'class' : 'flash-error an',
						'html' : 'Please Install <a target="_blank" href="http://get.adobe.com/flashplayer/">Adobe Flash Player</a>'
					}).inject($$('.photo-block-title')[0], 'after');
					
				}
			}
		});
	}
	
	var initRemovePic = function() {
		self = this;
		$$('#form-nat-pic .delete').removeClass('none').addEvent('click', function(e){
			e.stop();
			Popup.NaturalPicOverlay.disable();
			var picBox = $$('.natural-box')[0];
			var picWrap = picBox.getElement('.wrapper');
			new Request({
				url: '/private-v2/natural-pic',
				method: 'get',
				data: {'a': 'delete', pic_id: picWrap.get('id').replace('nat_' , '')},
				onSuccess: function (resp) {
					picWrap.destroy();
					new Element('div', {'class' : "no-natural-pic" }).inject(picBox, 'top');
					this.addClass('none');
					this.removeEvents('click');
					Popup.NaturalPicOverlay.enable();
				}.bind(this)
			}).send();
		});
	}
	
	initNatUpload($('form-nat-pic'));
	$('click-highslide').addEvent('click',function(event){
		event.stop();
		hs.expand($$('.highslide')[0]);

	})
	if(!$$('.no-natural-pic')[0]){
		initRemovePic();
	}
}

Popup.initEscortWorkingCities = function(container, uni_els) {
	if ( ! container.getElements('.working-cities-tab').length ) return;

	container.getElement('#country').addEvent('change', function() {
		ajaxCities(container, uni_els);
		ajaxCitiesZones(container,uni_els);
	});

	container.getElement('#cities').addEvent('change', function() {
		ajaxCitiesZones(container,uni_els);
	});


	var max_cities = container.getElement('#max_working_cities').get('value');
	var cities_remain = max_cities - container.getElement('#cities_count').get('value');

	var max_city_zone = 3;
	var city_zone_remain = max_city_zone - container.getElement('#city_zone_count').get('value');

	

	var initCityPicker = function () {
		var els = {
			add: container.getElement('#add-city'),
			remove: container.getElements('#locations .icon-remove'),
			city_id: container.getElements('input[name=city_id]')[0],
			cities: container.getElement('#cities'),
			selected: container.getElement('#selected')
		};


		var initLocations = function () {

			var city_id = els.cities.get('value'),
				city_title = els.cities.getSelected().get('html');

			if ( ! city_id ) return;

			var found = false;
			container.getElements('#locations input[type=hidden]').each(function (el) {
				var _city_id = el.get('value');
				if ( city_id == _city_id ) {
					found = true;
					el.set('value', city_id);
					el.getPrevious('label').getElement('span').set('html', city_title);
				}
			});

			if ( ! found ) {
				var location = new Element('div', {'class': 'location'}).inject(container.getElement('#locations'));

				var city_el = new Element('input', {
					type: 'radio',
					'class': 'base_city_id',
					name: 'city_id',
					value: city_id
				}).inject(new Element('div', {'class': 'fleft'}).inject(location));

				var label = new Element('label', {'class': 'fleft city-title'}).inject(location);

				var span = new Element('span', {
					html: city_title
				}).inject(label);

				var span2 = new Element('span', {
					'class': 'bc_title',
					html: ''
				}).inject(span, 'after');

				new Element('a', {'class': 'icon-remove'}).inject(label, 'bottom').addEvent('click', remove);

				new Element('input', {
					type: 'hidden',
					name: 'cities[]',
					value: city_id
				}).inject(location);

				new Element('div', {'class': 'clear'}).inject(location);
				new Element('div', {'class': 'clear'}).inject(location, 'after');

				uni_els.lookup([city_el]);
			}
		};

		var initBaseCity = function () {
			var bc = container.getElements('input[name=city_id]');

			if ( bc.length > 0 ) {
				bc.addEvent('click', function () {
					
					bc.each(function(it) {
						it.getParent('div.location').removeClass('base_city_selected');
						//it.set('checked', null);
					});

					$$('.bc_title').each(function(it) {
						it.set('html', '');
					});

					this.getParent('div.location').getElements('span.bc_title').set('html', ' - (Base city)');
					this.getParent('div.location').addClass('base_city_selected');
					//this.set('checked', 'checked');
					//uni_els.reInit(bc);
					uni_els.update(bc);
				});
			}
		};

		var clearBaseCity = function () {
			var bc = container.getElements('input[name=city_id]');			
			bc.removeEvents('click');
		};

		var remove = function () {
			if ( this.getParent('.location') ) {
				this.getParent('.location').destroy();

				clearBaseCity();
				initBaseCity();
				updateRemainingCities();
				ajaxCitiesZones(container,uni_els);
				//loadCityzones();
			}
		};

		var updateRemainingCities = function () {
			cities_remain = max_cities - container.getElements('#locations div.location').length ;
			//$('cities-remain').set('html', cities_remain);
		};

		els.remove.addEvent('click', remove);

		els.add.addEvent('click', function() {
			updateRemainingCities();

			if ( cities_remain == 0 ) {
				alert('pv2_cities_limit_exceeded');
				return;
			}

			var selected = els.cities.getSelected();

			if ( selected.length == 0 ) {
				return;
			}

			selected = selected[0];
			initLocations();
			clearBaseCity();
			initBaseCity();
			updateRemainingCities();
			ajaxCitiesZones(container,uni_els);
			//container.getElements('input[name=city_id]').fireEvent('click');

		});

		initBaseCity();

	};

	initCityPicker();

	//City zone
	var initCityZonePicker = function () {
		var els = {
			add: container.getElement('#add-city-zone'),
			remove: container.getElements('#city-zone-data .icon-remove'),
			city_zone_id: container.getElements('input[name=city_zone_id]')[0],
			city_zones: container.getElement('#city-zones'),
			selected: container.getElement('#selected')
		};


		var initCityZones = function () {
			var city_zone_id = els.city_zones.get('value'),
				city_zone_title = els.city_zones.getSelected().get('html');

			if ( ! city_zone_id ) return;


			var found = false;
			container.getElements('#city-zone-data input[type=hidden]').each(function (el) {
				var _city_zone_id = el.get('value');
				if ( city_zone_id == _city_zone_id ) {
					found = true;
					el.set('value', city_zone_id);
					el.getPrevious('label').getElement('span').set('html', city_zone_title);
				}
			});

			if ( ! found ) {
				var cityZones = new Element('div', {'class': 'city-zone-item'}).inject(container.getElement('#city-zone-data'));

				var zone_el = new Element('input', {
					type: 'radio',
					name: 'city_zone_id',
					value: city_zone_id
				}).inject(new Element('div', {'class': 'fleft', 'style': 'display:none'}).inject(cityZones));

				var label = new Element('label', {'class': 'fleft city-title'}).inject(cityZones);

				var span = new Element('span', {
					html: city_zone_title
				}).inject(label);

				// var span2 = new Element('span', {
				// 	'class': 'bc_title',
				// 	html: ''
				// }).inject(span, 'after');

				new Element('a', {'class': 'icon-remove'}).inject(label, 'bottom').addEvent('click', remove);

				new Element('input', {
					type: 'hidden',
					name: 'cityzones[]',
					value: city_zone_id
				}).inject(cityZones);

				new Element('div', {'class': 'clear'}).inject(cityZones);
				new Element('div', {'class': 'clear'}).inject(cityZones, 'after');

				uni_els.lookup([zone_el]);
			}
		};

		// var clearBaseCity = function () {
		// 	var bc = container.getElements('input[name=city_id]');			
		// 	bc.removeEvents('click');
		// };

		var remove = function () {
			if ( this.getParent('.city-zone-item') ) {
				this.getParent('.city-zone-item').destroy();

				// clearBaseCity();
				// initBaseCity();
				updateRemainingCityZone();
				//loadCityzones();
			}
		};

		var updateRemainingCityZone = function () {
			city_zone_remain = max_city_zone - container.getElements('#city-zone-data div.city-zone-item').length ;
		};

		els.remove.addEvent('click', remove);

		els.add.addEvent('click', function() {
			updateRemainingCityZone();

			if ( city_zone_remain == 0 ) {
				alert('pv2_city_zone_limit_exceeded');
				return;
			}

			var selected = els.city_zones.getSelected();

			if ( selected.length == 0 ) {
				return;
			}

			selected = selected[0];
			initCityZones();
			// clearBaseCity();
			// initBaseCity();
			updateRemainingCityZone();
			//container.getElements('input[name=city_id]').fireEvent('click');

		});

		// initBaseCity();

	};

	initCityZonePicker();
	//City zone
	var incallcheckbox=0, outcallcheckbox = 0;

	if ( container.getElements('input.incall_type_hidden').length ) {
		incallcheckbox = 1;
	}
	var first_time_click = 0;
	var handleIncall = function (el) {
		el.blur();
		
		if(first_time_click == 0){
			var firstEl = document.getElementById('private-apartment');
			firstEl.set('checked', 'checked');
		}
		first_time_click++;
		var disable = el.get('checked') ? null : 'disabled';
		var citizones_count = document.getElementById("city-zones").length;
		
		if(el.get('checked') && citizones_count > 1){
			
			incallcheckbox = 1;
			$('cityzones_wrp').show();
		}else{
			incallcheckbox = 0;
			$('cityzones_wrp').hide();
			$$('.city-zone-item').destroy();
		}
	
		
		$(el).getParent('.inner').getParent('.inner').getElements('input').each(function (it) {
			
			if ( it.get('id') == 'incall' ){
				return;
			} 
			
			it.set('disabled', disable);
			if ( disable ) it.addClass('disabled');
			else it.removeClass('disabled');

			uni_els.reInit([it]);
		});
        
		
		
		 disable = container.getElement('#hotel-room').get('checked') ? (container.getElement('#incall').get('checked') ? null : 'disabled') : 'disabled';
		container.getElement('#hotel-room').getParent('.inner').getNext('.options-vertical').getElements('input').set('disabled', disable);
		uni_els.reInit(container.getElement('#hotel-room').getParent('.inner').getNext('.options-vertical').getElements('input'));

		disable = container.getElement('#incall-other').get('checked') && el.get('checked') ? null : 'disabled';
		container.getElement('#incall-other').getParent('.inner').getElements('input[type=text]').set('disabled', disable)[(disable ? 'add' : 'remove') + 'Class']('disabled'); 
	};

	container.getElement('#incall').getParent('.inner').getParent('.inner').getElements('input').each(function (el) {
		if ( ! ['checkbox', 'radio'].contains(el.get('type')) ) return;

		el.addEvent('change', handleIncall.pass(container.getElement('#incall')));
	});

	var handleOutcall = function (el) {
		el.blur();
		var disable = el.get('checked') ? null : 'disabled';

		if(el.get('checked')){
			outcallcheckbox = 1;
		}else{
			outcallcheckbox = 0;
		}

		

		$(el).getParent('.inner').getParent('.inner').getElements('input').each(function (it) {
			if ( it.get('id') == 'outcall' ) return;
			it.set('disabled', disable);
			if ( disable ) it.addClass('disabled');
			else it.removeClass('disabled');

			uni_els.reInit([it]);
		});

		disable = container.getElement('#outcall-other').get('checked') && el.get('checked') ? null : 'disabled';
		container.getElement('#outcall-other').getParent('.inner').getParent('.inner').getElements('input[type=text]').set('disabled', disable)[(disable ? 'add' : 'remove') + 'Class']('disabled');
	};


	container.getElement('#outcall').getParent('.inner').getParent('.inner').getElements('input').each(function (el) {
		if ( ! ['checkbox', 'radio'].contains(el.get('type')) ) return;

		el.addEvent('change', handleOutcall.pass(container.getElement('#outcall')));
	});
};

Popup.initEscortRates = function(container) {

	if ( ! container.getElements('.rates-tab').length ) return;

	var currencies = JSON.decode($('currencies_json').get('html')), c = [];
	for ( var i in currencies ) {
		c.include({value: i, label: currencies[i]});
	}

	$$('.rates .currency').each(function (el) {
		el.empty();
		new Controls.SelectBox({
			values: c,
			selected: $('curr_currency').get('html')
		}).getElement().inject(el);
	});

	$$('.conditions .currency').each(function (el) {
		el.empty();
		new Controls.SelectBox({
			values: c,
			selected: $('curr_currency').get('html')
		}).getElement().inject(el);
	});

	RatesEditor.Units = JSON.decode($('time_units_json').get('html'));
	RatesEditor.Currencies = currencies;
	var travelEditor = new RatesEditor.Travel($$('.rates')[2], $$('.defined-rates ul')[2]);
	var incallEditor = new RatesEditor.Incall($$('.rates')[0], $$('.defined-rates ul')[0]);
	var outcallEditor = new RatesEditor.Outcall($$('.rates')[1], $$('.defined-rates ul')[1]);

	travelEditor.load(JSON.decode($('travel_rates_json').get('html')));
	incallEditor.load(JSON.decode($('incall_rates_json').get('html')));
	outcallEditor.load(JSON.decode($('outcall_rates_json').get('html')));
}

Popup.initEscortWorkingTimes = function(container, uniform_instance) {

	if ( ! container.getElements('.working-times-tab').length ) return;

	var elements = container.getElements('select, input[type=radio], input[type=checkbox]');

	container.getElement('#available-24-7').addEvent('change', function () {
		this.blur();
		var disable = ! this.get('checked') ? null : 'disabled';
		var enable = ! disable;

		container.getElement('#times')[(disable ? 'add' : 'remove') + 'Class']('work-times-disabled').getElements('input, select').set('disabled', disable);
		//$('night_escort').set('disabled', enable);
		//$('n-e')[(enable ? 'add' : 'remove') + 'Class']('work-times-disabled').getElements('input, select').set('disabled', enable);

		if ( ! disable ) {
			container.getElements('.work-times .wt-item input').fireEvent('change');
		}
		uniform_instance.reInit(elements);
	});

	container.getElements('.work-times .wt-item input[name^=day_]').addEvent('change', function () {
		this.blur();
		var disable = this.get('checked') ? null : 'disabled';
		var item = this.getParent('div.wt-item')[(disable ? 'add' : 'remove') + 'Class']('work-times-disabled');
		item.getElements('select').set('disabled', disable);
		item.getElement('input.night_escort').set('disabled', disable);

		if ( disable ) {
			item.getElements('div.from-to').addClass('none');
		} else {
			item.getElements('div.from-to').removeClass('none');
		}

		uniform_instance.reInit(this.getParent('div.wt-item').getElements('select, input[type=radio], input[type=checkbox]'));
	});

	container.getElement('#selectall').addEvent('change', function () {
		this.blur();
		var flag = this.get('checked') ? true : false;
		container.getElements('.work-times .wt-item input[name^=day_]').set('checked', flag ? 'checked' : '').fireEvent('change');

		var item = this.getParent('.work-times');
		if ( flag ) {
			item.getElements('div.from-to').removeClass('none');
		} else {
			item.getElements('div.from-to').addClass('none');
		}

		uniform_instance.reInit(elements);
	});

	container.getElements('.btn-work-days-all').addEvent('click', function (e) {
		e.stop();
		var selects = container.getElement('#times').getElements('select');

		container.getElements('.work-times .wt-item input[name^=day_]').each(function (el) {
			if ( el.get('checked') ) {
				el.getParent('.wt-item').getElements('select').each(function (sel, i) {
					sel.set('value', selects[i].getSelected()[0].get('value')).getParent('.wt-item').highlight('#FFFED2');
				});
			}
		});

		uniform_instance.reInit(elements);
	});

	container.getElements('a.return-from-vacation').addEvent('click', function(e) {
		e.stop();

		new Request({
			url: '/private/ajax-return-from-vacation?escort_id=' + this.get('rel'),
			method: 'get',
			onSuccess: function (resp) {
				container.getElements('select[name ^= vac_date]').set('value',0);
				var myFx = new Fx.Tween(container.getElement('#curr_vacation'), {
					duration: 400,
					onComplete: function() {
						container.getElement('#curr_vacation').setStyle('display', 'none');
					}
				});

				myFx.start('opacity', 1, 0);

				uniform_instance.reInit(elements);
			}
		}).send();
	});

	container.getElement('#available-24-7').fireEvent('change');
};

Popup.initEscortServices = function(container) {

	if ( ! container.getElements('.services-tab').length ) return;

	var currencies = JSON.decode(container.getElement('#currencies').get('html')), c = [];

	for ( var i in currencies ) {
		c.include({value: i, label: currencies[i]});
	}

	container.getElements('table.services input[type=text]').each(function (el) {
		var _el = el.getNext(), data = _el.get('text').split(':'), id = data[0].toInt(), cid = data[1].toInt();
		var select = new Controls.SelectBox({
			values: c,
			name: 'service_currencies[' + id + ']',
			disabled: el.get('disabled'),
			selected: cid
		});
		el.getParent('tr').select = select;
		select.getElement().replaces(_el, 'after')
	});

	container.getElements('table.services input[type=checkbox]').addEvent('change', function () {
		this.blur();
		var chk = this.getParent('tr').getElement('input[type=text]');

		if ( this.get('checked') ) {
			chk.set('disabled', '').focus();
			this.getParent('tr').select.enable();
		}
		else {
			chk.set('disabled', 'disabled');
			this.getParent('tr').select.disable();
		}
	}).fireEvent('change');

	container.getElements('.services input[type=text]').each(function (input) {input.blur();});

	// INIT services tab text lang picker
	if ( container.getElements('.services-tab .lng-picker').length ) {
		var lngPicker = new Controls.LngPicker(container.getElements('.lng-picker')[0], {
			defaultLang:'en',
			onChange: function (el) {
				var lng = el.get('cx:lng');
				$$('.additional-service-field').setStyle('display', 'none');
				$('additional-service-' + lng).setStyle('display', 'block');
			}
		});
	}

	if ( container.getElements('.services-tab .switch-to-create-mode').length ) {
		container.getElements('.services-tab .switch-to-create-mode').addEvent('click', function(e){
			e.stop();

			this.getNext('div.body').getElements('div.create-mode').removeClass('none');
			this.getNext('div.body').getElements('div.update-mode').addClass('none');
			this.destroy();
		});
	}
};

Popup.initEscortLanguages = function(container) {

	if ( ! container.getElements('.languages-tab').length ) return;

	var lng_selectbox = container.getElements('select[name=all_languages]')[0];
	var lng_add_btn = container.getElements('a.btn-add-lng')[0];
	var selected_languages = container.getElements('.selected-languages')[0];


	lng_add_btn.addEvent('click', function(e){
		e.stop();

		var selected = lng_selectbox.getSelected()[0].value;
		var selected_title = lng_selectbox.getSelected()[0].get('html');

		var is_in_list = false;

		if ( selected_languages.getElements('.' + selected).length ) {
			is_in_list = true;
			selected_languages.getElements('.' + selected)[0].highlight();
		}

		if( ! selected || is_in_list ) return;

		var lang_item_class = 'even';

		if ( selected_languages.getElements('.lang-item').length ) {
			if ( selected_languages.getElements('.lang-item')[0].hasClass('even') ) {
				lang_item_class = 'odd';
			}
		}

		var lang_item = new Element('div', {'class': 'lang-item ' + selected + ' ' + lang_item_class}).inject(selected_languages, 'top');
		var language = new Element('div', {'class': 'language'}).inject(lang_item, 'top');
		var lng_img = new Element('img', {'src': '/img/flags/' + selected + '.png'}).inject(language, 'top');
		var lng_title = new Element('span', {'html': selected_title}).inject(language, 'bottom');
		var slider = new Element('div', {'class':'slider'}).inject(lang_item, 'bottom');
		var fill = new Element('div', {'class':'fill'}).inject(slider, 'top');
		var knob = new Element('div', {'class':'knob'}).inject(slider, 'bottom');
		var action = new Element('div', {'class':'action'}).inject(lang_item, 'bottom');
		var lng_remove = new Element('a', {'class':'lng-remove'}).inject(action, 'top');
		var hidden = new Element('input', {
			'type': 'hidden',
			'name': 'langs[' + selected + ']',
			'value': '1',
			'init-value' : '1'
		}).inject(lang_item, 'bottom');
		var clear = new Element('div', {'class': 'clear'}).inject(lang_item, 'bottom');

		lng_remove.addEvent('click', function(e){
			e.stop();

			this.getParent('div.lang-item').destroy();
		});

		new Slider(slider, knob, {
			range: [1, 100],
			snap: false,
			wheel: false,
			offset: 0,
			onChange: function(step){
				this.knob.getParent('div').getElements('div.fill')[0].setStyle('width', this.knob.getStyle('left'));
			},
			onComplete: function(step){
				this.set(step);
				this.knob.getParent('div').getElements('div.fill')[0].setStyle('width', this.knob.getStyle('left'));
				this.knob.getParent('div').getNext('input[type=hidden]').set('value', step);
			}
		}).set(hidden.get('init-value'));
	});

	if ( container.getElements('.lang-item').length ) {
		container.getElements('.lang-item').each(function(it){
			new Slider(it.getElements('.slider')[0], it.getElements('.knob')[0], {
				range: [1, 100],
				snap: false,
				wheel: false,
				offset: 0,
				onChange: function(step){
					this.knob.getParent('div').getElements('div.fill')[0].setStyle('width', this.knob.getStyle('left'));
				},
				onComplete: function(step){
					this.set(step);
					this.knob.getParent('div').getElements('div.fill')[0].setStyle('width', this.knob.getStyle('left'));
					this.knob.getParent('div').getNext('input[type=hidden]').set('value', step);
				}
			}).set(it.getElements('input[type=hidden]').get('init-value'));

			it.getElements('a.lng-remove').addEvent('click', function(e){
				e.stop();

				this.getParent('div.lang-item').destroy();
			});
		});
	}
};

Sceon.initAgencyProfileBlock = function() {

	$$('#dashboard select[name=agency_escorts]').addEvent('change', function() {
		var escort_id = this.getSelected()[0].get('value');

		Sceon.reloadAgencyProfileBlock(escort_id);
	});

	if ( $$('#dashboard select[name=agency_escorts]') ) {
		new Mooniform($$('#dashboard select[name=agency_escorts]'));
	}

	if ( $$('#dashboard .switch-agency select').length ) {
		new Mooniform($$('#dashboard .switch-agency select'));


		$$('#dashboard .switch-agency .btn-switch-agency').addEvent('click', function(e) {
			e.stop();

			if ( $$('#dashboard .switch-agency select')[0].get('value') != 0 ) {
				window.location = "/private/switch-agency?id=" + $$('#dashboard .switch-agency select')[0].get('value');
			} else {
				
			}
		});		
	}
};

Sceon.reloadAgencyProfileBlock = function(agency_escort_id) {

	var container = $$('div.my-profile')[0];

	var page_overlay = new Cubix.Overlay(container, {loader: _st('loader-small.gif'), has_loader: true, opacity: 0.5, color: '#fff', z_index: 2});

	page_overlay.disable();

	new Request({
		url: lang_id + '/dashboard/ajax-load-profile-block?escort_id=' + agency_escort_id,
		method: 'get',
		onSuccess: function(resp){
			container.set('html', resp);

			Sceon.initAgencyProfileBlock();

			page_overlay.enable();
		}
	}).send();
};

Sceon.initOpenableBlocks = function() {
	if ( ! $$('#dashboard li.openable').length ) return;

	$$('#dashboard li.openable').each(function(li){
		li.getNext('li.sub').store('sub_height', li.getNext('li.sub').getDimensions(true).height).setStyle('height', 0);
	});

	$$('#dashboard li.openable').addEvent('click', function(e){
		e.stop();

		var self = this;

		var sub = this.getNext('li.sub');

		if ( this.hasClass('closed') ) {
			new Fx.Tween(sub, {
				duration: 350,
				transition: 'cubic:in:out',
				property: 'height',
				onStart: function() {
					self.removeClass('closed').addClass('open');
					self.getElements('a.btn-open-close').removeClass('closed').addClass('open');
				},
				onComplete: function() {
					sub.setStyle('height', 'auto')
				}
			}).start(0, sub.retrieve('sub_height'));
		} else {
			new Fx.Tween(sub, {
				duration: 350,
				transition: 'cubic:in',
				property: 'height',
				onStart: function() {
					self.removeClass('open').addClass('closed');
					self.getElements('a.btn-open-close').removeClass('open').addClass('closed');
				},
				onComplete: function() {

				}
			}).start(sub.retrieve('sub_height'), 0);
		}
	});
};

Sceon.initChangeWatchedType = function() {
	if ( ! $$('#dashboard .watched-type-form').length ) return;

    var form = $('watched-type-form');

    new Mooniform($$('#watched-type-form select'));

    var overlay = new Cubix.Overlay($$('#dashboard .watched-type-form').getParent('li.sub')[0], {loader: _st('loader-small.gif'), position: '50%', no_relative: false});

    form.getElements('input[type=image]').addEvent('click', function(e){
        e.stop();

        new Request({
            method: form.get('method'),
            url: form.get('action'),
            data: form.toQueryString(),
            onRequest: function() {
                form.getElements('.msg-success').addClass('none');
                overlay.disable();
            },
            onComplete: function(resp) {
                resp = JSON.decode(resp);

                form.getElements('.msg-success').removeClass('none');

                overlay.enable();
            }
        }).send();
    });
};

Sceon.initChangePassword = function() {
	if ( ! $$('#dashboard .change-pass-form').length ) return;

	var form = $('change-pass-form');

	var current_password = form.getElements('input[name=current_pass]');
	var initCurrentPassword = function(pass) {
		pass.addEvent('focus', function() {
			this.destroy();
			injectCurrentPassword();
		});
	};
	var injectCurrentPassword = function() {
		var pass = new Element('input', {
			name: 'current_pass',
			type: 'password',
			'class': 'txt',
			value: ''
		}).inject($('current_pass'), 'top');

		pass.addEvent('blur', function() {
			if ( ! this.get('value').length ) {
				this.destroy();
				var pass = new Element('input', {
					name: 'current_pass',
					type: 'text',
					'class': 'txt',
					value: Sceon.text_enter_current_pass
				});
				pass.inject($('current_pass'), 'top');
				initCurrentPassword(pass);
			}
		});

		pass.focus();
		pass.focus();
		pass.focus();
	};
	initCurrentPassword(current_password);


	var new_password = form.getElements('input[name=new_pass]');
	var initNewPassword = function(pass) {
		pass.addEvent('focus', function() {
			this.destroy();
			injectNewPassword();
		});
	};
	var injectNewPassword = function() {
		var pass = new Element('input', {
			name: 'new_pass',
			type: 'password',
			'class': 'txt',
			value: ''
		}).inject($('new_pass'), 'top');

		pass.addEvent('blur', function() {
			if ( ! this.get('value').length ) {
				this.destroy();
				var pass = new Element('input', {
					name: 'new_pass',
					type: 'text',
					'class': 'txt',
					value: Sceon.text_enter_new_pass
				});
				pass.inject($('new_pass'), 'top');
				initNewPassword(pass);
			}
		});

		pass.focus();
		pass.focus();
		pass.focus();
	};
	initNewPassword(new_password);

	var re_new_password = form.getElements('input[name=repeat_new_pass]');
	var initReNewPassword = function(pass) {
		pass.addEvent('focus', function() {
			this.destroy();
			injectReNewPassword();
		});
	};
	var injectReNewPassword = function() {
		var pass = new Element('input', {
			name: 'repeat_new_pass',
			type: 'password',
			'class': 'txt',
			value: ''
		}).inject($('repeat_new_pass'), 'top');

		pass.addEvent('blur', function() {
			if ( ! this.get('value').length ) {
				this.destroy();
				var pass = new Element('input', {
					name: 'repeat_new_pass',
					type: 'text',
					'class': 'txt',
					value: Sceon.text_reenter_new_pass
				});
				pass.inject($('repeat_new_pass'), 'top');
				initReNewPassword(pass);
			}
		});

		pass.focus();
		pass.focus();
		pass.focus();
	};
	initReNewPassword(re_new_password);

	var overlay = new Cubix.Overlay($$('#dashboard .change-pass-form').getParent('li.sub')[0], {loader: _st('loader-small.gif'), position: '50%', no_relative: false});

	form.getElements('input[type=image]').addEvent('click', function(e){
		e.stop();

		new Request({
			method: form.get('method'),
			url: form.get('action'),
			data: form.toQueryString(),
			onRequest: function() {
				form.getElements('input[type=text]').removeClass('error');
				form.getElements('input[type=password]').removeClass('error');
				form.getElements('.msg-success').addClass('none');
				form.getElements('.msg-error').addClass('none');
				overlay.disable();
			},
			onComplete: function(resp) {
				resp = JSON.decode(resp);

				if ( resp.status == 'error' ) {
					for ( var field in resp.msgs ) {
						var input = form.getElement('*[name=' + field + ']');
						input.addClass('error');
						form.getElements('.msg-error').set('html', resp.msgs[field]).removeClass('none');
					}
				}
				else if ( resp.status == 'success' ) {
					form.getElements('.msg-success').removeClass('none');
				}

				overlay.enable();
			}
		}).send();
	});
};

Sceon.initAgencyEscortsTabs = function() {
	if ( ! $$('#dashboard ul.escort-tabs').length ) return;

	if ( $$('#dashboard select[name=agency_escorts]') ) {
		new Mooniform($$('#dashboard select[name=agency_sort]'));
	}

	var tabs = $$('#dashboard ul.escort-tabs')[0].getElements('li');

	tabs.each(function(tab){
		if ( ! tab.hasClass('active') ) {
			$$('#dashboard div.' + tab.get('title')).addClass('none');
		}
	});

	tabs.addEvent('click', function(e){
		e.stop();

		tabs.removeClass('active');
		this.addClass('active');

		tabs.each(function(tab){
			$$('#dashboard div.' + tab.get('title')).addClass('none');
		});

		$$('#dashboard div.' + this.get('title')).removeClass('none');
	});
};

Sceon.agencyEscortAction = function(action, escort_id, page, use_overlay) {

	if ( ! page ) page = {};

	if ( ! $$('#dashboard .agency-escorts-wrapper').length ) return false;

	var container = $$('#dashboard .agency-escorts-wrapper')[0];

	if ( use_overlay ) {
		var overlay = new Cubix.Overlay(container, {loader: _st('loader-small.gif'), has_loader: true, opacity: 0.5, color: '#fff', z_index: 2});
		overlay.disable();
	}

	new Request({
		url: lang_id + '/dashboard/ajax-get-agency-escorts',
		method: 'post',
		data: Object.merge({
				a : action,
				escort_id: escort_id,
				active_tab: $$('#dashboard ul.escort-tabs')[0].getElements('li.active')[0].get('title'),
				agency_sort: $$('#dashboard select[name=agency_sort]')[0].getSelected()[0].get('value')
			}, page),
		onSuccess: function (resp) {
			container.set('html', resp);
			if ( use_overlay ) {
				overlay.enable();
			}
			Sceon.initAgencyEscortsTabs();
		}.bind(this)
	}).send();

	return false;
};



Popup.CreateSupportTicket = function(pos_el, x_offset) {

	var pos = pos_el.getCoordinates();

	create_support_ticket = new moopopup({
		title: 'Create Ticket',
		width: 415,
		top_left_x: pos.left - x_offset,
		top_left_y: pos.top + 40,
		resizable: false,
		max_body_height: 580,
		draggable: true,
		url: lang_id + '/support/ajax-add-ticket?ajax=1',
		overlay: false,
		onComplete: function() {

			this.container.getElements('.popup-close-btn').addEvent('click', function(){create_support_ticket.close()});
			this.container.getElements('button[type=reset]').addEvent('click', function(e){e.stop();create_support_ticket.close()});

			var container = this.container;

			var overlay = new Cubix.Overlay(container.getElements('.moopopup-body')[0], {color: '#fff', opacity: .6, loader: _st('loader-small.gif'), position: '50%', offset: {bottom: 0, top: 0}});

			tinyMCE.init({
				mode : "textareas",
				theme : "advanced",
				editor_selector : 'issue-ticket',

				onchange_callback : function(inst){
					container.getElement('#message').set('value', inst.getBody().innerHTML);
				},

				plugins: "emotions,advhr,preview,paste",
				theme_advanced_toolbar_location	  : "top",
				theme_advanced_toolbar_align	  : "left",
				theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,undo,redo,|,pastetext,pasteword,emotions,code,preview",
				theme_advanced_buttons2 : "",
				theme_advanced_buttons3_add : "",
				relative_urls : false,
				convert_urls : true,
				forced_root_block : false,
				force_br_newlines : true,
				force_p_newlines : false
			});


			/**
			 * Uploader instance
			 */
			var up = new FancyUpload3.Attach('attach-list', '#attach-file, #attach-file-2', {
				path: '/js/fancy/Swiff.Uploader.swf',
				url: "/support/add-attached-files",
				data: "PHPSESSID=" + Cookie.read('PHPSESSID'),
				fileSizeMax: 2 * 1024 * 1024,
				verbose: false,
				typeFilter: {
					'Files (*.rtf,*.doc,*.txt,*.pdf,*.doc,*.docx,*.rtf,*.ppt,*.pptx,*.xls,*.xlsx,*.jpg, *.jpeg, *.gif, *.png)':'*.rtf;*.doc;*.txt;*.pdf;*.doc;*.docx;*.rtf;*.ppt;*.pptx;*.xls;*.xlsx;*.jpg; *.jpeg; *.gif; *.png'
				},
				onSelectFail: function(files) {
					files.each(function(file) {
						new Element('li', {
							'class': 'file-invalid',
							events: {
								click: function() {
									this.destroy();
								}
							}
						}).adopt(
								new Element('span', {html: file.validationErrorMessage || file.validationError})
							).inject(this.list, 'bottom');
					}, this);
				},

				onFileSuccess: function(file, response) {
					var json = new Hash(JSON.decode(response, true) || {});

					if (json.get('status') == '1') {
						new Element('input', {type: 'checkbox', 'checked': true, name:'attach[]', value:json.get('id')}).inject(file.ui.element, 'top');
						file.ui.element.highlight('#e6efc2');
					}
					else{
						file.ui.element.destroy();
						new Element('li', {
							'class': 'file-invalid',
							events: {
								click: function() {
									this.destroy();
								}
							}
						}).adopt(
								new Element('span', {html: json.error})
							).inject(this.list, 'bottom');
					}
				},

				onFileError: function(file) {
					file.ui.cancel.set('html', 'Retry').removeEvents().addEvent('click', function() {
						file.requeue();
						return false;
					});

					new Element('span', {
						html: file.errorMessage,
						'class': 'file-error'
					}).inject(file.ui.cancel, 'after');
				},

				onFileRequeue: function(file) {
					file.ui.element.getElement('.file-error').destroy();

					file.ui.cancel.set('html', 'Cancel').removeEvents().addEvent('click', function() {
						file.remove();
						return false;
					});

					this.start();
				}

			});


			this.container.getElements('form').addEvent('submit', function(e){
				e.stop();

				overlay.disable();

				this.get('send').removeEvents('success');
				this.set('send', {
					onSuccess: function (resp) {
						resp = JSON.decode(resp);

						overlay.enable();

						container.getElements('input[type=text], textarea').each(function(el){
							el.tween('background-color', '#fff')
						});

						if ( resp.status == 'error' ) {
							for ( var field in resp.msgs ) {
								var input = container.getElement('*[name=' + field + ']');
								//input.addClass('error');
								input.tween('background-color', '#FAF0F4')
							}
						}
						else if ( resp.status == 'success' ) {
							create_support_ticket.close();
						}
					}.bind(this)
				});

				this.send();
			})
		},
		onClose: function() {
			$$('.swiff-uploader-box').destroy();
		}
	});

	create_support_ticket.display();

	return false;
};

Popup.SupportTicketsList = new moopopup({
	title: 'SupportTicketsList',
	width: 475,
	resizable: false,
	max_body_height: 2580,
	draggable: false,
	overlay: true,
	url: lang_id + '/support/ajax-tickets-list?ajax=1',
	onComplete: function() {

		this.container.getElements('.popup-close-btn').addEvent('click', function(){Popup.SupportTicketsList.close()});

		var container = this.container;

		container.getElements('a.subject').addEvent('click', function(e){
			e.stop();

			Popup.SupportTicket(this.get('rel'));
		});
	},
	onClose: function() {

	}
});

Popup.SupportTicket = function(id) {
	var support_ticket = new moopopup({
		title: 'SupportTicket',
		width: 500,
		resizable: false,
		max_body_height: 2580,
		draggable: false,
		overlay: true,
		url: lang_id + '/support/ticket?id=' + id,
		onComplete: function() {

			support_ticket.setFocus();


			this.container.getElements('.popup-close-btn').addEvent('click', function(){support_ticket.close()});
			this.container.getElements('a.cancel').addEvent('click', function(){support_ticket.close()});

			var container = this.container;

			tinyMCE.init({
				mode : "textareas",
				theme : "advanced",

				onchange_callback : function(inst){
					container.getElement('#r-message').set('value', inst.getBody().innerHTML);
				},

				plugins: "emotions,advhr,preview,paste",
				theme_advanced_toolbar_location	  : "top",
				theme_advanced_toolbar_align	  : "left",
				editor_selector : 'issue-ticket',
				theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,undo,redo,|,pastetext,pasteword,emotions,code,preview",
				theme_advanced_buttons2 : "",
				theme_advanced_buttons3_add : "",
				relative_urls : false,
				convert_urls : true,
				forced_root_block : false,
				force_br_newlines : true,
				force_p_newlines : false
			});

			/**
			 * Uploader instance
			 */
			if($defined($('attach-box'))){
				var up = new FancyUpload3.Attach('attach-list', '#attach-file, #attach-file-2', {
					path: '/js/fancy/Swiff.Uploader.swf',
					url: "/support/add-attached-files",
					data: "PHPSESSID=" + Cookie.read('PHPSESSID'),
					fileSizeMax: 2 * 1024 * 1024,
					verbose: false,
					typeFilter: {
						'Files (*.rtf,*.doc,*.txt,*.pdf,*.doc,*.docx,*.rtf,*.ppt,*.pptx,*.xls,*.xlsx,*.jpg, *.jpeg, *.gif, *.png)':'*.rtf;*.doc;*.txt;*.pdf;*.doc;*.docx;*.rtf;*.ppt;*.pptx;*.xls;*.xlsx;*.jpg; *.jpeg; *.gif; *.png'
					},
					onSelectFail: function(files) {
						files.each(function(file) {
							new Element('li', {
								'class': 'file-invalid',
								events: {
									click: function() {
										this.destroy();
									}
								}
							}).adopt(
									new Element('span', {html: file.validationErrorMessage || file.validationError})
								).inject(this.list, 'bottom');
						}, this);
					},

					onFileSuccess: function(file, response) {
						var json = new Hash(JSON.decode(response, true) || {});

						if (json.get('status') == '1') {
							new Element('input', {type: 'checkbox', 'checked': true, name:'attach[]', value:json.get('id')}).inject(file.ui.element, 'top');
							file.ui.element.highlight('#e6efc2');
						}
						else{
							file.ui.element.destroy();
							new Element('li', {
								'class': 'file-invalid',
								events: {
									click: function() {
										this.destroy();
									}
								}
							}).adopt(
									new Element('span', {html: json.error})
								).inject(this.list, 'bottom');
						}
					},

					onFileError: function(file) {
						file.ui.cancel.set('html', 'Retry').removeEvents().addEvent('click', function() {
							file.requeue();
							return false;
						});

						new Element('span', {
							html: file.errorMessage,
							'class': 'file-error'
						}).inject(file.ui.cancel, 'after');
					},

					onFileRequeue: function(file) {
						file.ui.element.getElement('.file-error').destroy();

						file.ui.cancel.set('html', 'Cancel').removeEvents().addEvent('click', function() {
							file.remove();
							return false;
						});

						this.start();
					}
				});
			}

		},
		onClose: function() {
			$$('.swiff-uploader-box').destroy();
		}
	});

	support_ticket.display();

	return false;
};



Popup.initAgencyLogo = function(container) {

	if ( ! container.getElements('.AgencyLogo').length ) return;



	$$('.swiff-uploader-box').destroy();

	var Cropper = new Class({
		Implements: [Events],

		el: null,
		els: {},
		bind: {},

		disabled: false,

		max: {x: 0, y: 0},
		mouse: {start: {x: 0, y: 0}, now: {x: 0, y: 0}, diff: {x: 0, y: 0}},

		moved: false,

		initialize: function (el) {
			this.el = $(el);
			this.els.img = this.el.getElement('img');
			this.els.img.ondragstart = function () {return false;};

			var initial = JSON.decode(el.get('cubix:initial'));

			var preventCache = Number.random(1,100000);
			this.els.img.set('src', this.els.img.get('src') + '#' + preventCache);

			this.els.img.onload = function () {
				this.bind = {
					start: this.handleStart.bindWithEvent(this),
					move: this.handleMove.bindWithEvent(this),
					end: this.handleEnd.bindWithEvent(this)
				};

				this.max = {
					x: this.els.img.getWidth() - this.el.getWidth(),
					y: this.els.img.getHeight() - this.el.getHeight()
				};				

				this.init();

				if ( initial ) {
					this.setInitial(initial.x, initial.y);
				}
			}.bind(this);
		},

		init: function () {
			this.els.img.addEvent('mousedown', this.bind.start);
		},

		handleStart: function (e) {
			e.stop();

			if ( this.disabled ) return;

			document.addEvent('mousemove', this.bind.move);
			document.addEvent('mouseup', this.bind.end)

			this.mouse.start = e.page;

			this.mouse.start.x -= this.mouse.diff.x;
			this.mouse.start.y -= this.mouse.diff.y;
		},

		handleMove: function (e) {
			this.moved = true;

			this.mouse.now = e.page;

			var x = this.mouse.now.x - this.mouse.start.x,
				y = this.mouse.now.y - this.mouse.start.y;

			this.mouse.diff = {x: x, y: y};

			if ( this.mouse.diff.x > 0 ) this.mouse.diff.x = 0;
			if ( this.mouse.diff.y > 0 ) this.mouse.diff.y = 0;

			if ( -1 * this.mouse.diff.x >= this.max.x ) {
				this.mouse.diff.x = -1 * this.max.x;
			}

			if ( -1 * this.mouse.diff.y >= this.max.y ) {
				this.mouse.diff.y = -1 * this.max.y;
			}

			this.update();
		},

		handleEnd: function (e) {
			document.removeEvent('mousemove', this.bind.move);
			document.removeEvent('mouseup', this.bind.end);

			if ( this.moved ) {
				this.fireEvent('complete', [this.mouse.diff]);
			}
		},

		update: function () {
			this.els.img.setStyles({
				left: this.mouse.diff.x,
				top: this.mouse.diff.y
			});
		},

		enable: function () {
			this.els.img.set('opacity', 1);
			this.disabled = false;
		},

		disable: function () {
			this.els.img.set('opacity', 0.5);
			this.disabled = true;
		},

		revert: function () {
			this.mouse.diff = this.initial;
			this.update();
		},

		initial: {},

		setInitial: function (x, y) {
			this.mouse.diff = {x: x, y: y};
			this.initial = {x: x, y: y}

			this.update();
		}
	});

	var saveAdjustment = function (args) {
		this.disable();

		args = $extend({
			px: args.x / 150.0,
			py: args.y / 200.0
		}, args);

		new Request({
			url: '/private-v2/agency-logo',
			method: 'get',
			data: $extend({a: 'set-adj', /*photo_id: this.el.get('cubix:photo-id'),*/ agency: container.getElement('#agency_id').get('value')}, args),
			onSuccess: function (resp) {
				resp = JSON.decode(resp);

				if ( resp.error ) {
					alert('An error occured');
					this.revert();
				}
				else {
					this.setInitial(args.x, args.y);
				}

				this.enable();
			}.bind(this)
		}).send();
	}

	container.getElements('.wrapper').each(function (el) {
		new Cropper(el).removeEvents('complete').addEvent('complete', saveAdjustment);
	});	

	var initRemovePhotos = function() {
		if ( ! container.getElements('a.remove').length ) return false;

		container.getElements('a.remove').removeEvents('click');
		container.getElements('a.remove').addEvent('click', function(e){
			e.stop();			

			var page_overlay = new Cubix.Overlay($$('.photos ')[0], {loader: _st('loader-small.gif'), has_loader: true, opacity: 0.5, color: '#fff', z_index: 2});
			page_overlay.disable();
			new Request({
				url: '/private-v2/agency-logo',
				method: 'get',
				data: {a: 'delete', agency: container.getElement('#agency_id').get('value')},
				onSuccess: function (resp) {					

					container.getElements('.public-photos-list').set('html', '');
					container.getElements('.photo-actions').addClass('none'); 

					page_overlay.enable();

				}.bind(this)
			}).send();
		});
	};

	initRemovePhotos();

	var initUpload = function(form) {


		var up = new FancyUpload2(form.getElement('.status'), form.getElement('.list'), {
			verbose: false,
			multiple: false,
			//limitFiles: 1,
			url: form.get('action'),
			path: '/js/fancy/Swiff.Uploader.swf',
			data: "a=upload&PHPSESSID=" + container.getElement('#sess_name').get('value'),
			typeFilter: {
				'Images (*.jpg, *.jpeg, *.gif, *.png)': '*.jpg; *.jpeg; *.gif; *.png'
			},
			appendCookieData: true,
			target: form.getElement('.browse'),
			container: $$('.gallery-tab-inner')[0],
			allowDuplicates: true,
			onLoad: function() {
				form.getElement('.status').removeClass('hide');
				this.target.addEvents({
					click: function() {
						return false;
					},
					mouseenter: function() {
						this.addClass('hover');
					},
					mouseleave: function() {
						this.removeClass('hover');
						this.blur();
					},
					mousedown: function() {
						this.focus();
					}
				});


				form.getElement('.clear-list').addEvent('click', function() {
					up.remove(); // remove all files
					return false;
				});
				form.getElement('.upload').addEvent('click', function() {
					up.start(); // start upload
					return false;
				});
			},

			onSelectFail: function(files) {
				files.each(function(file) {
					new Element('li', {
						'class': 'validation-error',
						html: file.validationErrorMessage || file.validationError,
						title: MooTools.lang.get('FancyUpload', 'removeTitle'),
						events: {
							click: function() {
								this.destroy();
							}
						}
					}).inject(this.list, 'top');
				}, this);
			},

			onFileSuccess: function(file, response) {
				var json = JSON.decode(response, true) || {};

				var public_photos_list = $$('.public-photos-list')[0];
				//var private_photos_list = $$('.private-photos-list')[0];

				if (json.status == '1') {
					count = file.element.getParent('ul').getElements('li.file').length;
					file.info.destroy();
					file.element.destroy();

					if ( $$('.no-photos').length ) {
						$$('.no-photos').destroy();
					}

					$$('.photo-actions').removeClass('none');

					var container = public_photos_list;

					container.set('html', '');

					var li = new Element('li').inject(container, 'top');

					/*li.setStyles({opacity: 0, display: 'block'});
					li.tween('opacity', 1);*/

					var wrapper = new Element('div', {
						'class': 'wrapper',
						'cubix:photo-id': json.photo_id,
						'cubix:initial': (json.args) ? json.args : '',
					}).inject(li, 'top');
					var img = new Element('img', {'src': json.photo_url}).inject(wrapper, 'top');
					

					/*var p_actions = new Element('div', {'class': 'p-actions'}).inject(wrapper, 'bottom');
					var move = new Element('span', {'class':'move'}).inject(p_actions, 'top');
					var check = new Element('span', {'class':'check'}).inject(p_actions, 'bottom');
					var input_check = new Element('input', {'type':'checkbox', 'name':'photo_id', 'value': json.photo_id}).inject(check, 'top');*/

					//new Mooniform([input_check]);

					/*if ( json.is_approved == 0 ) {
						var not_approved = new Element('div', {'class': 'not_approved', 'html': 'not approved'}).inject(wrapper, 'bottom');
					}*/

					//Popup.initPhotoSortables(container);

					container.getElements('.wrapper').each(function (el) {						
						new Cropper(el).removeEvents('complete').addEvent('complete', saveAdjustment);
					});

					//this.reposition();

				} else {
					file.element.addClass('file-failed');
					file.element.removeClass('file');
					file.info.set('html', (json.error ? (json.error) : response));

					count = file.element.getParent('ul').getElements('li.file').count;

					if ( ! count ) {
						//window.location = window.location;
					}

					if ( count == 0 ) {
						//window.location = window.location;
					}
				}


			},
			onFail: function(error) {
				if ( error == "flash" ) {
					$$('.swiff-uploader-box').addClass('none');

					new Element('p', {
						'class' : 'flash-error an',
						'html' : 'Please Install <a target="_blank" href="http://get.adobe.com/flashplayer/">Adobe Flash Player</a>'
					}).inject($$('.photo-block-title')[0], 'after');
					
				}
			}
		});
	};

	initUpload(container.getElement('#form-public'));
	//initUpload($('form-private'), 1);
};


Popup.EditAgencyProfile = function(pos_el, x_offset) {

	var pos = pos_el.getCoordinates();

	edit_agency_profile = new moopopup({
		title: 'Edit Agency Profile',
		width: 750,
		//top_left_x: pos.left - x_offset,
		//top_left_y: pos.top - 250,
		resizable: false,
		max_body_height: 5800,
		draggable: false,
		url: lang_id + '/private-v2/agency-profile?ajax=1',
		overlay: true,
		onComplete: function() {

			this.container.getElements('.popup-close-btn').addEvent('click', function(){edit_agency_profile.close()});
			this.container.getElements('a.cancel').addEvent('click', function(){edit_agency_profile.close()});

			var container = this.container;

			//Popup.initAgencyLogo(container);

			var tabs = container.getElements('ul.tabs li');
			var tabs_a = container.getElements('ul.tabs li a');
			tabs.each(function(tab){
				if ( ! tab.hasClass('active') ) {
					container.getElements('.' + tab.get('title')).addClass('none');
				}
			});

			Popup.initAgencyLogo(container);

			//var click_count = 1;
			
			tabs_a.addEvent('click', function(e){
				e.stop();

				container.getElements('ul.tabs li.active').removeClass('active');
				this.getParent('li').addClass('active');
				container.getElements('.' + this.getParent('li').get('title')).removeClass('none');
				tabs.each(function(tab){
					if ( ! tab.hasClass('active') ) {
						container.getElements('.' + tab.get('title')).addClass('none');
					}
				});

				$$('.progress-text').destroy();
				/*if (this.getParent('li').get('title') == 'AgencyLogo' ) {
					if ( click_count == 1 ) {
						Popup.initAgencyLogo(container);						
					}
					$$('.progress-text').destroy();
					click_count++;					
				}*/
				
			});

			var overlay = new Cubix.Overlay(container.getElements('.moopopup-body')[0], {color: '#fff', opacity: .6, loader: _st('loader-small.gif'), position: '50%', offset: {bottom: 0, top: 0}});

			new Mooniform(container.getElements('.contact-deatils select, .contact-deatils input[type=checkbox]'));

			// INIT about me text lang picker
			if ( container.getElements('.lng-picker').length ) {
				var lngPicker = new Controls.LngPicker(container.getElements('.lng-picker')[0], {
					defaultLang:'en',
					onChange: function (el) {
						var lng = el.get('cx:lng');
						container.getElements('.container-about-field').setStyle('display', 'none');
						container.getElement('#container-about-' + lng).setStyle('display', 'block').focus();
					}
				});
			}

			/*var link = $('select-0');
			var linkIdle = link.get('html');

			function linkUpdate() {
				if (!swf.uploading) return;
				var size = Swiff.Uploader.formatUnit(swf.size, 'b');
				link.set('html', '<span class="small">' + swf.percentLoaded + '% of ' + size + '</span>');
			}*/

			

			// Uploader instance
			/*var swf = new Swiff.Uploader({
				path: '/js/fancy/Swiff.Uploader.swf',
				url: '/private-v2/agency-logo',
				verbose: false,
				queued: false,
				multiple: false,
				target: link,
				instantStart: true,
				data: "PHPSESSID=" + Cookie.read('PHPSESSID'),
				typeFilter: {
					'Images (*.jpg, *.jpeg, *.gif, *.png)': '*.jpg; *.jpeg; *.gif; *.png'
				},
				fileSizeMax: 2 * 1024 * 1024,
				onSelectSuccess: function(files) {
					if (Browser.Platform.linux) window.alert('Warning: Due to a misbehaviour of Adobe Flash Player on Linux,\nthe browser will probably freeze during the upload process.\nSince you are prepared now, the upload will start right away ...');
					//log.alert('Starting Upload', 'Uploading <em>' + files[0].name + '</em> (' + Swiff.Uploader.formatUnit(files[0].size, 'b') + ')');
					this.setEnabled(false);
				},
				onSelectFail: function(files) {
					//log.alert('<em>' + files[0].name + '</em> was not added!', 'Please select an image smaller than 2 Mb. (Error: #' + files[0].validationError + ')');
				},
				//appendCookieData: true,
				onQueue: linkUpdate,
				onFileComplete: function(file) {

					// We *don't* save the uploaded images, we only take the md5 value and create a monsterid ;)
					if (file.response.error) {
						//log.alert('Failed Upload', 'Uploading <em>' + this.fileList[0].name + '</em> failed, please try again. (Error: #' + this.fileList[0].response.code + ' ' + this.fileList[0].response.error + ')');
					} else {
						var url = file.response.text;

						var img = $('demo-portrait');
						img.set('src', url);
						img.highlight();
					}

					file.remove();
					this.setEnabled(true);
				},
				onComplete: function() {
					link.set('html', linkIdle);
				}
			});*/

			this.container.getElements('form').addEvent('submit', function(e){
				e.stop();

				overlay.disable();

				this.get('send').removeEvents('success');
				this.set('send', {
					onSuccess: function (resp) {
						resp = JSON.decode(resp);

						overlay.enable();

						container.getElements('input[type=text], textarea').each(function(el){
							el.removeClass('error');
						});

						if ( resp.status == 'error' ) {
							for ( var field in resp.msgs ) {
								container.getElement('*[name=' + field + ']').addClass('error');
							}
						}
						else if ( resp.status == 'success' ) {
							var growl = new mooGrowl({
								position: "right"
							});

							growl.notify({
								//title: "this is a critical growl",
								text: "Agency Profile data saved successfully!",
								delay: 0
							});
						}
					}.bind(this)
				});

				this.send();
			})
		},
		onClose: function() {
			$$('.swiff-uploader-box').destroy();
		}
	});

	edit_agency_profile.display();

	return false;
};



InitVerificationPhotos = function(container, escort_id){
	$$('.swiff-uploader-box').destroy();

	container.getElements('.progress-text').destroy();

	var form = container.getElements('form')[0];

	var up = new FancyUpload2(form.getElement('.status'), form.getElement('.list'), {
		verbose: false,
		url: '/verify/upload-photos',
		limitFiles: 4,
		instantStart: true,
		path: '/js/fancy/Swiff.Uploader.swf',
		data: "escort_id=" + escort_id + "&PHPSESSID=" + Cookie.read('PHPSESSID'),
		typeFilter: {
			'Images (*.jpg, *.jpeg, *.gif, *.png)': '*.jpg; *.jpeg; *.gif; *.png'
		},
		appendCookieData: true,
		target: form.getElement('.browse'),
		container: $$('.gallery-tab-inner')[0],
		allowDuplicates: true,
		onLoad: function() {
			form.getElement('.status').removeClass('hide');
			this.target.addEvents({
				click: function() {
					return false;
				},
				mouseenter: function() {
					this.addClass('hover');
				},
				mouseleave: function() {
					this.removeClass('hover');
					this.blur();
				},
				mousedown: function() {
					this.focus();
				}
			});


			form.getElement('.clear-list').addEvent('click', function() {
				up.remove(); // remove all files
				return false;
			});
			form.getElement('.upload').addEvent('click', function() {
				up.start(); // start upload
				return false;
			});
		},

		onSelectFail: function(files) {
			files.each(function(file) {
				new Element('li', {
					'class': 'validation-error',
					html: file.validationErrorMessage || file.validationError,
					title: MooTools.lang.get('FancyUpload', 'removeTitle'),
					events: {
						click: function() {
							this.destroy();
						}
					}
				}).inject(this.list, 'top');
			}, this);
		},

		onFileSuccess: function(file, response) {
			var json = JSON.decode(response, true) || {};

			if (json.status == '1') {

				file.info.destroy();
				file.element.destroy();

				var images_wrapper = $('verify_images_wrapper');

				var item = new Element('div', {'class':'item'}).inject(images_wrapper, 'top');

				var img = new Element('img', {'src': json.photo_url}).inject(item, 'top');
				var hash = new Element('input', {'type' : 'hidden', 'name': 'hash[]', 'value' : json.hash}).inject(item, 'bottom');
				var ext = new Element('input', {'type' : 'hidden', 'name': 'ext[' + json.hash + '][]', 'value' : json.ext}).inject(item, 'bottom');
				var comment = new Element('input', {'type' : 'text', 'class' : 'txt', 'name': 'comment[' + json.hash + '][]', 'value' : ''}).inject(item, 'bottom');

				this.reposition();

			} else {
				file.element.addClass('file-failed');
				file.element.removeClass('file');
				file.info.set('html', (json.error ? (json.error) : response));
			}
		},
		onFail: function(error) {

		}
	});
};

Popup.Verification = new moopopup({
	title: 'Contact Us',
	width: 520,
	resizable: false,
	max_body_height: 2500,
	draggable: false,
	url: lang_id + '/verify/idcard?ajax=1',
	onComplete: function() {
		this.container.getElements('.popup-close-btn').addEvent('click', function(){Popup.Verification.close()});
		this.container.getElements('a.cancel').addEvent('click', function(e){e.stop();Popup.Verification.close()});

		if ( this.container.getElements('a.close').length ) {
			this.container.getElements('a.close').addEvent('click', function(e){e.stop();Popup.Verification.close()});
		}

		var container = this.container;

		var form = container.getElements('form')[0];

		var uni_els = new Mooniform(container.getElements('select'));

		var overlay = new Cubix.Overlay(container.getElements('.moopopup-body')[0], {color: '#fff', opacity: .6, loader: _st('loader-small.gif'), position: '50%', offset: {bottom: 0, top: 0}});

		var images_wrapper = container.getElement('#verify_images_wrapper');

		if ( container.getElements('input[name=i_escort_id]').length ) {
			InitVerificationPhotos(container, container.getElements('input[name=i_escort_id]')[0].get('value'));
		} else {

			InitVerificationPhotos(container, container.getElement('#escort_id').getSelected()[0].get('value'));

			container.getElement('#escort_id').addEvent('change', function(){
				InitVerificationPhotos(container, this.getSelected()[0].get('value'));
				images_wrapper.set('html', '');
			});
		}


		this.container.getElements('form').addEvent('submit', function(e){
			e.stop();

			overlay.disable();

			this.get('send').removeEvents('success');
			this.set('send', {
				onSuccess: function (resp) {
					resp = JSON.decode(resp);

					overlay.enable();

					container.getElements('.error').set('html', '');

					if ( resp.status == 'error' ) {

						for ( var field in resp.msgs ) {
							container.getElements('.error.' + field).set('html', '(' + resp.msgs[field] + ')');
						}
					}
					else if ( resp.status == 'success' ) {

						Popup.Verification.close();

						var successPopup = new moopopup({
							title: 'Verification Success',
							width: 520,
							resizable: false,
							max_body_height: 580,
							draggable: false,
							url: lang_id + '/verify/success?ajax=1',
							onComplete: function() {
								this.container.getElements('.popup-close-btn').addEvent('click', function(){successPopup.close()});
								this.container.getElements('a.close').addEvent('click', function(e){e.stop();successPopup.close()});
							},
							onClose: function() {

							}
						});
						successPopup.display();
					}
				}.bind(this)
			});

			this.send();
		})
	},
	onClose: function() {
		$$('.swiff-uploader-box').destroy();
	}
});



Popup.escortToursPopup = {};
Popup.escortToursPopup.inProcess = false;
Popup.escortToursPopup.url = '';

Popup.DeleteTours;
Popup.escortTourEdit;
Popup.CloseTourEditPopup;
Popup.SaveTour;

Popup.escortToursPopup.Show = function (url) {

	var box_height = 575;
	var box_width = 990;

	if ( Popup.escortToursPopup.inProcess ) return false;

	var page_overlay = new Cubix.Overlay(document.body, {has_loader: false, opacity: 0.5, color: '#000', z_index: 2, overlay_class: 'edit-escort-overlay'});
	page_overlay.disable();

	page_overlay.overlay.addClass('overlay-loader');

	var y_offset = 80;

	var container = new Element('div', {'class': 'escort-popup-wrapper'}).setStyles({
		left: window.getWidth() / 2 - box_width / 2,
		//top:  window.getHeight() / 2 - box_height / 2 + window.getScroll().y - y_offset,
		top:  y_offset,
		opacity: 0
	}).inject(document.body);

	Popup.escortToursPopup.inProcess = true;

	new Request({
		url: lang_id + url,
		method: 'get',
		onSuccess: function (resp) {

			Popup.escortToursPopup.inProcess = false;
			container.set('html', resp);


			var close_btn = new Element('div', {
				html: '',
				'class': 'escort-popup-close-btn-v2'
			}).inject(container);

			close_btn.addEvent('click', function() {
				$$('.escort-popup-wrapper').destroy();
				page_overlay.enable();
			});

			container.tween('opacity', '1');
			page_overlay.overlay.removeClass('overlay-loader');

			var ReloadTours;

			ReloadTours = function () {

				var rt_overlay = new Cubix.Overlay(container.getElement('#ajax-target'), {color: '#fff', opacity: .6, loader: _st('loader-small.gif'), position: '50%', offset: {bottom: 0, top: 0}});

				var target = container.getElement('#ajax-target'),
					escort_id = container.getElements('input[name=escort_id]')[0].get('value'),
					url = lang_id + '/private-v2/ajax-tours';

				rt_overlay.disable();
				new Request({
					url: url,
					data: {escort_id: escort_id},
					onSuccess: function (html) {
						target.set('html', html);
						rt_overlay.enable();
					}
				}).send();
			};

			Popup.DeleteTours = function () {
				var t = $$('.tours')[0].getElements('input[type=checkbox]').filter(function (el) {return (el.get('id') != 'check-all') && el.get('checked')});

				if ( ! t.length ) {
					alert('Please select some tours to delete');
					return;
				}

				if ( ! confirm('Are you sure you want to remove ' + t.length + ' selected tours?') ) {
					return;
				}

				var ids = [];
				t.each(function (el) {ids.include(el.get('value'));});

				new Request({
					url: lang_id + '/private-v2/remove-tours',
					data: {tours: ids},
					onSuccess: function () {
						ReloadTours();
					}
				}).send();
			};

			Popup.escortTourEdit = function (tour_id) {
				var escort_id = container.getElements('input[name=escort_id]')[0].get('value'),
					url = lang_id + '/private-v2/edit-tour',
					data = {},

					b = $(document.body);

				if ( tour_id ) {
					data.id = tour_id;
				} else {
					if ( ! escort_id ) {
						alert('Please select an escort to add tours for her/him');
						return;
					}

					data.escort_id = escort_id;
				}

				new Request({
					url: url,
					method: 'get',
					data: data,
					onSuccess: function (html) {
						var popup = new Element('div').inject(b).setStyles({
							position: 'absolute',
							//visibility: 'hidden',
							zIndex: 10000
						}).set('html', html);

						popup.setStyles({
							left: b.getWidth() / 2 - popup.getWidth() / 2,
							top: b.getHeight() / 2 - popup.getHeight() / 2,
							//visibility: null
						});

						InitEditPopup(popup);
					}
				}).send();
			};

			var InitEditPopup = function (p) {

				var uni_els = new Mooniform(p.getElements('select'));

				var date_from = new DatePicker(p.getElement('input[name=date_from]'), {pickerClass: 'datepicker',
					positionOffset: {x: -10, y: 0}, allowEmpty: true, format: 'j M Y'});
				var date_to = new DatePicker(p.getElement('input[name=date_to]'), {pickerClass: 'datepicker',
					positionOffset: {x: -10, y: 0}, allowEmpty: true, format: 'j M Y'});

				if ( $defined(p.getElement('select[name=country_id]')) ) {
					p.getElement('select[name=country_id]').addEvent('change', function () {Geography.loadCities(p.getElement('select[name=city_id]'), p.getElement('select[name=country_id]'), null, false);});
				}

				var at_overlay = new Cubix.Overlay(p.getElement('#tour-edit'), {color: '#fff', opacity: .6, loader: _st('loader-small.gif'), position: '50%', offset: {bottom: 0, top: 0}});

				p.getElement('form').set('send', {
					onRequest: function() {
						at_overlay.disable();
					},
					onSuccess: function (resp) {
						var json = JSON.decode(resp);
						if ( 'success' == json.status ) {
							Popup.CloseTourEditPopup();
							ReloadTours();
							return;
						}

						for ( var field in json.msgs ) {
							var el = p.getElement('*[name=' + field + ']');

							if ( ! el ) continue;
							el = el.getParent('.inner');
							new Element('div', {'class': 'error', html: json.msgs[field]}).inject(el);
						}

						at_overlay.enable();
					}
				});
			};

			Popup.CloseTourEditPopup = function () {
				$('tour-edit').destroy();
			};

			Popup.SaveTour = function (p) {
				p.getElements('.error').destroy();
				p.getElement('form').send();
			};

			ReloadTours();
		}
	}).send();


	return false;
}


Favorites = {};

Favorites.loadAll = function(page) {

	var container = $('ajax-content');
	var url = lang_id + "/favorites/ajax-get-all";

	data = {};

	var overlay = new Cubix.Overlay($('dashboard'), {color: '#fff', opacity: .6, loader: _st('loader-small.gif'), position: '50%', offset: {bottom: 0, top: 0}});

	if ( container.getElements('.filter-favorites select').length ) {
		data = {
			'sort_by' : container.getElements('select[name=sort_by]')[0].getSelected()[0].get('value'),
			'per_page' : container.getElements('select[name=per_page]')[0].getSelected()[0].get('value'),
			'fav_city_id' : container.getElements('select[name=fav_city_id]')[0].getSelected()[0].get('value'),
			'page' : page
		};
	}

	overlay.disable();

	new Request({
		url: url,
		data: data,
		onSuccess: function (html) {
			container.set('html', html);

			var elements = container.getElements('select');

			elements.addEvent('change', function(){
				Favorites.loadAll();
			});

			var uni_els = new Mooniform(elements);

			overlay.enable();
		}
	}).send();

	return false;
};

Favorites.remove = function(escort_id) {

	var container = $('ajax-content');

	var overlay = new Cubix.Overlay(container, {color: '#fff', opacity: .6, loader: _st('loader-small.gif'), position: '50%', offset: {bottom: 0, top: 0}});

	overlay.disable();

	new Request({
		url: lang_id + '/favorites/ajax-remove',
		data: {escort_id: escort_id},
		onSuccess: function (html) {

			overlay.enable();

			Favorites.loadAll(1);
		}
	}).send();

	return false;
};

/* Alerts */
Alerts = {};

Alerts.loadAll = function(page) {
	var container = $('ajax-content');
	var url = lang_id + "/alerts/ajax-get-all";

	data = {};

	var overlay = new Cubix.Overlay($('dashboard'), {color: '#fff', opacity: .6, loader: _st('loader-small.gif'), position: '50%', offset: {bottom: 0, top: 0}});

	if ( container.getElements('.filter-alerts select').length ) {
		data = {
			'show' : container.getElements('select[name=show]')[0].getSelected()[0].get('value'),
			'page' : page
		};
	}

	overlay.disable();

	new Request({
		url: url,
		data: data,
		onSuccess: function (html) {
			container.set('html', html);
			
			Slimbox.scanPage();
			
			container.getElements('select[name=show]').addEvent('change', function() {
				Alerts.loadAll();
			});
			
			var elements = container.getElements('select, input[type=checkbox]');
		
			var moo = new Mooniform(elements);
			
			/**/
			var cities = Array();
	
			$$('.cts').get('value').each(function(el) {
				cities.push(el);
			});
			
			$$('.alertme-save').get('rel').each(function(el) {
				Alerts.checkCHB(el, moo, elements);
			});

			$$('.alm-any').addEvent('click', function(e) {
				var arr = Array();
				arr = this.get('name').split('_');

				Alerts.checkCHB(arr[1], moo, elements);
			});
			
			$$('.alertme-city-btn').addEvent('click', function (e) {
				e.stop();
				var self = this;
				var escort_id = Number.from(self.get('rel').toString());
				var c = $('city_id_' + escort_id).getSelected();
				var c_id = Number.from(c.get('value').toString());
				var c_txt = c.get('text').toString();

				$('alm-6-' + escort_id).set('checked', 'checked');
				
				moo.reInit(elements);

				if (c_id != null && !cities.contains(escort_id + '-' + c_id))
				{
					cities.push(escort_id + '-' + c_id);
					Alerts.addCity(escort_id, c_id, c_txt);
					Alerts.removeCity(cities);
				}
			});
			
			Alerts.removeCity(cities);
			
			$$('.alertme-save').addEvent('click', function(e){
				e.stop();

				var escort_id = this.get('rel');
				var i = 0;
				var events = Array();
				var arr = Array();
				var cts = Array();
				var tmp = Array();

				$$("#form-" + escort_id + " input[type=checkbox]:checked").each(function(el) {
					arr = el.get('id').substr(4).split('-');
					events[i] = arr[0];
					i++;
				});

				i = 0;

				cities.each(function(el) {
					tmp = el.split('-');

					if (tmp[0] == escort_id)
					{
						cts[i] = tmp[1];
						i++;
					}
				});

				var cu = '';

				if (cts.length > 0)
					cu = '&cities=' + cts.join(',');

				var ev = events.join(',');

				if (ev == '5')
					ev = '1,2,3,4';
				else if (ev == '5,6')
					ev = '1,2,3,4,6';

				new Request({
					method: 'get',
					url: lang_id + '/alerts/save?escort_id=' + escort_id + '&events=' + ev + cu,
					onComplete: function(resp) {
						var cur_page = $('cur-page').get('value');
						Alerts.loadAll(cur_page);
					}
				}).send();

				return false;
			});

			$$('.alertme-remove').addEvent('click', function(e){
				e.stop();

				var escort_id = this.get('rel');

				new Request({
					method: 'get',
					url: lang_id + '/alerts/save?escort_id=' + escort_id + '&events=',
					onComplete: function(resp) {
						Alerts.loadAll(1);
					}
				}).send();

				return false;
			});
			
			$$('.alertme-set-active').addEvent('click', function(e){
				e.stop();

				var escort_id = this.get('rel');

				new Request({
					method: 'get',
					url: lang_id + '/alerts/set-status?escort_id=' + escort_id + '&status=1',
					onComplete: function(resp) {
						var cur_page = $('cur-page').get('value');
						Alerts.loadAll(cur_page);
					}
				}).send();

				return false;
			});
			
			$$('.alertme-set-inactive').addEvent('click', function(e){
				e.stop();

				var escort_id = this.get('rel');

				new Request({
					method: 'get',
					url: lang_id + '/alerts/set-status?escort_id=' + escort_id + '&status=0',
					onComplete: function(resp) {
						var cur_page = $('cur-page').get('value');
						Alerts.loadAll(cur_page);
					}
				}).send();

				return false;
			});
			/**/

			overlay.enable();
		}
	}).send();

	return false;
};

Alerts.checkCHB = function(escort_id, moo, els) {
	if ($$('input[name=5_' + escort_id + ']')[0].get('checked') == true)
	{
		$$('input[name=1_' + escort_id + ']').set('checked', '');
		$$('input[name=1_' + escort_id + ']').set('disabled', 'disabled');
		$$('input[name=2_' + escort_id + ']').set('checked', '');
		$$('input[name=2_' + escort_id + ']').set('disabled', 'disabled');
		$$('input[name=3_' + escort_id + ']').set('checked', '');
		$$('input[name=3_' + escort_id + ']').set('disabled', 'disabled');
		$$('input[name=4_' + escort_id + ']').set('checked', '');
		$$('input[name=4_' + escort_id + ']').set('disabled', 'disabled');
	}
	else
	{
		$$('input[name=1_' + escort_id + ']').set('disabled', '');
		$$('input[name=2_' + escort_id + ']').set('disabled', '');
		$$('input[name=3_' + escort_id + ']').set('disabled', '');
		$$('input[name=4_' + escort_id + ']').set('disabled', '');
	}
	
	moo.reInit(els);
};

Alerts.addCity = function(escort_id, id, text) {
	var myFirstElement  = new Element('div', {'class': 'city-item-wr'});
	var myCityTitleElement  = new Element('div', {'class': 'city-title'});
	var myClearElement  = new Element('div', {'class': 'clear'});
	var mySecondElement = new Element('a', {id: id, 'class': 'city-item'});
	var myHiddenElement = new Element('input', {type: 'hidden', value: escort_id + '-' + id, 'class': 'cts'});
	mySecondElement.inject(myFirstElement);
	myCityTitleElement.appendText(text);
	myCityTitleElement.inject(myFirstElement);
	myClearElement.inject(myFirstElement);
	myHiddenElement.inject(myFirstElement);
	myFirstElement.inject($('city-items-' + escort_id));
};

Alerts.removeCity = function(cities) {
	$$('.city-item').removeEvents();
	$$('.city-item').addEvent('click', function (e) {
		e.stop();
		var self = this;

		var id = self.get('id');
		var escort_id = self.get('rel');
		
		Alerts.removeByElement(cities, escort_id + '-' + id);
		self.getParent().dispose();
	});
};

Alerts.removeByElement = function(arrayName, arrayElement) {
	for (var i = 0; i < arrayName.length; i++)
	{ 
		if (arrayName[i] == arrayElement)
			arrayName.splice(i,1); 
	} 
};

/**/

/* City Alerts */
Cubix.CityAlerts = {};

Cubix.CityAlerts.loadAll = function() {
	var cont = $('ajax-content');
	var ov = new Cubix.Overlay(cont, {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});

	ov.disable();
	
	new Request({
		url: lang_id + '/city-alerts/index',
		method: 'get',
		onSuccess: function (resp) {			
			cont.set('html', resp);
			
			var elements = container.getElements('select, input[type=checkbox]');
		
			var moo = new Mooniform(elements);

			ov.enable();
			
			Cubix.CityAlerts.Show();
		}
	}).send();
	
	return false;
};

Cubix.CityAlerts.Show = function() {
	var cont = $('cont');
	var ov = new Cubix.Overlay(cont, {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});

	ov.disable();
	
	new Request({
		url: lang_id + '/city-alerts/ajax-get',
		method: 'get',
		onSuccess: function (resp) {			
			cont.set('html', resp);

			ov.enable();
		}
	}).send();
};

Cubix.CityAlerts.Remove = function(id) {
	var ov = new Cubix.Overlay($('cont'), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});

	ov.disable();
	
	new Request({
		url: lang_id + '/city-alerts/remove',
		method: 'post',
		data: {
			id: id
		},
		onSuccess: function (resp) {			
			ov.enable();
			
			Cubix.CityAlerts.Show();
		}
	}).send();
	
	return false;
};

Cubix.CityAlerts.Add = function() {
	var ov = new Cubix.Overlay($('city-alert-cont'), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});

	ov.disable();
		
	var data = {
		city_id: $('city').get('value'),
		/*notification: $$('input[name=notification]:checked').get('value').toString(),
		period: $('period').get('value')*/
	};
	
	if ($('escorts').get('checked'))
		data.escorts = 1;
	else
		data.escorts = 0;
	
	if ($('agencies').get('checked'))
		data.agencies = 1;
	else
		data.agencies = 0;
	
	new Request({
		url: lang_id + '/city-alerts/add',
		method: 'post',
		data: data,
		onSuccess: function (resp) {
			resp = JSON.decode(resp);
			var status = resp.status;
			
			$('city').removeClass('err');
			$('ch-wr').removeClass('err');

			if (!$('err-msg').hasClass('none'))
				$('err-msg').addClass('none');
			
			if (!$('err-msg2').hasClass('none'))
				$('err-msg2').addClass('none');

			if (status == 'success')
			{
				ov.enable();
				
				Cubix.CityAlerts.Show();
			}
			else if (status == 'error')
			{
				var msgs = resp.msgs;
				
				if (msgs.city_id)
				{
					$('city').addClass('err');
					
					if (msgs.city_id == 'exists')
						$('err-msg').removeClass('none');
					else
						$('err-msg2').removeClass('none');
				}

				if (msgs.about)
				{
					$('ch-wr').addClass('err');
				}
				
				ov.enable();
			}
		}
	}).send();
	
	return false;
};

/**/


/*-->Private Messaging*/
PrivateMessaging = {};
PrivateMessaging.composePopup;
PrivateMessaging.contactsPopup;
PrivateMessaging.Load = function() {
	var url = lang_id + '/private-messaging/index';

	if ( $defined($('thread-agency-escort-id')) && $('thread-agency-escort-id').get('value') ) {
		url += '&a_escort_id=' + $('thread-agency-escort-id').get('value');
	}

	var container = $('ajax-content');
	var overlayContainer = $('dashboard');
	
	if ( $('private-messaging') ) {
		overlayContainer = $('private-messaging');
	}
	
	var overlay = new Cubix.Overlay(overlayContainer, {color: '#fff', opacity: .6, loader: _st('loader-small.gif'), position: '50%', offset: {bottom: 0, top: 0}});
	
	overlay.disable();
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {
			container.set('html', resp);
			overlay.enable();
			new Fx.Scroll(window, {}).toElement($('private-messaging'));
		}
	}).send();

	return false;
}

PrivateMessaging.LoadThreads = function(page) {
	if ( undefined == page || page < 1 ) {
		if ( $$('#private-messaging .paging-list span').length ) {
			page = $$('#private-messaging .paging-list span').get('html');
		} else {
			page = 1;
		}
	}
	
	var url = lang_id + '/private-messaging/get-threads?page=' + page;

	/*if ( $defined($('thread-agency-escort-id')) && $('thread-agency-escort-id').get('value') ) {
		url += '&a_escort_id=' + $('thread-agency-escort-id').get('value');
	}*/

	
	var overlay = new Cubix.Overlay($('private-messaging'), {color: '#fff', opacity: .6, loader: _st('loader-small.gif'), position: '50%', offset: {bottom: 0, top: 0}});
	
	overlay.disable();
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {
			$$('#private-messaging .threads')[0].set('html', resp);
			overlay.enable();
		}
	}).send();

	return false;
}

PrivateMessaging.ShowThread = function(el, escortId) {
	if ( $('moopopup') ) {
		$('moopopup').destroy();
	}
	
	var pos = el.getCoordinates();
	var threadId = el.getAttribute('data-id');
	var mainOverlay = new Cubix.Overlay($('dashboard'), {color: '#fff', opacity: .6, has_loader: false});
	
	mainOverlay.disable();
	
	var url = lang_id + '/private-messaging/get-thread?id=' + threadId;
	if ( escortId ) {
		url += '&escort_id=' + escortId;
	}
	
	threadPopup = new moopopup({
		width: 540,
		top_left_x: pos.left - 560,
		top_left_y: pos.top - 200,
		resizable: false,
		max_body_height: 540,
		draggable: true,
		url: url,
		overlay: false,
		onComplete: function() {
			this.container.getElements('.close-btn').addEvent('click', function(){threadPopup.close();});
			var container = this.container;
			var overlay = new Cubix.Overlay($('pm-thread-container'), {color: '#fff', opacity: .6, has_loader: false});
			
			container.getElement('.conv').scrollTop = container.getElement('.conv').scrollHeight;
			
			container.getElements('textarea, input').addEvent('keypress', function(e) {
				if ( e.code == 13 && ! e.shift ) {
					e.stop();
					container.getElement('.send-btn').fireEvent('click');
				}
			});
			
			container.getElement('.send-btn').addEvent('click', function() {
				overlay.disable();
				new Request.JSON({
					url: lang_id + '/private-messaging/send-message-ajax',
					method: 'POST',
					data: {
						message: container.getElement('textarea').get('value'),
						captcha: container.getElement('input[name="captcha"]').get('value'),
						participant: $('participant-id').get('value'),
						escort_id: $('escort-id') ? $('escort-id').get('value') : false
					},
					onSuccess: function (resp) {
						container.getElements('span.error').set('html', '');
						container.getElement('input, textarea').removeClass('err');
						if ( resp.status == 'error' ) {
							for ( field in resp.msgs ) {
								container.getElement('input[name="' + field + '"], textarea[name="' + field + '"]').getNext('span.error').set('html', resp.msgs[field]);
								container.getElement('input[name="' + field + '"], textarea[name="' + field + '"]').addClass('err');
							}
						} else {
							container.getElement('textarea').set('value', '');
							
							
							/*-->Drawing message bubble*/
							var messageInfo = new Element('div', {'class': 'message-info'});
							new Element('span', {'class': 'name', 'html': resp.sender}).inject(messageInfo);
							new Element('span', {'class': 'date', 'html': resp.date}).inject(messageInfo);
							new Element('br').inject(messageInfo);
							new Element('p', {html: resp.message}).inject(messageInfo);
							
							var i = new Element('div', {'class': 'i'});
							var ii = new Element('div', {'class': 'ii'});
							messageInfo.inject(ii);
							ii.inject(i);
							var messageBody = new Element('div', {'class': 'message-body'});
							new Element('span', {'class': 'arr'}).inject(messageBody);
							i.inject(messageBody);
							
							var row = new Element('div', {'class': 'row self'});
							messageBody.inject(row);
							
							row.inject(container.getElement('.conv'));
							container.getElement('.conv').scrollTop = container.getElement('.conv').scrollHeight;
							/*<--Drawing message bubble*/
							
							reloadGrid = true;
						}
						container.getElement('span.captcha img').set('src', '/captcha?' + Math.floor(Math.random() * 10000 ));
						container.getElement('.captcha-input').set('value', '');
						overlay.enable();
					}
				}).send();
			});
	
		},
		onClose: function() {
			mainOverlay.enable();
			PrivateMessaging.LoadThreads();
		}
	});

	threadPopup.display();

	return false;
};

PrivateMessaging.ShowComposePopup = function(el, contactId) {
	var mainOverlay = new Cubix.Overlay($('dashboard'), {color: '#fff', opacity: .6, has_loader: false});
	mainOverlay.disable();
	
	var pos = el.getCoordinates();
	
	composePopup = new moopopup({
		id: 'compose-message-popup',
		html_node: 'compose-message-popup-wrapper',
		top_left_x: pos.left - 100,
		top_left_y: pos.top + 70,
		resizable: false,
		draggable: true,
		overlay: false,
		onClose: function() {
			mainOverlay.enable();
		}
	});
	
	
	composePopup.display();
	composePopup.container.getElements('.close-btn, .cancel').addEvent('click', function(){composePopup.close();});
	composePopup.container.getElements('.send').addEvent('click', function(){PrivateMessaging.SendComposeMessage(composePopup)});
	
	composePopup.container.getElements('textarea, input').addEvent('keypress', function(e) {
		if ( e.code == 13 && ! e.shift ) {
			e.stop();
			PrivateMessaging.SendComposeMessage(composePopup);
		}
	});
	
	if ( contactId ) {
		PrivateMessaging.contactsPopup.close();
		composePopup.container.getElements('select[name="participant_id"]').set('value', contactId);
	}

	new Mooniform($$('#compose-message-popup .styled'));
	
	
	return false;
};



PrivateMessaging.SendComposeMessage = function(popup) {
	var overlay = new Cubix.Overlay($$('#compose-message-popup .cont')[0], {color: '#fff', opacity: .6, loader: _st('loader-small.gif')});
	overlay.disable();
	
	new Request.JSON({
		url: lang_id + '/private-messaging/send-message-ajax',
		method: 'POST',
		data: {
			message: $$('#compose-message-popup textarea')[0].get('value'),
			captcha: $$('#compose-message-popup input[name="captcha')[0].get('value'),
			participant:  $$('#compose-message-popup select[name="participant_id"]')[0].getSelected()[0].value,
			escort_id:  $$('#compose-message-popup select[name="escort_id"]').length ? $$('#compose-message-popup select[name="escort_id"]')[0].getSelected()[0].value : false
		},
		onSuccess: function (resp) {
			popup.container.getElements('span.error').set('html', '');
			popup.container.getElement('input, textarea').removeClass('err');
			
			popup.container.getElement('span.captcha img').set('src', '/captcha?' + Math.floor(Math.random() * 10000 ));
			popup.container.getElement('.captcha-input').set('value', '');
			
			if ( resp.status == 'error' ) {
				for ( field in resp.msgs ) {
					$('compose-message-popup').getElements('input[name="' + field + '"], textarea[name="' + field + '"]').getNext('span.error').set('html', resp.msgs[field]);
					$('compose-message-popup').getElements('input[name="' + field + '"], textarea[name="' + field + '"]').addClass('err');
				}
			} else {
				popup.close();
				PrivateMessaging.LoadThreads();
			}
			
			overlay.enable();
		}
	}).send();
	
	return false;
};

PrivateMessaging.RemoveThread = function(id, selfEscortId)
{
	var overlay = new Cubix.Overlay($('private-messaging'), {color: '#fff', opacity: .6, has_loader: false});
	overlay.disable();
		
	url = lang_id + '/private-messaging/remove-thread?id=' + id
		
	if ( typeof selfEscortId !== 'undefined' ) {
		url += '&escort_id=' + selfEscortId;
	}
		
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {			
			overlay.enable();
			PrivateMessaging.LoadThreads();
		}
	}).send();
	

}

PrivateMessaging.BlockThread = function(id, selfEscortId)
{
	
	var overlay = new Cubix.Overlay($('private-messaging'), {color: '#fff', opacity: .6, has_loader: false});
	overlay.disable();
		
	url = lang_id + '/private-messaging/block-thread?id=' + id;
	if ( typeof selfEscortId !== 'undefined' ) {
		url += '&a_escort_id=' + selfEscortId;
	}
		
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {			
			overlay.enable();
			PrivateMessaging.LoadThreads();
		}
	}).send();
	
}

PrivateMessaging.UnBlockThread = function(id, selfEscortId)
{	
	var overlay = new Cubix.Overlay($('private-messaging'), {color: '#fff', opacity: .6, has_loader: false});
	overlay.disable();
	
	url = lang_id + '/private-messaging/unblock-thread?id=' + id;
	if ( typeof selfEscortId !== 'undefined' ) {
		url += '&a_escort_id=' + selfEscortId;
	}
		
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {
			overlay.enable();
			PrivateMessaging.LoadThreads();
		}
	}).send();
	
}

PrivateMessaging.RemoveContact = function(id)
{
	var overlay = new Cubix.Overlay($('private-messaging'), {color: '#fff', opacity: .6, has_loader: false});
	overlay.disable();
	
		
	var url = lang_id + '/private-messaging/remove-contact?id=' + id;
	
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {			
			overlay.enable();
			PrivateMessaging.LoadThreads();
			PrivateMessaging.LoadContacts();
		}
	}).send();
}

PrivateMessaging.AddContact = function(id)
{
	if ( $$('#contacts-popup-wrapper table tbody tr').length >= 10 ) {
		alert('You can have only 10 contacts.');
		return;
	}
	
	var overlay = new Cubix.Overlay($('private-messaging'), {color: '#fff', opacity: .6, has_loader: false});
	overlay.disable();
			
	var url = lang_id + '/private-messaging/add-contact?id=' + id;
	
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {			
			overlay.enable();
			PrivateMessaging.LoadThreads();
			PrivateMessaging.LoadContacts();
		}
	}).send();
}


PrivateMessaging.ShowContactsPopup = function(el) {
	var mainOverlay = new Cubix.Overlay($('dashboard'), {color: '#fff', opacity: .6, has_loader: false});
	mainOverlay.disable();
	
	var pos = el.getCoordinates();
	
	PrivateMessaging.contactsPopup = new moopopup({
		id: 'contacts-popup',
		html_node: 'contacts-popup-wrapper',
		top_left_x: pos.left - 280,
		top_left_y: pos.top - 10,
		overlay: false,
		onClose: function() {
			mainOverlay.enable();
		}
	});
	
	
	
	PrivateMessaging.contactsPopup.display();
	PrivateMessaging.contactsPopup.container.getElements('.close-btn').addEvent('click', function(){PrivateMessaging.contactsPopup.close();});
	PrivateMessaging.initContactListEvents();
	
	return false;
};


PrivateMessaging.LoadContacts = function() {
	if ( $('contacts-popup') ) {
		var overlay = new Cubix.Overlay($$('#contacts-popup .body')[0], {color: '#fff', opacity: .6, has_loader: false});
		overlay.disable();
	}
	
	new Request.JSON({
		url: lang_id + '/private-messaging/contacts?ajax=1',
		method: 'get',
		onSuccess: function (resp) {
			$$('#contacts-popup-wrapper .body')[0].set('html', resp.html);
			
			
			if ( $('contacts-popup') ) {
				PrivateMessaging.contactsPopup.container.getElements('.body')[0].set('html', resp.html);
				overlay.enable();
			}
			
			$$('.compose-message-popup select[name="participant_id"]')[0].empty();
			
			new Element('option', {
				value: '',
				html: 'Select Participant'
			}).inject($$('.compose-message-popup select[name="participant_id"]')[0]);
			resp.data.each(function(el, i) {
				new Element('option', {
					value: el.escort_id ? 'escort-' + el.escort_id : 'member-' + el.member_username,
					html: el.escort_showname ? el.escort_showname : el.member_username
				}).inject($$('.compose-message-popup select[name="participant_id"]')[0]);
			});
		}
	}).send();
};

PrivateMessaging.initContactListEvents = function() {
	$$('.contacts-popup .contact-compose').removeEvents();
	$$('.contacts-popup .contact-compose').each(function(el, i) {
		el.addEvent('click', function() {
			contactId = el.getParent('tr').get('data-id');
			PrivateMessaging.ShowComposePopup($$('#private-messaging .btn-compose-message')[0], contactId);
		});
	});
	
	$$('.contacts-popup .contact-remove').each(function(el, i) {
		el.addEvent('click', function() {
			var overlay = new Cubix.Overlay($$('#contacts-popup .body')[0], {color: '#fff', opacity: .6, has_loader: false});
			overlay.disable();
			contactId = el.getParent('tr').get('data-id');
			
			new Request.JSON({
				url: lang_id + '/private-messaging/remove-contact?id=' + contactId + '&get_html=1',
				method: 'post',
				onSuccess: function () {
					PrivateMessaging.LoadThreads();
					PrivateMessaging.LoadContacts();
					PrivateMessaging.initContactListEvents();
					
					overlay.enable();
					
				}
			}).send();
			
		});
	});
}

PrivateMessaging.updateUnreadThreadsCount = function(count)
{
	if ( $$('#dashboard .features .private-messages').length ) {
		
		if ( count == 0 ) {
			$$('#dashboard .features .private-messages span').destroy();
			return;
		}
		
		if ( ! $$('#dashboard .features .private-messages span').length ) {
			new Element('span').inject($$('#dashboard .features .private-messages')[0]);
		}

		if ( count > 9 ) {
			$$('#dashboard .features .private-messages span').set('html', '9+');
		} else {
			$$('#dashboard .features .private-messages span').set('html', count);
		}
	}
	
	return;
}

/*<--Private Messaging*/


Popup.addFavoriteNote = function(pos_el, x_offset, escort_id) {

	var pos = pos_el.getCoordinates();

	add_favorite_note = new moopopup({
		title: 'Add / Edit Favorite Note',
		width: 415,
		top_left_x: pos.left - x_offset,
		top_left_y: pos.top - 350,
		resizable: false,
		max_body_height: 580,
		draggable: true,
		url: lang_id + '/favorites/ajax-add-note?ajax=1&escort_id=' + escort_id,
		overlay: false,
		onComplete: function() {

			this.container.getElements('.popup-close-btn').addEvent('click', function(){add_favorite_note.close()});
			this.container.getElements('button[type=reset]').addEvent('click', function(e){e.stop();add_favorite_note.close()});

			var container = this.container;

			var overlay = new Cubix.Overlay(container.getElements('.moopopup-body')[0], {color: '#fff', opacity: .6, loader: _st('loader-small.gif'), position: '50%', offset: {bottom: 0, top: 0}});

			tinyMCE.init({
				mode : "textareas",
				theme : "advanced",
				editor_selector : 'note',

				onchange_callback : function(inst){
					container.getElement('#message').set('value', inst.getBody().innerHTML);
				},

				plugins: "emotions,advhr,preview,paste",
				theme_advanced_toolbar_location	  : "top",
				theme_advanced_toolbar_align	  : "left",
				theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,undo,redo,|,pastetext,pasteword,emotions,code,preview",
				theme_advanced_buttons2 : "",
				theme_advanced_buttons3_add : "",
				relative_urls : false,
				convert_urls : true,
				forced_root_block : false,
				force_br_newlines : true,
				force_p_newlines : false
			});		


			this.container.getElements('form').addEvent('submit', function(e){
				e.stop();

				overlay.disable();

				this.get('send').removeEvents('success');
				this.set('send', {
					onSuccess: function (resp) {
						resp = JSON.decode(resp);

						overlay.enable();

						container.getElements('textarea').each(function(el){
							el.tween('background-color', '#fff')
						});

						if ( resp.status == 'error' ) {
							for ( var field in resp.msgs ) {
								var input = container.getElement('*[name=' + field + ']');
								//input.addClass('error');
								input.tween('background-color', '#FAF0F4')
							}
						}
						else if ( resp.status == 'success' ) {
							add_favorite_note.close();
						}
					}.bind(this)
				});

				this.send();
			})
		}
	});

	add_favorite_note.display();

	return false;
};

Sceon.enableDisableProfile = function(btn) {
	var href = btn.get('href');

	var overlayContainer = $('dashboard');
	
	var overlay = new Cubix.Overlay(overlayContainer, {color: '#fff', opacity: .6, loader: _st('loader-small.gif'), position: '50%', offset: {bottom: 0, top: 0}});
	overlay.disable();

	new Request({

		url: lang_id + href,
		method: 'post',
		data: {},
		onSuccess: function (resp) {
			location.reload();
		}
	}).send();

	return false;
};

window.addEvent('domready', function() {
	Sceon.initAgencyProfileBlock();

	Sceon.initOpenableBlocks();
	Sceon.initChangePassword();
	Sceon.initChangeWatchedType();
	Sceon.initAgencyEscortsTabs();

	if ( $$('#dashboard .verification-request').length ) {
		$$('#dashboard .verification-request').addEvent('click', function(e){
			e.stop();

			Popup.Verification.display();
		});
	}

	$$('#dashboard .view-tickets, #dashboard .view-tickets-text').addEvent('click', function(e){
		e.stop();

		Popup.SupportTicketsList.display();
	});
});



/* CLIENT/HOTEL BLACKLIST */

ClientHotelBlacklist = {};

ClientHotelBlacklist.Load = function(type, clear_params, page) {
	var url = lang_id + '/client-hotel-blacklist/' + type;

	var container = $('ajax-content');
	var overlayContainer = $('dashboard');
	
	var overlay = new Cubix.Overlay(overlayContainer, {color: '#fff', opacity: .6, loader: _st('loader-small.gif'), position: '50%', offset: {bottom: 0, top: 0}});
	
	var data = {};

	if ( ! page ) {
		page = {};
	}

	if ( $$('#client-hotel-blacklist .form').length && ! clear_params ) {
		data = $$('#client-hotel-blacklist .form')[0].toQueryString().parseQueryString()
	}

	overlay.disable();
	new Request({
		url: url,
		method: 'post',
		data: Object.merge(data, page),
		onSuccess: function (resp) {
			container.set('html', resp);

			ClientHotelBlacklist.Init(container, type);

			if(type == 'clients')
			{
				$$('#attach-box').show();
				/**
				* Uploader instance
				*/
				var up = new FancyUpload3.Attach('attach-list', '#attach-file, #attach-file-2', {
					path: '/js/fancy/Swiff.Uploader.swf',
					url: "/private-v2/attached-image-blacklist",
					data: "PHPSESSID=" + $('sess_name').get('value'),
					fileSizeMax: 2 * 1024 * 1024,
					verbose: false,
					fileListMax: 4,
					typeFilter: {
						'Files (*.jpg, *.jpeg, *.gif, *.png)':'*.jpg; *.jpeg; *.gif; *.png'
					},
					onSelectFail: function(files) {
						files.each(function(file) {
							new Element('li', {
								'class': 'file-invalid',
								events: {
									click: function() {
										this.destroy();
									}
								}
							}).adopt(
								new Element('span', {html: file.validationErrorMessage || file.validationError})
							).inject(this.list, 'bottom');
						}, this);	
					},

					onFileSuccess: function(file, response) {
						var json = new Hash(JSON.decode(response, true) || {});
						

						if (json.get('status') == '1') {
							new Element('input', {type: 'checkbox', 'checked': true, name:'attach[]', value:json.get('id')}).inject(file.ui.element, 'top');
							file.ui.element.highlight('#e6efc2');
						}
						else{
							file.ui.element.destroy();
							new Element('li', {
								'class': 'file-invalid',
								events: {
									click: function() {
										this.destroy();
									}
								}
							}).adopt(
								new Element('span', {html: json.error})
							).inject(this.list, 'bottom');
						}
					},

					onFileError: function(file) {
						file.ui.cancel.set('html', 'Retry').removeEvents().addEvent('click', function() {
							file.requeue();
							return false;
						});

						new Element('span', {
							html: file.errorMessage,
							'class': 'file-error'
						}).inject(file.ui.cancel, 'after');
					},

					onFileRequeue: function(file) {
						file.ui.element.getElement('.file-error').destroy();

						file.ui.cancel.set('html', 'Cancel').removeEvents().addEvent('click', function() {
							file.remove();
							return false;
						});

						this.start();
					}

				});
			} else {
				$$('#attach-box').hide();
			}
			//
			overlay.enable();
			new Fx.Scroll(window, {}).toElement($('client-hotel-blacklist'));
		}
	}).send();

	return false;
}

ClientHotelBlacklist.Init = function(container, type) {
	var moo_elements = container.getElements('select');
	var uni_els = new Mooniform(moo_elements);

	var popup = container.getElements('.popup-entry')[0];
	var search_btn = container.getElements('.btn-search')[0];
	var btn_add = container.getElements('.btn-add')[0];

	popup.addClass('none');

	search_btn.addEvent('click', function(e){
		e.stop();

		ClientHotelBlacklist.Load(type, false, false);
	});

	btn_add.addEvent('click', function(e){
		e.stop();

		popup.toggleClass('none');
	});

	popup.getElements('.close-x').addEvent('click', function(e){
		e.stop();

		popup.addClass('none');
	});

	var date = new DatePicker(popup.getElement('input[name=date]'), {pickerClass: 'datepicker',
		positionOffset: {x: -10, y: 0}, allowEmpty: true, format: 'j M Y'});

	var error_box = popup.getElements('.error-msgs')[0];

	popup.getElement('select[name=country_id]').addEvent('change', function(){

		var self = this;
		var country_id = self.getSelected()[0].get('value');
		var city = popup.getElement('select[name=city_id]');

		new Request({
			url: lang_id + '/geography/ajax-get-cities',
			data: { country_id: country_id },
			method: 'get',
			onSuccess: function (resp) {
				resp = JSON.decode(resp, true);
				if ( ! resp ) return;
				city.empty();
				new Element('option', { value: '', html: '- city -' }).inject(city);

				resp.data.each(function (c) {
					new Element('option', {
						value: c.id,
						html: c.title + '&nbsp;&nbsp;',						
					}).inject(city);
				});

				uni_els.update(moo_elements);
			}
		}).send();
	});

	popup.getElements('.popup-form')[0].addEvent('submit', function(e) {
		e.stop();

		var self = this;

		var overlay = new Cubix.Overlay(popup.getElements('.inn')[0], {color: '#fff', opacity: .6, z_index: 10000, loader: _st('loader-small.gif'), position: '50%', offset: {bottom: 0, top: 0}});
		overlay.disable();
		new Request.JSON ({
			url: lang_id + this.get('action'),
			method: 'post',
			data: self.toQueryString().parseQueryString(),
			onSuccess: function (resp) {
				
				if ( resp.status == "error" ) {
					var errors = "";
					for ( msg in resp.msgs ) {
						errors += '<p>' + resp.msgs[msg] + '</p>';
					}
					error_box.set('html', errors).removeClass('none');
				} else {
					error_box.set('html', '').addClass('none');

					popup.addClass('none');
					self.reset();
				}

				overlay.enable();
			}
		}).send();
	});

	if ( container.getElements('.admin-entries-box').length ) {
		var admin_entries = container.getElements('.admin-entries-box')[0];

		var btn = container.getElements('.admin-entries a')[0];

		btn.addEvent('click', function(e){
			e.stop();

			if ( this.hasClass('show') ) {
				admin_entries.removeClass('none');
				this.removeClass('show').addClass('hide');
				Cookie.write('is_show_admin_entr', 1, {domain: headerVars.domain, duration: 365});
			} else {
				admin_entries.addClass('none');
				this.removeClass('hide').addClass('show');
				Cookie.write('is_show_admin_entr', 0, {domain: headerVars.domain, duration: 365});
			}
			var txt = this.get('html');
			this.set('html', this.get('rel')).set('rel', txt);
		});
	}
};

/* CLIENT/HOTEL BLACKLIST */

Sceon.initShowOnlineStatusBtn = function(flag) {

	var overlayContainer = $('dashboard');	
	var overlay = new Cubix.Overlay(overlayContainer, {color: '#fff', opacity: .6, loader: _st('loader-small.gif'), position: '50%', offset: {bottom: 0, top: 0}});
	overlay.disable();

	new Request ({
		url: lang_id + '/private-v2/ajax-update-show-online-status?flag=' + flag,
		method: 'get',			
		onSuccess: function (resp) {
			location.reload();
		}
	}).send();

	return false;
};