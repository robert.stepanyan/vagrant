Popup = {};

Popup.Advertise = new moopopup({
	id: 'advertise-popup',
	title: 'Advertise',
	width: 990,
	resizable: false,
	max_body_height: 800,
	z_index: 100,
	draggable: false,
	url: lang_id + '/index/advertise',
	onComplete: function() {

		this.container.getElements('.popup-close-btn').addEvent('click', function(){Popup.Advertise.close()});

		var container = this.container;

		var bottom_text = container.getElements('div.bottom-text')[0];

		bottom_text.position({
			relativeTo: container.getElements('.moopopup-body')[0],
			//position: 'centerBottom',
			edge: 'center'
		});

		bottom_text.setStyles({
			'top' : 'auto',
			'bottom': '-37px'
		});
	},
	onClose: function() {

	}
});

Popup.ContactUs = new moopopup({
	title: 'Contact Us',
	width: 990,
	resizable: false,
	max_body_height: 580,
	z_index: 100,
	draggable: false,
	url: lang_id + '/contacts/contact-us?ajax=1',
	onComplete: function() {

		this.container.getElements('.popup-close-btn').addEvent('click', function(){Popup.ContactUs.close()});

		var container = this.container;

		var overlay = new Cubix.Overlay(container.getElements('.moopopup-body')[0], {color: '#fff', opacity: .6, loader: _st('loader-small.gif'), position: '50%', offset: {bottom: 0, top: 0}});

		this.container.getElements('form').addEvent('submit', function(e){
			e.stop();

			overlay.disable();

			this.get('send').removeEvents('success');
			this.set('send', {
				onSuccess: function (resp) {
					resp = JSON.decode(resp);

					overlay.enable();

					container.getElements('input[type=text], textarea').each(function(el){
						el.tween('background-color', '#fff')
					});

					if ( resp.status == 'error' ) {
						for ( var field in resp.msgs ) {
							var input = container.getElement('*[name=' + field + ']');
							//input.addClass('error');
							input.tween('background-color', '#FAF0F4')
						}
					}
					else if ( resp.status == 'success' ) {
						Popup.ContactUs.close();

						var successPopup = new moopopup({
							title: 'Contact Us',
							width: 990,
							resizable: false,
							max_body_height: 580,
							draggable: false,
							url: lang_id + '/contacts/contact-us-success?ajax=1',
							onComplete: function() {
								this.container.getElements('.popup-close-btn').addEvent('click', function(){successPopup.close()});
							},
							onClose: function() {

							}
						});
						successPopup.display();
					}
				}.bind(this)
			});

			this.send();
		})
	},
	onClose: function() {

	}
});

Popup.ContactTo = function(pos_el, id, type, x_offset, ca) {

	var pos = pos_el.getCoordinates();

	var contact_to = new moopopup({
		title: 'Contact To',
		top_left_x: pos.left - x_offset,
		top_left_y: ca ? pos.top - 100 : pos.top - 330,
		width: 415,
		resizable: false,
		//max_body_height: 280,
		draggable: false,
		url: lang_id + '/contacts/contact-to?ajax=1&id=' + id + '&type=' + type,
		onComplete: function() {

			var container = this.container;

			container.getElements('.popup-close-btn').addEvent('click', function(){contact_to.close()});

			var overlay = new Cubix.Overlay(container.getElements('.moopopup-body')[0], {color: '#fff', opacity: .6, loader: _st('loader-small.gif'), position: '50%', offset: {bottom: 0, top: 0}});

			this.container.getElements('form').addEvent('submit', function(e){
				e.stop();


				overlay.disable();

				this.get('send').removeEvents('success');
				this.set('send', {
					onSuccess: function (resp) {
						resp = JSON.decode(resp);

						overlay.enable();

						container.getElements('input[type=text], textarea').each(function(el){
							el.tween('background-color', '#fff')
						});

						if ( resp.status == 'error' ) {
							for ( var field in resp.msgs ) {
								var input = container.getElement('*[name=' + field + ']');
								input.tween('background-color', '#FAF0F4')
							}
						}
						else if ( resp.status == 'success' ) {
							ga('send', 'event', 'MessageForm', 'sent');

							contact_to.close();
							var successPopup = new moopopup({
								title: 'Contact To',
								top_left_x: pos.left - x_offset,
								top_left_y: pos.top - 170,
								width: 395,
								resizable: false,
								//max_body_height: 280,
								draggable: false,
								url: lang_id + '/contacts/contact-to-success?ajax=1&id=' + id + '&type=' + type,
								onComplete: function() {
									this.container.getElements('.popup-close-btn').addEvent('click', function(){successPopup.close()});

								},
								onClose: function() {

								}
							});
							successPopup.display();
						}
					}.bind(this)
				});

				this.send();
			})
		},
		onClose: function() {

		}
	});

	contact_to.display();

	return false;
};

Popup.SignInUp = new moopopup({
    id: 's-moopopup',
	title: 'SignInUp',
	width: 947,
	height: 550,
	resizable: false,
	draggable: false,
	z_index: 100,
	url: lang_id + '/private/sign-in-up?ajax=1',
	onComplete: function() {

		this.container.getElements('.popup-close-btn-small').addEvent('click', function(){Popup.SignInUp.close()});

		this.container.getElements('.fgt-pass').addEvent('click', function(e){
			e.stop();

			Popup.ForgotPassword(this, -260, 40);
		});

		this.container.getElements('.signup-btn').addEvent('click', function(e){
			e.stop();

			Popup.SignInUp.close();

			Popup.SignUp();
		});

		var container = this.container;

		var overlay = new Cubix.Overlay(container.getElements('.moopopup-body')[0], {color: '#fff', opacity: .6, loader: _st('loader-small.gif'), position: '50%', offset: {bottom: 0, top: 0}});

		this.container.getElements('form').addEvent('submit', function(e){
			e.stop();

			overlay.disable();

			this.get('send').removeEvents('success');
			this.set('send', {
				onSuccess: function (resp) {
					resp = JSON.decode(resp);

					overlay.enable();

					container.getElements('div.error').set('html', '');

					container.getElements('input[type=text], input[type=password]').each(function(el){
						el.tween('background-color', '#fff')
					});

					if ( resp.status == 'error' ) {
						for ( var field in resp.msgs ) {
							var input = container.getElement('*[name=' + field + ']');
							input.tween('background-color', '#FAF0F4')
							input.getNext('div.error').set('html', resp.msgs[field]);
						}
					} else if ( resp.status == 'success' ) {
						Popup.SignInUp.close();
						ga('send', 'event', { eventCategory: resp.signin_type, eventAction: 'signup'});
						var e_overlay = new Cubix.Overlay($('container'), {color: '#fff', opacity: .6, loader: _st('loader-tiny.gif'), position: '90px', offset: {bottom: 0, top: 0}});
						e_overlay.disable();

						location.reload();
					}
				}.bind(this)
			});

			this.send();
		})
	},
	onClose: function() {

	}
});

Popup.ForgotPassword = function(pos_el, x_offset, y_offset) {

	var pos = pos_el.getCoordinates();

	var forgot_pass = new moopopup({
		title: 'Forgot Password',
		top_left_x: pos.left + x_offset,
		top_left_y: pos.top + y_offset,
		width: 395,
		/*height: 220,*/
		resizable: false,
		draggable: false,
		url: lang_id + '/private/forgot?ajax=1',
		onComplete: function() {

			forgot_pass.setFocus();

			var container = this.container;

			container.getElements('.popup-close-btn').addEvent('click', function(){forgot_pass.close()});

			var overlay = new Cubix.Overlay(container.getElements('.moopopup-body')[0], {color: '#fff', opacity: .6, loader: _st('loader-small.gif'), position: '50%', offset: {bottom: 0, top: 0}});

			this.container.getElements('form').addEvent('submit', function(e){
				e.stop();

				overlay.disable();

				this.get('send').removeEvents('success');
				this.set('send', {
					onSuccess: function (resp) {
						resp = JSON.decode(resp);

						overlay.enable();

						container.getElements('input[type=text]').each(function(el){
							el.tween('background-color', '#fff')
						});

						container.getElements('.err')[0].set('html', '');

						if ( resp.status == 'error' ) {
							var err_text = "";
							for ( var field in resp.msgs ) {
								var input = container.getElement('*[name=' + field + ']');
								input.tween('background-color', '#FAF0F4');

								err_text += resp.msgs[field];
							}

							container.getElements('.err')[0].set('html', err_text);
						}
						else if ( resp.status == 'success' ) {
							forgot_pass.close();

							var successPopup = new moopopup({
								title: 'Forgot Password Success',
								top_left_x: pos.left + x_offset,
								top_left_y: pos.top + y_offset,
								width: 395,
								height: 220,
								resizable: false,
								//max_body_height: 280,
								draggable: false,
								url: lang_id + '/private/forgot-success?ajax=1',
								onComplete: function() {
									this.container.getElements('.popup-close-btn, .close-btn').addEvent('click', function(e){e.stop(); successPopup.close()});
								},
								onClose: function() {

								}
							});
							successPopup.display();
						}
					}.bind(this)
				});

				this.send();
			})
		},
		onClose: function() {

		}
	});

	forgot_pass.display();

	return false;
};

Popup.SignUp = function() {

	var signUp = new moopopup({
		title: 'SignUp',
		width: 990,
		height: 580,
		resizable: false,
		draggable: false,
		url: lang_id + '/private/signup?ajax=1&step=type',
		onComplete: function() {

			signUp.setFocus();

			this.container.getElements('.popup-close-btn').addEvent('click', function(){signUp.close()});

			var container = this.container;

			var overlay = new Cubix.Overlay(container.getElements('.moopopup-body')[0], {color: '#fff', opacity: .6, loader: _st('loader-small.gif'), position: '50%', offset: {bottom: 0, top: 0}});

			this.container.getElements('.body a.btn').addEvent('click', function(e) {
				e.stop();

				overlay.disable();

				var user_type = this.get('rel');

				signUp.close();

				var formPopup = new moopopup({
					title: 'SignUp Form',
					width: 990,
					height: 580,
					resizable: false,
					draggable: false,
					url: lang_id + '/private/signup?ajax=1&step=form&user_type=' + user_type,
					onComplete: function() {
						this.container.getElements('.popup-close-btn').addEvent('click', function(){formPopup.close()});

						var container = this.container;

						validateUsername(container);
						validateEmail(container);
						validateConfirmEmail(container);
						validatePassword(container);
						validateConfirmPassword(container);

						Sceon.initRecaptcha();

						new Mooniform(this.container.getElements('input[name=terms]'));

						var overlay = new Cubix.Overlay(container.getElements('.moopopup-body')[0], {color: '#fff', opacity: .6, loader: _st('loader-small.gif'), position: '50%', offset: {bottom: 0, top: 0}});

						this.container.getElements('form').addEvent('submit', function(e){
							e.stop();


							overlay.disable();

							this.get('send').removeEvents('success');
							this.set('send', {
								onSuccess: function (resp) {
									resp = JSON.decode(resp);

									overlay.enable();

									container.getElements('.icon').each(function(el){
										el.removeClass('error').addClass('success');
									});

									container.getElements('.err-text').each(function(el){
										el.set('html', '');
										el.addClass('none');
									});

									if ( resp.status == 'error' ) {

										for ( var field in resp.msgs ) {
											var input = container.getElement('*[name=' + field + ']');
											//input.addClass('error');
											if ( input.getNext('.err-text') && resp.msgs[field].length > 0 ) {
												input.getNext('.err-text').set('html', resp.msgs[field]).removeClass('none');
											}

											input.getParent('div.p').getElements('.icon').removeClass('success').addClass('error');
										}



									}
									else if ( resp.status == 'success' ) {
										formPopup.close();
										ga('send', 'event', resp.user_type, 'signup', 'classic');

										var successPopup = new moopopup({
											title: 'SignUp Success',
											width: 990,
											height: 580,
											resizable: false,
											draggable: false,
											url: lang_id + '/private/signup?ajax=1&step=success',
											onComplete: function() {
												this.container.getElements('.popup-close-btn').addEvent('click', function(){successPopup.close()});

											},
											onClose: function() {

											}
										});
										successPopup.display();
									}
								}.bind(this)
							});

							this.send();
						});

					},
					onClose: function() {

					}
				});
				formPopup.display();
			});
		},
		onClose: function() {

		}
	});

	signUp.display();

	return false;
};


var validateUsername = function(el) {
	if (el.getElement('#username')){
		el.getElement('#username').addEvent('blur', function () {

			this.getNext('.err-text').set('html', '').addClass('none');
			this.getParent('div.line').getNext('.icon').removeClass('error').removeClass('success');

			if ( this.get('value').length < 6 ) {
				this.getNext('.err-text').set('html', headerVars.dictionary.su_username_at_least_6).removeClass('none');
				this.getParent('div.line').getNext('.icon').addClass('error');
				return;
			}

			var regex = /^[-_a-z0-9]+$/i;
			if ( ! regex.test(this.get('value')) ) {
				this.getNext('.err-text').set('html', headerVars.dictionary.su_only_az).removeClass('none');
				this.getParent('div.line').getNext('.icon').addClass('error');
				return;
			}

			this.getParent('div.line').getNext('.icon').addClass('loading');

			new Request({
				url: lang_id + '/private/check?username=' + this.get('value'),
				method: 'get',
				onSuccess: function (resp) {
					var resp = JSON.decode(resp);

					this.getParent('div.line').getNext('.icon').removeClass('loading');

					if ( resp.status == 'found' ) {
						this.getNext('.err-text').set('html', headerVars.dictionary.su_username_exist).removeClass('none');
						this.getParent('div.line').getNext('.icon').addClass('error');
					}
					else if ( resp.status == 'not found' ) {
						this.getNext('.err-text').set('html', '').addClass('none');
						this.getParent('div.line').getNext('.icon').addClass('success');
					}
				}.bind(this)
			}).send();
		});
	}
}

var validateEmail = function(el) {
	if (el.getElement('#email')){
		el.getElement('#email').addEvent('blur', function () {

			this.getNext('.err-text').set('html', '').addClass('none');
			this.getParent('div.line').getNext('.icon').removeClass('error').removeClass('success');

			var regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
			if ( ! this.get('value').length || ! regex.test(this.get('value') ) ) {
				this.getNext('.err-text').set('html', headerVars.dictionary.su_email_is_invalid).removeClass('none');
				this.getParent('div.line').getNext('.icon').addClass('error');
				return;
			}

			this.getParent('div.line').getNext('.icon').addClass('loading');

			new Request({
				url: lang_id + '/private/check?email=' + this.get('value'),
				method: 'get',
				onSuccess: function (resp) {
					var resp = JSON.decode(resp);

					this.getParent('div.line').getNext('.icon').removeClass('loading');

					if ( resp.status == 'found' ) {
						this.getNext('.err-text').set('html', headerVars.dictionary.su_email_exist).removeClass('none');
						this.getParent('div.line').getNext('.icon').addClass('error');
					}
					else if ( resp.status == 'not found' ) {
						this.getNext('.err-text').set('html', '').addClass('none');
						this.getParent('div.line').getNext('.icon').addClass('success');
					}
					else if ( resp.status == 'domain blacklisted' ) {
						this.getNext('.err-text').set('html', headerVars.dictionary.su_domain_blacklisted).removeClass('none');
						this.getParent('div.line').getNext('.icon').addClass('error');
					}
				}.bind(this)
			}).send();
		});
	}
}

var validateConfirmEmail = function(el) {
	if (el.getElement('#confirm_email')){
		el.getElement('#confirm_email').addEvent('blur', function () {

			this.getNext('.err-text').set('html', '').addClass('none');
			this.getParent('div.line').getNext('.icon').removeClass('error').removeClass('success');

			var regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
			if ( ! this.get('value').length || ! regex.test(this.get('value') ) ) {
				this.getParent('div.line').getNext('.icon').addClass('error');
				return;
			}

			if ( this.get('value') != el.getElement('#email').get('value') ) {
				this.getParent('div.line').getNext('.icon').addClass('error');
			} else if ( this.get('value') == el.getElement('#email').get('value') ) {
				this.getParent('div.line').getNext('.icon').addClass('success');
			}
		});
	}
}

var validatePassword = function(el) {
	el.getElement('#password').addEvent('blur', function () {

		this.getNext('.err-text').set('html', '').addClass('none');
		this.getParent('div.line').getNext('.icon').removeClass('error').removeClass('success');


		if ( this.get('value') == $$('#p-signup .f-body #username')[0].get('value') ) {
			this.getNext('.err-text').set('html', headerVars.dictionary.username_equal_password).removeClass('none');
			this.getParent('div.line').getNext('.icon').addClass('error');
			return;
		}

		if ( this.get('value').length < 6 ) {
			this.getNext('.err-text').set('html', headerVars.dictionary.su_password_at_least_6).removeClass('none');
			this.getParent('div.line').getNext('.icon').addClass('error');
		}
		else {
			this.getNext('.err-text').set('html', '').addClass('none');
			this.getParent('div.line').getNext('.icon').addClass('success');
		}
	});
};

var validateConfirmPassword = function(el) {
	el.getElement('#password2').addEvent('blur', function () {

		this.getNext('.err-text').set('html', '').addClass('none');
		this.getParent('div.line').getNext('.icon').removeClass('error').removeClass('success');

		if ( this.get('value') != $('password').get('value') ) {
			this.getNext('.err-text').set('html', headerVars.dictionary.su_password_must_equal).removeClass('none');
			this.getParent('div.line').getNext('.icon').addClass('error');
		}
		else {
			this.getNext('.err-text').set('html', '').addClass('none');
			this.getParent('div.line').getNext('.icon').addClass('success');
		}
	});
};