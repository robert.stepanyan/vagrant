Cubix.Blog = {};

Cubix.Blog.containerPosts = 'posts-wrap';
Cubix.Blog.containerComments = 'com-wrap';
Cubix.Blog.containerAddComment = 'c-form-wrap';
Cubix.Blog.scrollWrap = 'blog';

Cubix.Blog.Init = function (data) {	
	if (data.substr(data.length - 1) === '&') {
		data = data.substring(0, data.length - 1);
	}
	
	var data_obj = {};
	
	if ( data.length ) {
		var data_obj = data.parseQueryString();
	}
	
	clearArchiveBold();
	
	Cubix.Blog.Show(data_obj, true);
	
	return false;
};

Cubix.Blog.Show = function(data, allowScroll) {	
	var container = Cubix.Blog.containerPosts;
	
	var myScroll = new Fx.Scroll(window);
	var overlay = new Cubix.Overlay($(container), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	overlay.disable();
	
	if (data.date)
	{
		var b = data.date.split('.');
		
		if ($('a' + b[0] + b[1]))
		{
			$('a' + b[0] + b[1]).set('class', 'bold');
			
			$$('.cur').setProperty('style', '');
		}
		else
			$$('.cur').setProperty('style', 'display: none');
	}
	else
		$$('.cur').setProperty('style', 'display: none');
	
	new Request({
		url: lang_id + '/blog/ajax-posts',
		method: 'get',
		data: data,
		onSuccess: function (resp) {			
			$(container).set('html', resp);
			
			overlay.enable();
			
			if (allowScroll) {
				myScroll.toElement($(Cubix.Blog.scrollWrap));
			}
		}
	}).send();
	
	return false;
};

/****************************************************************/

Cubix.Blog.ShowComments = function(data) {	
	var container = Cubix.Blog.containerComments;
	
	var overlay = new Cubix.Overlay($(container), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	overlay.disable();
		
	new Request({
		url: lang_id + '/blog/ajax-comments',
		method: 'get',
		data: data,
		onSuccess: function (resp) {			
			$(container).set('html', resp);
			
			overlay.enable();
		}
	}).send();
	
	return false;
};

Cubix.Blog.ShowReplies = function(data) {	
	var container = 'replies-' + data.comment_id;
	
	var overlay = new Cubix.Overlay($(container), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	overlay.disable();
		
	new Request({
		url: lang_id + '/blog/ajax-replies',
		method: 'get',
		data: data,
		onSuccess: function (resp) {			
			$(container).set('html', resp);
			
			overlay.enable();
		}
	}).send();
	
	return false;
};

Cubix.Blog.AddComment = function(post_id) {	
	var container = Cubix.Blog.containerAddComment;
	
	var overlay = new Cubix.Overlay($(container), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	var data = {
		post_id: post_id,
		name: $('c-name').get('value'),
		email: $('c-email').get('value'),
		comment: $('c-comment').get('value')
	};
	
	overlay.disable();
		
	new Request({
		url: lang_id + '/blog/ajax-add-comment',
		method: 'post',
		data: data,
		onSuccess: function (resp) {
			$('c-name').removeClass('err');
			$('c-email').removeClass('err');
			$('c-comment').removeClass('err');
			
			if (!$('c-msg').hasClass('none'))
				$('c-msg').addClass('none');
			
			resp = JSON.decode(resp);
						
			if (resp.error)	{
				resp.error.each(function(e) {
					$('c-' + e).addClass('err');
				});
			}
			else if (resp.success) {
				$('c-name').set('value', '');
				$('c-email').set('value', '');
				$('c-comment').set('value', '');
				
				$('c-msg').removeClass('none');
			}
			
			overlay.enable();
		}
	}).send();
	
	return false;
};

Cubix.Blog.ReplyForm = function(comment_id) {	
	var container = 'reply-form-' + comment_id;
			
	new Request({
		url: lang_id + '/blog/ajax-add-reply',
		method: 'get',
		data: {
			comment_id: comment_id
		},
		onSuccess: function (resp) {			
			$(container).set('html', resp);
		}
	}).send();
	
	return false;
};

Cubix.Blog.AddReply = function(comment_id) {	
	var container = 'r-form-wrap-' + comment_id;
	
	var overlay = new Cubix.Overlay($(container), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	var data = {
		comment_id: comment_id,
		name: $('r-name').get('value'),
		email: $('r-email').get('value'),
		comment: $('r-comment').get('value')
	};
	
	overlay.disable();
		
	new Request({
		url: lang_id + '/blog/ajax-add-reply',
		method: 'post',
		data: data,
		onSuccess: function (resp) {
			$('r-name').removeClass('err');
			$('r-email').removeClass('err');
			$('r-comment').removeClass('err');
						
			resp = JSON.decode(resp);
						
			if (resp.error)	{
				resp.error.each(function(e) {
					$('r-' + e).addClass('err');
				});
			}
			else if (resp.success) {
				$('r-form-wrap-' + comment_id).destroy();
				$('r-msg-' + comment_id).removeClass('none');
			}
			
			overlay.enable();
		}
	}).send();
	
	return false;
};

/****************************************************************/

Cubix.BlogHash = {
	_current: '',
	
	init: function () {
		setInterval('Cubix.BlogHash.check()', 100);
	},
	
	check: function () {
		var hash = document.location.hash.substring(1);

		if (hash != this._current) {
			this._current = hash;
			var data = Cubix.BlogHash.Parse();
			Cubix.Blog.Init(data);
		}
	}
};

Cubix.BlogHash.Make = function (filter) {
	var hash = '';
	var sep = ';';
	var value_glue = ',';
	var search_box = $('search-wrap');
	var map = {};
	
	if ( search_box ) {
		var hidden_inputs = search_box.getElements('input[type=hidden]');
		hidden_inputs.each(function(input) {
			if ( input.get('name') && input.get('value').length ) {
				var index = input.get('name').replace('[]', '');
				if ( map[index] === undefined ) map[index] = new Array();
				map[index].append([input.get('value')]);
			}
		});

		var input_texts = search_box.getElements('input[type=text]');
		input_texts.each(function(input) {
			if ( input.get('value').length > 0 ) {
				var index = input.get('name').replace('[]', '');
				if ( map[index] === undefined ) map[index] = new Array();
				map[index].append([input.get('value')]);
			}
		});
	}

	map = Object.merge(map, filter);

	for (var i in map) {
		hash += i + '=' + map[i].join(value_glue) + sep;
	}
	
	return hash;
};

Cubix.BlogHash.SetFilter = function (filter) {
	Cubix.BlogHash.Set(Cubix.BlogHash.Make(filter));

	return false;
};

Cubix.BlogHash.Set = function (hash) {
	if ( hash.length ) {
		document.location.hash = '#' + hash;
	}
	else {
		document.location.hash = '';
	}
	
	return false;
};

Cubix.BlogHash.Parse = function () {
	var hash = document.location.hash.substring(1);
	
	if ( ! hash.length ) {
		return {};
	}
	
	var params = hash.split(';');
	
	var filter = '';
	
	var not_array_params = ['page', 'search', 'date'];
	
	params.each(function (param) {
		var key_value = param.split('=');
		
		var key = key_value[0];
		var val = key_value[1];
		
		if ( val === undefined ) return;
		
		if (key == 'search')
		{
			$('search').set('value', val);
		}
		else if (key == 'date')
		{
			$('date').set('value', val);
			
			
		}
		
		val = val.split(',');
				
		val.each(function(it) {
			filter += key + ( ( ! not_array_params.contains(key) ) ? '[]=' : '=' ) + it + '&';
		});
	});
	
	return filter;
};

window.addEvent('domready', function(){
	Sceon.initSideMenuSimple();
});

function clearArchiveBold () {
	if ($('archive-wrap'))
	{
		var ch = $('archive-wrap').getElements('a');

		ch.each(function(e) {
			e.removeClass('bold');
		});
	}	
}