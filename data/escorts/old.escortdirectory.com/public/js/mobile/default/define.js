
var MCubix = {
    Comments: {},
    Locations: {},
    Escort: {},
    URI: {},
    Private: {}
};

$(document).on('pageinit', function() {
    MCubix.Cookies.delete('page');
    //MCubix.Cookies.set('page', 1, 100);
});

MCubix.URI = {
    setGetParam: function(key, value){
        key = encodeURI(key); value = encodeURI(value);

        var kvp = document.location.search.substr(1).split('&');

        var i=kvp.length; var x;

        while(i--){
            x = kvp[i].split('=');

            if (x[0]==key)
            {
                x[1] = value;
                kvp[i] = x.join('=');
                break;
            }
        }

        if(i<0) {kvp[kvp.length] = [key,value].join('=');}

        document.location.search = kvp.join('&');
    }
};
