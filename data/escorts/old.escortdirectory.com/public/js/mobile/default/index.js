
MCubix.Locations = {

    hrefTo: function( url, param, value, first_page ){

        var url = url.replace( /\&ui-state=dialog/g, '' );

        var val = new RegExp('(\\?|\\&)' + param + '=.*?(?=(&|$))'),
            parts = url.toString().split('#'),
            url = parts[0],
            hash = parts[1]
            qstring = /\?.+$/,
            newURL = url;

        // Check if the parameter exists
        if ( val.test( url ) )
        {
            newURL = url.replace( val, '$1' + param + '=' + value );
        }
        else if ( qstring.test( url ) )
        {
            newURL = url + '&' + param + '=' + value;
        }
        else
        {
            newURL = url + '?' + param + '=' + value;
        }

        if ( hash )
        {
            newURL += '#' + hash;
        }

        if( first_page ){
            newURL = newURL.replace( '?page=', '' );
        }

        var _newURL = newURL.replace('&undefined=undefined', '');
        return  _newURL.replace('?undefined=undefined', '');
    },

    reload: function( url ){
        $.mobile.changePage(
            ( url ) ? url : window.location.href,
            {
                transition              : 'flow',
                showLoadMsg             : true,
                reloadPage              : true
            }
        );
    },

    clear: function( url ){
        var _newURL = url.replace('&undefined=undefined', '');
        console.log(_newURL);
        return  _newURL.replace('?undefined=undefined', '');
    },

    parse: function () {
        var hash = document.location.hash.substring(1);

        if ( ! hash.length ) {
            return {};
        }

        var params = hash.split(';');

        var filter = '';

        var not_array_params = ['segmented', 'top_search_input', 'ts_slug', 'ts_type', 'page', 'agency_sort', 'sort', 'list_type', 'reg', 'name', 'showname', 'agency', 'agency_slug', 'city', 'top_name', 'city_slug', 'phone', 'ethnicity', 'language', 'hair_color', 'hair_length', 'eye_color', 'age_from', 'age_to', 'orientation', 'incall', 'outcall'];

        $.each( params, function ( _key, param ) {
            var key_value = param.split('=');

            var key = key_value[0];
            var val = key_value[1];

            if ( val === undefined ) return;

            val = val.split(',');

            $.each( val, function(_key, it) {
                filter += key + ( ( jQuery.inArray( key, not_array_params ) < 0 ) ? '[]=' : '=' ) + it + '&';
            });
        });

        return parseQueryString( filter + 'ajax=1' );
    }
};

MCubix.Cookies = {
    set: function( cname, cvalue, exdays, domain, path )
    {
        var d = new Date();
        d.setTime( d.getTime() + ( exdays * 24 * 60 * 60 * 1000 ) );
        var expires = "expires=" + d.toGMTString();
        var cookie_str = cname + "=" + cvalue + "; " + expires;

        if ( domain ){
            cookie_str += "; domain=" + encodeURI ( domain );
        }

        if ( path ){
            cookie_str += "; path=" + encodeURI ( path );
        }

        document.cookie = cookie_str;
    },

    get: function( cname )
    {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for( var i=0; i<ca.length; i++ )
        {
            var c = ca[i].trim();
            if ( c.indexOf( name ) == 0 ) return c.substring( name.length,c.length );
        }
        return "";
    },

    delete: function( cname ){
        document.cookie = cname + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }
};

function parseQueryString(url) {
    var result = {};
    var qs = url.substring(url.lastIndexOf("?"));
    qs = qs.replace('?', '');
    var pairs = qs.split('&');
    $.each(pairs, function (i, v) {
        var pair = v.split('=');
        result[pair[0]] = pair[1];
    });
    return result;
}
