MCubix.MyReviews = {
    init: function(){
        var self = this;

        //self.setValidateDateRule();
        //self.validateSearchForm();
        self.slideItem();
    },

    setValidateDateRule: function(){
        $.validator.addMethod("dateFormat",
            function(value, element) {
                return value.match(/^\d{1,2}\/\d{1,2}\/\d{4}$/);
            },
            "Enter a valid format dd/mm/yyyy."
        );
    },

    validateSearchForm: function(){
        var self = this;
        var _form = $('#myReviewsMob');

        var addTicketValidator = _form.validate({
            rules: {
                date_added_f: {
                    //dateFormat: true
                },
                date_added_t: {
                    //dateFormat: true
                },
                meeting_date_f: {
                    //dateFormat: true
                },
                meeting_date_t: {
                    //dateFormat: true
                }
            },
            errorPlacement: function ( error, element ) {
                element.after( error );
            }
        });
    },

    slideItem: function(){
        var slide_btn = $('.slide-item');
        var wrappers = slide_btn.closest('.for-slide').find('.wrapper');

        slide_btn.on('click', function(){
            var wrapper = $(this).closest('.for-slide').find('.wrapper');

            if( wrapper.hasClass('in') ){
                wrapper.removeClass('in');
                $(this).text('Open');
            } else {
                wrappers.not(wrapper).removeClass('in');
                wrapper.addClass('in');

                $('html, body').animate({
                    scrollTop: wrapper.offset().top
                }, 400);
                $(this).text('Close');
            }
        });
    }
};