$(document).on(
    "pageinit", function(){

        var geo_filter = $('#changeGeolocation');
        var geo_btn = geo_filter.find(".geoSwitcher");
        var geo_filter_area = geo_filter.find('.mob-geo-filter');
        var geo_filter_close = geo_filter.find('.close-geolocation');
        var geo_filter_submit = geo_filter.find('.submit-geolocation');
        var input_country = geo_filter.find('#country');
        var input_city = geo_filter.find('#city');

        geo_btn.on("click", function(){
            geo_filter_area.stop().slideToggle( 200 );
        });

        geo_filter_close.on("click", function(){
            geo_filter_area.stop().slideUp( 200 );
        });

        geo_filter_submit.on("click", function(){

            var url,
                geo_country_id = input_country.find(":selected").attr('value');
            var geo_country_slug = input_country.find(":selected").attr('data-slug');

            var geo_city_id = input_city.find(":selected").attr('value');
            var geo_city_slug = input_city.find(":selected").attr('data-slug');

            if( !geo_country_id && !geo_country_slug ){
                input_country.parent().parent().effect( "bounce", { times: 3 }, 300 );
            } else {
                if( !geo_city_id && !geo_city_slug ){
                    url = "/escorts-" + geo_country_slug + "-c" + geo_country_id;
                    window.location.href = url;
                } else if( geo_city_id && geo_city_slug ){
                    url = "/escorts-" + geo_city_slug + "-" + geo_city_id;
                    window.location.href = url;
                }
            }
        });

        //escorts-estonia-c21
        //escorts-buenos-aires-290

        var nat_pic_bttn = $('.natural-pic-box');
        var nat_pic_wrapper = nat_pic_bttn.find('.natural-pic-large');
        var nat_close_bttn = nat_pic_bttn.find('.close');

        nat_pic_bttn.on("click", function(){
            nat_pic_wrapper.stop().slideToggle( 200 );
        });

        nat_close_bttn.on("click", function(){
            geo_filter_area.fadeOut( 300 );
        });
    }
);



MCubix.Escort.Ad = {
    ad_id: null,
    type: null,
    lang: null,
    wrapper: null,

    init: function(ad_id, type, lang){
        if( !type ) type = 'c';
        this.wrapper = $('#mobile-popup-overlay');
        this.ad_id = ad_id;
        this.type = type;
        this.lang = lang;

        this.openPopup();
    },

    openPopup: function(){
        var self = this;
        self.validateForm();
    },

    closeForm: function(){
        var self = this;
        $('#p-contact-to').find('.popup-close-btn').on('click', function(){
            self.wrapper.empty().fadeOut(300);
        });
    },

    refreshCaptcha: function(){
        var d = new Date();
        $('#p-contact-to .cap img').attr("src", "/captcha?" + d.getTime() + '&no_tidy' );
    },

    validateForm: function(){
        var self = this;
        var query = "/contacts/contact-to?id=" + self.ad_id + '&type=' + self.type;
        if( self.lang ){
            query = query + '&lang_id=' + self.lang;
        }

        var jqxhr = $.get( query, function() {

        })
            .fail(function() {
                alert( "error" );
            })
            .always(function(html) {
                self.wrapper.removeClass('in').html( html );
                self.closeForm();
                self.refreshCaptcha();

                $.validator.addMethod(
                    "serverControlCaptcha",
                    function(value, element) {
                        var result = true;
                        if ( value.length < 5 || value.length > 5 ) {
                            $.validator.messages.serverControl = "Required: Captcha code is 5 characters";
                            return false;
                        } else {
                            var myCaptcha = $.ajax({ type: "POST", url: "/escorts/get-captcha-status", data: { hash: value }, async: false }).responseText;
                            if (myCaptcha == "false") {
                                $.validator.messages.serverControl = "Captcha code is invalid, please try the new code.";
                                self.refreshCaptcha();
                                //$("span.refresh-captcha").click();
                                return false;
                            }
                        }
                        return result;
                    },
                    "captcha invalid"
                );

                $('form#contact-form-' + self.ad_id).removeAttr('novalidate').validate({
                    rules: {
                        email: {
                            required: true,
                            email: true
                        },
                        name: {
                            required: true
                        },
                        message: {
                            required: true
                        },
                        captcha: {
                            required: true,
                            serverControlCaptcha: true
                        }
                    },
                    errorPlacement: function ( error, element ) {
                        if ( $( element ).is('#captcha') ) {
                            element.closest('div.form-input').find('span.refresh-captcha').after( error ); // special placement for select elements
                        } else {
                            error.insertAfter( element );  // default placement for everything else
                        }
                    },
                    submitHandler: function(form) {
                        var ad_form_status = $('.send-form-status'),
                            form = $('[id^="contact-form-"]');

                        form.ajaxSubmit({
                            type: "POST",
                            url: "/contacts/contact-to?id=" + self.ad_id + '&type=' + self.type,
                            beforeSubmit: function( formData, jqForm, options ) {
                                ad_form_status.addClass('in').fadeIn(300);
                            },
                            success: function( result ){
                                var req = $.parseJSON( result );

                                if( req.status == 'error' ){
                                    ad_form_status.removeClass('in').addClass('error').text('Server error, please try later !');
                                } else if( req.status == 'success' ){
                                    ad_form_status.removeClass('in').addClass('success').text('Message Sent Successfully !');
                                }

                                ad_form_status.delay(2500).removeClass('error').fadeOut(300, function(){
                                    ad_form_status.empty().removeClass('success');
                                });
                                form.resetForm();
                                self.refreshCaptcha();
                            }
                        });
                    }
                });
            });

    }
};

MCubix.Escort.Data = {

    escort_id: null,
    slider: null,

    init: function( escort_id ){
        this.escort_id = escort_id;
        this.openPopup();
        this.showGalleries();
    },

    openPopup: function(){
        var self = this;

//        var elem = $('div[data-role=page]');
//
//        elem.off('scroll');

        $('div[data-role=popup]').css({position:'fixed',top:'0'});
        var height = $(window).height() - 400;
        var _elm = $( "#scrollable" );
        //$('div[data-role=popup]').animate({ scrollTop: 0 }, 1000);

//        _elm.niceScroll({ cursorcolor:"#F00", cursoropacitymin:0.7, cursoropacitymax:0.7, /*boxzoom:true,*/ touchbehavior:true });
        //self.sliderProcess();

        var elmAttr = "open-form-" + self.escort_id;

        $('a[id^=' + elmAttr + ']').on('click', function(){
            $('.contact-form-overlay').css('display', 'block');
            //var escort_id = $(this).attr('data-id');

            $( 'span.escort-contact-form-' + self.escort_id).css('display', 'block').html('<span class="load-block"><span class="contact-load"></span></span>').fadeIn(300);

            var contact_type = $( 'div#popup-wrapper').find('input[data-rel="contact-type"]').val();
            var lang_id = $( 'div#popup-wrapper').find('input[data-rel="lang-id"]').val();

            self.validateForm( self.escort_id, contact_type, lang_id );

        });

        var elm = $('ul#escort-profile-tabs li').not('li[data-show=to-escort-comments]');

        elm.bind("click", function( e ){
            e.preventDefault();
            e.stopPropagation();

            $('div#escort-popup-tabs').css('display', 'block');
            $('div#popup-escort-image-block').css('display', 'block');
            $('.prev-next-block').css('display', 'block');
            $('div#popup-image-wrapper').css('display', 'none');

            var scroller = $(this).closest('div#popup-wrapper').find('.scrolling-area');
            var scrollBlock = $(this).attr('data-show');
            var _elm = $(this).closest('div#popup-wrapper').find('div.show').find( '[data-id=' + scrollBlock + ']' );

            var scrl = $(this).closest('#container');
            //console.log(scrl.attr('class'));

            var parent = $(this).closest('div#popup-wrapper');

            parent.find('ul#escort-profile-tabs li').removeClass('active');
            $(this).addClass('active');

            var offset = _elm.offset();


            //console.log( offset.top );
            var _toScroll = offset.top;

            $(this).closest('body').stop().animate({
                scrollTop: _toScroll
            }, 300);

            return false;

        });

        $('li[data-show=to-escort-comments]').add('div#popup-escort-image-block img').click(function(){
            $('ul#escort-profile-tabs li').removeClass('active');
            $('li[data-show=to-escort-comments]').addClass('active');

            /*$(this).closest('.ui-page-active').stop().animate({
                scrollTop: 0
            }, 300);*/

            $('div#escort-popup-tabs').css('display', 'none');
            $('div#popup-escort-image-block').css('display', 'none');
            $('.prev-next-block').css('display', 'none');
            $('div#popup-image-wrapper').css('display', 'block');
        });

        var tabs = $('#rcTabs li');
        var tabs_blocks = $('.rTab').add('.cTab');
        tabs.click(function(){
            tabs.removeClass('active');
            tabs_blocks.removeClass('show').addClass('hide');
            $(this).addClass('active');

            if( $(this).hasClass('__comm') ){
                $('.cTab').addClass('show').removeClass('hide');
            } else if( $(this).hasClass('__rev') ){
                $('.rTab').addClass('show').removeClass('hide');
            }
        });
    },

    showGalleries: function(){
        var photos_wrapper = $('#popup-image-wrapper');
        var photos_gallery_buttons = photos_wrapper.find('.gallery-item');
        var photos_gallery_blocks = photos_wrapper.find('.swiper-wrapper');

        photos_gallery_buttons.on('click', function(){
            var button_block = $(this).attr('data-child');
            photos_gallery_buttons.removeClass('current');
            $(this).addClass('current');

            photos_gallery_blocks.css('display', 'none');
            $('.' + button_block).fadeIn(300);
        });
    },

    getComments: function(){
//        var showComment = $.get( "/escorts/contact-form?escort_id=" + this.escort_id, function() {
//
//        })
//            .fail(function() {
//                alert( "error" );
//            })
//            .always(function(html) {
//                $( 'span.escort-contact-form-' + this.escort_id).html( html );
//            });
    },

    sliderProcess: function(){

        var mySwiper = new Swiper('.swiper-container', {
            //loop: true,
            mode:'horizontal',
            grabCursor: true
        });

        $('div[class^=swiper-container] a.prev').on('click', function(e){
            e.preventDefault();
            mySwiper.swipePrev();
        });

        $('div[class^=swiper-container] a.next').on('click', function(e){
            e.preventDefault();
            mySwiper.swipeNext();
        });
    },

    validateForm: function( escort_id, contact_type, lang ){
        var to_user = $('#toUser').val();
        var user_type = $('#userType').val();
        var self = this;
        var query = "/escorts/contact-form?escort_id=" + escort_id + '&type=' + contact_type;

        if( lang == 'en' ) lang = null;
        console.log( lang );
        if( lang ){
            query = query + '&lang_id=' + lang;
        }

        var jqxhr = $.get( query, function() {

        })
            .fail(function() {
                alert( "error" );
            })
            .always(function(html) {
                $( 'span.escort-contact-form-' + escort_id).html( html );

                self.refreshCaptcha();

                $.validator.addMethod(
                    "serverControl",
                    function(value, element) {
                        var result = true;
                        if ( value.length < 5 || value.length > 5 ) {
                            $.validator.messages.serverControl = "Required: Captcha code is 5 characters";
                            return false;
                        } else {
                            var myCaptcha = $.ajax({ type: "POST", url: "/escorts/get-captcha-status", data: { hash: value }, async: false }).responseText;
                            if (myCaptcha == "false") {
                                $.validator.messages.serverControl = "Captcha code is invalid, please try the new code.";
                                $("span.refresh-captcha").click();
                                return false;
                            }
                        }
                        return result;
                    },
                    "captcha invalid"
                );

                $('form#contact-form-' + escort_id).removeAttr('novalidate').validate({
                    rules: {
                        email: {
                            required: true,
                            email: true
                        },
                        name: {
                            required: true
                        },
                        message: {
                            required: true
                        },
                        captcha: {
                            required: true,
                            serverControl: true
                        }
                    },
                    errorPlacement: function ( error, element ) {
                        if ( $( element ).is('#captcha') ) {
                            element.closest('div.form-input').find('span.refresh-captcha').after( error ); // special placement for select elements
                        } else {
                            error.insertAfter( element );  // default placement for everything else
                        }
                    },
                    submitHandler: function(form) {
                        form.submit();
                    }
                });

                var attrVal = "contact-submit-" + escort_id;

                $('input[id=' + attrVal + ']').on( "click", function(){

                    var form = $(this).closest('form');
                    var url = form.attr('data-action');
                    var type = $(this).siblings('contact-submit-input[name="contact-type"]').val();

                    if( form.valid() ){
                        form.ajaxSubmit({
                            type: "POST",
                            url: '/contacts/contact-to?ajax=1&id=' + to_user + '&type=' + user_type,
                            beforeSubmit: function( formData, jqForm, options ) {
                                $( 'div.form-overlay-' + self.escort_id).removeClass('hide').addClass('show');
                                $( 'img.form-loading-' + self.escort_id).removeClass('hide').addClass('show');
                            },
                            success: function( result ){
                                var req = $.parseJSON( result );
                                var form = $('form.contact-form');
                                var msgArea = form.find( 'p.form-message-' + self.escort_id );

                                $( 'div.form-overlay-' + self.escort_id).removeClass('show').addClass('hide');
                                $( 'img.form-loading-' + self.escort_id).removeClass('show').addClass('hide');

                                if( req.status == 'error' ){
                                    msgArea.removeClass('success').addClass('error').text('Server error, please try later !');
                                    msgArea.show('500').delay(4000).hide('500');
                                } else if( req.status == 'success' ){
                                    ga('send', 'event', 'MessageForm', 'sent');
                                    msgArea.removeClass('error').addClass('success').text('Message Sent Successfully !');
                                    msgArea.show('500').delay(4000).hide('500');
                                }

                                form.resetForm();
                            }
                        });
                    } else {
                        form.submit();
                    }
                });

                $('.contact-form').find('.popup-close-btn').on('click', function(){
                    $( this).closest('span[class^=escort-contact-form-]').add('.contact-form-overlay').fadeOut(300);
                });
            });
    },

    refreshCaptcha: function(){
        $('span.refresh-captcha').on('click', function(){
            var d = new Date();
            $('img#mail-captcha').attr("src", "/captcha?" + d.getTime() + '&no_tidy' );
        });

    }/*,

    loadPagingResults: function( pageCount ){
        $(document).on(
            "pageinit", function(){
                var elm = $('div[data-role=page]');
                var self = this;

                var lastScroll = 0;

                elm.off('scroll');
                    elm.on( 'scroll', function() {

                        var st = $(this).scrollTop();

                        (function(DOMParser) {
                            "use strict";
                            var DOMParser_proto = DOMParser.prototype
                                , real_parseFromString = DOMParser_proto.parseFromString;

                            // Firefox/Opera/IE throw errors on unsupported types
                            try {
                                // WebKit returns null on unsupported types
                                if ((new DOMParser).parseFromString("", "text/html")) {
                                    // text/html parsing is natively supported
                                    return;
                                }
                            } catch (ex) {}

                            DOMParser_proto.parseFromString = function(markup, type) {
                                if (/^\s*text\/html\s*(?:;|$)/i.test(type)) {
                                    var doc = document.implementation.createHTMLDocument("")
                                        , doc_elt = doc.documentElement
                                        , first_elt;

                                    doc_elt.innerHTML = markup;
                                    first_elt = doc_elt.firstElementChild;

                                    if (doc_elt.childElementCount === 1
                                        && first_elt.localName.toLowerCase() === "html") {
                                        doc.replaceChild(first_elt, doc_elt);
                                    }

                                    return doc;
                                } else {
                                    return real_parseFromString.apply(this, arguments);
                                }
                            };
                        }(DOMParser));
                        //console.log(st);

                        if( $(this).hasClass('ui-page-active') ){
                            //console.log( $(this).find('div.ui-footer').offset().top + ' / ' + $(document).height() );
                            var loadData = $('div[data-id="load-data"]');

                            if( ( $(this).find('div.ui-footer').offset().top < ( $(document).height() ) ) && ( st > lastScroll ) ) {
                                loadData.stop( true, true ).slideDown(300);

                                var pageElm = $('.scrollPageNumber');
                                var maxPage = Number( pageCount );

                                if( ( Number( pageElm.val() ) < maxPage + 1 ) ){
                                    pageElm.val( Number( pageElm.val() ) + 1 );
                                    window.location.hash = '#page=' + pageElm.val() + ';segmented=1;';
                                }

                                //var page = Number( pageElm.val() );

                                //console.log(maxPage);

                                if( pageElm.val() < maxPage + 1 ){
                                    var hash_all = window.location.hash.substring(1);
                                    var hash = hash_all.replace(';', '&');
                                    hash = hash.replace(';', '&');
                                    //var url = 'http://' + window.location.hostname + window.location.pathname + '?s=1';
                                    var url = MCubix.Locations.clear( document.URL, 's', 1, false );
                                }

                                if( Number( pageElm.val() ) < Number( maxPage + 1 ) && Number( pageElm.val() ) != 1 ){

                                    var contentCont = $('div.ui-page-active').find('.escorts-list');
                                    var data = MCubix.Locations.parse();

                                    var c_page = MCubix.Cookies.get('page');
                                    if( Number( c_page ) < Number( data.page ) ){
                                        MCubix.Cookies.set( 'page', data.page, 1, document.location.hostname, document.location.pathname );
                                    }

                                    console.log( data );

                                    $.post( url, data, function() {}, "json" )
                                        .always( function( html ) {
                                            var parser = new DOMParser();
                                            var contentParse = parser.parseFromString( html.responseText, "text/html" );
                                            var pageContent = contentParse.getElementsByClassName('escorts-list');

                                            //if( pageContent[0].innerHTML.length > 100 )
                                            //alert(pageContent[0].innerHTML);

                                            contentCont.html( contentCont.html() + pageContent[0].innerHTML );
                                        })
                                        .complete( function(){
                                            loadData.html('Complete...');
                                            if ( contentCont.hasClass('ui-listview') ){
                                                contentCont.listview('refresh');
                                            } else {
                                                contentCont.trigger('create');
                                            }

                                            MCubix.Escort.Data.openPopup();
                                            window.location.hash = '#page=' + pageElm.val() + ';segmented=0;';

                                            loadData.slideUp(300, function(){
                                                loadData.html('Loading Data...');
                                            });
                                        });
                                } else {

                                    pageElm.val( Number( pageElm.val() ) - 1 );
                                    window.location.hash = '#page=' + pageElm.val() + ';segmented=1;';

                                    loadData.delay(800).slideUp({
                                        duration: 1000,
                                        start: function(){ loadData.html('No results found !'); },
                                        complete: function(){ loadData.html('Loading Data...'); }
                                    });
                                }
                            }
                        }

                        lastScroll = st;
                    });
            }
        );
    }*/
};