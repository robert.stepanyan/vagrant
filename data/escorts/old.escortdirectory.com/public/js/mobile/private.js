MCubix.Private = {
    /******************* Profile page *********************/
    changeAboutLng: function(){
        var lng_tab = $('#about-me-profile .picker-tabs .lng-picker span');

        lng_tab.on('click', function(){
            $('#about-me-profile .picker-tabs .lng-picker span').removeClass('in');
            $(this).addClass('in');
            var sel = $(this).data('lang');
            $('.container-about-field').removeClass('in');
            $('.container-about-field.' + sel).addClass('in').focus();
        });
    },

    changeServicesLng: function(){
        var lng_tab = $('#services-profile .picker-tabs .lng-picker span');

        lng_tab.on('click', function(){
            $('#services-profile .picker-tabs .lng-picker span').removeClass('in');
            $(this).addClass('in');
            var sel = $(this).data('lang');
            $('.container-services-field').removeClass('in');
            $('.container-services-field.' + sel).addClass('in').focus();
        });
    },

    hasSubmenu: function( li_elm ){
        $(li_elm).each(function(i, elm){
            if( $(elm).has('ul').length ){
                //console.log($(elm).find('ul'));
                $(elm).addClass('has_submenu');
            }
            //console.log(i);
        });
    },

    openCollapse: function( ul_id, li_height, interval ){
        var self = this;
        if( !interval ) interval = 150;
        var elm = $('#' + ul_id + ' > li');
        self.hasSubmenu( elm );

        elm.find('a').on('click', function(){
            var li_elm = $(this).closest('li');

            if( li_elm.has('ul').length ){
                if( !li_elm.hasClass('active') ) {
                    elm.stop().animate({ 'height' : li_height }, interval).removeClass('active').find('ul').hide();
                    var gen_height = li_elm.find('ul').show().outerHeight();
                    li_elm.stop().animate({'height': ( gen_height + li_height )}, interval).addClass('active');
                } else {
                    li_elm.stop().animate({'height': 50 }, interval).removeClass('active');
                    li_elm.find('ul').hide();
                }
            }
        })
    },

    validateFormBiography: function(){
        var form = $('#bio-profile');
        form.removeAttr('novalidate').validate({
            rules: {
                showname: {
                    required: true,
                    minlength: 5,
                    maxlength: 30
                },
                gender: {
                    required: true
                }
            },
            errorPlacement: function ( error, element ) {
                error.insertAfter( element );
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    },

    gallerySortable: function(){
        var form = $('#gallery-profile');
        var ids = [];

        $('.p-actions input[type=checkbox]').each(function (index, element) {
            ids.push( $(this).val() );
        });

        form.ajaxSubmit({
            type: "get",
            url: '/private-v2/photos',
            data: { a: 'sort', photo_id: ids, escort: $('#escort_id').val() },
            beforeSubmit: function( formData, jqForm, options ) {
                form.addClass('in');
            },
            success: function( result ){
                form.removeClass('in');
            }
        });
    },

    gallerySetMain: function( elm, text_setmain, text_main ){
        var photo_id = elm.data('photo');
        var ids = [ photo_id ];
        var parent_li = elm.closest('li');

        $.ajax({
            method: "GET",
            url: '/private-v2/photos',
            data: { a: 'set-main', photo_id: ids, escort_id: $('#escort_id').val() },
            beforeSend: function (formData, jqForm, options) {
                parent_li.addClass('load');
            },
            success: function (result) {
                parent_li.removeClass('load').closest('ul').find('li').removeClass('main');
                parent_li.closest('ul').find('.set-main').removeClass('off').addClass('on-set-main').text(text_setmain);
                parent_li.removeClass('load').addClass('main').find('.set-main').addClass('off').removeClass('on-set-main').text(text_main);
            }
        });
    },

    galleryDeletePhoto: function( elm ){
        var photo_id = elm.data('photo');
        var ids = [ photo_id ];
        var parent_li = elm.closest('li');

        //?a=delete&photo_id[0]=327857&escort=9237

        $.ajax({
            method: "GET",
            url: '/private-v2/photos',
            data: { a: 'delete', photo_id: ids, escort_id: $('#escort_id').val() },
            beforeSubmit: function (formData, jqForm, options) {
                parent_li.addClass('load');
            },
            success: function (result) {
                parent_li.removeClass('load').fadeOut(350);
            }
        });
    }

    //checkForNewPms: function(){
    //
    //},
    //
    //createMssgDom: function( data ){
    //    var html = '<div class="row self">';
    //            html += '<div class="mess-avatar"></div>';
    //                html += '<div class="message-body"><span class="arr"></span>';
    //                    html += '<div class="i">';
    //                        html += '<div class="ii">';
    //                            html += ' <div class="message-info">';
    //                                html += '<span class="name"><?= $name ?></span>';
    //                                html += '<span class="date">( <?= date('Y-m-d', $message['date']) ?> | <?= date('i:s', $message['date']) ?> )</span><br/>';
    //                            html += '</div>';
    //                            html += '<p class="message"><?= $message['body'] ?></p>';
    //                        html += '</div>';
    //                    html += '</div>';
    //                    html += '<div class="mess-arrow"></div>';
    //                html += '</div>';
    //                html += '<div class="toClear"></div>';
    //            html += '</div>';
    //}
};

$(document).on(
    "pageinit", function(){
        MCubix.Private.changeAboutLng();
        MCubix.Private.changeServicesLng();
        MCubix.Private.openCollapse('escort-dashboard', 50 );
        MCubix.Private.validateFormBiography();
    });