var _log = function (data) {
	if ( console && console.log != undefined ) {
		console.log(data);
	}
};

var _st = function (object) {
	var parts = object.split('.');
	if ( parts.length < 2 ) {
		return _log('invalid parameter, must contain filename with extension');
	}

	var ext = parts[parts.length - 1];

	var base_url = 'http://st.escortdirectory.com',
		prefix = '';

	switch ( ext ) {
		case 'jpg':
		case 'gif':
		case 'png':
			prefix = '/img/';
			break;
		case 'css':
			prefix = '/css/';
			break;
		case 'js':
			prefix = '/js/';
			break;
		default:
			return _log('invalid extension "' + ext + '"');
	}

	url = base_url + prefix + object;

	return url;
};

var Geography = {
	loadCities: function ( city, country, lang_id, city_id, hidden) {

        if( lang_id == 'en' ){
            lang_id = '';
        } else {
            lang_id = '/' + lang_id;
        }

		if ( !hidden ) {
			hidden = false;
		}

		var selected;
		if ( hidden ) {
			selected = country.val();
		}
		else {
			selected = country.find(":selected");
		}

		if ( ! selected.length ) return;

		if ( ! hidden ) {
			var country_id = selected.val();
		}
		else {
			var country_id = selected;
		}

		if ( ! country_id ) {
			city.empty();
			new Element('option', { value: '', html: '- city -' }).inject(city);
			return;
		}

		$(country, city).attr('disabled', 'disabled');

        var request = $.ajax({
            url: lang_id + '/geography/ajax-get-cities',
            method: 'GET',
            data: { country_id: country_id }
        });

        request.done(function( resp ) {
            resp = jQuery.parseJSON(resp);
            resp = resp.data;
            if ( ! resp ) return;
            city.empty();

            city.append( '<option value="">- city -</option>' );

            $.each( resp, function (k, c) {

                var sel = ( city_id == c.id ) ? ' "selected="selected"' : '';
                city.append(
                    '<option data-slug="' + c.slug + '" value="' + c.id + '"' + sel + '">' + c.title + '</option>' );
                city.selectmenu('refresh', true);
            });

            $(country, city).attr('disabled', null);

			$('#locations').find('.location').remove();
			exists = [];

            //$(country, city).fireEvent('pakistan');
        });
	}
};