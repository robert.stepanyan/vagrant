Cubix.PReviews = {};

Cubix.PReviews.RevComSet = function() {
    var overlay = new Cubix.Overlay($('dashboard'), {color: '#fff', opacity: .6, loader: _st('loader-small.gif'), position: '50%', offset: {bottom: 0, top: 0}});
    overlay.disable();

    var myScroll = new Fx.Scroll(window);

    new Request({
        url: lang_id + '/reviews/get-rev-com-set',
        method: 'get',
        onSuccess: function (resp) {
            $('ajax-content').set('html', resp);

            new Mooniform($$('#ajax-content select'));

            overlay.enable();

            Cubix.PReviews.SelectEscort();

            myScroll.toElement($('ajax-content'));
        }
    }).send();

    return false;
}

Cubix.PReviews.SelectEscort = function() {
    if ($('escort'))
    {
        $('escort').addEvent('change', function() {
            var overlay = new Cubix.Overlay($('ajax-content'), {color: '#fff', opacity: .6, loader: _st('loader-small.gif'), position: '50%', offset: {bottom: 0, top: 0}});
            overlay.disable();

            var es = $('escort').get('value');

            if (es.length > 0)
            {
                new Request({
                    url: lang_id + '/reviews/get-rev-com-set-agency?e_id=' + es,
                    method: 'get',
                    onSuccess: function (resp) {
                        if (resp.length > 0)
                        {
                            resp = JSON.decode(resp);

                            var dis_rev = resp.dis_rev;
                            var dis_com = resp.dis_com;

                            if (dis_rev)
                            {
                                $('r-b').set('class', 'off');
                                $('r-h').set('value', 0);
                            }
                            else
                            {
                                $('r-b').set('class', 'on');
                                $('r-h').set('value', 1);
                            }

                            if (dis_com)
                            {
                                $('c-b').set('class', 'off');
                                $('c-h').set('value', 0);
                            }
                            else
                            {
                                $('c-b').set('class', 'on');
                                $('c-h').set('value', 1);
                            }

                            $('e-h').set('value', es);

                            $$('.acts').removeClass('none');

                            overlay.enable();
                        }
                    }
                }).send();
            }
            else
            {
                $$('.acts').addClass('none');
                overlay.enable();
            }
        });
    }
}

Cubix.PReviews.SetPrivacy = function(act) {
    var overlay = new Cubix.Overlay($('ajax-content'), {color: '#fff', opacity: .6, loader: _st('loader-small.gif'), position: '50%', offset: {bottom: 0, top: 0}});
    overlay.disable();

    new Request({
        url: lang_id + '/reviews/set-rev-com-set',
        method: 'post',
        data: {
            act: act,
            val: $(act + '-h').get('value'),
            e_id: $('e-h').get('value')
        },
        onSuccess: function (resp) {
            if ($('escort'))
            {
                $('escort').fireEvent('change');
            }
            else
            {
                Cubix.PReviews.RevComSet();
            }

            overlay.enable();
        }
    }).send();

    return false;
}

Cubix.PReviews.initAddForm = function() {
    new Mooniform($$('select'));

    var m_date = new DatePicker($('m_date'), {
        pickerClass: 'datepicker',
        positionOffset: { x: -10, y: 0 },
        allowEmpty: true,
        format: 'j M Y',
        months: Cubix.PReviews.months,
        days: Cubix.PReviews.days,
        maxDate: { date: Cubix.PReviews.curdate, format: 'd m Y' }
    });

    $('meeting_place').addEvent('change', function() {
        if ($('meeting_place').get('value') != '')
        {
            var text = $('meeting_place').getSelected().get('text');
            $('t_meeting_place').set('value', text);
        }
    });

    $('duration').addEvent('blur', function() {
        Cubix.PReviews.addDuration();
    });

    $('duration_unit').addEvent('change', function() {
        Cubix.PReviews.addDuration();
    });

    m_date.addEvent('select', function(d) {
        var text = this.format(d, this.options.format);

        $('t_meeting_date').set('value', text);
    });
}

Cubix.PReviews.addDuration = function() {
    if ($('duration').get('value') != '')
    {
        var text = $('duration').get('value');
        var text2 = $('duration_unit').getSelected().get('text');
        $('t_meeting_duration').set('value', text + ' ' + text2);
    }
}

Cubix.PReviews.load = function(data) {
    var overlay = new Cubix.Overlay($('dashboard'), {color: '#fff', opacity: .6, loader: _st('loader-small.gif'), position: '50%', offset: {bottom: 0, top: 0}});
    overlay.disable();

    var myScroll = new Fx.Scroll(window);

    new Request({
        url: lang_id + '/reviews/my-reviews',
        method: 'get',
        data: data,
        onSuccess: function (resp) {
            $('ajax-content').set('html', resp);

            new Mooniform($$('select'));

            date_meet_f = new DatePicker($('meeting_date_f'), {
                pickerClass: 'datepicker',
                positionOffset: { x: -10, y: 0 },
                allowEmpty: true,
                format: 'j M Y',
                months: Cubix.PReviews.months,
                days: Cubix.PReviews.days
            });

            date_meet_t = new DatePicker($('meeting_date_t'), {
                pickerClass: 'datepicker',
                positionOffset: { x: -10, y: 0 },
                allowEmpty: true,
                format: 'j M Y',
                months: Cubix.PReviews.months,
                days: Cubix.PReviews.days
            });

            date_add_f = new DatePicker($('date_added_f'), {
                pickerClass: 'datepicker',
                positionOffset: { x: -10, y: 0 },
                allowEmpty: true,
                format: 'j M Y',
                months: Cubix.PReviews.months,
                days: Cubix.PReviews.days
            });

            date_add_t = new DatePicker($('date_added_t'), {
                pickerClass: 'datepicker',
                positionOffset: { x: -10, y: 0 },
                allowEmpty: true,
                format: 'j M Y',
                months: Cubix.PReviews.months,
                days: Cubix.PReviews.days
            });

            overlay.enable();
            myScroll.toElement($('ajax-content'));

            if ($$('.sort'))
            {
                $$('.sort').addEvent('click', function() {
                    var self = this;

                    Cubix.PReviews.SetFilter({f_d: self.get('rel')});

                    return false;
                })
            }
        }
    }).send();

    return false;
}

Cubix.PReviews.open = function(rid) {
    $$('.detailed').addClass('none');
    $$('.hi').addClass('none');
    $$('.op').removeClass('none');

    $('r' + rid).removeClass('none');
    $('o' + rid).addClass('none');
    $('h' + rid).removeClass('none');

    var myScroll = new Fx.Scroll(window);
    myScroll.toElement($('scrl' + rid));

    return false;
}

Cubix.PReviews.hide = function(rid) {
    $('r' + rid).addClass('none');
    $('h' + rid).addClass('none');
    $('o' + rid).removeClass('none');

    return false;
}

Cubix.PReviews.SetFilter = function(data) {
    if ($('escort_id') && $('escort_id').get('value').length > 0)
        data.escort_id = $('escort_id').get('value');

    if ($('showname') && $('showname').get('value').length > 0)
        data.showname = $('showname').get('value');

    if ($('member') && $('member').get('value').length > 0)
        data.member = $('member').get('value');

    if ($('city').get('value').length > 0)
        data.city = $('city').get('value');

    if ($('commented').get('value').length > 0)
        data.commented = $('commented').get('value');

    if ($('date_added_f').get('value').length > 0)
        data.date_added_f = $('date_added_f').get('value');

    if ($('date_added_t').get('value').length > 0)
        data.date_added_t = $('date_added_t').get('value');

    if ($('meeting_date_f').get('value').length > 0)
        data.meeting_date_f = $('meeting_date_f').get('value');

    if ($('meeting_date_t').get('value').length > 0)
        data.meeting_date_t = $('meeting_date_t').get('value');

    var f_d = data.f_d;
    delete data.f_d;

    if (f_d && f_d.length > 0)
    {
        var arr = new Array();
        arr = f_d.split('|');

        var sort_field = arr[0];
        var sort_dir = '';

        if (arr[1] == undefined)
            sort_dir = 'desc';
        else
            sort_dir = arr[1];

        sort_dir = sort_dir.toLowerCase();

        if ( sort_dir == 'asc' ) {
            sort_dir = 'desc';
        }
        else {
            sort_dir = 'asc';
        }

        data.sort_field = sort_field;
        data.sort_dir = sort_dir;
    }

    Cubix.PReviews.load(data);

    return false;
}

Cubix.PReviews.AddComment = function(rid, com_text) {
    var overlay = new Cubix.Overlay($('container'), {color: '#000', opacity: .6, has_loader: false, position: '50%', offset: {bottom: 0, top: 0}});
    overlay.disable();

    new Request({
        url: lang_id + '/reviews/add-comment',
        method: 'get',
        data: {
            review_id: rid
        },
        onSuccess: function (resp) {
            $('add-com-' + rid).set('html', resp);

            $$('.x, .cancel').addEvent('click', function(e) {
                e.stop();

                $$('.add-comment-popup').destroy();
                overlay.enable();
            });

            $$('.save').addEvent('click', function(e) {
                e.stop();

                new Request({
                    url: lang_id + '/reviews/add-comment',
                    method: 'post',
                    data: {
                        comment: $('com').get('value'),
                        review_id: rid
                    },
                    onSuccess: function (resp) {
                        resp = JSON.decode(resp);

                        var status = resp.status;

                        if (status == 'error')
                        {
                            $('com').addClass('err');
                        }
                        else
                        {
                            $$('.add-comment-popup').destroy();
                            overlay.enable();

                            $('add-com-text-' + rid).set('html', com_text);
                            /**/
                        }
                    }
                }).send();
            })
        }
    }).send();

    return false;
}

Cubix.PReviews.Answer = function(rid, answer) {
    var overlay = new Cubix.Overlay($('my-reviews'), {color: '#000', opacity: .6, has_loader: false, position: '50%', offset: {bottom: 0, top: 0}});
    overlay.disable();

    new Request({
        url: lang_id + '/reviews/answer',
        method: 'post',
        data: {
            review_id: rid,
            answer: answer
        },
        onSuccess: function (resp) {
            resp = JSON.decode(resp);

            var status = resp.status;

            if (status == 'ok')
            {
                $('pend-' + rid).destroy();

                var c = $('p-c').get('html');
                c = parseInt(c) - 1;
                $('p-c').set('html', c);
            }

            overlay.enable();
        }
    }).send();

    return false;
}