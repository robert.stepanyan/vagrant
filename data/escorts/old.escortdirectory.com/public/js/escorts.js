/* --> Escorts */
Sceon.Escorts = {};

Sceon.scrolls_collection = [];
Sceon.all_scroll = [];


Sceon.Escorts.s_page = 1;
Sceon.Escorts.no_escorts = false;

Sceon.Escorts.ss;
Sceon.Escorts.scroll_offset = 1500;
Sceon.Escorts.finished = true;

Sceon.Escorts.scrollSpyLoadInit = function() {
	return false;

	if ( ! $('escorts') ) return;

	var finish = false;

	var req = function(){

		Sceon.Filter.Set({ page: [Sceon.Escorts.s_page], segmented: ['1'] });

		finish = true;
	};

	//console.log('started' + (window.getScrollSize().y - window.getSize().y - Sceon.Escorts.scroll_offset));
	Sceon.Escorts.ss = new ScrollSpy({
		min: window.getScrollSize().y - window.getSize().y - Sceon.Escorts.scroll_offset,
		onEnter: function(p,state,enters) {

			if ( ! Sceon.Escorts.finished || ! $('escorts') ) return;

			//console.log('enter');
			if ( p.y > window.getScrollSize().y - window.getSize().y - Sceon.Escorts.scroll_offset && ! Sceon.Escorts.no_escorts ) {

				Sceon.Filter.Set({ page: [++Sceon.Escorts.s_page], segmented: ['1'] });

				container = $('escorts');

				getSpinner().inject(container, 'after');

				Sceon.Escorts.finished = false;
			}
		},
		onLeave: function(position,state,leaves) {

		},
		onScroll: function(position,state,leaves) {


		},
		container: window
	});

	//console.log(Sceon.Escorts.ss.options.min);

	var getSpinner = function () {
		if ( ! $$('div.spinner').length  ) {
			var el = new Element('div', { 'class': 'spinner' });
			//new Element('span'/*, { class:'an', html: 'Loading more escorts... '}*/).inject(el);

			return el;
		}
		return $$('div.spinner')[0];
	};
};

Sceon.Escorts.GetRequestUrl = function (url) {
	var uri = new URI(document.location.href);

	uri = uri.get('directory') + uri.get('file');
	//uri = Sceon.Escorts.url;
	uri = uri.replace(/\/$/, '');

	if ( url ) {
		url = uri + '/' + url;
	} else {
		url = uri;
	}

	if ( -1 == url.indexOf('?') ) {
		url += '?';
	}
	else {
		url += '&';
	}

	url += 'ajax';

	if ( url == '/escorts/?ajax' ) url = '?ajax'; //TO AVOID REDIRECTS IN FILTERING

	return url;
};

Sceon.Escorts.Load = function (url, data, show_overlay) {
	var load_url = Sceon.Escorts.GetRequestUrl();

	if ( load_url.test('/escort/') || load_url.test('/agency/') ) {

		Sceon.loadEscortProfile(load_url);

		return false;
	}

	var container = $('body');
	var e_container = $('escorts');

	var data_obj = {};
	if ( data.length ) {
		var data_obj = data.parseQueryString();

		if ( data_obj.segmented == 0 || ! data_obj.segmented ) {
			var f_overlay = Sceon.createOverlay($('container'));
			f_overlay.show();
		}
	}
	

	var side_bar_countries = $('side-bar-countries');

	if ( show_overlay ) {
		var e_overlay = Sceon.createOverlay($('container'));
		e_overlay.show();
	}	

	var myScroll = new Fx.Scroll(document.getElement('body'));

	var url_ex = "";

	if ( data_obj.sort || data_obj.agency_sort ) {
		url_ex = "&s=1";
	}

	var myScroll = new Fx.Scroll(document.getElement('body'));

	new Request.JSON({	
		url: load_url + url_ex,
		method: 'post',
		data: data,
		onSuccess: function (resp) {
			if ( resp.escorts_count > 0 ) {
				if ( String(data).test('page=') ) {

					if ( data_obj.segmented == 1 ) {

						$('p_id').set('value', '');

						if ( e_container.getElements('div.group-head').length > 0 && resp.body.length ) {
							var last_el = e_container.getElements('div.group-head').getLast();
							var last_title = last_el.get('html');

							var last_el_new = e_container.getElements('p.escort-date-separator').getLast();
							if ( last_el_new ) {
								var last_title_new = last_el_new.get('html');
							}

							/*var last_el_type = e_container.getElements('p.escort-type-separator').getLast();
							if ( last_el_type ) {
								var last_title_type = last_el_type.get('html');
							}*/
							
							/*if ( last_el.getNext('div.simple-sep') ) {
								var last_sep = 	last_el.getNext('div.simple-sep');
							}*/


							var is_agency_escorts = e_container.getElements('.escort-type-separator.agency-escort').length;
							var is_indep_escorts = e_container.getElements('.escort-type-separator.indep-escort').length;

							var last_segment = new Element('div', {
								'class': 'last-segment'
							}).inject(e_container, 'bottom').set('html', resp.body);
							

							if ( e_container.getElements('.escort-type-separator').length ) {
								
								if ( is_agency_escorts ) {
									var agency_sep = last_segment.getElements('.escort-type-separator.agency-escort');
									agency_sep.getNext('div.clear').destroy();
									agency_sep.getPrevious('div.clear').destroy();
									agency_sep.destroy();
								}

								if ( is_indep_escorts ) {
									var indep_sep = last_segment.getElements('.escort-type-separator.indep-escort');
									indep_sep.getNext('div.clear').destroy();
									indep_sep.getPrevious('div.clear').destroy();
									indep_sep.destroy();
								}
							}

							last_segment.getElements('div.group-head').each(function(el){
								if ( el.get('html') == last_title ) {								

									el.addClass('none');

								}
							});

							if ( last_segment.getElements('p.escort-date-separator').length ) {
								last_segment.getElements('p.escort-date-separator').each(function(el){
									if ( el.get('html') == last_title_new ) {								

										el.addClass('none');

									}
								});
							}


							/*if ( last_segment.getElements('p.escort-type-separator').length ) {
								last_segment.getElements('p.escort-type-separator').each(function(el){
									if ( el.get('html') == last_title_type ) {								

										el.addClass('none');

									}
								});
							}*/


							last_segment.removeClass('last-segment');
						} else {

							var is_agency_escorts = e_container.getElements('.escort-type-separator.agency-escort').length;
							var is_indep_escorts = e_container.getElements('.escort-type-separator.indep-escort').length;

							if ( e_container.getElements('.escort-type-separator').length ) {
								var last_segment = new Element('div', {
									'class': 'last-segment'
								}).inject(e_container, 'bottom').set('html', resp.body);

								if ( is_agency_escorts ) {
									var agency_sep = last_segment.getElements('.escort-type-separator.agency-escort');
									agency_sep.getNext('div.clear').destroy();
									agency_sep.getPrevious('div.clear').destroy();
									agency_sep.destroy();
								}

								if ( is_indep_escorts ) {
									var indep_sep = last_segment.getElements('.escort-type-separator.indep-escort');
									indep_sep.getNext('div.clear').destroy();
									indep_sep.getPrevious('div.clear').destroy();
									indep_sep.destroy();
								}

								e_container.set('html', e_container.get('html') + last_segment.get('html'));								
								e_container.getElements('.last-segment').destroy();

							} else {

								e_container.set('html', e_container.get('html') + resp.body);
							}

						}



					} else {
						container.set('html', resp.body);

						myScroll.toTop();
					}

					Sceon.Escorts.s_page = data_obj.page;
				} else {
					Sceon.Escorts.s_page = 1;
					container.set('html', resp.body);
				}

				Sceon.Escorts.no_escorts = false;

			} else {
				Sceon.Escorts.no_escorts = true;
				if ( ! data_obj.segmented ) {
					container.set('html', resp.body);
				}
				
				/*if ( String(data).test('name=') ) {
					if ( String(data).test('page=') ) {
						e_container.set('html', e_container.get('html') + resp.body);
					} else {
						container.set('html', resp.body);
					}
				}*/				
			}

			if ( e_container ) {
				if ( e_container.getElements('.nearest-city-escorts').length ) {
					Sceon.Escorts.no_escorts = true;				
				}
			}


			if ( (data_obj.segmented == 0 || ! data_obj.segmented) && f_overlay ) {
				f_overlay.destroy();
			}

			if ( show_overlay ) {
				e_overlay.destroy();
			}


			if (resp.head_title) {
				window.document.title = resp.head_title;
			}

			if ( data_obj.segmented == 1 ) {
				Sceon.Filter.Set({ page: [Sceon.Escorts.s_page], segmented: ['0'] });
				Sceon.Escorts.finished = true;
			} else {

				//Sceon.initSearchBox();
				//Sceon.all_scroll.terminate();
				Sceon.initSorting();
				Sceon.initShowAllAgencyEscorts();
				Sceon.initAgencySorting();
				side_bar_countries.set('html', resp.side_bar);
				Sceon.initSideMenu();
				Sceon.initTopMenu();
			}

			Sceon.initSearchBox();
			Sceon.initAgencies();
			Sceon.initNewestSlider();
			Sceon.initProfileThumbs();
			Sceon.initListType();
			Sceon.initTips();
			Sceon.initAgencyTips();

			Sceon.PhotoRoller.Init();

			Sceon.initContactUsLinks();

			//f_overlay.enable();
			//s_overlay.enable();
			//search_overlay.enable();
			//c_overlay.enable();			
			
			
			Slimbox.scanPage();

			//myScroll.toTop();

			container.getElements('div.spinner').destroy();

			Sceon.Escorts.scrollSpyLoadInit();



			//Sceon.Escorts.ss.options.min = window.getScrollSize().y - window.getSize().y - Sceon.Escorts.scroll_offset;
		
			var e_position = $('p_id');	

			if ( e_position ) {

				if ( e_position.get('value') ) {
					var scroll_el = $('profile-' + e_position.get('value'));

					if ( data_obj.list_type == 'gallery' ) {
						scroll_el = scroll_el.getParent('div.escort-v2');
						//console.log(scroll_el);
					}

					var myFx = new Fx.Scroll(window,{
						offset: {
							'x': 0,
							'y': 0
						}
					});
					myFx.toElement(scroll_el);
				}
			}

		// hide or show filter	
		if(resp.availableFilters.real_pics){
			$$('.sub-search #real-pics').getParent('.inner').show();
			$$('.sub-search-as #real-pics-as').getParent('.inner').show();
		}else{
			$$('.sub-search #real-pics').getParent('.inner').hide();
			$$('.sub-search-as #real-pics-as').getParent('.inner').hide();
		}

		if(resp.availableFilters.verified_contact){
			$$('.sub-search #verified-contact').getParent('.inner').show();
			$$('.sub-search-as #verified-contact-as').getParent('.inner').show();
		}else{
			$$('.sub-search #verified-contact').getParent('.inner').hide();
			$$('.sub-search-as #verified-contact-as').getParent('.inner').hide();
		}

		if(resp.availableFilters.with_video){
			$$('.sub-search #with-video').getParent('.inner').show();
			$$('.sub-search-as #with-video-as').getParent('.inner').show();
		}else{
			$$('.sub-search #with-video').getParent('.inner').hide();
			$$('.sub-search-as #with-video-as').getParent('.inner').hide();
		}

		if(resp.availableFilters.pornstar){
			$$('.sub-search #pornstar').getParent('.inner').show();
			$$('.sub-search-as #pornstar-as').getParent('.inner').show();
		}else{
			$$('.sub-search #pornstar').getParent('.inner').hide();
			$$('.sub-search-as #pornstar-as').getParent('.inner').hide();
		}

		if(resp.availableFilters.online){
			$$('.sub-search #s_is_online_now').getParent('.inner').show();
			$$('.sub-search-as #s_is_online_now-as').getParent('.inner').show();
		}else{
			$$('.sub-search #s_is_online_now').getParent('.inner').hide();
			$$('.sub-search-as #s_is_online_now-as').getParent('.inner').hide();
		}

		}.bind(this)
	}).send();

	return false;
}


Sceon.initTopMenu = function() {

	if (typeof window.history.pushState == 'function') {
		$$('.logo-small, #top-menu .top-menu li a:not(a.no-ajax), #top-menu .sub-menu li a:not(a.no-ajax), #body-navigation .nav-item a, .top-categories a').removeEvents('click');
		$$('.logo-small, #top-menu .top-menu li a:not(a.no-ajax), #top-menu .sub-menu li a:not(a.no-ajax), #body-navigation .nav-item a, .top-categories a').addEvent('click', function(e){
			e.stop();

			var href = this.get('href');
			
			if (!href) return;

			history.pushState({}, '', href);

			Cookie.write('show_all_escorts', 0, {domain: headerVars.domain});

			var container = $('body');
			var side_bar_countries = $('side-bar-countries');
			var flags = $$('.flags')[0];
			var top_menu = $('top-menu-container');
			//var title = $$('.h-title')[0];

			/*var e_overlay = new Cubix.Overlay(container, {color: '#fff', opacity: .6, loader: _st('466.gif'), position: '90px', offset: {bottom: 0, top: 0}});
			e_overlay.disable();*/

			var e_overlay = Sceon.createOverlay($('container'));
			e_overlay.show();

			/*var c_overlay = new Cubix.Overlay(side_bar_countries, {color: '#fff', opacity: .6, loader: _st('466.gif'), position: '90px', offset: {bottom: 0, top: 0}});
			c_overlay.disable();*/



			//var myScroll = new Fx.Scroll(document.getElement('body'));

			new Request.JSON({
			//new Request({
				url: href + '?ajax',
				method: 'post',
				//data: data,
				//noCache: true,
				onSuccess: function (resp) {
					//resp = JSON.decode(resp);
					//resp = MessagePack.unpack(resp);

					if ( resp.side_bar ) {
						side_bar_countries.set('html', resp.side_bar);
					} else {
						alert('no geo content');
					}

					if ( resp.body )
						container.set('html', resp.body);

					if ( resp.flags )
						flags.set('html', resp.flags);
					//title.set('html', resp.city_country_title);

					if (resp.head_title) {
						window.document.title = resp.head_title;
					}

					ga('send', 'pageview', {'page': href,'title': resp.head_title});

					if (resp.top_menu) {
						top_menu.set('html', resp.top_menu);
					}


					Sceon.initTopMenu();

					Sceon.PhotoRoller.Init();

					Sceon.initLangPicker();
					Sceon.initNewestSlider();

					Sceon.initAgencies();
					Sceon.initSearchBox();
					Sceon.initSorting();
					Sceon.initShowAllAgencyEscorts();

					Sceon.initAgencySorting();
					Sceon.initListType();

					//Sceon.all_scroll.terminate();
					Sceon.initSideMenu();

					Sceon.initProfileThumbs();

					Sceon.initContactUsLinks();
					Sceon.initAdvertiseLinks();

					Sceon.destroyAllCountryScrolls();
					Sceon.initTips();
					Sceon.initAgencyTips();

					e_overlay.destroy();

					//c_overlay.destroy();

					Slimbox.scanPage();

					Sceon.Escorts.no_escorts = false;
					Sceon.Escorts.s_page = 1;
					Sceon.Escorts.scrollSpyLoadInit();

					//myScroll.toTop();

				}.bind(this)
			}).send();

			return false;

		});
	}
};

Sceon.initFooterMenu = function() {

	if (typeof window.history.pushState == 'function') {
		$$('#footer-left-menu a').removeEvents('click');
		$$('#footer-left-menu a').addEvent('click', function(e){
			e.stop();

			var href = this.get('href');

			history.pushState({}, '', href);
			//history.replaceState({}, '', '/escorts/city_ie_dublin');

			Cookie.write('show_all_escorts', 0, {domain: headerVars.domain});

			var container = $('body');
			var side_bar_countries = $('side-bar-countries');
			var flags = $$('.flags')[0];
			//var title = $$('.h-title')[0];

			/*var e_overlay = new Cubix.Overlay(container, {color: '#fff', opacity: .6, loader: _st('466.gif'), position: '90px', offset: {bottom: 0, top: 0}});
			e_overlay.disable();

			var c_overlay = new Cubix.Overlay(side_bar_countries, {color: '#fff', opacity: .6, loader: _st('466.gif'), position: '90px', offset: {bottom: 0, top: 0}});
			c_overlay.disable();*/

			var e_overlay = Sceon.createOverlay($('container'));
			e_overlay.show();

			var myScroll = new Fx.Scroll(document.getElement('body'));

			new Request.JSON({
			//new Request({
				url: href + '?ajax',
				method: 'post',
				//data: data,
				onSuccess: function (resp) {
					//resp = JSON.decode(resp);
					//resp = MessagePack.unpack(resp);

					container.set('html', resp.body);
					side_bar_countries.set('html', resp.side_bar);
					flags.set('html', resp.flags);

					if (resp.head_title) {
						window.document.title = resp.head_title;
					}

					ga('send', 'pageview', {'page': href,'title': resp.head_title});
					//title.set('html', resp.city_country_title);

					Sceon.initTopMenu();
					Sceon.initNewestSlider();

					Sceon.initLangPicker();

					Sceon.PhotoRoller.Init();

					Sceon.initAgencies();
					//Sceon.all_scroll.terminate();
					Sceon.initSideMenu();

					Sceon.initSearchBox();
					Sceon.initSorting();
					Sceon.initShowAllAgencyEscorts();

					Sceon.initAgencySorting();
					Sceon.initListType();

					Sceon.initContactUsLinks();

					Sceon.initProfileThumbs();

					Sceon.destroyAllCountryScrolls();
					Sceon.initTips();
					Sceon.initAgencyTips();

					e_overlay.destroy();

					Slimbox.scanPage();

					myScroll.toTop();

					Sceon.Escorts.no_escorts = false;
					Sceon.Escorts.s_page = 1;
					Sceon.Escorts.scrollSpyLoadInit();

				}.bind(this)
			}).send();

			return false;

		});
	}
};

Sceon.initSideMenu = function() {

	var top_btn = $$('#side-bar .block .head a.top-btn');

	var icons_new_update = $$('#side-bar .icon-new-update');

	var menu_lis = $$('#side-bar .block a.closed, #side-bar .block li.li-country');

	var subs = $$('#side-bar .block .sub');

	var root = $$('#side-bar ul.root');
	var root_all_countries = $$('#side-bar ul.root.all-countries')[0];

	icons_new_update.removeEvents('mouseenter');
	icons_new_update.removeEvents('mouseleave');
	icons_new_update.addEvent('mouseenter', function(){
		var sticker = this.getNext('.new-update-sticker');
		var pos = this.getCoordinates();

		sticker.setStyles({
			'top' : pos.top - 5,
			'left' : pos.left + 30
		});
		sticker.removeClass('none');
	});

	icons_new_update.addEvent('mouseleave', function(){
		var sticker = this.getNext('.new-update-sticker');
		var dim = this.getDimensions();

		sticker.addClass('none');
	});

	subs.each(function(el){
		var height = el.getCoordinates().height;
		el.store('sub-height', height);
		el.addClass('none');
	});

	root.removeClass('visibility-hidden');

	/*$$('.mscrollbar').destroy();
	var sc_root = new Scrollable(root_all_countries);
	Sceon.all_scroll = sc_root;

	if ( root_all_countries.getElements('li.li-country').length < 24 ) {
		Sceon.all_scroll.hideContainer(true);
	}*/

	/*var filter = new DG.Filter({
	  filterField : $('dgSearchField'),
	  filterEl : $$('.all-countries')[0],
	  xPathFilterElements : '.country',
	  onMatchShowChildren : true
	});*/

	var filter = new DG.Filter({
	  filterField : $('countrySearchField'),
	  filterEl : $$('.all-countries-filter')[0],
	  xPathFilterElements : '.country',
	  onMatchShowChildren : true
	});

	if ( $('citySearchField') ) {
		var filterCity = new DG.Filter({
		  filterField : $('citySearchField'),
		  filterEl : $$('.filter-cities')[0],
		  xPathFilterElements : '.city',
		  onMatchShowChildren : true,
		  listeners : {
		    beforefilter : function() { 

		    },
		    afterfilter : function() { 
		    	var sub = $$('li.sub')[0];
		    	var scrollbar = $$('.mscrollbar');

		    	var sub_lis = sub.getElements('ul li');
		    	
		    	var count = 0;
				sub_lis.each(function(el){
					if ( el.getStyle('display') != 'none' ) {
						count++;
					}
				});

				if ( count <= 12 ) {
					scrollbar.setStyle('display', 'none');
				} else {
					scrollbar.setStyle('display', '');
				}				

				window.fireEvent('resize');
		    }
		  }
		});
	}

	//block open/close btn init
	top_btn.addEvent('click', function(e){
		e.stop();

		var self = this;
		var root = this.getParent('div.head').getNext('ul.root');

		root.setStyle('overflow', 'hidden');

		if ( this.hasClass('closed') ) {
			new Fx.Tween(root, {
				duration: 300,
				transition: 'quart:out',
				property: 'height',
				onStart: function() {
					root.setStyle('height', 0);
					root.removeClass('none');
					self.removeClass('closed').addClass('opened');
				},
				onComplete: function() {
					root.setStyle('height', 'auto');

					//Sceon.Escorts.ss.options.min = window.getScrollSize().y - window.getSize().y - Sceon.Escorts.scroll_offset;
				}
			}).start(0, this.retrieve('root-height'));

		} else {

			var height = root.getCoordinates().height;
			this.store('root-height', height);

			new Fx.Tween(root, {
				duration: 300,
				transition: 'quart:out',
				property: 'height',
				onStart: function() {
					self.removeClass('opened').addClass('closed');
				},
				onComplete: function() {
					root.addClass('none');

					//Sceon.Escorts.ss.options.min = window.getScrollSize().y - window.getSize().y - Sceon.Escorts.scroll_offset;
				}
			}).start(height, 0);
		}
	});

	menu_lis.addEvent('click', function(event){

		if (event && event.stop) event.stop();

		/*var e;
		if (document.createEvent) { // no method in ie, not needed
			e = document.createEvent('MouseEvents');
			e.initEvent('click',true,true);
		}
		e.preventDefault();*/

		var self = this;

		if ( this.hasClass('not-openable') ) return;

		if ( this.hasClass('li-country') ) {
			var sub = this.getNext('li.sub');
		} else {
			var sub = this.getParent('li').getNext('li.sub');
		}

		var height = sub.retrieve('sub-height');
		var max_height = 422;

		sub.setStyle('overflow', 'hidden');

		var _height = height;
		if ( sub.getElements('li').length > 12 ) {
			_height = max_height;

			if ( ! sub.retrieve('scroll') ) {
				var sc = new Scrollable(sub);
				sub.store('scroll', sc);
				Sceon.scrolls_collection.push(sc);
			}
		}

		if ( this.hasClass('closed') ) {

			new Fx.Tween(sub, {
				duration: 300,
				transition: 'quart:out',
				property: 'height',
				onStart: function() {
					sub.setStyle('height', 0);
					sub.removeClass('none');

					if ( self.hasClass('li-country') ) {
						self.removeClass('closed').addClass('opened');
						self.getElements('a').removeClass('closed').addClass('opened');
					} else {
						self.removeClass('closed').addClass('opened');
						self.getParent('li').removeClass('closed').addClass('opened');
					}

					window.fireEvent('resize');
				},
				onComplete: function() {
					if ( sub.retrieve('scroll') ) {
						sub.retrieve('scroll').reposition();
						sub.retrieve('scroll').showContainer(true);
					}

					//Sceon.Escorts.ss.options.min = window.getScrollSize().y - window.getSize().y - Sceon.Escorts.scroll_offset;

					window.fireEvent('resize');
				}
			}).start(0, _height);

		} else {
			new Fx.Tween(sub, {
				duration: 300,
				transition: 'quart:out',
				property: 'height',
				onStart: function() {

					if ( sub.retrieve('scroll') ) {
						sub.retrieve('scroll').hideContainer(true);
					}

					if ( self.hasClass('li-country') ) {
						self.removeClass('opened').addClass('closed');
						self.getElements('a').removeClass('opened').addClass('closed');
					} else {
						self.removeClass('opened').addClass('closed');
						self.getParent('li').removeClass('opened').addClass('closed');
					}
					window.fireEvent('resize');
				},
				onComplete: function() {
					sub.addClass('none');
					window.fireEvent('resize');

					//Sceon.Escorts.ss.options.min = window.getScrollSize().y - window.getSize().y - Sceon.Escorts.scroll_offset;
				}
			}).start(_height, 0);

		}
	});

	//open selected country after refresh when city selected
	if ( $$('#side-bar .block a.city-selected').length ) {
		$$('#side-bar .block a.city-selected')[0].getParent('li.sub').getPrevious('li').getPrevious('li').getElements('a.country')[0].fireEvent('click');
	}

	//open selected country after refresh when country selected
	if ( $$('#side-bar .block a.country-selected').length ) {
		$$('#side-bar .block a.country-selected')[0].fireEvent('click');
	}

	Sceon.initSideBarUrl();

	if ( $$('#side-bar .block a.geo-selected').length ) {
		var e;
		if (document.createEvent) { // no method in ie, not needed
			e = document.createEvent('MouseEvents');
			e.initEvent('click',true,true);
		}
		$$('#side-bar .block a.geo-selected')[0].fireEvent('click', new Event(e));
	}
};

Sceon.initSideBarUrl = function() {

	if (typeof window.history.pushState == 'function') {

		$$('#side-bar ul.root li.city-li, #side-bar ul.root li.li-country, #side-bar ul.root a').removeEvents('click');
		$$('#side-bar ul.root li.city-li, #side-bar ul.root li.li-country, #side-bar ul.root a').addEvent('click', function(e){
			e.stop();

			//if ( this.hasClass('country') ) return;

			var self = this;

			if ( self.hasClass('geo-selected') ) return;

			$$('#side-bar ul.root a.city-selected').removeClass('city-selected');

			var href = this.get('href');
			if ( this.hasClass('city-li') ) {
				var href = this.getElements('a')[0].get('href');
				this.getElements('a')[0].addClass('city-selected');
			} else if ( this.hasClass('li-country') ) {
				var href = this.getElements('a')[0].get('href');
			}
			else {
				this.addClass('city-selected');
			}


			history.pushState({}, '', href);


			if ( self.hasClass('show-all-escorts') ) {
				Cookie.write('show_all_escorts', 1, {domain: headerVars.domain});
			} else {
				Cookie.write('show_all_escorts', 0, {domain: headerVars.domain});
			}

			var container = $('body');
			var top_menu = $('top-menu-container');
			var footer_menu = $('footer-menu-container');
			var side_bar_countries = $('side-bar-countries');

			/*var f_overlay = new Cubix.Overlay(container, {color: '#fff', opacity: .6, loader: _st('466.gif'), position: '90px', offset: {bottom: 0, top: 0}});
			f_overlay.disable();*/
			var e_overlay = Sceon.createOverlay($('container'));
			e_overlay.show();

			/*if ( ! self.hasClass('geo-selected') ){
				var c_overlay = new Cubix.Overlay(side_bar_countries, {color: '#fff', opacity: .6, loader: _st('466.gif'), position: '90px', offset: {bottom: 0, top: 0}});
				c_overlay.disable();
			}*/

			var flags = $$('.flags')[0];
			//var title = $$('.h-title')[0];

			var myScroll = new Fx.Scroll(document.getElement('body'));

			new Request.JSON({
			//new Request({
				url: href + '?ajax',
				method: 'post',
				//data: data,
				onSuccess: function (resp) {
					//resp = JSON.decode(resp);
					//resp = MessagePack.unpack(resp);

					container.set('html', resp.body);
					flags.set('html', resp.flags);
					//title.set('html', resp.city_country_title);

					if (resp.head_title) {
						window.document.title = resp.head_title;
					}

					ga('send', 'pageview', {'page': href,'title': resp.head_title});

					if (resp.top_menu) {
						top_menu.set('html', resp.top_menu);
					}

					if (resp.footer_menu) {
						footer_menu.set('html', resp.footer_menu);
					}

					if ( resp.side_bar && ! self.hasClass('geo-selected') ) {
						side_bar_countries.set('html', resp.side_bar);
					}

					Sceon.destroyAllCountryScrolls();

					Sceon.initTopMenu();

					Sceon.PhotoRoller.Init();

					Sceon.initFooterMenu();
					Sceon.initNewestSlider();

					Sceon.initLangPicker();
					Sceon.initAgencies();

					//Sceon.initScrollBars();
					if ( ! self.hasClass('geo-selected') ){
						Sceon.initSideMenu();
					}

					Sceon.initProfileThumbs();
					Sceon.initSearchBox();
					Sceon.initSorting();
					Sceon.initShowAllAgencyEscorts();

					Sceon.initAgencySorting();
					Sceon.initListType();
					Sceon.initTips();
					Sceon.initAgencyTips();

					Sceon.initContactUsLinks();

					e_overlay.destroy();

					Slimbox.scanPage();

					myScroll.toTop();

					Sceon.Escorts.no_escorts = false;
					Sceon.Escorts.s_page = 1;
					Sceon.Escorts.scrollSpyLoadInit();

					//Sceon.Escorts.ss.options.min = window.getScrollSize().y - window.getSize().y - Sceon.Escorts.scroll_offset;

				}.bind(this)
			}).send();

			return false;

		});
	}
};

Sceon.initProfileThumbs = function() {


	//var thumbs = $$('#escorts div.escort.image');
	
	var profile_btns = $$('#escorts .escort-v2 .view-profile, #escorts .view-profile, .newest-slider .view-profile, a.inner-agency-escort, #escorts .escort .more-info, .view-profile');

	if (typeof window.history.pushState == 'function') {

		profile_btns.removeEvents('click');
		profile_btns.addEvent('click', function(e){
			e.stop();

			var href = this.get('href');
			var escort_id = this.get('rel');

			if ( this.hasClass('more-info') ) {
				var href = this.getElements('a.view-profile')[0].get('href');
				var escort_id = this.getElements('a.view-profile')[0].get('rel');
			}			

			Sceon.LocationHash.AppendEscortPosition('p_id=' + escort_id + ';');

			history.pushState({}, '', href);

			Sceon.loadEscortProfile(href + '?ajax');

			return false;
		});
	}

	/*thumbs.each(function(it) {
		var more_info = it.getElements('.more-info')[0];
		
		more_info.set('tween', {
			duration: 200,
			transition: 'cubic:out',
			property: 'height',
			onStart: function() {
				more_info.removeClass('none');
			},
			onComplete: function() {
				
			}
		});
		
	});

	thumbs.addEvent('mouseenter', function() {
		var more_info = this.getElements('.more-info')[0];

		more_info.get('tween').start(300);
	});

	thumbs.addEvent('mouseleave', function(){
		var more_info = this.getChildren('.more-info')[0];

		more_info.get('tween').start(90);
	});*/
}

Sceon.initSorting = function() {

	if ( $$('.sorting-box').length == 0 ) return;

	var sort_box = $$('.sorting-box')[0];

	new Mooniform(sort_box.getElements('select[name=sort], select[name=currency]'));

	sort_box.getElements('select[name=sort], select[name=currency]').addEvent('change', function(e){
		e.stop();
		Sceon.LocationHash.Set(Sceon.LocationHash.Make());
	});
};

Sceon.initShowAllAgencyEscorts = function() {

	if ( $$('.btn-show-all-hidden-escorts').length == 0 ) return;

	var btn_show_all_agency_escorts = $$('.btn-show-all-hidden-escorts')[0];

	btn_show_all_agency_escorts.removeEvents('click');
	btn_show_all_agency_escorts.addEvent('click', function(e){
		e.stop();
		$('show_all_hidden_escorts').set('value', 1);
		Sceon.Filter.Set({page: [this.get('page')]});
		//Sceon.LocationHash.Set(Sceon.LocationHash.Make());
	});
};


Sceon.initAgencySorting = function() {

	if ( $$('.agency-sorting-box').length == 0 ) return;

	var sort_box = $$('.agency-sorting-box')[0];

	new Mooniform(sort_box.getElements('select[name=agency_sort], select[name=currency]'));

	sort_box.getElements('select[name=agency_sort], select[name=currency]').addEvent('change', function(e){
		e.stop();
		Sceon.LocationHash.Set(Sceon.LocationHash.Make());
	});
};

Sceon.initListType = function() {

	if ( $$('.view-type:not(.view-type.agency)').length ) {		
		var view_type = $$('.view-type:not(.view-type.agency)')[0];

		view_type.getElements('a').addEvent('click', function(e){
			e.stop();

			view_type.getElements('input[name=list_type]')[0].set('value', this.get('rel'));

			Cookie.write('list_type', this.get('rel'), {domain: headerVars.domain, duration: 30, path: '/'});

			/*if ( this.get('rel') == 'xl-gallery' ) {
				Sceon.PhotoRoller.thumb = 'thumb_ed_xl';
			}*/

			Sceon.LocationHash.Set(Sceon.LocationHash.Make());
		});
	}

	if ( $$('.view-type.agency').length ) {		
		var view_type = $$('.view-type.agency')[0];

		view_type.getElements('a').addEvent('click', function(e){
			e.stop();

			view_type.getElements('input[name=list_type_a]')[0].set('value', this.get('rel'));

			Cookie.write('list_type_a', this.get('rel'), {domain: headerVars.domain, duration: 30, path: '/'});

			/*if ( this.get('rel') == 'xl-gallery' ) {
				Sceon.PhotoRoller.thumb = 'thumb_ed_xl';
			}*/

			Sceon.LocationHash.Set(Sceon.LocationHash.Make());
		});
	}
};

Sceon.uniformSearchElements = function() {
	var elementsToStyle = $$('.search-body input[type=radio], .search-body input[type=checkbox], .search-body select:not(.styled), .services-body input[type=checkbox], .sub-search input[type=checkbox]');
	mooniformInstance = new Mooniform(elementsToStyle);
}

Sceon.searchBoxShowHide = function(advanced_search_box) {
	var dim = advanced_search_box.getDimensions();

	if (!advanced_search_box.retrieve('my_tween')) {
		advanced_search_box.setStyles({
			'display': 'block',
			'height': '0px'
		});
		advanced_search_box.store('my_tween', new Fx.Tween(advanced_search_box, {
			link: 'wait',
			duration: 400,
			transition: 'cubic:in:out',
			property: 'height'
		}));
	}

	var fx = advanced_search_box
		.retrieve('my_tween')
		.removeEvents('complete');

	if ( advanced_search_box.hasClass('opened') ) {
		fx.addEvent('complete', function () {
			advanced_search_box.setStyles({
				'display': 'none',
				'height': 'auto'
			});

			Cookie.write('show_search', false, {domain: headerVars.domain});
		}).start(0);
		advanced_search_box.removeClass('opened');
	} else {
		advanced_search_box.setStyle('height', '0px');
		advanced_search_box.setStyle('display', 'block');
		fx.addEvent('complete', function () {
			advanced_search_box.setStyle('height', null);

			Cookie.write('show_search', true, {domain: headerVars.domain});
		}).start(dim.height);
		advanced_search_box.addClass('opened');
	}
};

Sceon.servicesBoxShowHide = function(services_body) {

	var dim = services_body.getDimensions();

	if (!services_body.retrieve('my_tween')) {
		services_body.setStyles({
			'display': 'block',
			'height': '0px'
		});
		services_body.store('my_tween', new Fx.Tween(services_body, {
			link: 'wait',
			duration: 400,
			transition: 'cubic:in:out',
			property: 'height'
		}));
	}

	var fx = services_body
		.retrieve('my_tween')
		.removeEvents('complete');

	if ( services_body.hasClass('opened') ) {
		fx.addEvent('complete', function () {
			services_body.setStyles({
				'display': 'none',
				'height': 'auto'
			});

			//Cookie.write('show_services', false, {domain: headerVars.domain});
		}).start(0);
		services_body.removeClass('opened');
	} else {
		services_body.setStyle('height', '0px');
		services_body.setStyle('display', 'block');
		fx.addEvent('complete', function () {
			services_body.setStyle('height', null);

			//Cookie.write('show_services', true, {domain: headerVars.domain});
		}).start(dim.height);
		services_body.addClass('opened');
	}
};

Sceon.initSearchTabs = function() {
	if ( ! $$('#search-box-wrapper .tab-box').length ) return;

	var tabs = $$('#search-box-wrapper .tab-box')[0].getElements('ul li');
	var active_tab = $$('#search-box-wrapper .tab-box')[0].getElements('ul li.act')[0];
	var tab_bodies = $$('#advanced-search-box-v2 .search-body div.tab-body');

	tab_bodies.each(function(tab_body){
		if ( ! tab_body.hasClass(active_tab.get('title')) ) {
			tab_body.addClass('none');
		}
	});

	tabs.addEvent('click', function(e){
		e.stop();

		tabs.removeClass('act');
		this.addClass('act');

		var clicked = this.get('title');

		tab_bodies.each(function(body){
			if ( body.hasClass(clicked) ) {
				body.removeClass('none');
			} else {
				body.addClass('none');
			}
		});

	});
};

Sceon.initSearchBlueBox = function() {

	if ( ! $$('#search-box-wrapper .blue-box').length ) return;

	var blue_box = $$('#search-box-wrapper .blue-box');

	blue_box.each(function(bb){
		if ( ! bb.hasClass('selected') ) {			
			bb.getElements('.b-body').addClass('none');
			bb.addClass('closed');
		}

		bb.getElements('.b-header').removeEvents('click');
		bb.getElements('.b-header').addEvent('click', function() {
			//console.log('click');
		
			var b_body = this.getNext('div.b-body');

			if ( b_body.hasClass('none') ) {
				b_body.removeClass('none');
				bb.removeClass('closed');
			} else {
				b_body.addClass('none');
				bb.addClass('closed');
			}
		});
	});

	//console.log('ddd');
};

Sceon.initSearchBox = function() {

	if ( ! $('search-box-wrapper') ) return;

	var search_box_wrapper = $('search-box-wrapper');
	var advanced_search_btn = $$('.advanced-search-btn')[0];
	var quick_search_btn = $$('.quick-search-btn')[0];

	var quick_search_box = $('search-box');
	var advanced_search_box = $('advanced-search-box-v2');
	
	
	
	advanced_search_btn.addEvent('click', function(e){
		e.stop();
		
		//Cookie.write('show_advanced_search', true, {domain: headerVars.domain});
		advanced_search_box.removeClass('none').addClass('open');;
		quick_search_box.addClass('none');

		this.addClass('act');
		quick_search_btn.removeClass('act');

		//Sceon.Escorts.ss.options.min = window.getScrollSize().y - window.getSize().y - Sceon.Escorts.scroll_offset;
	});

	quick_search_btn.addEvent('click', function(e){
		e.stop();
		
		//Cookie.write('show_advanced_search', false, {domain: headerVars.domain});
		advanced_search_box.addClass('none').removeClass('open');;
		quick_search_box.removeClass('none');

		this.addClass('act');
		advanced_search_btn.removeClass('act');

		//Sceon.Escorts.ss.options.min = window.getScrollSize().y - window.getSize().y - Sceon.Escorts.scroll_offset;
	});

	/*var services_btn = $$('.services-btn')[0];
	var services_body = $$('.services-body')[0];*/

	/*services_btn.addEvent('click', function(){

		if ( this.hasClass('open') ) {
			this.removeClass('open').addClass('closed');
			this.set('html', searchVars.show_services);

			Cookie.write('show_services', false, {domain: headerVars.domain});
		} else {
			this.removeClass('closed').addClass('open');
			this.set('html', searchVars.hide_services);

			Cookie.write('show_services', true, {domain: headerVars.domain});
		}

		Sceon.servicesBoxShowHide(services_body);
	});

	if ( Cookie.read('show_services') == 'true' ) {
		services_btn.fireEvent('click');
		services_btn.addClass('open');
	} else {
		services_btn.addClass('closed');
	}*/

	/*advanced_search_btn.addEvent('click', function() {
		Sceon.searchBoxShowHide(advanced_search_box);
	});

	if ( Cookie.read('show_search') == 'true' ) {
		//advanced_search_btn.fireEvent('click');
		advanced_search_box.setStyle('height', 'auto');
		advanced_search_box.setStyle('display', 'block');
		advanced_search_box.addClass('opened');
	}

	search_close_btn.addEvent('click', function(e) {
		e.stop();

		Sceon.searchBoxShowHide(advanced_search_box);
	});*/




	var inputs = quick_search_box.getElements('input[type=checkbox]');
	inputs.addEvent('change', function() {		
		Sceon.LocationHash.Set(Sceon.LocationHash.Make({}));
	});


	/* ####### Init Autocompleter for City Search ######## */
	var input_city_con = search_box_wrapper.getElement('.choose_city');
	var input_city = search_box_wrapper.getElement('#city');

    if( $$('.auto-city-search-name').length ) {
        $$('.auto-city-search-name').destroy();
    }

	var auto_cities = new Autocompleter.Request.JSON(input_city,'/escorts/ajax-get-cities-search', {
		'minLength': 1,
		postVar: 'city_search_name',
		//'postData':{'country_id':$('c_country_id').get('value')},
		indicatorClass: 'autocompleter-loading',
        className: 'autocompleter-choices auto-city-search-name'
	}).addEvent('onSelection', function (element, selected, value, input) {
		search_box_wrapper.getElement('#s_slug').set('value', selected.inputValue.slug);
		search_box_wrapper.getElement('#s_type').set('value', selected.inputValue.type);
		//search_box_wrapper.getElement('#s_city_id').set('value', selected.inputValue.cityId);
		
	});

	input_city.addEvent('blur', function() {
		if ( ! this.get('value').length ) {
			search_box_wrapper.getElement('#s_slug').set('value', '');
			search_box_wrapper.getElement('#s_type').set('value', '');
			search_box_wrapper.getElement('#city').set('value', '');
			//search_box_wrapper.getElement('#s_city_id').set('value', '');
		}
	});
	/* ####### Init Autocompleter for City Search ######## */

	// function checkAvailableChangeCity(){
	// 	if($('c_country_id').get('value') == 0 || $('c_country_id').get('value') == ''){
	// 		input_city.setProperty('disabled','disabled');
	// 		input_city_con.addClass('not-active');
	// 	}else{
	// 		input_city.removeProperty('disabled');
	// 		input_city_con.removeClass('not-active');
	// 	}
	// }
	// checkAvailableChangeCity();
	/* ####### Init Autocompleter for Country Search ######## */
	// var input_country = search_box_wrapper.getElement('#country');
 //    if( $$('.auto-country-search-name').length ) {
 //        $$('.auto-country-search-name').destroy();
 //    }

	// var auto_country = new Autocompleter.Request.JSON(input_country, '/escorts/ajax-get-countries-search', {
	// 	'minLength': 1,
	// 	postVar: 'country_search_name',
	// 	indicatorClass: 'autocompleter-loading',
 //        className: 'autocompleter-choices auto-countries-search-name'
	// }).addEvent('onSelection', function (element, selected, value, input) {
	// 	search_box_wrapper.getElement('#c_slug').set('value', selected.inputValue.slug);
	// 	search_box_wrapper.getElement('#c_type').set('value', selected.inputValue.type);
	// 	//$('c_country_id').set("value",selected.inputValue.country_id);
	// 	auto_cities.options.postData.country_id = selected.inputValue.country_id;
	// 	checkAvailableChangeCity();

	// 	search_box_wrapper.getElement('#s_slug').set('value', '');
	// 	search_box_wrapper.getElement('#s_type').set('value', '');
	// 	search_box_wrapper.getElement('#city').set('value', '');
	// 	search_box_wrapper.getElement('#s_city_id').set('value', '');
	// 	$$('.multi_cities').set('html', '');
	// });

	// input_country.addEvent('blur', function() {
	// 	if ( ! this.get('value').length ) {
	// 		// remove City
	// 		search_box_wrapper.getElement('#s_slug').set('value', '');
	// 		search_box_wrapper.getElement('#s_type').set('value', '');

	// 		// remove Country
	// 		search_box_wrapper.getElement('#c_slug').set('value', '');
	// 		search_box_wrapper.getElement('#c_type').set('value', '');
	// 		search_box_wrapper.getElement('#c_country_id').set('value', '');
	// 		checkAvailableChangeCity();
	// 	}
	// });
	/* ####### Init Autocompleter for City Search ######## */

	// var add_city_btn = $$('.add-city-btn')[0];


	// add_city_btn.addEvent('click', function(e){
	// 	e.stop();
	// 	countryId = $('c_country_id').get('value');
	// 	cityId = $('s_city_id').get('value');
	// 	cityTitle = $('city').get('value');

	// 	if(countryId == 0 || countryId == ""){
	// 		return false; // Set Category
	// 	}
	// 	if(cityId == 0 || cityId == ""){
	// 		return false; // Set City
	// 	}else{

	// 		cityAlreadySelected = $$('.multi_cities .itemId_'+cityId).length;
	// 		if(cityAlreadySelected){
	// 			return false; // Already Selected
	// 		}

	// 		city_slug = search_box_wrapper.getElement('#s_slug').get('value');
	// 		var city  = new Element('div', {'class': 'city-item itemId_'+cityId});
	// 		var myHiddenElement = new Element('input', {type: 'hidden', value: cityId, 'name': 'multi_cities[]'});
	// 		var cityLabel = new Element('span', {html: cityTitle, 'class': 'city_title'});
	// 		var cityRemove = new Element('a', {'class': 'city-remove'});
			
	// 		myHiddenElement.inject(city);
	// 		cityLabel.inject(city);
	// 		cityRemove.inject(city);
			
	// 		city.inject($$('.multi_cities')[0]);

	// 		search_box_wrapper.getElement('#s_slug').set('value', '');
	// 		search_box_wrapper.getElement('#s_type').set('value', '');
	// 		search_box_wrapper.getElement('#city').set('value', '');
	// 		search_box_wrapper.getElement('#s_city_id').set('value', '');
	// 	}
	// });


	// $$('.multi_cities').addEvent('click:relay(.city-remove)', function(event, target){
	//    event.preventDefault();
	//    this.getParent('.city-item').destroy();
	// });

	/*var selects = search_box_wrapper.getElements('select');
	selects.addEvent('change', function() {
		
		Sceon.LocationHash.Set(Sceon.LocationHash.Make({}));
	});*/

	/* ####### Init Autocompleter for Agency Search ######## */
	var inputAgency = search_box_wrapper.getElement('#agency');

    if( $$('.auto-agency-search-name').length ) {
        $$('.auto-agency-search-name').destroy();
    }

	var auto_agencies = new Autocompleter.Request.JSON(inputAgency, '/escorts/ajax-get-agencies-search', {
		'minLength': 1,
		postVar: 'agency_search_name',
		indicatorClass: 'autocompleter-loading',
        className: 'autocompleter-choices auto-agency-search-name'
	}).addEvent('onSelection', function (element, selected, value, input) {
		search_box_wrapper.getElement('#a_slug').set('value', selected.inputValue.slug);
		search_box_wrapper.getElement('#a_type').set('value', selected.inputValue.type);
		//Sceon.LocationHash.Set(Sceon.LocationHash.Make());
	});

	inputAgency.addEvent('blur', function() {
		if ( ! this.get('value').length ) {
			search_box_wrapper.getElement('#a_slug').set('value', '');
			search_box_wrapper.getElement('#a_type').set('value', '');
			//Sceon.LocationHash.Set(Sceon.LocationHash.Make());
		}
	});
	/* ####### Init Autocompleter for Agency Search ######## */




	var topSearchInput = search_box_wrapper.getElement('#top_search_input');

    if( $$('.top-search-input').length ) {
        $$('.top-search-input').destroy();
    }

	var auto_top = new Autocompleter.Request.JSON(topSearchInput, '/escorts/ajax-get-top-search', {
		'minLength': 1,
		maxChoices: 30,
		postVar: 'top_search_name',
		indicatorClass: 'autocompleter-loading',
        className: 'autocompleter-choices top-search-input'
	}).addEvent('onSelection', function (element, selected, value, input) {

		if ( selected.inputValue.type  == 'escort' && selected.inputValue.method != 'see-all' ) {
			
			if (typeof window.history.pushState == 'function') {

				var href = '/escort/' + selected.inputValue.slug + '-' + selected.inputValue.escort_id;

				history.pushState({}, '', href);

				Sceon.loadEscortProfile(href + '?ajax');
			}

		} else {
			search_box_wrapper.getElement('#ts_slug').set('value', selected.inputValue.slug);

			if ( selected.inputValue.method != 'see-all' ) {
				search_box_wrapper.getElement('#ts_type').set('value', selected.inputValue.type);
			} else {
				search_box_wrapper.getElement('#ts_type').set('value', 'escort-see-all');
			}

			Sceon.LocationHash.Set(Sceon.LocationHash.Make());
		}
	});


	topSearchInput.addEvent('focus', function() {
		if ( this.get('value') == searchVars.search_by_showname ) {
			this.set('value', '');
			//this.removeClass('def-text');
		}
	});
	topSearchInput.addEvent('blur', function(){
		if ( ! this.get('value').length ) {
			this.set('value', searchVars.search_by_showname);
			//this.addClass('def-text');

			search_box_wrapper.getElement('#ts_slug').set('value', '');
			search_box_wrapper.getElement('#ts_type').set('value', '');

			/*if (typeof window.history.pushState == 'function') {
				history.pushState({}, '', '/');
			}*/
			Sceon.LocationHash.Set(Sceon.LocationHash.Make());
		}
	});

	search_box_wrapper.getElements('a.btn-make-search').addEvent('click', function(e) {
		e.stop();

		Sceon.LocationHash.Set(Sceon.LocationHash.Make());
	});

	Sceon.uniformSearchElements();

	Sceon.initSearchBlueBox();

	Sceon.initSearchTabs();

	/*if ( Cookie.read('show_advanced_search') == 'true' ) {
		advanced_search_box.removeClass('none').addClass('open');
		quick_search_box.addClass('none');

		quick_search_btn.removeClass('act');
		advanced_search_btn.addClass('act');
	} else {*/
		advanced_search_box.addClass('none').removeClass('open');
		quick_search_box.removeClass('none');

		quick_search_btn.addClass('act');
		advanced_search_btn.removeClass('act');
	//}
};

Sceon.InitSearchInput = function (inputs) {

	/*this.timer = null;

	inputs.each(function(el){

		if ( el.hasClass('showname') ) {
			el.addEvent('focus', function(){
				if ( this.get('value') == searchVars.search_by_showname ) {
					this.set('value', '');
					
				}
			});
			el.addEvent('blur', function(){
				if ( ! this.get('value').length ) {
					this.set('value', searchVars.search_by_showname);
					
				}
			});
		}

		el.addEvents({
			keyup: function () {
				clearTimeout(this.timer);
				this.timer = setTimeout(function(){
					if ( el.hasClass('name') == 'showname' ) {
						var value = ( el.get('value').length && el.get('value') != searchVars.search_by_showname ) ? el.get('value') : null;
						el.set('value', value);
					}

					

					Sceon.LocationHash.Set(Sceon.LocationHash.Make());
				}, 700);
			}.bind(this)
		});
	});*/
}

Sceon.initAgencies = function() {
	return false;

	if ( ! $$('.agency-escorts-wrapper').length ) return;

	var agency_escorts_wrapper = $$('.agency-escorts-wrapper');

	agency_escorts_wrapper.each(function(el){
		if ( ! el.hasClass('open') ) {
			el.eliminate('escorts-height');
			el.removeClass('none');
			el.setStyle('height', 'auto');
			var height = el.getCoordinates().height;
			el.store('escorts-height', height);
			el.addClass('none');
		}
	});



	$$('.btn-show').removeEvents('click');
	$$('.btn-show').addEvent('click', function(e){
		e.stop();

		var self = this;

		var id = this.get('rel');
		//var wrapper = $$('.wr-' + id)[0];
		var wrapper = this.getParent('div.row').getNext('.wr-' + id);

		var height = wrapper.retrieve('escorts-height');

		wrapper.setStyle('overflow', 'hidden');

		var fx = new Fx.Tween(wrapper, {
			duration: 400,
			transition: 'cubic:in:out',
			property: 'height',
			onStart: function() {
				if ( self.hasClass('closed') ) {
					wrapper.setStyle('height', 0);
					wrapper.removeClass('none').addClass('open').removeClass('closed');
					self.removeClass('closed').addClass('open');
				} else {
					self.removeClass('open').addClass('closed');
					wrapper.addClass('closed').removeClass('open');
				}
			},
			onComplete: function() {
				if ( self.hasClass('closed') ) {
					wrapper.addClass('none');

				} else {
					wrapper.setStyle('height', 'auto');
				}

				//Sceon.Escorts.ss.options.min = window.getScrollSize().y - window.getSize().y - Sceon.Escorts.scroll_offset;
			}
		});

		if ( this.hasClass('closed') ) {
			fx.start(0, height);
		} else {
			fx.start(height, 0);
		}
	});
}

Sceon.destroyAllCountryScrolls = function() {

	Sceon.scrolls_collection.each(function(sub){
		sub.hideContainer(true);
	});

};

Sceon.initNewestSlider = function() {

	if ( ! $$(".newest-slider").length ) return;

	var title = $$('.header-title p')[0];
	var wrapper = $$('.newest-slider-wrp')[0];

	title.removeEvents('click');
	title.addEvent('click', function() {

		var self = this;

		var fx = new Fx.Tween(wrapper, {
			duration: 300,
			transition: 'quart:in',
			property: 'height',
			onStart: function() {
				if ( self.hasClass('open') ) {
					self.removeClass('open').addClass('closed');
				} else {
					self.removeClass('closed').addClass('open');
				}
			},
			onComplete: function() {
				if ( self.hasClass('open') ) {
					Cookie.write('show_newest', true, {domain: headerVars.domain});
				} else {
					Cookie.write('show_newest', false, {domain: headerVars.domain});
				}
			}
		});

		if ( this.hasClass('open') ) {
			fx.start(185, 0);
		} else {
			fx.start(0, 185);
		}
	});

	new slideGallery($$(".newest-slider"), {
		steps: 6,
		mode: "callback",
		autoplay: false,
		paging: false,
		//pagingHolder: ".paging",
		onStart: function() {

		},
		onPlay: function() {
			this.fireEvent("start");
		}
	});


	if ( Cookie.read('show_newest') ) {
		if ( Cookie.read('show_newest') == 'true' ) {
			title.addClass('open').removeClass('closed');
			wrapper.setStyle('height', 185);
		} else {
			title.addClass('closed').removeClass('open');
			wrapper.setStyle('height', 0);
		}
	}
};

function preloadImages(array) {
	if (!preloadImages.list) {
		preloadImages.list = [];
	}
	for (var i = 0; i < array.length; i++) {
		var img = new Image();
		img.src = array[i];
		preloadImages.list.push(img);
	}
}

Sceon.initTips = function() {

	if( ! $$('.row').length ) return;

	if ( $$('.tip-wrap').length ) {
		$$('.tip-wrap').destroy();
	}

	/*var images = [];
	$$('.row').each(function(el){
		images.push(el.get('image'));
	});

	preloadImages(images);*/

	var myTips = new Tips('.row', {
		onShow: function(tip, el) {

			if ( ! el.get('image').length ) {
				tip.setStyles({
					display: 'none'
				});
				return;
			}

			var is_susp = el.get('is-susp');

			tip.setStyles({
				//visibility: 'hidden',
				display: 'block'
			});

			tip.getElements('.tip')[0].set('html', '');

			new Element('img', {
				src : el.get('image'),
				width: '118',
				height: '148'
			}).inject(tip.getElements('.tip')[0]);

			if ( is_susp == 'true' ) {
				new Element('img', {
					src : '/img/icon_susp_sticker_simple.png',
					width: '49',
					height: '48',
					'class': 'sticker-susp-simple'
				}).inject(tip.getElements('.tip')[0]);				
			}
		}
	});
};

Sceon.initAgencyTips = function() {

	if( ! $$('.ag-list').length ) return;

	/*if ( $$('.tip-wrap').length ) {
		$$('.tip-wrap').destroy();
	}*/	

	var myTips = new Tips('.agency-tip', {
		onShow: function(tip, el) {			
			tip.setStyles({				
				display: 'block',
				border: 'none',
				width: 'auto',
				height: 'auto'
			});
			tip.getElements('.tip')[0].set('html', '');

			new Request({
				url: lang_id + '/agencies/show-tool-tip?agency_id=' + el.get('agency_id') + '&type=' + el.get('typee'),
				method: 'get',
				onSuccess: function (resp) {
					tip.getElements('.tip')[0].set('html', resp);
				}
			}).send();

		}
	});
};

Sceon.createOverlay = function(container) {

	$$('.moo-overlay').destroy();
	return new Spinner(container, {'class':'moo-overlay'});
}

Sceon.resizeProfile = function() {

	//console.log('ddd');
	if ( $$('.comments-block')[0] ) {
	
		var leftSide = $('left');
		var profile = $('profile-container');
		var comments = $$('.comments-block')[0];

		if ( ! profile.get('rel') ) {
			profile.set('rel', profile.getSize().y);
		}

		var commentsHeight = comments.setStyles({
			visibility: 'hidden',
			display: 'block'
		}).getHeight();

		comments.setStyles({
			visibility: null,
			display: null
		})	

		var profileHeight = 0;

		profileHeight = (profile.get('rel')*1) + (commentsHeight*1) - 4950; //4700


		if ( leftSide.getSize().y > profileHeight ) {
			profile.setStyle('height', left.getSize().y);
		} else {			
			
			profile.setStyle('height', profileHeight);
		}
	} else {
		var $leftHeight = $("left").getCoordinates().height;
		var $profileHeight = $("profile-container").getCoordinates().height - 4600;

		if ( $leftHeight > $profileHeight ) {
			$("profile-container").setStyle('height', $leftHeight);
		} else {
			$("profile-container").setStyle('height', $profileHeight);
		}
	}
};

var reorderTabs = function(){
	var tabs_count = $$("#profile-tabs li").length;
	$$("#profile-tabs li").each(function(it, index){
		it.setStyle('z-index', tabs_count - index);
	});
};

Sceon.resizeProfileInner = function() {
	if ( ! $('profile-inner') ) return;

	var profile_inner = $('profile-inner');
	var profile_filler = $('fill-profile-scroll');

	profile_inner.setStyle('height', 'auto');

	var height = profile_inner.getSize().y;

	profile_inner.setStyle('height', height);
	profile_filler.setStyle('height', height);
};

Sceon.initLoadMoreBtn = function() {

	var more_photos_list = $$('.more-photos-list')[0];
	$$('.load-more-pics').addEvent('click', function(e){
		e.stop();

		more_photos_list.removeClass('none');
		this.getParent('.load-more-pics-center').destroy();

		Sceon.resizeProfile();

	});
};

Sceon.viewedEscorts = function() {

	if ( $('escort_id') ) {

		new Request({
			url: lang_id + '/escorts/viewed-escorts?escort_id=' + $('escort_id').get('value'),
			method: 'get',
			onSuccess: function (resp) {
				if(resp.contains('rapper'))
				{
					$$('.latest-visited-profiles')[0].setStyle('opacity', 0);
					var myFx = new Fx.Tween($$('.latest-visited-profiles')[0], {
						duration: 400,
						onComplete: function() {
							$$('.latest-visited-profiles')[0].set('html', resp);
							$$('.latest-visited-profiles')[0].tween('opacity', 0, 1);
							$$('.latest-visited-profiles')[0].addClass('border');
						}
					});
					myFx.start('height', '250');
				}
			}
		}).send();
	}
}

Sceon.initProfileV2 = function() {

	if ( ! $$('.profile-tabs').length ) return;

    if ( $$('.susp-photo').length ) {
        $$('.susp-photo').addEvent('click', function(e){
            e.stop();
            Cubix.RPopup.url = this.get('rel');
            Cubix.RPopup.type = this.get('class');
            Cubix.RPopup.location = document.body;

            var c = this.getCoordinates(document.body);
            Cubix.RPopup.Show(c.left - 115, c.top - 33);
        });
    }

    Sceon.viewedEscorts();

    Sceon.initLoadMoreBtn();
	
	Sceon.resizeProfile();
	Sceon.resizeProfileInner();

	reorderTabs();
	initGalleryTabs();
	var profile = $('profile-container');

	if ( $$('.hand-verified').length ) {
		
		$$('.hand-verified').addEvent('mouseenter', function(){
			this.getElements('.tip').removeClass('none');
		});

		$$('.hand-verified').addEvent('mouseleave', function(){
			this.getElements('.tip').addClass('none');
		});
	}

	$$('#profile-tabs li a').addEvent('click', function(e){
		e.stop();

		$$('.profile-tabs li.active').removeClass('active');

		var tab = this.get('href');

		var el = $$("#right a[rel="+ tab.replace("#", "") +"]")[0];
		
		this.getParent('li').addClass('active');

		if(el) {

	        var scroll = new Fx.Scroll(
	             $("profile-inner"),
	            {wait: false, duration: 700, transition: Fx.Transitions.Quad.easeInOut}
	        );
	        scroll.toElement(el);
	    }	
	});

	if ( profile.getElements('.ajax-comments-container').length ) {
		Cubix.Comments.container = profile.getElements('.ajax-comments-container')[0];
		Cubix.Comments.escort_id = profile.getElements('.comments-block')[0].get('e-id');
		Cubix.Comments.LoadComments();
	}

	Sceon.initFavorites();

	if ( $('escort_id') ) {
		var escort_id = $('escort_id').get('value');

		new Request({
			url: lang_id + '/index/get-escorts-view-count',
			method: 'post',
			data: {escort_id : escort_id},
			onSuccess: function (resp) {
				$('sedcard-views').set('html', resp);
			}.bind(this)
		}).send();
	}

	Sceon.AgencyEscorts.initThumbHover();
};

Sceon.ComTabs = function() {
    if ( $$('.reviews_comments ul li a').length > 1 ) {
        $$('.reviews_comments ul li a').addEvent('click', function() {

            $$('.r_c .rTab, .r_c .cTab').each(function(it){
                it.addClass('none');
            });

            $$('.reviews_comments .r_c .' + this.get('id')).removeClass('none');

            if ( this.hasClass('reviews') ) {
                this.addClass('active');

                this.getParent('li').getNext('li').removeClass('active');
                this.getParent('li').addClass('active');

                this.getParent('li').getNext('li').getElement('a').removeClass('active');
            }

            if ( this.hasClass('comments') ) {
                this.addClass('active');

                this.getParent('li').getPrevious('li').removeClass('active');
                this.getParent('li').addClass('active');

                this.getParent('li').getPrevious('li').getElement('a').removeClass('active');
            }

            Sceon.resizeProfile();
        })
    }
};

Sceon.ComTabsReversed = function() {
    if ( $$('.reviews_comments ul li a').length > 1 ) {
        $$('.reviews_comments ul li a').addEvent('click', function() {
        	
            $$('.r_c .rTab, .r_c .cTab').each(function(it){
                it.addClass('none');
            });

            $$('.reviews_comments .r_c .' + this.get('id')).removeClass('none');

            if ( this.hasClass('reviews') ) {
                this.addClass('active');

                this.getParent('li').getPrevious('li').removeClass('active');
                this.getParent('li').addClass('active');

                this.getParent('li').getPrevious('li').getElement('a').removeClass('active');
            }

            if ( this.hasClass('comments') ) {
                this.addClass('active');

                this.getParent('li').getNext('li').removeClass('active');
                this.getParent('li').addClass('active');

                this.getParent('li').getNext('li').getElement('a').removeClass('active');
            }

            Sceon.resizeProfile();
        })
    }
};

Sceon.loadEscortProfile = function(href) {
	var e_overlay = Sceon.createOverlay($('container'));
	e_overlay.show();

	var flags = $$('.flags')[0];

	new Request.JSON({
		//new Request({
			url: href,
			method: 'post',
			//data: data,
			onSuccess: function (resp) {

				if ( resp.body.length ) {
					$('body').set('html', resp.body);
				} else {
					window.location.href = '/';
				}

				if ( resp.flags ) {
					flags.set('html', resp.flags);
				}

				var title = $('body').getElement('#seo_title]').get('value');
				window.document.title = title;

				ga('send', 'pageview', {'page': href,'title': title});

				Sceon.initLangPicker();
				Sceon.initProfileV2();
				Sceon.initProfileThumbs();
				Slimbox.scanPage();

                if ($('profile-container').getElements('div.rTab').hasClass('none')[0])
                    Sceon.ComTabsReversed();
                else
                    Sceon.ComTabs();

				var myFx = new Fx.Scroll(window,{
					offset: {
						'x': 0,
						'y': -5
					}
				});
				myFx.toElement('top-menu');

				e_overlay.hide();

				Sceon.initFavorites();

			}.bind(this)
	}).send();
}

var initGalleryTabs = function() {
	$$('#photo-gallery-tabs li').addEvent('click',function(e){
		e.stop();
		$$('#photo-gallery-tabs li.tab').removeClass('active');
		this.addClass('active');
		showGalleryTab(this);
	})
}

var showGalleryTab = function(tab) {

	var a = tab.getElement('a');
	var galleryId = a.get('rel');
	var url = a.get('href');
	var content = $('gallery-content-' + galleryId);
	
	if (content.get('html').clean().length == 0) {
		var overlay = new Cubix.Overlay($$('.gallery-contents')[0], {
			loader: _st('loader-small.gif'),
			position: '50%'
		});
		overlay.disable();
		
		new Request({
			url: url,
			method: 'get',
			onSuccess: function (resp) {
				content.set('html',resp);
				cleanNone(content);
				overlay.enable();
			}.bind(this)
		}).send();

	}
	else{
		cleanNone(content);
	}
	
}

var cleanNone = function(content)
{
	$$('.gallery-content').each(function(it, index){
		if(it.hasClass('none') == false){
			it.addClass('none');
		}
	});
	content.removeClass('none');
}

Sceon.initFavorites = function() {

	if ( ! $('body').getElements('#afb .favorites').length ) return;
	
	new Request({
		url: lang_id + '/favorites/ajax-load-button?escort_id=' + $('body').getElement('#escort_id').get('value'),
		method: 'post',
		onSuccess: function (resp) {
			$('body').getElement('#afb').getElements('.favorites')[0].set('html', resp);

			$('body').getElement('#afb').getElements('.favorites')[0].getElements('a').addEvent('click', function(e){
				e.stop();

				if ( headerVars.is_member == true )
				{
					var url = lang_id + "/favorites/ajax-add-to-favorites";
					if ( this.hasClass('rfav') ) {
						var url = lang_id + "/favorites/ajax-remove";
					}

					var overlay = new Cubix.Overlay(this, {color: '#fdfdfd', opacity: .6, loader: _st('private/ajax-loader.gif'), position: '50%', offset: {bottom: -10, top: 5}});

					overlay.disable();

					new Request({
						url: url + '?escort_id=' + $('body').getElement('#escort_id').get('value'),
						method: 'post',
						onSuccess: function (resp) {
							Sceon.initFavorites();
						}
					}).send();
				} else {
					Popup.SignInUp.display();
				}
			});
		}.bind(this)
	}).send();
};

/* Alerts */
Sceon.alertsOpenPopup = function(escort_id) {	
	if ( !headerVars.is_member )
	{
		Popup.SignInUp.display();
		return;
	} 
	
	var container = $('alertme-popup-wrap');
		
	new Request({
		method: 'get',
		url: lang_id + '/alerts/open?escort_id=' + escort_id,
		onComplete: function(resp) {
			container.set('html', resp);
			
			var elements = container.getElements('select, input[type=checkbox]');
		
			var moo = new Mooniform(elements);
			
			var cities = Array();
	
			$$('.cts').get('value').each(function(el) {
				cities.push(el);
			});
						
			Sceon.alertsCheckCHB(moo, elements);
			
			$$('.alm-any').addEvent('click', function(e) {
				Sceon.alertsCheckCHB(moo, elements);
			});
			
			$$('.alertme-city-btn').addEvent('click', function (e) {
				e.stop();
				
				var c = $('city_id').getSelected();
				var c_id = Number.from(c.get('value').toString());
				var c_txt = c.get('text').toString();

				$('alm-6').set('checked', 'checked');
				
				moo.reInit(elements);

				if (c_id != null && !cities.contains(c_id))
				{
					cities.push(c_id);
					Sceon.AlertsAddCity(c_id, c_txt);
					Sceon.AlertsRemoveCity(cities);
				}
			});
			
			Sceon.AlertsRemoveCity(cities);
			
			$('a-save').addEvent('click', function(e){
				e.stop();
				
				var overlay = new Cubix.Overlay($('alert-popup'), {color: '#fff', opacity: .6, loader: _st('loader-small.gif'), position: '50%', offset: {bottom: 0, top: 0}});

				overlay.disable();

				var escort_id = $('escort-id').get('value');
								
				var i = 0;
				var events = Array();
				var arr = Array();
				var cts = Array();
				var tmp = Array();

				$$("#form input[type=checkbox]:checked").each(function(el) {
					arr = el.get('id').substr(4).split('-');
					events[i] = arr[0];
					i++;
				});
				
				i = 0;

				cities.each(function(el) {
					cts[i] = el;
					i++;
				});
				
				var cu = '';

				if (cts.length > 0)
					cu = '&cities=' + cts.join(',');

				var ev = events.join(',');

				if (ev == '5')
					ev = '1,2,3,4';
				else if (ev == '5,6')
					ev = '1,2,3,4,6';
				
				new Request({
					method: 'get',
					url: lang_id + '/alerts/save?escort_id=' + escort_id + '&events=' + ev + cu,
					onComplete: function(resp) {
						resp = JSON.decode(resp);
						$$('.cont').set('html', '<div style="text-align:center;">' + resp.msg + '</div>');
						$('tt').set('html', resp.msg_title);
						$$('.foot').destroy();
						
						overlay.enable();
					}
				}).send();

				return false;
			});
		}
	}).send();
	
	return false;
};
Sceon.alertsX = function () {
	$('alert-popup').destroy();
	
	return false;
};
Sceon.alertsCheckCHB = function(moo, els) {
	if ($$('input[name=5]')[0].get('checked') == true)
	{
		$$('input[name=1]').set('checked', '');
		$$('input[name=1]').set('disabled', 'disabled');
		$$('input[name=2]').set('checked', '');
		$$('input[name=2]').set('disabled', 'disabled');
		$$('input[name=3]').set('checked', '');
		$$('input[name=3]').set('disabled', 'disabled');
		$$('input[name=4]').set('checked', '');
		$$('input[name=4]').set('disabled', 'disabled');
	}
	else
	{
		$$('input[name=1]').set('disabled', '');
		$$('input[name=2]').set('disabled', '');
		$$('input[name=3]').set('disabled', '');
		$$('input[name=4]').set('disabled', '');
	}
	
	moo.reInit(els);
};
Sceon.AlertsAddCity = function(id, text) {
	var myFirstElement  = new Element('div', {'class': 'city-item-wr'});
	var myCityTitleElement  = new Element('div', {'class': 'city-title'});
	var myClearElement  = new Element('div', {'class': 'clear'});
	var mySecondElement = new Element('a', {id: id, 'class': 'city-item'});
	var myHiddenElement = new Element('input', {type: 'hidden', value: id, 'class': 'cts'});
	mySecondElement.inject(myFirstElement);
	myCityTitleElement.appendText(text);
	myCityTitleElement.inject(myFirstElement);
	myClearElement.inject(myFirstElement);
	myHiddenElement.inject(myFirstElement);
	myFirstElement.inject($('city-items'));
};
Sceon.AlertsRemoveCity = function(cities) {
	$$('.city-item').removeEvents();
	$$('.city-item').addEvent('click', function (e) {
		e.stop();
		var self = this;

		var id = self.get('id');
		
		Sceon.AlertsRemoveByElement(cities, id);
		self.getParent().dispose();
	});
};
Sceon.AlertsRemoveByElement = function(arrayName, arrayElement) {
	for (var i = 0; i < arrayName.length; i++)
	{ 
		if (arrayName[i] == arrayElement)
			arrayName.splice(i,1); 
	} 
};
/**/

/*--> Private Messaging*/
Sceon.PrivateMessagingPopup = function(participant_id) {	
	if ( ! headerVars.currentUser )
	{
		Popup.SignInUp.display();
		return false;
	} 
	
	if ( $('pm-escort-popup') ) {
		$('pm-escort-popup').destroy();
	}
	
	pmPopup = new moopopup({
		width: 470,
		resizable: false,
		draggable: true,
		url: lang_id + '/private-messaging/send-message-from-profile?participant=' + participant_id,
		overlay: false,
		onComplete: function() {
			var container = this.container;
			container.getElements('.close, .cancel').addEvent('click', function(){pmPopup.close();});
			
			var overlay = new Cubix.Overlay($$('#pm-escort-popup .pm-popup-body')[0], {color: '#fff', opacity: .6, has_loader: false});
			
			/*$$('#pm-escort-popup textarea, #pm-escort-popup input').addEvent('keypress', function(e) {
				if ( e.code == 13 && ! e.shift ) {
					e.stop();
					container.getElement('.send').fireEvent('click');
				}
			});*/
			
			new Mooniform($$('#pm-escort-popup .styled'));
			
			Sceon.initRecaptcha();
			
			container.getElement('.send').addEvent('click', function() {
				overlay.disable();
				new Request.JSON({
					url: lang_id + '/private-messaging/send-message-from-profile',
					method: 'POST',
					data: {
						message: container.getElement('textarea').get('value'),
						captcha: container.getElement('input[name="captcha"]').get('value'),
						participant: participant_id,
						escort_id: $('pm-escort-id') ? $('pm-escort-id').get('value') : false
					},
					onSuccess: function (resp) {
						container.getElements('span.error').set('html', '');
						container.getElements('span.err, textarea.err').removeClass('err');
						if ( resp.status == 'error' ) {
							for ( field in resp.msgs ) {
								container.getElement('input[name="' + field + '"], textarea[name="' + field + '"]').getNext('span.error').set('html', resp.msgs[field]);
								container.getElement('input[name="' + field + '"], textarea[name="' + field + '"]').addClass('err');
							}
							
							container.getElement('span.captcha img').set('src', '/captcha?' + Math.floor(Math.random() * 10000 ));
							container.getElement('.captcha-input').set('value', '');
						} else {
							$$('#pm-escort-popup .pm-popup-body')[0].empty();
							new Element('p', {
								'class': 'success-message',
								html: 'Your message has been sent.'
							}).inject($$('#pm-escort-popup .pm-popup-body')[0]);
							
							setInterval(function() {
								pmPopup.close();
							}, 2 * 1000);
						}
						
						overlay.enable();
					}
				}).send();
			});
		},
		onClose: function() {
		}
	});

	pmPopup.display();
	
	
	return false;
};
/*--> Private Messaging*/

Sceon.AgencyEscorts = {};

Sceon.AgencyEscorts.Load = function (page, escort_id, agency_id, el) {

	if ( ! el.hasClass('act') ) return false;
	
	//var url = '/escorts/agency-escorts-v2?agency_page=' + page + '&escort_id=' + escort_id + '&agency_id=' + agency_id + '&ajax=1';

	var data = {'agency_page':page, 'escort_id':escort_id, 'agency_id':agency_id,'ajax':1};
	
	Sceon.AgencyEscorts.Show(data);
	
	return false;
}

Sceon.AgencyEscorts.changeViewType = function (page, escort_id, agency_id, view_type, el) {

	if ( el.hasClass('act') ) return false;

	if ( view_type == "l" ) {
		$$('#agency-escorts-list .btn-view-type.large')[0].addClass('act');
		$$('#agency-escorts-list .btn-view-type.small')[0].removeClass('act');
	} else {
		$$('#agency-escorts-list .btn-view-type.small')[0].addClass('act');
		$$('#agency-escorts-list .btn-view-type.large')[0].removeClass('act');
	}
	
	//var url = '/escorts/agency-escorts-v2?agency_page=' + page + '&escort_id=' + escort_id + '&agency_id=' + agency_id + '&ajax=1';

	var data = {'agency_page':page, 'escort_id':escort_id, 'agency_id':agency_id, 'view_type': view_type, 's':1, 'ajax':1};
	
	Sceon.AgencyEscorts.Show(data);
	
	return false;
}

Sceon.AgencyEscorts.Show = function (data) {
	var container = $$('#agency-escorts-list .content')[0];

	var overlay = new Cubix.Overlay(container, {
		loader: _st('loader-small.gif'),
		position: '50%'
	});
	
	overlay.disable();
	
	new Request({
		url: lang_id + '/escorts/agency-escorts-v2',
		method: 'post',
		data: data,
		onSuccess: function (resp) {
			container.set('html', resp);
			overlay.enable();

			Sceon.initProfileThumbs();
			Sceon.AgencyEscorts.initThumbHover();
		}
	}).send();
}

Sceon.AgencyEscorts.initThumbHover = function() {

	if ( ! $$('#agency-escorts-list .content').length ) return false;

	var container = $$('#agency-escorts-list .content')[0];

	var items = container.getElements('div.item');
	var overlays = items.getElements('div.o');

	container.getElements('div.item').addEvent('mouseenter', function() {
		this.addClass('selected');
		this.getElements('.details')[0].removeClass('none');
		
		overlays.each(function(el){			
			if ( ! el.getParent('div.item')[0].hasClass('selected') ) {
				el.removeClass('none').addClass('overlay');				
			}
		});

	}).addEvent('mouseleave', function() {
		this.getElements('.details').addClass('none');
		this.removeClass('selected');
		
		overlays.each(function(el){
			el.addClass('none').removeClass('overlay');			
		});		
	});
};


window.addEvent('domready', function() {

	Sceon.PhotoRoller.Init();

	Sceon.initProfileV2();

	Sceon.initTips();
	Sceon.initAgencyTips();

	Sceon.initSideMenu();
	Sceon.initTopMenu();
	Sceon.initFooterMenu();

	Sceon.initProfileThumbs();

	Sceon.initSorting();
	Sceon.initShowAllAgencyEscorts();

	Sceon.initAgencySorting();

	Sceon.initSearchBox();

	Sceon.initAgencies();

	Sceon.initListType();

	Sceon.initNewestSlider();

	if (typeof window.history.pushState == 'function') {		
		if ( ! document.location.hash.length ) {			
			history.replaceState({}, '', window.location.pathname);			
		}
	}

	window.addEventListener('popstate', function(event) {
		if (event.state) {
			//location.reload();
			Sceon.Escorts.Load('', {}, true);
		}
	}, false);

	Sceon.Escorts.scrollSpyLoadInit();
});