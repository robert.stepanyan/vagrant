Cubix.Reviews = {};

Cubix.Reviews.container = 'list';
Cubix.Reviews.scrollWrap = 'reviews';

Cubix.Reviews.Init = function (data) {	
	if (data.substr(data.length - 1) === '&') {
		data = data.substring(0, data.length - 1);
	}
	
	var data_obj = {};
	
	if ( data.length ) {
		var data_obj = data.parseQueryString();
	}

	Cubix.Reviews.Show(data_obj, true);
	
	return false;
};

Cubix.Reviews.Show = function(data, allowScroll) {	
	var container = Cubix.Reviews.container;
	
	var myScroll = new Fx.Scroll(window);
	var overlay = new Cubix.Overlay($(container), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	overlay.disable();

    if ($('member_name').get('value').length > 0)
        data.member = $('member_name').get('value');

	new Request({
		url: lang_id + '/reviews/ajax-list',
		method: 'get',
		data: data,
		onSuccess: function (resp) {			
			$(container).set('html', resp);

            Cubix.Reviews.ReloadOptions();

			overlay.enable();
			
			if (allowScroll) {
				myScroll.toElement($(Cubix.Reviews.scrollWrap));
			}
		}
	}).send();
	
	return false;
};

Cubix.Reviews.ReloadOptions = function () {
    if ($('view_type').get('value') == 'l')
    {
        $$('.list').addClass('sel');
        $$('.detailed').removeClass('sel');
    }
    else
    {
        $$('.detailed').addClass('sel');
        $$('.list').removeClass('sel');
    }

    var ord_field = $('ord_field').get('value');
    var ord_dir = $('ord_dir').get('value');

    if (ord_field != 'meet')
    {
        if (ord_dir != 'asc')
        {
            $$('.add-desc').removeClass('none');
            $$('.meet-asc-desc').removeClass('none');
            $$('.add-asc').addClass('none');
            $$('.add-asc-desc').addClass('none');
            $$('.meet-asc').addClass('none');
            $$('.meet-desc').addClass('none');
        }
        else
        {
            $$('.add-asc').removeClass('none');
            $$('.meet-asc-desc').removeClass('none');
            $$('.add-desc').addClass('none');
            $$('.add-asc-desc').addClass('none');
            $$('.meet-asc').addClass('none');
            $$('.meet-desc').addClass('none');
        }
    }
    else
    {
        if (ord_dir != 'asc')
        {
            $$('.meet-desc').removeClass('none');
            $$('.add-asc-desc').removeClass('none');
            $$('.add-desc').addClass('none');
            $$('.meet-asc-desc').addClass('none');
            $$('.add-asc').addClass('none');
            $$('.meet-asc').addClass('none');
        }
        else
        {
            $$('.meet-asc').removeClass('none');
            $$('.add-asc-desc').removeClass('none');
            $$('.add-desc').addClass('none');
            $$('.meet-asc-desc').addClass('none');
            $$('.add-asc').addClass('none');
            $$('.meet-desc').addClass('none');
        }
    }

    if ($('country').get('value'))
    {
        Geography.loadCities($('city'), $('country'));

        var hash = document.location.hash.substring(1);
        if ( ! hash.length ) {
            return {};
        }
        var params = hash.split(';');
        var city_id;

        params.each(function (param) {
            var key_value = param.split('=');

            var key = key_value[0];
            var val = key_value[1];

            if (key != 'city_id') return;

            city_id = val;
        });

        if (city_id)
        {
            $('city').addEvent('pakistan', function(){
                $('city').set('value', city_id);
                mooniformInstance.update();
            });
        }
    }
}

Cubix.Reviews.EReviews = function(data) {
    var container = 'e-reviews';

    var myScroll = new Fx.Scroll(window);
    var overlay = new Cubix.Overlay($(container), {
        loader: _st('loader-small.gif'),
        position: '50%',
        offset: {
            left: 0,
            top: -1
        }
    });

    overlay.disable();

    data.mode = 'ajax';
    data.perpage = $('perpage').get('value');

    var showname = $('e-showname').get('value');
    var escort_id = $('e-escort-id').get('value');

    new Request({
        url: lang_id + '/reviews/' + showname + '-' + escort_id,
        method: 'get',
        data: data,
        onSuccess: function (resp) {
            $('body').set('html', resp);

            overlay.enable();

            myScroll.toElement($(container));

            globInit();
        }
    }).send();

    return false;
}

/****************************************************************/

Cubix.ReviewsHash = {
	_current: '',

	init: function () {
		setInterval('Cubix.ReviewsHash.check()', 100);
	},

	check: function () {
		var hash = document.location.hash.substring(1);

		if (hash != this._current) {
			this._current = hash;
			var data = Cubix.ReviewsHash.Parse();
			Cubix.Reviews.Init(data);
		}
	}
};

Cubix.ReviewsHash.Make = function (filter) {
	var hash = '';
	var sep = ';';
	var value_glue = ',';
	var search_box = $('filter');
	var map = {};

	if ( search_box ) {
		var hidden_inputs = search_box.getElements('input[type=hidden]');
		hidden_inputs.each(function(input) {
			if ( input.get('name') && input.get('value').length ) {
				var index = input.get('name').replace('[]', '');
				if ( map[index] === undefined ) map[index] = new Array();
				map[index].append([input.get('value')]);
			}
		});

		var input_texts = search_box.getElements('input[type=text]');
		input_texts.each(function(input) {
			if ( input.get('value').length > 0 && input.get('name') ) {
				var index = input.get('name').replace('[]', '');
				if ( map[index] === undefined ) map[index] = new Array();
				map[index].append([input.get('value')]);
			}
		});

        var input_checkbox = search_box.getElements('input[type=checkbox]:checked');
        input_checkbox.each(function(input) {
            if ( input.get('value').length > 0 && input.get('name') ) {
                var index = input.get('name').replace('[]', '');
                if ( map[index] === undefined ) map[index] = new Array();
                map[index].append([input.get('value')]);
            }
        });

        var selects = search_box.getElements('select');
        selects.each(function(input) {
            if ( input.getSelected().get("value")[0] ) {
                var index = input.get('name').replace('[]', '');
                if ( map[index] === undefined ) map[index] = new Array();
                map[index].append([input.getSelected().get("value")[0]]);
            }
        });
	}

	map = Object.merge(map, filter);

	for (var i in map) {
		hash += i + '=' + map[i].join(value_glue) + sep;
	}

	return hash;
};

Cubix.ReviewsHash.SetFilter = function (filter) {
	Cubix.ReviewsHash.Set(Cubix.ReviewsHash.Make(filter));

	return false;
};

Cubix.ReviewsHash.Set = function (hash) {
	if ( hash.length ) {
		document.location.hash = '#' + hash;
	}
	else {
		document.location.hash = '';
	}

	return false;
};

Cubix.ReviewsHash.Parse = function () {
	var hash = document.location.hash.substring(1);

	if ( ! hash.length ) {
		return {};
	}

	var params = hash.split(';');

	var filter = '';

	var not_array_params = ['page', 'showname', 'country_id', 'city_id', 'member', 'meeting_date_f', 'meeting_date_t', 'date_added_f', 'date_added_t', 'gender', 'gender_combined', 'view_type', 'ord_field', 'ord_dir'];

	params.each(function (param) {
		var key_value = param.split('=');

		var key = key_value[0];
		var val = key_value[1];

		if ( val === undefined ) return;

		if (key == 'showname')
		{
			$('showname').set('value', val);
		}
        else if (key == 'view_type')
        {
            $('view_type').set('value', val);
        }
        else if (key == 'ord_field')
        {
            $('ord_field').set('value', val);
        }
        else if (key == 'ord_dir')
        {
            $('ord_dir').set('value', val);
        }
		else if (key == 'country_id')
		{
			$('country').set('value', val);
		}
		else if (key == 'city_id')
		{
			$('city').set('value', val);
		}
		else if (key == 'member' && $('mode').get('value') != 'exact')
		{
			$('member').set('value', val);
		}
		else if (key == 'meeting_date_f')
		{
            date_meet_f.selectDate(val);
		}
		else if (key == 'meeting_date_t')
		{
            date_meet_t.selectDate(val);
		}
		else if (key == 'date_added_f')
		{
            date_add_f.selectDate(val);
		}
		else if (key == 'date_added_t')
		{
            date_add_t.selectDate(val);
		}
        else if (key == 'gender')
		{
            var g_arr = val.split(',');

            g_arr.each(function(it) {
                $('gender_' + it).set('checked', 'checked');
            });
		}

		val = val.split(',');

		val.each(function(it) {
			filter += key + ( ( ! not_array_params.contains(key) ) ? '[]=' : '=' ) + it + '&';
		});

        mooniformInstance.update();
	});

	return filter;
};

window.addEvent('domready', function(){
	Sceon.initSideMenuSimple();

    globInit();
});

var mooniformInstance;
var date_meet_f;
var date_meet_t;
var date_add_f;
var date_add_t;

function globInit() {
    mooniformInstance = new Mooniform($$('select, input[type=checkbox]'));

    date_meet_f = new DatePicker($('meeting_date_f'), {
        pickerClass: 'datepicker',
        positionOffset: { x: -10, y: 0 },
        allowEmpty: true,
        format: 'j M Y',
        months: Cubix.Reviews.months,
        days: Cubix.Reviews.days
    });

    date_meet_t = new DatePicker($('meeting_date_t'), {
        pickerClass: 'datepicker',
        positionOffset: { x: -10, y: 0 },
        allowEmpty: true,
        format: 'j M Y',
        months: Cubix.Reviews.months,
        days: Cubix.Reviews.days
    });

    date_add_f = new DatePicker($('date_added_f'), {
        pickerClass: 'datepicker',
        positionOffset: { x: -10, y: 0 },
        allowEmpty: true,
        format: 'j M Y',
        months: Cubix.Reviews.months,
        days: Cubix.Reviews.days
    });

    date_add_t = new DatePicker($('date_added_t'), {
        pickerClass: 'datepicker',
        positionOffset: { x: -10, y: 0 },
        allowEmpty: true,
        format: 'j M Y',
        months: Cubix.Reviews.months,
        days: Cubix.Reviews.days
    });
}

/*--> Private Messaging*/
Sceon.PrivateMessagingPopup = function(participant_id) {
    if ( ! headerVars.currentUser )
    {
        Popup.SignInUp.display();
        return false;
    }

    if ( $('pm-escort-popup') ) {
        $('pm-escort-popup').destroy();
    }

    pmPopup = new moopopup({
        width: 470,
        resizable: false,
        draggable: true,
        url: lang_id + '/private-messaging/send-message-from-profile?participant=' + participant_id,
        overlay: false,
        onComplete: function() {
            var container = this.container;
            container.getElements('.close, .cancel').addEvent('click', function(){pmPopup.close();});

            var overlay = new Cubix.Overlay($$('#pm-escort-popup .pm-popup-body')[0], {color: '#fff', opacity: .6, has_loader: false});

            new Mooniform($$('#pm-escort-popup .styled'));

            Sceon.initRecaptcha();

            container.getElement('.send').addEvent('click', function() {
                overlay.disable();
                new Request.JSON({
                    url: lang_id + '/private-messaging/send-message-from-profile',
                    method: 'POST',
                    data: {
                        message: container.getElement('textarea').get('value'),
                        captcha: container.getElement('input[name="captcha"]').get('value'),
                        participant: participant_id,
                        escort_id: $('pm-escort-id') ? $('pm-escort-id').get('value') : false
                    },
                    onSuccess: function (resp) {
                        container.getElements('span.error').set('html', '');
                        container.getElements('span.err, textarea.err').removeClass('err');
                        if ( resp.status == 'error' ) {
                            for ( field in resp.msgs ) {
                                container.getElement('input[name="' + field + '"], textarea[name="' + field + '"]').getNext('span.error').set('html', resp.msgs[field]);
                                container.getElement('input[name="' + field + '"], textarea[name="' + field + '"]').addClass('err');
                            }

                            container.getElement('span.captcha img').set('src', '/captcha?' + Math.floor(Math.random() * 10000 ));
                            container.getElement('.captcha-input').set('value', '');
                        } else {
                            $$('#pm-escort-popup .pm-popup-body')[0].empty();
                            new Element('p', {
                                'class': 'success-message',
                                html: 'Your message has been sent.'
                            }).inject($$('#pm-escort-popup .pm-popup-body')[0]);

                            setInterval(function() {
                                pmPopup.close();
                            }, 2 * 1000);
                        }

                        overlay.enable();
                    }
                }).send();
            });
        },
        onClose: function() {
        }
    });

    pmPopup.display();


    return false;
};
/*--> Private Messaging*/