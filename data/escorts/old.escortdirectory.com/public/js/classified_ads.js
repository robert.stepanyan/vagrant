/* --> LocationHash */
Cubix.LocationHash = {};

Cubix.LocationHash.Set = function (hash) {
	if ( hash.length ) {
		document.location.hash = '#' + hash;
	}
	else {
		document.location.hash = '';
	}
	
	return false;
}

Cubix.LocationHash.Parse = function () {
	var hash = document.location.hash.substring(1);
	
	if ( ! hash.length ) {
		return [];
	}
	
	var params = hash.split(';');
	
	var filter = '';
	
	var not_array_params = ['page', 'category', 'city_id', 'text', 'pp', 'country_id', 'date_f', 'date_t', 'with_photo', 'ord_dir'];
	
	params.each(function (param) {
		var key_value = param.split('=');
		
		var key = key_value[0];
		var val = key_value[1];

        if (key == 'ord_dir')
        {
            $('ord_dir').set('value', val);
        }
		
		if ( val === undefined ) return;
		
		val = val.split(',');
				
		val.each(function(it) {
			filter += key + ( ( ! not_array_params.contains(key) ) ? '[]=' : '=' ) + it + '&';
		});
	});
	//console.log(filter);
	return filter;
}

Cubix.LocationHash.Make = function (params) {
	var hash = '';
	var sep = ';';
	var value_glue = ',';
	
	var places = $$('#cads-filter-box, .per-page');
	
	var map = {};
	places.each(function(place) {
		var inputs = place.getElements('input[type=text]');
		inputs.append(place.getElements('input[type=hidden]'));
		inputs.each(function(input) {
			if ( input.get('value').length ) {
                if (!input.get('name')) return;
				var index = input.get('name').replace('[]', '');
				if ( map[index] === undefined ) map[index] = new Array();
				map[index].append([input.get('value')]);
			}
		});
		
		var selects = place.getElements('select');
		selects.each(function(select) {
			
			if ( select.getSelected().get('value') != 0 ) {
				var index = select.get('name').replace('[]', '');
				if ( map[index] === undefined ) map[index] = new Array();
				map[index].append([select.getSelected().get('value')]);
			}
		});
	});

    if (parseInt($$('input[name=with_photo]:checked').get('value')) == 1)
        map['with_photo'] = [1];
    else
        map['with_photo'] = [0];

	map = $merge(map, params);
	
	for (var i in map) {
		hash += i + '=' + map[i].join(value_glue) + sep;
	}
	
	return hash;
}
/* <-- */

/* --> HashController */
Cubix.HashController = {
	_current: '',
	
	init: function () {
		setInterval('Cubix.HashController.check()', 100);
	},
	
	check: function () {
		var hash = document.location.hash.substring(1);
		
		if (hash != this._current) {
			
			this._current = hash;
					
			var data = Cubix.LocationHash.Parse();
			
			
			Cubix.ClassifiedAds.Load(data);
			
			Cubix.ClassifiedAds.LoadFilters(data);
		}
	}
};
/* <-- */

Cubix.Filter = {};
Cubix.Filter.Set = function (filter) {
	Cubix.LocationHash.Set(Cubix.LocationHash.Make(filter));

	return false;
}

Cubix.ClassifiedAds = {};
Cubix.ClassifiedAds.container = 'list';
Cubix.ClassifiedAds.filter_container = 'filter-cont';
Cubix.ClassifiedAds.filter_url = '';
Cubix.ClassifiedAds.list_url = '';

Cubix.ClassifiedAds.Load = function (data) {
	
	var url = Cubix.ClassifiedAds.list_url;
	
	var overlay = new Cubix.Overlay($(Cubix.ClassifiedAds.container), {
		loader: _st('loader-small.gif'),
		position: '50%'
	});
	
	overlay.disable();
	
	new Request({
		url: url,
		method: 'POST',
		data: data,
		onSuccess: function (resp) {
			$(Cubix.ClassifiedAds.container).set('html', resp);

            var res_count = $('res_count').get('value');

            if (!res_count) res_count = 0;

            $$('.res_count').set('html', res_count);

            Cubix.ClassifiedAds.ReloadOptions();

			Slimbox.scanPage();
			
			//Cubix.ClassifiedAds.InitPerPage();
			
			overlay.enable();
		}
	}).send();
	
	return false;
}

Cubix.ClassifiedAds.ReloadOptions = function () {
    var ord_dir = $('ord_dir').get('value');

    if (ord_dir != 'asc')
    {
        $$('.add-desc').removeClass('none');
        $$('.add-asc').addClass('none');
    }
    else
    {
        $$('.add-asc').removeClass('none');
        $$('.add-desc').addClass('none');
    }
}

Cubix.ClassifiedAds.LoadFilters = function (data) {
	
	var url = Cubix.ClassifiedAds.filter_url;
	
	var overlay = new Cubix.Overlay($(Cubix.ClassifiedAds.filter_container), {
		loader: _st('loader-small.gif'),
		position: '50%'
	});
	
	overlay.disable();
	
	new Request({
		url: url,
		method: 'POST',
		data: data,
		onSuccess: function (resp) {			
			$(Cubix.ClassifiedAds.filter_container).set('html', resp);
			overlay.enable();
			
			Cubix.HashController.InitAll();			
		}
	}).send();
	
	return false;
}

Cubix.ClassifiedAds.InitFilters = function(){
    var mooniformInstance = new Mooniform($('filter-cont').getElements('select, input[type=checkbox]'));

    var date_f = new DatePicker($('date_f'), {
        pickerClass: 'datepicker',
        positionOffset: { x: -10, y: 0 },
        allowEmpty: true,
        format: 'j M Y',
        months: Cubix.ClassifiedAds.months,
        days: Cubix.ClassifiedAds.days
    });

    var date_t = new DatePicker($('date_t'), {
        pickerClass: 'datepicker',
        positionOffset: { x: -10, y: 0 },
        allowEmpty: true,
        format: 'j M Y',
        months: Cubix.ClassifiedAds.months,
        days: Cubix.ClassifiedAds.days
    });

    if ($('country').get('value'))
    {
        Geography.loadCitiesClassifiedAds($('city'), $('country'));

        var hash = document.location.hash.substring(1);
        if ( ! hash.length ) {
            return {};
        }
        var params = hash.split(';');
        var city_id;

        params.each(function (param) {
            var key_value = param.split('=');

            var key = key_value[0];
            var val = key_value[1];

            if (key != 'city_id') return;

            city_id = val;
        });

        if (city_id)
        {
            $('city').addEvent('pakistan', function(){
                $('city').set('value', city_id);
                mooniformInstance.update();
            });
        }
    }

	$$('.search').addEvent('click', function(e){
		e.stop();
		Cubix.LocationHash.Set(Cubix.LocationHash.Make());
	});

    $$('.reset').addEvent('click', function(e){
        e.stop();
        Cubix.ClassifiedAds.reset();
        mooniformInstance.update();
        Cubix.LocationHash.Set(Cubix.LocationHash.Make());
    });
};

Cubix.ClassifiedAds.reset = function(){
    $('country').set('value', '');
    $('city').set('value', '');
    $('category').set('value', '');
    $('text').set('value', '');
    $('date_f').set('value', '');
    $('date_t').set('value', '');
    $('with_photo').set('checked', '');
}

/*Cubix.ClassifiedAds.InitPerPage = function(){
	$$('.per-page select').addEvent('change', function(e){
		e.stop();
		Cubix.LocationHash.Set(Cubix.LocationHash.Make());
	});
};*/

Cubix.ClassifiedAds.initForm = function() {
    var mooniformInstance = new Mooniform($('ad-form').getElements('select'));

    if ( typeof(tinyMCE) !== 'undefined' ) {
        tinyMCE.init({
            mode : "exact",
            elements: "txt",
            theme : "advanced",

            plugins: "emotions,advhr,preview,paste",
            theme_advanced_toolbar_location	  : "top",
            theme_advanced_toolbar_align	  : "left",
            editor_selector : 'about-field',
            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,undo,redo,|,pastetext,pasteword,fullscreen,emotions,preview",
            theme_advanced_buttons2 : "",
            theme_advanced_buttons3_add : "",
            remove_script_host : false,
            relative_urls : false,
            convert_urls : false
        });
    }

    Cubix.ClassifiedAds.initDeletePhotos();
    Cubix.ClassifiedAds.initRemoveBtns();

    $('btn-city-add').addEvent('click', function(e){
        e.stop();

        Cubix.ClassifiedAds.initCitiesAdd();
        Cubix.ClassifiedAds.initRemoveBtns();
    });
}

Cubix.ClassifiedAds.initCitiesAdd = function() {
    var cities_count = 3;

    var popup = $$('.place-ad')[0];

    var city_select = popup.getElement('select[name=city_id]');
    var selected_cities = popup.getElement('#h-cities');
    var hiddens = popup.getElements('.h-inner')[0];

    var city_title = city_select.getSelected().get('html')[0];
    var city_id = city_select.getSelected().get('value')[0];

    if ( ! city_id ) return;

    if ( hiddens.getElement('input[value=' + city_id + ']') ) return;

    if ( hiddens.getElements('input[type=hidden]').length == cities_count ) {
        alert('You can select max ' + cities_count + ' cities');
        return;
    }

    new Element('div', {
        'class': 'clear'
    }).inject(selected_cities, 'top');
    var city_title = new Element('p', {
        'html': city_title
    }).inject(selected_cities, 'top');
    var btn_remove = new Element('a', {
        'href': city_id
    }).inject(selected_cities, 'top');

    new Element('input', {
        'type': 'hidden',
        'name': 'cities[]',
        'value': city_id
    }).inject(hiddens, 'top');
}

Cubix.ClassifiedAds.initRemoveBtns = function() {
    var popup = $$('.place-ad')[0];

    var selected_cities = popup.getElement('#h-cities');
    var hiddens = popup.getElements('.h-inner')[0];

    selected_cities.getElements('a').addEvent('click', function(e){
        e.stop();

        var city_id = this.get('href');

        this.getNext('div').destroy();
        this.getNext('p').destroy();
        this.destroy();

        hiddens.getElement('input[value=' + city_id + ']').destroy();
    });
}

Cubix.ClassifiedAds.deletePhoto = function(el) {
    var photo_inputs = $$('.photo-inputs')[0];

    var req = new Request.JSON({
        method: 'get',
        url: '/classified-ads/ajax-remove-photo?image_id=' + el.get('rel'),
        onComplete: function(resp) {
            if ( resp.status == 'success' ) {
                el.getParent('div').destroy();

                new Element('input', {
                    'type': 'file',
                    'class': 'w260',
                    'name' : 'photo_' + String.uniqueID()
                }).inject(photo_inputs, 'top');
            } else {

            }
        }
    }).send();
}

Cubix.ClassifiedAds.initDeletePhotos = function() {
    if ( $$('.pr-photos a.delete') ) {
        $$('.pr-photos a.delete').addEvent('click', function(e){
            e.stop();

            Cubix.ClassifiedAds.deletePhoto(this);
        });
    }
}

Cubix.HashController.InitAll = function(){
	Cubix.HashController.init();
	
	Cubix.ClassifiedAds.InitFilters();
	
	//Cubix.ClassifiedAds.InitPerPage();
};

window.addEvent('domready', function () {
    if (Cubix.ClassifiedAds.init)
    {
        Cubix.HashController.InitAll();

        Slimbox.scanPage();
    }
    else if (Cubix.ClassifiedAds.initStep1)
    {
        Cubix.ClassifiedAds.initForm();
    }
});
