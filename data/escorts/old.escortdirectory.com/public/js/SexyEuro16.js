Cubix.SexyEuro16 = {
    // parameters
    escortPhotos: [],
    popup: null,
    closeBtn: null,
    overlay: null,

    // methods
    initialize: function() {
        this.setElements();
        this.eventManager();
    },

    setElements: function() {
        this.escortPhotos = $$('.escort-photo');
        this.overlay = $$('.overlay')[0];
        this.closeBtn = $('closeBtn');
    },

    eventManager: function() {
        var self = this;

        // clicking the escort photo
        this.escortPhotos.addEvent('click', function() {
            self.openOverlay();
        });

        // clicking the close button
        this.closeBtn.addEvent('click', function() {
            self.closeOverlay();
        });

    },

    openOverlay: function() {
        this.overlay.fade('in');

        // preventing the page from scrolling
        document.addEvent('mousewheel', function(e) {
            e.preventDefault();
        });
    },

    closeOverlay: function() {
    	this.overlay.fade('out');
    	document.removeEvents();
    },

    getEscortsInfo: function () {
		var req = new Request({
			method: 'get',
			url: '/sexy-euro-16/',
			data: { 'do' : '1' },
			onRequest: function() { alert('Request made. Please wait...'); },
			onComplete: function(response) { alert('Response: ' + response); }
			}).send();
	}

};

