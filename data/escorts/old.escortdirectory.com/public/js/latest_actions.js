Cubix.LatActionsPopup = {};
Cubix.LatActionsPopup.inProcess = false;
Cubix.LatActionsPopup.url = '';

Cubix.LatActionsPopup.Show = function (box_height, box_width, el, event) {
	//if ( Cubix.LatActionsPopup.inProcess ) return false;

	/*var page_overlay = new Cubix.Overlay(el, { has_loader: false, opacity: 0.5, color: '#000' });
	page_overlay.disable();*/

	var y_offset = 30;

	var left = -95;
	var bottom = 120;
	if ( $$('.xl')[0] ) {
		left = -40;
	}

	var container = new Element('div', { 'class': 'LatActionsPopup-wrapper'}).setStyles({
		left: left, // window.getWidth() / 2 - box_width / 2,
		bottom: bottom, //window.getHeight() / 2 - box_height / 2 + window.getScroll().y - y_offset,
		opacity: 0
	}).inject(el);
	
	Cubix.LatActionsPopup.inProcess = true;

	var escort_id = el.get('escort_id');
	var act = el.get('act');
	var entity_id = el.get('entity_id');

	//if ( act != 1 && act != 5 && act != 3 && act != 4  ) return false;

	new Request({
		url: Cubix.LatActionsPopup.url + '&escort_id=' + escort_id + '&act=' + act + '&entity_id=' + entity_id,
		method: 'get',
		onSuccess: function (resp) {			
			Cubix.LatActionsPopup.inProcess = false;
			container.set('html', resp);	

			var close_btn = new Element('div', {
				html: '',
				'class': 'LatActionsPopup-close-btn-v2'
			}).inject(container);

			close_btn.addEvent('click', function() {
				$$('.LatActionsPopup-wrapper').destroy();
				page_overlay.enable();
			});
			
			Slimbox.scanPage();

			container.tween('opacity', '1');
		},
		onFailure: function(){
			Cubix.LatActionsPopup.inProcess = false;
		},
		onRequest: function(){
			Cubix.LatActionsPopup.inProcess = false;
		}		
	}).send();
	
	return false;
}

Cubix.LatActionsPopup.InitDetailsPopups = function() {
	
	$$('.la-wrapper').removeEvents('mouseenter');
	$$('.la-wrapper').removeEvents('mouseleave');
	$$('.la-wrapper').addEvent('mouseenter', function(e){
		e.stop();
		Cubix.LatActionsPopup.Show(100, 100, this, e);
		//console.log(e.page.x, e.page.y);
	});
	
	$$('.la-wrapper').addEvent('mouseleave', function(e){
		e.stop();
		this.getElements('.LatActionsPopup-wrapper').destroy();
	});
}

/* --> LocationHash */
Cubix.LocationHash = {};

Cubix.LocationHash.Set = function (hash) {
	if ( hash.length ) {
		document.location.hash = '#' + hash;
	}
	else {
		document.location.hash = '';
	}
	
	return false;
}

Cubix.LocationHash.Parse = function () {
	var hash = document.location.hash.substring(1);
	
	if ( ! hash.length ) {
		return {};
	}
	
	var params = hash.split(';');
	
	var filter = '';
	
	var not_array_params = ['page', 'country', 'city', 'action_type', 'pr'];
	
	params.each(function (param) {
		var key_value = param.split('=');
		
		var key = key_value[0];
		var val = key_value[1];
		
		if ( val === undefined ) return;
		
		val = val.split(',');
				
		val.each(function(it) {
			filter += key + ( ( ! not_array_params.contains(key) ) ? '[]=' : '=' ) + it + '&';
		});
	});
	
	return filter;
}

Cubix.LocationHash.Make = function (params) {
	
	var hash = '';
	var sep = ';';
	var value_glue = ',';
	
	var places = $$('#la-header-wrp .header');
	
	var map = {};
	places.each(function(place) {
		var inputs = place.getElements('input[type=text]');
		inputs.append(place.getElements('input[type=hidden]'));
		//inputs.append(place.getElements('input[type=text]'));
		inputs.each(function(input) {
			if ( input.get('value').length ) {
				var index = input.get('name').replace('[]', '');
				if ( map[index] === undefined ) map[index] = new Array();
				map[index].append([input.get('value')]);
			}
		});
		
		var selects = place.getElements('select');
		selects.each(function(select) {
			
			if ( select.getSelected().get('value') != 0 ) {
				var index = select.get('name').replace('[]', '');
				if ( map[index] === undefined ) map[index] = new Array();
				map[index].append([select.getSelected().get('value')]);
			}
		});
	});
	
	map = $merge(map, params);
	
	for (var i in map) {
		hash += i + '=' + map[i].join(value_glue) + sep;
	}
	
	return hash;
}
/* <-- */

/* --> HashController */
Cubix.HashController = {
	_current: '',
	
	init: function () {
		setInterval('Cubix.HashController.check()', 100);
	},
	
	check: function () {
		var hash = document.location.hash.substring(1);
		
		if (hash != this._current) {
			this._current = hash;
					
			var data = Cubix.LocationHash.Parse();
			
			var myScroll = new Fx.Scroll(window);
			myScroll.toElement($('top-menu-container'));
			
			Cubix.LatestActions.Load(data);
			Cubix.LatestActions.LoadHeader(data);
		}
	}
};
/* <-- */

Cubix.Filter = {};
Cubix.Filter.Set = function (filter) {
		
	Cubix.LocationHash.Set(Cubix.LocationHash.Make(filter));

	return false;
}

Cubix.LatestActions = {};

Cubix.LatestActions.container = 'la-escorts-wrp';
Cubix.LatestActions.header_container = 'la-header-wrp';

Cubix.LatestActions.header_url = '';
Cubix.LatestActions.list_url = '';

Cubix.LatestActions.Load = function (data) {
	
	var url = Cubix.LatestActions.list_url;
	
	var overlay = new Cubix.Overlay($(Cubix.LatestActions.container), {
		loader: _st('loader-small.gif'),
		position: '50%'
	});
	
	overlay.disable();
	
	new Request({
		url: url,
		method: 'POST',
		data: data,
		onSuccess: function (resp) {
			
			$(Cubix.LatestActions.container).set('html', resp);
			
			overlay.enable();
			
			Cubix.LatActionsPopup.InitDetailsPopups();
		}
	}).send();
	
	return false;
}

Cubix.LatestActions.LoadHeader = function (data) {
	
	var url = Cubix.LatestActions.header_url;
	
	var overlay = new Cubix.Overlay($(Cubix.LatestActions.header_container), {
		loader: _st('loader-small.gif'),
		position: '50%'
	});
	
	overlay.disable();
	
	new Request({
		url: url,
		method: 'POST',
		data: data,
		onSuccess: function (resp) {			
			$(Cubix.LatestActions.header_container).set('html', resp);
			overlay.enable();
			
			Cubix.HashController.InitAll();			
		}
	}).send();
	
	return false;
}

Cubix.LatestActions.InitFilters = function() {
	var elements = container.getElements('select');	
	var moo = new Mooniform(elements);
	
	$$('#la-header-wrp select').addEvent('change', function(e){
		e.stop();
		Cubix.LocationHash.Set(Cubix.LocationHash.Make());
	});
};

Cubix.LatestActions.InitViewButtons = function(){	
	$$('#la-header-wrp .la-list-type a').addEvent('click', function(e){
		e.stop();
		
		if ( this.hasClass('la-grid') ) {
			Cubix.Filter.Set({ pr: ['5'] });
		} else {
			Cubix.Filter.Set({ pr: ['3'] });
		}
	});
};

Cubix.HashController.InitAll = function(){
	Cubix.HashController.init();
	
	Cubix.LatestActions.InitFilters();
	
	Cubix.LatestActions.InitViewButtons();
	
	Cubix.LatActionsPopup.InitDetailsPopups();
};

window.addEvent('domready', function () {
	Cubix.HashController.InitAll();

	Sceon.initSideMenuSimple();
});
