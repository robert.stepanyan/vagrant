<?php

class ClientHotelBlacklistController extends Zend_Controller_Action
{
	public static $linkHelper;

	/**
	 * @var Zend_Session_Namespace
	 */
	protected $_session;

	public function init()
	{
		$this->_request->setParam('no_tidy', true);
		$this->view->layout()->setLayout('private');
		
		$anonym = array('signup', 'signin', 'forgot', 'forgot-success', 'activate', 'check', 'sign-in-up', 'change-pass');
		$this->blacklisted_usernames = array( 'admin' , 'moderator', 'webmaster' ,'manager', 'sales' );
		$this->user = Model_Users::getCurrent();

		if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {
			$this->_response->setRedirect($this->view->getLink('signin'));
			return;
		}
		
		$this->view->headTitle('Private Area', 'PREPEND');

		$this->_session = new Zend_Session_Namespace('private');
	}
	
	public function indexAction()
	{
		
	}

	public function loadAction()
	{
		$this->view->layout()->disableLayout();
		$types = array('clients', 'hotels');
		$req = $this->_request;
		$type = $this->view->type = $req->type;
		$country_id = $this->view->country_id = $req->country_id;

		if ( ! in_array($type, $types) ) die("Invalid Type");

		$per_pages = $this->view->per_pages = array(20, 50, 100);

		$page = 1;
		$per_page = $this->view->per_page = $this->_request->getParam('per_page', 20);

		if ( ! in_array($per_page, $per_pages) ) {
			$per_page = $this->view->per_page = $per_pages[0];
		}

		if($this->_getParam('page')){
			$page = $this->_getParam('page');
		}

		$search_str = $this->view->search_str = $req->search_str;

		$params = array($search_str, $country_id, $page, $per_page, $type, $req->lang_id);
		$data = Cubix_Api::getInstance()->call('getBlacklistedClientsHotels', array($params));

		// Entry part, get entry from API
		$adminEntries = Cubix_Api::getInstance()->call('getAdminBlacklistEntries');
		$this->view->adminEntries = $adminEntries;

		// Check if in cookie set show entry, show entry
		$show_admin_entries = false;
		if ( isset($_COOKIE['is_show_admin_entr']) ) {
			if ( $_COOKIE['is_show_admin_entr'] == 1 ) {
				$show_admin_entries = true;
			}
		}
		$this->view->show_admin_entries = $show_admin_entries;
		//
		
		$result = $this->view->result = $data['clients'];
		$count = $this->view->count = $data['count'];
		$this->view->page = $page;
		$this->view->per_page = $per_page;

		$m_country = new Cubix_Geography_Countries();
		$this->view->countries = $m_country->ajaxGetAll(false);

		$show_admin_entries = false;
		if ( isset($_COOKIE['is_show_admin_entr']) ) {
			if ( $_COOKIE['is_show_admin_entr'] == 1 ) {
				$show_admin_entries = true;
			}
		}

		$this->view->show_admin_entries = $show_admin_entries;
	}

	public function attachedImageBlacklistAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$images = new Cubix_ImagesCommon();
		$error = false;
		$extension = end(explode(".", $_FILES["Filedata"]["name"]));

		if ($error) {

			$return = array(
				'status' => '0',
				'error' => $error
			);

		} else {
			if ( strlen($_FILES['Filedata']['tmp_name']) ) {
				$photo = $images->save($_FILES['Filedata']);

				$return = array(
					'status' => '1',
					'id' => $photo['image_id']
				);
			}
		}
		
		echo json_encode($return);
	}

	public function addAction()
	{
		$req = $this->_request;
		
		$validator = new Cubix_Validator();
		$data = array();
		
		if ( $req->isPost() )
		{
			$type = $req->type;
			$date = intval($req->date);
			$name = $req->name;
			$phone = $req->phone;
			$comment = $req->comment;
			$country_id = $req->country_id;
			$city_id = $req->city_id;

			if(!empty($req->attach) && $type == 'clients'){
				if(count($req->attach) > 1){
					// only in this project if attach count > 1, array come double
					$photos = array();
					foreach ($req->attach as $k => $a) {
						$photos[] = $req->attach[$k][$k];
					}
				}else{
					$photos = $req->attach;
				}
			}

			if ( ! $city_id ) {
				$city_id = null;
			}

			if ( ! $date ) {
				$validator->setError('date', __('date_required'));
			}
			else if ( $date > time() ) {
				$validator->setError('date', __('date_is_on_future'));
			}

			if ( ! $country_id ) {
				$validator->setError('country', __('country_required'));
			}

			if ( $type == "clients" ) {
				if ( ! strlen($name) && ! strlen($phone) ) {
					$validator->setError('name_or_phone', __('name_or_phone_required'));
				}
			} else {
				if ( ! strlen($name) ) {
					$validator->setError('hotel_name', __('hotel_name_required'));
				}
			}

			if ( ! strlen($comment) ) {
				$validator->setError('err_comment', __('comment_required'));
			}

			if ( $validator->isValid() ) {

				$data = array(
					'application_id' => Cubix_Application::getId(),
					'user_id' => $this->user->id,
					'date' => $date,
					'name' => $name,
					'country_id' => $country_id,
					'city_id' => $city_id,
					'phone' => $phone,
					'comment' => $comment,
					'type' => $type
				);
				if($photos){
					$data['photos'] = $photos;
				}


				Cubix_Api::getInstance()->call('addClientHotelToBlacklist', array($data));
			}

			die(json_encode($validator->getStatus()));
		}
	}
}
