<?php

class OnlineBillingController extends Zend_Controller_Action
{

	public function init()
	{
		
	}
	
	public function indexAction()
	{
		
	}

	public function paymentAction()
	{
		
		if ( $this->_request->isPost() ) {
			//Temporary redirect to paygate456, katie
			header('Location:'. 'http://www.paygate456.com');
			return;
			$amount = str_replace(',', '', $this->_request->price);
			
			if ( $amount ) {
				try {
					/*$epg_payment = new Model_EpgGateway();
					$reference = 'ED-' . md5(rand(100000, 1000000000));
					$token = $epg_payment->getTokenForAuth($reference, $amount, 'http://www.escortdirectory.com/online-billing-v2/epg-response');

					header('Location:'. $epg_payment->getPaymentGatewayUrl($token));*/
					
					
					$nbx = new Model_NetbanxAPI();
	
					$order = $nbx->create_order(
						array(
							'total_amount'	=> $amount * 100,
							'currency_code'	=> 'EUR',
							'merchant_ref_num'	=> md5(time()),
							'redirect'	=> array(
								array(
									'rel'	=> 'on_success',
									'returnKeys'	=> array('id'),
									'uri'	=> 'http://www.escortdirectory.com/online-billing-v2/epg-response'
								)
							),
							/*'link'		=> array(
								array(
									'rel'	=> 'cancel_url',
									'uri'	=> 'https://www.adspay789.com'
								),
								array(
									'rel'	=> 'return_url',
									'uri'	=> 'https://www.adspay789.com'
								)
							)*/
						)
					);
					header('Location:'. $order->link[0]->uri);
				} catch(Exception $ex) {
					die('error');
				}
			}
		}
	}
	
	public function paymentSuccessAction()
	{
		$token = $_GET['Token'];
		$epg_payment = new Model_EpgGateway();
		$this->view->responce = $response = $epg_payment->getTransactionStatus($token);
		
		if ( ! $token || ! $response['TransactionId'] ) {
			$this->_helper->redirector->gotoUrl($this->view->getLink('payment'));
		}
	}
}