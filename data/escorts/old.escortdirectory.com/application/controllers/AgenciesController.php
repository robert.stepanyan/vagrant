<?php

class AgenciesController extends Zend_Controller_Action
{
	public static $linkHelper;
	
	public function init()
	{
		self::$linkHelper = $this->view->getHelper('GetLink');
	}

	public function indexAction()
	{
		
	}

	public function listAction()
	{
		$cache = Zend_Registry::get('cache');
		$this->_helper->viewRenderer->setScriptAction("index");

		$per_page = 40;
		$this->view->p_id = $this->_request->getParam('p_id');
		
		$this->view->static_page_key = 'agencies';
		$segmented = $this->view->segmented = $this->_getParam('segmented', 0);

		$list_types = array(/*'xl-gallery', 'gallery',*/ 'list', 'simple');

		if ( isset($_COOKIE['list_type_a']) && $_COOKIE['list_type_a'] ) {
			$list_type = $_COOKIE['list_type_a'];

			if ( ! in_array($list_type, $list_types) ) {
				$list_type = $list_types[0];
			}

			$this->view->render_list_type = $list_type;
		} else {
			$list_type = $list_types[0];
			$this->view->render_list_type = $list_types[0];
		}

		if ( $list_type == 'xl-gallery' ) {
			$per_page = 21;
		}

		if ( $list_type == 'simple' ) {
			$per_page = 70;
		}

		//Currency rates log
		$default_currency = "USD";
		$current_currency = $this->_getParam('currency', $default_currency);

		if ( $current_currency != $_COOKIE['currency'] && $this->_request->s ) {
			setcookie("currency", $current_currency, strtotime('+1 year'), "/", "www." . preg_replace('#www.|dev.#', '', $_SERVER['HTTP_HOST']));
			$_COOKIE['currency'] = $current_currency;			
		}

		if ( isset($_COOKIE['currency']) ) {
			$current_currency = $_COOKIE['currency'];
			$this->_request->setParam('currency', $current_currency);
		}

		$this->view->current_currency = $current_currency;
		//Currency rates log

		$default_sorting = "alpha";
		$current_sorting = $this->_getParam('agency_sort', $default_sorting);

		$sort_map = array(
			'newest' => 'date_registered DESC',
			'last_modified' => 'cd.last_modified DESC',
			'last-connection' => 'refresh_date DESC',
			'close-to-me' => 'cd.id ASC',
			'most-viewed' => 'hit_count DESC',
			'random' => 'RAND()',
			'by-country' => 'cr.title_en ASC',		
			'by-city' => 'ct.title_en ASC',			
			'alpha' => 'cd.club_name ASC',			
		);

		if ( !array_key_exists($current_sorting, $sort_map) ) {
			$current_sorting = $default_sorting;
			$this->_request->setParam('agency_sort', $default_sorting);
		}

		if ( $current_sorting != $_COOKIE['agency_sort'] && $this->_request->s ) {
			setcookie("a_sorting", $current_sorting, strtotime('+1 year'), "/", "www." . preg_replace('#www.|dev.#', '', $_SERVER['HTTP_HOST']));
			setcookie("a_sortingMap", $sort_map[$current_sorting]['map'], strtotime('+1 year'), "/", "www." . preg_replace('#www.|dev.#', '', $_SERVER['HTTP_HOST']));
			$_COOKIE['a_sorting'] = $current_sorting;
			$_COOKIE['a_sortingMap'] = $sort_map[$current_sorting]['map'];
		}

		if ( isset($_COOKIE['a_sorting']) /*&& ! $this->_request->isPost()*/ ) {
			$current_sorting = $_COOKIE['a_sorting'];
			$this->_request->setParam('agency_sort', $current_sorting);
		}

		if ( $current_sorting ) {
			$tmp = $sort_map[$current_sorting];
			unset($sort_map[$current_sorting]);
			$sort_map = array_merge(array( $current_sorting => $tmp), $sort_map);
		}

		if ( $current_sorting ) {
			$sorting = $current_sorting;
		}

		$this->view->sort_map = $sort_map;
		$this->view->current_sort = $current_sorting;

		$p_top_category = $this->view->p_top_category = $this->_request->getParam('p_top_category', 'agencies');
		$p_city_slug = $this->view->p_city_slug = $this->_request->p_city_slug;
		$p_city_id = $this->view->p_city_id = (int) $this->_request->p_city_id;
		$online_now = $this->view->online_now = (int)$this->_request->online_now;

		$p_country_slug = $this->view->p_country_slug = $this->_request->p_country_slug;
		$p_country_id = $this->view->p_country_id = (int) $this->_request->p_country_id;

		$page = intval($this->_request->page);
		if ( $page == 0 ) {
			$page = 1;
		}


		/*if ( ! $segmented ) {
			$per_page = $page * $per_page;
			$page = 1;
		}*/

		$geoData = array();
		if ( $sorting == 'close-to-me' ) {
			$geoData = Cubix_Geoip::getClientLocation();
		}

		$m_agencies = new Model_Agencies();

		$count = 0;
		$agencies = $m_agencies->getAll($page, $per_page, $p_city_id, $sorting, $p_country_id, $count, $list_type, $geoData, $online_now);

		$this->view->count = $count;
		$this->view->page = $page;
		$this->view->per_page = $per_page;
		$this->view->escorts = $agencies;

		$cache_key_bec = sha1("button_escort_counts_" . Cubix_Application::getId() . $p_country_id . $p_city_id . $online_now);
        if ( ! $button_escort_counts = $cache->load($cache_key_bec) ) {
            $button_escort_counts = Model_EscortsV2::getButtonEscortsCounts($p_country_id, $p_city_id, $online_now);
            $cache->save($button_escort_counts, $cache_key_bec, array());
        }
        $this->view->button_escort_counts = $button_escort_counts;

        $cache_key_bac = sha1("button_agency_counts_" . Cubix_Application::getId() . $p_country_id . $p_city_id . $online_now);
        if ( ! $button_agency_count = $cache->load($cache_key_bac) ) {
            $button_agency_count = Model_Agencies::getButtonAgenciesCounts($p_country_id, $p_city_id, $online_now);
            $cache->save($button_agency_count, $cache_key_bac, array());
        }
        $this->view->button_agency_count = $button_agency_count;

		
		if ( ! is_null($this->_getParam('ajax')) ) {
			$this->view->is_ajax = true;

			$side_bar = $this->view->render('escorts/side-bar-countries.phtml');
			$flags = $this->view->render('escorts/flags.phtml');
			$body = $this->view->render('agencies/index.phtml');

			$top_menu = $this->view->render('escorts/top-menu.phtml');
			$footer_menu = $this->view->render('escorts/footer-menu.phtml');

			$entity = 'agencies';
			$tpl_data = array();
			$primary_id = 0;

			$seo = $this->view->seo($entity, $primary_id, $tpl_data);

			$seo_title = $seo->title;
	        if ( $this->view->page > 1 ) {
	            $seo_title = $seo->title . ' page ' . $this->view->page;
	        }

			die(json_encode(array('body' => $body, 'escorts_count' => count($agencies), 'head_title' => $seo_title, 'top_menu' => $top_menu, 'footer_menu' => $footer_menu, 'side_bar' => $side_bar, 'flags' => $flags/*, 'sort_box' => $sort_box, 'city_country_title' => $city_country_title, 'search_box' => $search_box*/)));
		}
	}

	public function showToolTipAction()
	{
		$this->view->layout()->disableLayout();

		$agency_id = (int) $this->_request->agency_id;
		$type = $this->_request->type;
		$types = array('ec', 'vec');

		if ( ! in_array($type, $types) ) die('haha');

		$m_agencies = new Model_Agencies();
		$this->view->data = $m_agencies->getCountsById($agency_id);
		$this->view->type = $type;		
	}
	
	public function showAction()
	{
		$this->view->layout()->setLayout('main');

		if ( ! is_null($this->_getParam('ajax')) ) {
			$this->view->layout()->disableLayout();			
			$this->view->is_ajax = true;
		}

		$this->_helper->viewRenderer->setScriptAction("profile/profile");

		$agency_name = $this->_request->agencyName;
		$agency_id = (int) $this->_request->agency_id;

		$m_agencies = new Model_Agencies();

		$agency = $this->view->agency = $m_agencies->getLocalById($agency_id);

		if ( $agency ) {
			$params = array('e.agency_id = ?' => $agency_id);

			$escorts_count = 0;
			$this->view->a_escorts = $aaa = Model_Escort_List::getAgencyEscorts($params, 'newest', 1, 100, $escorts_count);

		} else {
			header("HTTP/1.1 301 Moved Permanently");
			header("Location: " . $this->view->getLink('navigation-link', array('top_category' => 'agencies')));
		}

		if ( ! is_null($this->_getParam('ajax')) ) {
			$body = $this->view->render('agencies/profile/profile.phtml');
			$flags = $this->view->render('escorts/flags.phtml');

			die(json_encode(array('body' => $body, 'flags' => $flags)));
		}
	}	
}
