	<?php 

/**
* 
*/
class EscortsApiController extends Zend_Controller_Action
{
	
	public function init()
	{
		$this->model = new Model_EscortsApi();
		if($this->model->checkToken($this->_getParam('token'))) {
			header('Content-Type: application/json');
		}
		else{
			header('HTTP/1.0 401 Unauthorized');
			die;
		}
	}
	
	public function indexAction()
	{
		ini_set('memory_limit', '1024M');
		set_time_limit(0);
		$filter = array();
		if($this->_getParam('last_days')){
			$filter['last_days'] = intval($this->_getParam('last_days')); 
		}		
		$escorts = $this->model->get($filter);
		echo json_encode($escorts);die;
	}

	public function voteForEscort()
	{
		
	}
}