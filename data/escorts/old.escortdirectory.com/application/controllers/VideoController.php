<?php


class VideoController extends Zend_Controller_Action {

		protected $session;
		private $model;
		private $user;
		protected $config;
		private $user_id;
		
		public function init()
		{
			 $anonym = array();
			 $this->user = Model_Users::getCurrent();
			 			 
			if (!$this->user  && !in_array($this->_request->getActionName(), $anonym))
			{
				$this->_redirect($this->view->getLink('signin'));
				return;
			}
			if($this->user['user_type']!='agency' && $this->user['user_type'] != 'escort' )
			{
					$this->_redirect($this->view->getLink('private'));
			}
			$this->config =  Zend_Registry::get('videos_config');
			$this->user_id = $this->user['user_type']!='agency' ? $this->user['escort_data']['escort_id'] : $this->user['agency_data']['agency_id'];
			
			$this->model = new Model_Video();
		}
		
		public function indexAction()
		{
			$this->view->layout()->setLayout('private');
			$this->view->config =$this->config;
			if($this->user['user_type']!='agency')
			{	$video = $this->model->GetEscortVideo($this->user_id,$this->config['VideoCount']);
				$this->view->photo = $video[0];
				$this->view->video = $video[1];
			}
			else
			{
				$this->_helper->viewRenderer->setScriptAction("agency");
				$this->view->escorts = $this->model->GetAgencyEscort($this->user_id);
				
			}
			$sess = new Zend_Session_Namespace('video-tmp-session');
			if(isset($sess->tpm_error))
			{	
				$this->view->error = $sess->tpm_error;
				unset($sess->tpm_error);	
			}
			elseif(isset($sess->tpm_success))
			{
				$this->view->success = $sess->tpm_success;
				unset($sess->tpm_success);
			}
			$this->view->escort_id = $this->user['escort_data']['escort_id'];
			$this->view->host =  Cubix_Application::getById(Cubix_Application::getId())->host;
		}
			
		public function removeAction()
		{	    
			$this->_helper->layout()->disableLayout(); 
			$this->_helper->viewRenderer->setNoRender(true);
			$req =  $this->_request;
			if($req->isXmlHttpRequest() && $req->isPost() && isset($req->check) && is_array($req->check) && !empty($req->check))
			{	
				$res = $this->model->Remove(reset($req->check),$this->user['escort_data']['escort_id']);
				die("$res");
			}
			
		}
		
		public function getvideoAction()
		{	
			$this->_helper->layout()->disableLayout();
			$this->_helper->viewRenderer->setScriptAction("agency_video");
			$req =  $this->_request;
			if($req->isXmlHttpRequest() && $this->user['user_type']=='agency' && $req->isPost() && isset($req->select_escort) && is_numeric($req->select_escort))
			{			
				$video = $this->model->GetEscortVideo($req->select_escort,$this->config['VideoCount'],$this->user_id);

				$this->view->host =  Cubix_Application::getById(Cubix_Application::getId())->host;
				$this->view->config =  Zend_Registry::get('videos_config');
				$this->view->escort_id = $req->select_escort;
				$this->view->photo = $video[0];
				$this->view->video = $video[1];
			}
			
		}
		
		public function uploadAction()
		{		
			$sess = new Zend_Session_Namespace('video-tmp-session');
			$this->view->layout()->setLayout('private');
			$this->_helper->viewRenderer->setScriptAction("index");
			$error = NULL;
			$req =  $this->_request;

			if($this->user['user_type']=='agency')
			{	
				if(!isset($req->select_escort) || !is_numeric($req->select_escort)  || is_array($req->select_escort))
				$error = Cubix_I18n::translate('sys_error_choose_escort');
				else 
				{	if($this->model->GetAgencyEscorts($this->user_id,$req->select_escort))
					$escort_id = $req->select_escort;
					else
					$error = Cubix_I18n::translate('sys_error_choose_From_Your_escorts');

				}
			}
			else
				$escort_id = $this->user_id;
			
			
			$response = array();
			$response['error'] = '';
			$response['finish'] = FALSE;
			if($this->model->UserVideoCount($escort_id) < $this->config['VideoCount'])
			{			
				$client = Cubix_Api_XmlRpc_Client::getInstance();
				$backend_video_count = $client->call('Application.UserVideoCount',array($escort_id));
				$check = Cubix_Videos::CheckVideoStatus($backend_video_count, $this->config['VideoCount'], true);
								
				if(!$check)
				{	
					$video = new Cubix_Videos($escort_id,$this->config);
					$response = $video->getResponse();
					
					if($response['finish'] && $response['error'] == 0 ){
						//$video->AddMetaData();
						$video->ConvertToMP4();

						$image = $video->SaveImage();		
						if(is_array($image))
						{	
							if($video->SaveVideoImageFtpFrontend($image,$client, 'mp4'))
								$sess->tpm_success = Cubix_I18n::translate('wait_check_video');

							else
								$error = Cubix_I18n::translate('sys_error_while_upload');
						}
						else
						{
							$response['error'] = $image;
						}
					}
				}
				else
					$response['error'] = $check;		
			}
			else{
				$response['error'] = Cubix_I18n::translate('sys_error_upload_video_count',array('count' =>$this->config['VideoCount']));
			}
			
			Cubix_Videos::showResponse($response);
			die;
		}
		
			
}
