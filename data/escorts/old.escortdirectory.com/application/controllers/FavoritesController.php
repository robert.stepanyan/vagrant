<?php

class FavoritesController extends Zend_Controller_Action
{
	public static $linkHelper;
	
	public function init()
	{
		$this->view->layout()->setLayout('private');

		$cache = Zend_Registry::get('cache');
		self::$linkHelper = $this->view->getHelper('GetLink');

		$anonym = array('ajax-load-button');

		$this->user = Model_Users::getCurrent();
		$this->defines = $this->view->defines = Zend_Registry::get('defines');
		$this->client = new Cubix_Api_XmlRpc_Client();

		$this->view->user = $this->user;

		if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}
	}

	public function indexAction()
	{
		
	}

	public function ajaxGetAllAction()
	{
		$this->view->layout()->disableLayout();

		$sort_array = $this->view->sort_array = array(
			'alpha' => __('alpha'),
			'date_added' => __('date_added')
		);

		$per_page_array = $this->view->per_page_array = array(14, 28, 42, 56);

		$sort_by = $this->view->sort_by = $this->_request->getParam('sort_by', 'alpha');
		$per_page = $this->view->per_page = $this->_request->getParam('per_page', $per_page_array[0]);
		$fav_city_id = $this->view->fav_city_id = $this->_request->getParam('fav_city_id');

		$page = $this->view->page = $this->_request->getParam('page', 1);

		$filter = array('act' => 2, 'city' => $fav_city_id);

		$escorts = $this->view->escorts = $this->client->call('Members.getEDFavorites', array($this->user->id, $filter, $sort_by, $page, $per_page));
		$cities = $this->view->cities = $this->client->call('Members.getEDFavorites', array($this->user->id, array('act' => 2), 'by_city', 1, 5000));
	}

	public function ajaxRemoveAction()
	{
		$this->view->layout()->disableLayout();

		$escort_id = (int) $this->_request->getParam('escort_id');
		if ( ! $escort_id ) die;

		$this->client->call('Members.removeFromFavorites', array($this->user->id, $escort_id));
		die;
	}

	public function ajaxAddNoteAction()
	{
		$this->view->layout()->disableLayout();

		$m_escort = new Model_EscortsV2();

		$escort_id = $this->view->escort_id = (int) $this->_request->getParam('escort_id');
		if ( ! $escort_id ) die;	

		$this->view->showname = $m_escort->getShownameById($escort_id);

		$this->view->comment = $this->client->call('Members.getCommentByEscortED', array($this->user->id, $escort_id));

		if ( $this->_request->isPost() ) {

			$note = $this->_request->getParam('note');

			$validator = new Cubix_Validator();

			/*if ( ! strlen($note) ) {
				$validator->setError('note', 'Note is required');
			}*/

			$result = $validator->getStatus();

			if ( $validator->isValid() ) {
				$this->client->call('Members.updateCommentED', array($this->user->id, $escort_id, $note));
			}

			die(json_encode($result));
		}
		
	}

	public function ajaxAddToFavoritesAction() 
	{
		$this->view->layout()->disableLayout();

		$escort_id = $this->view->escort_id = (int) $this->_request->getParam('escort_id');
		if ( ! $escort_id ) die;

		$this->client->call('Members.addToFavorites', array($this->user->id, $escort_id));

		die;
	}

	public function ajaxLoadButtonAction()
	{
		$this->view->layout()->disableLayout();

		$escort_id = $this->view->escort_id = (int) $this->_request->getParam('escort_id');
		if ( ! $escort_id ) die;

		$this->view->is_in_favorites = $this->client->call('Members.isInFavorites', array($this->user->id, $escort_id));
	}
}
