<?php

class IndexController extends Zend_Controller_Action
{
	public function indexAction()
	{
		$this->_forward('index', 'escorts');
	}

	protected function _ajaxError()
	{
		die(json_encode(array('result' => 'error')));
	}

	public function chatOnlineBtnAction()
	{
		$this->view->layout()->disableLayout();

		$req = $this->_request;

		$username = $req->username;
		try {
			$online_users = file_get_contents('http://www.6annonce.com:35555/roomonlineusers.js?roomid=1');
			
			if ( strlen($online_users) > 0 ) {
				$online_users = substr($online_users, 20, (strlen($online_users) - 21));

				$online_users = json_decode($online_users);

				if ( count($online_users) > 0 ) {
					foreach ( $online_users as $user ) {
						if( $user->name == $username ) {
							$this->view->show_btn = true;
							break;
						}
					}
				}
			}
			else {
				$this->view->show_btn = false;
			}
		}
		catch ( Exception $e ) {

		}
	}

	public function escortPhotosAction()
	{
		$id = intval($this->_getParam('id'));
		if ( $id <= 0 ) $this->_ajaxError();

		$escort = new Model_EscortItem();
		$escort->setId($id);

		$photos = $escort->getPhotos();

		$result = array();
		foreach ( $photos as $photo ) {
			$result[] = $photo->hash . '.' . $photo->ext;
		}
		
		die(json_encode(array('result' => 'success', 'data' => $result)));
	}

	public function checkHitsCacheAction()
	{
		$config = Zend_Registry::get('system_config');
		$data = Model_Escorts::getAllCachedHits();

		echo 'Cache Limit: ' . $config['hitsCacheLimit'] . "\n";
		echo 'Total Count: ' . count($data) . "\n\n";

		print_r($data);
		die;
	}

	public function splashAction()
	{
		$this->view->layout()->disableLayout();

		//$escorts = array(142241, 142256, 142257, 142260);

		//Model_Escorts::hit($escorts[rand(0, 3)]);

		/*$this->view->then = $this->_request->then;
		
		if ( $this->_request->enter ) {
			$domain = preg_replace('#^(.+?)([^\.]+)\.([^\.]+)$#', '.$2.$3', $_SERVER['HTTP_HOST']);
			setcookie('18', '1', null, '/', $domain);
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: ' . $this->view->then);
		}*/
	}
	
	public function feedbackAction()
	{
		$this->_request->setParam('no_tidy', true);

		$model = new Model_EscortsV2();

		$user = Model_Users::getCurrent();

		$is_premium = false;
		if ( isset($user->member_data) ) {
			$is_premium = $user->member_data['is_premium'];
		}
	
		$this->view->is_premium = $is_premium;

		// Fetch administrative emails from config
		$config = Zend_Registry::get('feedback_config');
		$site_emails = $config['emails'];
		
		$this->view->layout()->setLayout('main-simple');
		
		$is_ajax = ! is_null($this->_getParam('ajax'));

        $feedback = new Model_Feedback();

		// If the request is ajax, disable the layout
		if ( $is_ajax ) {
			$this->view->layout()->disableLayout();
		}
		
		// Get data from request
		$data = new Cubix_Form_Data($this->getRequest());
		$fields = array(
			'to' => '',
			'about' => 'int',
			'name' => 'notags|special',
			'email' => '',
			'message' => 'notags|special',
			'captcha' => '',
			'rand' => ''
		);
		$data->setFields($fields);
		$data = $data->getData();

        $blackListModel = new Model_BlacklistedWords();
		$this->view->data = $data;
		
		// If the to field is invalid, that means that user has typed by 
		// hand, so it's like a hacking attempt, simple die, without explanation
		if ( ! is_numeric($data['to']) && ! in_array($data['to'], array_keys($site_emails)) ) {
			die;
		}
		
		// If escort id was specified, fetch it's email and showname, 
		// construct the result email template and send it to escort
		if ( is_numeric($data['to']) ) {
			$escort = $model->get($data['to']);
			$this->view->escort = $escort;
			//var_dump($escort);die;
			// If the escort id is invalid that means that it is hacking attempt
			if ( ! $escort ) {
				die;
			}
			
			$data['to_addr'] = $escort->email;
			$data['to_name'] = $escort->showname;
			$data['escort_showname'] = $escort->showname;
			$email_tpl = 'escort_feedback';
		}
		// Else get administrative email from configuration file
		else {
			$data['to_addr'] = $site_emails[$data['to']];
			$email_tpl = 'feedback_template';
		}
		
		// In order when the form was posted validate fields
		if ( $this->_request->isPost() ) {
			$validator = new Cubix_Validator();

            if ( !$blackListModel->checkFeedback($data) ){
                $blackListModel->mergeFeedbackData($data);
               // $validator->setError('message', 'Do Not repeat The Same Message' /*'Email is required'*/);
            }
            

			if ( ! strlen($data['email']) ) {
				$validator->setError('email', '' /*'Email is required'*/);
			}
			elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
				$validator->setError('email', '' /*'Wrong email format'*/);
			}elseif( $blackListModel->checkEmail($data['email']) ){
                $data['flag'] = FEEDBACK_FLAG_BLOCKED_BY_EMAIL;
                $data['status'] = FEEDBACK_STATUS_DISABLED;
                $validator->setError('email', 'This email is blocked');
            }
			
			if ( ! strlen($data['message']) ) {
				$validator->setError('message','' /*'Please type the message you want to send'*/);
			}elseif($blackListModel->checkWords($data['message'])){
                $data['flag'] = FEEDBACK_FLAG_BLOCKED_BY_WORD;
				$data['status'] = FEEDBACK_STATUS_DISABLED;
                $validator->setError('message', 'You can`t use words "'.$blackListModel->getWords() .'"');
            }

            if( isset($data['flag']) && ($data['flag'] == FEEDBACK_FLAG_BLOCKED_BY_EMAIL || $data['flag'] == FEEDBACK_FLAG_BLOCKED_BY_WORD) ){
                $data['application_id'] = Cubix_Application::getId();
                $feedback->addFeedback($data);
            }

			/*$captcha = Cubix_Captcha::verify($this->_request->recaptcha_response_field);

			$captcha_errors = array(
				'invalid-site-public-key' => 'We weren\'t able to verify the public key.',
				'invalid-site-private-key' => 'We weren\'t able to verify the private key.',
				'invalid-request-cookie' => 'The challenge parameter of the verify script was incorrect.',
				'incorrect-captcha-sol' => 'The CAPTCHA solution was incorrect.',
				'verify-params-incorrect' => 'The parameters to /verify were incorrect, make sure you are passing all the required parameters.',
				'invalid-referrer' => 'reCAPTCHA API keys are tied to a specific domain name for security reasons.',
				'recaptcha-not-reachable' => 'reCAPTCHA never returns this error code. A plugin should manually return this code in the unlikely event that it is unable to contact the reCAPTCHA verify server.'
			);*/

			//var_dump($captcha);
			
			if ( ! strlen($data['captcha']) ) {
				$validator->setError('captcha', '');//Captcha is required
				}
			else {
				$session = new Zend_Session_Namespace('captcha');
				$orig_captcha = $session->captcha;

				if ( strtolower($data['captcha']) != $orig_captcha ) {
					$validator->setError('captcha', '');//Captcha is invalid
				}
			}		
			
			
			$result = $validator->getStatus();
			
			if ( ! $validator->isValid() ) {
				// If the request was ajax, send the validation result as json
				if ( $is_ajax ) {
					echo json_encode($result);
					die;
				}
				// Otherwise, render the phtml and show errors in it
				else {
					$this->view->errors = $result['msgs'];
					return;
				}
			}
			else
			{
				$sess = new Zend_Session_Namespace('feedback'); 
				
				if ($data['rand'] != $sess->rand)
				{
					echo json_encode(array('status' => 'success', 'msg' => '<p style="padding:0 10px"><span class="strong" style="color: #060">Your feedback has been successfully submitted!</span></p>'));
					die;
				}
			}
			
            $about_escort = null;
			if ( $data['about'] ) {
				$about_escort = $model->get($data['about']);
			}
			if ( ! $about_escort ) $about_escort = (object) array('id' => '', 'showname' => '');

			$data['application_id'] = Cubix_Application::getId();
			
			if ($config['approvation'] == 1)
			{
				$data['flag'] = FEEDBACK_FLAG_NOT_SEND;
				$data['status'] = FEEDBACK_STATUS_NOT_APPROVED;
			}
			else
			{
				$data['flag'] = FEEDBACK_FLAG_SENT;
				$data['status'] = FEEDBACK_STATUS_APPROVED;
				
				// Set the template parameters and send it to support or to an escort
				$tpl_data = array(
					'name' => $data['name'],
					'email' => $data['email'],
					'to_addr' => $data['to_addr'],
					'message' => $data['message'],
					'escort_showname' => $escort->showname,
					'escort_id' => $escort->id
				);

				if ( isset($data['username']) ) {
					$tpl_data['username'] = $data['username'];
				}
				
				Cubix_Email::sendTemplate($email_tpl, $data['to_addr'], $tpl_data, isset($escort) ? $data['email'] : null);

				$config = Zend_Registry::get('newsman_config');

				if ($config)
				{
					$list_id = $config['list_id'];
					$segment = $config['member'];
					$nm = new Cubix_Newsman($_SERVER["REMOTE_ADDR"]);

					$subscriber_id = $nm->subscribe($list_id, $data['email'],$data['name']);
					$is_added = $nm->bindToSegment($subscriber_id, $segment);
				}
			}
			
			$feedback->addFeedback($data);
			$sess->unsetAll();
			
			if ( $is_ajax ) {
				if ( isset($escort) ) {
					if ( $is_premium ) {
						$result['msg'] = '
							<p style="padding:0 10px; margin-top: 50px; width: 330px; font-size: 14px"><span class="strong" style="color: #FFF">' . __('message_sent_to_escort', array('showname' => $escort->showname)) . '</span></p>
						';
					}
					else {
						$result['msg'] = '
							<p style="padding:0 10px; margin-top: 50px; width: 330px; font-size: 14px"><span class="strong" style="color: #000">' . __('message_sent_to_escort', array('showname' => $escort->showname)) . '</span></p>
						';
					}
				}
				else {
					$result['msg'] = '
						<p style="padding:0 10px"><span class="strong" style="color: #060">Your feedback has been successfully submitted!</span></p>
					';
				}
				//<a href="#" onclick="return Cubix.Feedback.Show(this.getParent(\'.cbox-small-ii\'))">Click here</a> to hide this message</strong>
				echo json_encode($result);
				die;
			}
			else {
				$this->view->success = TRUE;
			}
		}
	}

	public function contactAction()
	{
		$this->_request->setParam('no_tidy', true);

		$this->_request->setParam('to', 'support');

		$model = new Model_EscortsV2();

		// Fetch administrative emails from config
		$config = Zend_Registry::get('feedback_config');
		$site_emails = $config['emails'];

		$this->view->layout()->setLayout('main-simple');

		$is_ajax = ! is_null($this->_getParam('ajax'));

		// If the request is ajax, disable the layout
		if ( $is_ajax ) {
			$this->view->layout()->disableLayout();
		}

		// Get data from request
		$data = new Cubix_Form_Data($this->getRequest());
		$fields = array(
			'to' => '',
			'about' => 'int',
			'name' => 'notags|special',
			'email' => '',
			'message' => 'notags|special',
			'captcha' => ''
		);
		$data->setFields($fields);
		$data = $data->getData();

		$this->view->data = $data;

		// If the to field is invalid, that means that user has typed by
		// hand, so it's like a hacking attempt, simple die, without explanation
		if ( ! is_numeric($data['to']) && ! in_array($data['to'], array_keys($site_emails)) ) {
			die;
		}

		// If escort id was specified, fetch it's email and showname,
		// construct the result email template and send it to escort
		if ( is_numeric($data['to']) ) {
			$escort = $model->get($data['to']);
			$this->view->escort = $escort;
			//var_dump($escort);die;
			// If the escort id is invalid that means that it is hacking attempt
			if ( ! $escort ) {
				die;
			}

			$data['to_addr'] = $escort->email;
			$data['to_name'] = $escort->showname;
			$data['username'] = $escort->username;
			$email_tpl = 'escort_feedback';
		}
		// Else get administrative email from configuration file
		else {
			$data['to_addr'] = $site_emails[$data['to']];
			$email_tpl = 'feedback_template';
		}

		// In order when the form was posted validate fields
		if ( $this->_request->isPost() ) {
			$validator = new Cubix_Validator();

			if ( ! strlen($data['email']) ) {
				$validator->setError('email', 'Email is required');
			}
			elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
				$validator->setError('email', 'Wrong email format');
			}

			if ( ! strlen($data['message']) ) {
				$validator->setError('message', 'Please type the message you want to send');
			}

			if ( ! strlen($data['captcha']) ) {
				$validator->setError('captcha', 'Captcha is required');
			}
			else {
				$session = new Zend_Session_Namespace('captcha');
				$orig_captcha = $session->captcha;

				if ( strtolower($data['captcha']) != $orig_captcha ) {
					$validator->setError('captcha', 'Captcha is invalid');
				}
			}

			$result = $validator->getStatus();

			if ( ! $validator->isValid() ) {
				// If the request was ajax, send the validation result as json
				if ( $is_ajax ) {
					echo json_encode($result);
					die;
				}
				// Otherwise, render the phtml and show errors in it
				else {
					$this->view->errors = $result['msgs'];
					return;
				}
			}
			
			$sent_by = '';
			if ( $user = Model_Users::getCurrent() )
			{
				$sent_by = $user->user_type.' '.$user->username.' wrote a message';
			}

			if ( $data['about'] ) {
				$about_escort = $model->get($data['about']);
			}
			if ( ! $about_escort ) $about_escort = (object) array('id' => '', 'showname' => '');

			// Set the template parameters and send it to support or to an escort
			$tpl_data = array(
				'name' => $data['name'],
				'email' => $data['email'],
				'to_addr' => $data['to_addr'],
				'message' => $data['message'],
				'escort_showname' => $about_escort->showname,
				'escort_id' => $about_escort->id,
				'sent_by' => $sent_by
			);

			if ( isset($data['username']) ) {
				$tpl_data['username'] = $data['username'];
			}

			Cubix_Email::sendTemplate($email_tpl, $data['to_addr'], $tpl_data, isset($escort) ? $data['email'] : null);
			
			// contact me log
			Cubix_Api::getInstance()->call('contactLog', array($data['to_addr'], $user ? $user->id : null));
			//

			if ( $is_ajax ) {
				if ( isset($escort) ) {
					$result['msg'] = '
						<p style="padding:0 10px"><span class="strong" style="color: #060">' . __('message_sent_to_escort', array('showname' => $escort->showname)) . '</span></p>
					';
				}
				else {
					$result['msg'] = '
						<p style="padding:0 10px"><span class="strong" style="color: #060">Your feedback has been successfully submitted!</span></p>
					';
				}
				//<a href="#" onclick="return Cubix.Feedback.Show(this.getParent(\'.cbox-small-ii\'))">Click here</a> to hide this message</strong>
				echo json_encode($result);
				die;
			}
			else {
				$this->view->success = TRUE;
			}
		}
	}

    public function captchaAction()
    {
        $this->view->layout()->disableLayout();

        $this->_request->setParam('no_tidy', true);

        $font = 'css/trebuc.ttf';

        $charset = '123456789ABCDEFGHIJKLMNPQRSTUVWXYZ';

        $code_length = 5;

        $height = 30;
        $width = 90;
        $code = '';

        for ( $i = 0; $i < $code_length; $i++ )
        {
            $code = $code . substr($charset, mt_rand(0, strlen($charset) - 1), 1);
        }

        $rgb[0] = array(204,0,0);
        $rgb[1] = array(34,136,0);
        $rgb[2] = array(51,102,204);
        $rgb[3] = array(141,214,210);
        $rgb[4] = array(214,141,205);
        $rgb[5] = array(100,138,204);

        $font_size = $height * 0.4;

        $bg = imagecreatefrompng('img/bg_captcha.png');
        $image = imagecreatetruecolor($width, $height);
        imagecopy($image, $bg, 0, 0, 0, 0, imagesx($bg), imagesy($bg));
        $background_color = imagecolorallocate($image, 255, 255, 255);
        $noise_color = imagecolorallocate($image, 20, 40, 100);

        for($i = 0; $i<$code_length; $i++)
        {
            $A[] = rand(-20, 20);
            $C[] = rand(0, 5);
            $text_color = imagecolorallocate($image, $rgb[$C[$i]][0], $rgb[$C[$i]][1], $rgb[$C[$i]][2]);
            imagettftext($image, $font_size, $A[$i], 7 + $i * 15, 20 + rand(-3, 3), $text_color, $font , $code[$i]);
        }

        $session = new Zend_Session_Namespace('captcha');
        $session->captcha = strtolower($code);


        header('Content-Type: image/png');
        imagepng($image);
        imagedestroy($image);

        die;
    }
	
	public function goAction()
	{
		$link = $_SERVER['QUERY_STRING'];
		
		if ( ! preg_match('#^http(s)?://#', $link) ) $link = 'http://' . $link;
		
		header('Location: ' . $link);
		die;
	}

	public function robotsAction()
	{
		$this->_request->setParam('no_tidy', true);

		$this->view->layout()->disableLayout();
		$model = new Model_Robots();

		$robots = $model->get();

		header('Content-Type: text/plain; charset=UTF-8', true);
		echo $robots;
		die;
	}

	public function benchmarkAction()
	{
		$db = Zend_Registry::get('db');
		$results = $db->fetchAll('SELECT * FROM debug_benchmark');

		$this->view->results = $results;
	}

	public function listDateImgAction()
	{
		$date = $this->_getParam('date');
		$date = intval($date);
		//$date = ( date('d.m.Y') == date('d.m.Y', $date) ? $this->view->t('today_new') : date('d.m / Y', $date) );

		$d_n = strtotime(date('d.m.Y'));
		$d = strtotime(date('d.m.Y', $date));

		$d_d = $d_n - $d;
		$d_d = floor($d_d / 60 / 60 / 24);

		if ($d_d == 0)
			$date = $this->view->t('today');
		elseif ($d_d == 1)
			$date = $this->view->t('yesterday');
		else
			$date = $this->view->t('days_ago', array('days' => $d_d));

		header('Content-Type: image/png');

		$expires = 60*60*24*10;
		header("Pragma: public");
		header("Cache-Control: maxage=" . $expires);
		header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expires) . ' GMT');


		$title = $date;

		$im = imagecreatetruecolor(75, 15);
		$white = imagecolorallocate($im, 255, 255, 255);
		$black = imagecolorallocate($im, 0, 0, 0);
		imagefill($im, 0, 0, $black);
		imagecolortransparent($im, $black);

		$white = imagecolorallocate($im, 255, 255, 255);
		$font = "css/arial.ttf";

		imagettftext($im, 8, 0, 0, 11, -$white, $font, $title);

		imagepng($im);
		imagedestroy($im);

		die;
	}

	public function photoViewerAction()
	{
		$response = array('status' => 'success', 'msg' => '', 'data' => array());

		$model = new Model_EscortsV2();

		$showname = $this->_getParam('showname');
		$escort_id = $this->_getParam('escort_id');
		//$escort = $model->get($showname);
		$escort = $model->get($escort_id);

		if ( ! $escort ) {
			$response['status'] = 'error';
			$response['msg'] = 'Showname is required';
		}
		else {
			$photos = $escort->getPhotos(null);

			foreach ( $photos as $i => $photo ) {
				$response['data'][$i] = array(
					'id' => $photo->id,
					'private' => $photo->type == 3,
					'h' => $photo->getUrl('orig'),
					'b' => $photo->getUrl('w514'),
					's' => $photo->getUrl('sthumb')
				);
			}
		}

		$this->_helper->json($response);
	}

	public function chatAction()
	{
		$this->view->layout()->disableLayout();
	}

	public function aboutAction()
	{
		$this->view->layout()->setLayout('main-simple');
	}
	
	public function noChatAction()
	{
		$this->view->layout()->disableLayout();
	}

	public function linksAction()
	{
		$this->view->layout()->setLayout('main');

		if ( ! is_null($this->_getParam('ajax')) ) {
			$side_bar = $this->view->render('escorts/side-bar-countries.phtml');
			$body = $this->view->render('index/links.phtml');
			die(json_encode(array('head_title' => 'Links', 'side_bar' => $side_bar, 'body' => $body)));
		}
	}
	
	public function catchEmailAction()
	{
		//$this->view->layout()->setLayout('email-catcher');
		$this->view->layout()->disablelayout();
		$email = trim($this->_request->email);
		
		if (strlen($email) > 0  && Cubix_Validator::validateEmailAddress($email)) {
			
			$list_id = 98;
			$segment = 265;
			
			$nm = new Cubix_Newsman($_SERVER["REMOTE_ADDR"]);
			
			$subscriber_id = $nm->subscribe($list_id, $email);
			$is_added = $nm->bindToSegment($subscriber_id, $segment);
			
			//$this->view->success = true;
			die('success_send');

		}
	}
	
	public function oEAction()
	{
		$m = new Model_EscortsV2();
		
		$escorts = $m->getLoginEscorts();
		
		$arr = array();
		
		if ($escorts)
		{
			foreach ($escorts as $e)
			{
				$arr[$e->escort_id] = $e->showname;
			}
		}
		
		echo json_encode($arr);
		die;
	}
	
	public function jsInitAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$view = $this->view->act = $req->view;
		
		$req->setParam('lang_id', $req->lang);
		
		$lang = $this->view->lang = $req->lang;
		
		if ( $view == 'main-page' ) {

		}
		else if ( $view == 'main-simple' ) {
			
		}
		else if ( $view == 'main' ) {
			
		}
		else if ( $view == 'private' ) {
			
		}
		else if ( $view == 'private-v2' ) {
			
		}
		else if ( $view == 'profile' ) {

		}
		else if ( $view == 'profile-v2' ) {

		}
		
		/*header('Content-type: text/javascript');
		$expires = 60*60*24*14;
		header("Pragma: public");
		header("Cache-Control: maxage=" . $expires);
		header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expires) . ' GMT');*/
		
	}
	
	public function jsInitExactAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$view = $this->view->act = $req->view;
		
		$req->setParam('lang_id', $req->lang);
		
		$lang = $this->view->lang = $req->lang;
		
		if ( $view == 'new-list' ) {

		}
		else if ( $view == 'list' ) {

		}
		else if ( $view == 'profile' ) {
			
		}
		else if ( $view == 'comment-tabs' ) {
			
		}
		else if ( $view == 'comment-tabs-reversed' ) {
			
		}
		else if ( $view == 'splash' ) {
			
		}	
		else if ( $view == 'signup' ) {

		}			
		else if ( $view == 'main' ) {

		}
		
		header('Content-type: text/javascript');
		/*$expires = 60*60*24*14;
		header("Pragma: public");
		header("Cache-Control: maxage=" . $expires);
		header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expires) . ' GMT');*/
	}

	public function advertiseAction()
	{
		$this->view->layout()->disableLayout();
	}

	public function testAction()
	{
	}

	public function getEscortsViewCountAction()
	{
		$this->view->layout()->disableLayout();
		$escort_id = $this->view->escort_id = (int) $this->_request->escort_id;
	}

	public function phoneImgAction()
	{
		$escort_id = (int) $this->_request->escort_id;
		$agency_id = (int) $this->_request->agency_id;
		$ad_id = (int) $this->_request->ad_id;
		$font_size = (int) $this->_request->fs;
		$width = (int) $this->_request->w;
		$height = (int) $this->_request->h;
		$v = (int) $this->_request->v;
		$color = $this->_request->c;
		$phone = $this->_request->p;
		$add = intval($this->_request->add);

		if (!in_array($add, array(1, 2)))
			$add = NULL;

		$colors = explode(',', $color);		

		if ( ! strlen($phone) ) {
			if ( $escort_id ) {
				$model = new Model_EscortsV2();
				$cache_key = 'v2_' . $escort_id .  '_profile_' . Cubix_I18n::getLang();
				$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
				$escort = $model->get($escort_id, $cache_key);
				$contacts = $escort->getContacts();

				if (!$add) {
					$filtered_phone =  (isset($contacts->phone_country_id) && !$contacts->disable_phone_prefix) ? '+'.Model_Countries::getPhonePrefixById($contacts->phone_country_id) . $contacts->phone_number : $contacts->phone_number;
				}
				else {
					$add_phone = $model->getAdditionalPhone($escort_id, $add);
					$filtered_phone =  (isset($add_phone->phone_country_id) && !$add_phone->disable_phone_prefix) ? '+' . Model_Countries::getPhonePrefixById($add_phone->phone_country_id) . $add_phone->phone_additional : $add_phone->phone_additional;
				}
			} else if ( $agency_id ) {
				$m_agencies = new Model_Agencies();
				$agency = $m_agencies->getLocalById($agency_id);

				if (!$add) {
					$filtered_phone =  (isset($agency->phone_country_id)) ? '+' . Model_Countries::getPhonePrefixById($agency->phone_country_id) . $agency->phone : $agency->phone;
				}
				else {
					$filtered_phone =  (isset($agency->{'phone_country_id_' . $add})) ? '+' . Model_Countries::getPhonePrefixById($agency->{'phone_country_id_' . $add}) . $agency->{'phone_' . $add} : $agency->{'phone_' . $add};
				}
			} else if ($ad_id) {
				$m_class = new Model_ClassifiedAds();
				$filtered_phone = $m_class->get($ad_id)->phone;
			}
		} else {
			$filtered_phone = $phone;
		}

		// Set the content-type
		header('Content-Type: image/png');


		// Create the image
		$im = imagecreatetruecolor($width, $height);

		// Create some colors
		$white = imagecolorallocate($im, 255, 255, 255);

		array_unshift($colors, $im);
		
		$text_color = call_user_func_array('imagecolorallocate', $colors);//($im, 33, 115, 161);

		imagefilledrectangle($im, 0, 0, 399, 29, $white);
		imagecolortransparent($im, $white);
	
		// Replace path by your own font path
		$font = 'css/ArchivoNarrow.ttf';

		// Add the text
		imagettftext($im, $font_size, 0, 0, $v, $text_color, $font, $filtered_phone);

		// Using imagepng() results in clearer text compared with imagejpeg()
		imagepng($im);
		imagedestroy($im);
		die;
	}
	
	public function emailTestAction()
	{
		Cubix_Email::sendTemplate('universal_message', 'dave.kasparov@gmail.com', array(
			'subject' => "subject",
			'message' => "message",
		));
		
		die("Email sent!");
	}

	public function memcacheTestAction()
	{
		
		$cache = Zend_Registry::get('cache');

		$cache_key = "memcache_test_" . Cubix_Application::getId();

		if ( ! $data = $cache->load($cache_key) ) {
			echo "no cache";
			$data = "cache data";
            $cache->save($data, $cache_key, array());            
        }

        echo '<br/>' . $data;

        die;
	}

	public function emailCollectingPopupAction()
	{
		$this->view->layout()->disableLayout();

		if ( $this->_request->isPost() ) {
			$email = $this->_request->email;
			$error = '';
			
			if ( ! $email ) {
				$error = Cubix_I18n::translate('email_required');
			} else if ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $email) ) {
				$error = Cubix_I18n::translate('invalid_email');
			}
			
			if (strlen($error) ) {
				die(json_encode(array('status' => 'error', 'msg' => $error)));
			} else {
				Cubix_Email::sendTemplate('newsletter_subscribe_confirmation', $email, array(
					'link'	=> Cubix_Application::getById()->url . $this->view->getLink('email-collecting-popup-confirmed', array('email' => $email, 'hash' => md5(str_repeat($email, 3))))
				), 'newsletter@escortdirectory.com', 'ED News');
				die(json_encode(array('status' => 'success', 'html' => $this->view->render('index/email-collecting-success.phtml'))));

			}
		} else {
			if ( $this->_request->ec_confirm == 1 ) {
				$email = $this->_request->email;
				$hash = $this->_request->hash;

				if ( md5(str_repeat($email, 3)) != $hash )	{
					setcookie('ecr', 0, time() + 60*60*24*365, '/', 'www.' . Cubix_Application::getById()->host);
				} else {
					setcookie('ecr', 1, time() + 60*60*24*365, '/', 'www.' . Cubix_Application::getById()->host);
					$LIST_ID = 331;
					$SEGMENT = 47597;

					$m_newsman = new Cubix_Newsman($_SERVER["REMOTE_ADDR"]);
					$subscriber_id = $m_newsman->subscribe($LIST_ID, $email, "dd", "dd");

					if($subscriber_id){
						$client = new Cubix_Api_XmlRpc_Client();		
						$client->call('Application.subscriberLog', array($email));
					}

					$m_newsman->bindToSegment($subscriber_id, $SEGMENT);
				}
				return $this->_redirect('/');
			} elseif ( isset($this->_request->ec_status) ) {
				if ( $this->_request->ec_status != 1 )  $this->view->error = true;
				$this->_helper->viewRenderer->setScriptAction('email-collecting-confirmed');

				unset($_COOKIE['ecr']);
			    setcookie('ecr', null, -1, '/', 'www.' . Cubix_Application::getById()->host);
			}
		}
	}

	public function exportJsonAction()
	{
		$escorts = Model_Escort_List::getForJson();
		file_put_contents('escorts.json',json_encode($escorts));
		
		die();
	}

	public function exportJsonPropsAction()
	{
		$defines = Zend_Registry::get('defines');

		//file_put_contents('keywords.json',json_encode($defines['keywords_ed']));
		//file_put_contents('languages.json',json_encode($defines['language_options']));
		//file_put_contents('genders.json',json_encode($defines['gender_options']));
		//file_put_contents('orientations.json',json_encode($defines['sexual_orientation_options']));
		//file_put_contents('services_for.json',json_encode($defines['sex_availability_options']));
		//file_put_contents('services.json',json_encode($defines['services']));

		die();
	}

	public function manifestAction()
	{
		$this->view->layout()->disableLayout();
		header('Content-Type: application/octet-stream');
		$escort_id = intval($this->_request->escort_id);
		$video_model = new Model_Video();
		$this->view->escort_id = $escort_id;
		$this->view->video = $video_model->getManifestData($escort_id);
		$this->view->dimensions = $video_model->dimensions;
		$this->view->configs = Zend_Registry::get('videos_config');

	}
	
	public function ageCheckAction()
	{
		$this->view->layout()->setLayout('age-check');
		$this->view->lang = Cubix_I18n::getLang();
	}		
}
