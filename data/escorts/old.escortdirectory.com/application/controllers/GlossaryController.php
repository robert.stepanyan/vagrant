<?php

class GlossaryController extends Zend_Controller_Action
{
	private $model;

	public function init()
	{
		$this->model = new Model_Glossary();
	}

	public function indexAction()
	{

	}

	public function getAction()
	{
		$this->view->layout()->disableLayout();

		$letter = $this->_request->letter;

		if (strlen($letter) == 1)
		{
			$code = ord($letter);

			if (($code >= 65 && $code <= 90) || ($code >= 97 && $code <= 122))
				$letter = strtolower($letter);
			else
				$letter = 'a';
		}
		else
		{
			if (!in_array($letter, array('all', 'others')))
				$letter = 'a';
		}

		$this->view->letter = $letter;

		$search = $this->_request->search;

		$this->view->items = $this->model->getAll($letter, $search);
	}
}
