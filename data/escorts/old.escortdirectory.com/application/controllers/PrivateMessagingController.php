<?php
class PrivateMessagingController extends Zend_Controller_Action {
	
	/**
	 * @var Model_EscortItem
	 */
	protected $escort;

	/**
	 * @var Model_AgencyItem
	 */
	protected $agency;

	private $_model;
	
	private $can_write_message = true;
	
	public function init() {
		$this->user = $this->view->user = Model_Users::getCurrent();
		
		$this->view->layout()->setLayout('private-v2');
		
		$anonym = array('check-for-new-messages');
		
		if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}
		
		$cache = Zend_Registry::get('cache');
		$cache_key =  'v2_user_pva_' . $this->user->id;
		
		if ( $this->user->isAgency() ) {
			
			if ( ! $agency = $cache->load($cache_key) ) {
				$agency = $this->user->getAgency();
				$cache->save($agency, $cache_key, array(), 300);
			}

			$this->agency = $agency;
			$this->view->active_agency_escorts = $agency->getEscortsWithPackages();
			$this->view->all_agency_escorts = $agency->getEscorts();
			
			$escort_id = $this->_getParam('escort_id');
			$a_escort_id = $this->_getParam('a_escort_id');
			if ( $escort_id ) {
				if ( ! $this->agency->hasEscort($escort_id) ) {
					$this->_redirect($this->view->getLink('private-v2'));
				}
				$this->_model = new Cubix_PrivateMessaging('escort', $escort_id);
				
				if ( ! $this->_model->checkIfActiveHasPackage($escort_id) ) {
					$this->can_write_message = false;
				}
			} else {
				if ( $a_escort_id && ! $this->agency->hasEscort($a_escort_id) ) {
					$this->_redirect($this->view->getLink('private-v2'));
				}
				$this->_model = new Cubix_PrivateMessaging('agency', $this->agency->id);
			}
		}
		else if ( $this->user->isEscort() ) {

			if ( ! $escort = $cache->load($cache_key) ) {
				$escort = $this->user->getEscort();
				$cache->save($escort, $cache_key, array(), 300);
			}
			$this->escort =  $escort;
			$this->_model = new Cubix_PrivateMessaging('escort', $escort->id);
			if ( ! $this->_model->checkIfActiveHasPackage($escort->id) ) {
				$this->can_write_message = false;
			}
		}
		else if ( $this->user->isMember() ) {
			if ( $this->user->status != 1 ) {
				$this->can_write_message = false;
			}
			$this->_model = new Cubix_PrivateMessaging('member', $this->user->id);
		}
		$this->view->can_write_message = $this->can_write_message;
	}
	
	public function indexAction() 
	{
		$this->view->layout()->disableLayout();
		
		$this->getThreadsAction();
		$this->contactsAction();
	}
	
	public function sendMessageAjaxAction()
	{
		$this->view->layout()->disableLayout();
		
		$data = new Cubix_Form_Data($this->getRequest());
		$fields = array(
			'participant'	=> '',
			'captcha' => '',
			'message' => 'notags|special',
			'escort_id' => 'int-nz'
		);
		$data->setFields($fields);
		$data = $data->getData();
		
		$this->view->data = $data;
		
		list($participant_type, $participant_id) = explode('-', $data['participant']);

		
		if ( $participant_type == 'escort' ) {
			$m_esc = new Model_EscortsV2();
			$this->view->showname = $m_esc->getShownameById($participant_id);
		} else if ( $participant_type == 'member' ) {
			$m_user = new Model_Users();
			$us = $m_user->getById($participant_id);
			$this->view->username = $us->username;
		}
		
		if ( $this->user->isAgency() ) {
			$this->view->agency_escorts = $this->agency->getEscortsWithPackages();
		}
		
		if ( $this->_request->isPost() ) {
			$validator = new Cubix_Validator();
			
			//$blackListModel = new Model_BlacklistedWords();
			if ( ! $participant_type || ! $participant_id ) {
				$validator->setError('participant', 'Select Participant');
			}
			
			if ( ! strlen($data['message']) ) {
				$validator->setError('f-message', 'Message required');
			} /*elseif( $blackListModel->checkWords($data['message']) ) {
				$validator->setError('f-message', 'You can`t use words "'. $blackListModel->getWords() .'"');
			}*/
			
			if ( ! $data['captcha'] ) {
				$validator->setError('captcha', 'Captcha required');//Captcha is required
				}
			else {
				$session = new Zend_Session_Namespace('captcha');
				$orig_captcha = $session->captcha;

				if ( strtolower($data['captcha']) != $orig_captcha ) {
					$validator->setError('captcha', 'Captcha is invalid');//Captcha is invalid
				}
			}

			if ( $this->user->isAgency() && ! $data['escort_id'] ) {
				$validator->setError('escort_id', 'Select escort');
			}
			
			$result = $validator->getStatus();
			
			if ( ! $validator->isValid() ) {
				die(json_encode($result));
			} else {
				/* if message from pinboard */
				$pinboard_post_id = intval($this->_request->pinboard_post_id);
				
				if ($pinboard_post_id)
				{
					$data['message'] = '<strong>PINBOARD #' . $pinboard_post_id . ':</strong> ' . $data['message'];
				}
				/****************************/
				
				list($participant_type, $participant_id) = explode('-', $data['participant']);
				$this->_model->sendMessage($data['message'], $participant_type, $participant_id, false);
				
				$result['message'] = $data['message'];
				$result['sender'] = __('me');
				$result['date'] = date('Y-m-d', time()) . ' | ' . date('i:s', time());
				die(json_encode($result));
			}
		}
	}
	
	public function sendMessageFromProfileAction()
	{
		$this->view->layout()->disableLayout();
		
		$data = new Cubix_Form_Data($this->getRequest());
		$fields = array(
			'participant'	=> '',
			'captcha' => '',
			'message' => 'notags|special',
			'escort_id' => 'int-nz'
		);
		$data->setFields($fields);
		$data = $data->getData();
		
		$this->view->data = $data;
		
		list($participant_type, $participant_id) = explode('-', $data['participant']);

		
		if ( $participant_type == 'escort' ) {
			$m_esc = new Model_EscortsV2();
			$this->view->showname = $m_esc->getShownameById($participant_id);
		} else if ( $participant_type == 'member' ) {
			$m_user = new Model_Users();
			$us = $m_user->getById($participant_id);
			$this->view->username = $us->username;
		}
		
		if ( $this->user->isAgency() ) {
			$this->view->agency_escorts = $this->agency->getEscortsWithPackages();
		}
		
		if ( $this->_request->isPost() ) {
			$validator = new Cubix_Validator();
			
			//$blackListModel = new Model_BlacklistedWords();
			if ( ! $participant_type || ! $participant_id ) {
				$validator->setError('participant', 'Select Participant');
			}
			
			if ( ! strlen($data['message']) ) {
				$validator->setError('f-message', 'Message required');
			} /*elseif( $blackListModel->checkWords($data['message']) ) {
				$validator->setError('f-message', 'You can`t use words "'. $blackListModel->getWords() .'"');
			}*/
			
			if ( ! $data['captcha'] ) {
				$validator->setError('captcha', 'Captcha required');//Captcha is required
				}
			else {
				$session = new Zend_Session_Namespace('captcha');
				$orig_captcha = $session->captcha;

				if ( strtolower($data['captcha']) != $orig_captcha ) {
					$validator->setError('captcha', 'Captcha is invalid');//Captcha is invalid
				}
			}

			if ( $this->user->isAgency() && ! $data['escort_id'] ) {
				$validator->setError('escort_id', 'Select escort');
			}
			
			$result = $validator->getStatus();
			
			if ( ! $validator->isValid() ) {
				die(json_encode($result));
			} else {
				/* if message from pinboard */
				$pinboard_post_id = intval($this->_request->pinboard_post_id);
				
				if ($pinboard_post_id)
				{
					$data['message'] = '<strong>PINBOARD #' . $pinboard_post_id . ':</strong> ' . $data['message'];
				}
				/****************************/
				
				list($participant_type, $participant_id) = explode('-', $data['participant']);
				$this->_model->sendMessage($data['message'], $participant_type, $participant_id, false);
				
				$result['message'] = $data['message'];
				$result['sender'] = __('me');
				$result['date'] = date('Y-m-d', time()) . ' | ' . date('i:s', time());
				die(json_encode($result));
			}
		}
	}
	
	
	public function getThreadsAction()
	{
		$this->view->layout()->disableLayout();
		
		$a_escort_id = $this->_request->getParam('a_escort_id', null); // agency case
		
		$page = $this->_request->getParam('page', 1);
		$per_page = 4;
		
		$result = $this->_model->getThreads($page, $per_page, $a_escort_id);
		
		$this->view->threads = $result['data'];
		$this->view->count = $result['count'];
		$this->view->page = $page;
		$this->view->per_page = $per_page;
	}
	
	public function removeThreadAction()
	{
		$id = $this->_getParam('id');
		if ( ! $id ) die;
		
		$this->_model->removeThread($id);
		
		die;
	}
	
	public function blockThreadAction()
	{
		$id = $this->_getParam('id');
		$a_escort_id = $this->_getParam('a_escort_id', null);
		
		if ( ! $id ) die;
		
		$this->_model->blockThread($id, $a_escort_id);
		
		die;
	}
	
	public function unblockThreadAction()
	{
		$id = $this->_getParam('id');
		$a_escort_id = $this->_getParam('a_escort_id', null);
		
		if ( ! $id ) die;
		
		$this->_model->unblockThread($id, $a_escort_id);
		
		die;
	}
	
	public function getParticipantsAction()
	{
		$search = $this->_request->getParam('search');
		$participants = $this->_model->getParticipants($search);
		
		$result = array();
		
		//$result[] = array('escort-15', 'Escorts', null, null);
		
		foreach($participants['escorts'] as $p) {
			$result[] = array('escort-' . $p['id'], $p['showname'] . '(escort)', null, null);
		}
		
		//$result[] = array('members', 'Members', null, null);
		
		foreach($participants['members'] as $p) {
			$result[] = array('member-' . $p['id'], $p['username'] . '(member)', null, null);
		}
		
		echo json_encode($result);die;
	}
	
	public function contactsAction()
	{
		$this->view->layout()->disableLayout();
		
		$result = $this->_model->getContacts(1, 10);
		
		$this->view->contacts = $result['data'];
		$this->view->contacts_count = $result['count'];
		
		
		if ( $this->_getParam('ajax') == 1 ) {
			die(json_encode(array(
				'html'	=> $this->view->render('/private-messaging/contacts.phtml'),
				'data'	=> $result['data']
			)));
		}
	}
	
	public function removeContactAction()
	{
		$p = $this->_getParam('id');
		$p = explode('-', $p);
		
		list($type, $id) = $p;
		if ( ! $id || ! $type ) die;
		
		$this->_model->removeContact($id, $type);
		
		if ( $this->_getParam('get_html') ) {
			$result = $this->_model->getContacts(1, 10);
			$this->view->contacts = $result['data'];
			die(json_encode(array('html' => $this->view->render('/private-messaging/contacts.phtml'))));
		}
		
		die;
	}
	
	public function addContactAction()
	{
		$p = $this->_getParam('id');
		$p = explode('-', $p);
		
		list($type, $id) = $p;
		if ( ! $id || ! $type ) die;
		$this->_model->addContact($id, $type);
		
		die;
	}
	
	
	public function getThreadAction()
	{
		$this->view->layout()->disableLayout();
		
		$this->view->id = $id = $this->_getParam('id');
		$this->view->escort_id = $escort_id = $this->_getParam('escort_id');
		
		if ( ! $id  ) {
			$this->_redirect($this->view->getLink('private-v2'));
		}
		if ( $this->user->isAgency() && ! $escort_id ) {
			$this->_redirect($this->view->getLink('private-v2'));
		}
		
		
		$result = $this->_model->getThread($id, null, null, 'asc');
		$thread = $result['data'];
		
		$participant = $this->_model->getThreadParticipant($id);
		
		$this->view->thread = $thread;
		$this->view->count = $result['count'];
		$this->view->page = $page;
		$this->view->per_page = $per_page;
		
		
		$participant_passive = false;
		if ( $participant['user_id'] ) {
			$participant_name = $participant['member_username'];
			$participant_id = 'member-' . $participant['user_id'];
			if ( $participant['status'] != 1 ) {
				$participant_passive = true;
			}
		} else {
			$participant_name = $participant['escort_showname'];
			$participant_id = 'escort-' . $participant['escort_id'];
			
			if ( ! $this->_model->checkIfActiveHasPackage($participant['escort_id']) ) {
				$participant_passive = true;
			}
			
		}
		$this->view->participant_passive = $participant_passive;
		$this->view->participant_name = $participant_name;
		$this->view->participant_id = $participant_id;
		
		if ( $this->user->isEscort() ) {
			$this->view->escort = $this->escort;
		} elseif ( $this->user->isAgency()  ) {
			$escorts = new Model_Escorts();
			$this->view->escort = $escorts->getById($escort_id);
			$this->view->self_escort_id = $escort_id;
		}
		
		$this->view->thread_id = $id;
		
		$this->view->layout()->global_btn_back_url = $this->view->getLink('private-messaging');
	}
	
	public function notifyByEmailAction(){
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$notify = intval($this->_getParam('notify'));
		$this->_model->notifyByMail($this->user->id,$notify);
		
	}
	
	public function notifyByEmailTextAction(){
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$notify = intval($this->_getParam('notify'));
		$this->_model->notifyByMailText($this->user->id,$notify);
	}
	
	public function checkForNewMessagesAction()
	{
		$curr_user =  Model_Users::getCurrent();
		if (  ! $curr_user ) {
			die(json_encode(array(
				'result'	=> false,
				'message'	=> 'Not logged in'
			)));
		}
		
		$this->view->layout()->disableLayout();
		$count = $this->_model->getUnreadThreadsCount();
		
		die(json_encode(array(
			'result'	=> true,
			'message'	=> 'Ok',
			'count'	=> $count
		)));
	}
}

?>
