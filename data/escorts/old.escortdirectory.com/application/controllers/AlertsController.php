<?php

class AlertsController extends Zend_Controller_Action
{	
	public function init()
	{
		$this->view->layout()->setLayout('private');

		$anonym = array();

		$this->user = Model_Users::getCurrent();
		$this->client = new Cubix_Api_XmlRpc_Client();

		$this->view->user = $this->user;

		if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}
	}

	public function indexAction()
	{
		
	}

	public function ajaxGetAllAction()
	{
		$this->view->layout()->disableLayout();

		$show_array = $this->view->show_array = array(
			'all' => $this->view->t('all'),
			'active' => $this->view->t('active'),
			'inactive' => $this->view->t('inactive')
		);

		$show = $this->view->show = $this->_request->getParam('show', 'all');
		$page = $this->view->page = $this->_request->getParam('page', 1);
		$per_page = $this->view->per_page = 10;
		$filter = array();
		
		if ($show == 'active')
		{
			$filter['show'] = 1;
		}
		elseif ($show == 'inactive')
		{
			$filter['show'] = 0;
		}

		$items = $this->client->call('Members.getEDAlertsGrouped', array($this->user->id, $filter, $page, $per_page));
		$this->view->count = $items['count'];
		$items = $items['data'];
		
		$escorts = array();
		
		if ($items)
		{
			foreach ($items as $i)
			{
				$escorts[] = $i['escort_id'];
			}
		}
		
		$items = $this->client->call('Members.getEDAlerts', array($this->user->id, $escorts));
		
		if ($items)
		{
			$arr = array();
			$esc_ids = array();

			foreach ($items as $item)
			{
				$arr[$item['escort_id']][] = $item['event'];
				$arr[$item['escort_id']]['status'] = $item['status'];

				if ($item['event'] == ALERT_ME_CITY)
					$arr[$item['escort_id']]['cities'] = $item['extra'];

				if (!in_array($item['escort_id'], $esc_ids))
					$esc_ids[] = $item['escort_id'];
			}
			
			$h = 1;
			$arr1 = $arr2 = array();
			
			foreach ($arr as $k => $a)
			{
				if ($h % 2 != 0)
					$arr1[$k] = $a;
				else
					$arr2[$k] = $a;
				
				$h ++;
			}
			
			$this->view->items1 = $arr1;
			$this->view->items2 = $arr2;

			$modelE = new Model_EscortsV2();
			$this->view->escorts = $modelE->getAlertEscrots(implode(',', $esc_ids));
		}
	}
	
	public function saveAction()
	{
		$this->view->layout()->disableLayout();
		
		$escort_id = intval($this->_getParam('escort_id'));
		$events = $this->_getParam('events');
		$cities = $this->_getParam('cities');
		
		if ($this->user->user_type != 'member' || $escort_id == 0)
		{
			echo 'error';
			die;
		}
		
		$model = new Model_Members();
		$c = $model->memberAlertsSave($this->user->id, $escort_id, $events, $cities);
		$modelE = new Model_EscortsV2();
		$showname = $modelE->getShownameById($escort_id);
		
		echo json_encode(array('status' => 'success', 'count' => $c, 'msg' => $this->view->t('alerts_manage'), 'msg_title' => $this->view->t('alert_save', array('SHOWNAME' => $showname))));
		die;
	}
	
	public function setStatusAction()
	{
		$this->view->layout()->disableLayout();
		
		$escort_id = intval($this->_getParam('escort_id'));
		$status = intval($this->_getParam('status'));
				
		if ($this->user->user_type != 'member' || $escort_id == 0)
		{
			echo 'error';
			die;
		}
		
		$this->client->call('Members.alertSetStatus', array($this->user->id, $escort_id, $status));
		
		echo json_encode(array('status' => 'success'));
		die;
	}
	
	public function openAction()
	{
		$this->view->layout()->disableLayout();
		
		$user = Model_Users::getCurrent();
		$this->view->escort_id = $escort_id = intval($this->_getParam('escort_id'));
		
		$modelE = new Model_EscortsV2();
		$this->view->showname = $modelE->getShownameById($escort_id);
		
		if ($user->user_type != 'member' || $escort_id == 0)
		{
			die;
		}
		
		$model = new Model_Members();
		
		$events = $model->getEscortAlerts($user->id, $escort_id);
		
		$arr = array();
		$cities_str = '';
			
		if ($events)
		{
			foreach ($events as $event)
			{
				$arr[] = $event['event'];
				
				if ($event['event'] == ALERT_ME_CITY)
					$cities_str = $event['extra'];
			}
		}
		
		sort($arr);
		$this->view->events_str = implode(',', $arr);
		$this->view->events = $arr;
		$this->view->cities_str = $cities_str;
	}
}
