<?php

class EscortsController extends Zend_Controller_Action
{
    public static $linkHelper;

    public function init()
    {
        self::$linkHelper = $this->view->getHelper('GetLink');
    }

    public static function _itemDecorator($item, $is_first = FALSE, $is_hidden = FALSE)
    {
        $has_childs = isset($item->childs) && is_array($item->childs) && count($item->childs);

        $classes = array();

        if ( $is_first ) $classes[] = 'first';
        if ( $has_childs ) $classes[] = 'sub';

        $link = '#';
        if ( ! $has_childs ) {
            $link = self::$linkHelper->getLink('escorts', array('filter' => $item->value, 'page' => NULL));
        }

        $title = $item->title;
        if ( $is_first && isset($item->title) ) {
            $title = $item->parent . ' - ' . $item->title;
        }

        $html = '<a ' . ($is_hidden ? 'style="display: none" ' : '') . '' . (count($classes) ? 'class="' . implode(' ', $classes) . '" ' : '') . 'href="' . $link . '"' . ($item->value ? ' rel="' . $item->value . '"' : '') . ' ' . ( true ? 'onclick="return Cubix.Filter.Set({ filter: ' . ($item->value ? "'" . $item->value . "'" : 'null') . ', page: null })"' : '' ) . '>' . $title . '</a>';

        if ( $is_first ) {
            $html = '<div class="input-w"><div class="i">' .
                $html .
                '</div></div>';
        }

        return $html;
    }

    public static function _sortItemDecorator($item, $is_first = FALSE, $is_hidden = FALSE)
    {
        $has_childs = isset($item->childs) && is_array($item->childs) && count($item->childs);

        $classes = array();

        if ( $is_first ) $classes[] = 'first';
        if ( $has_childs ) $classes[] = 'sub';

        $link = '#';
        if ( ! $has_childs ) {
            $link = self::$linkHelper->getLink('escorts', array('sort' => $item->value));
        }

        $title = $item->title;
        if ( $is_first && isset($item->title) ) {
            $title = $item->parent . ' - ' . $item->title;
        }

        $html = '<a ' . ($is_hidden ? 'style="display: none" ' : '') . '' . (count($classes) ? 'class="' . implode(' ', $classes) . '" ' : '') . 'href="' . $link . '"' . ($item->value ? ' rel="' . $item->value . '"' : '') . ' ' . ( true ? 'onclick="return Cubix.Filter.Set({ sort: \'' . $item->value . '\' })"' : '' ) . '>' . $title . '</a>';

        if ( $is_first ) {
            $html = '<div class="input-w"><div class="i">' .
                $html .
                '</div></div>';
        }

        return $html;
    }

    protected function _getFooterText($key = 'main')
    {
        // $model = new Model_StaticPage();

        // $lang_id = Cubix_I18n::getLang();

        // $page = $model->getBySlug('footer-text-escorts-' . $key, Cubix_Application::getId(), $lang_id);

        // if ( $page ) return $page->body;

        $seo = $this->view->seo('home-page-' . $key, null, array());

        if ( $seo ) {
            if ( strlen($seo->footer_text) ) {
                return $seo->footer_text;
            }
        }

        return '';
    }

    /**
     * Chat Online Escorts Action
     */
    public function chatOnlineAction()
    {
        $action = 'index';
        if ( ! is_null($this->_getParam('ajax')) ) {
            $action = 'list';
        }
        $this->_helper->viewRenderer->setScriptAction($action);

        $req = $this->_getParam('req');
        $param = explode('/', $req);
        $page = explode("_", ( isset($param[2]) ) ? $param[2] : '' );

        $ids = '';

        if ($this->view->allowChat2())
        {
            $m_e = new Model_EscortsV2();
            $res = $m_e->getChatNodeOnlineEscorts(1, 1000);
            $ids = $res['ids'];
        }

        if (strlen($ids) > 0)
        {
            $filter = array(
                'e.id IN (' . $ids . ')'
            );

            $page_num = 1;
            if ( isset($page[1]) && $page[1] ) {
                $page_num = $page[1];
            }

            $count = 0;
            $escorts = Model_Escort_List::getFiltered($filter, 'random', $page_num, 50, $count);
        }
        else
        {
            $count = 0;
            $escorts = array();
        }

        $this->view->count = $count;
        $this->view->escorts = $escorts;

        list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls();

        $this->view->chat_online = true;
    }

    /**
     * Happy Hours action
     */
    public function happyHoursAction()
    {
        $action = 'index';
        if ( ! is_null($this->_getParam('ajax')) ) {
            $action = 'list';
        }
        $this->_helper->viewRenderer->setScriptAction($action);

        $req = $this->_getParam('req');
        $param = explode('/', $req);
        $page = explode("_", ( isset($param[2]) ) ? $param[2] : '' );

        $filter = array('e.gender = ?' => 1, 'e.hh_is_active = ?' => 1);

        $page_num = 1;
        if ( isset($page[1]) && $page[1] ) {
            $page_num = $page[1];
        }

        $count = 0;
        $escorts = Model_Escort_List::getFiltered($filter, 'random', $page_num, 10, $count);

        $this->view->count = $count;
        $this->view->escorts = $escorts;

        list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls();

        $this->view->hh_page = true;
    }

    /**
     * Main premium spot action
     */
    public function mainPageAction()
    {
        $action = 'index';
        if ( ! is_null($this->_getParam('ajax')) ) {
            $action = 'list';
        }
        $this->_helper->viewRenderer->setScriptAction($action);
		
        $escorts = Model_Escort_List::getMainPremiumSpot();

        $this->view->escorts = $escorts;
        $this->view->no_paging = false;

        list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls();
        //$this->view->e_ids = Model_EscortsV2::getLateNightGirlsForMainPage();
        //print_r($this->view->e_ids);

        //////////////////////////////////////////////////////////////
        if ($this->view->allowChat2())
        {
            $model = new Model_EscortsV2();
            $res = $model->getChatNodeOnlineEscorts(1, 1000);
            $ids = $res['ids'];
            $this->view->escort_ids = explode(',', $ids);
        }
        /////////////////////////////////////////////////
    }

    /**
     * New escorts action
     */
    public function newListAction()
    {
        $this->view->layout()->setLayout('main-simple');

        $model = new Model_EscortsV2();

        $req = $this->_getParam('req');
        $param = explode('/', $req);
        $page = explode("_", ( isset($param[2]) ) ? $param[2] : '' );

        $this->view->country_id = $country_id = $this->_request->country_id;
        //print_r($country_id);
        $filter = array('e.gender = ?' => 1);

        if ( $country_id ) {
            $filter = array_merge($filter, array('e.country_id = ?' => $country_id));
        }

        $page_num = 1;
        if ( isset($page[1]) && $page[1] ) {
            $page_num = $page[1];
        }

        $count = 0;
        $escorts = Model_Escort_List::getFiltered($filter, 'newest', $page_num, 10, $count, 'new_list');

        if ( count($escorts) ) {
            foreach ( $escorts as $k => $escort ) {
                $cache_key = 'v2_' . $escort->showname . '_new_profile_' . Cubix_I18n::getLang() . '_page_' . $page_num . '_' . $escort->id;
                $cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
                $escorts[$k]->profile = $model->get($escort->id, $cache_key);
            }
        }

        $this->view->count = $count;
        $this->view->escorts = $escorts;

        $this->view->new_list_cache_key = 'new_list_' . $cache_key;
    }

    public function indexAction()
    {
        // <editor-fold defaultstate="collapsed" desc="If it is ajax request, disable the layout and render only list.phtml">
        if ( ! is_null($this->_getParam('ajax')) ) {
            $this->view->layout()->disableLayout();
            $this->_helper->viewRenderer->setScriptAction('list');
            $this->view->is_ajax = true;
        } else {
            setcookie("show_all_escorts", 0, 0, "/", "www." . preg_replace('#www.|dev.#', '', $_SERVER['HTTP_HOST']));
            //if ( isset($_COOKIE['show_all_escorts']) ) {
            $_COOKIE['show_all_escorts'] = 0;
            //}
        }
        // </editor-fold>

        // SEARCH PARAMS
        //$s_showname = $this->view->s_showname = $this->_request->showname;
        $top_search_input = $this->view->top_search_input = $this->_request->top_search_input;
        $ts_type = $this->view->ts_type = $this->_request->ts_type;
        $ts_slug = $this->view->ts_slug = $this->_request->ts_slug;

        $show_all_hidden_escorts = $this->view->show_all_hidden_escorts = (int) $this->_request->getParam('sahe', 0);

        $s_name = $this->view->s_name = $this->_request->name;
        $s_agency = $this->view->s_agency = $this->_request->agency;
        $s_agency_slug = $this->view->s_agency_slug = $this->_request->agency_slug;
        $s_gender = $this->view->s_gender = $this->_request->getParam('gender', array());
        $s_city = $this->view->s_city = $this->_request->city;
        $s_city_slug = $this->view->s_city_slug = $this->_request->city_slug;
        $s_phone = $this->view->s_phone = $this->_request->getParam('phone', null);
        $s_age_from = $this->view->s_age_from = (int)$this->_request->age_from;
        $s_age_to = $this->view->s_age_to = (int)$this->_request->age_to;
        $s_orientation = $this->view->s_orientation = (array) $this->_request->orientation;
        $s_keywords = $this->view->s_keywords = (array) $this->_request->keywords;
        $s_height_from = $this->view->s_height_from = (int)$this->_request->height_from;
        $s_height_to = $this->view->s_height_to = (int)$this->_request->height_to;
        $s_weight_from = $this->view->s_weight_from = (int)$this->_request->weight_from;
        $s_weight_to = $this->view->s_weight_to = (int)$this->_request->weight_to;
        $s_tatoo = $this->view->s_tatoo = (int)$this->_request->tatoo;
        $s_piercing = $this->view->s_piercing = (int)$this->_request->piercing;

        $s_ethnicity = $this->view->s_ethnicity = (array) $this->_request->ethnicity;
        $s_language = $this->view->s_language = (array) $this->_request->language;
        $s_nationality = $this->view->s_nationality = (array) $this->_request->nationality;
        $s_hair_color = $this->view->s_hair_color = (array)$this->_request->hair_color;
        $s_hair_length = $this->view->s_hair_length = (array)$this->_request->hair_length;
        $s_cup_size = $this->view->s_cup_size = (array) $this->_request->cup_size;
        $s_eye_color = $this->view->s_eye_color = (array)$this->_request->eye_color;
        $s_pubic_hair = $this->view->s_pubic_hair = (array) $this->_request->pubic_hair;

        $s_currency = $this->view->s_currency = $this->_request->s_currency;
        $s_price_from = $this->view->s_price_from = (int)$this->_request->price_from;
        $s_price_to = $this->view->s_price_to = (int)$this->_request->price_to;
        $s_sex_availability = $this->view->s_sex_availability = (array) $this->_request->sex_availability;
        $s_service = $this->view->s_service = $this->_request->getParam('service', array());

        $s_incall_other = $this->view->s_incall_other = (int)$this->_request->incall_other;
        $s_outcall_other = $this->view->s_outcall_other = (int)$this->_request->outcall_other;
        $s_incall = $this->view->s_incall = (array)$this->_request->incall;
        $s_incall_hotel_room = $this->view->s_incall_hotel_room = (array) $this->_request->incall_hotel_room;
        $s_outcall = $this->view->s_outcall = (array)$this->_request->outcall;
        $s_travel_place = $this->view->s_travel_place = (array) $this->_request->travel_place;

        $s_real_pics = $this->view->s_real_pics = (int)$this->_request->s_real_pics;
        $s_verified_contact = $this->view->s_verified_contact = (int)$this->_request->s_verified_contact;
        $s_with_video = $this->view->s_with_video = (int)$this->_request->s_with_video;
        $s_pornstar = $this->view->s_pornstar = (int)$this->_request->s_pornstar;
        $s_is_online_now = $this->view->s_is_online_now = (int)$this->_request->s_is_online_now;
        $online_now = $this->view->online_now = (int)$this->_request->online_now;

        $this->view->p_id = $this->_request->getParam('p_id');

        $segmented = $this->view->segmented = $this->_getParam('segmented', 0);
		
        $s_filter_params = array(
            'name' => $s_name,
            'agency-name' => $s_agency_slug,
            'gender' => $s_gender,
            'city' => $s_city_slug,
            'phone' => ($s_phone) ? "%" . $s_phone . "%" : '',
            'age-from' => $s_age_from,
            'age-to' => $s_age_to,
            'orientation' => $s_orientation,
            'keywords' => $s_keywords,
            'height-from' => $s_height_from,
            'height-to' => $s_height_to,
            'weight-from' => $s_weight_from,
            'weight-to' => $s_weight_to,
            'tatoo' => $s_tatoo,
            'piercing' => $s_piercing,

            'ethnicity' => $s_ethnicity,
            'language' => $s_language,
            'nationality' => $s_nationality,
            'hair-color' => $s_hair_color,
            'hair-length' => $s_hair_length,
            'cup-size' => $s_cup_size,
            'eye-color' => $s_eye_color,
            'pubic-hair' => $s_pubic_hair,

            'currency' => $s_currency,
            'price-from' => $s_price_from,
            'price-to' => $s_price_to,
            'sex-availability' => $s_sex_availability,
            'service' => $s_service,

            'incall-other' => $s_incall_other,
            'outcall-other' => $s_outcall_other,
            'incall' => $s_incall,
            'incall-hotel-room' => $s_incall_hotel_room,
            'outcall' => $s_outcall,
            'travel-place' => $s_travel_place,

            'real-pics' => $s_real_pics,
            'verified-contact' => $s_verified_contact,
            'with-video' => $s_with_video,
            'pornstar' => $s_pornstar,
            'is-online-now' => $s_is_online_now,
            'show-all-hidden-escorts' => $show_all_hidden_escorts,
        );

        if ( $ts_type == 'city' ) {
            $s_filter_params['city'] = $ts_slug;
        } else if ( $ts_type == 'country' ) {
            $s_filter_params['country'] = $ts_slug;
        } else if ( $ts_type == 'escort' ) {
            $s_filter_params['name'] = $ts_slug;
        } else if ( $ts_type == 'escort-see-all' ) {
            $s_filter_params['name'] = $ts_slug . "%";
        }

        $is_active_filter = false;
        foreach( $s_filter_params as $i => $par ) {
            if ( is_array($par) ) {
                if( count($par) ) {
                    $is_active_filter = true;
                    break;
                }
            } else {
                if( $par ) {
                    $is_active_filter = true;
                    break;
                }
            }
        }

        $this->view->is_active_filter = $is_active_filter;


        // SEARCH PARAMS

        $req = $this->_getParam('req');
        $req = explode('/', $req);

        $cache = Zend_Registry::get('cache');
        $defs = Zend_Registry::get('definitions');
        /*$menus = array('filter' => NULL, 'sort' => NULL);

        $cache = Zend_Registry::get('cache');
        $defs = Zend_Registry::get('definitions');

        $filterStructure = $defs['escorts_filter'];

        $menus['filter'] = new Cubix_NestedMenu(array('childs' => $filterStructure));
        $menus['filter']->setId('filter-options');
        $menus['filter']->setSelected($menus['filter']->getByValue(NULL));

        $menus['sort'] = new Cubix_NestedMenu(array('childs' => $defs['escorts_sort']));
        $menus['sort']->setId('sort-options');
        $menus['sort']->setSelected($menus['sort']->getByValue('random'));
        $this->view->menus = $menus;*/

        $params = array(
            'sort' => 'random',
            'filter' => array(array('field' => 'girls', 'value' => array())),
            'page' => 1
        );

        $static_page_key = 'main';
        $is_tour = false;

        $s_config = Zend_Registry::get('system_config');
        $this->view->showMainPremiumSpot = $s_config['showMainPremiumSpot'];

        if ( ! $s_config['showMainPremiumSpot'] ) {
            $static_page_key = 'regular';
        }

        //if ( ! $is_active_filter ) {
            $p_top_category = $this->view->p_top_category = $this->_request->getParam('p_top_category', 'escorts');           

            $f_top_category = $this->_request->getParam('f_top_category');

            if ( $f_top_category ) {
                $p_top_category = $this->view->f_top_category = $f_top_category;
            }
        //}

        $p_country_slug = $this->view->p_country_slug = $this->_request->p_country_slug;
        $p_country_id = $this->view->p_country_id = (int) $this->_request->p_country_id;

        $p_city_slug = $this->view->p_city_slug = $this->_request->p_city_slug;
        $p_city_id = $this->view->p_city_id = (int) $this->_request->p_city_id;

        /* OPEN COUNTRY/CITY SIDEBAR WHEN COUNTRY SELECTED IN FILTER */
        if ( $this->_request->ts_type == 'country' && $this->_request->ts_slug ) {
            $p_country_slug = $this->view->p_country_slug = $this->_request->ts_slug;
            $p_country_id = $this->view->p_country_id = Cubix_Geography_Countries::getIdBySlug($p_country_slug);

            $p_city_slug = $this->view->p_city_slug = null;
            $p_city_id = $this->view->p_city_id = null;
        }
		
		if ( ($this->_request->ts_type == 'city' && $this->_request->ts_slug) || $this->_request->city_slug ) {
            $c_slug = $this->_request->city_slug;
            if ( $this->_request->ts_slug ) $c_slug = $this->_request->ts_slug;

            $p_city_slug = $this->view->p_city_slug = $c_slug;
            $p_city_id = $this->view->p_city_id = Cubix_Geography_Cities::getIdBySlug($p_city_slug);

            $p_country_slug = $this->view->p_country_slug = null;
            $p_country_id = $this->view->p_country_id = null;
        }
        /* OPEN COUNTRY/CITY SIDEBAR WHEN COUNTRY SELECTED IN FILTER */

        if ( $p_top_category ) {
            switch( $p_top_category )
            {
                case 'newest':
                    $static_page_key = 'newest';
                    $params['filter'][0] = array('field' => 'newest', 'value' => array());
                    continue;
                case 'newest-independent':
                    $static_page_key = 'newest-independent';
                    $params['filter'][0] = array('field' => 'newest-independent', 'value' => array());
                    continue;
                case 'newest-agency-escorts':
                    $static_page_key = 'newest-agency-escorts';
                    $params['filter'][0] = array('field' => 'newest-agency-escorts', 'value' => array());
                    continue;

                case 'escorts':
                    $static_page_key = 'escorts';
                    $params['filter'][] = array('field' => 'escorts', 'value' => array());
                    continue;

                case 'escorts-only':
                    $static_page_key = 'escorts-only';
                    $params['filter'][] = array('field' => 'escorts-only', 'value' => array());
                    continue;

                case 'independent-escorts':
                    $static_page_key = 'independent-escorts';
                    $params['filter'][] = array('field' => 'independent-escorts', 'value' => array());
                    continue;
                case 'agency-escorts':
                    $static_page_key = 'agency-escorts';
                    $params['filter'][] = array('field' => 'agency', 'value' => array());
                    continue;

                case 'agencies':
                    $static_page_key = 'agencies';
                    $params['filter'][] = array('field' => 'agencies', 'value' => array());
                    continue;

                case 'bdsm':
                    $static_page_key = 'bdsm';
                    $params['filter'][0] = array('field' => 'bdsm', 'value' => array());
                    continue;
                case 'boys-trans':
                    $static_page_key = 'boys-trans';
                    $params['filter'][0] = array('field' => 'boys-trans', 'value' => array());
                    continue;
                 case 'boys':
                    $static_page_key = 'boys';
                    $params['filter'][0] = array('field' => 'boys', 'value' => array());
                    continue;
                 case 'boys-heterosexual':
                    $static_page_key = 'boys-heterosexual';
                    $params['filter'][0] = array('field' => 'boys-heterosexual', 'value' => array());
                    continue;
                 case 'boys-bisexual':
                    $static_page_key = 'boys-bisexual';
                    $params['filter'][0] = array('field' => 'boys-bisexual', 'value' => array());
                    continue;
                 case 'boys-homosexual':
                    $static_page_key = 'boys-homosexual';
                    $params['filter'][0] = array('field' => 'boys-homosexual', 'value' => array());
                    continue;
                 case 'trans':
                    $static_page_key = 'trans';
                    $params['filter'][0] = array('field' => 'trans', 'value' => array());
                    continue;


                case 'citytours':
                    $static_page_key = 'citytours';
                    $this->view->is_tour = $is_tour = true;
                    $is_city_tour = true;
                    $params['filter'][] = array('field' => 'tours', 'value' => array());
                    continue;
                case 'upcomingtours':
                    $static_page_key = 'upcomingtours';
                    $this->view->is_tour = $is_tour = true;
                    $this->view->is_upcomingtour = true;
                    $upcoming_tours = true;
                    $params['filter'][] = array('field' => 'upcomingtours', 'value' => array());
                    continue;


                case 'regular':
                    $static_page_key = 'regular';
                    $params['filter'][] = array('field' => 'regular', 'value' => array());
                    continue;
                default:
                    $this->_response->setRawHeader('HTTP/1.1 404 Not Found');
                    $this->_forward('error', 'error', null, array('error_msg' => '404 Page Not Found. Please try the following links instead.'));
                    return;
            }
        }

        $list_types = array('xl-gallery', 'gallery', 'list', 'simple');

        if ( isset($_COOKIE['list_type']) && $_COOKIE['list_type'] ) {
            $list_type = $_COOKIE['list_type'];

            if ( ! in_array($list_type, $list_types) ) {
                $list_type = $list_types[0];
            }

            $this->view->render_list_type = $list_type;
        } else {
            $list_type = $list_types[0];
            $this->view->render_list_type = $list_types[0];
        }

        $this->view->grouped_categories = $grouped_categories = array(
            'escorts',
            'agencies',
        );

        $show_grouped = true;
        /*if ( $list_type == 'simple' || in_array($static_page_key, $grouped_categories) ) {
            $show_grouped = true;
        }*/
        $this->view->show_grouped = $show_grouped;

        /*if ( $static_page_key == 'new' ) {
            $this->_request->setParam('country_id', $n_country_id);
            $this->_forward('new-list');
        }*/

        if ( $p_city_slug ) {
            $params['city'] = $p_city_slug;
            $params['filter'][] = array('field' => 'city_id', 'value' => array($p_city_id));
        }

        if ( $p_country_slug ) {
            $params['country'] = $p_country_slug;
            $params['filter'][] = array('field' => 'country_id', 'value' => array($p_country_id));
        }

        $this->view->static_page_key = $static_page_key;

        $filter_params = array(
            'order' => 'e.date_registered DESC',
            'limit' => array('page' => 1),
            'filter' => array()
        );

        // <editor-fold defaultstate="collapsed" desc="Map parameters from url to SQL where clauses">
        $filter_map = array(

            'name' => 'e.showname LIKE ?',
            'agency-name' => 'e.agency_slug = ?',
            'phone' => 'e.contact_phone_parsed LIKE ?',
            'age-from' => 'e.age >= ?',
            'age-to' => 'e.age <= ?',
            'height-from' => 'e.height >= ?',
            'height-to' => 'e.height <= ?',
            'weight-from' => 'e.weight >= ?',
            'weight-to' => 'e.weight <= ?',
            'tatoo' => 'e.tatoo = ?',
            'piercing' => 'e.piercing = ?',

            'real-pics' => 'e.verified_status = 2',
            'with-video' => 'with-video',
            'verified-contact' => 'e.last_hand_verification_date IS NOT NULL',
            'pornstar' => 'e.is_pornstar = 1',

            'incall-other' => 'e.incall_other IS NOT NULL AND e.incall_other <> ""',
            'outcall-other' => 'e.outcall_other IS NOT NULL AND e.outcall_other <> ""',

            //'ethnicity' => 'e.ethnicity = ?',

            //'gender' => 'e.gender IN (?)',
            //'height' => 'e.height < ? AND e.height > ?',
            //'weight' => 'e.weight < ? AND e.weight > ?',
            //'cup-size' => 'e.cup_size = ?',
            //'hair-color' => 'e.hair_color = ?',
            //'eye-color' => 'e.eye_color = ?',
            //'dress-size' => 'e.dress_size = ?',
            //'shoe-size' => 'e.shoe_size = ?',
            //'incall' => 'e.incall_type IS NOT NULL',
            //'outcall' => 'e.outcall_type IS NOT NULL',
            //'service-for' => 'FIND_IN_SET(?, e.sex_availability)',
            //'smoker' => 'e.is_smoking = ?',
            //'language' => 'FIND_IN_SET(?, e.languages)',
            //'orientation' => 'e.sex_orientation = ?',
            //'now-open' => 'e.is_now_open',

            'region' => 'r.slug = ?',
            'city' => 'ct.slug = ?',
            'city_id' => 'ct.id = ?',
            'country_id' => 'cr.id = ?',
            'country' => 'cr.slug = ?',
            'cityzone' => 'c.id = ?',
            'zone' => 'cz.slug = ?',



            'boys-heterosexual' => 'e.sex_orientation = ' . ORIENTATION_HETEROSEXUAL . ' AND e.gender = ' . GENDER_MALE,
            'boys-bisexual' => 'e.sex_orientation = ' . ORIENTATION_BISEXUAL . ' AND e.gender = ' . GENDER_MALE,
            'boys-homosexual' => 'e.sex_orientation = ' . ORIENTATION_HOMOSEXUAL . ' AND e.gender = ' . GENDER_MALE,


            'escorts' => 'eic.gender = ' . GENDER_FEMALE,
            'escorts-only' => 'eic.is_agency = 0 AND eic.gender = ' . GENDER_FEMALE,
            'agencies' => 'eic.is_agency = 1',
            'independent-escorts' => 'eic.is_agency = 0 AND eic.gender = ' . GENDER_FEMALE,
            'agency' => 'eic.is_agency = 1 AND eic.gender = ' . GENDER_FEMALE,
            'boys-trans' => '(eic.gender = ' . GENDER_MALE . ' OR eic.gender = ' . GENDER_TRANS . ')',
            'boys' => 'eic.gender = ' . GENDER_MALE,
            'trans' => 'eic.gender = ' . GENDER_TRANS,
            'bdsm' => 'eic.gender = ' . GENDER_BDSM,

            'newest' => 'eic.gender = ' . GENDER_FEMALE,
            'newest-independent' => 'eic.is_agency = 0 AND eic.gender = ' . GENDER_FEMALE,
            'newest-agency-escorts' => 'eic.is_agency = 1 AND eic.gender = ' . GENDER_FEMALE,

            'tours' => 'eic.is_tour = 1',
            'upcomingtours' => 'eic.is_upcoming = 1'
        );

        // <editor-fold defaultstate="collapsed" desc="Get page from request parameters">
        if ( $this->_request->page ) {
            $params['page'] = $this->_request->page;
        }

        if ( $this->_request->p_page && ! $this->_request->page ) {
            $params['page'] = $this->_request->p_page;
        }

        $page = intval($params['page']);
        if ( $page == 0 ) {
            $page = 1;
        }
        $filter_params['limit']['page'] = $this->view->page = $page;
        // </editor-fold>

        //$selected_filter = $menus['filter']->getSelected();

        foreach ( $params['filter'] as $i => $filter ) {

            if ( ! isset($filter_map[$filter['field']]) ) continue;

            $value = $filter['value'];

            if ( isset($filter['main']) ) {
                if ( isset($selected_filter->internal_value) ) {
                    $value = $selected_filter->internal_value;
                }
                elseif ( ! is_null($item = $menus['filter']->getByValue($filter['field'] . ( (isset($value[0]) && $value[0]) ? '_' . $value[0] : '')) ) ) {
                    $value = $item->internal_value;
                }

            }


            $filter_params['filter'][$filter_map[$filter['field']]] = $value;
        }
        $filter_params['filter']['online-now'] = $online_now;

        if ( $online_now ) {
            unset($filter_params['filter']['eic.gender = 1']);
        }
        // </editor-fold>

        foreach ( $s_filter_params as $i => $s_filter ) {
            if ( ! isset($filter_map[$i]) ) continue;

            if ( $s_filter ) {
                $filter_params['filter'][$filter_map[$i]] = $s_filter;
            }
        }

        //Currency rates log
        $default_currency = "USD";
        $current_currency = $this->_getParam('currency', $default_currency);

        if ( $current_currency != $_COOKIE['currency'] && $this->_request->s ) {
            setcookie("currency", $current_currency, strtotime('+1 year'), "/", "www." . preg_replace('#www.|dev.#', '', $_SERVER['HTTP_HOST']));
            $_COOKIE['currency'] = $current_currency;
        }

        if ( isset($_COOKIE['currency']) ) {
            $current_currency = $_COOKIE['currency'];
            $this->_request->setParam('currency', $current_currency);
        }

        $this->view->current_currency = $current_currency;

        //Currency rates log

        // <editor-fold defaultstate="collapsed" desc="Sorting parameters SQL mappings">
        $default_sorting = "close-to-me";
        $current_sorting = $this->_getParam('sort', $default_sorting);


        $sort_map = array(
            'newest' => 'date_registered DESC',
            'last-contact-verification' => 'last_hand_verification_date DESC',             
            'price-asc' => 'price-asc', // will be modified from escorts
            'price-desc' => 'price-desc', // will be modified from escorts
            'last_modified' => 'date_last_modified DESC',
            'last-connection' => 'refresh_date DESC',            
            'close-to-me' => 'ordering DESC',
            'most-viewed' => 'hit_count DESC',
            'random' => 'ordering DESC',
            'by-country' => 'ordering DESC',           
            'by-city' => 'ordering DESC',           
            'alpha' => 'showname ASC',
            'age' => '-age DESC',
        );

        if ( !array_key_exists($current_sorting, $sort_map) ) {
            $current_sorting = $default_sorting;
            $this->_request->setParam('sort', $default_sorting);
        }

        if ( $current_sorting != $_COOKIE['sorting'] && $this->_request->s ) {
            setcookie("sorting", $current_sorting, strtotime('+1 year'), "/", "www." . preg_replace('#www.|dev.#', '', $_SERVER['HTTP_HOST']));
            setcookie("sortingMap", $sort_map[$current_sorting]['map'], strtotime('+1 year'), "/", "www." . preg_replace('#www.|dev.#', '', $_SERVER['HTTP_HOST']));
            $_COOKIE['sorting'] = $current_sorting;
            $_COOKIE['sortingMap'] = $sort_map[$current_sorting]['map'];
        }

        if ( isset($_COOKIE['sorting']) /*&& ! $this->_request->isPost()*/ ) {
            $current_sorting = $_COOKIE['sorting'];
            $this->_request->setParam('sort', $current_sorting);
        }

        if ( $current_sorting ) {
            $tmp = $sort_map[$current_sorting];
            unset($sort_map[$current_sorting]);
            $sort_map = array_merge(array( $current_sorting => $tmp), $sort_map);
        }

        if ( isset($sort_map[$params['sort']]) ) {
            $filter_params['order'] = $sort_map[$params['sort']];
            $this->view->sort_param = $params['sort'];
        }

        $this->view->sort_map = $sort_map;
        $this->view->current_sort = $current_sorting;
        $params['sort'] = $current_sorting;
        // </editor-fold>

        $model = new Model_EscortsV2();
        $count = 0;

        // <editor-fold defaultstate="collapsed" desc="Cache key generation">
        $filters_str = '';

        foreach ( $filter_params['filter'] as $k => $filter ) {
            if ( ! is_array($filter) ) {
                $filters_str .= $k . '_' . $filter;
            }
            else {
                $filters_str .= $k;
                foreach($filter as $f) {
                    $filters_str .= '_' . $f;
                }
            }
        }
        // </editor-fold>

        $s_config = Zend_Registry::get('system_config');
        $e_config = Zend_Registry::get('escorts_config');

        $per_page = $e_config['perPage'];

        if ( $list_type == 'xl-gallery' ) {
            $per_page = $e_config['perPageXL'];
        } elseif ( $list_type == 'simple' ) {
            $per_page = $e_config['perPageSimple'];
        }

        /*if ( ! $segmented ) {
            $per_page = $filter_params['limit']['page'] * $per_page;
            $filter_params['limit']['page'] = 1;
        }*/
        $this->view->per_page = $per_page;

        $cache_key = 'v2_' . Cubix_I18n::getLang() . '_' . $params['sort'] . '_' . $filter_params['limit']['page'] . '_' . $filters_str . (string) $has_filter . (string) $segmented . $per_page . $p_top_category . $p_country_id . $p_city_id . (int) $upcoming_tours . (int) $is_tour . $list_type;
        $cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
        $this->view->params = $params;

        $this->view->is_main_page = $is_main_page = ($static_page_key == 'escorts' && ! $p_city_id && ! $p_country_id );

        // If we are on main premium spot forward to corresponding action
        /*if ( ! $s_config['showMainPremiumSpot'] ) {
            if ( $is_main_page ) {
                return $this->_forward('main-page');
            }
        }*/

        if ( $static_page_key == 'happy-hours' ) {
            return $this->_forward('happy-hours');
        }

        if ( $static_page_key == 'chat-online' ) {
            return $this->_forward('chat-online');
        }

        /* Ordering by country from GeoIp */
        if ($params['sort'] == 'random')
        {
            $modelC = new Model_Countries();

            $ret = $modelC->getByGeoIp();
            //if ($_GET['test'] == 1) { var_dump($ret);	die;		}
            if (is_array($ret))
            {
                $cache_key .= '_country_iso_' . $ret['country_iso'];
                $params['sort'] = $ret['ordering'];
            }
        }
        /**/

        foreach($s_filter_params as $k => $value ) {
            if ( is_array($value) ) {
                foreach( $value as $val ) {
                    $cache_key .= '_' . $val . '_';
                }
            } else {
                $cache_key .= '_' . $k . '_' . $value;
            }
        }

        if ( $show_grouped ) {
            $cache_key .= '_grouped';
        }

        $geo_cache = "";
        $geoData = array();
        if ( $params['sort'] == 'close-to-me' ) {
            $geoData = Cubix_Geoip::getClientLocation();
			if ($_GET['test'] == 1) { var_dump($geoData);	die;		}
            if ( strlen($geoData['country']) ) {
                $geo_cache = md5($geoData['latitude'] . $geoData['longitude']);
                $cache_key .= $geo_cache;
            }
        }

        $cache_key .= $this->view->render_list_type;
	    $cache_key .= Cubix_Countries::blacklistCountryCacheKeyCase();
        $cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

        $cache_key = sha1($cache_key);

        $cache_key_count = $cache_key . '_count';

        $count = $cache->load($cache_key_count);

        if ( $is_active_filter ) {
            $filter_params['filter']['show_all_agency_escorts'] = true;
        }

        $only_newest = false;
        if ( in_array($static_page_key, array('newest', 'newest-independent', 'newest-agency-escorts'))) {
            $params['sort'] = 'newest';
            $filter_params['filter']['e.is_new = 1'] = array();
            $only_newest = true;
        }
		

        if ( ! $escorts = $cache->load($cache_key) ) {
			if($_GET['test'] == 2){ die('no cache');}
            if ( isset($is_tour) && $is_tour ) {

                /*if ( ! $upcoming_tours ) {
                    unset($filter_params['filter']['cr.id = ?']);
                    unset($filter_params['filter']['ct.id = ?']);
                    if ( $p_country_id ) {
                        $filter_params['filter']['crt.id = ?'] = $p_country_id;
                    }
                    if ( $p_city_id ) {
                        $filter_params['filter']['ctt.id = ?'] = $p_city_id;
                    }
                } else {

                }*/
                $escorts = Model_Escort_List::getTours($upcoming_tours, $filter_params['filter'], $params['sort'], $filter_params['limit']['page'], $per_page, $count, $s_filter_params, $show_grouped, $geoData, $list_type, $current_currency);
            } else {

                /*if ( $p_top_category == 'escorts' && ! $is_active_filter ) {
                    $escorts = Model_Escort_List::getForMainPage($filter_params['filter'], $params['sort'], $filter_params['limit']['page'], $per_page, $count, 'regular_list', false, $s_filter_params, $show_grouped, $geoData, $list_type, $p_top_category, $p_city_id, $p_country_id);
                } else {*/
                $escorts = Model_Escort_List::getFiltered($filter_params['filter'], $params['sort'], $filter_params['limit']['page'], $per_page, $count, 'regular_list', false, $s_filter_params, $show_grouped, $geoData, $list_type, $p_top_category, $p_city_id, $p_country_id, $current_currency);
                //}
                $this->view->no_cache = true;
            }

            $cache->save($escorts, $cache_key, array());
            $cache->save($count, $cache_key_count, array());
        }

        if(!isset($is_tour)){
            $availableFilters = Model_Escort_List::getFiltered($filter_params['filter'], $params['sort'], $filter_params['limit']['page'], $per_page, $count, 'regular_list', false, $s_filter_params, $show_grouped, $geoData, $list_type, $p_top_category, $p_city_id, $p_country_id, $current_currency, true);
        }

        $cache_key_bec = sha1("button_escort_counts_" . Cubix_Application::getId() . $p_country_id . $p_city_id . $online_now . $only_newest);
        if ( ! $button_escort_counts = $cache->load($cache_key_bec) ) {
            $button_escort_counts = Model_EscortsV2::getButtonEscortsCounts($p_country_id, $p_city_id, $online_now, $only_newest);
            $cache->save($button_escort_counts, $cache_key_bec, array());
        }
        $this->view->button_escort_counts = $button_escort_counts;

        $cache_key_bac = sha1("button_agency_counts_" . Cubix_Application::getId() . $p_country_id . $p_city_id . $online_now);
        if ( ! $button_agency_count = $cache->load($cache_key_bac) ) {
            $button_agency_count = Model_Agencies::getButtonAgenciesCounts($p_country_id, $p_city_id, $online_now);
            $cache->save($button_agency_count, $cache_key_bac, array());
        }
        $this->view->button_agency_count = $button_agency_count;


        $this->view->global_cache_key = $cache_key . $_COOKIE['show_all_escorts'] . $current_currency;

        /*if ( ! $is_tour && $static_page_key != 'newest' && ! $segmented ) {
            $n_cache_key =  $cache_key . '_newest_' . $_COOKIE['show_all_escorts'];

            if ( ! $newest_escorts = $cache->load($n_cache_key) ) {

                $filter_params['filter']['e.is_new = 1'] = array();

                if ( is_null($p_city_slug) && is_null($p_country_slug) && ( isset($_COOKIE['show_all_escorts']) && $_COOKIE['show_all_escorts'] == 0 ) ) {
                    $modelCities = new Model_Cities();

                    $geo_city = $modelCities->getByGeoIp();

                    //var_dump($geo_city);

                    //$geo_city = array('city_id' => null, 'city_slug' => null, 'country_slug' => 'armenia', 'country_id' => 71);

                    if ( $geo_city ) {
                        $filter_params['filter']['cr.id = ?'] = array($geo_city['country_id']);
                    }
                }

                $newest_escorts = Model_Escort_List::getFiltered($filter_params['filter'], 'newest', 1, 20, $ncount, 'regular_list', false, $s_filter_params);

                $cache->save($newest_escorts, $n_cache_key, array());
            }
        }*/

        // <editor-fold defaultstate="collapsed" desc="Find and Slice Premium Escorts">

        // </editor-fold>

        /*if ( $is_main_page && $filter_params['order'] == 'ordering DESC' ) {
            shuffle($escorts);
        }*/

        // Video Merge
        $escorts_video = array();
        if( ! empty($escorts) ) {
            $video_cache_key = "video_escort_list_" . Cubix_Application::getId() . $this->view->global_cache_key;
            
            if ( !$vescorts = $cache->load($video_cache_key) ) {
                $e_id = array();

                foreach($escorts as $e)
                {
                    if( !is_null($e->id)){
                        $e_id[] = $e->id;
                    }
                }
                if($escorts['vips']){
                    foreach($escorts['vips'] as $e)
                    {
                        $e_id[] = $e->id;
                    }
                }
                if($escorts['premiums']){
                    foreach($escorts['premiums'] as $e)
                    {
                        $e_id[] = $e->id;
                    }
                }
                $video = new Model_Video();
                $vescorts = $video->GetEscortsVideoArray($e_id);
                $cache->save($vescorts, $video_cache_key, array());
            }

            if(!empty($vescorts))
            {
                $app_id = Cubix_Application::getId();
                foreach($vescorts as &$v)
                {
                    $photo = new Cubix_ImagesCommonItem(array(
                        'hash'=> $v->hash,
                        'width'=> $v->width,
                        'height'=> $v->height,
                        'ext'=> $v->ext,
                        'application_id'=> $app_id
                    ));

                    $v->photo = $photo->getUrl('orig');
                    $escorts_video[$v->escort_id] = $v;
                }
                $config = Zend_Registry::get('videos_config');
                $host =  Cubix_Application::getById(Cubix_Application::getId())->host;
                $this->view->video_rtmp = $config['Media'].'mp4:'.$host;
				$this->view->video_source = $config['remoteUrl'].$host;

            }
        }

        $this->view->escorts_video = $escorts_video;
        $this->view->count = $count;
        $this->view->escorts = $escorts;

	    /* watched escorts */
	    if ($list_type != 'simple')
	    {
		    $escorts_buf = $escorts;
		    $ids = array();
		    $watched_escorts = array();

		    if (isset($escorts_buf['premiums']))
		    {
			    foreach ($escorts_buf['premiums'] as $e)
			    {
				    $ids[] = $e->id;
			    }

			    unset($escorts_buf['premiums']);
		    }

		    if (isset($escorts_buf['vips']))
		    {
			    foreach ($escorts_buf['vips'] as $e)
			    {
				    $ids[] = $e->id;
			    }

			    unset($escorts_buf['vips']);
		    }

		    foreach ($escorts_buf as $e)
		    {
			    $ids[] = $e->id;
		    }

		    if ($ids)
		    {
			    $ids_str = trim(implode(',', $ids), ',');
			    $user = Model_Users::getCurrent();
			    $modelM = new Model_Members();

			    if ($user)
			    {
				    $escorts_watched_type = $user->escorts_watched_type;

				    if (!$escorts_watched_type) $escorts_watched_type = 1;

				    $res = $modelM->getFromWatchedEscortsForListing($ids_str, $escorts_watched_type, $user->id, session_id());
			    }
			    else
			    {
				    $res = $modelM->getFromWatchedEscortsForListing($ids_str, WATCH_TYPE_PER_SESSION, null, session_id());
			    }

			    if ($res)
			    {
				    foreach ($res as $r)
				    {
					    $watched_escorts[] = $r->escort_id;
				    }
			    }
		    }

		    $this->view->watched_escorts = $watched_escorts;
	    }
	    /**/

        $this->view->has_filter = $has_filter;

        $this->view->newest_escorts = $newest_escorts;


        if ( ! is_null($this->_getParam('ajax')) ) {
            $this->view->is_ajax = true;

            $tidy = new Tidy();

            $config = array(
                'clean'         => true,
                'output-xhtml'   => false,
                'drop-proprietary-attributes' => false,
                'join-styles' => false,
                'join-classes' => false,
                'merge-divs' => false,
                'drop-empty-elements' => false,
                'drop-empty-paras' => false,
                'show-body-only' => false,
                'doctype' => 'user',
            );


            $side_bar = $this->view->render('escorts/side-bar-countries.phtml');
            $tidy->parseString($side_bar, $config, 'utf8');
            $tidy->cleanRepair();
            $side_bar = tidy_get_output($tidy);


            $flags = $this->view->render('escorts/flags.phtml');

            if ( $list_type == 'simple' ) {
                $body = $this->view->render('escorts/list-grouped.phtml');
            } else {
                $body = $this->view->render('escorts/list.phtml');
            }
            /*$tidy->parseString($body, $config, 'utf8');
            $tidy->cleanRepair();
            $body = tidy_get_output($tidy);*/

            $top_menu = $this->view->render('escorts/top-menu.phtml');
            $footer_menu = $this->view->render('escorts/footer-menu.phtml');

            $entity = $static_page_key;

            if ( isset($params['city']) ) {
                $entity = 'city-' . $static_page_key;
            }

            if ( $p_country_id ) {
                $entity = 'country-' . $static_page_key;
            }

            if ( isset($entity) ) {
                $tpl_data = array();
                $primary_id = 0;


                switch ( $entity ) {
                    case 'city':
                    case 'city-escorts':
                    case 'city-escorts-only':
                    case 'city-independent-escorts':
                    case 'city-agency':
                    case 'city-agencies':
                    case 'city-agency-escorts':
                    case 'city-bdsm':
                    case 'city-boys':
                    case 'city-trans':
                    case 'city-citytours':
                    case 'city-upcomingtours':
                    case 'city-newest':
                    case 'city-newest-independent':
                    case 'city-newest-agency-escorts':
                        $m = new Cubix_Geography_Cities();
                        $city = $m->getBySlug($params['city']);

                        if ( ! $city ) break;

                        $primary_id = $city->id;
                        $tpl_data['city'] = $city->title;
                        $tpl_data['region'] = $city->getRegionTitle();

                        break;
                    case 'country':
                    case 'country-escorts':
                    case 'country-escorts-only':
                    case 'country-independent-escorts':
                    case 'country-agency':
                    case 'country-agencies':
                    case 'country-agency-escorts':
                    case 'country-bdsm':
                    case 'country-boys':
                    case 'country-trans':
                    case 'country-citytours':
                    case 'country-upcomingtours':
                    case 'country-newest':
                    case 'country-newest-independent':
                    case 'country-newest-agency-escorts':
                        $m = new Cubix_Geography_Countries();
                        $country_title = $m->getTitleById($p_country_id);

                        if ( ! $country_title ) break;

                        $primary_id = $p_country_id;
                        $tpl_data['country'] = $country_title;
                        $tpl_data['region'] = '';

                        break;
                }

                $seo = $this->view->seo($entity, $primary_id, $tpl_data);

                $seo_title = $seo->title;
                if ( $this->view->p_top_category && $this->view->page > 1 ) {
                    $seo_title = $seo->title . ' page ' . $this->view->page;
                }
            }

            die(json_encode(array('body' => $body, 'escorts_count' => count($escorts), 'head_title' => $seo_title, 'top_menu' => $top_menu, 'footer_menu' => $footer_menu, 'side_bar' => $side_bar, 'flags' => $flags, 'availableFilters' => $availableFilters)));
        }
    }


    public function ajaxBubbleAction()
    {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $page = intval($this->_getParam('page', 1));
        if ( $page < 1 ) $page = 1;
        $per_page = 20;

        echo $this->view->bubbleTextsWidget($page, $per_page);
    }


    public function ajaxOnlineAction()
    {
        $page = intval($this->_getParam('page', 1));
        if ( $page < 1 ) $page = 1;
        $per_page = 5;
        $this->view->layout()->disableLayout();

        if ($this->view->allowChat2())
        {
            $model = new Model_EscortsV2();
            $results = $model->getChatNodeOnlineEscorts($page, $per_page);

            $this->view->escorts = $results['escorts'];
            $this->view->per_page = $per_page;
            $this->view->page = $page;
            $this->view->count = $results['count'];
        }
    }

    /**
     *
     * @param string $r
     */
    protected function _makeFilterArgument($r)
    {
        $param = explode('_', $r);

        if ( count($param) < 2 ) {
            switch( $r ) {
                case 'independents':
                case 'agency':
                case 'boys':
                case 'trans':
                case 'citytours':
                case 'upcomingtours':
                    return array('field' => $r, 'value' => array());
            }
        }

        $param_name = $param[0];
        array_shift($param);

        switch ( $param_name ) {
            case 'filter':
                $field = reset($param);
                array_shift($param);

                $value = array_slice($param, 0, 2);

                $filter = array('field' => $field, 'value' => $value , 'main' => TRUE);
                break;
            case 'region':
            case 'state':
                $filter = array('field' => 'region', 'value' => $param[1]);
                break;
            case 'city':
            case 'cityzone':
                $filter = array('field' => $param_name, 'value' => array($param[1]));
                break;
            case 'name':
                $filter = array('field' => 'name', 'value' => array('%' . $param[0] . '%'));
                break;
        }

        return $filter;
    }

    /**
     * Calculates filter counts for each
     *
     * @param integer $count
     * @param array $filterParams
     * @param Cubix_NestedMenu $menu
     * @param string $cacheKey
     */
    protected function _calcFilterCounts($count, $filterParams, Cubix_NestedMenu $menu, $cacheKey = null)
    {
        $model = new Model_EscortsV2();
        $defs = Zend_Registry::get('defines');

        $filterMap = array(
            'svc-anal' => 'e.svc_anal = ?',
            'svc-kissing' => 'e.svc_kissing = ?',
            'svc-kissing-with-tongue' => 'e.svc_kissing = ?',
            'svc-blowjob-with-condom' => 'e.svc_blowjob = ?',
            'svc-blowjob-without-condom' => 'e.svc_blowjob = ?',
            'svc-cumshot-in-face' => 'e.svc_cumshot = ?',
            'svc-cumshot-in-mouth-swallow' => 'e.svc_cumshot = ?',
            'svc-cumshot-in-mouth-spit' => 'e.svc_cumshot = ?',

            'age' => 'e.age BETWEEN ? AND ?',
            'ethnic' => 'e.ethnicity = ?',
            'height' => 'e.height > ? AND e.height < ?',
            'weight' => 'e.weight > ? AND e.weight < ?',
            'breast-size' => 'e.breast_size = ?',
            'hair-color' => 'e.hair_color = ?',
            'hair-length' => 'e.hair_length = ?',
            'eye-color' => 'e.eye_color = ?',
            'dress-size' => 'e.dress_size = ?',
            'shoe-size' => 'e.shoe_size = ?',
            'available-for' => 'e.availability = ?',
            'service-for' => 'FIND_IN_SET(?, e.sex_availability)',
            'smoker' => 'e.is_smoker = ?',
            'language' => 'FIND_IN_SET(?, e.languages)',
            'now-open' => 'e.is_now_open',
            'region' => 'r.slug = ?',
            'city' => 'cc.slug = ?',
            'cityzone' => 'c.id = ?',
            'name' => 'e.showname LIKE ?',

            'new_arrivals' => 'e.gender = ' . GENDER_FEMALE,
            'independent' => 'e.agency_id IS NULL AND e.gender = ' . GENDER_FEMALE,
            'agency' => 'e.agency_id IS NOT NULL AND e.gender = ' . GENDER_FEMALE,
            'boys' => 'e.gender = ' . GENDER_MALE,
            'trans' => 'e.gender = ' . GENDER_TRANS,
            'girls' => 'e.gender = ' . GENDER_FEMALE,

            'tours' => 'e.is_on_tour = 1',
            'upcomingtours' => 'e.tour_date_from > CURDATE()'
        );

        $counts = array();

        $filterStructure = $defs['escorts_filter'];

        foreach ( $filterStructure as $i => $filter ) {
            $fake = false;
            if ( ! isset($filter['childs']) ) {
                $filter['childs'] = array($filter);
                $fake = true;
            }

            foreach ( $filter['childs'] as $j => $child ) {
                $f = $this->_makeFilterArgument('filter_' . $child['value']);

                $newFilter = $filterParams;

                if ( ! isset($filterMap[$f['field']]) ) continue;

                $value = $f['value'];

                if ( isset($f['main']) && $f['main'] ) {
                    if ( isset($child['internal_value']) ) {
                        $value = $child['internal_value'];
                    }
                }

                $newFilter['filter'][$filterMap[$f['field']]] = $value;

                $count = 0; // $model->getCount($newFilter);

                if ( $fake ) {
                    $filterStructure[$i]['title'] .= " ($count)";
                }
                else {
                    $filterStructure[$i]['childs'][$j]['title'] .= " ($count)";;
                }
            }
        }

        if ( ! is_null($cacheKey) ) {
            Zend_Registry::get('cache')->save($filterStructure, $cacheKey, array());
        }

        $menu->setStructure(array('childs' => $filterStructure));
    }

    public function profileAction()
    {
        $this->view->layout()->setLayout('profile');

        $this->view->is_profile = true;

        //////   redirect not active escorts to home page   ///////
        $m = new Model_EscortsV2();
        if ($this->_getParam('f') != 'b' && !$m->getShownameById($this->_getParam('escort_id')))
        {
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: /');
            die;
        }
        //////////////////////////////////////////////////////////

        $showname = $this->_getParam('escortName');
        $escort_id = $this->_getParam('escort_id');

        $model = new Model_EscortsV2();

        $cache_key = 'v2_' . $showname . '_' . $escort_id .  '_profile_' . Cubix_I18n::getLang();
        $cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

        $this->view->profile_cache_key = 'html_profile_' . $cache_key;

        //$escort = $model->get($showname, $cache_key);
        $escort = $model->get($escort_id, $cache_key);

        if ($escort->showname != $showname)
        {
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: /');
            die;
        }

        if ( ! $escort || isset($escort['error']) )
        {
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: /');
            die;
            return;
        }

        /*if ( isset($escort->escort_status) && $escort->escort_status != 32 ) {
            $this->_forward('profile-disabled');
            $this->_request->setParam('city_slug', $escort->city_slug);
        }*/

        $blockModel = new Model_BlockedCountries();
        if ( $blockModel->checkIp($escort->id) ){
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: /');
            die;
            return;
        }
        $vacation = new Model_EscortV2Item( array('id' => $escort->id  ));
        $vac = $vacation->getVacation();
        $add_esc_data = $model->getRevComById($escort->id);
        $bl = $model->getBlockWebsite($escort->id);
        $block_website = $bl->block_website;
        $check_website = $bl->check_website;

        $escort->block_website = $block_website;
        $escort->check_website = $check_website;
        $escort->disabled_reviews = $add_esc_data->disabled_reviews;
        $escort->disabled_comments = $add_esc_data->disabled_comments;
        $escort->vac_date_from = $vac->vac_date_from;
        $escort->vac_date_to = $vac->vac_date_to;
        $this->view->escort_id = $escort->id;
        $this->view->escort = $escort;

        if ( $escort->home_city_id ) {
            $m_city = new Cubix_Geography_Cities();
            $home_city = $m_city->get($escort->home_city_id);

            $m_country = new Cubix_Geography_Countries();
            $home_country = $m_country->get($home_city->country_id);

            $this->view->home_city_country = $home_city->{'title_' . $this->_request->lang_id} . ' (' . $home_country->{'title_' . $this->_request->lang_id} . ')';
        }

        $bt = $model->getBubbleText($escort->id);
        $this->view->bubble_text = $bt ? $bt->bubble_text : null;

        $this->view->travel_place = $model->getTravelPlace($escort->id);
        /* Last Review */
        $cache = Zend_Registry::get('cache');
        $cache_key = 'reviews_' . $escort->id;
        if ( ! $res = $cache->load($cache_key) ) {
            try {
                $res = $model->getEscortLastReview($escort->id);
            }
            catch ( Exception $e ) {
                $res = array(false, false);
            }

            $cache->save($res, $cache_key, array());
        }

        list($review, $rev_count) = $res;
        $this->view->review = $review;
        $this->view->rev_count = $rev_count;
        /* Last Review */

        $this->photosAction($escort);

        $scheduleModel = new Model_Schedule();
        $this->view->schedules = $scheduleModel->getEscortSchedule( $escort->id, true );


        if ( $escort->agency_id )
            $this->agencyEscortsAction($escort->agency_id, $escort->id);

        $user = Model_Users::getCurrent();
        $this->view->user = $user;

        $paging_key = $this->_getParam('from');
        $paging_keys_map = array(
            'last_viewed_escorts',
            'search_list',
            'regular_list',
            'new_list',
            'profile_disabled_list',
            'premiums',
            'right_premiums',
            'main_premium_spot',
            'tours_list',
            'tour_main_premium_spot',
            'up_tours_list',
            'up_tour_main_premium_spot'
        );

        if ( in_array($paging_key, $paging_keys_map) ) {
            $sid = 'sedcard_paging_' . Cubix_Application::getId();
            $ses = new Zend_Session_Namespace($sid);

            $ses_pages = $ses->{$paging_key};
            $criterias = $ses->{$paging_key . '_criterias'};

            if ( ! isset($criterias['first_page']) ) {
                $criterias['first_page'] = $criterias['page'];
            }
            if ( ! isset($criterias['last_page']) ) {
                $criterias['last_page'] = $criterias['page'];
            }

            $ses_index = 0;
            if ( is_array($ses_pages) ) {
                $ses_index = array_search($escort_id, $ses_pages);
            }

            if ( isset($ses_pages[$ses_index - 1]) ) {
                $this->view->back_showname = $model->getShownameById($ses_pages[$ses_index - 1]);
                $this->view->back_id = $back_id = $ses_pages[$ses_index - 1];
            } elseif ( $criterias['first_page'] > 1 ) { //Loading previous page escorts
                switch( $paging_key ) {
                    case 'regular_list' :
                        $prev_page_escorts = Model_Escort_List::getFiltered($criterias['filter'], $criterias['sort'], $criterias['first_page'] - 1);
                        break;
                    case 'tours_list':
                        $prev_page_escorts = Model_Escort_List::getTours($criterias['is_upcoming'], $criterias['filter'], $criterias['sort'], $criterias['first_page'] - 1);
                        break;
                }

                $p_escorts_count = count($prev_page_escorts);
                if ( count($p_escorts_count) ) {
                    $this->view->back_showname = $prev_page_escorts[$p_escorts_count - 1]->showname;
                    $this->view->back_id = $prev_page_escorts[$p_escorts_count - 1]->id;
                    //Adding previous page escorts to $ses_pages and set in session
                    $ids = array();
                    foreach($prev_page_escorts as $p_esc) {
                        $ids[] = $p_esc->id;
                    }

                    $criterias['first_page'] -= 1;
                    $ses->{$paging_key} = array_merge($ids, $ses_pages);
                    $ses->{$paging_key . '_criterias'} = $criterias;
                }
            }

            if ( isset($ses_pages[$ses_index + 1]) ) {
                $this->view->next_showname = $model->getShownameById($ses_pages[$ses_index + 1]);
                $this->view->next_id = $next_id = $ses_pages[$ses_index + 1];
            } else { //Loading next page escorts
                switch( $paging_key ) {
                    case 'regular_list' :
                        $next_page_escorts = Model_Escort_List::getFiltered($criterias['filter'], $criterias['sort'], $criterias['last_page'] + 1);
                        break;
                    case 'main_premium_spot':
                        $next_page_escorts = array();
                        break;
                    case 'tours_list':
                        $next_page_escorts = Model_Escort_List::getTours($criterias['is_upcoming'], $criterias['filter'], $criterias['sort'], $criterias['last_page'] + 1);
                        break;
                }

                if ( count($next_page_escorts) ) {
                    $this->view->next_showname = $next_page_escorts[0]->showname;
                    $this->view->next_id = $next_page_escorts[0]->id;
                    //Adding next page escorts to $ses_pages and set in session
                    foreach($next_page_escorts as $p_esc) {
                        $ses_pages[] = $p_esc->id;
                    }

                    $criterias['last_page'] += 1;
                    $ses->{$paging_key} = $ses_pages;
                    $ses->{$paging_key . '_criterias'} = $criterias;
                }
            }
            $this->view->paging_key = $paging_key;
        }

        /*list($prev, $next) = Model_Escort_List::getPrevNext($escort->showname);

        if ( ! is_null($prev) ) {
            $this->view->back_showname = $prev;
        }
        if ( ! is_null($next) ) {
            $this->view->next_showname = $next;
        }*/

        list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls();
    }

    public function profileV2Action()
    {
        $this->view->layout()->setLayout('main');

        if ( ! is_null($this->_getParam('ajax')) ) {
            $this->view->layout()->disableLayout();
            $this->view->is_ajax = true;
        }
		

        //Currency rates log
        $default_currency = "USD";
        $current_currency = $this->_getParam('currency', $default_currency);

        if ( $current_currency != $_COOKIE['currency'] && $this->_request->s ) {
            setcookie("currency", $current_currency, strtotime('+1 year'), "/", "www." . preg_replace('#www.|dev.#', '', $_SERVER['HTTP_HOST']));
            $_COOKIE['currency'] = $current_currency;
        }

        if ( isset($_COOKIE['currency']) ) {
            $current_currency = $_COOKIE['currency'];
            $this->_request->setParam('currency', $current_currency);
        }

        $this->view->current_currency = $current_currency;
        //Currency rates log


        $this->_helper->viewRenderer->setScriptAction("profile-v2/profile");

        $this->view->is_profile = true;

        //////   redirect not active escorts to home page   ///////
        $m = new Model_EscortsV2();
        if ($this->_getParam('f') != 'b' && !$m->getShownameById($this->_getParam('escort_id')))
        {
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: /');
            die;
        }
        //////////////////////////////////////////////////////////
		
        $showname = $this->_getParam('escortName');
        $escort_id = $this->_getParam('escort_id');

        $model = new Model_EscortsV2();

        $cache_key = 'v2_' . $showname . '_' . $escort_id .  '_profile_' . Cubix_I18n::getLang();
        $cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

        $this->view->profile_cache_key = 'html_profile_' . $cache_key;

        //$escort = $model->get($showname, $cache_key);
        $escort = $model->get($escort_id, $cache_key);

        $redirect = false;

        if ($escort->showname != $showname) {
            $redirect = true;
        }

        if ( ! $escort || isset($escort['error']) ) {
            $redirect = true;
        }

        $blockModel = new Model_BlockedCountries();
        if ( $blockModel->checkIp($escort->id) ) {
            $redirect = true;
        }

        if ( $redirect && ! $this->view->is_ajax ) {
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: /');
            die;
            return;
        } else if ( $redirect && $this->view->is_ajax ) {
            die;
        }
		
        $vacation = new Model_EscortV2Item( array('id' => $escort->id  ));
        $vac = $vacation->getVacation();
        $add_esc_data = $model->getRevComById($escort->id);
        $bl = $model->getBlockWebsite($escort->id);

        $vconfig = Zend_Registry::get('videos_config');
        $host =  Cubix_Application::getById(Cubix_Application::getId())->host;
        $video = new Model_Video();
        $escort_video = $video->GetEscortVideo($escort->id,1,NULL,FALSE);


        if(!empty($escort_video[0]))
        {
            $photo = new Cubix_ImagesCommonItem($escort_video[0][1]);
            $this->view->photo_video = $photo->getUrl('orig');
            $this->view->galerry_video =$photo->getUrl('pl');
            $this->view->vwidth = $photo['width'];
            $this->view->vheight = $photo['height'];

            $this->view->video =$vconfig['remoteUrl'].$host.'/'.$escort->id.'/'.$escort_video[1][0]->video;
        }

        $escort->block_website = $bl->block_website;
        $escort->check_website = $bl->check_website;
        $escort->bad_photo_history = $bl->bad_photo_history;
        $escort->disabled_reviews = $add_esc_data->disabled_reviews;
        $escort->disabled_comments = $add_esc_data->disabled_comments;
        $escort->vac_date_from = $vac->vac_date_from;
        $escort->vac_date_to = $vac->vac_date_to;
        $this->view->escort_id = $escort->id;
        $this->view->escort = $escort;

        $this->view->add_phones = $model->getAdditionalPhones($escort->id);
	    $this->view->apps = $model->getApps($escort->id);
		$this->view->natural_pic = $model->getNaturalPic($escort->id);
		
        if ( $escort->home_city_id ) {
            $m_city = new Cubix_Geography_Cities();
            $home_city = $m_city->get($escort->home_city_id);

            $m_country = new Cubix_Geography_Countries();
            $home_country = $m_country->get($home_city->country_id);

            $this->view->home_city_country = $home_city->{'title_' . $this->_request->lang_id} . ' (' . $home_country->{'title_' . $this->_request->lang_id} . ')';
        }

        $bt = $model->getBubbleText($escort->id);
        $this->view->bubble_text = $bt ? $bt->bubble_text : null;

        $this->view->travel_place = $model->getTravelPlace($escort->id);

        /* Last Review */
	    $m_r = new Model_Reviews();
	    $review = $m_r->getEscortLastReview($escort->id);

        $this->view->review = $review;
        /* Last Review */
		$this->view->galleries = $escort->getPhotoGalleries($escort->id);
		$this->galleryPhotosAction($escort);
        //$this->photosV2Action($escort);
		
        /*$scheduleModel = new Model_Schedule();
        $this->view->schedules = $scheduleModel->getEscortSchedule( $escort->id, true );*/


        if ( $escort->agency_id )
            $this->agencyEscortsV2Action($escort->agency_id, $escort->id);

        $user = Model_Users::getCurrent();
        $this->view->user = $user;

        /*$paging_key = $this->_getParam('from');
        $paging_keys_map = array(
            'last_viewed_escorts',
            'search_list',
            'regular_list',
            'new_list',
            'profile_disabled_list',
            'premiums',
            'right_premiums',
            'main_premium_spot',
            'tours_list',
            'tour_main_premium_spot',
            'up_tours_list',
            'up_tour_main_premium_spot'
        );

        if ( in_array($paging_key, $paging_keys_map) ) {
            $sid = 'sedcard_paging_' . Cubix_Application::getId();
            $ses = new Zend_Session_Namespace($sid);

            $ses_pages = $ses->{$paging_key};
            $criterias = $ses->{$paging_key . '_criterias'};

            if ( ! isset($criterias['first_page']) ) {
                $criterias['first_page'] = $criterias['page'];
            }
            if ( ! isset($criterias['last_page']) ) {
                $criterias['last_page'] = $criterias['page'];
            }

            $ses_index = 0;
            if ( is_array($ses_pages) ) {
                $ses_index = array_search($escort_id, $ses_pages);
            }

            if ( isset($ses_pages[$ses_index - 1]) ) {
                $this->view->back_showname = $model->getShownameById($ses_pages[$ses_index - 1]);
                $this->view->back_id = $back_id = $ses_pages[$ses_index - 1];
            } elseif ( $criterias['first_page'] > 1 ) { //Loading previous page escorts
                switch( $paging_key ) {
                    case 'regular_list' :
                        //$prev_page_escorts = Model_Escort_List::getFiltered($criterias['filter'], $criterias['sort'], $criterias['first_page'] - 1);
                        break;
                    case 'tours_list':
                        $prev_page_escorts = Model_Escort_List::getTours($criterias['is_upcoming'], $criterias['filter'], $criterias['sort'], $criterias['first_page'] - 1);
                        break;
                }

                $p_escorts_count = count($prev_page_escorts);
                if ( count($p_escorts_count) ) {
                    $this->view->back_showname = $prev_page_escorts[$p_escorts_count - 1]->showname;
                    $this->view->back_id = $prev_page_escorts[$p_escorts_count - 1]->id;
                    //Adding previous page escorts to $ses_pages and set in session
                    $ids = array();
                    foreach($prev_page_escorts as $p_esc) {
                        $ids[] = $p_esc->id;
                    }

                    $criterias['first_page'] -= 1;
                    $ses->{$paging_key} = array_merge($ids, $ses_pages);
                    $ses->{$paging_key . '_criterias'} = $criterias;
                }
            }

            if ( isset($ses_pages[$ses_index + 1]) ) {
                $this->view->next_showname = $model->getShownameById($ses_pages[$ses_index + 1]);
                $this->view->next_id = $next_id = $ses_pages[$ses_index + 1];
            } else { //Loading next page escorts
                switch( $paging_key ) {
                    case 'regular_list' :
                        //$next_page_escorts = Model_Escort_List::getFiltered($criterias['filter'], $criterias['sort'], $criterias['last_page'] + 1);
                        break;
                    case 'main_premium_spot':
                        $next_page_escorts = array();
                        break;
                    case 'tours_list':
                        $next_page_escorts = Model_Escort_List::getTours($criterias['is_upcoming'], $criterias['filter'], $criterias['sort'], $criterias['last_page'] + 1);
                        break;
                }

                if ( count($next_page_escorts) ) {
                    $this->view->next_showname = $next_page_escorts[0]->showname;
                    $this->view->next_id = $next_page_escorts[0]->id;
                    //Adding next page escorts to $ses_pages and set in session
                    foreach($next_page_escorts as $p_esc) {
                        $ses_pages[] = $p_esc->id;
                    }

                    $criterias['last_page'] += 1;
                    $ses->{$paging_key} = $ses_pages;
                    $ses->{$paging_key . '_criterias'} = $criterias;
                }
            }
            $this->view->paging_key = $paging_key;
        }*/

        /*list($prev, $next) = Model_Escort_List::getPrevNext($escort->showname);

        if ( ! is_null($prev) ) {
            $this->view->back_showname = $prev;
        }
        if ( ! is_null($next) ) {
            $this->view->next_showname = $next;
        }*/

        //list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls();

        /* watched */
        $model_m = new Model_Members();
        $sess_id = session_id();
        $user_id = null;
        $cur_user = Model_Users::getCurrent();

        if ($cur_user)
        {
            $user_id = $cur_user->id;

	        $has = $model_m->getFromWatchedEscortsForUser($sess_id, $escort_id, $user_id, date('Y-m-d'));

	        if ($has)
	        {
		        $model_m->updateToWatchedEscortsForUser($sess_id, $escort_id, $user_id);
	        }
	        else
	        {
		        $model_m->addToWatchedEscorts($sess_id, $escort_id, $user_id);
	        }
        }
        else
        {
            $has = $model_m->getFromWatchedEscorts($sess_id, $escort_id);

            if ($has)
            {
                $model_m->updateToWatchedEscorts($sess_id, $escort_id);
            }
            else
            {
                $model_m->addToWatchedEscorts($sess_id, $escort_id);
            }
        }
        /**/

        $this->view->escort_user_id = $model->getUserId($escort->id);

        if ( ! is_null($this->_getParam('ajax')) ) {
            $body = $this->view->render('escorts/profile-v2/profile.phtml');

            $flags = $this->view->render('escorts/flags.phtml');

            die(json_encode(array('body' => $body, 'flags' => $flags)));
        }
    }

    public function agencyEscortsV2Action($agency_id = null, $escort_id = null)
    {
        $config = Zend_Registry::get('escorts_config');

        if ( ! $agency_id )
        {
            $this->view->layout()->disableLayout();
            $agency_id = $this->view->agency_id = $this->_getParam('agency_id');
        }
        else
            $this->view->agency_id = $agency_id;

        if( ! $escort_id )
            $escort_id = $this->view->escort_id = $this->_getParam('escort_id');

        $page = intval($this->_getParam('agency_page'));
        if ( $page < 1 )
            $page = 1;

        $default_size = "s";
        $current_size = $this->_getParam('view_type', $default_size);

        if ( $current_size != $_COOKIE['ae-view-type'] && $this->_request->s ) {
            setcookie("ae-view-type", $current_size, strtotime('+1 year'), "/", "www." . preg_replace('#www.|dev.#', '', $_SERVER['HTTP_HOST']));
            $_COOKIE['ae-view-type'] = $current_size;
        }

        if ( isset($_COOKIE['ae-view-type']) ) {
            $current_size = $_COOKIE['ae-view-type'];            
        }

        $small_per_page = 14;
        $large_per_page = 3;

        $per_page = ($current_size == "l") ? $large_per_page : $small_per_page;

        $params = array('e.agency_id = ?' => (int)$agency_id,'e.id <> ?' => (int)$escort_id/*, 'show_all_agency_escorts' => 1*/);

        $escorts_count = 0;
        $escorts = Model_Escort_List::getAgencyEscorts($params, 'newest', $page, $per_page, $escorts_count);

        if (count($escorts) ) {
            $this->view->a_escorts = $escorts;
            $this->view->a_escorts_count = $escorts_count;

            $this->view->agencies_page = $page;
            $this->view->agencies_per_page = $per_page;
        }

        $m_esc = new Model_EscortsV2();
        $agency = $m_esc->getAgency($escort_id);

        $this->view->agency = new Model_AgencyItem($agency);
        $this->view->is_ajax = $this->_getParam('ajax', false);
        $this->view->size = $current_size;
    }

    public function profileDisabledAction()
    {
        $model = new Model_EscortsV2();
        $count = 0;

        $this->view->no_layout_banners = true;
        $this->view->no_paging = true;
        $this->view->no_js = true;

        $params = array('e.gender = 1' => array(), 'ct.slug = ?' => array($this->_request->city_slug));
        $escorts = Model_Escort_List::getFiltered($params, 'random', 1, 50, $count);

        $this->view->count = $count;
        $this->view->escorts = $escorts;
    }

    public function agencyEscortsAction($agency_id = null, $escort_id = null)
    {
        $config = Zend_Registry::get('escorts_config');

        if ( ! $agency_id )
        {
            $this->view->layout()->disableLayout();
            $agency_id = $this->view->agency_id = $this->_getParam('agency_id');
        }
        else
            $this->view->agency_id = $agency_id;

        if( ! $escort_id )
            $escort_id = $this->view->escort_id = $this->_getParam('escort_id');

        $page = intval($this->_getParam('agency_page'));
        if ( $page < 1 )
            $page = 1;

        $params = array('e.agency_id = ?' => $agency_id,'e.id <> ?' => $escort_id);

        $escorts_count = 0;
        $escorts = Model_Escort_List::getFiltered($params, 'random', $page, $config['widgetPerPage'], $escorts_count);

        if (count($escorts) ) {
            $this->view->a_escorts = $escorts;
            $this->view->a_escorts_count = $escorts_count;

            $this->view->agencies_page = $page;
            $this->view->widgetPerPage = $config['widgetPerPage'];

            $m_esc = new Model_EscortsV2();
            $agency = $m_esc->getAgency($escort_id);

            $this->view->agency = new Model_AgencyItem($agency);
        }
    }

    public function photosV2Action($escort = null)
    {
        $config = Zend_Registry::get('escorts_config');

        $page = $this->_getParam('photo_page');

        $escort_id = $this->_getParam('escort_id');
        $mode = $this->_getParam('mode');

        if( $mode == 'ajax' ) {
            $this->view->layout()->disableLayout();
            $model = new Model_EscortsV2();
            $escort = $model->get($escort_id);
            $this->view->escort_id = $escort_id;
        }

        if ( ! $page )
            $page = 1;

        $count = 0;
        if ( $this->view->is_preview || $this->_getParam('is_preview') ) {
            $photos = $escort->getPhotosApi($page, $count, true, true);
        }
        else {
            $count = 100;
            $photos = $escort->getPhotos($page, $count, true, false, null, null, false, true);
        }
        $this->view->photos = $photos;
        $this->view->photos_count = $count;
        $this->view->photos_page = $page;

        $this->view->photos_perPage = $config['photos']['perPage'];
    }

	public function galleryPhotosAction($escort = null)
	{
		$config = Zend_Registry::get('escorts_config');
		$page = 1;
		$count = 0;
		$galery_id = intval($this->_getParam('gallery_id'));
		
		if($this->_request->isXmlHttpRequest()){
			$this->view->layout()->disableLayout();
			$this->_helper->viewRenderer->setScriptAction("profile-v2/gallery-photos");
			$escort_id = intval($this->_getParam('escort_id'));
			$showname = $this->_getParam('showname');
			$cache_key = 'v2_' . $showname . '_' . $escort_id .  '_profile_' . Cubix_I18n::getLang();
			$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
			$model = new Model_EscortsV2();
			$escort = $model->get($escort_id,$cache_key);
			
			/*if ($escort->showname != $showname)
			{
				header('HTTP/1.1 301 Moved Permanently');
				header('Location: /');
				die;
			}*/

			$city = new Cubix_Geography_Cities();
			$city = $city->get($escort->base_city_id);
			$this->_request->setParam('region', $city->region_slug);
			$this->view->base_city = $city;

			if ( ! $escort || isset($escort['error']) )
			{
				header('HTTP/1.1 301 Moved Permanently');
				header('Location: /');
				die;
				return;
			}

			$blockModel = new Model_BlockedCountries();
			if ( $blockModel->checkIp($escort->id) ){
				header('HTTP/1.1 301 Moved Permanently');
				header('Location: /');
				die;
				return;
			}
			$this->view->escort = $escort;
			$this->view->escort_id = $escort_id;
			
		}
		if($_GET['test'] == 1){
			var_dump($galery_id);die;
		}
		if ( $this->view->is_preview || $this->_getParam('is_preview') ) {
			$photos = $escort->getPhotosApi($page, $count, true, true, $galery_id);
		}
		else {
			$count = 100;
			$photos = $escort->getPhotos($page, $count, true, false, null, null, false, true, $galery_id);
		}
		$this->view->photos = $photos;
		$this->view->photos_count = $count;
		$this->view->photos_page = $page;
		$this->view->photos_perPage = $config['photos']['perPage'];
		
	}
	
    public function photosAction($escort = null)
    {
        $config = Zend_Registry::get('escorts_config');

        $page = $this->_getParam('photo_page');

        $escort_id = $this->_getParam('escort_id');
        $mode = $this->_getParam('mode');

        if( $mode == 'ajax' ) {
            $this->view->layout()->disableLayout();
            $model = new Model_EscortsV2();
            $escort = $model->get($escort_id);
            $this->view->escort_id = $escort_id;
        }

        if ( ! $page )
            $page = 1;

        $count = 0;
        $photos = $escort->getPhotos($page, $count, false);
        $this->view->photos = $photos;
        $this->view->photos_count = $count;
        $this->view->photos_page = $page;

        $this->view->photos_perPage = $config['photos']['perPage'];
    }

    public function privatePhotosAction($escort = null)
    {
        $config = Zend_Registry::get('escorts_config');

        $page = $this->_getParam('photo_page');
        $escort_id = $this->_getParam('escort_id');

        if( $escort_id )
        {
            $this->view->layout()->disableLayout();
            $model = new Model_EscortsV2();
            $escort = $model->get($escort_id);
            $this->view->escort_id = $escort_id;
        }

        if ( ! $page )
            $page = 1;

        $count = 0;
        $this->view->private_photos = $escort->getPhotos($page, $count, false, true);
        $this->view->private_photos_count = $count;
        $this->view->private_photos_page = $page;
        $this->view->private_photos_perPage = $config['photos']['perPage'];
    }

    public function viewedEscortsAction()
    {
        $this->view->layout()->disableLayout();
        $escort_id = intval($this->_getParam('escort_id'));

        if ( $escort_id )
        {
            self::_addToLatestViewedList($escort_id);
            $viewedEscorts = self::_getLatestViewedList(7, $escort_id);
        }
        else
            $viewedEscorts = self::_getLatestViewedList(7);

        if ( ! is_array($viewedEscorts) || count($viewedEscorts) == 0 )
            return false;

        $ve = array();

        $ids = array();

        foreach ( $viewedEscorts as $e ) {
            $ids[] = $e['id'];
        }

        $filter = array('e.id IN (' . implode(', ', $ids) . ')' => array(), 'show_all_agency_escorts' => array(), 'remove_package_query' => array());
        $order = 'FIELD(eic.escort_id, ' . implode(', ', $ids) . ')';

        $count = 0;
        $escorts = Model_Escort_List::getFiltered($filter, $order, 1, 7, $count);
//var_dump($ids);

        $this->view->escorts = $escorts;
        $this->view->count = $count;
    }

    public function lateNightGirlsAction()
    {
        $this->view->layout()->disableLayout();

        $page = intval($this->_request->l_n_g_page);

        if ($page < 1) $page = 1;

        list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls($page);

        $this->view->l_n_g_page = $page;
    }

    private function _getLateNightGirls($page = 1)
    {
        return Model_EscortsV2::getLateNightGirls($page);
    }

    private function _addToLatestViewedList($escort_id)
    {
        $count = 7; // actually we need 6
        $escort = array('id' => $escort_id);
        $sid = 'last_viewed_escorts_' . Cubix_Application::getId();

        $ses = new Zend_Session_Namespace($sid);

        // Checking if session exists
        // creating if not exists
        if ( false == (isset($ses->escorts) && is_array($ses->escorts)) )
        {
            $ses->escorts = array();
        }
        else
        {
            // Checking if escort exists in stack
            // removing escort from stack if exists
            foreach ($ses->escorts as $key => $item)
            {
                if ( $escort['id'] == $item['id'] )
                {
                    array_splice($ses->escorts, $key, 1);
                }
            }
        }

        // Pushing escort to the end of the stack
        array_unshift($ses->escorts, $escort);

        // Checking if
        if ( count($ses->escorts) > $count )
        {
            array_pop($ses->escorts);
        }
        //var_dump($ses->escorts);
    }

    // Getting latest viewed escorts list (stack)
    // as Array of Assoc
    public function _getLatestViewedList($max, $exclude = NULL)
    {
        $sid = 'last_viewed_escorts_' . Cubix_Application::getId();

        $ses = new Zend_Session_Namespace($sid);

        if ( ! is_array($ses->escorts) )
            return array();

        if ( $exclude )
        {
            $arr = $ses->escorts;
            foreach($arr as $key => $item)
            {
                if($exclude == $item['id'])
                {
                    array_splice($arr, $key, 1);
                }
            }
            $ret = array_slice($arr, 0, $max);

            return $arr;
        }

        $ret = array_slice($ses->escorts, 0, $max);

        return $ret;
    }

    public function searchAction()
    {
        $this->view->layout()->setLayout('main-simple');


        $this->view->quick_links = false;

        $this->view->defines = Zend_Registry::get('defines');

        if( $this->_getParam('search') )
        {
            $params['filter'] = array();
            $params['order'] = array();

            $this->view->params = $this->_request->getParams();

            if( $this->_getParam('order') ) {

                $order_direction = '';
                $order_field = 'date_registered';

                switch($this->_getParam('order')){
                    case 'lastmodified':
                        $order_field = "date_last_modified";
                        break;
                    case 'popularity':
                        $order_field = "hit_count";
                        break;
                }

                if($this->_getParam('order_direction')){
                    $order_direction = $this->_getParam('order_direction');
                }
                $params['order'] = $order_field." ".$order_direction;

            }


            if($this->_getParam('page')){
                $params['limit']['page'] = $this->_getParam('page');
            }

            if( $this->_getParam('publish') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'DATEDIFF(NOW(),e.date_registered) <= ?' =>  trim($this->_getParam('days'))
                ));
            }

            if( $this->_getParam('showname') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.showname LIKE ?' => "%" . trim($this->_getParam('showname')) . "%"
                ));
            }

            if( $this->_getParam('agency_slug') && $this->_getParam('agency') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.agency_slug LIKE ?' => "%" . trim($this->_getParam('agency_slug')) . "%"
                ));
            }

            if( $this->_getParam('gender') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.gender = ?' => $this->_getParam('gender')
                ));
            }

            if ( $this->_getParam('country_id') && $this->_getParam('country_id') ) {
                $params['filter'][] = '(e.country_id= ' . $this->_getParam('country_id') . ')'; //. ' OR ' . ' etc.continent_id= ' . Model_Countries::getContinentId($this->_getParam('country_id'))
            }

            if ( $this->_getParam('city_id') && $this->_getParam('city_id') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'ct.id = ?' => $this->_getParam('city_id')
                ));
            }
            /*if( $this->_getParam('city') &&  $this->_getParam('city_slug') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'cc.slug = ?' => $this->_getParam('city_slug')
                ));
            }*/

            if( $this->_getParam('phone') ) {
                $phone = preg_replace('/^\+/', '00',$this->_getParam('phone'));
                $phone = preg_replace('/[^0-9]+/', '', $phone);
                $params['filter'] = array_merge($params['filter'], array(
                    'e.contact_phone_parsed LIKE ?' => "%".$phone."%"
                ));
            }

            if( $this->_getParam('available_for_incall') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.incall_type IS NOT NULL' => true
                ));
            }
            if( $this->_getParam('available_for_outcall') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.outcall_type IS NOT NULL' => true
                ));
            }

            if( $this->_getParam('nationality') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.nationality_id = ?' => $this->_getParam('nationality')
                ));
            }

            if( $this->_getParam('saf') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'FIND_IN_SET( ?, e.sex_availability)' => $this->_getParam('saf')
                ));
            }

            if( $this->_getParam('price_from') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.outcall_price >= ?' => $this->_getParam('price_from')
                ));
            }

            if( $this->_getParam('price_to') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.outcall_price <= ?' => $this->_getParam('price_to')
                ));
            }

            if( $this->_getParam('words') )
            {
                if ($this->_getParam('words_type') == 1)
                {
                    if ($pos = strpos($this->_getParam('words'), ' ')) {
                        $sub = substr($this->_getParam('words'), 0, $pos);
                    }
                    else {
                        $sub = $this->_getParam('words');
                    }

                    $params['filter'] = array_merge($params['filter'], array(
                        'e.' . Cubix_I18n::getTblField('about') . ' = ?' =>  $sub
                    ));
                }
                elseif ($this->_getParam('words_type') == 2)
                {
                    $params['filter'] = array_merge($params['filter'], array(
                        'e.' . Cubix_I18n::getTblField('about') . ' LIKE ?' => "%" . $this->_getParam('words') . "%"
                    ));
                }
            }

            /* ------------ >>>>>>>>>>>>>>>>>>>>>>>>>>>>>> <<<<<<<<<<<<<<<<<<<<<< --------------- */

            if( $this->_getParam('age_from') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.age >= ?' => $this->_getParam('age_from')
                ));
            }
            if( $this->_getParam('age_to') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.age <= ?' => $this->_getParam('age_to')
                ));
            }

            if( $this->_getParam('ethnicity') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.ethnicity = ?' => $this->_getParam('ethnicity')
                ));
            }

            if( $this->_getParam('hair_color') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.hair_color = ?' => $this->_getParam('hair_color')
                ));
            }

	        if( $this->_getParam('hair_length') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.hair_length = ?' => $this->_getParam('hair_length')
                ));
            }

            if( $this->_getParam('eye_color') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.eye_color = ?' => $this->_getParam('eye_color')
                ));
            }

            if( $this->_getParam('height_from') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.height >= ?' => $this->_getParam('height_from')
                ));
            }
            if( $this->_getParam('height_to') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.height <= ?' => $this->_getParam('height_to')
                ));
            }

            if( $this->_getParam('dress_size') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.dress_size = ?' => $this->_getParam('dress_size')
                ));
            }

            if( $this->_getParam('shoe_size') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.shoe_size = ?' => $this->_getParam('shoe_size')
                ));
            }

            if( $this->_getParam('cup_size') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.cup_size = ?' => $this->_getParam('cup_size')
                ));
            }

            if( $this->_getParam('pubic_hair') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.pubic_hair = ?' => $this->_getParam('pubic_hair')
                ));
            }

             if( $this->_getParam('smoker') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.is_smoking = ?' => $this->_getParam('smoker')
				));
			}

			if( $this->_getParam('drinking') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.is_drinking = ?' => $this->_getParam('drinking')
				));
			}			
            if( $this->_getParam('language') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.languages LIKE ?' => "%".$this->_getParam('language')."%"
                ));
            }
            /* Services */
            if( $this->_getParam('services') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'es.service_id = ?' => $this->_getParam('services')
                ));
            }
            /* Services */
            /* Working Times */
            if ( $this->_getParam('travel_date') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'DATE(sch.day) = ?' => date('Y-m-d', $this->_getParam('travel_date')),
                    'sch.type = ?'	=> Model_Schedule::AVAILABLE_DAY
                ));
            }

            if( $this->_getParam('day_index') ) {
                $dateTimes = array();
                $counter = 0;
                foreach($this->_getParam('day_index') as $day => $value){
                    $timeFrom = $this->_getParam('time_from');
                    $timeFromM = $this->_getParam('time_from_m');
                    $timeTo = $this->_getParam('time_to');
                    $timeToM = $this->_getParam('time_to_m');
                    $dateTimes[$counter]['day_index'] = $day;
                    $dateTimes[$counter]['time_from'] = $timeFrom[$day];
                    $dateTimes[$counter]['time_from_m'] = $timeFromM[$day];
                    $dateTimes[$counter]['time_to'] = $timeTo[$day];
                    $dateTimes[$counter]['time_to_m'] = $timeToM[$day];
                    $counter++;
                }
                $params['filter'] = array_merge($params['filter'], array(
                    'working_times = ?' => $dateTimes
                ));
            }

            /* Working Times */

            $page = 1;
            $page_size = 54;
            if($this->_getParam('page')){
                $page = $this->_getParam('page');
            }

            $model = new Model_EscortsV2();
            $count = 0;

            $escorts = $model->getSearchAll($params, $count,$page,$page_size);
            $x['times'] = $dateTimes;

            $this->view->data = $x;
            $this->view->page = $page;
            $this->view->count = $escorts['count'];
            $this->view->escorts = $escorts['data'];
            $this->view->search_list = true;
        }
    }

    public function gotmAction()
    {
        $this->view->layout()->setLayout('main-simple');
        $show_history = (bool) $this->_getParam('history');



        $page = intval($this->_getParam('page', 1));
        if ( $page < 1 ) $page = 1;
        $this->view->per_page = $per_page = 40;
        $this->view->page = $page;

        $result = Model_EscortsV2::getMonthGirls($page, $per_page, $show_history);



        $this->view->escorts = $escorts = $result['escorts'];
        $this->view->count = $result['count'];

        if ( ! $show_history ) {
            $this->view->gotm = Model_EscortsV2::getCurrentGOTM();
        }
        else {
            $this->_helper->viewRenderer->setScriptAction('gotm-history');
        }
    }

    /**
     * Voting Widget Action
     */
    public function voteAction()
    {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $response = array('status' => null, 'desc' => '', 'html' => '');

        $showname = $this->_getParam('showname');
        $escort_id = $this->_getParam('escort_id');
        $model = new Model_EscortsV2();
        //$escort = $model->get($showname);
        $escort = $model->get($escort_id);

        $user = Model_Users::getCurrent();

        if ( ! $user ) {
            $response['status'] = 'error';
            $response['desc'] = 'not signed in';
        }
        elseif ( 'member' != $user->user_type ) {
            $response['status'] = 'error';
            $response['desc'] = 'only members are allowed to vote';
        }
        elseif ( ! $escort ) {
            $response['status'] = 'error';
            $response['desc'] = 'invalid escort showname';
        }
        else {
            try {
                $result = $escort->vote($user->id);

                if ( $result !== true ) {
                    $response['status'] = 'error';
                    $response['desc'] = 'member has already voted';
                }
                else {
                    $response['status'] = 'success';
                    $response['html'] = $this->view->votingWidget($escort, true);
                }
            }
            catch ( Exception $e ) {
                $response['status'] = 'error';
                $response['desc'] = 'unexpected error';
            }
        }

        echo json_encode($response);
    }

    public function bookMeAction()
    {
        $this->view->layout()->disableLayout();

        $escort_id = $this->view->escort_id = $this->_request->escort_id;
        $showname = $this->view->showname = $this->_request->showname;

        $model = new Model_EscortsV2();

        $cache_key = 'v2_' . $showname . '_profile_' . Cubix_I18n::getLang();
        $cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

        $this->view->escort = $model->get($showname, $cache_key);

        $errors = array();

        if ( $this->_request->isPost() ) {
            $name = $this->view->name = $this->_request->name;
            $surname = $this->view->surname = $this->_request->surname;
            $address = $this->view->address = $this->_request->address;
            $city = $this->view->city = $this->_request->city;
            $state = $this->view->state = $this->_request->state;
            $country = $this->view->country = intval($this->_request->country);
            $zip = $this->view->zip = $this->_request->zip;
            $phone = $this->view->phone = $this->_request->phone;
            $email = $this->view->email = $this->_request->email;
            $request_date = $this->view->request_date = $this->_request->request_date;
            $duration = $this->view->duration = $this->_request->duration;
            $duration_unit = $this->view->duration_unit = $this->_request->duration_unit;
            $message = $this->view->message = $this->_request->message;

            $contact = $this->view->contact = $this->_request->contact;
            $time = $this->view->time = $this->_request->time;

            if ( ! $name ) {
                $errors[] = 'name';
            }
            /*if ( ! $surname ) {
                $errors[] = 'surname';
            }
            if ( ! $address ) {
                $errors[] = 'address';
            }
            if ( ! $city ) {
                $errors[] = 'city';
            }
            if ( ! $state ) {
                $errors[] = 'state';
            }*/
            if ( ! $country ) {
                $errors[] = 'country';
            }
            /*if ( ! $zip ) {
                $errors[] = 'zip';
            }
            if ( ! $phone ) {
                $errors[] = 'phone';
            }
            if ( ! $email ) {
                $errors[] = 'email';
            }
            if ( ! $request_date ) {
                $errors[] = 'request_date';
            }
            if ( ! $duration ) {
                $errors[] = 'duration';
            }*/

            if ( ! $phone && ! $email ) {
                if ( ! $phone ) {
                    $errors[] = 'phone';
                }
                if ( ! $email ) {
                    $errors[] = 'email';
                }
            }

            if ( count($contact) == 0 ) {
                //$errors[] = 'contact';
                $this->view->contact = array();
            }
            if ( count($time) == 0 ) {
                //$errors[] = 'time';
                $this->view->time = array();
            }

            // Save data
            if ( count($errors) == 0 ) {
                $data = array(
                    'escort_id' => $escort_id,
                    'name' => $name,
                    'surname' => $surname,
                    'address' => $address,
                    'city' => $city,
                    'state' => $state,
                    'country_id' => $country,
                    'zip' => $zip,
                    'phone' => $phone,
                    'email' => $email,
                    'request_date' => $request_date ? $request_date : null,
                    'duration' => $duration,
                    'duration_unit' => $duration_unit,
                    'message' => $message,
                    'contact' => $contact,
                    'preferred_time' => $time,
                    'application_id' => Cubix_Application::getId()
                );

                Cubix_Api::getInstance()->call('bookEscort', array($data));

                Cubix_Email::sendTemplate('booking_request', $email, array(
                    'username' => $name
                ));

                $this->view->success = true;
            }
        }
        else {
            $this->view->contact = array();
            $this->view->time = array();
        }
        $this->view->errors = $errors;
    }

    public function ajaxTellFriendAction()
    {
        $this->view->layout()->disableLayout();
        $this->view->full_url = $url = $_SERVER['HTTP_REFERER'];
        if($this->_request->isPost()){

            // Get data from request
            $data = new Cubix_Form_Data($this->getRequest());

            $fields = array(
                'name' => 'notags|special',
                'email' => '',
                'friend_name' => 'notags|special',
                'friend_email' => '',
                'message' => 'notags|special',

            );
            $data->setFields($fields);
            $data = $data->getData();

            $validator = new Cubix_Validator();

            if ( ! strlen($data['name']) ) {
                $validator->setError('name', 'Your Name is required');
            }

            if ( ! strlen($data['email']) ) {
                $validator->setError('email', 'Your Email is required');
            }
            elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
                $validator->setError('email', 'Wrong email format');
            }
            if ( ! strlen($data['friend_name']) ) {
                $validator->setError('friend_name', 'Friend Name is required');
            }

            if ( ! strlen($data['friend_email']) ) {
                $validator->setError('friend_email', 'Friend Email is required');
            }
            elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['friend_email']) ) {
                $validator->setError('friend_email', 'Wrong email format');
            }

            $result = $validator->getStatus();

            if ( $validator->isValid() ) {

                // Set the template parameters and send it to support
                $email_tpl = 'send_to_friend';
                $tpl_data = array(
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'friend_name' => $data['friend_name'],
                    'friend_email' => $data['friend_email'],
                    'message' => $data['message'],
                    'application_id' => Cubix_Application::getId(),
                    'url' => $url
                );
                $data['application_id'] = Cubix_Application::getId();

                Cubix_Email::sendTemplate($email_tpl, $data['friend_email'], $tpl_data, $data['email']);

                $config = Zend_Registry::get('newsman_config');

                $list_id = $config['list_id'];
                $segment = $config['member'];
                $nm = new Cubix_Newsman($_SERVER["REMOTE_ADDR"]);

                $subscriber_id = $nm->subscribe($list_id, $data['email'],$data['name']);
                $is_added = $nm->bindToSegment($subscriber_id, $segment);
                $subscriber_id_friend = $nm->subscribe($list_id, $data['friend_email'],$data['friend_name']);
                $is_added = $nm->bindToSegment($subscriber_id_friend, $segment);

                die(json_encode($result));
            }	// Otherwise, render the phtml and show errors in it
            else {
                die(json_encode($result));

            }
        }
    }

    public function ajaxSuspPhotoAction()
    {
        $this->view->layout()->disableLayout();

        if ($this->_request->isPost())
        {
            // Get data from request
            $data = new Cubix_Form_Data($this->getRequest());

            $fields = array(
                'escort_id' => 'int',
                'link_1' => '',
                'link_2' => '',
                'link_3' => '',
                'comment' => 'notags|special'
            );

            $data->setFields($fields);
            $data = $data->getData();

            $validator = new Cubix_Validator();

            if (!strlen($data['link_1']))
            {
                $validator->setError('link_1', $this->view->t('photo_link_required'));
            }
            elseif (!$validator->isValidURL($data['link_1']))
            {
                $validator->setError('link_1', $this->view->t('photo_link_not_valid'));
            }

            if (strlen($data['link_2']) && !$validator->isValidURL($data['link_2']))
            {
                $validator->setError('link_2', $this->view->t('photo_link_not_valid'));
            }

            if (strlen($data['link_3']) && !$validator->isValidURL($data['link_3']))
            {
                $validator->setError('link_3', $this->view->t('photo_link_not_valid'));
            }

            if (! strlen($data['comment']))
            {
                $validator->setError('comment', $this->view->t('comment_required'));
            }

            $result = $validator->getStatus();

            if ($validator->isValid())
            {
                $data['application_id'] = Cubix_Application::getId();
                $data['status'] = SUSPICIOUS_PHOTOS_REQUEST_PENDING;

                $user = Model_Users::getCurrent();
                $user_id = null;

                if ($user)
                    $user_id = $user->id;

                $data['user_id'] = $user_id;

                $client = new Cubix_Api_XmlRpc_Client();
                $client->call('Users.addSuspPhoto', array($data));
            }

            die(json_encode($result));
        }
    }

    public function ajaxReportProblemAction()
    {
        $this->view->layout()->disableLayout();
        $url = $_SERVER['HTTP_REFERER'];
        if($this->_request->isPost()){
            // Fetch administrative emails from config
            $config = Zend_Registry::get('feedback_config');
            $support_email = $config['emails']['support'];
            $email_tpl = 'report_problem';
            // Get data from request
            $data = new Cubix_Form_Data($this->getRequest());

            $fields = array(
                'escort_id' => 'int',
                'name' => 'notags|special',
                'email' => '',
                'message' => 'notags|special',

            );
            $data->setFields($fields);
            $data = $data->getData();

            $validator = new Cubix_Validator();

            if ( ! strlen($data['name']) ) {
                $validator->setError('name', 'Your Name is required');
            }

            if ( ! strlen($data['email']) ) {
                $validator->setError('email', 'Your Email is required');
            }
            elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
                $validator->setError('email', 'Wrong email format');
            }
            if ( ! strlen($data['message']) ) {
                $validator->setError('message','Please type the report you want to send');
            }

            $result = $validator->getStatus();

            if ( $validator->isValid() ) {

                // Set the template parameters and send it to support
                $tpl_data = array(
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'message' => $data['message'],
                    'application_id' => Cubix_Application::getId(),
                    'url' => $url
                );
                $data['application_id'] = Cubix_Application::getId();

                try{
                    Cubix_Email::sendTemplate($email_tpl, $support_email, $tpl_data, null);

                    $user_id = null;
                    $user = Model_Users::getCurrent();

                    if ($user && $user->user_type == 'member')
                        $user_id = $user->id;

                    $client = new Cubix_Api_XmlRpc_Client();
                    $client->call('Users.addProblemReport', array($data['name'], $data['email'], null, $data['message'], $data['escort_id'], $user_id));

                    $nm = new Cubix_Newsman($_SERVER["REMOTE_ADDR"]);
                    $subscriber_id = $nm->subscribe($list_id, $data['email'],$data['name']);
                    $is_added = $nm->bindToSegment($subscriber_id, $segment);

                }catch ( Exception $e ) {
                    die($e->getMessage());
                }
                die(json_encode($result));
            }	// Otherwise, render the phtml and show errors in it
            else {
                die(json_encode($result));

            }

        }
    }

    public function getFilterAction()
    {
        $params = $this->_getParam('json');
        $this->view->layout()->disableLayout();
        $menus = array('filter' => NULL, 'sort' => NULL);

        $defs = Zend_Registry::get('definitions');

        $filterStructure = $defs['escorts_filter'];

        $menus['filter'] = new Cubix_NestedMenu(array('childs' => $filterStructure));
        $menus['filter']->setId('filter-options');
        $menus['filter']->setSelected($menus['filter']->getByValue(NULL));

        $menus['sort'] = new Cubix_NestedMenu(array('childs' => $defs['escorts_sort']));
        $menus['sort']->setId('sort-options');
        $menus['sort']->setSelected($menus['sort']->getByValue('random'));
        $this->view->menus = $menus;
        $this->view->params = $params;

    }

    public function ajaxGetCitiesSearchAction()
    {
        $all_cities = Model_Statistics::getCities(null, null, null, null, null, null, null, $this->_request->getParam('city_search_name', null));

        $fn_order_title = create_function('$a, $b', '
			if ( is_object($a) && is_object($b) ) {
				return strnatcmp($a->city_title, $b->city_title);
			}
			elseif ( is_array($a) && is_array($b) ) {
				return strnatcmp($a["city_title"], $b["city_title"]);
			}
			else return 0;
		');

        usort($all_cities, $fn_order_title);
        foreach ( $all_cities as $k => $city ) {
            $all_cities[$k] = array(
                'type' => 'city',
                'group' => $this->view->t('cities'),
                'slug' => $city->city_slug,
                'name' => $city->city_title . ' (' . $city->country_title . ')'
            );
        }

        die(json_encode($all_cities));
    }

    public function ajaxGetAgenciesSearchAction()
    {
        $all_agencies = Model_EscortsV2::getAllAgencies($this->_request->getParam('agency_search_name', null));

        foreach ( $all_agencies as $k => $agency ) {
            $all_agencies[$k] = array(
                'type' => 'agency',
                'group' => $this->view->t('agency'),
                'slug' => $agency->agency_slug,
                'name' => $agency->agency_name
            );
        }

        die(json_encode($all_agencies));
    }

    public function ajaxGetTopSearchAction()
    {
        $count = 0;

        $filter_params['filter'] = array('e.showname LIKE ?' => $this->_request->getParam('top_search_name', null) . '%', 'show_all_agency_escorts' => true);

        $all_escorts = Model_Escort_List::getFiltered($filter_params['filter'], 'alpha', 1, 20, $count, 'regular_list', false);

        $_all_escorts = array();

        if ( $count ) {
            $_all_escorts[0] = array(
                'type' => 'escort',
                'method' => 'see-all',
                'group' => $this->view->t('escorts'),
                'slug' => $this->_request->getParam('top_search_name', null),
                'name' => $this->_request->getParam('top_search_name', null) . ' (' . $count . ' / ' . Cubix_I18n::translate('see_all') . ')'
            );
        }

        foreach ( $all_escorts as $k => $escort ) {

            $show_name = $escort->showname . ' (' . $escort->city . ' / ';

            if ( $escort->agency_id ) {
                $show_name .= Cubix_I18n::translate('agency') . ' ' . $escort->agency_name . ')';
            } else {
                $show_name .= Cubix_I18n::translate('independent') . ')';
            }

            $_all_escorts[$k+1] = array(
                'type' => 'escort',
                'group' => $this->view->t('escorts'),
                'escort_id' => $escort->id,
                'slug' => $escort->showname,
                'name' => $show_name,
            );
        }

        $all_cities = Model_Statistics::getCities(null, null, null, null, null, null, 5, $this->_request->getParam('top_search_name', null));


        $fn_order_title = create_function('$a, $b', '
			if ( is_object($a) && is_object($b) ) {
				return strnatcmp($a->city_title, $b->city_title);
			}
			elseif ( is_array($a) && is_array($b) ) {
				return strnatcmp($a["city_title"], $b["city_title"]);
			}
			else return 0;
		');

        usort($all_cities, $fn_order_title);
        foreach ( $all_cities as $k => $city ) {
            $all_cities[$k] = array(
                'type' => 'city',
                'group' => $this->view->t('cities'),
                'slug' => $city->city_slug,
                'name' => $city->city_title . ' (' . $city->escort_count . ')' . ' (' . $city->country_title . ')'
            );
        }

        $all_countries = Model_Statistics::getCountries(null, null, null, null, 5, $this->_request->getParam('top_search_name', null));

        $fn_order_title = create_function('$a, $b', '
			if ( is_object($a) && is_object($b) ) {
				return strnatcmp($a->country_title, $b->country_title);
			}
			elseif ( is_array($a) && is_array($b) ) {
				return strnatcmp($a["country_title"], $b["country_title"]);
			}
			else return 0;
		');

        usort($all_countries, $fn_order_title);
        foreach ( $all_countries as $k => $country ) {
            $all_countries[$k] = array(
                'type' => 'country',
                'group' => $this->view->t('countries'),
                'slug' => $country->country_slug,
                'name' => $country->country_title . ' (' . $country->escort_count . ')'
            );
        }

        $merged = array_merge($_all_escorts, $all_countries, $all_cities);

        die(json_encode($merged));
    }
}

