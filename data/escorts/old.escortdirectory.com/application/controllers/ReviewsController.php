<?php

class ReviewsController extends Zend_Controller_Action
{
	private $model;
	private $user;

	public function init()
	{
		$this->model = new Model_Reviews();
		$this->user = Model_Users::getCurrent();
	}

	public function indexAction()
	{
		$request = $this->_request;
		$this->view->view_type = 'd';
		$this->view->ord_dir = $ord_dir = 'desc';
		$this->view->ord_field = $ord_field = 'add';
		$this->view->page = $page = 1;

		$config = Zend_Registry::get('reviews_config');
		$this->view->per_page = $per_page = $config['perPage'];

		$filter = array();

		if ($request->mode == 'exact')
		{
			$filter['mode'] = $this->view->mode = 'exact';
			$filter['member'] = $this->view->member = $request->member;
		}

		switch ($ord_field)
		{
			case 'meet':
				$ord_field_v = 'r.meeting_date';
				break;
			case 'add':
			default:
				$ord_field_v = 'r.creation_date';
				break;
		}

		switch ($ord_dir)
		{
			case 'asc':
				$ord_dir_v = 'ASC';
				break;
			case 'desc':
			default:
				$ord_dir_v = 'DESC';
				break;
		}

		$ret_revs = $this->model->getReviews($page, $per_page, $filter, $ord_field_v, $ord_dir_v);

		foreach ( $ret_revs[0] as $i => $rev )
		{
			$photo_url = new Model_Escort_PhotoItem($rev);
			$ret_revs[0][$i]->photo_url = $photo_url->getUrl('lcthumb');
		}

		list($items, $count) = $ret_revs;
		$this->view->items = $items;
		$this->view->count = $count;
	}

	public function ajaxListAction()
	{
		$this->view->layout()->disableLayout();
		$request = $this->_request;

		$ord_field = $request->ord_field;
		if ($ord_field != 'meet') $ord_field = 'add';
		$this->view->ord_field = $ord_field;

		$ord_dir = $request->ord_dir;
		if ($ord_dir != 'asc') $ord_dir = 'desc';
		$this->view->ord_dir = $ord_dir;

		$page = intval($request->page);
		if ($page < 1) $page = 1;
		$this->view->page = $page;

		$config = Zend_Registry::get('reviews_config');
		$this->view->per_page = $per_page = $config['perPage'];

		$filter = array();

		$filter['country_id'] = intval($request->country_id);
		$filter['city_id'] = intval($request->city_id);
		$filter['showname'] = $request->showname;
		$filter['member'] = $request->member;
		$filter['meeting_date_f'] = intval($request->meeting_date_f);
		$filter['meeting_date_t'] = intval($request->meeting_date_t);
		$filter['date_added_f'] = intval($request->date_added_f);
		$filter['date_added_t'] = intval($request->date_added_t);
		$filter['gender_combined'] = intval($request->gender_combined);

		if ($request->gender)
		{
			if (is_array($request->gender))
				$filter['gender'] = implode(',', $request->gender);
			else
				$filter['gender'] = $request->gender;
		}
		else
			$filter['gender'] = null;


		switch ($ord_field)
		{
			case 'meet':
				$ord_field_v = 'r.meeting_date';
				break;
			case 'add':
			default:
				$ord_field_v = 'r.creation_date';
				break;
		}

		switch ($ord_dir)
		{
			case 'asc':
				$ord_dir_v = 'ASC';
				break;
			case 'desc':
			default:
				$ord_dir_v = 'DESC';
				break;
		}

		$ret_revs = $this->model->getReviews($page, $per_page, $filter, $ord_field_v, $ord_dir_v);

		foreach ( $ret_revs[0] as $i => $rev )
		{
			$photo_url = new Model_Escort_PhotoItem($rev);
			$ret_revs[0][$i]->photo_url = $photo_url->getUrl('lcthumb');
		}

		list($items, $count) = $ret_revs;
		$this->view->items = $items;
		$this->view->count = $count;

		if ($request->view_type == 'l')
			$this->_helper->viewRenderer->setScriptAction('list-view');
		else
			$this->_helper->viewRenderer->setScriptAction('detailed-view');
	}

	public function getRevComSetAction()
	{
		$this->view->layout()->disableLayout();

		if (!$this->user || $this->user->user_type == 'member')
			die;

		$this->view->user = $this->user;

		if ($this->user->isAgency())
		{
			$m_pl = new Model_Planner();
			$ag_id = $this->user->getAgency()->id;
			$this->view->escorts = $m_pl->getAgencyEscorts($ag_id);

			$this->view->dis_rev = 1;
			$this->view->dis_com = 1;
		}
		else
		{
			$e_id = $this->user->getEscort()->id;
			$ret = $this->model->allowRevCom($e_id);

			$this->view->dis_rev = $ret['disabled_reviews'];
			$this->view->dis_com = $ret['disabled_comments'];
		}
	}

	public function getRevComSetAgencyAction()
	{
		$this->view->layout()->disableLayout();

		if (!$this->user || $this->user->user_type == 'member' || $this->user->user_type == 'escort')
			die;

		$e_id = intval($this->_request->e_id);
		$ag_id = $this->user->getAgency()->id;

		if ($e_id)
		{
			$m_pl = new Model_Planner();
			$owner = $m_pl->isOwnerAgency($e_id, $ag_id);

			if ($owner)
			{
				$ret = $this->model->allowRevCom($e_id);

				$dis_rev = $ret['disabled_reviews'];
				$dis_com = $ret['disabled_comments'];

				echo json_encode(array('dis_rev' => $dis_rev, 'dis_com' => $dis_com));
			}
		}

		die;
	}

	public function setRevComSetAction()
	{
		$this->view->layout()->disableLayout();

		if ($this->_request->isPost())
		{
			if (!$this->user || $this->user->user_type == 'member')
				die;

			$act = $this->_request->act;

			if (!in_array($act, array('r', 'c')))
				die;

			$val = $this->_request->val;

			if (!in_array($val, array(0, 1)))
				die;

			if ($this->user->isAgency())
			{
				$e_id = intval($this->_request->e_id);
				$ag_id = $this->user->getAgency()->id;

				if ($e_id)
				{
					$m_pl = new Model_Planner();
					$owner = $m_pl->isOwnerAgency($e_id, $ag_id);

					if ($owner)
					{
						$this->model->setAllowRevCom($e_id, $act, $val);
					}
				}
			}
			else
			{
				$e_id = $this->user->getEscort()->id;
				$this->model->setAllowRevCom($e_id, $act, $val);
			}
		}

		die;
	}

	public function addReviewAction()
	{
		if ($this->user->user_type != 'member')
		{
			$this->_redirect($this->view->getLink('dashboard'));
			return;
		}

		$escort_id = $this->view->escort_id = intval($this->_request->escort_id);

		if (!$escort_id)
		{
			$this->_response->setRawHeader('HTTP/1.1 404 Not Found');
			$this->_forward('error', 'error', null, array('error_msg' => 'Wrong URL.'));
			return;
		}

		$dis_rev = $this->model->allowRevCom($escort_id);
		$dis_rev = $dis_rev['disabled_reviews'];

		if ($dis_rev)
		{
			$this->_redirect($this->view->getLink());
			return;
		}

		$showname = $this->model->getEscortShowname($escort_id);

		if (!$showname)
		{
			$this->_forward('error', 'error', null, array('error_msg' => 'Wrong URL.'));
			return;
		}

		$this->view->showname = $showname;
		$this->view->username = $this->user->username;

		if ($this->_request->isPost())
		{
			$validator = new Cubix_Validator();
			$req = $this->_request;

			if (intval($req->m_date) == 0)
				$validator->setError('m_date', 'Required');

			if (!$req->country_id)
				$validator->setError('country_id', 'Required');

			if (!$req->city_id)
				$validator->setError('city_id', 'Required');

			if (!$req->meeting_place)
				$validator->setError('meeting_place', 'Required');

			if (!trim($req->duration))
				$validator->setError('duration', 'Required');
			elseif (!is_numeric($req->duration))
				$validator->setError('duration', $this->view->t('must_be_numeric'));

			if (!$req->duration_unit)
				$validator->setError('duration_unit', 'Required');

			if (!trim($req->price))
				$validator->setError('price', 'Required');
			elseif (!is_numeric($req->price))
				$validator->setError('price', $this->view->t('must_be_numeric'));

			if (!$req->currency)
				$validator->setError('currency', 'Required');

			if ($req->looks_rate == '-1')
				$validator->setError('looks_rate', 'Required');

			if ($req->services_rate == '-1')
				$validator->setError('services_rate', 'Required');

			if (!$req->s_kissing)
				$validator->setError('s_kissing', 'Required');

			if (!$req->s_blowjob)
				$validator->setError('s_blowjob', 'Required');

			if (!$req->s_cumshot)
				$validator->setError('s_cumshot', 'Required');

			if (!$req->s_69)
				$validator->setError('s_69', 'Required');

			if (!$req->s_anal)
				$validator->setError('s_anal', 'Required');

			if (!$req->s_sex)
				$validator->setError('s_sex', 'Required');

			if (!$req->s_multiple_times_sex)
				$validator->setError('s_multiple_times_sex', 'Required');

			if (!$req->s_breast)
				$validator->setError('s_breast', 'Required');

			if (!$req->s_attitude)
				$validator->setError('s_attitude', 'Required');

			if (!$req->s_conversation)
				$validator->setError('s_conversation', 'Required');

			if (!$req->s_availability)
				$validator->setError('s_availability', 'Required');

			if (!$req->s_photos)
				$validator->setError('s_photos', 'Required');

			if (!trim($req->t_user_info))
				$validator->setError('t_user_info', 'Required');

			if (!trim($req->t_meeting_date))
				$validator->setError('t_meeting_date', 'Required');

			if ($req->hrs == '-1')
				$validator->setError('hrs', 'Required');

			if ($req->min == '-1')
				$validator->setError('min', 'Required');

			if (!trim($req->t_meeting_duration))
				$validator->setError('t_meeting_duration', 'Required');

			if (!trim($req->t_meeting_place))
				$validator->setError('t_meeting_place', 'Required');

			if (!trim($req->t_comments))
				$validator->setError('t_comments', 'Required');

			$captcha = trim($req->captcha);

			if ( ! strlen($captcha ) ) {
				$validator->setError('captcha', 'Required');
			}
			else {
				$session = new Zend_Session_Namespace('captcha');
				$orig_captcha = $session->captcha;

				if ( strtolower($captcha) != $orig_captcha ) {
					$validator->setError('captcha', 'Captcha is invalid');
				}
				else
				{
					$session->unsetAll();
				}
			}

			$t_user_info = trim($req->t_user_info);
			$t_meeting_date = trim($req->t_meeting_date);
			$t_meeting_time = $req->hrs . ':' . $req->min;
			$t_meeting_duration = trim($req->t_meeting_duration);
			$t_meeting_place = trim($req->t_meeting_place);

			if ($validator->isValid())
			{
				if ($_SERVER["HTTP_X_FORWARDED_FOR"] != "")
				{
					// for proxy
					$ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
				}
				else
				{
					// for normal user
					$ip = $_SERVER["REMOTE_ADDR"];
				}

				$req->setParam('ip', $ip);
				$req->setParam('user_id', $this->user->id);
				$req->setParam('meeting_city', 'other');
				$req->setParam('m_date', intval($req->m_date));

				list($sms_unique, $phone_to, $mem_susp) = Cubix_Api::getInstance()->call('addReview', array($this->getRequest()->getParams()));
				Cubix_Api::getInstance()->call('SyncNotifier', array($escort_id, 29 ,array('escort_id' => $escort_id) ));

				if (strlen(trim($phone_to)) > 0 && !$mem_susp)
				{
					$text = Cubix_I18n::translate('review_sms_template', array(
						'user_info' => $t_user_info,
						'showname' => $showname,
						'date' => $t_meeting_date,
						'meeting_place' => $t_meeting_place,
						'duration' => $t_meeting_duration,
						'time' => $t_meeting_time,
						'unique_number' => $sms_unique
					));

					$originator = Cubix_Application::getPhoneNumber(Cubix_Application::getId());

					$phone_from = $originator;
					$sms_id = Cubix_Api::getInstance()->call('outbox', array($escort_id, $phone_to, $phone_from, $text, Cubix_Application::getId()));

					//sms info
					$config = Zend_Registry::get('system_config');
					$sms_config = $config['sms'];

					$SMS_USERKEY = $sms_config['userkey'];
					$SMS_PASSWORD = $sms_config['password'];
					$SMS_DeliveryNotificationURL = $sms_config['DeliveryNotificationURL'];
					$SMS_NonDeliveryNotificationURL = $sms_config['NonDeliveryNotificationURL'];
					$SMS_BufferedNotificationURL = $sms_config['BufferedNotificationURL'];

					$sms = new Cubix_SMS($SMS_USERKEY, $SMS_PASSWORD);

					$sms->setOriginator($originator);
					$sms->addRecipient($phone_to, (string)$sms_id);
					$sms->setBufferedNotificationURL($SMS_BufferedNotificationURL);
					$sms->setDeliveryNotificationURL($SMS_DeliveryNotificationURL);
					$sms->setNonDeliveryNotificationURL($SMS_NonDeliveryNotificationURL);

					$sms->setContent($text);

					if (false/*1 != $result = $sms->sendSMS()*/)
					{

					}
					else
					{
						//Cubix_Api::getInstance()->call('smsSent', array($sms_unique));
					}
				}

				$this->_redirect($this->view->getLink('dashboard'));
			}
			else
			{
				$status = $validator->getStatus();
				$this->view->errors = $status['msgs'];
				$this->view->review = $req;
			}
		}
	}

	public function myReviewsAction()
	{
		$this->view->layout()->disableLayout();

		if (!$this->user)
		{
			die;
		}

		$this->view->user = $this->user;
		$page = intval($this->_request->page);
		if ($page < 1) $page = 1;
		$this->view->page = $page;

		$config = Zend_Registry::get('reviews_config');
		$this->view->per_page = $per_page = $config['my_perPage'];

		$lng = Cubix_I18n::getLang();

		$commented = $this->_request->commented;
		if (!in_array($commented, array('yes', 'no'))) $commented = null;

		$date_added_f = intval($this->_request->date_added_f);
		if (!$date_added_f) $date_added_f = null;

		$date_added_t = intval($this->_request->date_added_t);
		if (!$date_added_t) $date_added_t = null;

		$meeting_date_f = intval($this->_request->meeting_date_f);
		if (!$meeting_date_f) $meeting_date_f = null;

		$meeting_date_t = intval($this->_request->meeting_date_t);
		if (!$meeting_date_t) $meeting_date_t = null;

		$filter = array(
			'city' => $this->_request->city,
			'commented' => $commented,
			'date_added_f' => $date_added_f,
			'date_added_t' => $date_added_t,
			'meeting_date_f' => $meeting_date_f,
			'meeting_date_t' => $meeting_date_t
		);

		$sort_field = $this->_request->sort_field;
		if (!in_array($sort_field, array(
				'r.creation_date',
				'r.meeting_date',
				'e.showname',
				'agency_name',
				'r.looks_rating',
				'r.services_rating',
				'city_title',
				'r.fuckometer',
				'u.username'
		)))
			$sort_field = 'r.creation_date';

		$sort_dir = $this->_request->sort_dir;
		if (!in_array($sort_dir, array('asc', 'desc')))
			$sort_dir = 'desc';

		$this->view->sort_field = $sort_field;
		$this->view->sort_dir = $sort_dir;

		if ($this->user->user_type == 'member')
		{
			$filter['showname'] = $this->_request->showname;
			$this->view->filter = $filter;

			$res = $this->model->getReviewsForMember($this->user->id, $lng, $filter, $page, $per_page, $sort_field, $sort_dir);
			$this->view->items = $res[0];
			$this->view->count = $res[1];
		}
		elseif ($this->user->user_type == 'escort')
		{
			$filter['member'] = $this->_request->member;
			$this->view->filter = $filter;
			$escort_id = $this->user->getEscort()->id;

			$res = $this->model->getReviewsForEscort($escort_id, $lng, $filter, $page, $per_page, $sort_field, $sort_dir);
			$this->view->items = $res[0];
			$this->view->count = $res[1];

			/* Pending reviews */
			$pending = Cubix_Api::getInstance()->call('getPendingReviewsForEscort', array($this->user->id, $escort_id));
			$this->view->items_pending = $pending['reviews'];
			$this->view->items_pending_count = $pending['count'];
		}
		else
		{
			$filter['member'] = $this->_request->member;
			$escort_id = intval($this->_request->escort_id);
			if (!$escort_id) $escort_id = null;
			$filter['escort_id'] = $escort_id;
			$this->view->filter = $filter;

			$ag_id = $this->user->getAgency()->id;

			$res = $this->model->getReviewsForAgency($ag_id, $lng, $filter, $page, $per_page, $sort_field, $sort_dir);
			$this->view->items = $res[0];
			$this->view->count = $res[1];
			$this->view->escorts = $res[2];

			/* Pending reviews */
			$pending = Cubix_Api::getInstance()->call('getPendingReviewsForAgencyEscorts', array($this->user->id));
			$this->view->items_pending = $pending['reviews'];
			$this->view->items_pending_count = $pending['count'];
		}
	}

	public function addCommentAction()
	{
		$this->view->layout()->disableLayout();

		if ($this->user->user_type != 'escort' && $this->user->user_type != 'agency') die;

		$rev_id = intval($this->_request->review_id);

		if (!$rev_id) die;

		if ($this->user->user_type == 'escort')
		{
			if (!Cubix_Api::getInstance()->call('checkEscortReview', array($this->user->id, $rev_id)))
			{
				die;
			}
		}
		else
		{
			if (!Cubix_Api::getInstance()->call('checkEscortReviewForAgency', array($this->user->id, $rev_id)))
			{
				die;
			}
		}

		if ($this->_request->isPost())
		{
			$validator = new Cubix_Validator();

			if (!$this->_request->comment)
				$validator->setError('com', 'Required');

			if ($validator->isValid())
			{
				Cubix_Api::getInstance()->call('addReviewComment', array($rev_id, $this->_request->comment));
				echo json_encode(array('status' => 'ok'));
			}
			else
			{
				$status = $validator->getStatus();
				echo(json_encode($status));
			}

			die;
		}
	}

	public function answerAction()
	{
		$this->view->layout()->disableLayout();

		if ($this->user->user_type != 'escort' && $this->user->user_type != 'agency') die;

		$review_id = intval($this->_request->review_id);
		$answer = intval($this->_request->answer);

		if (!in_array($answer, array(1, 2)))
			$answer = null;

		if ($review_id && !is_null($answer))
		{
			if ($this->user->user_type == 'escort')
			{
				$check = Cubix_Api::getInstance()->call('checkPendingReviewForEscort', array($this->user->id, $review_id));

				if ($check)
				{
					Cubix_Api::getInstance()->call('setPendingReviewForEscort', array($review_id, $answer));
					echo json_encode(array('status' => 'ok'));
				}
				else
				{
					echo json_encode(array('status' => 'error'));
				}
			}
			else
			{
				$check = Cubix_Api::getInstance()->call('checkPendingReviewForAgencyEscort', array($this->user->id, $review_id));

				if ($check)
				{
					Cubix_Api::getInstance()->call('setPendingReviewForEscort', array($review_id, $answer));
					echo json_encode(array('status' => 'ok'));
				}
				else
				{
					echo json_encode(array('status' => 'error'));
				}
			}
		}
		else
		{
			echo json_encode(array('status' => 'error'));
		}

		die;
	}

	public function escortAction()
	{
		$request = $this->_request;

		if ($request->mode == 'ajax')
			$this->view->layout()->disableLayout();

		$showname = $request->showname;
		$escort_id = intval($request->escort_id);
		$escort = $this->model->getEscortInfo($escort_id);
		$escort = new Model_EscortV2Item($escort);
		$m_e = new Model_EscortsV2();
		$add_esc_data = $m_e->getRevComById($escort_id);
		$escort->disabled_reviews = $add_esc_data->disabled_reviews;
		$escort->disabled_comments = $add_esc_data->disabled_comments;

		$this->view->escort = $escort;

		$page = intval($request->page);
		if ($page < 1) $page = 1;
		$this->view->page = $page;

		$perpage = 2;

		if (isset($request->perpage))
		{
			if ($request->perpage == 'all')
				$perpage = 99999;
			elseif (intval($request->perpage) > 0)
				$perpage = intval($request->perpage);
		}

		$r_id = $request->r_id;
		$r_id = intval($r_id);

		if ($r_id > 0)
			$this->view->r_id = $r_id;
		else
			$r_id = null;

		$ret = $this->model->getEscortReviews($escort_id, $page, $perpage, $r_id);

		list($items, $count) = $ret;

		$this->view->items = $items;
		$this->view->count = $count;

		if ($perpage == 99999) $perpage = 'all';
		$this->view->perpage = $perpage;

		if (!$items && ($escort->status & ESCORT_STATUS_DELETED))
		{
			$this->_redirect($this->view->getLink('reviews'));
			return;
		}

		if (($escort->status & ESCORT_STATUS_DELETED) || ($escort->status & ESCORT_STATUS_OWNER_DISABLED) || ($escort->status & ESCORT_STATUS_ADMIN_DISABLED))
		{
			$this->view->disable = true;
		}
	}
}
