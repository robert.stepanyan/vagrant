<?php

class VerifyController extends Zend_Controller_Action
{
	/**
	 * @var Model_Escorts
	 */
	public $model;
	
	/**
	 * @var Model_EscortItem
	 */
	public $escort;
	
	/**
	 * @var Model_VerifyRequests
	 */
	public $verifyRequests;
	
	public function init()
	{
		$this->view->user = $this->user = Model_Users::getCurrent();
		
		if ( ! $this->user ) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}
		
		$this->verifyRequests = new Model_VerifyRequests();
		$this->model = new Model_Escorts();
	}
	
	
	protected function _check()
	{
		// Checks if there is an active request
		
		if ( $this->escort->isVerified() ) {
			//$this->_forward('confirmed');
			$this->_helper->viewRenderer->setScriptAction('confirmed');
		}
		
		if ( $this->activeRequest = $this->escort->getVerifyRequest() ) {
			$this->view->request = $this->activeRequest;
			
			if ( $this->activeRequest->isPending() ) {
				//$this->_forward('pending');
				$this->_helper->viewRenderer->setScriptAction('pending');
			}
			elseif ( $this->activeRequest->isConfirmed() ) {
				$this->_helper->viewRenderer->setScriptAction('confirmed');
				//$this->_forward('confirmed');
			}
			
			return TRUE;
		}
		
		return FALSE;
	}

	public function successAction()
	{
		$this->view->layout()->disableLayout();
	}
	
	public function idcardAction()
	{
		$this->view->layout()->disableLayout();

		if ( $this->user->isAgency() ) {
			$agency = $this->user->getAgency();

			$escorts = $agency->getEscorts(array(), $count);

			foreach ($escorts as $k => $escort)
			{
				$escort = new Model_EscortItem($escort);

				if ( $escort->isVerified() || is_null($escort['photo_hash']) )
				{
					unset($escorts[$k]);
				} else {
					if ( $this->activeRequest = $escort->getVerifyRequest() ) {
						if ( $this->activeRequest->isPending() ) {
							unset($escorts[$k]);
						}
						elseif ( $this->activeRequest->isConfirmed() ) {
							unset($escorts[$k]);
						}
					}
				}
			}

			$this->view->escorts = $escorts;
		} else if ( $this->user->isEscort() ) {
			$escort = $this->escort = $this->user->getEscort();
			$escort_id = $this->view->escort_id = $escort->id;

			$this->_check();
		}

		if ( $this->_request->isPost() ) {

			if ( $this->user->isAgency() ) {
				$escort_id = $this->_request->escort_id;
			}

			$hashes = $this->_request->hash;
			$exts = $this->_request->ext;
			$comments = $this->_request->comment;

			$validator = new Cubix_Validator();

			if ( ! $escort_id ) {
				$validator->setError('escort_id', Cubix_I18n::translate('escort_id_required'));
			}

			if ( ! count($hashes) ) {
				$validator->setError('photos', Cubix_I18n::translate('id_card_photos_required'));
			}
			

			if ( ! $validator->isValid() ) {
				die(json_encode($validator->getStatus()));
			} else {

				$item = new Model_VerifyRequest(array(
					'type' => Model_VerifyRequests::TYPE_IDCARD,
					'escort_id' => $escort_id
					//'inform_method' => $session->inform_method,
					//'phone' => $session->phone,
					//'email' => $session->email
					// status default PENDING
				));

				$save_data = array(
					'type' => Model_VerifyRequests::TYPE_IDCARD,
					'escort_id' => $this->escort->id
				);
				
				$item = $this->verifyRequests->save($save_data);

				foreach ( $hashes as $hash ) {
					$item->addPhoto(array(
						'hash' => $hash,
						'ext' => $exts[$hash][0],
						'comment' => $comments[$hash][0]
					));
				}

				die(json_encode($validator->getStatus()));
			}
		}
	}

	public function uploadPhotosAction()
	{
		try {


			$escort_id = (int) $this->_request->escort_id;

			if ( ! isset($_FILES['Filedata']) || ! isset($_FILES['Filedata']['name']) || ! strlen($_FILES['Filedata']['name']) ) {
				$this->view->uploadError = 'Please select a photo to upload';
				return;
			}

			// Save on remote storage
			$images = new Cubix_Images();
			$image = $images->save($_FILES['Filedata']['tmp_name'], $escort_id . '/verify', Cubix_Application::getId(), strtolower(@end(explode('.', $_FILES['Filedata']['name']))));

			$image = new Cubix_Images_Entry($image);
			$image->setSize('t100p');
			$image->setCatalogId($escort_id . '/verify');
			$image_url = $images->getUrl($image);


			echo json_encode(array(
				'status' => 1,
				'name' => "success",
				'escort_id' => $escort_id,
				//'photo_id' => $photo->id,
				'photo_url' => $image_url,
				'hash' => $image['hash'],
				'ext' => $image['ext']
			));
			die;

		} catch ( Exception $e ) {
			echo json_encode(array(
				'status' => 0,
				'error' => $e->getMessage()
			));
			die;
		}
	}
}
