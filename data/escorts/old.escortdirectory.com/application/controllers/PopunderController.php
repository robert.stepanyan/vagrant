<?php

class PopunderController extends Zend_Controller_Action
{
	public function init()
	{
		$this->view->layout()->setLayout('popunder');
	}

	public function indexAction()
	{
		$country_iso = $lang = $this->_getParam('lang');
		if(!in_array($lang, array('en','es','fr','de','ch','pl','cz','ru','at'))){
			$lang = "en";
			
		}
		if(!in_array($country_iso, array('es','fr','de','ch','pl','cz','ru','at','pt','ro','ae','th','hu','gr'))){
			$country_iso = null;
		}
		$this->view->lang = $lang;
		$this->_helper->viewRenderer->setScriptAction("popunder-".$lang);
		$m = new Model_EscortsV2();
		
		$this->view->escorts = $m->getPopunderEscorts($country_iso);
	}

	public function escortsAction()
	{
		$country_iso = $lang = $this->_getParam('lang');
		if(!in_array($lang, array('en','es','fr','de','ch','pl','ru','at'))){
			$lang = "en";
		}
		
		if(!in_array($country_iso, array('es','fr','de','ch','pl','cz','ru','at','pt','ro','ae','th','hu','gr'))){
			$country_iso = null;
		}
		$this->view->lang = $lang;
		$this->_helper->viewRenderer->setScriptAction("escorts-".$lang);
		$m = new Model_EscortsV2();
		$this->view->main_escort = $m->getMainPopunderEscorts($country_iso);
		$this->view->escorts = $m->getPopunderEscorts($country_iso);
	}
	
	public function escortsV2Action()
	{
		$country_iso = $lang = $this->_getParam('lang');
		if(!in_array($lang, array('en','es','fr','de','ch','pl','ru','at'))){
			$lang = "en";
		}
		
		if(!in_array($country_iso, array('es','fr','de','ch','pl','cz','ru','at','pt','ro','ae','th','hu','gr'))){
			$country_iso = null;
		}
		$this->view->lang = $lang;
		$this->_helper->viewRenderer->setScriptAction("escorts-".$lang);
		$m = new Model_EscortsV2();
		$this->view->escorts = $m->getPopunderEscorts($country_iso, 10);
	}
}