<?php

class SitemapController extends Zend_Controller_Action
{

    public function indexAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        //echo dirname(__FILE__); die;
        $fh = fopen("../public/sitemap.xml", 'w+') or die("can't open file");


        $time = date("Y-m-d")."T".date("H:i:s+00:00");

            $xml = '<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9' .
                ' ' . 'http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <sitemap>
        <loc>' . 'http://' . $_SERVER['SERVER_NAME'] . '/sitemaps/profiles-sitemap.xml</loc>
        <lastmod>' . $time . '</lastmod>
    </sitemap>
    <sitemap>
        <loc>' . 'http://' . $_SERVER['SERVER_NAME'] . '/sitemaps/pages-sitemap.xml</loc>
        <lastmod>' . $time . '</lastmod>
    </sitemap>
    <sitemap>
        <loc>' . 'http://' . $_SERVER['SERVER_NAME'] . '/sitemaps/countries-sitemap.xml</loc>
        <lastmod>' . $time . '</lastmod>
    </sitemap>
</sitemapindex>';

        //echo $xml;

        fwrite($fh, $xml);
        fclose($fh);
    }

    public function renderAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $profiles = $pages = $countries = array();

        $_time = date("Y-m-d")."T".date("H:i:s+00:00");

        $langs = Cubix_I18n::getLangs();

//        foreach( $langs as $lang ){
            //$_lang = ( $lang == 'en' ) ? '' : '/' . $lang;

            # ============================================ Rendering Profiles XML Sitemap ============================================ #

            $escorts_list = Model_Escort_List::getFiltered( array("show_all_agency_escorts" => 1), 'newest', 1, 20000 );

            $fh_profile = fopen("../public/sitemaps/profiles-sitemap.xml", 'w+') or die("can't open file");

            $xml_profiles = '<?xml version="1.0" encoding="UTF-8"?>
     <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/TR/xhtml11/xhtml11_schema.html">';


            $home_langs = '';
            foreach( $langs as $lang ){
                $_lang = ( $lang == 'en' ) ? '' : '/' . $lang;
                $home_langs .= '<xhtml:link
                             rel="alternate"
                             hreflang="' . trim( $lang, '/' ) . '"
                             href="http://' . $_SERVER['SERVER_NAME'] . $_lang . '/"
                             />' . "\n";
            }

            $xml_profiles .= '
                    <url>
                        <loc>http://' . $_SERVER['SERVER_NAME'] . '</loc>
                        ' . $home_langs . '
                        <lastmod>' . $_time . '</lastmod>
                        <changefreq>daily</changefreq>
                        <priority>1</priority>
                    </url>';

            foreach( $escorts_list as $escort ):
                $lang_pages = '';

                foreach( $langs as $lang ){
                    $_lang = ( $lang == 'en' ) ? '' : '/' . $lang;
                    $lang_pages .= '<xhtml:link
                         rel="alternate"
                         hreflang="' . trim( $lang, '/' ) . '"
                         href="http://' . $_SERVER['SERVER_NAME'] . $_lang . $this->view->getLink('profile', array('showname' => $escort->showname, 'escort_id' => $escort->id) ) . '"
                         />' . "\n";
                }

                $xml_profiles .= '
                    <url>
                        <loc>http://' . $_SERVER['SERVER_NAME'] . $this->view->getLink('profile', array('showname' => $escort->showname, 'escort_id' => $escort->id) ) . '</loc>
                        ' . $lang_pages . '
                        <lastmod>' . date( "Y-m-d", strtotime( $escort->date_last_modified ) )."T".date( "H:i:s+00:00", strtotime( $escort->date_last_modified ) ) . '</lastmod>
                        <changefreq>monthly</changefreq>
                        <priority>0.8</priority>
                    </url>';
            endforeach;

            $xml_profiles .= '</urlset>';

            fwrite($fh_profile, $xml_profiles);
            fclose($fh_profile);


            # ============================================ Rendering Countries XML Sitemap ============================================ #

            $countries_model = new Model_Statistics();
            $countries_list = $countries_model->getEDCitiesByCountry();
            $countries_list = $countries_list['data'];
            //print_r($countries_list); die;

            $fh_country = fopen("../public/sitemaps/countries-sitemap.xml", 'w+') or die("can't open file");

            $xml_profiles = '<?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/TR/xhtml11/xhtml11_schema.html">';

            foreach( $countries_list as $country ):
                $country_langs = '';
                foreach( $langs as $lang ){
                    $_lang = ( $lang == 'en' ) ? '' : '/' . $lang;
                    $country_langs .= '<xhtml:link
                         rel="alternate"
                         hreflang="' . trim( $lang, '/' ) . '"
                         href="http://' . $_SERVER['SERVER_NAME'] . $_lang . '/escorts-' . $country->country_slug . '-c' . $country->country_id . '"
                         />' . "\n";
                }

                $xml_profiles .= '
                    <url>
                        <loc>http://' . $_SERVER['SERVER_NAME'] . '/escorts-' . $country->country_slug . '-c' . $country->country_id . '</loc>
                        ' . $country_langs . '
                        <lastmod>' . $_time . '</lastmod>
                        <changefreq>weekly</changefreq>
                        <priority>0.6</priority>
                    </url>';
            endforeach;

            $xml_profiles .= '</urlset>';

            fwrite($fh_country, $xml_profiles);
            fclose($fh_country);

//        }

        $this->_pingSearchEngines();

    }

    private function _pingSearchEngines(){
        $sitemap_url = 'http://' . $_SERVER['SERVER_NAME'] . '/sitemap.xml';

        $ping_urls = array(
            "google" => "http://www.google.com/webmasters/sitemaps/ping?sitemap=" . $sitemap_url,
            "yandex" => "http://blogs.yandex.ru/pings/?status=success&url=" . $sitemap_url,
            "bing" => "http://www.bing.com/webmaster/ping.aspx?siteMap=" . $sitemap_url
        );

        foreach( $ping_urls as $se => $url ){
            echo '<h1 class="ping-notific"><span class="se-name">' . $se ."</span> ping url: " . $url . "\n";
            $response = file_get_contents($url);
            echo '<span class="ping-success"> Ping status Success !!!</span></h1>';
            //echo "\nResponse: " . $response . '</h1>';
        }

        echo '<style>
            body { margin: 0; padding: 0; }
            h1.ping-notific{
                font-size: 18px;
                margin-bottom: 20px;
                background: #DADADA;
                padding: 10px 0;
                font-family: Verdana, Arial, sans-serif;
                border-top: 1px solid #CCC;
                border-bottom: 1px solid #CCC;
                text-align: center;
                color: #9D0101;
            }
            span.se-name { text-transform: capitalize; }
            span.ping-success { color: #19B600; }
        </style>';

    }
}
