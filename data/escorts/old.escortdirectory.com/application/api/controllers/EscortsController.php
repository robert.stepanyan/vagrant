<?php

class Api_EscortsController extends Zend_Controller_Action
{
	/**
	 * @var Zend_Db_Adapter_Mysqli
	 */
	protected $_db;

	protected $_debug = true;

	public function init()
	{
		$this->_db = Zend_Registry::get('db');

		$this->view->layout()->disableLayout();
	}


	protected function _printUsage($label)
	{
		if ( $this->_debug ) {
			echo '<span class="strong">' . $label . '</span>: ' . (memory_get_usage() / 1024) . " KB<br/>\r\n";
		}
	}

	/**
	 * Get escorts with xml rpc and insert all rows into the short table
	 */
	public function indexAction()
	{

		$errors = array();

		$client = new Cubix_Api_XmlRpc_Client();

		$this->_printUsage('Before Escorts.getTableSqlDump');
		$dump = $client->call('Escorts.getTableSqlDump');
		$this->_printUsage('After Escorts.getTableSqlDump');

		try {
			$this->_db->query('DROP TABLE IF EXISTS escorts');
		}
		catch ( Exception $e ) {
			$errors[] = array('title' => 'Could not drop escorts short table', 'exception' => $e);
		}

		try {
			$this->_db->query($dump);
		}
		catch ( Exception $e ) {
			$errors[] = array('title' => 'Could not create escorts short table', 'exception' => $e);
		}

		$this->_printUsage('Before Escorts.getEscortCitiesSqlDump');
		$dump = $client->call('Escorts.getEscortCitiesSqlDump');
		$this->_printUsage('After Escorts.getEscortCitiesSqlDump');

		try {
			$this->_db->query('DROP TABLE IF EXISTS escort_cities');
		}
		catch ( Exception $e ) {
			$errors[] = array('title' => 'Could not drop escort_cities short table', 'exception' => $e);
		}

		try {
			$this->_db->query($dump);
		}
		catch ( Exception $e ) {
			$errors[] = array('title' => 'Could not create escort_cities short table', 'exception' => $e);
		}

		$this->_printUsage('Before Escorts.getUpcomingToursTableSqlDump');
		$dump = $client->call('Escorts.getUpcomingToursTableSqlDump');
		$this->_printUsage('After Escorts.getUpcomingToursTableSqlDump');

		try {
			$this->_db->query('DROP TABLE IF EXISTS upcoming_tours');
		}
		catch ( Exception $e ) {
			$errors[] = array('title' => 'Could not drop upcoming_tours short table', 'exception' => $e);
		}

		try {
			$this->_db->query($dump);
		}
		catch ( Exception $e ) {
			$errors[] = array('title' => 'Could not create upcoming_tours short table', 'exception' => $e);
		}

		$this->_printUsage('Before Escorts.getPhotosTableSqlDump');
		$dump = $client->call('Escorts.getPhotosTableSqlDump');
		$this->_printUsage('After Escorts.getPhotosTableSqlDump');

		try {
			$this->_db->query('DROP TABLE IF EXISTS escort_photos');
		}
		catch ( Exception $e ) {
			$errors[] = array('title' => 'Could not drop photos table', 'exception' => $e);
		}

		try {
			$this->_db->query($dump);
		}
		catch ( Exception $e ) {
			$errors[] = array('title' => 'Could not create photos table', 'exception' => $e);
		}

		// Transfer escorts short table

		$this->_printUsage('Before Escorts.getAll');
		$data = $client->call('Escorts.getAll');
		$this->_printUsage('After Escorts.getAll');

		if ( isset($data['error']) ) {
			print_r($data['error']);
			die;
		}

		$escorts = $data['result'];

		if ( count($escorts) ) {
			foreach ( $escorts as $escort ) {
				try {
					$this->_db->insert('escorts', $escort);
				}
				catch ( Exception $e ) {
					$errors[] = array('title' => 'Inserting of escort `' . $escort['showname'] . '` failed', 'exception' => $e);
				}
			}

			$this->_printUsage('Before unset');

			$data = null;
			$escorts = null;

			$this->_printUsage('After unset');
		}


		$this->_printUsage('Before Escorts.getAllUpcomingTours');
		// Transfer upcoming_tours short table
		$uts = $client->call('Escorts.getAllUpcomingTours');
		$this->_printUsage('After Escorts.getAllUpcomingTours');


		if ( count($uts) ) {
			foreach ( $uts as $ut ) {
				try {
					$this->_db->insert('upcoming_tours', $ut);
				}
				catch ( Exception $e ) {
					$errors[] = array('title' => 'Inserting of escort `' . $ut['id'] . '` failed', 'exception' => $e);
				}
			}

			$this->_printUsage('Before Unset');
			$uts = null;
			$this->_printUsage('After Unset');
		}
		///////////////////////////////


		$this->_printUsage('Before Escorts.getEscortCities');
		// Transfer escort_cities short table
		$ecs = $client->call('Escorts.getEscortCities');
		$this->_printUsage('After Escorts.getEscortCities');

		if ( count($ecs) ) {
			foreach ( $ecs as $ec ) {
				try {
					$this->_db->insert('escort_cities', $ec);
				}
				catch ( Exception $e ) {
					//$errors[] = array('title' => 'Inserting of escort `' . $ec['escort_id'] . '` and city `' . $ec['city_id'] . '` failed', 'exception' => $e);
				}
			}

			$this->_printUsage('Before Unset');
			$ecs = null;
			$this->_printUsage('After Unset');
		}
		///////////////////////////////


		$this->_printUsage('Before Escorts.getAllPhotos');
		// Transfer photos table
		$data = $client->call('Escorts.getAllPhotos');
		$this->_printUsage('After Escorts.getAllPhotos');

		if ( isset($data['error']) ) {
			print_r($data['error']);
			die;
		}

		$photos = $data['result'];

		if ( count($photos) ) {
			foreach ( $photos as $photo ) {
				try {
					unset($photo['_inter_index']);
					$this->_db->insert('escort_photos', $photo);
				}
				catch ( Exception $e ) {
					$errors[] = array('title' => 'Inserting of photo failed(escort_id: ' . $photo['escort_id'] . ', photo_id: ' . $photo['id'] . ')', 'exception' => $e);
				}
			}

			$this->_printUsage('Before Unset');
			$data = null;
			$photos = null;
			$this->_printUsage('After Unset');
		}

		// Display errors

		if ( count($errors) ) {
			foreach ( $errors as $error ) {
				echo '<dt>' . $error['title'];
				echo '<dd>' . $error['exception']->getMessage() . '</dd>';
				echo '</dt>';
			}
		}
		else {
			echo 'Transfer Complete !';
		}

		$cache = Zend_Registry::get('cache');
		$hits_cache = $cache->load(Model_Escorts::HITS_CACHE_KEY);
		//$cache->clean();
		if ( $hits_cache ) {
			$cache->save(Model_Escorts::HITS_CACHE_KEY, $hits_cache);
		}

		die;
	}

	protected function _call($method, $params = array())
	{
		return Cubix_Api::getInstance()->call($method, $params, 120);

		/*$url = Cubix_Api_XmlRpc_Client::getServer() . '/?api_key=' . Cubix_Api_XmlRpc_Client::getApiKey();
		$url .= '&method=' . $method;

		
		if ( count($params) ) {
			$postdata = http_build_query(array('params' => $params));
			$opts = array('http' => array(
				'method'  => 'POST',
				'header'  => 'Content-type: application/x-www-form-urlencoded',
				'content' => $postdata
			));

			$context  = stream_context_create($opts);
			$result = file_get_contents($url, false, $context);
		}
		else {
			$result = file_get_contents($url);
		}
		

		if ( false === $result ) {
			throw new Exception('Could not call method, probably server is unreachable!');
		}

		return unserialize($result);*/
	}

	public function plainAction()
	{
		$cli = new Cubix_Cli();

		Cubix_Cli::setPidFile('/var/run/ed-sync.pid');
		//Cubix_Cli::setPidFile('D:/var/run/ed-sync.pid');
		if ( Cubix_Cli::isRunning() ) {
			$cli->error('The sync is already running, exitting...');
			exit(1);
		}

		//ini_set('memory_limit', '2048M');
		ini_set('memory_limit', '5120M');
		set_time_limit(0);
		$errors = array();

		$client = new Cubix_Api_XmlRpc_Client();

		$dumps = $datum = $status = array();
		try {
			// Escorts Table
			$dumps['escorts'] = $this->_call('getEscortsV2TableDump');
			$this->info('Got Dump of `escorts` Table');

			// Escort Active Products Table
			/*$dumps['escort_products'] = $this->_call('getEscortProductsTableDump');
			$this->info('Got Dump of `escort_products` Table');*/

			// Escort Premium Cities Table
//			$dumps['premium_cities'] = $this->_call('getPremiumCitiesTableDump');
//			$this->info('Got Dump of `premium_cities` Table');

			// Escort Cities Table
			$dumps['escort_cities'] = $this->_call('getEscortCitiesTableDump');
			$this->info('Got Dump of `escort_cities` Table');
			
			if ( Cubix_Application::getId() == APP_ED || Cubix_Application::getId() == 33 ) {
				// Escort Travel Countries Table
				/*$dumps['escort_travel_countries'] = $this->_call('getEscortTravelCountriesTableDump');
				$this->info('Got Dump of `escort_travel_countries` Table');*/
				
				$dumps['escort_travel_continents'] = $this->_call('getEscortTravelContinentsTableDump');
				$this->info('Got Dump of `escort_travel_continents` Table');
			}

			// Escort Cityzones Table
			$dumps['escort_cityzones'] = $this->_call('getEscortCityzonesTableDump');
			$this->info('Got Dump of `escort_cityzones` Table');

			// Upcoming Tours Table
			$dumps['upcoming_tours'] = $this->_call('getUpcomingToursTableDump');
			$this->info('Got Dump of `upcoming_tours` Table');

			// Escort Photo Galleries
			$dumps['escort_galleries'] = $this->_call('getEscortGalleriesTableDump');
			$this->info('Got Dump of `escort_galleries` Table');
			
			// Escort Photos Table
			$dumps['escort_photos'] = $this->_call('getEscortPhotosTableDump');
			$this->info('Got Dump of `escort_photos` Table');

			// Escort Euro Lottery 
			$dumps['euro_lottery'] = $this->_call('getEuroLotteryTableDump');
			$this->info('Got Dump of `euro_lottery` Table');
			
			// Escort Natural Pics 
			$dumps['natural_pics'] = $this->_call('getNaturalPicsTableDump');
			$this->info('Got Dump of `natural_pics` Table');
			
			// Escort Profiles Table
			$dumps['escort_profiles'] = $this->_call('getEscortProfilesV2TableDump');
			$this->info('Got Dump of `escort_profiles` Table');

			// Escort Services Table
			$dumps['escort_services'] = $this->_call('getEscortServicesV2TableDump');
			$status[] = 'Got Dump of `escort_services` Table';

			// Escort Keywords Table
			$dumps['escort_keywords'] = $this->_call('getEscortKeywordsTableDump');
			$status[] = 'Got Dump of `escort_keywords` Table';

			//escorts` video Images table 
			$dumps['video_image'] = $this->_call('getVideosImageTableSqlDump');
			$this->info('Got Dump of `videos_image` Table');
			
			
			//escorts` video table 
			$dumps['video'] = $this->_call('getVideosTableSqlDump');
			$this->info('Got Dump of `video` Table');
			

			// --> Transfer Escorts Table
			$chunk = 100;
			$part = 0;
			$datum['escorts'] = /*$datum['escort_products'] =*/ $datum['premium_cities'] = $invalids = array();
			do {
				$data = $this->_call('getEscortsV2', array($part++ * $chunk, $chunk));
				
				if ( isset($data['error']) ) {
					print_r($data['error']);
					die;
				}
				echo count($data['result']) . ' - ';
				echo count($data['active_package_products']) . "\r\n";
				$datum['escorts'] = array_merge($datum['escorts'], $data['result']);
				//$datum['escort_products'] = array_merge($datum['escort_products'], $data['active_package_products']);
				$datum['premium_cities'] = array_merge($datum['premium_cities'], $data['premium_cities']);
				$invalids = array_merge($invalids, $data['invalids']);
				$this->info('Transfered Data of `escorts` Table');
				ob_flush();
			} while ( count($data['result']) > 0 );
			$data = null; unset($data);
			echo implode(', ', $invalids);
			// <--

			// Transfer Reviews
			/*$datum['reviews'] = $this->_call('getEscortReviewsV2');
			$this->info('Transfered Data of `reviews` Table');*/

			// --> Transfer Reviews
			/*$chunk = 4000;
			$part = 0;
			$datum['reviews'] = array();
			do {
				$data = $this->_call('getEscortReviewsV2', array($part++ * $chunk, $chunk));
				if ( isset($data['error']) ) {
					print_r($data['error']);
					die;
				}
				$datum['reviews'] = array_merge($datum['reviews'], $data['result']);
				$this->info('Transfered Data of `reviews` Table');
				$this->_printUsage('Reviews');
			} while ( count($data['result']) > 0 );
			$data = null; unset($data);*/
			// <--

			// --> Transfer Escort Working Times
			$chunk = 4000;
			$part = 0;
			$datum['escort_working_times'] = array();
			do {
				$data = $this->_call('getEscortWorkingTimesV2', array($part++ * $chunk, $chunk));
				if ( isset($data['error']) ) {
					print_r($data['error']);
					die;
				}
				$datum['escort_working_times'] = array_merge($datum['escort_working_times'], $data['result']);
				$this->info('Transfered Data of `escort_working_times` Table');
				$this->_printUsage('Escort working times');
			} while ( count($data['result']) > 0 );
			$data = null; unset($data);
			// <--

			// --> Transfer Escort Comments
			/*$chunk = 4000;
			$part = 0;
			$datum['comments'] = array();
			do {
				$data = $this->_call('getCommentsV2', array($part++ * $chunk, $chunk));
				if ( isset($data['error']) ) {
					print_r($data['error']);
					die;
				}
				$datum['comments'] = array_merge($datum['comments'], $data['result']);
				$this->info('Transfered Data of `comments` Table');
				$this->_printUsage('Escort comments');
			} while ( count($data['result']) > 0 );
			$data = null; unset($data);*/
			// <--


			// Transfer Upcoming Tours Table
			$datum['upcoming_tours'] = $this->_call('getUpcomingTours');
			$this->info('Transfered Data of `upcoming_tours` Table');

			// Transfer Escort Services
			$datum['escort_services'] = $this->_call('getEscortServicesV2');
			$status[] = 'Transfered Data of `escort_services` Table';

			// Transfer Escort Keywords
			$datum['escort_keywords'] = $this->_call('getEscortKeywords');
			$status[] = 'Transfered Data of `escort_keywords` Table';

			// Transfer Escort Cities Table
			$datum['escort_cities'] = $this->_call('getEscortCities');
			$this->info('Transfered Data of `escort_cities` Table');
			
			if ( Cubix_Application::getId() == APP_ED || Cubix_Application::getId() == 33 ) {
				// Transfer Escort Travel Countries Table
				//$datum['escort_travel_countries'] = $this->_call('getEscortTravelCountries');
				//$this->info('Transfered Data of `escort_travel_countries` Table');
				
				// Transfer Escort Travel Countries Table
				$datum['escort_travel_continents'] = $this->_call('getEscortTravelContinents');
				$this->info('Transfered Data of `escort_travel_continents` Table');
			}

			// Transfer Escort Cities Table
			$datum['escort_cityzones'] = $this->_call('getEscortCityzones');
			$this->info('Transfered Data of `escort_cityzones` Table');

			// --> Transfer Photos Table
			$chunk = 8000;
			$part = 0;
			$datum['escort_photos'] = array();
			do {
				$data = $this->_call('getEscortPhotos', array($part++ * $chunk, $chunk));
				if ( isset($data['error']) ) {
					print_r($data['error']);
					die;
				}
				$datum['escort_photos'] = array_merge($datum['escort_photos'], $data['result']);
				$this->info('Transfered Data of `escort_photos` Table');
				$this->_printUsage('Escort Photos');
			} while ( count($data['result']) > 0 );
			$data = null; unset($data);
			// <--
			
			// --> Transfer escort_galleries Table
			$chunk = 8000;
			$part = 0;
			$datum['escort_galleries'] = array();
			do {
				$data = $this->_call('getEscortgalleries', array($part++ * $chunk, $chunk));
				if ( isset($data['error']) ) {
					print_r($data['error']);
					die;
				}
				$datum['escort_galleries'] = array_merge($datum['escort_galleries'], $data['result']);
				$this->info('Transfered Data of `escort_galleries` Table');
				$this->_printUsage('Escort Galleries');
			} while ( count($data['result']) > 0 );
			$data = null; unset($data);
			// <--
			
			// --> Transfer natural_pics Table
			$chunk = 8000;
			$part = 0;
			$datum['natural_pics'] = array();
			do {
				$data = $this->_call('getNaturalPics', array($part++ * $chunk, $chunk));
				if ( isset($data['error']) ) {
					print_r($data['error']);
					die;
				}
				$datum['natural_pics'] = array_merge($datum['natural_pics'], $data['result']);
				$this->info('Transfered Data of `natural_pics` Table');
				$this->_printUsage('Natural Pics');
			} while ( count($data['result']) > 0 );
			$data = null; unset($data);
			// <--
			
			// --> Transfer Euro Lottery Table
			$chunk = 8000;
			$part = 0;
			$datum['euro_lottery'] = array();
			do {
				$data = $this->_call('getEuroLottery', array($part++ * $chunk, $chunk));
				if ( isset($data['error']) ) {
					print_r($data['error']);
					die;
				}
				$datum['euro_lottery'] = array_merge($datum['euro_lottery'], $data['result']);
				$this->info('Transfered Data of `euro_lottery` Table');
				$this->_printUsage('Euro Lottery');
			} while ( count($data['result']) > 0 );
			$data = null; unset($data);
			// <--
			
			// --> Transfer Photo Votes Table
			$chunk = 8000;
			$part = 0;
			$datum['escort_photo_votes'] = array();
			do {
				$data = $this->_call('getEscortPhotoVotes', array($part++ * $chunk, $chunk));
				if ( isset($data['error']) ) {
					print_r($data['error']);
					die;
				}
				$datum['escort_photo_votes'] = array_merge($datum['escort_photo_votes'], $data['result']);
				$this->info('Transfered Data of `escort_photo_votes` Table');
				$this->_printUsage('Escort Photo Votes');
			} while ( count($data['result']) > 0 );
			$data = null; unset($data);
			// <--


			// --> Transfer Profiles Table
			$chunk = 2000;
			$part = 0;
			$datum['escort_profiles'] = array();
			do {
				$data = $this->_call('getEscortProfilesV2', array($part++ * $chunk, $chunk));
				if ( isset($data['error']) ) {
					print_r($data['error']);
					die;
				}
				$datum['escort_profiles'] = array_merge($datum['escort_profiles'], $data['result']);
				$this->info('Transfered Data of `escort_profiles` Table');
				$this->_printUsage('Escort Profiles');
			} while ( count($data['result']) > 0 );
			$data = null; unset($data);
			// <--


			$this->info('------------------------------------------------');

			//$this->_db->query('TRUNCATE reviews');
			//$this->_db->query('TRUNCATE comments');
			$this->_db->query('TRUNCATE escort_working_times');
			
			// --> Finally update the database
			foreach ( $dumps as $table => $dump ) {
				if ( isset($dump) && $dump ) {
					$this->_db->query('DROP TABLE IF EXISTS `' . $table . '`');
					$this->_db->query($dump);
					$this->info('Updated Structure of `' . $table . '` Table');

					// Add price for sorting by it
					if ( 'escorts' == $table ) {
						$this->_db->query('ALTER TABLE escorts ADD COLUMN incall_price int(5) UNSIGNED NULL DEFAULT NULL AFTER rates');
						$this->_db->query('ALTER TABLE escorts ADD COLUMN outcall_price int(5) UNSIGNED NULL DEFAULT NULL AFTER rates');
						$this->_db->query('ALTER TABLE escorts ADD COLUMN incall_currency tinyint(1) UNSIGNED NULL DEFAULT NULL AFTER rates');
						$this->_db->query('ALTER TABLE escorts ADD COLUMN outcall_currency tinyint(1) UNSIGNED NULL DEFAULT NULL AFTER rates');
					}
				}
			}

			$this->addBillingFields();
			$this->addVideos();
						
			$this->info('------------------------------------------------');

			// Blacklisted countries
			$bl_countries_count = $this->_db->query('SELECT COUNT(id) AS count FROM countries')->fetch();
			$bl_countries_count = (object) $bl_countries_count;
			$bl_countries_fields = ceil($bl_countries_count->count / 63);

			$bl_countries_set_values = array();
			for ( $bli = 1; $bli <= $bl_countries_fields; $bli++ ) {									
				$_set_values = $this->_db->fetchRow('DESCRIBE escorts blacklisted_countries_' . $bli);
				$set = substr($_set_values->Type,5,strlen($_set_values->Type)-7);
			    $bl_countries_set_values[$bli] = preg_split("/','/", $set);
			}
			// Blacklisted countries			
			

			foreach ( $datum as $table => $data ) {
				
				/*$this->info('Disabling index for table ' . $table);
				$this->_db->query('ALTER TABLE ' . $table . ' DISABLE KEYS;');*/

				if ( count($data) > 0 ) {
					foreach ( $data as $row ) {
						//$this->_db->query('LOCK TABLES ' . $table . ' WRITE');
						if ( $table == 'escort_products' ) {
							foreach($row as $k => $r) {
								$this->_db->insert($table, $r);
							}
						}
						else if ( $table == 'premium_cities' ) {
							if ( count($row) > 0 ) {
								foreach($row as $k => $r) {
									$this->_db->insert($table, $r);
								}
							}
						}
						else {
							if ( 'escorts' == $table) {

								$incall_price = $incall_currency = $outcall_price = $outcall_currency = null;
								$rates = @unserialize($row['rates']);
								if ( is_array($rates) ) {
									// Get price and currency of regular incall and outcall rates for one hour
									//REFACTORED BY VAHAG
									foreach ( $rates as $rate ) {
										if ( ! $rate['type'] && intval($rate['price']) > 0 ) { // for regular rates and defined prices
											if ( ( TIME_HOURS == $rate['time_unit'] && 1 == $rate['time']) || ( TIME_MINUTES == $rate['time_unit'] && 60 == $rate['time'] ) ) {
												
												if ( AVAILABLE_INCALL == $rate['availability'] ) { 
													$incall_price = $rate['price'];
													$incall_currency = $rate['currency_id'];
												} elseif ( AVAILABLE_OUTCALL == $rate['availability'] ) {
													$outcall_price = $rate['price'];
													$outcall_currency = $rate['currency_id'];
												}
												
											}
										}
										/*if ( ! $rate['type'] && // for regular rate
												TIME_HOURS == $rate['time_unit'] && // for hour
												1 == $rate['time'] && // for one hour
												AVAILABLE_INCALL == $rate['availability'] && // for incall
												intval($rate['price']) > 0 // we need defined price
											) {

											$incall_price = $rate['price'];
											$incall_currency = $rate['currency_id'];
										}
										elseif ( ! $rate['type'] && // for regular rate
												TIME_HOURS == $rate['time_unit'] && // for hour
												1 == $rate['time'] && // for one hour
												AVAILABLE_OUTCALL == $rate['availability'] && // for incall
												intval($rate['price']) > 0 // we need defined price
											) {

											$outcall_price = $rate['price'];
											$outcall_currency = $rate['currency_id'];
										}*/
									}
								}

								$row['incall_price'] = $incall_price;
								$row['incall_currency'] = $incall_currency;
								$row['outcall_price'] = $outcall_price;
								$row['outcall_currency'] = $outcall_currency;
							}

							try {

								// Blacklisted countries
								if ( 'escorts' == $table ) {									
									$blacklisted_countries = $row['blacklisted_countries'];
									if ( ! is_array($blacklisted_countries) ) {
										$blacklisted_countries = array();
									}
									unset($row['blacklisted_countries']);									
								}
								// Blacklisted countries
								
								$this->_db->insert($table, $row);

								// Blacklisted countries
								if ( 'escorts' == $table ) {
									$set_value_update = array();
									foreach ( $bl_countries_set_values as $field_num => $set_vals ) {
										foreach( $set_vals as $set_value ) {
											if ( in_array($set_value, $blacklisted_countries) ) {
												$set_value_update[$field_num][] = $set_value;
											} else {
												$set_value_update[$field_num][] = 9999;
											}
										}
									}

									
									if ( count($set_value_update) ) {
										foreach($set_value_update as $k => $vals) {
											$this->_db->update('escorts', array('blacklisted_countries_' . $k => join(',', $vals)), $this->_db->quoteInto('id = ?', $row['id']));
										}
									}
								}
								// Blacklisted countries								
							}
							catch ( Exception $e ) {
								var_dump($row);
								echo $e;
								continue;
							}
						}
						//$this->_db->query('UNLOCK TABLES');
					}

					if ( $table == 'premium_cities' ) {
						//$this->info('Updated Data of `' . $table . '` Table (' . count($datum[$table]) . ' rows in total)');
					}
					else if ( $table == 'escort_products' ) {
						//$this->info('Updated Data of `' . $table . '` Table (' . count($datum[$table]) . ' rows in total)');
					}
					else {
						$this->info('Updated Data of `' . $table . '` Table (' . count($data) . ' rows in total)');
					}
				}

				/*$this->info('Enabling index for table ' . $table);
				$this->_db->query('ALTER TABLE ' . $table . ' ENABLE KEYS;');*/
			}
			// <--


			$this->info('------------------------------------------------');

			// Clear whole cache except hits count
			/*$cache = Zend_Registry::get('cache');
			$hits_cache = $cache->load(Model_Escorts::HITS_CACHE_KEY);
			$cache->clean();
			if ( $hits_cache ) {
				$cache->save($hits_cache, Model_Escorts::HITS_CACHE_KEY);
			}
			$this->info('Cache Cleared');*/


			$this->info('------------------------------------------------');

			$this->plainBilling();
			$this->_postSync();

			$this->info('Transfer Successfully Completed!');

			//$clear_cache = file_get_contents(Cubix_Application::getById(Cubix_Application::getId())->url . "/api/escorts/clear-cache");
			$clear_cache = Model_Applications::clearCache();
			echo($clear_cache) . "\r\n";

		}
		catch ( Exception $e ) {
			$this->info('ERROR: Exception Cauth');
			$this->info($e->__toString());
		}

		echo implode('<br/>' . "\r\n", $status);

		die;
	}

	private function _postSync()
	{
		try {
			$this->_db->query('TRUNCATE escorts_in_countries');
			$sql = '
				INSERT INTO escorts_in_countries (escort_id, country_id, gender, is_agency, is_tour, is_upcoming, is_premium)
				SELECT x.escort_id, x.country_id, x.gender, x.is_agency, x.is_tour, x.is_upcoming, x.is_premium
				FROM ((
					SELECT
						e.id AS escort_id, ct.country_id,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 0 AS is_tour, 0 AS is_upcoming,
						0 AS is_premium
					FROM cities AS ct
					INNER JOIN escort_cities ec ON ec.city_id = ct.id
					INNER JOIN escorts e ON e.id = ec.escort_id
					WHERE /*ct.country_id = 71 AND e.is_on_tour = 0 AND*/ FIND_IN_SET(1, e.products) > 0
					GROUP BY ct.country_id
				) UNION (
					SELECT
						e.id AS escort_id, ct.country_id,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 0 AS is_tour, 1 AS is_upcoming,
						e.is_tour_premium AS is_premium
					FROM upcoming_tours ut
					INNER JOIN cities ct ON ct.id = ut.tour_city_id
					INNER JOIN escorts e ON e.id = ut.id
					WHERE /*ct.country_id = 71 AND e.is_on_tour = 0 AND*/ FIND_IN_SET(7, e.products) > 0 AND
						DATE(ut.tour_date_from) > DATE(NOW()) AND ut.tour_date_from < CURDATE() + INTERVAL 7 DAY
					GROUP BY ct.country_id
				) UNION (
					SELECT
						e.id AS escort_id, ct.country_id,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 1 AS is_tour, 0 AS is_upcoming,
						e.is_tour_premium AS is_premium
					FROM escorts e
					INNER JOIN cities ct ON ct.id = e.tour_city_id
					WHERE
						e.is_on_tour = 1 AND FIND_IN_SET(7, e.products) > 0
					GROUP BY ct.country_id
				)) x
			';
			$this->_db->query($sql);
		}
		catch ( Exception $e ) {
			throw new Exception("Unable to repopulate cache table 'escorts_in_countries'\nError: {$e->getMessage()}");
		}
		
		// <editor-fold defaultstate="collapsed" desc="Populate Shorthand Table 'escorts_in_cities'">
		$this->info('------------------------------------------------');
		echo "Truncate escorts_in_cities... ";
		$sql = 'TRUNCATE escorts_in_cities';
		$this->_db->query($sql);
		echo "done\n";
		echo "Filling escorts_in_cities... ";
		$sql = '
			INSERT INTO escorts_in_cities (escort_id, region_id, city_id, gender, is_agency, is_tour, is_upcoming, is_premium, is_base, ordering)
				SELECT x.escort_id, x.region_id, x.id AS city_id, x.gender, x.is_agency, x.is_tour, x.is_upcoming, x.is_premium, x.is_base, x.ordering
				FROM ((
					SELECT
						ct.id AS id, ct.region_id, e.id AS escort_id, e.ordering,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 0 AS is_tour, 0 AS is_upcoming,
						EXISTS (SELECT 1 FROM premium_cities WHERE escort_id = e.id AND city_id = ct.id) AS is_premium,
						(e.city_id = ct.id) AS is_base
					FROM cities AS ct
					INNER JOIN escort_cities ec ON ec.city_id = ct.id
					INNER JOIN escorts e ON e.id = ec.escort_id
					WHERE 1 /*AND e.is_on_tour = 0*/ AND FIND_IN_SET(1, e.products) > 0
				) UNION (
					SELECT
						ut.tour_city_id AS id, ct.region_id, e.id AS escort_id, e.ordering,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 0 AS is_tour, 1 AS is_upcoming,
						e.is_tour_premium AS is_premium,
						FALSE AS is_base
					FROM upcoming_tours ut
					INNER JOIN cities ct ON ct.id = ut.tour_city_id
					INNER JOIN escorts e ON e.id = ut.id
					WHERE 1 /* e.is_on_tour = 0*/ AND FIND_IN_SET(7, e.products) > 0 AND
						DATE(ut.tour_date_from) > DATE(NOW()) AND ut.tour_date_from < CURDATE() + INTERVAL 7 DAY
				) UNION (
					SELECT
						e.tour_city_id AS id, ct.region_id, e.id AS escort_id, e.ordering,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 1 AS is_tour, 0 AS is_upcoming,
						e.is_tour_premium AS is_premium,
						TRUE AS is_base
					FROM escorts e
					INNER JOIN cities ct ON ct.id = e.tour_city_id
					WHERE
						e.is_on_tour = 1 AND FIND_IN_SET(7, e.products) > 0
				)) x
		';
		$this->_db->query($sql);

		$sql = '
				DELETE eic FROM escorts_in_cities AS eic
				WHERE ((SELECT e.is_on_tour FROM escorts e WHERE e.id = eic.escort_id) AND eic.is_tour = 0 AND eic.is_base = 0 AND eic.is_upcoming <> 1)
		';
		$this->_db->query($sql);
		echo "done\n";

		echo "Truncate escorts_in_cityzones... ";
		$sql = 'TRUNCATE escorts_in_cityzones';
		$this->_db->query($sql);
		echo "done\n";
		echo "Filling escorts_in_cityzones... ";
		$sql = '
			INSERT INTO escorts_in_cityzones (escort_id, city_id, cityzone_id, gender, is_agency, is_tour, is_upcoming, is_premium)
				SELECT x.escort_id, x.id AS city_id, x.cityzone_id, x.gender, x.is_agency, x.is_tour, x.is_upcoming, x.is_premium
				FROM ((
					SELECT
						cz.city_id AS id, e.id AS escort_id, cz.id AS cityzone_id,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 0 AS is_tour, 0 AS is_upcoming,
						EXISTS (SELECT 1 FROM premium_cities WHERE escort_id = e.id AND city_id = cz.id) AS is_premium
					FROM cityzones AS cz
					INNER JOIN escort_cityzones ecz ON ecz.city_zone_id = cz.id
					INNER JOIN escorts e ON e.id = ecz.escort_id
					INNER JOIN cities ct ON ct.id = cz.city_id
					WHERE /*ct.country_id = 71 AND e.is_on_tour = 0 AND*/ FIND_IN_SET(1, e.products) > 0
				) UNION (
					SELECT
						ut.tour_city_id AS id, e.id AS escort_id, cz.id AS cityzone_id,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 0 AS is_tour, 1 AS is_upcoming,
						e.is_tour_premium AS is_premium
					FROM upcoming_tours ut
					INNER JOIN cities ct ON ct.id = ut.tour_city_id
					INNER JOIN escorts e ON e.id = ut.id
					INNER JOIN escort_cityzones ecz ON ecz.escort_id = e.id
					INNER JOIN cityzones cz ON cz.id = ecz.city_zone_id AND cz.city_id = ct.id
					WHERE /*ct.country_id = 71 AND e.is_on_tour = 0 AND*/ FIND_IN_SET(7, e.products) > 0 AND
						DATE(ut.tour_date_from) > DATE(NOW()) AND ut.tour_date_from < CURDATE() + INTERVAL 7 DAY
				) UNION (
					SELECT
						e.tour_city_id AS id, e.id AS escort_id, cz.id AS cityzone_id,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 1 AS is_tour, 0 AS is_upcoming,
						e.is_tour_premium AS is_premium
					FROM escorts e
					INNER JOIN cities ct ON ct.id = e.tour_city_id
					INNER JOIN escort_cityzones ecz ON ecz.escort_id = e.id
					INNER JOIN cityzones cz ON cz.id = ecz.city_zone_id AND cz.city_id = ct.id
					WHERE
						e.is_on_tour = 1 AND FIND_IN_SET(7, e.products) > 0
				)) x
		';
		$this->_db->query($sql);
		echo "done\n";
		$this->info('------------------------------------------------');
		// </editor-fold>
	}

	const PRODUCT_NATIONAL_LISTING			= 1;
	const PRODUCT_INTERNATIONAL_DIRECTORY	= 2;
	const PRODUCT_GIRL_OF_THE_MONTH			= 3;
	const PRODUCT_MAIN_PREMIUM_SPOT			= 4;
	const PRODUCT_CITY_PREMIUM_SPOT			= 5;
	const PRODUCT_TOUR_PREMIUM_SPOT			= 6;
	const PRODUCT_TOUR_ABILITY				= 7;
	const PRODUCT_NO_REVIEWS				= 8;
	const PRODUCT_ADDITIONAL_CITY			= 9;
	const PRODUCT_SEARCH					= 10;

	public function addBillingFields()
	{
		$tbl_escorts = $this->_db->describeTable('escorts');

		if ( ! array_key_exists('package_id', $tbl_escorts) ) {
			$this->_db->query('ALTER TABLE escorts ADD COLUMN package_id int(10) UNSIGNED DEFAULT NULL');
		}

		if ( ! array_key_exists('products', $tbl_escorts) ) {
			$this->_db->query("ALTER TABLE escorts ADD COLUMN products SET('1','2','3','4','5','6','7','8','9','10','11','12','13','15','16','17') DEFAULT NULL");
		}

		if ( ! array_key_exists('is_main_premium_spot', $tbl_escorts) ) {
			$this->_db->query('ALTER TABLE escorts ADD COLUMN is_main_premium_spot tinyint(1) UNSIGNED DEFAULT 0');
		}

		if ( ! array_key_exists('is_premium', $tbl_escorts) ) {
			$this->_db->query('ALTER TABLE escorts ADD COLUMN is_premium tinyint(1) UNSIGNED DEFAULT 0');
		}

		if ( ! array_key_exists('is_tour_premium', $tbl_escorts) ) {
			$this->_db->query('ALTER TABLE escorts ADD COLUMN is_tour_premium tinyint(1) UNSIGNED DEFAULT 0');
		}

		$this->info('Adding Billing Fields ...');
	}

	public function plainBilling()
	{
		ini_set('memory_limit', '4096M');
		set_time_limit(0);

		/*$tbl_escorts = $this->_db->describeTable('escorts');

		if ( ! array_key_exists('products', $tbl_escorts) ) {
			$this->_db->query("ALTER TABLE escorts ADD COLUMN products SET('1','2','3','4','5','6','7','8','9','10','11','12','13') DEFAULT NULL");
		}

		if ( ! array_key_exists('is_main_premium_spot', $tbl_escorts) ) {
			$this->_db->query('ALTER TABLE escorts ADD COLUMN is_main_premium_spot tinyint(1) UNSIGNED DEFAULT 0');
		}

		if ( ! array_key_exists('is_premium', $tbl_escorts) ) {
			$this->_db->query('ALTER TABLE escorts ADD COLUMN is_premium tinyint(1) UNSIGNED DEFAULT 0');
		}

		if ( ! array_key_exists('is_tour_premium', $tbl_escorts) ) {
			$this->_db->query('ALTER TABLE escorts ADD COLUMN is_tour_premium tinyint(1) UNSIGNED DEFAULT 0');
		}*/

		$chunk = 5000;
		$part = 0;
		$data = array(); $c = 0;
		try {
			$result = array();
			do {
				$data = $this->_call('getProducts', array($part++ * $chunk, $chunk));
				if ( isset($data['error']) ) {
					print_r($data['error']);
					die;
				}
				$this->info('Got ' . ($part - 1) * $chunk . '/' . $data['count'] . ' chunks');
				$result = array_merge($result, $data['result']);
			} while ( count($data['result']) > 0 );
		}
		catch (Exception $e) {
			
		}
		$data = null; unset($data);
		
		$products = array();
		$packages = array();

		foreach ( $result as $item ) {
			$products[$item['id']][] = $item['product_id'];
			$packages[$item['id']] = $item['package_id'];
		}

		foreach ( $products as $escort_id => $product ) {
			$main_premium_spot = $is_premium = $is_tour_premium = 0;
			
			if ( in_array(self::PRODUCT_MAIN_PREMIUM_SPOT, $product ) ) {
				$main_premium_spot = 1;
			}

			$is_premium = 0;
			/*if ( in_array(self::PRODUCT_MAIN_PREMIUM_SPOT, $product ) || in_array(self::PRODUCT_CITY_PREMIUM_SPOT, $product ) ) {
				$is_premium = 1;
			}*/

			if ( in_array(self::PRODUCT_TOUR_PREMIUM_SPOT, $product ) ) {
				$is_tour_premium = 1;
			}

			$this->_db->update('escorts', array('package_id' => $packages[$escort_id], 'products' => implode(',', $product), 'is_main_premium_spot' => $main_premium_spot, 'is_premium' => $is_premium, 'is_tour_premium' => $is_tour_premium), $this->_db->quoteInto('id = ?', $escort_id) );
		}
		
		echo('Done !!!' . '\n');
	}

	public function info($str)
	{
		echo $str . "\n";
		ob_flush();
	}

	public function clearCacheAction()
	{
	// Clear whole cache except hits count
		$cache = Zend_Registry::get('cache');
		
		$keys = array('v2_escort_profile_views_%s', 'v2_banners_cache_%s', 'v2_%s_banners_cache', 'latest_action_date_%s');
		$apps = array(
			array('ae', 11),
			array('a6', 25),
			array('ed', 69),
			array('it', 9),
			array('sex', 6),
		);
		$saved = array();
		foreach ( $apps as $app ) {
			list($name, $aid) = $app;
			foreach ( $keys as $key ) {
				$key = sprintf($key, $name);
				$cache->setKeyPrefix($aid);
				$value = $cache->load($key);
				if ( ! isset($saved[$aid]) ) {
					$saved[$aid] = array();
				}
				if ( $value )
					$saved[$aid][$key] = $value;
			}
		}
		
		$cache->clean();
		
		foreach ( $saved as $aid => $data ) {
			foreach ( $data as $key => $value ) {
				//echo $aid . ':' . $key . '<br/>';
				if ( $value ) {
					$cache->setKeyPrefix($aid);
					$cache->save($value, $key);
				}
			}
		}
		
		$cache->setKeyPrefix(false);
		
		$sid = 'sedcard_paging_' . Cubix_Application::getId();
		$ses = new Zend_Session_Namespace($sid);
		$ses->unsetAll();

		die('Cache Cleared');
	}

	public function commentsPlainAction()
	{
		$this->_db->query('TRUNCATE comments');

		$comments = $this->_call('getComments');
		foreach ($comments as $comment)
		{
			$this->_db->insert('comments', $comment);
		}

		die;
	}


	public function updateHitsAction()
	{
		$data = Model_Escorts::getAllCachedHits();

		var_dump($data);
		echo "<br/>\r\n";

		$result = $this->_call('updateHits', array($data));

		if ( false === $result ) {
			echo 'Wrong parameters supplied';
		}
		elseif ( is_array($result) && count($result) ) {
			echo 'Could not update following escorts: ' . implode("<br/>\r\n", $result);
		}
		else {
			echo 'Successfully updated';
			Model_Escorts::resetHits();
		}

		die;
	}
	
	public function blockCountriesAction()
	{
		$this->_db->query('TRUNCATE escort_blocked_countries');

		$ec = $this->_call('getBlockedCountries');

		
		foreach ( $ec as $countries ) {
			$this->_db->insert('escort_blocked_countries', $countries);
		}		

		die('done');
	}
	
	public function addVideos()
	{
		$data = $this->_call('getAllVideo');
			
		if(isset($data['video']) && !empty($data['video']))
		{	
			foreach ($data['video'] as $v)
			{
				$video_id = $v['id'];
				try {

					$this->_db->insert('video', $v);
					$front_video_id = $this->_db->lastInsertId();
					foreach ($data['image'] as &$img)
					{	
						if($img['video_id'] == $video_id)
						{						
							$image = $img;
							$image['video_id'] = $front_video_id;
							$this->_db->insert('video_image',$image) ;
						}
					}
				}
				catch ( Exception $e ) {
					$errors[] = array('title' => 'Inserting of video `' . $video_id . '` failed', 'exception' => $e);
				}
			}
		}
		$this->info('Videos Transferred Successfully');
	}

	public function checkNationalityAction()
	{
		$this->_db->query('TRUNCATE nationalities_ed');
		$nationalities = $this->_db->fetchAll('SELECT id FROM nationalities');
		foreach ($nationalities as $nat) {
			$data = array();
			$data['id'] = $nat->id;
			$data['female'] = 0;
			$data['men'] = 0;
			$data['trans'] = 0;
			$data['bdsm'] = 0;



			// For Female
			$natFemale = $this->_db->fetchAll(
				'SELECT e.id FROM escorts e
				INNER JOIN escorts_in_cities c ON e.id = c.escort_id 
				where e.gender = "'.GENDER_FEMALE.'" AND e.nationality_id ='.$nat->id);

			if($natFemale){
				$data['female'] = 1;
			}

			//Newest
			$natNewest = $this->_db->fetchAll(
				'SELECT e.id FROM escorts e
				INNER JOIN escorts_in_cities c ON e.id = c.escort_id 
				where e.gender = "'.GENDER_FEMALE.'" AND e.is_new = 1 AND e.nationality_id ='.$nat->id);

			if($natNewest){
				$data['newest'] = 1;
			}

			// For male
			$natMen = $this->_db->fetchAll(
				'SELECT e.id FROM escorts e
				INNER JOIN escorts_in_cities c ON e.id = c.escort_id
				where e.gender = "'.GENDER_MALE.'" AND e.nationality_id ='.$nat->id);

			if($natMen){
				$data['men'] = 1;
			}

			// For trans
			$natTrans = $this->_db->fetchAll(
				'SELECT e.id FROM escorts e
				INNER JOIN escorts_in_cities c ON e.id = c.escort_id
				where e.gender = "'.GENDER_TRANS.'" AND e.nationality_id ='.$nat->id);
			
			if($natTrans){
				$data['trans'] = 1;
			}

			// For male
			$natBdsm = $this->_db->fetchAll(
				'SELECT e.id FROM escorts e
				INNER JOIN escorts_in_cities c ON e.id = c.escort_id
				where e.gender = "'.GENDER_BDSM.'" AND e.nationality_id ='.$nat->id);

			if($natBdsm){
				$data['bdsm'] = 1;
			}

			$this->_db->insert('nationalities_ed', $data);

		}

		die('Nationalities Check Successfully');
	}	
}
