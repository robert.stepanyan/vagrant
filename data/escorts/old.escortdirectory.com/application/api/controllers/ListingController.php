<?php

class Api_ListingController extends Zend_Controller_Action
{
	/**
	 * @var Zend_Db_Adapter_Mysqli
	 */
	protected $_db;
	protected $_debug = true;
	protected $_log;

	/**
	 * @var Cubix_Api
	 */
	protected $_client;

	public function init()
	{
		$this->_db = Zend_Registry::get('db');
		$this->_client = Cubix_Api::getInstance();

		$this->view->layout()->disableLayout();
	}

	/**
	 * @var Cubix_Cli
	 */
	private $_cli;

	public function syncAction()
	{
		ini_set('memory_limit', '1024M');
		
		$errors = array();
		
		// <editor-fold defaultstate="collapsed" desc="Init CLI">
		$log = $this->_log = new Cubix_Cli_Log('/var/log/ed-listing-sync.log');
		//$log = $this->_log = new Cubix_Cli_Log('D:/em-diff-sync.log');
		$cli = $this->_cli = new Cubix_Cli();
		$cli->clear();
		// </editor-fold>
		
		// <editor-fold defaultstate="collapsed" desc="Prevent from over-syncing">
		Cubix_Cli::setPidFile('/var/run/ed-listing-sync.pid');
		if ( Cubix_Cli::isRunning() ) {
			$cli->colorize('red')->out('Sync is already running, exitting...')->reset();
			exit(1);
		}
		// </editor-fold>

		$cli->out('Fetching \'club_directory\' table dump from api...');
		$table_dump = $this->_client->call('listing.getTableDumpStandart', array());
		
		if ( $table_dump ) {

			try {
				$this->_db->query('TRUNCATE TABLE club_directory');
				$cli->out('Dropping \'club_directory\' table ...');
				$this->_db->query('DROP TABLE IF EXISTS club_directory');
			}
			catch ( Exception $e ) {
				$errors[] = array('title' => 'Could not drop club_directory table', 'exception' => $e);
			}
			
			try {
				$cli->out('Creating \'club_directory\' table ...');
				$this->_db->query($table_dump);
			}
			catch ( Exception $e ) {
				$errors[] = array('title' => 'Could not create club_directory table', 'exception' => $e);
			}
		}
		
		$this->_db->query('TRUNCATE TABLE club_directory');
		
		$start = 0; $limit = 100;
		do {
			$cli->colorize('green')->out('Fetching agencies data from api (max 100)...')->reset();
			$data = $this->_client->call('listing.getAgenciesStandart', array($start++ * $limit, $limit));
			
			$count = count($data);
			
			if ( $count ) {
				foreach ( $data as $i => $res ) {
					$cli->out('[' . ($i+1) . '/' . $count . '] Inserting agency: ' . $res['club_name'])->reset();

					$this->_db->insert('club_directory', $res);
				}
			}
			
		} while ( count($data) > 0 );
		
		exit;
	}
	
	
}