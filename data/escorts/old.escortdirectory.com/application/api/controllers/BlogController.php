<?php

class Api_BlogController extends Zend_Controller_Action
{
	/**
	 * @var Zend_Db_Adapter_Mysqli
	 */
	protected $_db;

	protected $_debug = true;

	protected $_log;

	/**
	 * @var Cubix_Api
	 */
	protected $_client;

	public function init()
	{
		$this->_db = Zend_Registry::get('db');
		$this->_client = Cubix_Api::getInstance();

		$this->view->layout()->disableLayout();
	}

	/**
	 * @var Cubix_Cli
	 */
	private $_cli;

	public function syncAction()
	{
		ini_set('memory_limit', '1024M');

		$errors = array();


		// <editor-fold defaultstate="collapsed" desc="Init CLI">
		$log = $this->_log = new Cubix_Cli_Log('/var/log/ed-blog-sync.log');
		//$log = $this->_log = new Cubix_Cli_Log('D:/_server/var/log/ed-blog-sync.log');
		$cli = $this->_cli = new Cubix_Cli();
		$cli->clear();
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Prevent from over-syncing">
		Cubix_Cli::setPidFile('/var/run/ed-blog-sync.pid');
		//Cubix_Cli::setPidFile('D:/_server/var/run/ed-blog-sync.pid');
		if ( Cubix_Cli::isRunning() ) {
			$cli->colorize('red')->out('Sync is already running, exitting...')->reset();
			exit(1);
		}
		// </editor-fold>

		$cli->out('Fetching \'blog_posts\' table dump from api...');
		$table_dump = $this->_client->call('blog.getBlogPostsTableDump', array());
		

		if ( $table_dump ) {
			try {
				$cli->out('Dropping \'blog_posts\' table ...');
				$this->_db->query('DROP TABLE IF EXISTS blog_posts');
			}
			catch ( Exception $e ) {
				$errors[] = array('title' => 'Could not drop blog_posts table', 'exception' => $e);
			}
			
			try {
				$cli->out('Creating \'blog_posts\' table ...');
				$this->_db->query($table_dump);
			}
			catch ( Exception $e ) {
				$errors[] = array('title' => 'Could not create blog_posts table', 'exception' => $e);
			}
		}

		$cli->out('Fetching \'blog_category\' table dump from api...');
		$table_dump = $this->_client->call('blog.getBlogCategoryTableDump', array());
		
		if ( $table_dump ) {
			try {
				$cli->out('Dropping \'blog_category\' table ...');
				$this->_db->query('DROP TABLE IF EXISTS blog_category');
			}
			catch ( Exception $e ) {
				$errors[] = array('title' => 'Could not drop blog_category table', 'exception' => $e);
			}
			
			try {
				$cli->out('Creating \'blog_category\' table ...');
				$this->_db->query($table_dump);
			}
			catch ( Exception $e ) {
				$errors[] = array('title' => 'Could not create blog_category table', 'exception' => $e);
			}
		}

		$cli->out('Fetching \'blog_images\' table dump from api...');
		$table_dump = $this->_client->call('blog.getBlogImagesTableDump', array());

		if ( $table_dump ) {
			try {
				$cli->out('Dropping \'blog_images\' table ...');
				$this->_db->query('DROP TABLE IF EXISTS blog_images');
			}
			catch ( Exception $e ) {
				$errors[] = array('title' => 'Could not drop blog_images table', 'exception' => $e);
			}

			try {
				$cli->out('Creating \'blog_images\' table ...');
				$this->_db->query($table_dump);
			}
			catch ( Exception $e ) {
				$errors[] = array('title' => 'Could not create blog_images table', 'exception' => $e);
			}
		}
		
		$cli->out('Fetching \'blog_comments\' table dump from api...');
		$table_dump = $this->_client->call('blog.getBlogCommentsTableDump', array());
		
		if ( $table_dump ) {
			try {
				$cli->out('Dropping \'blog_comments\' table ...');
				$this->_db->query('DROP TABLE IF EXISTS blog_comments');
			}
			catch ( Exception $e ) {
				$errors[] = array('title' => 'Could not drop blog_comments table', 'exception' => $e);
			}
			
			try {
				$cli->out('Creating \'blog_comments\' table ...');
				$this->_db->query($table_dump);
			}
			catch ( Exception $e ) {
				$errors[] = array('title' => 'Could not create blog_comments table', 'exception' => $e);
			}
		}
				
		$start = 0; $limit = 100;
		do {
			
			$cli->colorize('green')->out('Fetching blog posts data from api (max 100)...')->reset();
			$data = $this->_client->call('blog.getBlogPosts', array($start++ * $limit, $limit));

			$count = count($data);

			if ( $count ) {
				foreach ( $data as $i => $res ) {
					$cli->out('[' . ($i+1) . '/' . $count . '] Inserting blog post: ' . $res['id'])->reset();

					$this->_db->insert('blog_posts', $res);
				}
			}
			
		} while ( count($data) > 0 );

		// Category
		$cli->colorize('green')->out('Fetching blog category data from api (all)...')->reset();
		$data = $this->_client->call('blog.getBlogCategory');

		$count = count($data);
			
		if ( $count ) {
			foreach ( $data as $i => $res ) {
				$cli->out('[' . ($i+1) . '/' . $count . '] Inserting blog category: ' . $res['id'])->reset();

				$this->_db->insert('blog_category', $res);
			}
		}
		//

		$start = 0; $limit = 100;
		do {

			$cli->colorize('green')->out('Fetching blog images data from api (max 100)...')->reset();
			$data = $this->_client->call('blog.getBlogImages', array($start++ * $limit, $limit));

			$count = count($data);

			if ( $count ) {
				foreach ( $data as $i => $res ) {
					$cli->out('[' . ($i+1) . '/' . $count . '] Inserting blog image: ' . $res['post_id'] . '-' . $res['image_id'])->reset();

					$this->_db->insert('blog_images', $res);
				}
			}

		} while ( count($data) > 0 );
		
		$start = 0; $limit = 100;
		do {
			
			$cli->colorize('green')->out('Fetching blog comments data from api (max 100)...')->reset();
			$data = $this->_client->call('blog.getBlogComments', array($start++ * $limit, $limit));
			
			$count = count($data);
			
			if ( $count ) {
				foreach ( $data as $i => $res ) {
					$cli->out('[' . ($i+1) . '/' . $count . '] Inserting blog comment: ' . $res['id'])->reset();

					$this->_db->insert('blog_comments', $res);
				}
			}
			
		} while ( count($data) > 0 );
				
		exit;
	}
}