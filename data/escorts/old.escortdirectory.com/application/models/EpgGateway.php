<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Model_EpgGateway_Exception extends Exception {
    //put your code here
}

class Model_EpgGateway
{
	private $client;					// Zend HTTP Client


//	private $TokenizerUrl = 'https://apps.is.epg-services.com/V2/pp/tokenizer/get';
//	private $PaymentGatewayUrl = 'https://apps.is.epg-services.com/V2/pp/paymentpage?Token=';
//	private $TransactionStatusUrl = 'https://apps.is.epg-services.com/V2/pp/tokenizer/get_result';

	private $TokenizerUrl = 'https://apps.epg-services.com/V2/pp/tokenizer/get';
	private $PaymentGatewayUrl = 'https://apps.epg-services.com/V2/pp/paymentpage?Token=';
    private $TransactionStatusUrl = 'https://apps.epg-services.com/V2/pp/tokenizer/get_result';


	// Params
	private $MerchantId = '1001447';
	private $MerchantGuid = '95EA2890-D436-CF99-C0B6-40C575E60FB7'; //'638925AF-270A-010A-6849-04440D34B4CD';
	private $MerchantInfo = 'http://www.escortforumit.xxx;info@escortforumit.xxx;+4168688565656';


	public $TransactionReference;		// A string defined by yourself to identify this particular transaction to your system.
										// alpha-numeric, max. length 150 chars

	public $TransactionUrl = "www.escortforumit.xxx";			// The URL of the site you've been sending the request from. This value will be displayed on the payment page.
										// If not send with the request the value from your merchant account will be used.
										// alpha-numeric, max. length 150 chars

	public $RedirectUrl;				// The URL of the page the customer should be redirected to after the transaction is complete.
                                                        // If not send with the request the value from your merchant account will be used.
                                                        // alpha-numeric, max. length 150 chars


	/*Required information for transaction types Auth and Sale
	* All fields in this section are required for the transaction types Auth and Sale
	*/

	public $TransactionType = 'Sale';	// Possible values Auth, Sale or Recurring
	public $Amount;						// The amount of the transaction. Decimal with a "." as decimal separator and 2 decimal places
	public $Currency = 'EUR';			// ISO 4217 3-digit currency code, EUR - 978, USD - 840, CHF - 756


	/* The following fields are For Recurring Payment
	* -------------------------------------------
	*/
	
	public $TrialAmount ;				// The amount to be booked for the duration of the trial. If no trial period should be initiated this value should be 0.00
										// Decimal with a "." as decimal separator and 2 decimal places

	public $TrialDuration;				// The duration of the trial in number of days.
										// mumeric

	public $RecurringMode;				// Possible values are weekly or monthly.
										// alpha

	public $RecurringAmount;			// The amount to be booked for each automatically executed payment.
										// Decimal with a "." as decimal separator and 2 decimal places

	public $RecurringDuration;			// The number of days to automatically book this payment.
										// numeric



	public function __construct($MerchantInfo = 'http://www.adsgate456.com;info@adsgate456.com', $TransactionUrl = 'www.adsgate456.com', $MerchantId = null, $MerchantGuid = null)
	{
		// setting up http cleint
		
		$this->client = new Zend_Http_Client();

		$this->MerchantInfo = $MerchantInfo;
		$this->TransactionUrl = $TransactionUrl;
		
		if ( $MerchantId ){
			$this->MerchantId = $MerchantId;
		}

		if ( $MerchantGuid ){
			$this->MerchantGuid = $MerchantGuid;
		}
	}

	// InitializingTokenizer Request
	private function initTokenizerRequest()
	{
		// Setting up the request URL
		$this->client->setUri($this->TokenizerUrl);

		// setting up initial parameters
		$this->setHttpParams(array('MerchantId', 'MerchantGuid', 'MerchantInfo' ));
		
	}


	// Multiple params set to HTTP
	private function setHttpParams($params)
	{
		foreach($params as $param)
		{
			if(isset($this->$param)) {
				$this->client->setParameterPost($param, $this->$param);
			}
		}
	}


	
	// processing a Sale Transaction
	public function getTokenForSale($reference, $amount, $redirect, $currency = null )
	{
		return $this->getTokenForSimpleTransaction('Sale', $reference, $amount, $redirect, $currency);
	}


	// processing an Auth Transaction
	public function getTokenForAuth($reference, $amount, $redirect, $currency = null )
	{
		return $this->getTokenForSimpleTransaction('Auth', $reference, $amount, $redirect, $currency);
	}



	// processing Recurring payment
	public function getTokenForRecurring($reference, $amount, $duration , $redirect, $mode = 'monthly', $trial_amount = 0, $trial_duration = 0 )
	{
		$this->initTokenizerRequest();

		$this->TransactionType = 'Recurring';
		$this->TransactionReference = $reference;
		$this->RecurringMode = $mode;
		$this->RecurringAmount = sprintf('%.2f', $amount);
		$this->RecurringDuration = (int)$duration;
		$this->RedirectUrl= $redirect;
		$this->TrialAmount = sprintf('%.2f', $trial_amount);
		$this->TrialDuration = $trial_duration;
		
		if(isset($currency) && strlen($currency) == 3) {
			$this->Currency = $currency ;
		}

		// Setting Http Params
		$this->setHttpParams(array( 'TransactionType',
                                            'TransactionReference',
                                            'TransactionUrl',
                                            'RecurringMode',
                                            'RecurringAmount',
                                            'RecurringDuration',
                                            'TrialAmount',
                                            'TrialDuration',
                                            'RedirectUrl',
                                            'Currency'));

		return $this->processTokenizerRequest();
	}



	// Preparing payment gateway URL with Token
	public function getPaymentGatewayUrl($token, $lang = '')
	{
		$url = $this->PaymentGatewayUrl.$token;
		// TODO: ask Piter how to implement Language redirection
		//$url .= ($lang) ? '&Lang='.$lang : '';
		return $url;
	}




	
	// make a simple Transaction Auth or Sale
	private function getTokenForSimpleTransaction($type, $reference, $amount, $redirect, $currency = null )
	{
		$this->initTokenizerRequest();
		
		$this->TransactionType = 'Sale';
		$this->TransactionReference = $reference;
		$this->Amount = sprintf('%.2f', $amount);
		$this->ReturnUrl = $redirect;

		if(isset($currency) && strlen($currency) == 3) {
			$this->Currency = $currency ;
		}
		
		// Setting Http Params
		$this->setHttpParams(array( 'TransactionType',
                                            'TransactionReference',
                                            'TransactionUrl',
                                            'Amount',
                                            'ReturnUrl',
                                            'Currency'));

		return $this->processTokenizerRequest();
	}
	


	// processing Tokenizer Request and getting Token
	private function processTokenizerRequest()
	{
		try
		{
                    //DEBUG: HTTP_Client dump
                    // die(print_r($this->client));

                    $http_response = $this->client->request('POST');

                    // DEBUG: HTTP_Response dump
//                    die(print_r($http_response));
					

                    $status = $http_response->getStatus();
                    $response = $http_response->getBody();

                    // Checking HTTP response status
                    if($status != 200) {
                            throw new Model_EpgGateway_Exception('Invalid HTTP Response from Gateway, status : '.$status);
                    }

                    // Parsing Responce string
                    $response_parsed = $this->parseGatewayResponse($response);

                    // If Gateway response is ERROR
                    if($response_parsed['ResultStatus'] == 'ERROR'){
                            throw new Model_EpgGateway_Exception('ERROR on Gateway: '.$response);
                    }

                    // Checking if Token is provided
                    if(!isset($response_parsed['Token'])) {
                            throw new Model_EpgGateway_Exception('Unexpected response, Success but no Token provided: '.$response);
                    }

                    return $response_parsed['Token'];

		}
		catch(Exception $e)
		{
                    throw $e;
                }
	}


        // Getting the Resulsts for the Transaction with given Tokenizer
        public function getTransactionStatus($token)
        {
            $this->client->setUri($this->TransactionStatusUrl);
			
            // Setting Http Params
            $this->setHttpParams(array( 'MerchantId',
                                        'MerchantGuid' ));

            $this->client->setParameterPost('Token', $token);
			
            $response = $this->client->request('POST');
			
            return $this->parseGatewayResponse($response->getBody());
        }


	// Parsing Gateway Response, example: ResultStatus=OK&ResultCode=10&ResultMessage=Success&Token=79616890338F0E806F74E708D2FC52F1A1173306
	private function parseGatewayResponse($response)
	{
		$ret = array();

		$array = explode('&', $response);
		foreach($array as $item)
		{
                    list($param, $value) = explode('=', $item);
                    $ret[$param] = $value;
		}
		return $ret;
	}





	
}


?>
