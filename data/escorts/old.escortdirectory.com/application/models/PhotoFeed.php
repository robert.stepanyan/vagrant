<?php

class Model_PhotoFeed extends Cubix_Model
{
	private $_cookie_name = "pfr";

	public function getAll($filter = array(), $sort = "r", $page = 1, $page_size = 30, &$count = 0)
	{		
		$page = (int) $page;
		if ( $page < 1 ) $page = 1;
		$limit = ($page_size * ($page - 1)) . ', ' . $page_size;
		
		$where = '';
		$having = '';
		$join = '';
		
		if ( isset($filter['city_id']) && $filter['city_id'] ) {
			$where .= ' AND eic.city_id = ' . $filter['city_id'] . ' ';
		}

		if ( isset($filter['country_id']) && $filter['country_id'] ) {
			$where .= ' AND c.id = ' . $filter['country_id'] . ' ';
			$join = ' INNER JOIN countries c ON c.id = e.country_id ';
		}
		
		$having = ' HAVING r_width > 100 ';

		if ( isset($filter['only_unrated']) && $filter['only_unrated'] ) {
			$having = ' HAVING COUNT(DISTINCT(epv.id)) = 0 AND r_width > 100 ';
		}

		if ( isset($filter['date']) && $filter['date'] ) {
			
			switch ( $filter['date'] ) {
				case 't':
					$where .= ' AND DATE(ep.creation_date) = DATE(NOW())';
					break;
				case 'y':
					$where .= ' AND DATE(ep.creation_date) = DATE(NOW() - INTERVAL 1 DAY)';
					break;
				case 'tw':
					$day = date('N');
					$week_start = date('Y-m-d', ( ($day == 1) ? time() : strtotime('-' . $day + 1 . ' days') ));					
					
					$where .= ' AND DATE(ep.creation_date) >= "' . $week_start . '"';
					break;
				case 'lw':
					$previous_week = strtotime("-1 week +1 day");

					$start_week = strtotime("last monday midnight",$previous_week);
					$end_week = strtotime("next sunday",$start_week);

					$start_week = date("Y-m-d",$start_week);
					$end_week = date("Y-m-d",$end_week);
					
					$where .= ' AND DATE(ep.creation_date) >= "' . $start_week . '" AND DATE(ep.creation_date) <= "' . $end_week . '"';
					break;
				case 'tm':					
					$month_start = date('Y-m-01');
					
					$where .= ' AND DATE(ep.creation_date) >= "' . $month_start . '"';
					break;
				case 'lm':
					$month_start = date('Y-m-01', strtotime('- 1 month'));
					$month_end = date('Y-m-t', strtotime('- 1 month'));

					$where .= ' AND DATE(ep.creation_date) >= "' . $month_start . '" AND DATE(ep.creation_date) <= "' . $month_end . '"';
					break;
			}
		}

		$where .= Cubix_Countries::blacklistCountryWhereCase();
		
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				ep.id AS photo_id, ep.escort_id, ep.ext, ep.`hash`, ep.creation_date, ep.ordering, ' . Cubix_Application::getId() . ' AS application_id, 
				ep.is_portrait, ep.height, ep.width, round((ep.width / ep.height) * 200) as r_width,
				COUNT(DISTINCT(epv.id)) AS votes_count, ROUND( (SELECT SUM(rating) FROM escort_photo_votes WHERE photo_id = ep.id) / COUNT(DISTINCT(epv.id)) ) AS rating
			FROM escort_photos ep
			INNER JOIN escorts e ON e.id = ep.escort_id
			INNER JOIN escorts_in_cities eic on eic.escort_id = e.id
			' . $join . '
			LEFT JOIN escort_photo_votes epv ON epv.photo_id = ep.id
			WHERE /*is_approved = 1 AND ep.width > 410 AND*/ e.gender = 1 ' . $where . ' AND ep.type IN (1,2)
			GROUP BY ep.id
			' . $having . '
			ORDER BY ' . $this->getSort($sort) . '
			LIMIT ' . $limit . '
		';

		$photos = self::db()->fetchAll($sql);
		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');		
		
		return $photos;
	}

	public function getSort($key) {
		$sort = array(
			'r' => 'RAND()',
			//'r' => 'ep.id',
			'n' => 'ep.id DESC',
			'mv' => 'votes_count DESC',
			'lv' => 'votes_count ASC',
			'ra' => 'rating ASC',
			'rd' => 'rating DESC',
		);

		if ( isset($sort[$key]) && $sort[$key] ) {
			return $sort[$key];
		}

		return $sort['r'];
	}

	public function getPhoto($escort_id, $photo_id) 
	{
		$sql = '
			SELECT
				ep.id AS photo_id, ep.escort_id, ep.ext, ep.`hash`, ep.creation_date, ep.ordering, ' . Cubix_Application::getId() . ' AS application_id,
				COUNT(DISTINCT(epv.id)) AS votes_count, ROUND( (SELECT SUM(rating) FROM escort_photo_votes WHERE photo_id = ep.id) / COUNT(DISTINCT(epv.id)) ) AS rating
			FROM escort_photos ep
			LEFT JOIN escort_photo_votes epv ON epv.photo_id = ep.id
			WHERE ep.escort_id = ? AND ep.id = ?
		';

		return self::db()->fetchRow($sql, array($escort_id, $photo_id));
	}

	public function getPhotos($exclude_photo_id = null, $escort_id, $page = 1, $page_size = 5, &$count = 0)
	{		
		$page = (int) $page;
		if ( $page < 1 ) $page = 1;
		$limit = ($page_size * ($page - 1)) . ', ' . $page_size;
		
		$where = '';
		
		if ( $exclude_photo_id ) {
			$where .= ' AND ep.id <> ' . $exclude_photo_id;
		}

		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				ep.id AS photo_id, ep.escort_id, ep.ext, ep.`hash`, ep.creation_date, ep.ordering, ' . Cubix_Application::getId() . ' AS application_id
			FROM escort_photos ep
			INNER JOIN escorts e ON e.id = ep.escort_id
			INNER JOIN escorts_in_cities eic on eic.escort_id = e.id
			WHERE /*is_approved = 1 AND*/ e.gender = 1 ' . $where . ' AND ep.escort_id = ?
			GROUP BY ep.id
			ORDER BY ep.id DESC
			LIMIT ' . $limit . '
		';
		
		
		$photos = self::db()->fetchAll($sql, $escort_id);
		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');		
		
		return $photos;
	}

	public function ratePhoto($photo_id, $rate, $ip, $escort_id)
	{
		self::db()->insert('escort_photo_votes', array('photo_id' => $photo_id, 'rating' => $rate, 'ip' => $ip, 'escort_id' => $escort_id));
	}

	public function getIp()
	{
		$ip = Cubix_Geoip::getIP();

		return $ip;
	}

	
	public static function getAllCitiesForFilter()
    {
        $lng = Cubix_I18n::getLang();
        $sql = '
			SELECT
				ct.id AS city_id,
				ct.title_' . $lng . ' AS city_title, ct.slug AS city_slug,
				ct.country_id, c.title_' . $lng . ' AS country_title
			FROM cities ct
			INNER JOIN escorts_in_cities eic ON eic.city_id = ct.id			
			INNER JOIN countries c ON c.id = ct.country_id
			INNER JOIN escorts e ON eic.escort_id = e.id
			INNER JOIN escort_photos ep ON ep.escort_id = e.id
			WHERE /*ep.is_approved = 1 AND*/ e.gender = 1
			GROUP BY ct.id ORDER BY ct.title_en ASC;
		';

        $cities = self::db()->fetchAll($sql);

        // divide cities array into array of arrays by city's country id
        $result = array();
        foreach ( $cities as $city ) {
            if ( ! isset($result[$city->country_id]) )
                $result[$city->country_id] = array();
            $result[$city->country_id][] = $city;
        }

        // find cities for each country from $result array
        $countries = Model_Statistics::getCountries($gender, $is_agency, $is_tour, $is_upcoming);
        foreach ( $countries as &$country ) {
            if ( isset($result[$country->country_id])) {
                $country->cities = $result[$country->country_id];
            }
        }

		foreach ($countries as $k => $c)
		{
			if (!isset($c->cities))
				unset($countries[$k]);
		}
		
        return $countries;
    }

    public function _checkIsPhotoRated($photo_id)
    {
    	if ( ! isset($_COOKIE[$this->_cookie_name]) ) {
    		return false;
    	} else {
    		$data = json_decode(stripslashes($_COOKIE[$this->_cookie_name]));

    		if ( count($data) ) {
	    		foreach( $data as $photo ) {
	    			if ( $photo_id == $photo[0] ) {
	    				return $photo[1];
	    			}
	    		}
    		}
    	}
    }

    public function _checkIsPhotoRatedByIp($photo_id, $ip)
    {
    	return self::db()->fetchOne('SELECT TRUE FROM escort_photo_votes WHERE photo_id = ? AND ip = ?', array($photo_id, $ip));
    }

    public function _ratePhoto($photo_id, $rate)
    {
    	if ( isset($_COOKIE[$this->_cookie_name]) ) {

    		if ( $this->_checkIsPhotoRated($photo_id) ) return false;

    		$data = json_decode(stripslashes($_COOKIE[$this->_cookie_name]));
			//var_dump($data);die;
    		$data[] = array($photo_id, $rate);

    		$data = json_encode($data);

    		setcookie($this->_cookie_name, $data, time() + (10 * 365 * 24 * 60 * 60), "/", "www." . preg_replace('#www.|dev.#', '', $_SERVER['HTTP_HOST']));
    		$_COOKIE[$this->_cookie_name] = $data;
    	} else {
    		$data[] = array($photo_id, $rate);

    		$data = json_encode($data);    		

    		setcookie($this->_cookie_name, $data, time() + (10 * 365 * 24 * 60 * 60), "/", "www." . preg_replace('#www.|dev.#', '', $_SERVER['HTTP_HOST']));
    		$_COOKIE[$this->_cookie_name] = $data;
    		//var_dump($_COOKIE[$this->_cookie_name]);die;
    	}
    }
}