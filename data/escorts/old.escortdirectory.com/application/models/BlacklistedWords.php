<?php

class Model_BlacklistedWords extends Cubix_Model
{

    public $message;
    public $email;
	
	const BL_TYPE_FEEDBACK = 1;			 //  000001
	const BL_TYPE_ABOUT = 2;		     //  000010
	const BL_TYPE_PRIVATE_MESSAGING = 4; //  000100
	const BL_TYPE_INSTANT_MESSAGES = 8;	 //  001000
	const BL_TYPE_BUBBLE_TEXT = 16;      //  010000 
	const BL_TYPE_CLASSIFIED_ADS = 32;   //  100000 
	
	const BL_SEARCH_TYPE_CONT = 1;
	const BL_SEARCH_TYPE_EXACT = 2;
	
    public function checkWords($text, $type = self::BL_TYPE_FEEDBACK)
	{
        $sql = '
			SELECT id, name, search_type
			FROM blacklisted_words
			WHERE types & ?';
        $res = array();
		$symbols = array("\r\n", "\n", "\r");
        $wordList = parent::_fetchAll($sql, array($type));
		$text = strtolower(str_replace($symbols, "",$text));

        if( count($wordList) > 0 ){
            foreach ( $wordList as $word ){
                if(strlen(trim($word->name)) > 0)
				{	
					$search_words = strtolower(str_replace( $symbols, "", $word->name));
					
					if($word->search_type == Model_BlacklistedWords::BL_SEARCH_TYPE_CONT){
						$result = strpos($text, $search_words);
					}
					else{
						$result = preg_match("/(\W+|^)".$search_words."(\W+|$)/", $text);
						$result = $result == 0 ? false : true; 
					}
					
					if ($result !== false) {

						$res[] = $word->name;

					}
				}
            }
			
            $this->message = implode(',', $res);
        }
        
        return $res;
    }

    public function getWords(){
        return $this->message;
    }

    public function checkEmail($email){
        $client = Cubix_Api::getInstance();

        $messageData = array(
            "email" => $email
        );
		
		return $client->call('checkEmail', array($messageData) );
    }

    public function getEmail(){
        return $this->email;
    }

    public function checkFeedback( $data ){
        $client = Cubix_Api::getInstance();

        $messageData = array(
            "email" => @$data['email'],
            "message" => @$data['message']
        );

		return $client->call('checkFeedback', array($messageData) );
    }

    public function mergeFeedbackData( $data ){
        try{
            $email = $data["email"];
            $message = $data["message"];

            $sql = '
                SELECT TRUE
                FROM blacklisted_emails
                WHERE name = "'.$email.'"';

            $res = parent::_fetchRow($sql);
            if ( !$res ){
                $blackData['name'] = $email;
                $this->_db->insert('blacklisted_emails', $blackData);
            }

            /*$sql = '
                SELECT TRUE
                FROM blacklisted_words
                WHERE name = "'.$message.'"';

            $res = parent::_fetchRow($sql);
            if ( !$res ){
                $blackData['name'] = $message;
                $this->_db->insert('blacklisted_words', $blackData);
            }*/
            return true;
        }  catch (Exception $ex){
            return false;
        }
           
    }
	
	public function checkMemberEmail($email){
        $client = Cubix_Api::getInstance();

        $messageData = array(
            "email" => $email
        );
		
		return $client->call('checkMemberEmail', array($messageData) );
    }

}
