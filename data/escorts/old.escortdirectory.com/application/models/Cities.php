<?php

class Model_Cities extends Cubix_Model 
{
	protected $_table = 'cities';

	public function getByCountry($id) 
	{
		$sql = '
			SELECT c.id, c.' . Cubix_I18n::getTblField('title') . ' as title
			FROM cities c
			WHERE country_id = ' . $id . '
			ORDER BY title
		';

		return parent::_fetchAll($sql);
	}

    public function getCountryIdById($id)
    {
        $sql = '
			SELECT country_id
			FROM cities c
			WHERE id = ' . $id . '
		';

        return parent::_fetchRow($sql);
    }

    public function getIdBySlug($slug)
    {
        $sql = '
			SELECT id
			FROM cities
			WHERE slug = "' . $slug . '"
		';

        return parent::_fetchRow( $sql );
    }

    public static function getCoordinatesById($id)
    {
        $sql = '
			SELECT id, latitude, longitude, country_id
			FROM cities
			WHERE id = ?
		';

        return self::db()->fetchRow($sql, $id);
    }
	
	public function getByCountries($ids) 
	{
		$ids_str = implode(',', $ids);
		$sql = '
			SELECT c.id, c.country_id, c.' . Cubix_I18n::getTblField('title') . ' as title
			FROM cities c
			WHERE country_id IN (' . $ids_str . ')
			ORDER BY title
		';

		return parent::_fetchAll($sql);
	}
	
	public function getByIds($ids)
	{
		return parent::_fetchAll('SELECT id, ' . Cubix_I18n::getTblField('title') . ' as title FROM cities WHERE id IN (' . $ids . ') ORDER BY title ASC');
	}

	public function getAll()
	{
		return parent::_fetchAll('SELECT id, ' . Cubix_I18n::getTblField('title') . ' as title FROM cities ORDER BY title ASC');
	}

	public function getByGeoIp()
	{
		$geoData = Cubix_Geoip::getClientLocation();

		if (!is_null($geoData))
		{
			// Hard code Exception for Hong Kong
			if ( $geoData['country'] == "Hong Kong" ) {
				//$geoData['country'] = "China";
				$geoData['city'] = "Hong Kong";
			}


			$log = false;

			/*if ( strlen($geoData['city']) )
			{
				$city = parent::getAdapter()->fetchRow('SELECT id, slug, country_id FROM cities WHERE title_geoip = ?', $geoData['city']);

				$coord = array(
					'lat' => $geoData['latitude'],
					'lon' => $geoData['longitude']
				);

				if ($city) {
					if ( $_GET['dd'] ) {
						var_dump('city');
					}
					return array('city_id' => $city->id, 'city_slug' => $city->slug, 'country_id' => $city->country_id);
				} else if ( $n_city = Model_Statistics::getNearestCitiesByCoordinates($coord) ) {
					if ( $_GET['dd'] ) {
						var_dump('n_city');
						var_dump('n_city');
					}
					return array('city_id' => $n_city->city_id, 'city_slug' => $n_city->city_slug, 'country_id' => $n_city->country_id);
				} else {
					$log = true;
				}

			} else*/
			if ( strlen($geoData['country']) ) {

				$country = parent::getAdapter()->fetchRow('SELECT id, slug FROM countries WHERE title_geoip = ?', $geoData['country']);

				if ($country) {

					$city_title = null;
					if ( strlen($geoData['city']) ) {
						$city_title = parent::getAdapter()->fetchOne('SELECT title_en FROM cities WHERE title_geoip = ?', $geoData['city']);
					}

					return array('city_id' => null, 'city_title' => $city_title, 'city_slug' => null, 'country_id' => $country->id, 'country_slug' => $country->slug);
				} else {
					$log = true;
				}

			} else {
				$log = false;
			}

			if ($log)
			{
				//$str = date('d M, Y H:i:s') . ' - City: ' . $geoData['city'] . ' - ' . var_export($geoData, true) . "\n";
				//file_put_contents('/var/log/geoip_missing_cities.log', $str, FILE_APPEND);

				$geoData['city'] = utf8_encode($geoData['city']);

				$c = parent::getAdapter()->fetchOne('SELECT COUNT(*) FROM geoip_missing_cities WHERE city = ?', $geoData['city']);

				if ($c == 0)
					parent::getAdapter()->insert('geoip_missing_cities', array(
						'ip' => $geoData['ip'],
						'country' => $geoData['country'],
						'country_iso' => $geoData['country_iso'],
						'region' => $geoData['region'],
						'city' => $geoData['city'],
						'latitude' => $geoData['latitude'],
						'longitude' => $geoData['longitude']
					));

				return null;
			}
		}
		else
			return null;
	}
	
	public function getById($id)
	{
		return $this->db()->query('SELECT c.*, ' . Cubix_I18n::getTblField('c.title') . ' as title, ' . Cubix_I18n::getTblField('co.title') . ' as country_title FROM cities c INNER JOIN countries co ON co.id = c.country_id WHERE c.id = ?', $id)->fetch();
	}
}
