<?php

class Model_Links extends Cubix_Model
{
	public static  function getCurrentUrlForLang( $lang )
	{
        $_lang_path = ( $lang == 'en' ) ? '' : '/' . $lang;
        $pageURL = 'http';
        if ($_SERVER["HTTPS"] == "on") { $pageURL .= "s"; }

        $pageURL .= "://";

//        if ($_SERVER["SERVER_PORT"] != "80") {
//            $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_lang_path . $_SERVER["REQUEST_URI"];
//        } else {
//            $pageURL .= $_SERVER["SERVER_NAME"] . $_lang_path . $_SERVER["REQUEST_URI"];
//        }

        $pageURL .= $_SERVER["SERVER_NAME"] . $_lang_path . str_replace( ( Cubix_I18n::getLang() != 'en' ) ? Cubix_I18n::getLang() . '/' : '', '', $_SERVER["REQUEST_URI"] );

        return $pageURL;
	}
}
