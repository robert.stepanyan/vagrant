<?php

class Model_Statistics extends Cubix_Model
{
	public function getEDCitiesByCountry($gender = null, $is_agency = null, $is_tour = null, $is_upcoming = null, $limit = null, $geo_city = array(), $city_id = null, $country_id = null, $is_new = false)
	{
		$where = array(

			'eic.is_agency = ?' => $is_agency,
			'eic.is_tour = ?' => $is_tour,
			'eic.is_upcoming = ?' => $is_upcoming
		);

		if ( is_array($gender) ) {
			$where['eic.gender = ? OR eic.gender = ?'] = $gender;//'2';//implode(',', $gender);
		} else {
			if ( ! is_null($gender) ) {
				$where['eic.gender = ?'] = (int)$gender;
			}
		}

		if ( $is_new ) {
			$where['e.is_new = ?'] = 1;
		}

		if ( ! $is_tour && ! $is_upcoming ) {
			//$where['(e.agency_id IS NULL OR (e.show_agency_escorts = 1 OR eic.is_premium = 1))'] = array();
		}

		$where = self::getWhereClause($where, true);

		if ( $is_new ) {
			if ( is_null($is_agency) ) {
				$where .= " AND e.gender = 1 /*AND eic.is_base = 1*/ AND (e.agency_id IS NULL OR (e.show_agency_escorts = 1 OR eic.is_premium = 1)) ";
			} elseif ( $is_agency == 0 ) {
				$where .= " AND eic.is_agency = 0 AND eic.gender = 1 /*AND eic.is_base = 1*/ AND (e.agency_id IS NULL OR (e.show_agency_escorts = 1 OR eic.is_premium = 1)) ";
			} elseif ( $is_agency == 1 ) {
				$where .= " AND eic.is_agency = 1 AND eic.gender = 1 /*AND eic.is_base = 1*/ AND (e.agency_id IS NULL OR (e.show_agency_escorts = 1 OR eic.is_premium = 1)) ";
			}			
		}

		$lng = Cubix_I18n::getLang();

		$geoData = Cubix_Geoip::getClientLocation();
		if( ! is_array($geoData) ) {
			$geoData = array();
		}		
		$fields = ' NULL AS distance ';
		if (array_key_exists("latitude", $geoData) && strlen($geoData['latitude']) > 0 && array_key_exists("longitude", $geoData) && strlen($geoData['longitude']) > 0) {
			$fields = '
				((2 * 6371 *
					ATAN2(
					SQRT(
						POWER(SIN((RADIANS(' . $geoData['latitude'] . ' - ct.latitude))/2), 2) +
						COS(RADIANS(ct.latitude)) *
						COS(RADIANS(' . $geoData['latitude'] . ')) *
						POWER(SIN((RADIANS(' . $geoData['longitude'] . ' - ct.longitude))/2), 2)
					),
					SQRT(1-(
						POWER(SIN((RADIANS(' . $geoData['latitude'] . ' - ct.latitude))/2), 2) +
						COS(RADIANS(ct.latitude)) *
						COS(RADIANS(' . $geoData['latitude'] . ')) *
						POWER(SIN((RADIANS(' . $geoData['longitude'] . ' - ct.longitude))/2), 2)
					))
					)
				)) AS distance
			';
		}
		

		$sql = '
			SELECT
				eic.city_id, COUNT(DISTINCT(eic.escort_id)) AS escort_count,
				ct.title_' . $lng . ' AS city_title, ct.slug AS city_slug,
				ct.country_id, c.title_' . $lng . ' AS country_title,
				c.iso AS country_iso, ' . $fields . '
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			INNER JOIN countries c ON c.id = ct.country_id
			' . (!is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.city_id
		';

		if ( array_key_exists("latitude", $geoData) && strlen($geoData['latitude']) > 0 ) {
			$sql .= ' ORDER BY distance ASC';
		} else if ( $city_id ) {
			$sql .= ' ORDER BY FIELD(ct.id, "' . $city_id . '") DESC, ct.title_' . $lng;
		} else {
			$sql .= ' ORDER BY ct.title_' . $lng . ' ASC';
		}

		/*if ( isset($geo_city['city_id']) ) {
			$sql .= ' ORDER BY FIELD(ct.id, ' . $geo_city['city_id'] . ') DESC, ct.title_' . $lng;
		} else if ( $city_id ) {
			$sql .= ' ORDER BY FIELD(ct.id, "' . $city_id . '") DESC, ct.title_' . $lng;
		} else {
			$sql .= ' ORDER BY ct.title_' . $lng . ' ASC';
		}*/


		$this->getAdapter()->query('SET NAMES `utf8`');
		$cities = $this->getAdapter()->fetchAll($sql);


		// divide cities array into array of arrays by city's country id
		$result = array();
		$total_escorts_count = 0;
		foreach ( $cities as $city ) {

			$total_escorts_count = $total_escorts_count + $city->escort_count;

			if ( ! isset($result[$city->country_id]) )
				$result[$city->country_id] = array();

			$result[$city->country_id][] = $city;
		}

		// find cities for each country from $result array
		$countries = $this->getEDCountries($gender, $is_agency, $is_tour, $is_upcoming, $limit, $geo_city, $city_id, $country_id, $is_new);


		foreach ( $countries as $i => $country ) {
			if ( isset($result[$country->country_id])) {
				$countries[$i]->cities = $result[$country->country_id];
				//var_dump($result[$country->country_id]);
			}
		}

		return array('data' => $countries, 'total_escorts_count' => $total_escorts_count);
	}

	public function getEDCountries($gender = null, $is_agency = null, $is_tour = null, $is_upcoming = null, $limit = null, $geo_city = array(), $city_id = null, $country_id = null, $is_new = false)
	{
		if ( $city_id ) {
			$city = $this->getAdapter()->fetchRow('SELECT id, country_id FROM cities WHERE id = ?', $city_id);
		}

		$where = array(
			'eeic.is_agency = ?' => $is_agency,
			'eeic.is_tour = ?' => $is_tour,
			'eeic.is_upcoming = ?' => $is_upcoming
		);

		// limit and filter for #YOU ARE HERE# one row
		if ( $limit ) {
			if ( $city_id ) {
				$where['c.id = ?'] = $city->country_id;
			} else if ( $country_id ) {
				$where['c.id = ?'] = $country_id;
			} else if ( isset($geo_city['country_id']) && $geo_city['country_id'] ) {
				$where['c.id = ?'] = $geo_city['country_id'];
			}
		}

		if ( $is_new ) {
			$where['e.is_new = ?'] = 1;
		}

		if ( is_array($gender) ) {
			$where['(eeic.gender = ? OR eeic.gender = ?)'] = $gender;//'2';//implode(',', $gender);
		} else {
			if ( ! is_null($gender) ) {
				$where['eeic.gender = ?'] = (int)$gender;
			}
		}

		if ( ! $is_tour && ! $is_upcoming ) {
			//$where['(e.agency_id IS NULL OR (e.show_agency_escorts = 1 OR eeic.is_premium = 1))'] = array();
		}

		$where = self::getWhereClause($where, true);

		if ( $is_new ) {
			if ( is_null($is_agency) ) {
				$where .= " AND e.gender = 1 /*AND eeic.is_base = 1*/ AND (e.agency_id IS NULL OR (e.show_agency_escorts = 1 OR eeic.is_premium = 1)) ";
			} elseif ( $is_agency == 0 ) {
				$where .= " AND eeic.is_agency = 0 AND eeic.gender = 1 /*AND eeic.is_base = 1*/ AND (e.agency_id IS NULL OR (e.show_agency_escorts = 1 OR eeic.is_premium = 1)) ";
			} elseif ( $is_agency == 1 ) {
				$where .= " AND eeic.is_agency = 1 AND eeic.gender = 1 /*AND eeic.is_base = 1*/ AND (e.agency_id IS NULL OR (e.show_agency_escorts = 1 OR eeic.is_premium = 1)) ";
			}			
		}

		$lng = Cubix_I18n::getLang();

		$sql = '
			SELECT
				eic.country_id, COUNT(DISTINCT(eeic.escort_id)) AS escort_count,
				c.title_' . $lng . ' AS country_title, c.iso, c.slug AS country_slug
			FROM escorts_in_cities eeic
			INNER JOIN escorts e ON e.id = eeic.escort_id
			INNER JOIN cities cc ON cc.id = eeic.city_id
			INNER JOIN escorts_in_countries eic ON cc.country_id = eic.country_id
			INNER JOIN countries c ON c.id = eic.country_id
			' . (!is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY c.id';


		if ( isset($geo_city['country_id']) ) {
			$sql .= ' ORDER BY FIELD(c.id, ' . $geo_city['country_id'] . ') DESC, c.title_' . $lng;
		} else if ( $city_id ) {
			$sql .= ' ORDER BY FIELD(c.id, ' . $city->country_id . ') DESC, c.title_' . $lng;
		} else if ( $country_id ) {
			$sql .= ' ORDER BY FIELD(c.id, ' . $country_id . ') DESC, c.title_' . $lng;
		} else {
			$sql .= ' ORDER BY c.title_' . $lng;
		}

		if ( $limit ) {
			$sql .= ' LIMIT ' . $limit;
		}

		$this->getAdapter()->query('SET NAMES `utf8`');
		$result = $this->getAdapter()->fetchAll($sql);


		return $result;
	}

	public function getCountryUpdatedOrNewEscorts($gender = null, $is_agency = null, $is_tour = null, $is_upcoming = null, $type = 'new', $is_new = false) {

		$use_visit_date = $_COOKIE['last_visit_date'];

		if ( ! is_numeric($use_visit_date) && ! $use_visit_date ) return array();

		$where = array(
			'eeic.is_agency = ?' => $is_agency,
			'eeic.is_tour = ?' => $is_tour,
			'eeic.is_upcoming = ?' => $is_upcoming
		);

		if ( is_array($gender) ) {
			$where['(eeic.gender = ? OR eeic.gender = ?)'] = $gender;//'2';//implode(',', $gender);
		} else {
			if ( ! is_null($gender) ) {
				$where['eeic.gender = ?'] = (int)$gender;
			}
		}

		if ( ! $is_tour && ! $is_upcoming ) {
			//$where['(e.agency_id IS NULL OR (e.show_agency_escorts = 1 OR eeic.is_premium = 1))'] = array();
		}

		if ( $type == 'new' ) {
			$where['UNIX_TIMESTAMP(e.status_active_date) > ' . $use_visit_date] = array();
			$where['e.is_new = 1'] = array();
		} elseif ($type == 'updated') {
			$where['UNIX_TIMESTAMP(e.date_last_modified) > ' . $use_visit_date] = array();
			$where['e.is_new <> 1'] = array();
		}

		$where = self::getWhereClause($where, true);

		if ( $is_new ) {
			if ( is_null($is_agency) ) {
				$where .= " AND e.gender = 1  AND (e.agency_id IS NULL OR (e.show_agency_escorts = 1 OR eeic.is_premium = 1)) ";
			} elseif ( $is_agency == 0 ) {
				$where .= " AND eeic.is_agency = 0 AND eeic.gender = 1 AND (e.agency_id IS NULL OR (e.show_agency_escorts = 1 OR eeic.is_premium = 1)) ";
			} elseif ( $is_agency == 1 ) {
				$where .= " AND eeic.is_agency = 1 AND eeic.gender = 1 AND (e.agency_id IS NULL OR (e.show_agency_escorts = 1 OR eeic.is_premium = 1)) ";
			}			
		}

		$sql = '
			SELECT
				c.id AS country_id, COUNT(eeic.escort_id) AS escorts_count		
			FROM escorts_in_cities eeic
			INNER JOIN escorts e ON e.id = eeic.escort_id
			INNER JOIN cities cc ON cc.id = eeic.city_id			
			INNER JOIN countries c ON c.id = cc.country_id
			' . (!is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY c.id
		';

		$res = $this->getAdapter()->fetchAll($sql);

		$result = array();
		if ( count($res) ) {
			foreach( $res as $r ) {
				$result[$r->country_id] = $r->escorts_count;
			}
		}

		return $result;
	}

	public function getCityUpdatedOrNewEscorts($gender = null, $is_agency = null, $is_tour = null, $is_upcoming = null, $type = 'new') {

		$use_visit_date = $_COOKIE['last_visit_date'];

		if ( ! is_numeric($use_visit_date) && ! $use_visit_date ) return array();

		$where = array(
			'eeic.is_agency = ?' => $is_agency,
			'eeic.is_tour = ?' => $is_tour,
			'eeic.is_upcoming = ?' => $is_upcoming
		);

		if ( is_array($gender) ) {
			$where['(eeic.gender = ? OR eeic.gender = ?)'] = $gender;//'2';//implode(',', $gender);
		} else {
			if ( ! is_null($gender) ) {
				$where['eeic.gender = ?'] = (int)$gender;
			}
		}

		if ( ! $is_tour && ! $is_upcoming ) {
			$where['(e.agency_id IS NULL OR (e.show_agency_escorts = 1 OR eeic.is_premium = 1))'] = array();
		}

		if ( $type == 'new' ) {
			$where['UNIX_TIMESTAMP(e.status_active_date) > ' . $use_visit_date] = array();
			$where['e.is_new = 1'] = array();
		} elseif ($type == 'updated') {
			$where['UNIX_TIMESTAMP(e.date_last_modified) > ' . $use_visit_date] = array();
			$where['e.is_new <> 1'] = array();
		}

		$where = self::getWhereClause($where, true);

		$sql = '
			SELECT
				ct.id AS city_id, COUNT(DISTINCT(eeic.escort_id)) AS escorts_count
			FROM escorts_in_cities eeic
			INNER JOIN escorts e ON e.id = eeic.escort_id
			INNER JOIN cities ct ON ct.id = eeic.city_id
			INNER JOIN countries c ON c.id = ct.country_id
			' . (!is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY ct.id
		';

		$res = $this->getAdapter()->fetchAll($sql);

		$result = array();
		if ( count($res) ) {
			foreach( $res as $r ) {
				$result[$r->city_id] = $r->escorts_count;
			}
		}

		return $result;
	}







	public function getEDCitiesByCountryForAgencies($limit = null, $geo_city = array(), $city_id = null, $country_id = null)
	{

		$lng = Cubix_I18n::getLang();

		$sql = '
			SELECT
				cd.city_id, COUNT(DISTINCT(cd.id)) AS escort_count,
				ct.title_' . $lng . ' AS city_title, ct.slug AS city_slug,
				ct.country_id, c.title_' . $lng . ' AS country_title,
				c.iso AS country_iso
			FROM club_directory cd
			INNER JOIN cities ct ON ct.id = cd.city_id
			INNER JOIN countries c ON c.id = ct.country_id
			GROUP BY cd.city_id
		';

		if ( isset($geo_city['city_id']) ) {
			$sql .= ' ORDER BY FIELD(ct.id, ' . $geo_city['city_id'] . ') DESC, ct.title_' . $lng;
		} else if ( $city_id ) {
			$sql .= ' ORDER BY FIELD(ct.id, "' . $city_id . '") DESC, ct.title_' . $lng;
		} else {
			$sql .= ' ORDER BY ct.title_' . $lng . ' ASC';
		}

		$cities = $this->getAdapter()->fetchAll($sql);

		// divide cities array into array of arrays by city's country id
		$result = array();
		foreach ( $cities as $city ) {
			if ( ! isset($result[$city->country_id]) )
				$result[$city->country_id] = array();

			$result[$city->country_id][] = $city;
		}

		// find cities for each country from $result array
		$countries = $this->getEDCountriesForAgencies($limit, $geo_city, $city_id, $country_id);


		foreach ( $countries as $i => $country ) {
			if ( isset($result[$country->country_id])) {
				$countries[$i]->cities = $result[$country->country_id];

			}
		}

		return $countries;
	}

	public function getEDCountriesForAgencies($limit = null, $geo_city = array(), $city_id = null, $country_id = null)
	{
		if ( $city_id ) {
			$city = $this->getAdapter()->fetchRow('SELECT id, country_id FROM cities WHERE id = ?', $city_id);
		}

		$lng = Cubix_I18n::getLang();

		$where = array();

		// limit and filter for #YOU ARE HERE# one row
		if ( $limit ) {
			if ( $city_id ) {
				$where['c.id = ?'] = $city->country_id;
			} else if ( $country_id ) {
				$where['c.id = ?'] = $country_id;
			}
		}

		$where = self::getWhereClause($where, true);

		$sql = '
			SELECT
				cc.country_id, COUNT(DISTINCT(cd.id)) AS escort_count,
				c.title_' . $lng . ' AS country_title, c.iso, c.slug AS country_slug
			FROM club_directory cd
			INNER JOIN cities cc ON cc.id = cd.city_id
			INNER JOIN countries c ON c.id = cc.country_id
			' . (!is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY c.id';

		if ( isset($geo_city['country_id']) ) {
			$sql .= ' ORDER BY FIELD(c.id, ' . $geo_city['country_id'] . ') DESC, c.title_' . $lng;
		} else if ( $city_id ) {
			$sql .= ' ORDER BY FIELD(c.id, ' . $city->country_id . ') DESC, c.title_' . $lng;
		} else if ( $country_id ) {
			$sql .= ' ORDER BY FIELD(c.id, ' . $country_id . ') DESC, c.title_' . $lng;
		} else {
			$sql .= ' ORDER BY c.title_' . $lng;
		}

		if ( $limit ) {
			$sql .= ' LIMIT ' . $limit;
		}

		$result = $this->getAdapter()->fetchAll($sql);


		return $result;
	}








	public static function getTotalCount($gender = null, $is_agency = null, $is_tour = null, $is_upcoming = null)
	{
		$where = array(
			'eic.gender = ?' => $gender,
			'eic.is_agency = ?' => $is_agency,
			'eic.is_tour = ?' => $is_tour,
			'eic.is_upcoming = ?' => $is_upcoming
		);

		$where = self::getWhereClause($where, true);

		$lng = Cubix_I18n::getLang();
		$sql = '
			SELECT
				COUNT(DISTINCT(eic.escort_id)) AS escort_count
			FROM escorts_in_cities eic
			/*INNER JOIN cities ct ON ct.id = eic.city_id
			INNER JOIN regions r ON r.id = ct.region_id*/
			' . (!is_null($where) ? 'WHERE ' . $where : '') . '
		';

		return self::db()->fetchOne($sql);
	}

	static public function getCountries($gender = null, $is_agency = null, $is_tour = null, $is_upcoming = null, $limit = null, $country_title = null)
	{
		$lng = Cubix_I18n::getLang();

		$where = array(
			'eeic.gender = ?' => $gender,
			'eeic.is_agency = ?' => $is_agency,
			'eeic.is_tour = ?' => $is_tour,
			'eeic.is_upcoming = ?' => $is_upcoming,
			'c.title_' . $lng . ' LIKE ?' => $country_title . '%'
		);

		$where = self::getWhereClause($where, true);

		$sql = '
			SELECT
				eic.country_id, COUNT(DISTINCT(eeic.escort_id)) AS escort_count,
				c.title_' . $lng . ' AS country_title, c.iso, c.slug AS country_slug
			FROM escorts_in_cities eeic
			INNER JOIN cities cc ON cc.id = eeic.city_id
			INNER JOIN escorts_in_countries eic ON cc.country_id = eic.country_id
			INNER JOIN countries c ON c.id = eic.country_id
			' . (!is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY c.id
			ORDER BY c.title_' . $lng . '
		';

		if ( $limit ) {
			$sql .= ' LIMIT ' . $limit;
		}

		$result = self::db()->fetchAll($sql);
		//usort($result, array('Model_Statistics', '_orderByEscortCount'));

		return $result;
	}

	public static function getCities($region_id = null, $gender = null, $is_agency = null, $is_tour = null, $is_upcoming = null,$wheretmp = null, $limit = null, $city_title = null)
	{
		$lng = Cubix_I18n::getLang();

		$where = array(
			'eic.region_id = ?' => $region_id,
			'eic.gender = ?' => $gender,
			'eic.is_agency = ?' => $is_agency,
			'eic.is_tour = ?' => $is_tour,
			'eic.is_upcoming = ?' => $is_upcoming,
			'ct.title_' . $lng . ' LIKE ?' => $city_title . '%'
		);

        if($wheretmp){
            $where = array_merge($wheretmp,$where);
        }

		$where = self::getWhereClause($where, true);


		$sql = '
			SELECT
				eic.city_id, COUNT(DISTINCT(eic.escort_id)) AS escort_count,
				ct.title_' . $lng . ' AS city_title, ct.slug AS city_slug,
				r.title_' . $lng . ' AS region_title, c.iso AS country_iso,
				c.title_' . $lng . ' AS country_title
			FROM escorts_in_cities eic
			INNER JOIN cities ct ON ct.id = eic.city_id
			LEFT JOIN regions r ON r.id = ct.region_id
			INNER JOIN countries c ON c.id = ct.country_id
			' . (!is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.city_id
		';
		
		if ( $limit ) {
			$sql .= ' LIMIT ' . $limit;
		}

		$result = self::db()->fetchAll($sql);
		usort($result, array('Model_Statistics', '_orderByEscortCount'));

		return $result;
	}

	public static function getNearestCitiesByCoordinates($coord = array(), $limit = 1, $exclude_city_id = null)
	{
		$lng = Cubix_I18n::getLang();

		$fields = '';

		$where = "";
		if ( $exclude_city_id ) {
			$where = " AND c.id <> " . $exclude_city_id;
			$country_id = Model_Countries::getCountryIdByCityId($exclude_city_id);
			$where .= " AND cr.id = " . $country_id;
		}

		if (array_key_exists("lat", $coord) && strlen($coord['lat']) > 0 && array_key_exists("lon", $coord) && strlen($coord['lon']) > 0)
			$fields = '
				((2 * 6371 *
					ATAN2(
					SQRT(
						POWER(SIN((RADIANS(' . $coord['lat'] . ' - c.latitude))/2), 2) +
						COS(RADIANS(c.latitude)) *
						COS(RADIANS(' . $coord['lat'] . ')) *
						POWER(SIN((RADIANS(' . $coord['lon'] . ' - c.longitude))/2), 2)
					),
					SQRT(1-(
						POWER(SIN((RADIANS(' . $coord['lat'] . ' - c.latitude))/2), 2) +
						COS(RADIANS(c.latitude)) *
						COS(RADIANS(' . $coord['lat'] . ')) *
						POWER(SIN((RADIANS(' . $coord['lon'] . ' - c.longitude))/2), 2)
					))
					)
				)) AS distance
			';
		else
			$fields = 'NULL AS distance ';

		$sql = '
			SELECT
				c.id AS city_id, c.title_' . $lng . ' AS city_title, c.slug AS city_slug, ' . $fields . ',
				cr.id AS country_id, cr.title_' . $lng . ' AS country_title
			FROM cities c
			INNER JOIN escorts_in_cities eic ON eic.city_id = c.id
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN countries cr ON cr.id = c.country_id
			WHERE c.latitude IS NOT NULL AND c.longitude IS NOT NULL ' . $where . '
			GROUP BY eic.escort_id
			ORDER BY distance
		';

		if ( $limit ) {
			$sql .= ' LIMIT ' . $limit;
		}

		$result = self::db()->fetchAll($sql);

		return $result;
	}

	public static function getCitiesByCountry($gender = null, $is_agency = null, $is_tour = null, $is_upcoming = null)
	{
		$where = array(
			'eic.gender = ?' => $gender,
			'eic.is_agency = ?' => $is_agency,
			'eic.is_tour = ?' => $is_tour,
			'eic.is_upcoming = ?' => $is_upcoming
		);

		$where = self::getWhereClause($where, true);

		$lng = Cubix_I18n::getLang();
		$sql = '
			SELECT
				eic.city_id, COUNT(DISTINCT(eic.escort_id)) AS escort_count,
				ct.title_' . $lng . ' AS city_title, ct.slug AS city_slug,
				r.title_' . $lng . ' AS region_title, ct.country_id, c.title_' . $lng . ' AS country_title
			FROM escorts_in_cities eic
			INNER JOIN cities ct ON ct.id = eic.city_id
			LEFT JOIN regions r ON r.id = ct.region_id
			INNER JOIN countries c ON c.id = ct.country_id
			' . (!is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.city_id
		';

		$cities = self::db()->fetchAll($sql);

		// divide cities array into array of arrays by city's country id
		$result = array();
		foreach ( $cities as $city ) {
			if ( ! isset($result[$city->country_id]) )
				$result[$city->country_id] = array();
			$result[$city->country_id][] = $city;
		}

		// find cities for each country from $result array
		$countries = self::getCountries($gender, $is_agency, $is_tour, $is_upcoming);

		foreach ( $countries as &$country ) {
			if ( isset($result[$country->country_id])) {
				$country->cities = $result[$country->country_id];
			}
		}

		return $countries;
	}

	public static function getRegions($gender = null, $is_agency = null, $is_tour = null, $is_upcoming = null)
	{
		$where = array(
			'eic.gender = ?' => $gender,
			'eic.is_agency = ?' => $is_agency,
			'eic.is_tour = ?' => $is_tour,
			'eic.is_upcoming = ?' => $is_upcoming
		);

		$where = self::getWhereClause($where, true);

		$lng = Cubix_I18n::getLang();
		$sql = '
			SELECT
				COUNT(DISTINCT(eic.escort_id)) AS escort_count,
				r.title_' . $lng . ' AS region_title, r.slug AS region_slug
			FROM escorts_in_cities eic
			INNER JOIN cities ct ON ct.id = eic.city_id
			INNER JOIN regions r ON r.id = ct.region_id
			' . (!is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.region_id;
		';

		$result = self::db()->fetchAll($sql);
		usort($result, array('Model_Statistics', '_orderByEscortCount'));

		return $result;
	}

	public static function getZones($city_id = null, $gender = null, $is_agency = null, $is_tour = null, $is_upcoming = null)
	{
		$where = array(
			'eic.city_id = ?' => $city_id,
			'eic.gender = ?' => $gender,
			'eic.is_agency = ?' => $is_agency,
			'eic.is_tour = ?' => $is_tour,
			'eic.is_upcoming = ?' => $is_upcoming
		);

		$where = self::getWhereClause($where, true);

		$lng = Cubix_I18n::getLang();
		$sql = '
			SELECT
				COUNT(DISTINCT(eic.escort_id)) AS escort_count,
				ct.title_' . $lng . ' AS city_title, ct.slug AS city_slug,
				cz.title_' . $lng . ' AS zone_title, cz.slug AS zone_slug
			FROM escorts_in_cityzones eic
			INNER JOIN cityzones cz ON cz.id = eic.cityzone_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			' . (!is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.cityzone_id;
		';

		$result = self::db()->fetchAll($sql);

		return $result;
	}

	public static function _orderByEscortCount($a, $b)
	{
		$a = (int) $a->escort_count; $b = (int) $b->escort_count;
		if ( $a == $b ) return 0;
		return $a < $b ? 1 : -1;
	}

	public static function ajaxSearchArray()
	{
		
	}
	
	public static function getCountryByCitySlug($city_slug)
	{
		$country_id = self::db()->fetchOne('
			SELECT co.id FROM countries co
			INNER JOIN cities c ON c.country_id = co.id
			WHERE c.slug = ?
		', array($city_slug));

		return $country_id;
	}
}
