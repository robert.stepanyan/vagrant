<?php

class Model_Glossary extends Cubix_Model
{
	public function getAll($letter, $search)
	{
		$where = '';

		if (strlen($letter) == 1)
			$where .= ' AND abbr LIKE "' . $letter . '%" ';
		else
		{
			if ($letter == 'others')
			{
				$where .= " AND abbr NOT REGEXP '^[A-Za-z]+' ";
			}
		}

		if ($search)
			$where .= ' AND ' . Cubix_I18n::getTblField('value') . ' LIKE "%' . $search . '%" ';

		$sql = '
			SELECT abbr, ' . Cubix_I18n::getTblField('value') . ' AS value
			FROM glossary
			WHERE 1 ' . $where . '
			ORDER BY abbr asc
		';

		return $this->getAdapter()->query($sql)->fetchAll();
	}
}
