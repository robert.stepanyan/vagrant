<?php

class Model_Escort_List extends Cubix_Model
{
	public static function getMainPremiumSpot()
	{
		$lng = Cubix_I18n::getLang();

		if (date('G') < 2)
			$date = 2;
		else
			$date = date('G');

		$sql = '
			SELECT
				/* escorts table */
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id,
				e.rates, e.incall_type, e.outcall_type, e.is_new, e.verified_status,
				e.photo_hash, e.photo_ext, e.photo_status, e.is_pornstar, e.last_hand_verification_date,
				e.products, e.slogan, e.date_last_modified,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.is_suspicious,e.comment_count, e.review_count,
				
				ct.title_' . $lng . ' AS city,

				/* seo_entity_instances table */
				IF (sei.primary_id IS NULL, se.title_' . $lng . ', sei.title_' . $lng . ') AS alt,

				IF (e.available_24_7 = 1 OR (e.is_now_open = 1 AND e.close_time >= "' . $date . '"), 1, 0) AS is_late_night,
				/* misc */
				1 AS is_premium, ' . Cubix_Application::getId() . ' AS application_id, e.hh_is_active, e.is_online
			FROM escorts e
			INNER JOIN escorts_in_cities eic ON eic.escort_id = e.id AND eic.is_base = 1
			INNER JOIN cities ct ON ct.id = eic.city_id
			LEFT JOIN seo_entity_instances sei ON sei.primary_id = e.id
			LEFT JOIN seo_entities se ON (se.id = sei.entity_id) OR (se.application_id = 2 AND se.slug = "escort")
			WHERE
				eic.gender = ' . GENDER_FEMALE . ' AND e.is_main_premium_spot = 1
			GROUP BY e.id
		';

		$cache = Zend_Registry::get('cache');
		$cache_key = 'main_premium_spot_' . Cubix_Application::getId() . '_lang_' . Cubix_I18n::getLang();

		if ( ! $escorts = $cache->load($cache_key) ) {
			
			$escorts = self::db()->fetchAll($sql);
			
			/* seo heading escorts */
			if ($escorts)
			{
				$es = array();
				foreach ($escorts as $e)
				{
					$es[] = $e->id;
				}
				$es_str = implode(',', $es);

				$query = "SELECT e.id, sei.heading_escort_{$lng} as heading_escort
					FROM escorts e
					INNER JOIN escorts_in_cities eic ON eic.escort_id = e.id AND eic.is_base = 1
					INNER JOIN seo_entity_instances sei ON sei.primary_id = eic.city_id
					INNER JOIN seo_entities se ON se.application_id = " . Cubix_Application::getId() . " AND se.slug = 'city' AND sei.entity_id = se.id
					WHERE e.id IN ({$es_str})
				";
				$result = self::db()->fetchAll($query);

				foreach ($result as $r)
				{				
					foreach ($escorts as $e)
					{
						if ($e->id == $r->id)
						{
							$e->heading_escort = $r->heading_escort;
							break;
						}
					}
				}
			}
			/**/

			$cache->save($escorts, $cache_key, array());
		}

		// <editor-fold defaultstate="collapsed" desc="Make Escort Alt for SEO">
		$tpl_data = array(
			'app_title' => Cubix_Application::getById()->title
		);

		foreach ( $escorts as $i =>  $escort ) {
			$additional = array(
				'showname' => $escort->showname,
				'city' => $escort->city
			);

			foreach ( $tpl_data + $additional as $tpl_var => $tpl_value ) {
				$escorts[$i]->alt = str_replace('%' . $tpl_var . '%', $tpl_value, $escorts[$i]->alt);
			}
		}
		// </editor-fold>

		shuffle($escorts);

		// <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
		$sid = 'sedcard_paging_' . Cubix_Application::getId();
		$sess = new Zend_Session_Namespace($sid);

		$sess->main_premium_spot = array();
		foreach ( $escorts as $escort ) {
			$sess->main_premium_spot[] = $escort->showname;
		}
		// </editor-fold>


		return $escorts;
	}

	public static function getForMainPage($filter, $ordering = 'random', $page = 1, $page_size = 50, &$count = 0, $sess_name = false, $only_vip = false, $s_filter_params = array(), $is_grouped = false, $geo_data = array(), $list_type = 'simple', $p_top_category = 'escorts', $city_id = null, $country_id = null)
	{
		$lng = Cubix_I18n::getLang();

		if (date('G') < 2)
			$date = 2;
		else
			$date = date('G');

		if ( 'price' == substr($ordering, 0, 5) ) {
			$filter[] = 'e.incall_price IS NOT NULL';
		}

		if ( isset($s_filter_params['gender']) && count($s_filter_params['gender']) ) {
			$filter[] = 'e.gender IN ( ' . implode(',', $s_filter_params['gender']) . ' )';
		}

		if ( isset($s_filter_params['service']) && count($s_filter_params['service']) ) {
			$filter[] = 'es.service_id IN ( ' . implode(',', $s_filter_params['service']) . ' )';
		}
		
		// Only escorts with base city (or city tour) will be shown in this list
		if ( ! isset($filter['ct.slug = ?']) && ! isset($filter['r.slug = ?']) ) {
			$filter[] = 'eic.is_base = 1';
		}
		else {
			$filter[] = 'eic.is_upcoming = 0';
		}

		$show_splited_agency_escorts = true;
		if ( isset($filter['show_all_agency_escorts']) ) {
			unset($filter['show_all_agency_escorts']);
			$show_splited_agency_escorts = false;
		}

		if ( $show_splited_agency_escorts ) {
			$filter[] = '(e.agency_id IS NULL OR (e.show_agency_escorts = 1 OR eic.is_premium = 1))';
		}

		$where = self::getWhereClause($filter, true);
		$order = self::_mapSorting($ordering, $is_grouped, $list_type);
		$ag_order = self::_mapSortingAgencies($ordering);

		$page = (int) $page;
		if ( $page < 1 ) $page = 1;
		$limit = ($page_size * ($page - 1)) . ', ' . $page_size;
		
		$j = "";
		$f = "";
		if ( isset($filter['f.user_id = ?']) ) {
			$j = " INNER JOIN favorites f ON f.escort_id = e.id ";
			$f = " , f.user_id AS fav_user_id ";
		}

		$group_by = " GROUP BY eic.escort_id ";

		$ag_where = " WHERE 1 ";

		if ( $city_id ) {
			$ag_where .= " AND cd.city_id = " . (int) $city_id;
		}

		if ( $country_id ) {
			$ag_where .= " AND cd.country_id = " . (int) $country_id;
		}

		/* IF AGENCY GROUP BY AGENCY ID FOR MIXED VIEW */
		/*if ( ! isset($filter['eic.is_agency = 1']) ) {
			$j .= " LEFT JOIN club_directory cd ON cd.agency_id = e.agency_id ";
		} else if ( isset($filter['eic.is_agency = 1']) ) {
			$j .= " INNER JOIN club_directory cd ON cd.agency_id = e.agency_id ";
		}*/

		/* GET AGENCY INFO */
		/*$f = "
			, cd.phone AS agency_phone, cd.web AS agency_web, cd.is_premium AS is_premium_agency, cd.phone_country_id AS agency_phone_country_id,
			COUNT(DISTINCT(e.id)) AS escorts_count,
			cd.about_" . $lng . " AS agency_about, act.title_" . $lng . " AS agency_city_title, acr.title_" . $lng . " AS agency_country_title,
			cd.last_modified AS agency_date_last_modified, cd.email AS agency_email, cd.photo_hash AS a_photo_hash, cd.photo_ext AS a_photo_ext
		 ";

		$j .= " LEFT JOIN cities act ON act.id = cd.city_id ";
		$j .= " LEFT JOIN countries acr ON acr.id = cd.country_id ";*/

		/*if ( $list_type == 'simple' ) {
			$group_by = " GROUP BY (CASE WHEN e.agency_id IS NOT NULL THEN e.agency_id ELSE e.id END) ";
		} else {
			$group_by = " GROUP BY (CASE WHEN (e.agency_id IS NOT NULL AND eic.is_premium <> 1 ) THEN e.agency_id ELSE e.id END) ";
		}*/
		/* IF AGENCY GROUP BY AGENCY ID FOR MIXED VIEW */


		/*if ( ! $is_grouped ) {
			$order = str_replace('cd.is_premium DESC,', '', $order);
		}*/

		$group_by = " GROUP BY eic.escort_id ";
		$union_order = "ORDER BY x.is_agency DESC";

		$fields = ' NULL AS distance ';
		if ( $ordering == 'close-to-me' && count($geo_data) ) {

			if (array_key_exists("latitude", $geo_data) && strlen($geo_data['latitude']) > 0 && array_key_exists("longitude", $geo_data) && strlen($geo_data['longitude']) > 0) {
				$fields = '
					((2 * 6371 *
						ATAN2(
						SQRT(
							POWER(SIN((RADIANS(' . $geo_data['latitude'] . ' - ct.latitude))/2), 2) +
							COS(RADIANS(ct.latitude)) *
							COS(RADIANS(' . $geo_data['latitude'] . ')) *
							POWER(SIN((RADIANS(' . $geo_data['longitude'] . ' - ct.longitude))/2), 2)
						),
						SQRT(1-(
							POWER(SIN((RADIANS(' . $geo_data['latitude'] . ' - ct.latitude))/2), 2) +
							COS(RADIANS(ct.latitude)) *
							COS(RADIANS(' . $geo_data['latitude'] . ')) *
							POWER(SIN((RADIANS(' . $geo_data['longitude'] . ' - ct.longitude))/2), 2)
						))
						)
					)) AS distance
				';
			}

			$union_order = "ORDER BY x.is_agency DESC, x.distance ASC";
		}

		//$order = str_replace('eic.is_premium', 'IF(eic.is_tour = 1, 0, eic.is_premium)', $order);

		/*if ( $p_top_category == 'escorts' && ! in_array($ordering, array('vips', 'premiums')) ) {
			$order = 'e.is_agency DESC, ' . $order;
		}*/
		

		$sql = '

			SELECT SQL_CALC_FOUND_ROWS

				x.is_agency,
				x.id,
				x.showname,
				x.age,
				x.date_registered,
				x.user_id,
				x.is_pornstar,
				x.last_hand_verification_date,
				x.date_activated,
				x.city_slug,
				x.city_id,
				x.city,
				x.rates,
				x.incall_type,
				x.outcall_type,
				x.is_new,
				x.country_id,
				x.country_slug,
				x.country,
				x.verified_status,
				x.photo_hash,
				x.photo_ext,
				x.photo_status,
				x.application_id,
				x.incall_currency,
				x.incall_price,
				x.outcall_currency,
				x.outcall_price,
				x.comment_count,
				x.review_count,
				x.travel_place,
				x.email,
				x.website,
				x.phone_instr,
				x.phone_instr_no_withheld,
				x.phone_instr_other,
				x.phone_country_id,
				x.disable_phone_prefix,
				x.phone_number,
				x.hit_count,
				x.slogan,			
				x.date_last_modified,
				x.is_premium,
				x.is_vip,
				x.hh_is_active,
				x.is_tour,
				x.tour_date_from,
				x.tour_date_to,
				x.tour_city,
				x.is_suspicious,
				x.is_online,
				x.about,
				x.agency_name,
				x.agency_slug,
				x.agency_id,
				x.distance

			FROM ((

				SELECT
					0 AS is_agency,
					e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id, e.is_pornstar, e.last_hand_verification_date,
					UNIX_TIMESTAMP(e.date_activated) AS date_activated, ct.slug AS city_slug, ct.id AS city_id, ct.title_' . $lng . ' AS city, e.rates,
					e.incall_type, e.outcall_type, e.is_new, cr.id AS country_id, cr.slug AS country_slug, cr.title_' . $lng . ' AS country, e.verified_status,
					e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,
					e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.comment_count, e.review_count,
					e.travel_place, e.email, e.website, e.phone_instr, e.phone_instr_no_withheld, e.phone_instr_other,
					e.phone_country_id, e.disable_phone_prefix, e.phone_number,
					e.hit_count, e.slogan, e.date_last_modified, eic.is_premium, e.is_vip, e.hh_is_active,
					e.is_on_tour AS is_tour, e.tour_date_from, e.tour_date_to, ctt.title_' . $lng . ' AS tour_city,
					e.is_suspicious, e.is_online, e.about_' . $lng . ' AS about, e.agency_name,
					e.agency_slug, e.agency_id, ' . $fields . '
				FROM escorts_in_cities eic /*USE INDEX(by_googo3)*/
				INNER JOIN escorts e ON e.id = eic.escort_id
				INNER JOIN cities ct ON ct.id = eic.city_id
				INNER JOIN countries cr ON cr.id = e.country_id
				LEFT JOIN cities ctt ON ctt.id = e.tour_city_id
				LEFT JOIN escort_services es ON es.escort_id = e.id
				' . (! is_null($where) ? 'WHERE ' . $where : '') . '
				' . $group_by . '
				ORDER BY ' . $order . '

			) UNION (

				SELECT 
					1 AS is_agency,
					cd.agency_id AS id,
					cd.club_name AS showname,
					NULL AS age,
					NULL AS date_registered,
					cd.user_id,
					NULL AS is_pornstar,
					NULL AS last_hand_verification_date,
					NULL AS date_activated,
					ct.slug AS city_slug,
					ct.id AS city_id,
					ct.title_' . $lng . ' AS city,
					NULL AS rates,
					NULL AS incall_type,
					NULL AS outcall_type,
					NULL AS is_new,
					cr.id AS country_id,
					cr.slug AS country_slug,
					cr.title_' . $lng . ' AS country,
					NULL AS verified_status,
					cd.photo_hash,
					cd.photo_ext,
					NULL AS photo_status,
					69 AS application_id,
					NULL AS incall_currency,
					NULL AS incall_price,
					NULL AS outcall_currency,
					NULL AS outcall_price,
					NULL AS comment_count,
					NULL AS review_count,
					NULL AS travel_place,
					cd.email,
					cd.web AS website,
					NULL AS phone_instr,
					NULL AS phone_instr_no_withheld,
					NULL AS phone_instr_other,
					cd.phone_country_id,
					NULL AS disable_phone_prefix,
					cd.phone AS phone_number,
					cd.hit_count,
					NULL AS slogan,
					cd.last_modified AS date_last_modified,
					cd.is_premium,
					NULL AS is_vip,
					NULL AS hh_is_active,
					NULL AS is_tour,
					NULL AS tour_date_from,
					NULL AS tour_date_to,
					NULL AS tour_city,
					NULL AS is_suspicious,
					NULL AS is_online,
					cd.about_' . $lng . ' AS about,
					cd.club_name AS agency_name,
					cd.club_slug AS agency_slug,
					cd.agency_id AS agency_id, ' . $fields . '
				FROM club_directory cd
				LEFT JOIN cities ct ON ct.id = cd.city_id
				LEFT JOIN countries cr ON cr.id = cd.country_id
				' . $ag_where . '
				GROUP BY cd.agency_id
				ORDER BY ' . $ag_order . '

			)) AS x
			' . $union_order . '
			LIMIT ' . $limit . '
		'; 
		
		$escorts = self::db()->fetchAll($sql);
		
		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');



		if ( count($escorts) && $is_grouped && $list_type == 'simple' ) {

			$grouped_escorts = array();

			foreach($escorts as $k => $escort) {

				$group_field = $escort->city;
				if ( $ordering == 'by-country' && ! isset($filter['ct.id = ?']) && ! isset($filter['cr.id = ?']) ) {
					$group_field = $escort->country;
				}

				if ( $p_top_category == 'escorts' ) {
					
					if ( $escort->is_agency ) {
						$grouped_escorts['agencies']['city_groups'][$group_field][] = $escort;
					} else {
						$grouped_escorts['escorts']['city_groups'][$group_field][] = $escort;
					}

				} else {
					$grouped_escorts['city_groups'][$group_field][] = $escort;
				}

				if ( $ordering != 'close-to-me' && $ordering != 'newest' ) {
					ksort($grouped_escorts['city_groups']);
				}
					
			}

			if ( count($escorts) && count($escorts) < 6 && $city_id ) {
				$city_coordinates = Model_Cities::getCoordinatesById($city_id);
				$nearest_cities = Model_Statistics::getNearestCitiesByCoordinates(array('lat' => $city_coordinates->latitude, 'lon' => $city_coordinates->longitude), 4, $city_id);

				$filter___near = $filter;
				foreach( $nearest_cities as $n_city ) {
					$filter___near['ct.id = ?'] = array($n_city->city_id);
					$nearest_escorts = self::getFiltered($filter___near, 'random', 1, 20);
					if ( count($nearest_escorts) > 1 ) break;
				}

				if ( count($nearest_escorts) ) {
					foreach( $nearest_escorts as $escort ) {					
						//$escorts['nearest_city_escorts'][] = $n_escort;	

						$group_field = $escort->city;
						if ( $ordering == 'by-country' && ! isset($filter['ct.id = ?']) && ! isset($filter['cr.id = ?']) ) {
							$group_field = $escort->country;
						}
						
						$grouped_escorts['nearest_city_escorts']['city_groups'][$group_field][] = $escort;
						

						if ( $ordering != 'close-to-me' && $ordering != 'newest' ) {
							ksort($grouped_escorts['city_groups']);
						}
					}
				}
			}

			if ( $page == 1 ) {

				$filter___p = $filter;
				$filter___p['eic.is_premium = 1 AND e.is_vip = 0'] = array();
				$premium_escorts = self::getFiltered($filter___p, 'premiums', 1, 100);

				if ( count($premium_escorts) ) {					
					foreach( $premium_escorts as  $k => $p_escort ) {

						if ( $p_escort->is_premium ) {
							$grouped_escorts['premiums'][] = $p_escort;
						}
					}
				}


				$filter___v = $filter;
				$filter___v['e.is_vip = 1'] = array();
				$vip_escorts = self::getFiltered($filter___v, 'vips', 1, 100);

				if ( count($vip_escorts) ) {
					foreach( $vip_escorts as  $k => $v_escort ) {

						if ( $v_escort->is_vip ) {
							$grouped_escorts['vips'][] = $v_escort;
						}
					}
				}
			}

			$escorts = $grouped_escorts;


		}

		if ( $p_top_category == 'escorts' && count($escorts) && ! in_array($ordering, array('vips', 'premiums')) && $list_type != 'simple' ) {
			
			foreach($escorts as $k => $escort) {
				if ( $escort->is_agency ) {
					$grouped_escorts['agencies'][] = $escort;
				} else {
					$grouped_escorts['escorts'][] = $escort;
				}
			}

			$escorts = $grouped_escorts;
		}

		if ( count($escorts['escorts']) && count($escorts['escorts']) < 6 && count($escorts['agencies']) < 6 && $city_id && $list_type != 'simple' ) {
			$city_coordinates = Model_Cities::getCoordinatesById($city_id);
			$nearest_cities = Model_Statistics::getNearestCitiesByCoordinates(array('lat' => $city_coordinates->latitude, 'lon' => $city_coordinates->longitude), 4, $city_id);

			$filter___near = $filter;
			foreach( $nearest_cities as $n_city ) {
				$filter___near['ct.id = ?'] = array($n_city->city_id);
				$nearest_escorts = self::getFiltered($filter___near, 'random', 1, 20);
				if ( count($nearest_escorts) > 1 ) break;
			}

			if ( count($nearest_escorts) ) {
				foreach( $nearest_escorts as $n_escort ) {
					$escorts['nearest_city_escorts'][] = $n_escort;					
				}
			}
		}

		if ( ! in_array($ordering, array('vips', 'premiums')) && $list_type != 'simple' && ! in_array($p_top_category, array('newest', 'citytours', 'upcomingtours')) ) {

			if ( $page == 1 ) {

				$filter___p = $filter;
				$filter___p['eic.is_premium = 1 AND e.is_vip = 0'] = array();
				$premium_escorts = self::getFiltered($filter___p, 'premiums', 1, 100);

				if ( count($premium_escorts) ) {					
					foreach( $premium_escorts as  $k => $p_escort ) {

						if ( $p_escort->is_premium ) {
							$escorts['premiums'][] = $p_escort;
						}
					}
				}


				$filter___v = $filter;
				$filter___v['e.is_vip = 1'] = array();
				$vip_escorts = self::getFiltered($filter___v, 'vips', 1, 100);

				if ( count($vip_escorts) ) {
					foreach( $vip_escorts as  $k => $v_escort ) {

						if ( $v_escort->is_vip ) {
							$escorts['vips'][] = $v_escort;
						}
					}
				}

			}
		}

		return $escorts;
	}

	public static function getFiltered($filter, $ordering = 'random', $page = 1, $page_size = 50, &$count = 0, $sess_name = false, $only_vip = false, $s_filter_params = array(), $is_grouped = false, $geo_data = array(), $list_type = 'simple', $p_top_category = 'escorts', $city_id = null, $country_id = null, $current_currency = null, $checkFilters = false)
	{
		if($checkFilters){
			// Copy filter for check wiche filter available
			$filterArray = $filter;
		}

		$lng = Cubix_I18n::getLang();

		// Remove VIP and premium escorts from normal listing
		if ( ! in_array($ordering, array('vips', 'premiums')) ) {
			$filter['eic.is_premium = 0 AND e.is_vip = 0'] = array();
			$filter['(( e.package_id <> 103 AND e.package_id <> 104) OR e.package_id IS NULL)'] = array();
		} else {
			unset($filter['eic.is_premium = 0 AND e.is_vip = 0']);
			unset($filter['(( e.package_id <> 103 AND e.package_id <> 104) OR e.package_id IS NULL)']);
		}
		// Remove VIP and premium escorts from normal listing
		
		if ( isset($filter['remove_package_query']) ) {
			unset($filter['eic.is_premium = 0 AND e.is_vip = 0']);
			unset($filter['(( e.package_id <> 103 AND e.package_id <> 104) OR e.package_id IS NULL)']);
			unset($filter['remove_package_query']);
		}		

		if ( isset($s_filter_params['currency']) && $s_filter_params['currency'] && ($s_filter_params['price-from'] || $s_filter_params['price-to']) ) {
			$current_currency = $s_filter_params['currency'];
		}		

		$currency_rate = Model_Currencies::getSelectedCurrencyRate($current_currency);
		$sql_join = " ";

		if (date('G') < 2)
			$date = 2;
		else
			$date = date('G');

		if ( 'price' == substr($ordering, 0, 5) ) {
			$filter[] = 'e.incall_price IS NOT NULL';
		}

		/* #### new search sql */
		if ( isset($s_filter_params['gender']) && count($s_filter_params['gender']) ) {
			unset($filter['eic.gender = 1']);
			$filter[] = 'e.gender IN ( ' . implode(',', $s_filter_params['gender']) . ' )';
		}

		if ( isset($s_filter_params['keywords']) && count($s_filter_params['keywords']) ) {
			$filter[] = 'ek.keyword_id IN ( ' . implode(',', $s_filter_params['keywords']) . ' )';
			//$sql_join .= " INNER JOIN escort_keywords ek ON ek.escort_id = e.id ";
		}

		if ( isset($s_filter_params['orientation']) && count($s_filter_params['orientation']) ) {			
			$filter[] = 'e.sex_orientation IN ( ' . implode(',', $s_filter_params['orientation']) . ' )';
		}

		if ( isset($s_filter_params['ethnicity']) && count($s_filter_params['ethnicity']) ) {			
			$filter[] = 'e.ethnicity IN ( ' . implode(',', $s_filter_params['ethnicity']) . ' )';
		}

		if ( isset($s_filter_params['language']) && count($s_filter_params['language']) ) {			
			$languagesQ = '';
			foreach ($s_filter_params['language'] as $value) {
				$languagesQ .= " e.languages like '%".$value."%' AND";
			}
			$languagesQ = substr($languagesQ, 0, -3);
			$filter[] = $languagesQ;
		}

		if ( isset($s_filter_params['nationality']) && count($s_filter_params['nationality']) ) {			
			$languagesQ = ' e.nationality_id IN(';
			foreach ($s_filter_params['nationality'] as $value) {
				$languagesQ .= $value.",";
			}
			$languagesQ = substr($languagesQ, 0, -1);
			$languagesQ .= ") ";
			$filter[] = $languagesQ;
		}

		if ( isset($s_filter_params['hair-color']) && count($s_filter_params['hair-color']) ) {			
			$filter[] = 'e.hair_color IN ( ' . implode(',', $s_filter_params['hair-color']) . ' )';
		}

		if ( isset($s_filter_params['hair-length']) && count($s_filter_params['hair-length']) ) {
			$filter[] = 'e.hair_length IN ( ' . implode(',', $s_filter_params['hair-length']) . ' )';
		}

		if ( isset($s_filter_params['cup-size']) && count($s_filter_params['cup-size']) ) {			
			$filter[] = 'e.cup_size IN ( "' . implode('","', $s_filter_params['cup-size']) . '" )';
		}

		if ( isset($s_filter_params['eye-color']) && count($s_filter_params['eye-color']) ) {			
			$filter[] = 'e.eye_color IN ( ' . implode(',', $s_filter_params['eye-color']) . ' )';
		}

		if ( isset($s_filter_params['pubic-hair']) && count($s_filter_params['pubic-hair']) ) {			
			$filter[] = 'e.pubic_hair IN ( ' . implode(',', $s_filter_params['pubic-hair']) . ' )';
		}

		if ( isset($s_filter_params['sex-availability']) && count($s_filter_params['sex-availability']) ) {			
			$filter[] = 'e.sex_availability IN ( "' . implode('","', $s_filter_params['sex-availability']) . '" )';
		}

		$service_having = " ";
		if ( isset($s_filter_params['service']) && count($s_filter_params['service']) ) {
			$filter[] = 'es.service_id IN ( ' . implode(',', $s_filter_params['service']) . ' )';
			$service_having = " HAVING COUNT(DISTINCT es.service_id) = " . count($s_filter_params['service']) . " ";
		}

		if ( isset($s_filter_params['price-from']) && $s_filter_params['price-from'] ) {
			$filter[] = '(e.incall_price / er.rate) * ' . round($currency_rate->rate) . ' >= ' . $s_filter_params['price-from'];
		}
		if ( isset($s_filter_params['price-to']) && $s_filter_params['price-to'] ) {
			$filter[] = '(e.incall_price / er.rate) * ' . round($currency_rate->rate) . ' <= ' . $s_filter_params['price-to'];
		}

		if ( isset($s_filter_params['incall']) && count($s_filter_params['incall']) ) {
			$filter[] = 'e.incall_type IN ( ' . implode(',', $s_filter_params['incall']) . ' )';
		}
		if ( isset($s_filter_params['incall-hotel-room']) && count($s_filter_params['incall-hotel-room']) ) {
			$filter[] = 'e.incall_hotel_room IN ( ' . implode(',', $s_filter_params['incall-hotel-room']) . ' )';
		}
		if ( isset($s_filter_params['outcall']) && count($s_filter_params['outcall']) ) {
			$filter[] = 'e.outcall_type IN ( ' . implode(',', $s_filter_params['outcall']) . ' )';
		}
		if ( isset($s_filter_params['travel-place']) && count($s_filter_params['travel-place']) ) {
			$filter[] = 'e.travel_place IN ( ' . implode(',', $s_filter_params['travel-place']) . ' )';
		}

		if ( isset($s_filter_params['is-online-now']) && $s_filter_params['is-online-now'] ) {
			$filter[] = 'ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE) AND allow_show_online = 1';
		}

		if ( isset($filter['online-now']) && $filter['online-now'] ) {
			$filter[] = 'ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE) AND allow_show_online = 1';
		}
		unset($filter['online-now']);
		
		/*if ( isset($s_filter_params['with-video']) && $s_filter_params['with-video'] ) {			
			$sql_join .= " INNER JOIN video v ON v.escort_id = e.id ";
		}*/		
		/* #### new search sql */
		
		// Only escorts with base city (or city tour) will be shown in this list
		if ( ! $city_id && ! $country_id ) {
			$filter[] = 'eic.is_base = 1';
		} else {
			$filter[] = 'eic.is_upcoming = 0';
		}		

		if ( $ordering == "premiums" ) {
			$del_val = "eic.is_base = 1";
			while(($key = array_search($del_val, $filter)) !== false) {
				unset($filter[$key]);
			}
		}

				
		$show_splited_agency_escorts = true;
		if ( isset($filter['show_all_agency_escorts']) ) {
			unset($filter['show_all_agency_escorts']);
			$show_splited_agency_escorts = false;
		}

		if ( isset($s_filter_params['show-all-hidden-escorts']) && $s_filter_params['show-all-hidden-escorts'] ) {
			unset($filter['show_all_agency_escorts']);
			$show_splited_agency_escorts = false;
		}

		if ( in_array($ordering, array('vips', 'premiums')) ) {
			unset($filter['show_all_agency_escorts']);
			unset($filter['(e.agency_id IS NULL OR (e.show_agency_escorts = 1 OR eic.is_premium = 1))']);
			$show_splited_agency_escorts = false;
		}

		if ( $show_splited_agency_escorts ) {
			$filter['(e.agency_id IS NULL OR (e.show_agency_escorts = 1 OR eic.is_premium = 1))'] = array();
		}

		$with_video = false;
		if ( isset($filter['with-video']) && $filter['with-video'] ) {
			$with_video = true;			
			$sql_join .= " INNER JOIN video v ON v.escort_id = e.id ";
		}
		unset($filter['with-video']);

		$where = self::getWhereClause($filter, true);
		$order = self::_mapSorting($ordering, $is_grouped, $list_type);

		if ( $with_video ) {
			$filter['with-video'] = 'with-video';
		}

		$page = (int) $page;
		if ( $page < 1 ) $page = 1;
		$limit = ($page_size * ($page - 1)) . ', ' . $page_size;
		
		$j = "";
		$f = "";
		if ( isset($filter['f.user_id = ?']) ) {
			$j = " INNER JOIN favorites f ON f.escort_id = e.id ";
			$f = " , f.user_id AS fav_user_id ";
		}

		$group_by = " GROUP BY eic.escort_id ";

		$fields = ' NULL AS distance ';
		if ( $ordering == 'close-to-me' && count($geo_data) ) {
			if (array_key_exists("latitude", $geo_data) && strlen($geo_data['latitude']) > 0 && array_key_exists("longitude", $geo_data) && strlen($geo_data['longitude']) > 0) {
				$fields = '
					((2 * 6371 *
						ATAN2(
						SQRT(
							POWER(SIN((RADIANS(' . $geo_data['latitude'] . ' - ct.latitude))/2), 2) +
							COS(RADIANS(ct.latitude)) *
							COS(RADIANS(' . $geo_data['latitude'] . ')) *
							POWER(SIN((RADIANS(' . $geo_data['longitude'] . ' - ct.longitude))/2), 2)
						),
						SQRT(1-(
							POWER(SIN((RADIANS(' . $geo_data['latitude'] . ' - ct.latitude))/2), 2) +
							COS(RADIANS(ct.latitude)) *
							COS(RADIANS(' . $geo_data['latitude'] . ')) *
							POWER(SIN((RADIANS(' . $geo_data['longitude'] . ' - ct.longitude))/2), 2)
						))
						)
					)) AS distance
				';
			}
		}

		is_null($where) ? $where = ' 1 ' . Cubix_Countries::blacklistCountryWhereCase() : $where .= Cubix_Countries::blacklistCountryWhereCase();

		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id, e.is_pornstar, e.last_hand_verification_date,
				UNIX_TIMESTAMP(e.date_activated) AS date_activated, ct.slug AS city_slug, ct.id AS city_id, ct.title_' . $lng . ' AS city, e.rates,
				e.incall_type, e.outcall_type, e.is_new, cr.id AS country_id, cr.slug AS country_slug, cr.title_' . $lng . ' AS country, e.verified_status,
				e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.comment_count, e.review_count,
				e.travel_place, e.email, e.website, e.phone_instr, e.phone_instr_no_withheld, e.phone_instr_other,
				e.phone_country_id, e.disable_phone_prefix, e.phone_number_free AS phone_number,
				IF (ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE), 1, 0) AS is_login, e.allow_show_online,
				e.hit_count, e.slogan, e.incall_price, e.date_last_modified, eic.is_premium, e.is_vip, e.hh_is_active,
				e.is_on_tour AS is_tour, e.tour_date_from, e.tour_date_to, ctt.title_' . $lng . ' AS tour_city,
				e.is_suspicious, e.is_online, ct.id AS city_id, e.about_' . $lng . ' AS about, e.agency_name,
				(e.incall_price / er.rate) * ' . round($currency_rate->rate) . ' AS incall_price_converted,
				e.agency_slug, e.agency_id, ulrt.refresh_date AS refresh_date, ' . $fields . '
			FROM escorts_in_cities eic /*USE INDEX(by_googo3)*/
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			INNER JOIN countries cr ON cr.id = ct.country_id
			LEFT JOIN users_last_refresh_time ulrt ON ulrt.user_id = e.user_id
			LEFT JOIN cities ctt ON ctt.id = e.tour_city_id
			/*LEFT JOIN countries crtt ON crtt.id = ctt.country_id*/
			' . $sql_join . '
			LEFT JOIN escort_services es ON es.escort_id = e.id
			LEFT JOIN escort_keywords ek ON ek.escort_id = e.id
			LEFT JOIN currencies cur ON cur.id = e.incall_currency
			LEFT JOIN exchange_rates er ON er.iso = cur.title

			' . (! is_null($where) ? 'WHERE ' . $where : '') . '
			' . $group_by . '
			ORDER BY ' . $order . '
			LIMIT ' . $limit . '
		';


		// Check filters
		//////////////////////////////////////
		if($checkFilters){
			$availableFilters = array();

			$availableFilters['real_pics'] = false;
			$availableFilters['verified_contact'] = false;
			$availableFilters['with_video'] = false;
			$availableFilters['pornstar'] = false;
			$availableFilters['online'] = false;

			// Check real pics filter
			if(isset($filterArray['e.verified_status = 2'])){
				$availableFilters['real_pics'] = true;
			}else{
				$whereR = $where. ' AND e.verified_status = 2';
				$realPicsSql = '
					SELECT count(e.id) as cnt
					FROM escorts_in_cities eic
					INNER JOIN escorts e ON e.id = eic.escort_id
					INNER JOIN cities ct ON ct.id = eic.city_id
					INNER JOIN countries cr ON cr.id = ct.country_id
					LEFT JOIN users_last_refresh_time ulrt ON ulrt.user_id = e.user_id
					LEFT JOIN cities ctt ON ctt.id = e.tour_city_id
					' . $sql_join . '
					LEFT JOIN escort_services es ON es.escort_id = e.id
					LEFT JOIN escort_keywords ek ON ek.escort_id = e.id
					LEFT JOIN currencies cur ON cur.id = e.incall_currency
					LEFT JOIN exchange_rates er ON er.iso = cur.title
					' . (! is_null($whereR) ? 'WHERE ' . $whereR : '');	

				$realPicsCount = self::db()->fetchOne($realPicsSql);
				if($realPicsCount > 0){
					$availableFilters['real_pics'] = true;
				}	
			}

			//Check Verified Contact Details filter
			if(isset($filterArray['e.last_hand_verification_date IS NOT NULL'])){
				$availableFilters['verified_contact'] = true;
			}else{
				$whereV = $where. ' AND e.last_hand_verification_date IS NOT NULL';
				$verificationContactSql = '
					SELECT count(e.id) as cnt
					FROM escorts_in_cities eic
					INNER JOIN escorts e ON e.id = eic.escort_id
					INNER JOIN cities ct ON ct.id = eic.city_id
					INNER JOIN countries cr ON cr.id = ct.country_id
					LEFT JOIN users_last_refresh_time ulrt ON ulrt.user_id = e.user_id
					LEFT JOIN cities ctt ON ctt.id = e.tour_city_id
					' . $sql_join . '
					LEFT JOIN escort_services es ON es.escort_id = e.id
					LEFT JOIN escort_keywords ek ON ek.escort_id = e.id
					LEFT JOIN currencies cur ON cur.id = e.incall_currency
					LEFT JOIN exchange_rates er ON er.iso = cur.title
					' . (! is_null($whereV) ? 'WHERE ' . $whereV : '');

				$verificationContact = self::db()->fetchOne($verificationContactSql);
				
				if($verificationContact > 0){
					$availableFilters['verified_contact'] = true;
				}	
			}

			//Check With video filter
			if($with_video){
				$availableFilters['with_video'] = true;
			}else{
				$sql_joinWV = $sql_join. '  INNER JOIN video v ON v.escort_id = e.id ';
				$withVideoSql = '
					SELECT count(e.id) as cnt
					FROM escorts_in_cities eic
					INNER JOIN escorts e ON e.id = eic.escort_id
					INNER JOIN cities ct ON ct.id = eic.city_id
					INNER JOIN countries cr ON cr.id = ct.country_id
					LEFT JOIN users_last_refresh_time ulrt ON ulrt.user_id = e.user_id
					LEFT JOIN cities ctt ON ctt.id = e.tour_city_id
					' . $sql_joinWV . '
					LEFT JOIN escort_services es ON es.escort_id = e.id
					LEFT JOIN escort_keywords ek ON ek.escort_id = e.id
					LEFT JOIN currencies cur ON cur.id = e.incall_currency
					LEFT JOIN exchange_rates er ON er.iso = cur.title
					' . (! is_null($where) ? 'WHERE ' . $where : '');

				$withVideoCount = self::db()->fetchOne($withVideoSql);
				
				if($withVideoCount > 0){
					$availableFilters['with_video'] = true;
				}	
			}

			//Check Pornstar filter
			if(isset($filterArray['e.is_pornstar = 1'])){
				$availableFilters['pornstar'] = true;
			}else{
				$wherePStar = $where. ' AND e.is_pornstar = 1';
				$pornoStarSql = '
					SELECT count(e.id) as cnt
					FROM escorts_in_cities eic
					INNER JOIN escorts e ON e.id = eic.escort_id
					INNER JOIN cities ct ON ct.id = eic.city_id
					INNER JOIN countries cr ON cr.id = ct.country_id
					LEFT JOIN users_last_refresh_time ulrt ON ulrt.user_id = e.user_id
					LEFT JOIN cities ctt ON ctt.id = e.tour_city_id
					' . $sql_join . '
					LEFT JOIN escort_services es ON es.escort_id = e.id
					LEFT JOIN escort_keywords ek ON ek.escort_id = e.id
					LEFT JOIN currencies cur ON cur.id = e.incall_currency
					LEFT JOIN exchange_rates er ON er.iso = cur.title
					' . (! is_null($wherePStar) ? 'WHERE ' . $wherePStar : '');

				$pornoStarCount = self::db()->fetchOne($pornoStarSql);
				
				if($pornoStarCount > 0){
					$availableFilters['pornstar'] = true;
				}	
			}

			//Check Online
			if ( isset($s_filter_params['is-online-now']) && $s_filter_params['is-online-now'] ) {
				$availableFilters['online'] = true;
			}else{
				$whereOnline = $where. ' AND ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE) AND allow_show_online = 1';
				$onlineSql = '
					SELECT count(e.id) as cnt
					FROM escorts_in_cities eic
					INNER JOIN escorts e ON e.id = eic.escort_id
					INNER JOIN cities ct ON ct.id = eic.city_id
					INNER JOIN countries cr ON cr.id = ct.country_id
					LEFT JOIN users_last_refresh_time ulrt ON ulrt.user_id = e.user_id
					LEFT JOIN cities ctt ON ctt.id = e.tour_city_id
					' . $sql_join . '
					LEFT JOIN escort_services es ON es.escort_id = e.id
					LEFT JOIN escort_keywords ek ON ek.escort_id = e.id
					LEFT JOIN currencies cur ON cur.id = e.incall_currency
					LEFT JOIN exchange_rates er ON er.iso = cur.title
					' . (! is_null($whereOnline) ? 'WHERE ' . $whereOnline : '');

				$onlineCount = self::db()->fetchOne($onlineSql);
				
				if($onlineCount > 0){
					$availableFilters['online'] = true;
				}	
			}

			return $availableFilters;
		}
		//////////////////////////////////////


		$escorts = self::db()->fetchAll($sql);
		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');


		//if escorts count less then 6, fill escorts list from near cities ***********************************
		if ( count($escorts) && count($escorts) < 6 && $city_id && $list_type != 'simple' && ! in_array($ordering, array('premiums', 'vips')) ) {
			$city_coordinates = Model_Cities::getCoordinatesById($city_id);
			
			$nearest_cities = Model_Statistics::getNearestCitiesByCoordinates(array('lat' => $city_coordinates->latitude, 'lon' => $city_coordinates->longitude), 4, $city_id);

			$excl_e_ids = array();
			foreach ($escorts as $esc) {
				$excl_e_ids[] = $esc->id;
			}
			
			$filter___near = $filter;
			foreach( $nearest_cities as $n_city ) {
				$filter___near['ct.id = ?'] = array($n_city->city_id);
				$filter___near['cr.id = ?'] = array($city_coordinates->country_id);
				$filter___near['e.id NOT IN (?)'] = array(implode(',', $excl_e_ids));

				$nearest_escorts = self::getFiltered($filter___near, 'random', 1, 20);
				
				if ( count($nearest_escorts) >= 1 ) break; 
			}

			if ( count($nearest_escorts) ) {
				foreach( $nearest_escorts as $n_escort ) {					
					$escorts['nearest_city_escorts'][] = $n_escort;					
				}
			}
		}
		// ********************************************************************************************************
		
		if ( count($escorts) && $is_grouped && $list_type == 'simple' ) {

			$grouped_escorts = array();

			if ( count($escorts) && count($escorts) < 6 && $city_id && ! in_array($ordering, array('premiums', 'vips')) ) {
				$city_coordinates = Model_Cities::getCoordinatesById($city_id);
				$nearest_cities = Model_Statistics::getNearestCitiesByCoordinates(array('lat' => $city_coordinates->latitude, 'lon' => $city_coordinates->longitude), 4, $city_id);

				$filter___near = $filter;
				foreach( $nearest_cities as $n_city ) {
					$filter___near['ct.id = ?'] = array($n_city->city_id);
					$nearest_escorts = self::getFiltered($filter___near, 'random', 1, 20);
					if ( count($nearest_escorts) >= 1 ) break;
				}

				if ( count($nearest_escorts) ) {
					foreach( $nearest_escorts as $escort ) {					
						//$escorts['nearest_city_escorts'][] = $n_escort;	

						$group_field = $escort->city;
						if ( $ordering == 'by-country' && ! isset($filter['ct.id = ?']) && ! isset($filter['cr.id = ?']) ) {
							$group_field = $escort->country;
						}
						
						$grouped_escorts['nearest_city_escorts']['city_groups'][$group_field][] = $escort;
						

						if ( $ordering != 'close-to-me' && $ordering != 'newest' ) {
							ksort($grouped_escorts['city_groups']);
						}
					}
				}
			}

			foreach($escorts as $k => $escort) {

				$group_field = $escort->city;
				if ( $ordering == 'by-country' && ! isset($filter['ct.id = ?']) && ! isset($filter['cr.id = ?']) ) {
					$group_field = $escort->country;
				}

				if ( in_array($p_top_category, array('newest', 'newest-independent', 'newest-agency-escorts')) ) {
					$grouped_escorts['newest'][date("d.m.Y", ($escort->date_activated) ? $escort->date_activated : $escort->date_registered)]['city_groups'][$group_field][] = $escort;					

				} else {

					$no_group_sorting = array('newest', 'random', 'alpha', 'most-viewed');
					if ( in_array($ordering, $no_group_sorting) ) {
						$group_field = '';
					} else {
						$group_field = $group_field . ' ' . __('escorts');
					}				

					$grouped_escorts['city_groups'][$group_field][] = $escort;
				}

				if ( $ordering != 'close-to-me' && $ordering != 'newest' ) {
					ksort($grouped_escorts['city_groups']);
				}
					
			}

			$escorts = $grouped_escorts;
		}

		// Load VIP and Premium Escorts on first page
		if ( $page == 1 && ! in_array($ordering, array('vips', 'premiums'))  && ! in_array($p_top_category, array('newest', 'newest-independent', 'newest-agency-escorts', 'citytours', 'upcomingtours')) ) {			

			$filter___p = $filter;
			$filter___p['eic.is_premium = 1 AND e.is_vip = 0'] = array();
			$premium_escorts = self::getFiltered($filter___p, 'premiums', 1, 30);
			$premium_agencies = Model_Agencies::getAllPremiums($city_id, $country_id);

			$premium_escorts = array_merge($premium_escorts, $premium_agencies);

			if ( count($premium_escorts) ) {					
				foreach( $premium_escorts as  $k => $p_escort ) {

					if ( $p_escort->is_premium ) {
						$escorts['premiums'][] = $p_escort;
					}
				}
			}


			$filter___v = $filter;
			$filter___v['e.is_vip = 1'] = array();				
			$vip_escorts = self::getFiltered($filter___v, 'vips', 1, 30);
			if ( count($vip_escorts) ) {
				foreach( $vip_escorts as  $k => $v_escort ) {

					if ( $v_escort->is_vip ) {
						$escorts['vips'][] = $v_escort;
					}
				}
			}			
		}

		return $escorts;
	}

	public static function getAgencyEscorts($filter, $ordering = 'random', $page = 1, $page_size = 50, &$count = 0)
	{
		$lng = Cubix_I18n::getLang();

		$page = (int) $page;
		if ( $page < 1 ) $page = 1;
		$limit = ($page_size * ($page - 1)) . ', ' . $page_size;


		$where = self::getWhereClause($filter, true);
		$order = self::_mapSorting($ordering);

		$group_by = " GROUP BY eic.escort_id ";

		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id, e.is_pornstar, e.last_hand_verification_date,
				UNIX_TIMESTAMP(e.date_activated) AS date_activated, ct.slug AS city_slug, ct.id AS city_id, ct.title_' . $lng . ' AS city, e.rates,
				e.incall_type, e.outcall_type, e.is_new, cr.id AS country_id, cr.slug AS country_slug, cr.title_' . $lng . ' AS country, e.verified_status,
				e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.comment_count, e.review_count,
				e.travel_place, e.email, e.website, e.phone_instr, e.phone_instr_no_withheld, e.phone_instr_other,
				e.phone_country_id, e.disable_phone_prefix, e.phone_number_free AS phone_number,
				IF (ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE), 1, 0) AS is_login, e.allow_show_online,
				e.hit_count, e.slogan, e.incall_price, e.date_last_modified, eic.is_premium, e.is_vip, e.hh_is_active,
				e.is_on_tour AS is_tour, e.tour_date_from, e.tour_date_to, ctt.title_' . $lng . ' AS tour_city,
				e.is_suspicious, e.is_online, ct.id AS city_id, e.about_' . $lng . ' AS about, e.agency_name,
				e.agency_slug, e.agency_id
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			INNER JOIN countries cr ON cr.id = ct.country_id
			LEFT JOIN users_last_refresh_time ulrt ON ulrt.user_id = e.user_id
			LEFT JOIN cities ctt ON ctt.id = e.tour_city_id			
			LEFT JOIN escort_services es ON es.escort_id = e.id
			LEFT JOIN escort_keywords ek ON ek.escort_id = e.id
			LEFT JOIN currencies cur ON cur.id = e.incall_currency
			LEFT JOIN exchange_rates er ON er.iso = cur.title

			' . (! is_null($where) ? 'WHERE ' . $where : '') . '
			' . $group_by . '
			ORDER BY ' . $order . '
			LIMIT ' . $limit . '
		';

		$escorts = self::db()->fetchAll($sql);

		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');

		return $escorts;
	}

	public static function getTours($is_upcoming, $filter, $ordering = 'random', $page = 1, $page_size = 50, &$count = 0, $s_filter_params = array(), $is_grouped = false, $geo_data = array(), $list_type = 'simple', $current_currency = null)
	{
		$lng = Cubix_I18n::getLang();

		if ( isset($s_filter_params['gender']) && count($s_filter_params['gender']) ) {
			$filter[] = 'e.gender IN ( ' . implode(',', $s_filter_params['gender']) . ' )';
		}

		if ( isset($s_filter_params['service']) && count($s_filter_params['service']) ) {
			$filter[] = 'es.service_id IN ( ' . implode(',', $s_filter_params['service']) . ' )';
		}

		if ( isset($filter['eic.gender = 1']) ) unset($filter['eic.gender = 1']);
		
		if ( ! $is_upcoming ) {
			$filter[] = 'eic.is_tour = 1';
		}
		else {
			$filter[] = 'eic.is_upcoming = 1';
		}

		unset($filter['online-now']);

		if ( isset($filter['show_all_agency_escorts']) ) {
			unset($filter['show_all_agency_escorts']);
		}
		
		$where = self::getWhereClause($filter, true);
		$order = self::_mapSorting($ordering, $is_grouped, $list_type);

		$page = (int) $page;
		if ( $page < 1 ) $page = 1;
		$limit = ($page_size * ($page - 1)) . ', ' . $page_size;

		$fields = ' , NULL AS distance ';
		if ( $ordering == 'close-to-me' && count($geo_data) ) {
			if (array_key_exists("latitude", $geo_data) && strlen($geo_data['latitude']) > 0 && array_key_exists("longitude", $geo_data) && strlen($geo_data['longitude']) > 0) {
				$fields = '
					 , ((2 * 6371 *
						ATAN2(
						SQRT(
							POWER(SIN((RADIANS(' . $geo_data['latitude'] . ' - ct.latitude))/2), 2) +
							COS(RADIANS(ct.latitude)) *
							COS(RADIANS(' . $geo_data['latitude'] . ')) *
							POWER(SIN((RADIANS(' . $geo_data['longitude'] . ' - ct.longitude))/2), 2)
						),
						SQRT(1-(
							POWER(SIN((RADIANS(' . $geo_data['latitude'] . ' - ct.latitude))/2), 2) +
							COS(RADIANS(ct.latitude)) *
							COS(RADIANS(' . $geo_data['latitude'] . ')) *
							POWER(SIN((RADIANS(' . $geo_data['longitude'] . ' - ct.longitude))/2), 2)
						))
						)
					)) AS distance
				';
			}
		}

		$currency_rate = Model_Currencies::getSelectedCurrencyRate($current_currency);

		$order = 'e.is_vip DESC, eic.is_premium DESC,' . $order;

		is_null($where) ? $where = ' 1 ' . Cubix_Countries::blacklistCountryWhereCase() : $where .= Cubix_Countries::blacklistCountryWhereCase();

		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id, e.is_pornstar, e.last_hand_verification_date,
				UNIX_TIMESTAMP(e.date_activated) AS date_activated, ct.slug AS city_slug, ct.id AS city_id, ct.title_' . $lng . ' AS city, e.rates,
				e.incall_type, e.outcall_type, e.is_new, cr.id AS country_id, cr.slug AS country_slug, cr.title_' . $lng . ' AS country, e.verified_status,
				e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.comment_count, e.review_count,
				e.travel_place, e.email, e.website, e.phone_instr, e.phone_instr_no_withheld, e.phone_instr_other,
				e.phone_country_id, e.disable_phone_prefix, e.phone_number_free AS phone_number,
				e.hit_count, e.slogan, e.incall_price, e.date_last_modified, eic.is_premium, e.is_vip, e.hh_is_active,
				e.is_suspicious, e.is_online, ct.id AS city_id, e.about_' . $lng . ' AS about, e.agency_id, e.agency_name, e.agency_slug,
				(e.incall_price / er.rate) * ' . round($currency_rate->rate) . ' AS incall_price_converted,
				eic.is_tour ' .
				(! $is_upcoming ? ', e.tour_date_from, e.tour_date_to, ctt.title_' . $lng . ' AS tour_city' : // if
				($is_upcoming ? ', ut.tour_date_from, ut.tour_date_to, ctu.title_' . $lng . ' AS tour_city' : '')) // else
				. $fields . '
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			INNER JOIN countries cr ON cr.id = ct.country_id
			LEFT JOIN users_last_refresh_time ulrt ON ulrt.user_id = e.user_id

			LEFT JOIN currencies cur ON cur.id = e.incall_currency
			LEFT JOIN exchange_rates er ON er.iso = cur.title
			
			LEFT JOIN escort_services es ON es.escort_id = e.id
			' . ($is_upcoming ? 'INNER JOIN upcoming_tours ut ON ut.id = eic.escort_id INNER JOIN cities ctu ON ctu.id = ut.tour_city_id' : 'LEFT JOIN cities ctt ON ctt.id = e.tour_city_id') . '
			' . (! is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.escort_id
			ORDER BY ' . $order . '
			LIMIT ' . $limit . '
		';

		$escorts = self::db()->fetchAll($sql);

		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');
		
		if ( count($escorts) && $is_grouped && $list_type == 'simple' ) {

			$grouped_escorts = array();
			foreach($escorts as $k => $escort) {					

				$group_field = $escort->city;
				if ( $ordering == 'by-country' && ! isset($filter['ct.id = ?']) && ! isset($filter['cr.id = ?']) ) {
					$group_field = $escort->country;
				}

				$no_group_sorting = array('newest', 'random', 'alpha', 'most-viewed');
				if ( in_array($ordering, $no_group_sorting) ) {
					$group_field = '';
				} else {
					$group_field = $group_field . ' ' . __('escorts');
				}				

				$grouped_escorts['city_groups'][$group_field][] = $escort;

				if ( $ordering != 'close-to-me' && $ordering != 'newest' ) {
					ksort($grouped_escorts['city_groups']);
				}
					
			}

			$escorts = $grouped_escorts;			
		}

		/*if ( $page == 1 && ! in_array($ordering, array('vips', 'premiums')) ) {			
			$key = array_search('eic.is_tour = 1', $filter);
			unset($filter[$key]);
			

			$filter___p = $filter;
			$filter___p['eic.is_premium = 1 AND e.is_vip = 0'] = array();
			$premium_escorts = self::getFiltered($filter___p, 'premiums', 1, 100);

			if ( count($premium_escorts) ) {					
				foreach( $premium_escorts as  $k => $p_escort ) {

					if ( $p_escort->is_premium ) {
						$escorts['premiums'][] = $p_escort;
					}
				}
			}

			unset($filter['eic.is_tour = 1']);
			unset($filter['eic.is_upcoming = 1']);

			$filter___v = $filter;
			$filter___v['e.is_vip = 1'] = array();
			$filter___v['e.gender <> 2'] = array();
			$filter___v['e.gender <> 3'] = array();
			$vip_escorts = self::getFiltered($filter___v, 'vips', 1, 100);

			if ( count($vip_escorts) ) {
				foreach( $vip_escorts as  $k => $v_escort ) {

					if ( $v_escort->is_vip ) {
						$escorts['vips'][] = $v_escort;
					}
				}
			}			
		}*/

		return $escorts;
	}

	private static function _mapSortingAgencies($param)
	{
		$map = array(
			'alpha' => 'cd.is_premium DESC, cd.club_name ASC',
			'random' => 'cd.is_premium DESC, RAND()',
			'last_modified' => 'cd.is_premium DESC, cd.last_modified DESC',
			'premium' => 'cd.is_premium DESC',
			'by-city' => 'ct.title_en ASC',
			'by-country' => 'cr.title_en ASC',
			'most-viewed' => 'cd.hit_count DESC',
			'close-to-me' => 'distance ASC',
		);

		$order = 'cd.club_name ASC';
		if ( isset($map[$param]) ) {
			$order = $map[$param];
		}
		elseif ( false !== strpos($param, 'FIELD') ) {
			$order = $param;
		}

		return $order;
	}

	private static function _mapSorting($param, $is_grouped = false, $view_type = 'simple')
	{
		$map = array(
			'close-to-me' => '/*e.is_vip DESC, eic.is_premium DESC,*/ distance ASC, eic.ordering DESC',
			'last-contact-verification' => 'e.last_hand_verification_date DESC',
			'last-connection' => 'ulrt.refresh_date DESC',


			'by-city' => '/*e.is_vip DESC, eic.is_premium DESC,*/ ct.title_en ASC, eic.ordering DESC',
			'by-country' => '/*e.is_vip DESC, eic.is_premium DESC,*/ cr.title_en ASC, eic.ordering DESC',
			'random' => '/*e.is_vip DESC, eic.is_premium DESC,*/ eic.ordering DESC',
			'alpha' => '/*e.is_vip DESC, eic.is_premium DESC,*/ e.showname ASC',
			'most-viewed' => '/*eic.is_premium DESC,*/ e.hit_count DESC',
			'newest' => 'COALESCE(e.date_activated, e.date_registered) DESC, e.id DESC',


			'premiums' => 'e.is_vip DESC, eic.is_premium DESC, eic.ordering DESC',
			'vips' => 'e.is_vip DESC, eic.ordering DESC',
			'price-asc' => 'incall_price_converted ASC',
			'price-desc' => 'incall_price_converted DESC',

			'last_modified' => 'e.date_last_modified DESC',
			'age' => '-e.age DESC',
		);

		if ( $view_type == 'simple' ) {
			$map = array(
				'close-to-me' => 'distance ASC, eic.ordering DESC',
				'last-contact-verification' => 'e.last_hand_verification_date DESC',
				'last-connection' => 'ulrt.refresh_date DESC',

				'by-city' => '/*e.is_vip DESC, eic.is_premium DESC,*/ ct.title_en ASC, eic.ordering DESC',
				'by-country' => '/*e.is_vip DESC, eic.is_premium DESC,*/ cr.title_en ASC, eic.ordering DESC',
				'random' => '/*e.is_vip DESC, eic.is_premium DESC,*/ eic.ordering DESC',
				'alpha' => '/*e.is_vip DESC, eic.is_premium DESC,*/ e.showname ASC',
				'most-viewed' => '/*eic.is_premium DESC,*/ e.hit_count DESC',
				'newest' => 'COALESCE(e.date_activated, e.date_registered) DESC',


				'premiums' => 'e.is_vip DESC, eic.is_premium DESC, eic.ordering DESC',
				'vips' => 'e.is_vip DESC, eic.ordering DESC',
				'price-asc' => 'incall_price_converted ASC',
				'price-desc' => 'incall_price_converted DESC',

				'last_modified' => 'e.date_last_modified DESC',
				'age' => '-e.age DESC',
			);
		}
		
		$order = 'e.ordering DESC';
		if ( isset($map[$param]) ) {
			$order = $map[$param];
		}
		elseif ( false !== strpos($param, 'FIELD') ) {
			$order = $param;
		}

		return $order;
	}

	public static function getPrevNext($showname)
	{
		return array(null, null);
		// Retrieve criterias from session
		$sid = 'v2_sedcard_paging_' . Cubix_Application::getId();
		$sess = new Zend_Session_Namespace($sid);

		$page = $sess->page;
		$page_size = $sess->page_size;
		$pages_count = $sess->pages_count;

		list($filter, $ordering) = $sess->criteria;
		foreach ( $sess->escorts as $i => $name ) {
			if ( $name == $showname ) {
				$prev_i = $i - 1;
				$next_i = $i + 1;

				break;
			}
		}

		echo "Prev_i: $prev_i, Next_i: $next_i\n";
		print_r($sess->escorts);

		if ( ! isset($prev_i) && ! isset($next_i) ) {
			return array(null, null);
		}

		// This is first escort, we need to fetch data from prev page
		if ( isset($prev_i) && (! isset($sess->escorts[$prev_i]) || $prev_i < 0) && $page > 1 ) {
			$sess->escorts = array_slice($sess->escorts, 0, $page_size * 2);

			$escorts = call_user_func_array(
				array(__CLASS__, $sess->callback),
				array($filter, $ordering, $page - 1, $page_size)
			);
			foreach ( $escorts as $i => $escort ) {
				$escorts[$i] = $escort->showname;
			}

			$sess->escorts = array_merge($escorts, $sess->escorts);
			
			$prev_i = $page_size - 1;
		}

		
		echo "Escorts after 'first' logic\n";
		echo "Prev_i: $prev_i, Next_i: $next_i\n";
		print_r($sess->escorts);
		
		// If this last escort on page, we need to fetch data from next page
		if ( isset($next_i) &&  (! isset($sess->escorts[$next_i]) || $next_i > count($sess->escorts) - 1 ) && $page < $pages_count ) {
			if ( count($sess->escorts) >= $page_size * 3 ) {
				$sess->escorts = array_slice($sess->escorts, $page_size, $page_size * 2);
				$next_i -= $page_size;
			}
			
			$escorts = call_user_func_array(
				array(__CLASS__, $sess->callback),
				array($filter, $ordering, $page + 1, $page_size)
			);
			foreach ( $escorts as $i => $escort ) {
				$escorts[$i] = $escort->showname;
			}

			$sess->escorts = array_merge($sess->escorts, $escorts);
		}

		echo "Escorts after 'last' logic\n";
		echo "Prev_i: $prev_i, Next_i: $next_i\n";
		print_r($sess->escorts);
		
		return array(isset($prev_i) ? $sess->escorts[$prev_i] : null, isset($next_i) ? $sess->escorts[$next_i] : null);
	}
	
	public static function getVIPEscorts($filter, $ordering = 'random', $page = 1, $page_size = 50, &$count = 0, $sess_name = false, $only_vip = false, $is_tour = false)
	{
		$lng = Cubix_I18n::getLang();

		if (date('G') < 2)
			$date = 2;
		else
			$date = date('G');

		if ( 'price' == substr($ordering, 0, 5) ) {
			$filter[] = 'e.incall_price IS NOT NULL';
		}
		
		if ( $only_vip ) {
			$filter[] = 'FIND_IN_SET(17, e.products) > 0';

			if ( isset($filter['ct.slug = ?']) && ! $is_tour ) {
				$country_id = Model_Statistics::getCountryByCitySlug($filter['ct.slug = ?'][0]);
				$filter[] = 'e.country_id = ' . $country_id;
				unset($filter['ct.slug = ?']);
			}
		}
		
		// Only escorts with base city (or city tour) will be shown in this list
		if ( ! isset($filter['ct.slug = ?']) && ! isset($filter['r.slug = ?']) ) {
			$filter[] = 'eic.is_base = 1';
		}
		else {
			$filter[] = 'eic.is_upcoming = 0';
		}

		$where = self::getWhereClause($filter, true);
		$order = self::_mapSorting($ordering);

		$page = (int) $page;
		if ( $page < 1 ) $page = 1;
		$limit = ($page_size * ($page - 1)) . ', ' . $page_size;
		
		$j = "";
		$f = "";
		if ( isset($filter['f.user_id = ?']) ) {
			$j = " INNER JOIN favorites f ON f.escort_id = e.id ";
			$f = " , f.user_id AS fav_user_id ";
		}

		$order = str_replace('eic.is_premium', 'IF(eic.is_tour = 1, 0, eic.is_premium)', $order);
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id, e.is_pornstar,
				UNIX_TIMESTAMP(e.date_activated) AS date_activated,
				ct.title_' . $lng . ' AS city, e.rates, e.incall_type, e.outcall_type, e.is_new,
				e.verified_status, e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.comment_count, e.review_count,

				IF (sei.primary_id IS NULL, se.title_' . $lng . ', sei.title_' . $lng . ') AS alt,
				IF (e.available_24_7 = 1 OR (e.is_now_open = 1 AND e.close_time >= "' . $date . '"), 1, 0) AS is_late_night,
				e.hit_count, e.slogan, e.incall_price,
				e.date_last_modified,
				/*IF(eic.is_tour = 1 AND eic.is_premium = 1, 0, eic.is_premium) AS is_premium,*/ 
				IF(eic.is_tour = 1, 0, eic.is_premium) AS is_premium,
				IF(FIND_IN_SET(17, e.products) > 0, 1, 0) AS is_vip,
				e.hh_is_active, e.is_suspicious, e.is_online ' . $f . ', ct.id AS city_id, e.about_' . $lng . ' AS about
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			INNER JOIN countries cr ON cr.id = e.country_id
			LEFT JOIN regions r ON r.id = eic.region_id
			' . $j . '
			LEFT JOIN escort_cityzones ecz ON ecz.escort_id = eic.escort_id
			LEFT JOIN cityzones cz ON cz.id = ecz.city_zone_id
			LEFT JOIN seo_entity_instances sei ON sei.primary_id = eic.escort_id
			LEFT JOIN seo_entities se ON (se.id = sei.entity_id) OR (se.application_id = 2 AND se.slug = "escort")
			LEFT JOIN escort_services es ON es.escort_id = e.id
			' . (! is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.escort_id
			ORDER BY ' . $order . '
			LIMIT ' . $limit . '
		'; 
		
		$escorts = self::db()->fetchAll($sql);
		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');
		
		
		// <editor-fold defaultstate="collapsed" desc="Make Escort Alt for SEO">
		$tpl_data = array(
			'app_title' => Cubix_Application::getById()->title
		);

		foreach ( $escorts as $i =>  $escort ) {
			$additional = array(
				'showname' => $escort->showname,
				'city' => $escort->city
			);

			foreach ( $tpl_data + $additional as $tpl_var => $tpl_value ) {
				$escorts[$i]->alt = str_replace('%' . $tpl_var . '%', $tpl_value, $escorts[$i]->alt);
			}
		}
		// </editor-fold>

		if ( $sess_name ) {
			// <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
			$sid = 'sedcard_paging_' . Cubix_Application::getId();
			$sess = new Zend_Session_Namespace($sid);

			$sess->{$sess_name} = array();
			foreach ( $escorts as $escort ) {
				$sess->{$sess_name}[] = $escort->id;
			}
			
			$sess->{$sess_name . '_criterias'} = array(
				'filter'	=> $filter,
				'sort'	=> $ordering,
				'page'		=> $page
			);
			// </editor-fold>
		}

		return $escorts;
	}

	public static function getForJson()
	{
		$lng = 'en';

		$sql = '
			SELECT
				e.id,
				e.user_id,
				e.showname, 
				e.age,
				e.gender,
				e.sex_orientation,
				e.height,
				e.weight,

				UNIX_TIMESTAMP(e.date_registered) AS registered_at,
				e.last_hand_verification_date AS last_hand_verification_at,
				e.date_last_modified AS last_modified_at,
				UNIX_TIMESTAMP(e.date_activated) AS activated_at,

				cr.id AS country_id, 
				cr.slug AS country_slug, 
				cr.title_' . $lng . ' AS country_title,

				ct.id AS city_id,
				ct.slug AS city_slug,				
				ct.title_' . $lng . ' AS city_title,
				
				e.is_pornstar,
				e.incall_type, 
				e.outcall_type, 
				e.is_new,
				e.verified_status,
				e.allow_show_online,
				e.is_suspicious, 
				e.is_online,
				eic.is_premium, 
				e.is_vip, 
				e.hh_is_active,
				e.is_on_tour AS is_tour,

				e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,
							
				e.incall_currency, 
				e.incall_price, 
				e.outcall_currency, 
				e.outcall_price, 

				e.comment_count, 
				e.review_count,

				e.email, 
				e.website, 
				e.phone_instr, 
				e.phone_instr_no_withheld, 
				e.phone_instr_other,
				e.phone_country_id, 
				e.disable_phone_prefix, 
				e.phone_number_free AS phone_number,
				
				e.hit_count, 
				e.slogan,
				
				e.tour_date_from, 
				e.tour_date_to, 
				ctt.id AS tour_city_id,
				ctt.slug AS tour_city_slug,
				ctt.title_' . $lng . ' AS tour_city_title,
				
				e.about_' . $lng . ' AS about,

				e.agency_id,
				e.agency_slug,
				e.agency_name,

				e.sex_availability AS services_for,
				e.languages			
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			INNER JOIN countries cr ON cr.id = ct.country_id			
			LEFT JOIN cities ctt ON ctt.id = e.tour_city_id			
			LEFT JOIN currencies cur ON cur.id = e.incall_currency
			LEFT JOIN exchange_rates er ON er.iso = cur.title
			WHERE 1
			AND eic.is_base = 1
			GROUP BY eic.escort_id
		';

		$escorts = self::db()->fetchAll($sql);

		$date_format = 'D M d Y H:i:s O';

		foreach ($escorts as $i => $escort) {
			$escort = new Model_EscortV2Item($escort);

			$escorts[$i]->country = array('id' => $escorts[$i]->country_id, 'slug' => $escorts[$i]->country_slug, 'title' => $escorts[$i]->country_title);
			unset($escorts[$i]->country_id);
			unset($escorts[$i]->country_slug);
			unset($escorts[$i]->country_title);

			$escorts[$i]->city = array('id' => $escorts[$i]->city_id, 'slug' => $escorts[$i]->city_slug, 'title' => $escorts[$i]->city_title);
			unset($escorts[$i]->city_id);
			unset($escorts[$i]->city_slug);
			unset($escorts[$i]->city_title);

			$escorts[$i]->agency = array('id' => $escorts[$i]->agency_id, 'slug' => $escorts[$i]->agency_slug, 'title' => $escorts[$i]->agency_name);
			unset($escorts[$i]->agency_id);
			unset($escorts[$i]->agency_slug);
			unset($escorts[$i]->agency_name);

			if ( $escorts[$i]->is_tour ) {
				$escorts[$i]->tour = array(
					'date' => array(
						'start' => date($date_format, strtotime($escorts[$i]->tour_date_from)), 
						'end' => date($date_format, strtotime($escorts[$i]->tour_date_to))
					), 
					'city' => array(
						'id' => $escorts[$i]->tour_city_id, 
						'slug' => $escorts[$i]->tour_city_slug, 
						'title' => $escorts[$i]->tour_city_title) 
				);
			} else {
				$escorts[$i]->tour = array(
					'date' => array(
						'start' => null, 
						'end' => null
					), 
					'city' => array(
						'id' => null, 
						'slug' => null,
						'title' => null)
				);
			}
			unset($escorts[$i]->tour_date_from);
			unset($escorts[$i]->tour_date_to);
			unset($escorts[$i]->tour_city_id);
			unset($escorts[$i]->tour_city_slug);
			unset($escorts[$i]->tour_city_title);

			$escorts[$i]->image_url = $escort->getMainPhoto()->getUrl('thumb_ed_v3');
			unset($escorts[$i]->photo_hash);
			unset($escorts[$i]->photo_ext);
			unset($escorts[$i]->photo_status);
			unset($escorts[$i]->application_id);
			
			$escorts[$i]->about = mb_convert_encoding(substr( strip_tags($escorts[$i]->about), 0, 450 ), "UTF-8" );

			$keywords = self::db()->fetchAll('SELECT keyword_id AS id FROM escort_keywords WHERE escort_id = ?', $escorts[$i]->id);
			$_keywords = array();
			foreach ($keywords as $key => $keyword) {
				$_keywords[] = $keyword->id;
			}
			$escorts[$i]->keywords = $_keywords;

			$services = self::db()->fetchAll('SELECT service_id AS id FROM escort_services WHERE escort_id = ?', $escorts[$i]->id);
			$_services = array();
			foreach ($services as $key => $service) {
				$_services[] = $service->id;
			}
			$escorts[$i]->services = $_services;

			$escorts[$i]->languages = explode(',', $escorts[$i]->languages);
			$escorts[$i]->services_for = explode(',', $escorts[$i]->services_for);

			$escorts[$i]->registered_at = date($date_format, $escorts[$i]->registered_at);
			if ( $escorts[$i]->last_hand_verification_at ) {
				$escorts[$i]->last_hand_verification_at = date($date_format, strtotime($escorts[$i]->last_hand_verification_at));
			} else {
				$escorts[$i]->last_hand_verification_at = null;
			}
			$escorts[$i]->last_modified_at = date($date_format, strtotime($escorts[$i]->last_modified_at));
			if ( $escorts[$i]->activated_at ) {
				$escorts[$i]->activated_at = date($date_format, $escorts[$i]->activated_at);
			} else {
				$escorts[$i]->activated_at = null;
			}
		}

		return $escorts;
	}
}
