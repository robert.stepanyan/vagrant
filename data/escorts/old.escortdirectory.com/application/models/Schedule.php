<?php
class Model_Schedule extends Cubix_Model
{
	protected $_table = 'schedule';
	
	const FREE_DAY = 0;
	const AVAILABLE_DAY = 1;
	const NON_AVAILABLE_DAY = 2;


	public function getEscortSchedule ( $escort_id, $use_cache = false ){

		if ( !$escort_id ){
			return;
		}
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		if ( $use_cache ){
			$cache = Zend_Registry::get('cache');
			$cache_key = 'escort_schedule_'.$escort_id ;


			if ( (! $schedule = $cache->load($cache_key)) ) {
				
				$schedule = $client->call('Escorts.getSchedule', array($escort_id));
				$cache->save($schedule, $cache_key, array(), 300);
			}
		}else{
			$schedule = $client->call('Escorts.getSchedule', array($escort_id));
		}	

		return  $schedule;

	}

	public function updateSchedule ( $data, $escort_id ){
		
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		return $r = $client->call('Escorts.updateSchedule', array($escort_id, $data));
		
	}

	
}
