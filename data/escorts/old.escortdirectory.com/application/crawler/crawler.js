var SimpleCrawler = require("simplecrawler"),
	MongoClient = require('mongodb').MongoClient,
	nodemailer = require("nodemailer"),
	fs = require('fs'),
	async = require('async'),
	collectionName = 'crawler_results',
	mongoClient,
	parsedPagesCount = 0,
	parsedPhoneNumbersCount = 0,
	parsedEmailsCount = 0;

var Crawler = function() {
	self = this;
	
	this.init = function(callback)
	{
		MongoClient.connect('mongodb://127.0.0.1:27017/ed_crawler', function(err, db) {
			if ( err ) throw err;
			mongoClient = db;
			
			
			callback();
		});
	}
	this.start = function(link, callback)
	{
		var crw = SimpleCrawler.crawl(link);

		crw.on('fetchcomplete', function(queueItem, responseBuffer, response){
			if ( queueItem.stateData.contentType && queueItem.stateData.contentType.indexOf('text/html') != -1 
			) {
				var page = responseBuffer.toString('utf-8');
				self.parseInfo(page, queueItem.host, queueItem.path, function(err) {
					if ( err ) {
						console.log(err);
					} else {
						console.log(queueItem.url + ' parsed.');
					}
				});	
			}
		});
		
		crw.on('fetcherror', function(queueItem, response) {
			console.log('Error occured: ' + queueItem.url);
		});
		
		crw.on('fetchtimeout', function(queueItem, crawlerTimeoutValue ) {
			console.log('timed out');
			return callback();
		});
		
		crw.on('discoverycomplete', function(queueItem, resources) {
			
		});
		
		crw.addFetchCondition(function(parsedURL) {
			return ! (
				parsedURL.path.match(/\.pdf$/i)	||
				parsedURL.path.match(/\.jpg$/i)	||
				parsedURL.path.match(/\.png$/i)	||
				parsedURL.path.match(/\.gif$/i)	||
				parsedURL.path.match(/\.css$/i)	||
				parsedURL.path.match(/\.js$/i)
			);
		});
		
		crw.maxConcurrency = 10;
		crw.userAgent = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36';
		crw.timeout = 20000;
		
		crw.on('complete', function(){
			console.log('complete');
			return callback();
		});
	}
	
	this.parseInfo = function(page, host, path, callback) {
		var phoneNumbers = self.getPhoneNumbers(page);
		var emails = self.getEmails(page);

		mongoClient.collection(collectionName).insert({host: host, path: path}, {w:1}, function(err, obj) {
			if ( err ) {
				return callback(err.message);
			}

			var parsed = 0;
			if ( phoneNumbers && phoneNumbers.length ) {
				self.insertPhoneNumbers(host, path, phoneNumbers, function(err) {
					if ( err ) {
						return callback(err.message);
					}
					if ( ++parsed == 2 ) {
						return callback();
					}
				});
			} else {
				if ( ++parsed == 2 ) {
					return callback();
				}
			}

			if ( emails && emails.length ) {
				self.insertEmails(host, path, emails, function(err) {
					if ( err ) {
						return callback(err.message);
					}

					if ( ++parsed == 2 ) {
						return callback();
					}
				});
			} else {
				if ( ++parsed == 2 ) {
					return callback();
				}
			}
		});
	}
	
	this.getPhoneNumbers = function(html)
	{
		//var phoneNumbers = html.match(/[0-9 \(\)\-\+]{8,18}/gi);
		
		
		//UK phone numbers regex
		//var phoneNumbers = html.match(/(71|73|74|75|77|78|79)\s?\d{2}(\s?\d{3}){2}(?![0-9.])/gi);
		
		//INternational
		//var phoneNumbers = html.match(/(\+|00)(\d|\s|\(|\)){11,16}/gi);
		
		//Poland
		//var phoneNumbers = html.match(/(50|51|53|57|60|66|69|72|73|78|79|88)\d([\s\-]\d{3}){2}/gi);
		
		//French phone numbers regex
		//var phoneNumbers = html.match(/(?:[^0-9])(0|\+33)[67]([-.\s]?[0-9]{2}){4}/gi);
		//var phoneNumbers = html.match(/(?:[^0-9])0[67]([-.\s]?[0-9]{2}){4}/gi);
		
		//Romanian
		//var phoneNumbers = html.match(/\(?((01|02|03|04|05|06|07)\d{2})\)?([-\s.]?\d{3}){2}(?![0-9.])/gi);
		
		//Hungarian
		//var phoneNumbers = html.match(/(20|30|31|70)[\s\/]?\d{3}[\s\-]?(\d{2}[\s\-]?){2}/gi);
		
		//spanish
		//var phoneNumbers = html.match(/((6|7)\d{2})([-\s.]?\d{3}){2}/gi);
		
		//australian
		//var phoneNumbers = html.match(/04\d{2}[-\s.](\d{3}[-\s.]?){2}/gi);
		
		//italian
		//var phoneNumbers = html.match(/3\d{9}/gi);
		
		//ireland
		//var phoneNumbers = html.match(/08\d{1}\s?\d{7}/gi);
		
		//brazilian
		//var phoneNumbers = html.match(/\(?\d{2}\)?([\s\-]{1,3})?(\d{1})?[\s\-\.]?\d{4}[\s\-\.]\d{4}/gi);
		
		//czech
		var phoneNumbers = html.match(/((60|70|72|73|77|79|91)\d{1}\s)(\d{3}\s)(\d){3}/gi);
		
		//russian
		/*var phoneNumbers = html.match(/(\+7|8)([\s-]{1})?9([\s-]{1})?[\d\s-]{9,12}/gi);
		if ( ! phoneNumbers ) phoneNumbers = [];
		
		var phoneNumbers2 = html.match(/(\+7|8)([-\s]{1})?\(9\d{2}\)\s\d{3}([-\s]\d{2}){2}/gi); //+7 (905) 259-50-39
		if ( ! phoneNumbers2 ) phoneNumbers2 = [];
		phoneNumbers = phoneNumbers.concat(phoneNumbers2);
		
		 **/
		
		//portuguese
		//var phoneNumbers = html.match(/9\d{2}([\s\-]?\d{3}){2}/gi);
		
		//slovakian
		//var phoneNumbers = html.match(/(\+421|0)9\d{8}/gi);
		
		//swiss
		//var phoneNumbers = html.match(/07[\d\s]{8,11}/gi);

		//Canadian
		//var phoneNumbers = html.match(/(?:[^0-9]{1})\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})(?![0-9.])/gi);
		
		
		if ( ! phoneNumbers ) return [];
		
		for ( i = 0; i < phoneNumbers.length; i++ ) {
			var num = phoneNumbers[i];
			
			num = num.replace(/[^0-9\+]+/g, "");
			
			if ( num.length > 8 && num.length < 11 ) {
				/*if ( num.indexOf('0') == 0 ) {
					num = num.slice(1, num.length);
				}*/
				phoneNumbers[i] = num;
			} else {
				delete phoneNumbers[i];
			}
		}
		
		return phoneNumbers;
	};
	
	this.getEmails = function(html)
	{
		var emails = html.match(/\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+/gi);
		if ( ! emails ) return [];
		
		for ( i = 0; i < emails.length; i++ ) {
			emails[i] = emails[i].replace(/\s/g, "").toLowerCase();
		}
		
		return emails;
	}
	
	this.insertPhoneNumbers = function(host, path, phoneNumbers, callback)
	{
		var ph = phoneNumbers.slice(0);
		(function insertOne() {
			var phoneNumber = ph.splice(0, 1)[0];
			
			mongoClient.collection(collectionName).find({phoneNumbers: {$in: [phoneNumber]}}).count(function(err, count) {
				if ( err ) {
					return callback(err);
				}

				if ( ! count ) {
					mongoClient.collection(collectionName).update({host: host, path: path}, {$push: {phoneNumbers: phoneNumber}}, {upsert: true}, function(err, res) {
						if (err) {
							return callback(err);
						}
						
						parsedPhoneNumbersCount++;
						if ( ph.length == 0 ) {
							return callback();
						} else {
							insertOne();
						}
						
					});
				} else {
					if ( ph.length == 0 ) {
						return callback();
					} else {
						insertOne();
					}
				}
				
				return callback();

			});
			
		})();
	}
	
	this.insertEmails = function(host, path, emails, callback)
	{
		var em = emails.slice(0);
		(function insertOne() {
			var email = em.splice(0, 1)[0];
			
			mongoClient.collection(collectionName).find({emails: {$in: [email]}}).count(function(err, count) {
				if ( err ) {
					return callback(err);
				}

				if ( ! count ) {
					mongoClient.collection(collectionName).update({host: host, path: path}, {$push: {emails: email}}, {upsert: true}, function(err, res) {
						if (err) {
							return callback(err);
						}
						
						parsedEmailsCount++;
						if ( em.length == 0 ) {
							return callback();
						} else {
							insertOne();
						}
						
					});
				} else {
					if ( em.length == 0 ) {
						return callback();
					}{
						insertOne();
					}
				}
				return callback();
				
			});
			
		})();
	}
	
	this.sendEmails = function()
	{
		return;
		var smtpTransport = nodemailer.createTransport("SMTP", {
			service: 'ESCORTGUIDE.CO.UK',
			host: "smtp.escortguide-news.co.uk",
			port: 25,
			auth: {
				user: "info@escortguide-news.co.uk",
				pass: "EGtr#28Q92Lx"
			}
		});
		
		
		var template = fs.readFileSync('template.html', "utf-8");
		
		var mailOptions = {
			from: "brandy@escortguide.co.uk",
			subject: "Free advertising for YOU !",
			html: template
		}
		
		smtpTransport.sendMail(mailOptions, function(error, response){
			if(error){
				console.log(error);
			} else{
				console.log('sended');
			}
		});
		
		
		
		cursor = mongoClient.collection(collectionName).find({'$where': 'this.emails && this.emails.length > 0'}, ['emails']);
		cursor.each(function(err, doc) {
			if ( err ) {
				console.log(err);
			}
			if ( doc && doc.emails ) {
				for(i in doc.emails) {
					(function(m) {
						mailOptions.to = m;
						smtpTransport.sendMail(mailOptions, function(error, response){
							if(error){
								console.log(error);
							}else{
								console.log("Message sent: " + m);
							}
						});
					}(doc.emails[i]));
				}
			}
		});
		
		
		
		/*emails = ['kvahagn@gmail.com', 'sharp17s@hushmail.com', 'dave.kasparov@gmail.com'];
		for ( i in emails) {
			(function(m) {
				mailOptions.to = m;
				smtpTransport.sendMail(mailOptions, function(error, response){
					if(error){
						console.log(error);
					}else {
						console.log('Sent to ' + m);
					}
				});
			}(emails[i]));
		}*/
		
	}
	this.exportPhoneNumbers = function()
	{
		cursor = mongoClient.collection(collectionName).find({phoneNumbers: {$exists: true}}, ['phoneNumbers']);
		
		var arr = [];
		cursor.toArray(function(err, docs) {
			for( j in docs ) {
				if ( docs[j].phoneNumbers && docs[j].phoneNumbers.length > 0 ) {
					for( i in docs[j].phoneNumbers) {
						pnumb = docs[j].phoneNumbers[i];
						/*docs[j].phoneNumbers[i] = docs[j].phoneNumbers[i].replace(/\s/g, '');
						docs[j].phoneNumbers[i] = docs[j].phoneNumbers[i].replace(/\D/g,'');
						
						if ( docs[j].phoneNumbers[i].length > 12 && docs[j].phoneNumbers[i].length < 15 ) {
							arr.push(docs[j].phoneNumbers[i]);
						}*/
						
						arr.push(docs[j].phoneNumbers[i]);
					}
				}
			}
			
			fs.writeFile('phone_numbers.txt', arr.join('\n'), function(err) {
				if ( err ) {
					console.log(err);
				} else {
					console.log('Phone numbers done');
				}
				
				mongoClient.close();
			});
		});
	}
	
	
	this.exportEmails = function()
	{
		cursor = mongoClient.collection(collectionName).find({emails: {$exists: true}, $where: 'this.emails.length > 0'}, ['emails']);
		
		var arr = [];
		cursor.toArray(function(err, docs) {
			for( j in docs ) {
				if ( docs[j].emails ) {
					for( i in docs[j].emails) {
						arr.push(docs[j].emails[i]);
					}
				}
			}
			
			fs.writeFile('emails.txt', arr.join('\n'), function(err) {
				if ( err ) {
					console.log(err);
				} else {
					console.log('Email export done');
				}
				
				mongoClient.close();
			});
		});
	}
}

var crawler = new Crawler();
crawler.init(function() {
	crawler.exportPhoneNumbers();
	crawler.exportEmails();
	return;
	
	async.eachLimit([
		//'http://www.escort-guide.cz',
		//'https://www.eroticprague.com',
		'https://escorts-prague.eu'
	], 1, crawler.start, function(err) {
		if ( err ) {
			console.log(err);
		}
		mongoClient.close();
	});
	
	setInterval(function () {
		if (typeof gc === 'function') {
			gc();
		}
		console.log('Memory Usage', process.memoryUsage());
	}, 60000);
	
});