<?php

class Mobile_EscortsController extends Zend_Controller_Action
{
	public function init()
	{
		$this->_helper->layout->setLayout('mobile');
		
        $_SESSION['request_url'] = $_SERVER['REQUEST_URI'];
		//die('Mobile version is temporary unavailable');
	}

    public function contactFormAction()
    {
        $request = $this->_request;

        $this->view->id = $request->escort_id;
        $this->view->type = $request->type;
        $this->view->layout()->disableLayout();
    }

    public function indexAction()
    {
        if ( ! is_null($this->_getParam('ajax')) ) {
           // $this->_helper->viewRenderer->setScriptAction('popup');
            $this->view->layout()->disableLayout();
            $this->view->is_ajax = true;
        }
        // </editor-fold>

        //$s_showname = $this->view->s_showname = $this->_request->showname;
        $top_search_input = $this->view->top_search_input = $this->_request->top_search_input;
        $ts_type = $this->view->ts_type = $this->_request->ts_type;
        $ts_slug = $this->view->ts_slug = $this->_request->ts_slug;

        $show_all_hidden_escorts = $this->view->show_all_hidden_escorts = (int) $this->_request->getParam('sahe', 0);

        $s_name = $this->view->s_name = $this->_request->name;
        $s_agency = $this->view->s_agency = $this->_request->agency;
        $s_agency_slug = $this->view->s_agency_slug = $this->_request->agency_slug;
        $s_gender = $this->view->s_gender = $this->_request->getParam('gender', array());
        $s_city = $this->view->s_city = $this->_request->city;
        $s_city_slug = $this->view->s_city_slug = $this->_request->city_slug;
        $s_phone = $this->view->s_phone = $this->_request->getParam('phone', null);
        $s_age_from = $this->view->s_age_from = (int)$this->_request->age_from;
        $s_age_to = $this->view->s_age_to = (int)$this->_request->age_to;
        $s_orientation = $this->view->s_orientation = (array) $this->_request->orientation;
        $s_keywords = $this->view->s_keywords = (array) $this->_request->keywords;
        $s_height_from = $this->view->s_height_from = (int)$this->_request->height_from;
        $s_height_to = $this->view->s_height_to = (int)$this->_request->height_to;
        $s_weight_from = $this->view->s_weight_from = (int)$this->_request->weight_from;
        $s_weight_to = $this->view->s_weight_to = (int)$this->_request->weight_to;
        $s_tatoo = $this->view->s_tatoo = (int)$this->_request->tatoo;
        $s_piercing = $this->view->s_piercing = (int)$this->_request->piercing;

        $s_ethnicity = $this->view->s_ethnicity = (array) $this->_request->ethnicity;
        $s_language = $this->view->s_language = (array) $this->_request->language;
        $s_hair_color = $this->view->s_hair_color = (array)$this->_request->hair_color;
        $s_hair_length = $this->view->s_hair_length = (array)$this->_request->hair_length;
        $s_cup_size = $this->view->s_cup_size = (array) $this->_request->cup_size;
        $s_eye_color = $this->view->s_eye_color = (array)$this->_request->eye_color;
        $s_pubic_hair = $this->view->s_pubic_hair = (array) $this->_request->pubic_hair;

        $s_currency = $this->view->s_currency = $this->_request->s_currency;
        $s_price_from = $this->view->s_price_from = (int)$this->_request->price_from;
        $s_price_to = $this->view->s_price_to = (int)$this->_request->price_to;
        $s_sex_availability = $this->view->s_sex_availability = (array) $this->_request->sex_availability;
        $s_service = $this->view->s_service = $this->_request->getParam('service', array());

        $s_incall_other = $this->view->s_incall_other = (int)$this->_request->incall_other;
        $s_outcall_other = $this->view->s_outcall_other = (int)$this->_request->outcall_other;
        $s_incall = $this->view->s_incall = (array)$this->_request->incall;
        $s_incall_hotel_room = $this->view->s_incall_hotel_room = (array) $this->_request->incall_hotel_room;
        $s_outcall = $this->view->s_outcall = (array)$this->_request->outcall;
        $s_travel_place = $this->view->s_travel_place = (array) $this->_request->travel_place;

        $s_real_pics = $this->view->s_real_pics = (int)$this->_request->s_real_pics;
        $s_verified_contact = $this->view->s_verified_contact = (int)$this->_request->s_verified_contact;
        $s_with_video = $this->view->s_with_video = (int)$this->_request->s_with_video;
        $s_pornstar = $this->view->s_pornstar = (int)$this->_request->s_pornstar;
        $s_is_online_now = $this->view->s_is_online_now = (int)$this->_request->s_is_online_now;
        $online_now = $this->view->online_now = (int)$this->_request->online_now;

        $this->view->p_id = $this->_request->getParam('p_id');

        $segmented = $this->view->segmented = $this->_getParam('segmented', 0);

        $s_filter_params = array(
            'name' => $s_name,
            'agency-name' => $s_agency_slug,
            'gender' => $s_gender,
            'city' => $s_city_slug,
            'phone' => ($s_phone) ? "%" . $s_phone . "%" : '',
            'age-from' => $s_age_from,
            'age-to' => $s_age_to,
            'orientation' => $s_orientation,
            'keywords' => $s_keywords,
            'height-from' => $s_height_from,
            'height-to' => $s_height_to,
            'weight-from' => $s_weight_from,
            'weight-to' => $s_weight_to,
            'tatoo' => $s_tatoo,
            'piercing' => $s_piercing,

            'ethnicity' => $s_ethnicity,
            'language' => $s_language,
            'hair-color' => $s_hair_color,
            'hair-length' => $s_hair_length,
            'cup-size' => $s_cup_size,
            'eye-color' => $s_eye_color,
            'pubic-hair' => $s_pubic_hair,

            'currency' => $s_currency,
            'price-from' => $s_price_from,
            'price-to' => $s_price_to,
            'sex-availability' => $s_sex_availability,
            'service' => $s_service,

            'incall-other' => $s_incall_other,
            'outcall-other' => $s_outcall_other,
            'incall' => $s_incall,
            'incall-hotel-room' => $s_incall_hotel_room,
            'outcall' => $s_outcall,
            'travel-place' => $s_travel_place,

            'real-pics' => $s_real_pics,
            'verified-contact' => $s_verified_contact,
            'with-video' => $s_with_video,
            'pornstar' => $s_pornstar,
            'is-online-now' => $s_is_online_now,
            'show-all-hidden-escorts' => $show_all_hidden_escorts,
        );

        if ( $ts_type == 'city' ) {
            $s_filter_params['city'] = $ts_slug;
        } else if ( $ts_type == 'escort' ) {
            $s_filter_params['name'] = $ts_slug;
        } else if ( $ts_type == 'escort-see-all' ) {
            $s_filter_params['name'] = $ts_slug . "%";
        }

        //print_r($s_filter_params['name']); die;

        $is_active_filter = false;
        foreach( $s_filter_params as $i => $par ) {
            if ( is_array($par) ) {
                if( count($par) ) {
                    $is_active_filter = true;
                    break;
                }
            } else {
                if( $par ) {
                    $is_active_filter = true;
                    break;
                }
            }
        }

        // SEARCH PARAMS

        $req = $this->_getParam('req');
        $req = explode('/', $req);

        $cache = Zend_Registry::get('cache');
        $defs = Zend_Registry::get('definitions');

        $params = array(
            'sort' => 'close-to-me',
            'filter' => array(array('field' => 'girls', 'value' => array())),
            'page' => 1
        );

        $static_page_key = 'main';
        $is_tour = false;

        $s_config = Zend_Registry::get('system_config');
        $this->view->showMainPremiumSpot = $s_config['showMainPremiumSpot'];

        if ( ! $s_config['showMainPremiumSpot'] ) {
            $static_page_key = 'regular';
        }

        if ( ! $is_active_filter ) {
            $p_top_category = $this->view->p_top_category = $this->_request->getParam('p_top_category', 'escorts');
            $p_city_slug = $this->view->p_city_slug = $this->_request->p_city_slug;
            $p_city_id = $this->view->p_city_id = (int) $this->_request->p_city_id;

            $p_country_slug = $this->view->p_country_slug = $this->_request->p_country_slug;
            $p_country_id = $this->view->p_country_id = (int) $this->_request->p_country_id;

            $f_top_category = $this->_request->getParam('f_top_category');

            if ( $f_top_category ) {
                $p_top_category = $this->view->f_top_category = $f_top_category;
            }
        }

        if ( $p_top_category ) {

            switch( $p_top_category )
            {
                case 'escorts':
                    $static_page_key = 'escorts';
                    $params['filter'][] = array('field' => 'escorts', 'value' => array());
                    continue;
                case 'escorts-only':
                    $static_page_key = 'escorts-only';
                    $params['filter'][] = array('field' => 'escorts-only', 'value' => array());
                    continue;

                case 'independent-escorts':
                    $static_page_key = 'escorts-only';
                    $params['filter'][] = array('field' => 'independent-escorts', 'value' => array());
                    continue;
                case 'agency-escorts':
                    $static_page_key = 'escorts-only';
                    $params['filter'][] = array('field' => 'agency', 'value' => array());
                    continue;
                case 'agencies':
                    $static_page_key = 'agencies';
                    $params['filter'][] = array('field' => 'agencies', 'value' => array());
                    continue;

                case 'bdsm':
                    $static_page_key = 'bdsm';
                    $params['filter'][0] = array('field' => 'bdsm', 'value' => array());
                    continue;
                case 'boys':
                    $static_page_key = 'boys';
                    $params['filter'][0] = array('field' => 'boys', 'value' => array());
                    continue;
				case 'trans':
                    $static_page_key = 'trans';
                    $params['filter'][0] = array('field' => 'trans', 'value' => array());
                    continue;
                case 'regular':
                    $static_page_key = 'regular';
                    $params['filter'][] = array('field' => 'regular', 'value' => array());
                    continue;
                default:
                    $this->_response->setRawHeader('HTTP/1.1 404 Not Found');
                    $this->_forward('error', 'error', null, array('error_msg' => '404 Page Not Found. Please try the following links instead.'));
                    return;
            }
        }

        $list_types = array('gallery', 'list', 'simple');

        if ( isset($_COOKIE['list_type']) && $_COOKIE['list_type'] ) {
            $list_type = $_COOKIE['list_type'];

            if ( ! in_array($list_type, $list_types) ) {
                $list_type = $list_types[0];
            }
        }

        $this->view->grouped_categories = $grouped_categories = array(
//            'escorts',
//            'agencies',
        );

        $show_grouped = false;
//        if ( $list_type == 'simple' || in_array($static_page_key, $grouped_categories) ) {
//            $show_grouped = true;
//        }


        $this->view->show_grouped = $show_grouped;

        /*if ( $static_page_key == 'new' ) {
            $this->_request->setParam('country_id', $n_country_id);
            $this->_forward('new-list');
        }*/

        $this->view->in_city = false;

        if ( $p_city_slug ) {
            $this->view->in_city = true;
            $params['city'] = $p_city_slug;
            $params['filter'][] = array('field' => 'city_id', 'value' => array($p_city_id));
            $this->view->city_id = $p_city_id;
            $model_country = new Model_Countries();
            $this->view->country_id = $model_country->getCountryIdByCityId($p_city_id);
        }

        if ( $p_country_slug ) {
            $params['country'] = $p_country_slug;
            $params['filter'][] = array('field' => 'country_id', 'value' => array($p_country_id));
            $this->view->country_id = $p_country_id;
        }

        $this->view->static_page_key = $static_page_key;

        $filter_params = array(
            'order' => 'e.date_registered DESC',
            'limit' => array('page' => 1),
            'filter' => array()
        );

        // <editor-fold defaultstate="collapsed" desc="Map parameters from url to SQL where clauses">
        $filter_map = array(

            'name' => 'e.showname LIKE ?',
            'agency-name' => 'e.agency_slug = ?',
            'phone' => 'e.contact_phone_parsed LIKE ?',
            'age-from' => 'e.age >= ?',
            'age-to' => 'e.age <= ?',
            'height-from' => 'e.height >= ?',
            'height-to' => 'e.height <= ?',
            'weight-from' => 'e.weight >= ?',
            'weight-to' => 'e.weight <= ?',
            'tatoo' => 'e.tatoo = ?',
            'piercing' => 'e.piercing = ?',

            'real-pics' => 'e.verified_status = 2',
            'with-video' => 'with-video',
            'verified-contact' => 'e.last_hand_verification_date IS NOT NULL',
            'pornstar' => 'e.is_pornstar = 1',

            'incall-other' => 'e.incall_other IS NOT NULL AND e.incall_other <> ""',
            'outcall-other' => 'e.outcall_other IS NOT NULL AND e.outcall_other <> ""',

            //'ethnicity' => 'e.ethnicity = ?',

//            'gender' => 'e.gender IN (?)',
            //'height' => 'e.height < ? AND e.height > ?',
            //'weight' => 'e.weight < ? AND e.weight > ?',
            //'cup-size' => 'e.cup_size = ?',
            //'hair-color' => 'e.hair_color = ?',
            //'eye-color' => 'e.eye_color = ?',
            //'dress-size' => 'e.dress_size = ?',
            //'shoe-size' => 'e.shoe_size = ?',
            //'incall' => 'e.incall_type IS NOT NULL',
            //'outcall' => 'e.outcall_type IS NOT NULL',
            //'service-for' => 'FIND_IN_SET(?, e.sex_availability)',
            //'smoker' => 'e.is_smoking = ?',
            //'language' => 'FIND_IN_SET(?, e.languages)',
            //'orientation' => 'e.sex_orientation = ?',
            //'now-open' => 'e.is_now_open',

            'region' => 'r.slug = ?',
            'city' => 'ct.slug = ?',
            'city_id' => 'ct.id = ?',
            'country_id' => 'cr.id = ?',
            'country' => 'cr.slug = ?',
            'cityzone' => 'c.id = ?',
            'zone' => 'cz.slug = ?',



            'boys-heterosexual' => 'e.sex_orientation = ' . ORIENTATION_HETEROSEXUAL . ' AND e.gender = ' . GENDER_MALE,
            'boys-bisexual' => 'e.sex_orientation = ' . ORIENTATION_BISEXUAL . ' AND e.gender = ' . GENDER_MALE,
            'boys-homosexual' => 'e.sex_orientation = ' . ORIENTATION_HOMOSEXUAL . ' AND e.gender = ' . GENDER_MALE,


            'escorts' => 'eic.gender = ' . GENDER_FEMALE,
            'escorts-only' => 'eic.is_agency = 0 AND eic.gender = ' . GENDER_FEMALE,
            'agencies' => 'eic.is_agency = 1',
            'independent-escorts' => 'eic.is_agency = 0 AND eic.gender = ' . GENDER_FEMALE,
            'agency' => 'eic.is_agency = 1 AND eic.gender = ' . GENDER_FEMALE,
            'boys-trans' => '(eic.gender = ' . GENDER_MALE . ' OR eic.gender = ' . GENDER_TRANS . ')',
            'boys' => 'eic.gender = ' . GENDER_MALE,
            'trans' => 'eic.gender = ' . GENDER_TRANS,
            'bdsm' => 'eic.gender = ' . GENDER_BDSM,

            'newest' => 'eic.gender = ' . GENDER_FEMALE,
            'newest-independent' => 'eic.is_agency = 0 AND eic.gender = ' . GENDER_FEMALE,
            'newest-agency-escorts' => 'eic.is_agency = 1 AND eic.gender = ' . GENDER_FEMALE,

            'tours' => 'eic.is_tour = 1',
            'upcomingtours' => 'eic.is_upcoming = 1'
        );

        // <editor-fold defaultstate="collapsed" desc="Get page from request parameters">
        if ( $this->_request->page ) {
            $params['page'] = $this->_request->page;
        }

        if ( $this->_request->p_page && ! $this->_request->page ) {
            $params['page'] = $this->_request->p_page;
        }

        $page = intval($params['page']);
        if ( $page == 0 ) {
            $page = 1;
        }
        $filter_params['limit']['page'] = $this->view->page = $page;
        // </editor-fold>

        //$selected_filter = $menus['filter']->getSelected();

        foreach ( $params['filter'] as $i => $filter ) {

            if ( ! isset($filter_map[$filter['field']]) ) continue;

            $value = $filter['value'];

            if ( isset($filter['main']) ) {
                if ( isset($selected_filter->internal_value) ) {
                    $value = $selected_filter->internal_value;
                }
                elseif ( ! is_null($item = $menus['filter']->getByValue($filter['field'] . ( (isset($value[0]) && $value[0]) ? '_' . $value[0] : '')) ) ) {
                    $value = $item->internal_value;
                }

            }

            $filter_params['filter'][$filter_map[$filter['field']]] = $value;
        }

        $filter_params['filter']['online-now'] = $online_now;

        foreach ( $s_filter_params as $i => $s_filter ) {

            if ( ! isset($filter_map[$i]) ) continue;

            if ( $s_filter ) {
                $filter_params['filter'][$filter_map[$i]] = $s_filter;
            }
        }
        // <editor-fold defaultstate="collapsed" desc="Sorting parameters SQL mappings">

        $default_sorting = "close-to-me";
        $current_sorting = $this->_getParam('sort', $default_sorting);


        $sort_map = array(
            'newest' => 'date_registered DESC',
            'last-contact-verification' => 'last_hand_verification_date DESC',
            'price-asc' => 'price-asc', // will be modified from escorts
            'price-desc' => 'price-desc', // will be modified from escorts
            'last_modified' => 'date_last_modified DESC',
            'last-connection' => 'refresh_date DESC',
            'close-to-me' => 'ordering DESC',
            'most-viewed' => 'hit_count DESC',
            'random' => 'ordering DESC',
            'by-country' => 'ordering DESC',
            'by-city' => 'ordering DESC',
            'alpha' => 'showname ASC',
            'age' => '-age DESC',
        );

        if ( !array_key_exists($current_sorting, $sort_map) ) {
            $current_sorting = $default_sorting;
            $this->_request->setParam('sort', $default_sorting);
        }

        if ( $current_sorting != $_COOKIE['sorting'] && $this->_request->s ) {
            setcookie("sorting", $current_sorting, strtotime('+1 year'), "/", "." . preg_replace('#www.|dev.#', '', $_SERVER['HTTP_HOST']));
            setcookie("sortingMap", $sort_map[$current_sorting]['map'], strtotime('+1 year'), "/", "." . preg_replace('#www.|dev.#', '', $_SERVER['HTTP_HOST']));
            $_COOKIE['sorting'] = $current_sorting;
            $_COOKIE['sortingMap'] = $sort_map[$current_sorting]['map'];
        }

        if ( isset($_COOKIE['sorting']) /*&& ! $this->_request->isPost()*/ ) {
            $current_sorting = $_COOKIE['sorting'];
            $this->_request->setParam('sort', $current_sorting);
        }

        if ( $current_sorting ) {
            $tmp = $sort_map[$current_sorting];
            unset($sort_map[$current_sorting]);
            $sort_map = array_merge(array( $current_sorting => $tmp), $sort_map);
        }

        if ( isset($sort_map[$params['sort']]) ) {
            $filter_params['order'] = $sort_map[$params['sort']];
            $this->view->sort_param = $params['sort'];
        }

        $this->view->sort_map = $sort_map;
        $this->view->current_sort = $current_sorting;
        $params['sort'] = $current_sorting;
        // </editor-fold>

        $model = new Model_EscortsV2();
        $count = 0;

        // <editor-fold defaultstate="collapsed" desc="Cache key generation">
        $filters_str = '';

        foreach ( $filter_params['filter'] as $k => $filter ) {
            if ( ! is_array($filter) ) {
                $filters_str .= $k . '_' . $filter;
            }
            else {
                $filters_str .= $k;
                foreach($filter as $f) {
                    $filters_str .= '_' . $f;
                }
            }
        }
        // </editor-fold>

        $cache_key = 'v2_' . Cubix_I18n::getLang() . '_' . $params['sort'] . '_' . $filter_params['limit']['page'] . '_' . $filters_str . (string) $has_filter;
        $cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
        $this->view->params = $params;

        $this->view->is_main_page = $is_main_page = ($static_page_key == 'main' && ( ! isset($params['city']) && ! isset($params['zone']) && ! isset($params['region']) ));

        $s_config = Zend_Registry::get('system_config');
        $e_config = Zend_Registry::get('escorts_config');

        $this->view->per_page = $per_page = 14 /* For mobile version, default $e_config['perPage']*/;
        $online_now = $this->view->online_now = (int)$this->_request->online_now;

        $segmented = $this->view->segmented = $this->_getParam('segmented', 0);

        if($this->getRequest()->getMethod() == 'GET'){
            if( isset($_COOKIE['page']) ){
                $per_page = $_COOKIE['page'] * $per_page;
                $filter_params['limit']['page'] = 1;
            }
        } else {
//            $per_page = $filter_params['limit']['page'] * $per_page;
//            $filter_params['limit']['page'] = 1;
        }

            //        if($this->getRequest()->getMethod() == 'GET'){
//            $url = $_SERVER["REQUEST_URI"];
//            $parsed = parse_url( $url );
//            print_r($parsed); die();
//        }


        // If we are on main premium spot forward to corresponding action
        if ( ! $s_config['showMainPremiumSpot'] ) {
            if ( $is_main_page ) {
                return $this->_forward('main-page');
            }
        }

        /* Ordering by country from GeoIp */
        if ($params['sort'] == 'random')
        {
            $modelC = new Model_Countries();

            $ret = $modelC->getByGeoIp();
            //if ($_GET['test'] == 1) { var_dump($ret);	die;		}
            if (is_array($ret))
            {
                $cache_key .= '_country_iso_' . $ret['country_iso'];
                $params['sort'] = $ret['ordering'];
            }
        }
        /**/

        foreach($s_filter_params as $k => $value ) {
            if ( is_array($value) ) {
                foreach( $value as $val ) {
                    $cache_key .= '_' . $val . '_';
                }
            } else {
                $cache_key .= '_' . $k . '_' . $value;
            }
        }

        if ( $show_grouped ) {
            $cache_key .= '_grouped';
        }

        $geoData = array();
        if ( $params['sort'] == 'close-to-me' ) {
            $geoData = Cubix_Geoip::getClientLocation();

            if ( strlen($geoData['country']) ) {
                $geo_cache = md5($geoData['latitude'] . $geoData['longitude']);
                $cache_key .= $geo_cache;
            }
        }

        $cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
        $cache_key_count = $cache_key . '_count';
        $cache_key = trim( $cache_key );
//        echo $cache_key;

        $count = $cache->load($cache_key_count);

        //print_r( $cache ); die();
        //if ( ! $escorts = $cache->load($cache_key) ) {

            if ( isset($is_tour) && $is_tour ) {
                $escorts = Model_Escort_Mobile_List::getTours($upcoming_tours, $filter_params['filter'], $params['sort'], $filter_params['limit']['page'], $e_config['perPage'], $count);
            }
            else {
                if (!$has_filter) {
                    $filter_params['filter']['e.is_main_premium_spot = 0'] = array();
                }

                //print_r( $filter_params['limit']['page'] ); die();

                $escorts = Model_Escort_Mobile_List::getFiltered($filter_params['filter'], $params['sort'], $filter_params['limit']['page'], /*$e_config['perPage']*/ $per_page, $count, 'regular_list', false, $s_filter_params, $show_grouped, $geoData);

                //echo $params['sort'];
                //$escorts_vip = Model_Escort_Mobile_List::getFiltered($filter_params['filter'], $params['sort'], $filter_params['limit']['page'], /*$e_config['perPage']*/ $per_page, $count, 'regular_list', false, $s_filter_params, $show_grouped);
            }

            $cache->save($escorts, $cache_key, array());
            $cache->save($count, $cache_key_count, array());
        //}

        $n_cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key) . '_newest';
        if ( ! $newest_escorts = $cache->load($n_cache_key) ) {
            if (!$has_filter) {
                $filter_params['filter']['e.is_main_premium_spot = 0'] = array();
            }

            $filter_params['filter']['e.is_new = 1'] = array();

            $newest_escorts = Model_Escort_Mobile_List::getFiltered($filter_params['filter'], 'newest', 1,/*$config['perPage']*/ $per_page, $count, 'regular_list', false, $s_filter_params);

            $cache->save($newest_escorts, $n_cache_key, array());
        }

        if ( $is_main_page && $filter_params['order'] == 'ordering DESC' ) {
            shuffle($escorts);
        }

        if ( count( $escorts ) > 0 ){
            // <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
            $sess_name = "prev_next";
            $sid = 'sedcard_paging_' . Cubix_Application::getId();
            $sess = new Zend_Session_Namespace($sid);

            $sess->{$sess_name} = array();
            foreach ( $escorts as $escort ) {
                //$sess->{$sess_name}[] = $escort->showname;
                $sess->{$sess_name}[] = $escort->id;
            }
            // </editor-fold>
        }

        $this->view->count = $count;

        $this->view->escorts = $escorts;
        $this->view->has_filter = $has_filter;
        $this->view->newest_escorts = $newest_escorts;

        if ( ! is_null($this->_getParam('ajax')) ) {
            $this->view->is_ajax = true;

            //if( !$this->_getParam('ajax') ){
                if ( $show_grouped ) {
                    $this->view->template = 'list-group';
                    $body = $this->view->render('escorts/list-grouped.phtml');
                } else {
                    $this->view->template = 'list';
                    $body = $this->view->render('escorts/list.phtml');

                }
            //}

            $entity = $static_page_key;

            if ( isset($params['city']) ) {
                $entity = 'city-' . $static_page_key;
            }

            if ( isset($entity) ) {
                $tpl_data = array();
                $primary_id = 0;

                switch ( $entity ) {
                    case 'city':
                    case 'city-escorts':
                    case 'city-independent-escorts':
                    case 'city-agency':
                    case 'city-bdsm':
                    case 'city-boys-trans':
                        $m = new Cubix_Geography_Cities();
                        $city = $m->getBySlug($params['city']);

                        if ( ! $city ) break;

                        $primary_id = $city->id;
                        $tpl_data['city'] = $city->title;
                        $tpl_data['region'] = $city->getRegionTitle();

                        break;
                }

                $seo = $this->view->seo($entity, $primary_id, $tpl_data);
            }

            //die(json_encode(array('head_title' => $seo->title, 'body' => $body)));
        }
    }

    public function profileV2Action()
    {
        if ( ! is_null($this->_getParam('ajax')) ) {
            //$this->_helper->viewRenderer->setScriptAction('popup');
            $this->view->layout()->disableLayout();
            $this->view->is_ajax = true;
        }

        $this->_helper->viewRenderer->setScriptAction('popup');
//        $this->view->layout()->disableLayout();
        $this->view->layout()->setLayout('mobile');
        $this->view->showSliderFiles = true;
//        $this->_helper->viewRenderer->setScriptAction("profile-v2/profile");

        $this->view->is_profile = true;

        //////   redirect not active escorts to home page   ///////
        $m = new Model_EscortsV2();
        if ($this->_getParam('f') != 'b' && !$m->getShownameById($this->_getParam('escort_id')))
        {
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: /');
            die;
        }
        //////////////////////////////////////////////////////////

        $showname = $this->_getParam('escortName');
        $escort_id = $this->_getParam('escort_id');

        $model = new Model_EscortsV2();

        $cache_key = 'v2_' . $showname . '_' . $escort_id .  '_profile_' . Cubix_I18n::getLang();
        $cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

        $this->view->profile_cache_key = 'html_profile_' . $cache_key;

        //$escort = $model->get($showname, $cache_key);
        $escort = $model->get($escort_id, $cache_key);

        if ($escort->showname != $showname)
        {
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: /');
            die;
        }

        if ( ! $escort || isset($escort['error']) )
        {
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: /');
            die;
            return;
        }

        /*if ( isset($escort->escort_status) && $escort->escort_status != 32 ) {
            $this->_forward('profile-disabled');
            $this->_request->setParam('city_slug', $escort->city_slug);
        }*/

        $blockModel = new Model_BlockedCountries();
        if ( $blockModel->checkIp($escort->id) ){
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: /');
            die;
            return;
        }

        $vacation = new Model_EscortV2Item( array('id' => $escort->id  ) );
        $vac = $vacation->getVacation();
        $add_esc_data = $model->getRevComById($escort->id);
        $bl = $model->getBlockWebsite($escort->id);
        $block_website = $bl->block_website;
        $check_website = $bl->check_website;

        $escort->block_website = $block_website;
        $escort->check_website = $check_website;
        $escort->disabled_reviews = $add_esc_data->disabled_reviews;
        $escort->disabled_comments = $add_esc_data->disabled_comments;
        $escort->vac_date_from = $vac->vac_date_from;
        $escort->vac_date_to = $vac->vac_date_to;
        $this->view->escort_id = $escort->id;
        $this->view->escort = $escort;

        $this->view->natural_pic = $model->getNaturalPic($escort->id);

        if ( $escort->home_city_id ) {
            $m_city = new Cubix_Geography_Cities();
            $home_city = $m_city->get($escort->home_city_id);

            $m_country = new Cubix_Geography_Countries();
            $home_country = $m_country->get($home_city->country_id);

            $this->view->home_city_country = $home_city->{'title_' . $this->_request->lang_id} . ' (' . $home_country->{'title_' . $this->_request->lang_id} . ')';
        }

        $bt = $model->getBubbleText($escort->id);
        $this->view->bubble_text = $bt ? $bt->bubble_text : null;


        $this->view->travel_place = $model->getTravelPlace($escort->id);
        /* Last Review */
        $cache = Zend_Registry::get('cache');
        $cache_key = 'reviews_' . $escort->id;
        if ( ! $res = $cache->load($cache_key) ) {
            try {
                $res = $model->getEscortLastReview($escort->id);

                //$res = Cubix_Api::getInstance()->call('getEscortLastReview', array($escort->id));
            }
            catch ( Exception $e ) {
                $res = array(false, false);
            }

            $cache->save($res, $cache_key, array());
        }

        list($review, $rev_count) = $res;
        $this->view->review = $review;
        $this->view->rev_count = $rev_count;
        /* Last Review */

        $this->photosV2Action($escort);

        $scheduleModel = new Model_Schedule();
        //$this->view->schedules = $scheduleModel->getEscortSchedule( $escort->id, true );
//
//        $this->view->schedules = $scheduleModel->getEscortSchedule( $escort->id, true );

        /*if ( $escort->agency_id )
            $this->agencyEscortsV2Action($escort->agency_id, $escort->id);*/

        $user = Model_Users::getCurrent();
        $this->view->user = $user;

        $paging_key = $this->_getParam('from');
        $paging_keys_map = array(
            'last_viewed_escorts',
            'search_list',
            'regular_list',
            'new_list',
            'profile_disabled_list',
            'premiums',
            'right_premiums',
            'main_premium_spot',
            'tours_list',
            'tour_main_premium_spot',
            'up_tours_list',
            'up_tour_main_premium_spot'
        );

        if ( in_array($paging_key, $paging_keys_map) ) {
            $sid = 'sedcard_paging_' . Cubix_Application::getId();
            $ses = new Zend_Session_Namespace($sid);

            $ses_pages = $ses->{$paging_key};
            $criterias = $ses->{$paging_key . '_criterias'};

            //echo '/'; print_r( $criterias ); echo '/';

            if ( ! isset($criterias['first_page']) ) {
                $criterias['first_page'] = $criterias['page'];
            }

            if ( ! isset($criterias['last_page']) ) {
                $criterias['last_page'] = $criterias['page'];
            }

            $ses_index = 0;
            if ( is_array($ses_pages) ) {
                $ses_index = array_search($escort_id, $ses_pages);
            }

            if ( isset($ses_pages[$ses_index - 1]) ) {
                $this->view->back_showname = $model->getShownameById($ses_pages[$ses_index - 1]);
                $this->view->back_id = $back_id = $ses_pages[$ses_index - 1];
                //$this->view->back_city = $model->getCityByEscortId($back_id);
            } elseif ( $criterias['first_page'] > 1 ) { //Loading previous page escorts
                switch( $paging_key ) {
                    case 'regular_list' :
                        $prev_page_escorts = Model_Escort_Mobile_List::getFiltered($criterias['filter'], $criterias['sort'], $criterias['first_page'] - 1);
                        break;
                    case 'tours_list':
                        $prev_page_escorts = Model_Escort_Mobile_List::getTours($criterias['is_upcoming'], $criterias['filter'], $criterias['sort'], $criterias['first_page'] - 1);
                        break;
                }

                $p_escorts_count = count($prev_page_escorts);
                if ( count($p_escorts_count) ) {
                    $this->view->back_showname = $prev_page_escorts[$p_escorts_count - 1]->showname;
                    $this->view->back_id = $prev_page_escorts[$p_escorts_count - 1]->id;
                    //Adding previous page escorts to $ses_pages and set in session
                    $ids = array();
                    foreach($prev_page_escorts as $p_esc) {
                        $ids[] = $p_esc->id;
                    }

                    $criterias['first_page'] -= 1;
                    $ses->{$paging_key} = array_merge($ids, $ses_pages);
                    $ses->{$paging_key . '_criterias'} = $criterias;
                }
            }

            if ( isset($ses_pages[$ses_index + 1]) ) {
                $this->view->next_showname = $model->getShownameById($ses_pages[$ses_index + 1]);
                $this->view->next_id = $next_id = $ses_pages[$ses_index + 1];
            } else { //Loading next page escorts
                switch( $paging_key ) {
                    case 'regular_list' :
                        $next_page_escorts = Model_Escort_Mobile_List::getFiltered($criterias['filter'], $criterias['sort'], $criterias['last_page'] + 1);
                        break;
                    case 'main_premium_spot':
                        $next_page_escorts = Model_Escort_Mobile_List::getMainPremiumSpot($criterias['last_page'] + 1);
                        $next_page_escorts = array();
                        break;
                    case 'tours_list':
                        $next_page_escorts = Model_Escort_Mobile_List::getTours($criterias['is_upcoming'], $criterias['filter'], $criterias['sort'], $criterias['last_page'] + 1);
                        break;
                }

                if ( count($next_page_escorts) ) {
                    $this->view->next_showname = $next_page_escorts[0]->showname;
                    $this->view->next_id = $next_page_escorts[0]->id;
                    //Adding next page escorts to $ses_pages and set in session
                    foreach($next_page_escorts as $p_esc) {
                        $ses_pages[] = $p_esc->id;
                    }

                    $criterias['last_page'] += 1;
                    $ses->{$paging_key} = $ses_pages;
                    $ses->{$paging_key . '_criterias'} = $criterias;
                }
            }

            $this->view->paging_key = $paging_key;
        }

        $_paging_key = "prev_next";
        $sid = 'sedcard_paging_' . Cubix_Application::getId();
        $ses = new Zend_Session_Namespace($sid);

        $city = new Cubix_Geography_Cities();
        $city = $city->get($escort->base_city_id);


        $ses_pages = $ses->{$_paging_key};

        $ses_index = 0;
        if ( is_array($ses_pages) ) {
            $ses_index = array_search($escort_id, $ses_pages);
        }

        if ( isset($ses_pages[$ses_index - 1]) ) {
            $this->view->back_id  = $back_id = $ses_pages[$ses_index - 1];
            $this->view->back_showname = $model->getShownameById($back_id);
        }

        if ( isset($ses_pages[$ses_index + 1]) ) {
            $this->view->next_id = $next_id =  $ses_pages[$ses_index + 1];
            $this->view->next_showname = $model->getShownameById($next_id);
        }

//        list($prev, $next) = Model_Escort_Mobile_List::getPrevNext($escort->showname);
//
//        if ( ! is_null($prev) ) {
//            $this->view->back_showname = $prev;
//        }
//
//        if ( ! is_null($next) ) {
//            $this->view->next_showname = $next;
//        }

        //list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls();

        $this->view->escort_user_id = $model->getUserId($escort->id);
    }

    public function photosV2Action($escort = null)
    {
        $config = Zend_Registry::get('escorts_config');

        $page = $this->_getParam('photo_page');

        $escort_id = $this->_getParam('escort_id');
        $mode = $this->_getParam('mode');

        if( $mode == 'ajax' ) {
            $this->view->layout()->disableLayout();
            $model = new Model_EscortsV2();
            $escort = $model->get($escort_id);
            $this->view->escort_id = $escort_id;
        }

        if ( ! $page )
            $page = 1;

        $count = 0;
        if ( $this->view->is_preview || $this->_getParam('is_preview') ) {
            $photos = $escort->getPhotosApi($page, $count, true, true);
        } else {
            $count = 100;

            $client = new Cubix_Api_XmlRpc_Client();

            $cur_gallery_id = $this->_getParam('gallery_id');
            $cur_gallery_id = is_null($cur_gallery_id) ? 0 : intval($cur_gallery_id);
            $this->view->cur_gallery_id = $cur_gallery_id;
            $_galleries = $galleries = $client->call('getPhotoGalleriesED', array($escort_id));

            $galleries_arr = array();

            foreach( $galleries as $gallery ):
                $galleries_arr[] = $gallery['fake_id'];
            endforeach;
            //print_r($galleries_arr); die;

            $photos = $escort->getPhotos($page, $count, true, false, null, null, false, true, $galleries_arr[0]);
            $photos_1 = $escort->getPhotos($page, $count, true, false, null, null, false, true, $galleries_arr[1]);
            $photos_2 = $escort->getPhotos($page, $count, true, false, null, null, false, true, $galleries_arr[2]);


            if( !count( $photos_1 ) && !count( $photos_2 ) ){
                unset( $_galleries[2] ); unset( $_galleries[1] );
            } elseif( !count( $photos_1 ) ){
                unset( $_galleries[1] );
            } elseif( !count( $photos_2 ) ){
                unset( $_galleries[2] );
            }

            $this->view->galleries = $_galleries;
        }

        $this->view->photos = $photos;
        $this->view->photos_1 = $photos_1;
        $this->view->photos_2 = $photos_2;
        $this->view->photos_count = $count;
        $this->view->photos_page = $page;

        $this->view->photos_perPage = $config['photos']['perPage'];
    }

    public function agencyEscortsV2Action($agency_id = null, $escort_id = null)
    {
        $config = Zend_Registry::get('escorts_config');

        if ( ! $agency_id )
        {
            $this->view->layout()->disableLayout();
            $agency_id = $this->view->agency_id = $this->_getParam('agency_id');
        }
        else
            $this->view->agency_id = $agency_id;

        if( ! $escort_id )
            $escort_id = $this->view->escort_id = $this->_getParam('escort_id');

        $page = intval($this->_getParam('agency_page'));
        if ( $page < 1 )
            $page = 1;

        $params = array('e.agency_id = ?' => $agency_id,'e.id <> ?' => $escort_id);

        $escorts_count = 0;

        $escorts = Model_Escort_Mobile_List::getFiltered( $params, 'random', $page, $config['widgetPerPage'], $escorts_count );

        if (count($escorts) ) {
            $this->view->a_escorts = $escorts;
            $this->view->a_escorts_count = $escorts_count;

            $this->view->agencies_page = $page;
            $this->view->widgetPerPage = $config['widgetPerPageV2'];

            $m_esc = new Model_EscortsV2();
            $agency = $m_esc->getAgency($escort_id);

            $this->view->agency = new Model_AgencyItem($agency);
        }
    }

    private function _getLateNightGirls($page = 1)
    {
        return Model_EscortsV2::getLateNightGirls($page);
    }

	public function reviewsAction() {
		$lng = Cubix_I18n::getLang();

		$request = $this->_request;
		if ($request->ajax) {
			$this->view->layout()->disableLayout();
			$this->view->ajax = true;
		}
		$filter = array();

		if (isset($request->city_id) && $request->city_id) {
			$filter['city_id'] = intval($request->city_id);
			$this->view->city_id = intval($request->city_id);
		}

		if (isset($request->showname) && $request->showname) {
			$filter['showname'] = $request->showname;
			$this->view->showname = $request->showname;
		}

		if (isset($request->member_name) && $request->member_name) {
			$filter['member_name'] = $request->member_name;
			$this->view->member_name = $request->member_name;
		}

		$ord_field_v = 'creation_date';
		$ord_field = 'r.creation_date';
		$ord_dir_v = 'desc';
		$ord_dir = 'DESC';

		if (count($filter) > 0)
			$arg_filter = $filter;
		else
			$arg_filter = '-999';
		$config = Zend_Registry::get('reviews_config');
		if (isset($request->page) && intval($request->page) > 0) {
			$page = intval($request->page);
		}
		else
			$page = 1;

		is_array($arg_filter) ? $filter_str = implode('.', $arg_filter) : $filter_str = $arg_filter;

		$ret_revs = Model_Reviews::getReviews($page, /*$config['perPage']*/ 14, $arg_filter, $ord_field, $ord_dir);
		$cities = Model_Reviews::getReviewsCities();


		$ret = array($ret_revs, $cities);

		list($ret_revs, $cities) = $ret;
		list($items, $count) = $ret_revs;
		$this->view->items = $items;
		$this->view->count = $count;
		$this->view->cities = $cities;
		$this->view->page = $page;
		$this->view->filter = $filter;

		$this->view->menuReviews = 1;
	}

    public function ajaxGetTopSearchAction()
    {
        $count = 0;

        $filter_params['filter'] = array('e.showname LIKE ?' => $this->_request->getParam('top_search_name', null) . '%');

        $all_escorts = Model_Escort_Mobile_List::getFiltered($filter_params['filter'], 'alpha', 1, 5, $count, 'regular_list', false);

        $_all_escorts = array();
        foreach ( $all_escorts as $k => $escort ) {
            $_all_escorts[$k] = array(
                'type' => 'escort',
                'group' => $this->view->t('escorts'),
                'slug' => $escort->showname,
                'name' => $escort->showname,
                'city' => $escort->city,
                'info' => ( $escort->agency_id ) ? " ( " . $escort->city . " / Agency " . $escort->agency_name . " )" : "( " . $escort->city . " / Independent )"
            );
        }

        //print_r($all_escorts); die;

        $all_cities = Model_Statistics::getCities(null, null, null, null, null, null, null, $this->_request->getParam('top_search_name', null));

        $fn_order_title = create_function('$a, $b', '
			if ( is_object($a) && is_object($b) ) {
				return strnatcmp($a->city_title, $b->city_title);
			}
			elseif ( is_array($a) && is_array($b) ) {
				return strnatcmp($a["city_title"], $b["city_title"]);
			}
			else return 0;
		');

        usort($all_cities, $fn_order_title);
        foreach ( $all_cities as $k => $city ) {
            $all_cities[$k] = array(
                'type' => 'city',
                'group' => $this->view->t('cities'),
                'slug' => $city->city_slug,
                'id' => $city->city_id,
                'name' => $city->city_title . ' (' . $city->escort_count . ')' . ' (' . $city->country_title . ')'
            );
        }

        $merged = array_merge($_all_escorts, $all_cities);

        die(json_encode($merged));
    }

    public function getCaptchaStatusAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $request = $this->_request;
        $hash = $request->hash;

        $session = new Zend_Session_Namespace('captcha');
        $orig_captcha = $session->captcha;

        //die($hash);

        if ( strtolower( $hash ) != $orig_captcha ) {
            echo 'false';
        } else {
            echo 'true';
        }

    }
}
