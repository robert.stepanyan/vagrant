<?php

class Mobile_ClassifiedAdsController extends Zend_Controller_Action
{
	protected $_session;
	protected $_perPages = array(10, 25, 50, 100);

	public function init()
	{
		$this->_session = new Zend_Session_Namespace('classified-ads');
		$this->model = new Model_ClassifiedAds();
		
		$this->view->per_pages = $this->_perPages;
		$this->defines = $this->view->defines = Zend_Registry::get('defines');
	}
	
	public function indexAction()
	{
        if ( ! is_null($this->_getParam('ajax')) ) {
            //$this->_helper->viewRenderer->setScriptAction('popup');
            $this->view->layout()->disableLayout();
            $this->view->is_ajax = true;
        } else {
            $this->view->heading_arr = array( __('classified_ads'), 'ads' );
            $this->view->layout()->setLayout('mobile-st');
        }
        $req = $this->_request;

        $this->view->perPage = $perPage = $this->_getParam('pp', 25);

        if ( ! in_array($perPage, $this->_perPages) ) {
            $this->view->perPage = $perPage = 25;
        }
        //$this->view->perPage = $perPage = 1;

        $page = $req->page;
        if ( $page < 1 ) {
            $page = 1;
        }
        $this->view->page = $page;

        $ord_dir = $req->ord_dir;
        if ($ord_dir != 'asc') $ord_dir = 'desc';
        $this->view->ord_dir = $ord_dir;

        $filter = array();

        $this->view->category = $category = $req->category;
        $this->view->city_id = $city = (int)$req->city_id;
        $this->view->country_id = $country_id = (int)$req->country_id;
        $this->view->text = $text = $req->text;
//        $date_f = $req->date_f;
//        $date_t = $req->date_t;
        $with_photo = $req->with_photo;

        if ( $category ) {
            $filter['category'] = $category;
        }

        if ( $city ) {
            $filter['city'] = $city;
        }

        if ( $country_id ) {
            $filter['country_id'] = $country_id;
        }

        if ( strlen($text) ) {
            $filter['text'] = $text;
        }

//        if ( strlen($date_f) ) {
//            $filter['date_f'] = $date_f;
//        }
//
//        if ( strlen($date_t) ) {
//            $filter['date_t'] = $date_t;
//        }

        if ( $with_photo == 1 ) {
            $filter['with_photo'] = 1;
        }

        $cache_key = Cubix_Application::getId() . '_classified_ads_' . $page . '_' . $perPage . '_' . $ord_dir . '_' . implode('_', $filter);
        $cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
        //print_r(array($filter, $page, $perPage, $ord_dir, $count)); die;

        $geoData = null;
        if ( IS_MOBILE_DEVICE ) {
            $geoData = Cubix_Geoip::getClientLocation();
        }

        $ads = $this->model->getList($filter, $page, $perPage, $ord_dir, $count, null, true, $geoData);
        $ret = array('ads' => $ads, 'count' => $count);

        $this->view->ads = $ret['ads'];
        $this->view->count = $ret['count'];
	}

	public function ajaxFilterAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$this->view->category = (int)$req->category;
		$this->view->city = (int)$req->city;
		$this->view->country_id = (int)$req->country_id;
		$this->view->text = $req->text;
		$this->view->date_f = $req->date_f;
		$this->view->date_t = $req->date_t;
		$this->view->with_photo = $req->with_photo;
	}
	
	public function ajaxListAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$this->view->perPage = $perPage = $this->_getParam('pp', 25);
		
		if ( ! in_array($perPage, $this->_perPages) ) {
			$this->view->perPage = $perPage = 25;
		}
		//$this->view->perPage = $perPage = 1;
		
		$page = $req->page;
		if ( $page < 1 ) {
			$page = 1;
		}
		$this->view->page = $page;
		
		$filter = array();
		
		$category = $req->category;
		$city = (int)$req->city;
		$country_id = (int)$req->country_id;
		$text = $req->text;
		$date_f = $req->date_f;
		$date_t = $req->date_t;
		$with_photo = $req->with_photo;
		
		if ( $category ) {
			$filter['category'] = $category;
		}
		
		if ( $city ) {
			$filter['city'] = $city;
		}

		if ( $country_id ) {
			$filter['country_id'] = $country_id;
		}
		
		if ( strlen($text) ) {
			$filter['text'] = $text;
		}

		if ( strlen($date_f) ) {
			$filter['date_f'] = $date_f;
		}

		if ( strlen($date_t) ) {
			$filter['date_t'] = $date_t;
		}

		if ( $with_photo == 1 ) {
			$filter['with_photo'] = 1;
		}
		
		$this->view->ads = $this->model->getList($filter, $page, $perPage, $count);
		$this->view->count = $count;
	}

	public function adAction()
	{
        if ( ! is_null($this->_getParam('ajax')) ) {
            //$this->_helper->viewRenderer->setScriptAction('popup');
            $this->view->layout()->disableLayout();
            $this->view->is_ajax = true;
        } else {
            $this->view->heading_arr = array( __('classified_ads'), 'ad' );
            $this->view->layout()->setLayout('mobile-st');
        }

		$id = (int)$this->_request->id;

		$this->view->ad = $ad = $this->model->get($id);

		$this->model->updateViewCount($id);

		if ( ! isset($this->view->ad->id) ) {
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: ' . $this->view->getLink('classified-ads-index'));
			return;
		}

		/* add more ads */

		$this->view->perPage = $perPage = 100;

		$this->view->page = $page = 1;

		$filter = array('category' => $ad->category_id);

		$this->view->ads = $this->model->getList($filter, $page, $perPage, $count, $id);
		$this->view->count = $count;
	}
	
	public function placeAdAction()
	{
        if ( ! is_null($this->_getParam('ajax')) ) {
            //$this->_helper->viewRenderer->setScriptAction('popup');
            $this->view->layout()->disableLayout();
            $this->view->is_ajax = true;
        } else {
            $this->view->heading_arr = array( __('classified_ads'), 'ads' );
            $this->view->layout()->setLayout('mobile-st');
        }

		$defines = $this->defines;
		$blackListModel = new Model_BlacklistedWords();
		$modelCities = new Model_Cities();

		$this->view->cities = $modelCities->getAll();
		$steps = array(1, 2, 3, 'finish');
		$edit_mode = false;
		$req = $this->_request;
		$step = $req->getParam('step', 1);
		$edit = (int) $req->getParam('edit');
		$plan = $req->getParam('plan', false);
		
		$data = array();
		
		if ( !in_array($step, $steps) ) {
			$step = 1;
		}
		
		if ( isset($this->_session->data) && count($this->_session->data) && $edit ) {
			$edit_mode = true;
		}
		
		if ( ! $req->isPost() && ! $edit ) {
			$this->_session->unsetAll();
		}
		
		if ( ! isset($this->_session->data) && ! count($this->_session->data) && ($step == 2 || $step == 3 || $step == 'finish') ) {
			$step = 1;
		}

		if ( $edit_mode ) {
			$data = $this->_session->data;
		}
		
		$photos = array();
		if ( count($this->_session->photos) ) {
			$photos = $this->_session->photos;
		}
		
		$errors = array();
		if ( $req->isPost() ) 
		{
			if ( ! $plan ) {
				try {
					$images = new Cubix_ImagesCommon();
					foreach ( $_FILES as $i => $file ) {
						if ( strlen($file['tmp_name']) ) {
							$photo = $images->save($file);
							if ( $photo && count($photos) < 3  ) {
								$photos[$photo['image_id']] = $photo;
							}
						}
					}
				} catch ( Exception $e ) {
					$errors['photos'][] = $e->getMessage();
				}

				$category = (int) $req->category;
				$cities =  $req->cities;
				$phone = $req->phone;
				$email = $req->email;
				$duration = (int) $req->duration;
				$title = $req->title;
				$text = $req->text;

				if ( ! $category ) {
					$errors['category'] = Cubix_I18n::translate('sys_error_required');
				}

				if ( ! count($cities) ) {
					$errors['city'] = Cubix_I18n::translate('sys_error_required');
				}

				if ( ! strlen($title) ) {
					$errors['title'] = Cubix_I18n::translate('sys_error_required');
				}

				if ( ! strlen($text) ) {
					$errors['text'] = Cubix_I18n::translate('sys_error_required');
				}
				else if ($bl_words = $blackListModel->checkWords($text, Model_BlacklistedWords::BL_TYPE_CLASSIFIED_ADS)){
					foreach($bl_words as $bl_word){
						$pattern = '/' . preg_quote($bl_word, '/') . '/';
						$text = preg_replace($pattern, '<abbr title="Blacklisted word" class = "black-listed" >' . $bl_word . '</abbr>', $text);
					}
					$errors['text'] = 'You can`t use word "'.$blackListModel->getWords().'"';
					
				}
				if ( ! strlen($email) && ! strlen($phone) ) {
					$errors['phone_email'] = Cubix_I18n::translate('sys_error_phone_email_required');
				}

				if ( strlen($email) ) {
					$valid = new Cubix_Validator();
					
					if ( ! $valid->isValidEmail($email) ) {
						$errors['invalid_email'] = Cubix_I18n::translate('invalid_email');
					}				
				}

				if ( ! $duration ) {
					$errors['duration'] = Cubix_I18n::translate('sys_error_required');
				}

				$data = array(
					'category' => $category,
					'cities' => $cities,
					'phone' => $phone,
					'email' => $email,
					'duration' => $duration,
					'title' => $title,
					'text' => $text
				);
				
				$user = Model_Users::getCurrent();
				$data['user_id'] = null;
				if ( $user ) {
					$data['user_id'] = $user->id;
				}
				
				$this->_session->photos = $photos;

				if ( ! count($errors) ) {
					$this->_session->data = $data;
					$step = 2;
				}
			} else {
				//FINISH Stage
				$packages = $defines['classified_ad_packages_arr'];
				
				if ( !in_array($plan, $packages) ) {
					$this->_redirect($this->view->getLink('classified-ads-place-ad'));
				}
				
				/************************* IP ****************************/
				if ( isset($_SERVER['HTTP_CLIENT_IP']) )
					$ip = $_SERVER['HTTP_CLIENT_IP'];
				elseif ( isset($_SERVER['HTTP_X_FORWARDED_FOR']) )
					$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
				elseif ( isset($_SERVER['REMOTE_ADDR']) )
					$ip = $_SERVER['REMOTE_ADDR'];
				else
					$ip = '';
				
				$data['ip'] = $ip;
				/*********************************************************/
												
				$m_ads = new Model_ClassifiedAds();
				
				$save_data = array('data' => $data, 'photos' => $photos);
					
				$ad_id = $m_ads->save($save_data);
					
				if ( $ad_id ) {
					$this->_session->unsetAll();
				}
				
				if ( $plan == 'free' ) {
					if ( is_array($ad_id) ) {
						$this->_redirect($this->view->getLink('classified-ads-error'));
					} else {
						$this->_redirect($this->view->getLink('classified-ads-success') . '?id=' . $ad_id);
					}
				} else {
					
				}
			}
		}
		
		$this->view->errors = $errors;
		$this->view->step = $step;
		$this->view->data = $data;
		$this->view->photos = $photos;
		$this->view->photos_count = count($photos);
		//var_dump($data);
		//var_dump($photos);
	}
	
	public function ajaxRemovePhotoAction()
	{
		$image_id = (int) $this->_request->image_id;
		
		$status = array('status' => 'error');
		if ( isset($this->_session->photos[$image_id]) ) {
			unset($this->_session->photos[$image_id]);
			$status = array('status' => 'success');
		}
		
		die(json_encode($status));
	}
	
	public function successAction()
	{
		$this->view->id = (int) $this->_request->id;
	}
	
	public function errorAction()
	{
		
	}

	public function printAction()
	{
		$this->view->layout()->disableLayout();

		$ad_id = (int) $this->_request->id;

		$this->view->ad = $this->model->get($ad_id);
	}
}