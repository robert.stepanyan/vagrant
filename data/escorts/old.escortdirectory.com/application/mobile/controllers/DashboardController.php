<?php

class Mobile_DashboardController extends Zend_Controller_Action
{
	public static $linkHelper;
	
	public function init()
	{
		$this->view->layout()->setLayout('mobile-st');

		$cache = Zend_Registry::get('cache');
		self::$linkHelper = $this->view->getHelper('GetLink');

		$anonym = array();

		$this->user = Model_Users::getCurrent();		
		$this->defines = $this->view->defines = Zend_Registry::get('defines');
		$this->client = Cubix_Api::getInstance();

		if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}

		$cache_key =  'v2_user_pva_' . $this->user->id;

		if ( $this->user->isAgency() ) {

			if ( ! $agency = $cache->load($cache_key) ) {
				$agency = $this->user->getAgency();
				$cache->save($agency, $cache_key, array(), 300);
			}

			$this->agency = $this->view->agency = $agency;

			//$this->agencyDashboardAction();
			//$this->view->layout()->enableLayout();
		} else if ( $this->user->isEscort() ) {

			if ( ! $escort = $cache->load($cache_key) ) {
				$escort = $this->user->getEscort();
				$cache->save($escort, $cache_key, array(), 300);
			}

			$this->escort = $this->view->escort = $escort;

			$this->mode = $this->view->mode = (($this->user->hasProfile()) ? 'update' : 'create');

			/* Grigor Update */
			if ( $this->escort->status & Model_Escorts::ESCORT_STATUS_SUSPICIOUS_DELETED &&
				in_array($this->_request->getActionName(), array( 'tours', 'escort-reviews', 'settings', 'support', 'happy-hour', 'client-blacklist','plain-photos'  )) ) {//'plain-photos',
				$this->_redirect($this->view->getLink('private-v2'));
			}
			/* Grigor Update */
		}

		$this->view->user = $this->user;
		$this->view->show_online_status = $this->user->getShowOnlineStatus();

		$config = Zend_Registry::get('escorts_config');
		$steps = array('biography', 'about-me', 'languages', 'working-cities', 'services', 'working-times');

		if ($config['profile']['rates'])
			$steps[] = 'prices';

		$steps[] = 'contact-info';
		$steps[] = 'gallery';

		$this->view->steps = $steps;
	}

	public function indexAction()
	{
		$modelSupport = new Model_Support();
		$supportUnreadsCount = $this->view->supportUnreadsCount = $modelSupport->getUnreadsCountV2($this->user->id);

		if ( $this->user->isAgency() ) {

			$this->view->all_agency_escorts = $this->agency->getEscorts();

			$this->ajaxGetAgencyEscortsAction();
			$this->view->layout()->enableLayout();

			$this->_helper->viewRenderer->setScriptAction('dashboard-agency');
			
			$private_messaging = new Cubix_PrivateMessaging('agency', $this->agency->id);
			$unread_threads_count = $private_messaging->getUnreadThreadsCount();

			$pending = Cubix_Api::getInstance()->call('getPendingReviewsForAgencyEscorts', array($this->user->id));
			$this->view->pending_reviews_count = $pending['count'];
            $this->view->escort_id = $this->_getParam('escort_id');
		} else if ( $this->user->isEscort() ) {
			$this->mode = $this->view->mode = (($this->user->hasProfile()) ? 'update' : 'create');
			$this->_helper->viewRenderer->setScriptAction('dashboard-escort');
			$private_messaging = new Cubix_PrivateMessaging('escort', $this->escort->id);
			$unread_threads_count = $private_messaging->getUnreadThreadsCount();

			$pending = Cubix_Api::getInstance()->call('getPendingReviewsForEscort', array($this->user->id, $this->escort->id));
			$this->view->pending_reviews_count = $pending['count'];
		} else if ( $this->user->isMember() ) {

			$this->_helper->viewRenderer->setScriptAction('dashboard-member');
			
			$private_messaging = new Cubix_PrivateMessaging('member', $this->user->id);
			$unread_threads_count = $private_messaging->getUnreadThreadsCount();
		}
		$this->view->signup_type = $this->_getParam('success');
		$this->view->unread_threads_count = $unread_threads_count;
	}

	public function ajaxGetAgencyEscortsAction()
	{
		$this->view->layout()->disableLayout();

		if ( ! $this->user->isAgency() ) die;

		$this->view->s_showname = '';

		$client = new Cubix_Api_XmlRpc_Client();

		$active_tab = $this->view->active_tab = $this->_getParam('active_tab', 'Active-tab');

		if ( $this->_request->isPost() ) {
			$action = $this->_getParam('a');

			if ( $this->user->isEscort() ) {
				$escort = $this->user->getEscort();
				$escort_id = $escort->id;
			}
			else {
				$escort_id = intval($this->_getParam('escort_id'));

				if ( 0 == $escort_id ) $escort_id = null;

				if ( ! is_null($escort_id) && ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
					die;
				}

				$model = new Model_Escorts();
				$escort = $model->getById($escort_id);
			}

			switch ( $action ) {
				case 'activate':
					$this->view->actionError = '';
					if ( $escort->status & Model_Escorts::ESCORT_STATUS_ADMIN_DISABLED ) {
						$this->view->actionError .= 'The escort is deactivated by administration!<br/>';
					}

					if ( $escort->status & Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS ) {
						$this->view->actionError .= 'You cannot activate this escort, not enough photos!<br/>';
					}


					if ( $escort->status & Model_Escorts::ESCORT_STATUS_IS_NEW || $escort->status & Model_Escorts::ESCORT_STATUS_NO_PROFILE ) {
						$this->view->actionError .= 'The profile of this escort has not been approved yet!<br/>';
					}

					if ( ! strlen($this->view->actionError) ) {
						$client->call('Escorts.activate', array($escort_id));

						//$this->view->escorts = $escorts = $this->agency->getEscorts();
					}

					break;
				case 'deactivate':
					$client->call('Escorts.deactivate', array($escort_id));
					break;
				case 'delete':
					// TODO: escort products deletion. Premium package assigned escorts error.
					$client->call('Escorts.deleteTemporary', array($escort_id));
					break;
				default:
				case 'restore':
					// TODO: escort restore
					$client->call('Escorts.restore', array($escort_id));
					// $this->view->escorts = $escorts = $this->agency->getEscorts();
					break;
				default:

			}
		}

		$per_page = 16;

		$escorts_a_page = $this->view->escorts_a_page = $this->_getParam('escorts_a_page', 1);
		$escorts_i_page = $this->view->escorts_i_page = $this->_getParam('escorts_i_page', 1);
		$escorts_d_page = $this->view->escorts_d_page = $this->_getParam('escorts_d_page', 1);

		$sort = $this->view->sort = $this->_getParam('agency_sort', 'alpha');
		switch($sort) {
			case 'paid-package':
				$sort = 'p.name DESC';
				BREAK;
			case 'alpha':
				$sort = 'e.showname';
				BREAK;
			case 'escort-id':
				$sort = 'e.id';
				BREAK;
			case 'last-modified':
				$sort = 'e.date_last_modified';
				BREAK;
		}

		$escorts_a = $this->agency->getEscortsPerPage($escorts_a_page, $per_page, 1, false, $sort); //32 = Active
		$escorts_i = $this->agency->getEscortsPerPage($escorts_i_page,$per_page, null, false, $sort);//default inactive
		$escorts_d = $this->agency->getEscortsPerPage($escorts_d_page,$per_page,-1, false, $sort);//default inactive

		$this->view->escorts_a_count = $escorts_a['escorts_count'];
		$this->view->escorts_i_count = $escorts_i['escorts_count'];
		$this->view->escorts_d_count = $escorts_d['escorts_count'];

		$this->view->escorts_per_page = $per_page;

		$this->view->escorts_active = $escorts_a['escorts'];
		$this->view->escorts_inactive = $escorts_i['escorts'];
		$this->view->escorts_deleted = $escorts_d['escorts'];

		$this->view->current_page = $cur_page;

		$this->_helper->viewRenderer->setScriptAction('agency-escorts-block');
	}

	public function ajaxLoadProfileBlockAction()
	{
		$this->view->layout()->disableLayout();

		if ( $this->user->isAgency() ) {
			$escort_id = $this->view->escort_id = (int) $this->_request->escort_id;

			$this->view->all_agency_escorts = $this->agency->getEscorts();

			if ( $escort_id ) {
				$this->view->enable_profile = true;
			}
			$this->_helper->viewRenderer->setScriptAction('agency-profile-block');

		} else if ( $this->user->isEscort() ) {
			$this->mode = $this->view->mode = (($this->user->hasProfile()) ? 'update' : 'create');
			$this->_helper->viewRenderer->setScriptAction('escort-profile-block');
		}
	}
}
