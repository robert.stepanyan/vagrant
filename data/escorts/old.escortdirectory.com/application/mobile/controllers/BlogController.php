<?php

class Mobile_BlogController extends Zend_Controller_Action
{
	protected $model;

	public function init()
	{
		$this->model = new Model_Blog();
		$this->blog_config = Zend_Registry::get('blog_config');
	}

	public function indexAction()
	{

        if(! is_null($this->_getParam('ajax'))){
            $this->view->layout()->disableLayout();
        } else {
            $this->view->heading_arr = array( __('blog'), 'blog' );
            $this->view->layout()->setLayout('mobile-st');
        }

        $page = intval($this->_request->page);

        if ($page < 1)
            $page = 1;

		$this->view->page = $page;
		//$this->view->per_page = $per_page = $this->blog_config['posts']['perPage'];
		$this->view->per_page = $per_page = 10;

		$filter['month'] = date('m');
		$filter['year'] = date('Y');
		$direct = false;

        if (trim($this->_request->search_blog))
        {
            $this->view->search = $filter['search'] = trim($this->_request->search_blog);
            $this->view->is_search = true;
        }
		
		if (trim($this->_request->date))
		{
			$date = trim($this->_request->date);
			
			$parts = explode('.', $date);
			
			if (in_array($parts[0], array('01', '02', '03', '04', '05', '06', '07'. '08', '09', '10', '11', '12')) && 
					in_array($parts[1], array('2013', '2014', '2015', '2016', '2017', '2018', '2019', '2020', '2021', '2022', '2023', '2024', '2025', '2026', '2027', '2028', '2029', '2030')))
			{
				$filter['month'] = $parts[0];
				$filter['year'] = $parts[1];
			}
		}
		else
			$direct = true;

        $filter['month'] = $this->_request->month;
        $filter['year'] = $this->_request->year;
		
		$ret = $this->model->getPosts($filter, $page, $per_page);
				
		$this->view->data = $ret['data'];
		$this->view->count = $ret['count'];
		
		$this->view->archive = $this->model->getArchiveMonthList();
		
		$this->view->month = $filter['month'];
		$this->view->year = $filter['year'];

		if ($ret['data'] && $ret['count'] < 10) // 10 - per page
		{
			$ids = array();

			foreach ($ret['data'] as $d)
				$ids[] = $d->id;

			$this->view->others = $this->model->getRandomPosts(implode(',', $ids), (10 - $ret['count']));
		}

		if ($direct && !$ret['data'])
		{
			$c_m = intval($filter['month']);
			$c_y = intval($filter['year']);

			$m = $c_m - 1;
			$y = $c_y;

			if ($m < 1)
			{
				$m = 12;
				$y--;
			}

			if (strlen($m) == 1)
				$m = '0' . $m;

			$filter_old['month'] = $m;
			$filter_old['year'] = $y;

			$ret = $this->model->getPosts($filter_old, 1, 50);
			$this->view->data_old = $ret['data'];
		}
	}
	
	public function ajaxPostsAction()
	{
		$this->view->layout()->disableLayout();
		
		$page = intval($this->_request->page);
		
		if ($page < 1)
			$page = 1;
		
		$this->view->page = $page;
		
		//$this->view->per_page = $per_page = $this->blog_config['posts']['perPage'];
		$this->view->per_page = $per_page = 10;

		$filter = array();
		
		if (trim($this->_request->search))
		{
			$filter['search'] = trim($this->_request->search);
			$this->view->search = true;
		}
		
		$filter['month'] = date('m');
		$filter['year'] = date('Y');
		
		if (trim($this->_request->date))
		{
			$date = trim($this->_request->date);
			
			$parts = explode('.', $date);

			if (in_array($parts[0], array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12')) && 
					in_array($parts[1], array('2013', '2014', '2015', '2016', '2017', '2018', '2019', '2020', '2021', '2022', '2023', '2024', '2025', '2026', '2027', '2028', '2029', '2030')))
			{
				$filter['month'] = $parts[0];
				$filter['year'] = $parts[1];				
			}
		}
		
		$ret = $this->model->getPosts($filter, $page, $per_page);
				
		$this->view->data = $ret['data'];
		$this->view->count = $ret['count'];
		
		$this->view->month = $filter['month'];
		$this->view->year = $filter['year'];

		if ($ret['data'] && $ret['count'] < 10) // 10 - per page
		{
			$ids = array();

			foreach ($ret['data'] as $d)
				$ids[] = $d->id;

			$this->view->others = $this->model->getRandomPosts(implode(',', $ids), (10 - $ret['count']));
		}
	}
	
	public function detailsAction()
	{

        if(! is_null($this->_getParam('ajax'))){
            $this->view->layout()->disableLayout();
        } else {
            $this->view->heading_arr = array( __('blog'), 'blog' );
            $this->view->layout()->setLayout('mobile-st');
        }

        $id = intval($this->_request->id);
        $slug = $this->_request->slug;

        $ret = $this->model->getPost($id, $slug);

        $this->view->post = $ret['data'];
        $this->view->photos = $ret['photos'];

        $this->model->updateViewCount($id);

        /* more random posts */
//        $ids = array($id);
//        $this->view->count = $count = 4;
//        $this->view->data = $this->model->getRandomPosts(implode(',', $ids), $count);
//        $this->view->page = 1;
//        $this->view->per_page = $per_page = 10;
	}
	
	public function ajaxCommentsAction()
	{
		$this->view->layout()->disableLayout();
		
		$page = intval($this->_request->page);
		
		if ($page < 1)
			$page = 1;
		
		$this->view->page = $page;
		$this->view->page_repllies = 1;
		
		$this->view->per_page = $per_page = $this->blog_config['comments']['perPage'];
		$this->view->per_page_replies = $per_page_replies = $this->blog_config['replies']['perPage'];
		
		$post_id = intval($this->_request->post_id);
		
		$ret = $this->model->getComments($post_id, $page, $per_page, $per_page_replies);
		
		$this->view->comments = $ret['data'];
		$this->view->count = $ret['count'];
	}
	
	public function ajaxRepliesAction()
	{
		$this->view->layout()->disableLayout();
		
		$page = intval($this->_request->page);
		
		if ($page < 1)
			$page = 1;
		
		$this->view->page_repllies = $page;
		$this->view->per_page_replies = $per_page_replies = $this->blog_config['replies']['perPage'];
		
		$comment_id = intval($this->_request->comment_id);
		$post_id = $this->model->getPostIdByCommentId($comment_id);
		
		$this->view->replies = $this->model->getReplies($post_id, $comment_id, $page, $per_page_replies);
	}
	
	public function ajaxAddCommentAction()
	{
		$this->view->layout()->disableLayout();
		
		if ($this->_request->isPost())
		{
			$post_id = intval($this->_request->post_id);
			$name = trim($this->_request->name);
			$email = trim($this->_request->email);
			$comment = trim($this->_request->comment);

			$error = array();
			$valid = new Cubix_Validator();

			if (!$post_id)
				$error[] = 'post_id';

			if (!strlen($name))
				$error[] = 'name';

			if (!strlen($email) || !$valid->isValidEmail($email))
				$error[] = 'email';

			if (!strlen($comment))
				$error[] = 'comment';

			if (count($error))
				die(json_encode(array('error' => $error)));
			
			if (isset($_SERVER['HTTP_CLIENT_IP']))
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            else if (isset($_SERVER['REMOTE_ADDR']))
                $ip = $_SERVER['REMOTE_ADDR'];
            else
                $ip = 'UNKNOWN';
			
			$data = array(
				'post_id' => $post_id,
				'comment' => $comment,
				'name' => $name,
				'email' => $email,
				'is_reply' => 0,
				'ip' => $ip
			);
			
			$this->model->addComment($data);
			
			die(json_encode(array('success' => TRUE)));
		}
	}
	
	public function ajaxAddReplyAction()
	{
		$this->view->layout()->disableLayout();
		
		$this->view->comment_id = $comment_id = intval($this->_request->comment_id);
		
		if (!$comment_id)
			die;
		
		if ($this->_request->isPost())
		{
			$name = trim($this->_request->name);
			$email = trim($this->_request->email);
			$comment = trim($this->_request->comment);

			$error = array();
			$valid = new Cubix_Validator();

			if (!strlen($name))
				$error[] = 'name';

			if (!strlen($email) || !$valid->isValidEmail($email))
				$error[] = 'email';

			if (!strlen($comment))
				$error[] = 'comment';

			if (count($error))
				die(json_encode(array('error' => $error)));
			
			if (isset($_SERVER['HTTP_CLIENT_IP']))
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            else if (isset($_SERVER['REMOTE_ADDR']))
                $ip = $_SERVER['REMOTE_ADDR'];
            else
                $ip = 'UNKNOWN';
			
			$post_id = $this->model->getPostIdByCommentId($comment_id);
			
			$data = array(
				'post_id' => $post_id,
				'comment' => $comment,
				'name' => $name,
				'email' => $email,
				'is_reply' => $comment_id,
				'ip' => $ip
			);
			
			$this->model->addComment($data);
			
			die(json_encode(array('success' => TRUE)));
		}
	}
}
