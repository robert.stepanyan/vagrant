<?php

class Mobile_IndexController extends Zend_Controller_Action
{
	public function init()
	{
		$this->_helper->layout->setLayout('mobile');
        if(!$this->_getParam('escort_id')){
            $_SESSION['request_url'] = $_SERVER['REQUEST_URI'];
        }
        if($_GET['test'] == 1){ die("mobile");}
		$this->view->static_page_key = 'main';
        	//die('Mobile version is temporary unavailable');http://m.6annonce.com/remove-from-favorites?=11490
	}

    public function phoneImgAction()
    {
        $escort_id = (int) $this->_request->escort_id;
        $agency_id = (int) $this->_request->agency_id;
        $ad_id = (int) $this->_request->ad_id;
        $font_size = (int) $this->_request->fs;
        $width = (int) $this->_request->w;
        $height = (int) $this->_request->h;
        $v = (int) $this->_request->v;
        $color = $this->_request->c;
        $phone = $this->_request->p;
        $add = intval($this->_request->add);

        if (!in_array($add, array(1, 2)))
            $add = NULL;

        $colors = explode(',', $color);

        if ( ! strlen($phone) ) {
            if ( $escort_id ) {
                $model = new Model_EscortsV2();
                $cache_key = 'v2_' . $escort_id .  '_profile_' . Cubix_I18n::getLang();
                $cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
                $escort = $model->get($escort_id, $cache_key);
                $contacts = $escort->getContacts();

                if (!$add) {
                    $filtered_phone =  (isset($contacts->phone_country_id) && !$contacts->disable_phone_prefix) ? '+'.Model_Countries::getPhonePrefixById($contacts->phone_country_id) . $contacts->phone_number : $contacts->phone_number;
                }
                else {
                    $add_phone = $model->getAdditionalPhone($escort_id, $add);
                    $filtered_phone =  (isset($add_phone->phone_country_id) && !$add_phone->disable_phone_prefix) ? '+' . Model_Countries::getPhonePrefixById($add_phone->phone_country_id) . $add_phone->phone_additional : $add_phone->phone_additional;
                }
            } else if ( $agency_id ) {
                $m_agencies = new Model_Agencies();
                $agency = $m_agencies->getLocalById($agency_id);

                if (!$add) {
                    $filtered_phone =  (isset($agency->phone_country_id)) ? '+' . Model_Countries::getPhonePrefixById($agency->phone_country_id) . $agency->phone : $agency->phone;
                }
                else {
                    $filtered_phone =  (isset($agency->{'phone_country_id_' . $add})) ? '+' . Model_Countries::getPhonePrefixById($agency->{'phone_country_id_' . $add}) . $agency->{'phone_' . $add} : $agency->{'phone_' . $add};
                }
            } else if ($ad_id) {
                $m_class = new Model_ClassifiedAds();
                $filtered_phone = $m_class->get($ad_id)->phone;
            }
        } else {
            $filtered_phone = $phone;
        }

        // Set the content-type
        header('Content-Type: image/png');

        // Create the image
        $im = imagecreatetruecolor($width, $height);

        // Create some colors
        $white = imagecolorallocate($im, 255, 255, 255);

        array_unshift($colors, $im);

        $text_color = call_user_func_array('imagecolorallocate', $colors);//($im, 33, 115, 161);

        imagefilledrectangle($im, 0, 0, 399, 29, $white);
        imagecolortransparent($im, $white);

        // Replace path by your own font path
        $font = 'css/ArchivoNarrow.ttf';

        // Add the text
        imagettftext($im, $font_size, 0, 0, $v, $text_color, $font, $filtered_phone);

        // Using imagepng() results in clearer text compared with imagejpeg()
        imagepng($im);
        imagedestroy($im);
        die;
    }

    public function indexAction()
	{
        //$this->_forward('index', 'escorts'); return;

        if ( ! is_null($this->_getParam('ajax')) ) {
            // $this->_helper->viewRenderer->setScriptAction('popup');
            $this->view->layout()->disableLayout();
            $this->view->is_ajax = true;
        }
        // </editor-fold>

        // SEARCH PARAMS
        //$s_showname = $this->view->s_showname = $this->_request->showname;
        $top_search_input = $this->view->top_search_input = $this->_request->top_search_input;
        $ts_type = $this->view->ts_type = $this->_request->ts_type;
        $ts_slug = $this->view->ts_slug = $this->_request->ts_slug;

        $s_name = $this->view->s_name = $this->_request->name;
        $s_agency = $this->view->s_agency = $this->_request->agency;
        $s_agency_slug = $this->view->s_agency_slug = $this->_request->agency_slug;
        $s_gender = $this->view->s_gender = $this->_request->getParam('gender', array());
        $s_city = $this->view->s_city = $this->_request->city;
        $s_city_slug = $this->view->s_city_slug = $this->_request->city_slug;
        $s_phone = $this->view->s_phone = $this->_request->getParam('phone', null);
        $s_incall = $this->view->s_incall = (int)$this->_request->incall;
        $s_outcall = $this->view->s_outcall = (int)$this->_request->outcall;
        $s_ethnicity = $this->view->s_ethnicity = (int)$this->_request->ethnicity;
        $s_language = $this->view->s_language = $this->_request->language;
        $s_hair_color = $this->view->s_hair_color = (int)$this->_request->hair_color;
        $s_hair_length = $this->view->s_hair_length = (int)$this->_request->hair_length;
        $s_eye_color = $this->view->s_eye_color = (int)$this->_request->eye_color;
        $s_age_from = $this->view->s_age_from = (int)$this->_request->age_from;
        $s_age_to = $this->view->s_age_to = (int)$this->_request->age_to;
        $s_orientation = $this->view->s_orientation = (int)$this->_request->orientation;
        $s_service = $this->view->s_service = $this->_request->getParam('service', array());

        $s_filter_params = array(
            'name' => $s_name,
            'agency-name' => $s_agency_slug,
            'gender' => $s_gender,
            'city' => $s_city_slug,
            'phone' => ($s_phone) ? "%" . $s_phone . "%" : '',
            'incall' => $s_incall,
            'outcall' => $s_outcall,
            'ethnicity' => $s_ethnicity,
            'language' => $s_language,
            'hair-color' => $s_hair_color,
            'hair-length' => $s_hair_length,
            'eye-color' => $s_eye_color,
            'age-from' => $s_age_from,
            'age-to' => $s_age_to,
            'orientation' => $s_orientation,
            'service' => $s_service
        );

        if ( $ts_type == 'city' ) {
            $s_filter_params['city'] = $ts_slug;
        } else if ( $ts_type == 'escort' ) {
            $s_filter_params['name'] = $ts_slug;
        }

        $is_active_filter = false;
        foreach( $s_filter_params as $i => $par ) {
            if ( is_array($par) ) {
                if( count($par) ) {
                    $is_active_filter = true;
                    break;
                }
            } else {
                if( $par ) {
                    $is_active_filter = true;
                    break;
                }
            }
        }

        // SEARCH PARAMS

        $req = $this->_getParam('req');
        $_sort = $this->_getParam('sort');
        $req = explode('/', $req);

        $cache = Zend_Registry::get('cache');
        $defs = Zend_Registry::get('definitions');

        $params = array(
            'sort' => 'random',
            'filter' => array(array('field' => 'girls', 'value' => array())),
            'page' => 1
        );

        $static_page_key = 'main';
        $is_tour = false;

        $s_config = Zend_Registry::get('system_config');
        $this->view->showMainPremiumSpot = $s_config['showMainPremiumSpot'];

        if ( ! $s_config['showMainPremiumSpot'] ) {
            $static_page_key = 'regular';
        }

        if ( ! $is_active_filter ) {
            $p_top_category = $this->view->p_top_category = $this->_request->getParam('p_top_category', 'escorts');
            $p_city_slug = $this->view->p_city_slug = $this->_request->p_city_slug;
            $p_city_id = $this->view->p_city_id = (int) $this->_request->p_city_id;

            $p_country_slug = $this->view->p_country_slug = $this->_request->p_country_slug;
            $p_country_id = $this->view->p_country_id = (int) $this->_request->p_country_id;

            $f_top_category = $this->_request->getParam('f_top_category');

            if ( $f_top_category ) {
                $p_top_category = $this->view->f_top_category = $f_top_category;
            }
        }

        if ( $p_top_category ) {

            switch( $p_top_category )
            {
                case 'escorts':
                    $static_page_key = 'escorts';
                    $params['filter'][] = array('field' => 'escorts', 'value' => array());
                    continue;
                case 'escorts-only':
                    $static_page_key = 'escorts-only';
                    $params['filter'][] = array('field' => 'escorts-only', 'value' => array());
                    continue;

                case 'independent-escorts':
                    $static_page_key = 'escorts-only';
                    $params['filter'][] = array('field' => 'independent-escorts', 'value' => array());
                    continue;
                case 'agency-escorts':
                    $static_page_key = 'escorts-only';
                    $params['filter'][] = array('field' => 'agency', 'value' => array());
                    continue;
                case 'agencies':
                    $static_page_key = 'agencies';
                    $params['filter'][] = array('field' => 'agencies', 'value' => array());
                    continue;

                case 'bdsm':
                    $static_page_key = 'bdsm';
                    $params['filter'][0] = array('field' => 'bdsm', 'value' => array());
                    continue;
                case 'boys-trans':
                    $static_page_key = 'boys-trans';
                    $params['filter'][0] = array('field' => 'boys-trans', 'value' => array());
                    continue;
                case 'regular':
                    $static_page_key = 'regular';
                    $params['filter'][] = array('field' => 'regular', 'value' => array());
                    continue;
                default:
                    $this->_response->setRawHeader('HTTP/1.1 404 Not Found');
                    $this->_forward('error', 'error', null, array('error_msg' => '404 Page Not Found. Please try the following links instead.'));
                    return;
            }
        }

        $list_types = array('gallery', 'list', 'simple');

        if ( isset($_COOKIE['list_type']) && $_COOKIE['list_type'] ) {
            $list_type = $_COOKIE['list_type'];

            if ( ! in_array($list_type, $list_types) ) {
                $list_type = $list_types[0];
            }
        }

        $this->view->grouped_categories = $grouped_categories = array(
//            'escorts',
//            'agencies',
        );

        $show_grouped = false;
//        if ( $list_type == 'simple' || in_array($static_page_key, $grouped_categories) ) {
//            $show_grouped = true;
//        }


        $this->view->show_grouped = $show_grouped;

        /*if ( $static_page_key == 'new' ) {
            $this->_request->setParam('country_id', $n_country_id);
            $this->_forward('new-list');
        }*/

        if ( $p_city_slug ) {
            $params['city'] = $p_city_slug;
            $params['filter'][] = array('field' => 'city_id', 'value' => array($p_city_id));
        }

        if ( $p_country_slug ) {
            $params['country'] = $p_country_slug;
            $params['filter'][] = array('field' => 'country_id', 'value' => array($p_country_id));
        }

        $this->view->static_page_key = $static_page_key;

        $filter_params = array(
            'order' => 'e.date_registered DESC',
            'limit' => array('page' => 1),
            'filter' => array()
        );

        // <editor-fold defaultstate="collapsed" desc="Map parameters from url to SQL where clauses">
        $filter_map = array(
            //'verified' => 'e.verified_status = 2',
            //'french' => 'e.nationality_id = 15',

            'age-from' => 'e.age >= ?',
            'age-to' => 'e.age <= ?',
            'ethnicity' => 'e.ethnicity = ?',
            'phone' => 'e.contact_phone_parsed LIKE ?',
            'gender' => 'e.gender IN (?)',
            //'height' => 'e.height < ? AND e.height > ?',
            //'weight' => 'e.weight < ? AND e.weight > ?',
            //'cup-size' => 'e.cup_size = ?',
            'hair-color' => 'e.hair_color = ?',
            'hair-length' => 'e.hair_length = ?',
            'eye-color' => 'e.eye_color = ?',
            //'dress-size' => 'e.dress_size = ?',
            //'shoe-size' => 'e.shoe_size = ?',
            'incall' => 'e.incall_type IS NOT NULL',
            'outcall' => 'e.outcall_type IS NOT NULL',
            //'service-for' => 'FIND_IN_SET(?, e.sex_availability)',
            //'smoker' => 'e.is_smoking = ?',
            'language' => 'FIND_IN_SET(?, e.languages)',
            'orientation' => 'e.sex_orientation = ?',
            //'now-open' => 'e.is_now_open',

            'region' => 'r.slug = ?',
            'city' => 'ct.slug = ?',
            'city_id' => 'ct.id = ?',
            'country_id' => 'cr.id = ?',
            'cityzone' => 'c.id = ?',
            'zone' => 'cz.slug = ?',
            'name' => 'e.showname LIKE ?',
            'agency-name' => 'e.agency_slug = ?',




            'escorts' => 'eic.gender = ' . GENDER_FEMALE,
            'escorts-only' => 'eic.is_agency = 0 AND eic.gender = ' . GENDER_FEMALE,
            'agencies' => 'eic.is_agency = 1',
            'independent-escorts' => 'eic.is_agency = 0 AND eic.gender = ' . GENDER_FEMALE,
            'agency' => 'eic.is_agency = 1 AND eic.gender = ' . GENDER_FEMALE,
            'boys-trans' => '(eic.gender = ' . GENDER_MALE . ' OR eic.gender = ' . GENDER_TRANS . ')',
            'bdsm' => 'eic.gender = ' . GENDER_BDSM,


            'tours' => 'eic.is_tour = 1',
            'upcomingtours' => 'eic.is_upcoming = 1'
        );

        // <editor-fold defaultstate="collapsed" desc="Get page from request parameters">
        if ( $this->_request->page ) {
            $params['page'] = $this->_request->page;
        }

        if ( $this->_request->p_page && ! $this->_request->page ) {
            $params['page'] = $this->_request->p_page;
        }

        $page = intval($params['page']);
        if ( $page == 0 ) {
            $page = 1;
        }
        $filter_params['limit']['page'] = $this->view->page = $page;
        // </editor-fold>

        //$selected_filter = $menus['filter']->getSelected();

        foreach ( $params['filter'] as $i => $filter ) {

            if ( ! isset($filter_map[$filter['field']]) ) continue;

            $value = $filter['value'];

            if ( isset($filter['main']) ) {
                if ( isset($selected_filter->internal_value) ) {
                    $value = $selected_filter->internal_value;
                }
                elseif ( ! is_null($item = $menus['filter']->getByValue($filter['field'] . ( (isset($value[0]) && $value[0]) ? '_' . $value[0] : '')) ) ) {
                    $value = $item->internal_value;
                }

            }

            $filter_params['filter'][$filter_map[$filter['field']]] = $value;
        }
        // </editor-fold>

        foreach ( $s_filter_params as $i => $s_filter ) {

            if ( ! isset($filter_map[$i]) ) continue;

            if ( $s_filter ) {
                $filter_params['filter'][$filter_map[$i]] = $s_filter;
            }
        }

        // <editor-fold defaultstate="collapsed" desc="Sorting parameters SQL mappings">

        $default_sorting = "random";
        $current_sorting = $this->_getParam('sort', $default_sorting);


        $sort_map = array(
            'newest' => 'date_registered DESC',
            'last-contact-verification' => 'last_hand_verification_date DESC',
            'price-asc' => 'price-asc', // will be modified from escorts
            'price-desc' => 'price-desc', // will be modified from escorts
            'last_modified' => 'date_last_modified DESC',
            'last-connection' => 'refresh_date DESC',
            'close-to-me' => 'ordering DESC',
            'most-viewed' => 'hit_count DESC',
            'random' => 'ordering DESC',
            'by-country' => 'ordering DESC',
            'by-city' => 'ordering DESC',
            'alpha' => 'showname ASC',
            'age' => '-age DESC',
        );

        if ( !array_key_exists($current_sorting, $sort_map) ) {
            $current_sorting = $default_sorting;
            $this->_request->setParam('sort', $default_sorting);
        }

        if ( $current_sorting != $_COOKIE['sorting'] && $this->_request->s ) {
            setcookie("sorting", $current_sorting, strtotime('+1 year'), "/", "." . preg_replace('#www.|dev.#', '', $_SERVER['HTTP_HOST']));
            setcookie("sortingMap", $sort_map[$current_sorting]['map'], strtotime('+1 year'), "/", "." . preg_replace('#www.|dev.#', '', $_SERVER['HTTP_HOST']));
            $_COOKIE['sorting'] = $current_sorting;
            $_COOKIE['sortingMap'] = $sort_map[$current_sorting]['map'];
        }

        if ( isset($_COOKIE['sorting']) /*&& ! $this->_request->isPost()*/ ) {
            $current_sorting = $_COOKIE['sorting'];
            $this->_request->setParam('sort', $current_sorting);
        }

        if ( $current_sorting ) {
            $tmp = $sort_map[$current_sorting];
            unset($sort_map[$current_sorting]);
            $sort_map = array_merge(array( $current_sorting => $tmp), $sort_map);
        }

        if ( isset($sort_map[$params['sort']]) ) {
            $filter_params['order'] = $sort_map[$params['sort']];
            $this->view->sort_param = $params['sort'];
        }

        $this->view->sort_map = $sort_map;
        $this->view->current_sort = $current_sorting;
//        $params['sort'] = $current_sorting;

        /*if( $_sort != 'newest' ){
            $params['sort'] = 'close-to-me';
//            $params['sort'] = 'newest';
        } else {
            $params['sort'] = 'newest';
        }*/
        if( !$_sort ){
            $params['sort'] = 'close-to-me';
        } else {
            $params['sort'] = $_sort;
        }

        // </editor-fold>

        $model = new Model_EscortsV2();
        $count = 0;

        // <editor-fold defaultstate="collapsed" desc="Cache key generation">
        $filters_str = '';

        foreach ( $filter_params['filter'] as $k => $filter ) {
            if ( ! is_array($filter) ) {
                $filters_str .= $k . '_' . $filter;
            }
            else {
                $filters_str .= $k;
                foreach($filter as $f) {
                    $filters_str .= '_' . $f;
                }
            }
        }
        // </editor-fold>

        $cache_key = 'v2_' . Cubix_I18n::getLang() . '_' . $params['sort'] . '_' . $filter_params['limit']['page'] . '_' . $filters_str . (string) $has_filter;
        $cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
        $this->view->params = $params;

        $this->view->is_main_page = $is_main_page = ($static_page_key == 'main' && ( ! isset($params['city']) && ! isset($params['zone']) && ! isset($params['region']) ));

        $s_config = Zend_Registry::get('system_config');
        $e_config = Zend_Registry::get('escorts_config');

        $per_page = 14 /* For mobile version, default $e_config['perPage']*/;

        $segmented = $this->view->segmented = $this->_getParam('segmented', 0);

        if($this->getRequest()->getMethod() == 'GET'){
            if( isset($_COOKIE['page']) ){
                $per_page = $_COOKIE['page'] * $per_page;
                $filter_params['limit']['page'] = 1;
            }
        } else {
//            $per_page = $filter_params['limit']['page'] * $per_page;
//            $filter_params['limit']['page'] = 1;
        }

        //        if($this->getRequest()->getMethod() == 'GET'){
//            $url = $_SERVER["REQUEST_URI"];
//            $parsed = parse_url( $url );
//            print_r($parsed); die();
//        }


        // If we are on main premium spot forward to corresponding action
        if ( ! $s_config['showMainPremiumSpot'] ) {
            if ( $is_main_page ) {
                return $this->_forward('main-page');
            }
        }

        /* Ordering by country from GeoIp */
//        if ($params['sort'] == 'random')
//        {
//            $modelC = new Model_Countries();
//
//            $ret = $modelC->getByGeoIp();
//
//            //if ($_GET['test'] == 1) { var_dump($ret);	die;		}
//            if (is_array($ret))
//            {
//                $cache_key .= '_country_iso_' . $ret['country_iso'];
//                $params['sort'] = $ret['ordering'];
//            }
//        }

        $maping = array();
        $coord = Cubix_Geoip::getClientLocation();

        if( !empty( $coord['latitude'] ) && !empty( $coord['longitude'] ) ){
            $maping['latitude'] = $coord['latitude'];
            $maping['longitude'] = $coord['longitude'];
        }
        /**/

        foreach($s_filter_params as $k => $value ) {
            if ( is_array($value) ) {
                foreach( $value as $val ) {
                    $cache_key .= '_' . $val . '_';
                }
            } else {
                $cache_key .= '_' . $k . '_' . $value;
            }
        }

        if ( $show_grouped ) {
            $cache_key .= '_grouped';
        }

        $cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

        $count = $cache->load($cache_key . '_count');

        //print_r( $cache ); die();
        if ( ! $escorts = $cache->load($cache_key) ) {

            if ( isset($is_tour) && $is_tour ) {
                $escorts = Model_Escort_Mobile_List::getTours($upcoming_tours, $filter_params['filter'], $params['sort'], $filter_params['limit']['page'], $e_config['perPage'], $count);
            }
            else {
                if (!$has_filter) {
                    $filter_params['filter']['e.is_main_premium_spot = 0'] = array();
                }

                //print_r( $filter_params['limit']['page'] ); die();


                $escorts = Model_Escort_Mobile_List::getFiltered($filter_params['filter'], $params['sort'], $filter_params['limit']['page'], /*$e_config['perPage']*/ $per_page, $count, 'regular_list', false, $s_filter_params, $show_grouped, $maping);

                //echo $params['sort']; die;

//                $filter_params['filter']['e.is_vip = 1'] = array();
//                $escorts_vip = Model_Escort_Mobile_List::getFiltered($filter_params['filter'], $params['sort'], $filter_params['limit']['page'], /*$e_config['perPage']*/ $per_page, $count, 'regular_list', false, $s_filter_params, $show_grouped, $maping);

            }

            $cache->save($escorts, $cache_key, array());
            $cache->save($count, $cache_key . '_count', array());
        }

        $n_cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key) . '_newest';
        if ( ! $newest_escorts = $cache->load($n_cache_key) ) {
            if (!$has_filter) {
                $filter_params['filter']['e.is_main_premium_spot = 0'] = array();
            }
            $filter_params['filter']['e.is_new = 1'] = array();

            $newest_escorts = Model_Escort_Mobile_List::getFiltered($filter_params['filter'], 'newest', 1,/*$config['perPage']*/ $per_page, $ncount, 'regular_list', false, $s_filter_params);

            $cache->save($newest_escorts, $n_cache_key, array());
        }

        if ( $is_main_page && $filter_params['order'] == 'ordering DESC' ) {
            shuffle($escorts);
        }

        if ( count( $escorts ) > 0 ){
            // <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
            $sess_name = "prev_next";
            $sid = 'sedcard_paging_' . Cubix_Application::getId();
            $sess = new Zend_Session_Namespace($sid);

            $sess->{$sess_name} = array();
            foreach ( $escorts as $escort ) {
                //$sess->{$sess_name}[] = $escort->showname;
                $sess->{$sess_name}[] = $escort->id;
            }
            // </editor-fold>
        }

        $this->view->count = $count;
        $this->view->escorts = $escorts;
        $this->view->has_filter = $has_filter;
        $this->view->newest_escorts = $newest_escorts;
        $this->view->escorts_vip = $escorts_vip;

        if ( ! is_null($this->_getParam('ajax')) ) {
            $this->view->is_ajax = true;

            //if( !$this->_getParam('ajax') ){
            if ( $show_grouped ) {
                $this->view->template = 'list-group';
                $body = $this->view->render('escorts/list-grouped.phtml');
            } else {
                $this->view->template = 'list';
                $body = $this->view->render('escorts/list.phtml');

            }
            //}

            $entity = $static_page_key;

            if ( isset($params['city']) ) {
                $entity = 'city-' . $static_page_key;
            }

            if ( isset($entity) ) {
                $tpl_data = array();
                $primary_id = 0;

                switch ( $entity ) {
                    case 'city':
                    case 'city-escorts':
                    case 'city-independent-escorts':
                    case 'city-agency':
                    case 'city-bdsm':
                    case 'city-boys-trans':
                        $m = new Cubix_Geography_Cities();
                        $city = $m->getBySlug($params['city']);

                        if ( ! $city ) break;

                        $primary_id = $city->id;
                        $tpl_data['city'] = $city->title;
                        $tpl_data['region'] = $city->getRegionTitle();

                        break;
                }

                $seo = $this->view->seo($entity, $primary_id, $tpl_data);
            }

            //die(json_encode(array('head_title' => $seo->title, 'body' => $body)));
        }

	}


    private function _orderTitles($a,$b){
        if ( is_object($a) && is_object($b) ) {
			return strnatcmp($a->city_title, $b->city_title);
		}
		elseif ( is_array($a) && is_array($b) ) {
			return strnatcmp($a["city_title"], $b["city_title"]);
		}
		else return 0;
    }

    public function searchAction()
	{
        $this->view->defines = Zend_Registry::get('defines');


        $this->view->layout()->setLayout('mobile-st');
        $all_cities = Model_Statistics::getCities();
        $config = Zend_Registry::get('mobile_config');
        $all_zones = Model_Statistics::getZones();


        $s_keywords = $this->view->s_keywords = (array) $this->_request->keywords;
        $s_orientation = $this->view->s_orientation = (array) $this->_request->orientation;

        usort($all_cities, array(self, "_orderTitles"));

        $this->view->menuSearch = 1;
        $this->view->cities = $all_cities;
        $this->view->zones = $all_zones;
        

        if( $this->_getParam('search') )
		{
			$params['filter'] = array();
            $params['order'] = array();

			$this->view->params = $this->_request->getParams();
            
            if( $this->_getParam('order') ) {

                $order_direction = '';
                $order_field = 'date_registered';

                switch($this->_getParam('order')){
                    case '1':
                       $order_field = "date_last_modified";
                       break;
                    case '2':
                       $order_field = "e.showname ASC";
                       break;
                   case '3':
                       $order_field = "e.hit_count DESC";
                       break;
                   case '4':
                       $order_field = "e.is_new DESC";
                       break;
                   case '5':
                       $order_field = "e.date_last_modified DESC";
                       break;
                   case '6':
                       $order_field = "e.outcall_price ASC";
                       break;
                   case '7':
                       $order_field = "e.outcall_price DESC";
                       break;
                }

                if($this->_getParam('order_direction')){
                   $order_direction = $this->_getParam('order_direction');
                }
                $params['order'] = $order_field." ".$order_direction;

			}


            if($this->_getParam('page')){
                $params['limit']['page'] = $this->_getParam('page');
            }
            if($this->_getParam('verified')){
                $params['filter'] = array_merge($params['filter'], array(
					'e.verified_status = ?' => Model_VerifyRequests::STATUS_VERIFIED
				));
            }


			if( $this->_getParam('showname') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.showname LIKE ?' => "%" . trim($this->_getParam('showname')) . "%"
				));
			}


			if( $this->_getParam('gender') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.gender = ?' => $this->_getParam('gender')
				));
			}


            if( $this->_getParam('agency_slug') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.agency_slug = ?' => $this->_getParam('agency_slug')
                ));
            }


            if( $this->_getParam('phone') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.phone_number LIKE ?' => "%" . trim($this->_getParam('phone')) . "%"
                ));
            }

			if( $this->_getParam('city') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'ct.slug = ?' => $this->_getParam('city')
				));
			}

            if( $this->_getParam('age_from') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.age >= ?' => $this->_getParam('age_from')
                ));
            }
            if( $this->_getParam('age_to') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.age <= ?' => $this->_getParam('age_to')
                ));
            }

            //print_r('( ' . implode(',', $this->_getParam('keywords')) . ' )'); die;
            if ( $this->_getParam('keywords') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'ek.keyword_id IN (?)' => $this->_getParam('keywords')
                ));
            }

			if( $this->_getParam('available_for_incall') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.incall_type IS NOT NULL' => true
				));
			}

			if( $this->_getParam('available_for_outcall') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.outcall_type IS NOT NULL' => true
				));
			}

            if( $this->_getParam('french') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.nationality_id = 15' => true
				));
			}


			/* ------------ >>>>>>>>>>>>>>>>>>>>>>>>>>>>>> <<<<<<<<<<<<<<<<<<<<<< --------------- */


            if( $this->_getParam('ethnicity') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.ethnicity = ?' => $this->_getParam('ethnicity')
				));
			}

            if( $this->_getParam('hair_color') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.hair_color = ?' => $this->_getParam('hair_color')
				));
			}

			if( $this->_getParam('hair_length') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.hair_length = ?' => $this->_getParam('hair_length')
				));
			}

			if( $this->_getParam('eye_color') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.eye_color = ?' => $this->_getParam('eye_color')
				));
			}

            

            if( $this->_getParam('pubic_hair') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.pubic_hair = ?' => $this->_getParam('pubic_hair')
				));
			}



			if( $this->_getParam('drinking') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.drinking = ?' => $this->_getParam('drinking')
				));
			}
        

            $page = 1;
            $page_size = $config['escorts']['perPage'];
            if($this->_getParam('page')){
                $page = $this->_getParam('page');
            }

			$model = new Model_EscortsV2();
			$count = 0;
			$escorts = $model->getSearchAll($params, $count,$page,$page_size);
            $this->view->page = $page;
			$this->view->count = $escorts['count'];
			$this->view->escorts = $escorts['data'];
			$this->view->search_list = true;

            $this->_helper->viewRenderer->setScriptAction("escort-list");

		}
	}

    public function favoritesAction()
	{
        $cook_name = "favorites";
        $data = unserialize($_COOKIE[$cook_name]);
        
        $page = 1;
        $page_size = 9;
        if($this->_getParam('page')){
            $page = $this->_getParam('page');
        }
        
        if($data && count($data) > 0){
            foreach ( $data as $e ) {
                $ids[] = $e;
            }

            $filter = array('e.id IN (' . implode(', ', $ids) . ')' => array());

            $count = 0;
            $escorts = Model_Escort_Mobile_List::getFiltered($filter, 'random', $page, $page_size, $count);


            $this->view->escorts = $escorts;
        }

        $this->view->page = $page;
		$this->view->count = $count;

        $this->view->menuFavorites = 1;

        $this->_helper->viewRenderer->setScriptAction("favorites-list");

	}

    

    public function escortsAction()
	{


	}


    public function addToFavoritesAction(){
		$req = $this->_request;
        $escort_id = intval($req->escort_id);
        $cook_name = "favorites";
        $cook_value = array($escort_id);
        if ( ! isset( $_COOKIE[$cook_name]) ) {
            setcookie($cook_name, serialize($cook_value), strtotime('+1 year'), "/", '.' . Cubix_Application::getById(Cubix_Application::getId())->host);
        }
        else {
            $cook_data = unserialize($_COOKIE[$cook_name]);
            if ( is_array($cook_data) ) {
                if ( in_array($cook_value[0], $cook_data) ) {
                    $this->view->alredy_in_favorites = true;
                }
                else {
                    $new_data = array_merge($cook_data, $cook_value);
                    setcookie($cook_name, serialize($new_data), strtotime('+1 year'), "/", '.' . Cubix_Application::getById(Cubix_Application::getId())->host);
                }
            }
        }

        $this->_redirect($_SERVER['HTTP_REFERER']);
    }
    
    
    public function removeFromFavoritesAction(){
		$req = $this->_request;
        $escort_id = intval($req->escort_id);
        $cook_name = "favorites";
        $cook_value = array($escort_id);
        if ( isset( $_COOKIE[$cook_name]) ) {
            $cook_data = unserialize($_COOKIE[$cook_name]);
            if ( is_array($cook_data) ) {
                if ( in_array($cook_value[0], $cook_data) ) {
                    $new_data = array_diff($cook_data, $cook_value);
                    setcookie($cook_name, serialize($new_data), strtotime('+1 year'), "/", '.' . Cubix_Application::getById(Cubix_Application::getId())->host);
                }
            }
        }

        $this->_redirect($_SERVER['HTTP_REFERER']);
    }


    public function feedbackAction()
	{
		$this->_request->setParam('no_tidy', true);

		$menu = array(
            'bio' => array(
                'name' => 'bio',
                'class' => 'w15',
            ),
            'services' => array(
                'name' => 'services',
                'class' => 'w23',
            ),
            'rates' => array(
                'name' => 'rates',
                'class' => 'w18',
            ),
            'contacts' => array(
                'name' => 'contact',
                'class' => 'w24',
            ),
            'photos' => array(
                'name' => 'photos',
                'class' => 'w20',
            ));

		$this->view->menu = $menu;


		$model = new Model_EscortsV2();

        $this->_helper->layout->setLayout('mobile');



		// Fetch administrative emails from config
		$config = Zend_Registry::get('feedback_config');
		$site_emails = $config['emails'];


		// Get data from request
		$data = new Cubix_Form_Data($this->getRequest());
		$fields = array(
			'to' => '',
			'name' => 'notags|special',
			'email' => '',
			'message' => 'notags|special'
		);
		$data->setFields($fields);
		$data = $data->getData();

		$this->view->data = $data;

		// If the to field is invalid, that means that user has typed by
		// hand, so it's like a hacking attempt, simple die, without explanation
      

		if ( ! is_numeric($data['to']) && ! in_array($data['to'], array_keys($site_emails)) ) {
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			die;
			return;
		}

		// If escort id was specified, fetch it's email and showname,
		// construct the result email template and send it to escort
		if ( is_numeric($data['to']) ) {
			$escort = $model->get($data['to']);
			$this->view->escort = $escort;

            
            
			// If the escort id is invalid that means that it is hacking attempt
			if ( ! $escort ) {
				header('HTTP/1.1 301 Moved Permanently');
                header('Location: /');
                die;
                return;
			}
            
			$data['to_addr'] = $escort->email;
			$data['to_name'] = $escort->showname;
			$data['username'] = $escort->username;
			$email_tpl = 'escort_feedback';

            $this->view->showname = $escort->showname;
		}

		// In order when the form was posted validate fields
		if ( $this->_request->isPost() ) {
			$validator = new Cubix_Validator();

			if ( ! strlen($data['email']) ) {
				$validator->setError('email', '' /*'Email is required'*/);
			}
			elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
				$validator->setError('email', '' /*'Wrong email format'*/);
			}

			if ( ! strlen($data['message']) ) {
				$validator->setError('message','' /*'Please type the message you want to send'*/);
			}

			
			$result = $validator->getStatus();
            
            if ( ! $validator->isValid() ) {
                $this->view->errors = $result['msgs'];
			}else{
               

                // Set the template parameters and send it to support or to an escort
                $tpl_data = array(
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'to_addr' => $data['to_addr'],
                    'message' => $data['message'],
                    'escort_showname' => $escort->showname,
                    'escort_id' => $escort->id
                );

                if ( isset($data['username']) ) {
                    $tpl_data['username'] = $data['username'];
                }
                
                Cubix_Email::sendTemplate($email_tpl, $data['to_addr'], $tpl_data, isset($escort) ? $data['email'] : null);

                $this->_redirect('escort/'.$escort->showname.'?success=true#contacts');

            }
		}
	}

	public function robotsAction()
	{
		$this->_request->setParam('no_tidy', true);

		$this->view->layout()->disableLayout();
		$model = new Model_Robots();

		$robots = $model->getMobile();

		header('Content-Type: text/plain; charset=UTF-8', true);
		echo $robots;
		/*echo trim("User-agent: *
Dissallow: /page/terms-and-conditions
Disallow: /favorites
Disallow: /en
Disallow: /search");*/
		die;
	}

    public function captchaAction()
    {
        $this->_request->setParam('no_tidy', true);

        $font = 'css/trebuc.ttf';

        $charset = '123456789ABCDEFGHIJKLMNPQRSTUVWXYZ';

        $code_length = 5;

        $height = 30;
        $width = 90;
        $code = '';

        for ( $i = 0; $i < $code_length; $i++ )
        {
            $code = $code . substr($charset, mt_rand(0, strlen($charset) - 1), 1);
        }

        $rgb[0] = array(204,0,0);
        $rgb[1] = array(34,136,0);
        $rgb[2] = array(51,102,204);
        $rgb[3] = array(141,214,210);
        $rgb[4] = array(214,141,205);
        $rgb[5] = array(100,138,204);

        $font_size = $height * 0.4;

        $bg = imagecreatefrompng('img/bg_captcha.png');
        $image = imagecreatetruecolor($width, $height);
        imagecopy($image, $bg, 0, 0, 0, 0, imagesx($bg), imagesy($bg));
        $background_color = imagecolorallocate($image, 255, 255, 255);
        $noise_color = imagecolorallocate($image, 20, 40, 100);

        for($i = 0; $i<$code_length; $i++)
        {
            $A[] = rand(-20, 20);
            $C[] = rand(0, 5);
            $text_color = imagecolorallocate($image, $rgb[$C[$i]][0], $rgb[$C[$i]][1], $rgb[$C[$i]][2]);
            imagettftext($image, $font_size, $A[$i], 7 + $i * 15, 20 + rand(-3, 3), $text_color, $font , $code[$i]);
        }

        $session = new Zend_Session_Namespace('captcha');
        $session->captcha = strtolower($code);

        header('Content-Type: image/png');
        imagepng($image);
        imagedestroy($image);

        die;
    }

    public function gpsLocationAction()
    {
        $req = $this->_request;

        if (isset($req->lat) && isset($req->long))
        {
            $config = Zend_Registry::get('mobile_config');
            $limit = $config['escorts']['perPage'];
            $page = $req->page;
            $page = $this->view->page = isset($page) && is_numeric($page) ? intval($page) : 1;
            $page = ($page - 1) * $limit;

            $model = new Model_EscortsV2();

            $this->view->lat = floatval($req->lat);
            $this->view->long = floatval($req->long);
            $escorts = $model->getEscortsByLocation($req->lat, $req->long, $limit, $page);
            $this->view->count = $escorts['count'];
            $this->view->escorts = $escorts['data'];
        }
        else
        {
            $this->view->error = 1;
        }
    }
}
