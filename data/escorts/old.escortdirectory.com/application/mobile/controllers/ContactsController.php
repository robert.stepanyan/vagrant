<?php

class Mobile_ContactsController extends Zend_Controller_Action
{
	public function indexAction()
	{

	}

	public function contactUsAction()
	{
//		$this->view->layout()->disableLayout();

        $this->view->heading_arr = array( __('contact'), 'cont' );
        $this->view->layout()->setLayout('mobile-st');

		if ( $this->_request->isPost() ) {

			$validator = new Cubix_Validator();

			$data = new Cubix_Form_Data($this->_request);
			$fields = array(
				'name' => '',
				'email' => '',
				'message' => ''
			);
			$data->setFields($fields);
			$data = $data->getData();

			if ( ! strlen($data['email']) ) {
				$validator->setError('email', 'Email is required');
			} elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
				$validator->setError('email', 'Wrong email format');
			}

			if ( ! strlen($data['message']) ) {
				$validator->setError('message', 'Message is required');
			}

			$result = $validator->getStatus();

			if ( ! $validator->isValid() ) {

				if ( ! is_null($this->_getParam('ajax')) ) {

					//$result['status'] = 'success';

					echo(json_encode($result));
					ob_flush();
					die;
				}
			}
			else {

				// Fetch administrative emails from config
				$config = Zend_Registry::get('feedback_config');
				$site_emails = $config['emails'];

				$email_tpl = 'feedback_template';
				$data['to_addr'] = $site_emails['support'];

				$tpl_data = array(
					'name' => $data['name'],
					'email' => $data['email'],
					'to_addr' => $data['to_addr'],
					'message' => $data['message'],
					//'escort_showname' => $about_escort->showname,
					//'escort_id' => $about_escort->id,
					//'sent_by' => $sent_by
				);

				Cubix_Email::sendTemplate($email_tpl, $data['to_addr'], $tpl_data, isset($escort) ? $data['email'] : null);

				echo json_encode($result);
				die;
			}
		}
	}

	public function contactUsSuccessAction()
	{
		$this->view->layout()->disableLayout();
	}

	public function contactToAction()
	{

        $this->view->layout()->disableLayout();
        //$this->_helper->viewRenderer->setNoRender(true);

		$req = $this->_request;

		$id = $this->view->id = $req->id;
		$type = $this->view->type = $req->type;

		$is_ajax = ! is_null($this->_getParam('ajax'));

		if ( ! $id || ! $type ) die;

		if ( $type == 'e' ) {
			$model = new Model_EscortsV2(); $id = intval( $id );
			$to_user = $this->view->to_user = $model->get($id, 'contact_to_cache_key' . $id . Cubix_Application::getId());

			$this->view->to_user_title = $this->view->t('escort');
		} elseif ( $type == 'a' ) {
			$model = new Model_Agencies();
			$to_user = $this->view->to_user = $model->get($id);
			$this->view->to_user_title = $this->view->t('agency');

		} elseif ( $type == 'm' ) {

			$this->view->to_user_title = $this->view->t('member');
		} elseif ( $type == 'c' ) {
            $model = new Model_ClassifiedAds();
            $to_user = $this->view->to_user_title = $model->get($id);
        }

		//$feedback = new Model_Feedback();
		//$blackListModel = new Model_BlacklistedWords();
		$config = Zend_Registry::get('feedback_config');

        if ( $req->isPost() ) {
            $data = new Cubix_Form_Data($this->getRequest());

            $fields = array(
                'name' => 'notags|special',
                'email' => '',
                'message' => 'notags|special',
                'captcha' => ''
            );

            $data->setFields($fields);
            $data = $data->getData();

            $data['to_addr'] = $to_user->email;
            if ( $type == 'e' ) {
                $data['to'] = $id;
                $data['to_name'] = $to_user->showname;
            }
            elseif ($type == 'c')
            {
                $current_user = Model_Users::getCurrent();
                $user_id = null;

                if ($current_user) $user_id = $current_user->id;

                $data['user_id'] = $user_id;
                $data['ca_id'] = $id;
            }


            $blackListModel = new Model_BlacklistedWords();
            $feedback = new Model_Feedback();
            $cl_ads = new Model_ClassifiedAds();

            $validator = new Cubix_Validator();

            if ( ! $blackListModel->checkFeedback($data) ){
                $blackListModel->mergeFeedbackData($data);
            }

            if ( ! strlen($data['email']) ) {
                $validator->setError('email', '' /*'Email is required'*/);
            } elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
                $validator->setError('email', '' /*'Wrong email format'*/);
            } elseif( $blackListModel->checkEmail($data['email']) ){
                $data['flag'] = FEEDBACK_FLAG_BLOCKED_BY_EMAIL;
                $data['status'] = FEEDBACK_STATUS_DISABLED;
                //$validator->setError('email', 'This email is blocked');
            }

            if ( ! strlen($data['message']) ) {
                $validator->setError('message','' /*'Please type the message you want to send'*/);
            } elseif($blackListModel->checkWords($data['message'])) {

                $data['flag'] = FEEDBACK_FLAG_BLOCKED_BY_WORD;
                $data['status'] = FEEDBACK_STATUS_DISABLED;
                $validator->setError('message', 'You can`t use words "' . $blackListModel->getWords() . '"');
            }

            if( isset($data['flag']) && ($data['flag'] == FEEDBACK_FLAG_BLOCKED_BY_EMAIL || $data['flag'] == FEEDBACK_FLAG_BLOCKED_BY_WORD) ){
                $data['application_id'] = Cubix_Application::getId();

                if ( $type == 'c' )
                    $cl_ads->addFeedback($data);
                else
                    $feedback->addFeedback($data);
            }

            if ( ! strlen($data['captcha']) ) {
                $validator->setError('captcha', '');//Captcha is required
            }
            else {
                $session = new Zend_Session_Namespace('captcha');
                $orig_captcha = $session->captcha;

                if ( strtolower($data['captcha']) != $orig_captcha ) {
                    $validator->setError('captcha', '');//Captcha is invalid
                }
            }

            $result = $validator->getStatus();

            if ( ! $validator->isValid() ) {

                if ( ! is_null($this->_getParam('ajax')) ) {
                    echo(json_encode($result));
                    ob_flush();
                    die;
                }
            }
            else {
                if ($data['status'] != FEEDBACK_STATUS_DISABLED)
                {
                    $email_tpl = "escort_feedback";

                    if ( $type == 'c' )
                    {
                        $data['application_id'] = Cubix_Application::getId();

                        if ( $config['classified_ads_approvation'] == 1 ) {
                            $data['flag'] = FEEDBACK_FLAG_NOT_SEND;
                            $data['status'] = FEEDBACK_STATUS_NOT_APPROVED;
                        } else {
                            $data['flag'] = FEEDBACK_FLAG_SENT;
                            $data['status'] = FEEDBACK_STATUS_APPROVED;

                            $tpl_data = array(
                                'name' => $data['name'],
                                'email' => $data['email'],
                                'to_addr' => $to_user->email,
                                'message' => $data['message'],

                                'escort_id' => $id,
                                'showname' => $to_user->showname,
                            );

                            Cubix_Email::sendTemplate($email_tpl, $to_user->email, $tpl_data, $data['email']);
                        }

                        $cl_ads->addFeedback($data);
                    }
                    else
                    {
                        $data['application_id'] = Cubix_Application::getId();

                        if ( $config['approvation'] == 1 ) {
                            $data['flag'] = FEEDBACK_FLAG_NOT_SEND;
                            $data['status'] = FEEDBACK_STATUS_NOT_APPROVED;
                        } else {
                            $data['flag'] = FEEDBACK_FLAG_SENT;
                            $data['status'] = FEEDBACK_STATUS_APPROVED;

                            $tpl_data = array(
                                'name' => $data['name'],
                                'email' => $data['email'],
                                'to_addr' => $to_user->email,
                                'message' => $data['message'],
                                'escort_id' => $id,
                                'showname' => $to_user->showname,
                            );

                            //print_r($tpl_data); die;

                            Cubix_Email::sendTemplate($email_tpl, $to_user->email, $tpl_data, $data['email']);
                        }

                        $feedback->addFeedback($data);
                    }
                }

                $session->unsetAll();

                echo json_encode($result);
                die;
            }
        }
	}

	public function contactToSuccessAction()
	{
		$this->view->layout()->disableLayout();

		$req = $this->_request;

		$id = $this->view->id = (int) $req->id;
		$type = $this->view->type = $req->type;
		$is_ajax = ! is_null($this->_getParam('ajax'));

		if ( ! $id || ! $type ) die;

		if ( $type == 'e' ) {
			$model = new Model_EscortsV2();
			$to_user = $this->view->to_user = $model->get($id);
			$this->view->to_user_title = $this->view->t('escort');
		} elseif ( $type == 'a' ) {
			$model = new Model_Agencies();
			$to_user = $this->view->to_user = $model->get($id);
			$this->view->to_user_title = $this->view->t('agency');

		} elseif ( $type == 'm' ) {

			$this->view->to_user_title = $this->view->t('member');
		}
	}
}
