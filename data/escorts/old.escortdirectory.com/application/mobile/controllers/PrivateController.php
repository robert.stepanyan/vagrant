<?php

class Mobile_PrivateController extends Zend_Controller_Action
{
	public static $linkHelper;

	/**
	 * @var Zend_Session_Namespace
	 */
	protected $_session;

	public function init()
	{
		$this->_request->setParam('no_tidy', true);
		$this->view->layout()->setLayout('private');
		
		$anonym = array('signup', 'signin', 'forgot', 'forgot-success', 'activate', 'check', 'sign-in-up', 'change-pass', 'signup-success');
		$this->blacklisted_usernames = array( 'admin' , 'moderator', 'webmaster' ,'manager', 'sales' );
		$this->user = Model_Users::getCurrent();

		if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {
			$this->_response->setRedirect($this->view->getLink('signin'));
			return;
		}
		
		$this->view->headTitle('Private Area', 'PREPEND');

		$this->_session = new Zend_Session_Namespace('private');
	}
	
	public function indexAction()
	{

		if ( $this->user->isAgency() || $this->user->isEscort() || $this->user->isMember() ) {
			$this->_redirect($this->view->getLink('dashboard'));
		}
	}

	public function signupAction()
	{
        $request = $this->_request;

        if( $request->mode == 'ajax' ){
            $this->view->layout()->disableLayout();
        } else {
            $this->view->heading_arr = array( __('register'), 'register' );
            $this->view->layout()->setLayout('mobile-st');
        }

		$req = $this->_request;
		$step = $req->step;

		//$types = array('type', 'form', 'success');
		$user_types = array('member', 'escort', 'agency');

		//if ( ! in_array($step, $types) ) die('Wrong step');

		//$this->_helper->viewRenderer->setScriptAction('signup-' . $step);
		$this->_helper->viewRenderer->setScriptAction('signup');



		if ( $this->_getParam('ajax') ) {
			$this->view->layout()->disableLayout();
		}

		$user_type = $this->view->user_type = $this->_getParam('user_type');

		if ( $step == 'form' && ! in_array($user_type, $user_types) ) die('User type is wrong or not specified');

		$signup_i18n = $this->view->signup_i18n = (object) array(
			'username_invalid' => __('su_username_at_least_6'),//'Username must be at least 6 characters long',
			'username_invalid_characters' => __('su_only_az'), //'Only "a-z", "0-9", "-" and "_" are allowed',
			'username_exists' => __('su_username_exist'), //'Username already exists, please choose another',
			'password_invalid' => __('su_password_at_least_6'), //'Password must contain at least 6 characters',
			'password2_invalid' => __('su_password_must_equal'), //'Passwords must be equal',
			'email_invalid' => __('su_email_is_invalid'), //'Email is invalid, please provide valid email address',
			'email_exists' => __('su_email_exist'), //'Email already exists, please enter another',
			'confirm_email' => __('su_email_must_equal'), //'Emails must be equal',
			'terms_required' => __('su_you_must_agree_terms'), //'You must agree with terms and conditions',
			'form_errors' => __('su_check_errors_in_form'), //'Please check errors in form',
			'terms_required' => __('su_have_to_accept_terms'), //'You have to accept terms and conditions before continue',
			'domain_blacklisted' => __('su_domain_blacklisted'), //'Domain is blacklisted',
			'user_type' => __('su_choose_your_business'), //'Choose your business'
			'username_equal_password' => __('username_equal_password'), //'Choose your business'
		);

		if ( $this->_request->isPost() ) {

			if ($this->_getParam('phone')){
				die;
			}
				
			$validator = new Cubix_Validator();

			$data = new Cubix_Form_Data($this->_request);
			$fields = array(
				'username' => '',
				'password' => '',
				'password2' => '',
				'email' => '',
				'confirm_email' => '',
//				'sales_person' => '',
				'terms' => 'int',
				// 'captcha' => ''
			);

			$data->setFields($fields);
			$data = $data->getData();

			$data['username'] = substr($data['username'], 0, 24);
			$data['user_type'] = $user_type;

			$this->view->data = $data;

			$has_bl_username = false;
			foreach($this->blacklisted_usernames as $bl_username){
				if( strpos( $data['username'], $bl_username) !== false){
					$has_bl_username = true;
					BREAK;
				}
			}

			$client = new Cubix_Api_XmlRpc_Client();
			$model = new Model_Users();

            if ( !($data['user_type']) ) {
                $validator->setError('user_type', $signup_i18n->user_type);
            }

			if ( strlen($data['username']) < 6 ) {
				$validator->setError('username', $signup_i18n->username_invalid);
			}
			elseif ( ! preg_match('/^[-_a-z0-9]+$/i', $data['username']) ) {
				$validator->setError('username', $signup_i18n->username_invalid_characters);
			}
			elseif ($has_bl_username) {
				$validator->setError('username', $signup_i18n->username_exists);
			}
			elseif ( $client->call('Users.getByUsername', array($data['username'])) ) {
				$validator->setError('username', $signup_i18n->username_exists);
			}

			if ( $data['password'] == $data['username']) {
				$validator->setError('password', $signup_i18n->username_equal_password);	
			}

			if ( strlen($data['password']) < 6 ) {
				$validator->setError('password', $signup_i18n->password_invalid);
				$validator->setError('password2', '');
			}
			elseif ( $data['password'] != $data['password2'] ) {
				$validator->setError('password2', $signup_i18n->password2_invalid);
			}

			if ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
				$validator->setError('email', $signup_i18n->email_invalid);
//				$validator->setError('confirm_email', '');
			}
			elseif( $client->call('Application.isDomainBlacklisted', array($data['email'])) )
			{
				$validator->setError('email', $signup_i18n->domain_blacklisted);
//				$validator->setError('confirm_email', '');
			}
			elseif ( $client->call('Users.getByEmail', array($data['email'])) ) {
				$validator->setError('email', $signup_i18n->email_exists);
//				$validator->setError('confirm_email', '');
			} elseif ( $data['email'] != $data['confirm_email'] ) {
				$validator->setError('confirm_email', $signup_i18n->confirm_email);
			}


			if ( ! $data['terms'] ) {
				$validator->setError('terms', $signup_i18n->terms_required);
			}

			// if ( ! strlen($data['captcha']) ) {
			// 	$validator->setError('captcha', 'Captcha is required');
			// }
			// else {
			// 	$session = new Zend_Session_Namespace('captcha');
			// 	$orig_captcha = $session->captcha;

			// 	if ( strtolower($data['captcha']) != $orig_captcha ) {
			// 		$validator->setError('captcha', 'Captcha is invalid');
			// 	}
			// }

			$result = $validator->getStatus();

			if ( ! $validator->isValid() ) {

				if ( ! is_null($this->_getParam('ajax')) ) {

					//$result['status'] = 'success';

					echo(json_encode($result));
					ob_flush();
					die;
				}
			}
			else {
				// will set the most free sales user id

				// Users model
				$user = new Model_UserItem(array(
					'username' => $data['username'],
					'email' => $data['email'],
					'user_type' => $user_type
				));

				Model_Hooks::preUserSignUp($user);

				$salt_hash = Cubix_Salt::generateSalt($data['email']);
				$user->salt_hash = $salt_hash;
				$user->password = Cubix_Salt::hashPassword($data['password'], $salt_hash);

				$new_user = $model->save($user);


				if ( 'escort' == $user_type ) {
					$m_escorts = new Model_Escorts();

					$escort = new Model_EscortItem(array(
						'user_id' => $user->new_user_id
					));

					// will set current app country id
					Model_Hooks::preEscortSignUp($escort);

					$escort = $m_escorts->save($escort);

					// will update escort status bits using api
					Model_Hooks::postEscortSignUp($escort);
					$result['user_type'] = 'escort';
				}
				elseif ( 'agency' == $user_type ) {
					$m_agencies = new Model_Agencies();

					$agency = new Model_AgencyItem(array(
						'user_id' => $user->new_user_id,
						'name' => ucfirst($user->username)
					));

					// will set current app country id
					Model_Hooks::preAgencySignUp($agency);

					$agency = $m_agencies->save($agency);

					//
					Model_Hooks::postAgencySignUp($agency);
					$result['user_type'] = 'agency';
				}
				elseif ( 'member' == $user_type || 'vip-member' == $user_type ) {
					unset($this->_session->want_premium);
					$m_members = new Model_Members;

					$member = new Model_MemberItem(array(
						'user_id' => $user->new_user_id,
						'email'   => $user->email
					));

					$this->view->n_member = $m_members->save($member);
					$result['user_type'] = 'member';
				}

				//newsletter email log
				$emails = array(
					'old' => null,
					'new' => $user->email
				);
				Cubix_Api::getInstance()->call('addNewlstterEmailLog', array($user->new_user_id, $user_type, 'add', $emails));
				//

				// will send activation email
				$user->reg_type = $user_type;
				Model_Hooks::postUserSignUp($user);

				if ( ! is_null($this->_getParam('ajax')) ) {

					echo json_encode($result);
					die;

				} else {
					/*if ( $user_type == 'vip-member' ) {
						$this->_session->want_premium = (object) array('member_data' => array('id' => $this->view->n_member->getId()), 'email' => $user->email);
						header('Location: /' . Cubix_I18n::getLang() . '/private-v2/upgrade');
						exit;
						//$this->_helper->viewRenderer->setScriptAction('signup-success-cc');
					}
					else {
						$this->_helper->viewRenderer->setScriptAction('signup-success');
					}*/
				}
			}
		}
	}

	public function signinAction()
	{
        $request = $this->_request;

        if( $request->mode == 'ajax' ){
            $this->view->layout()->disableLayout();
        } else {
            $this->view->heading_arr = array( __('login'), 'login' );
            $this->view->layout()->setLayout('mobile-st');
        }

		$this->view->data = array('username' => '');
		$this->view->errors = array();

		/*if (isset($_GET['test']))
		{
			if(isset($_SERVER['HTTP_CLIENT_IP']))
				$ip = $_SERVER['HTTP_CLIENT_IP'];
			else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
				$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			else if(isset($_SERVER['REMOTE_ADDR']))
				$ip = $_SERVER['REMOTE_ADDR'];
			else
				$ip = 'UNKNOWN';

			$client = new Cubix_Api_XmlRpc_Client();
			try {
				$user = $client->call('Users.getClientLocation', array($ip));
				print_r($user);die;
			}
			catch ( Exception $e ) {
				var_dump($client->getHttpClient()->getLastResponse()->getBody());
				die;
			}
		}*/
		
		if ( $this->_request->isPost() ) {

			$username = trim($this->_getParam('username'));
			$password = trim($this->_getParam('password'));
			$remember_me = intval($this->_getParam('remember_me'));
			
			
			$this->view->data['username'] = $username;

			$validator = new Cubix_Validator();

			if ( ! strlen($username) ) {
				$validator->setError('username', 'Username is required');
			}
			

			if ( ! strlen($password) ) {
				$validator->setError('password', 'Password is required');
			}

			/*if ( ! strlen($username) ) {
				$this->view->errors['username'] = 'Username is required';
			}
			
			if ( ! strlen($password) ) {
				$this->view->errors['password'] = 'Password is required';
			}*/

			if ( strlen($username) && strlen($password) ) {
				$model = new Model_Users();

				if ( ! $user = $model->getByUsernamePassword($username, $password) ) {
					$validator->setError('username', 'Wrong username/password combination');
				}

				if ( $user ) {
					if ( STATUS_ACTIVE != $user->status ) {
						$validator->setError('
						username', 'Your account is not active yet');
					}
				}
			}

/********** Disable Agencies **********/
//            if ( $user->user_type == 'agency' ) {
//                $validator->setError('password', 'No support agencies');
//            }
/********** End **********/

			$result = $validator->getStatus();

			if ( ! $validator->isValid() ) {
				
				$this->view->errors = $result['msgs'];
				
				if ( ! is_null($this->_getParam('ajax')) ) {
					echo(json_encode($result));
					ob_flush();
					die;
				}
			}
			else {
				/*$model = new Model_Users();
				
				if ( ! $user = $model->getByUsernamePassword($username, $password) ) {
					$this->view->errors['username'] = 'Wrong username/password combination';
					return;
				}
				
				if ( STATUS_ACTIVE != $user->status ) {
					$this->view->errors['username'] = 'Your account is not active yet';
					return;
				}*/
				
				$has_active_package = false;
				$is_susp = false;
				
				if ( $user->user_type == 'agency' ) {
					$_SESSION['fc_gender'] = 2;
				}
				else if ( $user->user_type == 'member' ) {
					$_SESSION['fc_gender'] = 1;
				}
				else if ( $user->user_type == 'escort' ) {
					$_SESSION['fc_gender'] = 2;
					
					/***********************************/
					$client = new Cubix_Api_XmlRpc_Client();
					$has_active_package = $client->call('Escorts.hasPaidActivePackageForEscort', array($user->id));
					$m_e = new Model_EscortsV2();
					$is_susp = $m_e->isSuspicious($user->id);
				}
				
				$_SESSION['fc_username'] = $username;
				$_SESSION['fc_password'] = $password;

				// UNCOMMENT WHEN DEPLOYED
				//Zend_Session::regenerateId();

				$user->sign_hash = md5(rand(100000, 999999) * microtime(true));
				$user->has_active_package = $has_active_package;
				$user->is_susp = $is_susp;
				
				/*-->Setting chat info*/
				$chat_info = array(
					'nickName'	=> $user->username,
					'userId'	=> $user->id,
					'userType'	=> $user->user_type,
					'imIsBlocked' => $user->im_is_blocked
				);
				if ( $user->user_type == 'escort' ) {
					$m = new Model_EscortsV2();
					$e = $m->getByUserId($user->id);
					$chat_info['nickName'] = $e->showname . ' (Escort)';
					$chat_info['isSusp'] = $is_susp;
					$chat_info['hasActivePackage'] = $has_active_package;
					$chat_info['link'] = '/escort/' . $e->showname . '-' . $e->id;
					
					//Getting main image url
					$parts = array();
					if ( strlen($e->id) > 2 ) {
						$parts[] = substr($e->id, 0, 2);
						$parts[] = substr($e->id, 2);
					}
					else {
						$parts[] = '_';
						$parts[] = $e->id;
					}
					$catalog = implode('/', $parts);
					
					$app = Cubix_Application::getById();
					$chat_info['avatar'] = 'http://pic.' . $app->host . '/' . $app->host . '/' . $catalog . '/' . $e->photo_hash . '_lvthumb.' . $e->photo_ext;
				} elseif ( $user->user_type == 'agency' ) {
					$m = new Model_Agencies();
					$a = $m->getByUserId($user->id);
					$chat_info['nickName'] = $a->name;
					$chat_info['hasActivePackage'] = $has_active_package;
				} elseif ( $user->user_type == 'member' ) {
					$chat_info['link'] = '/member/' . $user->username;
				}
				$user->chat_info = $chat_info;

				Model_Users::setCurrent($user);


				/*set client ID*/
				Model_Reviews::createCookieClientID($user->id);
				/**/

				if ( $remember_me ) {
					$this->addRememberMeCook($username, $password);
				}

				Model_Hooks::preUserSignIn($user->id);

				if ( ! is_null($this->_getParam('ajax')) ) {
					/*$result['msg'] = "
						<h1><img src='/img/" . Cubix_I18n::getLang() . "_h1_login.gif' alt='' title='' /></h1>
						<p style='padding:0 10px; width: 520px;'><span class='strong' style='color: #3F3F3F'>" . Cubix_I18n::translate('signin_success') . "</span></p>
					";
					$result['signin'] = true;*/
					echo json_encode($result);
					die;
				}
				else {
					$this->_response->setRedirect($this->view->getLink('dashboard')."?success=" . ucfirst($user->user_type));
				}
			}
		}
	}

	public function signInUpAction()
	{
		$this->view->layout()->disableLayout();
	}

	private function addRememberMeCook($username, $password)
	{
		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		$key = '_-_SignInRememberMe_-_' . Cubix_Application::getId();

		$login_data = serialize(array('username' => $username, 'password' => $password));

		$crypt_login_data = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $login_data, MCRYPT_MODE_ECB, $iv);
		$crypt_login_data = base64_encode($crypt_login_data);

		$cookie_name = 'signin_remember_' . Cubix_Application::getId();
		$cookie_lifetime = strtotime('+21 days');

		if ( ! isset($_COOKIE[$cookie_name]) ) {
			setcookie($cookie_name, $crypt_login_data, $cookie_lifetime, "/");
			$_COOKIE[$cookie_name] = $crypt_login_data;
		}
	}
	
	public function signoutAction()
	{
		Model_Users::setCurrent(NULL);
		Zend_Session::regenerateId();

		$cookie_name = 'signin_remember_' . Cubix_Application::getId();
		if ( isset($_COOKIE[$cookie_name]) && $_COOKIE[$cookie_name] ) {
			setcookie($cookie_name, null, strtotime('- 1 year'), "/");
		}

		//$this->_response->setRedirect($this->view->getLink());
		$this->_response->setRedirect('/');
	}
	
	public function activateAction()
	{
		$this->view->layout()->setLayout('mobile-st');
		$hash = $this->_getParam('hash');
		$email = $this->_getParam('email');
		
		$model = new Model_Users();
		

		if ( ! $user = $model->activate($email, $hash) ) {
			$this->_response->setRedirect($this->view->getLink('signin'));
			return;
		}
		
		Model_Hooks::postUserActivate($user);
		$this->view->user_info = $user;
	}
	
	public function forgotAction()
	{
        $request = $this->_request;

        if( $request->mode == 'ajax' ){
            $this->view->layout()->disableLayout();
        } else {
            $this->view->heading_arr = array( __('forgot'), 'forgot' );
            $this->view->layout()->setLayout('mobile-st');
        }

		if ( $this->_request->isPost() ) {
			$email = trim($this->_getParam('email'));

			$validator = new Cubix_Validator();

			if ( ! strlen($email) ) {
				$validator->setError('email', 'Email is required');
			}

			$model = new Model_Users();
			if ( ! ($user = $model->forgotPassword($email)) ) {
				$validator->setError('email', 'This email is not registered on our site');
			}

			$result = $validator->getStatus();

			if ( ! $validator->isValid() ) {

				if ( ! is_null($this->_getParam('ajax')) ) {
					echo(json_encode($result));
					die;
				}
			}
			else {

				Cubix_Email::sendTemplate('forgot_verification', $email, array(
					'username' => $user['username'],
					'hash' => $user['email_hash']
				));

				echo(json_encode($result));
				die;
			}
		}
	}

    public function forgotSuccessAction()
    {
        $request = $this->_request;

        if( $request->mode == 'ajax' ){
            $this->view->layout()->disableLayout();
        } else {
            $this->view->heading_arr = array( __('forgot'), 'forgot' );
            $this->view->layout()->setLayout('mobile-st');
        }
        //$this->view->layout()->disableLayout();
    }

    public function signupSuccessAction()
    {
        $request = $this->_request;

        if( $request->mode == 'ajax' ){
            $this->view->layout()->disableLayout();
        } else {
            $this->view->heading_arr = array( __('forgot'), 'forgot' );
            $this->view->layout()->setLayout('mobile-st');
        }
        //$this->view->layout()->disableLayout();
    }
	
	public function ajaxCheckEmailOrUsernameAction()
	{
		$username = $this->_getParam('username');
		$email = $this->_getParam('email');
		$client = new Cubix_Api_XmlRpc_Client();
		$model = new Model_Users();
		
		$result = array('status' => '');
		
		if ( ! is_null($username) ) {
			
			$has_bl_username = false;
			foreach($this->blacklisted_usernames as $bl_username){
				if( strpos( $username, $bl_username) !== false){
					$has_bl_username = true;
					BREAK;
				}
			}
			
			//if (stripos($username, 'admin') === FALSE)
			if (!$has_bl_username)
			{
				if ( ! $client->call('Users.getByUsername', array($username)) ) {
					$result['status'] = 'not found';
				}
				else {
					$result['status'] = 'found';
				}
			}
			else
				$result['status'] = 'found';
		}
		elseif ( ! is_null($email) ) {
						
			if ( $client->call('Application.isDomainBlacklisted', array($email)) )
			{
				$result['status'] = 'domain blacklisted';
			}
			else 
			{	
				if ( ! $client->call('Users.getByEmail', array($email)) ) {
					$result['status'] = 'not found';
				}
				else {
					$result['status'] = 'found';
				}
			}
		}
		
		die(json_encode($result));
	}

	public function changePasswordAction()
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$user_id = $this->user->id;				
		
		if ( $this->_request->isPost() ) {
			$data = array();
			
			$validator = new Cubix_Validator();
			
			$data = $this->_validatePassword($validator);
						
			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();
				
				$this->view->errors = $status['msgs'];
				
				return;
			}
			
			try {
				$result = $client->call('Users.updatePassword', array($user_id, $data['new_pass']));
			}
			catch (Exception $e) {
				var_dump($client->getLastResponse()); die;
			}
			
			if ( isset($result['error']) ) {
				print_r($result['error']); die;
			}
			else {
				if ( $result !== false ) {
					$this->view->saved = true;
					echo nl2br(print_r($result, true));
				}
			}
		}
	}
	
	protected function _validatePassword(Cubix_Validator $validator)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$req = $this->_request;
						
		$curr_pass = $req->curr_pass;
		if ( ! strlen($curr_pass) ) {
			$validator->setError('curr_pass', 'Current Password is required !');
		}
		else if ( strlen($curr_pass) && ! $client->call('Users.isPasswordTrue', array($this->user->id, $curr_pass)) ) {
			$validator->setError('curr_pass', 'Current Password is wrong !');
		}
		$data['curr_pass'] = $curr_pass;
		
		
		$new_pass = $req->new_pass;
		if ( ! strlen($new_pass) ) {
			$validator->setError('new_pass', 'New Password is required !');
		}
		else if ( strlen($new_pass) && (strlen($new_pass) < 6 || strlen($new_pass) > 16)  ) {
			$validator->setError('new_pass', 'Password length must be 6 - 16 !');
		}
		$data['new_pass'] = $new_pass;
		
		
		$confirm_pass = $req->confirm_pass;
		if ( ! strlen($confirm_pass) ) {
			$validator->setError('confirm_pass', 'Confirm Password is required !');
		}
		else if ( strlen($confirm_pass) && (strlen($confirm_pass) < 6 || strlen($confirm_pass) > 16)  ) {
			$validator->setError('confirm_pass', 'Password length must be 6 - 16 symbols!');
		}
		else if ( strlen($confirm_pass) && strlen($new_pass) ) {
			if ( $new_pass != $confirm_pass )
				$validator->setError('confirm_pass', 'New Password and Confirm Password must be an equal!');
		}
		$data['confirm_pass'] = $confirm_pass;
		
		return $data;
	}

	public function ajaxReturnFromVacationAction()
	{
		$this->view->layout()->disableLayout();

		$client = new Cubix_Api_XmlRpc_Client();

		if ( $this->user )
		{
			if ($this->_request->escort_id)
				$escort_id = $this->_request->escort_id;
			else
				$escort_id = $this->user->getEscort()->getId();
		}

		try {
			$result = $client->call('Escorts.removeVacation', array($escort_id));
			$m_escorts = new Model_Escorts();
			$m_escorts->removeVacation($escort_id);

		}
		catch (Exception $e) {
			echo json_encode($client->getLastResponse()); die;
		}

		die;
	}

	public function changePassAction()
	{
		$client = new Cubix_Api_XmlRpc_Client();
		if ( $this->_request->isPost() ) {
			$pass = $this->_getParam('password');
			$conf_pass = $this->_getParam('password2');
			$hash = $this->_getParam('hash');
			$id = intval($this->_getParam('id'));
			$validator = new Cubix_Validator();
			if ( strlen($pass) < 6 ) {
				$validator->setError('password', __('password_invalid'));
			}
			elseif ( $pass != $conf_pass ) {
				$validator->setError('password2',  __('password_missmatch'));
			}
			elseif ( ! preg_match('/[a-f0-9]{32}/', $hash) ) {
				$validator->setError('hash',  'Invalid hash !');
			}
			$this->view->errors = array();
			$result = $validator->getStatus();

			if ( ! $validator->isValid() ) {
				$this->view->errors = $result['msgs'];
			}
			else{
				$client->call('Users.checkUpdatePassword', array($id, $hash, $pass));
				$this->_helper->viewRenderer->setScriptAction('change-pass-success');
			}
		}
		else
		{
			$hash = $this->_getParam('hash');
			$username = $this->_getParam('username');
			$error = false;
			if(!isset($username) || !isset($hash)){
				$error = true;
			}
			$username = substr($username, 0, 24);
			if ( strlen($username) < 6 ) {
				$error = true;
			}
			elseif ( ! preg_match('/^[-_a-z0-9]+$/i', $username) ) {
				$error = true;
			}
			elseif ( ! preg_match('/[a-f0-9]{32}/', $hash) ) {
				$error = true;
			}

			if(!$error)
			{
				$id = $client->call('Users.getByUsernameMailHash', array($username, $hash));
				if($id){
					$this->view->id = $id;
					$this->view->hash = $hash;

				}
				else{
					$error = true;
				}

			}
			if ($error){
				$this->_response->setRedirect($this->view->getLink('signin'));
				/*$this->_response->setRawHeader('HTTP/1.1 404 Not Found');
				$this->_forward('error', 'error', null, array('error_msg' => '404 Page Not Found. Please try the following links instead.'));*/
			}
		}
	}

	public function checkAction()
	{
		$username = $this->_getParam('username');
		$email = $this->_getParam('email');
		$client = new Cubix_Api_XmlRpc_Client();
		$model = new Model_Users();

		$result = array('status' => '');

		if ( ! is_null($username) ) {

			$has_bl_username = false;
			foreach($this->blacklisted_usernames as $bl_username){
				if( strpos( $username, $bl_username) !== false){
					$has_bl_username = true;
					BREAK;
				}
			}

			//if (stripos($username, 'admin') === FALSE)
			if (!$has_bl_username)
			{
				if ( ! $client->call('Users.getByUsername', array($username)) ) {
					$result['status'] = 'not found';
				}
				else {
					$result['status'] = 'found';
				}
			}
			else
				$result['status'] = 'found';
		}
		elseif ( ! is_null($email) ) {

			if ( $client->call('Application.isDomainBlacklisted', array($email)) )
			{
				$result['status'] = 'domain blacklisted';
			}
			else
			{
				if ( ! $client->call('Users.getByEmail', array($email)) ) {
					$result['status'] = 'not found';
				}
				else {
					$result['status'] = 'found';
				}
			}
		}

		die(json_encode($result));
	}

	public function switchAgencyAction()
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$agency_id = $this->_getParam('id');
		$user = Model_Users::getCurrent();
		
		$old_agency = $client->call('Agencies.getByUserId', array($user->id));
		
		$linked_agencies = $client->call('Agencies.getLinkedAgencies', array($old_agency['id']));
		
		$found = false;
		foreach($linked_agencies as $l_ag) {
			if ( $l_ag['id'] == $agency_id ) $found = true;
		}
		
		if ( ! $found ) {
			$this->_response->setRedirect('/dashboard');
			return;
		}
		
		$agency = $client->call('Agencies.get', array($agency_id));
		
		$user->id = $agency['user_id'];
		
		$this->_response->setRedirect('/dashboard');
	}
}
