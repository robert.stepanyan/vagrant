<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	public function __construct($application)
	{
		require('../application/models/Plugin/Activity.php');

		parent::__construct($application);
	}

	public function run()
	{
		//Zend_Layout::startMvc();

		// Initialize hooks to avoid code confusion
		Model_Hooks::init();

		parent::run();
	}

	protected function _initRoutes()
	{
		require('../../library/Cubix/Security/Plugin.php');
		require('../application/models/Plugin/I18n.php');

		$this->bootstrap('frontController');
		$front = $this->getResource('frontController');

		$router = $front->getRouter();
		$router->removeDefaultRoutes();

		$router->addRoute(
			'video_manifest',
			new Zend_Controller_Router_Route('manifest',
				array(
					'module' => 'default',
					'controller' => 'index',
					'action' => 'manifest'
				)), array(

			)
		);
		
		$router->addRoute(
			'video',
			new Zend_Controller_Router_Route_Regex('^(video|Video)'
				,
				array(
					'module' => 'default',
					'controller' => 'video',
					'action' => 'index',
					'lang_id' => ''
					
				), array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);
		
		$router->addRoute(
			'video_upload',
			new Zend_Controller_Router_Route_Regex('^(video|Video)/upload',
				array(
					'module' => 'default',
					'controller' => 'video',
					'action' => 'upload',
					'lang_id' => ''
					
				), array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);
		
	
		$router->addRoute(
			'video_remove',
			new Zend_Controller_Router_Route_Regex('^(video|Video)/remove',
				array(
					'module' => 'default',
					'controller' => 'video',
					'action' => 'remove',
					'lang_id' => ''
					
				), array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);
		$router->addRoute(
			'get_video_escorts',
			new Zend_Controller_Router_Route_Regex('^(video|Video)/getvideo',
				array(
					'module' => 'default',
					'controller' => 'video',
					'action' => 'getvideo',
					'lang_id' => ''
					
				), array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);
		
		$router->addRoute(
			'home-page',
			new Zend_Controller_Router_Route(
				':lang_id/*',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'index',
					'lang_id' => ''
				), array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		/*$router->addRoute(
			'escorts-filter-def',
			new Zend_Controller_Router_Route_Regex(
				'^escorts(/.+)?',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'index'
				),
				array(
					1 => 'req'
				)
			)
		);*/

		$router->addRoute(
			'escorts-filter-def',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?(newest-independent|newest-agency-escorts|newest|citytours|upcomingtours|escorts-only|escorts|independent-escorts|agency-escorts|bdsm|boys-heterosexual|boys-bisexual|boys-homosexual|boys|trans)(-(.+)-([0-9]+))?',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'index',
					'lang_id' => null
				),
				array(
					2 => 'lang_id',
					3 => 'p_top_category',
					5 => 'p_city_slug',
					6 => 'p_city_id'
				)
			)
		);

		$router->addRoute(
			'escorts-filter-def-page',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?(newest-independent|newest-agency-escorts|newest|citytours|upcomingtours|escorts-only|escorts|independent-escorts|agency-escorts|bdsm|boys-heterosexual|boys-bisexual|boys-homosexual|boys|trans)-([0-9]+)',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'index',
					'lang_id' => null
				),
				array(
					2 => 'lang_id',
					3 => 'p_top_category',
					4 => 'p_page',					
				)
			)
		);

		$router->addRoute(
			'escorts-filter-city-page',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?(newest-independent|newest-agency-escorts|newest|citytours|upcomingtours|escorts-only|escorts|independent-escorts|agency-escorts|bdsm|boys-heterosexual|boys-bisexual|boys-homosexual|boys|trans)(-(.+)-([0-9]+)-([0-9]+))?',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'index',
					'lang_id' => null
				),
				array(
					2 => 'lang_id',
					3 => 'p_top_category',
					5 => 'p_city_slug',
					6 => 'p_city_id',
					7 => 'p_page'
				)
			)
		);

		/* CLASSIFIED ADS */
		$router->addRoute(
			'classified-ads',
			new Zend_Controller_Router_Route(':lang_id/classified-ads/:action',
				array(
					'module' => 'default',
					'controller' => 'classified-ads',
					'action' => 'index'
				)), array(
				'lang_id' => '[a-z]{2}'
			)
		);

		$router->addRoute(
			'classified-ads-def',
			new Zend_Controller_Router_Route('classified-ads/:action',
				array(
					'module' => 'default',
					'controller' => 'classified-ads',
					'action' => 'index'
				)), array(

			)
		);

		$router->addRoute(
			'classified-ads-details',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?classified-ads/(.+)?/ad/([0-9]+)',
				array(
					'module' => 'default',
					'controller' => 'classified-ads',
					'action' => 'ad'
				),
				array(
					2 => 'lang_id',
					3 => 'slug',
					4 => 'id'
				)
			)
		);

		$router->addRoute(
			'classified-ads-paging',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?classified-ads/([0-9]+)',
				array(
					'module' => 'default',
					'controller' => 'classified-ads',
					'action' => 'index'
				),
				array(
					2 => 'lang_id',
					3 => 'page'
				)
			)
		);

		$router->addRoute(
			'online-escorts',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?online-(escorts|independent-escorts|agency-escorts)',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'index',
					'online_now' => 1,
					'lang_id' => null
				),
				array(
					2 => 'lang_id',
					3 => 'p_top_category',
				)
			)
		);

		$router->addRoute(
			'agencies-show-tool-tip-def',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?agencies/show-tool-tip',
				array(
					'module' => 'default',
					'controller' => 'agencies',
					'action' => 'show-tool-tip',
					'lang_id' => null
				),
				array(
					2 => 'lang_id',
				)
			)
		);

		$router->addRoute(
			'agencies-filter-def',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?(agencies)(-(.+)-([0-9]+))?',
				array(
					'module' => 'default',
					'controller' => 'agencies',
					'action' => 'list',
					'lang_id' => null
				),
				array(
					2 => 'lang_id',
					3 => 'p_top_category',
					5 => 'p_city_slug',
					6 => 'p_city_id'
				)
			)
		);

		$router->addRoute(
			'agencies-filter-def-page',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?(agencies)-([0-9]+)',
				array(
					'module' => 'default',
					'controller' => 'agencies',
					'action' => 'list',
					'lang_id' => null
				),
				array(
					2 => 'lang_id',
					3 => 'p_top_category',
					4 => 'page',
				)
			)
		);

		$router->addRoute(
			'agencies-filter-city-page',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?(agencies)(-(.+)-([0-9]+)-([0-9]+))?',
				array(
					'module' => 'default',
					'controller' => 'agencies',
					'action' => 'list',
					'lang_id' => null
				),
				array(
					2 => 'lang_id',
					3 => 'p_top_category',
					5 => 'p_city_slug',
					6 => 'p_city_id',
					7 => 'page',
				)
			)
		);

		$router->addRoute(
			'agencies-countries-filter-def',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?(agencies)(-(.+)-c([0-9]+))?',
				array(
					'module' => 'default',
					'controller' => 'agencies',
					'action' => 'list',
					'lang_id' => null
				),
				array(
					2 => 'lang_id',
					3 => 'p_top_category',
					5 => 'p_country_slug',
					6 => 'p_country_id'
				)
			)
		);

		$router->addRoute(
			'agencies-countries-filter-def-page',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?(agencies)(-(.+)-c([0-9]+)-([0-9]+))?',
				array(
					'module' => 'default',
					'controller' => 'agencies',
					'action' => 'list',
					'lang_id' => null
				),
				array(
					2 => 'lang_id',
					3 => 'p_top_category',
					5 => 'p_country_slug',
					6 => 'p_country_id',
					7 => 'page',
				)
			)
		);

		$router->addRoute(
			'online-agencies',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?online-(agencies)',
				array(
					'module' => 'default',
					'controller' => 'agencies',
					'action' => 'list',
					'online_now' => 1,
					'lang_id' => null
				),
				array(
					2 => 'lang_id',
					3 => 'p_top_category',
				)
			)
		);		

		$router->addRoute(
			'escorts-countries-filter-def',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?(newest-independent|newest-agency-escorts|newest|citytours|upcomingtours|escorts-only|escorts|independent-escorts|agency-escorts|bdsm|boys-heterosexual|boys-bisexual|boys-homosexual|boys|trans)(-(.+)-c([0-9]+))?',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'index',
					'lang_id' => null
				),
				array(
					2 => 'lang_id',
					3 => 'p_top_category',
					5 => 'p_country_slug',
					6 => 'p_country_id'
				)
			)
		);

		$router->addRoute(
			'escorts-countries-filter-def-page',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?(newest-independent|newest-agency-escorts|newest|citytours|upcomingtours|escorts-only|escorts|independent-escorts|agency-escorts|bdsm|boys-heterosexual|boys-bisexual|boys-homosexual|boys|trans)(-(.+)-c([0-9]+)-([0-9]+))?',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'index',
					'lang_id' => null
				),
				array(
					2 => 'lang_id',
					3 => 'p_top_category',
					5 => 'p_country_slug',
					6 => 'p_country_id',
					7 => 'p_page',
				)
			)
		);

		$router->addRoute(
			'external-link',
			new Zend_Controller_Router_Route(
				'go',
				array(
					'module' => 'default',
					'controller' => 'index',
					'action' => 'go'
				)
			)
		);

		$router->addRoute(
			'links',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?links',
				array(
					'module' => 'default',
					'controller' => 'index',
					'action' => 'links',
					'lang_id' => null
				),
				array(
					2 => 'lang_id'
				))
		);

		$router->addRoute(
			'bubbles',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?ajax-bubble',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'ajax-bubble',
					'lang_id' => null
				),
				array(
					2 => 'lang_id'
				))
		);
				
		$router->addRoute(
			'blog',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?blog',
				array(
					'module' => 'default',
					'controller' => 'blog',
					'action' => 'index',
					'lang_id' => null
				),
				array(
					2 => 'lang_id'
				))
		);

		$router->addRoute(
			'blog-top10',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?blog/top10',
				array(
					'module' => 'default',
					'controller' => 'blog',
					'action' => 'top10',
					'lang_id' => null
				),
				array(
					2 => 'lang_id'
				))
		);

		$router->addRoute(
			'blog-details',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?blog/(.+)?\-([0-9]+)',
				array(
					'module' => 'default',
					'controller' => 'blog',
					'action' => 'details',
					'lang_id' => null
				),
				array(
					2 => 'lang_id',
					3 => 'slug',
					4 => 'id'
				)
			)
		);
		
		$router->addRoute(
			'blog-archive',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?blog/([.0-9]+)',
				array(
					'module' => 'default',
					'controller' => 'blog',
					'action' => 'index',
					'lang_id' => null
				),
				array(
					2 => 'lang_id',
					3 => 'date'
				)
			)
		);
		
		$router->addRoute(
			'blog-ajax-posts',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?blog/ajax-posts',
				array(
					'module' => 'default',
					'controller' => 'blog',
					'action' => 'ajax-posts',
					'lang_id' => null
				),
				array(
					2 => 'lang_id',
					3 => 'date'
				)
			)
		);
		
		$router->addRoute(
			'blog-ajax-comments',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?blog/ajax-comments',
				array(
					'module' => 'default',
					'controller' => 'blog',
					'action' => 'ajax-comments',
					'lang_id' => null
				),
				array(
					2 => 'lang_id'					
				)
			)
		);
		
		$router->addRoute(
			'blog-ajax-add-comment',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?blog/ajax-add-comment',
				array(
					'module' => 'default',
					'controller' => 'blog',
					'action' => 'ajax-add-comment',
					'lang_id' => null
				),
				array(
					2 => 'lang_id'					
				)
			)
		);
		
		$router->addRoute(
			'blog-ajax-add-reply',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?blog/ajax-add-reply',
				array(
					'module' => 'default',
					'controller' => 'blog',
					'action' => 'ajax-add-reply',
					'lang_id' => null
				),
				array(
					2 => 'lang_id'					
				)
			)
		);
		
		$router->addRoute(
			'blog-ajax-replies',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?blog/ajax-replies',
				array(
					'module' => 'default',
					'controller' => 'blog',
					'action' => 'ajax-replies',
					'lang_id' => null
				),
				array(
					2 => 'lang_id'					
				)
			)
		);

		$router->addRoute(
			'last-viewed-escorts',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/escorts/viewed-escorts',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'viewed-escorts'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'last-viewed-escorts-def',
			new Zend_Controller_Router_Route_Regex(
				'^escorts/viewed-escorts',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'viewed-escorts'
				)
			)
		);

		$router->addRoute(
			'reviews',
			new Zend_Controller_Router_Route('reviews/:action/*',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'index'
				))
		);

		$router->addRoute(
			'reviews-lng',
			new Zend_Controller_Router_Route(':lang_id/reviews/:action/*',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'index',
					'lang_id' => null
				),
				array(
					'lang_id' => '[a-z]{2}'
				))
		);

		$router->addRoute(
			'reviews-escort',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?reviews/(.+)?\-([0-9]+)',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'escort',
					'lang_id' => null
				),
				array(
					2 => 'lang_id',
					3 => 'showname',
					4 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'reviews-member',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?reviews/member/(.+)?',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'index',
					'mode' => 'exact',
					'lang_id' => null
				),
				array(
					2 => 'lang_id',
					3 => 'member'
				)
			)
		);
		
		$router->addRoute(
			'latest-actions',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?latest-actions',
				array(
					'module' => 'default',
					'controller' => 'latest-actions',
					'action' => 'index',
					'lang_id' => null
				),
				array(
					2 => 'lang_id'
				))
		);
		
		$router->addRoute(
			'latest-actions-ajax-get-details',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?latest-actions/ajax-get-details',
				array(
					'module' => 'default',
					'controller' => 'latest-actions',
					'action' => 'ajax-get-details',
					'lang_id' => null
				),
				array(
					2 => 'lang_id'
				))
		);
		
		$router->addRoute(
			'latest-actions-ajax-list',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?latest-actions/ajax-list',
				array(
					'module' => 'default',
					'controller' => 'latest-actions',
					'action' => 'ajax-list',
					'lang_id' => null
				),
				array(
					2 => 'lang_id'
				))
		);
		
		$router->addRoute(
			'latest-actions-ajax-header',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?latest-actions/ajax-header',
				array(
					'module' => 'default',
					'controller' => 'latest-actions',
					'action' => 'ajax-header',
					'lang_id' => null
				),
				array(
					2 => 'lang_id'
				))
		);


		/*PHOTO FEED*/
		$router->addRoute(
			'photo-feed',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?photo-feed',
				array(
					'module' => 'default',
					'controller' => 'photo-feed',
					'action' => 'index',
					'lang_id' => null
				),
				array(
					2 => 'lang_id'
				))
		);

		/*SEXY EURO '16*/
		$router->addRoute(
			'sexy-euro-16',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?sexy-euro-16',
				array(
					'module' => 'default',
					'controller' => 'sexy-euro',
					'action' => 'index',
					'lang_id' => null
				),
				array(
					2 => 'lang_id'
				))
		);
		
		/*SEXY EURO '16*/
		$router->addRoute(
			'sexy-euro-16-vote',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?sexy-euro-16/vote-for-escort',
				array(
					'module' => 'default',
					'controller' => 'sexy-euro',
					'action' => 'vote-for-escort',
					'lang_id' => null
				),
				array(
					2 => 'lang_id'
				))
		);
		
		$router->addRoute(
			'photo-feed-ajax-list',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?photo-feed/ajax-list',
				array(
					'module' => 'default',
					'controller' => 'photo-feed',
					'action' => 'ajax-list',
					'lang_id' => null
				),
				array(
					2 => 'lang_id'
				))
		);

		$router->addRoute(
			'photo-feed-ajax-header',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?photo-feed/ajax-header',
				array(
					'module' => 'default',
					'controller' => 'photo-feed',
					'action' => 'ajax-header',
					'lang_id' => null
				),
				array(
					2 => 'lang_id'
				))
		);

		$router->addRoute(
			'photo-feed-ajax-voting-box',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?photo-feed/ajax-voting-box/([0-9]+)/([0-9]+)',
				array(
					'module' => 'default',
					'controller' => 'photo-feed',
					'action' => 'ajax-voting-box',
					'lang_id' => null
				),
				array(
					2 => 'lang_id',
					3 => 'escort_id',
					4 => 'photo_id',
				))
		);
		$router->addRoute(
			'photo-feed-ajax-more-photos',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?photo-feed/ajax-get-more-photos/([0-9]+)/([0-9]+)/([0-9]+)',
				array(
					'module' => 'default',
					'controller' => 'photo-feed',
					'action' => 'ajax-get-more-photos',
					'lang_id' => null
				),
				array(
					2 => 'lang_id',
					3 => 'escort_id',
					4 => 'exclude_photo_id',
					5 => 'page',
				))
		);

		$router->addRoute(
			'photo-feed-ajax-vote',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?photo-feed/ajax-vote/([0-9]+)/([0-9]+)/([0-9]+)',
				array(
					'module' => 'default',
					'controller' => 'photo-feed',
					'action' => 'ajax-vote',
					'lang_id' => null
				),
				array(
					2 => 'lang_id',
					3 => 'escort_id',
					4 => 'photo_id',
					5 => 'rate',
				))
		);
		/*PHOTO FEED*/


		$router->addRoute(
			'captcha',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?captcha',
			array(
				'module' => 'default',
				'controller' => 'index',
				'action' => 'captcha',
					'lang_id' => null
			),
			array(
				2 => 'lang_id'
			))
		);

		$router->addRoute(
			'static-pages-def',
			new Zend_Controller_Router_Route_Regex(
				'^page/(.+)?',
				array(
					'module' => 'default',
					'controller' => 'static-page',
					'action' => 'show'
				),
				array(
					1 => 'page_slug'
				)
			)
		);

		$router->addRoute(
			'escort-photos-def',
			new Zend_Controller_Router_Route_Regex(
				'^escorts/photos',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'photos'
				)
			)
		);

		$router->addRoute(
			'escort-private-photos-def',
			new Zend_Controller_Router_Route_Regex(
				'^escorts/private-photos',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'private-photos'
				)
			)
		);

		$router->addRoute(
			'agency-profile-def',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?agency/(.+)?\-([0-9]+)$',
				array(
					'module' => 'default',
					'controller' => 'agencies',
					'action' => 'show',
					'lang_id' => null
				),
				array(
					2 => 'lang_id',
					3 => 'agencyName',
					4 => 'agency_id'
				)
			)
		);
		
		$router->addRoute(
			'escort-profile-v2-def',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?escort/(.+)?\-([0-9]+)$',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'profile-v2',
					'lang_id' => null
				),
				array(
					2 => 'lang_id',
					3 => 'escortName',
					4 => 'escort_id'
				)
			)
		);


		$router->addRoute(
			'geography',
			new Zend_Controller_Router_Route(':lang_id/geography/:action/*',
			array(
				'module' => 'default',
				'controller' => 'geography',
				'action' => 'index'
			), array(
				'lang_id' => '[a-z]{2}'
			))
		);

		$router->addRoute(
			'geography-def',
			new Zend_Controller_Router_Route('geography/:action/*',
			array(
				'module' => 'default',
				'controller' => 'geography',
				'action' => 'index'
			))
		);

		$router->addRoute(
			'private',
			new Zend_Controller_Router_Route(':lang_id/private/:action/*',
			array(
				'module' => 'default',
				'controller' => 'private',
				'action' => 'index'
			), array(
				'lang_id' => '[a-z]{2}'
			))
		);

		$router->addRoute(
			'private-def',
			new Zend_Controller_Router_Route('private/:action/*',
			array(
				'module' => 'default',
				'controller' => 'private',
				'action' => 'index'
			))
		);

		

		$router->addRoute(
			'privateV2Lng',
			new Zend_Controller_Router_Route(':lang_id/private-v2/:action/*',
			array(
				'module' => 'default',
				'controller' => 'private-v2',
				'action' => 'index',
				'lang_id' => ''
			),
			array(
				'lang_id' => '[a-z]{2}'
			))
		);

		$router->addRoute(
			'privateV2',
			new Zend_Controller_Router_Route('private-v2/:action/*',
			array(
				'module' => 'default',
				'controller' => 'private-v2',
				'action' => 'index'
			))
		);

		$router->addRoute(
			'privateProfileLng',
			new Zend_Controller_Router_Route(':lang_id/private-v2/profile/:action/*',
			array(
				'module' => 'default',
				'controller' => 'profile',
				'action' => 'index',
				'lang_id' => ''
			),
			array(
				'lang_id' => '[a-z]{2}'
			))
		);
		
		$router->addRoute(
			'privateProfile',
			new Zend_Controller_Router_Route('private-v2/profile/:action/*',
			array(
				'module' => 'default',
				'controller' => 'profile',
				'action' => 'index'
			))
		);
		
		$router->addRoute(
			'robots',
			new Zend_Controller_Router_Route('/robots.txt',
			array(
				'module' => 'default',
				'controller' => 'index',
				'action' => 'robots'
			))
		);

		$router->addRoute(
			'private-signup',
			new Zend_Controller_Router_Route_Regex('([a-z]{2})/private/signup-(member|vip-member|escort|agency)',
			array(
				'module' => 'default',
				'controller' => 'private',
				'action' => 'signup'
			), array(
				1 => 'lang_id',
				2 => 'type'
			))
		);

		$router->addRoute(
			'private-signup-def',
			new Zend_Controller_Router_Route_Regex('private/signup-(member|vip-member|escort|agency)',
			array(
				'module' => 'default',
				'controller' => 'private',
				'action' => 'signup'
			), array(
				1 => 'type'
			))
		);

		$router->addRoute(
			'private-activate',
			new Zend_Controller_Router_Route_Regex('([a-z]{2})/(.+)?/private/activate/(.+)?/([a-f0-9]{32})',
			array(
				'module' => 'default',
				'controller' => 'private',
				'action' => 'activate'
			), array(
				1 => 'lang_id',
				2 => 'user_type',
				3 => 'email',
				4 => 'hash'
			))
		);

		$router->addRoute(
			'private-activate-def',
			new Zend_Controller_Router_Route_Regex('(.+)?/private/activate/(.+)?/([a-f0-9]{32})',
			array(
				'module' => 'default',
				'controller' => 'private',
				'action' => 'activate'
			), array(
				1 => 'user_type',
				2 => 'email',
				3 => 'hash'
			))
		);
		
		$router->addRoute(
			'private-change-pass',
			new Zend_Controller_Router_Route_Regex('private/change-pass/([-_a-z0-9]+)/([a-f0-9]{32})',
			array(
				'module' => 'default',
				'controller' => 'private',
				'action' => 'change-pass',
			), array(
				1 => 'username',
				2 => 'hash'
			))
		);

		$router->addRoute(
			'private-verified-def',
			new Zend_Controller_Router_Route('verify/:action',
			array(
				'module' => 'default',
				'controller' => 'verify',
				'action' => 'index'
			))
		);
		$router->addRoute(
			'private-verified-lng',
			new Zend_Controller_Router_Route(':lang_id/verify/:action',
			array(
				'module' => 'default',
				'controller' => 'verify',
				'action' => 'index',
				'lang_id' => ''
			), array(
				'lang_id' => '[a-z]{2}'
			))
		);
		
		$router->addRoute(
			'city-alerts',
			new Zend_Controller_Router_Route('/city-alerts/:action/*',
			array(
				'module' => 'default',
				'controller' => 'city-alerts',
				'action' => 'index'
			))
		);
		$router->addRoute(
			'city-alerts-lng',
			new Zend_Controller_Router_Route(':lang_id/city-alerts/:action/*',
			array(
				'module' => 'default',
				'controller' => 'city-alerts',
				'action' => 'index',
				'lang_id' => ''
			), array(
				'lang_id' => '[a-z]{2}'
			))
		);

		$router->addRoute(
			'feedback',
			new Zend_Controller_Router_Route(':lang_id/feedback',
			array(
				'module' => 'default',
				'controller' => 'index',
				'action' => 'feedback'
			), array(
				'lang_id' => '[a-z]{2}'
			))
		);

		$router->addRoute(
			'feedback-def',
			new Zend_Controller_Router_Route('feedback',
			array(
				'module' => 'default',
				'controller' => 'index',
				'action' => 'feedback'
			))
		);

		$router->addRoute(
			'contact',
			new Zend_Controller_Router_Route(':lang_id/contact',
			array(
				'module' => 'default',
				'controller' => 'index',
				'action' => 'contact'
			), array(
				'lang_id' => '[a-z]{2}'
			))
		);

		$router->addRoute(
			'contact-def',
			new Zend_Controller_Router_Route('contact',
			array(
				'module' => 'default',
				'controller' => 'index',
				'action' => 'contact'
			))
		);


		/* --> Support <-- */
		$router->addRoute(
			'support',
			new Zend_Controller_Router_Route(':lang_id/support/:action',
			array(
				'module' => 'default',
				'controller' => 'support',
				'action' => 'index'
			), array(
				'lang_id' => '[a-z]{2}'
			))
		);

		$router->addRoute(
			'support-def',
			new Zend_Controller_Router_Route('support/:action',
			array(
				'module' => 'default',
				'controller' => 'support',
				'action' => 'index'
			))
		);
		/* <-- Support --> */




		$router->addRoute(
			'benchmark',
			new Zend_Controller_Router_Route('benchmark',
			array(
				'module' => 'default',
				'controller' => 'index',
				'action' => 'benchmark'
			))
		);

		$router->addRoute(
			'comments',
			new Zend_Controller_Router_Route(':lang_id/comments/ajax-show',
			array(
				'module' => 'default',
				'controller' => 'comments',
				'action' => 'ajax-show'
			)), array(
				'lang_id' => '[a-z]{2}'
			)
		);

		$router->addRoute(
			'comments-def',
			new Zend_Controller_Router_Route('comments/ajax-show',
			array(
				'module' => 'default',
				'controller' => 'comments',
				'action' => 'ajax-show'
			))
		);
		
		$router->addRoute(
			'comments-v2',
			new Zend_Controller_Router_Route(':lang_id/comments-v2/ajax-show',
			array(
				'module' => 'default',
				'controller' => 'comments-v2',
				'action' => 'ajax-show'
			)), array(
				'lang_id' => '[a-z]{2}'
			)
		);
		$router->addRoute(
			'comments-v2-def',
			new Zend_Controller_Router_Route('comments-v2/ajax-show',
			array(
				'module' => 'default',
				'controller' => 'comments-v2',
				'action' => 'ajax-show'
			))
		);
		
		$router->addRoute(
			'add-comment-v2',
			new Zend_Controller_Router_Route(':lang_id/comments-v2/ajax-add-comment',
			array(
				'module' => 'default',
				'controller' => 'comments-v2',
				'action' => 'ajax-add-comment'
			)), array(
				'lang_id' => '[a-z]{2}'
			)
		);
		$router->addRoute(
			'add-comment-v2-def',
			new Zend_Controller_Router_Route('comments-v2/ajax-add-comment',
			array(
				'module' => 'default',
				'controller' => 'comments-v2',
				'action' => 'ajax-add-comment'
			))
		);

		$router->addRoute(
			'add-comment',
			new Zend_Controller_Router_Route(':lang_id/comments/ajax-add-comment',
			array(
				'module' => 'default',
				'controller' => 'comments',
				'action' => 'ajax-add-comment'
			)), array(
				'lang_id' => '[a-z]{2}'
			)
		);

		$router->addRoute(
			'add-comment-def',
			new Zend_Controller_Router_Route('comments/ajax-add-comment',
			array(
				'module' => 'default',
				'controller' => 'comments',
				'action' => 'ajax-add-comment'
			))
		);

		$router->addRoute(
			'vote-comment',
			new Zend_Controller_Router_Route(':lang_id/comments-v2/ajax-vote',
			array(
				'module' => 'default',
				'controller' => 'comments',
				'action' => 'ajax-vote'
			)), array(
				'lang_id' => '[a-z]{2}'
			)
		);

		$router->addRoute(
			'vote-comment-def',
			new Zend_Controller_Router_Route('comments-v2/ajax-vote',
			array(
				'module' => 'default',
				'controller' => 'comments',
				'action' => 'ajax-vote'
			))
		);

		$router->addRoute(
			'member-info',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?member/(.+)?',
				array(
					'module' => 'default',
					'controller' => 'members',
					'action' => 'index'
				), array(
					2 => 'lang_id',
					3 => 'username'

				)
			)
		);

		$router->addRoute(
			'member-info-actions-lng',
			new Zend_Controller_Router_Route(':lang_id/members/:action/*',
			array(
				'module' => 'default',
				'controller' => 'members',
				'action' => 'index',
				'lang_id' => ''
			),
			array(
				'lang_id' => '[a-z]{2}'
			))
		);

		$router->addRoute(
			'member-info-actions',
			new Zend_Controller_Router_Route('members/:action/*',
			array(
				'module' => 'default',
				'controller' => 'members',
				'action' => 'index'
			))
		);

		$router->addRoute(
			'newsletter-cron-def',
			new Zend_Controller_Router_Route_Regex(
				'^newsletter/email-sync-cron',
				array(
					'module' => 'default',
					'controller' => 'newsletter',
					'action' => 'email-sync-cron'
				),
				array(

				)
			)
		);
		
		/* comment Subpage*/

        $router->addRoute(
			'subpagecomments',
			new Zend_Controller_Router_Route(':lang_id/comments',
			array(
				'module' => 'default',
				'controller' => 'comments',
				'action' => 'index'
			)), array(
				'lang_id' => '[a-z]{2}'
			)
		);

		$router->addRoute(
			'subpagecomments-def',
			new Zend_Controller_Router_Route('comments',
			array(
				'module' => 'default',
				'controller' => 'comments',
				'action' => 'index'
			))
		);

		$router->addRoute(
			'subpagecomments-escort',
			new Zend_Controller_Router_Route(
				':lang_id/escortcomments',
				array(
					'module' => 'default',
					'controller' => 'comments',
					'action' => 'get-escort-comments'
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'subpagecomments-escort-def',
			new Zend_Controller_Router_Route_Regex(
				'escortcomments',
				array(
					'module' => 'default',
					'controller' => 'comments',
					'action' => 'get-escort-comments'
				)
			)
		);
        /* Comments subpage*/

		$router->addRoute(
			'api-main',
			new Zend_Controller_Router_Route('api/:controller/:action/*',
			array(
				'module' => 'api',
				'controller' => 'index',
				'action' => 'index'
			))
		);

		$router->addRoute(
			'index-controller',
			new Zend_Controller_Router_Route('/index/:action',
			array(
				'module' => 'default',
				'controller' => 'index',
				'action' => 'index'
			))
		);

		$router->addRoute(
			'index-controller-lng',
			new Zend_Controller_Router_Route(':lang_id/index/:action',
			array(
				'module' => 'default',
				'controller' => 'index',
				'action' => 'index'
			),
			array(
				'lang_id' => '[a-z]{2}'
			))
		);


        $router->addRoute(
			'bubbles',
			new Zend_Controller_Router_Route('ajax-bubble',
			array(
				'module' => 'default',
				'controller' => 'escorts',
				'action' => 'ajax-bubble'
			))
		);

        $router->addRoute(
			'online',
			new Zend_Controller_Router_Route('ajax-online',
			array(
				'module' => 'default',
				'controller' => 'escorts',
				'action' => 'ajax-online'
			))
		);

        $router->addRoute(
			'confirm-deletion-lang',
			new Zend_Controller_Router_Route_Regex('([a-z]{2})/private-v2/confirm-deletion/([a-f0-9]{10})',
			array(
				'module' => 'default',
				'controller' => 'private-v2',
				'action' => 'confirm-deletion'
			), array(
				1 => 'lang_id',
				2 => 'hash'
			))
		);

        $router->addRoute(
			'confirm-deletion',
			new Zend_Controller_Router_Route_Regex('private-v2/confirm-deletion/([a-f0-9]{10})',
			array(
				'module' => 'default',
				'controller' => 'private-v2',
				'action' => 'confirm-deletion'
			), array(
				1 => 'hash'
			))
		);

		$router->addRoute(
			'get-gallery-photos',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?escorts/gallery-photos',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'gallery-photos'
				),
				array(
					2 => 'lang_id'
				)
			)
		);
		
		$router->addRoute(
			'ajax-tell-friend',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?escorts/ajax-tell-friend',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'ajax-tell-friend'
				),
				array(
					2 => 'lang_id'
				)
			)
		);
		
		$router->addRoute(
			'ajax-susp-photo',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?escorts/ajax-susp-photo',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'ajax-susp-photo'
				),
				array(
					2 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'ajax-report-problem',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?escorts/ajax-report-problem',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'ajax-report-problem'
				),
				array(
					2 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'email-collecting-popup',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?index/email-collecting-popup',
				array(
					'module' => 'default',
					'controller' => 'index',
					'action' => 'email-collecting-popup'
				), array(
					2 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'get-filter',
			new Zend_Controller_Router_Route_Regex('(([a-z]{2})/)?escorts/get-filter',
			array(
				'module' => 'default',
				'controller' => 'escorts',
				'action' => 'get-filter'
			), array(
				2 => 'lang_id'
			))
		);

        $router->addRoute(
            'get-sitemap',
            new Zend_Controller_Router_Route(
                '/sitemap',
                array(
                    'module' => 'default',
                    'controller' => 'sitemap',
                    'action' => 'index'
                )
            )
        );

        $router->addRoute(
            'render-sitemap',
            new Zend_Controller_Router_Route(
                '/sitemap/render',
                array(
                    'module' => 'default',
                    'controller' => 'sitemap',
                    'action' => 'render'
                )
            )
        );

        
/******************************************************* Mobile **********************************************************/

		// <editor-fold defaultstate="collapsed" desc="Mobile Version">

		$router->addRoute(
			'mobile',
			new Zend_Controller_Router_Route('mobile/:controller/:action/*',
                array(
                    'module' => 'mobile',
                    'controller' => 'escorts',
                    'action' => 'index'
                )
            )
		);

        $router->addRoute(
            'mobile-home-page',
            new Zend_Controller_Router_Route(
                'mobile/:lang_id/',
                array(
                    'module' => 'mobile',
                    'controller' => 'escorts',
                    'action' => 'index',
                    'lang_id' => ''
                ), array(
                    'lang_id' => '[a-z]{2}'
                )
            )
        );


        $router->addRoute(
            'def-mobile',
            new Zend_Controller_Router_Route('mobile/:lang_id/:controller/:action/*',
                array(
                    'module' => 'mobile',
                    'controller' => 'escorts',
                    'action' => 'index',
                    'lang_id' => ''
                ), array(
                    'lang_id' => '[a-z]{2}'
                )
            )
        );

        $router->addRoute(
            'mobile-allcities',
            new Zend_Controller_Router_Route_Regex(
                '^mobile/([a-z]{2})/allcities',
                array(
                    'module' => 'mobile',
                    'controller' => 'index',
                    'action' => 'index',
                    'all' => 1
                )
            )
        );

        $router->addRoute(
            'mobile-escorts-filter-def',
            new Zend_Controller_Router_Route_Regex(
                '^mobile/(([a-z]{2})\/)?(newest-independent|newest-agency-escorts|newest|escorts-only|escorts|agencies|independent-escorts|agency-escorts|bdsm|boys-heterosexual|boys-bisexual|boys-homosexual|boys|trans)(-(.+)-([0-9]+))?',
                array(
                    'module' => 'mobile',
                    'controller' => 'escorts',
                    'action' => 'index'
                ),
                array(
                    2 => 'lang_id',
                    3 => 'p_top_category',
                    5 => 'p_city_slug',
                    6 => 'p_city_id'
                )
            )
        );

        $router->addRoute(
            'mobile-escorts-filter-page',
            new Zend_Controller_Router_Route_Regex(
                '^mobile/(([a-z]{2})\/)?(newest-independent|newest-agency-escorts|newest|escorts-only|escorts|agencies|independent-escorts|agency-escorts|bdsm|boys-heterosexual|boys-bisexual|boys-homosexual|boys|trans)(-(.+)-([0-9]+)(-([0-9]+))?)?',
                array(
                    'module' => 'mobile',
                    'controller' => 'escorts',
                    'action' => 'index'
                ),
                array(
                    2 => 'lang_id',
                    3 => 'p_top_category',
                    5 => 'p_city_slug',
                    6 => 'p_city_id',
                    8 => 'p_page'
                )
            )
        );

        $router->addRoute(
            'mobile-escorts-countries-filter-def',
            new Zend_Controller_Router_Route_Regex(
                '^mobile/(([a-z]{2})\/)?(newest-independent|newest-agency-escorts|newest|escorts-only|escorts|agencies|independent-escorts|agency-escorts|bdsm|boys-heterosexual|boys-bisexual|boys-homosexual|boys|trans)(-(.+)-c([0-9]+))?',
                array(
                    'module' => 'mobile',
                    'controller' => 'escorts',
                    'action' => 'index'
                ),
                array(
                    2 => 'lang_id',
                    3 => 'p_top_category',
                    5 => 'p_country_slug',
                    6 => 'p_country_id'
                )
            )
        );

		$router->addRoute(
			'mobile-allcities-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/allcities',
				array(
					'module' => 'mobile',
					'controller' => 'index',
					'action' => 'index',
                    'all' => 1
				)
			)
		);



        $router->addRoute(
            '^mobile/private-activate',
            new Zend_Controller_Router_Route_Regex('([a-z]{2})/(.+)?/private/activate/(.+)?/([a-f0-9]{32})',
                array(
                    'module' => 'mobile',
                    'controller' => 'private',
                    'action' => 'activate'
                ), array(
                    1 => 'lang_id',
                    2 => 'user_type',
                    3 => 'email',
                    4 => 'hash'
                ))
        );

        $router->addRoute(
            '^mobile/private-activate-def',
            new Zend_Controller_Router_Route_Regex('(.+)?/private/activate/(.+)?/([a-f0-9]{32})',
                array(
                    'module' => 'mobile',
                    'controller' => 'private',
                    'action' => 'activate'
                ), array(
                    1 => 'user_type',
                    2 => 'email',
                    3 => 'hash'
                ))
        );

        $router->addRoute(
            '^mobile/private-change-pass',
            new Zend_Controller_Router_Route_Regex('private/change-pass/([-_a-z0-9]+)/([a-f0-9]{32})',
                array(
                    'module' => 'mobile',
                    'controller' => 'private',
                    'action' => 'change-pass',
                ), array(
                    1 => 'username',
                    2 => 'hash'
                ))
        );


		$router->addRoute(
			'mobile-robots',
			new Zend_Controller_Router_Route('mobile/robots.txt',
			array(
				'module' => 'mobile',
				'controller' => 'index',
				'action' => 'robots'
			))
		);

        $router->addRoute(
			'mobile-escorts',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/escorts(/.+)?',
				array(
					'module' => 'mobile',
					'controller' => 'escorts',
					'action' => 'index'
				),
				array(
					1 => 'lang_id',
					2 => 'req'
				)
			)
		);

        $router->addRoute(
            'mobile-comments-def',
            new Zend_Controller_Router_Route('^mobile/comments',
                array(
                    'module' => 'mobile',
                    'controller' => 'comments-v2',
                    'action' => 'index'
                ))
        );

        $router->addRoute(
            'mobile-comments',
            new Zend_Controller_Router_Route_Regex(
                '^mobile/(([a-z]{2})\/)?comments',
                array(
                    'module' => 'mobile',
                    'controller' => 'comments-v2',
                    'action' => 'index'
                ),
                array(
                    2 => 'lang_id'
                )
            )
        );

        $router->addRoute(
            'mobile-reviews-def',
            new Zend_Controller_Router_Route('^mobile/evaluations',
                array(
                    'module' => 'mobile',
                    'controller' => 'reviews',
                    'action' => 'index'
                ))
        );

        $router->addRoute(
            'mobile-reviews',
            new Zend_Controller_Router_Route_Regex(
                '^mobile/(([a-z]{2})\/)?evaluations',
                array(
                    'module' => 'mobile',
                    'controller' => 'reviews',
                    'action' => 'index'
                ),
                array(
                    2 => 'lang_id'
                )
            )
        );

		$router->addRoute(
			'mobile-escorts-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/escorts(/.+)?',
				array(
					'module' => 'mobile',
					'controller' => 'escorts',
					'action' => 'index'
				),
				array(
					1 => 'req'
				)
			)
		);

        $router->addRoute(
            'mobile-contact-form',
            new Zend_Controller_Router_Route_Regex(
                '^mobile/escorts/contact-form',
                array(
                    'module' => 'mobile',
                    'controller' => 'escorts',
                    'action' => 'contact-form'
                ),
                array(
                    1 => 'req'
                )
            )
        );

        $router->addRoute(
            'mobile-escort-popup',
            new Zend_Controller_Router_Route_Regex(
                '^mobile/escorts/popup',
                array(
                    'module' => 'mobile',
                    'controller' => 'escorts',
                    'action' => 'popup'
                ),
                array(
                    1 => 'req'
                )
            )
        );

        $router->addRoute(
			'mobile-escort-favorites',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/favorites',
				array(
					'module' => 'mobile',
					'controller' => 'index',
					'action' => 'favorites'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-favorites-def',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/favorites',
				array(
					'module' => 'mobile',
					'controller' => 'index',
					'action' => 'favorites'
				)
			)
		);


        $router->addRoute(
			'mobile-escort-search',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/(([a-z]{2})\/)?search',
				array(
					'module' => 'mobile',
					'controller' => 'index',
					'action' => 'search'
				),
				array(
					2 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-search-def',
			new Zend_Controller_Router_Route_Regex(
				'^/mobile/search',
				array(
					'module' => 'mobile',
					'controller' => 'index',
					'action' => 'search'
				)
			)
		);

        /*$router->addRoute(
            'mobile-escort-profile-def',
            new Zend_Controller_Router_Route_Regex(
                '^mobile/escort/(.+)?\-([0-9]+)',
                array(
                    'module' => 'mobile',
                    'controller' => 'profile',
                    'action' => 'escort'
                ),
                array(
                    1 => 'escortName',
                    2 => 'escort_id'
                )
            )
        );*/


        $router->addRoute(
			'mobile-escort-profile',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/(([a-z]{2})\/)?escort/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobile',
					'controller' => 'profile',
					'action' => 'escort'
				),
				array(
					2 => 'lang_id',
					3 => 'escortName',
					4 => 'escort_id'
				)
			)
		);

        $router->addRoute(
			'mobile-escort-services',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/escort/services/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobile',
					'controller' => 'profile',
					'action' => 'services'
				),
				array(
					1 => 'lang_id',
					2 => 'escortName',
					3 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-services-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/escort/services/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobile',
					'controller' => 'profile',
					'action' => 'services'
				),
				array(
					1 => 'escortName',
					2 => 'escort_id'
				)
			)
		);

        $router->addRoute(
			'mobile-escort-rates',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/escort/rates/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobile',
					'controller' => 'profile',
					'action' => 'rates'
				),
				array(
					1 => 'lang_id',
					2 => 'escortName',
					3 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-rates-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/escort/rates/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobile',
					'controller' => 'profile',
					'action' => 'rates'
				),
				array(
					1 => 'escortName',
					2 => 'escort_id'
				)
			)
		);

        $router->addRoute(
			'mobile-escort-contacts',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/escort/contacts/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobile',
					'controller' => 'profile',
					'action' => 'contacts'
				),
				array(
					1 => 'lang_id',
					2 => 'escortName',
					3 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-contacts-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/escort/contacts/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobile',
					'controller' => 'profile',
					'action' => 'contacts'
				),
				array(
					1 => 'escortName',
					2 => 'escort_id'
				)
			)
		);


        $router->addRoute(
			'mobile-escort-tours',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/escort/tours/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobile',
					'controller' => 'profile',
					'action' => 'tours'
				),
				array(
					1 => 'lang_id',
					2 => 'escortName',
					3 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-tours-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/escort/tours/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobile',
					'controller' => 'profile',
					'action' => 'tours'
				),
				array(
					1 => 'escortName',
					2 => 'escort_id'
				)
			)
		);

        $router->addRoute(
            'mobile-escort-profile-v2-def',
            new Zend_Controller_Router_Route_Regex(
                '^mobile/(([a-z]{2})\/)?escort/(.+)?\-([0-9]+)$',
                array(
                    'module' => 'mobile',
                    'controller' => 'escorts',
                    'action' => 'profile-v2'
                ),
                array(
                    2 => 'lang_id',
                    3 => 'escortName',
                    4 => 'escort_id'
                )
            )
        );

        $router->addRoute(
			'mobile-static-pages',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/page/(.+)?',
				array(
					'module' => 'mobile',
					'controller' => 'page',
					'action' => 'show'
				),
				array(
					1 => 'lang_id',
					2 => 'page_slug'
				)
			)
		);

		$router->addRoute(
			'mobile-static-pages-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/page/(.+)?',
				array(
					'module' => 'mobile',
					'controller' => 'page',
					'action' => 'show'
				),
				array(
					1 => 'page_slug'
				)
			)
		);

        $router->addRoute(
            'mobile-captcha-def',
            new Zend_Controller_Router_Route_Regex(
                '^mobile/(([a-z]{2})\/)?captcha',
                array(
                    'module' => 'mobile',
                    'controller' => 'index',
                    'action' => 'captcha'
                )),
                array(
                    2 => 'lang_id'
                )
        );

        $router->addRoute(
            'mobile-captcha-get-hast',
            new Zend_Controller_Router_Route('/mobile/escorts/get-captcha-status',
                array(
                    'module' => 'mobile',
                    'controller' => 'escorts',
                    'action' => 'get-captcha-status'
                ))
        );

        $router->addRoute(
			'mobile-escort-photo',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/escort/photo/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobile',
					'controller' => 'profile',
					'action' => 'photo'
				),
				array(
					1 => 'lang_id',
					2 => 'escortName',
					3 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-photo-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/escort/photo/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobile',
					'controller' => 'profile',
					'action' => 'photo'
				),
				array(
					1 => 'escortName',
					2 => 'escort_id'
				)
			)
		);

		/* mobile-photos */
		$router->addRoute(
			'mobile-escort-photos',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/escort/photos/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobile',
					'controller' => 'profile',
					'action' => 'profile-photos'
				),
				array(
					1 => 'lang_id',
					2 => 'escortName',
					3 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-photos-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/escort/photos/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobile',
					'controller' => 'profile',
					'action' => 'profile-photos'
				),
				array(
					1 => 'escortName',
					2 => 'escort_id'
				)
			)
		);
		/* escort photos end */



        $router->addRoute(
			'mobile-add-to-favorites',
			new Zend_Controller_Router_Route_Regex('([a-z]{2})/mobile/add-to-favorites(.+)?',
			array(
				'module' => 'mobile',
				'controller' => 'index',
				'action' => 'add-to-favorites'
			), array(
				1 => 'lang_id'
			))
		);

		$router->addRoute(
			'mobile-add-to-favorites-def',
			new Zend_Controller_Router_Route_Regex('mobile/add-to-favorites(.+)?',
			array(
				'module' => 'mobile',
				'controller' => 'index',
				'action' => 'add-to-favorites'
			))
		);

        $router->addRoute(
			'mobile-remove-from-favorites',
			new Zend_Controller_Router_Route_Regex('([a-z]{2})/mobile/remove-from-favorites(.+)?',
			array(
				'module' => 'mobile',
				'controller' => 'index',
				'action' => 'remove-from-favorites'
			), array(
				1 => 'lang_id'
			))
		);

		$router->addRoute(
			'mobile-remove-from-favorites-def',
			new Zend_Controller_Router_Route_Regex('mobile/remove-from-favorites(.+)?',
			array(
				'module' => 'mobile',
				'controller' => 'index',
				'action' => 'remove-from-favorites'
			))
		);

        $router->addRoute(
			'mobile-feedback',
			new Zend_Controller_Router_Route(':lang_id/mobile/feedback',
			array(
				'module' => 'mobile',
				'controller' => 'index',
				'action' => 'feedback'
			), array(
				'lang_id' => '[a-z]{2}'
			))
		);

		$router->addRoute(
			'mobile-feedback-def',
			new Zend_Controller_Router_Route('mobile/feedback',
			array(
				'module' => 'mobile',
				'controller' => 'index',
				'action' => 'feedback'
			))
		);



		$router->addRoute(
			'mobile-escort-evaluations',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/evaluations/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobile',
					'controller' => 'profile',
					'action' => 'reviews'
				),
				array(
					1 => 'lang_id',
					2 => 'escortName',
					3 => 'escort_id'
				)
			)
		);

        $router->addRoute(
            'mobile-ajax-get-top-search',
            new Zend_Controller_Router_Route_Regex(
                '^mobile/escorts/ajax-get-top-search',
                array(
                    'module' => 'mobile',
                    'controller' => 'escorts',
                    'action' => 'ajax-get-top-search'
                )
            )
        );

		$router->addRoute(
			'mobile-escort-evaluations-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/evaluations/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobile',
					'controller' => 'profile',
					'action' => 'reviews'
				),
				array(
					1 => 'escortName',
					2 => 'escort_id'
				)
			)
		);


		$router->addRoute(
			'mobile-evaluations',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/evaluations',
				array(
					'module' => 'mobile',
					'controller' => 'escorts',
					'action' => 'reviews'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'mobile-evaluations-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/evaluations',
				array(
					'module' => 'mobile',
					'controller' => 'escorts',
					'action' => 'reviews'
				),
				array(
				)
			)
		);

        $router->addRoute(
            'mob-geography',
            new Zend_Controller_Router_Route_Regex('^mobile/(([a-z]{2})\/)?/geography/ajax-get-cities*',
                array(
                    'module' => 'mobile',
                    'controller' => 'geography',
                    'action' => 'index'
                ),
                array(
                    2 => 'lang_id'
                ))
        );

        /*$router->addRoute(
            'mob-geography-def',
            new Zend_Controller_Router_Route('geography/:action/*',
                array(
                    'module' => 'mobile',
                    'controller' => 'geography',
                    'action' => 'index'
                ))
        );*/

        $router->addRoute(
            'mobile-contacts',
            new Zend_Controller_Router_Route('/contacts/:action/*',
                array(
                    'module' => 'mobile',
                    'controller' => 'contacts',
                    'action' => 'index'
                ))
        );
		
		$router->addRoute(
			'mobile-external-link',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/go',
				array(
					'module' => 'mobile',
					'controller' => 'index',
					'action' => 'go'
				)
			)
		);

        /*$router->addRoute(
            'mob-reviews',
            new Zend_Controller_Router_Route('reviews/:action/*',
                array(
                    'module' => 'mobile',
                    'controller' => 'reviews',
                    'action' => 'ajax-list'
                ))
        );*/

        /*$router->addRoute(
            'mob-reviews-lng',
            new Zend_Controller_Router_Route(':lang_id/reviews/:action/*',
                array(
                    'module' => 'mobile',
                    'controller' => 'reviews',
                    'action' => 'index',
                    'lang_id' => null
                ),
                array(
                    'lang_id' => '[a-z]{2}'
                ))
        );*/

        $router->addRoute(
            'mob-reviews-df',
            new Zend_Controller_Router_Route_Regex('^mobile/reviews',
                array(
                    'module' => 'mobile',
                    'controller' => 'reviews',
                    'action' => 'ajax-list'
                ))
        );

        $router->addRoute(
            'mob-reviews-lngs',
            new Zend_Controller_Router_Route_Regex('^mobile/(([a-z]{2})/)?reviews',
                array(
                    'module' => 'mobile',
                    'controller' => 'reviews',
                    'action' => 'ajax-list',
                    'lang_id' => null
                ),
                array(
                    2 => 'lang_id'
                ))
        );

        $router->addRoute(
            'mob-reviews-escort',
            new Zend_Controller_Router_Route_Regex('^mobile/(([a-z]{2})/)?reviews/(.+)?\-([0-9]+)',
                array(
                    'module' => 'mobile',
                    'controller' => 'reviews',
                    'action' => 'escort',
                    'lang_id' => null
                ),
                array(
                    2 => 'lang_id',
                    3 => 'showname',
                    4 => 'escort_id'
                )
            )
        );

        $router->addRoute(
            'mob-reviews-member',
            new Zend_Controller_Router_Route_Regex('^mobile/(([a-z]{2})/)?reviews/member/(.+)?',
                array(
                    'module' => 'mobile',
                    'controller' => 'reviews',
                    'action' => 'ajax-list',
                    'mode' => 'exact',
                    'lang_id' => null
                ),
                array(
                    2 => 'lang_id',
                    3 => 'member'
                )
            )
        );

        $router->addRoute(
            'mob-online-escorts',
            new Zend_Controller_Router_Route_Regex(
                '^mobile/(([a-z]{2})/)?online-(escorts|independent-escorts|agency-escorts)',
                array(
                    'module' => 'mobile',
                    'controller' => 'escorts',
                    'action' => 'index',
                    'online_now' => 1,
                    'lang_id' => null
                ),
                array(
                    2 => 'lang_id',
                    3 => 'p_top_category',
                )
            )
        );

        $router->addRoute(
            'mob-epg-payment',
            new Zend_Controller_Router_Route_Regex(
                '^mobile/(([a-z]{2})/)?payment',
                array(
                    'module' => 'mobile',
                    'controller' => 'online-billing',
                    'action' => 'payment',
                    'lang_id' => null
                ),
                array(
                    2 => 'lang_id'
                ))
        );
        $router->addRoute(
            'mob-epg-payment-success',
            new Zend_Controller_Router_Route_Regex(
                '^mobile/(([a-z]{2})/)?payment-success',
                array(
                    'module' => 'mobile',
                    'controller' => 'online-billing',
                    'action' => 'payment-success',
                    'lang_id' => null
                ),
                array(
                    2 => 'lang_id'
                ))

        );

        $router->addRoute(
            'mob-latest-actions',
            new Zend_Controller_Router_Route_Regex('^mobile/(([a-z]{2})/)?latest-actions',
                array(
                    'module' => 'mobile',
                    'controller' => 'latest-actions',
                    'action' => 'index',
                    'lang_id' => null
                ),
                array(
                    2 => 'lang_id'
                ))
        );

        $router->addRoute(
            'mob-latest-actions-ajax-get-details',
            new Zend_Controller_Router_Route_Regex('^mobile/(([a-z]{2})/)?latest-actions/ajax-get-details',
                array(
                    'module' => 'mobile',
                    'controller' => 'latest-actions',
                    'action' => 'ajax-get-details',
                    'lang_id' => null
                ),
                array(
                    2 => 'lang_id'
                ))
        );

        $router->addRoute(
            'mob-latest-actions-ajax-list',
            new Zend_Controller_Router_Route_Regex('^mobile/(([a-z]{2})/)?latest-actions/ajax-list',
                array(
                    'module' => 'mobile',
                    'controller' => 'latest-actions',
                    'action' => 'ajax-list',
                    'lang_id' => null
                ),
                array(
                    2 => 'lang_id'
                ))
        );

        $router->addRoute(
            'mob-latest-actions-ajax-header',
            new Zend_Controller_Router_Route_Regex('^mobile/(([a-z]{2})/)?latest-actions/ajax-header',
                array(
                    'module' => 'mobile',
                    'controller' => 'latest-actions',
                    'action' => 'ajax-header',
                    'lang_id' => null
                ),
                array(
                    2 => 'lang_id'
                ))
        );/* CLASSIFIED ADS */
        $router->addRoute(
            'mob-classified-ads',
            new Zend_Controller_Router_Route('mobile/:lang_id/classified-ads/:action',
                array(
                    'module' => 'mobile',
                    'controller' => 'classified-ads',
                    'action' => 'index'
                )), array(
                'lang_id' => '[a-z]{2}'
            )
        );

        $router->addRoute(
            'mob-classified-ads-def',
            new Zend_Controller_Router_Route('mobile/classified-ads/:action',
                array(
                    'module' => 'mobile',
                    'controller' => 'classified-ads',
                    'action' => 'index'
                )), array(

            )
        );

        $router->addRoute(
            'mob-classified-ads-details',
            new Zend_Controller_Router_Route_Regex('^mobile/(([a-z]{2})/)?classified-ads/(.+)?/ad/([0-9]+)',
                array(
                    'module' => 'mobile',
                    'controller' => 'classified-ads',
                    'action' => 'ad'
                ),
                array(
                    2 => 'lang_id',
                    3 => 'slug',
                    4 => 'id'
                )
            )
        );

        $router->addRoute(
            'mob-classified-ads-paging',
            new Zend_Controller_Router_Route_Regex('^mobile/(([a-z]{2})/)?classified-ads/([0-9]+)',
                array(
                    'module' => 'mobile',
                    'controller' => 'classified-ads',
                    'action' => 'index'
                ),
                array(
                    2 => 'lang_id',
                    3 => 'page'
                )
            )
        );

        $router->addRoute(
            'mobile-blog',
            new Zend_Controller_Router_Route_Regex('^mobile/(([a-z]{2})/)?blog',
                array(
                    'module' => 'mobile',
                    'controller' => 'blog',
                    'action' => 'index',
                    'lang_id' => null
                ),
                array(
                    2 => 'lang_id'
                ))
        );

        $router->addRoute(
            'mobile-blog-details',
            new Zend_Controller_Router_Route_Regex('^mobile/(([a-z]{2})/)?blog/(.+)?\-([0-9]+)',
                array(
                    'module' => 'mobile',
                    'controller' => 'blog',
                    'action' => 'details',
                    'lang_id' => null
                ),
                array(
                    2 => 'lang_id',
                    3 => 'slug',
                    4 => 'id'
                )
            )
        );

        $router->addRoute(
            'mobile-blog-archive',
            new Zend_Controller_Router_Route_Regex('^mobile/(([a-z]{2})/)?blog/([.0-9]+)',
                array(
                    'module' => 'mobile',
                    'controller' => 'blog',
                    'action' => 'index',
                    'lang_id' => null
                ),
                array(
                    2 => 'lang_id',
                    3 => 'date'
                )
            )
        );

        $router->addRoute(
            'mobile-blog-ajax-posts',
            new Zend_Controller_Router_Route_Regex('^mobile/(([a-z]{2})/)?blog/ajax-posts',
                array(
                    'module' => 'mobile',
                    'controller' => 'blog',
                    'action' => 'ajax-posts',
                    'lang_id' => null
                ),
                array(
                    2 => 'lang_id',
                    3 => 'date'
                )
            )
        );

        $router->addRoute(
            'mobile-blog-ajax-comments',
            new Zend_Controller_Router_Route_Regex('^mobile/(([a-z]{2})/)?blog/ajax-comments',
                array(
                    'module' => 'mobile',
                    'controller' => 'blog',
                    'action' => 'ajax-comments',
                    'lang_id' => null
                ),
                array(
                    2 => 'lang_id'
                )
            )
        );

        $router->addRoute(
            'mobile-blog-ajax-add-comment',
            new Zend_Controller_Router_Route_Regex('^mobile/(([a-z]{2})/)?blog/ajax-add-comment',
                array(
                    'module' => 'mobile',
                    'controller' => 'blog',
                    'action' => 'ajax-add-comment',
                    'lang_id' => null
                ),
                array(
                    2 => 'lang_id'
                )
            )
        );

        $router->addRoute(
            'mobile-blog-ajax-add-reply',
            new Zend_Controller_Router_Route_Regex('^mobile/(([a-z]{2})/)?blog/ajax-add-reply',
                array(
                    'module' => 'mobile',
                    'controller' => 'blog',
                    'action' => 'ajax-add-reply',
                    'lang_id' => null
                ),
                array(
                    2 => 'lang_id'
                )
            )
        );

        $router->addRoute(
            'mobile-blog-ajax-replies',
            new Zend_Controller_Router_Route_Regex('^mobile/(([a-z]{2})/)?blog/ajax-replies',
                array(
                    'module' => 'mobile',
                    'controller' => 'blog',
                    'action' => 'ajax-replies',
                    'lang_id' => null
                ),
                array(
                    2 => 'lang_id'
                )
            )
        );

        /******************* Private Area *******************/

        $router->addRoute(
            'mobile-privateV2Lng',
            new Zend_Controller_Router_Route('mobile/:lang_id/private-v2/:action/*',
                array(
                    'module' => 'mobile',
                    'controller' => 'private-v2',
                    'action' => 'index',
                    'lang_id' => ''
                ),
                array(
                    'lang_id' => '[a-z]{2}'
                ))
        );

        $router->addRoute(
            'mobile-privateV2',
            new Zend_Controller_Router_Route('mobile/private-v2/:action/*',
                array(
                    'module' => 'mobile',
                    'controller' => 'private-v2',
                    'action' => 'index'
                ))
        );

        $router->addRoute(
            'mobile-privateProfileLng',
            new Zend_Controller_Router_Route('mobile/:lang_id/private-v2/profile/:action/*',
                array(
                    'module' => 'mobile',
                    'controller' => 'profile',
                    'action' => 'index',
                    'lang_id' => ''
                ),
                array(
                    'lang_id' => '[a-z]{2}'
                ))
        );

        $router->addRoute(
            'mobile-privateProfile',
            new Zend_Controller_Router_Route('mobile/private-v2/profile/:action/*',
                array(
                    'module' => 'mobile',
                    'controller' => 'profile',
                    'action' => 'index'
                ))
        );

        $router->addRoute(
            'mobile-dashboard-def',
            new Zend_Controller_Router_Route('/dashboard/:action/*',
                array(
                    'module' => 'mobile',
                    'controller' => 'dashboard',
                    'action' => 'index'
                ))
        );

        $router->addRoute(
            'mobile-dashboard-lng',
            new Zend_Controller_Router_Route(':lang_id/dashboard/:action/*',
                array(
                    'module' => 'mobile',
                    'controller' => 'dashboard',
                    'action' => 'index',
                    'lang_id' => ''
                ),
                array(
                    'lang_id' => '[a-z]{2}'
                ))
        );

        $router->addRoute(
            'mobile-client-hotel-blacklist-load',
            new Zend_Controller_Router_Route_Regex(
                '^mobile/(([a-z]{2})/)?client-hotel-blacklist/(clients|hotels)',
                array(
                    'module' => 'mobile',
                    'controller' => 'client-hotel-blacklist',
                    'action' => 'load',
                    'lang_id' => null
                ),
                array(
                    2 => 'lang_id',
                    3 => 'type'
                )
            )
        );

        $router->addRoute(
            'mobile-client-hotel-blacklist-add',
            new Zend_Controller_Router_Route_Regex(
                '^mobile/(([a-z]{2})/)?client-hotel-blacklist/(clients|hotels)/add',
                array(
                    'module' => 'mobile',
                    'controller' => 'client-hotel-blacklist',
                    'action' => 'add',
                    'lang_id' => null
                ),
                array(
                    2 => 'lang_id',
                    3 => 'type'
                )
            )
        );

        $router->addRoute(
            'mobile-private-messaging',
            new Zend_Controller_Router_Route('/private-messaging/:action/*',
                array(
                    'module' => 'mobile',
                    'controller' => 'private-messaging',
                    'action' => 'index'
                ))
        );
        $router->addRoute(
            'mobile-private-messaging-lng',
            new Zend_Controller_Router_Route(':lang_id/private-messaging/:action/*',
                array(
                    'module' => 'mobile',
                    'controller' => 'private-messaging',
                    'action' => 'index',
                    'lang_id' => ''
                ),
                array(
                    'lang_id' => '[a-z]{2}'
                ))
        );

        $router->addRoute(
            'mob-agencies-filter-def',
            new Zend_Controller_Router_Route_Regex(
                '^mobile/(([a-z]{2})/)?(agencies)(-(.+)-([0-9]+))?',
                array(
                    'module' => 'mobile',
                    'controller' => 'agencies',
                    'action' => 'list',
                    'lang_id' => null
                ),
                array(
                    2 => 'lang_id',
                    3 => 'p_top_category',
                    5 => 'p_city_slug',
                    6 => 'p_city_id'
                )
            )
        );

        $router->addRoute(
            'mob-agencies-filter-def-page',
            new Zend_Controller_Router_Route_Regex(
                '^mobile/(([a-z]{2})/)?(agencies)-([0-9]+)',
                array(
                    'module' => 'mobile',
                    'controller' => 'agencies',
                    'action' => 'list',
                    'lang_id' => null
                ),
                array(
                    2 => 'lang_id',
                    3 => 'p_top_category',
                    4 => 'page',
                )
            )
        );

        $router->addRoute(
            'mob-agencies-filter-city-page',
            new Zend_Controller_Router_Route_Regex(
                '^mobile/(([a-z]{2})/)?(agencies)(-(.+)-([0-9]+)-([0-9]+))?',
                array(
                    'module' => 'mobile',
                    'controller' => 'agencies',
                    'action' => 'list',
                    'lang_id' => null
                ),
                array(
                    2 => 'lang_id',
                    3 => 'p_top_category',
                    5 => 'p_city_slug',
                    6 => 'p_city_id',
                    7 => 'page',
                )
            )
        );

        $router->addRoute(
            'mob-agencies-countries-filter-def',
            new Zend_Controller_Router_Route_Regex(
                '^mobile/(([a-z]{2})/)?(agencies)(-(.+)-c([0-9]+))?',
                array(
                    'module' => 'mobile',
                    'controller' => 'agencies',
                    'action' => 'list',
                    'lang_id' => null
                ),
                array(
                    2 => 'lang_id',
                    3 => 'p_top_category',
                    5 => 'p_country_slug',
                    6 => 'p_country_id'
                )
            )
        );

        $router->addRoute(
            'mob-agencies-countries-filter-def-page',
            new Zend_Controller_Router_Route_Regex(
                '^mobile/(([a-z]{2})/)?(agencies)(-(.+)-c([0-9]+)-([0-9]+))?',
                array(
                    'module' => 'mobile',
                    'controller' => 'agencies',
                    'action' => 'list',
                    'lang_id' => null
                ),
                array(
                    2 => 'lang_id',
                    3 => 'p_top_category',
                    5 => 'p_country_slug',
                    6 => 'p_country_id',
                    7 => 'page',
                )
            )
        );

        $router->addRoute(
            'mob-online-agencies',
            new Zend_Controller_Router_Route_Regex(
                '^mobile/(([a-z]{2})/)?online-(agencies)',
                array(
                    'module' => 'mobile',
                    'controller' => 'agencies',
                    'action' => 'list',
                    'online_now' => 1,
                    'lang_id' => null
                ),
                array(
                    2 => 'lang_id',
                    3 => 'p_top_category',
                )
            )
        );

        $router->addRoute(
            'mob-agencies',
            new Zend_Controller_Router_Route_Regex(
                '^mobile/agencies',
                array(
                    'module' => 'mobile',
                    'controller' => 'agencies',
                    'action' => 'list'
                )
            )
        );

        $router->addRoute(
            'mob-agency-profile-def',
            new Zend_Controller_Router_Route_Regex(
                '^mobile/(([a-z]{2})/)?agency/(.+)?\-([0-9]+)$',
                array(
                    'module' => 'mobile',
                    'controller' => 'agencies',
                    'action' => 'show',
                    'lang_id' => null
                ),
                array(
                    2 => 'lang_id',
                    3 => 'agencyName',
                    4 => 'agency_id'
                )
            )
        );

        $router->addRoute(
            'mob-favorites',
            new Zend_Controller_Router_Route('^mobile/favorites/:action/*',
                array(
                    'module' => 'mobile',
                    'controller' => 'favorites',
                    'action' => 'index'
                ))
        );
        $router->addRoute(
            'mob-favorites-lng',
            new Zend_Controller_Router_Route('^mobile/(([a-z]{2})/)?favorites/:action/*',
                array(
                    'module' => 'mobile',
                    'controller' => 'favorites',
                    'action' => 'index',
                    'lang_id' => ''
                ),
                array(
                    2 => 'lang_id'
                ))
        );

		$router->addRoute(
			'mob-private-activate',
			new Zend_Controller_Router_Route_Regex('mobile/([a-z]{2})/(.+)?/private/activate/(.+)?/([a-f0-9]{32})',
				array(
					'module' => 'mobile',
					'controller' => 'private',
					'action' => 'activate'
				), array(
					1 => 'lang_id',
					2 => 'user_type',
					3 => 'email',
					4 => 'hash'
				))
		);

		$router->addRoute(
			'mob-private-activate-def',
			new Zend_Controller_Router_Route_Regex('mobile/(.+)?/private/activate/(.+)?/([a-f0-9]{32})',
				array(
					'module' => 'mobile',
					'controller' => 'private',
					'action' => 'activate'
				), array(
					1 => 'user_type',
					2 => 'email',
					3 => 'hash'
				))
		);

        /*$router->addRoute(
            'mobile-signin',
            new Zend_Controller_Router_Route('^mobile/(([a-z]{2})/)?private/signin',
                array(
                    'module' => 'mobile',
                    'controller' => 'private-v2',
                    'action' => 'signin'
                ),
                array(
                    2 => 'lang_id'
                ))
        );

        $router->addRoute(
            'mobile-signup',
            new Zend_Controller_Router_Route('^mobile/(([a-z]{2})/)?private/signup',
                array(
                    'module' => 'mobile',
                    'controller' => 'private-v2',
                    'action' => 'signup'
                ),
                array(
                    2 => 'lang_id'
                ))
        );*/

		// </editor-fold>
















        $router->addRoute(
            'agencies',
            new Zend_Controller_Router_Route_Regex(
                '^agencies',
                array(
                    'module' => 'default',
                    'controller' => 'agencies',
                    'action' => 'list'
                )
            )
        );

		$router->addRoute(
			'contacts',
			new Zend_Controller_Router_Route('/contacts/:action/*',
				array(
					'module' => 'default',
					'controller' => 'contacts',
					'action' => 'index'
			))
		);
		$router->addRoute(
			'contacts-lng',
			new Zend_Controller_Router_Route(':lang_id/contacts/:action/*',
				array(
					'module' => 'default',
					'controller' => 'contacts',
					'action' => 'index',
					'lang_id' => ''
				),
				array(
					'lang_id' => '[a-z]{2}'
				))
		);

		$router->addRoute(
			'dashboard-def',
			new Zend_Controller_Router_Route('/dashboard/:action/*',
				array(
					'module' => 'default',
					'controller' => 'dashboard',
					'action' => 'index'					
				))
		);

		$router->addRoute(
			'dashboard-lng',
			new Zend_Controller_Router_Route(':lang_id/dashboard/:action/*',
			array(
				'module' => 'default',
				'controller' => 'dashboard',
				'action' => 'index',
				'lang_id' => ''
			),
			array(
				'lang_id' => '[a-z]{2}'
			))
		);

		$router->addRoute(
			'favorites',
			new Zend_Controller_Router_Route('/favorites/:action/*',
				array(
					'module' => 'default',
					'controller' => 'favorites',
					'action' => 'index'
				))
		);
		$router->addRoute(
			'favorites-lng',
			new Zend_Controller_Router_Route(':lang_id/favorites/:action/*',
				array(
					'module' => 'default',
					'controller' => 'favorites',
					'action' => 'index',
					'lang_id' => ''
			),
			array(
				'lang_id' => '[a-z]{2}'
			))
		);

		
		$router->addRoute(
			'alerts',
			new Zend_Controller_Router_Route('/alerts/:action/*',
				array(
					'module' => 'default',
					'controller' => 'alerts',
					'action' => 'index'
				))
		);
		$router->addRoute(
			'alerts-lng',
			new Zend_Controller_Router_Route(':lang_id/alerts/:action/*',
				array(
					'module' => 'default',
					'controller' => 'alerts',
					'action' => 'index',
					'lang_id' => ''
			),
			array(
				'lang_id' => '[a-z]{2}'
			))
		);
		
		$router->addRoute(
			'planner',
			new Zend_Controller_Router_Route('planner/:action/*',
			array(
				'module' => 'default',
				'controller' => 'planner',
				'action' => 'index'
			))
		);
		$router->addRoute(
			'planner-lng',
			new Zend_Controller_Router_Route(':lang_id/planner/:action/*',
			array(
				'module' => 'default',
				'controller' => 'planner',
				'action' => 'index',
				'lang_id' => ''
			),
			array(
				'lang_id' => '[a-z]{2}'
			))
		);
		
		$router->addRoute(
			'private-messaging',
			new Zend_Controller_Router_Route('/private-messaging/:action/*',
				array(
					'module' => 'default',
					'controller' => 'private-messaging',
					'action' => 'index'
				))
		);
		$router->addRoute(
			'private-messaging-lng',
			new Zend_Controller_Router_Route(':lang_id/private-messaging/:action/*',
				array(
					'module' => 'default',
					'controller' => 'private-messaging',
					'action' => 'index',
					'lang_id' => ''
				),
				array(
					'lang_id' => '[a-z]{2}'
				))
		);

		$router->addRoute(
			'epg-payment',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?payment',
				array(
					'module' => 'default',
					'controller' => 'online-billing',
					'action' => 'payment',
					'lang_id' => null
				),
				array(
					2 => 'lang_id'
				))
		);
		$router->addRoute(
			'epg-payment-success',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?payment-success',
				array(
					'module' => 'default',
					'controller' => 'online-billing',
					'action' => 'payment-success',
					'lang_id' => null
				),
				array(
					2 => 'lang_id'
				))
			
		);

		$router->addRoute(
			'client-hotel-blacklist-load',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?client-hotel-blacklist/(clients|hotels)',
				array(
					'module' => 'default',
					'controller' => 'client-hotel-blacklist',
					'action' => 'load',
					'lang_id' => null
				),
				array(
					2 => 'lang_id',
					3 => 'type'
				)
			)
		);

		$router->addRoute(
			'client-hotel-blacklist-add',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?client-hotel-blacklist/(clients|hotels)/add',
				array(
					'module' => 'default',
					'controller' => 'client-hotel-blacklist',
					'action' => 'add',
					'lang_id' => null
				),
				array(
					2 => 'lang_id',
					3 => 'type'
				)
			)
		);

		$router->addRoute(
			'agency-escorts-v2-def',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?escorts/agency-escorts-v2',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'agency-escorts-v2',
					'lang_id' => null
				),
				array(
					2 => 'lang_id',
					3 => 'type'
				)
			)
		);

		$router->addRoute(
			'ajax-get-cities-search',
			new Zend_Controller_Router_Route_Regex(
				'^escorts/ajax-get-cities-search',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'ajax-get-cities-search'
				)
			)
		);

		$router->addRoute(
			'ajax-get-countries-search',
			new Zend_Controller_Router_Route_Regex(
				'^escorts/ajax-get-countries-search',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'ajax-get-countries-search'
				)
			)
		);

		$router->addRoute(
			'ajax-get-top-search',
			new Zend_Controller_Router_Route_Regex(
				'^escorts/ajax-get-top-search',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'ajax-get-top-search'
				)
			)
		);

		$router->addRoute(
			'ajax-get-agencies-search',
			new Zend_Controller_Router_Route_Regex(
				'^escorts/ajax-get-agencies-search',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'ajax-get-agencies-search'
				)
			)
		);

		$router->addRoute(
			'glossary',
			new Zend_Controller_Router_Route('glossary/:action/*',
				array(
					'module' => 'default',
					'controller' => 'glossary',
					'action' => 'index'
				))
		);
		
		$router->addRoute(
			'access-page-18',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?age-check',
				array(
					'module' => 'default',
					'controller' => 'index',
					'action' => 'age-check'
				),
				array(
					2 => 'lang_id'
				)
			)
		);
		
		$router->addRoute(
			'escorts-api',
			new Zend_Controller_Router_Route('escorts-api/:action/*',
				array(
					'module' => 'default',
					'controller' => 'escorts-api',
					'action' => 'index'
				))
		);
		
		$router->addRoute(
			'popunder',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?popunder',
				array(
					'module' => 'default',
					'controller' => 'popunder',
					'action' => 'index'
				),
				array(
					2 => 'lang',
				)
			)
		);
		
		$router->addRoute(
			'popunder-escorts',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?popunder/escorts',
				array(
					'module' => 'default',
					'controller' => 'popunder',
					'action' => 'escorts'
				),
				array(
					2 => 'lang',
				)
			)
		);
		
		$router->addRoute(
			'popunder-escorts-v2',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?popunder/escorts-v2',
				array(
					'module' => 'default',
					'controller' => 'popunder',
					'action' => 'escorts-v2'
				),
				array(
					2 => 'lang',
				)
			)
		);
		
		$router->addRoute(
			'pundr-escorts',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?pundr/escorts',
				array(
					'module' => 'default',
					'controller' => 'popunder',
					'action' => 'escorts'
				),
				array(
					2 => 'lang',
				)
			)
		);
		
		$router->addRoute(
			'pundr-escorts-v2',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?pundr/escorts-v2',
				array(
					'module' => 'default',
					'controller' => 'popunder',
					'action' => 'escorts-v2'
				),
				array(
					2 => 'lang',
				)
			)
		);
				
		/*REDIRECT*/
		/*$router->addRoute(
			'redirect-escorts-to-index',
			new Zend_Controller_Router_Route('escorts',
			array(
				'module' => 'default',
				'controller' => 'redirect',
				'action' => 'index'
			))
		);*/

		$router->addRoute(
			'redirect-evaluations-to-reviews',
			new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?evaluations/(.+)',
			array(
				'module' => 'default',
				'controller' => 'redirect',
				'action' => 'reviews',
				'lang_id' => null
				),
				array(
					2 => 'lang_id',
				)
			)
		);
		
		/*$router->addRoute(
			'pundr-subdomain',
			new Zend_Controller_Router_Route_Hostname('pop.escortdirectory.com', 
			array(
				'module' => 'default',
				'controller' => 'popunder',
				'action' => 'escorts-v2'
			))
		);*/
		
		
		// POPUNDERS OF cloudsrv3.com
		
		//POPUNDER DEF
		$pundr_plain_route_def = new Zend_Controller_Router_Route('*');
		$pundr_host_route_def = new Zend_Controller_Router_Route_Hostname('www.cloudsrv3.com', 
			array(
				'module' => 'default',
				'controller' => 'popunder',
				'action' => 'escorts'
			));
		
		$router->addRoute('pundr-domain', $pundr_host_route_def->chain($pundr_plain_route_def));
		
		//POPUNDER VERSION 1
		$pundr_plain_route_v1 = new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?escorts',
				array('action' => 'escorts'),
				array( 2 => 'lang')
			);
		
		$pundr_host_route_v1 = new Zend_Controller_Router_Route_Hostname('www.cloudsrv3.com', 
			array(
				'module' => 'default',
				'controller' => 'popunder',
			));
		
		$router->addRoute('pundr-domain-v1', $pundr_host_route_v1->chain($pundr_plain_route_v1));
		
		//POPUNDER VERSION 2
		$pundr_plain_route_v2 = new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?escorts-v2',
				array('action' => 'escorts-v2'),
				array( 2 => 'lang')
			);
		
		$pundr_host_route_v2 = new Zend_Controller_Router_Route_Hostname('www.cloudsrv3.com', 
			array(
				'module' => 'default',
				'controller' => 'popunder',
			));
		
		$router->addRoute('pundr-domain-v2', $pundr_host_route_v2->chain($pundr_plain_route_v2));
		
		// POPUNDERS END
		
	}

	protected function _initAutoload()
	{
		$moduleLoader = new Zend_Application_Module_Autoloader(array(
			'namespace' => '',
			'basePath' => APPLICATION_PATH
		));

		$autoloader = Zend_Loader_Autoloader::getInstance();
		$autoloader->registerNamespace('Cubix_');


		return $moduleLoader;
	}

	protected function _initDatabase()
	{
		$this->bootstrap('db');
		$db = $this->getResource('db');
		$db->setFetchMode(Zend_Db::FETCH_OBJ);

		Zend_Registry::set('db', $db);
		Cubix_Model::setAdapter($db);

		$db->query('SET NAMES `utf8`');
	}

	protected function _initConfig()
	{
		Zend_Registry::set('images_config', $this->getOption('images'));
		Zend_Registry::set('videos_config', $this->getOption('videos'));
		Zend_Registry::set('escorts_config', $this->getOption('escorts'));
        Zend_Registry::set('mobile_config', $this->getOption('mobile'));
		Zend_Registry::set('reviews_config', $this->getOption('reviews'));
		Zend_Registry::set('faq_config', $this->getOption('faq'));
		Zend_Registry::set('blog_config', $this->getOption('blog'));
		Zend_Registry::set('feedback_config', $this->getOption('feedback'));
		Zend_Registry::set('system_config', $this->getOption('system'));
		Zend_Registry::set('newsman_config', $this->getOption('newsman'));
	}

	protected function _initViewHelpers()
	{
		$this->bootstrap('layout');
		$layout = $this->getResource('layout');
		$view = $layout->getView();

		$view->doctype('XHTML1_STRICT');

		$view->headMeta()
			//->appendHttpEquiv('Content-Type', 'text/html; charset=utf-8')
			->appendHttpEquiv('Cache-Control', 'no-cache');
		$view->headTitle()->setSeparator(' - ');
		//$view->headTitle('EscortForum Backend');
		
	}

	protected function _initDefines()
	{

	}

	protected function _initCache()
	{
		$frontendOptions = array(
			'lifetime' => null,
			'automatic_serialization' => true
		);

		$backendOptions = array(
			'servers' => array(
				array(
					'host' => 'unix:///run/memcached/memcached.sock',
					'port' => '0',
					'persistent' =>  true
				)
			),
		);

		if ( defined('IS_DEBUG') && IS_DEBUG ) {
			$cache = Zend_Cache::factory('Core', 'Blackhole', $frontendOptions, array());
		}
		else {
			$cache = Zend_Cache::factory(new Cubix_Cache_Core($frontendOptions), new Cubix_Cache_Backend_Memcached($backendOptions), $frontendOptions);
		}

		Zend_Registry::set('cache', $cache);
		
		/*************************************/
		/*$servers = array('host' => '172.16.1.51',  'port' => 11211);
		$memcache = new Memcache();
		$memcache->addServer($servers['host'], $servers['port']);
		
		Zend_Registry::set('cache_native', $memcache);*/
	}

	protected function _initBanners()
	{
		if ( defined('IS_CLI') && IS_CLI ) return;

		$banners = array();

		/*$cache = Zend_Registry::get('cache');

		$config = Zend_Registry::get('system_config');


		$banners['banners']['right_banners'] = array();
		$banners['banners']['links'] = array();


		if ( ! $banners = $cache->load('v2_ed_banners_cache') ) {
			for ($i = 0; $i < 5; $i++) {
				$banners['banners']['right_banners'][] = Cubix_Banners::GetBanner(1);
			}
			
			for ($i = 0; $i < 10; $i++) {
				$banners['banners']['right_banners'][] = Cubix_Banners::GetBanner(2);
			}
			
			for ($i = 0; $i < 100; $i++) {
				$banners['banners']['links'][] = Cubix_Banners::GetBanner(3);
			}
	
			$cache->save($banners, 'v2_ed_banners_cache', array(), $config['bannersCacheLifeTime']);
		}*/

		Zend_Registry::set('banners', $banners);
	}

	protected function _initApi()
	{
		$api_config = $this->getOption('api');

		die($api_config);
		
		Zend_Registry::set('api_config', $api_config);
		Cubix_Api_XmlRpc_Client::setApiKey($api_config['key']);
		Cubix_Api_XmlRpc_Client::setServer($api_config['server']);
	}
}
