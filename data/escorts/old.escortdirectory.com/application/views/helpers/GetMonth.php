<?php

class Zend_View_Helper_GetMonth
{
	public function getMonth($month_index, $lang_id = 'en', $long = false)
	{
		switch ($lang_id) {
			case 'en':
				$lang_id = 'en_US';
			break;
			case 'de':
				$lang_id = 'de';
			break;
			case 'it':
				$lang_id = 'it_IT';
			break;
			case 'fr':
				$lang_id = 'fr_FR';
			break;
			case 'pt':
				$lang_id = 'pt_PT';
			break;
			case 'es':
				$lang_id = 'es_ES';
			break;
			case 'ro':
				$lang_id = 'ro_RO';
			break;
			case 'ru':
				$lang_id = 'ru_RU';
			break;
		}

		$date = new Zend_Date();

		$date->set(1, Zend_Date::DAY);
		$date->set($month_index, Zend_Date::MONTH);
		
		$format = Zend_Date::MONTH_NAME_SHORT;
		
		if ($long)
			$format = Zend_Date::MONTH_NAME;
		
		$month = ucwords($date->get($format, $lang_id));

		return $month;
	}
}
