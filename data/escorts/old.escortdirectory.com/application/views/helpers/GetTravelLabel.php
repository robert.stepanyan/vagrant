<?php

class Zend_View_Helper_GetTravelLabel
{
	public function getTravelLabel($travel_id)
	{
		$div_class = '';
		switch($travel_id) {
			case TRAVEL_WORLDWIDE_I_USA :
				$div_class = 'travel-worldwide';
			break;
			case TRAVEL_WORLDWIDE_E_USA :
				$div_class = 'travel-worldwide-excl-usa';
			break;
			case TRAVEL_EUROPE :
				$div_class = 'travel-europe';
			break;
		}
		return '<div class="' . $div_class . '"></div>';
	}
}

