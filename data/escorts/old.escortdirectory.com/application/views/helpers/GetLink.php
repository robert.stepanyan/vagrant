<?php

class Zend_View_Helper_GetLink
{
	protected static $_params;
	
	public function getLink($type = '', array $args = array(), $clean = false, $no_args = false)
	{
		$lang_id = Cubix_I18n::getLang();

		$link = '/';
		if ( $lang_id != Cubix_Application::getDefaultLang() ) {
			$link .= $lang_id . '/';
		}
		
		if ( isset($args['state']) && 7 != Cubix_Application::getId() ) {
			$args['region'] = $args['state'];
			unset($args['state']);
		}
		
		switch ( $type ) {
			case 'home':
				$link = ($lang_id == 'en') ? '/' : '/' . $lang_id;
				break;
			case 'top-category':
				$link .= $args['top_category'];
				unset($args['top_category']);
				break;			
			case 'navigation-link':
				/*if( isset($args['online_now']) && $args['online_now'] ) {
					$link = preg_replace("#\/#", '/online-', $link);					
				}*/
				$clean_link = $link;

				if( isset($args['top_category']) ) {

					if ( $args['top_category'] == 'escorts' && ( !(isset($args['show_escorts']) && $args['show_escorts']) && !(isset($args['country_slug']) && strlen($args['country_slug'])) && !(isset($args['city_slug']) && strlen($args['city_slug'])) ) ) {
						if( isset($args['online_now']) && $args['online_now'] ) {
							$link = '/online-escorts';
						}
					} else {
						$link .= $args['top_category'];
					}					

					if ( (isset($args['city_slug']) && strlen($args['city_slug'])) || (isset($args['country_slug']) && strlen($args['country_slug'])) ) {
						$link .= '-';
					}
				}

				if( isset($args['city_slug']) && strlen($args['city_slug']) ) {
					$link .= $args['city_slug'];
				}

				if( isset($args['city_id']) && strlen($args['city_id']) && $args['city_id'] !== 0 ) {
					$link .= '-' . $args['city_id'];
				}

				if( isset($args['country_slug']) && strlen($args['country_slug']) ) {
					$link .= $args['country_slug'];
				}

				if( isset($args['country_id']) && strlen($args['country_id']) && $args['country_id'] !== 0 ) {
					$link .= '-c' . $args['country_id'];
				}

				if( isset($args['page']) && $args['page'] ) {
					$link .= '-' . $args['page'];
				}


				if( isset($args['online_now']) && $args['online_now'] ) {
					$link = $clean_link . 'online-' . $args['top_category'];					
				}


				unset($args['page']);
				unset($args['top_category']);
				unset($args['city_slug']);
				unset($args['country_slug']);
				unset($args['city_id']);
				unset($args['country_id']);
				unset($args['show_escorts']);
				unset($args['online_now']);

				break;
			case 'payment':
				$link .= 'payment';
				break;
			case 'online-now-escorts':
				$link .= 'online-escorts';
				break;
			case 'sexy-euro-16':
				$link .= 'sexy-euro-16';
				break;
			case 'latest-actions-index':
				$link .= 'latest-actions';
				break;
			case 'latest-actions-ajax-header':
				$link .= 'latest-actions/ajax-header?ajax';
				break;
			case 'latest-actions-ajax-list':
				$link .= 'latest-actions/ajax-list?ajax';
				break;
			case 'latest-actions-ajax-get-details':
				$link .= 'latest-actions/ajax-get-details?ajax';
				break;

			case 'photo-feed-index':
				$link .= 'photo-feed';
				break;
			case 'photo-feed-ajax-list':
				$link .= 'photo-feed/ajax-list?ajax';
				break;
			case 'photo-feed-ajax-header':
				$link .= 'photo-feed/ajax-header?ajax';
				break;
			case 'photo-feed-ajax-voting-box':
				$link .= 'photo-feed/ajax-voting-box/';
				break;

			case 'photo-feed-ajax-voting-box':
				$link .= 'photo-feed/ajax-voting-box/';
				break;
			case 'photo-feed-ajax-more-photos':
				$link .= 'photo-feed/ajax-get-more-photos/';
				break;
			case 'photo-feed-ajax-vote':
				$link .= 'photo-feed/ajax-vote/';
				break;

			case 'glossary':
				$link .= 'glossary';
				break;
			case 'forum':
				$link = 'http://www.escort-annonce.com/forum/?lang=' . $lang_id;
				break;
			case 'edforum':
			$link = 'http://forum.escortdirectory.com/';
			break;
			case 'chat':
				$link = 'http://chat.escort-annonce.com/';
				break;
			case 'cams':
				$link = 'http://escort-annonce.streamray.com/';
				break;
			case 'search':
				$link .= 'search';
				break;
			case 'escorts-list':
				$link .= 'escorts';
			break;
			case 'susp-photo':
				$link .= 'escorts/ajax-susp-photo';
			break;
			case 'classified-ads-index':
				$link .= 'classified-ads';
				break;
			case 'classified-ads-place-ad':
				$link .= 'classified-ads/place-ad';
				break;
			case 'classified-ads-success':
				$link .= 'classified-ads/success';
				break;
			case 'classified-ads-error':
				$link .= 'classified-ads/error';
				break;
			case 'classified-ads-ajax-filter':
				$link .= 'classified-ads/ajax-filter?ajax';
				break;
			case 'classified-ads-ajax-list':
				$link .= 'classified-ads/ajax-list?ajax';
				break;
			case 'classified-ads-print':
				$link .= 'classified-ads/print';
				break;
			case 'classified-ads-ad':
				$link .= 'classified-ads/' . $args['title'] . '/ad/' . $args['id'];
				unset($args['id']);
				unset($args['title']);
				break;
			case 'base-city':
				$section = isset($args['section']) ? $args['section'] . '/' : '';
				if ( strlen($section) ) unset($args['section']);

				$link .= 'escorts/' . $section . 'city_' . $args['city'];
				unset($args['city']);
			break;
			case 'escorts':
				if ( empty(self::$_params) ) {
					$request = Zend_Controller_Front::getInstance()->getRequest();
					$req = trim($request->getParam('req', ''), '/');
					$req = explode('/', $req);
					
					$params = array();
					
					foreach ($req as $r)  {
						$param = explode('_', $r);
						if ( count($param) < 2 ) {
							$params[] = $r;
							continue;
						}
						
						$param_name = reset($param);
						array_shift($param);
						$params[$param_name] = implode('_', $param);
					}
					
					self::$_params = $params;
				}
				
				if ( $clean ) {
					$params = array();
				}
				else {
					$params = self::$_params;
				}
				
				foreach ( $args as $key => $value ) {
					if ( is_null($value) ) {
						unset($params[$key]);
						continue;
					}
					
					$params[$key] = $value;
				}
				
				$link .= 'escorts/';
				foreach ( $params as $param => $value ) {
					if ( ! strlen($param) || ! strlen($value)) continue;
					if ( is_int($param) ) {
						if ( $value == 'tours' ) $value = 'citytours';
						$link .= $value . '/';
					}
					elseif ( strlen($param) ) {
						if ( $param == 'page' ) {
							if ( $value - 1 > 0 ) {
								$link .= ($value - 1) . '/';
							}
						}
						else {
							$link .= $param . '_' . $value . '/';
						}
					}
				}
				
				$link = rtrim($link, '/');
				
				$args = array();
			break;
			case 'gotm':
				$link .= 'girl-of-month';

				if ( isset($args['history']) && $args['history'] ) {
					$link .= '/history';
				}
				
				if ( isset($args['page']) ) {
					if ( $args['page'] != 1 ) {
						$link .= '/page_' . $args['page'];
						//$link .= '/page_' . $args['page'];
					}
					
					unset($args['page']);
				}
				break;
			case 'profile':
				$link .= 'escort/' . $args['showname'] . '-' . $args['escort_id'];
				unset($args['showname']);
				unset($args['escort_id']);
			break;
			case 'forgot':
				$link .= 'private/forgot';
			break;
			/*case 'signup':
				$link .= 'private/signup-';
				
				if ( ! isset($args['type']) ) {
					$args['type'] = 'member';
				}
				
				$link .= $args['type'];
			break;*/
			case 'signup':
				$link .= 'private/signup';
			break;
			case 'signin':
				$link .= 'private/signin';
			break;
			case 'signout':
				$link .= 'private/signout';
			break;
			case 'private':
				$link .= 'private';
			break;
			case 'membership-type':
				$link .= 'private/membership-type';
			break;
			case 'feedback':
				$link .= 'feedback';
			break;
			case 'contact':
				$link .= 'contact';
			break;
			case 'contact-us':
				$link .= 'contacts/contact-us';
			break;
			case 'contact-us-success':
				$link .= 'contacts/contact-us-success';
			break;
			case 'about':
				$link .= 'about';
			break;
			case 'captcha':
				$link .= 'captcha?' . rand();
				//$link = '/img/sample_captcha.gif?' . rand();
			break;
			case 'viewed-escorts':
				$link .= 'escorts/viewed-escorts';
			break;
			case '100p-verify':
				$link .= 'private/verify';
			break;
			case '100p-verify-webcam':
				$link .= 'private/verify/webcam';
			break;
			case '100p-verify-idcard':
				$link .= 'private/verify/idcard';
			break;
			case '100p-verify-idcard-popup':
				$link .= 'verify/idcard';
			break;
			case 'agency':
				$link .= 'agency/' . $args['slug'];
				unset($args['slug']);
			break;
			case 'agency-profile':
				$link .= 'agency/' . $args['agency_slug'] . '-' . $args['agency_id'];
				unset($args['agency_slug']);
				unset($args['agency_id']);
			break;
			case 'blog':
				$link .= 'blog';
			break;
			case 'blog-top10':
				$link .= 'blog/top10';
				break;
			case 'blog-archive':
				$link .= 'blog/' . $args['month'] . '.' . $args['year'];

				unset($args['month']);
				unset($args['year']);
			break;
			case 'blog-post':
				$link .= 'blog/' . $args['slug'] . '-' . $args['id'];
				unset($args['slug']);
				unset($args['id']);
			break;
			
			case 'favorites':
				$link .= 'private/favorites';
			break;
			case 'add-to-favorites':
				$link .= 'private/add-to-favorites';
			break;
			case 'remove-from-favorites':
				$link .= 'private/remove-from-favorites';
			break;
			
			// Private Area
            case 'simple-profile':
				$link .= 'private-v2/profile/simple';
				break;
			case 'edit-profile':
				$link .= 'private/profile';
			break;
			case 'edit-agency-profile':
				$link .= 'private/agency-profile';
			break;
			case 'profile-data':
				$link .= 'private/profile-data';
			break;
			case 'edit-photos':
				$link .= 'private/photos';
			break;
			case 'edit-rates':
				$link .= 'private/rates';
			break;
			case 'edit-escorts':
				$link .= 'private/escorts';
			break;
			case 'delete-escort':
				$link .= 'private/delete-escort';
			break;
			case 'edit-tours':
				$link .= 'private/tours';
			break;
			case 'change-passwd':
				$link .= 'private/change-password';
			break;
			case 'change-passwdord':
				$link .= 'private-v2/change-password';
			break;
			case 'update_watched_type':
				$link .= 'private-v2/update-watched-type';
			break;
			
			case 'search':
				$link .= 'search';
			break;
			case 'links':
				$link .= 'links';
			break;
			case 'late-night-girls':
				$link .= 'escorts/late-night-girls';
			break;
			
			case 'terms':
				$link .= 'page/terms-and-conditions';
			break;
			case 'webcam-lm':
				$link .= 'page/webcam-verification-learn-more';
			break;
			case 'passport-lm':
				$link .= 'page/passport-verification-learn-more';
			break;
			
			case 'external-link':
				$link = '/go?' . $args['link'];
				unset($args['link']);
			break;
			// V2 Private Area
			case 'private-v2':
				$link .= 'private-v2';
				break;
			case 'private-v2-settings':
				$link .= 'private-v2/settings';
				break;
			case 'private-v2-happy-hour':
				$link .= 'private-v2/happy-hour';
				break;
			case 'private-v2-schedule':
				$link .= 'private-v2/schedule';
				break;
			case 'private-v2-client-blacklist':
				$link .= 'private-v2/client-blacklist';
				break;
			case 'private-v2-add-client-to-blacklist':
				$link .= 'private-v2/add-client-to-blacklist';
				break;
			case 'private-v2-profile':
				$link .= 'private-v2/profile';
				break;
			case 'private-v2-photos':
				$link .= 'private-v2/photos';
				break;
			case 'private-v2-plain-photos':
				$link .= 'private-v2/plain-photos';
				break;
			case 'private-v2-video':
				$link .= 'private-v2/video';
				break;
			case 'private-v2-nat-pic':
				$link .= 'private-v2/natural-pic';
				break;
			case 'private-v2-tours':
				$link .= 'private-v2/tours';
				break;
			case 'private-v2-ajax-tours':
				$link .= 'private-v2/ajax-tours';
				break;
			case 'private-v2-ajax-tours-add':
				$link .= 'private-v2/ajax-tours-add';
				break;
			case 'private-v2-ajax-tours-remove':
				$link .= 'private-v2/ajax-tours-remove';
				break;
			case 'private-v2-verify':
				$link .= 'private-v2/verify';
				break;
			case 'private-v2-faq':
				$link .= 'private-v2/faq';
				break;
			case 'private-v2-premium':
				$link .= 'private-v2/premium';
				break;
			case 'private-v2-support':
				$link .= 'support';
				break;
			case 'private-v2-get-urgent-message':
				$link .= 'private-v2/get-urgent-message';
				break;
			case 'private-v2-set-urgent-message':
				$link .= 'private-v2/set-urgent-message';
				break;
			case 'private-v2-get-rejected-verification':
				$link .= 'private-v2/get-rejected-verification';
				break;
			case 'private-v2-set-rejected-verification':
				$link .= 'private-v2/set-rejected-verification';
				break;
			case 'alerts':
				$link .= 'private-v2/alerts';
				break;
			case 'private-v2-agency-profile':
				$link .= 'private-v2/agency-profile';
				break;
			case 'private-v2-member-profile':
				$link .= 'private-v2/member-profile';
				break;
			case 'private-v2-upgrade-premium':
				$link .= 'private-v2/upgrade';
				break;
			case 'ticket-open':
				$link .= 'support/ticket';
				break;
			case 'private-v2-escorts':
				$link .= 'private-v2/escorts';
				break;
            case 'private-v2-escorts-delete':
				$link .= 'private-v2/profile-delete';
				break;
             case 'private-v2-escorts-restore':
				$link .= 'private-v2/profile-restore';
				break;
			case 'reviews':
				$link .= 'reviews';
				break;
			case 'escort-reviews':
				$link .= 'reviews/' . $args['showname'] . '-' . $args['escort_id'];
				unset ($args['showname']);
				unset ($args['escort_id']);
				break;
			case 'member-reviews':
				$link .= 'reviews/member/' . $args['username'];
				unset ($args['username']);
				break;
			case 'add-review':
				$link .= 'reviews/add-review';
				break;
			case 'my-reviews':
				$link .= 'reviews/my-reviews';
				break;
			case 'voting-widget':
				$link .= 'escort/' . $args['showname'] . '-' . $args['escort_id'] . '/vote';
				unset($args['showname']);
				unset($args['escort_id']);
				break;
			case 'private-v2-add-reply';
				$link .= 'private-v2/add-reply';
				break;
			case 'private-v2-ajax-escort-comments';
				$link .= 'private-v2/ajax-escort-comments';
				break;
            case 'comments':
				$link .= 'comments';
				break;
			case 'member-info':
				$link .= 'member/'.$args['username'];
				unset($args['username']);
				break;
			case 'member-comments':
				$link .= 'members/get-member-comments';
				break;
			case 'tell-friend':
				$link .= 'escorts/ajax-tell-friend';
				break;
			case 'susp-photo':
				$link .= 'escorts/ajax-susp-photo';
				break;
			case 'report-problem':
				$link .= 'escorts/ajax-report-problem';
				break;
			case 'get-gallery-photos':
				$link .= 'escorts/gallery-photos';
				break;
			case 'get-filter':
				$link .= 'escorts/get-filter';
				break;
			case 'dashboard':
				$link .= 'dashboard';
				break;
			case 'email-collecting-popup-confirmed':
				$args['ec_confirm'] = 1;
				$link .= 'index/email-collecting-popup';
				break;
			case 'my-video':
				$link .= 'video/';
			break;
		}

		if ( ! $no_args ) {
			if ( count($args) ) {
				$link .= '?';
				$params = array();
				foreach ( $args as $arg => $value ) {
					if ( ! is_array($value) ) {
						$params[] = $arg . '=' . urlencode($value);
					}
					else {
						foreach ( $value as $v ) {
							$params[] = $arg . '[]=' . $v;
						}
					}
				}

				$link .= implode('&', $params);
			}
		}
		
		return rtrim($link, '/') . '/';
	}
}
