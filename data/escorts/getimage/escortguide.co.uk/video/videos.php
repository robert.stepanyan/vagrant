<?php

define('DEBUG', true);
set_time_limit(0);
$dimensions = array(
	"720" => array(
		'height' => 720, 'bitrate' => 1200
	),
	"480" => array(
		'height' => 480, 'bitrate' => 600
	),
	"360" => array(
		'height' => 360, 'bitrate' => 250
	)
);

function __error($msg)
{

	if ( defined('DEBUG') && DEBUG ) {
		echo date('[H:i:s] ') . $msg . "<br/>\n";
		print_r(debug_backtrace());
	}
	die;
}

function __getVideoInfo($fileName)
{
	ob_start();
	passthru("ffmpeg -i ".$fileName." 2>&1");
	$video_info = ob_get_contents();
	ob_end_clean();

	if((strpos($video_info, 'bitrate:')===false) || (strpos($video_info, 'Video:')===false) || (strpos($video_info, 'Duration:')===false)){
		 __error('not video file');
	}
	
	$video_data = array();
	$video_data['size'] = number_format((filesize($fileName)/pow(1024,2)),2);
	preg_match('/bitrate: ([0-9]+)/', $video_info, $match);
	$video_data['bitrate'] = $match[1];
	preg_match('/Stream #(?:[0-9\.]+)(?:.*)\: Video: (?P<videocodec>.*) (?P<width>[0-9]*)x(?P<height>[0-9]*)/', $video_info, $match);
	$video_data['height'] = $match['height'];
	$video_data['width'] =  $match['width'];
	preg_match("/Duration: ([\d]{2}):([\d]{2}):([\d]{2}).([\d]{1,2}), start/si", $video_info, $match);
	$video_data['duration'] = $match[1] * 3600 + $match[2] * 60 + $match[3];

	return $video_data;
}


$app = isset($_REQUEST['app']) ? $_REQUEST['app'] : null;
$act = isset($_REQUEST['a']) ? $_REQUEST['a'] : null;


if ( $act == 'set-dimensions' ) {
	
	$escort_id = isset($_REQUEST['eid']) ? $_REQUEST['eid'] : null;
	$hash = isset($_REQUEST['hash']) ? $_REQUEST['hash'] : null;
	$ext = isset($_REQUEST['ext']) ? $_REQUEST['ext'] : null;
	$max_height = isset($_REQUEST['height']) ? $_REQUEST['height']: null;
	$max_width = isset($_REQUEST['width']) ? $_REQUEST['width']: null;
	
	$parts = array();
	$is_converting = false; 
	$parts[] = $escort_id;
		
	$path = implode('/', $parts);

	if ( ! is_dir($dir = $app . '/' . $path) ) {
		__error("escort with id `$escort_id` does not have videos");
	}
	
	$video_hash = $max_height ? $hash .'_'.$max_height.'p' : $hash;
	$video = $dir . '/' . $video_hash . '.' . $ext;
	
	if (file_exists($video)) {
		
		if(is_null($max_height) || is_null($max_width)){
			$video_data = __getVideoInfo($video);
			$max_height = $video_data['height'];
			$max_width = $video_data['width'];
			$is_converting = true;
		}
		
	}
	else{
		__error("escort with id `$escort_id` does not have '$video' video");
	}
	
	foreach($dimensions as $dimension){
		
		$filename = $dir . '/' . $hash ."_". $dimension['height'] ."p.mp4";
		if (file_exists($filename)) {
			CONTINUE;
		}
		if( $max_height >= $dimension['height'] &&  $is_converting){
			
			//Odd number for width is not alloyable  
			$width = intval(($dimension['height'] * $max_width) / ( $max_height * 2 ) ) * 2;
			exec("ffmpeg -i $video -s ".$width."x".$dimension['height']." -vcodec libx264 -acodec libmp3lame -b ".$dimension['bitrate']."k -ab 196k $filename ");
			echo json_encode(array('height' => $dimension['height'], 'width' => $width));
			$is_converting = false;		
		}
		elseif($max_height > $dimension['height']){
			
			//Odd number for width is not alloyable  
			$width = intval(($dimension['height'] * $max_width) / ( $max_height * 2 ) ) * 2;
			exec("ffmpeg -i $video -s ".$width."x".$dimension['height']." -vcodec libx264 -acodec libmp3lame -b ".$dimension['bitrate']."k -ab 196k $filename");
		}
	}
}
die;