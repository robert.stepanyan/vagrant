<?php

set_time_limit(0);

require_once('common.php');

define('APP_NEW_ID',		1);
define('APP_HOST', 		'temp.pic');
define('APP_COUNTRY', 		101);
define('DEST_DIR',		'temp.test');
define('SERVER_OWNER',		'nobody');
define('SERVER_GROUP',		'nobody');
define('SERVER_PERMS',		0666);
define('SERVER_DIR_PERMS',	0777);

function stage_dlphotos()
{
	global $sDB;
	$photos = $sDB->fetchAll('
		SELECT e.___id AS old_id, e.showname, ep.escort_id, ep._inter_index, ep.hash, ep.ext, ep.type
		FROM escort_photos ep
		INNER JOIN escorts e ON e.id = ep.escort_id
		INNER JOIN users u ON u.id = e.user_id
		WHERE 
			u.application_id = ' . APP_NEW_ID . '/* AND 
			e.___id = 121163*/
		ORDER BY e.showname ASC
	');
	
	$photos_count = $sDB->fetchOne('
		SELECT COUNT(ep.id)
		FROM escort_photos ep
		INNER JOIN escorts e ON e.id = ep.escort_id
		INNER JOIN users u ON u.id = e.user_id
		WHERE 
			u.application_id = ' . APP_NEW_ID . '/* AND 
			e.___id = 121163*/
	');

	$c = 0;
	foreach ( $photos as $photo ) { //if ( $photo['showname'] != 'Mila_25_' ) continue; else { var_dump(filesize($file)); }
		$old_id = $photo['escort_id'];
		if ( $photo['old_id'] ) $old_id = $photo['old_id'];

		$file = _photos_get_path($photo);
		$c++;
		
		echo "[$c/$photos_count] (" . round($c / $photos_count * 100)  . "%) Escort '{$photo['showname']}':";
		
		if ( is_file($file) && filesize($file) > 10 ) {
			echo " skipped: $file\r\n";
			continue;
		}
		
		echo " download: $file - ";

		_photos_make_path($file);
		
		//$url = 'http://' . APP_HOST . '/ef/' . $old_id . '/' . $photo['_inter_index'] . '_orig.jpg';
		$url = 'http://pic.escortforum.net/' . $old_id . '/' . $photo['_inter_index'] . '_orig.jpg';

		// if ( $photo['escort_id'] == 134269 ) { echo $url; die; }

		$data = @file_get_contents($url);
		if ( false === $data ) {
			echo 'could not download (' . $url . ')' . "\n";
			continue;
		}

		file_put_contents($file, $data);

		chown($file, SERVER_OWNER);
		chgrp($file, SERVER_GROUP);
		chmod($file, SERVER_PERMS);

		echo "done\r\n";
	}
}

function _photos_get_path($photo)
{
	$catalog = $photo['escort_id'];
	
	$parts = array();
	if ( strlen($catalog) > 2 ) {
		$parts[] = substr($catalog, 0, 2);
		$parts[] = substr($catalog, 2);
	}
	else {
		$parts[] = '_';
		$parts[] = $catalog;
	}
	
	if ( 3 == $photo['type'] ) {
		$parts[] = 'private';
	}
	
	$catalog = implode('/', $parts);
	
	$path = './' . DEST_DIR . '/' . $catalog . '/' . $photo['hash'] . '.' . $photo['ext'];
	
	return $path;
}

function _photos_make_path($path)
{
	$path = explode('/', $path);
	$path = array_slice($path, 0, count($path));
	
	for ( $i = 1; $i < count($path); $i++ ) {
		if ( ! is_dir($p = implode('/', array_slice($path, 0, $i))) ) {
			mkdir($p, SERVER_DIR_PERMS);

			chown($p, SERVER_OWNER);
			chgrp($p, SERVER_GROUP);
			chmod($p, SERVER_DIR_PERMS);
		}
	}
}

$stage = isset($_REQUEST['stage']) ? $_REQUEST['stage'] : null;

$stage = 'dlphotos';

$func = 'stage_' . $stage;

if ( ! is_callable($func) ) {
	die('Invalid migration stage');
}
call_user_func($func);
