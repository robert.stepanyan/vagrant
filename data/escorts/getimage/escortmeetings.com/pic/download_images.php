<?

date_default_timezone_set('Europe/Paris');

if (isset($_POST))
{
	if (isset($_POST['escort_id']) && isset($_POST['photos']))
	{
		$showname = $url = preg_replace('# #', '_', $_POST['showname']);
		$host = $_POST['host'];
		
		$original_path = '/data/www/pic.' . $host . '/' . $host . '/';
		$download_path = '/data/www/pic.' . $host . '/download/';
		$dir_name = $showname . '_' . $_POST['escort_id'] . '_' . date('d-M-Y_H-i');
						
		if (!is_dir($download_path . $dir_name))
			mkdir($download_path . $dir_name, 0777);
				
		foreach ($_POST['photos'] as $photo)
		{
			$parts = explode('/', $photo);
			$path = $original_path . $parts[4] . '/' . $parts[5] . '/' . $parts[6];
			
			copy($path, $download_path . $dir_name . '/' . $parts[6]);
		}
		
		if (is_file($download_path . $dir_name . '.zip'))
		{
			unlink($download_path . $dir_name . '.zip');
		}
		
		$zip_file = $download_path . $dir_name . '.zip';
		
		if (exec('zip -r -j ' . $zip_file . ' ' . $download_path . $dir_name))
		{
			if (is_dir($download_path . $dir_name))
				system('rm -rf ' . $download_path . $dir_name);
		}
		
		echo 'http://pic.' . $host . '/download/' . $dir_name . '.zip';
	}
}

?>